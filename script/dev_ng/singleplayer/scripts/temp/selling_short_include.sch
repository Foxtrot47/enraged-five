// *****************************************************************************************
// *****************************************************************************************
//
//		FILE NAME		:	selling_short_include.sch
//		AUTHOR			:	Aaron Gandaa/Ste Kerrigan
//		DESCRIPTION		: 	TEMP SCRIPT TO BE USED BY NYC FOR PRERENDER
//
//							This is the Life Invader presentation from Lester 1B. This is
//							the whole presentation that plays out should the player fail to
//							make the call to the prototype cell phone in time.
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "rgeneral_include.sch"
USING "cutscene_public.sch"

//----------------------
//	CONSTS
//----------------------
ENUM FRONT_BLOCKS
	BLOCK_FRONT_1_A = 0,
	BLOCK_FRONT_1_B,
	BLOCK_FRONT_1_C,
	BLOCK_FRONT_1_E,
	BLOCK_FRONT_1_F,
	BLOCK_FRONT_1_G,
	BLOCK_FRONT_1_H,

	BLOCK_FRONT_2_A,
	BLOCK_FRONT_2_B,
	BLOCK_FRONT_2_C,
	BLOCK_FRONT_2_D,
	BLOCK_FRONT_2_E,
	BLOCK_FRONT_2_F,
	BLOCK_FRONT_2_G,
	BLOCK_FRONT_2_H,
	BLOCK_FRONT_2_I,
	BLOCK_FRONT_2_J,

	BLOCK_FRONT_3_A,
	BLOCK_FRONT_3_B,
	BLOCK_FRONT_3_C,
	BLOCK_FRONT_3_D,
	BLOCK_FRONT_3_E,
	BLOCK_FRONT_3_F,
	BLOCK_FRONT_3_G,
	BLOCK_FRONT_3_H,
	BLOCK_FRONT_3_I,
	BLOCK_FRONT_3_J,
	MAX_FRONT_BLOCKS
ENDENUM

ENUM MID_BLOCKS
	
	BLOCK_MID_1_A = 0,
	BLOCK_MID_1_B,
	BLOCK_MID_1_C,
	BLOCK_MID_1_D,
	BLOCK_MID_1_E,
	BLOCK_MID_1_F,
	BLOCK_MID_1_G,
	BLOCK_MID_1_H,
	BLOCK_MID_1_I,
	BLOCK_MID_1_J,
	
	BLOCK_MID_2_A,
	BLOCK_MID_2_B,
	BLOCK_MID_2_C,
	BLOCK_MID_2_D,
	BLOCK_MID_2_E,
	BLOCK_MID_2_F,
	BLOCK_MID_2_G,
	BLOCK_MID_2_H,
	BLOCK_MID_2_I,
	BLOCK_MID_2_J,
	
	BLOCK_MID_3_A,
	BLOCK_MID_3_B,
	BLOCK_MID_3_C,
	BLOCK_MID_3_D,
	BLOCK_MID_3_E,
	BLOCK_MID_3_F,
	BLOCK_MID_3_G,
	BLOCK_MID_3_H,
	BLOCK_MID_3_I,
	BLOCK_MID_3_J,
	
	MAX_MID_BLOCKS
ENDENUM

ENUM BACK_BLOCKS
	BLOCK_BACK_1_A,
	BLOCK_BACK_1_B,
	BLOCK_BACK_1_C,
	BLOCK_BACK_1_D,
	BLOCK_BACK_1_E,
	
	BLOCK_BACK_2_A,
	BLOCK_BACK_2_B,
	BLOCK_BACK_2_C,
	BLOCK_BACK_2_D,
	BLOCK_BACK_2_E,
	
	BLOCK_BACK_3_A,
	BLOCK_BACK_3_B,
	BLOCK_BACK_3_C,
	BLOCK_BACK_3_D,
	BLOCK_BACK_3_E,
	
	BLOCK_BACK_4_A,
	BLOCK_BACK_4_B,
	BLOCK_BACK_4_C,
	
	BLOCK_BACK_5_A,
	BLOCK_BACK_5_B,
	BLOCK_BACK_5_C,
	
	BLOCK_BACK_6_A,
	BLOCK_BACK_6_B,
	BLOCK_BACK_6_C,
	
	MAX_BACK_BLOCKS
ENDENUM

ENUM CUTSCENE_REACTIONS_SECTIONS
	WALKS_ON_STAGE = 0, 
	IDLE_POINT_1,
	CHEER_POINT_1,
	IDLE_POINT_2,
	CHEER_POINT_2,
	IDLE_POINT_3,	
	DISPLEASED_IFRUIT,
	IDLE_POINT_4,
	CHEER_POINT_3,
	IDLE_POINT_5,
	CHEER_POINT_4,
	IDLE_POINT_6,
	CHEER_POINT_5,
	IDLE_POINT_7,
	BOARD,
	FACE_EXPLODE,

	MAX_REACTIONS
ENDENUM

CONST_INT MAX_BLOCKS		87
CONST_INT MAX_ROWS			9
CONST_INT MAX_PEDMODELS		29		// don't make this bigger than 32 or the bit field stuff will go wrong.

//----------------------
//	STRUCT
//----------------------
STRUCT SPARE_SEAT
	BOOL bInUse = FALSE
	VECTOR vCoords		// center of seat
	VECTOR vRot			// rotation in coordinate form
	INT iAnimDict = 0	// number of the animdict to use
	FLOAT fSeatOffset = 0.0	// amount to shift seat (1 is 1 whole seat)
	FLOAT fSecondarySeatOffset = 0.0	// amount to shift seat (1 is 1 whole seat)
	PED_INDEX pedID
ENDSTRUCT

STRUCT HIGH_FID_ROW_DATA
	VECTOR vCoords		// center of row
	VECTOR vRot			// rotation in coordinate form
	
	INT iNumSeats = 6	// number of seats in row (0 - 6)
	INT iSeed			// this seed is used to pick what models and ped variations to use - make this unique for each row
	INT iAnimDict = 0	// number of the animdict to use
	
	FLOAT fSeatOffset = 0.0	// amount to shift seat (1 is 1 whole seat)
	
	PED_INDEX pedID[6]	// array of peds
ENDSTRUCT

STRUCT LOW_FID_ROW_DATA
	VECTOR vCoords[4]		// center of the block
	VECTOR vRot			// rotation in coordinate form
	
	MODEL_NAMES 	mod
	OBJECT_INDEX objectID[4]	// array of peds
ENDSTRUCT

STRUCT HIGH_FID_BLOCK_DATA
	HIGH_FID_ROW_DATA 	rowComplete[MAX_ROWS]		// these are rows with 6 people in
	INT iHighRowsUsed = 0
	SPARE_SEAT spareSeat[2]
ENDSTRUCT

STRUCT LOW_FID_BLOCK_DATA
	LOW_FID_ROW_DATA 	LowBlock
	INT 				iLowRowsUsed = 0
	BOOL				bUseLowPoly = FALSE
ENDSTRUCT

STRUCT TIME_STRUCT
	INT h
	INT m
	INT s
ENDSTRUCT
TIME_STRUCT sTime

STRUCT CUTSCENE_REACTION
	INT iStart[MAX_REACTIONS]
	INT iEnd[MAX_REACTIONS]
ENDSTRUCT
CUTSCENE_REACTION mCutsceneReact
INT iReactionBeingPerformed
//----------------------
//	VARIABLES
//----------------------
MODEL_NAMES crowdPedModel[MAX_PEDMODELS]
INT iModelsLeftCount = MAX_PEDMODELS
HIGH_FID_BLOCK_DATA crowdBlocksHigh[MAX_FRONT_BLOCKS]
	INT iOldBlockTurnedOnHIGH
LOW_FID_BLOCK_DATA crowdBlocksLowMid[MAX_MID_BLOCKS]
	INT iOldBlockTurnedOnLOW_MID
LOW_FID_BLOCK_DATA crowdBlocksLowBack[MAX_BACK_BLOCKS]
	INT iOldBlockTurnedOnLOW_BACK
INT iModelsUsedBitField	= 0				// this is a bit field which tells us which models are used up
FLOAT fSeatFwdShift = 0.075				// to stop people having their backs embedded in the chair
FLOAT SEAT_SPACING = 0.725

#IF IS_DEBUG_BUILD
	BOOL bBlockSwitchHigh[MAX_FRONT_BLOCKS]

	BOOL bBlockSwitchLowMid[MAX_MID_BLOCKS]
	
	BOOL bBlockSwitchLowBack[MAX_BACK_BLOCKS]
	
	BOOL bUseSeatDummy = FALSE
	FLOAT fTestSeatOffset
	VECTOR vTestSeatPos
	INT iTestSeatBlock
	INT iTestSeatRow
//	INT iBlocksActive
//	INT iBlocksActiveLow
#ENDIF

SCALEFORM_INDEX sLifeInvaderOverlay
SCALEFORM_INDEX sFormBreakingNewsOverlay
INT	rndrTgtPresentation
INT rndrTgtDefault
BOOL bShowOverlay = FALSE
BOOL bShowVersionNumber = FALSE
BOOL bExtractOffset = TRUE
BOOL bInstaBlend = TRUE
INT iScriptStage = 0

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID m_widgetGroup
	BOOL bOutputTTY = FALSE
#ENDIF

OBJECT_INDEX testSeatDummy
INT iPseudoRandomParam	// this is used to internally to generate random numbers
INT iRandomType = 0

SCENARIO_BLOCKING_INDEX siBlocking
//STREAMVOL_ID cStreamVol

BOOL bUseManualReactions = TRUE

//----------------------
//	RAND FUNCS
//-----------------------

/// PURPOSE:
///    Does Stick's random number generator
/// RETURNS:
///    Some value that we stick in our other functions
FUNC INT DO_PSUEDO_RANDOM()

	SWITCH (iRandomType)
		CASE 0 // RAND_STICK
			iPseudoRandomParam = (iPseudoRandomParam * 1664525 + 1013904223) // % 4294967296
		BREAK
		CASE 1 // RAND_LIM
			iPseudoRandomParam = (iPseudoRandomParam * 125) % 2796203
		BREAK
		CASE 2 //RAND_GERHARD
			iPseudoRandomParam = (iPseudoRandomParam * 32719 + 3) % 32749
		BREAK	
		DEFAULT
			iPseudoRandomParam = (iPseudoRandomParam * 1664525 + 1013904223) // % 4294967296
		BREAK
	ENDSWITCH 
	
	RETURN iPseudoRandomParam
ENDFUNC

/// PURPOSE:
///    Sets the seed for the random number
/// PARAMS:
///    seed - 
PROC SET_PSUEDO_RANDOM_SEED(INT seed)
	iPseudoRandomParam = seed
ENDPROC

/// PURPOSE:
///    Geneate a psuedo random int between a and b
/// PARAMS:
///    a - small value
///    b - big value
/// RETURNS:
///    random int
FUNC INT GENERATE_RANDOM_INT_BETWEEN(INT a, INT b)
	INT diff = b - a
	
	IF (a = b)
		RETURN a 
	ENDIF
	
	IF (b > a)
		RETURN (DO_PSUEDO_RANDOM() % diff) + a 
	ENDIF 
	
	// this if a is bigger than b
	RETURN (DO_PSUEDO_RANDOM() % ABSI(diff)) + b 
ENDFUNC


//----------------------
//	MISC FUNCTIONS
//----------------------

/// PURPOSE:
///    If ped exists it is deleted
/// PARAMS:
///    mPed - ped to delete
PROC SAFE_DELETE_PED(ped_index &mPed)
	IF DOES_ENTITY_EXIST(mPed)
		IF NOT IS_ENTITY_DEAD(mPed)
			SET_ENTITY_LOAD_COLLISION_FLAG(mPed, FALSE)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(mPed)
			SET_ENTITY_AS_MISSION_ENTITY(mPed)
		ENDIF

		DELETE_PED(mPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    If the object exists it is deleted
/// PARAMS:
///    mObject - the object to delete
PROC SAFE_DELETE_OBJECT(OBJECT_INDEX &mObject)
	IF DOES_ENTITY_EXIST(mObject)
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(mObject)
			DETACH_ENTITY(mObject)
		ENDIF
		DELETE_OBJECT(mObject)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the used ped model list
PROC RESET_CROWD_PED_MODELS_USED_LIST()
	iModelsLeftCount = MAX_PEDMODELS
	iModelsUsedBitField	= 0	
ENDPROC

/// PURPOSE:
///    Gets a random crowd ped model from list
///    Uses my psuedo random seed so we can seed it
/// RETURNS:
///    
FUNC MODEL_NAMES GET_RANDOM_CROWD_PED_MODEL()
	INT iCounter
	INT i
	
	//CPRINTLN(DEBUG_MISSION, "TRYING TO PICK A RANDOM PED MODEL")
	
	// reset the model list if all are used
	IF (iModelsLeftCount <= 0)
		RESET_CROWD_PED_MODELS_USED_LIST()
	ENDIF
	
	//CPRINTLN(DEBUG_MISSION, "   MODELS LEFT:", iModelsLeftCount)
	
	// if we only have one left just check through the list and grab the first one whose bit isn't set
	IF (iModelsLeftCount = 1)
		REPEAT COUNT_OF(crowdPedModel) i
			IF NOT IS_BIT_SET(iModelsUsedBitField, i)	
				SET_BIT(iModelsUsedBitField, i)
				iModelsLeftCount = 0
				RETURN crowdPedModel[i]
			ENDIF
		ENDREPEAT
	ENDIF
	
	// else loop through list
	iCounter = GENERATE_RANDOM_INT_BETWEEN(0, iModelsLeftCount)
	REPEAT COUNT_OF(crowdPedModel) i
		//CPRINTLN(DEBUG_MISSION, "   COUNTER:", iCounter)
		IF (iCounter <= 0)
			IF NOT IS_BIT_SET(iModelsUsedBitField, i) 
				SET_BIT(iModelsUsedBitField, i)
				//CPRINTLN(DEBUG_MISSION, "   MODEL USED:", i)
				iModelsLeftCount --
				RETURN crowdPedModel[i]
			ENDIF
		ELSE
			iCounter --
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("SOMETHING HAS GONE WRONG HERE")
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE: 
///  	Set a random variation for a ped body part
/// PARAMS
///    	ind - ped index
///     pc - ped component enum
PROC SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(PED_INDEX ind, PED_COMPONENT pc)
	INT iDrawable
	INT iTexture
	
	iDrawable = GENERATE_RANDOM_INT_BETWEEN(0, GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(ind, pc))
	iDrawable = CLAMP_INT(iDrawable, 0, GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(ind, pc) - 1)
	iTexture = GENERATE_RANDOM_INT_BETWEEN(0, GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(ind, pc, iDrawable))
	iTexture = CLAMP_INT(iTexture, 0, GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(ind, pc, iDrawable) - 1)
	
	SET_PED_COMPONENT_VARIATION(ind, pc, iDrawable, iTexture)
ENDPROC

/// PURPOSE:
///    Generates a Random Ped Variation Based On A Random Number Sees
/// PARAMS:
///    ped - 
///    iSeed - Use 
PROC SET_PSUEDO_RANDOM_CROWD_PED_VARIATION(PED_INDEX &ped, INT iSeed = 0)
	IF (iSeed > 0)
		SET_PSUEDO_RANDOM_SEED(iSeed)
	ENDIF
	
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD)
   	SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_TORSO)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_LEG)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_FEET)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_SPECIAL2)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_DECL)
    SET_PSUEDO_RANDOM_PED_COMPONENT_VARIATION(ped, PED_COMP_JBIB)
ENDPROC

//----------------------
//	ANIM DATA FUNCTIONS
//----------------------

/// PURPOSE:
///    Given of the 13 rows get the name of the anim dictionary
/// PARAMS:
///    animDictNumber - number from 0 to 12
/// RETURNS:
///    dictionary name
FUNC STRING GET_CROWD_ANIM_DICT_NAME(INT animDictNumber)
	STRING result

	SWITCH animDictNumber
		CASE 0
			result = "misslester1b_crowd@a_"
		BREAK
		CASE 1
			result = "misslester1b_crowd@b_"
		BREAK
		CASE 2
			result = "misslester1b_crowd@c_"
		BREAK
		CASE 3
			result = "misslester1b_crowd@d_"
		BREAK
		CASE 4
			result = "misslester1b_crowd@e_"
		BREAK
		CASE 5
			result = "misslester1b_crowd@f_"
		BREAK
		CASE 6
			result = "misslester1b_crowd@g_"
		BREAK
		CASE 7
			result = "misslester1b_crowd@h_"
		BREAK
		CASE 8
			result = "misslester1b_crowd@i_"
		BREAK
		CASE 9
			result = "misslester1b_crowd@j_"
		BREAK
		CASE 10
			result = "misslester1b_crowd@k_"
		BREAK
		CASE 11
			result = "misslester1b_crowd@l_"
		BREAK
		CASE 12
			result = "misslester1b_crowd@m_"
		BREAK
	ENDSWITCH
	
	RETURN result
ENDFUNC

/// PURPOSE:
///    Return an anim name from a dictionary number
/// PARAMS:
///    animDictNumber - index from 0 to 13
///    pedNumber - number from 0 to 5 (6 people in anim?)
/// RETURNS:
///    
FUNC TEXT_LABEL_15 GET_CROWD_ANIM_NAME_FROM_DICT(INT animDictNumber, INT pedNumber)
	TEXT_LABEL_15 result
	
	IF animDictNumber <= 4
		result = "001077_"
	ELIF animDictNumber <= 5 
		result = "001079_"
	ELIF animDictNumber <= 10
		result = "001080_"
	ELIF animDictNumber <= 11
		result = "001081_"
	ELIF animDictNumber <= 12
		result = "001082_"
	ENDIF
	
	SWITCH animDictNumber
		CASE 0	result += "01_"	BREAK
		CASE 1	result += "02_"	BREAK
		CASE 2	result += "03_"	BREAK
		CASE 3	result += "04_"	BREAK
		CASE 4	result += "05_"	BREAK
		
		CASE 5	result += "01_"	BREAK
		
		CASE 6	result += "01_"	BREAK
		CASE 7	result += "02_"	BREAK
		CASE 8	result += "03_"	BREAK
		CASE 9	result += "04_"	BREAK
		CASE 10	result += "05_"	BREAK
		
		CASE 11	result += "01_"	BREAK
		
		CASE 12	result += "01_"	BREAK
		
		CASE 13	result += "01_"	BREAK
	ENDSWITCH
	
	// make ped number wrap so we can have rows of more than 6 people
	pedNumber = pedNumber % 6
	
	// anim dict 3 seems to entirely female?
	IF animDictNumber = 3
		SWITCH pedNumber
			CASE 0	result += "F_a"	BREAK
			CASE 1	result += "F_b"	BREAK
			CASE 2	result += "F_c"	BREAK
			CASE 3	result += "F_d"	BREAK
			CASE 4	result += "F_e"	BREAK
			CASE 5	result += "F_f"	BREAK
		ENDSWITCH
	ELSE
		SWITCH pedNumber
			CASE 0	result += "F_a"	BREAK
			CASE 1	result += "M_a"	BREAK
			CASE 2	result += "M_b"	BREAK
			CASE 3	result += "M_c"	BREAK
			CASE 4	result += "M_d"	BREAK
			CASE 5	result += "M_e"	BREAK
		ENDSWITCH
	ENDIF
	
	RETURN result
ENDFUNC

//----------------------
//	ROW DATA FUNCTIONS
//----------------------

/// PURPOSE:
///    Fills a Row Data Struct with info 
/// PARAMS:
///    seat - data struct reference
///    pos - center point of the row
///    rot - rotation of row
PROC SET_SPARE_SEAT_DATA(SPARE_SEAT &seat, VECTOR pos, VECTOR rot, INT animdict = 0, FLOAT seatoff = 0.0,FLOAT sseatoff = 0.0 )
	seat.vCoords = pos 
	seat.vRot = rot 
	seat.iAnimDict = animdict
	seat.bInUse = TRUE
	seat.fSeatOffset = seatoff
	seat.fSecondarySeatOffset = sseatoff
ENDPROC

/// PURPOSE:
///    Fills a HIGH_FID_ROW_DATA Struct with info 
/// PARAMS:
///    seed - number used to generate the randomness
///    row - row data struct reference
///    pos - center point of the row
///    rot - rotation of row
///    seats - number of seats in the row
PROC SET_HIGH_FID_ROW_DATA(HIGH_FID_ROW_DATA &row, INT seed, VECTOR pos, VECTOR rot, INT seats, INT animdict = 0, FLOAT seatoff = 0.0)
	row.vCoords = pos 
	row.vRot = rot 
	row.iNumSeats = seats
	row.iSeed = seed
	row.iAnimDict = animdict
	row.fSeatOffset = seatoff
	
	/* if the number of seats is less than 6 we use anim set 12 as they are for seperate seats
	IF (row.iNumSeats < 6) 
		row.iAnimDict = 12
	ENDIF
	*/
ENDPROC

/// PURPOSE:
///    Fills a LOW_FID_ROW_DATA struct with info 
/// PARAMS:
///    seed - number used to generate the randomness
///    row - row data struct reference
///    pos - center point of the row
///    rot - rotation of row
///    seats - number of seats in the row
PROC SET_LOW_FID_ROW_DATA(LOW_FID_ROW_DATA &row, VECTOR pos, VECTOR rot, MODEL_NAMES mod, INT i)
	row.vCoords[i] = pos 
	row.vRot = rot 
	row.mod = mod
	/* if the number of seats is less than 6 we use anim set 12 as they are for seperate seats
	IF (row.iNumSeats < 6) 
		row.iAnimDict = 12
	ENDIF
	*/
ENDPROC

/// PURPOSE:
///    Deletes peds in row
/// PARAMS:
///    row - row data reference
PROC CLEANUP_ROW(HIGH_FID_ROW_DATA &row)
	INT i 
	
	REPEAT COUNT_OF(row.pedID) i 
		SAFE_DELETE_PED(row.pedID[i]) 
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  CPRINTLN(DEBUG_MISSION, _debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD

			CPRINTLN(DEBUG_MISSION, "MODEL LOADED", _modname)
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn - If true we unload the model after spawning the ped
///    bFreeze - if true we freeze the ped after spawning
///    bTempEvents - if true we block temp events
///    bTargetable - if false we set the ped to not be targetted
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir,   BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bTempEvents = TRUE, BOOL bTargetable = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			IF IS_ENTITY_OK(pedindex)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "PED CREATED !") #ENDIF
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, bTempEvents)
				ENDIF
				
				IF NOT bTargetable
					SET_PED_CAN_BE_TARGETTED(pedindex, bTargetable)
				ENDIF
				

				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Spawns an object. And activates its physics
/// PARAMS:
///    objectindex - The OBJECT_INDEX to write the newly create object to 
///    model - The model to load and use to create the object
///    pos - The position the object should be created
///    dir - The heading the object should have when created
/// RETURNS:
///    TRUE if the object was created
FUNC BOOL SPAWN_OBJECT(OBJECT_INDEX &objectindex, MODEL_NAMES model, VECTOR pos, FLOAT dir = 0.0, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(objectindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			objectindex = CREATE_OBJECT_NO_OFFSET(model, pos)
			
			IF DOES_ENTITY_EXIST(objectindex)
				SET_ENTITY_HEADING(objectindex, dir)
				//ACTIVATE_PHYSICS(objectindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Creates in row
/// PARAMS:
///    row - row data reference
PROC CREATE_ROW(HIGH_FID_ROW_DATA &row)
	INT i 
	FLOAT xd, x
	VECTOR v, off
	STRING str = GET_CROWD_ANIM_DICT_NAME(row.iAnimDict)
	MODEL_NAMES mdl //, lastmdl
	
	TEXT_LABEL_15 animname 
	ANIMATION_FLAGS animflags
	OBJECT_INDEX dummy 
	
	// delete all peds
	CLEANUP_ROW(row) 
	
	IF (bExtractOffset)
		animflags = AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS | AF_LOOPING
	ELSE
		animflags = AF_OVERRIDE_PHYSICS | AF_LOOPING
	ENDIF
	
	// seed the randomness
	SET_PSUEDO_RANDOM_SEED(row.iSeed)
	
	// can object we can use to base positions off
	dummy = CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BALL, row.vCoords)
	IF IS_ENTITY_OK(dummy)
		SET_ENTITY_COLLISION(dummy, FALSE)
		SET_ENTITY_ROTATION(dummy, row.vRot)
		FREEZE_ENTITY_POSITION(dummy, TRUE)
	ENDIF
	
	// work out offset
	off = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(dummy, <<row.fSeatOffset * SEAT_SPACING, fSeatFwdShift, 0.0>>)
	
	// create entities
	i = 0
	
	// work out end pos for seat - 
	IF (row.iNumSeats % 2 = 0)
		xd = (TO_FLOAT(row.iNumSeats) / 2.0) - 1.0
	ELSE 
	 	xd = (TO_FLOAT(row.iNumSeats) / 2.0) - 0.5
	ENDIF
	
	// do seat differential for rows of less than 6
	// we need to shift the start point so we can use the 6 seat anim for less than 6 peoplw
	x = (-SEAT_SPACING * xd)
	IF (row.iNumSeats <> 6)
		x -= (TO_FLOAT(6 - row.iNumSeats) / 2.0)
	ENDIF
	
	REPEAT row.iNumSeats i
	
		/* i don't like this one bit but this should make that 2 people sitting next to each other don't look the same
		WHILE (mdl = lastmdl)
			mdl = GET_RANDOM_CROWD_PED_MODEL()
		ENDWHILE
		*/
		
		mdl = GET_RANDOM_CROWD_PED_MODEL()
		IF (mdl = DUMMY_MODEL_FOR_SCRIPT)
			SCRIPT_ASSERT("RANDOM CROWD PED MODEL SELECT FAILED")
			SAFE_DELETE_OBJECT(dummy)
			EXIT 
		ENDIF
		
		// reset the model counter 
		IF (iModelsLeftCount = 0)
			RESET_CROWD_PED_MODELS_USED_LIST()
		ENDIF
		
		v = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(dummy, <<x + (TO_FLOAT(i) * SEAT_SPACING), 0, 0>>)
		
		WHILE NOT SPAWN_PED(row.pedID[i], mdl, v, row.vRot.z)
			WAIT(0)
		ENDWHILE
		
		IF IS_ENTITY_OK(row.pedID[i])
			SET_PSUEDO_RANDOM_CROWD_PED_VARIATION(row.pedID[i])
			animname = GET_CROWD_ANIM_NAME_FROM_DICT(row.iAnimDict, i)
			
			IF (bInstaBlend)
				TASK_PLAY_ANIM_ADVANCED(row.pedID[i], str, animname, off, row.vRot + <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, animflags, 0)
			ELSE
				TASK_PLAY_ANIM_ADVANCED(row.pedID[i], str, animname, off, row.vRot + <<0.0, 0.0, 180.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, animflags, 0)
			ENDIF
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(row.pedID[i])
		ENDIF
		
		//lastmdl = mdl
	ENDREPEAT
	
	// destroy the dummy object
	SAFE_DELETE_OBJECT(dummy)
ENDPROC

/// PURPOSE:
///    Creates in row
/// PARAMS:
///    row - row data reference
PROC CREATE_SPARE_SEAT(SPARE_SEAT &row)
	VECTOR off
	STRING str = GET_CROWD_ANIM_DICT_NAME(row.iAnimDict)
	MODEL_NAMES mdl //, lastmdl
	
	TEXT_LABEL_15 animname 
	ANIMATION_FLAGS animflags
	OBJECT_INDEX dummy 
	
	// delete all peds
	SAFE_DELETE_PED(row.pedID)
	
	IF (bExtractOffset)
		animflags = AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS | AF_LOOPING
	ELSE
		animflags = AF_OVERRIDE_PHYSICS | AF_LOOPING
	ENDIF
	
	// can object we can use to base positions off
	dummy = CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BALL, row.vCoords)
	IF IS_ENTITY_OK(dummy)
		SET_ENTITY_COLLISION(dummy, FALSE)
		SET_ENTITY_ROTATION(dummy, row.vRot)
		FREEZE_ENTITY_POSITION(dummy, TRUE)
	ENDIF
	
	// work out offset
	off = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(dummy, <<row.fSeatOffset * SEAT_SPACING, fSeatFwdShift, 0.0>>)

	mdl = GET_RANDOM_CROWD_PED_MODEL()
	IF (mdl = DUMMY_MODEL_FOR_SCRIPT)
		SCRIPT_ASSERT("RANDOM CROWD PED MODEL SELECT FAILED")
		SAFE_DELETE_OBJECT(dummy)
		EXIT 
	ENDIF
	
	// reset the model counter 
	IF (iModelsLeftCount = 0)
		RESET_CROWD_PED_MODELS_USED_LIST()
	ENDIF
	
	WHILE NOT SPAWN_PED(row.pedID, mdl, row.vCoords, row.vRot.z)
		WAIT(0)
	ENDWHILE

	IF IS_ENTITY_OK(row.pedID)
		SET_PSUEDO_RANDOM_CROWD_PED_VARIATION(row.pedID)
		animname = GET_CROWD_ANIM_NAME_FROM_DICT(row.iAnimDict, 0)
		
		IF (bInstaBlend)
			TASK_PLAY_ANIM_ADVANCED(row.pedID, str, animname, off, row.vRot + <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, animflags, 0)
		ELSE
			TASK_PLAY_ANIM_ADVANCED(row.pedID, str, animname, off, row.vRot + <<0.0, 0.0, 180.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, animflags, 0)
		ENDIF
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(row.pedID)
	ENDIF
	
	// destroy the dummy object
	SAFE_DELETE_OBJECT(dummy)
ENDPROC

FUNC STRING GET_LOW_POLY_ANIM_NAME(MODEL_NAMES mod)
	STRING name
	IF mod = DUMMY_MODEL_FOR_SCRIPT
	ENDIF
//	IF mod = P_PED_ROW_14_S
		name = "001077_01_14"
//	ELIF mod = P_PED_ROW_24_S
//		name = "001077_01_24"
//	ENDIF

	RETURN name
ENDFUNC

FUNC STRING GET_LOW_POLY_ANIM_BANK(MODEL_NAMES mod)
	STRING bank
	IF mod = DUMMY_MODEL_FOR_SCRIPT
	ENDIF
//	IF mod = P_PED_ROW_14_S
		bank = "misslester1b_crowdlow@14@"
//	ELIF mod = P_PED_ROW_24_S
//		bank = "misslester1b_crowdlow@24@"
//	ENDIF

	RETURN bank
ENDFUNC

PROC CREATE_LOW_POLY_BLOCK(LOW_FID_BLOCK_DATA &data)
	INT i
	FOR i=0 TO (data.iLowRowsUsed-1)
		WHILE NOT SPAWN_OBJECT(data.LowBlock.objectID[i] , data.LowBlock.mod, data.LowBlock.vCoords[i], data.LowBlock.vRot.z)
			CPRINTLN(DEBUG_MISSION, "Trying to create low poly mod array number = ", i)
			WAIT(0)
		ENDWHILE

		WHILE NOT DOES_ENTITY_HAVE_DRAWABLE(data.LowBlock.objectID[i])
			CPRINTLN(DEBUG_MISSION, "Waiting for array number object = ", i, " To have a drawable")
			WAIT(0)
		ENDWHILE

		IF IS_ENTITY_OK(data.LowBlock.objectID[i])
			STRING animBank, animname
			animBank = GET_LOW_POLY_ANIM_BANK(data.LowBlock.mod)
			animname = GET_LOW_POLY_ANIM_NAME(data.LowBlock.mod)
			CPRINTLN(DEBUG_MISSION, "Setting animations for array number = ", i)
			CPRINTLN(DEBUG_MISSION, animBank,  animname)
			PLAY_ENTITY_ANIM(data.LowBlock.objectID[i], animname, animBank, INSTANT_BLEND_IN, TRUE, FALSE, FALSE, 0, ENUM_TO_INT(AF_OVERRIDE_PHYSICS))
		ENDIF
	ENDFOR
ENDPROC

//----------------------
//	BLOCK FUNCTIONS
//----------------------

/// PURPOSE:
///    Resets the number of blocks used
/// PARAMS:
///    block - 
PROC RESET_BLOCK(HIGH_FID_BLOCK_DATA &block)
	block.iHighRowsUsed = 0
	block.spareSeat[0].bInUse = FALSE
	block.spareSeat[1].bInUse = FALSE
ENDPROC

/// PURPOSE:
///    Fills a Row Data Struct with info 
/// PARAMS:
///    block - row data struct reference
///    seed - number used to generate the randomness
///    pos - center point of the row
///    rot - rotation of row
///    seats - number of seats in the row
PROC ADD_ROW_TO_BLOCK(HIGH_FID_BLOCK_DATA &block, INT seed, VECTOR pos, VECTOR rot, INT seats, INT animdict = 0, FLOAT seatoff = 0.0)
	IF (block.iHighRowsUsed >= COUNT_OF(block.rowComplete))
		SCRIPT_ASSERT("TOO MANY ROWS IN THIS BLOCK")
		EXIT
	ENDIF 
	
	SET_HIGH_FID_ROW_DATA(block.rowComplete[block.iHighRowsUsed], seed, pos, rot, seats, animdict, seatoff)
	block.iHighRowsUsed ++
ENDPROC

/// PURPOSE:
///    Fills a Row Data Struct with info 
/// PARAMS:
///    block - row data struct reference
///    pos - center point of the row
///    rot - rotation of row
PROC ADD_ROW_TO_BLOCK_LOW(LOW_FID_BLOCK_DATA &block, VECTOR pos, VECTOR rot, MODEL_NAMES mod)
	
	SET_LOW_FID_ROW_DATA(block.lowBlock, pos, rot, mod, block.iLowRowsUsed)
	block.iLowRowsUsed ++
ENDPROC


/// PURPOSE:
///    Cleans up an entire block
/// PARAMS:
///    block - block reference
PROC CLEANUP_BLOCK_HIGH(HIGH_FID_BLOCK_DATA &block)
	INT i 
	
	REPEAT COUNT_OF(block.rowComplete) i 
		CLEANUP_ROW(block.rowComplete[i])
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(block.spareSeat) i 
		SAFE_DELETE_PED(block.spareSeat[i].pedID)
	ENDREPEAT

ENDPROC 

/// PURPOSE:
///    Cleans up an entire block
/// PARAMS:
///    block - block reference
PROC CLEANUP_BLOCK_LOW(LOW_FID_BLOCK_DATA &block)
	INT i 
	STRING animBank, animname
	animBank = GET_LOW_POLY_ANIM_BANK(block.LowBlock.mod)
	animname = GET_LOW_POLY_ANIM_NAME(block.LowBlock.mod)
	
	REPEAT COUNT_OF(block.LowBlock.objectID) i 
		IF DOES_ENTITY_EXIST(block.LowBlock.objectID[i])
		AND NOT IS_ENTITY_DEAD(block.LowBlock.objectID[i])
			STOP_ENTITY_ANIM(block.LowBlock.objectID[i], animname, animBank, INSTANT_BLEND_OUT)
		ENDIF
		SAFE_DELETE_OBJECT(block.LowBlock.objectID[i]) 
	ENDREPEAT
ENDPROC 
/// PURPOSE:
///    Creates up an entire block
/// PARAMS:
///    block - block reference
PROC CREATE_BLOCK(HIGH_FID_BLOCK_DATA &blk)
	INT i
	CLEANUP_BLOCK_HIGH(blk)
	RESET_CROWD_PED_MODELS_USED_LIST()
	
	REPEAT blk.iHighRowsUsed i 
		CREATE_ROW(blk.rowComplete[i])
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(blk.spareSeat) i 
		IF (blk.spareSeat[i].bInUse)
			CREATE_SPARE_SEAT(blk.spareSeat[i])
		ENDIF
	ENDREPEAT
ENDPROC 

PROC FILL_FRONT_BLOCKS_DATA(HIGH_FID_BLOCK_DATA &blk, INT blockNumber)
	VECTOR vHead 
	
	RESET_BLOCK(blk)
	CLEAR_BIT(iOldBlockTurnedOnHIGH, blockNumber) 
	
	// if the seat offset has been set, DON'T change the animation dictionary
	// or you will have to go through and reset everything up again
	
	SWITCH (INT_TO_ENUM(FRONT_BLOCKS, blockNumber))
		CASE BLOCK_FRONT_1_A // setup block data A
			vHead = <<0.0, 0.0, RAD_TO_DEG(-4.29) + 180.0>>
			
			ADD_ROW_TO_BLOCK(blk, 82047830, << 667.319, 572.133, 128.521 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 83248680, << 666.312, 571.675, 128.521 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 71526810, << 665.235, 571.183, 128.521 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 49826530, << 664.124, 570.679, 128.521 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 78575440, << 663.029, 570.181, 128.521 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 12133230, << 661.937, 569.685, 128.521 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 89665750, << 660.839, 569.194, 128.521 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 65990810, << 659.537, 569.392, 128.521 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 21233450, << 658.187, 569.572, 128.523 >>, vHead, 4, 0, 1.0)		// don't change the animdict or the offset
			SET_SPARE_SEAT_DATA(blk.spareSeat[0], <<659.867, 571.539, 128.531>>, vHead, 0, 0.5, -1.500)
			SET_SPARE_SEAT_DATA(blk.spareSeat[1], <<660.646, 567.108, 128.521>>, vHead, 0, 0.5, -1.500)
		BREAK
		CASE BLOCK_FRONT_1_B // setup block data B
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.98) + 180.0>>
			ADD_ROW_TO_BLOCK(blk, 21091970, << 670.312, 568.023, 128.521 >>, vHead, 5, 0, 0.5)	// don't change the animdict or the offset
			ADD_ROW_TO_BLOCK(blk, 97482490, << 669.189, 567.502, 128.522 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 66764240, << 668.280, 566.688, 128.522 >>, vHead, 6, 0)
			ADD_ROW_TO_BLOCK(blk, 82832850, << 667.384, 565.885, 128.522 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 33636100, << 666.491, 565.085, 128.522 >>, vHead, 6, 0)
			ADD_ROW_TO_BLOCK(blk, 22769640, << 665.595, 564.282, 128.522 >>, vHead, 6, 1)
//			ADD_ROW_TO_BLOCK(blk, 68914610, << 664.057, 564.357, 128.523 >>, vHead, 3, 11, -1.5)	// don't change the animdict or the offset
		BREAK 
		CASE BLOCK_FRONT_1_C // setup block data C
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.78) + 180.0>>	
			ADD_ROW_TO_BLOCK(blk, 91480760, << 673.700, 564.799, 128.521 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 64996320, << 672.996, 563.847, 128.521 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 30855220, << 672.271, 562.865, 128.521 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 98921990, << 671.557, 561.897, 128.521 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 73357200, << 670.258, 561.366, 128.522 >>, vHead, 6, 1)
//			ADD_ROW_TO_BLOCK(blk, 43783220, << 668.665, 561.031, 128.524 >>, vHead, 3, 11, -1.375)
			
			SET_SPARE_SEAT_DATA(blk.spareSeat[0], <<669.564, 563.469, 128.521>>, vHead, 2, -1.500, -1.500)
			SET_SPARE_SEAT_DATA(blk.spareSeat[1], <<672.340, 559.914, 128.522>>, vHead, 2, -1.500, -1.500)
		BREAK
		CASE BLOCK_FRONT_1_E // setup block data E
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.38) + 180.0>>
			ADD_ROW_TO_BLOCK(blk, 89689390, << 681.517, 555.801, 128.522 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 71057000, << 681.796, 556.967, 128.522 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 26206390, << 682.076, 558.137, 128.522 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 58207860, << 682.360, 559.342, 128.522 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 43622440, << 682.635, 560.475, 128.522 >>, vHead, 6, 1)
		BREAK	
		CASE BLOCK_FRONT_1_F // setup block data F
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.17) + 180.0>>
		
			ADD_ROW_TO_BLOCK(blk, 78555440, << 687.841, 559.785, 128.522 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 12324420, << 687.807, 558.602, 128.522 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 04083210, << 687.771, 557.382, 128.522 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 02021980, << 688.465, 556.159, 128.522 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 21091970, << 688.431, 554.961, 128.521 >>, vHead, 6, 1)
//			ADD_ROW_TO_BLOCK(blk, 65443320, << 689.447, 553.714, 128.519 >>, vHead, 3, 2, -1.5)			// don't change the animdict or the offset		
		
			SET_SPARE_SEAT_DATA(blk.spareSeat[0], <<685.931, 556.306, 128.522>>, vHead, 0, 0.5, -1.500)
			SET_SPARE_SEAT_DATA(blk.spareSeat[1], <<685.897, 555.108, 128.521>>, vHead, 1, -1.5,  -1.500)
		BREAK	
		CASE BLOCK_FRONT_1_G // setup block data G
			vHead = <<0.0, 0.0, RAD_TO_DEG(-2.97) + 180.0>>
			ADD_ROW_TO_BLOCK(blk, 31491440, << 692.632, 559.992, 128.522 >>, vHead, 5, 2, -0.5)		// don't change the animdict or the offset
			ADD_ROW_TO_BLOCK(blk, 34393340, << 693.189, 558.886, 128.522 >>, vHead, 6, 0)
			ADD_ROW_TO_BLOCK(blk, 94762900, << 693.396, 557.684, 128.522 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 95283980, << 693.600, 556.498, 128.522 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 92416940, << 693.803, 555.317, 128.522 >>, vHead, 6, 0)
			ADD_ROW_TO_BLOCK(blk, 53597080, << 694.007, 554.131, 128.522 >>, vHead, 6, 1)
//			ADD_ROW_TO_BLOCK(blk, 31957490, << 695.259, 553.235, 128.520 >>, vHead, 3, 0, 1.5)		// don't change the animdict or the offset
		BREAK	
		CASE BLOCK_FRONT_1_H // setup block data H
			vHead = <<0.0, 0.0, RAD_TO_DEG(-2.70) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 86170110, << 697.492, 561.188, 128.522 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 62247860, << 697.964, 560.188, 128.522 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 43665230, << 698.467, 559.116, 128.522 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 32906070, << 698.988, 558.013, 128.522 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 70836820, << 699.502, 556.925, 128.522 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 67800350, << 700.014, 555.841, 128.522 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 41115560, << 701.184, 555.063, 128.521 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 98430790, << 701.005, 553.758, 128.522 >>, vHead, 6, 1)
//			ADD_ROW_TO_BLOCK(blk, 35613940, << 702.801, 553.330, 128.520 >>, vHead, 4, 0, 1.0)			// don't change the animdict or the offset
			
			SET_SPARE_SEAT_DATA(blk.spareSeat[0], <<698.861, 554.048, 128.521>>, vHead, 0, 0.5, -1.500)
			SET_SPARE_SEAT_DATA(blk.spareSeat[1], <<703.267, 554.910, 128.522>>, vHead, 0, 0.500, -1.500)
		BREAK	
		CASE BLOCK_FRONT_2_A // setup block data I -A
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.99) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 13168530, << 652.833, 570.052, 128.529 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 92127780, << 652.006, 569.319, 128.528 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 51664260, << 651.136, 568.538, 128.729 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 35914010, << 650.242, 567.746, 128.729 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 43076060, << 649.661, 566.940, 128.929 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 21755390, << 648.426, 566.137, 128.929 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_2_B // setup block data L -B
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.90) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 20501110, << 656.955, 565.868, 128.530 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 10114780, << 656.193, 565.067, 128.530 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 12664730, << 655.358, 564.190, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 32956570, << 654.534, 563.324, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 11862860, << 653.696, 562.443, 128.930 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 87809290, << 652.862, 561.566, 128.930 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_2_C // setup block data F -C
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.76) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 75170110, << 662.947, 560.965, 128.529 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 62246860, << 662.306, 560.065, 128.529 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 83665250, << 661.603, 559.079, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 22016170, << 660.910, 558.106, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 41846840, << 660.204, 557.116, 128.929 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 10000350, << 659.502, 556.130, 128.929 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_2_D // setup block data F -D
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.68) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 86878180, << 667.872, 557.785, 128.530 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 22242820, << 667.310, 556.833, 128.530 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 93969290, << 666.693, 555.791, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 12101010, << 666.086, 554.762, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 20232220, << 665.467, 553.715, 128.930 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 57707370, << 664.852, 552.673, 128.930 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_2_E // setup block data F -E
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.53) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 13130110, << 674.882, 554.326, 128.529 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 97845810, << 674.460, 553.305, 128.529 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 12345670, << 673.997, 552.186, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 89101110, << 673.541, 551.081, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 12131410, << 673.076, 549.958, 128.929 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 51617180, << 672.613, 548.839, 128.929 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_2_F // setup block data X -F
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.45) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 60616260, << 680.396, 552.335, 128.530 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 36465660, << 680.063, 551.281, 128.530 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 67686970, << 679.697, 550.127, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 71727370, << 679.336, 548.988, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 47576770, << 678.969, 547.829, 128.930 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 78798080, << 678.603, 546.674, 128.930 >>, vHead, 6, 2)
		BREAK
		CASE BLOCK_FRONT_2_G // setup block data X -G
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.31) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 11511610, << 688.034, 550.483, 128.529 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 17118110, << 687.853, 549.393, 128.529 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 91201210, << 687.653, 548.199, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 12212310, << 687.457, 547.020, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 23124120, << 687.257, 545.821, 128.929 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 51261270, << 687.058, 544.627, 128.929 >>, vHead, 6, 2)
		BREAK		
		CASE BLOCK_FRONT_2_H // setup block data X -H
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.22) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 15515610, << 693.855, 549.783, 128.530 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 57158150, << 693.767, 548.682, 128.530 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 91601610, << 693.670, 547.474, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 16216310, << 693.575, 546.283, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 64165160, << 693.478, 545.071, 128.930 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 61671680, << 693.381, 543.865, 128.930 >>, vHead, 6, 2)
		BREAK		
		CASE BLOCK_FRONT_2_I // setup block data X -I
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.08) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 19920020, << 701.652, 549.708, 128.529 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 01202200, << 701.721, 548.605, 128.529 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 32042050, << 701.795, 547.396, 128.729 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 20620720, << 701.869, 546.203, 128.729 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 08209210, << 701.944, 544.990, 128.929 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 02112120, << 702.019, 543.781, 128.929 >>, vHead, 6, 2)
		BREAK		
		CASE BLOCK_FRONT_2_J // setup block data X -J
			vHead = <<0.0, 0.0, RAD_TO_DEG(-2.99) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 24024120, << 707.482, 550.335, 128.530 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 43244240, << 707.644, 549.242, 128.530 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 42452460, << 707.821, 548.044, 128.730 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 24724820, << 707.996, 546.862, 128.730 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 48249250, << 708.174, 545.659, 128.929 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 02512520, << 708.351, 544.461, 128.929 >>, vHead, 6, 2)
		BREAK		
		CASE BLOCK_FRONT_3_A // setup block data J -A
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.98) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 46779100, << 646.434, 564.522, 128.927 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 93643160, << 645.612, 563.784, 128.927 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 34729570, << 644.710, 562.975, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 28556490, << 643.821, 562.177, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 75335160, << 642.916, 561.365, 129.327 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 84872300, << 642.015, 560.557, 129.327 >>, vHead, 6, 2)
			
		BREAK	
		CASE BLOCK_FRONT_3_B // setup block data F  -B
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 65187100, << 651.092, 559.707, 128.926 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 87514460, << 650.325, 558.912, 128.926 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 46165810, << 649.483, 558.040, 128.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 12560720, << 648.654, 557.180, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 27854880, << 647.809, 556.306, 129.326 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 97601360, << 646.968, 555.435, 129.326 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_3_C // setup block data F -C
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 55178130, << 657.925, 554.108, 128.927 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 75148880, << 657.290, 553.204, 128.927 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 88865540, << 656.593, 552.213, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 64916770, << 655.906, 551.235, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 70536220, << 655.207, 550.241, 129.327 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 17101310, << 654.512, 549.250, 129.327 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_3_D // setup block data F -D 
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.68) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 76870120, << 663.545, 550.463, 128.926 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 32147850, << 662.977, 549.515, 128.926 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 97662220, << 662.353, 548.477, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 12107070, << 661.738, 547.453, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 60841880, << 661.111, 546.411, 129.326 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 27803350, << 660.489, 545.373, 129.326 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_3_E // setup block data F -E
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.53) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 19202120, << 671.531, 546.515, 128.927 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 22232420, << 671.116, 545.491, 128.927 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 52627280, << 670.660, 544.369, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 29303130, << 670.211, 543.262, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 23334350, << 669.753, 542.135, 129.327 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 36373830, << 669.298, 541.014, 129.327 >>, vHead, 6, 2)
		BREAK	
		CASE BLOCK_FRONT_3_F // setup block data X -F
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 18283840, << 677.828, 544.227, 128.926 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 85858680, << 677.487, 543.176, 128.926 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 78889900, << 677.112, 542.024, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 91929390, << 676.744, 540.888, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 49596970, << 676.368, 539.731, 129.326 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 98991000, << 675.994, 538.580, 129.326 >>, vHead, 6, 2)
		BREAK
		CASE BLOCK_FRONT_3_G // setup block data X -G
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 12812910, << 686.526, 542.118, 128.927 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 30131130, << 686.352, 541.027, 128.927 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 21331340, << 686.160, 539.831, 129.127 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 13513610, << 685.972, 538.652, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 37138130, << 685.779, 537.451, 129.327 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 91401410, << 685.588, 536.256, 129.327 >>, vHead, 6, 2)
		BREAK
		CASE BLOCK_FRONT_3_H // setup block data X -H
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.23) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 16917010, << 693.176, 541.306, 128.926 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 71172170, << 693.080, 540.205, 128.926 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 31741750, << 692.975, 538.998, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 17617810, << 692.871, 537.808, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 79180180, << 692.765, 536.597, 129.326 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 21831840, << 692.660, 535.391, 129.326 >>, vHead, 6, 2)
		BREAK
		CASE BLOCK_FRONT_3_I // setup block data X -I
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.07) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 21321420, << 702.065, 541.218, 128.927 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 15216210, << 702.140, 540.116, 128.927 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 72182190, << 702.223, 538.907, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 22022120, << 702.304, 537.715, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 22223220, << 702.387, 536.502, 129.327 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 42252260, << 703.116, 526.841, 129.618 >>, vHead, 6, 2)
		BREAK
		CASE BLOCK_FRONT_3_J // setup block data X -J
			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.00) + 180.0>>		
			ADD_ROW_TO_BLOCK(blk, 25325420, << 708.727, 541.922, 128.926 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 55256250, << 709.881, 539.828, 128.926 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 72582590, << 709.050, 539.628, 129.127 >>, vHead, 6, 2)
			ADD_ROW_TO_BLOCK(blk, 26026120, << 709.217, 538.445, 129.127 >>, vHead, 6)
			ADD_ROW_TO_BLOCK(blk, 62263260, << 709.386, 537.241, 129.326 >>, vHead, 6, 1)
			ADD_ROW_TO_BLOCK(blk, 42652650, << 709.555, 536.043, 129.326 >>, vHead, 6, 2)
		BREAK
		DEFAULT 
			SCRIPT_ASSERT("BLOCK HASN'T BEEN SET UP")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Fill a block using data from a block bumber
/// PARAMS:
///    blk - 
///    blockNumber - 
PROC FILL_MID_BLOCK_DATA(LOW_FID_BLOCK_DATA &blk, INT blockNumber)
//	VECTOR vHead 
	
	blk.iLowRowsUsed = 0
	CLEAR_BIT(iOldBlockTurnedOnLOW_MID, blockNumber) 
	IF blockNumber = ENUM_TO_INT(BLOCK_MID_1_A)
	AND blk.iLowRowsUsed = 0

	ENDIF

	// if the seat offset has been set, DON'T change the animation dictionary
	// or you will have to go through and reset everything up again

//	SWITCH (INT_TO_ENUM(MID_BLOCKS, blockNumber))
//		CASE BLOCK_MID_1_A // setup block data K -A
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.98) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 640.177, 558.796, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 637.553, 556.453, 129.414 >>, vHead, P_PED_ROW_14_S)
//		BREAK	
//		CASE BLOCK_MID_1_B // setup block data F -B
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 645.273, 553.596, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 642.838, 551.057, 129.413 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_1_C // setup block data F -C
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.76) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 653.137, 547.147, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 651.107, 544.274, 129.414 >>, vHead, P_PED_ROW_14_S)
//		BREAK	
//		CASE BLOCK_MID_1_D // setup block data F -D//
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.68) + 180.0>>
//
////			ADD_ROW_TO_BLOCK_LOW(blk, << 659.251, 543.198, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 657.450, 540.176, 129.413 >>, vHead, P_PED_ROW_14_S)
//		BREAK	
//		CASE BLOCK_MID_1_E // setup block data F -E
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.53) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 668.431, 538.649, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 667.100, 535.393, 129.414 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_1_F // setup block data X -F
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.45) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 675.277, 536.174, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 674.202, 532.825, 129.413 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_1_G // setup block data X -G
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 685.273, 533.745, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 684.709, 530.273, 129.414 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_1_H // setup block data X -H
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.23) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 692.500, 535.870, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 692.41498, 532.50818, 129.413 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_1_I // setup block data X -I
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.08) + 180.0>>
//
////			ADD_ROW_TO_BLOCK_LOW(blk, << 692.04401, 528.15192, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 692.27429, 531.43774, 129.414 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_1_J // setup block data X -J			
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.00) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 709.965, 533.542, 129.213 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 710.466, 530.060, 129.413 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//
//
//
//		CASE BLOCK_MID_2_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.98) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 629.22467, 549.71985, 130.84526 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 627.18097, 547.60828, 130.23686 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 636.59503, 542.53168, 130.86882 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 634.20117, 540.26135, 130.76871 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.76) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 644.93951, 535.83771, 130.88277 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 642.86884, 533.27838, 130.84540 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_D // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.68) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 652.69763, 530.70532, 130.84526 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 651.05157, 528.32056, 130.84528 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_E // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.53) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 663.35748, 525.66193, 130.84526 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 662.13672, 522.53821, 130.84540 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_F // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.45) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 671.30042, 522.68994, 130.84526 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 670.33948, 519.76477, 130.84540 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_G // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 683.49915, 519.82428, 130.84526 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 682.90887, 516.65698, 130.84540 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_H // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.23) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 691.14789, 518.88428, 130.84532 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 690.85291, 515.52454, 130.84598 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_I // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.08) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 703.60364, 518.79163, 130.84532 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 703.79498, 515.56531, 130.84598 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_2_J // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.00) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 712.12134, 519.63745, 130.84532 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 712.67535, 516.43152, 130.84546 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		
//		
//		
//		CASE BLOCK_MID_3_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.98) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 624.67615, 545.19342, 132.44513 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 622.27588, 543.10852, 132.44527 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 627.98254, 535.65479, 132.44514 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 629.94501, 537.87738, 132.44527 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.76) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 641.21808, 530.32111, 132.44514 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 639.52216, 527.64795, 132.44527 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_D // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.68) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 649.11749, 525.21375, 132.44514 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 647.42413, 522.55511, 132.44527 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_E // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.53) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 660.74646, 519.60590, 132.44514 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 659.52936, 516.71283, 132.44594 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_F // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.45) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 669.51019, 516.38519, 132.44591 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 668.50110, 513.36810, 132.44586 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_G // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 681.35992, 513.37756, 132.44623 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 680.96857, 510.20142, 132.44589 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_H // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.23) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 691.00439, 512.27448, 132.44562 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 690.69818, 509.09955, 132.44576 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_I // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.08) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 703.52747, 512.14130, 132.44562 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 703.76501, 509.06702, 132.44576 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		CASE BLOCK_MID_3_J // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.00) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 713.26208, 513.16144, 132.44560 >>, vHead, P_PED_ROW_14_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 713.65216, 510.14731, 132.44560 >>, vHead, P_PED_ROW_14_S)
//		BREAK
//		DEFAULT 
//			SCRIPT_ASSERT("BLOCK HASN'T BEEN SET UP")
//		BREAK
//	ENDSWITCH
ENDPROC


PROC FILL_BACK_BLOCK_DATA(LOW_FID_BLOCK_DATA &blk, INT blockNumber)
//	VECTOR vHead 
	
	blk.iLowRowsUsed = 0
	CLEAR_BIT(iOldBlockTurnedOnLOW_BACK, blockNumber) 
	
	// if the seat offset has been set, DON'T change the animation dictionary
	// or you will have to go through and reset everything up again
	IF blockNumber = ENUM_TO_INT(BLOCK_BACK_1_A)
	AND blk.iLowRowsUsed = 0

	ENDIF
//	SWITCH (INT_TO_ENUM(BACK_BLOCKS, blockNumber))
//		CASE BLOCK_BACK_1_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 617.88843, 543.03833, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 621.68939, 538.72412, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 624.82129, 535.56287,  134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 628.34796, 532.15045, 134.04568 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		
//		CASE BLOCK_BACK_1_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 634.95380, 526.65662, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 639.36688, 523.29993, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 643.39252, 520.93439, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 647.55023, 518.44086, 134.04568 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_1_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 655.79510, 514.52087, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 660.33447, 512.68939, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 664.54376, 511.12320, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 669.28674, 509.51315, 134.04568 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_1_D // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 678.03735, 507.41071, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 682.81488, 506.56265, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 687.18555, 506.08905, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 693.08087, 505.49121, 134.04568 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_1_E // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.07) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 701.53937, 505.35986, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 706.60132, 505.67145, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 710.87262, 506.16077, 134.04568 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 716.62128, 506.94400, 134.04568 >>, vHead, P_PED_ROW_24_S)
//		BREAK		
//		///---
//		CASE BLOCK_BACK_2_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 607.74573, 534.92047, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 612.09637, 529.97168, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 615.33801, 526.68567, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 620.22614, 521.98572, 135.64577 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_2_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 627.05231, 516.46173, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 632.25043, 512.79279, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 636.16168, 510.26428, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 642.16302, 506.70111, 135.64577 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_2_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 649.84863, 502.94952, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 656.28796, 500.34787, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 659.89240, 499.02524, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 666.34998, 496.86691, 135.64577 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_2_D // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 674.64233, 494.93097, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 681.38715, 493.73386, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 686.12457, 493.13196, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 692.60748, 492.60025, 135.64577 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_2_E // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.07) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 701.40277, 492.49536, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 708.03363, 492.89368, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 712.66180, 493.43518, 135.64577 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 719.33356, 494.37646, 135.64577 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		///----
//		CASE BLOCK_BACK_3_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.91) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 602.99500, 530.29071, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 607.43982, 525.32080, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 610.49139, 522.28625, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 615.65771, 517.37671, 137.24602 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_3_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 623.39764, 511.06079, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 628.92480, 507.14633, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 632.49176, 504.80341, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 638.63818, 501.24542, 137.24602 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		CASE BLOCK_BACK_3_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 647.76324, 496.84085, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 654.30249, 494.20917, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 657.25763, 493.05569, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 663.01276, 491.19016, 137.24602 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_3_D// setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 673.81738, 488.41635, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 680.87793, 487.15265, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 685.06750, 486.61343, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 691.64508, 486.12634, 137.24602 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_3_E // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.07) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 701.81427, 486.02383, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 708.85944, 486.31464, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 713.21985, 486.75510, 137.24602 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 719.90625, 487.82483, 137.24602 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		//---
//		CASE BLOCK_BACK_4_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 612.04083, 495.35550, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 616.76190, 491.89243, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 623.66223, 487.39578, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 629.03882, 484.23956, 140.44571 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_4_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 640.23761, 478.80984, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 645.98907, 476.36411, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 652.24304, 474.21466, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 658.12671, 472.21130, 140.44571 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_4_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 670.33374, 469.37854, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 676.35645, 468.11185, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 684.66376, 467.22672, 140.44571 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 690.41016, 466.75015, 140.44571 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		//---
//		CASE BLOCK_BACK_5_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 608.08057, 490.03299, 142.04439>>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 612.88171, 486.42233, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 620.25232, 481.71960, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 625.98395, 478.39410, 142.04439 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_5_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 637.57587, 472.81152, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 643.68237, 470.30762, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 650.07227, 467.89539, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 656.25452, 465.78842, 142.04439 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_5_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 668.91846, 462.95212, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 675.41846, 461.72995, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 684.01086, 460.67538, 142.04439 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 690.23132, 460.18726, 142.04439 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//		
//		//---
//		CASE BLOCK_BACK_6_A // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.75) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 604.04633, 484.79034, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 609.25531, 481.06906, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 616.93921, 476.12708, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 622.96472, 472.64734, 143.64494 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_6_B // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.46) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 634.88483, 466.74240, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 641.24152, 464.19589, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 648.08252, 461.63467, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 654.59442, 459.55212, 143.64494 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		CASE BLOCK_BACK_6_C // setup block data X -J
//			vHead = <<0.0, 0.0, RAD_TO_DEG(-3.30) + 180.0>>
////			ADD_ROW_TO_BLOCK_LOW(blk, << 667.53400, 456.43060, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 674.29901, 455.21283,  143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 683.36871, 453.97702, 143.64494 >>, vHead, P_PED_ROW_24_S)
////			ADD_ROW_TO_BLOCK_LOW(blk, << 689.84546, 453.64835, 143.64494 >>, vHead, P_PED_ROW_24_S)
//		BREAK
//
//		DEFAULT 
//			SCRIPT_ASSERT("BLOCK HASN'T BEEN SET UP")
//		BREAK
//	ENDSWITCH
ENDPROC

PROC POPULATE_CUTSCENE_REACTION(INT i, INT tstart, INT end)
	mCutsceneReact.iStart[i] = tstart
	mCutsceneReact.iEnd[i] = end
ENDPROC

PROC POPULATE_CUTSCENE_REACTIONS(INT i)
	
	SWITCH INT_TO_ENUM(CUTSCENE_REACTIONS_SECTIONS, i)
		CASE WALKS_ON_STAGE
			POPULATE_CUTSCENE_REACTION(i, 0, 10000)
		BREAK

		CASE IDLE_POINT_1
			POPULATE_CUTSCENE_REACTION(i, 9000, 10000)
		BREAK

		CASE CHEER_POINT_1
			POPULATE_CUTSCENE_REACTION(i, 27000, 32000)
		BREAK

		CASE IDLE_POINT_2
			POPULATE_CUTSCENE_REACTION(i, 32000, 42000)
		BREAK
		
		CASE CHEER_POINT_2
			POPULATE_CUTSCENE_REACTION(i, 51000, 63000)
		BREAK

		CASE IDLE_POINT_3
			POPULATE_CUTSCENE_REACTION(i, 63600, 69000)
		BREAK

		CASE DISPLEASED_IFRUIT
			POPULATE_CUTSCENE_REACTION(i, 66000, 70200)
		BREAK

		CASE IDLE_POINT_4
			POPULATE_CUTSCENE_REACTION(i, 69600, 79200)
		BREAK

		CASE CHEER_POINT_3
			POPULATE_CUTSCENE_REACTION(i, 77400, 84000)
		BREAK

		CASE CHEER_POINT_5
			POPULATE_CUTSCENE_REACTION(i, 81000, 93600)
		BREAK
		
		CASE IDLE_POINT_5
			POPULATE_CUTSCENE_REACTION(i, -1, -1)
		BREAK

		CASE IDLE_POINT_6
			POPULATE_CUTSCENE_REACTION(i, 90000, 154200)
		BREAK
		
		CASE CHEER_POINT_4
			POPULATE_CUTSCENE_REACTION(i, 129600, 133200)
		BREAK


		CASE IDLE_POINT_7
			POPULATE_CUTSCENE_REACTION(i, 144600, 154200)
		BREAK

		CASE BOARD
			POPULATE_CUTSCENE_REACTION(i, 152700, 154200)
		BREAK

		CASE FACE_EXPLODE
			POPULATE_CUTSCENE_REACTION(i, 154200, 154200)
		BREAK
	ENDSWITCH

ENDPROC

//----------------------
//	ASSET FUNCTIONS
//----------------------
PROC LOAD_SS_ASSETS(BOOL bLInvadeIPL = TRUE)
	INT i 
	CPRINTLN(DEBUG_MISSION, "LOADING - PLEASE WAIT")
	
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << 689.1, 586.5, 130.5 >>)	
		CLEAR_AREA_OF_PEDS(<< 689.1, 586.5, 130.5 >>, 500.0)
		REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
	ENDIF
	
	ENABLE_ALL_DISPATCH_SERVICES(FALSE)
	BLOCK_SCENARIOS_AND_AMBIENT(siBlocking)
	NEW_LOAD_SCENE_START_SPHERE(<< 689.1, 586.5, 130.5 >>, 80.0)
	
	WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
		WAIT(0)
	ENDWHILE

	NEW_LOAD_SCENE_STOP()
	//cStreamVol = STREAMVOL_CREATE_SPHERE(<< 689.1, 586.5, 130.5 >>, 80.0, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)
	IF bLInvadeIPL
		REQUEST_IPL("LInvader")

		CPRINTLN(DEBUG_MISSION, "LOADING - WAITING FOR IPL ACTIVATION")
		WHILE NOT IS_IPL_ACTIVE("LInvader")
			WAIT(0)
		ENDWHILE
	ENDIF
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
	// load some hipsters
	crowdPedModel[0] = A_M_Y_HIPSTER_01
	crowdPedModel[1] = A_M_Y_HIPSTER_02
	crowdPedModel[2] = A_M_Y_HIPSTER_03
	
	crowdPedModel[3] = A_F_Y_HIPSTER_01
	crowdPedModel[4] = A_F_Y_HIPSTER_02
	crowdPedModel[5] = A_F_Y_HIPSTER_03
	crowdPedModel[6] = A_F_Y_HIPSTER_04
	
	crowdPedModel[7] = A_F_Y_BEVHILLS_01
	crowdPedModel[8] = A_M_M_BUSINESS_01
	crowdPedModel[9] = A_M_M_BEVHILLS_02
	crowdPedModel[10] = A_M_M_SKATER_01
	crowdPedModel[11] = A_M_Y_BEACHVESP_01
	crowdPedModel[12] = A_M_Y_BEVHILLS_01

	crowdPedModel[13] = A_F_M_BEVHILLS_01
	crowdPedModel[14] = A_M_M_KTOWN_01
	crowdPedModel[15] = A_M_Y_BUSICAS_01
	crowdPedModel[16] = A_M_Y_BUSINESS_02
	crowdPedModel[17] = A_M_Y_GAY_01
	crowdPedModel[18] = A_M_Y_KTOWN_02
	
	crowdPedModel[19] = A_M_M_BEVHILLS_02
	crowdPedModel[20] = A_M_Y_BUSINESS_03
	crowdPedModel[21] = A_M_Y_GAY_02
	
	crowdPedModel[22] = A_M_M_MALIBU_01
	crowdPedModel[22] = A_M_Y_GAY_02
	crowdPedModel[23] = A_M_Y_EASTSA_02
	crowdPedModel[24] = A_M_Y_SOUCENT_02
	
	crowdPedModel[25] = A_M_Y_VINEWOOD_01
	crowdPedModel[26] = A_M_Y_VINEWOOD_02
	crowdPedModel[27] = A_M_Y_VINEWOOD_03
	crowdPedModel[28] = A_M_Y_VINEWOOD_04
	
//	REPEAT COUNT_OF(crowdPedModel) i
//		SECURE_REQUEST_AND_LOAD_MODEL(crowdPedModel[i])
//	ENDREPEAT
	
	FOR i = 0 TO 12	
		SECURE_REQUEST_AND_LOAD_ANIM_DICT(GET_CROWD_ANIM_DICT_NAME(i))
	ENDFOR
	
	SECURE_REQUEST_AND_LOAD_ANIM_DICT("misslester1b_crowdlow@14@")
	SECURE_REQUEST_AND_LOAD_ANIM_DICT("misslester1b_crowdlow@24@")
	
	SECURE_REQUEST_AND_LOAD_SCALEFORM("BREAKING_NEWS", sFormBreakingNewsOverlay)
	SECURE_REQUEST_AND_LOAD_SCALEFORM("lifeinvader_presentation", sLifeInvaderOverlay)
	SECURE_REQUEST_AND_LOAD_ADDITIONAL_TEXT("LEST1", MISSION_TEXT_SLOT)

	// setup block data 
	FOR i=0 TO (ENUM_TO_INT(BLOCK_FRONT_3_J))
		FILL_FRONT_BLOCKS_DATA(crowdBlocksHigh[i], i)
	ENDFOR
	
	FOR i=0 TO (ENUM_TO_INT(BLOCK_MID_3_J))
		FILL_MID_BLOCK_DATA(crowdBlocksLowMid[i], i)
	ENDFOR
	
	FOR i=0 TO (ENUM_TO_INT(BLOCK_BACK_6_C))
		FILL_BACK_BLOCK_DATA(crowdBlocksLowBack[i], i)
	ENDFOR
	
	/*
	CPRINTLN(DEBUG_MISSION, "LOADING - WAITING FOR STREAM VOLUME TO LOAD")
    WHILE NOT STREAMVOL_HAS_LOADED(cStreamVol)
          WAIT(0)
    ENDWHILE
    STREAMVOL_DELETE(cStreamVol)
	*/
	
	// Setup render targets
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	REGISTER_NAMED_RENDERTARGET("Big_Disp")
	LINK_NAMED_RENDERTARGET(PROP_HUGE_DISPLAY_01)
	LINK_NAMED_RENDERTARGET(PROP_HUGE_DISPLAY_02)
	rndrTgtPresentation = GET_NAMED_RENDERTARGET_RENDER_ID("Big_Disp")
	rndrTgtDefault = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
	
	CPRINTLN(DEBUG_MISSION, "LOADING - WAITING FOR COLLISION TO LOAD")
	WHILE IS_ENTITY_WAITING_FOR_WORLD_COLLISION(PLAYER_PED_ID())
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_MISSION, "LOADING - WAITING FOR CUTSCENE")
	WHILE NOT HAS_CUTSCENE_LOADED()
		WAIT(0)
	ENDWHILE	
	
	CPRINTLN(DEBUG_MISSION, "LOADING - WAIT TWO SECONDS FOR SCENE TO STREAM IN")
	WAIT(2000)
	CPRINTLN(DEBUG_MISSION, "LOADING COMPLETE")
	iScriptStage = 0
	
	sTime.h = 12
	sTime.m = 0
	sTime.s = 0
	
	FOR i = 0 TO (ENUM_TO_INT(MAX_REACTIONS)-1)
		POPULATE_CUTSCENE_REACTIONS(i)
	ENDFOR
	
	// just to stop an assert
	IF (bShowVersionNumber)
	ENDIF
ENDPROC

PROC UNLOAD_SS_ASSETS()
	INT i 
	
	IF IS_CUTSCENE_ACTIVE()
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE_IMMEDIATELY()
		ENDIF
		REMOVE_CUTSCENE()
	ENDIF
	
	SAFE_DELETE_OBJECT(testSeatDummy)
	
	RELEASE_NAMED_RENDERTARGET("Big_Disp")
	REMOVE_IPL("LInvader")
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sLifeInvaderOverlay)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sFormBreakingNewsOverlay)
	CLEAR_WEATHER_TYPE_PERSIST()
	REPEAT COUNT_OF(crowdBlocksHigh) i
		CLEANUP_BLOCK_HIGH(crowdBlocksHigh[i])
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(crowdBlocksLowMid) i
		CLEANUP_BLOCK_LOW(crowdBlocksLowMid[i])
	ENDREPEAT 
	
	i = 0
	REPEAT COUNT_OF(crowdBlocksLowBack) i
		CLEANUP_BLOCK_LOW(crowdBlocksLowBack[i])
	ENDREPEAT

	// unload models
	i = 0
	REPEAT COUNT_OF(crowdPedModel) i
		SECURE_UNLOAD_MODEL(crowdPedModel[i])
	ENDREPEAT
	
	FOR i = 0 TO 12	
		SECURE_UNLOAD_ANIM_DICT(GET_CROWD_ANIM_DICT_NAME(i))
	ENDFOR
	
	UNBLOCK_SCENARIOS_AND_AMBIENT(siBlocking)
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Prints Block Data to TempDebug.txt
/// PARAMS:
///    blk - block reference
PROC DUMP_BLOCK_DATA_TO_TTY(HIGH_FID_BLOCK_DATA &blk, STRING blkname)
	INT i 
	
	OPEN_DEBUG_FILE()
		// header
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(blkname)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		// row data
		REPEAT blk.iHighRowsUsed i
			SAVE_STRING_TO_DEBUG_FILE("ADD_ROW_TO_BLOCK(blk, ")
			SAVE_INT_TO_DEBUG_FILE(blk.rowComplete[i].iSeed)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(blk.rowComplete[i].vCoords)
			SAVE_STRING_TO_DEBUG_FILE(", vHead, ")
			SAVE_INT_TO_DEBUG_FILE(blk.rowComplete[i].iAnimDict)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(blk.rowComplete[i].fSeatOffset)
			SAVE_STRING_TO_DEBUG_FILE(")")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		
		// seat data
		i = 0
		REPEAT COUNT_OF(blk.spareSeat) i
			IF (blk.spareSeat[i].bInUse)
				SAVE_STRING_TO_DEBUG_FILE("SET_SPARE_SEAT_DATA(blk.spareSeat[")
				SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("], ")
				SAVE_VECTOR_TO_DEBUG_FILE(blk.spareSeat[i].vCoords)
				SAVE_STRING_TO_DEBUG_FILE(", vHead, ")
				SAVE_INT_TO_DEBUG_FILE(blk.spareSeat[i].iAnimDict)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(blk.spareSeat[i].fSeatOffset)
				SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT

		SAVE_NEWLINE_TO_DEBUG_FILE()
	CLOSE_DEBUG_FILE()
ENDPROC

/// PURPOSE:
///    Sets up Block Widget we can turn of a block and control row seeds from here
/// PARAMS:
///    blk - block reference
///    nme - widget name
///    ind - index of block (for blockswitch array)
PROC SETUP_SS_SEAT_WIDGET(SPARE_SEAT &blk, STRING nme)
	START_WIDGET_GROUP(nme)	
		ADD_WIDGET_BOOL("In Use", blk.bInUse)	
		ADD_WIDGET_VECTOR_SLIDER("Seat Pos", blk.vCoords, -5000.0, 5000.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Seat Offset", blk.fSeatOffset, -10, 10, 0.5)
		ADD_WIDGET_INT_SLIDER("Anim Dict", blk.iAnimDict, 0, 12, 1) 
	STOP_WIDGET_GROUP()		
ENDPROC

/// PURPOSE:
///    Sets up Block Widget we can turn of a block and control row seeds from here
/// PARAMS:
///    blk - block reference
///    nme - widget name
///    ind - index of block (for blockswitch array)
PROC SETUP_SS_BLOCK_WIDGET(HIGH_FID_BLOCK_DATA &blk, STRING nme, INT ind)
	INT i
	TEXT_LABEL rownme
	
	IF (ind > 0)
	ENDIF
	
	START_WIDGET_GROUP(nme)	
		ADD_WIDGET_BOOL("Show Block", bBlockSwitchHigh[ind])	
		START_WIDGET_GROUP("Seating Plan")	
			REPEAT blk.iHighRowsUsed i
				rownme = "Row" 
				rownme += i
			 	START_WIDGET_GROUP(rownme)
					ADD_WIDGET_INT_SLIDER("Seed", blk.rowComplete[i].iSeed, 0, 100000000, 10)
					ADD_WIDGET_INT_SLIDER("Seats", blk.rowComplete[i].iNumSeats, 0, 7, 1) 
					ADD_WIDGET_FLOAT_SLIDER("Seat Offset", blk.rowComplete[i].fSeatOffset, -6, 6, 0.125)
					ADD_WIDGET_INT_SLIDER("Anim Dict", blk.rowComplete[i].iAnimDict, 0, 12, 1) 
				STOP_WIDGET_GROUP()
			ENDREPEAT
			
			SETUP_SS_SEAT_WIDGET(blk.spareSeat[0], "Spare Seat 0")
			SETUP_SS_SEAT_WIDGET(blk.spareSeat[1], "Spare Seat 1")
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC



/// PURPOSE:
///    SETS UP ALL RAMPAGE DEBUG WIDGETS
PROC SETUP_SS_DEBUG_WIDGETS()
	m_widgetGroup = START_WIDGET_GROUP("LesterScene PedController")	
		ADD_WIDGET_BOOL("Show Overlay", bShowOverlay)
		ADD_WIDGET_BOOL("Output Block Data To TTY", bOutputTTY)
		ADD_WIDGET_BOOL("Show Seat Dummy", bUseSeatDummy)
		ADD_WIDGET_BOOL("Should blocks use Scripted reactions", bUseManualReactions)
		
		START_WIDGET_GROUP("Time Of Day")
			ADD_WIDGET_INT_SLIDER("Hours", sTime.h, 0, 23, 1)
			ADD_WIDGET_INT_SLIDER("Minutes", sTime.m, 0, 59, 1)
			ADD_WIDGET_INT_SLIDER("Seconds", sTime.s, 0, 59, 1)
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("Block Switching")
			START_WIDGET_GROUP("High Poly Front 3 Rows")
				START_WIDGET_GROUP("Front Row 1")
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[0], "Block A", ENUM_TO_INT(BLOCK_FRONT_1_A))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[1], "Block B", ENUM_TO_INT(BLOCK_FRONT_1_B))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[2], "Block C", ENUM_TO_INT(BLOCK_FRONT_1_C))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[3], "Block E", ENUM_TO_INT(BLOCK_FRONT_1_E))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[4], "Block F", ENUM_TO_INT(BLOCK_FRONT_1_F))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[5], "Block G", ENUM_TO_INT(BLOCK_FRONT_1_G))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[6], "Block H", ENUM_TO_INT(BLOCK_FRONT_1_H))
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Front Row 2")
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[7], "Block A", ENUM_TO_INT(BLOCK_FRONT_2_A))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[8], "Block B", ENUM_TO_INT(BLOCK_FRONT_2_B))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[9], "Block C", ENUM_TO_INT(BLOCK_FRONT_2_C))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[10], "Block D", ENUM_TO_INT(BLOCK_FRONT_2_D))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[11], "Block E", ENUM_TO_INT(BLOCK_FRONT_2_E))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[12], "Block F", ENUM_TO_INT(BLOCK_FRONT_2_F))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[13], "Block G", ENUM_TO_INT(BLOCK_FRONT_2_G))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[14], "Block H", ENUM_TO_INT(BLOCK_FRONT_2_H))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[15], "Block I", ENUM_TO_INT(BLOCK_FRONT_2_I))
					SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[16], "Block J", ENUM_TO_INT(BLOCK_FRONT_2_J))
				STOP_WIDGET_GROUP()
						
				START_WIDGET_GROUP("Front Row 3")
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[17], "Block A", ENUM_TO_INT(BLOCK_FRONT_3_A))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[18], "Block B", ENUM_TO_INT(BLOCK_FRONT_3_B))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[19], "Block C", ENUM_TO_INT(BLOCK_FRONT_3_C))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[20], "Block D", ENUM_TO_INT(BLOCK_FRONT_3_D))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[21], "Block E", ENUM_TO_INT(BLOCK_FRONT_3_E))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[22], "Block F", ENUM_TO_INT(BLOCK_FRONT_3_F))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[23], "Block G", ENUM_TO_INT(BLOCK_FRONT_3_G))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[24], "Block H", ENUM_TO_INT(BLOCK_FRONT_3_H))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[25], "Block I", ENUM_TO_INT(BLOCK_FRONT_3_I))
						SETUP_SS_BLOCK_WIDGET(crowdBlocksHigh[26], "Block J", ENUM_TO_INT(BLOCK_FRONT_3_J))
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Middle 3 Rows")
				START_WIDGET_GROUP("Middle Row 1")
						ADD_WIDGET_BOOL("Show Block Middle 1 A", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_A)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 B", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_B)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 C", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_C)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 D", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_D)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 E", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_E)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 F", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_F)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 G", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_G)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 H", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_H)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 I", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_I)])	
						ADD_WIDGET_BOOL("Show Block Middle 1 J", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_J)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Middle Row 2")
						ADD_WIDGET_BOOL("Show Block Middle 2 A", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_A)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 B", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_B)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 C", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_C)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 D", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_D)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 E", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_E)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 F", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_F)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 G", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_G)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 H", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_H)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 I", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_I)])	
						ADD_WIDGET_BOOL("Show Block Middle 2 J", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_2_J)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("Middle Row 3")
						ADD_WIDGET_BOOL("Show Block Middle 3 A", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_A)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 B", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_1_B)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 C", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_C)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 D", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_D)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 E", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_E)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 F", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_F)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 G", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_G)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 H", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_H)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 I", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_I)])	
						ADD_WIDGET_BOOL("Show Block Middle 3 J", bBlockSwitchLowMid[ENUM_TO_INT(BLOCK_MID_3_J)])	
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("BACK 3 Rows")
				START_WIDGET_GROUP("BACK Row 1")
						ADD_WIDGET_BOOL("Show Block BACK 1 A", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_1_A)])	
						ADD_WIDGET_BOOL("Show Block BACK 1 B", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_1_B)])	
						ADD_WIDGET_BOOL("Show Block BACK 1 C", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_1_C)])	
						ADD_WIDGET_BOOL("Show Block BACK 1 D", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_1_D)])	
						ADD_WIDGET_BOOL("Show Block BACK 1 E", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_1_E)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("BACK Row 2")
						ADD_WIDGET_BOOL("Show Block BACK 2 A", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_2_A)])	
						ADD_WIDGET_BOOL("Show Block BACK 2 B", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_2_B)])	
						ADD_WIDGET_BOOL("Show Block BACK 2 C", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_2_C)])	
						ADD_WIDGET_BOOL("Show Block BACK 2 D", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_2_D)])	
						ADD_WIDGET_BOOL("Show Block BACK 2 E", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_2_E)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("BACK Row 3")
						ADD_WIDGET_BOOL("Show Block BACK 3 A", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_3_A)])	
						ADD_WIDGET_BOOL("Show Block BACK 3 B", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_3_B)])	
						ADD_WIDGET_BOOL("Show Block BACK 3 C", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_3_C)])	
						ADD_WIDGET_BOOL("Show Block BACK 3 D", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_3_D)])	
						ADD_WIDGET_BOOL("Show Block BACK 3 E", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_3_E)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("BACK Row 4")
						ADD_WIDGET_BOOL("Show Block BACK 4 A", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_4_A)])	
						ADD_WIDGET_BOOL("Show Block BACK 4 B", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_4_B)])	
						ADD_WIDGET_BOOL("Show Block BACK 4 C", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_4_C)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("BACK Row 5")
						ADD_WIDGET_BOOL("Show Block BACK 5 A", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_5_A)])	
						ADD_WIDGET_BOOL("Show Block BACK 5 B", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_5_B)])	
						ADD_WIDGET_BOOL("Show Block BACK 5 C", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_5_C)])	
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("BACK Row 6")
						ADD_WIDGET_BOOL("Show Block BACK 6 A", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_6_A)])	
						ADD_WIDGET_BOOL("Show Block BACK 6 B", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_6_B)])	
						ADD_WIDGET_BOOL("Show Block BACK 6 C", bBlockSwitchLowBack[ENUM_TO_INT(BLOCK_BACK_6_C)])	
				STOP_WIDGET_GROUP()

			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()

		//ADD_WIDGET_BOOL("Extract Anim Offset", bExtractOffset)	
		//ADD_WIDGET_INT_SLIDER("Randomization Type", iRandomType, 0, 2, 1) // don't let people mess around with this
		
		START_WIDGET_GROUP("Debug Options")	
			ADD_WIDGET_BOOL("Show Version Number", bShowVersionNumber)
			ADD_WIDGET_INT_READ_ONLY("Models Used Bits", iModelsUsedBitField)
			ADD_WIDGET_INT_READ_ONLY("Models Left", iModelsLeftCount)
			ADD_WIDGET_FLOAT_SLIDER("Fwd Seat Shift", fSeatFwdShift, -2, 2, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("Seat Spacing", SEAT_SPACING, 0, 2, 0.005)
			ADD_WIDGET_BOOL("Instant Blend Anim", bInstaBlend)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spare Seat Dropper")
			ADD_WIDGET_INT_SLIDER("Block", iTestSeatBlock, 0, (ENUM_TO_INT(MAX_BLOCKS )- 1), 1)
			ADD_WIDGET_INT_SLIDER("Row", iTestSeatRow, 0, MAX_ROWS - 1, 1)
			ADD_WIDGET_FLOAT_SLIDER("Seat Offset", fTestSeatOffset, -10, 10, 0.125)
			ADD_WIDGET_VECTOR_SLIDER("Seat Pos", vTestSeatPos, -5000.0, 5000.0, 0.001)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE: 
/// 	DELETES ALL RAMPAGE DEBUG WIDGETS
PROC CLEANUP_SS_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_widgetGroup)
		DELETE_WIDGET_GROUP(m_widgetGroup)
	ENDIF
ENDPROC 

PROC CHECK_SPAWN_FRONT_BLOCKS()
	FRONT_BLOCKS eBlocks
	REPEAT MAX_FRONT_BLOCKS eBlocks	
		IF (IS_BIT_SET(iOldBlockTurnedOnHIGH, ENUM_TO_INT(eBlocks)) != bBlockSwitchHigh[ENUM_TO_INT(eBlocks)])
			CPRINTLN(DEBUG_MISSION, "Old HIGH block value different to current one", ENUM_TO_INT(eBlocks))
			
			IF (bBlockSwitchHigh[ENUM_TO_INT(eBlocks)] = FALSE)				
				CPRINTLN(DEBUG_MISSION, "Removing HIGH Block array number =", ENUM_TO_INT(eBlocks))
				CLEANUP_BLOCK_HIGH(crowdBlocksHigh[ENUM_TO_INT(eBlocks)])
//				iBlocksActive--
			ELSE
//				IF iBlocksActive < 2
					CPRINTLN(DEBUG_MISSION, "Creating HIGH Block array number = ", ENUM_TO_INT(eBlocks))
					CREATE_BLOCK(crowdBlocksHigh[ENUM_TO_INT(eBlocks)])
//					iBlocksActive++
//				ELSE
//					CPRINTLN(DEBUG_MISSION, "Active HIGH Blocks was over 10 resetting last flag = ", iBlocksActiveLow)
//					bBlockSwitchHigh[ENUM_TO_INT(eBlocks)] = FALSE
//				ENDIF
			ENDIF
			IF bBlockSwitchHigh[ENUM_TO_INT(eBlocks)]
				CPRINTLN(DEBUG_MISSION, "bBlockSwitchHigh set to TRUE updating old block status = ", ENUM_TO_INT(eBlocks))
				SET_BIT(iOldBlockTurnedOnHIGH, ENUM_TO_INT(eBlocks))
			ELSE
				CPRINTLN(DEBUG_MISSION, "bBlockSwitchHigh set to FALSE updating old block status = ", ENUM_TO_INT(eBlocks))
				CLEAR_BIT(iOldBlockTurnedOnHIGH, ENUM_TO_INT(eBlocks))
			ENDIF
		ENDIF
		
	ENDREPEAT 
ENDPROC

PROC CHECK_SPAWN_MID_BLOCKS()
	MID_BLOCKS eBlocks
	REPEAT MAX_MID_BLOCKS eBlocks	
		IF (IS_BIT_SET(iOldBlockTurnedOnLOW_MID, ENUM_TO_INT(eBlocks)) != bBlockSwitchLowMid[ENUM_TO_INT(eBlocks)])
			CPRINTLN(DEBUG_MISSION, "Old low block value different to current one", ENUM_TO_INT(eBlocks))
			
			IF (bBlockSwitchLowMid[ENUM_TO_INT(eBlocks)] = FALSE)		
				CPRINTLN(DEBUG_MISSION, "Removing low Block array number = ", ENUM_TO_INT(eBlocks))
				
				CLEANUP_BLOCK_LOW(crowdBlocksLowMid[ENUM_TO_INT(eBlocks)])
//				iBlocksActiveLow--
			ELSE
//				IF iBlocksActiveLow < 10
					CPRINTLN(DEBUG_MISSION, "Creating low Block array number = ", ENUM_TO_INT(eBlocks))
					CREATE_LOW_POLY_BLOCK(crowdBlocksLowMid[ENUM_TO_INT(eBlocks)])
//					iBlocksActiveLow++
//					CPRINTLN(DEBUG_MISSION, "Active low Blocks = ", iBlocksActiveLow)
//				ELSE
//					CPRINTLN(DEBUG_MISSION, "Active low Blocks was over 10 resetting last flag = ", iBlocksActiveLow)
//					bBlockSwitchLowMid[ENUM_TO_INT(eBlocks)] = FALSE
//				ENDIF
			ENDIF
			IF bBlockSwitchLowMid[ENUM_TO_INT(eBlocks)]
				CPRINTLN(DEBUG_MISSION, "bBlockSwitchLowMid set to true updating old block status = ", ENUM_TO_INT(eBlocks))
				SET_BIT(iOldBlockTurnedOnLOW_MID, ENUM_TO_INT(eBlocks))
			ELSE
				CPRINTLN(DEBUG_MISSION, "bBlockSwitchLowMid set to false updating old block status = ", ENUM_TO_INT(eBlocks))
				CLEAR_BIT(iOldBlockTurnedOnLOW_MID, ENUM_TO_INT(eBlocks))
			ENDIF
		ENDIF
	ENDREPEAT 
ENDPROC

PROC CHECK_SPAWN_BACK_BLOCKS()
	BACK_BLOCKS eBlocks
	REPEAT MAX_BACK_BLOCKS eBlocks	
		IF (IS_BIT_SET(iOldBlockTurnedOnLOW_BACK, ENUM_TO_INT(eBlocks)) != bBlockSwitchLowBack[ENUM_TO_INT(eBlocks)])
			CPRINTLN(DEBUG_MISSION, "Old low block value different to current one", ENUM_TO_INT(eBlocks))
			
			IF (bBlockSwitchLowBack[ENUM_TO_INT(eBlocks)] = FALSE)		
				CPRINTLN(DEBUG_MISSION, "Removing low Block array number = ", ENUM_TO_INT(eBlocks))
				
				CLEANUP_BLOCK_LOW(crowdBlocksLowBack[ENUM_TO_INT(eBlocks)])
//				iBlocksActiveLow--
			ELSE
//				IF iBlocksActiveLow < 10
					CPRINTLN(DEBUG_MISSION, "Creating low Block array number = ", ENUM_TO_INT(eBlocks))
					CREATE_LOW_POLY_BLOCK(crowdBlocksLowBack[ENUM_TO_INT(eBlocks)])
//					iBlocksActiveLow++
//					CPRINTLN(DEBUG_MISSION, "Active low Blocks = ", iBlocksActiveLow)
//				ELSE
//					CPRINTLN(DEBUG_MISSION, "Active low Blocks was over 10 resetting last flag = ", iBlocksActiveLow)
//					bBlockSwitchLowBack[ENUM_TO_INT(eBlocks)] = FALSE
//				ENDIF
			ENDIF
			IF bBlockSwitchLowBack[ENUM_TO_INT(eBlocks)]
				CPRINTLN(DEBUG_MISSION, "bBlockSwitchLowBack set to true updating old block status = ", ENUM_TO_INT(eBlocks))
				SET_BIT(iOldBlockTurnedOnLOW_BACK, ENUM_TO_INT(eBlocks))
			ELSE
				CPRINTLN(DEBUG_MISSION, "bBlockSwitchLowBack set to false updating old block status = ", ENUM_TO_INT(eBlocks))
				CLEAR_BIT(iOldBlockTurnedOnLOW_BACK, ENUM_TO_INT(eBlocks))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    UPDATES DEBUG WIDGETS
PROC UPDATE_SS_DEBUG_WIDGETS()
	VECTOR pos

	CHECK_SPAWN_FRONT_BLOCKS()
	CHECK_SPAWN_MID_BLOCKS()
	CHECK_SPAWN_BACK_BLOCKS()
	
	IF (bOutputTTY)
		CPRINTLN(DEBUG_MISSION, "DUMPING BLOCK INFO TO TTY")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[0], "BLOCK_FRONT_1_A")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[1], "BLOCK_FRONT_1_B")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[2], "BLOCK_FRONT_1_C")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[3], "BLOCK_FRONT_1_E")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[4], "BLOCK_FRONT_1_F")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[5], "BLOCK_FRONT_1_G")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[6], "BLOCK_FRONT_1_H")

			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[7], "BLOCK_FRONT_2_A")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[8], "BLOCK_FRONT_2_B")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[9], "BLOCK_FRONT_2_C")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[10], "BLOCK_FRONT_2_D")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[11], "BLOCK_FRONT_2_E")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[12], "BLOCK_FRONT_2_F")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[13], "BLOCK_FRONT_2_G")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[14], "BLOCK_FRONT_2_H")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[15], "BLOCK_FRONT_2_I")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[16], "BLOCK_FRONT_2_J")
			
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[17], "BLOCK_FRONT_3_A")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[18], "BLOCK_FRONT_3_B")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[19], "BLOCK_FRONT_3_C")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[20], "BLOCK_FRONT_3_D")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[21], "BLOCK_FRONT_3_E")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[22], "BLOCK_FRONT_3_F")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[23], "BLOCK_FRONT_3_G")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[24], "BLOCK_FRONT_3_H")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[25], "BLOCK_FRONT_3_I")
			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[26], "BLOCK_FRONT_3_J")
			
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[27], "BLOCK_MID_1_A")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[28], "BLOCK_MID_1_B")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[29], "BLOCK_MID_1_C")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[30], "BLOCK_MID_1_D")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[31], "BLOCK_MID_1_E")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[32], "BLOCK_MID_1_F")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[33], "BLOCK_MID_1_G")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[34], "BLOCK_MID_1_H")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[35], "BLOCK_MID_1_I")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[36], "BLOCK_MID_1_J")
//			
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[37], "BLOCK_MID_2_A")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[38], "BLOCK_MID_2_B")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[39], "BLOCK_MID_2_C")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[40], "BLOCK_MID_2_D")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[41], "BLOCK_MID_2_E")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[42], "BLOCK_MID_2_F")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[43], "BLOCK_MID_2_G")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[44], "BLOCK_MID_2_H")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[45], "BLOCK_MID_2_I")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[46], "BLOCK_MID_2_J")
//			
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[47], "BLOCK_MID_3_A")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[48], "BLOCK_MID_3_B")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[49], "BLOCK_MID_3_C")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[50], "BLOCK_MID_3_D")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[51], "BLOCK_MID_3_E")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[52], "BLOCK_MID_3_F")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[53], "BLOCK_MID_3_G")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[54], "BLOCK_MID_3_H")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[55], "BLOCK_MID_3_I")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[56], "BLOCK_MID_3_J")
//			
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[57], "BLOCK_BACK_1_A")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[58], "BLOCK_BACK_1_B")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[59], "BLOCK_BACK_1_C")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[60], "BLOCK_BACK_1_D")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[61], "BLOCK_BACK_1_E")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[62], "BLOCK_BACK_1_F")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[63], "BLOCK_BACK_1_G")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[64], "BLOCK_BACK_1_H")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[65], "BLOCK_BACK_1_I")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[66], "BLOCK_BACK_1_J")
//			
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[67], "BLOCK_BACK_2_A")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[68], "BLOCK_BACK_2_B")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[69], "BLOCK_BACK_2_C")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[70], "BLOCK_BACK_2_D")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[71], "BLOCK_BACK_2_E")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[72], "BLOCK_BACK_2_F")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[73], "BLOCK_BACK_2_G")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[74], "BLOCK_BACK_2_H")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[75], "BLOCK_BACK_2_I")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[76], "BLOCK_BACK_2_J")
//			
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[77], "BLOCK_BACK_3_A")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[78], "BLOCK_BACK_3_B")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[79], "BLOCK_BACK_3_C")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[80], "BLOCK_BACK_3_D")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[81], "BLOCK_BACK_3_E")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[82], "BLOCK_BACK_3_F")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[83], "BLOCK_BACK_3_G")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[84], "BLOCK_BACK_3_H")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[85], "BLOCK_BACK_3_I")
//			DUMP_BLOCK_DATA_TO_TTY(crowdBlocksHigh[86], "BLOCK_BACK_3_J")
		
		CPRINTLN(DEBUG_MISSION, "BLOCK DUMP COMPLETE")
		bOutputTTY = FALSE
	ENDIF
	
	IF (bUseSeatDummy = FALSE)
		IF DOES_ENTITY_EXIST(testSeatDummy)
			SAFE_DELETE_OBJECT(testSeatDummy)
		ENDIF
		
		EXIT
	ENDIF
	
	pos = crowdBlocksHigh[iTestSeatBlock].rowComplete[iTestSeatRow].vCoords
	IF NOT DOES_ENTITY_EXIST(testSeatDummy)
		testSeatDummy = CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BALL, pos)
		SET_ENTITY_COLLISION(testSeatDummy, FALSE)
		FREEZE_ENTITY_POSITION(testSeatDummy, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(testSeatDummy)
		SET_ENTITY_COORDS(testSeatDummy, pos)
		SET_ENTITY_ROTATION(testSeatDummy, crowdBlocksHigh[iTestSeatBlock].rowComplete[iTestSeatRow].vRot)
		vTestSeatPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(testSeatDummy, <<fTestSeatOffset * SEAT_SPACING, fSeatFwdShift, 0.0>>)
		
		DRAW_DEBUG_SPHERE(vTestSeatPos, 0.0625)
		DRAW_DEBUG_LINE(vTestSeatPos - <<0, 0, 3>>, vTestSeatPos + <<0, 0, 3>>)
	ENDIF
	
ENDPROC
#ENDIF

PROC SET_PED_ANIMS_FOR_START(INT i)
	INT r, p
	FOR r=0 TO (crowdBlocksHigh[i].iHighRowsUsed-1)
		FOR p=0 TO (crowdBlocksHigh[i].rowComplete[r].iNumSeats-1)
			TEXT_LABEL_15 animname = GET_CROWD_ANIM_NAME_FROM_DICT(crowdBlocksHigh[i].rowComplete[r].iAnimDict, p)
			IF DOES_ENTITY_EXIST(crowdBlocksHigh[i].rowComplete[r].pedID[p])
			AND NOT IS_PED_INJURED(crowdBlocksHigh[i].rowComplete[r].pedID[p])
				IF IS_ENTITY_PLAYING_ANIM(crowdBlocksHigh[i].rowComplete[r].pedID[p], GET_CROWD_ANIM_DICT_NAME(crowdBlocksHigh[i].rowComplete[r].iAnimDict),animname)
					SET_ANIM_PHASE(crowdBlocksHigh[i].rowComplete[r].pedID[p], 0.0)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(crowdBlocksHigh[i].rowComplete[r].pedID[p])
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

PROC SET_OBJECT_ANIMS_FOR_START(LOW_FID_BLOCK_DATA &data)
	INT o
	STRING animBank, animname
	animBank = GET_LOW_POLY_ANIM_BANK(data.LowBlock.mod)
	animname = GET_LOW_POLY_ANIM_NAME(data.LowBlock.mod)
	FOR o=0 TO (data.iLowRowsUsed-1)
		IF IS_ENTITY_OK(data.LowBlock.objectID[o])
			IF IS_ENTITY_PLAYING_ANIM(data.LowBlock.objectID[o], animBank,animname)
				SET_ENTITY_ANIM_CURRENT_TIME(data.LowBlock.objectID[o], animBank, animname, 0.0)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(data.LowBlock.objectID[o])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
PROC PAUSE_FOR_RAG_EDITING()
	#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("If you need to set up stuff in RAG do it now")
		
		WHILE NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
		
			DRAW_RECT(0.5, 0.15, 0.75, 0.1, 0, 0, 0, 192)
			SET_TEXT_SCALE(0.75, 0.5)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.1, "STRING", "SWITCH ON THE CROWD BLOCKS IN RAG AND THEN")
			
			SET_TEXT_SCALE(0.75, 0.5)
			SET_TEXT_CENTRE(TRUE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.15, "STRING", "PRESS Z ON KEYBOARD TO CONTINUE")
			
			UPDATE_SS_DEBUG_WIDGETS()
			SET_CLOCK_TIME(sTime.h, sTime.m, sTime.s)
			WAIT(0)
		ENDWHILE
		INT i
		REPEAT COUNT_OF(bBlockSwitchHigh) i 
			IF bBlockSwitchHigh[i]
				SET_PED_ANIMS_FOR_START(i)
			ENDIF
		ENDREPEAT

		i=0
		REPEAT COUNT_OF(bBlockSwitchLowMid) i 
			IF bBlockSwitchLowMid[i]
				SET_OBJECT_ANIMS_FOR_START(crowdBlocksLowMid[i])
			ENDIF
		ENDREPEAT

		i=0
		REPEAT COUNT_OF(bBlockSwitchLowBack) i 
			IF bBlockSwitchLowBack[i]
				SET_OBJECT_ANIMS_FOR_START(crowdBlocksLowBack[i])
			ENDIF
		ENDREPEAT
		WAIT(1000)	// wait quarter a second to give people time to slide in
	#ENDIF
ENDPROC

FUNC BOOL IS_SECTION_SITTING(CUTSCENE_REACTIONS_SECTIONS section)
	IF section = IDLE_POINT_1
	OR section = IDLE_POINT_2
	OR section = IDLE_POINT_3
	OR section = IDLE_POINT_4
	OR section = IDLE_POINT_5
	OR section = IDLE_POINT_6
	OR section = IDLE_POINT_7
		CPRINTLN(DEBUG_MISSION, "Anim section is seating and is =  ", section)
		RETURN TRUE
	ENDIF

	CPRINTLN(DEBUG_MISSION, "Anim section is not seating and is =  ", section)
	RETURN FALSE
ENDFUNC


FUNC FLOAT GET_PHASE_TYPE(CUTSCENE_REACTIONS_SECTIONS section, INT i)
	FLOAT phase
	
	IF section = WALKS_ON_STAGE 
		CPRINTLN(DEBUG_MISSION, "WALKS_ON_STAGE ", section)
		phase = 0.041

	ELIF section = IDLE_POINT_1
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_1 ", section)
		phase = 0.280
		
	ELIF section = CHEER_POINT_1
		CPRINTLN(DEBUG_MISSION, "CHEER_POINT_1 ", section)
		phase = 0.147

	ELIF section = IDLE_POINT_2
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_2 ", section)
		phase = 0.280

	ELIF section = CHEER_POINT_2
		CPRINTLN(DEBUG_MISSION, "CHEER_POINT_2 ", section)
		phase = 0.235

	ELIF section = IDLE_POINT_3
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_3 ", section)
		phase = 0.278

	ELIF section = DISPLEASED_IFRUIT //possible its at this point 
		CPRINTLN(DEBUG_MISSION, "DISPLEASED_IFRUIT ", section)
		phase = 0.350

	ELIF section = IDLE_POINT_4
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_4 ", section)
		phase = 0.411

	ELIF section = CHEER_POINT_3
		CPRINTLN(DEBUG_MISSION, "CHEER_POINT_3 ", section)
		phase = 0.438

	ELIF section = IDLE_POINT_5
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_5 ", section)
		phase = 0.280

	ELIF section = CHEER_POINT_4
		CPRINTLN(DEBUG_MISSION, "CHEER_POINT_4 ", section)
		phase = 0.179

	ELIF section = IDLE_POINT_6
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_6 ", section)
		phase = 0.280

	ELIF section = CHEER_POINT_5
		CPRINTLN(DEBUG_MISSION, "CHEER_POINT_5 ", section)
		phase = 0.634

	ELIF section = IDLE_POINT_7
		CPRINTLN(DEBUG_MISSION, "IDLE_POINT_7 ", section)
		phase = 0.280

	ELIF section = BOARD
		CPRINTLN(DEBUG_MISSION, "BOARD ", section)
		phase = 0.739

	ELIF section = FACE_EXPLODE
		CPRINTLN(DEBUG_MISSION, "FACE_EXPLODE ", section)
		phase = 0.894

	ENDIF

	IF IS_SECTION_SITTING(section)
		FLOAT storage = phase
		phase += (i/10)
		CPRINTLN(DEBUG_MISSION, "phase adjust  = ", phase)
		
		IF phase >= 1.0
			phase = storage
			CPRINTLN(DEBUG_MISSION, "phase adjust too high phase = storage = ", phase)
		ENDIF
	ENDIF

	RETURN phase
ENDFUNC


FUNC VECTOR GET_ANIM_OFFSET(HIGH_FID_ROW_DATA row)
	OBJECT_INDEX dummy 
	VECTOR off
	// can object we can use to base positions off
	dummy = CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BALL, row.vCoords)
	IF IS_ENTITY_OK(dummy)
		SET_ENTITY_COLLISION(dummy, FALSE)
		SET_ENTITY_ROTATION(dummy, row.vRot)
		FREEZE_ENTITY_POSITION(dummy, TRUE)
	ENDIF
	
	// work out offset
	off = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(dummy, <<row.fSeatOffset * SEAT_SPACING, fSeatFwdShift, 0.0>>)
	SAFE_DELETE_OBJECT(dummy)

	RETURN off
ENDFUNC

FUNC VECTOR GET_ANIM_OFFSET_SPARE(SPARE_SEAT seat, BOOL bUsesecondayoffset)
	OBJECT_INDEX dummy 
	VECTOR off
	// can object we can use to base positions off
	dummy = CREATE_OBJECT_NO_OFFSET(PROP_GOLF_BALL, seat.vCoords)
	IF IS_ENTITY_OK(dummy)
		SET_ENTITY_COLLISION(dummy, FALSE)
		SET_ENTITY_ROTATION(dummy, seat.vRot)
		FREEZE_ENTITY_POSITION(dummy, TRUE)
	ENDIF
	
	FLOAT f
	
	IF NOT bUsesecondayoffset
		f = seat.fSeatOffset
	ELSE
		f = seat.fSecondarySeatOffset
	ENDIF
	
	// work out offset
	off = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(dummy, <<f * SEAT_SPACING, fSeatFwdShift, 0.0>>)
	SAFE_DELETE_OBJECT(dummy)

	RETURN off
ENDFUNC

PROC SET_HIGH_BLOCK_REACTION(CUTSCENE_REACTIONS_SECTIONS section, HIGH_FID_BLOCK_DATA &blk)
	INT i, p
	BOOL bSwitch = FALSE
	STRING DictName
	TEXT_LABEL_15 AnimName
	VECTOR off
	INT iSittingDictID = 11
	FLOAT phaser
	ANIMATION_FLAGS animflags = AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS | AF_LOOPING
	IF IS_SECTION_SITTING(section)
		REPEAT blk.iHighRowsUsed i 
			IF NOT bSwitch
				iSittingDictID = 11
				bSwitch = TRUE
			ELSE
				iSittingDictID = 12
				bSwitch = FALSE
			ENDIF
			REPEAT blk.rowComplete[i].iNumSeats p
				IF IS_ENTITY_OK(blk.rowComplete[i].pedID[p])
					phaser = GET_PHASE_TYPE(section, p)
					DictName = GET_CROWD_ANIM_DICT_NAME(iSittingDictID)
					AnimName = GET_CROWD_ANIM_NAME_FROM_DICT(iSittingDictID, p)
					CPRINTLN(DEBUG_MISSION, "ID = ", iSittingDictID, "DICT NAME = ", DictName, "ANIM NAME = ", AnimName)
					off = GET_ANIM_OFFSET(blk.rowComplete[i])
					TASK_PLAY_ANIM_ADVANCED(blk.rowComplete[i].pedID[p], DictName, AnimName, off, blk.rowComplete[i].vRot + <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, animflags, phaser)
//					SET_ENTITY_ANIM_CURRENT_TIME(crowdBlocksHigh[eFBlocks].rowComplete[i].pedID[p], DictName, AnimName , phaser)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(blk.rowComplete[i].pedID[p])
				ENDIF
			ENDREPEAT
			
		ENDREPEAT
		
		p = 0
		REPEAT COUNT_OF(blk.spareSeat) p
			IF IS_ENTITY_OK(blk.spareSeat[p].pedID)
				phaser = GET_PHASE_TYPE(section, p)
				DictName = GET_CROWD_ANIM_DICT_NAME(iSittingDictID)
				AnimName = GET_CROWD_ANIM_NAME_FROM_DICT(iSittingDictID, p)
				CPRINTLN(DEBUG_MISSION, "ID = ", iSittingDictID, "DICT NAME = ", DictName, "ANIM NAME = ", AnimName)
				off = GET_ANIM_OFFSET_SPARE(blk.spareSeat[p], TRUE)
				TASK_PLAY_ANIM_ADVANCED(blk.spareSeat[p].pedID, DictName, AnimName, off, blk.spareSeat[p].vRot + <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, animflags, phaser)
//					SET_ENTITY_ANIM_CURRENT_TIME(crowdBlocksHigh[eFBlocks].rowComplete[i].pedID[p], DictName, AnimName , phaser)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(blk.spareSeat[p].pedID)
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT blk.iHighRowsUsed i 
			REPEAT blk.rowComplete[i].iNumSeats p
				IF IS_ENTITY_OK(blk.rowComplete[i].pedID[p])
					phaser = GET_PHASE_TYPE(section, p)
					DictName = GET_CROWD_ANIM_DICT_NAME(blk.rowComplete[i].iAnimDict)
					AnimName = GET_CROWD_ANIM_NAME_FROM_DICT(blk.rowComplete[i].iAnimDict, p)
					off = GET_ANIM_OFFSET(blk.rowComplete[i])
					TASK_PLAY_ANIM_ADVANCED(blk.rowComplete[i].pedID[p], DictName, AnimName, off, blk.rowComplete[i].vRot + <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, animflags, phaser)

					FORCE_PED_AI_AND_ANIMATION_UPDATE(blk.rowComplete[i].pedID[p])
				ENDIF
			ENDREPEAT
			
		ENDREPEAT
		
		p = 0
		REPEAT COUNT_OF(blk.spareSeat) p
			IF IS_ENTITY_OK(blk.spareSeat[p].pedID)
				phaser = GET_PHASE_TYPE(section, p)
				DictName = GET_CROWD_ANIM_DICT_NAME(blk.spareSeat[p].iAnimDict)
				AnimName = GET_CROWD_ANIM_NAME_FROM_DICT(blk.spareSeat[p].iAnimDict, p)
				off = GET_ANIM_OFFSET_SPARE(blk.spareSeat[p], FALSE)
				TASK_PLAY_ANIM_ADVANCED(blk.spareSeat[p].pedID, DictName, AnimName, off, blk.spareSeat[p].vRot + <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, animflags, phaser)

				FORCE_PED_AI_AND_ANIMATION_UPDATE(blk.spareSeat[p].pedID)
			ENDIF
			
		ENDREPEAT
	ENDIF
ENDPROC

PROC SET_MID_LOW_BLOCK_REACTION(CUTSCENE_REACTIONS_SECTIONS section, MID_BLOCKS eMBlocks)
	INT i
	FLOAT phaser = GET_PHASE_TYPE(section, i)
	REPEAT crowdBlocksLowMid[eMBlocks].iLowRowsUsed i 
		IF IS_ENTITY_OK(crowdBlocksLowMid[eMBlocks].LowBlock.objectID[i])
			SET_ENTITY_ANIM_CURRENT_TIME(crowdBlocksLowMid[eMBlocks].LowBlock.objectID[i], GET_LOW_POLY_ANIM_BANK(crowdBlocksLowMid[eMBlocks].LowBlock.mod), GET_LOW_POLY_ANIM_NAME(crowdBlocksLowMid[eMBlocks].LowBlock.mod), phaser)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(crowdBlocksLowMid[eMBlocks].LowBlock.objectID[i])
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SET_BACK_LOW_BLOCK_REACTION(CUTSCENE_REACTIONS_SECTIONS section, BACK_BLOCKS eBBlocks)
	INT i
	FLOAT phaser = GET_PHASE_TYPE(section, i)
	REPEAT crowdBlocksLowBack[eBBlocks].iLowRowsUsed i 
		IF IS_ENTITY_OK(crowdBlocksLowBack[eBBlocks].LowBlock.objectID[i])
			SET_ENTITY_ANIM_CURRENT_TIME(crowdBlocksLowBack[eBBlocks].LowBlock.objectID[i], GET_LOW_POLY_ANIM_BANK(crowdBlocksLowBack[eBBlocks].LowBlock.mod), GET_LOW_POLY_ANIM_NAME(crowdBlocksLowBack[eBBlocks].LowBlock.mod), phaser)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(crowdBlocksLowBack[eBBlocks].LowBlock.objectID[i])
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC SET_ACTIVE_BLOCKS_TO_REACT( #IF IS_DEBUG_BUILD CUTSCENE_REACTIONS_SECTIONS section #ENDIF )
	#IF IS_DEBUG_BUILD 
		FRONT_BLOCKS eFBlocks
		REPEAT MAX_FRONT_BLOCKS eFBlocks	
			IF bBlockSwitchHigh[ENUM_TO_INT(eFBlocks)]
				SET_HIGH_BLOCK_REACTION(section, crowdBlocksHigh[eFBlocks])
			ENDIF
		ENDREPEAT

		MID_BLOCKS eMBlocks
		REPEAT MAX_MID_BLOCKS eMBlocks	
			IF bBlockSwitchLowMid[ENUM_TO_INT(eMBlocks)]
				SET_MID_LOW_BLOCK_REACTION(section, eMBlocks)
			ENDIF
		ENDREPEAT

		BACK_BLOCKS eBBlocks
		REPEAT MAX_BACK_BLOCKS eBBlocks	
			IF bBlockSwitchLowBack[ENUM_TO_INT(eBBlocks)]
				SET_BACK_LOW_BLOCK_REACTION(section, eBBlocks)
			ENDIF
		ENDREPEAT
	#ENDIF
ENDPROC

PROC MONITOR_CUTSCENE_REACTIONS()
	INT iTime = GET_CUTSCENE_TIME()
	INT i
	
	FOR i = 0 TO (ENUM_TO_INT(MAX_REACTIONS)-1)
		IF mCutsceneReact.iStart[i] != -1
			IF NOT IS_BIT_SET(iReactionBeingPerformed, i)
			AND iTime >= mCutsceneReact.iStart[i]
			AND iTime < mCutsceneReact.iEnd[i]
				SET_ACTIVE_BLOCKS_TO_REACT(#IF IS_DEBUG_BUILD INT_TO_ENUM(CUTSCENE_REACTIONS_SECTIONS, i) #ENDIF )
				SET_BIT(iReactionBeingPerformed, i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL UPDATE_LF_CUTSCENE_DONE()
	SET_CLOCK_TIME(sTime.h, sTime.m, sTime.s)
	SET_WIND(-1)
	SWITCH (iScriptStage)
		CASE 0
			DO_SCREEN_FADE_IN(0)
			PAUSE_FOR_RAG_EDITING()
			DO_SCREEN_FADE_IN(500)
			START_CUTSCENE()
			iScriptStage ++ 
		BREAK
		CASE 1
			IF NOT HAS_CUTSCENE_FINISHED()
				IF bUseManualReactions
					MONITOR_CUTSCENE_REACTIONS()
				ENDIF
				SET_TEXT_RENDER_ID (rndrTgtPresentation)
				DRAW_SCALEFORM_MOVIE(sLifeInvaderOverlay, 0.200000, 0.340000, 0.410001,0.690000, 100,100,100,255)
				
				SET_TEXT_RENDER_ID(rndrTgtDefault)
				IF bShowOverlay
					BEGIN_SCALEFORM_MOVIE_METHOD(sFormBreakingNewsOverlay, "SET_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("KEYNOTE_NAME")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("KEYNOTE_TITLE")
					END_SCALEFORM_MOVIE_METHOD()
					DRAW_SCALEFORM_MOVIE(sFormBreakingNewsOverlay, 0.5, 0.5, 1.0, 1.0, 100,100,100,255)	
				ENDIF
			ELSE 
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH		
	
	RETURN FALSE
ENDFUNC


