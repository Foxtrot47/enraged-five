//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	vehicle_gen_controller.sc									//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Script version of the vehicle generation system. Data for   //
//							each vehicle is stored in the private header. Globals have  //
//							been setup to flag which vehicles are available and require //
//							extra processing such as displaying help or sending txts.   //
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "commands_extrametadata.sch"
USING "script_player.sch"
USING "vehicle_gen_private.sch"
USING "vehicle_gen_public.sch"
USING "flow_help_public.sch"
USING "flow_public_core_override.sch"
USING "context_control_public.sch"
USING "commands_interiors.sch"
USING "website_public.sch"
USING "building_control_private.sch"
USING "random_events_public.sch"
USING "menu_public.sch"
USING "cellphone_public.sch"
USING "ambient_common.sch"
USING "beast_secret_peyote.sch"
#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "properties_public.sch"
#endif
#endif

USING "vehgen_garage_scene_public.sch"


CONST_INT MAX_IMPOUND_SLOTS					2

CONST_INT VEHGEN_BIT_FLAG_PLAYER_USED_VEHICLE				0
CONST_INT VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER				1
CONST_INT VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ 				2
CONST_INT VEHGEN_BIT_FLAG_PLAYER_LEFT_HANDED_OVER_VEHICLE 	3

INT iProcessSlot
INT iVehGenProcessFlags[NUMBER_OF_VEHICLES_TO_GEN]
MODEL_NAMES eVehicleModel[NUMBER_OF_VEHICLES_TO_GEN]
VECTOR vVehicleCoordsAtHandOver[NUMBER_OF_VEHICLES_TO_GEN]
BOOL bCarstealWallLoaded = FALSE
BOOL bCarstealWallUpdated = FALSE

CONST_INT   CARSTEAL_WALL_MONROE	0
CONST_INT   CARSTEAL_WALL_CHEETAH	1
CONST_INT   CARSTEAL_WALL_STINGER	2
CONST_INT   CARSTEAL_WALL_JB700		3
CONST_INT   CARSTEAL_WALL_ENTITYXF	4
CONST_INT   CARSTEAL_WALL_ZTYPE		5
OBJECT_INDEX oCarstealWallLine[6]

BOOL bCheckAllNearbyVehiclesInit
BOOL bForceCleanupSetup = FALSE


CONST_INT GVT_PLANE 	0
CONST_INT GVT_BOAT 		1
CONST_INT GVT_HELI 		2
CONST_INT GVT_CAR 		3

STRUCT GARAGE_SETUP_STRUCT
	VEHICLE_GEN_NAME_ENUM eClosestGen
	FLOAT fClosestDist
	INT iPurchaseControl
	INT iWarpControl
	INT iTimer
	INT iContextID
	INT iVehicleType
	MODEL_NAMES ePedModel
	VEHICLE_GEN_DATA_STRUCT sVehGenData
	PURCHASABLE_GARAGE_DATA_STRUCT sVehGenPurchData
	CAMERA_INDEX camCutscene, camCutscene2
	INTERIOR_INSTANCE_INDEX interiorInstanceIndex
	VEHICLE_INDEX vehTrack
ENDSTRUCT
GARAGE_SETUP_STRUCT sGarageData

STRUCT DISPLAY_PURCHASE_MSG_STRUCT
	SCALEFORM_INDEX siScaleform
	BOOL bMessageSet
	BOOL bMessageOut
	BOOL bSoundPlayed
	BANK_ACCOUNT_ACTION_SOURCE_BAAC baacType
ENDSTRUCT
DISPLAY_PURCHASE_MSG_STRUCT sPurchaseMsgData

//Purposefully "hidden" in amongst this script. We want it to be hard
//for hackers to spot these scripts in decompiled output. -BenR
#IF FEATURE_SP_DLC_BEAST_SECRET
#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
    BeastPeyoteVars sBeastPeyoteVars
#ENDIF
#ENDIF

STRUCT_VEHGEN_GARAGE_SCENE sGarageVehicleWarpData

INT iVehicleMenuSelection
INT iVehicleMenuOptions[4]
INT iCustomVehicleSelection
INT iCustomVehicleOptions[1]
INT iVehicleMenuControl
INT iVehicleMenuContextID = NEW_CONTEXT_INTENTION
INT iGarageEntryContextID = NEW_CONTEXT_INTENTION
BOOL bMenuInitialised, bMenuRebuild, bConfirmUpdate, bRebuildHelp
INT iGarageEntrySynchSceneID = -1

INT iImpoundMenuSelection
INT iImpoundMenuOptions
INT iImpoundMenuSlotTrack[MAX_IMPOUND_SLOTS]
INT iImpoundMenuControl
INT iImpoundMenuContextID = NEW_CONTEXT_INTENTION
INT iContextCounter
BOOL bImpoundDoorOverride = FALSE

VEHICLE_INDEX vehImpoundedPlayerVehicle

enumCharacterList eCurrentPlayerPed
BOOL bCurrentVehicleGenSafeForPlayer

VEHICLE_GEN_DATA_STRUCT sData
PURCHASABLE_GARAGE_DATA_STRUCT sPurchData

VEHICLE_SETUP_STRUCT tempVehicleData

BOOL bDoTutorial

INT iEmailToProcess

BOOL bLeaveAreaAfterPurchase

BLIP_INDEX blipImpound

OBJECT_INDEX objForSaleSign[6]
BOOL bObjectRequested

INT iVehGenCheckCount
VEHICLE_GEN_NAME_ENUM eVehGenCheck[NUMBER_OF_VEHICLES_TO_GEN]
INT iVehGenCheck[(NUMBER_OF_VEHICLES_TO_GEN/32)+1]

VEHICLE_GEN_NAME_ENUM ePurchasedVehicleGen = VEHGEN_NONE

FLOAT fDistToVehGen[NUMBER_OF_VEHICLES_TO_GEN]

VEHICLE_STATS_FOR_CLASS_TEMP sVehStatsData

INT iWantedLevelBeforeWarp
INT iGarageHelpStage
BOOL bForceGarageChecks
BOOL bProcessingGarageChecks

//BOOL bExitGarageWhenSittingInVehicle = TRUE
BOOL bExitGarageInDLCVehicle
MODEL_NAMES eDLCModel

BOOL bRepeatPlayActiveState

TEXT_LABEL_15 tlSelectHelpPrinted, tlWarpHelpPrinted

VEHICLE_INDEX lastVehicleBeforeWarp

VEHICLE_INDEX vehPreviousCarToTrackForImpound
VEHICLE_INDEX vehLastMissionVehGenToStreamOut

BOOL bNozzelSet = FALSE
//BOOL bPropertyUnlockChecksProcessedThisFrame = FALSE

CONST_FLOAT fCONST_VEHGEN_TARGET_RADIUS			1.0
CONST_FLOAT fCONST_VEHGEN_STRAIGHT_LINE_DIST	100.0
BOOL bLoadSceneStartedFromVehGen = FALSE
BOOL bAnimDictLoadSceneStartedFromVehGen = FALSE

INT fakeGarageCarEnterCutsceneStage = 0
BOOL bFakeGarageReverse = FALSE
STRING sOpenGarageAnimClip

// url:bugstar:2108774
INT iGarageClear

FUNC BOOL IS_THIS_MODEL_SAFE_FOR_HANGAR(MODEL_NAMES eModel)

	IF eModel = JET
		RETURN FALSE
	ENDIF
	
	IF NOT IS_THIS_MODEL_A_PLANE(eModel)
	AND eModel != MARSHALL
	AND eModel != RHINO
	AND eModel != BARRACKS
	AND eModel != CRUSADER
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_MODEL_SAFE_FOR_GARAGE(MODEL_NAMES eModel)

	IF eModel = MARSHALL
	OR eModel = BARRACKS
	OR eModel = CRUSADER
		RETURN FALSE
	ENDIF
	
	IF eModel = RHINO
		RETURN FALSE
	ENDIF
	
	IF NOT IS_THIS_MODEL_A_CAR(eModel)
	AND NOT IS_THIS_MODEL_A_BIKE(eModel)
	AND NOT IS_THIS_MODEL_A_BICYCLE(eModel)
	AND NOT IS_THIS_MODEL_A_QUADBIKE(eModel)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(MODEL_NAMES eModel)
	SWITCH sGarageData.iVehicleType
		CASE GVT_PLANE
			RETURN IS_THIS_MODEL_SAFE_FOR_HANGAR(eModel)
		BREAK
		CASE GVT_BOAT
			IF (IS_THIS_MODEL_A_BOAT(eModel))
			OR (IS_THIS_MODEL_A_JETSKI(eModel))
			OR (eModel = SUBMERSIBLE2)
			OR (eModel = DODO)
				RETURN TRUE
			ENDIF
		BREAK
		CASE GVT_CAR
			RETURN IS_THIS_MODEL_SAFE_FOR_GARAGE(eModel)
		BREAK
		CASE GVT_HELI
			RETURN IS_THIS_MODEL_A_HELI(eModel) AND eModel != SKYLIFT
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_INTERP_POINT_FLOAT(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

#IF IS_DEBUG_BUILD
BOOL b_DebugGivePlayerAllGarages
#ENDIF

/// PURPOSE:
///    To clone a local version of a vehicle including any damage for a cutscene
FUNC BOOL CREATE_CUTSCENE_VEHICLE_CLONE(VEHICLE_INDEX &NewVehicleId, VEHICLE_INDEX VehToClone, VECTOR vPos, FLOAT fHeading) 
	
	model_names vehModel
	INT irepeat
	
	VEHICLE_SETUP_STRUCT fakeGarageVehicleSetupStructSp
	RESET_VEHICLE_SETUP_STRUCT(fakeGarageVehicleSetupStructSp)
	
	IF IS_VEHICLE_DRIVEABLE(VehToClone)
		vehModel = GET_ENTITY_MODEL(VehToClone)
		GET_VEHICLE_SETUP(VehToClone, fakeGarageVehicleSetupStructSp)
	ELSE
		#IF IS_DEBUG_BUILD	
			DEBUG_PRINTCALLSTACK()			
			SCRIPT_ASSERT("CREATE_CUTSCENE_VEHICLE_CLONE: IS_VEHICLE_DRIVEABLE  = FALSE")		
			PRINTSTRING("CREATE_CUTSCENE_VEHICLE_CLONE - called by ") PRINTSTRING(GET_THIS_SCRIPT_NAME()) 
		#ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD	
		IF NOT REQUEST_LOAD_MODEL(vehModel)
			DEBUG_PRINTCALLSTACK()			
			SCRIPT_ASSERT("CREATE_CUTSCENE_VEHICLE_CLONE: LOAD_MODEL()  = FALSE")		
		ENDIF	
		PRINTSTRING("CREATE_CUTSCENE_VEHICLE_CLONE - called by ")PRINTSTRING(GET_THIS_SCRIPT_NAME()) 
		PRINTSTRING(", Model = ")PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(vehModel))PRINTNL()
	#ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(NewVehicleId)
		NewVehicleId = CREATE_VEHICLE(vehModel, vPos, fHeading, FALSE, FALSE)
		
		GET_VEHICLE_SETUP(NewVehicleId, fakeGarageVehicleSetupStructSp)
		
		IF NOT IS_THIS_MODEL_A_BIKE(vehModel)
			FOR irepeat = ENUM_TO_INT(SC_DOOR_FRONT_LEFT) TO ENUM_TO_INT(SC_DOOR_BOOT)
				IF IS_VEHICLE_DOOR_DAMAGED(VehToClone,INT_TO_ENUM(SC_DOOR_LIST,irepeat))
					SET_VEHICLE_DOOR_BROKEN(NewVehicleId,INT_TO_ENUM(SC_DOOR_LIST,irepeat),TRUE)
				ENDIF
			ENDFOR
		ENDIF
		FOR irepeat = ENUM_TO_INT(SC_WHEEL_CAR_FRONT_LEFT) TO ENUM_TO_INT(SC_WHEEL_BIKE_REAR)
			IF IS_VEHICLE_TYRE_BURST(VehToClone,INT_TO_ENUM(SC_WHEEL_LIST,irepeat),TRUE)
				SET_VEHICLE_TYRE_BURST(NewVehicleId,INT_TO_ENUM(SC_WHEEL_LIST,irepeat),TRUE)
			ELIF IS_VEHICLE_TYRE_BURST(VehToClone,INT_TO_ENUM(SC_WHEEL_LIST,irepeat))
				SET_VEHICLE_TYRE_BURST(NewVehicleId,INT_TO_ENUM(SC_WHEEL_LIST,irepeat))
			ENDIF
		ENDFOR
		IF NOT IS_THIS_MODEL_A_BIKE(vehModel)
			FOR irepeat = ENUM_TO_INT(SC_WINDOW_FRONT_LEFT) TO ENUM_TO_INT(SC_WINDOW_REAR_RIGHT)
				IF NOT IS_VEHICLE_WINDOW_INTACT(VehToClone,INT_TO_ENUM(SC_WINDOW_LIST,irepeat))
					REMOVE_VEHICLE_WINDOW(NewVehicleId,INT_TO_ENUM(SC_WINDOW_LIST,irepeat))
				ENDIF
			ENDFOR
		ENDIF
		
		COPY_VEHICLE_DAMAGES(VehToClone, NewVehicleId)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(NewVehicleId)
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



#IF IS_DEBUG_BUILD
PROC DRAW_DEBUG_VEHGEN_SCENE_TEXT(TEXT_LABEL_63 str, INT iWarpControl, FLOAT &fStrOffset, HUD_COLOURS hudColour = HUD_COLOUR_SIMPLEBLIP_DEFAULT, INT iColumn = 0)
	INT iRed, iGreen, iBlue, iAlpha
	IF (hudColour = HUD_COLOUR_SIMPLEBLIP_DEFAULT)
		IF iWarpControl = 0
			GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
		ELIF iWarpControl < 10
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
		ENDIF
	ELSE
		GET_HUD_COLOUR(hudColour, iRed, iGreen, iBlue, iAlpha)
	ENDIF
	
	DRAW_DEBUG_TEXT_2D(str, <<0.2,0.1,0>>+(<<0.0,0.01,0>>*fStrOffset)+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)
	
	IF iWarpControl > -1
		IF fStrOffset = 0.0
			PRINTLN(str)
		ELSE
			PRINTLN(" * ", str)
		ENDIF
	ELSE
		PRINTLN(str)
	ENDIF
	
	fStrOffset += 1.0
ENDPROC
#ENDIF

VEHICLE_INDEX vehFakeGarageEnter
FLOAT fFakeVehicleOffset
INT iGarageCutTimer
//OBJECT_INDEX objFakeGarageDoor
//MODEL_NAMES fakeGarageDoorModel = V_ILEV_CSR_GARAGEDOOR
FUNC BOOL DO_FAKE_GARAGE_CAR_DRIVE(PED_INDEX PedIndex,
		structSceneTool_Point mPoint, structSceneTool_Marker mMarker,
		FLOAT fDurationMS,
		structSceneTool_Placer mPlacer, CAMERA_GRAPH_TYPE camGraphTypeFakeGarage,
		FLOAT &fStrOffset)
	
	VECTOR vStartPoint = mPoint.vPos
	VECTOR vStartRotation = mPoint.vRot
	VECTOR vEndPoint = mMarker.vPos
	VECTOR vEndRotation = mPoint.vRot
	
	CONST_INT FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT		0
	CONST_INT FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING	1
	CONST_INT FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE	2
	
	#IF IS_DEBUG_BUILD
	INT iRed, iGreen, iBlue, iAlpha
	HUD_COLOURS hudColour = HUD_COLOUR_YELLOW
	GET_HUD_COLOUR(hudColour, iRed, iGreen, iBlue, iAlpha)
	TEXT_LABEL_63 str = ""
	
	VECTOR vDiffPoint
	FLOAT fPlacerHeading, fDiffHeading
	vDiffPoint = vStartPoint - vEndPoint
	fPlacerHeading = ((vStartRotation.z+vEndRotation.z) / 2.0) - 180.0
	IF fPlacerHeading > 180
		fPlacerHeading -= 360
	ENDIF
	IF fPlacerHeading < -180
		fPlacerHeading += 360
	ENDIF
	fDiffHeading = GET_HEADING_FROM_VECTOR_2D(vDiffPoint.x, vDiffPoint.y)
	IF fDiffHeading > 180
		fDiffHeading -= 360
	ENDIF
	IF fDiffHeading < -180
		fDiffHeading += 360
	ENDIF
	#ENDIF
	
	IF (fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT)
		#IF IS_DEBUG_BUILD
		str  = "FAKE_GARAGE_CAR_ENTER_CUTSCENE_INIT"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "vStartPoint: "
		str += GET_STRING_FROM_FLOAT(vStartPoint.x)
		str += ", "
		str += GET_STRING_FROM_FLOAT(vStartPoint.y)
		str += ", "
		str += GET_STRING_FROM_FLOAT(vStartPoint.z)
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "vEndPoint: "
		str += GET_STRING_FROM_FLOAT(vEndPoint.x)
		str += ", "
		str += GET_STRING_FROM_FLOAT(vEndPoint.y)
		str += ", "
		str += GET_STRING_FROM_FLOAT(vEndPoint.z)
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "heading: "
		str += GET_STRING_FROM_FLOAT(fDiffHeading)
		str += " ["
		str += GET_STRING_FROM_FLOAT(fPlacerHeading)
		str += "]"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "distance: "
		str += GET_STRING_FROM_FLOAT(VDIST(vStartPoint, vEndPoint))
		str += "m"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		PRINTLN("")
		#ENDIF
		
		//Clones
		VEHICLE_INDEX vehPlayer
		vehPlayer = GET_VEHICLE_PED_IS_IN(PedIndex)
	/*	CREATE_CUTSCENE_VEHICLE_CLONE(vehFakeGarageEnter, vehPlayer, vStartPoint, vStartRotation.Z) */
		vehFakeGarageEnter = vehPlayer

		SET_VEHICLE_DOORS_SHUT(vehFakeGarageEnter, TRUE)
		SET_VEHICLE_DOORS_LOCKED(vehFakeGarageEnter, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehFakeGarageEnter)
		SET_VEHICLE_ENGINE_ON(vehFakeGarageEnter, TRUE, TRUE)
		
		IF IS_PED_ON_ANY_BIKE(PedIndex)
			GIVE_PED_HELMET(PedIndex, FALSE, PV_FLAG_NONE)
			SET_PED_HELMET(PedIndex, TRUE)
		ENDIF
		
		VECTOR vFakeGaragePlayerCarCoords
		vFakeGaragePlayerCarCoords = GET_ENTITY_COORDS(vehFakeGarageEnter)
		
		fFakeVehicleOffset = vFakeGaragePlayerCarCoords.Z - vStartPoint.Z
		
		SET_ENTITY_COORDS(vehFakeGarageEnter, vStartPoint + <<0.0, 0.0, -10.0>>)
		
		IF IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
			SET_VEHICLE_LIGHTS(vehFakeGarageEnter, SET_VEHICLE_LIGHTS_ON)
		ENDIF
		
		iGarageCutTimer = GET_GAME_TIMER()
		fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING
	ENDIF
	IF (fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING)
		#IF IS_DEBUG_BUILD
		str  = "FAKE_GARAGE_CAR_ENTER_CUTSCENE_RUNNING"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "iGarageCutTimer: "
		str += GET_STRING_FROM_FLOAT(TO_FLOAT(iGarageCutTimer) / 1000.0)
		str += "s, fDurationMS: "
		str += GET_STRING_FROM_FLOAT((fDurationMS) / 1000.0)
		str += "s"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "iGarageCutTimer+fDurationMS: "
		str += GET_STRING_FROM_FLOAT(((TO_FLOAT(iGarageCutTimer) + (fDurationMS)) - GET_GAME_TIMER()) / 1000.0)
		str += "s"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "GET_GAME_TIMER: "
		str += GET_STRING_FROM_FLOAT(TO_FLOAT(GET_GAME_TIMER()) / 1000.0)
		str += "s"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		str  = "heading: "
		str += GET_STRING_FROM_FLOAT(fDiffHeading)
		str += " ["
		str += GET_STRING_FROM_FLOAT(fPlacerHeading)
		str += "]"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		FLOAT fDistanceInMeters
		FLOAT fDurationInSeconds
		fDistanceInMeters = VDIST(vStartPoint, vEndPoint)
		fDurationInSeconds = fDurationMS/1000.0
		
		FLOAT fDistanceInKilometers
		FLOAT fDurationInHours
		fDistanceInKilometers = fDistanceInMeters/1000.0
		fDurationInHours = fDurationInSeconds/3600.0
		
		FLOAT fSpeedInKilometersPerHour
		fSpeedInKilometersPerHour = fDistanceInKilometers / fDurationInHours
		
		str  = "distance: "
		str += GET_STRING_FROM_FLOAT(VDIST(vStartPoint, vEndPoint))
		str += "m, speed: "
		str += GET_STRING_FROM_FLOAT(fSpeedInKilometersPerHour)
		str += " Km/h"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		IF bFakeGarageReverse
			str  = "bFakeGarageReverse"
			DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		ELSE
			str  = "NOT bFakeGarageReverse"
			DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		ENDIF
		
		str  = "GRAPH_TYPE_"
		SWITCH camGraphTypeFakeGarage
			CASE GRAPH_TYPE_LINEAR
				str += "LINEAR"
			BREAK
			CASE GRAPH_TYPE_SIN_ACCEL_DECEL
				str += "SIN_ACCEL_DECEL"
			BREAK
			CASE GRAPH_TYPE_ACCEL
				str += "ACCEL"
			BREAK
			CASE GRAPH_TYPE_DECEL
				str += "DECEL"
			BREAK
			
			DEFAULT
				str += "UNKNOWN_"
				str += ENUM_TO_INT(camGraphTypeFakeGarage)
			BREAK
		ENDSWITCH
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		
		CONST_FLOAT fReasonableSpeed	30.0
		IF fSpeedInKilometersPerHour > fReasonableSpeed
			/*
			VECTOR vStartPoint = mPoint.vPos
			VECTOR vStartRotation = mPoint.vRot
			VECTOR vEndPoint = mMarker.vPos
			VECTOR vEndRotation = mPoint.vRot
			*/
			IF sGarageData.iWarpControl = 3
				CWARNINGLN(DEBUG_AMBIENT, "speed to damn high (enterVeh, vEndPoint: ",vEndPoint, ")")
			ELIF sGarageData.iWarpControl = 13
				CWARNINGLN(DEBUG_AMBIENT, "speed to damn high (exitVeh, vEndPoint: ",vEndPoint, ")")
			ELSE
				CWARNINGLN(DEBUG_AMBIENT, "speed to damn high (iWarpControl: ", sGarageData.iWarpControl, ")")
			ENDIF
			
		ENDIF
		#ENDIF
		
		IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
		AND IS_VEHICLE_DRIVEABLE(vehFakeGarageEnter)
			
			FLOAT fStartTime = TO_FLOAT(iGarageCutTimer)
			FLOAT fEndTime = fStartTime + fDurationMS
			FLOAT fPointTime = CLAMP(TO_FLOAT(GET_GAME_TIMER()), fStartTime, fEndTime) 
			fPointTime -= fStartTime
			fPointTime /= fDurationMS
			
			IF camGraphTypeFakeGarage = GRAPH_TYPE_SIN_ACCEL_DECEL
				fPointTime = GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fPointTime)
			ELIF camGraphTypeFakeGarage = GRAPH_TYPE_ACCEL
				fPointTime = GET_GRAPH_TYPE_ACCEL(fPointTime)
			ELIF camGraphTypeFakeGarage = GRAPH_TYPE_DECEL
				fPointTime = GET_GRAPH_TYPE_DECEL(fPointTime)
			ENDIF
			
			fPointTime *= fDurationMS
			fPointTime += fStartTime
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			
			SET_ENTITY_COORDS_NO_OFFSET(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(
					vStartPoint + <<0.0, 0.0, fFakeVehicleOffset>>,			//VECTOR vStartPos
					vEndPoint + <<0.0, 0.0, fFakeVehicleOffset>>,			//VECTOR vEndPos
					fStartTime, fEndTime, fPointTime))
			
			
			#IF IS_DEBUG_BUILD
			DRAW_DEBUG_SPHERE(vStartPoint + <<0.0, 0.0, fFakeVehicleOffset>>, 1.0,				iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha) / 0.15))
			DRAW_DEBUG_TEXT("vStartPoint", vStartPoint + <<0.0, 0.0, fFakeVehicleOffset>>,		255-iRed, 255-iGreen, 255-iBlue, ROUND(TO_FLOAT(iAlpha) / 0.15))
			DRAW_DEBUG_SPHERE(vEndPoint + <<0.0, 0.0, fFakeVehicleOffset>>, 1.0,				255-iRed, 255-iGreen, 255-iBlue, ROUND(TO_FLOAT(iAlpha) / 0.15))
			DRAW_DEBUG_TEXT("vEndPoint", vEndPoint + <<0.0, 0.0, fFakeVehicleOffset>>,			iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha) / 0.15))
			DRAW_DEBUG_LINE_WITH_TWO_COLOURS(
					vStartPoint + <<0.0, 0.0, fFakeVehicleOffset>>,
					vEndPoint + <<0.0, 0.0, fFakeVehicleOffset>>,
					iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha) / 0.15),
					255-iRed, 255-iGreen, 255-iBlue, ROUND(TO_FLOAT(iAlpha) / 0.15))
			#ENDIF
			
			IF bFakeGarageReverse AND NOT IS_PED_ON_ANY_BIKE(PedIndex)
				SET_ENTITY_ROTATION(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(
						<<0.0 - vStartRotation.X, 0.0 - vStartRotation.Y, vStartRotation.Z + 180.0>>,
						<<0.0 - vEndRotation.X, 0.0 - vEndRotation.Y, vEndRotation.Z + 180.0>>,
						fStartTime, fEndTime, fPointTime))
			ELSE
				
				IF NOT IS_PED_INJURED(PedIndex)
					IF IS_PED_ON_ANY_BIKE(PedIndex)
						SET_PED_RESET_FLAG(PedIndex, PRF_PreventGoingIntoStillInVehicleState, TRUE)
						SET_PED_RESET_FLAG(PedIndex, PRF_DisableInVehicleActions, TRUE)
					ENDIF
				ENDIF
				
				SET_ENTITY_ROTATION(vehFakeGarageEnter, GET_INTERP_POINT_VECTOR(
						vStartRotation, vEndRotation,
						fStartTime, fEndTime, fPointTime))
			ENDIF
			SET_ENTITY_COLLISION(vehFakeGarageEnter, FALSE)
			FREEZE_ENTITY_POSITION(vehFakeGarageEnter, TRUE)
		ELSE 
			fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
			RETURN FALSE
		ENDIF
		
		IF TO_FLOAT(GET_GAME_TIMER()) > TO_FLOAT(iGarageCutTimer) + (fDurationMS)
		AND TO_FLOAT(GET_GAME_TIMER()) > TO_FLOAT(iGarageCutTimer) + (fDurationMS) + 2600.0 // + 1000 for bug 1635631
			fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE
			RETURN FALSE
		ENDIF
	ENDIF
	IF (fakeGarageCarEnterCutsceneStage = FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE)
		#IF IS_DEBUG_BUILD
		str  = "FAKE_GARAGE_CAR_ENTER_CUTSCENE_COMPLETE"
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, -1, fStrOffset, hudColour)
		#ENDIF
		
		SET_PED_RESET_FLAG(PedIndex, PRF_PreventGoingIntoStillInVehicleState, FALSE)
		SET_PED_RESET_FLAG(PedIndex, PRF_DisableInVehicleActions, FALSE)
		
		IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
			IF NOT ARE_VECTORS_EQUAL(mPlacer.vPos, <<0,0,0>>)
				SET_ENTITY_COORDS(vehFakeGarageEnter, mPlacer.vPos)
				SET_ENTITY_HEADING(vehFakeGarageEnter, mPlacer.fRot)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehFakeGarageEnter)
			ENDIF
			
			SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
			FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
			vehFakeGarageEnter = NULL
		ENDIF
		
		IF IS_PED_ON_ANY_BIKE(PedIndex)
			REMOVE_PED_HELMET(PedIndex, FALSE)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(fStrOffset)
	#ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID wVehicleGenControllerWidget
	BOOL bSetMissionVehGenDebug
	BOOL bSetMissionVehGenDebug2
	BOOL bCreateImpoundVehDebug
	BOOL bClearImpoundVehDebug
	BOOL bOutputImpoundVehDebug
	BOOL bTrackImpoundVehDebug
	INT iImpoundVehSlotChar
	INT iImpoundVehSlotDebug
	BOOL bOutputSpecialVehicleInfo
	VEHICLE_INDEX vehImpound
	
	INT iVehgen_garage_scene_type
	structSceneTool_Launcher rag_vehgen_garage_scene
	BOOL bSnapMarkerToPoint
	
	BOOL bPlayTutorial
	
	FUNC BOOL Debug_RUN_VEHGEN_GARAGE_SCENE(STRUCT_VEHGEN_GARAGE_SCENE &scene, enumVEHGEN_GARAGE_SCENE_Type eVehgen_garage_scene_type)
		IF IS_PED_INJURED(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
		
		enumVEHGEN_GARAGE_SCENE_Pans ePan
		enumVEHGEN_GARAGE_SCENE_Markers eMarker
		enumVEHGEN_GARAGE_SCENE_Placers ePlacers
		enumVEHGEN_GARAGE_SCENE_Points ePoint
		
		VEHICLE_INDEX currentVeh
		OBJECT_INDEX garageObj
		CAMERA_INDEX camCutscene, camCutscene2
		
		WIDGET_GROUP_ID wDebugVehGenSceneWidget
		BOOL bEditScene
		FLOAT fSynchScenePhase
		INT iGraph = ENUM_TO_INT(GRAPH_TYPE_LINEAR)
		STRING sOpenGarageAnimDict = "ANIM@APT_TRANS@GARAGE"
		REQUEST_ANIM_DICT(sOpenGarageAnimDict)
		
		SWITCH eVehgen_garage_scene_type
			CASE VEHGEN_GARAGE_SCENE_TYPE_enterPed
				ePan = VEHGEN_GARAGE_SCENE_PAN_enterPed
				eMarker = VEHGEN_GARAGE_SCENE_MARKER_MAX
				ePlacers = VEHGEN_GARAGE_SCENE_PLACER_enterPed
				ePoint = VEHGEN_GARAGE_SCENE_POINT_MAX
			BREAK
			CASE VEHGEN_GARAGE_SCENE_TYPE_enterVeh
				ePan = VEHGEN_GARAGE_SCENE_PAN_enterVeh
				eMarker = VEHGEN_GARAGE_SCENE_MARKER_enterVeh
				ePlacers = VEHGEN_GARAGE_SCENE_PLACER_MAX
				ePoint = VEHGEN_GARAGE_SCENE_POINT_enterVeh
				iGraph = ENUM_TO_INT(GRAPH_TYPE_ACCEL)
			BREAK
			CASE VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen
				ePan = VEHGEN_GARAGE_SCENE_PAN_exitPedOpen
				eMarker = VEHGEN_GARAGE_SCENE_MARKER_MAX
				ePlacers = VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen
				ePoint = VEHGEN_GARAGE_SCENE_POINT_MAX
			BREAK
//			CASE VEHGEN_GARAGE_SCENE_TYPE_exitPedClose
//				ePan = VEHGEN_GARAGE_SCENE_PAN_exitPedClose
//				eMarker = VEHGEN_GARAGE_SCENE_MARKER_exitPedClose
//				ePlacers = VEHGEN_GARAGE_SCENE_PLACER_exitPedClose
//				ePoint = VEHGEN_GARAGE_SCENE_POINT_MAX
//			BREAK
			CASE VEHGEN_GARAGE_SCENE_TYPE_exitVeh
				ePan = VEHGEN_GARAGE_SCENE_PAN_exitVeh
				eMarker = VEHGEN_GARAGE_SCENE_MARKER_exitVeh
				ePlacers = VEHGEN_GARAGE_SCENE_PLACER_exitVeh
				ePoint = VEHGEN_GARAGE_SCENE_POINT_exitVeh
				iGraph = ENUM_TO_INT(GRAPH_TYPE_DECEL)
			BREAK
		
		ENDSWITCH
		
		IF eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_enterVeh
		OR eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_exitVeh
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				currentVeh = GET_CLOSEST_VEHICLE(scene.mPoints[ePoint].vPos, 50.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				
				IF DOES_ENTITY_EXIST(currentVeh)
					IF NOT IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(GET_ENTITY_MODEL(currentVeh))
						PRINTLN("finding currentVeh - DELETE_VEHICLE(", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(currentVeh), " not suitable for garage)")
						
						SET_ENTITY_AS_MISSION_ENTITY(currentVeh)
						DELETE_VEHICLE(currentVeh)
						
						RETURN FALSE
					ENDIF
					
					PED_INDEX pedIndex = GET_PED_IN_VEHICLE_SEAT(currentVeh, VS_DRIVER)
					IF DOES_ENTITY_EXIST(pedIndex)
						SET_ENTITY_AS_MISSION_ENTITY(pedIndex)
						CLEAR_PED_TASKS_IMMEDIATELY(pedIndex)
						DELETE_PED(pedIndex)
					ENDIF
					
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), currentVeh, VS_DRIVER)
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("finding currentVeh - DOES_ENTITY_EXIST(currentVeh)")
				currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			
			IF DOES_ENTITY_EXIST(currentVeh)
			AND IS_VEHICLE_DRIVEABLE(currentVeh)
				SET_ENTITY_PROOFS(currentVeh, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_ENTITY_COORDS(currentVeh, scene.mPoints[ePoint].vPos)
				SET_ENTITY_HEADING(currentVeh, scene.mPoints[ePoint].vRot.z)
				SET_VEHICLE_ON_GROUND_PROPERLY(currentVeh)	
			ENDIF
			
//			REQUEST_MODEL(fakeGarageDoorModel)
//			
//			garageObj = CREATE_OBJECT_NO_OFFSET(fakeGarageDoorModel, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos)
//			SET_ENTITY_ROTATION(garageObj, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot)
			
			fakeGarageCarEnterCutsceneStage = 0
//			TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), currentVeh, scene.mMarkers[eMarker].vPos, 5.0, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, fCONST_VEHGEN_TARGET_RADIUS, fCONST_VEHGEN_STRAIGHT_LINE_DIST)
			
			SET_CURRENT_WIDGET_GROUP(rag_vehgen_garage_scene.hWidget)
			wDebugVehGenSceneWidget = START_WIDGET_GROUP("wDebugVehGenSceneWidget")
				ADD_WIDGET_BOOL("bEditScene", bEditScene)
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
					ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
				STOP_WIDGET_COMBO("iGraph", iGraph)
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(rag_vehgen_garage_scene.hWidget)
		ELIF eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_enterPed
			OR eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[ePlacers].vPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[ePlacers].fRot)
			
			SET_CURRENT_WIDGET_GROUP(wVehicleGenControllerWidget)
			wDebugVehGenSceneWidget = START_WIDGET_GROUP("wDebugVehGenSceneWidget")
				ADD_WIDGET_BOOL("bEditScene", bEditScene)
				ADD_WIDGET_FLOAT_SLIDER("fSynchScenePhase", fSynchScenePhase, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(wVehicleGenControllerWidget)
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[ePlacers].vPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[ePlacers].fRot)	
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), scene.mMarkers[eMarker].vPos, PEDMOVE_WALK)
			
			SET_CURRENT_WIDGET_GROUP(wVehicleGenControllerWidget)
			wDebugVehGenSceneWidget = START_WIDGET_GROUP("wDebugVehGenSceneWidget")
				ADD_WIDGET_BOOL("bEditScene", bEditScene)
			//	ADD_WIDGET_FLOAT_SLIDER("fSynchScenePhase", fSynchScenePhase, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(wVehicleGenControllerWidget)
		ENDIF
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
		SceneTool_ExecutePan(scene.mPans[ePan], camCutscene, camCutscene2)
		
		DISPLAY_HUD(FALSE)
		DISPLAY_RADAR(FALSE)
		
		SETTIMERA(0)
		WHILE (TIMERA() <= ((scene.mPans[ePan].fDuration*1000)))
		OR bEditScene
			FLOAT fStrOffset = 0
			TEXT_LABEL_63 str
			IF NOT bEditScene
				str  = "Cam Timer: "
				str += TIMERA()
				str += " > "
				str += ROUND(scene.mPans[ePan].fDuration*1000)
			ELSE
				SETTIMERA(0)
				
				str  = "fSynchScenePhase: "
				str += GET_STRING_FROM_FLOAT(fSynchScenePhase)
			ENDIF
			
			SET_TEXT_COLOUR(255, 100, 255, 255)
			SET_TEXT_SCALE(0.7, 0.8)
			SET_TEXT_WRAP(0.0, 1.0)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", str)
			
			IF eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_enterVeh
			OR eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_exitVeh
				
				structSceneTool_Placer mPlacer
				IF ePlacers < VEHGEN_GARAGE_SCENE_PLACER_MAX
					mPlacer = scene.mPlacers[ePlacers]
				ELSE
					mPlacer.vPos = <<0,0,0>>
					mPlacer.fRot = 0.0
				ENDIF
				
				DO_FAKE_GARAGE_CAR_DRIVE(PLAYER_PED_ID(),
						scene.mPoints[ePoint],
						scene.mMarkers[eMarker],
						(scene.mPans[ePan].fDuration*1000.0) - 0500,
						mPlacer, INT_TO_ENUM(CAMERA_GRAPH_TYPE, iGraph),
						fStrOffset)
			ELIF eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_enterPed
			OR eVehgen_garage_scene_type = VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen
				SWITCH GET_RANDOM_INT_IN_RANGE(0, 6)
					CASE 0	sOpenGarageAnimClip = "gar_open_1_left" BREAK
					CASE 1	sOpenGarageAnimClip = "gar_open_1_right" BREAK
					CASE 2	sOpenGarageAnimClip = "gar_open_2_left" BREAK
					CASE 3	sOpenGarageAnimClip = "gar_open_2_right" BREAK
					CASE 4	sOpenGarageAnimClip = "gar_open_3_left" BREAK
					CASE 5	sOpenGarageAnimClip = "gar_open_3_right" BREAK
				ENDSWITCH
				
				VECTOR vGarageEntrySynchScenePos21 = scene.mPlacers[ePlacers].vPos
				FLOAT fGarageEntrySynchSceneHead21 = scene.mPlacers[ePlacers].fRot
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iGarageEntrySynchSceneID)
					REQUEST_ANIM_DICT(sOpenGarageAnimDict)
					IF HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						iGarageEntrySynchSceneID = CREATE_SYNCHRONIZED_SCENE(vGarageEntrySynchScenePos21, <<0,0,fGarageEntrySynchSceneHead21>>)
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iGarageEntrySynchSceneID,
								sOpenGarageAnimDict, sOpenGarageAnimClip, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
								DEFAULT,		//SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_NONE,
								DEFAULT,		//RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_NONE,
								DEFAULT,		//FLOAT moverBlendInDelta = INSTANT_BLEND_IN,
								DEFAULT)		//IK_CONTROL_FLAGS ikFlags = AIK_NONE)
					ENDIF
				ELSE
					IF bEditScene
						SET_SYNCHRONIZED_SCENE_ORIGIN(iGarageEntrySynchSceneID, vGarageEntrySynchScenePos21, <<0,0,fGarageEntrySynchSceneHead21>>)
						SET_SYNCHRONIZED_SCENE_PHASE(iGarageEntrySynchSceneID, fSynchScenePhase)
					ELSE
						fSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iGarageEntrySynchSceneID)
					ENDIF
				ENDIF
			ENDIF
			
			WAIT(0)
		ENDWHILE
		
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
			SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
			FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
			vehFakeGarageEnter = NULL
		ENDIF
		IF DOES_ENTITY_EXIST(garageObj)
			DELETE_OBJECT(garageObj)
		ENDIF
		
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		IF DOES_CAM_EXIST(camCutscene)
			SET_CAM_ACTIVE(camCutscene, FALSE)
			DESTROY_CAM(camCutscene)
		ENDIF
		IF DOES_CAM_EXIST(camCutscene2)
			SET_CAM_ACTIVE(camCutscene2, FALSE)
			DESTROY_CAM(camCutscene2)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(wDebugVehGenSceneWidget)
			DELETE_WIDGET_GROUP(wDebugVehGenSceneWidget)
		ENDIF
		
		RETURN TRUE
	ENDFUNC

	PROC SETUP_VEHICLE_CONTROL_WIDGETS()
		wVehicleGenControllerWidget = START_WIDGET_GROUP("Vehicle Gen Controller")
			
			START_WIDGET_GROUP("CGtoNG")
				ADD_WIDGET_BOOL("CGtoNG Player Debug", g_bLastGenPlayer)
				ADD_WIDGET_BOOL("Stalion Unlocked", g_savedGlobals.sCountryRaceData.bStallionUnlocked)
				ADD_WIDGET_BOOL("Gauntlet Unlocked", g_savedGlobals.sCountryRaceData.bGauntletUnlocked)
				ADD_WIDGET_BOOL("Dominator Unlocked", g_savedGlobals.sCountryRaceData.bDominatorUnlocked)
				ADD_WIDGET_BOOL("Buffalo Unlocked", g_savedGlobals.sCountryRaceData.bBuffaloUnlocked)
				ADD_WIDGET_BOOL("Marshall Unlocked", g_savedGlobals.sCountryRaceData.bMarshallUnlocked)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Always display impound help", g_bAlwaysDisplayImpoundHelp)
			ADD_WIDGET_BOOL("Set mission vehgen", bSetMissionVehGenDebug)
			ADD_WIDGET_BOOL("Set mission vehgen with coords", bSetMissionVehGenDebug2)
			ADD_WIDGET_BOOL("Check nearby vehicles", g_sVehicleGenNSData.bCheckVehGensLoaded)
			ADD_WIDGET_BOOL("Track current vehicle for impound", bTrackImpoundVehDebug)
			ADD_WIDGET_BOOL("Create impound vehicle", bCreateImpoundVehDebug)
			ADD_WIDGET_BOOL("Clear impound vehicle", bClearImpoundVehDebug)
			ADD_WIDGET_BOOL("Output impound info", bOutputImpoundVehDebug)
			ADD_WIDGET_BOOL("Output special vehicle info", bOutputSpecialVehicleInfo)
			ADD_WIDGET_INT_SLIDER("Impound char slot (0 = Michael, 1 = Franklin, 2 = Trevor", iImpoundVehSlotChar, 0, 2, 1)
			ADD_WIDGET_INT_SLIDER("Impound slot", iImpoundVehSlotDebug, 0, 1, 1)
			
			ADD_WIDGET_INT_READ_ONLY("Purchase control", sGarageData.iPurchaseControl)
			ADD_WIDGET_BOOL("bPlayTutorial", bPlayTutorial)
			
			
			
			ADD_WIDGET_INT_READ_ONLY("Warp control", sGarageData.iWarpControl)
//			ADD_WIDGET_BOOL("Exit garage when sitting in vehicle", bExitGarageWhenSittingInVehicle)
			
			START_SCENETOOL_LAUNCHER("Player garage scene editor", rag_vehgen_garage_scene)
				VEHICLE_GEN_NAME_ENUM eName
				REPEAT VEHGEN_MISSION_VEH eName
					IF eName = VEHGEN_WEB_CAR_MICHAEL
						ADD_TO_WIDGET_COMBO("VEHGEN_WEB_CAR_MICHAEL")
					ELIF eName = VEHGEN_WEB_CAR_FRANKLIN
						ADD_TO_WIDGET_COMBO("VEHGEN_WEB_CAR_FRANKLIN")
					ELIF eName = VEHGEN_WEB_CAR_TREVOR
						ADD_TO_WIDGET_COMBO("VEHGEN_WEB_CAR_TREVOR")
					ELSE
						ADD_TO_WIDGET_COMBO("pick a building")
					ENDIF
				ENDREPEAT
			STOP_SCENETOOL_LAUNCHER(rag_vehgen_garage_scene)
			
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL	rag_vehgen_garage_scene.iLaunchSceneID = ENUM_TO_INT(VEHGEN_WEB_CAR_MICHAEL) BREAK
				CASE CHAR_FRANKLIN	rag_vehgen_garage_scene.iLaunchSceneID = ENUM_TO_INT(VEHGEN_WEB_CAR_FRANKLIN) BREAK
				CASE CHAR_TREVOR	rag_vehgen_garage_scene.iLaunchSceneID = ENUM_TO_INT(VEHGEN_WEB_CAR_TREVOR) BREAK
			ENDSWITCH
			
			ADD_WIDGET_BOOL("bFakeGarageReverse", bFakeGarageReverse)
			
//			START_WIDGET_GROUP("FAKE \"Drive into garage\" scene editor")
//				ADD_WIDGET_BOOL("Play", bDebugFakeGarageInterp)
//				ADD_WIDGET_BOOL("Pause", bDebugFakeGaragePause)
//				ADD_WIDGET_BOOL("Loop", bDebugFakeGarageLoop)
//				ADD_WIDGET_FLOAT_SLIDER("Phase", fDebugFakeGaragePhase, 0.0, 1.0, 0.1)
//				ADD_WIDGET_STRING("")
//				START_WIDGET_GROUP("Vehicle")
//					ADD_WIDGET_BOOL("Set Start Point (At Car)", bDebugFakeGarageSetCar1)
//					ADD_WIDGET_VECTOR_SLIDER("Start Point", vDebugFakeGaragePoint1, -10000.0, 10000.0, 0.01)
//					ADD_WIDGET_VECTOR_SLIDER("Start Rotation", vDebugFakeGarageRotation1, -180.0, 180.0, 1.0)
//					ADD_WIDGET_STRING("")
//					ADD_WIDGET_BOOL("Set End Point (Ahead Of Car)", bDebugFakeGarageSetCar2)
//					ADD_WIDGET_VECTOR_SLIDER("End Point", vDebugFakeGaragePoint2, -10000.0, 10000.0, 0.01)
//					ADD_WIDGET_VECTOR_SLIDER("End Rotation", vDebugFakeGarageRotation2, -180.0, 180.0, 1.0)
//					ADD_WIDGET_STRING("")
//					ADD_WIDGET_INT_SLIDER("Duration", iDebugFakeGarageDuration, 0, 60000, 1)
//				STOP_WIDGET_GROUP()
//				START_WIDGET_GROUP("Camera")
//					ADD_WIDGET_BOOL("Enable", bDebugFakeGarageCamEnable)
//					ADD_WIDGET_STRING("")
//					ADD_WIDGET_BOOL("Grab Start Camera", bDebugFakeGarageCamGrab1)
//					ADD_WIDGET_VECTOR_SLIDER("Coordinates", vDebugFakeGarageCamCoord1, -10000.0, 10000.0, 0.01)
//					ADD_WIDGET_VECTOR_SLIDER("Rotation", vDebugFakeGarageCamRot1, -180.0, 180.0, 1.0)
//					ADD_WIDGET_FLOAT_SLIDER("FOV", fDebugFakeGarageCamFov1, 1.0, 130.0, 0.1)
//					ADD_WIDGET_STRING("")
//					ADD_WIDGET_BOOL("Grab End Camera", bDebugFakeGarageCamGrab2)
//					ADD_WIDGET_VECTOR_SLIDER("Coordinates", vDebugFakeGarageCamCoord2, -10000.0, 10000.0, 0.01)
//					ADD_WIDGET_VECTOR_SLIDER("Rotation", vDebugFakeGarageCamRot2, -180.0, 180.0, 1.0)
//					ADD_WIDGET_FLOAT_SLIDER("FOV", fDebugFakeGarageCamFov2, 1.0, 130.0, 0.1)
//					ADD_WIDGET_BOOL("Flip Rotation (if camera rotates the wrong way around)", bDebugFakeGarageCamRotateFlip)
//					ADD_WIDGET_STRING("")
//					ADD_WIDGET_INT_SLIDER("Pan Duration", iDebugFakeGarageCamDuration, 0, 60000, 1)
//					ADD_WIDGET_FLOAT_SLIDER("Shake", fDebugFakeGarageCamShake, 0.0, 1.0, 0.01)
//					START_NEW_WIDGET_COMBO()
//						ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
//						ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
//						ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
//						ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
//					STOP_WIDGET_COMBO("GRAPH_TYPE", camGraphTypeFakeGarage)
//				STOP_WIDGET_GROUP()
//				ADD_WIDGET_STRING("")
//				ADD_WIDGET_BOOL("Debug Lines", bDebugFakeGarageRender)
//				ADD_WIDGET_BOOL("Ghost Cars", bDebugFakeGarageGhostCar)
//				ADD_WIDGET_BOOL("Release Player Car", bDebugFakeGarageUnfreezeCar)
//				ADD_WIDGET_STRING("")
//				ADD_WIDGET_BOOL("Output Script", bDebugFakeGarageOutput)			
//			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC MAINTAIN_VEHICLE_CONTROL_WIDGETS()
		
		IF bTrackImpoundVehDebug
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehTemp)
				AND IS_VEHICLE_DRIVEABLE(vehTemp)
					TRACK_VEHICLE_FOR_IMPOUND(vehTemp)
				ENDIF
			ENDIF
			bTrackImpoundVehDebug = FALSE
		ENDIF
		IF bSetMissionVehGenDebug
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehTemp)
				AND IS_VEHICLE_DRIVEABLE(vehTemp)
					SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, GET_ENTITY_COORDS(vehTemp), GET_ENTITY_HEADING(vehTemp))
				ENDIF
			ENDIF
			bSetMissionVehGenDebug = FALSE
		ENDIF
		IF bSetMissionVehGenDebug2
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehTemp)
				AND IS_VEHICLE_DRIVEABLE(vehTemp)
					SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<91.9203, -744.7191, 44.7552>>, 210.1958)
				ENDIF
			ENDIF
			bSetMissionVehGenDebug2 = FALSE
		ENDIF
		
		IF bCreateImpoundVehDebug
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(iImpoundVehSlotDebug, GET_CURRENT_PLAYER_PED_ENUM())
				OR CREATE_IMPOUND_VEHICLE(vehImpound, iImpoundVehSlotDebug, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), TRUE)
					IF DOES_ENTITY_EXIST(vehImpound)
					AND IS_VEHICLE_DRIVEABLE(vehImpound)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehImpound)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehImpound)
					ENDIF
					bCreateImpoundVehDebug = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bClearImpoundVehDebug
			CLEAR_VEHICLE_IMPOUND_SLOT(iImpoundVehSlotDebug)
			bClearImpoundVehDebug = FALSE
		ENDIF
		
		IF bOutputImpoundVehDebug
			PRINTLN("IMPOUND VEHICLE INFO")
			PRINTLN("...char = ", iImpoundVehSlotChar)
			PRINTLN("...slot = ", iImpoundVehSlotDebug)
			PRINTLN("...model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_savedGlobals.sVehicleGenData.sImpoundVehicles[iImpoundVehSlotChar][iImpoundVehSlotDebug].eModel))
			PRINTLN("...plate = ", g_savedGlobals.sVehicleGenData.sImpoundVehicles[iImpoundVehSlotChar][iImpoundVehSlotDebug].tlPlateText)
			bOutputImpoundVehDebug = FALSE
		ENDIF
		
		IF bPlayTutorial
			sGarageData.iPurchaseControl = 2
			
			iContextCounter = 0
			sGarageData.iTimer = GET_GAME_TIMER()
			RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
			
			bDoTutorial = TRUE
			bPlayTutorial = FALSE
		ENDIF
		
		IF rag_vehgen_garage_scene.bEnableTool
			Private_EDIT_VEHGEN_GARAGE_SCENE(rag_vehgen_garage_scene, INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, rag_vehgen_garage_scene.iLaunchSceneID), &Debug_RUN_VEHGEN_GARAGE_SCENE, iVehgen_garage_scene_type, bSnapMarkerToPoint)	//, objFakeGarageDoor)
			rag_vehgen_garage_scene.bEnableTool = FALSE
			b_DebugGivePlayerAllGarages = FALSE
		ENDIF
		
//		//fake garage debug
//		IF bDebugFakeGarageInterp
//			BOOL bCleanupInterp = FALSE
//			IF fDebugFakeGarageStartTime = -1
//				fDebugFakeGarageStartTime = TO_FLOAT(GET_GAME_TIMER())
//				
//				IF bDebugFakeGarageCamEnable
//					IF NOT DOES_CAM_EXIST(camFakeGarage)
//						camFakeGarage = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vDebugFakeGarageCamCoord1, vDebugFakeGarageCamRot1, fDebugFakeGarageCamFov1)
//						//SET_CAM_PARAMS(camFakeGarage, vDebugFakeGarageCamCoord2, vDebugFakeGarageCamRot2, fDebugFakeGarageCamFov2, iDebugFakeGarageCamDuration, INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage), INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage))
//						SHAKE_CAM(camFakeGarage, "HAND_SHAKE", fDebugFakeGarageCamShake)
//						SET_CAM_SHAKE_AMPLITUDE(camFakeGarage, fDebugFakeGarageCamShake)
//						SET_CAM_ACTIVE(camFakeGarage, TRUE)
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					ELSE
//						SET_CAM_PARAMS(camFakeGarage, vDebugFakeGarageCamCoord1, vDebugFakeGarageCamRot1, fDebugFakeGarageCamFov1)
//						//SET_CAM_PARAMS(camFakeGarage, vDebugFakeGarageCamCoord2, vDebugFakeGarageCamRot2, fDebugFakeGarageCamFov2, iDebugFakeGarageCamDuration, INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage), INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage))
//						SHAKE_CAM(camFakeGarage, "HAND_SHAKE", fDebugFakeGarageCamShake)
//						SET_CAM_SHAKE_AMPLITUDE(camFakeGarage, fDebugFakeGarageCamShake)
//						SET_CAM_ACTIVE(camFakeGarage, TRUE)
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					ENDIF
//				ENDIF
//			ELIF TO_FLOAT(GET_GAME_TIMER()) > fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration)
//			AND bDebugFakeGaragePause = FALSE
//			AND TO_FLOAT(GET_GAME_TIMER()) > fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageCamDuration)
//				bCleanupInterp = TRUE
//			ELSE
//	//			SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK
//	//			SET_ADDITIONAL_ROTATION_FOR_RECORDED_VEHICLE_PLAYBACK
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//					IF NOT IS_ENTITY_DEAD(vehIndex)
//						SET_ENTITY_COORDS_NO_OFFSET(vehIndex, GET_INTERP_POINT_VECTOR(vDebugFakeGaragePoint1, vDebugFakeGaragePoint2, fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration), CLAMP(TO_FLOAT(GET_GAME_TIMER()), fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration))))
//						SET_ENTITY_ROTATION(vehIndex, GET_INTERP_POINT_VECTOR(vDebugFakeGarageRotation1, vDebugFakeGarageRotation2, fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration), CLAMP(TO_FLOAT(GET_GAME_TIMER()), fDebugFakeGarageStartTime, fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration))))
//						SET_ENTITY_COLLISION(vehIndex, FALSE)
//						FREEZE_ENTITY_POSITION(vehIndex, TRUE)
//						SET_VEHICLE_ENGINE_ON(vehIndex, TRUE, TRUE)
//						PRINTLN("VEH_GEN_CONTROLLER: Setting car position in debug")
//						
//						IF DOES_CAM_EXIST(camFakeGarage)
//							FLOAT fPhase
//							
//							IF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_LINEAR
//								fPhase = (1.0 / TO_FLOAT(iDebugFakeGarageCamDuration)) * CLAMP(TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGarageStartTime, 0.0, TO_FLOAT(iDebugFakeGarageCamDuration))
//							ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_SIN_ACCEL_DECEL
//								fPhase = GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fDebugFakeGaragePhase)
//							ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_ACCEL
//								fPhase = GET_GRAPH_TYPE_ACCEL(fDebugFakeGaragePhase)
//							ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_DECEL
//								fPhase = GET_GRAPH_TYPE_DECEL(fDebugFakeGaragePhase)
//							ENDIF
//							
//							SET_CAM_ACTIVE(camFakeGarage, TRUE)
//							SET_CAM_PARAMS(camFakeGarage, 
//											GET_INTERP_POINT_VECTOR(vDebugFakeGarageCamCoord1, vDebugFakeGarageCamCoord2, 0.0, 1.0, fPhase), 
//											GET_INTERP_POINT_VECTOR(vDebugFakeGarageCamRot1, vDebugFakeGarageCamRot2, 0.0, 1.0, fPhase), 
//											GET_INTERP_POINT_FLOAT(fDebugFakeGarageCamFov1, fDebugFakeGarageCamFov2, 0.0, 1.0, fPhase))
//						ENDIF
//					ELSE
//						bCleanupInterp = TRUE
//					ENDIF
//				ELSE
//					bCleanupInterp = TRUE
//				ENDIF
//			ENDIF
//			IF bCleanupInterp
//				fDebugFakeGarageStartTime = -1
//				bDebugFakeGarageInterp = FALSE
//			ENDIF
//		ENDIF
//		IF bDebugFakeGaragePause
//			fDebugFakeGarageStartTime = TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGaragePauseTime
//			IF fDebugFakeGaragePauseTime <> (TO_FLOAT(iDebugFakeGarageDuration) * fDebugFakeGaragePhase)
//				IF iDebugFakeGarageDuration >= iDebugFakeGarageCamDuration
//					fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageDuration) * fDebugFakeGaragePhase)
//				ELSE
//					fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageCamDuration) * fDebugFakeGaragePhase)
//				ENDIF
//			ENDIF
//		ELSE
//			IF iDebugFakeGarageDuration >= iDebugFakeGarageCamDuration
//				fDebugFakeGaragePhase = (1.0 / TO_FLOAT(iDebugFakeGarageDuration)) * CLAMP(TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGarageStartTime, 0.0, TO_FLOAT(iDebugFakeGarageDuration))
//			ELSE
//				fDebugFakeGaragePhase = (1.0 / TO_FLOAT(iDebugFakeGarageCamDuration)) * CLAMP(TO_FLOAT(GET_GAME_TIMER()) - fDebugFakeGarageStartTime, 0.0, TO_FLOAT(iDebugFakeGarageCamDuration))
//			ENDIF
//			IF GET_GAME_TIMER() < fDebugFakeGarageStartTime + TO_FLOAT(iDebugFakeGarageDuration)
//				IF iDebugFakeGarageDuration >= iDebugFakeGarageCamDuration
//					fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageDuration) * fDebugFakeGaragePhase)
//				ELSE
//					fDebugFakeGaragePauseTime = (TO_FLOAT(iDebugFakeGarageCamDuration) * fDebugFakeGaragePhase)
//				ENDIF
//			ENDIF
//		ENDIF
//		IF bDebugFakeGarageLoop
//			IF bDebugFakeGarageInterp = FALSE
//				bDebugFakeGarageInterp = TRUE
//			ENDIF
//		ENDIF
//		IF bDebugFakeGarageSetCar1
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				IF NOT IS_ENTITY_DEAD(vehIndex)
//					vDebugFakeGaragePoint1 = GET_ENTITY_COORDS(vehIndex)
//					vDebugFakeGarageRotation1 = GET_ENTITY_ROTATION(vehIndex)
//				ENDIF
//			ENDIF
//			
//			bDebugFakeGarageSetCar1 = FALSE
//		ENDIF
//		IF bDebugFakeGarageSetCar2
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				IF NOT IS_ENTITY_DEAD(vehIndex)
//					vDebugFakeGaragePoint2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIndex, <<0.0, 10.0, 0.0>>)
//					vDebugFakeGarageRotation2 = GET_ENTITY_ROTATION(vehIndex)
//				ENDIF
//			ENDIF
//			
//			bDebugFakeGarageSetCar2 = FALSE
//		ENDIF
//		IF bDebugFakeGarageRender
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//			
//			DRAW_DEBUG_SPHERE(vDebugFakeGaragePoint1, 0.5)
//			DRAW_DEBUG_SPHERE(vDebugFakeGaragePoint2, 0.5)
//			DRAW_DEBUG_LINE(vDebugFakeGaragePoint1, vDebugFakeGaragePoint2)
//		ENDIF
//		IF bDebugFakeGarageGhostCar
//			BOOL bCleanupGhostCar = FALSE
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehIndex)
//				IF NOT IS_ENTITY_DEAD(vehIndex)
//					IF NOT DOES_ENTITY_EXIST(vehGhost1)
//					OR NOT DOES_ENTITY_EXIST(vehGhost2)
//						REQUEST_MODEL(vehModel)
//						IF HAS_MODEL_LOADED(vehModel)
//							IF NOT DOES_ENTITY_EXIST(vehGhost1)
//								vehGhost1 = CREATE_VEHICLE(vehModel, vDebugFakeGaragePoint1, vDebugFakeGarageRotation1.Z)
//								FREEZE_ENTITY_POSITION(vehGhost1, TRUE)
//								SET_ENTITY_COLLISION(vehGhost1, FALSE)
//								SET_ENTITY_INVINCIBLE(vehGhost1, TRUE)
//							ENDIF
//							IF NOT DOES_ENTITY_EXIST(vehGhost2)
//								vehGhost2 = CREATE_VEHICLE(vehModel, vDebugFakeGaragePoint2, vDebugFakeGarageRotation2.Z)
//								FREEZE_ENTITY_POSITION(vehGhost2, TRUE)
//								SET_ENTITY_COLLISION(vehGhost2, FALSE)
//								SET_ENTITY_INVINCIBLE(vehGhost2, TRUE)
//							ENDIF
//							SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
//						ENDIF
//					ELSE
//						IF GET_ENTITY_MODEL(vehGhost1) = vehModel
//							SET_ENTITY_COORDS_NO_OFFSET(vehGhost1, vDebugFakeGaragePoint1)
//							SET_ENTITY_ROTATION(vehGhost1, vDebugFakeGarageRotation1)
//						ELSE
//							bCleanupGhostCar = TRUE
//						ENDIF
//						
//						IF GET_ENTITY_MODEL(vehGhost2) = vehModel
//							SET_ENTITY_COORDS_NO_OFFSET(vehGhost2, vDebugFakeGaragePoint2)
//							SET_ENTITY_ROTATION(vehGhost2, vDebugFakeGarageRotation2)
//						ELSE
//							bCleanupGhostCar = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			IF bCleanupGhostCar
//				IF DOES_ENTITY_EXIST(vehGhost1)
//					DELETE_VEHICLE(vehGhost1)
//				ENDIF
//				IF DOES_ENTITY_EXIST(vehGhost2)
//					DELETE_VEHICLE(vehGhost2)
//				ENDIF
//			ENDIF
//		ELSE
//			IF DOES_ENTITY_EXIST(vehGhost1)
//				DELETE_VEHICLE(vehGhost1)
//			ENDIF
//			IF DOES_ENTITY_EXIST(vehGhost2)
//				DELETE_VEHICLE(vehGhost2)
//			ENDIF
//		ENDIF
//		IF bDebugFakeGarageOutput
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("--- VEH GEN CONTROLLER  ---")
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("vStartPoint = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGaragePoint1)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("vStartRotation = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGarageRotation1)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("vEndPoint = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGaragePoint2)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("vEndRotation = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGarageRotation2)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("fDuration = ")SAVE_INT_TO_DEBUG_FILE(iDebugFakeGarageDuration)SAVE_NEWLINE_TO_DEBUG_FILE()
//			
//			IF bDebugFakeGarageCamEnable
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamCoord1 = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGarageCamCoord1)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamRot1 = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGarageCamRot1)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fCamFov1 = ")SAVE_FLOAT_TO_DEBUG_FILE(fDebugFakeGarageCamFov1)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamCoord2 = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGarageCamCoord2)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.vCamRot2 = ")SAVE_VECTOR_TO_DEBUG_FILE(vDebugFakeGarageCamRot2)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fCamFov2 = ")SAVE_FLOAT_TO_DEBUG_FILE(fDebugFakeGarageCamFov2)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fDuration = ")SAVE_INT_TO_DEBUG_FILE(iDebugFakeGarageCamDuration)SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("fakeGarageDriveData.fCamShake = ")SAVE_FLOAT_TO_DEBUG_FILE(fDebugFakeGarageCamShake)SAVE_NEWLINE_TO_DEBUG_FILE()
//				
//				IF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_LINEAR
//					SAVE_STRING_TO_DEBUG_FILE("camGraphType = GRAPH_TYPE_LINEAR")
//				ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_SIN_ACCEL_DECEL
//					SAVE_STRING_TO_DEBUG_FILE("camGraphType = GRAPH_TYPE_SIN_ACCEL_DECEL")
//				ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_ACCEL
//					SAVE_STRING_TO_DEBUG_FILE("camGraphType = GRAPH_TYPE_ACCEL")
//				ELIF INT_TO_ENUM(CAMERA_GRAPH_TYPE, camGraphTypeFakeGarage) = GRAPH_TYPE_DECEL
//					SAVE_STRING_TO_DEBUG_FILE("camGraphType = GRAPH_TYPE_DECEL")
//				ENDIF
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//			ENDIF
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("-----------------------------")
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			
//			bDebugFakeGarageOutput = FALSE
//		ENDIF
//		IF bDebugFakeGarageCamGrab1
//			vDebugFakeGarageCamCoord1 = GET_FINAL_RENDERED_CAM_COORD()
//			vDebugFakeGarageCamRot1 = GET_FINAL_RENDERED_CAM_ROT()
//			fDebugFakeGarageCamFov1 = GET_FINAL_RENDERED_CAM_FOV()
//			
//			bDebugFakeGarageCamGrab1 = FALSE
//		ENDIF
//		IF bDebugFakeGarageCamGrab2
//			vDebugFakeGarageCamCoord2 = GET_FINAL_RENDERED_CAM_COORD()
//			vDebugFakeGarageCamRot2 = GET_FINAL_RENDERED_CAM_ROT()
//			fDebugFakeGarageCamFov2 = GET_FINAL_RENDERED_CAM_FOV()
//			
//			bDebugFakeGarageCamGrab2 = FALSE
//		ENDIF
//		IF bDebugFakeGarageCamRotateFlip
//			IF vDebugFakeGarageCamRot2.Z < vDebugFakeGarageCamRot1.Z
//				vDebugFakeGarageCamRot2.Z += 360.0
//			ELSE
//				vDebugFakeGarageCamRot2.Z -= 360.0
//			ENDIF
//			
//			bDebugFakeGarageCamRotateFlip = FALSE
//		ENDIF
//		IF bDebugFakeGarageUnfreezeCar
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				IF NOT IS_ENTITY_DEAD(vehIndex)
//					SET_ENTITY_COORDS_NO_OFFSET(vehIndex, vDebugFakeGaragePoint1)
//					SET_ENTITY_ROTATION(vehIndex, vDebugFakeGarageRotation1)
//					SET_ENTITY_COLLISION(vehIndex, TRUE)
//					FREEZE_ENTITY_POSITION(vehIndex, FALSE)
//					SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex)
//				ENDIF
//			ENDIF
//			
//			IF DOES_CAM_EXIST(camFakeGarage)
//				SET_CAM_ACTIVE(camFakeGarage, FALSE)
//				STOP_CAM_SHAKING(camFakeGarage, TRUE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				DESTROY_CAM(camFakeGarage)
//			ENDIF
//			
//			bDebugFakeGarageUnfreezeCar = FALSE
//		ENDIF
//		IF NOT bDebugFakeGarageCamEnable
//			IF DOES_CAM_EXIST(camFakeGarage)
//				SET_CAM_ACTIVE(camFakeGarage, FALSE)
//				STOP_CAM_SHAKING(camFakeGarage, TRUE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				DESTROY_CAM(camFakeGarage)
//			ENDIF
//		ENDIF
		
		IF NOT b_DebugGivePlayerAllGarages
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_DebugGivePlayerAllGarages")
				SAVE_STRING_TO_DEBUG_FILE("b_DebugGivePlayerAllGarages\n")
				VEHICLE_GEN_NAME_ENUM eVehGen
				REPEAT VEHGEN_TREV1_SMASHED_TRAILER eVehGen
					SET_VEHICLE_GEN_AVAILABLE(eVehGen, TRUE)
					SET_VEHICLE_GEN_SAVED_FLAG_STATE(eVehGen, VEHGEN_S_FLAG_ACQUIRED, TRUE)
					
					IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eVehGen])
						REMOVE_BLIP(g_sVehicleGenNSData.blipID[eVehGen])
					ENDIF
				ENDREPEAT
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				g_bLastGenPlayer = TRUE
				g_savedGlobals.sCountryRaceData.bStallionUnlocked = TRUE
				g_savedGlobals.sCountryRaceData.bGauntletUnlocked = TRUE
				g_savedGlobals.sCountryRaceData.bDominatorUnlocked = TRUE
				g_savedGlobals.sCountryRaceData.bBuffaloUnlocked = TRUE
				
				b_DebugGivePlayerAllGarages = TRUE
			ENDIF
		ENDIF
	ENDPROC
	
/*
	BOOL bHandOverToMissionVehGen
	BOOL bWarp[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bDelete[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bMakeAvailable[NUMBER_OF_VEHICLES_TO_GEN]
	BOOL bMakeUnavailable[NUMBER_OF_VEHICLES_TO_GEN]
	VEHICLE_GEN_DATA_STRUCT sTempDataForWidget
	PROC SETUP_VEHICLE_CONTROL_WIDGETS()
		START_WIDGET_GROUP("Vehicle Gen Controller")
			ADD_WIDGET_BOOL("Blocked on mission", g_sVehicleGenNSData.bDisabledForMissions)
			ADD_WIDGET_BOOL("Handover to mision veh gen", bHandOverToMissionVehGen)
			INT i
			REPEAT NUMBER_OF_VEHICLES_TO_GEN i
				IF GET_VEHICLE_GEN_DATA(sTempDataForWidget, INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, i))
					ADD_BIT_FIELD_WIDGET(sTempDataForWidget.dbg_name, g_savedGlobals.sVehicleGenData.iProperties[i])
					ADD_WIDGET_BOOL("Warp", bWarp[i])
					ADD_WIDGET_BOOL("Delete", bDelete[i])
					ADD_WIDGET_BOOL("Make Available", bMakeAvailable[i])
					ADD_WIDGET_BOOL("Make Unavailable", bMakeUnavailable[i])
					ADD_WIDGET_BOOL("Leave area before creating", g_sVehicleGenNSData.bLeaveAreaBeforeCreating[i])
				ENDIF
			ENDREPEAT
		STOP_WIDGET_GROUP()
	ENDPROC

	PROC MAINTAIN_VEHICLE_CONTROL_WIDGETS()
		INT i
		REPEAT NUMBER_OF_VEHICLES_TO_GEN i
			IF bWarp[i]
				IF GET_VEHICLE_GEN_DATA(sTempDataForWidget, INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, i))
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), sTempDataForWidget.coords-<<0.0, 3.0, 0.0>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
						WAIT(0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					ENDIF
				ENDIF
				bWarp[i] = FALSE
			ENDIF
			IF bDelete[i]
				DELETE_VEHICLE_GEN_VEHICLE(INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, i))
				bDelete[i] = FALSE
			ENDIF
			IF bMakeAvailable[i]
				SET_VEHICLE_GEN_AVAILABLE(INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, i), TRUE)
				bMakeAvailable[i] = FALSE
			ENDIF
			IF bMakeUnavailable[i]
				SET_VEHICLE_GEN_AVAILABLE(INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, i), FALSE)
				bMakeUnavailable[i] = FALSE
			ENDIF
		ENDREPEAT
		/*IF bUpdateDynamicVeh
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(tempVeh)
				AND IS_VEHICLE_DRIVEABLE(tempVeh)
					VEHICLE_SETUP_STRUCT sTempVehStruct
					GET_VEHICLE_SETUP(tempVeh, sTempVehStruct)
					UPDATE_DYNAMIC_VEHICLE_GEN_DATA(VEHGEN_DYNAMIC_TEST, sTempVehStruct, GET_ENTITY_COORDS(tempVeh), GET_ENTITY_HEADING(tempVeh))
					SET_VEHICLE_GEN_VEHICLE(VEHGEN_DYNAMIC_TEST, tempVeh)
				ENDIF
			ENDIF
			
			bUpdateDynamicVeh = FALSE
		ENDIF*/
		/*IF bHandOverToMissionVehGen
			SET_MISSION_VEHICLE_GEN_VEHICLE(GET_PLAYERS_LAST_VEHICLE(), <<0.0,0.0,0.0>>, 0.0)
			bHandOverToMissionVehGen = FALSE
		ENDIF
	ENDPROC*/
#ENDIF

/// PURPOSE: Prints out debug text with vehicle gen prefix.
PROC PRINT_VEHGEN_DBG(STRING sDebugString)
	IF NOT Is_String_Null_Or_Empty(sDebugString)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n[")PRINTSTRING(sData.dbg_name)PRINTSTRING("] ")PRINTSTRING(sDebugString)PRINTNL()
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Prints out debug text with vehicle gen prefix.
PROC PRINT_VEHGEN_DBG_WITH_FLOAT(STRING sDebugString, FLOAT f)
	IF NOT Is_String_Null_Or_Empty(sDebugString)
	
		// just to stop it crying during a release build
		IF (f <> 0.0) 
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n[")PRINTSTRING(sData.dbg_name)PRINTSTRING("] ")PRINTSTRING(sDebugString)PRINTSTRING(" ")PRINTFLOAT(f)PRINTNL()
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Prints out debug text with vehicle gen prefix.
PROC PRINT_VEHGEN_DBG_WITH_INT(STRING sDebugString, INT i)
	IF NOT Is_String_Null_Or_Empty(sDebugString)
	
		// just to stop it crying during a release build
		IF (i <> 0) 
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n[")PRINTSTRING(sData.dbg_name)PRINTSTRING("] ")PRINTSTRING(sDebugString)PRINTSTRING(" ")PRINTINT(i)PRINTNL()
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Prints out debug text with vehicle gen prefix.
PROC PRINT_VEHGEN_DBG_WITH_VECTOR(STRING sDebugString, VECTOR v)
	IF NOT Is_String_Null_Or_Empty(sDebugString)
	
		// just to stop it crying during a release build
		IF (v.x <> 0.0) 
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTSTRING("\n[")PRINTSTRING(sData.dbg_name)PRINTSTRING("] ")PRINTSTRING(sDebugString)PRINTSTRING(" ")PRINTVECTOR(v)PRINTNL()
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Determines if the controller is allowed to run
FUNC BOOL IS_CONTROLLER_SAFE_TO_RUN()
	// Debug flag not set
	#IF IS_DEBUG_BUILD
		IF NOT g_savedGlobals.sFlow.isGameflowActive
		AND NOT g_bVehicleGenAvailableInDebug
		AND NOT IS_REPEAT_PLAY_ACTIVE()
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// No issues found so assume it is safe to run
	RETURN TRUE
ENDFUNC

/// PURPOSE: Marks the vehicle model as no longer needed if it is has been requested
///    NOTE: The sData struct must be set using GET_VEHICLE_GEN_DATA(...) before this function is called.
PROC CLEANUP_VEHICLE_MODEL(VEHICLE_GEN_NAME_ENUM eName)
	IF eVehicleModel[eName] <> DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(eVehicleModel[eName])
		eVehicleModel[eName] = DUMMY_MODEL_FOR_SCRIPT
	ENDIF
ENDPROC

/// PURPOSE: Marks the vehicle model as no longer needed if it is has been requested
///    NOTE: The sData struct must be set using GET_VEHICLE_GEN_DATA(...) before this function is called.
PROC CLEANUP_ONE_TIME_VEH_GEN(VEHICLE_GEN_NAME_ENUM eName)
	// Mark as unavailable
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_AVAILABLE_ONCE))
		SET_VEHICLE_GEN_AVAILABLE(eName, FALSE)
	ENDIF
ENDPROC

/// PURPOSE: Apply damages to a vehicle when we create it
PROC APPLY_CUSTOM_VEHICLE_DAMAGE(VEHICLE_INDEX vehID, VEHICLE_GEN_NAME_ENUM eName)
	IF DOES_ENTITY_EXIST(vehID)
	AND IS_VEHICLE_DRIVEABLE(vehID)
		SWITCH eName
			CASE VEHGEN_BJXL_CRASH_POST_ARM3
				SET_VEHICLE_DAMAGE(vehID, <<-0.84, 2.21, 0.22>>, 100.0, 400.0, TRUE)
				SET_VEHICLE_DAMAGE(vehID, <<0.67, 2.12, -0.06>>, 100.0, 400.0, TRUE)
				SET_VEHICLE_DAMAGE(vehID, <<0.05, 1.97, 0.2>>, 100.0, 400.0, TRUE)
			BREAK
			case VEHGEN_TREV1_SMASHED_TRAILER
				SET_VEHICLE_EXTRA(vehID,6,FALSE) 
                SET_VEHICLE_EXTRA(vehID,1,TRUE)
			break
		ENDSWITCH
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_SPECIAL_VEHICLES()
	INT iTotalVehicles = 0
//	INT iVeh
//	scrShopVehicleData vehicleData
	
//	///////////////////////////////////////
//	///     COLLECTORS EDITION
//	IF IS_COLLECTORS_EDITION_GAME()
//		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
//			iTotalVehicles++//HOTKNIFE
//			iTotalVehicles++//CARBONRS
//		ENDIF
//		IF NETWORK_IS_GAME_IN_PROGRESS()
//			iTotalVehicles++//KHAMELION
//		ENDIF
//	ENDIF
	
	///////////////////////////////////////
	///     SOCIAL CLUB
	IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE)
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			iTotalVehicles++//ELEGY2
		ENDIF
	ENDIF
	
	///////////////////////////////////////
	///     SPACE DOCKER
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[ENUM_TO_INT(RC_OMEGA_2)].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			iTotalVehicles++//DUNE2
		ENDIF
	ENDIF
	
	///////////////////////////////////////
	///     CG TO NG
	IF IS_LAST_GEN_PLAYER()
	AND NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_RANDOM_EVENT_COMPLETE(RE_MONKEYPHOTO)
		AND NOT IS_TEXT_MESSAGE_REGISTERED(TEXT_MONKEY_CAR_UNLOCK)
			iTotalVehicles++//BLISTA3
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bStallionUnlocked
			iTotalVehicles++//STALION2
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bGauntletUnlocked
			iTotalVehicles++//GAUNTLET2
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bDominatorUnlocked
			iTotalVehicles++//DOMINATOR2
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bBuffaloUnlocked
			iTotalVehicles++//BUFFALO3
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bMarshallUnlocked
			iTotalVehicles++//MARSHALL
		ENDIF
	ENDIF
	
	///////////////////////////////////////
	///     DLC
//	REPEAT GET_NUM_DLC_VEHICLES() iVeh
//		IF GET_DLC_VEHICLE_DATA(iVeh, vehicleData)
//			IF NOT IS_CONTENT_ITEM_LOCKED(vehicleData.m_lockHash)
//			AND NOT IS_DLC_VEHICLE_LOCKED_BY_SCRIPT(GET_DLC_VEHICLE_MODEL(iVeh))
//				IF IS_THIS_MODEL_A_BIKE(GET_DLC_VEHICLE_MODEL(iVeh))
//				OR IS_THIS_MODEL_A_CAR(GET_DLC_VEHICLE_MODEL(iVeh))
//					// These have been added to the websites.
//				ELSE
//					iTotalVehicles++
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	RETURN iTotalVehicles
ENDFUNC

FUNC MODEL_NAMES GET_SPECIAL_VEHICLE_MODEL(INT iSlot)

	INT iModel//, iVeh
	MODEL_NAMES eVehModels[128]		//[10]
//	scrShopVehicleData vehicleData
	
//	///////////////////////////////////////
//	///     COLLECTORS EDITION
//	IF IS_COLLECTORS_EDITION_GAME()
//		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
//			IF iModel < COUNT_OF(eVehModels)-1
//				eVehModels[iModel] = HOTKNIFE
//				iModel++
//			ENDIF
//			
//			IF iModel < COUNT_OF(eVehModels)-1
//				eVehModels[iModel] = CARBONRS
//				iModel++
//			ENDIF
//		ENDIF
//		IF NETWORK_IS_GAME_IN_PROGRESS()
//			IF iModel < COUNT_OF(eVehModels)-1
//				eVehModels[iModel] = KHAMELION
//				iModel++
//			ENDIF
//		ENDIF
//	ENDIF
	
	///////////////////////////////////////
	///     SOCIAL CLUB
	IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE)
		IF iModel < COUNT_OF(eVehModels)-1
			eVehModels[iModel] = ELEGY2
			iModel++
		ENDIF
	ENDIF
	
	///////////////////////////////////////
	///     SPACE DOCKER
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[ENUM_TO_INT(RC_OMEGA_2)].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		IF iModel < COUNT_OF(eVehModels)-1
			eVehModels[iModel] = DUNE2
			iModel++
		ENDIF
	ENDIF
	
	///////////////////////////////////////
	///     CG TO NG
	IF IS_LAST_GEN_PLAYER()
	AND NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_RANDOM_EVENT_COMPLETE(RE_MONKEYPHOTO)
		AND NOT IS_TEXT_MESSAGE_REGISTERED(TEXT_MONKEY_CAR_UNLOCK)
			eVehModels[iModel] = BLISTA3
			iModel++
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bStallionUnlocked
			eVehModels[iModel] = STALION2
			iModel++
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bGauntletUnlocked
			eVehModels[iModel] = GAUNTLET2
			iModel++
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bDominatorUnlocked
			eVehModels[iModel] = DOMINATOR2
			iModel++
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bBuffaloUnlocked
			eVehModels[iModel] = BUFFALO3
			iModel++
		ENDIF
		IF g_savedGlobals.sCountryRaceData.bMarshallUnlocked
			eVehModels[iModel] = MARSHALL
			iModel++
		ENDIF
	ENDIF
	
//	///////////////////////////////////////
//	///     DLC
//	REPEAT GET_NUM_DLC_VEHICLES() iVeh
//		IF GET_DLC_VEHICLE_DATA(iVeh, vehicleData)
//			IF NOT IS_CONTENT_ITEM_LOCKED(vehicleData.m_lockHash)
//			AND NOT IS_DLC_VEHICLE_LOCKED_BY_SCRIPT(GET_DLC_VEHICLE_MODEL(iVeh))
//				IF iModel < COUNT_OF(eVehModels)-1
//					IF IS_THIS_MODEL_A_BIKE(GET_DLC_VEHICLE_MODEL(iVeh))
//					OR IS_THIS_MODEL_A_CAR(GET_DLC_VEHICLE_MODEL(iVeh))
//						// These have been added to the websites.
//					ELSE
//						eVehModels[iModel] = GET_DLC_VEHICLE_MODEL(iVeh)
//						iModel++
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
	
	IF iSlot >= 0 AND iSlot < iModel
		RETURN eVehModels[iSlot]
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL HAS_SPAWN_VEHICLE_GOT_CUSTOM_SETUP(MODEL_NAMES eModel)
	SWITCH eModel
		CASE MARSHALL
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAME_VEHICLE_MODEL_NEARBY_PLAYER(MODEL_NAMES eModel, INT iLiveryCheck = -1)
	VEHICLE_INDEX nearbyVehicles[50]
	INT iVeh
	INT iVehicleCount = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehicles)
	REPEAT iVehicleCount iVeh
		IF DOES_ENTITY_EXIST(nearbyVehicles[iVeh])
		AND IS_VEHICLE_DRIVEABLE(nearbyVehicles[iVeh])
		AND GET_ENTITY_MODEL(nearbyVehicles[iVeh]) = eModel
			IF iLiveryCheck = -1 OR GET_VEHICLE_LIVERY(nearbyVehicles[iVeh]) = iLiveryCheck
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SAME_VEHICLE_MODEL_NEARBY_VEHGEN(MODEL_NAMES eModel, INT iLiveryCheck = -1)
	INT iFlags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED|VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING|VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
	VEHICLE_INDEX nearbyVehicle = GET_CLOSEST_VEHICLE(sData.coords, VEHICLE_GEN_CREATE_RANGE, eModel, iFlags)
	IF DOES_ENTITY_EXIST(nearbyVehicle)
	AND IS_VEHICLE_DRIVEABLE(nearbyVehicle)
	AND GET_ENTITY_MODEL(nearbyVehicle) = eModel
		IF iLiveryCheck = -1 OR GET_VEHICLE_LIVERY(nearbyVehicle) = iLiveryCheck
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Don't spawn this specific vehicle gen with model.
FUNC BOOL BLOCK_SPECIFIC_VEHICLE_GEN_CREATION(VEHICLE_GEN_NAME_ENUM eName)
	IF eName = VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY
		// Don't spawn TPI frogger if one already exists. 
		IF sData.model = FROGGER2
			IF IS_SAME_VEHICLE_MODEL_NEARBY_PLAYER(sData.model, -1)//g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].iLivery)
			OR IS_SAME_VEHICLE_MODEL_NEARBY_VEHGEN(sData.model, -1)//g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].iLivery)
			OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("exile2")) > 0
			//B* 1816732: Check if FIB2 mission has been completed
			OR NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_2)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//B* 1892051
	IF eName = VEHGEN_MISSION_VEH
		IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY])
		AND NOT IS_ENTITY_DEAD(g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY])
		AND IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY])
			IF sData.model = GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY]) 
				INT c1,c2
				GET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY],c1,c2)
				IF c1 = sData.colour1 AND c2 = sData.colour2		
					DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
//	///////////////////////////////////////
//	///     COLLECTORS EDITION
//	IF NOT IS_COLLECTORS_EDITION_GAME()
//		IF sData.model = HOTKNIFE
//		OR sData.model = CARBONRS
//			RETURN TRUE
//		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks to see if the player is close to the vehicle !! model must be loaded !!
FUNC BOOL IS_PLAYER_IN_VEHICLE_BOUNDS(MODEL_NAMES eModel, VECTOR vCoords, BOOL bIgnoreForStartup = TRUE)

	IF bIgnoreForStartup
	AND IS_SCREEN_FADED_OUT()
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) > 0
		PRINT_VEHGEN_DBG("player is in vehicle bounds - \"startup_positioning\" is running.")
		
		RETURN FALSE
	ENDIF

	VECTOR vMin, vMax
	GET_MODEL_DIMENSIONS(eModel, vMin, vMax)
	
	FLOAT fLength = GET_DISTANCE_BETWEEN_COORDS(vMax, vMin, TRUE)
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoords) < (fLength*0.5)
		PRINT_VEHGEN_DBG_WITH_FLOAT("player is in vehicle bounds - fLength: ", fLength)
		PRINT_VEHGEN_DBG_WITH_FLOAT("player is in vehicle bounds - fDistance: ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoords))
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC KEEP_DEFAULT_VEHICLE_FLAGS_WITH_SPECIAL_CASE_MODELS(INT bitsetFrom, INT &bitsetTo, MODEL_NAMES model)
	INT bsMask = HIGHEST_INT
	//B* 2098125: Skip special bits for some vehicles; ADD NEW MODELS HERE
	SWITCH model
		CASE COQUETTE2
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_0)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_1)
			BREAK
		CASE KALAHARI	
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_0)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_1)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_2)
			BREAK
		CASE VOLTIC
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_0)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_1)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_2)
			BREAK
		CASE BANSHEE
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_0)
			BREAK
		CASE STALION
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_0)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_1)
			CLEAR_BIT(bsMask,VEHICLE_SETUP_FLAG_EXTRA_2)
			BREAK
		CASE CHINO
			CLEAR_BIT(bsMask, VEHICLE_SETUP_FLAG_EXTRA_0)
			CLEAR_BIT(bsMask, VEHICLE_SETUP_FLAG_EXTRA_1)
			CLEAR_BIT(bsMask, VEHICLE_SETUP_FLAG_EXTRA_2)
			BREAK
	ENDSWITCH
	INT bsNotMask = HIGHEST_INT - bsMask
	
	CDEBUG1LN(DEBUG_AMBIENT,"Combining vehicle flags (value)|(mask) ",bitsetFrom,"|",bsMask," with ",bitsetTo,"|",bsNotMask)
	//Get the masked bits from both bitsets
	bsMask = bsMask & bitsetFrom
	bsNotMask = bitsetTo & bsNotMask
	
	//Set the final bit
	bitsetTo = bsMask | bsNotMask
	
	CDEBUG1LN(DEBUG_AMBIENT, "Setting vehicle flags for model ",GET_MODEL_NAME_FOR_DEBUG(model)," - old|new ",bsMask,"|",bsNotMasK)
//	Old logic:
//	REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
//		IF IS_BIT_SET(bsMask, iExtra)
//			IF IS_BIT_SET(bsMask, GET_VEHICLE_EXTRA_FLAG_FOR_EXTRA_INDEX(iExtra+1))
//				SET_BIT(bitsetTo, GET_VEHICLE_EXTRA_FLAG_FOR_EXTRA_INDEX(iExtra+1))
//			ELSE
//				CLEAR_BIT(bitsetTo, GET_VEHICLE_EXTRA_FLAG_FOR_EXTRA_INDEX(iExtra+1))
//			ENDIF
//		ENDIF
//	ENDREPEAT	

ENDPROC

/// PURPOSE: Deals with creating/removing a vehicle
///    NOTE: The sData struct must be set using GET_VEHICLE_GEN_DATA(...) before this function is called.
PROC PROCESS_VEHICLE(VEHICLE_GEN_NAME_ENUM eName)
	FLOAT cleanupRange = VEHICLE_GEN_CLEANUP_RANGE
	FLOAT createRange = VEHICLE_GEN_CREATE_RANGE
	
	// Cleanup the vehicle model if we no longer require it
	IF NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
		CLEANUP_VEHICLE_MODEL(eName)
	ENDIF
	CLEAR_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
	
	FLOAT fPlayerHandoverDistance
	IF IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER)
		fPlayerHandoverDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vVehicleCoordsAtHandOver[eName])
	ELSE
		fPlayerHandoverDistance = 99999.99
	ENDIF
	
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))	
		IF sData.model = BLIMP
			cleanupRange = VEHICLE_GEN_CLEANUP_RANGE_BLIMP
			createRange = VEHICLE_GEN_CREATE_RANGE_BLIMP
		ELSE
			cleanupRange = VEHICLE_GEN_CLEANUP_RANGE_LONG
			createRange = VEHICLE_GEN_CREATE_RANGE_LONG
		ENDIF
	ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_SHORT_RANGE_DIST_CHECKS))
		cleanupRange = VEHICLE_GEN_CLEANUP_RANGE_SHORT
		createRange = VEHICLE_GEN_CREATE_RANGE_SHORT
	ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
	OR IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
	OR eName = VEHGEN_MISSION_VEH
	OR eName = VEHGEN_MISSION_VEH_FBI4_PREP
		cleanupRange = VEHICLE_GEN_CLEANUP_RANGE_HIGH_PRIORITY
		createRange = VEHICLE_GEN_CREATE_RANGE_HIGH_PRIORITY
	
	ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_WORLD_RANGE_DIST_CHECKS))
	AND (eCurrentPlayerPed = CHAR_MICHAEL AND sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
	OR eCurrentPlayerPed = CHAR_MICHAEL AND sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
	OR eCurrentPlayerPed = CHAR_MICHAEL AND sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR)
		cleanupRange = VEHICLE_GEN_CLEANUP_RANGE_WORLD
		createRange = VEHICLE_GEN_CREATE_RANGE_WORLD
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     Vehicle cleanup + updates
	///    
	
	IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleID[eName])
		IF IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleID[eName])
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// Last player character to use mission vehgen
				IF eName = VEHGEN_MISSION_VEH
					PED_INDEX piTemp = GET_PED_IN_VEHICLE_SEAT(g_sVehicleGenNSData.vehicleID[eName])
					IF NOT DOES_ENTITY_EXIST(piTemp)
						piTemp = GET_LAST_PED_IN_VEHICLE_SEAT(g_sVehicleGenNSData.vehicleID[eName])
					ENDIF
					enumCharacterList eTempPed = GET_PLAYER_PED_ENUM(piTemp)
					IF eTempPed != g_savedGlobals.sVehicleGenData.eLastCharToUseMissionVehGen
						IF IS_PLAYER_PED_PLAYABLE(eTempPed)
							PRINT_VEHGEN_DBG_WITH_INT("Updating last character to use vehicle gen", ENUM_TO_INT(eTempPed))
							g_savedGlobals.sVehicleGenData.eLastCharToUseMissionVehGen = eTempPed
						ENDIF
					ENDIF
				ENDIF
			
				// Owned by other script
				IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sVehicleGenNSData.vehicleID[eName])
					PRINT_VEHGEN_DBG("No longer needed: Vehicle owned by other script")
					IF eName = VEHGEN_MISSION_VEH
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
						SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
					ENDIF
					g_sVehicleGenNSData.vehicleID[eName] = NULL
					g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
					CLEANUP_ONE_TIME_VEH_GEN(eName)
					EXIT
				ENDIF
				
				// Player used vehicle (allow mission vehgens to be used)
				IF IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_USED_VEHICLE)
				AND NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLOCK_CLEANUP_ON_ENTRY))
				AND eName != VEHGEN_MISSION_VEH AND eName != VEHGEN_MISSION_VEH_FBI4_PREP
				
					PRINT_VEHGEN_DBG("No longer needed: Player used vehicle")
					IF eName = VEHGEN_MISSION_VEH
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
						SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
					ENDIF
					SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
					g_sVehicleGenNSData.vehicleID[eName] = NULL
					g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
					CLEANUP_ONE_TIME_VEH_GEN(eName)
					EXIT
				ENDIF
				
				// Player damaged vehicle (allow mission vehgens to be used)
				IF NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER)
				AND eName != VEHGEN_MISSION_VEH AND eName != VEHGEN_MISSION_VEH_FBI4_PREP
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sVehicleGenNSData.vehicleID[eName], PLAYER_PED_ID())
						PRINT_VEHGEN_DBG("No longer needed: Player damaged vehicle")
						IF eName = VEHGEN_MISSION_VEH
						AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
						AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
						AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
							SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
						ENDIF
						SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
						g_sVehicleGenNSData.vehicleID[eName] = NULL
						g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
						CLEANUP_ONE_TIME_VEH_GEN(eName)
						EXIT
					ENDIF
				ENDIF
				
				// Vehicle has been moved
				FLOAT fMoveDistCheck = 8.0
				IF eName = VEHGEN_MISSION_VEH
				OR eName = VEHGEN_MISSION_VEH_FBI4_PREP
				OR eName = VEHGEN_WEB_CAR_MICHAEL
				OR eName = VEHGEN_WEB_CAR_FRANKLIN
				OR eName = VEHGEN_WEB_CAR_TREVOR
				OR eName = VEHGEN_MICHAEL_GARAGE_1
				OR eName = VEHGEN_MICHAEL_GARAGE_2
				OR eName = VEHGEN_MICHAEL_GARAGE_3
				OR eName = VEHGEN_FRANKLIN_GARAGE_1
				OR eName = VEHGEN_FRANKLIN_GARAGE_2
				OR eName = VEHGEN_FRANKLIN_GARAGE_3
				OR eName = VEHGEN_TREVOR_GARAGE_1
				OR eName = VEHGEN_TREVOR_GARAGE_2
				OR eName = VEHGEN_TREVOR_GARAGE_3
					fMoveDistCheck = 20.0
				ENDIF
				
				IF (IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(g_sVehicleGenNSData.vehicleID[eName]), vVehicleCoordsAtHandOver[eName]) > fMoveDistCheck)
				OR (NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(g_sVehicleGenNSData.vehicleID[eName]), sData.coords) > fMoveDistCheck)
					PRINT_VEHGEN_DBG("No longer needed: Vehicle has been moved")
					PRINTLN("...", GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(g_sVehicleGenNSData.vehicleID[eName])))
					IF eName = VEHGEN_MISSION_VEH
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
						SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
					ENDIF
					SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
					g_sVehicleGenNSData.vehicleID[eName] = NULL
					g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
					CLEANUP_ONE_TIME_VEH_GEN(eName)
					EXIT
				ENDIF
				
				// Vehicle gen no longer available
				IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
					PRINT_VEHGEN_DBG("No longer needed: Vehicle gen no longer available")
					IF eName = VEHGEN_MISSION_VEH
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
						SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
					ENDIF
					SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
					g_sVehicleGenNSData.vehicleID[eName] = NULL
					g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
					CLEANUP_ONE_TIME_VEH_GEN(eName)
					EXIT
				ENDIF
				
				// Mission vehicle gen moved to garage.
				IF eName = VEHGEN_MISSION_VEH
					IF IS_VEHICLE_IN_PLAYERS_GARAGE(g_sVehicleGenNSData.vehicleID[eName], eCurrentPlayerPed, TRUE)
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
						PRINT_VEHGEN_DBG("No longer needed: Mission vehicle gen moved to players garage")
						IF GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
							SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
						ENDIF
						g_sVehicleGenNSData.vehicleID[eName] = NULL
						g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
						CLEANUP_ONE_TIME_VEH_GEN(eName)
						EXIT
					ENDIF
				ENDIF
				
				// Player out of range
				IF fDistToVehGen[eName] > cleanupRange
				AND (NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER) OR fPlayerHandoverDistance > cleanupRange)
					
					// [IMPOUND] Send mission vehicle gen to impound if it's not been used
					IF eName = VEHGEN_MISSION_VEH
						TIMEOFDAY eCurrentTOD = GET_CURRENT_TIMEOFDAY()
						TIMEOFDAY eHandOverTOD = g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp
						ADD_TIME_TO_TIMEOFDAY(eHandOverTOD, 0, 0, 17)
						IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(eCurrentTOD, eHandOverTOD)
							IF NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
							AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
							AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
								SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
							ENDIF
							TRACK_VEHICLE_FOR_IMPOUND(g_sVehicleGenNSData.vehicleID[eName], g_savedGlobals.sVehicleGenData.eLastCharToUseMissionVehGen)
							g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
							CLEANUP_ONE_TIME_VEH_GEN(eName)
							
						ELSE
							// [IMPOUND FIX 2] Copy the data incase we overwrite it when it's streamed out
							IF IS_VEHICLE_SAFE_FOR_IMPOUND(g_sVehicleGenNSData.vehicleID[eName])
								PRINTLN("IMPOUND FIX 2 - cache the mission vehgen data as vehicle is streaming out")
								GET_VEHICLE_SETUP(g_sVehicleGenNSData.vehicleID[eName], g_sPreviousMissionVehGenData)
								g_ePreviousMissionVehGenPed = g_savedGlobals.sVehicleGenData.eLastCharToUseMissionVehGen
								vehLastMissionVehGenToStreamOut = g_sVehicleGenNSData.vehicleID[eName]
							ENDIF
						ENDIF
					ENDIF
					
					PRINT_VEHGEN_DBG("No longer needed: Player out for range")
					SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
					CLEAR_AREA(sData.coords, 3.0, FALSE)
					CLEAR_AREA_OF_VEHICLES(sData.coords, 3.0)
					g_sVehicleGenNSData.vehicleID[eName] = NULL
					
					// [PVB] Player is going out of range so switch over to using the coords
					IF (eName = VEHGEN_MICHAEL_SAVEHOUSE AND eCurrentPlayerPed = CHAR_MICHAEL)
					OR (eName = VEHGEN_TREVOR_SAVEHOUSE_COUNTRY AND eCurrentPlayerPed = CHAR_TREVOR)
					OR (eName = VEHGEN_FRANKLIN_SAVEHOUSE_CAR AND eCurrentPlayerPed = CHAR_FRANKLIN)
						g_sVehicleGenNSData.vPlayerVehicleCoords = sData.coords
						g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
					ENDIF
					
					EXIT
				ENDIF
				
				// anchor when no collision
				IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
					IF NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHORED))
						IF NOT HAS_ENTITY_COLLIDED_WITH_ANYTHING(g_sVehicleGenNSData.vehicleID[eName])
						AND NOT IS_ENTITY_ATTACHED(g_sVehicleGenNSData.vehicleID[eName])
							VECTOR vAnchorPos = GET_ENTITY_COORDS(g_sVehicleGenNSData.vehicleID[eName])
							IF vAnchorPos.z >= GET_ANCHOR_HEIGHT_FOR_VEHICLE_GEN(eName)
								SET_BOAT_ANCHOR(g_sVehicleGenNSData.vehicleID[eName], TRUE)
								SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHORED))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// No need to process creation
			EXIT
		ENDIF
	ELSE
		g_sVehicleGenNSData.vehicleID[eName] = NULL
	ENDIF
	
	// Vehicle destroyed
	IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleID[eName])
		
		// Owned by other script
		/*
			// If we get a bug about not owning the vehicle then we will need 
			// a 'bCheckDead' flag on the DOES_ENTITY_BELONG_TO_THIS_SCRIPT command.
		
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sVehicleGenNSData.vehicleID[eName])
			PRINT_VEHGEN_DBG("No longer needed: Vehicle owned by other script")
			g_sVehicleGenNSData.vehicleID[eName] = NULL
			EXIT
		ENDIF*/
		
		PRINT_VEHGEN_DBG("No longer needed: Vehicle not driveable")
		SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
		g_sVehicleGenNSData.vehicleID[eName] = NULL
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		CLEANUP_ONE_TIME_VEH_GEN(eName)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     Vehicle create
	///
	
	//Check if a script is trying to handover a vehicle to this gen.
	IF g_eVehGenToRecieveVehicle = eName
	
		PRINT_VEHGEN_DBG("Processing a vehgen vehicle handover request.")
		
		//Take ownership of handover vehicle.
		IF DOES_ENTITY_EXIST(g_vehHandoverToGen)
		AND IS_VEHICLE_DRIVEABLE(g_vehHandoverToGen)
		
			// Cleanup current vehicle index if it exists.
			IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleID[eName])
			AND IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleID[eName])
			
				// Ignore if we already have this vehicle
				IF g_sVehicleGenNSData.vehicleID[eName] = g_vehHandoverToGen
					PRINT_VEHGEN_DBG("Vehicle to be handed over is the same vehicle.")
					g_eVehGenToRecieveVehicle = VEHGEN_NONE
					g_vehHandoverToGen = NULL
					EXIT
				ELSE
					PRINT_VEHGEN_DBG("No longer needed: Ready to accept handover vehicle.")
					IF eName = VEHGEN_MISSION_VEH
					AND NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(g_sVehicleGenNSData.vehicleID[eName])
					AND GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleID[eName]) != MONSTER
						SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
					ENDIF
					SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[eName])
					g_sVehicleGenNSData.vehicleID[eName] = NULL
				ENDIF
			ENDIF
			
			g_sVehicleGenNSData.vehicleID[eName] = g_vehHandoverToGen
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
			CLEAR_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_USED_VEHICLE)
			SET_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER)
			CLEAR_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_LEFT_HANDED_OVER_VEHICLE)
			vVehicleCoordsAtHandOver[eName] = GET_ENTITY_COORDS(g_vehHandoverToGen)
			g_eVehGenToRecieveVehicle = VEHGEN_NONE
			
			// Track index so we can delete when spawning a new vehicle for vehicle select menu
			IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
				VEHICLE_SETUP_STRUCT sHandOverData
				GET_VEHICLE_SETUP(g_vehHandoverToGen, sHandOverData)
				
				IF g_bVehHandoverUseVehicleCoords
					UPDATE_DYNAMIC_VEHICLE_GEN_DATA(eName, sHandOverData, GET_ENTITY_COORDS(g_vehHandoverToGen), GET_ENTITY_HEADING(g_vehHandoverToGen), GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(g_vehHandoverToGen))
				ELSE
					UPDATE_DYNAMIC_VEHICLE_GEN_DATA(eName, sHandOverData, g_savedGlobals.sVehicleGenData.vDynamicCoords[sData.dynamicSlotIndex], g_savedGlobals.sVehicleGenData.fDynamicHeading[sData.dynamicSlotIndex], GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(g_vehHandoverToGen))
				ENDIF
				
				g_sVehicleGenNSData.vehicleID[eName] = g_vehHandoverToGen // need to set this again
				g_sVehicleGenNSData.vehicleSelectID[eName] = g_sVehicleGenNSData.vehicleID[eName]
			ENDIF
			
			IF eName = VEHGEN_MISSION_VEH_FBI4_PREP 	//#1581646
				enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
				IF IS_PLAYER_PED_PLAYABLE(ePed)
					PRINT_VEHGEN_DBG("Players stored switch vehicle cleared for prep getaway.")
					g_vPlayerVeh[ePed] = NULL
				ENDIF
			ENDIF 
			SET_ENTITY_AS_MISSION_ENTITY(g_sVehicleGenNSData.vehicleID[eName], TRUE, TRUE)
			
			IF eName = VEHGEN_MISSION_VEH
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], FALSE)
			ENDIF
			
			g_vehHandoverToGen = NULL
			
			// [IMPOUND FIX 2] Send the last vehicle gen to the impound
			IF g_sPreviousMissionVehGenData.eModel != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("IMPOUND FIX 2 - we have a new mission vehgen and the old one is cached - send to impound")
				SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(g_sPreviousMissionVehGenData, g_ePreviousMissionVehGenPed)
				g_sPreviousMissionVehGenData.eModel = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			
			EXIT
		ENDIF
		
		// [IMPOUND FIX 2] Send the last vehicle gen to the impound
		IF g_sPreviousMissionVehGenData.eModel != DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("IMPOUND FIX 2 - we have a new mission vehgen and the old one is cached - send to impound")
			SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(g_sPreviousMissionVehGenData, g_ePreviousMissionVehGenPed)
			g_sPreviousMissionVehGenData.eModel = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		PRINT_VEHGEN_DBG("Vehicle to be handed over doesn't exist.")
		g_eVehGenToRecieveVehicle = VEHGEN_NONE
		g_vehHandoverToGen = NULL
	ENDIF
	
	// Player must leave the area
	IF g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName]
		IF fDistToVehGen[eName] >= cleanupRange
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = FALSE
			PRINT_VEHGEN_DBG("Leave area flag cleared")
		ENDIF
		
		// [PVB] Clear the coords for the vehicle
		IF (eName = VEHGEN_MICHAEL_SAVEHOUSE AND eCurrentPlayerPed = CHAR_MICHAEL)
		OR (eName = VEHGEN_TREVOR_SAVEHOUSE_COUNTRY AND eCurrentPlayerPed = CHAR_TREVOR)
		OR (eName = VEHGEN_FRANKLIN_SAVEHOUSE_CAR AND eCurrentPlayerPed = CHAR_FRANKLIN)
			g_sVehicleGenNSData.vPlayerVehicleCoords = <<0,0,0>>
			g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
		ENDIF
		
		EXIT
	ENDIF
	
	BOOL bContinuePVChecks = FALSE
	
	// Player not in range
	IF fDistToVehGen[eName] > createRange
	
		// [IMPOUND] Send mission vehicle gen to impound if it's not been used
		IF eName = VEHGEN_MISSION_VEH
		AND g_savedGlobals.sVehicleGenData.iPlayerVehicle[sData.dynamicSlotIndex] != 0						// +1 so we dont have to initiailise to -1
		AND g_savedGlobals.sVehicleGenData.iPlayerVehicle[sData.dynamicSlotIndex] > NUM_OF_PLAYABLE_PEDS	//
		AND sData.model != DUMMY_MODEL_FOR_SCRIPT
		AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
		AND IS_VEHICLE_AVAILABLE_FOR_GAME(sData.model)
			TIMEOFDAY eCurrentTOD = GET_CURRENT_TIMEOFDAY()
			TIMEOFDAY eHandOverTOD = g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp
			ADD_TIME_TO_TIMEOFDAY(eHandOverTOD, 0, 0, 17)
			IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(eCurrentTOD, eHandOverTOD)
				SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex], g_savedGlobals.sVehicleGenData.eLastCharToUseMissionVehGen)
				CLEANUP_ONE_TIME_VEH_GEN(eName)
				g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
				PRINT_VEHGEN_DBG("Cannot be created: Vehicle ready for impound")
				
				// [IMPOUND FIX 2] Clear the cached data as we will no longer need to monitor this.
				g_sPreviousMissionVehGenData.eModel = DUMMY_MODEL_FOR_SCRIPT
				EXIT
			ENDIF
		ENDIF
		
		// [PVB] Player is not in range but allow the normal checks to be processed so we can check if it's safe to blip
		IF (eName = VEHGEN_MICHAEL_SAVEHOUSE AND eCurrentPlayerPed = CHAR_MICHAEL)
		OR (eName = VEHGEN_TREVOR_SAVEHOUSE_COUNTRY AND eCurrentPlayerPed = CHAR_TREVOR)
		OR (eName = VEHGEN_FRANKLIN_SAVEHOUSE_CAR AND eCurrentPlayerPed = CHAR_FRANKLIN)
			bContinuePVChecks = TRUE
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	// [PVB] Clear the coords for the vehicle and only set them if the vehicle is safe to create
	IF (eName = VEHGEN_MICHAEL_SAVEHOUSE AND eCurrentPlayerPed = CHAR_MICHAEL)
	OR (eName = VEHGEN_TREVOR_SAVEHOUSE_COUNTRY AND eCurrentPlayerPed = CHAR_TREVOR)
	OR (eName = VEHGEN_FRANKLIN_SAVEHOUSE_CAR AND eCurrentPlayerPed = CHAR_FRANKLIN)
		g_sVehicleGenNSData.vPlayerVehicleCoords = <<0,0,0>>
		g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
	ENDIF
	
	// Vehicle details invalid
	IF sData.model = DUMMY_MODEL_FOR_SCRIPT
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Dummy model")
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(sData.model)
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Vehicle gen model is no longer installed")
		EXIT
	ENDIF
	
	// Vehicle gen not available
	IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Vehicle gen not available")
		EXIT
	ENDIF
	
	// Vehicle gen not purchased
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
	AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED)
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Vehicle gen not purchased")
		EXIT
	ENDIF
	
	// Not available on mission
	IF (IS_VEHCILE_GEN_DISABLED_ON_MISSION() AND IS_CURRENTLY_ON_MISSION_TO_TYPE() AND eName != VEHGEN_MISSION_VEH AND eName != VEHGEN_MISSION_VEH_FBI4_PREP AND eName != VEHGEN_TREV1_SMASHED_TRAILER)
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Vehicle gens blocked on mission")
		EXIT
	ENDIF
	
	// Character not suitable
	IF NOT bCurrentVehicleGenSafeForPlayer
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Player character not valid")
		EXIT
	ENDIF
	
	// Dynamic vehicle still exists
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
		IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[eName])
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
			PRINT_VEHGEN_DBG("Cannot be created: Previous dyanmic vehicle still exists")
			EXIT
		ELSE
			g_sVehicleGenNSData.vehicleSelectID[eName] = NULL
		ENDIF
	ENDIF
	
	// Same vehicle model nearby gen location
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_NEARBY_MODEL_CHECK))
		IF IS_SAME_VEHICLE_MODEL_NEARBY_PLAYER(sData.model)
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
			PRINT_VEHGEN_DBG("Cannot be created: Same vehicle model found nearby player")
			EXIT
		ENDIF
		
		IF IS_SAME_VEHICLE_MODEL_NEARBY_VEHGEN(sData.model)
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
			PRINT_VEHGEN_DBG("Cannot be created: Same vehicle model found nearby gen location")
			EXIT
		ENDIF
	ENDIF
	
	// Not in garage
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
		IF sGarageData.iWarpControl = 0
			EXIT
		ENDIF
	ENDIF
	
	// Vehicle gen+model specific checks
	IF BLOCK_SPECIFIC_VEHICLE_GEN_CREATION(eName)
		g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
		PRINT_VEHGEN_DBG("Cannot be created: Vehgen+model specific checks failed")
		EXIT
	ENDIF
	
	// Player is doing a short range switch
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
	AND GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
		IF eName = VEHGEN_WEB_HANGAR_MICHAEL
		OR eName = VEHGEN_WEB_HANGAR_FRANKLIN
		OR eName = VEHGEN_WEB_HANGAR_TREVOR
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
			PRINT_VEHGEN_DBG("Cannot be created: Short range switch in progress")
			EXIT
		ENDIF
	ENDIF
	
	VEHICLE_CREATE_TYPE_ENUM eVehicleType = VEHICLE_TYPE_DEFAULT
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
		eVehicleType = VEHICLE_TYPE_BIKE
	ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
		eVehicleType = VEHICLE_TYPE_CAR
	ENDIF
	
	IF eName = VEHGEN_MISSION_VEH
	AND g_savedGlobals.sVehicleGenData.iPlayerVehicle[sData.dynamicSlotIndex] > 0						// +1 so we dont have to initiailise to -1
	AND g_savedGlobals.sVehicleGenData.iPlayerVehicle[sData.dynamicSlotIndex] <= NUM_OF_PLAYABLE_PEDS	//
		IF IS_THIS_MODEL_A_BIKE(sData.model)
			eVehicleType = VEHICLE_TYPE_BIKE
			sData.ped = INT_TO_ENUM(enumCharacterList, g_savedGlobals.sVehicleGenData.iPlayerVehicle[sData.dynamicSlotIndex]-1)
		ELIF IS_THIS_MODEL_A_CAR(sData.model)
			eVehicleType = VEHICLE_TYPE_CAR
			sData.ped = INT_TO_ENUM(enumCharacterList, g_savedGlobals.sVehicleGenData.iPlayerVehicle[sData.dynamicSlotIndex]-1)
		ENDIF
	ENDIF
	
	IF eVehicleType != VEHICLE_TYPE_DEFAULT
		// Player vehicle already exists in map
		IF IS_PLAYER_VEHICLE_IN_AREA(sData.ped, eVehicleType, sData.coords, -1) // -1 checks if any exist
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
			PRINT_VEHGEN_DBG("Cannot be created: Same player vehicle found nearby gen location")
			
			IF eName = VEHGEN_MISSION_VEH
				SET_VEHICLE_GEN_AVAILABLE(eName, FALSE)
			ENDIF
			
			EXIT
		ENDIF
		
		// Player vehicle just been cleaned up (ignore mission vehgen)
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_BIKE))
		OR IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PLAYER_VEH_CAR))
			IF eVehicleType = VEHICLE_TYPE_BIKE
				IF g_iCreatedPlayerVehicleCleanupTimer[sData.ped][SAVED_VEHICLE_SLOT_BIKE] != -1
				AND (GET_GAME_TIMER() - g_iCreatedPlayerVehicleCleanupTimer[sData.ped][SAVED_VEHICLE_SLOT_BIKE]) < (GET_MILLISECONDS_PER_GAME_MINUTE()*180)
					g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
					PRINT_VEHGEN_DBG("Cannot be created: Same player vehicle cleaned up within the last 3 hours")
					TEXT_LABEL_23 tlTimeLeft = "..."
					tlTimeLeft += ((GET_MILLISECONDS_PER_GAME_MINUTE()*180) - (GET_GAME_TIMER() - g_iCreatedPlayerVehicleCleanupTimer[sData.ped][SAVED_VEHICLE_SLOT_BIKE])) / 1000
					tlTimeLeft += " seconds"
					PRINT_VEHGEN_DBG(tlTimeLeft)
					EXIT
				ENDIF
			ELIF eVehicleType = VEHICLE_TYPE_CAR
				IF g_iCreatedPlayerVehicleCleanupTimer[sData.ped][SAVED_VEHICLE_SLOT_CAR] != -1
				AND (GET_GAME_TIMER() - g_iCreatedPlayerVehicleCleanupTimer[sData.ped][SAVED_VEHICLE_SLOT_CAR]) < (GET_MILLISECONDS_PER_GAME_MINUTE()*180)
					g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
					PRINT_VEHGEN_DBG("Cannot be created: Same player vehicle cleaned up within the last 3 hours")
					TEXT_LABEL_23 tlTimeLeft = "..."
					tlTimeLeft += ((GET_MILLISECONDS_PER_GAME_MINUTE()*180) - (GET_GAME_TIMER() - g_iCreatedPlayerVehicleCleanupTimer[sData.ped][SAVED_VEHICLE_SLOT_BIKE])) / 1000
					tlTimeLeft += " seconds"
					PRINT_VEHGEN_DBG(tlTimeLeft)
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// [PVB] Player vehicle is safe to create so set the coords until it has actually spawned
	IF (eName = VEHGEN_MICHAEL_SAVEHOUSE AND eCurrentPlayerPed = CHAR_MICHAEL)
	OR (eName = VEHGEN_TREVOR_SAVEHOUSE_COUNTRY AND eCurrentPlayerPed = CHAR_TREVOR)
	OR (eName = VEHGEN_FRANKLIN_SAVEHOUSE_CAR AND eCurrentPlayerPed = CHAR_FRANKLIN)
		g_sVehicleGenNSData.vPlayerVehicleCoords = sData.coords
	ENDIF
	
	// [PVB] Player vehicle is out of the range but we still processed the other checks so we can add a blip at the correct coords
	IF bContinuePVChecks
		EXIT
	ENDIF
	
	IF eVehicleType != VEHICLE_TYPE_DEFAULT
		// Model not loaded
		eVehicleModel[eName] = GET_PLAYER_VEH_MODEL(sData.ped, eVehicleType)
		REQUEST_MODEL(eVehicleModel[eName])
		SET_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
		IF NOT HAS_MODEL_LOADED(eVehicleModel[eName])
			PRINT_VEHGEN_DBG("Cannot be created: Waiting for player vehicle model to load")
			EXIT
		ENDIF
		IF IS_PLAYER_IN_VEHICLE_BOUNDS(eVehicleModel[eName], sData.coords)
			PRINT_VEHGEN_DBG("Cannot be created: Player is too close to spawn position (default vehicle type)")
			EXIT
		ENDIF
		
		// All conditions met so clear things up and create the vehicle	
		CLEAR_AREA(sData.coords, 3.0, FALSE)
		CLEAR_AREA_OF_VEHICLES(sData.coords, 3.0)
		
		IF eVehicleType = VEHICLE_TYPE_BIKE
			CREATE_PLAYER_VEHICLE(g_sVehicleGenNSData.vehicleID[eName], sData.ped, sData.coords, sData.heading, FALSE, VEHICLE_TYPE_BIKE)
		ELIF eVehicleType = VEHICLE_TYPE_CAR
			CREATE_PLAYER_VEHICLE(g_sVehicleGenNSData.vehicleID[eName], sData.ped, sData.coords, sData.heading, FALSE, VEHICLE_TYPE_CAR)
		ELSE
			g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] =  TRUE
			PRINT_VEHGEN_DBG("Cannot be created: Invalid player vehicle type specified")
			EXIT
		ENDIF
	ELSE
		// Model not loaded
		REQUEST_MODEL(sData.model)
		eVehicleModel[eName] = sData.model
		SET_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
		IF NOT HAS_MODEL_LOADED(sData.model)
			PRINT_VEHGEN_DBG("Cannot be created: Waiting for model to load")
			EXIT
		ENDIF
		IF IS_PLAYER_IN_VEHICLE_BOUNDS(sData.model, sData.coords)
			PRINT_VEHGEN_DBG("Cannot be created: Player is too close to spawn position (specific vehicle type)")
			EXIT
		ENDIF
		
		// All conditions met so clear things up and create the vehicle	
		CLEAR_AREA(sData.coords, 3.0, FALSE)
		CLEAR_AREA_OF_VEHICLES(sData.coords, 3.0)
		
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_LONG_RANGE_DIST_CHECKS))	
			REQUEST_COLLISION_AT_COORD(sData.coords)
		ENDIF
		
		IF eName = VEHGEN_WEB_MARINA_MICHAEL
		OR eName = VEHGEN_WEB_MARINA_FRANKLIN
		OR eName = VEHGEN_WEB_MARINA_TREVOR
			IF sData.model = SUBMERSIBLE2
				//sData.coords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.coords, sData.heading, <<0.5, 0.0, 0.0>>)
				sData.coords.z = -3.0
			ENDIF
		ENDIF
		
		g_sVehicleGenNSData.vehicleID[eName] = CREATE_VEHICLE(sData.model, sData.coords, sData.heading)
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DYNAMIC_DATA))
			//B* 2005584: Setting the landing gear on planes to the default state of OUT
			IF IS_THIS_MODEL_A_PLANE(sData.model)
				SET_BIT(g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].iFlags,VEHICLE_SETUP_FLAG_LANDING_GEAR_OUT)
			ENDIF
		
			// Fix for bug # 1853505 - Need to keep default extras!
			IF IS_BIT_SET(g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].iFlags, VEHICLE_SETUP_FLAG_KEEP_DEFAULT_EXTRAS)
				GET_VEHICLE_SETUP(g_sVehicleGenNSData.vehicleID[eName], tempVehicleData)
				
				KEEP_DEFAULT_VEHICLE_FLAGS_WITH_SPECIAL_CASE_MODELS(tempVehicleData.iFlags,g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].iFlags,sData.model)
				
				CLEAR_BIT(g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex].iFlags, VEHICLE_SETUP_FLAG_KEEP_DEFAULT_EXTRAS)
			ENDIF
			
			SET_VEHICLE_SETUP(g_sVehicleGenNSData.vehicleID[eName], g_savedGlobals.sVehicleGenData.sDynamicData[sData.dynamicSlotIndex])
			// Track index so we can delete when spawning a new vehicle for vehicle select menu
			g_sVehicleGenNSData.vehicleSelectID[eName] = g_sVehicleGenNSData.vehicleID[eName]
		ELSE
			IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_FORCE_COLOURS))
				SET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], sData.colour1, sData.colour2)
			ENDIF
			IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_NON_INTERACTABLE))
				SET_VEHICLE_DOORS_LOCKED(g_sVehicleGenNSData.vehicleID[eName], VEHICLELOCK_LOCKED)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(g_sVehicleGenNSData.vehicleID[eName], FALSE)
				SET_VEHICLE_CAN_BE_TARGETTED(g_sVehicleGenNSData.vehicleID[eName], FALSE)
				SET_VEHICLE_CAN_BREAK(g_sVehicleGenNSData.vehicleID[eName], FALSE)
				SET_VEHICLE_CAN_LEAK_OIL(g_sVehicleGenNSData.vehicleID[eName], FALSE)
				SET_VEHICLE_CAN_LEAK_PETROL(g_sVehicleGenNSData.vehicleID[eName], FALSE)
				SET_VEHICLE_TYRES_CAN_BURST(g_sVehicleGenNSData.vehicleID[eName], FALSE)
				SET_ENTITY_INVINCIBLE(g_sVehicleGenNSData.vehicleID[eName], TRUE)
				FREEZE_ENTITY_POSITION(g_sVehicleGenNSData.vehicleID[eName], TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleID[eName])
	
		PRINT_VEHGEN_DBG_WITH_VECTOR("Created - Coords: ", sData.coords)
		PRINT_VEHGEN_DBG_WITH_FLOAT("Created - Dist From Player:", fDistToVehGen[eName])
		
		// [PVB] Player vehicle has been created so clear the coords
		IF (eName = VEHGEN_MICHAEL_SAVEHOUSE AND eCurrentPlayerPed = CHAR_MICHAEL)
		OR (eName = VEHGEN_TREVOR_SAVEHOUSE_COUNTRY AND eCurrentPlayerPed = CHAR_TREVOR)
		OR (eName = VEHGEN_FRANKLIN_SAVEHOUSE_CAR AND eCurrentPlayerPed = CHAR_FRANKLIN)
			g_sVehicleGenNSData.vPlayerVehicleCoords = <<0,0,0>>
		ENDIF
		
		SWITCH sData.model
			CASE MILJET			// Fix for bug # 1880278 - Need to set milject plane white
				SET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 121, 21)
				SET_VEHICLE_EXTRA_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 8, 156)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> MILJET colours [121, 21], extra colours [8, 156]")
			BREAK
			CASE BESRA 			// Fix for bug # 1957537 - Need to set besra plane match wiki
				SET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 122, 89)
				SET_VEHICLE_EXTRA_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 25, 121)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> BESRA colours [122, 89], extra colours [25, 121]")
			BREAK
			CASE BUZZARD		// Fix for bug # 2056616 - Buzzard - The Buzzard is completely white when buying it through the Warstock Cache and Carry website. 
			CASE BUZZARD2
				SET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 0, 0)
				SET_VEHICLE_EXTRA_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 5, 156)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> BUZZARD colours [0, 0], extra colours [5, 156]")
			BREAK
			CASE DUKES2			//No longer setting Dukes2 colour, see B* 2539535
			
			BREAK
			CASE RHINO			// Fix for bug # 2349206 - [Rhino Tank] The Rhino Tank has missing textures on some parts of the vehicle. Some areas appear completely black.
				SET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 131, 132)
				SET_VEHICLE_EXTRA_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 132, 156)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> RHINO colours [131, 132], extra colours [132, 156]")
			BREAK

			CASE LUXOR2			// Fix for bug # 2336050 - Both the Luxor Deluxe and Swift Deluxe spawn in black rather than gold
			CASE SWIFT2
				SET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 159, 0)
				SET_VEHICLE_EXTRA_COLOURS(g_sVehicleGenNSData.vehicleID[eName], 160, 156)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> LUXOR2/SWIFT2 colours [159, 0], extra colours [160, 156]")
			BREAK
			
			#IF IS_DEBUG_BUILD
			CASE CUBAN800
			CASE DUSTER
				INT ReturnColour1, ReturnColour2, ReturnExtraCarColour1, ReturnExtraCarColour2
				GET_VEHICLE_COLOURS(g_sVehicleGenNSData.vehicleID[eName], ReturnColour1, ReturnColour2)
				GET_VEHICLE_EXTRA_COLOURS(g_sVehicleGenNSData.vehicleID[eName], ReturnExtraCarColour1, ReturnExtraCarColour2)
				
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> ", GET_MODEL_NAME_FOR_DEBUG(sData.model), " colours [", ReturnColour1, ", ", ReturnColour2, "], extra colours [", ReturnExtraCarColour1, ", ", ReturnExtraCarColour2, "]")
			BREAK
			#ENDIF
			
//			DEFAULT
//				SITE_BUYABLE_VEHICLE veh
//				veh = GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(sData.model)
//				IF IS_SBV_A_VEHICLE_WITH_LIVERIES(veh)
//					// Fix for bug # 2071091 - The Buckingham Swift is delivered to the player's helipad in the alternative livery to what was ordered
//					VEHICLE_SETUP_STRUCT vss
//					CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(veh, vss, FALSE, eCurrentPlayerPed)
//					SET_VEHICLE_SETUP(g_sVehicleGenNSData.vehicleID[eName], vss, DEFAULT, DEFAULT)
//					
//					IF vss.iLivery >= 0
//						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> ", GET_MODEL_NAME_FOR_DEBUG(sData.model), " configured colours [", vss.iColour1, ", ", vss.iColour2, "], extra colours [", vss.iColourExtra1, ", livery: ", vss.iColourExtra2, "], ", vss.iLivery)
//					ELSE
//						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_VEHICLE -> ", GET_MODEL_NAME_FOR_DEBUG(sData.model), " configured colours [", vss.iColour1, ", ", vss.iColour2, "], extra colours [", vss.iColourExtra1, ", ", vss.iColourExtra2, "]")
//					ENDIF
//					
//				ENDIF
//			BREAK
		ENDSWITCH
		
		// Anchor sea-based vehicles....arr!
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_CREATED))
			SET_BOAT_ANCHOR(g_sVehicleGenNSData.vehicleID[eName], TRUE)
		ENDIF
		
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
			CLEAR_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHORED))
		ENDIF			
		
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LOCK_DOORS))
			SET_VEHICLE_DOORS_LOCKED(g_sVehicleGenNSData.vehicleID[eName], VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED) // B*1513943 - Trevor should have to break into the MrsPhilips2 veh gens
			SET_VEHICLE_ALARM(g_sVehicleGenNSData.vehicleID[eName], TRUE) // B*1552531 - Drugs vans are now alarmed
		ENDIF
		
		APPLY_CUSTOM_VEHICLE_DAMAGE(g_sVehicleGenNSData.vehicleID[eName], eName)
		
		IF NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ONLY_SPAWN_IN_GARAGE))
		AND NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_ANCHOR_WHEN_NO_COLLISION))
			SET_VEHICLE_ON_GROUND_PROPERLY(g_sVehicleGenNSData.vehicleID[eName])
		ENDIF
		SET_VEHICLE_DIRT_LEVEL(g_sVehicleGenNSData.vehicleID[eName], 0.0)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sVehicleGenNSData.vehicleID[eName], TRUE)
		SET_VEHICLE_CAN_SAVE_IN_GARAGE(g_sVehicleGenNSData.vehicleID[eName], IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_CAN_SAVE_IN_GARAGE)))
	ENDIF
	
	// So we can tell if the player has picked up the vehicle
	CLEAR_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_USED_VEHICLE)
	
	// So we know if we created the vehicle from scratch or if it was handed over by another script
	CLEAR_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER)
	
	// So we don't instantly create another vehicle if the player destroys this one
	g_sVehicleGenNSData.bLeaveAreaBeforeCreating[eName] = TRUE
	
	// So player has to wait some time for player vehicle to be returned to savehouse once destroyed
	IF eVehicleType != VEHICLE_TYPE_DEFAULT
		g_sVehicleGenNSData.bCheckPlayerVehicleCleanupTimer[eName] = TRUE
	ENDIF
	
	// [IMPOUND FIX 1] If we created a new version of the vehicle we last tracked for impound then update the vehicle index
	INT iTrackedVehGen = GET_PACKED_STAT_INT(PACKED_SP_VEHGEN_TRACKED_FOR_IMPOUND)
	IF iTrackedVehGen != 0
	AND iTrackedVehGen = ENUM_TO_INT(eName)
		vehPreviousCarToTrackForImpound = g_sVehicleGenNSData.vehicleID[eName]
		PRINTLN("IMPOUND FIX 1 - vehgen has been re-created so updating previous impound track vehicle index")
	ENDIF
	
	// [IMPOUND FIX 2] Clear the cached data as we will no longer need to monitor this.
	IF eName = VEHGEN_MISSION_VEH
		g_sPreviousMissionVehGenData.eModel = DUMMY_MODEL_FOR_SCRIPT
	ENDIF
ENDPROC

/// PURPOSE: Perform any necessary checks before we process the blips, help, txts, and vehicle
PROC PROCESS_CHECKS(VEHICLE_GEN_NAME_ENUM eName)

	// Suitable for current character
	IF NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_DO_PED_CHECK))
	OR sData.ped = eCurrentPlayerPed
		bCurrentVehicleGenSafeForPlayer = TRUE
	ELSE
		bCurrentVehicleGenSafeForPlayer = FALSE
	ENDIF
	
	//B* 2194503: Disable vehicle gens in Director mode
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		bCurrentVehicleGenSafeForPlayer = FALSE
	ENDIF
	
	// Closest vehicle gen for purchase check
	IF bCurrentVehicleGenSafeForPlayer
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
	
		IF sGarageData.iPurchaseControl = 0
		AND sGarageData.iWarpControl = 0
		
			IF fDistToVehGen[eName] < sGarageData.fClosestDist
			OR sGarageData.eClosestGen = eName
			
				IF sGarageData.eClosestGen != eName
					#IF IS_DEBUG_BUILD
						PRINTLN("DO_VEHICLE_GEN_PROCESING() - Updating closest vehicle gen to ", sData.dbg_name)
					#ENDIF
					
					sGarageData.sVehGenData = sData
					sGarageData.sVehGenPurchData = sPurchData
					sGarageData.eClosestGen = eName
					
					INT iWebVeh
					REPEAT COUNT_OF(g_sVehicleGenNSData.eWebVehicles) iWebVeh
						g_sVehicleGenNSData.eWebVehicles[iWebVeh] = UNSET_BUYABLE_VEHICLE
					ENDREPEAT
					sGarageData.iVehicleType = -1
					
					IF sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_FRANKLIN
					OR sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_TREVOR
						sGarageData.iVehicleType = GVT_PLANE
						
					ELIF sGarageData.eClosestGen = VEHGEN_WEB_MARINA_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_MARINA_FRANKLIN
					OR sGarageData.eClosestGen = VEHGEN_WEB_MARINA_TREVOR
						sGarageData.iVehicleType = GVT_BOAT
						
					ELIF sGarageData.eClosestGen = VEHGEN_WEB_HELIPAD_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_HELIPAD_FRANKLIN
					OR sGarageData.eClosestGen = VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY
						sGarageData.iVehicleType = GVT_HELI
						
					ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
					OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
						sGarageData.iVehicleType = GVT_CAR
						
					ENDIF
					
					#IF IS_DEBUG_BUILD
						INT iIncreaseCount
					#ENDIF
					INT iStoredWebVehs
					MODEL_NAMES eWebVehModel
					REPEAT ENUM_TO_INT(NUMBER_OF_BUYABLE_VEHICLES_SP) iWebVeh
						eWebVehModel = GET_MODEL_FOR_BUYABLE_VEHICLE(INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iWebVeh))
						IF eWebVehModel != DUMMY_MODEL_FOR_SCRIPT
							IF iStoredWebVehs < COUNT_OF(g_sVehicleGenNSData.eWebVehicles)
								SWITCH sGarageData.iVehicleType
									CASE GVT_CAR
										IF IS_THIS_MODEL_SAFE_FOR_GARAGE(eWebVehModel)
											g_sVehicleGenNSData.eWebVehicles[iStoredWebVehs] = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iWebVeh)
											iStoredWebVehs++
										ENDIF
									BREAK
									CASE GVT_HELI
										IF IS_THIS_MODEL_A_HELI(eWebVehModel)
											g_sVehicleGenNSData.eWebVehicles[iStoredWebVehs] = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iWebVeh)
											iStoredWebVehs++
										ENDIF
									BREAK
									CASE GVT_BOAT
										IF IS_THIS_MODEL_A_BOAT(eWebVehModel)
										OR IS_THIS_MODEL_A_JETSKI(eWebVehModel)
										OR (eWebVehModel = SUBMERSIBLE2)
											g_sVehicleGenNSData.eWebVehicles[iStoredWebVehs] = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iWebVeh)
											iStoredWebVehs++
										ENDIF
									BREAK
									CASE GVT_PLANE
										IF IS_THIS_MODEL_SAFE_FOR_HANGAR(eWebVehModel)
											g_sVehicleGenNSData.eWebVehicles[iStoredWebVehs] = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iWebVeh)
											iStoredWebVehs++
										ENDIF
									BREAK
								ENDSWITCH
							ELSE
								#IF IS_DEBUG_BUILD
									iIncreaseCount++
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					#IF IS_DEBUG_BUILD
						IF iIncreaseCount != 0
							PRINTLN("Web vehicle count has increased by ", iIncreaseCount)
							SCRIPT_ASSERT("Web vehicle count has increased. Add bug for Kenneth R.")
						ENDIF
					#ENDIF
				ENDIF
				
				sGarageData.fClosestDist = fDistToVehGen[eName]
			ENDIF
		ENDIF
	ELIF sGarageData.eClosestGen = eName
		sGarageData.eClosestGen = VEHGEN_NONE
		sGarageData.fClosestDist = 99999.99
	ENDIF
	
	// Player used vehicle
	IF NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_USED_VEHICLE)
		IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleID[eName])
			IF IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleID[eName])
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleID[eName])
					
						// Wait for player to leave handed over vehicles.
						IF NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_HANDED_OVER) OR IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_LEFT_HANDED_OVER_VEHICLE)
							SET_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_USED_VEHICLE)
							
							// Update the saved states that relate to a vehicle being used
							SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_PLAYER_USED_VEH, TRUE)
							SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_BLIP_PROCESSED, TRUE)
						ENDIF
					ELSE
						SET_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_PLAYER_LEFT_HANDED_OVER_VEHICLE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Fix for bug # 1872855 - Vulkan / Hydra spawns in VTOL mode by default in SP hangars, which makes it hard to navigate out of.
	IF eName = VEHGEN_WEB_HANGAR_TREVOR
		IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[eName])
		AND IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleSelectID[eName])
		AND (GET_ENTITY_MODEL(g_sVehicleGenNSData.vehicleSelectID[eName]) = HYDRA)
			IF NOT bNozzelSet
				IF IS_ENTITY_IN_ANGLED_AREA(g_sVehicleGenNSData.vehicleSelectID[eName], <<1738.686401,3283.422607,45.242832>>, <<1724.511108,3328.807861,39.597805>>, 21.000000)
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION(g_sVehicleGenNSData.vehicleSelectID[eName], 0)
					bNozzelSet = TRUE
				ENDIF
			ELSE
				IF NOT IS_ENTITY_IN_ANGLED_AREA(g_sVehicleGenNSData.vehicleSelectID[eName], <<1738.686401,3283.422607,45.242832>>, <<1724.511108,3328.807861,39.597805>>, 21.000000)
					SET_VEHICLE_FLIGHT_NOZZLE_POSITION(g_sVehicleGenNSData.vehicleSelectID[eName], 1.0)
					bNozzelSet = FALSE
				ENDIF
			ENDIF
		ELSE
			bNozzelSet = FALSE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handle the help text
PROC PROCESS_HELP(VEHICLE_GEN_NAME_ENUM eName)

	// The sData struct must be set using GET_VEHICLE_GEN_DATA(...)
	// before this function is called.
	
	// Check that the vehicle gen is available and help not processed
	IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
	AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_HELP_PROCESSED)
		
		// Player must have used vehicle before we process the help
		IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_PLAYER_USED_VEH)
			// Print a help message if any help flag has been set
			IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PRINT_HELP_ONCE))
				ADD_HELP_TO_FLOW_QUEUE(sData.help)
				SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_HELP_PROCESSED, TRUE)
			ELSE
				// No help text for this vehicle gen so mark as processed
				SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_HELP_PROCESSED, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handle the vehicle blips
PROC PROCESS_BLIP(VEHICLE_GEN_NAME_ENUM eName)

	// The sData struct must be set using GET_VEHICLE_GEN_DATA(...)
	// before this function is called.
	
	BOOL bRemoveBlip = TRUE
	
	// Blip the vehicle gen if it is available and requires a constant blip or needs temp blip
	IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
	AND bCurrentVehicleGenSafeForPlayer
	

		// Add a blip if any blip flag has been set
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP))
		OR (IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_ONCE)) AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_BLIP_PROCESSED))
		
			// And not on mission
			IF (NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_MISSION))
			OR (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY) 
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS) 
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP) 
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR) 
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME) 
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)))
			AND (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 OR GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED) OR NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_HIDE_BLIP_ON_WANTED)))

				bRemoveBlip = FALSE	
				IF NOT DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eName])
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
						// Use the purchase blip/coords
						IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
						AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED)
							g_sVehicleGenNSData.blipID[eName] = ADD_BLIP_FOR_COORD(sPurchData.vBlipCoords)
							IF sPurchData.eBlipSprite != RADAR_TRACE_INVALID
								SET_BLIP_SPRITE(g_sVehicleGenNSData.blipID[eName], sPurchData.eBlipSprite)
								//SHOW_FOR_SALE_ICON_ON_BLIP(g_sVehicleGenNSData.blipID[eName], TRUE)
								IF NOT IS_STRING_NULL_OR_EMPTY(sPurchData.tl15BlipLabel)
									SET_BLIP_NAME_FROM_TEXT_FILE(g_sVehicleGenNSData.blipID[eName], sPurchData.tl15BlipLabel)
								ENDIF
							ENDIF
							
						// Use vehicle spawn menu coords
						ELIF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED)
						AND (eName = VEHGEN_WEB_CAR_MICHAEL OR eName = VEHGEN_WEB_CAR_FRANKLIN OR eName = VEHGEN_WEB_CAR_TREVOR OR eName = VEHGEN_WEB_HANGAR_TREVOR)
							g_sVehicleGenNSData.blipID[eName] = ADD_BLIP_FOR_COORD(sPurchData.vBlipCoords)
							IF sData.blip != RADAR_TRACE_INVALID
								SET_BLIP_SPRITE(g_sVehicleGenNSData.blipID[eName], sData.blip)
								//SHOW_FOR_SALE_ICON_ON_BLIP(g_sVehicleGenNSData.blipID[eName], FALSE)
								IF NOT IS_STRING_NULL_OR_EMPTY(sPurchData.tl15BlipLabel)
									SET_BLIP_NAME_FROM_TEXT_FILE(g_sVehicleGenNSData.blipID[eName], sPurchData.tl15BlipLabel)
								ENDIF
								
								// Change colour to match character.
								//IF eName != VEHGEN_WEB_HANGAR_TREVOR // Do not set colour for the hangar, just the cars.
								// Rob - 2118987 - blip storage properties the correct colour
									INT iBlipCol
									IF sData.ped = CHAR_MICHAEL
										iBlipCol = BLIP_COLOUR_MICHAEL
									ELIF sData.ped = CHAR_FRANKLIN
										iBlipCol = BLIP_COLOUR_FRANKLIN
									ELIF sData.ped = CHAR_TREVOR
										iBlipCol = BLIP_COLOUR_TREVOR
									ENDIF
									SET_BLIP_COLOUR(g_sVehicleGenNSData.blipID[eName], iBlipCol)
								//ENDIF
							ENDIF
							
						// Use default blip/coords
						ELSE
							g_sVehicleGenNSData.blipID[eName] = ADD_BLIP_FOR_COORD(sData.coords)
							IF sData.blip != RADAR_TRACE_INVALID
								SET_BLIP_SPRITE(g_sVehicleGenNSData.blipID[eName], sData.blip)
								//SHOW_FOR_SALE_ICON_ON_BLIP(g_sVehicleGenNSData.blipID[eName], FALSE)
								IF NOT IS_STRING_NULL_OR_EMPTY(sPurchData.tl15BlipLabel)
									SET_BLIP_NAME_FROM_TEXT_FILE(g_sVehicleGenNSData.blipID[eName], sPurchData.tl15BlipLabel)
								ENDIF
								
								// Rob - 2118987 - blip storage properties the correct colour
								IF eName = VEHGEN_WEB_HANGAR_MICHAEL 
								OR eName = VEHGEN_WEB_HANGAR_FRANKLIN 
								OR eName = VEHGEN_WEB_HANGAR_TREVOR 
								OR eName = VEHGEN_WEB_MARINA_MICHAEL 
								OR eName = VEHGEN_WEB_MARINA_FRANKLIN 
								OR eName = VEHGEN_WEB_MARINA_TREVOR 
								OR eName = VEHGEN_WEB_HELIPAD_MICHAEL 
								OR eName = VEHGEN_WEB_HELIPAD_FRANKLIN 
								OR eName = VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY 
									INT iBlipCol
									IF sData.ped = CHAR_MICHAEL
										iBlipCol = BLIP_COLOUR_MICHAEL
									ELIF sData.ped = CHAR_FRANKLIN
										iBlipCol = BLIP_COLOUR_FRANKLIN
									ELIF sData.ped = CHAR_TREVOR
										iBlipCol = BLIP_COLOUR_TREVOR
									ENDIF
									SET_BLIP_COLOUR(g_sVehicleGenNSData.blipID[eName], iBlipCol)
								ENDIF
								
							ENDIF
						ENDIF
						
						SET_BLIP_AS_SHORT_RANGE(g_sVehicleGenNSData.blipID[eName], NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_BLIP_LONG_RANGE)))
						SET_BLIP_FLASHES(g_sVehicleGenNSData.blipID[eName], FALSE)
						SET_BLIP_PRIORITY(g_sVehicleGenNSData.blipID[eName], BLIPPRIORITY_LOW)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bRemoveBlip
		IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eName])
			REMOVE_BLIP(g_sVehicleGenNSData.blipID[eName])
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handle the txt messages
PROC PROCESS_TXT(VEHICLE_GEN_NAME_ENUM eName)

	// The sData struct must be set using GET_VEHICLE_GEN_DATA(...)
	// before this function is called.
	
	// Check that the vehicle gen is available and txt not processed
	IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
	AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_TXT_PROCESSED)
	
		// Send a txt if any txt flag has been set
		IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_SEND_TXT_ONCE))
			// [TODO] Handle txt messages
			
			// If we need to wait for the player to leave then hold off on the txt...
			// Add a switch case for each vehgen
			// CARAPP, send from social club.
			
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_TXT_PROCESSED, TRUE)
		ELSE
			// Not required to send a txt so mark as processed
			SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_TXT_PROCESSED, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Handle the acquired state when linked to propery
PROC PROCESS_PROPERTY(VEHICLE_GEN_NAME_ENUM eName)

	// The sData struct must be set using GET_VEHICLE_GEN_DATA(...)
	// before this function is called.
	
	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_LINKED_TO_PROPERTY))
		IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
		AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED)
			enumCharacterList ePedProperty = NO_CHARACTER
			IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PROPERTY_CAR))
				ePedProperty = GET_CURRENT_PROPERTY_OWNER(PROPERTY_CAR_SCRAP_YARD)
			ELIF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PROPERTY_MODSHOP))
				ePedProperty = GET_CURRENT_PROPERTY_OWNER(PROPERTY_CAR_MOD_SHOP)
			ENDIF
			IF ePedProperty = sData.ped
				SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
//	IF IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE))
//		IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED)
//		AND NOT bPropertyUnlockChecksProcessedThisFrame
//			// Franklin gets garage for free in the special edition game
//			IF eName = VEHGEN_WEB_CAR_FRANKLIN
//				IF IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE)
//					SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE, TRUE)
//					SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED, TRUE)
//					IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eName])
//						REMOVE_BLIP(g_sVehicleGenNSData.blipID[eName])
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			// Give free garage when we have special vehicles available/installed
//			INT iVehicleTypeToCheck = GVT_CAR
//			
//			IF eName = VEHGEN_WEB_CAR_FRANKLIN
//			OR eName = VEHGEN_WEB_CAR_MICHAEL
//			OR eName = VEHGEN_WEB_CAR_TREVOR
//				iVehicleTypeToCheck = GVT_CAR
//			ELIF eName = VEHGEN_WEB_HELIPAD_MICHAEL
//			OR eName = VEHGEN_WEB_HELIPAD_FRANKLIN
//				iVehicleTypeToCheck = GVT_HELI
//			ELIF eName = VEHGEN_WEB_HANGAR_MICHAEL
//			OR eName = VEHGEN_WEB_HANGAR_FRANKLIN
//			OR eName = VEHGEN_WEB_HANGAR_TREVOR
//				iVehicleTypeToCheck = GVT_PLANE
//			ELIF eName = VEHGEN_WEB_MARINA_MICHAEL
//			OR eName = VEHGEN_WEB_MARINA_FRANKLIN
//			OR eName = VEHGEN_WEB_MARINA_TREVOR
//				iVehicleTypeToCheck = GVT_BOAT
//			ENDIF
//			
//			// Check special vehicles
//			INT iDLCVehCount, iDLCVeh
//			iDLCVehCount = GET_NUMBER_OF_SPECIAL_VEHICLES()
//			REPEAT iDLCVehCount iDLCVeh
//				IF IS_THIS_MODEL_A_PLANE(GET_SPECIAL_VEHICLE_MODEL(iDLCVeh)) AND iVehicleTypeToCheck = GVT_PLANE
//				OR IS_THIS_MODEL_A_BOAT(GET_SPECIAL_VEHICLE_MODEL(iDLCVeh)) AND iVehicleTypeToCheck = GVT_BOAT
//				OR IS_THIS_MODEL_A_CAR(GET_SPECIAL_VEHICLE_MODEL(iDLCVeh)) AND iVehicleTypeToCheck = GVT_CAR
//				OR IS_THIS_MODEL_A_BIKE(GET_SPECIAL_VEHICLE_MODEL(iDLCVeh)) AND iVehicleTypeToCheck = GVT_CAR
//				OR IS_THIS_MODEL_A_HELI(GET_SPECIAL_VEHICLE_MODEL(iDLCVeh)) AND iVehicleTypeToCheck = GVT_HELI
//					SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE, TRUE)
//					SET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED, TRUE)
//					IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eName])
//						REMOVE_BLIP(g_sVehicleGenNSData.blipID[eName])
//					ENDIF
//					iDLCVeh = iDLCVehCount+1//Bail
//				ENDIF
//			ENDREPEAT
//			
//			bPropertyUnlockChecksProcessedThisFrame = TRUE
//		ENDIF
//	ENDIF
ENDPROC

/// PURPOSE: Handle the blocking areas for a vehgen
PROC PROCESS_BLOCKING_AREAS(VEHICLE_GEN_NAME_ENUM eName)

	// The sData struct must be set using GET_VEHICLE_GEN_DATA(...)
	// before this function is called.
	
	BOOL bBlockArea = FALSE
	IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_AVAILABLE)
	AND (NOT IS_BIT_SET(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_PURCHASABLE)) OR GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
		bBlockArea = TRUE
	ENDIF
	
	IF g_sVehicleGenNSData.bScenarioBlockSet[eName] != bBlockArea
		IF NOT ARE_VECTORS_EQUAL(sData.scenario_block_minXYZ, <<0.0,0.0,0.0>>)
			IF NOT bBlockArea
				IF g_sVehicleGenNSData.bScenarioBlockSet[eName]
					REMOVE_SCENARIO_BLOCKING_AREA(g_sVehicleGenNSData.scenarioBlock[eName])
					#IF IS_DEBUG_BUILD
						PRINTLN("PROCESS_BLOCKING_AREAS - Removed scenario block for vehgen ", sData.dbg_name, ".")
					#ENDIF
				ENDIF
			ELSE
				IF NOT g_sVehicleGenNSData.bScenarioBlockSet[eName]
					g_sVehicleGenNSData.scenarioBlock[eName] = ADD_SCENARIO_BLOCKING_AREA(sData.scenario_block_minXYZ, sData.scenario_block_maxXYZ)
					#IF IS_DEBUG_BUILD
						PRINTLN("PROCESS_BLOCKING_AREAS - Adding scenario block for vehgen ", sData.dbg_name, ".")
					#ENDIF
				ENDIF
			ENDIF	
		ENDIF
		g_sVehicleGenNSData.bScenarioBlockSet[eName] = bBlockArea
	ENDIF
ENDPROC

PROC PROCESS_FOR_SALE_SIGN(VEHICLE_GEN_NAME_ENUM eName)

	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	BOOL bCreateForSaleSign = FALSE
	VECTOR vCoords
	FLOAT fHeading
	INT iSign = -1
	MODEL_NAMES signModel = Prop_forSale_DYN_01
	
	SWITCH eName
		CASE VEHGEN_WEB_HANGAR_MICHAEL
		CASE VEHGEN_WEB_HANGAR_FRANKLIN
			bCreateForSaleSign = (NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
			vCoords = << -961.42, -2794.47, 12.96 >>
			fHeading = -209.22
			iSign = 0
			signModel = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Airport_Sale_Sign"))
		BREAK
		CASE VEHGEN_WEB_MARINA_MICHAEL
		CASE VEHGEN_WEB_MARINA_FRANKLIN	
		CASE VEHGEN_WEB_MARINA_TREVOR
			bCreateForSaleSign = (NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
			vCoords = <<-827.93, -1368.14, 3.9982 >>
			fHeading = -68.75
			iSign = 1
		BREAK
		CASE VEHGEN_WEB_HELIPAD_MICHAEL
		CASE VEHGEN_WEB_HELIPAD_FRANKLIN
			bCreateForSaleSign = (NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
			bCreateForSaleSign = TRUE
			vCoords = << -710.07, -1414.31, 4.00 >>
			fHeading = -41.25
			iSign = 2
		BREAK
		CASE VEHGEN_WEB_CAR_MICHAEL
			bCreateForSaleSign = (NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
			vCoords = <<-66.21, 77.76, 70.51>>
			fHeading = -27.00
			iSign = 3
		BREAK
		CASE VEHGEN_WEB_CAR_FRANKLIN
			bCreateForSaleSign = (NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
			vCoords = <<-76.02, -1825.61, 25.88>>
			fHeading = -129.67
			iSign = 4
		BREAK
		CASE VEHGEN_WEB_CAR_TREVOR
			bCreateForSaleSign = (NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(eName, VEHGEN_S_FLAG_ACQUIRED))
			vCoords = <<-218.68, -1165.76, 21.99>>
			fHeading = 89.95
			iSign = 5
		BREAK
	ENDSWITCH
	
	IF bCreateForSaleSign
	AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoords) < 250
		
		IF NOT DOES_ENTITY_EXIST(objForSaleSign[iSign])
			REQUEST_MODEL(signModel)
			bObjectRequested = TRUE
			
			IF HAS_MODEL_LOADED(signModel)
				IF bObjectRequested
					objForSaleSign[iSign] = CREATE_OBJECT_NO_OFFSET(signModel, vCoords, FALSE)
					SET_ENTITY_ROTATION(objForSaleSign[iSign], <<0,0,fHeading>>)
					SET_ENTITY_CAN_BE_DAMAGED(objForSaleSign[iSign], FALSE)
					FREEZE_ENTITY_POSITION(objForSaleSign[iSign], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(signModel)
					bObjectRequested = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELIF iSign != -1
		IF DOES_ENTITY_EXIST(objForSaleSign[iSign])
		AND NOT IS_ENTITY_ON_SCREEN(objForSaleSign[iSign])
		AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoords) > 255
			DELETE_OBJECT(objForSaleSign[iSign])
			IF bObjectRequested
				SET_MODEL_AS_NO_LONGER_NEEDED(signModel)
				bObjectRequested = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC ADD_VEHICLE_GEN_TO_PROCESS_LIST(VEHICLE_GEN_NAME_ENUM eName)
	INT iVehBitset = ENUM_TO_INT(eName)/32
	INT iVehBit = ENUM_TO_INT(eName)%32
	IF NOT IS_BIT_SET(iVehGenCheck[iVehBitset], iVehBit)
		SET_BIT(iVehGenCheck[iVehBitset], iVehBit)
		eVehGenCheck[iVehGenCheckCount] = eName
		iVehGenCheckCount++
	ENDIF
ENDPROC

/// PURPOSE: Calls the appropriate procs for the current frame
PROC DO_VEHICLE_GEN_PROCESING()
	
	// We only process property unlock checks once per frame, clear it here.
//	bPropertyUnlockChecksProcessedThisFrame = FALSE
	
	IF g_sVehicleGenNSData.bCheckVehGensLoaded
		IF NOT bCheckAllNearbyVehiclesInit
			g_sVehicleGenNSData.iCheckVehGensLoadedCounter = 0
			bCheckAllNearbyVehiclesInit = TRUE
		
		ELIF g_sVehicleGenNSData.iCheckVehGensLoadedCounter >= NUMBER_OF_VEHICLES_TO_GEN
			#IF IS_DEBUG_BUILD
				PRINTLN("PROCESS_ALL_VEHICLE_GENS_LOADED - All vehicle gens loaded near player")
			#ENDIF
			g_sVehicleGenNSData.bCheckVehGensLoaded = FALSE
			bCheckAllNearbyVehiclesInit = FALSE
		ENDIF
	ELSE
		bCheckAllNearbyVehiclesInit = FALSE
	ENDIF
	
	// Cycle through all the vehicles in the check list
	INT i
	VEHICLE_GEN_NAME_ENUM eName
	REPEAT iVehGenCheckCount i
		
		eName = eVehGenCheck[i]
		
		// Set up the data before we process
		IF GET_VEHICLE_GEN_DATA(sData, eName)
		
			
			GET_PURCHASABLE_GARAGE_DATA(sPurchData, eName)
			
			IF (sPurchData.bWarpToGarage AND sGarageData.iWarpControl = 0)
			OR (eName = VEHGEN_WEB_HANGAR_TREVOR)
				fDistToVehGen[eName] = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sPurchData.vBlipCoords, FALSE)
			ELSE
				fDistToVehGen[eName] = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), sData.coords, FALSE)
			ENDIF
			
			
			IF IS_CONTROLLER_SAFE_TO_RUN()
				PROCESS_CHECKS(eName)
				PROCESS_HELP(eName)
				PROCESS_BLIP(eName)
				PROCESS_TXT(eName)
				PROCESS_PROPERTY(eName)
				PROCESS_BLOCKING_AREAS(eName)
				PROCESS_FOR_SALE_SIGN(eName)
				PROCESS_VEHICLE(eName)
				
				IF IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
					IF g_sVehicleGenNSData.bCheckVehGensLoaded
						#IF IS_DEBUG_BUILD
							PRINTLN("PROCESS_ALL_VEHICLE_GENS_LOADED - WAIT: Currently creating ", sData.dbg_name, ".")
						#ENDIF
						bCheckAllNearbyVehiclesInit = FALSE
					ENDIF
					
					// Requesting model so keep processing
					ADD_VEHICLE_GEN_TO_PROCESS_LIST(eName)
				ENDIF
			ELSE
				CLEANUP_VEHICLE_MODEL(eName)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Check what vehgens need to be processed
	INT iVehGenCheckCount_Temp = iVehGenCheckCount
	iVehGenCheckCount = 0
	REPEAT (COUNT_OF(iVehGenCheck)) i
		iVehGenCheck[i] = 0
	ENDREPEAT
	REPEAT iVehGenCheckCount_Temp i
		// If the shop is still in range add it to the new list
		IF IS_BIT_SET(iVehGenProcessFlags[eVehGenCheck[i]], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
			ADD_VEHICLE_GEN_TO_PROCESS_LIST(eVehGenCheck[i])
		ENDIF
	ENDREPEAT
	IF ePurchasedVehicleGen != VEHGEN_NONE
		ADD_VEHICLE_GEN_TO_PROCESS_LIST(ePurchasedVehicleGen)
		ePurchasedVehicleGen = VEHGEN_NONE
	ENDIF
	
	// Update next default vehicle to watch
	iProcessSlot++
	IF iProcessSlot >= NUMBER_OF_VEHICLES_TO_GEN
		iProcessSlot = 0
	ENDIF
	ADD_VEHICLE_GEN_TO_PROCESS_LIST(INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, iProcessSlot))
	
	
	IF bForceGarageChecks
		ADD_VEHICLE_GEN_TO_PROCESS_LIST(VEHGEN_WEB_CAR_MICHAEL)
		ADD_VEHICLE_GEN_TO_PROCESS_LIST(VEHGEN_WEB_CAR_FRANKLIN)
		ADD_VEHICLE_GEN_TO_PROCESS_LIST(VEHGEN_WEB_CAR_TREVOR)
		bForceGarageChecks = FALSE
		bProcessingGarageChecks = TRUE
	ELIF bProcessingGarageChecks
		bProcessingGarageChecks = FALSE
	ENDIF
	
	IF g_sVehicleGenNSData.bCheckVehGensLoaded
		g_sVehicleGenNSData.iCheckVehGensLoadedCounter++
	ENDIF
ENDPROC


PROC CLEANUP_CARSTEAL_WALL()
	PRINTLN("<VEHGEN> Cleaning up carsteal wall area in chopshop.")
	INT iLineIndex
	REPEAT 6 iLineIndex
		IF DOES_ENTITY_EXIST(oCarstealWallLine[iLineIndex])
			DELETE_OBJECT(oCarstealWallLine[iLineIndex])
		ENDIF
	ENDREPEAT
ENDPROC



//Updates the black-lit wall in the chop shop to show the names of the cars that have been stolen on the
//carsteal missions. Wasn't sure the best script to use to handle this, but it is closely related to the
//carsteal cargens so it runs in the vehgen thread for now. BenR.
PROC DO_CARSTEAL_WALL_PROCESSING()

	FLOAT lineZFwd = -1308.545
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<475.1920, -1313.4802, 28.2074>>) < 1000
			IF NOT bCarstealWallUpdated
				REQUEST_MODEL(V_ILEV_UVLINE)
				bCarstealWallLoaded = TRUE
				IF HAS_MODEL_LOADED(V_ILEV_UVLINE)
					PRINTLN("<VEHGEN> Updated Car Steal wall on entering chopshop area.")
					
					// Create line stroke objects.
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
						PRINTLN("<VEHGEN> Carsteal 1 complete. Marking EntityXF and Cheetah as stolen.")
						oCarstealWallLine[CARSTEAL_WALL_ENTITYXF] = CREATE_OBJECT(V_ILEV_UVLINE, 	<<471.48, lineZFwd, 30.33>>)
						SET_ENTITY_COORDS(oCarstealWallLine[CARSTEAL_WALL_ENTITYXF], 				<<471.48, lineZFwd, 30.33>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_ENTITYXF], 				<<0.0000, 0.0000, 116.9000>>)
						
						oCarstealWallLine[CARSTEAL_WALL_CHEETAH] = CREATE_OBJECT(V_ILEV_UVLINE, 	<<471.48, lineZFwd, 30.15>>)
						SET_ENTITY_COORDS(oCarstealWallLine[CARSTEAL_WALL_CHEETAH], 				<<471.48, lineZFwd, 30.15>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_CHEETAH], 				<<0.0000, 0.0000, 116.9000>>)
					ENDIF
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_2)
						PRINTLN("<VEHGEN> Carsteal 2 complete. Marking ZType as stolen.")
						oCarstealWallLine[CARSTEAL_WALL_ZTYPE] = CREATE_OBJECT(V_ILEV_UVLINE, 		<<471.48, lineZFwd, 29.98>>)
						SET_ENTITY_COORDS(oCarstealWallLine[CARSTEAL_WALL_ZTYPE], 					<<471.48, lineZFwd, 29.98>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_ZTYPE], 				<<0.0000, 0.0000, 116.9000>>)
					ENDIF
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_3)
						PRINTLN("<VEHGEN> Carsteal 3 complete. Marking JB700 as stolen.")
						oCarstealWallLine[CARSTEAL_WALL_JB700] = CREATE_OBJECT(V_ILEV_UVLINE, 		<<471.48, lineZFwd, 29.82>>)
						SET_ENTITY_COORDS(oCarstealWallLine[CARSTEAL_WALL_JB700], 					<<471.48, lineZFwd, 29.82>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_JB700], 				<<0.0000, 0.0000, 116.9000>>)
					ENDIF
					
					/* OLD Create line stroke objects.
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_1)
						PRINTLN("<VEHGEN> Carsteal 1 complete. Marking EntityXF and Cheetah as stolen.")
						oCarstealWallLine[CARSTEAL_WALL_ENTITYXF] = CREATE_OBJECT(V_ILEV_UVLINE, 	<<471.4763, -1308.540, 29.665>>)
						SET_ENTITY_COORDS_NO_OFFSET(oCarstealWallLine[CARSTEAL_WALL_ENTITYXF], 		<<471.4763, -1308.540, 29.665>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_ENTITYXF], 				<<0.0000, 0.0000, 116.9000>>)
						oCarstealWallLine[CARSTEAL_WALL_CHEETAH] = CREATE_OBJECT(V_ILEV_UVLINE, 	<<471.4763, -1308.540, 30.145>>)
						SET_ENTITY_COORDS_NO_OFFSET(oCarstealWallLine[CARSTEAL_WALL_CHEETAH], 		<<471.4763, -1308.540, 30.145>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_CHEETAH], 				<<0.0000, 0.0000, 116.9000>>)
					ENDIF
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_2)
						PRINTLN("<VEHGEN> Carsteal 2 complete. Marking ZType as stolen.")
						oCarstealWallLine[CARSTEAL_WALL_ZTYPE] = CREATE_OBJECT(V_ILEV_UVLINE, 		<<471.476, -1308.540, 29.525>>)
						SET_ENTITY_COORDS_NO_OFFSET(oCarstealWallLine[CARSTEAL_WALL_ZTYPE], 		<<471.476, -1308.540, 29.525>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_ZTYPE], 				<<0.0000, 0.0000, 116.9000>>)
					ENDIF
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_3)
						PRINTLN("<VEHGEN> Carsteal 3 complete. Marking JB700 as stolen.")
						oCarstealWallLine[CARSTEAL_WALL_JB700] = CREATE_OBJECT(V_ILEV_UVLINE, 		<<471.4763, -1308.540, 29.815>>)
						SET_ENTITY_COORDS_NO_OFFSET(oCarstealWallLine[CARSTEAL_WALL_JB700], 		<<471.4763, -1308.540, 29.815>>)
						SET_ENTITY_ROTATION(oCarstealWallLine[CARSTEAL_WALL_JB700], 				<<0.0000, 0.0000, 116.9000>>)
					ENDIF
					*/
					
					//Configure objects.
					INTERIOR_INSTANCE_INDEX intChopshop = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<475.1920, -1313.4802, 28.2074>>, "v_chopshop")
					INT iLineIndex
					REPEAT 6 iLineIndex
						IF DOES_ENTITY_EXIST(oCarstealWallLine[iLineIndex])
							SET_ENTITY_VISIBLE(oCarstealWallLine[iLineIndex], TRUE)
							SET_ENTITY_ALWAYS_PRERENDER(oCarstealWallLine[iLineIndex], TRUE)
							RETAIN_ENTITY_IN_INTERIOR(oCarstealWallLine[iLineIndex], intChopshop)
						ENDIF
					ENDREPEAT
					
					SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_UVLINE)
					
					bCarstealWallUpdated = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bCarstealWallLoaded
				PRINTLN("<VEHGEN> Unloaded carsteal wall assets.")
				SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_UVLINE)
				bCarstealWallLoaded = FALSE
			ENDIF
			IF bCarstealWallUpdated
				PRINTLN("<VEHGEN> Left car steal wall area in chopshop.")
				CLEANUP_CARSTEAL_WALL()
				bCarstealWallUpdated = FALSE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC SEND_DYNAMIC_EMAIL_FOR_PROPERTY_PURCHASE(VEHICLE_GEN_NAME_ENUM paramPurchased)

	//Work out the dynamic thread and email thread to use.
	DYNAMIC_EMAIL_THREAD_NAMES eDynamicThread
	EMAIL_MESSAGE_ENUMS eEmailThread
	SWITCH paramPurchased
	
		CASE VEHGEN_WEB_CAR_MICHAEL
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_GARAGE_MIKE
			eEmailThread = EMAIL_GARAGE_MIKE
		BREAK
		
		CASE VEHGEN_WEB_CAR_FRANKLIN
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_GARAGE_FRANKLIN
			eEmailThread = EMAIL_GARAGE_FRANK
		BREAK
		
		CASE VEHGEN_WEB_CAR_TREVOR
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_GARAGE_TREVOR
			eEmailThread = EMAIL_GARAGE_TREV
		BREAK
		
		CASE VEHGEN_WEB_HANGAR_MICHAEL
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_HANGAR_MIKE
			eEmailThread = EMAIL_HANGAR_MIKE
		BREAK
		
		CASE VEHGEN_WEB_HANGAR_FRANKLIN
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_HANGAR_FRANKLIN
			eEmailThread = EMAIL_HANGAR_FRANK
		BREAK
		
		CASE VEHGEN_WEB_HANGAR_TREVOR
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_HANGAR_TREVOR
			eEmailThread = EMAIL_HANGAR_TREV
		BREAK
		
		CASE VEHGEN_WEB_MARINA_MICHAEL
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_MARINA_MIKE
			eEmailThread = EMAIL_MARINA_MIKE
		BREAK
		
		CASE VEHGEN_WEB_MARINA_FRANKLIN
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_MARINA_FRANKLIN
			eEmailThread = EMAIL_MARINA_FRANK
		BREAK
		
		CASE VEHGEN_WEB_MARINA_TREVOR
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_MARINA_TREVOR
			eEmailThread = EMAIL_MARINA_TREV
		BREAK
		
		CASE VEHGEN_WEB_HELIPAD_MICHAEL
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_HELIPAD_MIKE
			eEmailThread = EMAIL_HELIPAD_MIKE
		BREAK
		
		CASE VEHGEN_WEB_HELIPAD_FRANKLIN
			eDynamicThread = DYNAMIC_THREAD_BOUGHT_HELIPAD_FRANKLIN
			eEmailThread = EMAIL_HELIPAD_FRANK
		BREAK
	ENDSWITCH

	ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD(eDynamicThread)
	INT iPrime = PRIME_EMAIL_FOR_FIRING_INTO_DYNAMIC_THREAD_IN_HOURS(eDynamicThread, eEmailThread, 1)
	IF iPrime != 0
	
	ELSE	
		//Couldn't do delayed send, purge.
		RELEASE_DYNAMIC_THREAD_HOLD_ON_BUFFER(eDynamicThread)
		//Try immediate send.
		IF FIRE_EMAIL_INTO_DYNAMIC_THREAD(eDynamicThread, eEmailThread,TRUE)
			
			PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD(eDynamicThread)
			RELEASE_DYNAMIC_THREAD_HOLD_ON_BUFFER(eDynamicThread)
		ELSE
			//Failed to make dynamic thread for this notification.
			SCRIPT_ASSERT("SEND_DYNAMIC_EMAIL_FOR_PROPERTY_PURCHASE: Failed to send purchase notification email.")
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Query to find which player character owns each purchasable garage. Returns no
///    character for none purchasable garage vehicle gens.
FUNC enumCharacterList GET_PURCHASABLE_GARAGE_OWNER(VEHICLE_GEN_NAME_ENUM eGarage)
	SWITCH eGarage
		CASE VEHGEN_WEB_CAR_MICHAEL
		CASE VEHGEN_MICHAEL_GARAGE_1
		CASE VEHGEN_MICHAEL_GARAGE_2
		CASE VEHGEN_MICHAEL_GARAGE_3
			RETURN CHAR_MICHAEL
		BREAK
		
		CASE VEHGEN_WEB_CAR_TREVOR
		CASE VEHGEN_TREVOR_GARAGE_1
		CASE VEHGEN_TREVOR_GARAGE_2
		CASE VEHGEN_TREVOR_GARAGE_3
			RETURN CHAR_TREVOR
		BREAK
		
		CASE VEHGEN_WEB_CAR_FRANKLIN
		CASE VEHGEN_FRANKLIN_GARAGE_1
		CASE VEHGEN_FRANKLIN_GARAGE_2
		CASE VEHGEN_FRANKLIN_GARAGE_3
			RETURN CHAR_FRANKLIN
		BREAK
	ENDSWITCH
	
	RETURN NO_CHARACTER
ENDFUNC
	
	
/// PURPOSE: Displays a menu allows the player to change the dyanmic vehicle data
PROC PROCESS_GARAGE_VEHICLE_SELECT()

	// Placeholder stuff for now - only works with hangar
	// This is tied into the purchasable vehicle gens
	
	VEHICLE_SETUP_STRUCT_MP VehicleSetupMP
	VEHICLE_SETUP_STRUCT sDLCVeh
	
	BOOL bDisplayNoVehicleHelp = FALSE
	BOOL bCursorAccept = FALSE
		
	IF iVehicleMenuControl > 0
	AND iVehicleMenuControl != 99
		IF sGarageData.eClosestGen = VEHGEN_NONE
		OR IS_PED_INJURED(PLAYER_PED_ID())
		OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
		OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vMenuPos1, sGarageData.sVehGenPurchData.vMenuPos2, sGarageData.sVehGenPurchData.fMenuWidth)
		//OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)	B*1157533  let us use plane selector when in a vehicle - disable radio
		OR (DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]) AND IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]) AND (IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]) OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen], TRUE)))
		OR ((DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]) AND DOES_ENTITY_EXIST(PLAYER_PED_ID())) AND ( (GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) - 1.0) > 0.15 AND IS_ENTITY_TOUCHING_ENTITY( PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen] ) ) )		//	B* 2330436
		OR ( ( DOES_ENTITY_EXIST( g_sVehicleGenNSData.vehicleSelectID[ sGarageData.eClosestGen ] ) AND DOES_ENTITY_EXIST( PLAYER_PED_ID() ) ) AND ( IS_PED_IN_VEHICLE( PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleSelectID[ sGarageData.eClosestGen ], TRUE ) ) )
		OR ( ( DOES_ENTITY_EXIST(PLAYER_PED_ID() ) ) AND ( IS_PED_GETTING_INTO_A_VEHICLE( PLAYER_PED_ID() ) ) ) 
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		OR (IS_REPEAT_PLAY_ACTIVE() != bRepeatPlayActiveState)
		OR bDoTutorial
		OR IS_PLAYER_UNDER_ATTACK()
			CDEBUG1LN(DEBUG_GOLF, "[RBJ] - Kicked out here...")
			iVehicleMenuControl = 99
		ENDIF
	ENDIF
	
	SWITCH iVehicleMenuControl
		CASE 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND NOT g_bBrowserVisible
			AND NOT bDoTutorial
			AND NOT IS_SCREEN_FADED_OUT()
				IF sGarageData.sVehGenPurchData.bDataSet
				AND sGarageData.iPurchaseControl = 0 // not running cutscne
				AND (g_sVehicleGenNSData.eWebVehicles[0] != UNSET_BUYABLE_VEHICLE OR GET_NUMBER_OF_SPECIAL_VEHICLES() != 0)
				AND sGarageData.eClosestGen != VEHGEN_NONE
				AND NOT bExitGarageInDLCVehicle
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
				AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				AND NOT IS_PLAYER_UNDER_ATTACK()
					IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
						//do nothing
						
					ELSE
						IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_AVAILABLE)
						AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_ACQUIRED)
						
	//						IF NOT bLeaveAreaAfterPurchase
	//							DRAW_MARKER_FOR_TRIGGER_LOCATION(sGarageData.sVehGenPurchData.vBlipCoords)
	//						ENDIF
						
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vMenuPos1, sGarageData.sVehGenPurchData.vMenuPos2, sGarageData.sVehGenPurchData.fMenuWidth)
							AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())	
							AND NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
							
								IF NOT DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
								OR NOT IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
								OR (NOT IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen], TRUE))
									
									// Only allow the trigger if we have a vehicle...
									INT i, j
									
									// Check special vehicles
									INT iDLCVehCount, iDLCVeh
									iDLCVehCount = GET_NUMBER_OF_SPECIAL_VEHICLES()
									REPEAT iDLCVehCount iDLCVeh
										IF IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(GET_SPECIAL_VEHICLE_MODEL(iDLCVeh))
											j++
										ENDIF
									ENDREPEAT
									
									// Check website vehicles
									REPEAT COUNT_OF(g_sVehicleGenNSData.eWebVehicles) i
										IF g_sVehicleGenNSData.eWebVehicles[i] != UNSET_BUYABLE_VEHICLE
											IF GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(g_sVehicleGenNSData.eWebVehicles[i], eCurrentPlayerPed)
											AND g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[g_sVehicleGenNSData.eWebVehicles[i]] = INVALID_TIMEOFDAY								
											AND (g_sVehicleGenNSData.eWebVehicles[i] != BV_NG_MARSHALL OR NOT g_savedGlobals.sCountryRaceData.bMarshallUnlocked)
												j++
											ENDIF
										ENDIF
									ENDREPEAT
									
									IF j > 0
									
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											bLeaveAreaAfterPurchase = TRUE
											
											IF sGarageData.iVehicleType = GVT_CAR
												REGISTER_CONTEXT_INTENTION(iVehicleMenuContextID, CP_MEDIUM_PRIORITY, "WEB_VEH_TRIG2")	// Press ~a~ to browse special vehicles.
											ELSE
												REGISTER_CONTEXT_INTENTION(iVehicleMenuContextID, CP_MEDIUM_PRIORITY, "WEB_VEH_TRIG")	// Press ~a~ to change vehicle.
											ENDIF
											bRepeatPlayActiveState = IS_REPEAT_PLAY_ACTIVE()
											iVehicleMenuControl++
										ENDIF
									ELIF NOT bLeaveAreaAfterPurchase
										IF sGarageData.iVehicleType = GVT_PLANE
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HANGAR_NO")
												PRINT_HELP("HANGAR_NO")
												tlSelectHelpPrinted = "HANGAR_NO"
											ENDIF
											bDisplayNoVehicleHelp = TRUE
										ELIF sGarageData.iVehicleType = GVT_BOAT
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MARINA_NO")
												PRINT_HELP("MARINA_NO")
												tlSelectHelpPrinted = "MARINA_NO"
											ENDIF
											bDisplayNoVehicleHelp = TRUE
										ELIF sGarageData.iVehicleType = GVT_HELI
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELIPAD_NO")
												PRINT_HELP("HELIPAD_NO")
												tlSelectHelpPrinted = "HELIPAD_NO"
											ENDIF
											bDisplayNoVehicleHelp = TRUE
										ELIF sGarageData.iVehicleType = GVT_CAR
											
											TEXT_LABEL_15 tlLabel
											tlLabel = "CAR_GAR_NO"
											IF IS_PS3_VERSION()
											OR IS_PLAYSTATION_PLATFORM()
												tlLabel += "_1"
											ELIF IS_XBOX360_VERSION()
											OR IS_XBOX_PLATFORM()
												tlLabel += "_2"
											ENDIF
											
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlLabel)
												PRINT_HELP(tlLabel)
												tlSelectHelpPrinted = tlLabel
											ENDIF
											bDisplayNoVehicleHelp = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								bLeaveAreaAfterPurchase = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND LOAD_MENU_ASSETS()
				IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
					iVehicleMenuControl = 0
					EXIT
				ENDIF
				
				IF HAS_CONTEXT_BUTTON_TRIGGERED(iVehicleMenuContextID)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ELSE
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)	// B*1157533 - disable radio if player is using selector in a vehicle
							CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_VEHICLE_SELECT -> player's vehicle radio disabled")	
						ENDIF
					ENDIF
					SET_CURSOR_POSITION_FOR_MENU()
					bMenuInitialised = FALSE
					bMenuRebuild = FALSE
					bConfirmUpdate = FALSE
					iVehicleMenuSelection = -1
					iVehicleMenuControl++
					EXIT
				ENDIF
			ENDIF
		BREAK
		
		// Select vehicle
		CASE 2
			// Build the menu
			IF NOT bMenuInitialised OR bMenuRebuild
			
				CLEAR_MENU_DATA()
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_ICON)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				SET_MENU_TITLE("WEB_VEH_TITLE")
				
				INT i
				REPEAT COUNT_OF(iVehicleMenuOptions) i
					iVehicleMenuOptions[i] = 0
				ENDREPEAT
				INT iFirstMenuItem
				iFirstMenuItem = -1
				
				BOOL bVehicleSelectionCurrent
				bVehicleSelectionCurrent = FALSE
				
				INT iVeh, iItem		
				
				
				GET_VEHICLE_GEN_DATA(sGarageData.sVehGenData, sGarageData.eClosestGen)
				
				// Grab the current item.
				iItem = 0
				
				// Check special vehicles
				INT iDLCVehCount
				iDLCVehCount = GET_NUMBER_OF_SPECIAL_VEHICLES()
				REPEAT iDLCVehCount iVeh
					IF IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(GET_SPECIAL_VEHICLE_MODEL(iVeh))
						
						IF iFirstMenuItem = -1
							iFirstMenuItem = iItem
						ENDIF
						IF sGarageData.sVehGenData.model = GET_SPECIAL_VEHICLE_MODEL(iVeh)
						
							PRINTLN("***Special vehicle is stored as default - veh=", iVeh, ", item=", iItem)
						
							iVehicleMenuSelection = iItem
							bVehicleSelectionCurrent = TRUE
						ENDIF
						iItem++
					ENDIF
				ENDREPEAT
				
				// Check website vehicles
				REPEAT COUNT_OF(g_sVehicleGenNSData.eWebVehicles) iVeh
					IF g_sVehicleGenNSData.eWebVehicles[iVeh] != UNSET_BUYABLE_VEHICLE
						IF GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(g_sVehicleGenNSData.eWebVehicles[iVeh], eCurrentPlayerPed)
						AND g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[g_sVehicleGenNSData.eWebVehicles[iVeh]] = INVALID_TIMEOFDAY
						AND (g_sVehicleGenNSData.eWebVehicles[iVeh] != BV_NG_MARSHALL OR NOT g_savedGlobals.sCountryRaceData.bMarshallUnlocked)
						
							CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh], VehicleSetupMP, FALSE, eCurrentPlayerPed)
							IF iFirstMenuItem = -1
								iFirstMenuItem = iItem
							ENDIF
							IF sGarageData.sVehGenData.model = VehicleSetupMP.VehicleSetup.eModel
							
								PRINTLN("***WEB vehicle is stored as default - veh=", iVeh, ", item=", iItem)
								
								iVehicleMenuSelection = iItem
								bVehicleSelectionCurrent = TRUE
							ENDIF
							iItem++
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF iVehicleMenuSelection = -1
					iVehicleMenuSelection = iFirstMenuItem
				ENDIF

				// Build the menu
				iItem = 0
				
				// Check special vehicles
				iDLCVehCount = GET_NUMBER_OF_SPECIAL_VEHICLES()
				REPEAT iDLCVehCount iVeh
					IF IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(GET_SPECIAL_VEHICLE_MODEL(iVeh))
						
						//PRINTLN("DLC vehicle[", iVeh, "] name = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iVeh)))
						PRINTLN("BUILD MENU: Adding special vehicle ", iVeh, " to menu slot ", iItem, ", label = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iVeh)))
						
						SET_BIT(iVehicleMenuOptions[iItem/32], iItem%32)
						ADD_MENU_ITEM_TEXT(iItem, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iVeh)))
						
						IF iVehicleMenuSelection = iItem
						AND bVehicleSelectionCurrent
						AND sGarageData.iVehicleType != GVT_CAR
							ADD_MENU_ITEM_ICON(iItem, MENU_ICON_TICK)
						ELSE
							ADD_MENU_ITEM_ICON(iItem, MENU_ICON_DUMMY)
						ENDIF
						iItem++
					ENDIF
				ENDREPEAT
				
				// Check for website vehicles
				REPEAT COUNT_OF(g_sVehicleGenNSData.eWebVehicles) iVeh
					IF g_sVehicleGenNSData.eWebVehicles[iVeh] != UNSET_BUYABLE_VEHICLE
						IF GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(g_sVehicleGenNSData.eWebVehicles[iVeh], eCurrentPlayerPed)
						AND g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[g_sVehicleGenNSData.eWebVehicles[iVeh]] = INVALID_TIMEOFDAY
						AND (g_sVehicleGenNSData.eWebVehicles[iVeh] != BV_NG_MARSHALL OR NOT g_savedGlobals.sCountryRaceData.bMarshallUnlocked)
							
							PRINTLN("BUILD MENU: Adding WEB vehicle ", iVeh, " \"", GET_LABEL_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh]), "\" to menu slot ", iItem)
							
							SET_BIT(iVehicleMenuOptions[iItem/32], iItem%32)
							CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh], VehicleSetupMP, FALSE, eCurrentPlayerPed)
							
							IF (g_sVehicleGenNSData.eWebVehicles[iVeh] != BV_DLC_SWIFT)
								ADD_MENU_ITEM_TEXT(iItem, GET_LABEL_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh]))
							ELSE
								IF (VehicleSetupMP.VehicleSetup.iLivery = 0)
									ADD_MENU_ITEM_TEXT(iItem,"TWOSTRINGS",2)
									ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_LABEL_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh]))
									ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VNX_SWFTC")	//Classic
								ELIF (VehicleSetupMP.VehicleSetup.iLivery = 1)
									ADD_MENU_ITEM_TEXT(iItem,"TWOSTRINGS",2)
									ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_LABEL_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh]))
									ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VNX_SWFTB")	//Flying Bravo
								ELSE
									ADD_MENU_ITEM_TEXT(iItem, GET_LABEL_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[iVeh]))
								ENDIF
							ENDIF
							
							IF iVehicleMenuSelection = iItem
							AND bVehicleSelectionCurrent
								ADD_MENU_ITEM_ICON(iItem, MENU_ICON_TICK)
							ELSE
								ADD_MENU_ITEM_ICON(iItem, MENU_ICON_DUMMY)
							ENDIF
							iItem++
						ENDIF
					ENDIF
				ENDREPEAT
				
				SET_CURRENT_MENU_ITEM(iVehicleMenuSelection)
				
				bRebuildHelp = TRUE
				
				bMenuRebuild = FALSE
				bMenuInitialised = TRUE
			
	
			ELSE
			
				///////////////////////////////////
				// Mouse menu support
				bCursorAccept = FALSE
				
				IF IS_PC_VERSION()	
					
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
						
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
						
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
											
						HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
						HANDLE_MENU_CURSOR(FALSE)
						
						IF IS_MENU_CURSOR_ACCEPT_RELEASED()
						
							IF g_iMenuCursorItem != iVehicleMenuSelection
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								iVehicleMenuSelection = g_iMenuCursorItem
								SET_CURRENT_MENU_ITEM(iVehicleMenuSelection)
								bRebuildHelp = TRUE
							ELSE
								bCursorAccept = TRUE
							ENDIF						
						
						ENDIF
					
					ENDIF
				
				ENDIF
			
				// Up
			
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					
					bRebuildHelp = TRUE
					
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					INT i
					BOOL bItemFound
					FOR i = (iVehicleMenuSelection-1) TO 0 STEP -1
						IF IS_BIT_SET(iVehicleMenuOptions[i/32], i%32)
							iVehicleMenuSelection = i
							bItemFound = TRUE
							i = 0 // Bail
						ENDIF
					ENDFOR
					IF NOT bItemFound
					FOR i = (COUNT_OF(iVehicleMenuOptions)*32)-1 TO (iVehicleMenuSelection+1) STEP -1
							IF IS_BIT_SET(iVehicleMenuOptions[i/32], i%32)
								iVehicleMenuSelection = i
								bItemFound = TRUE
							i = 0 // Bail
							ENDIF
						ENDFOR
					ENDIF
					
					IF bItemFound
						SET_CURRENT_MENU_ITEM(iVehicleMenuSelection)
					ENDIF
					
					
				// Down
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				
					bRebuildHelp = TRUE
					
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					INT i
					BOOL bItemFound
				FOR i = (iVehicleMenuSelection+1) TO (COUNT_OF(iVehicleMenuOptions)*32)-1
						IF IS_BIT_SET(iVehicleMenuOptions[i/32], i%32)
							iVehicleMenuSelection = i
							bItemFound = TRUE
						i = (COUNT_OF(iVehicleMenuOptions)*32)+1 // Bail
						ENDIF
					ENDFOR
					IF NOT bItemFound 
						FOR i = 0 TO (iVehicleMenuSelection-1)
							IF IS_BIT_SET(iVehicleMenuOptions[i/32], i%32)
								iVehicleMenuSelection = i
								bItemFound = TRUE
							i = (COUNT_OF(iVehicleMenuOptions)*32)+1 // Bail
							ENDIF
						ENDFOR
					ENDIF
					
					IF bItemFound
						SET_CURRENT_MENU_ITEM(iVehicleMenuSelection)
					ENDIF
					
				// Select
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR bCursorAccept = TRUE
				
					BOOL bVehicleSelected
					bVehicleSelected = FALSE
					bCursorAccept = FALSE
					
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					// Update stored vehicle
					INT i, j
					j = 0
					
					// Special vehicles
					INT iDLCVehCount
					iDLCVehCount = GET_NUMBER_OF_SPECIAL_VEHICLES()
					REPEAT iDLCVehCount i
						IF IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(GET_SPECIAL_VEHICLE_MODEL(i))
					
							IF iVehicleMenuSelection = j
							AND (sGarageData.sVehGenData.model != GET_SPECIAL_VEHICLE_MODEL(i)
									OR NOT DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
									OR NOT IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
									OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR)
							
							
								PRINTLN("***DLC vehicle selected veh=", i, ", item=", j)
							
								bVehicleSelected = TRUE
							
								// Confirm
								IF (NOT bConfirmUpdate AND sGarageData.sVehGenData.model != GET_SPECIAL_VEHICLE_MODEL(i))
								OR (NOT bConfirmUpdate AND sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL)
								OR (NOT bConfirmUpdate AND sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN)
								OR (NOT bConfirmUpdate AND sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR)
								
	//								IF sGarageData.iVehicleType = GVT_CAR
	//									SET_CURRENT_MENU_ITEM_DESCRIPTION("VEH_SELECT_CNF")
	//								ELSE
										SET_CURRENT_MENU_ITEM_DESCRIPTION("VEH_SELECT_CNFA")
	//								ENDIF
									REMOVE_MENU_HELP_KEYS()
									ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "ITEM_YES")
									ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "ITEM_NO")
									bConfirmUpdate = TRUE
									i = GET_NUMBER_OF_SPECIAL_VEHICLES()+1 // Bail
								ELSE
									IF sGarageData.iVehicleType = GVT_CAR
										IF HAS_SPAWN_VEHICLE_GOT_CUSTOM_SETUP(GET_SPECIAL_VEHICLE_MODEL(i))
											eDLCModel = GET_SPECIAL_VEHICLE_MODEL(i)
											bMenuInitialised = FALSE
											bMenuRebuild = FALSE
											iCustomVehicleSelection = 0
											iVehicleMenuControl++
										ELSE
											bExitGarageInDLCVehicle = TRUE // Spawn player inside DLC vehicle
											eDLCModel = GET_SPECIAL_VEHICLE_MODEL(i)
											iVehicleMenuControl = 99
										ENDIF
									ELIF sGarageData.iVehicleType = GVT_PLANE // MARSHALL GOES TO HANGAR
									AND HAS_SPAWN_VEHICLE_GOT_CUSTOM_SETUP(GET_SPECIAL_VEHICLE_MODEL(i))
										eDLCModel = GET_SPECIAL_VEHICLE_MODEL(i)
										bMenuInitialised = FALSE
										bMenuRebuild = FALSE
										iCustomVehicleSelection = 0
										iVehicleMenuControl++
									ELSE
										// Delete the previous vehicle if it still exists
										IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
											SET_ENTITY_AS_MISSION_ENTITY(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen], FALSE, TRUE)
											DELETE_VEHICLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
										ENDIF
										
										sDLCVeh.eModel = GET_SPECIAL_VEHICLE_MODEL(i)
										SET_BIT(sDLCVeh.iFlags, VEHICLE_SETUP_FLAG_KEEP_DEFAULT_EXTRAS)
									
//										// Fix for bug # 1950776 - Pick a random livery for this vehicle
//										IF sDLCVeh.eModel = SWIFT
//											sDLCVeh.iLivery = GET_RANDOM_INT_IN_RANGE(0, 2) // Possible return values {0,1}
//										ENDIF
									
										UPDATE_DYNAMIC_VEHICLE_GEN_DATA(sGarageData.eClosestGen, sDLCVeh, <<0,0,0>>, -1)
										CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(sGarageData.eClosestGen)
										
										// Update the vehicle data...
										GET_VEHICLE_GEN_DATA(sGarageData.sVehGenData, sGarageData.eClosestGen)
										
										i = GET_NUMBER_OF_SPECIAL_VEHICLES()+1 // Bail
										bMenuRebuild = TRUE
									ENDIF
								ENDIF
							ENDIF
							j++
						ENDIF
					ENDREPEAT
					
					// Website vehicles
					IF NOT bVehicleSelected
					
						REPEAT COUNT_OF(g_sVehicleGenNSData.eWebVehicles) i
							IF g_sVehicleGenNSData.eWebVehicles[i] != UNSET_BUYABLE_VEHICLE
								IF GET_HAS_VEHICLE_BEEN_BOUGHT_FROM_WEBSITE(g_sVehicleGenNSData.eWebVehicles[i], eCurrentPlayerPed)
								AND g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[g_sVehicleGenNSData.eWebVehicles[i]] = INVALID_TIMEOFDAY
								AND (g_sVehicleGenNSData.eWebVehicles[i] != BV_NG_MARSHALL OR NOT g_savedGlobals.sCountryRaceData.bMarshallUnlocked)
									
									CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[i], VehicleSetupMP, FALSE, eCurrentPlayerPed)
									IF iVehicleMenuSelection = j
									AND (sGarageData.sVehGenData.model != VehicleSetupMP.VehicleSetup.eModel
										OR NOT DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
										OR NOT IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]))
										
										PRINTLN("***WEB vehicle selected veh=", i, ", item=", j, ", VehicleSetupMP.VehicleSetup.eModel=", GET_MODEL_NAME_FOR_DEBUG(VehicleSetupMP.VehicleSetup.eModel))
									
										bVehicleSelected = TRUE
										
										// Confirm
										IF NOT bConfirmUpdate
											IF sGarageData.iVehicleType = GVT_CAR
												SET_CURRENT_MENU_ITEM_DESCRIPTION("VEH_SELECT_CNF")
											ELSE
												SET_CURRENT_MENU_ITEM_DESCRIPTION("VEH_SELECT_CNFA")
											ENDIF
											REMOVE_MENU_HELP_KEYS()
											ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "ITEM_YES")
											ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "ITEM_NO")
											bConfirmUpdate = TRUE
											i = COUNT_OF(g_sVehicleGenNSData.eWebVehicles)+1 // Bail
										ELIF VehicleSetupMP.VehicleSetup.eModel = MARSHALL
											iVehicleMenuControl = 3
											bMenuRebuild = TRUE
											i = COUNT_OF(g_sVehicleGenNSData.eWebVehicles)+1 // Bail
											
										ELSE
											// Delete the previous vehicle if it still exists
											IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
												SET_ENTITY_AS_MISSION_ENTITY(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen], FALSE, TRUE)
												DELETE_VEHICLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
											ENDIF
											
											CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(g_sVehicleGenNSData.eWebVehicles[i], VehicleSetupMP, FALSE, eCurrentPlayerPed)
											SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_KEEP_DEFAULT_EXTRAS)
											
											// Deploy landing gear for planes
											IF IS_THIS_MODEL_A_PLANE(VehicleSetupMP.VehicleSetup.eModel)
												SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_LANDING_GEAR_OUT)
											ENDIF
											
											UPDATE_DYNAMIC_VEHICLE_GEN_DATA(sGarageData.eClosestGen, VehicleSetupMP.VehicleSetup, <<0,0,0>>, -1)
											CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(sGarageData.eClosestGen)
											
											// Update the vehicle data...
											GET_VEHICLE_GEN_DATA(sGarageData.sVehGenData, sGarageData.eClosestGen)
											
											i = COUNT_OF(g_sVehicleGenNSData.eWebVehicles)+1 // Bail
											bMenuRebuild = TRUE
										ENDIF
									ENDIF
									j++
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
				// Exit/Decline
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				
					PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					// Decline
					IF bConfirmUpdate
						bRebuildHelp = TRUE
						bConfirmUpdate = FALSE
					ELSE
						iVehicleMenuControl = 99
					ENDIF
				ENDIF
				
			ENDIF
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF bRebuildHelp
				SET_CURRENT_MENU_ITEM_DESCRIPTION("")
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "ITEM_EXIT")
				bConfirmUpdate = FALSE
				bRebuildHelp = FALSE
			ENDIF
			
			DRAW_MENU()
		BREAK
		
		// Select custom setup
		CASE 3
			// Build the menu
			IF NOT bMenuInitialised OR bMenuRebuild
			
				CLEAR_MENU_DATA()
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
				
				INT i
				REPEAT COUNT_OF(iCustomVehicleOptions) i
					iCustomVehicleOptions[i] = 0
				ENDREPEAT
				
				// Add all the custom spawn options for this vehicle.
				SET_MENU_TITLE("WEB_VEH_TITLE2") // SELECT FLAG
				TEXT_LABEL_15 tlTempLabel
				REPEAT 25 i
					tlTempLabel = "WEB_VEH_FLAG_"
					tlTempLabel += i
					ADD_MENU_ITEM_TEXT(i, tlTempLabel)
					SET_BIT(iCustomVehicleOptions[i/32], i%32)
				ENDREPEAT
				
				IF sData.ped = CHAR_MICHAEL
					iCustomVehicleSelection = ENUM_TO_INT(g_savedGlobals.sBuyableVehicleSavedData.g_eOwnedVehicleColoursM[BV_NG_MARSHALL])
				ELIF sData.ped = CHAR_FRANKLIN
					iCustomVehicleSelection = ENUM_TO_INT(g_savedGlobals.sBuyableVehicleSavedData.g_eOwnedVehicleColoursF[BV_NG_MARSHALL])
				ELSE
					iCustomVehicleSelection = ENUM_TO_INT(g_savedGlobals.sBuyableVehicleSavedData.g_eOwnedVehicleColoursT[BV_NG_MARSHALL])
				ENDIF
				SET_CURRENT_MENU_ITEM(iCustomVehicleSelection)
				
				bRebuildHelp = TRUE
				bMenuRebuild = FALSE
				bMenuInitialised = TRUE
			ELSE
			
				///////////////////////////////////
				// Mouse menu support
				bCursorAccept = FALSE
				
				IF IS_PC_VERSION()
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
						
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
											
						HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
						HANDLE_MENU_CURSOR(FALSE)
						
						IF IS_MENU_CURSOR_ACCEPT_RELEASED()
							IF g_iMenuCursorItem != iCustomVehicleSelection
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								iCustomVehicleSelection = g_iMenuCursorItem
								SET_CURRENT_MENU_ITEM(iCustomVehicleSelection)
								bRebuildHelp = TRUE
							ELSE
								bCursorAccept = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// Up
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					
					bRebuildHelp = TRUE
					
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					INT i
					BOOL bItemFound
					FOR i = (iCustomVehicleSelection-1) TO 0 STEP -1
						IF IS_BIT_SET(iCustomVehicleOptions[i/32], i%32)
							iCustomVehicleSelection = i
							bItemFound = TRUE
							i = 0 // Bail
						ENDIF
					ENDFOR
					IF NOT bItemFound
						FOR i = 31 TO (iCustomVehicleSelection+1) STEP -1
							IF IS_BIT_SET(iCustomVehicleOptions[i/32], i%32)
								iCustomVehicleSelection = i
								bItemFound = TRUE
								i = iCustomVehicleSelection // Bail
							ENDIF
						ENDFOR
					ENDIF
					
					IF bItemFound
						SET_CURRENT_MENU_ITEM(iCustomVehicleSelection)
					ENDIF
					
					
				// Down
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				
					bRebuildHelp = TRUE
					
					PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					INT i
					BOOL bItemFound
					FOR i = (iCustomVehicleSelection+1) TO 31
						IF IS_BIT_SET(iCustomVehicleOptions[i/32], i%32)
							iCustomVehicleSelection = i
							bItemFound = TRUE
							i = 31 // Bail
						ENDIF
					ENDFOR
					IF NOT bItemFound 
						FOR i = 0 TO (iCustomVehicleSelection-1)
							IF IS_BIT_SET(iCustomVehicleOptions[i/32], i%32)
								iCustomVehicleSelection = i
								bItemFound = TRUE
								i = iCustomVehicleSelection // Bail
							ENDIF
						ENDFOR
					ENDIF
					
					IF bItemFound
						SET_CURRENT_MENU_ITEM(iCustomVehicleSelection)
					ENDIF
					
				// Select
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR bCursorAccept = TRUE
				
					bCursorAccept = FALSE
					
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					// Delete the previous vehicle if it still exists
					IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
						SET_ENTITY_AS_MISSION_ENTITY(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen], FALSE, TRUE)
						DELETE_VEHICLE(g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen])
					ENDIF
					
					SITE_BUYABLE_VEHICLE_COLOURS eCol
					eCol = INT_TO_ENUM(SITE_BUYABLE_VEHICLE_COLOURS, iCustomVehicleSelection+1)
					
					PRINTLN("***WEB vehicle selected eCol=", eCol)
					
					IF eCurrentPlayerPed = CHAR_MICHAEL
			            g_savedGlobals.sBuyableVehicleSavedData.g_eOwnedVehicleColoursM[BV_NG_MARSHALL] = eCol
			        ELIF eCurrentPlayerPed = CHAR_FRANKLIN
			            g_savedGlobals.sBuyableVehicleSavedData.g_eOwnedVehicleColoursF[BV_NG_MARSHALL] = eCol
			        ELIF eCurrentPlayerPed = CHAR_TREVOR
			            g_savedGlobals.sBuyableVehicleSavedData.g_eOwnedVehicleColoursT[BV_NG_MARSHALL] = eCol
					ELSE
						PRINTLN("***WEB vehicle selected invalid eCurrentPlayerPed=", eCurrentPlayerPed)
			        ENDIF
					
					CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(BV_NG_MARSHALL, VehicleSetupMP, FALSE, eCurrentPlayerPed)
					SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_KEEP_DEFAULT_EXTRAS)
					
					// Deploy landing gear for planes
					IF IS_THIS_MODEL_A_PLANE(VehicleSetupMP.VehicleSetup.eModel)
						SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_LANDING_GEAR_OUT)
					ENDIF
					
					UPDATE_DYNAMIC_VEHICLE_GEN_DATA(sGarageData.eClosestGen, VehicleSetupMP.VehicleSetup, <<0,0,0>>, -1)
					CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(sGarageData.eClosestGen)
					
					// Update the vehicle data...
					GET_VEHICLE_GEN_DATA(sGarageData.sVehGenData, sGarageData.eClosestGen)
					
					bMenuRebuild = TRUE
					iVehicleMenuControl = 2
					
				// Backup
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				
					PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					// Backup
					bMenuInitialised = FALSE
					bMenuRebuild = FALSE
					iVehicleMenuControl--
				ENDIF
				
			ENDIF
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF bRebuildHelp
				SET_CURRENT_MENU_ITEM_DESCRIPTION("")
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "ITEM_EXIT")
				bConfirmUpdate = FALSE
				bRebuildHelp = FALSE
			ENDIF
			
			DRAW_MENU()
		BREAK
		
		CASE 70
			
			
			
		BREAK
		
		CASE 99
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF NOT IS_PLAYER_VEH_RADIO_ENABLE()
						SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE) // B*1157533 - disable radio if player is using selector in a vehicle
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_VEHICLE_SELECT -> player's vehicle radio enabled")	
					ENDIF
				ENDIF
			ENDIF
			CLEANUP_MENU_ASSETS()
			iVehicleMenuControl = 0
			RELEASE_CONTEXT_INTENTION(iVehicleMenuContextID)
			iVehicleMenuContextID = NEW_CONTEXT_INTENTION
		BREAK
	ENDSWITCH
	
	IF NOT bDisplayNoVehicleHelp
	AND IS_HELP_MESSAGE_BEING_DISPLAYED()
		IF NOT IS_STRING_NULL_OR_EMPTY(tlSelectHelpPrinted)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HANGAR_NO")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MARINA_NO")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELIPAD_NO")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR_GAR_NO_1")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR_GAR_NO_2")
				CLEAR_HELP()
			ENDIF
			tlSelectHelpPrinted = ""
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Update the dyanmic vehicle gen if player lands/parks a new vehicle on the spot.
PROC PROCESS_GARAGE_VEHICLE_DROPOFF()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF sGarageData.sVehGenPurchData.bDataSet
		AND NOT sGarageData.sVehGenPurchData.bWarpToGarage // no longer allowing car garage to use dropoff method.
		AND sGarageData.eClosestGen != VEHGEN_NONE
		AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_AVAILABLE)
		AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_ACQUIRED)
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("michael1")) = 0
			
			IF NOT DOES_ENTITY_EXIST(sGarageData.vehTrack)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(vehTemp)
					AND IS_VEHICLE_DRIVEABLE(vehTemp)
						MODEL_NAMES eModel = GET_ENTITY_MODEL(vehTemp)
						
						IF IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(eModel)
							sGarageData.vehTrack = vehTemp
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_VEHICLE_DRIVEABLE(sGarageData.vehTrack)
			AND IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(GET_ENTITY_MODEL(sGarageData.vehTrack))
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sGarageData.vehTrack)
					IF IS_ENTITY_IN_ANGLED_AREA(sGarageData.vehTrack, sGarageData.sVehGenPurchData.vStorePos1, sGarageData.sVehGenPurchData.vStorePos2, sGarageData.sVehGenPurchData.fStoreWidth)
					
						IF NOT IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(sGarageData.vehTrack)
						AND NOT IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(sGarageData.vehTrack)
						AND sGarageData.vehTrack != g_sVehicleGenNSData.vehicleSelectID[sGarageData.eClosestGen]
							#IF IS_DEBUG_BUILD
								PRINTLN("PROCESS_GARAGE_VEHICLE_DROPOFF - Player has left a vehicle in the garage area for vehgen ", sGarageData.sVehGenData.dbg_name, ".")
							#ENDIF
							
							SET_VEHICLE_GEN_VEHICLE(sGarageData.eClosestGen, sGarageData.vehTrack)
						ENDIF
						sGarageData.vehTrack = NULL	
					ENDIF
				ENDIF
			ELSE
				sGarageData.vehTrack = NULL
			ENDIF
		ELSE
			sGarageData.vehTrack = NULL
		ENDIF
	ENDIF

ENDPROC

FUNC VEHICLE_GEN_NAME_ENUM GET_GARAGE_VEHICLE_GEN_PLAYER_IS_IN()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_CAR_MICHAEL]	RETURN VEHGEN_WEB_CAR_MICHAEL ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_MICHAEL_GARAGE_1]	RETURN VEHGEN_MICHAEL_GARAGE_1 ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_MICHAEL_GARAGE_2]	RETURN VEHGEN_MICHAEL_GARAGE_2 ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_MICHAEL_GARAGE_3]	RETURN VEHGEN_MICHAEL_GARAGE_3 ENDIF
			
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_CAR_FRANKLIN]		RETURN VEHGEN_WEB_CAR_FRANKLIN ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_FRANKLIN_GARAGE_1]	RETURN VEHGEN_FRANKLIN_GARAGE_1 ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_FRANKLIN_GARAGE_2]	RETURN VEHGEN_FRANKLIN_GARAGE_2 ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_FRANKLIN_GARAGE_3]	RETURN VEHGEN_FRANKLIN_GARAGE_3 ENDIF
			
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_WEB_CAR_TREVOR]	RETURN VEHGEN_WEB_CAR_TREVOR ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_TREVOR_GARAGE_1]	RETURN VEHGEN_TREVOR_GARAGE_1 ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_TREVOR_GARAGE_2]	RETURN VEHGEN_TREVOR_GARAGE_2 ENDIF
			IF vehID = g_sVehicleGenNSData.vehicleSelectID[VEHGEN_TREVOR_GARAGE_3]	RETURN VEHGEN_TREVOR_GARAGE_3 ENDIF
		ENDIF
	ENDIF
	RETURN VEHGEN_NONE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SUITABLE_STAGE_FOR_GARAGE()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	OR GET_FREE_VEHICLE_GEN_SLOT_IN_GARAGE(sGarageData.eClosestGen) != VEHGEN_NONE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC EARLY_REQUEST_GARAGE_VEHICLES(VEHICLE_GEN_NAME_ENUM eGarage)
	INT i, iSlot	//Dynamic vehicle slot
	VEHICLE_GEN_NAME_ENUM eName
	REPEAT 4 i	//4 vehicle gens per garage
		eName = eGarage
		iSlot = 9 + ENUM_TO_INT(eName - VEHGEN_WEB_CAR_MICHAEL) //Web_Car_Michael starts at 1
		IF i > 0
			//This section grabs all the related player garage vehicle slots, check the Enum order for the formula
			//If the enums change/move this WILL BREAK
			eName = INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM,ENUM_TO_INT(eName) + 2 + i*3)
			iSlot = 9 + ENUM_TO_INT(eName - VEHGEN_WEB_CAR_MICHAEL) - 2
		ENDIF
		eVehicleModel[eName] = g_savedGlobals.sVehicleGenData.sDynamicData[iSlot].eModel
		IF eVehicleModel[eName] != INT_TO_ENUM(MODEL_NAMES, 0)
			IF NOT IS_BIT_SET(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
				REQUEST_MODEL(eVehicleModel[eName])
				SET_BIT(iVehGenProcessFlags[eName], VEHGEN_BIT_FLAG_VEHICLE_MODEL_REQ)
				ADD_VEHICLE_GEN_TO_PROCESS_LIST(eName)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC		

/// PURPOSE: Warp the player into their garage and manage vehicles.
PROC PROCESS_GARAGE_VEHICLE_WARP()

	// Note, we only deal with v_garagem in singleplayer to cool to hardcode values here for quickness.
	
	PED_INDEX nearbyPeds[10]
	BOOL bPrintWarpHelp = FALSE
	VEHICLE_INDEX currentVeh
	
	CONST_INT iCONST_WARP_0					0
	CONST_INT iCONST_WARP_1					1
	CONST_INT iCONST_WARP_2					2
	CONST_INT iCONST_WARP_3_enterVeh		3
	CONST_INT iCONST_WARP_3_5				35
	CONST_INT iCONST_WARP_4_enterPed		4
	CONST_INT iCONST_WARP_5_fadingOutPed	5
	CONST_INT iCONST_WARP_6_fadingInPed		6
	
	CONST_INT iCONST_WARP_10				10
	CONST_INT iCONST_WARP_11_exitPed		11
	CONST_INT iCONST_WARP_12_exitVehInt		12
	CONST_INT iCONST_WARP_13_exitFadingOut	13
	CONST_INT iCONST_WARP_14_exitFadein		14
	CONST_INT iCONST_WARP_15_exitVehExt		15
	CONST_INT iCONST_WARP_16				16
	CONST_INT iCONST_WARP_17				17
	CONST_INT iCONST_WARP_18				18
	
	IF (sGarageData.sVehGenPurchData.bDataSet
	AND sGarageData.sVehGenPurchData.bWarpToGarage
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND sGarageData.iPurchaseControl = 0
	AND sGarageData.eClosestGen != VEHGEN_NONE
	AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_AVAILABLE)
	AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_ACQUIRED)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	AND NOT g_bBrowserVisible
	AND NOT IS_REPEAT_PLAY_ACTIVE(TRUE)
	AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS())

	OR (sGarageData.iWarpControl > iCONST_WARP_1 AND NOT IS_REPEAT_PLAY_ACTIVE() AND NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS())
		
		Private_GET_GARAGE_VEHICLE_WARP_DATA(sGarageData.eClosestGen, sGarageVehicleWarpData)
		
		FLOAT fStrOffset = 0.0
		
		#IF IS_DEBUG_BUILD
		INT iRed, iGreen, iBlue, iAlpha
		IF sGarageData.iWarpControl = iCONST_WARP_0
			GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, iAlpha)
		ELIF sGarageData.iWarpControl < iCONST_WARP_10
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, iRed, iGreen, iBlue, iAlpha)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
		ENDIF
		TEXT_LABEL_63 str = "sGarageData.iWarpControl: iCONST_WARP_"
		str += sGarageData.iWarpControl
		IF sGarageData.iWarpControl > iCONST_WARP_0
			DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
		ENDIF
		#ENDIF
		
		STRING sOpenGarageAnimDict = "ANIM@APT_TRANS@GARAGE"
		VECTOR vGarageM_SP_coord = <<198.3659, -1020.2732, -100.0000>>
		VECTOR vGarageInteriorPos = vGarageM_SP_coord
		VECTOR vGarageOnfootWarp = <<198.9538, -1026.1301, -100.0000>>
		
		CONST_INT	iGarageHold			2250
		CONST_FLOAT	fGARAGE_OPEN_PHASE	0.4		//0.5
		
		enumCharacterList eClosestGarageOwner = GET_PURCHASABLE_GARAGE_OWNER(sGarageData.eClosestGen)
		
		SWITCH sGarageData.iWarpControl
			CASE iCONST_WARP_0
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<207.433578,-1019.795410,-100.472763>>, <<189.933777,-1019.623474,-95.568832>>, 17.187500)
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					sGarageData.iWarpControl = iCONST_WARP_10
					PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - set player as \"inside garage\" since he IS inside garage!")
					
					EXIT
				ENDIF
				
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vBuyPos1, sGarageData.sVehGenPurchData.vBuyPos2, sGarageData.sVehGenPurchData.fBuyWidth)
				AND (IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), sGarageData.sVehGenPurchData.fDoorHeading, 90.0) OR IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_PED_BEING_JACKED(PLAYER_PED_ID())
				AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				AND (eClosestGarageOwner = eCurrentPlayerPed OR eClosestGarageOwner = NO_CHARACTER)
					IF IS_PLAYER_IN_SUITABLE_STAGE_FOR_GARAGE()
						BOOL bSafe
						bSafe = TRUE
						
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
							IF DOES_ENTITY_EXIST(currentVeh)
								IF IS_VEHICLE_DRIVEABLE(currentVeh)
									MODEL_NAMES eModel
									eModel = GET_ENTITY_MODEL(currentVeh)
									IF NOT IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(eModel)
									OR IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(currentVeh)
									OR IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(currentVeh)
									OR NOT IS_VEHICLE_AVAILABLE_FOR_GAME(eModel)
									OR IS_BIG_VEHICLE(currentVeh)
									OR (NOT IS_THIS_MODEL_A_CAR(eModel) AND NOT IS_THIS_MODEL_A_BIKE(eModel) AND NOT IS_THIS_MODEL_A_QUADBIKE(eModel))
									OR eModel = MONSTER
									OR IS_ENTITY_ON_FIRE(currentVeh)
									OR IS_VEHICLE_ATTACHED_TO_TRAILER(currentVeh)
									OR (IS_PED_IN_ANY_TAXI(PLAYER_PED_ID()) AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("taxi_procedural")) > 0) 
										bSafe = FALSE
									ENDIF
								ELSE
									bSafe = FALSE
								ENDIF
							ENDIF
						ELSE
							IF DOES_ANIM_DICT_EXIST(sOpenGarageAnimDict)
								REQUEST_ANIM_DICT(sOpenGarageAnimDict)
								IF NOT HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
									bSafe = FALSE
								ENDIF
							ENDIF
						ENDIF
						
						IF bSafe
							REGISTER_CONTEXT_INTENTION(iGarageEntryContextID, CP_MEDIUM_PRIORITY, "WEB_VEH_ENTER")	// Press ~a~ to enter garage.
							bFakeGarageReverse = FALSE
							sGarageData.iWarpControl = iCONST_WARP_1
						ELSE
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WEB_VEH_INV")
									PRINT_HELP("WEB_VEH_INV")
									tlWarpHelpPrinted = "WEB_VEH_INV"
									bPrintWarpHelp = TRUE
								ENDIF
							ELSE
								//waiting on animation...
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WEB_VEH_FULL")
							PRINT_HELP("WEB_VEH_FULL")
							tlWarpHelpPrinted = "WEB_VEH_FULL"
							bPrintWarpHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_WARP_1
				
				IF NOT bAnimDictLoadSceneStartedFromVehGen
					IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						bAnimDictLoadSceneStartedFromVehGen = FALSE
						
						REQUEST_ANIM_DICT(sOpenGarageAnimDict)
						IF HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
							PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - anim dict \"", sOpenGarageAnimDict, "\" loaded")
							
							bAnimDictLoadSceneStartedFromVehGen = TRUE
						ENDIF
					ELSE
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - remove anim dict \"", sOpenGarageAnimDict, "\"")
						REMOVE_ANIM_DICT(sOpenGarageAnimDict)
						
						bAnimDictLoadSceneStartedFromVehGen = TRUE
					ENDIF
				ENDIF
				
				BOOL bSafe
				bSafe = TRUE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
					IF DOES_ENTITY_EXIST(currentVeh)
						IF IS_VEHICLE_DRIVEABLE(currentVeh)
							MODEL_NAMES eModel
							eModel = GET_ENTITY_MODEL(currentVeh)
							IF NOT IS_VEHICLE_MODEL_SUITABLE_FOR_DYNAMIC_UPDATE(eModel)
							OR IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(currentVeh)
							OR IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(currentVeh)
							OR IS_BIG_VEHICLE(currentVeh)
							OR (NOT IS_THIS_MODEL_A_CAR(eModel) AND NOT IS_THIS_MODEL_A_BIKE(eModel) AND NOT IS_THIS_MODEL_A_QUADBIKE(eModel))
							OR eModel = MONSTER
							OR IS_ENTITY_ON_FIRE(currentVeh)
							OR IS_VEHICLE_ATTACHED_TO_TRAILER(currentVeh)
							OR (IS_PED_IN_ANY_TAXI(PLAYER_PED_ID()) AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("taxi_procedural")) > 0) 
								bSafe = FALSE
							ENDIF
						ELSE	
							bSafe = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vBuyPos1, sGarageData.sVehGenPurchData.vBuyPos2, sGarageData.sVehGenPurchData.fBuyWidth)
				AND (IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), sGarageData.sVehGenPurchData.fDoorHeading, 90.0) OR IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND IS_PLAYER_IN_SUITABLE_STAGE_FOR_GARAGE()
				AND NOT IS_PED_BEING_JACKED(PLAYER_PED_ID())
				AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
				AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				AND (eClosestGarageOwner = eCurrentPlayerPed OR eClosestGarageOwner = NO_CHARACTER)
				AND bSafe
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					OR (HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict) OR NOT DOES_ANIM_DICT_EXIST(sOpenGarageAnimDict))
						IF HAS_CONTEXT_BUTTON_TRIGGERED(iGarageEntryContextID)
							RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
							bAnimDictLoadSceneStartedFromVehGen = FALSE
							sGarageData.iWarpControl = iCONST_WARP_2
						ENDIF
						
						SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, FALSE)
						SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, FALSE)
													
						//pin the interior into memory	///
						sGarageData.interiorInstanceIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vGarageM_SP_coord, "v_garagem_sp")
						IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
							IF NOT IS_INTERIOR_READY(sGarageData.interiorInstanceIndex)
								//PIN_INTERIOR_IN_MEMORY(sGarageData.interiorInstanceIndex)	
								#IF IS_DEBUG_BUILD
								str  = "interior NOT READY"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
								#ENDIF
						
								
								IF GET_FRAME_COUNT() % 10 = 0
									#IF IS_DEBUG_BUILD
									str  = "interior REQUESTING"
									DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
									#ENDIF
									PIN_INTERIOR_IN_MEMORY(sGarageData.interiorInstanceIndex)
								ENDIF
						
								CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> interior pinned in memory request. frame = ", GET_FRAME_COUNT())
							ELSE
								#IF IS_DEBUG_BUILD
								str  = "interior ready"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								#ENDIF
							ENDIF
							g_tlIgnoreBuildingControllerChecks = "v_garagem_sp"
						ELSE
							#IF IS_DEBUG_BUILD
							str  = "interior NOT VALID"
							DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
							#ENDIF
					
						ENDIF
				
						
						//*********************
				
				
						IF NOT bLoadSceneStartedFromVehGen
						AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								VEHICLE_GEN_NAME_ENUM eCurrentVehGen
								eCurrentVehGen = GET_FREE_VEHICLE_GEN_SLOT_IN_GARAGE(sGarageData.eClosestGen)
								
								VEHICLE_GEN_DATA_STRUCT sVehGenData
								GET_VEHICLE_GEN_DATA(sVehGenData, eCurrentVehGen)
								vGarageInteriorPos = sVehGenData.coords
								
								PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - start vehicle load scene [", vGarageInteriorPos, ", eCurrentVehGen: ", eCurrentVehGen, "]")
								NEW_LOAD_SCENE_START_SPHERE(vGarageInteriorPos, 20.0)
							ELSE
								PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - start ped load scene [", vGarageOnfootWarp, "+<<0,0,1>>]")
								NEW_LOAD_SCENE_START_SPHERE(vGarageOnfootWarp+<<0,0,1>>, 20.0)
							ENDIF
							bLoadSceneStartedFromVehGen = TRUE
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF bLoadSceneStartedFromVehGen
							IF (NOT IS_NEW_LOAD_SCENE_ACTIVE())
								#IF IS_DEBUG_BUILD
								str  = "load scene not active"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
								#ENDIF
								
							ELIF (IS_NEW_LOAD_SCENE_LOADED())
								#IF IS_DEBUG_BUILD
								str  = "load scene loaded"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								#ENDIF
								
							ELSE
								#IF IS_DEBUG_BUILD
								str  = "wait for load scene"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
								#ENDIF
							ENDIF
						ENDIF
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), sGarageData.sVehGenPurchData.fDoorHeading, 90.0, TRUE)
								str  = "heading is acceptable"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ELSE
								str  = "heading is NOT acceptable"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ENDIF
						ELSE
							IF NOT DOES_ANIM_DICT_EXIST(sOpenGarageAnimDict)
								str  = "anim dict \""
								str += (sOpenGarageAnimDict)
								str += "\" doesn't exist"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_BLACK)
							ELIF HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
								str  = "loaded anim dict \""
								str += (sOpenGarageAnimDict)
								str += "\""
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ELSE
								str  = "waiting for anim dict \""
								str += (sOpenGarageAnimDict)
								str += "\""
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ENDIF
							
							VECTOR vBuyMidPos1, vBuyMidPos2
							vBuyMidPos1 = ((sGarageData.sVehGenPurchData.vBuyPos1*2.0)+(sGarageData.sVehGenPurchData.vBuyPos2*1.0))/3.0
							vBuyMidPos2 = ((sGarageData.sVehGenPurchData.vBuyPos1*1.0)+(sGarageData.sVehGenPurchData.vBuyPos2*2.0))/3.0
							
							BOOL bFoundAngledArea
							bFoundAngledArea = FALSE
							
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
									sGarageData.sVehGenPurchData.vBuyPos1,
									<<vBuyMidPos1.x, vBuyMidPos1.y, sGarageData.sVehGenPurchData.vBuyPos2.z>>,
									sGarageData.sVehGenPurchData.fBuyWidth)
								str  = "left garage clip"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								bFoundAngledArea = TRUE
							ENDIF
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
									<<vBuyMidPos1.x, vBuyMidPos1.y, sGarageData.sVehGenPurchData.vBuyPos1.z>>,
									<<vBuyMidPos2.x, vBuyMidPos2.y, sGarageData.sVehGenPurchData.vBuyPos2.z>>,
									sGarageData.sVehGenPurchData.fBuyWidth)
								str  = "centre garage clip"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								bFoundAngledArea = TRUE
							ENDIF
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
									<<vBuyMidPos2.x, vBuyMidPos2.y, sGarageData.sVehGenPurchData.vBuyPos1.z>>,
									sGarageData.sVehGenPurchData.vBuyPos2,
									sGarageData.sVehGenPurchData.fBuyWidth)
								str  = "right garage clip"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								bFoundAngledArea = TRUE
							ENDIF
							IF NOT bFoundAngledArea
								str  = "UNKNOWN garage clip"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ENDIF
						ENDIF
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							str  = "ped sitting in any vehicle"
							DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						ELSE
							IF NOT DOES_ANIM_DICT_EXIST(sOpenGarageAnimDict)
								str  = "anim dict \""
								str += (sOpenGarageAnimDict)
								str += "\" doesn't exist"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_BLACK)
							ELIF HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
								str  = "loaded anim dict \""
								str += (sOpenGarageAnimDict)
								str += "\""
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ELSE
								str  = "waiting for anim dict \""
								str += (sOpenGarageAnimDict)
								str += "\""
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
							ENDIF
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					IF bLoadSceneStartedFromVehGen
					AND IS_NEW_LOAD_SCENE_ACTIVE()
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - stop load scene")
						NEW_LOAD_SCENE_STOP()
						bLoadSceneStartedFromVehGen = FALSE
					ENDIF
					
					bAnimDictLoadSceneStartedFromVehGen = FALSE
					RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
					REMOVE_ANIM_DICT(sOpenGarageAnimDict)
					sGarageData.iWarpControl = iCONST_WARP_0
				ENDIF
			BREAK
			CASE iCONST_WARP_2
				SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, FALSE)
				
				FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
				
				g_sVehicleGenNSData.bInGarage = TRUE
				g_sVehicleGenNSData.vGarageEntryCoords = sGarageData.sVehGenPurchData.vBlipCoords
				
				iWantedLevelBeforeWarp = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(currentVeh)
					AND IS_VEHICLE_DRIVEABLE(currentVeh)
						
						IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(currentVeh), sGarageData.sVehGenPurchData.fDoorHeading, 90.0 #IF IS_DEBUG_BUILD , TRUE #ENDIF)
							bFakeGarageReverse = FALSE
						ELSE
							bFakeGarageReverse = TRUE
						ENDIF
						
						SET_ENTITY_PROOFS(currentVeh, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
						
						CLEAR_AREA_OF_OBJECTS(sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos, 20)
						CLEAR_AREA_OF_PROJECTILES(sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos, 20)
						
						REMOVE_PARTICLE_FX_IN_RANGE(sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos, 7)		
						
						SET_ENTITY_COORDS(currentVeh, sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos)
						
						IF bFakeGarageReverse AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
							SET_ENTITY_HEADING(currentVeh, sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z+180.0)
						ELSE
							SET_ENTITY_HEADING(currentVeh, sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z)
						ENDIF
						
						IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
							GIVE_PED_HELMET(PLAYER_PED_ID(), FALSE, PV_FLAG_NONE)
							SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
						ENDIF
						
						SET_VEHICLE_ON_GROUND_PROPERLY(currentVeh)
						
						
					ENDIF
					
//					objFakeGarageDoor = CREATE_OBJECT_NO_OFFSET(fakeGarageDoorModel, sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos)
//					SET_ENTITY_ROTATION(objFakeGarageDoor, sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
					SceneTool_ExecutePan(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh], sGarageData.camCutscene, sGarageData.camCutscene2)
					sGarageData.iWarpControl = iCONST_WARP_3_enterVeh
				ELSE
					// rob - 2114869 - clear previous lastVehicleBeforeWarp
					IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
					AND NOT IS_ENTITY_DEAD( lastVehicleBeforeWarp )
						IF IS_ENTITY_A_MISSION_ENTITY(lastVehicleBeforeWarp)
						AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(lastVehicleBeforeWarp)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(lastVehicleBeforeWarp)
						ENDIF
					ENDIF				
				
					lastVehicleBeforeWarp = GET_PLAYERS_LAST_VEHICLE()
					IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
					AND IS_VEHICLE_DRIVEABLE(lastVehicleBeforeWarp)
					AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(lastVehicleBeforeWarp))
					AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(lastVehicleBeforeWarp))
					AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(lastVehicleBeforeWarp))
						IF NOT IS_ENTITY_A_MISSION_ENTITY(lastVehicleBeforeWarp)
							SET_ENTITY_AS_MISSION_ENTITY(lastVehicleBeforeWarp, FALSE, FALSE)
						ENDIF
						
						IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
						AND IS_VEHICLE_DRIVEABLE(lastVehicleBeforeWarp)
							BOOL bWarpLastVehicleForEntry
							
							IF NOT bWarpLastVehicleForEntry
								IF IS_ENTITY_IN_ANGLED_AREA(lastVehicleBeforeWarp,
										sGarageData.sVehGenPurchData.vBuyPos1,
										sGarageData.sVehGenPurchData.vBuyPos2,
										sGarageData.sVehGenPurchData.fBuyWidth)
									PRINTLN("vehicle is in area, reposition")
									bWarpLastVehicleForEntry = TRUE
								ENDIF
							ENDIF
							
							IF NOT bWarpLastVehicleForEntry
								FLOAT fDist2
								fDist2 = VDIST2(GET_ENTITY_COORDS(lastVehicleBeforeWarp), sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vPos)
								
								IF fDist2 < (5.0*5.0)
									PRINTLN("vehicle close to camera (", SQRT(fDist2), "m), reposition")
									bWarpLastVehicleForEntry = TRUE
								ENDIF
							ENDIF
							
							IF bWarpLastVehicleForEntry
								//bug:2343256 using veh gen to make sure cars dont just disapear when going in and out of the garages 
								VEHICLE_INDEX tempVeh
								tempVeh = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH)
								IF GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(tempVehicleData,VEHGEN_MISSION_VEH)									
									SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(tempVehicleData,GET_CURRENT_PLAYER_PED_ENUM())
									//Delete if it exists
									IF DOES_ENTITY_EXIST(tempVeh)
										DELETE_VEHICLE(tempVeh)
									ENDIF
								ENDIF	
							
								IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
									CLEAR_AREA(<<-89.3770, 92.6583, 71.2349>>, 5.0, TRUE)
									SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-89.3770, 92.6583, 71.2349>>,154.4846)		
								ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
									CLEAR_AREA(<<-62.0307, -1839.8585, 25.6787>>, 5.0, TRUE)
									SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-62.0307, -1839.8585, 25.6787>>,319.6985)		
								ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
									CLEAR_AREA(<<-234.7648, -1150.3105, 21.9224>>, 5.0, TRUE)
									SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-234.7648, -1150.3105, 21.9224>>,270.8741)		
								ENDIF
								
								SET_VEHICLE_ON_GROUND_PROPERLY(lastVehicleBeforeWarp)
															
								PRINTLN("vehicle has been repositioned")
							ELSE
								PRINTLN("vehicle NOT in area, leave where is")
								
							ENDIF
						ENDIF
					ELSE
						lastVehicleBeforeWarp = NULL
					ENDIF
					
					VECTOR vBuyMidPos1, vBuyMidPos2
					vBuyMidPos1 = ((sGarageData.sVehGenPurchData.vBuyPos1*2.0)+(sGarageData.sVehGenPurchData.vBuyPos2*1.0))/3.0
					vBuyMidPos2 = ((sGarageData.sVehGenPurchData.vBuyPos1*1.0)+(sGarageData.sVehGenPurchData.vBuyPos2*2.0))/3.0
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
							sGarageData.sVehGenPurchData.vBuyPos1,
							<<vBuyMidPos1.x, vBuyMidPos1.y, sGarageData.sVehGenPurchData.vBuyPos2.z>>,
							sGarageData.sVehGenPurchData.fBuyWidth)
						sOpenGarageAnimClip = ""
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
							CASE 0	sOpenGarageAnimClip = "gar_open_1_left" BREAK
							CASE 1	sOpenGarageAnimClip = "gar_open_2_left" BREAK
						ENDSWITCH
						
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - left garage clip \"", sOpenGarageAnimClip, "\".")
					ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
							<<vBuyMidPos1.x, vBuyMidPos1.y, sGarageData.sVehGenPurchData.vBuyPos1.z>>,
							<<vBuyMidPos2.x, vBuyMidPos2.y, sGarageData.sVehGenPurchData.vBuyPos2.z>>,
							sGarageData.sVehGenPurchData.fBuyWidth)
						sOpenGarageAnimClip = ""
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
							CASE 0	sOpenGarageAnimClip = "gar_open_1_left" BREAK
							CASE 1	sOpenGarageAnimClip = "gar_open_1_right" BREAK
						ENDSWITCH
						
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - centre garage clip \"", sOpenGarageAnimClip, "\".")
					ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
							<<vBuyMidPos2.x, vBuyMidPos2.y, sGarageData.sVehGenPurchData.vBuyPos1.z>>,
							sGarageData.sVehGenPurchData.vBuyPos2,
							sGarageData.sVehGenPurchData.fBuyWidth)
						sOpenGarageAnimClip = ""
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
							CASE 0	sOpenGarageAnimClip = "gar_open_1_right" BREAK
							CASE 1	sOpenGarageAnimClip = "gar_open_2_right" BREAK
						ENDSWITCH
						
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - right garage clip \"", sOpenGarageAnimClip, "\".")
					ELSE
						sOpenGarageAnimClip = ""
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 6)
							CASE 0	sOpenGarageAnimClip = "gar_open_1_left" BREAK
							CASE 1	sOpenGarageAnimClip = "gar_open_1_right" BREAK
							CASE 2	sOpenGarageAnimClip = "gar_open_2_left" BREAK
							CASE 3	sOpenGarageAnimClip = "gar_open_2_right" BREAK
							CASE 4	sOpenGarageAnimClip = "gar_open_3_left" BREAK
							CASE 5	sOpenGarageAnimClip = "gar_open_3_right" BREAK
						ENDSWITCH
						
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - garage clip \"", sOpenGarageAnimClip, "\".")
					ENDIF
					
					CLEAR_AREA_OF_OBJECTS(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos, 20)
					CLEAR_AREA_OF_PROJECTILES(sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos, 20)
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot)
					
					iGarageEntrySynchSceneID = CREATE_SYNCHRONIZED_SCENE(
							sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos,
							<<0,0,sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot>>)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iGarageEntrySynchSceneID,
							sOpenGarageAnimDict, sOpenGarageAnimClip, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
							DEFAULT,		//SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_NONE,
							DEFAULT,		//RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_NONE,
							DEFAULT,		//FLOAT moverBlendInDelta = INSTANT_BLEND_IN,
							DEFAULT)		//IK_CONTROL_FLAGS ikFlags = AIK_NONE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
					SceneTool_ExecutePan(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed], sGarageData.camCutscene, sGarageData.camCutscene2)
					
					sGarageData.iWarpControl = iCONST_WARP_4_enterPed
				ENDIF
				
				//pin the interior into memory
				sGarageData.interiorInstanceIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vGarageM_SP_coord, "v_garagem_sp")
				IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
					IF NOT IS_INTERIOR_READY(sGarageData.interiorInstanceIndex)
						PIN_INTERIOR_IN_MEMORY(sGarageData.interiorInstanceIndex)	
						
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> interior pinned in memory request. frame = ", GET_FRAME_COUNT())
					ENDIF
					g_tlIgnoreBuildingControllerChecks = "v_garagem_sp"
				ENDIF
				
				IF NOT bLoadSceneStartedFromVehGen
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_GEN_NAME_ENUM eCurrentVehGen
						eCurrentVehGen = GET_FREE_VEHICLE_GEN_SLOT_IN_GARAGE(sGarageData.eClosestGen)
						
						VEHICLE_GEN_DATA_STRUCT sVehGenData
						GET_VEHICLE_GEN_DATA(sVehGenData, eCurrentVehGen)
						vGarageInteriorPos = sVehGenData.coords
						
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - start vehicle load scene [", vGarageInteriorPos, ", eCurrentVehGen: ", eCurrentVehGen, "]")
						NEW_LOAD_SCENE_START_SPHERE(vGarageInteriorPos, 20.0)
					ELSE
						PRINTLN("PROCESS_GARAGE_VEHICLE_WARP - start ped load scene [", vGarageOnfootWarp, "]")
						NEW_LOAD_SCENE_START_SPHERE(vGarageOnfootWarp, 20.0)
					ENDIF
				ENDIF
				SET_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_WORLD_RANGE_DIST_CHECKS))
				
				DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<198.0043, -999.7775, -100.0000>>, 50.0)
				
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(sGarageData.eClosestGen)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MICHAEL_GARAGE_1)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MICHAEL_GARAGE_2)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MICHAEL_GARAGE_3)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_GARAGE_1)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_GARAGE_2)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_GARAGE_3)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_FRANKLIN_GARAGE_1)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_FRANKLIN_GARAGE_2)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_FRANKLIN_GARAGE_3)
				
				SETTIMERA(0)
				fakeGarageCarEnterCutsceneStage = 0
			BREAK
			
			CASE iCONST_WARP_4_enterPed
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration*1000)
				IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					str += " (veh)"
				ELSE
					str += " (ped)"
				ENDIF
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				
				DRAW_DEBUG_LINE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos+<<0,0,1>>, sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos+<<0,0,1>>,
						iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos+<<0,0,1>>, 0.1,
						iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos+<<0,0,1>>, 0.1,
						iRed, iGreen, iBlue, iAlpha)
				#ENDIF
				
				BOOL bProgessPastStage4
				bProgessPastStage4 = TRUE
				
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE())
					#IF IS_DEBUG_BUILD
					str  = "load scene not active"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
					#ENDIF
					
				ELIF (IS_NEW_LOAD_SCENE_LOADED())
					#IF IS_DEBUG_BUILD
					str  = "load scene loaded"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "wait for load scene"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
					#ENDIF
					
					bProgessPastStage4 = FALSE
				ENDIF
				
				IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
					IF NOT IS_INTERIOR_READY(sGarageData.interiorInstanceIndex)
						#IF IS_DEBUG_BUILD
						str  = "interior NOT READY"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
						#ENDIF
						
						PIN_INTERIOR_IN_MEMORY(sGarageData.interiorInstanceIndex)
					ELSE
						#IF IS_DEBUG_BUILD
						str  = "interior ready"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
					ENDIF
					g_tlIgnoreBuildingControllerChecks = "v_garagem_sp"
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "interior NOT VALID"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
					#ENDIF
					
					sGarageData.interiorInstanceIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vGarageM_SP_coord, "v_garagem_sp")
				ENDIF
				IF HAVE_ALL_VEHICLE_GENS_LOADED_NEAR_PLAYER()
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "two - wait for vehgens"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
					bProgessPastStage4 = FALSE
				ENDIF
				IF (DOES_CAM_EXIST(sGarageData.camCutscene) AND IS_CAM_RENDERING(sGarageData.camCutscene))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - camCutscene interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage4 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - timera: "
						str += TIMERA()
						str += " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage4 = FALSE
					ENDIF
				ELIF (DOES_CAM_EXIST(sGarageData.camCutscene2) AND IS_CAM_RENDERING(sGarageData.camCutscene2))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene2)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - camCutscene2 interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						#ENDIF
						
						bProgessPastStage4 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - timera: "
						str += TIMERA()
						str +=  " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						#ENDIF
						
						bProgessPastStage4 = FALSE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "four-five"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					
					#ENDIF
					
					bProgessPastStage4 = FALSE
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iGarageEntrySynchSceneID)
					FLOAT fSynchScenePhase
					fSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iGarageEntrySynchSceneID)
					
					IF fSynchScenePhase < fGARAGE_OPEN_PHASE
						#IF IS_DEBUG_BUILD
						str  = "synch scene["
						str += iGarageEntrySynchSceneID
						str += "] running "
						str += GET_STRING_FROM_FLOAT(fSynchScenePhase)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						str  = "sOpenGarageAnimClip \""
						str += sOpenGarageAnimClip
						str += "\" "
						str += GET_STRING_FROM_FLOAT(GET_ANIM_DURATION(sOpenGarageAnimDict, sOpenGarageAnimClip))
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage4 = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						str  = "synch scene["
						str += iGarageEntrySynchSceneID
						str += "] ending "
						str += GET_STRING_FROM_FLOAT(fSynchScenePhase)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "synch scene["
					str += iGarageEntrySynchSceneID
					str += "] NOT running"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					
					#ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF sGarageVehicleWarpData.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterPed]
					SET_TEXT_COLOUR(100, 255, 255, 255)
					SET_TEXT_SCALE(0.7, 0.8)
					SET_TEXT_WRAP(0.0, 1.0)
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", "PLACEHOLDER CAMERA")
				ENDIF
				#ENDIF
				
				IF bProgessPastStage4
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					sGarageData.iWarpControl = iCONST_WARP_5_fadingOutPed
				ENDIF
			BREAK
			CASE iCONST_WARP_5_fadingOutPed
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					INT iSoundID
					iSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iSoundID,"GARAGE_DOOR_SCRIPTED_CLOSE")
					SET_VARIABLE_ON_SOUND(iSoundID, "hold", iGarageHold/1000.0)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					IF DOES_CAM_EXIST(sGarageData.camCutscene)
						SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
						DESTROY_CAM(sGarageData.camCutscene)
					ENDIF
					IF DOES_CAM_EXIST(sGarageData.camCutscene2)
						SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
						DESTROY_CAM(sGarageData.camCutscene2)
					ENDIF
					IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
						SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
						FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
						vehFakeGarageEnter = NULL
					ENDIF
//					IF DOES_ENTITY_EXIST(objFakeGarageDoor)
//						DELETE_OBJECT(objFakeGarageDoor)
//					ENDIF
					
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iGarageEntrySynchSceneID)
						DETACH_SYNCHRONIZED_SCENE(iGarageEntrySynchSceneID)
					ENDIF
					iGarageEntrySynchSceneID = -1
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<194.5394215,-1026.319946, -100.0000>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 334.1665)
					
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SETTIMERA(0)
					sGarageData.iWarpControl = iCONST_WARP_6_fadingInPed
				ENDIF
			BREAK
			CASE iCONST_WARP_6_fadingInPed
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				//Request the vehicle models early
				EARLY_REQUEST_GARAGE_VEHICLES(sGarageData.eClosestGen)
				
				IF IS_SCREEN_FADED_OUT()
				AND TIMERA() > iGarageHold
					REMOVE_ANIM_DICT(sOpenGarageAnimDict)
					RESET_VEHICLE_GEN_LOADED_CHECKS()
					
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					
					SETTIMERA(0)
					iGarageHelpStage = 0
					bLoadSceneStartedFromVehGen = FALSE
					
					sGarageData.iWarpControl = iCONST_WARP_10
				ENDIF
			BREAK
			
			CASE iCONST_WARP_3_enterVeh
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration*1000)
				str += " (veh)"
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				//Request the vehicle models early
				EARLY_REQUEST_GARAGE_VEHICLES(sGarageData.eClosestGen)
				
				BOOL bProgessPastStage3
				bProgessPastStage3 = TRUE
				
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE())
					#IF IS_DEBUG_BUILD
					str  = "load scene not active"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
					#ENDIF
					
				ELIF (IS_NEW_LOAD_SCENE_LOADED())
					#IF IS_DEBUG_BUILD
					str  = "load scene loaded"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "wait for load scene"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
					
					#ENDIF
					
					bProgessPastStage3 = FALSE
				ENDIF
				IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
					IF NOT IS_INTERIOR_READY(sGarageData.interiorInstanceIndex)
						#IF IS_DEBUG_BUILD
						str  = "interior NOT READY"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
						#ENDIF
						
						PIN_INTERIOR_IN_MEMORY(sGarageData.interiorInstanceIndex)
					ELSE
						#IF IS_DEBUG_BUILD
						str  = "interior ready"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
					ENDIF
					g_tlIgnoreBuildingControllerChecks = "v_garagem_sp"
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "interior NOT VALID"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
					#ENDIF
					
					sGarageData.interiorInstanceIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(vGarageM_SP_coord, "v_garagem_sp")
				ENDIF
				IF HAVE_ALL_VEHICLE_GENS_LOADED_NEAR_PLAYER()
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "two - wait for vehgens"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
					bProgessPastStage3 = FALSE
				ENDIF
				IF (DOES_CAM_EXIST(sGarageData.camCutscene) AND IS_CAM_RENDERING(sGarageData.camCutscene))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - camCutscene interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage3 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - timera: "
						str += TIMERA()
						str += " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage3 = FALSE
					ENDIF
				ELIF (DOES_CAM_EXIST(sGarageData.camCutscene2) AND IS_CAM_RENDERING(sGarageData.camCutscene2))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene2)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - camCutscene2 interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage3 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - timera: "
						str += TIMERA()
						str +=  " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						#ENDIF
						
						bProgessPastStage3 = FALSE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "four-five"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
					bProgessPastStage3 = FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF sGarageVehicleWarpData.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterVeh]
					SET_TEXT_COLOUR(100, 255, 255, 255)
					SET_TEXT_SCALE(0.7, 0.8)
					SET_TEXT_WRAP(0.0, 1.0)
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", "PLACEHOLDER CAMERA")
				ENDIF
				#ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					structSceneTool_Placer mPlacer
					mPlacer.vPos = <<0,0,0>>
					mPlacer.fRot = 0.0
					DO_FAKE_GARAGE_CAR_DRIVE(PLAYER_PED_ID(),
							sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh],
							sGarageVehicleWarpData.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh],
							(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration*1000.0) - 0500,
							mPlacer, GRAPH_TYPE_ACCEL,
							fStrOffset)
				ENDIF
				
				IF bProgessPastStage3
					RESET_VEHICLE_GEN_LOADED_CHECKS()
					
//					//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//					RENDER_SCRIPT_CAMS(FALSE,FALSE)
//					IF DOES_CAM_EXIST(sGarageData.camCutscene)
//						SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
//						DESTROY_CAM(sGarageData.camCutscene)
//					ENDIF
//					IF DOES_CAM_EXIST(sGarageData.camCutscene2)
//						SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
//						DESTROY_CAM(sGarageData.camCutscene2)
//					ENDIF
//					IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
//						SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
//						FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
//						vehFakeGarageEnter = NULL
//					ENDIF
////					IF DOES_ENTITY_EXIST(objFakeGarageDoor)
////						DELETE_OBJECT(objFakeGarageDoor)
////					ENDIF
//					
//					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
//					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					
//					FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
//					
//					IF IS_NEW_LOAD_SCENE_ACTIVE()
//						NEW_LOAD_SCENE_STOP()
//					ENDIF
//					
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//						IF DOES_ENTITY_EXIST(currentVeh)
//						AND IS_VEHICLE_DRIVEABLE(currentVeh)
//						
//							VEHICLE_GEN_NAME_ENUM eCurrentVehGen
//							eCurrentVehGen = GET_FREE_VEHICLE_GEN_SLOT_IN_GARAGE(sGarageData.eClosestGen)
//							
//							VEHICLE_GEN_DATA_STRUCT sVehGenData
//							GET_VEHICLE_GEN_DATA(sVehGenData, eCurrentVehGen)
//							SET_ENTITY_COORDS(currentVeh, sVehGenData.coords)
//							SET_ENTITY_HEADING(currentVeh, sVehGenData.heading)
//							
//							SET_VEHICLE_DOORS_SHUT(currentVeh)
//							SET_VEHICLE_ENGINE_ON(currentVeh, FALSE, TRUE)
//							SET_VEHICLE_LIGHTS(currentVeh, SET_VEHICLE_LIGHTS_OFF)
//							SET_VEHICLE_INDICATOR_LIGHTS(currentVeh, TRUE, FALSE)
//							SET_VEHICLE_INDICATOR_LIGHTS(currentVeh, FALSE, FALSE)
//							SET_VEHICLE_RADIO_ENABLED(currentVeh, FALSE)
//							SET_ENTITY_PROOFS(currentVeh, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
//							
//							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), currentVeh)
//							
//							FREEZE_ENTITY_POSITION(currentVeh, FALSE)
//							
//							VEHICLE_SETUP_STRUCT sVehgenSetup
//							GET_VEHICLE_SETUP(currentVeh, sVehgenSetup)
//							UPDATE_DYNAMIC_VEHICLE_GEN_DATA(eCurrentVehGen, sVehgenSetup, <<0,0,0>>, -1)
//							
//							SET_VEHICLE_GEN_VEHICLE(eCurrentVehGen, currentVeh)
//							
//							STOP_TRACKING_VEHICLE_FOR_IMPOUND(currentVeh)
//						ENDIF
//						
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//					ELSE
//						//
//					ENDIF
//					
//					// Reposition buddies
//					INT iNearbyPedCount
//					INT iNearbyPed
//					iNearbyPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
//					REPEAT iNearbyPedCount iNearbyPed
//						IF DOES_ENTITY_EXIST(nearbyPeds[iNearbyPed])
//						AND NOT IS_PED_INJURED(nearbyPeds[iNearbyPed])
//						AND IS_PED_GROUP_MEMBER(nearbyPeds[iNearbyPed], GET_PLAYER_GROUP(PLAYER_ID()))
//							CPRINTLN(DEBUG_AMBIENT, "PROCESS_GARAGE_VEHICLE_WARP -> reposition player group member [", iNearbyPed, ":", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyPeds[iNearbyPed])), "] to inside garage")
//							
//							SET_ENTITY_COORDS_NO_OFFSET(nearbyPeds[iNearbyPed], <<206.8020, -1018.0115, -100.0000>>)
//						ENDIF
//					ENDREPEAT
					
					SETTIMERA(0)
//					iGarageHelpStage = 0
//					bLoadSceneStartedFromVehGen = FALSE
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					sGarageData.iWarpControl = iCONST_WARP_3_5
				ENDIF
			BREAK
			CASE iCONST_WARP_3_5
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh]*1000)
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				BOOL bProgessPastStage35
				bProgessPastStage35 = TRUE

				//Request the vehicle models early
				EARLY_REQUEST_GARAGE_VEHICLES(sGarageData.eClosestGen)
				
				IF (TIMERA() <= ((sGarageVehicleWarpData.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh]*1000)))
					#IF IS_DEBUG_BUILD
					str  = "fHold: "
					str += TIMERA()
					str +=  " < "
					str += ROUND(sGarageVehicleWarpData.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh]*1000)
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					bProgessPastStage35 = FALSE
				ENDIF
				
				IF bProgessPastStage35
					RESET_VEHICLE_GEN_LOADED_CHECKS()
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					IF DOES_CAM_EXIST(sGarageData.camCutscene)
						SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
						DESTROY_CAM(sGarageData.camCutscene)
					ENDIF
					IF DOES_CAM_EXIST(sGarageData.camCutscene2)
						SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
						DESTROY_CAM(sGarageData.camCutscene2)
					ENDIF
					IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
						SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
						FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
						vehFakeGarageEnter = NULL
					ENDIF
//					IF DOES_ENTITY_EXIST(objFakeGarageDoor)
//						DELETE_OBJECT(objFakeGarageDoor)
//					ENDIF
					
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
					
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(currentVeh)
						AND IS_VEHICLE_DRIVEABLE(currentVeh)
						
							VEHICLE_GEN_NAME_ENUM eCurrentVehGen
							eCurrentVehGen = GET_FREE_VEHICLE_GEN_SLOT_IN_GARAGE(sGarageData.eClosestGen)
							
							VEHICLE_GEN_DATA_STRUCT sVehGenData
							GET_VEHICLE_GEN_DATA(sVehGenData, eCurrentVehGen)
							SET_ENTITY_COORDS(currentVeh, sVehGenData.coords)
							SET_ENTITY_HEADING(currentVeh, sVehGenData.heading)
							
							SET_VEHICLE_DOORS_SHUT(currentVeh)
							SET_VEHICLE_ENGINE_ON(currentVeh, FALSE, TRUE)
							SET_VEHICLE_LIGHTS(currentVeh, SET_VEHICLE_LIGHTS_OFF)
							SET_VEHICLE_INDICATOR_LIGHTS(currentVeh, TRUE, FALSE)
							SET_VEHICLE_INDICATOR_LIGHTS(currentVeh, FALSE, FALSE)
							SET_VEHICLE_RADIO_ENABLED(currentVeh, FALSE)
							SET_ENTITY_PROOFS(currentVeh, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
							
							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), currentVeh)
							
							FREEZE_ENTITY_POSITION(currentVeh, FALSE)
							
							VEHICLE_SETUP_STRUCT sVehgenSetup
							GET_VEHICLE_SETUP(currentVeh, sVehgenSetup)
							UPDATE_DYNAMIC_VEHICLE_GEN_DATA(eCurrentVehGen, sVehgenSetup, <<0,0,0>>, -1)
							
							SET_VEHICLE_GEN_VEHICLE(eCurrentVehGen, currentVeh)
							
							STOP_TRACKING_VEHICLE_FOR_IMPOUND(currentVeh)
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ELSE
						//
					ENDIF
					
					// Reposition buddies
					INT iNearbyPedCount
					INT iNearbyPed
					iNearbyPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
					REPEAT iNearbyPedCount iNearbyPed
						IF DOES_ENTITY_EXIST(nearbyPeds[iNearbyPed])
						AND NOT IS_PED_INJURED(nearbyPeds[iNearbyPed])
						AND IS_PED_GROUP_MEMBER(nearbyPeds[iNearbyPed], GET_PLAYER_GROUP(PLAYER_ID()))
							CPRINTLN(DEBUG_AMBIENT, "PROCESS_GARAGE_VEHICLE_WARP -> reposition player group member [", iNearbyPed, ":", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyPeds[iNearbyPed])), "] to inside garage")
							
							SET_ENTITY_COORDS_NO_OFFSET(nearbyPeds[iNearbyPed], <<206.8020, -1018.0115, -100.0000>>)
						ENDIF
					ENDREPEAT
					
					SETTIMERA(0)
					iGarageHelpStage = 0
					bLoadSceneStartedFromVehGen = FALSE
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					sGarageData.iWarpControl = iCONST_WARP_10
				ENDIF
			BREAK
			
			CASE iCONST_WARP_10
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " iGarageHelpStage: "
				str += iGarageHelpStage
				str += " \""
				str += tlWarpHelpPrinted
				str += "\""
				#ENDIF
				
				IF TIMERA() < 7000
				OR NOT g_savedGlobals.sVehicleGenData.bGarageIntroRun
					
					#IF IS_DEBUG_BUILD
					str  = "bGarageIntroRun = FALSE, iGarageHelpStage: "
					str += iGarageHelpStage
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF

					
					IF NOT g_savedGlobals.sVehicleGenData.bGarageIntroRun
						IF (iGarageHelpStage = 0)
							PRINT_HELP("CAR_GAR_05")
							tlWarpHelpPrinted = "CAR_GAR_05"
							bPrintWarpHelp = TRUE
							IF TIMERA() >= 7000
								SETTIMERA(0)
								iGarageHelpStage++
							ENDIF
						ELIF (iGarageHelpStage = 1)
							PRINT_HELP("CAR_GAR_06")
							tlWarpHelpPrinted = "CAR_GAR_06"
							bPrintWarpHelp = TRUE
							IF TIMERA() >= 7000
								SETTIMERA(0)
								g_savedGlobals.sVehicleGenData.bGarageIntroRun = TRUE
							ENDIF
						ENDIF
					ELSE
						PRINT_HELP("CAR_GAR_EXIT")
						tlWarpHelpPrinted = "CAR_GAR_EXIT"
						bPrintWarpHelp = TRUE
					ENDIF
				ELSE
					g_savedGlobals.sVehicleGenData.bGarageIntroRun = TRUE
				ENDIF
				
				BOOL bProgessPastStage10
				bProgessPastStage10 = FALSE
				
				IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
						RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
					ENDIF
					
					currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(currentVeh)
					AND IS_VEHICLE_DRIVEABLE(currentVeh)
						IF GET_IS_VEHICLE_ENGINE_RUNNING(currentVeh)
						OR IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
							IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
								#IF IS_DEBUG_BUILD
								str  = "Wait for ACCELERATE or BRAKE"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								#ENDIF
								
								IF (GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
								OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE))
									bProgessPastStage10 = TRUE
								ENDIF
								IF (GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
								OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE))
									bProgessPastStage10 = TRUE
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								str  = "entering/exiting the vehicle"
								DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					REQUEST_ANIM_DICT(sOpenGarageAnimDict)
					
					#IF IS_DEBUG_BUILD
					IF NOT DOES_ANIM_DICT_EXIST(sOpenGarageAnimDict)
						str  = "anim dict \""
						str += (sOpenGarageAnimDict)
						str += "\" doesn't exist"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_BLACK)
					ELIF HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
						str  = "loaded anim dict \""
						str += (sOpenGarageAnimDict)
						str += "\""
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					ELSE
						str  = "waiting for anim dict \""
						str += (sOpenGarageAnimDict)
						str += "\""
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					ENDIF
					
					IF (iGarageEntryContextID = NEW_CONTEXT_INTENTION)
						str  = "iGarageEntryContextID = NEW_CONTEXT_INTENTION"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					ELSE
						INT iContext
						iContext = GET_CONTEXT_INTENTION_ARRAY_INDEX(iGarageEntryContextID)
						str  = "iGarageEntryContextID = \""
						IF (iContext >= 0)
						AND (iContext < MAX_CONTEXT_INTENTION)
							str += g_IntentionList[iContext].helpLabel
						ELSE
							str += "unknown_"
							str += iContext
						ENDIF
						str += "\""
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					ENDIF
					IF (iVehicleMenuContextID = NEW_CONTEXT_INTENTION)
						str  = "iVehicleMenuContextID = NEW_CONTEXT_INTENTION"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					ELSE
						INT iContext
						iContext = GET_CONTEXT_INTENTION_ARRAY_INDEX(iVehicleMenuContextID)
						str  = "iVehicleMenuContextID = \""
						IF (iContext >= 0)
						AND (iContext < MAX_CONTEXT_INTENTION)
							str += g_IntentionList[iContext].helpLabel
						ELSE
							str += "unknown_"
							str += iContext
						ENDIF
						str += "\""
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					ENDIF
					
					#ENDIF
					
					IF ((IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<191.049149,-1026.318237,-104.999962>>, <<198.029694,-1026.321655,-96.812462>>, 2.062500)
					AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), 180.0, 90.0)))
						IF iVehicleMenuContextID != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(iVehicleMenuContextID)
						ENDIF
						IF (iGarageEntryContextID = NEW_CONTEXT_INTENTION)
							REGISTER_CONTEXT_INTENTION(iGarageEntryContextID, CP_MEDIUM_PRIORITY, "WEB_VEH_EXIT")	// Press ~a~ to exit garage.
						ENDIF
						
						IF NOT (iVehicleMenuContextID != NEW_CONTEXT_INTENTION)
						AND NOT (iGarageEntryContextID = NEW_CONTEXT_INTENTION)
							IF HAS_CONTEXT_BUTTON_TRIGGERED(iGarageEntryContextID)
								RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
								bProgessPastStage10 = TRUE
							ENDIF
						ENDIF
					ELSE
						IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
							RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
						ENDIF
					ENDIF
				ENDIF
				
				IF bExitGarageInDLCVehicle
					bProgessPastStage10 = TRUE
				ENDIF
				
				IF bProgessPastStage10
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					OR bExitGarageInDLCVehicle
						SETTIMERA(0)
						NEW_LOAD_SCENE_START_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos, 20.0)
						
						IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
							RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
						ENDIF
						
						sGarageData.iWarpControl = iCONST_WARP_12_exitVehInt
					ELSE
						IF HAS_ANIM_DICT_LOADED(sOpenGarageAnimDict)
							sOpenGarageAnimClip = ""
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 6)
								CASE 0	sOpenGarageAnimClip = "gar_open_1_left" BREAK
								CASE 1	sOpenGarageAnimClip = "gar_open_1_right" BREAK
								CASE 2	sOpenGarageAnimClip = "gar_open_2_left" BREAK
								CASE 3	sOpenGarageAnimClip = "gar_open_2_right" BREAK
								CASE 4	sOpenGarageAnimClip = "gar_open_3_left" BREAK
								CASE 5	sOpenGarageAnimClip = "gar_open_3_right" BREAK
							ENDSWITCH
							
							iGarageEntrySynchSceneID = CREATE_SYNCHRONIZED_SCENE(
									sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos,
									<<0,0,sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].fRot>>)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iGarageEntrySynchSceneID,
									sOpenGarageAnimDict, sOpenGarageAnimClip, NORMAL_BLEND_IN, NORMAL_BLEND_OUT,
									DEFAULT,		//SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_NONE,
									DEFAULT,		//RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_NONE,
									DEFAULT,		//FLOAT moverBlendInDelta = INSTANT_BLEND_IN,
									DEFAULT)		//IK_CONTROL_FLAGS ikFlags = AIK_NONE)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
							SceneTool_ExecutePan(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen], sGarageData.camCutscene, sGarageData.camCutscene2)
							
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							NEW_LOAD_SCENE_START_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos, 20.0)
							SETTIMERA(0)
							
							IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
								RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
							ENDIF
							sGarageData.iWarpControl = iCONST_WARP_11_exitPed
						ELIF NOT DOES_ANIM_DICT_EXIST(sOpenGarageAnimDict)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].fRot)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
							SceneTool_ExecutePan(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen], sGarageData.camCutscene, sGarageData.camCutscene2)
							
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							NEW_LOAD_SCENE_START_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos, 20.0)
							SETTIMERA(0)
							
							IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
								RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
							ENDIF
							sGarageData.iWarpControl = iCONST_WARP_11_exitPed
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_WARP_11_exitPed
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				
				DRAW_DEBUG_LINE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos+<<0,0,1>>, sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos+<<0,0,1>>,
						iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos+<<0,0,1>>, 0.1,
						iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_SPHERE(sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos+<<0,0,1>>, 0.1,
						iRed, iGreen, iBlue, iAlpha)
				#ENDIF
				
				BOOL bProgessPastStage11
				bProgessPastStage11 = TRUE
				
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE())
					#IF IS_DEBUG_BUILD
					str  = "load scene not active"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
					#ENDIF
					
				ELIF (IS_NEW_LOAD_SCENE_LOADED())
					#IF IS_DEBUG_BUILD
					str  = "load scene loaded"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "wait for load scene"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
					#ENDIF
					
					bProgessPastStage11 = FALSE
				ENDIF
				IF HAVE_ALL_VEHICLE_GENS_LOADED_NEAR_PLAYER()
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "two - wait for vehgens"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
					bProgessPastStage11 = FALSE
				ENDIF
//				IF (TIMERA() > ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)+1000))
//					
//				ELSE
//					str  = "emergency timer! - timera: "
//					str += TIMERA()
//					str += " < "
//					str += ROUND((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)+1000)
//					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
//					
//					bProgessPastStage11 = FALSE
//				ENDIF
				IF (DOES_CAM_EXIST(sGarageData.camCutscene) AND IS_CAM_RENDERING(sGarageData.camCutscene))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - camCutscene interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						#ENDIF
						
						bProgessPastStage11 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - timera: "
						str += TIMERA()
						str += " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage11 = FALSE
					ENDIF
				ELIF (DOES_CAM_EXIST(sGarageData.camCutscene2) AND IS_CAM_RENDERING(sGarageData.camCutscene2))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene2)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - camCutscene2 interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage11 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - timera: "
						str += TIMERA()
						str +=  " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage11 = FALSE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "four-five"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
					bProgessPastStage11 = FALSE
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iGarageEntrySynchSceneID)
					FLOAT fSynchScenePhase
					fSynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iGarageEntrySynchSceneID)
					
					IF fSynchScenePhase < fGARAGE_OPEN_PHASE
						#IF IS_DEBUG_BUILD
						str  = "synch scene["
						str += iGarageEntrySynchSceneID
						str += "] running "
						str += GET_STRING_FROM_FLOAT(fSynchScenePhase)
						str += " < "
						str += GET_STRING_FROM_FLOAT(fGARAGE_OPEN_PHASE)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						
						str  = "sOpenGarageAnimClip \""
						str += sOpenGarageAnimClip
						str += "\" "
						str += GET_STRING_FROM_FLOAT(GET_ANIM_DURATION(sOpenGarageAnimDict, sOpenGarageAnimClip))
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
						bProgessPastStage11 = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						str  = "synch scene["
						str += iGarageEntrySynchSceneID
						str += "] ending "
						str += GET_STRING_FROM_FLOAT(fSynchScenePhase)
						str += " >= "
						str += GET_STRING_FROM_FLOAT(fGARAGE_OPEN_PHASE)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "synch scene["
					str += iGarageEntrySynchSceneID
					str += "] NOT running"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
				ENDIF
				
				
				#IF IS_DEBUG_BUILD
				IF sGarageVehicleWarpData.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen]
					SET_TEXT_COLOUR(100, 255, 255, 255)
					SET_TEXT_SCALE(0.7, 0.8)
					SET_TEXT_WRAP(0.0, 1.0)
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", "PLACEHOLDER CAMERA")
				ENDIF
				#ENDIF
				
				IF bProgessPastStage11
					
					iGarageHelpStage = 0
					bLoadSceneStartedFromVehGen = FALSE
					
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					SETTIMERA(0)
					
					sGarageData.iWarpControl = iCONST_WARP_13_exitFadingOut
				ENDIF
			BREAK
			CASE iCONST_WARP_12_exitVehInt
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				IF (TIMERA() > 0500)
					VEHICLE_GEN_NAME_ENUM eCurrentVehGen
					eCurrentVehGen = GET_GARAGE_VEHICLE_GEN_PLAYER_IS_IN()
					IF eCurrentVehGen = VEHGEN_WEB_CAR_MICHAEL
					OR eCurrentVehGen = VEHGEN_WEB_CAR_FRANKLIN
					OR eCurrentVehGen = VEHGEN_WEB_CAR_TREVOR
						VEHICLE_SETUP_STRUCT sDummyStruct
						UPDATE_DYNAMIC_VEHICLE_GEN_DATA(eCurrentVehGen, sDummyStruct, <<0,0,0>>, -1)
						CLEANUP_VEHICLE_GEN_VEHICLE(eCurrentVehGen)
					
					ELIF eCurrentVehGen != VEHGEN_NONE
						IF NOT bExitGarageInDLCVehicle
							SET_VEHICLE_GEN_AVAILABLE(eCurrentVehGen, FALSE)
							CLEANUP_VEHICLE_GEN_VEHICLE(eCurrentVehGen)
						ENDIF
					ENDIF
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					REMOVE_ANIM_DICT(sOpenGarageAnimDict)
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					
					sGarageData.iWarpControl = iCONST_WARP_13_exitFadingOut
				ENDIF
			BREAK
			CASE iCONST_WARP_13_exitFadingOut
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					INT iSoundID
					iSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(iSoundID,"GARAGE_DOOR_SCRIPTED_CLOSE")
					SET_VARIABLE_ON_SOUND(iSoundID, "hold", iGarageHold/1000.0)
					
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					IF DOES_CAM_EXIST(sGarageData.camCutscene)
						SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
						DESTROY_CAM(sGarageData.camCutscene)
					ENDIF
					IF DOES_CAM_EXIST(sGarageData.camCutscene2)
						SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
						DESTROY_CAM(sGarageData.camCutscene2)
					ENDIF
					IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
						SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
						FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
						vehFakeGarageEnter = NULL
					ENDIF
//					IF DOES_ENTITY_EXIST(objFakeGarageDoor)
//						DELETE_OBJECT(objFakeGarageDoor)
//					ENDIF
					
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT bExitGarageInDLCVehicle
							currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(currentVeh)
							AND IS_VEHICLE_DRIVEABLE(currentVeh)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								SET_ENTITY_COORDS(currentVeh, sGarageData.sVehGenPurchData.vGarageExitCoords[1])
								SET_ENTITY_HEADING(currentVeh, sGarageData.sVehGenPurchData.fGarageExitHeading[1])
								SET_VEHICLE_ON_GROUND_PROPERLY(currentVeh)
							ENDIF
						ENDIF
					ELSE
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iGarageEntrySynchSceneID)
							DETACH_SYNCHRONIZED_SCENE(iGarageEntrySynchSceneID)
						ENDIF
						iGarageEntrySynchSceneID = -1
						
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vGarageExitCoords[1])
						SET_ENTITY_HEADING(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.fGarageExitHeading[1])
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SETTIMERA(0)
					sGarageData.iWarpControl = iCONST_WARP_14_exitFadein
				ENDIF
			BREAK
			CASE iCONST_WARP_14_exitFadein
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " > "
				str += iGarageHold
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				IF (IS_SCREEN_FADED_OUT() AND TIMERA() > iGarageHold)
					BOOL bProgessPastStage12
					bProgessPastStage12 = TRUE
					
					IF bExitGarageInDLCVehicle
						REQUEST_MODEL(eDLCModel)
						IF HAS_MODEL_LOADED(eDLCModel)
							VEHICLE_INDEX tempVeh
							tempVeh = CREATE_VEHICLE(eDLCModel, sGarageData.sVehGenPurchData.vGarageExitCoords[0], sGarageData.sVehGenPurchData.fGarageExitHeading[0])
							IF eDLCModel = WINDSOR
								//B* 2326886: Create default blank livery
								SET_VEHICLE_LIVERY(tempveh,0)
							ENDIF
							IF DOES_ENTITY_EXIST(tempVeh)
							AND IS_VEHICLE_DRIVEABLE(tempVeh)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), tempVeh)
								
								SET_VEHICLE_DIRT_LEVEL(tempVeh, 0.0)
								
								SET_MODEL_AS_NO_LONGER_NEEDED(eDLCModel)
								
								// Custom setups.
								IF HAS_SPAWN_VEHICLE_GOT_CUSTOM_SETUP(eDLCModel)
									PRINTLN("HAS_SPAWN_VEHICLE_GOT_CUSTOM_SETUP = TRUE")
									SWITCH eDLCModel
										CASE MARSHALL
											PRINTLN("...setting livery on MARSHALL, iCustomVehicleSelection = ", iCustomVehicleSelection)
											SET_VEHICLE_LIVERY(tempVeh, iCustomVehicleSelection)
										BREAK
									ENDSWITCH
								ENDIF
								
								SET_VEHICLE_AS_NO_LONGER_NEEDED(tempVeh)
							ENDIF
							bExitGarageInDLCVehicle = FALSE
						ELSE
							#IF IS_DEBUG_BUILD
							str  = "model \""
							str += GET_MODEL_NAME_FOR_DEBUG(eDLCModel)
							str += "\" not loaded"
							DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
							#ENDIF
							
							bProgessPastStage12 = FALSE
						ENDIF
					ENDIF
					
					IF (NOT IS_NEW_LOAD_SCENE_ACTIVE())
						#IF IS_DEBUG_BUILD
						str  = "load scene not active"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
						#ENDIF
						
					ELIF (IS_NEW_LOAD_SCENE_LOADED())
						#IF IS_DEBUG_BUILD
						str  = "load scene loaded"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						
					ELSE
						#IF IS_DEBUG_BUILD
						str  = "wait for load scene"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
						#ENDIF
						
						bProgessPastStage12 = FALSE
					ENDIF
					
					IF bProgessPastStage12
						IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
						AND IS_VEHICLE_DRIVEABLE(lastVehicleBeforeWarp)
							//bug:2343256 using veh gen to make sure cars dont just disapear when going in and out of the garages 
							VEHICLE_INDEX tempVeh
							tempVeh = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH)
							IF GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(tempVehicleData,VEHGEN_MISSION_VEH)
								SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(tempVehicleData,GET_CURRENT_PLAYER_PED_ENUM())
								//Delete if it exists
								IF DOES_ENTITY_EXIST(tempVeh)
									DELETE_VEHICLE(tempVeh)
								ENDIF
							ENDIF	
						
							IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
								CLEAR_AREA(<<-89.3770, 92.6583, 71.2349>>, 5.0, TRUE)
								SET_ENTITY_COORDS(lastVehicleBeforeWarp, <<-89.3770, 92.6583, 71.2349>>)
								SET_ENTITY_HEADING(lastVehicleBeforeWarp, 154.4846)
								SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-89.3770, 92.6583, 71.2349>>,154.4846)		
							ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
								CLEAR_AREA(<<-62.0307, -1839.8585, 25.6787>>, 5.0, TRUE)
								SET_ENTITY_COORDS(lastVehicleBeforeWarp, <<-62.0307, -1839.8585, 25.6787>>)
								SET_ENTITY_HEADING(lastVehicleBeforeWarp, 319.6985)
								SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-62.0307, -1839.8585, 25.6787>>,319.6985)		
							ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
								CLEAR_AREA(<<-234.7648, -1150.3105, 21.9224>>, 5.0, TRUE)
								SET_ENTITY_COORDS(lastVehicleBeforeWarp, <<-234.7648, -1150.3105, 21.9224>>)
								SET_ENTITY_HEADING(lastVehicleBeforeWarp, 270.8741)
								SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-234.7648, -1150.3105, 21.9224>>,270.8741)		
							ENDIF
							SET_VEHICLE_ON_GROUND_PROPERLY(lastVehicleBeforeWarp)					
							PRINTLN("Previous vehicle has been repositioned near the garage, added to the MISSION_VEHICLE_GEN")
						ENDIF
					
						IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(currentVeh)
							AND IS_VEHICLE_DRIVEABLE(currentVeh)
								SET_ENTITY_COORDS(currentVeh, sGarageVehicleWarpData.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh].vPos)
								SET_ENTITY_HEADING(currentVeh, sGarageData.sVehGenPurchData.fGarageExitHeading[0])
								SET_VEHICLE_ON_GROUND_PROPERLY(currentVeh)
								
								IF eDLCModel = MONSTER
								OR eDLCModel = MARSHALL
								OR eDLCModel = RHINO
									SET_VEHICLE_CAN_SAVE_IN_GARAGE(currentVeh, FALSE)
								ELSE
									SET_VEHICLE_CAN_SAVE_IN_GARAGE(currentVeh, TRUE)
								ENDIF
								
								IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
									GIVE_PED_HELMET(PLAYER_PED_ID(), FALSE, PV_FLAG_NONE)
									SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
								ENDIF
								
								SET_VEHICLE_RADIO_ENABLED(currentVeh, TRUE)
								
								UPDATE_PLAYER_PED_SAVED_VEHICLE(GET_CURRENT_PLAYER_PED_ENUM(), currentVeh, SAVED_VEHICLE_SLOT_MODDED, TRUE)
							ENDIF
							
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//							TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), currentVeh, sGarageVehicleWarpData.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh].vPos, 5.0, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, fCONST_VEHGEN_TARGET_RADIUS, fCONST_VEHGEN_STRAIGHT_LINE_DIST)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							SceneTool_ExecutePan(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh], sGarageData.camCutscene, sGarageData.camCutscene2)
							
							bFakeGarageReverse = FALSE
							iGarageClear = -1
							sGarageData.iWarpControl = iCONST_WARP_15_exitVehExt
						ELSE
						
							REMOVE_ANIM_DICT(sOpenGarageAnimDict)
							RESET_VEHICLE_GEN_LOADED_CHECKS()
							
							//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							RENDER_SCRIPT_CAMS(FALSE,FALSE)
							IF DOES_CAM_EXIST(sGarageData.camCutscene)
								SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
								DESTROY_CAM(sGarageData.camCutscene)
							ENDIF
							IF DOES_CAM_EXIST(sGarageData.camCutscene2)
								SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
								DESTROY_CAM(sGarageData.camCutscene2)
							ENDIF
							IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
								SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
								FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
								vehFakeGarageEnter = NULL
							ENDIF
//							IF DOES_ENTITY_EXIST(objFakeGarageDoor)
//								DELETE_OBJECT(objFakeGarageDoor)
//							ENDIF
							
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
		//					IF IS_SYNCHRONIZED_SCENE_RUNNING(iGarageEntrySynchSceneID)
		//						DETACH_SYNCHRONIZED_SCENE(iGarageEntrySynchSceneID)
		//					ENDIF
		//					iGarageEntrySynchSceneID = -1
							
							FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<198.9538, -1026.1301, -100.0000>>)
		//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 334.1665)
							
							SET_ENTITY_COORDS(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vGarageExitCoords[1])
							SET_ENTITY_HEADING(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.fGarageExitHeading[1])
							
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							SETTIMERA(0)
							
							// Moved here due to url:bugstar:2108774
							DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<198.0043, -999.7775, -100.0000>>, 50.0)
							
//							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sGarageVehicleWarpData.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitPedClose].vPos, PEDMOVE_WALK)
//							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
//							SceneTool_ExecutePan(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedClose], sGarageData.camCutscene, sGarageData.camCutscene2)
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
							sGarageData.iWarpControl = iCONST_WARP_16
						ENDIF
						
						IF sGarageData.iWarpControl <> iCONST_WARP_16
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						
						//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						
						SETTIMERA(0)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						
						// Reposition buddies
						INT iNearbyPedCount
						INT iNearbyPed
						iNearbyPedCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
						REPEAT iNearbyPedCount iNearbyPed
							IF DOES_ENTITY_EXIST(nearbyPeds[iNearbyPed])
							AND NOT IS_PED_INJURED(nearbyPeds[iNearbyPed])
							AND IS_PED_GROUP_MEMBER(nearbyPeds[iNearbyPed], GET_PLAYER_GROUP(PLAYER_ID()))
								IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
									currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									IF DOES_ENTITY_EXIST(currentVeh)
									AND IS_VEHICLE_DRIVEABLE(currentVeh)
									AND (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(currentVeh) > iNearbyPed)	//	GET_VEHICLE_NUMBER_OF_PASSENGERS(currentVeh))
										CPRINTLN(DEBUG_AMBIENT, "PROCESS_GARAGE_VEHICLE_WARP -> reposition player group member [", iNearbyPed, ":", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyPeds[iNearbyPed])), "] into players vehicle")
										SET_PED_INTO_VEHICLE(nearbyPeds[iNearbyPed], currentVeh, INT_TO_ENUM( VEHICLE_SEAT, iNearbyPed ) )
									ENDIF
								ELSE
									CPRINTLN(DEBUG_AMBIENT, "PROCESS_GARAGE_VEHICLE_WARP -> reposition player group member [", iNearbyPed, ":", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(nearbyPeds[iNearbyPed])), "] to exterior ", sGarageData.sVehGenPurchData.vGarageExitCoords[1])
									SET_ENTITY_COORDS_NO_OFFSET(nearbyPeds[iNearbyPed], sGarageData.sVehGenPurchData.vGarageExitCoords[1])
								ENDIF
							ENDIF
						ENDREPEAT
						
						//DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<198.0043, -999.7775, -100.0000>>, 50.0) - Moved due to url:bugstar:2108774
						
						FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
						
						fakeGarageCarEnterCutsceneStage = 0
						
					ENDIF
				ENDIF
			BREAK
			CASE iCONST_WARP_15_exitVehExt
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " > "
				str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				BOOL bProgessPastStage13
				bProgessPastStage13 = TRUE
				
				IF (NOT IS_NEW_LOAD_SCENE_ACTIVE())
					#IF IS_DEBUG_BUILD
					str  = "load scene not active"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_RED)
					#ENDIF
					
				ELIF (IS_NEW_LOAD_SCENE_LOADED())
					#IF IS_DEBUG_BUILD
					str  = "load scene loaded"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "wait for load scene"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset, HUD_COLOUR_REDDARK)
					#ENDIF
					
					bProgessPastStage13 = FALSE
				ENDIF
				IF HAVE_ALL_VEHICLE_GENS_LOADED_NEAR_PLAYER()
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "two - wait for vehgens"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					bProgessPastStage13 = FALSE
				ENDIF
//				IF (TIMERA() > ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)+1000))
//					
//				ELSE
//					str  = "emergency timer! - timera: "
//					str += TIMERA()
//					str += " < "
//					str += ROUND((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)+1000)
//					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
//					
//					bProgessPastStage13 = FALSE
//				ENDIF
				IF (DOES_CAM_EXIST(sGarageData.camCutscene) AND IS_CAM_RENDERING(sGarageData.camCutscene))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - camCutscene interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						bProgessPastStage13 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene - timera: "
						str += TIMERA()
						str += " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						bProgessPastStage13 = FALSE
					ENDIF
				ELIF (DOES_CAM_EXIST(sGarageData.camCutscene2) AND IS_CAM_RENDERING(sGarageData.camCutscene2))
					IF IS_CAM_INTERPOLATING(sGarageData.camCutscene2)
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - camCutscene2 interping"
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						bProgessPastStage13 = FALSE
					ENDIF
					IF (TIMERA() <= ((sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)))
						#IF IS_DEBUG_BUILD
						str  = "camCutscene2 - timera: "
						str += TIMERA()
						str +=  " < "
						str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)
						DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
						#ENDIF
						bProgessPastStage13 = FALSE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "four-five"
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					bProgessPastStage13 = FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF sGarageVehicleWarpData.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitVeh]
					SET_TEXT_COLOUR(100, 255, 255, 255)
					SET_TEXT_SCALE(0.7, 0.8)
					SET_TEXT_WRAP(0.0, 1.0)
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.6, 0.85, "STRING", "PLACEHOLDER CAMERA")
				ENDIF
				#ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())					
					DO_FAKE_GARAGE_CAR_DRIVE(PLAYER_PED_ID(),
							sGarageVehicleWarpData.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh],
							sGarageVehicleWarpData.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh],
							(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000.0) - 0500,
							sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh],
							GRAPH_TYPE_DECEL,
							fStrOffset)
				ENDIF
				
				// Moved here due to url:bugstar:2108774
				// Clear gararge once player is outside
				IF iGarageClear < 1
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<198.0043, -999.7775, -100.0000>>) > 55
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc iGarageClear = ", iGarageClear)
					IF iGarageClear = 0
						DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<198.0043, -999.7775, -100.0000>>, 50.0)
					ENDIF
					iGarageClear++
				ENDIF
				
				IF bProgessPastStage13
					SETTIMERA(0)
					sGarageData.iWarpControl = iCONST_WARP_16
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
			BREAK
			CASE iCONST_WARP_16
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh]*1000)
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				BOOL bProgessPastStage16
				bProgessPastStage16 = TRUE
				
				IF (TIMERA() <= ((sGarageVehicleWarpData.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh]*1000)))
					#IF IS_DEBUG_BUILD
					str  = "fHold: "
					str += TIMERA()
					str +=  " < "
					str += ROUND(sGarageVehicleWarpData.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh]*1000)
					DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
					#ENDIF
					bProgessPastStage16 = FALSE
				ENDIF
				
				IF bProgessPastStage16
					sGarageData.iWarpControl = iCONST_WARP_17
				ENDIF
			BREAK
			CASE iCONST_WARP_17
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
					UNPIN_INTERIOR(sGarageData.interiorInstanceIndex)
				ENDIF
				g_tlIgnoreBuildingControllerChecks = ""
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_GARAGEM_SP, TRUE)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEM_SP, TRUE)
				
				g_sVehicleGenNSData.bInGarage = FALSE
				
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				ENDIF
				
				SETTIMERA(0)
			
				//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				IF DOES_CAM_EXIST(sGarageData.camCutscene)
					SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
					DESTROY_CAM(sGarageData.camCutscene)
				ENDIF
				IF DOES_CAM_EXIST(sGarageData.camCutscene2)
					SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
					DESTROY_CAM(sGarageData.camCutscene2)
				ENDIF
				IF DOES_ENTITY_EXIST(vehFakeGarageEnter)
					SET_ENTITY_COLLISION(vehFakeGarageEnter, TRUE)
					FREEZE_ENTITY_POSITION(vehFakeGarageEnter, FALSE)
					vehFakeGarageEnter = NULL
				ENDIF
//				IF DOES_ENTITY_EXIST(objFakeGarageDoor)
//					DELETE_OBJECT(objFakeGarageDoor)
//				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(currentVeh)
					AND IS_VEHICLE_DRIVEABLE(currentVeh)
						SET_ENTITY_COORDS(currentVeh, sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos)
						SET_ENTITY_HEADING(currentVeh, sGarageVehicleWarpData.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].fRot)
						
						SET_VEHICLE_ON_GROUND_PROPERLY(currentVeh)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
					ENDIF
				ENDIF
				
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				IF iWantedLevelBeforeWarp > 0
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iWantedLevelBeforeWarp, FALSE)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID(), FALSE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
				//AND IS_VEHICLE_DRIVEABLE(lastVehicleBeforeWarp) - rob - 2114869 - ensure the lastvehiclebeforewarp is cleaned up even if dead
				AND IS_ENTITY_A_MISSION_ENTITY(lastVehicleBeforeWarp)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(lastVehicleBeforeWarp, FALSE)
				AND lastVehicleBeforeWarp <> GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH)	// - 2357715 - Don't delete vehicle if it's being used by the mission vehicle gen
					SET_VEHICLE_AS_NO_LONGER_NEEDED(lastVehicleBeforeWarp)
				ENDIF
				
				CLEAR_BIT(sData.flags, ENUM_TO_INT(VEHGEN_NS_FLAG_USE_WORLD_RANGE_DIST_CHECKS))
				bLoadSceneStartedFromVehGen = FALSE
				
				sGarageData.iWarpControl = iCONST_WARP_18
			BREAK
			CASE iCONST_WARP_18
				#IF IS_DEBUG_BUILD
				str  = "TIMERA(): "
				str += TIMERA()
				str += " "
				str += ROUND(sGarageVehicleWarpData.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration*1000)
				DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iWarpControl, fStrOffset)
				#ENDIF
				
				IF IS_SCREEN_FADED_IN()
					sGarageData.iWarpControl = iCONST_WARP_0
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF g_sVehicleGenNSData.bInGarage
			PRINTLN("Cleaning up garage interior as garage entry is not allowed.")
			SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_GARAGEM_SP, TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEM_SP, TRUE)
			g_sVehicleGenNSData.bInGarage = FALSE
		ENDIF
		
		sGarageData.iWarpControl = iCONST_WARP_0
		IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
			RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
		ENDIF
	ENDIF
	
	IF g_sVehicleGenNSData.bInGarage
	
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		
		DISABLE_SELECTOR_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	ENDIF
	
	
	IF NOT bPrintWarpHelp
	AND sGarageData.iPurchaseControl = 0
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF NOT IS_STRING_NULL_OR_EMPTY(tlWarpHelpPrinted)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WEB_VEH_INV")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WEB_VEH_FULL")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR_GAR_05")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR_GAR_06")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CAR_GAR_EXIT")
					CLEAR_HELP()
				ENDIF
				tlWarpHelpPrinted = ""
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Process the purchase of a garage
PROC PROCESS_GARAGE_PURCHASE()
	IF IS_PED_INJURED(PLAYER_PED_ID())
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	OR NOT IS_PLAYER_PED_PLAYABLE(eCurrentPlayerPed)
		EXIT
	ENDIF
	
	// Emails for vehicles purchases via website
	IF NOT g_bBrowserVisible
		IF g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[iEmailToProcess] != INVALID_TIMEOFDAY
			IF NOT g_sVehicleGenNSData.bLeaveAreaBeforeCreating[g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].eVehGen[iEmailToProcess]]
				IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(GET_CURRENT_TIMEOFDAY(), g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[iEmailToProcess])
					
					WEBSITE_INDEX_ENUM eSiteID = INT_TO_ENUM(WEBSITE_INDEX_ENUM, g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].iSiteID[iEmailToProcess])
					CPRINTLN(DEBUG_INTERNET, "SEND_SITE_VEHICLE_CONFIRMATION_MAIL: Site = ",
							GET_WEBSITE_FROM_INDEX(eSiteID),
							", Veh = ",
							GET_LABEL_BUYABLE_VEHICLE(INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iEmailToProcess)),
							", Ped = ", 
							GET_PLAYER_PED_STRING(eCurrentPlayerPed))
					
					SEND_SITE_VEHICLE_CONFIRMATION_MAIL(eSiteID, INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iEmailToProcess), eCurrentPlayerPed)
					
					VEHICLE_GEN_DATA_STRUCT sTempData
					GET_VEHICLE_GEN_DATA(sTempData, g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].eVehGen[iEmailToProcess])
					IF sTempData.model = DUMMY_MODEL_FOR_SCRIPT
						//commented out as the car was getting added twice if they took it out of the garage before it hit this. bug:2331192
//						VEHICLE_SETUP_STRUCT vss 
//						CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(INT_TO_ENUM(SITE_BUYABLE_VEHICLE, iEmailToProcess), vss, FALSE, eCurrentPlayerPed)
//						SET_BIT(vss.iFlags, VEHICLE_SETUP_FLAG_KEEP_DEFAULT_EXTRAS)
//						UPDATE_DYNAMIC_VEHICLE_GEN_DATA(g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].eVehGen[iEmailToProcess], vss, <<0,0,0>>, -1)
					ELSE
						// Make sure it is available.
						SET_VEHICLE_GEN_AVAILABLE(g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].eVehGen[iEmailToProcess], TRUE)
					ENDIF
					
					g_savedGlobals.sVehicleGenData.sWebVehicles[eCurrentPlayerPed].todEmailDate[iEmailToProcess] = INVALID_TIMEOFDAY
				ENDIF
			ENDIF
		ENDIF
		iEmailToProcess++
		IF iEmailToProcess >= ENUM_TO_INT(NUMBER_OF_BUYABLE_VEHICLES_SP)
			iEmailToProcess = 0
		ENDIF
	ENDIF
	
	
	
	
	CONST_INT iCONST_PURCHASE_0				0
	CONST_INT iCONST_PURCHASE_1				1
	CONST_INT iCONST_PURCHASE_2				2
	CONST_INT iCONST_PURCHASE_3				3
	CONST_INT iCONST_PURCHASE_4				4
	CONST_INT iCONST_PURCHASE_5				5
	CONST_INT iCONST_PURCHASE_6				6
	CONST_INT iCONST_PURCHASE_7				7
	CONST_INT iCONST_PURCHASE_8				8
	CONST_INT iCONST_PURCHASE_9				9
	
	CONST_INT iCONST_PURCHASE_90			90
	CONST_INT iCONST_PURCHASE_99			99
	
	#IF IS_DEBUG_BUILD
	FLOAT fStrOffset = 0
	TEXT_LABEL_63 str = "sGarageData.iPurchaseControl: iCONST_PURCHASE_"
	str += sGarageData.iPurchaseControl
	IF sGarageData.iPurchaseControl > iCONST_PURCHASE_0
		DRAW_DEBUG_VEHGEN_SCENE_TEXT(str, sGarageData.iPurchaseControl, fStrOffset, DEFAULT, 1)
	ENDIF
	#ENDIF
	
	
	// We update the sGarageData.eClosestGen during the vehicle gen update process 
	
	// Clear any previous data and reset
	IF GET_ENTITY_MODEL(PLAYER_PED_ID()) != sGarageData.ePedModel
	OR sGarageData.iPurchaseControl = iCONST_PURCHASE_99
		IF sGarageData.iContextID != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
		ENDIF
		
		IF sGarageData.iPurchaseControl > iCONST_PURCHASE_0
			CLEAR_HELP()
		ENDIF
		
		sGarageData.ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		sGarageData.iContextID = NEW_CONTEXT_INTENTION
		sGarageData.iPurchaseControl = iCONST_PURCHASE_0
		sGarageData.iTimer = 0
		
		IF bDoTutorial
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP(TRUE)
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			IF DOES_CAM_EXIST(sGarageData.camCutscene)
				CDEBUG2LN(DEBUG_AMBIENT, "Destroying Cam1...")
				SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
				DESTROY_CAM(sGarageData.camCutscene)
			ENDIF
			IF DOES_CAM_EXIST(sGarageData.camCutscene2)
				CDEBUG1LN(DEBUG_AMBIENT, "Destroying Cam2...")
				SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
				DESTROY_CAM(sGarageData.camCutscene2)
			ENDIF
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
			ENDIF
			
			VEHICLE_INDEX vehLastCar
			vehLastCar = GET_PLAYERS_LAST_VEHICLE()
			IF DOES_ENTITY_EXIST(vehLastCar)
			AND IS_VEHICLE_DRIVEABLE(vehLastCar)
				SET_ENTITY_VISIBLE(vehLastCar, TRUE)
				FREEZE_ENTITY_POSITION(vehLastCar, FALSE)
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehLastCar)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehLastCar)
				ENDIF
			ENDIF
			
			INT iMaxTime, iTimer
			iMaxTime = 5000
			iTimer = GET_GAME_TIMER()
				
			WHILE NOT IS_GAMEPLAY_CAM_RENDERING()
			AND GET_GAME_TIMER() - iTimer < iMaxTime
				WAIT(0)
			ENDWHILE
							
			IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
				UNPIN_INTERIOR(sGarageData.interiorInstanceIndex)
			ENDIF
			g_tlIgnoreBuildingControllerChecks = ""
			
			IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
			OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
			OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
				CDEBUG2LN(DEBUG_AMBIENT, "Disabling the interior...")
				SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, TRUE)
				SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, TRUE)
			ENDIF
			NEW_LOAD_SCENE_STOP()
			CLEAR_FOCUS()
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	
			bDoTutorial = FALSE
			g_bCurrentlyBuyingProperty = FALSE
			CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> cleaned up cutscene early")	
		ENDIF
	ENDIF
	
	BOOL bSuppressStartPurchase = FALSE
	IF (NOT bDoTutorial AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vBuyPos1, sGarageData.sVehGenPurchData.vBuyPos2, sGarageData.sVehGenPurchData.fBuyWidth))
	OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND NOT sGarageData.sVehGenPurchData.bWarpToGarage)
	OR (NOT IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
	OR (NOT bDoTutorial AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_ACQUIRED))
	OR (NOT bDoTutorial AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE))
	OR (NOT bDoTutorial AND IS_PAUSE_MENU_ACTIVE())
	OR NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
	OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
	OR IS_PED_CLIMBING(PLAYER_PED_ID()) 
	OR IS_PED_ON_VEHICLE(PLAYER_PED_ID())
	OR g_bPlayerHasActiveProstitute
	OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PED_BEING_JACKED(PLAYER_PED_ID()))
		IF sGarageData.iPurchaseControl > iCONST_PURCHASE_0
		AND sGarageData.iPurchaseControl != iCONST_PURCHASE_90
			bForceGarageChecks = TRUE
			sGarageData.iPurchaseControl = iCONST_PURCHASE_99
		ENDIF
		bSuppressStartPurchase = TRUE
	ENDIF
	
	TEXT_LABEL_15 tl15HelpText
	FLOAT fStreamingFarClip = 20
	NEWLOADSCENE_FLAGS loadSceneFlags = NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE | NEWLOADSCENE_FLAG_REQUIRE_COLLISION
	VECTOR vLoadSceneDir
	
	SWITCH sGarageData.iPurchaseControl
		CASE iCONST_PURCHASE_0
			IF sGarageData.eClosestGen != VEHGEN_NONE
			AND GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_AVAILABLE)
			AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_ACQUIRED)
			AND NOT bProcessingGarageChecks
			AND NOT bSuppressStartPurchase
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vBuyPos1, sGarageData.sVehGenPurchData.vBuyPos2, sGarageData.sVehGenPurchData.fBuyWidth)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					tl15HelpText = sGarageData.sVehGenPurchData.tl15HelpText
					tl15HelpText += "_01" // Press ~a~ to buy
					REGISTER_CONTEXT_INTENTION(sGarageData.iContextID, CP_MEDIUM_PRIORITY, tl15HelpText)
					sGarageData.iPurchaseControl = iCONST_PURCHASE_1
				ENDIF
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_1
			IF HAS_CONTEXT_BUTTON_TRIGGERED(sGarageData.iContextID)
				IF (GET_TOTAL_CASH(eCurrentPlayerPed) < sGarageData.sVehGenPurchData.iCost)
					sGarageData.iTimer = GET_GAME_TIMER()
					RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
					iContextCounter = 0
					sGarageData.iPurchaseControl = iCONST_PURCHASE_90
				ELSE
					iContextCounter = 0
					sGarageData.iTimer = GET_GAME_TIMER()
					RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
					
					sGarageData.iPurchaseControl = iCONST_PURCHASE_2
				ENDIF
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_2
			iContextCounter++
			IF iContextCounter >= 3
				tl15HelpText = sGarageData.sVehGenPurchData.tl15HelpText
				tl15HelpText += "_02" // Press ~a~ to confirm
				REGISTER_CONTEXT_INTENTION(sGarageData.iContextID, CP_MEDIUM_PRIORITY, tl15HelpText)	
				sGarageData.iPurchaseControl = iCONST_PURCHASE_3
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_3
			IF HAS_CONTEXT_BUTTON_TRIGGERED(sGarageData.iContextID)
				IF (GET_TOTAL_CASH(eCurrentPlayerPed) < sGarageData.sVehGenPurchData.iCost)
					sGarageData.iTimer = GET_GAME_TIMER()
					sGarageData.iPurchaseControl = iCONST_PURCHASE_90
				ELSE
				
					PLAY_SOUND_FRONTEND(-1, "PROPERTY_PURCHASE_MEDIUM", "HUD_PROPERTY_SOUNDSET")
				
					bDoTutorial = FALSE
					IF sGarageData.eClosestGen = VEHGEN_WEB_MARINA_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_MARINA_TREVOR	
					OR sGarageData.eClosestGen = VEHGEN_WEB_MARINA_FRANKLIN	
					
						//DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, BAAC_PROP_MARINA, sGarageData.sVehGenPurchData.iCost)
						sPurchaseMsgData.baacType = BAAC_PROP_MARINA
						bDoTutorial = TRUE
					
					ELIF sGarageData.eClosestGen = VEHGEN_WEB_HELIPAD_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_HELIPAD_FRANKLIN	
						
						//DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, BAAC_PROP_HELIPAD, sGarageData.sVehGenPurchData.iCost)
						sPurchaseMsgData.baacType = BAAC_PROP_HELIPAD
						bDoTutorial = TRUE
					
					ELIF sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_MICHAEL	
					OR sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_FRANKLIN	
					
						//DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, BAAC_PROP_HANGAR, sGarageData.sVehGenPurchData.iCost)
						sPurchaseMsgData.baacType = BAAC_PROP_HANGAR
						bDoTutorial = TRUE
						
					ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
					OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
					
						//DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, BAAC_PROP_GARAGE, sGarageData.sVehGenPurchData.iCost)
						sPurchaseMsgData.baacType = BAAC_PROP_GARAGE
						bDoTutorial = TRUE
						
					ENDIF
					
					// Request the scaleform movie to display at the end
					sPurchaseMsgData.siScaleform = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
					sPurchaseMsgData.bMessageSet = FALSE
					sPurchaseMsgData.bSoundPlayed = FALSE
					sPurchaseMsgData.bMessageOut = FALSE
					
					bLeaveAreaAfterPurchase = TRUE
					
					//Set the buying property global bool to TRUE early
					g_bCurrentlyBuyingProperty = TRUE
					
					IF bDoTutorial
						// Vehicle garage also within an interior.
						IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
						OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
						OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
						
							SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, FALSE)
							SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, FALSE)
							
							//pin the interior into memory
							sGarageData.interiorInstanceIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<198.3659, -1020.2732, -100.0000>>, "v_garagem_sp")
							IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
								IF NOT IS_INTERIOR_READY(sGarageData.interiorInstanceIndex)
									PIN_INTERIOR_IN_MEMORY(sGarageData.interiorInstanceIndex)	
									
									CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> interior pinned in memory request. frame = ", GET_FRAME_COUNT())
								ENDIF
								g_tlIgnoreBuildingControllerChecks = "v_garagem_sp"
							ENDIF
						ENDIF
						
						// rob - 2109875 - set player invincible while loading tutorial assets
						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						
						NEW_LOAD_SCENE_STOP()
						sGarageData.iTimer = GET_GAME_TIMER()
						sGarageData.iPurchaseControl = iCONST_PURCHASE_4
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> tutorial set to go ahead")	
					ELSE					
						sGarageData.iPurchaseControl = iCONST_PURCHASE_8
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> tutorial skipped")	
					ENDIF					
					RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
				ENDIF
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_4
		
			BOOL bSafeToContinue
			bSafeToContinue = TRUE
		
			// Wait for interior to be uncapped/enabled...
			IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
			OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
			OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
				sGarageData.interiorInstanceIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<198.3659, -1020.2732, -100.0000>>, "v_garagem_sp")
				IF NOT IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
				OR IS_INTERIOR_CAPPED(sGarageData.interiorInstanceIndex)
				OR IS_INTERIOR_DISABLED(sGarageData.interiorInstanceIndex)
					SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, FALSE)
					SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, FALSE)
					bSafeToContinue = FALSE
				ENDIF
			ENDIF
			
			IF bSafeToContinue
				// tweak the default values for the hangar setups
				IF sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_MICHAEL
					fStreamingFarClip = 95.0	// for use with NEW_LOAD_SCENE_START bigger dist for hangars as the shot is pulled out a lot
					loadSceneFlags = NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE
				ELIF sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_FRANKLIN	
					fStreamingFarClip = 150.0	// for use with NEW_LOAD_SCENE_START bigger dist for hangars as the shot is pulled out a lot
					loadSceneFlags = NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE
					
				ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
				OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
				OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
					fStreamingFarClip = 150.0	// for use with NEW_LOAD_SCENE_START bigger dist for hangars as the shot is pulled out a lot
					loadSceneFlags = NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE
					
				ENDIF
				vLoadSceneDir = <<COS(sGarageData.sVehGenPurchData.mPans1.mStart.vRot.Z+90), SIN(sGarageData.sVehGenPurchData.mPans1.mStart.vRot.Z+90), 0.0>>
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> NEW_LOAD_SCENE_START - Pos : ", sGarageData.sVehGenPurchData.mPans2.mStart.vPos,
											" vLoadSceneDir was :", vLoadSceneDir, " fStreamingFarClip : ", fStreamingFarClip, " loadSceneFlags : ", loadSceneFlags)	
											
				IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
				OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
				OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
					IF NEW_LOAD_SCENE_START_SPHERE(<<200.4651, -1020.6310, -100.0000>>, 50.0)
					OR ((GET_GAME_TIMER() - sGarageData.iTimer) > 3500)	// fail safe for NEW_LOAD_SCENE_START() not returning TRUE
						sGarageData.iTimer = GET_GAME_TIMER()
						sGarageData.iPurchaseControl = iCONST_PURCHASE_5
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> load scene for cutscene setup.
						Fail safe timer at - (GET_GAME_TIMER() - sGarageData.iTimer) > 3500) : ", ((GET_GAME_TIMER() - sGarageData.iTimer) > 3500))
					ENDIF
				ELSE
					// B*1155612 - stream in area ready for the cutscene shot
					IF NEW_LOAD_SCENE_START(sGarageData.sVehGenPurchData.mPans1.mStart.vPos, vLoadSceneDir, fStreamingFarClip, loadSceneFlags)
					OR ((GET_GAME_TIMER() - sGarageData.iTimer) > 3500)	// fail safe for NEW_LOAD_SCENE_START() not returning TRUE
						sGarageData.iTimer = GET_GAME_TIMER()
						sGarageData.iPurchaseControl = iCONST_PURCHASE_5
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> load scene for cutscene setup.
						Fail safe timer at - (GET_GAME_TIMER() - sGarageData.iTimer) > 3500) : ", ((GET_GAME_TIMER() - sGarageData.iTimer) > 3500))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_5
			IF (IS_NEW_LOAD_SCENE_ACTIVE()
			AND IS_NEW_LOAD_SCENE_LOADED()
			AND (NOT IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex) OR IS_INTERIOR_READY(sGarageData.interiorInstanceIndex)))
			OR ((GET_GAME_TIMER() - sGarageData.iTimer) > 10000)	// fail safe for IS_NEW_LOAD_SCENE_LOADED() not returning TRUE
				sGarageData.iTimer = GET_GAME_TIMER()
				sGarageData.iPurchaseControl = iCONST_PURCHASE_6
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> cutscene waiting on load scene : ", ((GET_GAME_TIMER() - sGarageData.iTimer)))
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_6
			IF ((GET_GAME_TIMER() - sGarageData.iTimer) > 1000)	// hold load scene for a second
				// rob - 2114869 - clear previous lastVehicleBeforeWarp
				IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
				AND IS_ENTITY_A_MISSION_ENTITY(lastVehicleBeforeWarp)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(lastVehicleBeforeWarp)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(lastVehicleBeforeWarp)
				ENDIF
						
				lastVehicleBeforeWarp = GET_PLAYERS_LAST_VEHICLE()
				IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
				AND IS_VEHICLE_DRIVEABLE(lastVehicleBeforeWarp)
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(lastVehicleBeforeWarp))
				AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(lastVehicleBeforeWarp))
				AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(lastVehicleBeforeWarp))
					IF NOT IS_ENTITY_A_MISSION_ENTITY(lastVehicleBeforeWarp)
						SET_ENTITY_AS_MISSION_ENTITY(lastVehicleBeforeWarp, FALSE, FALSE)
					ENDIF
				ELSE
					lastVehicleBeforeWarp = NULL
				ENDIF
				
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_EXPLOSIONS)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_PROJECTILES)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REENABLE_CONTROL_ON_DEATH)
					SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
				CLEAR_PRINTS()
				CLEAR_HELP()
				//vLoadSceneDir = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sGarageData.sVehGenPurchData.mPans2.mStart.vPos, sGarageData.sVehGenPurchData.mPans2.mStart.vPos.Z, <<0.0,1.0,0.0>>)	// works but gives negative values whic the load scene func asserts about
				vLoadSceneDir = <<COS(sGarageData.sVehGenPurchData.mPans1.mStart.vRot.Z+90), SIN(sGarageData.sVehGenPurchData.mPans1.mStart.vRot.Z+90), 0.0>>
				IF NOT sGarageData.sVehGenPurchData.bWarpToGarage
					SET_FOCUS_POS_AND_VEL(sGarageData.sVehGenPurchData.mPans1.mStart.vPos, vLoadSceneDir)	// sGarageData.sVehGenData.coords, << 0.0, 0.0, 0.0 >>)	// B*1155612 - streaming improvement
				ENDIF
				// Setup the cutscene camera
				IF NOT DOES_CAM_EXIST(sGarageData.camCutscene)
					sGarageData.camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)			
				ENDIF
				IF DOES_CAM_EXIST(sGarageData.camCutscene)
				
					IF sGarageData.sVehGenPurchData.bWarpToGarage
						SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("GtaMloRoom001")
					ENDIF
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
//					SET_CAM_PARAMS(sGarageData.camCutscene,sGarageData.sVehGenPurchData.vCamPos1,sGarageData.sVehGenPurchData.vCamRot1,sGarageData.sVehGenPurchData.fCamFov1, 0)
//					SET_CAM_PARAMS(sGarageData.camCutscene,sGarageData.sVehGenPurchData.vCamPos2,sGarageData.sVehGenPurchData.vCamRot2,sGarageData.sVehGenPurchData.fCamFov2, 7500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//					SHAKE_CAM(sGarageData.camCutscene,"HAND_SHAKE",0.2)
					SceneTool_ExecutePan(sGarageData.sVehGenPurchData.mPans1, sGarageData.camCutscene, sGarageData.camCutscene2)
				ENDIF						
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				NEW_LOAD_SCENE_STOP()
				sGarageData.iPurchaseControl = iCONST_PURCHASE_7
				sGarageData.iTimer = GET_GAME_TIMER()
				RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> start cutscene")	
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_7
			IF (GET_GAME_TIMER() - sGarageData.iTimer) < 7000
			AND NOT IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()	
				tl15HelpText = sGarageData.sVehGenPurchData.tl15HelpText
				tl15HelpText += "_05" //Updated for B*1155613
				// Helipad - Helicopters purchased from websites or that you land here will be stored here.
				// Marina - Boats purchased from websites or that you dock here will be stored here.
				// Hangar - Planes purchased from websites or that you land here will be stored here.
				// Garafe - Vehicles purchased from websites or driven into the garage will be stored here.
				PRINT_HELP(tl15HelpText)
				
				// stream in the area for the next camera shot ready
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()	
					// tweak the default values for the hangar setups
					IF sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_MICHAEL	
					OR sGarageData.eClosestGen = VEHGEN_WEB_HANGAR_FRANKLIN	
						fStreamingFarClip = 85.0	// for use with NEW_LOAD_SCENE_START bigger dist for hangars as the shot is pulled out a lot
						loadSceneFlags = NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE
					ENDIF
					vLoadSceneDir = <<COS(sGarageData.sVehGenPurchData.mPans2.mStart.vPos.Z+90), SIN(sGarageData.sVehGenPurchData.mPans2.mStart.vPos.Z+90), 0.0>>
			
					NEW_LOAD_SCENE_START(sGarageData.sVehGenPurchData.mPans2.mStart.vPos, vLoadSceneDir, fStreamingFarClip, loadSceneFlags)
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> NEW_LOAD_SCENE_START - Pos : ", sGarageData.sVehGenPurchData.mPans2.mStart.vPos,
										" vLoadSceneDir was :", vLoadSceneDir, " fStreamingFarClip : ", fStreamingFarClip, " loadSceneFlags : ", loadSceneFlags)	
					
				ENDIF
			ELSE
				IF DOES_CAM_EXIST(sGarageData.camCutscene)
					IF sGarageData.sVehGenPurchData.bWarpToGarage
						SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("GtaMloRoom001")
					ENDIF
					
//					SET_CAM_PARAMS(sGarageData.camCutscene,sGarageData.sVehGenPurchData.vCamPos3,sGarageData.sVehGenPurchData.vCamRot3,sGarageData.sVehGenPurchData.fCamFov3, 0)
//					SET_CAM_PARAMS(sGarageData.camCutscene,sGarageData.sVehGenPurchData.vCamPos4,sGarageData.sVehGenPurchData.vCamRot4,sGarageData.sVehGenPurchData.fCamFov4, 11500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//					SHAKE_CAM(sGarageData.camCutscene,"HAND_SHAKE",0.2)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
					SceneTool_ExecutePan(sGarageData.sVehGenPurchData.mPans2, sGarageData.camCutscene, sGarageData.camCutscene2)
				ENDIF							
				
				CLEAR_FOCUS()
				CLEAR_HELP()
				sGarageData.iTimer = GET_GAME_TIMER()
				sGarageData.iPurchaseControl = iCONST_PURCHASE_8
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_8
			IF (GET_GAME_TIMER() - sGarageData.iTimer) < 7000
			AND NOT IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()	
				tl15HelpText = sGarageData.sVehGenPurchData.tl15HelpText
				tl15HelpText += "_06" //Updated for B*1155613
				
				// Helipad - Whilst at the helipad use ~PAD_DPAD_RIGHT~ to select which helicopter you want to pilot.
				// Marina - Whilst at the slip use ~PAD_DPAD_RIGHT~ to select which boat you want to pilot.
				// Hangar - Whilst in the hangar use ~PAD_DPAD_RIGHT~ to select which plane you want to pilot.
				// Garage - Special vehicles will also be stored here. Visit PlayStation®Store to find new downloadable content.
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP(tl15HelpText, 7000)
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: Printing 'Whilst in the hangar'... help")
				ENDIF
			ELSE
				CLEAR_HELP()
				sGarageData.iTimer = GET_GAME_TIMER()
				sGarageData.iPurchaseControl = iCONST_PURCHASE_9
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_9
			IF (GET_GAME_TIMER() - sGarageData.iTimer) < 3500
			AND NOT IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()	
				tl15HelpText = sGarageData.sVehGenPurchData.tl15HelpText
				tl15HelpText += "_03" // Purchased
				//PRINT_HELP(tl15HelpText) 
				IF HAS_SCALEFORM_MOVIE_LOADED(sPurchaseMsgData.siScaleform)
					IF sPurchaseMsgData.bMessageSet
						// The property type has been passed to the scaleform message to display it
						IF NOT sPurchaseMsgData.bSoundPlayed
							PLAY_SOUND_FRONTEND(-1, "UNDER_THE_BRIDGE", "HUD_AWARDS")
							sPurchaseMsgData.bSoundPlayed = TRUE
						ENDIF
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(sPurchaseMsgData.siScaleform, 255, 255, 255, 255)
					ELSE
						BEGIN_SCALEFORM_MOVIE_METHOD(sPurchaseMsgData.siScaleform, "SHOW_SHARD_MIDSIZED_MESSAGE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15HelpText)
						END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
						sPurchaseMsgData.bMessageSet = TRUE
					ENDIF
				ENDIF
			ELIF (GET_GAME_TIMER() - sGarageData.iTimer) < 4000
			AND NOT IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				IF HAS_SCALEFORM_MOVIE_LOADED(sPurchaseMsgData.siScaleform)
					IF sPurchaseMsgData.bMessageOut
						DRAW_SCALEFORM_MOVIE_FULLSCREEN(sPurchaseMsgData.siScaleform, 255, 255, 255, 255)
					ELSE
						BEGIN_SCALEFORM_MOVIE_METHOD(sPurchaseMsgData.siScaleform, "SHARD_ANIM_OUT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE)) 
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
						END_SCALEFORM_MOVIE_METHOD()
						sPurchaseMsgData.bMessageOut = TRUE
					ENDIF
				ENDIF
			ELSE
				IF bDoTutorial	// cutscene cleanup (also done if we drop out early...update this to do it in one place to keep things in sync)
					/*IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF*/
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					IF DOES_CAM_EXIST(sGarageData.camCutscene)
						CDEBUG2LN(DEBUG_AMBIENT, "Destroying Cam1...")
						SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
						DESTROY_CAM(sGarageData.camCutscene)
					ENDIF
					IF DOES_CAM_EXIST(sGarageData.camCutscene2)
						CDEBUG1LN(DEBUG_AMBIENT, "Destroying Cam2...")
						SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
						DESTROY_CAM(sGarageData.camCutscene2)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
										
					VEHICLE_INDEX vehLastCar
					vehLastCar = GET_PLAYERS_LAST_VEHICLE()
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
						
						IF DOES_ENTITY_EXIST(vehLastCar)
						AND IS_VEHICLE_DRIVEABLE(vehLastCar)
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehLastCar)
							SET_ENTITY_COORDS(vehLastCar, sGarageData.sVehGenPurchData.vPlayerCoords, TRUE, TRUE)
							SET_ENTITY_HEADING(vehLastCar, sGarageData.sVehGenPurchData.fPlayerHeading)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehLastCar)
						ELSE
							SET_ENTITY_COORDS(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.vPlayerCoords, TRUE, TRUE)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), sGarageData.sVehGenPurchData.fPlayerHeading)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(lastVehicleBeforeWarp)
					AND IS_VEHICLE_DRIVEABLE(lastVehicleBeforeWarp)
						//bug:2343256 using veh gen to make sure cars dont just disapear when going in and out of the garages 
						VEHICLE_INDEX tempVeh
						tempveh = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH)
						IF GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(tempVehicleData,VEHGEN_MISSION_VEH)
							SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(tempVehicleData,GET_CURRENT_PLAYER_PED_ENUM())
							//Delete if it exists
							IF DOES_ENTITY_EXIST(tempVeh)
								DELETE_VEHICLE(tempVeh)
							ENDIF
						ENDIF	
						IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
							CLEAR_AREA(<<-89.3770, 92.6583, 71.2349>>, 5.0, TRUE)
							SET_ENTITY_COORDS(lastVehicleBeforeWarp, <<-89.3770, 92.6583, 71.2349>>)
							SET_ENTITY_HEADING(lastVehicleBeforeWarp, 154.4846)
							SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-89.3770, 92.6583, 71.2349>>,154.4846)		
						ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
							CLEAR_AREA(<<-62.0307, -1839.8585, 25.6787>>, 5.0, TRUE)
							SET_ENTITY_COORDS(lastVehicleBeforeWarp, <<-62.0307, -1839.8585, 25.6787>>)
							SET_ENTITY_HEADING(lastVehicleBeforeWarp, 319.6985)
							SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-62.0307, -1839.8585, 25.6787>>,319.6985)		
						ELIF sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
							CLEAR_AREA(<<-234.7648, -1150.3105, 21.9224>>, 5.0, TRUE)
							SET_ENTITY_COORDS(lastVehicleBeforeWarp, <<-234.7648, -1150.3105, 21.9224>>)
							SET_ENTITY_HEADING(lastVehicleBeforeWarp, 270.8741)
							SET_MISSION_VEHICLE_GEN_VEHICLE(lastVehicleBeforeWarp,<<-234.7648, -1150.3105, 21.9224>>,270.8741)		
						ENDIF
						SET_VEHICLE_ON_GROUND_PROPERLY(lastVehicleBeforeWarp)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehLastCar)
					AND IS_VEHICLE_DRIVEABLE(vehLastCar)
						SET_ENTITY_VISIBLE(vehLastCar, TRUE)
						FREEZE_ENTITY_POSITION(vehLastCar, FALSE)
						IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehLastCar)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehLastCar)
						ENDIF
					ENDIF
						
					INT iMaxTime, iTimer
					iMaxTime = 5000
					iTimer = GET_GAME_TIMER()
						
					WHILE NOT IS_GAMEPLAY_CAM_RENDERING()
					AND GET_GAME_TIMER() - iTimer < iMaxTime
						WAIT(0)
					ENDWHILE
							
					IF IS_VALID_INTERIOR(sGarageData.interiorInstanceIndex)
						UNPIN_INTERIOR(sGarageData.interiorInstanceIndex)
					ENDIF
					g_tlIgnoreBuildingControllerChecks = ""
						
					IF sGarageData.eClosestGen = VEHGEN_WEB_CAR_MICHAEL
					OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_FRANKLIN
					OR sGarageData.eClosestGen = VEHGEN_WEB_CAR_TREVOR
						CDEBUG2LN(DEBUG_AMBIENT, "Disabling the interior...")
						SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, TRUE)
						SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, TRUE)
						g_savedGlobals.sVehicleGenData.bGarageIntroRun = TRUE
					ENDIF
											
					NEW_LOAD_SCENE_STOP()
					CLEAR_FOCUS()
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	
					bDoTutorial = FALSE
					g_bCurrentlyBuyingProperty = FALSE
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: PROCESS_GARAGE_PURCHASE -> finished cutscene")	
				ENDIF		
				IF HAS_SCALEFORM_MOVIE_LOADED(sPurchaseMsgData.siScaleform)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sPurchaseMsgData.siScaleform)
				ENDIF
				
				DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, sPurchaseMsgData.baacType, sGarageData.sVehGenPurchData.iCost)		
				
				SET_VEHICLE_GEN_SAVED_FLAG_STATE(sGarageData.eClosestGen, VEHGEN_S_FLAG_ACQUIRED, TRUE)
				
				//Queue up a dynamic email for this property purchase.
				SEND_DYNAMIC_EMAIL_FOR_PROPERTY_PURCHASE(sGarageData.eClosestGen)
				
				// Delete the blip so we can change sprite when it gets re-created at the spawn coords
				IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[sGarageData.eClosestGen])
					REMOVE_BLIP(g_sVehicleGenNSData.blipID[sGarageData.eClosestGen])
				ENDIF
				ePurchasedVehicleGen = sGarageData.eClosestGen
				
				MAKE_AUTOSAVE_REQUEST()
				sGarageData.iPurchaseControl = iCONST_PURCHASE_99
			ENDIF
		BREAK
		CASE iCONST_PURCHASE_90
			iContextCounter++
			IF iContextCounter >= 3
				IF (GET_GAME_TIMER() - sGarageData.iTimer) < 4000
					tl15HelpText = sGarageData.sVehGenPurchData.tl15HelpText
					tl15HelpText += "_04" // You can't afford
					PRINT_HELP_FOREVER(tl15HelpText) 
				ELSE
					sGarageData.iPurchaseControl = iCONST_PURCHASE_99
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Allows player to purchase a save garage and store/select vehicles.
PROC DO_PURCHASABLE_GARAGE_PROCESSING()
	PROCESS_GARAGE_PURCHASE()
	PROCESS_GARAGE_VEHICLE_WARP()
	PROCESS_GARAGE_VEHICLE_SELECT()
	PROCESS_GARAGE_VEHICLE_DROPOFF()	
ENDPROC

PROC DO_PLAYER_VEHICLE_BLIP_MANAGEMENT()

	
	
	// Remove the blip
	IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipPlayerVehicle)
	
		BOOL bRemoveBlip = FALSE
		
		// Remove blip if the character has changed.
		IF eCurrentPlayerPed != g_sVehicleGenNSData.ePlayerVehiclePed
			PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Removing player vehicle blip for character swap")
			bRemoveBlip = TRUE
			
		// Do not show the blip on mission.
		ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Removing player vehicle blip for mission")
			bRemoveBlip = TRUE
		
		// If we added for coords, make sure it's still safe
		ELIF g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoords
			IF NOT ARE_VECTORS_ALMOST_EQUAL(g_sVehicleGenNSData.vPlayerVehicleCoords, GET_BLIP_COORDS(g_sVehicleGenNSData.blipPlayerVehicle))
			OR GET_NUMBER_OF_SCRIPT_CREATED_PLAYER_VEHICLES(g_sVehicleGenNSData.ePlayerVehiclePed, GET_PLAYER_VEH_MODEL(g_sVehicleGenNSData.ePlayerVehiclePed, VEHICLE_TYPE_CAR)) > 0
				PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Removing player vehicle blip at coords")
				bRemoveBlip = TRUE
			ENDIF
			
		ELIF g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoordsCached
			IF NOT ARE_VECTORS_ALMOST_EQUAL(g_sVehicleGenNSData.vPlayerVehicleCoordsCached, GET_BLIP_COORDS(g_sVehicleGenNSData.blipPlayerVehicle))
			OR GET_NUMBER_OF_SCRIPT_CREATED_PLAYER_VEHICLES(g_sVehicleGenNSData.ePlayerVehiclePed, GET_PLAYER_VEH_MODEL(g_sVehicleGenNSData.ePlayerVehiclePed, VEHICLE_TYPE_CAR)) > 0
				PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Removing player vehicle blip at cached coords")
				bRemoveBlip = TRUE
			ENDIF
		
		// Otherwise check the vehicle is still safe
		ELSE
			IF NOT DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehPlayerVehicle)
			OR NOT IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehPlayerVehicle)
			OR (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sVehicleGenNSData.vehPlayerVehicle))
				
				// If the vehicle exists but has been destroyed we should clear out the cached coords.
				IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehPlayerVehicle)
				AND NOT IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehPlayerVehicle)
					g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
				ENDIF
				
				PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Removing player vehicle blip on vehicle")
				bRemoveBlip = TRUE
			ELSE
				// - To prevent blips popping in and out as we wait for the vehgen update to take place
				//   we cache the coords so we can estimate the position
				// If the vehicle exists but has been destroyed we should clear out the cached coords.
				IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehPlayerVehicle)
					IF IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehPlayerVehicle)
						g_sVehicleGenNSData.vPlayerVehicleCoordsCached = GET_ENTITY_COORDS(g_sVehicleGenNSData.vehPlayerVehicle)
					ELSE
						g_sVehicleGenNSData.vPlayerVehicleCoordsCached = <<0,0,0>>
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bRemoveBlip
			REMOVE_BLIP(g_sVehicleGenNSData.blipPlayerVehicle)
			g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoords = FALSE
			g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoordsCached = FALSE
			g_sVehicleGenNSData.vehPlayerVehicle = NULL
		ENDIF	
	ELSE
		g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoords = FALSE
		g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoordsCached = FALSE
		g_sVehicleGenNSData.vehPlayerVehicle = NULL
	ENDIF
	
	
	// Add the blip
	IF NOT DOES_BLIP_EXIST(g_sVehicleGenNSData.blipPlayerVehicle)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		// Get the current players vehicle.
		BOOL bVehicleAvailable = FALSE
		INT i
		REPEAT NUM_PLAYER_VEHICLE_IDS i
			IF g_eCreatedPlayerVehiclePed[i] != NO_CHARACTER
			AND g_eCreatedPlayerVehicleModel[i] != DUMMY_MODEL_FOR_SCRIPT
			AND DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[i])
			AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(g_viCreatedPlayerVehicleIDs[i]))
			AND g_eCreatedPlayerVehiclePed[i] = eCurrentPlayerPed
			AND IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i]))
				IF (NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_viCreatedPlayerVehicleIDs[i]))
					PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Adding player vehicle blip for vehicle")
					g_sVehicleGenNSData.vehPlayerVehicle = g_viCreatedPlayerVehicleIDs[i]
					g_sVehicleGenNSData.ePlayerVehiclePed = eCurrentPlayerPed
					g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoords = FALSE
					g_sVehicleGenNSData.blipPlayerVehicle = ADD_BLIP_FOR_ENTITY(g_sVehicleGenNSData.vehPlayerVehicle)
				ENDIF
				i = NUM_PLAYER_VEHICLE_IDS+1// Bail
				bVehicleAvailable = TRUE
			ENDIF
		ENDREPEAT
		
		IF NOT bVehicleAvailable
			// No vehicle? Try vehgen coords.
			IF NOT DOES_BLIP_EXIST(g_sVehicleGenNSData.blipPlayerVehicle)
			AND NOT ARE_VECTORS_EQUAL(g_sVehicleGenNSData.vPlayerVehicleCoords, <<0,0,0>>)
				PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Adding player vehicle blip for coords")
				g_sVehicleGenNSData.vehPlayerVehicle = NULL
				g_sVehicleGenNSData.ePlayerVehiclePed = eCurrentPlayerPed
				g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoords = TRUE
				g_sVehicleGenNSData.blipPlayerVehicle = ADD_BLIP_FOR_COORD(g_sVehicleGenNSData.vPlayerVehicleCoords)
			ENDIF
			
			// No vehicle? Try cached coords.
			IF NOT DOES_BLIP_EXIST(g_sVehicleGenNSData.blipPlayerVehicle)
			AND NOT ARE_VECTORS_EQUAL(g_sVehicleGenNSData.vPlayerVehicleCoordsCached, <<0,0,0>>)
				PRINTLN("[PVB] DO_PLAYER_VEHICLE_BLIP_MANAGEMENT - Adding player vehicle blip for cached coords")
				g_sVehicleGenNSData.vehPlayerVehicle = NULL
				g_sVehicleGenNSData.ePlayerVehiclePed = eCurrentPlayerPed
				g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoordsCached = TRUE
				g_sVehicleGenNSData.blipPlayerVehicle = ADD_BLIP_FOR_COORD(g_sVehicleGenNSData.vPlayerVehicleCoordsCached)
			ENDIF
		ELSE
			// Clear out some flags
			g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoords = FALSE
			g_sVehicleGenNSData.bPlayerVehicleBlipUsingCoordsCached = FALSE
		ENDIF
		
		// Configure the blip
		IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipPlayerVehicle)
			SET_BLIP_SPRITE(g_sVehicleGenNSData.blipPlayerVehicle, RADAR_TRACE_GANG_VEHICLE)
			SET_BLIP_NAME_FROM_TEXT_FILE(g_sVehicleGenNSData.blipPlayerVehicle, "PVEHICLE")
			SET_BLIP_AS_SHORT_RANGE(g_sVehicleGenNSData.blipPlayerVehicle, FALSE)
			SET_BLIP_PRIORITY(g_sVehicleGenNSData.blipPlayerVehicle, BLIPPRIORITY_LOW)
			
			INT iBlipCol
			IF eCurrentPlayerPed = CHAR_MICHAEL
				iBlipCol = BLIP_COLOUR_MICHAEL
			ELIF eCurrentPlayerPed = CHAR_FRANKLIN
				iBlipCol = BLIP_COLOUR_FRANKLIN
			ELIF eCurrentPlayerPed = CHAR_TREVOR
				iBlipCol = BLIP_COLOUR_TREVOR
			ENDIF
			SET_BLIP_COLOUR(g_sVehicleGenNSData.blipPlayerVehicle, iBlipCol)
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_ANY_VEHICLE_IMPOUND_SLOT_OCCUPIED(enumCharacterList ePed)
	INT i
	REPEAT MAX_IMPOUND_SLOTS i
		IF IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(i, ePed)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IMPOUND_AREA_OCCUPIED_BY_VEHICLES_OR_PEDS()
	
	IF IS_POSITION_OCCUPIED(<<431.442352,-997.730835,24.761612>>, 4.75, FALSE, TRUE, TRUE, FALSE, FALSE)
		RETURN TRUE
	ELIF IS_POSITION_OCCUPIED(<<436.691315,-997.586914,24.755816>>, 4.75, FALSE, TRUE, TRUE, FALSE, FALSE)
		RETURN TRUE
	ELIF IS_POSITION_OCCUPIED(<<431.07, -1005.57, 26.20>>, 4.75, FALSE, TRUE, TRUE, FALSE, FALSE)
		RETURN TRUE
	ELIF IS_POSITION_OCCUPIED(<<436.52, -1005.47, 26.17>>, 4.75, FALSE, TRUE, TRUE, FALSE, FALSE)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Disables player controls, but enables certain actions like frontend up/down, accept/cancel and looking around with the right stick
PROC DISABLE_PLAYER_CONTROLS()
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_UP)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_DOWN)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_X)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_Y)		
ENDPROC

PROC DO_IMPOUND_SELECT_PROCESSING()

	BOOL bCursorAccept = FALSE

	//////////////////////////////////////////////////////
	///     LAST CAR FOR IMPOUND
	///     
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) = 0
		IF g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle
			IF NOT DOES_ENTITY_EXIST(g_vCarToTrackForImpound)
				SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(g_savedGlobals.sVehicleGenData.sCarForImpound, g_savedGlobals.sVehicleGenData.eCurrentTrackedChar)
				g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle = FALSE
			ELIF NOT IS_VEHICLE_DRIVEABLE(g_vCarToTrackForImpound)
			OR IS_VEHICLE_IN_PLAYERS_GARAGE(g_vCarToTrackForImpound, g_savedGlobals.sVehicleGenData.eCurrentTrackedChar, TRUE)
				g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle = FALSE
				PRINTLN("DO_IMPOUND_SELECT_PROCESSING - Impound track vehicle chracter has entered garage - stop tracking")
			ELSE
				// Update the last character to use the vehicle gen so we send it to the correct impound slot.
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = g_vCarToTrackForImpound
				AND GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) != g_savedGlobals.sVehicleGenData.eCurrentTrackedChar
					PRINTLN("DO_IMPOUND_SELECT_PROCESSING - Impound track vehicle chracter has changed, updating eCurrentTrackedChar.")
					g_savedGlobals.sVehicleGenData.eCurrentTrackedChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
				ENDIF
				
				// Stop tracking if this is owned by the vehicle gen controller script
				IF g_vCarToTrackForImpound != vehImpoundedPlayerVehicle
					INT iInstanceID
					STRING sScriptName = GET_ENTITY_SCRIPT(g_vCarToTrackForImpound, iInstanceID)
					IF NOT IS_STRING_NULL_OR_EMPTY(sScriptName)
						IF GET_HASH_KEY(sScriptName) = GET_HASH_KEY("vehicle_gen_controller")
							PRINTLN("DO_IMPOUND_SELECT_PROCESSING - Impound track vehicle is now owned by vehicle gen controller, not sending to impound. model: ",GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(g_vCarToTrackForImpound)))
							g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle = FALSE
							
							// Keep a hold of this vehicle ID so we can re-track if it is no longer owned by the vehicle gen script.
							vehPreviousCarToTrackForImpound = g_vCarToTrackForImpound
							IF g_sVehicleGenNSData.vehicleID[VEHGEN_MISSION_VEH] = g_vCarToTrackForImpound
							OR (g_vehHandoverToGen = g_vCarToTrackForImpound AND g_eVehGenToRecieveVehicle = VEHGEN_MISSION_VEH)
								SET_PACKED_STAT_INT(PACKED_SP_VEHGEN_TRACKED_FOR_IMPOUND, ENUM_TO_INT(VEHGEN_MISSION_VEH))
							ELSE
								SET_PACKED_STAT_INT(PACKED_SP_VEHGEN_TRACKED_FOR_IMPOUND, 0)
							ENDIF
							
							g_vCarToTrackForImpound = NULL
						ENDIF
					ENDIF
				ENDIF
				
				// Stop tracking if we have switched away and the switch system has a handle to this vehicle.
				IF g_savedGlobals.sVehicleGenData.eCurrentTrackedChar != GET_CURRENT_PLAYER_PED_ENUM()
				AND g_sPlayerLastVeh[g_savedGlobals.sVehicleGenData.eCurrentTrackedChar].model = g_savedGlobals.sVehicleGenData.sCarForImpound.eModel
				AND ARE_STRINGS_EQUAL(g_sPlayerLastVeh[g_savedGlobals.sVehicleGenData.eCurrentTrackedChar].tlNumberPlate, g_savedGlobals.sVehicleGenData.sCarForImpound.tlPlateText)
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					PRINTLN("DO_IMPOUND_SELECT_PROCESSING - Impound track vehicle is now stored in switch data, not sending to impound.")
					CLONE_VEHICLE_SETUP_STRUCT(g_savedGlobals.sVehicleGenData.sCarForImpound, g_savedGlobals.sVehicleGenData.sImpoundSwitchVehicles[g_savedGlobals.sVehicleGenData.eCurrentTrackedChar])
					g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle = FALSE
					
					PRINTLN("IMPOUND FIX 1 - previous impound track is now handled by the switch vehicle - ignore")
					vehPreviousCarToTrackForImpound = NULL
					SET_PACKED_STAT_INT(PACKED_SP_VEHGEN_TRACKED_FOR_IMPOUND, 0)
					g_vCarToTrackForImpound = NULL
				ENDIF
			ENDIF
		ELSE
			// Track again if player removes from garage
			IF DOES_ENTITY_EXIST(g_vCarToTrackForImpound)
			AND IS_VEHICLE_DRIVEABLE(g_vCarToTrackForImpound)
			AND NOT IS_VEHICLE_IN_PLAYERS_GARAGE(g_vCarToTrackForImpound, g_savedGlobals.sVehicleGenData.eCurrentTrackedChar, TRUE)
				g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle = TRUE
				PRINTLN("DO_IMPOUND_SELECT_PROCESSING - Impound track vehicle chracter has left garage - start tracking")
			ENDIF
		ENDIF
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///     IMPOUND FIX 1 - If a previous impound vehicle was owned by a vehicle gen script, re-track if it's no longer owned by it.
		///     
		IF DOES_ENTITY_EXIST(vehPreviousCarToTrackForImpound)
		AND IS_VEHICLE_DRIVEABLE(vehPreviousCarToTrackForImpound)
			IF NOT g_savedGlobals.sVehicleGenData.bTrackingImpoundVehicle
			AND vehPreviousCarToTrackForImpound != g_vCarToTrackForImpound
			AND vehPreviousCarToTrackForImpound != vehLastMissionVehGenToStreamOut
			AND NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPreviousCarToTrackForImpound)
				PRINTLN("IMPOUND FIX 1 - previous impound track is no longer a vehgen - re-track")
				TRACK_VEHICLE_FOR_IMPOUND(vehPreviousCarToTrackForImpound)
				vehPreviousCarToTrackForImpound = NULL
				SET_PACKED_STAT_INT(PACKED_SP_VEHGEN_TRACKED_FOR_IMPOUND, 0)
			ENDIF
		ELSE
			IF vehPreviousCarToTrackForImpound != NULL
				PRINTLN("IMPOUND FIX 1 - previous impound track is no longer valid - ignore")
				vehPreviousCarToTrackForImpound = NULL
				SET_PACKED_STAT_INT(PACKED_SP_VEHGEN_TRACKED_FOR_IMPOUND, 0)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehLastMissionVehGenToStreamOut)
		AND IS_VEHICLE_DRIVEABLE(vehLastMissionVehGenToStreamOut)
			// Nothing to do just now.
		ELSE
			IF vehLastMissionVehGenToStreamOut != NULL
				PRINTLN("IMPOUND FIX 1 - mission vehicle gen streamed out by code so clear vehicle index")
				vehLastMissionVehGenToStreamOut = NULL
			ENDIF
		ENDIF
	ENDIF
	
	//////////////////////////////////////////////////////
	///     LAST MISSION VEHGEN FOR IMPOUND
	///     
	IF sTempImpoundVehData.eModel != DUMMY_MODEL_FOR_SCRIPT
	AND g_vehHandoverToGen = NULL
		SEND_VEHICLE_DATA_TO_IMPOUND_USING_SETUP(sTempImpoundVehData, eTempImpoundChar)
		sTempImpoundVehData.eModel = DUMMY_MODEL_FOR_SCRIPT
	ENDIF

	VECTOR vLocatePos1 = <<433.672119,-1006.537537,25.963509>>
	VECTOR vLocatePos2 = <<433.657806,-1017.500000,32.098953>>
	FLOAT fWidth = 11.25
	
	IF iImpoundMenuControl > 0
	AND iImpoundMenuControl < 99
		IF iImpoundMenuControl != 3
			IF IS_PED_INJURED(PLAYER_PED_ID())
			OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vLocatePos1, vLocatePos2, fWidth)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Cancel impound retrieval...")
				iImpoundMenuControl = 99
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ANY_VEHICLE_IMPOUND_SLOT_OCCUPIED(eCurrentPlayerPed)
		IF NOT DOES_BLIP_EXIST(blipImpound)
			IF (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION) OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE))
			AND iImpoundMenuControl != 3
//			AND GET_TOTAL_CASH(eCurrentPlayerPed) >= 250
			AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				blipImpound = CREATE_BLIP_FOR_COORD(<<428.37, -1013.50, 27.93>>)
				SET_BLIP_SPRITE(blipImpound, RADAR_TRACE_GANG_VEHICLE)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipImpound, "IMPOUND_BLIPNAME")
				SET_BLIP_AS_SHORT_RANGE(blipImpound, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Car impounded, blip created")
			ENDIF
		ELIF NOT (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION) OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE))
		OR iImpoundMenuControl = 3
//		OR GET_TOTAL_CASH(eCurrentPlayerPed) < 250
		OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			REMOVE_BLIP(blipImpound)
			CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Removed blip because player is on-mission, wanted or still collecting")
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipImpound)
			REMOVE_BLIP(blipImpound)
			CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> No cars impounded, removing blip")
		ENDIF
	ENDIF
	
	SWITCH iImpoundMenuControl
		CASE 0
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF sGarageData.iPurchaseControl = 0 // not running cutscne
				and (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION) OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE))
				
				AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF IS_ANY_VEHICLE_IMPOUND_SLOT_OCCUPIED(eCurrentPlayerPed)		
					
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vLocatePos1, vLocatePos2, fWidth)
							if GET_TOTAL_CASH(eCurrentPlayerPed) >= 250
								INT iNumOccupiedSlots
								iNumOccupiedSlots = 0
								INT i
								REPEAT MAX_IMPOUND_SLOTS i
									IF IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(i, eCurrentPlayerPed)
										iNumOccupiedSlots++
									ENDIF
								ENDREPEAT
								
								CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player can start impound retrieval")
								IF iNumOccupiedSlots > 1
									REGISTER_CONTEXT_INTENTION(iImpoundMenuContextID, CP_MEDIUM_PRIORITY, "IMPOUND_TRIG2")	// Press ~a~ to retrieve an impounded vehicle.
								ELSE
									REGISTER_CONTEXT_INTENTION(iImpoundMenuContextID, CP_MEDIUM_PRIORITY, "IMPOUND_TRIG1")	// Press ~a~ to retrieve an impounded vehicle for $250.
								ENDIF
								iImpoundMenuControl = 1
							else
								PRINT_HELP("SCLUB_NO_MONEY")
							endif
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND LOAD_MENU_ASSETS()
				//CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Waiting to start impound retrieval...")
				IF HAS_CONTEXT_BUTTON_TRIGGERED(iImpoundMenuContextID)
					RELEASE_CONTEXT_INTENTION(iImpoundMenuContextID)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					DISABLE_PLAYER_CONTROLS()
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player began impound retrieval")
					INT iNumOccupiedSlots
					iNumOccupiedSlots = 0
					INT i
					REPEAT MAX_IMPOUND_SLOTS i
						IF IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(i, eCurrentPlayerPed)
							iNumOccupiedSlots++
						ENDIF
					ENDREPEAT
					IF iNumOccupiedSlots > 1
						bMenuInitialised = FALSE
						bMenuRebuild = FALSE
						bConfirmUpdate = FALSE
						iImpoundMenuSelection = -1
						iImpoundMenuControl++
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Only 1 impound car, skipping menu!")
						INT iVeh
						REPEAT MAX_IMPOUND_SLOTS iVeh
							IF IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(iVeh, eCurrentPlayerPed)
								VECTOR vSpawnLoc
								IF iVeh = 0
									// Use the 'first' location
									vSpawnLoc = <<431.40, -997.33, 24.76>>
									// 179.24
								ELSE
									// Use the 'second' location
									vSpawnLoc = <<436.39, -997.25, 24.76>>
									// 179.24
								ENDIF
								WHILE NOT CREATE_IMPOUND_VEHICLE(vehImpoundedPlayerVehicle, iVeh, vSpawnLoc, 179.24, TRUE)
									WAIT(0)
									CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Waiting to create Impound car...")
								ENDWHILE
								IF DOES_ENTITY_EXIST(vehImpoundedPlayerVehicle)
									IF iVeh = 0
										LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_L, TRUE, FALSE)
									ELSE
										LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_R, TRUE, FALSE)
									ENDIF
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									ENDIF
									CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Impound car created")
									// Charge the player here
									DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, BAAC_POLICE_STATION_SC, 250)
									CLEAR_VEHICLE_IMPOUND_SLOT(iVeh)
									TRACK_VEHICLE_FOR_IMPOUND(vehImpoundedPlayerVehicle, GET_CURRENT_PLAYER_PED_ENUM())
									CLEANUP_MENU_ASSETS()
									iImpoundMenuControl = 3
									RELEASE_CONTEXT_INTENTION(iImpoundMenuContextID)
									iImpoundMenuContextID = NEW_CONTEXT_INTENTION
									bConfirmUpdate = FALSE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			INT n
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					n = 0
				BREAK
				CASE CHAR_FRANKLIN
					n = 1
				BREAK
				CASE CHAR_TREVOR
					n = 2
				BREAK
			ENDSWITCH
			DISABLE_PLAYER_CONTROLS()
			//CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Disabling vehicle cinematic cam")
			// Build the menu
			IF NOT bMenuInitialised OR bMenuRebuild
			
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Initialising menu")
			
				CLEAR_MENU_DATA()
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				SET_MENU_TITLE("IMPOUND_TITLE")
				
				iImpoundMenuOptions = 0
				INT iFirstMenuItem
				iFirstMenuItem = -1
				
				INT iVeh, iItem
				
				// Grab the current item.
				iItem = 0
				
				REPEAT MAX_IMPOUND_SLOTS iVeh
					IF IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(iVeh, eCurrentPlayerPed)
						ADD_MENU_ITEM_TEXT(iItem, GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_savedGlobals.sVehicleGenData.sImpoundVehicles[n][iVeh].eModel))
						IF iFirstMenuItem = -1
							iFirstMenuItem = iItem
							iImpoundMenuSelection = iItem
						ENDIF
						SET_BIT(iImpoundMenuOptions, iItem)
						iImpoundMenuSlotTrack[iItem] = iVeh // Store which impound slot this vehicle is in
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> ","BUILD MENU: Adding impounded vehicle ", iVeh, " to menu slot ", iItem)
						ADD_MENU_ITEM_TEXT(iItem, "IMPOUND_COST", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(250) //This may change to some sort of algorithmic thing based on how much the vehicle costs 
						iItem++
					ENDIF
				ENDREPEAT
			
				iItem = 0
				
				SET_CURRENT_MENU_ITEM(iImpoundMenuSelection)
				
				bRebuildHelp = TRUE
				
				bMenuRebuild = FALSE
				bMenuInitialised = TRUE
			
			ELSE
						
				///////////////////////////////////
				// Mouse menu support
				bCursorAccept = FALSE
				
				IF IS_PC_VERSION()	
					
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
						
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
											
						HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
						HANDLE_MENU_CURSOR(FALSE)
						
						IF IS_MENU_CURSOR_ACCEPT_RELEASED()
						
							IF g_iMenuCursorItem != iImpoundMenuSelection
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								iImpoundMenuSelection = g_iMenuCursorItem
								SET_CURRENT_MENU_ITEM(iImpoundMenuSelection)
								bRebuildHelp = TRUE
							ELSE
								bCursorAccept = TRUE
							ENDIF						
						
						ENDIF
					
					ENDIF
				
				ENDIF
			
			
				// Up
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					
					IF NOT bConfirmUpdate
						bRebuildHelp = TRUE
						
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player UP")
						
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						INT i
						BOOL bItemFound
						FOR i = (iImpoundMenuSelection-1) TO 0 STEP -1
							IF IS_BIT_SET(iImpoundMenuOptions, i)
								iImpoundMenuSelection = i
								bItemFound = TRUE
								i = 0 // Bail
							ENDIF
						ENDFOR
						IF NOT bItemFound
							FOR i = 31 TO (iImpoundMenuSelection+1) STEP -1
								IF IS_BIT_SET(iImpoundMenuOptions, i)
									iImpoundMenuSelection = i
									bItemFound = TRUE
									i = iImpoundMenuSelection // Bail
								ENDIF
							ENDFOR
						ENDIF
						
						IF bItemFound
							SET_CURRENT_MENU_ITEM(iImpoundMenuSelection)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player confirming choice, disable directions!")
					ENDIF
					
					
				// Down
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				
					IF NOT bConfirmUpdate
						bRebuildHelp = TRUE
						
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player DOWN")
						
						PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						INT i
						BOOL bItemFound
						FOR i = (iImpoundMenuSelection+1) TO 31
							IF IS_BIT_SET(iImpoundMenuOptions, i)
								iImpoundMenuSelection = i
								bItemFound = TRUE
								i = 31 // Bail
							ENDIF
						ENDFOR
						IF NOT bItemFound 
							FOR i = 0 TO (iImpoundMenuSelection-1)
								IF IS_BIT_SET(iImpoundMenuOptions, i)
									iImpoundMenuSelection = i
									bItemFound = TRUE
									i = iImpoundMenuSelection // Bail
								ENDIF
							ENDFOR
						ENDIF
						
						IF bItemFound
							SET_CURRENT_MENU_ITEM(iImpoundMenuSelection)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player confirming choice, disable directions!")
					ENDIF
					
				// Select
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR bCursorAccept = TRUE
				
					bCursorAccept = FALSE
				
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player ACCEPT")
					
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					// Confirm
					IF (NOT bConfirmUpdate)
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player confirming...")
						SET_CURRENT_MENU_ITEM_DESCRIPTION("IMPOUND_CNF")
						REMOVE_MENU_HELP_KEYS()
						ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "ITEM_YES")
						ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "ITEM_NO")
						bConfirmUpdate = TRUE
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player confirmed!")
						VECTOR vSpawnLoc
						IF iImpoundMenuSelection = 0
							// Use the 'first' location
							vSpawnLoc = <<431.40, -997.33, 24.76>>
							// 179.24
						ELSE
							// Use the 'second' location
							vSpawnLoc = <<436.39, -997.25, 24.76>>
							// 179.24
						ENDIF
						
						IF IS_VEHICLE_IMPOUND_SLOT_OCCUPIED(iImpoundMenuSlotTrack[iImpoundMenuSelection], eCurrentPlayerPed)
							WHILE NOT CREATE_IMPOUND_VEHICLE(vehImpoundedPlayerVehicle, iImpoundMenuSlotTrack[iImpoundMenuSelection], vSpawnLoc, 179.24, TRUE)
								WAIT(0)
								CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Waiting to create Impound car...")
							ENDWHILE
							IF DOES_ENTITY_EXIST(vehImpoundedPlayerVehicle)
								IF iImpoundMenuSelection = 0
									LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_L, TRUE, FALSE)
								ELSE
									LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_R, TRUE, FALSE)
								ENDIF
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								ENDIF
								CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Impound car created")
								// Charge the player here
								DEBIT_BANK_ACCOUNT(eCurrentPlayerPed, BAAC_POLICE_STATION_SC, 250)
								CLEAR_VEHICLE_IMPOUND_SLOT(iImpoundMenuSlotTrack[iImpoundMenuSelection])
								TRACK_VEHICLE_FOR_IMPOUND(vehImpoundedPlayerVehicle, GET_CURRENT_PLAYER_PED_ENUM())
								CLEANUP_MENU_ASSETS()
								iImpoundMenuControl++
								RELEASE_CONTEXT_INTENTION(iImpoundMenuContextID)
								iImpoundMenuContextID = NEW_CONTEXT_INTENTION
								bConfirmUpdate = FALSE
							ENDIF
						ENDIF

						bMenuRebuild = TRUE
					ENDIF
					
				// Exit/Decline
				ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player CANCEL")
					
					PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
					// Decline
					IF bConfirmUpdate
						bRebuildHelp = TRUE
						bConfirmUpdate = FALSE
					ELSE
						iImpoundMenuControl = 99
					ENDIF
				ENDIF
				
			ENDIF
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF bRebuildHelp
				SET_CURRENT_MENU_ITEM_DESCRIPTION("")
				REMOVE_MENU_HELP_KEYS()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "ITEM_EXIT")
				bConfirmUpdate = FALSE
				bRebuildHelp = FALSE
			ENDIF
			
			IF LOAD_MENU_ASSETS()
				DRAW_MENU()
			ENDIF
		BREAK
		CASE 3
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<431.40, -997.33, 24.76>>) > 20
			AND NOT IS_IMPOUND_AREA_OCCUPIED_BY_VEHICLES_OR_PEDS()
				// Player has left the immediate area and it is clear of things in the way
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player recovered vehicle, area clear, closing normally...")
				TRACK_VEHICLE_FOR_IMPOUND(vehImpoundedPlayerVehicle, GET_CURRENT_PLAYER_PED_ENUM())
				CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player vehicle being tracked for impound")
				iImpoundMenuControl = 99
				
			ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<431.40, -997.33, 24.76>>) > 100
				// Player has left the area but there are vehicles/peds in the way of the doors
				IF DOES_ENTITY_EXIST(vehImpoundedPlayerVehicle)
					IF NOT IS_ENTITY_DEAD(vehImpoundedPlayerVehicle)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehImpoundedPlayerVehicle)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehImpoundedPlayerVehicle), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 100
							IF NOT IS_ENTITY_ON_SCREEN(vehImpoundedPlayerVehicle)
								CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player recovered vehicle, didn't drive it away, area not clear!")
								SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpoundedPlayerVehicle, TRUE) //Re-impound vehicle if player didn't take it
								DELETE_VEHICLE(vehImpoundedPlayerVehicle) //Remove this instance of the vehicle
								CLEAR_AREA_OF_VEHICLES(<<431.40, -997.33, 24.76>>, 10.0, DEFAULT, DEFAULT, TRUE, TRUE) //Remove all the left vehicles around the impound
								CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Vehicle re-impounded, area cleared")
								iImpoundMenuControl = 99
							ENDIF
						ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehImpoundedPlayerVehicle)
							// If the player is in the new vehicle, assume they don't care about anything left behind
							CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player recovered vehicle, area not clear!")
							CLEAR_AREA_OF_VEHICLES(<<431.40, -997.33, 24.76>>, 10.0, DEFAULT, DEFAULT, TRUE, TRUE) //Remove all the left vehicles around the impound
							CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Area cleared")
							TRACK_VEHICLE_FOR_IMPOUND(vehImpoundedPlayerVehicle, GET_CURRENT_PLAYER_PED_ENUM())
							CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Player vehicle being tracked for impound")
							iImpoundMenuControl = 99
						ENDIF
					ELSE
						// If the new vehicle was created but is now dead, clear the area now we're far enough away
						CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Recovered vehicle dead? Clearing up area...")
						CLEAR_AREA_OF_VEHICLES(<<431.40, -997.33, 24.76>>, 10.0, DEFAULT, DEFAULT, TRUE, TRUE) //Remove all the left vehicles around the impound
						iImpoundMenuControl = 99
					ENDIF
				ELSE
					// If the new vehicle doesn't exist or wasn't created, why are we here?
					CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Recovered vehicle doesn't exist!? Clearing up area...")
					CLEAR_AREA_OF_VEHICLES(<<431.40, -997.33, 24.76>>, 10.0, DEFAULT, DEFAULT, TRUE, TRUE) //Remove all the left vehicles around the impound
					iImpoundMenuControl = 99
				ENDIF
			ENDIF
		BREAK
		CASE 99
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			RELEASE_CONTEXT_INTENTION(iImpoundMenuContextID)
			INT i
			REPEAT MAX_IMPOUND_SLOTS i
				iImpoundMenuSlotTrack[i] = -1
			ENDREPEAT
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_L, FALSE, FALSE)
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_R, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(vehImpoundedPlayerVehicle)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehImpoundedPlayerVehicle)
			ENDIF
			CPRINTLN(DEBUG_AMBIENT, "vehicle_gen_controller.sc: DO_IMPOUND_SELECT_PROCESSING -> Done!")
			iImpoundMenuControl = 0
		BREAK
	ENDSWITCH
	
	// Fix for 2039985 - Opening door if player is locked inside.
	IF iImpoundMenuControl = 0
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<428.292847,-996.683411,24.488642>>, <<439.543823,-996.711365,28.103334>>, 8.687500)
			IF NOT bImpoundDoorOverride
				PRINTLN("IMPOUND_DOOR - Unlocking impound door as player is stuck inside")
				LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_L, TRUE, FALSE)
				bImpoundDoorOverride = TRUE
			ENDIF
		ELIF bImpoundDoorOverride
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<431.271484,-1004.058960,23.981976>>, <<431.039429,-993.620972,27.618681>>, 6.812500)
			PRINTLN("IMPOUND_DOOR - Locking impound door as player is no longer stuck inside")
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_IMPOUND_L, FALSE, FALSE)
			bImpoundDoorOverride = FALSE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Cleans up any assets that may have been created
PROC CLEANUP_SCRIPT()
	INT i
	REPEAT NUMBER_OF_VEHICLES_TO_GEN i
		IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[i])
			REMOVE_BLIP(g_sVehicleGenNSData.blipID[i])
		ENDIF
		IF DOES_ENTITY_EXIST(g_sVehicleGenNSData.vehicleID[i])
			IF IS_VEHICLE_DRIVEABLE(g_sVehicleGenNSData.vehicleID[i])
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sVehicleGenNSData.vehicleID[i])
					SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sVehicleGenNSData.vehicleID[i])
					g_sVehicleGenNSData.vehicleID[i] = NULL
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_CAM_EXIST(sGarageData.camCutscene)
		SET_CAM_ACTIVE(sGarageData.camCutscene, FALSE)
		DESTROY_CAM(sGarageData.camCutscene)
	ENDIF
	IF DOES_CAM_EXIST(sGarageData.camCutscene2)
		SET_CAM_ACTIVE(sGarageData.camCutscene2, FALSE)
		DESTROY_CAM(sGarageData.camCutscene2)
	ENDIF
	IF sGarageData.iWarpControl != 0
		RESET_VEHICLE_GEN_LOADED_CHECKS()
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF DOES_BLIP_EXIST(g_sVehicleGenNSData.blipPlayerVehicle)
		REMOVE_BLIP(g_sVehicleGenNSData.blipPlayerVehicle)
	ENDIF
	
	IF (iGarageEntryContextID != NEW_CONTEXT_INTENTION)
		RELEASE_CONTEXT_INTENTION(iGarageEntryContextID)
	ENDIF
	IF iVehicleMenuContextID != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iVehicleMenuContextID)
	ENDIF
	IF sGarageData.iContextID != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(sGarageData.iContextID)
	ENDIF
	IF iImpoundMenuContextID != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(iImpoundMenuContextID)
	ENDIF
	
	// Fix for 2269369. Ensure "inGarage" flag is cleared if the veh gen controller cleans up
	// while the player is in the garage.
	IF sGarageData.iWarpControl > 0
		IF g_sVehicleGenNSData.bInGarage
			PRINTLN("Cleaning up garage interior as veh_gen_controller cleans up.")
			SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_GARAGEM_SP, TRUE)
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEM_SP, TRUE)
			g_sVehicleGenNSData.bInGarage = FALSE
			sGarageData.iWarpControl = 0	//Reset warp state
		ENDIF
	ENDIF
	
	// Fix for 2294541. Ensure that the vehicle gen loaded checks are cancelled if this script
	// cleans up early. Stops startup positioning hanging forever if we transition to MP at a 
	// bad time. -BenR
	g_sVehicleGenNSData.bCheckVehGensLoaded = FALSE
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES()
	// Fix for bugs 1666251, 1666378, and 1666444
	// If the getaway vehicle is no longer valid, replace with a standard vehicle.
	
	// CASE VEHGEN_MISSION_VEH_FBI4_PREP
	//		sData.dynamicSlotIndex = 22 
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MISSION_VEH_FBI4_PREP)
		IF g_savedGlobals.sVehicleGenData.sDynamicData[22].eModel != DUMMY_MODEL_FOR_SCRIPT
		AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[22].eModel)
		
			PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - Prep vehicle no longer valid, replacing with FUGITIVE")
		
			g_savedGlobals.sVehicleGenData.sDynamicData[22].eModel = FUGITIVE
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iFlags = 0
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iWindowTintColour = 0
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iTyreR = 255
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iTyreG = 255
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iTyreB = 255
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iColour1 = 0
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iColour2 = 0
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iColourExtra1 = 0
			g_savedGlobals.sVehicleGenData.sDynamicData[22].iColourExtra2 = 156
			
			INT i
			REPEAT MAX_VEHICLE_MOD_SLOTS i
				g_savedGlobals.sVehicleGenData.sDynamicData[22].iModIndex[i] = 0
			ENDREPEAT
			REPEAT MAX_VEHICLE_MOD_VAR_SLOTS i
				g_savedGlobals.sVehicleGenData.sDynamicData[22].iModVariation[i] = 0
			ENDREPEAT
		ENDIF
	ENDIF
	
	
	// Fix for bug 1666179
	// If the garage vehicle models are no longer valid, set a dummy model so the slot can be re-used in GET_FREE_VEHICLE_GEN_SLOT_IN_GARAGE
	
	//CASE VEHGEN_WEB_CAR_MICHAEL
	//	sData.dynamicSlotIndex = 9
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_CAR_MICHAEL)
	AND g_savedGlobals.sVehicleGenData.sDynamicData[9].eModel != DUMMY_MODEL_FOR_SCRIPT
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[9].eModel)
		g_savedGlobals.sVehicleGenData.sDynamicData[9].eModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_WEB_CAR_MICHAEL longer valid")
	ENDIF
	
	//CASE VEHGEN_WEB_CAR_FRANKLIN
	//	sData.dynamicSlotIndex = 10
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_CAR_FRANKLIN)
	AND g_savedGlobals.sVehicleGenData.sDynamicData[10].eModel != DUMMY_MODEL_FOR_SCRIPT
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[10].eModel)
		g_savedGlobals.sVehicleGenData.sDynamicData[10].eModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_WEB_CAR_FRANKLIN longer valid")
	ENDIF
	
	//CASE VEHGEN_WEB_CAR_TREVOR
	//	sData.dynamicSlotIndex = 11
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_CAR_TREVOR)
	AND g_savedGlobals.sVehicleGenData.sDynamicData[11].eModel != DUMMY_MODEL_FOR_SCRIPT
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[11].eModel)
		g_savedGlobals.sVehicleGenData.sDynamicData[11].eModel = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_WEB_CAR_TREVOR longer valid")
	ENDIF
	
	//CASE VEHGEN_MICHAEL_GARAGE_1
	//CASE VEHGEN_FRANKLIN_GARAGE_1
	//CASE VEHGEN_TREVOR_GARAGE_1
	//	iOffset = (ENUM_TO_INT(eName) - ENUM_TO_INT(VEHGEN_MICHAEL_GARAGE_1))
	//	sData.dynamicSlotIndex = 12 + iOffset
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_GARAGE_1)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[12].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_GARAGE_1, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_MICHAEL_GARAGE_1 longer valid")
	ENDIF
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_GARAGE_1)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[13].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_GARAGE_1, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_FRANKLIN_GARAGE_1 longer valid")
	ENDIF
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_GARAGE_1)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[14].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_GARAGE_1, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_TREVOR_GARAGE_1 longer valid")
	ENDIF
	
	//CASE VEHGEN_MICHAEL_GARAGE_2
	//CASE VEHGEN_FRANKLIN_GARAGE_2
	//CASE VEHGEN_TREVOR_GARAGE_2
	//	iOffset = (ENUM_TO_INT(eName) - ENUM_TO_INT(VEHGEN_MICHAEL_GARAGE_2))
	//	sData.dynamicSlotIndex = 15 + iOffset
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_GARAGE_2)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[15].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_GARAGE_2, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_MICHAEL_GARAGE_2 longer valid")
	ENDIF
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_GARAGE_2)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[16].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_GARAGE_2, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_FRANKLIN_GARAGE_2 longer valid")
	ENDIF
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_GARAGE_2)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[17].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_GARAGE_2, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_TREVOR_GARAGE_2 longer valid")
	ENDIF
	
	//CASE VEHGEN_MICHAEL_GARAGE_3
	//CASE VEHGEN_FRANKLIN_GARAGE_3
	//CASE VEHGEN_TREVOR_GARAGE_3
	//	iOffset = (ENUM_TO_INT(eName) - ENUM_TO_INT(VEHGEN_MICHAEL_GARAGE_3))
	//	sData.dynamicSlotIndex = 18 + iOffset
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_GARAGE_3)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[18].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_GARAGE_3, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_MICHAEL_GARAGE_3 longer valid")
	ENDIF
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_GARAGE_3)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[19].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_GARAGE_3, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_FRANKLIN_GARAGE_3 longer valid")
	ENDIF
	IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_GARAGE_3)
	AND NOT IS_VEHICLE_AVAILABLE_FOR_GAME(g_savedGlobals.sVehicleGenData.sDynamicData[20].eModel)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_TREVOR_GARAGE_3, FALSE)
		PRINTLN("RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES - VEHGEN_TREVOR_GARAGE_3 longer valid")
	ENDIF
	
ENDPROC


SCRIPT

	PRINTSTRING("\nStarting vehicle_gen_controller")PRINTNL()
	
	// Setup some debug widgets
	#IF IS_DEBUG_BUILD
		SETUP_VEHICLE_CONTROL_WIDGETS()
	#ENDIF
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// Proprety vehgens need to be available from start, they will get set to acquired
	// when the correct character owns the property.
	//SET_VEHICLE_GEN_AVAILABLE(VEHGEN_LOCKUP_MICHAEL_CAR_SCRAP_YARD, TRUE)
	
	// Ensure we creat the helipad blip each time the script launches
	sGarageData.iContextID = NEW_CONTEXT_INTENTION
	sGarageData.eClosestGen = VEHGEN_NONE
	sGarageData.fClosestDist = 99999.99
	
	//Purposefully "hidden" in amongst this script. We want it to be hard
	//for hackers to spot these scripts in decompiled output. -BenR
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
		Reset_Beast_Peyote_Variables(sBeastPeyoteVars)
    
        #IF IS_DEBUG_BUILD
            Create_Beast_Peyote_Widgets(sBeastPeyoteVars, wVehicleGenControllerWidget)
        #ENDIF
	#ENDIF
	#ENDIF
		
	// Clear create timers
	INT i
	REPEAT NUM_OF_PLAYABLE_PEDS i
		g_iCreatedPlayerVehicleCleanupTimer[i][0] = -1
		g_iCreatedPlayerVehicleCleanupTimer[i][1] = -1
	ENDREPEAT
	
	IF NOT g_savedGlobals.sVehicleGenData.bInitialDataSetup
		REPEAT NUMBER_OF_BUYABLE_VEHICLES_SP i
			g_savedGlobals.sVehicleGenData.sWebVehicles[0].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobals.sVehicleGenData.sWebVehicles[1].todEmailDate[i] = INVALID_TIMEOFDAY
			g_savedGlobals.sVehicleGenData.sWebVehicles[2].todEmailDate[i] = INVALID_TIMEOFDAY
		ENDREPEAT
		
		REPEAT NUMBER_OF_DYNAMIC_VEHICLE_GENS i
			g_savedGlobals.sVehicleGenData.fDynamicHeading[i] = -1
		ENDREPEAT
		
		g_savedGlobals.sVehicleGenData.eMissionVehTimeStamp = INVALID_TIMEOFDAY
		g_savedGlobals.sVehicleGenData.bInitialDataSetup = TRUE
	ENDIF
	
	// remove the blimp vehicle gens if the DLC has been removed
	IF NOT IS_PREORDER_GAME() 
    AND NOT IS_SPECIAL_EDITION_GAME() 
    AND NOT IS_COLLECTORS_EDITION_GAME()
	AND NOT IS_JAPANESE_SPECIAL_EDITION_GAME()
		CPRINTLN(DEBUG_REPEAT, "Removing blip vehicle gen as DLC has been removed.")
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BLIMP_CASINO, FALSE)
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BLIMP_DOCKS, FALSE)
	ENDIF
	
	RUN_VALID_MODEL_CHECK_FOR_STORED_VEHICLES()
	
	// Blast through all the vehicle gens to see what needs loaded on startup
	REPEAT NUMBER_OF_VEHICLES_TO_GEN i
		// If the shop is still in range add it to the new list
		ADD_VEHICLE_GEN_TO_PROCESS_LIST(INT_TO_ENUM(VEHICLE_GEN_NAME_ENUM, i))
	ENDREPEAT
	
	// Main loop
	WHILE (TRUE)
	
		// This script needs to cleanup only when the game moves from SP to MP.
		IF NOT bForceCleanupSetup
			PRINTLN("...vehicle_gen_controller.sc setting up a new force cleanup.")
			bForceCleanupSetup = TRUE
			IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
				PRINTLN("...vehicle_gen_controller.sc has been forced to cleanup.")

				//Only clean up with a REPEAT_PLAY flag if we know it is for a recording playback.
				IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_REPEAT_PLAY
					IF NOT g_bForceCleaupVehgenForRecordingPlayback
						PRINTLN("...vehicle_gen_controller.sc has found a repeat play cleanup without the recoring playback flag.")
						PRINTLN("...vehicle_gen_controller.sc skipping force cleanup.")
						bForceCleanupSetup = FALSE //Set up a new force cleanup next frame.
					ELSE
						PRINTLN("...vehicle_gen_controller.sc cleaning up for recording playback.")
						CLEANUP_CARSTEAL_WALL()
						CLEANUP_SCRIPT()
					ENDIF
					g_bForceCleaupVehgenForRecordingPlayback = FALSE
				ELSE
					PRINTLN("...vehicle_gen_controller.sc cleaning up for multiplayer or magdemo.")
					CLEANUP_CARSTEAL_WALL()
					CLEANUP_SCRIPT()
				ENDIF
			ENDIF
		ENDIF
	
		WAIT(0)
		
		IF (GET_INDEX_OF_CURRENT_LEVEL() != LEVEL_NET_TEST)
			// Maintain the debug widgets
			#IF IS_DEBUG_BUILD
				MAINTAIN_VEHICLE_CONTROL_WIDGETS()
				
				IF bOutputSpecialVehicleInfo
					PRINTLN("SPECIAL VEHICLE INFO")
					INT iSpecialVehs = GET_NUMBER_OF_SPECIAL_VEHICLES()
					INT iSpecialVeh
					PRINTLN("...Total = ", iSpecialVehs)
					REPEAT iSpecialVehs iSpecialVeh
						IF IS_THIS_MODEL_A_PLANE(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh))
							PRINTLN("...vehicle[", iSpecialVeh, "] name='", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh)), "', type='plane'")
						ELIF IS_THIS_MODEL_A_BOAT(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh))
							PRINTLN("...vehicle[", iSpecialVeh, "] name='", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh)), "', type='boat'")
						ELIF IS_THIS_MODEL_A_CAR(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh))
							PRINTLN("...vehicle[", iSpecialVeh, "] name='", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh)), "', type='car'")
						ELIF IS_THIS_MODEL_A_BIKE(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh))
							PRINTLN("...vehicle[", iSpecialVeh, "] name='", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh)), "', type='bike'")
						ELIF IS_THIS_MODEL_A_HELI(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh))
							PRINTLN("...vehicle[", iSpecialVeh, "] name='", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh)), "', type='heli'")
						ELSE
							PRINTLN("...vehicle[", iSpecialVeh, "] name='", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_SPECIAL_VEHICLE_MODEL(iSpecialVeh)), "', type='invalid'")
						ENDIF
					ENDREPEAT
					bOutputSpecialVehicleInfo = FALSE
				ENDIF
				
			#ENDIF
			
			eCurrentPlayerPed = GET_CURRENT_PLAYER_PED_ENUM()
			
			DO_VEHICLE_GEN_PROCESING()
			DO_CARSTEAL_WALL_PROCESSING()
			
			//Purposefully "hidden" in amongst this script. We want it to be hard
			//for hackers to spot these scripts in decompiled output. -BenR
			#IF FEATURE_SP_DLC_BEAST_SECRET
			#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
				Maintain_Beast_Peyote_Progression(sBeastPeyoteVars)
			#ENDIF
			#ENDIF
			
			DO_IMPOUND_SELECT_PROCESSING()
			DO_PURCHASABLE_GARAGE_PROCESSING()
			
//			#IF IS_DEBUG_BUILD
//				IF GET_COMMANDLINE_PARAM_EXISTS("sc_BlipSPVehicle")
					DO_PLAYER_VEHICLE_BLIP_MANAGEMENT()
//				ENDIF
//			#ENDIF
			
			GENERATE_VEHICLE_STATS_FOR_ALL_VEHICLE_CLASSES(sVehStatsData)
		ENDIF
		
		//Purposefully "hidden" in amongst this script. We want it to be hard
        //for hackers to spot these scripts in decompiled output. -BenR
        #IF FEATURE_SP_DLC_BEAST_SECRET
		#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
            Maintain_Play_Beast_Peyote_Sound(sBeastPeyoteVars)
            
            #IF IS_DEBUG_BUILD
                Update_Beast_Peyote_Widgets(sBeastPeyoteVars)
            #ENDIF
		#ENDIF
        #ENDIF
        
		
	ENDWHILE
	
ENDSCRIPT
