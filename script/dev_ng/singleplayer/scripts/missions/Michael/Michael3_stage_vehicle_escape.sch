USING "asset_management_public.sch"
USING "commands_itemsets.sch"
USING "Michael3_support.sch"
USING "commands_script.sch"
USING "locates_public.sch"
USING "commands_task.sch"


CONST_INT						iCONST_HELI_DITCH_DISTANCE			28900	//(170 squared)
CONST_INT						iCONST_HELI_START_DISTANCE_CHECK	10000	//(100 squared)

ENUM	TREVOR_GTFO_STATE
	TREVOR_GTFO_INIT,
	TREVOR_GTFO_WAIT_IN_CHOPPER,
	TREVOR_DONE
ENDENUM
TREVOR_GTFO_STATE	eTrevorGTFOState = TREVOR_GTFO_INIT

ENUM	VEHICLE_STAT_STATE
	VSS_INIT,
	VSS_NOT_IN_CAR,
	VSS_IN_CAR,
	VSS_DONE
ENDENUM
VEHICLE_STAT_STATE	eVehicleStatState = VSS_INIT

PROC UPDATE_VEHICLE_DAMAGE_STAT()
	VEHICLE_INDEX	currentVehicle
	SWITCH eVehicleStatState
		CASE VSS_INIT
			CPRINTLN(DEBUG_MISSION, "UPDATE_VEHICLE_DAMAGE_STAT - Going to VSS_NOT_IN_CAR")
			eVehicleStatState = VSS_NOT_IN_CAR
		BREAK
		
		CASE VSS_NOT_IN_CAR
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				currentVehicle = GET_PLAYERS_LAST_VEHICLE()
				IF IS_ENTITY_OK(currentVehicle)
					CPRINTLN(DEBUG_MISSION, "UPDATE_VEHICLE_DAMAGE_STAT - Adding car")
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(currentVehicle)
				ENDIF
				CPRINTLN(DEBUG_MISSION, "UPDATE_VEHICLE_DAMAGE_STAT - Going to VSS_IN_CAR")
				eVehicleStatState = VSS_IN_CAR
			ENDIF
		BREAK
		
		CASE VSS_IN_CAR
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
				CPRINTLN(DEBUG_MISSION, "UPDATE_VEHICLE_DAMAGE_STAT - Going to VSS_NOT_IN_CAR")
				eVehicleStatState = VSS_NOT_IN_CAR
			ENDIF
		BREAK
		
		CASE VSS_DONE
		BREAK
	ENDSWITCH
ENDPROC

ENUM	VEHICLE_ESCAPE_STATE
	VEHICLE_ESCAPE_INIT,
	VEHICLE_ESCAPE_STREAMING,
	VEHICLE_ESCAPE_DRIVE,
	VEHICLE_ESCAPE_IDLE,
	VEHICLE_ESCAPE_SLOW_VEHICLE,
	VEHICLE_ESCAPE_WAIT_FOR_PLAYER_OUT_OF_CAR,
	VEHICLE_ESCAPE_COMPLETE
ENDENUM
VEHICLE_ESCAPE_STATE	eVehicleEscapeState = VEHICLE_ESCAPE_INIT
MISSION_MESSAGE		eVehicleEscapeMessages = MISSION_MESSAGE_01

ENUM	CUTSCENE_LEADIN_STATE
	LEADIN_STATE_INIT,
	LEADIN_STATE_STREAMING,
	LEADIN_STATE_WAIT_FOR_CLOSE,
	LEADIN_STATE_CREATE_STUFF,
	LEADIN_STATE_WAIT_FOR_DISTANT,
	LEADIN_STATE_DONE
ENDENUM
CUTSCENE_LEADIN_STATE	eCutsceneLeadinState = LEADIN_STATE_INIT

ENUM	CUTSCENE_STREAMING_STATE
	STREAMING_STATE_INIT,
	STREAMING_STATE_LOAD,
	STREAMING_STATE_UNLOAD,
	STREAMING_STATE_DONE
ENDENUM
CUTSCENE_STREAMING_STATE	eCutsceneStreamingState = STREAMING_STATE_INIT

STRING strMichaelAnimDict = "missfbi3leadinout"
STRING strTrevorAnimDict = "missmic_3_ext@leadin@mic_3_ext"


BOOL				bShouldTaskTrevorGTFO
BOOL				bChopperMentioned
BOOL				bUnloadAssets = FALSE
BOOL				bUseLocates
BOOL				bUseDistanceCheck
BOOL 				bHeliCreated = FALSE
BOOL				bShouldUpdateOtherPlayer
STRING				sRendezousObj

TRIGGER_BOX			tb_Alley
structTimer			tmrVehicleEscapeConvController
VEHICLE_INDEX		vehPlayerCar

structTimer			tmrVehicleEscapedWantedDelay
CONST_FLOAT			fCONST_WANTED_DELAY_TIME			10.000


PROC UPDATE_CUTSCENE_STREAMING()
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		SWITCH eCutsceneStreamingState
			CASE STREAMING_STATE_INIT
				CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Going to STREAMING_STATE_LOAD")
				eCutsceneStreamingState = STREAMING_STATE_LOAD
			BREAK
			
			CASE STREAMING_STATE_LOAD
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_blip_alley_coord) <= DEFAULT_CUTSCENE_LOAD_DIST
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Requesting the Michael leadin")
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MIC_3_EXT", CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5
																	   | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 
																	   | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13 
																	   | CS_SECTION_14 | CS_SECTION_15 | CS_SECTION_16 | CS_SECTION_17)
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Requesting the Trevor leadin")
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MIC_3_EXT", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5
																	   | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 
																	   | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13 
																	   | CS_SECTION_14 | CS_SECTION_15 | CS_SECTION_16 | CS_SECTION_17)
					ENDIF
					CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Going to STREAMING_STATE_UNLOAD")
					eCutsceneStreamingState = STREAMING_STATE_UNLOAD
				ENDIF
			BREAK
			
			CASE STREAMING_STATE_UNLOAD
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_blip_alley_coord) >= DEFAULT_CUTSCENE_UNLOAD_DIST
					REMOVE_CUTSCENE()
					CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - We drove away, going back to STREAMING_STATE_LOAD")
					eCutsceneStreamingState = STREAMING_STATE_LOAD
				ENDIF
				
				IF HAS_CUTSCENE_LOADED()
					CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Cutscene's loaded, going to STREAMING_STATE_DONE")
					eCutsceneStreamingState = STREAMING_STATE_DONE
				ENDIF
			BREAK
			
			CASE STREAMING_STATE_DONE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_blip_alley_coord) >= DEFAULT_CUTSCENE_UNLOAD_DIST
					REMOVE_CUTSCENE()
					CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - We drove away, going back to STREAMING_STATE_LOAD")
					eCutsceneStreamingState = STREAMING_STATE_LOAD
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC UPDATE_CUTSCENE_LEAD_IN()
	
	UPDATE_CUTSCENE_STREAMING()
	
	SWITCH eCutsceneLeadinState
		CASE LEADIN_STATE_INIT
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				Load_Asset_AnimDict(sAssetData, strMichaelAnimDict)
				Load_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				Load_Asset_AnimDict(sAssetData, strTrevorAnimDict)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Going to LEADIN_STATE_STREAMING")
			eCutsceneLeadinState = LEADIN_STATE_STREAMING
		BREAK
		
		CASE LEADIN_STATE_STREAMING
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF HAS_ANIM_DICT_LOADED(strMichaelAnimDict)
				AND HAS_MODEL_LOADED(PROP_CS_CIGGY_01B)
					CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Going to LEADIN_STATE_WAIT_FOR_CLOSE")
					eCutsceneLeadinState = LEADIN_STATE_WAIT_FOR_CLOSE
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF HAS_ANIM_DICT_LOADED(strTrevorAnimDict)
					CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Going to LEADIN_STATE_WAIT_FOR_CLOSE")
					eCutsceneLeadinState = LEADIN_STATE_WAIT_FOR_CLOSE
				ENDIF
			ENDIF
		BREAK
		
		CASE LEADIN_STATE_WAIT_FOR_CLOSE
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_blip_alley_coord) <= 150.0
//			AND NOT IS_SPHERE_VISIBLE(v_blip_alley_coord, 2.0)
//				bShouldUpdateOtherPlayer = FALSE
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					DELETE_PED(peds[mpf_mike].id)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					DELETE_PED(peds[mpf_trev].id)
				ENDIF
				UnLoad_Asset_Recording(sAssetData, rec_otherp_esc, str_carrecs)
				CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Going to LEADIN_STATE_CREATE_STUFF")
				eCutsceneLeadinState = LEADIN_STATE_CREATE_STUFF
				FALLTHRU
			ENDIF
		BREAK
		
		
		CASE LEADIN_STATE_CREATE_STUFF
			CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - blah blah blah")
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - We're Trevor")
				IF Create_Player_Ped(peds[mpf_mike], SELECTOR_PED_MICHAEL, (<<-1459.46338, -380.24106, 37.78629>>), 315.38)
					IF IS_ENTITY_OK(peds[mpf_mike].id)
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Creating Michael")
						FREEZE_ENTITY_POSITION(peds[mpf_mike].id, TRUE)
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = PLAYER_PED_ID()
						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_mike].id
						
						objs[mof_mike_ciggy].id = CREATE_OBJECT(PROP_CS_CIGGY_01B, GET_PED_BONE_COORDS(MIKE_PED_ID(), BONETAG_PH_R_HAND, <<0,0,0>>))
						ATTACH_ENTITY_TO_ENTITY(objs[mof_mike_ciggy].id, MIKE_PED_ID(), GET_PED_BONE_INDEX(MIKE_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
						Unload_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_mike].id, TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(NULL, strMichaelAnimDict, "idle_a")//, (<<-1459.46338, -380.24106, 37.78629>>), <<0,0,315.38>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_USE_KINEMATIC_PHYSICS)
							TASK_PLAY_ANIM(NULL, strMichaelAnimDict, "idle_b")//, (<<-1459.46338, -380.24106, 37.78629>>), <<0,0,315.38>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_USE_KINEMATIC_PHYSICS)
							TASK_PLAY_ANIM(NULL, strMichaelAnimDict, "idle_c")//, (<<-1459.46338, -380.24106, 37.78629>>), <<0,0,315.38>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_USE_KINEMATIC_PHYSICS)
							TASK_PLAY_ANIM(NULL, strMichaelAnimDict, "idle_b")//, (<<-1459.46338, -380.24106, 37.78629>>), <<0,0,315.38>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_USE_KINEMATIC_PHYSICS)
							TASK_PLAY_ANIM(NULL, strMichaelAnimDict, "idle_a")//, (<<-1459.46338, -380.24106, 37.78629>>), <<0,0,315.38>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_USE_KINEMATIC_PHYSICS)
							TASK_PLAY_ANIM(NULL, strMichaelAnimDict, "idle_c")//, (<<-1459.46338, -380.24106, 37.78629>>), <<0,0,315.38>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_USE_KINEMATIC_PHYSICS)
							SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(MIKE_PED_ID(), seq)
						CLEAR_SEQUENCE_TASK(seq)
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Going to LEADIN_STATE_WAIT_FOR_DISTANT")
						eCutsceneLeadinState = LEADIN_STATE_WAIT_FOR_DISTANT
					ENDIF
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Creating Trevor")
				IF Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, (<<-1460.95679, -380.33527, 37.83759>>), 220.0)
					IF IS_ENTITY_OK(peds[mpf_trev].id)
						FREEZE_ENTITY_POSITION(peds[mpf_trev].id, TRUE)	
						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = PLAYER_PED_ID()
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trev].id
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_trev].id, TRUE)
						TASK_PLAY_ANIM(peds[mpf_trev].id, strTrevorAnimDict, "_leadin_trevor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						TASK_LOOK_AT_ENTITY(peds[mpf_trev].id, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_LEAD_IN - Going to LEADIN_STATE_WAIT_FOR_DISTANT")
						eCutsceneLeadinState = LEADIN_STATE_WAIT_FOR_DISTANT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE LEADIN_STATE_WAIT_FOR_DISTANT
		BREAK
		
		CASE LEADIN_STATE_DONE
		BREAK
	ENDSWITCH
ENDPROC

PROC CHANGE_HELI_WEAPONS_BASED_ON_SPEED()
	WEAPON_TYPE currentHeliWeapon
	IF IS_ENTITY_OK(PLAYER_PED_ID())
	AND IS_ENTITY_OK(peds[mpf_mw_heli_3_pilot].id)
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) <= 25.0
			IF GET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_mw_heli_3_pilot].id, currentHeliWeapon)
				IF currentHeliWeapon <> WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
					CPRINTLN(DEBUG_MISSION, "Player's driving slow, set the helicopter to use the Minigun")
					SET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_mw_heli_3_pilot].id, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)	
				ENDIF
			ENDIF
		ELIF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 25.0
			IF GET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_mw_heli_3_pilot].id, currentHeliWeapon)
				IF currentHeliWeapon <> WEAPONTYPE_VEHICLE_SPACE_ROCKET
					CPRINTLN(DEBUG_MISSION, "Player's driving fast, set the helicopter to use the Rockets")
					SET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_mw_heli_3_pilot].id, WEAPONTYPE_VEHICLE_SPACE_ROCKET)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC EVENT_heli_3(MISSION_EVENT_DATA &sData)

	FLOAT fTargetSpeed
	CHANGE_HELI_WEAPONS_BASED_ON_SPEED()
	// Kill this heli event if the heli is dead
	
	IF bHeliCreated
		IF NOT DOES_ENTITY_EXIST(vehs[mvf_mw_heli_3].id)
		OR NOT IS_VEHICLE_DRIVEABLE(vehs[mvf_mw_heli_3].id)
		OR IS_ENTITY_DEAD(vehs[mvf_mw_heli_3].id)
		OR IS_PED_INJURED(peds[mpf_mw_heli_3_pilot].id)
			IF DOES_ENTITY_EXIST(vehs[mvf_mw_heli_3].id)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehs[mvf_mw_heli_3].id)
			ENDIF
			
			STOP_AUDIO_SCENE("MI_3_ESCAPE_HELICOPTER_ATTACK")
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIC3_HELIKILL)
			INFORM_MISSION_STATS_OF_INCREMENT(MIC3_HELIKILL)
			sData.iStage = -1
			END_MISSION_EVENT(sData)
		ENDIF
	ENDIF

//	CPRINTLN(DEBUG_MISSION, "EVENT_heli_3 - sData.iStage = ", sData.iStage)

	// Track the player going through locates
	SWITCH i_heli_3_locate_tracker
		CASE 1
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2337.807861,350.530640,172.147903>>, <<-2350.545898,379.242340,178.501419>>, 24.937500)
				i_heli_3_locate_tracker++
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			 AND g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE)
				IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-2345.213867,312.380219,150.101990>>, <<-2277.425537,159.590637,185.995422>>, 90.000000)
					IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-2345.213867,312.380219,150.101990>>, <<-2277.425537,159.590637,185.995422>>, 90.000000)
						CPRINTLN(DEBUG_MISSION, "We left in a non-standard way")
						bUseDistanceCheck = FALSE
					ENDIF
					i_heli_3_locate_tracker++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2289.227539,438.354828,173.397552>>, <<-2323.085693,440.380646,178.706161>>, 27.250000)
				i_heli_3_locate_tracker++
			ENDIF
		BREAK
	ENDSWITCH

	// event flow
	SWITCH sData.iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_heli_mw)
			Load_Asset_Recording(sAssetData, rec_mw_heli_3, str_carrecs)
			
			VECTOR vMin, vMax
			vMin = <<-2239.659668,470.221832,157.555359>> - <<115.937500,108.875000,27.437500>>
			vMax = <<-2239.659668,470.221832,157.555359>> + <<115.937500,108.875000,27.437500>>
			SET_ROADS_IN_AREA(vMin, vMax, FALSE)
			
			CLEAR_AREA_OF_VEHICLES(<<-2239.659668,470.221832,157.555359>>, 115.0)
			
			bUseDistanceCheck = TRUE
			i_heli_3_locate_tracker = 1
			sData.iStage++
		BREAK
		CASE 2
			IF i_heli_3_locate_tracker > 1
			AND HAS_MODEL_LOADED(mod_heli_mw)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_mw_heli_3, str_carrecs)
				
				vehs[mvf_mw_heli_3].id = CREATE_VEHICLE(mod_heli_mw, << -2232.1025, 418.3100, 165.3925 >>, 65.8194)
				Create_Mission_Ped_In_Vehicle(peds[mpf_mw_heli_3_pilot], mod_ped_mw, vehs[mvf_mw_heli_3], VS_DRIVER, "HELI_3_DRIVER_0", REL_MW, WEAPONTYPE_UNARMED, 100)
				bHeliCreated = TRUE
				SAFE_BLIP_VEHICLE(vehs[mvf_mw_heli_3].blip, vehs[mvf_mw_heli_3].id, TRUE)
				START_AUDIO_SCENE("MI_3_ESCAPE_HELICOPTER_ATTACK")
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_mw_heli_3].id, "MI_3_HELICOPTER_01")
				SET_PED_ACCURACY(peds[mpf_mw_heli_3_pilot].id, 1)
				SET_PED_SHOOT_RATE(peds[mpf_mw_heli_3_pilot].id, 50)
				SET_PED_FIRING_PATTERN(peds[mpf_mw_heli_3_pilot].id, FIRING_PATTERN_FULL_AUTO)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_3].id, rec_mw_heli_3, str_carrecs)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_3].id, 500)
				SET_PLAYBACK_SPEED(vehs[mvf_mw_heli_3].id, 1.5)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_mw_heli_3].id)
				Unload_asset_model(sAssetData, mod_heli_mw)
				eVehicleEscapeMessages = MISSION_MESSAGE_04
				sData.iStage++
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_3].id)
			OR i_heli_3_locate_tracker > 2
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_3].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_3].id)
				ENDIF
				TASK_HELI_MISSION(peds[mpf_mw_heli_3_pilot].id, vehs[mvf_mw_heli_3].id, NULL, PLAYER_PED_ID(), (<<0,0,0>>), MISSION_ATTACK, 50.0, 30.0, -1, 5, 5)
				sData.iGenericFlag = 1000
				sData.iStage++
			ENDIF
		BREAK
		CASE 4
			IF NOT bUseDistanceCheck
				IF IS_ENTITY_OK(vehs[mvf_mw_heli_3].id)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehs[mvf_mw_heli_3].id)) <= iCONST_HELI_START_DISTANCE_CHECK
						CPRINTLN(DEBUG_MISSION, "Helicopter got close, restart the distance check")
						bUseDistanceCheck = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_blip_alley_coord) <= 450.0
			OR (bUseDistanceCheck AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehs[mvf_mw_heli_3].id)) >= iCONST_HELI_DITCH_DISTANCE)
				sData.iStage++
			ENDIF
		BREAK
		CASE 5
			//@RJP - 1427770 - losing the chopper is dependant on the player being 200 m away or killing it
			IF IS_ENTITY_OK(vehs[mvf_mw_heli_3].id)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehs[mvf_mw_heli_3].id)) >= iCONST_HELI_DITCH_DISTANCE 
				
					IF IS_ENTITY_OK(vehs[mvf_mw_heli_3].id) AND IS_ENTITY_OK(peds[mpf_mw_heli_3_pilot].id)
						SET_PED_KEEP_TASK(peds[mpf_mw_heli_3_pilot].id, TRUE)
						TASK_HELI_MISSION(peds[mpf_mw_heli_3_pilot].id, vehs[mvf_mw_heli_3].id, null, null, <<-4445.6064, -2004.2947, 157.0255>>, MISSION_GOTO, 80, 20, -1, 530, 100)
					ENDIF
				
					SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_mw_heli_3_pilot].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mw_heli_3].id)
					SAFE_REMOVE_BLIP(vehs[mvf_mw_heli_3].blip)
					END_MISSION_EVENT(sData)

				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF sData.iStage > 3
	AND IS_ENTITY_OK(vehs[mvf_mw_heli_3].id) AND IS_ENTITY_OK(peds[mpf_mw_heli_3_pilot].id)
	
		IF GET_GAME_TIMER() - sData.iGenericFlag >= 1000
		
			fTargetSpeed 	= CLAMP(GET_ENTITY_SPEED(PLAYER_PED_ID()) * 3.0, 40, 999)
			VECTOR vPlayerCoord
			vPlayerCoord = GET_ENTITY_COORDS( PLAYER_PED_ID() )
			IF IS_ENTITY_OK(vehs[mvf_mw_heli_3].id) AND IS_ENTITY_OK(peds[mpf_mw_heli_3_pilot].id)
				TASK_HELI_MISSION(peds[mpf_mw_heli_3_pilot].id, vehs[mvf_mw_heli_3].id, null, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, fTargetSpeed, 30.0, -1, ROUND( vPlayerCoord.z ) + 40, 15)
			ENDIF
			
			sData.iGenericFlag = GET_GAME_TIMER()
					
		ENDIF
	
	ENDIF
	
	IF IS_MISSION_EVENT_ENDED(sData)
		//@RJP - 1427779 - Tell player to go to destination once chopper is lost/destroyed
		SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_mw_heli_3_pilot].id)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mw_heli_3].id)
		Unload_Asset_Model(sAssetData, mod_heli_mw)
		Unload_Asset_Model(sAssetData, mod_ped_mw)
		bUseLocates = TRUE
		bChopperMentioned = FALSE
		// @SBA - ok to get a wanted level again after timer
		START_TIMER_NOW(tmrVehicleEscapedWantedDelay)
		TRIGGER_MUSIC_EVENT("MIC3_MEET")
	ENDIF
ENDPROC

PROC CREATE_VEHICLE_ESCAPE_TRIGGERS()
	tb_Alley = CREATE_TRIGGER_BOX(<<-1473.439575,-397.264618,37.053715>>, <<-1465.145264,-389.775482,40.561657>>, 8.187500)
ENDPROC


PROC UPDATE_TREVOR_GTFO_IN_HELI()
	IF IS_ENTITY_OK(TREV_PED_ID())
		SWITCH eTrevorGTFOState
			CASE TREVOR_GTFO_INIT
				IF IS_ENTITY_OK(vehs[mvf_trev_heli].id)
//					DELETE_PED(peds[mpf_trev].id)
					TASK_ENTER_VEHICLE(TREV_PED_ID(), vehs[mvf_trev_heli].id)
					CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_GTFO_IN_HELI - going to TREVOR_GTFO_WAIT_IN_CHOPPER")
					eTrevorGTFOState = TREVOR_GTFO_WAIT_IN_CHOPPER
				ENDIF
			BREAK
			
			CASE TREVOR_GTFO_WAIT_IN_CHOPPER
				IF IS_ENTITY_OK(vehs[mvf_trev_heli].id)
					IF IS_PED_IN_ANY_HELI(TREV_PED_ID())
						TASK_HELI_MISSION(TREV_PED_ID(), vehs[mvf_trev_heli].id, NULL, NULL, (<<111.96901, -1646.88354, 78.14513>>), MISSION_GOTO, 50, 5.0, -1, 100, 30)
						CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_GTFO_IN_HELI - going to TREVOR_DONE")
						eTrevorGTFOState = TREVOR_DONE
					ENDIF
				ENDIF
			BREAK
			
			CASE TREVOR_DONE
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_GTFO_IN_HELI - Trevor's not ok!?!?!??!")
	ENDIF
ENDPROC


PROC UPDATE_VEHICLE_MICHAEL_WAR_CRIES()
	
	IF IS_ENTITY_OK(PLAYER_PED_ID())
			
		//Control the guys war cries!
		IF GET_TIMER_IN_SECONDS(m_tmrWarCries) >= 10.0
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF Play_Warcry("M3_CHOPATT")
						
				RESTART_TIMER_NOW(m_tmrWarCries)
			ENDIF						
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_VEHICLE_ESCAPE_CONVERSATIONS()
	
	IF bChopperMentioned
	AND IS_ENTITY_OK(vehs[mvf_mw_heli_3].id)
	AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		UPDATE_VEHICLE_MICHAEL_WAR_CRIES()
	ENDIF
	
	IF m_bDavePlayGetInCarMessage
		m_bDavePlayGetInCarMessage = FALSE
		eVehicleEscapeMessages = MISSION_MESSAGE_10
	ENDIF
	
	SWITCH eVehicleEscapeMessages
	
		CASE MISSION_MESSAGE_01
			IF Play_Conversation("M3_ESC1")
				bUseLocates = TRUE
				eVehicleEscapeMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
	
		CASE MISSION_MESSAGE_04
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF Play_Conversation("M3_SEECHOP")
					bChopperMentioned = TRUE
					RESTART_TIMER_NOW(m_tmrWarCries)
					RESTART_TIMER_NOW(tmrVehicleEscapeConvController)
					eVehicleEscapeMessages = MISSION_MESSAGE_05
				ENDIF
			ELSE
				bChopperMentioned = TRUE
				eVehicleEscapeMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_05
			IF bUseLocates
				eVehicleEscapeMessages = MISSION_MESSAGE_IDLE
			ENDIF
			IF GET_TIMER_IN_SECONDS(tmrVehicleEscapeConvController) >= 15.0
				IF Play_Conversation("M3_TAIL")
					eVehicleEscapeMessages = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_10
			IF Play_Conversation("M3_GETINCAR")
				eVehicleEscapeMessages = MISSION_MESSAGE_11
			ENDIF
		BREAK
	
		CASE MISSION_MESSAGE_11
			IF Play_Conversation("M3_BYEDAVE")
				eVehicleEscapeMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC


PROC MISSION_STAGE_VEHICLE_ESCAPE()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		CLEANUP_AI_PED_BLIP(peds[mpf_trev].stealthBlip)
		SAFE_REMOVE_BLIP(peds[mpf_trev].blip)
		SAFE_REMOVE_BLIP(blipOtherPlayer)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		CLEANUP_AI_PED_BLIP(peds[mpf_mike].stealthBlip)
		SAFE_REMOVE_BLIP(peds[mpf_mike].blip)
		SAFE_REMOVE_BLIP(blipOtherPlayer)
	ENDIF

	IF eVehicleEscapeState > VEHICLE_ESCAPE_STREAMING
		UPDATE_VEHICLE_ESCAPE_CONVERSATIONS()
		UPDATE_DAVE_FLEEING_MUSEUM()
		UPDATE_VEHICLE_DAMAGE_STAT()
		UPDATE_CUTSCENE_LEAD_IN()
		RETASK_LAST_PARKING_LOT_PED()
		IF bShouldUpdateOtherPlayer
			UPDATE_OTHER_PLAYER_FLEEING_MUSEUM()
		ENDIF
		
		IF bShouldTaskTrevorGTFO
			UPDATE_TREVOR_GTFO_IN_HELI()
		ENDIF
	ENDIF
	
	UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_1].id, peds[mpf_pl_1_mw_1].stealthBlip, -1, NULL, TRUE)
	UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_2].id, peds[mpf_pl_1_mw_2].stealthBlip, -1, NULL, TRUE)
	UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_3].id, peds[mpf_pl_1_mw_3].stealthBlip, -1, NULL, TRUE)
	UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_4].id, peds[mpf_pl_1_mw_4].stealthBlip, -1, NULL, TRUE)
	UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_5].id, peds[mpf_pl_1_mw_5].stealthBlip, -1, NULL, TRUE)
	UPDATE_AI_PED_BLIP(peds[mpf_mw_jeep_2_1].id, peds[mpf_mw_jeep_2_1].stealthBlip, -1, NULL, TRUE)
	UPDATE_AI_PED_BLIP(peds[mpf_mw_jeep_2_2].id, peds[mpf_mw_jeep_2_2].stealthBlip, -1, NULL, TRUE)

	IF m_bDoStairFires
		BLOCK_STAIRWAY_PATH()
	ENDIF
	
	//@RJP - B*1427770 - remove other player's blip
	IF PLAYER_PED_ID() = peds[mpf_mike].id
		SAFE_REMOVE_BLIP(peds[mpf_trev].blip)
	ENDIF
	IF PLAYER_PED_ID() = peds[mpf_trev].id
		SAFE_REMOVE_BLIP(peds[mpf_mike].blip)
	ENDIF
	
	SWITCH eVehicleEscapeState
	
		CASE VEHICLE_ESCAPE_INIT
			Load_Asset_Model(sAssetData, mod_ped_mw)
			Load_Asset_Model(sAssetData, DOMINATOR)
			Load_Asset_Model(sAssetData, CARBONIZZARE)
			Load_Asset_Waypoint(sAssetData, m_strDaveWaypointRec)
			Load_Asset_Waypoint(sAssetData, m_strOtherPlayerWaypointRec)
			Load_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
			Load_Asset_Recording(sAssetData, rec_otherp_esc, str_carrecs)
			
			CREATE_VEHICLE_ESCAPE_TRIGGERS()
			REQUEST_MODEL(DOMINATOR)
			TRIGGER_MUSIC_EVENT("MIC3_ESCAPE")
			bShouldTaskTrevorGTFO = FALSE
			bChopperMentioned = FALSE
			bUseLocates = FALSE
			bCanBlipTrevor = FALSE
			bShouldUpdateOtherPlayer = TRUE
			eVehicleStatState = VSS_INIT
			eTrevorGTFOState = TREVOR_GTFO_INIT
			eCutsceneLeadinState = LEADIN_STATE_INIT
			eCutsceneStreamingState = STREAMING_STATE_INIT
			sRendezousObj = "M3_04"
			DELETE_PED(peds[mpf_mw_heli_dave_escape_pilot].id)
			SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_mw_heli_2_pilot].id)
			DELETE_VEHICLE(vehs[mvf_mw_heli_dave_escape].id)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mw_heli_2].id)
			CANCEL_TIMER(tmrVehicleEscapedWantedDelay)
			TRIGGER_MISSION_EVENT(sEvents[mef_heli_3].sData)
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - going to VEHICLE_ESCAPE_STREAMING")
						
			eVehicleEscapeState = VEHICLE_ESCAPE_STREAMING
		BREAK
		
		CASE VEHICLE_ESCAPE_STREAMING
			IF HAS_MODEL_LOADED(mod_ped_mw)
			AND HAS_MODEL_LOADED(DOMINATOR)
			AND HAS_MODEL_LOADED(CARBONIZZARE)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_dave_esc, str_carrecs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_otherp_esc, str_carrecs)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(m_strOtherPlayerWaypointRec)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(m_strDaveWaypointRec)
				IF NOT IS_AUDIO_SCENE_ACTIVE("MI_3_ESCAPE_IN_CAR")
					START_AUDIO_SCENE("MI_3_ESCAPE_IN_CAR")
				ENDIF
				SAFE_REMOVE_BLIP(peds[mpf_mike].blip)
				SAFE_REMOVE_BLIP(peds[mpf_trev].blip)
				
				IF IS_ENTITY_OK(peds[mpf_mw_heli_dave_escape_pilot].id)
					SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_mw_heli_dave_escape_pilot].id)
				ENDIF
				
				IF IS_ENTITY_OK(vehs[mvf_mw_heli_dave_escape].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mw_heli_dave_escape].id)
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				AND g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_ON_ROOF
					CPRINTLN(DEBUG_MISSION, "Trevor's on the roof, get him outta here!")
					bShouldTaskTrevorGTFO = TRUE
				ENDIF
				i_MichaelEscapeAI = i_MichaelEscapeAI
				i_TrevorEscapeAI = i_TrevorEscapeAI
				IF IS_ENTITY_OK(MIKE_PED_ID())
					ADD_PED_FOR_DIALOGUE(sConvo, 0, MIKE_PED_ID(), "MICHAEL")
				ENDIF
				
				IF IS_ENTITY_OK(TREV_PED_ID())
					ADD_PED_FOR_DIALOGUE(sConvo, 2, TREV_PED_ID(), "TREVOR")
				ENDIF
				SETUP_DAVE_ATTRIBUTES()
				DISABLE_TAXI_HAILING()
			//	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_DRIVING )
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "VEHICLE ESCAPE", TRUE)
				eVehicleEscapeMessages = MISSION_MESSAGE_01
				END_MISSION_EVENT(sEvents[mef_manage_switch].sData)
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - going to VEHICLE_ESCAPE_DRIVE")
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				eVehicleEscapeState = VEHICLE_ESCAPE_DRIVE
			ENDIF
		BREAK
		
		CASE VEHICLE_ESCAPE_DRIVE
			INT i
			
			//@RJP - 1409495 - Unload and delete assets once player gets far enough away
				IF NOT bUnloadAssets
					IF VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-2315.2520, 277.0216, 168.5901>> ) > 40000 //200^2
						Unload_Asset_Model(sAssetData, CARBONIZZARE)
						Unload_Asset_Model(sAssetData, DOMINATOR)
						Unload_Asset_Model(sAssetData, VACCA)
						Unload_Asset_Model(sAssetData, IG_DAVENORTON)
						Unload_Asset_Model(sAssetData, mod_ped_cia)
						Unload_Asset_Model(sAssetData, mod_ped_fib)
						Unload_Asset_Model(sAssetData, mod_ped_mw)
						Unload_asset_model(sAssetData, mod_trev_heli)
						Unload_asset_model(sAssetData, mod_mw_jeep)
						Unload_asset_model(sAssetData, mod_mw_van)
						Unload_asset_model(sAssetData, P_CS_NEWSPAPER_S)
						Unload_Asset_Anim_Dict(sAssetData, m_strAnimDict_IG1)
						Unload_Asset_Recording(sAssetData, rec_mw_heli_1, str_carrecs)
						Unload_Asset_Recording(sAssetData, rec_mw_heli_2, str_carrecs)
						Unload_Asset_Recording(sAssetData, rec_mw_heli_3, str_carrecs)
						Unload_Asset_Recording(sAssetData, rec_mw_jeep_2, str_carrecs)
						Unload_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
						Unload_Asset_Audio_Bank(sAssetData, str_AudioBankFIBShootout)
						
						DELETE_OBJECT(objs[mof_crashed_heli].id)
						DELETE_PED(peds[mpf_pl_1_mw_1].id)
						DELETE_PED(peds[mpf_pl_1_mw_2].id)
						DELETE_PED(peds[mpf_pl_1_mw_3].id)
						DELETE_PED(peds[mpf_pl_1_mw_4].id)
						DELETE_PED(peds[mpf_pl_1_mw_5].id)
						DELETE_PED(peds[mpf_mw_jeep_2_1].id)
						DELETE_PED(peds[mpf_mw_jeep_2_2].id)
						
						CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaCIA, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaFIB, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaMW, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psStairsMW, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psStairsFIB, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorSaveDaveFIB, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psFIBIG_6, TRUE)
						CLEANUP_PED_STRUCT(m_psMWIG_7, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psMichaelSecondWave, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psMichaelThirdWave, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psMWIG_8, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWalkwayCIA1, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWalkwayFIB2, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWalkwayMW2, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWalkwayMW3, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psStairwayMW, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWarFIB, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWarCIA, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psWarMWCourtyard, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psFinalWalkwayCIA, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW1, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW2, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW3, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW4, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW5, TRUE)
						CLEANUP_PED_STRUCT_PEDS(m_psTrevorDangerPeds, TRUE)
						
						FOR i = mpf_start_fib_lead TO mpf_pl_1_mw_5
							IF DOES_ENTITY_EXIST( peds[i].id )
								DELETE_PED( peds[i].id )
							ENDIF
						ENDFOR
						FOR i = mpf_mw_jeep_2_1 TO mpf_mw_jeep_2_2
							IF DOES_ENTITY_EXIST( peds[i].id )
								DELETE_PED( peds[i].id )
							ENDIF
						ENDFOR
						FOR i = 0 TO COUNT_OF(m_objCourtyardCover) - 1
							IF DOES_ENTITY_EXIST(m_objCourtyardCover[i])
								DELETE_OBJECT(m_objCourtyardCover[i])
							ENDIF
						ENDFOR
						FOR i = 0 TO enum_to_int(mvf_num_vehicles) - 1
							IF DOES_ENTITY_EXIST(vehs[i].id)
							AND IS_VEHICLE_DRIVEABLE(vehs[i].id)
							AND NOT (i = ENUM_TO_INT(mvf_dave_car))
							AND NOT (i = ENUM_TO_INT(mvf_other_player_car))
							AND NOT (i = ENUM_TO_INT(mvf_mw_heli_3))
								IF NOT (GET_PLAYERS_LAST_VEHICLE() = vehs[i].id)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
								ENDIF
							ENDIF
						ENDFOR
						bUnloadAssets = TRUE
					ENDIF
				ENDIF
			
			IF bUseLocates
				
				IF IS_PLAYER_AT_ANGLED_AREA_ANY_MEANS(s_locatesDataMike, v_blip_alley_coord, tb_Alley.vMin, tb_Alley.vMax, tb_Alley.flWidth, TRUE, sRendezousObj, TRUE, TRUE)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1442.794312,-381.582275,37.320213>>, <<-1470.929932,-363.552002,42.366222>>, 41.000000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1436.604248,-387.493134,34.620579>>, <<-1444.665283,-376.455261,40.701355>>, 8.750000)
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID())	= 0			
						STOP_AUDIO_SCENE("MI_3_ESCAPE_IN_CAR")
						STOP_AUDIO_SCENE("MI_3_ESCAPE_HELICOPTER_ATTACK")
						IF IS_ENTITY_OK(MIKE_PED_ID())
							IF IS_PED_USING_ACTION_MODE(MIKE_PED_ID())
								SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), FALSE)
							ENDIF
						ENDIF
						IF IS_ENTITY_OK(TREV_PED_ID())
							IF IS_PED_USING_ACTION_MODE(TREV_PED_ID())
								SET_PED_USING_ACTION_MODE(TREV_PED_ID(), FALSE)
							ENDIF
						ENDIF
						IF Play_Conversation("M3_EXT_LI")
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								vehPlayerCar = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
								CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - going to VEHICLE_ESCAPE_SLOW_VEHICLE")
								eVehicleEscapeState = VEHICLE_ESCAPE_SLOW_VEHICLE
							ELSE
								CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - going to VEHICLE_ESCAPE_COMPLETE")
								eVehicleEscapeState = VEHICLE_ESCAPE_COMPLETE
							ENDIF
						ENDIF
						
						SET_MAX_WANTED_LEVEL( 0 )
						SET_WANTED_LEVEL_MULTIPLIER( 0.0 )
						CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
						
					ENDIF
				ENDIF
				//@RJP - 1427770 - Tell player to lose chopper
				IF bChopperMentioned
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
						PRINT("M3_05", DEFAULT_GOD_TEXT_TIME, 1)
						sRendezousObj = "M3_04A"
						CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - CHOPPER ARRIVED, LOSE CHOPPER")
						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS( s_locatesDataMike )
						bUseLocates = FALSE
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE VEHICLE_ESCAPE_SLOW_VEHICLE
			IF IS_ENTITY_OK(vehPlayerCar)
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 2.0)
					CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - going to VEHICLE_ESCAPE_WAIT_FOR_PLAYER_OUT_OF_CAR")
					eVehicleEscapeState = VEHICLE_ESCAPE_WAIT_FOR_PLAYER_OUT_OF_CAR
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())	
				ENDIF
			ENDIF
		BREAK
		
		CASE VEHICLE_ESCAPE_WAIT_FOR_PLAYER_OUT_OF_CAR
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - going to VEHICLE_ESCAPE_COMPLETE")
				eVehicleEscapeState = VEHICLE_ESCAPE_COMPLETE
			ENDIF
		BREAK
		
		CASE VEHICLE_ESCAPE_COMPLETE
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF IS_ENTITY_OK(peds[mpf_trev].id)
					FREEZE_ENTITY_POSITION(peds[mpf_trev].id, FALSE)
				ENDIF
				IF IS_ENTITY_OK(peds[mpf_mike].id)
					FREEZE_ENTITY_POSITION(peds[mpf_mike].id, FALSE)
				ENDIF
				IF IS_ENTITY_OK(peds[mpf_mw_heli_3_pilot].id)
					DELETE_PED(peds[mpf_mw_heli_3_pilot].id)
				ENDIF
				IF IS_ENTITY_OK(vehs[mvf_mw_heli_3].id)
					DELETE_VEHICLE(vehs[mvf_mw_heli_3].id)
				ENDIF
				DELETE_OBJECT(objs[mof_mike_ciggy].id)
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_VEHICLE_ESCAPE - Stage complete, going to VEHICLE_ESCAPE_IDLE")
				eVehicleEscapeState = VEHICLE_ESCAPE_IDLE
				Mission_Set_Stage(STAGE_END_CUTSCENE)
			ENDIF
		BREAK
		
		CASE VEHICLE_ESCAPE_IDLE
		BREAK
	ENDSWITCH
	
			
	// @SBA reset ability to become wanted
	IF bUseLocates
		IF TIMER_DO_ONCE_WHEN_READY(tmrVehicleEscapedWantedDelay, fCONST_WANTED_DELAY_TIME)
			CPRINTLN(DEBUG_MISSION, "OK for player to get wanted level again.")
			//CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_MAX_WANTED_LEVEL(5)
		ENDIF
	ENDIF
	
ENDPROC

