//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║				Author: Alan Litobarski				Date: 16/08/2011		 	║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║																				║
//║ 			Michael 2 - Fresh Meat (Michael2.sc)							║
//║ 																			║
//║ 			Player gets kidnapped.									 		║
//║ 			Use hot-swap to narrow down the location of a ped.			 	║
//║																				║
//╚═════════════════════════════════════════════════════════════════════════════╝

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//═════════════════════════════════╡ HEADERS ╞═══════════════════════════════════

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch" 
USING "commands_camera.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_object.sch"
USING "commands_task.sch"
USING "commands_misc.sch"
USING "commands_entity.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"

USING "cellphone_public.sch"
USING "flow_public_core_override.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "locates_public.sch"
USING "chase_hint_cam.sch"
USING "LineActivation.sch"
USING "script_blips.sch"
USING "script_heist.sch"
USING "script_buttons.sch"
USING "replay_public.sch"
USING "cam_recording_public.sch"
USING "help_at_location.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "shared_hud_displays.sch"
USING "emergency_call.sch"
USING "spline_cam_edit.sch"
USING "building_control_public.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"
USING "push_in_public.sch"
USING "script_misc.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//══════════════════════════════════╡ CONST ╞════════════════════════════════════

CONST_INT iTotalRailNodes 40

CONST_INT iEnemyIntroCut 10
CONST_INT iEnemyOutside 3
CONST_INT iEnemyShootout 16
CONST_INT iEnemyBackup 12
CONST_INT iEnemySetPiece 10
CONST_INT iEnemyEscape 8
CONST_INT iEnemyChase 2
CONST_INT iTotalEnemy iEnemyIntroCut + iEnemyOutside + iEnemyShootout + iEnemyBackup + iEnemySetPiece + iEnemyEscape + iEnemyChase

//══════════════════════════════════╡ ENUMS ╞════════════════════════════════════

ENUM MissionObjective
	initMission,
	stageCutIntro,
	stageFindMichael,
	stageAbattoirShootout,
	stageSwitchToMichael,
	stageMichaelEscape,
	stageMichaelFree,
	stageTriadsChase,
	stageBackToMichaels,
	stageCutEnd,
	passMission,
	failMission
ENDENUM

MissionObjective eMissionObjective = initMission

#IF IS_DEBUG_BUILD

MissionObjective eMissionObjectiveAutoJSkip = initMission

#ENDIF

ENUM MissionFail
	failFranklinDied,
	failMichaelDied,
	failFranklinAbandon,
	failMichaelAbandon,
	failCarDestroyed,
	failOutOfAmmo,
	failGeneric
ENDENUM

MissionFail eMissionFail

//Combat Attributes
ENUM ADVANCE_STYLE
	GO_TO_COMBAT,
	GO_TO_POINT,
	AIM_TO_POINT,
	GUN_TO_POINT,
	PEEK_FROM_POINT
ENDENUM

//Audio
ENUM AUDIO_TRACK
	NO_AUDIO,
	MIC2_START,
	MIC2_FRANK_VEH,
	MIC2_FIND_A_WAY,
	MIC2_SWITCHED,
	MIC2_FIGHT_BEGINS,
	MIC2_FIGHT_CONT,
	MIC2_ABATTOIR_PROGRESS,
	MIC2_ACID_BATH_OS,
	MIC2_MULCHED,
	MIC2_SPINNING_BLADES,
	MIC2_HANGING_MICHAEL,
	MIC2_BACK_TO_FRANK,
	MIC3_FRANK_DOWN,
	MIC2_FRANK_SAVED,
	MIC2_VEHICLE_READY,
	MIC2_LOSE_TRIADS,
	MIC2_TRIADS_LOST,
	MIC2_RADIO_SETUP,
	MIC2_OVER,
	MIC2_DEAD,
	MIC2_FIND_MIKE_RT,
	MIC2_FIGHT_BEGINS_RT,
	MIC2_HANGING_RT,
	MIC2_MICHAEL_ESCAPE_RT,
	MIC2_TRIADS_CHASE_RT
ENDENUM

AUDIO_TRACK ePrepAudioTrack = NO_AUDIO
AUDIO_TRACK ePlayAudioTrack = NO_AUDIO

//════════════════════════════════╡ STRUCTURES ╞═════════════════════════════════

//SWITCH
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails

//Dialogue
structPedsForConversation sPedsForConversation

//Locates Struct
LOCATES_HEADER_DATA sLocatesData

STRUCT RAIL_NODE_STRUCT
	VECTOR vPos
	VECTOR vRot
ENDSTRUCT

RAIL_NODE_STRUCT sRailNodes[iTotalRailNodes]

STRUCT ACTOR
	PED_INDEX pedIndex
	BLIP_INDEX blipIndex
	
	INT iStage = 0
	INT iTimer
	
	//Advance Conditions
	INT iTime[3]
	PED_INDEX pedCheck[3]
	VECTOR vLocate[3]
	VECTOR vLocSize[3]
	FLOAT fDist[3]
	INT iBitsetStrict	//BOOL bStrict[3]
	
	//Advance Style
	ADVANCE_STYLE eAdvanceStyle[3]
	FLOAT fSpeed[3]
	INT iBitsetNavMesh	//BOOL bNavMesh[3]
	
	//Advance Point
	VECTOR vPoint[3]
	
	//Advance Point
	STRING sAnimDict
	STRING sAnimName
	
	#IF IS_DEBUG_BUILD
	STRING sDebugName
	#ENDIF
ENDSTRUCT

ACTOR sEnemyIntroCut[iEnemyIntroCut]
ACTOR sEnemyOutside[iEnemyOutside]
ACTOR sEnemyShootout[iEnemyShootout]
ACTOR sEnemyBackup[iEnemyBackup]
ACTOR sEnemySetPiece[iEnemySetPiece]
ACTOR sEnemyEscape[iEnemyEscape]
ACTOR sEnemyChase[iEnemyChase]

AI_BLIP_STRUCT blipStructOutside[iEnemyOutside]
AI_BLIP_STRUCT blipStructShootout[iEnemyShootout]
AI_BLIP_STRUCT blipStructBackup[iEnemyBackup]
AI_BLIP_STRUCT blipStructSetPiece[iEnemySetPiece]
AI_BLIP_STRUCT blipStructEscape[iEnemyEscape]

//════════════════════════════════╡ VARIABLES ╞══════════════════════════════════

//Rail Variables
INT iCurrentNode
INT iRailNodeToStopAt
BOOL bOkToMoveRail
BOOl bOkToHangFromHook
//BOOL bFixedRotation
VECTOR vRailCurrent

FLOAT fMoveSpeedMult = 0.225 // / 2
FLOAT fHookMass = 2000.0

//Switch Variables
BOOL bOkToSwitch

//BOOL bReattach
//INT iReattach

BOOL bRailInit

FLOAT fDistTotalFirstFinalNode
FLOAT fDistFromCurrentNode
FLOAT fDistFromFinalNode
INT iTimerNode = -1

//VECTOR vHeadingForSwappingToFranklin
//VECTOR vFranklinGameCamRot = <<0.0, 0.0, 190.5426>>
//BOOL bUseNewInterpCam

//Gun Throw Variables
//BOOL bShootingUpsideDown

FLOAT fHeadingTrack

//Set Piece Variables
ENUM SET_PIECE
	SET_PIECE_FALL,
//	SET_PIECE_KNIFE,
	SET_PIECE_RAIL,
	SET_PIECE_STEAM,
	SET_PIECE_STAIR,
	SET_PIECE_GRINDER,
	SET_PIECE_CUTTER,
	SET_PIECE_MINCER,
	SET_PIECE_CONVEYOR,
	SET_PIECE_GRAPPLE,
	SET_PIECE_COW
ENDENUM

INT iSetPiece[COUNT_OF(SET_PIECE)]

INT sceneSteam
INT sceneGrinder
INT sceneConveyor

INT sceneGrapple
VECTOR vGrapplePos = <<997.380, -2103.649, 29.450 + 0.07>>
VECTOR vGrappleRot = <<0.0, 0.0, -95.0>>
INT iGrappleStage	//Grapple set piece Sync Scene stage

//Integers
INT iCutsceneStage

INT iDialogueStage = 0
INT iDialogueLineCount[5]
INT iDialogueTimer[4]

//#IF IS_DEBUG_BUILD
//INT iDebugNode = -1 //DEBUG REMOVE THIS
//#ENDIF

//INT iHealthStore = -1

BOOL bBlood //Track if blood has been applied to Michael
enumCharacterList pedBlood //Ped char changes...

INT iBulletTimer	//For tracking the time since the player shot their weapon

INT iDamageTimer	//Time before peds can be damaged by buddy
INT iAdvanceTimer	//Time before peds advance

INT iCowShootTimer
OBJECT_INDEX objCow

INT iDoesObjectOfTypeExistAtCoords

//INT iDriveByTimer

ENUM CHASE_TIMER_INDEX
	CHASE_TIMER_ENEMY_1,
	CHASE_TIMER_ENEMY_2,
	CHASE_TIMER_CAR
ENDENUM

INT iChaseTimer[3]
VECTOR vCurrentPoint, vChasePoint
FLOAT fCurrentPoint, fChasePoint

//Stats
//INT iAmmo = -1
//WEAPON_TYPE wtCurrent

//Bools
BOOL bVideoRecording

BOOL bInitStage
BOOL bCleanupStage
BOOL bRadar

#IF IS_DEBUG_BUILD //Debug
	BOOL bAutoSkipping
#ENDIF

#IF IS_DEBUG_BUILD //Debug
	BOOL bDebugMichaelDeathSkip
	BOOL bDebugMichaelDeathDoNotFail
	
	BOOL bDebugMichaelSkipToStart
	
	BOOL bDebugMichaelSkipToCapture
	BOOL bDebugMichaelCapturePoint
	INT iCaptureNode
	VECTOR vCaptureCurrent, vRailCurrentRot
	INT iCaptureStage
	INT iCaptureTimer
	
	BOOL bDebugMichaelPausePlay
	INT iDebugMichaelDebugTime = -1
	
	BOOL bDebugMichaelDebugTime
	
	BOOL bDebugMichaelSetDebugCam
	BOOL bDebugMichaelCaptureDebugCam
	CAMERA_INDEX camDebug
	
	INT iDebugMichaelHandCam, iDebugMichaelHandCamLast
	FLOAT fDebugMichaelHandCam = 1.0
#ENDIF

BOOL bSkipped //Used for J-skipping and Mid Mission Replay

BOOL bReplaySkip

BOOL bCutsceneSkipped	//Tracks if WAS_CUTSCENE_SKIPPED() returns TRUE

BOOL bPreloaded //For Preloaded Cutscene

BOOL bPinnedAbattoir

BOOL bGrabGrinder
BOOL bGrabCutter
BOOL bGrabMincer

BOOL bFranklinToMichael

BOOL bPhoneSwitch

BOOL bHaltVehicle

BOOL bPassed	//Passed the mission

BOOL bSetUncapped

#IF IS_DEBUG_BUILD
BOOL bDebugAudio
#ENDIF

BOOL bFrankToMikeSwitchCam, bFrankToMikeSwitchCam2	//Keep whichever camera is loaded for the switch, even if the player gets out the car again

//Vectors and Floats
VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>

VECTOR vPlayerStart = <<997.8165, -2178.0439, 28.3477>>
CONST_FLOAT fPlayerStart 87.8708

VECTOR vChainOffset = <<0.0, -0.09, -0.875>>
VECTOR vChainRotation = <<-0.01, 0.0, 180.0>>
VECTOR vPadlockOffset = <<0.0, 0.075, -0.91>>
VECTOR vPadlockRotation = <<0.0, 0.0, 180.0>>

VECTOR vMichaelAttachOffset = <<-0.01, -0.05, -2.05 + 0.25>>

//VECTOR vHookPosOffset = VECTOR_ZERO

//Strings
STRING sAnimDictMic2SetPiece1 = "missmic2@goon1"
STRING sAnimDictMic2SetPiece2 = "missmic2@goon2"
STRING sAnimDictMic2Hook = "missmic2@meat_hook"
STRING sAnimDictMic2Switch = "missmic2@switch"
STRING sAnimDictMic2Smoking = "missmic2@enemy"
STRING sAnimDictMic2WashFace = "missmic2_washing_face"
STRING sAnimDictMic2FranklinBeckon = "missmic2@franklin_beckon"
STRING sAnimDictMic2LeadOut = "missmic_2_intleadout"
STRING sAnimDictMic2IG11 = "missmic2ig_11"
//STRING sAnimDictHang = "misswiphanging"
STRING sAnimDictIdle1 = "amb@world_human_hang_out_street@male_a@idle_a"	//idle_a b c d e
STRING sAnimDictIdle2 = "amb@world_human_hang_out_street@male_b@idle_a"	//idle_a b c d
STRING sAnimDictIdle3 = "amb@world_human_hang_out_street@male_c@base"	//base
STRING sAnimDictIdle4 = "amb@world_human_hang_out_street@male_c@idle_a"	//idle_a b c
STRING sAnimDictMachine = "missmic2@machine"

//Scenes
INT sceneSwitch
VECTOR sceneSwitchPos = <<994.094, -2150.380, 31.385>>	//<<994.157, -2150.380, 31.385>>
VECTOR sceneSwitchRot = <<0.0, -4.430, -6.840>>	//<<0.526, 0.0, -4.430>>

INT sceneCow
VECTOR sceneCowPos = <<964.096, -2106.750, 30.469>>
VECTOR sceneCowRot = <<0.0, 0.0, -4.0>>

INT sceneWashFace
VECTOR sceneWashFacePos = <<-803.583, 168.284, 76.285>>
VECTOR sceneWashFaceRot = <<0.0, 0.0, 110.0>>

//═════════════════════════════════╡ INDEXES ╞═══════════════════════════════════

//Ped
PED_INDEX pedClosestEnemy
PED_INDEX pedIntro
PED_INDEX pedDenise
PED_INDEX pedFriend1
PED_INDEX pedFriend2

//Vehicle
VEHICLE_INDEX vehCar
VEHICLE_INDEX vehFranklin
VEHICLE_INDEX vehEnemy[iEnemyChase]
VEHICLE_INDEX vehEnemyChase[iEnemyChase / 2]
VEHICLE_INDEX vehHachiRoku
VEHICLE_INDEX vehEscape
VEHICLE_INDEX vehEnd

//Blip
BLIP_INDEX blipMichael
BLIP_INDEX blipFranklin
BLIP_INDEX blipDestination
BLIP_INDEX blipEnemyVan[iEnemyChase / 2]
BLIP_INDEX blipGuns

//Pickup

//Camera
CAMERA_INDEX camMain
CAMERA_INDEX camAnim
CAMERA_INDEX camSwitch

//Sequence
SEQUENCE_INDEX seqMain

//Interior
INTERIOR_INSTANCE_INDEX intAbattoir
INTERIOR_INSTANCE_INDEX intMansion

//Object
OBJECT_INDEX objDoorAnim
OBJECT_INDEX objDoorReal
OBJECT_INDEX objHook
OBJECT_INDEX objChain
OBJECT_INDEX objPadlock
OBJECT_INDEX objCrate[3]
OBJECT_INDEX objGrinder
OBJECT_INDEX objCutter
OBJECT_INDEX objMincer
VECTOR vCigaretteAttachOffset = <<0.035, 0.0, 0.0>>
VECTOR vCigaretteParticleOffset = <<-0.08, 0.0, 0.0>>
PTFX_ID ptfxCigarette[4]
OBJECT_INDEX objCigarette[4]
OBJECT_INDEX objGun
//OBJECT_INDEX objBinBag

//Pickup
PICKUP_INDEX piPickups[5]

//Doors
INT iFrontDoor = HASH("DOORHASH_F_HOUSE_SC_F")
INT iEntranceDoor = ENUM_TO_INT(DOORHASH_ABBATOIR_FRONT_L)
INT iExitDoor = HASH("DOORHASH_ABATTOIR_EXIT")

//Navmesh
INT iNavBlock[5]

//Particle

//Cover
COVERPOINT_INDEX covPoint[22]

//Groups

//Weapons
WEAPON_TYPE wtLastWeapon = WEAPONTYPE_UNARMED

//Relationship Groups
REL_GROUP_HASH relGroupFriendlyFire
REL_GROUP_HASH relGroupBuddy
REL_GROUP_HASH relGroupEnemy
REL_GROUP_HASH relGroupPassive

//Sound
INT sfxRail = -1
INT sfxWoosh = -1
INT sfxCow = -1

//Push In Cam Struct
PUSH_IN_DATA pushInData		//BUG 2006022, 2005375, 2005357

//Rayfire

//Model Names

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widGroup
#ENDIF

//════════════════════════════════╡ FUNCTIONS ╞══════════════════════════════════
BOOL bPlayerControl = TRUE

PROC SAFE_SET_PLAYER_CONTROL(PLAYER_INDEX iPlayerIndex, BOOL bSetControlOn, SET_PLAYER_CONTROL_FLAGS iFlags = 0)
	SET_PLAYER_CONTROL(iPlayerIndex, bSetControlOn, iFlags)
	bPlayerControl = bSetControlOn
ENDPROC

FUNC BOOL SAFE_IS_PLAYER_CONTROL_ON()
	IF bPlayerControl
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Stage Selector
#IF IS_DEBUG_BUILD

INT iStageSkipMenu

CONST_INT MAX_SKIP_MENU_LENGTH COUNT_OF(MissionObjective) - 2 //Number of stages in mission minus Pass/Fail

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]

PROC MissionNames()
	SkipMenuStruct[0].sTxtLabel = "initMission"
	SkipMenuStruct[1].sTxtLabel = "stageCutIntro - (MIC_2_INT)"
	SkipMenuStruct[2].sTxtLabel = "stageFindMichael - (MIC_2_MCS_1)"
	SkipMenuStruct[3].sTxtLabel = "stageAbattoirShootout"
	SkipMenuStruct[4].sTxtLabel = "stageSwitchToMichael"
	SkipMenuStruct[5].sTxtLabel = "stageMichaelEscape"
	SkipMenuStruct[6].sTxtLabel = "stageMichaelFree"
	SkipMenuStruct[7].sTxtLabel = "stageTriadsChase"
	SkipMenuStruct[8].sTxtLabel = "stageBackToMichaels"
	SkipMenuStruct[9].sTxtLabel = "stageCutEnd - (mic_2_mcs_3_concat)"
ENDPROC

#ENDIF

//Text
STRING sConversationBlock = "MCH2AUD"

INT iTriggeredTextHashes[120]
INT iNumTextHashesStored = 0

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

//Models
INT iLoadedModelHashes[20]

FUNC BOOL HAS_MODEL_BEEN_LOADED(MODEL_NAMES mnModel)
	INT iHash = ENUM_TO_INT(mnModel)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedModelHashes) i
		IF iLoadedModelHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_MODEL_AS_LOADED(MODEL_NAMES mnModel, BOOL bIsLoaded)
	INT iHash = ENUM_TO_INT(mnModel)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedModelHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedModelHashes[i] = 0
				iLoadedModelHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedModelHashes[i] = iHash
				iLoadedModelHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_MODELS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedModelHashes) i
		iLoadedModelHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_CHECK(MODEL_NAMES mnModel)
	IF NOT HAS_MODEL_BEEN_LOADED(mnModel)
		REQUEST_MODEL(mnModel)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING MODEL #", ENUM_TO_INT(mnModel))	#ENDIF
		
		IF HAS_MODEL_LOADED(mnModel)
			SET_MODEL_AS_LOADED(mnModel, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("MODEL #", ENUM_TO_INT(mnModel), " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Recordings
INT iLoadedRecordingHashes[30]

FUNC BOOL HAS_RECORDING_BEEN_LOADED(INT iFileNumber, STRING sRecordingName)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		IF iLoadedRecordingHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_RECORDING_AS_LOADED(INT iFileNumber, STRING sRecordingName, BOOL bIsLoaded)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedRecordingHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedRecordingHashes[i] = 0
				iLoadedRecordingHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedRecordingHashes[i] = iHash
				iLoadedRecordingHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_RECORDINGS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		iLoadedRecordingHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_RECORDING_LOADED_CHECK(INT iFileNumber, STRING sRecordingName)
	IF NOT HAS_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
		REQUEST_VEHICLE_RECORDING(iFileNumber, sRecordingName)
		
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
			SET_RECORDING_AS_LOADED(iFileNumber, sRecordingName, TRUE)
			PRINTSTRING("RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName) PRINTSTRING(" LOADED")
			PRINTNL()
		ELSE
			PRINTSTRING("LOADING RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName)
			PRINTNL()
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Animation Dictionaries
INT iAnimDictHashes[20]

FUNC BOOL HAS_ANIM_DICT_BEEN_LOADED(STRING strAnimDict)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		IF iAnimDictHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_ANIM_DICT_AS_LOADED(STRING strAnimDict, BOOL bIsLoaded)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	BOOL bQuitLoop = FALSE
	
	WHILE i < iNumHashes AND NOT bQuitLoop
		IF bIsLoaded
			IF iAnimDictHashes[i] = 0
				iAnimDictHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iAnimDictHashes[i] = iHash
				iAnimDictHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_ANIM_DICTS()
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		iAnimDictHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_ANIM_DICT_LOADED_CHECK(STRING strAnimDict)
	IF NOT HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REQUEST_ANIM_DICT(strAnimDict)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING ANIM DICT ", strAnimDict)	#ENDIF
		
		IF HAS_ANIM_DICT_LOADED(strAnimDict)
			SET_ANIM_DICT_AS_LOADED(strAnimDict, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING ANIM DICT ", strAnimDict, " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ANIM_DICT(STRING strAnimDict)
	IF HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REMOVE_ANIM_DICT(strAnimDict)	#IF IS_DEBUG_BUILD	PRINTLN("REMOVE ANIM DICT ", strAnimDict)	#ENDIF
		SET_ANIM_DICT_AS_LOADED(strAnimDict, FALSE)
	ENDIF
ENDPROC

PROC ADVANCE_CUTSCENE()
	SETTIMERA(0)
	
	iCutsceneStage++
	
	PRINTLN("iCutsceneStage = ", iCutsceneStage)
ENDPROC

PROC ADVANCE_STAGE()
	bCleanupStage = TRUE
ENDPROC

FUNC BOOL SKIPPED_STAGE()
	IF bSkipped = TRUE
		bSkipped = FALSE
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL INIT_STAGE()
	IF bInitStage = FALSE
		SETTIMERA(0)
		
		bInitStage = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL CLEANUP_STAGE()
	IF bCleanupStage = TRUE
		SETTIMERA(0)
		
		bInitStage = FALSE
		bCleanupStage = FALSE
		iCutsceneStage = 0
		
		iDialogueStage = 0
		INT i
		REPEAT COUNT_OF(iDialogueLineCount) i
			iDialogueLineCount[i] = -1
		ENDREPEAT
		REPEAT COUNT_OF(iDialogueTimer) i
			iDialogueTimer[i] = 0
		ENDREPEAT
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SAFE_CLEAR_HELP(BOOL bClearNow = TRUE)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(bClearNow)
	ENDIF
ENDPROC

PROC CLEAR_TEXT()
	CLEAR_PRINTS()
	SAFE_CLEAR_HELP(TRUE)
	CLEAR_ALL_FLOATING_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
ENDPROC

PROC PRINT_ADV(STRING sPrint, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
		PRINT_NOW(sPrint, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
	ENDIF
ENDPROC

PROC PRINT_HELP_ADV(STRING sPrint, BOOL bOnce = TRUE, BOOL bForever = FALSE, INT iOverrideTime = -1)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
		IF NOT bForever
			PRINT_HELP(sPrint, iOverrideTime)
		ELSE
			PRINT_HELP_FOREVER(sPrint)
		ENDIF
		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
	ENDIF
ENDPROC

PROC PRINT_HELP_ADV_POS(STRING sPrint, VECTOR vLocation, ENTITY_INDEX entIndex = NULL, BOOL bOnce = TRUE, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = 7500)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
		IF NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
		AND vLocation.Z != 0.0
		AND entIndex = NULL
			HELP_AT_LOCATION(sPrint, vLocation, eArrow, iDuration)
		ELIF NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
		AND vLocation.Z = 0.0
		AND entIndex = NULL
			HELP_AT_SCREEN_LOCATION(sPrint, vLocation.X, vLocation.Y, eArrow, iDuration)
		ELIF entIndex != NULL
		AND ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
			HELP_AT_ENTITY(sPrint, entIndex, eArrow, iDuration)
		ELIF entIndex != NULL
		AND NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
			HELP_AT_ENTITY_OFFSET(sPrint, entIndex, vLocation, eArrow, iDuration)
		ENDIF
		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
	ENDIF
ENDPROC

FUNC PED_INDEX PLAYER_PED(enumCharacterList CHAR_TYPE)
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TYPE
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TYPE)]
	ENDIF
ENDFUNC

FUNC PED_INDEX NOT_PLAYER_PED_ID()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_MICHAEL)]
	ELSE //GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_FRANKLIN)]
	ENDIF
ENDFUNC

PROC SET_PED_POSITION(PED_INDEX pedIndex, VECTOR vCoords, FLOAT fHeading, BOOL bKeepVehicle = TRUE)
	IF bKeepVehicle = TRUE
		SET_PED_COORDS_KEEP_VEHICLE(pedIndex, vCoords)
	ELSE
		SET_ENTITY_COORDS(pedIndex, vCoords)
	ENDIF
	
	SET_ENTITY_HEADING(pedIndex, fHeading)
ENDPROC

PROC SET_VEHICLE_POSITION(VEHICLE_INDEX vehicleIndex, VECTOR vCoords, FLOAT fHeading)
	SET_ENTITY_COORDS(vehicleIndex, vCoords)
	SET_ENTITY_HEADING(vehicleIndex, fHeading)
ENDPROC

PROC SPAWN_PLAYER(PED_INDEX &pedIndex, enumCharacterList eChar, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = FALSE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedIndex, eChar, vStart, fStart)
			WAIT(0)
		ENDWHILE
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_PED(PED_INDEX &pedIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		pedIndex = CREATE_PED(PEDTYPE_MISSION, eModel, vStart, fStart)
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_VEHICLE(VEHICLE_INDEX &vehIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, INT iColour = -1, FLOAT fDirt = 0.0)
	IF NOT DOES_ENTITY_EXIST(vehIndex)
		vehIndex = CREATE_VEHICLE(eModel, vStart, fStart)
		IF iColour >= 0
			SET_VEHICLE_COLOURS(vehIndex, iColour, iColour)
		ENDIF
		SET_VEHICLE_DIRT_LEVEL(vehIndex, fDirt)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex, TRUE)
	ENDIF
ENDPROC

FUNC BOOL SAFE_IS_PED_DEAD(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_IS_VEHICLE_DEAD(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_ENTITY_DEAD(pedIndex)
		OR IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_VEHICLE(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
		OR NOT IS_VEHICLE_DRIVEABLE(vehIndex)
		OR IS_VEHICLE_PERMANENTLY_STUCK(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SAFE_ADD_BLIP_LOCATION(BLIP_INDEX &blipIndex, VECTOR vCoords, BOOL bRoute = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		blipIndex = CREATE_BLIP_FOR_COORD(vCoords, bRoute)
		SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGHEST)
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(pedIndex)
			IF NOT IS_PED_INJURED(pedIndex)
				blipIndex = CREATE_BLIP_FOR_PED(pedIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_VEHICLE(BLIP_INDEX &blipIndex, VEHICLE_INDEX &vehIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(vehIndex)
			IF NOT IS_ENTITY_DEAD(vehIndex)
				blipIndex = CREATE_BLIP_FOR_VEHICLE(vehIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
		REMOVE_BLIP(blipIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		DELETE_PED(pedIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_VEHICLE(VEHICLE_INDEX &vehicleIndex)
	IF DOES_ENTITY_EXIST(vehicleIndex)
		DELETE_VEHICLE(vehicleIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_OBJECT(OBJECT_INDEX &objectIndex)
	IF DOES_ENTITY_EXIST(objectIndex)
		DELETE_OBJECT(objectIndex)
	ENDIF
ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &vehIndex, FLOAT fTime)
	IF IS_VEHICLE_DRIVEABLE(vehIndex)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehIndex)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehIndex, fTime - GET_TIME_POSITION_IN_RECORDING(vehIndex))
		ENDIF
	ENDIF
ENDPROC

STRUCT BLIP_TIMER
	BLIP_INDEX blipIndex
	INT iBlipTimer
ENDSTRUCT

BLIP_TIMER sBlipTimer[10]

PROC SET_BLIP_FLASH_DURATION(BLIP_INDEX blipIndex, INT iDuration = 7500)
	INT i
	
	BOOL bSlotCheck
	BOOL bFreeSlot
	
	REPEAT 10 i
		IF sBlipTimer[i].blipIndex = blipIndex
			bSlotCheck = TRUE
		ENDIF
	ENDREPEAT
	
	IF bSlotCheck = FALSE
		REPEAT 10 i
			IF bFreeSlot = FALSE
				IF sBlipTimer[i].blipIndex = NULL
					sBlipTimer[i].blipIndex = blipIndex
					sBlipTimer[i].iBlipTimer = GET_GAME_TIMER() + iDuration
					
					SET_BLIP_FLASHES(sBlipTimer[i].blipIndex, TRUE)
					
					bFreeSlot = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bFreeSlot = FALSE
		SCRIPT_ASSERT("No free blip timer slots! Consider expanding array.")
	ENDIF
ENDPROC

PROC UPDATE_BLIP_FLASH_TIMERS()
	INT i
	
	REPEAT 10 i
		IF sBlipTimer[i].blipIndex != NULL
			IF GET_GAME_TIMER() > sBlipTimer[i].iblipTimer
				SET_BLIP_FLASHES(sBlipTimer[i].blipIndex, FALSE)
				
				sBlipTimer[i].blipIndex = NULL
				sBlipTimer[i].iblipTimer = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SAFE_FREEZE_ENTITY_POSITION(ENTITY_INDEX EntityIndex, BOOL FrozenByScriptFlag)
	IF NOT IS_ENTITY_ATTACHED(EntityIndex)
		IF (IS_ENTITY_A_PED(EntityIndex)
		AND NOT IS_PED_IN_ANY_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(EntityIndex)))
		OR NOT IS_ENTITY_A_PED(EntityIndex)
			FREEZE_ENTITY_POSITION(EntityIndex, FrozenByScriptFlag)
		ENDIF
	ENDIF
ENDPROC

FUNC STRUCT_ENTITY_ID CONSTRUCT_ENTITY_ID()
	STRUCT_ENTITY_ID structEntityID
	RETURN structEntityID
ENDFUNC

FUNC FLOAT RETURN_GROUND_Z_FOR_3D_COORD(VECTOR vCoord)
	GET_GROUND_Z_FOR_3D_COORD(vCoord, vCoord.Z)
	RETURN vCoord.Z
ENDFUNC

FUNC FLOAT GET_INTERP_POINT_FLOAT(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

//This function will return the value of a number after it has been raised to a power
FUNC FLOAT TPO(FLOAT fNumber, INT iPower)
	INT iLoopControl				//This will act as a counter to control how long the loop runs
	FLOAT fInitialValue = fNumber	//This will be used in the loop to multiply the new value
								//by its initial value
	FLOAT fResult = 0			//This is the value that the function returns
	
	//Use an if statement to determine how to handle the power function
	//This loop will determine how many times to multiply the number by itself
	IF iPower = 0
		fResult = 1 //Note n^0 = 1
	ELIF iPower = 1
		fResult = fNumber
	ELIF iPower < 0
		iLoopControl = -iPower + 1
		WHILE iLoopControl > 0
			fNumber *= fInitialValue
			iLoopControl--
		ENDWHILE
		fResult = 1 / fNumber
	ELIF iPower > 1
		iLoopControl = iPower - 1
		WHILE iLoopControl > 0
			fNumber *= fInitialValue
			iLoopControl--
		ENDWHILE
		fResult = fNumber
	ENDIF
	
	RETURN fResult
ENDFUNC

FUNC VECTOR HERMITE_CURVE(VECTOR vStartPoint, VECTOR vStartTangent, VECTOR vEndPoint, VECTOR vEndTangent, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	FLOAT fScale, H1, H2, H3, H4
	VECTOR vPoint
	
	fScale = fPointTime / (fEndTime - fStartTime)
	H1 = 2 * TPO(fScale, 3) - 3 * TPO(fScale, 2) + 1
	H2 = -2 * TPO(fScale, 3) + 3 * TPO(fScale, 2)
	H3 = TPO(fScale, 3) - 2 * TPO(fScale, 2) + fScale
	H4 = TPO(fScale, 3) - TPO(fScale, 2)
	vPoint = H1 * vStartPoint + H2 * vEndPoint + H3 * vStartTangent + H4 * vEndTangent
	RETURN vPoint
ENDFUNC

///Function for returning the position on the Bezier curve defined by the values passed at time "fTime". 
FUNC VECTOR GET_POSITION_ON_CURVE(VECTOR vStartPos, VECTOR vInterPos1, VECTOR vInterPos2, VECTOR vEndPos, FLOAT fTime, BOOL bIsCubic = TRUE)
	IF bIsCubic
		RETURN (((1-fTime) * (1-fTime) * (1-fTime) * vStartPos)
				+ (3 * (1 - fTime) * (1 - fTime) * fTime * vInterPos1)
				+ (3 * (1 - fTime) * fTime * fTime * vInterPos2)
				+ (fTime * fTime * fTime * vEndPos))
	ENDIF
	
	RETURN ((((1-fTime)* (1-fTime)) * vStartPos)
			+ (2 * (1-fTime) * fTime * vInterPos1)
			+ (fTime * fTime * vEndPos))
ENDFUNC

FUNC FLOAT HEADING_BETWEEN_COORDS(VECTOR vCoord1, VECTOR vCoord2)
	VECTOR vDirection = vCoord2 - vCoord1
	RETURN ATAN2(vDirection.Y, vDirection.X)
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING CONCATENATE_STRINGS(STRING sString1, STRING sString2, STRING sString3 = NULL, STRING sString4 = NULL, STRING sString5 = NULL, STRING sString6 = NULL, STRING sString7 = NULL, STRING sString8 = NULL)
	TEXT_LABEL txtStrings = sString1
	txtStrings += sString2
	IF NOT IS_STRING_NULL_OR_EMPTY(sString3)
		txtStrings += sString3
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sString4)
		txtStrings += sString4
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sString5)
		txtStrings += sString5
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sString6)
		txtStrings += sString6
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sString7)
		txtStrings += sString7
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sString8)
		txtStrings += sString8
	ENDIF
	RETURN GET_STRING_FROM_STRING(txtStrings, 0, GET_LENGTH_OF_LITERAL_STRING(txtStrings))
ENDFUNC
#ENDIF

FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x, V2.y-V1.y)
ENDFUNC

FUNC FLOAT GET_HEADING_FROM_VECTOR(VECTOR vVector)
	RETURN vVector.Z
ENDFUNC

//MIC2_START				=	2/2
//MIC2_FRANK_VEH			=
//MIC2_FIND_A_WAY			=
//MIC2_SWITCHED				=
//MIC2_FIGHT_BEGINS			=	2/2
//MIC2_FIGHT_CONT			=
//MIC2_ABATTOIR_PROGRESS	=
//MIC2_ACID_BATH_OS			=	2/2
//MIC2_MULCHED				=	2/2
//MIC2_SPINNING_BLADES		=	2/2
//MIC2_HANGING_MICHAEL		=
//MIC2_BACK_TO_FRANK		=
//MIC3_FRANK_DOWN			=	2/2
//MIC2_FRANK_SAVED			=	2/2
//MIC2_VEHICLE_READY		=
//MIC2_LOSE_TRIADS			=	2/2
//MIC2_TRIADS_LOST			=
//MIC2_OVER					=
//MIC2_DEAD					=
//MIC2_FIND_MIKE_RT			=
//MIC2_FIGHT_BEGINS_RT		=
//MIC2_HANGING_RT			=
//MIC2_MICHAEL_ESCAPE_RT	=
//MIC2_TRIADS_CHASE_RT		=

STRING sAudioEvent
INT iAudioPrepareTimer	//This is an override in case an audio event is prepared but never used

PROC AUDIO_CONTROLLER()	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("<<<<<<<< AUDIO_CONTROLLER >>>>>>>> (sAudioEvent = ", sAudioEvent, ")")	ENDIF	#ENDIF
	IF ePlayAudioTrack = NO_AUDIO
	AND (NOT IS_MUSIC_ONESHOT_PLAYING()
	OR GET_GAME_TIMER() > iAudioPrepareTimer)
		SWITCH ePrepAudioTrack
			CASE MIC2_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_START")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_START")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_START")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_FIGHT_BEGINS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_FIGHT_BEGINS")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_FIGHT_BEGINS")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_FIGHT_BEGINS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_FIGHT_BEGINS")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_FIGHT_BEGINS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_ACID_BATH_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_ACID_BATH_OS")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_ACID_BATH_OS")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_ACID_BATH_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_ACID_BATH_OS")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_ACID_BATH_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_MULCHED	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_MULCHED")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_MULCHED")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_MULCHED"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_MULCHED")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_MULCHED")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_SPINNING_BLADES	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_SPINNING_BLADES")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_SPINNING_BLADES")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_SPINNING_BLADES"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_SPINNING_BLADES")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_SPINNING_BLADES")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC3_FRANK_DOWN	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC3_FRANK_DOWN")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC3_FRANK_DOWN")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC3_FRANK_DOWN"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC3_FRANK_DOWN")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC3_FRANK_DOWN")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_FRANK_SAVED	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_FRANK_SAVED")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_FRANK_SAVED")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_FRANK_SAVED"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_FRANK_SAVED")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_FRANK_SAVED")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_LOSE_TRIADS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_LOSE_TRIADS")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_LOSE_TRIADS")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_LOSE_TRIADS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_LOSE_TRIADS")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_LOSE_TRIADS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE MIC2_RADIO_SETUP	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - MIC2_RADIO_SETUP")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_RADIO_SETUP")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "MIC2_RADIO_SETUP"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("MIC2_RADIO_SETUP")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - MIC2_RADIO_SETUP")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH ePlayAudioTrack
		CASE MIC2_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_START")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_START")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_START")
					IF TRIGGER_MUSIC_EVENT("MIC2_START")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_FRANK_VEH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FRANK_VEH")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_FRANK_VEH")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FRANK_VEH")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_FIND_A_WAY	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FIND_A_WAY")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_FIND_A_WAY")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FIND_A_WAY")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_SWITCHED	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_SWITCHED")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_SWITCHED")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_SWITCHED")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_FIGHT_BEGINS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FIGHT_BEGINS")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_FIGHT_BEGINS")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_FIGHT_BEGINS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_FIGHT_BEGINS")
					IF TRIGGER_MUSIC_EVENT("MIC2_FIGHT_BEGINS")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FIGHT_BEGINS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_FIGHT_CONT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FIGHT_CONT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_FIGHT_CONT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FIGHT_CONT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_ABATTOIR_PROGRESS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_ABATTOIR_PROGRESS")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_ABATTOIR_PROGRESS")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_ABATTOIR_PROGRESS")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_ACID_BATH_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_ACID_BATH_OS")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_ACID_BATH_OS")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_ACID_BATH_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_ACID_BATH_OS")
					IF TRIGGER_MUSIC_EVENT("MIC2_ACID_BATH_OS")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_ACID_BATH_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_MULCHED	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_MULCHED")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_MULCHED")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_MULCHED"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_MULCHED")
					IF TRIGGER_MUSIC_EVENT("MIC2_MULCHED")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_MULCHED")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_SPINNING_BLADES	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_SPINNING_BLADES")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_SPINNING_BLADES")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_SPINNING_BLADES"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_SPINNING_BLADES")
					IF TRIGGER_MUSIC_EVENT("MIC2_SPINNING_BLADES")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_SPINNING_BLADES")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_HANGING_MICHAEL	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_HANGING_MICHAEL")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_HANGING_MICHAEL")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_HANGING_MICHAEL")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_BACK_TO_FRANK	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_BACK_TO_FRANK")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_BACK_TO_FRANK")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_BACK_TO_FRANK")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC3_FRANK_DOWN	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC3_FRANK_DOWN")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC3_FRANK_DOWN")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC3_FRANK_DOWN"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC3_FRANK_DOWN")
					IF TRIGGER_MUSIC_EVENT("MIC3_FRANK_DOWN")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC3_FRANK_DOWN")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_FRANK_SAVED	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FRANK_SAVED")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_FRANK_SAVED")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_FRANK_SAVED"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_FRANK_SAVED")
					IF TRIGGER_MUSIC_EVENT("MIC2_FRANK_SAVED")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FRANK_SAVED")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_VEHICLE_READY	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_VEHICLE_READY")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_VEHICLE_READY")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_VEHICLE_READY")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_LOSE_TRIADS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_LOSE_TRIADS")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_LOSE_TRIADS")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_LOSE_TRIADS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_LOSE_TRIADS")
					IF TRIGGER_MUSIC_EVENT("MIC2_LOSE_TRIADS")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_LOSE_TRIADS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_TRIADS_LOST	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_TRIADS_LOST")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_TRIADS_LOST")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_TRIADS_LOST")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_RADIO_SETUP	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_RADIO_SETUP")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "MIC2_RADIO_SETUP")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "MIC2_RADIO_SETUP"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("MIC2_RADIO_SETUP")
					IF TRIGGER_MUSIC_EVENT("MIC2_RADIO_SETUP")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_RADIO_SETUP")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MIC2_OVER	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_OVER")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_OVER")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_OVER")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_DEAD	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_DEAD")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_DEAD")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_DEAD")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_FIND_MIKE_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FIND_MIKE_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_FIND_MIKE_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FIND_MIKE_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_FIGHT_BEGINS_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_FIGHT_BEGINS_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_FIGHT_BEGINS_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_FIGHT_BEGINS_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_HANGING_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_HANGING_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_HANGING_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_HANGING_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_MICHAEL_ESCAPE_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_MICHAEL_ESCAPE_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_MICHAEL_ESCAPE_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_MICHAEL_ESCAPE_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE MIC2_TRIADS_CHASE_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - MIC2_TRIADS_CHASE_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("MIC2_TRIADS_CHASE_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - MIC2_TRIADS_CHASE_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC LOAD_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePrepAudioTrack = eSetAudioTrack	
	
	AUDIO_CONTROLLER()
ENDPROC

PROC PLAY_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePlayAudioTrack = eSetAudioTrack
	
	AUDIO_CONTROLLER()
ENDPROC

STRUCT SPLINE_NODE
	VECTOR vPos
	VECTOR vRot
	FLOAT fFov
ENDSTRUCT

//SPLINE_NODE splineNodes[18]
//
//PROC initSplineNodeList() //FOV 50.0
//	//Clockwise
//	splineNodes[0].vPos = <<1012.896301, -2101.372314, 33.291309>>
//	splineNodes[0].vRot = <<-1.982723, 0.338979, 173.891754>>
//	splineNodes[0].fFov = 50.0
//	splineNodes[1].vPos = <<1007.293213, -2167.156982, 33.054180>>
//	splineNodes[1].vRot = <<-1.182525, 0.338979, 174.261124>>
//	splineNodes[1].fFov = 50.0
//	splineNodes[2].vPos = <<989.095398, -2197.869385, 33.599380>>
//	splineNodes[2].vRot = <<-2.301748, 0.338979, 88.595459>>
//	splineNodes[2].fFov = 50.0
//	splineNodes[3].vPos = <<955.204102, -2196.909912, 32.745079>>
//	splineNodes[3].vRot = <<-8.845912, 0.338979, -1.773986>>
//	splineNodes[3].fFov = 50.0
//	splineNodes[4].vPos = <<938.759583, -2185.577393, 32.985966>>
//	splineNodes[4].vRot = <<-8.738976, 0.338979, -96.202721>>
//	splineNodes[4].fFov = 50.0
//	splineNodes[5].vPos = <<958.455505, -2184.489014, 31.564182>>
//	splineNodes[5].vRot = <<-6.233361, 0.338979, -94.898773>>
//	splineNodes[5].fFov = 50.0
//	
//	//Counter-Clockwise
//	splineNodes[6].vPos = <<983.426758, -2090.378418, 33.904240>>
//	splineNodes[6].vRot = <<-0.707793, 0.0, 89.679756>>
//	splineNodes[6].fFov = 50.0
//	splineNodes[7].vPos = <<933.889465,-2107.938232,32.834606>>
//	splineNodes[7].vRot = <<-2.559668,0.000000,175.719452>>
//	splineNodes[7].fFov = 50.0
//	splineNodes[8].vPos = <<930.852417, -2135.413330, 32.779202>>
//	splineNodes[8].vRot = <<-0.668986, -6.738220, 173.527206>>
//	splineNodes[8].fFov = 50.0
//	splineNodes[9].vPos = <<929.129028, -2172.105713, 33.454044>>
//	splineNodes[9].vRot = <<-3.121807, -21.816061, -133.945679>>
//	splineNodes[9].fFov = 50.0
//	splineNodes[10].vPos = <<952.159302, -2183.835449, 31.973093>>
//	splineNodes[10].vRot = <<-3.935187, -5.522160, -102.999886>>
//	splineNodes[10].fFov = 50.0
//	splineNodes[11].vPos = <<974.950012, -2186.682373, 31.249016>>
//	splineNodes[11].vRot = <<-2.949606, -1.684473, -96.856033>>
//	splineNodes[11].fFov = 50.0
//	
//	//Inside
//	splineNodes[12].vPos = <<991.835388, -2187.673828, 31.325569>> + <<2.5, 5.0, 0.0>>
//	splineNodes[12].vRot = <<0.243132, -25.443731, -54.801926>>
//	splineNodes[12].fFov = 50.0
//	splineNodes[13].vPos = <<995.288513, -2173.272461, 31.397337>>
//	splineNodes[13].vRot = <<0.243132, -25.443737, 47.891655>>
//	splineNodes[13].fFov = 50.0
//	splineNodes[14].vPos = <<983.897522, -2168.887207, 31.449553>>
//	splineNodes[14].vRot = <<0.243131, 0.0, 82.929100>>
//	splineNodes[14].fFov = 50.0
//	splineNodes[15].vPos = <<969.269470, -2163.052002, 31.514753>>
//	splineNodes[15].vRot = <<1.166207, 30.385775, -12.229650>>
//	splineNodes[15].fFov = 50.0
//	splineNodes[16].vPos = <<982.599854, -2159.198730, 31.864603>>
//	splineNodes[16].vRot = <<1.659278, 0.0, -94.700310>>
//	splineNodes[16].fFov = 50.0
//	splineNodes[17].vPos = <<995.072327, -2156.664307, 32.219673>>
//	splineNodes[17].vRot = <<0.836721, -26.218670, -6.102451>>
//	splineNodes[17].fFov = 50.0
//ENDPROC
	
FUNC BOOL HAS_HOOK_REACHED_RAIL_NODE(INT iNode)
	RETURN (iCurrentNode >= iNode)
ENDFUNC
	
PROC SET_RAIL_FINAL_NODE(INT iFinalNode)
	//Occasionally want to stop the rail moving at a point before the end of the node list. Will stop at node iFinalNode.
	iRailNodeToStopAt = iFinalNode + 1
	
	//Get total distance from first node to final node
	INT i
	
	fDistTotalFirstFinalNode = 0
	
	REPEAT iRailNodeToStopAt i
		fDistTotalFirstFinalNode += GET_DISTANCE_BETWEEN_COORDS(sRailNodes[CLAMP_INT(i, 0, COUNT_OF(sRailNodes) - 1)].vPos, sRailNodes[CLAMP_INT(i + 1, 0, COUNT_OF(sRailNodes) - 1)].vPos)
	ENDREPEAT
ENDPROC

///PURPOSE: The hook "moves" along the rail by interplolating betwee a series of nodes. This defines the position & rotation of all the nodes
PROC initRailNodeList()
	sRailNodes[0].vPos = <<997.99719, -2178.23584, 33.21508>>
	sRailNodes[0].vRot = <<0.0, 0.0, -90.0>>
	sRailNodes[1].vPos = <<998.68988, -2170.42310, 33.21508>>
	sRailNodes[1].vRot = <<0.0, 0.0, -90.0>>
	sRailNodes[2].vPos = <<998.58350, -2170.03711, 33.21508>>
	sRailNodes[2].vRot = <<0.0, 0.0, -55.8000>>
	sRailNodes[3].vPos = <<998.14276, -2169.83252, 33.21508>>
	sRailNodes[3].vRot = <<0.0, 0.0, -20.0>>
	sRailNodes[4].vPos = <<992.80927, -2169.33667, 33.21372>>
	sRailNodes[4].vRot = <<0.0, 0.0, 0.0>>
	sRailNodes[5].vPos = <<988.82288, -2169.05298, 33.04533>>
	sRailNodes[5].vRot = <<0.0, 0.0, 0.0>>
	sRailNodes[6].vPos = <<981.45752, -2168.24048, 33.04327>>
	sRailNodes[6].vRot = <<0.0, 0.0, 0.0>>
	sRailNodes[7].vPos = <<969.84045, -2167.29272, 33.00440>>
	sRailNodes[7].vRot = <<0.0, 0.0, -17.0000>>
	sRailNodes[8].vPos = <<969.53687, -2167.20068, 33.00438>>
	sRailNodes[8].vRot = <<0.0, 0.0, -20.0>>
	sRailNodes[9].vPos = <<969.34314, -2167.00415, 33.00327>>
	sRailNodes[9].vRot = <<0.0, 0.0, -40.8500>>
	sRailNodes[10].vPos = <<969.27594, -2166.70215, 32.97229>>
	sRailNodes[10].vRot = <<0.0, 0.0, -84.0000>>
	sRailNodes[11].vPos = <<969.87482, -2160.25610, 32.97181>>
	sRailNodes[11].vRot = <<0.0, 0.0, -93.0000>>
	sRailNodes[12].vPos = <<970.00366, -2159.66211, 32.96849>>
	sRailNodes[12].vRot = <<0.0, 0.0, -109.6500>>
	sRailNodes[13].vPos = <<970.22723, -2159.42822, 32.96897>>
	sRailNodes[13].vRot = <<0.0, 0.0, -129.0000>>
	sRailNodes[14].vPos = <<970.54535, -2159.38843, 32.96876>>
	sRailNodes[14].vRot = <<0.0, 0.0, -173.0000>>
	sRailNodes[15].vPos = <<979.39026, -2160.19849, 33.00122>>
	sRailNodes[15].vRot = <<0.0, 0.0, -186.0000>>
	sRailNodes[16].vPos = <<979.91333, -2160.17480, 33.00301>>
	sRailNodes[16].vRot = <<0.0, 0.0, -175.0000>>
	sRailNodes[17].vPos = <<980.32690, -2160.36475, 33.00453>>
	sRailNodes[17].vRot = <<0.0, 0.0, 164.5500>>
	sRailNodes[18].vPos = <<981.12488, -2160.94116, 33.04178>>
	sRailNodes[18].vRot = <<0.0, 0.0, 174.0000>>
	sRailNodes[19].vPos = <<985.11310, -2161.31104, 33.06876>>
	sRailNodes[19].vRot = <<0.0, 0.0, -185.0000>>
	sRailNodes[20].vPos = <<985.51215, -2161.29443, 33.06952>>
	sRailNodes[20].vRot = <<0.0, 0.0, -178.3000>>
	sRailNodes[21].vPos = <<985.83417, -2161.06323, 33.07181>>
	sRailNodes[21].vRot = <<0.0, 0.0, -124.7500>>
	sRailNodes[22].vPos = <<985.97974, -2160.69263, 33.07404>>
	sRailNodes[22].vRot = <<0.0, 0.0, -102.4500>>
	sRailNodes[23].vPos = <<986.08618, -2159.59692, 33.07473>>
	sRailNodes[23].vRot = <<0.0, 0.0, -95.5000>>
	sRailNodes[24].vPos = <<986.22394, -2159.27075, 33.07572>>
	sRailNodes[24].vRot = <<0.0, 0.0, -110.8500>>
	sRailNodes[25].vPos = <<986.44983, -2159.14307, 33.07625>>
	sRailNodes[25].vRot = <<0.0, 0.0, -145.8500>>
	sRailNodes[26].vPos = <<986.96997, -2159.15112, 33.07859>>
	sRailNodes[26].vRot = <<0.0, 0.0, -177.2999>>
	sRailNodes[27].vPos = <<992.41510, -2159.66333, 33.06979>>
	sRailNodes[27].vRot = <<0.0, 0.0, -178.7499>>
	sRailNodes[28].vPos = <<992.87366, -2159.68018, 33.07030>>
	sRailNodes[28].vRot = <<0.0, 0.0, -175.3499>>
	sRailNodes[29].vPos = <<993.19458, -2159.43481, 33.07386>>
	sRailNodes[29].vRot = <<0.0, 0.0, -132.3499>>
	sRailNodes[30].vPos = <<993.33282, -2159.03345, 33.07018>>
	sRailNodes[30].vRot = <<0.0, 0.0, 83.4>>
	sRailNodes[31].vPos = <<994.17920, -2150.31909, 33.06801>>
	sRailNodes[31].vRot = <<0.0, 0.0, 83.4>>
	sRailNodes[32].vPos = <<995.29425, -2150.44263, 33.01155>>
	sRailNodes[32].vRot = <<0.0, 0.0, 83.4>>
	sRailNodes[33].vPos = <<996.92010, -2150.59131, 33.00539>>
	sRailNodes[33].vRot = <<0.0, 0.0, -77.9499>>
	sRailNodes[34].vPos = <<997.23938, -2150.55811, 33.00547>>
	sRailNodes[34].vRot = <<0.0, 0.0, -83.3499>>
	sRailNodes[35].vPos = <<997.46094, -2150.40381, 33.00445>>
	sRailNodes[35].vRot = <<0.0, 0.0, -102.3499>>
	sRailNodes[36].vPos = <<997.57794, -2150.12500, 32.97338>>
	sRailNodes[36].vRot = <<0.0, 0.0, -115.4499>>
	sRailNodes[37].vPos = <<998.53046, -2139.93213, 32.97335>>
	sRailNodes[37].vRot = <<0.0, 0.0, -100.0999>>
	sRailNodes[38].vPos = <<998.78827, -2137.11938, 32.97280>>
	sRailNodes[38].vRot = <<0.0, 0.0, -97.5499>>
	sRailNodes[39].vPos = <<998.78827, -2137.11938, 32.97280>>
	sRailNodes[39].vRot = <<0.0, 0.0, -97.5499>>
	
	INT i
	
	REPEAT COUNT_OF(sRailNodes) i
		sRailNodes[i].vRot.X = 0.0
	ENDREPEAT
	
	SET_RAIL_FINAL_NODE(40)
ENDPROC

//Cleanup
PROC MISSION_CLEANUP(BOOL bRestart = FALSE, BOOL bDeathArrest = FALSE)
	IF bVideoRecording
		REPLAY_STOP_EVENT()
		
		bVideoRecording = FALSE
	ENDIF
	
	INT i
	
	RESET_PUSH_IN(pushInData)
	
	//Stats
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
	
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
	
	//Doors
	IF bDeathArrest
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iEntranceDoor)
			IF DOOR_SYSTEM_GET_DOOR_STATE(iEntranceDoor) != DOORSTATE_LOCKED
				DOOR_SYSTEM_SET_DOOR_STATE(iEntranceDoor, DOORSTATE_LOCKED, FALSE, TRUE)
			ENDIF
		ENDIF
	ELSE
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iEntranceDoor)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(iEntranceDoor) <> 0.0
			OR DOOR_SYSTEM_GET_DOOR_STATE(iEntranceDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(iEntranceDoor, 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iEntranceDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
	ENDIF
	//Reset player
	SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF bPassed
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
							DETACH_ENTITY(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ELSE
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
			
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			
			//SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
	AND NOT IS_PED_INJURED(PLAYER_PED(CHAR_MICHAEL))
		SET_ENABLE_BOUND_ANKLES(PLAYER_PED(CHAR_MICHAEL), FALSE)
		
		RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED(CHAR_MICHAEL), FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_FRANKLIN))
	AND NOT IS_PED_INJURED(PLAYER_PED(CHAR_FRANKLIN))
	AND PLAYER_PED(CHAR_FRANKLIN) != PLAYER_PED_ID()
		SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED(CHAR_FRANKLIN), FALSE)
	ENDIF
	
	CLEAR_FOCUS()
	
	REMOVE_NAVMESH_REQUIRED_REGIONS()
	
	//Delete all peds, vehicles, objects and blips
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	REPEAT COUNT_OF(covPoint) i
		REMOVE_COVER_POINT(covPoint[i])
	ENDREPEAT
	
	REPEAT COUNT_OF(piPickups) i
		IF DOES_PICKUP_EXIST(piPickups[i])
			REMOVE_PICKUP(piPickups[i])
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD	IF bAutoSkipping	#ENDIF	//Z-skips cleanup
	SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	
	SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	
	SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	
	SAFE_DELETE_PED(pedIntro)
	#IF IS_DEBUG_BUILD	ENDIF	#ENDIF
	
	//Actors
	REPEAT iEnemyIntroCut i
		IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
			#IF IS_DEBUG_BUILD	IF bAutoSkipping	#ENDIF	//Z-skips cleanup
			SAFE_DELETE_PED(sEnemyIntroCut[i].pedIndex)
			#IF IS_DEBUG_BUILD	ENDIF	#ENDIF
		ENDIF
		SAFE_REMOVE_BLIP(sEnemyIntroCut[i].blipIndex)
	ENDREPEAT
	
	REPEAT iEnemyOutside i
		IF DOES_ENTITY_EXIST(sEnemyOutside[i].pedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyOutside[i].pedIndex)
		ENDIF
		SAFE_REMOVE_BLIP(sEnemyOutside[i].blipIndex)
	ENDREPEAT
	
	REPEAT iEnemyShootout i
		IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyShootout[i].pedIndex)
		ENDIF
		SAFE_REMOVE_BLIP(sEnemyShootout[i].blipIndex)
	ENDREPEAT
	
	REPEAT iEnemyBackup i
		IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyBackup[i].pedIndex)
		ENDIF
		SAFE_REMOVE_BLIP(sEnemyBackup[i].blipIndex)
	ENDREPEAT
	
	REPEAT iEnemySetPiece i
		IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(sEnemySetPiece[i].pedIndex)
		ENDIF
		SAFE_REMOVE_BLIP(sEnemySetPiece[i].blipIndex)
	ENDREPEAT
	
	REPEAT iEnemyEscape i
		IF DOES_ENTITY_EXIST(sEnemyEscape[i].pedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyEscape[i].pedIndex)
		ENDIF
		SAFE_REMOVE_BLIP(sEnemyEscape[i].blipIndex)
	ENDREPEAT
	
	REPEAT iEnemyChase i
		IF DOES_ENTITY_EXIST(sEnemyChase[i].pedIndex)
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyChase[i].pedIndex)
		ENDIF
		SAFE_REMOVE_BLIP(sEnemyChase[i].blipIndex)
	ENDREPEAT
	
	//Vehicle
	IF DOES_ENTITY_EXIST(vehFranklin)
	AND NOT IS_ENTITY_DEAD(vehFranklin)
		IF IS_ENTITY_A_MISSION_ENTITY(vehFranklin)
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehFranklin)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehFranklin)
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(vehEnemyChase) i
		IF DOES_ENTITY_EXIST(vehEnemyChase[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEnemyChase[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(vehEnemy) i
		IF DOES_ENTITY_EXIST(vehEnemy[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEnemy[i])
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD	IF bAutoSkipping	#ENDIF	//Z-skips cleanup
	SAFE_DELETE_VEHICLE(vehCar)
	
	IF DOES_ENTITY_EXIST(vehHachiRoku)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehHachiRoku)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehEscape)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEscape)
	ENDIF
	#IF IS_DEBUG_BUILD	ENDIF	#ENDIF
	
	#IF IS_DEBUG_BUILD	IF bAutoSkipping	#ENDIF	//Z-skips cleanup
	//Object
	SAFE_DELETE_OBJECT(objHook)
	SAFE_DELETE_OBJECT(objChain)
	SAFE_DELETE_OBJECT(objPadlock)
	
	REPEAT COUNT_OF(objCrate) i
		SAFE_DELETE_OBJECT(objCrate[i])
	ENDREPEAT
	
	REPEAT COUNT_OF(objCigarette) i
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[i])
			STOP_PARTICLE_FX_LOOPED(ptfxCigarette[i])
		ENDIF
		SAFE_DELETE_OBJECT(objCigarette[i])
	ENDREPEAT
	
	SAFE_DELETE_OBJECT(objGun)
//	SAFE_DELETE_OBJECT(objBinBag)
	
	#IF IS_DEBUG_BUILD	ENDIF	#ENDIF
	
	//Doors
//	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
//		REMOVE_DOOR_FROM_SYSTEM(iFrontDoor)
//	ENDIF
//	
//	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
//		REMOVE_DOOR_FROM_SYSTEM(iExitDoor)
//	ENDIF
	
	//Blip
	SAFE_REMOVE_BLIP(blipMichael)
	SAFE_REMOVE_BLIP(blipDestination)
	SAFE_REMOVE_BLIP(blipFranklin)
	REPEAT COUNT_OF(blipEnemyVan) i
		SAFE_REMOVE_BLIP(blipEnemyVan[i])
	ENDREPEAT
	SAFE_REMOVE_BLIP(blipGuns)
	
	//Particles
	
	//Camera
	STOP_GAMEPLAY_HINT()
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
//	REPEAT COUNT_OF(splineNodes) i
//		IF DOES_CAM_EXIST(camSplineNode[i])
//			DESTROY_CAM(camSplineNode[i])
//		ENDIF
//	ENDREPEAT
	
	//Dialogue
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 3)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 8)
	
	//Reset Variables
	bPassed = FALSE	//Reset the bool
	
	iCurrentNode = 0
	SET_RAIL_FINAL_NODE(0)
	iTimerNode = -1
	bOkToMoveRail = FALSE
	bOkToHangFromHook = FALSE
	vRailCurrent = VECTOR_ZERO
	bRailInit = FALSE
	
	fMoveSpeedMult = 0.225 // / 2
	fHookMass = 2000.0

	bOkToSwitch = FALSE
	
	bFrankToMikeSwitchCam = FALSE
	bFrankToMikeSwitchCam2 = FALSE
	
//	bReattach = FALSE
//	iReattach = 0
	
//	vHeadingForSwappingToFranklin = VECTOR_ZERO
//	vFranklinGameCamRot = VECTOR_ZERO
//	bUseNewInterpCam = FALSE
	
	//bShootingUpsideDown = FALSE
	
	REPEAT COUNT_OF(SET_PIECE) i
		iSetPiece[i] = 0
	ENDREPEAT
	
	bBlood = FALSE
	
	intAbattoir = NULL
	bPinnedAbattoir = FALSE
	
	IF intMansion <> NULL
		UNPIN_INTERIOR(intMansion)
		
		intMansion = NULL
	ENDIF
	
//	vHookPosOffset = VECTOR_ZERO
	
//	initSplineNodeList()
	
	initRailNodeList()
	
	REPEAT COUNT_OF(sRailNodes) i
		sRailNodes[i].vRot.X = 0.0
	ENDREPEAT
	
	CLEAR_TRIGGERED_LABELS()
	CLEAR_LOADED_MODELS()
	CLEAR_LOADED_RECORDINGS()
	CLEAR_LOADED_ANIM_DICTS()
	
	SETTIMERA(0)
	SETTIMERB(0)
	
	iGrappleStage = 0
	
	iCutsceneStage = 0
	
	iDialogueStage = 0
	REPEAT COUNT_OF(iDialogueLineCount) i
		iDialogueLineCount[i] = -1
	ENDREPEAT
	REPEAT COUNT_OF(iDialogueTimer) i
		iDialogueTimer[i] = 0
	ENDREPEAT
	
	bInitStage = FALSE
	
	bReplaySkip = FALSE
	
	bCutsceneSkipped = FALSE
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	//Radio
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
	//Sound
	IF sfxRail != -1
		STOP_SOUND(sfxRail)
		RELEASE_SOUND_ID(sfxRail)
		sfxRail = -1
	ENDIF
	
	IF sfxWoosh != -1
		STOP_SOUND(sfxWoosh)
		RELEASE_SOUND_ID(sfxWoosh)
		sfxWoosh = -1
	ENDIF
	
	IF sfxCow != -1
		STOP_SOUND(sfxCow)
		RELEASE_SOUND_ID(sfxCow)
		sfxCow = -1
	ENDIF
	
	//Audio Scene
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_MICHAEL_ON_MEATHOOK")
		STOP_AUDIO_SCENE("MI_2_MICHAEL_ON_MEATHOOK")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
		STOP_AUDIO_SCENE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_FIND_A_WAY_IN")
		STOP_AUDIO_SCENE("MI_2_FIND_A_WAY_IN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SPLINE_CAM_1")
		STOP_AUDIO_SCENE("MI_2_SPLINE_CAM_1")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_MAIN")
		STOP_AUDIO_SCENE("MI_2_SHOOTOUT_MAIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_ACID")
		STOP_AUDIO_SCENE("MI_2_SHOOTOUT_ACID")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_1")
		STOP_AUDIO_SCENE("MI_2_GRINDER_1")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_THE_GUARD")
		STOP_AUDIO_SCENE("MI_2_SHOOT_THE_GUARD")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_MEAT_SLICER")
		STOP_AUDIO_SCENE("MI_2_MEAT_SLICER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_THROW_THE_GUN")
		STOP_AUDIO_SCENE("MI_2_THROW_THE_GUN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_FROM_MEATHOOK")
		STOP_AUDIO_SCENE("MI_2_SHOOT_FROM_MEATHOOK")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_FRANKLIN_KNIFE_ATTACK")
		STOP_AUDIO_SCENE("MI_2_FRANKLIN_KNIFE_ATTACK")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SPLINE_2")
		STOP_AUDIO_SCENE("MI_2_SPLINE_2")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_SAVE_FRANKLIN")
		STOP_AUDIO_SCENE("MI_2_SAVE_FRANKLIN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_SHOOTOUT")
		STOP_AUDIO_SCENE("MI_2_ESCAPE_SHOOTOUT")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_2")
		STOP_AUDIO_SCENE("MI_2_GRINDER_2")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_IN_CAR")
		STOP_AUDIO_SCENE("MI_2_ESCAPE_IN_CAR")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_HOME")
		STOP_AUDIO_SCENE("MI_2_DRIVE_HOME")
	ENDIF
	
	//Audio
	PLAY_AUDIO(MIC2_DEAD)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	//Navmesh Blocking
	REPEAT COUNT_OF(iNavBlock) i
		IF iNavBlock[i] <> 0
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlock[i])
			iNavBlock[i] = 0
		ENDIF
	ENDREPEAT
	
	//Waypoints
	
	//Sequence
	CLEAR_SEQUENCE_TASK(seqMain)
	
	//Assisted
	
	//Relationship Groups
	REMOVE_RELATIONSHIP_GROUP(relGroupFriendlyFire)
	REMOVE_RELATIONSHIP_GROUP(relGroupBuddy)
	REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	REMOVE_RELATIONSHIP_GROUP(relGroupPassive)
	
	//Trackify
	ENABLE_SECOND_SCREEN_TRACKIFY_APP(FALSE)
	
	//Interface
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	//Roads
	SET_ROADS_BACK_TO_ORIGINAL(<<-877.67, 224.67, 72.30>>, <<-853.08, 125.01, 56.63>>)
	
	//Budget
	SET_PED_POPULATION_BUDGET(3)
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
	
	//New Load Scene
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	//Shops
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	
	//Changing clothes
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	
	//Taxi
	DISABLE_TAXI_HAILING(FALSE)
	
	//Cheat
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, FALSE)
	
	//Wanted
	SET_MAX_WANTED_LEVEL(6)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	//Emergency Calls
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	//Ambulances etc.
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_GANGS, TRUE)
	
	IF NOT CAN_CREATE_RANDOM_COPS()
		SET_CREATE_RANDOM_COPS(TRUE)
	ENDIF
	
	//Cap Interior
	IF bRestart = FALSE
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_ABATTOIR, TRUE)
	ELSE
		SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, TRUE)
	ENDIF
	
	IF bRestart = FALSE
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//Mission Passed
PROC missionPassed()
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
	
	CLEAR_TEXT()
	
	Mission_Flow_Mission_Passed()
	
	bPassed = TRUE
	
	MISSION_CLEANUP()
ENDPROC

/// PURPOSE:
///    Sets Michael as unavailable.
///    Called in normal fail and force cleanup
PROC SET_MICHAEL_UNAVAILABLE_ON_FAIL()
	SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
ENDPROC


//Mission Failed
PROC missionFailed()
	enumCharacterList eCharFail = GET_CURRENT_PLAYER_PED_ENUM()
	STORE_FAIL_WEAPON(PLAYER_PED_ID(), ENUM_TO_INT(eCharFail))
	
	CLEAR_TEXT()
	
	//Audio
	PLAY_AUDIO(MIC2_DEAD)
	
	SWITCH eMissionFail
		CASE failFranklinDied
			MISSION_FLOW_SET_FAIL_REASON("MCH2_FFDED")
		BREAK
		CASE failMichaelDied
			MISSION_FLOW_SET_FAIL_REASON("MCH2_FMDED")
		BREAK
		CASE failFranklinAbandon
			MISSION_FLOW_SET_FAIL_REASON("CMN_FLEFT")
		BREAK
		CASE failMichaelAbandon
			MISSION_FLOW_SET_FAIL_REASON("CMN_MLEFT")
		BREAK
		CASE failCarDestroyed
			MISSION_FLOW_SET_FAIL_REASON("MCH2_FCAR")
		BREAK
		CASE failOutOfAmmo
			MISSION_FLOW_SET_FAIL_REASON("MCH2_FAMMO")
		BREAK
		CASE failGeneric
			//Shouldn't get here
		BREAK
	ENDSWITCH
	
	Mission_Flow_Mission_Failed()
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off).
		
		//Keep disabling first person camera when fading out if on the hook as Michael
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) = PERFORMING_TASK
					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()	PRINTLN("DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 2")
				ENDIF
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//1988571 - Need to deal with keeping the player out of the factory when rejecting a replay (and keeping Michael inside).
	IF NOT HAS_PLAYER_ACCEPTED_REPLAY()
		//Lock the exit to the factory.
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
			DOOR_SYSTEM_SET_OPEN_RATIO(iExitDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iExitDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
	
		//If the player is Michael (and isn't still tied up) then warp them inside the factory, otherwise it's possible for Franklin to meet him when rejecting a retry.
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<933.0538, -2162.6216, 29.5012>>) < 200.0
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<985.9138, -2126.9465, 29.4755>>)
		ENDIF
	ENDIF
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<967.567566, -2195.217529, 25.539518>>, <<972.950378, -2098.676270, 49.888527>>, 70.0)
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<933.0538, -2162.6216, 29.5012>>, 96.4993)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<929.5760, -2162.3455, 29.3527>>, 175.2960)
	ENDIF
	
	//Michael Unavailable
	SET_MICHAEL_UNAVAILABLE_ON_FAIL()
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
ENDPROC

PROC DEATH_CHECKS()
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			eMissionFail = failFranklinDied
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			eMissionFail = failMichaelDied
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF DOES_ENTITY_EXIST(objHook)
		AND DOES_ENTITY_EXIST(objChain)
		AND DOES_ENTITY_EXIST(objPadlock)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], objHook)
				IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE), 3.0)
				OR GET_NUMBER_OF_FIRES_IN_RANGE(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE), 3.0) > 0
				OR IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				OR IS_ENTITY_ON_FIRE(objHook)
				OR IS_ENTITY_ON_FIRE(objChain)
				OR IS_ENTITY_ON_FIRE(objPadlock)
					APPLY_DAMAGE_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 1000, TRUE)
					SET_PED_TO_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 10000, 10000, TASK_RELAX, FALSE, FALSE)
					
					//Pin the ankle to the bottom of the hook
					VECTOR vRightFootAttach
					vRightFootAttach = <<-0.075, -0.1, -0.85>>
					ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], objHook, GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_R_FOOT), -1, vRightFootAttach, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
					ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], objHook, GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_L_FOOT), -1, <<-vRightFootAttach.X, vRightFootAttach.Y, vRightFootAttach.Z>>, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		SAFE_REMOVE_BLIP(blipMichael)
		
		eMissionFail = failMichaelDied
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		SAFE_REMOVE_BLIP(blipFranklin)
		
		eMissionFail = failFranklinDied
		
		missionFailed()
	ENDIF
	
//	IF eMissionObjective = stageTriadsChase
//		IF SAFE_DEATH_CHECK_VEHICLE(vehEscape)
//		OR SAFE_DEATH_CHECK_VEHICLE(vehHachiRoku)
//			eMissionFail = failCarDestroyed
//			
//			missionFailed()
//		ENDIF
//	ENDIF
	
	//Actors
//	BOOL bInAbattoir
//	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<974.844238, -2100.828613, 28.905632>>, <<966.149780, -2201.403564, 45.697361>>, 60.0)
//		bInAbattoir = TRUE
//	ENDIF
	
	IF eMissionObjective < stageMichaelEscape
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF bPinnedAbattoir = FALSE
					SAFE_FREEZE_ENTITY_POSITION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				ELSE
					SAFE_FREEZE_ENTITY_POSITION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Closest Ped
	PED_INDEX pedClosestEnemyTemp = pedClosestEnemy //We'll store this to check for a closer ped to speak. Saves extra script elsewhere.
	
	INT i
	
	REPEAT iEnemyIntroCut i
		IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
			IF NOT IS_ENTITY_DEAD(sEnemyIntroCut[i].pedIndex)
				IF bPinnedAbattoir = FALSE
					FREEZE_ENTITY_POSITION(sEnemyIntroCut[i].pedIndex, TRUE)
				ELSE
					FREEZE_ENTITY_POSITION(sEnemyIntroCut[i].pedIndex, FALSE)
				ENDIF
			ENDIF
			
//			IF NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
//				IF NOT DOES_ENTITY_EXIST(pedClosestEnemy)
//				OR (DOES_ENTITY_EXIST(pedClosestEnemy)
//				AND IS_PED_INJURED(pedClosestEnemy))
//				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy, FALSE))
//					pedClosestEnemy = sEnemyIntroCut[i].pedIndex
//				ENDIF
//			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iEnemyOutside i
		IF DOES_ENTITY_EXIST(sEnemyOutside[i].pedIndex)
//			IF IS_ENTITY_DEAD(sEnemyOutside[i].pedIndex)
//			OR eMissionObjective >= stageTriadsChase
//				SAFE_REMOVE_BLIP(sEnemyOutside[i].blipIndex)
//			ELSE
//				IF NOT DOES_BLIP_EXIST(sEnemyOutside[i].blipIndex)
//					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyOutside[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 15.0
//					AND IS_ENTITY_ON_SCREEN(sEnemyOutside[i].pedIndex)
//						SAFE_ADD_BLIP_PED(sEnemyOutside[i].blipIndex, sEnemyOutside[i].pedIndex, TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
			
			UPDATE_AI_PED_BLIP(sEnemyOutside[i].pedIndex, blipStructOutside[i])
			
//			IF NOT IS_ENTITY_DEAD(sEnemyOutside[i].pedIndex)
//				IF bPinnedAbattoir = FALSE
//					SAFE_FREEZE_ENTITY_POSITION(sEnemyOutside[i].pedIndex, TRUE)
//				ELSE
//					SAFE_FREEZE_ENTITY_POSITION(sEnemyOutside[i].pedIndex, FALSE)
//				ENDIF
//			ENDIF
			
//			IF NOT IS_PED_INJURED(sEnemyOutside[i].pedIndex)
//				IF NOT DOES_ENTITY_EXIST(pedClosestEnemy)
//				OR (DOES_ENTITY_EXIST(pedClosestEnemy)
//				AND IS_PED_INJURED(pedClosestEnemy))
//				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyOutside[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy, FALSE))
//					pedClosestEnemy = sEnemyOutside[i].pedIndex
//				ENDIF
//			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iEnemyShootout i
		IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
//			IF IS_ENTITY_DEAD(sEnemyShootout[i].pedIndex)
//			OR eMissionObjective >= stageTriadsChase	//OR bInAbattoir = FALSE
//				SAFE_REMOVE_BLIP(sEnemyShootout[i].blipIndex)
//			ELSE
//				SAFE_ADD_BLIP_PED(sEnemyShootout[i].blipIndex, sEnemyShootout[i].pedIndex, TRUE)
//			ENDIF
			
			UPDATE_AI_PED_BLIP(sEnemyShootout[i].pedIndex, blipStructShootout[i])
			
			IF NOT IS_ENTITY_DEAD(sEnemyShootout[i].pedIndex)
				IF bPinnedAbattoir = FALSE
					FREEZE_ENTITY_POSITION(sEnemyShootout[i].pedIndex, TRUE)
				ELSE
					FREEZE_ENTITY_POSITION(sEnemyShootout[i].pedIndex, FALSE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
				IF NOT DOES_ENTITY_EXIST(pedClosestEnemy)
				OR (DOES_ENTITY_EXIST(pedClosestEnemy)
				AND IS_PED_INJURED(pedClosestEnemy))
				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyShootout[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy, FALSE))
					pedClosestEnemy = sEnemyShootout[i].pedIndex
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iEnemyBackup i
		IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
//			IF IS_ENTITY_DEAD(sEnemyBackup[i].pedIndex)
//			OR eMissionObjective >= stageTriadsChase	//OR bInAbattoir = FALSE
//				SAFE_REMOVE_BLIP(sEnemyBackup[i].blipIndex)
//			ELSE
//				SAFE_ADD_BLIP_PED(sEnemyBackup[i].blipIndex, sEnemyBackup[i].pedIndex, TRUE)
//			ENDIF
			
			UPDATE_AI_PED_BLIP(sEnemyBackup[i].pedIndex, blipStructBackup[i])
			
			IF NOT IS_ENTITY_DEAD(sEnemyBackup[i].pedIndex)
				IF bPinnedAbattoir = FALSE
					FREEZE_ENTITY_POSITION(sEnemyBackup[i].pedIndex, TRUE)
				ELSE
					FREEZE_ENTITY_POSITION(sEnemyBackup[i].pedIndex, FALSE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
				IF NOT DOES_ENTITY_EXIST(pedClosestEnemy)
				OR (DOES_ENTITY_EXIST(pedClosestEnemy)
				AND IS_PED_INJURED(pedClosestEnemy))
				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyBackup[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy, FALSE))
					pedClosestEnemy = sEnemyBackup[i].pedIndex
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iEnemySetPiece i
		IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
		AND NOT (i = ENUM_TO_INT(SET_PIECE_CUTTER)
		AND iSetPiece[SET_PIECE_CUTTER] >= 3)
//			IF IS_ENTITY_DEAD(sEnemySetPiece[i].pedIndex)
//			OR eMissionObjective >= stageTriadsChase	//OR bInAbattoir = FALSE
//				SAFE_REMOVE_BLIP(sEnemySetPiece[i].blipIndex)
//			ELSE
//				IF i <> ENUM_TO_INT(SET_PIECE_GRAPPLE)
//					SAFE_ADD_BLIP_PED(sEnemySetPiece[i].blipIndex, sEnemySetPiece[i].pedIndex, TRUE)
//				ENDIF
//			ENDIF
			
			IF i <> ENUM_TO_INT(SET_PIECE_GRAPPLE)
				UPDATE_AI_PED_BLIP(sEnemySetPiece[i].pedIndex, blipStructSetPiece[i])
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(sEnemySetPiece[i].pedIndex)
				IF bPinnedAbattoir = FALSE
					FREEZE_ENTITY_POSITION(sEnemySetPiece[i].pedIndex, TRUE)
				ELSE
					FREEZE_ENTITY_POSITION(sEnemySetPiece[i].pedIndex, FALSE)
				ENDIF
			ENDIF
			
//			IF NOT IS_PED_INJURED(sEnemySetPiece[i].pedIndex)
//				IF NOT DOES_ENTITY_EXIST(pedClosestEnemy)
//				OR (DOES_ENTITY_EXIST(pedClosestEnemy)
//				AND IS_PED_INJURED(pedClosestEnemy))
//				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemySetPiece[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy, FALSE))
//					pedClosestEnemy = sEnemySetPiece[i].pedIndex
//				ENDIF
//			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT iEnemyEscape i
		IF DOES_ENTITY_EXIST(sEnemyEscape[i].pedIndex)
//			IF IS_ENTITY_DEAD(sEnemyEscape[i].pedIndex)
//			OR eMissionObjective >= stageTriadsChase	//OR bInAbattoir = FALSE
//				SAFE_REMOVE_BLIP(sEnemyEscape[i].blipIndex)
//			ELSE
//				SAFE_ADD_BLIP_PED(sEnemyEscape[i].blipIndex, sEnemyEscape[i].pedIndex, TRUE)
//			ENDIF
			
			UPDATE_AI_PED_BLIP(sEnemyEscape[i].pedIndex, blipStructEscape[i])
			
			IF NOT IS_ENTITY_DEAD(sEnemyEscape[i].pedIndex)
				IF bPinnedAbattoir = FALSE
					FREEZE_ENTITY_POSITION(sEnemyEscape[i].pedIndex, TRUE)
				ELSE
					FREEZE_ENTITY_POSITION(sEnemyEscape[i].pedIndex, FALSE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(sEnemyEscape[i].pedIndex)
				IF NOT DOES_ENTITY_EXIST(pedClosestEnemy)
				OR (DOES_ENTITY_EXIST(pedClosestEnemy)
				AND IS_PED_INJURED(pedClosestEnemy))
				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyEscape[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy, FALSE))
					pedClosestEnemy = sEnemyEscape[i].pedIndex
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND pedClosestEnemyTemp != pedClosestEnemy
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, pedClosestEnemy, "MCH2GOON")
	ENDIF
ENDPROC

PROC WAIT_WITH_DEATH_CHECKS(INT iWait = 0)
	INT iGameTime = GET_GAME_TIMER() + iWait	
	
	WHILE GET_GAME_TIMER() <= iGameTime
		WAIT(0)
		
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FreshMeat")
		
		DEATH_CHECKS()
	ENDWHILE
ENDPROC

PROC LOAD_SCENE_ADV(VECTOR vCoords, FLOAT fRadius = 20.0)
	NEW_LOAD_SCENE_START_SPHERE(vCoords, fRadius)
	
	INT iTimeOut = GET_GAME_TIMER() + 20000
	
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_LOADED()
	AND GET_GAME_TIMER() < iTimeOut
		#IF IS_DEBUG_BUILD
		PRINTLN("NEW_LOAD_SCENE_START_SPHERE(", vCoords.X, ", ", vCoords.Y, ", ", vCoords.Z, ", ", fRadius, ")...")
		#ENDIF
		
		PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
		
		WAIT_WITH_DEATH_CHECKS(0)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	IF GET_GAME_TIMER() > iTimeOut
		SCRIPT_ASSERT("NEW_LOAD_SCENE_START_SPHERE timed out, see log for details.")
	ENDIF
	#ENDIF
	
	NEW_LOAD_SCENE_STOP()
ENDPROC

PROC CREATE_CONVERSATION_ADV(STRING sLabel, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE, enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		WHILE NOT CREATE_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, eConvPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
	ENDIF
ENDPROC

PROC CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelSub)
		WHILE NOT CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
	ENDIF
ENDPROC

PROC PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelSub)
		WHILE NOT PLAY_SINGLE_LINE_FROM_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(", ") PRINTSTRING(sLabelSub) PRINTSTRING(" CONVERSATION")
			PRINTNL()

			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabelSub, bOnce)
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_START_CUTSCENE(FLOAT fRadius = 0.0)
	IF CAN_PLAYER_START_CUTSCENE() = TRUE
		IF fRadius <> 0.0
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), fRadius, TRUE)
		ENDIF
		
		CLEAR_TEXT()
		KILL_PHONE_CONVERSATION()
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	ENDIF
	
	RETURN CAN_PLAYER_START_CUTSCENE()
ENDFUNC

PROC createEnemy(ACTOR &sEnemy, VECTOR vPosition, FLOAT fHeading, WEAPON_TYPE wpType = WEAPONTYPE_PISTOL, MODEL_NAMES modelName = G_M_M_CHIGOON_02)
	SPAWN_PED(sEnemy.pedIndex, modelName, vPosition, fHeading)
	SET_PED_RANDOM_COMPONENT_VARIATION(sEnemy.pedIndex)
	IF modelName = G_M_M_CHIGOON_02
		INT iRandHair = GET_RANDOM_INT_IN_RANGE(0, 2)	//Hair
		IF iRandHair = 0
			SET_PED_COMPONENT_VARIATION(sEnemy.pedIndex, PED_COMP_HAIR, 0, GET_RANDOM_INT_IN_RANGE(0, 2))
		ELIF iRandHair = 1
			SET_PED_COMPONENT_VARIATION(sEnemy.pedIndex, PED_COMP_HAIR, 1, GET_RANDOM_INT_IN_RANGE(0, 2))
		ELIF iRandHair = 2
			SET_PED_COMPONENT_VARIATION(sEnemy.pedIndex, PED_COMP_HAIR, 2, 0)
		ENDIF
	ENDIF
	SET_PED_KEEP_TASK(sEnemy.pedIndex, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(sEnemy.pedIndex, relGroupEnemy)
	SET_PED_AS_ENEMY(sEnemy.pedIndex, TRUE)
	SET_PED_SPHERE_DEFENSIVE_AREA(sEnemy.pedIndex, vPosition, 3.0)
	SET_ENTITY_HEALTH(sEnemy.pedIndex, 200)
	SET_PED_MAX_HEALTH(sEnemy.pedIndex, 200)
	SET_PED_DIES_WHEN_INJURED(sEnemy.pedIndex, TRUE)
	SET_PED_ACCURACY(sEnemy.pedIndex, 7)
	GIVE_WEAPON_TO_PED(sEnemy.pedIndex, wpType, INFINITE_AMMO, TRUE)
	SET_CURRENT_PED_WEAPON(sEnemy.pedIndex, wpType, TRUE)
	//SET_PED_SHOOT_RATE(sEnemy.pedIndex, 99)
	SET_PED_COMBAT_MOVEMENT(sEnemy.pedIndex, CM_DEFENSIVE)
	//SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_REQUIRES_LOS_TO_SHOOT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
	//SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
	//SET_COMBAT_FLOAT(sEnemy.pedIndex, CCF_BURST_DURATION_IN_COVER, 4.0)
	//SET_COMBAT_FLOAT(sEnemy.pedIndex, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
	//SET_COMBAT_FLOAT(sEnemy.pedIndex, CCF_TIME_BETWEEN_PEEKS, 5.0)
	SET_PED_CONFIG_FLAG(sEnemy.pedIndex, PCF_DisableHurt, TRUE)
	SET_PED_CONFIG_FLAG(sEnemy.pedIndex, PCF_KillWhenTrapped, TRUE)
	SET_PED_CONFIG_FLAG(sEnemy.pedIndex, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(sEnemy.pedIndex, PCF_DisableExplosionReactions, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_DISABLE_PINNED_DOWN, TRUE)
	IF HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
		SET_PED_STRAFE_CLIPSET(sEnemy.pedIndex, "MOVE_STRAFE@COP")
	ENDIF
	
	INT i
	
	sEnemy.iStage = 0
	sEnemy.iTimer = 0
	
	REPEAT 3 i
		sEnemy.iTime[i] = -1
		sEnemy.pedCheck[i] = NULL
		sEnemy.vLocate[i] = VECTOR_ZERO
		sEnemy.vLocSize[i] = VECTOR_ZERO
		sEnemy.fDist[i] = -1.0
		CLEAR_BIT(sEnemy.iBitsetStrict, i)	//sEnemy.bStrict[i] = FALSE
		sEnemy.eAdvanceStyle[i] = GO_TO_COMBAT
		sEnemy.fSpeed[i] = 1.0
		CLEAR_BIT(sEnemy.iBitsetNavMesh, i)	//sEnemy.bNavMesh[i] = TRUE
		sEnemy.vPoint[i] = VECTOR_ZERO
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	sEnemy.sDebugName = NULL
	#ENDIF
	
	//Stats
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(sEnemy.pedIndex)
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		SWITCH CAM VARIABLES
// -----------------------------------------------------------------------------------------------------------

ENUM MICHAEL2_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_REQUEST_ASSETS,
	SWITCH_CAM_SETUP_SPLINE,
	SWITCH_CAM_PLAYING_SPLINE,
	SWITCH_CAM_SHUTDOWN_SPLINE,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
MICHAEL2_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCam_FranklinToMichael
SWITCH_CAM_STRUCT scsSwitchCam_FranklinInCarToMichael

BOOL bFrankToMikeSwitchComplete

SWITCH_CAM_STRUCT scsSwitchCam_MichaelToFranklin

SWITCH_CAM_STRUCT scsSwitchCam_FranklinGrappleToMichael

#IF IS_DEBUG_BUILD
//BOOL bSwitchCamDebugScenarioEnabled = TRUE
//BOOL bResetDebugScenario = FALSE
#ENDIF

BOOL bPlayerControlGiven

FLOAT SwitchCamGameplayCamHeading = 0.0
FLOAT SwitchCamGameplayCamPitch = 0.0

FLOAT FrankGrappleToMikeGameplayCamHeading = 0.0
FLOAT FrankGrappleToMikeGameplayCamPitch = -2.8

BOOL bFranklinPlayedCellAnim
BOOL bFranklinPlayAnimFromHangupPos
OBJECT_INDEX oiFrankCellPhoneProp
OBJECT_INDEX oiFrankCellPhoneDisplay

INT iFrankToMike_PhoneAudID
BOOL bFrankToMikeSwitch_PhoneSoundPlayed

BOOL bMikeToFrank_SwitchToFrank
BOOL bMikeToFrank_StopDialogue
FLOAT fMikeToFrank_StopDialoguePhase = 0.53
BOOL bMikeToFrank_SwitchedToFrank
FLOAT fMikeToFrank_SwitchToFrankPhase = 0.6
//BOOL bMikeToFrank_DestGeoRequested
//FLOAT fMikeToFrank_PreloadDestGeo = 0.0
//FLOAT fSRLTime

PED_INDEX piMichaelSwitch
PED_INDEX piFranklinSwitch

MODEL_NAMES mn_FrankPhoneDisplay = PROP_PHONE_OVERLAY_01
VECTOR v_FrankPhoneDisplayOffset = <<0.0, 0.0, 0.0>>
VECTOR v_FrankPhoneDisplayRotation = <<0.0, 0.0, 0.0>>

#IF IS_DEBUG_BUILD
INT iFrankToMike_MikeHangingAnimNum = 2
#ENDIF

BOOL bDoMikeToFranklinCustomSwitch
BOOL bMikeToFranklinSwitchTriggered
#IF IS_DEBUG_BUILD
	BOOL bSkipToFrankToMikeSwitch = FALSE
	BOOL bSkipCombatFrankGrappleToMikeSwitch = FALSE
#ENDIF

BOOL bFrankGrappleToMike_GameplayCamShakeActivated = FALSE
INT iFrankGrappleToMike_GameplayCamShakeDuration = 8000
FLOAT fFrankGrappleToMike_GameplayCamShakeAmplitude = 0.2
FLOAT fFrankGrappleToMike_ReturnPlayerControlPhase = 0.62

PROC SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piFranklin, PED_INDEX &piMichael)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_MICHAEL")
	
	bMikeToFranklinSwitchTriggered = bMikeToFranklinSwitchTriggered
	bDoMikeToFranklinCustomSwitch = bDoMikeToFranklinCustomSwitch
	
	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].vNodePos = <<-0.0606, 1.7105 - 0.35, 0.3801>>
		thisSwitchCam.nodes[0].vNodeDir = <<0.2063, 0.0631, 0.4815>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		
		thisSwitchCam.nodes[1].iNodeTime = 1200
		thisSwitchCam.nodes[1].vNodePos = <<-0.0610, 1.7100 - 0.35, 0.3800>>
		thisSwitchCam.nodes[1].vNodeDir = <<0.2060, 0.0630, 0.4810>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortFranklinIn
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.9500

		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-0.0838, 1.8496, 2.7160>>
		thisSwitchCam.nodes[2].vNodeDir = <<0.1833, 0.2025, 2.8170>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelMid
		thisSwitchCam.nodes[2].fMinExposure = 200.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 50
		thisSwitchCam.nodes[2].iRampDownDuration = 50
		thisSwitchCam.nodes[2].iHoldDuration = 100
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.4000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].vNodePos = <<-1.1545, 1.0247, -1.9282>>
		thisSwitchCam.nodes[4].vNodeDir = <<-2.8900, 0.0000, -132.1460>>
		thisSwitchCam.nodes[4].fNodeFOV = 50.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = TRUE

		thisSwitchCam.nodes[5].iNodeTime = 600
		thisSwitchCam.nodes[5].vNodePos = <<-1.0990, 0.9744, -0.4433>>
		thisSwitchCam.nodes[5].vNodeDir = <<-2.8898, 0.0000, -132.1461>>
		thisSwitchCam.nodes[5].fNodeFOV = 50.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.4000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = TRUE

		thisSwitchCam.nodes[6].iNodeTime = 1600
		thisSwitchCam.nodes[6].vNodePos = <<-1.04405, 0.92568, -0.4433>>	//<<-1.2667, 1.1257, -0.4319>>
		thisSwitchCam.nodes[6].vNodeDir = <<-2.8898, 0.0000, -132.1461>>
		thisSwitchCam.nodes[6].fNodeFOV = 50.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = TRUE


		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.3000
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.4500
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = FALSE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Michael2_FranklinToMichael.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_Michael2_FranklinToMichael.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piFranklin
	thisSwitchCam.piPeds[1] = piMichael
	
		
ENDPROC


PROC SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_IN_CAR_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piFranklin, PED_INDEX &piMichael)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_IN_CAR_TO_MICHAEL")
	
	bMikeToFranklinSwitchTriggered = bMikeToFranklinSwitchTriggered
	bDoMikeToFranklinCustomSwitch = bDoMikeToFranklinCustomSwitch
	
	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-0.1073, 2.1319, 0.6445>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<0.2353, 0.0176, 0.5680>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1200
		thisSwitchCam.nodes[1].vNodePos = <<-0.1199, 2.2099, 0.6473>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<0.2354, 0.0176, 0.5680>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortFranklinIn
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.9500
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-0.1100, 2.1471, 2.4307>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<0.2453, -0.0454, 2.3514>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelMid
		thisSwitchCam.nodes[2].fMinExposure = 200.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 50
		thisSwitchCam.nodes[2].iRampDownDuration = 50
		thisSwitchCam.nodes[2].iHoldDuration = 100
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.4000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<-1.1545, 1.0247, -1.9282>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-2.8900, 0.0000, -132.1460>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 50.0000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 600
		thisSwitchCam.nodes[5].vNodePos = <<-1.0990, 0.9744, -0.4433>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<-2.8898, 0.0000, -132.1461>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 50.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.4000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 1600
		thisSwitchCam.nodes[6].vNodePos = <<-1.04405, 0.92568, -0.4433>>	//<<-1.2667, 1.1257, -0.4319>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<-2.8898, 0.0000, -132.1461>>
		thisSwitchCam.nodes[6].bPointAtEntity = FALSE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 50.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = TRUE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000



		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.3000
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.4500
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = FALSE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Michael2_FranklinInCarToMichael.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_Michael2_FranklinInCarToMichael.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piFranklin
	thisSwitchCam.piPeds[1] = piMichael
	
		
ENDPROC


/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_FRANKLIN_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	INT iCurrentSwitchNode
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")
			SET_PED_POPULATION_BUDGET(0)
			SET_VEHICLE_POPULATION_BUDGET(0)
			SET_REDUCE_PED_MODEL_BUDGET(TRUE)
			SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)

			REQUEST_CUTSCENE("MIC_2_MCS_1")

		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE")
			
			piMichaelSwitch = PLAYER_PED(CHAR_MICHAEL)
			piFranklinSwitch = PLAYER_PED(CHAR_FRANKLIN)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(piFranklinSwitch)
				SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_MICHAEL(thisSwitchCam, piFranklinSwitch, piMichaelSwitch)
			ELSE
				SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_IN_CAR_TO_MICHAEL(thisSwitchCam, piFranklinSwitch, piMichaelSwitch)
			ENDIF

			IF DOES_SWITCH_CAM_EXIST(thisSwitchCam)
				DESTROY_SWITCH_CAM(thisSwitchCam)
			ENDIF
			
			CREATE_SPLINE_CAM(thisSwitchCam)
			
			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			SHAKE_CAM(thisSwitchCam.ciSpline, "HAND_SHAKE", 1.0)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			SETTIMERB(0)
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
				VEHICLE_INDEX vehBringToHalt
				
				vehBringToHalt = GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_FRANKLIN))
				SET_VEHICLE_RADIO_ENABLED(vehBringToHalt, FALSE)

				IF DOES_ENTITY_EXIST(vehBringToHalt)
				AND NOT IS_ENTITY_DEAD(vehBringToHalt)
					bHaltVehicle = TRUE
				ELSE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("SHAPETEST[TRUE]")
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED(CHAR_FRANKLIN))
				SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<-14.5290, -1446.9047, 29.6462>>, 182.3441, FALSE)
				TASK_PLAY_ANIM(PLAYER_PED(CHAR_FRANKLIN), "cellphone@", "cellphone_text_read_base", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING | AF_UPPERBODY)	// | AF_SECONDARY)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
				FORCE_PED_MOTION_STATE(PLAYER_PED(CHAR_FRANKLIN), MS_ON_FOOT_IDLE)
			ENDIF
			
			bPlayerControlGiven = FALSE
			bFrankToMikeSwitch_PhoneSoundPlayed = FALSE
			iFrankToMike_PhoneAudID = 0
			
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			
			IF NOT IS_ENTITY_ATTACHED(piMichaelSwitch)
				ATTACH_ENTITY_TO_ENTITY(piMichaelSwitch, objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
			ENDIF
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE")
			CDEBUG3LN(DEBUG_MISSION, GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))
			
			iCurrentSwitchNode = UPDATE_SPLINE_CAM(thisSwitchCam)
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				
				IF iCurrentSwitchNode >= thisSwitchCam.iCamSwitchFocusNode
					IF NOT HAS_LABEL_BEEN_TRIGGERED("LoadSceneFranklin")
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(SwitchCamGameplayCamHeading)
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(SwitchCamGameplayCamPitch)
						
						CLEAR_PED_TASKS(PLAYER_PED(CHAR_FRANKLIN))

//						IF IS_NEW_LOAD_SCENE_ACTIVE()
//							NEW_LOAD_SCENE_STOP()
//						ENDIF
						
//						PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
//						PLAY_SOUND_FRONTEND(-1, "Short_Transition_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						WAIT_WITH_DEATH_CHECKS(0)
						
						//[MF] Loading geo at Franklin's position for the next switch RIGHT AWAY.
						NEW_LOAD_SCENE_START_SPHERE(<<-85.4048, -1483.7083, 32.0893>>, 200.0)	//, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	//GET_ENTITY_COORDS(piFranklinSwitch, FALSE)
						
						SET_LABEL_AS_TRIGGERED("LoadSceneFranklin", TRUE)
					ENDIF
				ENDIF
				
				IF NOT bFrankToMikeSwitch_PhoneSoundPlayed
					PLAY_SOUND_FRONTEND(iFrankToMike_PhoneAudID, "Pull_Out", "Phone_SoundSet_Franklin")
					bFrankToMikeSwitch_PhoneSoundPlayed = TRUE
				ENDIF
				
				//[MF] Once cam is at the end, reset the gameplay cam positon and advance to next state.
				IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				AND GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.00
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
						CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
						eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
					ENDIF
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			
			SET_TIME_SCALE(1.0)
			
			IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bPlayerControlGiven = TRUE
			ENDIF
			
			eSwitchCamState = SWITCH_CAM_IDLE

			thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
			sCamDetails.bRun = FALSE
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			
			SET_PED_POPULATION_BUDGET(3)
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_REDUCE_PED_MODEL_BUDGET(FALSE)
			SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
			
			DELETE_OBJECT(oiFrankCellPhoneProp)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_phone_ING_03)
			
			DELETE_OBJECT(oiFrankCellPhoneDisplay)
			SET_MODEL_AS_NO_LONGER_NEEDED(mn_FrankPhoneDisplay)
			
			CLEAR_PED_SECONDARY_TASK(PLAYER_PED(CHAR_FRANKLIN))
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
				SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_FRANKLIN)), TRUE)
			ENDIF

			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC


PROC SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piMichael, PED_INDEX &piFranklin)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_FRANKLIN")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].vNodePos = <<0.8701, 1.3396, -0.5233>>
		thisSwitchCam.nodes[0].vNodeDir = <<0.0970, -0.0262, -0.5900>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bIsLowDetailNode = TRUE

		thisSwitchCam.nodes[1].iNodeTime = 3200
		thisSwitchCam.nodes[1].vNodePos = <<0.5453, 1.0492, -0.4973>>
		thisSwitchCam.nodes[1].vNodeDir = <<-0.0304, 0.0807, -0.5986>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.9000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelIn
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.9900
		thisSwitchCam.nodes[1].bIsLowDetailNode = TRUE

		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<0.7067, 1.7139, 1.9620>>
		thisSwitchCam.nodes[2].vNodeDir = <<-0.0713, 0.0428, 1.9701>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
//		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
//		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortFranklinMid
		thisSwitchCam.nodes[2].fMinExposure = 200.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 50
		thisSwitchCam.nodes[2].iRampDownDuration = 50
		thisSwitchCam.nodes[2].iHoldDuration = 100
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.6000
		thisSwitchCam.nodes[2].bIsLowDetailNode = TRUE

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].vNodePos = <<-1.8041, 2.1361, -0.3195>>
		thisSwitchCam.nodes[4].vNodeDir = <<0.2940, 0.3268, -0.1647>>
		thisSwitchCam.nodes[4].bPointAtEntity = TRUE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].fNodeFOV = 45.0000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.9000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = TRUE

		thisSwitchCam.nodes[5].iNodeTime = 500
		thisSwitchCam.nodes[5].vNodePos = <<-1.9438 - 0.25, 1.9582 - 1.35, 0.3947>>
		thisSwitchCam.nodes[5].vNodeDir = <<0.2150, 0.2517, 0.2640>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].fNodeFOV = 45.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = TRUE

		thisSwitchCam.nodes[6].iNodeTime = 3000
		thisSwitchCam.nodes[6].vNodePos = <<-1.9158 + 1.25 - 0.25, 1.7808 - 1.35, 0.5142>>
		thisSwitchCam.nodes[6].vNodeDir = <<0.0695, 0.1097, 0.5908>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].fNodeFOV = 45.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = TRUE
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
			thisSwitchCam.nodes[7].iNodeTime = 500
			thisSwitchCam.nodes[7].vNodePos = <<-0.3329, 0.5154, 0.5142>>
			thisSwitchCam.nodes[7].vNodeDir = <<0.0695, 0.1097, 0.5908>>
			thisSwitchCam.nodes[7].bPointAtEntity = TRUE
			thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = TRUE
			thisSwitchCam.nodes[7].bAttachOffsetIsRelative = TRUE
			thisSwitchCam.nodes[7].fNodeFOV = 45.0000
			thisSwitchCam.nodes[7].bCamEaseForceLinear = TRUE
			thisSwitchCam.nodes[7].bCamEaseForceLevel = TRUE
		ENDIF
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
		thisSwitchCam.iNumNodes = 8
		ELSE
		thisSwitchCam.iNumNodes = 7
		ENDIF
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.4500
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.5200
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = TRUE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0
		
		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Michael2_MichaelToFranklin.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_Michael2_MichaelToFranklin.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piMichael
	thisSwitchCam.piPeds[1] = piFranklin
	
		
ENDPROC

/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_MICHAEL_TO_FRANKLIN(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	//INT iCurrentSwitchNode
		
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")
			NEW_LOAD_SCENE_START_SPHERE(<<-85.4048, -1483.7083, 32.0893>>, 200.0)	//, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	//GET_ENTITY_COORDS(piFranklinSwitch)
			SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			SET_FOCUS_ENTITY(PLAYER_PED(CHAR_FRANKLIN))
			ADD_NAVMESH_REQUIRED_REGION(990.39264, -2162.96143, 25.0)
//			SET_FOCUS_POS_AND_VEL()
			
//			PREFETCH_SRL("mike2_mike_to_frank")
		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE")

			piMichaelSwitch = PLAYER_PED(CHAR_MICHAEL)
			piFranklinSwitch = PLAYER_PED(CHAR_FRANKLIN)
			
			//PREFETCH_SRL("mike2_mike_to_frank")
//			IF IS_SRL_LOADED()
//				CDEBUG3LN(DEBUG_MISSION, "SRL Loaded")

				SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_FRANKLIN(thisSwitchCam, piMichaelSwitch, piFranklinSwitch)

				IF DOES_SWITCH_CAM_EXIST(thisSwitchCam)
					DESTROY_SWITCH_CAM(thisSwitchCam)
				ENDIF
				
				CREATE_SPLINE_CAM(thisSwitchCam)
				
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)	
				
				SETTIMERB(0)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				bPlayerControlGiven = FALSE
				
//				bMikeToFrank_DestGeoRequested = FALSE
				bMikeToFrank_SwitchToFrank = FALSE
				bMikeToFrank_SwitchedToFrank = FALSE
				bMikeToFrank_StopDialogue = FALSE
				
				SET_GAME_PAUSES_FOR_STREAMING(TRUE)
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehFranklin)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehFranklin)
				ENDIF
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
					START_PLAYBACK_RECORDED_VEHICLE(vehFranklin, 001, "ALrollingstart")	PRINTLN("START_PLAYBACK_RECORDED_VEHICLE(vehFranklin, 001, 'ALrollingstart')")
					SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehFranklin, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "ALrollingstart") - 4000.0)
					SET_PLAYBACK_SPEED(vehFranklin, 0.0)
					SET_VEHICLE_ENGINE_ON(vehFranklin, TRUE, TRUE)
				ENDIF

//				SET_SRL_READAHEAD_TIMES(-1, 6, -1, 6)
//				BEGIN_SRL()
//				SET_SRL_POST_CUTSCENE_CAMERA(<<-99.7, -1499.5, 34.9>>, <<-0.8, 0.1, 140.7>>)
//				fSRLTime = 0
//				SET_SRL_TIME(fSRLTime)

				eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE
				FALLTHRU
//			ENDIF
		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE")
			CDEBUG3LN(DEBUG_MISSION, GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))
			
			//iCurrentSwitchNode = UPDATE_SPLINE_CAM(thisSwitchCam)
			
			UPDATE_SPLINE_CAM(thisSwitchCam)
			
				
			//[MF] To fix an issue where the HUD and radar were getting displayed early		
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)

//				IF NOT bMikeToFrank_DestGeoRequested
//					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fMikeToFrank_PreloadDestGeo
//						NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(piFranklinSwitch), 100.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
//						bMikeToFrank_DestGeoRequested = TRUE
//					ENDIF
//				ENDIF
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fMikeToFrank_SwitchToFrankPhase
					IF NOT bMikeToFrank_SwitchToFrank
						bMikeToFrank_SwitchToFrank = TRUE
						SET_GAME_PAUSES_FOR_STREAMING(FALSE)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(SwitchCamGameplayCamHeading)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(SwitchCamGameplayCamPitch)	
				ENDIF
				
				IF NOT bMikeToFrank_StopDialogue
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) > fMikeToFrank_StopDialoguePhase
						STOP_SCRIPTED_CONVERSATION(FALSE)
						bMikeToFrank_StopDialogue = TRUE
					ENDIF
				ENDIF
				
//				PRINTLN("TIMERB() = ", TIMERB())
//				PRINTLN("GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) = ", GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MichaelToFranklinCarSwitchCut")
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 0.506712	//0.479589	//0.517945
						ANIMPOSTFX_PLAY("SwitchShortFranklinMid", 0, FALSE)
						PRINTLN("SwitchShortFranklinMid . . .")
						SET_LABEL_AS_TRIGGERED("MichaelToFranklinCarSwitchCut", TRUE)
					ENDIF
				ENDIF
				
				//IF iCurrentSwitchNode >= (thisSwitchCam.iCamSwitchFocusNode -1)
//					fSRLTime += GET_FRAME_TIME()
//					IF fSRLTime < 0.0
//						fSRLTime = 0.0
//					ENDIF
//					SET_SRL_TIME(fSRLTime)
				//ENDIF
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchFlashIntoFranklinDriving")
						IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 0.75	//1.0
						AND bMikeToFrank_SwitchedToFrank
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							
							SET_LABEL_AS_TRIGGERED("SwitchFlashIntoFranklinDriving", TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				//[MF] Once cam is at the end, reset the gameplay cam positon and advance to next state.
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 0.8	//1.0
				AND bMikeToFrank_SwitchedToFrank
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY

			REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0)

			SET_TIME_SCALE(1.0)

			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF DOES_SWITCH_CAM_EXIST(thisSwitchCam)
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
				DESTROY_SWITCH_CAM(thisSwitchCam)
			ENDIF
			
			IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bPlayerControlGiven = TRUE
			ENDIF

			eSwitchCamState = SWITCH_CAM_IDLE

			thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
			bOkToSwitch = TRUE
			sCamDetails.bRun = FALSE
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
//			END_SRL()

			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_GRAPPLE_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piMichael, PED_INDEX &piFranklin)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_MICHAEL")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[0].vNodePos = <<998.4370, -2109.4990, 30.0180>>
		thisSwitchCam.nodes[0].vNodeDir = <<-1.8130, 2.1560, -4.1430>>
		thisSwitchCam.nodes[0].fNodeFOV = 25.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[1].iNodeTime = 2000
		thisSwitchCam.nodes[1].vNodePos = <<998.4368, -2109.4993, 30.0184>>
		thisSwitchCam.nodes[1].vNodeDir = <<-1.8126, 2.1560, -4.1427>>
		thisSwitchCam.nodes[1].fNodeFOV = 25.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.6000
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortFranklinIn
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.9000

		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-4.3700, -43.7979, 0.0358>>
		thisSwitchCam.nodes[2].vNodeDir = <<-0.9587, 0.0000, -2.0252>>
		thisSwitchCam.nodes[2].fNodeFOV = 45.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0700

		thisSwitchCam.nodes[3].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[3].iNodeTime = 1500
		thisSwitchCam.nodes[3].vNodePos = <<-0.2142, -0.7233, -0.9051>>
		thisSwitchCam.nodes[3].vNodeDir = <<0.5315, 0.0000, -7.4585>>
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].fNodeFOV = 45.0000
		thisSwitchCam.nodes[3].fNodeCamShake = 1.0000
		thisSwitchCam.nodes[3].bFlashEnabled = TRUE
		thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelMid

		thisSwitchCam.iNumNodes = 4
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.4300
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.5800
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = TRUE
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 500


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_Michael2_FrankGrappleToMike.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_Michael2_FrankGrappleToMike.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piMichael
	thisSwitchCam.piPeds[1] = piFranklin
	
		
ENDPROC

/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_FRANKLIN_GRAPPLE_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")
			
			piMichaelSwitch = PLAYER_PED(CHAR_MICHAEL)
			piFranklinSwitch = PLAYER_PED(CHAR_FRANKLIN)

			SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_GRAPPLE_TO_MICHAEL(thisSwitchCam, piFranklinSwitch, piMichaelSwitch)			
			LOAD_CAM_SHAKE_LIBRARIES(thisSwitchCam)
			
		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE")

			piMichaelSwitch = PLAYER_PED(CHAR_MICHAEL)
			piFranklinSwitch = PLAYER_PED(CHAR_FRANKLIN)

			SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_GRAPPLE_TO_MICHAEL(thisSwitchCam, piFranklinSwitch, piMichaelSwitch)
		
			//[MF] Required to make space for our switch cams.
			//DESTROY_ALL_CAMS()
			
			IF DOES_SWITCH_CAM_EXIST(thisSwitchCam)
				DESTROY_SWITCH_CAM(thisSwitchCam)
			ENDIF
			
			CREATE_SPLINE_CAM(thisSwitchCam)
			
			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)	
			
			SETTIMERB(0)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			bPlayerControlGiven = FALSE
			
			sCamDetails.bPedSwitched = FALSE
			sCamDetails.bOKToSwitchPed = FALSE		
			
//			bMikeToFrank_DestGeoRequested = FALSE
			bMikeToFrank_SwitchToFrank = FALSE
			bMikeToFrank_SwitchedToFrank = FALSE
			bFrankGrappleToMike_GameplayCamShakeActivated = FALSE

			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE
		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE", "  ", GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))
			
			UPDATE_SPLINE_CAM(thisSwitchCam)		
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(FrankGrappleToMikeGameplayCamHeading)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(FrankGrappleToMikeGameplayCamPitch)				
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= thisSwitchCam.iCamSwitchFocusNode
					sCamDetails.bOKToSwitchPed = TRUE
				ENDIF
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fFrankGrappleToMike_ReturnPlayerControlPhase
					IF NOT bPlayerControlGiven
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						DISPLAY_RADAR(TRUE)
						DISPLAY_HUD(TRUE)
						bPlayerControlGiven = TRUE
					ENDIF
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE) //[MF] Force the reticle to draw on screen
				ELSE
					HIDE_HUD_AND_RADAR_THIS_FRAME() //[MF] This is temp until we can figure out what else in the script is turning these elements back on.
				ENDIF
				
				//[MF] Once cam is at the end, reset the gameplay cam positon and advance to next state.
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.00
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY

			SET_TIME_SCALE(1.0)

			RENDER_SCRIPT_CAMS(FALSE, FALSE)

			IF DOES_SWITCH_CAM_EXIST(thisSwitchCam)
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
				DESTROY_SWITCH_CAM(thisSwitchCam)
			ENDIF			

			IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				bPlayerControlGiven = TRUE
			ENDIF

			bRadar = TRUE
			
//			SETTIMERA(0)
			SETTIMERB(0)
			
			eSwitchCamState = SWITCH_CAM_IDLE

			thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
			sCamDetails.bRun = FALSE

			SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", fFrankGrappleToMike_GameplayCamShakeAmplitude)
			bFrankGrappleToMike_GameplayCamShakeActivated = TRUE
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC


#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Create the debug widgets for working with the scripted character switch cam.
PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")

	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables")
		
		START_WIDGET_GROUP("Franklin To Michael")
			ADD_WIDGET_BOOL("Skip To: Frank to Mike Switch", bSkipToFrankToMikeSwitch)
			ADD_WIDGET_VECTOR_SLIDER("Phone Display Offset", v_FrankPhoneDisplayOffset, 0.0, 1.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Phone Display Rotation", v_FrankPhoneDisplayRotation, -360.0, 360.0, 10.0)
			START_WIDGET_GROUP("Michael Tuner")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Anim A")
					ADD_TO_WIDGET_COMBO("Anim B")
					ADD_TO_WIDGET_COMBO("Anim C")
					ADD_TO_WIDGET_COMBO("Anim D")
				STOP_WIDGET_COMBO("Michael Hanging Anim", iFrankToMike_MikeHangingAnimNum)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Michael to Franklin")
//			ADD_WIDGET_FLOAT_SLIDER("Request Geo Cam Phase", fMikeToFrank_PreloadDestGeo, 0.0, 1.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Switch To Franklin Cam Phase", fMikeToFrank_SwitchToFrankPhase, 0.0, 1.0, 0.1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Frank Grapple to Mike")
			ADD_WIDGET_BOOL("Skip Combat", bSkipCombatFrankGrappleToMikeSwitch)
			ADD_WIDGET_FLOAT_SLIDER("Return Player Control Phase", fFrankGrappleToMike_ReturnPlayerControlPhase, 0.0, 1.0, 0.1)
			START_WIDGET_GROUP("Gameplay Cam Angle Tuner")
				ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Heading", FrankGrappleToMikeGameplayCamHeading, -180.0, 180.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Pitch", FrankGrappleToMikeGameplayCamPitch, -180.0, 180.0, 1.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Gameplay Cam Shake Tuner")
				ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Shake Amplitude", fFrankGrappleToMike_GameplayCamShakeAmplitude, 0.0, 2.0, 0.1)
				ADD_WIDGET_INT_SLIDER("Gameplay Cam Shake Duration", iFrankGrappleToMike_GameplayCamShakeDuration, 0, 20000, 500)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()

	IF bSkipCombatFrankGrappleToMikeSwitch = TRUE
		INT index 
	
		REPEAT COUNT_OF(sEnemyBackup) index 
			IF NOT IS_ENTITY_DEAD(sEnemyBackup[index].pedIndex)
				PRINTLN("Killing sEnemyBackup: ", index)
				SET_ENTITY_HEALTH(sEnemyBackup[index].pedIndex, 0)
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

#ENDIF


#IF IS_DEBUG_BUILD
PROC ASSIGN_DEBUG_NAMES(PED_INDEX pedIndex, STRING sName, INT iIndex)
	TEXT_LABEL tDebugName = sName
	IF iIndex <> -1
		tDebugName += iIndex
	ENDIF
	
	SET_PED_NAME_DEBUG(pedIndex, tDebugName)
ENDPROC
#ENDIF

PROC ADVANCE_COMBAT_UPDATE(ACTOR &sEnemy)
	IF DOES_ENTITY_EXIST(sEnemy.pedIndex)
		IF NOT IS_PED_INJURED(sEnemy.pedIndex)
			IF sEnemy.iStage < 3
				BOOL bProceed = FALSE
				
				//Timer
				IF sEnemy.iTime[sEnemy.iStage] <> -1
					IF IS_PED_IN_COMBAT(sEnemy.pedIndex)
						IF sEnemy.iTimer = -1
							sEnemy.iTimer = GET_GAME_TIMER() + sEnemy.iTime[sEnemy.iStage]	PRINTLN("Timer Started: ", sEnemy.iTimer)
						ELIF GET_GAME_TIMER() > sEnemy.iTimer
							bProceed = TRUE 
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " Timer Ended: bProceed = TRUE") #ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Ped
				IF sEnemy.pedCheck[sEnemy.iStage] != NULL
					IF DOES_ENTITY_EXIST(sEnemy.pedCheck[sEnemy.iStage])
						IF IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(sEnemy.pedCheck[sEnemy.iStage]))
							bProceed = TRUE
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " PedCheck Died: bProceed = TRUE") #ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Locate
				IF NOT ARE_VECTORS_EQUAL(sEnemy.vLocate[sEnemy.iStage], VECTOR_ZERO)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), sEnemy.vLocate[sEnemy.iStage], sEnemy.vLocSize[sEnemy.iStage])
						bProceed = TRUE
						#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " PedCheck Died: bProceed = TRUE") #ENDIF
					ENDIF
				ENDIF
				
				//Distance
				IF sEnemy.fDist[sEnemy.iStage] <> -1.0
					IF IS_ENTITY_ON_SCREEN(sEnemy.pedIndex)
					AND NOT IS_ENTITY_OCCLUDED(sEnemy.pedIndex)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemy.pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < sEnemy.fDist[sEnemy.iStage]
							bProceed = TRUE
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " Distance Under: bProceed = TRUE") #ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Advance Combat Stage
				IF bProceed = TRUE
					//Reset Combat Float
					SET_COMBAT_FLOAT(sEnemy.pedIndex, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
					
					SWITCH sEnemy.eAdvanceStyle[sEnemy.iStage]
						CASE GO_TO_COMBAT
							REMOVE_PED_DEFENSIVE_AREA(sEnemy.pedIndex)
							CLEAR_PED_TASKS(sEnemy.pedIndex)
							TASK_COMBAT_PED(sEnemy.pedIndex, PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " GO_TO_COMBAT...") #ENDIF
						BREAK
						CASE GO_TO_POINT
							REMOVE_PED_DEFENSIVE_AREA(sEnemy.pedIndex)
							CLEAR_PED_TASKS(sEnemy.pedIndex)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								IF IS_BIT_SET(sEnemy.iBitsetStrict, sEnemy.iStage)	//sEnemy.bStrict[sEnemy.iStage] = TRUE
									IF IS_BIT_SET(sEnemy.iBitsetNavMesh, sEnemy.iStage)	//sEnemy.bNavMesh[sEnemy.iStage] = TRUE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, sEnemy.vPoint[sEnemy.iStage], sEnemy.fSpeed[sEnemy.iStage])
									ELSE
										TASK_GO_STRAIGHT_TO_COORD(NULL, sEnemy.vPoint[sEnemy.iStage], sEnemy.fSpeed[sEnemy.iStage])
									ENDIF
								ENDIF
								TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, sEnemy.vPoint[sEnemy.iStage], 2.0)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sEnemy.pedIndex, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " GO_TO_POINT...") #ENDIF
						BREAK
						CASE AIM_TO_POINT
							REMOVE_PED_DEFENSIVE_AREA(sEnemy.pedIndex)
							CLEAR_PED_TASKS(sEnemy.pedIndex)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								IF IS_BIT_SET(sEnemy.iBitsetStrict, sEnemy.iStage)	//IF sEnemy.bStrict[sEnemy.iStage] = TRUE
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sEnemy.vPoint[sEnemy.iStage], PLAYER_PED_ID(), sEnemy.fSpeed[sEnemy.iStage], FALSE, 0.5, 4, IS_BIT_SET(sEnemy.iBitsetNavMesh, sEnemy.iStage))	//sEnemy.bNavMesh[sEnemy.iStage])
								ENDIF
								TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, sEnemy.vPoint[sEnemy.iStage], 2.0)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sEnemy.pedIndex, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " AIM_TO_POINT...") #ENDIF
						BREAK
						CASE GUN_TO_POINT
							REMOVE_PED_DEFENSIVE_AREA(sEnemy.pedIndex)
							CLEAR_PED_TASKS(sEnemy.pedIndex)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								IF IS_BIT_SET(sEnemy.iBitsetStrict, sEnemy.iStage)	//IF sEnemy.bStrict[sEnemy.iStage] = TRUE
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, sEnemy.vPoint[sEnemy.iStage], PLAYER_PED_ID(), sEnemy.fSpeed[sEnemy.iStage], TRUE, 0.5, 4, IS_BIT_SET(sEnemy.iBitsetNavMesh, sEnemy.iStage))	//sEnemy.bNavMesh[sEnemy.iStage])
								ENDIF
								TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, sEnemy.vPoint[sEnemy.iStage], 2.0)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sEnemy.pedIndex, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " GUN_TO_POINT...") #ENDIF
						BREAK
						CASE PEEK_FROM_POINT
							SET_PED_CAN_PEEK_IN_COVER(sEnemy.pedIndex, TRUE)
							SET_COMBAT_FLOAT(sEnemy.pedIndex, CCF_TIME_BETWEEN_BURSTS_IN_COVER, -1) //Set Combat Float
							
							REMOVE_PED_DEFENSIVE_AREA(sEnemy.pedIndex)
							CLEAR_PED_TASKS_IMMEDIATELY(sEnemy.pedIndex)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								IF IS_BIT_SET(sEnemy.iBitsetStrict, sEnemy.iStage)	//IF sEnemy.bStrict[sEnemy.iStage] = TRUE
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, sEnemy.vPoint[sEnemy.iStage], -1, TRUE)
								ENDIF
								TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, sEnemy.vPoint[sEnemy.iStage], 2.0)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sEnemy.pedIndex, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " PEEK_FROM_POINT...") #ENDIF
						BREAK
					ENDSWITCH
					
					sEnemy.iTimer = -1
					sEnemy.iStage++
					#IF IS_DEBUG_BUILD PRINTLN(sEnemy.sDebugName, " iStage = ", sEnemy.iStage) #ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(sEnemy.pedIndex) - <<0.0, 0.0, 1.0>>, <<5.0, 5.0, 5.0>>)
//				SET_PED_COMBAT_MOVEMENT(sEnemy.pedIndex, CM_WILLADVANCE)
				
//				SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_AGGRESSIVE, TRUE)
//				SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_USE_COVER, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(sEnemy.pedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				
				REMOVE_PED_DEFENSIVE_AREA(sEnemy.pedIndex)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemy.pedIndex, FALSE)
				
				sEnemy.iStage = 3
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Mid Mission Replay
ENUM MissionReplay
	replayStart,
	replayAbattoirShootout,
	replayMichaelEscape,
	replayMichaelFree,
	replayTriadsChase,
	replayCutEnd
ENDENUM

PROC MISSION_REPLAY(MissionReplay eMissionReplay = replayStart)
	//-	Replay Specific Position
	VECTOR vStartReplay
	FLOAT fStartReplay
	
	SWITCH eMissionReplay
		CASE replayStart
			vStartReplay = vPlayerStart
			fStartReplay = fPlayerStart
		BREAK
		CASE replayAbattoirShootout
			vStartReplay = <<934.6419, -2183.5613, 29.4654>>
			fStartReplay = 355.3260
		BREAK
		CASE replayMichaelEscape
			vStartReplay = <<996.5976, -2148.9912, 29.0>>
			fStartReplay = 143.7361
		BREAK
		CASE replayMichaelFree
			vStartReplay = <<993.9323, -2150.0239, 28.4763>>
			fStartReplay = 354.8923
		BREAK
		CASE replayTriadsChase
			vStartReplay = <<961.2763, -2106.1240, 30.5098>>
			fStartReplay = 89.0595
		BREAK
		CASE replayCutEnd
			vStartReplay = <<-860.1198, 157.7610, 63.9212>>
			fStartReplay = 339.3617
		BREAK
	ENDSWITCH
	
	START_REPLAY_SETUP(vStartReplay, fStartReplay, FALSE)
	
	//Set Player
	IF eMissionReplay <> replayTriadsChase
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
			WAIT(0)
		ENDWHILE
	ENDIF
	
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	
	SWITCH eMissionReplay
		CASE replayStart
			//Michael
//			REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN))
//			
//			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN))
//				WAIT(0)
//			ENDWHILE
//			
//			WHILE NOT CREATE_PLAYER_VEHICLE(vehCar, CHAR_FRANKLIN, <<-25.2062, -1445.3279, 29.6541>>, 180.2567, TRUE, VEHICLE_TYPE_CAR)
//				WAIT(0)
//			ENDWHILE
//			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehCar, TRUE)
//			
//			SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
//			ACTIVATE_PHYSICS(vehCar)
			
			eMissionObjective = stageFindMichael
		BREAK
		CASE replayAbattoirShootout
			//Michael
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT(0)
			ENDWHILE
			
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vPlayerStart, fPlayerStart)
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 19, 0)
			
			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)
			
			SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED_ID(), TLR_NEVER_LOSE_TARGET)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), TRUE)
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_RunFromFiresAndExplosions, FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			CLEAR_AREA(vPlayerStart, 500.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<-26.816481, -1446.621826, 44.654160>> - <<30.0, 30.0, 30.0>>, <<-26.816481, -1446.621826, 44.654160>> + <<30.0, 30.0, 30.0>>)
			
			CLEAR_AREA(<<941.6578, -2181.0586, 29.5517>>, 1000.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<966.108276, -2149.353271, 55.977444>> - <<60.0, 70.0, 30.0>>, <<966.108276, -2149.353271, 55.977444>> + <<60.0, 70.0, 30.0>>)
			
			//Franklin
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PUMPSHOTGUN, 50)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE , 200, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED,  TRUE)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], relGroupBuddy)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Enemy (Outside)
			REQUEST_MODEL(G_M_M_CHIGOON_01)
			REQUEST_MODEL(G_M_M_CHIGOON_02)
			
			WHILE NOT HAS_MODEL_LOADED(G_M_M_CHIGOON_01)
			OR NOT HAS_MODEL_LOADED(G_M_M_CHIGOON_02)
				WAIT(0)
			ENDWHILE
			
			createEnemy(sEnemyOutside[0], <<951.0605, -2184.5310, 29.5517>>, 24.5982, WEAPONTYPE_PISTOL)
			SET_PED_COMPONENT_VARIATION(sEnemyOutside[0].pedIndex, PED_COMP_HAIR, 2, 0)
			SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[0].pedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[0].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
			SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyOutside[0].pedIndex, <<953.1196, -2190.9177, 29.5518>>, 2.0)
			//SET_PED_RELATIONSHIP_GROUP_HASH(sEnemyOutside[0].pedIndex, relGroupPassive)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[0].pedIndex, TRUE)
			SET_LABEL_AS_TRIGGERED("sEnemyOutside[0]Blocking", TRUE)
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, sEnemyOutside[0].pedIndex, "MCH2CHIN1")
//			createEnemy(sEnemyOutside[1], <<962.5493, -2166.0269, 36.0140>>, 179.9051, WEAPONTYPE_PISTOL)
//			sEnemyOutside[1].vLocate[0] = <<948.549438,-2178.816895,32.051651>>		sEnemyOutside[1].vLocSize[0] = <<12.0, 10.0 ,2.5>>	sEnemyOutside[1].eAdvanceStyle[0] = GUN_TO_POINT sEnemyOutside[1].vPoint[0] = <<955.2181, -2169.3628, 35.9913>>
			createEnemy(sEnemyOutside[2], <<951.1171, -2183.3445, 29.5517>>, 186.0809, WEAPONTYPE_PISTOL)
			SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[2].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[2].pedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
			SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyOutside[2].pedIndex, <<954.4744, -2186.4724, 29.5518>>, 2.0)
			//SET_PED_RELATIONSHIP_GROUP_HASH(sEnemyOutside[2].pedIndex, relGroupPassive)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[2].pedIndex, TRUE)
			SET_LABEL_AS_TRIGGERED("sEnemyOutside[2]Blocking", TRUE)
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, sEnemyOutside[2].pedIndex, "MCH2CHIN5")
			
			#IF IS_DEBUG_BUILD
			INT i
			
			REPEAT COUNT_OF(sEnemyOutside) i
				IF NOT IS_PED_INJURED(sEnemyOutside[i].pedIndex)
					ASSIGN_DEBUG_NAMES(sEnemyOutside[i].pedIndex, "Outside ", i)
					sEnemyOutside[i].sDebugName = CONCATENATE_STRINGS("Outside ", GET_STRING_FROM_INT(i))
				ENDIF
			ENDREPEAT
			#ENDIF
			
			//Rail
			SET_RAIL_FINAL_NODE(31)
			
			//Hook
			objHook = CREATE_OBJECT_NO_OFFSET(PROP_LD_HOOK, sRailNodes[31].vPos)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHook, FALSE)
			FREEZE_ENTITY_POSITION(objHook, TRUE)
			RETAIN_ENTITY_IN_INTERIOR(objHook, intAbattoir)
			
			objChain = CREATE_OBJECT_NO_OFFSET(PROP_CS_LEG_CHAIN_01, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, 0.01>>)
			ATTACH_ENTITY_TO_ENTITY(objChain, objHook, -1, vChainOffset, vChainRotation)
			
			objPadlock = CREATE_OBJECT_NO_OFFSET(PROP_CS_PADLOCK, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, -0.01>>)
			ATTACH_ENTITY_TO_ENTITY(objPadlock, objHook, -1, vPadlockOffset, vPadlockRotation)
			
			//Particles
			
			//Cam
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ENDIF
			
			eMissionObjective = stageAbattoirShootout
		BREAK
		CASE replayMichaelEscape
			//Michael
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT(0)
			ENDWHILE
			
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vPlayerStart, fPlayerStart)
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 19, 0)
			
			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)
			
			SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED_ID(), TLR_NEVER_LOSE_TARGET)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), TRUE)
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_RunFromFiresAndExplosions, FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			CLEAR_AREA(vPlayerStart, 500.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<-26.816481, -1446.621826, 44.654160>> - <<30.0, 30.0, 30.0>>, <<-26.816481, -1446.621826, 44.654160>> + <<30.0, 30.0, 30.0>>)
			
			CLEAR_AREA(<<941.6578, -2181.0586, 29.5517>>, 1000.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<966.108276, -2149.353271, 55.977444>> - <<60.0, 70.0, 30.0>>, <<966.108276, -2149.353271, 55.977444>> + <<60.0, 70.0, 30.0>>)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			
			//ADD_ARMOUR_TO_PED(PLAYER_PED(CHAR_MICHAEL), 200)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
			ENDIF
			
			//Franklin
			SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PUMPSHOTGUN, 50)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE , 200, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED,  TRUE)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Rail
			SET_RAIL_FINAL_NODE(31)
			
			//Hook
			objHook = CREATE_OBJECT_NO_OFFSET(PROP_LD_HOOK, sRailNodes[31].vPos)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHook, FALSE)
			FREEZE_ENTITY_POSITION(objHook, TRUE)
			RETAIN_ENTITY_IN_INTERIOR(objHook, intAbattoir)
			
			objChain = CREATE_OBJECT_NO_OFFSET(PROP_CS_LEG_CHAIN_01, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, 0.01>>)
			ATTACH_ENTITY_TO_ENTITY(objChain, objHook, -1, vChainOffset, vChainRotation)
			
			objPadlock = CREATE_OBJECT_NO_OFFSET(PROP_CS_PADLOCK, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, -0.01>>)
			ATTACH_ENTITY_TO_ENTITY(objPadlock, objHook, -1, vPadlockOffset, vPadlockRotation)
			
			//Particles
			
			//Cam
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ENDIF
			
			//Reset gameplay camera to third person, see B*1993022
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_NEAR)
				PRINTLN("RESETTING GAMEPLAY CAM VIEW MODE TO CAM_VIEW_MODE_THIRD_PERSON_NEAR 1")
			ENDIF
			
			eMissionObjective = stageMichaelEscape
		BREAK
		CASE replayMichaelFree
			//Michael
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT(0)
			ENDWHILE
			
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vPlayerStart, fPlayerStart)
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 19, 0)
			
			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)
			
			SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED_ID(), TLR_NEVER_LOSE_TARGET)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), TRUE)
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_RunFromFiresAndExplosions, FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			CLEAR_AREA(vPlayerStart, 500.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<-26.816481, -1446.621826, 44.654160>> - <<30.0, 30.0, 30.0>>, <<-26.816481, -1446.621826, 44.654160>> + <<30.0, 30.0, 30.0>>)
			
			CLEAR_AREA(<<941.6578, -2181.0586, 29.5517>>, 1000.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<966.108276, -2149.353271, 55.977444>> - <<60.0, 70.0, 30.0>>, <<966.108276, -2149.353271, 55.977444>> + <<60.0, 70.0, 30.0>>)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
			ENDIF
			
			//Franklin
			SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PUMPSHOTGUN, 50)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE , 200, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED,  TRUE)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Rail
			SET_RAIL_FINAL_NODE(31)
			
			//Hook
			objHook = CREATE_OBJECT_NO_OFFSET(PROP_LD_HOOK, sRailNodes[31].vPos)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHook, FALSE)
			FREEZE_ENTITY_POSITION(objHook, TRUE)
			RETAIN_ENTITY_IN_INTERIOR(objHook, intAbattoir)
			
			objChain = CREATE_OBJECT_NO_OFFSET(PROP_CS_LEG_CHAIN_01, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, 0.01>>)
			ATTACH_ENTITY_TO_ENTITY(objChain, objHook, -1, vChainOffset, vChainRotation)
			
			objPadlock = CREATE_OBJECT_NO_OFFSET(PROP_CS_PADLOCK, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, -0.01>>)
			ATTACH_ENTITY_TO_ENTITY(objPadlock, objHook, -1, vPadlockOffset, vPadlockRotation)
			
			//Particles
			
			//Cam
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ENDIF
			
			eMissionObjective = stageMichaelFree
		BREAK
		CASE replayTriadsChase
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				//Michael
				REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				
				WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
					WAIT(0)
				ENDWHILE
				
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, vPlayerStart, fPlayerStart)
					WAIT(0)
				ENDWHILE
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				//Michael
				REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				
				WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
					WAIT(0)
				ENDWHILE
				
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vPlayerStart, fPlayerStart)
					WAIT(0)
				ENDWHILE
			#IF IS_DEBUG_BUILD
			ELSE
				SCRIPT_ASSERT("Warning - The player is not Michael or Franklin!")
			#ENDIF
			ENDIF
			
			SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED_ID(), TLR_NEVER_LOSE_TARGET)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), TRUE)
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_RunFromFiresAndExplosions, FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			CLEAR_AREA(vPlayerStart, 500.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<-26.816481, -1446.621826, 44.654160>> - <<30.0, 30.0, 30.0>>, <<-26.816481, -1446.621826, 44.654160>> + <<30.0, 30.0, 30.0>>)
			
			CLEAR_AREA(<<941.6578, -2181.0586, 29.5517>>, 1000.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<966.108276, -2149.353271, 55.977444>> - <<60.0, 70.0, 30.0>>, <<966.108276, -2149.353271, 55.977444>> + <<60.0, 70.0, 30.0>>)
			
			//ADD_ARMOUR_TO_PED(NOT_PLAYER_PED_ID(), 200)
			
			//Michael
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED(CHAR_MICHAEL), PED_COMP_TORSO, 19, 0)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
			ENDIF
			
			//Franklin
			SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PUMPSHOTGUN, 50)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE , 200, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED,  TRUE)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Car
			REQUEST_MODEL(COQUETTE)
			
			WHILE NOT HAS_MODEL_LOADED(COQUETTE)
				WAIT(0)
			ENDWHILE
			
			vehHachiRoku = CREATE_VEHICLE(COQUETTE, <<950.4739, -2104.9517, 29.6107>>, 106.1659)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehHachiRoku, TRUE)
			SET_VEHICLE_COLOURS(vehHachiRoku, 4, 0)
			SET_VEHICLE_EXTRA_COLOURS(vehHachiRoku, 0, 0)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehHachiRoku)
			SET_VEHICLE_TYRES_CAN_BURST(vehHachiRoku, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(COQUETTE)
			
			//Car
			REQUEST_MODEL(FELTZER2)
			
			WHILE NOT HAS_MODEL_LOADED(FELTZER2)
				WAIT(0)
			ENDWHILE
			
			vehEscape = CREATE_VEHICLE(FELTZER2, <<953.9548, -2113.3518, 29.5516>>, 88.1350)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEscape, TRUE)
			SET_VEHICLE_COLOURS(vehEscape, 38, 0)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehEscape)
			SET_VEHICLE_TYRES_CAN_BURST(vehEscape, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(FELTZER2)
			
			//Cam
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ENDIF
			
			eMissionObjective = stageTriadsChase
		BREAK
		CASE replayCutEnd
			//Michael
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT(0)
			ENDWHILE
			
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<-860.1198, 157.7610, 63.9212>>, 339.3617)
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_TORSO, 19, 0)
			
			SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED_ID(), TLR_NEVER_LOSE_TARGET)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), TRUE)
			
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_RunFromFiresAndExplosions, FALSE)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			CLEAR_AREA(vPlayerStart, 500.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<-26.816481, -1446.621826, 44.654160>> - <<30.0, 30.0, 30.0>>, <<-26.816481, -1446.621826, 44.654160>> + <<30.0, 30.0, 30.0>>)
			
			CLEAR_AREA(<<941.6578, -2181.0586, 29.5517>>, 1000.0, TRUE)
			ADD_SCENARIO_BLOCKING_AREA(<<966.108276, -2149.353271, 55.977444>> - <<60.0, 70.0, 30.0>>, <<966.108276, -2149.353271, 55.977444>> + <<60.0, 70.0, 30.0>>)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			
			//ADD_ARMOUR_TO_PED(PLAYER_PED(CHAR_MICHAEL), 200)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
			ENDIF
			
			//Franklin
			SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PUMPSHOTGUN, 50)
//			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE , 200, TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED,  TRUE)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			eMissionObjective = stageCutEnd
		BREAK
	ENDSWITCH
	
	END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
	
	bSkipped = TRUE
	bReplaySkip = TRUE
	
//	IF IS_SCREEN_FADED_OUT()
//		DO_SCREEN_FADE_IN(1000)
//	ENDIF
ENDPROC

PROC LOAD_UNLOAD_ASSETS()
	//Stages Reference
		//stageFindMichael
		//passMission
		//failMission	
	
	//Mission Peds:
		//GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
		//GET_PLAYER_PED_MODEL(CHAR_FRANKLIN)
		//G_M_M_CHIGOON_01
		//G_M_M_CHIGOON_02
		
	//Mission Cars:
		//BISON	
		//FELTZER2
		//COQUETTE
		//BURRITO
		
	//Mission Props:
		//PROP_LD_HOOK
		//PROP_CS_PADLOCK
		//PROP_CS_LEG_CHAIN_01
				
	//Mission Recordings:
		IF eMissionObjective >= stageCutIntro
		AND eMissionObjective <= stageFindMichael
			HAS_RECORDING_LOADED_CHECK(001, "ALrollingstart")
		ENDIF
		
	//Request Models
		
	//Vehicle Recordings
		
	//Waypoint Recordings
		
	//Request Model
		HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		
		HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
		
		IF eMissionObjective >= stageFindMichael
		AND eMissionObjective <= stageTriadsChase
			HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_01)
			HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_02)
			HAS_MODEL_LOADED_CHECK(CSB_CHIN_GOON)
		ENDIF
		
		IF eMissionObjective >= stageFindMichael
		AND eMissionObjective <= stageMichaelFree
			HAS_MODEL_LOADED_CHECK(PROP_LD_HOOK)
			HAS_MODEL_LOADED_CHECK(PROP_CS_PADLOCK)
			HAS_MODEL_LOADED_CHECK(PROP_CS_LEG_CHAIN_01)
		ENDIF
		
		IF eMissionObjective >= stageFindMichael
		AND eMissionObjective <= stageAbattoirShootout
			HAS_MODEL_LOADED_CHECK(BISON)
		ENDIF
		
		IF eMissionObjective >= stageMichaelEscape
		AND eMissionObjective <= stageTriadsChase
			HAS_MODEL_LOADED_CHECK(FELTZER2)
		ENDIF
		
		IF eMissionObjective >= stageMichaelEscape
		AND eMissionObjective <= stageTriadsChase
			HAS_MODEL_LOADED_CHECK(COQUETTE)
		ENDIF
		
		IF eMissionObjective >= stageAbattoirShootout
		AND eMissionObjective <= stageMichaelFree
			HAS_MODEL_LOADED_CHECK(PROP_WATERCRATE_01)
		ENDIF
		
//		HAS_MODEL_LOADED_CHECK(BURRITO)
		
		IF eMissionObjective >= stageAbattoirShootout
		AND eMissionObjective <= stageMichaelFree
			HAS_MODEL_LOADED_CHECK(GET_WEAPONTYPE_MODEL(WEAPONTYPE_COMBATPISTOL))
			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_COMBATPISTOL)
		ENDIF
		
		IF eMissionObjective >= stageSwitchToMichael
		AND eMissionObjective <= stageMichaelFree
			HAS_MODEL_LOADED_CHECK(V_IND_COO_HALF)
		ENDIF
		
	
	//Animation Dictionaries
		IF eMissionObjective >= stageCutIntro
		AND eMissionObjective <= stageMichaelFree
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2Hook)
		ENDIF
		
		IF eMissionObjective >= stageAbattoirShootout
		AND eMissionObjective <= stageSwitchToMichael
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Switch)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2Switch)
		ENDIF
		
		IF eMissionObjective >= stageFindMichael
		AND eMissionObjective <= stageAbattoirShootout
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece1)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2SetPiece1)
		ENDIF
		
		IF eMissionObjective >= stageMichaelEscape
		AND eMissionObjective <= stageMichaelFree
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece2)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2SetPiece2)
		ENDIF
		
		IF eMissionObjective >= stageCutIntro
		AND eMissionObjective <= stageAbattoirShootout
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Smoking)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2Smoking)
		ENDIF
		
		IF eMissionObjective >= stageBackToMichaels
		AND eMissionObjective <= stageCutEnd
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2WashFace)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2WashFace)
		ENDIF
		
		IF eMissionObjective >= stageMichaelEscape
		AND eMissionObjective <= stageMichaelFree
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2FranklinBeckon)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2FranklinBeckon)
		ENDIF
		
		IF eMissionObjective >= stageCutIntro
		AND eMissionObjective <= stageFindMichael
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle1)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle2)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle3)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle4)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictIdle1)
			UNLOAD_ANIM_DICT(sAnimDictIdle2)
			UNLOAD_ANIM_DICT(sAnimDictIdle3)
			UNLOAD_ANIM_DICT(sAnimDictIdle4)
		ENDIF
		
		IF eMissionObjective = stageMichaelEscape
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2IG11)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictMic2IG11)
		ENDIF
		
		IF eMissionObjective >= stageFindMichael
		AND eMissionObjective <= stageAbattoirShootout
			IF NOT HAS_CLIP_SET_LOADED("move_ped_strafing")
				REQUEST_CLIP_SET("move_ped_strafing")
			ENDIF
		ENDIF
		
		IF eMissionObjective >= stageCutIntro
		AND eMissionObjective <= stageTriadsChase
			IF NOT HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
				REQUEST_CLIP_SET("MOVE_STRAFE@COP")
			ENDIF
		ENDIF
		
		IF eMissionObjective >= stageAbattoirShootout
		AND eMissionObjective <= stageMichaelFree
			IF NOT HAS_CLIP_SET_LOADED(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
				REQUEST_CLIP_SET(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
			ENDIF
//		ELSE
//			IF HAS_CLIP_SET_LOADED(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
//				REMOVE_CLIP_SET(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
//			ENDIF
		ENDIF
	
	//Audio
//		IF eMissionObjective >= stageFindMichael
			REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Kidnap_Rail")
//		ENDIF
ENDPROC

PROC SET_PED_ON_GROUND_PROPERLY(PED_INDEX &pedIndex)
	VECTOR vGround = GET_ENTITY_COORDS(pedIndex)
	
	GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(pedIndex), vGround.Z)
	
	SET_PED_POSITION(pedIndex, vGround, GET_ENTITY_HEADING(pedIndex))
ENDPROC

//PURPOSE: Stores the details of the currently active cutscene cam while Michael is attached to the rail
PROC GET_ACTIVE_MICHAEL_CAM_DETAILS(VECTOR &vPos, VECTOR &vRot, FLOAT &fFov)
	BOOL bGotDetails = FALSE
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF DOES_CAM_EXIST(camMain)
			IF IS_CAM_ACTIVE(camMain)
				vPos = GET_CAM_COORD(camMain)
				vRot = GET_CAM_ROT(camMain)
				fFov = GET_CAM_FOV(camMain)
				bGotDetails = TRUE
			ENDIF
		ENDIF
	ELSE
		bGotDetails = TRUE
	ENDIF
	
	IF NOT bGotDetails
		PRINTLN("eMichaelHostageProg: ", ENUM_TO_INT(eMissionObjective))
		SCRIPT_ASSERT("GET_ACTIVE_MICHAEL_CAM_DETAILS: Didn't get cam details")
	ENDIF
ENDPROC

PROC SWITCH_CONTROL()
	#IF IS_DEBUG_BUILD	
	IF DOES_CAM_EXIST(sCamDetails.camID)
		PRINTLN("SPLINE PHASE = ", GET_CAM_SPLINE_PHASE(sCamDetails.camID))
	ENDIF
	#ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DeathSwitch")
		IF iCurrentNode > 31
		AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_LABEL_AS_TRIGGERED("DeathSwitch", TRUE)
		ENDIF
	ENDIF
	
	IF ((bOkToSwitch
	AND iCurrentNode <= 31)
	OR HAS_LABEL_BEEN_TRIGGERED("DeathSwitch"))
		IF NOT sCamDetails.bRun
			BOOL bSwitchPedSelected
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DeathSwitchOnce")
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
					IF UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE)
						bSwitchPedSelected = TRUE
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
					IF (NOT IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<974.402039, -2147.449463, 49.164970>>, <<67.25, 64.5, 20.0>>)
					AND UPDATE_SELECTOR_HUD(sSelectorPeds)) //Returns TRUE when the player has made a selection
					OR HAS_LABEL_BEEN_TRIGGERED("DeathSwitch")
						IF HAS_LABEL_BEEN_TRIGGERED("DeathSwitch")
							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
							
							SET_LABEL_AS_TRIGGERED("DeathSwitchOnce", TRUE)
						ENDIF
						
						VEHICLE_INDEX vehBringToHalt
						
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
							vehBringToHalt = GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_FRANKLIN))
						ENDIF
						
						IF DOES_ENTITY_EXIST(vehBringToHalt)
						AND NOT IS_ENTITY_DEAD(vehBringToHalt)
							BRING_VEHICLE_TO_HALT(vehBringToHalt, DEFAULT_VEH_STOPPING_DISTANCE, 1, TRUE)
						ENDIF
						
						bSwitchPedSelected = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bSwitchPedSelected = TRUE
				IF IS_CELLPHONE_TRACKIFY_IN_USE()
					bPhoneSwitch = IS_PHONE_ONSCREEN(TRUE)
				ENDIF
				
				//Stats
				INFORM_MISSION_STATS_OF_INCREMENT(MIC2_TIMES_SWITCHED)
				
				IF NOT bDoMikeToFranklinCustomSwitch
					CLEAR_TEXT()
					KILL_ANY_CONVERSATION()
				ELSE
					CLEAR_PRINTS() 
					SAFE_CLEAR_HELP(TRUE)
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					bFranklinToMichael = TRUE
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF NOT HAS_LABEL_BEEN_TRIGGERED("RollingStartInit")
						IF DOES_ENTITY_EXIST(vehFranklin)
						AND NOT IS_ENTITY_DEAD(vehFranklin)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN)), <<-16.57771, -1445.89917, 29.64787>>) < 60.0
								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
								AND IS_VEHICLE_SEAT_FREE(vehFranklin)
									SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
								ENDIF
								
								IF IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
									IF NOT IS_ENTITY_AT_COORD(vehFranklin, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "ALrollingstart"), "ALrollingstart"), <<5.0, 5.0, 5.0>>)
										SET_VEHICLE_POSITION(vehFranklin, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "ALrollingstart"), "ALrollingstart"), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(001, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "ALrollingstart"), "ALrollingstart")))
									ENDIF
								ENDIF
								
								IF IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
									START_PLAYBACK_RECORDED_VEHICLE(vehFranklin, 001, "ALrollingstart")	PRINTLN("START_PLAYBACK_RECORDED_VEHICLE(vehFranklin, 001, 'ALrollingstart')")
									SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehFranklin, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "ALrollingstart") - 4000.0)
									SET_PLAYBACK_SPEED(vehFranklin, 0.0)
									SET_VEHICLE_ENGINE_ON(vehFranklin, TRUE, TRUE)
								ENDIF
							ENDIF
							
							SET_LABEL_AS_TRIGGERED("RollingStartInit", TRUE)
						ENDIF
					ENDIF
					
					bFranklinToMichael = FALSE
				ENDIF
				
				sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
				sCamDetails.bRun = TRUE
			ENDIF
		ENDIF
		
		IF sCamDetails.bRun
			BOOL bSwapping = FALSE
			IF HAS_LABEL_BEEN_TRIGGERED("MCH2_ABATTF")
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<969.710876, -2070.853760, 28.733320>>, <<958.640198, -2220.841797, 39.551666>>, 100.0)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioSwitched")
					PLAY_AUDIO(MIC2_SWITCHED)
					
					SET_LABEL_AS_TRIGGERED("AudioSwitched", TRUE)
				ENDIF
				
				IF bFranklinToMichael = TRUE
					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails)	//, 0, 0, SELECTOR_CAM_DEFAULT, 0)
						bSwapping = TRUE
					ENDIF
				ELSE
					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails) //Returns FALSE when the camera spline is complete
						bSwapping = TRUE
					ENDIF
				ENDIF
				
//				IF IS_SCREEN_FADED_IN()
//					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//						IF IS_CAM_ACTIVE(sCamDetails.camID)
//							DO_SCREEN_FADE_OUT(500)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF IS_SCREEN_FADED_OUT()
//					IF IS_GAMEPLAY_CAM_RENDERING()
//						DO_SCREEN_FADE_IN(500)
//					ELSE
//						SET_CAM_SPLINE_PHASE(sCamDetails.camID, 1.0)
//						
//						SET_CAM_ACTIVE(sCamDetails.camID, FALSE)
//						
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//					ENDIF
//				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchCamera")
					IF bFranklinToMichael = FALSE
						IF NOT DOES_CAM_EXIST(camSwitch)
							IF DOES_ENTITY_EXIST(vehFranklin)
							AND NOT IS_ENTITY_DEAD(vehFranklin)
							AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN)), <<-16.57771, -1445.89917, 29.64787>>) < 60.0
									IF NOT DOES_CAM_EXIST(camSwitch)
										camSwitch = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
									ENDIF
									
									SET_CAM_PARAMS(camSwitch, 
													<<-108.298317, -1512.187012, 52.940929>>,
													<<-89.499535, -0.000218, 142.212173>>,
													45.0, 
													0)
								ENDIF
							ENDIF
							
							SET_LABEL_AS_TRIGGERED("SwitchCamera", TRUE)
						ENDIF
					ENDIF
				ENDIF
			
				IF NOT bDoMikeToFranklinCustomSwitch
					PRINTLN("bFranklinToMichael = ", bFranklinToMichael)
					
					IF (bFranklinToMichael = TRUE
					AND RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails, ENUM_TO_INT(SWITCH_FLAG_SKIP_OUTRO)))
						bSwapping = TRUE	PRINTLN("RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails, ENUM_TO_INT(SWITCH_FLAG_SKIP_OUTRO))")
					ENDIF
					
					IF bFranklinToMichael = FALSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("RollingStartStop")
							IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM_LONG_RANGE(sCamDetails, camSwitch, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO))
								bSwapping = TRUE	
								PRINTLN("RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM_LONG_RANGE(sCamDetails, camSwitch, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO)) - bSwapping = TRUE")
							ENDIF
						ELSE				
							IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO))
								bSwapping = TRUE	
								PRINTLN("(RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO) - bSwapping = TRUE")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_FRANKLIN)
						bMikeToFranklinSwitchTriggered = TRUE
					ENDIF
					
					IF bMikeToFranklinSwitchTriggered
						IF DOES_ENTITY_EXIST(vehFranklin) AND IS_VEHICLE_DRIVEABLE(vehFranklin)
							SET_FORCE_HD_VEHICLE(vehFranklin, TRUE)
						ENDIF
						
						IF HAS_LABEL_BEEN_TRIGGERED("RollingStartStop")
						OR (DOES_ENTITY_EXIST(vehFranklin) AND IS_VEHICLE_DRIVEABLE(vehFranklin) AND IS_VEHICLE_HIGH_DETAIL(vehFranklin))	PRINTLN("IS_VEHICLE_HIGH_DETAIL(vehFranklin) = TRUE")
							IF HANDLE_SWITCH_CAM_MICHAEL_TO_FRANKLIN(scsSwitchCam_MichaelToFranklin)
								bSwapping = FALSE
								bMikeToFranklinSwitchTriggered = FALSE
								bDoMikeToFranklinCustomSwitch = FALSE
							ELSE
								IF bMikeToFrank_SwitchToFrank
									IF NOT bMikeToFrank_SwitchedToFrank
										SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED(CHAR_FRANKLIN), FALSE)
										
										CLEAR_FOCUS()
										
										REMOVE_NAVMESH_REQUIRED_REGIONS()
										
										IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)	//, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
											INT i
											
											REPEAT COUNT_OF(sEnemyIntroCut) i
												SAFE_DELETE_PED(sEnemyIntroCut[i].pedIndex)
											ENDREPEAT
											
											bMikeToFrank_SwitchedToFrank = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bSwapping = TRUE
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						TAKE_CONTROL_OF_PED_FLAGS eTakeControlFlags = TCF_NONE
						
						IF bFranklinToMichael
							eTakeControlFlags = TCF_CLEAR_TASK_INTERRUPT_CHECKS
						ENDIF
						
						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, eTakeControlFlags)	#IF IS_DEBUG_BUILD	PRINTLN("Michael 2 - TAKE_CONTROL_OF_SELECTOR_PED")	#ENDIF
							//Create or delete intro enemies
							IF eMissionObjective = stageFindMichael
								IF bFranklinToMichael = FALSE
									INT i
									
									REPEAT COUNT_OF(sEnemyIntroCut) i
										SAFE_DELETE_PED(sEnemyIntroCut[i].pedIndex)
									ENDREPEAT
								ELSE
									//Enemy Intro (Inside)
									createEnemy(sEnemyIntroCut[0], <<996.9860, -2185.0713, 28.9775>>, 9.6956, WEAPONTYPE_SMG, G_M_M_CHIGOON_01)	//Original Position: <<991.6176, -2171.4707, 29.2246>>, 270.7684
									SET_PED_DEFAULT_COMPONENT_VARIATION(sEnemyIntroCut[0].pedIndex)
									SET_MODEL_AS_NO_LONGER_NEEDED(CSB_CHIN_GOON)
									createEnemy(sEnemyIntroCut[1], <<992.2509, -2175.3486 -0.4, 28.9769>>, 242.3, WEAPONTYPE_PISTOL, G_M_M_CHIGOON_01)
									createEnemy(sEnemyIntroCut[2], <<988.6379, -2170.6748, 29.2006>>, 352.4113, WEAPONTYPE_PISTOL)
									createEnemy(sEnemyIntroCut[3], <<977.6, -2166.4, 30.4104 + 0.25>>, 70.2, WEAPONTYPE_UNARMED)
									createEnemy(sEnemyIntroCut[4], <<970.7029, -2169.6233, 28.4637>>, 170.9878, WEAPONTYPE_UNARMED, G_M_M_CHIGOON_01)
									createEnemy(sEnemyIntroCut[5], <<977.9589, -2156.2410, 30.0791>>, 125.1269, WEAPONTYPE_UNARMED, G_M_M_CHIGOON_01)
									createEnemy(sEnemyIntroCut[6], <<984.0753, -2158.3696, 30.0793>>, 219.8727, WEAPONTYPE_PISTOL)
									createEnemy(sEnemyIntroCut[7], <<997.6616, -2157.8818, 28.4766>>, 265.7728, WEAPONTYPE_MICROSMG, G_M_M_CHIGOON_01)
									createEnemy(sEnemyIntroCut[8], <<999.5024, -2145.7878, 28.4765>>, 275.3511, WEAPONTYPE_SMG) //Patrol: <<969.6483, -2160.4150, 28.4750>>, 178.3322
									createEnemy(sEnemyIntroCut[9], <<969.2003, -2164.1885, 28.4756>>, 187.7889, WEAPONTYPE_SMG, G_M_M_CHIGOON_01) //Patrol: <<997.3615, -2168.5959, 28.4752>>, 358.6634
									
									INT i
									
									REPEAT COUNT_OF(sEnemyIntroCut) i
										IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
											//RETAIN_ENTITY_IN_INTERIOR(sEnemyIntroCut[i].pedIndex, intAbattoir)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyIntroCut[i].pedIndex, TRUE)
										ENDIF
									ENDREPEAT
									
									#IF IS_DEBUG_BUILD
									REPEAT COUNT_OF(sEnemyIntroCut) i
										IF NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
											ASSIGN_DEBUG_NAMES(sEnemyIntroCut[i].pedIndex, "IntroCut ", i)
											sEnemyIntroCut[i].sDebugName = CONCATENATE_STRINGS("IntroCut ", GET_STRING_FROM_INT(i))
										ENDIF
									ENDREPEAT
									#ENDIF
								ENDIF
							ENDIF
							
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
							IF DOES_CAM_EXIST(camSwitch)
								DESTROY_CAM(camSwitch)
							ENDIF
							
							SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED(CHAR_MICHAEL), TLR_NEVER_LOSE_TARGET)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), TRUE)
							
							//[MF] Fix for Michael being invisible when we switch to him.
//							IF eMissionObjective = stageFindMichael
//								IF PLAYER_PED_ID() = PLAYER_PED(CHAR_MICHAEL)
//									WHILE NOT IS_INTERIOR_READY(intAbattoir)
//										PRINTLN("Loading Interior....")
//										WAIT(0)
//									ENDWHILE
//									FORCE_ROOM_FOR_ENTITY(PLAYER_PED(CHAR_MICHAEL), intAbattoir, GET_ROOM_KEY_FROM_ENTITY(objHook)) 
//								ENDIF
//							ENDIF							
							
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupFriendlyFire)
							ENDIF
							
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
							ENDIF
							
							REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
							REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
							
							IF eMissionObjective = stageFindMichael
								
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, NULL, "MICHAEL")
							ELSE
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
							ENDIF
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
							
							//Interior
							IF intAbattoir = NULL
								intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(vPlayerStart, "v_abattoir")
							ENDIF
							
							RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED(CHAR_MICHAEL), intAbattoir)
							
							sCamDetails.bPedSwitched = TRUE
						ENDIF
					ELSE
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RAIL_STOP_MOVING(INT iNextNodeToMoveTo)
	//Stop moving the rail if we've reached the maximum number of nodes,
	//if the next node hasn't been defined (i.e. it's position is at the origin),
	//or if the next node is a pre-determined stop point (iRailNodeToStopAt)
	RETURN((iNextNodeToMoveTo = iTotalRailNodes)
	OR (iNextNodeToMoveTo = iRailNodeToStopAt)
	OR (ARE_VECTORS_EQUAL(sRailNodes[iNextNodeToMoveTo].vPos, VECTOR_ZERO)))
ENDFUNC

///PURPOSE: Controls moving the ped dangling from the hook along the rail. This version uses the new DANGLE_FROM_MEATHOOK behaviour to update the position on the rail
FUNC BOOL MOVE_PED_ALONG_RAIL()
	VECTOR vHookCurLoc
	VECTOR vHookToNextNode
	VECTOR vPotentialNextPos
	
	INT iNextNodeIndex = iCurrentNode + 1
	
	IF NOT DOES_ENTITY_EXIST(objHook)
		SCRIPT_ASSERT("Hook doesn't exist!")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
		SCRIPT_ASSERT("Rail ped is dead!")
	ENDIF
	
	IF bRailInit = FALSE
		iCurrentNode = 0
		vRailCurrent = sRailNodes[0].vPos
		iNextNodeIndex = iCurrentNode + 1
		PRINTLN("Initialised Rail Node Variables...")
		bRailInit = TRUE
	ENDIF
	
	IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(objHook)
		PRINTLN("objHook WAITING FOR COLLISION...")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDebugMichaelDeathSkip
		SET_RAIL_FINAL_NODE(40)
		iCurrentNode = 33
		vRailCurrent = sRailNodes[33].vPos
		SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
		SET_ENTITY_ROTATION(objHook, sRailNodes[33].vRot)
		iCutsceneStage = 22
		SETTIMERA(13000 * 2)
		
		bDebugMichaelDeathSkip = FALSE
	ENDIF
	
	IF bDebugMichaelSkipToStart
		SET_RAIL_FINAL_NODE(40)
		iCurrentNode = 0
		vRailCurrent = sRailNodes[0].vPos
		SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
		SET_ENTITY_ROTATION(objHook, sRailNodes[0].vRot)
		iCutsceneStage = 2
		SETTIMERA(0)
		
		bDebugMichaelSkipToStart = FALSE
	ENDIF
	
	IF bDebugMichaelSkipToCapture
		SET_RAIL_FINAL_NODE(40)
		iCurrentNode = iCaptureNode
		vRailCurrent = vCaptureCurrent
		SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
		SET_ENTITY_ROTATION(objHook, vRailCurrentRot)
		iCutsceneStage = iCaptureStage
		SETTIMERA(iCaptureTimer)
		
		bDebugMichaelSkipToCapture = FALSE
	ENDIF
	
	IF bDebugMichaelCapturePoint
		iCaptureNode = iCurrentNode
		vCaptureCurrent = vRailCurrent
		vRailCurrentRot = GET_ENTITY_ROTATION(objHook)
		iCaptureStage =	iCutsceneStage
		iCaptureTimer = TIMERA()
		
		bDebugMichaelCapturePoint = FALSE
	ENDIF
	
	IF bDebugMichaelPausePlay
		IF fMoveSpeedMult <> 0.225
			fMoveSpeedMult = 0.225
			iDebugMichaelDebugTime = -1
		ELSE
			fMoveSpeedMult = 0.0
			iDebugMichaelDebugTime = TIMERA()
		ENDIF
		
		bDebugMichaelPausePlay = FALSE
	ENDIF
	
	IF iDebugMichaelDebugTime <> -1
		SETTIMERA(iDebugMichaelDebugTime)
	ENDIF
	
	IF bDebugMichaelDebugTime
		TEXT_LABEL txtLabel
		txtLabel = "Time: "
		txtLabel += TIMERA()
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_CENTRE(TRUE)
		IF fMoveSpeedMult <> 0.225
			SET_TEXT_COLOUR(255, 0, 0, 255)
		ELSE
			SET_TEXT_COLOUR(255, 255, 0, 255)
		ENDIF
		DISPLAY_TEXT(0.1, 0.9, txtLabel)
		
		txtLabel = "Stage: "
		txtLabel += iCutsceneStage
		SET_TEXT_SCALE(0.5, 0.5)
		SET_TEXT_CENTRE(TRUE)
		IF fMoveSpeedMult <> 0.225
			SET_TEXT_COLOUR(255, 0, 0, 255)
		ELSE
			SET_TEXT_COLOUR(255, 255, 0, 255)
		ENDIF
		DISPLAY_TEXT(0.1, 0.925, txtLabel)
	ENDIF
	
	CAMERA_INDEX camDebugCurrent = GET_DEBUG_CAM()
	
	IF NOT DOES_CAM_EXIST(camDebug)
		camDebug = CREATE_CAMERA(CAMTYPE_SCRIPTED)
	ENDIF
	
	IF bDebugMichaelSetDebugCam
		SET_CAM_COORD(camDebugCurrent, GET_CAM_COORD(camDebug))
		SET_CAM_ROT(camDebugCurrent, GET_CAM_ROT(camDebug))
		SET_CAM_FOV(camDebugCurrent, GET_CAM_FOV(camDebug))
		
		bDebugMichaelSetDebugCam = FALSE
	ENDIF
	
	IF bDebugMichaelCaptureDebugCam
		SET_CAM_COORD(camDebug, GET_CAM_COORD(camDebugCurrent))
		SET_CAM_ROT(camDebug, GET_CAM_ROT(camDebugCurrent))
		SET_CAM_FOV(camDebug, GET_CAM_FOV(camDebugCurrent))
		
		bDebugMichaelCaptureDebugCam = FALSE
	ENDIF
	
	IF iDebugMichaelHandCamLast <> iDebugMichaelHandCam
		IF iDebugMichaelHandCam = 0	//NONE
			IF IS_CAM_SHAKING(camDebugCurrent)
				STOP_CAM_SHAKING(camDebugCurrent, TRUE)
			ENDIF
		ELIF iDebugMichaelHandCam = 1	//"light"
			IF HAS_ANIM_DICT_LOADED("shake_cam_all@")
				ANIMATED_SHAKE_CAM(camDebugCurrent, "shake_cam_all@", "light", "", fDebugMichaelHandCam)
			ENDIF
		ELIF iDebugMichaelHandCam = 2	//"medium"
			IF HAS_ANIM_DICT_LOADED("shake_cam_all@")
				ANIMATED_SHAKE_CAM(camDebugCurrent, "shake_cam_all@", "medium", "", fDebugMichaelHandCam)
			ENDIF
		ELIF iDebugMichaelHandCam = 3	//"heavy"
			IF HAS_ANIM_DICT_LOADED("shake_cam_all@")
				ANIMATED_SHAKE_CAM(camDebugCurrent, "shake_cam_all@", "heavy", "", fDebugMichaelHandCam)
			ENDIF
		ENDIF
		
		iDebugMichaelHandCamLast = iDebugMichaelHandCam
	ENDIF
	#ENDIF
	
	vHookCurloc = vRailCurrent
	IF NOT SHOULD_RAIL_STOP_MOVING(iNextNodeIndex)
	AND fMoveSpeedMult <> 0.0
		vHookToNextNode = sRailNodes[iNextNodeIndex].vPos - vHookCurLoc
		vPotentialNextPos = vHookCurLoc +((GET_FRAME_TIME() * NORMALISE_VECTOR(vHookToNextNode) * fMoveSpeedMult))
		vRailCurrent = vPotentialNextPos
		SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
		IF GET_DISTANCE_BETWEEN_COORDS(vPotentialNextPos, sRailNodes[iNextNodeIndex].vPos)
		< GET_DISTANCE_BETWEEN_COORDS(vHookCurloc, sRailNodes[iNextNodeIndex].vPos)
//			IF GET_ENABLE_BOUND_ANKLES(PLAYER_PED(CHAR_MICHAEL))
//				vRailCurrent = vPotentialNextPos
//				FREEZE_ENTITY_POSITION(objHook, FALSE)
//				DANGLE_FROM_MEATHOOK(FALSE, PLAYER_PED(CHAR_MICHAEL), objHook, vRailCurrent + vHookPosOffset, FALSE, 0.0, 1000, 60000, bFixedRotation, -0.3, -0.3, -0.3, 0.3, 0.3, 0.3)	//#IF IS_DEBUG_BUILD	PRINTLN("DANGLE_FROM_MEATHOOK REF1")	#ENDIF
////				PRINTLN("Hook should be at: <<", vRailCurrent.X, ", ", vRailCurrent.Y, ", ", vRailCurrent.Z, ">>")
//			ELSE
//				SET_ENABLE_BOUND_ANKLES(PLAYER_PED(CHAR_MICHAEL), TRUE)
//				PRINTLN("Bind those ankles!")
//			ENDIF
		ELSE
			PRINTLN("iCurrentNode(", iCurrentNode, ") = iNextNodeIndex(", iNextNodeIndex, ")")
			
			iCurrentNode = iNextNodeIndex
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iLastAnim = 2	//Michael_meat_hook_react_c

///PURPOSE: Deals with moving the rail, and making sure the ragdoll behaviour doesn't timeout
PROC UPDATE_RAIL_MOVE()
	IF bOkToMoveRail
		MOVE_PED_ALONG_RAIL()
		
		IF sfxRail = -1
			sfxRail = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(sfxRail, "RAIL_LOOP", objHook, "MICHAEL_2_SOUNDS")
		ENDIF
	ELSE
		IF sfxRail != -1
			STOP_SOUND(sfxRail)
			RELEASE_SOUND_ID(sfxRail)
			sfxRail = -1
		ENDIF
	ENDIF
	
	IF bOkToHangFromHook
		//Reattach
		IF DOES_ENTITY_EXIST(objHook)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) != PERFORMING_TASK
				IF NOT IS_ENTITY_ATTACHED(PLAYER_PED(CHAR_MICHAEL))
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
				ELSE
					IF HAS_ENTITY_ANIM_FINISHED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_a")
					OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_b")
					OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c")
					OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_d")
					OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_a")
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_a") > 0.675)
					OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_b")
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_b") > 0.8)
					OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c")
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c") > 0.685)
					OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_d")
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_d") > 0.675)
						INT iRand
						
						iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
						
						IF iRand = iLastAnim
							iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
						ENDIF
						
						IF iRand = iLastAnim
							iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
						ENDIF
						
						IF iRand = 0
							TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.025)
						ELIF iRand = 1
							TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.1)
						ELIF iRand = 2
							TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.030)
						ELIF iRand = 3
							TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_d", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.15)
						ENDIF
						
						iLastAnim = iRand
					ENDIF
				ENDIF
			ENDIF
			
//			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook)
//			OR GET_DISTANCE_BETWEEN_COORDS(GET_PED_BONE_COORDS(PLAYER_PED(CHAR_MICHAEL), BONETAG_L_FOOT, VECTOR_ZERO), GET_ENTITY_COORDS(objHook)) > 1.0
//			OR GET_DISTANCE_BETWEEN_COORDS(GET_PED_BONE_COORDS(PLAYER_PED(CHAR_MICHAEL), BONETAG_R_FOOT, VECTOR_ZERO), GET_ENTITY_COORDS(objHook)) > 1.0
//				#IF IS_DEBUG_BUILD
//				PRINTLN("Left Foot Dist: ", GET_DISTANCE_BETWEEN_COORDS(GET_PED_BONE_COORDS(PLAYER_PED(CHAR_MICHAEL), BONETAG_L_FOOT, VECTOR_ZERO), GET_ENTITY_COORDS(objHook)), "   Right Foot Dist: ", GET_DISTANCE_BETWEEN_COORDS(GET_PED_BONE_COORDS(PLAYER_PED(CHAR_MICHAEL), BONETAG_R_FOOT, VECTOR_ZERO), GET_ENTITY_COORDS(objHook)))
//				#ENDIF
//				
//				IF IS_VECTOR_ZERO(vRailCurrent)
//					vRailCurrent = sRailNodes[0].vPos
//				ENDIF
//				
//				SAFE_DELETE_OBJECT(objHook)
//				objHook = CREATE_OBJECT_NO_OFFSET(PROP_LD_HOOK, vRailCurrent)
//				
//				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHook, FALSE)
//				RETAIN_ENTITY_IN_INTERIOR(objHook, intAbattoir)
//				
//				WHILE NOT DOES_ENTITY_HAVE_PHYSICS(objHook)
//				OR NOT HAS_COLLISION_FOR_MODEL_LOADED(PROP_LD_HOOK)
//					WAIT_WITH_DEATH_CHECKS(0)
//				ENDWHILE
//				
//				SET_OBJECT_PHYSICS_PARAMS(objHook, fHookMass, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)	
//				SET_ENTITY_DYNAMIC(objHook, TRUE)
//				
//				SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
//				SET_ENTITY_ROTATION(objHook, sRailNodes[iCurrentNode].vRot)
//				FREEZE_ENTITY_POSITION(objHook, TRUE)
//				
//				//Attach Michael to hook
//				ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
//				
//				WHILE NOT IS_ENTITY_ATTACHED(PLAYER_PED(CHAR_MICHAEL))
//					WAIT_WITH_DEATH_CHECKS(0)
//				ENDWHILE
//				
//				TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)	//TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
//				
//				iReattach = 0
//				
//				bReattach = TRUE
//			ENDIF
//			
//			IF bReattach = TRUE
//				SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
//				SET_ENTITY_ROTATION(objHook, sRailNodes[iCurrentNode].vRot)
//				
//				ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
//				
//				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_idle")	//IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle")
//					IF iReattach = 0
//						#IF IS_DEBUG_BUILD
//						PRINTLN("1 second to reattach...")
//						#ENDIF
//						
//						iReattach = GET_GAME_TIMER() + 1000
//					ENDIF
//				ENDIF
//				
//				IF iReattach > 0 AND GET_GAME_TIMER() > iReattach
//				AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_idle")	//AND IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle")
//				AND NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(PLAYER_PED(CHAR_MICHAEL))
//					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
//					OR (IS_ENTITY_ON_SCREEN(PLAYER_PED(CHAR_MICHAEL)) AND NOT IS_ENTITY_OCCLUDED(PLAYER_PED(CHAR_MICHAEL)))
//						FREEZE_ENTITY_POSITION(objHook, FALSE)
//						
//						DETACH_ENTITY(PLAYER_PED(CHAR_MICHAEL))
//						DANGLE_FROM_MEATHOOK(TRUE, PLAYER_PED(CHAR_MICHAEL), objHook, vRailCurrent, FALSE, 0.0, 1000, 60000, bFixedRotation, -0.3, -0.3, -0.3, 0.3, 0.3, 0.3)	//#IF IS_DEBUG_BUILD	PRINTLN("DANGLE_FROM_MEATHOOK REF2")	#ENDIF
//						STOP_ANIM_TASK(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_idle", INSTANT_BLEND_OUT)	//STOP_ANIM_TASK(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_OUT)
//						
//						//Add ankle cuffs
//						SET_ENABLE_BOUND_ANKLES(PLAYER_PED(CHAR_MICHAEL), TRUE)
//				        
//						//Pin the ankle to the bottom of the hook
//						VECTOR vLeftFootAttach
//						vLeftFootAttach = <<-0.075, -0.1, -0.85>>
//						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_L_FOOT), -1, vLeftFootAttach, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
//						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_R_FOOT), -1, <<-vLeftFootAttach.X, vLeftFootAttach.Y, vLeftFootAttach.Z>>, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
//						
//						#IF IS_DEBUG_BUILD
//						PRINTLN("Reattached!")
//						#ENDIF
//						
//						bReattach = FALSE
//						iReattach = 0
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF NOT IS_PED_INJURED(PLAYER_PED(CHAR_MICHAEL))
//				IF IS_PED_RAGDOLL(PLAYER_PED(CHAR_MICHAEL))
//					IF IS_PED_RUNNING_RAGDOLL_TASK(PLAYER_PED(CHAR_MICHAEL))
//						RESET_PED_RAGDOLL_TIMER(PLAYER_PED(CHAR_MICHAEL))	//#IF IS_DEBUG_BUILD	PRINTLN("RESET_PED_RAGDOLL_TIMER")	#ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
//			IF bShootingUpsideDown
//				SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
//				VECTOR vFinalCam = GET_FINAL_RENDERED_CAM_ROT()
//				SET_ENTITY_ROTATION(objHook, <<sRailNodes[0].vRot.X, sRailNodes[0].vRot.Y, vFinalCam.Z>>)
//				
//				ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, <<-0.025, -0.05, -1.68>>, <<0.0, 0.0, 180.0>>)
//			ENDIF
		ENDIF
	ENDIF
ENDPROC

//#IF IS_DEBUG_BUILD
//
//CONST_INT iDebugWindow 10
//
//STRUCT DEBUG_WINDOW
//	BOOL bActive
//	FLOAT xPos
//	FLOAT yPos
//	FLOAT fWidth
//	FLOAT fHeight
//	STRING sTitle = NULL
//	BOOL bCenterOrigin = FALSE
//	BOOL bMaxBox = TRUE
//	BOOL bMinBox = TRUE
//	BOOL bCloseBox = TRUE
//	BOOL bSimple = FALSE
//	BOOL bGradient = TRUE
//	VECTOR vColourTopLeft
//	VECTOR vColourTopRight
//	VECTOR vColourBottomLeft
//	VECTOR vColourBottomRight
//ENDSTRUCT
//
//DEBUG_WINDOW sDebugWindow[iDebugWindow]
//
//PROC DRAW_DEBUG_WINDOW()
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	
//	INT i
//	
//	REPEAT COUNT_OF(sDebugWindow) i
//		IF sDebugWindow[i].bActive = TRUE
//			IF sDebugWindow[i].bSimple = FALSE
//				VECTOR vOrigin
//				
//				IF sDebugWindow[i].bCenterOrigin = FALSE
//					vOrigin = <<sDebugWindow[i].xPos, sDebugWindow[i].yPos, 0.0>>
//				ELSE
//					vOrigin = <<sDebugWindow[i].xPos - (sDebugWindow[i].fWidth / 2), sDebugWindow[i].yPos - (sDebugWindow[i].fHeight / 2), 0.0>>
//				ENDIF
//				
//				DRAW_DEBUG_POLY_2D_WITH_THREE_COLOURS(vOrigin, 															 	//Top Left
//													  vOrigin + <<sDebugWindow[i].fWidth, 0.0, 0.0>>,   				 	//Top Right
//													  vOrigin + <<0.0, sDebugWindow[i].fHeight, 0.0>>,  				 	//Bottom Left
//													  ROUND(sDebugWindow[i].vColourTopLeft.X), ROUND(sDebugWindow[i].vColourTopLeft.Y), ROUND(sDebugWindow[i].vColourTopLeft.Z),			//Top Left Colour
//													  ROUND(sDebugWindow[i].vColourTopRight.X), ROUND(sDebugWindow[i].vColourTopRight.Y), ROUND(sDebugWindow[i].vColourTopRight.Z),			//Top Right Colour
//													  ROUND(sDebugWindow[i].vColourBottomLeft.X), ROUND(sDebugWindow[i].vColourBottomLeft.Y), ROUND(sDebugWindow[i].vColourBottomLeft.Z))	//Bottom Left Colour
//				
//				DRAW_DEBUG_POLY_2D_WITH_THREE_COLOURS(vOrigin + <<0.0, sDebugWindow[i].fHeight, 0.0>>, 					 	//Bottom Left
//													  vOrigin + <<sDebugWindow[i].fWidth, 0.0, 0.0>>, 					 	//Top Right
//													  vOrigin + <<sDebugWindow[i].fWidth, sDebugWindow[i].fHeight, 0.0>>,	//Bottom Right
//													  ROUND(sDebugWindow[i].vColourBottomLeft.X), ROUND(sDebugWindow[i].vColourBottomLeft.Y), ROUND(sDebugWindow[i].vColourBottomLeft.Z),		//Top Left Colour
//													  ROUND(sDebugWindow[i].vColourTopRight.X), ROUND(sDebugWindow[i].vColourTopRight.Y), ROUND(sDebugWindow[i].vColourTopRight.Z),				//Top Right Colour
//													  ROUND(sDebugWindow[i].vColourBottomRight.X), ROUND(sDebugWindow[i].vColourBottomRight.Y), ROUND(sDebugWindow[i].vColourBottomRight.Z))	//Bottom Left Colour
//			ELSE
//				VECTOR vOrigin
//				
//				IF sDebugWindow[i].bCenterOrigin = FALSE
//					vOrigin = <<sDebugWindow[i].xPos, sDebugWindow[i].yPos, 0.0>>
//				ELSE
//					vOrigin = <<sDebugWindow[i].xPos - (sDebugWindow[i].fWidth / 2), sDebugWindow[i].yPos - (sDebugWindow[i].fHeight / 2), 0.0>>
//				ENDIF
//				
//				DRAW_DEBUG_POLY_2D_WITH_THREE_COLOURS(vOrigin, 															 	//Top Left
//													  vOrigin + <<sDebugWindow[i].fWidth, 0.0, 0.0>>,   				 	//Top Right
//													  vOrigin + <<0.0, sDebugWindow[i].fHeight, 0.0>>,  				 	//Bottom Left
//													  ROUND(sDebugWindow[i].vColourTopLeft.X), ROUND(sDebugWindow[i].vColourTopLeft.Y), ROUND(sDebugWindow[i].vColourTopLeft.Z),			//Top Left Colour
//													  ROUND(sDebugWindow[i].vColourTopRight.X), ROUND(sDebugWindow[i].vColourTopRight.Y), ROUND(sDebugWindow[i].vColourTopRight.Z),			//Top Right Colour
//													  ROUND(sDebugWindow[i].vColourBottomLeft.X), ROUND(sDebugWindow[i].vColourBottomLeft.Y), ROUND(sDebugWindow[i].vColourBottomLeft.Z))	//Bottom Left Colour
//				
//				DRAW_DEBUG_POLY_2D_WITH_THREE_COLOURS(vOrigin + <<0.0, sDebugWindow[i].fHeight, 0.0>>, 					 	//Bottom Left
//													  vOrigin + <<sDebugWindow[i].fWidth, 0.0, 0.0>>, 					 	//Top Right
//													  vOrigin + <<sDebugWindow[i].fWidth, sDebugWindow[i].fHeight, 0.0>>,	//Bottom Right
//													  ROUND(sDebugWindow[i].vColourBottomLeft.X), ROUND(sDebugWindow[i].vColourBottomLeft.Y), ROUND(sDebugWindow[i].vColourBottomLeft.Z),		//Top Left Colour
//													  ROUND(sDebugWindow[i].vColourTopRight.X), ROUND(sDebugWindow[i].vColourTopRight.Y), ROUND(sDebugWindow[i].vColourTopRight.Z),				//Top Right Colour
//													  ROUND(sDebugWindow[i].vColourBottomRight.X), ROUND(sDebugWindow[i].vColourBottomRight.Y), ROUND(sDebugWindow[i].vColourBottomRight.Z))	//Bottom Left Colour
//			ENDIF
//		ENDIF
//	ENDREPEAT
//ENDPROC
//
//#ENDIF

#IF IS_DEBUG_BUILD

//Debug Variables
//- General
BOOL bAutoUpdateSpline_SCE = TRUE
BOOL bUpdateSpline_SCE
INT iSplineCamType_SCE, iSplineCamTypeLast_SCE
INT iTotalDuration_SCE
INT iSmoothingStyle_SCE

CAMERA_INDEX camEdit_SCE
CAMERA_INDEX camEditSpline_SCE

CONST_INT iTotalNodes_SCE 20

STRUCT SPLINE_CAM_NODE_SCE
	VECTOR vPos
	VECTOR vRot
	FLOAT fFov
	INT iTime
	INT iCAM_SPLINE_NODE_FLAGS
ENDSTRUCT

SPLINE_CAM_NODE_SCE SplineCamNodes_SCE[iTotalNodes_SCE]

//- Editing
INT iCurrentNode_SCE, iCurrentNodeLast_SCE
BOOL bCaptureCurrentCamera_SCE
VECTOR vCurrentNodePos_SCE
VECTOR vCurrentNodeRot_SCE
FLOAT fCurrentNodeFOV_SCE
INT iCurrentNodeTime_SCE
INT iCurrentCAM_SPLINE_NODE_FLAGS_SCE

//- Preview
BOOL bRenderSplineCam_SCE
BOOl bStartSplineCam_SCE

//- Drawing
BOOL bDrawDebug_SCE
//BOOL bDrawCam_SCE
//BOOL bDrawNode_SCE
//BOOL bDrawPath_SCE
//BOOL bDrawSpeed_SCE
//INT iDrawPathDetail_SCE
//ENUM DRAWNODE_SCE
//	DRAWNODE_NONE_SCE,
//	DRAWNODE_POINT_SCE,
//	DRAWNODE_WIREFRAME_SCE,
//	DRAWNODE_SOLID_SCE
//ENDENUM
//DRAWNODE_SCE eDrawNode_SCE

//- Save/Load

PROC GENERATE_SPLINE_ROUTE()
	IF DOES_CAM_EXIST(camEditSpline_SCE)
		DESTROY_CAM(camEditSpline_SCE)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(camEditSpline_SCE)
		IF iSplineCamType_SCE = 0
			camEditSpline_SCE = CREATE_CAM("TIMED_SPLINE_CAMERA")
		ELIF iSplineCamType_SCE = 1
			camEditSpline_SCE = CREATE_CAM("ROUNDED_SPLINE_CAMERA")
		ELIF iSplineCamType_SCE = 2
			camEditSpline_SCE = CREATE_CAM("SMOOTHED_SPLINE_CAMERA")
		ENDIF
	ENDIF
	
	BOOL bBreak
	INT iLoop
	
	WHILE bBreak = FALSE
		IF DOES_CAM_EXIST(camEditSpline_SCE)
			IF IS_VECTOR_ZERO(SplineCamNodes_SCE[iLoop].vPos)
				bBreak = TRUE
			ELSE
				SET_CAM_PARAMS(camEdit_SCE, SplineCamNodes_SCE[iLoop].vPos, SplineCamNodes_SCE[iLoop].vRot, SplineCamNodes_SCE[iLoop].fFov)
				ADD_CAM_SPLINE_NODE_USING_CAMERA(camEditSpline_SCE, camEdit_SCE, SplineCamNodes_SCE[iLoop].iTime, INT_TO_ENUM(CAM_SPLINE_NODE_FLAGS, SplineCamNodes_SCE[iLoop].iCAM_SPLINE_NODE_FLAGS))
					
				iLoop++
				
				IF iLoop = iTotalNodes_SCE
					bBreak = TRUE
				ENDIF
			ENDIF
		ELSE
			bBreak = TRUE
		ENDIF
	ENDWHILE
	
	IF DOES_CAM_EXIST(camEditSpline_SCE)
		IF iSplineCamType_SCE = 0
			
		ELIF iSplineCamType_SCE = 1
			SET_CAM_SPLINE_SMOOTHING_STYLE(camEditSpline_SCE, INT_TO_ENUM(CAM_SPLINE_SMOOTHING_FLAGS, iSmoothingStyle_SCE + 1))
			SET_CAM_SPLINE_DURATION(camEditSpline_SCE, iTotalDuration_SCE)
		ELIF iSplineCamType_SCE = 2
			SET_CAM_SPLINE_SMOOTHING_STYLE(camEditSpline_SCE, INT_TO_ENUM(CAM_SPLINE_SMOOTHING_FLAGS, iSmoothingStyle_SCE + 1))
			SET_CAM_SPLINE_DURATION(camEditSpline_SCE, iTotalDuration_SCE)
		ENDIF
	ENDIF
ENDPROC

PROC SPLINE_CAM_EDITOR
	IF NOT DOES_CAM_EXIST(camEdit_SCE)
		camEdit_SCE = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	ENDIF
	
	IF bUpdateSpline_SCE = TRUE
		GENERATE_SPLINE_ROUTE()
		
		bUpdateSpline_SCE = FALSE
	ENDIF
	
	IF iSplineCamTypeLast_SCE <> iSplineCamType_SCE
		GENERATE_SPLINE_ROUTE()
		
		iSplineCamTypeLast_SCE = iSplineCamType_SCE
	ENDIF
	
	//ADD_CAM_SPLINE_NODE_USING_CAMERA(camEditSpline_SCE, camEdit_SCE, 0)
	
	IF bCaptureCurrentCamera_SCE = TRUE
		vCurrentNodePos_SCE = GET_FINAL_RENDERED_CAM_COORD()
		vCurrentNodeRot_SCE = GET_FINAL_RENDERED_CAM_ROT()
		fCurrentNodeFOV_SCE = GET_FINAL_RENDERED_CAM_FOV()
		
		bCaptureCurrentCamera_SCE = FALSE
	ENDIF
	
	IF iCurrentNodeLast_SCE <> iCurrentNode_SCE
		vCurrentNodePos_SCE = SplineCamNodes_SCE[iCurrentNode_SCE].vPos
		vCurrentNodeRot_SCE = SplineCamNodes_SCE[iCurrentNode_SCE].vRot
		fCurrentNodeFOV_SCE = SplineCamNodes_SCE[iCurrentNode_SCE].fFOV
		iCurrentNodeTime_SCE = SplineCamNodes_SCE[iCurrentNode_SCE].iTime
		iCurrentCAM_SPLINE_NODE_FLAGS_SCE = SplineCamNodes_SCE[iCurrentNode_SCE].iCAM_SPLINE_NODE_FLAGS
		
		iCurrentNodeLast_SCE = iCurrentNode_SCE
	ENDIF
	
	SplineCamNodes_SCE[iCurrentNode_SCE].vPos = vCurrentNodePos_SCE
	SplineCamNodes_SCE[iCurrentNode_SCE].vRot = vCurrentNodeRot_SCE
	SplineCamNodes_SCE[iCurrentNode_SCE].fFOV = fCurrentNodeFOV_SCE
	SplineCamNodes_SCE[iCurrentNode_SCE].iTime = iCurrentNodeTime_SCE
	SplineCamNodes_SCE[iCurrentNode_SCE].iCAM_SPLINE_NODE_FLAGS = iCurrentCAM_SPLINE_NODE_FLAGS_SCE
	
	IF bRenderSplineCam_SCE
		IF DOES_CAM_EXIST(camEditSpline_SCE)
			//SET_DEBUG_CAM_ACTIVE(FALSE)
			
			IF IS_CAM_ACTIVE(camEditSpline_SCE)
				SET_CAM_ACTIVE(camEditSpline_SCE, FALSE)
			ENDIF
			
			SET_CAM_ACTIVE(camEditSpline_SCE, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	PRINTLN("Camera Preview Started...")
		ENDIF
		
		bRenderSplineCam_SCE = FALSE
	ENDIF
	
	IF DOES_CAM_EXIST(camEditSpline_SCE)
		WHILE IS_CAM_ACTIVE(camEditSpline_SCE)
			PRINTLN("Previewing Camera!")
			
			IF GET_CAM_SPLINE_PHASE(camEditSpline_SCE) >= 1.0
				SET_CAM_ACTIVE(camEditSpline_SCE, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)	PRINTLN("Camera Preview Ended...")
				SET_CAM_SPLINE_PHASE(camEditSpline_SCE, 0.0)
			ENDIF
			
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF bStartSplineCam_SCE
//		SET_CAM_ACTIVE(camEditSpline_SCE, TRUE)
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		bStartSplineCam_SCE = FALSE
	ENDIF
	
	IF bDrawDebug_SCE
		
	ENDIF
ENDPROC

#ENDIF

//Creation Proc
PROC MAKE_PICKUPS()
	INT iPlacementFlags
	
	//Set health pack placement flags
	iPlacementFlags = 0
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	
	IF NOT DOES_PICKUP_EXIST(piPickups[0])
		piPickups[0] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<988.861, -2126.522, 30.976>>, <<0, 0, 269>>, iPlacementFlags)
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[0], "abaprodfloor")
	ENDIF
	IF NOT DOES_PICKUP_EXIST(piPickups[1])
		piPickups[1] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<992.9708, -2174.6101, 30.4568>>, <<0, 0, -6>>, iPlacementFlags)
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[1], "abaSLAUGHT")
	ENDIF
	IF NOT DOES_PICKUP_EXIST(piPickups[2])
		piPickups[2] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<989.534, -2150.114, 31.046>>, <<0, 0, 88>>, iPlacementFlags)
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[2], "abattmainsec2")
	ENDIF
	IF NOT DOES_PICKUP_EXIST(piPickups[3])
		piPickups[3] = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PISTOL, <<952.2078, -2127.2258, 31.1718>>, <<90, 0, 0>>, iPlacementFlags)
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[3], "ababackpass")
	ENDIF
	IF NOT DOES_PICKUP_EXIST(piPickups[4])
		piPickups[4] = CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, <<953.7915, -2127.4309, 31.2614>>, <<90, 0, 0>>, iPlacementFlags)
		ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[4], "ababackpass")
	ENDIF
ENDPROC

//═══════════════════════════╡ OBJECTIVE PROCEDURES ╞════════════════════════════

PROC initialiseMission()
	IF INIT_STAGE()
		//INFORM_MISSION_STATS_OF_MISSION_START_MICHAEL_TWO()
		
		//Michael Available
		SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
		
		//Enable Special Ability
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
		
		//Cap Interior
		SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, FALSE)
		
		#IF IS_DEBUG_BUILD
		//Auto Skipping
		IF bAutoSkipping = TRUE
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		ENDIF
		#ENDIF
		
		//Prepare Mission
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Block Shops
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
		
		//Block changing clothes
		SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
		
		//Block Ambient Variations
		SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
		
		//Request Additional Text
		REQUEST_ADDITIONAL_TEXT("MCH2", MISSION_TEXT_SLOT)
		//REQUEST_ADDITIONAL_TEXT("MCH2AUD", MISSION_DIALOGUE_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)
			
			PRINTSTRING("LOADING TEXT")
			PRINTNL()
		ENDWHILE
		
		//Doors
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
			ADD_DOOR_TO_SYSTEM(iExitDoor, PROP_ABAT_SLIDE, <<962.9084, -2105.8137, 34.6432>>)
		ENDIF
		
		//Particle Effects
		REQUEST_PTFX_ASSET()
		
		WHILE NOT HAS_PTFX_ASSET_LOADED()
			WAIT(0)
			
			PRINTSTRING("LOADING PARTICLES")
			PRINTNL()
		ENDWHILE
		
		//Audio
		REGISTER_SCRIPT_WITH_AUDIO()
		
		//Relationships
		ADD_RELATIONSHIP_GROUP("FRIEND", relGroupFriendlyFire)
		ADD_RELATIONSHIP_GROUP("BUDDIES", relGroupBuddy)
		ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
		ADD_RELATIONSHIP_GROUP("PASSIVE", relGroupPassive)
		
		//Buddy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupBuddy)
		
		//Friendly Fire
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendlyFire, RELGROUPHASH_PLAYER)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendlyFire, relGroupBuddy)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupEnemy, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendlyFire, relGroupEnemy)
		
		//Passive
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupPassive, relGroupBuddy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupBuddy, relGroupPassive)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupPassive, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, relGroupPassive)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupPassive, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupEnemy, relGroupPassive)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupPassive, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupFriendlyFire, relGroupPassive)
		
		//Enemy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBuddy, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, relGroupBuddy)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, RELGROUPHASH_PLAYER)
		
		#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widGroup)
				widGroup = START_WIDGET_GROUP("MICHAEL 2")
					START_WIDGET_GROUP("Debug Print")
						ADD_WIDGET_BOOL("Audio", bDebugAudio)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Debug Michael Rail Death")
						ADD_WIDGET_BOOL("Skip to End", bDebugMichaelDeathSkip)
						ADD_WIDGET_BOOL("Do Not Fail", bDebugMichaelDeathDoNotFail)
						ADD_WIDGET_STRING("")
						ADD_WIDGET_BOOL("Skip to Start", bDebugMichaelSkipToStart)
						ADD_WIDGET_STRING("")
						ADD_WIDGET_BOOL("Skip to Capture", bDebugMichaelSkipToCapture)
						ADD_WIDGET_BOOL("Capture Point", bDebugMichaelCapturePoint)
						ADD_WIDGET_STRING("")
						ADD_WIDGET_BOOL("Pause/Play", bDebugMichaelPausePlay)
						ADD_WIDGET_STRING("")
						ADD_WIDGET_BOOL("Debug Time", bDebugMichaelDebugTime)
						ADD_WIDGET_STRING("")
						ADD_WIDGET_BOOL("Set Debug Cam", bDebugMichaelSetDebugCam)
						ADD_WIDGET_BOOL("Capture Debug Cam", bDebugMichaelCaptureDebugCam)
						ADD_WIDGET_STRING("")
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("NONE")
							ADD_TO_WIDGET_COMBO("SHAKE_CAM_LIGHT")
				            ADD_TO_WIDGET_COMBO("SHAKE_CAM_MEDIUM")
				            ADD_TO_WIDGET_COMBO("SHAKE_CAM_HEAVY")
						STOP_WIDGET_COMBO("Handcam Shake", iDebugMichaelHandCam)
						ADD_WIDGET_FLOAT_SLIDER("Handcam Amplitude", fDebugMichaelHandCam, 0.0, 10.0, 0.1)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Spline Camera Editor")
						START_NEW_WIDGET_COMBO()
							ADD_TO_WIDGET_COMBO("TIMED_SPLINE_CAMERA")
				            ADD_TO_WIDGET_COMBO("ROUNDED_SPLINE_CAMERA")
				            ADD_TO_WIDGET_COMBO("SMOOTHED_SPLINE_CAMERA")
						STOP_WIDGET_COMBO("Spline Camera Type", iSplineCamType_SCE)
						
						ADD_WIDGET_BOOL("Auto Update Spline Path", bAutoUpdateSpline_SCE)
						ADD_WIDGET_BOOL("Update Spline Path", bUpdateSpline_SCE)
						
						ADD_WIDGET_STRING("")
						
						START_WIDGET_GROUP("Rounded and Smoothed Splines")
						
							START_NEW_WIDGET_COMBO()
								ADD_TO_WIDGET_COMBO("NO_SMOOTH")
								ADD_TO_WIDGET_COMBO("SLOW_IN_SMOOTH")
								ADD_TO_WIDGET_COMBO("SLOW_OUT_SMOOTH")
								ADD_TO_WIDGET_COMBO("SLOW_IN_OUT_SMOOTH")
							STOP_WIDGET_COMBO("Smoothing Style", iSmoothingStyle_SCE)
							
							ADD_WIDGET_INT_SLIDER("Total Spline Duration (0 = Off)", iTotalDuration_SCE, 0, 100000, 1)
						
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("Nodes Edit")
							
							ADD_WIDGET_INT_SLIDER("Camera Index", iCurrentNode_SCE, 0, iTotalNodes_SCE - 1, 1)
							
							ADD_WIDGET_BOOL("Capture Current Camera as Node", bCaptureCurrentCamera_SCE)
							
							ADD_WIDGET_STRING("")
							
							ADD_WIDGET_FLOAT_SLIDER("Camera Position X", vCurrentNodePos_SCE.X, -10000.0, 10000.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Camera Position Y", vCurrentNodePos_SCE.Y, -10000.0, 10000.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Camera Position Z", vCurrentNodePos_SCE.Z, -10000.0, 10000.0, 0.1)
							
							ADD_WIDGET_STRING("")
							
							ADD_WIDGET_FLOAT_SLIDER("Camera Rotation X", vCurrentNodeRot_SCE.X, -10000.0, 10000.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Camera Rotation Y", vCurrentNodeRot_SCE.Y, -10000.0, 10000.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("Camera Rotation Z", vCurrentNodeRot_SCE.Z, -10000.0, 10000.0, 0.1)
							
							ADD_WIDGET_STRING("")
							
							ADD_WIDGET_FLOAT_SLIDER("Camera FOV", fCurrentNodeFOV_SCE, 0.0, 130.0, 0.1)
							
							ADD_WIDGET_STRING("")
							
							ADD_WIDGET_INT_SLIDER("Camera Time", iCurrentNodeTime_SCE, 0, 100000, 1)
							
							ADD_WIDGET_STRING("")
							
							START_NEW_WIDGET_COMBO()
								ADD_TO_WIDGET_COMBO("NO_FLAGS")
								ADD_TO_WIDGET_COMBO("SMOOTH_ROT")
								ADD_TO_WIDGET_COMBO("SMOOTH_LENS_PARAMS")
							STOP_WIDGET_COMBO("CAM_SPLINE_NODE_FLAGS", iCurrentCAM_SPLINE_NODE_FLAGS_SCE)
							
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("Preview")
							
							ADD_WIDGET_BOOL("Preview Spline Camera", bRenderSplineCam_SCE)
							
						STOP_WIDGET_GROUP()
						
						START_WIDGET_GROUP("Debug Drawing")
							
							
							
						STOP_WIDGET_GROUP()
				    STOP_WIDGET_GROUP()
					CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
				STOP_WIDGET_GROUP()
				
				SET_LOCATES_HEADER_WIDGET_GROUP(widGroup)
				
				
				SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_TO_MICHAEL(scsSwitchCam_FranklinToMichael, piFranklinSwitch, piMichaelSwitch)
				SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_IN_CAR_TO_MICHAEL(scsSwitchCam_FranklinInCarToMichael, piFranklinSwitch, piMichaelSwitch)
				SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_FRANKLIN(scsSwitchCam_MichaelToFranklin, piFranklinSwitch, piMichaelSwitch)
				SETUP_SPLINE_CAM_NODE_ARRAY_FRANKLIN_GRAPPLE_TO_MICHAEL(scsSwitchCam_FranklinGrappleToMichael, piFranklinSwitch, piMichaelSwitch)
				
				
				CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam_FranklinToMichael, "Franklin", "Michael", widGroup)
				CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam_FranklinInCarToMichael, "Franklin Car", "Michael", widGroup)
				CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam_MichaelToFranklin, "Michael", "Franklin", widGroup)
				CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam_FranklinGrappleToMichael, "Frank Grapple", "Michael", widGroup)
			ENDIF
		#ENDIF
		
		//Variables
//		initSplineNodeList()
		
		initRailNodeList()
		
		//Traffic
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN), TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(FELTZER2, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(COQUETTE, TRUE)
		
		//Taxi
		DISABLE_TAXI_HAILING(TRUE)
		
		SUPPRESS_EMERGENCY_CALLS()
		
		//Franklin's Vehicle
		IF NOT IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<3.5, 10.0, 3.0>>, FALSE)
		OR (DOES_ENTITY_EXIST(vehFranklin) AND IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehFranklin)))
		OR (DOES_ENTITY_EXIST(vehFranklin) AND IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehFranklin)))
		OR (DOES_ENTITY_EXIST(vehFranklin) AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehFranklin)))
		OR (DOES_ENTITY_EXIST(vehFranklin) AND IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(vehFranklin)))
			SAFE_DELETE_VEHICLE(vehFranklin)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehFranklin)
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_FRANKLIN)
			
			WHILE NOT CREATE_PLAYER_VEHICLE(vehFranklin, CHAR_FRANKLIN, <<-24.1802, -1437.7277, 29.6542>>, 181.1853, TRUE, VEHICLE_TYPE_CAR)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehFranklin, TRUE)
		ELSE
			IF NOT IS_ENTITY_DEAD(vehFranklin)
				SET_VEHICLE_POSITION(vehFranklin, <<-24.1802, -1437.7277, 29.6542>>, 181.1853)
			ENDIF
		ENDIF
		
		//Set player
		IF NOT IS_CUTSCENE_PLAYING()
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
				WAIT(0)
			ENDWHILE
		ENDIF
		
		//Dialogue
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, NULL, "MCH2GOON")
		
		//Minimap
		bRadar = TRUE
		
		IF SKIPPED_STAGE()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		ADVANCE_STAGE()
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Your mission is being replayed
		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted = TRUE
				MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE() + 1))
            ELSE
            	MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE()))
            ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayStart), "initMission")
	ENDIF
ENDPROC

PROC cutIntro()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Cutscene
		IF bReplaySkip = FALSE
		AND bPreloaded = FALSE
			REQUEST_CUTSCENE("MIC_2_INT")
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, NULL, "LESTER")
		
		//Trackify
		ENABLE_SECOND_SCREEN_TRACKIFY_APP(TRUE)
		
		SET_TRACKIFY_TARGET_VECTOR(<<997.1, -2178.9, 30.8>>)
		
		//Radar
		bRadar = FALSE
		
		//Audio
		LOAD_AUDIO(MIC2_START)
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED_ID(), <<-14.4239, -1446.2704, 29.6472>>, 120.3018, FALSE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED,  TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - MIC_2_INT
		IF bReplaySkip = FALSE
		AND bPreloaded = FALSE
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//				IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_FRANKLIN))
//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
//				ENDIF
//				
//				IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_TREVOR))
//				AND NOT IS_PED_INJURED(PLAYER_PED(CHAR_TREVOR))
//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED(CHAR_TREVOR))
//				ENDIF
				
				SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_FRANKLIN, "Franklin")
				SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF SAFE_START_CUTSCENE(1000.0)
				OR bReplaySkip = TRUE
				OR bPreloaded = TRUE
					IF HAS_THIS_CUTSCENE_LOADED("MIC_2_INT")
					OR bReplaySkip = TRUE
					OR bPreloaded = TRUE
						IF bReplaySkip = FALSE
						AND bPreloaded = FALSE
							REGISTER_ENTITY_FOR_CUTSCENE(pedIntro, "Knocked_Out_Ped", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, G_M_Y_FAMCA_01)
							
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
								ELSE
									REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
								ENDIF
							ENDIF
							
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
								ELSE
									REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
								ENDIF
							ENDIF
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
							START_CUTSCENE()
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
							WAIT_WITH_DEATH_CHECKS(0)
							
							//Clear Area
							CLEAR_AREA(vPlayerStart, 200.0, TRUE)
							
							IF IS_SCREEN_FADED_OUT()
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
							SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
						ENDIF
						
						SET_FRONTEND_RADIO_ACTIVE(FALSE)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF DOES_ENTITY_EXIST(objDoorAnim)
					IF GET_CUTSCENE_TIME() > ROUND(10.000000 * 1000.0)
					OR WAS_CUTSCENE_SKIPPED()
						SAFE_DELETE_OBJECT(objDoorAnim)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objDoorReal)
					IF GET_CUTSCENE_TIME() > ROUND(10.000000 * 1000.0)
					OR WAS_CUTSCENE_SKIPPED()
						IF NOT IS_ENTITY_VISIBLE(objDoorReal)
							SET_ENTITY_VISIBLE(objDoorReal, TRUE)
						ENDIF
						
						SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorReal)
					ENDIF
				ENDIF
				
				IF GET_CUTSCENE_TIME() > ROUND(10.000000 * 1000.0)
					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
						ADD_DOOR_TO_SYSTEM(iFrontDoor, V_ILEV_FA_FRONTDOOR, <<-14.8689, -1441.1821, 31.1932>>)
					ELSE
						IF DOOR_SYSTEM_GET_OPEN_RATIO(iFrontDoor) <> 0.0
						OR DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
							DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, 0.0, FALSE, FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedIntro)
					ENTITY_INDEX entityIntro
					entityIntro = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Knocked_Out_Ped")
					
					IF DOES_ENTITY_EXIST(entityIntro)
						pedIntro = GET_PED_INDEX_FROM_ENTITY_INDEX(entityIntro)
					ENDIF
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
					IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						ENTITY_INDEX entityFranklin
						entityFranklin = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin")
						
						IF DOES_ENTITY_EXIST(entityFranklin)
							sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = GET_PED_INDEX_FROM_ENTITY_INDEX(entityFranklin)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						ENTITY_INDEX entityTrevor
						entityTrevor = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")
						
						IF DOES_ENTITY_EXIST(entityTrevor)
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(entityTrevor)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					
					REPLAY_STOP_EVENT()
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(168.6 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Knocked_Out_Ped")
					IF DOES_ENTITY_EXIST(pedIntro)
					AND NOT IS_ENTITY_DEAD(pedIntro)
						DISABLE_PED_PAIN_AUDIO(pedIntro, TRUE)
						
//						TASK_PLAY_ANIM_ADVANCED(pedIntro, sAnimDictMic2LeadOut, "mic_2_int_leadout_ped", <<-13.818, -1449.380, 30.608>>, <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 1.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//						
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIntro)
						
						SET_PED_TO_RAGDOLL(pedIntro, 10000, 10000, TASK_RELAX)
						
						SET_ENTITY_HEALTH(pedIntro, 5)
						APPLY_DAMAGE_TO_PED(pedIntro, 500, FALSE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						
						SET_LABEL_AS_TRIGGERED("SwitchFX[MIC_2_INT]", TRUE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						
						SET_LABEL_AS_TRIGGERED("SwitchFX[MIC_2_INT]", TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//						OPEN_SEQUENCE_TASK(seqMain)
//							IF WAS_CUTSCENE_SKIPPED()
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2LeadOut, "mic_2_int_leadout_trv", <<-13.818, -1449.380, 30.608>>, <<0.0, 0.0, 180.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 1.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//							ELSE
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2LeadOut, "mic_2_int_leadout_trv", <<-13.818, -1449.380, 30.608>>, <<0.0, 0.0, 180.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.006, EULER_YXZ, AIK_DISABLE_LEG_IK)
//							ENDIF
//							TASK_WANDER_STANDARD(NULL, 92.8)
//						CLOSE_SEQUENCE_TASK(seqMain)
//						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//						CLEAR_SEQUENCE_TASK(seqMain)
						
						TASK_WANDER_STANDARD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 92.8)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					ENDIF
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					bCutsceneSkipped = TRUE
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
				OR bReplaySkip = TRUE
					IF bReplaySkip = FALSE
						IF bCutsceneSkipped
							LOAD_SCENE_ADV(<<-14.0313, -1446.6488, 29.6462>>, 50.0)
							
							SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<-14.0313, -1446.6488, 29.6462>>, 157.2684)
							
							IF NOT DOES_ENTITY_EXIST(pedIntro)
								REQUEST_MODEL(G_M_Y_FamCA_01)
								
								WHILE NOT HAS_MODEL_LOADED(G_M_Y_FamCA_01)
									WAIT_WITH_DEATH_CHECKS(0)
								ENDWHILE
								
								SPAWN_PED(pedIntro, G_M_Y_FamCA_01, <<-13.818, -1449.380, 30.608>>, 0.0)
								
								DISABLE_PED_PAIN_AUDIO(pedIntro, TRUE)
								
								TASK_PLAY_ANIM_ADVANCED(pedIntro, sAnimDictMic2LeadOut, "mic_2_int_leadout_ped", <<-13.818, -1449.380, 30.608>>, <<0.0, 0.0, 180.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 1.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
								
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIntro)
								
								SET_PED_TO_RAGDOLL(pedIntro, 10000, 10000, TASK_RELAX)
								
								SET_ENTITY_HEALTH(pedIntro, 5)
								APPLY_DAMAGE_TO_PED(pedIntro, 500, FALSE)
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<-18.4660, -1452.3175, 29.5917>>, 93.9740)
								
								TASK_WANDER_STANDARD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 92.8)
								
								FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ENDIF
							
							CLEAR_AREA(<<-14.0313, -1446.6488, 29.6462>>, 100.0, TRUE)
							
							DO_SCREEN_FADE_IN(1000)	#IF IS_DEBUG_BUILD	PRINTLN("DO_SCREEN_FADE_IN")	#ENDIF
						ENDIF
						
						bCutsceneSkipped = FALSE
						
						SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						
						WAIT_WITH_DEATH_CHECKS(0)
						
						//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
						
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						IF HAS_LABEL_BEEN_TRIGGERED("SwitchFX[MIC_2_INT]")
							ANIMPOSTFX_PLAY("SwitchSceneFranklin", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						ENDIF
						
						//Radar
						bRadar = TRUE
						
						DISABLE_CELLPHONE(FALSE)
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
						
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0)
						WHILE NOT PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_LESTER, sConversationBlock, "MCH2_PHO", CONV_PRIORITY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
						WHILE NOT IS_PHONE_ONSCREEN()
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
//						SET_FINISHED_CALL_RETURNS_TO_HOMESCREEN(TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objDoorAnim)
						SAFE_DELETE_OBJECT(objDoorAnim)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objDoorReal)
						IF NOT IS_ENTITY_VISIBLE(objDoorReal)
							SET_ENTITY_VISIBLE(objDoorReal, TRUE)
						ENDIF
						
						SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorReal)
					ENDIF
					
					//Cutscene
					REQUEST_CUTSCENE("MIC_2_MCS_1")
					
					//Interior
					IF bPinnedAbattoir = FALSE
						intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(vPlayerStart, "v_abattoir")
						
						PIN_INTERIOR_IN_MEMORY(intAbattoir)
					ENDIF
					
					eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS
					HANDLE_SWITCH_CAM_FRANKLIN_TO_MICHAEL(scsSwitchCam_FranklinToMichael)
					bFranklinPlayedCellAnim = FALSE
					bFranklinPlayAnimFromHangupPos = FALSE
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
//				IF IS_CELLPHONE_ON_HOMESCREEN()
//				OR NOT IS_PHONE_ONSCREEN()
//					LAUNCH_TRACKIFY_IMMEDIATELY()
				
				TEXT_LABEL_23 txtRoot
				txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				
				IF (IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_PHO")
				AND ARE_STRINGS_EQUAL(txtRoot, "MCH2_PHO_7"))
				OR IS_CELLPHONE_ON_HOMESCREEN()
				OR NOT IS_PHONE_ONSCREEN()
					IF IS_CELLPHONE_ON_HOMESCREEN()
					OR NOT IS_PHONE_ONSCREEN()
						bFranklinPlayAnimFromHangupPos = TRUE
					ENDIF
					
					SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LESTER, "MCH2_APPTXT", TXTMSG_UNLOCKED)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
//				IF (IS_CELLPHONE_TRACKIFY_IN_USE()
//				AND TIMERA() > 4000)
//				OR (NOT IS_CELLPHONE_TRACKIFY_IN_USE()
//				AND TIMERA() > 1000)
				
				IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_PHO")
				OR IS_CELLPHONE_ON_HOMESCREEN()
				OR NOT IS_PHONE_ONSCREEN()
				OR g_ConversationStatus = CONV_STATE_HANGUPAWAY
					DISABLE_CELLPHONE(TRUE)	//DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					IF NOT bFranklinPlayedCellAnim
						IF HAS_ANIM_DICT_LOADED("cellphone@")
						AND HAS_MODEL_LOADED(Prop_phone_ING_03)
						AND HAS_MODEL_LOADED(mn_FrankPhoneDisplay)
							IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
								oiFrankCellPhoneProp = CREATE_OBJECT(Prop_phone_ING_03, GET_ENTITY_COORDS(PLAYER_PED_ID()))
								oiFrankCellPhoneDisplay = CREATE_OBJECT(mn_FrankPhoneDisplay, GET_ENTITY_COORDS(PLAYER_PED_ID()))
								
								ATTACH_ENTITY_TO_ENTITY(oiFrankCellPhoneProp,  PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>))
								ATTACH_ENTITY_TO_ENTITY(oiFrankCellPhoneDisplay,  PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), v_FrankPhoneDisplayOffset, v_FrankPhoneDisplayRotation)
								
								//[MF] Depending on if Franklin has recieved the Trackify app yet or not, play a slightly different animation to better match his current state.
								IF bFranklinPlayAnimFromHangupPos
									OPEN_SEQUENCE_TASK(seqMain)
										TASK_PLAY_ANIM(NULL, "cellphone@", "cellphone_text_in", SLOW_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_UPPERBODY)
										TASK_PLAY_ANIM(NULL, "cellphone@", "cellphone_text_read_base", NORMAL_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING | AF_UPPERBODY) // | AF_SECONDARY)
									CLOSE_SEQUENCE_TASK(seqMain)
									TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
									CLEAR_SEQUENCE_TASK(seqMain)
								ELSE
									CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
									OPEN_SEQUENCE_TASK(seqMain)
										TASK_PLAY_ANIM(NULL, "cellphone@", "cellphone_call_to_text", SLOW_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_UPPERBODY) // | AF_SECONDARY)
										TASK_PLAY_ANIM(NULL, "cellphone@", "cellphone_text_read_base", NORMAL_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING | AF_UPPERBODY) // | AF_SECONDARY)
									CLOSE_SEQUENCE_TASK(seqMain)
									TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
									CLEAR_SEQUENCE_TASK(seqMain)
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
								ENDIF
								
								IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
									SET_GAMEPLAY_ENTITY_HINT(oiFrankCellPhoneProp, VECTOR_ZERO, DEFAULT, DEFAULT_DWELL_TIME, DEFAULT_INTERP_IN_TIME / 4, DEFAULT_INTERP_OUT_TIME, HINTTYPE_NO_FOV)
								ENDIF
							ENDIF
							
							bFranklinPlayedCellAnim = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF TIMERA() > 3000
				AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_PHO")
				AND bFranklinPlayedCellAnim
//					DO_SCREEN_FADE_OUT(500)
//					
//					WHILE NOT IS_SCREEN_FADED_OUT()
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
					
					HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(TRUE)
					
					DISABLE_CELLPHONE(TRUE)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
		
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK) 
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		
		//Request Cutscene Variations - MIC_2_MCS_1
		IF iCutsceneStage > 1
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_MICHAEL, "Michael")	//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
//				SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAIR, g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_HAIR], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_HAIR])
//				SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HEAD, g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_HEAD], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_HEAD])
//				SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_BERD, g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_BERD], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_BERD])
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_TORSO, 19, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_LEG, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAND, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_FEET, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_TEETH, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_DECL, 0, 0)
			    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_JBIB, 0, 0)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_HEAD, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_EYES, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_EARS, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_MOUTH, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_LEFT_HAND, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_RIGHT_HAND, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_LEFT_WRIST, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_RIGHT_WRIST, -1)
				SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_HIP, -1)
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Chinese_gunman", sEnemyIntroCut[0].pedIndex)
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		REMOVE_TEXT_MESSAGE_FEED_ENTRY("MCH2_APPTXT")
		DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS("MCH2_APPTXT")
		CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_FRANKLIN)
		
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 8)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
			SET_PED_POSITION(PLAYER_PED_ID(), <<-14.9012, -1446.1061, 29.6447>>, 53.1046)
			
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<-24.2141, -1448.7147, 29.6236>>, 150.7758)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
			ENDIF
			
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			
			SET_PED_POSITION(PLAYER_PED_ID(), <<-24.2141, -1448.7147, 29.6236>>, 150.7758)
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
		
		IF NOT HAS_THIS_CUTSCENE_LOADED("MIC_2_MCS_1")
			REMOVE_CUTSCENE()
			
			WHILE HAS_CUTSCENE_LOADED()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
		ENDIF
		
		HANG_UP_AND_PUT_AWAY_PHONE()
		
		SET_FRONTEND_RADIO_ACTIVE(TRUE)
		
		SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
		
		//Hint
		STOP_GAMEPLAY_HINT()
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC findMichael()
	IF INIT_STAGE()
		//Set player
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN)) AND bReplaySkip = FALSE
			SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
		
		//Trackify
		ENABLE_SECOND_SCREEN_TRACKIFY_APP(TRUE)
		
		SET_TRACKIFY_TARGET_VECTOR(<<997.1, -2178.9, 30.8>>)
		
		//Uncapped
		bSetUncapped = TRUE
		
		//Interior
		intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(vPlayerStart, "v_abattoir")
		
		PIN_INTERIOR_IN_MEMORY(intAbattoir)
		
		WHILE NOT IS_INTERIOR_READY(intAbattoir)
			PRINTLN("PINNING INTERIOR...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		bPinnedAbattoir = TRUE
		
		MAKE_PICKUPS()
		
//		VECTOR vFranklinPosition = <<-14.4239, -1446.2704, 29.6472>>
//		FLOAT fFranklinHeading = 120.3018
//		
//		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN)), <<-14.4239, -1446.2704, 29.6472>>) < 50.0
//			vFranklinPosition = GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))
//			fFranklinHeading = GET_ENTITY_HEADING(PLAYER_PED(CHAR_FRANKLIN))
//		ENDIF
//		
//		SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart, FALSE)
		
		//Radar
		bRadar = FALSE
		
		//Set player
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
//			SET_PED_POSITION(PLAYER_PED_ID(), <<-14.9012, -1446.1061, 29.6447>>, 53.1046)
			
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vPlayerStart, fPlayerStart)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
			ENDIF
			
//			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
//			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
//			
//			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)
		ENDIF
		
		CLEAR_AREA(vPlayerStart, 500.0, TRUE)
		ADD_SCENARIO_BLOCKING_AREA(<<-26.816481, -1446.621826, 44.654160>> - <<30.0, 30.0, 30.0>>, <<-26.816481, -1446.621826, 44.654160>> + <<30.0, 30.0, 30.0>>)
		
		CLEAR_AREA(<<941.6578, -2181.0586, 29.5517>>, 1000.0, TRUE)
		ADD_SCENARIO_BLOCKING_AREA(<<966.108276, -2149.353271, 55.977444>> - <<60.0, 70.0, 30.0>>, <<966.108276, -2149.353271, 55.977444>> + <<60.0, 70.0, 30.0>>)
		
		RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED(CHAR_MICHAEL), intAbattoir)
		
		SET_PED_POSITION(PLAYER_PED(CHAR_MICHAEL), vPlayerStart, fPlayerStart, FALSE)
		
		SET_PED_TARGET_LOSS_RESPONSE(PLAYER_PED(CHAR_MICHAEL), TLR_NEVER_LOSE_TARGET)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), TRUE)
		
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_RunFromFiresAndExplosions, FALSE)
		
		REMOVE_ALL_PED_WEAPONS(PLAYER_PED(CHAR_MICHAEL))
		SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_UNARMED, TRUE)
		
//		SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED(CHAR_MICHAEL))
		
		WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		SET_PED_COMPONENT_VARIATION(PLAYER_PED(CHAR_MICHAEL), PED_COMP_TORSO, 19, 0)
		
		WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL))
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		//Franklin
		//CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, vFranklinPosition, fFranklinHeading) //<<-24.2141, -1448.7147, 29.6236>>, 150.7758)
		
//		GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PUMPSHOTGUN, 50)
//		GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE , 200, TRUE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED,  TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
		
		//Friendly Fire Relationship Group
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupFriendlyFire)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
		ENDIF
		
		//Dialogue
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, NULL, "MICHAEL")	//PLAYER_PED(CHAR_MICHAEL)
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
		
		//Rail
		bOkToHangFromHook = FALSE
		bOkToMoveRail = FALSE
		bOkToSwitch = FALSE
		
		SET_RAIL_FINAL_NODE(40)
		
		//Hook
		objHook = CREATE_OBJECT_NO_OFFSET(PROP_LD_HOOK, sRailNodes[0].vPos)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHook, FALSE)
		FREEZE_ENTITY_POSITION(objHook, TRUE)
		RETAIN_ENTITY_IN_INTERIOR(objHook, intAbattoir)
		
		objChain = CREATE_OBJECT_NO_OFFSET(PROP_CS_LEG_CHAIN_01, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, 0.01>>)
		ATTACH_ENTITY_TO_ENTITY(objChain, objHook, -1, vChainOffset, vChainRotation)
		
		objPadlock = CREATE_OBJECT_NO_OFFSET(PROP_CS_PADLOCK, GET_ENTITY_COORDS(objHook) + <<0.0, 0.0, -0.01>>)
		ATTACH_ENTITY_TO_ENTITY(objPadlock, objHook, -1, vPadlockOffset, vPadlockRotation)
		
		WHILE NOT DOES_ENTITY_HAVE_PHYSICS(objHook)
		OR NOT HAS_COLLISION_FOR_MODEL_LOADED(PROP_LD_HOOK)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_ENTITY_COORDS_NO_OFFSET(objHook, sRailNodes[0].vPos)
		SET_ENTITY_ROTATION(objHook, sRailNodes[0].vRot)
		FREEZE_ENTITY_POSITION(objHook, TRUE)
		SET_OBJECT_PHYSICS_PARAMS(objHook, fHookMass, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)	
		SET_ENTITY_DYNAMIC(objHook, TRUE)
		
		//Attach Michael to hook
		ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
		
		WHILE NOT IS_ENTITY_ATTACHED(PLAYER_PED(CHAR_MICHAEL))
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE

		TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)	//TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
		
		SET_PED_RESET_FLAG(PLAYER_PED(CHAR_MICHAEL), PRF_AllowUpdateIfNoCollisionLoaded, TRUE)
		
		//Enemy Intro (Inside)
		createEnemy(sEnemyIntroCut[0], <<996.9860, -2185.0713, 28.9775>>, 9.6956, WEAPONTYPE_SMG, CSB_CHIN_GOON)	//Original Position: <<991.6176, -2171.4707, 29.2246>>, 270.7684
		SET_PED_DEFAULT_COMPONENT_VARIATION(sEnemyIntroCut[0].pedIndex)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sEnemyIntroCut[0].pedIndex, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(CSB_CHIN_GOON)
		createEnemy(sEnemyIntroCut[1], <<992.2509, -2175.3486 -0.4, 28.9769>>, 242.3, WEAPONTYPE_PISTOL, G_M_M_CHIGOON_01)
		createEnemy(sEnemyIntroCut[2], <<988.6379, -2170.6748, 29.2006>>, 352.4113, WEAPONTYPE_PISTOL)
		createEnemy(sEnemyIntroCut[3], <<977.6, -2166.4, 30.4104 + 0.25>>, 70.2, WEAPONTYPE_UNARMED)
		createEnemy(sEnemyIntroCut[4], <<970.7029, -2169.6233, 28.4637>>, 170.9878, WEAPONTYPE_UNARMED, G_M_M_CHIGOON_01)
		createEnemy(sEnemyIntroCut[5], <<977.9589, -2156.2410, 30.0791>>, 125.1269, WEAPONTYPE_UNARMED, G_M_M_CHIGOON_01)
		createEnemy(sEnemyIntroCut[6], <<984.0753, -2158.3696, 30.0793>>, 219.8727, WEAPONTYPE_PISTOL)
		createEnemy(sEnemyIntroCut[7], <<997.6616, -2157.8818, 28.4766>>, 265.7728, WEAPONTYPE_MICROSMG, G_M_M_CHIGOON_01)
		createEnemy(sEnemyIntroCut[8], <<999.5024, -2145.7878, 28.4765>>, 275.3511, WEAPONTYPE_SMG) //Patrol: <<969.6483, -2160.4150, 28.4750>>, 178.3322
		createEnemy(sEnemyIntroCut[9], <<969.2003, -2164.1885, 28.4756>>, 187.7889, WEAPONTYPE_SMG, G_M_M_CHIGOON_01) //Patrol: <<997.3615, -2168.5959, 28.4752>>, 358.6634
		
		INT i
		
		REPEAT COUNT_OF(sEnemyIntroCut) i
			IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
				//RETAIN_ENTITY_IN_INTERIOR(sEnemyIntroCut[i].pedIndex, intAbattoir)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyIntroCut[i].pedIndex, TRUE)
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(sEnemyIntroCut) i
			IF NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
				ASSIGN_DEBUG_NAMES(sEnemyIntroCut[i].pedIndex, "IntroCut ", i)
				sEnemyIntroCut[i].sDebugName = CONCATENATE_STRINGS("IntroCut ", GET_STRING_FROM_INT(i))
			ENDIF
		ENDREPEAT
		#ENDIF
		
		//Enemy (Outside)
		createEnemy(sEnemyOutside[0], <<951.0605, -2184.5310, 29.5517>>, 24.5982, WEAPONTYPE_PISTOL)
		SET_PED_COMPONENT_VARIATION(sEnemyOutside[0].pedIndex, PED_COMP_HAIR, 2, 0)
		SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[0].pedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[0].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyOutside[0].pedIndex, <<953.1196, -2190.9177, 29.5518>>, 2.0)
		//SET_PED_RELATIONSHIP_GROUP_HASH(sEnemyOutside[0].pedIndex, relGroupPassive)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[0].pedIndex, TRUE)
		SET_LABEL_AS_TRIGGERED("sEnemyOutside[0]Blocking", TRUE)
//		createEnemy(sEnemyOutside[1], <<962.5493, -2166.0269, 36.0140>>, 179.9051, WEAPONTYPE_PISTOL)
//		sEnemyOutside[1].vLocate[0] = <<948.549438,-2178.816895,32.051651>>		sEnemyOutside[1].vLocSize[0] = <<12.0, 10.0 ,2.5>>	sEnemyOutside[1].eAdvanceStyle[0] = GUN_TO_POINT sEnemyOutside[1].vPoint[0] = <<955.2181, -2169.3628, 35.9913>>
		createEnemy(sEnemyOutside[2], <<951.1171, -2183.3445, 29.5517>>, 186.0809, WEAPONTYPE_PISTOL)
		SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[2].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sEnemyOutside[2].pedIndex, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyOutside[2].pedIndex, <<954.4744, -2186.4724, 29.5518>>, 2.0)
		//SET_PED_RELATIONSHIP_GROUP_HASH(sEnemyOutside[2].pedIndex, relGroupPassive)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[2].pedIndex, TRUE)
		SET_LABEL_AS_TRIGGERED("sEnemyOutside[2]Blocking", TRUE)
		
		#IF IS_DEBUG_BUILD
		
		REPEAT COUNT_OF(sEnemyOutside) i
			IF NOT IS_PED_INJURED(sEnemyOutside[i].pedIndex)
				ASSIGN_DEBUG_NAMES(sEnemyOutside[i].pedIndex, "Outside ", i)
				sEnemyOutside[i].sDebugName = CONCATENATE_STRINGS("Outside ", GET_STRING_FROM_INT(i))
			ENDIF
		ENDREPEAT
		
		#ENDIF
		
		//Particles
		
		//Cam
		IF NOT DOES_CAM_EXIST(camMain)
			camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		IF NOT bReplaySkip
			REQUEST_CUTSCENE("MIC_2_MCS_1")
		ENDIF
		
		//SET_RADIO_AUTO_UNFREEZE(TRUE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//[MF] Setup Franklin To Michael switch so it's ready to start playback.
		eSwitchCamState = SWITCH_CAM_SETUP_SPLINE
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<-24.2141, -1448.7147, 29.6236>>, 150.7758)
			
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			
			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<997.8495, -2178.1909, 31.5116>>, 40.0)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Audio
			PLAY_AUDIO(MIC2_FIND_MIKE_RT)
			
			SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			IF g_bShitskipAccepted = TRUE
				bReplaySkip = FALSE
			ENDIF
			
//			IF bReplaySkip = FALSE
//				IF IS_SCREEN_FADED_OUT()
//					DO_SCREEN_FADE_IN(500)
//				ENDIF
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - MIC_2_MCS_1
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_MICHAEL, "Michael")	//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAIR, g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_HAIR], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_HAIR])
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HEAD, g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_HEAD], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_HEAD])
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_BERD, g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_BERD], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_BERD])
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_TORSO, 19, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_LEG, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAND, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_FEET, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_TEETH, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_DECL, 0, 0)
		    SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_JBIB, 0, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_HEAD, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_EYES, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_EARS, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_MOUTH, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_LEFT_HAND, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_RIGHT_HAND, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_LEFT_WRIST, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_RIGHT_WRIST, -1)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", ANCHOR_HIP, -1)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Chinese_gunman", sEnemyIntroCut[0].pedIndex)
		ENDIF
		
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
//			IF NOT SAFE_IS_PLAYER_CONTROL_ON()
//				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			ENDIF
//		ENDIF
//		
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
//			SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//		ENDIF
		
		//Rail
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//		AND NOT (DOES_CAM_EXIST(sCamDetails.camID)
//		AND (IS_CAM_ACTIVE(sCamDetails.camID)
//		OR IS_CAM_RENDERING(sCamDetails.camID)))
//			SET_RAIL_FINAL_NODE(40)
//		ELSE
//			SET_RAIL_FINAL_NODE(31)
//		ENDIF
		
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			IF iCutsceneStage > 0 AND iCutsceneStage < 3
//				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//					CLEAR_TEXT()
//					SET_LABEL_AS_TRIGGERED("MCH2_INT1", TRUE)
//					SET_LABEL_AS_TRIGGERED("MCH2_INT2", TRUE)
//					SET_LABEL_AS_TRIGGERED("INTRO2", TRUE)
//					
//					SETTIMERA(7000)
//					
//					iCutsceneStage = 3
//				ENDIF
//			ENDIF
//		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_MICHAEL), PRF_AllowUpdateIfNoCollisionLoaded, TRUE)
				
				IF bReplaySkip = TRUE
				OR HAS_THIS_CUTSCENE_LOADED("MIC_2_MCS_1")
					OVERRIDE_LODSCALE_THIS_FRAME(1.0)
					
					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT HAS_LABEL_BEEN_TRIGGERED("SHAPETEST[RAN]")
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
						BOOL bShapeTest
						SHAPETEST_INDEX shapeTest
						SHAPETEST_STATUS shapeTestStatus
						INT bShapeTestHitSomething
						VECTOR vShapeTestPos
						VECTOR vShapeTestNormal
						ENTITY_INDEX shapeTestEntityIndex
						
						shapeTest = START_SHAPE_TEST_CAPSULE(GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN)), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED(CHAR_FRANKLIN), <<0.0, 2.5, 0.0>>), 0.5, DEFAULT, PLAYER_PED(CHAR_FRANKLIN))
						PRINTLN("SHAPETEST[STARTING]")
						
						WHILE NOT bShapeTest
							shapeTestStatus = GET_SHAPE_TEST_RESULT(shapeTest, bShapeTestHitSomething, vShapeTestPos, vShapeTestNormal, shapeTestEntityIndex)
							PRINTLN("SHAPETEST[RUNNING]")
							IF shapeTestStatus != SHAPETEST_STATUS_RESULTS_NOTREADY
								IF shapeTestStatus = SHAPETEST_STATUS_RESULTS_READY
									PRINTLN("SHAPETEST[SHAPETEST_STATUS_RESULTS_READY]")
								ELIF shapeTestStatus = SHAPETEST_STATUS_NONEXISTENT
									PRINTLN("SHAPETEST[SHAPETEST_STATUS_NONEXISTENT]")
								ENDIF
								PRINTLN("SHAPETEST[HitSomething=", bShapeTestHitSomething, "]")
								PRINTLN("SHAPETEST[shapeTestEntityIndex=", DOES_ENTITY_EXIST(shapeTestEntityIndex), "]")
								IF bShapeTestHitSomething = 1
								OR DOES_ENTITY_EXIST(shapeTestEntityIndex)
									PRINTLN("SHAPETEST[TRUE]")
									SET_LABEL_AS_TRIGGERED("SHAPETEST[TRUE]", TRUE)
								ENDIF
								
								bShapeTest = !bShapeTest
								
								SET_LABEL_AS_TRIGGERED("SHAPETEST[RAN]", TRUE)
							ENDIF
							
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
					ENDIF
					
					IF NOT IS_SCREEN_FADED_OUT()
						IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN)) AND bFrankToMikeSwitchCam2 = FALSE)
						OR bFrankToMikeSwitchCam = TRUE
							bFrankToMikeSwitchComplete = HANDLE_SWITCH_CAM_FRANKLIN_TO_MICHAEL(scsSwitchCam_FranklinInCarToMichael)
							bFrankToMikeSwitchCam = TRUE
						ELSE
							bFrankToMikeSwitchComplete = HANDLE_SWITCH_CAM_FRANKLIN_TO_MICHAEL(scsSwitchCam_FranklinToMichael)
							bFrankToMikeSwitchCam2 = TRUE
						ENDIF
					ENDIF
					
					IF IS_SCREEN_FADED_OUT() //[MF] Assuming we're starting from a z-skip or other case where we don't want to play the switch cam.
					OR bFrankToMikeSwitchComplete
						//[MF] Incase we're transitioning from a Z-skip
						IF IS_SCREEN_FADED_OUT()
							//[MF]Put Franklin in his car
							IF DOES_ENTITY_EXIST(vehFranklin)
							AND NOT IS_ENTITY_DEAD(vehFranklin)
							AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
									SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
									PRINTLN("SET FRANKLIN INTO HIS CAR")
								ENDIF
							ENDIF
						ENDIF
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						IF NOT bReplaySkip
							IF NOT IS_CUTSCENE_PLAYING()
								DETACH_ENTITY(PLAYER_PED(CHAR_MICHAEL))
								DETACH_ENTITY(objChain)
								DETACH_ENTITY(objPadlock)
								
								REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED(CHAR_MICHAEL), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
								REGISTER_ENTITY_FOR_CUTSCENE(sEnemyIntroCut[0].pedIndex, "Chinese_gunman", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(objHook, "Meathook", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(objChain, "Leg_Chain", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(objPadlock, "Padlock", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
//								REPLAY_RECORD_BACK_FOR_TIME(3.0)
																
								START_CUTSCENE()
								
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
							ENDIF
						ENDIF
						
						SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						
						//Audio
						PLAY_AUDIO(MIC2_START)
						
						bFrankToMikeSwitchComplete = FALSE
						
						SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED(CHAR_FRANKLIN), TRUE)
						
						ADVANCE_CUTSCENE()
					ELSE
						IF DOES_CAM_EXIST(scsSwitchCam_FranklinToMichael.ciSpline)
							IF GET_CAM_SPLINE_NODE_INDEX(scsSwitchCam_FranklinToMichael.ciSpline) > scsSwitchCam_FranklinToMichael.iCamSwitchFocusNode
								IF DOES_ENTITY_EXIST(vehFranklin)
								AND NOT IS_ENTITY_DEAD(vehFranklin)
								AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
										SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
										PRINTLN("SET FRANKLIN INTO HIS CAR")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				SET_PED_RESET_FLAG(PLAYER_PED(CHAR_MICHAEL), PRF_AllowUpdateIfNoCollisionLoaded, TRUE)
				
				//Fade
				IF NOT WAS_CUTSCENE_SKIPPED()
				AND bReplaySkip = FALSE
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
				
				//[MF] destroy old switch cam if it still exists
				IF DOES_SWITCH_CAM_EXIST(scsSwitchCam_FranklinToMichael)
					DESTROY_SWITCH_CAM(scsSwitchCam_FranklinToMichael)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
					IF NOT DOES_CAM_EXIST(camMain)
						camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					ENDIF
					
					SET_CAM_ACTIVE(camMain, TRUE)
					
					SET_CAM_PARAMS(camMain, 
									<<995.944946,-2170.087158,31.105616>>,
									<<1.338266,0.000000,-174.621979>>,
									26.994741)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Chinese_gunman")
				OR bReplaySkip = TRUE
					IF DOES_ENTITY_EXIST(sEnemyIntroCut[0].pedIndex)
						IF NOT IS_PED_INJURED(sEnemyIntroCut[0].pedIndex)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("UpdateChineseGunman")
								IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
									SET_PED_POSITION(sEnemyIntroCut[0].pedIndex, <<996.9860, -2185.0713, 28.9775>>, 9.6956)
									
									//SET_PED_ON_GROUND_PROPERLY(sEnemyIntroCut[0].pedIndex)
									
									TASK_PLAY_ANIM(sEnemyIntroCut[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
									SET_CURRENT_PED_WEAPON(sEnemyIntroCut[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemyIntroCut[0].pedIndex)
									
									SET_LABEL_AS_TRIGGERED("UpdateChineseGunman", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Meathook")
				OR bReplaySkip = TRUE
					SET_ENTITY_COORDS_NO_OFFSET(objHook, sRailNodes[0].vPos)
					SET_ENTITY_ROTATION(objHook, sRailNodes[0].vRot)
					FREEZE_ENTITY_POSITION(objHook, TRUE)
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objHook)
					
					IF sfxRail = -1
						sfxRail = GET_SOUND_ID()
						PLAY_SOUND_FROM_ENTITY(sfxRail, "Rail_Loop_Skip_Start", objHook, "MICHAEL_2_SOUNDS")
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Leg_Chain")
					ATTACH_ENTITY_TO_ENTITY(objChain, objHook, -1, vChainOffset, vChainRotation)
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objChain)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Leg_Chain^1")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Padlock")
					ATTACH_ENTITY_TO_ENTITY(objPadlock, objHook, -1, vPadlockOffset, vPadlockRotation)
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objPadlock)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				OR bReplaySkip = TRUE
					//Attach Michael to hook
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
					
					TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE, 0.5)	//TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
					
					bOkToHangFromHook = TRUE
					bOkToMoveRail = TRUE
					bOkToSwitch = TRUE
					bDoMikeToFranklinCustomSwitch = TRUE
					
					UPDATE_RAIL_MOVE()
					
					PRINTLN("Michael EXIT STATE")
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
				OR bReplaySkip = TRUE
					eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS
					HANDLE_SWITCH_CAM_MICHAEL_TO_FRANKLIN(scsSwitchCam_MichaelToFranklin)
					eSwitchCamState = SWITCH_CAM_SETUP_SPLINE
					
					IF DOES_ENTITY_EXIST(sEnemyIntroCut[0].pedIndex)
						IF NOT IS_PED_INJURED(sEnemyIntroCut[0].pedIndex)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("UpdateChineseGunman")
								IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
									SET_PED_POSITION(sEnemyIntroCut[0].pedIndex, <<996.9860, -2185.0713, 28.9775>>, 9.6956)
									
									//SET_PED_ON_GROUND_PROPERLY(sEnemyIntroCut[0].pedIndex)
									
									TASK_PLAY_ANIM(sEnemyIntroCut[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
									SET_CURRENT_PED_WEAPON(sEnemyIntroCut[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemyIntroCut[0].pedIndex)
									
									SET_LABEL_AS_TRIGGERED("UpdateChineseGunman", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
					
					DISABLE_CELLPHONE(FALSE)
					
					bRadar = TRUE
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				ADVANCE_CUTSCENE()
			BREAK
			CASE 3
				//Stats
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC2_MIKE_RESCUE_TIMER)
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 4
				ADVANCE_CUTSCENE()
			BREAK
			CASE 5
				IF DOES_CAM_EXIST(camMain)
					SET_CAM_PARAMS(camMain, //Camera 1
									GET_INTERP_POINT_VECTOR(<<995.944946,-2170.087158,31.105616>>, <<996.894836,-2169.905273,31.103991>>, 0, 8000 * 2, TO_FLOAT(TIMERA())),
									GET_INTERP_POINT_VECTOR(<<1.338266,0.000000,-174.621979>>, <<-1.324255,0.000000,-165.109879>>, 0, 8000 * 2, TO_FLOAT(TIMERA())),
									GET_INTERP_POINT_FLOAT(26.994741, 26.994741, 0, 8000 * 2, TO_FLOAT(TIMERA())))
				ENDIF
				
				IF TIMERA() > 500
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
				
				IF TIMERA() > 8000 * 2
					IF HAS_ANIM_DICT_LOADED("shake_cam_all@")
						ANIMATED_SHAKE_CAM(camMain, "shake_cam_all@", "light", "", 1.0)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 6
				SET_CAM_PARAMS(camMain,  //Camera 2 - Range = (0 to 2000 * 2)
								GET_INTERP_POINT_VECTOR(<<998.115906, -2178.469971, 31.164551>>, <<998.277161, -2174.949463, 31.030722>>, 0, (2000 * 2) + (11000 * 2), TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-2.174487, 0.0, -2.626945>>, <<-0.057604, 0.0, -2.078272>>, 0, (2000 * 2) + (11000 * 2), TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(36.612831, 36.612831, 0, (2000 * 2) + (11000 * 2), TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 2000 * 2
	 				IF NOT IS_PED_INJURED(sEnemyIntroCut[0].pedIndex)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_CLEAR_LOOK_AT(NULL)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<982.7635, -2169.8921, 29.6195>>, 0.5, -1, 0.5)						
							TASK_ACHIEVE_HEADING(NULL, 1.9956)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED(CHAR_MICHAEL), -1)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyIntroCut[0].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
					
					IF NOT IS_PED_INJURED(sEnemyIntroCut[1].pedIndex)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_CLEAR_LOOK_AT(NULL)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<983.6574, -2169.6614, 29.6194>>, 0.5, -1, 0.5)						
							TASK_ACHIEVE_HEADING(NULL, 1.9956)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED(CHAR_MICHAEL), -1)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyIntroCut[1].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 7
				SET_CAM_PARAMS(camMain,  //Camera 2 cont. - Range = (2000 * 2 to 11000 * 2)
								GET_INTERP_POINT_VECTOR(<<998.115906, -2178.469971, 31.164551>>, <<998.277161, -2174.949463, 31.030722>>, 0, (2000 * 2) + (11000 * 2), TO_FLOAT(TIMERA() + (2000 * 2))),
								GET_INTERP_POINT_VECTOR(<<-2.174487, 0.0, -2.626945>>, <<-0.057604, 0.0, -2.078272>>, 0, (2000 * 2) + (11000 * 2), TO_FLOAT(TIMERA() + (2000 * 2))),
								GET_INTERP_POINT_FLOAT(36.612831, 36.612831, 0, (2000 * 2) + (11000 * 2), TO_FLOAT(TIMERA() + (2000 * 2))))
				
				IF TIMERA() > 11000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 8
				SET_CAM_PARAMS(camMain,  //Camera 3
								GET_INTERP_POINT_VECTOR(<<999.704102,-2170.810303,31.299297>>, <<995.043762,-2170.193115,30.922483>>, 0, 11000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-4.819607,0.134012,73.641083>>, <<-1.248098,0.134012,75.161530>>, 0, 11000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(49.755203, 49.755203, 0, 11000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 11000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 9
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<992.238770,-2172.430420,31.089552>>, <<988.114502,-2171.906006,31.098305>>, 0, 9000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-3.338497,0.120910,-7.401173>>, <<-3.338496,0.120910,-5.875839>>, 0, 9000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(43.777863, 43.777863, 0, 9000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 9000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 10
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<983.821472,-2171.255615,30.270624>>, <<980.964600,-2170.211670,30.421558>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<8.599643,-0.371932,-67.799362>>, <<5.847109,0.120910,-68.360344>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(32.738300, 43.777863, 0, 10000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 7000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 11
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<978.0251, -2166.5701, 32.8924>>, <<973.6019, -2166.4131, 32.8924>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-11.5991, 0.0000, -109.7861>>, <<-11.5991, 0.0000, -109.8682>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(30.4727, 30.4727, 0, 10000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 9500 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 12
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<969.461731,-2170.036377,30.004295>>, <<968.464966,-2168.176270,30.004295>>, 0, 13000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<8.817223,-0.000000,-75.833801>>, <<8.817223,-0.000000,-75.833801>>, 0, 13000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(32.738300, 32.738300, 0, 13000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 12500 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 13
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<967.195313,-2170.564453,33.671185>>, <<967.195313,-2170.564453,31.921423>>, 0, 13000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-14.831502,-0.000000,-70.284996>>, <<-5.630730,-0.000001,-31.627630>>, 0, 13000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(25.751530, 33.373501, 0, 13000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 13000 * 2
					IF NOT IS_PED_INJURED(sEnemyIntroCut[5].pedIndex)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<978.9319, -2157.5186, 29.6194>>, PEDMOVE_WALK, -1, 0.5)						
							TASK_ACHIEVE_HEADING(NULL, 105.8316)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyIntroCut[5].pedIndex, seqMain)
					ENDIF
					
					SET_CAM_PARAMS(camMain, 
									<<965.011719,-2162.894287,29.411800>>,
									<<9.375818,-0.000001,-101.021996>>,
									45.0, 
									0)
					
					POINT_CAM_AT_COORD(camMain, GET_ENTITY_COORDS(objHook) - <<0.0, 0.0, 2.5>>)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 14
				POINT_CAM_AT_COORD(camMain, GET_ENTITY_COORDS(objHook) - <<0.0, 0.0, 2.5>>)
				
				IF TIMERA() > 13500 * 2
					IF DOES_CAM_EXIST(camMain)
						STOP_CAM_POINTING(camMain)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 15
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<974.405823,-2155.704590,29.089001>>, <<974.405823,-2155.704590,29.089001>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<22.455841,-0.000001,126.167778>>, <<22.455843,-0.000000,165.535431>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(33.373501, 33.373501, 0, 10000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 10000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 16
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<980.607300,-2156.581787,32.035435>>, <<981.430054,-2157.140869,32.099709>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-6.997153,0.000000,117.716827>>, <<-6.997152,0.000000,115.624878>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(30.322689, 30.322689, 0, 10000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 10000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 17
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<976.429810,-2161.189453 + 0.2,28.865499>>, <<979.780518,-2161.445801 + 0.2,28.865499>>, 0, 12000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<46.458088,0.000003,-62.346085>>, <<46.458088,0.000003,-62.346085>>, 0, 12000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(36.598598, 36.598598, 0, 12000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 10000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 18
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<983.044556,-2164.222168,31.131218>>, <<985.341797,-2163.953613,31.142937>>, 0, 15000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<1.462163,-0.294281,7.502443>>, <<1.133829,-0.294281,-4.563818>>, 0, 15000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(34.916599, 34.916599, 0, 15000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 14000 * 2
					SET_CAM_PARAMS(camMain, 
									<<986.355103,-2155.670166,31.569838>>,
									<<-0.775808,-0.000000,-167.990448>>,
									40.113567, 
									0)
					
					POINT_CAM_AT_COORD(camMain, GET_ENTITY_COORDS(objHook) - <<0.0, 0.0, 2.5>>)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 19
				POINT_CAM_AT_COORD(camMain, GET_ENTITY_COORDS(objHook) - <<0.0, 0.0, 2.5>>)
				
				IF TIMERA() > 10000 * 2
					STOP_CAM_POINTING(camMain)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 20
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<998.1546, -2155.2043, 30.5652>>, <<998.4847, -2156.0303, 30.5652>>, 0, 12000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<2.0353, 0.0000, 111.5716>>, <<2.0353, 0.0000, 111.5716>>, 0, 12000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(25.4629, 25.4629, 0, 12000 * 2, TO_FLOAT(TIMERA())))
				
				#IF IS_DEBUG_BUILD IF #ENDIF	
				LOAD_STREAM("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
				#IF IS_DEBUG_BUILD PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loaded")
				ELSE
					PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loading...")	
				ENDIF #ENDIF	
				
				IF TIMERA() > 10000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 21
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<992.222351,-2158.235840,34.462818>>, <<992.181763,-2154.594238,34.462818>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-64.751854,5.262573,-49.382565>>, <<-64.751854,5.262573,-49.382565>>, 0, 10000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(37.423904, 37.423904, 0, 10000 * 2, TO_FLOAT(TIMERA())))
				
				#IF IS_DEBUG_BUILD IF #ENDIF	
				LOAD_STREAM("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
				#IF IS_DEBUG_BUILD PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loaded")
				ELSE
					PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loading...")	
				ENDIF #ENDIF	
				
				IF TIMERA() > 10000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 22
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<989.786743,-2145.830322,32.251602>>, <<989.792480,-2145.666260,32.267788>>, 0, 15000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-8.540795,0.396388,-139.441086>>, <<-8.816824,0.396388,-129.708176>>, 0, 15000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(22.816650, 22.816650, 0, 15000 * 2, TO_FLOAT(TIMERA())))
				
				#IF IS_DEBUG_BUILD IF #ENDIF	
				LOAD_STREAM("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
				#IF IS_DEBUG_BUILD PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loaded")
				ELSE
					PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loading...")	
				ENDIF #ENDIF	
				
				IF TIMERA() > 13000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 23
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<998.532532,-2154.596924,31.448441>>, <<997.954041,-2153.499023,31.398094>>, 0, 12000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<-1.786677,0.000000,13.967978>>, <<-0.549397,0.000000,-0.618225>>, 0, 12000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(32.833126, 32.833126, 0, 12000 * 2, TO_FLOAT(TIMERA())))
				
				#IF IS_DEBUG_BUILD IF #ENDIF	
				LOAD_STREAM("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
				#IF IS_DEBUG_BUILD PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loaded")
				ELSE
					PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loading...")	
				ENDIF #ENDIF	
				
				IF TIMERA() > 6000 * 2
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 24
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<998.532532,-2154.596924,31.448441>>, <<997.954041,-2153.499023,31.398094>>, 0, 12000 * 2, TO_FLOAT(TIMERA()) + (6000 * 2)),
								GET_INTERP_POINT_VECTOR(<<-1.786677,0.000000,13.967978>>, <<-0.549397,0.000000,-0.618225>>, 0, 12000 * 2, TO_FLOAT(TIMERA()) + (6000 * 2)),
								GET_INTERP_POINT_FLOAT(32.833126, 32.833126, 0, 12000 * 2, TO_FLOAT(TIMERA()) + (6000 * 2)))
				
				#IF IS_DEBUG_BUILD IF #ENDIF	
				LOAD_STREAM("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
				#IF IS_DEBUG_BUILD PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loaded")
				ELSE
					PRINTLN("MIC_2_MICHAEL_CHOPPED_UP_MASTER - Loading...")	
				ENDIF #ENDIF	
				
				IF TIMERA() > 5000 * 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED(CHAR_MICHAEL), "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 25
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<999.275269,-2133.896729,30.527813>>, <<998.915771,-2136.124023,30.771074>>, 0, 4000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<6.171403,0.000000,175.146454>>, <<4.129750,-0.000000,174.688812>>, 0, 4000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(31.021326, 31.021326, 0, 4000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 4000 * 2
					TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_minced", REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 26
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF TIMERA() > 1500 * 2
						CREATE_CONVERSATION_ADV("MCH2_MINCE")
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
					IF LOAD_STREAM("MIC_2_MICHAEL_CHOPPED_UP_MASTER")
						IF TIMERA() > 1500 * 2
							PLAY_STREAM_FRONTEND()
							
							SET_LABEL_AS_TRIGGERED("MIC_2_MICHAEL_CHOPPED_UP_MASTER", TRUE)
						ENDIF
					ELSE
						PRINTLN("LOAD_STREAM('MIC_2_MICHAEL_CHOPPED_UP_MASTER') did not load in time!")
					ENDIF
				ENDIF
				
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<992.797363,-2142.922119,28.903460>>, <<993.104980,-2140.263184,28.906197>>, 0, 5000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_VECTOR(<<21.775642,0.063216,-99.448692>>, <<21.775642,0.063216,-94.619537>>, 0, 5000 * 2, TO_FLOAT(TIMERA())),
								GET_INTERP_POINT_FLOAT(27.044939, 27.044939, 0, 5000 * 2, TO_FLOAT(TIMERA())))
				
				IF TIMERA() > 4250 * 2
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
					START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_abattoir_ped_sliced", PLAYER_PED_ID(), <<0.0, -0.1, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 27
				SET_CAM_PARAMS(camMain, 
								GET_INTERP_POINT_VECTOR(<<992.797363,-2142.922119,28.903460>>, <<993.104980,-2140.263184,28.906197>>, 0, 5000 * 2, TO_FLOAT(TIMERA()) + (4250 * 2)),
								GET_INTERP_POINT_VECTOR(<<21.775642,0.063216,-99.448692>>, <<21.775642,0.063216,-94.619537>>, 0, 5000 * 2, TO_FLOAT(TIMERA()) + (4250 * 2)),
								GET_INTERP_POINT_FLOAT(27.044939, 27.044939, 0, 5000 * 2, TO_FLOAT(TIMERA()) + (4250 * 2)))
				
				SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
				
				IF TIMERA()	> 2000
				#IF IS_DEBUG_BUILD	AND bDebugMichaelDeathDoNotFail = FALSE	#ENDIF
					eMissionFail = failMichaelDied
					
					missionFailed()
				ENDIF
			BREAK
		ENDSWITCH
		
		INT i
		
		//Text
		IF NOT sCamDetails.bRun
			IF iCutsceneStage > 0
				IF iCutsceneStage > 3
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_INTRO2")
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
						CREATE_CONVERSATION_ADV("MCH2_IG1", CONV_PRIORITY_MEDIUM, FALSE, DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
					
					PRINT_ADV("MCH2_INTRO2")
					
					IF NOT IS_CELLPHONE_DISABLED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						AND (NOT DOES_CAM_EXIST(sCamDetails.camID)
						OR (DOES_CAM_EXIST(sCamDetails.camID)
						AND NOT IS_CAM_ACTIVE(sCamDetails.camID)))
							IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_INTRO3")
								CLEAR_TEXT()
								PRINT_ADV("MCH2_INTRO3")
								
								PRINT_HELP_ADV("MCH2_INT3HLP2", TRUE, FALSE, DEFAULT_HELP_TEXT_TIME * 2)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_INT3HLP3")
						IF HAS_LABEL_BEEN_TRIGGERED("MCH2_INT3HLP2")
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MCH2_INT3HLP2")
								IF IS_CELLPHONE_TRACKIFY_IN_USE()
									PRINT_HELP_ADV("MCH2_INT3HLP3")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					AND IS_CAM_RENDERING(camMain)
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF iDialogueStage = 0
								IF GET_GAME_TIMER() > iDialogueTimer[0]
									IF iDialogueLineCount[iDialogueStage] = -1 
										 iDialogueLineCount[iDialogueStage] = 6 - 1	//One line plays alongside the god text
									ENDIF
									
									IF iDialogueLineCount[iDialogueStage] > 0
										ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
										CREATE_CONVERSATION_ADV("MCH2_IG1", CONV_PRIORITY_MEDIUM, FALSE)

										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 15000)
										
										iDialogueLineCount[iDialogueStage]--
									ELIF iCutsceneStage < 25
										PLAY_PED_AMBIENT_SPEECH(PLAYER_PED(CHAR_MICHAEL), "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR) //also there is GENERIC_CURSE_MED
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 15000)
									ENDIF
								ENDIF
							ELIF iDialogueStage = 1
								IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HOOK1")
									INT iClosest
									
									REPEAT iEnemyIntroCut i
										IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
										AND NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
											IF NOT DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											OR (DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											AND IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex))
											OR (DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											AND NOT IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex)
											OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[iClosest].pedIndex, FALSE)))
												iClosest = i
											ENDIF
										ENDIF
									ENDREPEAT
									
									IF DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
									AND NOT IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex)
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[iClosest].pedIndex)) < 10.0
										ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, sEnemyIntroCut[iClosest].pedIndex, "MCH2CHIN2")
										
										CREATE_CONVERSATION_ADV("MCH2_HOOK1")
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(25000, 25000)
									ENDIF
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HOOK2")
									INT iClosest
									
									REPEAT iEnemyIntroCut i
										IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
										AND NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
											IF NOT DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											OR (DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											AND IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex))
											OR (DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											AND NOT IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex)
											OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[iClosest].pedIndex, FALSE)))
												iClosest = i
											ENDIF
										ENDIF
									ENDREPEAT
									
									IF DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
									AND NOT IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex)
									AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[iClosest].pedIndex)) < 10.0
										ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, sEnemyIntroCut[iClosest].pedIndex, "MCH2CHIN2")
										
										CREATE_CONVERSATION_ADV("MCH2_HOOK2")
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(20000, 25000)
									ENDIF
								ELSE
									IF GET_GAME_TIMER() > iDialogueTimer[0]
										IF iDialogueLineCount[iDialogueStage] = -1 
											 iDialogueLineCount[iDialogueStage] = 6
										ENDIF
										
										IF iDialogueLineCount[iDialogueStage] > 0
											INT iClosest
											
											REPEAT iEnemyIntroCut i
												IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
												AND NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
													IF NOT DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
													OR (DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
													AND IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex))
													OR (DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
													AND NOT IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex)
													OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[iClosest].pedIndex, FALSE)))
														iClosest = i
													ENDIF
												ENDIF
											ENDREPEAT
											
											IF DOES_ENTITY_EXIST(sEnemyIntroCut[iClosest].pedIndex)
											AND NOT IS_PED_INJURED(sEnemyIntroCut[iClosest].pedIndex)
											AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyIntroCut[iClosest].pedIndex)) < 10.0
												ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, sEnemyIntroCut[iClosest].pedIndex, "MCH2CHIN2")
												
												CREATE_CONVERSATION_ADV("MCH2_WATCH", CONV_PRIORITY_MEDIUM, FALSE)
												
												iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 15000)
												
												iDialogueLineCount[iDialogueStage]--
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF iDialogueStage < 1
								iDialogueStage++
							ELSE
								iDialogueStage = 0
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<952.6562, -2186.5637, 29.5517>>, <<300.0, 300.0, 300.0>>, TRUE)
				IF NOT DOES_ENTITY_EXIST(vehEnemy[0])
					IF HAS_MODEL_LOADED_CHECK(BISON)
						vehEnemy[0] = CREATE_VEHICLE(BISON, <<953.1112, -2187.5334, 29.5516>>, 159.9804)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEnemy[0], TRUE)
						SET_VEHICLE_COLOURS(vehEnemy[0], 42, 42)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehEnemy[0])
						SET_VEHICLE_DOORS_LOCKED(vehEnemy[0], VEHICLELOCK_LOCKED)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehEnemy[1])
					IF HAS_MODEL_LOADED_CHECK(BISON)
						vehEnemy[1] = CREATE_VEHICLE(BISON, <<941.9060, -2177.2876, 29.5517>>, 9.5321)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEnemy[1], TRUE)
						SET_VEHICLE_COLOURS(vehEnemy[0], 52, 52)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehEnemy[1])
						SET_VEHICLE_DOORS_LOCKED(vehEnemy[1], VEHICLELOCK_LOCKED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Render Rail Cam
		IF NOT sCamDetails.bRun
		OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND IS_PLAYER_SWITCH_IN_PROGRESS()
		AND GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
		AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_HOLD)
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF DOES_CAM_EXIST(camMain)
					IF NOT IS_CAM_RENDERING(camMain)
					AND (IS_GAMEPLAY_CAM_RENDERING()
					OR (IS_PLAYER_SWITCH_IN_PROGRESS()
					AND GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
					AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_HOLD))
						IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook)
							SET_CAM_ACTIVE(camMain, TRUE)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				IF DOES_CAM_EXIST(camMain)
					IF IS_CAM_RENDERING(camMain)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Game Viewport for Cameras
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF DOES_CAM_EXIST(camMain)
				IF IS_CAM_RENDERING(camMain)	
					SWITCH iCutsceneStage
						CASE 0
							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("abaSLAUGHT")
						BREAK
						CASE 1
						CASE 2
						CASE 3
						CASE 4
						CASE 5
							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("ababetween")
						BREAK
						CASE 6
						CASE 7
							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("abaSLAUGHT")
						BREAK
						CASE 8
						CASE 9
						CASE 10
						CASE 11
						CASE 12
						CASE 13
						CASE 14
							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("ababetween")
						BREAK
						CASE 15
						CASE 16
						CASE 17
						CASE 18
						CASE 19
						CASE 20
						CASE 21
						CASE 22
							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("abattmainsec1")
						BREAK
						CASE 23
							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("abattmainsec2")
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF iCutsceneStage > 1
				REPEAT COUNT_OF(sEnemyIntroCut) i
					IF DOES_ENTITY_EXIST(sEnemyIntroCut[i].pedIndex)
						IF NOT IS_PED_INJURED(sEnemyIntroCut[i].pedIndex)
							IF i <> 0 AND i <> 2 AND i <> 4 AND i <> 5 AND i <> 8 AND i <> 9
								IF sEnemyIntroCut[i].iTimer < GET_GAME_TIMER()
									IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_a")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_b")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_c")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_d")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_e")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_a")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_b")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_c")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_d")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle4, "idle_a")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle4, "idle_b")
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle4, "idle_c")
										INT iAnimDict = GET_RANDOM_INT_IN_RANGE(0, 3)
										INT iAnimName = GET_RANDOM_INT_IN_RANGE(0, 5 - iAnimDict)
										
										SWITCH iAnimDict
											CASE 0
												sEnemyIntroCut[i].sAnimDict = sAnimDictIdle1
											BREAK
											CASE 1
												sEnemyIntroCut[i].sAnimDict = sAnimDictIdle2
											BREAK
											CASE 2
												sEnemyIntroCut[i].sAnimDict = sAnimDictIdle4
											BREAK
										ENDSWITCH
										
										SWITCH iAnimName
											CASE 0
												sEnemyIntroCut[i].sAnimName = "idle_a"
											BREAK
											CASE 1
												sEnemyIntroCut[i].sAnimName = "idle_b"
											BREAK
											CASE 2
												sEnemyIntroCut[i].sAnimName = "idle_c"
											BREAK
											CASE 3
												sEnemyIntroCut[i].sAnimName = "idle_d"
											BREAK
											CASE 4
												sEnemyIntroCut[i].sAnimName = "idle_e"
											BREAK
										ENDSWITCH
										
										TASK_PLAY_ANIM(sEnemyIntroCut[i].pedIndex, sEnemyIntroCut[i].sAnimDict, sEnemyIntroCut[i].sAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
										
										PRINTLN("TASK_PLAY_ANIM(sEnemyIntroCut[i].pedIndex, ", sEnemyIntroCut[i].sAnimDict, ", ", sEnemyIntroCut[i].sAnimName, ", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)")
									ELSE
										sEnemyIntroCut[i].iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000 + (ROUND(GET_ENTITY_ANIM_TOTAL_TIME(sEnemyIntroCut[i].pedIndex, sEnemyIntroCut[i].sAnimDict, sEnemyIntroCut[i].sAnimName)) * 1000), 5000 + (ROUND(GET_ENTITY_ANIM_TOTAL_TIME(sEnemyIntroCut[i].pedIndex, sEnemyIntroCut[i].sAnimDict, sEnemyIntroCut[i].sAnimName)) * 1000))
										
										PRINTLN("GET_ENTITY_ANIM_TOTAL_TIME = ", ROUND(GET_ENTITY_ANIM_TOTAL_TIME(sEnemyIntroCut[i].pedIndex, sEnemyIntroCut[i].sAnimDict, sEnemyIntroCut[i].sAnimName)) * 1000)
									ENDIF
								ENDIF
								
								IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_a")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_b")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_c")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_d")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle1, "idle_e")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_a")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_b")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_c")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle2, "idle_d")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle4, "idle_a")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle4, "idle_b")
								AND NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle4, "idle_c")
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
										IF NOT IS_PED_FACING_PED(sEnemyIntroCut[i].pedIndex, PLAYER_PED(CHAR_MICHAEL), 60.0)
											IF GET_SCRIPT_TASK_STATUS(sEnemyIntroCut[i].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
											AND GET_SCRIPT_TASK_STATUS(sEnemyIntroCut[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
												TASK_TURN_PED_TO_FACE_COORD(sEnemyIntroCut[i].pedIndex, GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
									TASK_LOOK_AT_ENTITY(sEnemyIntroCut[i].pedIndex, PLAYER_PED(CHAR_MICHAEL), 10000)
								ENDIF
							ELIF i = 0 OR i = 4
								INT iCigarette
								
								IF i = 0
									iCigarette = 0
								ELSE
									iCigarette = 2
								ENDIF
								
								IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
									TASK_PLAY_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictMic2Smoking, "smoking_loop", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
									SET_CURRENT_PED_WEAPON(sEnemyIntroCut[i].pedIndex, WEAPONTYPE_UNARMED, TRUE)
								ELSE
									TEXT_LABEL txtLabel
									
									txtLabel = "SMOKINGPUFF"
									txtLabel += i
									
									IF NOT HAS_LABEL_BEEN_TRIGGERED(txtLabel)
										IF GET_ENTITY_ANIM_CURRENT_TIME(sEnemyIntroCut[i].pedIndex, sAnimDictMic2Smoking, "smoking_loop") >= 0.3
										AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemyIntroCut[i].pedIndex, sAnimDictMic2Smoking, "smoking_loop") < 0.4
											START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", sEnemyIntroCut[i].pedIndex, <<0.0, 0.15, -0.05>>, VECTOR_ZERO, BONETAG_HEAD)
											
											SET_LABEL_AS_TRIGGERED(txtLabel, TRUE)
										ENDIF
									ELSE
										IF GET_ENTITY_ANIM_CURRENT_TIME(sEnemyIntroCut[i].pedIndex, sAnimDictMic2Smoking, "smoking_loop") >= 0.9
											START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", sEnemyIntroCut[i].pedIndex, <<0.0, 0.15, -0.05>>, VECTOR_ZERO, BONETAG_HEAD)
											
											SET_LABEL_AS_TRIGGERED(txtLabel, FALSE)
										ENDIF
									ENDIF
								ENDIF
								
								IF NOT DOES_ENTITY_EXIST(objCigarette[iCigarette])
									IF HAS_MODEL_LOADED_CHECK(PROP_CS_CIGGY_01)
										objCigarette[iCigarette] = CREATE_OBJECT(PROP_CS_CIGGY_01, GET_PED_BONE_COORDS(sEnemyIntroCut[i].pedIndex, BONETAG_PH_R_HAND, VECTOR_ZERO))
									ENDIF
								ELSE
									IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCigarette[iCigarette], sEnemyIntroCut[i].pedIndex)
										ATTACH_ENTITY_TO_ENTITY(objCigarette[iCigarette], sEnemyIntroCut[i].pedIndex, GET_PED_BONE_INDEX(sEnemyIntroCut[i].pedIndex, BONETAG_PH_R_HAND), vCigaretteAttachOffset, VECTOR_ZERO)
										IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[iCigarette])
											ptfxCigarette[iCigarette] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objCigarette[iCigarette], vCigaretteParticleOffset, VECTOR_ZERO)
										ENDIF
										SET_CURRENT_PED_WEAPON(sEnemyIntroCut[i].pedIndex, WEAPONTYPE_UNARMED, TRUE)
									ENDIF
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
									TASK_LOOK_AT_ENTITY(sEnemyIntroCut[i].pedIndex, PLAYER_PED(CHAR_MICHAEL), 10000)
								ENDIF
							ELIF i = 2 OR i = 5
								IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle3, "base")
									TASK_PLAY_ANIM(sEnemyIntroCut[i].pedIndex, sAnimDictIdle3, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY | AF_SECONDARY)
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
									TASK_LOOK_AT_ENTITY(sEnemyIntroCut[i].pedIndex, PLAYER_PED(CHAR_MICHAEL), 10000)
								ENDIF
							ELIF i = 8
								IF IS_ENTITY_AT_COORD(sEnemyIntroCut[i].pedIndex, <<999.5024, -2145.7878, 28.4765>>, <<5.0, 5.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sEnemyIntroCut[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
										TASK_FOLLOW_NAV_MESH_TO_COORD(sEnemyIntroCut[i].pedIndex, <<969.6483, -2160.4150, 28.4750>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)	//, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 178.3322)
									ENDIF
								ENDIF
								
								IF IS_ENTITY_AT_COORD(sEnemyIntroCut[i].pedIndex, <<969.6483, -2160.4150, 28.4750>>, <<5.0, 5.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sEnemyIntroCut[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
										TASK_FOLLOW_NAV_MESH_TO_COORD(sEnemyIntroCut[i].pedIndex, <<999.5024, -2145.7878, 28.4765>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)	//, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 275.3511)
									ENDIF
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
									TASK_LOOK_AT_ENTITY(sEnemyIntroCut[i].pedIndex, PLAYER_PED(CHAR_MICHAEL), 10000)
								ENDIF
							ELIF i = 9
								IF IS_ENTITY_AT_COORD(sEnemyIntroCut[i].pedIndex, <<969.2003, -2164.1885, 28.4756>>, <<5.0, 5.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sEnemyIntroCut[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
										TASK_FOLLOW_NAV_MESH_TO_COORD(sEnemyIntroCut[i].pedIndex, <<997.3615, -2168.5959, 28.4752>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)	//, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 358.6634)
									ENDIF
								ENDIF
								
								IF IS_ENTITY_AT_COORD(sEnemyIntroCut[i].pedIndex, <<997.3615, -2168.5959, 28.4752>>, <<5.0, 5.0, 5.0>>)
									IF GET_SCRIPT_TASK_STATUS(sEnemyIntroCut[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
										TASK_FOLLOW_NAV_MESH_TO_COORD(sEnemyIntroCut[i].pedIndex, <<969.2003, -2164.1885, 28.4756>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)	//, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 187.7889)
									ENDIF
								ENDIF
								
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyIntroCut[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 10.0
									TASK_LOOK_AT_ENTITY(sEnemyIntroCut[i].pedIndex, PLAYER_PED(CHAR_MICHAEL), 10000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		
		//Audio
		IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioInCar")
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
				PLAY_AUDIO(MIC2_FRANK_VEH)
				
				SET_LABEL_AS_TRIGGERED("AudioInCar", TRUE)
			ENDIF
		ENDIF
		
		//Radar / Mini Map
		DISABLE_SELECTOR_MAP_UPDATE_THIS_FRAME()
		
		//Michael Reaches Abattoir
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Kidnap_Rail")
			REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Meat_Chopper")
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			IF NOT bMikeToFrank_SwitchedToFrank
				SET_FORCE_OBJECT_THIS_FRAME(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), 30.0)
			ENDIF
		ELSE
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Kidnap_Rail")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Meat_Chopper")
		ENDIF
		
		//Franklin Reaches Abattoir
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					NEW_LOAD_SCENE_STOP()
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_WAYIN")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<982.266663, -2198.750977, 28.551861>>, <<991.753418, -2096.931885, 55.123489>>, 40.0)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<936.555542, -2116.675781, 28.508316>>, <<1009.005981, -2123.745361, 54.551849>>, 40.0)
						CREATE_CONVERSATION_ADV("MCH2_WAYIN")
					ENDIF
				ENDIF
			ENDIF
			
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
				AND NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex))
				AND (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
				AND NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex))
					IF GET_GAME_TIMER() > iDialogueTimer[3]
						IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<950.836731, -2183.857666, 38.551857>>, <<25.0, 25.0, 10.0>>)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK_1")
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, sEnemyOutside[0].pedIndex, "MCH2CHIN1")
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, sEnemyOutside[2].pedIndex, "MCH2CHIN5")
								
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK", "MCH2_TALK_1")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK_2")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK", "MCH2_TALK_2")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK2_1")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK2", "MCH2_TALK2_1")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK2_2")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK2", "MCH2_TALK2_2")
							ENDIF
							
							iDialogueTimer[3] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ABATTF")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<972.600098, -2065.420410, 26.316271>>, <<959.969910, -2203.768799, 39.551682>>, 100.0)
					SET_LABEL_AS_TRIGGERED("MCH2_ABATTF", TRUE)	//PRINT_ADV("MCH2_ABATTF")
//					PRINT_HELP_ADV("MCH2_SWITCHCAM")
					
					PLAY_AUDIO(MIC2_FIND_A_WAY)
				ENDIF
			ELSE
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Smoking)
					IF DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
						IF NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex)
							IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyOutside[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
								TASK_PLAY_ANIM(sEnemyOutside[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
								SET_CURRENT_PED_WEAPON(sEnemyOutside[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
						IF NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
							IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
								TASK_PLAY_ANIM(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0.75)
								SET_CURRENT_PED_WEAPON(sEnemyOutside[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
							ELSE
								SET_ENTITY_ANIM_SPEED(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop", 0.75)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_TANKSHELL, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_HI_OCTANE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_BULLET, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_FLARE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
				OR IS_EXPLOSION_IN_AREA(EXP_TAG_PROGRAMMABLEAR, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>))
				OR IS_PROJECTILE_IN_AREA(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
				OR IS_BULLET_IN_BOX(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
				OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
				OR ((IS_PLAYER_FREE_AIMING(PLAYER_ID())
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex))
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<951.146362, -2185.526367, 34.551811>>, <<15.0, 15.0, 5.0>>))
				OR (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
				AND GET_ENTITY_HEALTH(sEnemyOutside[0].pedIndex) < 200)
				OR (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
				AND GET_ENTITY_HEALTH(sEnemyOutside[2].pedIndex) < 200)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
				OR IS_BULLET_IN_ANGLED_AREA(<<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
					SET_LABEL_AS_TRIGGERED("AlertedTriads", TRUE)
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<951.146362, -2185.526367, 34.551811>>, <<15.0, 15.0, 5.0>>)
				OR HAS_LABEL_BEEN_TRIGGERED("AlertedTriads")
					ADVANCE_STAGE()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehRollingStart = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehRollingStart)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehRollingStart)
			ENDIF
		ENDIF
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Kidnap_Rail")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Meat_Chopper")
		
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
		ENDIF
		
		HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
		
		DISABLE_CELLPHONE(FALSE)
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(sEnemyIntroCut) i
			SAFE_DELETE_PED(sEnemyIntroCut[i].pedIndex)
		ENDREPEAT
		
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
		
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, MIC2_MIKE_RESCUE_TIMER)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC abattoirShootout()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Trackify
		ENABLE_SECOND_SCREEN_TRACKIFY_APP(TRUE)
		
		SET_TRACKIFY_TARGET_VECTOR(<<994.2946, -2150.3882, 29.4763>>)
		
		//Uncapped
		bSetUncapped = TRUE
		
		//Cover Blocking
		ADD_COVER_BLOCKING_AREA(<<997.93073, -2186.16016, 28.97771>>, <<997.42834, -2185.66040, 28.97763>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<978.15338, -2186.26709, 28.97741>>, <<979.04889, -2185.23193, 28.97736>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<978.405457, -2186.15625, 28.7274>>, <<978.905457, -2185.65625, 31.2274>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<993.80737, -2174.23584, 29.20387>> - <<0.25, 0.25, 1.0>>, <<993.80737, -2174.23584, 29.20387>> + <<0.25, 0.25, 1.25>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<972.773499, -2165.905273, 29.469034>> - <<0.35, 1.0, 1.5>>, <<972.773499, -2165.905273, 29.469034>> + <<0.35, 1.0, 1.5>>, TRUE, TRUE, TRUE)
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, FALSE)
		
		IF CAN_CREATE_RANDOM_COPS()
			SET_CREATE_RANDOM_COPS(FALSE)
		ENDIF
		
		//Radar
		bRadar = TRUE
		
		//Cam
		IF NOT DOES_CAM_EXIST(camMain)
			camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		ENDIF
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		IF DOES_CAM_EXIST(camMain)
			STOP_CAM_POINTING(camMain)
		ENDIF
		
		//Michael
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_DisableExplosionReactions, TRUE)
		
		//Switch
		bOkToSwitch = FALSE
		
		//Rail
		bOkToMoveRail = FALSE
		bOkToHangFromHook = TRUE
//		bReattach = TRUE
//		bFixedRotation = TRUE
		
		//Skip Michael Forwards!
		SET_RAIL_FINAL_NODE(31)
		iCurrentNode = 31
		vRailCurrent = sRailNodes[31].vPos
//		IF DOES_ENTITY_EXIST(objHook)
//			IF IS_PED_RAGDOLL(PLAYER_PED(CHAR_MICHAEL))
//				DANGLE_FROM_MEATHOOK(TRUE, PLAYER_PED(CHAR_MICHAEL), objHook, vRailCurrent + vHookPosOffset, FALSE, 0.0, 1000, 60000, bFixedRotation, -0.3, -0.3, -0.3, 0.3, 0.3, 0.3)	//#IF IS_DEBUG_BUILD	PRINTLN("DANGLE_FROM_MEATHOOK REF4")	#ENDIF
//			ELIF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook)
//				SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
//				SET_ENTITY_ROTATION(objHook, sRailNodes[31].vRot)
//				
//				ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
//			ENDIF
//		ENDIF
		
		SET_ENTITY_COORDS_NO_OFFSET(objHook, vRailCurrent)
		SET_ENTITY_ROTATION(objHook, sRailNodes[31].vRot)
		
		IF NOT IS_ENTITY_ATTACHED(PLAYER_PED(CHAR_MICHAEL))
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, vMichaelAttachOffset, <<0.0, 0.0, 180.0>>, TRUE, TRUE)
		ENDIF
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c")
			TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
		ENDIF
		
		RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED(CHAR_MICHAEL), intAbattoir)
		
		SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_MICHAEL), FALSE)
		
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_MICHAEL), RBF_PLAYER_BUMP | RBF_PLAYER_IMPACT | RBF_PLAYER_RAGDOLL_BUMP)
		
		REMOVE_ALL_PED_WEAPONS(PLAYER_PED(CHAR_MICHAEL))
		SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_UNARMED, TRUE)
	    
		CLEAR_TEXT()
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayAbattoirShootout), "stageAbattoirShootout")
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupFriendlyFire)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_MAIN")
			START_AUDIO_SCENE("MI_2_SHOOTOUT_MAIN")
		ENDIF
		
		//Audio
		LOAD_AUDIO(MIC2_FIGHT_BEGINS)
		
		PLAY_SOUND_FROM_COORD(-1, "MINCER_LOOP", <<992.7037, -2161.9229, 31.8097 - 0.5>>, "MICHAEL_2_SOUNDS")
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<961.5743, -2185.7659, 29.4823>>)
			covPoint[0] = ADD_COVER_POINT(<<961.5743, -2185.7659, 29.4823>>, 265.3580, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<970.8589, -2186.5432, 28.9773>>)
			covPoint[1] = ADD_COVER_POINT(<<970.8589, -2186.5432, 28.9773>>, 82.2686, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<981.2763, -2188.5393, 28.9777>>)
			covPoint[2] = ADD_COVER_POINT(<<981.2763, -2188.5393, 28.9777>>, 84.4550, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<996.7552, -2187.5620, 28.9779>>)
			covPoint[3] = ADD_COVER_POINT(<<996.7552, -2187.5620, 28.9779>>, 353.0754, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<988.2324, -2166.8076, 28.4755>>)
			covPoint[4] = ADD_COVER_POINT(<<988.2324, -2166.8076, 28.4755>>, 89.9152, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<993.1907, -2172.7732, 29.2011>>)
			covPoint[5] = ADD_COVER_POINT(<<993.1907, -2172.7732, 29.2011>>, 177.1490, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<954.7870, -2182.6062, 29.5519>>)
			covPoint[6] = ADD_COVER_POINT(<<954.7870, -2182.6062, 29.5519>>, 256.9729, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<986.1974, -2166.4761, 28.4756>>)
			covPoint[7] = ADD_COVER_POINT(<<986.1974, -2166.4761, 28.4756>>, 262.3954, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<972.2457, -2168.9514, 28.4613>>)
			covPoint[8] = ADD_COVER_POINT(<<972.2457, -2168.9514, 28.4613>>, 264.5069, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<973.2443, -2162.0090, 28.4757>>)
			covPoint[9] = ADD_COVER_POINT(<<973.2443, -2162.0090, 28.4757>>, 263.8469, COVUSE_WALLTONEITHER, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<967.4846, -2157.8857, 28.4747>>)
			covPoint[10] = ADD_COVER_POINT(<<967.4846, -2157.8857, 28.4747>>, 205.8383, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<986.0016, -2162.1299, 28.4763>>)
			covPoint[11] = ADD_COVER_POINT(<<986.0016, -2162.1299, 28.4763>>, 83.7432, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<996.7911, -2158.3494, 28.4766>>)
			covPoint[12] = ADD_COVER_POINT(<<996.7911, -2158.3494, 28.4766>>, 85.2102, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<996.3251, -2142.2861, 28.4762>>)
			covPoint[13] = ADD_COVER_POINT(<<996.3251, -2142.2861, 28.4762>>, 4.7376, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<976.4183, -2166.4651, 28.5421>>)
			covPoint[14] = ADD_COVER_POINT(<<976.4183, -2166.4651, 28.5421>>, 85.8822, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<973.4745, -2167.0322, 28.4617>>)
			covPoint[15] = ADD_COVER_POINT(<<973.4745, -2167.0322, 28.4617>>, 359.6113, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<993.2383, -2131.0708, 29.4758>>)
			covPoint[16] = ADD_COVER_POINT(<<993.2383, -2131.0708, 29.4758>>, 11.4085, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<989.3762, -2108.0239, 29.4751>>)
			covPoint[17] = ADD_COVER_POINT(<<989.3762, -2108.0239, 29.4751>>, 178.0079, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<934.6276, -2183.5769, 29.4658>>)
			covPoint[18] = ADD_COVER_POINT(<<934.6276, -2183.5769, 29.4658>>, 272.6803, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<969.1201, -2186.1074, 28.9771>>)
			covPoint[19] = ADD_COVER_POINT(<<969.1201, -2186.1074, 28.9771>>, 264.5366, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<975.9344, -2185.0681, 28.9773>>)
			covPoint[20] = ADD_COVER_POINT(<<975.9344, -2185.0681, 28.9773>>, 265.3824, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<984.1754, -2186.1909, 28.9776>>)
			covPoint[21] = ADD_COVER_POINT(<<984.1754, -2186.1909, 28.9776>>, 265.3159, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		ENDIF
		
		//Gang Inside Abattoir
		createEnemy(sEnemyShootout[0], <<978.5643, -2185.9519, 29.0001>>, 173.2941, WEAPONTYPE_PISTOL)
		sEnemyShootout[0].vLocate[0] = <<960.382202, -2185.503418, 32.497513>>	sEnemyShootout[0].vLocSize[0] = <<10.0, 3.0, 3.0>>	sEnemyShootout[0].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyShootout[0].vPoint[0] = <<978.6102, -2186.8325, 29.0012>>	sEnemyShootout[0].fSpeed[0] = PEDMOVE_SPRINT
		SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[0].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
		sEnemyShootout[0].iTime[1] = 1000	sEnemyShootout[0].eAdvanceStyle[1] = GUN_TO_POINT	sEnemyShootout[0].vPoint[1] = <<970.7040, -2186.3083, 29.0011>>	sEnemyShootout[0].fSpeed[1] = PEDMOVE_SPRINT
		createEnemy(sEnemyShootout[1], <<981.2392, -2188.4341, 28.9778>>, 353.2939, WEAPONTYPE_SMG)
		sEnemyShootout[1].vLocate[0] = <<964.029846, -2185.516846, 31.073631>>	sEnemyShootout[1].vLocSize[0] = <<2.0, 2.0, 2.0>>	sEnemyShootout[1].pedCheck[0] = sEnemyShootout[0].pedIndex	sEnemyShootout[1].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyShootout[1].vPoint[0] = <<978.5643, -2185.9519, 29.0001>>	sEnemyShootout[1].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
		sEnemyShootout[1].fDist[1] = 8.0	sEnemyShootout[1].eAdvanceStyle[1] = GUN_TO_POINT 	sEnemyShootout[1].vPoint[1] = <<985.3617, -2186.0742, 29.7047>>	sEnemyShootout[1].fSpeed[1] = PEDMOVEBLENDRATIO_RUN
		createEnemy(sEnemyShootout[2], <<971.4545, -2182.5701, 28.9997>>, 86.5340, WEAPONTYPE_ASSAULTRIFLE, G_M_M_CHIGOON_01)
		SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[2].pedIndex, CA_BLIND_FIRE_IN_COVER, FALSE)
		sEnemyShootout[2].vLocate[0] = <<960.322937, -2185.139648, 31.551695>>	sEnemyShootout[2].vLocSize[0] = <<2.5, 2.5, 2.5>>	sEnemyShootout[2].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyShootout[2].vPoint[0] = <<985.5667, -2183.5007, 29.7432>>	sEnemyShootout[2].fSpeed[0] = PEDMOVE_RUN	SET_BIT(sEnemyShootout[2].iBitsetStrict, 0)	/*sEnemyShootout[2].bStrict[0] = TRUE*/	SET_ENTITY_HEALTH(sEnemyShootout[2].pedIndex, 105)
		sEnemyShootout[2].vLocate[1] = <<979.112732, -2186.871338, 31.000574>>	sEnemyShootout[2].vLocSize[1] = <<2.5, 2.5, 2.0>>	sEnemyShootout[2].eAdvanceStyle[1] = GUN_TO_POINT	sEnemyShootout[2].vPoint[1] = <<985.7039, -2182.7744, 29.7432>>	sEnemyShootout[2].fSpeed[1] = PEDMOVE_RUN
		sEnemyShootout[2].iTime[2] = 0	sEnemyShootout[2].eAdvanceStyle[2] = GUN_TO_POINT	sEnemyShootout[2].vPoint[2] = <<994.7982, -2173.6492, 29.2247>>	sEnemyShootout[2].fSpeed[2] = PEDMOVE_WALK
		createEnemy(sEnemyShootout[3], <<998.0875, -2188.8955, 28.9782>>, 353.2941, WEAPONTYPE_PISTOL)
		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyShootout[3].pedIndex, <<998.0875, -2188.8955, 28.9782>>, 1.5)
		//Ped who runs out
		createEnemy(sEnemyShootout[10], <<964.1022, -2186.9495, 29.3077>>, 49.2363, WEAPONTYPE_PISTOL)
		SET_PED_COMBAT_MOVEMENT(sEnemyShootout[10].pedIndex, CM_STATIONARY)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[10].pedIndex, TRUE)
		
		//Entry Corridoor - Shootout: 4, 5, 6
		
		//Entry Room - Shootout: 7, 8, 9 and Set Piece: SET_PIECE_STEAM, SET_PIECE_RAIL
		
		//Main Corridoor - Shootout: 11, 12, 13, 14, 15 and Set Piece: SET_PIECE_STAIR, SET_PIECE_GRINDER, SET_PIECE_CUTTER
		
		INT i
		
		REPEAT COUNT_OF(sEnemyShootout) i
			IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
				//RETAIN_ENTITY_IN_INTERIOR(sEnemyShootout[i].pedIndex, intAbattoir)
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(sEnemyShootout) i
			IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
				ASSIGN_DEBUG_NAMES(sEnemyShootout[i].pedIndex, "Shootout ", i)
				sEnemyShootout[i].sDebugName = CONCATENATE_STRINGS("Shootout ", GET_STRING_FROM_INT(i))
			ENDIF
		ENDREPEAT
		#ENDIF
		
		//Enemy Cars
		IF NOT DOES_ENTITY_EXIST(vehEnemy[0])
			vehEnemy[0] = CREATE_VEHICLE(BISON, <<952.7221, -2187.8667, 29.5517>>, 187.2181)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEnemy[0], TRUE)
			SET_VEHICLE_COLOURS(vehEnemy[0], 42, 42)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehEnemy[0])
			SET_VEHICLE_DOORS_LOCKED(vehEnemy[0], VEHICLELOCK_LOCKED)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehEnemy[1])
			vehEnemy[1] = CREATE_VEHICLE(BISON, <<941.9060, -2177.2876, 29.5517>>, 9.5321)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEnemy[1], TRUE)
			SET_VEHICLE_COLOURS(vehEnemy[0], 52, 52)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehEnemy[1])
			SET_VEHICLE_DOORS_LOCKED(vehEnemy[1], VEHICLELOCK_LOCKED)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			IF bReplaySkip = TRUE
				SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<934.6419, -2183.5613, 29.4654>>, 355.3260, FALSE)
				
				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED(CHAR_FRANKLIN), <<934.6419, -2183.5613, 29.4654>>, -1, FALSE, 0, TRUE, TRUE, covPoint[18])
				
				TASK_COMBAT_PED(sEnemyOutside[0].pedIndex, PLAYER_PED(CHAR_FRANKLIN), COMBAT_PED_DISABLE_AIM_INTRO)
				TASK_COMBAT_PED(sEnemyOutside[2].pedIndex, PLAYER_PED(CHAR_FRANKLIN), COMBAT_PED_DISABLE_AIM_INTRO)
				SET_PED_COMBAT_MOVEMENT(sEnemyOutside[2].pedIndex, CM_STATIONARY)
				
				SET_LABEL_AS_TRIGGERED("AlertedTriads", TRUE)
				SET_LABEL_AS_TRIGGERED("MCH2_ABATTF", TRUE)
			ELSE
				SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<931.4130, -2179.8796, 29.3223>>, 263.7494, FALSE)
			ENDIF
			
			IF GET_BEST_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN)) = WEAPONTYPE_UNARMED
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
					GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
				ENDIF
			ENDIF
			
			enumCharacterList eCharFail = GET_CURRENT_PLAYER_PED_ENUM()
			
			IF GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)) != WEAPONTYPE_UNARMED AND GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)) != WEAPONTYPE_INVALID AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)))
				SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)), TRUE)
			ELSE
				SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), GET_BEST_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN)), TRUE)
			ENDIF
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<941.6578, -2181.0586, 29.5517>>, 40.0)
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			MAKE_PICKUPS()
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, NULL, "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Audio
			PLAY_AUDIO(MIC2_FIGHT_BEGINS_RT)
			
			IF bReplaySkip = TRUE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-109.5 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		INT i
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ABATTF")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<972.600098, -2065.420410, 26.316271>>, <<959.969910, -2203.768799, 39.551682>>, 100.0)
						PRINT_ADV("MCH2_ABATTF")	//SET_LABEL_AS_TRIGGERED("MCH2_ABATTF", TRUE)
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("AlertedTriads")
						IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Smoking)
							IF DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
								IF NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex)
									IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyOutside[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
										IF NOT HAS_LABEL_BEEN_TRIGGERED("Goon0Intimidate")
											TASK_PLAY_ANIM(sEnemyOutside[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
											SET_CURRENT_PED_WEAPON(sEnemyOutside[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
										ELSE
											TASK_PLAY_ANIM(sEnemyOutside[0].pedIndex, sAnimDictMic2Smoking, "smoking_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY | AF_SECONDARY)
											SET_CURRENT_PED_WEAPON(sEnemyOutside[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
									ENDIF
									
									IF NOT DOES_ENTITY_EXIST(objCigarette[0])
										IF HAS_MODEL_LOADED_CHECK(PROP_CS_CIGGY_01)
											objCigarette[0] = CREATE_OBJECT(PROP_CS_CIGGY_01, GET_PED_BONE_COORDS(sEnemyOutside[0].pedIndex, BONETAG_PH_R_HAND, VECTOR_ZERO))
											ATTACH_ENTITY_TO_ENTITY(objCigarette[0], sEnemyOutside[0].pedIndex, GET_PED_BONE_INDEX(sEnemyOutside[0].pedIndex, BONETAG_PH_R_HAND), vCigaretteAttachOffset, VECTOR_ZERO)
											IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[0])
												ptfxCigarette[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objCigarette[0], vCigaretteParticleOffset, VECTOR_ZERO)
											ENDIF
											SET_CURRENT_PED_WEAPON(sEnemyOutside[0].pedIndex, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
								IF NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
									IF NOT IS_ENTITY_PLAYING_ANIM(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop")
										IF NOT HAS_LABEL_BEEN_TRIGGERED("Goon2Intimidate")
											TASK_PLAY_ANIM(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING, 0.75)
											SET_CURRENT_PED_WEAPON(sEnemyOutside[2].pedIndex, WEAPONTYPE_UNARMED, TRUE)
										ELSE
											TASK_PLAY_ANIM(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY | AF_SECONDARY, 0.75)
											SET_CURRENT_PED_WEAPON(sEnemyOutside[2].pedIndex, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
									ELSE
										SET_ENTITY_ANIM_SPEED(sEnemyOutside[2].pedIndex, sAnimDictMic2Smoking, "smoking_loop", 0.75)
									ENDIF
									
									IF NOT DOES_ENTITY_EXIST(objCigarette[1])
										IF HAS_MODEL_LOADED_CHECK(PROP_CS_CIGGY_01)
											objCigarette[1] = CREATE_OBJECT(PROP_CS_CIGGY_01, GET_PED_BONE_COORDS(sEnemyOutside[2].pedIndex, BONETAG_PH_R_HAND, VECTOR_ZERO))
											ATTACH_ENTITY_TO_ENTITY(objCigarette[1], sEnemyOutside[2].pedIndex, GET_PED_BONE_INDEX(sEnemyOutside[2].pedIndex, BONETAG_PH_R_HAND), vCigaretteAttachOffset, VECTOR_ZERO)
											IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[1])
												ptfxCigarette[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objCigarette[1], vCigaretteParticleOffset, VECTOR_ZERO)
											ENDIF
											SET_CURRENT_PED_WEAPON(sEnemyOutside[2].pedIndex, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					BOOL bAlert
					bAlert = FALSE
					
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<951.146362, -2185.526367, 34.551811>>, <<15.0, 15.0, 5.0>>)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("BestWeaponEquip")
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
								AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
								AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
								AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
									
									SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
									
									SET_LABEL_AS_TRIGGERED("BestWeaponEquip", TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex)
						AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex) AND NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex) AND NOT IS_PED_SHOOTING(PLAYER_PED_ID()) AND (HAS_LABEL_BEEN_TRIGGERED("sEnemyOutside[0]Blocking") AND CAN_PED_SEE_HATED_PED(sEnemyOutside[0].pedIndex, PLAYER_PED_ID())))
						OR (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
						AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex) AND NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex) AND NOT IS_PED_SHOOTING(PLAYER_PED_ID()) AND (HAS_LABEL_BEEN_TRIGGERED("sEnemyOutside[2]Blocking") AND CAN_PED_SEE_HATED_PED(sEnemyOutside[2].pedIndex, PLAYER_PED_ID())))
							IF HAS_LABEL_BEEN_TRIGGERED("MCH2_SEESF")
								IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_SHTS", "MCH2_SHTS_1")	PRINTLN("SCRIPT POINT 1")
								ELSE
									IF HAS_LABEL_BEEN_TRIGGERED("MCH2_SHTS_1")
										IF TIMERA() > 1500
											bAlert = TRUE
										ENDIF
									ELSE
										SETTIMERA(0)
									ENDIF
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
									IF NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("Goon0Intimidate")
											IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<940.934326, -2180.620605, 32.547226>>, <<5.0, 8.0, 3.0>>)
												CLEAR_PED_TASKS(sEnemyOutside[0].pedIndex)
												
												TASK_GO_STRAIGHT_TO_COORD(sEnemyOutside[0].pedIndex, <<946.9802, -2184.5342, 29.5519>>, PEDMOVE_WALK)
											ENDIF
											
											SET_LABEL_AS_TRIGGERED("Goon0Intimidate", TRUE)
										ENDIF
										
										SET_PED_MOVE_RATE_OVERRIDE(sEnemyOutside[0].pedIndex, 0.6)
										
										TASK_LOOK_AT_ENTITY(sEnemyOutside[0].pedIndex, PLAYER_PED(CHAR_FRANKLIN), -1)
									ENDIF
								ENDIF
								
								IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
									IF NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("Goon2Intimidate")
											IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<940.934326, -2180.620605, 32.547226>>, <<5.0, 8.0, 3.0>>)
												CLEAR_PED_TASKS(sEnemyOutside[2].pedIndex)
												
												TASK_GO_STRAIGHT_TO_COORD(sEnemyOutside[2].pedIndex, <<948.7826, -2182.6172, 29.5519>>, PEDMOVE_WALK)
											ENDIF
											
											SET_LABEL_AS_TRIGGERED("Goon2Intimidate", TRUE)
										ENDIF
										
										SET_PED_MOVE_RATE_OVERRIDE(sEnemyOutside[2].pedIndex, 0.5)
										
										TASK_LOOK_AT_ENTITY(sEnemyOutside[2].pedIndex, PLAYER_PED(CHAR_FRANKLIN), -1)
									ENDIF
								ENDIF
								
								IF (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
								AND NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex))
								AND (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
								AND NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex))
									IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
									AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_SEES")
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, sEnemyOutside[2].pedIndex, "MCH2CHIN5")
											REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0)
											CREATE_CONVERSATION_ADV("MCH2_SEES")
										ELSE
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, sEnemyOutside[0].pedIndex, "MCH2CHIN1")
											REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0)
											CREATE_CONVERSATION_ADV("MCH2_SEESF")
										ENDIF
									ELSE
										IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_TALK")
										OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_TALK2")
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex))
						AND (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex))
							IF NOT HAS_LABEL_BEEN_TRIGGERED("Goon0Intimidate")
							AND bAlert = FALSE
								IF GET_GAME_TIMER() > iDialogueTimer[3]
									IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<950.836731, -2183.857666, 38.551857>>, <<25.0, 25.0, 10.0>>)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK_1")
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, sEnemyOutside[0].pedIndex, "MCH2CHIN1")
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, sEnemyOutside[2].pedIndex, "MCH2CHIN5")
											
											PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK", "MCH2_TALK_1")
										ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK_2")
											PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK", "MCH2_TALK_2")
										ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK2_1")
											PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK2", "MCH2_TALK2_1")
										ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TALK2_2")
											PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TALK2", "MCH2_TALK2_2")
										ENDIF
										
										iDialogueTimer[3] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bAlert = TRUE
						PRINTLN("bAlert = TRUE")
					ENDIF
					
					IF (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
					AND NOT IS_ENTITY_DEAD(sEnemyOutside[0].pedIndex)
					AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[0].pedIndex, FALSE)
						
						SET_LABEL_AS_TRIGGERED("sEnemyOutside[0]Blocking", FALSE)
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[0].pedIndex, TRUE)
						
						SET_LABEL_AS_TRIGGERED("sEnemyOutside[0]Blocking", TRUE)
					ENDIF
					
					IF (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
					AND NOT IS_ENTITY_DEAD(sEnemyOutside[2].pedIndex)
					AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[2].pedIndex, FALSE)
						
						SET_LABEL_AS_TRIGGERED("sEnemyOutside[2]Blocking", FALSE)
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[2].pedIndex, TRUE)
						
						SET_LABEL_AS_TRIGGERED("sEnemyOutside[2]Blocking", TRUE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_TANKSHELL, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_HI_OCTANE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_BULLET, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_FLARE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_PROGRAMMABLEAR, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
						PRINTLN("IS_EXPLOSION_IN_AREA([-SEE SCRIPT-], <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)")
					ENDIF
					IF IS_PROJECTILE_IN_AREA(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
						PRINTLN("IS_PROJECTILE_IN_AREA(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)")
					ENDIF
					IF IS_BULLET_IN_BOX(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
						PRINTLN("IS_BULLET_IN_BOX(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)")
					ENDIF
					IF IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
						PRINTLN("IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)")
					ENDIF
					IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
						PRINTLN("IS_PLAYER_FREE_AIMING(PLAYER_ID()")
					ENDIF
					IF DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
					AND NOT IS_ENTITY_DEAD(sEnemyOutside[0].pedIndex)
					AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex)
					AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<951.146362, -2185.526367, 34.551811>>, <<15.0, 15.0, 5.0>>)
						PRINTLN("IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex)")
					ENDIF
					IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
					AND NOT IS_ENTITY_DEAD(sEnemyOutside[2].pedIndex)
					AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex)
					AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<951.146362, -2185.526367, 34.551811>>, <<15.0, 15.0, 5.0>>)
						PRINTLN("IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex)")
					ENDIF
					IF (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
					AND GET_ENTITY_HEALTH(sEnemyOutside[0].pedIndex) < 200)
						PRINTLN("GET_ENTITY_HEALTH(sEnemyOutside[0].pedIndex) < 200")
					ENDIF
					IF (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
					AND GET_ENTITY_HEALTH(sEnemyOutside[2].pedIndex) < 200)
						PRINTLN("GET_ENTITY_HEALTH(sEnemyOutside[2].pedIndex) < 200")
					ENDIF
					#ENDIF
					
					IF bAlert = TRUE
					OR HAS_LABEL_BEEN_TRIGGERED("AlertedTriads")
					OR (IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_TANKSHELL, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_HI_OCTANE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_BULLET, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_FLARE, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_PROGRAMMABLEAR, <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>))
					OR IS_PROJECTILE_IN_AREA(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
					OR IS_BULLET_IN_BOX(<<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
					OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), <<914.99, -2146.68, 28.63>>, <<981.29, -2201.92, 40.55>>, FALSE)
					OR ((IS_PLAYER_FREE_AIMING(PLAYER_ID())
					OR (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
					AND NOT IS_ENTITY_DEAD(sEnemyOutside[0].pedIndex)
					AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[0].pedIndex))
					OR (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
					AND NOT IS_ENTITY_DEAD(sEnemyOutside[2].pedIndex)
					AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemyOutside[2].pedIndex)))
					AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<951.146362, -2185.526367, 34.551811>>, <<15.0, 15.0, 2.0>>))
					OR (DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
					AND GET_ENTITY_HEALTH(sEnemyOutside[0].pedIndex) < 200)
					OR (DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
					AND GET_ENTITY_HEALTH(sEnemyOutside[2].pedIndex) < 200)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
					OR IS_BULLET_IN_ANGLED_AREA(<<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
						IF DOES_ENTITY_EXIST(sEnemyOutside[0].pedIndex)
							IF NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex)
								IF NOT IS_PED_IN_COMBAT(sEnemyOutside[0].pedIndex)
									CLEAR_PED_TASKS(sEnemyOutside[0].pedIndex)
								ENDIF
								CLEAR_PED_SECONDARY_TASK(sEnemyOutside[0].pedIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[0].pedIndex, FALSE)
								
								SET_LABEL_AS_TRIGGERED("sEnemyOutside[0]Blocking", FALSE)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
							IF NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
								IF bAlert = TRUE
									WAIT_WITH_DEATH_CHECKS(250)
								ELSE
									SET_PED_COMBAT_MOVEMENT(sEnemyOutside[2].pedIndex, CM_STATIONARY)
								ENDIF
								
								IF NOT IS_PED_IN_COMBAT(sEnemyOutside[2].pedIndex)
									CLEAR_PED_TASKS(sEnemyOutside[2].pedIndex)
								ENDIF
								CLEAR_PED_SECONDARY_TASK(sEnemyOutside[2].pedIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyOutside[2].pedIndex, FALSE)
								
								SET_LABEL_AS_TRIGGERED("sEnemyOutside[2]Blocking", FALSE)
							ENDIF
						ENDIF
						
						//SET_PED_RELATIONSHIP_GROUP_HASH(sEnemyOutside[0].pedIndex, relGroupEnemy)
						//SET_PED_RELATIONSHIP_GROUP_HASH(sEnemyOutside[2].pedIndex, relGroupEnemy)
						
						//TASK_COMBAT_PED(sEnemyOutside[0].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
						//TASK_COMBAT_PED(sEnemyOutside[2].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
						
						REPEAT COUNT_OF(sEnemyShootout) i
							IF i != 6
								IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
									IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
										TASK_COMBAT_PED(sEnemyShootout[i].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						
						SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
						
						REPEAT COUNT_OF(objCigarette) i
							IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[i])
								STOP_PARTICLE_FX_LOOPED(ptfxCigarette[i])
							ENDIF
							SAFE_DELETE_OBJECT(objCigarette[i])
						ENDREPEAT
						
						SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_CIGGY_01)
						
						//Audio
						PLAY_AUDIO(MIC2_FIGHT_BEGINS)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF TIMERA() > 250
					IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
					AND NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
						SET_PED_COMBAT_MOVEMENT(sEnemyOutside[2].pedIndex, CM_DEFENSIVE)
					ENDIF
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_PED_INJURED(sEnemyOutside[0].pedIndex)
					AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != intAbattoir
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, sEnemyOutside[0].pedIndex, "MCH2CHIN1")
						
						CREATE_CONVERSATION_ADV("MCH2_GUN1")
					ELIF NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
					AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != intAbattoir
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, sEnemyOutside[2].pedIndex, "MCH2CHIN5")
						
						CREATE_CONVERSATION_ADV("MCH2_GUN")
					ELSE
						IF DOES_ENTITY_EXIST(sEnemyShootout[10].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyShootout[10].pedIndex)
							CLEAR_PED_TASKS(sEnemyShootout[10].pedIndex)
							
							TASK_COMBAT_PED(sEnemyShootout[10].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
							SET_PED_ALERTNESS(sEnemyShootout[10].pedIndex, AS_MUST_GO_TO_COMBAT)
							
							FORCE_PED_MOTION_STATE(sEnemyShootout[10].pedIndex, MS_ON_FOOT_RUN)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyShootout[10].pedIndex, <<957.4456, -2185.1714, 29.5391>>, 1.0)
							SET_COMBAT_FLOAT(sEnemyShootout[10].pedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 0.0)
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[10].pedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[10].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_MOVEMENT(sEnemyShootout[10].pedIndex, CM_DEFENSIVE)
							
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[10].pedIndex, CA_CAN_CHARGE, TRUE)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[10].pedIndex, FALSE)
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, sEnemyShootout[10].pedIndex, "MCH2CHIN2")
							
							PLAY_PED_AMBIENT_SPEECH(sEnemyShootout[10].pedIndex, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_SHOUTED)
						ENDIF
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF DOES_ENTITY_EXIST(sEnemyOutside[2].pedIndex)
				AND NOT IS_PED_INJURED(sEnemyOutside[2].pedIndex)
					SET_PED_COMBAT_MOVEMENT(sEnemyOutside[2].pedIndex, CM_DEFENSIVE)
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intAbattoir
					PRINT_ADV("MCH2_RESCUE")
					
					SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF HAS_LABEL_BEEN_TRIGGERED("MCH2_RESCUE")
				AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH2_RESCUE")
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV("MCH2_SHTS", "MCH2_SHTS_2")
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 4
				IF IS_GAMEPLAY_CAM_RENDERING()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF GET_GAME_TIMER() > iDialogueTimer[0]
						OR (NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
						AND iSetPiece[SET_PIECE_RAIL] = 3)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy)) < 20.0
							AND IS_ENTITY_ON_SCREEN(pedClosestEnemy)
							AND IS_PED_SHOOTING(PLAYER_PED(CHAR_FRANKLIN))
							OR (NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
							AND iSetPiece[SET_PIECE_RAIL] = 3)
								IF iDialogueLineCount[0] = -1 
									 iDialogueLineCount[0] = 6
								ENDIF
								
								IF iDialogueLineCount[1] = -1 
									iDialogueLineCount[1] = 6
								ENDIF
								
								IF iDialogueLineCount[2] = -1 
									iDialogueLineCount[2] = 6
								ENDIF
								
								IF iDialogueLineCount[iDialogueStage] > 0
									IF iDialogueStage = 0
										IF DOES_ENTITY_EXIST(pedClosestEnemy)
										AND NOT IS_PED_INJURED(pedClosestEnemy)
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, pedClosestEnemy, "MCH2CHIN1")
											
											CREATE_CONVERSATION_ADV("MCH2_BEFORE")
										ENDIF
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000, 5000)
										
										iDialogueLineCount[iDialogueStage]--
										
										iDialogueStage++
									ELIF iDialogueStage = 1
										IF DOES_ENTITY_EXIST(pedClosestEnemy)
										AND NOT IS_PED_INJURED(pedClosestEnemy)
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedClosestEnemy, "MCH2CHIN5")
											
											CREATE_CONVERSATION_ADV("MCH2_BEFOR2")
										ENDIF
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000, 5000)
										
										iDialogueLineCount[iDialogueStage]--
										
										iDialogueStage++
									ELIF iDialogueStage = 2
										CREATE_CONVERSATION_ADV("MCH2_SHTF", CONV_PRIORITY_MEDIUM, FALSE)
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000, 5000)
										
										iDialogueLineCount[iDialogueStage]--
										
										iDialogueStage = 0
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("EnteredAbattoir")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<984.463074, -2187.068848, 28.977724>>, <<952.025391, -2183.805908, 33.051861>>, 4.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
			OR IS_BULLET_IN_ANGLED_AREA(<<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
			OR (intAbattoir != NULL
			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intAbattoir)
				SET_LABEL_AS_TRIGGERED("EnteredAbattoir", TRUE)
			ENDIF
		ELSE
			//Spawn Enemies
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ShootoutWave1")	//Entry Corridoor - Shootout: 4, 5, 6
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<974.844421, -2184.315918, 31.977201>>, <<10.0, 5.0, 3.0>>)
					//sEnemyShootout
					createEnemy(sEnemyShootout[4], <<998.4456, -2180.6499, 28.7595>>, 135.8026, WEAPONTYPE_SMG, G_M_M_CHIGOON_01)
					sEnemyShootout[4].vLocate[0] = <<984.091248, -2187.797119, 30.501015>>	sEnemyShootout[4].vLocSize[0] = <<1.5, 1.5, 1.5>>	sEnemyShootout[4].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyShootout[4].vPoint[0] = <<997.2999, -2185.4778, 29.0007>>	sEnemyShootout[4].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
					SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[4].pedIndex, CA_BLIND_FIRE_IN_COVER, FALSE)
					createEnemy(sEnemyShootout[5], <<993.3096, -2172.7703, 29.2005>>, 263.6470, WEAPONTYPE_PUMPSHOTGUN, G_M_M_CHIGOON_01)
					SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyShootout[5].pedIndex, <<993.3096, -2172.7703, 29.2005>>, 1.5)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[5].pedIndex, CA_BLIND_FIRE_IN_COVER, FALSE)
					TASK_STAY_IN_COVER(sEnemyShootout[5].pedIndex)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyShootout[5].pedIndex, 500.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[5].pedIndex, TRUE)
					createEnemy(sEnemyShootout[6], <<995.6730, -2179.0708, 29.0006>>, -5.3, WEAPONTYPE_SMG)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[6].pedIndex, CM_STATIONARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[6].pedIndex, TRUE)
					
					REPEAT COUNT_OF(sEnemyShootout) i
						IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
							//RETAIN_ENTITY_IN_INTERIOR(sEnemyShootout[i].pedIndex, intAbattoir)
						ENDIF
					ENDREPEAT
					
					#IF IS_DEBUG_BUILD
					REPEAT COUNT_OF(sEnemyShootout) i
						IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
							ASSIGN_DEBUG_NAMES(sEnemyShootout[i].pedIndex, "Shootout ", i)
							sEnemyShootout[i].sDebugName = CONCATENATE_STRINGS("Shootout ", GET_STRING_FROM_INT(i))
						ENDIF
					ENDREPEAT
					#ENDIF
					
					REPEAT COUNT_OF(sEnemyShootout) i
						IF i != 6
							IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
								IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
									TASK_COMBAT_PED(sEnemyShootout[i].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					SET_LABEL_AS_TRIGGERED("ShootoutWave1", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ShootoutWave2")	//Entry Room - Shootout: 7, 8, 9 and Set Piece: SET_PIECE_STEAM, SET_PIECE_RAIL
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<994.374084, -2175.041748, 31.483383>>, <<7.0, 8.0, 3.5>>)
					//sEnemyShootout
					createEnemy(sEnemyShootout[7], <<970.51898, -2158.60938, 28.4752>>, 266.3385, WEAPONTYPE_PISTOL)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[7].pedIndex, CM_STATIONARY)
					SET_PED_ACCURACY(sEnemyShootout[7].pedIndex, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[7].pedIndex, TRUE)
					createEnemy(sEnemyShootout[8], <<967.1279, -2166.9990, 29.8864>>, 265.5443, WEAPONTYPE_ASSAULTRIFLE, G_M_M_CHIGOON_01)
					SET_ENTITY_HEALTH(sEnemyShootout[8].pedIndex, 105)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[8].pedIndex, CM_STATIONARY)
					SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyShootout[8].pedIndex, <<967.1279, -2166.9990, 29.8864>>, 1.5, TRUE)
					createEnemy(sEnemyShootout[9], <<966.2576, -2161.1187, 28.4930>>, 178.2352, WEAPONTYPE_SMG)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[9].pedIndex, CM_DEFENSIVE)
					
					REPEAT COUNT_OF(sEnemyShootout) i
						IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
							//RETAIN_ENTITY_IN_INTERIOR(sEnemyShootout[i].pedIndex, intAbattoir)
						ENDIF
					ENDREPEAT
					
					#IF IS_DEBUG_BUILD
					REPEAT COUNT_OF(sEnemyShootout) i
						IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
							ASSIGN_DEBUG_NAMES(sEnemyShootout[i].pedIndex, "Shootout ", i)
							sEnemyShootout[i].sDebugName = CONCATENATE_STRINGS("Shootout ", GET_STRING_FROM_INT(i))
						ENDIF
					ENDREPEAT
					#ENDIF
					
					REPEAT COUNT_OF(sEnemyShootout) i
						IF i != 6
							IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
								IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
									TASK_COMBAT_PED(sEnemyShootout[i].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					//sEnemySetPiece
					createEnemy(sEnemySetPiece[SET_PIECE_STEAM], <<976.6239, -2166.3784, 30.4104 + 0.25>>, 266.3925, WEAPONTYPE_ASSAULTRIFLE)
					SET_PED_ALLOWED_TO_DUCK(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, FALSE)
					createEnemy(sEnemySetPiece[SET_PIECE_RAIL], <<970.7867, -2163.6367, 28.4890>>, 266.1377, WEAPONTYPE_PISTOL)
					
					#IF IS_DEBUG_BUILD
					REPEAT COUNT_OF(sEnemySetPiece) i
						IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
							IF NOT IS_PED_INJURED(sEnemySetPiece[i].pedIndex)
								ASSIGN_DEBUG_NAMES(sEnemySetPiece[i].pedIndex, "SetPiece ", i)
								sEnemySetPiece[i].sDebugName = CONCATENATE_STRINGS("SetPiece ", GET_STRING_FROM_INT(i))
							ENDIF
						ENDIF
					ENDREPEAT
					#ENDIF
					
					REPEAT COUNT_OF(sEnemySetPiece) i
						IF i = ENUM_TO_INT(SET_PIECE_STEAM) OR i = ENUM_TO_INT(SET_PIECE_GRINDER) OR i = ENUM_TO_INT(SET_PIECE_CUTTER)
							IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
								//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[i].pedIndex, intAbattoir)
								SET_PED_CONFIG_FLAG(sEnemySetPiece[i].pedIndex, PCF_DontActivateRagdollFromBulletImpact, TRUE)
								SET_PED_CONFIG_FLAG(sEnemySetPiece[i].pedIndex, PCF_DontActivateRagdollFromElectrocution, TRUE)
								SET_PED_CONFIG_FLAG(sEnemySetPiece[i].pedIndex, PCF_DontActivateRagdollFromFire, TRUE)
								SET_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[i].pedIndex, RBF_BULLET_IMPACT | RBF_EXPLOSION | RBF_ELECTROCUTION | RBF_FIRE | RBF_RUBBER_BULLET)SET_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[i].pedIndex, RBF_FIRE)
								SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[i].pedIndex, CM_STATIONARY)	#IF	IS_DEBUG_BUILD	PRINTLN("SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[", i,"].pedIndex, CM_STATIONARY)")	#ENDIF
								SET_PED_SUFFERS_CRITICAL_HITS(sEnemySetPiece[i].pedIndex, FALSE)
								SET_PED_MAX_HEALTH(sEnemySetPiece[i].pedIndex, 1000)
								SET_ENTITY_HEALTH(sEnemySetPiece[i].pedIndex, 1000)
								SET_PED_STRAFE_CLIPSET(sEnemySetPiece[i].pedIndex, "move_ped_strafing")
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
						//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, intAbattoir)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, TRUE)
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("ShootoutWave2", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ShootoutWave3")	//Main Corridoor - Shootout: 11, 12, 13, 14, 15 and Set Piece: SET_PIECE_STAIR, SET_PIECE_GRINDER, SET_PIECE_CUTTER
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<972.544800, -2166.483154, 31.963011>>, <<8.5, 5.5, 3.5>>)
					//sEnemyShootout
					createEnemy(sEnemyShootout[11], <<994.4498, -2152.9666, 28.4764>>, 137.2933, WEAPONTYPE_PISTOL)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[11].pedIndex, CM_STATIONARY)
					createEnemy(sEnemyShootout[12], <<985.8372, -2161.9763, 28.4865>>, 353.2941, WEAPONTYPE_ASSAULTRIFLE, G_M_M_CHIGOON_01)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[12].pedIndex, CM_DEFENSIVE)
					SET_ENTITY_LOD_DIST(sEnemyShootout[12].pedIndex, 20)
					createEnemy(sEnemyShootout[13], <<996.5764, -2158.7720, 28.5003>>, 173.2941, WEAPONTYPE_SMG)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[13].pedIndex, CM_DEFENSIVE)
					createEnemy(sEnemyShootout[14], <<996.8381, -2152.1233, 28.4765>>, 137.2934, WEAPONTYPE_SMG)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[14].pedIndex, CM_STATIONARY)
					createEnemy(sEnemyShootout[15], <<975.6500, -2159.4011, 28.4758>>, 209.9887, WEAPONTYPE_PISTOL, G_M_M_CHIGOON_01)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[15].pedIndex, CM_DEFENSIVE)
					SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyShootout[15].pedIndex, <<975.6500, -2159.4011, 28.4758>>, 1.5, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[15].pedIndex, TRUE)
					
					REPEAT COUNT_OF(sEnemyShootout) i
						IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
							//RETAIN_ENTITY_IN_INTERIOR(sEnemyShootout[i].pedIndex, intAbattoir)
						ENDIF
					ENDREPEAT
					
					#IF IS_DEBUG_BUILD
					REPEAT COUNT_OF(sEnemyShootout) i
						IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
							ASSIGN_DEBUG_NAMES(sEnemyShootout[i].pedIndex, "Shootout ", i)
							sEnemyShootout[i].sDebugName = CONCATENATE_STRINGS("Shootout ", GET_STRING_FROM_INT(i))
						ENDIF
					ENDREPEAT
					#ENDIF
					
					REPEAT COUNT_OF(sEnemyShootout) i
						IF i != 6
							IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
								IF NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
									TASK_COMBAT_PED(sEnemyShootout[i].pedIndex, PLAYER_PED(CHAR_FRANKLIN))
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					//sEnemySetPiece
					createEnemy(sEnemySetPiece[SET_PIECE_GRINDER], <<990.69, -2161.42, 30.4404>>, 82.2840, WEAPONTYPE_SMG, G_M_M_CHIGOON_01)
					SET_PED_ALLOWED_TO_DUCK(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, FALSE)
					createEnemy(sEnemySetPiece[SET_PIECE_STAIR], <<978.2984, -2156.2178, 30.1027>>, 86.4433, WEAPONTYPE_PUMPSHOTGUN, G_M_M_CHIGOON_01)
					createEnemy(sEnemySetPiece[SET_PIECE_CUTTER], <<997.6876, -2144.3420, 28.5000>>, 173.3068, WEAPONTYPE_ASSAULTRIFLE)
					
					#IF IS_DEBUG_BUILD
					REPEAT COUNT_OF(sEnemySetPiece) i
						IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
							IF NOT IS_PED_INJURED(sEnemySetPiece[i].pedIndex)
								ASSIGN_DEBUG_NAMES(sEnemySetPiece[i].pedIndex, "SetPiece ", i)
								sEnemySetPiece[i].sDebugName = CONCATENATE_STRINGS("SetPiece ", GET_STRING_FROM_INT(i))
							ENDIF
						ENDIF
					ENDREPEAT
					#ENDIF
					
					REPEAT COUNT_OF(sEnemySetPiece) i
						IF i = ENUM_TO_INT(SET_PIECE_STEAM) OR i = ENUM_TO_INT(SET_PIECE_GRINDER) OR i = ENUM_TO_INT(SET_PIECE_CUTTER)
							IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
								//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[i].pedIndex, intAbattoir)
								SET_PED_CONFIG_FLAG(sEnemySetPiece[i].pedIndex, PCF_DontActivateRagdollFromBulletImpact, TRUE)
								SET_PED_CONFIG_FLAG(sEnemySetPiece[i].pedIndex, PCF_DontActivateRagdollFromElectrocution, TRUE)
								SET_PED_CONFIG_FLAG(sEnemySetPiece[i].pedIndex, PCF_DontActivateRagdollFromFire, TRUE)
								SET_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[i].pedIndex, RBF_BULLET_IMPACT | RBF_EXPLOSION | RBF_ELECTROCUTION | RBF_FIRE | RBF_RUBBER_BULLET)SET_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[i].pedIndex, RBF_FIRE)
								SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[i].pedIndex, CM_STATIONARY)	#IF	IS_DEBUG_BUILD	PRINTLN("SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[", i,"].pedIndex, CM_STATIONARY)")	#ENDIF
								SET_PED_SUFFERS_CRITICAL_HITS(sEnemySetPiece[i].pedIndex, FALSE)
								SET_PED_MAX_HEALTH(sEnemySetPiece[i].pedIndex, 1000)
								SET_ENTITY_HEALTH(sEnemySetPiece[i].pedIndex, 1000)
								SET_PED_STRAFE_CLIPSET(sEnemySetPiece[i].pedIndex, "move_ped_strafing")
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
						//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, intAbattoir)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, TRUE)
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("ShootoutWave3", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF (iCutsceneStage < 3
		OR TIMERA() < 3000)
		AND NOT HAS_LABEL_BEEN_TRIGGERED("EnteredAbattoir")
			REPEAT COUNT_OF(sEnemyShootout) i
				IF i <> 10
					IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
					AND NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
						SET_PED_RESET_FLAG(sEnemyShootout[i].pedIndex, PRF_BlockWeaponFire, TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
//		IF IS_ENTITY_ON_SCREEN(PLAYER_PED(CHAR_MICHAEL))	
//			IF NOT bDoPlayerShootUpsideDown
//				IF CREATE_CONVERSATION(sPedsForConversation, "MCH2AUD", "MCHHP1", CONV_PRIORITY_MEDIUM)
//					iShootDownMichaelTextProg++
//				ENDIF
//			ELSE
//				IF CREATE_CONVERSATION(sPedsForConversation, "MCH2AUD", "MCHTWP", CONV_PRIORITY_MEDIUM) //Franklin! Throw me a fucking gun!
//					iShootDownMichaelTextProg++
//				ENDIF
//			ENDIF
//		ENDIF
				
//		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			IF NOT bDoPlayerSHootUpsideDown
//				PRINT_ADV("MCH2_RELMCH")
//				
//				IF DOES_ENTITY_EXIST(objHook)
//					SET_OBJECT_TARGETTABLE(objHook, TRUE)
//				ENDIF
//				
//				iShootDownMichaelTextProg++
//			ELSE
//				PRINT_ADV("MCH2_GETCLM") //Get closer to Michael
//				iShootDownMichaelTextProg++
//			ENDIF
//		ENDIF
//			BREAK
//			CASE 2
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
//					IF NOT bDoPlayerSHootUpsideDown
//						IF iShootDownMichaelTimer = 0
//							GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPlayer)
//							
//							IF wtPlayer  = WEAPONTYPE_PUMPSHOTGUN 
//								IF NOT IS_ENTITY_DEAD(sBuddy[MISSION_CHAR_MICHAEL].ped)
//									IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sBuddy[MISSION_CHAR_MICHAEL].ped)
//									OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), objHook)
//									OR IS_PLAYER_FREE_AIMING_AT_ENTITY( PLAYER_ID(), sBuddy[MISSION_CHAR_MICHAEL].ped)
//									OR IS_PLAYER_FREE_AIMING_AT_ENTITY( PLAYER_ID(), objHook)
//										IF CREATE_CONVERSATION(sPedsForConversation, "MCH2AUD", "MCHES4", CONV_PRIORITY_MEDIUM)
//											iShootDownMichaelTextProg++
//										ENDIF
//									ELSE
//										IF CREATE_CONVERSATION(sPedsForConversation, "MCH2AUD", "MCHHP2", CONV_PRIORITY_MEDIUM)
//											iShootDownMichaelTextProg++
//										ENDIF
//									ENDIF
//								ENDIF
//							ELSE
//								IF CREATE_CONVERSATION(sPedsForConversation, "MCH2AUD", "MCHHP2", CONV_PRIORITY_MEDIUM)
//									iShootDownMichaelTextProg++
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF bPlayerSHootingUpsideDown
//							PRINT_HELP_ADV("MCH2_UPSAIM") //Controls for aiming upside down
//							iShootDownMichaelTextProg++
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			CASE 3
//				IF NOT bDoPlayerSHootUpsideDown
//					IF iShootDownMichaelTimer = 0
//						GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPlayer)
//						IF wtPlayer  = WEAPONTYPE_PUMPSHOTGUN 
//							KILL_ANY_CONVERSATION()
//							IF NOT IS_ENTITY_DEAD(sBuddy[MISSION_CHAR_MICHAEL].ped)
//								IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sBuddy[MISSION_CHAR_MICHAEL].ped)
//								OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), objHook)
//								OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sBuddy[MISSION_CHAR_MICHAEL].ped)
//								OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), objHook)
//									CREATE_CONVERSATION_ADV("MCHES4")
//									
//									iShootDownMichaelTextProg++
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//						PRINT_HELP("MCH2_RELMCH2")
//						iShootDownMichaelTextProg++
//					ENDIF
//				ENDIF
//			BREAK
//		ENDSWITCH
		
		//Tyre Burst!
		IF NOT IS_ENTITY_DEAD(vehEnemy[0])
			//CAR MODEL: Bison
			//FRONT LEFT WHEEL OFFSET: <<-1.0000, 1.8000, -0.1500>>
			//FRONT RIGHT WHEEL OFFSET: <<1.0000, 1.8000, -0.1500>>
			//REAR LEFT WHEEL OFFSET: <<1.0000, -1.6000, -0.1500>>
			//REAR RIGHT WHEEL OFFSET: <<-1.0000, -1.6000, -0.1500>>
			//FRONT LEFT DAMAGE OFFSET: <<-1.0000, 0.6500, 0.5000>>
			//FRONT RIGHT DAMAGE OFFSET: <<1.0000, 0.6500, 0.5000>>
			//REAR LEFT DAMAGE OFFSET: <<-1.0000, -2.0000, 0.5000>>
			//REAR RIGHT DAMAGE OFFSET: <<1.0000, -2.0500, 0.5000>>
			//DAMAGE: 100.0000
			//DEFORMATION: 100.0000
			
			IF NOT IS_VEHICLE_TYRE_BURST(vehEnemy[0], SC_WHEEL_CAR_FRONT_RIGHT)
				IF IS_BULLET_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEnemy[0], <<1.0000, 1.8000, -0.1500>>), 2.0)
					SET_VEHICLE_TYRE_BURST(vehEnemy[0], SC_WHEEL_CAR_FRONT_RIGHT)
					SMASH_VEHICLE_WINDOW(vehEnemy[0], SC_WINDOW_FRONT_RIGHT)
					SMASH_VEHICLE_WINDOW(vehEnemy[0], SC_WINDOW_REAR_RIGHT)
					//SET_VEHICLE_DAMAGE(vehEnemy[0], <<1.0000, 0.6500, 0.5000>>, 100.0, 100.0, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_VEHICLE_TYRE_BURST(vehEnemy[0], SC_WHEEL_CAR_REAR_RIGHT)
				IF IS_BULLET_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEnemy[0], <<1.0000, -1.6000, -0.1500>>), 2.0)
					SET_VEHICLE_TYRE_BURST(vehEnemy[0], SC_WHEEL_CAR_REAR_RIGHT)
					//SET_VEHICLE_DAMAGE(vehEnemy[0], <<1.0000, -2.0500, 0.5000>>, 100.0, 100.0, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		//Open Combat
		IF DOES_ENTITY_EXIST(sEnemyShootout[10].pedIndex)
		AND NOT IS_PED_INJURED(sEnemyShootout[10].pedIndex)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyShootout[10].pedIndex)) <= 7.5
				OR GET_ENTITY_HEALTH(sEnemyShootout[10].pedIndex) < 200
				OR IS_PED_RAGDOLL(sEnemyShootout[10].pedIndex)
				OR IS_ENTITY_AT_COORD(sEnemyShootout[10].pedIndex, <<957.4456, -2185.1714, 29.5391>>, <<1.0, 1.0, 3.0>>)
					SET_PED_ALERTNESS(sEnemyShootout[10].pedIndex, AS_MUST_GO_TO_COMBAT)
					
					REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[10].pedIndex)
					SET_COMBAT_FLOAT(sEnemyShootout[10].pedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 0.75)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[10].pedIndex, CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[10].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
					SET_PED_COMBAT_MOVEMENT(sEnemyShootout[10].pedIndex, CM_WILLADVANCE)
					
					SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[10].pedIndex, CA_CAN_CHARGE, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyShootout[10].pedIndex, FALSE)
					
					SET_LABEL_AS_TRIGGERED("OpenCombat", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat0")
			IF DOES_ENTITY_EXIST(sEnemyShootout[1].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[1].pedIndex)
					IF GET_ENTITY_HEALTH(sEnemyShootout[0].pedIndex) < 200
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<963.077393, -2185.471924, 31.150967>>, <<3.0, 2.0, 2.0>>)
						SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyShootout[1].pedIndex, <<978.5643, -2185.9519, 29.0001>>, 2.0)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[1].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[1].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[1].pedIndex, CA_CAN_CHARGE, TRUE)
						
						IF NOT IS_PED_INJURED(sEnemyShootout[0].pedIndex)
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[0].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_MOVEMENT(sEnemyShootout[0].pedIndex, CM_WILLADVANCE)
							
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[0].pedIndex, CA_CAN_CHARGE, TRUE)
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("OpenCombat0", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat1")
			IF DOES_ENTITY_EXIST(sEnemyShootout[3].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[3].pedIndex)
					IF GET_ENTITY_HEALTH(sEnemyShootout[3].pedIndex) < 200
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<982.726868, -2187.271484, 30.727684>>, <<2.0, 2.0, 2.0>>)
					OR (IS_PED_INJURED(sEnemyShootout[0].pedIndex)
					AND IS_PED_INJURED(sEnemyShootout[1].pedIndex)
					AND IS_PED_INJURED(sEnemyShootout[2].pedIndex))
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[3].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[3].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[3].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[3].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat1", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat2")
			IF DOES_ENTITY_EXIST(sEnemyShootout[4].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[4].pedIndex)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<982.726868, -2187.271484, 30.727684>>, <<2.0, 2.0, 2.0>>)
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[4].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[4].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[4].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[4].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat2", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat3")
			IF DOES_ENTITY_EXIST(sEnemyShootout[5].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[5].pedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<993.417542, -2174.615967, 28.490120>>, <<991.907959, -2190.386963, 33.978271>>, 12.5)
					AND IS_ENTITY_ON_SCREEN(PLAYER_PED_ID())
					AND NOT IS_ENTITY_OCCLUDED(PLAYER_PED_ID())
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[5].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[5].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[5].pedIndex, CM_WILLADVANCE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyShootout[5].pedIndex, 500.0)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[5].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat3", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat4")
			IF DOES_ENTITY_EXIST(sEnemyShootout[6].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[6].pedIndex)
					IF IS_PED_INJURED(sEnemyShootout[5].pedIndex)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<996.895630, -2172.183350, 30.974617>>, <<4.0, 2.0, 3.0>>)
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[6].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[6].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[6].pedIndex, CM_WILLADVANCE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyShootout[6].pedIndex, 500.0)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[6].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat4", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat4a")
			IF DOES_ENTITY_EXIST(sEnemyShootout[7].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[7].pedIndex)
					IF IS_PED_INJURED(sEnemyShootout[8].pedIndex)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<984.216492, -2168.708496, 32.463554>>, <<5.0, 4.0, 4.0>>)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<971.6310, -2169.5830, 28.5254>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyShootout[7].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[7].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[7].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[7].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[7].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat4a", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat5")
			IF DOES_ENTITY_EXIST(sEnemyShootout[9].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[9].pedIndex)
					IF (IS_PED_INJURED(sEnemyShootout[6].pedIndex)
					AND IS_PED_INJURED(sEnemyShootout[7].pedIndex)
					AND IS_PED_INJURED(sEnemyShootout[8].pedIndex))
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<968.906372, -2167.271240, 30.966833>>,<<4.0, 4.0, 3.0>>)
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[9].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[9].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[9].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[9].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat5", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("OpenCombat5")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat5a")
				IF DOES_ENTITY_EXIST(sEnemyShootout[15].pedIndex)
					IF NOT IS_PED_INJURED(sEnemyShootout[15].pedIndex)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("EnteredArea5a")
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<963.611145, -2166.840820, 28.225246>>, <<977.714966, -2168.133057, 33.455669>>, 7.0)
								iAdvanceTimer = GET_GAME_TIMER() + 1000
								
								SET_LABEL_AS_TRIGGERED("EnteredArea5a", TRUE)
							ENDIF
						ENDIF
						
						IF HAS_LABEL_BEEN_TRIGGERED("EnteredArea5a")
						AND (GET_GAME_TIMER() > iAdvanceTimer
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<972.798096, -2167.464355, 28.460669>>, <<963.478210, -2166.797852, 33.475464>>, 7.5))
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<970.4205, -2166.4172, 28.4646>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sEnemyShootout[15].pedIndex, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[15].pedIndex)
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[15].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_MOVEMENT(sEnemyShootout[15].pedIndex, CM_WILLADVANCE)
							
							SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[15].pedIndex, CA_CAN_CHARGE, TRUE)
							
							SET_LABEL_AS_TRIGGERED("OpenCombat5a", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FreeEnemy")
			IF DOES_ENTITY_EXIST(sEnemyShootout[2].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[2].pedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<984.336243, -2168.873291, 28.462013>>, <<990.817139, -2169.578857, 33.468571>>, 7.5)
					AND NOT IS_ENTITY_ON_SCREEN(sEnemyShootout[2].pedIndex)
					AND IS_ENTITY_OCCLUDED(sEnemyShootout[2].pedIndex)
						SET_PED_POSITION(sEnemyShootout[2].pedIndex, <<987.8972, -2181.2058, 29.0561>>, 311.3820)
						
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[2].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[2].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[2].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[2].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("FreeEnemy", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat6")	
			IF DOES_ENTITY_EXIST(sEnemyShootout[12].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[12].pedIndex)
					IF IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<969.533569, -2158.632324, 30.974899>>, <<6.0, 6.0, 3.0>>)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<973.709839, -2159.690430, 30.475533>>, <<2.0, 2.0, 2.0>>)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<976.0801, -2159.8713, 28.4758>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
							ENDIF
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyShootout[12].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
						SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyShootout[12].pedIndex, <<991.809692, -2160.414063, 28.580540>>, <<973.678284, -2158.776123, 35.475449>>, 11.0)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[12].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[12].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[12].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat6", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(sEnemyShootout[12].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[12].pedIndex)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<973.709839, -2159.690430, 30.475533>>, <<2.0, 2.0, 2.0>>)
						IF GET_SCRIPT_TASK_STATUS(sEnemyShootout[12].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
							CLEAR_PED_TASKS(sEnemyShootout[12].pedIndex)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyShootout[12].pedIndex, 500.0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat7")
			IF DOES_ENTITY_EXIST(sEnemyShootout[11].pedIndex)
			AND DOES_ENTITY_EXIST(sEnemyShootout[14].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[11].pedIndex)
				AND NOT IS_PED_INJURED(sEnemyShootout[14].pedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<975.501404, -2163.882324, 28.475899>>, <<976.430664, -2153.790771, 32.475471>>, 5.0)
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<989.3090, -2159.1226, 28.4763>>, PLAYER_PED_ID(), PEDMOVE_SPRINT, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<986.5796, -2161.7283, 28.4763>>, PLAYER_PED_ID(), PEDMOVE_SPRINT, TRUE)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyShootout[11].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						FORCE_PED_MOTION_STATE(sEnemyShootout[11].pedIndex, MS_ON_FOOT_RUN)
						
						SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyShootout[11].pedIndex, <<991.809692, -2160.414063, 28.580540>>, <<973.678284, -2158.776123, 35.475449>>, 11.0)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[11].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[11].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[11].pedIndex, CA_CAN_CHARGE, TRUE)
						
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<984.2305, -2157.1267, 30.0791>>, PLAYER_PED_ID(), PEDMOVE_SPRINT, TRUE)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyShootout[14].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						FORCE_PED_MOTION_STATE(sEnemyShootout[14].pedIndex, MS_ON_FOOT_RUN)
						
						SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyShootout[14].pedIndex, <<991.809692, -2160.414063, 28.580540>>, <<973.678284, -2158.776123, 35.475449>>, 11.0)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[14].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[14].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[14].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat7", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat8")
			IF DOES_ENTITY_EXIST(sEnemyShootout[13].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[13].pedIndex)
					IF (IS_PED_INJURED(sEnemyShootout[11].pedIndex)
					AND IS_PED_INJURED(sEnemyShootout[14].pedIndex))
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<981.156494, -2159.278564, 30.976007>>,<<6.0, 5.5, 3.0>>)
						REMOVE_PED_DEFENSIVE_AREA(sEnemyShootout[13].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[13].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemyShootout[13].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemyShootout[13].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_LABEL_AS_TRIGGERED("OpenCombat8", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Runs
		IF NOT HAS_LABEL_BEEN_TRIGGERED("PedRuns")
			IF DOES_ENTITY_EXIST(sEnemyShootout[6].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyShootout[6].pedIndex)
					VECTOR vGameCamRot = GET_GAMEPLAY_CAM_ROT()
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<995.530334, -2190.534180, 28.978443>>, <<996.244202, -2182.337158, 34.977020>>, 6.5)
					OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<985.945251, -2187.553955, 28.977818>>, <<993.839844, -2188.007324, 35.035198>>, 7.0)
					AND (WRAP(vGameCamRot.Z, 0.0, 360.0) < 10.0 OR WRAP(vGameCamRot.Z, 0.0, 360.0) > 280.0)
					AND IS_ENTITY_ON_SCREEN(sEnemyShootout[6].pedIndex))
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<981.6053, -2171.1108, 29.2243>>, PEDMOVE_RUN)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sEnemyShootout[6].pedIndex, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						FORCE_PED_MOTION_STATE(sEnemyShootout[6].pedIndex, MS_ON_FOOT_RUN)
						
						SET_LABEL_AS_TRIGGERED("PedRuns", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Rail
		SWITCH iSetPiece[SET_PIECE_RAIL]
			CASE 0
				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
					IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
						IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece1)
							TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail", <<979.592, -2168.694, 28.539>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
							
							iSetPiece[SET_PIECE_RAIL]++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail")
					SET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail", 0.400)
					SET_ENTITY_ANIM_SPEED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail", 0.0)
					
					iSetPiece[SET_PIECE_RAIL]++
				ENDIF
			BREAK
			CASE 2
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<995.957214, -2169.708740, 31.497860>>, <<5.0, 5.0, 3.0>>)
					IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail")
						SET_ENTITY_ANIM_SPEED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail", 1.0)
						
						iSetPiece[SET_PIECE_RAIL]++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
					IF NOT IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, sAnimDictMic2SetPiece1, "goon_leap_rail")
					OR IS_PED_RAGDOLL(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
						IF NOT IS_PED_RAGDOLL(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, <<986.1974, -2166.4761, 28.4756>>, -1, FALSE, 0.0, TRUE, FALSE, covPoint[7])
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, TRUE)
						ENDIF
						
						iSetPiece[SET_PIECE_RAIL]++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
					IF IS_PED_IN_COVER(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, TRUE)
					OR IS_PED_RAGDOLL(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
					OR GET_SCRIPT_TASK_STATUS(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, SCRIPT_TASK_ANY) != PERFORMING_TASK
//					OR (NOT IS_PED_IN_COVER(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
//					AND NOT IS_PED_GOING_INTO_COVER(sEnemySetPiece[SET_PIECE_RAIL].pedIndex))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, FALSE)
						SET_PED_ALERTNESS(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, AS_MUST_GO_TO_COMBAT)
						TASK_COMBAT_PED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, PLAYER_PED(CHAR_FRANKLIN))	//TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, 1000.0)
						
						REMOVE_PED_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, CA_CAN_CHARGE, TRUE)
						
						iSetPiece[SET_PIECE_RAIL]++
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF iSetPiece[SET_PIECE_RAIL] < 3
			IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
						CLEAR_PED_TASKS(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, FALSE)
						
						SET_PED_SPHERE_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_RAIL].pedIndex, <<986.2274, -2166.6877, 28.4755>>, 2.0)
						
						iSetPiece[SET_PIECE_RAIL] = 100
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
			IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
				REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Acid_Bath")
			ELSE
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Acid_Bath")
			ENDIF
		ENDIF
		
		//Steam
		SWITCH iSetPiece[SET_PIECE_STEAM]
			CASE 0
				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
					PED_BONETAG pbt
					
					GET_PED_LAST_DAMAGE_BONE(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, pbt)
					
					IF GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_STEAM].pedIndex) = GET_PED_MAX_HEALTH(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
						IF NOT IS_ENTITY_AT_COORD(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, <<976.603149, -2166.035400, 32.155220>>, <<1.25, 1.25, 1.75>>)
							SET_PED_POSITION(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, <<976.6239, -2166.3784, 30.4104 + 0.25>>, 266.3925)
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
						CLEAR_PED_TASKS(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, FALSE)
						
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, 105)
						
						iSetPiece[SET_PIECE_STEAM] = 100
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
						IF (GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_STEAM].pedIndex) < 925
						OR pbt = BONETAG_HEAD)
							IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<990.087097, -2169.110107, 32.472157>>, <<10.0, 8.5, 4.0>>)
								AND IS_ENTITY_AT_COORD(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, <<976.603149, -2166.035400, 32.155220>>, <<1.25, 1.25, 1.75>>)
								AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_STEAM].pedIndex), 5.0)
									INFORM_MISSION_STATS_OF_INCREMENT(MIC2_UNIQUE_TRIAD_DEATHS)
									
									SET_PED_CAN_BE_TARGETTED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, FALSE)
									
									//TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin", <<976.661, -2166.402, 31.687>>, <<0.0, 0.0, -124.560>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY | AF_HOLD_LAST_FRAME, 0.02, EULER_YXZ, AIK_DISABLE_LEG_IK)
									sceneSteam = CREATE_SYNCHRONIZED_SCENE(<<976.661, -2166.402, 31.687>>, <<0.0, 0.0, -124.560>>)
									TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sceneSteam, sAnimDictMic2SetPiece1, "goonfall_into_bin", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN, AIK_DISABLE_LEG_IK)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneSteam, TRUE)
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
									
									SET_PED_DROPS_WEAPON(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
									
									//Audio Scene
									IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_ACID")
										START_AUDIO_SCENE("MI_2_SHOOTOUT_ACID")
									ENDIF
									
									//Audio
									PLAY_AUDIO(MIC2_ACID_BATH_OS)
									
									LOAD_AUDIO(MIC2_MULCHED)
									
									PLAY_SOUND_FROM_ENTITY(-1, "ACID_BATH_FALL", sEnemySetPiece[SET_PIECE_STEAM].pedIndex, "MICHAEL_2_SOUNDS")
									
									IF NOT bVideoRecording
										REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
										
										bVideoRecording = TRUE
									ENDIF
									
									SETTIMERB(0)
									iSetPiece[SET_PIECE_STEAM]++
								ELSE
									SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, 5)
									APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, 500, FALSE)
									SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_STEAM].blipIndex)
									
									iSetPiece[SET_PIECE_STEAM] = 100
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneSteam)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin")
					SET_SYNCHRONIZED_SCENE_RATE(sceneSteam, 0.75)	//SET_ENTITY_ANIM_SPEED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin", 0.75)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneSteam)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneSteam) >= 0.400	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin") > 0.400
					//Removed by request - 740685
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_acid_bath_splash", <<975.0, -2166.0, 29.5>>, VECTOR_ZERO)
					
//					SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 1.5)
//					
//					SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 150)
					
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF DOES_ENTITY_EXIST(pedClosestEnemy)
						AND NOT IS_PED_INJURED(pedClosestEnemy)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy)) < 20.0
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedClosestEnemy, "MCH2CHIN4")
								
								CREATE_CONVERSATION_ADV("MCH2_EWW")
							ENDIF
						ENDIF
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_STEAM]++
				ENDIF
			BREAK
			CASE 2
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneSteam)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneSteam) >= 0.7	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_bin") >= 0.7 //0.800
					IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, 5)
						APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, 500, FALSE)
						SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_STEAM].blipIndex)
						SET_ENTITY_VISIBLE(sEnemySetPiece[SET_PIECE_STEAM].pedIndex, FALSE)
					ENDIF
					
//					IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
//						SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeadingTrack)
//					ENDIF
//					
//					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					
//					SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 2.0)
//					
//					STOP_CAM_SHAKING(camMain, TRUE)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_ACID")
						STOP_AUDIO_SCENE("MI_2_SHOOTOUT_ACID")
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_STEAM]++
				ENDIF
			BREAK
			CASE 3
				IF TIMERB() > 1000
//					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_STEAM]++
				ENDIF
			BREAK
		ENDSWITCH
		
		//Stair
		SWITCH iSetPiece[SET_PIECE_STAIR]
			CASE 0
				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
						IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece1)
							TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair", <<974.258, -2155.561, 28.499>> + <<0.81, -0.07, 0.0>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)	//AF_FORCE_START
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
							
							iSetPiece[SET_PIECE_STAIR]++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair")
						SET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair", 0.2)
						SET_ENTITY_ANIM_SPEED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair", 0.0)
						
						iSetPiece[SET_PIECE_STAIR]++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					VECTOR vGameCamRot
					vGameCamRot = GET_GAMEPLAY_CAM_ROT()
					
					IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<972.404663, -2160.942139, 28.475464>>, <<963.123535, -2170.124512, 33.975536>>, 9.0)
					AND (WRAP(vGameCamRot.Z, 0.0, 360.0) < 20.0 OR WRAP(vGameCamRot.Z, 0.0, 360.0) > 240.0))
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<973.534729, -2162.575195, 28.475765>>, <<963.480103, -2161.785400, 33.974361>>, 3.5)
					OR IS_BULLET_IN_ANGLED_AREA(<<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
						IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair")
							SET_ENTITY_ANIM_SPEED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair", 1.0)
							
							iSetPiece[SET_PIECE_STAIR]++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					IF ((IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair")
					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair") > 0.552)	//0.573)
					AND CAN_PED_SEE_HATED_PED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, PLAYER_PED(CHAR_FRANKLIN)))
					OR NOT IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair")
					OR GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_STAIR].pedIndex) < 200
						SET_PED_RESET_FLAG(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, PRF_InstantBlendToAim, TRUE)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, FALSE)
						
						SET_PED_SPHERE_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, <<967.4478, -2157.9443, 28.4746>>, 2.0)
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<963.701782, -2158.603516, 28.474281>>, <<983.335876, -2160.993896, 34.476147>>, 10.0)
							IF NOT IS_PED_IN_COMBAT(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<967.4478, -2157.9443, 28.4746>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
							ENDIF
						ENDIF
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, CA_CAN_CHARGE, TRUE)
						
						iSetPiece[SET_PIECE_STAIR]++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					IF NOT IS_PED_IN_COMBAT(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
						IF (IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair")
						AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, sAnimDictMic2SetPiece1, "goon_rundownstair") > 0.5)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<967.4478, -2157.9443, 28.4746>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
						ENDIF
					ENDIF
					
					SET_PED_RESET_FLAG(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, PRF_InstantBlendToAim, TRUE)
				ENDIF
			BREAK
		ENDSWITCH
		
		IF iSetPiece[SET_PIECE_STAIR] < 3
			IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<980.336243, -2159.359131, 32.683861>>, <<964.162415, -2157.859863, 35.670811>>, 10.0)
						CLEAR_PED_TASKS(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, FALSE)
						
						SET_PED_SPHERE_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_STAIR].pedIndex, <<967.4478, -2157.9443, 28.4746>>, 2.0)
						
						iSetPiece[SET_PIECE_STAIR] = 100
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
			IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
				REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Kidnap_Mincer")
			ELSE
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Kidnap_Mincer")
			ENDIF
		ENDIF
		
		//Grinder
		SWITCH iSetPiece[SET_PIECE_GRINDER]
			CASE 0
				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
					PED_BONETAG pbt
					
					GET_PED_LAST_DAMAGE_BONE(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, pbt)
					
					IF GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex) = GET_PED_MAX_HEALTH(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
						IF NOT IS_ENTITY_AT_COORD(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, <<990.705078, -2161.330322, 32.166130>>, <<2.0, 2.0, 2.0>>)
							SET_PED_POSITION(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, <<990.69, -2161.42, 30.4404>>, 82.2840)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
						IF (GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex) < 925
						OR pbt = BONETAG_HEAD)
							IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<975.189087, -2158.689941, 30.939890>>, <<12.0, 7.0, 2.25>>)
								AND IS_ENTITY_AT_COORD(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, <<990.705078, -2161.330322, 32.166130>>, <<2.0, 2.0, 2.0>>)
								AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex), 5.0)
									INFORM_MISSION_STATS_OF_INCREMENT(MIC2_UNIQUE_TRIAD_DEATHS)
									
									SET_PED_CAN_BE_TARGETTED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, FALSE)
									
									//TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_grinder", <<990.626, -2161.452, 31.691>>, <<0.0, 0.0, 90.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)	//, 0.04
									sceneGrinder = CREATE_SYNCHRONIZED_SCENE(<<990.626, -2161.452, 31.691>>, <<0.0, 0.0, 90.0>>)
									TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, sceneGrinder, sAnimDictMic2SetPiece1, "goonfall_into_grinder", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN, AIK_DISABLE_LEG_IK)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneGrinder, TRUE)
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
									
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
									
									//Audio Scene
									IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_1")
										START_AUDIO_SCENE("MI_2_GRINDER_1")
									ENDIF
									
									//Audio
									PLAY_AUDIO(MIC2_MULCHED)
									
									LOAD_AUDIO(MIC2_SPINNING_BLADES)
									
									IF NOT bVideoRecording
										REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
										
										bVideoRecording = TRUE
									ENDIF
									
									SETTIMERB(0)
									iSetPiece[SET_PIECE_GRINDER]++
								ELSE
									APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, 500, FALSE)	//SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, 5)
									SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_GRINDER].blipIndex)
									
									iSetPiece[SET_PIECE_GRINDER] = 100
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrinder)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_grinder")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrinder) >= 0.409	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_grinder") > 0.409	//0.265
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_abattoir_ped_minced", <<992.7037, -2161.6229, 31.8097 - 1.0>>, VECTOR_ZERO)
					
					PLAY_SOUND_FROM_ENTITY(-1, "MINCER_FALL", sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, "MICHAEL_2_SOUNDS")	//<<992.7037, -2161.9229, 31.8097 - 0.5>>
					
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF DOES_ENTITY_EXIST(pedClosestEnemy)
						AND NOT IS_PED_INJURED(pedClosestEnemy)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy)) < 20.0
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedClosestEnemy, "MCH2CHIN3")
								
								CREATE_CONVERSATION_ADV("MCH2_EWW2")
							ENDIF
						ENDIF
					ENDIF
					
//					SET_CAM_PARAMS(camMain, 
//										<<991.046021,-2158.079102,30.699644>>,
//										<<7.825684,-0.000000,-155.579086>>,
//										23.640654, 
//										0)
//					
//					SET_CAM_PARAMS(camMain, 
//										<<991.211609,-2158.388672,30.747850>>,
//										<<5.259593,-0.000000,-153.277069>>,
//										23.640654, 
//										2000,
//										GRAPH_TYPE_LINEAR,
//										GRAPH_TYPE_LINEAR)
//					
//					SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 2.0)
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_GRINDER]++
				ENDIF
			BREAK
			CASE 2
//				HIDE_HUD_AND_RADAR_THIS_FRAME()
//				
//				SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrinder)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_grinder")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrinder) >= 0.9	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, sAnimDictMic2SetPiece1, "goonfall_into_grinder") >= 0.9 //1.000
					IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, 5)
						APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex, 500, FALSE)
						SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_GRINDER].blipIndex)
					ENDIF
					
					SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
					
//					IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
//						SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeadingTrack)
//					ENDIF
					
//					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					
//					STOP_CAM_SHAKING(camMain, TRUE)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_1")
						STOP_AUDIO_SCENE("MI_2_GRINDER_1")
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_GRINDER]++
				ENDIF
			BREAK
			CASE 3
				IF TIMERB() > 1000
//					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_GRINDER]++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(objCutter)
			REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Meat_Chopper")
		ELSE
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Meat_Chopper")
		ENDIF
		
		//Cutter
		SWITCH iSetPiece[SET_PIECE_CUTTER]
			CASE 0
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<996.487732, -2158.077881, 32.500244>>, <<25.0, 25.0, 4.0>>)
					LOAD_STREAM("MICHAEL_2_GOON_CHOPPED_APART_MASTER")
				ENDIF
				
				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<996.487732, -2158.077881, 32.500244>>, <<15.0, 15.0, 4.0>>)
						TASK_AIM_GUN_AT_ENTITY(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, PLAYER_PED(CHAR_MICHAEL), -1)
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_CUTTER]++
					ENDIF
					
					IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), 5.0)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), <<5.0, 5.0, 4.0>>)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, FALSE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 100.0)
						
						REMOVE_PED_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CA_CAN_CHARGE, TRUE)
						
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 150)
						
						SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
						CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
						
						iSetPiece[SET_PIECE_CUTTER] = 100
					ENDIF
				ENDIF
			BREAK
			CASE 1
				LOAD_STREAM("MICHAEL_2_GOON_CHOPPED_APART_MASTER")
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<996.487732, -2158.077881, 32.500244>>, <<6.0, 7.5, 4.0>>)
				AND NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					TASK_AIM_GUN_AT_ENTITY(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, PLAYER_PED(CHAR_MICHAEL), -1)
					
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, "MCH2GOON")
					
					CREATE_CONVERSATION_ADV("MCH2_GSHT")
					
					//Audio Scene
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_THE_GUARD")
						START_AUDIO_SCENE("MI_2_SHOOT_THE_GUARD")
					ENDIF
					
					iBulletTimer = -1
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CUTTER]++
				ENDIF
				
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), 5.0)
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), <<5.0, 5.0, 4.0>>)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, FALSE)
					
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 100.0)
					
					REMOVE_PED_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
					SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CM_WILLADVANCE)
					
					SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CA_CAN_CHARGE, TRUE)
					
					SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 150)
					
					SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
					CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
					
					iSetPiece[SET_PIECE_CUTTER] = 100
				ENDIF
			BREAK
			CASE 2
				LOAD_STREAM("MICHAEL_2_GOON_CHOPPED_APART_MASTER")
				
				PED_BONETAG pbt
				
				GET_PED_LAST_DAMAGE_BONE(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, pbt)
				
				IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF (GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex) < 975
					OR pbt = BONETAG_HEAD)
						IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
							IF IS_ENTITY_AT_COORD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, <<998.189453, -2143.476318, 30.476349>>, <<1.5, 1.5, 2.25>>)
							AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<993.761047, -2156.059082, 30.976448>>, <<8.0, 10.0, 2.75>>)
							AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), 5.0)
								INFORM_MISSION_STATS_OF_INCREMENT(MIC2_UNIQUE_TRIAD_DEATHS)
								
								IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_GSHT")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								
								IF LOAD_STREAM("MICHAEL_2_GOON_CHOPPED_APART_MASTER")
									PLAY_STREAM_FROM_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)	//PLAY_STREAM_FRONTEND()
								ENDIF
								
								SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
								
								fHeadingTrack = GET_ENTITY_HEADING(PLAYER_PED_ID())
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								SET_CAM_PARAMS(camMain, 
												<<996.938293,-2145.933838,31.029486>>,
												<<-28.754696,-0.006011,-40.283401>>,
												21.381561, 
												0)
								
								SET_CAM_PARAMS(camMain, 
												<<996.989197,-2145.752930,30.981258>>,
												<<-22.393389,0.499936,-24.676105>>,
												21.381561, 
												1300, 
												GRAPH_TYPE_LINEAR, 
												GRAPH_TYPE_LINEAR)
								
								SHAKE_CAM(camMain, "HAND_SHAKE", 1.0)
								
								SET_CAM_ACTIVE(camMain, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
								
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								
								SET_PED_CAN_BE_TARGETTED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, FALSE)
								
								TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer", <<997.507, -2143.002, 28.507>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_FORCE_START | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY | AF_ENDS_IN_DEAD_POSE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
								PLAY_FACIAL_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, "Goon_MeatSlicer_Facial", sAnimDictMic2SetPiece1)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
								
								IF DOES_ENTITY_EXIST(objCutter)
									IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMachine)
										IF IS_ENTITY_PLAYING_ANIM(objCutter, sAnimDictMachine, "beefsplitter_loop")
											SET_ENTITY_ANIM_CURRENT_TIME(objCutter, sAnimDictMachine, "beefsplitter_loop", 0.600)
										ENDIF
									ENDIF
								ENDIF
								
								//Audio Scene
								IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_THE_GUARD")
									STOP_AUDIO_SCENE("MI_2_SHOOT_THE_GUARD")
								ENDIF
								
								IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_MEAT_SLICER")
									START_AUDIO_SCENE("MI_2_MEAT_SLICER")
								ENDIF
								
								IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioSpinningBlades")
									//Audio
									PLAY_AUDIO(MIC2_SPINNING_BLADES)
									
									SET_LABEL_AS_TRIGGERED("AudioSpinningBlades", TRUE)
								ENDIF
								
								IF NOT bVideoRecording
									REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
									
									bVideoRecording = TRUE
								ENDIF
								
								SETTIMERB(0)
								iSetPiece[SET_PIECE_CUTTER]++
							ELSE	#IF IS_DEBUG_BUILD	PRINTLN("PEDS NOT IN THEIR POSITIONS")	#ENDIF
								SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 5)
								APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 500, FALSE)
								SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
								CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
								
								iSetPiece[SET_PIECE_CUTTER] = 100
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF iSetPiece[SET_PIECE_CUTTER] = 2
					IF TIMERB() > 7000
					OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), 5.0)
					OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), 5.0)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex), <<5.0, 5.0, 4.0>>)
						#IF IS_DEBUG_BUILD	PRINTLN("TOO LONG, SHOT WRONG OR OUT OF PLACE")	#ENDIF
						IF iBulletTimer = -1
							iBulletTimer = GET_GAME_TIMER() + 100
						ELIF GET_GAME_TIMER() > iBulletTimer
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 100.0)
							
							REMOVE_PED_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
							SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CM_WILLADVANCE)
							
							SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, CA_CAN_CHARGE, TRUE)
							
							SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 150)
							
							SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
							CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
							
							iSetPiece[SET_PIECE_CUTTER] = 100
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer") > 0.128 //0.200
						SET_CAM_PARAMS(camMain, 
										<<998.183350,-2143.903076,29.707603>>,
										<<-0.967933,0.515198,-5.987529>>,
										39.959354,
										0)
						
						SET_CAM_PARAMS(camMain, 
										<<998.147949,-2143.089355,29.527300>>,
										<<48.835812,0.311738,-7.783955>>,
										40.035347, 
										3500, 
										GRAPH_TYPE_DECEL, 
										GRAPH_TYPE_DECEL)
						
						SHAKE_CAM(camMain, "HAND_SHAKE", 1.0)
						
						PLAY_PAIN(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, AUD_DAMAGE_REASON_SCREAM_PANIC)
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_CUTTER]++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer") > 0.350
//						SET_CAM_PARAMS(camMain, 
//										<<998.295959,-2141.102539,32.364044>>,
//										<<-83.002708,-2.292470,139.844986>>,
//										32.875706, 
//										0)
//						
//						SET_CAM_PARAMS(camMain, 
//										<<998.144470,-2140.178467,31.618706>>,
//										<<-85.997765,-2.292466,150.822296>>,
//										32.875706, 
//										4000, 
//										GRAPH_TYPE_DECEL, 
//										GRAPH_TYPE_DECEL)
//						
//						SHAKE_CAM(camMain, "HAND_SHAKE", 1.6)
						
						IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
							SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeadingTrack)
						ENDIF
						
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						STOP_CAM_SHAKING(camMain, TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						
						PLAY_PAIN(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, AUD_DAMAGE_REASON_SCREAM_TERROR)
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_CUTTER]++
					ENDIF
				ENDIF
			BREAK
			CASE 5
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer") > 0.400
//						SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 4.0)
//						
//						PLAY_PAIN(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, AUD_DAMAGE_REASON_ON_FIRE)
//						
//						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_abattoir_ped_sliced", sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, <<0.0, -0.1, -0.25>>, <<0.0, 0.0, 84.0 + 90.0>>, 0.6)
//						
//						SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 200)
//						
//						IF NOT IS_MESSAGE_BEING_DISPLAYED()
//						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							IF DOES_ENTITY_EXIST(pedClosestEnemy)
//							AND NOT IS_PED_INJURED(pedClosestEnemy)
//								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy)) < 20.0
//									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, pedClosestEnemy, "MCH2CHIN1")
//									
//									CREATE_CONVERSATION_ADV("MCH2_EWW3")
//								ENDIF
//							ENDIF
//						ENDIF
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_CUTTER]++
					ENDIF
				ENDIF
			BREAK
			CASE 6
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF NOT IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					OR (IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer") >= 0.460)	//0.550)
						PLAY_PAIN(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, AUD_DAMAGE_REASON_ON_FIRE)
						
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_abattoir_ped_sliced", sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, <<0.0, -0.1, -0.25>>, <<0.0, 0.0, 84.0 + 90.0>>, 0.6)
						
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.725, 0.434,  302.360, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.624, 0.434,  310.913, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.519, 0.434,  317.949, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.810, 0.434,  293.645, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.921, 0.434,  280.271, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_RIGHT_ARM), 0.000, 0.413,  337.557, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_RIGHT_ARM), 0.259, 0.413,  330.867, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_LEFT_LEG), 0.836, 0.698,  283.194, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_LEFT_LEG), 0.704, 0.698,  292.980, 1.0, 1, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_LEFT_LEG), 0.587, 0.667,  301.741, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_RIGHT_LEG), 0.164, 0.693,  320.334, 1.0, 0, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_RIGHT_LEG), 0.376, 0.688,  312.187, 1.0, 1, 0.0, "ShotgunLargeMonolithic")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.730, 0.614,  301.073, 1.0, 1, 0.0, "Scripted_Ped_Splash_Back")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.730, 0.444,  315.623, 1.0, 2, 0.0, "Scripted_Ped_Splash_Back")
						APPLY_PED_BLOOD_SPECIFIC(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, ENUM_TO_INT(PDZ_TORSO), 0.730, 0.339,  330.850, 1.0, 0, 0.0, "Scripted_Ped_Splash_Back")
						
						//Audio Scene
						IF IS_AUDIO_SCENE_ACTIVE("MI_2_MEAT_SLICER")
							STOP_AUDIO_SCENE("MI_2_MEAT_SLICER")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_THROW_THE_GUN")
							START_AUDIO_SCENE("MI_2_THROW_THE_GUN")
						ENDIF
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_CUTTER]++
					ENDIF
				ELSE
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					STOP_CAM_SHAKING(camMain, TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("MI_2_MEAT_SLICER")
						STOP_AUDIO_SCENE("MI_2_MEAT_SLICER")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_THROW_THE_GUN")
						START_AUDIO_SCENE("MI_2_THROW_THE_GUN")
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CUTTER]++
				ENDIF
			BREAK
			CASE 7
				//HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					IF NOT IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					OR (IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer") >= 1.000)
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 5)
						APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, 500, FALSE)
						
						SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
						CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_CUTTER]++
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
					CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CUTTER]++
				ENDIF
			BREAK
			CASE 8
				IF TIMERB() > 1000
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CUTTER]++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			IF iSetPiece[SET_PIECE_CUTTER] > 2
			AND iSetPiece[SET_PIECE_CUTTER] < 4
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
						IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
							SET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer", 0.99)
						ENDIF
						
//						IF IS_STREAM_PLAYING()
//							STOP_STREAM()
//						ENDIF
						
						iSetPiece[SET_PIECE_CUTTER] = 4
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioSpinningBlades")
			IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
				IF IS_PED_INJURED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
					//Audio
					PLAY_AUDIO(MIC2_SPINNING_BLADES)
					
					SET_LABEL_AS_TRIGGERED("AudioSpinningBlades", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF (intAbattoir != NULL
		AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intAbattoir)
			IF NOT DOES_BLIP_EXIST(blipMichael)
				IF HAS_LABEL_BEEN_TRIGGERED("MCH2_RESCUE")
					SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				ENDIF
			ENDIF
			
//			IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<995.975464, -2148.494629, 33.499943>>, <<25.0, 25.0, 15.0>>)
//				//Cutscene
//				REQUEST_CUTSCENE("mic_2_mcs_2")
//				
//				//Request Cutscene Variations - mic_2_mcs_2
//				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
//				ENDIF
//			ENDIF
			
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("AbandonMichael")
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_FIND1")
//						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<994.403198, -2184.370850, 28.477783>>, <<996.124634, -2166.494141, 32.963730>>, 10.0)
//							CREATE_CONVERSATION_ADV("MCH2_FIND1")
//						ENDIF
//					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_FIND2")
//						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<963.563538, -2165.346924, 28.029001>>, <<993.229797, -2167.891113, 32.475231>>, 10.0)
//							CREATE_CONVERSATION_ADV("MCH2_FIND2")
//						ENDIF
//					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_FIND3")
//						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<973.843018, -2158.792725, 27.975471>>, <<999.225342, -2160.920166, 32.476738>>, 10.0)
//							CREATE_CONVERSATION_ADV("MCH2_FIND3")
//						ENDIF
//					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HLP1")
						//Far from Michael
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<963.563843, -2166.085693, 28.475433>>, <<974.759644, -2167.140625, 34.459885>>, 5.250)
							SET_LABEL_AS_TRIGGERED("ApproachingMichael", FALSE)
						ENDIF
						//Near to Michael
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<963.502747, -2160.814941, 28.474331>>, <<974.999939, -2161.838135, 34.475826>>, 5.250)
							SET_LABEL_AS_TRIGGERED("ApproachingMichael", TRUE)
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("ApproachingMichael")
							IF GET_GAME_TIMER() > iDialogueTimer[2]
								IF iDialogueLineCount[4] = -1 
									 iDialogueLineCount[4] = 8
								ELIF iDialogueLineCount[4] > 0
									iDialogueTimer[2] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 6000)
									
									iDialogueLineCount[4]--
									
									CREATE_CONVERSATION_ADV("MCH2_LOOK", CONV_PRIORITY_MEDIUM, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF HAS_LABEL_BEEN_TRIGGERED("ApproachingMichael")
							IF GET_GAME_TIMER() > iDialogueTimer[2]
								IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_STUCK") AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_STUCK2")
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 15.0
										CREATE_CONVERSATION_ADV("MCH2_STUCK")	//Near
									ELSE
										CREATE_CONVERSATION_ADV("MCH2_STUCK2")	//Far
									ENDIF
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HLP1_01")
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_HLP1", "MCH2_HLP1_01")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HLP1_02")
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_HLP1", "MCH2_HLP1_02")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HLP1_03")
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_HLP1", "MCH2_HLP1_03")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HLP1_04")
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_HLP1", "MCH2_HLP1_04")
								ENDIF
								
								iDialogueTimer[2] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 6000)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("MCH2_GSHT")
			OR (DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
			AND IS_PED_INJURED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex))
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_GREET1")
//						IF IS_GAMEPLAY_CAM_RENDERING()
//							IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<995.128967, -2150.473389, 31.476431>>, <<7.0, 16.0, 3.0>>)
//								CREATE_CONVERSATION_ADV("MCH2_GREET1")
//							ENDIF
//						ENDIF
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_STUCK") AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_STUCK2")
						CREATE_CONVERSATION_ADV("MCH2_STUCK")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_GREET2")
						IF IS_GAMEPLAY_CAM_RENDERING()
							IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<995.128967, -2150.473389, 31.476431>>, <<7.0, 16.0, 3.0>>)
								IF GET_GAME_TIMER() > iDialogueTimer[1]
									CREATE_CONVERSATION_ADV("MCH2_GREET2")
									
									iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5000)
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TWPM")
						IF IS_GAMEPLAY_CAM_RENDERING()
							IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<995.128967, -2150.473389, 31.476431>>, <<7.0, 16.0, 3.0>>)
								IF GET_GAME_TIMER() > iDialogueTimer[1]
									CREATE_CONVERSATION_ADV("MCH2_TWPM")
									
									iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5000)
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_HLP2")
						IF IS_GAMEPLAY_CAM_RENDERING()
							IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<995.128967, -2150.473389, 31.476431>>, <<7.0, 16.0, 3.0>>)
								IF GET_GAME_TIMER() > iDialogueTimer[1]
									CREATE_CONVERSATION_ADV("MCH2_HLP2")
									
									iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 5000)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_GAMEPLAY_CAM_RENDERING()
							IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<988.615295, -2144.727051, 31.475969>>, <<12.0, 20.0, 3.0>>)
								IF GET_GAME_TIMER() > iDialogueTimer[1]
									IF iDialogueLineCount[3] = -1 
										 iDialogueLineCount[3] = 5
									ELIF iDialogueLineCount[3] > 0
										iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 6000)
										
										iDialogueLineCount[3]--
										
										CREATE_CONVERSATION_ADV("MCH2_HELP", CONV_PRIORITY_MEDIUM, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<995.975464, -2148.494629, 33.499943>>, <<8.0, 8.0, 5.0>>)
				IF IS_ENTITY_ON_SCREEN(PLAYER_PED(CHAR_MICHAEL))
					IF IS_GAMEPLAY_CAM_RENDERING()
						FLOAT fDistance = 50.0
						
						REPEAT COUNT_OF(sEnemyShootout) i
							IF DOES_ENTITY_EXIST(sEnemyShootout[i].pedIndex)
							AND NOT IS_PED_INJURED(sEnemyShootout[i].pedIndex)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyShootout[i].pedIndex)) < fDistance
									fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemyShootout[i].pedIndex))
								ENDIF
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(sEnemySetPiece) i
							IF i <> ENUM_TO_INT(SET_PIECE_STEAM)
								IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
								AND NOT IS_PED_INJURED(sEnemySetPiece[i].pedIndex)
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemySetPiece[i].pedIndex)) < fDistance
									AND NOT IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[i].pedIndex, sAnimDictMic2SetPiece1, "goon_meatslicer")
									AND iSetPiece[SET_PIECE_CUTTER] <= 4
										fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sEnemySetPiece[i].pedIndex))
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
						
						IF fDistance > 30.0
							PRINT_HELP_ADV("MCH2_THRWP", TRUE, TRUE)
							
							SET_LABEL_AS_TRIGGERED("AbandonMichael", TRUE)
							
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
							
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
							
								ADVANCE_STAGE()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MCH2_THRWP")
					SAFE_CLEAR_HELP()
					
					SET_LABEL_AS_TRIGGERED("MCH2_THRWP", FALSE)
				ENDIF
			ENDIF
			
			//Audio
			IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioEnter")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<962.123413, -2184.951416, 28.976068>>, <<969.415161, -2185.648193, 33.977135>>, 5.0)
					PLAY_AUDIO(MIC2_FIGHT_CONT)
					
					LOAD_AUDIO(MIC2_ACID_BATH_OS)
					
					SET_LABEL_AS_TRIGGERED("AudioEnter", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioProgress")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<994.319214, -2166.333984, 28.476685>>, <<993.632202, -2173.296875, 34.201157>>, 14.0)
					PLAY_AUDIO(MIC2_ABATTOIR_PROGRESS)
					
					SET_LABEL_AS_TRIGGERED("AudioProgress", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("AbandonMichael")
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) > 20.0
				IF NOT HAS_LABEL_BEEN_TRIGGERED("Abandon1")
					PRINT_ADV("CMN_MLEAVE", DEFAULT_GOD_TEXT_TIME, FALSE)
					
					SET_LABEL_AS_TRIGGERED("Abandon1", TRUE)
				ENDIF
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) > 30.0
				eMissionFail = failMichaelAbandon
				
				missionFailed()
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("MCH2_ABATTF")
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) > 250.0
				eMissionFail = failMichaelAbandon
				
				missionFailed()
			ENDIF
		ENDIF
		
		//Clear blocking vehicle
		IF (intAbattoir = NULL
		OR (intAbattoir != NULL
		AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != intAbattoir))
		AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<936.760864, -2183.205811, 39.011833>>, <<30.0, 20.0, 10.0>>)
			VEHICLE_INDEX vehClosest = GET_CLOSEST_VEHICLE(<<962.19891, -2184.75244, 29.47602>>, 7.5, DUMMY_MODEL_FOR_SCRIPT, ENUM_TO_INT(VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES))
			
			IF DOES_ENTITY_EXIST(vehClosest)
				PRINTLN("vehClosest = ", NATIVE_TO_INT(vehClosest), " [model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehClosest)), "]")
				IF NOT IS_VEHICLE_DRIVEABLE(vehClosest)
					PRINTLN("vehClosest is not driveable")
					IF NOT IS_ENTITY_ON_SCREEN(vehClosest)
					OR IS_ENTITY_OCCLUDED(vehClosest)
						PRINTLN("vehClosest is out of sight")
						IF IS_ENTITY_A_MISSION_ENTITY(vehClosest)
							PRINTLN("vehClosest is a mission vehicle, marking as no longer needed")
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehClosest)
						ENDIF
						
						CLEAR_AREA_OF_VEHICLES(<<962.19891, -2184.75244, 29.47602>>, 7.5)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		INT i
		
		REPEAT COUNT_OF(objCigarette) i
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[i])
				STOP_PARTICLE_FX_LOOPED(ptfxCigarette[i])
			ENDIF
			SAFE_DELETE_OBJECT(objCigarette[i])
		ENDREPEAT
		
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_CIGGY_01)
		
		IF DOES_ENTITY_EXIST(vehFranklin)
		AND NOT IS_ENTITY_DEAD(vehFranklin)
			IF IS_ENTITY_A_MISSION_ENTITY(vehFranklin)
			AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehFranklin)
				SET_MISSION_VEHICLE_GEN_VEHICLE(vehFranklin, <<930.7598, -2158.7644, 29.3458>>, 174.4858)
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehFranklin)
			ENDIF
		ENDIF
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_MAIN")
			STOP_AUDIO_SCENE("MI_2_SHOOTOUT_MAIN")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOTOUT_ACID")
			STOP_AUDIO_SCENE("MI_2_SHOOTOUT_ACID")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_1")
			STOP_AUDIO_SCENE("MI_2_GRINDER_1")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_THE_GUARD")
			STOP_AUDIO_SCENE("MI_2_SHOOT_THE_GUARD")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_MEAT_SLICER")
			STOP_AUDIO_SCENE("MI_2_MEAT_SLICER")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_THROW_THE_GUN")
			STOP_AUDIO_SCENE("MI_2_THROW_THE_GUN")
		ENDIF
		
		REPEAT COUNT_OF(sEnemyShootout) i
			SAFE_DELETE_PED(sEnemyShootout[i].pedIndex)
		ENDREPEAT
		
		REPEAT COUNT_OF(sEnemySetPiece) i
			IF i <> ENUM_TO_INT(SET_PIECE_CUTTER)
				SAFE_DELETE_PED(sEnemySetPiece[i].pedIndex)
			ENDIF
		ENDREPEAT
		
		CLEAR_TEXT()
		
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		
		SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CUTTER].blipIndex)
		CLEANUP_AI_PED_BLIP(blipStructSetPiece[SET_PIECE_CUTTER])
		
		//Trackify
		ENABLE_SECOND_SCREEN_TRACKIFY_APP(FALSE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC switchToMichael
	IF INIT_STAGE()
		//Replay
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayMichaelEscape), "stageMichaelEscape")
		
		//Uncapped
		bSetUncapped = TRUE
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, FALSE)
		
		IF CAN_CREATE_RANDOM_COPS()
			SET_CREATE_RANDOM_COPS(FALSE)
		ENDIF
		
		//Radar
		bRadar = FALSE
		
		//Rail
		bOkToHangFromHook = FALSE
		
//		//Skip Michael Forwards!
//		iCurrentNode = 31
//		vRailCurrent = sRailNodes[31].vPos
//		SET_ENTITY_COORDS_NO_OFFSET(objHook, sRailNodes[31].vPos)
//		SET_ENTITY_ROTATION(objHook, sRailNodes[31].vRot)
//		SAFE_FREEZE_ENTITY_POSITION(objHook, TRUE)
		
		//Navmesh Blocking Areas
		IF iNavBlock[1] = 0
			iNavBlock[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<992.0, -2121.8, 29.5>>, <<4.0, 20.0, 10.0>>, (-5.0 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[1] = ", iNavBlock[1])
		ENDIF
		
		IF iNavBlock[2] = 0
			iNavBlock[2] = ADD_NAVMESH_BLOCKING_OBJECT(<<1001.1, -2121.8, 29.5>>, <<4.6, 20.0, 10.0>>, (-5.0 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[2] = ", iNavBlock[2])
		ENDIF
		
		IF iNavBlock[3] = 0
			iNavBlock[3] = ADD_NAVMESH_BLOCKING_OBJECT(<<984.1, -2135.5, 29.0>>, <<8.3, 8.0, 10.0>>, (-5.250 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[3] = ", iNavBlock[3])
		ENDIF
		
		//Setup Franklin as the buddy
		SET_ENTITY_PROOFS(PLAYER_PED(CHAR_FRANKLIN), TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_FRANKLIN), FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
		
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
		
		REQUEST_WEAPON_ASSET(WEAPONTYPE_COMBATPISTOL)
		
		IF bSkipped
		#IF IS_DEBUG_BUILD
		OR bAutoSkipping
		#ENDIF
			INT i
				
			REPEAT COUNT_OF(sEnemyOutside) i
				SAFE_DELETE_PED(sEnemyOutside[i].pedIndex)
			ENDREPEAT
			
			REPEAT COUNT_OF(sEnemyShootout) i
				SAFE_DELETE_PED(sEnemyShootout[i].pedIndex)
			ENDREPEAT
			
			SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_RAIL].pedIndex)
			SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_STEAM].pedIndex)
			SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_STAIR].pedIndex)
			SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_GRINDER].pedIndex)
			SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
//		//Cutscene
//		REQUEST_CUTSCENE("mic_2_mcs_2")
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<998.7538, -2150.1536, 28.5002>>, 90.6113, FALSE)
			
			//Freeze
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			
			//Pin Interior
			IF intAbattoir = NULL
				intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<990.9518, -2178.6208, 29.0257>>, "v_abattoir")
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(intAbattoir)
			
			WHILE NOT IS_INTERIOR_READY(intAbattoir)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedAbattoir = TRUE
			
			//Unfreeze
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<996.5976, -2148.9912, 29.0>>)
			ENDIF
			
			//Audio
			PLAY_AUDIO(MIC2_HANGING_RT)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
//		//Request Cutscene Variations - mic_2_mcs_2
//		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
//		ENDIF
		
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2159461
		
		SWITCH iCutsceneStage
			CASE 0
				IF SAFE_START_CUTSCENE(10.0)
					//Force the switch to Michael
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
					ENDIF
					
					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					bRadar = FALSE
					
					CLEAR_TEXT()
					
					GET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon)
					
					SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED, TRUE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_UNARMED, TRUE)
					
					//Skip Michael Forwards!
					iCurrentNode = 31
					vRailCurrent = sRailNodes[31].vPos
					SET_ENTITY_COORDS_NO_OFFSET(objHook, sRailNodes[31].vPos)
					SET_ENTITY_ROTATION(objHook, <<0.0, 0.0, -6.6>>)	//sRailNodes[31].vRot)
					FREEZE_ENTITY_POSITION(objHook, TRUE)
					
					DETACH_ENTITY(PLAYER_PED(CHAR_MICHAEL))
					
					IF NOT DOES_ENTITY_EXIST(objGun)
						objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_COMBATPISTOL), sRailNodes[31].vPos)
					ENDIF
					
					sceneSwitch = CREATE_SYNCHRONIZED_SCENE(sceneSwitchPos, sceneSwitchRot)
					
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_MICHAEL), sceneSwitch, sAnimDictMic2Switch, "meathook_gun_switch_mic", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneSwitch, sAnimDictMic2Switch, "meathook_gun_switch_fra", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objGun, sceneSwitch, "meathook_gun_switch_pistol", sAnimDictMic2Switch, INSTANT_BLEND_IN)
					
					//Cam
					IF NOT DOES_CAM_EXIST(camAnim)
						camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					ENDIF
					
					PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneSwitch, "meathook_gun_switch_cam", sAnimDictMic2Switch)
					
					SET_CAM_ACTIVE(camAnim, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					WHILE NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_COMBATPISTOL)
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					IF NOT DOES_ENTITY_EXIST(sEnemyBackup[0].pedIndex)
						createEnemy(sEnemyBackup[0], <<997.7915, -2118.1855, 29.4757>>, 174.1204, WEAPONTYPE_PUMPSHOTGUN)
						//sEnemyBackup[0].iTime[0] = 0	sEnemyBackup[0].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[0].vPoint[0] = <<993.5497, -2142.2900, 28.4761>>	sEnemyBackup[0].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
						SET_PED_RESET_FLAG(sEnemyBackup[0].pedIndex, PRF_InstantBlendToAim, TRUE)
						TASK_COMBAT_PED(sEnemyBackup[0].pedIndex, PLAYER_PED(CHAR_MICHAEL))
						SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[0].pedIndex, <<993.5497, -2142.2900, 28.4761>>, 2.0)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[0].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						//RETAIN_ENTITY_IN_INTERIOR(sEnemyBackup[0].pedIndex, intAbattoir)
					ENDIF
					IF NOT DOES_ENTITY_EXIST(sEnemyBackup[1].pedIndex)
						createEnemy(sEnemyBackup[1], <<996.0300, -2123.4690, 29.4757>>, 174.1522, WEAPONTYPE_ASSAULTRIFLE)
						//sEnemyBackup[1].iTime[0] = 0	sEnemyBackup[1].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[1].vPoint[0] = <<995.7127, -2135.2080, 29.4761>>	sEnemyBackup[1].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
						SET_PED_RESET_FLAG(sEnemyBackup[1].pedIndex, PRF_InstantBlendToAim, TRUE)
						TASK_COMBAT_PED(sEnemyBackup[1].pedIndex, PLAYER_PED(CHAR_MICHAEL))
						SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[1].pedIndex, <<995.7127, -2135.2080, 29.4761>>, 1.5)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[1].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						//RETAIN_ENTITY_IN_INTERIOR(sEnemyBackup[1].pedIndex, intAbattoir)
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION_ADV("MCH2_TWPF")
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchFX[Flash1]")
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneSwitch)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneSwitch) >= 0.375)
						PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						IF sfxWoosh = -1
							sfxWoosh = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(sfxWoosh, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						ENDIF
						ANIMPOSTFX_PLAY("SwitchShortFranklinIn", 0, FALSE)
						
						SET_LABEL_AS_TRIGGERED("SwitchFX[Flash1]", TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchFX[Flash2]")
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneSwitch)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneSwitch) >= 0.550)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						IF sfxWoosh != -1
							STOP_SOUND(sfxWoosh)
							RELEASE_SOUND_ID(sfxWoosh)
							sfxWoosh = -1
						ENDIF
						ANIMPOSTFX_PLAY("SwitchShortMichaelMid", 0, FALSE)
						
						SET_LABEL_AS_TRIGGERED("SwitchFX[Flash2]", TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("InterpCam")
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneSwitch)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneSwitch) >= 0.8)
						//RENDER_SCRIPT_CAMS(FALSE, TRUE, 500, FALSE)
						
						IF NOT DOES_CAM_EXIST(camMain)
							camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						ENDIF
						
						SET_CAM_PARAMS(camMain, 
										<<993.677734, -2151.261230, 30.430367>>,
										<<0.0, 0.0, -6.601181>>,
										45.0)
						
						SET_CAM_ACTIVE_WITH_INTERP(camMain, camAnim, 500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
						PRINTLN("SET_CAM_ACTIVE_WITH_INTERP, GET_SYNCHRONIZED_SCENE_PHASE(sceneSwitch) = ", GET_SYNCHRONIZED_SCENE_PHASE(sceneSwitch))
						
						bRadar = TRUE
						
						SET_LABEL_AS_TRIGGERED("InterpCam", TRUE)
					ENDIF
				ENDIF
				
				IF ((IS_SYNCHRONIZED_SCENE_RUNNING(sceneSwitch)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneSwitch) >= 1.0)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneSwitch))
					//Michael
					//ADD_ARMOUR_TO_PED(PLAYER_PED(CHAR_MICHAEL), 200)
					
					//Start the upside down shoot task
					ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, <<-0.0148, -0.063, -1.683>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)	//<<0.0, -0.05, -1.68>>
//					TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
//					WHILE NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle")
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
					
					//SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
					//SET_PLAYER_FORCED_ZOOM(PLAYER_ID(), TRUE)
					
					GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350, TRUE)
					SET_PED_AMMO(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350)
					TASK_AIM_GUN_SCRIPTED(PLAYER_PED(CHAR_MICHAEL), SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN, TRUE, TRUE)	PRINTLN("SHOOTING/HANGING UPSIDE DOWN!")
					
					//Rail
					bOkToMoveRail = FALSE
					bOkToHangFromHook = FALSE
					
					//SET_ENTITY_HEADING(PLAYER_PED(CHAR_MICHAEL), -1.90)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL), TRUE)
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF sfxWoosh != -1
			STOP_SOUND(sfxWoosh)
			RELEASE_SOUND_ID(sfxWoosh)
			sfxWoosh = -1
		ENDIF
		
		//Force the switch to Michael
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
			ENDIF
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
			ENDIF
			
			//Franklin
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED(CHAR_FRANKLIN))
			
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
				GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
			ENDIF
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
			SET_ENTITY_PROOFS(PLAYER_PED(CHAR_FRANKLIN), FALSE, FALSE, FALSE, FALSE, FALSE)
			
			SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE) < (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_ASSAULTRIFLE) * 4)
			SET_PED_AMMO(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_ASSAULTRIFLE) * 4))
		ENDIF
		
//		REMOVE_CUTSCENE()
//		
//		WHILE HAS_CUTSCENE_LOADED()
//			WAIT_WITH_DEATH_CHECKS(0)
//		ENDWHILE
		
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		SAFE_DELETE_OBJECT(objGun)
		
		SAFE_REMOVE_BLIP(blipMichael)
		
		//Rail
		SET_RAIL_FINAL_NODE(31)
		fMoveSpeedMult = 0.0
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC michaelEscape()
	IF INIT_STAGE()
		//Replay
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayMichaelEscape), "stageMichaelEscape")
		
		//Uncapped
		bSetUncapped = TRUE
		
		//Cheat
		DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, TRUE)
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, FALSE)
		
		IF CAN_CREATE_RANDOM_COPS()
			SET_CREATE_RANDOM_COPS(FALSE)
		ENDIF
		
		//Player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
		
		//Cam
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//		
//		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
//		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
//		
//		DESTROY_ALL_CAMS()
		
		//Radar
		bRadar = TRUE
		
		//Vehicle
		vehHachiRoku = CREATE_VEHICLE(COQUETTE, <<950.4739, -2104.9517, 29.6107>>, 106.1659)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehHachiRoku, TRUE)
		SET_VEHICLE_COLOURS(vehHachiRoku, 4, 0)
		SET_VEHICLE_EXTRA_COLOURS(vehHachiRoku, 0, 0)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehHachiRoku)
		SET_VEHICLE_TYRES_CAN_BURST(vehHachiRoku, FALSE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(COQUETTE)
		
		vehEscape = CREATE_VEHICLE(FELTZER2, <<953.9548, -2113.3518, 29.5516>>, 88.1350)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEscape, TRUE)
		SET_VEHICLE_COLOURS(vehEscape, 38, 0)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehEscape)
		SET_VEHICLE_TYRES_CAN_BURST(vehEscape, FALSE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(FELTZER2)
		
		//Rail
		bOkToMoveRail = FALSE
		bOkToHangFromHook = FALSE
		
		//Audio
		PLAY_AUDIO(MIC2_HANGING_MICHAEL)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_FROM_MEATHOOK")
			START_AUDIO_SCENE("MI_2_SHOOT_FROM_MEATHOOK")
		ENDIF
		
		//bShootingUpsideDown = TRUE
		
		CREATE_CONVERSATION_ADV("MCH2_MORE", CONV_PRIORITY_MEDIUM, TRUE, DO_NOT_DISPLAY_SUBTITLES)
		
		SAFE_ADD_BLIP_PED(blipFranklin, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
		
		//Crates
		IF NOT DOES_ENTITY_EXIST(objCrate[0])
			objCrate[0] = CREATE_OBJECT_NO_OFFSET(PROP_WATERCRATE_01, <<985.1165, -2111.0317, 29.4751>>)
			RETAIN_ENTITY_IN_INTERIOR(objCrate[0], intAbattoir)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objCrate[1])
			objCrate[1] = CREATE_OBJECT_NO_OFFSET(PROP_WATERCRATE_01, <<977.4531, -2128.1602, 29.4753>>)
			RETAIN_ENTITY_IN_INTERIOR(objCrate[1], intAbattoir)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objCrate[2])
			objCrate[2] = CREATE_OBJECT_NO_OFFSET(PROP_WATERCRATE_01, <<987.7, -2125.6, 29.4753>>)
			RETAIN_ENTITY_IN_INTERIOR(objCrate[2], intAbattoir)
		ENDIF
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<996.3627, -2142.4551, 28.4762>>)
			covPoint[13] = ADD_COVER_POINT(<<996.3627, -2142.4551, 28.4762>>, 4.7376, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<993.2383, -2131.0708, 29.4758>>)
			covPoint[16] = ADD_COVER_POINT(<<993.2383, -2131.0708, 29.4758>>, 11.4085, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<989.3762, -2108.0239, 29.4751>>)
			covPoint[17] = ADD_COVER_POINT(<<989.3762, -2108.0239, 29.4751>>, 178.0079, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		ENDIF
		
		ADD_COVER_BLOCKING_AREA(<<985.11951, -2110.06934, 29.47508>>, <<984.31635, -2110.86963, 29.47506>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<990.234863, -2109.403076, 29.475147>>, <<991.734863, -2107.903076, 35.475147>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<984.292725, -2125.887451, 29.475412>>, <<985.292725, -2124.887451, 33.475412>>, TRUE, TRUE, TRUE)
		
		//Navmesh Blocking Areas
		IF iNavBlock[1] = 0
			iNavBlock[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<992.0, -2121.8, 29.5>>, <<4.0, 20.0, 10.0>>, (-5.0 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[1] = ", iNavBlock[1])
		ENDIF
		
		IF iNavBlock[2] = 0
			iNavBlock[2] = ADD_NAVMESH_BLOCKING_OBJECT(<<1001.1, -2121.8, 29.5>>, <<4.6, 20.0, 10.0>>, (-5.0 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[2] = ", iNavBlock[2])
		ENDIF
		
		IF iNavBlock[3] = 0
			iNavBlock[3] = ADD_NAVMESH_BLOCKING_OBJECT(<<984.1, -2135.5, 29.0>>, <<8.3, 8.0, 10.0>>, (-5.250 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[3] = ", iNavBlock[3])
		ENDIF
		
		//Enemies
		//Set Piece
		createEnemy(sEnemySetPiece[SET_PIECE_GRAPPLE], <<983.5596, -2110.1997, 29.4750>>, 299.8301, WEAPONTYPE_KNIFE)
		
		IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
			SET_ENTITY_VISIBLE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, FALSE)
			
			//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, intAbattoir)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromBulletImpact, TRUE)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromExplosions, TRUE)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromElectrocution, TRUE)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromFire, TRUE)
			SET_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, RBF_BULLET_IMPACT | RBF_EXPLOSION | RBF_ELECTROCUTION | RBF_FIRE | RBF_RUBBER_BULLET)
			SET_ENTITY_PROOFS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_ENTITY_INVINCIBLE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, TRUE)
			SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, CM_STATIONARY)
			SET_PED_MAX_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, 600)
			SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, 600)
		ENDIF
		
		//Backup
		IF NOT DOES_ENTITY_EXIST(sEnemyBackup[0].pedIndex)
			createEnemy(sEnemyBackup[0], <<997.7915, -2118.1855, 29.4757>>, 174.1204, WEAPONTYPE_PUMPSHOTGUN)
			//sEnemyBackup[0].iTime[0] = 0	sEnemyBackup[0].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[0].vPoint[0] = <<993.5497, -2142.2900, 28.4761>>	sEnemyBackup[0].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
			SET_PED_RESET_FLAG(sEnemyBackup[0].pedIndex, PRF_InstantBlendToAim, TRUE)
			TASK_COMBAT_PED(sEnemyBackup[0].pedIndex, PLAYER_PED(CHAR_MICHAEL))
			SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[0].pedIndex, <<993.5497, -2142.2900, 28.4761>>, 2.0)
			SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[0].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
			//RETAIN_ENTITY_IN_INTERIOR(sEnemyBackup[0].pedIndex, intAbattoir)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(sEnemyBackup[1].pedIndex)
			createEnemy(sEnemyBackup[1], <<996.0300, -2123.4690, 29.4757>>, 174.1522, WEAPONTYPE_ASSAULTRIFLE)
			//sEnemyBackup[1].iTime[0] = 0	sEnemyBackup[1].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[1].vPoint[0] = <<995.7127, -2135.2080, 29.4761>>	sEnemyBackup[1].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
			SET_PED_RESET_FLAG(sEnemyBackup[1].pedIndex, PRF_InstantBlendToAim, TRUE)
			TASK_COMBAT_PED(sEnemyBackup[1].pedIndex, PLAYER_PED(CHAR_MICHAEL))
			SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[1].pedIndex, <<993.2, -2143.0, 29.4761>>, 1.5)
			SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[1].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
			//RETAIN_ENTITY_IN_INTERIOR(sEnemyBackup[1].pedIndex, intAbattoir)
		ENDIF
		createEnemy(sEnemyBackup[2], <<995.9071, -2108.7920, 29.4754>>, 185.6451, WEAPONTYPE_SMG, G_M_M_CHIGOON_01)												
		TASK_COMBAT_PED(sEnemyBackup[2].pedIndex, PLAYER_PED(CHAR_MICHAEL))
		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[2].pedIndex, <<994.2408, -2136.2017, 29.2397>>, 1.5)
		SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[2].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
		//sEnemyBackup[2].pedCheck[0] = sEnemyBackup[0].pedIndex	sEnemyBackup[2].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[2].vPoint[0] = <<994.2408, -2136.2017, 29.2397>>	sEnemyBackup[2].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
		createEnemy(sEnemyBackup[3], <<993.0202, -2106.5718, 29.6306>>, 223.5710, WEAPONTYPE_SMG)
		sEnemyBackup[3].pedCheck[0] = sEnemyBackup[1].pedIndex	sEnemyBackup[3].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[3].vPoint[0] = <<994.7, -2134.4, 29.4761>>	sEnemyBackup[3].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
		createEnemy(sEnemyBackup[4], <<995.6806, -2106.8789, 29.6252>>, 183.2036, WEAPONTYPE_ASSAULTRIFLE)										
		sEnemyBackup[4].pedCheck[0] = sEnemyBackup[2].pedIndex	sEnemyBackup[4].eAdvanceStyle[0] = GUN_TO_POINT 	sEnemyBackup[4].vPoint[0] = <<995.0988, -2129.1052, 29.4759>>	sEnemyBackup[4].fSpeed[0] = PEDMOVEBLENDRATIO_RUN
		createEnemy(sEnemyBackup[5], <<990.7935, -2105.4414, 29.6615>>, 185.5090, WEAPONTYPE_PISTOL, G_M_M_CHIGOON_01)
		sEnemyBackup[5].vLocate[0] = <<995.4, -2133.0, 31.225800>>	sEnemyBackup[5].vLocSize[0] = <<5.0, 4.0, 3.0>>	sEnemyBackup[5].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyBackup[5].vPoint[0] = <<999.898499, -2137.357666, 29.0755>>	sEnemyBackup[5].fSpeed[0] = PEDMOVE_SPRINT
		createEnemy(sEnemyBackup[6], <<989.1515, -2105.3550, 29.4987>>, 268.4109, WEAPONTYPE_PISTOL)
		sEnemyBackup[6].vLocate[0] = <<997.6, -2119.8, 31.225800>>	sEnemyBackup[6].vLocSize[0] = <<5.0, 4.0, 3.0>>	sEnemyBackup[6].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyBackup[6].vPoint[0] = <<997.829773, -2116.072266, 29.4993>>	sEnemyBackup[6].fSpeed[0] = PEDMOVE_SPRINT
		//Two more enemies created in "OpenCombat9"
		//Three more enemies created in "OpenCombat10"
		
//		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[0].pedIndex, <<993.6147, -2129.6279, 29.4995>>, 2.0)
//		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[1].pedIndex, <<995.8074, -2130.9795, 29.4996>>, 2.0)
		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[2].pedIndex, <<992.9218, -2134.4761, 29.4997>>, 2.0)
//		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[3].pedIndex, <<991.7494, -2137.2988, 29.2466>>, 2.0)
//		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[4].pedIndex, <<999.7883, -2137.4788, 29.2466>>, 2.0)
		
		INT i
		
		REPEAT COUNT_OF(sEnemyBackup) i
			IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
				IF i <> 0 AND i <> 1
					//RETAIN_ENTITY_IN_INTERIOR(sEnemyBackup[i].pedIndex, intAbattoir)
					
					SET_ENTITY_HEALTH(sEnemyBackup[i].pedIndex, 170)
					SET_PED_MAX_HEALTH(sEnemyBackup[i].pedIndex, 170)
				ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(sEnemyBackup) i
			IF NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
				ASSIGN_DEBUG_NAMES(sEnemyBackup[i].pedIndex, "sEnemyBackup ", i)
				sEnemyBackup[i].sDebugName = CONCATENATE_STRINGS("sEnemyBackup ", GET_STRING_FROM_INT(i))
			ENDIF
		ENDREPEAT
		#ENDIF
		
		//Michael
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_MICHAEL), CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_MICHAEL), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, FALSE)
		SET_PED_COMBAT_MOVEMENT(PLAYER_PED(CHAR_MICHAEL), CM_DEFENSIVE)
		
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_MICHAEL), RBF_BULLET_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
		
		SET_PED_CAN_BE_TARGETTED(PLAYER_PED(CHAR_MICHAEL), FALSE)
		
		//Franklin Combat
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_FRANKLIN), CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_FRANKLIN), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_FRANKLIN), CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_COMBAT_MOVEMENT(PLAYER_PED(CHAR_FRANKLIN), CM_DEFENSIVE)
		
		SET_PED_ACCURACY(PLAYER_PED(CHAR_FRANKLIN), 20)
		
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_FRANKLIN), RBF_BULLET_IMPACT)
		
		SET_PED_SPHERE_DEFENSIVE_AREA(PLAYER_PED(CHAR_FRANKLIN), <<996.1766, -2142.4341, 28.50>>, 1.0)
		
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED(CHAR_FRANKLIN))
		
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
		
		SET_ENTITY_PROOFS(PLAYER_PED(CHAR_FRANKLIN), FALSE, FALSE, FALSE, FALSE, FALSE)
		
		OPEN_SEQUENCE_TASK(seqMain)  
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<996.2867, -2143.0442, 28.4763>>, <<994.3478, -2136.1184, 29.2463>>, PEDMOVE_RUN, TRUE, 4.0, 4.0, TRUE, ENAV_NO_STOPPING | ENAV_DONT_ADJUST_TARGET_POSITION)
			TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, <<996.3146, -2142.4307, 28.4762>>, 1000, FALSE, 1.0, TRUE, TRUE, covPoint[13], TRUE)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 1000.0)
		CLOSE_SEQUENCE_TASK(seqMain)
		TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_FRANKLIN), seqMain)
		CLEAR_SEQUENCE_TASK(seqMain)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_FRANKLIN), TRUE, -1, "DEFAULT_ACTION")
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<996.5976, -2148.9912, 29.0>>, 143.7361, FALSE)
			
			//Freeze
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), TRUE)
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			
			//Pin Interior
			IF intAbattoir = NULL
				intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<990.9518, -2178.6208, 29.0257>>, "v_abattoir")
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(intAbattoir)
			
			WHILE NOT IS_INTERIOR_READY(intAbattoir)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedAbattoir = TRUE
			
			//Unfreeze
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), FALSE)
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<996.5976, -2148.9912, 29.0>>)
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			MAKE_PICKUPS()
			
			//Michael
			//ADD_ARMOUR_TO_PED(PLAYER_PED(CHAR_MICHAEL), 200)
			
			//Skip Michael Forwards!
			iCurrentNode = 31
			vRailCurrent = sRailNodes[31].vPos
			SET_ENTITY_COORDS_NO_OFFSET(objHook, sRailNodes[31].vPos)
			SET_ENTITY_ROTATION(objHook, <<0.0, 0.0, -6.6>>)	//sRailNodes[31].vRot
			FREEZE_ENTITY_POSITION(objHook, TRUE)
			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_COMBATPISTOL)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
			
			WHILE NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_COMBATPISTOL)
			OR NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				PRINTLN("LOADING WEAPON ASSETS (WEAPONTYPE_COMBATPISTOL)...")
				PRINTLN("LOADING WEAPON ASSETS (WEAPONTYPE_ASSAULTRIFLE)...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			//Start the upside down shoot task
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, <<0.0, -0.05, -1.68>> , <<0.0, 0.0, 0.0>>, TRUE, TRUE)
//			TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
//			WHILE NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle")
//				WAIT_WITH_DEATH_CHECKS(0)
//			ENDWHILE
			//SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
			
			//Reset gameplay camera to third person, see B*1993022
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_NEAR)
				PRINTLN("RESETTING GAMEPLAY CAM VIEW MODE TO CAM_VIEW_MODE_THIRD_PERSON_NEAR 2")
			ENDIF
			
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED(CHAR_MICHAEL))
			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350, TRUE)
			SET_PED_AMMO(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350)
			TASK_AIM_GUN_SCRIPTED(PLAYER_PED(CHAR_MICHAEL), SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN)	PRINTLN("SHOOTING/HANGING UPSIDE DOWN!")
			
//			SET_ENTITY_HEADING(PLAYER_PED(CHAR_MICHAEL), -1.90)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
			
			SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			//Franklin
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
				GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
			ENDIF
			
			enumCharacterList eCharFail = GET_CURRENT_PLAYER_PED_ENUM()
			
			IF GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)) != WEAPONTYPE_UNARMED AND GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)) != WEAPONTYPE_INVALID AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)))
				SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)), TRUE)
			ELSE
				SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Audio
			PLAY_AUDIO(MIC2_HANGING_RT)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), objHook)
				INT iTolerance
				
				iTolerance = 0
				
				INT iLTSx, iLTSy, iRTSx, iRTSy
				
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
				
				VECTOR vGameplayCamRot = GET_GAMEPLAY_CAM_ROT()
				
				//SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE()
				
				
				IF vGameplayCamRot.Z < -80.0
					IF iRTSx > iTolerance
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)	//PRINTLN("DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)")
					ENDIF
					IF vGameplayCamRot.Z < -85.0
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
					ENDIF
				ELIF vGameplayCamRot.Z > 80.0
					IF iRTSx < -iTolerance
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)	//PRINTLN("DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)")
					ENDIF
					IF vGameplayCamRot.Z > 85.0
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
					ENDIF
				ENDIF
				
				IF TIMERB() > 1000
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MichaelIntro")
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) != WAITING_TO_START_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_AIM_GUN_SCRIPTED) != PERFORMING_TASK
							SETTIMERB(0)
						ELSE
							IF IS_SCREEN_FADED_OUT()
								DO_SCREEN_FADE_IN(500)
							ENDIF
							
							SET_LABEL_AS_TRIGGERED("MichaelIntro", TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL) <= 0
					eMissionFail = failOutOfAmmo
					
					missionFailed()
				ENDIF
				
				IF NOT IS_PED_INJURED(sEnemyBackup[0].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[1].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[2].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[3].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[4].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[7].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[8].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[9].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[10].pedIndex)
				OR NOT IS_PED_INJURED(sEnemyBackup[11].pedIndex)
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_GAME_TIMER() > iDialogueTimer[0]
							IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_BKUP_1")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_BKUP", "MCH2_BKUP_1")
								
								iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_BKUP_2")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_BKUP", "MCH2_BKUP_2")
								
								iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_BKUP_3")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_BKUP", "MCH2_BKUP_3")
								
								iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 3000)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_BKUP_4")
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_BKUP", "MCH2_BKUP_4")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			ENDIF
			bOkToHangFromHook = FALSE
		ELSE
			//SET_PLAYER_FORCED_ZOOM(PLAYER_ID(), FALSE)
		ENDIF
		
//		IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchToFranklin")
//			IF IS_PED_AIMING_FROM_COVER(PLAYER_PED(CHAR_FRANKLIN))
//				SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_ForcedToStayInCover, TRUE)
//			ENDIF
//		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("FranklinGrapple")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("PrepMichaelAnim")
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					DETACH_ENTITY(PLAYER_PED(CHAR_MICHAEL))
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED(CHAR_MICHAEL))
					
					SET_ENABLE_BOUND_ANKLES(PLAYER_PED(CHAR_MICHAEL), FALSE)
					
					bOkToMoveRail = FALSE
					bOkToHangFromHook = FALSE
					
					SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_MICHAEL), TRUE)
					
					TASK_PLAY_ANIM_ADVANCED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall", <<994.205, -2150.821, 29.474>> + <<-0.09, 0.40, -0.00>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK) //0.552)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
					
					SET_ENTITY_HEADING(PLAYER_PED(CHAR_MICHAEL), 350.8180)
				ELSE
					SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall", 0.0)
					SET_ENTITY_ANIM_SPEED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall", 0.0)
					
					FILL_PUSH_IN_DATA(pushInData, PLAYER_PED(CHAR_MICHAEL), CHAR_MICHAEL, 0.75, 800, 800, 500, 400, PUSH_IN_SPEED_UP_PROPORTION)
					
					SET_LABEL_AS_TRIGGERED("PrepMichaelAnim", TRUE)
				ENDIF
			ELIF HAS_LABEL_BEEN_TRIGGERED("StartMichaelAnim")
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
					SET_ENTITY_ANIM_SPEED(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall", 1.0)
				ENDIF
				
				bRadar = TRUE
				
//				INT iTolerance
//				
//				iTolerance = 16
//				
//				INT iLTSx, iLTSy, iRTSx, iRTSy
//				
//				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
//				
//				IF (iLTSx < -iTolerance OR iLTSx > iTolerance)
//				OR (iLTSy < -iTolerance OR iLTSy > iTolerance)
//					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
//					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall") > 0.775
//						CLEAR_PED_TASKS(PLAYER_PED(CHAR_MICHAEL))
//						
//						SET_ENTITY_HEADING(PLAYER_PED(CHAR_MICHAEL), 350.8180)
//						
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
//					ENDIF
//				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MichaelFallShot")
					IF (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall") > 0.258)
						SET_PED_SHOOTS_AT_COORD(PLAYER_PED(CHAR_MICHAEL), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)) + <<0.0, 0.0, 2.0>>)
						
						INT iFramePass
						
						FOR iFramePass = 0 TO 10
							DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()	PRINTLN("DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()")
							
							WAIT_WITH_DEATH_CHECKS(0)
						ENDFOR
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mich2_spark_impact", <<994.175659, -2150.427490, 32.231403>>, VECTOR_ZERO)
						
						SET_LABEL_AS_TRIGGERED("MichaelFallShot", TRUE)
					ENDIF
				ENDIF
				
				//Push In
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MichaelFallPushIn")
						IF (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
						AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall") > 0.85)
							PRINTLN("PUSH IN CAM ACTIVE!")
							IF HANDLE_PUSH_IN(pushInData, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
								SET_LABEL_AS_TRIGGERED("MichaelFallPushIn", TRUE)	PRINTLN("SET_LABEL_AS_TRIGGERED(\"MichaelFallPushIn\", TRUE)")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
				AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall") > 0.99)
				OR NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
				OR HAS_LABEL_BEEN_TRIGGERED("MichaelFallPushIn")
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), FALSE)
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED(CHAR_MICHAEL))
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					ENDIF
					
//					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
//						ANIMPOSTFX_PLAY("CamPushInMichael", 0, FALSE)
//					ENDIF
					
//					CLEAR_PED_TASKS(PLAYER_PED(CHAR_MICHAEL))
//					
//					SET_ENTITY_HEADING(PLAYER_PED(CHAR_MICHAEL), 350.8180)
//					
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			ENDIF
		ELSE
			bOkToHangFromHook = TRUE
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
			IF IS_GAMEPLAY_CAM_RENDERING()
			AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) = PERFORMING_TASK
					INT iTolerance
					
					iTolerance = 16
					
					INT iLTSx, iLTSy, iRTSx, iRTSy
					
					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
					
					IF (iLTSx < -iTolerance OR iLTSx > iTolerance)
					OR (iLTSy < -iTolerance OR iLTSy > iTolerance)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ELIF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) = PERFORMING_TASK
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-6.0 - (GET_ENTITY_HEADING(PLAYER_PED_ID()) - 90.0))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ENDIF
					
		//Open Combat
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat9")
			INT iEnemiesRemain = 5
			
			IF IS_PED_INJURED(sEnemyBackup[0].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[1].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[2].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[3].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[4].pedIndex)
				iEnemiesRemain--
			ENDIF
			
			IF iEnemiesRemain <= 1
				VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				createEnemy(sEnemyBackup[7], <<982.8243, -2144.1726, 28.4757>>, 314.9142, WEAPONTYPE_PISTOL)
				createEnemy(sEnemyBackup[8], <<986.0364, -2144.4543, 28.4759>>, 318.1739, WEAPONTYPE_PISTOL)
				
				OPEN_SEQUENCE_TASK(seqMain)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<988.5842, -2143.4475, 28.4759>>, <<vPlayerCoord.X, vPlayerCoord.Y, 28.4759>>, PEDMOVE_RUN, TRUE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
				CLOSE_SEQUENCE_TASK(seqMain)
				TASK_PERFORM_SEQUENCE(sEnemyBackup[7].pedIndex, seqMain)
				CLEAR_SEQUENCE_TASK(seqMain)
				
				OPEN_SEQUENCE_TASK(seqMain)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<988.7651, -2139.6868, 28.4759>>, <<vPlayerCoord.X, vPlayerCoord.Y, 28.4759>>, PEDMOVE_RUN, TRUE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
				CLOSE_SEQUENCE_TASK(seqMain)
				TASK_PERFORM_SEQUENCE(sEnemyBackup[8].pedIndex, seqMain)
				CLEAR_SEQUENCE_TASK(seqMain)
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sEnemyBackup[7].pedIndex, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sEnemyBackup[8].pedIndex, TRUE)
				
				SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyBackup[7].pedIndex, <<994.424316, -2143.735352, 27.976158>>, <<984.857361, -2131.580078, 32.475563>>, 10.0)
				SET_PED_COMBAT_MOVEMENT(sEnemyBackup[7].pedIndex, CM_WILLADVANCE)
				
				SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyBackup[8].pedIndex, <<994.424316, -2143.735352, 27.976158>>, <<984.857361, -2131.580078, 32.475563>>, 10.0)
				SET_PED_COMBAT_MOVEMENT(sEnemyBackup[8].pedIndex, CM_WILLADVANCE)
				
				iDamageTimer = GET_GAME_TIMER() + 15000
				iAdvanceTimer = GET_GAME_TIMER() + 15000
				
				SET_LABEL_AS_TRIGGERED("OpenCombat9", TRUE)
			ENDIF
		ELSE
			IF GET_GAME_TIMER() > iDamageTimer
				IF NOT IS_PED_INJURED(sEnemyBackup[7].pedIndex)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sEnemyBackup[7].pedIndex, FALSE)
				ENDIF
				
				IF NOT IS_PED_INJURED(sEnemyBackup[8].pedIndex)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sEnemyBackup[8].pedIndex, FALSE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_FLANK_RESPONSE")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF NOT IS_PED_INJURED(sEnemyBackup[7].pedIndex)
					OR NOT IS_PED_INJURED(sEnemyBackup[8].pedIndex)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_FLANK")
							CREATE_CONVERSATION_ADV("MCH2_FLANK")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_MOREF")
							CREATE_CONVERSATION_ADV("MCH2_MOREF", CONV_PRIORITY_MEDIUM, FALSE)
							
							SET_LABEL_AS_TRIGGERED("MCH2_FLANK_RESPONSE", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Open Combat
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat10")
			INT iEnemiesRemain = 5
			
			IF IS_PED_INJURED(sEnemyBackup[0].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[1].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[2].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[3].pedIndex)
				iEnemiesRemain--
			ENDIF
			IF IS_PED_INJURED(sEnemyBackup[4].pedIndex)
				iEnemiesRemain--
			ENDIF
			
			IF (iEnemiesRemain <= 1
			AND (IS_PED_INJURED(sEnemyBackup[7].pedIndex)
			OR IS_PED_INJURED(sEnemyBackup[8].pedIndex)))
			OR (HAS_LABEL_BEEN_TRIGGERED("OpenCombat9")
			AND GET_GAME_TIMER() > iAdvanceTimer)
				createEnemy(sEnemyBackup[9], <<993.0953, -2105.2952, 29.4752>>, 204.5829, WEAPONTYPE_PISTOL)
				createEnemy(sEnemyBackup[10], <<993.6476, -2106.8474, 29.4752>>, 261.5278, WEAPONTYPE_SMG)
				createEnemy(sEnemyBackup[11], <<994.7681, -2106.3376, 29.4753>>, 186.8079, WEAPONTYPE_SMG)
				
				SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyBackup[9].pedIndex, <<994.966064, -2130.581299, 28.225883>>, <<993.716187, -2143.722168, 32.476128>>, 10.0)
				SET_PED_COMBAT_MOVEMENT(sEnemyBackup[9].pedIndex, CM_WILLADVANCE)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyBackup[9].pedIndex, 500.0)
				
				SET_PED_ANGLED_DEFENSIVE_AREA(sEnemyBackup[10].pedIndex, <<994.966064, -2130.581299, 28.225883>>, <<993.716187, -2143.722168, 32.476128>>, 10.0)
				SET_PED_COMBAT_MOVEMENT(sEnemyBackup[10].pedIndex, CM_WILLADVANCE)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyBackup[10].pedIndex, 500.0)
				
				SET_ENTITY_HEALTH(sEnemyBackup[11].pedIndex, 105)
				SET_PED_COMBAT_MOVEMENT(sEnemyBackup[11].pedIndex, CM_STATIONARY)
				SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyBackup[11].pedIndex, <<999.9770, -2137.3657, 29.2547>>, 1.5, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyBackup[11].pedIndex, TRUE)
				
				TASK_GO_STRAIGHT_TO_COORD(sEnemyBackup[11].pedIndex, <<1002.3436, -2114.6580, 29.4758>>, PEDMOVE_RUN)
				
				SET_PED_CAN_BE_TARGETTED(sEnemyBackup[11].pedIndex, FALSE)
				
				SET_LABEL_AS_TRIGGERED("OpenCombat10", TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEnemyBackup[11].pedIndex)
		AND NOT IS_PED_INJURED(sEnemyBackup[11].pedIndex)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat10a")
				IF IS_ENTITY_AT_COORD(sEnemyBackup[11].pedIndex, <<1002.3436, -2114.6580, 29.4758>>, <<2.0, 2.0, 2.0>>)
					SET_PED_POSITION(sEnemyBackup[11].pedIndex, <<1000.0, -2132.2, 29.4763>>, 173.1776)
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<999.9770, -2137.3657, 29.2547>>, PLAYER_PED_ID(), PEDMOVE_WALK, TRUE, 0.1, 1, FALSE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<994.3035, -2136.5476, 29.2231>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.1, 1, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(sEnemyBackup[11].pedIndex, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					REMOVE_PED_DEFENSIVE_AREA(sEnemyBackup[11].pedIndex)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[11].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
					SET_PED_COMBAT_MOVEMENT(sEnemyBackup[11].pedIndex, CM_WILLADVANCE)
					
					SET_PED_CAN_BE_TARGETTED(sEnemyBackup[11].pedIndex, TRUE)
					
					SET_LABEL_AS_TRIGGERED("OpenCombat10a", TRUE)
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_MACHINE_RESPONSE")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						IF NOT IS_PED_INJURED(sEnemyBackup[7].pedIndex)
						OR NOT IS_PED_INJURED(sEnemyBackup[8].pedIndex)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_MACHINE")
								CREATE_CONVERSATION_ADV("MCH2_MACHINE")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_MOREG")
								CREATE_CONVERSATION_ADV("MCH2_MOREG", CONV_PRIORITY_MEDIUM, FALSE)
								
								SET_LABEL_AS_TRIGGERED("MCH2_MACHINE_RESPONSE", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_INJURED(sEnemyBackup[0].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[1].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[2].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[3].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[4].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[7].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[8].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[9].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[10].pedIndex)
		AND IS_PED_INJURED(sEnemyBackup[11].pedIndex)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchToFranklin")
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
				IF IS_PED_IN_COMBAT(PLAYER_PED(CHAR_FRANKLIN))
					CLEAR_PED_TASKS(PLAYER_PED(CHAR_FRANKLIN))
					TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED(CHAR_FRANKLIN), <<996.3146, -2142.4307, 28.4762>>, -1, FALSE, 1.0, TRUE, TRUE, covPoint[13], TRUE)
					PRINTLN("Franklin was in combat, so we clear his tasks to put him into cover!")
				ENDIF
				
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<996.3133, -2142.4089, 28.4762>>, <<2.0, 2.0, 2.0>>)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED(CHAR_FRANKLIN), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<996.2867, -2143.0442, 28.4763>>, <<994.3478, -2136.1184, 29.2463>>, PEDMOVE_RUN, TRUE)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, <<996.3146, -2142.4307, 28.4762>>, -1, FALSE, 1.0, TRUE, TRUE, covPoint[13], TRUE)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_FRANKLIN), seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						PRINTLN("Franklin wasn't in the right spot, so he gets a sequence to enter cover in the right place!")
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_AHEAD_1")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_AHEAD", "MCH2_AHEAD_1")	PRINTLN("All the enemies are dead, so we can switch to Franklin!")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_AHEAD_2")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_AHEAD", "MCH2_AHEAD_2")
//					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchToFranklin")
//					AND GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
//						PRINT_ADV("MCH2_FSWITCH")
					ENDIF
				ENDIF
//			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("OneFrameDisableFirstPersonCam")
//				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()	PRINTLN("DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()")
//				
//				SET_LABEL_AS_TRIGGERED("OneFrameDisableFirstPersonCam", TRUE)
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF2")
						IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<993.211975, -2149.697754, 31.476280>>, <<8.0, 8.0, 3.0>>)
							CREATE_CONVERSATION_ADV("MCH2_CLEAR")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("MCH2_AHEAD_1")
				REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Break_Free")
				
				IF NOT sCamDetails.bRun
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					OR (IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
					OR iSetPiece[SET_PIECE_GRAPPLE] > 4)
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_intro_p_one")
						AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one")
						AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one")
						AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_a_p_one")
						AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one")
						AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
							IF (NOT HAS_LABEL_BEEN_TRIGGERED("SwitchToFranklin")	//UPDATE_SELECTOR_HUD(sSelectorPeds)
							AND IS_PED_IN_COVER(PLAYER_PED(CHAR_FRANKLIN)))
								IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
									CLEAR_PRINTS()
									
									MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
									
									//Stats
									INFORM_MISSION_STATS_OF_INCREMENT(MIC2_TIMES_SWITCHED)
									
//									IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchToFranklin")
//										MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//									ELSE
//										//Stats
//										INFORM_MISSION_STATS_OF_INCREMENT(MIC2_TIMES_SWITCHED)
//									ENDIF
									
									IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
										GET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon)
									ENDIF
									
									sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
									sCamDetails.bRun  = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
					
					IF NOT (IS_PLAYER_SWITCH_IN_PROGRESS() AND GET_PLAYER_SHORT_SWITCH_STATE() >= SHORT_SWITCH_STATE_OUTRO)
						DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()	PRINTLN("DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()")
					ENDIF
					
					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)	//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED_RACE(sCamDetails) //Returns FALSE when the camera spline is complete
						IF sCamDetails.bOKToSwitchPed
							IF NOT sCamDetails.bPedSwitched
								TAKE_CONTROL_OF_PED_FLAGS eTakeControlFlags = TCF_NONE
								
								IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchToFranklin")
									eTakeControlFlags = TCF_CLEAR_TASK_INTERRUPT_CHECKS
									
									SET_LABEL_AS_TRIGGERED("SwitchToFranklin", TRUE)
								ENDIF
								
								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, eTakeControlFlags)
									//Dialogue
									REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
									REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
									
									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
									
									//Update the relationship groups
									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
									ENDIF
									
									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
									ENDIF
									
									SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(PLAYER_PED(CHAR_FRANKLIN), PLAYER_PED(CHAR_MICHAEL), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
									SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(PLAYER_PED(CHAR_MICHAEL), PLAYER_PED(CHAR_FRANKLIN), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
									
									SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_MICHAEL), RBF_BULLET_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
									SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_FRANKLIN), RBF_BULLET_IMPACT)
									
									SAFE_REMOVE_BLIP(blipMichael)
									SAFE_REMOVE_BLIP(blipFranklin)
									
									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
										SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
										
										SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
										
										IF IS_ENTITY_ATTACHED(PLAYER_PED(CHAR_MICHAEL))
											TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_react_c", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)	//TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictHang, "idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
										ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
										SAFE_ADD_BLIP_PED(blipFranklin, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
										
										SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
									ENDIF
									
									SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_ForcedToStayInCover, FALSE)
									
									SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
									SET_PED_USING_ACTION_MODE(NOT_PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
									
									IF GET_ENTITY_HEALTH(PLAYER_PED(CHAR_MICHAEL)) < GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_MICHAEL)) / 2
										SET_ENTITY_HEALTH(PLAYER_PED(CHAR_MICHAEL), GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_MICHAEL)) / 2)
									ENDIF
									
									IF GET_ENTITY_HEALTH(PLAYER_PED(CHAR_FRANKLIN)) < GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_FRANKLIN)) / 2
										SET_ENTITY_HEALTH(PLAYER_PED(CHAR_FRANKLIN), GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_FRANKLIN)) / 2)
									ENDIF
									
									IF iCutsceneStage > 0
										iCutsceneStage--	PRINTLN("iCutsceneStage = ", iCutsceneStage)	//Should re-assign the tasks
									ENDIF
									
									IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
										//First switch task for Michael
										ATTACH_ENTITY_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook, 0, <<0.0, -0.1, -1.68>> , <<0.0, 0.0, 0.0>>, TRUE, TRUE)
										
										IF wtLastWeapon != WEAPONTYPE_UNARMED
											IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon)
												GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon, 350, TRUE)
											ENDIF
											
											SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon, TRUE)
										ELSE
											IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
												GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
											ENDIF
											
											SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
										ENDIF
										
										//Audio
										PLAY_AUDIO(MIC2_BACK_TO_FRANK)
										
										LOAD_AUDIO(MIC3_FRANK_DOWN)
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
											CREATE_CONVERSATION_ADV("MCH2_ESCF1")
										ELSE
											SET_LABEL_AS_TRIGGERED("MCH2_ESCF1", TRUE)
										ENDIF
										
										CREATE_PICKUP(PICKUP_WEAPON_SAWNOFFSHOTGUN, <<995.77649, -2126.69238, 29.47581>>)
										
										SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									ENDIF
									
									sCamDetails.bPedSwitched = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
		OR (DOES_CAM_EXIST(sCamDetails.camID)
		AND NOT IS_CAM_ACTIVE(sCamDetails.camID))
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NOT_PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 25.0
//			AND NOT IS_ENTITY_ON_SCREEN(NOT_PLAYER_PED_ID())
//				IF iHealthStore = -1
//					iHealthStore = GET_ENTITY_HEALTH(NOT_PLAYER_PED_ID())
//					SET_ENTITY_HEALTH(NOT_PLAYER_PED_ID(), 101)
//				ENDIF
//			ELIF iHealthStore > -1
//				SET_ENTITY_HEALTH(NOT_PLAYER_PED_ID(), iHealthStore)
//				iHealthStore = -1
//			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NOT_PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 60.0
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					eMissionFail = failFranklinAbandon
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					eMissionFail = failMichaelAbandon
				ENDIF
				
				missionFailed()
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
		AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCM")
		AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF2")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				CREATE_CONVERSATION_ADV("MCH2_ESCM")
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF1")
		AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_ESCF2")
			IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<996.047791, -2125.919189, 31.225800>>, <<5.0, 4.0, 3.0>>)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					CREATE_CONVERSATION_ADV("MCH2_ESCF2")
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
		AND iSetPiece[SET_PIECE_GRAPPLE] > 5)
			IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_intro_p_one")
			AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one")
			AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one")
			AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_a_p_one")
			AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one")
			AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictMic2Hook, "michael_meat_hook_fall")
			AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
				ADVANCE_STAGE()
			ENDIF
		ENDIF
		
		//Grapple
		SWITCH iSetPiece[SET_PIECE_GRAPPLE]
			CASE 0
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
					//IF GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex) < 400
					//OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex), 5.0)
					//OR IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<997.968079, -2106.348145, 31.975391>>, <<8.0, 5.0, 2.5>>)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("AbandonGrapple")
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<984.412720, -2102.345703, 29.301226>>, <<981.532898, -2130.348389, 35.475380>>, 15.0)
							PRINT_ADV("CMN_MLEAVE", DEFAULT_GOD_TEXT_TIME, FALSE)
							
							IF NOT IS_PED_INJURED(sEnemyBackup[5].pedIndex)
								CLEAR_PED_TASKS(sEnemyBackup[5].pedIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyBackup[5].pedIndex, TRUE)
								SET_PED_ACCURACY(sEnemyBackup[5].pedIndex, 80)
								SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[5].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
								SET_PED_COMBAT_MOVEMENT(sEnemyBackup[5].pedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(sEnemyBackup[5].pedIndex, PLAYER_PED(CHAR_MICHAEL), VECTOR_ZERO, 10.0)
								TASK_COMBAT_PED(sEnemyBackup[5].pedIndex, PLAYER_PED(CHAR_MICHAEL))
							ENDIF
							
							IF NOT IS_PED_INJURED(sEnemyBackup[6].pedIndex)
								CLEAR_PED_TASKS(sEnemyBackup[6].pedIndex)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyBackup[6].pedIndex, TRUE)
								SET_PED_ACCURACY(sEnemyBackup[6].pedIndex, 80)
								SET_PED_COMBAT_ATTRIBUTES(sEnemyBackup[6].pedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
								SET_PED_COMBAT_MOVEMENT(sEnemyBackup[6].pedIndex, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(sEnemyBackup[6].pedIndex, PLAYER_PED(CHAR_MICHAEL), VECTOR_ZERO, 10.0)
								TASK_COMBAT_PED(sEnemyBackup[6].pedIndex, PLAYER_PED(CHAR_MICHAEL))
							ENDIF
							
							SET_LABEL_AS_TRIGGERED("AbandonGrapple", TRUE)
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<984.412720, -2102.345703, 29.301226>>, <<981.532898, -2130.348389, 35.475380>>, 15.0)
							IF NOT IS_PED_INJURED(sEnemyBackup[5].pedIndex)
								IF IS_ENTITY_AT_COORD(sEnemyBackup[5].pedIndex, <<995.896423, -2148.686768, 31.476355>>, <<10.0, 10.0, 3.0>>)
									APPLY_DAMAGE_TO_PED(PLAYER_PED(CHAR_MICHAEL), 1000, TRUE)
									SET_PED_TO_RAGDOLL(PLAYER_PED(CHAR_MICHAEL), 10000, 10000, TASK_RELAX, FALSE, FALSE)
									
									//Pin the ankle to the bottom of the hook
									VECTOR vRightFootAttach
									vRightFootAttach = <<-0.075, -0.1, -0.85>>
									ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_R_FOOT), -1, vRightFootAttach, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
									ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_L_FOOT), -1, <<-vRightFootAttach.X, vRightFootAttach.Y, vRightFootAttach.Z>>, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(sEnemyBackup[6].pedIndex)
								IF IS_ENTITY_AT_COORD(sEnemyBackup[6].pedIndex, <<995.896423, -2148.686768, 31.476355>>, <<10.0, 10.0, 3.0>>)
									APPLY_DAMAGE_TO_PED(PLAYER_PED(CHAR_MICHAEL), 1000, TRUE)
									SET_PED_TO_RAGDOLL(PLAYER_PED(CHAR_MICHAEL), 10000, 10000, TASK_RELAX, FALSE, FALSE)
									
									//Pin the ankle to the bottom of the hook
									VECTOR vRightFootAttach
									vRightFootAttach = <<-0.075, -0.1, -0.85>>
									ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_R_FOOT), -1, vRightFootAttach, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
									ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_L_FOOT), -1, <<-vRightFootAttach.X, vRightFootAttach.Y, vRightFootAttach.Z>>, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<971.733765, -2122.363770, 29.477753>>, <<989.067078, -2124.301025, 34.475548>>, 5.0)
							eMissionFail = failMichaelAbandon
							
							missionFailed()
						ENDIF
					ENDIF
					
					IF IS_PED_INJURED(sEnemyBackup[5].pedIndex)
					AND IS_PED_INJURED(sEnemyBackup[6].pedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, TRUE)
						
						IF NOT HAS_PED_GOT_WEAPON(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, WEAPONTYPE_KNIFE)
							GIVE_WEAPON_TO_PED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, WEAPONTYPE_KNIFE, INFINITE_AMMO, TRUE)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, WEAPONTYPE_KNIFE, TRUE)
						
						SETTIMERB(0)
						iSetPiece[SET_PIECE_GRAPPLE]++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
//					BOOL bClearOfEnemies
//					
//					bClearOfEnemies = TRUE
					
//					REPEAT COUNT_OF(sEnemyBackup) i
//						IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
//						AND NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
//							IF IS_ENTITY_AT_COORD(sEnemyBackup[i].pedIndex, <<997.968079, -2106.348145, 31.975391>>, <<8.0, 5.0, 2.5>>)
//								bClearOfEnemies = FALSE
//							ENDIF
//						ENDIF
//					ENDREPEAT

//					IF IS_PED_INJURED(sEnemyBackup[5].pedIndex)
//					AND IS_PED_INJURED(sEnemyBackup[6].pedIndex)
//						
//					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("AbandonGrapple")
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<984.412720, -2102.345703, 29.301226>>, <<981.532898, -2130.348389, 35.475380>>, 15.0)
							PRINT_ADV("CMN_MLEAVE", DEFAULT_GOD_TEXT_TIME, FALSE)
							
							SET_LABEL_AS_TRIGGERED("AbandonGrapple", TRUE)
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<971.733765, -2122.363770, 29.477753>>, <<989.067078, -2124.301025, 34.475548>>, 5.0)
							eMissionFail = failMichaelAbandon
							
							missionFailed()
						ENDIF
					ENDIF
					
					IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<987.029297, -2105.918701, 29.391869>>, <<999.674927, -2104.281738, 34.211304>>, 7.0)
					AND TIMERB() > 2000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_FRANKLIN), <<989.328735, -2108.531250, 29.475145>>, <<989.938782, -2102.828369, 34.225105>>, 3.75)
						IF NOT IS_PED_RAGDOLL(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)	//, SPC_LEAVE_CAMERA_CONTROL_ON)
							
							INT i
							
							REPEAT COUNT_OF(sEnemyBackup) i
								IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
									IF NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
										SET_ENTITY_VISIBLE(sEnemyBackup[i].pedIndex, FALSE)
										FREEZE_ENTITY_POSITION(sEnemyBackup[i].pedIndex, TRUE)
									ELIF IS_ENTITY_IN_ANGLED_AREA(sEnemyBackup[i].pedIndex, <<991.811279, -2105.769775, 28.975157>>, <<999.906250, -2107.010254, 33.475464>>, 3.0)
										SAFE_DELETE_PED(sEnemyBackup[i].pedIndex)
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF NOT HAS_PED_GOT_WEAPON(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, WEAPONTYPE_KNIFE)
								GIVE_WEAPON_TO_PED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, WEAPONTYPE_KNIFE, INFINITE_AMMO, TRUE)
							ENDIF
							
							SET_CURRENT_PED_WEAPON(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, WEAPONTYPE_KNIFE, TRUE)
							
							GET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon)
							
							SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED, TRUE)
							
							SAFE_ADD_BLIP_PED(sEnemySetPiece[SET_PIECE_GRAPPLE].blipIndex, sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, TRUE)
							
							//Cam
							IF NOT DOES_CAM_EXIST(camAnim)
								camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
							ENDIF
							
							PLAY_CAM_ANIM(camAnim, "mic_2_ig_11_intro_cam", sAnimDictMic2IG11, <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>)
							
							SET_CAM_ACTIVE(camAnim, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							
							SET_ENTITY_VISIBLE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, TRUE)
							
							CLEAR_AREA(<<997.0422, -2107.2380, 29.4754>>, 10.0, TRUE)
							
							SET_ENTITY_NO_COLLISION_ENTITY(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PLAYER_PED(CHAR_FRANKLIN), FALSE)
							SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED(CHAR_FRANKLIN), sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, FALSE)
							
//							OPEN_SEQUENCE_TASK(seqMain)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_intro_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)	//, 0.373)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_a_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//							CLOSE_SEQUENCE_TASK(seqMain)
//							TASK_PERFORM_SEQUENCE(PLAYER_PED(CHAR_FRANKLIN), seqMain)
//							CLEAR_SEQUENCE_TASK(seqMain)
//							
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
//							
//							OPEN_SEQUENCE_TASK(seqMain)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)	//, 0.373)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictMic2IG11, "mic_2_ig_11_a_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//							CLOSE_SEQUENCE_TASK(seqMain)
//							TASK_PERFORM_SEQUENCE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, seqMain)
//							CLEAR_SEQUENCE_TASK(seqMain)
//							
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
							
							iGrappleStage = 1
							
							SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromBulletImpact, FALSE)
							SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromExplosions, FALSE)
							SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromElectrocution, FALSE)
							SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, PCF_DontActivateRagdollFromFire, FALSE)
							CLEAR_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, RBF_BULLET_IMPACT | RBF_EXPLOSION | RBF_ELECTROCUTION | RBF_FIRE | RBF_RUBBER_BULLET)
							SET_ENTITY_PROOFS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_ENTITY_INVINCIBLE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, FALSE)
							SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, 105)
							
							//Audio Scene
							IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_FROM_MEATHOOK")
								STOP_AUDIO_SCENE("MI_2_SHOOT_FROM_MEATHOOK")
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_FRANKLIN_KNIFE_ATTACK")
								START_AUDIO_SCENE("MI_2_FRANKLIN_KNIFE_ATTACK")
							ENDIF
							
							//Audio
							PLAY_AUDIO(MIC3_FRANK_DOWN)
							
							SETTIMERB(0)
							
							eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS
							HANDLE_SWITCH_CAM_FRANKLIN_GRAPPLE_TO_MICHAEL(scsSwitchCam_FranklinGrappleToMichael)
							
							IF NOT bVideoRecording
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
								
								bVideoRecording = TRUE
							ENDIF
							
							iSetPiece[SET_PIECE_GRAPPLE]++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 0.350
				//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon")
				//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon") >= 0.350
				//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon") > 0.650
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					
					//Blocking
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
					
					//Dialogue
					REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
					REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
					
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
					
					//Update the relationship groups
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
					ENDIF
					
					SAFE_REMOVE_BLIP(blipMichael)
					SAFE_REMOVE_BLIP(blipFranklin)
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SAFE_ADD_BLIP_PED(blipFranklin, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
					ENDIF
					
					SETTIMERB(0)
					eSwitchCamState = SWITCH_CAM_SETUP_SPLINE
					iSetPiece[SET_PIECE_GRAPPLE]++
				ENDIF
			BREAK
			CASE 3
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				INT i
				
//				IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon")
//				AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon") > 0.750	//0.650
					REPEAT COUNT_OF(sEnemyBackup) i
						IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
							SET_ENTITY_VISIBLE(sEnemyBackup[i].pedIndex, TRUE)
							FREEZE_ENTITY_POSITION(sEnemyBackup[i].pedIndex, FALSE)
						ENDIF
					ENDREPEAT
					
					IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, 600)
						SET_PED_SUFFERS_CRITICAL_HITS(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, FALSE)
					ENDIF
					
					TASK_AIM_GUN_SCRIPTED(PLAYER_PED(CHAR_MICHAEL), SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN)	PRINTLN("SHOOTING/HANGING UPSIDE DOWN!")
					
					IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL)
						GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350, TRUE)
					ENDIF
					
					IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL) < (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 2)
						SET_PED_AMMO(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 2))
					ENDIF
					
					SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, TRUE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
					
					//Cam
//					IF NOT DOES_CAM_EXIST(camMain)
//						camMain = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
//					ENDIF
//					
//					SET_CAM_PARAMS(camMain, GET_CAM_COORD(camAnim), GET_CAM_ROT(camAnim), GET_CAM_FOV(camAnim))
//					
//					SET_CAM_PARAMS(camMain, 
//									<<996.274475, -2126.295410, 30.160732>>, 
//									<<-2.658349, 0.699294, -10.082686>>, 
//									44.001450,
//									DEFAULT_INTERP_TO_FROM_GAME, 
//									GRAPH_TYPE_ACCEL,
//									GRAPH_TYPE_ACCEL)
//					
//					SET_CAM_ACTIVE(camMain, TRUE)
					
					
					//RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME * 2, FALSE)

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC2_TIME_TO_SAVE_FRANKLIN)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("MI_2_FRANKLIN_KNIFE_ATTACK")
						STOP_AUDIO_SCENE("MI_2_FRANKLIN_KNIFE_ATTACK")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_SPLINE_2")
						START_AUDIO_SCENE("MI_2_SPLINE_2")
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_GRAPPLE]++
//				ENDIF
			BREAK
			CASE 4
				IF wtLastWeapon != WEAPONTYPE_UNARMED
					REQUEST_WEAPON_ASSET(wtLastWeapon)
				ELSE
					REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
				ENDIF
				
//				IF IS_CAM_RENDERING(camMain)
//					RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME * 2, FALSE)
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//					
////					SET_LABEL_AS_TRIGGERED("FranklinGrapple", TRUE)
//					
//					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC2_TIME_TO_SAVE_FRANKLIN)
//				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchBackToMichael")
					IF HANDLE_SWITCH_CAM_FRANKLIN_GRAPPLE_TO_MICHAEL(scsSwitchCam_FranklinGrappleToMichael)
					//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_FINAL_RENDERED_CAM_COORD()) < 2.0
						//Audio Scene
						IF IS_AUDIO_SCENE_ACTIVE("MI_2_SPLINE_2")
							STOP_AUDIO_SCENE("MI_2_SPLINE_2")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_SAVE_FRANKLIN")
							START_AUDIO_SCENE("MI_2_SAVE_FRANKLIN")
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("SwitchBackToMichael", TRUE)
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN)
				ENDIF
				
				//[MF] Stop gameplay cam shake for after we return to Mike
				IF bFrankGrappleToMike_GameplayCamShakeActivated
					IF TIMERB() > iFrankGrappleToMike_GameplayCamShakeDuration
						STOP_GAMEPLAY_CAM_SHAKING(FALSE)
						bFrankGrappleToMike_GameplayCamShakeActivated = FALSE
					ENDIF
				ENDIF
				
				IF NOT SAFE_IS_PLAYER_CONTROL_ON()
					IF eSwitchCamState != SWITCH_CAM_PLAYING_SPLINE
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN)
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_FINAL_RENDERED_CAM_COORD()) < 2.0
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
				
				IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				OR eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				ENDIF
				
				//Audio
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FranklinSaved")
					IF NOT IS_MUSIC_ONESHOT_PLAYING()
						LOAD_AUDIO(MIC2_FRANK_SAVED)
						
						SET_LABEL_AS_TRIGGERED("FranklinSaved", TRUE)
					ENDIF
				ENDIF
				
//				IF IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
				IF GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex) < 600
					TASK_PLAY_ANIM_ADVANCED(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_IGNORE_GRAVITY, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
					TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_b_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_IGNORE_GRAVITY | AF_ENDS_IN_DEAD_POSE, 0.469, EULER_YXZ, AIK_DISABLE_LEG_IK)
					
					SET_PED_DROPS_WEAPON(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
					
					SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_FRANKLIN), TRUE, -1, "DEFAULT_ACTION")
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
					CREATE_CONVERSATION_ADV("MCH2_SAVE")
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					
					SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_GRAPPLE].blipIndex)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("MI_2_SAVE_FRANKLIN")
						STOP_AUDIO_SCENE("MI_2_SAVE_FRANKLIN")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_SHOOTOUT")
						START_AUDIO_SCENE("MI_2_ESCAPE_SHOOTOUT")
					ENDIF
					
					//Audio
					PLAY_AUDIO(MIC2_FRANK_SAVED)
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, MIC2_TIME_TO_SAVE_FRANKLIN)
					
					IF NOT DOES_CAM_EXIST(camMain)
						camMain = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					ENDIF
					
					SET_CAM_PARAMS(camMain, 
									<<995.838806, -2109.341309, 30.128569>>,
									<<6.531624, 0.0, -44.823853>>,
									33.426998)
					
					SET_CAM_PARAMS(camMain, 
									<<996.001099, -2109.178223, 30.154921>>,
									<<6.531624, 0.0, -44.823853>>,
									33.426998,
									3000, 
									GRAPH_TYPE_SIN_ACCEL_DECEL,
									GRAPH_TYPE_SIN_ACCEL_DECEL)
					
					SET_CAM_ACTIVE(camMain, TRUE)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					SHAKE_CAM(camMain, "MEDIUM_EXPLOSION_SHAKE", 0.2)
					
					SET_LABEL_AS_TRIGGERED("FranklinGrapple", TRUE)

					//[MF] Safety stop gameplay cam shake for after we return to Mike
					IF bFrankGrappleToMike_GameplayCamShakeActivated
						STOP_GAMEPLAY_CAM_SHAKING(FALSE)
						bFrankGrappleToMike_GameplayCamShakeActivated = FALSE
					ENDIF

					iSetPiece[SET_PIECE_GRAPPLE]++
				ENDIF
				
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<997.893860, -2107.579834, 31.975397>>, <<8.0, 4.5, 2.5>>)
//					SET_PED_CAPSULE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, 0.75)
//					
//					IF NOT IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon")
//					AND NOT (iGrappleStage >= 10
//					AND (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 0.373))
//						TASK_PLAY_ANIM_ADVANCED(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_a_p_one", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//						
//						TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon", <<997.380, -2103.649, 29.450 + 0.07>>, <<0.0, 0.0, -95.0>>, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
//					ENDIF
//				ENDIF
				
//				IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon")
//				AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon") >= 0.373
				IF iGrappleStage >= 10
				AND (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 0.373)
					CLEAR_TEXT()
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("scr_mich2_blood_stab")
//					IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon")
//					AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon") >= 0.400
					IF iGrappleStage >= 10
					AND (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 0.400)
						//START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mich2_blood_stab", <<998.649719, -2107.086182, 29.821081>>, VECTOR_ZERO)
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_mich2_blood_stab", sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, VECTOR_ZERO, VECTOR_ZERO)
						
						SET_LABEL_AS_TRIGGERED("scr_mich2_blood_stab", TRUE)
					ENDIF
				ENDIF
				
//				IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon")
//				AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon") >= 0.600
				IF iGrappleStage >= 10
				AND (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 0.600)
					bRadar = FALSE
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
					eMissionFail = failFranklinDied
					
					missionFailed()
				ENDIF
			BREAK
			CASE 5
				IF IS_CAM_SHAKING(camMain)
					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one")
					OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one")
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one") >= 0.15)
						STOP_CAM_SHAKING(camMain)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one")
				OR (IS_ENTITY_PLAYING_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one")
				AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED(CHAR_FRANKLIN), sAnimDictMic2IG11, "mic_2_ig_11_b_p_one") >= 0.75)
					REPLAY_RECORD_BACK_FOR_TIME(4.0)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_LABEL_AS_TRIGGERED("StartMichaelAnim", TRUE)
					
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
					SET_PED_USING_ACTION_MODE(NOT_PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
					
					SET_PED_ACCURACY(NOT_PLAYER_PED_ID(), 20)
					
					IF wtLastWeapon != WEAPONTYPE_UNARMED
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), wtLastWeapon, TRUE)
					ELSE
						IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
							GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
					ENDIF
					
					SET_ENTITY_HEALTH(PLAYER_PED(CHAR_FRANKLIN), GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_FRANKLIN)))
					
					//Blocking
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), TRUE)
					
					IF iCutsceneStage > 0
						iCutsceneStage--	PRINTLN("iCutsceneStage = ", iCutsceneStage)	//Should re-assign the tasks
					ENDIF
					
					iSetPiece[SET_PIECE_GRAPPLE]++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF iSetPiece[SET_PIECE_GRAPPLE] <= 4
			SWITCH iGrappleStage
				CASE 0
					//Do nothing
				BREAK
				CASE 1
					sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
					
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_intro_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
					
					TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_intro_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
					
					iGrappleStage++
				BREAK
				CASE 2
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 4
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 5
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 6
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 7
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 8
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_winning_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 9
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_losing_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
				CASE 10
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 1.0)
						sceneGrapple = CREATE_SYNCHRONIZED_SCENE(vGrapplePos, vGrappleRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_FRANKLIN), sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_a_p_one", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_FRANKLIN))
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sceneGrapple, sAnimDictMic2IG11, "mic_2_ig_11_a_goon", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
						
						iGrappleStage++
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex)
		AND GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex) >= 600
		AND iSetPiece[SET_PIECE_GRAPPLE] > 1
//		AND NOT (IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon")
//		AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, sAnimDictMic2IG11, "mic_2_ig_11_a_goon") >= 0.373)
		AND NOT (iGrappleStage >= 10
		AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneGrapple) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneGrapple) >= 0.373)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				//IF GET_GAME_TIMER() > iDialogueTimer[0]
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_KNIFE_1")
					
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, sEnemySetPiece[SET_PIECE_GRAPPLE].pedIndex, "MCH2GOON")
						
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_KNIFE", "MCH2_KNIFE_1")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_KNIFE_2")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_KNIFE", "MCH2_KNIFE_2")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_KNIFE_3")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_KNIFE", "MCH2_KNIFE_3")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_KNIFE_4")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_KNIFE", "MCH2_KNIFE_4")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_KNIFE_5")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_KNIFE", "MCH2_KNIFE_5")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_KNIFE_6")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_KNIFE", "MCH2_KNIFE_6")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_1")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_1")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_2")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_2")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_3")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_3")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_4")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_4")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_5")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_5")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_6")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_6")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_7")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_7")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_8")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_8")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_9")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_9")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_TRAP_10")
						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_TRAP", "MCH2_TRAP_10")
					ENDIF
					
				//	iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 5000)
				//ENDIF
			ENDIF
		ENDIF
		
		//Kill enemies who ragdoll inside the navmesh blocking area
		INT i
		
		REPEAT COUNT_OF(sEnemyBackup) i
			IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
			AND NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//				DRAW_DEBUG_POLY_ANGLED_AREA(<<991.279846, -2131.790283, 29.475735>>, <<993.158691, -2111.559814, 34.475296>>, 4.5)
//				DRAW_DEBUG_POLY_ANGLED_AREA(<<1000.158203, -2132.114014, 29.476189>>, <<1001.959473, -2111.474365, 34.475670>>, 5.25)
				IF IS_ENTITY_IN_ANGLED_AREA(sEnemyBackup[i].pedIndex, <<991.279846, -2131.790283, 29.475735>>, <<993.158691, -2111.559814, 34.475296>>, 4.5)
				OR IS_ENTITY_IN_ANGLED_AREA(sEnemyBackup[i].pedIndex, <<1000.158203, -2132.114014, 29.476189>>, <<1001.959473, -2111.474365, 34.475670>>, 5.25)
					IF IS_PED_RAGDOLL(sEnemyBackup[i].pedIndex)
						SET_ENTITY_HEALTH(sEnemyBackup[i].pedIndex, 5)
						APPLY_DAMAGE_TO_PED(sEnemyBackup[i].pedIndex, 500, FALSE)
						PRINTLN("sEnemyBackup[", i, "].pedIndex ragdolled into a navmesh blocking area and had to be killed.")
					ELSE
						APPLY_DAMAGE_TO_PED(sEnemyBackup[i].pedIndex, 1, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Update Players navmesh blocknig area so the buddy won't run into him...		
		IF iNavBlock[0] = 0
			iNavBlock[0] = ADD_NAVMESH_BLOCKING_OBJECT(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0.75, 0.75, 1.0>>, (GET_ENTITY_HEADING(PLAYER_PED_ID()) / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[0] = ", iNavBlock[1])
		ELSE
			UPDATE_NAVMESH_BLOCKING_OBJECT(iNavBlock[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0.75, 0.75, 1.0>>, (GET_ENTITY_HEADING(PLAYER_PED_ID()) / 360) * 6.28)
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_SHOOT_FROM_MEATHOOK")
			STOP_AUDIO_SCENE("MI_2_SHOOT_FROM_MEATHOOK")
		ENDIF
		
		//Cheat
		DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, FALSE)
		
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, MIC2_TIME_TO_SAVE_FRANKLIN)
		
		SAFE_REMOVE_BLIP(blipMichael)
		SAFE_REMOVE_BLIP(blipFranklin)
		SAFE_REMOVE_BLIP(blipDestination)
		
		IF IS_ENTITY_ATTACHED(PLAYER_PED(CHAR_MICHAEL))
			DETACH_ENTITY(PLAYER_PED(CHAR_MICHAEL))
			SET_ENABLE_BOUND_ANKLES(PLAYER_PED(CHAR_MICHAEL), FALSE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
		
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_ForcedToStayInCover, FALSE)
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(sEnemyBackup) i
			IF DOES_ENTITY_EXIST(sEnemyBackup[i].pedIndex)
			AND NOT IS_PED_INJURED(sEnemyBackup[i].pedIndex)
				SET_ENTITY_VISIBLE(sEnemyBackup[i].pedIndex, TRUE)
				FREEZE_ENTITY_POSITION(sEnemyBackup[i].pedIndex, FALSE)
			ENDIF
		ENDREPEAT
		
		REPEAT iEnemyBackup i
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyBackup[i].pedIndex)
			SAFE_REMOVE_BLIP(sEnemyBackup[i].blipIndex)
		ENDREPEAT
		
		REPEAT iEnemySetPiece i
			SET_PED_AS_NO_LONGER_NEEDED(sEnemySetPiece[i].pedIndex)
			SAFE_REMOVE_BLIP(sEnemySetPiece[i].blipIndex)
		ENDREPEAT
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("Michael_2_Break_Free")
		
		REMOVE_CLIP_SET(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC michaelFree()
	IF INIT_STAGE()
		//Replay
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayMichaelFree), "stageMichaelFree")
		
		//Uncapped
		bSetUncapped = TRUE
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, FALSE)
		
		IF CAN_CREATE_RANDOM_COPS()
			SET_CREATE_RANDOM_COPS(FALSE)
		ENDIF
		
		//Player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Radar
		bRadar = TRUE
		
		//Vehicle
		IF NOT DOES_ENTITY_EXIST(vehHachiRoku)
			vehHachiRoku = CREATE_VEHICLE(COQUETTE, <<950.4739, -2104.9517, 29.6107>>, 106.1659)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehHachiRoku, TRUE)
			SET_VEHICLE_COLOURS(vehHachiRoku, 4, 0)
			SET_VEHICLE_EXTRA_COLOURS(vehHachiRoku, 0, 0)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehHachiRoku)
			SET_VEHICLE_TYRES_CAN_BURST(vehHachiRoku, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(COQUETTE)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehEscape)
			vehEscape = CREATE_VEHICLE(FELTZER2, <<953.9548, -2113.3518, 29.5516>>, 88.1350)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehEscape, TRUE)
			SET_VEHICLE_COLOURS(vehEscape, 38, 0)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehEscape)
			SET_VEHICLE_TYRES_CAN_BURST(vehEscape, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(FELTZER2)
		ENDIF
		
		//Rail
		bOkToMoveRail = FALSE
		bOkToHangFromHook = FALSE
		
		//Audio
		PLAY_AUDIO(MIC2_FRANK_SAVED)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_SHOOTOUT")
			START_AUDIO_SCENE("MI_2_ESCAPE_SHOOTOUT")
		ENDIF
		
		SAFE_ADD_BLIP_PED(blipFranklin, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
		
		//Crates
		IF NOT DOES_ENTITY_EXIST(objCrate[0])
			objCrate[0] = CREATE_OBJECT_NO_OFFSET(PROP_WATERCRATE_01, <<985.1165, -2111.0317, 29.4751>>)
			RETAIN_ENTITY_IN_INTERIOR(objCrate[0], intAbattoir)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objCrate[1])
			objCrate[1] = CREATE_OBJECT_NO_OFFSET(PROP_WATERCRATE_01, <<977.4531, -2128.1602, 29.4753>>)
			RETAIN_ENTITY_IN_INTERIOR(objCrate[1], intAbattoir)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objCrate[2])
			objCrate[2] = CREATE_OBJECT_NO_OFFSET(PROP_WATERCRATE_01, <<987.7, -2125.6, 29.4753>>)
			RETAIN_ENTITY_IN_INTERIOR(objCrate[2], intAbattoir)
		ENDIF
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<996.3627, -2142.4551, 28.4762>>)
			covPoint[13] = ADD_COVER_POINT(<<996.3627, -2142.4551, 28.4762>>, 4.7376, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<993.2383, -2131.0708, 29.4758>>)
			covPoint[16] = ADD_COVER_POINT(<<993.2383, -2131.0708, 29.4758>>, 11.4085, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<989.3762, -2108.0239, 29.4751>>)
			covPoint[17] = ADD_COVER_POINT(<<989.3762, -2108.0239, 29.4751>>, 178.0079, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		ENDIF
		
		ADD_COVER_BLOCKING_AREA(<<985.11951, -2110.06934, 29.47508>>, <<984.31635, -2110.86963, 29.47506>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<990.234863, -2109.403076, 29.475147>>, <<991.734863, -2107.903076, 35.475147>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<984.292725, -2125.887451, 29.475412>>, <<985.292725, -2124.887451, 33.475412>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<971.352966, -2122.628662, 30.562727>> - <<0.25, 0.25, 1.25>>, <<971.352966, -2122.628662, 30.562727>> + <<0.25, 0.25, 1.25>>, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<967.890320,-2120.730469,31.229494>> - <<0.75, 1.0, 1.0>>, <<967.890320,-2120.730469,31.229494>> + <<0.75, 1.0, 1.0>>, TRUE, TRUE, TRUE)
		
		//Navmesh Blocking Areas
		IF iNavBlock[1] = 0
			iNavBlock[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<992.0, -2121.8, 29.5>>, <<4.0, 20.0, 10.0>>, (-5.0 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[1] = ", iNavBlock[1])
		ENDIF
		
		IF iNavBlock[2] = 0
			iNavBlock[2] = ADD_NAVMESH_BLOCKING_OBJECT(<<1001.1, -2121.8, 29.5>>, <<4.6, 20.0, 10.0>>, (-5.0 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[2] = ", iNavBlock[2])
		ENDIF
		
		IF iNavBlock[3] = 0
			iNavBlock[3] = ADD_NAVMESH_BLOCKING_OBJECT(<<984.1, -2135.5, 29.0>>, <<8.3, 8.0, 10.0>>, (-5.250 / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[3] = ", iNavBlock[3])
		ENDIF
		
		IF iNavBlock[4] = 0
			iNavBlock[4] = ADD_NAVMESH_BLOCKING_OBJECT(<<984.791199, -2115.926270, 31.313723>>, <<1.75, 3.75, 2.0>>, 0.0)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[4] = ", iNavBlock[4])
		ENDIF
		
		//Enemies
		//Set Piece
		createEnemy(sEnemySetPiece[SET_PIECE_CONVEYOR], <<981.4547, -2120.6746, 29.4753>>, 333.5943, WEAPONTYPE_ASSAULTRIFLE)	//Checkme
		createEnemy(sEnemySetPiece[SET_PIECE_COW], <<967.1338, -2106.4326, 30.4775>>, 176.0, WEAPONTYPE_PISTOL)
		
		IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
			//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, intAbattoir)
			//SET_PED_CAN_RAGDOLL(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, FALSE)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, PCF_DontActivateRagdollFromBulletImpact, TRUE)
			//SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, PCF_DontActivateRagdollFromExplosions, TRUE)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, PCF_DontActivateRagdollFromElectrocution, TRUE)
			SET_PED_CONFIG_FLAG(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, PCF_DontActivateRagdollFromFire, TRUE)
			SET_RAGDOLL_BLOCKING_FLAGS(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, RBF_BULLET_IMPACT | RBF_EXPLOSION | RBF_ELECTROCUTION | RBF_FIRE | RBF_RUBBER_BULLET)
			//SET_ENTITY_PROOFS(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, FALSE, FALSE, TRUE, FALSE, FALSE)
			SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, CM_STATIONARY)
			SET_PED_SUFFERS_CRITICAL_HITS(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, FALSE)
			SET_PED_MAX_HEALTH(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 1000)
			SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 1000)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_COW].pedIndex)
			//RETAIN_ENTITY_IN_INTERIOR(sEnemySetPiece[SET_PIECE_COW].pedIndex, intAbattoir)
			SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_COW].pedIndex, CM_STATIONARY)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_COW].pedIndex, TRUE)
		ENDIF
		
		//Escape
		createEnemy(sEnemyEscape[0], <<985.5591, -2120.3320, 29.4990>>, 73.0589, WEAPONTYPE_SMG, G_M_M_CHIGOON_01)
		SET_PED_COMBAT_MOVEMENT(sEnemyEscape[0].pedIndex, CM_DEFENSIVE)
		createEnemy(sEnemyEscape[1], <<973.8496, -2126.1538, 29.4987>>, 341.4595, WEAPONTYPE_ASSAULTRIFLE)
		sEnemyEscape[1].pedCheck[0] = sEnemyEscape[0].pedIndex	sEnemyEscape[1].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyEscape[1].vPoint[0] = <<984.9330, -2126.6907, 29.4991>>	sEnemyEscape[1].fSpeed[0] = PEDMOVE_WALK
		//createEnemy(sEnemyEscape[2], <<977.23, -2116.37, 30.4615>>, -65.1603, WEAPONTYPE_SMG)
		createEnemy(sEnemyEscape[3], <<965.7297, -2126.3198, 30.4936>>, 269.6515, WEAPONTYPE_PUMPSHOTGUN, G_M_M_CHIGOON_01)
		sEnemyEscape[3].vLocate[0] = <<976.414978, -2125.813965, 31.498732>>	sEnemyEscape[3].vLocSize[0] = <<15.0, 5.0, 2.0>>	sEnemyEscape[3].eAdvanceStyle[0] = GUN_TO_POINT	sEnemyEscape[3].vPoint[0] = <<959.6390, -2125.6313, 30.4828>>	sEnemyEscape[3].fSpeed[0] = 0.5
		createEnemy(sEnemyEscape[4], <<967.5165, -2121.6035, 30.4793>>, 175.4117, WEAPONTYPE_PISTOL, G_M_M_CHIGOON_01)
		SET_PED_SPHERE_DEFENSIVE_AREA(sEnemyEscape[4].pedIndex, <<967.5165, -2121.6035, 30.4793>>, 1.5)
		TASK_SEEK_COVER_TO_COORDS(sEnemyEscape[4].pedIndex, <<967.5165, -2121.6035, 30.4793>>, <<978.9468, -2125.8413, 29.4752>>, -1, TRUE)
		createEnemy(sEnemyEscape[5], <<968.5446, -2105.6531, 30.4971>>, 93.5294, WEAPONTYPE_SMG)
		createEnemy(sEnemyEscape[6], <<952.9742, -2124.7236, 30.4485>>, 266.8633, WEAPONTYPE_PISTOL)
		createEnemy(sEnemyEscape[7], <<958.5165, -2123.0886, 30.4577>>, 263.1990, WEAPONTYPE_PISTOL)
		
		INT i
		
		REPEAT COUNT_OF(sEnemyEscape) i
			IF DOES_ENTITY_EXIST(sEnemyEscape[i].pedIndex)
				//RETAIN_ENTITY_IN_INTERIOR(sEnemyEscape[i].pedIndex, intAbattoir)
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(sEnemyEscape) i
			IF NOT IS_PED_INJURED(sEnemyEscape[i].pedIndex)
				ASSIGN_DEBUG_NAMES(sEnemyEscape[i].pedIndex, "sEnemyEscape ", i)
				sEnemyEscape[i].sDebugName = CONCATENATE_STRINGS("sEnemyEscape ", GET_STRING_FROM_INT(i))
			ENDIF
		ENDREPEAT
		#ENDIF
		
		//Michael
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_MICHAEL), CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_MICHAEL), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, FALSE)
		SET_PED_COMBAT_MOVEMENT(PLAYER_PED(CHAR_MICHAEL), CM_DEFENSIVE)
		
		SET_PED_ACCURACY(PLAYER_PED(CHAR_MICHAEL), 20)
		
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_MICHAEL), RBF_BULLET_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_MICHAEL), TRUE, -1, "DEFAULT_ACTION")
		
		//Franklin Combat
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_FRANKLIN), CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_FRANKLIN), CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED(CHAR_FRANKLIN), CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_COMBAT_MOVEMENT(PLAYER_PED(CHAR_FRANKLIN), CM_DEFENSIVE)
		
		SET_PED_ACCURACY(PLAYER_PED(CHAR_FRANKLIN), 20)
		
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_FRANKLIN), RBF_BULLET_IMPACT)
		
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
		
		SET_ENTITY_PROOFS(PLAYER_PED(CHAR_FRANKLIN), FALSE, FALSE, FALSE, FALSE, FALSE)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_FRANKLIN), TRUE, -1, "DEFAULT_ACTION")
		
		//Skip Michael Forwards!
		iCurrentNode = 31
		vRailCurrent = sRailNodes[31].vPos
		SET_ENTITY_COORDS_NO_OFFSET(objHook, sRailNodes[31].vPos)
		SET_ENTITY_ROTATION(objHook, <<0.0, 0.0, -6.6>>)	//sRailNodes[31].vRot
		FREEZE_ENTITY_POSITION(objHook, TRUE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_MICHAEL), <<993.9323, -2150.0239, 28.4763>>, 354.8923, FALSE)
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<998.3578, -2106.8503, 29.4754>>, 84.7433, FALSE)
			
			//Freeze
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), TRUE)
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			
			//Pin Interior
			IF intAbattoir = NULL
				intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<990.9518, -2178.6208, 29.0257>>, "v_abattoir")
			ENDIF
			
			PIN_INTERIOR_IN_MEMORY(intAbattoir)
			
			WHILE NOT IS_INTERIOR_READY(intAbattoir)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedAbattoir = TRUE
			
			//Unfreeze
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), FALSE)
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<996.5976, -2148.9912, 29.0>>)
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			MAKE_PICKUPS()
			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_COMBATPISTOL)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
			
			WHILE NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_COMBATPISTOL)
			OR NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				PRINTLN("LOADING WEAPON ASSETS (WEAPONTYPE_COMBATPISTOL)...")
				PRINTLN("LOADING WEAPON ASSETS (WEAPONTYPE_ASSAULTRIFLE)...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED(CHAR_MICHAEL))
			GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350, TRUE)
			SET_PED_AMMO(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
			
			SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			//Franklin
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE)
				GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, 350, TRUE)
			ENDIF
			
			enumCharacterList eCharFail = GET_CURRENT_PLAYER_PED_ENUM()
			
			IF GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)) != WEAPONTYPE_UNARMED AND GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)) != WEAPONTYPE_INVALID AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)))
				SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), GET_FAIL_WEAPON(ENUM_TO_INT(eCharFail)), TRUE)
			ELSE
				SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_ASSAULTRIFLE, TRUE)
			ENDIF
			
			//Dialogue
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
			
			//Audio
			PLAY_AUDIO(MIC2_MICHAEL_ESCAPE_RT)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		SWITCH iCutsceneStage
			CASE 0
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FreeMichaelProgress")
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						//Blip
						SAFE_ADD_BLIP_LOCATION(blipDestination, <<960.7275, -2105.6614, 30.9001>>)
						
						//Objective
						PRINT_ADV("MCH2_ESCABA")
						
						ADVANCE_CUTSCENE()
					ENDIF
				ELSE	//The AI behaviour should always progress after the initial wait for player has been reached
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<996.805237, -2107.241699, 34.475368>>, <<8.0, 5.0, 5.0>>)
				AND (NOT IS_ENTITY_PLAYING_ANIM(NOT_PLAYER_PED_ID(), sAnimDictMic2FranklinBeckon, "beckon_a_player1")
				AND NOT IS_ENTITY_PLAYING_ANIM(NOT_PLAYER_PED_ID(), sAnimDictMic2FranklinBeckon, "beckon_b_player1")))
				OR HAS_LABEL_BEEN_TRIGGERED("FreeMichaelProgress")
					IF NOT IS_PED_INJURED(sEnemyEscape[0].pedIndex)
					OR NOT IS_PED_INJURED(sEnemyEscape[1].pedIndex)
						SET_PED_SPHERE_DEFENSIVE_AREA(NOT_PLAYER_PED_ID(), <<985.3551, -2110.1902, 29.4751>>, 10.0)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NOT_PLAYER_PED_ID(), FALSE)
						
						SET_PED_COMBAT_ATTRIBUTES(NOT_PLAYER_PED_ID(), CA_CAN_IGNORE_BLOCKED_LOS_WEIGHTING, TRUE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NOT_PLAYER_PED_ID(), 100.0, COMBAT_PED_DISABLE_AIM_INTRO)
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("FreeMichaelProgress", TRUE)
					
					ADVANCE_CUTSCENE()
				ELSE
					IF IS_ENTITY_AT_COORD(NOT_PLAYER_PED_ID(), <<996.805237, -2107.241699, 34.475368>>, <<8.0, 5.0, 5.0>>)
						IF NOT IS_PED_FACING_PED(NOT_PLAYER_PED_ID(), PLAYER_PED_ID(), 60.0)
							IF GET_SCRIPT_TASK_STATUS(NOT_PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(NOT_PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
								TASK_TURN_PED_TO_FACE_COORD(NOT_PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)))
							ENDIF
						ELSE
							IF GET_GAME_TIMER() > iDialogueTimer[1]
								IF NOT IS_ENTITY_PLAYING_ANIM(NOT_PLAYER_PED_ID(), sAnimDictMic2FranklinBeckon, "beckon_a_player1")
								AND NOT IS_ENTITY_PLAYING_ANIM(NOT_PLAYER_PED_ID(), sAnimDictMic2FranklinBeckon, "beckon_b_player1")
									INT iRandom
									
									iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
									
									IF iRandom = 0
										TASK_PLAY_ANIM(NOT_PLAYER_PED_ID(), sAnimDictMic2FranklinBeckon, "beckon_a_player1", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
									ELIF iRandom = 1
										TASK_PLAY_ANIM(NOT_PLAYER_PED_ID(), sAnimDictMic2FranklinBeckon, "beckon_b_player1", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
									ENDIF
									
									IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
										IF iDialogueLineCount[4] = -1
											iDialogueLineCount[4] = 4
										ENDIF
										
										IF iDialogueLineCount[4] > 0
											IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL))) < 50.0
												enumSubtitlesState eSubtitles
												
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
													eSubtitles = DISPLAY_SUBTITLES
												ELSE
													eSubtitles = DO_NOT_DISPLAY_SUBTITLES
												ENDIF
												
												CREATE_CONVERSATION_ADV("MCH2_MWRONG", CONV_PRIORITY_MEDIUM, FALSE, eSubtitles)
												
												iDialogueLineCount[4]--
											ENDIF
										ENDIF
									ENDIF
									
									iDialogueTimer[1] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF IS_PED_INJURED(sEnemyEscape[0].pedIndex)
				AND IS_PED_INJURED(sEnemyEscape[1].pedIndex)
				//AND IS_PED_INJURED(sEnemyEscape[2].pedIndex)
				AND IS_PED_INJURED(sEnemySetPiece[SET_PIECE_MINCER].pedIndex)
					IF NOT IS_PED_INJURED(sEnemyEscape[3].pedIndex)
					OR NOT IS_PED_INJURED(sEnemyEscape[4].pedIndex)
						//Combat
						CLEAR_PED_TASKS(NOT_PLAYER_PED_ID())
						
						SET_PED_SPHERE_DEFENSIVE_AREA(NOT_PLAYER_PED_ID(), <<972.0844, -2122.0654, 29.4749>>, 2.0, TRUE)
						SET_PED_SPHERE_DEFENSIVE_AREA(NOT_PLAYER_PED_ID(), <<978.3341, -2128.0117, 29.4753>>, 2.0, TRUE, TRUE)
						
						SET_PED_COMBAT_ATTRIBUTES(NOT_PLAYER_PED_ID(), CA_CAN_IGNORE_BLOCKED_LOS_WEIGHTING, FALSE)
						
						IF NOT IS_ENTITY_AT_COORD(NOT_PLAYER_PED_ID(), <<972.0844, -2122.0654, 29.4749>>, <<4.0, 4.0, 3.0>>)
						AND NOT IS_ENTITY_AT_COORD(NOT_PLAYER_PED_ID(), <<978.3341, -2128.0117, 29.4753>>, <<4.0, 4.0, 3.0>>)
							OPEN_SEQUENCE_TASK(seqMain)  
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<985.8, -2126.6, 29.4753>>, <<963.8723, -2125.1963, 30.4913>>, PEDMOVE_RUN, TRUE, 2.0)
								//TASK_SEEK_COVER_TO_COORDS(NULL, <<978.3341, -2128.0117, 29.4753>>, <<963.8723, -2125.1963, 30.4913>>, 1000, TRUE)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(NOT_PLAYER_PED_ID(), seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
						ENDIF
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF IS_PED_INJURED(sEnemyEscape[3].pedIndex)
				AND IS_PED_INJURED(sEnemyEscape[4].pedIndex)
				AND IS_PED_INJURED(sEnemyEscape[6].pedIndex)
				AND IS_PED_INJURED(sEnemyEscape[7].pedIndex)
					IF NOT IS_PED_INJURED(sEnemyEscape[5].pedIndex)
					//AND NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_COW].pedIndex)	//sEnemyEscape[6].pedIndex)
						//Combat
						CLEAR_PED_TASKS(NOT_PLAYER_PED_ID())
						
						SET_PED_SPHERE_DEFENSIVE_AREA(NOT_PLAYER_PED_ID(), <<968.1083, -2122.2932, 30.4033>>, 2.0, TRUE)
						SET_PED_SPHERE_DEFENSIVE_AREA(NOT_PLAYER_PED_ID(), <<962.6502, -2125.5088, 30.4637>>, 4.0, TRUE, TRUE)
						
						IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<968.1083, -2122.2932, 30.4033>>, <<2.0, 2.0, 2.0>>)
							IF NOT IS_ENTITY_AT_COORD(NOT_PLAYER_PED_ID(), <<968.1083, -2122.2932, 30.4033>>, <<4.0, 4.0, 3.0>>)
							AND NOT IS_ENTITY_AT_COORD(NOT_PLAYER_PED_ID(), <<962.6502, -2125.5088, 30.4637>>, <<4.0, 4.0, 3.0>>)
								OPEN_SEQUENCE_TASK(seqMain)  
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<968.2432, -2122.7563, 30.3472>>, <<965.9512, -2111.9126, 30.4946>>, PEDMOVE_RUN, TRUE, 1.0, 4.0, TRUE, ENAV_NO_STOPPING, GO_TO_AIM_AT_GOTO_COORD_IF_TARGET_LOS_BLOCKED)
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, <<968.0346, -2122.3384, 30.4009>>, -1, TRUE, 1.0, TRUE, TRUE, NULL, TRUE)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(NOT_PLAYER_PED_ID(), seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
							ENDIF
						ENDIF
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 4
				IF GET_SCRIPT_TASK_STATUS(NOT_PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
					SET_PED_RESET_FLAG(NOT_PLAYER_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
					
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<968.0346, -2122.3384, 30.4009>>, <<2.0, 2.0, 2.0>>)
					AND IS_PED_IN_COVER(PLAYER_PED_ID())
						CLEAR_PED_TASKS(NOT_PLAYER_PED_ID())
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NOT_PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_COMBAT(NOT_PLAYER_PED_ID())
				AND IS_PED_IN_COVER(NOT_PLAYER_PED_ID())
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NOT_PLAYER_PED_ID(), 50.0)
				ENDIF
				
				IF IS_PED_INJURED(sEnemyEscape[5].pedIndex)
				AND IS_PED_INJURED(sEnemySetPiece[SET_PIECE_COW].pedIndex)	//sEnemyEscape[6].pedIndex)
					//Combat
					CLEAR_PED_TASKS(NOT_PLAYER_PED_ID())
					
					REMOVE_PED_DEFENSIVE_AREA(NOT_PLAYER_PED_ID())
					REMOVE_PED_DEFENSIVE_AREA(NOT_PLAYER_PED_ID(), TRUE)
					
					OPEN_SEQUENCE_TASK(seqMain)  
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<957.6793, -2107.4541, 29.5544>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 89.4953)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(NOT_PLAYER_PED_ID(), seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF HAS_LABEL_BEEN_TRIGGERED("FreeMichaelProgress")
				IF GET_GAME_TIMER() > iDialogueTimer[0]
					IF iDialogueStage = 0
						IF iDialogueLineCount[iDialogueStage] = -1 
							 iDialogueLineCount[iDialogueStage] = 4
						ENDIF
					ELIF iDialogueStage = 1
						IF iDialogueLineCount[iDialogueStage] = -1 
							 iDialogueLineCount[iDialogueStage] = 6
						ENDIF
					ELIF iDialogueStage = 2
						IF iDialogueLineCount[iDialogueStage] = -1 
							 iDialogueLineCount[iDialogueStage] = 4
						ENDIF
					ELIF iDialogueStage = 3
						IF iDialogueLineCount[iDialogueStage] = -1 
							 iDialogueLineCount[iDialogueStage] = 6
						ENDIF
					ENDIF
					
					IF iDialogueLineCount[iDialogueStage] > 0
						IF DOES_ENTITY_EXIST(pedClosestEnemy)
						AND NOT IS_ENTITY_DEAD(pedClosestEnemy)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy)) < 20.0
						AND IS_ENTITY_ON_SCREEN(pedClosestEnemy)
							IF iDialogueStage = 0
								CREATE_CONVERSATION_ADV("MCH2_ESCRF", CONV_PRIORITY_MEDIUM, FALSE)
							ELIF iDialogueStage = 1
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedClosestEnemy, "MCH2CHIN3")
									
								CREATE_CONVERSATION_ADV("MCH2_AFTER", CONV_PRIORITY_MEDIUM, FALSE)
							ELIF iDialogueStage = 2
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_ESCRM", "MCH2_ESCRM_02")	//CREATE_CONVERSATION_ADV("MCH2_ESCRM", CONV_PRIORITY_MEDIUM, FALSE)
							ELIF iDialogueStage = 3
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedClosestEnemy, "MCH2CHIN4")
									
								CREATE_CONVERSATION_ADV("MCH2_AFTER4", CONV_PRIORITY_MEDIUM, FALSE)
							ENDIF
							
							iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
							
							iDialogueLineCount[iDialogueStage]--
						ENDIF
					ENDIF
					
					IF iDialogueStage < 3
						iDialogueStage++
					ELSE
						iDialogueStage = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Open Combat
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat11")
			IF NOT IS_PED_INJURED(sEnemyEscape[0].pedIndex)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<986.739502, -2109.038330, 31.225237>>, <<3.0, 4.0, 3.0>>)
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<987.7, -2110.5, 29.4990>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(sEnemyEscape[0].pedIndex, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(sEnemyEscape[0].pedIndex, PLAYER_PED_ID(), VECTOR_ZERO, 3.0)
					
					//REMOVE_PED_DEFENSIVE_AREA(sEnemyEscape[0].pedIndex)
					//SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[0].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
					//SET_PED_COMBAT_MOVEMENT(sEnemyEscape[0].pedIndex, CM_WILLADVANCE)
					
					SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[0].pedIndex, CA_CAN_CHARGE, TRUE)
					
					SET_PED_CONFIG_FLAG(sEnemyEscape[0].pedIndex, PCF_TreatAsPlayerDuringTargeting, TRUE)
					
					SET_LABEL_AS_TRIGGERED("OpenCombat11", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat12")
			IF NOT IS_PED_INJURED(sEnemyEscape[1].pedIndex)
				IF IS_PED_INJURED(sEnemyEscape[0].pedIndex)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[1].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
					SET_PED_COMBAT_MOVEMENT(sEnemyEscape[1].pedIndex, CM_WILLADVANCE)
					
					SET_LABEL_AS_TRIGGERED("OpenCombat12", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("OpenCombat13")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<976.414978, -2125.813965, 31.498732>>, <<15.0, 5.0, 2.0>>)
				SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[3].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_MOVEMENT(sEnemyEscape[3].pedIndex, CM_WILLADVANCE)
				
				REMOVE_PED_DEFENSIVE_AREA(sEnemyEscape[6].pedIndex)
				SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[6].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_MOVEMENT(sEnemyEscape[6].pedIndex, CM_WILLADVANCE)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyEscape[6].pedIndex, 500.0)
				
				REMOVE_PED_DEFENSIVE_AREA(sEnemyEscape[7].pedIndex)
				SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[7].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_MOVEMENT(sEnemyEscape[7].pedIndex, CM_WILLADVANCE)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyEscape[7].pedIndex, 500.0)
				
				SET_LABEL_AS_TRIGGERED("OpenCombat13", TRUE)
			ENDIF
		ENDIF
		
		IF NOT sCamDetails.bRun
			IF iCutsceneStage <= 4
				IF UPDATE_SELECTOR_HUD(sSelectorPeds)     // Returns TRUE when the player has made a selection
					IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
						CLEAR_PRINTS()
						
						//Stats
						INFORM_MISSION_STATS_OF_INCREMENT(MIC2_TIMES_SWITCHED)
						
						sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
						sCamDetails.bRun  = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_NONE)
							//Dialogue
							REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 0)
							REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 0, PLAYER_PED(CHAR_MICHAEL), "MICHAEL")
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, PLAYER_PED(CHAR_FRANKLIN), "FRANKLIN")
							
							//Update the relationship groups
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_MICHAEL])
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_MICHAEL], relGroupBuddy)
							ENDIF
							
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[CHAR_FRANKLIN])
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[CHAR_FRANKLIN], relGroupBuddy)
							ENDIF
							
							SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(PLAYER_PED(CHAR_FRANKLIN), PLAYER_PED(CHAR_MICHAEL), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
							SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(PLAYER_PED(CHAR_MICHAEL), PLAYER_PED(CHAR_FRANKLIN), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
							
							SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_MICHAEL), RBF_BULLET_IMPACT | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP)
							SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED(CHAR_FRANKLIN), RBF_BULLET_IMPACT)
							
							SAFE_REMOVE_BLIP(blipMichael)
							SAFE_REMOVE_BLIP(blipFranklin)
							
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
							ENDIF
							
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								SAFE_ADD_BLIP_PED(blipFranklin, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
								
								SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
							ENDIF
							
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
							SET_PED_USING_ACTION_MODE(NOT_PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
							
							IF GET_ENTITY_HEALTH(PLAYER_PED(CHAR_MICHAEL)) < GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_MICHAEL)) / 2
								SET_ENTITY_HEALTH(PLAYER_PED(CHAR_MICHAEL), GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_MICHAEL)) / 2)
							ENDIF
							
							IF GET_ENTITY_HEALTH(PLAYER_PED(CHAR_FRANKLIN)) < GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_FRANKLIN)) / 2
								SET_ENTITY_HEALTH(PLAYER_PED(CHAR_FRANKLIN), GET_PED_MAX_HEALTH(PLAYER_PED(CHAR_FRANKLIN)) / 2)
							ENDIF
							
							IF iCutsceneStage > 0
								iCutsceneStage--	PRINTLN("iCutsceneStage = ", iCutsceneStage)	//Should re-assign the tasks
							ENDIF
							
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
								IF NOT HAS_PED_GOT_WEAPON(NOT_PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
									GIVE_WEAPON_TO_PED(NOT_PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 2), TRUE)
								ENDIF
								
								IF GET_AMMO_IN_PED_WEAPON(NOT_PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL) < (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 2)
									SET_PED_AMMO(NOT_PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 2))
								ENDIF
								
								SET_CURRENT_PED_WEAPON(NOT_PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, TRUE)
							ENDIF
							
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
								IF NOT HAS_PED_GOT_WEAPON(NOT_PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE)
									GIVE_WEAPON_TO_PED(NOT_PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_ASSAULTRIFLE) * 2), TRUE)
								ENDIF
								
								IF GET_AMMO_IN_PED_WEAPON(NOT_PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE) < (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_ASSAULTRIFLE) * 2)
									SET_PED_AMMO(NOT_PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_ASSAULTRIFLE) * 2))
								ENDIF
								
								SET_CURRENT_PED_WEAPON(NOT_PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, TRUE)
							ENDIF
							
							sCamDetails.bPedSwitched = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
		OR (DOES_CAM_EXIST(sCamDetails.camID)
		AND NOT IS_CAM_ACTIVE(sCamDetails.camID))
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NOT_PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 25.0
//			AND NOT IS_ENTITY_ON_SCREEN(NOT_PLAYER_PED_ID())
//			AND iHealthStore = -1
//				iHealthStore = GET_ENTITY_HEALTH(NOT_PLAYER_PED_ID())
//				SET_ENTITY_HEALTH(NOT_PLAYER_PED_ID(), 101)
//			ELIF iHealthStore > -1
//				SET_ENTITY_HEALTH(NOT_PLAYER_PED_ID(), iHealthStore)
//				iHealthStore = -1
//			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NOT_PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 60.0
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					eMissionFail = failFranklinAbandon
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					eMissionFail = failMichaelAbandon
				ENDIF
				
				missionFailed()
			ENDIF
		ENDIF
		
		BOOL bEnemiesDead = TRUE
		
		INT i
		
		REPEAT COUNT_OF(sEnemyEscape) i
			IF DOES_ENTITY_EXIST(sEnemyEscape[i].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyEscape[i].pedIndex)
					bEnemiesDead = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(sEnemySetPiece) i
			IF DOES_ENTITY_EXIST(sEnemySetPiece[i].pedIndex)
				IF NOT IS_PED_INJURED(sEnemySetPiece[i].pedIndex)
					bEnemiesDead = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bEnemiesDead = TRUE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				CREATE_CONVERSATION_ADV("MCH2_ESCF3")
			ENDIF
		ENDIF
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_WAYM")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_WAYF")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(NOT_PLAYER_PED_ID()), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 15.0
					IF IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_MICHAEL), <<966.969360, -2115.812988, 32.474960>>, <<4.0, 13.0, 3.0>>)
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION_ADV("MCH2_WAYM")
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION_ADV("MCH2_WAYF")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("ProgressStage")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<958.522461, -2105.835449, 33.043030>>, <<4.0, 4.0, 2.5>>)
			AND NOT IS_PED_IN_COMBAT(NOT_PLAYER_PED_ID())
				SET_LABEL_AS_TRIGGERED("ProgressStage", TRUE)
			ENDIF
		ELIF iCutsceneStage > 4
			ADVANCE_STAGE()
		ENDIF
		
		//Enemies can charge
		IF NOT HAS_LABEL_BEEN_TRIGGERED("EnemiesCanCharge1")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<988.917908, -2108.542236, 29.475147>>, <<989.372009, -2102.780029, 33.475086>>, 4.5)
				SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[0].pedIndex, CA_CAN_CHARGE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[1].pedIndex, CA_CAN_CHARGE, TRUE)
				
				SET_LABEL_AS_TRIGGERED("EnemiesCanCharge1", TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("EnemiesCanCharge2")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<988.862610, -2126.699219, 29.475573>>, <<967.577759, -2124.836182, 34.478863>>, 7.0)
				REPEAT COUNT_OF(sEnemyEscape) i
					IF i > 1 AND i != 5
						IF DOES_ENTITY_EXIST(sEnemyEscape[i].pedIndex)
						AND NOT IS_PED_INJURED(sEnemyEscape[i].pedIndex)
							SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[i].pedIndex, CA_CAN_CHARGE, TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
				
				SET_LABEL_AS_TRIGGERED("EnemiesCanCharge2", TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("EnemiesCanCharge3")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<960.695251, -2124.824463, 30.460133>>, <<967.684204, -2125.457520, 34.436863>>, 7.0)
				SET_PED_COMBAT_ATTRIBUTES(sEnemyEscape[5].pedIndex, CA_CAN_CHARGE, TRUE)
				
				SET_LABEL_AS_TRIGGERED("EnemiesCanCharge3", TRUE)
			ENDIF
		ENDIF
		
		//Conveyor
		SWITCH iSetPiece[SET_PIECE_CONVEYOR]
			CASE 0
				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
					PED_BONETAG pbt
					
					GET_PED_LAST_DAMAGE_BONE(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, pbt)
					
					IF NOT IS_PED_IN_COMBAT(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<987.460327, -2105.635498, 31.975031>>, <<4.0, 3.5, 2.5>>)
						OR IS_ENTITY_AT_COORD(NOT_PLAYER_PED_ID(), <<987.460327, -2105.635498, 31.975031>>, <<4.0, 3.5, 2.5>>)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 500.0)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
						IF GET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex) < 925
						OR pbt = BONETAG_HEAD
						OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex), 5.0)
							IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
								IF IS_ENTITY_AT_COORD(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, <<981.4547, -2120.6746, 29.4753>>, <<0.5, 0.5, 3.5>>)
								AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<984.946350, -2110.417969, 31.975075>>, <<7.0, 7.5, 5.0>>)
									INFORM_MISSION_STATS_OF_INCREMENT(MIC2_UNIQUE_TRIAD_DEATHS)
									
									SET_PED_CAN_BE_TARGETTED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, FALSE)
									
									//TASK_PLAY_ANIM_ADVANCED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor", <<980.074, -2121.410, 29.482>>, <<0.0, 0.0, 0.0>>, SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
									sceneConveyor = CREATE_SYNCHRONIZED_SCENE(<<980.074, -2121.410, 29.482>>, <<0.0, 0.0, 0.0>>)
									TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sceneConveyor, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN, AIK_DISABLE_LEG_IK)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneConveyor, TRUE)
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
									
									SET_ENTITY_INVINCIBLE(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, TRUE)
									
									SET_PED_CAN_BE_TARGETTED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, FALSE)
									
									PLAY_PAIN(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, AUD_DAMAGE_REASON_SCREAM_PANIC)
									
									IF NOT bVideoRecording
										REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
										
										bVideoRecording = TRUE
									ENDIF
									
									SETTIMERB(0)
									iSetPiece[SET_PIECE_CONVEYOR]++
								ELSE
									SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 5)
									APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 500, FALSE)
									SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CONVEYOR].blipIndex)
									
									iSetPiece[SET_PIECE_CONVEYOR] = 100
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneConveyor)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneConveyor) >= 0.254	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor") > 0.254
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CONVEYOR]++
				ENDIF
			BREAK
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneConveyor)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneConveyor) >= 0.413	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor") > 0.413
					SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 4.0)
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CONVEYOR]++
				ENDIF
			BREAK
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneConveyor)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneConveyor) >= 0.525	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor") > 0.525
					//Audio Scene
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_2")
						START_AUDIO_SCENE("MI_2_GRINDER_2")
					ENDIF
					
					PLAY_PAIN(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, AUD_DAMAGE_REASON_ON_FIRE)
					
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_abattoir_ped_minced", <<976.72, -2120.27, 31.5>>, VECTOR_ZERO)
					
					PLAY_SOUND_FROM_COORD(-1, "MINCER_FALL", <<976.72, -2120.27, 31.5>>, "MICHAEL_2_SOUNDS")
					
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF DOES_ENTITY_EXIST(pedClosestEnemy)
						AND NOT IS_PED_INJURED(pedClosestEnemy)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedClosestEnemy)) < 20.0
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, pedClosestEnemy, "MCH2CHIN5")
								
								CREATE_CONVERSATION_ADV("MCH2_EWW5")
							ENDIF
						ENDIF
					ENDIF
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CONVEYOR]++
				ENDIF
			BREAK
			CASE 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneConveyor)	//IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor")
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneConveyor) >= 1.000	//AND GET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor") >= 1.000
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("MI_2_GRINDER_2")
						STOP_AUDIO_SCENE("MI_2_GRINDER_2")
					ENDIF
					
					SET_ENTITY_INVINCIBLE(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, FALSE)
					
					IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
						SET_ENTITY_HEALTH(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 5)
						APPLY_DAMAGE_TO_PED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex, 500, FALSE)
					ENDIF
					
					SAFE_REMOVE_BLIP(sEnemySetPiece[SET_PIECE_CONVEYOR].blipIndex)
					
					SAFE_DELETE_PED(sEnemySetPiece[SET_PIECE_CONVEYOR].pedIndex)
					
//					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					
//					STOP_CAM_SHAKING(camMain, TRUE)
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SETTIMERB(0)
					iSetPiece[SET_PIECE_CONVEYOR]++
				ENDIF
			BREAK
//			CASE 5
//				IF TIMERB() > 1000
//					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
//					
//					SETTIMERB(0)
//					iSetPiece[SET_PIECE_CONVEYOR]++
//				ENDIF
//			BREAK
		ENDSWITCH
		
//		IF NOT IS_GAMEPLAY_CAM_RENDERING()
//			IF iSetPiece[SET_PIECE_CONVEYOR] > 2
//			AND iSetPiece[SET_PIECE_CONVEYOR] < 100
//				IF DOES_ENTITY_EXIST(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex)
//					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//						IF IS_ENTITY_PLAYING_ANIM(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor")
//							SET_ENTITY_ANIM_CURRENT_TIME(sEnemySetPiece[SET_PIECE_CUTTER].pedIndex, sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor", 1.000)
//						ENDIF
//						
//						iSetPiece[SET_PIECE_CONVEYOR] = 4
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Cow
		SWITCH iSetPiece[SET_PIECE_COW]
			CASE 0
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_COW].pedIndex)
					IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece2)
						objCow = CREATE_OBJECT_NO_OFFSET(V_IND_COO_HALF, <<967.0997, -2108.2424, 34.3186>>)
						
						iSetPiece[SET_PIECE_COW]++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF DOES_ENTITY_EXIST(objCow)
					IF DOES_ENTITY_HAVE_DRAWABLE(objCow)
						sceneCow = CREATE_SYNCHRONIZED_SCENE(sceneCowPos, sceneCowRot)
						
						TASK_SYNCHRONIZED_SCENE(sEnemySetPiece[SET_PIECE_COW].pedIndex, sceneCow, sAnimDictMic2SetPiece2, "goon_pushcow_goon", INSTANT_BLEND_IN, WALK_BLEND_OUT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sEnemySetPiece[SET_PIECE_COW].pedIndex)
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objCow, sceneCow, "goon_pushcow_beef", sAnimDictMic2SetPiece2, INSTANT_BLEND_IN, WALK_BLEND_OUT)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneCow, 0.250)
						SET_SYNCHRONIZED_SCENE_RATE(sceneCow, 0.0)
						
						iSetPiece[SET_PIECE_COW]++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_COW].pedIndex)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneCow)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<967.617432, -2124.157959, 32.474319>>, <<1.5, 4.5, 2.5>>)
							SET_SYNCHRONIZED_SCENE_RATE(sceneCow, 1.0)
							
							IF NOT HAS_PED_GOT_WEAPON(sEnemySetPiece[SET_PIECE_COW].pedIndex, WEAPONTYPE_PISTOL)
								GIVE_WEAPON_TO_PED(sEnemySetPiece[SET_PIECE_COW].pedIndex, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
							ENDIF
							
							SET_CURRENT_PED_WEAPON(sEnemySetPiece[SET_PIECE_COW].pedIndex, WEAPONTYPE_PISTOL, TRUE)
							
							PLAY_PED_AMBIENT_SPEECH(sEnemySetPiece[SET_PIECE_COW].pedIndex, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							
							IF sfxCow = -1
								sfxCow = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(sfxCow, "MEAT_SLIDE", objCow, "MICHAEL_2_SOUNDS")	//MIC_2_MEAT_SLIDE_ON_STOP_MASTER
							ENDIF
							
							iSetPiece[SET_PIECE_COW]++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_PED_INJURED(sEnemySetPiece[SET_PIECE_COW].pedIndex)
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneCow)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneCow) >= 1.0)
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneCow)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemySetPiece[SET_PIECE_COW].pedIndex, FALSE)
						
						REMOVE_PED_DEFENSIVE_AREA(sEnemySetPiece[SET_PIECE_COW].pedIndex)
						SET_PED_COMBAT_ATTRIBUTES(sEnemySetPiece[SET_PIECE_COW].pedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_MOVEMENT(sEnemySetPiece[SET_PIECE_COW].pedIndex, CM_WILLADVANCE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemySetPiece[SET_PIECE_COW].pedIndex, 500.0)
						
						iSetPiece[SET_PIECE_COW]++
					ELIF GET_GAME_TIMER() > iCowShootTimer
						IF HAS_PED_GOT_WEAPON(sEnemySetPiece[SET_PIECE_COW].pedIndex, WEAPONTYPE_PISTOL)
							SET_PED_SHOOTS_AT_COORD(sEnemySetPiece[SET_PIECE_COW].pedIndex, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, VECTOR_ZERO))
						ENDIF
						
						iCowShootTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(200, 500)
					ENDIF
				ENDIF
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneCow)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneCow) >= 1.0)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneCow)
					IF sfxCow != -1
						STOP_SOUND(sfxCow)
						RELEASE_SOUND_ID(sfxCow)
						sfxCow = -1
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		//Update Players navmesh blocknig area so the buddy won't run into him...		
		IF iNavBlock[0] = 0
			iNavBlock[0] = ADD_NAVMESH_BLOCKING_OBJECT(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0.75, 0.75, 1.0>>, (GET_ENTITY_HEADING(PLAYER_PED_ID()) / 360) * 6.28)
			PRINTLN("ADD_NAVMESH_BLOCKING_OBJECT | iNavBlock[0] = ", iNavBlock[1])
		ELSE
			UPDATE_NAVMESH_BLOCKING_OBJECT(iNavBlock[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0.75, 0.75, 1.0>>, (GET_ENTITY_HEADING(PLAYER_PED_ID()) / 360) * 6.28)
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_SHOOTOUT")
			STOP_AUDIO_SCENE("MI_2_ESCAPE_SHOOTOUT")
		ENDIF
		
		IF sfxCow != -1
			STOP_SOUND(sfxCow)
			RELEASE_SOUND_ID(sfxCow)
			sfxCow = -1
		ENDIF
		
//		SAFE_REMOVE_BLIP(blipMichael)
//		SAFE_REMOVE_BLIP(blipFranklin)
		SAFE_REMOVE_BLIP(blipDestination)
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(covPoint) i
			REMOVE_COVER_POINT(covPoint[i])
		ENDREPEAT
		
		REPEAT iEnemySetPiece i
			SET_PED_AS_NO_LONGER_NEEDED(sEnemySetPiece[i].pedIndex)
			SAFE_REMOVE_BLIP(sEnemySetPiece[i].blipIndex)
		ENDREPEAT
		
		REPEAT iEnemyEscape i
			SET_PED_AS_NO_LONGER_NEEDED(sEnemyEscape[i].pedIndex)
			SAFE_REMOVE_BLIP(sEnemyEscape[i].blipIndex)
		ENDREPEAT
		
		SAFE_DELETE_OBJECT(objHook)
		SAFE_DELETE_OBJECT(objChain)
		SAFE_DELETE_OBJECT(objPadlock)
		
		REPEAT COUNT_OF(objCrate) i
			SAFE_DELETE_OBJECT(objCrate[i])
		ENDREPEAT
		
		REPEAT COUNT_OF(objCigarette) i
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxCigarette[i])
				STOP_PARTICLE_FX_LOOPED(ptfxCigarette[i])
			ENDIF
			SAFE_DELETE_OBJECT(objCigarette[i])
		ENDREPEAT
		
		REPEAT COUNT_OF(vehEnemy) i
			SAFE_DELETE_VEHICLE(vehEnemy[i])
		ENDREPEAT
		
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_HOOK)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_PADLOCK)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_LEG_CHAIN_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(BISON)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_COMBATPISTOL))
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_WATERCRATE_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(V_IND_COO_HALF)
		
		REMOVE_PTFX_ASSET()
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC triadsChase()
	IF INIT_STAGE()
		//Replay
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayTriadsChase), "stageTriadsChase", TRUE)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_MICHAEL), TRUE)
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_FRANKLIN), TRUE)
		
		//Uncapped
		bSetUncapped = TRUE
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, FALSE)
		
		IF CAN_CREATE_RANDOM_COPS()
			SET_CREATE_RANDOM_COPS(FALSE)
		ENDIF
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
//		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED(CHAR_FRANKLIN), <<962.994446, -2107.998047, 32.986130>>,<<10.0, 7.0, 2.5>>)
//			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<965.1993, -2108.8315, 30.4914>>, 63.8301)
//		ENDIF
		
		//Blocking of Temporary Events
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_FRANKLIN), PCF_GetOutUndriveableVehicle, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_GetOutUndriveableVehicle, FALSE)
		
		//Objective
		//PRINT_ADV("MCH2_GCLEAR")
		
		//Audio
		PLAY_AUDIO(MIC2_VEHICLE_READY)
		
		LOAD_AUDIO(MIC2_LOSE_TRIADS)
		
//		SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
//		
//		FREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
//		
//		SET_RADIO_AUTO_UNFREEZE(FALSE)
//		
//		SET_RADIO_TRACK("RADIO_01_CLASS_ROCK", "MIC2_OVER_RADIO")
		
		//Taxi
		DISABLE_TAXI_HAILING(TRUE)
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_MICHAEL), <<961.6073, -2107.3604, 30.7408>>, 47.8221, FALSE)
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<960.3185, -2105.0186, 30.9585>>, 99.0454)
			
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), TRUE)
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), TRUE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<961.2763, -2106.1240, 30.5098>>)
			ENDIF
			
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL)
				GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350, TRUE)
			ENDIF
			
			SET_PED_AMMO(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, 350)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), GET_BEST_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL)), TRUE)
			
			//Audio
			PLAY_AUDIO(MIC2_TRIADS_CHASE_RT)
			
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_MICHAEL), FALSE)
			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			
			FORCE_PED_MOTION_STATE(PLAYER_PED(CHAR_FRANKLIN), MS_ON_FOOT_IDLE)
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			ELIF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE	
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175707
		
		SWITCH iCutsceneStage
			CASE 0
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_DRV1")
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						CREATE_CONVERSATION_ADV("MCH2_DRV1")
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_SEECAR")
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED(CHAR_MICHAEL), <<942.229126, -2109.321777, 29.255819>>, <<962.332581, -2109.342529, 33.953644>>, 16.5)
							CREATE_CONVERSATION_ADV("MCH2_SEECAR")
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_GETCAR")
					AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_MDRIVE")
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION_ADV("MCH2_GETCAR")
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION_ADV("MCH2_MDRIVE")
						ENDIF
					ENDIF
				ENDIF
				
				IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<946.914734, -2113.857910, 27.548405>>, <<972.384338, -2116.126465, 35.474815>>, 28.0))
				AND (intAbattoir != NULL
				AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != intAbattoir
				AND GET_INTERIOR_FROM_ENTITY(NOT_PLAYER_PED_ID()) != intAbattoir)
					IF (DOES_ENTITY_EXIST(vehHachiRoku)
					AND NOT IS_ENTITY_DEAD(vehHachiRoku))
					AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehHachiRoku)
						vehEnemyChase[0] = vehHachiRoku
					ELIF (DOES_ENTITY_EXIST(vehEscape)
					AND NOT IS_ENTITY_DEAD(vehEscape))
					AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehEscape)
						vehEnemyChase[0] = vehEscape
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						//Michael driveby weapon
						IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL)
							GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 5, TRUE)
						ENDIF
						
						IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL) < (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 5)
							SET_PED_AMMO(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_COMBATPISTOL) * 5))
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_COMBATPISTOL, TRUE)
						
						//Franklin driveby weapon
						IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PISTOL)
							GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PISTOL, 350, TRUE)
						ENDIF
						
						IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PISTOL) < (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_PISTOL) * 5)
							SET_PED_AMMO(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PISTOL, (GET_WEAPON_CLIP_SIZE(WEAPONTYPE_PISTOL) * 5))
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_PISTOL, TRUE)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT DOES_ENTITY_EXIST(sEnemyChase[0].pedIndex)
				AND NOT DOES_ENTITY_EXIST(sEnemyChase[1].pedIndex)
					createEnemy(sEnemyChase[0], <<965.9470, -2109.8545, 30.4696>>, 56.7835, WEAPONTYPE_PISTOL)
					createEnemy(sEnemyChase[1], <<968.0931, -2110.3267, 30.4746>>, 56.7835, WEAPONTYPE_SMG, G_M_M_CHIGOON_01)
					SET_PED_COMBAT_MOVEMENT(sEnemyChase[0].pedIndex, CM_WILLADVANCE)
					SET_PED_COMBAT_MOVEMENT(sEnemyChase[1].pedIndex, CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[0].pedIndex, CA_AGGRESSIVE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[1].pedIndex, CA_AGGRESSIVE, TRUE)
					
					REMOVE_PED_DEFENSIVE_AREA(sEnemyChase[0].pedIndex)
					REMOVE_PED_DEFENSIVE_AREA(sEnemyChase[1].pedIndex)
					
					GIVE_WEAPON_TO_PED(sEnemyChase[1].pedIndex, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE, FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyChase[0].pedIndex, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sEnemyChase[1].pedIndex, TRUE)
					
					OPEN_SEQUENCE_TASK(seqMain)  
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<960.2808, -2106.7200, 30.9505>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(sEnemyChase[0].pedIndex, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					OPEN_SEQUENCE_TASK(seqMain)  
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<960.2409, -2104.9978, 30.9505>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(sEnemyChase[1].pedIndex, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC2_TIME_TO_LOSE_TRIADS)
				ENDIF
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<941.783325, -2112.604248, 27.505041>>, <<972.538818, -2114.131592, 35.474781>>, 25.0)
					IF (DOES_ENTITY_EXIST(sEnemyChase[0].pedIndex) AND NOT IS_PED_INJURED(sEnemyChase[0].pedIndex))
					OR (DOES_ENTITY_EXIST(sEnemyChase[1].pedIndex) AND NOT IS_PED_INJURED(sEnemyChase[1].pedIndex))
						//Audio
						PLAY_AUDIO(MIC2_LOSE_TRIADS)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				IF IS_PED_INJURED(sEnemyChase[0].pedIndex)
				AND IS_PED_INJURED(sEnemyChase[1].pedIndex)
					ADVANCE_CUTSCENE()
				ELSE
					IF ((NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 350.0)
					OR IS_PED_INJURED(sEnemyChase[0].pedIndex))
					AND ((NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 350.0)
					OR IS_PED_INJURED(sEnemyChase[1].pedIndex))
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-858.0734, 157.1951, 63.7158>>) < 200.0
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF TIMERA() > 1500
						IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_DRV2")
						AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_AFTERUS")
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) < 25.0
							AND ((DOES_ENTITY_EXIST(sEnemyChase[0].pedIndex)
							AND NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
							OR (DOES_ENTITY_EXIST(sEnemyChase[1].pedIndex)
							AND NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
							OR (DOES_ENTITY_EXIST(vehEnemyChase[0])
							AND NOT IS_ENTITY_DEAD(vehEnemyChase[0])
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEnemyChase[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 150.0))
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION_ADV("MCH2_DRV2")
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION_ADV("MCH2_AFTERUS")
								ENDIF
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_LOSEM")
						AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_LOSE")
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							AND IS_PED_IN_VEHICLE(NOT_PLAYER_PED_ID(), GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) < 25.0
								AND ((DOES_ENTITY_EXIST(sEnemyChase[0].pedIndex)
								AND NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
								OR (DOES_ENTITY_EXIST(sEnemyChase[1].pedIndex)
								AND NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
								OR (DOES_ENTITY_EXIST(vehEnemyChase[0])
								AND NOT IS_ENTITY_DEAD(vehEnemyChase[0])
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEnemyChase[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 150.0))
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_MICHAEL))
									AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_MICHAEL)), VS_DRIVER) = PLAYER_PED(CHAR_MICHAEL)
										CREATE_CONVERSATION_ADV("MCH2_LOSEM")
									ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
									AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_FRANKLIN)), VS_DRIVER) = PLAYER_PED(CHAR_FRANKLIN)
										CREATE_CONVERSATION_ADV("MCH2_LOSE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehEscape)
				AND NOT IS_ENTITY_DEAD(vehEscape)
					IF NOT GET_VEHICLE_TYRES_CAN_BURST(vehEscape)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEscape), <<965.9470, -2109.8545, 30.4696>>) > 50.0
							SET_VEHICLE_TYRES_CAN_BURST(vehEscape, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehHachiRoku)
				AND NOT IS_ENTITY_DEAD(vehHachiRoku)
					IF NOT GET_VEHICLE_TYRES_CAN_BURST(vehHachiRoku)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehHachiRoku), <<965.9470, -2109.8545, 30.4696>>) > 50.0
							SET_VEHICLE_TYRES_CAN_BURST(vehHachiRoku, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				BOOL bCloseCombat
				bCloseCombat = TRUE
				
				IF NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
					IF ((IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) < 3.0)
					OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 15.0
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[0].pedIndex, CA_LEAVE_VEHICLES, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[0].pedIndex, CA_USE_VEHICLE, FALSE)
					ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 25.0
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEnemyChase[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 25.0
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[0].pedIndex, CA_LEAVE_VEHICLES, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[0].pedIndex, CA_USE_VEHICLE, TRUE)
						
						bCloseCombat = FALSE
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
					IF ((IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())) < 3.0)
					OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 15.0
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[1].pedIndex, CA_LEAVE_VEHICLES, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[1].pedIndex, CA_USE_VEHICLE, FALSE)
					ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 25.0
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEnemyChase[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 25.0
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[1].pedIndex, CA_LEAVE_VEHICLES, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sEnemyChase[1].pedIndex, CA_USE_VEHICLE, TRUE)
						
						bCloseCombat = FALSE
					ENDIF
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
						IF NOT IS_ENTITY_DEAD(vehEnemyChase[0])
							IF IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex)
							AND NOT IS_PED_GETTING_INTO_A_VEHICLE(sEnemyChase[0].pedIndex)
							AND (IS_PED_INJURED(sEnemyChase[1].pedIndex)
							OR (NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
							AND IS_PED_IN_ANY_VEHICLE(sEnemyChase[1].pedIndex)
							AND NOT IS_PED_GETTING_INTO_A_VEHICLE(sEnemyChase[1].pedIndex)))
								VECTOR vClosestRoad
								FLOAT fClosestRoad
								INT iClosestRoad
								
								GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1, vClosestRoad, fClosestRoad, iClosestRoad, NF_NONE)
								
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								AND IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
								AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
								AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
								AND NOT IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								AND GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 5.0
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCurrentPoint) > 30.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vClosestRoad) < (5.0 + iClosestRoad)
									vChasePoint = vCurrentPoint
									fChasePoint = fCurrentPoint
									
									vCurrentPoint = GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									fCurrentPoint = GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								ENDIF
								
								IF IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex)
									IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									AND IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
									OR IS_ENTITY_IN_WATER(PLAYER_PED_ID())
										IF GET_SCRIPT_TASK_STATUS(sEnemyChase[0].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[0].pedIndex, 500.0)
										ENDIF
									ELSE
										IF GET_SCRIPT_TASK_STATUS(sEnemyChase[0].pedIndex, SCRIPT_TASK_VEHICLE_CHASE) != PERFORMING_TASK
											TASK_VEHICLE_CHASE(sEnemyChase[0].pedIndex, PLAYER_PED_ID())
										ENDIF
									ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
									IF GET_SCRIPT_TASK_STATUS(sEnemyChase[1].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[1].pedIndex, 500.0)
									ENDIF
								ENDIF
								
								IF IS_ENTITY_ON_SCREEN(vehEnemyChase[0])
								OR IS_SPHERE_VISIBLE(vChasePoint, 5.0)
									iChaseTimer[CHASE_TIMER_CAR] = GET_GAME_TIMER() + 3000
								ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_CAR]
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehEnemyChase[0])) > 40.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vChasePoint) > 20.0 - (GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) / 5.0)
									IF IS_VEHICLE_ON_ALL_WHEELS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
									AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
									AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
										IF NOT IS_VECTOR_ZERO(vChasePoint)
											CLEAR_AREA(vChasePoint, 3.0, TRUE)
											
											SET_VEHICLE_POSITION(vehEnemyChase[0], vChasePoint, fChasePoint)
											
											SET_VEHICLE_FORWARD_SPEED(vehEnemyChase[0], 10.0)
											
											SET_VEHICLE_ENGINE_ON(vehEnemyChase[0], TRUE, TRUE)
											
											iChaseTimer[CHASE_TIMER_CAR] = GET_GAME_TIMER() + (6000 - CLAMP_INT(ROUND(GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) * GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))), 0, 3000))
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT bCloseCombat
									IF NOT IS_PED_IN_VEHICLE(sEnemyChase[0].pedIndex, vehEnemyChase[0])
									AND NOT IS_PED_GETTING_INTO_A_VEHICLE(sEnemyChase[0].pedIndex)
									AND IS_VEHICLE_SEAT_FREE(vehEnemyChase[0], VS_DRIVER)
										IF (IS_ENTITY_ON_SCREEN(sEnemyChase[0].pedIndex)
										AND NOT IS_ENTITY_OCCLUDED(sEnemyChase[0].pedIndex))
										OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), 1.0)
											IF GET_SCRIPT_TASK_STATUS(sEnemyChase[0].pedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
												TASK_ENTER_VEHICLE(sEnemyChase[0].pedIndex, vehEnemyChase[0], DEFAULT_TIME_NEVER_WARP)
											ENDIF
											
											iChaseTimer[CHASE_TIMER_ENEMY_1] = GET_GAME_TIMER() + 5000
										ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_ENEMY_1]
										AND (NOT IS_ENTITY_ON_SCREEN(vehEnemyChase[0])
										AND IS_ENTITY_OCCLUDED(vehEnemyChase[0])
										AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehEnemyChase[0]), 2.0))
											SET_PED_INTO_VEHICLE(sEnemyChase[0].pedIndex, vehEnemyChase[0])
										ENDIF
									ENDIF
									
									IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
										IF NOT IS_PED_IN_VEHICLE(sEnemyChase[1].pedIndex, vehEnemyChase[0])
										AND NOT IS_PED_GETTING_INTO_A_VEHICLE(sEnemyChase[1].pedIndex)
										AND IS_VEHICLE_SEAT_FREE(vehEnemyChase[0], VS_FRONT_RIGHT)
											IF (IS_ENTITY_ON_SCREEN(sEnemyChase[1].pedIndex)
											AND NOT IS_ENTITY_OCCLUDED(sEnemyChase[1].pedIndex))
											OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), 1.0)
												IF GET_SCRIPT_TASK_STATUS(sEnemyChase[1].pedIndex, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
													TASK_ENTER_VEHICLE(sEnemyChase[1].pedIndex, vehEnemyChase[0], DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT)
												ENDIF
												
												iChaseTimer[CHASE_TIMER_ENEMY_2] = GET_GAME_TIMER() + 5000
											ELIF GET_GAME_TIMER() > iChaseTimer[CHASE_TIMER_ENEMY_2]
											AND (NOT IS_ENTITY_ON_SCREEN(vehEnemyChase[0])
											AND IS_ENTITY_OCCLUDED(vehEnemyChase[0])
											AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehEnemyChase[0]), 2.0))
												SET_PED_INTO_VEHICLE(sEnemyChase[1].pedIndex, vehEnemyChase[0], VS_FRONT_RIGHT)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(sEnemyChase[0].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[0].pedIndex, 500.0)
									ENDIF
									
									IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
										IF GET_SCRIPT_TASK_STATUS(sEnemyChase[1].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[1].pedIndex, 500.0)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(sEnemyChase[0].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[0].pedIndex, 500.0)
							ENDIF
							
							IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
								IF GET_SCRIPT_TASK_STATUS(sEnemyChase[1].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[1].pedIndex, 500.0)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
							IF GET_SCRIPT_TASK_STATUS(sEnemyChase[1].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[1].pedIndex, 500.0)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
						IF GET_SCRIPT_TASK_STATUS(sEnemyChase[0].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[0].pedIndex, 500.0)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
						IF GET_SCRIPT_TASK_STATUS(sEnemyChase[1].pedIndex, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sEnemyChase[1].pedIndex, 500.0)
						ENDIF
					ENDIF
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_LABEL_BEEN_TRIGGERED("MCH2_DRV2")
					AND HAS_LABEL_BEEN_TRIGGERED("MCH2_LOSE")
						IF GET_GAME_TIMER() > iDialogueTimer[0]
							IF iDialogueLineCount[0] = -1 
								 iDialogueLineCount[0] = 4
							ELIF iDialogueLineCount[1] = -1 
								 iDialogueLineCount[1] = 4
							ELIF iDialogueLineCount[2] = -1 
								iDialogueLineCount[2] = 4
							ELIF iDialogueLineCount[3] = -1 
								iDialogueLineCount[3] = 4
							ELIF iDialogueLineCount[4] = -1 
								iDialogueLineCount[4] = 5
							ENDIF
							
							IF iDialogueLineCount[iDialogueStage] > 0
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) < 25.0
								AND ((DOES_ENTITY_EXIST(sEnemyChase[0].pedIndex)
								AND NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[0].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
								OR (DOES_ENTITY_EXIST(sEnemyChase[1].pedIndex)
								AND NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[1].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
								OR (DOES_ENTITY_EXIST(vehEnemyChase[0])
								AND NOT IS_ENTITY_DEAD(vehEnemyChase[0])
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEnemyChase[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 150.0))
									IF iDialogueStage = 0
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_MICHAEL))
											IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_MICHAEL)), VS_DRIVER) = PLAYER_PED(CHAR_MICHAEL)
												CREATE_CONVERSATION_ADV("MCH2_TRY", CONV_PRIORITY_MEDIUM, FALSE)
											ELSE
												CREATE_CONVERSATION_ADV("MCH2_DRV2M", CONV_PRIORITY_MEDIUM, FALSE)
											ENDIF
											
											iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueLineCount[iDialogueStage]--
										ENDIF
									ELIF iDialogueStage = 1
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED(CHAR_FRANKLIN))
											IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_FRANKLIN)), VS_DRIVER) = PLAYER_PED(CHAR_FRANKLIN)
												CREATE_CONVERSATION_ADV("MCH2_DRV2F", CONV_PRIORITY_MEDIUM, FALSE)
											ELSE
												CREATE_CONVERSATION_ADV("MCH2_GOGO", CONV_PRIORITY_MEDIUM, FALSE)
											ENDIF
											
											iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueLineCount[iDialogueStage]--
										ENDIF
									ELIF iDialogueStage = 2
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND DOES_ENTITY_EXIST(sEnemyChase[0].pedIndex)
										AND NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
										AND IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex)
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, sEnemyChase[0].pedIndex, "MCH2CHIN6")
											
											CREATE_CONVERSATION_ADV("MCH2_CHASE", CONV_PRIORITY_MEDIUM, FALSE)
											
											iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueLineCount[iDialogueStage]--
										ENDIF
									ELIF iDialogueStage = 3
										IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND DOES_ENTITY_EXIST(sEnemyChase[1].pedIndex)
										AND NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
										AND IS_PED_IN_ANY_VEHICLE(sEnemyChase[1].pedIndex)
											ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, sEnemyChase[1].pedIndex, "MCH2CHIN6")
											
											CREATE_CONVERSATION_ADV("MCH2_CHASE2", CONV_PRIORITY_MEDIUM, FALSE)
											
											iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueLineCount[iDialogueStage]--
										ENDIF
									ELIF iDialogueStage = 4
										CREATE_CONVERSATION_ADV("MCH2_TRIADS", CONV_PRIORITY_MEDIUM, FALSE)
										
										iDialogueTimer[0] = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueLineCount[iDialogueStage]--
									ENDIF
								ENDIF
								
								IF iDialogueStage < 4
									iDialogueStage++
								ELSE
									iDialogueStage = 0
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
		AND NOT IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex)
			SAFE_ADD_BLIP_PED(sEnemyChase[0].blipIndex, sEnemyChase[0].pedIndex)
		ENDIF
		
		IF NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
		AND NOT IS_PED_IN_ANY_VEHICLE(sEnemyChase[1].pedIndex)
			SAFE_ADD_BLIP_PED(sEnemyChase[1].blipIndex, sEnemyChase[1].pedIndex)
		ENDIF
		
		IF IS_PED_INJURED(sEnemyChase[0].pedIndex)
		OR IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex)
			SAFE_REMOVE_BLIP(sEnemyChase[0].blipIndex)
		ENDIF
		
		IF IS_PED_INJURED(sEnemyChase[1].pedIndex)
		OR IS_PED_IN_ANY_VEHICLE(sEnemyChase[1].pedIndex)
			SAFE_REMOVE_BLIP(sEnemyChase[1].blipIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehEnemyChase[0])
		AND NOT IS_ENTITY_DEAD(vehEnemyChase[0])
			IF IS_PAUSE_MENU_ACTIVE()
				IF DOES_BLIP_EXIST(blipEnemyVan[0])
					SET_BLIP_COORDS(blipEnemyVan[0], GET_ENTITY_COORDS(vehEnemyChase[0]))
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(sEnemyChase[0].pedIndex)
			AND IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex)
			OR NOT IS_PED_INJURED(sEnemyChase[1].pedIndex)
			AND IS_PED_IN_ANY_VEHICLE(sEnemyChase[1].pedIndex)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEnemyChase[0], "MI_2_ENEMY_CAR")
				
				IF NOT DOES_BLIP_EXIST(blipEnemyVan[0])
					blipEnemyVan[0] = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehEnemyChase[0]))
					SET_BLIP_COLOUR(blipEnemyVan[0], BLIP_COLOUR_RED)
					SET_BLIP_PRIORITY(blipEnemyVan[0], BLIPPRIORITY_HIGHEST)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipEnemyVan[0], "MCH2_BLIPVEH")
				ENDIF
				
				VECTOR vEnemyChase = GET_ENTITY_COORDS(vehEnemyChase[0])
				VECTOR vBlipChase = GET_BLIP_COORDS(blipEnemyVan[0])
				
				FLOAT fModifier = 1.0 + CLAMP((GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehEnemyChase[0])) / 4), 0.0, 15.0)
				
				vBlipChase.X = vBlipChase.X +@ (((vEnemyChase.X - vBlipChase.X) / fModifier) * 15)
				vBlipChase.Y = vBlipChase.Y +@ (((vEnemyChase.Y - vBlipChase.Y) / fModifier) * 15)
				vBlipChase.Z = vBlipChase.Z +@ (((vEnemyChase.Z - vBlipChase.Z) / fModifier) * 15)
				
				SET_BLIP_COORDS(blipEnemyVan[0], vBlipChase)
			ELSE
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEnemyChase[0])
			ENDIF
		ENDIF
		
		IF (IS_PED_INJURED(sEnemyChase[0].pedIndex)
		OR NOT IS_PED_IN_ANY_VEHICLE(sEnemyChase[0].pedIndex))
		AND (IS_PED_INJURED(sEnemyChase[1].pedIndex)	
		OR NOT IS_PED_IN_ANY_VEHICLE(sEnemyChase[1].pedIndex))
			SAFE_REMOVE_BLIP(blipEnemyVan[0])
		ENDIF
		
		//Fail
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) > 250.0
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				eMissionFail = failFranklinAbandon
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				eMissionFail = failMichaelAbandon
			ENDIF
			
			missionFailed()
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			//Audio Scene
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_IN_CAR")
				START_AUDIO_SCENE("MI_2_ESCAPE_IN_CAR")
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(blipMichael)
		OR DOES_BLIP_EXIST(blipFranklin)
			VEHICLE_INDEX vehCurrent
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehCurrent = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehCurrent)
			AND NOT IS_ENTITY_DEAD(vehCurrent)
			AND IS_PED_IN_VEHICLE(NOT_PLAYER_PED_ID(), vehCurrent)
				SAFE_REMOVE_BLIP(blipMichael)
				SAFE_REMOVE_BLIP(blipFranklin)
				
				SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_MICHAEL), FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_FRANKLIN), FALSE)
			ENDIF
		ENDIF
		
		IF iCutsceneStage > 0
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND IS_PED_IN_VEHICLE(NOT_PLAYER_PED_ID(), GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
							CREATE_CONVERSATION_ADV("MCH2_WHY")
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("InCarTogether", TRUE)
					ELIF HAS_LABEL_BEEN_TRIGGERED("InCarTogether")
						CREATE_CONVERSATION_ADV("MCH2_WHY2")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED(CHAR_MICHAEL))
			SAFE_REMOVE_BLIP(blipMichael)
			SAFE_REMOVE_BLIP(blipFranklin)
			
			STRING sCommonLeave
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				sCommonLeave = "CMN_FLEAVE"
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				sCommonLeave = "CMN_MLEAVE"
			ENDIF
			
			VECTOR vLocSize
			
			vLocSize = <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_HEIGHT>>
			
			#IF IS_DEBUG_BUILD	DONT_DO_J_SKIP(sLocatesData)	#ENDIF
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, <<-858.0734, 157.1951, 63.7158>>, vLocSize, TRUE, NOT_PLAYER_PED_ID(), "MCH2_GMCSH", sCommonLeave, FALSE, TRUE)
			OR iCutsceneStage > 2
				SET_LABEL_AS_TRIGGERED("MCH2_GMCSH", TRUE)
				
				CLEAR_PED_TASKS(NOT_PLAYER_PED_ID())
				
				//Audio
				PLAY_AUDIO(MIC2_TRIADS_LOST)
				
				ADVANCE_STAGE()
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_ESCAPE_IN_CAR")
			STOP_AUDIO_SCENE("MI_2_ESCAPE_IN_CAR")
		ENDIF
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, MIC2_TIME_TO_LOSE_TRIADS)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_MICHAEL), FALSE)
		SET_PED_USING_ACTION_MODE(PLAYER_PED(CHAR_FRANKLIN), FALSE)
		
//		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
//			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
//			
//			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
//			ENDIF
//		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(sEnemyChase) i
			IF DOES_ENTITY_EXIST(sEnemyChase[i].pedIndex)
				IF NOT IS_PED_INJURED(sEnemyChase[i].pedIndex)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sEnemyChase[i].pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 250.0
					SAFE_DELETE_PED(sEnemyChase[i].pedIndex)
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(sEnemyChase[i].pedIndex)
				ENDIF
				
				SAFE_REMOVE_BLIP(sEnemyChase[i].blipIndex)
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehEnemyChase) i
			IF DOES_ENTITY_EXIST(vehEnemyChase[i])
				IF NOT IS_ENTITY_DEAD(vehEnemyChase[i])
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehEnemyChase[i]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 250.0
					SAFE_DELETE_VEHICLE(vehEnemyChase[i])
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEnemyChase[i])
				ENDIF
				
				SAFE_REMOVE_BLIP(blipEnemyVan[i])
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(objCow)
			SET_OBJECT_AS_NO_LONGER_NEEDED(objCow)
		ENDIF
		
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehHachiRoku)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEscape)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(G_M_M_CHIGOON_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(G_M_M_CHIGOON_02)
		SET_MODEL_AS_NO_LONGER_NEEDED(BISON)
		SET_MODEL_AS_NO_LONGER_NEEDED(FELTZER2)
		SET_MODEL_AS_NO_LONGER_NEEDED(COQUETTE)
		
		REMOVE_CLIP_SET("MOVE_STRAFE@COP")
		REMOVE_CLIP_SET("move_ped_strafing")
		
		//SET_ANIM_DICT_AS_LOADED(sAnimDictMic2, TRUE)	//Odd fix - something to do with clipsets and anim dicts removing together?
		
		SAFE_REMOVE_BLIP(blipMichael)
		SAFE_REMOVE_BLIP(blipFranklin)
		
		//Taxi
		DISABLE_TAXI_HAILING(FALSE)
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC backToMichaels()
	IF INIT_STAGE()
		//Wanted
		SET_MAX_WANTED_LEVEL(6)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		
		//Ambulances etc.
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_GANGS, TRUE)
		
		IF NOT CAN_CREATE_RANDOM_COPS()
			SET_CREATE_RANDOM_COPS(TRUE)
		ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_HOME")
			START_AUDIO_SCENE("MI_2_DRIVE_HOME")
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
		//Audio
		LOAD_AUDIO(MIC2_RADIO_SETUP)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_FRANKLIN), FALSE)
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_MICHAEL), <<961.2763, -2106.1240, 30.5098>>, 89.0595, FALSE)
			
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<961.0361, -2104.6360, 30.5076>>, 100.5089)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<961.2763, -2106.1240, 30.5098>>)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		SWITCH iCutsceneStage
			CASE 0
				//Cutscene
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-858.0734, 157.1951, 63.7158>>) < DEFAULT_CUTSCENE_LOAD_DIST / 4.0
					REQUEST_CUTSCENE("mic_2_mcs_3_concat")
					
					//Request Cutscene Variations - mic_2_mcs_3_concat
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
					ENDIF
				ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-858.0734, 157.1951, 63.7158>>) > DEFAULT_CUTSCENE_UNLOAD_DIST / 4.0
					IF HAS_CUTSCENE_LOADED()
						REMOVE_CUTSCENE()
					ENDIF
				ENDIF
				
				VECTOR vLocSize
				
				vLocSize = <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_HEIGHT>>
				
				STRING sCommonLeave
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					sCommonLeave = "CMN_FLEAVE"
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					sCommonLeave = "CMN_MLEAVE"
				ENDIF
				
				VEHICLE_INDEX vehArrive
				
				#IF IS_DEBUG_BUILD	DONT_DO_J_SKIP(sLocatesData)	#ENDIF
				IF NOT HAS_LABEL_BEEN_TRIGGERED("OutroCutArrive")
					SAFE_REMOVE_BLIP(blipMichael)
					SAFE_REMOVE_BLIP(blipFranklin)
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_GMCSH")
						IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, <<-858.0734, 157.1951, 63.7158>>, vLocSize, TRUE, NOT_PLAYER_PED_ID(), "MCH2_GMCSH", sCommonLeave, FALSE, TRUE)
							IF CAN_PLAYER_START_CUTSCENE()
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehArrive = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								ENDIF
								
								IF DOES_ENTITY_EXIST(vehArrive)
								AND NOT IS_ENTITY_DEAD(vehArrive)
									SET_LABEL_AS_TRIGGERED("OutroCutArrive", TRUE)
								ELSE
									ADVANCE_STAGE()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(sLocatesData, <<-858.0734, 157.1951, 63.7158>>, vLocSize, TRUE, NOT_PLAYER_PED_ID(), "", sCommonLeave, FALSE, TRUE)
							IF CAN_PLAYER_START_CUTSCENE()
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehArrive = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								ENDIF
								
								IF DOES_ENTITY_EXIST(vehArrive)
								AND NOT IS_ENTITY_DEAD(vehArrive)
									SET_LABEL_AS_TRIGGERED("OutroCutArrive", TRUE)
								ELSE
									ADVANCE_STAGE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("OutroCutArrive")
					//B* 2135862: Force multihead blinders on early
					SET_MULTIHEAD_SAFE(TRUE)
				
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						vehArrive = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehArrive)
						IF IS_VEHICLE_ON_ALL_WHEELS(vehArrive)
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehArrive, DEFAULT, DEFAULT, DEFAULT, TRUE)
						ENDIF
						
						IF IS_VEHICLE_STOPPED(vehArrive)
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 400, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(NOT_PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								TASK_LEAVE_ANY_VEHICLE(NOT_PLAYER_PED_ID(), 800, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
							ENDIF
						ENDIF
					ENDIF
					
					IF (GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = NULL 
					OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_SEAT_PED_IS_IN(PLAYER_PED_ID()))))
					AND (GET_VEHICLE_PED_IS_USING(NOT_PLAYER_PED_ID()) = NULL
					OR (IS_PED_IN_ANY_VEHICLE(NOT_PLAYER_PED_ID()) AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(NOT_PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(NOT_PLAYER_PED_ID()), GET_SEAT_PED_IS_IN(NOT_PLAYER_PED_ID()))))
						ADVANCE_STAGE()
					ENDIF
				ENDIF
				
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-858.0734, 157.1951, 63.7158>>) >= 200.0
						IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() % 2 = 0											
							CREATE_CONVERSATION_ADV("MCH2_DRV3")
						ELSE
							CREATE_CONVERSATION_ADV("MCH2_BANTER")
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("MCH2_DRV3", TRUE)
						SET_LABEL_AS_TRIGGERED("MCH2_BANTER", TRUE)
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("ItsOver")
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							PLAY_AUDIO(MIC2_RADIO_SETUP)
						ELSE						
							PLAY_AUDIO(MIC2_OVER)
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("ItsOver", TRUE)
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("MCH2_DRV3")
				OR HAS_LABEL_BEEN_TRIGGERED("MCH2_BANTER")
					IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_DRV3")
					OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("MCH2_BANTER")
						IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							//IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							//ENDIF
						ELSE
							//IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), GET_ENTITY_COORDS(PLAYER_PED(CHAR_FRANKLIN))) > 250.0
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						eMissionFail = failFranklinAbandon
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						eMissionFail = failMichaelAbandon
					ENDIF
					
					missionFailed()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_HOME")
			STOP_AUDIO_SCENE("MI_2_DRIVE_HOME")
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC cutEnd()
	IF INIT_STAGE()
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		bRadar = FALSE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Roads
		SET_ROADS_IN_AREA(<<-877.67, 224.67, 72.30>>, <<-853.08, 125.01, 56.63>>, FALSE)
		
		//Cutscene
		REQUEST_CUTSCENE("mic_2_mcs_3_concat")
		
		//Interior
		intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
		
		PIN_INTERIOR_IN_MEMORY(intMansion)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED(CHAR_MICHAEL), <<-860.1198, 157.7610, 63.9212>>, 339.3617, FALSE)
			
			SET_PED_POSITION(PLAYER_PED(CHAR_FRANKLIN), <<-859.0430, 160.0430, 64.3859>>, 156.8454, FALSE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<-860.1198, 157.7610, 63.9212>>, 20.0)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
		ENDIF
	ELSE
		//Request Cutscene Variations - mic_2_mcs_3_concat
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
				OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("mic_2_mcs_3_concat")
						CLEAR_TEXT()
						
//						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
//							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
//							
//							SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						ENDIF
//						
//						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
							
							SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						ENDIF
						
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
//						vehEnd = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
//						
//						IF DOES_ENTITY_EXIST(vehEnd)
//						AND NOT IS_ENTITY_DEAD(vehEnd)
//							SET_ENTITY_AS_MISSION_ENTITY(vehEnd)
//							
//							SET_VEHICLE_ENGINE_ON(vehEnd, FALSE, TRUE)
//							SET_VEHICLE_RADIO_ENABLED(vehEnd, FALSE)
//							
//							REGISTER_ENTITY_FOR_CUTSCENE(vehEnd, "MIC2_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//						ENDIF
						
//						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MIC2_Car", CU_DONT_ANIMATE_ENTITY, COQUETTE)
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						START_CUTSCENE()
						
						WAIT_WITH_DEATH_CHECKS(0)
						
						CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED(CHAR_MICHAEL))
						
						APPLY_PED_DAMAGE_PACK(PLAYER_PED(CHAR_MICHAEL), "SCR_Torture", 0.0, 1.0)
						
						CLEAR_AREA(<<-857.7942, 159.1706, 64.1449>>, 100.0, TRUE)
						
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 1
				//Remove Peds and Cars from the cutscene
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MichaelInjuries")
					ENTITY_INDEX entityMichael
					entityMichael = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Michael", PLAYER_ZERO)
					
					IF DOES_ENTITY_EXIST(entityMichael)
					AND NOT IS_ENTITY_DEAD(entityMichael)
						APPLY_PED_DAMAGE_PACK(PLAYER_PED(CHAR_MICHAEL), "SCR_Torture", 0.0, 1.0)
						
						SET_LABEL_AS_TRIGGERED("MichaelInjuries", TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("FadeOutEnd")
					IF GET_CUTSCENE_TIME() > ROUND(116.000000 * 1000.0)
						IF NOT IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_OUT(2000)
						ENDIF
						
						SET_LABEL_AS_TRIGGERED("FadeOutEnd", TRUE)
					ENDIF
					
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MIC2_Car")
					IF DOES_ENTITY_EXIST(vehEnd)
					AND NOT IS_ENTITY_DEAD(vehEnd)
						SET_VEHICLE_POSITION(vehEnd, <<-858.4802, 154.9797, 63.5643>>, -5.3912)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehEnd)
						ACTIVATE_PHYSICS(vehEnd)
						SET_VEHICLE_RADIO_ENABLED(vehEnd, TRUE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					REPLAY_STOP_EVENT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)	//SET_GAMEPLAY_CAM_RELATIVE_HEADING(64.7 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//					IF DOES_ENTITY_EXIST(vehEnd)
//					AND NOT IS_ENTITY_DEAD(vehEnd)
//						SET_PED_POSITION(PLAYER_PED_ID(), <<-860.5838, 153.3469, 62.9417>>, 359.1850, FALSE)
//						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehEnd, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK)
//						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					ELSE
//						SET_PED_POSITION(PLAYER_PED_ID(), <<-854.4845, 147.4481, 61.7792>>, 180.0839, FALSE)
//						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 750, 177.1006, FALSE)
//						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneWashFace)
						IF NOT DOES_CAM_EXIST(camAnim)
							camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						ENDIF
						
						sceneWashFace = CREATE_SYNCHRONIZED_SCENE(sceneWashFacePos, sceneWashFaceRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_MICHAEL), sceneWashFace, sAnimDictMic2WashFace, "michael_washing_face", INSTANT_BLEND_IN, WALK_BLEND_OUT)
						PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneWashFace, "michael_washing_face_cam", sAnimDictMic2WashFace)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneWashFace, 0.250)
						SET_SYNCHRONIZED_SCENE_RATE(sceneWashFace, 0.0)
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					LOAD_SCENE_ADV(<<-803.2339, 169.1168, 75.7405>>, 20.0)
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneWashFace)
						IF NOT DOES_CAM_EXIST(camAnim)
							camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						ENDIF
						
						sceneWashFace = CREATE_SYNCHRONIZED_SCENE(sceneWashFacePos, sceneWashFaceRot)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED(CHAR_MICHAEL), sceneWashFace, sAnimDictMic2WashFace, "michael_washing_face", INSTANT_BLEND_IN, WALK_BLEND_OUT)
						PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneWashFace, "michael_washing_face_cam", sAnimDictMic2WashFace)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneWashFace, 0.250)
						SET_SYNCHRONIZED_SCENE_RATE(sceneWashFace, 0.0)
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					//Clear Blood
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED(CHAR_MICHAEL))
					
					//Restore previous clothes
					//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED(CHAR_MICHAEL))
					
					WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_NAVY_SUIT, FALSE)
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
					
					HIDE_LOADING_ON_FADE_THIS_FRAME()
					
					iAdvanceTimer = GET_GAME_TIMER() + 30000
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				HIDE_LOADING_ON_FADE_THIS_FRAME()
				
				REQUEST_SCRIPT_AUDIO_BANK("FAM5_WASH_FACE")
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneWashFace)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL)))
				OR GET_GAME_TIMER() > iAdvanceTimer
				OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) < 1
					IF (g_bResultScreenDisplaying)
					OR GET_GAME_TIMER() > iAdvanceTimer
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) < 1
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 3
				HIDE_LOADING_ON_FADE_THIS_FRAME()
				
				IF (REQUEST_SCRIPT_AUDIO_BANK("FAM5_WASH_FACE")
				AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneWashFace)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL)))
				OR GET_GAME_TIMER() > iAdvanceTimer
				OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) < 1
					IF (NOT g_bResultScreenDisplaying)
					OR GET_GAME_TIMER() > iAdvanceTimer
					OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) < 1
						WAIT_WITH_DEATH_CHECKS(500)
						
						SET_CLOCK_TIME(11, 0, 0)
						
//						IF NOT DOES_CAM_EXIST(camMain)
//							camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//						ENDIF
//						
//						SET_CAM_PARAMS(camMain, 
//										<<-804.204712, 168.047638, 76.793808>>,
//										<<5.523620, 0.0, -6.194664>>,
//										45.0)
//										
//						SET_CAM_PARAMS(camMain, 
//										<<-804.204712, 168.047638, 76.793808>>,
//										<<6.774823, 0.0, -8.368400>>,
//										45.0,
//										5000)
//						
//						SET_CAM_ACTIVE(camMain, TRUE)
//						
//						STOP_CAM_SHAKING(camMain, TRUE)
						
						REQUEST_PTFX_ASSET()
						
						WHILE NOT HAS_PTFX_ASSET_LOADED()
							WAIT(0)
							
							PRINTSTRING("LOADING PARTICLES")
							PRINTNL()
						ENDWHILE
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneWashFace, 0.250)
						SET_SYNCHRONIZED_SCENE_RATE(sceneWashFace, 1.0)
						
						START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_pts_headsplash", PLAYER_PED_ID(), VECTOR_ZERO, VECTOR_ZERO, BONETAG_PH_L_HAND)
						
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(2000)
						ENDIF
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneWashFace)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneWashFace) >= 0.875)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneWashFace)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 21.0, FALSE)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 5
				IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		//SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		
		SAFE_REMOVE_BLIP(blipDestination)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

//══════════════════════════════════╡ DEBUG ╞════════════════════════════════════

#IF IS_DEBUG_BUILD

PROC debugRoutine()
	IF bAutoSkipping = TRUE
		IF ENUM_TO_INT(eMissionObjective) >= ENUM_TO_INT(eMissionObjectiveAutoJSkip)
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//				
//				WHILE IS_SCREEN_FADING_IN()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
			
			eMissionObjectiveAutoJSkip = initMission
			
			bAutoSkipping = FALSE
			bSkipped = TRUE
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		missionPassed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		missionFailed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	OR ENUM_TO_INT(eMissionObjective) < ENUM_TO_INT(eMissionObjectiveAutoJSkip)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bSkipped = TRUE
		ENDIF
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		ADVANCE_STAGE()
		
		TEXT_LABEL txtLabel
		
		txtLabel = "Stage: "
		txtLabel += ENUM_TO_INT(eMissionObjective)
		
		PRINTSTRING(txtLabel)
		PRINTNL()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
	OR LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iStageSkipMenu, ENUM_TO_INT(eMissionObjective))
		IF bAutoSkipping = FALSE
			bAutoSkipping = TRUE
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				eMissionObjectiveAutoJSkip = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) - 1)
			ELSE
				eMissionObjectiveAutoJSkip = INT_TO_ENUM(MissionObjective, iStageSkipMenu)
			ENDIF
			
			eMissionObjective = initMission
			
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
			//Cutscene
			WHILE NOT HAS_CUTSCENE_FINISHED()
				STOP_CUTSCENE(TRUE)
				
				PRINTLN("STOPPING CUTSCENE...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			REMOVE_CUTSCENE()
			
			WHILE HAS_CUTSCENE_LOADED()
				PRINTLN("REMOVING CUTSCENE...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			MISSION_CLEANUP(TRUE)
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10000.0, TRUE)
		ENDIF
	ENDIF
ENDPROC 

#ENDIF

//══════════════════════════════╡ MISSION SCRIPT ╞═══════════════════════════════

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		IF DOES_ENTITY_EXIST(NOT_PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(NOT_PLAYER_PED_ID())
			IF IS_ENTITY_ATTACHED(NOT_PLAYER_PED_ID())
				g_bDontLetBuddiesReactToNextPlayerDeath = TRUE
			ENDIF
			SET_PED_KEEP_TASK(NOT_PLAYER_PED_ID(), TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objHook)
			IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
			AND IS_PED_INJURED(PLAYER_PED(CHAR_MICHAEL))
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL), FALSE), GET_ENTITY_COORDS(objHook)) < 5.0
			AND ARE_COORDS_IN_3D_AREA(GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL), FALSE), <<994.382935, -2150.192871, 31.976345>> - <<1.0, 1.0, 2.5>>, <<994.382935, -2150.192871, 31.976345>> + <<1.0, 1.0, 2.5>>)
				//Pin the ankle to the bottom of the hook
				VECTOR vRightFootAttach
				vRightFootAttach = <<-0.075, -0.1, -0.85>>
				ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_R_FOOT), -1, vRightFootAttach, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
				ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(PLAYER_PED(CHAR_MICHAEL), objHook, GET_PED_BONE_INDEX(PLAYER_PED(CHAR_MICHAEL), BONETAG_L_FOOT), -1, <<-vRightFootAttach.X, vRightFootAttach.Y, vRightFootAttach.Z>>, VECTOR_ZERO, VECTOR_ZERO, -1.0, TRUE, FALSE, FALSE, FALSE)
			ENDIF
		ENDIF
		
		Mission_Flow_Mission_Force_Cleanup()
		
		enumCharacterList eCharFail = GET_CURRENT_PLAYER_PED_ENUM()
		STORE_FAIL_WEAPON(PLAYER_PED_ID(), ENUM_TO_INT(eCharFail))
		
		//Because Michael is restricted you are switched to Franklin, so we need to check if he's in the building
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<967.567566, -2195.217529, 25.539518>>, <<972.950378, -2098.676270, 49.888527>>, 70.0)
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<933.0538, -2162.6216, 29.5012>>, 96.4993)
			SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<929.5760, -2162.3455, 29.3527>>, 175.2960)
		ENDIF
		
		SET_MICHAEL_UNAVAILABLE_ON_FAIL()
		
		MISSION_CLEANUP(FALSE, TRUE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
//═══════════════════════════════╡ MISSION LOOP ╞════════════════════════════════
	
	#IF IS_DEBUG_BUILD
		MissionNames()
	#ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])	//Franklin
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
				//Transfer the global index to a local index and grab ownership of the entity.
				sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = g_sTriggerSceneAssets.ped[3]
				SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])	//Denise
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedDenise = g_sTriggerSceneAssets.ped[0]
			SET_ENTITY_AS_MISSION_ENTITY(pedDenise, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])	//Friend 1
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedFriend1 = g_sTriggerSceneAssets.ped[1]
			SET_ENTITY_AS_MISSION_ENTITY(pedFriend1, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])	//Friend 2
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedFriend2 = g_sTriggerSceneAssets.ped[2]
			SET_ENTITY_AS_MISSION_ENTITY(pedFriend2, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])	//Animated Door
		//Transfer the global index to a local index and grab ownership of the entity.
		objDoorAnim = g_sTriggerSceneAssets.object[0]
		SET_ENTITY_AS_MISSION_ENTITY(objDoorAnim, TRUE, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])	//Real Door
		//Transfer the global index to a local index and grab ownership of the entity.
		objDoorReal = g_sTriggerSceneAssets.object[1]
		SET_ENTITY_AS_MISSION_ENTITY(objDoorReal, TRUE, TRUE)
	ENDIF
	
	IF NOT IS_REPLAY_IN_PROGRESS()
		//Safely reposition players vehicle
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID())	!= CHAR_TREVOR
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != NULL
						vehFranklin = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())	PRINTLN("vehIndex = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())")
					ELIF GET_PLAYERS_LAST_VEHICLE() != NULL
						vehFranklin = GET_PLAYERS_LAST_VEHICLE()	PRINTLN("vehIndex = GET_PLAYERS_LAST_VEHICLE()")
						IF IS_ENTITY_DEAD(vehFranklin)
						OR (NOT IS_ENTITY_DEAD(vehFranklin)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehFranklin)) > 50.0)
							vehFranklin = NULL	PRINTLN("vehIndex = NULL")
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehFranklin)
						SET_ENTITY_AS_MISSION_ENTITY(vehFranklin, TRUE, TRUE)	PRINTLN("SET_ENTITY_AS_MISSION_ENTITY(vehIndex)")
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehFranklin, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_THIS_CUTSCENE_LOADED("MIC_2_INT")
			PED_INDEX pedLastPlayer
			pedLastPlayer = PLAYER_PED_ID()
			
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
					
					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(pedLastPlayer)
					
					SET_LABEL_AS_TRIGGERED("SwitchFX[MIC_2_INT]", TRUE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			REGISTER_ENTITY_FOR_CUTSCENE(pedIntro, "Knocked_Out_Ped", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, G_M_Y_FAMCA_01)
			
			IF DOES_ENTITY_EXIST(pedDenise)
				REGISTER_ENTITY_FOR_CUTSCENE(pedDenise, "DENISE", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedFriend1)
				REGISTER_ENTITY_FOR_CUTSCENE(pedFriend1, "Denises_Friend_1", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedFriend2)
				REGISTER_ENTITY_FOR_CUTSCENE(pedFriend2, "Denises_Friend_2", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				ENDIF
			ENDIF
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			START_CUTSCENE()
			
			SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
			
			bPreloaded = TRUE
		ENDIF
	ENDIF
	
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_MICHAEL_2)
	
	PRINTLN("Michael 2 - Mission Launch Check")
	
	WHILE (TRUE)
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FreshMeat")
		
//		#IF IS_DEBUG_BUILD
//			SPLINE_CAM_EDITOR()
//		#ENDIF
		
		INT i
		
		//Fail Checks
		DEATH_CHECKS()
		
		// Temp fix to force the interior to uncap when you're close to it.
		IF bSetUncapped
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<993.818237, -2180.848389, 31.164871>>) < 240.0
				SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, FALSE)
				bSetUncapped = FALSE
			ENDIF
		ENDIF
		
		//Deadly deadly mincers
		IF NOT IS_SCREEN_FADED_OUT()
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<975.882324, -2166.031006, 28.599106>>, <<973.063477, -2165.699951, 30.599342>>, 1.75)
			OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<993.639832, -2161.967773, 29.369600>>, <<991.663147, -2161.787354, 32.325283>>, 2.0)
			AND NOT IS_PED_JUMPING(PLAYER_PED_ID()))
			OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<976.623962, -2119.597168, 30.455158>>, <<976.444641, -2121.593750, 33.455223>>, 2.0)
			AND NOT IS_PED_JUMPING(PLAYER_PED_ID()))
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<998.518921, -2139.156494, 27.753616>>, <<998.213562, -2142.145020, 32.753616>>, 2.0)
				IF NOT GET_PLAYER_INVINCIBLE(PLAYER_ID())
				AND NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
						SET_HIGH_FALL_TASK(PLAYER_PED_ID(), 10000, 10000)
					ENDIF
					
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<998.392700,-2140.970215,30.976324>>, <<10.0, 12.0, 2.5 >>)
						REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Chopped_Up")
					ENDIF
					
					IF (HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece1)
					AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece2))
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<998.518921, -2139.156494, 27.753616>>, <<998.213562, -2142.145020, 32.753616>>, 2.0)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<993.639832, -2161.967773, 29.369600>>, <<991.663147, -2161.787354, 32.325283>>, 2.0)
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_abattoir_ped_minced", <<992.7037, -2161.9229, 31.8097 - 1.0>>, VECTOR_ZERO)
							
							PLAY_SOUND_FROM_COORD(-1, "MINCER_FALL", <<992.7037, -2161.9229, 31.8097 - 1.0>>, "MICHAEL_2_SOUNDS")
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
							
							//Cam
							IF NOT DOES_CAM_EXIST(camMain)
								camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							ENDIF
							
							SET_CAM_PARAMS(camMain, 
											<<990.255493, -2156.313232, 31.377579>>,
											<<-1.060463, 0.0,-153.399750>>,
											17.464592)
							
							SET_CAM_ACTIVE(camMain, TRUE)
							SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 2.0)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							
							TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), sAnimDictMic2SetPiece1, "goonfall_into_grinder", <<990.626, -2161.452, 31.691>>, <<0.0, 0.0, 90.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY | AF_HOLD_LAST_FRAME, 0.215, EULER_YXZ, AIK_DISABLE_LEG_IK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							
							WAIT_WITH_DEATH_CHECKS(1000)
							
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							
							WHILE NOT IS_SCREEN_FADED_OUT()
								WAIT_WITH_DEATH_CHECKS(0)
							ENDWHILE
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							
							SET_PED_POSITION(PLAYER_PED_ID(), <<990.255493, -2156.313232, 31.377579>>, 0.0, FALSE)
						ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<976.623962, -2119.597168, 30.455158>>, <<976.444641, -2121.593750, 33.455223>>, 2.0)
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_abattoir_ped_minced", <<976.72, -2120.27, 31.5>>, VECTOR_ZERO)
							
							PLAY_SOUND_FROM_COORD(-1, "MINCER_FALL", <<976.72, -2120.27, 31.5>>, "MICHAEL_2_SOUNDS")
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
							
							//Cam
							IF NOT DOES_CAM_EXIST(camMain)
								camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							ENDIF
							
							SET_CAM_PARAMS(camMain, 
										<<974.021423, -2116.019531, 32.641281>>,
										<<-3.792372, 0.0, -151.450134>>,
										30.157396)
							
							SET_CAM_ACTIVE(camMain, TRUE)
							SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 2.0)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
							
							TASK_PLAY_ANIM_ADVANCED(PLAYER_PED_ID(), sAnimDictMic2SetPiece2, "goon_fall_onto_conveyor", <<980.074, -2121.410, 29.482>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_FORCE_START | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_IGNORE_GRAVITY | AF_HOLD_LAST_FRAME, 0.559, EULER_YXZ, AIK_DISABLE_LEG_IK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							
							WAIT_WITH_DEATH_CHECKS(1000)
							
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							
							WHILE NOT IS_SCREEN_FADED_OUT()
								WAIT_WITH_DEATH_CHECKS(0)
							ENDWHILE
							
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							
							SET_PED_POSITION(PLAYER_PED_ID(), <<974.021423, -2116.019531, 32.641281>>, 0.0, FALSE)
						ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<998.518921, -2139.156494, 27.753616>>, <<998.213562, -2142.145020, 32.753616>>, 2.0)
							//Cam
							IF NOT DOES_CAM_EXIST(camMain)
								camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							ENDIF
							
							SET_CAM_PARAMS(camMain, 
											<<989.861145, -2144.378174, 31.004501>>,
											<<-3.873163, 0.0, -79.889450>>,
											38.757084, 
											0)
							
							SET_CAM_PARAMS(camMain, 
											<<989.861145, -2144.378174, 31.004501>>,
											<<-3.873163, 0.0, -79.889450>>,
											35.423241, 
											1000,
											GRAPH_TYPE_DECEL,
											GRAPH_TYPE_DECEL)
							
							SHAKE_CAM(camMain, "ROAD_VIBRATION_SHAKE", 1.0)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_abattoir_ped_sliced", PLAYER_PED_ID(), <<0.0, -0.1, -1.0>>, <<0.0, 0.0, 84.0>>, 0.8)
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
							
							IF REQUEST_SCRIPT_AUDIO_BANK("Michael_2_Chopped_Up")
								PLAY_SOUND_FROM_ENTITY(-1, "MIC_2_CHOPPED_UP_JUMPIN_MASTER", PLAYER_PED_ID())
							ENDIF
							
							WAIT_WITH_DEATH_CHECKS(1500)
							
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								eMissionFail = failFranklinDied
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								eMissionFail = failMichaelDied
							ENDIF
							
							missionFailed()
						ENDIF
					ENDIF
					
					SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
				ENDIF
			ENDIF
		ENDIF
		
		//Interior Pinning - Abattoir
		IF intAbattoir = NULL
			intAbattoir = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<990.9518, -2178.6208, 29.0257>>, "v_abattoir")
		ENDIF
		
		IF intAbattoir != NULL
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intAbattoir
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> DORMANT_TASK
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<961.281494, -2217.055420, 24.568760>>, <<974.338867, -2067.088379, 79.930626>>, 120.0)
			IF intAbattoir != NULL
				IF bPinnedAbattoir = FALSE
					PIN_INTERIOR_IN_MEMORY(intAbattoir)
					
					IF NOT IS_INTERIOR_READY(intAbattoir)
						PRINTLN("PINNING INTERIOR...")
					ELSE
						bPinnedAbattoir = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<959.795593, -2254.591309, 24.537045>>, <<979.547913, -2022.923828, 87.146744>>, 150.0)
			IF intAbattoir <> NULL
				IF bPinnedAbattoir = TRUE
					SAFE_DELETE_OBJECT(objCow)	//Fix for bug 2022984
					
					UNPIN_INTERIOR(intAbattoir)
					
					bPinnedAbattoir = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<976.138733, -2084.538330, 29.001110>>, <<966.163025, -2202.844727, 44.551697>>, 100.0)
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iEntranceDoor)
				ADD_DOOR_TO_SYSTEM(iEntranceDoor, V_ILEV_ABBMAINDOOR, <<962.0958, -2183.8306, 31.0619>>)
			ENDIF
			
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(iEntranceDoor)
				IF DOOR_SYSTEM_GET_OPEN_RATIO(iEntranceDoor) <> 1.0
				OR DOOR_SYSTEM_GET_DOOR_STATE(iEntranceDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
					DOOR_SYSTEM_SET_OPEN_RATIO(iEntranceDoor, 1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iEntranceDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			IF DOES_ENTITY_EXIST(objHook)
//				IF NOT (IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED(CHAR_MICHAEL), objHook)
//				AND bReattach = FALSE)
//					IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intAbattoir
//						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
//						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
//						//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_X)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Shutter Door
		IF eMissionObjective >= stageMichaelEscape
		AND eMissionObjective < stageCutEnd
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<962.529602, -2105.850342, 30.0>>, <<50.0, 40.0, 30.0>>)
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
					ADD_DOOR_TO_SYSTEM(iExitDoor, PROP_ABAT_SLIDE, <<962.9084, -2105.8137, 34.6432>>)
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
					IF DOOR_SYSTEM_GET_OPEN_RATIO(iExitDoor) <> 1.0
					OR DOOR_SYSTEM_GET_DOOR_STATE(iExitDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
						DOOR_SYSTEM_SET_OPEN_RATIO(iExitDoor, 1.0, FALSE, FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iExitDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
				ADD_DOOR_TO_SYSTEM(iExitDoor, PROP_ABAT_SLIDE, <<962.9084, -2105.8137, 34.6432>>)
			ENDIF
			
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(iExitDoor)
				IF DOOR_SYSTEM_GET_OPEN_RATIO(iExitDoor) <> 0.0
				OR DOOR_SYSTEM_GET_DOOR_STATE(iExitDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
					DOOR_SYSTEM_SET_OPEN_RATIO(iExitDoor, 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iExitDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		//Animated Machines
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intAbattoir
			IF NOT DOES_ENTITY_EXIST(objGrinder)
				IF bGrabGrinder = FALSE
					IF iDoesObjectOfTypeExistAtCoords = 0
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<993.48, -2161.49, 29.96>>, 2.0, V_ILEV_ABMINCER)
							objGrinder = GET_CLOSEST_OBJECT_OF_TYPE(<<993.48, -2161.49, 29.96>>, 2.0, V_ILEV_ABMINCER)
							
							IF objGrinder != NULL
								bGrabGrinder = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMachine)
					IF NOT IS_ENTITY_PLAYING_ANIM(objGrinder, sAnimDictMachine, "beefsplitter_loop")
						IF DOES_ENTITY_HAVE_DRAWABLE(objGrinder)
							PLAY_ENTITY_ANIM(objGrinder, "beefsplitter_loop", sAnimDictMachine, INSTANT_BLEND_IN, TRUE, FALSE)
							
							SET_CAN_CLIMB_ON_ENTITY(objGrinder, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(objCutter)
				IF bGrabCutter = FALSE
					IF iDoesObjectOfTypeExistAtCoords = 1
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<998.49, -2140.63, 28.47>>, 2.0, P_BEEFSPLITTER_S)
							objCutter = GET_CLOSEST_OBJECT_OF_TYPE(<<998.49, -2140.63, 28.47>>, 2.0, P_BEEFSPLITTER_S)	#IF IS_DEBUG_BUILD	PRINTLN("objCutter = GET_CLOSEST_OBJECT_OF_TYPE(<<998.49, -2140.63, 28.47>>, 2.0, P_BEEFSPLITTER_S)")	#ENDIF
							
							IF objCutter != NULL
								bGrabCutter = TRUE	#IF IS_DEBUG_BUILD	PRINTLN("bGrabCutter = TRUE")	#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMachine)
					IF NOT IS_ENTITY_PLAYING_ANIM(objCutter, sAnimDictMachine, "beefsplitter_loop")
						IF DOES_ENTITY_HAVE_DRAWABLE(objCutter)
							PLAY_ENTITY_ANIM(objCutter, "beefsplitter_loop", sAnimDictMachine, INSTANT_BLEND_IN, TRUE, FALSE)	#IF IS_DEBUG_BUILD	PRINTLN("PLAY_ENTITY_ANIM(objCutter, 'beefsplitter_loop', sAnimDictMachine, INSTANT_BLEND_IN, TRUE, FALSE)")	#ENDIF
							
							SET_CAN_CLIMB_ON_ENTITY(objCutter, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(objMincer)
				IF bGrabMincer = FALSE
					IF iDoesObjectOfTypeExistAtCoords = 2
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<975.99, -2120.83, 30.94>>, 2.0, V_ILEV_ABMINCER)
							objMincer = GET_CLOSEST_OBJECT_OF_TYPE(<<975.99, -2120.83, 30.94>>, 2.0, V_ILEV_ABMINCER)
							
							IF objMincer != NULL
								bGrabMincer = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMachine)
					IF NOT IS_ENTITY_PLAYING_ANIM(objMincer, sAnimDictMachine, "beefsplitter_loop")
						IF DOES_ENTITY_HAVE_DRAWABLE(objMincer)
							PLAY_ENTITY_ANIM(objMincer, "beefsplitter_loop", sAnimDictMachine, INSTANT_BLEND_IN, TRUE, FALSE)
							
							SET_CAN_CLIMB_ON_ENTITY(objMincer, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			iDoesObjectOfTypeExistAtCoords++
			
			IF iDoesObjectOfTypeExistAtCoords > 2
				iDoesObjectOfTypeExistAtCoords = 0
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(objGrinder)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objGrinder)
				bGrabGrinder = FALSE
			ENDIF
			IF DOES_ENTITY_EXIST(objCutter)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objCutter)
				bGrabCutter = FALSE
			ENDIF
			IF DOES_ENTITY_EXIST(objMincer)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objMincer)
				bGrabMincer = FALSE
			ENDIF
			
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<962.529602, -2105.850342, 30.0>>, <<50.0, 40.0, 30.0>>)
				UNLOAD_ANIM_DICT(sAnimDictMachine)
			ENDIF
		ENDIF
		
		//Blips
		UPDATE_BLIP_FLASH_TIMERS()
		
		//Switch
		SWITCH_CONTROL()
		
		//Rail
		IF eMissionObjective > initMission
		AND eMissionObjective <= stageMichaelEscape
		AND NOT IS_CUTSCENE_PLAYING()
			IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
				UPDATE_RAIL_MOVE()
				
				IF bOkToMoveRail
					IF iTimerNode <> iCurrentNode
						fDistFromFinalNode = 0
						
						FOR i = iCurrentNode + 1 TO iRailNodeToStopAt
							fDistFromFinalNode += GET_DISTANCE_BETWEEN_COORDS(sRailNodes[CLAMP_INT(i, 0, COUNT_OF(sRailNodes) - 1)].vPos, sRailNodes[CLAMP_INT(i + 1, 0, COUNT_OF(sRailNodes) - 1)].vPos)
						ENDFOR
						
						iTimerNode = iCurrentNode
					ENDIF
					
					fDistFromCurrentNode = GET_DISTANCE_BETWEEN_COORDS(vRailCurrent, sRailNodes[CLAMP_INT(iCurrentNode + 1, 0, COUNT_OF(sRailNodes) - 1)].vPos)
					
					IF IS_SELECTOR_ONSCREEN(TRUE)
						SET_SWITCH_WHEEL_AND_STATS_UNDER_HUD_THIS_FRAME()
					ELIF IS_SELECTOR_ONSCREEN(FALSE)
						SET_SWITCH_WHEEL_UNDER_HUD_THIS_FRAME()
					ELIF (IS_PHONE_ONSCREEN(TRUE)
					OR IS_CELLPHONE_TRACKIFY_IN_USE())
					AND NOT (IS_PLAYER_IN_FIRST_PERSON_CAMERA())
						SET_PHONE_UNDER_HUD_THIS_FRAME()
					ENDIF
					
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND eSwitchCamState = SWITCH_CAM_IDLE
						IF GET_IS_HIDEF()
						OR NOT IS_PHONE_ONSCREEN(TRUE)
							IF NOT g_bCurrentlyUsingTelescope
								DRAW_GENERIC_METER(ROUND(fDistFromFinalNode + fDistFromCurrentNode), ROUND(fDistTotalFirstFinalNode), "MCH2_TIMER", HUD_COLOUR_WHITE, -1, HUDORDER_TOP)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//PRINTLN("iTotalRailNodes - iCurrentNode / iTotalRailNodes = ", iTotalRailNodes - iCurrentNode, ", ", iTotalRailNodes)
			ENDIF
		ENDIF
		
		IF bBlood = FALSE
		OR pedBlood != GET_CURRENT_PLAYER_PED_ENUM()
			IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
			AND NOT IS_PED_INJURED(PLAYER_PED(CHAR_MICHAEL))
				IF DOES_ENTITY_HAVE_DRAWABLE(PLAYER_PED(CHAR_MICHAEL))
				AND DOES_ENTITY_HAVE_PHYSICS(PLAYER_PED(CHAR_MICHAEL))
					IF IS_ENTITY_ON_SCREEN(PLAYER_PED(CHAR_MICHAEL))
					AND NOT IS_SCREEN_FADED_OUT()
						APPLY_PED_DAMAGE_PACK(PLAYER_PED(CHAR_MICHAEL), "SCR_Torture", 0.0, 1.0)
						
						pedBlood = GET_CURRENT_PLAYER_PED_ENUM()
						
						bBlood = TRUE	#IF IS_DEBUG_BUILD	PRINTLN("APPLY_PED_BLOOD_DAMAGE_BY_ZONE - bBlood = TRUE")	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Combat
		REPEAT COUNT_OF(sEnemyShootout) i
			ADVANCE_COMBAT_UPDATE(sEnemyShootout[i])
		ENDREPEAT
		REPEAT COUNT_OF(sEnemyEscape) i
			ADVANCE_COMBAT_UPDATE(sEnemyEscape[i])
		ENDREPEAT
		REPEAT COUNT_OF(sEnemyBackup) i
			ADVANCE_COMBAT_UPDATE(sEnemyBackup[i])
		ENDREPEAT
		
		//Debug Routine
		#IF IS_DEBUG_BUILD
			debugRoutine()
		#ENDIF
		
		//Hide HUD/Radar
		IF bRadar = FALSE
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
		ELSE
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
		ENDIF
		
		//Phone
//		IF NOT IS_GAMEPLAY_CAM_RENDERING()
//		#IF IS_DEBUG_BUILD	AND NOT IS_CAM_RENDERING(GET_DEBUG_CAM())	#ENDIF
//		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())	//Work around for bug 946097
//		AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
//		AND IS_PLAYER_SWITCH_IN_PROGRESS()
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
//			
//			IF NOT IS_CELLPHONE_DISABLED()
//				DISABLE_CELLPHONE(TRUE)	PRINTLN("DISABLE_CELLPHONE(TRUE) 1")
//			ENDIF
//		ENDIF
		
		//Audio Scene
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND eMissionObjective < stageMichaelEscape
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_MICHAEL_ON_MEATHOOK")
				START_AUDIO_SCENE("MI_2_MICHAEL_ON_MEATHOOK")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("MI_2_MICHAEL_ON_MEATHOOK")
				STOP_AUDIO_SCENE("MI_2_MICHAEL_ON_MEATHOOK")
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		AND eMissionObjective = stageFindMichael
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
					STOP_AUDIO_SCENE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
					START_AUDIO_SCENE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
				ENDIF
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
				STOP_AUDIO_SCENE("MI_2_DRIVE_TO_SLAUGHTERHOUSE")
			ENDIF
		ENDIF
		
		//Temporary Radar
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND eMissionObjective < stageMichaelEscape
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//			IF NOT IS_CELLPHONE_DISABLED()
//				DISABLE_CELLPHONE(TRUE)	PRINTLN("DISABLE_CELLPHONE(TRUE) 2")
//			ENDIF
			
			DISPLAY_RADAR(FALSE)
		ELSE
			IF eMissionObjective <> stageCutIntro
			AND NOT (eMissionObjective = stageFindMichael AND iCutsceneStage = 0)
				IF IS_GAMEPLAY_CAM_RENDERING()
					IF IS_CELLPHONE_DISABLED()
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							DISABLE_CELLPHONE(FALSE)	PRINTLN("DISABLE_CELLPHONE(FALSE) 1")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Rolling Start
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR DOES_CAM_EXIST(scsSwitchCam_MichaelToFranklin.ciSpline)
			IF DOES_ENTITY_EXIST(vehFranklin)
			AND NOT IS_ENTITY_DEAD(vehFranklin)
			AND IS_PED_IN_VEHICLE(PLAYER_PED(CHAR_FRANKLIN), vehFranklin)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehFranklin)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("RollingStartSkip")
						SWITCH_STATE switchState
						INT switchIndex
						
						//[MF] Make sure we're not in a custom switch cam.
						IF eSwitchCamState = SWITCH_CAM_IDLE
							switchState = GET_PLAYER_SWITCH_STATE()	
							switchIndex = GET_PLAYER_SWITCH_JUMP_CUT_INDEX()
						ENDIF
						
						IF (switchState = SWITCH_STATE_JUMPCUT_DESCENT AND switchIndex = 0)
						OR (DOES_CAM_EXIST(scsSwitchCam_MichaelToFranklin.ciSpline) AND GET_CAM_SPLINE_NODE_INDEX(scsSwitchCam_MichaelToFranklin.ciSpline) >= scsSwitchCam_MichaelToFranklin.iCamSwitchFocusNode)
							PRINTLN("STARTING FRANKLIN'S CAR MOVING...")
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehFranklin, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(001, "ALrollingstart") - 4000.0)
							SET_PLAYBACK_SPEED(vehFranklin, 1.0)
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehFranklin)
							SET_PLAYBACK_TO_USE_AI(vehFranklin)	//START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehFranklin, 001, "ALrollingstart", GET_VEHICLE_ESTIMATED_MAX_SPEED(vehFranklin) / 2.0)
							
							SET_VEHICLE_FORWARD_SPEED(vehFranklin, GET_VEHICLE_ESTIMATED_MAX_SPEED(vehFranklin) / 2.0)
							SET_VEHICLE_ENGINE_ON(vehFranklin, TRUE, TRUE)
							
							CLEAR_AREA(GET_ENTITY_COORDS(vehFranklin, FALSE), 100.0, TRUE)
							
							SET_LABEL_AS_TRIGGERED("RollingStartSkip", TRUE)
						ENDIF
					ELSE
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						AND eSwitchCamState = SWITCH_CAM_IDLE
							IF HAS_LABEL_BEEN_TRIGGERED("RollingStartSkip")
								INT iXValue, iYValue
								
								iXValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_MOVE_LR) * 255.0)
								iYValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_MOVE_UD) * 255.0)
								
								IF (iXValue < -50 OR iXValue > 50)
								OR (iYValue < -50 OR iYValue > 50)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)
									//FLOAT fSpeed = GET_ENTITY_SPEED(vehFranklin)
									
									STOP_PLAYBACK_RECORDED_VEHICLE(vehFranklin)

									//SET_VEHICLE_FORWARD_SPEED(vehFranklin, fSpeed)
									
									SET_LABEL_AS_TRIGGERED("RollingStartStop", TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Bin Bag Items
//		IF eMissionObjective >= stageAbattoirShootout AND eMissionObjective <= stageBackToMichaels
//			IF NOT DOES_ENTITY_EXIST(objBinBag)
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("ReclaimedGuns")
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_WEAPON_1")
//					AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH2_WEAPON_2")
//						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<968.464600, -2122.092773, 32.316872>>, <<21.0, 10.0, 3.0>>)
//							IF HAS_MODEL_LOADED_CHECK(PROP_RUB_BINBAG_SD_02)
//								objBinBag = CREATE_OBJECT(PROP_RUB_BINBAG_SD_02, <<948.190002, -2124.189941, 31.780001>>)
//								
//								SET_ENTITY_COORDS(objBinBag, <<948.190002, -2124.189941, 31.780001>>)
//								SET_ENTITY_ROTATION(objBinBag, <<0.0, 0.0, 9.167327>>)
//								
//								FREEZE_ENTITY_POSITION(objBinBag, TRUE)
//								
//								SET_MODEL_AS_NO_LONGER_NEEDED(PROP_RUB_BINBAG_SD_02)
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<963.873108, -2124.677002, 33.467842>>, <<4.0, 4.0, 3.0>>)
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("ReclaimedGuns")
//						IF NOT DOES_BLIP_EXIST(blipGuns)
//							blipGuns = CREATE_BLIP_FOR_OBJECT(objBinBag)
//							
//							PRINT_HELP_ADV("MCH2_STASHHLP")
//						ENDIF
//					ENDIF
//					
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_WEAPON", "MCH2_WEAPON_1")
//						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("MCH2_WEAPON", "MCH2_WEAPON_2")
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<948.621826, -2124.189697, 33.438431>>, <<1.75, 1.75, 3.0>>)
//				OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<948.621826, -2124.189697, 25.438431>>, <<50.0, 50.0, 50.0>>)
//					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<948.621826, -2124.189697, 33.438431>>, <<1.75, 1.75, 3.0>>)
//						PLAY_SOUND_FROM_COORD(-1, "PICKUP_WEAPON_DEFAULT", GET_ENTITY_COORDS(objBinBag), "HUD_FRONTEND_WEAPONS_PICKUPS_SOUNDSET")
//						
//						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//							RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED(CHAR_MICHAEL), FALSE)
//						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//							SET_LABEL_AS_TRIGGERED("RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT", TRUE)
//						ENDIF
//					ENDIF
//					
//					SAFE_DELETE_OBJECT(objBinBag)
//					SAFE_REMOVE_BLIP(blipGuns)
//					
//					SET_LABEL_AS_TRIGGERED("ReclaimedGuns", TRUE)
//				ENDIF
//			ENDIF
//			
//			IF HAS_LABEL_BEEN_TRIGGERED("RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT")
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				AND IS_PED_IN_VEHICLE(NOT_PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//						CREATE_CONVERSATION_ADV("MCH2_WEAPON2")
//						
//						RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED(CHAR_MICHAEL), FALSE)
//						
//						SET_LABEL_AS_TRIGGERED("RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT", FALSE)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF bPhoneSwitch
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND bFranklinToMichael = FALSE
			AND GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
				SWITCH_STATE switchState
				IF IS_PLAYER_SWITCH_IN_PROGRESS()
					switchState = GET_PLAYER_SWITCH_STATE()
				ENDIF
				
				PRINTLN("switchState = ", switchState)
				
				IF (NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND IS_GAMEPLAY_CAM_RENDERING())
				OR (IS_PLAYER_SWITCH_IN_PROGRESS()
				AND switchState >= SWITCH_STATE_OUTRO_HOLD)
					DISABLE_CELLPHONE(FALSE)	PRINTLN("DISABLE_CELLPHONE(FALSE) 2")
					
					LAUNCH_TRACKIFY_IMMEDIATELY(TRUE)
					
					bPhoneSwitch = FALSE	PRINTLN("bPhoneSwitch = FALSE   LAUNCH_TRACKIFY_IMMEDIATELY()")
				ENDIF
			ENDIF
		ENDIF
		
		IF bHaltVehicle
			VEHICLE_INDEX vehBringToHalt
			
			vehBringToHalt = GET_VEHICLE_PED_IS_IN(PLAYER_PED(CHAR_FRANKLIN))
			
			IF DOES_ENTITY_EXIST(vehBringToHalt)
			AND NOT IS_ENTITY_DEAD(vehBringToHalt)
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehBringToHalt, DEFAULT_VEH_STOPPING_DISTANCE * 2, 1, 0.5, TRUE)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					bHaltVehicle = FALSE
				ENDIF
			ELSE
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				bHaltVehicle = FALSE
			ENDIF
		ENDIF
		
		//Stats
		
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), MIC2_DAMAGE)
		
		IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != NULL
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), MIC2_MAX_SPEED)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), MIC2_CAR_DAMAGE)
		ELSE
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, MIC2_MAX_SPEED)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, MIC2_CAR_DAMAGE)
		ENDIF
		
		//Kills a Ped
		INT iEventCount = 0
		
		STRUCT_ENTITY_ID sei = CONSTRUCT_ENTITY_ID()
		EVENT_NAMES eventType
		
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventCount
			eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventCount)
			
			SWITCH (eventType)
				CASE EVENT_ENTITY_DESTROYED
					//Figure out what type of entity it is
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventCount, sei, SIZE_OF(STRUCT_ENTITY_ID))
					
					IF DOES_ENTITY_EXIST(sei.EntityId)//sei.EntityId
						IF IS_ENTITY_A_PED(sei.EntityId)
							IF IS_ENTITY_A_MISSION_ENTITY(sei.EntityId)
								INFORM_MISSION_STATS_OF_INCREMENT(MIC2_KILLS)
//							ELSE
//								INFORM_MISSION_STATS_OF_INCREMENT(MIC2_INNOCENTS_KILLED)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
		
//		WEAPON_TYPE wtCurrentUpdate = WEAPONTYPE_UNARMED
//		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtCurrentUpdate)
//		
//		IF wtCurrent != wtCurrentUpdate
//		OR iAmmo = -1
//			wtCurrent = wtCurrentUpdate
//			iAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtCurrent)
//		ENDIF
//		
//		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtCurrent) < iAmmo
//			INT iDifference = iAmmo - GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtCurrent)
//			
//			REPEAT iDifference i
//				INFORM_MISSION_STATS_OF_INCREMENT(MIC2_BULLETS_FIRED)
//			ENDREPEAT
//			
//			iAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtCurrent)
//		ENDIF
		
//		IF IS_CELLPHONE_TRACKIFY_IN_USE()
//			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC2_TRACKIFY_APP_USE)
//		ELSE
//			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, MIC2_TRACKIFY_APP_USE)
//		ENDIF
		
		IF IS_WAYPOINT_ACTIVE()
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIC2_WAYPOINT_USED)
		ENDIF
		
//		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
//			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC2_SPECIAL_ABILITY_TIME)
//		ELSE
//			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
//		ENDIF
		
		//INFORM_MISSION_STATS_OF_INCREMENT(MIC2_)
		
		//Stop peds attacking player
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		LOAD_UNLOAD_ASSETS()
		
		AUDIO_CONTROLLER()
		
		//Instance Priority
		IF eMissionObjective >= stageAbattoirShootout AND eMissionObjective <= stageMichaelEscape
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
		ELIF eMissionObjective = stageTriadsChase
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
		ELSE
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		ENDIF
		
		//Replay Camera
		SWITCH eMissionObjective
			CASE stageAbattoirShootout
			CASE stageSwitchToMichael
			CASE stageMichaelEscape
			CASE stageMichaelFree
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220869
			BREAK
		ENDSWITCH
		
		//Objective switch
		SWITCH eMissionObjective
			CASE initMission
				initialiseMission()
			BREAK
			CASE stageCutIntro
				IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2LeadOut)
				AND HAS_ANIM_DICT_LOADED_CHECK("cellphone@")
				AND HAS_MODEL_LOADED_CHECK(Prop_phone_ING_03)
				AND HAS_MODEL_LOADED_CHECK(mn_FrankPhoneDisplay)
					cutIntro()
				ENDIF
			BREAK
			CASE stageFindMichael
				IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_01)
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_02)
				AND HAS_MODEL_LOADED_CHECK(CSB_CHIN_GOON)
				AND HAS_MODEL_LOADED_CHECK(PROP_LD_HOOK)
				AND HAS_MODEL_LOADED_CHECK(PROP_CS_PADLOCK)
				AND HAS_MODEL_LOADED_CHECK(PROP_CS_LEG_CHAIN_01)
				AND HAS_MODEL_LOADED_CHECK(BISON)
				AND HAS_MODEL_LOADED_CHECK(Prop_phone_ING_03)
				AND HAS_MODEL_LOADED_CHECK(mn_FrankPhoneDisplay)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Smoking)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle1)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle2)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle3)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictIdle4)
				AND HAS_ANIM_DICT_LOADED_CHECK("shake_cam_all@")
				AND HAS_ANIM_DICT_LOADED_CHECK("cellphone@")
				AND HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
				AND HAS_RECORDING_LOADED_CHECK(001, "ALrollingstart")
					findMichael()
				ENDIF
			BREAK
			CASE stageAbattoirShootout
				IF HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_01)
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_02)
				AND HAS_MODEL_LOADED_CHECK(BISON)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece1)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Smoking)
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
				AND HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
					abattoirShootout()
				ENDIF
			BREAK
			CASE stageSwitchToMichael
				IF HAS_MODEL_LOADED_CHECK(GET_WEAPONTYPE_MODEL(WEAPONTYPE_COMBATPISTOL))
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Switch)
				AND HAS_CLIP_SET_LOADED(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
				AND HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
					switchToMichael()
				ENDIF
			BREAK
			CASE stageMichaelEscape
				IF HAS_MODEL_LOADED_CHECK(GET_WEAPONTYPE_MODEL(WEAPONTYPE_COMBATPISTOL))
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_01)
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_02)
				AND HAS_MODEL_LOADED_CHECK(FELTZER2)
				AND HAS_MODEL_LOADED_CHECK(COQUETTE)
				AND HAS_MODEL_LOADED_CHECK(BISON)
				AND HAS_MODEL_LOADED_CHECK(PROP_WATERCRATE_01)
				AND HAS_MODEL_LOADED_CHECK(V_IND_COO_HALF)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
				AND HAS_CLIP_SET_LOADED(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
				AND HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
					michaelEscape()
				ENDIF
			BREAK
			CASE stageMichaelFree
				IF HAS_MODEL_LOADED_CHECK(GET_WEAPONTYPE_MODEL(WEAPONTYPE_COMBATPISTOL))
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_01)
				AND HAS_MODEL_LOADED_CHECK(G_M_M_CHIGOON_02)
				AND HAS_MODEL_LOADED_CHECK(FELTZER2)
				AND HAS_MODEL_LOADED_CHECK(COQUETTE)
				AND HAS_MODEL_LOADED_CHECK(BISON)
				AND HAS_MODEL_LOADED_CHECK(PROP_WATERCRATE_01)
				AND HAS_MODEL_LOADED_CHECK(V_IND_COO_HALF)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2Hook)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2SetPiece2)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2FranklinBeckon)
				AND HAS_CLIP_SET_LOADED(GET_CLIP_SET_FOR_SCRIPTED_GUN_TASK(SCRIPTED_GUN_TASK_HANGING_UPSIDE_DOWN))
				AND HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
					michaelFree()
				ENDIF
			BREAK
			CASE stageTriadsChase
				IF HAS_MODEL_LOADED_CHECK(BISON)
				AND HAS_MODEL_LOADED_CHECK(FELTZER2)
				AND HAS_MODEL_LOADED_CHECK(COQUETTE)
				AND HAS_CLIP_SET_LOADED("MOVE_STRAFE@COP")
					triadsChase()
				ENDIF
			BREAK
			CASE stageBackToMichaels
				backToMichaels()
			BREAK
			CASE stageCutEnd
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMic2WashFace)
					cutEnd()
				ENDIF
			BREAK
			CASE passMission
				missionPassed()
			BREAK
			CASE failMission
				missionFailed()
			BREAK
		ENDSWITCH
		
		IF eMissionObjective >= stageSwitchToMichael AND eMissionObjective <= stageMichaelEscape
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND NOT HAS_LABEL_BEEN_TRIGGERED("MichaelFallPushIn")
						DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()	PRINTLN("DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam_FranklinToMichael)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam_FranklinInCarToMichael)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam_MichaelToFranklin)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam_FranklinGrappleToMichael)
			HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
		#ENDIF

		WAIT(0)
	ENDWHILE
	
//Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
