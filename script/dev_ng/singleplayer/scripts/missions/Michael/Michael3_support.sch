USING "player_ped_public.sch"
USING "rgeneral_include.sch"
USING "selector_public.sch"
USING "taxi_functions.sch"
USING "trigger_box.sch"

/*
											 _     _  _______  ______   _  _______  ______   _        _______   ______ 
											(_)   (_)(_______)(_____ \ | |(_______)(____  \ (_)      (_______) / _____)
											 _     _  _______  _____) )| | _______  ____)  ) _        _____   ( (____  
											| |   | ||  ___  ||  __  / | ||  ___  ||  __  ( | |      |  ___)   \____ \ 
											 \ \ / / | |   | || |  \ \ | || |   | || |__)  )| |_____ | |_____  _____) )
											  \___/  |_|   |_||_|   |_||_||_|   |_||______/ |_______)|_______)(______/
*/

// Enumerators
//---------------------------------------------------------------------------------------------------------------------------------------------------

	// An enum for all the different stages of the mission
	ENUM MISSION_STAGE_FLAG
		STAGE_WALK_TO_DAVE = 0,
		STAGE_MEETING_CUTSCENE,
		STAGE_FIRST_AREA,
		STAGE_TREVOR_SHOOTS_HELICOPTER,
		STAGE_TREVOR_SAVES_DAVE,
		STAGE_DAVE_BY_FOUNTAIN,
		STAGE_DAVE_ESCAPES,
		STAGE_ESCAPE_MUSEUM,
		STAGE_VEHICLE_ESCAPE,
		STAGE_END_CUTSCENE,
		STAGE_MISSION_PASSED,
		NUM_STAGES
	ENDENUM 

	// Enum used for the conversation systems
	ENUM MISSION_MESSAGE
		MISSION_MESSAGE_01,
		MISSION_MESSAGE_02,
		MISSION_MESSAGE_03,
		MISSION_MESSAGE_04,
		MISSION_MESSAGE_05,
		MISSION_MESSAGE_06,
		MISSION_MESSAGE_07,
		MISSION_MESSAGE_08,
		MISSION_MESSAGE_09,
		MISSION_MESSAGE_10,
		MISSION_MESSAGE_11,
		MISSION_MESSAGE_12,
		MISSION_MESSAGE_13,
		MISSION_MESSAGE_14,
		MISSION_MESSAGE_15,
		MISSION_MESSAGE_16,
		MISSION_MESSAGE_17,
		MISSION_MESSAGE_18,
		MISSION_MESSAGE_19,
		MISSION_MESSAGE_20,
		MISSION_MESSAGE_IDLE
	ENDENUM

	// Enum used for mission_stage management
	ENUM STAGE_SWITCHSTATE
		STAGESWITCH_IDLE,
		STAGESWITCH_REQUESTED,
		STAGESWITCH_EXITING,
		STAGESWITCH_ENTERING
	ENDENUM

	// Hot swap status
	enum SWITCHSTATE
		SWITCH_NOSTATE,
		SWITCH_SELECTION_MADE,
		SWITCH_COMPLETE,
		SWITCH_IN_PROGRESS
	ENDENUM

	// All possible fail conditions
	ENUM MISSION_FAIL_FLAG
		mff_default,		// Force cleanup fail
		mff_debug_forced,	// Debug: User pressed F on keyboard to force a fail
		mff_michael_dead,	// Michael died
		mff_trevor_dead,	// Trevor died
		mff_dave_dead,		// Dave died
		mff_dave_abdn		// Dave abandoned
	ENDENUM

	// List of key events in the mission
	ENUM MISSION_EVENT_FLAG
		mef_manage_radar,
		mef_manage_switch,
		mef_manage_ped_spatial_data,
		
		mef_fight_balc_1,					// gun fight: MW from the balconies by the parking lot
		mef_fight_balc_2,					// gun fight: MW from the balconies to the right
		mef_fight_balc_3,					// gun fight: MW from the balcony bridge
		
		mef_fight_cy_fountain,				// gun fight: MW from the stairs by the fountain
		
		mef_fight_cy_1,						// gun fight: MW vs FIB bottom of the stairs start of the court yard
		mef_fight_cy_2,						// gun fight: MW come around by the fountain
		mef_fight_cy_3,						// gun fight: MW when nearing the car park
		
		mef_fight_parking_lot,				// gun fight: MW near pillar and a parked jeep
		mef_parking_lot_jeep,				// vehicle arrival: MW arrive in a jeep at the parking lot
		mef_escape_vehicles,				// sets up some escape vehicles
		
		mef_heli_2,
		mef_heli_3,

		mef_trevor_arrival_and_snipe,
		
		mef_num_events
	ENDENUM

	// List of peds used in the mission
	ENUM MISSION_PED_FLAGS
		mpf_invalid = -1,
	
		mpf_mike,
		mpf_trev,
		mpf_frank,
		mpf_dave,
		mpf_andreas,
		
		// IG anim peds
		mpf_ig_1_mw_1,
		mpf_ig_1_mw_2,
		
		// First big fight at the start
		mpf_start_fib_lead,
		mpf_start_fib_1,
		mpf_start_fib_2,
		mpf_start_fib_3,
		
		mpf_start_cia_lead,
		mpf_start_cia_2,
		mpf_start_cia_3,
		mpf_start_cia_4,
		
		// Fight from the stairs to the balcony
		mpf_heli_1_mw_1,
		mpf_heli_1_fib_1,
		mpf_heli_1_fib_2,
		
		mpf_heli_strafe_fib_1,
		mpf_heli_strafe_fib_2,
		mpf_heli_strafe_fib_3,
		
		// first courtyard fight at the bottom of the stairs
		mpf_cy_1_mw_1,
		mpf_cy_1_mw_2,
		mpf_cy_1_mw_3,
		mpf_cy_1_fib_1,
		mpf_cy_1_fib_2,
		mpf_cy_1_fib_3,
		
		// second courtyard fight FIB vs MW in the middle of the CY
		mpf_cy_2_mw_1,
		mpf_cy_2_mw_2,
		mpf_cy_2_mw_3,
		mpf_cy_2_mw_4,
		mpf_cy_2_mw_5,
		mpf_cy_2_mw_6,
		
		// third courtyard, guys coming from the car park
		mpf_cy_3_mw_1,
		mpf_cy_3_mw_2,
		
		// mw coming from the stairs by the fountain
		mpf_cy_fount_mw_1,
		mpf_cy_fount_mw_2,
		
		// Peds appear on the balcony to the left
		mpf_balc_1_mw_1,
		mpf_balc_1_mw_2,
		
		// Peds appear on the balcony to the right
		mpf_balc_2_mw_1,
		mpf_balc_2_mw_2,
		
		// Peds appear on the balcony bridge
		mpf_balc_3_mw_1,
		mpf_balc_3_mw_3,
		mpf_balc_3_mw_4,
		
		// Single peds that pop out 
		mpf_popout_stairs_1,
		mpf_popout_balc_1,
		
		// First group of peds on the parking lot
		mpf_pl_1_mw_1,
		mpf_pl_1_mw_2,
		mpf_pl_1_mw_3,
		mpf_pl_1_mw_4,
		mpf_pl_1_mw_5,
		
		// Heli pilots
		mpf_mw_heli_1_pilot,
		mpf_mw_heli_2_pilot,
		mpf_mw_heli_3_pilot,
		mpf_mw_heli_dave_escape_pilot,
		
		// 2 people inside the Merry weather jeep
		mpf_mw_jeep_2_1,
		mpf_mw_jeep_2_2,

		mpf_num_peds
	ENDENUM

	// List of vehicles used
	ENUM MISSION_VEHICLE_FLAGS
		mvf_player_car,
		
		mvf_mw_heli_1,
		mvf_mw_heli_2,
		mvf_mw_heli_3,
		mvf_mw_heli_dave_escape,
		
		mvf_mw_jeep_1,
		mvf_mw_jeep_2,
		mvf_mw_van_1,
		
		mvf_escape_1,
		mvf_escape_2,
		mvf_escape_3,
		mvf_escape_4,
		mvf_escape_5,
		
		mvf_dave_car,
		mvf_crash_heli,
		mvf_other_player_car,
		mvf_trev_heli,
		mvf_num_vehicles
	ENDENUM
	
	ENUM MISSION_OBJECT_FLAGS
		mof_daves_paper,
		mof_large_blocker,
		mof_small_blocker_start_right,
		mof_crashed_heli,
		mof_mike_ciggy,
		mof_num_objects
	ENDENUM
	
	ENUM MISSION_CAMERA_FLAGS
		mcf_generic,
		mcf_sniped_cam1,
		mcf_sniped_cam2,
		mcf_num_cameras
	ENDENUM
	
	ENUM SYNCED_SCENE_FLAGS
		ssf_invalid = -1,
		ssf_dave_idle,
		ssf_dave_dialogue_idle,
		ssf_dave_dialogue_outro,
		ssf_ig_1_mw,
		ssf_ig_1_dreyfuss,
		ssf_ig_1_mw_kill_dreyfuss,
		ssf_ig_2_fib_strafe_death,
		ssf_ig_6_swat_cover,
		ssf_ig_7_mw_bridge,
		ssf_ig_7_fib_mike_only,
		ssf_ig_8_mw_room,
		ssf_heli_leadout_main,
		ssf_heli_leadout_cockpit,
		ssf_num_synced_scenes
	ENDENUM
	
	ENUM CREW_MEMBER
		cmf_mike,
		cmf_hacker,
		cmf_num_crew_members
	ENDENUM
	
	ENUM MUSEUM_SECTIONS
		MUS_NO_AREA	= -1,
	
		MUS_GND_YARD_ONE,	// Ground floor sections
		MUS_GND_YARD_TWO,
		MUS_GND_YARD_THREE,
		MUS_GND_END,
		MUS_GND_TO_PL,		// stairs from the ground floor to the parking lot
		MUS_GND_PL_END,	
		MUS_BALC_TO_GND_START,	// Upper balcony sections
		MUS_BALC_START,
		MUS_BALC_ONE,
		MUS_BALC_TWO,
		MUS_BALC_BRIDGE_SMALL,
		MUS_BALC_THREE,
		MUS_BALC_BRIDGE_BIG,
		MUS_BALC_FOUR,
		MUS_BALC_END,
		
		MUS_NUM_SECTIONS
	ENDENUM
	
	ENUM MUSEUM_SECTION_FLAGS
		MSF_GND_YARD_ONE		= 1,
		MSF_GND_YARD_TWO		= 2,
		MSF_GND_YARD_THREE		= 4,
		MSF_GND_END				= 8,
		MSF_GND_TO_PL			= 16,
		MSF_GND_PL_END			= 32,	
		MSF_BALC_TO_GND_START	= 64,
		MSF_BALC_START			= 128,
		MSF_BALC_ONE			= 256,
		MSF_BALC_TWO			= 512,
		MSF_BALC_BRIDGE_SMALL	= 1024,
		MSF_BALC_THREE			= 2048,
		MSF_BALC_BRIDGE_BIG		= 4096,
		MSF_BALC_FOUR			= 8192,
		MSF_BALC_END			= 16384
	ENDENUM
	
	ENUM AI_MIKE_FLAG
		AI_M_IDLE,
		AI_M_MOVING_FORWARD,
		AI_M_MOVING_TO_END,
		AI_M_WAITING_AT_END,
		AI_M_MOVING_TO_COVER,
		AI_M_PINNED_IN_COVER
	ENDENUM
	
	ENUM AI_TREV_FLAG
		AI_T_IDLE,
		AI_T_MOVING_TO_SNIPING_POS,
		AI_T_IN_COVER_NO_TARGET,
		AI_T_IN_COVER_WITH_TARGET
	ENDENUM


	ENUM ENEMY_WAVE_STATE
		ENEMY_WAVE_INIT, 
		ENEMY_WAVE_WAIT_FOR_INJURED, 
		ENEMY_WAVE_CREATE_NEW_ENEMIES,
		ENEMY_WAVE_DONE
	ENDENUM


	ENUM	DAVE_FLEE_STATE
		DAVE_FLEE_INIT,
		DAVE_FLEE_TASK_TO_PARKING_LOT,
		DAVE_FLEE_WAIT_TO_START_ATTACKING,
		DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN,
		DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR,
		DAVE_FLEE_WAIT_UNTIL_IN_CAR,
		DAVE_FLEE_TASK_GTFO,
		DAVE_FLEE_TASK_AI_DRIVING,
		DAVE_FLEE_TASK_WAIT_TO_DELETE,
		DAVE_FLEE_DONE
	ENDENUM
	DAVE_FLEE_STATE		eDaveFleeState = DAVE_FLEE_INIT
	DAVE_FLEE_STATE		eOtherPlayerFleeState = DAVE_FLEE_INIT

	ENUM PLAYER_SWITCH_STATE
		PLAYER_SWITCH_SETUP,
		PLAYER_SWITCH_SELECTING,
		PLAYER_SWITCH_SWAP_PLAYER_PEDS,
		PLAYER_SWITCH_COMPLETE
	ENDENUM

	ENUM	MICHAEL_AI_STATE
		MICHAEL_AI_INIT,
		MICHAEL_AI_WAIT_FOR_DAVE_TO_REACH_FOUNTAIN,
		MICHAEL_AI_WAIT_FOR_SECOND_ENEMIES_SPAWNED,
		MICHAEL_AI_SECOND_ZONE,
		MICHAEL_AI_WAIT_FOR_THIRD_ENEMIES_SPAWNED, 
		MICHAEL_AI_THIRD_ZONE,
		MICHAEL_AI_GET_TO_DAVE,
		MICHAEL_AI_GET_WAIT_TO_REACH_DAVE,
		MICHAEL_AI_DONE
	ENDENUM
	MICHAEL_AI_STATE	eMichaelAIState

	ENUM SNIPER_SCOPE_AUDIO_STATE
		SSA_INIT, 
		SSA_NO_SCOPE_ACTIVE,
		SSA_SCOPE_ACTIVE,
		SSA_DONE
	ENDENUM
	SNIPER_SCOPE_AUDIO_STATE eSniperAudioState

// Constants
//---------------------------------------------------------------------------------------------------------------------------------------------------
	
	TEXT_LABEL_7	str_dialogue				= "M3AUD"
	STRING			m_strDaveWaypointRec		= "mic3_dave_escape"
	STRING			m_strOtherPlayerWaypointRec	= "mic3_otherp_escape"
	STRING			m_strCutsceneHelicopter		= "mic_3_mcs_1_p1_a2"
	STRING		 	m_strAnimDict_IG1 			= "missmic3"
	STRING			m_strAnimDict_IG6			= "missmic3ig_6"
	STRING			m_strAnimDict_IG7			= "missmic3ig_7"
	STRING			m_strAnimDict_HeliLeadout	= "missmic3leadinout_mcs1"
	
	
	CONST_INT		iCONST_ONE_SHOT_KILL_HEALTH				101
	
	// Number of enemies in the first stage
	CONST_INT		iCONST_NUM_FIRST_AREA_CIA				4
	CONST_INT		iCONST_NUM_FIRST_AREA_FIB				4
	CONST_INT		iCONST_NUM_FIRST_AREA_MW				2
	
	// Number of enemies on the stairs
	CONST_INT		iCONST_NUM_STAIRS_MW					4

	// Number of enemies while Trevor saves Dave
	CONST_INT		iCONST_NUM_TREVOR_DAVE_FIB				6
	
	// Number of enemies during the Dave Escapes phase
	CONST_INT		iCONST_NUM_FINAL_WAVE_CIA				4
	CONST_INT		iCONST_NUM_FINAL_WAVE_FIB				3
	CONST_INT		iCONST_NUM_FINAL_WAVE_MW				3

	// Number of enemies during the Escape					
	CONST_INT		iCONST_NUM_ESCAPE_MW1					2
	CONST_INT		iCONST_NUM_ESCAPE_MW2					2
	CONST_INT		iCONST_NUM_ESCAPE_MW3					2
	CONST_INT		iCONST_NUM_ESCAPE_MW4					4
	CONST_INT		iCONST_NUM_ESCAPE_MW5					4

	CONST_INT		iCONST_NUM_TO_FAIL						3
	CONST_INT		iCONST_NUM_OF_AGGRESSIVE_PEDS			4
	
	// Tuning values for Dave's advancing system
	CONST_FLOAT		flCONST_TIME_FOR_DAVE_DEATH				30.0
	CONST_FLOAT		flCONST_TIME_FOR_DAVE_AGGRESSIVE		15.0
	CONST_FLOAT		flCONST_DAVE_DEATH_PERCENTAGE			0.5
	
	CONST_INT		I_TREVOR_LOS_TEST_TIMEOUT	6000
	CONST_INT		I_TREVOR_ENEMY_DIST_TIMER	4000
	CONST_INT		I_TREVOR_TARGET_TIMEOUT		12000
	
	// mission_stage management consts
	CONST_INT 		STAGE_ENTRY					0
	CONST_INT 		STAGE_EXIT					-1
	CONST_INT		I_NUM_DEF_SPOTS				10
	CONST_INT		I_LAST_COVER_IN_SECTION		I_NUM_DEF_SPOTS - 1
	CONST_INT		I_COMBAT_PED_MIN			3000
	CONST_INT		I_COMBAT_PED_MAX			12000
	
	//	Global replay stats
	CONST_INT		PLAYER_AT_FOUNTAIN			0
	CONST_INT		iCONST_MICHAEL				0
	CONST_INT		iCONST_TREVOR				1
	
	CONST_INT		TREVOR_RESPAWN_LOCATION		1
	CONST_INT		iCONST_ON_ROOF				0
	CONST_INT		iCONST_AT_DAVE				1
	
	CONST_INT		GENERIC_MISSION_FLAGS		2
	CONST_INT		iCONST_HELI_CUT_WATCHED		1
	
	//Props
	CONST_INT		iCONST_NUM_COURTYARD_COVER	18

	CONST_FLOAT		flCONST_WAVE_HEARTBEAT		0.500
	
	CONST_FLOAT		flCONST_CHANCE_FIRE_BLANKS_MIN	0.65
	CONST_FLOAT		flCONST_CHANCE_FIRE_BLANKS_MAX	0.65

	
	// Model names
	MODEL_NAMES		mod_ped_mw					= S_M_Y_BLACKOPS_01
	MODEL_NAMES		mod_ped_fib					= S_M_Y_SWAT_01
	MODEL_NAMES		mod_ped_cia					= S_M_M_CIASEC_01
	MODEL_NAMES		mod_ped_dave				= IG_DAVENORTON
	
	MODEL_NAMES		mod_heli_mw					= BUZZARD
	MODEL_NAMES		mod_heli_crashed			= PROP_WRECKED_BUZZARD
	
	MODEL_NAMES		mod_trev_heli				= FROGGER // MAVERICK
	MODEL_NAMES		mod_mw_jeep					= MESA3
	MODEL_NAMES		mod_mw_van					= MESA3 //BURRITO3
	
	// Enemy weapons
	WEAPON_TYPE		weap_cia_handgun			= WEAPONTYPE_PISTOL
	WEAPON_TYPE		weap_cia_rifle				= WEAPONTYPE_CARBINERIFLE
	WEAPON_TYPE		weap_fib_rifle				= WEAPONTYPE_CARBINERIFLE
	WEAPON_TYPE		weap_mw_rifle				= WEAPONTYPE_CARBINERIFLE
	
	// Vehicle Recordings
	TEXT_LABEL_15 	str_carrecs					= "michaelThree"
	
	TEXT_LABEL_31	str_AudioBankFIBShootout	= "FBI_HEIST_3B_SHOOTOUT"
	
	CONST_INT		rec_mw_heli_1				50
	CONST_INT		rec_mw_heli_2				4
	CONST_INT		rec_mw_heli_3				12
	CONST_INT		rec_mw_jeep_2				7
	CONST_INT		rec_dave_esc				1
	CONST_INT		rec_otherp_esc				2
	CONST_INT		rec_crash_heli				3
	
	// Vectors
	VECTOR			v_trev_heli_coord					= << -2223.3765, 192.4628, 193.6015 >>
	CONST_FLOAT		f_trev_heli_heading					260.0503
	CONST_INT		i_trev_heli_variation				1	
	
	VECTOR 			v_blip_museum_entrance 				= <<-2290.3428, 364.1428, 173.6017>>
	VECTOR			v_blip_parking_lot_coord			= <<-2334.5464, 294.4146, 169.3519>>
	VECTOR			v_blip_alley_coord					= <<-1468.3783, -392.9619, 37.5397 >>

	INT 			m_iFirstAreaHelicopterState 		= 1
	INT				iTrevorDaveNavBlock[6]
	INT				iMichaelNavBlock
	INT				iCourtyardPoolNavBlock[3]
	INT				iTrevorPathNavmeshBlock
	
	BOOL			m_bBlipManagementDuringAbandonChecks

// Structs
//---------------------------------------------------------------------------------------------------------------------------------------------------
	
	// Holds information about a custom coverpoint
	STRUCT COVER_STRUCT
		COVERPOINT_INDEX	id
		BOOL				bDoesCovExist
		BOOL				bFaceLeft
		VECTOR				pos
		FLOAT				dir
	ENDSTRUCT

	// struct, info about a ped
	STRUCT PED_STRUCT
		PED_INDEX			id
		COVER_STRUCT		cov
		BLIP_INDEX			blip
		INT					iGenericFlag					// this generic flag can be used in different situations where required
		INT					iDeadTimer			= -1		// time of death
		BOOL				bKilledByThePlayer				// flag stays on for a second to indicate this ped was killed by the player
		ITEMSET_INDEX		set								// preferred cover for this ped
		AI_BLIP_STRUCT		stealthBlip						// new blip system for blips fading in/out when noticed
		MUSEUM_SECTIONS		eSectionPedIsIn
		BOOL				bCanTrevorSeeThisPed
		INT					iTrevLOSCheckTimer
		FLOAT				fSquareDistFromMike	= -1.0
	ENDSTRUCT

	// Struct for keeping track of peds in a vehicle
	STRUCT VEH_STRUCT
		VEHICLE_INDEX		id
		BLIP_INDEX			blip		
		FLOAT				fStartSpeed
		FLOAT				fDesiredSpeed
		FLOAT				fCurrentSpeed				// keeps track of the vehicles playback speed on a recording
		INT					iSpeedTimeStamp
		INT					iDesiredDuration
	ENDSTRUCT
	
	// Struct to hold extra info about whether the object has been grabbed from the world or created
	STRUCT OBJ_STRUCT
		OBJECT_INDEX	id
		BOOL			bCustom
	ENDSTRUCT
	
	//PURPOSE: Contains additional data for interpolating the camera using a script calculated interpolation (this allows it to work with the timescale stuff)
	STRUCT CAM_STRUCT
		CAMERA_INDEX		id
		BOOL				bCustomInterp		// camera will be interpolated using the script system, not the code system
		VECTOR				vStartPos			// start position of the camera
		VECTOR				vEndPos				// position to interpolate to
		INT					iTimeStamp			// the time the interpolation was triggered
		INT					iInterpDuration		// the duration to interpolate over
	ENDSTRUCT

	//PURPOSE: 
	STRUCT MUSEUM_SECTION_STRUCT
		VECTOR				v1
		VECTOR				v2
		FLOAT				fWidth
		MUSEUM_SECTIONS		eNextSection
		MUSEUM_SECTIONS		eAlternativeSection		// alternative route
		MUSEUM_SECTIONS		ePrevSection
		BOOL				bCanBePinnedFromHere[MUS_NUM_SECTIONS]
		VECTOR				vDefensiveAreas[I_NUM_DEF_SPOTS]
		VECTOR				vNavToCoord
	ENDSTRUCT

TYPEDEF PROC EnemyWaveTaskingFunction(PED_STRUCT &ps, TRIGGER_BOX tbDefensiveArea)

	STRUCT ENEMY_WAVE
		ENEMY_WAVE_STATE			state
		structTimer					tmrHeartbeat
		STRING						strDebugName
		INT							iNumSpawned
		INT							iMaxNumber
		INT							iAccuracy
		FLOAT						flHeartbeatTime
		BOOL						bBlipPeds
		BOOL						bLookForSpawnPoints
		EnemyWaveTaskingFunction	taskFunction
	ENDSTRUCT

	STRUCT 	FLAG
		VECTOR pos
		FLOAT  dir
	ENDSTRUCT
	
// Variables
//---------------------------------------------------------------------------------------------------------------------------------------------------
	
	structPedsForConversation	sConvo							// used for dialogue
	INT							i_dialogue_timer				// timer used for last time a convo was playing
	SEQUENCE_INDEX				seq								// sequence used for tasking peds
	BOOL						bSequenceOpen					// used with custom tasks to detect if sequence is open or not
	LOCATES_HEADER_DATA			s_locatesDataMike				// locates header struct, used for locate tests
//	LOCATES_HEADER_DATA			s_locatesDataTrev				// locates header struct, used for locate tests
	BOOL 						b_cutscene_ready_for_exit
	SCENARIO_BLOCKING_INDEX 	blockingArea_kortzCenter
	
	REL_GROUP_HASH				REL_FRIEND						// relationship group for buddies
	REL_GROUP_HASH				REL_FIB
	REL_GROUP_HASH				REL_MW
	REL_GROUP_HASH				REL_CIA
	REL_GROUP_HASH				REL_FIB_FINAL					// relationship groups for the last group of enemies, that are only interested in the player
	REL_GROUP_HASH				REL_MW_FINAL					// relationship groups for the last group of enemies, that are only interested in the player
	REL_GROUP_HASH				REL_CIA_FINAL					// relationship groups for the last group of enemies, that are only interested in the player
	
	BLIP_INDEX					blip_Objective					// blip used for mission objectives
	#IF IS_DEBUG_BUILD
	BOOL						bDebugEnabled
	INT							iDebugInfo						// Debug info stuff
	#ENDIF
	
	// Mission entity arrays
	PED_STRUCT					peds[mpf_num_peds]				// ped array
	VEH_STRUCT					vehs[mvf_num_vehicles]			// vehicle array
	OBJ_STRUCT					objs[mof_num_objects]			// object/prop array
	CAM_STRUCT					cams[mcf_num_cameras]			// camera array
	INT							syncedScenes[ssf_num_synced_scenes]
	
	// Event and loading stuff
	MISSION_EVENT_LINK_STRUCT	sEvents[mef_num_events]
	ASSET_MANAGEMENT_DATA		sAssetData							// monitors what is loaded and is helpful in z skips
	
	// Stage management and skip stuff
	STAGE_SWITCHSTATE 			stageSwitch						// current switching status
	INT 						mission_stage					// current mission mission_stage
	INT 						mission_substage				// current mission mission_substage (switch flag used in mission_stage procs)
	INT 						requestedStage					// the mission_stage requested by a mission_stage switch
	INT							iStageTimer						// timer used for debug
	BOOL						bDoSkip							// triggers a stage skip
	INT							iSkipToStage					// the stage to skip to
	MISSION_STAGE_FLAG			msfStageSkip
	BOOL						m_bDoStairFires
	
	#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct	zMenuNames[NUM_STAGES] 		// z menu names
	#ENDIF
	
	// Hotswap stuff
	SELECTOR_PED_STRUCT			sSelectorPeds					
	SELECTOR_CAM_STRUCT 		sCamDetails
	INT 						i_MichaelEscapeAI
	INT 						i_TrevorEscapeAI
	
	// Heli 1 & Trevor Slow-mo snipe cutscene
	FLOAT				f_current_time_scale
	BOOL				bCanBlipTrevor
	
	// Heli 2
	
	// Heli 3
	INT 				i_heli_3_locate_tracker
	
// Buddy AI 
	// Mike
	MUSEUM_SECTION_STRUCT		sMuseumSections[MUS_NUM_SECTIONS]
	AI_MIKE_FLAG 				ai_michael							= AI_M_IDLE
	AI_TREV_FLAG 				ai_trevor							= AI_T_IDLE
	AI_TREV_FLAG 				ai_trevor_prevFrame					= AI_T_IDLE
	MUSEUM_SECTIONS 			eCurrentSection						= MUS_NO_AREA
	MUSEUM_SECTIONS				eNextSection						= MUS_NO_AREA
	MUSEUM_SECTIONS				ePrevSection						= MUS_NO_AREA
	MUSEUM_SECTIONS				ePinnedFromHere						= MUS_NO_AREA
	BOOL						bHasMikeEnteredNewArea				= FALSE	
	BOOL 						bEnemyInArea[MUS_NUM_SECTIONS]
	BOOL						m_bMichaelAIGoesStraightToDave
	BOOL						m_bDavePlayGetInCarMessage
	
	// Trev
	INT							iTrevTargetUpdateTimer				= 0
	
	DECAL_ID					decal_andreas
	
	// Debug vars
	#IF IS_DEBUG_BUILD
		WIDGET_GROUP_ID 			widget_debug					// debug widget
		FLOAT						f_debug_gam_cam_rel_heading
		FLOAT						f_debug_gam_cam_rel_pitch
		FLOAT						f_debug_player_heading
		FLOAT						f_debug_rec_phase
		
		//AI DEBUG
		TEXT_WIDGET_ID 				twi_mike_ai_stage
		TEXT_WIDGET_ID 				twi_mike_ai_section_current
		TEXT_WIDGET_ID 				twi_mike_ai_section_next
		TEXT_WIDGET_ID 				twi_mike_ai_section_next_alt
		TEXT_WIDGET_ID				twi_mike_ai_section_pinned
		TEXT_WIDGET_ID 				twi_trev_ai_stage
	#ENDIF

	PED_INDEX		m_pedAllyDave					// Dave, our ally through the whole mission.
	PED_INDEX		m_pedOtherPlayer
	BLIP_INDEX		m_blipAllyDave					// Dave's blip
	BLIP_INDEX		blipOtherPlayer					// Blip for the other player during the fountain stage
	

	structTimer		m_tmrMikeInDanger				// Timer to control Mike in Danger when the 2 FIB come attack him if you're playing as Trevor during Dave At Fountain
	structTimer		m_tmrWarCries
	structTimer		m_tmrDaveDeathAdvance			// Timer to control when Dave should advance to death
	structTimer 	m_tmrFailWarningTimer
	structTimer		m_tmrDaveAggressiveAdvance		// Timer to control when Dave should advance aggressively
	structTimer		m_tmrDelayBeforeSwitch			// Small delay after conversation's done before we swap to Michael
	structTimer		m_tmrDaveReassessTargets		// Timer to check for new targets for Dave
	structTimer		m_tmrOtherPlayerReassessTargets	// Timer to check for new targets for the other player
	BOOL			m_bAllEnemiesDeadDuringAdvance	// Check to see if everyone's dead while Dave's advancing
	
	
	FLAG			m_flagDaveCoverPositions[6]		// Cover positions for Dave
	FLAG			m_flagTrevorPosition			// Trevor's position on the roof
	
	//	First Stage enemies
	PED_STRUCT		m_psFirstAreaCIA[iCONST_NUM_FIRST_AREA_CIA]
	PED_STRUCT		m_psFirstAreaFIB[iCONST_NUM_FIRST_AREA_FIB]
	PED_STRUCT		m_psFirstAreaMW[iCONST_NUM_FIRST_AREA_MW]
	
	//	On Stairs Enemies
	PED_STRUCT 		m_psStairsMW[iCONST_NUM_STAIRS_MW]
	PED_STRUCT 		m_psStairsFIB[2]
	
	
	//	Michael/Trevor Switch Stage Enemies
	PED_STRUCT		m_psTrevorSaveDaveFIB[iCONST_NUM_TREVOR_DAVE_FIB]
	
	//	Dave at the Fountain Stage Enemies
	PED_STRUCT		m_psFIBIG_6[3]							// FIB guys that play mocap MIC_3_IG_6
	PED_STRUCT		m_psMichaelSecondWave[2]				// FIB guys that play mocap MIC_3_IG_7 from a different monster closet, only if you're playing as Trevor
	PED_STRUCT		m_psMichaelThirdWave[3]					// Merryweather guys that attack Michael
	PED_STRUCT		m_psMWIG_7								// Merryweather guys that play mocap MIC_3_IG_7
	PED_STRUCT		m_psMWIG_8[5]							// Merryweather guys that play mocap MIC_3_IG_8
	
	//	Enemies that Michael fights on the walkways making his way to Dave
	PED_STRUCT		m_psWalkwayCIA1[2]
	PED_STRUCT		m_psWalkwayFIB2[3]
	PED_STRUCT		m_psWalkwayMW2[2]
	PED_STRUCT		m_psWalkwayMW3[2]
	PED_STRUCT		m_psStairwayMW[1]
	
	//	Wave enemies that are spawned to keep the war going in the courtyard
	PED_STRUCT		m_psWarFIB[3]
	PED_STRUCT		m_psWarCIA[3]
	PED_STRUCT		m_psWarMWCourtyard[3]
	
	//  CIA peds at the top of the walkway
	PED_STRUCT		m_psFinalWalkwayCIA[2]
	
	//	Escaping the Museum
	PED_STRUCT		m_psTrevorOnlyMW1[iCONST_NUM_ESCAPE_MW1]
	PED_STRUCT		m_psTrevorOnlyMW2[iCONST_NUM_ESCAPE_MW2]
	PED_STRUCT		m_psTrevorOnlyMW3[iCONST_NUM_ESCAPE_MW3]
	PED_STRUCT		m_psTrevorOnlyMW4[iCONST_NUM_ESCAPE_MW4]
	PED_STRUCT		m_psTrevorOnlyMW5[iCONST_NUM_ESCAPE_MW5]
	PED_STRUCT		m_psTrevorDangerPeds[3]
	
	//	Empty ped struct used for unnecessary peds in Dave Progression Checks
	PED_STRUCT		m_psEMPTY[1]

	COVER_STRUCT	csIG_8CoverPoints[5]
	COVER_STRUCT	csIG_6CoverPoints[2]

	TRIGGER_BOX		m_tbLargeRoofVolume
	TRIGGER_BOX		m_tbDefensiveAreaTrevorOnRoof
	TRIGGER_BOX		m_tbDefensiveAreaAtFountain
	TRIGGER_BOX		m_tbMichaelTaskZone1
	TRIGGER_BOX		m_tbMichaelTaskZone2
	TRIGGER_BOX		m_tbMichaelTaskZone3
	TRIGGER_BOX		m_tbAbandonFailFirstArea1
	TRIGGER_BOX		m_tbAbandonFailFirstArea2
	TRIGGER_BOX		m_tbAbandonFailOnStairs
	TRIGGER_BOX		m_tbFailKillVolume

	TRIGGER_BOX		m_tbDefensiveAreaTrevorDangerPeds

	TRIGGER_BOX		m_tbRoofObjective
	TRIGGER_BOX		m_tbParkingLotObjective
	
	// Trevor-Only spawns if he runs down the stairs
	TRIGGER_BOX			m_tbSpawnTrevorOnlyMW1
	TRIGGER_BOX			m_tbSpawnTrevorOnlyMW2
	TRIGGER_BOX			m_tbSpawnTrevorOnlyMW3
	TRIGGER_BOX			m_tbSpawnTrevorOnlyMW4
	TRIGGER_BOX			m_tbSpawnTrevorOnlyMW5

	// Defensive Areas for the final fight
	TRIGGER_BOX			m_tbDefensiveAreaFinalFightMW
	TRIGGER_BOX			m_tbDefensiveAreaFinalFightFIB

	TRIGGER_BOX			m_tbDefensiveAreaFIBClose
	TRIGGER_BOX			m_tbDefensiveAreaMWClose
//	TRIGGER_BOX			m_tbDefensiveAreaClosest
	TRIGGER_BOX			m_tbDefensiveAreaLargerFountainArea


	//	Defensive Areas for the War peds
	//TRIGGER_BOX		m_tbDefensiveAreaWarCIA
	TRIGGER_BOX		m_tbDefensiveAreaWarFIB
	TRIGGER_BOX		m_tbDefensiveAreaWarMWCourtyard
	VECTOR 			m_vecFIBSpawnPoints[6]
	//VECTOR 			m_vecCIASpawnPoints[6]
	VECTOR 			m_vecMWCourtSpawnPoints[6]
	VECTOR			m_vHatedTargetPositionForAI = <<-2248.15405, 269.91110, 173.60196>>

	ENEMY_WAVE 		m_EnemyWaveFIB
	ENEMY_WAVE 		m_EnemyWaveCIA
	ENEMY_WAVE 		m_EnemyWaveMWCourt
	
	BOOL			m_bFirstSwitch
	BOOL			m_bCheckFailureKillVolume
	BOOL			m_bLastParkingLotGuyTasked
	
	BOOL			m_bSpawnTrevorOnlyMW1
	BOOL			m_bSpawnTrevorOnlyMW2
	BOOL			m_bSpawnTrevorOnlyMW3
	BOOL			m_bSpawnTrevorOnlyMW4
	BOOL			m_bSpawnTrevorOnlyMW5
	BOOL			bSeriouslyDontDoSwitchUIShitAnymore
	BOOL			bSafeToTaskMichaelFIBEnemies
	
	PED_STRUCT					pedCurrentTrevTarget

	COVER_STRUCT				csFirstAreaIPLCover[2]

	COVER_STRUCT				csMichaelMIC3_INTCover
	COVER_STRUCT				csMichaelStartingCover
	COVER_STRUCT				csDaveBottomOfStairsCover
	
	COVER_STRUCT				csPlayerFountainCover
	COVER_STRUCT				csDaveFountainCover
	COVER_STRUCT				csOtherPlayerFountainCover
	COVER_STRUCT				csPlayerParkingLotCover
	
	COVER_STRUCT				covTrevorOnlyMW1
	COVER_STRUCT				csTrevorCoverSpot
	COVER_STRUCT				m_csFIBCover[iCONST_NUM_TREVOR_DAVE_FIB]
	
	//FIRE_INDEX					m_StairFires[13]
	PTFX_ID						m_StairFire
	
	PLAYER_SWITCH_STATE 		ePlayerSwitchState
	
	WEAPON_TYPE					wtMichaelPreviousWeapon
	WEAPON_TYPE					wtTrevorPreviousWeapon
	
	VEHICLE_INDEX				m_vehPlayerStartingCar
	OBJECT_INDEX				m_objCourtyardCover[iCONST_NUM_COURTYARD_COVER]
	
	OBJECT_INDEX				m_michaelsGun
	
	// Timer to block camera movement for the replay editor B*2226230
	INT							iBlockReplayCameraTimer
	
/*
												 _     _  _______  _        ______  _______  ______    ______ 
												(_)   (_)(_______)(_)      (_____ \(_______)(_____ \  / _____)
												 _______  _____    _        _____) )_____    _____) )( (____  
												|  ___  ||  ___)  | |      |  ____/|  ___)  |  __  /  \____ \ 
												| |   | || |_____ | |_____ | |     | |_____ | |  \ \  _____) )
												|_|   |_||_______)|_______)|_|     |_______)|_|   |_|(______/ 
*/

#IF IS_DEBUG_BUILD

PROC DRAW_DEBUG_TEXT_2D_IN_SLOT( INT &iSlot, STRING strString )

	IF bDebugEnabled

		CONST_FLOAT		fCONST_DEBUG_X_INDENT			0.05
		CONST_FLOAT		fCONST_DEBUG_Y_INDENT			0.05
		CONST_FLOAT		fCONST_DEBUG_Y_SPACING			0.015

		CONST_INT 		iCONST_DEBUG_TEXT_COLOUR_R 		255
		CONST_INT 		iCONST_DEBUG_TEXT_COLOUR_G 		255
		CONST_INT 		iCONST_DEBUG_TEXT_COLOUR_B 		0
		CONST_INT 		iCONST_DEBUG_TEXT_COLOUR_A 		255

		DRAW_DEBUG_TEXT_2D( strString, 
				<<fCONST_DEBUG_X_INDENT, fCONST_DEBUG_Y_INDENT + ( fCONST_DEBUG_Y_SPACING * iSlot ),0>>, 
				iCONST_DEBUG_TEXT_COLOUR_R, iCONST_DEBUG_TEXT_COLOUR_G, iCONST_DEBUG_TEXT_COLOUR_B, iCONST_DEBUG_TEXT_COLOUR_A )
		
		iSlot++
	
	ENDIF

ENDPROC

#ENDIF

//Cuts down on vehicle/ ped population budgets for the cutscene
PROC REDUCE_AMBIENT_MODELS(BOOL bTurnOn)
	IF bTurnOn
		CPRINTLN(DEBUG_MISSION, "REDUCE_AMBIENT_MODELS: Setting ON")
		SET_VEHICLE_POPULATION_BUDGET(0) 
		SET_PED_POPULATION_BUDGET(0)
	ELSE
		CPRINTLN(DEBUG_MISSION, "REDUCE_AMBIENT_MODELS: Setting OFF")
		SET_VEHICLE_POPULATION_BUDGET(3) 
		SET_PED_POPULATION_BUDGET(3)
	ENDIF

ENDPROC

//PURPOSE: Returns Michael's ped index
FUNC PED_INDEX MIKE_PED_ID()
	return peds[mpf_mike].id
ENDFUNC

//PURPOSE: Returns Franklin's ped index
FUNC PED_INDEX FRANK_PED_ID()
	return peds[mpf_frank].id
ENDFUNC

//PURPOSE: Returns Trevor's ped index
FUNC PED_INDEX TREV_PED_ID()
	return peds[mpf_trev].id
ENDFUNC

//PURPOSE: Safely open the main sequence
PROC SAFE_OPEN_SEQUENCE(BOOL bClearTask = TRUE)
	IF bClearTask
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
	IF NOT bSequenceOpen
		OPEN_SEQUENCE_TASK(seq)
		bSequenceOpen = TRUE
	ENDIF
ENDPROC

//PURPOSE: Safely close the main sequence
PROC SAFE_CLOSE_SEQUENCE()
	IF bSequenceOpen
		CLOSE_SEQUENCE_TASK(seq)
		bSequenceOpen = FALSE
	ENDIF
ENDPROC

//PURPOSE: Makes sure a sequence is closed before using it. Also auto cleans the seqence if required
PROC SAFE_PERFORM_SEQUENCE(PED_INDEX ped, BOOL bCleanupSequence = TRUE)
	SAFE_CLOSE_SEQUENCE()
	IF DOES_ENTITY_EXIST(ped)
		CLEAR_PED_TASKS(ped)
		TASK_PERFORM_SEQUENCE(ped, seq)
		IF bCleanupSequence
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Safely remove blip
PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blip)
	IF DOES_BLIP_EXIST(blip)
		REMOVE_BLIP(blip)
	ENDIF
ENDPROC

//PURPOSE: Safely blip a coordindate
PROC SAFE_BLIP_COORD(BLIP_INDEX &blip, VECTOR vec, BOOL bShowRoute = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_COORD(vec, bShowRoute)
ENDPROC

//PURPOSE: Safely blip a ped
PROC SAFE_BLIP_PED(BLIP_INDEX &blip, PED_INDEX ped, BOOL bIsEnemy = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_PED(ped, bIsEnemy)
ENDPROC

//PURPOSE: Safely blip a vehicle
PROC SAFE_BLIP_VEHICLE(BLIP_INDEX &blip, VEHICLE_INDEX veh, BOOL bIsEnemy = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_VEHICLE(veh, bIsEnemy)
ENDPROC

//PURPOSE: Safely blip an object
PROC SAFE_BLIP_OBJECT(BLIP_INDEX &blip, OBJECT_INDEX objID)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_OBJECT(objID)
ENDPROC

//PURPOSE: Safely delete a vehcile and any peds contained by it.
PROC SAFE_DELETE_VEHICLE(VEHICLE_INDEX vehicle)
	IF DOES_ENTITY_EXIST(vehicle)
		VEHICLE_SEAT seat
		FOR seat = VS_DRIVER to VS_BACK_RIGHT
			PED_INDEX ped = GET_PED_IN_VEHICLE_SEAT(vehicle, seat)
			IF DOES_ENTITY_EXIST(ped)
			AND IS_ENTITY_A_MISSION_ENTITY(ped)
				DELETE_PED(ped)
			ENDIF
		ENDFOR
		DELETE_VEHICLE(vehicle)
	ENDIF
ENDPROC

//PURPOSE: Safely clears up a cover point
PROC SAFE_REMOVE_COVERPOINT(COVER_STRUCT &cov)
	IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(cov.pos) 
	AND cov.bDoesCovExist 
	AND cov.id != null
		REMOVE_COVER_POINT(cov.id)
	ENDIF
	cov.bDoesCovExist = FALSE
	cov.pos = <<0,0,0>>
	cov.dir = 0.0
ENDPROC

//PURPOSE: creates a prop and marks it as a custom one, so that it will be cleared up at the end of the script
PROC Create_Prop(OBJ_STRUCT &obj, MODEL_NAMES model, VECTOR pos, FLOAT heading)
	obj.id = CREATE_OBJECT(model, pos)
	SET_ENTITY_HEADING(obj.id, heading)
	obj.bCustom = TRUE
ENDPROC

//PURPOSE: Checks if a world point lies within a given screen area
FUNC BOOL IS_POINT_IN_SCREEN_AREA(vector vWorldCoord, float x1, float y1, float x2, float y2)
      float sX, sY
      GET_SCREEN_COORD_FROM_WORLD_COORD(vWorldCoord,sX,sY)
      if sX>= x1 and sX<=x2
            if sY>=y1  and sy<=y2
                  return TRUE
            ENDIF
      ENDIF
      return FALSE
ENDFUNC

/// PURPOSE: Sets a ped to ditch their defensive areas when reached, and to use charging behaviors
PROC SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(PED_INDEX &ped)
	IF NOT IS_ENTITY_DEAD(ped)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
		REMOVE_PED_DEFENSIVE_AREA(ped)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_CAN_CHARGE, TRUE)
		SET_PED_COMBAT_MOVEMENT(ped, CM_WILLADVANCE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the initial scene and sets up the museum
/// PARAMS:
///    bGrabFromTriggerScene - 
PROC CREATE_INITIAL_SCENE(BOOL bGrabFromTriggerScene)

	IF bGrabFromTriggerScene
	
		// Dave Norton
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			m_pedAllyDave = g_sTriggerSceneAssets.ped[0]
			SET_ENTITY_AS_MISSION_ENTITY(m_pedAllyDave, TRUE, TRUE)
		ENDIF
		
		// Daves Newspaper
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
			objs[mof_daves_paper].id = g_sTriggerSceneAssets.object[0]
			SET_ENTITY_AS_MISSION_ENTITY(objs[mof_daves_paper].id, TRUE, TRUE)
		ENDIF
		
		// Trevors Heli
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			vehs[mvf_trev_heli].id = g_sTriggerSceneAssets.veh[0]
			SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_trev_heli].id, TRUE, TRUE)
		ENDIF
		
		// Large blocking object
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
			objs[mof_large_blocker].id = g_sTriggerSceneAssets.object[1]
			SET_ENTITY_AS_MISSION_ENTITY(objs[mof_large_blocker].id, TRUE, TRUE)
		ENDIF
		
		// Small blocking objects
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[2])
			objs[mof_small_blocker_start_right].id = g_sTriggerSceneAssets.object[2]
			SET_ENTITY_AS_MISSION_ENTITY(objs[mof_small_blocker_start_right].id, TRUE, TRUE)
		ENDIF
		
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_MICHAEL_3)
	
	ENDIF

ENDPROC

//PURPOSE: Sets the gameplay camera'heading relative to the world coordinate system (not relative to the players local heading)
PROC SET_GAMEPLAY_CAM_WORLD_HEADING(FLOAT fHeading = 0.0, PED_INDEX ped = NULL)
	IF NOT DOES_ENTITY_EXIST(ped)
		ped = PLAYER_PED_ID()
	ENDIF
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading - GET_ENTITY_HEADING(ped))
ENDPROC

//PURPOSE: Returns the world heading of the gameplay camera.
FUNC FLOAT GET_GAMEPLAY_CAM_WORLD_HEADING(PED_INDEX ped = null)
	IF NOT DOES_ENTITY_EXIST(ped)
		ped = PLAYER_PED_ID()
	ENDIF
	RETURN WRAP(GET_GAMEPLAY_CAM_RELATIVE_HEADING() + GET_ENTITY_HEADING(ped),-180,180)
ENDFUNC

//PURPOSE: Points the gameplay cam heading at a target coordinate given in wolrd coords
PROC SET_GAMEPLAY_CAM_LOOK_AT_WORLD_TARGET(VECTOR vTarget, PED_INDEX ped = null)
	IF NOT DOES_ENTITY_EXIST(ped)
		ped = PLAYER_PED_ID()
	ENDIF
	VECTOR vLookAt = vTarget - GET_ENTITY_COORDS(ped)
	SET_GAMEPLAY_CAM_WORLD_HEADING(GET_HEADING_FROM_VECTOR_2D(vLookAt.x, vLookAt.y), ped)
ENDPROC

//PURPOSE: Smoothly transition time scale
FUNC BOOL Smooth_Time_Scale_Transition(FLOAT &fCurrentTimeScale, FLOAT fStartTimeScale, FLOAT fDesiredTimeScale, INT iTimeStamp, INT iDuration)
	FLOAT fAlpha = CLAMP((GET_GAME_TIMER() - iTimeStamp)/ TO_FLOAT(ABSI(iDuration)), 0.0, 1.0)
	fCurrentTimeScale = LERP_FLOAT(fStartTimeScale, fDesiredTimeScale, fAlpha)
	SET_TIME_SCALE(fCurrentTimeScale)
	
	IF fAlpha = 1.0 OR fCurrentTimeScale = fDesiredTimeScale
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PURPOSE: Check the entity is facing a certain heading within a tolerance
FUNC BOOL IS_ENTITY_ROUGHLY_FACING_THIS_DIRECTION(ENTITY_INDEX id, FLOAT idealHeading, FLOAT acceptableRange = 30.0)
	FLOAT upperLimit, lowerLimit
	upperLimit = idealHeading + (acceptableRange/2)
	IF upperLimit > 360
		upperLimit -= 360.0
	ENDIF
	
	lowerLimit = idealHeading - (acceptableRange/2)
	IF lowerLimit < 0
		lowerLimit += 360.0
	ENDIF
	
	IF DOES_ENTITY_EXIST(id)
		IF upperLimit > lowerLimit
			IF GET_ENTITY_HEADING(id) < upperLimit
			AND GET_ENTITY_HEADING(id) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF GET_ENTITY_HEADING(id) < upperLimit
			OR GET_ENTITY_HEADING(id) > lowerLimit
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	 
	RETURN FALSE
ENDFUNC

//PURPOSE: Finds if a player character has just taken out one of the enemies
FUNC BOOL HasPlayerJustKilledEnemy()
	INT i 
	REPEAT COUNT_OF(peds) i
		IF peds[i].bKilledByThePlayer
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC



//PURPOSE:
FUNC BOOL Set_Vehicle_Playback_Speed(VEH_STRUCT &veh, FLOAT fNewSpeed, INT iDuration = 0)
	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh.id)
		SCRIPT_ASSERT("Playback not ")
		RETURN TRUE
	ENDIF
	
	// Must be starting a new 
	IF veh.fDesiredSpeed != fNewSpeed
	OR veh.iDesiredDuration != iDuration
		veh.iSpeedTimeStamp = GET_GAME_TIMER()
		veh.fDesiredSpeed = fNewSpeed
		veh.fStartSpeed = veh.fCurrentSpeed
		veh.iDesiredDuration = iDuration
	ENDIF
	
	IF veh.fCurrentSpeed = veh.fDesiredSpeed
		RETURN TRUE	// already equal, return true
	ELSE
		// Calculate the speed
		FLOAT fSpeedChange = ABSF(veh.fDesiredSpeed - veh.fStartSpeed) * CLAMP((GET_GAME_TIMER() - veh.iSpeedTimeStamp)/TO_FLOAT(veh.iDesiredDuration), 0.0, 1.0)
		
		IF veh.fDesiredSpeed < veh.fStartSpeed
			veh.fCurrentSpeed = CLAMP(veh.fCurrentSpeed - fSpeedChange, veh.fDesiredSpeed, veh.fStartSpeed)
			SET_PLAYBACK_SPEED(veh.id, veh.fCurrentSpeed)
		ELSE
			veh.fCurrentSpeed = CLAMP(veh.fCurrentSpeed + fSpeedChange, veh.fStartSpeed, veh.fDesiredSpeed)
			SET_PLAYBACK_SPEED(veh.id, veh.fCurrentSpeed)
		ENDIF
		
		CDEBUG1LN(DEBUG_MISSION, "******** PLAYBACK SPEED CONVERGING: StartSpeed: ", veh.fStartSpeed," DesiredSpeed: ", veh.fDesiredSpeed," CurrentSpeed: ", veh.fCurrentSpeed)
		IF veh.fCurrentSpeed != fNewSpeed
			RETURN FALSE
		ELSE
			CDEBUG1LN(DEBUG_MISSION, "******** PLAYBACK SPEED COMPLETE! *********")
			RETURN TRUE
		ENDIF
	ENDIF
ENDFUNC

/*
	   ______                                        __  ___                                                           __ 
	  / ____/____ _ ____ ___   ___   _____ ____ _   /  |/  /____ _ ____   ____ _ ____ _ ___   ____ ___   ___   ____   / /_
	 / /    / __ `// __ `__ \ / _ \ / ___// __ `/  / /|_/ // __ `// __ \ / __ `// __ `// _ \ / __ `__ \ / _ \ / __ \ / __/
	/ /___ / /_/ // / / / / //  __// /   / /_/ /  / /  / // /_/ // / / // /_/ // /_/ //  __// / / / / //  __// / / // /_  
	\____/ \__,_//_/ /_/ /_/ \___//_/    \__,_/  /_/  /_/ \__,_//_/ /_/ \__,_/ \__, / \___//_/ /_/ /_/ \___//_/ /_/ \__/  
	                                                                          /____/ 
*/

//PURPOSE:
PROC Interpolate_Camera(CAM_STRUCT &cam, VECTOR vDestCoord, INT iDuration)
	IF DOES_CAM_EXIST(cam.id)
	AND NOT IS_CAM_INTERPOLATING(cam.id)
		cam.vStartPos 			= GET_CAM_COORD(cam.id)
		cam.vEndPos 			= vDestCoord
		cam.iInterpDuration 	= iDuration
		cam.iTimeStamp 			= GET_GAME_TIMER()
		cam.bCustomInterp 		= TRUE
	ENDIF
ENDPROC

//PURPOSE: Manages the custom camera interpolation system to allow for slow-mo cameras to work with the time scale.
PROC Manage_Cameras()
	INT x
	REPEAT COUNT_OF(cams) x
		IF DOES_CAM_EXIST(cams[x].id)				// exists
		AND (NOT IS_CAM_INTERPOLATING(cams[x].id))		// is not code interping
		AND cams[x].bCustomInterp					// marked for script interp
			FLOAT fAlpha = CLAMP((GET_GAME_TIMER() - cams[x].iTimeStamp) / TO_FLOAT(cams[x].iInterpDuration), 0.0, 1.0)
			
			VECTOR vNewPos
			vNewPos = LERP_VECTOR(cams[x].vStartPos, cams[x].vEndPos, fAlpha)
			SET_CAM_COORD(cams[x].id, vNewPos)
			
			IF fAlpha = 1.0
			OR ARE_VECTORS_EQUAL(cams[x].vEndPos, vNewPos)
				cams[x].bCustomInterp = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Time in ms since last conversation was started
FUNC INT Get_Time_Since_Last_Conversation()
	RETURN GET_GAME_TIMER() - i_dialogue_timer
ENDFUNC

//PURPOSE: Plays the specified conversation
FUNC BOOL Play_Conversation(STRING rootLabel)
	IF IS_SAFE_TO_START_CONVERSATION()
		IF CREATE_CONVERSATION(sConvo, str_dialogue, rootLabel, CONV_PRIORITY_HIGH)
			i_dialogue_timer 		= GET_GAME_TIMER()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Play_Warcry(STRING rootLabel)
	IF IS_SAFE_TO_START_CONVERSATION()
		IF CREATE_CONVERSATION(sConvo, str_dialogue, rootLabel, CONV_PRIORITY_MEDIUM)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


PROC REMOVE_ALL_SCRIPT_FIRES()
//	INT i
//	FOR i = 0 TO COUNT_OF(m_StairFires) - 1
//		REMOVE_SCRIPT_FIRE(m_StairFires[i])
//	ENDFOR
	IF DOES_PARTICLE_FX_LOOPED_EXIST( m_StairFire )
		STOP_PARTICLE_FX_LOOPED( m_StairFire )
	ENDIF
ENDPROC

PROC BLOCK_STAIRWAY_PATH()
//	INT iNumGenerations = 5
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2195.615723,225.797516,182.795273>>, <<-2192.232910,218.546585,191.020248>>, 1.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2193.159668,223.607300,183.595764>>, <<-2191.268066,218.976242,188.495193>>, 4.500000)
		START_ENTITY_FIRE(PLAYER_PED_ID())
		SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
	ENDIF
	
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( m_StairFire )
		REQUEST_PTFX_ASSET()
		IF HAS_PTFX_ASSET_LOADED()
			m_StairFire = START_PARTICLE_FX_LOOPED_AT_COORD( "scr_mich3_heli_fire", <<-2193.47, 220.48, 183.60>>, <<0,0,0>>, 1.0 )
		ENDIF
	ENDIF
	
ENDPROC

structTimer	tmrHeliStart
INT		iBlockingPathState = 0
FLOAT	flWidgetTimeToStartHeli = 1.0
FLOAT	flWidgetTimeToSkipInHeliRecording = 2000.0
FLOAT	flWidgetRecordingSpeed = 0.75
FLOAT	flDistanceToBlowUpHeli = 5.0
VECTOR	vWidgetExplosionPosition = <<-2196.37085, 216.70497, 186.63254>>
//OBJECT_INDEX objHiddenCollision

PROC UPDATE_HELICOPTER_CRASH()


	SWITCH iBlockingPathState
		CASE 0 
			RESTART_TIMER_NOW(tmrHeliStart)
			iBlockingPathState++
		BREAK
		
		CASE 1
			IF GET_TIMER_IN_SECONDS(tmrHeliStart) >= flWidgetTimeToStartHeli
				iBlockReplayCameraTimer = GET_GAME_TIMER() + 3000 // For B*2226230
				vehs[mvf_crash_heli].id = CREATE_VEHICLE(mod_heli_mw, (<<-2251.52515, 325.16736, 191.61157>>), 121)
				IF IS_ENTITY_OK(vehs[mvf_crash_heli].id)
					SET_HELI_BLADES_FULL_SPEED(vehs[mvf_crash_heli].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_crash_heli].id, rec_crash_heli, str_carrecs)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_crash_heli].id, flWidgetTimeToSkipInHeliRecording)
					SET_PLAYBACK_SPEED(vehs[mvf_crash_heli].id, flWidgetRecordingSpeed)
					iBlockingPathState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehs[mvf_crash_heli].id, vWidgetExplosionPosition) <= flDistanceToBlowUpHeli
				EXPLODE_VEHICLE(vehs[mvf_crash_heli].id, TRUE, TRUE)
				SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.1)
				ADD_EXPLOSION(vWidgetExplosionPosition, EXP_TAG_TANKSHELL)
//				SET_ENTITY_COORDS(m_pedAllyDave, (<<-2205.52368, 215.14833, 183.60187>>))
				CLEAR_PED_TASKS(m_pedAllyDave)
				REMOVE_PED_DEFENSIVE_AREA(m_pedAllyDave)
				SET_PED_SPHERE_DEFENSIVE_AREA(m_pedAllyDave, m_flagDaveCoverPositions[1].pos, 1.0, TRUE)
				OPEN_SEQUENCE_TASK(seq)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_SEEK_COVER_TO_COORDS(NULL, m_flagDaveCoverPositions[1].pos, (<<-2210.18311, 222.04420, 178.61201>>), -1)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(m_pedAllyDave, seq)
				CLEAR_SEQUENCE_TASK(seq)
				m_bDoStairFires = TRUE
				iBlockingPathState++
			ENDIF
		BREAK
		
//		CASE 3
//			objHiddenCollision = CREATE_OBJECT(P_GDOOR1COLOBJECT_S, <<-2193.36987, 221.10823, 183.60187>>)
//			SET_ENTITY_ROTATION(objHiddenCollision, (<<0,0, 116.12>>))
//			SET_ENTITY_VISIBLE(objHiddenCollision, FALSE)
//			RESTART_TIMER_NOW(tmrHeliStart)
//			iBlockingPathState++
//		BREAK
//		
//		CASE 4
//			IF GET_TIMER_IN_SECONDS(tmrHeliStart) >= 2.0
//				DELETE_OBJECT(objHiddenCollision)
//				SET_MODEL_AS_NO_LONGER_NEEDED(P_GDOOR1COLOBJECT_S)
//				m_bDoStairFires = TRUE
//				iBlockingPathState++
//			ENDIF
//		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Creates a specified crew member, can be either a player crew member or a hired crew member.
FUNC BOOL Create_Player_Ped(PED_STRUCT &ped, SELECTOR_SLOTS_ENUM selectPed, VECTOR vSpawnCoord, FLOAT fSpawnHeading, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, INT iAccuracy = 20, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE, BOOL bBlip = FALSE, INT iAmmo = 60)
	IF NOT DOES_ENTITY_EXIST(ped.id)
	
		MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
		
		enumCharacterList eChar
		IF selectPed = SELECTOR_PED_MULTIPLAYER
			SCRIPT_ASSERT("INVALID SELECTOR PED: MULTIPLAYER")
		ENDIF
		
		INT 			iDialogueID
		TEXT_LABEL_15	strPedName
		
		// Selector ped crew member
		SWITCH selectPed
			CASE SELECTOR_PED_MICHAEL	eChar = CHAR_MICHAEL	iDialogueID = 0		strPedName = "MICHAEL"	BREAK
			CASE SELECTOR_PED_FRANKLIN	eChar = CHAR_FRANKLIN	iDialogueID = 1		strPedName = "FRANKLIN"	BREAK
			CASE SELECTOR_PED_TREVOR	eChar = CHAR_TREVOR		iDialogueID = 2		strPedName = "TREVOR"	BREAK
		ENDSWITCH
			
		IF bWait
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(ped.id, eChar, vSpawnCoord, fSpawnHeading, bCleanUpModel)
				WAIT(0)
			ENDWHILE
		ELSE
			CREATE_PLAYER_PED_ON_FOOT(ped.id, eChar, vSpawnCoord, fSpawnHeading, bCleanUpModel)
		ENDIF
		
		
		// Ped has been created now, set up relationships and stuff
		IF DOES_ENTITY_EXIST(ped.id)
			IF bBlip
				IF DOES_BLIP_EXIST(ped.blip)
					REMOVE_BLIP(ped.blip)
				ENDIF
				SAFE_BLIP_PED(ped.blip, ped.id, FALSE)
			ENDIF
			SET_PED_TO_LOAD_COVER(ped.id, TRUE)
			SET_PED_ACCURACY(ped.id, iAccuracy)
			SET_PED_RELATIONSHIP_GROUP_HASH(ped.id, REL_FRIEND)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id, TRUE)
			SET_PED_CAN_BE_TARGETTED(ped.id, false)
			SET_PED_SUFFERS_CRITICAL_HITS(ped.id, FALSE)
			SET_ENTITY_HEALTH(ped.id, 800)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped.id, FALSE)
			SET_RAGDOLL_BLOCKING_FLAGS(ped.id, RBF_BULLET_IMPACT)
			SET_PED_VISUAL_FIELD_PROPERTIES(ped.id, 200, 200, 180, -180, 180)
			REMOVE_PED_FOR_DIALOGUE(sConvo, iDialogueID)
			ADD_PED_FOR_DIALOGUE(sConvo, iDialogueID, ped.id, strPedName, FALSE)
			// Has been given a valid weapon
			IF weapon != WEAPONTYPE_INVALID
				IF iAmmo <= 0
					GIVE_WEAPON_TO_PED(ped.id, weapon, INFINITE_AMMO, TRUE, TRUE)
				ELSE
					GIVE_WEAPON_TO_PED(ped.id, weapon, iAmmo, TRUE, TRUE)
				ENDIF
			// if not look for the best they currently have
			ELSE
				WEAPON_TYPE bestWeapon = GET_BEST_PED_WEAPON(ped.id)
				
				IF bestWeapon != WEAPONTYPE_UNARMED
				AND bestWeapon != WEAPONTYPE_BAT
				AND bestWeapon != WEAPONTYPE_CROWBAR
				AND bestWeapon != WEAPONTYPE_FIREEXTINGUISHER
				AND bestWeapon != WEAPONTYPE_HAMMER
//				AND bestWeapon != WEAPONTYPE_LASSO
				//AND bestWeapon != WEAPONTYPE_DLC_LOUDHAILER
				AND bestWeapon != WEAPONTYPE_NIGHTSTICK
				AND bestWeapon != WEAPONTYPE_PETROLCAN
				AND bestWeapon != WEAPONTYPE_STUNGUN
				AND bestWeapon != WEAPONTYPE_DLC_BOTTLE
					SET_CURRENT_PED_WEAPON(ped.id, bestWeapon, TRUE)
				ELSE
					GIVE_WEAPON_TO_PED(ped.id, WEAPONTYPE_PISTOL, 60, TRUE, TRUE)
				ENDIF
			ENDIF
			
			// Clean up model if used
			IF bCleanUpModel
			AND model != DUMMY_MODEL_FOR_SCRIPT
				Unload_Asset_Model(sAssetData, model)
			ENDIF
			
			RETURN TRUE
		
		// Not yet created still waiting
		ELSE
			RETURN FALSE
		ENDIF
				
	// Entity already exists
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

//PURPOSE: Sets the currently selected selector ped
FUNC BOOL Set_Current_Player_Ped(SELECTOR_SLOTS_ENUM pedChar, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE)
	// SELECTOR_PED_MULTIPLAYER NOT ALLOWED!
	IF pedChar = SELECTOR_PED_MULTIPLAYER SCRIPT_ASSERT("INVALID SELECTOR PED: Cannot select SELECTOR_PED_MULTIPLAYER") RETURN FALSE ENDIF
	
	IF bWait
		WHILE NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)	
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)
			RETURN FALSE
		ENDIF
	ENDIF
			
	// Additional stuff to set up ped for use in mission
	SWITCH pedChar
		CASE SELECTOR_PED_MICHAEL
			peds[mpf_mike].id = PLAYER_PED_ID()
			REMOVE_PED_FOR_DIALOGUE(sConvo, 0)
			ADD_PED_FOR_DIALOGUE(sConvo, 0, MIKE_PED_ID(), "MICHAEL")
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			peds[mpf_frank].id = PLAYER_PED_ID()
			REMOVE_PED_FOR_DIALOGUE(sConvo, 1)
			ADD_PED_FOR_DIALOGUE(sConvo, 1, FRANK_PED_ID(), "FRANKLIN")
		BREAK
		CASE SELECTOR_PED_TREVOR
			peds[mpf_trev].id = PLAYER_PED_ID()
			REMOVE_PED_FOR_DIALOGUE(sConvo, 2)
			ADD_PED_FOR_DIALOGUE(sConvo, 2, TREV_PED_ID(), "TREVOR")
		BREAK
	ENDSWITCH
	
	IF sSelectorPeds.ePreviousSelectorPed != pedChar
		MISSION_PED_FLAGS pedFlag
		SWITCH sSelectorPeds.ePreviousSelectorPed
			CASE SELECTOR_PED_MICHAEL
				pedFlag = mpf_mike
			BREAK
			CASE SELECTOR_PED_FRANKLIN
				pedFlag = mpf_frank
			BREAK
			CASE SELECTOR_PED_TREVOR
				pedFlag = mpf_trev
			BREAK
		ENDSWITCH
		peds[pedFlag].id = NULL
		SAFE_REMOVE_BLIP(peds[pedFlag].blip)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC Set_Mission_Ped_Params(PED_STRUCT &ped, STRING pedDebugName, REL_GROUP_HASH relGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, INT accuracy = 30, INT armour = 0, BOOL bBlip = FALSE, ITEMSET_INDEX preferredCover = NULL)
	// Debug name
	IF NOT IS_STRING_NULL_OR_EMPTY(pedDebugName)		SET_PED_NAME_DEBUG(ped.id, pedDebugName)					ENDIF
	// Relationship group
	IF ped.id != PLAYER_PED_ID()
		SET_PED_RELATIONSHIP_GROUP_HASH(ped.id, relGroup)
	ENDIF
	// Weapon
	IF weapon != WEAPONTYPE_UNARMED						GIVE_WEAPON_TO_PED(ped.id , weapon, INFINITE_AMMO, true)	ENDIF
	// Blip
	IF bBlip = TRUE
		IF relGroup = REL_FIB OR relGroup = REL_MW	OR relGroup = REL_CIA OR relGroup = REL_MW_FINAL OR relGroup = REL_FIB_FINAL OR relGroup = REL_CIA_FINAL
			SAFE_BLIP_PED(ped.blip, ped.id, TRUE)
		ELSE
			SAFE_BLIP_PED(ped.blip, ped.id, FALSE)						
		ENDIF
	ENDIF
	// Preferred cover set
	IF preferredCover != NULL							SET_PED_PREFERRED_COVER_SET(ped.id, preferredCover)			ENDIF
	// Armour
	INT currentArmour = GET_PED_ARMOUR(ped.id) 
	IF currentArmour != armour
		ADD_ARMOUR_TO_PED(ped.id, armour - currentArmour)
	ENDIF
	// Other arributes
	SET_PED_MONEY(ped.id, 0)
	SET_PED_ACCURACY(ped.id, accuracy)
	SET_PED_KEEP_TASK(ped.id, TRUE)
	SET_PED_CONFIG_FLAG(ped.id , PCF_RunFromFiresAndExplosions, FALSE)
	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id , true)
	SET_PED_USING_ACTION_MODE(ped.id, TRUE, -1, "DEFAULT_ACTION")
ENDPROC

// Safely creates a ped in the specified ped index
PROC Create_Mission_Ped(PED_STRUCT &ped, MODEL_NAMES model, VECTOR position, FLOAT heading, STRING pedDebugName, REL_GROUP_HASH relGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, INT accuracy = 10, INT armour = 0, BOOL bBlip = FALSE, ITEMSET_INDEX preferredCover = NULL)
	
	// Safely create the ped
	IF DOES_ENTITY_EXIST(ped.id)
		SET_PED_AS_NO_LONGER_NEEDED(ped.id)
		SAFE_REMOVE_COVERPOINT(ped.cov)
		IF DOES_BLIP_EXIST(ped.blip)
			REMOVE_BLIP(ped.blip)
		ENDIF
	ENDIF
	
	// Create and setup the ped
	ped.id = CREATE_PED(PEDTYPE_MISSION, model, position, heading)
	ped.iGenericFlag = 0
	ADD_DEADPOOL_TRIGGER(ped.id, MIC3_KILLS)
	IF relGroup <> REL_FRIEND
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(ped.id)
	ENDIF
	
	IF relGroup = REL_FIB
	OR relGroup = REL_FIB_FINAL
		SET_PED_COMPONENT_VARIATION(ped.id, PED_COMP_DECL, 		0, 1, 0) //(decl)
	ENDIF
	
	SET_PED_DIES_WHEN_INJURED(ped.id, TRUE)
	Set_Mission_Ped_Params(ped, pedDebugName, relGroup, weapon, accuracy, armour, bBlip, preferredCover)
ENDPROC

//PURPOSE: Creates and sets up a ped inside a vehicle for use in the mission
PROC Create_Mission_Ped_In_Vehicle(PED_STRUCT &ped, MODEL_NAMES model, VEH_STRUCT &veh, VEHICLE_SEAT seat, STRING pedDebugName, REL_GROUP_HASH relGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, INT accuracy = 30, INT armour = 0, BOOL bBlip = FALSE, ITEMSET_INDEX preferredCover = NULL)
	
	// Safely create the ped
	IF DOES_ENTITY_EXIST(ped.id)
		SET_PED_AS_NO_LONGER_NEEDED(ped.id)
		SAFE_REMOVE_COVERPOINT(ped.cov)
		IF DOES_BLIP_EXIST(ped.blip)
			REMOVE_BLIP(ped.blip)
		ENDIF
	ENDIF
	ped.id = CREATE_PED_INSIDE_VEHICLE(veh.id, PEDTYPE_MISSION, model, seat)
	
	Set_Mission_Ped_Params(ped, pedDebugName, relGroup, weapon, accuracy, armour, bBlip, preferredCover)
ENDPROC

//PURPOSE: Creates a coverpoint and populates a passed in cover struct with the details.
PROC Create_Cover_Point(COVER_STRUCT &covStruct, VECTOR pos, FLOAT dir, COVERPOINT_USAGE use, COVERPOINT_HEIGHT height, COVERPOINT_ARC arc, BOOL bFaceLeft = TRUE)
	covStruct.id 				= ADD_COVER_POINT(pos,dir,use,height,arc)
	covStruct.pos				= pos
	covStruct.dir				= dir
	covStruct.bDoesCovExist 	= TRUE
	covStruct.bFaceLeft			= bFaceLeft
ENDPROC

PROC CREATE_FLAG(FLAG &flagStruct, VECTOR pos, FLOAT dir)
	flagStruct.pos = pos
	flagStruct.dir = dir
ENDPROC

//PURPOSE: Creates a cover point for a ped, stores it with the peds data and makes it the preferred point for that ped
PROC Create_Cover_Point_For_Ped(PED_STRUCT &ped, VECTOR pos, FLOAT dir, COVERPOINT_USAGE use, COVERPOINT_HEIGHT height, COVERPOINT_ARC arc)
	Create_Cover_Point(ped.cov, pos, dir, use, height, arc)
	IF NOT IS_ITEMSET_VALID(ped.set)
		ped.set = CREATE_ITEMSET(TRUE)
	ENDIF
	ADD_TO_ITEMSET(ped.cov.id, ped.set)
	SET_PED_PREFERRED_COVER_SET(ped.id, ped.set)
ENDPROC

//PURPOSE: Sets the combat parameters of the ped
PROC Set_Ped_Combat_Params(PED_STRUCT &ped, VECTOR defensiveSphereCoord, FLOAT defensiveSphereRadius, COMBAT_MOVEMENT cm = CM_DEFENSIVE, COMBAT_RANGE cr = CR_MEDIUM, COMBAT_TARGET_LOSS_RESPONSE tlr = TLR_NEVER_LOSE_TARGET, BOOL bBlockNonTempEvents = TRUE, FLOAT fChanceOfStrafing = 1.0, FLOAT fChanceOfWalkingWhileStrafing = 0.0, BOOL bGoToOpenCombat = FALSE, BOOL bUseCenterAsGoTo = TRUE)
	IF NOT IS_PED_INJURED(ped.id)
		SET_PED_COMBAT_MOVEMENT(ped.id, cm)
		SET_PED_COMBAT_RANGE(ped.id, cr)
		SET_PED_TARGET_LOSS_RESPONSE(ped.id, tlr)
		SET_COMBAT_FLOAT(ped.id, CCF_STRAFE_WHEN_MOVING_CHANCE, fChanceOfStrafing)
		SET_COMBAT_FLOAT(ped.id, CCF_WALK_WHEN_STRAFING_CHANCE, fChanceOfWalkingWhileStrafing)
		SET_PED_SPHERE_DEFENSIVE_AREA(ped.id, defensiveSphereCoord, defensiveSphereRadius, bUseCenterAsGoTo)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id, bBlockNonTempEvents)
		SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, bGoToOpenCombat)
	ENDIF
ENDPROC

PROC TASK_TREVOR_INTO_ROOF_CORNER_COVER()
	CLEAR_PED_TASKS(TREV_PED_ID())
	SAFE_OPEN_SEQUENCE()
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, csTrevorCoverSpot.pos, PEDMOVE_SPRINT, -1)
		TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csTrevorCoverSpot.pos, -1, TRUE, 0.5, FALSE, TRUE, csTrevorCoverSpot.id, TRUE)
	SAFE_PERFORM_SEQUENCE(TREV_PED_ID())
ENDPROC


PROC DEAL_WITH_DEAD_PED(PED_STRUCT &ped)

	IF ped.iDeadTimer = -1
	
		ped.iDeadTimer = GET_GAME_TIMER()
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped.id, PLAYER_PED_ID(), TRUE)
			ped.bKilledByThePlayer = TRUE
		ENDIF
		
		SAFE_REMOVE_COVERPOINT(ped.cov)

	ELIF GET_GAME_TIMER() - ped.iDeadTimer >= 2000
	
		SET_PED_AS_NO_LONGER_NEEDED(ped.id)
		ped.bKilledByThePlayer = FALSE
	
	ENDIF

ENDPROC


PROC SET_PEDS_AS_ONE_HIT_KILLS(PED_STRUCT &ps[])
	INT i 
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF IS_ENTITY_OK(ps[i].id)
			SET_ENTITY_HEALTH(ps[i].id, iCONST_ONE_SHOT_KILL_HEALTH)
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Finds the nearest defensive area in the section.
/// PARAMS:
///    vNearestToCoord - coord you are searching near
///    eSection - museum section being searched
/// RETURNS:
///    Returns a vector of the nearest defensive point.
FUNC VECTOR GET_NEAREST_DEFENSIVE_POINT_IN_SECTION(VECTOR vNearestToCoord, MUSEUM_SECTIONS eSection)
	
	INT i, iResult
	FLOAT fStoredDist = -1.0
	
	REPEAT I_NUM_DEF_SPOTS i
		IF NOT IS_VECTOR_ZERO(sMuseumSections[eSection].vDefensiveAreas[i])	
			FLOAT fTempDist	= VDIST2(vNearestToCoord, sMuseumSections[eSection].vDefensiveAreas[i])
			
			IF fStoredDist = -1
			OR fTempDist < fStoredDist
				fStoredDist = fTempDist
				iResult 	= i
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN sMuseumSections[eSection].vDefensiveAreas[iResult]
ENDFUNC

/// PURPOSE:
///    Sets Michael's AI state
/// PARAMS:
///    newState - the new state to set.
PROC SET_MIKE_PED_AI(AI_MIKE_FLAG newState)
	peds[mpf_mike].iGenericFlag 	= 0
	ai_michael 						= newState
ENDPROC

/// PURPOSE:
///    Sets Trevor's AI state
/// PARAMS:
///    newState - the new state to set.
PROC SET_TREV_AI_STATE(AI_TREV_FLAG newState)
	peds[mpf_trev].iGenericFlag		= 0
	ai_trevor						= newState
	CDEBUG1LN(DEBUG_MISSION, "TREV STATE CHANGED", newState)
ENDPROC

//PURPOSE: Blocks the paths and scenarios at the kortz centre
PROC Clear_And_Block_Area(BOOL bBlock)
	IF bBlock
		IF blockingArea_kortzCenter != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(blockingArea_kortzCenter)
			blockingArea_kortzCenter = NULL
		ENDIF
		
		IF DOES_SCENARIO_GROUP_EXIST("KORTZ_SECURITY")
			IF IS_SCENARIO_GROUP_ENABLED("KORTZ_SECURITY")
				SET_SCENARIO_GROUP_ENABLED("KORTZ_SECURITY", FALSE)
			ENDIF
		ENDIF
		blockingArea_kortzCenter = ADD_SCENARIO_BLOCKING_AREA(<< -2360.4126, 139.9656, 163.4271 >>, << -2142.3582, 438.0082, 203.5064 >>)
		SET_PED_PATHS_IN_AREA(<< -2360.4126, 139.9656, 163.4271 >>, << -2142.3582, 438.0082, 203.5064 >>, FALSE)
		SET_PED_NON_CREATION_AREA(<< -2360.4126, 139.9656, 163.4271 >>, << -2142.3582, 438.0082, 203.5064 >>)
		CLEAR_AREA_OF_PEDS(<<-2260.095947,296.531677,171.352081>>, 500.0)
		
		// disable parking lot roads
		SET_ROADS_IN_ANGLED_AREA(<<-2305.770020,271.116028,167.602097>>, <<-2358.835205,392.012024,192.779190>>, 67.812500, FALSE, FALSE)
		CLEAR_AREA_OF_VEHICLES(<<-2331.230469,331.164948,169.602051>>, 103.0)
	ELSE
		IF blockingArea_kortzCenter != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(blockingArea_kortzCenter)
			blockingArea_kortzCenter = NULL
		ENDIF
		
		IF DOES_SCENARIO_GROUP_EXIST("KORTZ_SECURITY")
			IF NOT IS_SCENARIO_GROUP_ENABLED("KORTZ_SECURITY")
				SET_SCENARIO_GROUP_ENABLED("KORTZ_SECURITY", TRUE)
			ENDIF
		ENDIF
		
		SET_PED_PATHS_BACK_TO_ORIGINAL(<< -2360.4126, 139.9656, 163.4271 >>, << -2142.3582, 438.0082, 203.5064 >>)
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-2305.770020,271.116028,168.602097>>, <<-2358.835205,392.012024,192.779190>>, 67.812500)
		CLEAR_PED_NON_CREATION_AREA()
	ENDIF
ENDPROC

//PURPOSE: Prepares the hotswap peds, should be called when a change is made to the availability of peds
PROC Prep_Hotswap(BOOL bBlockMike = FALSE, BOOL bBlockFrank = FALSE, BOOL bBlockTrev = FALSE,
					BOOL bHintMike = FALSE, BOOL bHintFrank = FALSE, BOOL bHintTrev = FALSE)

	if DOES_ENTITY_EXIST(peds[mpf_frank].id)
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = peds[mpf_frank].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,bBlockFrank) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_FRANKLIN,bHintFrank)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,true)
	ENDIF
	
	if DOES_ENTITY_EXIST(peds[mpf_trev].id)
		sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trev].id 
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,bBlockTrev) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_TREVOR,bHintTrev)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,true)
	ENDIF
	
	if DOES_ENTITY_EXIST(peds[mpf_mike].id)	
		sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_mike].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,bBlockMike) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_MICHAEL,bHintMike)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,true)
	ENDIF

ENDPROC

//PURPOSE: Checks back through from the current section to see if the player has been through the specified section
FUNC BOOL Has_Mike_Passed_Through_Section(MUSEUM_SECTION_FLAGS eSectionFlags)
	MUSEUM_SECTIONS eSectionChecking = eCurrentSection

	WHILE (eSectionChecking != MUS_NO_AREA AND eSectionChecking != MUS_NUM_SECTIONS)

		IF IS_BIT_SET(enum_to_int(eSectionFlags), enum_to_int(eSectionChecking))
			RETURN TRUE
		ELSE
			eSectionChecking = sMuseumSections[eSectionChecking].ePrevSection
		ENDIF
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_PED_STRUCT_BLIP(PED_STRUCT &ps, BOOL bCleanupPed = TRUE)
	IF DOES_ENTITY_EXIST(ps.id)
		UPDATE_AI_PED_BLIP(ps.id, ps.stealthBlip, -1, NULL, TRUE)
		// OMGHAX - Totally piggybacking on this code here, but it's the only place where I can be sure all my enemies are 
		IF bCleanupPed
			IF IS_ENTITY_DEAD(ps.id)
				CPRINTLN(DEBUG_MISSION, "OMGHAX - Setting ped as no longer needed after he died.")
				SET_PED_AS_NO_LONGER_NEEDED(ps.id)
				SAFE_REMOVE_BLIP(ps.blip)
				ps.id = NULL
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PED_STRUCT_ARRAY_BLIPS(PED_STRUCT &ps[], BOOL bCleanupPed = TRUE)
	INT i
	
	FOR i = 0 TO COUNT_OF(ps) -1
		UPDATE_PED_STRUCT_BLIP(ps[i], bCleanupPed)
	ENDFOR
ENDPROC

PROC UPDATE_ALL_ENEMY_BLIPS()
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psFirstAreaCIA)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psFirstAreaFIB)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psFirstAreaMW)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psStairsMW)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psStairsFIB)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psTrevorSaveDaveFIB)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWalkwayCIA1)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWalkwayFIB2)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psStairwayMW)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psMichaelThirdWave)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWarFIB)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWarMWCourtyard)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psTrevorOnlyMW1)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psTrevorOnlyMW2)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psTrevorOnlyMW3)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psTrevorOnlyMW4)
	UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psTrevorOnlyMW5)
ENDPROC



PROC SETUP_DAVE_ATTRIBUTES()

	IF IS_ENTITY_OK(m_pedAllyDave)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(m_pedAllyDave, TRUE)
		GIVE_WEAPON_TO_PED(m_pedAllyDave, weap_fib_rifle, INFINITE_AMMO, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(m_pedAllyDave, FALSE)
		SET_ENTITY_PROOFS(m_pedAllyDave, TRUE, TRUE, TRUE, FALSE, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(m_pedAllyDave, REL_FRIEND)
		SET_PED_KEEP_TASK(m_pedAllyDave, TRUE)
		SET_ENTITY_MAX_HEALTH(m_pedAllyDave, 200)
		SET_ENTITY_HEALTH(m_pedAllyDave, 200)
		SET_PED_CONFIG_FLAG(m_pedAllyDave, PCF_OnlyAttackLawIfPlayerIsWanted, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(m_pedAllyDave, CA_CAN_IGNORE_BLOCKED_LOS_WEIGHTING, TRUE)
		SAFE_REMOVE_BLIP(m_blipAllyDave)
		SAFE_BLIP_PED(m_blipAllyDave, m_pedAllyDave)
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(m_pedAllyDave, "MI_3_DAVE_GROUP")
		STOP_PED_SPEAKING(m_pedAllyDave, TRUE)
		ADD_PED_FOR_DIALOGUE(sConvo, 4, m_pedAllyDave, "Dave")
	ENDIF
ENDPROC

PROC INITIALIZE_DAVE_MONITORING_SYSTEM()
	CANCEL_TIMER(m_tmrDaveDeathAdvance)	
	CANCEL_TIMER(m_tmrDaveAggressiveAdvance)
	m_bAllEnemiesDeadDuringAdvance = FALSE
ENDPROC


FUNC INT GET_NUM_PEDS_ALIVE(PED_STRUCT &ps[])
	INT iRetVal = 0
	INT i 
	
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF DOES_ENTITY_EXIST(ps[i].id)
			IF NOT IS_PED_DEAD_OR_DYING(ps[i].id)
			AND NOT IS_PED_INJURED(ps[i].id)
				iRetVal++
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iRetVal
ENDFUNC

PROC CREATE_POOL_NAVMESH_BLOCKERS()
	iCourtyardPoolNavBlock[0] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2267.079, 312.111, 174.348>>),(<<7.5, 22, 2.6>>), DEG_TO_RAD(24.000))
	iCourtyardPoolNavBlock[1] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2258.979, 293.811, 174.348>>),(<<7.5, 22, 2.6>>), DEG_TO_RAD(24.000))
	iCourtyardPoolNavBlock[2] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2253.079, 280.511, 174.348>>),(<<7.5, 16, 2.6>>), DEG_TO_RAD(24.000))
ENDPROC

PROC DELETE_POOL_NAVMESH_BLOCKERS()
	INT i 
	FOR i = 0 TO COUNT_OF(iCourtyardPoolNavBlock) - 1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iCourtyardPoolNavBlock[i])
			REMOVE_NAVMESH_BLOCKING_OBJECT(iCourtyardPoolNavBlock[i])
		ENDIF
	ENDFOR
ENDPROC

PROC CREATE_TREVOR_DAVE_NAVMESH_BLOCKERS()
	iTrevorDaveNavBlock[0] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2265.381, 275.608, 173.895>>),(<<20, 20, 5>>), DEG_TO_RAD(23.956))
	iTrevorDaveNavBlock[1] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2272.281, 294.308, 173.895>>),(<<20, 20, 5>>), DEG_TO_RAD(23.956))
	iTrevorDaveNavBlock[2] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2254.881, 302.108, 173.895>>),(<<20, 20, 5>>), DEG_TO_RAD(23.956))
	iTrevorDaveNavBlock[3] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2236.681, 310.608, 173.895>>),(<<20, 20, 5>>), DEG_TO_RAD(23.956))
//	iTrevorDaveNavBlock[4] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2222.135, 237.227, 173.895>>),(<<20, 20, 5>>), DEG_TO_RAD(-12.792))
	iTrevorDaveNavBlock[5] = ADD_NAVMESH_BLOCKING_OBJECT((<<-2214.814, 295.635, 174.033>>),(<<6.600, 20, 4.000>>), DEG_TO_RAD(25.016))
ENDPROC

PROC DELETE_TREVOR_DAVE_NAVMESH_BLOCKERS()
	INT i 
	FOR i = 0 TO COUNT_OF(iTrevorDaveNavBlock) - 1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iTrevorDaveNavBlock[i])
			REMOVE_NAVMESH_BLOCKING_OBJECT(iTrevorDaveNavBlock[i])
		ENDIF
	ENDFOR
ENDPROC

PROC TASK_PED_RUN_TO_COORD(PED_INDEX ped, VECTOR vDestination, BOOL bUseCombatMovement = TRUE, FLOAT pedMove = PEDMOVE_SPRINT, BOOL bClearCurrentTask = TRUE)
	FLOAT radius = 2.0
	IF IS_ENTITY_OK(ped)
		IF bClearCurrentTask
			CLEAR_PED_TASKS_IMMEDIATELY(ped)
		ENDIF
		REMOVE_PED_DEFENSIVE_AREA(ped)
		SET_PED_SPHERE_DEFENSIVE_AREA(ped, vDestination, radius)
		SET_PED_COMBAT_MOVEMENT(ped, CM_DEFENSIVE)
		IF bUseCombatMovement
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped, 500.0)
		ELSE
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
			SAFE_OPEN_SEQUENCE()
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDestination, pedMove)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
			SAFE_PERFORM_SEQUENCE(ped)
		ENDIF
//		SAFE_OPEN_SEQUENCE()
//			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDestination, PEDMOVE_SPRINT)
//			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//		SAFE_PERFORM_SEQUENCE(ped)
	ENDIF
ENDPROC

PROC TASK_DAVE_RUN_TO_COORD(VECTOR vDestination, BOOL bUseCombatMovement = TRUE, BOOL bClearCurrentTask = TRUE)
	TASK_PED_RUN_TO_COORD(m_pedAllyDave, vDestination, bUseCombatMovement, DEFAULT, bClearCurrentTask)
ENDPROC

PROC MOVE_DAVE_TO_LOCATION_AGGRESSIVE(VECTOR vDestination)
	CPRINTLN(DEBUG_MISSION, "MOVE_DAVE_TO_LOCATION_AGGRESSIVE")
	TASK_DAVE_RUN_TO_COORD(vDestination)
ENDPROC

PROC MOVE_DAVE_TO_LOCATION_TO_DIE(VECTOR vDestination, PED_STRUCT &pedEnemyCIA[], PED_STRUCT &pedEnemyFIB[], PED_STRUCT &pedEnemyMW[])
	INT i
	INT iMinPauseTime = 250
	INT iMaxPauseTime = 1000
	IF IS_ENTITY_OK(m_pedAllyDave)
		SET_ENTITY_HEALTH(m_pedAllyDave, iCONST_ONE_SHOT_KILL_HEALTH)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(m_pedAllyDave, FALSE)
		SET_ENTITY_PROOFS(m_pedAllyDave, FALSE, FALSE, FALSE, FALSE, FALSE)
		TASK_DAVE_RUN_TO_COORD(vDestination)
	ENDIF

	FOR i = 0 TO COUNT_OF(pedEnemyCIA) - 1
		IF DOES_ENTITY_EXIST(pedEnemyCIA[i].id)
			IF NOT IS_PED_DEAD_OR_DYING(pedEnemyCIA[i].id)
				SAFE_OPEN_SEQUENCE()
					TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(iMinPauseTime, iMaxPauseTime))
					TASK_SHOOT_AT_ENTITY(NULL, m_pedAllyDave, -1, FIRING_TYPE_CONTINUOUS)
				SAFE_PERFORM_SEQUENCE(pedEnemyCIA[i].id, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO COUNT_OF(pedEnemyFIB) - 1
		IF DOES_ENTITY_EXIST(pedEnemyFIB[i].id)
			IF NOT IS_PED_DEAD_OR_DYING(pedEnemyFIB[i].id)
				SAFE_OPEN_SEQUENCE()
					TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(iMinPauseTime, iMaxPauseTime))
					TASK_SHOOT_AT_ENTITY(NULL, m_pedAllyDave, -1, FIRING_TYPE_CONTINUOUS)
				SAFE_PERFORM_SEQUENCE(pedEnemyFIB[i].id, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO COUNT_OF(pedEnemyMW) - 1
		IF DOES_ENTITY_EXIST(pedEnemyMW[i].id)
			IF NOT IS_PED_DEAD_OR_DYING(pedEnemyMW[i].id)
				SAFE_OPEN_SEQUENCE()
					TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(iMinPauseTime, iMaxPauseTime))
					TASK_SHOOT_AT_ENTITY(NULL, m_pedAllyDave, -1, FIRING_TYPE_CONTINUOUS)
				SAFE_PERFORM_SEQUENCE(pedEnemyMW[i].id, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

FUNC INT GET_NUMBER_ALIVE_ALL_FACTIONS(PED_STRUCT	&pedEnemyCIA[], PED_STRUCT &pedEnemyFIB[], PED_STRUCT &pedEnemyMW[])
	INT iTotalAlive = 0
	
	// Add up the total alive from the CIA
	iTotalAlive +=	GET_NUM_PEDS_ALIVE(pedEnemyCIA)
	
	// Add up the total alive from the FIB
	iTotalAlive +=	GET_NUM_PEDS_ALIVE(pedEnemyFIB)
	
	// Add up the total alive from Merryweather
	iTotalAlive +=	GET_NUM_PEDS_ALIVE(pedEnemyMW)
	
	RETURN iTotalAlive
ENDFUNC

/// PURPOSE:
///    Determines whether we've eliminated all of the enemies that are impeding Dave's progress, in which case, he can advance safely.
///    He won't need to kill anyone along the way
/// PARAMS:
///    pedEnemyCIA - CIA enemies
///    pedEnemyFIB - FIB enemies
///    pedEnemyMW - Merryweather enemies
/// RETURNS:
///    TRUE if everyone from the three enemy groups is dead or dying, FALSE if anyone's still alive
FUNC BOOL ARE_ALL_ENEMIES_DEAD(PED_STRUCT	&pedEnemyCIA[], PED_STRUCT &pedEnemyFIB[], PED_STRUCT &pedEnemyMW[])
	RETURN (GET_NUMBER_ALIVE_ALL_FACTIONS(pedEnemyCIA, pedEnemyFIB, pedEnemyMW) = 0)
ENDFUNC

PROC RETASK_DAVE_IF_ALL_ENEMIES_DEAD(VECTOR vDestination, PED_STRUCT &pedEnemyCIA[], PED_STRUCT &pedEnemyFIB[], PED_STRUCT &pedEnemyMW[])
	IF NOT m_bAllEnemiesDeadDuringAdvance
		IF ARE_ALL_ENEMIES_DEAD(pedEnemyCIA, pedEnemyFIB, pedEnemyMW)
			CPRINTLN(DEBUG_MISSION, "RETASK_DAVE_IF_ALL_ENEMIES_DEAD")
			m_bAllEnemiesDeadDuringAdvance = TRUE
			TASK_DAVE_RUN_TO_COORD(vDestination,FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines whether we haven't killed enough enemies, and Dave should advance and die.
/// PARAMS:
///    pedEnemyCIA - CIA enemies
///    pedEnemyFIB - FIB enemies
///    pedEnemyMW - Merryweather enemies
/// RETURNS:
///    
FUNC BOOL DAVE_SHOULD_DIE(PED_STRUCT &pedEnemyCIA[], PED_STRUCT &pedEnemyFIB[], PED_STRUCT &pedEnemyMW[], FLOAT flTimeToFail = flCONST_TIME_FOR_DAVE_DEATH)
	INT 	iTotalAlive = 0
	INT 	iGrandTotal = 0
	
	IF NOT IS_TIMER_STARTED(m_tmrDaveDeathAdvance)
		START_TIMER_NOW(m_tmrDaveDeathAdvance)
	ELSE
		// Death timer has expired, add up the enemies, and see if Dave should die
		IF GET_TIMER_IN_SECONDS(m_tmrDaveDeathAdvance) >= flTimeToFail			
			IF DOES_ENTITY_EXIST(pedEnemyCIA[0].id)
				iTotalAlive += GET_NUM_PEDS_ALIVE(pedEnemyCIA)
				iGrandTotal += COUNT_OF(pedEnemyCIA)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedEnemyFIB[0].id)
				iTotalAlive += GET_NUM_PEDS_ALIVE(pedEnemyFIB)
				iGrandTotal += COUNT_OF(pedEnemyFIB)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedEnemyMW[0].id)
				iTotalAlive += GET_NUM_PEDS_ALIVE(pedEnemyMW)
				iGrandTotal += COUNT_OF(pedEnemyMW)
			ENDIF
			
			IF iTotalAlive = iGrandTotal
				CPRINTLN(DEBUG_MISSION, "DAVE_SHOULD_DIE - iTotalAlive = ", iTotalAlive, " and iGrandTotal = ", iGrandTotal, ".  Fail.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


//PURPOSE: Sets the mission stage
PROC Mission_Set_Stage(mission_stage_flag newStage)
	requestedStage = enum_to_int(newStage)
	stageSwitch = STAGESWITCH_REQUESTED
ENDPROC

PROC CLEANUP_PED_STRUCT(PED_STRUCT& pedStruct, BOOL bDelete = FALSE)

	IF DOES_ENTITY_EXIST(pedstruct.id)
		IF bDelete
			DELETE_PED(pedstruct.id)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedstruct.id)
		ENDIF
	ENDIF
	SAFE_REMOVE_BLIP(pedstruct.blip)
	CLEANUP_AI_PED_BLIP(pedstruct.stealthBlip)

ENDPROC

PROC CLEANUP_PED_STRUCT_PEDS(PED_STRUCT &pedstruct[], BOOL bDelete = FALSE)
	INT i 
	FOR i = 0 TO COUNT_OF(pedstruct) - 1
		CLEANUP_PED_STRUCT( pedStruct[i], bDelete )
	ENDFOR
ENDPROC

PROC DESTROY_ALL_MISSION_PEDS(BOOL bVehicles = TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaCIA, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaFIB, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaMW, TRUE)
	
	CLEANUP_PED_STRUCT_PEDS(m_psStairsMW, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psStairsFIB, TRUE)
	
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorSaveDaveFIB, TRUE)
	
	CLEANUP_PED_STRUCT_PEDS(m_psFIBIG_6, TRUE)
	CLEANUP_PED_STRUCT(m_psMWIG_7, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psMichaelSecondWave, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psMichaelThirdWave, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psMWIG_8, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayCIA1, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayFIB2, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayMW2, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayMW3, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psStairwayMW, TRUE)
	
	CLEANUP_PED_STRUCT_PEDS(m_psWarFIB, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psWarCIA, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psWarMWCourtyard, TRUE)
	
	CLEANUP_PED_STRUCT_PEDS(m_psFinalWalkwayCIA, TRUE)
	
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW1, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW2, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW3, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW4, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW5, TRUE)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorDangerPeds, TRUE)
	
	INT i
	FOR i = 0 TO enum_to_int(mpf_num_peds) - 1
		IF DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
		AND PLAYER_PED_ID() != peds[i].id
			DELETE_PED(peds[i].id)
		ENDIF
		
		SAFE_REMOVE_BLIP(peds[i].blip)
	ENDFOR
	
	IF bVehicles
		FOR i = 0 TO enum_to_int(mvf_num_vehicles) - 1
			IF DOES_ENTITY_EXIST(vehs[i].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[i].id)
				DELETE_VEHICLE(vehs[i].id)
			ENDIF
			SAFE_REMOVE_BLIP(vehs[i].blip)
		ENDFOR
	ENDIF
	
	IF DOES_ENTITY_EXIST(m_pedAllyDave)
		DELETE_PED(m_pedAllyDave)
	ENDIF
	
ENDPROC

PROC DELETE_IG_8_COVER_POINTS()
	REMOVE_COVER_POINT(csIG_8CoverPoints[0].id)
	REMOVE_COVER_POINT(csIG_8CoverPoints[1].id)
	REMOVE_COVER_POINT(csIG_8CoverPoints[2].id)
	REMOVE_COVER_POINT(csIG_8CoverPoints[3].id)
	REMOVE_COVER_POINT(csIG_8CoverPoints[4].id)
ENDPROC

PROC CREATE_IG_8_COVER_POINTS()
	DELETE_IG_8_COVER_POINTS()
	Create_Cover_Point(csIG_8CoverPoints[0], (<<-2274.92456, 262.41965, 183.60112>>), 236, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
	Create_Cover_Point(csIG_8CoverPoints[1], (<<-2277.51001, 259.84430, 183.60129>>), 236, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
	Create_Cover_Point(csIG_8CoverPoints[2], (<<-2275.34863, 283.59723, 183.60147>>), 310, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60, FALSE)
	Create_Cover_Point(csIG_8CoverPoints[3], (<<-2271.35767, 264.46384, 183.60159>>), 220, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, FALSE)
	Create_Cover_Point(csIG_8CoverPoints[4], (<<-2271.49512, 274.47726, 183.60159>>), 310, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60, FALSE)
ENDPROC

// To fix url:bugstar:2080971
PROC SAFE_SET_PED_LOAD_COLLISION_FLAG(PED_INDEX ped, BOOL bLoadCollision)
	// Don't call SET_ENTITY_LOAD_COLLISION_FLAG if we're setting it to false on the player
	IF NOT ((ped = PLAYER_PED_ID()) AND (bLoadCollision = FALSE))
		SET_ENTITY_LOAD_COLLISION_FLAG(ped, bLoadCollision)
	ENDIF
ENDPROC

//PURPOSE: Cleans up the mission passing failing or dying
PROC Mission_Cleanup()
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	STOP_AUDIO_SCENES()
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	If NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		CLEAR_PED_ALTERNATE_WALK_ANIM(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	REDUCE_AMBIENT_MODELS(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	INT i
	FOR i = 0 TO enum_to_int(mpf_num_peds) - 1
		IF DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
		AND PLAYER_PED_ID() != peds[i].id
			SET_PED_AS_NO_LONGER_NEEDED(peds[i].id)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO enum_to_int(mvf_num_vehicles) - 1
		IF DOES_ENTITY_EXIST(vehs[i].id)
		AND IS_VEHICLE_DRIVEABLE(vehs[i].id)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
		ENDIF
	ENDFOR
	
	IF IS_ENTITY_OK(m_pedAllyDave)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(m_pedAllyDave)
	ENDIF
	
	IF IS_ENTITY_OK(TREV_PED_ID())
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(TREV_PED_ID())
	ENDIF
	
	IF IS_ENTITY_OK(MIKE_PED_ID())
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(MIKE_PED_ID())
		SET_PED_CAN_SWITCH_WEAPON(MIKE_PED_ID(), TRUE)
	ENDIF
	
	FOR i = 0 TO COUNT_OF(m_objCourtyardCover) - 1
		IF DOES_ENTITY_EXIST(m_objCourtyardCover[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(m_objCourtyardCover[i])
		ENDIF
	ENDFOR
	
	IF IS_ENTITY_OK(MIKE_PED_ID())
		SAFE_SET_PED_LOAD_COLLISION_FLAG(MIKE_PED_ID(), FALSE)
	ENDIF
		
	IF IS_ENTITY_OK(TREV_PED_ID())
		SAFE_SET_PED_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE)
	ENDIF
	
	REMOVE_NAVMESH_BLOCKING_OBJECT(iTrevorPathNavmeshBlock)
	
	IF DOES_ENTITY_EXIST(m_michaelsGun)
		DELETE_OBJECT(m_michaelsGun)
	ENDIF
	
	CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaCIA)
	CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaFIB)
	CLEANUP_PED_STRUCT_PEDS(m_psFirstAreaMW)
	
	CLEANUP_PED_STRUCT_PEDS(m_psStairsMW)
	CLEANUP_PED_STRUCT_PEDS(m_psStairsFIB)
	
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorSaveDaveFIB)
	
	CLEANUP_PED_STRUCT_PEDS(m_psFIBIG_6)
	CLEANUP_PED_STRUCT(m_psMWIG_7)
	CLEANUP_PED_STRUCT_PEDS(m_psMichaelSecondWave)
	CLEANUP_PED_STRUCT_PEDS(m_psMichaelThirdWave)
	CLEANUP_PED_STRUCT_PEDS(m_psMWIG_8)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayCIA1)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayFIB2)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayMW2)
	CLEANUP_PED_STRUCT_PEDS(m_psWalkwayMW3)
	CLEANUP_PED_STRUCT_PEDS(m_psStairwayMW)
	CLEANUP_PED_STRUCT_PEDS(m_psWarFIB)
	CLEANUP_PED_STRUCT_PEDS(m_psWarCIA)
	CLEANUP_PED_STRUCT_PEDS(m_psWarMWCourtyard)
	
	CLEANUP_PED_STRUCT_PEDS(m_psFinalWalkwayCIA)
	
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW1)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW2)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW3)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW4)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorOnlyMW5)
	CLEANUP_PED_STRUCT_PEDS(m_psTrevorDangerPeds)
	
	
	SAFE_REMOVE_BLIP(m_blipAllyDave)
	SAFE_REMOVE_BLIP(blipOtherPlayer)
	SAFE_REMOVE_BLIP(blip_Objective)
	
	REPEAT COUNT_OF(objs) i
		IF DOES_ENTITY_EXIST(objs[i].id)
		AND objs[i].bCustom
			SET_OBJECT_AS_NO_LONGER_NEEDED(objs[i].id)
		ENDIF
	ENDREPEAT
	
	FOR i = 0 TO COUNT_OF(csFirstAreaIPLCover) - 1
		SAFE_REMOVE_COVERPOINT(csFirstAreaIPLCover[i])
	ENDFOR
	
	FOR i =  0 TO COUNT_OF(csIG_8CoverPoints) - 1
		SAFE_REMOVE_COVERPOINT(csIG_8CoverPoints[i])
	ENDFOR
	
	FOR i =  0 TO COUNT_OF(csIG_6CoverPoints) - 1
		SAFE_REMOVE_COVERPOINT(csIG_6CoverPoints[i])
	ENDFOR
	
	SAFE_REMOVE_COVERPOINT(csMichaelStartingCover)
	SAFE_REMOVE_COVERPOINT(csPlayerFountainCover)
	SAFE_REMOVE_COVERPOINT(csDaveFountainCover)
	SAFE_REMOVE_COVERPOINT(csOtherPlayerFountainCover)
	SAFE_REMOVE_COVERPOINT(csMichaelMIC3_INTCover)
	SAFE_REMOVE_COVERPOINT(csPlayerParkingLotCover)
	SAFE_REMOVE_COVERPOINT(csTrevorCoverSpot)
	SAFE_REMOVE_COVERPOINT(csDaveBottomOfStairsCover)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	REPEAT COUNT_OF(cams) i
		IF DOES_CAM_EXIST(cams[i].id)
			DESTROY_CAM(cams[i].id)
		ENDIF
	ENDREPEAT
	
	DELETE_TREVOR_DAVE_NAVMESH_BLOCKERS()
	DELETE_POOL_NAVMESH_BLOCKERS()
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iMichaelNavBlock)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iMichaelNavBlock)
	ENDIF
	
	REMOVE_DECAL(decal_andreas)
	
	SET_TIME_SCALE(1.0)
	f_current_time_scale = 1.0
	
	REMOVE_ALL_SCRIPT_FIRES()
	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL( 5 )
	SET_CREATE_RANDOM_COPS(TRUE)
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_SUPER_JUMP)
		DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, FALSE) 
	ENDIF
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_FAST_RUN)
		DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, FALSE) 
	ENDIF
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_SPAWN_VEHICLE)
		DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, FALSE) 
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
	DELETE_IG_8_COVER_POINTS()
	DISABLE_TAXI_HAILING(FALSE)
	Clear_And_Block_Area(FALSE)
	
//	IF IS_IPL_ACTIVE("mic3_chopper_debris")
//		REMOVE_IPL("mic3_chopper_debris")
//	ENDIF
	SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
	
ENDPROC

//PURPOSE: Cleans up the missin without terminating the script
PROC Mission_Reset_Cleanup()
	
	RESET_ALL_MISSION_EVENTS(sEvents)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(s_locatesDataMike)
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	DESTROY_ALL_CAMS()
	
	SET_TIME_SCALE(1.0)
	f_current_time_scale = 1.0
	
	REMOVE_PED_FOR_DIALOGUE(sConvo, 0)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 1)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 2)
	
	// Delete all peds
	INT i
	FOR i = 0 TO enum_to_int(mpf_num_peds)-1
		IF DOES_ENTITY_EXIST(peds[i].id)
		AND PLAYER_PED_ID() != peds[i].id
			DELETE_PED(peds[i].id)
		ENDIF

		SAFE_REMOVE_COVERPOINT(peds[i].cov)
	ENDFOR
	
	// Delete all vehicles	
	REPEAT COUNT_OF(vehs) i
		IF DOES_ENTITY_EXIST(vehs[i].id)
						
			// make sure player is not in it before deleting
			IF IS_VEHICLE_DRIVEABLE(vehs[i].id)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[i].id)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehs[i].id) + <<-2.0,0,0>>)
				ENDIF
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[i].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[i].id)
				ENDIF
			ENDIF
			
			vehs[i].fCurrentSpeed 		= 1.0
			vehs[i].fStartSpeed 		= 1.0
			vehs[i].fDesiredSpeed 		= 1.0
			vehs[i].iSpeedTimeStamp 	= -1
			vehs[i].iDesiredDuration	= 0
			DELETE_VEHICLE(vehs[i].id)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(objs) i
		IF DOES_ENTITY_EXIST(objs[i].id)
		AND objs[i].bCustom
			DELETE_OBJECT(objs[i].id)
		ENDIF
	ENDREPEAT
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	REPEAT COUNT_OF(cams) i
		IF DOES_CAM_EXIST(cams[i].id)
			DESTROY_CAM(cams[i].id)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(m_michaelsGun)
		DELETE_OBJECT(m_michaelsGun)
	ENDIF
	
	// remove objective blip
	IF DOES_BLIP_EXIST(blip_Objective)
		REMOVE_BLIP(blip_Objective)
	ENDIF
	
	// Clean up area
	REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_SMG)
	REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_PUMPSHOTGUN)
	REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_DLC_ASSAULTSMG)
	REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_DLC_HEAVYRIFLE)
	
	REMOVE_DECALS_IN_RANGE(<< -2166.5374, 232.3802, 183.6015 >>, 200.0)
	REMOVE_PARTICLE_FX_IN_RANGE(<< -2166.5374, 232.3802, 183.6015 >>, 200.0)
	CLEAR_AREA_OF_PEDS(<< -2166.5374, 232.3802, 183.6015 >>, 200.0)
	
//	IF IS_IPL_ACTIVE("mic3_chopper_debris")
//		REMOVE_IPL("mic3_chopper_debris")
//	ENDIF
	SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	
	bCanBlipTrevor							= FALSE
	eCurrentSection						= MUS_NO_AREA
	
	Clear_And_Block_Area(FALSE)
ENDPROC

//PURPOSE: Called when the mission has been successfully completed.
PROC Mission_Passed()	
	CLEAR_PRINTS()
	CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
	Mission_Flow_Mission_Passed()	
	Mission_Cleanup()
	g_bBlockShopRoberies = FALSE
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE: Called when the mission has failed and for what reason
PROC Mission_Failed(mission_fail_flag fail_condition = mff_default)

	TRIGGER_MUSIC_EVENT("MIC3_MISSION_FAIL")

	// Force failed - player death, arrested, switch to Multiplayer, etc
	IF fail_condition = mff_default
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		Mission_Cleanup()
		
	// Normal mission failed
	ELSE
		// Process fail msg
		STRING strFailMsg
		SWITCH fail_condition
			CASE 	mff_debug_forced		strFailMsg = "M3_FF"		BREAK
			CASE 	mff_michael_dead		strFailMsg = "M3_FMDEAD"	BREAK
			CASE 	mff_trevor_dead			strFailMsg = "M3_FTDEAD"	BREAK
			CASE	mff_dave_dead			strFailMsg = "M3_DVDEAD"	BREAK
			CASE	mff_dave_abdn			strFailMsg = "M3_DVABDN"	BREAK
			// should never get here but use default fail here too
			DEFAULT							strFailMsg = "M3_FF"		BREAK
		ENDSWITCH

		MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailMsg)
		
		WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
			WAIT(0)
		ENDWHILE

		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
		IF fail_condition = mff_debug_forced
			Mission_Reset_Cleanup()
		ENDIF
		
	
		MISSION_CLEANUP() // must only take 1 frame and terminate the thread

	ENDIF
	g_bBlockShopRoberies = FALSE
	TERMINATE_THIS_THREAD()
ENDPROC


PROC FAIL_MISSION_IF_DAVE_ABANDONED(FLOAT flFailureDistance = 60.0, FLOAT flWarningDistance = 50.0)
	IF IS_ENTITY_OK(PLAYER_PED_ID())
	AND IS_ENTITY_OK(m_pedAllyDave)
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), m_pedAllyDave) >= flFailureDistance
			Mission_Failed(mff_dave_abdn)
		ENDIF
		
		IF IS_TIMER_STARTED(m_tmrFailWarningTimer)
			IF GET_TIMER_IN_SECONDS(m_tmrFailWarningTimer) >= 10.0
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), m_pedAllyDave) >= flWarningDistance
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						RESTART_TIMER_NOW(m_tmrFailWarningTimer)
						PRINT_NOW("M3_DVWARN", DEFAULT_GOD_TEXT_TIME, 1)
						IF int_to_enum(mission_stage_flag, mission_stage) = STAGE_DAVE_BY_FOUNTAIN
							IF DOES_BLIP_EXIST(blip_Objective)	
								SAFE_REMOVE_BLIP(blip_Objective)
							ENDIF
						ENDIF
					ENDIF
				ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), m_pedAllyDave) <= flWarningDistance
					IF IS_THIS_PRINT_BEING_DISPLAYED("M3_DVWARN")
						CLEAR_THIS_PRINT("M3_DVWARN")
					ENDIF
					IF m_bBlipManagementDuringAbandonChecks
						IF int_to_enum(mission_stage_flag, mission_stage) = STAGE_DAVE_BY_FOUNTAIN
							IF NOT DOES_BLIP_EXIST(blip_Objective)
								blip_Objective = ADD_BLIP_FOR_COORD(<<-2247.73169, 268.95413, 173.60196>>)
								PRINT_NOW("M3_02", DEFAULT_GOD_TEXT_TIME, 1)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_THIS_PRINT_BEING_DISPLAYED("M3_DVWARN")
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), m_pedAllyDave) <= flWarningDistance
					CLEAR_THIS_PRINT("M3_DVWARN")
				ENDIF
			ENDIF
		ELSE
			RESTART_TIMER_NOW(m_tmrFailWarningTimer)
		ENDIF
	ENDIF
ENDPROC



PROC UPDATE_HELICOPTER()
		
	// VEHICLE RECORDING MANAGEMENT
	/*FLOAT fRecPhase
	IF IS_VEHICLE_DRIVEABLE(vehs[mvf_mw_heli_1].id)
	AND NOT IS_PED_INJURED(peds[mpf_mw_heli_1_pilot].id)
				
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_1].id)
			fRecPhase = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_mw_heli_1].id)
			
			#IF IS_DEBUG_BUILD
				f_debug_rec_phase = fRecPhase
			#ENDIF
			
			SWITCH peds[mpf_mw_heli_1_pilot].iGenericFlag
			// >>>> INITIAL HOVER START
				CASE 1
					IF fRecPhase >= 16736.200
						peds[mpf_mw_heli_1_pilot].iGenericFlag = 101
					ENDIF
				BREAK
				CASE 101
					IF Set_Vehicle_Playback_Speed(vehs[mvf_mw_heli_1], -0.5, 1000)
						peds[mpf_mw_heli_1_pilot].iGenericFlag = 2
					ENDIF
				BREAK
				CASE 2
					IF fRecPhase <= 15000.000
						peds[mpf_mw_heli_1_pilot].iGenericFlag = 202
					ENDIF
				BREAK
				CASE 202
					IF Set_Vehicle_Playback_Speed(vehs[mvf_mw_heli_1], 0.5, 1000)
						peds[mpf_mw_heli_1_pilot].iGenericFlag = 1
					ENDIF
				BREAK
			// <<<< INTIAIL HOVER END
			
			// START STRAFE
				CASE 3
					IF Set_Vehicle_Playback_Speed(vehs[mvf_mw_heli_1], 1.6, 1000)
						peds[mpf_mw_heli_1_pilot].iGenericFlag++
					ENDIF
				BREAK
				CASE 4
					// Slow down speed during the strafe
					IF fRecPhase >= 22361.150
						peds[mpf_mw_heli_1_pilot].iGenericFlag++
					ENDIF
				BREAK
				CASE 5
					IF Set_Vehicle_Playback_Speed(vehs[mvf_mw_heli_1], 1.0, 2000)
						peds[mpf_mw_heli_1_pilot].iGenericFlag++
					ENDIF
				BREAK
			// >>>> REACHED END
				CASE 6
					IF fRecPhase >= 32000.480
						STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_1].id)
					ENDIF
				BREAK

			ENDSWITCH
			
		ELSE
			fRecPhase = -1
		ENDIF
	ENDIF
	*/
	IF m_iFirstAreaHelicopterState = 1
		vehs[mvf_mw_heli_1].id = CREATE_VEHICLE(mod_heli_mw, << -2149.5046, 240.5233, 187.5145 >>, 201.0944)
		Create_Mission_Ped_In_Vehicle(peds[mpf_mw_heli_1_pilot], mod_ped_mw, vehs[mvf_mw_heli_1], VS_DRIVER, "MW_HELI1_1", REL_MW)
		SET_PED_COMBAT_ATTRIBUTES(peds[mpf_mw_heli_1_pilot].id, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
		SET_PED_TARGET_LOSS_RESPONSE(peds[mpf_mw_heli_1_pilot].id, TLR_NEVER_LOSE_TARGET)
		SET_PED_COMBAT_RANGE(peds[mpf_mw_heli_1_pilot].id, CR_FAR)
		SET_PED_SHOOT_RATE(peds[mpf_mw_heli_1_pilot].id, 100)
		SET_PED_FIRING_PATTERN(peds[mpf_mw_heli_1_pilot].id, FIRING_PATTERN_FULL_AUTO)
		SET_PED_ACCURACY(peds[mpf_mw_heli_1_pilot].id, 100)
		SET_HELI_BLADES_FULL_SPEED(vehs[mvf_mw_heli_1].id)
		SET_PED_CAN_BE_TARGETTED(peds[mpf_mw_heli_1_pilot].id, FALSE)

		SET_ENTITY_INVINCIBLE(peds[mpf_mw_heli_1_pilot].id, TRUE)
		SET_ENTITY_PROOFS(peds[mpf_mw_heli_1_pilot].id, TRUE, TRUE, TRUE, FALSE, FALSE)
		SAFE_BLIP_VEHICLE(vehs[mvf_mw_heli_1].blip, vehs[mvf_mw_heli_1].id, TRUE)
		
		SET_VEHICLE_RADIO_ENABLED(vehs[mvf_mw_heli_1].id, FALSE)
		SET_ENTITY_INVINCIBLE(vehs[mvf_mw_heli_1].id, TRUE)
		SET_ENTITY_PROOFS(vehs[mvf_mw_heli_1].id, FALSE, TRUE, TRUE, FALSE, FALSE)
		SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_mw_heli_1].id, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(vehs[mvf_mw_heli_1].id, FALSE)
		SET_VEHICLE_CAN_LEAK_OIL(vehs[mvf_mw_heli_1].id, FALSE)
		START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_1].id, rec_mw_heli_1, str_carrecs)
		SET_PLAYBACK_SPEED(vehs[mvf_mw_heli_1].id, 1.3)
		vehs[mvf_mw_heli_1].fCurrentSpeed = 1.3
		peds[mpf_mw_heli_1_pilot].iGenericFlag = 1
		m_iFirstAreaHelicopterState++
		
	ELIF m_iFirstAreaHelicopterState >= 1
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_mw_heli_1].id)
		
			IF NOT IS_PED_INJURED(peds[mpf_mw_heli_1_pilot].id)
			
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_1].id)
					SET_ENTITY_INVINCIBLE(peds[mpf_mw_heli_1_pilot].id, FALSE)
					SET_PED_ACCURACY(peds[mpf_mw_heli_1_pilot].id, 5)
					SET_PED_FIRING_PATTERN(peds[mpf_mw_heli_1_pilot].id, FIRING_PATTERN_BURST_FIRE_HELI)
					TASK_HELI_MISSION(peds[mpf_mw_heli_1_pilot].id, vehs[mvf_mw_heli_1].id, null, MIKE_PED_ID(), <<0,0,0>>, MISSION_FLEE, 10.0, 48.0, -1, 200, 18)
					m_iFirstAreaHelicopterState++
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC EVENT_escape_vehicles(MISSION_EVENT_DATA &sData)
	SWITCH sData.iStage
		CASE 1
			Load_Asset_Model(sAssetData, DOMINATOR)
			Load_Asset_Model(sAssetData, CARBONIZZARE)
			Load_Asset_Model(sAssetData, VACCA)
			
			sData.iStage++
		BREAK
		CASE 2
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_escape_1].id)
			AND HAS_MODEL_LOADED(VACCA)
				vehs[mvf_escape_1].id = CREATE_VEHICLE(VACCA, << -2296.1980, 415.6821, 173.4666 >>, 342.8948)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_escape_2].id)
			AND HAS_MODEL_LOADED(DOMINATOR)
				vehs[mvf_escape_2].id = CREATE_VEHICLE(DOMINATOR, << -2332.3652, 349.8775, 171.7965 >>, 25.3173)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_escape_3].id)
			AND HAS_MODEL_LOADED(CARBONIZZARE)
				vehs[mvf_escape_3].id = CREATE_VEHICLE(CARBONIZZARE, << -2319.7976, 302.1291, 168.4667 >>, 115.6683)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_escape_4].id)
			AND HAS_MODEL_LOADED(DOMINATOR)
				vehs[mvf_escape_4].id = CREATE_VEHICLE(DOMINATOR, << -2289.5198, 412.0399, 173.4670 >>, 324.3684)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehs[mvf_escape_1].id)
			AND DOES_ENTITY_EXIST(vehs[mvf_escape_2].id)
			AND DOES_ENTITY_EXIST(vehs[mvf_escape_3].id)
			AND DOES_ENTITY_EXIST(vehs[mvf_escape_4].id)
				Unload_Asset_Model(sAssetData, VACCA)
			
				END_MISSION_EVENT(sData)				
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_ENEMY(PED_STRUCT &ped, MODEL_NAMES model, VECTOR vStartPos, FLOAT flHeading, VECTOR vDefensiveSphere, STRING strDebugName, BOOL bUseCover = TRUE)
	REL_GROUP_HASH relGroup
	WEAPON_TYPE wType
	
	SWITCH model
		CASE S_M_Y_SWAT_01
			relGroup = REL_FIB
			wType = weap_fib_rifle
		BREAK
		
		CASE S_M_M_CIASEC_01
			relGroup = REL_CIA
			wType = weap_cia_handgun
		BREAK
		
		CASE S_M_Y_BLACKOPS_01
			relGroup = REL_MW
			wType = weap_mw_rifle
		BREAK
		
		DEFAULT
			relGroup = REL_CIA
			wType = weap_cia_handgun
		BREAK
	ENDSWITCH
	Create_Mission_Ped(ped, model, vStartPos, flHeading, strDebugName, relGroup, wType, 2, 0, FALSE)
	Set_Ped_Combat_Params(ped, vDefensiveSphere, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.id, FALSE)
	TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped.id, 100.0)
	SET_PED_DIES_WHEN_INJURED(ped.id, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_USE_COVER, bUseCover)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id, FALSE)
	SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(ped.id)
ENDPROC

PROC SETUP_ENEMY_AND_TASK_TO_COORD(PED_STRUCT &ped, MODEL_NAMES model, VECTOR vStartPos, FLOAT flHeading, VECTOR vDest, VECTOR vAimCoord, STRING strDebugName, FLOAT flPedSpeed = PEDMOVE_WALK)
	REL_GROUP_HASH relGroup
	WEAPON_TYPE wType
	
	SWITCH model
		CASE S_M_Y_SWAT_01
			relGroup = REL_FIB
			wType = weap_fib_rifle
		BREAK
		
		CASE S_M_M_CIASEC_01
			relGroup = REL_CIA
			wType = weap_cia_handgun
		BREAK
		
		CASE S_M_Y_BLACKOPS_01
			relGroup = REL_MW
			wType = weap_mw_rifle
		BREAK
		
		DEFAULT
			relGroup = REL_CIA
			wType = weap_cia_handgun
		BREAK
	ENDSWITCH
	Create_Mission_Ped(ped, model, vStartPos, flHeading, strDebugName, relGroup, wType, 5, 0, FALSE)
	Set_Ped_Combat_Params(ped, vDest, 0.75, CM_WILLADVANCE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.id, FALSE)
	SET_PED_DIES_WHEN_INJURED(ped.id, TRUE)
	SAFE_OPEN_SEQUENCE()
		TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vDest, vAimCoord, flPedSpeed, TRUE)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100)
	SAFE_PERFORM_SEQUENCE(ped.id)
ENDPROC

PROC CREATE_TREVOR_HELICOPTER()
	vehs[mvf_trev_heli].id = CREATE_VEHICLE(mod_trev_heli, v_trev_heli_coord, f_trev_heli_heading)
	IF IS_ENTITY_OK(vehs[mvf_trev_heli].id)
		SET_VEHICLE_COLOURS(vehs[mvf_trev_heli].id, 34, 34)
		SET_VEHICLE_EXTRA_COLOURS(vehs[mvf_trev_heli].id, 0, 0)
		SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_trev_heli].id, i_trev_heli_variation)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_trev_heli].id, FALSE)
		Unload_Asset_Model(sAssetData,mod_trev_heli)
		
	ENDIF
ENDPROC


FUNC  INT GET_NUMBER_OF_PEDS_INJURED(PED_STRUCT &thesePeds[])

	INT i
	INT iCount
	
	FOR i = 0 TO COUNT_OF(thesePeds) -1
		IF IS_PED_INJURED(thesePeds[i].id)
		OR IS_PED_HURT(thesePeds[i].id)
			iCount++
		ENDIF
	ENDFOR
	
	RETURN iCount
	
ENDFUNC

FUNC PED_INDEX GET_ALIVE_PED_FROM_PED_STRUCT(PED_STRUCT &thesePeds[])

	INT i
	
	FOR i = 0 TO COUNT_OF(thesePeds) - 1
		IF IS_ENTITY_OK(thesePeds[i].id)
			RETURN thesePeds[i].id
		ENDIF
	ENDFOR
	
	RETURN NULL
ENDFUNC

PROC SET_ACCURACY_FOR_PED_STRUCT(PED_STRUCT &thesePeds[], INT iAccuracy)

	INT i
	
	FOR i = 0 TO COUNT_OF(thesePeds) -1
		IF IS_ENTITY_OK(thesePeds[i].id)
			SET_PED_ACCURACY(thesePeds[i].id, iAccuracy)
		ENDIF
	ENDFOR
	
ENDPROC

PROC CHECK_FOR_TREVOR_LOS_TO_PED(PED_STRUCT &ps)
	IF IS_ENTITY_OK(ps.id)
	AND (GET_PED_RELATIONSHIP_GROUP_HASH(ps.id) = REL_MW
	 OR GET_PED_RELATIONSHIP_GROUP_HASH(ps.id) = REL_CIA
	 OR GET_PED_RELATIONSHIP_GROUP_HASH(ps.id) = REL_FIB)
	
		IF CAN_PED_SEE_HATED_PED(TREV_PED_ID(), ps.id)
			ps.iTrevLOSCheckTimer = GET_GAME_TIMER()
			ps.bCanTrevorSeeThisPed = TRUE
		ELSE
			IF GET_GAME_TIMER() - ps.iTrevLOSCheckTimer > I_TREVOR_LOS_TEST_TIMEOUT
				ps.iTrevLOSCheckTimer = GET_GAME_TIMER()
				ps.bCanTrevorSeeThisPed = FALSE
			ENDIF
		ENDIF
	ELSE
		ps.bCanTrevorSeeThisPed = FALSE
	ENDIF
ENDPROC

PROC CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(PED_STRUCT &ps[])
	INT i 
	FOR i = 0 TO COUNT_OF(ps) - 1
		CHECK_FOR_TREVOR_LOS_TO_PED(ps[i])
	ENDFOR
ENDPROC

PROC DO_ALL_TREVOR_LOS_CHECKS()
	IF IS_ENTITY_OK(TREV_PED_ID())
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWarCIA)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWarFIB)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWarMWCourtyard)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWalkwayCIA1)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWalkwayFIB2)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psFinalWalkwayCIA)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWalkwayMW2)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psWalkwayMW3)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psStairwayMW)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psFIBIG_6)
		CHECK_FOR_TREVOR_LOS_TO_PED(m_psMWIG_7)
		CHECK_FOR_TREVOR_LOS_TO_PEDS_IN_ARRAY(m_psMWIG_8)
	ENDIF
ENDPROC


PROC SET_PED_TO_FIRE_MANY_BLANKS(PED_INDEX	&pi)
	IF IS_ENTITY_OK(pi)
		SET_PED_CHANCE_OF_FIRING_BLANKS(pi, flCONST_CHANCE_FIRE_BLANKS_MIN, flCONST_CHANCE_FIRE_BLANKS_MAX)
	ENDIF
ENDPROC



PROC CREATE_ENEMY_WAVE_PED(PED_STRUCT &ps, VECTOR vSpawnPoint, MODEL_NAMES model, TRIGGER_BOX tbDefensiveArea, INT &iNumSpawned, INT iAccuracy, BOOL bBlipPed, STRING strDebugName, EnemyWaveTaskingFunction fncn)
	WEAPON_TYPE 	wType
	FLOAT			flHeading
	TEXT_LABEL_15	tlDebugName
	REL_GROUP_HASH	relGroup
	
	SWITCH model
		CASE S_M_Y_SWAT_01
			relGroup = REL_FIB
			wType = weap_fib_rifle
			flHeading = 112.7339
		BREAK
		
		CASE S_M_M_CIASEC_01
			relGroup = REL_CIA
			wType = weap_cia_handgun
			flHeading = 203.4995
		BREAK
		
		CASE S_M_Y_BLACKOPS_01
			relGroup = REL_MW
			wType = weap_mw_rifle
			flHeading = 297.6012
		BREAK
	ENDSWITCH
	
	tlDebugName = strDebugName
	tlDebugName += iNumSpawned
	CPRINTLN(DEBUG_MISSION, "CREATE_ENEMY_WAVE_PED - Creating ped named '", tlDebugName, "'")
	iNumSpawned++
	Create_Mission_Ped(ps, model, vSpawnPoint, flHeading, tlDebugName, relGroup, wType, iAccuracy)
	
	IF IS_ENTITY_OK(ps.id)
		IF NOT bBlipPed
			SAFE_REMOVE_BLIP(ps.blip)
		ENDIF
		CALL fncn(ps, tbDefensiveArea)
		
	ENDIF
ENDPROC

PROC UPDATE_ENEMY_WAVE(ENEMY_WAVE &eWave, PED_STRUCT &psWaveEnemies[], VECTOR &vSpawnPoints[], MODEL_NAMES model, TRIGGER_BOX tbDefensiveArea, BOOL bDisplayDebug = FALSE)
	INT i, j
	
	IF eWave.state > ENEMY_WAVE_INIT
		IF eWave.bBlipPeds
			UPDATE_PED_STRUCT_ARRAY_BLIPS(psWaveEnemies)
		ENDIF
	ENDIF
	
	SWITCH eWave.state
		
		CASE ENEMY_WAVE_INIT
			// Create the initial group of enemies here
			FOR i = 0 TO COUNT_OF(psWaveEnemies) - 1
				CREATE_ENEMY_WAVE_PED(psWaveEnemies[i], vSpawnPoints[i], model, tbDefensiveArea, eWave.iNumSpawned, eWave.iAccuracy, eWave.bBlipPeds, eWave.strDebugName, eWave.taskFunction)
			ENDFOR
			RESTART_TIMER_NOW(eWave.tmrHeartbeat)
			IF bDisplayDebug
				CPRINTLN(DEBUG_MISSION, "UPDATE_ENEMY_WAVE - Going to ENEMY_WAVE_WAIT_FOR_INJURED")
			ENDIF
			eWave.bLookForSpawnPoints = TRUE
			eWave.state = ENEMY_WAVE_WAIT_FOR_INJURED
		BREAK
		
		CASE ENEMY_WAVE_WAIT_FOR_INJURED
			// Wait for a certain number of peds to be injured before creating any more
			IF GET_NUMBER_OF_PEDS_INJURED(psWaveEnemies) >= 2
				RESTART_TIMER_NOW(eWave.tmrHeartbeat)
				IF bDisplayDebug
					CPRINTLN(DEBUG_MISSION, "UPDATE_ENEMY_WAVE - Going to ENEMY_WAVE_CREATE_NEW_ENEMIES")
				ENDIF
				eWave.state = ENEMY_WAVE_CREATE_NEW_ENEMIES
			ENDIF
		BREAK
		
		CASE ENEMY_WAVE_CREATE_NEW_ENEMIES
			IF IS_TIMER_STARTED(eWave.tmrHeartbeat)
				IF GET_TIMER_IN_SECONDS(eWave.tmrHeartbeat) > eWave.flHeartbeatTime
					// Create the necessary enemies
					IF GET_NUMBER_OF_PEDS_INJURED(psWaveEnemies) = 0 
						IF bDisplayDebug
							CPRINTLN(DEBUG_MISSION, "UPDATE_ENEMY_WAVE - Going to ENEMY_WAVE_WAIT_FOR_INJURED")
						ENDIF
						eWave.state = ENEMY_WAVE_WAIT_FOR_INJURED
					ENDIF
					
					FOR i = 0 TO COUNT_OF (psWaveEnemies) - 1
						IF IS_ENTITY_DEAD(psWaveEnemies[i].id)
							FOR j = 0 TO COUNT_OF(vSpawnPoints) - 1
								IF eWave.bLookForSpawnPoints
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vSpawnPoints[j]) >= 15.0
									AND NOT IS_SPHERE_VISIBLE(vSpawnPoints[j], 0.5)
										RESTART_TIMER_NOW(eWave.tmrHeartbeat)
										CREATE_ENEMY_WAVE_PED(psWaveEnemies[i], vSpawnPoints[j], model, tbDefensiveArea, eWave.iNumSpawned, eWave.iAccuracy, eWave.bBlipPeds, eWave.strDebugName, eWave.taskFunction)
										eWave.bLookForSpawnPoints = FALSE
										IF eWave.iMaxNumber <> -1
											IF eWave.iNumSpawned >= eWave.iMaxNumber
												IF bDisplayDebug
													CPRINTLN(DEBUG_MISSION, "UPDATE_ENEMY_WAVE - We've created all the enemies we were going to, going to ENEMY_WAVE_DONE")
												ENDIF
												eWave.state = ENEMY_WAVE_DONE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
						eWave.bLookForSpawnPoints = TRUE
					ENDFOR
				ENDIF
			ELSE
				RESTART_TIMER_NOW(eWave.tmrHeartbeat)
			ENDIF
		BREAK
		
		CASE ENEMY_WAVE_DONE
			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_ENEMY_WAVE_DONE_SPAWNING(ENEMY_WAVE &eWave)
	RETURN (eWave.state = ENEMY_WAVE_DONE OR (eWave.iNumSpawned >= eWave.iMaxNumber))
ENDFUNC

PROC ENEMY_WAVE_DEFAULT_TASK(PED_STRUCT &ps, TRIGGER_BOX tbDefensiveArea)
	CPRINTLN(DEBUG_MISSION, "ENEMY_WAVE_DEFAULT_TASK")
	IF IS_ENTITY_OK(ps.id)
		SET_PED_ANGLED_DEFENSIVE_AREA(ps.id, tbDefensiveArea.vMin, tbDefensiveArea.vMax, tbDefensiveArea.flWidth)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(ps.id, 250)
		SET_PED_TO_FIRE_MANY_BLANKS(ps.id)
	ENDIF
ENDPROC

PROC CREATE_ENEMY_WAVE(ENEMY_WAVE &wave, INT iAccuracy, STRING strDebugName, EnemyWaveTaskingFunction taskFunc, FLOAT flHeartbeat = flCONST_WAVE_HEARTBEAT, BOOL bBlipPeds = TRUE, INT iMaxNumber = -1)
	wave.state = ENEMY_WAVE_INIT
	wave.iNumSpawned = 0
	wave.iAccuracy = iAccuracy
	wave.iMaxNumber = iMaxNumber
	wave.strDebugName = strDebugName
	wave.flHeartbeatTime = flHeartbeat
	wave.bBlipPeds = bBlipPeds
	wave.taskFunction = taskFunc
	wave.bLookForSpawnPoints = TRUE
ENDPROC

PROC CHANGE_DEFENSIVE_AREA_FOR_PEDS(PED_STRUCT &ps[], TRIGGER_BOX tb)
	INT i 
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF IS_ENTITY_OK(ps[i].id)
			REMOVE_PED_DEFENSIVE_AREA(ps[i].id, TRUE)
			SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(ps[i].id, tb)
		ENDIF
	ENDFOR
ENDPROC

PROC SET_COMBAT_RANGE_FOR_PEDS(PED_STRUCT &ps[], COMBAT_RANGE cr)
	INT i 
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF IS_ENTITY_OK(ps[i].id)
			SET_PED_COMBAT_RANGE(ps[i].id, cr)
		ENDIF
	ENDFOR
ENDPROC

PROC TASK_PED_STRUCT_ATTACK_PLAYER(PED_STRUCT &ps[])
	INT i 
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF IS_ENTITY_OK(ps[i].id)
			CLEAR_PED_TASKS_IMMEDIATELY(ps[i].id)
			TASK_COMBAT_PED(ps[i].id, PLAYER_PED_ID())
		ENDIF
	ENDFOR
ENDPROC


PROC GET_DAVE_OUT()
	//VECTOR vAimCoord = <<-2313.83032, 274.61502, 168.60179>>
	CPRINTLN(DEBUG_MISSION, "GET_DAVE_OUT")
	IF IS_ENTITY_OK(m_pedAllyDave)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedAllyDave, TRUE)
		TASK_FOLLOW_WAYPOINT_RECORDING(m_pedAllyDave, m_strDaveWaypointRec, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
	ENDIF
ENDPROC


PROC UPDATE_PED_TARGETS_THROUGH_PARKING_LOT(PED_INDEX &ped, structTimer &timer, BOOL bReverseOrder = FALSE)
	INT i 
	IF IS_ENTITY_OK(ped)
		IF IS_TIMER_STARTED(timer)
			IF GET_TIMER_IN_SECONDS(timer) >= 2.0
				IF bReverseOrder
					FOR i = ENUM_TO_INT(mpf_pl_1_mw_5) TO ENUM_TO_INT(mpf_pl_1_mw_1) STEP -1
						IF IS_ENTITY_OK(peds[i].id)
							WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(ped, peds[i].id, FALSE)
							RESTART_TIMER_NOW(timer)
						ENDIF
					ENDFOR
				ELSE
					FOR i = ENUM_TO_INT(mpf_pl_1_mw_1) TO ENUM_TO_INT(mpf_pl_1_mw_5)
						IF IS_ENTITY_OK(peds[i].id)
							WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(ped, peds[i].id, FALSE)
							RESTART_TIMER_NOW(timer)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ELSE
			RESTART_TIMER_NOW(timer)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL	ARE_PARKING_LOT_ENEMIES_DEAD()
	IF (IS_ENTITY_DEAD(peds[mpf_pl_1_mw_1].id)
	AND IS_ENTITY_DEAD(peds[mpf_pl_1_mw_2].id)
	AND IS_ENTITY_DEAD(peds[mpf_pl_1_mw_3].id)
	AND IS_ENTITY_DEAD(peds[mpf_pl_1_mw_4].id)
	AND IS_ENTITY_DEAD(peds[mpf_pl_1_mw_5].id))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_DAVE_FLEEING_MUSEUM()
	IF IS_ENTITY_OK(m_pedAllyDave)
		SWITCH eDaveFleeState
			CASE DAVE_FLEE_INIT
				IF NOT DOES_ENTITY_EXIST(vehs[mvf_dave_car].id)
					vehs[mvf_dave_car].id = CREATE_VEHICLE(CARBONIZZARE, << -2336.49512, 271.24817, 168.46722 >>, 203.97)
					IF IS_ENTITY_OK(vehs[mvf_dave_car].id)
						SET_ENTITY_PROOFS(vehs[mvf_dave_car].id, TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_dave_car].id, FALSE)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_TO_PARKING_LOT")
				eDaveFleeState = DAVE_FLEE_TASK_TO_PARKING_LOT
			BREAK
			
			CASE DAVE_FLEE_TASK_TO_PARKING_LOT
				GET_DAVE_OUT()
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_TO_START_ATTACKING")
				eDaveFleeState = DAVE_FLEE_WAIT_TO_START_ATTACKING
			BREAK
			
			CASE DAVE_FLEE_WAIT_TO_START_ATTACKING
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(m_pedAllyDave)
					IF GET_PED_WAYPOINT_PROGRESS(m_pedAllyDave) >= 29
						WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(m_pedAllyDave, peds[mpf_pl_1_mw_1].id, FALSE)
						CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN")
						eDaveFleeState = DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(m_pedAllyDave, PEDMOVE_RUN)
				UPDATE_PED_TARGETS_THROUGH_PARKING_LOT(m_pedAllyDave, m_tmrDaveReassessTargets)
				IF GET_PED_WAYPOINT_PROGRESS(m_pedAllyDave) >= 53
				OR ARE_PARKING_LOT_ENEMIES_DEAD()
					WAYPOINT_PLAYBACK_STOP_AIMING_OR_SHOOTING(m_pedAllyDave)
					CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR")
					eDaveFleeState = DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(m_pedAllyDave, PEDMOVE_SPRINT)
				IF IS_ENTITY_OK(vehs[mvf_dave_car].id)
				AND IS_ENTITY_OK(m_pedAllyDave)
					IF GET_DISTANCE_BETWEEN_ENTITIES(m_pedAllyDave, vehs[mvf_dave_car].id) <= 7.0
						CLEAR_PED_TASKS(m_pedAllyDave)
						m_bDavePlayGetInCarMessage = TRUE
						TASK_ENTER_VEHICLE(m_pedAllyDave, vehs[mvf_dave_car].id)
						CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_UNTIL_IN_CAR")
						eDaveFleeState = DAVE_FLEE_WAIT_UNTIL_IN_CAR
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_WAIT_UNTIL_IN_CAR
				IF IS_ENTITY_OK(vehs[mvf_dave_car].id)
				AND IS_ENTITY_OK(m_pedAllyDave)
					IF IS_PED_IN_VEHICLE(m_pedAllyDave, vehs[mvf_dave_car].id)
						SAFE_REMOVE_BLIP(m_blipAllyDave)
						CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_GTFO")
						eDaveFleeState = DAVE_FLEE_TASK_GTFO
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_TASK_GTFO
				IF IS_ENTITY_OK(vehs[mvf_dave_car].id)
				AND IS_ENTITY_OK(m_pedAllyDave)
					//TASK_VEHICLE_MISSION(m_pedAllyDave, vehs[mvf_dave_car].id, NULL, MISSION_FLEE, 60, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
					//TASK_SMART_FLEE_PED(m_pedAllyDave, PLAYER_PED_ID(), 500, -1)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_dave_car].id, rec_dave_esc, str_carrecs)
					Unload_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
					SET_PLAYBACK_SPEED(vehs[mvf_dave_car].id, 1.15)
					CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_AI_DRIVING")
					eDaveFleeState = DAVE_FLEE_TASK_AI_DRIVING
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_TASK_AI_DRIVING
				IF IS_ENTITY_OK(vehs[mvf_dave_car].id)
				AND IS_ENTITY_OK(m_pedAllyDave)
					IF GET_SCRIPT_TASK_STATUS( m_pedAllyDave, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE ) > PERFORMING_TASK
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_dave_car].id)
							IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_dave_car].id) > 25000
								STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_dave_car].id)
								CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - task Dave to drive out of lot")
								TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE( m_pedAllyDave, vehs[mvf_dave_car].id, <<-1809.1891, 72.2607, 70.6699>>, 120, DRIVINGMODE_AVOIDCARS_RECKLESS, 2.0 )
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( m_pedAllyDave, <<-1809.1891, 72.2607, 70.6699>>, <<15,15,15>> )
							CLEAR_PED_TASKS(m_pedAllyDave)
							TASK_VEHICLE_MISSION_PED_TARGET(m_pedAllyDave, vehs[mvf_dave_car].id, PLAYER_PED_ID(), MISSION_FLEE, 120, DRIVINGMODE_AVOIDCARS_RECKLESS, -1, -1)
							CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_WAIT_TO_DELETE")
							eDaveFleeState = DAVE_FLEE_TASK_WAIT_TO_DELETE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_TASK_WAIT_TO_DELETE
				IF IS_ENTITY_OK(vehs[mvf_dave_car].id)
				AND IS_ENTITY_OK(m_pedAllyDave)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(m_pedAllyDave)) > 10000
					AND NOT IS_ENTITY_ON_SCREEN(m_pedAllyDave)
						DELETE_PED(m_pedAllyDave)
						DELETE_VEHICLE(vehs[mvf_dave_car].id)
						CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_FLEEING_MUSEUM - going to DAVE_FLEE_DONE")
						eDaveFleeState = DAVE_FLEE_DONE
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_DONE
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


PROC GET_OTHER_PLAYER_OUT()
	CPRINTLN(DEBUG_MISSION, "GET_OTHER_PLAYER_OUT")
	IF IS_ENTITY_OK(m_pedOtherPlayer)
		IF IS_ENTITY_IN_TRIGGER_BOX(m_tbRoofObjective, m_pedOtherPlayer)
			CLEAR_PED_TASKS_IMMEDIATELY(m_pedOtherPlayer)
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<-2222.94434, 197.51961, 193.61169>>), PEDMOVE_SPRINT, -1)
				TASK_STAND_STILL(NULL, -1)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(m_pedOtherPlayer, seq)
			CLEAR_SEQUENCE_TASK(seq)
		ELSE 
			OPEN_SEQUENCE_TASK(seq)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
				TASK_PAUSE(NULL, 1000)
				IF NOT IS_ENTITY_IN_TRIGGER_BOX(m_tbParkingLotObjective, m_pedOtherPlayer)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<-2278.46899, 300.54025, 183.61159>>), PEDMOVE_SPRINT, -1)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<-2275.21875, 276.01495, 173.60211>>), PEDMOVE_SPRINT, -1)
				ENDIF
				TASK_FOLLOW_WAYPOINT_RECORDING(NULL, m_strOtherPlayerWaypointRec, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)	
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(m_pedOtherPlayer, seq)
			CLEAR_SEQUENCE_TASK(seq)
		ENDIF
	ENDIF
ENDPROC

structTimer	m_tmrWaitToDrive
PROC UPDATE_OTHER_PLAYER_FLEEING_MUSEUM()
	
	IF IS_ENTITY_OK(m_pedOtherPlayer)
		SWITCH eOtherPlayerFleeState
			CASE DAVE_FLEE_INIT
				IF NOT DOES_ENTITY_EXIST(vehs[mvf_other_player_car].id)
					vehs[mvf_other_player_car].id = CREATE_VEHICLE(DOMINATOR, << -2346.8799, 279.8263, 168.7561 >>, 114.33)
					IF IS_ENTITY_OK(vehs[mvf_other_player_car].id)
						SET_ENTITY_PROOFS(vehs[mvf_other_player_car].id, TRUE, TRUE, TRUE, TRUE, TRUE)
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_other_player_car].id, FALSE)
						ENDIF
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_TO_PARKING_LOT")
				eOtherPlayerFleeState = DAVE_FLEE_TASK_TO_PARKING_LOT
			BREAK
			
			CASE DAVE_FLEE_TASK_TO_PARKING_LOT
				GET_OTHER_PLAYER_OUT()
				CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_TO_START_ATTACKING")
				eOtherPlayerFleeState = DAVE_FLEE_WAIT_TO_START_ATTACKING
			BREAK
			
			CASE DAVE_FLEE_WAIT_TO_START_ATTACKING
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(m_pedOtherPlayer)
					IF GET_PED_WAYPOINT_PROGRESS(m_pedOtherPlayer) >= 33
						IF IS_ENTITY_OK(peds[mpf_pl_1_mw_5].id)
							WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(m_pedOtherPlayer, peds[mpf_pl_1_mw_5].id, FALSE)
						ENDIF
						CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN")
						eOtherPlayerFleeState = DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_WAIT_TO_START_RUNNING_AGAIN
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(m_pedOtherPlayer)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(m_pedOtherPlayer, PEDMOVE_RUN)
					UPDATE_PED_TARGETS_THROUGH_PARKING_LOT(m_pedOtherPlayer, m_tmrOtherPlayerReassessTargets, TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(m_pedOtherPlayer) >= 53
					OR ARE_PARKING_LOT_ENEMIES_DEAD()
						WAYPOINT_PLAYBACK_STOP_AIMING_OR_SHOOTING(m_pedOtherPlayer)
						CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR")
						eOtherPlayerFleeState = DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_WAIT_FOR_DAVE_AT_CAR
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(m_pedOtherPlayer)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(m_pedOtherPlayer, PEDMOVE_SPRINT)
					IF IS_ENTITY_OK(vehs[mvf_other_player_car].id)
					AND IS_ENTITY_OK(m_pedOtherPlayer)
						IF GET_DISTANCE_BETWEEN_ENTITIES(m_pedOtherPlayer, vehs[mvf_other_player_car].id) <= 15.0
							CLEAR_PED_TASKS(m_pedOtherPlayer)
							TASK_ENTER_VEHICLE(m_pedOtherPlayer, vehs[mvf_other_player_car].id)
							CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_WAIT_UNTIL_IN_CAR")
							eOtherPlayerFleeState = DAVE_FLEE_WAIT_UNTIL_IN_CAR
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_WAIT_UNTIL_IN_CAR
				IF IS_ENTITY_OK(vehs[mvf_other_player_car].id)
				AND IS_ENTITY_OK(m_pedOtherPlayer)
					IF IS_PED_IN_VEHICLE(m_pedOtherPlayer, vehs[mvf_other_player_car].id)
						RESTART_TIMER_NOW(m_tmrWaitToDrive)
						CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_GTFO")
						eOtherPlayerFleeState = DAVE_FLEE_TASK_GTFO
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_TASK_GTFO
				IF IS_ENTITY_OK(vehs[mvf_other_player_car].id)
				AND IS_ENTITY_OK(m_pedOtherPlayer)
				AND GET_TIMER_IN_SECONDS(m_tmrWaitToDrive) >= 4.0
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_other_player_car].id, rec_otherp_esc, str_carrecs)
					UnLoad_Asset_Recording(sAssetData, rec_otherp_esc, str_carrecs)
					SET_PLAYBACK_SPEED(vehs[mvf_other_player_car].id, 1.15)
					CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_AI_DRIVING")
					eOtherPlayerFleeState = DAVE_FLEE_TASK_AI_DRIVING
				ENDIF
			BREAK
			
			
			CASE DAVE_FLEE_TASK_AI_DRIVING
				IF IS_ENTITY_OK(m_pedOtherPlayer)
					IF GET_SCRIPT_TASK_STATUS( m_pedOtherPlayer, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE ) > PERFORMING_TASK
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_other_player_car].id)
							IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_other_player_car].id) > 16000
								STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_other_player_car].id)
								CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - task other player to drive out of lot")
								TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE( m_pedOtherPlayer, vehs[mvf_other_player_car].id, <<-1809.1891, 72.2607, 70.6699>>, 120, DRIVINGMODE_AVOIDCARS_RECKLESS, 2.0 )
							ENDIF
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(m_pedOtherPlayer, (<<-1809.1891, 72.2607, 70.6699>>)) <= 15.0
							CLEAR_PED_TASKS(m_pedOtherPlayer)
							TASK_VEHICLE_MISSION_PED_TARGET(m_pedOtherPlayer, vehs[mvf_other_player_car].id, PLAYER_PED_ID(), MISSION_FLEE, 120, DRIVINGMODE_AVOIDCARS_RECKLESS, -1, -1)
							CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_TASK_WAIT_TO_DELETE")
							eOtherPlayerFleeState = DAVE_FLEE_TASK_WAIT_TO_DELETE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_TASK_WAIT_TO_DELETE
				IF IS_ENTITY_OK(vehs[mvf_other_player_car].id)
				AND IS_ENTITY_OK(m_pedOtherPlayer)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(m_pedOtherPlayer)) > 10000
					AND NOT IS_ENTITY_ON_SCREEN(m_pedOtherPlayer)
						DELETE_PED(m_pedOtherPlayer)
						DELETE_VEHICLE(vehs[mvf_other_player_car].id)
						CPRINTLN(DEBUG_MISSION, "UPDATE_OTHER_PLAYER_FLEEING_MUSEUM - going to DAVE_FLEE_DONE")
						eOtherPlayerFleeState = DAVE_FLEE_DONE
					ENDIF
				ENDIF
			BREAK
			
			CASE DAVE_FLEE_DONE
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC



FUNC BOOL FIND_TARGET_FROM_PEDSTRUCT(PED_STRUCT &ps[], PED_STRUCT &targetPed)
	INT i
	FLOAT fDist = 9999999
	PED_STRUCT 	tempTarget
	
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF DOES_ENTITY_EXIST(ps[i].id)
		AND NOT IS_PED_INJURED(ps[i].id)
		AND (GET_PED_RELATIONSHIP_GROUP_HASH(ps[i].id) = REL_CIA
		OR GET_PED_RELATIONSHIP_GROUP_HASH(ps[i].id) = REL_MW
		OR GET_PED_RELATIONSHIP_GROUP_HASH(ps[i].id) = REL_FIB)
 			IF ps[i].bCanTrevorSeeThisPed
			
				VECTOR vMikeCoord	= GET_ENTITY_COORDS(MIKE_PED_ID())
				VECTOR vEnemyCoord	= GET_ENTITY_COORDS(ps[i].id)
				FLOAT tempDist 
				tempDist = VDIST2(vMikeCoord, vEnemyCoord)
				// Apply a z height bias so that peds on the same level as micahel are more likely to be selected
				tempDist += ABSF(vMikeCoord.z - vEnemyCoord.z)
				
				IF tempDist < fDist
					fDist 	= tempDist
					tempTarget 	= ps[i]
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
		
	IF IS_ENTITY_OK(tempTarget.id)
		targetPed = tempTarget
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds a target for trevor to aim at. 
/// PARAMS:
///    targetPed - the enum index of the ped
/// RETURNS:
///    Returns TRUE if a target is found
FUNC BOOL FIND_TARGET_FOR_TREVOR(PED_STRUCT &targetPed)
	
	IF FIND_TARGET_FROM_PEDSTRUCT(m_psWarCIA, targetPed)
		RETURN TRUE
	ENDIF
	
	IF FIND_TARGET_FROM_PEDSTRUCT(m_psWarFIB, targetPed)
		RETURN TRUE
	ENDIF
	
	IF FIND_TARGET_FROM_PEDSTRUCT(m_psWarMWCourtyard, targetPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC UPDATE_TREVOR_AI()
	
	IF DOES_ENTITY_EXIST(TREV_PED_ID())
	AND PLAYER_PED_ID() != TREV_PED_ID()
	
		// >>>>>>>>>>> PROCESS AI STATE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		ai_trevor_prevFrame = ai_trevor
		
		SET_COMBAT_FLOAT(TREV_PED_ID(), CCF_BURST_DURATION_IN_COVER, 5.0)
		SET_COMBAT_FLOAT(TREV_PED_ID(), CCF_TIME_BETWEEN_BURSTS_IN_COVER, 1.25)
		SET_COMBAT_FLOAT(TREV_PED_ID(), CCF_WEAPON_ACCURACY, 1.0)
		SET_COMBAT_FLOAT(TREV_PED_ID(), CCF_BLIND_FIRE_CHANCE, 0.0)
		SET_COMBAT_FLOAT(TREV_PED_ID(), CCF_TIME_BETWEEN_PEEKS, 5.0)
		SET_COMBAT_FLOAT(TREV_PED_ID(), CCF_MAX_SHOOTING_DISTANCE, 500.0)
		SET_PED_COMBAT_RANGE(TREV_PED_ID(), CR_FAR)
		SET_PED_COMBAT_ATTRIBUTES(TREV_PED_ID(), CA_REQUIRES_LOS_TO_SHOOT, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(TREV_PED_ID(), CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TREV_PED_ID(), CA_DISABLE_PINNED_DOWN, TRUE)
		//SET_PED_FIRING_PATTERN(TREV_PED_ID(), FIRING_PATTERN_SINGLE_SHOT)
		
		SWITCH ai_trevor
			
			CASE AI_T_MOVING_TO_SNIPING_POS
				
				IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(TREV_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				AND GET_SEQUENCE_PROGRESS(TREV_PED_ID()) = 1
				AND IS_PED_IN_COVER(TREV_PED_ID())
				AND ai_trevor = ai_trevor_prevFrame
					SET_TREV_AI_STATE(AI_T_IN_COVER_NO_TARGET)
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: Trevor is in position")
				ENDIF
			BREAK
			
			CASE AI_T_IN_COVER_NO_TARGET		
			
				IF NOT IS_ENTITY_OK(pedCurrentTrevTarget.id)
					// Look for a target
					IF FIND_TARGET_FOR_TREVOR(pedCurrentTrevTarget)
						SET_TREV_AI_STATE(AI_T_IN_COVER_WITH_TARGET)
						CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: A new target was found")
					ELSE
						CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: NO target found")
					ENDIF
					iTrevTargetUpdateTimer = GET_GAME_TIMER()
				ELSE
					SET_TREV_AI_STATE(AI_T_IN_COVER_WITH_TARGET)
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: Existing target found")
				ENDIF
			BREAK
			
			CASE AI_T_IN_COVER_WITH_TARGET
				
				IF IS_ENTITY_OK(pedCurrentTrevTarget.id)
					
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedCurrentTrevTarget.id) + <<0,0, 1.3>>, 0.25, 255,0,0,255)

					// Can the ped be seen?
					IF NOT pedCurrentTrevTarget.bCanTrevorSeeThisPed
						pedCurrentTrevTarget.id = NULL
						SET_TREV_AI_STATE(AI_T_IN_COVER_NO_TARGET)
						CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: Target can't be seen, reset target to NULL")
				
					// Ped has moved away from michael, pick a new target
					ELIF GET_GAME_TIMER() - iTrevTargetUpdateTimer >= I_TREVOR_ENEMY_DIST_TIMER
					AND pedCurrentTrevTarget.fSquareDistFromMike > 625 // (25 squared)
						pedCurrentTrevTarget.id = NULL
						SET_TREV_AI_STATE(AI_T_IN_COVER_NO_TARGET)
						CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: Target moved away from Michael, reset target to NULL")
						
					// Had the same target for too long
					ELIF GET_GAME_TIMER() - iTrevTargetUpdateTimer >= I_TREVOR_TARGET_TIMEOUT
						pedCurrentTrevTarget.id = NULL
						SET_TREV_AI_STATE(AI_T_IN_COVER_NO_TARGET)
						CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: Current target timed out, reset target to NULL")
						
					ENDIF
				ELSE
					pedCurrentTrevTarget.id = NULL
					SET_TREV_AI_STATE(AI_T_IN_COVER_NO_TARGET)
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: Target was killed/deleted, reset target to NULL")
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		// >>>>>>>>>>> PROCESS AI TASKS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		SWITCH ai_trevor
			CASE AI_T_MOVING_TO_SNIPING_POS 

				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(TREV_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				OR ai_trevor != ai_trevor_prevFrame
					
					SAFE_OPEN_SEQUENCE()
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<-2267.479980,235.144760,193.601273>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 1.0)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(null, <<-2267.479980,235.144760,193.601273>>, -1, FALSE, 0, TRUE, FALSE, NULL, TRUE)
					SAFE_PERFORM_SEQUENCE(TREV_PED_ID())
					
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: TO COVER SEQUENCE")
					
				ENDIF
				
			BREAK
			
			CASE AI_T_IN_COVER_NO_TARGET
			
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(TREV_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER_FROM_TARGET)
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(TREV_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				
					TASK_PUT_PED_DIRECTLY_INTO_COVER(TREV_PED_ID(), <<-2267.479980,235.144760,193.601273>>, -1, FALSE, 0, TRUE, FALSE, NULL)
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: TASK_PUT_PED_DIRECTLY_INTO_COVER")
					
				ENDIF
			
			BREAK
			
			CASE AI_T_IN_COVER_WITH_TARGET
				
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(TREV_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER_FROM_TARGET)
				
					TASK_PUT_PED_DIRECTLY_INTO_COVER_FROM_TARGET(TREV_PED_ID(), pedCurrentTrevTarget.id, null, <<0,0,0>>, <<-2267.479980,235.144760,193.601273>>, -1, TRUE, 0, TRUE, FALSE, NULL)
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: TASK_PUT_PED_DIRECTLY_INTO_COVER_FROM_TARGET")
					
				ELIF ai_trevor != ai_trevor_prevFrame
				
					SET_COVER_TASK_TARGET(TREV_PED_ID(), pedCurrentTrevTarget.id, null, <<0,0,0>>)
					CDEBUG1LN(DEBUG_MISSION, "********** TREVOR AI: SET_COVER_TASK_TARGET")
				
				ENDIF
				
			BREAK
		ENDSWITCH
	ENDIF 
ENDPROC



PROC TASK_REMAINING_MIKE_ONLY_WAVE_2_PEDS_IF_TREVOR()
	INT i 
	FOR i = 0 TO COUNT_OF(m_psMichaelSecondWave) - 1
		IF IS_ENTITY_OK(m_psMichaelSecondWave[i].id)
			REMOVE_PED_DEFENSIVE_AREA(m_psMichaelSecondWave[i].id)
			SET_PED_ANGLED_DEFENSIVE_AREA(m_psMichaelSecondWave[i].id, <<-2225.684814,303.013367,183.353027>>, <<-2241.656250,295.691162,186.605148>>, 3.500000)
			SET_PED_COMBAT_ATTRIBUTES(m_psMichaelSecondWave[i].id, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(m_psMichaelSecondWave[i].id, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(m_psMichaelSecondWave[i].id, CA_CAN_CHARGE, FALSE)
		ENDIF
	ENDFOR
ENDPROC

PROC TASK_REMAINING_MIKE_ONLY_WAVE_2_PEDS_IF_MIKE()
	IF IS_ENTITY_OK(m_psMichaelSecondWave[0].id)
		SET_PED_ACCURACY(m_psMichaelSecondWave[0].id, 5)
		SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMichaelSecondWave[0].id)
		REMOVE_PED_DEFENSIVE_AREA(m_psMichaelSecondWave[0].id)
		SET_PED_SPHERE_DEFENSIVE_AREA(m_psMichaelSecondWave[0].id, (<<-2224.53247, 302.57278, 183.60303>>), 5.0, TRUE)
	ENDIF
	
	IF IS_ENTITY_OK(m_psMichaelSecondWave[1].id)
		SET_PED_ACCURACY(m_psMichaelSecondWave[1].id, 5)
		SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMichaelSecondWave[1].id)
		REMOVE_PED_DEFENSIVE_AREA(m_psMichaelSecondWave[1].id)
		SET_PED_SPHERE_DEFENSIVE_AREA(m_psMichaelSecondWave[1].id, (<<-2220.86694, 295.16733, 183.60303>>), 5.0, TRUE)
	ENDIF
ENDPROC



PROC UPDATE_PLAYER_SWITCHING()

	SET_PED_RESET_FLAG( MIKE_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE )
	SET_PED_RESET_FLAG( TREV_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE )
	
	SWITCH ePlayerSwitchState
		CASE PLAYER_SWITCH_SETUP
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, TRUE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
			CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Going to PLAYER_SWITCH_SELECTING ")
			ePlayerSwitchState = PLAYER_SWITCH_SELECTING
		BREAK
		CASE PLAYER_SWITCH_SELECTING
			IF UPDATE_SELECTOR_HUD(sSelectorPeds,IS_PLAYER_CONTROL_ON(PLAYER_ID()))     // Returns TRUE when the player has made a selection
				IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MICHAEL)
				OR HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_TREVOR)
					sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
					sCamDetails.bRun  = TRUE
					
					GET_CURRENT_PED_WEAPON(MIKE_PED_ID(), wtMichaelPreviousWeapon)
					GET_CURRENT_PED_WEAPON(TREV_PED_ID(), wtTrevorPreviousWeapon)
					
					IF m_bFirstSwitch
//						IF IS_ENTITY_AT_COORD(MIKE_PED_ID(), csMichaelStartingCover.pos, (<<2,2,2>>))
//							CPRINTLN(DEBUG_MISSION, "Tasking Mike into cover before the swap")
//							CLEAR_PED_TASKS_IMMEDIATELY(MIKE_PED_ID())
//							TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csMichaelStartingCover.pos, 5000, TRUE, 0, TRUE, FALSE, csMichaelStartingCover.id)
//						ENDIF
						IF PREPARE_MUSIC_EVENT("MIC3_SNIPE")
							TRIGGER_MUSIC_EVENT("MIC3_SNIPE")
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
						ENDIF
					ENDIF
					
					CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Going to PLAYER_SWITCH_SWAP_PLAYER_PEDS ")
					ePlayerSwitchState = PLAYER_SWITCH_SWAP_PLAYER_PEDS
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYER_SWITCH_SWAP_PLAYER_PEDS
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
							INFORM_MISSION_STATS_OF_INCREMENT(MIC3_SWITCHES)
							IF m_bFirstSwitch
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), csMichaelStartingCover.pos, (<<2,2,2>>))
									CPRINTLN(DEBUG_MISSION, "Tasking Mike into cover after the swap")
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), csMichaelStartingCover.pos, 5000, TRUE, 0, TRUE, FALSE, csMichaelStartingCover.id)
								ENDIF
								
								IF int_to_enum(mission_stage_flag, mission_stage) = STAGE_DAVE_BY_FOUNTAIN
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2209.834473,240.265152,183.354874>>, <<-2228.960449,280.903229,186.604080>>, 13.000000)
										Create_Mission_Ped(m_psWalkwayFIB2[2], mod_ped_fib, (<<-2235.64282, 296.48892, 183.60515>>), 203, "WKWAY_FIB2_3", REL_FIB_FINAL, weap_fib_rifle)
										REMOVE_PED_DEFENSIVE_AREA(m_psWalkwayFIB2[2].id)
										SET_PED_SPHERE_DEFENSIVE_AREA(m_psWalkwayFIB2[2].id, (<<-2235.64282, 296.48892, 183.60515>>), 2.0)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psWalkwayFIB2[2].id, 100)
									ENDIF
								ENDIF
								m_bFirstSwitch = FALSE
							ENDIF
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								IF int_to_enum(mission_stage_flag, mission_stage) = STAGE_DAVE_BY_FOUNTAIN
								OR int_to_enum(mission_stage_flag, mission_stage) = STAGE_DAVE_ESCAPES
									IF IS_ENTITY_IN_TRIGGER_BOX(m_tbLargeRoofVolume, TREV_PED_ID())
										CPRINTLN(DEBUG_MISSION, "Trevor's on the roof, set the camera down")
										SET_GAMEPLAY_CAM_RELATIVE_PITCH(-30)
									ENDIF
								ENDIF
							ENDIF
							SAFE_REMOVE_BLIP(blipOtherPlayer)
							SAFE_REMOVE_BLIP(peds[mpf_mike].blip)
							SAFE_REMOVE_BLIP(peds[mpf_trev].blip)
							CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Going to PLAYER_SWITCH_COMPLETE ")
							ePlayerSwitchState = PLAYER_SWITCH_COMPLETE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE PLAYER_SWITCH_COMPLETE
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails) 		
				//switch still in progress
			ELSE
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
				SAFE_REMOVE_BLIP(blipOtherPlayer)
				SAFE_REMOVE_BLIP(peds[mpf_mike].blip)
				SAFE_REMOVE_BLIP(peds[mpf_trev].blip)
				IF IS_ENTITY_OK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
				ENDIF
				IF IS_ENTITY_OK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
				ENDIF
				
				IF IS_ENTITY_OK(PLAYER_PED_ID())
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ENDIF
				
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = GET_PLAYER_PED_MODEL(CHAR_TREVOR) 
				//	SAFE_BLIP_PED(blipOtherPlayer, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
					peds[mpf_trev].id = PLAYER_PED_ID()
					m_pedOtherPlayer = MIKE_PED_ID()
					ADD_PED_FOR_DIALOGUE(sConvo, 0, peds[mpf_mike].id, "MICHAEL")
					ADD_PED_FOR_DIALOGUE(sConvo, 2, PLAYER_PED_ID(), "TREVOR")
					
					SET_CURRENT_PED_WEAPON(TREV_PED_ID(), wtTrevorPreviousWeapon, TRUE)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(MIKE_PED_ID(), "MI_3_MICHAEL_GROUP")
					STOP_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
					START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
					
					//TODO:  Hook up AI tasking here for Michael, switch-statement based on his current position on the walkway
					SWITCH int_to_enum(mission_stage_flag, mission_stage)
						CASE STAGE_DAVE_BY_FOUNTAIN
							SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
							IF bSafeToTaskMichaelFIBEnemies
								TASK_REMAINING_MIKE_ONLY_WAVE_2_PEDS_IF_TREVOR()
							ENDIF
							IF IS_ENTITY_IN_TRIGGER_BOX(m_tbMichaelTaskZone1, MIKE_PED_ID())
								CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - STAGE_DAVE_BY_FOUNTAIN - Michael Tasked to MICHAEL_AI_WAIT_FOR_SECOND_ENEMIES_SPAWNED ")
								TASK_PED_RUN_TO_COORD(MIKE_PED_ID(), (<<-2229.82471, 278.22928, 183.60408>>), FALSE)
								eMichaelAIState = MICHAEL_AI_WAIT_FOR_SECOND_ENEMIES_SPAWNED
							ELIF IS_ENTITY_IN_TRIGGER_BOX(m_tbMichaelTaskZone2, MIKE_PED_ID())
								IF IS_ENTITY_OK(m_psWalkwayFIB2[2].id)
									SET_ENTITY_HEALTH(m_psWalkwayFIB2[2].id, 0)
								ENDIF
								
								IF GET_NUM_PEDS_ALIVE(m_psMichaelSecondWave) = 0 
									bSeriouslyDontDoSwitchUIShitAnymore = TRUE
								ENDIF
								
								CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - STAGE_DAVE_BY_FOUNTAIN - Michael Tasked to MICHAEL_AI_SECOND_ZONE ")
								eMichaelAIState = MICHAEL_AI_SECOND_ZONE
							ELIF IS_ENTITY_IN_TRIGGER_BOX(m_tbMichaelTaskZone3, MIKE_PED_ID())
								CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - STAGE_DAVE_BY_FOUNTAIN - Michael Tasked to MICHAEL_AI_THIRD_ZONE ")
								eMichaelAIState = MICHAEL_AI_THIRD_ZONE
							ELIF m_bMichaelAIGoesStraightToDave
								CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - STAGE_DAVE_BY_FOUNTAIN - Michael Tasked to MICHAEL_AI_GET_TO_DAVE ")
								eMichaelAIState = MICHAEL_AI_GET_TO_DAVE
							ENDIF
						BREAK
						
						CASE STAGE_DAVE_ESCAPES
							IF IS_ENTITY_OK(MIKE_PED_ID())
								CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Tasking Michael in STAGE_DAVE_ESCAPES")
								CLEAR_PED_TASKS_IMMEDIATELY(MIKE_PED_ID())
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), FALSE)
								SET_PED_COMBAT_MOVEMENT(MIKE_PED_ID(), CM_DEFENSIVE)
								SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(MIKE_PED_ID(), m_tbDefensiveAreaLargerFountainArea)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(MIKE_PED_ID(), 250)
							ENDIF
						BREAK
						
						CASE STAGE_ESCAPE_MUSEUM
						CASE STAGE_VEHICLE_ESCAPE
							eOtherPlayerFleeState = DAVE_FLEE_TASK_TO_PARKING_LOT
						BREAK
						
					ENDSWITCH
				ENDIF
				
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = GET_PLAYER_PED_MODEL(CHAR_MICHAEL) 
					SAFE_BLIP_PED(blipOtherPlayer, sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					peds[mpf_trev].id = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
					peds[mpf_mike].id = PLAYER_PED_ID()
					m_pedOtherPlayer = TREV_PED_ID()
					ADD_PED_FOR_DIALOGUE(sConvo, 0, PLAYER_PED_ID(), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(sConvo, 2, peds[mpf_trev].id, "TREVOR")
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(TREV_PED_ID(), "MI_3_TREVOR_GROUP")
					START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
					STOP_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
					SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), wtMichaelPreviousWeapon, TRUE)
					
					SWITCH int_to_enum(mission_stage_flag, mission_stage)
						CASE STAGE_DAVE_BY_FOUNTAIN
							IF bSafeToTaskMichaelFIBEnemies
								TASK_REMAINING_MIKE_ONLY_WAVE_2_PEDS_IF_MIKE()
							ENDIF
							IF IS_ENTITY_IN_TRIGGER_BOX(m_tbLargeRoofVolume, TREV_PED_ID())
								REMOVE_PED_DEFENSIVE_AREA(TREV_PED_ID())
								SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(TREV_PED_ID(), m_tbDefensiveAreaTrevorOnRoof)
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 250)
						BREAK
						
						CASE STAGE_DAVE_ESCAPES
							IF IS_ENTITY_OK(TREV_PED_ID())
								IF IS_ENTITY_IN_TRIGGER_BOX(m_tbLargeRoofVolume, TREV_PED_ID())
									CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Tasking Trevor in STAGE_DAVE_ESCAPES to stay on the roof")
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TREV_PED_ID(), FALSE)
									SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(TREV_PED_ID(), m_tbDefensiveAreaTrevorOnRoof)
									TASK_TREVOR_INTO_ROOF_CORNER_COVER()
								ELSE //IF GET_DISTANCE_BETWEEN_ENTITIES(TREV_PED_ID(), m_pedAllyDave) <= 30.0
									CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Tasking Trevor in STAGE_DAVE_ESCAPES to stay near Dave")
									CLEAR_PED_TASKS_IMMEDIATELY(TREV_PED_ID())
									REMOVE_PED_DEFENSIVE_AREA(TREV_PED_ID())
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TREV_PED_ID(), FALSE)
									SET_PED_COMBAT_MOVEMENT(TREV_PED_ID(), CM_DEFENSIVE)
									SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(TREV_PED_ID(), m_tbDefensiveAreaLargerFountainArea)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(TREV_PED_ID(), 250)
								ENDIF
							ENDIF
							
							SET_ENTITY_INVINCIBLE(m_pedAllyDave, TRUE)
							SET_ENTITY_PROOFS(m_pedAllyDave, TRUE, TRUE, TRUE, TRUE, TRUE)
							
						BREAK
						
						CASE STAGE_ESCAPE_MUSEUM
						CASE STAGE_VEHICLE_ESCAPE
							eOtherPlayerFleeState = DAVE_FLEE_TASK_TO_PARKING_LOT
						BREAK
					ENDSWITCH
				ENDIF
				CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_SWITCHING - Going to PLAYER_SWITCH_SETUP ")
				ePlayerSwitchState = PLAYER_SWITCH_SETUP
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC RESET_GAME_CAMERA(FLOAT fAngle = 0.0)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fAngle)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

ENDPROC

//PURPOSE: Starts or stops a cutscene. If TRUE is passed TIMERA() is set to Zero
PROC SET_CUTSCENE_RUNNING(BOOL isRunning, BOOL turnOffGadgets = TRUE)

	IF NOT isRunning
		RESET_GAME_CAMERA()
	ENDIF

	SET_WIDESCREEN_BORDERS(isRunning,0)
	IF turnOffGadgets = TRUE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning),  SPC_DEACTIVATE_GADGETS)
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
	ENDIF
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(isRunning)
	DISABLE_CELLPHONE(isRunning)
	DISPLAY_RADAR(NOT isRunning)
	DISPLAY_HUD(NOT isRunning)
ENDPROC


PROC CREATE_CRASHED_HELI()
	objs[mof_crashed_heli].id = CREATE_OBJECT(mod_heli_crashed,(<<-2197.09375, 219.52654, 183.60187>>))
	IF IS_ENTITY_OK(objs[mof_crashed_heli].id)
		SET_ENTITY_HEADING(objs[mof_crashed_heli].id, 203)
	ENDIF
	Unload_Asset_Model(sAssetData, mod_heli_crashed)
ENDPROC


PROC UPDATE_TREVOR_ONLY_ENEMY_SPAWNS()
	IF NOT m_bSpawnTrevorOnlyMW1
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbSpawnTrevorOnlyMW1)
			Create_Mission_Ped(m_psTrevorOnlyMW1[0], mod_ped_mw, (<<-2264.40820, 222.08583, 188.61163>>), 90.0, "TREV_MW1_1", REL_MW, weap_cia_handgun, 2)
			Create_Cover_Point(covTrevorOnlyMW1, (<<-2264.40820, 222.08583, 188.61163>>),  90.0, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, COVARC_315TO0, FALSE)
			Set_Ped_Combat_Params(m_psTrevorOnlyMW1[0], GET_ENTITY_COORDS(m_psTrevorOnlyMW1[0].id), 1.0)
			
			TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psTrevorOnlyMW1[0].id, covTrevorOnlyMW1.pos, -1, TRUE, 0, FALSE, FALSE, covTrevorOnlyMW1.id)
			m_bSpawnTrevorOnlyMW1 = TRUE
		ENDIF
	ENDIF
	
	IF NOT m_bSpawnTrevorOnlyMW2
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbSpawnTrevorOnlyMW2)
			Create_Mission_Ped(m_psTrevorOnlyMW2[0], mod_ped_mw, (<<-2274.91943, 222.98207, 168.60199>>), 180, "TREV_MW2_1", REL_MW, weap_cia_handgun, 2)
			Set_Ped_Combat_Params(m_psTrevorOnlyMW2[0], (<<-2275.33887, 213.20009, 168.61174>>), 5.0)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psTrevorOnlyMW2[0].id, 100.0)
			m_bSpawnTrevorOnlyMW2 = TRUE
		ENDIF
	ENDIF
	
	IF NOT m_bSpawnTrevorOnlyMW3
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbSpawnTrevorOnlyMW3)
			Create_Mission_Ped(m_psTrevorOnlyMW3[0], mod_ped_mw, (<<-2270.01465, 237.28992, 168.60199>>), 80, "TREV_MW3_1", REL_MW, weap_cia_handgun, 2)
			Set_Ped_Combat_Params(m_psTrevorOnlyMW3[0], (<<-2276.76099, 238.55177, 168.60199>>), 5.0)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psTrevorOnlyMW3[0].id, 100.0)
			m_bSpawnTrevorOnlyMW3 = TRUE
		ENDIF
	ENDIF
	
	IF NOT m_bSpawnTrevorOnlyMW4
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbSpawnTrevorOnlyMW4)
//			SETUP_ENEMY(m_psTrevorOnlyMW4[0], mod_ped_mw, (<<-2268.75073, 211.04308, 180.40208>>), 299.1509, (<<-2264.39746, 213.80823, 183.61140>>), "TREV_MW4_1")
//			SETUP_ENEMY(m_psTrevorOnlyMW4[1], mod_ped_mw, (<<-2264.03467, 210.92635, 178.61218>>), 299.1509, (<<-2271.68481, 211.65504, 181.10181>>), "TREV_MW4_2")
//			SETUP_ENEMY(m_psTrevorOnlyMW4[2], mod_ped_mw, (<<-2254.77563, 204.97714, 173.60191>>), 109.1509, (<<-2262.41333, 202.24286, 173.60172>>), "TREV_MW4_3")
//			SETUP_ENEMY(m_psTrevorOnlyMW4[3], mod_ped_mw, (<<-2253.63501, 205.18712, 173.60197>>), 109.1509, (<<-2253.64575, 197.42163, 173.60182>>), "TREV_MW4_4")
			
			Create_Mission_Ped(m_psTrevorOnlyMW4[2], mod_ped_mw, (<<-2254.77563, 204.97714, 173.60191>>), 109.1509, "TREV_MW4_3", REL_MW, weap_mw_rifle, 2)
			Set_Ped_Combat_Params(m_psTrevorOnlyMW4[2], (<<-2262.41333, 202.24286, 173.60172>>), 5.0)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psTrevorOnlyMW4[2].id, 100.0)
			
			Create_Mission_Ped(m_psTrevorOnlyMW4[3], mod_ped_mw, (<<-2253.63501, 205.18712, 173.60197>>), 109.1509, "TREV_MW4_4", REL_MW, weap_mw_rifle, 2)
			Set_Ped_Combat_Params(m_psTrevorOnlyMW4[3], (<<-2253.64575, 197.42163, 173.60182>>), 5.0)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psTrevorOnlyMW4[3].id, 100.0)
			
			SET_ACCURACY_FOR_PED_STRUCT(m_psTrevorOnlyMW4, 3)
			m_bSpawnTrevorOnlyMW4 = TRUE
		ENDIF
	ENDIF
	
	IF NOT m_bSpawnTrevorOnlyMW5
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbSpawnTrevorOnlyMW5)
//			SETUP_ENEMY(m_psTrevorOnlyMW5[0], mod_ped_mw, (<<-2234.89258, 179.25562, 173.60182>>), 109.1509, (<<-2241.38574, 175.23581, 173.60182>>), "TREV_MW5_1")
//			SETUP_ENEMY(m_psTrevorOnlyMW5[1], mod_ped_mw, (<<-2233.59717, 179.87456, 173.60182>>), 109.1509, (<<-2238.19507, 176.98560, 173.60182>>), "TREV_MW5_2")
//			SETUP_ENEMY(m_psTrevorOnlyMW5[2], mod_ped_mw, (<<-2213.21729, 190.60484, 173.60182>>), 109.1509, (<<-2212.80933, 181.47726, 173.60182>>), "TREV_MW5_3")
//			SETUP_ENEMY(m_psTrevorOnlyMW5[3], mod_ped_mw, (<<-2213.57153, 191.69353, 173.60182>>), 109.1509, (<<-2212.09790, 187.43587, 173.60182>>), "TREV_MW5_4")
//			SET_ACCURACY_FOR_PED_STRUCT(m_psTrevorOnlyMW5, 3)
			m_bSpawnTrevorOnlyMW5 = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_TREVOR_SAVES_DAVE_FIB()
	//FIRING_TYPE fType = FIRING_TYPE_RANDOM_BURSTS
	INT i 
	
	Create_Cover_Point(m_csFIBCover[0], (<<-2247.1006, 273.1696, 173.6020>>), 210, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, FALSE)
	Create_Cover_Point(m_csFIBCover[1], (<<-2251.3889, 271.2775, 173.6020>>), 210, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
	Create_Cover_Point(m_csFIBCover[2], (<<-2231.96191, 275.00153, 173.60196>>), 210, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60)
	Create_Cover_Point(m_csFIBCover[4], (<<-2227.24146, 264.47415, 173.60196>>), 210, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60)
	Create_Cover_Point(m_csFIBCover[3], (<<-2229.51709, 269.74698, 173.60196>>), 210, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60)
//	Create_Cover_Point(m_csFIBCover[5], (<<-2247.47095, 266.94583, 173.60196>>), 210, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60)

	
	Create_Mission_Ped(m_psTrevorSaveDaveFIB[0], mod_ped_fib, m_csFIBCover[0].pos, m_csFIBCover[0].dir, "MT_FIB1", REL_FIB, weap_fib_rifle, 1)
	Create_Mission_Ped(m_psTrevorSaveDaveFIB[1], mod_ped_fib, m_csFIBCover[1].pos, m_csFIBCover[1].dir, "MT_FIB2", REL_FIB, weap_fib_rifle, 1)
	Create_Mission_Ped(m_psTrevorSaveDaveFIB[2], mod_ped_fib, <<-2223.5210, 287.8860, 173.6020>>, 125.0, "MT_FIB3", REL_FIB, weap_fib_rifle, 1)
	Create_Mission_Ped(m_psTrevorSaveDaveFIB[3], mod_ped_fib, <<-2223.4180, 279.7906, 173.6020>>, 125.0, "MT_FIB4", REL_FIB, weap_fib_rifle, 1)
	Create_Mission_Ped(m_psTrevorSaveDaveFIB[4], mod_ped_fib, <<-2223.7397, 282.7443, 173.6020>>, 125.0, "MT_FIB5", REL_FIB, weap_fib_rifle, 1)
//	Create_Mission_Ped(m_psTrevorSaveDaveFIB[5], mod_ped_fib, m_csFIBCover[5].pos, m_csFIBCover[5].dir, "MT_FIB6", REL_FIB, weap_fib_rifle, 1)
	
	SET_PEDS_AS_ONE_HIT_KILLS(m_psTrevorSaveDaveFIB)
	FOR i = 0 TO COUNT_OF(m_psTrevorSaveDaveFIB) - 1
		IF IS_ENTITY_OK(m_psTrevorSaveDaveFIB[i].id)
			REGISTER_HATED_TARGETS_AROUND_PED(m_psTrevorSaveDaveFIB[i].id, 250)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(m_psTrevorSaveDaveFIB[i].id, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psTrevorSaveDaveFIB[i].id, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(m_psTrevorSaveDaveFIB[i].id, CA_USE_COVER, TRUE)
			REMOVE_PED_DEFENSIVE_AREA(m_psTrevorSaveDaveFIB[i].id, TRUE)
			SET_PED_SPHERE_DEFENSIVE_AREA(m_psTrevorSaveDaveFIB[i].id, m_csFIBCover[i].pos, 1.0)// GET_ENTITY_COORDS(m_psTrevorSaveDaveFIB[i].id), 1.0)
			TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psTrevorSaveDaveFIB[i].id, m_csFIBCover[i].pos, -1, TRUE)
			SET_PED_TO_FIRE_MANY_BLANKS(m_psTrevorSaveDaveFIB[i].id)
		ENDIF
	ENDFOR
ENDPROC


PROC CREATE_LAST_STRAGGLER_FOR_DAVE_TO_SHOOT()
	Create_Mission_Ped(m_psTrevorSaveDaveFIB[5], mod_ped_fib, (<<-2228.75293, 295.34073, 173.60196>>), 157.70, "MT_FIB6", REL_FIB_FINAL, weap_fib_rifle, 5)
	SET_PED_SPHERE_DEFENSIVE_AREA(m_psTrevorSaveDaveFIB[5].id, (<<-2238.60229, 271.32703, 173.60196>>), 2.0)
	SET_PED_COMBAT_ATTRIBUTES(m_psTrevorSaveDaveFIB[5].id, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(m_psTrevorSaveDaveFIB[5].id, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(m_psTrevorSaveDaveFIB[5].id, CA_CAN_CHARGE, FALSE)
	SET_PED_COMBAT_MOVEMENT(m_psTrevorSaveDaveFIB[5].id, CM_DEFENSIVE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(m_psTrevorSaveDaveFIB[5].id, FALSE)
	SET_ENTITY_HEALTH(m_psTrevorSaveDaveFIB[5].id, 150)
	TASK_COMBAT_PED(m_psTrevorSaveDaveFIB[5].id, m_pedAllyDave)
ENDPROC

PROC CREATE_IG_8_ENEMIES_FOR_TREVOR_DURING_COURTYARD_FIGHT()
	INT i 
	Create_Mission_Ped(m_psMWIG_8[0], mod_ped_mw, (<<-2276.92749, 262.90067, 183.60114>>), 203, "MW_IG8_1", REL_MW_FINAL, weap_mw_rifle)
	Create_Mission_Ped(m_psMWIG_8[1], mod_ped_mw, (<<-2279.00513, 261.18652, 183.60120>>), 203, "MW_IG8_2", REL_MW_FINAL, weap_mw_rifle)
	Create_Mission_Ped(m_psMWIG_8[2], mod_ped_mw, (<<-2282.02075, 259.25253, 183.60132>>), 203, "MW_IG8_3", REL_MW_FINAL, weap_mw_rifle)
	m_tbDefensiveAreaTrevorDangerPeds = m_tbDefensiveAreaTrevorDangerPeds
	FOR i = 0 TO COUNT_OF(m_psMWIG_8) - 1
		IF IS_ENTITY_OK(m_psMWIG_8[i].id)
			SET_PED_ACCURACY(m_psMWIG_8[i].id, 1)
			SET_PED_COMBAT_RANGE(m_psMWIG_8[i].id, CR_FAR)
			SET_PED_COMBAT_MOVEMENT(m_psMWIG_8[i].id, CM_DEFENSIVE)
			TASK_COMBAT_PED(m_psMWIG_8[i].id, TREV_PED_ID())
		ENDIF
	ENDFOR
ENDPROC


FUNC OBJECT_INDEX CREATE_OBJECT_WITH_HEADING(MODEL_NAMES model, VECTOR vPos, FLOAT flHeading)
	OBJECT_INDEX objRetVal
	
	objRetVal = CREATE_OBJECT(model, vPos)
	IF IS_ENTITY_OK(objRetVal)
		SET_ENTITY_HEADING(objRetVal, flHeading)
	ENDIF
	RETURN objRetVal
ENDFUNC

PROC FINAL_ENEMY_WAVE_TASKING(PED_STRUCT &ps, TRIGGER_BOX tbDefensiveArea)
	CPRINTLN(DEBUG_MISSION, "FINAL_ENEMY_WAVE_TASKING")
	IF IS_ENTITY_OK(ps.id)
		IF GET_PED_RELATIONSHIP_GROUP_HASH(ps.id ) = REL_MW
			CPRINTLN(DEBUG_MISSION, "FINAL_ENEMY_WAVE_TASKING - MW ped, set to REL_MW_FINAL")
			SET_PED_RELATIONSHIP_GROUP_HASH(ps.id, REL_MW_FINAL)
		ELIF GET_PED_RELATIONSHIP_GROUP_HASH(ps.id ) = REL_FIB
			CPRINTLN(DEBUG_MISSION, "FINAL_ENEMY_WAVE_TASKING - FIB ped, set to REL_FIB_FINAL")
			SET_PED_RELATIONSHIP_GROUP_HASH(ps.id, REL_FIB_FINAL)
		ENDIF
		REMOVE_PED_DEFENSIVE_AREA(ps.id)
		SET_PED_ACCURACY(ps.id, 1)
		SET_PED_ANGLED_DEFENSIVE_AREA(ps.id, tbDefensiveArea.vMin, tbDefensiveArea.vMax, tbDefensiveArea.flWidth)
		SET_PED_COMBAT_ATTRIBUTES(ps.id, CA_CAN_CHARGE, TRUE)
		SET_PED_COMBAT_MOVEMENT(ps.id, CM_WILLADVANCE)
		SET_PED_COMBAT_RANGE(ps.id, CR_NEAR)
		SET_PED_TO_FIRE_MANY_BLANKS(ps.id)
		TASK_COMBAT_HATED_TARGETS_IN_AREA(ps.id, m_vHatedTargetPositionForAI, 5.0)
	ENDIF
ENDPROC

PROC SETUP_WAR_PEDS_FOR_FINAL_SHOWDOWN_FROM_RESTART()
	INT i 
	FOR i = 0 TO COUNT_OF(m_psWarFIB) - 1
		FINAL_ENEMY_WAVE_TASKING(m_psWarFIB[i], m_tbDefensiveAreaFinalFightFIB)
	ENDFOR
					
	FOR i = 0 TO COUNT_OF(m_psWarMWCourtyard) - 1
		FINAL_ENEMY_WAVE_TASKING(m_psWarMWCourtyard[i], m_tbDefensiveAreaFinalFightMW)
	ENDFOR
	
	IF IS_ENTITY_OK(m_psWarFIB[1].id)
		SET_ENTITY_COORDS(m_psWarFIB[1].id, (<<-2228.47217, 294.59753, 173.60196>>))
		SET_ENTITY_HEADING(m_psWarFIB[1].id, 113)
	ENDIF
	
	IF IS_ENTITY_OK(m_psWarFIB[2].id)
		SET_ENTITY_COORDS(m_psWarFIB[2].id, (<<-2224.64819, 285.82687, 173.60196>>))
		SET_ENTITY_HEADING(m_psWarFIB[2].id, 113)
	ENDIF
					
	IF IS_ENTITY_OK(m_psWarMWCourtyard[1].id)
		SET_ENTITY_COORDS(m_psWarMWCourtyard[1].id, (<<-2271.63745, 274.98660, 173.60211>>))
		SET_ENTITY_HEADING(m_psWarMWCourtyard[1].id, 203)
	ENDIF
	
	IF IS_ENTITY_OK(m_psWarMWCourtyard[2].id)
		SET_ENTITY_COORDS(m_psWarMWCourtyard[2].id, (<<-2268.77563, 268.89124, 173.60211>>))
		SET_ENTITY_HEADING(m_psWarMWCourtyard[2].id, 293)
	ENDIF
ENDPROC



FUNC BOOL HAS_PED_JUST_KILLED_PED(PED_INDEX killerPed, PED_INDEX &targetPed)
	IF NOT IS_ENTITY_OK(killerPed)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(targetPed)
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(targetPed)
		ENTITY_INDEX killerEntity = GET_PED_SOURCE_OF_DEATH(targetPed)
		IF DOES_ENTITY_EXIST(killerEntity)
			IF IS_ENTITY_A_PED(killerEntity)
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(killerEntity) = killerPed
					IF GET_PED_TIME_OF_DEATH(targetPed) = GET_GAME_TIMER()
						CPRINTLN(DEBUG_MISSION, "HAS_PED_JUST_KILLED_ENEMY - dead enemy was killed at: ", GET_PED_TIME_OF_DEATH(targetPed), " , game time is: ", GET_GAME_TIMER())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_JUST_KILLED_ANYONE_FROM_PEDS(PED_STRUCT &ps[])
	INT i
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF HAS_PED_JUST_KILLED_PED(PLAYER_PED_ID(), ps[i].id)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FLOAT m_flDoorDelta = 0.05

FLOAT m_flMikeOnly_DoorLRatio = 0.0
FLOAT m_flMikeOnly_DoorRRatio = 0.0
PROC OPEN_MIKE_ONLY_FIB_DOORS()
	IF m_flMikeOnly_DoorLRatio <= -1.0
	AND m_flMikeOnly_DoorRRatio >= 1.0
		EXIT
	ENDIF
	
	IF m_flMikeOnly_DoorLRatio >= -1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_TALES_L), m_flMikeOnly_DoorLRatio, TRUE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_TALES_L), DOORSTATE_LOCKED, TRUE, TRUE)
		m_flMikeOnly_DoorLRatio -= m_flDoorDelta
	ENDIF
	
	IF m_flMikeOnly_DoorRRatio <= 1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_TALES_R), m_flMikeOnly_DoorRRatio, TRUE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_TALES_R), DOORSTATE_LOCKED, TRUE, TRUE)
		m_flMikeOnly_DoorRRatio += m_flDoorDelta
	ENDIF
ENDPROC

PROC CLOSE_MIKE_ONLY_FIB_DOORS(BOOL bSnapClose = FALSE)
	DOOR_STATE_ENUM doorState
	IF bSnapClose
		doorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
	ELSE
		doorState = DOORSTATE_LOCKED
	ENDIF
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_TALES_L), 0.0, TRUE, FALSE)
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_TALES_R), 0.0, TRUE, FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_TALES_L), doorState, TRUE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_TALES_R), doorState, TRUE, TRUE)
ENDPROC

FLOAT m_flIG7_DoorLRatio = 0.0
FLOAT m_flIG7_DoorRRatio = 0.0

PROC OPEN_IG_7_DOORS()
	IF m_flIG7_DoorLRatio <= -1.0
	AND m_flIG7_DoorRRatio >= 1.0
		EXIT
	ENDIF
	
	IF m_flIG7_DoorLRatio >= -1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_QING_L), m_flIG7_DoorLRatio, TRUE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_QING_L), DOORSTATE_LOCKED, TRUE, TRUE)
		m_flIG7_DoorLRatio -= m_flDoorDelta
	ENDIF
	
	IF m_flIG7_DoorRRatio <= 1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_QING_R), m_flIG7_DoorRRatio, TRUE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_QING_R), DOORSTATE_LOCKED, TRUE, TRUE)
		m_flIG7_DoorRRatio += m_flDoorDelta
	ENDIF
ENDPROC

PROC CLOSE_IG_7_DOORS(BOOL bSnapClose = FALSE)
	DOOR_STATE_ENUM doorState
	IF bSnapClose
		doorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
	ELSE
		doorState = DOORSTATE_LOCKED
	ENDIF
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_QING_L), 0.0, TRUE, FALSE)
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_QING_R), 0.0, TRUE, FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_QING_L), doorState, TRUE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_QING_R), doorState, TRUE, TRUE)
ENDPROC

FLOAT m_flIG8_DoorLRatio = 0.0
FLOAT m_flIG8_DoorRRatio = 0.0
PROC OPEN_IG_8_DOORS()
	
	IF m_flIG8_DoorLRatio <= -1.0
	AND m_flIG8_DoorRRatio >= 1.0
		EXIT
	ENDIF
	
	IF m_flIG8_DoorLRatio >= -1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L), m_flIG8_DoorLRatio, TRUE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L), DOORSTATE_LOCKED, TRUE, TRUE)
		m_flIG8_DoorLRatio -= m_flDoorDelta
	ENDIF
	
	IF m_flIG8_DoorRRatio <= 1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_R), m_flIG8_DoorRRatio, TRUE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_R), DOORSTATE_LOCKED, TRUE, TRUE)
		m_flIG8_DoorRRatio += m_flDoorDelta
	ENDIF
ENDPROC

PROC CLOSE_IG_8_DOORS(BOOL bSnapClose = FALSE)
	DOOR_STATE_ENUM doorState
	IF bSnapClose
		doorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
	ELSE
		doorState = DOORSTATE_LOCKED
	ENDIF
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L), 0.0, TRUE, FALSE)
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_R), 0.0, TRUE, FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L), doorState, TRUE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_R), doorState, TRUE, TRUE)
ENDPROC

FUNC INT GET_NUM_PEDS_IN_TRIGGER_BOX(TRIGGER_BOX tb, PED_STRUCT &ps[])
	INT i
	INT iRetVal = 0
	
	FOR i = 0 TO COUNT_OF(ps) - 1
		IF IS_ENTITY_OK(ps[i].id)
			IF IS_ENTITY_IN_TRIGGER_BOX(tb, ps[i].id)
				iRetVal++
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iRetVal
ENDFUNC

PROC SETUP_MICHAEL_FOR_HELICOPTER_SCENE()
	IF IS_ENTITY_OK(MIKE_PED_ID())
		CPRINTLN(DEBUG_MISSION, "Setting up Michael for the heli start - 5")
		SET_ENTITY_PROOFS(MIKE_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
		CLEAR_PED_TASKS_IMMEDIATELY(MIKE_PED_ID())
		SET_ENTITY_COORDS(MIKE_PED_ID(), csMichaelStartingCover.pos)
		SET_ENTITY_HEADING(MIKE_PED_ID(), csMichaelStartingCover.dir)
		REMOVE_PED_DEFENSIVE_AREA(MIKE_PED_ID())
		SET_PED_SPHERE_DEFENSIVE_AREA(MIKE_PED_ID(), GET_ENTITY_COORDS(MIKE_PED_ID()), 5.0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), TRUE)
		//TASK_COMBAT_HATED_TARGETS_AROUND_PED(MIKE_PED_ID(), 250.0)
		TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csMichaelStartingCover.pos, -1, FALSE, 0, FALSE, FALSE, csMichaelStartingCover.id)
		SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_ForcedToStayInCover, TRUE)
		FREEZE_ENTITY_POSITION(MIKE_PED_ID(), FALSE)
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(MIKE_PED_ID(), "MI_3_MICHAEL_GROUP")
	ENDIF
ENDPROC

PROC SETUP_PED_VARIATIONS_FOR_CIA_GOON1()
	//CIA GOON 1
	CPRINTLN(DEBUG_MISSION, "SETUP_PED_VARIATIONS_FOR_CIA_GOON1")
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[1].id, PED_COMP_HEAD, 		1, 1, 0) //(head)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[1].id, PED_COMP_HAIR, 		1, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[1].id, PED_COMP_TORSO,		0, 2, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[1].id, PED_COMP_LEG, 		0, 2, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[1].id, PED_COMP_SPECIAL, 	0, 0, 0) //(accs)
ENDPROC

PROC SETUP_PED_VARIATIONS_FOR_CIA_GOON2()
	//CIA GOON 2
	CPRINTLN(DEBUG_MISSION, "SETUP_PED_VARIATIONS_FOR_CIA_GOON2")
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[2].id, PED_COMP_HEAD, 		2, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[2].id, PED_COMP_HAIR, 		3, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[2].id, PED_COMP_TORSO,		1, 1, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[2].id, PED_COMP_LEG, 		0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[2].id, PED_COMP_SPECIAL, 	0, 0, 0) //(accs)
ENDPROC

PROC SETUP_PED_VARIATIONS_FOR_CIA_GOON3()
	//CIA GOON 3
	CPRINTLN(DEBUG_MISSION, "SETUP_PED_VARIATIONS_FOR_CIA_GOON3")
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[3].id, PED_COMP_HEAD, 		0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[3].id, PED_COMP_HAIR, 		0, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[3].id, PED_COMP_TORSO,		2, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[3].id, PED_COMP_LEG, 		0, 1, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(m_psFirstAreaCIA[3].id, PED_COMP_SPECIAL, 	0, 0, 0) //(accs)
ENDPROC

FUNC BOOL IS_ONE_PARKING_LOT_ENEMY_LEFT(PED_INDEX &outLastAlivePed)
	INT iNumAlive = 0
	
	IF IS_ENTITY_OK(peds[mpf_pl_1_mw_1].id)
		outLastAlivePed = peds[mpf_pl_1_mw_1].id
		iNumAlive++
	ENDIF
	
	IF IS_ENTITY_OK(peds[mpf_pl_1_mw_2].id)
		outLastAlivePed = peds[mpf_pl_1_mw_2].id
		iNumAlive++
	ENDIF
	
	IF IS_ENTITY_OK(peds[mpf_pl_1_mw_3].id)
		outLastAlivePed = peds[mpf_pl_1_mw_3].id
		iNumAlive++
	ENDIF
	
	IF IS_ENTITY_OK(peds[mpf_pl_1_mw_4].id)
		outLastAlivePed = peds[mpf_pl_1_mw_4].id
		iNumAlive++
	ENDIF
	
	IF IS_ENTITY_OK(peds[mpf_pl_1_mw_5].id)
		outLastAlivePed = peds[mpf_pl_1_mw_5].id
		iNumAlive++
	ENDIF
	
	IF IS_ENTITY_OK(peds[mpf_mw_jeep_2_1].id)
		outLastAlivePed = peds[mpf_mw_jeep_2_1].id
		iNumAlive++
	ENDIF
	
	IF IS_ENTITY_OK(peds[mpf_mw_jeep_2_2].id)
		outLastAlivePed = peds[mpf_mw_jeep_2_2].id
		iNumAlive++
	ENDIF
	
	IF iNumAlive = 1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RETASK_LAST_PARKING_LOT_PED()
	PED_INDEX pedLastGuy
	IF NOT m_bLastParkingLotGuyTasked
		IF IS_ONE_PARKING_LOT_ENEMY_LEFT(pedLastGuy)
			IF IS_ENTITY_OK(pedLastGuy)
				CLEAR_PED_TASKS_IMMEDIATELY(pedLastGuy)
				REMOVE_PED_DEFENSIVE_AREA(pedLastGuy)
				SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(pedLastGuy, PLAYER_PED_ID(), <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 10.0,FALSE)
				SET_PED_COMBAT_ATTRIBUTES(pedLastGuy, CA_CAN_CHARGE, TRUE)
				TASK_COMBAT_PED(pedLastGuy, PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "RETASK_LAST_PARKING_LOT_PED - setting this up")
				m_bLastParkingLotGuyTasked = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

