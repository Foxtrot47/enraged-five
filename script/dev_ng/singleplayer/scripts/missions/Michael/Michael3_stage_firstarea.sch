USING "Michael3_support.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_itemsets.sch"

ENUM	FIRST_AREA_STATE
	FIRST_AREA_INIT,
	FIRST_AREA_STREAMING,
	FIRST_AREA_COMBAT,
	FIRST_AREA_WAIT_TO_START_STAIR_FIGHT,
	DAVE_ON_STAIRS_COMBAT,
	DAVE_ON_STAIRS_WAIT_TO_REACH_DESTINATION,
	DAVE_ON_STAIRS_MICHAEL_SWAP_WEAPON,
	FIRST_AREA_COMPLETE,
	FIRST_AREA_IDLE
ENDENUM
FIRST_AREA_STATE	eFirstAreaState = FIRST_AREA_INIT
MISSION_MESSAGE		eMessagesFirstArea = MISSION_MESSAGE_IDLE
MISSION_MESSAGE		eMessagesMichaelDave = MISSION_MESSAGE_01

structTimer			tmrControlAmbientDialogue
structTimer			tmrControlMichaelDaveDialogue

TRIGGER_BOX			tb_HelicopterFires
TRIGGER_BOX			tb_FirstAreaEndTrigger
TRIGGER_BOX			tb_DefensiveAreaSmaller

INT 				iCIAGuyState = 0
BOOL				bFirstAreaGoonsRetasked = FALSE
BOOL				bDaveTaskedDownstairs = FALSE
BOOL				bTriggeredFIB

// On Stairs Stuff
MISSION_MESSAGE	eDaveStairsMessages = MISSION_MESSAGE_01

TRIGGER_BOX			tb_MichaelStairTrigger
TRIGGER_BOX			tb_CutsceneTrigger
TRIGGER_BOX			tb_FIBTrigger
//TRIGGER_BOX			tb_AbandonDaveAtStairs

structTimer			tmr_StartStairFight
structTimer			tmr_TriggerFIBBackup
structTimer			tmr_BringUpStraggler

COVER_STRUCT		csFIBStairCoverPoints[2]

INT					iStreamPlayingState
INT					iCutsceneDelayTime


PROC SETUP_STAIR_ENEMY(PED_STRUCT &ped, VECTOR vStartPos, FLOAT flHeading, VECTOR vDestination, STRING strDebugName, INT iPauseTime)
	Create_Mission_Ped(ped, mod_ped_mw, vStartPos, flHeading, strDebugName, REL_MW_FINAL, weap_mw_rifle, 5, 0, FALSE)
	IF IS_ENTITY_OK(ped.id)
		Set_Ped_Combat_Params(ped, vDestination, 0.75, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(ped.id, FALSE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(ped.id, FALSE)
		SET_PED_DIES_WHEN_INJURED(ped.id, TRUE)
		
		SAFE_OPEN_SEQUENCE()	
			TASK_PAUSE(NULL, iPauseTime)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
		SAFE_PERFORM_SEQUENCE(ped.id)
	ENDIF
ENDPROC


PROC CREATE_STAIR_ENEMIES()
	SETUP_STAIR_ENEMY(m_psStairsMW[0], <<-2206.07690, 221.78526, 178.30203>>, 117.8388, <<-2201.55322, 228.03366, 181.11182>>, "Stair_MW1", 0)
//	SETUP_STAIR_ENEMY(m_psStairsMW[1], <<-2205.46924, 220.42017, 178.30203>>, 117.8388, <<-2203.65967, 225.94734, 181.00186>>, "Stair_MW2", 2000)
	
//	Create_Mission_Ped(m_psStairsMW[2], mod_ped_mw, <<-2205.20264, 222.02794, 177.90204>>, 117.8388, "Stair_MW3", REL_MW_FINAL, weap_mw_rifle, 5, 0, FALSE)
//	SET_PED_SPHERE_DEFENSIVE_AREA(m_psStairsMW[2].id, (<<-2207.72656, 222.42500, 178.61203>>), 2.0, TRUE)
//	TASK_SEEK_COVER_TO_COORDS(m_psStairsMW[2].id, (<<-2207.72656, 222.42500, 178.61203>>), (<<-2199.89380, 226.76622, 181.99519>>), -1, TRUE)

	INT i 
	FOR i = 0 TO COUNT_OF(m_psStairsMW) - 1
		IF IS_ENTITY_OK(m_pedAllyDave) AND IS_ENTITY_OK(m_psStairsMW[i].id)
			REGISTER_TARGET(m_pedAllyDave, m_psStairsMW[i].id)
			SET_ENTITY_HEALTH(m_psStairsMW[i].id, iCONST_ONE_SHOT_KILL_HEALTH)
		ENDIF
	ENDFOR
	RESTART_TIMER_NOW(tmr_BringUpStraggler)
ENDPROC

PROC UPDATE_CREATE_FIB()
	IF NOT bTriggeredFIB
		IF IS_PLAYER_IN_TRIGGER_BOX(tb_FIBTrigger)
//		OR GET_TIMER_IN_SECONDS(tmr_TriggerFIBBackup) >= 4.0
//			CPRINTLN(DEBUG_MISSION, "UPDATE_CREATE_FIB - Creating guys")
//			// mpf_heli_1_fib_1
//			Create_Cover_Point(csFIBStairCoverPoints[0], (<<-2194.28247, 247.46591, 183.61197>>), 202, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60)
//			Create_Mission_Ped(m_psStairsFIB[0], mod_ped_fib,<< -2213.86230, 246.99609, 183.60408 >>, 203.1862, "HELI_STRAFE_FIB_1", REL_FIB_FINAL, weap_fib_rifle, 1, 0, FALSE)
//			REMOVE_PED_DEFENSIVE_AREA(m_psStairsFIB[0].id)
//			SET_PED_ANGLED_DEFENSIVE_AREA(m_psStairsFIB[0].id, <<-2196.051514,244.924591,183.601868>>, <<-2198.373047,251.017700,186.891953>>, 7.000000)
//			SAFE_OPEN_SEQUENCE()
//				TASK_SEEK_COVER_TO_COVER_POINT(NULL, csFIBStairCoverPoints[0].id, (<<-2188.13086, 233.40884, 183.74260>>), -1, TRUE)
//			SAFE_PERFORM_SEQUENCE(m_psStairsFIB[0].id)
			
			// mpf_heli_1_fib_2
			Create_Mission_Ped(m_psStairsFIB[1], mod_ped_fib, << -2214.50000, 248.44427, 183.60408 >>, 203.2991, "HELI_STRAFE_FIB_2", REL_FIB_FINAL, weap_fib_rifle, 1, 0, FALSE)
			Create_Cover_Point(csFIBStairCoverPoints[1], (<<-2199.16333, 245.10863, 183.60487>>), 203.2991, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, COVARC_300TO0)
			REMOVE_PED_DEFENSIVE_AREA(m_psStairsFIB[1].id)
			SET_PED_ANGLED_DEFENSIVE_AREA(m_psStairsFIB[1].id, <<-2196.051514,244.924591,183.601868>>, <<-2198.373047,251.017700,186.891953>>, 7.000000)
			SAFE_OPEN_SEQUENCE()
				TASK_SEEK_COVER_TO_COVER_POINT(NULL, csFIBStairCoverPoints[1].id, (<<-2190.85547, 228.72301, 183.60187>>), -1, TRUE)
			SAFE_PERFORM_SEQUENCE(m_psStairsFIB[1].id)
			bTriggeredFIB = TRUE
//			eDaveStairsMessages = MISSION_MESSAGE_03 
		ENDIF
	ENDIF
ENDPROC



PROC CREATE_FIRST_AREA_TRIGGERS()
	tb_HelicopterFires = tb_HelicopterFires
	tb_HelicopterFires = CREATE_TRIGGER_BOX(<<-2198.195557,221.261963,183.351700>>, <<-2208.155273,216.706421,187.253143>>, 10.000000)
	tb_FirstAreaEndTrigger = CREATE_TRIGGER_BOX(<<-2186.798584,234.810059,183.501862>>, <<-2194.094971,231.461990,187.418823>>, 45.000000)
	tb_DefensiveAreaSmaller = CREATE_TRIGGER_BOX(<<-2160.648926,233.601608,183.351868>>, <<-2180.935303,224.597900,186.851868>>, 25.250000)
	
	//Stairs Stuff
	tb_MichaelStairTrigger = CREATE_TRIGGER_BOX(<<-2186.798584,234.810059,183.501862>>, <<-2194.094971,231.461990,187.418823>>, 45.000000)
	tb_CutsceneTrigger = CREATE_TRIGGER_BOX(<<-2211.363525,238.609055,183.504868>>, <<-2229.537842,280.064331,186.604080>>, 16.000000)
	tb_FIBTrigger = CREATE_TRIGGER_BOX(<<-2178.276611,237.987442,183.351868>>, <<-2193.523438,231.284363,187.101868>>, 40.000000)
//	tb_AbandonDaveAtStairs = CREATE_TRIGGER_BOX(<<-2208.054932,239.987625,183.604874>>, <<-2210.520508,245.411530,185.354874>>, 12.500000)
ENDPROC


PROC CREATE_FIRST_AREA_ENEMIES()
	INT i 
	// FIB
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[0].id)
		Create_Mission_Ped(m_psFirstAreaFIB[0], mod_ped_fib, << -2161.50024, 216.97882, 183.60187 >>, 19.8485, "START_FIB_LEAD", REL_FIB, weap_fib_rifle, 5, 0, FALSE)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaFIB[0], <<-2162.86499, 220.12059, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaFIB[0].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaFIB[0].id, FALSE)
	SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaFIB[0].id)
	
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[1].id)
		Create_Mission_Ped(m_psFirstAreaFIB[1], mod_ped_fib, << -2167.17993, 216.26184, 183.60187 >>, 3.3767, "START_FIB_1", REL_FIB, weap_fib_rifle, 5, 0, FALSE)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaFIB[1], <<-2167.97681, 218.86346, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaFIB[1].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaFIB[1].id, FALSE)
	SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaFIB[1].id)
	
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[2].id)
		Create_Mission_Ped(m_psFirstAreaFIB[2], mod_ped_fib, << -2168.7646, 216.6734, 183.6020 >>, 56.8330, "START_FIB_2", REL_FIB, weap_fib_rifle, 5, 0, FALSE)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaFIB[2], <<-2169.52002, 219.41731, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaFIB[2].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaFIB[2].id, FALSE)
	SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaFIB[2].id)
	
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[3].id)
		Create_Mission_Ped(m_psFirstAreaFIB[3], mod_ped_fib, << -2158.4556, 220.2280, 183.6016 >>, 24.2844, "START_FIB_3", REL_FIB, weap_fib_rifle, 5, 0, FALSE)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaFIB[3], <<-2161.16113, 221.81531, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaFIB[3].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaFIB[3].id, FALSE)
	SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaFIB[3].id)
	
	

	// CIA
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[0].id)
		Create_Mission_Ped(m_psFirstAreaCIA[0], IG_PAPER, <<-2170.68091, 244.19972, 183.60187>>, 203.5326, "START_CIA_LEAD", REL_CIA, weap_cia_handgun, 5, 0, FALSE)
		Create_Cover_Point_For_Ped(m_psFirstAreaCIA[0], <<-2162.7827, 232.4936, 183.6019>>, 202.8664, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
	ENDIF
	IF IS_ENTITY_OK(m_psFirstAreaCIA[0].id)
		Set_Ped_Combat_Params(m_psFirstAreaCIA[0], <<-2162.8796, 232.5484, 183.6019>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaCIA[0].id, FALSE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaCIA[0].id, FALSE)
		SET_PED_PROP_INDEX(m_psFirstAreaCIA[0].id, ANCHOR_EYES,0)
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[1].id)
		Create_Mission_Ped(m_psFirstAreaCIA[1], mod_ped_cia, <<-2167.22290, 241.20326, 183.60187>>, 294.7903, "START_CIA_2", REL_CIA, weap_cia_rifle, 5, 0, FALSE)
		SETUP_PED_VARIATIONS_FOR_CIA_GOON1()
		FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaCIA[1].id)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaCIA[1], <<-2165.82080, 237.71999, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaCIA[1].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaCIA[1].id, FALSE)
		
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[2].id)
		Create_Mission_Ped(m_psFirstAreaCIA[2], mod_ped_cia, << -2179.18188, 236.76740, 183.60187 >>, 292.3021, "START_CIA_3", REL_CIA, weap_cia_handgun, 5, 0, FALSE)
		SETUP_PED_VARIATIONS_FOR_CIA_GOON2()
//		Create_Cover_Point(m_psFirstAreaCIA[2].cov, <<-2167.15967, 230.07074, 183.60187>>, 108.6263, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_120)
		TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaCIA[2].id, <<-2167.15967, 230.07074, 183.60187>>, -1, FALSE, 1, TRUE, FALSE, m_psFirstAreaCIA[2].cov.id, FALSE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaCIA[2].id)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaCIA[2], <<-2177.97632, 233.94264, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaCIA[2].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaCIA[2].id, FALSE)
	
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[3].id)
		Create_Mission_Ped(m_psFirstAreaCIA[3], mod_ped_cia, <<-2170.8118, 241.1140, 183.6019>>, 204.9214, "START_CIA_4", REL_CIA, weap_cia_handgun, 5, 0, FALSE)
		SETUP_PED_VARIATIONS_FOR_CIA_GOON3()
		Create_Cover_Point(m_psFirstAreaCIA[3].cov, <<-2181.46362, 235.57019, 183.60187>>, 288.6263, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaCIA[3].id, <<-2181.46362, 235.57019, 183.60187>>, -1, FALSE, 1, TRUE, FALSE, m_psFirstAreaCIA[3].cov.id, FALSE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaCIA[3].id)
	ENDIF
	Set_Ped_Combat_Params(m_psFirstAreaCIA[3], <<-2181.46362, 235.57019, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaCIA[3].id, FALSE)
	SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaCIA[3].id, FALSE)
	
	
	// Merryweather
	/*
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaMW[0].id)
		Create_Mission_Ped(m_psFirstAreaMW[0], mod_ped_mw, << -2200.4321, 223.9028, 181.1019 >>, 117.7441, "START_MW_1", REL_MW, weap_mw_rifle, 5, 0, FALSE)
		Set_Ped_Combat_Params(m_psFirstAreaMW[0], << -2185.2820, 218.5643, 183.6019 >>, 2.5, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 0.75, 0.5)
		SET_PED_SPHERE_DEFENSIVE_AREA(m_psFirstAreaMW[0].id, <<-2178.1055, 232.9911, 183.6019>>, 15.0, FALSE, TRUE)
		Create_Cover_Point_For_Ped(m_psFirstAreaMW[0], << -2185.2820, 218.5643, 183.6019 >>, 293.1335, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaMW[0].id, FALSE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaMW[0].id, FALSE)
	ENDIF
	*/
	IF NOT DOES_ENTITY_EXIST(m_psFirstAreaMW[1].id)
		Create_Mission_Ped(m_psFirstAreaMW[1], mod_ped_mw, << -2189.3320, 225.2645, 183.6019 >>, 201.8352, "START_MW_2", REL_MW, weap_mw_rifle, 5, 0, FALSE)
		Set_Ped_Combat_Params(m_psFirstAreaMW[1], <<-2189.3320, 225.2645, 183.6019>>, 5.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.75, 0.5, FALSE)
		Create_Cover_Point_For_Ped(m_psFirstAreaMW[1], <<-2189.3320, 225.2645, 183.6019>>, 293.9212, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
		SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaMW[1].id, FALSE)
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaMW[1].id, FALSE)
	ENDIF
				
	// Combat Tasks - FIB
	FOR i = 0 TO COUNT_OF(m_psFirstAreaFIB) - 1
		IF IS_ENTITY_OK(m_psFirstAreaFIB[i].id)
			SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaFIB[i].id)
			REGISTER_HATED_TARGETS_AROUND_PED(m_psFirstAreaFIB[i].id, 50.0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaFIB[i].id, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psFirstAreaFIB[i].id, 100)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO COUNT_OF(m_psFirstAreaCIA) - 1
		IF IS_ENTITY_OK(m_psFirstAreaCIA[i].id)
			SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaCIA[i].id)
			REGISTER_HATED_TARGETS_AROUND_PED(m_psFirstAreaCIA[i].id, 50.0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaCIA[i].id, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psFirstAreaCIA[i].id, 100)
		ENDIF
	ENDFOR
	
	// Combat Tasks - MW
	REGISTER_HATED_TARGETS_AROUND_PED(m_psFirstAreaMW[1].id, 50.0)
	SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFirstAreaMW[1].id)
	SAFE_OPEN_SEQUENCE()
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null, FALSE)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100)
	SAFE_PERFORM_SEQUENCE(m_psFirstAreaMW[1].id)
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psFirstAreaCIA, 1)
	SET_ACCURACY_FOR_PED_STRUCT(m_psFirstAreaFIB, 1)
	SET_ACCURACY_FOR_PED_STRUCT(m_psFirstAreaMW, 1)
	
	//Create Agent Sanchez on a replay
	
	IF IS_REPLAY_IN_PROGRESS()
	AND NOT DOES_ENTITY_EXIST( peds[mpf_andreas].id )
	
		peds[mpf_andreas].id = CREATE_PED(PEDTYPE_MISSION, IG_ANDREAS, <<-2153.3, 235.50, 184.59>>)
		
		TASK_PLAY_ANIM_ADVANCED(peds[mpf_andreas].id, "Dead", "dead_g", 
							<<-2153.3, 235.581,184.623>>, <<0.000, 0.000, 41.50>>, 
							instant_blend_in, normal_blend_out, -1, 
							AF_ENDS_IN_DEAD_POSE | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE, 0.99)
			
		FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_andreas].id, TRUE)
							
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_HEAD, 1, 0)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_BERD, 0, 0, 0) //(berd)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_HAIR, 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_TORSO, 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_LEG, 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_HAND, 0, 0, 0) //(hand)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_FEET, 0, 0, 0) //(feet)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_TEETH, 0, 0, 0) //(teef)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_SPECIAL2, 0, 0, 0) //(task)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_DECL, 0, 0, 0) //(decl)
		SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_JBIB, 0, 0, 0) //(jbib)
		
		REMOVE_DECAL(decal_andreas)
		decal_andreas = ADD_DECAL(int_to_enum(DECAL_RENDERSETTING_ID, 9001), <<-2153.70, 236.07, 183.60>>, <<0,0,-1.0>>, <<0.0,1.0,0.0>>, 0.5, 0.5, 0.196, 0, 0, 1.0, -1)	
		//decal_andreas = add_decal(DECAL_RSID_BLOOD_DIRECTIONAL, <<-2153.9771, 236.8414, 184.8435>>, <<0.0, 0.0, -1.0>> , normalise_vector(<<0.0, 0.0, 0.0>>), 0.7, 0.3, 0.196, 0, 0, 1.0, -1.0)
		
	ENDIF
	
ENDPROC


PROC RETASK_ALL_FIRST_AREA_PEDS()
	SET_COMBAT_RANGE_FOR_PEDS(m_psFirstAreaCIA, CR_NEAR)
	SET_COMBAT_RANGE_FOR_PEDS(m_psFirstAreaFIB, CR_NEAR)
	SET_COMBAT_RANGE_FOR_PEDS(m_psFirstAreaMW, CR_NEAR)
	tb_DefensiveAreaSmaller = tb_DefensiveAreaSmaller
//	CHANGE_DEFENSIVE_AREA_FOR_PEDS(m_psFirstAreaCIA, tb_DefensiveAreaSmaller)
//	CHANGE_DEFENSIVE_AREA_FOR_PEDS(m_psFirstAreaFIB, tb_DefensiveAreaSmaller)
//	CHANGE_DEFENSIVE_AREA_FOR_PEDS(m_psFirstAreaMW, tb_DefensiveAreaSmaller)
ENDPROC


PROC UPDATE_FIRST_AREA_AMBIENT_DIALOGUE()
	PED_INDEX nearbyPeds[5]
	
	// We might randomly play some ambient dialogue but not always
	IF GET_TIMER_IN_SECONDS(tmrControlAmbientDialogue) >= 5.0
		IF GET_RANDOM_BOOL()
			GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
			INT iRnd
			iRnd = 0 
			
			IF DOES_ENTITY_EXIST(nearbyPeds[iRnd])
			AND NOT IS_PED_INJURED(nearbyPeds[iRnd])
				IF NOT IS_AMBIENT_SPEECH_PLAYING(nearbyPeds[iRnd])
					REL_GROUP_HASH relHash
					relHash = GET_PED_RELATIONSHIP_GROUP_HASH(nearbyPeds[iRnd])
					
					IF relHash = REL_CIA
						SWITCH GET_RANDOM_INT_IN_RANGE(1, 5)
							CASE 1
								CDEBUG3LN(DEBUG_MISSION, "AMBIENT DIALOGUE: IAA 1")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(nearbyPeds[iRnd], "MCH3_ARAA", "MIC3CIA3", SPEECH_PARAMS_FORCE)
							BREAK
							CASE 2
								CDEBUG3LN(DEBUG_MISSION, "AMBIENT DIALOGUE: IAA 2")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(nearbyPeds[iRnd], "MCH3_AQAA", "MIC3AGEN3", SPEECH_PARAMS_FORCE)		
							BREAK
							CASE 3
								CDEBUG3LN(DEBUG_MISSION, "AMBIENT DIALOGUE: IAA 3")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(nearbyPeds[iRnd], "MCH3_APAA", "MIC3CIA2", SPEECH_PARAMS_FORCE)		
							BREAK
							CASE 4
								CDEBUG3LN(DEBUG_MISSION, "AMBIENT DIALOGUE: IAA 4")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(nearbyPeds[iRnd], "MCH3_ANAA", "MIC3CIA4", SPEECH_PARAMS_FORCE)		
							BREAK
						ENDSWITCH
					ELIF relHash = REL_FIB
						SWITCH GET_RANDOM_INT_IN_RANGE(1, 3)
							CASE 1
								CDEBUG3LN(DEBUG_MISSION, "AMBIENT DIALOGUE: FIB 1")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(nearbyPeds[iRnd], "MCH3_ASAA", "MIC3FIB1", SPEECH_PARAMS_FORCE)
							BREAK
							CASE 2
								CDEBUG3LN(DEBUG_MISSION, "AMBIENT DIALOGUE: FIB 2")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(nearbyPeds[iRnd], "MCH3_ATAA", "MIC3FIB2", SPEECH_PARAMS_FORCE)
							BREAK
						ENDSWITCH
					ELIF relHash = REL_MW
						// No MW in this stage	
					ENDIF
					RESTART_TIMER_NOW(tmrControlAmbientDialogue)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC UPDATE_FIRST_AREA_WAR_CRIES()
	INT randLine
	IF IS_ENTITY_OK(m_pedAllyDave)
	AND IS_ENTITY_OK(PLAYER_PED_ID())
			
		//Control the guys war cries!
		IF GET_TIMER_IN_SECONDS(m_tmrWarCries) >= 12.5
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			INT randDialogue = GET_RANDOM_INT_IN_RANGE(0,2)
			
			SWITCH randDialogue
			
				CASE 0
					IF IS_PED_SHOOTING(PLAYER_PED_ID())
						randLine = GET_RANDOM_INT_IN_RANGE(0,2)
						IF randLine = 0
							IF Play_Warcry("M3_FAMIWAR")
								RESTART_TIMER_NOW(m_tmrWarCries)
							ENDIF
						ELIF randLine = 1
							IF Play_Warcry("M3_MSHOUT")
								RESTART_TIMER_NOW(m_tmrWarCries)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF IS_PED_SHOOTING(m_pedAllyDave)
						IF Play_Warcry("M3_FADVWAR")
							RESTART_TIMER_NOW(m_tmrWarCries)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH				
		ENDIF
	ENDIF
ENDPROC


structTimer	tmr_CIA_Yell
PED_INDEX 	pedCIAToYell
PROC UPDATE_CIA_GUY_YELLING()
	SWITCH iCIAGuyState
		CASE 0 
			RESTART_TIMER_NOW(tmr_CIA_Yell)
			iCIAGuyState++
		BREAK
		
		CASE 1
			IF GET_TIMER_IN_SECONDS(tmr_CIA_Yell) >= 10.0
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF) 
					pedCIAToYell = GET_ALIVE_PED_FROM_PED_STRUCT(m_psFirstAreaCIA)
					IF IS_ENTITY_OK(pedCIAToYell)
						ADD_PED_FOR_DIALOGUE(sConvo, 8, pedCIAToYell, "MIC3CIA1")
						iCIAGuyState = 2
					ELSE
						iCIAGuyState = 99
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF Play_Conversation("M3_IAA1")
				iCIAGuyState = 99
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC UPDATE_MICHAEL_DAVE_RADIO_CONVERSATION()
	SWITCH eMessagesMichaelDave
		CASE MISSION_MESSAGE_01
			//IF Play_Conversation("M3_FASTART")
			IF Play_Conversation("M3_RADIO1")
				eMessagesMichaelDave = MISSION_MESSAGE_02
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
//				PRINT_NOW("M3_OBJFRSTA", DEFAULT_GOD_TEXT_TIME, 1)
				eMessagesMichaelDave = MISSION_MESSAGE_03
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_03
			IF m_bDoStairFires
				IF IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
					CLEAR_PRINTS()
				ENDIF
				// Play crash/cut off dialogue
				eMessagesFirstArea = MISSION_MESSAGE_09
				eMessagesMichaelDave = MISSION_MESSAGE_04
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_04
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
				SAFE_REMOVE_BLIP(blip_Objective)
				SAFE_BLIP_COORD(blip_Objective, (<<-2213.08618, 240.99930, 183.60487>>))
				PRINT_NOW("M3_OBJFRSTA", DEFAULT_GOD_TEXT_TIME, 1)
				RESTART_TIMER_NOW(tmrControlMichaelDaveDialogue)
				eMessagesMichaelDave = MISSION_MESSAGE_05
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_05
			IF GET_TIMER_IN_SECONDS(tmrControlMichaelDaveDialogue) >= 1.0
				IF Play_Conversation("M3_RADIO2")
					eMessagesMichaelDave = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC  UPDATE_FIRST_AREA_CONVERSATIONS()
	UPDATE_MICHAEL_DAVE_RADIO_CONVERSATION()
	UPDATE_FIRST_AREA_AMBIENT_DIALOGUE()
	UPDATE_FIRST_AREA_WAR_CRIES()
	UPDATE_CIA_GUY_YELLING()
	
	SWITCH eMessagesFirstArea
		
		CASE MISSION_MESSAGE_01
			IF Play_Conversation("M3_EXPLODE")
				eMessagesFirstArea = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_09
			IF Play_Conversation("M3_FADVEXP")
				eMessagesFirstArea = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_10
			IF Play_Conversation("M3_DVTHX")
				eMessagesFirstArea = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC UPDATE_STREAM_PLAYING()
	SWITCH iStreamPlayingState
		CASE 0 
			IF LOAD_STREAM("MIC_3_DISTANT_GUNFIGHT_MASTER")
				iStreamPlayingState = 1
			ENDIF
		BREAK
		
		CASE 1
			PLAY_STREAM_FROM_POSITION(<<-2245.99463, 266.09586, 173.60196>>)
			iStreamPlayingState = 2
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_ON_STAIRS_WAR_CRIES()
	IF IS_ENTITY_OK(m_pedAllyDave)
		//Control the guys war cries!
		IF GET_TIMER_IN_SECONDS(m_tmrWarCries) >= 10.0
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF IS_PED_SHOOTING(m_pedAllyDave)
				IF Play_Warcry("M3_OSDVWAR")
					RESTART_TIMER_NOW(m_tmrWarCries)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_DAVE_ON_STAIRS_CONVERSATIONS()
	SWITCH eDaveStairsMessages
		/*
		CASE MISSION_MESSAGE_01
			IF eFirstAreaState > FIRST_AREA_WAIT_TO_START_STAIR_FIGHT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ELIF IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
					CLEAR_PRINTS()
				ENDIF
			ENDIF
			eDaveStairsMessages = MISSION_MESSAGE_02
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF Play_Conversation("M3_OSDVHLP")
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS - Dave needs help!")
				eDaveStairsMessages = MISSION_MESSAGE_03
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_03
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS (03) - print objective to help Dave!")
				PRINT_NOW("M3_OBJHELPD", DEFAULT_GOD_TEXT_TIME, 1)
				eDaveStairsMessages = MISSION_MESSAGE_04
			ELIF IS_PLAYER_IN_TRIGGER_BOX(tb_AbandonDaveAtStairs)
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS (03) - player in abandon warning trigger!")
				KILL_ANY_CONVERSATION()
				IF IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
					CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS (03) - clear prints!")
					CLEAR_PRINTS()
				ENDIF
				eDaveStairsMessages = MISSION_MESSAGE_04
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_04
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_AbandonDaveAtStairs)
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS (04) - player in abandon warning trigger!")
				KILL_ANY_CONVERSATION()
				IF IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
					CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS (04) - clear prints!")
					CLEAR_PRINTS()
				ENDIF
				eDaveStairsMessages = MISSION_MESSAGE_05
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_05
			IF Play_Conversation("M3_OSDVDSP")
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS - play warning dialogue!")
				eDaveStairsMessages = MISSION_MESSAGE_06
			ENDIF
		BREAK
		CASE MISSION_MESSAGE_06
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
				CPRINTLN(DEBUG_MISSION, "UPDATE_DAVE_ON_STAIRS_CONVERSATIONS (06) - print objective to help Dave!")
				PRINT_NOW("M3_OBJHELPD", DEFAULT_GOD_TEXT_TIME, 1)
				eDaveStairsMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		*/
		CASE MISSION_MESSAGE_10
			IF IS_THIS_PRINT_BEING_DISPLAYED("M3_OBJHELPD")
				CLEAR_PRINTS()
			ENDIF
			IF Play_Conversation("M3_OSDVADV")
				eDaveStairsMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MISSION_STAGE_FIRST_AREA()
	
	IF eFirstAreaState > FIRST_AREA_STREAMING
		UPDATE_ALL_ENEMY_BLIPS()
		UPDATE_HELICOPTER()

		UPDATE_HELICOPTER_CRASH()
	ENDIF
	
	IF eFirstAreaState >= DAVE_ON_STAIRS_COMBAT
		UPDATE_DAVE_ON_STAIRS_CONVERSATIONS()
	ENDIF
	
	IF m_bDoStairFires
		BLOCK_STAIRWAY_PATH()
	ENDIF
	
	SWITCH eFirstAreaState
	
		CASE FIRST_AREA_INIT
			
			iBlockReplayCameraTimer = GET_GAME_TIMER() + 2000 // For B*2226230
			
			//Prep the mission progression system here
			Load_Asset_Model(sAssetData, mod_ped_fib)
			Load_Asset_Model(sAssetData, mod_ped_cia)
			Load_Asset_Model(sAssetData, mod_heli_mw)
			Load_Asset_Model(sAssetData, mod_ped_mw)
			Load_Asset_Model(sAssetData, IG_PAPER)
			Load_Asset_Model(sAssetData, P_GDOOR1COLOBJECT_S)
			
			Load_Asset_Model(sAssetData, IG_ANDREAS)
			
			Load_Asset_Recording(sAssetData, rec_mw_heli_1, str_carrecs)
			Load_Asset_Recording(sAssetData, rec_crash_heli,  str_carrecs)
			Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
			eMessagesMichaelDave = MISSION_MESSAGE_01
			eMessagesFirstArea = MISSION_MESSAGE_IDLE
			CREATE_FIRST_AREA_TRIGGERS()
			bTriggeredFIB = FALSE
			m_bDoStairFires = FALSE
			bFirstAreaGoonsRetasked = FALSE
			bDaveTaskedDownstairs = FALSE
			m_iFirstAreaHelicopterState = 1
//			iMerryWeatherMocapState = 0
//			iDreyfussMocapState = 0
			iBlockingPathState = 0
			iCIAGuyState = 0
			
			eDaveStairsMessages = MISSION_MESSAGE_01
			iStreamPlayingState = 0
			RESTART_TIMER_NOW(m_tmrWarCries)
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - going to FIRST_AREA_STREAMING")
			eFirstAreaState = FIRST_AREA_STREAMING
		BREAK
		
		CASE FIRST_AREA_STREAMING
			IF HAS_MODEL_LOADED(mod_ped_fib)
			AND HAS_MODEL_LOADED(mod_ped_cia)
			AND HAS_MODEL_LOADED(mod_heli_mw)
			AND HAS_MODEL_LOADED(mod_ped_mw)
			AND HAS_MODEL_LOADED(IG_PAPER)
			AND HAS_MODEL_LOADED(P_GDOOR1COLOBJECT_S)
			AND HAS_MODEL_LOADED(IG_ANDREAS)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_mw_heli_1, str_carrecs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_crash_heli, str_carrecs)
			AND REQUEST_SCRIPT_AUDIO_BANK(str_AudioBankFIBShootout)
				START_AUDIO_SCENE("MI_3_SHOOTOUT_START")
				m_bCheckFailureKillVolume = TRUE
				CREATE_FIRST_AREA_ENEMIES()
				ADD_PED_FOR_DIALOGUE(sConvo, 3, m_psFirstAreaCIA[0].id, "MIC3CIA3")
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "FIRST AREA", FALSE)
				TRIGGER_MISSION_EVENT(sEvents[mef_manage_ped_spatial_data].sData)
				SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
				IF IS_ENTITY_OK(m_pedAllyDave)
					SETUP_DAVE_ATTRIBUTES()
					INITIALIZE_DAVE_MONITORING_SYSTEM()
				ENDIF
				IF IS_ENTITY_OK(PLAYER_PED_ID())
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				RESTART_TIMER_NOW(tmrControlAmbientDialogue)
				TRIGGER_MUSIC_EVENT("MIC3_FIGHT_START")
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - going to FIRST_AREA_COMBAT")
				eFirstAreaState = FIRST_AREA_COMBAT
			ENDIF
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				CPRINTLN(DEBUG_MISSION, "Player control is off?  Restoring it.")
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		BREAK
		
		CASE FIRST_AREA_COMBAT
			UPDATE_FIRST_AREA_CONVERSATIONS()
			IF NOT bFirstAreaGoonsRetasked
				IF GET_NUMBER_ALIVE_ALL_FACTIONS(m_psFirstAreaCIA, m_psFirstAreaFIB, m_psFirstAreaMW) <= 5
					RETASK_ALL_FIRST_AREA_PEDS()
					bFirstAreaGoonsRetasked = TRUE
				ENDIF
			ENDIF
			
			IF ARE_ALL_ENEMIES_DEAD(m_psFirstAreaCIA, m_psFirstAreaFIB, m_psFirstAreaMW)
			OR IS_PLAYER_IN_TRIGGER_BOX(tb_FirstAreaEndTrigger)
			
				IF NOT IS_AUDIO_SCENE_ACTIVE("MI_3_SHOOTOUT_START")
					START_AUDIO_SCENE("MI_3_SHOOTOUT_START")
				ENDIF
				RESTART_TIMER_NOW(tmr_StartStairFight)
				RESTART_TIMER_NOW(m_tmrWarCries)
				RESTART_TIMER_NOW(tmr_TriggerFIBBackup)
				SET_PED_COMBAT_ATTRIBUTES(m_pedAllyDave, CA_CAN_IGNORE_BLOCKED_LOS_WEIGHTING, FALSE)
				INITIALIZE_DAVE_MONITORING_SYSTEM()
			
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - going to FIRST_AREA_WAIT_TO_START_STAIR_FIGHT")
				eFirstAreaState = FIRST_AREA_WAIT_TO_START_STAIR_FIGHT
			ENDIF
		BREAK
		
		CASE FIRST_AREA_WAIT_TO_START_STAIR_FIGHT
			
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_MichaelStairTrigger)
				//AND IS_SPHERE_VISIBLE((<<-2202.26782, 224.62138, 182.95221>>), 1.0))
			OR GET_TIMER_IN_SECONDS(tmr_StartStairFight) >= 10.0
				CREATE_STAIR_ENEMIES()
				IF IS_ENTITY_OK(m_pedAllyDave)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedAllyDave, FALSE)
					REMOVE_PED_DEFENSIVE_AREA(m_pedAllyDave)
					SET_PED_SPHERE_DEFENSIVE_AREA(m_pedAllyDave, m_flagDaveCoverPositions[1].pos, 3.0, TRUE)
//					TASK_PUT_PED_DIRECTLY_INTO_COVER(m_pedAllyDave, m_flagDaveCoverPositions[1].pos, -1, TRUE, 0, TRUE, TRUE)
				ENDIF
				START_AUDIO_SCENE("MI_3_SHOOTOUT_ENEMIES_ON_STAIRS")
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - going to DAVE_ON_STAIRS_COMBAT")
				eFirstAreaState = DAVE_ON_STAIRS_COMBAT
			ENDIF
		BREAK
		
		CASE DAVE_ON_STAIRS_COMBAT
			UPDATE_CREATE_FIB()
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_CutsceneTrigger)
				STOP_STREAM()
				LOAD_STREAM("MIC_3_HELICOPTER_SHOT_DOWN_MASTER")
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - player in trigger, going to DAVE_ON_STAIRS_MICHAEL_SWAP_WEAPON")
				eFirstAreaState = DAVE_ON_STAIRS_MICHAEL_SWAP_WEAPON
			ENDIF
			
			// If we've killed some, but not all, Dave will advance and kill on the way
			IF NOT bDaveTaskedDownstairs
				IF GET_NUM_PEDS_ALIVE(m_psStairsMW) = 0
					eDaveStairsMessages = MISSION_MESSAGE_10
					REQUEST_CUTSCENE(m_strCutsceneHelicopter)
					REMOVE_PED_DEFENSIVE_AREA(m_pedAllyDave)
					// bug 1955438
					SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_pedAllyDave, FALSE)
					SET_PED_PATH_CAN_USE_CLIMBOVERS(m_pedAllyDave, FALSE)
					
					SET_PED_SPHERE_DEFENSIVE_AREA(m_pedAllyDave, csDaveBottomOfStairsCover.pos, 2.0, TRUE)
					SAFE_OPEN_SEQUENCE()
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, csDaveBottomOfStairsCover.pos, PEDMOVE_SPRINT)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csDaveBottomOfStairsCover.pos, -1, TRUE, 0, TRUE, TRUE, csDaveBottomOfStairsCover.id, TRUE)
					SAFE_PERFORM_SEQUENCE(m_pedAllyDave)
					STOP_AUDIO_SCENE("MI_3_SHOOTOUT_ENEMIES_ON_STAIRS")
					bDaveTaskedDownstairs = TRUE
				ENDIF
			ELSE
				UPDATE_STREAM_PLAYING()
			ENDIF
			
		BREAK
		
		CASE DAVE_ON_STAIRS_MICHAEL_SWAP_WEAPON
			// Give Michael a weapon to match cutscene
			WEAPON_TYPE wtPlayerWeap
			SET_PED_CAN_SWITCH_WEAPON(MIKE_PED_ID(), FALSE)
			IF GET_CURRENT_PED_WEAPON(MIKE_PED_ID(), wtPlayerWeap)
				IF wtPlayerWeap = WEAPONTYPE_ASSAULTRIFLE
					CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - player has assault rifle, going to FIRST_AREA_COMPLETE")
					iCutsceneDelayTime = 0
					eFirstAreaState = FIRST_AREA_COMPLETE
				ELSE
					IF NOT HAS_PED_GOT_WEAPON(MIKE_PED_ID(), WEAPONTYPE_ASSAULTRIFLE)
						CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DAVE_ON_STAIRS - player needs an assault rifle, give it to him")
						GIVE_WEAPON_TO_PED(MIKE_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 100, TRUE, TRUE)
					ELSE
						CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DAVE_ON_STAIRS - giving player assault rifle, start timer, going to DAVE_ON_STAIRS_COMPLETE")
						SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, FALSE)
						TASK_SWAP_WEAPON(MIKE_PED_ID(), TRUE)
					ENDIF
					iCutsceneDelayTime = GET_GAME_TIMER() + 1000
					eFirstAreaState = FIRST_AREA_COMPLETE
				ENDIF
			ENDIF
		BREAK
		
		
		CASE FIRST_AREA_COMPLETE
			IF GET_GAME_TIMER() > iCutsceneDelayTime
				Unload_Asset_Model(sAssetData, IG_ANDREAS)
				Unload_Asset_Model(sAssetData, IG_PAPER)
				
				SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_andreas].id)
				
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_FIRST_AREA - Stage complete, going to DAVE_ON_STAIRS_IDLE")
				eFirstAreaState = FIRST_AREA_IDLE
				REMOVE_COVER_POINT(csFIBStairCoverPoints[0].id)
				REMOVE_COVER_POINT(csFIBStairCoverPoints[1].id)
				SET_PED_CAN_SWITCH_WEAPON(MIKE_PED_ID(), TRUE)
				SAFE_REMOVE_BLIP(blip_Objective)
				SET_PED_COMBAT_ATTRIBUTES(m_pedAllyDave, CA_CAN_IGNORE_BLOCKED_LOS_WEIGHTING, TRUE)
				Mission_Set_Stage(STAGE_TREVOR_SHOOTS_HELICOPTER)	
			ENDIF
		BREAK
		
		CASE FIRST_AREA_IDLE
		BREAK
	ENDSWITCH
ENDPROC

