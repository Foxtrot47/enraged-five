USING "Michael3_support.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_itemsets.sch"

CONST_INT		iCONST_ACCURACY_WARPEDS_BALCONY	5
CONST_INT		iCONST_ACCURACY_WARPEDS_GROUND	1

CONST_INT		iCONST_ACCURACY_WALKWAY_FIB2	2
CONST_INT		iCONST_ACCURACY_WALKWAY_CIA1	10
CONST_INT		iCONST_ACCURACY_WALKWAY_MW2		5
CONST_INT		iCONST_ACCURACY_WALKWAY_MW3		2
CONST_INT		iCONST_ACCURACY_STAIRWAY_MW		2

CONST_INT		iCONST_ACCURACY_IG_6_FIB		5
CONST_INT		iCONST_ACCURACY_IG_7_MW			5
CONST_INT		iCONST_ACCURACY_IG_8_MW			5

CONST_INT		iCONST_ACCURACY_SECOND_WAVE 	5
CONST_INT		iCONST_ACCURACY_THIRD_WAVE		5


ENUM	DAVE_AT_FOUNTAIN_STATE
	DAVE_AT_FOUNTAIN_INIT,
	DAVE_AT_FOUNTAIN_STREAMING,
	DAVE_AT_FOUNTAIN_MAIN,
	DAVE_AT_FOUNTAIN_COMPLETE,
	DAVE_AT_FOUNTAIN_IDLE
ENDENUM
DAVE_AT_FOUNTAIN_STATE	eDaveFountainState = DAVE_AT_FOUNTAIN_INIT
MISSION_MESSAGE		eDaveFountainMessages = MISSION_MESSAGE_01

ENUM	PED_ACCURACY_STATE
	PAS_AS_TREVOR,
	PAS_WAIT_TO_RAISE_ACCURACY,
	PAS_AS_MICHAEL,
	PAS_DONE
ENDENUM
PED_ACCURACY_STATE	ePedAccuracyState = PAS_AS_TREVOR

INT		iMocapIG6State = 0
INT		iMocapIG7State = 0
INT		iMocapIG8State = 0
INT		iMocapMikeOnlyIG7State = 0
INT 	iCIADialogueState = 0
INT		iReminderDialogueState = 0 
INT		iHeliDriveByInitialWaitTime = 0

MISSION_MESSAGE 	eHeli2DialogueState

BOOL	bHeliDialogueSaid = FALSE
BOOL	bTryDaveSayUrgentLine = FALSE
BOOL	bUrgentLineSaid = FALSE

structTimer		tmrStartIG_6				// Timer to start IG_6
structTimer		tmrStartIG_8				// Timer to start IG_8
structTimer		tmrDaveMoveFailsafe			// Failsafe timer until AI gets some stuff fixed
structTimer		tmrReminderDialogue
structTimer		tmrRaiseAccuracy			// Handles tuning accuracy down when we switch to Michael

TRIGGER_BOX		tb_DaveAtFountain
TRIGGER_BOX		tb_SpawnHeli2
TRIGGER_BOX		tb_SpawnIG_6
TRIGGER_BOX		tb_SpawnIG_7
TRIGGER_BOX		tb_CloseIG_7Doors
TRIGGER_BOX		tb_CloseIG_8Doors
TRIGGER_BOX		tb_CloseMikeOnlyFIBDoors

TRIGGER_BOX		tb_SpawnWalkwayFIB2
TRIGGER_BOX		tb_SpawnWalkwayMW2
TRIGGER_BOX		tb_SpawnWalkwayMW3
//TRIGGER_BOX		tb_SpawnWalkwayCIA1
TRIGGER_BOX		tb_SpawnStairwayMW
TRIGGER_BOX		tb_MichaelDownstairs
TRIGGER_BOX		tb_TrevorShouldLeave

TRIGGER_BOX		tb_CIAReactionDialogue
TRIGGER_BOX		tb_DaveDownstairsDialogue

TRIGGER_BOX		tb_MichaelSpawnSecondWave
TRIGGER_BOX		tb_MichaelSpawnThirdWave

TRIGGER_BOX		tb_ReminderDialogue
TRIGGER_BOX		tb_ShutOffKillVolume


BOOL			bSpawnCIAGroup1
BOOL			bSpawnFIBGroup2
BOOL			bSpawnMWGroup2
BOOL			bSpawnMWGroup3
BOOL			bSpawnStairMW
BOOL			bPlayerDownstairs
BOOL			bSpawnMikeOnlyIG7
BOOL			bDaveDownstairsDialoguePlayed
BOOL			bSafeToHaveDialogue
BOOL			bRunTheWar
BOOL			bCanPlayHeliDialogue
BOOL			bShutOffKillVolume
BOOL			bDaveRetaskedToCover
BOOL			bIG7DialoguePlayed
BOOL			bIG8DialoguePlayed

PED_INDEX		pedFIBToSpeak

OBJECT_INDEX	obj_IG8DoorL
OBJECT_INDEX	obj_IG8DoorR

PROC CREATE_DAVE_AT_FOUNTAIN_TRIGGERS()

	tb_SpawnHeli2 = CREATE_TRIGGER_BOX(<<-2229.059082,275.223663,183.354080>>, <<-2231.212891,280.063812,187.854080>>, 6.500000)
	tb_SpawnIG_6 = CREATE_TRIGGER_BOX(<<-2218.376465,252.419479,183.354080>>, <<-2220.788330,257.745911,188.104080>>, 8.000000)		
	tb_SpawnIG_7 = CREATE_TRIGGER_BOX(<<-2233.645508,299.343933,183.355148>>, <<-2235.260010,298.635071,189.355148>>, 8.000000) 		
	tb_SpawnWalkwayFIB2 = CREATE_TRIGGER_BOX(<<-2226.069824,269.024078,183.354080>>, <<-2226.779541,270.540436,189.354080>>, 8.000000)	
	tb_SpawnWalkwayMW2 = CREATE_TRIGGER_BOX(<<-2223.427979,301.122925,183.303024>>, <<-2226.298584,306.909912,186.603027>>, 6.000000)	
	tb_SpawnWalkwayMW3 = CREATE_TRIGGER_BOX(<<-2250.255859,312.132629,183.301987>>, <<-2257.336670,324.838074,186.601273>>, 8.000000)	
	tb_SpawnStairwayMW = CREATE_TRIGGER_BOX(<<-2279.674805,302.213898,183.361572>>, <<-2278.418945,299.515991,189.351624>>, 3.500000)
//	tb_SpawnWalkwayCIA1 = CREATE_TRIGGER_BOX(<<-2222.714111,299.955566,183.303024>>, <<-2225.954834,307.142853,186.603027>>, 8.000000)

	tb_DaveAtFountain = CREATE_TRIGGER_BOX(<<-2248.243408,269.741760,173.101959>>, <<-2249.806152,273.130096,175.601959>>, 5.000000)
	tb_DaveAtFountain = tb_DaveAtFountain
	tb_TrevorShouldLeave = CREATE_TRIGGER_BOX(<<-2267.455322,315.045715,183.352127>>, <<-2286.537354,306.799316,185.351227>>, 11.250000)
	tb_MichaelDownstairs = CREATE_TRIGGER_BOX(<<-2279.402344,302.329895,183.361572>>, <<-2271.738770,286.466248,175.352112>>, 11.250000)
	
	//Michael AI state machine triggers
	tb_MichaelSpawnSecondWave = CREATE_TRIGGER_BOX(<<-2229.541748,275.717682,183.354080>>, <<-2231.284180,279.606781,185.354080>>, 5.250000)
	tb_MichaelSpawnThirdWave = CREATE_TRIGGER_BOX(<<-2249.341309,309.196350,183.351990>>, <<-2252.509521,317.150970,185.351273>>, 11.250000)

	tb_CIAReactionDialogue = CREATE_TRIGGER_BOX(<<-2255.105713,320.620483,183.101273>>, <<-2286.249023,306.856567,187.101212>>, 13.750000)
	tb_DaveDownstairsDialogue = CREATE_TRIGGER_BOX(<<-2281.745117,303.645599,173.102112>>, <<-2269.277100,276.101929,176.102112>>, 27.500000)

	tb_ReminderDialogue = CREATE_TRIGGER_BOX(<<-2192.543701,218.409454,191.074844>>, <<-2240.757324,325.476227,182.632874>>, 46.000000)
	tb_ShutOffKillVolume = CREATE_TRIGGER_BOX(<<-2268.196289,315.128326,203.602112>>, <<-2341.796875,281.688995,169.467224>>, 102.750000)

	tb_CloseIG_7Doors = CREATE_TRIGGER_BOX(<<-2257.980957,319.804199,183.351273>>, <<-2247.894287,324.256836,186.927643>>, 8.500000)
	tb_CloseIG_8Doors = CREATE_TRIGGER_BOX(<<-2278.864990,265.310730,183.351349>>, <<-2281.860840,273.366150,186.701462>>, 11.250000)
	tb_CloseMikeOnlyFIBDoors = CREATE_TRIGGER_BOX(<<-2224.927002,303.156647,183.353027>>, <<-2213.796631,308.538483,186.927643>>, 8.500000)

	CREATE_IG_8_COVER_POINTS()
	
	Create_Cover_Point(csIG_6CoverPoints[0], (<<-2225.15332, 271.64371, 183.60408>>), 201.4499, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_0TO60, FALSE)
	Create_Cover_Point(csIG_6CoverPoints[1], (<<-2229.24097, 269.47818, 183.60406>>), 201.4499, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, COVARC_0TO60, TRUE)
	
	tb_TrevorShouldLeave = tb_TrevorShouldLeave
ENDPROC

PROC UPDATE_THE_WAR()
	UPDATE_ENEMY_WAVE(m_EnemyWaveFIB, m_psWarFIB, m_vecFIBSpawnPoints, mod_ped_fib, m_tbDefensiveAreaWarFIB, TRUE)
	//UPDATE_ENEMY_WAVE(m_EnemyWaveCIA, m_psWarCIA, m_vecCIASpawnPoints, mod_ped_cia, m_tbDefensiveAreaWarCIA)
	UPDATE_ENEMY_WAVE(m_EnemyWaveMWCourt, m_psWarMWCourtyard, m_vecMWCourtSpawnPoints, mod_ped_mw, m_tbDefensiveAreaWarMWCourtyard)
ENDPROC



PROC RESTORE_ALL_FOUNTAIN_PED_ACCURACIES()
	IF bPlayerDownstairs
		SET_ACCURACY_FOR_PED_STRUCT(m_psWarCIA, iCONST_ACCURACY_WARPEDS_GROUND)
		SET_ACCURACY_FOR_PED_STRUCT(m_psWarFIB, iCONST_ACCURACY_WARPEDS_GROUND)
		SET_ACCURACY_FOR_PED_STRUCT(m_psWarMWCourtyard, iCONST_ACCURACY_WARPEDS_GROUND)
	ELSE
		SET_ACCURACY_FOR_PED_STRUCT(m_psWarCIA, iCONST_ACCURACY_WARPEDS_BALCONY)
		SET_ACCURACY_FOR_PED_STRUCT(m_psWarFIB, iCONST_ACCURACY_WARPEDS_BALCONY)
		SET_ACCURACY_FOR_PED_STRUCT(m_psWarMWCourtyard, iCONST_ACCURACY_WARPEDS_BALCONY)
	ENDIF
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayFIB2, iCONST_ACCURACY_WALKWAY_FIB2)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayCIA1, iCONST_ACCURACY_WALKWAY_CIA1)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayMW2, iCONST_ACCURACY_WALKWAY_MW2)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayMW3, iCONST_ACCURACY_WALKWAY_MW3)
//	SET_ACCURACY_FOR_PED_STRUCT(m_psStairwayMW, iCONST_ACCURACY_STAIRWAY_MW)
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psFIBIG_6, iCONST_ACCURACY_IG_6_FIB)
	//SET_ACCURACY_FOR_PED_STRUCT(m_psMWIG_7, iCONST_ACCURACY_IG_7_MW)
	IF IS_ENTITY_OK(m_psMWIG_7.id)
		SET_PED_ACCURACY(m_psMWIG_7.id, iCONST_ACCURACY_IG_7_MW)
	ENDIF
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psMWIG_8, iCONST_ACCURACY_IG_8_MW)
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psMichaelSecondWave, iCONST_ACCURACY_SECOND_WAVE)
	SET_ACCURACY_FOR_PED_STRUCT(m_psMichaelThirdWave, iCONST_ACCURACY_THIRD_WAVE)

ENDPROC

PROC LOWER_ALL_FOUNTAIN_PEDS_ACCURACIES()
	SET_ACCURACY_FOR_PED_STRUCT(m_psWarCIA, 0)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWarFIB, 0)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWarMWCourtyard, 0)
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayFIB2, 0)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayCIA1, 0)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayMW2, 0)
	SET_ACCURACY_FOR_PED_STRUCT(m_psWalkwayMW3, 0)
//	SET_ACCURACY_FOR_PED_STRUCT(m_psStairwayMW, 0)
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psFIBIG_6, 0)
	//SET_ACCURACY_FOR_PED_STRUCT(m_psMWIG_7, 0)
	IF IS_ENTITY_OK(m_psMWIG_7.id)
		SET_PED_ACCURACY(m_psMWIG_7.id, 0)
	ENDIF
	SET_ACCURACY_FOR_PED_STRUCT(m_psMWIG_8, 0)
	
	SET_ACCURACY_FOR_PED_STRUCT(m_psMichaelSecondWave, 0)
	SET_ACCURACY_FOR_PED_STRUCT(m_psMichaelThirdWave, 0)
ENDPROC


PROC UPDATE_ACCURACY_STATE
	SWITCH ePedAccuracyState
		CASE PAS_AS_TREVOR
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				LOWER_ALL_FOUNTAIN_PEDS_ACCURACIES()
				RESTART_TIMER_NOW(tmrRaiseAccuracy)
				CPRINTLN(DEBUG_MISSION, "UPDATE_ACCURACY_STATE - PAS_AS_TREVOR: Switched to Michael, Going to PAS_WAIT_TO_RAISE_ACCURACY")
				ePedAccuracyState = PAS_WAIT_TO_RAISE_ACCURACY
			ENDIF
		BREAK
	
		CASE PAS_WAIT_TO_RAISE_ACCURACY
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CPRINTLN(DEBUG_MISSION, "UPDATE_ACCURACY_STATE - PAS_WAIT_TO_RAISE_ACCURACY: Switched early, returning to PAS_AS_TREVOR")
				RESTORE_ALL_FOUNTAIN_PED_ACCURACIES()
				ePedAccuracyState = PAS_AS_TREVOR
			ENDIF
			
			IF GET_TIMER_IN_SECONDS(tmrRaiseAccuracy) >= 5.0
				CPRINTLN(DEBUG_MISSION, "UPDATE_ACCURACY_STATE - PAS_WAIT_TO_RAISE_ACCURACY: Timer expired, going to PAS_AS_MICHAEL")
				RESTORE_ALL_FOUNTAIN_PED_ACCURACIES()
				ePedAccuracyState = PAS_AS_MICHAEL
			ENDIF
		BREAK
		
		CASE PAS_AS_MICHAEL
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CPRINTLN(DEBUG_MISSION, "UPDATE_ACCURACY_STATE - PAS_AS_MICHAEL: Switched to Trevor, Going to PAS_AS_TREVOR")
				ePedAccuracyState = PAS_AS_TREVOR
			ENDIF
		BREAK
		
		CASE PAS_DONE
		BREAK
	ENDSWITCH
ENDPROC



PROC UPDATE_WALKWAY_SPAWNS()
	// FIB group immediately after the mocap'd group
	IF NOT bSpawnFIBGroup2
		IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnWalkwayFIB2)
			SETUP_ENEMY(m_psWalkwayFIB2[0], mod_ped_fib, (<<-2217.39917, 288.01218, 183.60303>>), 203.2528, (<<-2225.86255, 279.07101, 183.60408>>), "WKWAY_FIB2_1")
			REMOVE_PED_DEFENSIVE_AREA(m_psWalkwayFIB2[0].id)
			SAFE_OPEN_SEQUENCE()
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, (<<-2223.18872, 281.20486, 183.60408>>), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100)
			SAFE_PERFORM_SEQUENCE(m_psWalkwayFIB2[0].id)
			
			
//			SETUP_ENEMY(m_psWalkwayFIB2[1], mod_ped_fib, (<<-2219.49805, 293.03241, 183.60303>>), 203.2528, (<<-2217.10791, 283.48981, 183.60408>>), "WKWAY_FIB2_2")
//			REMOVE_PED_DEFENSIVE_AREA(m_psWalkwayFIB2[1].id)
//			SAFE_OPEN_SEQUENCE()
//				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, (<<-2217.10791, 283.48981, 183.60408>>), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
//				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100)
//			SAFE_PERFORM_SEQUENCE(m_psWalkwayFIB2[1].id)
			
			SET_PED_TO_FIRE_MANY_BLANKS(m_psWalkwayFIB2[0].id)
//			SET_PED_TO_FIRE_MANY_BLANKS(m_psWalkwayFIB2[1].id)
			bSpawnFIBGroup2 = TRUE
		ENDIF
	ENDIF
	
	// Merryweather group that spawns near the monster closet
	IF NOT bSpawnMWGroup2
		IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnWalkwayMW2)
//			Create_Mission_Ped(m_psWalkwayMW2[0], mod_ped_mw, (<<-2254.02759, 318.10284, 183.60127>>), 203.2528, "WKWAY_MW2_1", REL_MW, weap_mw_rifle, 5)
//			SET_PED_SPHERE_DEFENSIVE_AREA(m_psWalkwayMW2[0].id, (<<-2255.32129, 316.06833, 183.60127>>), 2.0)
//			TASK_SEEK_COVER_TO_COORDS(m_psWalkwayMW2[0].id, (<<-2255.32129, 316.06833, 183.60127>>), (<<-2245.87549, 301.91486, 183.60199>>), -1)

			Create_Mission_Ped(m_psWalkwayMW2[1], mod_ped_mw, (<<-2254.15942, 318.98264, 183.60127>>), 203, "WKWAY_MW2_2", REL_MW, weap_mw_rifle, 5)
			SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psWalkwayMW2[1].id)
			SET_PED_SPHERE_DEFENSIVE_AREA(m_psWalkwayMW2[1].id, (<<-2247.46094, 305.55252, 183.60199>>), 2.0, TRUE)
			SAFE_OPEN_SEQUENCE()
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, (<<-2247.46094, 305.55252, 183.60199>>), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
			SAFE_PERFORM_SEQUENCE(m_psWalkwayMW2[1].id)
			
//			SETUP_ENEMY(m_psWalkwayMW2[1], mod_ped_mw, (<<-2254.15942, 318.98264, 183.60127>>), 203.2528, (<<-2250.61182, 311.68082, 183.60199>>), "WKWAY_MW2_2")
//			TASK_PED_RUN_TO_COORD(m_psWalkwayMW2[1].id, (<<-2250.61182, 311.68082, 183.60199>>), TRUE)
//			SET_PED_TO_FIRE_MANY_BLANKS(m_psWalkwayMW2[0].id)
			SET_PED_TO_FIRE_MANY_BLANKS(m_psWalkwayMW2[1].id)
			eDaveFountainMessages = MISSION_MESSAGE_06
			bSpawnMWGroup2 = TRUE
		ENDIF
	ENDIF
	
	// Merryweather group on the opposite side of the long stretch of walkway
	IF NOT bSpawnMWGroup3
		IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnWalkwayMW3)
			Create_Mission_Ped(m_psWalkwayMW3[0], mod_ped_mw, (<<-2286.05518, 310.08865, 183.60109>>), 203.2528, "WKWAY_MW3_1", REL_MW, weap_mw_rifle, 5)
			REMOVE_PED_DEFENSIVE_AREA(m_psWalkwayMW3[0].id)
			SET_PED_SPHERE_DEFENSIVE_AREA(m_psWalkwayMW3[0].id, (<<-2282.88770, 308.17365, 183.60211>>), 2.5, TRUE)
			SET_PED_COMBAT_MOVEMENT(m_psWalkwayMW3[0].id, CM_DEFENSIVE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psWalkwayMW3[0].id, 200.0)
			SET_PED_TO_FIRE_MANY_BLANKS(m_psWalkwayMW3[0].id)
			bSpawnMWGroup3 = TRUE
		ENDIF
	ENDIF
/*
	// CIA guys above the front entrance
	IF NOT bSpawnCIAGroup1
		IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnWalkwayCIA1)
			Create_Mission_Ped(m_psWalkwayCIA1[0], mod_ped_cia, (<<-2287.62085, 335.01697, 183.60150>>), 203, "WKWAY_CIA1_1", REL_CIA_FINAL, weap_cia_handgun)
			Create_Cover_Point(m_csWalkwayCIA[0], (<<-2288.48877, 333.27570, 183.60149>>), 148, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, COVARC_300TO0, FALSE)
			SET_PED_SPHERE_DEFENSIVE_AREA(m_psWalkwayCIA1[0].id, (<<-2288.48877, 333.27570, 183.60149>>), 1.5)
			TASK_SEEK_COVER_TO_COVER_POINT(m_psWalkwayCIA1[0].id, m_csWalkwayCIA[0].id, (<<-2281.84717, 314.84586, 184.45016>>), -1)
			SET_PED_TO_FIRE_MANY_BLANKS(m_psWalkwayCIA1[0].id)
			
			Create_Mission_Ped(m_psWalkwayCIA1[1], mod_ped_cia, (<<-2269.66968, 342.33414, 183.60150>>), 203.2528, "WKWAY_CIA1_2", REL_CIA, weap_cia_handgun)
			Create_Cover_Point(m_csWalkwayCIA[1], (<<-2268.72852, 342.10699, 183.60149>>), 247, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO60, FALSE)
			SET_PED_SPHERE_DEFENSIVE_AREA(m_psWalkwayCIA1[1].id, (<<-2268.72852, 342.10699, 183.60149>>), 1.5)
			TASK_SEEK_COVER_TO_COVER_POINT(m_psWalkwayCIA1[1].id, m_csWalkwayCIA[1].id, (<<-2261.57861, 320.58514, 183.42294>>), -1)
			bSpawnCIAGroup1 = TRUE
		ENDIF
	ENDIF
*/	
	IF NOT bSpawnStairMW
		IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnStairwayMW)
//			SETUP_ENEMY(m_psStairwayMW[0], mod_ped_mw, (<<-2281.85913, 298.66669, 173.60211>>), 203.2528, (<<-2275.01001, 287.51437, 178.61182>>), "STWAY_MW1_1")
//			SET_PED_TO_FIRE_MANY_BLANKS(m_psStairwayMW[0].id)
			bSpawnStairMW = TRUE
			m_bMichaelAIGoesStraightToDave = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_MOCAP_IG_8()
	TEXT_LABEL_15	tlDebugName
	INT i
	
	IF iMocapIG8State > 1
		UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psMWIG_8)
	
		IF NOT bIG8DialoguePlayed
			IF IS_ENTITY_OK(m_psMWIG_8[1].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 14, m_psMWIG_8[1].id, "MERRYWEATHERE")
				IF Play_Conversation("M3_IG8")
					bIG8DialoguePlayed = TRUE
				ENDIF
			ELSE
				bIG8DialoguePlayed = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iMocapIG8State
		CASE 0 
			obj_IG8DoorL = GET_CLOSEST_OBJECT_OF_TYPE((<<-2280.5969, 265.4320, 184.9264>>), 1.0, PROP_CH1_07_DOOR_01L, FALSE)
			obj_IG8DoorR = GET_CLOSEST_OBJECT_OF_TYPE((<<-2278.0386, 266.5699, 184.9264>>), 1.0, PROP_CH1_07_DOOR_01R, FALSE)
			
			m_flIG8_DoorLRatio = 0.0
			m_flIG8_DoorRRatio = 0.0
			
			RESTART_TIMER_NOW(tmrStartIG_8)
			
			iMocapIG8State++
			CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State)
		BREAK
		
		CASE 1
			IF GET_TIMER_IN_SECONDS(tmrStartIG_8) >= 10.0
			
				FOR i = 0 TO COUNT_OF(m_psMWIG_8) - 1
					IF i != 2
					AND i != 4
						tlDebugName = "MW_IG8_"
						tlDebugName += i
						Create_Mission_Ped(m_psMWIG_8[i], mod_ped_mw, (<<-2262.7842, 225.3247, 193.6510>>), 0, tlDebugName, REL_MW, weap_mw_rifle, 5)
						SET_PED_TO_FIRE_MANY_BLANKS(m_psMWIG_8[i].id)
						
						IF IS_ENTITY_OK(m_psMWIG_8[i].id)
							IF DOES_ENTITY_EXIST(obj_IG8DoorL)
								CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - Disabling collision between dude and left door")
								SET_ENTITY_NO_COLLISION_ENTITY(m_psMWIG_8[i].id, obj_IG8DoorL, FALSE)
							ENDIF
							IF DOES_ENTITY_EXIST(obj_IG8DoorR)
								CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - Disabling collision between dude and right door")
								SET_ENTITY_NO_COLLISION_ENTITY(m_psMWIG_8[i].id, obj_IG8DoorR, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			
				syncedScenes[ssf_ig_8_mw_room] = CREATE_SYNCHRONIZED_SCENE((<<-2262.7842, 225.3247, 193.6510>>), (<<0,0,0>>))
				IF IS_ENTITY_OK(m_psMWIG_8[0].id)
					TASK_SYNCHRONIZED_SCENE(m_psMWIG_8[0].id, syncedScenes[ssf_ig_8_mw_room], m_strAnimDict_IG1, "mw01run_into_cover", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_IMPACT_OBJECT)
				ENDIF
				
				IF IS_ENTITY_OK(m_psMWIG_8[1].id)
					TASK_SYNCHRONIZED_SCENE(m_psMWIG_8[1].id, syncedScenes[ssf_ig_8_mw_room], m_strAnimDict_IG1, "mw02run_into_cover", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_IMPACT_OBJECT)
				ENDIF
				
//				IF IS_ENTITY_OK(m_psMWIG_8[2].id)
//					TASK_SYNCHRONIZED_SCENE(m_psMWIG_8[2].id, syncedScenes[ssf_ig_8_mw_room], m_strAnimDict_IG1, "mw03run_into_cover", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_IMPACT_OBJECT)
//				ENDIF
				
				IF IS_ENTITY_OK(m_psMWIG_8[3].id)
					TASK_SYNCHRONIZED_SCENE(m_psMWIG_8[3].id, syncedScenes[ssf_ig_8_mw_room], m_strAnimDict_IG1, "mw04run_into_cover", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_IMPACT_OBJECT)
				ENDIF
				
//				IF IS_ENTITY_OK(m_psMWIG_8[4].id)
//					TASK_SYNCHRONIZED_SCENE(m_psMWIG_8[4].id, syncedScenes[ssf_ig_8_mw_room], m_strAnimDict_IG1, "mw05run_into_cover", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_IMPACT_OBJECT)
//				ENDIF
				
				//Open the doors
				IF bSafeToHaveDialogue
					eDaveFountainMessages = MISSION_MESSAGE_18
				ENDIF
				
				iMocapIG8State = 2
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State)
			ENDIF
		BREAK
		
		CASE 2
			OPEN_IG_8_DOORS()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_ig_8_mw_room])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_8_mw_room]) >= 0.3
					IF GET_NUM_PEDS_IN_TRIGGER_BOX(tb_CloseIG_8Doors, m_psMWIG_8) = 0
						CLOSE_IG_8_DOORS()
						iMocapIG8State = 3
						CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State)
					ENDIF
				ENDIF
			ELSE
				iMocapIG8State = 3
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State)
			ENDIF
		BREAK
		
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_ig_8_mw_room])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_8_mw_room]) = 1.0
					FOR i = 0 TO COUNT_OF(m_psMWIG_8) - 1
						IF IS_ENTITY_OK(m_psMWIG_8[i].id)
							SAFE_OPEN_SEQUENCE()
								TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csIG_8CoverPoints[i].pos, GET_RANDOM_INT_IN_RANGE(1000, 2000), TRUE, 0.0, FALSE, csIG_8CoverPoints[i].bFaceLeft, csIG_8CoverPoints[i].id)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 250)
							SAFE_PERFORM_SEQUENCE(m_psMWIG_8[i].id)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psMWIG_8[i].id)
							SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMWIG_8[i].id)
						ENDIF
					ENDFOR
					iMocapIG8State = 4
					CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State)
				ENDIF
			ELSE
				FOR i = 0 TO COUNT_OF(m_psMWIG_8) - 1
					IF IS_ENTITY_OK(m_psMWIG_8[i].id)
						SAFE_OPEN_SEQUENCE()
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, csIG_8CoverPoints[i].pos, PEDMOVE_RUN, -1)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csIG_8CoverPoints[i].pos, GET_RANDOM_INT_IN_RANGE(1000, 2000), TRUE, 0.0, FALSE, csIG_8CoverPoints[i].bFaceLeft, csIG_8CoverPoints[i].id)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 250)
						SAFE_PERFORM_SEQUENCE(m_psMWIG_8[i].id)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psMWIG_8[i].id)
						SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMWIG_8[i].id)
					ENDIF
				ENDFOR
				CLOSE_IG_8_DOORS()
				iMocapIG8State = 4
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State)
			ENDIF
		BREAK
		
		CASE 4
			IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L)) <> 0.0
			OR DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_RICHES_L)) <> 0.0
				CLOSE_IG_8_DOORS()
			ELSE
				iMocapIG8State = 5
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_8 - going to State ", iMocapIG8State, ", done.")
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC UPDATE_MOCAP_IG_7()


	
	IF iMocapIG7State > 1
		UPDATE_PED_STRUCT_BLIP(m_psMWIG_7)
	ENDIF
	
	IF iMocapIG7State > 2
		IF NOT bIG7DialoguePlayed
			IF IS_ENTITY_OK(m_psMWIG_7.id)
				ADD_PED_FOR_DIALOGUE(sConvo, 13, m_psMWIG_7.id, "MERRYWEATHERD")
				IF Play_Conversation("M3_IG7")
					bIG7DialoguePlayed = TRUE
				ENDIF
			ELSE
				bIG7DialoguePlayed = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iMocapIG7State
		CASE 0 
			iMocapIG7State = 1
			m_flIG7_DoorLRatio = 0.0
			m_flIG7_DoorRRatio = 0.0
			CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
		BREAK
		
		CASE 1
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnIG_7)

				Create_Mission_Ped(m_psMWIG_7, mod_ped_mw, (<< -2255.192, 322.262, 184.926 >>), 0, "MW_IG7", REL_MW, weap_mw_rifle)
				SET_PED_TO_FIRE_MANY_BLANKS(m_psMWIG_7.id)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psMWIG_7.id, TRUE)
			
				syncedScenes[ssf_ig_7_mw_bridge] = CREATE_SYNCHRONIZED_SCENE(<< -2255.192, 322.262, 184.926 >>, <<0.000, 0.000, 113.910>>)
			
				IF IS_ENTITY_OK(m_psMWIG_7.id)
					TASK_SYNCHRONIZED_SCENE(m_psMWIG_7.id, syncedScenes[ssf_ig_7_mw_bridge], m_strAnimDict_IG7, "WALKWAY_B", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				
				iMocapIG7State = 2
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
			ENDIF
		BREAK
		
		
		CASE 2
			IF GET_PED_HEALTH_RATIO(m_psMWIG_7.id) < 1.0
				IF IS_ENTITY_OK(m_psMWIG_7.id)
					CLEAR_PED_TASKS(m_psMWIG_7.id)
					REMOVE_PED_DEFENSIVE_AREA(m_psMWIG_7.id, TRUE)
					SET_PED_SPHERE_DEFENSIVE_AREA(m_psMWIG_7.id, (<<-2258.96021, 319.14108, 183.60211>>), 3.0, TRUE)
					SAFE_OPEN_SEQUENCE()
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<-2258.96021, 319.14108, 183.60211>>), PEDMOVE_RUN, -1)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					SAFE_PERFORM_SEQUENCE(m_psMWIG_7.id)
					iMocapIG7State = 100
					CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
				ENDIF
			ENDIF
			OPEN_IG_7_DOORS()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_ig_7_mw_bridge])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_7_mw_bridge]) >= 0.3
					//IF GET_NUM_PEDS_IN_TRIGGER_BOX(tb_CloseIG_7Doors, m_psMWIG_7) = 0
					IF NOT IS_ENTITY_IN_TRIGGER_BOX(tb_CloseIG_7Doors, m_psMWIG_7.id )
						CPRINTLN(DEBUG_MISSION, "Everyone's out of the closet (tee hee), close the doors")
						CLOSE_IG_7_DOORS()
						iMocapIG7State = 3
						CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 100
			CLOSE_IG_7_DOORS()
			IF IS_ENTITY_DEAD(m_psMWIG_7.id)
			OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(m_psMWIG_7.id, (<<-2263.21313, 317.12668, 183.60211>>)) <= 5.0
				SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMWIG_7.id)
				iMocapIG7State = 4
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
			ENDIF
		BREAK
		
		CASE 3
			IF GET_PED_HEALTH_RATIO(m_psMWIG_7.id) = 1.0
				IF IS_ENTITY_OK(m_psMWIG_7.id)
					SET_PED_RESET_FLAG(m_psMWIG_7.id, PRF_InstantBlendToAim, TRUE)
				ENDIF
			ELSE
				IF IS_ENTITY_OK(m_psMWIG_7.id)
					CLEAR_PED_TASKS(m_psMWIG_7.id)
					REMOVE_PED_DEFENSIVE_AREA(m_psMWIG_7.id, TRUE)
					SET_PED_SPHERE_DEFENSIVE_AREA(m_psMWIG_7.id, (<<-2263.21313, 317.12668, 183.60211>>), 3.0, TRUE)
					SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMWIG_7.id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psMWIG_7.id, 100.0)
				ENDIF
				iMocapIG7State = 4
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_ig_7_mw_bridge])
				CPRINTLN(DEBUG_MISSION, "IG_7 progress = ", GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_7_mw_bridge]))
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_7_mw_bridge]) = 1.0
					
					IF IS_ENTITY_OK(m_psMWIG_7.id)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psMWIG_7.id, 100)
					
						SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMWIG_7.id)
						SET_PED_RESET_FLAG(m_psMWIG_7.id, PRF_InstantBlendToAim, TRUE)
					ENDIF
					
					iMocapIG7State = 4
					CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State)
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_QING_L)) <> 0.0
			OR DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_QING_R)) <> 0.0
				CLOSE_IG_7_DOORS()
			ELSE
				iMocapIG7State = 5
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_7 - going to State ", iMocapIG7State, ", done.")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_MOCAP_IG_6()
	TEXT_LABEL_15	tlDebugName
	INT i
	
	IF iMocapIG6State > 1
		UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psFIBIG_6)
	ENDIF
	
	SWITCH iMocapIG6State
		CASE 0 

			RESTART_TIMER_NOW(tmrStartIG_6)

			CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_6 - going to State ", iMocapIG6State)
			iMocapIG6State++
		BREAK
		
		CASE 1
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_SpawnIG_6)
//			OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//			 AND NOT IS_SPHERE_VISIBLE((<<-2225.22388, 280.16913, 183.60408>>), 2.0)
//			 AND GET_TIMER_IN_SECONDS(tmrStartIG_6) >= 3.0)
				
				FOR i = 0 TO COUNT_OF(m_psFIBIG_6) - 1
					tlDebugName = "WKWAY_FIB1_"
					tlDebugName += i 
					Create_Mission_Ped(m_psFIBIG_6[i], mod_ped_fib, (<<0,0,0>>), 0, tlDebugName, REL_FIB_FINAL, weap_fib_rifle, 5)
					SET_PED_TO_FIRE_MANY_BLANKS(m_psFIBIG_6[i].id)		
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFIBIG_6[i].id, TRUE)
				ENDFOR

				syncedScenes[ssf_ig_6_swat_cover] = CREATE_SYNCHRONIZED_SCENE(<< -2225.181, 271.275, 183.599 >>, <<0.000, 0.000, 111.5>>)
			
				IF IS_ENTITY_OK(m_psFIBIG_6[0].id)
					TASK_SYNCHRONIZED_SCENE(m_psFIBIG_6[0].id, syncedScenes[ssf_ig_6_swat_cover], m_strAnimDict_IG6, "SWAT_RUN_2_COVER_A", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
				ENDIF
				
				IF IS_ENTITY_OK(m_psFIBIG_6[1].id)
					TASK_SYNCHRONIZED_SCENE(m_psFIBIG_6[1].id, syncedScenes[ssf_ig_6_swat_cover], m_strAnimDict_IG6, "SWAT_RUN_2_COVER_B", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
				ENDIF
				
				IF IS_ENTITY_OK(m_psFIBIG_6[2].id)
//					TASK_SYNCHRONIZED_SCENE(m_psFIBIG_6[2].id, syncedScenes[ssf_ig_6_swat_cover], m_strAnimDict_IG6, "SWAT_RUN_2_RAIL", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					SET_ENTITY_COORDS(m_psFIBIG_6[2].id, (<<-2225.95703, 281.26270, 183.60408>>))
					SET_ENTITY_HEADING(m_psFIBIG_6[2].id, 203)
					SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFIBIG_6[2].id)
					SET_PED_SPHERE_DEFENSIVE_AREA(m_psFIBIG_6[2].id, (<<-2231.04028, 279.02939, 183.60408>>), 3.0, TRUE)
					SAFE_OPEN_SEQUENCE()
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, (<<-2231.04028, 279.02939, 183.60408>>), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
					SAFE_PERFORM_SEQUENCE(m_psFIBIG_6[2].id)
				ENDIF
				
				
				
				
//				pedFIBToSpeak = GET_ALIVE_PED_FROM_PED_STRUCT(m_psFIBIG_6)
//				IF IS_ENTITY_OK(pedFIBToSpeak)
//					ADD_PED_FOR_DIALOGUE(sConvo, 4, pedFIBToSpeak, "MIC3FIB1")
//				ENDIF
			
				IF bSafeToHaveDialogue
					eDaveFountainMessages = MISSION_MESSAGE_10
				ENDIF
			
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_6_swat_cover], 0.0)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_6_swat_cover], 0.2)
				ENDIF
				iMocapIG6State = 2
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_6 - going to State ", iMocapIG6State)
			ENDIF
		BREAK
		
		CASE 2
			IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_6_swat_cover]) = 1.0
			OR (IS_PED_INJURED(m_psFIBIG_6[0].id)
			OR IS_PED_INJURED(m_psFIBIG_6[1].id))
			OR (VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(m_psFIBIG_6[0].id, FALSE) ) < 100)
			OR (VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(m_psFIBIG_6[1].id, FALSE) ) < 100)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( m_psFIBIG_6[0].id, PLAYER_PED_ID() )
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( m_psFIBIG_6[1].id, PLAYER_PED_ID() )
				IF IS_ENTITY_OK(m_psFIBIG_6[0].id) AND NOT IS_PED_INJURED(m_psFIBIG_6[0].id)
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_6_swat_cover]) = 1.0
						SAFE_OPEN_SEQUENCE()
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csIG_6CoverPoints[0].pos, GET_RANDOM_INT_IN_RANGE(1000, 2000), TRUE, 0.0, FALSE, csIG_6CoverPoints[0].bFaceLeft, csIG_6CoverPoints[0].id)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 250)
						SAFE_PERFORM_SEQUENCE(m_psFIBIG_6[0].id)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFIBIG_6[0].id)
					ELSE
						IF IS_ENTITY_OK(m_psFIBIG_6[0].id) AND NOT IS_PED_INJURED(m_psFIBIG_6[0].id)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psFIBIG_6[0].id, 250)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_OK(m_psFIBIG_6[1].id) AND NOT IS_PED_INJURED(m_psFIBIG_6[1].id)
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_6_swat_cover]) = 1.0
						SAFE_OPEN_SEQUENCE()
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csIG_6CoverPoints[1].pos, GET_RANDOM_INT_IN_RANGE(1000, 2000), TRUE, 0.0, FALSE, csIG_6CoverPoints[1].bFaceLeft, csIG_6CoverPoints[1].id)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 250)
						SAFE_PERFORM_SEQUENCE(m_psFIBIG_6[1].id)
					ELSE
						IF IS_ENTITY_OK(m_psFIBIG_6[1].id) AND NOT IS_PED_INJURED(m_psFIBIG_6[1].id)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psFIBIG_6[1].id, 250)
						ENDIF
					ENDIF
					FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFIBIG_6[1].id)
				ENDIF
				
				SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFIBIG_6[0].id)
				SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psFIBIG_6[1].id)
				iMocapIG6State = 3
				CPRINTLN(DEBUG_MISSION, "UPDATE_MOCAP_IG_6 - Done")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



FUNC PED_INDEX	GET_FIB_PED_TO_YELL_AT_HELI()
	PED_INDEX pedRetVal
	pedRetVal = GET_ALIVE_PED_FROM_PED_STRUCT(m_psWarFIB)
	IF IS_ENTITY_OK(pedRetVal)
		RETURN pedRetVal
	ENDIF
	
	pedRetVal = GET_ALIVE_PED_FROM_PED_STRUCT(m_psFIBIG_6)
	IF IS_ENTITY_OK(pedRetVal)
		RETURN pedRetVal
	ENDIF
	
	pedRetVal = GET_ALIVE_PED_FROM_PED_STRUCT(m_psMichaelSecondWave)
	IF IS_ENTITY_OK(pedRetVal)
		RETURN pedRetVal
	ENDIF
	
	RETURN NULL
ENDFUNC

structTimer	tmrIgnoreCrashDialogue
PROC UPDATE_HELI_2_DIALOGUE()
	INT	iRandomDialogue
	
	SWITCH eHeli2DialogueState
		CASE MISSION_MESSAGE_01
			pedFIBToSpeak = GET_FIB_PED_TO_YELL_AT_HELI()
			IF IS_ENTITY_OK(pedFIBToSpeak)
				ADD_PED_FOR_DIALOGUE(sConvo, 5, pedFIBToSpeak, "MIC3FIB4")
				eHeli2DialogueState = MISSION_MESSAGE_02
			ELSE
				eHeli2DialogueState = MISSION_MESSAGE_03
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF IS_ENTITY_OK(pedFIBToSpeak)
				IF Play_Conversation("M3_HELI")
					eHeli2DialogueState = MISSION_MESSAGE_03
				ENDIF
			ELSE
				eHeli2DialogueState = MISSION_MESSAGE_03
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_03
			iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 2)
			// Dave says his line
			IF iRandomDialogue = 0
				IF Play_Conversation("M3_CHOPPER")
					eHeli2DialogueState = MISSION_MESSAGE_04
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_04
			IF Play_Conversation("M3_TAKEOUT")
				eHeli2DialogueState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_05
			IF GET_TIMER_IN_SECONDS(tmrIgnoreCrashDialogue) >= 2.5
				CPRINTLN(DEBUG_MISSION, "Don't play the helicopter crash dialogue if it's been too long.")
				eHeli2DialogueState = MISSION_MESSAGE_IDLE
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF Play_Conversation("M3_HELI2T")
					eHeli2DialogueState = MISSION_MESSAGE_IDLE
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF Play_Conversation("M3_HELI2M")
					eHeli2DialogueState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC


PROC EVENT_heli_2(MISSION_EVENT_DATA &sData)
	
	IF bSafeToHaveDialogue
		IF bCanPlayHeliDialogue
			UPDATE_HELI_2_DIALOGUE()
		ENDIF
	ENDIF
	
	IF sData.iStage = 1
		Load_Asset_Model(sAssetData, mod_ped_mw)
		Load_Asset_Model(sAssetData, mod_heli_mw)
		Load_Asset_Recording(sAssetData, rec_mw_heli_2, str_carrecs)
		
		IF HAS_MODEL_LOADED(mod_ped_mw)
		AND HAS_MODEL_LOADED(mod_heli_mw)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_mw_heli_2, str_carrecs)
		AND IS_ENTITY_IN_TRIGGER_BOX(tb_SpawnHeli2, MIKE_PED_ID())
			vehs[mvf_mw_heli_2].id = CREATE_VEHICLE(mod_heli_mw, << -2140.1531, 57.7755, 149.4462 >>, 0.0)
			Create_Mission_Ped_In_Vehicle(peds[mpf_mw_heli_2_pilot], mod_ped_mw, vehs[mvf_mw_heli_2], VS_DRIVER, "MW_HELI_2", REL_MW, WEAPONTYPE_UNARMED, 5, 0, FALSE)
			SAFE_BLIP_VEHICLE(vehs[mvf_mw_heli_2].blip, vehs[mvf_mw_heli_2].id, TRUE)

			SET_PED_SHOOT_RATE(peds[mpf_mw_heli_2_pilot].id, 100)
			SET_PED_FIRING_PATTERN(peds[mpf_mw_heli_2_pilot].id, FIRING_PATTERN_BURST_FIRE_HELI)
			
			START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_2].id, rec_mw_heli_2, str_carrecs)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_2].id, 4000.000)
		
			sData.iStage++
		ENDIF
	ELIF sData.iStage > 1
		IF NOT bHeliDialogueSaid
			IF NOT IS_ENTITY_OK(vehs[mvf_mw_heli_2].id)
			OR NOT IS_VEHICLE_DRIVEABLE(vehs[mvf_mw_heli_2].id)
				RESTART_TIMER_NOW(tmrIgnoreCrashDialogue)
				eHeli2DialogueState = MISSION_MESSAGE_05
				bHeliDialogueSaid = TRUE
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_mw_heli_2].id)
		AND NOT IS_PED_INJURED(peds[mpf_mw_heli_2_pilot].id)
			SWITCH sData.iStage
				CASE 2
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_2].id)
						FLOAT fVehicleRecPos 
						fVehicleRecPos = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_mw_heli_2].id)	
						#IF IS_DEBUG_BUILD
							f_debug_rec_phase = fVehicleRecPos
						#ENDIF
						
						IF fVehicleRecPos >= 13327.000
							bCanPlayHeliDialogue = TRUE
							sData.iStage++
						ENDIF
					ELSE
						sData.iStage++
					ENDIF
				BREAK
				CASE 3
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_2].id)
						FLOAT fVehicleRecPos 
						fVehicleRecPos = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_mw_heli_2].id)	
						#IF IS_DEBUG_BUILD
							f_debug_rec_phase = fVehicleRecPos
						#ENDIF
						IF fVehicleRecPos >= 22000.0
							STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_2].id)
							SET_PED_FIRING_PATTERN(peds[mpf_mw_heli_2_pilot].id, FIRING_PATTERN_BURST_FIRE_HELI)
							iHeliDriveByInitialWaitTime = GET_GAME_TIMER() + 5000
							sData.iStage++
						ENDIF
					ELSE
						iHeliDriveByInitialWaitTime = GET_GAME_TIMER() + 5000
						sData.iStage++
					ENDIF
				BREAK
				CASE 4
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_mw_heli_2_pilot].id, SCRIPT_TASK_VEHICLE_MISSION)
					OR GET_GAME_TIMER() - peds[mpf_mw_heli_2_pilot].iGenericFlag > 1000

						TASK_HELI_MISSION(peds[mpf_mw_heli_2_pilot].id, vehs[mvf_mw_heli_2].id, null, null, <<-2267.3616, 313.1219, 197.7129>>, 
						MISSION_GOTO, 3.0, 0.1, GET_HEADING_FROM_ENTITIES(vehs[mvf_mw_heli_2].id, MIKE_PED_ID()), 197, 10)
						
						sData.iStage++
					ENDIF
				BREAK
				CASE 5
					IF GET_GAME_TIMER() > iHeliDriveByInitialWaitTime
						IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_mw_heli_2_pilot].id, SCRIPT_TASK_VEHICLE_MISSION)
							TASK_DRIVE_BY(peds[mpf_mw_heli_2_pilot].id, MIKE_PED_ID(), null, <<0,0,0>>, 5000, 100, TRUE, FIRING_PATTERN_BURST_FIRE_HELI)
							peds[mpf_mw_heli_2_pilot].iGenericFlag = GET_GAME_TIMER()
							sData.iStage--
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_mw_heli_2].id)
			AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_heli_2].id)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_heli_2].id)
			ENDIF
		ENDIF
	ENDIF
ENDPROC




PROC UPDATE_MIKE_ONLY_FIB_IG_7()
	
	TEXT_LABEL_31	tlDebugName
	INT i
	
	IF iMocapMikeOnlyIG7State > 1
		UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psMichaelSecondWave)
	ENDIF
	
	SWITCH iMocapMikeOnlyIG7State
		CASE 0 
			bSpawnMikeOnlyIG7 = FALSE
			m_flMikeOnly_DoorLRatio = 0.0
			m_flMikeOnly_DoorRRatio = 0.0				
			
			iMocapMikeOnlyIG7State++
			CPRINTLN(DEBUG_MISSION, "UPDATE_MIKE_ONLY_FIB_IG_7 - going to State ", iMocapMikeOnlyIG7State)
			
		BREAK
		
		CASE 1
			IF bSpawnMikeOnlyIG7
			
				FOR i = 0 TO COUNT_OF(m_psMichaelSecondWave) - 1
					tlDebugName = "MCH_WAVE2_"
					tlDebugName += i
					Create_Mission_Ped(m_psMichaelSecondWave[i], mod_ped_fib, << -2222.39136, 305.64835, 184.926 >>, 0, tlDebugName, REL_FIB_FINAL, weap_fib_rifle, 5)
					SET_PED_TO_FIRE_MANY_BLANKS(m_psMichaelSecondWave[i].id)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(m_psMichaelSecondWave[i].id, TRUE)	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psMichaelSecondWave[i].id, TRUE)
				ENDFOR
			
				syncedScenes[ssf_ig_7_fib_mike_only] = CREATE_SYNCHRONIZED_SCENE(<< -2222.39136, 305.64835, 184.926 >>, <<0.000, 0.000, 113.910>>)
			
				IF IS_ENTITY_OK(m_psMichaelSecondWave[0].id)
					TASK_SYNCHRONIZED_SCENE(m_psMichaelSecondWave[0].id, syncedScenes[ssf_ig_7_fib_mike_only], m_strAnimDict_IG7, "WALKWAY_A", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF IS_ENTITY_OK(m_psMichaelSecondWave[1].id)
					TASK_SYNCHRONIZED_SCENE(m_psMichaelSecondWave[1].id, syncedScenes[ssf_ig_7_fib_mike_only], m_strAnimDict_IG7, "WALKWAY_B", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				iMocapMikeOnlyIG7State = 2
				CPRINTLN(DEBUG_MISSION, "UPDATE_MIKE_ONLY_FIB_IG_7 - going to State ", iMocapMikeOnlyIG7State)
			ENDIF
		BREAK
		
		CASE 2
			OPEN_MIKE_ONLY_FIB_DOORS()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_ig_7_fib_mike_only])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_7_fib_mike_only]) >= 0.3
					IF GET_NUM_PEDS_IN_TRIGGER_BOX(tb_CloseMikeOnlyFIBDoors, m_psMichaelSecondWave) = 0
						iMocapMikeOnlyIG7State = 3
						CLOSE_MIKE_ONLY_FIB_DOORS()
						bSafeToTaskMichaelFIBEnemies = TRUE
						CPRINTLN(DEBUG_MISSION, "UPDATE_MIKE_ONLY_FIB_IG_7 - going to State ", iMocapMikeOnlyIG7State)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_OK(m_psMichaelSecondWave[0].id)
					SAFE_OPEN_SEQUENCE()
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, (<<-2234.24414, 299.18054, 183.60515>>), MIKE_PED_ID(), PEDMOVE_WALK, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					SAFE_PERFORM_SEQUENCE(m_psMichaelSecondWave[0].id)
				ENDIF
				
				IF IS_ENTITY_OK(m_psMichaelSecondWave[1].id)
					SAFE_OPEN_SEQUENCE()
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, (<<-2230.75684, 300.76196, 183.60515>>), MIKE_PED_ID(), PEDMOVE_WALK, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
					SAFE_PERFORM_SEQUENCE(m_psMichaelSecondWave[1].id)
				ENDIF
				bSafeToTaskMichaelFIBEnemies = TRUE
				iMocapMikeOnlyIG7State = 4
				CPRINTLN(DEBUG_MISSION, "UPDATE_MIKE_ONLY_FIB_IG_7 - interrupted, going to State ", iMocapMikeOnlyIG7State)
			ENDIF
		BREAK
		
		CASE 3
			IF IS_ENTITY_OK(m_psMichaelSecondWave[0].id)
				SET_PED_RESET_FLAG(m_psMichaelSecondWave[0].id, PRF_InstantBlendToAim, TRUE)
			ENDIF
			IF IS_ENTITY_OK(m_psMichaelSecondWave[1].id)
				SET_PED_RESET_FLAG(m_psMichaelSecondWave[1].id, PRF_InstantBlendToAim, TRUE)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_ig_7_fib_mike_only])
			AND GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_ig_7_fib_mike_only]) = 1.0
			OR (IS_PED_INJURED(m_psMichaelSecondWave[0].id)
			OR  IS_PED_INJURED(m_psMichaelSecondWave[1].id))
				FOR i = 0 TO COUNT_OF(m_psMichaelSecondWave) - 1
					IF IS_ENTITY_OK(m_psMichaelSecondWave[i].id)
						CLEAR_PED_TASKS(m_psMichaelSecondWave[i].id)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psMichaelSecondWave[i].id, 250)
					ENDIF
				ENDFOR
				FOR i = 0 TO COUNT_OF(m_psMichaelSecondWave) - 1
					IF IS_ENTITY_OK(m_psMichaelSecondWave[i].id)
						SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(m_psMichaelSecondWave[i].id)
						SET_PED_RESET_FLAG(m_psMichaelSecondWave[i].id, PRF_InstantBlendToAim, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psMichaelSecondWave[i].id, FALSE)
					ENDIF
				ENDFOR
				
				bSafeToTaskMichaelFIBEnemies = TRUE
				iMocapMikeOnlyIG7State = 4
				CPRINTLN(DEBUG_MISSION, "UPDATE_MIKE_ONLY_FIB_IG_7 - going to State ", iMocapMikeOnlyIG7State, ", done.")
			ENDIF
		BREAK
		
		CASE 4
			IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_TALES_L)) <> 0.0
			OR DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_TALES_R)) <> 0.0
				CLOSE_MIKE_ONLY_FIB_DOORS()
			ELSE
				iMocapMikeOnlyIG7State = 5
				CPRINTLN(DEBUG_MISSION, "UPDATE_MIKE_ONLY_FIB_IG_7 - going to State ", iMocapMikeOnlyIG7State, ", done.")
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC


PROC UPDATE_MICHAEL_AI()
	WEAPON_TYPE weapBest
	
	SWITCH eMichaelAIState
		CASE MICHAEL_AI_INIT
			weapBest = GET_BEST_PED_WEAPON(MIKE_PED_ID())
			SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weapBest, TRUE)
			SET_PED_SPHERE_DEFENSIVE_AREA(MIKE_PED_ID(), GET_ENTITY_COORDS(MIKE_PED_ID()), 1.0)
			TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csMichaelStartingCover.pos, -1, FALSE, 0, TRUE, FALSE, csMichaelStartingCover.id)
			
			CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_WAIT_FOR_DAVE_TO_REACH_FOUNTAIN")
			eMichaelAIState = MICHAEL_AI_WAIT_FOR_DAVE_TO_REACH_FOUNTAIN
		BREAK
		
		CASE MICHAEL_AI_WAIT_FOR_DAVE_TO_REACH_FOUNTAIN
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(m_pedAllyDave, csDaveFountainCover.pos) <= 3.0
				
				CLEAR_PED_TASKS(MIKE_PED_ID())
				REMOVE_PED_DEFENSIVE_AREA(MIKE_PED_ID())
				OPEN_SEQUENCE_TASK(seq)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<-2229.82471, 278.22928, 183.60408>>), PEDMOVE_SPRINT)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_PED_ID(), seq)
				CLEAR_SEQUENCE_TASK(seq)
				CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_WAIT_FOR_SECOND_ENEMIES_SPAWNED")
				eMichaelAIState = MICHAEL_AI_WAIT_FOR_SECOND_ENEMIES_SPAWNED
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_WAIT_FOR_SECOND_ENEMIES_SPAWNED
			IF IS_ENTITY_IN_TRIGGER_BOX(tb_MichaelSpawnSecondWave, MIKE_PED_ID())
				
				bSpawnMikeOnlyIG7 = TRUE	// Tells the FIB guys to spawn from the monster closet
//				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
				SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
				
				CLEAR_PED_TASKS(MIKE_PED_ID())
				REMOVE_PED_DEFENSIVE_AREA(MIKE_PED_ID())
				OPEN_SEQUENCE_TASK(seq)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					IF IS_ENTITY_OK(m_psMichaelSecondWave[0].id)
						TASK_SHOOT_AT_ENTITY(NULL, m_psMichaelSecondWave[0].id, 3000, FIRING_TYPE_DEFAULT)
					ENDIF
					
					IF IS_ENTITY_OK(m_psMichaelSecondWave[1].id)
						TASK_SHOOT_AT_ENTITY(NULL, m_psMichaelSecondWave[1].id, 3000, FIRING_TYPE_DEFAULT)
					ENDIF
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_PED_ID(), seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				RESTART_TIMER_NOW(m_tmrMikeInDanger)
				IF bSafeToHaveDialogue
					eDaveFountainMessages = MISSION_MESSAGE_05
				ENDIF
				CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_SECOND_ZONE")
				eMichaelAIState = MICHAEL_AI_SECOND_ZONE
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_SECOND_ZONE
		
			IF IS_TIMER_STARTED(m_tmrMikeInDanger)
				IF GET_TIMER_IN_SECONDS(m_tmrMikeInDanger) >= 10.0
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), FALSE)
					SET_ENTITY_PROOFS(MIKE_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	//				SET_ENTITY_HEALTH(MIKE_PED_ID(), iCONST_ONE_SHOT_KILL_HEALTH)
					CANCEL_TIMER(m_tmrMikeInDanger)
				ENDIF
			ENDIF
		
			IF GET_NUM_PEDS_ALIVE(m_psMichaelSecondWave) = 0 
				IF NOT bSeriouslyDontDoSwitchUIShitAnymore
					SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
					IF NOT m_bFirstSwitch
						SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
					ENDIF
					CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - bSeriouslyDontDoSwitchUIShitAnymore set to true")
					bSeriouslyDontDoSwitchUIShitAnymore = TRUE
				ENDIF
				IF bSafeToHaveDialogue
					eDaveFountainMessages = MISSION_MESSAGE_11
				ENDIF
				SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
				SET_ENTITY_PROOFS(MIKE_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
				TASK_PED_RUN_TO_COORD(MIKE_PED_ID(), (<<-2250.49536, 311.88065, 183.60199>>), FALSE, PEDMOVE_RUN)
				CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_WAIT_FOR_THIRD_ENEMIES_SPAWNED")
				eMichaelAIState = MICHAEL_AI_WAIT_FOR_THIRD_ENEMIES_SPAWNED
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_WAIT_FOR_THIRD_ENEMIES_SPAWNED
			IF IS_ENTITY_IN_TRIGGER_BOX(tb_MichaelSpawnThirdWave, MIKE_PED_ID())
				SETUP_ENEMY_AND_TASK_TO_COORD(m_psMichaelThirdWave[0], mod_ped_mw, (<<-2276.10010, 313.38040, 183.60211>>), 293.8379, (<<-2259.50708, 320.30450, 183.60211>>), (<<-2250.49536, 311.88065, 183.60199>>), "MCH_WAVE3_1")
				SETUP_ENEMY_AND_TASK_TO_COORD(m_psMichaelThirdWave[1], mod_ped_mw, (<<-2275.43335, 311.93585, 183.60213>>), 293.8379, (<<-2255.67871, 320.16754, 183.60127>>), (<<-2250.49536, 311.88065, 183.60199>>), "MCH_WAVE3_2")
				SETUP_ENEMY_AND_TASK_TO_COORD(m_psMichaelThirdWave[2], mod_ped_mw, (<<-2272.43604, 311.41516, 183.60213>>), 293.8379, (<<-2252.57861, 317.36679, 183.60127>>), (<<-2250.49536, 311.88065, 183.60199>>), "MCH_WAVE3_3")
				SET_PED_TO_FIRE_MANY_BLANKS(m_psMichaelThirdWave[0].id)
				SET_PED_TO_FIRE_MANY_BLANKS(m_psMichaelThirdWave[1].id)
				SET_PED_TO_FIRE_MANY_BLANKS(m_psMichaelThirdWave[2].id)
				IF bSafeToHaveDialogue
					eDaveFountainMessages = MISSION_MESSAGE_07
				ENDIF
				SET_PEDS_AS_ONE_HIT_KILLS(m_psMichaelThirdWave)
				CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_THIRD_ZONE")
				eMichaelAIState = MICHAEL_AI_THIRD_ZONE
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_THIRD_ZONE
			IF GET_NUM_PEDS_ALIVE(m_psMichaelThirdWave) = 0 
				m_bMichaelAIGoesStraightToDave = TRUE	
				//TASK_PED_RUN_TO_COORD(MIKE_PED_ID(), (<<-2280.01, 309.88, 183.60>>), FALSE)
				CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_GET_TO_DAVE")
				eMichaelAIState = MICHAEL_AI_GET_TO_DAVE
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_GET_TO_DAVE
			IF IS_ENTITY_OK(MIKE_PED_ID())
				SAFE_OPEN_SEQUENCE()
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(m_pedAllyDave),PEDMOVE_RUN, -1)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100)
				SAFE_PERFORM_SEQUENCE(MIKE_PED_ID())
				CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_GET_WAIT_TO_REACH_DAVE")
				eMichaelAIState = MICHAEL_AI_GET_WAIT_TO_REACH_DAVE
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_GET_WAIT_TO_REACH_DAVE
			IF GET_DISTANCE_BETWEEN_ENTITIES(MIKE_PED_ID(), m_pedAllyDave) <= 4.0
				IF IS_ENTITY_OK(MIKE_PED_ID())
					SET_PED_COMBAT_MOVEMENT(MIKE_PED_ID(), CM_DEFENSIVE)
					SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(MIKE_PED_ID(), m_tbDefensiveAreaAtFountain)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(MIKE_PED_ID(), 250)
					CPRINTLN(DEBUG_MISSION, "UPDATE_MICHAEL_AI - going to MICHAEL_AI_DONE")
					eMichaelAIState = MICHAEL_AI_DONE
				ENDIF
			ENDIF
		BREAK
		
		CASE MICHAEL_AI_DONE
		BREAK
	ENDSWITCH
ENDPROC


PROC UPDATE_OTHER_PLAYER_TASKING()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		UPDATE_TREVOR_ONLY_ENEMY_SPAWNS()
		UPDATE_MICHAEL_AI()
	ENDIF
ENDPROC

PROC UPDATE_DAVE_AT_FOUNTAIN_WAR_CRIES()
	INT iRandomSpeaker
	INT iRandomLine
	
	IF GET_TIMER_IN_SECONDS(m_tmrWarCries) >= 15.0
	AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		iRandomSpeaker = GET_RANDOM_INT_IN_RANGE(0,3)
		SWITCH iRandomSpeaker
			
			// MICHAEL
			CASE 0
				iRandomLine = GET_RANDOM_INT_IN_RANGE(0,2)
				
				IF iRandomLine = 0 
					IF Play_Warcry("M3_DFMIWAR")
						RESTART_TIMER_NOW(m_tmrWarCries)
					ENDIF
				ELIF iRandomLine = 1
					IF Play_Warcry("M3_FRAY")
						RESTART_TIMER_NOW(m_tmrWarCries)
					ENDIF
				ENDIF
			BREAK
			
			// DAVE
			CASE 1
				IF NOT bPlayerDownstairs
					IF Play_Warcry("M3_DFDVWAR")
						RESTART_TIMER_NOW(m_tmrWarCries)
					ENDIF
				ELSE
					IF Play_Warcry("M3_DFDVWARCY")
						RESTART_TIMER_NOW(m_tmrWarCries)
					ENDIF
				ENDIF
			BREAK
			
			// TREVOR
			CASE 2
				IF Play_Conversation("M3_DFTRWAR")
					RESTART_TIMER_NOW(m_tmrWarCries)
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF

ENDPROC



PROC UPDATE_CIA_WALKWAY_VO()
	SWITCH iCIADialogueState
		CASE 0 
			IF bSpawnCIAGroup1
				iCIADialogueState++
			ENDIF
		BREAK
		
		CASE 1
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_CIAReactionDialogue)
			AND IS_SPHERE_VISIBLE((<<-2275.62256, 331.06650, 185.06528>>), 1.0)
			AND GET_NUM_PEDS_ALIVE(m_psWalkwayCIA1) <> 0
				IF bSafeToHaveDialogue
					eDaveFountainMessages = MISSION_MESSAGE_08
				ENDIF
				iCIADialogueState++
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC UPDATE_REMINDER()
	SWITCH iReminderDialogueState
		CASE 0 
			RESTART_TIMER_NOW(tmrReminderDialogue)
			iReminderDialogueState = 1
		BREAK
		
		CASE 1
			IF GET_TIMER_IN_SECONDS(tmrReminderDialogue) >= 60.0
				IF IS_PLAYER_IN_TRIGGER_BOX(tb_ReminderDialogue)
					IF Play_Conversation("M3_REMIND")
						iReminderDialogueState = 2
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

structTimer	tmrChat
INT	iChatState = 0
PROC UPDATE_CHATS()
	SWITCH iChatState
		CASE 0 
			RESTART_TIMER_NOW(tmrChat)
			CPRINTLN(DEBUG_MISSION, "UPDATE_CHATS - Going to State 1")
			iChatState = 1
		BREAK
		
		CASE 1
			IF GET_TIMER_IN_SECONDS(tmrChat) >= 19.0
				IF Play_Conversation("M3_CHAT1")
					CPRINTLN(DEBUG_MISSION, "UPDATE_CHATS - Going to State 2")
					iChatState = 2
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF Play_Conversation("M3_CHAT2")
					RESTART_TIMER_NOW(tmrChat)
					CPRINTLN(DEBUG_MISSION, "UPDATE_CHATS - Going to State 3")
					iChatState = 3
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF GET_TIMER_IN_SECONDS(tmrChat) >= 15.0
				IF Play_Conversation("M3_CHAT3")
					RESTART_TIMER_NOW(tmrChat)
					CPRINTLN(DEBUG_MISSION, "UPDATE_CHATS - Going to State 4")
					iChatState = 4
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF GET_TIMER_IN_SECONDS(tmrChat) >= 15.0
				IF Play_Conversation("M3_CHAT4")
					CPRINTLN(DEBUG_MISSION, "UPDATE_CHATS - Going to State 5")
					iChatState = 5
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF Play_Conversation("M3_CHAT5")
					CPRINTLN(DEBUG_MISSION, "UPDATE_CHATS - Going to State 6")
					iChatState = 6
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC TASK_DAVE_INTO_COVER()
	CPRINTLN(DEBUG_MISSION, "TASK_DAVE_INTO_COVER")
	IF IS_ENTITY_OK(m_pedAllyDave)
		FREEZE_ENTITY_POSITION(m_pedAllyDave, FALSE)
		CLEAR_PED_TASKS(m_pedAllyDave)
		REMOVE_PED_DEFENSIVE_AREA(m_pedAllyDave)
		SET_PED_SPHERE_DEFENSIVE_AREA(m_pedAllyDave, csDaveFountainCover.pos, 2.0, TRUE)
		SAFE_OPEN_SEQUENCE()
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, csDaveFountainCover.pos, PEDMOVE_RUN, -1)
			TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csDaveFountainCover.pos, -1, TRUE, 0, FALSE, FALSE, csDaveFountainCover.id, TRUE)
		SAFE_PERFORM_SEQUENCE(m_pedAllyDave)
	ENDIF
ENDPROC

structTimer	tmrPlayUrgentLine
PROC UPDATE_DAVE_AT_FOUNTAIN_CONVERSATIONS()
	INT iRandomDialogue
	IF bSafeToHaveDialogue
		UPDATE_DAVE_AT_FOUNTAIN_WAR_CRIES()
		UPDATE_CIA_WALKWAY_VO()
		UPDATE_REMINDER()
		UPDATE_CHATS()
	ENDIF
	
	IF NOT bDaveDownstairsDialoguePlayed
		IF IS_ENTITY_IN_TRIGGER_BOX(tb_DaveDownstairsDialogue, MIKE_PED_ID())
//			IF bSafeToHaveDialogue
//				eDaveFountainMessages = MISSION_MESSAGE_09
//			ENDIF
			TASK_PED_STRUCT_ATTACK_PLAYER(m_psWarCIA)
			TASK_PED_STRUCT_ATTACK_PLAYER(m_psWarFIB)
			TASK_PED_STRUCT_ATTACK_PLAYER(m_psWarMWCourtyard)
			bDaveDownstairsDialoguePlayed = TRUE
			m_bMichaelAIGoesStraightToDave = TRUE
		ENDIF
	ENDIF

	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2298.768066,337.792999,182.351654>>, <<-2275.331299,284.907318,187.351822>>, 9.750000)
		bTryDaveSayUrgentLine = TRUE
	ENDIF
	
	IF NOT bUrgentLineSaid
		IF bTryDaveSayUrgentLine
			IF Play_Conversation("M3_DFDVWARC")
				CPRINTLN(DEBUG_MISSION, "We've said his urgent line - 4")
				bTryDaveSayUrgentLine = FALSE
				bUrgentLineSaid = TRUE
				bRunTheWar = FALSE
			ENDIF
		ENDIF
	ENDIF

	
	SWITCH eDaveFountainMessages
		CASE MISSION_MESSAGE_01
			bSafeToHaveDialogue = FALSE
			iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandomDialogue = 0
				IF Play_Conversation("M3_DVTOFNT")
					eDaveFountainMessages = MISSION_MESSAGE_02
				ENDIF
			ELIF iRandomDialogue = 1
				IF Play_Conversation("M3_MAKEPLAY")
					eDaveFountainMessages = MISSION_MESSAGE_02
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locatesDataMike)
				PRINT_NOW("M3_02", DEFAULT_GOD_TEXT_TIME, 1)
				SAFE_REMOVE_BLIP(blip_Objective)
				blip_Objective = ADD_BLIP_FOR_COORD(<<-2247.73169, 268.95413, 173.60196>>)
				bSafeToHaveDialogue = TRUE
				m_bBlipManagementDuringAbandonChecks = TRUE
				bRunTheWar = TRUE
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_04
			IF Play_Conversation("M3_ESC1")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		
		CASE MISSION_MESSAGE_05
			iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandomDialogue = 0
				IF Play_Conversation("M3_MOREFIB2")
					eDaveFountainMessages = MISSION_MESSAGE_IDLE
				ENDIF
			ELIF iRandomDialogue = 1
				IF Play_Conversation("M3_MFIBM")
					eDaveFountainMessages = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_06
			IF Play_Conversation("M3_DFMIMWRCT")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_07
			IF Play_Conversation("M3_DFMIMWWV3")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_08
			IF Play_Conversation("M3_DFMICIARC")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_09
			IF Play_Conversation("DFDVDWNST")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_10
			IF Play_Conversation("M3_MOREFIB")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_11
			iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 2)
			IF iRandomDialogue = 0
				IF Play_Conversation("M3_MOVEUP")
					eDaveFountainMessages = MISSION_MESSAGE_IDLE
				ENDIF
			ELIF iRandomDialogue = 1
				IF Play_Conversation("M3_SNIPEM")
					eDaveFountainMessages = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
			
			
		BREAK
		
		// Merryweather bursting from first Monster Closet across from Trevor
		CASE MISSION_MESSAGE_18
			iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 2)
			// Dave says his line
			IF iRandomDialogue = 0
				IF Play_Conversation("M3_GUYLEFT")
					eDaveFountainMessages = MISSION_MESSAGE_IDLE
				ENDIF
			ELIF iRandomDialogue = 1
				//Players say their lines
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF Play_Conversation("M3_TBUILD")
						eDaveFountainMessages = MISSION_MESSAGE_IDLE
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF Play_Conversation("M3_MBUILD")
						eDaveFountainMessages = MISSION_MESSAGE_IDLE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_20
			IF Play_Conversation("M3_TFIBM")
				eDaveFountainMessages = MISSION_MESSAGE_IDLE
			ENDIF
			
		BREAK
		
	ENDSWITCH
ENDPROC

PROC MISSION_STAGE_DAVE_AT_FOUNTAIN()
	INT i 
	
	IF eDaveFountainState > DAVE_AT_FOUNTAIN_STREAMING
		UPDATE_DAVE_AT_FOUNTAIN_CONVERSATIONS()
		UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWarCIA)
		UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWalkwayMW2)
		UPDATE_PED_STRUCT_ARRAY_BLIPS(m_psWalkwayMW3)
		UPDATE_PLAYER_SWITCHING()
		UPDATE_ALL_ENEMY_BLIPS()
		UPDATE_WALKWAY_SPAWNS()
		UPDATE_ACCURACY_STATE()
		UPDATE_MOCAP_IG_6()
		UPDATE_MOCAP_IG_7()
		UPDATE_MOCAP_IG_8()
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		OR bSpawnMikeOnlyIG7
			UPDATE_MIKE_ONLY_FIB_IG_7()
		ENDIF
		
		IF bRunTheWar
			UPDATE_THE_WAR()
		ELSE
			IF NOT bDaveRetaskedToCover
				IF GET_NUM_PEDS_ALIVE(m_psWarFIB) + GET_NUM_PEDS_ALIVE(m_psWarMWCourtyard) = 0 
					TASK_DAVE_INTO_COVER()
					bDaveRetaskedToCover = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bShutOffKillVolume
			IF IS_ENTITY_IN_TRIGGER_BOX(tb_ShutOffKillVolume, MIKE_PED_ID())
				m_bCheckFailureKillVolume = FALSE
				bShutOffKillVolume = TRUE
			ENDIF
		ENDIF
		
		IF ePlayerSwitchState = PLAYER_SWITCH_SETUP
		OR ePlayerSwitchState = PLAYER_SWITCH_SELECTING
			UPDATE_OTHER_PLAYER_TASKING()
		ENDIF
		
		IF NOT bPlayerDownstairs
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_MichaelDownstairs)
				SET_ACCURACY_FOR_PED_STRUCT(m_psWarCIA, 1)
				SET_ACCURACY_FOR_PED_STRUCT(m_psWarFIB, 1)
				SET_ACCURACY_FOR_PED_STRUCT(m_psWarMWCourtyard, 1)
				m_EnemyWaveCIA.iAccuracy = 1
				m_EnemyWaveFIB.iAccuracy = 1
				m_EnemyWaveMWCourt.iAccuracy = 1
				m_EnemyWaveMWCourt.flHeartbeatTime = 10.0
				TASK_DAVE_INTO_COVER()
				bPlayerDownstairs = TRUE
				m_bMichaelAIGoesStraightToDave = TRUE
				
			ENDIF
		ENDIF
	ENDIF
	
	IF m_bDoStairFires
		BLOCK_STAIRWAY_PATH()
	ENDIF
	
	SWITCH eDaveFountainState
	
		CASE DAVE_AT_FOUNTAIN_INIT
			
			//Prep the mission progression system here
			Load_Asset_Model(sAssetData, mod_ped_fib)
			Load_Asset_Model(sAssetData, mod_ped_cia)
			Load_Asset_Model(sAssetData, mod_ped_mw)
			Load_Asset_Model(sAssetData, mod_heli_mw)
			Load_Asset_AnimDict(sAssetData, m_strAnimDict_IG1)
			Load_Asset_AnimDict(sAssetData, m_strAnimDict_IG6)
			Load_Asset_AnimDict(sAssetData, m_strAnimDict_IG7)
			Load_Asset_Recording(sAssetData, rec_mw_heli_2, str_carrecs)
			iChatState = 0
			iMocapIG6State = 0
			iMocapIG7State = 0
			iMocapIG8State = 0
			iCIADialogueState = 0
			iReminderDialogueState = 0 
			iMocapMikeOnlyIG7State = 0
			bRunTheWar = FALSE
			bSpawnStairMW = FALSE
			m_bFirstSwitch = TRUE
			m_bDoStairFires = TRUE
			bSpawnMWGroup2 = FALSE
			bSpawnMWGroup3 = FALSE
			bSpawnCIAGroup1 = FALSE
			bSpawnFIBGroup2 = FALSE
			bUrgentLineSaid = FALSE
			bHeliDialogueSaid = FALSE
			bPlayerDownstairs = FALSE
			bShutOffKillVolume = FALSE
			bIG7DialoguePlayed = FALSE
			bIG8DialoguePlayed = FALSE
			bSafeToHaveDialogue = FALSE
			bCanPlayHeliDialogue = FALSE
			bDaveRetaskedToCover = FALSE
			m_bSpawnTrevorOnlyMW1 = FALSE
			m_bSpawnTrevorOnlyMW2 = FALSE
			m_bSpawnTrevorOnlyMW3 = FALSE
			m_bSpawnTrevorOnlyMW4 = FALSE
			m_bSpawnTrevorOnlyMW5 = FALSE
			bTryDaveSayUrgentLine = FALSE
			bSafeToTaskMichaelFIBEnemies = FALSE
			bDaveDownstairsDialoguePlayed = FALSE
			m_bMichaelAIGoesStraightToDave = FALSE
			bSeriouslyDontDoSwitchUIShitAnymore = FALSE
			m_bBlipManagementDuringAbandonChecks = TRUE
			m_bBlipManagementDuringAbandonChecks = FALSE
			eHeli2DialogueState = MISSION_MESSAGE_01
			ePlayerSwitchState = PLAYER_SWITCH_SETUP
			Set_Trev_AI_STATE(AI_T_MOVING_TO_SNIPING_POS)
			eMichaelAIState = MICHAEL_AI_INIT
			ePedAccuracyState = PAS_AS_TREVOR
			CREATE_ENEMY_WAVE(m_EnemyWaveFIB, 5, "WAR_FIB", &ENEMY_WAVE_DEFAULT_TASK, 3.0)
			CREATE_ENEMY_WAVE(m_EnemyWaveCIA, 5, "WAR_CIA", &ENEMY_WAVE_DEFAULT_TASK, 3.0)
			CREATE_ENEMY_WAVE(m_EnemyWaveMWCourt, 5, "WAR_MW", &ENEMY_WAVE_DEFAULT_TASK, 3.0)
			CANCEL_TIMER(m_tmrDelayBeforeSwitch)
			CREATE_DAVE_AT_FOUNTAIN_TRIGGERS()
			CANCEL_TIMER(tmrPlayUrgentLine)
			iMichaelNavBlock = ADD_NAVMESH_BLOCKING_OBJECT((<<-2212.839, 240.922, 184.717>>),(<<6.100, 6.100, 3.300>>), DEG_TO_RAD(25.000))
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DAVE_AT_FOUNTAIN - going to DAVE_AT_FOUNTAIN_STREAMING")
			eDaveFountainState = DAVE_AT_FOUNTAIN_STREAMING
		BREAK
		
		CASE DAVE_AT_FOUNTAIN_STREAMING
			IF HAS_MODEL_LOADED(mod_ped_fib)
			AND HAS_MODEL_LOADED(mod_ped_cia)
			AND HAS_MODEL_LOADED(mod_ped_mw)
			AND HAS_MODEL_LOADED(mod_heli_mw)
			AND HAS_ANIM_DICT_LOADED(m_strAnimDict_IG1)
			AND HAS_ANIM_DICT_LOADED(m_strAnimDict_IG6)
			AND HAS_ANIM_DICT_LOADED(m_strAnimDict_IG7)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_mw_heli_2, str_carrecs)
				RESTART_TIMER_NOW(tmrDaveMoveFailsafe)
				RESTART_TIMER_NOW(m_tmrWarCries)
				
				IF IS_ENTITY_OK(MIKE_PED_ID())
					SET_ENTITY_PROOFS(MIKE_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(MIKE_PED_ID(), TRUE)
				ENDIF
				
				IF IS_ENTITY_OK(TREV_PED_ID())
					SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), TRUE)
				ENDIF
				
				IF IS_ENTITY_OK(m_pedAllyDave)
					REMOVE_PED_DEFENSIVE_AREA(m_pedAllyDave)
					SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(m_pedAllyDave, m_tbDefensiveAreaAtFountain)
					TASK_SEEK_COVER_TO_COVER_POINT(m_pedAllyDave, csDaveFountainCover.id, (<<-2275.31787, 330.67139, 174.51192>>), -1, TRUE)
				ENDIF
				
				UPDATE_THE_WAR()
				TRIGGER_MISSION_EVENT(sEvents[mef_heli_2].sData)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "DAVE BY FOUNTAIN", FALSE)
				SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_MICHAEL, TRUE)
				INITIALIZE_DAVE_MONITORING_SYSTEM()
				CREATE_POOL_NAVMESH_BLOCKERS()
				SET_ENTITY_PROOFS(m_pedAllyDave, TRUE, TRUE, FALSE, TRUE, TRUE)
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DAVE_AT_FOUNTAIN - going to DAVE_AT_FOUNTAIN_MAIN")
				eDaveFountainState = DAVE_AT_FOUNTAIN_MAIN
			ENDIF
		BREAK
		
		CASE DAVE_AT_FOUNTAIN_MAIN
			IF GET_DISTANCE_BETWEEN_ENTITIES(MIKE_PED_ID(), m_pedAllyDave) <= 4.0
			OR GET_DISTANCE_BETWEEN_ENTITIES(TREV_PED_ID(), m_pedAllyDave) <= 4.0
			
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_MICHAEL
				ENDIF
				
				IF IS_ENTITY_IN_TRIGGER_BOX(m_tbLargeRoofVolume, TREV_PED_ID())
					g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_ON_ROOF
				ELSE
					g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
				ENDIF
				m_bBlipManagementDuringAbandonChecks = FALSE
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DAVE_AT_FOUNTAIN - Stage complete, going to DAVE_AT_FOUNTAIN_COMPLETE")
				eDaveFountainState = DAVE_AT_FOUNTAIN_COMPLETE
			ENDIF
		BREAK
		
		CASE DAVE_AT_FOUNTAIN_COMPLETE
		
			REMOVE_ANIM_DICT(m_strAnimDict_IG6)
			REMOVE_ANIM_DICT(m_strAnimDict_IG7)
			
			FOR i =  0 TO COUNT_OF(csIG_6CoverPoints) - 1
				SAFE_REMOVE_COVERPOINT(csIG_6CoverPoints[i])
			ENDFOR
			
			FOR i = 0 TO COUNT_OF(m_psWarFIB) - 1
				FINAL_ENEMY_WAVE_TASKING(m_psWarFIB[i], m_tbDefensiveAreaFinalFightFIB)
			ENDFOR
							
			FOR i = 0 TO COUNT_OF(m_psWarMWCourtyard) - 1
				FINAL_ENEMY_WAVE_TASKING(m_psWarMWCourtyard[i], m_tbDefensiveAreaFinalFightMW)
			ENDFOR
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF IS_ENTITY_OK(MIKE_PED_ID())
					CLEAR_PED_TASKS(MIKE_PED_ID())
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), FALSE)
					SET_PED_COMBAT_MOVEMENT(MIKE_PED_ID(), CM_DEFENSIVE)
					SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(MIKE_PED_ID(), m_tbDefensiveAreaLargerFountainArea)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(MIKE_PED_ID(), 250)
				ENDIF
			ENDIF
			m_bCheckFailureKillVolume = FALSE
			SAFE_REMOVE_BLIP(blip_Objective)
			REMOVE_NAVMESH_BLOCKING_OBJECT(iMichaelNavBlock)
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DAVE_AT_FOUNTAIN - Stage complete, going to DAVE_AT_FOUNTAIN_IDLE")
			eDaveFountainState = DAVE_AT_FOUNTAIN_IDLE
			Mission_Set_Stage(STAGE_DAVE_ESCAPES)
		BREAK
		
		CASE DAVE_AT_FOUNTAIN_IDLE
		BREAK
	ENDSWITCH
ENDPROC

