//|=======================================================================================|
//|                 Author:  Lukasz Bogaj / Bobby Wright 	 Date: 29/06/2011             |
//|=======================================================================================|
//|                                  	  MICHAEL1.sc	                                  |
//|                            		   BURY THE HATCHET	                          	  	  |
//|                             									                      |
//|=======================================================================================|

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//|==================================== INCLUDE FILES ====================================|


USING "rage_builtins.sch"
USING "globals.sch"

//Commands headers
USING "commands_ped.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_fire.sch"
USING "commands_path.sch"
USING "commands_audio.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "commands_cutscene.sch"
USING "commands_itemsets.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "commands_recording.sch"

//Script headers
USING "script_misc.sch"
USING "script_ped.sch"
USING "script_maths.sch"
USING "script_blips.sch"
USING "script_player.sch"
USING "script_buttons.sch"
USING "script_drawing.sch"

//Public headers
USING "flow_public_core_override.sch"
USING "flow_public_game.sch"
USING "replay_public.sch"
USING "locates_public.sch"
USING "emergency_call.sch"
USING "taxi_functions.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"
USING "carsteal_public.sch"
USING "dialogue_public.sch"
USING "cellphone_public.sch"
USING "player_ped_public.sch"
USING "vehicle_gen_public.sch"
USING "achievement_public.sch"
USING "mission_stat_public.sch"
USING "cheat_controller_public.sch"
USING "CompletionPercentage_public.sch"

//Debug headers
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
	USING "scripted_cam_editor_public.sch"
#ENDIF

//|===================================== ENUMS & STRUCTS =================================|

/// PURPOSE: Specifies stages of the mission
ENUM MISSION_STAGES
	MISSION_STAGE_CUTSCENE_INTRO			= 0,		//Intro cutscene
	MISSION_STAGE_GET_TO_AIRPORT			= 1,		//Michael and Trevor race to their specific airports
	MISSION_STAGE_FLY_TO_NORTH_YANKTON		= 2,		//Trevor flies to North Yankton from countryside airport, used on retry or skip
	MISSION_STAGE_CUTSCENE_AIRPORT			= 3,		//Cutscene showing Michael at airport terminal
	MISSION_STAGE_GET_TO_GRAVEYARD			= 4,		//Michael drives to the graveyard
	MISSION_STAGE_GET_TO_GRAVE				= 5,		//Michael walks to the grave
	MISSION_STAGE_CUTSCENE_GRAVEYARD		= 6,		//Cutscene at the graveyard
	MISSION_STAGE_GRAVEYARD_SHOOTOUT		= 7,		//Shootout at the graveyard
	MISSION_STAGE_CUTSCENE_KIDNAPPED		= 8,		//Cutscene showing Michael getting kidnapped by the triads
	MISSION_STAGE_FLY_HOME					= 9,		//Trevor flies back home
	MISSION_STAGE_END						= 10,		//End of mission
	MISSION_STAGE_PASSED,								//Mission Passed
	MISSION_STAGE_FAILED								//Mission Failed
ENDENUM

/// PURPOSE: Specifies mission fail reasons.
ENUM MISSION_FAILS
	MISSION_FAIL_EMPTY = 0,								//Generic fail
	MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE, 				//Michael's car undrivable
	MISSION_FAIL_MICHAELS_CAR_DEAD, 					//Michael's car destroyed
	MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND,				//Michael's car left behind
	MISSION_FAIL_GRAVEYARD_NOT_REACHED,					//Michael did not reach the graveyard, drove off route to graveyard
	MISSION_FAIL_GRAVE_NOT_REACHED,						//Michael did not reach the grave
	MISSION_FAIL_GRAVEYARD_ABANDONED,					//Michael left the graveyard area during the shootout
	MISSION_FAIL_TREVORS_CAR_UNDRIVABLE, 				//Trevor's car undrivable
	MISSION_FAIL_TREVORS_CAR_DEAD, 						//Trevor's car destroyed
	MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND,				//Trevor's car left behind
	MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE,				//Trevor's plane undrivable
	MISSION_FAIL_TREVORS_PLANE_DEAD,					//Trevor's plane destroyed
	MISSION_FAIL_MICHAEL_DEAD,							//Michael dead
	MISSION_FAIL_TREVOR_DEAD,							//Trevor dead
	MISSION_FAIL_FORCE_FAIL,							//Debug forced fail
	
	MAX_MISSION_FAILS
ENDENUM

/// PURPOSE: Specifies mission checkpoints.
ENUM MID_MISSION_STAGES
	MID_MISSION_STAGE_GET_TO_AIRPORT		= 0,		//Checkpoint at MISSION_STAGE_GET_TO_AIRPORT
	MID_MISSION_STAGE_FLY_TO_NORTH_YANKTON	= 1,		//Checkpoint at MISSION_STAGE_FLY_TO_NORTH_YANKTON
	MID_MISSION_STAGE_GET_TO_GRAVEYARD		= 2,		//Checkpoint at MISSION_STAGE_GET_TO_GRAVEYARD
	MID_MISSION_STAGE_GET_TO_GRAVE			= 3,		//Checkpoint at MISSION_STAGE_GET_TO_GRAVE
	MID_MISSION_STAGE_GRAVEYARD_SHOOTOUT	= 4,		//Checkpoint at MISSION_STAGE_GRAVEYARD_SHOOTOUT
	MID_MISSION_STAGE_FLY_HOME				= 5			//Checkpoint at MISSION_STAGE_FLY_HOME
ENDENUM

HASH_ENUM GRAVEYARD_GATES
	GRAVEYARD_GATE_WEST_L,					//0
	GRAVEYARD_GATE_WEST_R,					//1
	GRAVEYARD_GATE_EAST_L,					//2
	GRAVEYARD_GATE_EAST_R,					//3
	GRAVEYARD_GATE_SOUTH_L,					//4
	GRAVEYARD_GATE_SOUTH_R					//5
ENDENUM

ENUM ENEMY_STATES
	ENEMY_STATE_IDLE						= 0,
	ENEMY_STATE_SCRIPTED_ENTRY,
	ENEMY_STATE_PATROLLING,
	ENEMY_STATE_INVESTIGATING,
	ENEMY_STATE_MOVING_TO_LOCATION,
	ENEMY_STATE_STANDING_AT_LOCATION,
	ENEMY_STATE_PERFORMING_ENEMY_ACTION,
	ENEMY_STATE_WAITING_FOR_TARGET,
	ENEMY_STATE_GO_TO_COORD,
	ENEMY_STATE_FOLLOW_WAYPOINT_RECORDING,
	ENEMY_STATE_SNIPING,
	ENEMY_STATE_COMBAT
ENDENUM

ENUM DETECTED_LOCATIONS
	DL_NOT_DETECTED							= 0,
	DL_START_AREA							= 1,
	DL_MIDDLE_AREA_01						= 2,
	DL_MIDDLE_AREA_02						= 3,
	DL_CHURCH_AREA							= 4,
	DL_PARKING_LOT							= 5
ENDENUM

ENUM PED_CLEANUP_REASONS
	PED_CLEANUP_NO_CLEANUP					= 0,
	PED_CLEANUP_KILLED_BY_PLAYER_AS_MICHAEL	= 1,
	PED_CLEANUP_KILLED_BY_VEHICLE			= 2
ENDENUM

/// PURPOSE: Specifies details of mission peds.
STRUCT PED_STRUCT
	PED_INDEX				PedIndex					//Ped Index
	BLIP_INDEX				BlipIndex					//Ped's Blip Index
	VECTOR					vPosition					//Ped's start location
	FLOAT					fHeading					//Ped's start heading
	MODEL_NAMES				ModelName					//Ped's model
	VECTOR					vDestination				//Ped's current destination vector
	INT						iTimer						//Ped's timer
	INT						iConversationTimer
	INT						iConversationProgress	
ENDSTRUCT

/// PURPOSE: Specifies details of mission enemies.
STRUCT ENEMY_STRUCT
	PED_INDEX				PedIndex					//Enemy Ped Index
	BLIP_INDEX				BlipIndex					//Enemy Blip Index
	AI_BLIP_STRUCT			EnemyBlipData				//Enemy Blip Data
	ITEMSET_INDEX			ItemsetIndex				//Enemy Itemset Index, used for preferred cover points
	BOOL					bCreated					//Has this enemy been created
	BOOL					bHasTask					
	BOOL					bFallingBack
	BOOL					bSearching
	INT						iProgress					//Enemy progress counter
	INT						iTimer						//Enemy timer
	INT						iConversationTimer
	ENEMY_STATES			eState
	PED_CLEANUP_REASONS		eCleanupReason
	INT						iPatrolNode
	BOOL					bReversing
	VECTOR					vAimPosition
	INT						iWaypointProgress
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31		sDebugName
	#ENDIF
ENDSTRUCT

/// PURPOSE: Specifies details of mission vehicles used by player or other peds.
STRUCT VEH_STRUCT
	VEHICLE_INDEX			VehicleIndex				//Vehicle index
	BLIP_INDEX				BlipIndex					//Vehicle blip
	VECTOR					vPosition					//Vehicle postion
	FLOAT					fHeading					//Vehicle heading
	MODEL_NAMES				ModelName					//Vehicle model
	INT 					iRecordingNumber			//Vehicle recording number
	INT						iProgress					//Vehicle progress, used for set pieces
	INT						iTimer						//Vehicle timer
	FLOAT					fPlaybackSpeed				//Vehicle recording playback speed
	BOOL					bDriverDead					//Vehicle Driver dead
	BOOL					bAutoDriveActive			//Is vehicle currently using auto drive (used after hot swap)
	BOOL					bSmoking
ENDSTRUCT

/// PURPOSE: Specifies details of mission specific objects.
STRUCT OBJECT_STRUCT
	OBJECT_INDEX			ObjectIndex					//Object index
	MODEL_NAMES				ModelName					//Object model
	VECTOR					vPosition					//Object postion
	VECTOR					vRotation					//Object rotation
ENDSTRUCT

/// PURPOSE: Specifies details of mission trains.
STRUCT TRAIN_STRUCT
	VEHICLE_INDEX			VehicleIndex
	PED_INDEX				PedIndex
	INT						iConfiguration
	VECTOR					vPosition
	MODEL_NAMES				ModelName1
	MODEL_NAMES				ModelName2
	MODEL_NAMES				ModelName3
	MODEL_NAMES				ModelName4
	INT						iProgress
ENDSTRUCT

/// PURPOSE: Specifies details of locates used in mission.
STRUCT LOCATE_STRUCT
	VECTOR 					vPosition					//Locate position
	VECTOR 					vSize						//Locate size
ENDSTRUCT

/// PURPOSE: Specifies details of script created coverpoints to use for peds in combat.
STRUCT COVERPOINT_STRUCT
	COVERPOINT_INDEX		CoverpointIndex				//CoverpointIndex	
	VECTOR					vPosition					//Coverpoint position
	FLOAT					fHeading					//Coverpoint heading
	COVERPOINT_USAGE		CoverpointUsage				//Coverpoint usage
	COVERPOINT_HEIGHT		CoverpointHeight			//Coverpoint height
	COVERPOINT_ARC			CoverpointArc				//Coverpoint arc
	FLOAT					fDistance					//Coverpoint distance from the start of the street
	BOOL					bActive						//Indicates if this coverpoint should be considered when searching for nearest coverpoint
	BOOL					bStationary					//Indicates if this coverpoint is meant for stationary combat or cover combat
	BOOL					bTaken						//Indicates if this coverpoint is already being moved to by another ped
	PED_INDEX				TakenPedIndex				//Points at ped who is currently moving to this coverpoint
ENDSTRUCT

/// PURPOSE: Specifies details of spheres used for extra bullets to frag tombstones in the graveyard.
STRUCT TOMBSTONE_STRUCT
	VECTOR 					vPosition					//Tombstone position
	VECTOR					vShootSphereCenter			//Center of the sphere used for extra bullets
	FLOAT 					fShootSphereRadius			//Radius of the sphere used for extra bullets
ENDSTRUCT

/// PURPOSE: Specifies details of gate props used to block player's exit from the graveyard area.
STRUCT GATE_STRUCT
	GRAVEYARD_GATES			eHashEnum					//Door hash enum
	VECTOR					vPosition					//Door position
	MODEL_NAMES				ModelName					//Door model
	BOOL					bClosed						//Specifies if door is closed (open ratio i 0.0)
ENDSTRUCT

/// PURPOSE: Specifies details of nodes used to check if player is not driving off route when driving to Ludendorff Cemetery
STRUCT ROADNODE_STRUCT
	VECTOR					vPosition
	BOOL					bCurrent
	BOOL					bVisited
	BOOL					bInVehicleOnly
	FLOAT					fAllowedDistance
	FLOAT					fFailDistance
ENDSTRUCT

//|======================================= CONSTANTS =====================================|

CONST_INT 					MAX_MODELS 						6
CONST_INT 					MAX_CAR_RECORDINGS				6
CONST_INT					MAX_GRAVEYARD_GATES				6

CONST_INT					CHINESEGOON1SPEAKERID			3
CONST_INT					CHINESEGOON2SPEAKERID			4
CONST_INT					CHINESEGOON3SPEAKERID			5
CONST_INT					CHINESEGOON4SPEAKERID			6
CONST_INT					CHINESEGOON5SPEAKERID			7

STRING						CHINESEGOON1VOICENAME			= "MCH1CHIN1"
STRING						CHINESEGOON2VOICENAME			= "MCH1CHIN2"
STRING						CHINESEGOON3VOICENAME			= "MCH1CHIN3"
STRING						CHINESEGOON4VOICENAME			= "MCH1CHIN4"
STRING						CHINESEGOON5VOICENAME			= "MCH1CHIN5"
STRING						NUMBERPLATES					= "LB080984"

CONST_INT					ABANDON_CAR_WARNING_RANGE		25	//30
CONST_INT					ABANDON_CAR_FAIL_RANGE			90

TWEAK_FLOAT					TRIGGER_CALL_RANGE				6200.0
TWEAK_FLOAT					PEDMOVE_SLOWRUN					1.65
TWEAK_FLOAT					PEDMOVE_SLOWWALK				0.70

TWEAK_FLOAT					fFirstTrainSpeed				16.0

TWEAK_INT					iCameraInterpolationTimer		2750
TWEAK_FLOAT					fFirstRecordingEndTime			6300.0
TWEAK_FLOAT					fSecondRecordingStartTime		1500.0
TWEAK_FLOAT					fSecondRecordingEndTime			8200.0
TWEAK_FLOAT					fPlaneRecordingSkipTime			500.0 //9000.0

TWEAK_INT 					iInterpTime			  			3000
TWEAK_INT 					iCutsceneTime		  			8000

STRING						sVehicleRecordingsFile			= "michaelone"

MODEL_NAMES					mnEnemy							= G_M_M_CHICOLD_01
MODEL_NAMES					mnEnemyVan						= BURRITO5

VECTOR						vInteriorPosition				= << -802.46, 173.03, 71.84 >>		//room: V_Michael_G_Lounge
VECTOR						vGravePosition					= << 3259.89, -4573.24, 117.09 >>
VECTOR						vShootoutStartPosition			= << 3264.36, -4569.73, 117.07 >>
VECTOR						vFlightBackDestination			= << 1655.24, 3237.49, 39.55 >>//<< 1705.61, 3250.92, 40.01 >> //runway //<< 1731.52, 3310.17, 40.22 >> 	//hangar //<< 250.0, 2000.0, 40.0 >> //map center
VECTOR						vZero							= << 0.0, 0.0, 0.0 >>
VECTOR						vGlobalFlightOffset				= << -800.0, 350.0, 0.0 >>			//<< 800.0 , 800.0, 20.0>>
VECTOR						vLocalPlaneOffset				= << 0.0, 0.0, 0.0 >>				//<< -2.0, 0.0, -1.0 >>
VECTOR						vFirstTrainPosition				= << 3854.3, -4896.99, 111.58 >>
VECTOR						vParticleFXPosition				= << 0.0, -0.250, 1.200 >>
VECTOR						vParticleFXRotation				= << 0.0, 0.0, 0.0 >>

VECTOR						vStreamTriggerPosition1			= << 5258.50, -5119.35, 83.92 >>	//<< 5398.48, -5122.27, 84.26 >>
VECTOR						vStreamTriggerPosition2			= << 3851.85, -5014.12, 104.46 >>	//<< 3957.56, -5042.42, 109.03 >>
VECTOR						vStreamTriggerPosition3			= << 3222.89, -4708.41, 111.88 >>	//<< 3217.52, -4769.19, 110.82 >>

//used for vehicle tyre popping for burrito van
VECTOR 						vFrontLeftOffset				= <<-1.0500, 1.8000, -0.4000>>
VECTOR 						vFrontRightOffset				= <<1.0500, 1.8000, -0.4000>>
VECTOR 						vRearLeftOffset					= <<-1.0500, -1.9500, -0.4000>>
VECTOR 						vRearRightOffset				= <<1.0500, -1.9500, -0.4000>>

VECTOR 						vFrontLeftDamageOffset			= <<-1.0000, 2.3500, 0.0500>>
VECTOR 						vFrontRightDamageOffset			= <<1.0000, 2.3500, 0.0500>>
VECTOR 						vRearLeftDamageOffset			= <<-1.1000, -2.3500, -0.0500>>
VECTOR 						vRearRightDamageOffset			= <<1.1000, -2.3500, -0.0500>>
				
VECTOR						vGraveyardDefAreaPosisiton1 	= <<3264.078857,-4670.798340,111.055817>>
VECTOR						vGraveyardDefAreaPosistion2		= <<3266.365723,-4562.407227,121.417885>>
CONST_FLOAT					fGraveyardDefAreaWidth			48.0

TWEAK_FLOAT					fFocusPushHintFOV				30.0
TWEAK_FLOAT					fFocusPushSideOffset			0.010
TWEAK_FLOAT					fFocusPushVerticalOffset		-0.025
TWEAK_FLOAT					fFocusPushFollowDistanceScalar	0.400
TWEAK_FLOAT					fFocusPushBaseOrbitPitchOffset	-4.0

TWEAK_FLOAT					fParticleFXTriggerPhase			0.445

TWEAK_INT					iFirstPersonFXTime				2500

//|======================================= VARIABLES =====================================|

MISSION_STAGES 				eMissionStage 				= MISSION_STAGE_CUTSCENE_INTRO
MISSION_FAILS				eMissionFail 				= MISSION_FAIL_EMPTY
DETECTED_LOCATIONS			eDetectedLocation			= DL_NOT_DETECTED
WEAPON_TYPE					eMichaelsFailWeapon			= WEAPONTYPE_INVALID

//mission peds
PED_STRUCT					psBrad
PED_STRUCT					psTrevor
PED_STRUCT					psMichael

//mission vehicles
VEH_STRUCT					vsMichaelsCar
VEH_STRUCT					vsTrevorsCar
VEH_STRUCT					vsTrevorsPlane
VEH_STRUCT					vsCutsceneCar
VEHICLE_SETUP_STRUCT		TrevorsVehicleData
TRAIN_STRUCT				tsTrain

OBJECT_STRUCT				osGolfBall
OBJECT_STRUCT				osPropShovel
OBJECT_STRUCT				osPropPickaxe
OBJECT_STRUCT				osPropCoffin

//structs and variables for hot swap
SELECTOR_PED_STRUCT 		sSelectorPeds

//locates header data
LOCATES_HEADER_DATA 		sLocatesData

//conversation struct
structPedsForConversation 	sMichael1Conversation

//mission locates
LOCATE_STRUCT 				sLocateFlight

REL_GROUP_HASH 				rgEnemies

//progress counters
INT 						iStageSetupProgress
INT 						iMichaelStageProgress
INT 						iTrevorStageProgress
INT 						iMissionStageProgress

//conversation flags and timers
INT							iSnowStuckTimer
INT 						iEnemyConversationTimer
INT							iMichaelRandomConversationDelay
INT							iEnemyRandomConversationDelay
BOOL						bShootingConversationFlag
BOOL						bPlayVanExitConversation
BOOL						bPlayVanOpenConversation
BOOL						bStartSearching
BOOL						bEnemiesBlockedByTrain

BOOL 						bSkipFlag
BOOL 						bReplayFlag
BOOL 						bLoadingFlag
BOOL						bCutsceneSkipped
BOOL						bStageReplayInProgress

BOOL						bWeaponsHelpDisplayed
BOOL						bMichaelsWeaponsStored

BOOL 						bSnowOn
BOOL						bProloguePedComponentsActive
BOOL 						bPrologueMapActive
BOOL						bPrologueGraveIPLSwapped
BOOL						bTrevorFlightActive

INT							iMichaelsObjectiveTimer

BOOL						bTaxiLocationSet
BOOL						bShouldDisplayMichaelsObjective
BOOL 						bMichaelTrevorPhoneCallTriggered
BOOL						bCoverpointsCreated
BOOL						bVehiclePlaybackInterrupted
BOOL						bGameplayCameraSet
BOOL						bCutsceneTriggeredByTrevor
BOOL						bCutsceneTriggeredByMichael
BOOL						bSafeToStartMichaelsCall
BOOL						bCamViewModeContextChanged
BOOL						bFirstPersonFXTriggered

BOOL						bPlayerDroveOffRoute
BOOL						bPlayerUsedAlternativePath

BOOL						bMusicEventTrevorPlaneTriggered
BOOL						bMusicEventArrivedChurchTriggered
BOOL						bMusicEventShootoutStartTriggered
BOOL						bMusicEventFirstTwoDeadTriggered
BOOL						bMusicEventFirstVanTriggered
BOOL						bMusicEventAlertedTriggered
BOOL						bMusicEventTrainTriggered
BOOL						bMusicEventKidnappedTriggered
BOOL						bMusicEventReadyToFlyTriggered

//enemy waves
BOOL						bPlayerDetected
BOOL						bDetectedConversationPlaying
BOOL						bFirstEnemyWaveSpawned
BOOL						bSecondEnemyWaveSpawned
BOOL						bThirdEnemyWaveSpawned
BOOL						bFourthEnemyWaveSpawned
BOOL						bFifthEnemyWaveSpawned
BOOL						bFifthEnemyWaveIgnored

BOOL						bFirstEnemyReinforcementWaveSpawned
BOOL						bSecondEnemyReinforcementWaveSpawned
//BOOL						bThirdEnemyReinforcementWaveSpawned

INT							iSecondEnemyReinforcementDelayTimer
INT							iThirdEnemyReinforcementDelayTimer

BOOL						bOpenGateTriggered
BOOL						bSmoothCloseGateTriggered
BOOL						bTrevorsCarSetAsVehicleGenVehicle

INT							iLoadSceneTimer

INT							iLeadInTimer
BOOL						bLeadInTiggered
BOOL						bPlayerPedScriptTaskInterrupted

INT							iGateCloseProgress

INT							iRainOnPlaneSoundID
INT							iTrainBellsSoundID
INT							iChurchBellsSoundID
INT							iChurchBellsProgress

//asset request counters
INT 						iModelsRequested
INT 						iRecordingsRequested

INT							iFirstVanTriggerTimer
INT							iPlayerMichaelKills

INT							iWeatherTypeTimer
BOOL						bWeatherTypeOvercastSet
BOOL						bWeatherTypeRainSet
BOOL						bWeatherTypeThunderSet

BOOL						bRenderBlips
INT							bRenderBlipsTimer

FLOAT						fDistanceToDestination

TEXT_LABEL					sLabelGTLC							= "MCH1_GTLC"
TEXT_LABEL 					sLabelTOBJ1 						= "MCH1_GT_T1"
TEXT_LABEL 					sLabelTGETBCK1 						= "CMN_TGETBCK"
TEXT_LABEL 					sLabelTOBJ2 						= "MCH1_GT_T3"
TEXT_LABEL 					sLabelTGETIN2 						= "CMN_GENGETINPL"
TEXT_LABEL 					sLabelTGETBCK2 						= "CMN_GENGETBCKPL"

//cutscene camera
CAMERA_INDEX 				ScriptedCamera

//tombstone fragging
PED_INDEX					TombstoneShooterPed				//ped index of the ped to shoot extra bullets
INT							iEnemyTombstoneShootTimer		//timer used to check if extra bullets should be fired

OBJECT_INDEX				WeaponMichaelObjectIndex
OBJECT_INDEX				WeaponEnemyObjectIndex

INT 						iHealthPickupPlacementFlags
INT 						iPistolPickupPlacementFlags
INT							iRiflePickupPlacementFlags
PICKUP_INDEX 				HealthPickup
PICKUP_INDEX 				PistolPickup
PICKUP_INDEX				RiflePickup

INT							iPlaneSceneID		= -1
INT							iBradCadaverScene 	= -1
INT							iBradCadaverProgress

INT							iTrevorDriveOutProgress
//BOOL						bShouldUseDefaultTrevorsCar

INT							iCutsceneTriggerTimer
BOOL						bCutsceneTimeTrigger
BOOL						bCutsceneProximityTrigger

BOOL						bPTFXStarted

BOOL						bTextMessageSent
BOOL						bDamageDecalApplied, bDirtDecalApplied
INT							iSendTextMessageTimer

BOOL						bStartFlashBackStream
INT							iFlashbackStreamProgress
INT							iFlashbackSubtitleProgress

BOOL						bReplayStopEventTriggered, bReplayStartEventTriggered

INT							iCarTrackedPoint 	= -1

INT							iNMBlockingObject01	= -1		//blocks the right side train tracks nav mesh in graveyard
INT							iNMBlockingObject02	= -1		//blocks the right gate nav mesh in graveyard
INT							iNMBlockingObject03	= -1		//blocks the back gate nav mesh in graveyard
INT							iNMBlockingObject04	= -1		//blocks the left gate nav mesh in graveyard
INT							iNMBlockingObject05	= -1		//blocks the left side train tracks nav mesh in graveyard
INT							iNMBlockingObject06	= -1		//blocks the road to the graveyard
INT							iNMBlockingObject07 = -1		//blocks the uncovered grave, hopefully should stop peds falling into the grave

INT							iBlockVehicleFirstPersonCameraTimer = 0		//used to block first person vehicle camera for limited time, see B*2031566
BOOL						bBlockVehicleFirstPersonCamera		= FALSE

//|========================================= ARRAYS ======================================|

//text printing
INT 						iTriggeredTextHashes[30]
INT 						iNumTextHashesStored

BOOL						FailFlags[MAX_MISSION_FAILS]

//asset arrays
MODEL_NAMES 				MissionModels[MAX_MODELS]
INT 						MissionRecordings[MAX_CAR_RECORDINGS]

ROADNODE_STRUCT				sRoadNodes[59]

ENEMY_STRUCT				esVanEnemies[4]
ENEMY_STRUCT				esWallEnemies[2]
ENEMY_STRUCT				esMiddleEnemies[3]
ENEMY_STRUCT				esChurchEnemies[2]
ENEMY_STRUCT				esExitEnemies[5]
ENEMY_STRUCT				esSideVanEnemies[2]
ENEMY_STRUCT				esSideVanBackupEnemies[2]
ENEMY_STRUCT				esRunners[2]
ENEMY_STRUCT				esVan4Enemies[4]
ENEMY_STRUCT				esSideEnemies[3]

ENEMY_STRUCT				esFirstReinforcementEnemies[2]
ENEMY_STRUCT				esSecondReinforcementEnemies[2]
ENEMY_STRUCT				esThirdReinforcementEnemies[2]

VEH_STRUCT					vsEnemyVans[5]

//scripted coverpints and positions for shootout
COVERPOINT_STRUCT 			csCoverpoints[36]
TOMBSTONE_STRUCT			FraggableTombstones[3]

GATE_STRUCT					Gates[MAX_GRAVEYARD_GATES]

//|========================= MISCELLANEOUS PROCEDURES & FUNCTIONS ========================|

/// PURPOSE:
///    Resets all mission flags and progress counters to their default values.
PROC RESET_MISSION_FLAGS()

	//reset setup progress to 1, so that iSetupProgress = 0 only runs once, on initial mission load.
	iStageSetupProgress 				= 1
	
	iMichaelStageProgress 				= 0
	iTrevorStageProgress 				= 0
	iMissionStageProgress				= 0
	
	psMichael.iTimer					= 0
	psTrevor.iTimer						= 0
	
	psMichael.iConversationTimer		= 0
	psTrevor.iConversationTimer			= 0
	
	psMichael.iConversationProgress		= 0
	psTrevor.iConversationProgress		= 0
	
	tsTrain.iProgress					= 0
	
	iSnowStuckTimer						= 0
	iEnemyConversationTimer				= 0
	iMichaelRandomConversationDelay		= 0
	iEnemyRandomConversationDelay		= 0
	bShootingConversationFlag			= FALSE
	bPlayVanOpenConversation 			= FALSE
	bPlayVanExitConversation			= FALSE
	bStartSearching						= FALSE
	bEnemiesBlockedByTrain				= FALSE
	
	bPrologueGraveIPLSwapped			= FALSE
	bPlayerDetected						= FALSE
	bDetectedConversationPlaying		= FALSE
	bFirstEnemyWaveSpawned				= FALSE
	bSecondEnemyWaveSpawned				= FALSE
	bThirdEnemyWaveSpawned				= FALSE
	bFourthEnemyWaveSpawned				= FALSE
	bFifthEnemyWaveSpawned				= FALSE
	bFifthEnemyWaveIgnored				= FALSE
	
	bFirstEnemyReinforcementWaveSpawned	= FALSE
	bSecondEnemyReinforcementWaveSpawned= FALSE
	//bThirdEnemyReinforcementWaveSpawned	= FALSE
	
	iSecondEnemyReinforcementDelayTimer	= 0
	iThirdEnemyReinforcementDelayTimer	= 0
	
	iEnemyTombstoneShootTimer			= 0
	
	iMichaelsObjectiveTimer						= 0
	
	bTaxiLocationSet					= FALSE
	bShouldDisplayMichaelsObjective		= FALSE
	bMichaelTrevorPhoneCallTriggered 	= FALSE
	
	bVehiclePlaybackInterrupted			= FALSE
	
	bCutsceneSkipped					= FALSE
	bStageReplayInProgress				= FALSE
	
	bOpenGateTriggered					= FALSE
	bSmoothcloseGateTriggered			= FALSE
	
	iGateCloseProgress					= 0
	
	iChurchBellsProgress				= 0
	
	iLoadSceneTimer						= 0
	
	bSafeToStartMichaelsCall		= FALSE
	
	bPlayerDroveOffRoute				= FALSE
	bPlayerUsedAlternativePath			= FALSE

	sLabelGTLC							= "MCH1_GTLC"
	sLabelTOBJ1 						= "MCH1_GT_T1"
	sLabelTGETBCK1 						= "CMN_TGETBCK"
	sLabelTOBJ2 						= "MCH1_GT_T3"
	sLabelTGETIN2 						= "CMN_GENGETINPL"
	sLabelTGETBCK2 						= "CMN_GENGETBCKPL"
	
	eDetectedLocation					= DL_NOT_DETECTED
	
	bMusicEventTrevorPlaneTriggered		= FALSE
	bMusicEventShootoutStartTriggered	= FALSE
	bMusicEventArrivedChurchTriggered	= FALSE
	bMusicEventFirstTwoDeadTriggered	= FALSE
	bMusicEventFirstVanTriggered		= FALSE
	bMusicEventAlertedTriggered			= FALSE
	bMusicEventTrainTriggered			= FALSE
	bMusicEventKidnappedTriggered		= FALSE
	bMusicEventReadyToFlyTriggered		= FALSE
	
	iFirstVanTriggerTimer				= 0
	
	iWeatherTypeTimer					= 0
	bWeatherTypeOvercastSet				= FALSE
	bWeatherTypeRainSet					= FALSE
	bWeatherTypeThunderSet				= FALSE
	
	iLeadInTimer						= 0
	bLeadInTiggered						= FALSE
	bPlayerPedScriptTaskInterrupted		= FALSE
	
	bRenderBlips						= FALSE
	bRenderBlipsTimer					= 0
	
	bTrevorFlightActive					= FALSE
	
	fDistanceToDestination				= 0.0
	
	iTrevorDriveOutProgress				= 0
	//bShouldUseDefaultTrevorsCar		= FALSE
	
	iBradCadaverProgress				= 0
	
	iSendTextMessageTimer				= 0
	
	iCutsceneTriggerTimer				= 0
	bCutsceneTimeTrigger				= FALSE
	bCutsceneProximityTrigger			= FALSE
	
	bPTFXStarted						= FALSE

	bStartFlashBackStream				= FALSE
	iFlashbackStreamProgress			= 0
	iFlashbackSubtitleProgress			= 0
	
	bReplayStopEventTriggered			= FALSE
	bReplayStartEventTriggered			= FALSE
	
	bFirstPersonFXTriggered				= FALSE
	bCamViewModeContextChanged			= FALSE
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission flags and progress counters are reset.")
	#ENDIF

ENDPROC

/// PURPOSE:
///    Sets all fail flags to specified boolean value.
/// PARAMS:
///    bNewBool - Boolean value to set all flags to TRUE or FALSE.
PROC SET_FAIL_FLAGS(BOOL bNewBool)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(FailFlags) - 1 )

		FailFlags[i] = bNewBool

	ENDFOR

ENDPROC

FUNC SCENARIO_BLOCKING_INDEX CREATE_SCENARIO_BLOCKING_AREA(VECTOR vPosition, VECTOR vSize)
	RETURN ADD_SCENARIO_BLOCKING_AREA(<< vPosition.x - vSize.x, vPosition.y - vSize.y, vPosition.z - vSize.z >>,
									  << vPosition.x + vSize.x, vPosition.y + vSize.y, vPosition.z + vSize.z >>)
ENDFUNC

PROC REMOVE_PED_WEAPONS_LEAVING_ONE(PED_INDEX PedIndex, WEAPON_TYPE eWeaponToLeave, INT iMinBulletCount = 100)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)

			INT 		iWeaponSlot
			WEAPON_SLOT	eWeaponSlot

			FOR iWeaponSlot = 0 TO ENUM_TO_INT(NUM_WEAPONSLOTS)-1

				eWeaponSlot = GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot)
				
				IF ( eWeaponSlot != WEAPONSLOT_INVALID )
				
					WEAPON_TYPE eCurrentWeaponType
					
					eCurrentWeaponType = GET_PED_WEAPONTYPE_IN_SLOT(PedIndex, eWeaponSlot)

					IF 	( eCurrentWeaponType != eWeaponToLeave  )
					AND ( eCurrentWeaponType != WEAPONTYPE_UNARMED  )
					AND ( eCurrentWeaponType != WEAPONTYPE_INVALID  )
						
						SET_PED_AMMO(PedIndex, eCurrentWeaponType, 0)
						REMOVE_WEAPON_FROM_PED(PedIndex, eCurrentWeaponType)

						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing ped weapon ", GET_WEAPON_NAME(eCurrentWeaponType), ".")
						#ENDIF
					
					ENDIF
				
				ENDIF

			ENDFOR
			
			//remove dlc weapons
			
			INT 				iDLCWeaponIndex
			INT 				iDLCWeapons = GET_NUM_DLC_WEAPONS()
			scrShopWeaponData 	weaponData
			
			REPEAT iDLCWeapons iDLCWeaponIndex
				IF GET_DLC_WEAPON_DATA(iDLCWeaponIndex, weaponData)
				
					WEAPON_TYPE eCurrentWeaponType = INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash)
					
					IF 	( eCurrentWeaponType != eWeaponToLeave )
					AND	( eCurrentWeaponType != WEAPONTYPE_INVALID )
					AND ( eCurrentWeaponType != WEAPONTYPE_UNARMED )
							
						SET_PED_AMMO(PedIndex, eCurrentWeaponType, 0)
						REMOVE_WEAPON_FROM_PED(PedIndex, eCurrentWeaponType)

						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing ped DLC weapon ", GET_WEAPON_NAME(eCurrentWeaponType), ".")
						#ENDIF
						
					ENDIF
				ENDIF
			ENDREPEAT
			
			
			//check if player has a pistol
			
			IF HAS_PED_GOT_WEAPON(PedIndex, eWeaponToLeave)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped has weapon ", GET_WEAPON_NAME(eWeaponToLeave), ".")
				#ENDIF
				
				INT iAmmo = GET_AMMO_IN_PED_WEAPON(PedIndex, eWeaponToLeave)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped has got ", iAmmo, " bullets in weapon ", GET_WEAPON_NAME(eWeaponToLeave), ".")
				#ENDIF
				
				IF ( iAmmo < iMinBulletCount )
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped has got less than ", iMinBulletCount, " bullets in weapon ", GET_WEAPON_NAME(eWeaponToLeave), ".")
					#ENDIF
					
					INT iBulletCountDifference
					
					iBulletCountDifference = iMinBulletCount - iAmmo
				
					ADD_AMMO_TO_PED(PedIndex, eWeaponToLeave, iBulletCountDifference)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding ", iBulletCountDifference, " bullets to ped weapon ", GET_WEAPON_NAME(eWeaponToLeave), ".")
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped has now got ", GET_AMMO_IN_PED_WEAPON(PedIndex, eWeaponToLeave), " bullets in weapon ", GET_WEAPON_NAME(eWeaponToLeave), ".")
					#ENDIF
					
				ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped does not have weapon ", GET_WEAPON_NAME(eWeaponToLeave), ".")
				#ENDIF
				
				GIVE_WEAPON_TO_PED(PedIndex, eWeaponToLeave, iMinBulletCount, FALSE, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Giving ped weapon ", GET_WEAPON_NAME(eWeaponToLeave), " with ", iMinBulletCount, " bullets.")
				#ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

PROC SET_CUTSCENE_TRIGGER_FLAG_FOR_PLAYER_PED_ENUM(enumCharacterList ePlayerPedCharacter, BOOL &bMichaelTriggerFlag, BOOL &bTrevorTriggerFlag)

	IF ( ePlayerPedCharacter = CHAR_MICHAEL )
		bMichaelTriggerFlag = TRUE
		bTrevorTriggerFlag 	= FALSE
	ELIF ( ePlayerPedCharacter = CHAR_TREVOR )
		bMichaelTriggerFlag = FALSE
		bTrevorTriggerFlag 	= TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting graveyard cutscene trigger flags bMichaelTriggerFlag to ", bMichaelTriggerFlag,
				" and bTrevorTriggerFlag  to ", bTrevorTriggerFlag, " due to debug skips, shit skip or mission replay.")
	#ENDIF

ENDPROC

/// PURPOSE:
///    Returns the next mission stage after the specified mission stage.
/// PARAMS:
///    eStage - Mission stage to get the next mission stage.
/// RETURNS:
///    Mission stage that is next after the specified mission stage.
FUNC MISSION_STAGES GET_NEXT_MISSION_STAGE(MISSION_STAGES eStage)

	IF eStage = MISSION_STAGE_END
		RETURN MISSION_STAGE_END
	ENDIF
	
	IF eStage = MISSION_STAGE_GET_TO_AIRPORT
		RETURN MISSION_STAGE_CUTSCENE_AIRPORT
	ENDIF
	
	MISSION_STAGES eNextStage
	
	INT iNextStage = ENUM_TO_INT(eStage) + 1
	
	eNextStage = INT_TO_ENUM(MISSION_STAGES, iNextStage)
	
	RETURN eNextStage

ENDFUNC

/// PURPOSE:
///    Check is the specified time has passed using specified timer.
/// PARAMS:
///    iTimeAmount - Time to check.
///    iTimer - Timer to use.
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimeAmount, INT iTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Performs a screen fade in with a wait for a specified time.
/// PARAMS:
///    iTime - Duration of the fade in miliseconds.
PROC DO_SAFE_SCREEN_FADE_IN(INT iTime)

	IF NOT IS_SCREEN_FADED_IN()
	
		DO_SCREEN_FADE_IN(iTime)
		
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Performs a screen fade out with a wait for a specified time.
/// PARAMS:
///    iTime - Duration of the fade in miliseconds.
PROC DO_SAFE_SCREEN_FADE_OUT(INT iTime)

	IF NOT IS_SCREEN_FADED_OUT()
	
		DO_SCREEN_FADE_OUT(iTime)
		
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Stops an active mocap cutscene and waits until it has finished.
PROC DO_SAFE_STOP_CUTSCENE()

	IF IS_CUTSCENE_PLAYING()
		SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
		STOP_CUTSCENE(TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)	//needs to be called to make sure all scripted systems work correctly when cutscene is skipped		
	ENDIF

	WHILE NOT HAS_CUTSCENE_FINISHED()
		WAIT(0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for the cutscene to finish.")
		#ENDIF
	ENDWHILE
	
ENDPROC

/// PURPOSE:
///    Checks if the specified mocap cutscene has loaded.
///    Removes any other loaded mocap cutscene and requests the specified cutscene until it is loaded.
/// PARAMS:
///    sSceneName - Mocap cutscene name to check for being loaded.
///    CutsceneSection - Cutscene section specifying at what section to start the cutscene. Use CS_SECTION_1 to play from start.
/// RETURNS:
///    TRUE if the specified cutscene has loaded, FALSE if otherwise.   
FUNC BOOL HAS_REQUESTED_CUTSCENE_LOADED(STRING sSceneName, CUTSCENE_SECTION CutsceneSection = CS_SECTION_1)

	//check if the specified cutscene has loaded first
	IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(sSceneName)
	
		RETURN TRUE
	
	ELSE
		
		//if the expected reuqested cutscene has not loaded, but there already is a loaded cutscene
		//then remove it and keep requesting specified mocap cutscene
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
			REMOVE_CUTSCENE()
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": HAS_REQUESTED_CUTSCENE_LOADED() Removing cutscene from memory, because cutscene ", sSceneName, " was expected to be loaded.")
			#ENDIF
			
		ENDIF
	
		IF ( CutsceneSection = CS_SECTION_1) 
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE(sSceneName)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, ".")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene ", sSceneName, " is loading with playback list.")
			#ENDIF
			
			//if the cutscene is not loaded, keep requesting it
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(sSceneName, CutsceneSection)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting ", sSceneName, " cutscene with playback list.")
			#ENDIF
		
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC START_CUTSCENE_AT_COORDS_WITH_HEADING(VECTOR vPosition, FLOAT fHeading)

	SET_CUTSCENE_TRIGGER_AREA(vZero, 0.0, fHeading, 0.0)

	START_CUTSCENE_AT_COORDS(vPosition)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting cutscene at ", vPosition, ", ", fHeading, ".")
	#ENDIF

ENDPROC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " TRUE.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", strLabel, " TRUE.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_LABEL_AS_TRIGGERED(): ", iNumTextHashesStored, "/", COUNT_OF(iTriggeredTextHashes), ".")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

PROC PRINT_GOD_TEXT_ADVANCED(STRING sLabel, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		PRINT(sLabel, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Printing god text ", sLabel, ".")
		#ENDIF
	ENDIF
ENDPROC

PROC PRINT_FAKE_SUBTITLE(STRING sLabel, INT iDuration = DEFAULT_GOD_TEXT_TIME)

	FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
	IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
		CLEAR_PRINTS()
		PRINT(sLabel, iDuration, 1)
	ENDIF

ENDPROC

PROC DISPLAY_TEXT_ON_BLACK_SCREEN(STRING sText)
	
	FLOAT fFontHeight

	IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
		SET_TEXT_SCALE(0.675, 0.675)  						//150% larger font for korean, japanese and chinese
		fFontHeight = GET_RENDERED_CHARACTER_HEIGHT(0.675)
	ELSE  													//all other languages
		SET_TEXT_SCALE(0.45, 0.45)
		fFontHeight = GET_RENDERED_CHARACTER_HEIGHT(0.45)
	ENDIF

	SET_TEXT_CENTRE(TRUE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(sText)
	END_TEXT_COMMAND_DISPLAY_TEXT(0.5, 0.5 - (fFontHeight / 2))

	RESET_SCRIPT_GFX_ALIGN()
	HIDE_LOADING_ON_FADE_THIS_FRAME()
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()

ENDPROC

PROC SMOOTH_LOCK_GRAVEYARD_GATE(INT i, FLOAT fSpeed = 0.2, FLOAT fTolerance = 0.01)

	IF ( Gates[i].bClosed = FALSE )
	
        FLOAT fOpenRatio
	
		fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(Gates[i].eHashEnum))
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_ABOVE_COORDS(Gates[i].vPosition, GET_STRING_FROM_FLOAT(fOpenRatio), 1.0)
		#ENDIF
		
		IF fOpenRatio <= -fTolerance
        OR fOpenRatio >= fTolerance
		
        	IF fOpenRatio > 0.0
                  fOpenRatio -= fSpeed
            ELIF fOpenRatio < 0.0
                  fOpenRatio += fSpeed
            ENDIF
               
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(Gates[i].eHashEnum), fOpenRatio, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(Gates[i].eHashEnum), DOORSTATE_LOCKED, FALSE, TRUE)
            
			CLEAR_AREA_OF_OBJECTS(Gates[i].vPosition, 2.0)
						
		ELSE
		
			fOpenRatio = 0.0
                    
            DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(Gates[i].eHashEnum), fOpenRatio, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(Gates[i].eHashEnum), DOORSTATE_LOCKED, FALSE, TRUE)
			
			Gates[i].bClosed = TRUE
			
		ENDIF	
	ENDIF

ENDPROC

PROC REGISTER_GRAVEYARD_GATE(INT i)

	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(Gates[i].eHashEnum))
		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(Gates[i].eHashEnum), Gates[i].ModelName, Gates[i].vPosition, FALSE, FALSE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding door ", i, " with int ", ENUM_TO_INT(Gates[i].eHashEnum), " to system with model ",
					GET_MODEL_NAME_FOR_DEBUG(Gates[i].ModelName), " at coordinates ", Gates[i].vPosition, ".")
		#ENDIF
	ENDIF

ENDPROC

PROC UNREGISTER_GRAVEYARD_GATE(INT i)

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(Gates[i].eHashEnum))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(Gates[i].eHashEnum))
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing door ", i, " with int ", ENUM_TO_INT(Gates[i].eHashEnum), " from system with model ",
					GET_MODEL_NAME_FOR_DEBUG(Gates[i].ModelName), " at coordinates ", Gates[i].vPosition, ".")
		#ENDIF
	ENDIF

ENDPROC

PROC LOCK_GRAVEYARD_GATE(INT i)

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(Gates[i].eHashEnum))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(Gates[i].eHashEnum), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(Gates[i].eHashEnum), DOORSTATE_LOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Locking door ", i, " with int ", ENUM_TO_INT(Gates[i].eHashEnum), " with model ",
					GET_MODEL_NAME_FOR_DEBUG(Gates[i].ModelName), " at coordinates ", Gates[i].vPosition, ".")
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Door ", i, " with int ", ENUM_TO_INT(Gates[i].eHashEnum), " with model ",
					GET_MODEL_NAME_FOR_DEBUG(Gates[i].ModelName), " at coordinates ", Gates[i].vPosition, " is not registered with door system.")
		#ENDIF
	ENDIF

ENDPROC

PROC UNLOCK_GRAVEYARD_GATE(INT i)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(Gates[i].eHashEnum))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(Gates[i].eHashEnum), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(Gates[i].eHashEnum), DOORSTATE_UNLOCKED, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unlocking door ", i, " with int ", ENUM_TO_INT(Gates[i].eHashEnum), " with model ",
					GET_MODEL_NAME_FOR_DEBUG(Gates[i].ModelName), " at coordinates ", Gates[i].vPosition, ".")
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Door ", i, " with int ", ENUM_TO_INT(Gates[i].eHashEnum), " with model ",
					GET_MODEL_NAME_FOR_DEBUG(Gates[i].ModelName), " at coordinates ", Gates[i].vPosition, " is not registered with door system.")
		#ENDIF
	ENDIF
	
ENDPROC

PROC REGISTER_GRAVEYARD_GATES()
	
	INT i = 0
	
	FOR i = 0 TO ( MAX_GRAVEYARD_GATES - 1 )
		REGISTER_GRAVEYARD_GATE(i)
	ENDFOR
	
ENDPROC

PROC UNREGISTER_GRAVEYARD_GATES()

	INT i = 0
	
	FOR i = 0 TO ( MAX_GRAVEYARD_GATES - 1 )
		UNREGISTER_GRAVEYARD_GATE(i)
	ENDFOR

ENDPROC

PROC LOCK_GRAVEYARD_GATES()

	INT i = 0
	
	FOR i = 0 TO ( MAX_GRAVEYARD_GATES - 1 )
		LOCK_GRAVEYARD_GATE(i)
	ENDFOR

ENDPROC

PROC UNLOCK_GRAVEYARD_GATES()

	INT i = 0
	
	FOR i = 0 TO ( MAX_GRAVEYARD_GATES - 1 )
		UNLOCK_GRAVEYARD_GATE(i)
	ENDFOR

ENDPROC

PROC RUN_GRAVEYARD_GATES_CHECK(MISSION_STAGES eStage, BOOL bOpenTriggered, BOOL bCloseTriggered, INT &iGateProgress)

	SWITCH eStage
		CASE MISSION_STAGE_GET_TO_GRAVEYARD
		CASE MISSION_STAGE_GET_TO_GRAVE
		CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
		CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
				
//			IF NOT IS_CUTSCENE_PLAYING()
//			ENDIF
			
		BREAK

		CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
		
			SWITCH iGateProgress
			
				CASE 0
				
					LOCK_GRAVEYARD_GATE(0)
					LOCK_GRAVEYARD_GATE(1)
					
					iGateProgress++
					
				BREAK
			
				CASE 1
				
					IF ( bOpenTriggered = TRUE )
				
						UNLOCK_GRAVEYARD_GATE(0)
						UNLOCK_GRAVEYARD_GATE(1)

						Gates[0].bClosed = FALSE
						Gates[1].bClosed = FALSE

						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unlocking doors for smooth locking.")
						#ENDIF

						iGateProgress++
						
					ENDIF
				
				BREAK
				
				CASE 2
				
					IF ( bCloseTriggered = TRUE )
				
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Running smooth locking.")
						#ENDIF
				
						SMOOTH_LOCK_GRAVEYARD_GATE(0, 0.075, 0.05)
						SMOOTH_LOCK_GRAVEYARD_GATE(1, 0.075, 0.05)
					
						IF 	( Gates[0].bClosed = TRUE )
						AND ( Gates[1].bClosed = TRUE )
						
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Smooth locking is done.")
							#ENDIF
						
							iGateProgress++
						
						ENDIF
						
					ENDIF
				
				BREAK
				
				CASE 3
				
					LOCK_GRAVEYARD_GATE(0)
					LOCK_GRAVEYARD_GATE(1)
				
					iGateProgress++
				
				BREAK
			
			ENDSWITCH
		
		BREAK
		
	ENDSWITCH

ENDPROC

PROC ADD_NAVMESH_BLOCKING_OBJECTS()

	IF ( iNMBlockingObject01 = -1)
		iNMBlockingObject01 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3242.716, -4707.644, 112.0 >>, << 18.144, 14.964, 5.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject01.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject02 = -1)
		iNMBlockingObject02 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3294.783, -4613.386, 112.0 >>, << 4.200, 6.294, 5.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject02.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject03 = -1)
		iNMBlockingObject03 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3263.877, -4560.729, 118.000 >>, << 6.650, 6.000, 5.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject03.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject04 = -1)
		iNMBlockingObject04 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3238.994, -4592.786, 115.000 >>, << 6.650, 6.000, 5.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject04.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject05 = -1)
		iNMBlockingObject05 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3198.355, -4706.313, 112.000 >>, << 12.179, 14.115, 5.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject05.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject06 = -1)
		iNMBlockingObject06 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3214.000, -4765.436, 112.000 >>, << 64.0, 32.0, 8.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject06.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject07 = -1)
		iNMBlockingObject07 = ADD_NAVMESH_BLOCKING_OBJECT(<< 3259.800, -4573.900, 117.300 >>, << 2.5, 4.0, 2.0 >>, 0.0)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding nav mesh blocking object iNMBlockingObject07.")
		#ENDIF
	ENDIF

ENDPROC

PROC REMOVE_NAVMESH_BLOCKING_OBJECTS()

	IF ( iNMBlockingObject01 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject01)
		iNMBlockingObject01 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject01.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject02 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject02)
		iNMBlockingObject02 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject02.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject03 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject03)
		iNMBlockingObject03 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject03.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject04 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject04)
		iNMBlockingObject04 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject04.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject05 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject05)
		iNMBlockingObject05 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject05.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject06 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject06)
		iNMBlockingObject06 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject06.")
		#ENDIF
	ENDIF
	IF ( iNMBlockingObject07 != -1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNMBlockingObject07)
		iNMBlockingObject07 = -1
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing nav mesh blocking object iNMBlockingObject07.")
		#ENDIF
	ENDIF

ENDPROC

PROC CREATE_TRACKED_POINT_FOR_COORD(INT &iTrackedPoint, VECTOR vPosition, FLOAT fRadius)

	IF ( iTrackedPoint = -1  )
		iTrackedPoint = CREATE_TRACKED_POINT()
		SET_TRACKED_POINT_INFO(iTrackedPoint, vPosition, fRadius)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating tracked point with id ", iTrackedPoint, " for coord ", vPosition, " and radius ", fRadius, ".")
		#ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_TRACKED_POINT(INT &iTrackedPoint)

	IF ( iTrackedPoint != -1  )
		DESTROY_TRACKED_POINT(iTrackedPoint)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Destroying tracked point with id ", iTrackedPoint, ".")
		#ENDIF
		iTrackedPoint = -1
	ENDIF

ENDPROC

FUNC BOOL DOES_TRACKED_POINT_EXIST(INT iTrackedPoint)

	RETURN iTrackedPoint != -1
	
ENDFUNC

FUNC BOOL CAN_GET_CLOSEST_COVERPOINT_FOR_PED(PED_INDEX PedIndex, COVERPOINT_STRUCT &sArray[], COVERPOINT_STRUCT &sResult,
										 BOOL bCheckForPlayerPed = FALSE, FLOAT fPlayerDistance = 3.0,
										 BOOL bPreferCoverPoints = FALSE)

	INT i = 0

	FLOAT fShortestDistance = 1000.0
	
	FLOAT fCurrentDistance = 0.0
	
	INT iClosestCoverpoint = -1

	PED_INDEX ClosestPed

	IF NOT IS_PED_INJURED(PedIndex)

		FOR i = 0 TO ( COUNT_OF(sArray) - 1 )

			IF ( sArray[i].bActive = TRUE )
			AND ( sArray[i].bTaken = FALSE )
				
				//check if there is no other ped there
				IF NOT GET_CLOSEST_PED(sArray[i].vPosition, fPlayerDistance, FALSE, TRUE, ClosestPed)
				
					IF ( bPreferCoverPoints = FALSE )
						
						fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PedIndex), sArray[i].vPosition)
					
						IF ( fCurrentDistance  < fShortestDistance)
						
							IF ( bCheckForPlayerPed = TRUE)
						
								IF ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sArray[i].vPosition) > fPlayerDistance)
								
									fShortestDistance = fCurrentDistance

									iClosestCoverpoint = i
									
								ELSE
								
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_COVERPOINT_FOR_PED() Player ped is within the distance of the selected active coverpoint with index ", i, ".")
									#ENDIF
								
								ENDIF
								
							ELIF ( bCheckForPlayerPed = FALSE )
							
								fShortestDistance = fCurrentDistance
							
								iClosestCoverpoint = i
								
							ENDIF
						
						ENDIF
					
					ELIF ( bPreferCoverPoints = TRUE )
					
						IF ( sArray[i].bStationary = FALSE )
						
							fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PedIndex), sArray[i].vPosition)
					
							IF ( fCurrentDistance  < fShortestDistance)
							
								IF ( bCheckForPlayerPed = TRUE)
							
									IF ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sArray[i].vPosition) > fPlayerDistance)
									
										fShortestDistance = fCurrentDistance

										iClosestCoverpoint = i
										
									ELSE
									
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_COVERPOINT_FOR_PED() Player ped is within the distance of the selected active coverpoint with index ", i, ".")
										#ENDIF
									
									ENDIF
									
								ELIF ( bCheckForPlayerPed = FALSE )
								
									fShortestDistance = fCurrentDistance
								
									iClosestCoverpoint = i
									
								ENDIF
							
							ENDIF
						
						ENDIF
					
					ENDIF
					
				ENDIF

			ENDIF
			
		ENDFOR
	
	ENDIF
	
	IF ( iClosestCoverpoint = -1 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_COVERPOINT_FOR_PED() No active coverpoints found. Check your scripted coverpoints, fShortestDistance or ped's state.")
		#ENDIF
	
		RETURN FALSE
	
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_COVERPOINT_FOR_PED() Found closest active coverpoint from coverpoint array at index ", iClosestCoverpoint, ".")
	#ENDIF
	
	sResult = sArray[iClosestCoverpoint]
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_COVERPOINT_FOR_PED() Setting coverpoint at index ", iClosestCoverpoint, " as taken.")
	#ENDIF
	
	sArray[iClosestCoverpoint].bTaken = TRUE
	sArray[iClosestCoverpoint].TakenPedIndex = PedIndex
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), sArray[iClosestCoverpoint].vPosition, 0, 255, 0)
	#ENDIF

	RETURN TRUE

ENDFUNC

FUNC INT GET_NUMBER_OF_PEDS_FROM_RELATIONSHIP_GROUP_FOR_PED(PED_INDEX PedIndex, REL_GROUP_HASH rgHash, BOOL bCheckEntitiesOnScreen = FALSE)

	PED_INDEX pedArray[16]
	
	INT i = 0
	
	INT iNumber = 0
	
	IF NOT IS_PED_INJURED(PedIndex)

		GET_PED_NEARBY_PEDS(PedIndex, pedArray, PEDTYPE_ANIMAL)
	
		FOR i = 0 TO ( COUNT_OF(pedArray) - 1 )
		
			IF DOES_ENTITY_EXIST(pedArray[i])
			
				IF NOT IS_PED_INJURED(pedArray[i])
				
					IF ( bCheckEntitiesOnScreen = TRUE ) 
				
						IF IS_ENTITY_ON_SCREEN(pedArray[i])
							IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = rgHash )
								iNumber++
							ENDIF
						ENDIF
						
					ELSE
					
						IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = rgHash )
							iNumber++
						ENDIF
					
					ENDIF
				
				ENDIF
				
			ENDIF
		
		ENDFOR
	
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_NUMBER_OF_PEDS_FROM_RELATIONSHIP_GROUP_FOR_PED() returning ", iNumber, ".")
//	#ENDIF
	
	RETURN iNumber

ENDFUNC

FUNC PED_INDEX GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PED_INDEX PedIndex, REL_GROUP_HASH rgHash, INT iProximity = 0, BOOL bOnScreen = FALSE
															   #IF IS_DEBUG_BUILD, INT iRed = 0, INT iGreen = 255, INT iBlue = 0 #ENDIF)
	
	PED_INDEX pedArray[16]
	
	INT i = 0
		
	IF NOT IS_PED_INJURED(PedIndex)
	
		GET_PED_NEARBY_PEDS(PedIndex, pedArray, PEDTYPE_ANIMAL)
		
		FOR i = 0 TO ( COUNT_OF(pedArray) - 1 )
		
			IF DOES_ENTITY_EXIST(pedArray[i])
			
				IF NOT IS_PED_INJURED(pedArray[i])
				
					IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = rgHash )
				
						IF ( iProximity <= 0)

							IF ( bOnScreen = TRUE )
						
								IF IS_ENTITY_ON_SCREEN(pedArray[i])

									#IF IS_DEBUG_BUILD
										DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i]), iRed, iGreen, iBlue)
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Closest nearby ped on screen has index ", i, ".")
									#ENDIF

									RETURN pedArray[i]

								ENDIF
					
							ELSE

								#IF IS_DEBUG_BUILD
									DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i]), iRed, iGreen, iBlue)
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Closest nearby ped has index ", i, ".")
								#ENDIF

								RETURN pedArray[i]
								
							ENDIF
							
						ELSE
						
							IF ( i + iProximity <= COUNT_OF(pedArray) - 1 )
							
								IF DOES_ENTITY_EXIST(pedArray[i + iProximity])
			
									IF NOT IS_PED_INJURED(pedArray[i + iProximity])
									
										IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i + iProximity]) = rgHash )
									
											IF ( bOnScreen = TRUE )
										
												IF IS_ENTITY_ON_SCREEN(pedArray[i + iProximity])

													#IF IS_DEBUG_BUILD
														DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i + iProximity]), iRed, iGreen, iBlue)
														PRINTLN(GET_THIS_SCRIPT_NAME(), ": Nearby ped on screen offset by ", iProximity, " from closest nearby ped has index ", i + iProximity, ".")
													#ENDIF

													RETURN pedArray[i + iProximity]

												ENDIF
									
											ELSE

												#IF IS_DEBUG_BUILD
													DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i + iProximity]), iRed, iGreen, iBlue)
													PRINTLN(GET_THIS_SCRIPT_NAME(), ": Nearby ped offset by ", iProximity, " from closest nearby ped has index ", i + iProximity, ".")
												#ENDIF

												RETURN pedArray[i + iProximity]
												
											ENDIF
																		
										ENDIF
									
									ENDIF
								
								ENDIF
							
							ENDIF
							
						ENDIF
													
					ENDIF
				
				ENDIF
			
			ENDIF
	
		ENDFOR
		
	ENDIF
	
	RETURN NULL

ENDFUNC

FUNC PED_INDEX GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_BETWEEN_PED_AND_COORD(PED_INDEX PedIndex, REL_GROUP_HASH rgHash, VECTOR vCoordinates
																			 #IF IS_DEBUG_BUILD, INT iRed = 0, INT iGreen = 255, INT iBlue = 0 #ENDIF)

	PED_INDEX pedArray[16]
	
	INT i = 0
	
	IF NOT IS_PED_INJURED(PedIndex)
	
		//find nearby peds and sort them closest distance from specified ped
		//put the found ped indexes in the array
		GET_PED_NEARBY_PEDS(PedIndex, pedArray, PEDTYPE_ANIMAL)
		
		VECTOR vPedCoords = GET_ENTITY_COORDS(PedIndex)
		
		FOR i = 0 TO ( COUNT_OF(pedArray) - 1 )
		
			IF DOES_ENTITY_EXIST(pedArray[i])
			
				IF NOT IS_PED_INJURED(pedArray[i])
				
					//first ped that is in the specified relationship group should be the closest one
					//because GET_PED_NEARBY_PEDS sorts peds by distance
					IF ( GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = rgHash )
					
						IF ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedArray[i]), vCoordinates) < GET_DISTANCE_BETWEEN_COORDS(vPedCoords, vCoordinates) )
						
							#IF IS_DEBUG_BUILD
								DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(pedArray[i]), iRed, iGreen, iBlue)
//								PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_BETWEEN_PED_AND_COORD() Found closest ped in relationship group ")
//								IF (rgHash = rgEnemies)
//									PRINTSTRING("ENEMIES")
//								ENDIF
//								PRINTSTRING(" between specified ped and coordinates ")
//								PRINTSTRING(GET_STRING_FROM_VECTOR(vCoordinates))
//								PRINTSTRING(" at coordinates ")PRINTSTRING(GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(pedArray[i])))
//								PRINTSTRING(".")PRINTNL()
							#ENDIF

							RETURN pedArray[i]
						
						ENDIF
					
					ENDIF
				
				ENDIF
			
			ENDIF
			
		ENDFOR
	
	ENDIF
	
	//if no peds were found,
	//or if peds are not in the specified relationship group
//	#IF IS_DEBUG_BUILD
//		PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_CLOSEST_PED_BETWEEN_PED_AND_COORD() No peds found between specified ped and coordinates ", vCoordinates, ".")
//	#ENDIF

	RETURN NULL

ENDFUNC

FUNC BOOL HAS_PED_DAMAGED_ENTITY(PED_INDEX PedIndex, ENTITY_INDEX EntityIndex)

	IF NOT IS_PED_INJURED(PedIndex)

		IF DOES_ENTITY_EXIST(EntityIndex)
		
			IF NOT IS_ENTITY_DEAD(EntityIndex)

				RETURN HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(EntityIndex, PedIndex, TRUE)
						
			ENDIF
		
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_PED_DAMAGED_VEHICLES(PED_INDEX PedIndex, VEH_STRUCT &vsArray[])

	INT i = 0
	
	BOOL bReturn = FALSE
	
	FOR i = 0 TO ( COUNT_OF(vsArray) - 1 )
	
		IF HAS_PED_DAMAGED_ENTITY(PedIndex, vsArray[i].VehicleIndex)
		
			bReturn = TRUE
		
		ENDIF
	
	ENDFOR

	RETURN bReturn

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_ENTITY(PED_INDEX PedIndex, ENTITY_INDEX EntityIndex)

	IF NOT IS_PED_INJURED(PedIndex)

		IF DOES_ENTITY_EXIST(EntityIndex)
		
			IF NOT IS_ENTITY_DEAD(EntityIndex)

				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(EntityIndex, PedIndex, FALSE)
				
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(EntityIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
					
						RETURN TRUE
						
					ENDIF
				
				ENDIF
		
			ENDIF
		
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_VEHICLES(PED_INDEX PedIndex, VEH_STRUCT &vsArray[])

	INT i = 0
	
	BOOL bReturn = FALSE
	
	FOR i = 0 TO ( COUNT_OF(vsArray) - 1 )
	
		IF IS_PED_SHOOTING_AT_ENTITY(PedIndex, vsArray[i].VehicleIndex)
		
			bReturn = TRUE
		
		ENDIF
	
	ENDFOR

	RETURN bReturn

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_ENEMIES(PED_INDEX PedIndex, ENEMY_STRUCT &esArray[])

	INT i = 0
	
	BOOL bReturn = FALSE
	
	FOR i = 0 TO ( COUNT_OF(esArray) - 1 )
	
		IF IS_PED_SHOOTING_AT_ENTITY(PedIndex, esArray[i].PedIndex)
		
			bReturn = TRUE
		
		ENDIF
	
	ENDFOR

	RETURN bReturn

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_PEDS(PED_INDEX PedIndex, PED_STRUCT &psArray[])

	INT i = 0
	
	BOOL bReturn = FALSE
	
	FOR i = 0 TO ( COUNT_OF(psArray) - 1 )
	
		IF IS_PED_SHOOTING_AT_ENTITY(PedIndex, psArray[i].PedIndex)
		
			bReturn = TRUE
		
		ENDIF
	
	ENDFOR

	RETURN bReturn

ENDFUNC

FUNC BOOL IS_PLAYER_SHOOTING_AT_ANY_PEDS(PED_INDEX PedIndex)

	IF IS_PLAYER_PLAYING(PLAYER_ID())
	
		IF NOT IS_PED_INJURED(PedIndex)
		
			IF IS_PED_SHOOTING(PedIndex)
				
				IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			
					RETURN TRUE
					
				ENDIF

			ELSE
			
				//reset the flag is player is not shooting
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())

			ENDIF
			
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PED_SHOOTING_AT_AREA(PED_INDEX PedIndex, BOOL bIsPlayer, VECTOR vPosition, FLOAT fRadius #IF IS_DEBUG_BUILD, BOOL bDrawDebugSphere = TRUE,
								  INT iRed = 255, INT iGreen = 0, INT iBlue = 0, INT iAlpha = 128 #ENDIF)

	IF NOT IS_PED_INJURED(PedIndex)

		#IF IS_DEBUG_BUILD
			IF ( bDrawDebugSphere = TRUE )
				DRAW_DEBUG_SPHERE(vPosition, fRadius, iRed, iGreen, iBlue, iAlpha)
			ENDIF
		#ENDIF

		IF IS_PED_SHOOTING(PedIndex)
			RETURN IS_BULLET_IN_AREA(vPosition, fRadius, bIsPlayer)
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ENTITY_IN_LOCATE(ENTITY_INDEX EntityIndex, VECTOR vPosition, VECTOR vSize #IF IS_DEBUG_BUILD, STRING sDisplayName = NULL #ENDIF)

	//display debug locate name first
	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sDisplayName)
			DRAW_DEBUG_TEXT(sDisplayName, vPosition)
		ENDIF
	#ENDIF

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			IF IS_ENTITY_AT_COORD(EntityIndex, vPosition, vSize)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ENTITY_IN_ANGLED_AREA_LOCATE(ENTITY_INDEX EntityIndex, VECTOR vPosition1, VECTOR vPosition2, FLOAT fWidth #IF IS_DEBUG_BUILD, STRING sDisplayName = NULL #ENDIF)

	//display debug locate name first
	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sDisplayName)
			DRAW_DEBUG_TEXT(sDisplayName, vPosition1)
		ENDIF
	#ENDIF

	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			IF IS_ENTITY_IN_ANGLED_AREA(EntityIndex, vPosition1, vPosition2, fWidth)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Shoots an extra bullet from specified ped's weapon at sphere specified by coordinates and radius if this ped is firing.
/// PARAMS:
///    PedIndex - PED_INDEX of ped to shoot the bullet.
///    vCoordinates - Specifies coordinates to shoot the bullet at.
///    fRadius - Specifies the radius of the sphere where the bullet will randomly fly after being fired. Use 0.0 for precise shot at specified coord.
///    iDamage - Specifies the damage the bullet should cause.
PROC SHOOT_BULLET_FROM_PED_WEAPON_AT_COORDS(PED_INDEX PedIndex, VECTOR vCoordinates, FLOAT fRadius, INT iDamage)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			IF IS_PED_SHOOTING(PedIndex)
			
				WEAPON_TYPE CurrentWeapon
				
				IF GET_CURRENT_PED_WEAPON(PedIndex, CurrentWeapon)
			
					ENTITY_INDEX WeaponIndex = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(PedIndex)

					VECTOR vFinalCoordinates = << vCoordinates.X + GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), vCoordinates.Y + GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius), vCoordinates.Z + GET_RANDOM_FLOAT_IN_RANGE(-fRadius, fRadius) >>
								
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_ENTITY_COORDS(WeaponIndex), vFinalCoordinates, iDamage, TRUE, CurrentWeapon, PedIndex)
					
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_LINE(GET_ENTITY_COORDS(WeaponIndex), vFinalCoordinates, 255, 255, 0)
					#ENDIF
					
				ENDIF
			
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_SPHERE(vCoordinates, fRadius, 255, 255, 0, 64)
			#ENDIF
		
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_PED_AT_FRAGGABLE_TOMBSTONE_POSITION(PED_INDEX PedIndex, TOMBSTONE_STRUCT &TombstonesArray[], FLOAT fRadius, INT &iOutTombstone)

	INT i = 0

	IF NOT IS_PED_INJURED(PedIndex)
	
		FOR i = 0 TO ( COUNT_OF(TombstonesArray) - 1 )
		
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PedIndex), TombstonesArray[i].vPosition) < fRadius
			
				iOutTombstone = i
				
				RETURN TRUE
			
			ENDIF
		
		ENDFOR
		
	ENDIF

	RETURN FALSE

ENDFUNC

PROC RUN_TOMBSTONES_FRAG_CHECK(PED_INDEX PedIndex, PED_INDEX &EnemyPed, INT &iTimer, INT iTimeBetweenChecks, FLOAT fRadius)

	IF NOT IS_PED_INJURED(PedIndex)
	
		IF IS_PED_IN_COVER(PedIndex)
		OR IS_PED_DUCKING(PedIndex)
		
			INT iCurrentTombstone = 0
		
			//if is ped at one of the specified tombstone fraggable points
			IF IS_PED_AT_FRAGGABLE_TOMBSTONE_POSITION(PedIndex, FraggableTombstones, fRadius, iCurrentTombstone)
			
				IF NOT DOES_ENTITY_EXIST(EnemyPed)
				OR ( DOES_ENTITY_EXIST(EnemyPed) AND HAS_TIME_PASSED(iTimeBetweenChecks, iTimer) )
				
					EnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PedIndex, rgEnemies, 0, TRUE #IF IS_DEBUG_BUILD, 255, 255, 0 #ENDIF)
				
					iTimer = GET_GAME_TIMER()
					
				ENDIF
			
				SHOOT_BULLET_FROM_PED_WEAPON_AT_COORDS(EnemyPed, FraggableTombstones[iCurrentTombstone].vShootSphereCenter,
												  	   FraggableTombstones[iCurrentTombstone].fShootSphereRadius, 100)
			
			ELSE
			
				EnemyPed = NULL
				
				iTimer = GET_GAME_TIMER()
				
			ENDIF
		
		ENDIF
			
	ENDIF

ENDPROC

/// PURPOSE:
///    Returns PED_INDEX for specified player ped character from enumCharacterList.
/// PARAMS:
///    CHAR_TYPE - Character to get ped index for. For example CHAR_FRANKLIN, CHAR_TREVOR or CHAR_MICHAEL.
/// RETURNS:
///    PED_INDEX for the specified player ped character.
FUNC PED_INDEX GET_PED_INDEX(enumCharacterList CHAR_TYPE)

      IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TYPE )
	  
            RETURN PLAYER_PED_ID()
			
      ELSE
	  
            RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TYPE)]
			
      ENDIF
	  
ENDFUNC

/// PURPOSE:
///    Warps specified ped to new coordinates and sets specified ped's new heading.
/// PARAMS:
///    ped - PED_INDEX to warp.
///    vNewPosition - VECTOR coordinates specifying new ped's position.
///    fNewHeading - FLOAT specifying new ped's heading.
///    bKeepVehicle - Specifies if ped should keep vehicle they are currently in.
///    bResetGameplayCamera - Specifies if gameplay camera should be positioned behind player's back. Works only for PLAYER_PED_ID().
///    bLoadScene - Specifies if scene at new provided coordinates should be loaded. Works only for PLAYER_PED_ID().
PROC WARP_PED(PED_INDEX ped, VECTOR vNewPosition, FLOAT fNewHeading, BOOL bKeepVehicle, BOOL bResetGameplayCamera, BOOL bLoadScene)

	IF NOT IS_PED_INJURED(ped)
	
		
	
		IF ( bKeepVehicle = TRUE )
			SET_PED_COORDS_KEEP_VEHICLE(ped, vNewPosition)
		ELIF ( bKeepVehicle = FALSE ) 
			SET_ENTITY_COORDS(ped, vNewPosition)
		ENDIF
		
		SET_ENTITY_HEADING(ped, fNewHeading)
		
		IF ( ped = PLAYER_PED_ID())
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
		
			IF ( bResetGameplayCamera = TRUE)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF
			
			IF ( bLoadScene = TRUE )
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vNewPosition, ". This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vNewPosition)
				
			ENDIF
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping ped to coordinates ", vNewPosition, " and heading ", fNewHeading, ".")
			#ENDIF
		
		ENDIF
		
	ENDIF
	
ENDPROC

PROC EQUIP_PED_BEST_WEAPON(PED_INDEX PedIndex, BOOL bForceIntoHand)
	IF NOT IS_PED_INJURED(PedIndex)
	    WEAPON_TYPE eBestWeaponType = GET_BEST_PED_WEAPON(PedIndex, TRUE)
	    IF ( eBestWeaponType <> WEAPONTYPE_UNARMED )
			SET_CURRENT_PED_WEAPON(PedIndex, eBestWeaponType, bForceIntoHand)
			
			IF ( GET_AMMO_IN_PED_WEAPON(PedIndex, eBestWeaponType) < GET_MAX_AMMO_IN_CLIP(PedIndex, eBestWeaponType) )
				ADD_AMMO_TO_PED(PedIndex, eBestWeaponType, GET_MAX_AMMO_IN_CLIP(PedIndex, eBestWeaponType) * 2)
			ENDIF
			
	    ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates scripted coverpoints for peds to use in the shootout
/// PARAMS:
///    sArray - Array of coverpoint_struct instances, where all coverpoint details are stored.
PROC CREATE_COVERPOINTS(COVERPOINT_STRUCT &sArray[], VECTOR vStartPosition)

	//player's start cover point
	sArray[0].vPosition 		= << 3256.5776, -4575.2520, 117.2670 >>
	sArray[0].fHeading 			= 180.0
	sArray[0].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[0].CoverpointHeight 	= COVHEIGHT_LOW
	sArray[0].CoverpointArc 	= COVARC_90
	sArray[0].bActive			= FALSE
	sArray[0].bStationary		= FALSE
	
	//enemy in mocap coverpoint
	sArray[1].vPosition 		= << 3265.5886, -4590.6963, 116.0538 >> 
	sArray[1].fHeading 			= 356.8507
	sArray[1].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[1].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[1].CoverpointArc 	= COVARC_90
	sArray[1].bActive			= FALSE
	sArray[1].bStationary		= FALSE
	
	sArray[2].vPosition 		= << 3252.2168, -4600.8184, 115.6129 >>
	sArray[2].fHeading 			= 358.2709
	sArray[2].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[2].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[2].CoverpointArc 	= COVARC_90
	sArray[2].bActive			= TRUE
	sArray[2].bStationary		= FALSE 
	
	sArray[3].vPosition 		= << 3263.4075, -4671.8623, 112.9176 >>
	sArray[3].fHeading 			= 343.1592
	sArray[3].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[3].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[3].CoverpointArc 	= COVARC_90
	sArray[3].bActive			= TRUE
	sArray[3].bStationary		= FALSE
	
	sArray[4].vPosition 		= << 3259.5498, -4670.3813, 113.0985 >>
	sArray[4].fHeading 			= 333.0004
	sArray[4].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[4].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[4].CoverpointArc 	= COVARC_90
	sArray[4].bActive			= TRUE
	sArray[4].bStationary		= FALSE

	sArray[5].vPosition 		= << 3284.39, -4619.99, 115.01 >>
	sArray[5].fHeading 			= 333.0004
	sArray[5].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[5].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[5].CoverpointArc 	= COVARC_180
	sArray[5].bActive			= TRUE
	sArray[5].bStationary		= TRUE
	
	sArray[6].vPosition 		= << 3281.66, -4627.20, 114.88 >>
	sArray[6].fHeading 			= 333.0004
	sArray[6].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[6].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[6].CoverpointArc 	= COVARC_180
	sArray[6].bActive			= TRUE
	sArray[6].bStationary		= TRUE
	
	sArray[7].vPosition 		= << 3266.56, -4624.87, 115.22 >>
	sArray[7].fHeading 			= 333.0004
	sArray[7].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[7].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[7].CoverpointArc 	= COVARC_180
	sArray[7].bActive			= TRUE
	sArray[7].bStationary		= TRUE
	
	sArray[8].vPosition 		= << 3272.55, -4634.51, 114.69 >>
	sArray[8].fHeading 			= 333.0004
	sArray[8].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[8].CoverpointHeight 	= COVHEIGHT_TOOHIGH
	sArray[8].CoverpointArc 	= COVARC_180
	sArray[8].bActive			= TRUE
	sArray[8].bStationary		= TRUE
	
	sArray[9].vPosition 		= <<3275.6057, -4655.5288, 113.0920>>
	sArray[9].fHeading 			= 0.0
	sArray[9].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[9].CoverpointHeight 	= COVHEIGHT_LOW
	sArray[9].CoverpointArc 	= COVARC_90
	sArray[9].bActive			= TRUE
	sArray[9].bStationary		= FALSE
	
	sArray[10].vPosition 		= << 3270.9761, -4639.2290, 113.7132 >>
	sArray[10].fHeading 		= 0.0
	sArray[10].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[10].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[10].CoverpointArc 	= COVARC_90
	sArray[10].bActive			= TRUE
	sArray[10].bStationary		= FALSE
	
	sArray[11].vPosition 		= << 3274.2800, -4639.3032, 113.7452 >>
	sArray[11].fHeading 		= 0.0
	sArray[11].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[11].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[11].CoverpointArc 	= COVARC_90
	sArray[11].bActive			= TRUE
	sArray[11].bStationary		= FALSE

	sArray[12].vPosition 		= <<3269.2097, -4627.7412, 114.8999>>
	sArray[12].fHeading 		= 0.0
	sArray[12].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[12].CoverpointHeight = COVHEIGHT_LOW
	sArray[12].CoverpointArc 	= COVARC_90
	sArray[12].bActive			= TRUE
	sArray[12].bStationary		= FALSE
	
	sArray[13].vPosition 		= <<3275.9355, -4617.3945, 115.1056>>
	sArray[13].fHeading 		= 0.0
	sArray[13].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[13].CoverpointHeight = COVHEIGHT_LOW
	sArray[13].CoverpointArc 	= COVARC_90
	sArray[13].bActive			= TRUE
	sArray[13].bStationary		= FALSE
	
	sArray[14].vPosition 		= << 3278.8743, -4629.7856, 115.0547 >>
	sArray[14].fHeading 		= 89.5197
	sArray[14].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[14].CoverpointHeight = COVHEIGHT_LOW
	sArray[14].CoverpointArc 	= COVARC_90
	sArray[14].bActive			= TRUE
	sArray[14].bStationary		= FALSE
	
	sArray[15].vPosition 		= << 3255.8416, -4685.8252, 111.9065 >>
	sArray[15].fHeading 		= 346.2400
	sArray[15].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[15].CoverpointHeight = COVHEIGHT_LOW
	sArray[15].CoverpointArc 	= COVARC_90
	sArray[15].bActive			= TRUE
	sArray[15].bStationary		= FALSE

	sArray[16].vPosition 		= << 3256.8000, -4692.0205, 111.8840 >>
	sArray[16].fHeading 		= 310.2977
	sArray[16].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[16].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[16].CoverpointArc 	= COVARC_180
	sArray[16].bActive			= TRUE
	sArray[16].bStationary		= FALSE
	
	sArray[17].vPosition 		= <<3281.5632, -4665.9849, 113.1895>>
	sArray[17].fHeading 		= 0.0
	sArray[17].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[17].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[17].CoverpointArc 	= COVARC_90
	sArray[17].bActive			= TRUE
	sArray[17].bStationary		= FALSE
	
	sArray[18].vPosition 		= << 3251.7041, -4684.2788, 112.0217 >>
	sArray[18].fHeading 		= 336.7272 
	sArray[18].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[18].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[18].CoverpointArc 	= COVARC_0TO45
	sArray[18].bActive			= FALSE
	sArray[18].bStationary		= TRUE

	sArray[19].vPosition 		= << 3262.1086, -4691.1997, 112.0502 >>
	sArray[19].fHeading 		= 34.5997
	sArray[19].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[19].CoverpointHeight = COVHEIGHT_LOW
	sArray[19].CoverpointArc 	= COVARC_180
	sArray[19].bActive			= TRUE
	sArray[19].bStationary		= FALSE
	
	sArray[20].vPosition 		= <<3281.5667, -4649.8442, 113.5098>>
	sArray[20].fHeading 		= 0.0
	sArray[20].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[20].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[20].CoverpointArc 	= COVARC_90
	sArray[20].bActive			= TRUE
	sArray[20].bStationary		= FALSE
	
	sArray[21].vPosition 		= <<3275.0962, -4659.8999, 113.0035>>
	sArray[21].fHeading 		= 0.0
	sArray[21].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[21].CoverpointHeight = COVHEIGHT_LOW
	sArray[21].CoverpointArc 	= COVARC_90
	sArray[21].bActive			= TRUE
	sArray[21].bStationary		= FALSE
	
	sArray[22].vPosition 		= <<3265.5234, -4657.7856, 113.0917>>
	sArray[22].fHeading 		= 0.0
	sArray[22].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[22].CoverpointHeight = COVHEIGHT_LOW
	sArray[22].CoverpointArc 	= COVARC_90
	sArray[22].bActive			= TRUE
	sArray[22].bStationary		= FALSE
	
	sArray[23].vPosition 		= << 3246.5354, -4679.5972, 112.1638 >> 
	sArray[23].fHeading 		= 341.1975
	sArray[23].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[23].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[23].CoverpointArc 	= COVARC_90
	sArray[23].bActive			= TRUE
	sArray[23].bStationary		= FALSE

	sArray[24].vPosition 		= << 3248.0078, -4680.0767, 112.3925 >>
	sArray[24].fHeading 		= 337.8813
	sArray[24].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[24].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[24].CoverpointArc 	= COVARC_90
	sArray[24].bActive			= TRUE
	sArray[24].bStationary		= FALSE
	
	sArray[25].vPosition 		= << 3240.0486, -4665.3052, 114.2898 >> 
	sArray[25].fHeading 		= 0.0
	sArray[25].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[25].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[25].CoverpointArc 	= COVARC_90
	sArray[25].bActive			= TRUE
	sArray[25].bStationary		= FALSE

	sArray[26].vPosition 		= << 3231.7991, -4681.4941, 111.9197 >>
	sArray[26].fHeading 		= 272.3667
	sArray[26].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[26].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[26].CoverpointArc 	= COVARC_90
	sArray[26].bActive			= TRUE
	sArray[26].bStationary		= FALSE
	
	sArray[27].vPosition 		= <<3258.3953, -4608.1440, 115.4520>>
	sArray[27].fHeading 		= 4.3189
	sArray[27].CoverpointUsage 	= COVUSE_WALLTOLEFT
	sArray[27].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[27].CoverpointArc 	= COVARC_90
	sArray[27].bActive			= TRUE
	sArray[27].bStationary		= FALSE

	sArray[28].vPosition 		= << 3266.5139, -4616.4390, 115.0480 >>
	sArray[28].fHeading 		= 3.0096
	sArray[28].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[28].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[28].CoverpointArc 	= COVARC_90
	sArray[28].bActive			= TRUE
	sArray[28].bStationary		= FALSE
	
	sArray[29].vPosition 		= <<3213.1648, -4715.6494, 111.8129>> //<<3213.2832, -4715.4478, 111.8368>>
	sArray[29].fHeading 		= 295.1623
	sArray[29].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[29].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[29].CoverpointArc 	= COVARC_90
	sArray[29].bActive			= FALSE
	sArray[29].bStationary		= FALSE
	
	sArray[30].vPosition		= <<3265.4260, -4586.9629, 117.0842>>
	sArray[30].fHeading 		= 180.0
	sArray[30].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[30].CoverpointHeight = COVHEIGHT_LOW
	sArray[30].CoverpointArc 	= COVARC_90
	sArray[30].bActive			= FALSE
	sArray[30].bStationary		= FALSE
	
									//3262.43, -4586.83, 117.20
	sArray[31].vPosition		= << 3262.45, -4586.90, 117.11 >>
	sArray[31].fHeading 		= 180.0
	sArray[31].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[31].CoverpointHeight = COVHEIGHT_LOW
	sArray[31].CoverpointArc 	= COVARC_90
	sArray[31].bActive			= FALSE
	sArray[31].bStationary		= FALSE
	
	sArray[32].vPosition		= <<3274.1230, -4635.4863, 114.7559>>
	sArray[32].fHeading 		= 180.0
	sArray[32].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[32].CoverpointHeight = COVHEIGHT_LOW
	sArray[32].CoverpointArc 	= COVARC_90
	sArray[32].bActive			= FALSE
	sArray[32].bStationary		= FALSE
	
	sArray[33].vPosition		= <<3271.0203, -4635.4619, 114.6817>>
	sArray[33].fHeading 		= 180.0
	sArray[33].CoverpointUsage 	= COVUSE_WALLTONEITHER
	sArray[33].CoverpointHeight = COVHEIGHT_LOW
	sArray[33].CoverpointArc 	= COVARC_90
	sArray[33].bActive			= FALSE
	sArray[33].bStationary		= FALSE
	
	sArray[34].vPosition		= <<3260.0383, -4669.2715, 112.8813>>
	sArray[34].fHeading 		= 161.3189
	sArray[34].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[34].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[34].CoverpointArc 	= COVARC_90
	sArray[34].bActive			= FALSE
	sArray[34].bStationary		= FALSE

	sArray[35].vPosition		= <<3233.0288, -4681.5645, 112.2276>>
	sArray[35].fHeading 		= 92.2093
	sArray[35].CoverpointUsage 	= COVUSE_WALLTORIGHT
	sArray[35].CoverpointHeight = COVHEIGHT_TOOHIGH
	sArray[35].CoverpointArc 	= COVARC_90
	sArray[35].bActive			= FALSE
	sArray[35].bStationary		= FALSE
	
	
	//create coverpoints
	INT i = 0

	FOR i = 0 TO ( COUNT_OF(sArray) - 1 )
	
		IF ( sArray[i].bStationary = FALSE )
	
			sArray[i].CoverpointIndex = ADD_COVER_POINT(sArray[i].vPosition, sArray[i].fHeading, sArray[i].CoverpointUsage,
														sArray[i].CoverpointHeight, sArray[i].CoverpointArc)
		
		ENDIF
		
		//save the distance from start of the shootout corrdinates
		sArray[i].fDistance = VDIST2(vStartPosition, sArray[i].vPosition)
		
		sArray[i].bTaken = FALSE
		sArray[i].TakenPedIndex = NULL
		
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted coverpoints.")
	#ENDIF
	
ENDPROC

PROC REMOVE_COVERPOINTS(COVERPOINT_STRUCT &sArray[])

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(sArray) - 1 )
	
		REMOVE_COVER_POINT(sArray[i].CoverpointIndex)
	
	ENDFOR

ENDPROC

PROC INITIALISE_FRAGGABLE_TOMBSTONES(TOMBSTONE_STRUCT &tArray[])

	tArray[0].vPosition 			= << 3256.42, -4575.27, 117.15 >>
	tArray[0].vShootSphereCenter 	= << 3256.39, -4575.69, 118.25 >>
	tArray[0].fShootSphereRadius	= 0.35
	
	tArray[1].vPosition 			= << 3254.7119, -4575.1860, 117.1567 >>
	tArray[1].vShootSphereCenter 	= << 3254.68, -4576.13, 118.15 >>
	tArray[1].fShootSphereRadius	= 0.35
	
	tArray[2].vPosition 			= << 3252.7468, -4575.3774, 117.1567 >>
	tArray[2].vShootSphereCenter 	= << 3252.76, -4576.09, 118.15 >>
	tArray[2].fShootSphereRadius	= 0.35
	
ENDPROC

/// PURPOSE:
///    Requests IPLs for prologue map
PROC REQUEST_PROLOGUE_IPLS(#IF IS_DEBUG_BUILD BOOL bPrintOutput = FALSE #ENDIF)

	REQUEST_IPL("prologue01")
	REQUEST_IPL("prologue01c")
	REQUEST_IPL("prologue01d")
	REQUEST_IPL("prologue01e")
	REQUEST_IPL("prologue01f")
	REQUEST_IPL("prologue01g")
	REQUEST_IPL("prologue01h")
	REQUEST_IPL("prologue01i")
	REQUEST_IPL("prologue01j")
	REQUEST_IPL("prologue01k")
	REQUEST_IPL("prologue01z")
	
	REQUEST_IPL("prologue02")
	
	REQUEST_IPL("prologue03")
	REQUEST_IPL("prologue03b")
	
	REQUEST_IPL("prologue04")
	REQUEST_IPL("prologue04b")
	
	REQUEST_IPL("prologue05")
	REQUEST_IPL("prologue05b")
	
	REQUEST_IPL("prologue06")
	REQUEST_IPL("prologue06b")
	
	REQUEST_IPL("prologuerd")
	REQUEST_IPL("prologuerdb")
	
	REQUEST_IPL("prologue_occl")
	
	REQUEST_IPL("prologue_m2_door")	//cash depot door
	REQUEST_IPL("prologue06_pannel")//cash depot panel covering blood decal
	
	REQUEST_IPL("prologue_LODLights") REMOVE_IPL_FROM_REMOVAL_LIST("prologue_LODLights")
	REQUEST_IPL("prologue_DistantLights") REMOVE_IPL_FROM_REMOVAL_LIST("prologue_DistantLights")
	
	#IF IS_DEBUG_BUILD
		IF ( bPrintOutput = TRUE )
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting prologue IPLs.")
		ENDIF
	#ENDIF

ENDPROC

PROC REQUEST_PROLOGUE_GRAVE_IPL(BOOL bGraveCovered)

	IF ( bGraveCovered = TRUE )
		REQUEST_IPL("prologue03_grv_cov")
	ELSE
		REQUEST_IPL("prologue03_grv_dug")
		REQUEST_IPL("prologue_grv_torch")
	ENDIF
	
ENDPROC

PROC REMOVE_PROLOGUE_GRAVE_IPL(BOOL bGraveCovered)

	IF ( bGraveCovered = TRUE )
		REMOVE_IPL("prologue03_grv_cov")
	ELSE
		REMOVE_IPL("prologue03_grv_dug")
		REMOVE_IPL("prologue_grv_torch")
	ENDIF

ENDPROC

/// PURPOSE:
///    Removes IPLs for prologue map
PROC REMOVE_ALL_PROLOGUE_IPLS()

	REMOVE_IPL("prologue01")
	REMOVE_IPL("prologue01c")
	REMOVE_IPL("prologue01d")
	REMOVE_IPL("prologue01e")
	REMOVE_IPL("prologue01f")
	REMOVE_IPL("prologue01g")
	REMOVE_IPL("prologue01h")
	REMOVE_IPL("prologue01i")
	REMOVE_IPL("prologue01j")
	REMOVE_IPL("prologue01k")
	REMOVE_IPL("prologue01z")
	
	REMOVE_IPL("prologue02")
	
	REMOVE_IPL("prologue03")
	REMOVE_IPL("prologue03b")
	
	REMOVE_IPL("prologue04")
	REMOVE_IPL("prologue04b")
	
	REMOVE_IPL("prologue05")
	REMOVE_IPL("prologue05b")
	
	REMOVE_IPL("prologue06")
	REMOVE_IPL("prologue06b")
	
	REMOVE_IPL("prologuerd")
	REMOVE_IPL("prologuerdb")
	
	REMOVE_IPL("prologue_occl")
	
	REMOVE_IPL("prologue_m2_door")		//cash depot door
	REMOVE_IPL("prologue06_pannel")		//cash depot panel covering blood decal
	
	REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_LODLights")
	REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_DistantLights")
	
	//remove grave IPLs
	REMOVE_IPL("prologue03_grv_cov")	//grave covered up
	REMOVE_IPL("prologue03_grv_dug")	//grave dug up
	REMOVE_IPL("prologue_grv_torch")	//grave torch
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing prologue IPLs.")
	#ENDIF

ENDPROC

FUNC BOOL ARE_PROLOGUE_IPLS_ACTIVE(#IF IS_DEBUG_BUILD BOOL bPrintOutput = FALSE #ENDIF)

	IF	IS_IPL_ACTIVE("prologue01")
	AND	IS_IPL_ACTIVE("prologue01c")
	AND	IS_IPL_ACTIVE("prologue01d")
	AND	IS_IPL_ACTIVE("prologue01e")
	AND	IS_IPL_ACTIVE("prologue01f")
	AND	IS_IPL_ACTIVE("prologue01g")
	AND	IS_IPL_ACTIVE("prologue01h")
	AND	IS_IPL_ACTIVE("prologue01i")
	AND	IS_IPL_ACTIVE("prologue01j")
	AND	IS_IPL_ACTIVE("prologue01k")
	AND	IS_IPL_ACTIVE("prologue01z")
	AND	IS_IPL_ACTIVE("prologue02")
	AND	IS_IPL_ACTIVE("prologue03")
	AND	IS_IPL_ACTIVE("prologue03b")
	AND	IS_IPL_ACTIVE("prologue04")
	AND	IS_IPL_ACTIVE("prologue04b")
	AND	IS_IPL_ACTIVE("prologue05")
	AND	IS_IPL_ACTIVE("prologue05b")
	AND	IS_IPL_ACTIVE("prologue06")
	AND	IS_IPL_ACTIVE("prologue06b")
	AND	IS_IPL_ACTIVE("prologuerd")
	AND	IS_IPL_ACTIVE("prologuerdb")
	AND	IS_IPL_ACTIVE("prologue_occl")
	AND IS_IPL_ACTIVE("prologue_m2_door")
	AND IS_IPL_ACTIVE("prologue06_pannel")
	AND IS_IPL_ACTIVE("prologue_LODLights")
	AND IS_IPL_ACTIVE("prologue_DistantLights")

		#IF IS_DEBUG_BUILD
			IF ( bPrintOutput = TRUE )
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested prologue IPLs are active.")
			ENDIF
		#ENDIF

		RETURN TRUE
		
	ELSE
	
		REQUEST_PROLOGUE_IPLS(#IF IS_DEBUG_BUILD bPrintOutput #ENDIF)
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PROLOGUE_GRAVE_IPL_ACTIVE(BOOL bGraveCovered)

	IF ( bGraveCovered = TRUE 	AND IS_IPL_ACTIVE("prologue03_grv_cov"))
	OR ( bGraveCovered = FALSE 	AND IS_IPL_ACTIVE("prologue03_grv_dug") AND IS_IPL_ACTIVE("prologue_grv_torch") )
	
		RETURN TRUE

	ELSE
	
		REQUEST_PROLOGUE_GRAVE_IPL(bGraveCovered)
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC INITIALISE_ROADNODES(ROADNODE_STRUCT &sArray[])

	INT i 
	
	FOR i = 0 TO ( COUNT_OF(sArray) - 1 )
		sArray[i].bCurrent 			= FALSE
		sArray[i].bVisited 			= FALSE
		sArray[i].bInVehicleOnly 	= FALSE
		sArray[i].fAllowedDistance 	= 30.0
		sArray[i].fFailDistance 	= 65.0
	ENDFOR

	sArray[0].vPosition 		= << 5389.9370, -5119.8447, 77.2221 >>
	sArray[0].fAllowedDistance	= 60.0
	sArray[0].fFailDistance		= 75.0
	
	sArray[1].vPosition			= << 5345.9414, -5119.0063, 77.5530 >>
	sArray[2].vPosition 		= << 5296.8979, -5117.0566, 77.7141 >>
	sArray[3].vPosition			= << 5247.0103, -5118.9624, 78.1801 >>
	sArray[4].vPosition 		= << 5204.3687, -5109.8389, 80.1127 >>
	sArray[5].vPosition 		= << 5159.1436, -5099.8164, 82.9601 >>
	sArray[6].vPosition 		= << 5108.6816, -5096.3379, 85.0854 >>
	sArray[7].vPosition			= << 5062.9180, -5100.3804, 86.1269 >>
	sArray[8].vPosition 		= << 5015.0063, -5099.8042, 87.3856 >>
	sArray[9].vPosition			= << 4969.4209, -5094.0811, 89.0202 >>
	sArray[10].vPosition 		= << 4919.6489, -5074.7393, 92.9304 >>
	sArray[11].vPosition		= << 4870.7705, -5078.6123, 93.9856 >>
	sArray[12].vPosition 		= << 4832.3604, -5084.3633, 99.4540 >>
	sArray[13].vPosition		= << 4785.0508, -5086.8774, 105.334 >>
	sArray[14].vPosition 		= << 4738.4673, -5080.2607, 105.8345 >>
	sArray[15].vPosition		= << 4686.5527, -5076.7695, 104.6976 >>
	sArray[16].vPosition 		= << 4647.9287, -5081.8486, 104.5225 >>
	sArray[17].vPosition		= << 4602.0444, -5065.8916, 107.5456 >>
	sArray[18].vPosition 		= << 4565.0513, -5069.5972, 109.6650 >>
	sArray[19].vPosition		= << 4536.7461, -5087.0767, 109.5740 >>
	sArray[20].vPosition 		= << 4509.1904, -5097.9014, 109.6287 >>
	sArray[21].vPosition 		= << 4456.6255, -5103.6973, 110.0983 >>
	sArray[22].vPosition		= << 4424.8999, -5098.8540, 109.9986 >>
	sArray[23].vPosition 		= << 4396.0020, -5093.2793, 110.1756 >>
	sArray[24].vPosition		= << 4353.3970, -5084.6865, 110.0036 >>
	sArray[25].vPosition 		= << 4320.7988, -5079.9478, 109.9395 >>
	sArray[26].vPosition		= << 4291.6147, -5075.3096, 109.5563 >>
	sArray[27].vPosition		= << 4267.0718, -5073.1646, 109.8080 >>
	sArray[28].vPosition 		= << 4226.2935, -5070.5444, 109.8746 >>
	sArray[29].vPosition		= << 4174.7227, -5066.4771, 109.3556 >>
	sArray[30].vPosition 		= << 4143.5771, -5062.4434, 108.4574 >>
	sArray[31].vPosition		= << 4093.8284, -5059.8164, 107.4577 >>
	sArray[32].vPosition 		= << 4052.0479, -5055.2261, 107.6872 >>
	sArray[32].fFailDistance	= 75.0
	sArray[33].vPosition		= << 4004.2981, -5049.7749, 108.2748 >>
	sArray[33].fFailDistance	= 75.0
	sArray[34].vPosition 		= << 3964.8726, -5042.9771, 108.9244 >>
	sArray[34].fFailDistance	= 80.0
	sArray[35].vPosition		= << 3917.5972, -5032.8413, 109.8531 >>
	sArray[35].fFailDistance	= 80.0
	sArray[36].vPosition 		= << 3878.2581, -5020.6216, 110.4011 >>
	sArray[36].fFailDistance	= 80.0
	sArray[37].vPosition		= << 3833.2644, -5007.9419, 110.8658 >>
	sArray[37].fFailDistance	= 80.0
	sArray[38].vPosition 		= << 3794.0808, -4991.2241, 110.7152 >>
	sArray[38].fFailDistance	= 80.0
	sArray[39].vPosition		= << 3760.8127, -4980.0356, 110.1112 >>
	sArray[39].fFailDistance	= 80.0
	sArray[40].vPosition 		= << 3732.6055, -4965.3408, 110.3322 >>
	sArray[40].fFailDistance	= 80.0
	sArray[41].vPosition		= << 3693.6980, -4948.2495, 110.6774 >>
	sArray[41].fFailDistance	= 80.0
	sArray[42].vPosition 		= << 3671.0850, -4935.0083, 110.6896 >>
	sArray[42].fFailDistance	= 80.0
	sArray[43].vPosition		= << 3627.5833, -4914.8174, 110.6058 >>
	sArray[44].vPosition 		= << 3590.4031, -4898.1479, 110.6681 >>
	sArray[45].vPosition		= << 3553.3826, -4885.2036, 110.7241 >>
	sArray[46].vPosition 		= << 3524.9050, -4873.6226, 110.7502 >>
	sArray[47].vPosition		= << 3481.6187, -4862.8203, 110.7855 >>
	sArray[48].vPosition 		= << 3447.1887, -4856.9600, 110.7985 >>
	sArray[49].vPosition		= << 3415.6260, -4851.4873, 110.8072 >>
	sArray[50].vPosition 		= << 3387.0598, -4848.4624, 110.7909 >>
	sArray[51].vPosition 		= << 3343.5305, -4845.6938, 110.8147 >>
	sArray[52].vPosition		= << 3313.5476, -4842.9521, 110.8140 >>
	sArray[53].vPosition 		= << 3270.7791, -4839.1641, 110.8142 >>
	sArray[54].vPosition		= << 3242.1299, -4837.0957, 110.8146 >>
	sArray[55].vPosition 		= << 3212.8823, -4833.6548, 110.8150 >>
	sArray[55].fAllowedDistance	= 25.0
	sArray[55].fFailDistance	= 50.0
	sArray[56].vPosition		= << 3214.8853, -4805.0098, 110.8144 >>
	sArray[56].fAllowedDistance	= 35.0
	sArray[56].fFailDistance	= 55.0
	sArray[57].vPosition 		= << 3217.2200, -4777.5288, 110.8147 >>
	sArray[58].vPosition 		= << 3220.8281, -4727.3423, 111.2513 >>
	sArray[58].bInVehicleOnly	= TRUE

ENDPROC

FUNC BOOL CAN_GET_CLOSEST_ROADNODE_FOR_PED(PED_INDEX PedIndex, ROADNODE_STRUCT &sArray[], ROADNODE_STRUCT &sResult
										   #IF IS_DEBUG_BUILD, BOOL bPrintOutput = FALSE #ENDIF)

	INT i = 0

	FLOAT fShortestDistance = 1000.0
	
	FLOAT fCurrentDistance = 0.0
	
	INT iClosestRoadNode = -1

	IF NOT IS_PED_INJURED(PedIndex)

		FOR i = 0 TO ( COUNT_OF(sArray) - 1 )
			
			IF ( sArray[i].bVisited = FALSE )	//check only road nodes not yet visited
				
				fCurrentDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PedIndex), sArray[i].vPosition)
			
				IF ( fCurrentDistance  < fShortestDistance)
				
					fShortestDistance = fCurrentDistance
				
					iClosestRoadNode = i
					
					sArray[i].bCurrent = TRUE
					
					IF ( i > 0 )
					
						IF ( sArray[i - 1].bCurrent = TRUE )
							sArray[i - 1].bCurrent = FALSE
							sArray[i - 1].bVisited = TRUE
						ENDIF
					
					ENDIF
				
				ENDIF
				
			ENDIF

		ENDFOR
	
	ENDIF
	
	IF ( iClosestRoadNode = -1 )
	
		#IF IS_DEBUG_BUILD
			IF ( bPrintOutput = TRUE )
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_ROADNODE_FOR_PED() No road nodes found.")
			ENDIF
		#ENDIF
	
		RETURN FALSE
	
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF ( bPrintOutput = TRUE )
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_GET_CLOSEST_ROADNODE_FOR_PED() Found closest road node from road nodes array at index ", iClosestRoadNode, ".")
		ENDIF
	#ENDIF
	
	sResult = sArray[iClosestRoadNode]

	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), sArray[iClosestRoadNode].vPosition, 0, 255, 0)
	#ENDIF

	RETURN TRUE

ENDFUNC

PROC RUN_ROADNODES_CHECK(PED_INDEX PedIndex #IF IS_DEBUG_BUILD, BOOL bPrintOutput = FALSE #ENDIF)

	IF NOT IS_PED_INJURED(PedIndex)

		ROADNODE_STRUCT sCurrentRoadNode

		IF CAN_GET_CLOSEST_ROADNODE_FOR_PED(PedIndex, sRoadNodes, sCurrentRoadNode #IF IS_DEBUG_BUILD, bPrintOutput #ENDIF)
		
			FLOAT fDistanceToCurrentRoadNode = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PedIndex), sCurrentRoadNode.vPosition)
		
			//check if player should fail the mission for driving far away from current road node
			IF (  fDistanceToCurrentRoadNode > sCurrentRoadNode.fFailDistance )
		
				IF ( sCurrentRoadNode.bInVehicleOnly = TRUE )
		
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)						//if player is in the mission car
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())							//or if player is in any other car (used to push mission car around), fix for B*1713614
						bPlayerDroveOffRoute = TRUE
						
					ENDIF	
				ELSE
				
					bPlayerDroveOffRoute = TRUE											//this will cause a fail in run_fail_checks() proc
					
				ENDIF
		
			ELIF (  fDistanceToCurrentRoadNode > sCurrentRoadNode.fAllowedDistance )	//warn the player about driving too far away
			
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)							//if player is in the car
				
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GBRLC")
						IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							CLEAR_PRINTS()
						ENDIF
						PRINT_GOD_TEXT_ADVANCED("MCH1_GBRLC")
					ENDIF
					
				ENDIF

			ELSE
			
				//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
				//	IF HAS_LABEL_BEEN_TRIGGERED("MCH1_GBRLC")
				//		SET_LABEL_AS_TRIGGERED("MCH1_GBRLC", FALSE)
				//	ENDIF
				//ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Adds a model to model request array.
/// PARAMS:
///    ModelName - Model name being requested.
///    array - MODEL_NAMES array to add the requested model name to.
///    iArrayLength - Maximum length of the array.
///    iModelsAlreadyRequested - Number of models already requested in this array.
PROC ADD_MODEL_REQUEST_TO_ARRAY(MODEL_NAMES ModelName, MODEL_NAMES &array[], INT iArrayLength, INT &iModelsAlreadyRequested)

	INT i = 0
	
	BOOL bModelAlreadyRequested = FALSE
	
	IF ( iModelsAlreadyRequested >= 0 )
	
		IF ( iModelsAlreadyRequested <= ( iArrayLength - 1 ) )
		
			//loop through the array (from 0 to number of models already requested)
			//to see if the array already contains the model name being requested
			//modify the boolean flag if the model was found in array, otherwise leave the flag unchanged
			FOR i = 0 TO ( iModelsAlreadyRequested )
			
				IF ( array[i] = ModelName )
				
					bModelAlreadyRequested = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " is already in model array.")
					#ENDIF
					
				ENDIF
				
			ENDFOR
			
			IF ( bModelAlreadyRequested = FALSE )
			
				REQUEST_MODEL(ModelName)
				
				//iModelsAlreadyRequested will be the model array index of the model being requested
				array[iModelsAlreadyRequested] = ModelName
				
				iModelsAlreadyRequested++
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested model ", GET_MODEL_NAME_FOR_DEBUG(ModelName), " and added to model array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of models requested: ", iModelsAlreadyRequested, ".")
				#ENDIF
				
			ENDIF
		
		ELSE
		
			SCRIPT_ASSERT("Number of requested models is greater than the length of the model array.")
		
		ENDIF
	
	ELSE
	
		SCRIPT_ASSERT("Number of requested models is less than 0.")
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if all requested models from model array are loaded into memory.
/// PARAMS:
///    array - MODEL_NAMES array holding the models.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
/// RETURNS:
///    TRUE if all models from model array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_MODELS_LOADED(MODEL_NAMES &array[], INT &iModelsAlreadyRequested) 

	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model in array[", i, "]: ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
			#ENDIF
		
			IF NOT HAS_MODEL_LOADED(array[i])
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " is loading.")
				#ENDIF
				
				//if the model is not loaded keep requesting it
				REQUEST_MODEL(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), ".")
				#ENDIF
				
				RETURN FALSE
				
			ENDIF
			
		ENDFOR
		
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Models from model array loaded. Number of models loaded: ", iModelsAlreadyRequested, ".")
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans model array from requested models.
/// PARAMS:
///    array - MODEL_NAMES array.
///    iModelsAlreadyRequested - Number of already requested models. Should be greated than 0, since 0 means no models have been requested.
PROC CLEANUP_MODEL_ARRAY(MODEL_NAMES &array[], INT &iModelsAlreadyRequested)
	
	INT i = 0
	
	IF ( iModelsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iModelsAlreadyRequested - 1 )
		
			IF array[i] <> DUMMY_MODEL_FOR_SCRIPT
			
				SET_MODEL_AS_NO_LONGER_NEEDED(array[i])
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model ", GET_MODEL_NAME_FOR_DEBUG(array[i]), " cleaned up from model array by setting it as no longer needed.")
				#ENDIF
				
				array[i] = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			
		ENDFOR
		
	ENDIF
	
	iModelsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Model array cleaned up. Number of requested models set to 0.")
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Adds a car recording number to recording request array.
/// PARAMS:
///    iRecording - Recording number being requested.
///    sFile - Name of the file containing the car recording.
///    array - INT arry to add the requested recording number to.
///    iArrayLength - Maximum length of the array
///    iRecordingsAlreadyRequested - Number of recordings already requested in this array.
PROC ADD_CARREC_REQUEST_TO_ARRAY(INT iRecording, STRING sFile, INT &array[], INT iArrayLength, INT &iRecordingsAlreadyRequested)

	INT i = 0

	BOOL bRecordingRequested = FALSE

	IF ( iRecordingsAlreadyRequested >= 0 )

		IF ( iRecordingsAlreadyRequested <= ( iArrayLength - 1 ) )

			FOR i = 0 TO ( iRecordingsAlreadyRequested )
			
				IF array[i] = iRecording
				
					bRecordingRequested = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested car recording ", iRecording, " is already in recording array.")
					#ENDIF
					
				ENDIF
				
			ENDFOR
			
			IF ( bRecordingRequested = FALSE )
			
				REQUEST_VEHICLE_RECORDING(iRecording, sFile)
			
				array[iRecordingsAlreadyRequested] = iRecording
				
				iRecordingsAlreadyRequested++
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requested car recording ", iRecording, " and added to recording array.")
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Number of car recordings requested: ", iRecordingsAlreadyRequested, ".")
				#ENDIF
				
			ENDIF

		ELSE

			SCRIPT_ASSERT("Number of requested recordings is greater than the length of the recording array.")

		ENDIF

	ELSE
	
		SCRIPT_ASSERT("Number of requested recordings is less than 0.")
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if all requested car recordings are loaded into memory.
/// PARAMS:
///    sFile - File name of the car recording file.
///    array - INT array holding the recording numbers.
///    iRecordingsAlreadyRequested - Number of already requested recordings. Should be greated than 0, since 0 means no recordings have been requested.
/// RETURNS:
///    TRUE if all recordings from recording array are loaded into memory. FALSE if otherwise.
FUNC BOOL ARE_REQUESTED_CARRECS_LOADED(STRING sFile, INT &array[], INT &iRecordingsAlreadyRequested)

	INT i = 0
	
	IF ( iRecordingsAlreadyRequested > 0 )
	
        FOR i = 0 TO ( iRecordingsAlreadyRequested - 1 )
		
            IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(array[i], sFile)
			
                #IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Car recording ", array[i], " is loading.")
				#ENDIF
				
				//if the vehicle recording is not loaded keep requesting it
				REQUEST_VEHICLE_RECORDING(array[i], sFile)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting vehicle recording ", array[i], ".")
				#ENDIF
				
				RETURN FALSE
				
            ENDIF
			
        ENDFOR
		
    ENDIF 
	
   	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Car recordings from recording array loaded. Number of car recordings loaded: ", iRecordingsAlreadyRequested, ".")
	#ENDIF
	
    RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Cleans recording array from requested recordings.
/// PARAMS:
///    sFile - File name of the car recording file.
///    array - INT array holding the recording numbers.
///    iRecordingsAlreadyRequested - Number of already requested recordings. Should be greated than 0, since 0 means no recordings have been requested.
PROC CLEANUP_CARREC_ARRAY(STRING sFile, INT &array[], INT &iRecordingsAlreadyRequested)
	
	INT i = 0
	
	IF ( iRecordingsAlreadyRequested > 0 )
	
		FOR i = 0 TO ( iRecordingsAlreadyRequested - 1 )
		
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(array[i], sFile)
			
				REMOVE_VEHICLE_RECORDING(array[i], sFile)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Car recording ", array[i], " cleaned up from recording array.")
				#ENDIF
				
				array[i] = 0
				
			ENDIF
			
		ENDFOR
		
	ENDIF
	
	iRecordingsAlreadyRequested = 0
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Recording array cleaned up. Number of requested car recordings set to 0.")
	#ENDIF
	
ENDPROC

//|======================= END MISCELLANEOUS PROCEDURES & FUNCTIONS ======================|

//|============================== CLEANUP PROCEDURES & FUNCTIONS =========================|

FUNC PED_CLEANUP_REASONS GET_PED_CLEANUP_REASON(PED_INDEX PedIndex #IF IS_DEBUG_BUILD, STRING sDebugName #ENDIF)
	
	IF DOES_ENTITY_EXIST(PedIndex)
	
		IF IS_ENTITY_DEAD(PedIndex)
	
			ENTITY_INDEX SourceOfDeathEntity
						
			SourceOfDeathEntity = GET_PED_SOURCE_OF_DEATH(PedIndex)

			IF IS_ENTITY_A_PED(SourceOfDeathEntity)

				PED_INDEX SourceOfDeathPed
				
				SourceOfDeathPed = GET_PED_INDEX_FROM_ENTITY_INDEX(SourceOfDeathEntity)
				
				IF ( SourceOfDeathPed = PLAYER_PED_ID() )
				
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_PLAYER_AS_MICHAEL.")
						#ENDIF
						RETURN PED_CLEANUP_KILLED_BY_PLAYER_AS_MICHAEL
					ENDIF

				ENDIF
			
			ELIF IS_ENTITY_A_VEHICLE(SourceOfDeathEntity)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Ped ", sDebugName, " cleaned up with reason PED_CLEANUP_KILLED_BY_VEHICLE.")
				#ENDIF
				
				RETURN PED_CLEANUP_KILLED_BY_VEHICLE
					
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN PED_CLEANUP_NO_CLEANUP
	
ENDFUNC

PROC UPDATE_PLAYER_KILL_COUNTERS(PED_CLEANUP_REASONS eCleanupReason, INT &iPlayerKillsAsMichael)

	SWITCH eCleanupReason
		CASE PED_CLEANUP_KILLED_BY_PLAYER_AS_MICHAEL
			iPlayerKillsAsMichael++
			INFORM_MISSION_STATS_OF_INCREMENT(MIC1_KILLS)	//increment mission stat
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": **************************** UPDATE_PLAYER_KILL_COUNTERS ******************************")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player kills as Michael: ", iPlayerKillsAsMichael, ".")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": ***************************************************************************************")
	#ENDIF	

ENDPROC

PROC CLEANUP_ENEMY_PED(ENEMY_STRUCT &enemy, BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	IF ( bDelete = FALSE )
	
		IF DOES_ENTITY_EXIST(enemy.PedIndex)
			
			IF ( bDeathOnly = TRUE )
		
				IF IS_PED_INJURED(enemy.PedIndex)
				
					enemy.eCleanupReason = GET_PED_CLEANUP_REASON(enemy.PedIndex #IF IS_DEBUG_BUILD, enemy.sDebugName #ENDIF)

					UPDATE_PLAYER_KILL_COUNTERS(enemy.eCleanupReason, iPlayerMichaelKills)

					REMOVE_PED_DEFENSIVE_AREA(enemy.PedIndex)

					SET_PED_AS_NO_LONGER_NEEDED(enemy.PedIndex)
					
					IF DOES_BLIP_EXIST(enemy.BlipIndex)
						REMOVE_BLIP(enemy.BlipIndex)
					ENDIF
					
					IF IS_ITEMSET_VALID(enemy.ItemsetIndex)
						DESTROY_ITEMSET(enemy.ItemsetIndex)
					ENDIF

				ENDIF
				
			ELIF ( bDeathOnly = FALSE )

				IF NOT IS_PED_INJURED(enemy.PedIndex)
				
					SET_PED_KEEP_TASK(enemy.PedIndex, bKeepTask)
					SET_PED_AS_NO_LONGER_NEEDED(enemy.PedIndex)
					
					IF DOES_BLIP_EXIST(enemy.BlipIndex)
						REMOVE_BLIP(enemy.BlipIndex)
					ENDIF
					
				ENDIF
				
				IF IS_ITEMSET_VALID(enemy.ItemsetIndex)
					DESTROY_ITEMSET(enemy.ItemsetIndex)
				ENDIF

			ENDIF
		
		ENDIF
	
	ELIF ( bDelete = TRUE )

		IF DOES_ENTITY_EXIST(enemy.PedIndex)

			IF IS_PED_INJURED(enemy.PedIndex)
			OR NOT IS_PED_INJURED(enemy.PedIndex)

				IF IS_ENTITY_DEAD(enemy.PedIndex)
				OR NOT IS_ENTITY_DEAD(enemy.PedIndex)
					REMOVE_PED_DEFENSIVE_AREA(enemy.PedIndex)
				ENDIF
				
				DELETE_PED(enemy.PedIndex)
				
				IF DOES_BLIP_EXIST(enemy.BlipIndex)
					REMOVE_BLIP(enemy.BlipIndex)
				ENDIF
				
			ENDIF

		ENDIF
		
		IF IS_ITEMSET_VALID(enemy.ItemsetIndex)
			DESTROY_ITEMSET(enemy.ItemsetIndex)
		ENDIF
		
		CLEANUP_AI_PED_BLIP(enemy.EnemyBlipData)
		
		enemy.bCreated 				= FALSE
		enemy.bFallingBack 			= FALSE
		enemy.bHasTask				= FALSE
		enemy.bSearching			= FALSE
		enemy.bReversing			= FALSE
		
		enemy.iTimer				= 0
		enemy.iProgress 			= 0
		enemy.iWaypointProgress 	= 0
		enemy.iConversationTimer 	= 0
		enemy.eCleanupReason		= PED_CLEANUP_NO_CLEANUP
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Handles removing dead enemy peds from a specified enemy ped array by setting them as no longer needed.
///    Deletes the peds no matter of their health state if bDelete is set to TRUE.
/// PARAMS:
///    array - Array of enemy peds to cleanup.
///    bDelete - If set to TRUE this will delete the peds no matter if they are dead or alive.
///    bDeathOnly - IF set to TRUE this will set peds as no longer needed, when bDelete is FALSE. Use for mission cleanup.
///    bKeepTask - IF set to TRUE this will make peds keep their tasks. Use for mission cleanup.
PROC CLEANUP_ENEMY_GROUP(ENEMY_STRUCT &array[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bKeepTask = TRUE)

	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		CLEANUP_ENEMY_PED(array[i], bDelete, bDeathOnly, bKeepTask)
		IF ( bDelete = TRUE )
			array[i].bCreated = FALSE
		ENDIF
	ENDFOR

ENDPROC

/// PURPOSE:
///    Handles removing dead enemy vehicles from a specified vehicle struct array by setting them as no longer needed.
///    Deletes the vehicles no matter of their health state if bDelete is set to TRUE.
/// PARAMS:
///    vsArray - Array of instances of vehicle_struct.
///    bDelete - If set to TRUE this will delete the vehicles no matter if they are dead or alive.
///    bDeathOnly - If set to TRUE this will set vehicles as no longer needed, when bDelete is FALSE. Use for mission cleanup.
///    bBlipOnly - If set to TRUE this will remove dead or undriveable vehicle blip only.
PROC CLEANUP_ENEMY_VEHICLES(VEH_STRUCT &vsArray[], BOOL bDelete = FALSE, BOOL bDeathOnly = TRUE, BOOL bBlipOnly = FALSE)
	
	INT i = 0
	
	FOR i = 0 TO ( COUNT_OF(vsArray) - 1 )

		IF ( bDelete = TRUE )

			IF DOES_ENTITY_EXIST(vsArray[i].VehicleIndex)
			
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) = vsArray[i].VehicleIndex
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			
				DELETE_VEHICLE(vsArray[i].VehicleIndex)
			ENDIF
			
			IF DOES_BLIP_EXIST(vsArray[i].BlipIndex)
				REMOVE_BLIP(vsArray[i].BlipIndex)
			ENDIF
			
			vsArray[i].iProgress = 0
			vsArray[i].bSmoking	= FALSE
			vsArray[i].bDriverDead = FALSE
			vsArray[i].bAutoDriveActive = FALSE
			
		ELIF ( bDelete = FALSE )
			
			IF DOES_ENTITY_EXIST(vsArray[i].VehicleIndex)
			
				IF ( bDeathOnly = TRUE )
					
					IF ( bBlipOnly = FALSE )
				
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vsArray[i].VehicleIndex)
						
					ENDIF
					
					IF DOES_BLIP_EXIST(vsArray[i].BlipIndex)
						REMOVE_BLIP(vsArray[i].BlipIndex)
					ENDIF
				
				ELIF ( bDeathOnly = FALSE )

						IF DOES_BLIP_EXIST(vsArray[i].BlipIndex)
							REMOVE_BLIP(vsArray[i].BlipIndex)
						ENDIF

						SET_VEHICLE_AS_NO_LONGER_NEEDED(vsArray[i].VehicleIndex)
					
					vsArray[i].iProgress = 0
					vsArray[i].bDriverDead = FALSE
					vsArray[i].bAutoDriveActive = FALSE
				
				ENDIF
				
			ENDIF
		
		ENDIF

	ENDFOR
	
ENDPROC

PROC CLEANUP_MISSION_OBJECT(OBJECT_STRUCT &osObject, BOOL bDelete = FALSE)

	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)

		IF IS_ENTITY_ATTACHED(osObject.ObjectIndex)
			DETACH_ENTITY(osObject.ObjectIndex)
		ENDIF

		IF ( bDelete = TRUE )
			DELETE_OBJECT(osObject.ObjectIndex)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(osObject.ObjectIndex)
		ENDIF
	
	ENDIF

ENDPROC

PROC RUN_MISSION_STAGE_CLEANUP(MISSION_STAGES eStage)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission stage cleanup.")
	#ENDIF
	
	KILL_PHONE_CONVERSATION()
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)

	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
	REMOVE_CUTSCENE()
	
	IF ( bReplayStartEventTriggered = TRUE )
		REPLAY_STOP_EVENT()
	ENDIF

	IF ( bProloguePedComponentsActive = TRUE )	//set non prologue clothes on player peds
				
		IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_MICHAEL))
			IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_MICHAEL))
				RESTORE_PLAYER_PED_VARIATIONS(GET_PED_INDEX(CHAR_MICHAEL))
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_TREVOR))
			IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
				RESTORE_PLAYER_PED_VARIATIONS(GET_PED_INDEX(CHAR_TREVOR))
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring default player ped component variations.")
		#ENDIF
		
		bProloguePedComponentsActive = FALSE
		
	ENDIF

	//general stage cleanup
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	
		IF NOT IS_PED_INJURED(psTrevor.PedIndex)
			DELETE_PED(psTrevor.PedIndex)
		ENDIF
		
		IF DOES_BLIP_EXIST(psTrevor.BlipIndex)
			REMOVE_BLIP(psTrevor.BlipIndex)
		ENDIF
		
		IF DOES_BLIP_EXIST(psMichael.BlipIndex)
			REMOVE_BLIP(psMichael.BlipIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
			DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
			DELETE_VEHICLE(vsTrevorsCar.VehicleIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
			DELETE_VEHICLE(vsTrevorsPlane.VehicleIndex)
		ENDIF
		
		IF DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
			REMOVE_BLIP(vsMichaelsCar.BlipIndex)
		ENDIF
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	
		IF NOT IS_PED_INJURED(psMichael.PedIndex)
			DELETE_PED(psMichael.PedIndex)
		ENDIF			
		
		IF DOES_BLIP_EXIST(psMichael.BlipIndex)
			REMOVE_BLIP(psMichael.BlipIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
			DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
			DELETE_VEHICLE(vsTrevorsCar.VehicleIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
			DELETE_VEHICLE(vsTrevorsPlane.VehicleIndex)
		ENDIF
		
		IF DOES_BLIP_EXIST(vsTrevorsCar.BlipIndex)
			REMOVE_BLIP(vsTrevorsCar.BlipIndex)
		ENDIF
		
		IF DOES_BLIP_EXIST(vsTrevorsPlane.BlipIndex)
			REMOVE_BLIP(vsTrevorsPlane.BlipIndex)
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(psBrad.PedIndex)
		DELETE_PED(psBrad.PedIndex)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vsCutsceneCar.VehicleIndex)
		DELETE_VEHICLE(vsCutsceneCar.VehicleIndex)
	ENDIF
	
	
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
	IF 	DOES_ENTITY_EXIST(tsTrain.VehicleIndex)
	AND DOES_ENTITY_EXIST(tsTrain.PedIndex)
		DELETE_PED(tsTrain.PedIndex)
		DELETE_MISSION_TRAIN(tsTrain.VehicleIndex)
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleted mission train.")
		#ENDIF
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
		
	//reset this flag for skips
	bGameplayCameraSet = FALSE
	
	CLEANUP_MISSION_OBJECT(osGolfBall, TRUE)
	CLEANUP_MISSION_OBJECT(osPropCoffin, TRUE)
	CLEANUP_MISSION_OBJECT(osPropShovel, TRUE)
	CLEANUP_MISSION_OBJECT(osPropPickaxe, TRUE)
	
	CLEANUP_TRACKED_POINT(iCarTrackedPoint)
	
	REMOVE_ANIM_DICT("dead")
	REMOVE_ANIM_DICT("missmic1ig_2")
	REMOVE_ANIM_DICT("missmic1ig_3_patrol")
	REMOVE_ANIM_DICT("missmic1ig_zero_hit_wheel")
	REMOVE_ANIM_DICT("missmic1leadinoutmic_1_mcs_2")
	
	//specific stage cleanup
	SWITCH eStage
	
		CASE MISSION_STAGE_GET_TO_GRAVEYARD
		CASE MISSION_STAGE_GET_TO_GRAVE
		CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
		CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
		
			CLEANUP_ENEMY_GROUP(esWallEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esVanEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esMiddleEnemies, TRUE)	
			CLEANUP_ENEMY_GROUP(esChurchEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esSideVanEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esSideVanBackupEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esExitEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esRunners, TRUE)
			CLEANUP_ENEMY_GROUP(esVan4Enemies, TRUE)
			CLEANUP_ENEMY_GROUP(esSideEnemies, TRUE)
			
			CLEANUP_ENEMY_GROUP(esFirstReinforcementEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esSecondReinforcementEnemies, TRUE)
			CLEANUP_ENEMY_GROUP(esThirdReinforcementEnemies, TRUE)
			
			CLEANUP_ENEMY_VEHICLES(vsEnemyVans, TRUE)
			
			//reset stats for headshots
			RESET_MISSION_STATS_ENTITY_WATCH()
						
			REMOVE_COVERPOINTS(csCoverpoints)
			bCoverpointsCreated = FALSE
			
			//remove the tombstones
			REMOVE_IPL("prologue03b")
			
			SET_MODEL_AS_NO_LONGER_NEEDED(mnEnemy)
			
			IF DOES_ENTITY_EXIST(WeaponMichaelObjectIndex)
				DELETE_OBJECT(WeaponMichaelObjectIndex)
			ENDIF
			
			IF DOES_ENTITY_EXIST(WeaponEnemyObjectIndex)
				DELETE_OBJECT(WeaponEnemyObjectIndex)
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
	IF DOES_PICKUP_EXIST(HealthPickup)
		REMOVE_PICKUP(HealthPickup)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PistolPickup)
		REMOVE_PICKUP(PistolPickup)
	ENDIF
	
	IF DOES_PICKUP_EXIST(RiflePickup)
		REMOVE_PICKUP(RiflePickup)
	ENDIF
	
	//music score stop & cleanup
	TRIGGER_MUSIC_EVENT("MIC1_FAIL")
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_FAIL.")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping score music.")
	#ENDIF
	CANCEL_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")
	CANCEL_MUSIC_EVENT("MIC1_SHOOTOUT_START")
	CANCEL_MUSIC_EVENT("MIC1_1ST_VAN")
	CANCEL_MUSIC_EVENT("MIC1_ALERTED")
	CANCEL_MUSIC_EVENT("MIC1_KIDNAPPED")
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cancelling music events.")
	#ENDIF
	
	STOP_STREAM()
	STOP_AUDIO_SCENES()
	
	STOP_SOUND(iTrainBellsSoundID)
	STOP_SOUND(iChurchBellsSoundID)
	STOP_SOUND(iRainOnPlaneSoundID)
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Sounds")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_HEIST_3B_SHOOTOUT")
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage cleanup.")
	#ENDIF

ENDPROC

/// PURPOSE:
///    Handles cleaning up when mission ends.
PROC MISSION_CLEANUP()

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting mission cleanup.")
	#ENDIF

	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	
	KILL_FACE_TO_FACE_CONVERSATION()
	KILL_ANY_CONVERSATION()

	CLEANUP_MODEL_ARRAY(MissionModels, iModelsRequested)
	CLEANUP_CARREC_ARRAY(sVehicleRecordingsFile, MissionRecordings, iRecordingsRequested)

	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_ALL_CAMS()
	
	IF ( bReplayStartEventTriggered = TRUE )
		REPLAY_STOP_EVENT()
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_ANGRY(PLAYER_PED_ID(), FALSE)
		STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF ( bProloguePedComponentsActive = TRUE )	//set non prologue clothes on player peds
				
		IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_MICHAEL))
			IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_MICHAEL))
				//RESTORE_PLAYER_PED_VARIATIONS(GET_PED_INDEX(CHAR_MICHAEL))
				RESTORE_MISSION_START_OUTFIT()
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_TREVOR))
			IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
				RESTORE_PLAYER_PED_VARIATIONS(GET_PED_INDEX(CHAR_TREVOR))
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring default player ped component variations.")
		#ENDIF
		
		bProloguePedComponentsActive = FALSE
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(psBrad.PedIndex)
		IF NOT IS_ENTITY_DEAD(psBrad.PedIndex)
			SET_PED_KEEP_TASK(psBrad.PedIndex, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(psBrad.PedIndex)
		ENDIF
	ENDIF
	
	//clear mission vehicles
	IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
		IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsMichaelsCar.VehicleIndex, TRUE)
			IF 	IS_ENTITY_A_MISSION_ENTITY(vsMichaelsCar.VehicleIndex)
			AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vsMichaelsCar.VehicleIndex)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
		IF NOT IS_ENTITY_DEAD(vsTrevorsCar.VehicleIndex)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsTrevorsCar.VehicleIndex, TRUE)
			IF ( bTrevorsCarSetAsVehicleGenVehicle = FALSE )
				IF 	IS_ENTITY_A_MISSION_ENTITY(vsTrevorsCar.VehicleIndex)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vsTrevorsCar.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vsTrevorsCar.VehicleIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
		IF NOT IS_ENTITY_DEAD(vsTrevorsPlane.VehicleIndex)
			IF 	IS_ENTITY_A_MISSION_ENTITY(vsTrevorsPlane.VehicleIndex)
			AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vsTrevorsPlane.VehicleIndex)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vsTrevorsPlane.VehicleIndex)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(tsTrain.VehicleIndex)
		IF NOT IS_ENTITY_DEAD(tsTrain.VehicleIndex)
			IF IS_SCREEN_FADED_OUT()
				DELETE_MISSION_TRAIN(tsTrain.VehicleIndex)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleting mission train.")
				#ENDIF
			ELSE
				SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(tsTrain.VehicleIndex)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting mission train as no longer needed.")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vsCutsceneCar.VehicleIndex)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vsCutsceneCar.VehicleIndex)
	ENDIF

	//clear blips
	IF DOES_BLIP_EXIST(psMichael.BlipIndex)
		REMOVE_BLIP(psMichael.BlipIndex)
	ENDIF

	IF DOES_BLIP_EXIST(psTrevor.BlipIndex)
		REMOVE_BLIP(psTrevor.BlipIndex)
	ENDIF

	IF DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
		REMOVE_BLIP(vsMichaelsCar.BlipIndex)
	ENDIF
	
	IF DOES_BLIP_EXIST(vsTrevorsCar.BlipIndex)
		REMOVE_BLIP(vsTrevorsCar.BlipIndex)
	ENDIF
	
	IF DOES_BLIP_EXIST(vsTrevorsPlane.BlipIndex)
		REMOVE_BLIP(vsTrevorsPlane.BlipIndex)
	ENDIF
	
	CLEANUP_ENEMY_GROUP(esVanEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esMiddleEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esWallEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esChurchEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esSideVanBackupEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esSideVanEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esExitEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esVan4Enemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esRunners, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esSideEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esFirstReinforcementEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esSecondReinforcementEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	CLEANUP_ENEMY_GROUP(esThirdReinforcementEnemies, IS_SCREEN_FADED_OUT(), FALSE)
	
	CLEANUP_ENEMY_VEHICLES(vsEnemyVans, IS_SCREEN_FADED_OUT(), FALSE)
	
	CLEANUP_MISSION_OBJECT(osGolfBall, FALSE)
	CLEANUP_MISSION_OBJECT(osPropCoffin, FALSE)
	CLEANUP_MISSION_OBJECT(osPropShovel, FALSE)
	CLEANUP_MISSION_OBJECT(osPropPickaxe, FALSE)
	
	CLEANUP_TRACKED_POINT(iCarTrackedPoint)
	
	IF DOES_PICKUP_EXIST(HealthPickup)
		REMOVE_PICKUP(HealthPickup)
	ENDIF
	
	IF DOES_PICKUP_EXIST(PistolPickup)
		REMOVE_PICKUP(PistolPickup)
	ENDIF
	
	IF DOES_PICKUP_EXIST(RiflePickup)
		REMOVE_PICKUP(RiflePickup)
	ENDIF

	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR), FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(ASEA2, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(MESA2, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEOLD1, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEOLD2, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(mnEnemy, FALSE)
	
	//enable the car gen on mission cleanup
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	DISABLE_CELLPHONE(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	
	SET_MAX_WANTED_LEVEL(5)
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	//enable dispatch services
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	
	STOP_SOUND(iTrainBellsSoundID)
	RELEASE_SOUND_ID(iTrainBellsSoundID)
	
	STOP_SOUND(iChurchBellsSoundID)
	RELEASE_SOUND_ID(iChurchBellsSoundID)
	
	STOP_SOUND(iRainOnPlaneSoundID)
	RELEASE_SOUND_ID(iRainOnPlaneSoundID)
	
	SET_ROADS_IN_ANGLED_AREA(<<5526.240234,-5137.229980,61.789253>>, <<3679.326660,-4973.879395,125.082840>>, 192.0, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<3691.211426,-4941.240234,94.593681>>, <<3511.115479,-4869.191406,126.762108>>, 16.0, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<3510.004395,-4865.810059,94.695572>>, <<3204.424316,-4833.816895,126.815216>>, 16.0, FALSE, FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<3186.533691,-4832.797852,109.814827>>, <<3202.187256,-4833.993164,114.814995>>, 16.0, FALSE, FALSE) //road leading down into town
	
	REMOVE_PTFX_ASSET()
	REMOVE_ANIM_DICT("missmic1ig_2")
	REMOVE_ANIM_DICT("missmic1ig_3_patrol")
	REMOVE_ANIM_DICT("missmic1ig_zero_hit_wheel")
	REMOVE_ANIM_DICT("missmic1leadinoutmic_1_mcs_2")
	
	SET_USER_RADIO_CONTROL_ENABLED(TRUE)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Sounds")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_HEIST_3B_SHOOTOUT")
	RELEASE_SCRIPT_AUDIO_BANK()
	
	STOP_STREAM()
	STOP_AUDIO_SCENES()
	
	//music score stop & cleanup
	TRIGGER_MUSIC_EVENT("MIC1_FAIL")
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_FAIL.")
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Stopping score music.")
	#ENDIF
	CANCEL_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")
	CANCEL_MUSIC_EVENT("MIC1_SHOOTOUT_START")
	CANCEL_MUSIC_EVENT("MIC1_1ST_VAN")
	CANCEL_MUSIC_EVENT("MIC1_ALERTED")
	CANCEL_MUSIC_EVENT("MIC1_KIDNAPPED")
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cancelling music events.")
	#ENDIF
	
	IF ( bWeatherTypeOvercastSet = TRUE )
	OR ( bWeatherTypeRainSet = TRUE )
	OR ( bWeatherTypeThunderSet = TRUE )
		CLEAR_WEATHER_TYPE_PERSIST()
	ENDIF
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	REMOVE_NAVMESH_BLOCKING_OBJECTS()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, FALSE)
	
	SET_RANDOM_TRAINS(TRUE)
	SET_ALLOW_STREAM_PROLOGUE_NODES(FALSE)
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	RESET_WORLD_BOUNDARY_FOR_PLAYER()
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_SUPER_JUMP)
		DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, FALSE)
	ENDIF
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_SPAWN_VEHICLE)
		DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, FALSE)
	ENDIF
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_ADVANCE_WEATHER)
		DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, FALSE)
	ENDIF
	
	SET_AMBIENT_VEHICLE_NEON_ENABLED(TRUE)
	
	SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
	PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting audio flag DisableReplayScriptStreamRecording to false.")
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission cleanup.")
	#ENDIF
	
ENDPROC

PROC PROLOGUE_MAP_CLEANUP()

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting prologue map cleanup.")
	#ENDIF
	
	UNLOCK_GRAVEYARD_GATES()
	UNREGISTER_GRAVEYARD_GATES()
	
	REMOVE_ALL_PROLOGUE_IPLS()
	
	SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)
	
	IF ( bSnowOn = TRUE )
		//clear weather type
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		CLEAR_WEATHER_TYPE_PERSIST()
		UNLOAD_ALL_CLOUD_HATS()
	ELIF ( bSnowOn = FALSE )
		CLEAR_WEATHER_TYPE_PERSIST()
	ENDIF
	
	CLEAR_TIMECYCLE_MODIFIER()
	DISABLE_MOON_CYCLE_OVERRIDE()
	SET_AMBIENT_VEHICLE_NEON_ENABLED(TRUE)
	
	DELETE_ALL_TRAINS()
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished prologue map cleanup.")
	#ENDIF

ENDPROC

//|============================ END CLEANUP PROCEDURES & FUNCTIONS =======================|

//|======================= DEBUG VARIABLES, PROCEDURES & FUNCTIONS =======================|

#IF IS_DEBUG_BUILD

	//mission skip menu variables
	MissionStageMenuTextStruct	SkipMenu[11]
	INT 						iReturnStage
	BOOL 						bStageResetFlag

	//debug widgets veriables
	WIDGET_GROUP_ID 			Michael1Widgets
	BOOL						bResetGameplayHint		= FALSE
	BOOL 						bSnowToggle 			= FALSE
	BOOL 						bApplyForceToggle 		= FALSE
	BOOL 						bEnableApplyForceWidget = FALSE
	BOOL 						bPrintDebugOutput		= FALSE
	BOOL						bDrawDebugLinesAndSpheres
	BOOL						bDrawDebugStates
	BOOL						bDrawDebugCoverpoints
	VECTOR 						vApplyForceWidgetForce
	VECTOR						vApplyForceWidgetOffset

	FUNC STRING GET_MISSION_STAGE_NAME_FOR_DEBUG(MISSION_STAGES eStage)
	
		SWITCH eStage
			CASE MISSION_STAGE_CUTSCENE_INTRO
				RETURN "MISSION_STAGE_CUTSCENE_INTRO"
			BREAK
			CASE MISSION_STAGE_GET_TO_AIRPORT
				RETURN "MISSION_STAGE_GET_TO_AIRPORT"
			BREAK
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
				RETURN "MISSION_STAGE_FLY_TO_NORTH_YANKTON"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_AIRPORT
				RETURN "MISSION_STAGE_CUTSCENE_AIRPORT"
			BREAK
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
				RETURN "MISSION_STAGE_GET_TO_GRAVEYARD"
			BREAK
			CASE MISSION_STAGE_GET_TO_GRAVE
				RETURN "MISSION_STAGE_GET_TO_GRAVE"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
				RETURN "MISSION_STAGE_CUTSCENE_GRAVEYARD"
			BREAK
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
				RETURN "MISSION_STAGE_GRAVEYARD_SHOOTOUT"
			BREAK
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
				RETURN "MISSION_STAGE_CUTSCENE_KIDNAPPED"
			BREAK
			CASE MISSION_STAGE_FLY_HOME
				RETURN "MISSION_STAGE_FLY_HOME"
			BREAK
			CASE MISSION_STAGE_END
				RETURN "MISSION_STAGE_END"
			BREAK
		ENDSWITCH
		
		RETURN "INCORRECT_MISSION_STAGE"
	
	ENDFUNC
	
	FUNC STRING GET_MISSION_FAIL_NAME_FOR_DEBUG(MISSION_FAILS eFailReason)
	
		SWITCH eFailReason
			CASE MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE
				RETURN "MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE"
			BREAK
			CASE MISSION_FAIL_MICHAELS_CAR_DEAD
				RETURN "MISSION_FAIL_MICHAELS_CAR_DEAD"
			BREAK
			CASE MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND
				RETURN "MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND"
			BREAK
			CASE MISSION_FAIL_GRAVEYARD_ABANDONED
				RETURN "MISSION_FAIL_GRAVEYARD_ABANDONED"
			BREAK
			CASE MISSION_FAIL_GRAVEYARD_NOT_REACHED							
				RETURN "MISSION_FAIL_GRAVEYARD_NOT_REACHED"
			BREAK
			CASE MISSION_FAIL_GRAVE_NOT_REACHED
				RETURN "MISSION_FAIL_GRAVE_NOT_REACHED"
			BREAK
			CASE MISSION_FAIL_TREVORS_CAR_UNDRIVABLE						
				RETURN "MISSION_FAIL_TREVORS_CAR_UNDRIVABLE"
			BREAK
			CASE MISSION_FAIL_TREVORS_CAR_DEAD								
				RETURN "MISSION_FAIL_TREVORS_CAR_DEAD"
			BREAK
			CASE MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND						
				RETURN "MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND"
			BREAK
			CASE MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE						
				RETURN "MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE"
			BREAK
			CASE MISSION_FAIL_TREVORS_PLANE_DEAD							
				RETURN "MISSION_FAIL_TREVORS_PLANE_DEAD"
			BREAK
			CASE MISSION_FAIL_MICHAEL_DEAD									
				RETURN "MISSION_FAIL_MICHAEL_DEAD"
			BREAK
			CASE MISSION_FAIL_TREVOR_DEAD									
				RETURN "MISSION_FAIL_TREVOR_DEAD"
			BREAK
			CASE MISSION_FAIL_FORCE_FAIL									
				RETURN "MISSION_FAIL_FORCE_FAIL"
			BREAK
		ENDSWITCH
	
		RETURN "INCORRECT_MISSION_FAIL"
	ENDFUNC
	
	PROC CREATE_DEBUG_WIDGETS()
	
		IF NOT DOES_WIDGET_GROUP_EXIST(Michael1Widgets)
		
			Michael1Widgets = START_WIDGET_GROUP("Mission: Michael1")
			
				ADD_WIDGET_BOOL("Print debug output", bPrintDebugOutput)
			
				ADD_WIDGET_BOOL("Toggle snow", bSnowToggle)
				
				START_WIDGET_GROUP("Focus push")
					ADD_WIDGET_BOOL("bResetGameplayHint", bResetGameplayHint)
					ADD_WIDGET_FLOAT_SLIDER("fFocusPushHintFOV", fFocusPushHintFOV, 1.0, 60.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("fFocusPushSideOffset", fFocusPushSideOffset, -10.0, 10.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fFocusPushVerticalOffset", fFocusPushVerticalOffset, -10.0, 10.0, 0.1) 
					ADD_WIDGET_FLOAT_SLIDER("fFocusPushFollowDistanceScalar", fFocusPushFollowDistanceScalar, -10.0, 10.0, 0.1)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Shovel PTFX")
					ADD_WIDGET_FLOAT_SLIDER("fParticleFXTriggerPhase", fParticleFXTriggerPhase, 0.0, 1.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vParticleFXPosition", vParticleFXPosition, -10.0, 10.0, 1.0)
					ADD_WIDGET_VECTOR_SLIDER("vParticleFXRotation", vParticleFXRotation, -10.0, 10.0, 1.0)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_FLOAT_SLIDER("fPlaneRecordingSkipTime", fPlaneRecordingSkipTime, 0.0, 10000.0, 1.0)
				ADD_WIDGET_VECTOR_SLIDER("Flight global offset", vGlobalFlightOffset, -1000, 1000, 1.0)
				ADD_WIDGET_VECTOR_SLIDER("Flight plane offset", vLocalPlaneOffset, -1000, 1000, 1.0)
				ADD_WIDGET_VECTOR_SLIDER("vFirstTrainPosition", vFirstTrainPosition, -10000, 10000, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("fFirstTrainSpeed", fFirstTrainSpeed, 0.0, 20.0, 1.0)
				
				START_WIDGET_GROUP("Flight locate settings")
				
					ADD_WIDGET_VECTOR_SLIDER("Position", sLocateFlight.vPosition, 0.0, 10000.0, 10.0)
					
					ADD_WIDGET_VECTOR_SLIDER("Size", sLocateFlight.vSize, 0.0, 5000.0, 1.0)
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Vehicle Damage")
				
					ADD_WIDGET_BOOL("Enable widget", bEnableApplyForceWidget)
				
					ADD_WIDGET_BOOL("Apply force", bApplyForceToggle)
				
					ADD_WIDGET_VECTOR_SLIDER("Force", vApplyForceWidgetForce, 0.0, 100.0, 1.0)
					
					ADD_WIDGET_VECTOR_SLIDER("Offset", vApplyForceWidgetOffset, -10.0, 10, 0.1)
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Flight cutscene")
				
					ADD_WIDGET_INT_SLIDER("iInterpTime", iInterpTime, 0, 10000, 1)
					ADD_WIDGET_INT_SLIDER("iCutsceneTime", iCutsceneTime, 0, 10000, 1)
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Arival cutscenes")
				
					ADD_WIDGET_INT_SLIDER("iCameraInterpolationTimer", iCameraInterpolationTimer, 0, 5000, 1)
					ADD_WIDGET_FLOAT_SLIDER("fFirstRecordingEndTime", fFirstRecordingEndTime, 0, 20000.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("fSecondRecordingStartTime", fSecondRecordingStartTime, 0, 20000.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("fSecondRecordingEndTime", fSecondRecordingEndTime, 0, 20000.0, 1.0)
				
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_INT_SLIDER("iFirstPersonFXTime", iFirstPersonFXTime, 0, 10000, 100)
				
			STOP_WIDGET_GROUP()
			
			SET_LOCATES_HEADER_WIDGET_GROUP(Michael1Widgets)
			
		ENDIF
	
	ENDPROC
	
	PROC DELETE_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(Michael1Widgets)
			DELETE_WIDGET_GROUP(Michael1Widgets)
		ENDIF
	ENDPROC
	
	PROC CREATE_DEBUG_MISSION_STAGE_MENU()
	
		SkipMenu[0].sTxtLabel		= "CUT - INTRO - MIC_1_INT"
		SkipMenu[1].sTxtLabel		= "GET TO AIRPORT"
		SkipMenu[2].sTxtLabel		= "FLY TO NORTH YANKTON"
		SkipMenu[3].sTxtLabel		= "CUT - AIRPORT - MIC_1_MCS_1"
		SkipMenu[4].sTxtLabel		= "GET TO GRAVEYARD"
		SkipMenu[5].sTxtLabel		= "GET TO GRAVE"
		SkipMenu[6].sTxtLabel		= "CUT - GRAVEYARD - MIC_1_MCS_2"
		SkipMenu[7].sTxtLabel		= "GRAVEYARD SHOOTOUT"
		SkipMenu[8].sTxtLabel		= "CUT - KIDNAPPED - MIC_1_MCS_3"
		SkipMenu[9].sTxtLabel		= "FLY HOME"
		SkipMenu[10].sTxtLabel		= "END"
		
	ENDPROC

	PROC RUN_DEBUG_MISSION_STAGE_MENU(INT &iSelectedStage, MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenu, iSelectedStage, ENUM_TO_INT(eStage), FALSE, "MICHAEL 1")
		
			DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			
			DO_SAFE_STOP_CUTSCENE()
				
			CLEAR_PRINTS()
		
			CLEAR_HELP()
			
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped invincible for debug skip.")
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			RUN_MISSION_STAGE_CLEANUP(eStage)
			
			CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
			
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			
			CLEAR_TRIGGERED_LABELS()
			
			eStage = INT_TO_ENUM(MISSION_STAGES, iSelectedStage)
			
			IF ( eStage = MISSION_STAGE_CUTSCENE_GRAVEYARD )
				SET_CUTSCENE_TRIGGER_FLAG_FOR_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), bCutsceneTriggeredByMichael, bCutsceneTriggeredByTrevor)
			ENDIF
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via mission stage menu.")
			
			bResetStage = FALSE
			
			bSkipActive = TRUE
			
			RESET_MISSION_FLAGS()
		
		ENDIF
	
	ENDPROC
	
	PROC RUN_DEBUG_WIDGETS(MISSION_STAGES eStage)

		INT i = 0

		IF ( bEnableApplyForceWidget = TRUE )
		
			ENTITY_INDEX entityFocus = GET_FOCUS_ENTITY_INDEX()
			
			IF NOT IS_ENTITY_DEAD(entityFocus)
			AND IS_ENTITY_A_VEHICLE(entityFocus)
			
				VEHICLE_INDEX vehicleFocus = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityFocus)
				
				IF IS_VEHICLE_DRIVEABLE(vehicleFocus)
				
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleFocus, vApplyForceWidgetOffset), 0.125, 255, 0, 0)
					
					IF ( bApplyForceToggle = TRUE )
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicleFocus)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehicleFocus)
						ENDIF
						
						APPLY_FORCE_TO_ENTITY(vehicleFocus, APPLY_TYPE_IMPULSE, vApplyForceWidgetForce, vApplyForceWidgetOffset, 0, TRUE, TRUE, TRUE)
						
						bApplyForceToggle = FALSE
					
					ENDIF
					
				ENDIF
				
			ENDIF

		ENDIF
		
		IF ( bPrologueMapActive = TRUE )
			
			//control snow toggle
			IF ( bSnowToggle = TRUE )
			
				IF bSnowOn = FALSE
					SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")	//gives clear sky
					UNLOAD_ALL_CLOUD_HATS()
					LOAD_CLOUD_HAT("Snowy 01")
					bSnowOn = TRUE
				
				ELIF bSnowOn = TRUE
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					CLEAR_WEATHER_TYPE_PERSIST()
					UNLOAD_ALL_CLOUD_HATS()
					bSnowOn = FALSE
				ENDIF
				
				bSnowToggle = FALSE
			
			ENDIF
		
		ENDIF

		SWITCH eStage

			CASE MISSION_STAGE_GET_TO_GRAVEYARD
			CASE MISSION_STAGE_GET_TO_GRAVE
			CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
				
				IF ( bDrawDebugLinesAndSpheres = TRUE )
				
					FOR i = 0 TO ( COUNT_OF(sRoadNodes) - 1 )
						IF ( sRoadNodes[i].bCurrent = TRUE )
							DRAW_DEBUG_SPHERE(sRoadNodes[i].vPosition, 1.0, 0, 255, 0)
						ELIF ( sRoadNodes[i].bVisited = TRUE )
							DRAW_DEBUG_SPHERE(sRoadNodes[i].vPosition, 1.0, 255, 0, 0)
						ELSE
							DRAW_DEBUG_SPHERE(sRoadNodes[i].vPosition, 1.0, 0, 0, 255)
						ENDIF
						DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(i), << sRoadNodes[i].vPosition.x, sRoadNodes[i].vPosition.y, sRoadNodes[i].vPosition.z + 1.5>>) 
					ENDFOR
					
					DRAW_DEBUG_SPHERE(vStreamTriggerPosition1, 0.5, 255, 255, 255)
					DRAW_DEBUG_TEXT("STREAM 1", << vStreamTriggerPosition1.x, vStreamTriggerPosition1.y, vStreamTriggerPosition1.z + 1.5>>)
					
					DRAW_DEBUG_SPHERE(vStreamTriggerPosition2, 0.5, 255, 255, 255)
					DRAW_DEBUG_TEXT("STREAM 2", << vStreamTriggerPosition2.x, vStreamTriggerPosition2.y, vStreamTriggerPosition2.z + 1.5>>)
					
					DRAW_DEBUG_SPHERE(vStreamTriggerPosition3, 0.5, 255, 255, 255)
					DRAW_DEBUG_TEXT("STREAM 3", << vStreamTriggerPosition3.x, vStreamTriggerPosition3.y, vStreamTriggerPosition3.z + 1.5>>)
					
					IF ( bDrawDebugCoverpoints = TRUE )
					
								
						FOR i = 0 TO ( COUNT_OF(csCoverpoints) - 1 )
						
							IF ( csCoverpoints[i].bActive = TRUE )
						
								//draw non stationary coverpoints
								IF ( csCoverpoints[i].bStationary = FALSE )
						
									DRAW_DEBUG_SPHERE(csCoverpoints[i].vPosition, 0.25, 0, 255, 0)
								
								//draw stationary coverpoints
								ELIF ( csCoverpoints[i].bStationary = TRUE )
								
									DRAW_DEBUG_SPHERE(csCoverpoints[i].vPosition, 0.25, 0, 255, 255)
								
								ENDIF
								
								DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(i), << csCoverpoints[i].vPosition.x, csCoverpoints[i].vPosition.y, csCoverpoints[i].vPosition.z + 0.25>>) 
									
								IF ( csCoverpoints[i].bTaken = TRUE )
								
									DRAW_DEBUG_TEXT("TAKEN", << csCoverpoints[i].vPosition.x, csCoverpoints[i].vPosition.y, csCoverpoints[i].vPosition.z + 0.55>>) 
								
								ENDIF
								
							ELSE
							
								DRAW_DEBUG_SPHERE(csCoverpoints[i].vPosition, 0.25, 255, 0, 0)
								
								DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(i), << csCoverpoints[i].vPosition.x, csCoverpoints[i].vPosition.y, csCoverpoints[i].vPosition.z + 0.25>>)
							
							ENDIF
						
						ENDFOR
									
						FOR i = 0 TO ( COUNT_OF(FraggableTombstones) - 1 )
							
							DRAW_DEBUG_SPHERE(FraggableTombstones[i].vPosition, 1.25, 255, 255, 0, 64)
							
							DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(i), << FraggableTombstones[i].vPosition.x, FraggableTombstones[i].vPosition.y, FraggableTombstones[i].vPosition.z + 0.5>>)

						ENDFOR
						
					ENDIF
					
				ENDIF
				
			BREAK
		
		ENDSWITCH
		
	ENDPROC
	
	PROC RUN_DEBUG_PASS_AND_FAIL_CHECK(MISSION_STAGES &eStage, MISSION_FAILS &eFailReason)
	
		IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
			
			//force player to be Trevor on s-pass, since mission can be normally only completed as Trevor
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
			
				IF ( bMichaelsWeaponsStored = FALSE )
					STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID()).")
					#ENDIF
					REMOVE_PED_WEAPONS_LEAVING_ONE(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
					bMichaelsWeaponsStored = TRUE
				ENDIF
			
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WAIT(0)
				ENDWHILE
			ENDIF

			eStage = MISSION_STAGE_PASSED
	    ENDIF
		
	    IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
			eStage = MISSION_STAGE_FAILED
			eFailReason = MISSION_FAIL_FORCE_FAIL
	    ENDIF
	
	ENDPROC
		
	PROC RUN_DEBUG_SKIPS(MISSION_STAGES &eStage, BOOL &bSkipActive, BOOL &bResetStage)
	
		INT iCurrentStage = ENUM_TO_INT(eStage)
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
			IF eStage <> MISSION_STAGE_END
		
				DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped invincible for debug j-skip.")
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP(eStage)
				
				iCurrentStage++
				
				eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
				
				//If playing as Trevor, J-skip on get to airport should take you to the fly plane stage.
				IF eStage = MISSION_STAGE_FLY_TO_NORTH_YANKTON
					
					IF ( GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR )
						iCurrentStage++
						eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
					ENDIF
				
				ENDIF
				
				IF ( eStage = MISSION_STAGE_CUTSCENE_GRAVEYARD )
					SET_CUTSCENE_TRIGGER_FLAG_FOR_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), bCutsceneTriggeredByMichael, bCutsceneTriggeredByTrevor)
				ENDIF
				
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via J skip.")
				
				bResetStage = FALSE
				
				bSkipActive = TRUE
				
				RESET_MISSION_FLAGS()
				
			ENDIF
			
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
			IF eStage <> MISSION_STAGE_CUTSCENE_INTRO
		
				DO_SAFE_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				
				DO_SAFE_STOP_CUTSCENE()
				
				CLEAR_PRINTS()
			
				CLEAR_HELP()
				
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting player ped invincible for debug p-skip.")
				
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				RUN_MISSION_STAGE_CLEANUP(eStage)
				
				//handle normal p-skip
				IF ( bResetStage = FALSE )
				
					iCurrentStage--
				
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
					
					IF eStage = MISSION_STAGE_CUTSCENE_AIRPORT
						IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR )
							iCurrentStage--
							eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
						ENDIF
					ENDIF
					
					IF eStage = MISSION_STAGE_FLY_TO_NORTH_YANKTON
						IF ( GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR )
							iCurrentStage--
							eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)
						ENDIF
					ENDIF

					IF ( eStage = MISSION_STAGE_CUTSCENE_GRAVEYARD )
						SET_CUTSCENE_TRIGGER_FLAG_FOR_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM(), bCutsceneTriggeredByMichael, bCutsceneTriggeredByTrevor)
					ENDIF
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage changed to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " via P skip.")
				
				//handle p-skip stage reset
				ELIF ( bResetStage = TRUE )
				
					eStage = INT_TO_ENUM(MISSION_STAGES, iCurrentStage)			
					
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Current mission stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), " reset via P skip.")
				
				ENDIF

				bResetStage = FALSE
				
				bSkipActive = TRUE
				
				RESET_MISSION_FLAGS()
				
			ENDIF
			
		ENDIF
	
	ENDPROC
	
	FUNC STRING GET_ENEMY_STATE_NAME_FOR_DEBUG(ENEMY_STATES eState)
	
		SWITCH eState
		
			CASE ENEMY_STATE_IDLE
				RETURN "ENEMY_STATE_IDLE"
			BREAK
			CASE ENEMY_STATE_SCRIPTED_ENTRY
				RETURN "ENEMY_STATE_SCRIPTED_ENTRY"
			BREAK
			CASE ENEMY_STATE_PATROLLING
				RETURN "ENEMY_STATE_PATROLLING"
			BREAK
			CASE ENEMY_STATE_INVESTIGATING
				RETURN "ENEMY_STATE_INVESTIGATING"
			BREAK
			CASE ENEMY_STATE_MOVING_TO_LOCATION
				RETURN "ENEMY_STATE_MOVING_TO_LOCATION"
			BREAK
			CASE ENEMY_STATE_STANDING_AT_LOCATION
				RETURN "ENEMY_STATE_STANDING_AT_LOCATION"
			BREAK
			CASE ENEMY_STATE_PERFORMING_ENEMY_ACTION
				RETURN "ENEMY_STATE_PERFORMING_ENEMY_ACTION"
			BREAK
			CASE ENEMY_STATE_WAITING_FOR_TARGET
				RETURN "ENEMY_STATE_WAITING_FOR_TARGET"
			BREAK
			CASE ENEMY_STATE_GO_TO_COORD
				RETURN "ENEMY_STATE_GO_TO_COORD"
			BREAK
			CASE ENEMY_STATE_FOLLOW_WAYPOINT_RECORDING
				RETURN "ENEMY_STATE_FOLLOW_WAYPOINT_RECORDING"
			BREAK
			CASE ENEMY_STATE_SNIPING
				RETURN "ENEMY_STATE_SNIPING"
			BREAK
			CASE ENEMY_STATE_COMBAT
				RETURN "ENEMY_STATE_COMBAT"
			BREAK
			
		ENDSWITCH
		
		RETURN "INVALID_ENEMY_STATE"
	
	ENDFUNC
	
	FUNC STRING GET_DETECTED_LOCATION_NAME_FOR_DEBUG(DETECTED_LOCATIONS eLocation)
	
		SWITCH eLocation
		
			CASE DL_NOT_DETECTED
				RETURN "DL_NOT_DETECTED"
			BREAK
			CASE DL_START_AREA
				RETURN "DL_START_AREA"
			BREAK
			CASE DL_MIDDLE_AREA_01
				RETURN "DL_MIDDLE_AREA_01"
			BREAK
			CASE DL_MIDDLE_AREA_02
				RETURN "DL_MIDDLE_AREA_02"
			BREAK
			CASE DL_CHURCH_AREA
				RETURN "DL_CHURCH_AREA"
			BREAK
			CASE DL_PARKING_LOT
				RETURN "DL_PARKING_LOT"
			BREAK
			
		ENDSWITCH
		
		RETURN "INVALID_SPOTTED_LOCATION"
	
	ENDFUNC
	
	PROC RUN_DEBUG_INFORMATION_DISPLAY()
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			IF ( bDrawDebugLinesAndSpheres = FALSE)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				bDrawDebugLinesAndSpheres = TRUE
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				bDrawDebugLinesAndSpheres = FALSE
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			IF ( bDrawDebugStates = FALSE)
				bDrawDebugStates = TRUE
			ELSE
				bDrawDebugStates = FALSE
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			IF ( bDrawDebugCoverpoints = FALSE)
				bDrawDebugCoverpoints = TRUE
			ELSE
				bDrawDebugCoverpoints = FALSE
			ENDIF
		ENDIF
	
	ENDPROC

#ENDIF
//|===================== END DEBUG VARIABLES, PROCEDURES & FUNCTIONS =====================|

//|======================== CAR RECORDING PROCEDURES & FUNCTIONS =========================|

//Author: Ross Wallace
//PURPOSE: Returns the percentage progress of a car recording...
FUNC FLOAT GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(VEHICLE_INDEX VehicleIndex #IF IS_DEBUG_BUILD, BOOL bPrintRecordingTime = FALSE #ENDIF)

	IF DOES_ENTITY_EXIST(VehicleIndex)
	AND IS_VEHICLE_DRIVEABLE(VehicleIndex)
		    
	    IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VehicleIndex)
		
          	RECORDING_ID RecordingID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(VehicleIndex)             
                
            #IF IS_DEBUG_BUILD
                INT iPercentage = ROUND( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(VehicleIndex) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(RecordingID) ) )
				TEXT_LABEL_63 debugName = GET_VEHICLE_RECORDING_NAME(RecordingID)
                debugName += iPercentage
                SET_VEHICLE_NAME_DEBUG(VehicleIndex, debugName)
				IF ( bPrintRecordingTime = TRUE )
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Recording ", GET_VEHICLE_RECORDING_NAME(RecordingID), " playback percentage ", 100.0 * ( GET_TIME_POSITION_IN_RECORDING(VehicleIndex) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(RecordingID) ) )
				ENDIF
            #ENDIF
            
			#IF IS_DEBUG_BUILD
				IF ( bPrintRecordingTime = TRUE )
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Recording ", GET_VEHICLE_RECORDING_NAME(RecordingID), " time is ", GET_TIME_POSITION_IN_RECORDING(VehicleIndex), ".")
				ENDIF
			#ENDIF
			
            RETURN ( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(VehicleIndex) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(RecordingID) ) )

	    ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE: Playback not going on for vehicle.")
			#ENDIF
			
	    ENDIF 
		
	ELSE
	
		#IF IS_DEBUG_BUILD
	    	PRINTLN(GET_THIS_SCRIPT_NAME(), ": GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE: Vehicle does not exist or is not driveable.")
		#ENDIF
		
	ENDIF

	RETURN -1.0
      
ENDFUNC

PROC START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE(VEH_STRUCT vsCar, STRING sFileName, FLOAT fPlaybackSpeed, BOOL bPaused = FALSE,
												  FLOAT fTime = 0.0, BOOL bSkipToVehiclePosition = FALSE, BOOL bUseFlags = FALSE,
												  INT iFlags = SWITCH_ON_PLAYER_VEHICLE_IMPACT)

	IF IS_VEHICLE_DRIVEABLE(vsCar.VehicleIndex)
	
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsCar.VehicleIndex)
					
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(vsCar.iRecordingNumber, sFileName)
			
				IF ( bUseFlags = FALSE )
			
					START_PLAYBACK_RECORDED_VEHICLE(vsCar.VehicleIndex, vsCar.iRecordingNumber, sFileName)

				ELIF ( bUseFlags = TRUE )
				
					START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vsCar.VehicleIndex, vsCar.iRecordingNumber, sFileName, iFlags)
					
				ENDIF
				
				IF ( fTime > 0.0 )
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vsCar.VehicleIndex, fTime)
					
				ELIF ( bSkipToVehiclePosition = TRUE )
				
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vsCar.VehicleIndex, GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(vsCar.VehicleIndex), vsCar.iRecordingNumber, sFileName, 16))
					
				ENDIF
				
				SET_PLAYBACK_SPEED(vsCar.VehicleIndex, fPlaybackSpeed)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Vehicle recording ", sFileName, " number ", vsCar.iRecordingNumber, " for vehicle ", GET_MODEL_NAME_FOR_DEBUG(vsCar.ModelName), " started with playback speed ", fPlaybackSpeed, ".")
				#ENDIF
				
				
				IF ( bPaused = TRUE )
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vsCar.VehicleIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Vehicle recording ", sFileName, " number ", vsCar.iRecordingNumber, " for vehicle ", GET_MODEL_NAME_FOR_DEBUG(vsCar.ModelName), " paused.")
					#ENDIF
				ENDIF
				
			ELSE
		
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Vehicle recording number ", vsCar.iRecordingNumber, " for vehicle ", GET_MODEL_NAME_FOR_DEBUG(vsCar.ModelName), " is not loaded.")
				#ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC

//|====================== END CAR RECORDING PROCEDURES & FUNCTIONS =======================|

//|==================================== PROCEDURES =======================================|

/// PURPOSE:
///    Sets mission stage enum for replay.
/// PARAMS:
///    eStage - Mission stage enum variable.
///    iValue - Mid-mission replay value returned by the script when replaying the mission.
PROC SET_MISSION_STAGE_FOR_REPLAY(MISSION_STAGES &eStage, INT iValue)

	IF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_AIRPORT)
	
		eStage = MISSION_STAGE_GET_TO_AIRPORT
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_FLY_TO_NORTH_YANKTON)
	
		eStage = MISSION_STAGE_FLY_TO_NORTH_YANKTON
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_GRAVEYARD)
	
		eStage = MISSION_STAGE_GET_TO_GRAVEYARD
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_GRAVE)
	
		eStage = MISSION_STAGE_GET_TO_GRAVE
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_GRAVEYARD_SHOOTOUT)
	
		eStage = MISSION_STAGE_GRAVEYARD_SHOOTOUT
		
	ELIF iValue = ENUM_TO_INT(MID_MISSION_STAGE_FLY_HOME)
	
		eStage = MISSION_STAGE_FLY_HOME

	ENDIF
	
	bStageReplayInProgress = TRUE
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission is being replayed at stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

/// PURPOSE:
///    Initialise arrays for peds, vehicles, objects etc for a specified stage.
/// PARAMS:
///    eStage - Mission stage to initialise arrays for.
PROC INITIALISE_ARRAYS_FOR_MISSION_STAGE(MISSION_STAGES eStage)

	//Michael's ped details, identical for every mission stage
	psMichael.ModelName 		= GET_PLAYER_PED_MODEL(CHAR_MICHAEL) 

	//Trevor's ped details, identical for every mission stage
	psTrevor.ModelName 			= GET_PLAYER_PED_MODEL(CHAR_TREVOR) 

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
	Gates[0].eHashEnum	= GRAVEYARD_GATE_WEST_L
	Gates[0].vPosition	= <<3293.6226, -4611.3037, 116.7559>>
	Gates[0].ModelName 	= prop_cs_gravyard_gate_l
	
	Gates[1].eHashEnum	= GRAVEYARD_GATE_WEST_R
	Gates[1].vPosition 	= <<3293.2231, -4615.0601, 116.7540>>
	Gates[1].ModelName 	= prop_cs_gravyard_gate_r
	
	Gates[2].eHashEnum	= GRAVEYARD_GATE_EAST_L
	Gates[2].vPosition 	= <<3239.1865, -4594.8237, 117.7705>>
	Gates[2].ModelName 	= prop_cs_gravyard_gate_l
	
	Gates[3].eHashEnum	= GRAVEYARD_GATE_EAST_R
	Gates[3].vPosition 	= <<3239.9766, -4591.1299, 117.7685>>
	Gates[3].ModelName 	= prop_cs_gravyard_gate_r
	
	Gates[4].eHashEnum	= GRAVEYARD_GATE_SOUTH_L
	Gates[4].vPosition 	= <<3262.5972, -4561.6587, 119.0779>>
	Gates[4].ModelName 	= prop_cs_gravyard_gate_l
	
	Gates[5].eHashEnum	= GRAVEYARD_GATE_SOUTH_R
	Gates[5].vPosition 	= <<3266.3745, -4561.6631, 119.0760>>
	Gates[5].ModelName 	= prop_cs_gravyard_gate_r

	SWITCH eStage
	
		CASE MISSION_STAGE_CUTSCENE_INTRO

			psTrevor.vPosition					= <<-802.4639, 173.0358, 71.8447>>
			psTrevor.fHeading					= 288.8440
			psTrevor.vDestination				= << 1733.6469, 3306.0200, 40.2261 >>
			
			psMichael.vPosition 				= <<-802.4639, 173.0358, 71.8447>>
			psMichael.fHeading 					= 288.8440
			psMichael.vDestination				= << -1033.20, -2731.17, 19.05 >>
			
			vsMichaelsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			vsMichaelsCar.vPosition				= <<-829.2751, 176.4524, 69.8634>>
			vsMichaelsCar.fHeading				= 151.8630
			vsMichaelsCar.iRecordingNumber		= 003
			
			vsTrevorsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
			vsTrevorsCar.vPosition				= << -825.8718, 157.3143, 69.4619 >>
			vsTrevorsCar.fHeading				= 90.0
			vsTrevorsCar.iRecordingNumber		= 001
			
			vsTrevorsPlane.ModelName			= CUBAN800
			vsTrevorsPlane.vPosition			= <<1731.8021, 3308.9109, 40.2237>>
			vsTrevorsPlane.fHeading				= 194.8553
			vsTrevorsPlane.iRecordingNumber		= 002
			
			sLocateFlight.vPosition				= <<-3841.2847, 5020.6064, 174.8651>>//<< 4390.0, 8850.0, 900.0 >>
			sLocateFlight.vSize					= << 0.5, 0.5, 0.5 >>
			
		BREAK
		
		CASE MISSION_STAGE_GET_TO_AIRPORT

			psTrevor.vPosition					= <<-818.0981, 158.3042, 69.7868>>
			psTrevor.fHeading					= 109.1361
			psTrevor.vDestination				= << 1733.6469, 3306.0200, 40.2261 >>
			
			psMichael.vPosition 				= <<-813.9473, 179.2056, 71.1592>>
			psMichael.fHeading 					= 111.3824
			psMichael.vDestination				= << -1033.20, -2731.17, 19.05 >>
			
			vsMichaelsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			vsMichaelsCar.vPosition				= <<-829.2751, 176.4524, 69.8634>>
			vsMichaelsCar.fHeading				= 151.8630
			vsMichaelsCar.iRecordingNumber		= 003
			
			vsTrevorsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
			vsTrevorsCar.vPosition				= << -825.8718, 157.3143, 69.4619 >>
			vsTrevorsCar.fHeading				= 90.0
			vsTrevorsCar.iRecordingNumber		= 001
			
			vsTrevorsPlane.ModelName			= CUBAN800
			vsTrevorsPlane.vPosition			= <<1731.8021, 3308.9109, 40.2237>>
			vsTrevorsPlane.fHeading				= 194.8553
			vsTrevorsPlane.iRecordingNumber		= 002
			
			sLocateFlight.vPosition				= <<-3841.2847, 5020.6064, 174.8651>>//<< 4390.0, 8850.0, 900.0 >>
			sLocateFlight.vSize					= << 0.5, 0.5, 0.5 >>
			
			osGolfBall.vPosition				= sLocateFlight.vPosition
			osGolfBall.vRotation				= << 0.0, 0.0, 0.0 >>
			osGolfBall.ModelName				= PROP_GOLF_BALL
			
		BREAK
		
		CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON

			psTrevor.vPosition					= << 1746.7969, 3291.4446, 40.1072 >>
			psTrevor.fHeading					= 55.8571
			psTrevor.vDestination				= << 1733.6469, 3306.0200, 40.2261 >>
			
			psMichael.vPosition 				= << -826.91, 176.45, 69.97 >> 
			psMichael.fHeading 					= 129.1458
			psMichael.vDestination				= << -1033.20, -2731.17, 19.05 >>
			
			vsMichaelsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			vsMichaelsCar.vPosition				= << -828.4514, 177.0704, 69.9859 >>
			vsMichaelsCar.fHeading				= 147.0378
			vsMichaelsCar.iRecordingNumber		= 003
			
			vsTrevorsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
			vsTrevorsCar.vPosition				= << 1747.3265, 3294.0227, 40.1071 >>
			vsTrevorsCar.fHeading				= 84.1211
			vsTrevorsCar.iRecordingNumber		= 001
			
			vsTrevorsPlane.ModelName			= CUBAN800
			vsTrevorsPlane.vPosition			= <<1731.8021, 3308.9109, 40.2237>>
			vsTrevorsPlane.fHeading				= 194.8553
			vsTrevorsPlane.iRecordingNumber		= 002
			
			sLocateFlight.vPosition				= <<-3841.2847, 5020.6064, 174.8651>>//<< 4390.0, 8850.0, 900.0 >>
			sLocateFlight.vSize					= << 0.5, 0.5, 0.5 >>
			
			osGolfBall.vPosition				= sLocateFlight.vPosition
			osGolfBall.vRotation				= << 0.0, 0.0, 0.0 >>
			osGolfBall.ModelName				= PROP_GOLF_BALL
			
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_AIRPORT

			psMichael.vPosition 				= << -1033.3214, -2727.2915, 19.1813 >> 
			psMichael.fHeading 					= 239.5299
			psMichael.vDestination				= << 0.0, 0.0, 0.0 >>
			
			psTrevor.vPosition 					= << -1012.2794, -2738.5303, 19.1873 >>
			psTrevor.fHeading 					= 329.2714
			psTrevor.vDestination				= << 0.0, 0.0, 0.0 >>
			
			vsMichaelsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			vsMichaelsCar.vPosition 			= << -1033.4814, -2728.8206, 19.1441 >>
			vsMichaelsCar.fHeading				= 239.6801
			
			vsTrevorsPlane.ModelName			= CUBAN800
			vsTrevorsPlane.vPosition			= <<1731.8021, 3308.9109, 40.2237>>
			vsTrevorsPlane.fHeading				= 194.8553
		
		BREAK
		
		CASE MISSION_STAGE_GET_TO_GRAVEYARD

			psMichael.vPosition 				= << 5482.5986, -5125.9849, 77.4370 >>
			psMichael.fHeading 					= 264.8607
			psMichael.vDestination				= << 3216.8445, -4682.9536, 111.7127 >>
		
			psTrevor.vPosition 					= << 3259.5271, -4571.6938, 117.1567 >>
			psTrevor.fHeading 					= 181.5470
			
			vsMichaelsCar.ModelName				= ASEA2
			vsMichaelsCar.vPosition				= << 5575.9858, -5130.2427, 79.5043 >>
			vsMichaelsCar.fHeading				= 86.0572
			vsMichaelsCar.iRecordingNumber		= 005
			
			vsTrevorsCar.ModelName				= MESA2
			vsTrevorsCar.vPosition				= <<3257.9006, -4689.8130, 111.8699>> 
			vsTrevorsCar.fHeading				= 278.2631
			
			tsTrain.iConfiguration				= 9
			tsTrain.vPosition					= vFirstTrainPosition
			tsTrain.ModelName1					= FREIGHT
			tsTrain.ModelName2					= FREIGHTCAR
			tsTrain.ModelName3					= FREIGHTGRAIN
			tsTrain.ModelName4					= S_M_M_TRUCKER_01

		BREAK
		
		CASE MISSION_STAGE_GET_TO_GRAVE

			psMichael.vPosition 				= <<3216.5117, -4688.0327, 111.6721>>
			psMichael.fHeading 					= 268.5004
		
			psTrevor.vPosition 					= << 3259.5271, -4571.6938, 117.1567 >>
			psTrevor.fHeading 					= 181.5470
			
			vsMichaelsCar.ModelName				= ASEA2
			vsMichaelsCar.vPosition				= << 3212.0027, -4689.3232, 111.6762 >>
			vsMichaelsCar.fHeading				= 225.1631
			
			vsTrevorsCar.ModelName				= MESA2
			vsTrevorsCar.vPosition				= <<3257.9006, -4689.8130, 111.8699>> 
			vsTrevorsCar.fHeading				= 278.2631
			
			osPropShovel.vPosition				= <<3259.5710, -4572.7739, 116.1162>>
			osPropShovel.vRotation				= <<-91.0000, -38.0000, 0.0000>>
			osPropShovel.ModelName				= PROP_LD_SHOVEL
			
			osPropPickaxe.vPosition				= <<3259.2451, -4573.0298, 116.8103>>
			osPropPickaxe.vRotation				= <<158.0000, 0.0000, 77.4000>>
			osPropPickaxe.ModelName				= PROP_TOOL_PICKAXE
			
			osPropCoffin.vPosition				= <<3259.7339, -4573.8970, 115.8785>>
			osPropCoffin.vRotation				= <<5.0000, 0.0000, -180.0000>>
			osPropCoffin.ModelName				= PROP_COFFIN_02
		
		BREAK
				
		CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
		
			psBrad.vPosition					= <<3240.7776, -4559.6108, 116.9483>> 
			psBrad.fHeading						= 92.2719
			psBrad.ModelName					= CS_BRADCADAVER

			psMichael.vPosition 				= << 3258.16, -4572.49, 117.16 >> 
			psMichael.fHeading					= 180.0			
			psMichael.vDestination				= << 0.0, 0.0, 0.0 >>
		
			psTrevor.vPosition 					= << 3257.53, -4574.04, 117.16 >>
			psTrevor.fHeading 					= 0.0
			psTrevor.vDestination				= << 0.0, 0.0, 0.0 >>
			
			vsMichaelsCar.ModelName				= ASEA2
			vsMichaelsCar.vPosition				= << 3211.3403, -4688.4854, 111.7328 >>
			vsMichaelsCar.fHeading				= 222.9433
			
			vsTrevorsCar.ModelName				= MESA2
			vsTrevorsCar.vPosition				= <<3257.9006, -4689.8130, 111.8699>> 
			vsTrevorsCar.fHeading				= 278.2631
			
			osPropShovel.vPosition				= <<3259.5710, -4572.7739, 116.1162>>
			osPropShovel.vRotation				= <<-91.0000, -38.0000, 0.0000>>
			osPropShovel.ModelName				= PROP_LD_SHOVEL
			
			osPropPickaxe.vPosition				= <<3259.2451, -4573.0298, 116.8103>>
			osPropPickaxe.vRotation				= <<158.0000, 0.0000, 77.4000>>
			osPropPickaxe.ModelName				= PROP_TOOL_PICKAXE
			
			osPropCoffin.vPosition				= <<3259.7339, -4573.8970, 115.8785>>
			osPropCoffin.vRotation				= <<5.0000, 0.0000, -180.0000>>
			osPropCoffin.ModelName				= PROP_COFFIN_02
			
		BREAK
		
		CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
		
			psBrad.vPosition					= <<3240.7776, -4559.6108, 116.9483>> 
			psBrad.fHeading						= 92.2719
			psBrad.ModelName					= CS_BRADCADAVER

			psMichael.vPosition					= << 3256.4697, -4575.1128, 117.1567 >>
			psMichael.fHeading 					= 273.8974
			psMichael.vDestination 				= << 3248.886230, -4687.962891, 114.841042 >>
			
			vsMichaelsCar.ModelName				= ASEA2
			vsMichaelsCar.vPosition				= << 3211.3403, -4688.4854, 111.7328 >>
			vsMichaelsCar.fHeading				= 222.9433
			
			osPropShovel.vPosition				= <<3259.5710, -4572.7739, 116.1162>>
			osPropShovel.vRotation				= <<-91.0000, -38.0000, 0.0000>>
			osPropShovel.ModelName				= PROP_LD_SHOVEL
			
			osPropPickaxe.vPosition				= <<3259.2451, -4573.0298, 116.8103>>
			osPropPickaxe.vRotation				= <<158.0000, 0.0000, 77.4000>>
			osPropPickaxe.ModelName				= PROP_TOOL_PICKAXE
			
			osPropCoffin.vPosition				= <<3259.7339, -4573.8970, 115.8785>>
			osPropCoffin.vRotation				= <<5.0000, 0.0000, -180.0000>>
			osPropCoffin.ModelName				= PROP_COFFIN_02
			
			vsEnemyVans[0].vPosition			= << 3304.86, -4641.48, 113.08 >>
			vsEnemyVans[0].fHeading				= 27.521
			vsEnemyVans[0].ModelName			= mnEnemyVan
			vsEnemyVans[0].iRecordingNumber 	= 012
						
			vsEnemyVans[1].vPosition			= << 3242.78, -4673.15, 113.18 >>
			vsEnemyVans[1].fHeading				= 53.3085
			vsEnemyVans[1].ModelName			= mnEnemyVan
			vsEnemyVans[1].iRecordingNumber 	= 013
			
			vsEnemyVans[2].vPosition			= << 3259.67, -4690.98, 112.69 >>
			vsEnemyVans[2].fHeading				= 303.13
			vsEnemyVans[2].ModelName			= mnEnemyVan
			
			vsEnemyVans[3].vPosition			= << 3254.34, -4683.82, 112.67 >>
			vsEnemyVans[3].fHeading				= 249.15
			vsEnemyVans[3].ModelName			= mnEnemyVan
			
			vsEnemyVans[4].vPosition			= << 3235.7214, -4835.2661, 110.8135 >>
			vsEnemyVans[4].fHeading				= 84.1679
			vsEnemyVans[4].ModelName			= mnEnemyVan
			vsEnemyVans[4].iRecordingNumber		= 015

			tsTrain.iConfiguration				= 23
			tsTrain.vPosition					= << 3154.38, -4698.16, 111.63 >>
			tsTrain.ModelName1					= FREIGHT
			tsTrain.ModelName2					= FREIGHTCAR
			tsTrain.ModelName3					= FREIGHTCONT1
			tsTrain.ModelName4					= S_M_M_TRUCKER_01
			
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_KIDNAPPED

			psMichael.vPosition 				= << 3217.3970, -4687.5581, 111.7128 >>
			psMichael.fHeading 					= 94.6391
			
			vsMichaelsCar.ModelName				= ASEA2
			vsMichaelsCar.vPosition				= << 3211.3403, -4688.4854, 111.7328 >>
			vsMichaelsCar.fHeading				= 222.9433
	
		BREAK
		
		CASE MISSION_STAGE_FLY_HOME

			psTrevor.vPosition					= <<-4568.2949, 4981.7153, 136.7620>>	//<< 3382.9780, 8102.7705, 21.5856 >>
			psTrevor.fHeading					= 259.5159								//143.8278
		
			vsTrevorsPlane.ModelName			= CUBAN800
			vsTrevorsPlane.vPosition			= <<-4568.2949, 4981.7153, 136.7620>>	//<< 3382.9780, 8102.7705, 21.5856 >>
			vsTrevorsPlane.fHeading				= 259.5159								//143.8278
			vsTrevorsPlane.iRecordingNumber		= 008 //006
			
			vsTrevorsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
			vsTrevorsCar.vPosition				= << 1747.3265, 3294.0227, 40.1071 >>
			vsTrevorsCar.fHeading				= 84.1211
						
		BREAK
		
		CASE MISSION_STAGE_END

			psTrevor.vPosition					= <<1725.0645, 3304.4546, 40.2237>>
			psTrevor.fHeading					= 345.1909							
		
			vsTrevorsPlane.ModelName			= CUBAN800
			vsTrevorsPlane.vPosition			= <<1732.3759, 3306.6619, 40.2237>>
			vsTrevorsPlane.fHeading				= 14.8253							
			
			vsTrevorsCar.ModelName				= GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
			vsTrevorsCar.vPosition				= << 1747.3265, 3294.0227, 40.1071 >>
			vsTrevorsCar.fHeading				= 84.1211
			
		BREAK
	
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initialising arrays for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF

ENDPROC

PROC BUILD_REQUEST_BANK_FOR_MISSION_STAGE(MISSION_STAGES eStage)
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
	SWITCH eStage
	
		CASE MISSION_STAGE_CUTSCENE_INTRO
			//don't request anything here
		BREAK
		
		CASE MISSION_STAGE_GET_TO_AIRPORT
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					//
				BREAK
				CASE CHAR_TREVOR
					IF NOT DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
						ADD_MODEL_REQUEST_TO_ARRAY(vsTrevorsPlane.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF NOT DOES_ENTITY_EXIST(psTrevor.PedIndex)
					ADD_MODEL_REQUEST_TO_ARRAY(psTrevor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF NOT DOES_ENTITY_EXIST(psMichael.PedIndex)
					ADD_MODEL_REQUEST_TO_ARRAY(psTrevor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsTrevorsPlane.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_AIRPORT
			//
		BREAK
		
		CASE MISSION_STAGE_GET_TO_GRAVEYARD
			REQUEST_PTFX_ASSET()
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF NOT DOES_ENTITY_EXIST(psMichael.PedIndex)
					ADD_MODEL_REQUEST_TO_ARRAY(psMichael.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsTrevorsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_CARREC_REQUEST_TO_ARRAY(004, sVehicleRecordingsFile, MissionRecordings, MAX_CAR_RECORDINGS, iRecordingsRequested)
			ADD_CARREC_REQUEST_TO_ARRAY(005, sVehicleRecordingsFile, MissionRecordings, MAX_CAR_RECORDINGS, iRecordingsRequested)
		BREAK
		
		CASE MISSION_STAGE_GET_TO_GRAVE
			REQUEST_PTFX_ASSET()
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsTrevorsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			REQUEST_PTFX_ASSET()
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF NOT DOES_ENTITY_EXIST(psMichael.PedIndex)
					ADD_MODEL_REQUEST_TO_ARRAY(psMichael.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
				ENDIF
			ENDIF
			ADD_MODEL_REQUEST_TO_ARRAY(mnEnemy, MissionModels, MAX_MODELS, iModelsRequested)
			ADD_MODEL_REQUEST_TO_ARRAY(mnEnemyVan, MissionModels, MAX_MODELS, iModelsRequested)
			ADD_CARREC_REQUEST_TO_ARRAY(vsEnemyVans[0].iRecordingNumber, sVehicleRecordingsFile, MissionRecordings, MAX_CAR_RECORDINGS, iRecordingsRequested)
			ADD_CARREC_REQUEST_TO_ARRAY(vsEnemyVans[1].iRecordingNumber, sVehicleRecordingsFile, MissionRecordings, MAX_CAR_RECORDINGS, iRecordingsRequested)
			ADD_CARREC_REQUEST_TO_ARRAY(vsEnemyVans[4].iRecordingNumber, sVehicleRecordingsFile, MissionRecordings, MAX_CAR_RECORDINGS, iRecordingsRequested)
		BREAK
		
		CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
			REQUEST_PTFX_ASSET()
			IF NOT DOES_ENTITY_EXIST(psMichael.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psMichael.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsMichaelsCar.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_FLY_HOME
			IF NOT DOES_ENTITY_EXIST(psTrevor.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psTrevor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsTrevorsPlane.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			ADD_CARREC_REQUEST_TO_ARRAY(vsTrevorsPlane.iRecordingNumber, sVehicleRecordingsFile, MissionRecordings, MAX_CAR_RECORDINGS, iRecordingsRequested)
		BREAK
		
		CASE MISSION_STAGE_END
			IF NOT DOES_ENTITY_EXIST(psTrevor.PedIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(psTrevor.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
				ADD_MODEL_REQUEST_TO_ARRAY(vsTrevorsPlane.ModelName, MissionModels, MAX_MODELS, iModelsRequested)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished building request bank for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
	#ENDIF
	
ENDPROC

FUNC BOOL IS_VEHICLE_STUCK_IN_SNOW_TIMER_UP(VEHICLE_INDEX VehicleIndex, INT &iStuckTimer, INT iStuckTime)

	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF NOT IS_ENTITY_DEAD(VehicleIndex)
		
			MATERIAL_NAMES eLastMaterialName = GET_LAST_MATERIAL_HIT_BY_ENTITY(VehicleIndex)
			
			#IF IS_DEBUG_BUILD
				SWITCH eLastMaterialName
					CASE GENERAL_SNOW_LOOSE
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, "GENERAL_SNOW_LOOSE", 1.0)
					BREAK
					CASE GENERAL_SNOW_COMPACT
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, "GENERAL_SNOW_COMPACT", 1.0)
					BREAK
					CASE GENERAL_SNOW_DEEP
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, "GENERAL_SNOW_DEEP", 1.0)
					BREAK
					CASE GENERAL_SNOW_TARMAC
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, "GENERAL_SNOW_TARMAC", 1.0)
					BREAK
				ENDSWITCH
			#ENDIF
			
			SWITCH eLastMaterialName
				CASE GENERAL_SNOW_LOOSE
				CASE GENERAL_SNOW_COMPACT
				CASE GENERAL_SNOW_DEEP
				CASE GENERAL_SNOW_TARMAC
	
					IF ( NOT IS_VEHICLE_STOPPED(VehicleIndex) AND GET_ENTITY_SPEED(VehicleIndex) < 2.0 )
					OR ( IS_VEHICLE_STOPPED(VehicleIndex) AND ( ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) * 255.0) >=250 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) * 255.0) >=250 ))
						IF ( iStuckTimer = 0 )
							iStuckTimer = GET_GAME_TIMER()
						ELSE
							IF HAS_TIME_PASSED(iStuckTime, iStuckTimer)
								RETURN TRUE
							ENDIF
						ENDIF
					ELSE
						iStuckTimer = 0
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, GET_STRING_FROM_INT(CLAMP_INT(iStuckTime - ( GET_GAME_TIMER() - iStuckTimer ), 0, iStuckTime)), 1.25)
					#ENDIF
				
				BREAK
				
				DEFAULT
					iStuckTimer = 0
				BREAK
				
			ENDSWITCH
		
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_VEHICLE_PERMANENTLY_STUCK_WITH_TIMERS(VEHICLE_INDEX &veh, INT iRoofTime = ROOF_TIME, INT iJammedTime = 30000,
												   INT iHungUpTime = 15000, INT iSideTime = 20000)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, iRoofTime)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_JAMMED, iJammedTime)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_HUNG_UP, iHungUpTime)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, iSideTime)
			RETURN TRUE
		ENDIF 
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for various mission fail conditions. Redirects mission flow to MISSION_STAGE_FAILED when one of fail conditions is true.
/// PARAMS:
///    eStage - Mission stage variable.
///    eFail - Mission fail reason.
PROC RUN_FAIL_CHECKS(MISSION_STAGES &eStage, MISSION_FAILS &eFail)

	IF 	eStage <> MISSION_STAGE_PASSED
	AND eStage <> MISSION_STAGE_FAILED
		
		IF 	NOT IS_CUTSCENE_PLAYING()
		AND NOT IS_SELECTOR_CAM_ACTIVE()
			
			IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				IF ( FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] = TRUE )
					IF IS_VEHICLE_PERMANENTLY_STUCK(vsMichaelsCar.VehicleIndex)
						eFail = MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE
						eStage = MISSION_STAGE_FAILED
					ENDIF
					IF ( eStage = MISSION_STAGE_GET_TO_GRAVEYARD )
						IF IS_VEHICLE_STUCK_IN_SNOW_TIMER_UP(vsMichaelsCar.VehicleIndex, iSnowStuckTimer, 4000)
							eFail = MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE
							eStage = MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] = TRUE )
					IF NOT IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex, TRUE)
						eFail = MISSION_FAIL_MICHAELS_CAR_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] = TRUE )
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						IF ( eStage = MISSION_STAGE_GRAVEYARD_SHOOTOUT )
							IF NOT IS_ENTITY_IN_ANGLED_AREA_LOCATE(PLAYER_PED_ID(), << 3263.05, -4704.67, 104.67 >>,
																   << 3267.14, -4561.37, 132.76 >>, 64.0 #IF IS_DEBUG_BUILD, "CarParkAreaLocate" #ENDIF)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex) > ABANDON_CAR_FAIL_RANGE
									eFail = MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND
									eStage = MISSION_STAGE_FAILED
								ENDIF
							ENDIF
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3165.779785,-4700.698242,113.623360>>, << 16.0, 16.0,3.0 >>)	//train tracks check
							OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3287.108887,-4719.235840,113.776268>>, << 16.0, 16.0,3.0 >>)	//train tracks check
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3257.560059,-4712.962891,111.171898>>, <<3273.731201,-4714.457520,116.705261>>, 18.0)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3229.477539,-4768.596680,110.500343>>, <<3308.396484,-4775.955078,124.718132>>, 100.000000)
								eFail = MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND
								eStage = MISSION_STAGE_FAILED
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_MICHAEL))
								IF GET_DISTANCE_BETWEEN_ENTITIES(GET_PED_INDEX(CHAR_MICHAEL), vsMichaelsCar.VehicleIndex) > ABANDON_CAR_FAIL_RANGE
									eFail = MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND
									eStage = MISSION_STAGE_FAILED
								ENDIF
							ENDIF
						ENDIF
					ENDIF				
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_GRAVEYARD_NOT_REACHED] = TRUE )
					IF ( eStage = MISSION_STAGE_GET_TO_GRAVEYARD )
						IF ( bPlayerDroveOffRoute = TRUE )
							eFail = MISSION_FAIL_GRAVEYARD_NOT_REACHED
							eStage = MISSION_STAGE_FAILED
						ENDIF
						//fix for B*1713614, extra checks if player tries to get further into north yankton town
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3122.884277,-4705.447754,110.703247>>, <<3107.735596,-4895.832520,130.591019>>, 64.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3165.752686,-4756.501465,110.109207>>, <<3167.850830,-4733.836914,119.107994>>, 32.000000)
							eFail = MISSION_FAIL_GRAVEYARD_NOT_REACHED
							eStage = MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_GRAVE_NOT_REACHED] = TRUE )
					IF ( eStage = MISSION_STAGE_GET_TO_GRAVE )
						IF NOT IS_ENTITY_IN_ANGLED_AREA_LOCATE(PLAYER_PED_ID(), << 3263.05, -4704.67, 104.67 >>,
															   << 3267.14, -4561.37, 132.76 >>, 64.0 #IF IS_DEBUG_BUILD, "CarParkAreaLocate" #ENDIF)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vsMichaelsCar.vPosition) > ABANDON_CAR_FAIL_RANGE
								eFail = MISSION_FAIL_GRAVE_NOT_REACHED
								eStage = MISSION_STAGE_FAILED
							ENDIF
						ENDIF
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 3165.78, -4700.70, 113.62 >>, << 16.0, 16.0,3.0 >>)	//train tracks check
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 3287.11, -4719.23, 113.77 >>, << 16.0, 16.0,3.0 >>)	//train tracks check
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 3257.56, -4712.96, 111.17 >>, << 3273.73, -4714.45, 116.70 >>, 18.0)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3229.477539,-4768.596680,110.500343>>, <<3308.396484,-4775.955078,124.718132>>, 100.000000)
							eFail = MISSION_FAIL_GRAVE_NOT_REACHED
							eStage = MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_GRAVEYARD_ABANDONED] = TRUE )
					IF ( eStage = MISSION_STAGE_GRAVEYARD_SHOOTOUT )
						IF NOT IS_ENTITY_IN_LOCATE(PLAYER_PED_ID(), <<3255.412842,-4628.320801,125.917946>>,
												   << 58.0, 78.0, 16.0 >> #IF IS_DEBUG_BUILD, "GraveyardLocate" #ENDIF)
							eFail = MISSION_FAIL_GRAVEYARD_ABANDONED
							eStage = MISSION_STAGE_FAILED
						ENDIF
					ENDIF				
				ENDIF
				
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
				IF ( FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] = TRUE )
					IF IS_VEHICLE_PERMANENTLY_STUCK(vsTrevorsCar.VehicleIndex)
						eFail = MISSION_FAIL_TREVORS_CAR_UNDRIVABLE
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] = TRUE )
					IF NOT IS_VEHICLE_DRIVEABLE(vsTrevorsCar.VehicleIndex, TRUE)
						eFail = MISSION_FAIL_TREVORS_CAR_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				
				IF ( FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = TRUE )
					IF IS_VEHICLE_DRIVEABLE(vsTrevorsCar.VehicleIndex)
						IF GET_DISTANCE_BETWEEN_ENTITIES(GET_PED_INDEX(CHAR_TREVOR), vsTrevorsCar.VehicleIndex) > 100
							eFail = MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND
							eStage = MISSION_STAGE_FAILED
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
				IF ( FailFlags[MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE] = TRUE )
					IF IS_VEHICLE_PERMANENTLY_STUCK_WITH_TIMERS(vsTrevorsPlane.VehicleIndex)
						eFail = MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
				IF ( FailFlags[MISSION_FAIL_TREVORS_PLANE_DEAD] = TRUE )
					IF NOT IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex, TRUE)					
						eFail = MISSION_FAIL_TREVORS_PLANE_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psMichael.PedIndex)
				IF ( FailFlags[MISSION_FAIL_MICHAEL_DEAD] = TRUE )
					IF IS_PED_INJURED(psMichael.PedIndex)
						eFail = MISSION_FAIL_MICHAEL_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
				IF ( FailFlags[MISSION_FAIL_TREVOR_DEAD] = TRUE )
					IF IS_PED_INJURED(psTrevor.PedIndex)
						eFail = MISSION_FAIL_TREVOR_DEAD
						eStage = MISSION_STAGE_FAILED
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets mission as failed and provides mission flow with reason to print on screen.
/// PARAMS:
///    eFailReason - One of MISSION_FAILS values.
PROC SET_MISSION_FAILED_WITH_REASON(MISSION_FAILS eFailReason, BOOL bPlayerInPrologue)   

	SWITCH eFailReason
		CASE MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE						//Michael's car is undriveable
			IF ( bPlayerInPrologue = FALSE )
				MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_MCUND")
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_CUND")
			ENDIF
		BREAK
		CASE MISSION_FAIL_MICHAELS_CAR_DEAD								//Michael's car is destroyed
			IF ( bPlayerInPrologue = FALSE )
				MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_MDEST")
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_GENDEST")
			ENDIF
		BREAK
		CASE MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND						//Michael's car is left behind
			IF ( bPlayerInPrologue = FALSE )
				MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_MCLEFT")
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_CLEFT")
			ENDIF
		BREAK
		CASE MISSION_FAIL_GRAVEYARD_ABANDONED							//Cemetery is abandoned
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_CLEFT")
		BREAK
		CASE MISSION_FAIL_GRAVEYARD_NOT_REACHED							//Cemetery is not reached
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_FTRC")
		BREAK
		CASE MISSION_FAIL_GRAVE_NOT_REACHED								//Grave is not reached
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_FTRG")
		BREAK
		CASE MISSION_FAIL_TREVORS_CAR_UNDRIVABLE						//Trevor's car is undriveable
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_TCUND")
		BREAK
		CASE MISSION_FAIL_TREVORS_CAR_DEAD								//Trevor's car is destroyed
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_TDEST")
		BREAK
		CASE MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND						//Trevor's car left behind
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_TCLEFT")
		BREAK
		CASE MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE						//Trevor's plane is undriveable
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_PUND")
		BREAK
		CASE MISSION_FAIL_TREVORS_PLANE_DEAD							//Trevor's plane is destroyed
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_PDEAD")
		BREAK
		CASE MISSION_FAIL_MICHAEL_DEAD									//Michael is dead
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_MDIED")
		BREAK
		CASE MISSION_FAIL_TREVOR_DEAD									//Trevor is dead
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("CMN_TDIED")
		BREAK
		CASE MISSION_FAIL_FORCE_FAIL									//Debug forced fail
			MISSION_FLOW_MISSION_FAILED_WITH_REASON("MCH1_FAIL")
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission failed with reason ", GET_MISSION_FAIL_NAME_FOR_DEBUG(eFailReason), ".")
	#ENDIF
	
ENDPROC

PROC SET_MISSION_PED_PROPERTIES(PED_INDEX PedIndex, REL_GROUP_HASH relGroupHash, BOOL bCanFlyThroughWindscreen, BOOL bKeepRelGroupOnCleanup,
								BOOL bCanBeTargetted, BOOL bIsEnemy)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_WillFlyThroughWindscreen, bCanFlyThroughWindscreen)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, bKeepRelGroupOnCleanup)
			SET_PED_CAN_BE_TARGETTED(PedIndex, bCanBeTargetted)
			SET_PED_AS_ENEMY(PedIndex, bIsEnemy)
			
			IF relGroupHash != RELGROUPHASH_NO_RELATIONSHIP
			
				SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, relGroupHash)
			ENDIF
		
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates a ped or player ped to be used by player and returns TRUE if such pad was created successfully.
/// PARAMS:
///    psPed - PED_STRUCT containg ped details, like PED_INDEX, blip, coordinates, heading, model etc.
///    bPlayerPed - Boolean indicating if a player ped or NPC ped should be created.
///    relGroupHash - Relationship group hash for the created ped.
///    bCreateBlip - Boolean indicating if a blip for this ped should be created.
///    eCharacter - Enum specifying character from story characters list. Use NO_CHARACTER for characters not stored in that list and provide a MODEL_NAMES in PED_STRUCT.
///    bCanFlyThroughWindscreen - Sets if ped can fly through windscreen when car crashes.
///    bCanBeTargetted - Sets if ped can be targetted by player.
///    bIsEnemy - Sets if ped is considered an enemy.
///    VehicleIndex - Specify a vehicle index if ped should be created inside a vehicle. IF not use NULL.
///    eVehicleSeat - Vehicle seat enum for peds created in vehicles.
/// RETURNS:
///    TRUE if ped was created successfully, FALSE if otherwise.
FUNC BOOL HAS_MISSION_PED_BEEN_CREATED(PED_STRUCT &psPed, BOOL bPlayerPed, REL_GROUP_HASH relGroupHash, BOOL bCreateBlip, enumCharacterList eCharacter,
									   BOOL bCanFlyThroughWindscreen = FALSE, BOOL bCanBeTargetted = FALSE, BOOL bIsEnemy = FALSE, VEHICLE_INDEX VehicleIndex = NULL,
									   VEHICLE_SEAT eVehicleSeat = VS_DRIVER, BOOL bKeepRelGroupOnCleanup = TRUE #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	IF ( bPlayerPed = FALSE )	//create non player ped
		
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
		
			REQUEST_MODEL(psPed.ModelName)
			
			IF HAS_MODEL_LOADED(psPed.ModelName)
		
				IF ( VehicleIndex = NULL )	//create ped outside of vehicle
				
					IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT if no character was specified
					
						psPed.PedIndex = CREATE_PED(PEDTYPE_MISSION, psPed.ModelName, psPed.vPosition, psPed.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
					
					ELSE								//use character name to define the ped MODEL_NAMES
					
						IF CREATE_NPC_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
						ENDIF
					
					ENDIF
					
					IF NOT IS_PED_INJURED(psPed.PedIndex)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
												   
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
					
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF
					#ENDIF
					
				ELIF ( VehicleIndex != NULL )	//create ped in a vehicle
				
					IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
						IF ( eCharacter = NO_CHARACTER )	//use MODEL_NAMES stored in the PED_STRUCT
					
							psPed.PedIndex = CREATE_PED_INSIDE_VEHICLE(VehicleIndex, PEDTYPE_MISSION, psPed.ModelName, eVehicleSeat)
							SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							
						ELSE								//use character name to define the ped MODEL_NAMES
						
							IF CREATE_NPC_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat)
								SET_MODEL_AS_NO_LONGER_NEEDED(psPed.ModelName)
							ENDIF
						
						ENDIF
						
						IF NOT IS_PED_INJURED(psPed.PedIndex)
						
							SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
													   bCanBeTargetted, bIsEnemy)
							
							IF ( bCreateBlip = TRUE )
								psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " in vehicle.")
							ENDIF
						#ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ELSE
		
			//if the ped already exists return true
			RETURN TRUE
		
		ENDIF
		
		
	ELIF ( bPlayerPed = TRUE )	//create player ped, for example a ped that player can hotswap to and take control of
	
		IF NOT DOES_ENTITY_EXIST(psPed.PedIndex)
	
			IF ( VehicleIndex = NULL )	//create player ped outside of vehicle
	
				IF CREATE_PLAYER_PED_ON_FOOT(psPed.PedIndex, eCharacter, psPed.vPosition, psPed.fHeading, TRUE)
				
					SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
					
					IF ( bCreateBlip = TRUE )
						psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT Is_String_Null_Or_Empty(sDebugName)
							SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), " at coordinates ", psPed.vPosition, ".")
						ENDIF	
					#ENDIF
					
					RETURN TRUE
					
				ENDIF
				
			ELIF ( VehicleIndex != NULL )	//create player ped in a vehicle
			
				IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
				
					IF CREATE_PLAYER_PED_INSIDE_VEHICLE(psPed.PedIndex, eCharacter, VehicleIndex, eVehicleSeat, TRUE)
					
						SET_MISSION_PED_PROPERTIES(psPed.PedIndex, relGroupHash, bCanFlyThroughWindscreen, bKeepRelGroupOnCleanup,
												   bCanBeTargetted, bIsEnemy)
						
						IF ( bCreateBlip = TRUE )
							psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
						ENDIF
						
						
						#IF IS_DEBUG_BUILD
							IF NOT Is_String_Null_Or_Empty(sDebugName)
								SET_PED_NAME_DEBUG(psPed.PedIndex, sDebugName)
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped ", sDebugName, " in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ELSE
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player ped  in vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(psPed.ModelName), ".")
							ENDIF
						#ENDIF
						
						RETURN TRUE
					
					ENDIF
				
				ENDIF
			
			ENDIF
		
		ELSE
		
			//if the ped already exists, return true
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Creates vehicle to be used on a mission by player and returns TRUE if such vehicle was created successfully.
/// PARAMS:
///    vsVehicle - VEH_STRUCT containing model, position and heading of the vehicle.
///    bPlayerVehicle - If set to TRUE will create player specific vehicles using CREATE_PLAYER_VEHICLE()
///    bCarStealVehicle - If set to TRUE will create car steal strand vehicle with a specific command. Works when bPlayerVehicle is FALSE.
///    eCharacter - Character enum indicating which character specific vehicle to create.
///    bMissionCritical - Sets the vehicle to be unable to leak oil/petrol and break off doors. Set to TRUE to stop these damage types to vehicle.
///    iColourCombination - Colour combination of the vehicle.
///    iColour1 - Colour 1 of the vehicle.
///    iColour2 - Colour 2 of the vehicle.
/// RETURNS:
///    TRUE if vehicle was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_VEHICLE_BEEN_CREATED(VEH_STRUCT &vsVehicle, BOOL bPlayerVehicle = FALSE, BOOL bCarStealVehicle = FALSE, enumCharacterList eCharacter = CHAR_MICHAEL,
										   BOOL bMissionCritical = TRUE, INT iColourCombination = -1, INT iColour1 = -1, INT iColour2 = -1
										   #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)

	//create any vehicle that can be used on a mission by player
	IF ( bPlayerVehicle = FALSE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
		
			REQUEST_MODEL(vsVehicle.ModelName)
			
			IF HAS_MODEL_LOADED(vsVehicle.ModelName)

				IF( bCarStealVehicle = TRUE )
				
					vsVehicle.VehicleIndex = CREATE_CAR_STEAL_STRAND_CAR(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
					SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
				ELSE
				
					IF ( eCharacter = NO_CHARACTER )
					
						vsVehicle.VehicleIndex = CREATE_VEHICLE(vsVehicle.ModelName, vsVehicle.vPosition, vsVehicle.fHeading)
						SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
					
					ELSE								//create npc vehicle based on the character enum specified
					
						IF CREATE_NPC_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(vsVehicle.ModelName)
						ENDIF
					
					ENDIF
					
				ENDIF
				
				
				
				IF IS_VEHICLE_DRIVEABLE(vsVehicle.VehicleIndex)
				
					//set vehicle colours, if they are provided
					IF ( iColour1 != -1)
					AND ( iColour2 != -1 )
						SET_VEHICLE_COLOURS(vsVehicle.VehicleIndex, iColour1, iColour2)
					ENDIF
					
					//set vehicle colour combination, if it is provided
					IF ( iColourCombination != -1 )
						SET_VEHICLE_COLOUR_COMBINATION(vsVehicle.VehicleIndex, iColourCombination)
					ENDIF
					
					SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
					
					//limit damage that can be done to the vehicle
					SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					//make the vehicle not attach to tow truck if mission critical
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
					
					IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
					ENDIF
					
					IF IS_THIS_MODEL_A_HELI(vsVehicle.ModelName)
					OR IS_THIS_MODEL_A_PLANE(vsVehicle.ModelName)
						SET_VEHICLE_ENGINE_CAN_DEGRADE(vsVehicle.VehicleIndex, NOT bMissionCritical)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(vsVehicle.VehicleIndex)
					
				ENDIF

				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ELSE		
						IF ( bCarStealVehicle = TRUE )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created car steal strand vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
						ENDIF
					ENDIF
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
			
		ENDIF
		
	//create player specific vehicle
	ELIF ( bPlayerVehicle = TRUE )
	
		IF NOT DOES_ENTITY_EXIST(vsVehicle.VehicleIndex)
		
			//delete all previous instances of player ped vehicle in the world
			DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(eCharacter)
	
			//call the player vehicle creation until it returns true
			IF CREATE_PLAYER_VEHICLE(vsVehicle.VehicleIndex, eCharacter, vsVehicle.vPosition, vsVehicle.fHeading, TRUE)
			
				SET_VEHICLE_HAS_STRONG_AXLES(vsVehicle.VehicleIndex, bMissionCritical)
			
				//limit damage that can be done to the vehicle
				SET_VEHICLE_CAN_LEAK_OIL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				SET_VEHICLE_CAN_LEAK_PETROL(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				//make the vehicle not attach to tow truck if mission critical
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vsVehicle.VehicleIndex, NOT bMissionCritical)
				
				IF IS_THIS_MODEL_A_CAR(vsVehicle.ModelName)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vsVehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
				ENDIF
			
				#IF IS_DEBUG_BUILD
					IF NOT Is_String_Null_Or_Empty(sDebugName)
						SET_VEHICLE_NAME_DEBUG(vsVehicle.VehicleIndex, sDebugName)
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle ", sDebugName, " with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ELSE
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(vsVehicle.ModelName), " at coordinates ", vsVehicle.vPosition, ".")
					ENDIF	
				#ENDIF
				
				RETURN TRUE
			
			ENDIF
		
		ELSE	//if the vehicle exists, return true
		
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_VEHICLE_AS_MISSION_CRITICAL(VEH_STRUCT &vehicle, BOOL bMissionCritical)

	SET_VEHICLE_HAS_STRONG_AXLES(vehicle.VehicleIndex, bMissionCritical)

	//limit damage that can be done to the vehicle
	SET_VEHICLE_CAN_LEAK_OIL(vehicle.VehicleIndex, NOT bMissionCritical)
	SET_VEHICLE_CAN_LEAK_PETROL(vehicle.VehicleIndex, NOT bMissionCritical)
	
	//make the vehicle not attach to tow truck if mission critical
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehicle.VehicleIndex, NOT bMissionCritical)
	
	IF IS_THIS_MODEL_A_CAR(vehicle.ModelName)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehicle.VehicleIndex, SC_DOOR_FRONT_LEFT, NOT bMissionCritical)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehicle.VehicleIndex, SC_DOOR_FRONT_RIGHT, NOT bMissionCritical)
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates an object and returns TRUE if such object was created successfully.
/// PARAMS:
///    osObject - OBJECT_STRUCT containing model, position and rotation of the object.
///    bFreezeObject - Specify if this object's position should be frozen.
/// RETURNS:
///    TRUE if object was created, FALSE if otherwise.
FUNC BOOL HAS_MISSION_OBJECT_BEEN_CREATED(OBJECT_STRUCT &osObject, BOOL bFreezeObject = FALSE)

	IF NOT DOES_ENTITY_EXIST(osObject.ObjectIndex)
		
		REQUEST_MODEL(osObject.ModelName)
		
		IF HAS_MODEL_LOADED(osObject.ModelName)
		
			osObject.ObjectIndex = CREATE_OBJECT(osObject.ModelName, osObject.vPosition)
			SET_ENTITY_COORDS_NO_OFFSET(osObject.ObjectIndex, osObject.vPosition)
			SET_ENTITY_ROTATION(osObject.ObjectIndex, osObject.vRotation)
			FREEZE_ENTITY_POSITION(osObject.ObjectIndex, bFreezeObject)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(osObject.ModelName)
		
		ENDIF
	
	ENDIF
	
	IF DOES_ENTITY_EXIST(osObject.ObjectIndex)
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object with model ", GET_MODEL_NAME_FOR_DEBUG(osObject.ModelName), " at coordinates ", osObject.vPosition, ".")
		#ENDIF

		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_MISSION_TRAIN_BEEN_CREATED(TRAIN_STRUCT &sTrain, FLOAT fSpeed, BOOL bDirectionFlag = FALSE)

	IF NOT DOES_ENTITY_EXIST(sTrain.VehicleIndex)
				
		REQUEST_MODEL(sTrain.ModelName1)
		REQUEST_MODEL(sTrain.ModelName2)
		REQUEST_MODEL(sTrain.ModelName3)
		REQUEST_MODEL(sTrain.ModelName4)
	
		IF 	HAS_MODEL_LOADED(sTrain.ModelName1)
		AND HAS_MODEL_LOADED(sTrain.ModelName2)
		AND HAS_MODEL_LOADED(sTrain.ModelName3)
		AND HAS_MODEL_LOADED(sTrain.ModelName4)

			sTrain.VehicleIndex = CREATE_MISSION_TRAIN(sTrain.iConfiguration, sTrain.vPosition, bDirectionFlag)
			sTrain.PedIndex		= CREATE_PED_INSIDE_VEHICLE(sTrain.VehicleIndex, PEDTYPE_MISSION, sTrain.ModelName4)
			
			SET_TRAIN_SPEED(sTrain.VehicleIndex, fSpeed)
			SET_TRAIN_CRUISE_SPEED(sTrain.VehicleIndex, fSpeed)
			
			SET_CAN_AUTO_VAULT_ON_ENTITY(sTrain.VehicleIndex, FALSE)
			SET_CAN_CLIMB_ON_ENTITY(sTrain.VehicleIndex, FALSE)
			
			SET_PED_CONFIG_FLAG(sTrain.PedIndex, PCF_GetOutBurningVehicle, TRUE)
			SET_PED_CONFIG_FLAG(sTrain.PedIndex, PCF_GetOutUndriveableVehicle, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sTrain.PedIndex, TRUE)
			
			INT i
			
			FOR i = 0 TO 12
			
				VEHICLE_INDEX CarriageVehicleIndex
				
				CarriageVehicleIndex = GET_TRAIN_CARRIAGE(sTrain.VehicleIndex, i)
				
				IF DOES_ENTITY_EXIST(CarriageVehicleIndex)
					IF NOT IS_ENTITY_DEAD(CarriageVehicleIndex)
						SET_CAN_AUTO_VAULT_ON_ENTITY(CarriageVehicleIndex, FALSE)
						SET_CAN_CLIMB_ON_ENTITY(CarriageVehicleIndex, FALSE)
					ENDIF
				ENDIF
				
			ENDFOR
			
			SET_MODEL_AS_NO_LONGER_NEEDED(sTrain.ModelName1)
			SET_MODEL_AS_NO_LONGER_NEEDED(sTrain.ModelName2)
			SET_MODEL_AS_NO_LONGER_NEEDED(sTrain.ModelName3)
			SET_MODEL_AS_NO_LONGER_NEEDED(sTrain.ModelName4)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created mission train with configuration ", sTrain.iConfiguration, " at coordinates ", sTrain.vPosition, ".")
			#ENDIF
			
			RETURN TRUE
			
		ENDIF
	
	ELSE
	
		IF IS_VEHICLE_DRIVEABLE(sTrain.VehicleIndex)
	
			RETURN TRUE
			
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_PED_PROPERTIES_AFTER_HOTSWAP(PED_STRUCT &psPed, PED_INDEX ped, BOOL bCreateBlip, REL_GROUP_HASH relGroupHash)

	IF NOT IS_PED_INJURED(ped)
		
		psPed.PedIndex = ped
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psPed.PedIndex, TRUE)
		SET_PED_CAN_BE_TARGETTED(psPed.PedIndex, FALSE)
		SET_PED_AS_ENEMY(psPed.PedIndex, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(psPed.PedIndex, relGroupHash)
		
		
		IF ( bCreateBlip = TRUE )
		
			IF NOT DOES_BLIP_EXIST(psPed.BlipIndex)
			
				psPed.BlipIndex = CREATE_BLIP_FOR_ENTITY(psPed.PedIndex, FALSE)
			
			ENDIF
		
		ENDIF
				
		//set the ped timer to current game timer when the hotswap happens
		psPed.iTimer = GET_GAME_TIMER()
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Use for blocking first person camera for a limited time, see B*2031566
PROC UPDATE_FIRST_PERSON_VEHICLE_CAMERA_BLOCKING()

	IF ( bBlockVehicleFirstPersonCamera = TRUE )
		
		//block first person vehicle camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
		
		//check timer
		IF ( iBlockVehicleFirstPersonCameraTimer != 0 )
			IF HAS_TIME_PASSED(2000, iBlockVehicleFirstPersonCameraTimer)
				bBlockVehicleFirstPersonCamera = FALSE
			ELIF HAS_TIME_PASSED(1700, iBlockVehicleFirstPersonCameraTimer)
				IF ( bFirstPersonFXTriggered = FALSE )
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					bFirstPersonFXTriggered = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_STAGE_LOADED(MISSION_STAGES eStage, INT &iSetupProgress, BOOL &bStageLoaded, BOOL &bStageSkippedTo, BOOL &bStageReplayed)

	//handle initial mission setup
	//only run this IF block once, when the mission first loads
	IF ( iSetupProgress = 0 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting initial mission loading.")
		#ENDIF

		//clear the screen of any text displayed before mission started
		CLEAR_PRINTS()
		CLEAR_HELP()
		
		//loads the mission text
		REQUEST_ADDITIONAL_TEXT("MCH1", MISSION_TEXT_SLOT)
		
		REQUEST_ADDITIONAL_TEXT("MCH1AUD", MISSION_DIALOGUE_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
			
		//suppress Michael's and Trevor's cars for the duration of the mission
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR), TRUE)
		
		SET_PED_MODEL_IS_SUPPRESSED(mnEnemy, TRUE)
		
		ADD_RELATIONSHIP_GROUP("ENEMIES", rgEnemies)

		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rgEnemies,RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,rgEnemies)
	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rgEnemies,rgEnemies)

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		ENDIF

		IF IS_REPLAY_IN_PROGRESS() OR IS_REPEAT_PLAY_ACTIVE()
			DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
		ENDIF
	
		iTrainBellsSoundID	= GET_SOUND_ID()
		iRainOnPlaneSoundID = GET_SOUND_ID()
		iChurchBellsSoundID = GET_SOUND_ID()
		
		#IF IS_DEBUG_BUILD
			IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player replay start vehicle model is ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_START_VEHICLE_MODEL()), ".")
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player replay checkpoint vehicle model is ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
			ENDIF
		#ENDIF
		
		IF ( bStageReplayed = FALSE )
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					//
				BREAK
				CASE CHAR_TREVOR
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIC1_STARTING_CHAR)
				BREAK
			ENDSWITCH
		ENDIF
		
		CREATE_SCENARIO_BLOCKING_AREA(<< 1415.83, 3108.05, 41.00 >>, << 48.0, 16.0, 2.0 >>)
		
		ADD_COVER_BLOCKING_AREA(<<3278.639160 - 0.1, -4629.158691 - 0.1, 115.868752 - 1.5>>,
								<<3278.639160 + 0.1, -4629.158691 + 0.1, 115.868752 + 1.5>>, TRUE, TRUE, TRUE)
								
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished initial mission loading.")
		#ENDIF
		
		iSetupProgress++
	
	ENDIF


	//run this each time new mission stage needs to be loaded
	IF ( iSetupProgress = 1 )
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Started mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded = FALSE
	
		//initialise arrays for peds, vehicles and objects
		INITIALISE_ARRAYS_FOR_MISSION_STAGE(eStage)
		
		//cleanup the asset arrays so that they are ready to be populated with assets needed for the stage being loaded
		CLEANUP_MODEL_ARRAY(MissionModels, iModelsRequested)
		CLEANUP_CARREC_ARRAY(sVehicleRecordingsFile, MissionRecordings, iRecordingsRequested)
		
		//clear triggered text labels
		CLEAR_TRIGGERED_LABELS()

		iSetupProgress++
	
	ENDIF
	
	//handle player positioning when current stage is being skipped to or played normally
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 2 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Warping player ped to new position for mission stage being loaded due to skipping or replay.")
			#ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
				
			IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL )
				WARP_PED(PLAYER_PED_ID(), psMichael.vPosition, psMichael.fHeading, FALSE, TRUE, FALSE)
			ELIF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR )
				WARP_PED(PLAYER_PED_ID(), psTrevor.vPosition, psTrevor.fHeading, FALSE, TRUE, FALSE)
			ENDIF

			IF ( bStageSkippedTo )
				//pin the interior at player start position for stages starting in interiors or requiring interiors
				IF ( eStage = MISSION_STAGE_CUTSCENE_INTRO )
				OR ( eStage = MISSION_STAGE_GET_TO_AIRPORT AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL )
				
					WHILE NOT IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						
						PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Pinning interior at coordinates ", vInteriorPosition, " in memory.")
						#ENDIF
						
						WAIT(0)
					
					ENDWHILE
				
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				
				STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				
				CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0, TRUE)
				STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_DECALS_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 1000.0)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
			ENDIF
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
			
			bDirtDecalApplied		= FALSE
			bDamageDecalApplied 	= FALSE
			bWeaponsHelpDisplayed	= FALSE
			
			bBlockVehicleFirstPersonCamera 		= FALSE
			iBlockVehicleFirstPersonCameraTimer = 0
			
			SWITCH eStage
				CASE MISSION_STAGE_CUTSCENE_INTRO
				CASE MISSION_STAGE_GET_TO_AIRPORT
				CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
				CASE MISSION_STAGE_CUTSCENE_AIRPORT
				CASE MISSION_STAGE_FLY_HOME
				CASE MISSION_STAGE_END		
					REMOVE_ALL_PROLOGUE_IPLS()
				BREAK
				CASE MISSION_STAGE_GET_TO_GRAVEYARD
				CASE MISSION_STAGE_GET_TO_GRAVE
				CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
				CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
				CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
					REQUEST_PROLOGUE_IPLS()
					REQUEST_PROLOGUE_GRAVE_IPL(FALSE)
				BREAK
			ENDSWITCH
			
		ENDIF
		
		IF ( bStageReplayed = TRUE )
		
			IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL )
				START_REPLAY_SETUP(psMichael.vPosition, psMichael.fHeading)
			ELIF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR )
				START_REPLAY_SETUP(psTrevor.vPosition, psTrevor.fHeading)
			ENDIF
		
		ENDIF
		
		//request assets needed for current stage
		BUILD_REQUEST_BANK_FOR_MISSION_STAGE(eStage)
		
		iSetupProgress++
	
	ENDIF
	
	//handle loading of mission models that have been requested in the previous stage in BUILD_REQUEST_BANK_FOR_MISSION_STAGE()
	IF ( iSetupProgress = 3)
	
		IF ARE_REQUESTED_MODELS_LOADED(MissionModels, iModelsRequested)
			IF ARE_REQUESTED_CARRECS_LOADED(sVehicleRecordingsFile, MissionRecordings, iRecordingsRequested)
				iSetupProgress++
			ENDIF
		ENDIF
	
	ENDIF
	
	//handle creating vehicles/peds and setting up fail flags
	//run this each time new mission stage needs to be loaded
	//THIS SETUP SECTION ASSUMES THAT ALL REQUIRED ASSETS HAVE BEEN ALREADY LOADED TO MEMORY IN PREVIOUS SETUP SECTIONS
	IF ( iSetupProgress = 4 )
	
		SET_FAIL_FLAGS(FALSE)
	
		SWITCH eStage
		
			CASE MISSION_STAGE_CUTSCENE_INTRO
				iSetupProgress++
			BREAK
			
			CASE MISSION_STAGE_GET_TO_AIRPORT

				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL	//when playing as Michael
						
					IF HAS_MISSION_PED_BEEN_CREATED(psTrevor, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_TREVOR, FALSE, FALSE, FALSE,  vsMichaelsCar.VehicleIndex)//vsTrevorsCar.VehicleIndex)
				
						FailFlags[MISSION_FAIL_TREVOR_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= TRUE
						FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= TRUE
				
						IF ( bStageReplayed = TRUE )					//if this is a mission replay
						AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
						AND IS_THIS_MODEL_A_CAR(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())

							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle is available for stage replay for player ped Michael.")
							#ENDIF
						
							IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)	//create replay start vehicle
							
								REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
								
								IF HAS_MODEL_LOADED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						
									vsMichaelsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.vPosition, vsMichaelsCar.fHeading)
									
									SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
									SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
									
									SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
									
									#IF IS_DEBUG_BUILD
										SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RC_Michaels_Car")
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle for player ped Michael with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
									#ENDIF
									
								ENDIF
							
							ELSE
							
								IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
								
									FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= FALSE
									FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] 	= FALSE
									FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] 	= FALSE
									
									iSetupProgress++
									
								ENDIF
								
							ENDIF
						
						ELSE	//create Michael's default car
						
							IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)

								FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= FALSE
								FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] 	= FALSE
								FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] 	= FALSE
								
								iSetupProgress++

							ENDIF
						
						ENDIF
				
					ENDIF
					
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsPlane, FALSE, FALSE, NO_CHARACTER, TRUE, 1)
							
						FailFlags[MISSION_FAIL_TREVORS_PLANE_DEAD] 			= TRUE
						FailFlags[MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE] 	= TRUE
										
						IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL)	//create default Michael's car
							
							FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= FALSE		
							FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] 	= FALSE		
							FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] 	= FALSE
							
							SET_VEHICLE_AS_RESTRICTED(vsMichaelsCar.VehicleIndex, 0)					//set it as restricted so that if Trevor
																										//takes it will be deleted on mission cleanup
																										//so that only 1 instance of Michael's car exists
																										//in the world
							
							IF ( bStageReplayed = TRUE )			//if this is a mission replay
							AND IS_REPLAY_START_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay start vehicle is available for stage replay for player ped Trevor.")
								#ENDIF
							
								IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)	//create replay start vehicle
								
									REQUEST_REPLAY_START_VEHICLE_MODEL()
									
									IF HAS_MODEL_LOADED(GET_REPLAY_START_VEHICLE_MODEL())
							
										vsTrevorsCar.VehicleIndex = CREATE_REPLAY_START_VEHICLE(vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										
										SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
										
										SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_START_VEHICLE_MODEL())
										
										#IF IS_DEBUG_BUILD
											SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay start vehicle for player ped Trevor with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_START_VEHICLE_MODEL()), ".")
										#ENDIF
										
									ENDIF
								
								ELSE
								
									IF IS_VEHICLE_DRIVEABLE(vsTrevorsCar.VehicleIndex)
									
										FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= FALSE
										FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= FALSE
										FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = FALSE
																				
										iSetupProgress++
									ENDIF
									
								ENDIF
							
							ELSE	//create Trevor's default car
							
								IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR)

									FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= FALSE
									FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= FALSE
									FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = FALSE
									
									iSetupProgress++

								ENDIF
							
							ENDIF

						ENDIF

					ENDIF

					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
				
				IF HAS_MISSION_OBJECT_BEEN_CREATED(osGolfBall, TRUE)
					
					IF DOES_ENTITY_EXIST(osGolfBall.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osGolfBall.ObjectIndex)
							SET_ENTITY_HEADING(osGolfBall.ObjectIndex, 315.0)
							SET_ENTITY_VISIBLE(osGolfBall.ObjectIndex, FALSE)
							SET_ENTITY_COLLISION(osGolfBall.ObjectIndex, FALSE)
						ENDIF
					ENDIF
				
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsPlane, FALSE, FALSE, NO_CHARACTER, TRUE, 1)
							
						FailFlags[MISSION_FAIL_TREVORS_PLANE_DEAD] = TRUE
						FailFlags[MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE] = TRUE
										
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							
							IF HAS_MISSION_PED_BEEN_CREATED(psTrevor, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_TREVOR)
				
								sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = psTrevor.PedIndex
								
								FailFlags[MISSION_FAIL_TREVOR_DEAD] = TRUE
								
								IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR)

									FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= FALSE
									FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= FALSE
									FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = FALSE
									
									iSetupProgress++

								ENDIF
								
							ENDIF
						
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						
							IF ( bStageReplayed = TRUE )					//if this is a mission replay
							AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()	//and snapshot vehicle data exists
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle is available for stage replay for player ped Trevor.")
								#ENDIF
							
								IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)	//create replay checkpoint vehicle
								
									REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
									
									IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

										vsTrevorsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										
										SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
										
										SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
										
										#IF IS_DEBUG_BUILD
											SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle for player ped Trevor with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
										#ENDIF
										
									ENDIF
								
								ELSE
								
									IF IS_VEHICLE_DRIVEABLE(vsTrevorsCar.VehicleIndex)
									
										FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= FALSE
										FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= FALSE
										FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = FALSE
										
										iSetupProgress++
									ENDIF
									
								ENDIF
							
							ELSE	//create Trevor's default car
							
								IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR)

									FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= FALSE
									FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= FALSE
									FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = FALSE
									
									iSetupProgress++

								ENDIF
							
							ENDIF
							
						ENDIF

					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_AIRPORT
			
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL

					iSetupProgress++
						
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		
					iSetupProgress++

				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
						
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, FALSE, FALSE, NO_CHARACTER, TRUE, -1, 86, 0)
				
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] 	= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] 	= TRUE
					FailFlags[MISSION_FAIL_GRAVEYARD_NOT_REACHED] 		= TRUE
					
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						SET_VEH_RADIO_STATION(vsMichaelsCar.VehicleIndex, "OFF")
						SET_VEHICLE_RADIO_ENABLED(vsMichaelsCar.VehicleIndex, FALSE)
						SET_VEHICLE_NUMBER_PLATE_TEXT(vsMichaelsCar.VehicleIndex, NUMBERPLATES)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsMichaelsCar.VehicleIndex, TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsMichaelsCar.VehicleIndex, TRUE)
					ENDIF

					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, FALSE, FALSE, NO_CHARACTER)
					
						FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] 		= FALSE	//TRUE
						FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] 	= FALSE	//TRUE
					
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL

							iSetupProgress++

						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						
							IF HAS_MISSION_PED_BEEN_CREATED(psMichael, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_MICHAEL, FALSE, FALSE, FALSE, vsMichaelsCar.VehicleIndex)
					
								sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = psMichael.PedIndex
								
								FailFlags[MISSION_FAIL_MICHAEL_DEAD] = TRUE
								
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVE
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, FALSE, FALSE, NO_CHARACTER, TRUE, -1, 86, 0)
				
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] 	= TRUE
					FailFlags[MISSION_FAIL_GRAVE_NOT_REACHED] 			= TRUE
					
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						SET_VEH_RADIO_STATION(vsMichaelsCar.VehicleIndex, "OFF")
						SET_VEHICLE_RADIO_ENABLED(vsMichaelsCar.VehicleIndex, FALSE)
						SET_VEHICLE_NUMBER_PLATE_TEXT(vsMichaelsCar.VehicleIndex, NUMBERPLATES)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsMichaelsCar.VehicleIndex, TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsMichaelsCar.VehicleIndex, FALSE)
					ENDIF

					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, FALSE, FALSE, NO_CHARACTER)
					
						IF IS_VEHICLE_DRIVEABLE(vsTrevorsCar.VehicleIndex)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsTrevorsCar.VehicleIndex, FALSE)
						ENDIF
					
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL

							FailFlags[MISSION_FAIL_TREVOR_DEAD] 	= TRUE
							FailFlags[MISSION_FAIL_MICHAEL_DEAD] 	= TRUE

							iSetupProgress++

						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						
							IF HAS_MISSION_PED_BEEN_CREATED(psMichael, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_MICHAEL, FALSE, FALSE, FALSE)
					
								sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = psMichael.PedIndex
								
								FailFlags[MISSION_FAIL_TREVOR_DEAD] 	= TRUE
								FailFlags[MISSION_FAIL_MICHAEL_DEAD] 	= TRUE
								
								iSetupProgress++
								
							ENDIF
						
						ENDIF
						
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, FALSE, FALSE, NO_CHARACTER, TRUE, -1, 86, 0)
				
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] = TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] = TRUE

					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						SET_VEH_RADIO_STATION(vsMichaelsCar.VehicleIndex, "OFF")
						SET_VEHICLE_RADIO_ENABLED(vsMichaelsCar.VehicleIndex, FALSE)
						SET_VEHICLE_NUMBER_PLATE_TEXT(vsMichaelsCar.VehicleIndex, NUMBERPLATES)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsMichaelsCar.VehicleIndex, TRUE)
					ENDIF

					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					
						FailFlags[MISSION_FAIL_TREVOR_DEAD] 	= TRUE
						FailFlags[MISSION_FAIL_MICHAEL_DEAD] 	= TRUE

						iSetupProgress++

					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					
						IF HAS_MISSION_PED_BEEN_CREATED(psMichael, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_MICHAEL)
				
							sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = psMichael.PedIndex
							
							FailFlags[MISSION_FAIL_TREVOR_DEAD] 	= TRUE
							FailFlags[MISSION_FAIL_MICHAEL_DEAD] 	= TRUE
							
							iSetupProgress++
							
						ENDIF
					
					ENDIF
					
				ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			
				IF ( bCoverpointsCreated = FALSE )
					CREATE_COVERPOINTS(csCoverpoints, vShootoutStartPosition)
					INITIALISE_FRAGGABLE_TOMBSTONES(FraggableTombstones)
					bCoverpointsCreated = TRUE
				ENDIF
				
				iPlayerMichaelKills		= 0
			
				IF	HAS_MISSION_OBJECT_BEEN_CREATED(osPropCoffin, TRUE)
				AND	HAS_MISSION_OBJECT_BEEN_CREATED(osPropShovel, TRUE)
				AND	HAS_MISSION_OBJECT_BEEN_CREATED(osPropPickaxe, TRUE)
				AND HAS_MISSION_PED_BEEN_CREATED(psBrad, FALSE, RELGROUPHASH_NO_RELATIONSHIP, FALSE, NO_CHARACTER, FALSE, FALSE, FALSE)
			
					IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, FALSE, FALSE, NO_CHARACTER, TRUE, -1, 86, 0)
					
						FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] = TRUE
						FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] = TRUE

						IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
							SET_VEH_RADIO_STATION(vsMichaelsCar.VehicleIndex, "OFF")
							SET_VEHICLE_RADIO_ENABLED(vsMichaelsCar.VehicleIndex, FALSE)
							SET_VEHICLE_NUMBER_PLATE_TEXT(vsMichaelsCar.VehicleIndex, NUMBERPLATES)
							SET_VEHICLE_UNDRIVEABLE(vsMichaelsCar.VehicleIndex, TRUE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsMichaelsCar.VehicleIndex, TRUE)
						ENDIF

						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
													
							iSetupProgress++
							
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							
							IF HAS_MISSION_PED_BEEN_CREATED(psMichael, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_MICHAEL)
						
								sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = psMichael.PedIndex
								
								FailFlags[MISSION_FAIL_MICHAEL_DEAD] = TRUE
								
								iSetupProgress++
								
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, FALSE, FALSE, NO_CHARACTER, TRUE, -1, 86, 0)
				
					FailFlags[MISSION_FAIL_MICHAELS_CAR_DEAD] = TRUE
					FailFlags[MISSION_FAIL_MICHAELS_CAR_UNDRIVABLE] = TRUE
					
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						SET_VEH_RADIO_STATION(vsMichaelsCar.VehicleIndex, "OFF")
						SET_VEHICLE_RADIO_ENABLED(vsMichaelsCar.VehicleIndex, FALSE)
						SET_VEHICLE_NUMBER_PLATE_TEXT(vsMichaelsCar.VehicleIndex, NUMBERPLATES)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vsMichaelsCar.VehicleIndex, TRUE)
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
											
						iSetupProgress++
						
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						
						IF HAS_MISSION_PED_BEEN_CREATED(psMichael, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_MICHAEL)
					
							sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = psMichael.PedIndex
							
							FailFlags[MISSION_FAIL_MICHAEL_DEAD] = TRUE
							
							iSetupProgress++
							
						ENDIF
						
					ENDIF
						
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_FLY_HOME
			CASE MISSION_STAGE_END
			
				IF HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsPlane, FALSE, FALSE, NO_CHARACTER, TRUE, 1)
					
					IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsTrevorsPlane.VehicleIndex, TRUE)
					ENDIF
					
					FailFlags[MISSION_FAIL_TREVORS_PLANE_DEAD] 			= TRUE
					FailFlags[MISSION_FAIL_TREVORS_PLANE_UNDRIVABLE] 	= TRUE
										
					SWITCH eStage
						CASE MISSION_STAGE_FLY_HOME
							IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
								FREEZE_ENTITY_POSITION(vsTrevorsPlane.VehicleIndex, TRUE)
							ENDIF
						BREAK
						CASE MISSION_STAGE_END
							IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
								IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
									SET_HELI_BLADES_FULL_SPEED(vsTrevorsPlane.VehicleIndex)
									SET_VEHICLE_ENGINE_ON(vsTrevorsPlane.VehicleIndex, TRUE, TRUE)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD
						SWITCH GET_PLAYER_CHARACTER_AT_MISSION_START()
							CASE CHAR_MICHAEL
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission started as Michael.")
							BREAK
							CASE CHAR_TREVOR
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission started as Trevor.")
							BREAK
						ENDSWITCH
						
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player playing as Michael.")
							BREAK
							CASE CHAR_TREVOR
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player playing as Trevor.")
							BREAK
						ENDSWITCH
						
						IF IS_REPLAY_START_VEHICLE_AVAILABLE()
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay start vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_START_VEHICLE_MODEL()), " is available for stage.")
						ENDIF
						IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), " is available for stage.")
						ENDIF
						IF ( TrevorsVehicleData.eModel <> DUMMY_MODEL_FOR_SCRIPT )
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": TrevorsVehicleData.eModel is model ", GET_MODEL_NAME_FOR_DEBUG(TrevorsVehicleData.eModel), ".")
						ENDIF
						
					#ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					
						IF HAS_MISSION_PED_BEEN_CREATED(psTrevor, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_TREVOR, FALSE, FALSE, FALSE, vsTrevorsPlane.VehicleIndex)
						
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = psTrevor.PedIndex
								
							FailFlags[MISSION_FAIL_TREVOR_DEAD] = TRUE
							
							IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_TREVOR AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()				
						
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), " is available for stage.")
								#ENDIF
							
								IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)	//create replay checkpoint saved vehicle
								
									REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
									
									IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

										CLEAR_AREA(vsTrevorsCar.vPosition, 10.0, TRUE)
										
										vsTrevorsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										
										SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
										
										IF IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, TRUE)
										ENDIF
										
										SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
										
										#IF IS_DEBUG_BUILD
											SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle for player ped Trevor with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
										#ENDIF
										
									ENDIF
								
								ELSE
									
									iSetupProgress++
									
								ENDIF
							
							ELIF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), " is available for stage.")
								#ENDIF
							
								IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)	//create replay checkpoint saved vehicle
								
									REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
									
									IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

										CLEAR_AREA(vsTrevorsCar.vPosition, 10.0, TRUE)
										
										vsTrevorsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										
										SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
										
										IF IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, TRUE)
										ENDIF
										
										SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
										
										#IF IS_DEBUG_BUILD
											SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle for player ped Trevor with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
										#ENDIF
										
									ENDIF
								
								ELSE
									
									iSetupProgress++
									
								ENDIF
							
							ELSE
								
								IF ( TrevorsVehicleData.eModel = DUMMY_MODEL_FOR_SCRIPT AND HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR) )
								OR ( TrevorsVehicleData.eModel <> DUMMY_MODEL_FOR_SCRIPT )
									
									IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
										IF NOT IS_ENTITY_DEAD(vsTrevorsCar.VehicleIndex)
											IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vsTrevorsCar.VehicleIndex))
												SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, TRUE)
											ENDIF
										ENDIF
									ENDIF
						
									iSetupProgress++
							
								ENDIF
							
							ENDIF
					
						ENDIF 
					
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					
						IF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_TREVOR AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()				
						
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), " is available for stage.")
								#ENDIF
							
								IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)	//create replay checkpoint saved vehicle
								
									REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
									
									IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

										CLEAR_AREA(vsTrevorsCar.vPosition, 10.0, TRUE)
										
										vsTrevorsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										
										SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
										
										IF IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, TRUE)
										ENDIF
										
										SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
										
										#IF IS_DEBUG_BUILD
											SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle for player ped Trevor with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
										#ENDIF
										
									ENDIF
								
								ELSE
									
									iSetupProgress++
									
								ENDIF
							
							ELIF GET_PLAYER_CHARACTER_AT_MISSION_START() = CHAR_MICHAEL AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay checkpoint vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), " is available for stage.")
								#ENDIF
							
								IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)	//create replay checkpoint saved vehicle
								
									REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
									
									IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()

										CLEAR_AREA(vsTrevorsCar.vPosition, 10.0, TRUE)
										
										vsTrevorsCar.VehicleIndex = CREATE_REPLAY_CHECKPOINT_VEHICLE(vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										
										SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
										
										IF IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, TRUE)
										ENDIF
										
										SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
										
										#IF IS_DEBUG_BUILD
											SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating replay checkpoint vehicle for player ped Trevor with model ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
										#ENDIF
										
									ENDIF
								
								ELSE
									
									iSetupProgress++
									
								ENDIF
							
							ELSE
								
								IF ( TrevorsVehicleData.eModel = DUMMY_MODEL_FOR_SCRIPT AND HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR) )
								OR ( TrevorsVehicleData.eModel <> DUMMY_MODEL_FOR_SCRIPT )
									
									IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
										IF NOT IS_ENTITY_DEAD(vsTrevorsCar.VehicleIndex)
											IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vsTrevorsCar.VehicleIndex))
												SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, TRUE)
											ENDIF
										ENDIF
									ENDIF
						
									iSetupProgress++
							
								ENDIF
							
							ENDIF

					ENDIF
				
				ENDIF
			
			BREAK
		
		ENDSWITCH
	
	ENDIF

	//set the current player ped enum depending on mission stage
	//delete the other player ped if mission stage requires only one player ped
	IF ( iSetupProgress = 5 )
	
		bTrevorFlightActive = FALSE
	
		SWITCH eStage

			CASE MISSION_STAGE_CUTSCENE_INTRO
				//
			BREAK
			
			CASE MISSION_STAGE_GET_TO_AIRPORT
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, TRUE)
					BREAK
					CASE CHAR_TREVOR
						EXTEND_WORLD_BOUNDARY_FOR_PLAYER(sLocateFlight.vPosition)
						LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, TRUE)
					BREAK
				ENDSWITCH
			BREAK
			
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
			
				bTrevorFlightActive = TRUE
				
				IF ( bStageSkippedTo = TRUE )
				OR ( bStageReplayed	= TRUE )
			
					IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)

						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Performing script controlled hotswap to Trevor for mission stage being loaded. Setting Michael player ped as no longer needed.")
						#ENDIF
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							ENDIF
						ENDIF
						
					ENDIF
					
					EXTEND_WORLD_BOUNDARY_FOR_PLAYER(sLocateFlight.vPosition)
					
				ENDIF

			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_AIRPORT			
				//
			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
			CASE MISSION_STAGE_GET_TO_GRAVE
			CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
			
				IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Performing script controlled hotswap to Michael for mission stage being loaded. Setting Trevor player ped as no longer needed.")
					#ENDIF
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleting Trevor ped.")
							#ENDIF
						ENDIF
					ENDIF

				ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_FLY_HOME
			CASE MISSION_STAGE_END
			
				bTrevorFlightActive = TRUE
			
				IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)

					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Performing script controlled hotswap to Trevor for mission stage being loaded. Setting Michael player ped as no longer needed.")
					#ENDIF

					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						ENDIF
					ENDIF
					
				ENDIF
				
				SWITCH eStage
					CASE MISSION_STAGE_FLY_HOME
						EXTEND_WORLD_BOUNDARY_FOR_PLAYER(vsTrevorsPlane.vPosition + vGlobalFlightOffset)
					BREAK
				ENDSWITCH

			BREAK
		
		ENDSWITCH
	
		iSetupProgress++
		
	ENDIF
	
	//handle the prologue minimap display, weather and dispatch services depending on current mission stage 
	//run this each time new mission stage needs to be loaded
	IF ( iSetupProgress = 6 )
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

		SWITCH eStage
	
			//mission is in san andreas map
			CASE MISSION_STAGE_CUTSCENE_INTRO
			CASE MISSION_STAGE_GET_TO_AIRPORT
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
			CASE MISSION_STAGE_CUTSCENE_AIRPORT
			CASE MISSION_STAGE_FLY_HOME
			CASE MISSION_STAGE_END
			
				SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
			
				//if prologue map is active, which means mission stages are skipped backwards
				//then remove the prologue IPLs
				IF ( bPrologueMapActive = TRUE )
					
					UNLOCK_GRAVEYARD_GATES()
					UNREGISTER_GRAVEYARD_GATES()
					
					REMOVE_ALL_PROLOGUE_IPLS()
					
					SET_ROADS_IN_ANGLED_AREA(<<5526.240234,-5137.229980,61.789253>>, <<3679.326660,-4973.879395,125.082840>>, 192.0, FALSE, FALSE)
					SET_ROADS_IN_ANGLED_AREA(<<3691.211426,-4941.240234,94.593681>>, <<3511.115479,-4869.191406,126.762108>>, 16.0, FALSE, FALSE)
					SET_ROADS_IN_ANGLED_AREA(<<3510.004395,-4865.810059,94.695572>>, <<3204.424316,-4833.816895,126.815216>>, 16.0, FALSE, FALSE)
					SET_ROADS_IN_ANGLED_AREA(<<3186.533691,-4832.797852,109.814827>>, <<3202.187256,-4833.993164,114.814995>>, 16.0, FALSE, FALSE) //road leading down into town
					
					CLEAR_TIMECYCLE_MODIFIER()
					DISABLE_MOON_CYCLE_OVERRIDE()
					SET_AMBIENT_VEHICLE_NEON_ENABLED(TRUE)
					
					RELEASE_SUPPRESSED_EMERGENCY_CALLS()
					
					IF DOES_PICKUP_EXIST(HealthPickup)
						REMOVE_PICKUP(HealthPickup)
					ENDIF
					
					IF IS_CHEAT_DISABLED(CHEAT_TYPE_SUPER_JUMP)
						DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, FALSE)
					ENDIF
					IF IS_CHEAT_DISABLED(CHEAT_TYPE_SPAWN_VEHICLE)
						DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, FALSE)
					ENDIF
					IF IS_CHEAT_DISABLED(CHEAT_TYPE_ADVANCE_WEATHER)
						DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, FALSE)
					ENDIF
					
					bPrologueMapActive = FALSE
					
				ENDIF
			
				SET_RANDOM_TRAINS(TRUE)									//allow random trains when not in prologue map
				SET_ALLOW_STREAM_PROLOGUE_NODES(FALSE)					//turn streaming of prologue vehicle nodes off
				SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)	//turn prologue minimap off
				
				//enable dispatch services
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
				
				//set non prologue multipliers
				SET_MAX_WANTED_LEVEL(5)
				SET_CREATE_RANDOM_COPS(TRUE)
				SET_WANTED_LEVEL_MULTIPLIER(1.0)
				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(ASEA2, FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(MESA2, FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI, FALSE)
				
				//turn snow off if it is snowing
				IF ( bSnowOn = TRUE )
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					CLEAR_WEATHER_TYPE_PERSIST()
					UNLOAD_ALL_CLOUD_HATS()
					bSnowOn = FALSE
				ENDIF
				
				SET_MAPDATACULLBOX_ENABLED("prologue", FALSE)
				
				#IF USE_TU_CHANGES
					SET_MAPDATACULLBOX_ENABLED("Prologue_Main", FALSE)
				#ENDIF
				
				IF ( bProloguePedComponentsActive = TRUE )	//set non prologue clothes on player peds
				OR ( bStageSkippedTo = TRUE )
				OR ( bStageReplayed	= TRUE )

					SWITCH eStage
					
						CASE MISSION_STAGE_FLY_HOME
						CASE MISSION_STAGE_END
						
							RESTORE_PLAYER_PED_VARIATIONS(GET_PED_INDEX(CHAR_TREVOR))
						
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring player ped variations.")
							#ENDIF
						
						BREAK
						
						DEFAULT

							RESTORE_MISSION_START_OUTFIT()
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Restoring default player ped component variations.")
							#ENDIF
						
						BREAK
						
					ENDSWITCH
											
					bProloguePedComponentsActive = FALSE
					
				ENDIF
				
				IF ( eStage = MISSION_STAGE_FLY_HOME )
					SET_CLOCK_TIME(6, 15, 0)
					//SET_WEATHER_TYPE_NOW("THUNDER")
					SET_WEATHER_TYPE_NOW("OVERCAST")
				ENDIF
				
				//enable vehicle radios
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				SET_USER_RADIO_CONTROL_ENABLED(TRUE)
				STOP_AUDIO_SCENE("MIC1_RADIO_DISABLE")
				
				SET_AMBIENT_ZONE_LIST_STATE("ZL_YANKTON_ZONE_KILL_LIST", TRUE, TRUE)
				SET_AMBIENT_ZONE_LIST_STATE("ZONE_LIST_YANKTON", FALSE, TRUE)
				
				REMOVE_NAVMESH_BLOCKING_OBJECTS()
				
				IF ( bStageSkippedTo = TRUE )	//on debug skip unblock player ped weapons and restore them
					IF ( bMichaelsWeaponsStored = TRUE )
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID()).")
								#ENDIF
							BREAK
							CASE CHAR_TREVOR
								RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL)
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL).")
								#ENDIF
							BREAK
						ENDSWITCH
						bMichaelsWeaponsStored = FALSE
					ENDIF
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
					g_eCurrentDoorState[DOORNAME_M_MANSION_F_L] = DOORSTATE_FORCE_UNLOCKED_THIS_FRAME	//set state for building controller to pick up
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
					g_eCurrentDoorState[DOORNAME_M_MANSION_F_R] = DOORSTATE_FORCE_UNLOCKED_THIS_FRAME	//set state for building controller to pick up
				ENDIF

			BREAK
			
			//mission is in north yankton map
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
			CASE MISSION_STAGE_GET_TO_GRAVE
			CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED

				SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
				
				IF ( bPrologueMapActive = FALSE )
				
					IF NOT IS_REPLAY_BEING_SET_UP()
						REQUEST_PROLOGUE_IPLS()
						REQUEST_PROLOGUE_GRAVE_IPL(FALSE)
					ENDIF
					
					REGISTER_GRAVEYARD_GATES()
					LOCK_GRAVEYARD_GATES()
					
					SET_ROADS_IN_ANGLED_AREA(<<5526.240234,-5137.229980,61.789253>>, <<3679.326660,-4973.879395,125.082840>>, 192.0, FALSE, TRUE)
					SET_ROADS_IN_ANGLED_AREA(<<3691.211426,-4941.240234,94.593681>>, <<3511.115479,-4869.191406,126.762108>>, 16.0, FALSE, TRUE)
					SET_ROADS_IN_ANGLED_AREA(<<3510.004395,-4865.810059,94.695572>>, <<3204.424316,-4833.816895,126.815216>>, 16.0, FALSE, TRUE)
					SET_ROADS_IN_ANGLED_AREA(<<3186.533691,-4832.797852,109.814827>>, <<3202.187256,-4833.993164,114.814995>>, 16.0, FALSE, TRUE) //road leading down into town
					
					SUPPRESS_EMERGENCY_CALLS()

					SWITCH eStage
						CASE MISSION_STAGE_GET_TO_GRAVEYARD
							SET_CLOCK_TIME(22, 0, 0)
						BREAK
						CASE MISSION_STAGE_GET_TO_GRAVE
						CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
						CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
						CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
							SET_CLOCK_TIME(0, 0, 0)
						BREAK
					ENDSWITCH
					
					//handle taking away weapons from Michael and leaving him only with a pistol for North Yankton stages
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL
							IF ( bMichaelsWeaponsStored = FALSE )
								IF NOT bStageReplayInProgress
									STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID()).")
									#ENDIF
								ENDIF
								REMOVE_PED_WEAPONS_LEAVING_ONE(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
								bMichaelsWeaponsStored = TRUE
							ENDIF
						BREAK
						CASE CHAR_TREVOR
							//
						BREAK
					ENDSWITCH
					
					IF NOT DOES_PICKUP_EXIST(HealthPickup)	//create health pickup in the graveyard
					
						iHealthPickupPlacementFlags = 0
					
						SET_BIT(iHealthPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
						SET_BIT(iHealthPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
						SET_BIT(iHealthPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))

						HealthPickup = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, << 3243.380 , -4647.380, 114.975 >>,
															<< 0.0, 0.0, 94.5 >>, iHealthPickupPlacementFlags)									 						
					ENDIF
					
					DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, TRUE)
					DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
					DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, TRUE)
					
					ENABLE_MOON_CYCLE_OVERRIDE(0.5)
					SET_AMBIENT_VEHICLE_NEON_ENABLED(FALSE)
					
					bPrologueMapActive = TRUE
					
				ENDIF
				
				SWITCH eStage
					CASE MISSION_STAGE_GET_TO_GRAVEYARD
						CLEAR_TIMECYCLE_MODIFIER()
					BREAK
					CASE MISSION_STAGE_GET_TO_GRAVE
						SET_TRANSITION_TIMECYCLE_MODIFIER("graveyard_shootout", 30.0)
					BREAK
					CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
					CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT					
					CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
						IF ( bStageSkippedTo = TRUE OR bStageReplayed = TRUE )
							SET_TIMECYCLE_MODIFIER("graveyard_shootout")
						ENDIF
					BREAK
				ENDSWITCH
				
				SWITCH eStage
					CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
						SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
						SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
						
						IF NOT DOES_PICKUP_EXIST(PistolPickup)	//create pistol pickup in the graveyard, Trevor throws pistol in the cutscene
						
							iPistolPickupPlacementFlags = 0
						
							SET_BIT(iPistolPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
							SET_BIT(iPistolPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
							SET_BIT(iPistolPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))

							PistolPickup = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PISTOL, <<3258.7891, -4575.3062, 117.2570>>,
																<<-88.2000, 65.8800, 0.0000>>, iPistolPickupPlacementFlags, 12)
						ENDIF
						
						IF NOT DOES_PICKUP_EXIST(RiflePickup)	//create rifle pickup in the graveyard
						
							iRiflePickupPlacementFlags = 0
						
							SET_BIT(iRiflePickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
							SET_BIT(iRiflePickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
							SET_BIT(iRiflePickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))

							RiflePickup = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ASSAULTRIFLE, <<3265.8049, -4591.3149, 116.1286>>,
															   <<-70.2000, -14.4000, -28.8000>>, iRiflePickupPlacementFlags, 50)
						ENDIF
						
					BREAK
				ENDSWITCH
				
				SET_RANDOM_TRAINS(FALSE)								//allow random trains when not in prologue map
				SET_ALLOW_STREAM_PROLOGUE_NODES(TRUE)					//turn streaming of prologue vehicle nodes off
				SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(TRUE)	//turn prologue minimap off
				
				//disable dispatch services
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
				
				//set prologue multipliers
				SET_MAX_WANTED_LEVEL(0)
				SET_CREATE_RANDOM_COPS(FALSE)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
				
				//suppress the prologue specific vehicles and peds
				SET_VEHICLE_MODEL_IS_SUPPRESSED(ASEA2, TRUE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(MESA2, TRUE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEOLD1, TRUE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(POLICEOLD2, TRUE)				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI, TRUE)
				
				//turn snow on if it is not snowing
				//set the time once only
				IF ( bSnowOn = FALSE )
					CLEAR_WEATHER_TYPE_PERSIST()
					SET_WEATHER_TYPE_NOW_PERSIST("SNOWLIGHT")
					UNLOAD_ALL_CLOUD_HATS()
					LOAD_CLOUD_HAT("Snowy 01")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting weather type to SNOWLIGHT.")
					#ENDIF
					bSnowOn = TRUE
				ENDIF
				
				SET_MAPDATACULLBOX_ENABLED("prologue", TRUE)
				
				#IF USE_TU_CHANGES
					SET_MAPDATACULLBOX_ENABLED("Prologue_Main", TRUE)
				#ENDIF
				
				IF ( bProloguePedComponentsActive = FALSE )	//set prologue clothes on player peds
				
					IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_MICHAEL))
						IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_MICHAEL))
							SET_PED_COMP_ITEM_CURRENT_SP(GET_PED_INDEX(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF, FALSE)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_TREVOR))
						IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
							SET_PED_COMP_ITEM_CURRENT_SP(GET_PED_INDEX(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_LUDENDORFF, FALSE)
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting prologue player ped component variations.")
					#ENDIF
					
					bProloguePedComponentsActive = TRUE
					
				ENDIF
				
				SWITCH eStage
					CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
					CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
						IF ( bDamageDecalApplied = FALSE )
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								APPLY_PED_DAMAGE_DECAL(PLAYER_PED_ID(), PDZ_HEAD, 0.621, 0.755, 55.084, 0.01, 1.0, 1, TRUE, "bruise")
								bDamageDecalApplied = TRUE
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH

				//disable vehicle radios in prologue
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				SET_USER_RADIO_CONTROL_ENABLED(FALSE)
				IF NOT IS_AUDIO_SCENE_ACTIVE("MIC1_RADIO_DISABLE")
					START_AUDIO_SCENE("MIC1_RADIO_DISABLE")
				ENDIF
				
				SET_AMBIENT_ZONE_LIST_STATE("ZL_YANKTON_ZONE_KILL_LIST", FALSE, TRUE)
				SET_AMBIENT_ZONE_LIST_STATE("ZONE_LIST_YANKTON", TRUE, TRUE)
				
				ADD_NAVMESH_BLOCKING_OBJECTS()
				
				IF ( bStageSkippedTo = TRUE	OR bStageReplayed = TRUE )
					DELETE_ALL_TRAINS()
				ENDIF

			BREAK
		
		ENDSWITCH

		iSetupProgress++

	ENDIF
	
	//make sure prologue IPLs are loaded when going to prologue map
	IF ( iSetupProgress = 7 )
	
		IF ( bPrologueMapActive = TRUE ) 
		
			REQUEST_PTFX_ASSET()
		
			IF 	HAS_PTFX_ASSET_LOADED()
			AND ARE_PROLOGUE_IPLS_ACTIVE()
			AND IS_PROLOGUE_GRAVE_IPL_ACTIVE(FALSE)
			AND REQUEST_SCRIPT_AUDIO_BANK("ICE_FOOTSTEPS")
			AND REQUEST_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS")
			AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_HEIST_3B_SHOOTOUT")
				iSetupProgress++
			ENDIF
				
		ELIF ( bPrologueMapActive = FALSE )
			
			REMOVE_PTFX_ASSET()
			RELEASE_SCRIPT_AUDIO_BANK()
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_HEIST_3B_SHOOTOUT")
			
			iSetupProgress++
			
		ENDIF
	
	ENDIF

	//handle setup peds for dialogue, taxi hailing, cellphone
	IF ( iSetupProgress = 8 )
	
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 0)	//Michael
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 2)	//Trevor
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 3)	//Chinese Goon 1	/Dave Norton (phone call)
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 4)	//Chinese Goon 2	/Ron (phone call)
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 5)	//Chinese Goon 3	/Cheng senior (phone call)
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 6)	//Chinese Goon 4
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 7)	//Chinese Goon 5
		REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 8)	//Air traffic control
		
		SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting audio flag DisableReplayScriptStreamRecording to false.")
		
		SWITCH eStage
		
			CASE MISSION_STAGE_GET_TO_AIRPORT
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
				
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 0, PLAYER_PED_ID(), "MICHAEL")
						ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 2, NULL, "TREVOR")
					BREAK
					CASE CHAR_TREVOR
						ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 2, PLAYER_PED_ID(), "TREVOR")
						ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 0, NULL, "MICHAEL")
					BREAK
				ENDSWITCH
				
				ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 3, NULL, "DAVE")
				ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 4, NULL, "NERVOUSRON")
				ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 8, NULL, "MCH1AIRTRAFFIC")
				
				DISABLE_CELLPHONE(FALSE)
				DISABLE_TAXI_HAILING(FALSE)

			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
			CASE MISSION_STAGE_GET_TO_GRAVE
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 0, PLAYER_PED_ID(), "MICHAEL")
				ENDIF

				DISABLE_CELLPHONE(FALSE)
				DISABLE_TAXI_HAILING(TRUE)
				
				SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting audio flag DisableReplayScriptStreamRecording to true.")
				
			BREAK
						
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED

				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 0, PLAYER_PED_ID(), "MICHAEL")
				ENDIF
				
				DISABLE_CELLPHONE(FALSE)
				DISABLE_TAXI_HAILING(TRUE)
				
			BREAK
			
			CASE MISSION_STAGE_FLY_HOME

				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 2, PLAYER_PED_ID(), "TREVOR")
				ENDIF
				
				ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 5, NULL, "CHENGSR")
				
				DISABLE_CELLPHONE(FALSE)
				DISABLE_TAXI_HAILING(TRUE)
				
			BREAK
			
			CASE MISSION_STAGE_END
				DISABLE_CELLPHONE(FALSE)
				DISABLE_TAXI_HAILING(FALSE)
			BREAK
					
		ENDSWITCH
	
		iSetupProgress++
		
	ENDIF
	
	IF ( iSetupProgress = 9 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE )
		OR ( bStageReplayed	= TRUE )
		
			SWITCH eStage
				CASE MISSION_STAGE_GET_TO_AIRPORT
				
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL
							iSetupProgress++
						BREAK
						CASE CHAR_TREVOR
							
							REQUEST_WAYPOINT_RECORDING("mic1_tdrive")
							
							IF GET_IS_WAYPOINT_RECORDING_LOADED("mic1_tdrive")
								IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
									
										IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
											SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
										ENDIF
										
										CLEAR_AREA_OF_PEDS(<<-857.8701, 172.5816, 67.0251>>, 10.0)
										CLEAR_AREA_OF_VEHICLES(<<-857.8701, 172.5816, 67.0251>>, 10.0)
										
										SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, <<-857.8701, 172.5816, 67.0251>>)
										SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, 355.2254)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
										
										OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.VehicleIndex)
										
										SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
										SET_VEHICLE_FORWARD_SPEED(vsMichaelsCar.VehicleIndex, 10.0)
										TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, "mic1_tdrive", DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
										
										iSetupProgress++
										
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
					ENDSWITCH
				
				BREAK
				DEFAULT
					iSetupProgress++
				BREAK
			ENDSWITCH
		
		ELSE
		
			iSetupProgress++
		
		ENDIF
	
	ENDIF
	
	//handle player positioning when current stage is being skipped to replayed
	//fade in the screen if it was faded out due to skipping
	IF ( iSetupProgress = 10 )
	
		//stage was skipped to or is being replayed
		IF ( bStageSkippedTo = TRUE )
		OR ( bStageReplayed	= TRUE )
		
		
			IF ( bStageReplayed = FALSE )
		
				VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling LOAD_SCENE() at coordinates ", vPlayerCoords, " for mission replay or stage skip. This might take a while to load.")
				#ENDIF
			
				LOAD_SCENE(vPlayerCoords)
				
				WAIT(1000)	//add wait to hide any load scene freezes
				
			ELSE
			
				END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
			
			ENDIF
			
			
			
			//put player in vehicle
			SWITCH eStage
			
				CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
				
					WARP_PED(PLAYER_PED_ID(), << 3256.5776, -4575.2520, 117.2670 >>, 267.6326, FALSE, FALSE, FALSE)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), csCoverpoints[0].vPosition, -1, TRUE, 0, TRUE, TRUE, csCoverpoints[0].CoverpointIndex)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					
					//check for Michaels's fail weapon
					IF ( eMichaelsFailWeapon = WEAPONTYPE_INVALID )
					OR ( eMichaelsFailWeapon = WEAPONTYPE_UNARMED )
						eMichaelsFailWeapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID(), TRUE)
						IF ( eMichaelsFailWeapon = WEAPONTYPE_INVALID )
						OR ( eMichaelsFailWeapon = WEAPONTYPE_UNARMED )
							eMichaelsFailWeapon = WEAPONTYPE_PISTOL	//if fail weapon is invalid or unarmed, give scripted weapon
						ENDIF
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Michael's fail weapon is on stage retry is ", GET_WEAPON_NAME(eMichaelsFailWeapon), ".")
					#ENDIF
					
					INT iAmmo

					IF ( GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), eMichaelsFailWeapon) < GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), eMichaelsFailWeapon) )
						iAmmo = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), eMichaelsFailWeapon) * 2
					ENDIF
					
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), eMichaelsFailWeapon, iAmmo, TRUE)

					WAIT(500)
				
				BREAK

				//put player ped into vehicle
				CASE MISSION_STAGE_CUTSCENE_AIRPORT
				CASE MISSION_STAGE_GET_TO_GRAVEYARD
					
					IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_MICHAEL))
						IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
							SET_PED_INTO_VEHICLE(GET_PED_INDEX(CHAR_MICHAEL), vsMichaelsCar.VehicleIndex, VS_DRIVER)
							SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
							SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
						ENDIF
					ENDIF
					
				BREAK
				
				CASE MISSION_STAGE_FLY_HOME
				CASE MISSION_STAGE_END
				
					IF NOT IS_PED_INJURED(GET_PED_INDEX(CHAR_TREVOR))
						IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
							IF NOT IS_PED_SITTING_IN_VEHICLE(GET_PED_INDEX(CHAR_TREVOR), vsTrevorsPlane.VehicleIndex)
								SET_PED_INTO_VEHICLE(GET_PED_INDEX(CHAR_TREVOR), vsTrevorsPlane.VehicleIndex, VS_DRIVER)
							ENDIF
							SET_VEHICLE_ENGINE_ON(vsTrevorsPlane.VehicleIndex, TRUE, TRUE)
						ENDIF
					ENDIF
					
				BREAK

			ENDSWITCH

			IF ( bStageSkippedTo = TRUE )
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing player ped invincibility for mission stage being skipped to.")
				#ENDIF
			ENDIF

			IF ( eStage =  MISSION_STAGE_GRAVEYARD_SHOOTOUT )
				IF ( bGameplayCameraSet = FALSE  )
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-16.536800)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-45.0000)
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-16.536800)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-72.373550)
					ENDIF
					bGameplayCameraSet = TRUE
				ENDIF
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
			
			//handle music for skips/retry/replay of mission stages
			SWITCH eStage
			
				CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
					TRIGGER_MUSIC_EVENT("MIC1_SHOOTOUT_RT")
				BREAK
				
				CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
					TRIGGER_MUSIC_EVENT("MIC1_SKIPPED_TO_KIDNAP")
				BREAK
				
				CASE MISSION_STAGE_FLY_HOME
					TRIGGER_MUSIC_EVENT("MIC1_FLY_HOME_RT")
				BREAK
			
			ENDSWITCH
			
			SWITCH eStage
				CASE MISSION_STAGE_GET_TO_AIRPORT
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PLAYER_ANGRY(PLAYER_PED_ID(), TRUE)
					ENDIF
				BREAK
				DEFAULT
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_PLAYER_ANGRY(PLAYER_PED_ID(), FALSE)
					ENDIF
				BREAK
			ENDSWITCH

			//handle fade in for each mission stage
			//some stages start with or require a fade out
			SWITCH eStage
			
				CASE MISSION_STAGE_CUTSCENE_INTRO
				CASE MISSION_STAGE_CUTSCENE_AIRPORT
				CASE MISSION_STAGE_GET_TO_GRAVEYARD
				CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
				CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
				CASE MISSION_STAGE_FLY_HOME
				
					//do not fade in, mission stage will handle a fade in from fade out
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Halting loading screen fade in for skip to a mission stage that will fade the screen in.")
					#ENDIF
					
				BREAK
			
				DEFAULT
				
					//fade in
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling a loading screen fade in.")
					#ENDIF
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
				BREAK

			ENDSWITCH

		ENDIF
		
		iSetupProgress++
	
	ENDIF
	
	//handle setting of mid-mission replay checkpoints
	IF ( iSetupProgress = 11 )
	
		SWITCH eStage
		
			CASE MISSION_STAGE_GET_TO_AIRPORT
			
				//don't store replay checkpoint vehicle here, the replay start vehicle can be used here
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_AIRPORT), "GET TO AIRPORT", FALSE, FALSE, NULL, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_AIRPORT), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
			
				//store replay checkpoint vehicle here
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_FLY_TO_NORTH_YANKTON), "FLY TO NORTH YANKTON", FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_FLY_TO_NORTH_YANKTON), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
			
				//don't store replay checkpoint vehicle here
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_GRAVEYARD), "GET TO GRAVEYARD", FALSE, FALSE, NULL, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_GRAVEYARD), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVE
			
				//don't store replay checkpoint vehicle here
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_GRAVE), "GET TO GRAVE", FALSE, FALSE, NULL, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_GRAVE), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
			
			BREAK
			
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
			
				//don't store replay checkpoint vehicle here
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GRAVEYARD_SHOOTOUT), "GRAVEYARD SHOOTOUT", FALSE, FALSE, NULL, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_GRAVEYARD_SHOOTOUT), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
			
			CASE MISSION_STAGE_FLY_HOME
			
				//don't store replay checkpoint vehicle here
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_FLY_HOME), "FLY HOME", TRUE, FALSE, NULL, FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_FLY_HOME), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
				#ENDIF
				
			BREAK
						
		ENDSWITCH
	
		iSetupProgress++
	
	ENDIF
	
	//print the message only once per mission stage
	IF ( iSetupProgress = 12 )
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Finished mission stage loading for ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eStage), ".")
		#ENDIF
		
		bStageLoaded 	= TRUE
		bStageReplayed 	= FALSE
		bStageSkippedTo = FALSE
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		iSetupProgress++
		
	ENDIF
		
	IF ( iSetupProgress = 13 )

		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

//|============================ COMBAT PROCEDURES & FUNCTIONS ============================|

FUNC PED_INDEX CREATE_ENEMY_PED(MODEL_NAMES modelName, VECTOR vPosition, FLOAT fHeading, REL_GROUP_HASH group,
								WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED, WEAPONCOMPONENT_TYPE eWeaponComponent = WEAPONCOMPONENT_INVALID,
								BOOL bDucking = FALSE, INT iHealth = 200, INT iArmour = 0)


	PED_INDEX PedIndex = CREATE_PED(PEDTYPE_MISSION, modelName, vPosition, fHeading)

	SET_PED_COMPONENT_VARIATION(PedIndex, PED_COMP_HAIR, 1, 0)

	//enemy health and armor
	SET_PED_MAX_HEALTH(PedIndex, iHealth)
	SET_ENTITY_HEALTH(PedIndex, iHealth)
	ADD_ARMOUR_TO_PED(PedIndex, iArmour)
	SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, group)
	SET_PED_AS_ENEMY(PedIndex, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(PedIndex, TLR_NEVER_LOSE_TARGET)
	SET_PED_COMBAT_ATTRIBUTES(PedIndex, CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(PedIndex, CA_ENABLE_TACTICAL_POINTS_WHEN_DEFENSIVE, TRUE)
		
	//enemy weapons
	GIVE_WEAPON_TO_PED(PedIndex, eWeaponType, INFINITE_AMMO, TRUE)
	SET_CURRENT_PED_WEAPON(PedIndex, eWeaponType, TRUE)
	SET_PED_INFINITE_AMMO(PedIndex, TRUE, eWeaponType)
		
	IF ( eWeaponComponent <> WEAPONCOMPONENT_INVALID )
		GIVE_WEAPON_COMPONENT_TO_PED(PedIndex, eWeaponType, eWeaponComponent)
	ENDIF

	SET_PED_DUCKING(PedIndex, bDucking)
	
	//disable ped being able to climb
	SET_PED_PATH_CAN_USE_CLIMBOVERS(PedIndex, FALSE)
	
	SET_PED_CONFIG_FLAG(PedIndex, PCF_DisableHurt, TRUE)
	SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	SET_COMBAT_FLOAT(PedIndex, CCF_TIME_TO_INVALIDATE_INJURED_TARGET, 30.0)
	
	//add ped to be watched by headshot stat
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(PedIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy ped with model ", GET_MODEL_NAME_FOR_DEBUG(modelName), " at coordinates ", vPosition, ".")
	#ENDIF
		
	RETURN PedIndex

ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED_IN_VEHICLE(MODEL_NAMES modelName, VEHICLE_INDEX vehicleIndex, VEHICLE_SEAT eSeat, REL_GROUP_HASH group,
										   WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED, WEAPONCOMPONENT_TYPE eWeaponComponent = WEAPONCOMPONENT_INVALID,
										   INT iHealth = 200, INT iArmour = 0)
										   
	PED_INDEX ped
	
	IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
		ped = CREATE_ENEMY_PED(modelName, GET_ENTITY_COORDS(vehicleIndex) + <<0.0, 0.0, 3.0>>, 0.0, group, eWeaponType, eWeaponComponent, FALSE, iHealth, iArmour)
		SET_PED_INTO_VEHICLE(ped, vehicleIndex, eSeat)
	ENDIF
	
	RETURN ped

ENDFUNC

PROC SET_ENEMY_PED_PROPERTIES(PED_INDEX PedIndex, REL_GROUP_HASH group, WEAPON_TYPE eWeaponType = WEAPONTYPE_UNARMED, BOOL bCanClimb = FALSE,
							  INT iHealth = 200, INT iArmour = 0, BOOL bKeepRelGroupOnCleanup = TRUE)
	
	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_PED_INJURED(PedIndex)
		
			SET_ENTITY_AS_MISSION_ENTITY(PedIndex, TRUE)
		
			GIVE_WEAPON_TO_PED(PedIndex, eWeaponType, INFINITE_AMMO, FALSE)
			SET_PED_INFINITE_AMMO(PedIndex, TRUE, eWeaponType)
			SET_PED_MAX_HEALTH(PedIndex, iHealth)
			SET_ENTITY_HEALTH(PedIndex, iHealth)
			ADD_ARMOUR_TO_PED(PedIndex, iArmour)
			SET_PED_DIES_WHEN_INJURED(PedIndex, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, group)
			SET_PED_AS_ENEMY(PedIndex, TRUE)
			SET_PED_TARGET_LOSS_RESPONSE(PedIndex, TLR_NEVER_LOSE_TARGET)
			SET_PED_COMBAT_ATTRIBUTES(PedIndex, CA_USE_VEHICLE, FALSE)
			
			//disable ped being able to climb
			SET_PED_PATH_CAN_USE_CLIMBOVERS(PedIndex, bCanClimb)
			
			SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_KeepRelationshipGroupAfterCleanUp, bKeepRelGroupOnCleanup)
			
			//add ped to be watched by headshot stat
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(PedIndex)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set enemy ped properties.")
			#ENDIF
			
		ENDIF
	ENDIF
						  
ENDPROC

PROC INITIALISE_ENEMY_GROUP(ENEMY_STRUCT &esEnemies[], BOOL bAddBlips = TRUE #IF IS_DEBUG_BUILD, STRING sDebugGroupName = NULL #ENDIF)

	INT i = 0

	FOR i = 0 TO ( COUNT_OF(esEnemies) - 1 )
	
		IF NOT IS_PED_INJURED(esEnemies[i].PedIndex)
			
			TASK_STAND_STILL(esEnemies[i].PedIndex, -1)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esEnemies[i].PedIndex, TRUE)
			
			IF ( bAddBlips = TRUE )
				esEnemies[i].BlipIndex =  CREATE_BLIP_FOR_ENTITY(esEnemies[i].PedIndex, TRUE)
			ENDIF
			
			esEnemies[i].bCreated = TRUE

			#IF IS_DEBUG_BUILD
				IF NOT IS_STRING_NULL_OR_EMPTY(sDebugGroupName)
					TEXT_LABEL_31 tlDebugName = sDebugGroupName
					tlDebugName += i
					SET_PED_NAME_DEBUG(esEnemies[i].PedIndex, tlDebugName)
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Initialised enemy ped with debug name ", tlDebugName, ".")
				ENDIF
			#ENDIF
			
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC ADD_BLIPS_FOR_ENEMY_GROUP(ENEMY_STRUCT &esEnemies[])

	INT i = 0

	FOR i = 0 TO ( COUNT_OF(esEnemies) - 1 )
	
		IF DOES_ENTITY_EXIST(esEnemies[i].PedIndex)
			IF NOT IS_PED_INJURED(esEnemies[i].PedIndex)
				IF NOT DOES_BLIP_EXIST(esEnemies[i].BlipIndex)
					esEnemies[i].BlipIndex =  CREATE_BLIP_FOR_ENTITY(esEnemies[i].PedIndex, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(ENEMY_STRUCT &array[], BOOL bRenderBlip = TRUE)

	INT i
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		IF ( bRenderBlip = TRUE )
			UPDATE_AI_PED_BLIP(array[i].PedIndex, array[i].EnemyBlipData)
		ELSE
			CLEANUP_AI_PED_BLIP(array[i].EnemyBlipData)
		ENDIF
	ENDFOR

ENDPROC

PROC BLOCK_WEAPON_FIRE_FOR_ENEMY_GROUP(ENEMY_STRUCT &array[])

	INT i
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		IF DOES_ENTITY_EXIST(array[i].PedIndex)
			IF NOT IS_ENTITY_DEAD(array[i].PedIndex)
				SET_PED_RESET_FLAG(array[i].PedIndex, PRF_BlockWeaponFire, TRUE)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC SET_PED_COMBAT_PROPERTIES(PED_INDEX pedIndex, COMBAT_MOVEMENT eCombatMovement, COMBAT_ABILITY_LEVEL eCombatAbilityLevel,
							   COMBAT_RANGE eCombatRange, COMBAT_ATTRIBUTE eCombatAttributes, BOOL bCanRagdoll = TRUE)
			
	IF NOT IS_PED_INJURED(pedIndex)

		SET_PED_TARGET_LOSS_RESPONSE(pedIndex, TLR_NEVER_LOSE_TARGET)

		SET_PED_COMBAT_MOVEMENT(pedIndex, eCombatMovement)
		SET_PED_COMBAT_ABILITY(pedIndex, eCombatAbilityLevel)
		SET_PED_COMBAT_RANGE(pedIndex, eCombatRange)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_USE_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_DO_DRIVEBYS, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_LEAVE_VEHICLES, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_CAN_INVESTIGATE, FALSE)
		
		IF ( eCombatAttributes <> CA_INVALID)
			SET_PED_COMBAT_ATTRIBUTES(pedIndex, eCombatAttributes, TRUE)
		ENDIF
		
		SET_PED_CAN_RAGDOLL(pedIndex, bCanRagdoll)
		
	ENDIF

ENDPROC

PROC SET_PED_COMBAT_MOVEMENT_VALUES(PED_INDEX PedIndex, FLOAT fStrafeMovingChance, FLOAT fWalkStrafingChance)

	IF NOT IS_PED_INJURED(PedIndex)
		SET_COMBAT_FLOAT(PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, fStrafeMovingChance)
		SET_COMBAT_FLOAT(PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, fWalkStrafingChance)
	ENDIF

ENDPROC

PROC SET_PED_DEFENSIVE_AREA_ANGLED(PED_INDEX PedIndex, VECTOR vPosition1, VECTOR vPosition2, FLOAT fWidth, BOOL bGoToCenter = FALSE)
	IF NOT IS_PED_INJURED(PedIndex)
		SET_PED_ANGLED_DEFENSIVE_AREA(PedIndex, vPosition1, vPosition2, fWidth, bGoToCenter)
	ENDIF
ENDPROC

PROC SET_PED_DEFENSIVE_AREA_SPHERE(PED_INDEX PedIndex, VECTOR vPosition, FLOAT fRadius, BOOL bGoToCenter = FALSE)
	IF NOT IS_PED_INJURED(PedIndex)
		SET_PED_SPHERE_DEFENSIVE_AREA(PedIndex, vPosition, fRadius, bGoToCenter)
	ENDIF
ENDPROC

PROC SET_PED_SHOOTING_PROPERTIES(PED_INDEX PedIndex, INT iAccuracy, INT iShootRate = 100, FIRING_PATTERN_HASH eFiringPattern = FIRING_PATTERN_BURST_FIRE)

	IF NOT IS_PED_INJURED(PedIndex)
		SET_PED_ACCURACY(PedIndex, iAccuracy)
		SET_PED_SHOOT_RATE(PedIndex, iShootRate)
		SET_PED_FIRING_PATTERN(PedIndex, eFiringPattern)
	ENDIF

ENDPROC

PROC SET_PED_COVER_PROPERTIES(PED_INDEX PedIndex, BOOL bCanUseCover, BOOL bForceFaceLeft = FALSE, BOOL bForceFaceRight = FALSE,
							  BOOL bBlockTurnInCover = FALSE, FLOAT fTimeBetweenPeeks = 10.0, BOOL bCanBlindFire = TRUE,
							  FLOAT fBlindFireChance = 0.05, FLOAT fTimeBetweenBursts = 1.25, FLOAT fBurstDuration = 4.0)

	IF NOT IS_PED_INJURED(PedIndex)
	
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_USE_COVER, bCanUseCover)
	
		SET_PED_CONFIG_FLAG(PedIndex, PCF_ForcePedToFaceLeftInCover, bForceFaceLeft)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_ForcePedToFaceRightInCover, bForceFaceRight)
		SET_PED_CONFIG_FLAG(PedIndex, PCF_BlockPedFromTurningInCover, bBlockTurnInCover)
		
		SET_PED_COMBAT_ATTRIBUTES(PedIndex, CA_BLIND_FIRE_IN_COVER, bCanBlindFire)
		
		IF ( fTimeBetweenPeeks >= 0.0 )
			SET_COMBAT_FLOAT(PedIndex, CCF_TIME_BETWEEN_PEEKS, fTimeBetweenPeeks)
		ENDIF
		
		IF ( fTimeBetweenBursts >= 0.0 )
			SET_COMBAT_FLOAT(PedIndex, CCF_TIME_BETWEEN_BURSTS_IN_COVER, fTimeBetweenBursts)
		ENDIF
		
		IF ( fBurstDuration >= 0.0 )
			SET_COMBAT_FLOAT(PedIndex, CCF_BURST_DURATION_IN_COVER, fBurstDuration)
		ENDIF
		
		IF ( fBlindFireChance >= 0.0 )
			SET_COMBAT_FLOAT(PedIndex, CCF_BLIND_FIRE_CHANCE, fBlindFireChance)
		ENDIF
	
	ENDIF

ENDPROC

PROC SET_PED_PREFERRED_COVERPOINT(PED_INDEX PedIndex, ITEMSET_INDEX &ItemsetIndex, COVERPOINT_INDEX &CoverpointIndex)

	IF NOT IS_PED_INJURED(PedIndex)
	
		IF IS_ITEMSET_VALID(ItemsetIndex)
			
			DESTROY_ITEMSET(ItemsetIndex)

		ENDIF
	
		IF NOT IS_ITEMSET_VALID(ItemsetIndex)
			ItemsetIndex = CREATE_ITEMSET(TRUE)
		ENDIF
	
		ADD_TO_ITEMSET(CoverpointIndex, ItemsetIndex)	
		SET_PED_PREFERRED_COVER_SET(PedIndex, ItemsetIndex)
		
	ENDIF
	
ENDPROC

PROC REMOVE_PED_PREFERRED_COVERPOINT(PED_INDEX PedIndex, ITEMSET_INDEX &ItemsetIndex)
	
	IF NOT IS_PED_INJURED(PedIndex)
		REMOVE_PED_PREFERRED_COVER_SET(PedIndex)
		IF IS_ITEMSET_VALID(ItemsetIndex)
			DESTROY_ITEMSET(ItemsetIndex)
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_NUMBER_OF_ENEMIES_ALIVE(ENEMY_STRUCT &esArray[])

	INT i = 0
	
	INT iEnemiesAlive = 0
	
	FOR i = 0 TO ( COUNT_OF(esArray) - 1 )
	
		IF DOES_ENTITY_EXIST(esArray[i].PedIndex)
		
			IF NOT IS_PED_INJURED(esArray[i].PedIndex)
			
				iEnemiesAlive++
			
			ENDIF
		
		ENDIF
	
	ENDFOR

	RETURN iEnemiesAlive

ENDFUNC

FUNC INT GET_NUMBER_OF_ENEMIES_DEAD(ENEMY_STRUCT &esArray[])

	INT i = 0
	
	INT iEnemiesDead = 0
	
	FOR i = 0 TO (COUNT_OF(esArray) - 1)
	
		IF ( esArray[i].bCreated = TRUE )
	
			IF DOES_ENTITY_EXIST(esArray[i].PedIndex)
			
				IF IS_PED_INJURED(esArray[i].PedIndex)
				OR IS_ENTITY_DEAD(esArray[i].PedIndex)
				
					iEnemiesDead++
				
				ENDIF
				
			ELSE
			
				iEnemiesDead++
				
			ENDIF
			
		ENDIF
	
	ENDFOR

	RETURN iEnemiesDead

ENDFUNC

/// PURPOSE:
///    Checks if all enemy peds in specified group are dead.
///    If this group has not been created yet, it will be treated as dead.
/// PARAMS:
///    esArray - Enemy ped array to check.
/// RETURNS:
///    TRUE if all created enemy peds from specified group are dead, or if the peds have not yet been created.
///    FALSE if otherwise.
FUNC BOOL IS_ENEMY_GROUP_DEAD(ENEMY_STRUCT &esArray[])

	INT i = 0
	
	FOR i = 0 TO (COUNT_OF(esArray) - 1)
	
		IF ( esArray[i].bCreated = TRUE )
		
			IF NOT IS_PED_INJURED(esArray[i].PedIndex)
				RETURN FALSE	
			ENDIF

		ENDIF 
	
	ENDFOR
	
	RETURN TRUE

ENDFUNC

PROC GO_TO_ENEMY_STATE(ENEMY_STRUCT &enemy, ENEMY_STATES eNewEnemyState)

	enemy.bHasTask = FALSE
	enemy.eState = eNewEnemyState
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(enemy.sDebugName)
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Sending enemy ped with debug name ", enemy.sDebugName, " to state ", GET_ENEMY_STATE_NAME_FOR_DEBUG(eNewEnemyState), ".")
		ENDIF
	#ENDIF
	
ENDPROC

FUNC BOOL IS_ENEMY_IN_ENEMY_STATE(ENEMY_STRUCT &enemy, ENEMY_STATES eEnemyState)

	IF DOES_ENTITY_EXIST(enemy.PedIndex)
		IF NOT IS_PED_INJURED(enemy.PedIndex)
			IF ( enemy.eState = eEnemyState )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC VEHICLE_INDEX CREATE_ENEMY_VEHICLE(VEH_STRUCT &sVehicle, INT iHealth = 1000, BOOL bCreateBlip = FALSE, BOOL bConsideredByPlayer = FALSE, BOOL bDriveable = TRUE, BOOL bCanBeTargetted = FALSE,
										LOCK_STATE eLockState = VEHICLELOCK_UNLOCKED, INT iColour1 = -1, INT iColour2 = -1,
										BOOL bFrozen = FALSE #IF IS_DEBUG_BUILD, STRING sDebugName = NULL #ENDIF)
	
	VEHICLE_INDEX VehicleIndex = CREATE_VEHICLE(sVehicle.ModelName, sVehicle.vPosition, sVehicle.fHeading)
		
	//vehicle health
	SET_ENTITY_HEALTH(VehicleIndex, iHealth)
	SET_VEHICLE_ENGINE_HEALTH(VehicleIndex, TO_FLOAT(iHealth))
	SET_VEHICLE_PETROL_TANK_HEALTH(VehicleIndex, TO_FLOAT(iHealth))
		
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(VehicleIndex, bConsideredByPlayer)
	SET_VEHICLE_UNDRIVEABLE(VehicleIndex, NOT bDriveable)
	SET_VEHICLE_CAN_BE_TARGETTED(VehicleIndex, bCanBeTargetted)
	SET_VEHICLE_DOORS_LOCKED(VehicleIndex, eLockState)
	
	//disable radio
	SET_VEHICLE_RADIO_ENABLED(VehicleIndex, FALSE)
	
	IF ( bCreateBlip = TRUE )
		IF NOT DOES_BLIP_EXIST(sVehicle.BlipIndex)
			sVehicle.BlipIndex = CREATE_BLIP_FOR_VEHICLE(VehicleIndex, TRUE)
		ENDIF
	ENDIF
	
	IF ( iColour1 != -1)
	AND ( iColour2 != -1 )
		SET_VEHICLE_COLOURS(VehicleIndex, iColour1, iColour2)
		SET_VEHICLE_EXTRA_COLOURS(VehicleIndex, 0, 0)	//make sure only normal colours are set, no extras
	ENDIF
	
	IF ( bFrozen = TRUE )
		FREEZE_ENTITY_POSITION(VehicleIndex, bFrozen)
		SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(VehicleIndex, bFrozen)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT Is_String_Null_Or_Empty(sDebugName)
			SET_VEHICLE_NAME_DEBUG(VehicleIndex, sDebugName)
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(sVehicle.ModelName), " and debug name ", sDebugName, " and health ", GET_ENTITY_HEALTH(VehicleIndex), ".")
		ELSE
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created enemy vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(sVehicle.ModelName), ".")
		
		ENDIF
	#ENDIF
		
	RETURN vehicleIndex
	
ENDFUNC

PROC COMBAT_SEQUENCE_GO_TO_COMBAT(PED_INDEX PedIndex, FLOAT fRange = 100.0)

	IF NOT IS_PED_INJURED(PedIndex)
	
		SEQUENCE_INDEX SequenceIndex
		
		OPEN_SEQUENCE_TASK(SequenceIndex)
					
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, fRange)
		
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		
		TASK_PERFORM_SEQUENCE(PedIndex, SequenceIndex)
		
		CLEAR_SEQUENCE_TASK(SequenceIndex)
	
	ENDIF

ENDPROC

PROC COMBAT_SEQUENCE_GO_TO_COORD_THEN_COMBAT(PED_INDEX PedIndex, VECTOR vDestination, FLOAT fMoveSpeed, INT iTime = DEFAULT_TIME_BEFORE_WARP, BOOL bUseNavmesh = TRUE, 
											FLOAT fRange = 100.0, FLOAT fRadius = DEFAULT_NAVMESH_RADIUS, ENAV_SCRIPT_FLAGS eNavigationFlags = ENAV_DEFAULT)

	IF NOT IS_PED_INJURED(PedIndex)
	
		SEQUENCE_INDEX SequenceIndex
		
		OPEN_SEQUENCE_TASK(SequenceIndex)
		
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			
			IF ( bUseNavmesh = TRUE )
			
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDestination, fMoveSpeed, iTime, fRadius, eNavigationFlags)
			
			ELSE
			
				TASK_GO_STRAIGHT_TO_COORD(NULL, vDestination, fMoveSpeed, iTime)
			
			ENDIF
					
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, fRange)
		
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		
		TASK_PERFORM_SEQUENCE(PedIndex, SequenceIndex)
		
		CLEAR_SEQUENCE_TASK(SequenceIndex)
	
	ENDIF

ENDPROC

PROC COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(PED_INDEX PedIndex, VECTOR vDestination, FLOAT fMoveSpeed,
															  BOOL bAimAtHatedPeds, VECTOR vAimPosition, ENTITY_INDEX EntityIndex = NULL,
															  BOOL bShootWhileMoving = FALSE, BOOL bCombatAfterStopped = TRUE,
															  BOOL bShootAfterStopped = FALSE, INT iAimShootTime = 1000,
															  FIRING_TYPE eFiringType = FIRING_TYPE_RANDOM_BURSTS,
															  BOOL bUnblockEvents = TRUE, FLOAT fTargetDistance = 0.5,
															  FLOAT fSlowDistance = 0.5, BOOL bUseNavmesh = TRUE, FLOAT fCombatRange = 100.0,
															  ENAV_SCRIPT_FLAGS eNavigationFlags = ENAV_DEFAULT)
	
	IF NOT IS_PED_INJURED(PedIndex)

		SEQUENCE_INDEX SequenceIndex
		
		OPEN_SEQUENCE_TASK(SequenceIndex)
			
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			
			IF ( bAimAtHatedPeds = TRUE )	//aim at hated entities makes the ped automatically change aiming to nearest hated ped, once the initial ped is dead
			
				IF IS_ENTITY_DEAD(EntityIndex)
				
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vDestination, vAimPosition, fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowDistance, bUseNavmesh, eNavigationFlags) 
					
				ELSE
					
					//if the entity does not exist, what then? might end up aiming at <<0,0,0>>
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vDestination, GET_ENTITY_COORDS(EntityIndex), fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowDistance, bUseNavmesh, eNavigationFlags) 
				
				ENDIF
				
			ELIF ( bAimAtHatedPeds = FALSE )	// aim at entity does not update aiming when that entity is dead, ped will keep aiming at dead ped
			
				IF IS_ENTITY_DEAD(EntityIndex)
			
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vDestination, vAimPosition, fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowdistance, bUseNavmesh)
					
				ELSE
				
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vDestination, EntityIndex, fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowDistance, bUseNavmesh) 
				
				ENDIF
			
			ENDIF
			
			IF ( bCombatAfterStopped = FALSE )
			
				IF IS_ENTITY_DEAD(EntityIndex)
			
					IF ( bShootAfterStopped = TRUE )
					
						TASK_SHOOT_AT_COORD(NULL, vAimPosition, iAimShootTime, eFiringType)
					
					ELSE
					
						TASK_AIM_GUN_AT_COORD(NULL, vAimPosition, iAimShootTime)
					
					ENDIF
				
				ELSE
				
					IF ( bShootAfterStopped = TRUE )
					
						TASK_SHOOT_AT_ENTITY(NULL, EntityIndex, iAimShootTime, eFiringType)
					
					ELSE
					
						TASK_AIM_GUN_AT_ENTITY(NULL, EntityIndex, iAimShootTime)
					
					ENDIF
				
				ENDIF
			
			ENDIF
			
			IF ( bUnblockEvents = TRUE )
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			ENDIF
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, fCombatRange)
			
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		
		TASK_PERFORM_SEQUENCE(PedIndex, SequenceIndex)
		
		CLEAR_SEQUENCE_TASK(SequenceIndex)
	
	ENDIF
	
ENDPROC

PROC COMBAT_SEQUENCE_GO_TO_COVER_WHILE_AIMING_THEN_COMBAT(PED_INDEX PedIndex, VECTOR vDestination, FLOAT fMoveSpeed,
														  BOOL bAimAtHatedPeds, VECTOR vAimPosition, ENTITY_INDEX EntityIndex = NULL,
														  BOOL bShootWhileMoving = FALSE, INT iTimeInCover = 1000, BOOL bCanPeekAndAim = FALSE,
														  BOOL bForceFaceDirection = FALSE, BOOL bForceFaceLeft = FALSE,
														  BOOL bAimShootAfterStopped = FALSE, BOOL bShootAfterStopped = FALSE,
														  INT iAimShootTime = 1000, FIRING_TYPE eFiringType = FIRING_TYPE_RANDOM_BURSTS,
														  BOOL bUnblockEvents = TRUE, FLOAT fTargetDistance = 0.5,
														  FLOAT fSlowDistance = 0.5, BOOL bUseNavmesh = TRUE, FLOAT fCombatRange = 100.0,
														  ENAV_SCRIPT_FLAGS eNavigationFlags = ENAV_DEFAULT)
	
	IF NOT IS_PED_INJURED(PedIndex)

		SEQUENCE_INDEX SequenceIndex
		
		OPEN_SEQUENCE_TASK(SequenceIndex)
			
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			
			IF ( bAimAtHatedPeds = TRUE )	//aim at hated entities makes the ped automatically change aiming to nearest hated ped, once the initial ped is dead
			
				IF IS_ENTITY_DEAD(EntityIndex)
				
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vDestination, vAimPosition, fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowDistance, bUseNavmesh, eNavigationFlags) 
					
				ELSE
					
					//if the entity does not exist, what then? might end up aiming at <<0,0,0>>
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vDestination, GET_ENTITY_COORDS(EntityIndex), fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowDistance, bUseNavmesh, eNavigationFlags) 
				
				ENDIF
				
			ELIF ( bAimAtHatedPeds = FALSE )	// aim at entity does not update aiming when that entity is dead, ped will keep aiming at dead ped
			
				IF IS_ENTITY_DEAD(EntityIndex)
			
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vDestination, vAimPosition, fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowdistance, bUseNavmesh)
					
				ELSE
				
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vDestination, EntityIndex, fMoveSpeed, bShootWhileMoving, fTargetDistance, fSlowDistance, bUseNavmesh) 
				
				ENDIF
			
			ENDIF
			
			IF ( bAimShootAfterStopped = TRUE )
			
				IF IS_ENTITY_DEAD(EntityIndex)
			
					IF ( bShootAfterStopped = TRUE )
					
						TASK_SHOOT_AT_COORD(NULL, vAimPosition, iAimShootTime, eFiringType)
					
					ELSE
					
						TASK_AIM_GUN_AT_COORD(NULL, vAimPosition, iAimShootTime)
					
					ENDIF
				
				ELSE
				
					IF ( bShootAfterStopped = TRUE )
					
						TASK_SHOOT_AT_ENTITY(NULL, EntityIndex, iAimShootTime, eFiringType)
					
					ELSE
					
						TASK_AIM_GUN_AT_ENTITY(NULL, EntityIndex, iAimShootTime)
					
					ENDIF
				
				ENDIF
			
			ENDIF
			
			TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, vDestination, iTimeInCover, bCanPeekAndAim, 1.5, bForceFaceDirection, bForceFaceLeft, NULL, TRUE)
			
			IF ( bUnblockEvents = TRUE )
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			ENDIF
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, fCombatRange)
			
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		
		TASK_PERFORM_SEQUENCE(PedIndex, SequenceIndex)
		
		CLEAR_SEQUENCE_TASK(SequenceIndex)
	
	ENDIF

ENDPROC

//|========================== END COMBAT PROCEDURES & FUNCTIONS ==========================|

//|=============================== MISSION STAGES FUNCTIONS ==============================|

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_INTRO_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
                  
      	CASE 0

			IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL )
			
				IF HAS_REQUESTED_CUTSCENE_LOADED("mic_1_int", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4)
				
					REGISTER_ENTITY_FOR_CUTSCENE(psTrevor.PedIndex, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					iStageProgress++
				
				ELSE
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						//Michael's variations
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					ENDIF
				
				ENDIF
				
			ELIF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR )
				//Activate multihead blinders early
				SET_MULTIHEAD_SAFE(TRUE,TRUE)
				
				IF HAS_REQUESTED_CUTSCENE_LOADED("mic_1_int", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5)

					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
						
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
							
							psMichael.PedIndex = g_sTriggerSceneAssets.ped[0]
							
							SET_MISSION_PED_PROPERTIES(psMichael.PedIndex, RELGROUPHASH_PLAYER, FALSE, TRUE, FALSE, FALSE)
						
							#IF IS_DEBUG_BUILD
								SET_PED_NAME_DEBUG(psMichael.PedIndex, "TS_Michael")
							#ENDIF
						
							REGISTER_ENTITY_FOR_CUTSCENE(psMichael.PedIndex, "Michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
						ENDIF
					ENDIF
					
					IF 	DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
						IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
						
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
								
							vsMichaelsCar.VehicleIndex = g_sTriggerSceneAssets.veh[0]
						
							SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
							SET_VEHICLE_LIGHTS(vsMichaelsCar.VehicleIndex, SET_VEHICLE_LIGHTS_ON)
							SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS(vsMichaelsCar.VehicleIndex, TRUE)
							SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_UNLOCKED)
							REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, TAILGATER)
							
							#IF IS_DEBUG_BUILD
								SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "TS_Michaels_Car")
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Registering trigger scene vehicle for intro cutscene as Michaels_car.")
							#ENDIF
							
						ELSE
						
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
							DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
							CLEAR_AREA(vsMichaelsCar.vPosition, 10.0, TRUE)
							
							REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Michaels_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, TAILGATER)
						
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Registering Michaels_car to be created by the cutscene due to trigger scene vehcile being undriveable.")
							#ENDIF
						
						ENDIF
						
					ELSE
					
						IF 	IS_REPLAY_START_VEHICLE_AVAILABLE()
						AND GET_REPLAY_VEHICLE_OWNER(TRUE) = CHAR_TREVOR	
						AND DOES_ENTITY_EXIST(GET_MISSION_START_VEHICLE_INDEX())
						AND IS_VEHICLE_DRIVEABLE(GET_MISSION_START_VEHICLE_INDEX(), TRUE)
							
							IF IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(GET_MISSION_START_VEHICLE_INDEX(), CHAR_MICHAEL)
							
								SET_ENTITY_AS_MISSION_ENTITY(GET_MISSION_START_VEHICLE_INDEX(), TRUE, TRUE)
						
								vsMichaelsCar.VehicleIndex = GET_MISSION_START_VEHICLE_INDEX()
								
								SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
								SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
								SET_VEHICLE_LIGHTS(vsMichaelsCar.VehicleIndex, SET_VEHICLE_LIGHTS_ON)
								
								SET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex, GET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
								SET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
								SET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
								STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.5)
								
								IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)
									SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)
								ENDIF
								IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
									SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
								ENDIF
								IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
									SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
								ENDIF
								IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
									SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
								ENDIF
							
								REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, TAILGATER)
					
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Registering Trevor's mission start vehicle with intro cutscene as it was player CHAR_MICHAEL personal vehicle.")
								#ENDIF
								
							
							ELSE
							
								REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Michaels_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, TAILGATER)
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Registering Michaels_car to be created by the cutscene due to trigger scene vehicle not existing and CHAR_TREVOR mission start vehicle not being CHAR_MICHAEL personal vehicle.")
								#ENDIF
							
							ENDIF
						
						ELSE
						
							REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Michaels_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, TAILGATER)
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Registering Michaels_car to be created by the cutscene due to trigger scene vehicle not existing and mission start vehicle not existing.")
							#ENDIF
							
						ENDIF
						
					ENDIF

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					iStageProgress++
				
				ELSE
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						//Trevor's variations
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED_ID())
					ENDIF
				
				ENDIF

         	ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
				
				IF 	IS_VEHICLE_GEN_AVAILABLE(VEHGEN_MICHAEL_SAVEHOUSE)
				AND	DOES_ENTITY_EXIST(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE))
				AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE), TRUE)
				
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Vehicle gen vehicle VEHGEN_MICHAEL_SAVEHOUSE is available for mission use.")
					#ENDIF

					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE))
					
						SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE), TRUE, TRUE)
						
						vsMichaelsCar.VehicleIndex = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MICHAEL_SAVEHOUSE)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting vehicle gen vehicle VEHGEN_MICHAEL_SAVEHOUSE as mission entity.")
						#ENDIF
						
					ENDIF
					
					IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					
						STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.5)
						SET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex, GET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						SET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						SET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						
						IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)
							SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)
						ENDIF
						IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
							SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
						ENDIF
						IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
							SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
						ENDIF
						IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
							SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
						ENDIF
						
						SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
						SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
						SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
						SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)

					ENDIF
				
				ENDIF
				
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				
					SWITCH GET_REPLAY_VEHICLE_OWNER(TRUE)
					
						CASE CHAR_MICHAEL
					
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay start vehicle is available for player ped CHAR_MICHAEL.")
							#ENDIF

							IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE(), TRUE)

								IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
								
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last player CHAR_MICHAEL vehicle is model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " which is allowed for mission.")
									#ENDIF
									
									IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
									
										IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
											SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
										ENDIF
									
										vsMichaelsCar.VehicleIndex = GET_PLAYERS_LAST_VEHICLE()
										
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting last player CHAR_MICHAEL vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " as mission entity.")
										#ENDIF
									
										SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
										SET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex, GET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
										SET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
										SET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
										STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.5)
										
										IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)
											SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)
										ENDIF
										IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
											SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
										ENDIF
										IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
											SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
										ENDIF
										IF IS_VEHICLE_TYRE_BURST(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
											SET_VEHICLE_TYRE_FIXED(vsMichaelsCar.VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
										ENDIF
										
										SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
										SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
										SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
										SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
										SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)

									ELSE
									
										DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
										SET_VEHICLE_DOORS_SHUT(GET_PLAYERS_LAST_VEHICLE(), TRUE)
										SET_VEHICLE_ENGINE_ON(GET_PLAYERS_LAST_VEHICLE(), FALSE, TRUE)
										SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), vsTrevorsCar.vPosition)
										SET_ENTITY_HEADING(GET_PLAYERS_LAST_VEHICLE(), vsTrevorsCar.fHeading)
										SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
										SET_MISSION_VEHICLE_GEN_VEHICLE(GET_PLAYERS_LAST_VEHICLE(), vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)
										CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
										
										#IF IS_DEBUG_BUILD
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting last player CHAR_MICHAEL vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " as mission vehicle gen vehicle.")
										#ENDIF
										
									ENDIF
								
								ELSE
								
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last player CHAR_MICHAEL vehicle is model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " which is not allowed for mission.")
									#ENDIF
								
								ENDIF
							
							ENDIF
							
						BREAK
						
						CASE CHAR_TREVOR
					
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Replay start vehicle is available for player ped CHAR_TREVOR.")
							#ENDIF

							IF 	DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
							AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE(), TRUE)
							AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
							AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last player CHAR_TREVOR vehicle is model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " which is allowed for mission.")
								#ENDIF
								
								IF NOT IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(GET_PLAYERS_LAST_VEHICLE(), CHAR_MICHAEL)
								
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last player CHAR_TREVOR vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " is not a personal vehicle of ped CHAR_MICHAEL.")
									#ENDIF
								
									IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
										SET_ENTITY_AS_MISSION_ENTITY(GET_PLAYERS_LAST_VEHICLE())
									ENDIF
								
									SET_ENTITY_HEALTH(GET_PLAYERS_LAST_VEHICLE(), GET_ENTITY_HEALTH(GET_PLAYERS_LAST_VEHICLE()) + 500)
									SET_VEHICLE_ENGINE_HEALTH(GET_PLAYERS_LAST_VEHICLE(), GET_VEHICLE_ENGINE_HEALTH(GET_PLAYERS_LAST_VEHICLE()) + 500)
									SET_VEHICLE_PETROL_TANK_HEALTH(GET_PLAYERS_LAST_VEHICLE(), GET_VEHICLE_PETROL_TANK_HEALTH(GET_PLAYERS_LAST_VEHICLE()) + 500)
									STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE()), 2.5)
									
									IF IS_VEHICLE_TYRE_BURST(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_FRONT_LEFT)
										SET_VEHICLE_TYRE_FIXED(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_FRONT_LEFT)
									ENDIF
									IF IS_VEHICLE_TYRE_BURST(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_FRONT_RIGHT)
										SET_VEHICLE_TYRE_FIXED(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_FRONT_RIGHT)
									ENDIF
									IF IS_VEHICLE_TYRE_BURST(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_REAR_LEFT)
										SET_VEHICLE_TYRE_FIXED(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_REAR_LEFT)
									ENDIF
									IF IS_VEHICLE_TYRE_BURST(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_REAR_RIGHT)
										SET_VEHICLE_TYRE_FIXED(GET_PLAYERS_LAST_VEHICLE(), SC_WHEEL_CAR_REAR_RIGHT)
									ENDIF
								
								ELSE
								
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last player CHAR_TREVOR vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())), " is a personal vehicle of ped CHAR_MICHAEL.")
									#ENDIF
								
								ENDIF
							
							ENDIF
					
						BREAK
					
					ENDSWITCH
					
				ENDIF
				
				IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vsMichaelsCar.VehicleIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Overriding replay checkpoint vehicle with vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vsMichaelsCar.VehicleIndex)), ".")
					#ENDIF
				ENDIF
				
				DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
				//DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
				LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, TRUE)
			
				CLEAR_AREA(psMichael.vPosition, 25.0, TRUE)
				CLEAR_AREA_OF_PEDS(psMichael.vPosition, 250.0)
				CLEAR_AREA_OF_VEHICLES(psMichael.vPosition, 250.0)
				CLEAR_AREA_OF_PROJECTILES(psMichael.vPosition, 250.0)
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
				ENDIF

				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			//create player peds by extracting them from mocap cutscene
			IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL )
				IF NOT DOES_ENTITY_EXIST(psTrevor.PedIndex)
					IF DOES_CUTSCENE_ENTITY_EXIST("Trevor")
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
						
							psTrevor.PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
							
							SET_MISSION_PED_PROPERTIES(psTrevor.PedIndex, RELGROUPHASH_PLAYER, FALSE, TRUE, FALSE, FALSE)
						
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = psTrevor.PedIndex
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set up properites for mission player ped Trevor created in the intro cutscene.")
							#ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
						
			//create vehicles during mocap cutscene
			IF IS_CUTSCENE_PLAYING()

				//create Michael's car for the mission
				IF NOT DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					
						CASE CHAR_MICHAEL
							
							IF 	IS_REPLAY_START_VEHICLE_AVAILABLE()
							AND DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
							AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
							AND IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
								
								vsMichaelsCar.VehicleIndex = GET_PLAYERS_LAST_VEHICLE()
								
								IF NOT IS_ENTITY_A_MISSION_ENTITY(vsMichaelsCar.VehicleIndex)
									SET_ENTITY_AS_MISSION_ENTITY(vsMichaelsCar.VehicleIndex)
								ENDIF
								
								SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
								
								SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
								SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
								SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_UNLOCKED)
							
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "RS_Michaels_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Using player's replay start vehicle as Michael's mission vehicle for player to use.")
								#ENDIF
								
							ELSE
							
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating Michael's car during intro cutscene for player to use.")
								#ENDIF
								HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL, TRUE, -1, -1, -1 #IF IS_DEBUG_BUILD, "M_Michaels_Car" #ENDIF)
							
							ENDIF
							
						BREAK
						
						CASE CHAR_TREVOR
						
							IF DOES_CUTSCENE_ENTITY_EXIST("Michaels_car")
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_car"))
								
									vsMichaelsCar.VehicleIndex =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_car"))
									
									SET_VEHICLE_AS_MISSION_CRITICAL(vsMichaelsCar, TRUE)
									SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
									SET_VEHICLE_LIGHTS(vsMichaelsCar.VehicleIndex, SET_VEHICLE_LIGHTS_ON)
									SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS(vsMichaelsCar.VehicleIndex, TRUE)
									SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_UNLOCKED)
									
									#IF IS_DEBUG_BUILD
										SET_VEHICLE_NAME_DEBUG(vsMichaelsCar.VehicleIndex, "CS_Michaels_Car")
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle Michaels_car from the cutscene.")
									#ENDIF
									
								ENDIF
							ENDIF
					
						BREAK
						
					ENDSWITCH

				ELSE
					IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)				
						REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vsMichaelsCar.VehicleIndex)
					ENDIF
				ENDIF
				
				//create Trevor's car parked in the back of the driveway
				IF NOT DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
				
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					
						CASE CHAR_MICHAEL
							//don't create Trevor's car when player is Michael
						BREAK
				
						CASE CHAR_TREVOR
							
							IF 	IS_REPLAY_START_VEHICLE_AVAILABLE()	AND GET_REPLAY_VEHICLE_OWNER(TRUE) = CHAR_TREVOR	
							AND	DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
							
								vsTrevorsCar.VehicleIndex = GET_PLAYERS_LAST_VEHICLE()
								
								IF NOT IS_ENTITY_A_MISSION_ENTITY(vsTrevorsCar.VehicleIndex)
									SET_ENTITY_AS_MISSION_ENTITY(vsTrevorsCar.VehicleIndex)
								ENDIF
								
								SET_VEHICLE_AS_MISSION_CRITICAL(vsTrevorsCar, TRUE)
								
								SET_ENTITY_COORDS(vsTrevorsCar.VehicleIndex, vsTrevorsCar.vPosition)
								SET_ENTITY_HEADING(vsTrevorsCar.VehicleIndex, vsTrevorsCar.fHeading)
								
								SET_VEHICLE_DOORS_SHUT(vsTrevorsCar.VehicleIndex)
								SET_VEHICLE_ON_GROUND_PROPERLY(vsTrevorsCar.VehicleIndex)
								
								#IF IS_DEBUG_BUILD
									SET_VEHICLE_NAME_DEBUG(vsTrevorsCar.VehicleIndex, "RS_Trevors_Car")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Using player's replay start vehicle as Trevor's mission vehicle for player to use.")
								#ENDIF
							
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Creating Trevor's car during intro cutscene for player to use.")
								#ENDIF
								HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR, TRUE, -1, -1, -1 #IF IS_DEBUG_BUILD, "M_Trevors_Car" #ENDIF)
							ENDIF
							
						BREAK
						
					ENDSWITCH
					
				ELSE
					IF NOT IS_ENTITY_DEAD(vsTrevorsCar.VehicleIndex)
						REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vsTrevorsCar.VehicleIndex)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michaels_car.")
				#ENDIF
				
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						//don't do anything when playing mission as Michael
					BREAK
					CASE CHAR_TREVOR
						IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
							SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
							SET_VEHICLE_FORWARD_SPEED(vsMichaelsCar.VehicleIndex, 10.0)
							SET_VEHICLE_DOORS_LOCKED(vsMichaelsCar.VehicleIndex, VEHICLELOCK_UNLOCKED)
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
				
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF ( bCutsceneSkipped = FALSE )
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
								WARP_PED(PLAYER_PED_ID(), <<-808.4427, 177.8966, 71.3801>>, 18.2986, FALSE, TRUE, FALSE)
								SEQUENCE_INDEX SequenceIndex
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								OPEN_SEQUENCE_TASK(SequenceIndex)
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -809.27, 179.55, 71.15 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -811.32, 180.11, 71.15 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.92, 179.22, 71.16 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 111.3824)
									ELSE
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -809.27, 179.55, 71.15 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -811.32, 180.11, 71.15 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.17, 179.48, 71.16 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -813.92, 179.22, 71.16 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
									ENDIF
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
							ELSE
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
								WARP_PED(PLAYER_PED_ID(), <<-813.9473, 179.2056, 71.1592>>, 111.3824, FALSE, TRUE, FALSE)
							ENDIF
						ENDIF	
					BREAK
					CASE CHAR_TREVOR
						//
					BREAK
				ENDSWITCH
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Trevor.")
				#ENDIF
				
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
							IF NOT IS_ENTITY_DEAD(psTrevor.PedIndex)
								IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
										SET_PED_INTO_VEHICLE(psTrevor.PedIndex, vsMichaelsCar.VehicleIndex, VS_DRIVER)
									ENDIF
								ENDIF
							ENDIF
						ENDIF						
					BREAK
					CASE CHAR_TREVOR
						
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
							bBlockVehicleFirstPersonCamera 		= TRUE				//start blocking first person vehicle camera
							iBlockVehicleFirstPersonCameraTimer = GET_GAME_TIMER()	//start first person vehicle camera blocking timer, see B*2031566
						ENDIF
					
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							
							IF GET_IS_WAYPOINT_RECORDING_LOADED("mic1_tdrive")
								IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
										IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
											SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
										ENDIF
										SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
										SET_VEHICLE_FORWARD_SPEED(vsMichaelsCar.VehicleIndex, 10.0)
										TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, "mic1_tdrive",
																			   DRIVINGMODE_AVOIDCARS_RECKLESS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_CAMERA() returned true for camera.")
				#ENDIF
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Intro mocap cutscene has finished.")
				#ENDIF
			
				IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL AND HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL) )
				OR ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR AND HAS_MISSION_VEHICLE_BEEN_CREATED(vsTrevorsCar, TRUE, FALSE, CHAR_TREVOR) AND HAS_MISSION_VEHICLE_BEEN_CREATED(vsMichaelsCar, TRUE, FALSE, CHAR_MICHAEL) )
										
					IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorPosition))
						UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPosition))
					ENDIF
					
					REPLAY_STOP_EVENT()
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
					ELSE
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					
					MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_MICHAEL_1)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF

					RETURN TRUE
				
				ENDIF
			
			ELSE
			
				//if cutscene is skipped, fade out and remain cutscene on faded out screen
				IF (bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Intro mocap cutscene was skipped by the player.")
						#ENDIF
					ENDIF
				ENDIF

			ENDIF
			
			REQUEST_WAYPOINT_RECORDING("mic1_tdrive")
					
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Handles cell phone conversations for Michael and Trevor when they drive to the airports.
/// PARAMS:
///    iMichaelProgress - 
///    iTrevorProgress - 
PROC MANAGE_CONVERSATIONS_DURING_AIRPORT_RIDE(INT &iMichaelProgress, INT &iTrevorProgress)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL

		SWITCH iMichaelProgress
		
			CASE 0
				
				//IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					psMichael.iConversationTimer = GET_GAME_TIMER()
					iMichaelProgress++
				//ENDIF
				
			BREAK
			
			CASE 1
			
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_TTMC")
					IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
						IF NOT IS_ENTITY_DEAD(psTrevor.PedIndex)
							IF IS_PED_IN_ANY_VEHICLE(psTrevor.PedIndex)
								IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_USING(psTrevor.PedIndex))
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_TTMC", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("MCH1_TTMC", TRUE)
											ENDIF
										ELSE
											IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "MCH1_AEAA", "MICHAEL", SPEECH_PARAMS_FORCE_NORMAL)
												SET_LABEL_AS_TRIGGERED("MCH1_TTMC", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_LABEL_AS_TRIGGERED("MCH1_TTMC", TRUE)
					ENDIF
				ENDIF
			
				IF ( bMichaelTrevorPhoneCallTriggered = FALSE )

					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						//IF HAS_TIME_PASSED(10000, psMichael.iConversationTimer)
						IF NOT DOES_ENTITY_EXIST(psTrevor.PedIndex) //when Trevor is removed
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							
								IF NOT DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_TREVOR))
									REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 2)				//remove Trevor from dialogue
									ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 2, NULL, "TREVOR")	//add Trevor for dialogue with ped index null
								ENDIF
							
								IF PLAYER_CALL_CHAR_CELLPHONE(sMichael1Conversation, CHAR_TREVOR, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)
									REQUEST_ANIM_DICT("missmic1ig_zero_hit_wheel")
									bMichaelTrevorPhoneCallTriggered = TRUE
									iMichaelProgress++
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 2
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					psMichael.iConversationTimer = GET_GAME_TIMER()
					iMichaelProgress++
				ENDIF
			
			BREAK
			
			CASE 3
			
				REQUEST_ANIM_DICT("missmic1ig_zero_hit_wheel")
			
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF HAS_ANIM_DICT_LOADED("missmic1ig_zero_hit_wheel")
						IF HAS_TIME_PASSED(2000, psMichael.iConversationTimer)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_MSHIT", CONV_PRIORITY_MEDIUM)
										IF NOT IS_PED_INJURED(PLAYER_PED_ID())
											IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
												IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
													TASK_PLAY_ANIM(PLAYER_PED_ID(), "missmic1ig_zero_hit_wheel", "michael_hit_wheel",
																   SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
												ENDIF
											ENDIF
										ENDIF
										iMichaelProgress++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 4
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					psMichael.iConversationTimer = GET_GAME_TIMER()
					iMichaelProgress++
				ENDIF
			
			BREAK
			
			CASE 5
			
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF HAS_TIME_PASSED(3500, psMichael.iConversationTimer)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							IF PLAYER_CALL_CHAR_CELLPHONE(sMichael1Conversation, CHAR_DAVE, "MCH1AUD", "MCH1_CP02", CONV_PRIORITY_VERY_HIGH)
								REMOVE_ANIM_DICT("missmic1ig_zero_hit_wheel")
								iMichaelProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
		
		ENDSWITCH
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR

		//check for locates header destination blip
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			
			//resume paused conversations when player gets back in the car
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ENDIF
		
		ELSE //destination blip does not exist, player is out of the vehicle or wanted
			
			//handle wanted level dialogue when it gets added
			
//			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	//when wanted, kill conversation, pause it and play wanted conversation
//				
//				IF ( bConversationPaused = FALSE )
//				
//					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						lConversationLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
//						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					ENDIF
//					IF CREATE_CONVERSATION(sFranklin1Conversation, "FKN1AUD", "F1_WNTD", CONV_PRIORITY_HIGH)
//						bConversationPaused = TRUE
//					ENDIF
//					
//				ENDIF
//				
//			ELSE	//player not wanted, pause face to face conversations
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
				
//			ENDIF

		ENDIF
		
		SWITCH iTrevorProgress
		
			CASE 0
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)				//this blip will exist but will not be displayed
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())					//player as Trevor can now use any vehicle
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())	
						OR ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), psTrevor.vPosition) > 100.0 )
							psTrevor.iConversationTimer = GET_GAME_TIMER()
							iTrevorProgress++
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 1
			
				IF ( bMichaelTrevorPhoneCallTriggered = FALSE )
					IF ( bSafeToStartMichaelsCall = TRUE )
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							OR ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), psTrevor.vPosition) > 100.0 )
								IF  HAS_TIME_PASSED(10000, psTrevor.iConversationTimer)
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
									
										IF NOT DOES_ENTITY_EXIST(GET_PED_INDEX(CHAR_MICHAEL))
											REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, 0)				//remove Michael from dialogue
											ADD_PED_FOR_DIALOGUE(sMichael1Conversation, 0, NULL, "MICHAEL")	//add Michael for dialogue with ped index null
										ENDIF
									
										IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(sMichael1Conversation, CHAR_MICHAEL, "MCH1AUD", "MCH1_CP01T", CONV_PRIORITY_VERY_HIGH)
											bMichaelTrevorPhoneCallTriggered = TRUE
											iTrevorProgress++
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF ( bMichaelTrevorPhoneCallTriggered = TRUE )
					iTrevorProgress++
				ENDIF
				
			BREAK
			
			CASE 2
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					psTrevor.iConversationTimer = GET_GAME_TIMER()
					iTrevorProgress++
				ENDIF
							
			BREAK
			
			CASE 3
			
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						OR ( GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), psTrevor.vPosition) > 100.0 ) 
						
							//check if the player is not too close to the airport, so that the phone call with Ron makes sense
							//and not in the plane already
							IF 	NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1738.423584,3291.455322,40.149773>>, << 500.0, 500.0, 50.0 >>)
							AND NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
								IF HAS_TIME_PASSED(5000, psTrevor.iConversationTimer)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
											IF PLAYER_CALL_CHAR_CELLPHONE(sMichael1Conversation, CHAR_RON, "MCH1AUD", "MCH1_CP03", CONV_PRIORITY_VERY_HIGH)
												iTrevorProgress++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE	//if player is too close to the airport on in the plane already, then skip the conversation with Ron completely
								iTrevorProgress++
							ENDIF

						ENDIF
					ENDIF
				ENDIF

			BREAK
		
		ENDSWITCH
		
	ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_IN_CONTROL_OF_VEHICLE(VEHICLE_INDEX VehicleIndex)

	IF IS_VEHICLE_DRIVEABLE(VehicleIndex)

		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), VehicleIndex)
	
			INT iXValue, iYValue
			
			iXValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_MOVE_LR) * 255.0)
			iYValue = ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_VEH_MOVE_UD) * 255.0)

			IF (iXValue < -50 OR iXValue > 50)
			OR (iYValue < -50 OR iYValue > 50)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_AIM)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)
			
				RETURN TRUE

			ENDIF
		
		ENDIF
		
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC UPDATE_TRIGGERED_LABEL(TEXT_LABEL &sLabel)

	IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)					//if the label is not empty, which means not yet printed by locates header
	
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)				//if it was not set as triggered by script

			IF IS_THIS_PRINT_BEING_DISPLAYED(sLabel)		//if the label was displayed on screen

				SET_LABEL_AS_TRIGGERED(sLabel, TRUE)		//mark it as triggered

			ENDIF
			
		ELSE												//if the label was set as triggered by script,
															//which means it was printed by locates header
			IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sLabel)	//but is not currently being printed, cleared off screen or timed out
			
				sLabel = ""									//mark is as empty
			
			ENDIF
		
		ENDIF
		
		
	ENDIF
	
ENDPROC

PROC MANAGE_TREVOR_DRIVING_OUT(PED_STRUCT &ped, INT &iProgress)

	IF DOES_ENTITY_EXIST(ped.PedIndex)
		IF NOT IS_ENTITY_DEAD(ped.PedIndex)
		
			SWITCH iProgress
	
				CASE 0
					
					REQUEST_WAYPOINT_RECORDING("mic1_tdrive")
					
					IF GET_IS_WAYPOINT_RECORDING_LOADED("mic1_tdrive")
				
						IF NOT IS_PED_IN_VEHICLE(ped.PedIndex, vsMichaelsCar.VehicleIndex)
							IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
								SET_PED_INTO_VEHICLE(ped.PedIndex, vsMichaelsCar.VehicleIndex, VS_DRIVER)
							ENDIF
						ENDIF
						
						CLEAR_AREA_OF_PROJECTILES(vsMichaelsCar.vPosition, 5.0)
						SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_GetOutBurningVehicle, FALSE)
						SET_PED_CONFIG_FLAG(ped.PedIndex, PCF_GetOutUndriveableVehicle, FALSE)
						
						CREATE_TRACKED_POINT_FOR_COORD(iCarTrackedPoint, vsMichaelsCar.vPosition, 4.5)
					
						ped.iTimer = GET_GAME_TIMER()
						iProgress++
					ENDIF

				BREAK
				
				CASE 1
				
					IF HAS_TIME_PASSED(15000, ped.iTimer)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -818.93, 180.14, 70.23 >>, << -817.07, 175.22, 75.23 >>, 3.5) 		//front door exterior
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -791.74, 177.00, 71.33 >>, << -799.15, 174.12, 75.83 >>, 3.0) 		//garden side door
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -806.57, 185.56, 71.26 >>, << -805.63, 183.03, 73.85 >>, 1.8)		//garage door corridor, start Trevor earlier if player is trying to reach garage
					OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -817.04, 172.44,68.58 >>, << -791.70, 182.09, 79.83 >>, 18.0) 	//house
					OR ( IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -818.52, 177.49, 71.02 >>, << -796.41, 186.11, 75.10 >>, 6.5)
						 AND DOES_TRACKED_POINT_EXIST(iCarTrackedPoint) AND IS_TRACKED_POINT_VISIBLE(iCarTrackedPoint) )				//front hall interior player can see the car
					
						IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
							IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
							
								IF NOT IS_PED_IN_VEHICLE(ped.PedIndex, vsMichaelsCar.VehicleIndex)
									SET_PED_INTO_VEHICLE(ped.PedIndex, vsMichaelsCar.VehicleIndex, VS_DRIVER)
								ENDIF
								
								SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
									
								IF IS_PED_RUNNING(PLAYER_PED_ID())
								OR IS_PED_SPRINTING(PLAYER_PED_ID()) 
									SET_VEHICLE_FORWARD_SPEED(vsMichaelsCar.VehicleIndex, 15.0)
								ELSE
									SET_VEHICLE_FORWARD_SPEED(vsMichaelsCar.VehicleIndex, 12.5)
								ENDIF
								
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(ped.PedIndex, vsMichaelsCar.VehicleIndex, "mic1_tdrive", DRIVINGMODE_AVOIDCARS_RECKLESS)				
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.PedIndex, TRUE)
								
								CLEANUP_TRACKED_POINT(iCarTrackedPoint)
								
								TRACK_VEHICLE_FOR_IMPOUND(vsMichaelsCar.VehicleIndex, CHAR_MICHAEL) // Send this to Michael's impound when deleted

								vsMichaelsCar.bAutoDriveActive = FALSE	//use this flag to control the michael's house gate

								iProgress++
								
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE 2
				
					IF GET_SCRIPT_TASK_STATUS(ped.PedIndex, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK

						IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
							IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
							
								#IF IS_DEBUG_BUILD
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsMichaelsCar.VehicleIndex, GET_STRING_FROM_INT(GET_VEHICLE_WAYPOINT_PROGRESS(vsMichaelsCar.VehicleIndex)), 1.0)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsMichaelsCar.VehicleIndex, GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(vsMichaelsCar.VehicleIndex)), 2.0)
								#ENDIF
							
								IF GET_VEHICLE_WAYPOINT_PROGRESS(vsMichaelsCar.VehicleIndex) >= 10
								OR IS_ENTITY_AT_COORD(vsMichaelsCar.VehicleIndex,<<-845.781982,159.075562,67.057610>>, << 1.5, 3.0, 2.0 >>)
									bShouldDisplayMichaelsObjective = TRUE
								ENDIF
								
								IF GET_VEHICLE_WAYPOINT_PROGRESS(vsMichaelsCar.VehicleIndex) >= 45 //50
								OR ( GET_VEHICLE_WAYPOINT_PROGRESS(vsMichaelsCar.VehicleIndex) >= 35 AND GET_DISTANCE_BETWEEN_PEDS(ped.PedIndex, PLAYER_PED_ID()) > 25.0 )
								
									IF NOT IS_ENTITY_ON_SCREEN(vsMichaelsCar.VehicleIndex)
									OR IS_ENTITY_OCCLUDED(vsMichaelsCar.VehicleIndex)
										DELETE_PED(ped.PedIndex)
										DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
										REMOVE_WAYPOINT_RECORDING("mic1_tdrive")
									ELSE
										TASK_VEHICLE_MISSION_PED_TARGET(ped.PedIndex, vsMichaelsCar.VehicleIndex, PLAYER_PED_ID(), MISSION_FLEE, 20.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000.0, 10.0)
										SET_PED_KEEP_TASK(ped.PedIndex, TRUE)
										SET_PED_AS_NO_LONGER_NEEDED(ped.PedIndex)
										SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
										REMOVE_WAYPOINT_RECORDING("mic1_tdrive")
									ENDIF
									
								ENDIF
								
								//fix for B*1815090
								//delete Trevor when he is off screen if the player is using a vehicle to chase after him
								IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								
									IF NOT DOES_TRACKED_POINT_EXIST(iCarTrackedPoint)
										CREATE_TRACKED_POINT_FOR_COORD(iCarTrackedPoint, GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 4.0)
									ELSE
										SET_TRACKED_POINT_INFO(iCarTrackedPoint, GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 4.0)
									ENDIF
									
									IF ( GET_VEHICLE_WAYPOINT_PROGRESS(vsMichaelsCar.VehicleIndex) >= 17 AND GET_DISTANCE_BETWEEN_PEDS(ped.PedIndex, PLAYER_PED_ID()) > 35.0 )
									
										IF DOES_TRACKED_POINT_EXIST(iCarTrackedPoint) AND NOT IS_TRACKED_POINT_VISIBLE(iCarTrackedPoint)
											DELETE_PED(ped.PedIndex)
											DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
											REMOVE_WAYPOINT_RECORDING("mic1_tdrive")
											CLEANUP_TRACKED_POINT(iCarTrackedPoint)	
										ENDIF
									ENDIF
									
									//fix for B*1956131 and B*1956140
									//delete Trevor's car earlier if player is trying to use a bike
									IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
									
										IF ( GET_VEHICLE_WAYPOINT_PROGRESS(vsMichaelsCar.VehicleIndex) >= 10 )
									
											IF DOES_TRACKED_POINT_EXIST(iCarTrackedPoint) AND NOT IS_TRACKED_POINT_VISIBLE(iCarTrackedPoint)
												DELETE_PED(ped.PedIndex)
												DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
												REMOVE_WAYPOINT_RECORDING("mic1_tdrive")
												CLEANUP_TRACKED_POINT(iCarTrackedPoint)	
											ENDIF
											
										ENDIF
									
									ENDIF
									
									
								ENDIF
								
							ENDIF
						ENDIF

					ENDIF

				BREAK

			ENDSWITCH
		
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GET_TO_AIRPORT_COMPLETED(INT &iStageProgress)

	//don't use locates header J skip
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	MANAGE_CONVERSATIONS_DURING_AIRPORT_RIDE(psMichael.iConversationProgress, psTrevor.iConversationProgress)

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	
		CASE CHAR_MICHAEL
		
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			ENDIF

			IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			OR IS_PLAYER_CHANGING_CLOTHES()						//check if player changed clothes before cutscene
		  
				IF HAS_THIS_CUTSCENE_LOADED("mic_1_mcs_1")
				OR HAS_CUTSCENE_LOADED()
				OR IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()							//remove cutscene from memory if player changed clothes
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player browsing items in a shop or changing clothes. Removing cutscene.")
					#ENDIF
				ENDIF
				                        
			ELSE
			
				IF GET_DISTANCE_BETWEEN_COORDS(<< -1037.0256, -2735.8008, 19.1693 >>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < DEFAULT_CUTSCENE_LOAD_DIST
			
					//request mocap cutscene for Michael
					REQUEST_CUTSCENE("mic_1_mcs_1")
					
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())	//Michael's variations
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting cutscene ped components for cutscene ped Michael from player ped.")
						#ENDIF
					ENDIF
					
				ELSE
				
					IF HAS_THIS_CUTSCENE_LOADED("mic_1_mcs_1")
					OR HAS_CUTSCENE_LOADED()
					OR IS_CUTSCENE_ACTIVE()
						REMOVE_CUTSCENE()
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
						#ENDIF
					ENDIF

				ENDIF
				
			ENDIF
			
			MANAGE_TREVOR_DRIVING_OUT(psTrevor, iTrevorDriveOutProgress)
		
			SWITCH iStageProgress
			
				CASE 0
								
					SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<< -1039.57, -2664.37, 12.83 >>, 30.0)
				
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DRIVE_TO_AIRPORT")
						START_AUDIO_SCENE("MI_1_MIC_DRIVE_TO_AIRPORT")
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
						
							SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_RUN)
						
							IF ( bPlayerPedScriptTaskInterrupted = FALSE )
								IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
								OR (ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0) < -20 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 255.0) > 20)
								OR (ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0) < -20 OR ROUND(GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 255.0) > 20)
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									bPlayerPedScriptTaskInterrupted = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF ( bShouldDisplayMichaelsObjective = FALSE )
							IF ( iMichaelsObjectiveTimer = 0 )
								iMichaelsObjectiveTimer = GET_GAME_TIMER()
							ELSE
								IF HAS_TIME_PASSED(7500, iMichaelsObjectiveTimer)
									bShouldDisplayMichaelsObjective = TRUE
								ENDIF
							ENDIF
						ENDIF
		
						IF ( bShouldDisplayMichaelsObjective = TRUE )
							IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, << -1037.8436, -2655.3862, 12.8312 >>, << 0.25, 0.25, LOCATE_SIZE_HEIGHT >>, TRUE, "MCH1_GTLSIA", TRUE)
						ENDIF
				
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						
							IF ( bTaxiLocationSet = FALSE )
								SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(sLocatesData.LocationBlip, <<-1051.6487, -2712.0117, 12.7057>>, 241.7918)
								bTaxiLocationSet = TRUE
							ENDIF
						
							//player can drive or walk into the parking lot locates
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1047.949463,-2649.732422,12.331177>>, <<-1027.131958,-2661.605469,15.981733>>, 7.0)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1037.549438,-2680.126465,12.481735>>, <<-1047.742188,-2670.507324,15.831176>>, 7.0)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1057.455078,-2653.344482,12.331177>>, <<-1047.972046,-2671.035645,15.831175>>, 7.0)							
								iStageProgress++
							ENDIF
							//player can only be on foot in this bigger locate
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-962.393799,-2736.521484,11.818534>>, <<-1083.925415,-2666.653564,28.817842>>, 78.0)
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									iStageProgress++
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(vInteriorPosition))	//unpin the interior if it is pinned and player is away from it
							IF ( GET_DISTANCE_BETWEEN_COORDS(psMichael.vPosition, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE) > 50.0 )
								UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(vInteriorPosition))
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Unpinning interior at coordinates ", vInteriorPosition, " in memory.")
								#ENDIF
							ENDIF
						ENDIF

					ENDIF
				
				BREAK
				
				CASE 1
				
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 4.0)
									CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
									iStageProgress++
								ENDIF
							ELSE
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 2
				
					IF IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DRIVE_TO_AIRPORT")
						STOP_AUDIO_SCENE("MI_1_MIC_DRIVE_TO_AIRPORT")
					ENDIF
					
					IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, << -1037.63, -2737.48, 19.17 >>, << 0.25, 0.25, LOCATE_SIZE_HEIGHT>>, TRUE, "", TRUE)
					
					//print objective to walk to terminal
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GTTE")
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								PRINT_GOD_TEXT_ADVANCED("MCH1_GTTE", DEFAULT_GOD_TEXT_TIME, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF DOES_BLIP_HAVE_GPS_ROUTE(sLocatesData.LocationBlip)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_BLIP_COORDS(sLocatesData.LocationBlip), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0
								SET_BLIP_ROUTE(sLocatesData.LocationBlip, FALSE)
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_BLIP_COORDS(sLocatesData.LocationBlip), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 110.0
								SET_BLIP_ROUTE(sLocatesData.LocationBlip, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1026.306519,-2741.366699,18.669733>>, <<-1046.329590,-2730.003174,23.169733>>, 18.0)
									iStageProgress++
								ENDIF
							ELSE
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1046.410645,-2736.281982,18.670202>>, <<-1032.384521,-2744.483154,23.170202>>, 17.0)
									iStageProgress++
								ENDIF
							ENDIF
						ENDIF
					ENDIF

				BREAK
				
				CASE 3
				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 4.0)
								KILL_ANY_CONVERSATION()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
									CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
									iStageProgress++
								ENDIF
							ENDIF
						ELSE
							KILL_ANY_CONVERSATION()
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							iStageProgress++
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 4
				
					IF NOT DOES_CAM_EXIST(ScriptedCamera)
					
						//create animated camera for establishing shot
						DESTROY_ALL_CAMS()
						
						ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
						SET_CAM_PARAMS(ScriptedCamera, <<-1011.717896,-2738.015137,22.563950>>, <<-0.092603,-0.004846,82.387924>>, 39.955357)
						SET_CAM_PARAMS(ScriptedCamera, <<-1012.978027,-2737.459717,22.562250>>, <<0.573072,-0.004846,83.246178>>, 39.955357,
									   5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
						
						SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.2)
						
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						psMichael.iTimer = GET_GAME_TIMER()
						
						//task the player to walk to cutscene start coords
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), << -1038.07, -2738.51, 19.17 >>, PEDMOVE_WALK)
						ENDIF
					
					ELSE
					
						IF IS_CAM_RENDERING(ScriptedCamera)
							IF HAS_TIME_PASSED(4000, psMichael.iTimer)
							OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1038.07, -2738.51, 19.17 >>, << 2.0, 2.0, 2.0 >>)

								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									
										IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
											SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
										ENDIF
																				
										REMOVE_ANIM_DICT("missmic1ig_zero_hit_wheel")
									
										RETURN TRUE

									ENDIF
								ENDIF
							
							ENDIF
						ENDIF
					
					ENDIF

				BREAK
				
			ENDSWITCH
		
		BREAK
		
		CASE CHAR_TREVOR
		
			FLOAT 	fDistanceToBlip
			VECTOR	vOffsetToBlip
			VECTOR 	vPlayerPosition
		
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			UPDATE_TRIGGERED_LABEL(sLabelTOBJ1)
			UPDATE_TRIGGERED_LABEL(sLabelTOBJ2)
			UPDATE_TRIGGERED_LABEL(sLabelTGETBCK1)
			UPDATE_TRIGGERED_LABEL(sLabelTGETBCK2)
			UPDATE_TRIGGERED_LABEL(sLabelTGETIN2)
					
			SWITCH iStageProgress
			
				CASE 0
				
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_TREV_DRIVE_TO_PLANE")
						START_AUDIO_SCENE("MI_1_TREV_DRIVE_TO_PLANE")
					ENDIF
					
					IF ( psTrevor.iTimer = 0 )
						psTrevor.iTimer = GET_GAME_TIMER()
					ENDIF
					
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK
						IF ( bPlayerPedScriptTaskInterrupted = FALSE )
							IF ( psTrevor.iTimer != 0 )
								IF HAS_TIME_PASSED(3000, psTrevor.iTimer)
									IF IS_PLAYER_IN_CONTROL_OF_VEHICLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
										CLEAR_PED_TASKS(PLAYER_PED_ID())
										bPlayerPedScriptTaskInterrupted = TRUE
									ENDIF
								ENDIF
								IF HAS_TIME_PASSED(10000, psTrevor.iTimer)
									CLEAR_PED_TASKS(PLAYER_PED_ID())
									bPlayerPedScriptTaskInterrupted = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//need locates header stuff here
					IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, << 1743.932739, 3289.193848, 40.1 >>, << 0.25, 0.25, LOCATE_SIZE_HEIGHT >>, TRUE, sLabelTOBJ1, TRUE)
								
					IF ( bSafeToStartMichaelsCall = FALSE )
					
						//if player is close enough to the desert airport destination (far enough from Michael's house)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1743.932739,3289.193848,42.103081>>) < 3800.0
							
							bSafeToStartMichaelsCall = TRUE

						ELSE
						
							#IF IS_DEBUG_BUILD
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), GET_STRING_FROM_FLOAT(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1743.932739,3289.193848,42.103081>>)), 1.25)
							#ENDIF
						
						ENDIF
					
					ENDIF
				
					//if player walks into the countryside airport locate with car being outside the locate
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1743.932739,3289.193848,42.103081>>,<<18.000000,12.000000,2.000000>>)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1739.666504,3280.869141,39.093464>>, <<1726.727661,3328.008057,44.226074>>, 28.0)
						
						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
													
						FailFlags[MISSION_FAIL_TREVORS_CAR_UNDRIVABLE] = FALSE
						FailFlags[MISSION_FAIL_TREVORS_CAR_DEAD] = FALSE
						FailFlags[MISSION_FAIL_TREVORS_CAR_LEFT_BEHIND] = FALSE
						
						psTrevor.iTimer = GET_GAME_TIMER()
						
						bTrevorFlightActive = TRUE
						
						//set checkpoint reached and store mid mission snapshot vehicle
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_FLY_TO_NORTH_YANKTON), "FLY TO NORTH YANKTON", FALSE)
					
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting replay mid-mission checkpoint to ", ENUM_TO_INT(MID_MISSION_STAGE_FLY_TO_NORTH_YANKTON), " for stage ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
							IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player's replay checkpoint vehicle model is ", GET_MODEL_NAME_FOR_DEBUG(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()), ".")
							ENDIF
						#ENDIF
						
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF (GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) <> vsTrevorsCar.VehicleIndex)
								IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
									IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), VS_DRIVER)
										
										VEHICLE_INDEX TrevorsVehicleIndex
										
										TrevorsVehicleIndex = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
										
										IF DOES_ENTITY_EXIST(TrevorsVehicleIndex)
											IF NOT IS_ENTITY_DEAD(TrevorsVehicleIndex)
												IF IS_VEHICLE_DRIVEABLE(TrevorsVehicleIndex)
												
													GET_VEHICLE_SETUP(TrevorsVehicleIndex, TrevorsVehicleData)
										
													#IF IS_DEBUG_BUILD
														PRINTLN(GET_THIS_SCRIPT_NAME(), ": Saving vehicle setup for player vehicle with model ", GET_MODEL_NAME_FOR_DEBUG(TrevorsVehicleData.eModel), " for mid-mission checkpoint.")
													#ENDIF
													
												ENDIF
											ENDIF
										ENDIF

									ENDIF
								ENDIF
							ENDIF
						ENDIF
										
						iStageProgress++

					ENDIF
				
				BREAK
				
				CASE 1
		
					IF IS_AUDIO_SCENE_ACTIVE("MI_1_TREV_DRIVE_TO_PLANE")
						STOP_AUDIO_SCENE("MI_1_TREV_DRIVE_TO_PLANE")
					ENDIF
				
					//check if player as Trevor is in Trevor's plane
					IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())

							//use locates header to take care of the objective and text and blips
							IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, sLocateFlight.vPosition, sLocateFlight.vSize, FALSE, vsTrevorsPlane.VehicleIndex, sLabelTOBJ2, sLabelTGETIN2, sLabelTGETBCK2, TRUE)
							
							//remove Trevor's plane blip if it exists from previous stage
							//when Trevor was driving to the plane
							IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
								IF DOES_BLIP_EXIST(vsTrevorsPlane.BlipIndex)
									REMOVE_BLIP(vsTrevorsPlane.BlipIndex)
								ENDIF
							ENDIF
							
							//move on when player gets inside the plane
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)

								REPLAY_RECORD_BACK_FOR_TIME(10.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)

								TRIGGER_MUSIC_EVENT("MIC1_TREVOR_PLANE")
								
								START_AUDIO_SCENE("MI_1_TREV_FLY_TO_LUDENDORFF")

								iStageProgress++
								
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player as Trevor entered the plane.")
								#ENDIF
							
							ENDIF
							
						ENDIF
						
						
						//if by any chance Trevor is performing a sequence which was not cleared during the hotswap call of CLEAR_PED_TASKS
						//and now player is unable to control Trevor because he is performing a sequnce, then clear it
						//this can for example happen when ped is playing animation of getting in/out of a vehicle
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
							IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
								IF GET_SCRIPT_TASK_STATUS(psTrevor.PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
									CLEAR_PED_TASKS(psTrevor.PedIndex)
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Clearing Trevor player ped tasks. Trevor ped was performing a sequence while being the current player ped enum.")
									#ENDIF
									
								ENDIF
							ENDIF
						ENDIF
							
					ENDIF
				
				BREAK
				
				CASE 2
				
					//check if player as Trevor is in Trevor's plane
					IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					
							IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)

								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
									
									IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)					
										//remove collision flag for the plane entity
										SET_ENTITY_LOAD_COLLISION_FLAG(vsTrevorsPlane.VehicleIndex, FALSE)
									ENDIF
									
									IF ( bWeatherTypeOvercastSet = FALSE )
										SET_WEATHER_TYPE_OVERTIME_PERSIST("OVERCAST", 60.0)
										iWeatherTypeTimer = GET_GAME_TIMER()
										bWeatherTypeOvercastSet = TRUE
									ENDIF
									
									IF ( bWeatherTypeRainSet = FALSE )
										IF ( bWeatherTypeOvercastSet = TRUE)
											IF ( iWeatherTypeTimer <> 0 AND HAS_TIME_PASSED(60000, iWeatherTypeTimer) )
												SET_WEATHER_TYPE_OVERTIME_PERSIST("RAIN", 35.0)
												IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
													PLAY_SOUND_FROM_ENTITY(iRainOnPlaneSoundID, "MIC_1_RAIN_ON_PLANE_MASTER", vsTrevorsPlane.VehicleIndex)
												ENDIF
												iWeatherTypeTimer = GET_GAME_TIMER()
												bWeatherTypeRainSet = TRUE
											ENDIF
										ENDIF
									ENDIF
									
									IF ( bWeatherTypeThunderSet = FALSE )
										IF ( bWeatherTypeRainSet = TRUE)
											IF ( iWeatherTypeTimer <> 0 AND HAS_TIME_PASSED(35000, iWeatherTypeTimer) )
												SET_WEATHER_TYPE_OVERTIME_PERSIST("THUNDER", 35.0)
												bWeatherTypeThunderSet = TRUE
											ENDIF
										ENDIF
									ENDIF
									
								ELSE
									IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
										//add collision flag for the plane entity
										SET_ENTITY_LOAD_COLLISION_FLAG(vsTrevorsPlane.VehicleIndex, TRUE)
									ENDIF
								ENDIF
								
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(osGolfBall.ObjectIndex)
								IF HAS_MISSION_OBJECT_BEEN_CREATED(osGolfBall, TRUE)
									SET_ENTITY_HEADING(osGolfBall.ObjectIndex, 315.0)
									SET_ENTITY_VISIBLE(osGolfBall.ObjectIndex, FALSE)
									SET_ENTITY_COLLISION(osGolfBall.ObjectIndex, FALSE)
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								fDistanceToBlip = GET_DISTANCE_BETWEEN_COORDS(GET_BLIP_COORDS(sLocatesData.LocationBlip), vPlayerPosition)
								IF DOES_ENTITY_EXIST(osGolfBall.ObjectIndex)
									IF NOT IS_ENTITY_DEAD(osGolfBall.ObjectIndex)
										vOffsetToBlip = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(osGolfBall.ObjectIndex, vPlayerPosition)
									ENDIF
								ENDIF
								#IF IS_DEBUG_BUILD
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsTrevorsPlane.VehicleIndex, GET_STRING_FROM_FLOAT(fDistanceToBlip), 1.25)
									DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsTrevorsPlane.VehicleIndex, GET_STRING_FROM_FLOAT(vOffsetToBlip.y), 2.0)
									DRAW_DEBUG_SPHERE(GET_BLIP_COORDS(sLocatesData.LocationBlip), 500, 0, 0, 255, 128)
								#ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_TRON")
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									IF fDistanceToBlip > 0 AND fDistanceToBlip < 2200
										SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_RON, "MCH1_TRON", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
										SET_LABEL_AS_TRIGGERED("MCH1_TRON", TRUE)
									ENDIF
								ENDIF
							ENDIF

							IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << sLocateFlight.vPosition.x, sLocateFlight.vPosition.y, vPlayerPosition.z >>, sLocateFlight.vSize, FALSE, vsTrevorsPlane.VehicleIndex, sLabelTOBJ2, sLabelTGETIN2, sLabelTGETBCK2, TRUE)
							OR ( DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND vOffsetToBlip.y > 0 AND  fDistanceToBlip > 0 AND fDistanceToBlip < 500 ) //plane in front of blip
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							
								TRIGGER_MUSIC_EVENT("MIC1_FLIGHT_ARRIVING")
							
								iStageProgress++
								
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player as Trevor entered the North Yankton flight locate.")
								#ENDIF
							
							ENDIF
							
						ENDIF
						
						//stop Trevor's plane recording playback when player is playing as Trevor
						//switch off Trevor's plane invincibility
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsTrevorsPlane.VehicleIndex)
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								STOP_PLAYBACK_RECORDED_VEHICLE(vsTrevorsPlane.VehicleIndex)
								SET_ENTITY_INVINCIBLE(vsTrevorsPlane.VehicleIndex, FALSE)
								SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vsTrevorsPlane.VehicleIndex, TRUE)
							ENDIF
						ENDIF
					ENDIF
				
				BREAK
				
				CASE 3
					
					//remove Michael's car
					IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
						DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
					ENDIF
					
					//remove Trevor's car
					IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
						DELETE_VEHICLE(vsTrevorsCar.VehicleIndex)
					ENDIF
					
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					CLEANUP_MISSION_OBJECT(osGolfBall, TRUE)

					//also remove Trevor's plane on fade out
					IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vsTrevorsPlane.VehicleIndex)
					ENDIF
					
					IF NOT HAS_SOUND_FINISHED(iRainOnPlaneSoundID)
						STOP_SOUND(iRainOnPlaneSoundID)
					ENDIF
					
					REMOVE_CUTSCENE()
					
					bCutsceneTriggeredByTrevor = TRUE
					
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
					
					iStageProgress++
				
				BREAK
				
				CASE 4
		
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
					IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
					
						RETURN TRUE
					
					ENDIF
				
				BREAK
			
			ENDSWITCH
			
		BREAK
	
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MISSION_STAGE_FLY_TO_NORTH_YANKTON_COMPLETED(INT &iTrevorProgress)

	//don't use locates header J skip
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	FLOAT 	fDistanceToBlip
	VECTOR	vOffsetToBlip
	VECTOR 	vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	SWITCH iTrevorProgress

		CASE 0

			IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)

				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
					IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)					
						//remove collision flag for the plane entity
						SET_ENTITY_LOAD_COLLISION_FLAG(vsTrevorsPlane.VehicleIndex, FALSE)
					ENDIF
					
					IF ( bMusicEventTrevorPlaneTriggered = FALSE )
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
							IF TRIGGER_MUSIC_EVENT("MIC1_TREVOR_PLANE")
								START_AUDIO_SCENE("MI_1_TREV_FLY_TO_LUDENDORFF")
								bMusicEventTrevorPlaneTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF ( bWeatherTypeOvercastSet = FALSE )
						SET_WEATHER_TYPE_OVERTIME_PERSIST("OVERCAST", 60.0)
						iWeatherTypeTimer = GET_GAME_TIMER()
						bWeatherTypeOvercastSet = TRUE
					ENDIF
					
					IF ( bWeatherTypeRainSet = FALSE )
						IF ( bWeatherTypeOvercastSet = TRUE)
							IF ( iWeatherTypeTimer <> 0 AND HAS_TIME_PASSED(60000, iWeatherTypeTimer) )
								SET_WEATHER_TYPE_OVERTIME_PERSIST("RAIN", 35.0)
								IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
									PLAY_SOUND_FROM_ENTITY(iRainOnPlaneSoundID, "MIC_1_RAIN_ON_PLANE_MASTER", vsTrevorsPlane.VehicleIndex)
								ENDIF
								iWeatherTypeTimer = GET_GAME_TIMER()
								bWeatherTypeRainSet = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF ( bWeatherTypeThunderSet = FALSE )
						IF ( bWeatherTypeRainSet = TRUE)
							IF ( iWeatherTypeTimer <> 0 AND HAS_TIME_PASSED(35000, iWeatherTypeTimer) )
								SET_WEATHER_TYPE_OVERTIME_PERSIST("THUNDER", 35.0)
								bWeatherTypeThunderSet = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
						//add collision flag for the plane entity
						SET_ENTITY_LOAD_COLLISION_FLAG(vsTrevorsPlane.VehicleIndex, TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				fDistanceToBlip = GET_DISTANCE_BETWEEN_COORDS(GET_BLIP_COORDS(sLocatesData.LocationBlip), vPlayerPosition)
				IF DOES_ENTITY_EXIST(osGolfBall.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osGolfBall.ObjectIndex)
						vOffsetToBlip = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(osGolfBall.ObjectIndex, vPlayerPosition)
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsTrevorsPlane.VehicleIndex, GET_STRING_FROM_FLOAT(fDistanceToBlip), 1.25)
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsTrevorsPlane.VehicleIndex, GET_STRING_FROM_FLOAT(vOffsetToBlip.y), 2.0)
					DRAW_DEBUG_SPHERE(GET_BLIP_COORDS(sLocatesData.LocationBlip), 500, 0, 0, 255, 128)
				#ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_TRON")
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF fDistanceToBlip > 0 AND fDistanceToBlip < 2200
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_RON, "MCH1_TRON", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
						SET_LABEL_AS_TRIGGERED("MCH1_TRON", TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << sLocateFlight.vPosition.x, sLocateFlight.vPosition.y, vPlayerPosition.z >> , sLocateFlight.vSize, FALSE, vsTrevorsPlane.VehicleIndex, "MCH1_GT_T3", "CMN_GENGETINPL", "CMN_GENGETBCKPL", TRUE)
			OR ( DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND vOffsetToBlip.y > 0 AND  fDistanceToBlip > 0 AND fDistanceToBlip < 500 ) //plane in front of blip
				
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
				TRIGGER_MUSIC_EVENT("MIC1_FLIGHT_ARRIVING")
			
				IF NOT IS_PED_INJURED(psMichael.PedIndex)
					CLEAR_PED_TASKS(psMichael.PedIndex)
					SET_PED_AS_NO_LONGER_NEEDED(psMichael.PedIndex)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ENDIF
				
				IF DOES_BLIP_EXIST(psMichael.BlipIndex)
					REMOVE_BLIP(psMichael.BlipIndex)
				ENDIF
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				CLEANUP_MISSION_OBJECT(osGolfBall, TRUE)
								
				//also remove Trevor's plane on fade out
				IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vsTrevorsPlane.VehicleIndex)
				ENDIF
				
				bCutsceneTriggeredByTrevor = TRUE
				
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
				
				iTrevorProgress++
			
			ENDIF

		BREAK
		
		CASE 1
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
			IF IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_OUT()
			
				RETURN TRUE
			
			ENDIF
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_AIRPORT_COMPLETED(INT &iMichaelProgress, INT &iTrevorProgress)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL

		SWITCH iMichaelProgress
		
			CASE 0
		  
	      		IF HAS_REQUESTED_CUTSCENE_LOADED("mic_1_mcs_1")
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	              		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(vsCutsceneCar.VehicleIndex, "Chinese_Goon_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, ORACLE2)
										
					CLEAR_PRINTS()
					CLEAR_HELP()
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
										
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)		//make sure the cutscene fades out at the end
					
					iMichaelProgress++
				
				ELSE
				
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						//Michael's variations
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					ENDIF
					
	         	ENDIF
			
			BREAK
			
			CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
				IF IS_CUTSCENE_PLAYING()
				
					SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)	//this will stop player from being wanted for being at the airport
				
					IF DOES_CAM_EXIST(ScriptedCamera)
						DESTROY_ALL_CAMS()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						DISPLAY_HUD(TRUE)
						DISPLAY_RADAR(TRUE)
					ENDIF
				
					VEHICLE_INDEX LastPlayerVehicleIndex
				
					LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
					
					IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
					
						IF NOT IS_ENTITY_DEAD(LastPlayerVehicleIndex)
					
							IF GET_DISTANCE_BETWEEN_COORDS(psMichael.vPosition, GET_ENTITY_COORDS(LastPlayerVehicleIndex)) < 15.0
							
								IF ( LastPlayerVehicleIndex <> vsMichaelsCar.VehicleIndex )
									SET_ENTITY_AS_MISSION_ENTITY(LastPlayerVehicleIndex)
								ENDIF

								SET_ENTITY_COORDS(LastPlayerVehicleIndex, vsMichaelsCar.vPosition)
								SET_ENTITY_HEADING(LastPlayerVehicleIndex, vsMichaelsCar.fHeading)
								SET_VEHICLE_ON_GROUND_PROPERLY(LastPlayerVehicleIndex)
								SET_VEHICLE_DOORS_SHUT(LastPlayerVehicleIndex, TRUE)
								SET_VEHICLE_ENGINE_ON(LastPlayerVehicleIndex, FALSE, TRUE)
							
							ENDIF
							
						ENDIF
						
					ENDIF

					//clear area when starting the cutscene
					//CLEAR_AREA(psMichael.vPosition, 25.0, TRUE)
					STOP_FIRE_IN_RANGE(psMichael.vPosition, 25.0)
					CLEAR_AREA_OF_VEHICLES(psMichael.vPosition, 25.0)
					CLEAR_AREA_OF_PROJECTILES(psMichael.vPosition, 25.0)
					REMOVE_PARTICLE_FX_IN_RANGE(psMichael.vPosition, 25.0)
					
					//clear area of peds that could run into the cutscene area, see B*1799643
					CLEAR_AREA_OF_PEDS(<< -1035.41455, -2743.94922, 19.16928 >>, 6.0)
					CLEAR_AREA_OF_PEDS(<< -1035.51782, -2739.14380, 19.16928 >>, 3.5)
				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
					ENDIF
				
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					iMichaelProgress++
					
				ENDIF
			
			BREAK
			
			CASE 2
			
				VEHICLE_INDEX LastPlayerVehicleIndex
				
				LastPlayerVehicleIndex = GET_PLAYERS_LAST_VEHICLE()
				
				IF DOES_ENTITY_EXIST(LastPlayerVehicleIndex)
					IF ( LastPlayerVehicleIndex <> vsMichaelsCar.VehicleIndex )
						IF IS_ENTITY_A_MISSION_ENTITY(LastPlayerVehicleIndex)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(LastPlayerVehicleIndex)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vsCutsceneCar.VehicleIndex)
					IF DOES_CUTSCENE_ENTITY_EXIST("Chinese_Goon_Car")
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Chinese_Goon_Car"))
							vsCutsceneCar.VehicleIndex =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Chinese_Goon_Car"))
							SET_VEHICLE_ENGINE_ON(vsCutsceneCar.VehicleIndex, TRUE, TRUE)
							SET_VEHICLE_LIGHTS(vsCutsceneCar.VehicleIndex, SET_VEHICLE_LIGHTS_ON)
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created vehicle Chinese_Goon_Car in the cutscene.")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Chinese_Goon_Car")
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Chinese_Goon_Car.")
					#ENDIF
					
					IF DOES_ENTITY_EXIST(vsCutsceneCar.VehicleIndex)
						DELETE_VEHICLE(vsCutsceneCar.VehicleIndex)
					ENDIF
					
				ENDIF

				IF HAS_CUTSCENE_FINISHED()

					IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vsMichaelsCar.VehicleIndex)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vsCutsceneCar.VehicleIndex)
						DELETE_VEHICLE(vsCutsceneCar.VehicleIndex)
					ENDIF

					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					iMichaelProgress++

				ELSE
				
					//if cutscene is skipped keep the screen faded out
					//it will be faded in during the next stage
					IF WAS_CUTSCENE_SKIPPED()
					
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)

					ENDIF
					
					//start requesting prologue IPLs
					REQUEST_PROLOGUE_IPLS(#IF IS_DEBUG_BUILD bPrintDebugOutput #ENDIF)
					REQUEST_PROLOGUE_GRAVE_IPL(FALSE)

				ENDIF
			
			BREAK
			
			CASE 3
				//This is a hacky fix for 2834796 where the warning message forces a fade-in
				IF NOT IS_SCREEN_FADING_OUT() OR NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			
				IF IS_SCREEN_FADED_OUT()
				
					IF 	ARE_PROLOGUE_IPLS_ACTIVE()
					AND IS_PROLOGUE_GRAVE_IPL_ACTIVE(FALSE)
						REPLAY_STOP_EVENT()
						RETURN TRUE
						
					ENDIF
				
				ENDIF
			
			BREAK
		
		ENDSWITCH
	
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		
		IF IS_SCREEN_FADED_OUT()
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
			SWITCH iTrevorProgress
			
				CASE 0

					//start requesting prologue IPLs
					REQUEST_PROLOGUE_IPLS(#IF IS_DEBUG_BUILD TRUE #ENDIF)
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
					
					iTrevorProgress++
				
				BREAK
				
				CASE 1

					IF 	ARE_PROLOGUE_IPLS_ACTIVE(#IF IS_DEBUG_BUILD TRUE #ENDIF)
					AND IS_PROLOGUE_GRAVE_IPL_ACTIVE(TRUE)
				
						REPLAY_STOP_EVENT()
				
						RETURN TRUE
						
					ENDIF

				BREAK
			
			ENDSWITCH
		
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

PROC MANAGE_MISSION_TRAIN_DURING_DRIVE_TO_GRAVEYARD(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<4363.457031,-5083.799805,109.941292>>,<<32.000000,164.000000,8.000000>>)
			
				iProgress++
				
			ENDIF
		
		BREAK
	
		CASE 1
	
			IF 	REQUEST_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")
			AND HAS_MISSION_TRAIN_BEEN_CREATED(tsTrain, fFirstTrainSpeed, FALSE)

				IF HAS_SOUND_FINISHED(iTrainBellsSoundID)
					PLAY_SOUND_FROM_COORD(iTrainBellsSoundID, "Train_Bell", << 3882.67, -5017.19, 113.72 >>, "Prologue_Sounds")
				ENDIF
						
				iProgress++
			
			ENDIF
			
		BREAK
		
		CASE 2
		
			IF 	DOES_ENTITY_EXIST(tsTrain.VehicleIndex)
			AND	DOES_ENTITY_EXIST(tsTrain.PedIndex)
			
				IF IS_ENTITY_AT_COORD(tsTrain.VehicleIndex, << 3960.78, -5569.91, 110.00 >>, << 10.0, 10.0, 10.0 >>)

					DELETE_PED(tsTrain.PedIndex)
					DELETE_MISSION_TRAIN(tsTrain.VehicleIndex)
					
					IF NOT HAS_SOUND_FINISHED(iTrainBellsSoundID)
						STOP_SOUND(iTrainBellsSoundID)
					ENDIF
					
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleted mission train.")
					#ENDIF
				
					iProgress++
				
				ENDIF
			
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_WEAPONS_HELP_TEXT()

	IF ( bWeaponsHelpDisplayed = FALSE )
		IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_HWEAP")
			IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
				PRINT_HELP("MCH1_HWEAP", DEFAULT_HELP_TEXT_TIME)
				SET_LABEL_AS_TRIGGERED("MCH1_HWEAP", TRUE)
				bWeaponsHelpDisplayed = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_FLASHBACK_STREAM(INT &iProgress)

	IF ( bStartFlashBackStream = TRUE )
		SWITCH iProgress
			CASE 0
				IF NOT IS_STREAM_PLAYING()
					
					IF IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
						STOP_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
					ENDIF
				
					IF LOAD_STREAM("FLASHBACK_01", "MICHAEL1_FLASHBACK_SOUNDSET")
						IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStreamTriggerPosition1, << 42.0, 128.0, 8.0 >>)
								PLAY_STREAM_FRONTEND()
								iFlashbackSubtitleProgress	= 0
								SET_LABEL_AS_TRIGGERED("FLASHBACK_01", TRUE)	
								START_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
								iProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_STREAM_PLAYING()
				
					IF IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
						STOP_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
					ENDIF
				
					IF LOAD_STREAM("FLASHBACK_02", "MICHAEL1_FLASHBACK_SOUNDSET")
						IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStreamTriggerPosition2, << 132.0, 196.0, 16.0 >>)
								PLAY_STREAM_FRONTEND()
								iFlashbackSubtitleProgress	= 10
								SET_LABEL_AS_TRIGGERED("FLASHBACK_02", TRUE)	
								START_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
								iProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_STREAM_PLAYING()
				
					IF IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
						STOP_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
					ENDIF
				
					IF LOAD_STREAM("FLASHBACK_03", "MICHAEL1_FLASHBACK_SOUNDSET")
						IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vStreamTriggerPosition3, << 72.0, 72.0, 16.0 >>)
								PLAY_STREAM_FRONTEND()
								iFlashbackSubtitleProgress	= 20
								SET_LABEL_AS_TRIGGERED("FLASHBACK_03", TRUE)	
								START_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
								iProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH iFlashbackSubtitleProgress
		CASE 0
			IF HAS_LABEL_BEEN_TRIGGERED("FLASHBACK_01")
				IF IS_STREAM_PLAYING()
					IF GET_STREAM_PLAY_TIME() >= 100
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
							PRINT_FAKE_SUBTITLE("MCH1_FLASH1", 10000)
						ENDIF
						iFlashbackSubtitleProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 10300
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH2", DEFAULT_GOD_TEXT_TIME)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 13500
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH3", 10000)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 23000
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH4", DEFAULT_GOD_TEXT_TIME)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 26500
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH5", 10000)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 36500
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH6", 10000)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 42300
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH7", 5000)
						iFlashbackSubtitleProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			IF HAS_LABEL_BEEN_TRIGGERED("FLASHBACK_02")
				IF IS_STREAM_PLAYING()
					IF GET_STREAM_PLAY_TIME() >= 100
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
							PRINT_FAKE_SUBTITLE("MCH1_FLASH8", DEFAULT_GOD_TEXT_TIME)
						ENDIF
						iFlashbackSubtitleProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 11
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 7500
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH9", DEFAULT_GOD_TEXT_TIME)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 12
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 12500
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH10", 10000)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 13
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 22600
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH11", 4300)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 20
			IF HAS_LABEL_BEEN_TRIGGERED("FLASHBACK_03")
				IF IS_STREAM_PLAYING()
					IF GET_STREAM_PLAY_TIME() >= 100
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
							PRINT_FAKE_SUBTITLE("MCH1_FLASH12", DEFAULT_GOD_TEXT_TIME)
						ENDIF
						iFlashbackSubtitleProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 21
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 4000
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH13", DEFAULT_GOD_TEXT_TIME)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 22
			IF IS_STREAM_PLAYING()
				IF GET_STREAM_PLAY_TIME() >= 7500
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MCH1_GBRLC")
						PRINT_FAKE_SUBTITLE("MCH1_FLASH14", 4500)
					ENDIF
					iFlashbackSubtitleProgress++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_STREAM_PLAYING()
		IF NOT IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex) AND bMusicEventArrivedChurchTriggered = FALSE //if not in mission vehicle and not arrived at church yet
		
			STOP_STREAM()
			STOP_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
			
			IF IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH1")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH2")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH3")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH4")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH5")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH6")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH7")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH8")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH9")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH10")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH11")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH12")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH13")
			OR IS_THIS_PRINT_BEING_DISPLAYED("MCH1_FLASH14")
				CLEAR_PRINTS()
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Fix for B*1713614, disable jumpin and climbing in norty yankton locates that allowed player to escape playable map area.
PROC DISABLE_JUMPING_AND_CLIMBING_TO_STOP_MAP_ESCAPE()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3198.784912,-4801.354004,115.809845>>,<<5.000000,5.000000,5.000000>>)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3187.924316,-4756.938965,115.639969>>,<<20.000000,46.000000,5.000000>>)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GET_TO_GRAVEYARD_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	IF DOES_CAM_EXIST(ScriptedCamera)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	IF ( GET_CLOCK_HOURS() < 22 )
	OR ( GET_CLOCK_HOURS() = 23 AND GET_CLOCK_MINUTES() = 59)
		SET_CLOCK_TIME(0, 0, 0)
	ENDIF

	MANAGE_WEAPONS_HELP_TEXT()
	UPDATE_TRIGGERED_LABEL(sLabelGTLC)
	MANAGE_FLASHBACK_STREAM(iFlashbackStreamProgress)
	MANAGE_MISSION_TRAIN_DURING_DRIVE_TO_GRAVEYARD(tsTrain.iProgress)
	
	DISABLE_JUMPING_AND_CLIMBING_TO_STOP_MAP_ESCAPE()	//fix for B*1713614

	#IF IS_DEBUG_BUILD
		IF ( bDrawDebugStates = TRUE )
			DRAW_DEBUG_VEHICLE_RECORDING_INFO(vsMichaelsCar.VehicleIndex, 1.0)
		ENDIF
	#ENDIF

	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DRIVE_TO_GRAVEYARD")
				START_AUDIO_SCENE("MI_1_MIC_DRIVE_TO_GRAVEYARD")
			ENDIF

			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
			
				STOP_PED_SPEAKING(PLAYER_PED_ID(), TRUE)
			
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
				SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, TRUE, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
				MODIFY_VEHICLE_TOP_SPEED(vsMichaelsCar.VehicleIndex, -20.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
			ENDIF

			INITIALISE_ROADNODES(sRoadNodes)
			
			DISABLE_CELLPHONE(TRUE)
	
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
	
			iStageProgress++
		
		BREAK
		
		CASE 1	//create scripted camera for Ludendorff arrival
		
			//create scripted camera for graveyard arrival
			IF NOT DOES_CAM_EXIST(ScriptedCamera)
				
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				SET_CAM_PARAMS(ScriptedCamera,<<5442.244141,-5127.124023,78.327057>>,<<-0.382159,-0.560862,56.809986>>,41.604355)
							   
			  	SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.1)
			   
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
				#ENDIF

				//SET_FRONTEND_RADIO_ACTIVE(FALSE)

			ENDIF
			
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			
				NEW_LOAD_SCENE_START(<< 5442.24, -5127.12, 78.24 >>, << -0.84, 0.55, -0.01>>, 128.0)
				
				iLoadSceneTimer = GET_GAME_TIMER()
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
				#ENDIF
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Load scene timer: ", GET_GAME_TIMER() - iLoadSceneTimer, ".")
			#ENDIF
			
			IF IS_NEW_LOAD_SCENE_LOADED()
			OR HAS_TIME_PASSED(10000, iLoadSceneTimer)

				#IF IS_DEBUG_BUILD
					IF HAS_TIME_PASSED(10000, iLoadSceneTimer)
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to iLoadSceneTimer timeout.")
					ELSE
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
					ENDIF
				#ENDIF
				
				NEW_LOAD_SCENE_STOP()
						
				iStageProgress++
			
			ENDIF
		
		BREAK
		
		CASE 3
			
			//start rendering the interpolating camera
			IF DOES_CAM_EXIST(ScriptedCamera)
				IF NOT IS_CAM_RENDERING(ScriptedCamera)
			
					SET_CAM_PARAMS(ScriptedCamera,<<5442.244141,-5127.124023,78.327057>>,<<-0.382159,-0.560862,56.809986>>,41.604355)
					SET_CAM_PARAMS(ScriptedCamera,<<5442.244141,-5127.124023,78.086525>>,<<-0.382159,-0.560862,56.809986>>,41.604355, 6000,
								   GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
					
				ENDIF
			ENDIF
			
			CLEAR_AREA_OF_VEHICLES(<< 5433.34, -5122.94, 77.07 >>, 60.0)

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			
			//start vehicle recording playback
			START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE(vsMichaelsCar, sVehicleRecordingsFile, 0.75, FALSE, 3750)

			TRIGGER_MUSIC_EVENT("MIC1_DRIVE_TO_GRAVEYARD")

			bStartFlashBackStream = TRUE

			iStageProgress++
		
		BREAK
				
		CASE 4
		
			IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
				IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
				
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsMichaelsCar.VehicleIndex)
						IF GET_TIME_POSITION_IN_RECORDING(vsMichaelsCar.VehicleIndex) >= fFirstRecordingEndTime

							//destroy scripted camera
							IF DOES_CAM_EXIST(ScriptedCamera)
							
								IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
									SETTIMERA(0)
									SETTIMERB(0)
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA is ", TIMERA(), ".")
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERB is ", TIMERB(), ".")
									bFirstPersonFXTriggered = FALSE
									bCamViewModeContextChanged = TRUE
									SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
								ENDIF
								
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
							
								SET_CAM_ACTIVE(ScriptedCamera, FALSE)
								RENDER_SCRIPT_CAMS(FALSE, TRUE, iCameraInterpolationTimer)
								DESTROY_CAM(ScriptedCamera)
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								//SET_FRONTEND_RADIO_ACTIVE(TRUE)
							ENDIF
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

							vsMichaelsCar.iRecordingNumber = 004	//change the recording number for the next recording needed
							
							DISABLE_CELLPHONE(FALSE)	
							
							iStageProgress++
						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK

		CASE 5
		
			//disable default first person flash and handle one manually
			DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()

			//disable player camera control when camera is interpolating from script camera to gameplay camera
			IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
				
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERA is ", TIMERA(), ".")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": TIMERB is ", TIMERB(), ".")
				
				IF ( bCamViewModeContextChanged = TRUE )
					IF ( bFirstPersonFXTriggered = FALSE )
						IF TIMERA() > iFirstPersonFXTime
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bFirstPersonFXTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF ( bCamViewModeContextChanged = TRUE )
					SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE, CAM_VIEW_MODE_FIRST_PERSON)
					bCamViewModeContextChanged = FALSE
				ENDIF
			ENDIF

			//check for playback interruption from player controls
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsMichaelsCar.VehicleIndex)

				IF ( bVehiclePlaybackInterrupted = FALSE )

					IF GET_TIME_POSITION_IN_RECORDING(vsMichaelsCar.VehicleIndex) >= 10250.0
						STOP_PLAYBACK_RECORDED_VEHICLE(vsMichaelsCar.VehicleIndex)					
					ENDIF

					IF IS_PLAYER_IN_CONTROL_OF_VEHICLE(vsMichaelsCar.VehicleIndex)
					
						STOP_PLAYBACK_RECORDED_VEHICLE(vsMichaelsCar.VehicleIndex)
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						bVehiclePlaybackInterrupted = TRUE

					ENDIF

				ENDIF
			
			ENDIF

			//display locates stuff and objective
			IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, psMichael.vDestination, << 0.25, 0.25, LOCATE_SIZE_HEIGHT >>, TRUE, vsMichaelsCar.VehicleIndex, sLabelGTLC, "CMN_GENGETIN", "CMN_GENGETBCK", TRUE)
			
			RUN_ROADNODES_CHECK(PLAYER_PED_ID() #IF IS_DEBUG_BUILD, bPrintDebugOutput #ENDIF)

			PREPARE_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")

			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF IS_ENTITY_AT_COORD(vsMichaelsCar.VehicleIndex, << 3219.976563, -4706.303223, 112.876556 >>, << 18.0, 20.0, 2.0 >>)

					//create scripted camera for graveyard arrival
					IF NOT DOES_CAM_EXIST(ScriptedCamera)
						
						ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

						SET_CAM_PARAMS(ScriptedCamera, <<3212.395996,-4728.643555,113.433495>>,<<7.027314,0.084961,-27.492634>>,33.441017)
						SET_CAM_PARAMS(ScriptedCamera, <<3213.253906,-4726.994629,113.662704>>,<<7.027314,0.084961,-27.492634>>,33.441017, 10000,
									   GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
									   
					  	SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.1)
					   
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
						#ENDIF
						
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
						
						//SET_FRONTEND_RADIO_ACTIVE(FALSE)

					ENDIF
					
					IF DOES_CAM_EXIST(ScriptedCamera)
					
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

						START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE(vsMichaelsCar, sVehicleRecordingsFile, 1.0, FALSE, fSecondRecordingStartTime)

						DISABLE_CELLPHONE(TRUE)
						KILL_FACE_TO_FACE_CONVERSATION()
						INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()

						iStageProgress++
						
					ENDIF
					
					IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
						IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
							CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 5.0)
							REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 5.0)
						ENDIF
					ENDIF
					
					CLEAR_AREA(<< 3223.31, -4703.96, 111.83 >>, 10.0, TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
						SET_ENTITY_INVINCIBLE(vsMichaelsCar.VehicleIndex, TRUE)
						IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
			
			IF 	IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3236.007568,-4682.933105,113.858047>>,<<36.000000,16.000000,3.000000>>)
			AND IS_ENTITY_AT_COORD(vsMichaelsCar.VehicleIndex, <<3236.007568,-4682.933105,113.858047>>,<<36.000000,16.000000,3.000000>>)
					
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
				
				IF PREPARE_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")
					IF TRIGGER_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")
					ENDIF
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION()
				
				iStageProgress = 9	//skip to the last case of this stage
				
			ENDIF

			REQUEST_VEHICLE_RECORDING(vsMichaelsCar.iRecordingNumber, sVehicleRecordingsFile)
		
		BREAK
		
		CASE 6
		
			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
		
				IF ( bMusicEventArrivedChurchTriggered = FALSE)
					IF PREPARE_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")
						IF ( GET_TIME_POSITION_IN_RECORDING(vsMichaelsCar.VehicleIndex) > 3750.0 )
							IF TRIGGER_MUSIC_EVENT("MIC1_ARRIVED_CHURCH")
								bMusicEventArrivedChurchTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsMichaelsCar.VehicleIndex)
					IF GET_TIME_POSITION_IN_RECORDING(vsMichaelsCar.VehicleIndex) >= fSecondRecordingEndTime
				
						STOP_PLAYBACK_RECORDED_VEHICLE(vsMichaelsCar.VehicleIndex)
						
						SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, << 3212.0027, -4689.3232, 111.6762 >>)
						SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, 225.1631)
						
						SET_VEHICLE_LIGHTS(vsMichaelsCar.VehicleIndex, FORCE_VEHICLE_LIGHTS_OFF)
						SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
						
						psMichael.iTimer = GET_GAME_TIMER()
						
						iStageProgress++
						
					ENDIF
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 7

			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)

				IF HAS_TIME_PASSED(1000, psMichael.iTimer)
				
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
					
						IF DOES_CAM_EXIST(ScriptedCamera)
						
							SET_CAM_PARAMS(ScriptedCamera, <<3209.035889,-4688.779297,113.423576>>,<<-1.887575,-0.000000,-81.365829>>,54.466209)
							SET_CAM_PARAMS(ScriptedCamera, <<3209.035889,-4688.779297,113.423576>>,<<-1.887575,-0.000000,-78.470566>>,54.466209, 3000,
									   	   GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
									   
					  		SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.1)
						
						ENDIF
					
						SEQUENCE_INDEX SequenceIndex
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_LEAVE_VEHICLE(NULL, vsMichaelsCar.VehicleIndex)
							TASK_TURN_PED_TO_FACE_COORD(NULL, << 3255.48, -4685.06, 113.11 >>)
							TASK_LOOK_AT_COORD(NULL, << 3255.48, -4685.06, 113.11 >>, 3000)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
						CLEAR_SEQUENCE_TASK(SequenceIndex)
						
						INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
				
						psMichael.iTimer = GET_GAME_TIMER()
						bFirstPersonFXTriggered = FALSE
					
						iStageProgress++

					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 8
		
			IF DOES_CAM_EXIST(ScriptedCamera)
			
				IF HAS_TIME_PASSED(1500, psMichael.iTimer)
				
					iStageProgress++
					
				ELIF HAS_TIME_PASSED(1200, psMichael.iTimer)
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						IF ( bFirstPersonFXTriggered = FALSE )
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bFirstPersonFXTriggered = TRUE
						ENDIF
					ENDIF
				
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 9	//other cases reference this value, update them as well, when this value changes

			IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				SET_ENTITY_INVINCIBLE(vsMichaelsCar.VehicleIndex, FALSE)
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
		
			DISABLE_CELLPHONE(FALSE)
		
			bCutsceneTriggeredByMichael = TRUE

			STOP_AUDIO_SCENE("MI_1_MIC_DRIVE_TO_GRAVEYARD")
			
			IF NOT IS_STREAM_PLAYING()
				STOP_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
			ENDIF
			
			iStageProgress++
		
		BREAK
		
		CASE 10
		
			IF DOES_CAM_EXIST(ScriptedCamera)
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON

					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(45.0)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ELSE
				
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				ENDIF
				
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				DESTROY_CAM(ScriptedCamera)
				DESTROY_ALL_CAMS()
				
			ENDIF
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						
			IF 	DOES_ENTITY_EXIST(tsTrain.VehicleIndex)
			AND DOES_ENTITY_EXIST(tsTrain.PedIndex)
				DELETE_PED(tsTrain.PedIndex)
				DELETE_MISSION_TRAIN(tsTrain.VehicleIndex)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleted mission train.")
				#ENDIF
			ENDIF
			
			IF NOT HAS_SOUND_FINISHED(iTrainBellsSoundID)
				STOP_SOUND(iTrainBellsSoundID)
			ENDIF
			
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")

			RETURN TRUE
			
		BREAK

	ENDSWITCH
		
	RETURN FALSE

ENDFUNC

PROC MANAGE_GAMEPLAY_HINT()

	IF NOT IS_GAMEPLAY_HINT_ACTIVE()
		SET_GAMEPLAY_HINT_FOV(fFocusPushHintFOV)
		SET_GAMEPLAY_ENTITY_HINT(psTrevor.PedIndex, << 0.0, 0.0, 1.0>>, TRUE, -1, 2500)
		
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
		
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fFocusPushSideOffset)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fFocusPushVerticalOffset)

		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fFocusPushFollowDistanceScalar)
		SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fFocusPushBaseOrbitPitchOffset)
	ELSE
		#IF IS_DEBUG_BUILD
			IF ( bResetGameplayHint = TRUE )
				STOP_GAMEPLAY_HINT(TRUE)
				bResetGameplayHint = FALSE
			ENDIF
		#ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GET_TO_GRAVE_COMPLETED(INT &iStageProgress)

	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	
	IF ( GET_CLOCK_HOURS() < 22 )
	OR ( GET_CLOCK_HOURS() = 23 AND GET_CLOCK_MINUTES() = 59)
		SET_CLOCK_TIME(0, 0, 0)
	ENDIF
	
	MANAGE_WEAPONS_HELP_TEXT()
	DISABLE_JUMPING_AND_CLIMBING_TO_STOP_MAP_ESCAPE()	//fix for B*1713614
	
	IF GET_DISTANCE_BETWEEN_COORDS(vGravePosition, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 75.0
			
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("mic_1_mcs_2", CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5)
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			//Michael's variation
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
			//Trevor's variation
			IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
				IF NOT IS_ENTITY_DEAD(psTrevor.PedIndex)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", psTrevor.PedIndex, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				ENDIF
			ELSE
				SET_CUTSCENE_PED_OUTFIT("Trevor", GET_PLAYER_PED_MODEL(CHAR_TREVOR), OUTFIT_P2_LUDENDORFF)
			ENDIF
			//Chinese gunman's variations
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_gunman", 	PED_COMP_HAIR, 1, 0)
		ENDIF
		
	ELSE
	
		IF HAS_THIS_CUTSCENE_LOADED("mic_1_mcs_2")
		OR HAS_CUTSCENE_LOADED()
		OR IS_CUTSCENE_ACTIVE()
			REMOVE_CUTSCENE()
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player too far away from destination to keep cutscene in memory. Removing cutscene.")
			#ENDIF
		ENDIF
	
	ENDIF

	SWITCH iStageProgress
	
		CASE 0
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ENTER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,  INPUT_VEH_EXIT)

			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_WALK_TO_GRAVEYARD")
				IF IS_STREAM_PLAYING() STOP_STREAM() ENDIF	
				START_AUDIO_SCENE("MI_1_MIC_WALK_TO_GRAVEYARD")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
				IF NOT IS_STREAM_PLAYING()
					STOP_AUDIO_SCENE("MI_1_MIC_DUCKING_FOR_SPEECH_SCENE")
				ENDIF
			ENDIF
		
			IF NOT DOES_BLIP_EXIST(psMichael.BlipIndex)
				psMichael.BlipIndex = ADD_BLIP_FOR_COORD(vGravePosition)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GRAVE")
				IF DOES_BLIP_EXIST(psMichael.BlipIndex)
					PRINT_GOD_TEXT_ADVANCED("MCH1_GRAVE", DEFAULT_GOD_TEXT_TIME, TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GBTLC")
				IF DOES_BLIP_EXIST(psMichael.BlipIndex)
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 3263.05, -4704.67, 104.67 >>, << 3267.14, -4561.37, 132.76 >>, 64.0)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vsMichaelsCar.vPosition) > ABANDON_CAR_WARNING_RANGE
							CLEAR_PRINTS()
							PRINT_GOD_TEXT_ADVANCED("MCH1_GBTLC", DEFAULT_GOD_TEXT_TIME, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
				IF NOT IS_ENTITY_DEAD(psTrevor.PedIndex)
				
					REQUEST_PTFX_ASSET()
					SET_PED_RESET_FLAG(psTrevor.PedIndex, PRF_DisableFriendlyGunReactAudio, TRUE)
				
					IF NOT DOES_ENTITY_EXIST(osPropShovel.ObjectIndex)
						HAS_MISSION_OBJECT_BEEN_CREATED(osPropShovel, FALSE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(osPropShovel.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osPropShovel.ObjectIndex)
							IF NOT IS_ENTITY_ATTACHED(osPropShovel.ObjectIndex)
								ATTACH_ENTITY_TO_ENTITY(osPropShovel.ObjectIndex, psTrevor.PedIndex,
														GET_PED_BONE_INDEX(psTrevor.PedIndex, BONETAG_PH_R_HAND),vZero, vZero)
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_PTFX_ASSET_LOADED()
						IF DOES_ENTITY_EXIST(osPropShovel.ObjectIndex) AND NOT IS_ENTITY_DEAD(osPropShovel.ObjectIndex)
							IF DOES_ENTITY_EXIST(psTrevor.PedIndex) AND NOT IS_ENTITY_DEAD(psTrevor.PedIndex)
								IF IS_ENTITY_ATTACHED_TO_ENTITY(osPropShovel.ObjectIndex, psTrevor.PedIndex)
									IF IS_ENTITY_PLAYING_ANIM(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor")
										IF GET_ENTITY_ANIM_CURRENT_TIME(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor") <= 0.1
											bPTFXStarted = FALSE
										ENDIF
										IF GET_ENTITY_ANIM_CURRENT_TIME(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor") >= fParticleFXTriggerPhase
											IF ( bPTFXStarted = FALSE )
												IF START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("cs_mich1_spade_dirt_throw", psTrevor.PedIndex,
																						  vParticleFXPosition, vParticleFXRotation)
													bPTFXStarted = TRUE	
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_PLAYING_ANIM(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor")
						REQUEST_ANIM_DICT("missmic1leadinoutmic_1_mcs_2")
						IF HAS_ANIM_DICT_LOADED("missmic1leadinoutmic_1_mcs_2")
							TASK_PLAY_ANIM_ADVANCED(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor",
													vGravePosition, << 0.0, 0.0, 137.88 >>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
													AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE)
						ENDIF
					ENDIF
					
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3240.262695,-4574.430176,115.941917>>, <<3294.226807,-4574.134766,120.941917>>, 31.0)//28.0)
					
						bLeadInTiggered	= TRUE
						
						IF ( iLeadInTimer = 0 )
							iLeadInTimer = GET_GAME_TIMER()
						ENDIF
						
					ENDIF
					
					IF ( bLeadInTiggered = TRUE )
					
						MANAGE_GAMEPLAY_HINT()
						UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
					
						IF IS_ENTITY_PLAYING_ANIM(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor")
							IF HAS_TIME_PASSED(3000, iLeadIntimer)
								IF GET_ENTITY_ANIM_CURRENT_TIME(psTrevor.PedIndex, "missmic1leadinoutmic_1_mcs_2", "_leadin_trevor") > 0.75
							
									bCutsceneTriggeredByMichael = TRUE
									
									REMOVE_ANIM_DICT("missmic1leadinoutmic_1_mcs_2")
									
									IF DOES_BLIP_EXIST(psMichael.BlipIndex)
										REMOVE_BLIP(psMichael.BlipIndex)
									ENDIF
									
									STOP_AUDIO_SCENE("MI_1_MIC_DRIVE_TO_GRAVEYARD")
									STOP_AUDIO_SCENE("MI_1_MIC_WALK_TO_GRAVEYARD")
									
									RETURN TRUE
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				IF HAS_MISSION_PED_BEEN_CREATED(psTrevor, TRUE, RELGROUPHASH_PLAYER, FALSE, CHAR_TREVOR)
				
					SET_ENTITY_LOD_DIST(psTrevor.PedIndex, 150)		
					SET_PED_LOD_MULTIPLIER(psTrevor.PedIndex, 5.0)
				
					STOP_PED_SPEAKING(psTrevor.PedIndex, TRUE)
					SET_PED_CONFIG_FLAG(psTrevor.PedIndex, PCF_CannotBeTargeted, TRUE)
					SET_PED_CONFIG_FLAG(psTrevor.PedIndex, PCF_DisableExplosionReactions, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(psTrevor.PedIndex, COMP_TYPE_OUTFIT, OUTFIT_P2_LUDENDORFF, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psTrevor.PedIndex, TRUE)
					
					IF ( bDirtDecalApplied = FALSE )
						CLEAR_PED_DECORATIONS_LEAVE_SCARS(psTrevor.PedIndex)//fix for B*2018060
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_HEAD, 0.590, 0.720,  25.0, 1.0, 1.0, 0, TRUE, "basic_dirt_skin")
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_HEAD, 0.400, 0.440,  30.0, 1.0, 1.0, 1, TRUE, "basic_dirt_skin")
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_RIGHT_ARM, 0.400, 0.120, 30.0, 0.6, 1.0, 1, TRUE, "basic_dirt_skin")
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_LEFT_ARM, 0.480, 0.120, 30.0, 0.6, 1.0, 1, TRUE, "basic_dirt_skin") 
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_TORSO, 0.644, 0.407, 30.0, 1.0, 1.0, 6, TRUE, "basic_dirt_cloth")
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_TORSO, 0.860, 0.632, 90.0, 1.0, 1.0, 6, TRUE, "basic_dirt_cloth")
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_LEFT_LEG, 0.775, 0.550, 0.0, 1.0, 1.0, 0, TRUE, "basic_dirt_cloth")
						APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_RIGHT_LEG, 0.415, 0.750, 36.0, 1.0, 1.0, 1, TRUE, "basic_dirt_cloth")
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Applying basic_dirt_cloth and basic_dirt_skin damage decal to Trevor ped before mocap cutscene.")
						#ENDIF
						bDirtDecalApplied = TRUE
					ENDIF
					
					FailFlags[MISSION_FAIL_TREVOR_DEAD] = TRUE
					
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH 

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_GRAVEYARD_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress

		CASE 0

			STOP_AUDIO_SCENE("MI_1_MIC_DRIVE_TO_GRAVEYARD")
			STOP_AUDIO_SCENE("MI_1_MIC_WALK_TO_GRAVEYARD")
			STOP_AUDIO_SCENE("MI_1_TREV_FLY_TO_LUDENDORFF")

			IF ( bCutsceneTriggeredByMichael = TRUE )
			
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("mic_1_mcs_2", CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5)
			
				//go straight to cutscene start
				iStageProgress = 3

			ELIF ( bCutsceneTriggeredByTrevor = TRUE )

				CLEAR_HELP()
				CLEAR_PRINTS()
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					WARP_PED(PLAYER_PED_ID(), psMichael.vPosition, psMichael.fHeading, FALSE, FALSE, FALSE)
				ENDIF
				
				IF NOT IS_PED_INJURED(psTrevor.PedIndex)
					CLEAR_PED_TASKS(psTrevor.PedIndex)
					WARP_PED(psTrevor.PedIndex, psTrevor.vPosition, psTrevor.fHeading, FALSE, FALSE, FALSE)
				ENDIF

				SETTIMERA(0)

				iStageProgress++
			
			ENDIF

		BREAK
	
		CASE 1
		
			IF ( bCutsceneTriggeredByTrevor = TRUE )

				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISPLAY_TEXT_ON_BLACK_SCREEN("MH1_SETTING")

				IF ( TIMERA() > 5000 )
					
					iStageProgress++
				
				ENDIF
			
			ENDIF			

		BREAK
		
		CASE 2	//make sure weapons are loaded and in hand of the peds before they get registered for the cutscene
		
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISPLAY_TEXT_ON_BLACK_SCREEN("MH1_SETTING")
			
			REMOVE_PROLOGUE_GRAVE_IPL(FALSE)
			REQUEST_PROLOGUE_GRAVE_IPL(TRUE)

			REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)

			IF 	IS_PROLOGUE_GRAVE_IPL_ACTIVE(TRUE)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
			
				IPL_GROUP_SWAP_START("prologue03_grv_cov", "prologue03_grv_dug")

				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Starting prologue IPLs swap for prologue03_grv_cov to prologue03_grv_dug by calling IPL_GROUP_SWAP_START().")
				#ENDIF
				
				iStageProgress++

			ENDIF
		
		BREAK

		CASE 3

			IF ( bCutsceneTriggeredByMichael = TRUE )
			
				IF HAS_REQUESTED_CUTSCENE_LOADED("mic_1_mcs_2", CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5)
				
					IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
						IF NOT IS_ENTITY_DEAD(psTrevor.PedIndex)
							REGISTER_ENTITY_FOR_CUTSCENE(psTrevor.PedIndex, "Trevor", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(osPropShovel.ObjectIndex)
						IF NOT IS_ENTITY_DEAD(osPropShovel.ObjectIndex)
							IF IS_ENTITY_ATTACHED(osPropShovel.ObjectIndex)
								DETACH_ENTITY(osPropShovel.ObjectIndex)
							ENDIF
							REGISTER_ENTITY_FOR_CUTSCENE(osPropShovel.ObjectIndex, "MIC1_Shovel", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, osPropShovel.ModelName)
						ENDIF
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(osPropShovel.ObjectIndex, "MIC1_Shovel", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osPropShovel.ModelName)
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(psBrad.PedIndex, "Dead_Brad", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, CS_BRADCADAVER)
					REGISTER_ENTITY_FOR_CUTSCENE(osPropPickaxe.ObjectIndex, "MIC1_PickAxe", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osPropPickaxe.ModelName)
					REGISTER_ENTITY_FOR_CUTSCENE(osPropCoffin.ObjectIndex, "Coffin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osPropCoffin.ModelName)

					REGISTER_ENTITY_FOR_CUTSCENE(esWallEnemies[0].PedIndex, "Chinese_gunman", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mnEnemy)
					
					WeaponMichaelObjectIndex = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, FALSE)
					
					IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
						REMOVE_WEAPON_COMPONENT_FROM_WEAPON_OBJECT(WeaponMichaelObjectIndex, WEAPONCOMPONENT_AT_PI_SUPP_02)
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing WEAPONCOMPONENT_AT_PI_SUPP_02 from player weapon WEAPONTYPE_PISTOL for cutscene.")
						#ENDIF
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(WeaponMichaelObjectIndex, "FBI_Agent_1_Gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					CLEAR_HELP()
					CLEAR_PRINTS()

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

					START_CUTSCENE()
					
									
					bPrologueGraveIPLSwapped = TRUE
									
					iStageProgress++
				
				ENDIF
		
			ELIF ( bCutsceneTriggeredByTrevor = TRUE )
			
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	  			DISPLAY_TEXT_ON_BLACK_SCREEN("MH1_SETTING")	//display the text on black for one more frame until the cutscene is active
				
				IF 	HAS_REQUESTED_CUTSCENE_LOADED("mic_1_mcs_2")
				AND IS_PROLOGUE_GRAVE_IPL_ACTIVE(TRUE)
				AND IPL_GROUP_SWAP_IS_READY()
				
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	              		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF NOT IS_PED_INJURED(psTrevor.PedIndex)
	              		REGISTER_ENTITY_FOR_CUTSCENE(psTrevor.PedIndex, "Trevor", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(psTrevor.PedIndex, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, psTrevor.ModelName)
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(psBrad.PedIndex, "Dead_Brad", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, CS_BRADCADAVER)
					REGISTER_ENTITY_FOR_CUTSCENE(osPropShovel.ObjectIndex, "MIC1_Shovel", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osPropShovel.ModelName)
					REGISTER_ENTITY_FOR_CUTSCENE(osPropPickaxe.ObjectIndex, "MIC1_PickAxe", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osPropPickaxe.ModelName)
					REGISTER_ENTITY_FOR_CUTSCENE(osPropCoffin.ObjectIndex, "Coffin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, osPropCoffin.ModelName)
					
					REGISTER_ENTITY_FOR_CUTSCENE(esWallEnemies[0].PedIndex, "Chinese_gunman", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mnEnemy)
					
					WeaponMichaelObjectIndex = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, FALSE)
					
					IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
						REMOVE_WEAPON_COMPONENT_FROM_WEAPON_OBJECT(WeaponMichaelObjectIndex, WEAPONCOMPONENT_AT_PI_SUPP_02)
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing WEAPONCOMPONENT_AT_PI_SUPP_02 from player weapon WEAPONTYPE_PISTOL for cutscene.")
						#ENDIF
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(WeaponMichaelObjectIndex, "FBI_Agent_1_Gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					CLEAR_HELP()
					CLEAR_PRINTS()
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

					START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
					
					iStageProgress++
					
	         	ENDIF
				
			ENDIF
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				//Michael's variation
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				//Trevor's variation
				SET_CUTSCENE_PED_OUTFIT("Trevor", GET_PLAYER_PED_MODEL(CHAR_TREVOR), OUTFIT_P2_LUDENDORFF)
				//Chinese gunman's variations
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_gunman", 	PED_COMP_HAIR, 1, 0)
			ENDIF

		BREAK
		
		CASE 4
		
			IF IS_CUTSCENE_PLAYING()
			
				STOP_GAMEPLAY_HINT(TRUE)
			
				CLEAR_AREA(psMichael.vPosition, 250.0, TRUE)
				CLEAR_AREA_OF_VEHICLES(psMichael.vPosition, 250.0)

				//destroy scripted camera
				IF DOES_CAM_EXIST(ScriptedCamera)
					SET_CAM_ACTIVE(ScriptedCamera, FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(ScriptedCamera)
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					//SET_FRONTEND_RADIO_ACTIVE(TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
					DELETE_VEHICLE(vsTrevorsCar.VehicleIndex)				
				ENDIF
				
				//reposition Michael's car
				IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					IF NOT IS_ENTITY_DEAD(vsMichaelsCar.VehicleIndex)
						SET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex, vsMichaelsCar.vPosition)
						SET_ENTITY_HEADING(vsMichaelsCar.VehicleIndex, vsMichaelsCar.fHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
						SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
						SET_VEHICLE_DOORS_SHUT(vsMichaelsCar.VehicleIndex, TRUE)
						SET_VEHICLE_ENGINE_CAN_DEGRADE(vsMichaelsCar.VehicleIndex, FALSE)
						SET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex, GET_ENTITY_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						SET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						SET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(vsMichaelsCar.VehicleIndex) + 500)
						STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(vsMichaelsCar.VehicleIndex), 2.5)						
					ENDIF					
				ENDIF
			
				TRIGGER_MUSIC_EVENT("MIC1_GRAVE_CS")
			
				SET_CLOCK_TIME(0, 0, 0)
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF ( bCutsceneTriggeredByTrevor = TRUE )
				IF ( bPrologueGraveIPLSwapped = FALSE )
					IF ( GET_CUTSCENE_TIME() >= 24634 )
						REQUEST_IPL("prologue_grv_torch")
						IPL_GROUP_SWAP_FINISH()
						bPrologueGraveIPLSwapped = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Swapping prologue IPLs prologue03_grv_cov for prologue03_grv_dug by calling IPL_GROUP_SWAP_FINISH().")
						#ENDIF
						
						IF ( bDirtDecalApplied = FALSE )
							IF NOT IS_PED_INJURED(psTrevor.PedIndex)
								CLEAR_PED_DECORATIONS_LEAVE_SCARS(psTrevor.PedIndex)//fix for B*2018060
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_HEAD, 0.590, 0.720,  25.0, 1.0, 1.0, 0, TRUE, "basic_dirt_skin")
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_HEAD, 0.400, 0.440,  30.0, 1.0, 1.0, 1, TRUE, "basic_dirt_skin")
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_RIGHT_ARM, 0.400, 0.120, 30.0, 0.6, 1.0, 1, TRUE, "basic_dirt_skin")
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_LEFT_ARM, 0.480, 0.120, 30.0, 0.6, 1.0, 1, TRUE, "basic_dirt_skin") 
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_TORSO, 0.644, 0.407, 30.0, 1.0, 1.0, 6, TRUE, "basic_dirt_cloth")
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_TORSO, 0.860, 0.632, 90.0, 1.0, 1.0, 6, TRUE, "basic_dirt_cloth")
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_LEFT_LEG, 0.775, 0.550, 0.0, 1.0, 1.0, 0, TRUE, "basic_dirt_cloth")
								APPLY_PED_DAMAGE_DECAL(psTrevor.PedIndex, PDZ_RIGHT_LEG, 0.415, 0.750, 36.0, 1.0, 1.0, 1, TRUE, "basic_dirt_cloth")
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Applying basic_dirt_cloth and basic_dirt_skin damage decal to Trevor ped during mocap cutscene.")
								#ENDIF
							ENDIF
							bDirtDecalApplied = TRUE
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			IF ( bDamageDecalApplied = FALSE )
				IF IS_CUTSCENE_PLAYING()
					IF ( GET_CUTSCENE_TIME() > 173000 )
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							APPLY_PED_DAMAGE_DECAL(PLAYER_PED_ID(), PDZ_HEAD, 0.621, 0.755, 55.084, 0.01, 1.0, 1, TRUE, "bruise")
							bDamageDecalApplied = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF ( bMusicEventShootoutStartTriggered = FALSE )
				IF IS_CUTSCENE_PLAYING()
					IF ( GET_CUTSCENE_TIME() > 160000 )
						IF PREPARE_MUSIC_EVENT("MIC1_SHOOTOUT_START")
							IF ( GET_CUTSCENE_TIME() > 170500 )
								IF ( bMusicEventShootoutStartTriggered = FALSE )
									IF TRIGGER_MUSIC_EVENT("MIC1_SHOOTOUT_START")
										bMusicEventShootoutStartTriggered = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(psTrevor.PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Trevor")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
						psTrevor.PedIndex = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created ped Trevor in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(psBrad.PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Dead_Brad")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Dead_Brad"))
						psBrad.PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Dead_Brad"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created ped Dead_Brad in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osPropShovel.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("MIC1_Shovel")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MIC1_Shovel"))
						osPropShovel.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MIC1_Shovel"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object MIC1_Shovel in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osPropPickaxe.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("MIC1_PickAxe")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MIC1_PickAxe"))
						osPropPickaxe.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MIC1_PickAxe"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object MIC1_PickAxe in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(osPropCoffin.ObjectIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Coffin")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Coffin"))
						osPropCoffin.ObjectIndex =  GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Coffin"))
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created object Coffin in the cutscene.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//create enemy ped during the mocap cutscene
			IF NOT DOES_ENTITY_EXIST(esWallEnemies[0].PedIndex)
				IF DOES_CUTSCENE_ENTITY_EXIST("Chinese_gunman")
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Chinese_gunman"))
					
						esWallEnemies[0].PedIndex =  GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Chinese_gunman"))
						
						SET_ENEMY_PED_PROPERTIES(esWallEnemies[0].PedIndex, rgEnemies, WEAPONTYPE_PISTOL)
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Set up properites for enemy ped 'Chinese_gunman' in the cutscene.")
						#ENDIF

					ENDIF
				ENDIF

			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dead_Brad")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Dead_Brad.")
				#ENDIF
				
				IF NOT IS_PED_INJURED(psBrad.PedIndex)
					IF HAS_ANIM_DICT_LOADED("dead")
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iBradCadaverScene)
						
							iBradCadaverScene = CREATE_SYNCHRONIZED_SCENE(<< 3258.899, -4574.090, 115.350 >>, << 173.880, 51.480, 5.040 >>, EULER_XYZ)
							
							TASK_SYNCHRONIZED_SCENE(psBrad.PedIndex, iBradCadaverScene, "dead", "dead_g", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(psBrad.PedIndex)
						
							STOP_PED_SPEAKING(psBrad.PedIndex, TRUE)
							SET_ENTITY_INVINCIBLE(psBrad.PedIndex, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psBrad.PedIndex, TRUE)
						
						ENDIF						
					ENDIF
				ENDIF
			ENDIF 
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Trevor.")
				#ENDIF
				
				IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
					DELETE_PED(psTrevor.PedIndex)
				ENDIF
			
				IF NOT DOES_PICKUP_EXIST(PistolPickup)
						
					iPistolPickupPlacementFlags = 0
				
					SET_BIT(iPistolPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
					SET_BIT(iPistolPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
					SET_BIT(iPistolPickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))

					PistolPickup = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PISTOL, <<3258.7891, -4575.3062, 117.2570>>,
														<<-88.2000, 65.8800, 0.0000>>, iPistolPickupPlacementFlags)									 						
				ENDIF
			ENDIF
			
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Michael.")
				#ENDIF
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
					
					WARP_PED(PLAYER_PED_ID(), << 3256.5776, -4575.2520, 117.2670 >>, 267.6326, FALSE, FALSE, FALSE)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), csCoverpoints[0].vPosition, -1, TRUE, 0, TRUE, TRUE, csCoverpoints[0].CoverpointIndex)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FBI_Agent_1_Gun")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for FBI_Agent_1_Gun.")
				#ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					GIVE_WEAPON_OBJECT_TO_PED(WeaponMichaelObjectIndex, PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-16.536800)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-45.0000)
				ELSE
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-16.536800)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-72.373550)
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Chinese_gunman")
				IF NOT IS_PED_INJURED(esWallEnemies[0].PedIndex)
					WARP_PED(esWallEnemies[0].PedIndex, csCoverpoints[1].vPosition, csCoverpoints[1].fHeading, FALSE, FALSE, FALSE)
					SET_CURRENT_PED_WEAPON(esWallEnemies[0].PedIndex, WEAPONTYPE_PISTOL, TRUE)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(esWallEnemies[0].PedIndex,  csCoverpoints[1].vPosition, -1, FALSE, 0, TRUE, TRUE, csCoverpoints[1].CoverpointIndex)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(esWallEnemies[0].PedIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Enemy ped is put directly into cover in position ", csCoverpoints[1].vPosition, " when CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for 'Chinese_gunman'.")
					#ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MIC1_Shovel")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for MIC1_Shovel.")
				#ENDIF
				IF DOES_ENTITY_EXIST(osPropShovel.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPropShovel.ObjectIndex)
						SET_ENTITY_COORDS_NO_OFFSET(osPropShovel.ObjectIndex, osPropShovel.vPosition)
						SET_ENTITY_ROTATION(osPropShovel.ObjectIndex, osPropShovel.vRotation)
						FREEZE_ENTITY_POSITION(osPropShovel.ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MIC1_PickAxe")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for MIC1_PickAxe.")
				#ENDIF
				IF DOES_ENTITY_EXIST(osPropPickaxe.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPropPickaxe.ObjectIndex)
						SET_ENTITY_COORDS_NO_OFFSET(osPropPickaxe.ObjectIndex, osPropPickaxe.vPosition)
						SET_ENTITY_ROTATION(osPropPickaxe.ObjectIndex, osPropPickaxe.vRotation)
						FREEZE_ENTITY_POSITION(osPropPickaxe.ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Coffin")
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned true for Coffin.")
				#ENDIF
				IF DOES_ENTITY_EXIST(osPropCoffin.ObjectIndex)
					IF NOT IS_ENTITY_DEAD(osPropCoffin.ObjectIndex)
						SET_ENTITY_COORDS_NO_OFFSET(osPropCoffin.ObjectIndex, osPropCoffin.vPosition)
						SET_ENTITY_ROTATION(osPropCoffin.ObjectIndex, osPropCoffin.vRotation)
						FREEZE_ENTITY_POSITION(osPropCoffin.ObjectIndex, TRUE)
					ENDIF
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()			

				IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
				AND ( bCoverpointsCreated = TRUE )
				AND IS_PROLOGUE_GRAVE_IPL_ACTIVE(FALSE)
				
					IF ( bCutsceneSkipped = TRUE )
					
						IF ( bDamageDecalApplied = FALSE )
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								APPLY_PED_DAMAGE_DECAL(PLAYER_PED_ID(), PDZ_HEAD, 0.621, 0.755, 55.084, 0.01, 1.0, 1, TRUE, "bruise")
								bDamageDecalApplied = TRUE
							ENDIF
						ENDIF
					
						WAIT(2000)	//wait until player ped is in low cover pose to hide any pops from standing still into low cover
					ENDIF
					
					IF ( bCutsceneSkipped = TRUE )	//set the gameplay camera relative pitch and heading again after skip
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF IS_PED_IN_COVER(PLAYER_PED_ID())
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(-16.536800)
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(-45.0000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
					psMichael.iTimer = GET_GAME_TIMER()
				
					IF ( bCutsceneSkipped = FALSE )
						TRIGGER_MUSIC_EVENT("MIC1_GAMEPLAY_STARTS")
					ELSE
						CANCEL_MUSIC_EVENT("MIC1_SHOOTOUT_START")
						TRIGGER_MUSIC_EVENT("MIC1_ARGUE_CS_SKIP")
					ENDIF
					
					IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
						DELETE_VEHICLE(vsTrevorsCar.VehicleIndex)				
					ENDIF
					
					IF DOES_ENTITY_EXIST(WeaponMichaelObjectIndex)
						DELETE_OBJECT(WeaponMichaelObjectIndex)
					ENDIF
					
					IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
						DELETE_PED(psTrevor.PedIndex)
					ENDIF
				
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF

					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)

					RETURN TRUE
					
				ENDIF

			ELSE
			
				REQUEST_ANIM_DICT("dead")
				REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)

				//create coverpoints during cutscene
				IF ( bCoverpointsCreated = FALSE )
					CREATE_COVERPOINTS(csCoverpoints, vShootoutStartPosition)
					INITIALISE_FRAGGABLE_TOMBSTONES(FraggableTombstones)
					bCoverpointsCreated = TRUE
				ENDIF
				
				//if cutscene is skipped take control of the fade
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Cutscene was skipped by the player.")
						#ENDIF
					ENDIF
				ENDIF

			ENDIF
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_SHOOTOUT(INT &iConversationProgress, INT &iConversationTimer, INT &iEnemyTimer, INT &iMichaelTime,
										  INT &iEnemyTime, BOOL &bShootingConversationPlayed)

	PED_INDEX 	ClosestEnemyPed
	FLOAT		fDistanceToClosestEnemyPed
	INT			i, k
	INT			iEnemySpeakerID
	STRING		sEnemyVoiceName
	STRING		sEnemyConversationRoot

	SWITCH iConversationProgress
	
		CASE 0
		
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies)
				
					IF ( ClosestEnemyPed <> NULL )
					AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(ClosestEnemyPed)
				
						ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON4SPEAKERID, ClosestEnemyPed, CHINESEGOON4VOICENAME)
				
						IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSGCB", CONV_PRIORITY_MEDIUM)

							iMichaelTime = 5000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
							iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
							iConversationTimer = GET_GAME_TIMER()
							iEnemyTimer = GET_GAME_TIMER()
							
							bShootingConversationPlayed = FALSE

							iConversationProgress++
						
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSMA")
				IF ( GET_NUMBER_OF_ENEMIES_DEAD(esWallEnemies) >=2 )
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSMA", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("MCH1_GSMA", TRUE)
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF

			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSS08")
				IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
					IF DOES_ENTITY_EXIST(esVanEnemies[0].PedIndex)
					OR DOES_ENTITY_EXIST(esVanEnemies[1].PedIndex)
					OR DOES_ENTITY_EXIST(esVanEnemies[2].PedIndex)
					OR DOES_ENTITY_EXIST(esVanEnemies[3].PedIndex)
						IF ( bSmoothCloseGateTriggered = TRUE )
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								
									i = GET_RANDOM_INT_IN_RANGE(0, 4)

									IF NOT IS_PED_INJURED(esVanEnemies[i].PedIndex)
									
										FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
											REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
										ENDFOR
									
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON4SPEAKERID, esVanEnemies[i].PedIndex, CHINESEGOON4VOICENAME)
									
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSS08", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("MCH1_GSS08", TRUE)
											iConversationTimer = GET_GAME_TIMER()
											iEnemyTimer = GET_GAME_TIMER()
											iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											iEnemyTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//play dialogue for enemies exiting the van
			IF 	NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGR2")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGR3")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGR4")
				
				IF ( bPlayVanOpenConversation = TRUE )
				
					IF DOES_ENTITY_EXIST(esVanEnemies[0].PedIndex)					//check if any of the peds in van exists
					OR DOES_ENTITY_EXIST(esVanEnemies[1].PedIndex)
					OR DOES_ENTITY_EXIST(esVanEnemies[2].PedIndex)
					OR DOES_ENTITY_EXIST(esVanEnemies[3].PedIndex)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)

								i = GET_RANDOM_INT_IN_RANGE(0, 4)					//select random ped exiting the van
									
								IF NOT IS_PED_INJURED(esVanEnemies[i].PedIndex)		//check if selected ped is not injured
							
									SWITCH i	
									
										CASE 0
											iEnemySpeakerID			= CHINESEGOON2SPEAKERID
											sEnemyVoiceName			= CHINESEGOON2VOICENAME
											sEnemyConversationRoot	= "MCH1_GSGR2"
										BREAK
										
										CASE 1
											iEnemySpeakerID			= CHINESEGOON2SPEAKERID
											sEnemyVoiceName			= CHINESEGOON2VOICENAME
											sEnemyConversationRoot	= "MCH1_GSGR2"
										BREAK
										
										CASE 2
											iEnemySpeakerID			= CHINESEGOON4SPEAKERID
											sEnemyVoiceName			= CHINESEGOON4VOICENAME
											sEnemyConversationRoot	= "MCH1_GSGR3"
										BREAK
										
										CASE 3
											iEnemySpeakerID			= CHINESEGOON4SPEAKERID
											sEnemyVoiceName			= CHINESEGOON4VOICENAME
											sEnemyConversationRoot	= "MCH1_GSGR4"
										BREAK
										
									ENDSWITCH
									
									FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
										REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
									ENDFOR
									
									ADD_PED_FOR_DIALOGUE(sMichael1Conversation, iEnemySpeakerID, esVanEnemies[i].PedIndex, sEnemyVoiceName)

									IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", sEnemyConversationRoot, CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED(sEnemyConversationRoot, TRUE)
										iConversationTimer = GET_GAME_TIMER()
										iEnemyTimer = GET_GAME_TIMER()
										iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
										iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
			//play dialogue for enemy ped who exited the van and is calling out for the player
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGCA")
				IF HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGR2")
				OR HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGR3")
				OR HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGR4")
					IF ( bPlayVanExitConversation = TRUE)
						IF DOES_ENTITY_EXIST(esVanEnemies[2].PedIndex)
							IF NOT IS_PED_INJURED(esVanEnemies[2].PedIndex)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					
										FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
											REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
										ENDFOR
					
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON4SPEAKERID, esVanEnemies[2].PedIndex, CHINESEGOON4VOICENAME)
										
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSGCA", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("MCH1_GSGCA", TRUE)
											iConversationTimer = GET_GAME_TIMER()
											iEnemyTimer = GET_GAME_TIMER()
											iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											IF ( bReplayStopEventTriggered = FALSE )
												IF ( bReplayStartEventTriggered = TRUE )
													REPLAY_STOP_EVENT()
													bReplayStopEventTriggered = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//play dialogue for more enemies running from behind church
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSM04")
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3262.183105,-4629.664551,116.845070>>, << 30.0, 2.0, 3.0 >>)
					IF ( esExitEnemies[COUNT_OF(esExitEnemies) - 1].bCreated = TRUE )
						IF 	NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSM04", CONV_PRIORITY_HIGH)
								SET_LABEL_AS_TRIGGERED("MCH1_GSM04", TRUE)
								iConversationTimer = GET_GAME_TIMER()
								iEnemyTimer = GET_GAME_TIMER()
								iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
								iEnemyTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//play dialogue for enemies blocked by the train when they arrive
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSGBA")
				IF ( bEnemiesBlockedByTrain = TRUE )
					IF DOES_ENTITY_EXIST(esVan4Enemies[0].PedIndex)
					OR DOES_ENTITY_EXIST(esVan4Enemies[1].PedIndex)
					OR DOES_ENTITY_EXIST(esVan4Enemies[2].PedIndex)
					OR DOES_ENTITY_EXIST(esVan4Enemies[3].PedIndex)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)

								i = GET_RANDOM_INT_IN_RANGE(0, 4)

								IF NOT IS_PED_INJURED(esVan4Enemies[i].PedIndex)
									
									FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
										REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
									ENDFOR
									
									ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON5SPEAKERID, esVan4Enemies[i].PedIndex, CHINESEGOON5VOICENAME)
								
									IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSGBA", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("MCH1_GSGBA", TRUE)
										iConversationTimer = GET_GAME_TIMER()
										iEnemyTimer = GET_GAME_TIMER()
										iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
										iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF ( bPlayerDetected = FALSE )	//player not detected by enemies
			
				//play van exit conversations for enemies
				IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_MOVE1")
					IF ( bPlayVanExitConversation = TRUE)
						IF DOES_ENTITY_EXIST(esVanEnemies[2].PedIndex)
							IF NOT IS_PED_INJURED(esVanEnemies[2].PedIndex)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									
										FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
											REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
										ENDFOR
									
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON1SPEAKERID, esVanEnemies[2].PedIndex, CHINESEGOON1VOICENAME)
										
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_MOVE1", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("MCH1_MOVE1", TRUE)
											iConversationTimer = GET_GAME_TIMER()
											iEnemyTimer = GET_GAME_TIMER()
											iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF 	NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				
					//play dialogue when middle enemies are spawned
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_MOVE3")
						IF ( bSecondEnemyWaveSpawned = TRUE )
							IF DOES_ENTITY_EXIST(esMiddleEnemies[0].PedIndex)
							OR DOES_ENTITY_EXIST(esMiddleEnemies[1].PedIndex)
							OR DOES_ENTITY_EXIST(esMiddleEnemies[2].PedIndex)
							
								i = GET_RANDOM_INT_IN_RANGE(0, 3)

								IF NOT IS_PED_INJURED(esMiddleEnemies[i].PedIndex)
								
									FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
										REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
									ENDFOR
									
									ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON3SPEAKERID, esMiddleEnemies[i].PedIndex, CHINESEGOON3VOICENAME)
								
									IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_MOVE3", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("MCH1_MOVE3", TRUE)
										iConversationTimer = GET_GAME_TIMER()
										iEnemyTimer = GET_GAME_TIMER()
										iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
										iEnemyTime = 4000 + GET_RANDOM_INT_IN_RANGE(0, 2001)
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//play enemy searching dialogue
					IF ( esMiddleEnemies[1].bSearching = TRUE )
						IF DOES_ENTITY_EXIST(esMiddleEnemies[1].PedIndex)
							IF NOT IS_PED_INJURED(esMiddleEnemies[1].PedIndex)
								IF (esMiddleEnemies[1].iConversationTimer != 0)
									IF HAS_TIME_PASSED(7500, esMiddleEnemies[1].iConversationTimer)
									
										IF NOT IS_PED_INJURED(esMiddleEnemies[1].PedIndex)
										
											FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
												REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
											ENDFOR
										
											ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON3SPEAKERID, esMiddleEnemies[1].PedIndex, CHINESEGOON3VOICENAME)
											
										ENDIF
									
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SURR3", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
											esMiddleEnemies[1].iConversationTimer = GET_GAME_TIMER()											
										ENDIF
									ENDIF
								ELSE
									esMiddleEnemies[1].iConversationTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_TIME_PASSED(iEnemyTime, iEnemyTimer)
						
						ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies, GET_RANDOM_INT_IN_RANGE(0, 4))
					
						IF ( ClosestEnemyPed <> NULL )
						AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(ClosestEnemyPed)
					
							FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
								REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
							ENDFOR
						
							IF ( GET_NUMBER_OF_PEDS_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies, FALSE) >= 1 )
								
								SWITCH GET_RANDOM_INT_IN_RANGE(0,4)
								
									CASE 0
									
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON1SPEAKERID, ClosestEnemyPed, CHINESEGOON1VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_MOVE1", CONV_PRIORITY_MEDIUM)
											iEnemyTimer = GET_GAME_TIMER()
											iEnemyTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bShootingConversationPlayed = FALSE
										ENDIF
									
									BREAK
									
									CASE 1
										
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON1SPEAKERID, ClosestEnemyPed, CHINESEGOON1VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SURR1", CONV_PRIORITY_MEDIUM)
											iEnemyTimer = GET_GAME_TIMER()
											iEnemyTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bShootingConversationPlayed = FALSE
										ENDIF
									
									BREAK
									
									CASE 2
									
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON2SPEAKERID, ClosestEnemyPed, CHINESEGOON2VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_MOVE2", CONV_PRIORITY_MEDIUM)
											iEnemyTimer = GET_GAME_TIMER()
											iEnemyTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bShootingConversationPlayed = FALSE
										ENDIF
										
									BREAK
									
									CASE 3
									
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON2SPEAKERID, ClosestEnemyPed, CHINESEGOON2VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SURR2", CONV_PRIORITY_MEDIUM)
											iEnemyTimer = GET_GAME_TIMER()
											iEnemyTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bShootingConversationPlayed = FALSE
										ENDIF
									
									BREAK
									
								ENDSWITCH
								
							ENDIF
						
							
						ELSE
							iEnemyTimer = GET_GAME_TIMER()
							iEnemyTime = 2000 + GET_RANDOM_INT_IN_RANGE(0, 1001)
							bShootingConversationPlayed = FALSE
						ENDIF
					
					ENDIF
				
				ENDIF
			
			ELSE							//player detected by enemies
			
				//play dialogue for enemies spotting Michael
				IF 	NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT1")
				AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT2")
				AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT3")
				AND NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT4")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						
							ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies)
					
							IF ( ClosestEnemyPed <> NULL )
							
								FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
									REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
								ENDFOR
								
								SWITCH GET_RANDOM_INT_IN_RANGE(1, 4)
									CASE 1
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON1SPEAKERID, ClosestEnemyPed, CHINESEGOON1VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SPOT1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
											SET_LABEL_AS_TRIGGERED("MCH1_SPOT1", TRUE)
											iConversationTimer = GET_GAME_TIMER()
											iEnemyTimer = GET_GAME_TIMER()
											iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bDetectedConversationPlaying = TRUE
										ENDIF									
									BREAK
									CASE 2
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON2SPEAKERID, ClosestEnemyPed, CHINESEGOON2VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SPOT2", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
											SET_LABEL_AS_TRIGGERED("MCH1_SPOT2", TRUE)
											iConversationTimer = GET_GAME_TIMER()
											iEnemyTimer = GET_GAME_TIMER()
											iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bDetectedConversationPlaying = TRUE
										ENDIF
									BREAK
									CASE 3
										ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON3SPEAKERID, ClosestEnemyPed, CHINESEGOON3VOICENAME)
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SPOT3", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
											SET_LABEL_AS_TRIGGERED("MCH1_SPOT3", TRUE)
											iConversationTimer = GET_GAME_TIMER()
											iEnemyTimer = GET_GAME_TIMER()
											iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bDetectedConversationPlaying = TRUE
										ENDIF
									BREAK
								ENDSWITCH
						
							ENDIF
						ENDIF
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ELSE
				
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						TEXT_LABEL_23 CurrentConversationRoot
						
						CurrentConversationRoot 	= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF NOT IS_STRING_NULL_OR_EMPTY(CurrentConversationRoot)
							IF ARE_STRINGS_EQUAL(CurrentConversationRoot, "MCH1_SPOT1")
							OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "MCH1_SPOT2")
							OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "MCH1_SPOT3")
							OR ARE_STRINGS_EQUAL(CurrentConversationRoot, "MCH1_SPOT4")
								bDetectedConversationPlaying = TRUE
							ELSE
								bDetectedConversationPlaying = FALSE
							ENDIF
						ELSE
							bDetectedConversationPlaying = FALSE
						ENDIF
					ELSE
						bDetectedConversationPlaying = FALSE
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_SURROUND")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							
								ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies)
					
								IF ( ClosestEnemyPed <> NULL )
								
									FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
										REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
									ENDFOR
									
									SWITCH GET_RANDOM_INT_IN_RANGE(1, 4)
										CASE 1
											ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON1SPEAKERID, ClosestEnemyPed, CHINESEGOON1VOICENAME)
											IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SURR1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
												SET_LABEL_AS_TRIGGERED("MCH1_SURROUND", TRUE)
												iConversationTimer = GET_GAME_TIMER()
												iEnemyTimer = GET_GAME_TIMER()
												iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												bDetectedConversationPlaying = TRUE
											ENDIF									
										BREAK
										CASE 2
											ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON2SPEAKERID, ClosestEnemyPed, CHINESEGOON2VOICENAME)
											IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SURR2", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
												SET_LABEL_AS_TRIGGERED("MCH1_SURROUND", TRUE)
												iConversationTimer = GET_GAME_TIMER()
												iEnemyTimer = GET_GAME_TIMER()
												iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												bDetectedConversationPlaying = TRUE
											ENDIF
										BREAK
										CASE 3
											ADD_PED_FOR_DIALOGUE(sMichael1Conversation, CHINESEGOON3SPEAKERID, ClosestEnemyPed, CHINESEGOON3VOICENAME)
											IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_SURR3", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
												SET_LABEL_AS_TRIGGERED("MCH1_SURROUND", TRUE)
												iConversationTimer = GET_GAME_TIMER()
												iEnemyTimer = GET_GAME_TIMER()
												iMichaelTime = 6000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												bDetectedConversationPlaying = TRUE
											ENDIF
										BREAK
									ENDSWITCH
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			
				IF 	NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			
					//play Michael's shooting dialogue
					IF ( bShootingConversationPlayed = FALSE )
						//check for number of alive enemies
						IF ( GET_NUMBER_OF_PEDS_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies, FALSE) > 0 )
							IF IS_PLAYER_SHOOTING_AT_ANY_PEDS(PLAYER_PED_ID())
						
								SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)	//play shooting conversation with 75% chance
							
									CASE 0
									CASE 1
									CASE 2
								
										IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSM02", CONV_PRIORITY_HIGH)
											iConversationTimer = GET_GAME_TIMER()
											iMichaelTime = 9000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
											bShootingConversationPlayed = TRUE
										ENDIF
										
									BREAK
									
									DEFAULT
										//play no conversation, 25% chance
									BREAK
									
								ENDSWITCH
								
							ENDIF
						ENDIF
						
					ENDIF
				
					//play Michael's random dialogue
					IF HAS_TIME_PASSED(iMichaelTime, iConversationTimer)
			
						ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies)	//get closest enemy
			
						IF ( ClosestEnemyPed <> NULL )	//if the closest enemy ped exists
						
							fDistanceToClosestEnemyPed = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ClosestEnemyPed)	//get distance to enemy
						
							IF ( fDistanceToClosestEnemyPed < 35.0 )	//do a distance check
						
								IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSM01", CONV_PRIORITY_MEDIUM)
									iConversationTimer = GET_GAME_TIMER()
									iMichaelTime = 9000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
									bShootingConversationPlayed = FALSE
								ENDIF
								
							ELSE
								iConversationTimer = GET_GAME_TIMER()					//if distance check failed
								iMichaelTime = 4000 + GET_RANDOM_INT_IN_RANGE(0, 2001)	//reset conversation timers and flags
								bShootingConversationPlayed = FALSE						//set time amount between checks to be lower than usual 
							ENDIF

						ELSE
							iConversationTimer = GET_GAME_TIMER()
							iMichaelTime = 9000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
							bShootingConversationPlayed = FALSE
						ENDIF

					ENDIF
				
					//play enemy random dialogue
					IF HAS_TIME_PASSED(iEnemyTime, iEnemyTimer)
					
						IF ( GET_NUMBER_OF_PEDS_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies, FALSE) = 1 )	//one enemy is alive
							
							ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies)
							
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
							
								CASE 0	//Chinese Goon 1 is the last enemy remaining
									iEnemySpeakerID			= CHINESEGOON1SPEAKERID
									sEnemyVoiceName			= CHINESEGOON1VOICENAME
									sEnemyConversationRoot	= "MCH1_GSGL1"
								BREAK
								
								CASE 1	//Chinese Goon 2 is the last enemy remaining
									iEnemySpeakerID			= CHINESEGOON2SPEAKERID
									sEnemyVoiceName			= CHINESEGOON2VOICENAME
									sEnemyConversationRoot	= "MCH1_GSGL2"
								BREAK
								
								CASE 2	//Chinese Goon 3 is the last enemy remaining, actual voice name 4
									iEnemySpeakerID			= CHINESEGOON4SPEAKERID
									sEnemyVoiceName			= CHINESEGOON4VOICENAME
									sEnemyConversationRoot	= "MCH1_GSGL3"
								BREAK
								
								CASE 3	//Chinese Goon 4 is the last enemy remaining
									iEnemySpeakerID			= CHINESEGOON4SPEAKERID
									sEnemyVoiceName			= CHINESEGOON4VOICENAME
									sEnemyConversationRoot	= "MCH1_GSGL4"
								BREAK
							
							ENDSWITCH
							
							FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
								REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
							ENDFOR
							
							ADD_PED_FOR_DIALOGUE(sMichael1Conversation, iEnemySpeakerID, ClosestEnemyPed, sEnemyVoiceName)
							
							IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", sEnemyConversationRoot, CONV_PRIORITY_MEDIUM)
								iEnemyTimer = GET_GAME_TIMER()
								iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
								bShootingConversationPlayed = FALSE
							ENDIF

						ELSE	//multiple enemies are alive
						
							ClosestEnemyPed = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(), rgEnemies, GET_RANDOM_INT_IN_RANGE(0, 3))
					
							IF ( ClosestEnemyPed <> NULL )
						
								SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)	//randomly play enemy shouting to themselves or enemy shouting to Michael 
								
									CASE 0	//enemies shout to themselves in Mandarin
									
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
										
											CASE 0	//Chinese Goon 1 attacks and shouts to other goons in Mandarin
												iEnemySpeakerID			= CHINESEGOON1SPEAKERID
												sEnemyVoiceName			= CHINESEGOON1VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGA1"
											BREAK
											
											CASE 1	//Chinese Goon 2 attacks and shouts to other goons in Mandarin
												iEnemySpeakerID			= CHINESEGOON2SPEAKERID
												sEnemyVoiceName			= CHINESEGOON2VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGA2"
											BREAK
											
											CASE 2	//Chinese Goon 3 attacks and shouts to other goons in Mandarin, actual vaoice name is 4
												iEnemySpeakerID			= CHINESEGOON4SPEAKERID
												sEnemyVoiceName			= CHINESEGOON4VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGA3"
											BREAK
											
											CASE 3	//Chinese Goon 4 attacks and shouts to other goons in Mandarin
												iEnemySpeakerID			= CHINESEGOON4SPEAKERID
												sEnemyVoiceName			= CHINESEGOON4VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGA4"
											BREAK

											CASE 4	//Chinese Goon 5 attacks and shouts to to other goons in Mandarin, train is blocking them
												IF ( bEnemiesBlockedByTrain = TRUE )
													iEnemySpeakerID			= CHINESEGOON5SPEAKERID
													sEnemyVoiceName			= CHINESEGOON5VOICENAME
													sEnemyConversationRoot	= "MCH1_GSGA5"
												ELSE
													iEnemySpeakerID			= CHINESEGOON4SPEAKERID
													sEnemyVoiceName			= CHINESEGOON4VOICENAME
													sEnemyConversationRoot	= "MCH1_GSGA4"
												ENDIF
											BREAK	
										
										ENDSWITCH

									BREAK
									
									CASE 1	//enemies shout to Michael
									
										SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
										
											CASE 0	//Chinese Goon 1 shouts to Michael
												iEnemySpeakerID			= CHINESEGOON1SPEAKERID
												sEnemyVoiceName			= CHINESEGOON1VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGS1"
											BREAK
											
											CASE 1	//Chinese Goon 2 shouts to Michael
												iEnemySpeakerID			= CHINESEGOON2SPEAKERID
												sEnemyVoiceName			= CHINESEGOON2VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGS2"
											BREAK
											
											CASE 2	//Chinese Goon 3 shouts to Michael, actual voice name is 4
												iEnemySpeakerID			= CHINESEGOON4SPEAKERID
												sEnemyVoiceName			= CHINESEGOON4VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGS3"
											BREAK
											
											CASE 3	//Chinese Goon 4 shouts to Michael
												iEnemySpeakerID			= CHINESEGOON4SPEAKERID
												sEnemyVoiceName			= CHINESEGOON4VOICENAME
												sEnemyConversationRoot	= "MCH1_GSGS4"
											BREAK
											
											CASE 4	//Chinese Goon 5 shouts to Michael, train is blocking them
												IF ( bEnemiesBlockedByTrain = TRUE )
													iEnemySpeakerID			= CHINESEGOON5SPEAKERID
													sEnemyVoiceName			= CHINESEGOON5VOICENAME
													sEnemyConversationRoot	= "MCH1_GSGS5"
												ELSE
													iEnemySpeakerID			= CHINESEGOON4SPEAKERID
													sEnemyVoiceName			= CHINESEGOON4VOICENAME
													sEnemyConversationRoot	= "MCH1_GSGS4"
												ENDIF
											BREAK
										
										ENDSWITCH
									
									BREAK
								
								ENDSWITCH
								
								FOR k = CHINESEGOON1SPEAKERID TO CHINESEGOON5SPEAKERID
									REMOVE_PED_FOR_DIALOGUE(sMichael1Conversation, k)
								ENDFOR
								
								ADD_PED_FOR_DIALOGUE(sMichael1Conversation, iEnemySpeakerID, ClosestEnemyPed, sEnemyVoiceName)
											
								IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", sEnemyConversationRoot, CONV_PRIORITY_MEDIUM)
									iEnemyTimer = GET_GAME_TIMER()
									iEnemyTime = 7000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
									bShootingConversationPlayed = FALSE
								ENDIF
							
							ELSE
								iEnemyTimer = GET_GAME_TIMER()
								iEnemyTime = 5000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
								bShootingConversationPlayed = FALSE
							ENDIF
							
						ENDIF
					
					ENDIF
					
					//play dialogue for spotting the second van
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSM05_1")
						IF DOES_ENTITY_EXIST(vsEnemyVans[1].VehicleIndex) AND NOT IS_ENTITY_DEAD(vsEnemyVans[1].VehicleIndex)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[1].VehicleIndex)
								IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PLAYER_PED_ID(), vsEnemyVans[1].VehicleIndex)
									IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSM05", CONV_PRIORITY_MEDIUM)
										iConversationTimer = GET_GAME_TIMER()
										iMichaelTime = 9000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
										SET_LABEL_AS_TRIGGERED("MCH1_GSM05_1", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//play dialogue for spotting the third van
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSM05_2")
						IF DOES_ENTITY_EXIST(vsEnemyVans[4].VehicleIndex) AND NOT IS_ENTITY_DEAD(vsEnemyVans[4].VehicleIndex)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsEnemyVans[4].VehicleIndex)
								IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PLAYER_PED_ID(), vsEnemyVans[4].VehicleIndex)
									IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSM05", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("MCH1_GSM05_2", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_GSM06")
							IF DOES_ENTITY_EXIST(vsEnemyVans[4].VehicleIndex) AND NOT IS_ENTITY_DEAD(vsEnemyVans[4].VehicleIndex)
								IF DOES_ENTITY_EXIST(tsTrain.VehicleIndex) AND NOT IS_ENTITY_DEAD(tsTrain.VehicleIndex)
									IF IS_VEHICLE_STOPPED(vsEnemyVans[4].VehicleIndex)
										IF 	IS_ENTITY_AT_COORD(tsTrain.VehicleIndex, <<3245.581543,-4715.378906,115.788742>>,<<128.000000,32.000000,6.000000>>)
										AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3227.010742,-4693.197754,115.645851>>,<<28.000000,22.000000,6.000000>>)
											IF CREATE_CONVERSATION(sMichael1Conversation, "MCH1AUD", "MCH1_GSM06", CONV_PRIORITY_MEDIUM)
												iConversationTimer = GET_GAME_TIMER()
												iMichaelTime = 9000 + GET_RANDOM_INT_IN_RANGE(0, 3001)
												SET_LABEL_AS_TRIGGERED("MCH1_GSM06", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF

		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_CHURCH_BELL_SOUNDS(INT &iProgress)

	IF NOT REQUEST_AMBIENT_AUDIO_BANK("CHURCH_BELL")
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Loading ambient audio bank CHURCH_BELL.")
		#ENDIF
	
	ELSE
	
		SWITCH iProgress
		
			CASE 0
			
				iProgress++
			
			BREAK
			
			CASE 1
			
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3259.471436,-4664.254395,116.152191>>,<<36.000000,30.000000,5.000000>>)
					PLAY_SOUND_FROM_COORD(iChurchBellsSoundID, "RING", << 3243.52, -4665.81, 126.0 >>,  "CHURCH_BELL_SOUNDSET")
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Playing sound RING.")
					#ENDIF
					iProgress++
				ENDIF
			
			BREAK
			
			CASE 2
			
				IF NOT HAS_SOUND_FINISHED(iChurchBellsSoundID)
					SET_VARIABLE_ON_SOUND(iChurchBellsSoundID, "CHURCH_BELL_RING_COUNT", 0.0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Sound playing, setting variable.")
					#ENDIF
				ELSE
					iProgress++
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Sound finished.")
					#ENDIF
				ENDIF
				
			BREAK
		
		ENDSWITCH
	
	ENDIF

ENDPROC

PROC RUN_VEHICLE_TYRE_BURST_CHECK(VEHICLE_INDEX VehicleIndex, SC_WHEEL_LIST eVehicleWheel, VECTOR vWheelOffset, FLOAT fRadius, BOOL bPlayerOnly,
								  BOOL bApplyDamage, VECTOR vDamageOffset, FLOAT fDamage = 100.0, FLOAT fDeformation = 100.0
								  #IF IS_DEBUG_BUILD, BOOL bDrawDebugInfo = TRUE #ENDIF)

	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
			IF NOT IS_VEHICLE_TYRE_BURST(VehicleIndex, eVehicleWheel)
				IF IS_BULLET_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vWheelOffset), fRadius, bPlayerOnly)
				
					SWITCH eVehicleWheel
					
						CASE SC_WHEEL_CAR_FRONT_LEFT
							SET_VEHICLE_TYRE_BURST(VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT)						
		           			SMASH_VEHICLE_WINDOW(VehicleIndex, SC_WINDOW_FRONT_LEFT)
						BREAK
						
						CASE SC_WHEEL_CAR_FRONT_RIGHT
							SET_VEHICLE_TYRE_BURST(VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT)
		           			SMASH_VEHICLE_WINDOW(VehicleIndex, SC_WINDOW_FRONT_RIGHT)
						BREAK
						
						CASE SC_WHEEL_CAR_REAR_LEFT
							SET_VEHICLE_TYRE_BURST(VehicleIndex, SC_WHEEL_CAR_REAR_LEFT)
		           			SMASH_VEHICLE_WINDOW(VehicleIndex, SC_WINDOW_REAR_LEFT)
						BREAK
						
						CASE SC_WHEEL_CAR_REAR_RIGHT
							SET_VEHICLE_TYRE_BURST(VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT)
		           			SMASH_VEHICLE_WINDOW(VehicleIndex, SC_WINDOW_REAR_RIGHT)
						BREAK
						
					ENDSWITCH
				
					IF ( bApplyDamage = TRUE )
		            	SET_VEHICLE_DAMAGE(VehicleIndex, vDamageOffset, fDamage, fDeformation, TRUE)
					ENDIF
				
				ENDIF
				
				#IF IS_DEBUG_BUILD
				
					IF ( bDrawDebugInfo = TRUE )
				
						DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vWheelOffset), fRadius, 0, 0, 255, 64)
						DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vDamageOffset), 0.1, 255, 0, 0)
						
//						DRAW_DEBUG_TEXT(GET_STRING_FROM_VECTOR(vDamageOffset),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vDamageOffset), 255, 0, 0)
//						
//						SWITCH eVehicleWheel
//						
//							CASE SC_WHEEL_CAR_FRONT_LEFT
//								DRAW_DEBUG_TEXT("FRONT LEFT", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vWheelOffset))
//							BREAK
//							
//							CASE SC_WHEEL_CAR_FRONT_RIGHT
//								DRAW_DEBUG_TEXT("FRONT RIGHT", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vWheelOffset))
//							BREAK
//							
//							CASE SC_WHEEL_CAR_REAR_LEFT
//								DRAW_DEBUG_TEXT("REAR LEFT", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vWheelOffset))
//							BREAK
//							
//							CASE SC_WHEEL_CAR_REAR_RIGHT
//								DRAW_DEBUG_TEXT("REAR RIGHT", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vWheelOffset))
//							BREAK
//							
//						ENDSWITCH
					ENDIF
					
				#ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(VEH_STRUCT &sVehicle, PED_INDEX PedIndex, VECTOR vLocatePosition, VECTOR vLocateSize)

	IF NOT IS_PED_INJURED(PedIndex)

		IF DOES_ENTITY_EXIST(sVehicle.VehicleIndex)
			IF IS_VEHICLE_DRIVEABLE(sVehicle.VehicleIndex)

				IF NOT IS_ENTITY_DEAD(sVehicle.VehicleIndex)
					
					IF ( sVehicle.bSmoking = FALSE )	//use this flag to mark vehicle as no longer usable by player
				
						//make the vehicle look like undriveable when it is driven into a locate
						IF IS_PED_IN_VEHICLE(PedIndex, sVehicle.VehicleIndex)
							IF IS_ENTITY_AT_COORD(sVehicle.VehicleIndex, vLocatePosition, vLocateSize)
								SET_VEHICLE_ENGINE_HEALTH(sVehicle.VehicleIndex, 50)
								SET_VEHICLE_ENGINE_ON(sVehicle.VehicleIndex, FALSE, TRUE)
								SET_VEHICLE_UNDRIVEABLE(sVehicle.VehicleIndex, TRUE)
								sVehicle.bSmoking = TRUE	//use this flag to mark vehicle as no longer usable by player
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
					
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC UPDATE_VEHICLE_DAMAGE_FROM_PED(VEHICLE_INDEX VehicleIndex, PED_INDEX PedIndex, INT iExplodeHealth, INT iSmokeHealth, BOOL &bSmokingFlag,
									VECTOR vDistanceFromVehicle, VECTOR vForce, VECTOR vForceOffset, BOOL bApplyForceDuringPlayback = FALSE,
									FLOAT fStartPlaybackPercentage = 0.0, FLOAT fEndPlaybackPercentage = 100.0)
	
	IF NOT IS_PED_INJURED(PedIndex)
							 
		IF DOES_ENTITY_EXIST(VehicleIndex)
			IF NOT IS_ENTITY_DEAD(VehicleIndex)
			
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, GET_STRING_FROM_INT(GET_ENTITY_HEALTH(VehicleIndex)), 2.15)
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, GET_STRING_FROM_FLOAT(GET_VEHICLE_ENGINE_HEALTH(VehicleIndex)), 1.95)
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(VehicleIndex, GET_STRING_FROM_FLOAT(GET_VEHICLE_PETROL_TANK_HEALTH(VehicleIndex)), 1.75)
				#ENDIF
				
				IF ( GET_ENTITY_HEALTH(VehicleIndex) = 0 )
				
					IF NOT IS_ENTITY_AT_ENTITY(PedIndex, VehicleIndex, vDistanceFromVehicle)
						EXPLODE_VEHICLE(VehicleIndex)
					ELSE
						SET_VEHICLE_ENGINE_HEALTH(VehicleIndex, -100)
						SET_VEHICLE_PETROL_TANK_HEALTH(VehicleIndex, -100)
					ENDIF

				ELIF ( GET_ENTITY_HEALTH(VehicleIndex) < iExplodeHealth )
				
					//only player can damage it below certain health to make it explode
					IF IS_PED_SHOOTING_AT_ENTITY(PedIndex, VehicleIndex)
						IF NOT IS_ENTITY_AT_ENTITY(PedIndex, VehicleIndex, vDistanceFromVehicle)
						
							IF ( bApplyForceDuringPlayback = TRUE )
						
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VehicleIndex)
							
									FLOAT fPlaybackPercentage
									
									fPlaybackPercentage = GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(VehicleIndex)
							
									IF ( fPlaybackPercentage > fStartPlaybackPercentage AND fPlaybackPercentage < fEndPlaybackPercentage )
										STOP_PLAYBACK_RECORDED_VEHICLE(VehicleIndex)
										APPLY_FORCE_TO_ENTITY(VehicleIndex, APPLY_TYPE_IMPULSE, vForce, vForceOffset, 0, TRUE, TRUE, TRUE)
									ENDIF
									
								ENDIF
								
							ENDIF
								
							EXPLODE_VEHICLE(VehicleIndex)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(VehicleIndex)
							REPLAY_RECORD_BACK_FOR_TIME(3, 0, REPLAY_IMPORTANCE_HIGH)
						ELSE
							SET_VEHICLE_ENGINE_HEALTH(VehicleIndex, -100)
							SET_VEHICLE_PETROL_TANK_HEALTH(VehicleIndex, -100)
	                    	SET_ENTITY_HEALTH(VehicleIndex, 100)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(VehicleIndex)
						ENDIF
					ENDIF
				
				ELIF ( GET_ENTITY_HEALTH(VehicleIndex) < iSmokeHealth )
				
					IF ( bSmokingFlag = FALSE )
				
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VehicleIndex)
							SET_VEHICLE_ENGINE_HEALTH(VehicleIndex, 50)
							SET_VEHICLE_UNDRIVEABLE(VehicleIndex, TRUE)
							bSmokingFlag = TRUE
						ENDIF
					ENDIF
				
                ENDIF
				
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks if a recording playback is going on for a vehicle or vehicle driver was killed when recording playback was going on for vehicle.
/// PARAMS:
///    VehicleIndex - Vehicle to check for playback or dead driver during playback.
///    fPlaybackPercentage - Outputs the playback percentage going on for the specified vehicle.
///    bDriverDead - Outputs if the driver of the specified vehicle was killed when playback was going of the vehicle.
/// RETURNS:
///    TRUE if recording playback is going on for vehicle or the driver was killed during an ongoing playback, FALSE if otherwise.
FUNC BOOL IS_VEHICLE_RECORDING_PLAYBACK_RUNNING_OR_DRIVER_DEAD(VEHICLE_INDEX VehicleIndex, FLOAT &fPlaybackPercentage, BOOL &bDriverDead)

	IF DOES_ENTITY_EXIST(VehicleIndex)
	
		IF IS_VEHICLE_DRIVEABLE(VehicleIndex)
		
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VehicleIndex)
			
				fPlaybackPercentage = GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(VehicleIndex #IF IS_DEBUG_BUILD, bPrintDebugOutput #ENDIF)
			
				PED_INDEX DriverPedIndex
				
				DriverPedIndex = GET_PED_IN_VEHICLE_SEAT(VehicleIndex, VS_DRIVER)
				
				IF DOES_ENTITY_EXIST(DriverPedIndex)
					IF IS_PED_INJURED(DriverPedIndex)
					
						bDriverDead = TRUE
					
						STOP_PLAYBACK_RECORDED_VEHICLE(VehicleIndex)
					
					ENDIF
				ENDIF
				
				RETURN TRUE
			
			ELSE
			
				IF ( bDriverDead = TRUE )
				
					RETURN TRUE
				
				ELSE
				
					RETURN FALSE
				
				ENDIF
			
			ENDIF
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if one enemy ped should 
/// PARAMS:
///    esEnemy1 - Enemy ped to replace the other enemy ped when esEnemy2 dies.
///    esEnemy2 - Enemy ped to check for death.
/// RETURNS:
///    TRUE if both enemy peds were created, one esEnemy2 has died, while esEnemy1 is alive.
FUNC BOOL SHOULD_ENEMY_REPLACE_DEAD_ENEMY(ENEMY_STRUCT esEnemy, ENEMY_STRUCT esDeadEnemy)

	//only run this if both enemies were created
	//and if the enemy is not already replacing another dead ped
	IF ( esDeadEnemy.bCreated = TRUE )
	AND ( esEnemy.bCreated = TRUE )
	AND ( esEnemy.iProgress = 0 )
	
		IF IS_PED_INJURED(esDeadEnemy.PedIndex)
		AND NOT IS_PED_INJURED(esEnemy.PedIndex)
		
			RETURN TRUE		
		
		ENDIF

	ENDIF

	RETURN FALSE

ENDFUNC

PROC MANAGE_PARKED_ENEMY_VEHICLES(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			REQUEST_MODEL(mnEnemy)
			REQUEST_MODEL(mnEnemyVan)
			
			iProgress++
		
		BREAK
		
		CASE 1	//create parked vans
		
			REQUEST_MODEL(mnEnemy)
			REQUEST_MODEL(mnEnemyVan)

			IF HAS_MODEL_LOADED(mnEnemy)
			AND HAS_MODEL_LOADED(mnEnemyVan)
			
				vsEnemyVans[2].VehicleIndex = CREATE_ENEMY_VEHICLE(vsEnemyVans[2], 1000, FALSE, TRUE, TRUE, FALSE, VEHICLELOCK_UNLOCKED, -1, -1, FALSE #IF IS_DEBUG_BUILD,"EnemyVan 2" #ENDIF)
				vsEnemyVans[3].VehicleIndex = CREATE_ENEMY_VEHICLE(vsEnemyVans[3], 800, FALSE, TRUE, TRUE, FALSE, VEHICLELOCK_UNLOCKED, -1, -1, FALSE #IF IS_DEBUG_BUILD, "EnemyVan 3" #ENDIF)
				
				iProgress++
			
			ENDIF
		
		BREAK
		

	
	ENDSWITCH
	
//	UPDATE_VEHICLE_DAMAGE_FROM_PED(vsEnemyVans[2].VehicleIndex, PLAYER_PED_ID(), 250, 475, vsEnemyVans[2].bSmoking,
//								   <<10.0, 10.0, 3.0>>, <<0.0, 0.0, 5.0>>, <<0.0, -1.5, 0.0>>, TRUE, 45.0, 70.0)
	CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(vsEnemyVans[2], PLAYER_PED_ID(), <<3222.843506,-4688.635742,114.212746>>, << 12.0, 16.0, 3.0 >>)
	
	UPDATE_VEHICLE_DAMAGE_FROM_PED(vsEnemyVans[3].VehicleIndex, PLAYER_PED_ID(), 300, 475, vsEnemyVans[3].bSmoking,
								   <<10.0, 10.0, 3.0>>, <<0.0, 0.0, 5.0>>, <<0.0, -1.5, 0.0>>, TRUE, 45.0, 70.0)
	CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(vsEnemyVans[3], PLAYER_PED_ID(), <<3222.843506,-4688.635742,114.212746>>,<< 12.0, 16.0, 3.0 >>)

	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[2].VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT, vFrontRightOffset, 0.8, FALSE, TRUE, vFrontRightDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[2].VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT, vFrontLeftOffset, 0.8, FALSE, TRUE, vFrontLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[2].VehicleIndex, SC_WHEEL_CAR_REAR_LEFT, vRearLeftOffset, 0.8, FALSE, TRUE, vRearLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[2].VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT, vRearRightOffset, 0.8, FALSE, TRUE, vRearRightDamageOffset, 150.0)
	
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[3].VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT, vFrontRightOffset, 0.8, FALSE, TRUE, vFrontRightDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[3].VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT, vFrontLeftOffset, 0.8, FALSE, TRUE, vFrontLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[3].VehicleIndex, SC_WHEEL_CAR_REAR_LEFT, vRearLeftOffset, 0.8, FALSE, TRUE, vRearLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[3].VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT, vRearRightOffset, 0.8, FALSE, TRUE, vRearRightDamageOffset, 150.0)

ENDPROC

PROC MANAGE_MISSION_TRAIN(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
	
			IF HAS_MISSION_TRAIN_BEEN_CREATED(tsTrain, 8.0, FALSE)
						
				iProgress++
			
			ENDIF
			
		BREAK
		
		CASE 1
		
			IF 	DOES_ENTITY_EXIST(tsTrain.VehicleIndex)
			AND	DOES_ENTITY_EXIST(tsTrain.PedIndex)

				IF HAS_SOUND_FINISHED(iTrainBellsSoundID)
					IF REQUEST_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")
						PLAY_SOUND_FROM_COORD(iTrainBellsSoundID, "Train_Bell", << 3217.51, -4702.84, 115.13 >>, "Prologue_Sounds")
					ENDIF
				ENDIF
			
				IF IS_ENTITY_AT_COORD(tsTrain.VehicleIndex, <<4297.463867,-4725.532715,120.312271>>, << 10.0, 10.0, 10.0 >>)

					DELETE_PED(tsTrain.PedIndex)
					DELETE_MISSION_TRAIN(tsTrain.VehicleIndex)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleted mission train.")
					#ENDIF
					
					IF NOT HAS_SOUND_FINISHED(iTrainBellsSoundID)
						STOP_SOUND(iTrainBellsSoundID)
					ENDIF
					
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")
				
					iProgress++
				
				ENDIF
			
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC MANAGE_FIRST_ENEMY_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bPreviousEnemyWaveSpawned)

	//create enemy wave when locate is reached
	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF ( bPreviousEnemyWaveSpawned = TRUE )
			
			IF HAS_MODEL_LOADED(mnEnemy)

				//create first wall enemies
				IF NOT DOES_ENTITY_EXIST(esWallEnemies[0].PedIndex)
					esWallEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3265.4900, -4590.8828, 115.9832 >>, 94.4126, rgEnemies, WEAPONTYPE_PISTOL)
				ENDIF
				esWallEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3262.19, -4593.96, 115.82 >>, 0.0, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH)
				INITIALISE_ENEMY_GROUP(esWallEnemies, FALSE #IF IS_DEBUG_BUILD, "Wall " #ENDIF)
				
				SET_PED_COMBAT_PROPERTIES(esWallEnemies[0].PedIndex, CM_DEFENSIVE, CAL_POOR, CR_FAR, CA_USE_COVER)				
				SET_PED_DEFENSIVE_AREA_ANGLED(esWallEnemies[0].PedIndex, <<3264.836914,-4590.833496,115.385437>>, <<3265.977295,-4590.834961,118.236443>>, 1.0)
				SET_PED_PREFERRED_COVERPOINT(esWallEnemies[0].PedIndex, esWallEnemies[0].ItemsetIndex, csCoverpoints[1].CoverpointIndex)
				SET_PED_COVER_PROPERTIES(esWallEnemies[0].PedIndex, TRUE, TRUE, FALSE, FALSE, 5.0, FALSE, 0.0, -1.0)
				SET_PED_SHOOTING_PROPERTIES(esWallEnemies[0].PedIndex, 3)
				
				SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esWallEnemies[0].PedIndex, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(esWallEnemies[0].PedIndex, 100.0)
				esWallEnemies[0].iTimer = GET_GAME_TIMER()


				COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esWallEnemies[1].PedIndex, << 3264.27, -4585.25, 117.07 >>, PEDMOVE_RUN, FALSE, << 3256.55, -4575.72, 118.63 >>,
																  	 PLAYER_PED_ID(), FALSE, FALSE, FALSE, 1750, FIRING_TYPE_CONTINUOUS)
				SET_PED_COMBAT_PROPERTIES(esWallEnemies[1].PedIndex, CM_WILLADVANCE, CAL_POOR, CR_NEAR, CA_AGGRESSIVE)
				SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
				SET_PED_SHOOTING_PROPERTIES(esWallEnemies[1].PedIndex, 3, 100, FIRING_PATTERN_BURST_FIRE)
				esWallEnemies[1].iTimer = GET_GAME_TIMER()
				bCurrentEnemyWaveSpawned = TRUE

			ENDIF
			
		ENDIF
		
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		//update the first goon, who was in the cutscene
		IF NOT IS_PED_INJURED(esWallEnemies[0].PedIndex)
			SWITCH esWallEnemies[0].iProgress
			
				CASE 0	
				
					IF HAS_TIME_PASSED(10000, esWallEnemies[0].iTimer)
					OR IS_PED_INJURED(esWallEnemies[1].PedIndex)

						esWallEnemies[0].bHasTask = FALSE
						esWallEnemies[0].iProgress++
						
					ENDIF
					
				BREAK
				
				CASE 1	//make the enemy peek more from cover
				
					IF ( esWallEnemies[0].bHasTask = FALSE )
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_COVER_PROPERTIES(esWallEnemies[0].PedIndex, TRUE, TRUE, FALSE, FALSE, 10.0, FALSE, 0.0, 1.0, 5.0)
						esWallEnemies[0].iTimer = GET_GAME_TIMER()
						esWallEnemies[0].bHasTask = TRUE
					ENDIF
					
					IF HAS_TIME_PASSED(18000, esWallEnemies[0].iTimer)
						esWallEnemies[0].bHasTask = FALSE
						esWallEnemies[0].iProgress++
					ENDIF
				
				BREAK
				
				CASE 2
				
					IF ( esWallEnemies[0].bHasTask = FALSE )
						REMOVE_PED_DEFENSIVE_AREA(esWallEnemies[0].PedIndex)
						SET_PED_COMBAT_PROPERTIES(esWallEnemies[0].PedIndex, CM_WILLADVANCE, CAL_POOR, CR_NEAR, CA_AGGRESSIVE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COVER_PROPERTIES(esWallEnemies[0].PedIndex, TRUE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esWallEnemies[0].PedIndex, 100.0)
						esWallEnemies[0].iTimer = GET_GAME_TIMER()
						esWallEnemies[0].bHasTask = TRUE
					ENDIF
					
					IF HAS_TIME_PASSED(10000, esWallEnemies[0].iTimer)
						esWallEnemies[0].bHasTask = FALSE
						esWallEnemies[0].iProgress++
					ENDIF
				
				BREAK
				
				CASE 3	//make the enemy more aggressive
				
					IF ( esWallEnemies[0].bHasTask = FALSE )
						REMOVE_PED_DEFENSIVE_AREA(esWallEnemies[0].PedIndex)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_FLANK, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_AGGRESSIVE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_COMBAT_RANGE(esWallEnemies[0].PedIndex, CR_NEAR)
						SET_PED_COMBAT_MOVEMENT(esWallEnemies[0].PedIndex, CM_WILLADVANCE)
						SET_PED_SHOOTING_PROPERTIES(esWallEnemies[0].PedIndex, 3, 125, FIRING_PATTERN_BURST_FIRE)
						esWallEnemies[0].bHasTask = TRUE
					ENDIF
					
				BREAK
			
			ENDSWITCH
		ENDIF
		
		//update the second goon, the one who charges at the player initially
		IF NOT IS_PED_INJURED(esWallEnemies[1].PedIndex)
			SWITCH esWallEnemies[1].iProgress
			
				CASE 0	
				
					IF HAS_TIME_PASSED(6000, esWallEnemies[1].iTimer)
					OR IS_PED_IN_COMBAT(esWallEnemies[1].PedIndex)

						esWallEnemies[1].bHasTask = FALSE
						esWallEnemies[1].iProgress++
						
					ENDIF
					
				BREAK
				
				CASE 1	//make the enemy move to the left
				
					IF ( esWallEnemies[1].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esWallEnemies[1].PedIndex, 100.0)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
						SET_PED_COMBAT_PROPERTIES(esWallEnemies[1].PedIndex, CM_WILLADVANCE, CAL_AVERAGE, CR_NEAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_COMBAT_MOVEMENT_VALUES(esWallEnemies[1].PedIndex, 1.0, 1.0)
						SET_PED_DEFENSIVE_AREA_SPHERE(esWallEnemies[1].PedIndex, << 3267.71, -4577.71, 117.21 >>, 1.25)
						SET_PED_COVER_PROPERTIES(esWallEnemies[1].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, FALSE, 0.0, 2.0)
						SET_PED_SHOOTING_PROPERTIES(esWallEnemies[1].PedIndex, 3, 125, FIRING_PATTERN_BURST_FIRE)
						esWallEnemies[1].iTimer = GET_GAME_TIMER()
						esWallEnemies[1].bHasTask = TRUE
					ENDIF
					
					IF HAS_TIME_PASSED(15000, esWallEnemies[1].iTimer)
					
						esWallEnemies[1].bHasTask = FALSE
						esWallEnemies[1].iProgress++
						
					ENDIF
				
				BREAK
				
				CASE 2	//make the enemy more aggressive
				
					IF ( esWallEnemies[1].bHasTask = FALSE )
						REMOVE_PED_DEFENSIVE_AREA(esWallEnemies[1].PedIndex)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_CAN_FLANK, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_AGGRESSIVE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esWallEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_COMBAT_RANGE(esWallEnemies[1].PedIndex, CR_NEAR)
						SET_PED_COMBAT_MOVEMENT(esWallEnemies[1].PedIndex, CM_WILLADVANCE)
						SET_PED_SHOOTING_PROPERTIES(esWallEnemies[1].PedIndex, 3, 150, FIRING_PATTERN_BURST_FIRE)
						esWallEnemies[1].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		ENDIF

		CLEANUP_ENEMY_GROUP(esWallEnemies)
	
		IF ( GET_NUMBER_OF_ENEMIES_DEAD(esWallEnemies) = 2 )
			IF ( iFirstVanTriggerTimer = 0 )
				iFirstVanTriggerTimer = GET_GAME_TIMER()
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Staring van trigger timer.")
				#ENDIF
			ENDIF
		ENDIF
	
	ENDIF

	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esWallEnemies)
	
ENDPROC

PROC MANAGE_SECOND_ENEMY_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bPreviousEnemyWaveSpawned, INT iAliveEnemiesCountToTriggerWave)

	//create enemy wave when locate is reached
	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF ( bPreviousEnemyWaveSpawned = TRUE )
			
			IF IS_ENTITY_IN_LOCATE(PLAYER_PED_ID(), << 3267.560303, -4588.922363, 118.005730 >>, << 30.0, 3.5, 2.0 >> #IF IS_DEBUG_BUILD, "SecondWaveTriggerLocate" #ENDIF)
			OR ( iFirstVanTriggerTimer != 0 AND HAS_TIME_PASSED(26000, iFirstVanTriggerTimer) )
			OR ( GET_NUMBER_OF_ENEMIES_ALIVE(esWallEnemies) <= iAliveEnemiesCountToTriggerWave )

				//create middle enemies
				esMiddleEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3270.1250, -4642.8452, 113.1802 >>, 5.9782, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_INVALID, TRUE)
				esMiddleEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3275.9019, -4645.0601, 113.2832 >>, 359.8844, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, TRUE)
				esMiddleEnemies[2].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3268.3623, -4654.7510, 113.0602 >>, 359.9476, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_INVALID, TRUE)
				INITIALISE_ENEMY_GROUP(esMiddleEnemies, FALSE #IF IS_DEBUG_BUILD, "Middle " #ENDIF)

				SET_PED_SEEING_RANGE(esMiddleEnemies[0].PedIndex, 20.0)
				SET_PED_SEEING_RANGE(esMiddleEnemies[1].PedIndex, 15.0)
				SET_PED_SEEING_RANGE(esMiddleEnemies[2].PedIndex, 20.0)
				
				GO_TO_ENEMY_STATE(esMiddleEnemies[0], ENEMY_STATE_SCRIPTED_ENTRY)
				GO_TO_ENEMY_STATE(esMiddleEnemies[1], ENEMY_STATE_SCRIPTED_ENTRY)
				GO_TO_ENEMY_STATE(esMiddleEnemies[2], ENEMY_STATE_SCRIPTED_ENTRY)
				
				bCurrentEnemyWaveSpawned = TRUE
					
			ENDIF
		ENDIF
		
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		//block enemy weapon fire until the 'player detected' conversation plays out
		IF ( bPlayerDetected = FALSE )
			BLOCK_WEAPON_FIRE_FOR_ENEMY_GROUP(esMiddleEnemies)
		ELIF ( bPlayerDetected = TRUE )
			IF ( bDetectedConversationPlaying = TRUE )
				BLOCK_WEAPON_FIRE_FOR_ENEMY_GROUP(esMiddleEnemies)
			ENDIF
		ENDIF
	
		IF NOT IS_PED_INJURED(esMiddleEnemies[0].PedIndex)
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(esMiddleEnemies[0].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esMiddleEnemies[0].eState), 1.25)
			#ENDIF
		
			SWITCH esMiddleEnemies[0].eState
			
				CASE ENEMY_STATE_SCRIPTED_ENTRY
			
					SWITCH esMiddleEnemies[0].iProgress
			
						CASE 0
						
							esMiddleEnemies[0].iTimer = GET_GAME_TIMER()
							esMiddleEnemies[0].iProgress++
						BREAK
						
						CASE 1
						
							IF ( esMiddleEnemies[0].bHasTask = FALSE )
								IF HAS_TIME_PASSED(5000, esMiddleEnemies[0].iTimer)
					
									COMBAT_SEQUENCE_GO_TO_COVER_WHILE_AIMING_THEN_COMBAT(esMiddleEnemies[0].PedIndex, << 3276.90, -4617.48, 115.02 >>, PEDMOVE_SLOWRUN, FALSE, << 3273.65, -4590.99, 117.19 >>, NULL, FALSE, -1)
									SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
									SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[0].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_DEFENSIVE_AREA_SPHERE(esMiddleEnemies[0].PedIndex, << 3276.90, -4617.48, 115.02 >>, 2.75)
									SET_PED_COVER_PROPERTIES(esMiddleEnemies[0].PedIndex, TRUE, TRUE, FALSE, FALSE, 6.0, FALSE, 0.0, 3.0)
									SET_PED_SHOOTING_PROPERTIES(esMiddleEnemies[0].PedIndex, 4)
									
									esMiddleEnemies[0].bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( esMiddleEnemies[0].bFallingBack = FALSE )
								IF ( bPlayerUsedAlternativePath = TRUE )
									esMiddleEnemies[0].bHasTask 	= FALSE
									esMiddleEnemies[0].iProgress 	= 100
									esMiddleEnemies[0].bFallingBack = TRUE
								ENDIF
							ENDIF
						
						BREAK
			
						CASE 100
						
							IF ( esMiddleEnemies[0].bHasTask = FALSE )
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esMiddleEnemies[0].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
								SET_PED_DEFENSIVE_AREA_ANGLED(esMiddleEnemies[0].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
								SET_PED_COMBAT_MOVEMENT_VALUES(esMiddleEnemies[0].PedIndex, 0.8, 0.25)
								SET_PED_COVER_PROPERTIES(esMiddleEnemies[0].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
								
								esMiddleEnemies[0].bHasTask = TRUE
							ENDIF
						
						BREAK
					
					ENDSWITCH
					
					IF IS_PED_IN_COMBAT(esMiddleEnemies[0].PedIndex)
					OR ( bPlayerDetected = TRUE )
						GO_TO_ENEMY_STATE(esMiddleEnemies[0], ENEMY_STATE_COMBAT)
					ENDIF

				BREAK
				
				CASE ENEMY_STATE_COMBAT
				
					IF ( esMiddleEnemies[0].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esMiddleEnemies[0].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[0].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_DEFENSIVE_AREA_SPHERE(esMiddleEnemies[0].PedIndex, << 3276.90, -4617.48, 115.02 >>, 2.75)
						SET_PED_COVER_PROPERTIES(esMiddleEnemies[0].PedIndex, TRUE, TRUE, FALSE, FALSE, 6.0, FALSE, 0.0, 3.0)
						SET_PED_SHOOTING_PROPERTIES(esMiddleEnemies[0].PedIndex, 4)
						SET_PED_VISUAL_FIELD_PROPERTIES(esMiddleEnemies[0].PedIndex)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esMiddleEnemies[0].PedIndex, FALSE)
						bPlayerDetected = TRUE
						esMiddleEnemies[0].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH

		ENDIF
		
		IF NOT IS_PED_INJURED(esMiddleEnemies[1].PedIndex)

			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(esMiddleEnemies[1].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esMiddleEnemies[1].eState), 1.25)
			#ENDIF

			SWITCH esMiddleEnemies[1].eState
				
				CASE ENEMY_STATE_SCRIPTED_ENTRY
				
					SWITCH esMiddleEnemies[1].iProgress
					
						CASE 0
							esMiddleEnemies[1].iTimer = GET_GAME_TIMER()
							esMiddleEnemies[1].iProgress++
						BREAK
						
						CASE 1
						
							IF ( esMiddleEnemies[1].bHasTask = FALSE )
						
								IF HAS_TIME_PASSED(5000, esMiddleEnemies[1].iTimer)
									COMBAT_SEQUENCE_GO_TO_COVER_WHILE_AIMING_THEN_COMBAT(esMiddleEnemies[1].PedIndex, << 3269.19, -4627.51, 114.85 >>, PEDMOVE_SLOWRUN, FALSE, << 3273.65, -4590.99, 117.19 >>, NULL, FALSE, -1)
									SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
									SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[1].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_DEFENSIVE_AREA_SPHERE(esMiddleEnemies[1].PedIndex,<< 3269.19, -4627.51, 114.85 >>, 3.0)
									SET_PED_COVER_PROPERTIES(esMiddleEnemies[1].PedIndex, TRUE, FALSE, TRUE, FALSE, 7.0, FALSE, 0.0, 3.0)
									SET_PED_SHOOTING_PROPERTIES(esMiddleEnemies[1].PedIndex, 4)
									esMiddleEnemies[1].bHasTask = TRUE
								ENDIF
							ENDIF
							
							
							IF ( bStartSearching = TRUE )
								GO_TO_ENEMY_STATE(esMiddleEnemies[1], ENEMY_STATE_FOLLOW_WAYPOINT_RECORDING)
							ENDIF
							
							IF ( esMiddleEnemies[1].bFallingBack = FALSE )
								IF ( bPlayerUsedAlternativePath = TRUE )
									esMiddleEnemies[1].bHasTask 	= FALSE
									esMiddleEnemies[1].iProgress 	= 100
									esMiddleEnemies[1].bFallingBack = TRUE
								ENDIF
							ENDIF
						
						BREAK
			
						CASE 100
						
							IF ( esMiddleEnemies[1].bHasTask = FALSE )
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esMiddleEnemies[1].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
								SET_PED_DEFENSIVE_AREA_ANGLED(esMiddleEnemies[1].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
								SET_PED_COMBAT_MOVEMENT_VALUES(esMiddleEnemies[1].PedIndex, 0.9, 0.1)
								SET_PED_COVER_PROPERTIES(esMiddleEnemies[1].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
								esMiddleEnemies[1].bHasTask = TRUE
							ENDIF
						
						BREAK
					
					ENDSWITCH
					
					IF IS_PED_IN_COMBAT(esMiddleEnemies[1].PedIndex)
					OR ( bPlayerDetected = TRUE )
						GO_TO_ENEMY_STATE(esMiddleEnemies[1], ENEMY_STATE_COMBAT)
					ENDIF
				
				BREAK
				
				CASE ENEMY_STATE_FOLLOW_WAYPOINT_RECORDING
					
					SET_PED_RESET_FLAG(esMiddleEnemies[1].PedIndex, PRF_UseHeadOrientationForPerception, TRUE)
					
					IF ( esMiddleEnemies[1].bHasTask = FALSE )
					
						REQUEST_WAYPOINT_RECORDING("mic1_c")
						REQUEST_ANIM_DICT("missmic1ig_3_patrol")
						
						
						IF 	GET_IS_WAYPOINT_RECORDING_LOADED("mic1_c")
						AND HAS_ANIM_DICT_LOADED("missmic1ig_3_patrol")
						
							INT		iClostestWaypoint
						
							IF 	WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("mic1_c", GET_ENTITY_COORDS(esMiddleEnemies[1].PedIndex), iClostestWaypoint)
							AND ( iClostestWaypoint > 30 )	//this makes enemy walk in a loop forever starting from waypoint 33 in the route
								TASK_FOLLOW_WAYPOINT_RECORDING(esMiddleEnemies[1].PedIndex, "mic1_c", 32, EWAYPOINT_TURN_TO_FACE_WAYPOINT_HEADING_AT_END)	
							ELSE							//this makes enemy walk to the start waypoint in the route
								TASK_FOLLOW_WAYPOINT_RECORDING(esMiddleEnemies[1].PedIndex, "mic1_c", 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_TURN_TO_FACE_WAYPOINT_HEADING_AT_END)
							ENDIF
							
							esMiddleEnemies[1].iWaypointProgress = 0
							esMiddleEnemies[1].bSearching = TRUE
							esMiddleEnemies[1].bHasTask = TRUE
							
						ENDIF

					ENDIF
					
					IF 	IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(esMiddleEnemies[1].PedIndex)
					AND NOT IS_ENTITY_PLAYING_ANIM(esMiddleEnemies[1].PedIndex, "missmic1ig_3_patrol", "search_walk_fwd_b")
					
						IF ( esMiddleEnemies[1].iWaypointProgress != 0 )

							IF ( esMiddleEnemies[1].iWaypointProgress != GET_PED_WAYPOINT_PROGRESS(esMiddleEnemies[1].PedIndex) )
					
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Progress: ", GET_PED_WAYPOINT_PROGRESS(esMiddleEnemies[1].PedIndex))
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": enemy.iWaypointProgress: ", esMiddleEnemies[1].iWaypointProgress)
								#ENDIF
							
								INT 	iTotalPoints
								VECTOR 	vNodeVector
								
								IF WAYPOINT_RECORDING_GET_NUM_POINTS("mic1_c", iTotalPoints)
								
									IF ( esMiddleEnemies[1].iWaypointProgress < iTotalPoints )
									
										IF WAYPOINT_RECORDING_GET_COORD("mic1_c", esMiddleEnemies[1].iWaypointProgress + 3, vNodeVector)

											TASK_PLAY_ANIM(esMiddleEnemies[1].PedIndex, "missmic1ig_3_patrol", "search_walk_fwd_b",
														   WALK_BLEND_IN, WALK_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING |
														   AF_TAG_SYNC_IN | AF_TAG_SYNC_OUT | AF_TAG_SYNC_CONTINUOUS | AF_UPPERBODY)
											
											esMiddleEnemies[1].iWaypointProgress = GET_PED_WAYPOINT_PROGRESS(esMiddleEnemies[1].PedIndex)
										
										ENDIF
									
									ENDIF
								
								ENDIF
								
							ENDIF
						
						ELSE
						
							esMiddleEnemies[1].iWaypointProgress = GET_PED_WAYPOINT_PROGRESS(esMiddleEnemies[1].PedIndex)
							
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Progress: ", GET_PED_WAYPOINT_PROGRESS(esMiddleEnemies[1].PedIndex))
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": enemy.iWaypointProgress: ", esMiddleEnemies[1].iWaypointProgress)
							#ENDIF
						
						ENDIF
					
					ELSE
					
						IF GET_SCRIPT_TASK_STATUS(esMiddleEnemies[1].PedIndex, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = FINISHED_TASK
							GO_TO_ENEMY_STATE(esMiddleEnemies[1], ENEMY_STATE_FOLLOW_WAYPOINT_RECORDING)
						ENDIF
						
					ENDIF
					
					IF IS_PED_IN_COMBAT(esMiddleEnemies[1].PedIndex)
					OR ( bPlayerDetected = TRUE )
						CLEAR_PED_SECONDARY_TASK(esMiddleEnemies[1].PedIndex)
						GO_TO_ENEMY_STATE(esMiddleEnemies[1], ENEMY_STATE_COMBAT)
					ENDIF
					
				BREAK
				
				CASE ENEMY_STATE_COMBAT
				
					IF ( esMiddleEnemies[1].bHasTask = FALSE )
						CLEAR_PED_SECONDARY_TASK(esMiddleEnemies[1].PedIndex)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esMiddleEnemies[1].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[1].PedIndex, CM_WILLADVANCE, CAL_AVERAGE, CR_NEAR, CA_USE_COVER)
						SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						REMOVE_PED_DEFENSIVE_AREA(esMiddleEnemies[1].PedIndex)
						SET_PED_SHOOTING_PROPERTIES(esMiddleEnemies[1].PedIndex, 4, 100, FIRING_PATTERN_BURST_FIRE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esMiddleEnemies[1].PedIndex, FALSE)
						SET_PED_VISUAL_FIELD_PROPERTIES(esMiddleEnemies[1].PedIndex)
						bPlayerDetected = TRUE
						esMiddleEnemies[1].bSearching = FALSE
						esMiddleEnemies[1].bHasTask = TRUE
					ENDIF
				
				BREAK
				
			ENDSWITCH

		ENDIF
		
		IF NOT IS_PED_INJURED(esMiddleEnemies[2].PedIndex)
		
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(esMiddleEnemies[2].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esMiddleEnemies[2].eState), 1.25)
			#ENDIF
		
			SWITCH esMiddleEnemies[2].eState
				
				CASE ENEMY_STATE_SCRIPTED_ENTRY
				
					SWITCH esMiddleEnemies[2].iProgress
			
						CASE 0
							esMiddleEnemies[2].iTimer = GET_GAME_TIMER()
							esMiddleEnemies[2].iProgress++
						BREAK
						
						CASE 1
						
							IF ( esMiddleEnemies[2].bHasTask = FALSE )
								IF HAS_TIME_PASSED(5000, esMiddleEnemies[2].iTimer)
									COMBAT_SEQUENCE_GO_TO_COVER_WHILE_AIMING_THEN_COMBAT(esMiddleEnemies[2].PedIndex, << 3279.21, -4628.83, 114.92 >>, PEDMOVE_SLOWRUN, FALSE, << 3273.65, -4590.99, 117.19 >>, NULL, FALSE, -1)
									SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[2].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
									SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[2].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_DEFENSIVE_AREA_SPHERE(esMiddleEnemies[2].PedIndex, << 3279.21, -4628.83, 114.92 >>, 3.0)
									SET_PED_COVER_PROPERTIES(esMiddleEnemies[2].PedIndex, TRUE, FALSE, TRUE, FALSE, 5.0, FALSE, 0.0, 2.5)
									SET_PED_SHOOTING_PROPERTIES(esMiddleEnemies[2].PedIndex, 4)
									esMiddleEnemies[2].bHasTask = TRUE
								ENDIF
							ENDIF
							
							IF ( esMiddleEnemies[2].bFallingBack = FALSE )
								IF ( bPlayerUsedAlternativePath = TRUE )
									esMiddleEnemies[2].bHasTask 	= FALSE
									esMiddleEnemies[2].iProgress 	= 100
									esMiddleEnemies[2].bFallingBack = TRUE
								ENDIF
							ENDIF
						
						BREAK
			
						CASE 100
						
							IF ( esMiddleEnemies[2].bHasTask = FALSE )
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esMiddleEnemies[2].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[2].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
								SET_PED_DEFENSIVE_AREA_ANGLED(esMiddleEnemies[2].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
								SET_PED_COMBAT_MOVEMENT_VALUES(esMiddleEnemies[2].PedIndex, 0.9, 0.1)
								SET_PED_COVER_PROPERTIES(esMiddleEnemies[2].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
								esMiddleEnemies[2].bHasTask = TRUE
							ENDIF
						
						BREAK
					
					ENDSWITCH
					
					IF IS_PED_IN_COMBAT(esMiddleEnemies[2].PedIndex)
					OR ( bPlayerDetected = TRUE )
						GO_TO_ENEMY_STATE(esMiddleEnemies[2], ENEMY_STATE_COMBAT)
					ENDIF
				
				BREAK
				
				CASE ENEMY_STATE_COMBAT
				
					IF ( esMiddleEnemies[2].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esMiddleEnemies[2].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esMiddleEnemies[2].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[2].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esMiddleEnemies[2].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_DEFENSIVE_AREA_SPHERE(esMiddleEnemies[2].PedIndex, << 3279.21, -4628.83, 114.92 >>, 3.0)
						SET_PED_COVER_PROPERTIES(esMiddleEnemies[2].PedIndex, TRUE, FALSE, TRUE, FALSE, 5.0, FALSE, 0.0, 2.5)
						SET_PED_SHOOTING_PROPERTIES(esMiddleEnemies[2].PedIndex, 4)
						SET_PED_VISUAL_FIELD_PROPERTIES(esMiddleEnemies[2].PedIndex)
						esMiddleEnemies[2].bHasTask = TRUE
					ENDIF
				
				BREAK
				
			ENDSWITCH
			
		ENDIF
	
		CLEANUP_ENEMY_GROUP(esMiddleEnemies)
	
	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esMiddleEnemies)
	
ENDPROC

PROC MANAGE_THIRD_ENEMY_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bPreviousEnemyWaveSpawned)

	//create enemy wave when locate is reached
	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF ( bPreviousEnemyWaveSpawned = TRUE )
			
			IF IS_ENTITY_IN_LOCATE(PLAYER_PED_ID(), << 3264.409424, -4611.560547, 117.022507 >>, << 30.0, 3.0, 2.0 >> #IF IS_DEBUG_BUILD, "ThirdWaveLocate" #ENDIF)
			//OR ( GET_NUMBER_OF_ENEMIES_DEAD(esMiddleEnemies) + GET_NUMBER_OF_ENEMIES_DEAD(esVanEnemies) >= 6 )

				esRunners[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3266.25, -4642.72, 113.23 >>, 324.2408, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esRunners[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3274.37, -4640.03, 113.66 >>, 0.0, rgEnemies, WEAPONTYPE_PISTOL)
				INITIALISE_ENEMY_GROUP(esRunners, FALSE #IF IS_DEBUG_BUILD, "Runners " #ENDIF)
				
				COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esRunners[0].PedIndex, << 3271.2998, -4631.0703, 114.7037 >>, PEDMOVE_RUN, FALSE, vZero, PLAYER_PED_ID(), TRUE)
				SET_PED_COMBAT_PROPERTIES(esRunners[0].PedIndex, CM_WILLADVANCE, CAL_POOR, CR_NEAR, CA_AGGRESSIVE)
				SET_PED_COMBAT_ATTRIBUTES(esRunners[0].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esRunners[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esRunners[0].PedIndex, CA_CAN_CHARGE, TRUE)
				SET_PED_SHOOTING_PROPERTIES(esRunners[0].PedIndex, 4, 50, FIRING_PATTERN_FULL_AUTO)
				
				COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esRunners[1].PedIndex, << 3273.3306, -4623.3579, 114.7698 >>, PEDMOVE_RUN, FALSE, vZero, PLAYER_PED_ID(), TRUE)
				SET_PED_COMBAT_PROPERTIES(esRunners[1].PedIndex, CM_WILLADVANCE, CAL_POOR, CR_NEAR, CA_AGGRESSIVE)
				SET_PED_COMBAT_ATTRIBUTES(esRunners[1].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esRunners[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esRunners[1].PedIndex, CA_CAN_CHARGE, TRUE)
				SET_PED_SHOOTING_PROPERTIES(esRunners[1].PedIndex, 4)
				
				esChurchEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3285.61, -4651.54, 112.84 >>, 0.0, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esChurchEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3279.87, -4671.24, 112.83 >>, 0.0, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_INVALID, TRUE)
				INITIALISE_ENEMY_GROUP(esChurchEnemies, FALSE #IF IS_DEBUG_BUILD, "Church " #ENDIF)
				
				COMBAT_SEQUENCE_GO_TO_COMBAT(esChurchEnemies[0].PedIndex)
				SET_PED_COMBAT_PROPERTIES(esChurchEnemies[0].PedIndex, CM_DEFENSIVE, CAL_POOR, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
				SET_PED_COMBAT_ATTRIBUTES(esChurchEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esChurchEnemies[0].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_DEFENSIVE_AREA_ANGLED(esChurchEnemies[0].PedIndex, <<3282.731934,-4650.353516,112.839226>>, <<3278.938232,-4650.336426,115.636375>>, 2.0)
				SET_PED_COVER_PROPERTIES(esChurchEnemies[0].PedIndex, TRUE, TRUE, FALSE, TRUE, 6.0, FALSE, 0.0, 3.0)
				//SET_PED_PREFERRED_COVERPOINT(esChurchEnemies[0].PedIndex, esChurchEnemies[0].ItemsetIndex, csCoverpoints[20].CoverpointIndex)
				SET_PED_SHOOTING_PROPERTIES(esChurchEnemies[0].PedIndex, 3)
				
				COMBAT_SEQUENCE_GO_TO_COORD_THEN_COMBAT(esChurchEnemies[1].PedIndex, << 3277.91, -4670.24, 112.87 >>, PEDMOVE_WALK, -1)
				SET_PED_COMBAT_PROPERTIES(esChurchEnemies[1].PedIndex, CM_DEFENSIVE, CAL_POOR, CR_FAR, CA_USE_COVER)
				SET_PED_COMBAT_ATTRIBUTES(esChurchEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esChurchEnemies[1].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_DEFENSIVE_AREA_SPHERE(esChurchEnemies[1].PedIndex, <<3277.91, -4670.24, 112.87>>, 2.0)
				SET_PED_SHOOTING_PROPERTIES(esChurchEnemies[1].PedIndex, 4)

				bCurrentEnemyWaveSpawned = TRUE
				
			ENDIF
		ENDIF
		
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		//update the first runner ped charging at the player
		IF NOT IS_PED_INJURED(esRunners[0].PedIndex)
		
			IF ( esRunners[0].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esRunners[0].bHasTask 		= FALSE
					esRunners[0].iProgress 		= 100
					esRunners[0].bFallingBack 	= TRUE
				ENDIF
			ENDIF
	
			SWITCH esRunners[0].iProgress
			
				CASE 0	
				
					IF IS_PED_IN_COMBAT(esRunners[0].PedIndex)	//if the enemy ped is done with the combat sequence and is now in combat

						esRunners[0].bHasTask = FALSE
						esRunners[0].iProgress++
						
					ENDIF
					
				BREAK
				
				CASE 1		//make the runner enemy go to ai combat and try to seek cover
				
					IF ( esRunners[0].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esRunners[0].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esRunners[0].PedIndex, CM_WILLRETREAT, CAL_POOR, CR_MEDIUM, CA_USE_COVER)
						SET_PED_SHOOTING_PROPERTIES(esRunners[0].PedIndex, 4)
						esRunners[0].bHasTask = TRUE
					ENDIF
				
				BREAK
				
				CASE 100	//make the enemy fall back to the parking lot
				
					IF ( esRunners[0].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esRunners[0].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esRunners[0].PedIndex, CM_DEFENSIVE, CAL_POOR, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esRunners[0].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esRunners[0].PedIndex, 0.5, 0.0)
						esRunners[0].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		ENDIF
		
		//update the second runner ped charging at the player
		IF NOT IS_PED_INJURED(esRunners[1].PedIndex)
		
			IF ( esRunners[1].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esRunners[1].bHasTask 		= FALSE
					esRunners[1].iProgress 		= 100
					esRunners[1].bFallingBack 	= TRUE
				ENDIF
			ENDIF
		
			SWITCH esRunners[1].iProgress
			
				CASE 0	
				
					IF IS_PED_IN_COMBAT(esRunners[1].PedIndex)

						esRunners[1].bHasTask = FALSE
						esRunners[1].iProgress++
						
					ENDIF
					
				BREAK
				
				CASE 1	//make the runner enemy go to ai combat and try to seek cover
				
					IF ( esRunners[1].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esRunners[1].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esRunners[1].PedIndex, CM_WILLRETREAT, CAL_POOR, CR_MEDIUM, CA_USE_COVER)
						SET_PED_SHOOTING_PROPERTIES(esRunners[1].PedIndex, 4)
						esRunners[1].bHasTask = TRUE
					ENDIF
				
				BREAK
				
				CASE 100	//make the enemy fall back to the parking lot
				
					IF ( esRunners[1].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esRunners[1].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esRunners[1].PedIndex, CM_DEFENSIVE, CAL_POOR, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esRunners[1].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esRunners[1].PedIndex, 0.5, 0.0)
						esRunners[1].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		ENDIF
		
		IF NOT IS_PED_INJURED(esChurchEnemies[0].PedIndex)
		
			IF ( esChurchEnemies[0].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esChurchEnemies[0].bHasTask 	= FALSE
					esChurchEnemies[0].iProgress 	= 100
					esChurchEnemies[0].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esChurchEnemies[0].iProgress
			
				CASE 100
				
					IF ( esChurchEnemies[0].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esChurchEnemies[0].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esChurchEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esChurchEnemies[0].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esChurchEnemies[0].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esChurchEnemies[0].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						//SET_PED_PREFERRED_COVERPOINT(esChurchEnemies[0].PedIndex, esChurchEnemies[0].ItemsetIndex, csCoverpoints[26].CoverpointIndex)
						esChurchEnemies[0].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
			
		ENDIF
		
		IF NOT IS_PED_INJURED(esChurchEnemies[1].PedIndex)
		
			IF ( esChurchEnemies[1].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esChurchEnemies[1].bHasTask 	= FALSE
					esChurchEnemies[1].iProgress 	= 100
					esChurchEnemies[1].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esChurchEnemies[1].iProgress
			
				CASE 100
				
					IF ( esChurchEnemies[1].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esChurchEnemies[1].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esChurchEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esChurchEnemies[1].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esChurchEnemies[1].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esChurchEnemies[1].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						esChurchEnemies[1].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
			
		ENDIF
	
		CLEANUP_ENEMY_GROUP(esRunners)
		CLEANUP_ENEMY_GROUP(esChurchEnemies)
		
	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esRunners)
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esChurchEnemies)
	
ENDPROC

PROC MANAGE_FOURTH_ENEMY_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bPreviousEnemyWaveSpawned, INT iAliveEnemiesCountToTriggerWave)

	//create enemy wave when locate is reached
	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF ( bPreviousEnemyWaveSpawned = TRUE )
			
			IF IS_ENTITY_IN_LOCATE(PLAYER_PED_ID(), <<3262.183105,-4629.664551,116.845070>>, << 30.0, 2.0, 3.0 >> #IF IS_DEBUG_BUILD, "FourthWaveLocate" #ENDIF)
			OR ( ( GET_NUMBER_OF_ENEMIES_ALIVE(esRunners) + GET_NUMBER_OF_ENEMIES_ALIVE(esChurchEnemies) ) <= iAliveEnemiesCountToTriggerWave )

				esExitEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3253.03, -4672.16, 113.57 >>, 0.0, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esExitEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3253.27, -4686.10, 111.96 >>, 341.08, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esExitEnemies[2].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3257.50, -4695.39, 111.85 >>, 345.48, rgEnemies, WEAPONTYPE_PUMPSHOTGUN)
				esExitEnemies[3].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3251.4146, -4686.4194, 111.8743 >>, 350.8474, rgEnemies, WEAPONTYPE_PUMPSHOTGUN)
				esExitEnemies[4].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3261.0928, -4693.9453, 111.8941 >>, 0.7368, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_INVALID, TRUE)
				INITIALISE_ENEMY_GROUP(esExitEnemies, FALSE #IF IS_DEBUG_BUILD, "Exit " #ENDIF)
				
				COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esExitEnemies[0].PedIndex, << 3272.56, -4664.50, 112.84 >>, PEDMOVE_RUN, FALSE, vZero, PLAYER_PED_ID(), TRUE)
				SET_PED_SHOOTING_PROPERTIES(esExitEnemies[0].PedIndex, 3)
				SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				
				COMBAT_SEQUENCE_GO_TO_COMBAT(esExitEnemies[1].PedIndex)
				SET_PED_COMBAT_PROPERTIES(esExitEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
				SET_PED_DEFENSIVE_AREA_SPHERE(esExitEnemies[1].PedIndex, << 3256.53, -4686.60, 111.91 >>, 3.0)
				SET_PED_COVER_PROPERTIES(esExitEnemies[1].PedIndex, TRUE, FALSE, TRUE, FALSE, 0.0, FALSE)
				SET_PED_SHOOTING_PROPERTIES(esExitEnemies[1].PedIndex, 4)
				SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				
				COMBAT_SEQUENCE_GO_TO_COMBAT(esExitEnemies[2].PedIndex)
				SET_PED_COMBAT_PROPERTIES(esExitEnemies[2].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_USE_COVER)
				SET_PED_DEFENSIVE_AREA_SPHERE(esExitEnemies[2].PedIndex, << 3255.64, -4692.65, 111.84 >>, 3.0)
				SET_PED_COVER_PROPERTIES(esExitEnemies[2].PedIndex, TRUE, TRUE, FALSE, FALSE, 0.0, FALSE)
				SET_PED_SHOOTING_PROPERTIES(esExitEnemies[2].PedIndex, 3)
				SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[2].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)

				COMBAT_SEQUENCE_GO_TO_COMBAT(esExitEnemies[3].PedIndex)
				SET_PED_COMBAT_PROPERTIES(esExitEnemies[3].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
				SET_PED_DEFENSIVE_AREA_SPHERE(esExitEnemies[3].PedIndex, << 3263.23, -4672.08, 112.81 >>, 1.5)
				SET_PED_COVER_PROPERTIES(esExitEnemies[3].PedIndex, TRUE, TRUE, FALSE, FALSE, 0.0, FALSE)
				SET_PED_SHOOTING_PROPERTIES(esExitEnemies[3].PedIndex, 4)
				SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[3].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)

				COMBAT_SEQUENCE_GO_TO_COMBAT(esExitEnemies[4].PedIndex)
				SET_PED_COMBAT_PROPERTIES(esExitEnemies[4].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
				SET_PED_DEFENSIVE_AREA_SPHERE(esExitEnemies[4].PedIndex, << 3261.49, -4691.44, 112.03 >>, 3.0)
				SET_PED_COVER_PROPERTIES(esExitEnemies[4].PedIndex, TRUE, FALSE, TRUE, FALSE, 0.0, FALSE)
				SET_PED_SHOOTING_PROPERTIES(esExitEnemies[4].PedIndex, 3)
				SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[4].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)

				bCurrentEnemyWaveSpawned = TRUE
				
			ENDIF
		ENDIF
		
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		IF NOT IS_PED_INJURED(esExitEnemies[0].PedIndex)
		
			IF ( esExitEnemies[0].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esExitEnemies[0].bHasTask 	= FALSE
					esExitEnemies[0].iProgress 	= 100
					esExitEnemies[0].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esExitEnemies[0].iProgress
			
				CASE 100
				
					IF ( esExitEnemies[0].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esExitEnemies[0].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esExitEnemies[0].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esExitEnemies[0].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esExitEnemies[0].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						esExitEnemies[0].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		IF NOT IS_PED_INJURED(esExitEnemies[1].PedIndex)
		
			IF ( esExitEnemies[1].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esExitEnemies[1].bHasTask 	= FALSE
					esExitEnemies[1].iProgress 	= 100
					esExitEnemies[1].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esExitEnemies[1].iProgress
				
				CASE 0
				
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3257.505371,-4658.068848,116.680290>>,<<36.0,10.0,4.0>>)
					//OR ( GET_NUMBER_OF_ENEMIES_ALIVE(esChurchEnemies) + GET_NUMBER_OF_ENEMIES_ALIVE(esSideVanEnemies) < 3 )
		
						esExitEnemies[1].bHasTask = FALSE
						esExitEnemies[1].iProgress++
						
					ENDIF
				
				BREAK
				
				CASE 1
				
					IF ( esExitEnemies[1].bHasTask = FALSE )
						COMBAT_SEQUENCE_GO_TO_COMBAT(esExitEnemies[1].PedIndex)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[1].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_MEDIUM, CA_CAN_CHARGE)
						SET_PED_COVER_PROPERTIES(esExitEnemies[1].PedIndex, TRUE, FALSE, TRUE, FALSE, 7.5, FALSE, 0.0, 2.5, 5.5)
						SET_PED_SHOOTING_PROPERTIES(esExitEnemies[1].PedIndex, 4)
						esExitEnemies[1].bHasTask = TRUE
					ENDIF
				
				BREAK
			
				CASE 100
				
					IF ( esExitEnemies[1].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esExitEnemies[1].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esExitEnemies[1].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esExitEnemies[1].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esExitEnemies[1].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						esExitEnemies[1].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		IF NOT IS_PED_INJURED(esExitEnemies[2].PedIndex)
		
			IF ( esExitEnemies[2].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esExitEnemies[2].bHasTask 	= FALSE
					esExitEnemies[2].iProgress 	= 100
					esExitEnemies[2].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esExitEnemies[2].iProgress
				
				CASE 0
				
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3257.505371,-4658.068848,116.680290>>,<<36.0,10.0,4.0>>)
					//OR ( GET_NUMBER_OF_ENEMIES_ALIVE(esChurchEnemies) + GET_NUMBER_OF_ENEMIES_ALIVE(esSideVanEnemies) < 3 )
		
						esExitEnemies[2].bHasTask = FALSE
						esExitEnemies[2].iProgress++
						
					ENDIF
				
				BREAK
				
				CASE 1
				
					IF ( esExitEnemies[2].bHasTask = FALSE )
						COMBAT_SEQUENCE_GO_TO_COMBAT(esExitEnemies[2].PedIndex)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[2].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_MEDIUM, CA_CAN_CHARGE)
						SET_PED_COVER_PROPERTIES(esExitEnemies[2].PedIndex, TRUE, TRUE, FALSE, FALSE, 9.0, FALSE, 0.0, 3.0, 4.0)
						SET_PED_SHOOTING_PROPERTIES(esExitEnemies[2].PedIndex, 3)
						esExitEnemies[2].bHasTask = TRUE
					ENDIF
				
				BREAK
			
				CASE 100
				
					IF ( esExitEnemies[2].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esExitEnemies[2].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[2].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esExitEnemies[2].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esExitEnemies[2].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esExitEnemies[2].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						REMOVE_PED_PREFERRED_COVERPOINT(esExitEnemies[2].PedIndex, esExitEnemies[2].ItemsetIndex)
						esExitEnemies[2].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		IF NOT IS_PED_INJURED(esExitEnemies[3].PedIndex)
		
			IF ( esExitEnemies[3].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esExitEnemies[3].bHasTask 	= FALSE
					esExitEnemies[3].iProgress 	= 100
					esExitEnemies[3].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esExitEnemies[3].iProgress
				
				CASE 0
				
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3257.505371,-4658.068848,116.680290>>,<<36.0,10.0,4.0>>)
					OR ( GET_NUMBER_OF_ENEMIES_ALIVE(esChurchEnemies) + GET_NUMBER_OF_ENEMIES_ALIVE(esSideVanEnemies) < 3 )
		
						esExitEnemies[3].bHasTask = FALSE
						esExitEnemies[3].iProgress++
						
					ENDIF
				
				BREAK
				
				CASE 1
				
					IF ( esExitEnemies[3].bHasTask = FALSE )
						COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esExitEnemies[3].PedIndex, << 3263.08, -4665.64, 112.71 >>, PEDMOVE_RUN, FALSE, vZero, PLAYER_PED_ID(), FALSE)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[3].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_MEDIUM, CA_USE_COVER)
						SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[3].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[3].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_COVER_PROPERTIES(esExitEnemies[3].PedIndex, TRUE, FALSE, FALSE, FALSE, 0.0, FALSE)
						SET_PED_SHOOTING_PROPERTIES(esExitEnemies[3].PedIndex, 4)
						esExitEnemies[3].bHasTask = TRUE
					ENDIF
				
				BREAK
			
				CASE 100
				
					IF ( esExitEnemies[3].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esExitEnemies[3].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[3].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esExitEnemies[3].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esExitEnemies[3].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esExitEnemies[3].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						esExitEnemies[3].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		IF NOT IS_PED_INJURED(esExitEnemies[4].PedIndex)
		
			IF ( esExitEnemies[4].bFallingBack = FALSE )
				IF ( bPlayerUsedAlternativePath = TRUE )
					esExitEnemies[4].bHasTask 	= FALSE
					esExitEnemies[4].iProgress 	= 100
					esExitEnemies[4].bFallingBack = TRUE
				ENDIF
			ENDIF
			
			SWITCH esExitEnemies[4].iProgress
				
				CASE 0
				
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3257.505371,-4658.068848,116.680290>>,<<36.0,10.0,4.0>>)
					OR ( GET_NUMBER_OF_ENEMIES_ALIVE(esChurchEnemies) + GET_NUMBER_OF_ENEMIES_ALIVE(esSideVanEnemies) < 3 )
		
						esExitEnemies[4].bHasTask = FALSE
						esExitEnemies[4].iProgress++
						
					ENDIF
				
				BREAK
				
				CASE 1
				
					IF ( esExitEnemies[4].bHasTask = FALSE )
						COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esExitEnemies[4].PedIndex, << 3261.90, -4680.61, 111.95 >>, PEDMOVE_RUN, FALSE, vZero, PLAYER_PED_ID(), TRUE)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[4].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_COMBAT_ATTRIBUTES(esExitEnemies[4].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
						SET_PED_DEFENSIVE_AREA_SPHERE(esExitEnemies[4].PedIndex, << 3268.94, -4663.72, 112.88 >>, 4.0)
						SET_PED_COVER_PROPERTIES(esExitEnemies[4].PedIndex, TRUE, FALSE, FALSE, FALSE, 0.0, FALSE)
						SET_PED_SHOOTING_PROPERTIES(esExitEnemies[4].PedIndex, 3)
						esExitEnemies[4].bHasTask = TRUE
					ENDIF
				
				BREAK
			
				CASE 100
				
					IF ( esExitEnemies[4].bHasTask = FALSE )
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esExitEnemies[4].PedIndex, 100.0)
						SET_PED_COMBAT_PROPERTIES(esExitEnemies[4].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
						SET_PED_DEFENSIVE_AREA_ANGLED(esExitEnemies[4].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
						SET_PED_COMBAT_MOVEMENT_VALUES(esExitEnemies[4].PedIndex, 0.5, 0.0)
						SET_PED_COVER_PROPERTIES(esExitEnemies[4].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
						esExitEnemies[4].bHasTask = TRUE
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
		ENDIF
	
		CLEANUP_ENEMY_GROUP(esExitEnemies)

	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esExitEnemies)
	
ENDPROC

PROC MANAGE_FIFTH_ENEMY_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL&bCurrentEnemyWaveIgnored, BOOL bPreviousEnemyWaveSpawned)

	//check if locate ignoring this enemy wave was reached
	IF ( bCurrentEnemyWaveIgnored = FALSE )
		IF IS_ENTITY_IN_ANGLED_AREA_LOCATE(PLAYER_PED_ID(), <<3294.089111,-4665.589844,112.151215>>,
										   <<3222.692383,-4659.922852,117.261169>>, 16.0 #IF IS_DEBUG_BUILD, "IgnoreFifthWaveLocate" #ENDIF)
			bCurrentEnemyWaveIgnored = TRUE
		ENDIF
	ENDIF

	//create enemy wave when locate is reached
	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF ( bPreviousEnemyWaveSpawned = TRUE )

			IF ( bCurrentEnemyWaveIgnored = FALSE )	//create this enemy wave when locate for ignoring was not reached by player
			AND IS_ENTITY_IN_ANGLED_AREA_LOCATE(PLAYER_PED_ID(), <<3238.833984,-4639.374023,113.510277>>,
												<<3234.517334,-4652.879395,118.686172>>, 14.0 #IF IS_DEBUG_BUILD, "FifthWaveLocate" #ENDIF)

				esSideEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3240.9082, -4665.7764, 114.6382 >>, 0.0, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esSideEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3244.5220, -4672.7925, 113.1723 >>, 48.64, rgEnemies, WEAPONTYPE_PUMPSHOTGUN)
				esSideEnemies[2].PedIndex = CREATE_ENEMY_PED(mnEnemy, << 3231.5359, -4684.4063, 111.6351 >>, 306.53, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				INITIALISE_ENEMY_GROUP(esSideEnemies, FALSE #IF IS_DEBUG_BUILD, "Side " #ENDIF)
			
				
				SET_PED_COMBAT_PROPERTIES(esSideEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)				
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[0].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[0].PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
				SET_PED_DEFENSIVE_AREA_ANGLED(esSideEnemies[0].PedIndex, <<3239.528320,-4665.080566,113.577934>>, <<3240.278564,-4665.159180,116.508667>>, 1.0)
				SET_PED_PREFERRED_COVERPOINT(esSideEnemies[0].PedIndex, esSideEnemies[0].ItemsetIndex, csCoverpoints[25].CoverpointIndex)
				SET_PED_COVER_PROPERTIES(esSideEnemies[0].PedIndex, TRUE, TRUE, FALSE, FALSE, 8.0, FALSE, 0.0, 1.0, 4.5)
				SET_PED_SHOOTING_PROPERTIES(esSideEnemies[0].PedIndex, 15)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esSideEnemies[0].PedIndex, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideEnemies[0].PedIndex, 100.0)
				esSideEnemies[0].iTimer = GET_GAME_TIMER()
				
				COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esSideEnemies[1].PedIndex, << 3237.66, -4665.48, 113.57 >>, PEDMOVE_RUN,
																	 FALSE, vZero, PLAYER_PED_ID(), FALSE, FALSE, TRUE, 1000, FIRING_TYPE_CONTINUOUS)
				SET_PED_COMBAT_PROPERTIES(esSideEnemies[1].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, CA_AGGRESSIVE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[1].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[1].PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
				SET_PED_SHOOTING_PROPERTIES(esSideEnemies[1].PedIndex, 15, 125, FIRING_PATTERN_FULL_AUTO)
				
				SET_PED_COMBAT_PROPERTIES(esSideEnemies[2].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, CA_AGGRESSIVE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[2].PedIndex, CA_CAN_CHARGE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[2].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[2].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(esSideEnemies[2].PedIndex, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
				SET_PED_SHOOTING_PROPERTIES(esSideEnemies[2].PedIndex, 15, 125, FIRING_PATTERN_FULL_AUTO)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esSideEnemies[2].PedIndex, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideEnemies[2].PedIndex, 100.0)
				
				bCurrentEnemyWaveSpawned = TRUE
			
			ENDIF
			
		ENDIF

	ELSE	//wave is spawned, keep updating enemy behaviours
			
		CLEANUP_ENEMY_GROUP(esSideEnemies)
		
	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esSideEnemies)

ENDPROC

PROC MANAGE_FIRST_ENEMY_REINFORCEMENT_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bMainEnemyWaveSpawned)

	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF ( bMainEnemyWaveSpawned = FALSE )
			
			IF ( GET_NUMBER_OF_ENEMIES_DEAD(esMiddleEnemies) + GET_NUMBER_OF_ENEMIES_DEAD(esVanEnemies) + GET_NUMBER_OF_ENEMIES_DEAD(esWallEnemies) >= 8 )	

				esFirstReinforcementEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, <<3265.5913, -4642.5308, 113.3619>>, 341.20, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, TRUE)
				esFirstReinforcementEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, <<3284.5649, -4651.4722, 113.3171>>, 6.62, rgEnemies, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, TRUE)
				INITIALISE_ENEMY_GROUP(esFirstReinforcementEnemies, FALSE #IF IS_DEBUG_BUILD, "1st " #ENDIF)

				bCurrentEnemyWaveSpawned = TRUE
					
			ENDIF
			
		ENDIF
	
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		IF DOES_ENTITY_EXIST(esFirstReinforcementEnemies[0].PedIndex)
			IF NOT IS_ENTITY_DEAD(esFirstReinforcementEnemies[0].PedIndex)
				
				SWITCH esFirstReinforcementEnemies[0].iProgress
				
					CASE 0
						IF ( esFirstReinforcementEnemies[0].bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[0].PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[0].PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[0].PedIndex, CA_LEAVE_VEHICLES, TRUE)
							
							SET_PED_COMBAT_RANGE(esFirstReinforcementEnemies[0].PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(esFirstReinforcementEnemies[0].PedIndex, CM_WILLADVANCE)
							
							SET_PED_SHOOTING_PROPERTIES(esFirstReinforcementEnemies[0].PedIndex, 3)
							
							SET_COMBAT_FLOAT(esFirstReinforcementEnemies[0].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(esFirstReinforcementEnemies[0].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esFirstReinforcementEnemies[0].PedIndex, 200.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esFirstReinforcementEnemies[0].PedIndex, FALSE)
							
							esFirstReinforcementEnemies[0].bHasTask = TRUE
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(esFirstReinforcementEnemies[1].PedIndex)
			IF NOT IS_ENTITY_DEAD(esFirstReinforcementEnemies[1].PedIndex)
				
				SWITCH esFirstReinforcementEnemies[1].iProgress
				
					CASE 0
						IF ( esFirstReinforcementEnemies[1].bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[1].PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[1].PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(esFirstReinforcementEnemies[1].PedIndex, CA_LEAVE_VEHICLES, TRUE)
							
							SET_PED_COMBAT_RANGE(esFirstReinforcementEnemies[1].PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(esFirstReinforcementEnemies[1].PedIndex, CM_WILLADVANCE)
							
							SET_PED_SHOOTING_PROPERTIES(esFirstReinforcementEnemies[1].PedIndex, 4)
							
							SET_COMBAT_FLOAT(esFirstReinforcementEnemies[1].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(esFirstReinforcementEnemies[1].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esFirstReinforcementEnemies[1].PedIndex, 200.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esFirstReinforcementEnemies[1].PedIndex, FALSE)
							
							esFirstReinforcementEnemies[1].bHasTask = TRUE
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
		ENDIF
	
		CLEANUP_ENEMY_GROUP(esFirstReinforcementEnemies)

	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esFirstReinforcementEnemies)

ENDPROC

PROC MANAGE_SECOND_ENEMY_REINFORCEMENT_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bPreviousEnemyWaveSpawned, BOOL bMainEnemyWaveSpawned)

	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF 	( bMainEnemyWaveSpawned = FALSE)
		AND	( bPreviousEnemyWaveSpawned = TRUE )
			
			IF ( GET_NUMBER_OF_ENEMIES_DEAD(esFirstReinforcementEnemies) >= 1 )
			
				IF iSecondEnemyReinforcementDelayTimer = 0
				
					iSecondEnemyReinforcementDelayTimer = GET_GAME_TIMER()
					
				ELSE
				
					IF HAS_TIME_PASSED(3000, iSecondEnemyReinforcementDelayTimer)

						esSecondReinforcementEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, <<3283.0278, -4639.9214, 114.3816>>, 7.6314, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH, TRUE)
						esSecondReinforcementEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, <<3261.2634, -4645.3687, 112.9646>>, 352.95, rgEnemies, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, TRUE)
						INITIALISE_ENEMY_GROUP(esSecondReinforcementEnemies, FALSE #IF IS_DEBUG_BUILD, "2nd " #ENDIF)

						bCurrentEnemyWaveSpawned = TRUE
						
					ENDIF
					
				ENDIF
					
			ENDIF
			
		ENDIF
	
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		IF DOES_ENTITY_EXIST(esSecondReinforcementEnemies[0].PedIndex)
			IF NOT IS_ENTITY_DEAD(esSecondReinforcementEnemies[0].PedIndex)
				
				SWITCH esSecondReinforcementEnemies[0].iProgress
				
					CASE 0
						IF ( esSecondReinforcementEnemies[0].bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[0].PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[0].PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[0].PedIndex, CA_LEAVE_VEHICLES, TRUE)
							
							SET_PED_COMBAT_RANGE(esSecondReinforcementEnemies[0].PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(esSecondReinforcementEnemies[0].PedIndex, CM_WILLADVANCE)
							
							SET_PED_SHOOTING_PROPERTIES(esSecondReinforcementEnemies[0].PedIndex, 3)
							
							SET_COMBAT_FLOAT(esSecondReinforcementEnemies[0].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(esSecondReinforcementEnemies[0].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSecondReinforcementEnemies[0].PedIndex, 200.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esSecondReinforcementEnemies[0].PedIndex, FALSE)
							
							esSecondReinforcementEnemies[0].bHasTask = TRUE
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(esSecondReinforcementEnemies[1].PedIndex)
			IF NOT IS_ENTITY_DEAD(esSecondReinforcementEnemies[1].PedIndex)
				
				SWITCH esSecondReinforcementEnemies[1].iProgress
				
					CASE 0
						IF ( esSecondReinforcementEnemies[1].bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[1].PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[1].PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(esSecondReinforcementEnemies[1].PedIndex, CA_LEAVE_VEHICLES, TRUE)
							
							SET_PED_COMBAT_RANGE(esSecondReinforcementEnemies[1].PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(esSecondReinforcementEnemies[1].PedIndex, CM_WILLADVANCE)
							
							SET_PED_SHOOTING_PROPERTIES(esSecondReinforcementEnemies[1].PedIndex, 4)
							
							SET_COMBAT_FLOAT(esSecondReinforcementEnemies[1].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(esSecondReinforcementEnemies[1].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSecondReinforcementEnemies[1].PedIndex, 200.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esSecondReinforcementEnemies[1].PedIndex, FALSE)
							
							esSecondReinforcementEnemies[1].bHasTask = TRUE
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
		ENDIF
	
		CLEANUP_ENEMY_GROUP(esSecondReinforcementEnemies)

	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esSecondReinforcementEnemies)

ENDPROC

PROC MANAGE_THIRD_ENEMY_REINFORCEMENT_WAVE(BOOL &bCurrentEnemyWaveSpawned, BOOL bPreviousEnemyWaveSpawned, BOOL bMainEnemyWaveSpawned)

	IF ( bCurrentEnemyWaveSpawned = FALSE )

		IF 	( bMainEnemyWaveSpawned = FALSE)
		AND	( bPreviousEnemyWaveSpawned = TRUE )
			
			IF 	( GET_NUMBER_OF_ENEMIES_DEAD(esSecondReinforcementEnemies) >= 1 )
				
				IF iThirdEnemyReinforcementDelayTimer = 0
				
					iThirdEnemyReinforcementDelayTimer = GET_GAME_TIMER()
					
				ELSE
				
					IF HAS_TIME_PASSED(5000, iThirdEnemyReinforcementDelayTimer)

						esThirdReinforcementEnemies[0].PedIndex = CREATE_ENEMY_PED(mnEnemy, <<3265.5913, -4642.5308, 113.3619>>, 341.20, rgEnemies, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, TRUE)
						esThirdReinforcementEnemies[1].PedIndex = CREATE_ENEMY_PED(mnEnemy, <<3284.5649, -4651.4722, 113.3171>>, 6.62, rgEnemies, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, TRUE)
						INITIALISE_ENEMY_GROUP(esThirdReinforcementEnemies, FALSE #IF IS_DEBUG_BUILD, "3rd " #ENDIF)

						bCurrentEnemyWaveSpawned = TRUE
						
					ENDIF
					
				ENDIF
					
			ENDIF
			
		ENDIF
	
	ELSE	//wave is spawned, keep updating enemy behaviours
	
		IF DOES_ENTITY_EXIST(esThirdReinforcementEnemies[0].PedIndex)
			IF NOT IS_ENTITY_DEAD(esThirdReinforcementEnemies[0].PedIndex)
				
				SWITCH esThirdReinforcementEnemies[0].iProgress
				
					CASE 0
						IF ( esThirdReinforcementEnemies[0].bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[0].PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[0].PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[0].PedIndex, CA_LEAVE_VEHICLES, TRUE)
							
							SET_PED_COMBAT_RANGE(esThirdReinforcementEnemies[0].PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(esThirdReinforcementEnemies[0].PedIndex, CM_WILLADVANCE)
							
							SET_PED_SHOOTING_PROPERTIES(esThirdReinforcementEnemies[0].PedIndex, 3)
							
							SET_COMBAT_FLOAT(esThirdReinforcementEnemies[0].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(esThirdReinforcementEnemies[0].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esThirdReinforcementEnemies[0].PedIndex, 200.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esThirdReinforcementEnemies[0].PedIndex, FALSE)
							
							esThirdReinforcementEnemies[0].bHasTask = TRUE
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(esThirdReinforcementEnemies[1].PedIndex)
			IF NOT IS_ENTITY_DEAD(esThirdReinforcementEnemies[1].PedIndex)
				
				SWITCH esThirdReinforcementEnemies[1].iProgress
				
					CASE 0
						IF ( esThirdReinforcementEnemies[1].bHasTask = FALSE )
						
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[1].PedIndex, CA_AGGRESSIVE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[1].PedIndex, CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(esThirdReinforcementEnemies[1].PedIndex, CA_LEAVE_VEHICLES, TRUE)
							
							SET_PED_COMBAT_RANGE(esThirdReinforcementEnemies[1].PedIndex, CR_NEAR)
							SET_PED_COMBAT_MOVEMENT(esThirdReinforcementEnemies[1].PedIndex, CM_WILLADVANCE)
							
							SET_PED_SHOOTING_PROPERTIES(esThirdReinforcementEnemies[1].PedIndex, 4)
							
							SET_COMBAT_FLOAT(esThirdReinforcementEnemies[1].PedIndex, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
							SET_COMBAT_FLOAT(esThirdReinforcementEnemies[1].PedIndex, CCF_WALK_WHEN_STRAFING_CHANCE, 0.0)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esThirdReinforcementEnemies[1].PedIndex, 200.0)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esThirdReinforcementEnemies[1].PedIndex, FALSE)
							
							esThirdReinforcementEnemies[1].bHasTask = TRUE
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
		ENDIF
	
		CLEANUP_ENEMY_GROUP(esThirdReinforcementEnemies)

	ENDIF
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esThirdReinforcementEnemies)

ENDPROC

PROC MANAGE_FIRST_ENEMY_VAN_AND_ENEMIES(INT &iProgress)
	
	FLOAT fPlaybackPercentage
		
	SWITCH iProgress
	
		CASE 0
		
			REQUEST_ANIM_DICT("missmic1ig_2")
	
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 3267.560303, -4588.922363, 118.005730 >>, << 30.0, 3.5, 2.0 >>)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3287.814453,-4589.952148,116.589165>>, <<3294.002441,-4561.976074,120.122002>>, 12.0)
			OR ( iFirstVanTriggerTimer != 0 AND HAS_TIME_PASSED(25000, iFirstVanTriggerTimer) )
				
				IF 	HAS_MODEL_LOADED(mnEnemy)
				AND HAS_MODEL_LOADED(mnEnemyVan)
			
					//first van on recording
					vsEnemyVans[0].VehicleIndex = CREATE_ENEMY_VEHICLE(vsEnemyVans[0], 800, FALSE, TRUE, TRUE, TRUE, VEHICLELOCK_UNLOCKED, 30, 2, FALSE #IF IS_DEBUG_BUILD, "EnemyVan 0" #ENDIF)
					
					//make the van only take damage from the player initially, so that it does not take damage when smashing through the gate
					SET_ENTITY_INVINCIBLE(vsEnemyVans[0].VehicleIndex, TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vsEnemyVans[0].VehicleIndex, TRUE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vsEnemyVans[0].VehicleIndex, FALSE)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsEnemyVans[0].VehicleIndex, TRUE)
					
					esVanEnemies[0].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[0].VehicleIndex, VS_DRIVER, rgEnemies, WEAPONTYPE_PISTOL)
					esVanEnemies[1].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[0].VehicleIndex, VS_FRONT_RIGHT, rgEnemies, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH)
					esVanEnemies[2].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[0].VehicleIndex, VS_BACK_LEFT, rgEnemies, WEAPONTYPE_ASSAULTRIFLE, WEAPONCOMPONENT_AT_AR_FLSH)
					esVanEnemies[3].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[0].VehicleIndex, VS_BACK_RIGHT, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
					INITIALISE_ENEMY_GROUP(esVanEnemies, FALSE #IF IS_DEBUG_BUILD, "Van " #ENDIF)
					
					SET_PED_SEEING_RANGE(esVanEnemies[0].PedIndex, 20.0)
					SET_PED_SEEING_RANGE(esVanEnemies[1].PedIndex, 15.0)
					SET_PED_SEEING_RANGE(esVanEnemies[2].PedIndex, 20.0)
					SET_PED_SEEING_RANGE(esVanEnemies[3].PedIndex, 20.0)
					
					SET_ENTITY_INVINCIBLE(esVanEnemies[0].PedIndex, TRUE)
					SET_ENTITY_INVINCIBLE(esVanEnemies[1].PedIndex, TRUE)
					SET_ENTITY_INVINCIBLE(esVanEnemies[2].PedIndex, TRUE)
					SET_ENTITY_INVINCIBLE(esVanEnemies[3].PedIndex, TRUE)
				
					START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE(vsEnemyVans[0], sVehicleRecordingsFile, 1.35)
					
					START_AUDIO_SCENE("MI_1_VAN_ARRIVES_01")
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vsEnemyVans[0].VehicleIndex, "MI_1_VAN_01")
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
					bReplayStartEventTriggered = TRUE

					iProgress++
					
				ENDIF
				
			ENDIF
				
		BREAK
		
		CASE 1
		
			REQUEST_ANIM_DICT("missmic1ig_2")
		
			IF IS_VEHICLE_RECORDING_PLAYBACK_RUNNING_OR_DRIVER_DEAD(vsEnemyVans[0].VehicleIndex, fPlaybackPercentage, vsEnemyVans[0].bDriverDead)
				
				IF 	( fPlaybackPercentage > 32.0 )
				AND ( fPlaybackPercentage < 48.0 )

					bOpenGateTriggered = TRUE

				ELIF ( fPlaybackPercentage > 48.0 )

					IF ( bSmoothCloseGateTriggered = FALSE )
					
						//make the van be damaged by anything and any ped after it has got past the gate
						IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
							SET_ENTITY_INVINCIBLE(vsEnemyVans[0].VehicleIndex, FALSE)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vsEnemyVans[0].VehicleIndex, FALSE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vsEnemyVans[0].VehicleIndex, TRUE)
							
							IF DOES_ENTITY_EXIST(esVanEnemies[0].PedIndex)
								IF NOT IS_ENTITY_DEAD(esVanEnemies[0].PedIndex)
									SET_ENTITY_INVINCIBLE(esVanEnemies[0].PedIndex, FALSE)
								ENDIF
							ENDIF
							IF DOES_ENTITY_EXIST(esVanEnemies[1].PedIndex)
								IF NOT IS_ENTITY_DEAD(esVanEnemies[1].PedIndex)
									SET_ENTITY_INVINCIBLE(esVanEnemies[1].PedIndex, FALSE)
								ENDIF
							ENDIF
							IF DOES_ENTITY_EXIST(esVanEnemies[2].PedIndex)
								IF NOT IS_ENTITY_DEAD(esVanEnemies[2].PedIndex)
									SET_ENTITY_INVINCIBLE(esVanEnemies[2].PedIndex, FALSE)
								ENDIF
							ENDIF
							IF DOES_ENTITY_EXIST(esVanEnemies[3].PedIndex)
								IF NOT IS_ENTITY_DEAD(esVanEnemies[3].PedIndex)
									SET_ENTITY_INVINCIBLE(esVanEnemies[3].PedIndex, FALSE)
								ENDIF
							ENDIF
							
						ENDIF

						bSmoothCloseGateTriggered = TRUE
					
					ENDIF

				ENDIF

				
				IF ( fPlaybackPercentage > 97.0 )
					
					IF ( esVanEnemies[0].bHasTask = FALSE )
					
						IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
							STOP_PLAYBACK_RECORDED_VEHICLE(vsEnemyVans[0].VehicleIndex)
						ENDIF
						
						esVanEnemies[0].bHasTask = TRUE
						
					ENDIF

				ENDIF
				
				//if driver was killed during a playback was going on
				//make the peds leave the vehicle and reset the dead driver flag
				//to make IS_VEHICLE_RECORDING_PLAYBACK_RUNNING_OR_DRIVER_DEAD() keep returning FALSE next frame
				IF ( vsEnemyVans[0].bDriverDead = TRUE )
					vsEnemyVans[0].bDriverDead = FALSE
				ENDIF
				
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsEnemyVans[0].VehicleIndex, PLAYER_PED_ID())
					bPlayerDetected = TRUE
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player damaged enemy van 0 during recording playback. Setting bPlayerDetected to TRUE.")
					#ENDIF
				ENDIF
				
			ELSE

				iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 2 //play the van exit animation
		
			REQUEST_ANIM_DICT("missmic1ig_2")
			
			IF HAS_ANIM_DICT_LOADED("missmic1ig_2")
			
				IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
				
					IF ( bPlayerDetected = FALSE )
			
						VECTOR vVanPosition
						VECTOR vVanRotation
						
						vVanPosition = GET_ENTITY_COORDS(vsEnemyVans[0].VehicleIndex)
						vVanRotation = GET_ENTITY_ROTATION(vsEnemyVans[0].VehicleIndex)	

						IF NOT IS_ENTITY_DEAD(esVanEnemies[0].PedIndex)
							CLEAR_PED_TASKS_IMMEDIATELY(esVanEnemies[0].PedIndex)
							EQUIP_PED_BEST_WEAPON(esVanEnemies[0].PedIndex, TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(esVanEnemies[0].PedIndex, vsEnemyVans[0].VehicleIndex, TRUE)
							TASK_PLAY_ANIM_ADVANCED(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda", vVanPosition, vVanRotation,
													SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(esVanEnemies[0].PedIndex)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_BULLET_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_PLAYER_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
							
							GO_TO_ENEMY_STATE(esVanEnemies[0], ENEMY_STATE_SCRIPTED_ENTRY)
							esVanEnemies[0].iProgress = 0
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(esVanEnemies[1].PedIndex)
							CLEAR_PED_TASKS_IMMEDIATELY(esVanEnemies[1].PedIndex)
							EQUIP_PED_BEST_WEAPON(esVanEnemies[1].PedIndex, TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(esVanEnemies[1].PedIndex, vsEnemyVans[0].VehicleIndex, TRUE)
							TASK_PLAY_ANIM_ADVANCED(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb", vVanPosition, vVanRotation,
													SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(esVanEnemies[1].PedIndex)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_BULLET_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_PLAYER_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
							
							GO_TO_ENEMY_STATE(esVanEnemies[1], ENEMY_STATE_SCRIPTED_ENTRY)
							esVanEnemies[1].iProgress = 0
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(esVanEnemies[2].PedIndex)
							CLEAR_PED_TASKS_IMMEDIATELY(esVanEnemies[2].PedIndex)
							EQUIP_PED_BEST_WEAPON(esVanEnemies[2].PedIndex, TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(esVanEnemies[2].PedIndex, vsEnemyVans[0].VehicleIndex, TRUE)
							TASK_PLAY_ANIM_ADVANCED(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc", vVanPosition, vVanRotation,
													SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(esVanEnemies[2].PedIndex)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_BULLET_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_PLAYER_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
							
							GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_SCRIPTED_ENTRY)
							esVanEnemies[2].iProgress = 0
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(esVanEnemies[3].PedIndex)
							CLEAR_PED_TASKS_IMMEDIATELY(esVanEnemies[3].PedIndex)
							EQUIP_PED_BEST_WEAPON(esVanEnemies[3].PedIndex, TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(esVanEnemies[3].PedIndex, vsEnemyVans[0].VehicleIndex, TRUE)
							TASK_PLAY_ANIM_ADVANCED(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd", vVanPosition, vVanRotation,
													SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,
													AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(esVanEnemies[3].PedIndex)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_BULLET_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_PLAYER_IMPACT)
							SET_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
							
							GO_TO_ENEMY_STATE(esVanEnemies[3], ENEMY_STATE_SCRIPTED_ENTRY)
							esVanEnemies[3].iProgress = 0
						ENDIF
						
						//lower the health a bit when enemies start coming out
						SET_ENTITY_HEALTH(vsEnemyVans[0].VehicleIndex, CLAMP_INT(GET_ENTITY_HEALTH(vsEnemyVans[0].VehicleIndex) - 100, 400, 700))
						
						//make the van no longer targetable
						SET_VEHICLE_CAN_BE_TARGETTED(vsEnemyVans[0].VehicleIndex, FALSE)
					
					ELSE
					
						GO_TO_ENEMY_STATE(esVanEnemies[0], ENEMY_STATE_COMBAT)
						GO_TO_ENEMY_STATE(esVanEnemies[1], ENEMY_STATE_COMBAT)
						GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_COMBAT)
						GO_TO_ENEMY_STATE(esVanEnemies[3], ENEMY_STATE_COMBAT)
					
					ENDIF
				
				ENDIF
				
				STOP_AUDIO_SCENE("MI_1_VAN_ARRIVES_01")
				IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[0].VehicleIndex)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vsEnemyVans[0].VehicleIndex)
				ENDIF
			
				iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 3	//update enemies
			
			//block enemy weapon fire until the 'player detected' conversation plays out
			IF ( bPlayerDetected = FALSE )
				BLOCK_WEAPON_FIRE_FOR_ENEMY_GROUP(esVanEnemies)
			ELIF ( bPlayerDetected = TRUE )
				IF ( bDetectedConversationPlaying = TRUE )
					BLOCK_WEAPON_FIRE_FOR_ENEMY_GROUP(esVanEnemies)
				ENDIF
				IF ( bReplayStopEventTriggered = FALSE )
					REPLAY_STOP_EVENT()
					bReplayStopEventTriggered = TRUE
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(esVanEnemies[0].PedIndex)
				IF  NOT IS_ENTITY_DEAD(esVanEnemies[0].PedIndex)
				
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(esVanEnemies[0].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esVanEnemies[0].eState), 1.25)
						esVanEnemies[0].sDebugName = "VanEnemy 0"
					#ENDIF
					
					SWITCH esVanEnemies[0].eState
					
						CASE ENEMY_STATE_SCRIPTED_ENTRY
						
							SWITCH esVanEnemies[0].iProgress
					
								CASE 0

									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda")
										IF HAS_ANIM_EVENT_FIRED(esVanEnemies[0].PedIndex, GET_HASH_KEY("door"))
											IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
												SET_VEHICLE_DOOR_OPEN(vsEnemyVans[0].VehicleIndex, SC_DOOR_FRONT_LEFT)
												bPlayVanOpenConversation = TRUE
												esVanEnemies[0].iProgress++
											ENDIF
										ENDIF
									ENDIF

								BREAK
								
								CASE 1
									
									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda")						
										IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda") > 0.975 )
											esVanEnemies[0].bHasTask = FALSE
											esVanEnemies[0].iProgress++
										ELSE
											IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda") > 0.4 )
											
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_BULLET_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_PLAYER_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
											
												IF 	( GET_DISTANCE_BETWEEN_PEDS(esVanEnemies[0].PedIndex, PLAYER_PED_ID()) < 5.0 )
												AND CAN_PED_SEE_HATED_PED(esVanEnemies[0].PedIndex, PLAYER_PED_ID())
													GO_TO_ENEMY_STATE(esVanEnemies[0], ENEMY_STATE_COMBAT)
													esVanEnemies[0].iProgress = 0
												ENDIF
												IF IS_ENTITY_TOUCHING_ENTITY(esVanEnemies[0].PedIndex, PLAYER_PED_ID())
												OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[0].PedIndex, PLAYER_PED_ID())
													CLEAR_ENTITY_LAST_DAMAGE_ENTITY(esVanEnemies[0].PedIndex)
													GO_TO_ENEMY_STATE(esVanEnemies[0], ENEMY_STATE_COMBAT)
													bPlayerDetected = TRUE	//set the player detected flag to true here for other peds to react
													esVanEnemies[0].iProgress = 0
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 2
								
									IF ( esVanEnemies[0].bHasTask = FALSE )
										TASK_PUT_PED_DIRECTLY_INTO_COVER(esVanEnemies[0].PedIndex, csCoverpoints[28].vPosition, -1, FALSE, 0, TRUE, TRUE, csCoverpoints[28].CoverpointIndex)							
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[0].PedIndex, FALSE)
										esVanEnemies[0].iTimer = GET_GAME_TIMER()
										esVanEnemies[0].bHasTask = TRUE
									ENDIF
								
								BREAK
								
							ENDSWITCH
							
							IF IS_PED_IN_COMBAT(esVanEnemies[0].PedIndex)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[0].PedIndex, PLAYER_PED_ID())
							OR ( bPlayerDetected = TRUE )
								IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda")
								OR ( IS_ENTITY_PLAYING_ANIM(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda")
									 AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda") > 0.4 )
									
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_BULLET_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_PLAYER_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[0].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
									 
									GO_TO_ENEMY_STATE(esVanEnemies[0], ENEMY_STATE_COMBAT)
									esVanEnemies[0].iProgress = 0
								ENDIF
							ENDIF
							
							IF ( bPlayerDetected = FALSE )
								IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsEnemyVans[0].VehicleIndex, PLAYER_PED_ID())
										OR IS_PED_SHOOTING_AT_AREA(PLAYER_PED_ID(), TRUE, GET_ENTITY_COORDS(vsEnemyVans[0].VehicleIndex), 4.0 #IF IS_DEBUG_BUILD, TRUE #ENDIF)
											bPlayerDetected = TRUE
											#IF IS_DEBUG_BUILD
												PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player shot at enemy van 0 during scripted entry state. Setting bPlayerDetected to TRUE.")
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE ENEMY_STATE_COMBAT
						
							IF ( esVanEnemies[0].bHasTask = FALSE )
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVanEnemies[0].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esVanEnemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_USE_COVER)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[0].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_DEFENSIVE_AREA_SPHERE(esVanEnemies[0].PedIndex, << 3266.66, -4615.01, 115.07 >>, 2.75)
								SET_PED_COMBAT_MOVEMENT_VALUES(esVanEnemies[0].PedIndex, 1.0, 1.0)
								SET_PED_COVER_PROPERTIES(esVanEnemies[0].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, FALSE, 0.0, 2.5)
								//SET_PED_PREFERRED_COVERPOINT(esVanEnemies[0].PedIndex, esVanEnemies[0].ItemsetIndex, csCoverpoints[28].CoverpointIndex)
								SET_PED_SHOOTING_PROPERTIES(esVanEnemies[0].PedIndex, 4)
								SET_PED_VISUAL_FIELD_PROPERTIES(esVanEnemies[0].PedIndex)	//restore ped's default seeing values
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[0].PedIndex, FALSE)
								
								bPlayerDetected = TRUE
								
								esVanEnemies[0].bHasTask = TRUE
							ENDIF
						
						BREAK
					
					ENDSWITCH
					
				ELSE
				
					IF 	IS_ENTITY_PLAYING_ANIM(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda") 
					AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda") > 0.399
						STOP_ANIM_PLAYBACK(esVanEnemies[0].PedIndex)
						SET_HIGH_FALL_TASK(esVanEnemies[0].PedIndex, 2000, 10000)
						APPLY_FORCE_TO_ENTITY(esVanEnemies[0].PedIndex, APPLY_TYPE_IMPULSE, << 0.0, 5.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
					ENDIF
				
					IF ( bPlayerDetected = FALSE )
						IF ( esVanEnemies[0].eState = ENEMY_STATE_SCRIPTED_ENTRY )
							IF DOES_ENTITY_EXIST(GET_PED_SOURCE_OF_DEATH(esVanEnemies[0].PedIndex))
								IF ( GET_PED_INDEX_FROM_ENTITY_INDEX(GET_PED_SOURCE_OF_DEATH(esVanEnemies[0].PedIndex)) = PLAYER_PED_ID() )
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player detected for killing ", esVanEnemies[0].sDebugName, " in scripted entry state.")
									#ENDIF
									bPlayerDetected = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[0].PedIndex, "missmic1ig_2", "jump_out_peda")
						CLEANUP_ENEMY_PED(esVanEnemies[0])
					ENDIF
				
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(esVanEnemies[1].PedIndex)
				IF NOT IS_ENTITY_DEAD(esVanEnemies[1].PedIndex)
				
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(esVanEnemies[1].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esVanEnemies[1].eState), 1.25)
						esVanEnemies[1].sDebugName = "VanEnemy 1"
					#ENDIF
					
					SWITCH esVanEnemies[1].eState
					
						CASE ENEMY_STATE_SCRIPTED_ENTRY
						
							SWITCH esVanEnemies[1].iProgress
					
								CASE 0

									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb")
										IF HAS_ANIM_EVENT_FIRED(esVanEnemies[1].PedIndex, GET_HASH_KEY("door"))
											IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
												SET_VEHICLE_DOOR_OPEN(vsEnemyVans[0].VehicleIndex, SC_DOOR_FRONT_RIGHT)
												bPlayVanOpenConversation = TRUE
												esVanEnemies[1].iProgress++
											ENDIF
										ENDIF
									ENDIF
								BREAK
								
								CASE 1
									
									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb")
										IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb") > 0.95 )
											esVanEnemies[1].bHasTask = FALSE
													esVanEnemies[1].iProgress++
										ELSE
											IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb") > 0.185 )
											
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_BULLET_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_PLAYER_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
											
												IF ( GET_DISTANCE_BETWEEN_PEDS(esVanEnemies[1].PedIndex, PLAYER_PED_ID()) < 5.0 )
												AND CAN_PED_SEE_HATED_PED(esVanEnemies[1].PedIndex, PLAYER_PED_ID())
													GO_TO_ENEMY_STATE(esVanEnemies[1], ENEMY_STATE_COMBAT)
													esVanEnemies[1].iProgress = 0
												ENDIF
												IF IS_ENTITY_TOUCHING_ENTITY(esVanEnemies[1].PedIndex, PLAYER_PED_ID())
												OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[1].PedIndex, PLAYER_PED_ID())
													CLEAR_ENTITY_LAST_DAMAGE_ENTITY(esVanEnemies[1].PedIndex)
													GO_TO_ENEMY_STATE(esVanEnemies[1], ENEMY_STATE_COMBAT)
													bPlayerDetected = TRUE	//set the player detected flag to true here for other peds to react
													esVanEnemies[1].iProgress = 0
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 2
								
									//make ped stay move to the pillar cover while aiming
									IF ( esVanEnemies[1].bHasTask = FALSE )
										SEQUENCE_INDEX SequenceIndex
										OPEN_SEQUENCE_TASK(SequenceIndex)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, csCoverpoints[2].vPosition, << 3254.10, -4587.78, 118.05 >>, PEDMOVE_SLOWWALK, FALSE, 1.0, 0.0, TRUE, ENAV_NO_STOPPING)
											TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, csCoverpoints[2].vPosition, -1, TRUE, 0.25, TRUE, FALSE, csCoverpoints[2].CoverpointIndex, TRUE)
										CLOSE_SEQUENCE_TASK(SequenceIndex)
										TASK_PERFORM_SEQUENCE(esVanEnemies[1].PedIndex, SequenceIndex)
										CLEAR_SEQUENCE_TASK(SequenceIndex)
										SET_PED_COMBAT_PROPERTIES(esVanEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED)
										SET_PED_DEFENSIVE_AREA_SPHERE(esVanEnemies[1].PedIndex, << 3252.73, -4600.65, 115.64 >>, 1.5)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[1].PedIndex, FALSE)
										esVanEnemies[1].iTimer = GET_GAME_TIMER()
										esVanEnemies[1].bHasTask = TRUE
									ENDIF
								
								BREAK
								
							ENDSWITCH
							
							IF IS_PED_IN_COMBAT(esVanEnemies[1].PedIndex)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[1].PedIndex, PLAYER_PED_ID())
							OR ( bPlayerDetected = TRUE )
								IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb")
								OR ( IS_ENTITY_PLAYING_ANIM(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb")
									 AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb") > 0.185 )
									 
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_BULLET_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_PLAYER_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[1].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
									 
									GO_TO_ENEMY_STATE(esVanEnemies[1], ENEMY_STATE_COMBAT)
									esVanEnemies[1].iProgress = 0
								ENDIF
							ENDIF
							
							IF ( bPlayerDetected = FALSE )
								IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsEnemyVans[0].VehicleIndex, PLAYER_PED_ID())
										OR IS_PED_SHOOTING_AT_AREA(PLAYER_PED_ID(), TRUE, GET_ENTITY_COORDS(vsEnemyVans[0].VehicleIndex), 4.0 #IF IS_DEBUG_BUILD, TRUE #ENDIF)
											bPlayerDetected = TRUE
											#IF IS_DEBUG_BUILD
												PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player shot at enemy van 0 during scripted entry state. Setting bPlayerDetected to TRUE.")
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE ENEMY_STATE_COMBAT
						
							IF ( esVanEnemies[1].bHasTask = FALSE )
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVanEnemies[1].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esVanEnemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED)
								SET_PED_DEFENSIVE_AREA_SPHERE(esVanEnemies[1].PedIndex, << 3252.73, -4600.65, 115.64 >>, 1.5)
								SET_PED_SHOOTING_PROPERTIES(esVanEnemies[1].PedIndex, 4)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
								SET_COMBAT_FLOAT(esVanEnemies[1].PedIndex, CCF_BLIND_FIRE_CHANCE, 0.05)
								SET_PED_VISUAL_FIELD_PROPERTIES(esVanEnemies[1].PedIndex)	//restore ped's default seeing values
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[1].PedIndex, FALSE)
								bPlayerDetected = TRUE
								esVanEnemies[1].bHasTask = TRUE
							ENDIF
						
						BREAK
					
					ENDSWITCH
					
				ELSE
				
					IF 	IS_ENTITY_PLAYING_ANIM(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb") 
					AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb") > 0.165
						STOP_ANIM_PLAYBACK(esVanEnemies[1].PedIndex)
						SET_HIGH_FALL_TASK(esVanEnemies[1].PedIndex, 2000, 10000)
						APPLY_FORCE_TO_ENTITY(esVanEnemies[1].PedIndex, APPLY_TYPE_IMPULSE, << 0.0, 5.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
					ENDIF
				
					IF ( bPlayerDetected = FALSE )
						IF ( esVanEnemies[1].eState = ENEMY_STATE_SCRIPTED_ENTRY )
							IF DOES_ENTITY_EXIST(GET_PED_SOURCE_OF_DEATH(esVanEnemies[1].PedIndex))
								IF ( GET_PED_INDEX_FROM_ENTITY_INDEX(GET_PED_SOURCE_OF_DEATH(esVanEnemies[1].PedIndex)) = PLAYER_PED_ID() )
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player detected for killing ", esVanEnemies[1].sDebugName, " in scripted entry state.")
									#ENDIF
									bPlayerDetected = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[1].PedIndex, "missmic1ig_2", "jump_out_pedb")
						CLEANUP_ENEMY_PED(esVanEnemies[1])
					ENDIF
				
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(esVanEnemies[2].PedIndex)
				IF NOT IS_ENTITY_DEAD(esVanEnemies[2].PedIndex)
				
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(esVanEnemies[2].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esVanEnemies[2].eState), 1.25)
						esVanEnemies[2].sDebugName = "VanEnemy 2"
					#ENDIF
					
					SWITCH esVanEnemies[2].eState
					
						CASE ENEMY_STATE_SCRIPTED_ENTRY
						
							SWITCH esVanEnemies[2].iProgress
					
								CASE 0
								
									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc")
										IF HAS_ANIM_EVENT_FIRED(esVanEnemies[2].PedIndex, GET_HASH_KEY("door"))
											IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
												SET_VEHICLE_DOOR_OPEN(vsEnemyVans[0].VehicleIndex, SC_DOOR_REAR_LEFT)
												bPlayVanOpenConversation = TRUE
												esVanEnemies[2].iProgress++
											ENDIF
										ENDIF
									ENDIF

								BREAK
								
								CASE 1
									
									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc")
									
										SET_PED_RESET_FLAG(esVanEnemies[2].PedIndex, PRF_InstantBlendToAim, TRUE)
									
										IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc") > 0.925 )
											esVanEnemies[2].bHasTask = FALSE
											esVanEnemies[2].iProgress++
										ELSE
											IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc") > 0.585 )
							
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_BULLET_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_PLAYER_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
							
												IF 	( GET_DISTANCE_BETWEEN_PEDS(esVanEnemies[2].PedIndex, PLAYER_PED_ID()) < 5.0 )
												AND	CAN_PED_SEE_HATED_PED(esVanEnemies[2].PedIndex, PLAYER_PED_ID())
													GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_COMBAT)
													esVanEnemies[2].iProgress = 0
												ENDIF
												IF IS_ENTITY_TOUCHING_ENTITY(esVanEnemies[2].PedIndex, PLAYER_PED_ID())
												OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[2].PedIndex, PLAYER_PED_ID())
													CLEAR_ENTITY_LAST_DAMAGE_ENTITY(esVanEnemies[2].PedIndex)
													GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_COMBAT)
													bPlayerDetected = TRUE	//set the player detected flag to true here for other peds to react
													esVanEnemies[2].iProgress = 0
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 2
								
									SET_PED_RESET_FLAG(esVanEnemies[2].PedIndex, PRF_InstantBlendToAim, TRUE)
								
									IF ( esVanEnemies[2].bHasTask = FALSE )
										COMBAT_SEQUENCE_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(esVanEnemies[2].PedIndex, << 3260.40, -4605.49, 115.50 >>, PEDMOVE_SLOWWALK, FALSE, << 3264.06, -4588.00, 116.88 >>, NULL, FALSE, FALSE, FALSE, -1)
										esVanEnemies[2].iTimer = GET_GAME_TIMER()
										esVanEnemies[2].bHasTask = TRUE
									ENDIF
									
									IF HAS_TIME_PASSED(750, esVanEnemies[2].iTimer)
										bPlayVanExitConversation = TRUE
									ENDIF
									
									IF HAS_TIME_PASSED(6000, esVanEnemies[2].iTimer)
										GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_STANDING_AT_LOCATION)
									ENDIF

								BREAK

							ENDSWITCH
							
							IF IS_PED_IN_COMBAT(esVanEnemies[2].PedIndex)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[2].PedIndex, PLAYER_PED_ID())
							OR ( bPlayerDetected = TRUE )
								IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc")
								OR ( IS_ENTITY_PLAYING_ANIM(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc")
									 AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc") > 0.585 )
									 
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_BULLET_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_PLAYER_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[2].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
									
									GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_COMBAT)
									esVanEnemies[2].iProgress = 0
								ENDIF
							ENDIF
							
							IF ( bPlayerDetected = FALSE )
								IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsEnemyVans[0].VehicleIndex, PLAYER_PED_ID())
										OR IS_PED_SHOOTING_AT_AREA(PLAYER_PED_ID(), TRUE, GET_ENTITY_COORDS(vsEnemyVans[0].VehicleIndex), 4.0 #IF IS_DEBUG_BUILD, TRUE #ENDIF)
											bPlayerDetected = TRUE
											#IF IS_DEBUG_BUILD
												PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player shot at enemy van 0 during scripted entry state. Setting bPlayerDetected to TRUE.")
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE ENEMY_STATE_STANDING_AT_LOCATION
						
							IF ( esVanEnemies[2].bHasTask = FALSE )
								SEQUENCE_INDEX SequenceIndex
								OPEN_SEQUENCE_TASK(SequenceIndex)
									TASK_AIM_GUN_AT_COORD(NULL, << 3264.06, -4588.00, 116.88 >>, 5000, FALSE)
									TASK_AIM_GUN_AT_COORD(NULL, << 3251.02, -4590.45, 117.24 >>, 5000, FALSE)
									TASK_AIM_GUN_AT_COORD(NULL, << 3264.06, -4588.00, 116.88 >>, 5000, FALSE)
									TASK_AIM_GUN_AT_COORD(NULL, << 3272.29, -4590.30, 117.61 >>, 5000, FALSE)
									SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
								CLOSE_SEQUENCE_TASK(SequenceIndex)
								TASK_PERFORM_SEQUENCE(esVanEnemies[2].PedIndex, SequenceIndex)
								CLEAR_SEQUENCE_TASK(SequenceIndex)
								esVanEnemies[2].iTimer = GET_GAME_TIMER()
								esVanEnemies[2].bHasTask = TRUE
							ENDIF
							
							IF HAS_TIME_PASSED(10000, esVanEnemies[2].iTimer)
								bStartSearching = TRUE
							ENDIF
							
							IF IS_PED_IN_COMBAT(esVanEnemies[2].PedIndex)
							OR ( bPlayerDetected = TRUE )
								GO_TO_ENEMY_STATE(esVanEnemies[2], ENEMY_STATE_COMBAT)
								esVanEnemies[2].iProgress = 0
							ENDIF
						
						BREAK
					
						CASE ENEMY_STATE_COMBAT
						
							IF ( esVanEnemies[2].bHasTask = FALSE )
							
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVanEnemies[2].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esVanEnemies[2].PedIndex, CM_WILLADVANCE, CAL_AVERAGE, CR_NEAR, CA_AGGRESSIVE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[2].PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[2].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[2].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								REMOVE_PED_DEFENSIVE_AREA(esVanEnemies[2].PedIndex)
								SET_PED_COVER_PROPERTIES(esVanEnemies[2].PedIndex, TRUE)
								SET_PED_SHOOTING_PROPERTIES(esVanEnemies[2].PedIndex, 3)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[2].PedIndex, FALSE)
								SET_PED_VISUAL_FIELD_PROPERTIES(esVanEnemies[2].PedIndex)	//restore ped's default seeing values
								bPlayerDetected = TRUE

								esVanEnemies[2].bHasTask = TRUE
								
							ENDIF

						BREAK
					
					ENDSWITCH
					
				ELSE
				
					IF 	IS_ENTITY_PLAYING_ANIM(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc") 
					AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc") > 0.51
						STOP_ANIM_PLAYBACK(esVanEnemies[2].PedIndex)
						SET_HIGH_FALL_TASK(esVanEnemies[2].PedIndex, 2000, 10000)
						APPLY_FORCE_TO_ENTITY(esVanEnemies[2].PedIndex, APPLY_TYPE_IMPULSE, << 0.0, 5.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
					ENDIF
				
					IF ( bPlayerDetected = FALSE )
						IF ( esVanEnemies[2].eState = ENEMY_STATE_SCRIPTED_ENTRY ) 
							IF DOES_ENTITY_EXIST(GET_PED_SOURCE_OF_DEATH(esVanEnemies[2].PedIndex))
								IF ( GET_PED_INDEX_FROM_ENTITY_INDEX(GET_PED_SOURCE_OF_DEATH(esVanEnemies[2].PedIndex)) = PLAYER_PED_ID() )
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player detected for killing ", esVanEnemies[2].sDebugName, " in scripted entry state.")
									#ENDIF
									bPlayerDetected = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[2].PedIndex, "missmic1ig_2", "jump_out_pedc")
						CLEANUP_ENEMY_PED(esVanEnemies[2])
					ENDIF
					
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(esVanEnemies[3].PedIndex)
				IF NOT IS_ENTITY_DEAD(esVanEnemies[3].PedIndex)
				
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(esVanEnemies[3].PedIndex, GET_ENEMY_STATE_NAME_FOR_DEBUG(esVanEnemies[3].eState), 1.25)
						esVanEnemies[3].sDebugName = "VanEnemy 3"
					#ENDIF
				
					SWITCH esVanEnemies[3].eState
					
						CASE ENEMY_STATE_SCRIPTED_ENTRY
						
							SWITCH esVanEnemies[3].iProgress
						
								CASE 0
								
									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd")
										IF HAS_ANIM_EVENT_FIRED(esVanEnemies[3].PedIndex, GET_HASH_KEY("door"))
											IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
												SET_VEHICLE_DOOR_OPEN(vsEnemyVans[0].VehicleIndex, SC_DOOR_REAR_RIGHT)
												bPlayVanOpenConversation = TRUE
												esVanEnemies[3].iProgress++
											ENDIF
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 1
									
									IF IS_ENTITY_PLAYING_ANIM(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd")
										IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd") > 0.95 )
											esVanEnemies[3].bHasTask = FALSE
											esVanEnemies[3].iProgress++
										ELSE
											IF ( GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd") > 0.53 )
											
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_BULLET_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_PLAYER_IMPACT)
												CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
												
												IF 	( GET_DISTANCE_BETWEEN_PEDS(esVanEnemies[3].PedIndex, PLAYER_PED_ID()) < 5.0 )
												AND CAN_PED_SEE_HATED_PED(esVanEnemies[3].PedIndex, PLAYER_PED_ID())
													GO_TO_ENEMY_STATE(esVanEnemies[3], ENEMY_STATE_COMBAT)
													esVanEnemies[3].iProgress = 0
												ENDIF
												IF IS_ENTITY_TOUCHING_ENTITY(esVanEnemies[3].PedIndex, PLAYER_PED_ID())
												OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[3].PedIndex, PLAYER_PED_ID())
													CLEAR_ENTITY_LAST_DAMAGE_ENTITY(esVanEnemies[3].PedIndex)
													GO_TO_ENEMY_STATE(esVanEnemies[3], ENEMY_STATE_COMBAT)
													bPlayerDetected = TRUE	//set the player detected flag to true here for other peds to react
													esVanEnemies[3].iProgress = 0
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								BREAK
								
								CASE 2
								
									IF ( esVanEnemies[3].bHasTask = FALSE )
										TASK_PUT_PED_DIRECTLY_INTO_COVER(esVanEnemies[3].PedIndex, csCoverpoints[27].vPosition, -1, FALSE, 0, TRUE, FALSE, csCoverpoints[27].CoverpointIndex)							
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[3].PedIndex, FALSE)
										esVanEnemies[3].iTimer = GET_GAME_TIMER()
										esVanEnemies[3].bHasTask = TRUE
									ENDIF
																
								BREAK

							ENDSWITCH
							
							IF IS_PED_IN_COMBAT(esVanEnemies[3].PedIndex)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(esVanEnemies[3].PedIndex, PLAYER_PED_ID())
							OR ( bPlayerDetected = TRUE )
								IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd")
								OR ( IS_ENTITY_PLAYING_ANIM(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd") 
									 AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd") > 0.53 )
									 
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_BULLET_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_PLAYER_IMPACT)
									CLEAR_RAGDOLL_BLOCKING_FLAGS(esVanEnemies[3].PedIndex, RBF_ALLOW_BLOCK_DEAD_PED)
									
									GO_TO_ENEMY_STATE(esVanEnemies[3], ENEMY_STATE_COMBAT)
									esVanEnemies[3].iProgress = 0
								ENDIF
							ENDIF
							
							IF ( bPlayerDetected = FALSE )
								IF DOES_ENTITY_EXIST(vsEnemyVans[0].VehicleIndex)
									IF NOT IS_ENTITY_DEAD(vsEnemyVans[0].VehicleIndex)
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vsEnemyVans[0].VehicleIndex, PLAYER_PED_ID())
										OR IS_PED_SHOOTING_AT_AREA(PLAYER_PED_ID(), TRUE, GET_ENTITY_COORDS(vsEnemyVans[0].VehicleIndex), 4.0 #IF IS_DEBUG_BUILD, TRUE #ENDIF)
											bPlayerDetected = TRUE
											#IF IS_DEBUG_BUILD
												PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player shot at enemy van 0 during scripted entry state. Setting bPlayerDetected to TRUE.")
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE ENEMY_STATE_COMBAT
						
							IF ( esVanEnemies[3].bHasTask = FALSE )
									
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVanEnemies[3].PedIndex, 100.0)
								SET_PED_COMBAT_PROPERTIES(esVanEnemies[3].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_USE_COVER)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[3].PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[3].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esVanEnemies[3].PedIndex, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_DEFENSIVE_AREA_SPHERE(esVanEnemies[3].PedIndex, << 3259.12, -4608.15, 115.51 >>, 2.5)
								SET_PED_COVER_PROPERTIES(esVanEnemies[3].PedIndex, TRUE, FALSE, TRUE, FALSE, 5.0, FALSE, 0.0, 2.0)
								//SET_PED_PREFERRED_COVERPOINT(esVanEnemies[3].PedIndex, esVanEnemies[3].ItemsetIndex, csCoverpoints[27].CoverpointIndex)
								SET_PED_SHOOTING_PROPERTIES(esVanEnemies[3].PedIndex, 3)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVanEnemies[3].PedIndex, FALSE)
								SET_PED_VISUAL_FIELD_PROPERTIES(esVanEnemies[3].PedIndex)	//restore ped's default seeing values
								bPlayerDetected = TRUE
							
								esVanEnemies[3].bHasTask = TRUE
							
							ENDIF
						
						BREAK
					
					ENDSWITCH
				
				ELSE
					
					IF 	IS_ENTITY_PLAYING_ANIM(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd") 
					AND GET_ENTITY_ANIM_CURRENT_TIME(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd") > 0.51
						STOP_ANIM_PLAYBACK(esVanEnemies[3].PedIndex)
						SET_HIGH_FALL_TASK(esVanEnemies[3].PedIndex, 2000, 10000)
						APPLY_FORCE_TO_ENTITY(esVanEnemies[3].PedIndex, APPLY_TYPE_IMPULSE, << 0.0, 5.0, 0.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
					ENDIF
				
					IF ( bPlayerDetected = FALSE )
						IF ( esVanEnemies[3].eState = ENEMY_STATE_SCRIPTED_ENTRY )
							IF DOES_ENTITY_EXIST(GET_PED_SOURCE_OF_DEATH(esVanEnemies[3].PedIndex))
								IF ( GET_PED_INDEX_FROM_ENTITY_INDEX(GET_PED_SOURCE_OF_DEATH(esVanEnemies[3].PedIndex)) = PLAYER_PED_ID() )
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player detected for killing ", esVanEnemies[3].sDebugName, " in scripted entry state.")
									#ENDIF
									bPlayerDetected = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_PLAYING_ANIM(esVanEnemies[3].PedIndex, "missmic1ig_2", "jump_out_pedd")
						CLEANUP_ENEMY_PED(esVanEnemies[3])
					ENDIF
				
				ENDIF
				
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
	//CLEANUP_ENEMY_GROUP(esVanEnemies)

	UPDATE_VEHICLE_DAMAGE_FROM_PED(vsEnemyVans[0].VehicleIndex, PLAYER_PED_ID(), 300, 400, vsEnemyVans[0].bSmoking,
								   <<10.0, 10.0, 3.0>>, <<0.0, 0.0, 5.0>>, <<0.0, -1.5, 0.0>>, TRUE, 45.0, 70.0)
	CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(vsEnemyVans[0], PLAYER_PED_ID(), <<3238.346680,-4687.663086,114.673004>>,<<36.0,15.0,4.0>>)

	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[0].VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT, vFrontRightOffset, 0.8, FALSE, TRUE, vFrontRightDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[0].VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT, vFrontLeftOffset, 0.8, FALSE, TRUE, vFrontLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[0].VehicleIndex, SC_WHEEL_CAR_REAR_LEFT, vRearLeftOffset, 0.8, FALSE, TRUE, vRearLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[0].VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT, vRearRightOffset, 0.8, FALSE, TRUE, vRearRightDamageOffset, 150.0)
	
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esVanEnemies)
	
ENDPROC

PROC MANAGE_SECOND_ENEMY_VAN_AND_ENEMIES(INT &iProgress)

	FLOAT fPlaybackPercentage
	
	SWITCH iProgress
	
		CASE 0
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 3266.92, -4603.24, 116.83 >>, << 30.0, 3.5, 3.0 >>)
			OR ( GET_NUMBER_OF_ENEMIES_DEAD(esMiddleEnemies) + GET_NUMBER_OF_ENEMIES_DEAD(esVanEnemies) >= 5 )					
			
				IF 	HAS_MODEL_LOADED(mnEnemy)
				AND HAS_MODEL_LOADED(mnEnemyVan)
				
					//second van on recording
					vsEnemyVans[1].VehicleIndex = CREATE_ENEMY_VEHICLE(vsEnemyVans[1], 600, FALSE, TRUE, TRUE, FALSE, VEHICLELOCK_UNLOCKED, 56, 1, FALSE #IF IS_DEBUG_BUILD, "Van 1" #ENDIF)

					SET_ENTITY_INVINCIBLE(vsEnemyVans[1].VehicleIndex, TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vsEnemyVans[1].VehicleIndex, TRUE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vsEnemyVans[1].VehicleIndex, FALSE)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsEnemyVans[1].VehicleIndex, TRUE)

					esSideVanEnemies[0].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[1].VehicleIndex, VS_DRIVER, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
					esSideVanEnemies[1].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[1].VehicleIndex, VS_FRONT_RIGHT, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
					INITIALISE_ENEMY_GROUP(esSideVanEnemies, FALSE #IF IS_DEBUG_BUILD, "SideVan " #ENDIF)
				
					//only create backup enemies if player triggered this wave based of kills of the previous wave
					//commenting this backup wave for now
					//IF 	NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 3266.92, -4603.24, 116.83 >>, << 30.0, 3.5, 3.0 >>)
					//AND ( GET_NUMBER_OF_ENEMIES_DEAD(esMiddleEnemies) + GET_NUMBER_OF_ENEMIES_DEAD(esVanEnemies) >= 5 )
					//	esSideVanBackupEnemies[0].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[1].VehicleIndex, VS_BACK_LEFT, rgEnemies, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH)
					//	esSideVanBackupEnemies[1].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[1].VehicleIndex, VS_BACK_RIGHT, rgEnemies, WEAPONTYPE_PUMPSHOTGUN)
					//	INITIALISE_ENEMY_GROUP(esSideVanBackupEnemies, FALSE #IF IS_DEBUG_BUILD, "SideVanBackup " #ENDIF)
					//ENDIF

					START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE(vsEnemyVans[1], sVehicleRecordingsFile, 1.40, FALSE, 1750)

					START_AUDIO_SCENE("MI_1_VAN_ARRIVES_02")
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vsEnemyVans[1].VehicleIndex, "MI_1_VAN_02")

					bRenderBlips 		= FALSE
					bRenderBlipsTimer 	= GET_GAME_TIMER() 

					iProgress++
					
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE 1
		
			IF IS_VEHICLE_RECORDING_PLAYBACK_RUNNING_OR_DRIVER_DEAD(vsEnemyVans[1].VehicleIndex, fPlaybackPercentage, vsEnemyVans[1].bDriverDead)
		
				IF ( fPlaybackPercentage > 57.5)
				OR ( vsEnemyVans[1].bDriverDead = TRUE )
					SET_ENTITY_INVINCIBLE(vsEnemyVans[1].VehicleIndex, FALSE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vsEnemyVans[1].VehicleIndex, FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vsEnemyVans[1].VehicleIndex, TRUE)
				ENDIF
		
		
				IF ( fPlaybackPercentage > 96.0 )
				OR ( vsEnemyVans[1].bDriverDead = TRUE )
				
					IF DOES_BLIP_EXIST(vsEnemyVans[1].BlipIndex)
						REMOVE_BLIP(vsEnemyVans[1].BlipIndex)
					ENDIF
				
				ENDIF
				
				IF ( fPlaybackPercentage > 96.0 )
				OR ( vsEnemyVans[1].bDriverDead = TRUE )
				
					IF NOT IS_PED_INJURED(esSideVanEnemies[0].PedIndex)
						SET_PED_DEFENSIVE_AREA_ANGLED(esSideVanEnemies[0].PedIndex, vGraveyardDefAreaPosisiton1, vGraveyardDefAreaPosistion2, fGraveyardDefAreaWidth)
						SET_PED_COMBAT_PROPERTIES(esSideVanEnemies[0].PedIndex, CM_WILLADVANCE, CAL_AVERAGE, CR_MEDIUM, CA_USE_COVER)
						SET_PED_COMBAT_ATTRIBUTES(esSideVanEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esSideVanEnemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_SHOOTING_PROPERTIES(esSideVanEnemies[0].PedIndex, 4)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideVanEnemies[0].PedIndex, 100.0)
					ENDIF
					
					IF NOT IS_PED_INJURED(esSideVanEnemies[1].PedIndex)
						SET_PED_DEFENSIVE_AREA_ANGLED(esSideVanEnemies[1].PedIndex, vGraveyardDefAreaPosisiton1, vGraveyardDefAreaPosistion2, fGraveyardDefAreaWidth)
						SET_PED_COMBAT_PROPERTIES(esSideVanEnemies[1].PedIndex, CM_WILLADVANCE, CAL_AVERAGE, CR_MEDIUM, CA_USE_COVER)
						SET_PED_COMBAT_ATTRIBUTES(esSideVanEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(esSideVanEnemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						SET_PED_SHOOTING_PROPERTIES(esSideVanEnemies[1].PedIndex, 3)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideVanEnemies[1].PedIndex, 100.0)
					ENDIF
				
				ENDIF
				
				IF ( vsEnemyVans[1].bDriverDead = TRUE )
					vsEnemyVans[1].bDriverDead = FALSE
				ENDIF
			
			ELSE
			
				iProgress++
								
			ENDIF
		
		BREAK
		
		CASE 2	//enemies are out of the van, keep updating their combat tasks here
		
			IF IS_AUDIO_SCENE_ACTIVE("MI_1_VAN_ARRIVES_02")
				STOP_AUDIO_SCENE("MI_1_VAN_ARRIVES_02")
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vsEnemyVans[1].VehicleIndex)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vsEnemyVans[1].VehicleIndex)
			ENDIF
		
			IF NOT IS_PED_INJURED(esSideVanEnemies[0].PedIndex)
		
				IF ( esSideVanEnemies[0].bFallingBack = FALSE )
					IF ( bPlayerUsedAlternativePath = TRUE )
						esSideVanEnemies[0].bHasTask 	= FALSE
						esSideVanEnemies[0].iProgress 	= 100
						esSideVanEnemies[0].bFallingBack = TRUE
					ENDIF
				ENDIF
				
				SWITCH esSideVanEnemies[0].iProgress
				
					CASE 100
					
						IF ( esSideVanEnemies[0].bHasTask = FALSE )
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideVanEnemies[0].PedIndex, 100.0)
							SET_PED_COMBAT_PROPERTIES(esSideVanEnemies[0].PedIndex, CM_DEFENSIVE, CAL_AVERAGE, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
							SET_PED_DEFENSIVE_AREA_ANGLED(esSideVanEnemies[0].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
							SET_PED_COMBAT_MOVEMENT_VALUES(esSideVanEnemies[0].PedIndex, 0.6, 0.25)
							SET_PED_COVER_PROPERTIES(esSideVanEnemies[0].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
							esSideVanEnemies[0].bHasTask = TRUE
						ENDIF
					
					BREAK
				
				ENDSWITCH
				
			ENDIF
			
			IF NOT IS_PED_INJURED(esSideVanEnemies[1].PedIndex)
		
				IF ( esSideVanEnemies[1].bFallingBack = FALSE )
					IF ( bPlayerUsedAlternativePath = TRUE )
						esSideVanEnemies[1].bHasTask 	= FALSE
						esSideVanEnemies[1].iProgress 	= 100
						esSideVanEnemies[1].bFallingBack = TRUE
					ENDIF
				ENDIF
				
				SWITCH esSideVanEnemies[1].iProgress
				
					CASE 100
					
						IF ( esSideVanEnemies[1].bHasTask = FALSE )
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideVanEnemies[1].PedIndex, 100.0)
							SET_PED_COMBAT_PROPERTIES(esSideVanEnemies[1].PedIndex, CM_DEFENSIVE, CAL_AVERAGE, CR_MEDIUM, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
							SET_PED_DEFENSIVE_AREA_ANGLED(esSideVanEnemies[1].PedIndex, <<3266.473145,-4689.624023,111.113983>>, <<3226.107422,-4686.157715,118.637154>>, 22.0)
							SET_PED_COMBAT_MOVEMENT_VALUES(esSideVanEnemies[1].PedIndex, 0.75, 0.0)
							SET_PED_COVER_PROPERTIES(esSideVanEnemies[1].PedIndex, TRUE, FALSE, FALSE, FALSE, 6.0, TRUE, 0.1, 2.0)
							esSideVanEnemies[1].bHasTask = TRUE
						ENDIF
					
					BREAK
				
				ENDSWITCH
				
			ENDIF
			
			IF DOES_ENTITY_EXIST(esSideVanBackupEnemies[0].PedIndex)
				IF NOT IS_ENTITY_DEAD(esSideVanBackupEnemies[0].PedIndex)
					
					SWITCH esSideVanBackupEnemies[0].iProgress
					
						CASE 0
							IF ( esSideVanBackupEnemies[0].bHasTask = FALSE )
							
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[0].PedIndex, CA_AGGRESSIVE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[0].PedIndex, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[0].PedIndex, CA_LEAVE_VEHICLES, TRUE)
								
								SET_PED_COMBAT_RANGE(esSideVanBackupEnemies[0].PedIndex, CR_FAR)
								SET_PED_COMBAT_MOVEMENT(esSideVanBackupEnemies[0].PedIndex, CM_WILLADVANCE)
								
								SET_PED_SHOOTING_PROPERTIES(esSideVanBackupEnemies[0].PedIndex, 4)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideVanBackupEnemies[0].PedIndex, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esSideVanBackupEnemies[0].PedIndex, FALSE)
								
								esSideVanBackupEnemies[0].bHasTask = TRUE
							ENDIF
						BREAK
					
					ENDSWITCH
					
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(esSideVanBackupEnemies[1].PedIndex)
				IF NOT IS_ENTITY_DEAD(esSideVanBackupEnemies[1].PedIndex)
					
					SWITCH esSideVanBackupEnemies[1].iProgress
					
						CASE 0
							IF ( esSideVanBackupEnemies[1].bHasTask = FALSE )
							
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[1].PedIndex, CA_AGGRESSIVE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[1].PedIndex, CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(esSideVanBackupEnemies[1].PedIndex, CA_LEAVE_VEHICLES, TRUE)
								
								SET_PED_COMBAT_RANGE(esSideVanBackupEnemies[1].PedIndex, CR_MEDIUM)
								SET_PED_COMBAT_MOVEMENT(esSideVanBackupEnemies[1].PedIndex, CM_WILLADVANCE)
								
								SET_PED_SHOOTING_PROPERTIES(esSideVanBackupEnemies[1].PedIndex, 4)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(esSideVanBackupEnemies[1].PedIndex, 200.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esSideVanBackupEnemies[1].PedIndex, FALSE)
								
								esSideVanBackupEnemies[1].bHasTask = TRUE
							ENDIF
						BREAK
					
					ENDSWITCH
					
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
	CLEANUP_ENEMY_GROUP(esSideVanEnemies)
	CLEANUP_ENEMY_GROUP(esSideVanBackupEnemies)
	
	UPDATE_VEHICLE_DAMAGE_FROM_PED(vsEnemyVans[1].VehicleIndex, PLAYER_PED_ID(), 300, 450, vsEnemyVans[1].bSmoking,
								   <<10.0, 10.0, 3.0>>, <<0.0, 0.0, 5.0>>, <<0.0, -1.5, 0.0>>)
	CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(vsEnemyVans[1], PLAYER_PED_ID(), <<3238.346680,-4687.663086,114.673004>>,<<36.0,15.0,4.0>>)
	
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[1].VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT, vFrontRightOffset, 0.8, FALSE, TRUE, vFrontRightDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[1].VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT, vFrontLeftOffset, 0.8, FALSE, TRUE, vFrontLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[1].VehicleIndex, SC_WHEEL_CAR_REAR_LEFT, vRearLeftOffset, 0.8, FALSE, TRUE, vRearLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[1].VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT, vRearRightOffset, 0.8, FALSE, TRUE, vRearRightDamageOffset, 150.0)

	IF ( bRenderBlips = FALSE )
		IF ( bRenderBlipsTimer <> 0 )
			IF HAS_TIME_PASSED(2000, bRenderBlipsTimer)
				bRenderBlips = TRUE
			ENDIF
		ENDIF
	ENDIF

	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esSideVanEnemies, bRenderBlips)
	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esSideVanBackupEnemies, bRenderBlips)

ENDPROC

PROC MANAGE_THIRD_ENEMY_VAN_AND_ENEMIES(INT &iProgress)

	SWITCH iProgress
	
		CASE 0
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3238.346680,-4687.663086,114.673004>>, <<36.0,15.0,4.0>>)
			
				iProgress++
			
			ENDIF
		
		BREAK
	
		CASE 1
		
			REQUEST_MODEL(mnEnemyVan)
			REQUEST_MODEL(mnEnemy)
			REQUEST_VEHICLE_RECORDING(vsEnemyVans[4].iRecordingNumber, sVehicleRecordingsFile)
			
			IF 	HAS_MODEL_LOADED(mnEnemyVan)
			AND HAS_MODEL_LOADED(mnEnemy)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(vsEnemyVans[4].iRecordingNumber, sVehicleRecordingsFile)
			
				vsEnemyVans[4].VehicleIndex = CREATE_ENEMY_VEHICLE(vsEnemyVans[4], 950, FALSE, TRUE, TRUE, FALSE, VEHICLELOCK_UNLOCKED, -1, -1, FALSE #IF IS_DEBUG_BUILD, "EnemyVan 4" #ENDIF)
				
				esVan4Enemies[0].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[4].VehicleIndex, VS_DRIVER, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esVan4Enemies[1].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[4].VehicleIndex, VS_FRONT_RIGHT, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esVan4Enemies[2].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[4].VehicleIndex, VS_BACK_LEFT, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				esVan4Enemies[3].PedIndex = CREATE_ENEMY_PED_IN_VEHICLE(mnEnemy, vsEnemyVans[4].VehicleIndex, VS_BACK_RIGHT, rgEnemies, WEAPONTYPE_ASSAULTRIFLE)
				INITIALISE_ENEMY_GROUP(esVan4Enemies, FALSE #IF IS_DEBUG_BUILD, "Van4 " #ENDIF)

				START_VEHICLE_RECORDING_PLAYBACK_FOR_VEHICLE(vsEnemyVans[4], sVehicleRecordingsFile, 1.2, FALSE, 2750)

				START_AUDIO_SCENE("MI_1_VAN_ARRIVES_03")
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vsEnemyVans[4].VehicleIndex, "MI_1_VAN_03")
				SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vsEnemyVans[4].VehicleIndex, TRUE)
			
				iProgress++

			ENDIF
		
		BREAK
		
		CASE 2
		
			IF ( GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(vsEnemyVans[4].VehicleIndex) > 98.0 )

				IF DOES_BLIP_EXIST(vsEnemyVans[4].BlipIndex)
					REMOVE_BLIP(vsEnemyVans[4].BlipIndex)
				ENDIF
				
				IF NOT IS_PED_INJURED(esVan4Enemies[0].PedIndex)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVan4Enemies[0].PedIndex, 150.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVan4Enemies[0].PedIndex, FALSE)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[0].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
					SET_PED_DEFENSIVE_AREA_SPHERE(esVan4Enemies[0].PedIndex, << 3214.98, -4716.22, 111.82 >>, 2.0)
					SET_PED_SHOOTING_PROPERTIES(esVan4Enemies[0].PedIndex, 3, 100, FIRING_PATTERN_FULL_AUTO)
					SET_PED_COVER_PROPERTIES(esVan4Enemies[0].PedIndex, TRUE, FALSE, FALSE, FALSE)
					SET_PED_PREFERRED_COVERPOINT(esVan4Enemies[0].PedIndex, esVan4Enemies[0].ItemsetIndex, csCoverpoints[29].CoverpointIndex)
				ENDIF
				
				IF NOT IS_PED_INJURED(esVan4Enemies[1].PedIndex)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVan4Enemies[1].PedIndex, 150.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVan4Enemies[1].PedIndex, FALSE)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[1].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
					SET_PED_DEFENSIVE_AREA_SPHERE(esVan4Enemies[1].PedIndex, << 3224.99, -4716.13, 111.41 >>, 1.5)
					SET_PED_SHOOTING_PROPERTIES(esVan4Enemies[1].PedIndex, 3, 100, FIRING_PATTERN_FULL_AUTO)
				ENDIF
				
				IF NOT IS_PED_INJURED(esVan4Enemies[2].PedIndex)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVan4Enemies[2].PedIndex, 150.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVan4Enemies[2].PedIndex, FALSE)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[2].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
					SET_PED_DEFENSIVE_AREA_SPHERE(esVan4Enemies[2].PedIndex, << 3218.49, -4716.52, 111.37 >>, 2.0)
					SET_PED_SHOOTING_PROPERTIES(esVan4Enemies[2].PedIndex, 4, 100, FIRING_PATTERN_FULL_AUTO)
				ENDIF
				
				IF NOT IS_PED_INJURED(esVan4Enemies[3].PedIndex)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(esVan4Enemies[3].PedIndex, 150.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(esVan4Enemies[3].PedIndex, FALSE)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[3].PedIndex, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_FAR, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH)
					SET_PED_DEFENSIVE_AREA_SPHERE(esVan4Enemies[3].PedIndex, << 3224.70, -4718.32, 111.37 >>, 1.5)
					SET_PED_SHOOTING_PROPERTIES(esVan4Enemies[3].PedIndex, 4, 100, FIRING_PATTERN_FULL_AUTO)
				ENDIF
			
				bEnemiesBlockedByTrain = TRUE

				vsEnemyVans[4].iTimer = GET_GAME_TIMER()
				
				STOP_AUDIO_SCENE("MI_1_VAN_ARRIVES_03")
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vsEnemyVans[4].VehicleIndex)
			
				iProgress++
			
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF HAS_TIME_PASSED(34000, vsEnemyVans[4].iTimer) //40000
			
				IF NOT IS_PED_INJURED(esVan4Enemies[0].PedIndex)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[0].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, CA_AGGRESSIVE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[0].PedIndex, CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[0].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
					REMOVE_PED_DEFENSIVE_AREA(esVan4Enemies[0].PedIndex)
				ENDIF
				IF NOT IS_PED_INJURED(esVan4Enemies[1].PedIndex)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[1].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, CA_AGGRESSIVE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[1].PedIndex, CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[1].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
					REMOVE_PED_DEFENSIVE_AREA(esVan4Enemies[1].PedIndex)
				ENDIF
				IF NOT IS_PED_INJURED(esVan4Enemies[2].PedIndex)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[2].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, CA_AGGRESSIVE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[2].PedIndex, CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[2].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
					REMOVE_PED_DEFENSIVE_AREA(esVan4Enemies[2].PedIndex)
				ENDIF
				IF NOT IS_PED_INJURED(esVan4Enemies[3].PedIndex)
					SET_PED_COMBAT_PROPERTIES(esVan4Enemies[3].PedIndex, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, CA_AGGRESSIVE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[3].PedIndex, CA_CAN_CHARGE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(esVan4Enemies[3].PedIndex, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
					REMOVE_PED_DEFENSIVE_AREA(esVan4Enemies[3].PedIndex)
				ENDIF
			
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Moving enemies from third van.")
				#ENDIF
			
				iProgress++
			
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
	CLEANUP_ENEMY_GROUP(esVan4Enemies)
	
	UPDATE_VEHICLE_DAMAGE_FROM_PED(vsEnemyVans[4].VehicleIndex, PLAYER_PED_ID(), 300, 500, vsEnemyVans[4].bSmoking,
								   <<10.0, 10.0, 3.0>>, <<0.0, 0.0, 5.0>>, <<0.0, -1.5, 0.0>>)
								   
	CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(vsEnemyVans[4], PLAYER_PED_ID(), <<3222.843506,-4688.635742,114.212746>>, << 12.0, 16.0, 3.0 >>)
	CHECK_VEHICLE_FOR_DRIVING_INTO_LOCATE(vsEnemyVans[4], PLAYER_PED_ID(), <<3217.266113,-4748.119629,112.924225>>, << 16.0, 16.0, 3.0 >>)
	
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[4].VehicleIndex, SC_WHEEL_CAR_FRONT_RIGHT, vFrontRightOffset, 0.8, FALSE, TRUE, vFrontRightDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[4].VehicleIndex, SC_WHEEL_CAR_FRONT_LEFT, vFrontLeftOffset, 0.8, FALSE, TRUE, vFrontLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[4].VehicleIndex, SC_WHEEL_CAR_REAR_LEFT, vRearLeftOffset, 0.8, FALSE, TRUE, vRearLeftDamageOffset, 150.0)
	RUN_VEHICLE_TYRE_BURST_CHECK(vsEnemyVans[4].VehicleIndex, SC_WHEEL_CAR_REAR_RIGHT, vRearRightOffset, 0.8, FALSE, TRUE, vRearRightDamageOffset, 150.0)

	UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(esVan4Enemies)

ENDPROC

PROC CHECK_PLAYER_DETECTED_LOCATION(BOOL bPlayerSpotted, DETECTED_LOCATIONS &eLocation)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF ( eLocation = DL_NOT_DETECTED )
		
			IF ( bPlayerSpotted = TRUE )

				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3267.363770,-4574.668457,119.156708>>,<<36.000000,16.000000,4.000000>>)
				
					eLocation = DL_START_AREA
				
				ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3266.107422,-4602.592773,117.675850>>,<<36.000000,13.000000,4.000000>>)
				
					eLocation = DL_MIDDLE_AREA_01
				
				ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3264.762207,-4626.325195,117.186195>>,<<36.000000,11.000000,4.000000>>)
				
					eLocation = DL_MIDDLE_AREA_02
				
				ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3262.034668,-4655.088379,114.936089>>,<<36.000000,17.000000,4.000000>>)
				
					eLocation = DL_CHURCH_AREA
				
				ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3243.011963,-4691.706055,113.654236>>,<<50.000000,20.000000,4.000000>>)
				
					eLocation = DL_PARKING_LOT
				
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Player detected in location ", GET_DETECTED_LOCATION_NAME_FOR_DEBUG(eLocation), ".")
				#ENDIF
			
			ENDIF

		ENDIF
		
	ENDIF
	
ENDPROC

PROC TRIGGER_MUSIC_EVENTS()

	IF ( bMusicEventFirstTwoDeadTriggered = FALSE )
		IF ( bMusicEventAlertedTriggered = FALSE )
			IF ( GET_NUMBER_OF_ENEMIES_DEAD(esWallEnemies) = 2 )
				IF TRIGGER_MUSIC_EVENT("MIC1_FIRST_TWO_DEAD")
					bMusicEventFirstTwoDeadTriggered = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ( bMusicEventFirstVanTriggered = FALSE )
		IF ( bOpenGateTriggered = TRUE )
			IF PREPARE_MUSIC_EVENT("MIC1_1ST_VAN")
				IF TRIGGER_MUSIC_EVENT("MIC1_1ST_VAN")
					bMusicEventFirstVanTriggered = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ( bMusicEventAlertedTriggered = FALSE )
	
		IF ( bPlayerDetected = TRUE )
			IF HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT1")
			OR HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT2")
			OR HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT3")
			OR HAS_LABEL_BEEN_TRIGGERED("MCH1_SPOT4")
			OR HAS_LABEL_BEEN_TRIGGERED("MCH1_SURROUND")
				IF PREPARE_MUSIC_EVENT("MIC1_ALERTED")
					IF TRIGGER_MUSIC_EVENT("MIC1_ALERTED")
						STOP_AUDIO_SCENE("MI_1_SHOOTOUT_STEALTH")
						START_AUDIO_SCENE("MI_1_SHOOTOUT_ENEMIES_ALERTED")
						bMusicEventAlertedTriggered = TRUE
					ENDIF
				ENDIF				
			ENDIF
		ENDIF
	
	ENDIF
	
	IF ( bMusicEventTrainTriggered = FALSE )
		IF ( bEnemiesBlockedByTrain = TRUE )
			IF TRIGGER_MUSIC_EVENT("MIC1_TRAIN")
				bMusicEventTrainTriggered = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_BRAD_CADAVER(INT &iProgress)

	IF DOES_ENTITY_EXIST(psBrad.PedIndex)
		IF NOT IS_ENTITY_DEAD(psBrad.PedIndex)
		
			SWITCH iProgress

				CASE 0
		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iBradCadaverScene)
			
						REQUEST_ANIM_DICT("dead")
						
						IF HAS_ANIM_DICT_LOADED("dead")
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iBradCadaverScene)
							
								iBradCadaverScene = CREATE_SYNCHRONIZED_SCENE(<< 3258.899, -4574.090, 115.350 >>, << 173.880, 51.480, 5.040 >>, EULER_XYZ)
								
								TASK_SYNCHRONIZED_SCENE(psBrad.PedIndex, iBradCadaverScene, "dead", "dead_g", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(psBrad.PedIndex)
							
								STOP_PED_SPEAKING(psBrad.PedIndex, TRUE)
								SET_ENTITY_INVINCIBLE(psBrad.PedIndex, TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(psBrad.PedIndex, TRUE)
							
							ENDIF						
						ENDIF
					
					ELSE
					
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_FIRE)
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_MELEE)
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_EXPLOSION)
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_PLAYER_BUMP)
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_BULLET_IMPACT)
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_PLAYER_IMPACT)
						SET_RAGDOLL_BLOCKING_FLAGS(psBrad.PedIndex, RBF_IMPACT_OBJECT)
						
						iProgress++
					
					ENDIF
					
				BREAK
				
				CASE 1
				
				BREAK
				
			ENDSWITCH
		
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_MISSION_STAGE_GRAVEYARD_SHOOTOUT_COMPLETED(INT &iStageProgress)

	//don't use locates header J skip
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)
	
	SET_CLOCK_TIME(0, 0, 0)
	SET_PICKUP_AMMO_AMOUNT_SCALER(3.0)
	
	//using locates header display player objective and check if player made it to the graveyard entrance
	IF ( iStageProgress < 40 )
		
		IF NOT DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
			vsMichaelsCar.BlipIndex = CREATE_BLIP_FOR_VEHICLE(vsMichaelsCar.VehicleIndex)
			PRINT_GOD_TEXT_ADVANCED("CMN_GENGETIN")
		ENDIF
		
		IF ( bPlayerUsedAlternativePath = FALSE )
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<3276.662598,-4691.776367,112.158768>>, <<3218.352783,-4637.393066,120.542297>>, 16.000000)
				bPlayerUsedAlternativePath = TRUE
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_LOCATE(PLAYER_PED_ID(), <<3238.346680,-4687.663086,114.673004>>,<<36.0,15.0,4.0>> #IF IS_DEBUG_BUILD, "GraveyardEntranceLocate" #ENDIF)
		
			//set the fail flag for abandoning the car
			FailFlags[MISSION_FAIL_MICHAELS_CAR_LEFT_BEHIND] = TRUE
			
			//unset the fail for leaving the graveyard, distance to car check will now take over checking for fails
			FailFlags[MISSION_FAIL_GRAVEYARD_ABANDONED] = FALSE

			iStageProgress = 40
		
		ENDIF

	ENDIF

	RUN_TOMBSTONES_FRAG_CHECK(PLAYER_PED_ID(), TombstoneShooterPed, iEnemyTombstoneShootTimer, 4000, 1.25)
	
	MANAGE_FIRST_ENEMY_WAVE(bFirstEnemyWaveSpawned, TRUE)
	MANAGE_SECOND_ENEMY_WAVE(bSecondEnemyWaveSpawned, bFirstEnemyWaveSpawned, -1)
	MANAGE_THIRD_ENEMY_WAVE(bThirdEnemyWaveSpawned, bSecondEnemyWaveSpawned)
	MANAGE_FOURTH_ENEMY_WAVE(bFourthEnemyWaveSpawned, bThirdEnemyWaveSpawned, -1)
	MANAGE_FIFTH_ENEMY_WAVE(bFifthEnemyWaveSpawned, bFifthEnemyWaveIgnored, TRUE)
	
	MANAGE_FIRST_ENEMY_REINFORCEMENT_WAVE(bFirstEnemyReinforcementWaveSpawned, bThirdEnemyWaveSpawned)
	MANAGE_SECOND_ENEMY_REINFORCEMENT_WAVE(bSecondEnemyReinforcementWaveSpawned, bFirstEnemyReinforcementWaveSpawned, bThirdEnemyWaveSpawned)
	//MANAGE_THIRD_ENEMY_REINFORCEMENT_WAVE(bThirdEnemyReinforcementWaveSpawned, bSecondEnemyReinforcementWaveSpawned, bThirdEnemyWaveSpawned)
	
	MANAGE_FIRST_ENEMY_VAN_AND_ENEMIES(vsEnemyVans[0].iProgress)
	MANAGE_SECOND_ENEMY_VAN_AND_ENEMIES(vsEnemyVans[1].iProgress)
	MANAGE_THIRD_ENEMY_VAN_AND_ENEMIES(vsEnemyVans[4].iProgress)

	TRIGGER_MUSIC_EVENTS()
	MANAGE_CHURCH_BELL_SOUNDS(iChurchBellsProgress)
	MANAGE_PARKED_ENEMY_VEHICLES(vsEnemyVans[2].iProgress)
	CHECK_PLAYER_DETECTED_LOCATION(bPlayerDetected, eDetectedLocation)
	MANAGE_CONVERSATIONS_DURING_SHOOTOUT(psMichael.iConversationProgress, psMichael.iConversationTimer, iEnemyConversationTimer,
										 iMichaelRandomConversationDelay, iEnemyRandomConversationDelay, bShootingConversationFlag) 

	CLEANUP_ENEMY_VEHICLES(vsEnemyVans, FALSE, TRUE, TRUE)

	MANAGE_WEAPONS_HELP_TEXT()
	MANAGE_BRAD_CADAVER(iBradCadaverProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_SHOOTOUT_STEALTH")
				START_AUDIO_SCENE("MI_1_SHOOTOUT_STEALTH")
			ENDIF

			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				//IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
				//	TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),  csCoverpoints[0].vPosition, -1, TRUE, 0, TRUE, TRUE, csCoverpoints[0].CoverpointIndex)
				//	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				//ENDIF

				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)

			ENDIF
			
			IF ( bGameplayCameraSet = FALSE  )
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-16.536800)
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(-72.373550)
				bGameplayCameraSet = TRUE
			ENDIF

			FailFlags[MISSION_FAIL_GRAVEYARD_ABANDONED] = TRUE

			SETTIMERB(0)

			psMichael.iTimer = GET_GAME_TIMER()

			iStageProgress++
			
		BREAK
		
		CASE 1

			IF ( TIMERB() > 5000 )

				#IF IS_DEBUG_BUILD
					bStageResetFlag = TRUE
				#ENDIF
				
				iStageProgress++
			
			ENDIF
			
		BREAK
			
		CASE 40
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("MCH1_RTC")
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 3263.05, -4704.67, 104.67 >>, << 3267.14, -4561.37, 132.76 >>, 64.0)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex) > ABANDON_CAR_WARNING_RANGE
						CLEAR_PRINTS()
						PRINT_GOD_TEXT_ADVANCED("MCH1_RTC", DEFAULT_GOD_TEXT_TIME, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//if in the target vehicle only
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, TRUE)
				DISABLE_PLAYER_VEHICLE_DRIVING_CONTROLS_THIS_FRAME()
				IF DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
					REMOVE_BLIP(vsMichaelsCar.BlipIndex)
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
					vsMichaelsCar.BlipIndex = CREATE_BLIP_FOR_VEHICLE(vsMichaelsCar.VehicleIndex)
				ENDIF
			ENDIF
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_GET_TO_CAR")
				START_AUDIO_SCENE("MI_1_GET_TO_CAR")
			ENDIF

			MANAGE_MISSION_TRAIN(tsTrain.iProgress)

			//if player enters the carreaches the car
			IF 	IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex)
				//Speed up the multihead blinders
				SET_MULTIHEAD_SAFE(TRUE)
			
				IF ( bCutsceneTimeTrigger = FALSE )
					IF ( iCutsceneTriggerTimer = 0)
						iCutsceneTriggerTimer = GET_GAME_TIMER()
					ELSE
						IF HAS_TIME_PASSED(1000, iCutsceneTriggerTimer)
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering mic_1_mcs_3 cutscene due to timer trigger.")
							#ENDIF
							bCutsceneTimeTrigger = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF ( bCutsceneProximityTrigger = FALSE )
				
					PED_INDEX ClosestPedIndex
					
					ClosestPedIndex = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), rgEnemies)
					
					IF DOES_ENTITY_EXIST(ClosestPedIndex)
						IF NOT IS_ENTITY_DEAD(ClosestPedIndex)
							IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), ClosestPedIndex) < 5.0
								#IF IS_DEBUG_BUILD
									PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering mic_1_mcs_3 cutscene due to proximity trigger.")
								#ENDIF
								bCutsceneProximityTrigger = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR bCutsceneProximityTrigger = TRUE OR bCutsceneTimeTrigger = TRUE
				
					KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION()	
				
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
						IF DOES_BLIP_EXIST(vsMichaelsCar.BlipIndex)
							REMOVE_BLIP(vsMichaelsCar.BlipIndex)
						ENDIF

						IF ( bCoverpointsCreated = TRUE )
							REMOVE_COVERPOINTS(csCoverpoints)
							bCoverpointsCreated = FALSE
						ENDIF

						STOP_SOUND(iChurchBellsSoundID)
						
						REMOVE_ANIM_DICT("dead")
						REMOVE_ANIM_DICT("missmic1ig_2")
						
						IF IS_AUDIO_SCENE_ACTIVE("MI_1_GET_TO_CAR")
							STOP_AUDIO_SCENE("MI_1_GET_TO_CAR")
						ENDIF
						
						IF IS_AUDIO_SCENE_ACTIVE("MI_1_SHOOTOUT_ENEMIES_ALERTED")
							STOP_AUDIO_SCENE("MI_1_SHOOTOUT_ENEMIES_ALERTED")
						ENDIF
						
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_HEIST_3B_SHOOTOUT")
						
						CLEANUP_MISSION_OBJECT(osPropCoffin, TRUE)
						CLEANUP_MISSION_OBJECT(osPropShovel, TRUE)
						CLEANUP_MISSION_OBJECT(osPropPickaxe, TRUE)
												
						IF DOES_ENTITY_EXIST(psBrad.PedIndex)
							DELETE_PED(psBrad.PedIndex)
						ENDIF
						
						RETURN TRUE
						
					ENDIF
					
				ENDIF
			ENDIF
			
			REQUEST_CUTSCENE("mic_1_mcs_3")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Left_Gun", 		PED_COMP_HAIR, 	1, 0)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Kneeling_Gunman", PED_COMP_HAIR, 	1, 0)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_HEAD, 	0, 0) //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_HAIR, 	0, 0) //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_TORSO, 1, 0) //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_LEG, 	1, 0) //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_HAND, 	1, 0) //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_FEET, 	1, 0) //(feet)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_JBIB, 	1, 0) //(jbib)
				
			ENDIF

		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_CUTSCENE_KIDNAPPED_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0

      		IF HAS_REQUESTED_CUTSCENE_LOADED("mic_1_mcs_3")
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
              		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				
//					IF IS_VEHICLE_DOOR_DAMAGED(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_LEFT)
//						SET_VEHICLE_DOOR_CONTROL(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
//					ENDIF
//					IF IS_VEHICLE_DOOR_DAMAGED(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT)
//						SET_VEHICLE_DOOR_CONTROL(vsMichaelsCar.VehicleIndex, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.0)
//					ENDIF
				
					SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
              		REGISTER_ENTITY_FOR_CUTSCENE(vsMichaelsCar.VehicleIndex, "Chinese_Goon_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
				//clear area when starting the cutscene
				CLEAR_AREA(psMichael.vPosition, 100.0, TRUE)
				CLEAR_AREA_OF_VEHICLES(psMichael.vPosition, 100.0)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)	//make sure cutscene ends with a fade
				
				iStageProgress++
			
			ELSE
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Left_Gun", 		PED_COMP_HAIR, 	1, 0)
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Kneeling_Gunman", PED_COMP_HAIR, 	1, 0)
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_HEAD, 	0, 0) //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_HAIR, 	0, 0) //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_TORSO, 1, 0) //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_LEG, 	1, 0) //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_HAND, 	1, 0) //(hand)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_FEET, 	1, 0) //(feet)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Chinese_Goon", 	PED_COMP_JBIB, 	1, 0) //(jbib)
					
				ENDIF

         	ENDIF
		
		BREAK
		
		CASE 1	//fade screen in if screen was faded out due to shit skip or debug skip
		
			IF IS_CUTSCENE_PLAYING()
			
				CLEANUP_ENEMY_GROUP(esVanEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esMiddleEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esWallEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esChurchEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esSideVanEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esSideVanBackupEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esExitEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esVan4Enemies, TRUE)
				CLEANUP_ENEMY_GROUP(esRunners, TRUE)
				CLEANUP_ENEMY_GROUP(esSideEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esFirstReinforcementEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esSecondReinforcementEnemies, TRUE)
				CLEANUP_ENEMY_GROUP(esThirdReinforcementEnemies, TRUE)
				
				CLEANUP_ENEMY_VEHICLES(vsEnemyVans, TRUE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF ( bMusicEventKidnappedTriggered = FALSE )
				IF PREPARE_MUSIC_EVENT("MIC1_KIDNAPPED")
					IF TRIGGER_MUSIC_EVENT("MIC1_KIDNAPPED")
						bMusicEventKidnappedTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			IF ( bMusicEventReadyToFlyTriggered = FALSE )
				IF IS_CUTSCENE_PLAYING()
					IF ( GET_CUTSCENE_TIME() > 4500 )
						IF TRIGGER_MUSIC_EVENT("MIC1_READY_TO_FLY")
							bMusicEventReadyToFlyTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("asea")
				IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
					SET_VEHICLE_ENGINE_ON(vsMichaelsCar.VehicleIndex, FALSE, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vsMichaelsCar.VehicleIndex)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				IF 	IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vsMichaelsCar.VehicleIndex, VS_DRIVER)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF


			IF HAS_CUTSCENE_FINISHED()
				
				REPLAY_STOP_EVENT()
								
				//start rendering from scripted camera early so that map streams in
				IF DOES_CAM_EXIST(ScriptedCamera)			
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF

				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				iStageProgress++

			ELSE
						
				//create scripted camera in advance
				IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

					SET_CAM_PARAMS(ScriptedCamera, <<3157.128906,7499.776855,5.372416>>,<<11.137081,-0.000000,145.805832>>,59.407524)
					SHAKE_CAM(ScriptedCamera, "HAND_SHAKE", 0.25)
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
					#ENDIF

				ENDIF

			ENDIF
		
		BREAK
		
		CASE 3
		
			IF IS_SCREEN_FADED_OUT()
			
				//delete train when screen is faded out if the train exists				
				IF 	DOES_ENTITY_EXIST(tsTrain.VehicleIndex)
				AND DOES_ENTITY_EXIST(tsTrain.PedIndex)
					DELETE_PED(tsTrain.PedIndex)
					DELETE_MISSION_TRAIN(tsTrain.VehicleIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Deleted mission train.")
					#ENDIF
				ENDIF
				
				IF NOT HAS_SOUND_FINISHED(iTrainBellsSoundID)
					STOP_SOUND(iTrainBellsSoundID)
				ENDIF
				
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")
				
				//warp player ped out of the car
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				ENDIF
				
				//delete player's car
				IF DOES_ENTITY_EXIST(vsMichaelsCar.VehicleIndex)
					IF IS_VEHICLE_DRIVEABLE(vsMichaelsCar.VehicleIndex)
						DELETE_VEHICLE(vsMichaelsCar.VehicleIndex)
					ENDIF
				ENDIF
				
				UNLOCK_GRAVEYARD_GATES()
				UNREGISTER_GRAVEYARD_GATES()
				
				RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID()).")
				#ENDIF
			
				RETURN TRUE
			
			ENDIF
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_CONVERSATIONS_DURING_FLIGHT_HOME(INT &iTrevorProgress)

	SWITCH iTrevorProgress
	
		CASE 0

			IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
				IF ( fDistanceToDestination != 0 AND fDistanceToDestination < TRIGGER_CALL_RANGE )
					psTrevor.iConversationTimer = GET_GAME_TIMER()
					iTrevorProgress++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
		
			IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)	
				IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(sMichael1Conversation, CHAR_CHENGSR, "MCH1AUD",
														   PICK_STRING(bStageReplayInProgress, "MCH1_CP04", "MCH1_CP04b"), CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("MCH1_CP04", TRUE)

					REPLAY_RECORD_BACK_FOR_TIME(1.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)

					iTrevorProgress++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
		
			IF HAS_LABEL_BEEN_TRIGGERED("MCH1_CP04")
		
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					psTrevor.iConversationTimer = GET_GAME_TIMER()
					iTrevorProgress++
				ENDIF
				
				IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Last phone call was interrupted.")
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 3
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				iTrevorProgress++
				
			ENDIF
		
		BREAK
		
		CASE 4
			
			ADD_CONTACT_TO_PHONEBOOK(CHAR_CHENGSR, TREVOR_BOOK, TRUE)
			
			iTrevorProgress++
			
		BREAK
		
		CASE 5
			
			IF ( bTextMessageSent = FALSE )
				IF ( iSendTextMessageTimer = 0 )
					iSendTextMessageTimer = GET_GAME_TIMER()
				ELSE
					IF HAS_TIME_PASSED(16000, iSendTextMessageTimer)
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LAMAR, "MIC1_END", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
						bTextMessageSent 		= TRUE
						iSendTextMessageTimer	= GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC BOOL IS_MISSION_STAGE_FLY_HOME_COMPLETED(INT &iStageProgress)

	//don't use locates header J skip
	SET_BIT(sLocatesData.iLocatesBitSet, BS_DONT_DO_J_SKIP)

	SWITCH iStageProgress
	
		CASE 0
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_1_FLY_PRE_PHONECALL")
				START_AUDIO_SCENE("MI_1_FLY_PRE_PHONECALL")
			ENDIF

			IF NOT DOES_CAM_EXIST(ScriptedCamera)
			
				ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)

				SET_CAM_PARAMS(ScriptedCamera, <<3157.128906,7499.776855,5.372416>> + vGlobalFlightOffset, <<11.137081,-0.000000,145.805832>>,59.407524)
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Created scripted camera.")
				#ENDIF

			ENDIF
			
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
				
				SET_HELI_BLADES_FULL_SPEED(vsTrevorsPlane.VehicleIndex)
				SET_VEHICLE_ENGINE_ON(vsTrevorsPlane.VehicleIndex, TRUE, TRUE)
				
				FREEZE_ENTITY_POSITION(vsTrevorsPlane.VehicleIndex, TRUE)
			ENDIF
			
			bTextMessageSent = FALSE
			
			iStageProgress++			
			
		BREAK

		CASE 1
		
			IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
		
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				
					NEW_LOAD_SCENE_START(GET_CAM_COORD(ScriptedCamera), GET_ENTITY_FORWARD_VECTOR(vsTrevorsPlane.VehicleIndex), 4500.0)
					
					iLoadSceneTimer = GET_GAME_TIMER()
					
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_START().")
					#ENDIF
					
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Load scene timer: ", GET_GAME_TIMER() - iLoadSceneTimer, ".")
				#ENDIF
				
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR HAS_TIME_PASSED(10000, iLoadSceneTimer)

					#IF IS_DEBUG_BUILD
						IF HAS_TIME_PASSED(10000, iLoadSceneTimer)
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to iLoadSceneTimer timeout.")
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling NEW_LOAD_SCENE_STOP() due to IS_NEW_LOAD_SCENE_LOADED() returning TRUE.")
						ENDIF
					#ENDIF
					
					NEW_LOAD_SCENE_STOP()
				
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
				
					iStageProgress++
				
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE 2	//scene visible in camera should be streamed in by now
		
			REQUEST_ANIM_DICT("missmic1")
			
			IF HAS_ANIM_DICT_LOADED("missmic1")

				IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
					
					FREEZE_ENTITY_POSITION(vsTrevorsPlane.VehicleIndex, FALSE)
					
					START_PLAYBACK_RECORDED_VEHICLE(vsTrevorsPlane.VehicleIndex, vsTrevorsPlane.iRecordingNumber, sVehicleRecordingsFile)			
					SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vsTrevorsPlane.VehicleIndex, vGlobalFlightOffset)
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vsTrevorsPlane.VehicleIndex, vLocalPlaneOffset)
					
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vsTrevorsPlane.VehicleIndex, fPlaneRecordingSkipTime)
					SET_PLAYBACK_SPEED(vsTrevorsPlane.VehicleIndex, 0.9)
					
					SET_VEHICLE_ENGINE_ON(vsTrevorsPlane.VehicleIndex, TRUE, TRUE)
					SET_HELI_BLADES_FULL_SPEED(vsTrevorsPlane.VehicleIndex)
					CONTROL_LANDING_GEAR(vsTrevorsPlane.VehicleIndex, LGC_RETRACT_INSTANT)					
					
				ENDIF
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iPlaneSceneID)
				
					iPlaneSceneID = CREATE_SYNCHRONIZED_SCENE(<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
					
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iPlaneSceneID, vsTrevorsPlane.VehicleIndex, 0)
					
					DESTROY_ALL_CAMS()
					
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					
					PLAY_SYNCHRONIZED_CAM_ANIM(ScriptedCamera, iPlaneSceneID, "trevor_plane_cam", "missmic1")
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_FIRST_PERSON
						bCamViewModeContextChanged = TRUE
						SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
					ENDIF
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
				ENDIF
				
				psTrevor.iTimer = GET_GAME_TIMER()

				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)

				iStageProgress++
				
			ENDIF
		
		BREAK
		
		CASE 3
		
			#IF IS_DEBUG_BUILD
				IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsTrevorsPlane.VehicleIndex)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(vsTrevorsPlane.VehicleIndex, GET_STRING_FROM_FLOAT(GET_TIME_POSITION_IN_RECORDING(vsTrevorsPlane.VehicleIndex)), 1.25)
					ENDIF
				ENDIF
			#ENDIF
			
			DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
			DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)

			DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
			
			IF HAS_TIME_PASSED(iCutsceneTime, psTrevor.iTimer)
				
				RENDER_SCRIPT_CAMS(FALSE, TRUE, iInterpTime, FALSE)	//interpolate back to gameplay camera from animated camera

				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				IF bStageReplayInProgress = FALSE
					IF bCamViewModeContextChanged = FALSE
						ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					ENDIF
				ENDIF
				
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC1_LANDING_TIME)
			
				bWeatherTypeThunderSet	= FALSE
				bWeatherTypeOvercastSet = FALSE
				bFirstPersonFXTriggered = FALSE
				
				SETTIMERA(0)
				
				iStageProgress++
			ENDIF
		
		BREAK 
		
		CASE 4
		
			IF ( bWeatherTypeOvercastSet = FALSE )
				SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 60.0)
				iWeatherTypeTimer = GET_GAME_TIMER()
				bWeatherTypeOvercastSet = TRUE
			ENDIF
		
			fDistanceToDestination = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFlightBackDestination)
		
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), GET_STRING_FROM_FLOAT(fDistanceToDestination), 0.5, 255, 255, 255)
			#ENDIF
		
			//disable player camera control when camera is interpolating from script camera to gameplay camera
			IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_VEH_LOOK_BEHIND)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
				
				IF ( bCamViewModeContextChanged = TRUE )
					IF ( bFirstPersonFXTriggered = FALSE )
						IF TIMERA() > iInterpTime - 300
							ANIMPOSTFX_PLAY("CamPushInTrevor", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bFirstPersonFXTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				IF ( bCamViewModeContextChanged = TRUE )
					SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT, CAM_VIEW_MODE_FIRST_PERSON)
					bCamViewModeContextChanged = FALSE
				ENDIF
			ENDIF
			
			MANAGE_CONVERSATIONS_DURING_FLIGHT_HOME(psTrevor.iConversationProgress)
			
			IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vFlightBackDestination, DEFAULT_LOCATES_SIZE_VECTOR(), TRUE, vsTrevorsPlane.VehicleIndex, "MCH1_GT_T8", sLabelTGETIN2, sLabelTGETBCK2, TRUE)
			
			IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsTrevorsPlane.VehicleIndex)

					IF ( bVehiclePlaybackInterrupted = FALSE )
					
						IF ( GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE(vsTrevorsPlane.VehicleIndex) > 98.0 )
							STOP_PLAYBACK_RECORDED_VEHICLE(vsTrevorsPlane.VehicleIndex)					
						ENDIF

						IF IS_PLAYER_IN_CONTROL_OF_VEHICLE(vsTrevorsPlane.VehicleIndex)
						
							STOP_PLAYBACK_RECORDED_VEHICLE(vsTrevorsPlane.VehicleIndex)
							
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							
							bVehiclePlaybackInterrupted = TRUE

						ENDIF

					ENDIF
				
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
				
					IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1719.452637,3254.324951,40.144505>>, <<1048.329956,3073.852295,50.704060>>, 34.0)
					AND IS_ENTITY_IN_ANGLED_AREA(vsTrevorsPlane.VehicleIndex, <<1719.452637,3254.324951,40.144505>>, <<1048.329956,3073.852295,50.704060>>, 34.0)
				
						IF ( GET_ENTITY_HEIGHT_ABOVE_GROUND(vsTrevorsPlane.VehicleIndex) < 2.0 )	
							IF ( GET_ENTITY_SPEED(vsTrevorsPlane.VehicleIndex) < 10.0 )
							OR IS_VEHICLE_STOPPED(vsTrevorsPlane.VehicleIndex)
							
								iStageProgress++
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 5
		
			IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vsTrevorsPlane.VehicleIndex, 6.0)
			
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
				DESTROY_ALL_CAMS()
							
				TRIGGER_MUSIC_EVENT("MIC1_FLIGHT_LANDED")
						
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			
				RETURN TRUE
			
			ENDIF
		
		BREAK
			
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_MISSION_STAGE_END_COMPLETED(INT &iStageProgress)

	SWITCH iStageProgress
	
		CASE 0
		
			IF DOES_ENTITY_EXIST(vsTrevorsCar.VehicleIndex)
				IF NOT IS_ENTITY_DEAD(vsTrevorsCar.VehicleIndex)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vsTrevorsCar.VehicleIndex))
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vsTrevorsCar.VehicleIndex, FALSE)
					ENDIF
					SET_MISSION_VEHICLE_GEN_VEHICLE(vsTrevorsCar.VehicleIndex, vsTrevorsCar.vPosition, vsTrevorsCar.fHeading)					
					bTrevorsCarSetAsVehicleGenVehicle = TRUE
					WAIT(1)
				ENDIF
			ENDIF
			
			IF ( bTextMessageSent = FALSE )
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LAMAR, "MIC1_END", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
				bTextMessageSent = TRUE
			ENDIF
		
			RETURN TRUE
		
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

//|============================= END MISSION STAGES FUNCTIONS ============================|

//|=================================== MAIN SCRIPT LOOP ==================================|

SCRIPT
    
	SET_MISSION_FLAG (TRUE)
	
    //check for death or arrest
    IF HAS_FORCE_CLEANUP_OCCURRED()
	
		STORE_FAIL_WEAPON(GET_PED_INDEX(CHAR_MICHAEL), ENUM_TO_INT(SELECTOR_PED_MICHAEL))
		
		Mission_Flow_Mission_Force_Cleanup()
		
		MISSION_CLEANUP()
		
		IF ( bPrologueMapActive = TRUE )					//call the delayed cleanup of prologue assets/settings
			SET_CLEANUP_TO_RUN_ON_RESPAWN(RCI_MICHAEL1)		//any changes to this script prologue cleanup
															//should be updated in RUN_RCI_MICHAEL1() from respawn_cleanup_data.sch
			
			g_RestoreSnapshotWeaponsOnDeath = TRUE										
			RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID()).")
			#ENDIF
			
		ENDIF												

		TERMINATE_THIS_THREAD()
		
    ENDIF
	
	//create debug widgets and skip menu
	#IF IS_DEBUG_BUILD
		CREATE_DEBUG_MISSION_STAGE_MENU()
		CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	//handle mission replay
	IF IS_REPLAY_IN_PROGRESS()
	
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission replay is in progress.")
		#ENDIF
	
		SET_MISSION_STAGE_FOR_REPLAY(eMissionStage, Get_Replay_Mid_Mission_Stage())
		
		eMichaelsFailWeapon = GET_FAIL_WEAPON(ENUM_TO_INT(SELECTOR_PED_MICHAEL))
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Michael's fail weapon from GET_FAIL_WEAPON() is ", GET_WEAPON_NAME(eMichaelsFailWeapon), ".")
		#ENDIF
		
		bReplayFlag = TRUE

		//handle shitskip message
		IF ( g_bShitSkipAccepted = TRUE )
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip accepted by the player.")
			#ENDIF
			
			SWITCH eMissionStage												//check current mission stage when shit skip is accepted
			
				CASE MISSION_STAGE_GET_TO_AIRPORT								//if replaying and shit skipping MISSION_STAGE_GET_TO_AIRPORT
				
					SWITCH GET_PLAYER_CHARACTER_AT_MISSION_START() 				//check character who started the mission
						CASE CHAR_TREVOR										//if mission was started as Trevor
							eMissionStage = MISSION_STAGE_FLY_TO_NORTH_YANKTON	//change stage to MISSION_STAGE_FLY_TO_NORTH_YANKTON
						BREAK
						CASE CHAR_MICHAEL										
							eMissionStage = GET_NEXT_MISSION_STAGE(eMissionStage)
						BREAK
					ENDSWITCH
					
				BREAK
				
				CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON
					
					SWITCH GET_PLAYER_CHARACTER_AT_MISSION_START() 				//check character who started the mission
						CASE CHAR_TREVOR										//if mission was started as Trevor
							eMissionStage = MISSION_STAGE_CUTSCENE_GRAVEYARD	//change stage to MISSION_STAGE_FLY_TO_NORTH_YANKTON
						BREAK
						CASE CHAR_MICHAEL										
							eMissionStage = GET_NEXT_MISSION_STAGE(eMissionStage)
						BREAK
					ENDSWITCH
					
				BREAK
				
				DEFAULT
					eMissionStage = GET_NEXT_MISSION_STAGE(eMissionStage)
				BREAK
				
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip changed mission stage to ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), ".")
			#ENDIF
		
		ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Shitskip not triggered.")
			#ENDIF
		
		ENDIF
		
		//if mission is being replayed or shit skipped to stage MISSION_STAGE_CUTSCENE_GRAVEYARD
		IF ( eMissionStage = MISSION_STAGE_CUTSCENE_GRAVEYARD )
			SET_CUTSCENE_TRIGGER_FLAG_FOR_PLAYER_PED_ENUM(GET_PLAYER_CHARACTER_AT_MISSION_START(), bCutsceneTriggeredByMichael, bCutsceneTriggeredByTrevor)
		ENDIF

	ELSE
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(MID_MISSION_STAGE_GET_TO_AIRPORT), "GET TO AIRPORT", FALSE)
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active.")
		#ENDIF
		
		IF NOT IS_REPLAY_IN_PROGRESS()
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active and mission replay is not in progress.")
			#ENDIF
		
			eMissionStage = MISSION_STAGE_CUTSCENE_INTRO
		
			bReplayFlag = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Setting local replay flag to TRUE for stage loading.")
			#ENDIF
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission stage is ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), " for repeat play and no mission replay.")
			#ENDIF
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Repeat play is active and mission replay is in progress.")
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Mission stage is ", GET_MISSION_STAGE_NAME_FOR_DEBUG(eMissionStage), " for repeat play and mission replay.")
			#ENDIF
		
		ENDIF
		
	ENDIF

    //loop forever until mission is passed or failed
    WHILE TRUE
		
		//check each frame if mission is failed due to fail conditions
		//check only if mission stage loading has finished
		IF ( bLoadingFlag = TRUE )
			RUN_FAIL_CHECKS(eMissionStage, eMissionFail)
		ENDIF
		
		IF ( bPrologueMapActive = TRUE )
			RUN_GRAVEYARD_GATES_CHECK(eMissionStage, bOpenGateTriggered, bSmoothCloseGateTriggered, iGateCloseProgress)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())))
						IF NOT DOES_ENTITY_EXIST(g_MissionStatSingleSpeedWatchEntity)
						OR (IS_ENTITY_A_VEHICLE(g_MissionStatSingleSpeedWatchEntity) AND GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleSpeedWatchEntity) != GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BuryTheHatchet")
		
		SWITCH eMissionStage
		
			CASE MISSION_STAGE_CUTSCENE_INTRO
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				UPDATE_FIRST_PERSON_VEHICLE_CAMERA_BLOCKING()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_INTRO_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GET_TO_AIRPORT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_GET_TO_AIRPORT
			
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						IF DOES_ENTITY_EXIST(psTrevor.PedIndex)
							SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
						ENDIF
					BREAK
					CASE CHAR_TREVOR
						IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
						ENDIF
					BREAK
				ENDSWITCH
				
				UPDATE_FIRST_PERSON_VEHICLE_CAMERA_BLOCKING()
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GET_TO_AIRPORT_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_AIRPORT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_FLY_TO_NORTH_YANKTON		//this stage can only be reached on a retry or debug skip
			
				IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				ENDIF
			
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_FLY_TO_NORTH_YANKTON_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
						eMissionStage = MISSION_STAGE_CUTSCENE_AIRPORT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_AIRPORT
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_AIRPORT_COMPLETED(iMichaelStageProgress, iTrevorStageProgress)
						RESET_MISSION_FLAGS()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							eMissionStage = MISSION_STAGE_GET_TO_GRAVEYARD
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							eMissionStage = MISSION_STAGE_CUTSCENE_GRAVEYARD
							bCutsceneTriggeredByMichael = FALSE
							bCutsceneTriggeredByTrevor 	= TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
		
			CASE MISSION_STAGE_GET_TO_GRAVEYARD
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GET_TO_GRAVEYARD_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GET_TO_GRAVE
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_GET_TO_GRAVE
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GET_TO_GRAVE_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_GRAVEYARD
					ENDIF
				ENDIF
			BREAK

			CASE MISSION_STAGE_CUTSCENE_GRAVEYARD
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_GRAVEYARD_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_GRAVEYARD_SHOOTOUT
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_GRAVEYARD_SHOOTOUT
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_GRAVEYARD_SHOOTOUT_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_CUTSCENE_KIDNAPPED
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_CUTSCENE_KIDNAPPED
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_CUTSCENE_KIDNAPPED_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_FLY_HOME
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_FLY_HOME
			
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_FLY_HOME_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_END
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_END
				IF IS_MISSION_STAGE_LOADED(eMissionStage, iStageSetupProgress, bLoadingFlag, bSkipFlag, bReplayFlag)
					IF IS_MISSION_STAGE_END_COMPLETED(iMissionStageProgress)
						RESET_MISSION_FLAGS()
						eMissionStage = MISSION_STAGE_PASSED
					ENDIF
				ENDIF
			BREAK
			
			CASE MISSION_STAGE_PASSED
						
				AWARD_ACHIEVEMENT_FOR_MISSION(ACH03) // The Not-So Shallow Grave
				
				MISSION_FLOW_MISSION_PASSED()	
				
			    MISSION_CLEANUP()
				
				IF ( bPrologueMapActive = TRUE )
					PROLOGUE_MAP_CLEANUP()			//remove prologue map if active
				ENDIF
				
				TERMINATE_THIS_THREAD()
				
			BREAK
			
			CASE MISSION_STAGE_FAILED
				
				STORE_FAIL_WEAPON(GET_PED_INDEX(CHAR_MICHAEL), ENUM_TO_INT(SELECTOR_PED_MICHAEL))
				
				SET_MISSION_FAILED_WITH_REASON(eMissionFail, bPrologueMapActive)
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_CHENGSR, TREVOR_BOOK)
				ENDIF
				
				TRIGGER_MUSIC_EVENT("MIC1_FAIL")													//stop all music
				IF IS_STREAM_PLAYING() STOP_STREAM() ENDIF											//stop flashback stream if it is playing
				
				IF ( bPlayerDroveOffRoute = TRUE )													//if player failed for going off route
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())								//stop the player when going off route
						BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 10.0, 5)	//to the graveyard
					ENDIF
				ENDIF
				
				WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
					WAIT(0)
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Waiting for GET_MISSION_FLOW_SAFE_TO_CLEANUP() to return TRUE.")
					#ENDIF
				ENDWHILE
				
				SWITCH GET_CURRENT_PLAYER_PED_ENUM()
					CASE CHAR_MICHAEL
						IF ( bPrologueMapActive = TRUE )
							RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())								//restore weapons stroed in snapshot
							MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1039.6115, -2740.5698, 19.1693>>, 328.9699)	//warp Michael to airport terminal
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID()).")
							#ENDIF
						ENDIF
					BREAK
					CASE CHAR_TREVOR
						IF ( bTrevorFlightActive = TRUE )
							MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1782.9651, 3314.9270, 40.4374>>, 341.4644)	//warp Trevor near countryside air
						ENDIF
					BREAK
				ENDSWITCH

				MISSION_CLEANUP()
				
				IF ( bPrologueMapActive = TRUE )
					PROLOGUE_MAP_CLEANUP()			//remove prologue map if active
				ENDIF
				
				
				TERMINATE_THIS_THREAD()
			BREAK
			
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), MIC1_DAMAGE)
			
			IF DOES_ENTITY_EXIST(vsTrevorsPlane.VehicleIndex)
			AND IS_VEHICLE_DRIVEABLE(vsTrevorsPlane.VehicleIndex)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsTrevorsPlane.VehicleIndex)
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vsTrevorsPlane.VehicleIndex, MIC1_PLANE_DAMAGE)
			ELSE
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, MIC1_PLANE_DAMAGE)
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND (IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))) OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MIC1_CAR_DAMAGE)
			ELSE
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, MIC1_CAR_DAMAGE)
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD																			//handle debug functionality
			RUN_DEBUG_PASS_AND_FAIL_CHECK(eMissionStage, eMissionFail)								//handle key_f and key_s presses
			RUN_DEBUG_MISSION_STAGE_MENU(iReturnStage, eMissionStage, bSkipFlag, bStageResetFlag)	//handle z-skip menu
			RUN_DEBUG_SKIPS(eMissionStage, bSkipFlag, bStageResetFlag)								//handle j-skip and p-skip
			RUN_DEBUG_WIDGETS(eMissionStage)														//handle rag widgets
			RUN_DEBUG_INFORMATION_DISPLAY()															//handle drawing of debug information
		#ENDIF
		
		WAIT(0)
		
    ENDWHILE
	
ENDSCRIPT
