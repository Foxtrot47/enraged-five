//Solomon5.sc
//Ross Wallace 20/06/2012

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "commands_clock.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "blip_control_public.sch"
// ____________________________________ INCLUDES ___________________________________________
USING "commands_script.sch"
USING "CompletionPercentage_public.sch"
using "commands_pad.sch"
using "commands_misc.sch"
using "commands_player.sch"
USING "Commands_streaming.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "script_MISC.sch"
USING "commands_object.sch"
USING "script_ped.sch"
USING "chase_hint_cam.sch"
USING "script_blips.sch" 
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "script_maths.sch"
USING "mission_stat_public.sch"
USING "cutscene_public.sch"
using "selector_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "shop_public.sch"
USING "rappel_public.sch"
USING "Shared_hud_Displays.sch"
USING "mission_stat_public.sch"
USING "clearMissionArea.sch"
USING "commands_recording.sch"

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					144
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					35	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				27	
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		6
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK		7
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		5
USING "Traffic.sch"


//USING "trigger_scene_michael4.sch"
//
VEHICLE_INDEX carMichaelIsIn
BOOL bHasChanged

#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

WEAPON_TYPE MichaelsWeapon = WEAPONTYPE_ASSAULTRIFLE

INT iRandomVariation = -1
BOOL bDisplayTimer
// ____________________________________ VARIABLES __________________________________________

INT iMissionTimer 
INT iMissionStartTime

INT iExtraBadGuysComingIntoDiningRoomCount

INT MISSION_TIME_LIMIT = 120000

INT iDisableReplayCameraTimer //Fix for bug 2227677

WEAPON_TYPE wtCurentWeapon

//STAGE_TIMELAPSE

//CAMERA_INDEX cutsceneCamera
//CAMERA_INDEX initialCam
//CAMERA_INDEX destinationCam

#IF IS_DEBUG_BUILD 

	WIDGET_GROUP_ID sol5WidgetGroup
	
	INT iDebugMissionStage
	BOOL bDebugInitialised
	BOOL bIsSuperDebugEnabled
	BOOL bDebugOn = TRUE
	
	CONST_INT MAX_SKIP_MENU_LENGTH 16                      // number of stages in mission + 2 (for menu )
	INT iReturnStage                                       // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 
	
	TEXT_LABEL_63 debugPedName
	
#ENDIF

REL_GROUP_HASH relGroupMerryWeather

structPedsForConversation myScriptedSpeech
INT i_current_event

LOCATES_HEADER_DATA sLocatesData

INTERIOR_INSTANCE_INDEX intMichaelsHouse

VEHICLE_INDEX carMichael
VEHICLE_INDEX carMichael2
PED_INDEX pedJimmy
PED_INDEX pedLimoDriver
//PED_INDEX pedCrowd[16]

VECTOR vMichaelsHouse = <<-828.3110, 174.0465, 69.6267>>
//VECTOR vJimmyBedRoomCoords = << -808.2546, 171.7330, 75.7408 >>
VECTOR vInsideHouseCoords = << -815.3414, 179.0607, 71.1530 >>

VEHICLE_INDEX badguyCarOutsideHouse1
VEHICLE_INDEX badguyCarOutsideHouse2

PED_INDEX pedAmanda
PED_INDEX pedTracey
PED_INDEX pedHostagetakerTracey
PED_INDEX pedHostagetakerAmanda

PED_INDEX pedWritheGuy

OBJECT_INDEX oiAmandasShoe

CONST_INT NUMBER_OF_MERRYWEATHER 17

//PED_INDEX pedMerryWeatherDummyGrenade

PED_INDEX pedMerryWeather[NUMBER_OF_MERRYWEATHER]
AI_BLIP_STRUCT blipPedMerryWeather[NUMBER_OF_MERRYWEATHER]

PED_INDEX pedMerryWeatherCutscene[NUMBER_OF_MERRYWEATHER]
//VEHICLE_INDEX chopperMerryWeather

INT iTimeOfMerryWeatherSpawn[NUMBER_OF_MERRYWEATHER]
BOOL bMWCharging[NUMBER_OF_MERRYWEATHER]

//RAPPEL_DATA pedsRappelData[2]

BLIP_INDEX blipPedHostagetaker
BLIP_INDEX blipInsideHouse
BLIP_INDEX blipPedTracey
BLIP_INDEX blipTraceysRoom
BLIP_INDEX blipAmanda

INT iSceneId 
VECTOR vAmandaScenePos =  << -807.212, 182.799, 74.000 >>//<<-807.239, 182.773, 74.000>>
VECTOR vAmandaSceneRot = << 0.000, 0.000, 21.000 >>//<<0.0, 0.0, 20.0>>

VECTOR	vTraceyScenePos = << -803.998, 175.910, 75.745 >> //<< -803.998, 175.910, 75.671 >>
VECTOR	vTraceySceneRot = << 0.000, 0.000, 21.120 >>

MODEL_NAMES mnGoonModel = S_M_Y_BLACKOPS_01
MODEL_NAMES mnGoonModel2 = S_M_Y_BLACKOPS_02

// ____________________________________ FUNCTIONS __________________________________________


ENUM MISSION_STAGE_FLAG 

	STAGE_INITIALISE,						//0
	STAGE_PICKUP_JIMMY,						//1
	STAGE_GET_TO_MOVIE_PREMIERE,			//2
	STAGE_MOVIE_PREMIERE_CUT,				//3
	STAGE_GET_TO_MICHAELS_HOUSE,			//4
	STAGE_ENTER_THE_HOUSE,					//5
	STAGE_SAVE_AMANDA,						//6
	STAGE_SAVE_TRACEY,						//7
	STAGE_TRACEY_CAPTOR_SHOT,				//8
	STAGE_FIGHT_INCOMING_BAD_GUYS,			//9
	STAGE_REUNITE_WITH_FAMILY,				//10
	STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE,		//11
	STAGE_MISSION_PASSED,					//12
	STAGE_RECORD_UBER,				
	STAGE_DEBUG								//13

ENDENUM


MISSION_STAGE_FLAG mission_stage =  STAGE_INITIALISE //STAGE_TIME_LAPSE //STAGE_INITIALISE //STAGE_TIME_LAPSE


PROC SETUP_TRAFFIC()

//TrafficCarPos[0] = <<-729.6351, -155.1821, 36.4754>>
//TrafficCarQuatX[0] = -0.0031
//TrafficCarQuatY[0] = 0.0135
//TrafficCarQuatZ[0] = 0.2759
//TrafficCarQuatW[0] = 0.9611
//TrafficCarRecording[0] = 2
//TrafficCarStartime[0] = 2442.0000
//TrafficCarModel[0] = dilettante

TrafficCarPos[1] = <<-751.3091, -97.0937, 37.2116>>
TrafficCarQuatX[1] = -0.0151
TrafficCarQuatY[1] = 0.0044
TrafficCarQuatZ[1] = 0.8566
TrafficCarQuatW[1] = 0.5158
TrafficCarRecording[1] = 3
TrafficCarStartime[1] = 5742.0000
TrafficCarModel[1] = dilettante

ParkedCarPos[0] = <<-751.0682, -90.6509, 36.9707>>
ParkedCarQuatX[0] = -0.0402
ParkedCarQuatY[0] = 0.0010
ParkedCarQuatZ[0] = 0.5160
ParkedCarQuatW[0] = 0.8556
ParkedCarModel[0] = washington

TrafficCarPos[2] = <<-775.5784, -70.1046, 37.2953>>
TrafficCarQuatX[2] = 0.0007
TrafficCarQuatY[2] = 0.0127
TrafficCarQuatZ[2] = -0.2998
TrafficCarQuatW[2] = 0.9539
TrafficCarRecording[2] = 4
TrafficCarStartime[2] = 7920.0000
TrafficCarModel[2] = washington

TrafficCarPos[3] = <<-773.5848, -61.4278, 37.3519>>
TrafficCarQuatX[3] = -0.0044
TrafficCarQuatY[3] = 0.0007
TrafficCarQuatZ[3] = 0.8512
TrafficCarQuatW[3] = 0.5249
TrafficCarRecording[3] = 5
TrafficCarStartime[3] = 8580.0000
TrafficCarModel[3] = dilettante

TrafficCarPos[4] = <<-770.5067, -57.7925, 37.4167>>
TrafficCarQuatX[4] = 0.0014
TrafficCarQuatY[4] = -0.0016
TrafficCarQuatZ[4] = 0.8783
TrafficCarQuatW[4] = 0.4782
TrafficCarRecording[4] = 6
TrafficCarStartime[4] = 8778.0000
TrafficCarModel[4] = washington

TrafficCarPos[5] = <<-771.5404, -53.2816, 37.4863>>
TrafficCarQuatX[5] = -0.0037
TrafficCarQuatY[5] = 0.0017
TrafficCarQuatZ[5] = 0.8545
TrafficCarQuatW[5] = 0.5195
TrafficCarRecording[5] = 7
TrafficCarStartime[5] = 9438.0000
TrafficCarModel[5] = taxi

TrafficCarPos[6] = <<-800.9717, -48.9470, 37.4214>>
TrafficCarQuatX[6] = 0.0037
TrafficCarQuatY[6] = -0.0005
TrafficCarQuatZ[6] = 0.9582
TrafficCarQuatW[6] = -0.2862
TrafficCarRecording[6] = 8
TrafficCarStartime[6] = 11088.0000
TrafficCarModel[6] = dilettante

TrafficCarPos[7] = <<-727.7749, -43.7349, 37.3255>>
TrafficCarQuatX[7] = 0.0112
TrafficCarQuatY[7] = 0.0188
TrafficCarQuatZ[7] = -0.5240
TrafficCarQuatW[7] = 0.8515
TrafficCarRecording[7] = 9
TrafficCarStartime[7] = 13266.0000
TrafficCarModel[7] = washington

TrafficCarPos[8] = <<-718.5084, -32.0203, 37.5871>>
TrafficCarQuatX[8] = -0.0007
TrafficCarQuatY[8] = -0.0001
TrafficCarQuatZ[8] = -0.3955
TrafficCarQuatW[8] = 0.9185
TrafficCarRecording[8] = 10
TrafficCarStartime[8] = 13596.0000
TrafficCarModel[8] = fugitive

TrafficCarPos[9] = <<-706.7288, -36.0906, 37.2160>>
TrafficCarQuatX[9] = 0.0080
TrafficCarQuatY[9] = -0.0123
TrafficCarQuatZ[9] = 0.9999
TrafficCarQuatW[9] = 0.0029
TrafficCarRecording[9] = 11
TrafficCarStartime[9] = 13794.0000
TrafficCarModel[9] = dilettante

//TrafficCarPos[10] = <<-705.1202, -80.0561, 37.5704>>
//TrafficCarQuatX[10] = 0.0008
//TrafficCarQuatY[10] = 0.0012
//TrafficCarQuatZ[10] = -0.5236
//TrafficCarQuatW[10] = 0.8520
//TrafficCarRecording[10] = 12
//TrafficCarStartime[10] = 14058.0000
//TrafficCarModel[10] = fugitive

ParkedCarPos[1] = <<-675.0334, -53.0923, 37.7784>>
ParkedCarQuatX[1] = -0.0312
ParkedCarQuatY[1] = 0.0263
ParkedCarQuatZ[1] = 0.9788
ParkedCarQuatW[1] = -0.2005
ParkedCarModel[1] = voltic

ParkedCarPos[2] = <<-659.4994, -49.5529, 39.0128>>
ParkedCarQuatX[2] = -0.0449
ParkedCarQuatY[2] = 0.0172
ParkedCarQuatZ[2] = 0.9969
ParkedCarQuatW[2] = -0.0625
ParkedCarModel[2] = fugitive
//
//TrafficCarPos[11] = <<-645.8763, -54.1061, 40.4247>>
//TrafficCarQuatX[11] = -0.0151
//TrafficCarQuatY[11] = -0.0365
//TrafficCarQuatZ[11] = 0.7463
//TrafficCarQuatW[11] = 0.6644
//TrafficCarRecording[11] = 13
//TrafficCarStartime[11] = 17688.0000
//TrafficCarModel[11] = patriot

TrafficCarPos[12] = <<-681.9445, -11.6259, 37.9297>>
TrafficCarQuatX[12] = -0.0047
TrafficCarQuatY[12] = -0.0143
TrafficCarQuatZ[12] = 0.8913
TrafficCarQuatW[12] = 0.4532
TrafficCarRecording[12] = 14
TrafficCarStartime[12] = 21978.0000
TrafficCarModel[12] = dilettante

TrafficCarPos[13] = <<-692.7006, -14.9014, 37.7940>>
TrafficCarQuatX[13] = -0.0062
TrafficCarQuatY[13] = -0.0142
TrafficCarQuatZ[13] = 0.8314
TrafficCarQuatW[13] = 0.5555
TrafficCarRecording[13] = 15
TrafficCarStartime[13] = 22242.0000
TrafficCarModel[13] = taxi

TrafficCarPos[14] = <<-700.3921, -17.4427, 37.6499>>
TrafficCarQuatX[14] = -0.0053
TrafficCarQuatY[14] = -0.0096
TrafficCarQuatZ[14] = 0.8488
TrafficCarQuatW[14] = 0.5286
TrafficCarRecording[14] = 16
TrafficCarStartime[14] = 22440.0000
TrafficCarModel[14] = fugitive

TrafficCarPos[15] = <<-683.7543, -10.9341, 37.9169>>
TrafficCarQuatX[15] = -0.0088
TrafficCarQuatY[15] = -0.0150
TrafficCarQuatZ[15] = 0.8015
TrafficCarQuatW[15] = 0.5977
TrafficCarRecording[15] = 17
TrafficCarStartime[15] = 27324.0000
TrafficCarModel[15] = dilettante

TrafficCarPos[16] = <<-666.5709, -5.1678, 38.7878>>
TrafficCarQuatX[16] = -0.0242
TrafficCarQuatY[16] = -0.0329
TrafficCarQuatZ[16] = 0.7574
TrafficCarQuatW[16] = 0.6517
TrafficCarRecording[16] = 18
TrafficCarStartime[16] = 28644.0000
TrafficCarModel[16] = fugitive

TrafficCarPos[17] = <<-611.8765, 2.8695, 41.8305>>
TrafficCarQuatX[17] = -0.0411
TrafficCarQuatY[17] = -0.0073
TrafficCarQuatZ[17] = 0.7520
TrafficCarQuatW[17] = 0.6578
TrafficCarRecording[17] = 19
TrafficCarStartime[17] = 29370.0000
TrafficCarModel[17] = dilettante

TrafficCarPos[18] = <<-654.8570, 35.0608, 39.3026>>
TrafficCarQuatX[18] = -0.0146
TrafficCarQuatY[18] = -0.0421
TrafficCarQuatZ[18] = 0.9981
TrafficCarQuatW[18] = -0.0433
TrafficCarRecording[18] = 20
TrafficCarStartime[18] = 37224.0000
TrafficCarModel[18] = fugitive

TrafficCarPos[19] = <<-655.0488, 48.7777, 41.5350>>
TrafficCarQuatX[19] = -0.0194
TrafficCarQuatY[19] = -0.1278
TrafficCarQuatZ[19] = 0.9911
TrafficCarQuatW[19] = 0.0312
TrafficCarRecording[19] = 21
TrafficCarStartime[19] = 39072.0000
TrafficCarModel[19] = patriot

//TrafficCarPos[20] = <<-662.9526, 126.2494, 56.7466>>
//TrafficCarQuatX[20] = 0.0074
//TrafficCarQuatY[20] = -0.0020
//TrafficCarQuatZ[20] = -0.6753
//TrafficCarQuatW[20] = 0.7375
//TrafficCarRecording[20] = 22
//TrafficCarStartime[20] = 42372.0000
//TrafficCarModel[20] = fugitive

TrafficCarPos[21] = <<-651.6327, 132.7944, 56.6805>>
TrafficCarQuatX[21] = -0.0004
TrafficCarQuatY[21] = 0.0018
TrafficCarQuatZ[21] = 0.7071
TrafficCarQuatW[21] = 0.7071
TrafficCarRecording[21] = 23
TrafficCarStartime[21] = 42570.0000
TrafficCarModel[21] = INFERNUS

TrafficCarPos[22] = <<-651.4924, 163.9558, 59.5013>>
TrafficCarQuatX[22] = -0.0201
TrafficCarQuatY[22] = -0.1194
TrafficCarQuatZ[22] = 0.9926
TrafficCarQuatW[22] = -0.0115
TrafficCarRecording[22] = 24
TrafficCarStartime[22] = 47256.0000
TrafficCarModel[22] = dilettante

TrafficCarPos[23] = <<-651.2297, 144.6613, 56.8862>>
TrafficCarQuatX[23] = -0.0198
TrafficCarQuatY[23] = -0.0254
TrafficCarQuatZ[23] = 0.9994
TrafficCarQuatW[23] = 0.0093
TrafficCarRecording[23] = 25
TrafficCarStartime[23] = 47982.0000
TrafficCarModel[23] = fugitive

TrafficCarPos[24] = <<-653.9053, 242.1704, 80.8715>>
TrafficCarQuatX[24] = -0.0091
TrafficCarQuatY[24] = -0.0022
TrafficCarQuatZ[24] = 0.9966
TrafficCarQuatW[24] = -0.0816
TrafficCarRecording[24] = 26
TrafficCarStartime[24] = 52866.0000
TrafficCarModel[24] = dilettante

TrafficCarPos[25] = <<-637.2720, 278.5612, 80.9255>>
TrafficCarQuatX[25] = -0.0084
TrafficCarQuatY[25] = -0.0005
TrafficCarQuatZ[25] = 0.6222
TrafficCarQuatW[25] = 0.7828
TrafficCarRecording[25] = 27
TrafficCarStartime[25] = 60984.0000
TrafficCarModel[25] = voltic

ParkedCarPos[3] = <<-569.8005, 269.0992, 82.5511>>
ParkedCarQuatX[3] = 0.0030
ParkedCarQuatY[3] = -0.0111
ParkedCarQuatZ[3] = 0.6585
ParkedCarQuatW[3] = 0.7525
ParkedCarModel[3] = STRETCH

TrafficCarPos[26] = <<-591.6848, 262.7580, 81.9388>>
TrafficCarQuatX[26] = -0.0076
TrafficCarQuatY[26] = -0.0067
TrafficCarQuatZ[26] = 0.6679
TrafficCarQuatW[26] = 0.7442
TrafficCarRecording[26] = 28
TrafficCarStartime[26] = 62964.0000
TrafficCarModel[26] = taxi

TrafficCarPos[27] = <<-587.4163, 258.1250, 81.9937>>
TrafficCarQuatX[27] = 0.0021
TrafficCarQuatY[27] = 0.0299
TrafficCarQuatZ[27] = 0.7597
TrafficCarQuatW[27] = -0.6496
TrafficCarRecording[27] = 29
TrafficCarStartime[27] = 63096.0000
TrafficCarModel[27] = akuma

//TrafficCarPos[28] = <<-578.1308, 256.4715, 82.5131>>
//TrafficCarQuatX[28] = -0.0102
//TrafficCarQuatY[28] = 0.0112
//TrafficCarQuatZ[28] = 0.7497
//TrafficCarQuatW[28] = -0.6616
//TrafficCarRecording[28] = 30
//TrafficCarStartime[28] = 63228.0000
//TrafficCarModel[28] = fugitive

//TrafficCarPos[29] = <<-565.4684, 255.4141, 82.5930>>
//TrafficCarQuatX[29] = 0.0048
//TrafficCarQuatY[29] = 0.0110
//TrafficCarQuatZ[29] = 0.7280
//TrafficCarQuatW[29] = -0.6854
//TrafficCarRecording[29] = 31
//TrafficCarStartime[29] = 63228.0000
//TrafficCarModel[29] = INFERNUS

//TrafficCarPos[30] = <<-558.5502, 254.5110, 82.7675>>
//TrafficCarQuatX[30] = 0.0008
//TrafficCarQuatY[30] = -0.0003
//TrafficCarQuatZ[30] = 0.7334
//TrafficCarQuatW[30] = -0.6798
//TrafficCarRecording[30] = 32
//TrafficCarStartime[30] = 63228.0000
//TrafficCarModel[30] = fugitive

//TrafficCarPos[31] = <<-572.1489, 255.9922, 82.5679>>
//TrafficCarQuatX[31] = 0.0021
//TrafficCarQuatY[31] = 0.0156
//TrafficCarQuatZ[31] = 0.7357
//TrafficCarQuatW[31] = -0.6771
//TrafficCarRecording[31] = 33
//TrafficCarStartime[31] = 63954.0000
//TrafficCarModel[31] = voltic

TrafficCarPos[32] = <<-519.6771, 258.4605, 82.5956>>
TrafficCarQuatX[32] = -0.0006
TrafficCarQuatY[32] = -0.0011
TrafficCarQuatZ[32] = 0.6662
TrafficCarQuatW[32] = 0.7458
TrafficCarRecording[32] = 34
TrafficCarStartime[32] = 66792.0000
TrafficCarModel[32] = Vader

TrafficCarPos[33] = <<-459.0908, 255.1122, 82.4951>>
TrafficCarQuatX[33] = -0.0116
TrafficCarQuatY[33] = 0.0117
TrafficCarQuatZ[33] = 0.6560
TrafficCarQuatW[33] = 0.7546
TrafficCarRecording[33] = 35
TrafficCarStartime[33] = 71940.0000
TrafficCarModel[33] = dilettante

TrafficCarPos[34] = <<-436.5470, 247.8752, 82.8304>>
TrafficCarQuatX[34] = -0.0046
TrafficCarQuatY[34] = -0.0005
TrafficCarQuatZ[34] = 0.6607
TrafficCarQuatW[34] = 0.7506
TrafficCarRecording[34] = 36
TrafficCarStartime[34] = 73656.0000
TrafficCarModel[34] = patriot

TrafficCarPos[35] = <<-408.9057, 245.0887, 82.8828>>
TrafficCarQuatX[35] = -0.0038
TrafficCarQuatY[35] = 0.0026
TrafficCarQuatZ[35] = 0.6792
TrafficCarQuatW[35] = 0.7339
TrafficCarRecording[35] = 37
TrafficCarStartime[35] = 75504.0000
TrafficCarModel[35] = dilettante

TrafficCarPos[36] = <<-406.1917, 237.2878, 83.0315>>
TrafficCarQuatX[36] = -0.0010
TrafficCarQuatY[36] = -0.0001
TrafficCarQuatZ[36] = -0.0051
TrafficCarQuatW[36] = 1.0000
TrafficCarRecording[36] = 38
TrafficCarStartime[36] = 75702.0000
TrafficCarModel[36] = voltic

//TrafficCarPos[37] = <<-342.1515, 241.0291, 85.4560>>
//TrafficCarQuatX[37] = 0.0132
//TrafficCarQuatY[37] = -0.0221
//TrafficCarQuatZ[37] = -0.6066
//TrafficCarQuatW[37] = 0.7946
//TrafficCarRecording[37] = 39
//TrafficCarStartime[37] = 79530.0000
//TrafficCarModel[37] = patriot

//TrafficCarPos[38] = <<-290.4436, 255.7736, 88.9882>>
//TrafficCarQuatX[38] = 0.0169
//TrafficCarQuatY[38] = -0.0260
//TrafficCarQuatZ[38] = -0.6503
//TrafficCarQuatW[38] = 0.7591
//TrafficCarRecording[38] = 40
//TrafficCarStartime[38] = 82500.0000
//TrafficCarModel[38] = BUS

TrafficCarPos[39] = <<-299.1910, 263.4909, 87.9138>>
TrafficCarQuatX[39] = 0.0174
TrafficCarQuatY[39] = -0.0643
TrafficCarQuatZ[39] = 0.7701
TrafficCarQuatW[39] = 0.6345
TrafficCarRecording[39] = 41
TrafficCarStartime[39] = 83028.0000
TrafficCarModel[39] = akuma

//TrafficCarPos[40] = <<-258.6573, 258.5381, 90.8643>>
//TrafficCarQuatX[40] = 0.0281
//TrafficCarQuatY[40] = -0.0313
//TrafficCarQuatZ[40] = -0.7035
//TrafficCarQuatW[40] = 0.7095
//TrafficCarRecording[40] = 42
//TrafficCarStartime[40] = 84282.0000
//TrafficCarModel[40] = taxi

//TrafficCarPos[41] = <<-254.6566, 254.5974, 91.2276>>
//TrafficCarQuatX[41] = -0.0455
//TrafficCarQuatY[41] = 0.0137
//TrafficCarQuatZ[41] = 0.7120
//TrafficCarQuatW[41] = -0.7005
//TrafficCarRecording[41] = 43
//TrafficCarStartime[41] = 84414.0000
//TrafficCarModel[41] = STRETCH

//TrafficCarPos[42] = <<-245.5204, 258.8765, 92.0566>>
//TrafficCarQuatX[42] = -0.0009
//TrafficCarQuatY[42] = 0.0009
//TrafficCarQuatZ[42] = 0.7486
//TrafficCarQuatW[42] = -0.6631
//TrafficCarRecording[42] = 44
//TrafficCarStartime[42] = 84942.0000
//TrafficCarModel[42] = BUS

ParkedCarPos[4] = <<-250.2790, 286.0272, 91.3140>>
ParkedCarQuatX[4] = -0.0195
ParkedCarQuatY[4] = 0.0289
ParkedCarQuatZ[4] = 0.7379
ParkedCarQuatW[4] = -0.6740
ParkedCarModel[4] = dilettante

TrafficCarPos[43] = <<-241.4356, 254.0293, 91.5941>>
TrafficCarQuatX[43] = -0.0009
TrafficCarQuatY[43] = 0.0009
TrafficCarQuatZ[43] = 0.7333
TrafficCarQuatW[43] = -0.6799
TrafficCarRecording[43] = 45
TrafficCarStartime[43] = 85140.0000
TrafficCarModel[43] = voltic

TrafficCarPos[44] = <<-203.4949, 264.4652, 91.5720>>
TrafficCarQuatX[44] = -0.0050
TrafficCarQuatY[44] = -0.0075
TrafficCarQuatZ[44] = 0.6829
TrafficCarQuatW[44] = 0.7305
TrafficCarRecording[44] = 46
TrafficCarStartime[44] = 87516.0000
TrafficCarModel[44] = INFERNUS

TrafficCarPos[45] = <<-187.1415, 262.1929, 92.5558>>
TrafficCarQuatX[45] = -0.0086
TrafficCarQuatY[45] = -0.0083
TrafficCarQuatZ[45] = 0.6573
TrafficCarQuatW[45] = 0.7536
TrafficCarRecording[45] = 47
TrafficCarStartime[45] = 88704.0000
TrafficCarModel[45] = BUS

TrafficCarPos[46] = <<-142.2844, 258.0151, 94.5132>>
TrafficCarQuatX[46] = -0.0292
TrafficCarQuatY[46] = -0.0211
TrafficCarQuatZ[46] = 0.6964
TrafficCarQuatW[46] = 0.7167
TrafficCarRecording[46] = 48
TrafficCarStartime[46] = 91872.0000
TrafficCarModel[46] = dilettante

TrafficCarPos[47] = <<-115.8647, 258.4900, 96.5116>>
TrafficCarQuatX[47] = -0.0339
TrafficCarQuatY[47] = -0.0267
TrafficCarQuatZ[47] = 0.7336
TrafficCarQuatW[47] = 0.6783
TrafficCarRecording[47] = 49
TrafficCarStartime[47] = 93852.0000
TrafficCarModel[47] = bati

TrafficCarPos[48] = <<-112.0918, 236.9431, 97.3335>>
TrafficCarQuatX[48] = -0.0420
TrafficCarQuatY[48] = 0.0124
TrafficCarQuatZ[48] = 0.9929
TrafficCarQuatW[48] = -0.1104
TrafficCarRecording[48] = 50
TrafficCarStartime[48] = 94314.0000
TrafficCarModel[48] = patriot

TrafficCarPos[49] = <<-107.0848, 258.9811, 97.4725>>
TrafficCarQuatX[49] = -0.0390
TrafficCarQuatY[49] = -0.0295
TrafficCarQuatZ[49] = 0.7291
TrafficCarQuatW[49] = 0.6826
TrafficCarRecording[49] = 51
TrafficCarStartime[49] = 94578.0000
TrafficCarModel[49] = Vader

TrafficCarPos[50] = <<-75.1509, 262.1369, 101.6077>>
TrafficCarQuatX[50] = -0.0486
TrafficCarQuatY[50] = -0.0420
TrafficCarQuatZ[50] = 0.7448
TrafficCarQuatW[50] = 0.6641
TrafficCarRecording[50] = 52
TrafficCarStartime[50] = 97086.0000
TrafficCarModel[50] = BUS

TrafficCarPos[51] = <<-66.0495, 250.1121, 102.1338>>
TrafficCarQuatX[51] = 0.0424
TrafficCarQuatY[51] = -0.0468
TrafficCarQuatZ[51] = -0.6581
TrafficCarQuatW[51] = 0.7503
TrafficCarRecording[51] = 53
TrafficCarStartime[51] = 97680.0000
TrafficCarModel[51] = coquette

TrafficCarPos[52] = <<-26.4831, 273.7352, 106.4029>>
TrafficCarQuatX[52] = -0.0188
TrafficCarQuatY[52] = 0.0440
TrafficCarQuatZ[52] = 0.9202
TrafficCarQuatW[52] = -0.3886
TrafficCarRecording[52] = 54
TrafficCarStartime[52] = 101904.0000
TrafficCarModel[52] = coquette

//TrafficCarPos[53] = <<9.3503, 256.3408, 108.9411>>
//TrafficCarQuatX[53] = -0.0107
//TrafficCarQuatY[53] = 0.0070
//TrafficCarQuatZ[53] = 0.7823
//TrafficCarQuatW[53] = -0.6228
//TrafficCarRecording[53] = 55
//TrafficCarStartime[53] = 103818.0000
//TrafficCarModel[53] = dilettante

TrafficCarPos[54] = <<10.4011, 261.0676, 109.0329>>
TrafficCarQuatX[54] = -0.0100
TrafficCarQuatY[54] = 0.0082
TrafficCarQuatZ[54] = 0.7729
TrafficCarQuatW[54] = -0.6344
TrafficCarRecording[54] = 56
TrafficCarStartime[54] = 104016.0000
TrafficCarModel[54] = voltic

TrafficCarPos[55] = <<12.4599, 251.5634, 109.0181>>
TrafficCarQuatX[55] = -0.0105
TrafficCarQuatY[55] = 0.0069
TrafficCarQuatZ[55] = 0.7860
TrafficCarQuatW[55] = -0.6181
TrafficCarRecording[55] = 57
TrafficCarStartime[55] = 104082.0000
TrafficCarModel[55] = dilettante

//TrafficCarPos[56] = <<14.5970, 255.8522, 109.1289>>
//TrafficCarQuatX[56] = -0.0026
//TrafficCarQuatY[56] = 0.0038
//TrafficCarQuatZ[56] = 0.7953
//TrafficCarQuatW[56] = -0.6062
//TrafficCarRecording[56] = 58
//TrafficCarStartime[56] = 104280.0000
//TrafficCarModel[56] = akuma

TrafficCarPos[57] = <<15.1965, 260.1009, 109.1271>>
TrafficCarQuatX[57] = 0.0951
TrafficCarQuatY[57] = 0.0901
TrafficCarQuatZ[57] = 0.7780
TrafficCarQuatW[57] = -0.6145
TrafficCarRecording[57] = 59
TrafficCarStartime[57] = 105732.0000
TrafficCarModel[57] = nemesis

TrafficCarPos[58] = <<51.7524, 249.5017, 109.1136>>
TrafficCarQuatX[58] = -0.0112
TrafficCarQuatY[58] = 0.0070
TrafficCarQuatZ[58] = 0.5701
TrafficCarQuatW[58] = 0.8214
TrafficCarRecording[58] = 60
TrafficCarStartime[58] = 110154.0000
TrafficCarModel[58] = dilettante

TrafficCarPos[59] = <<77.1412, 246.6330, 109.1588>>
TrafficCarQuatX[59] = 0.0102
TrafficCarQuatY[59] = 0.0115
TrafficCarQuatZ[59] = 0.5713
TrafficCarQuatW[59] = 0.8206
TrafficCarRecording[59] = 61
TrafficCarStartime[59] = 111210.0000
TrafficCarModel[59] = BUS

TrafficCarPos[60] = <<108.8754, 234.9620, 107.3687>>
TrafficCarQuatX[60] = 0.0110
TrafficCarQuatY[60] = 0.0099
TrafficCarQuatZ[60] = 0.5742
TrafficCarQuatW[60] = 0.8186
TrafficCarRecording[60] = 62
TrafficCarStartime[60] = 112002.0000
TrafficCarModel[60] = coquette

TrafficCarPos[61] = <<120.7711, 235.0460, 107.1257>>
TrafficCarQuatX[61] = -0.0014
TrafficCarQuatY[61] = 0.0268
TrafficCarQuatZ[61] = 0.5704
TrafficCarQuatW[61] = 0.8209
TrafficCarRecording[61] = 63
TrafficCarStartime[61] = 112992.0000
TrafficCarModel[61] = INFERNUS

TrafficCarPos[62] = <<161.7951, 220.0426, 105.9757>>
TrafficCarQuatX[62] = 0.0041
TrafficCarQuatY[62] = 0.0179
TrafficCarQuatZ[62] = 0.6066
TrafficCarQuatW[62] = 0.7948
TrafficCarRecording[62] = 64
TrafficCarStartime[62] = 116358.0000
TrafficCarModel[62] = dilettante

TrafficCarPos[63] = <<176.7703, 204.2240, 105.5744>>
TrafficCarQuatX[63] = 0.0128
TrafficCarQuatY[63] = -0.0140
TrafficCarQuatZ[63] = 0.8145
TrafficCarQuatW[63] = -0.5799
TrafficCarRecording[63] = 65
TrafficCarStartime[63] = 117612.0000
TrafficCarModel[63] = voltic

//TrafficCarPos[64] = <<176.3473, 195.5004, 105.3261>>
//TrafficCarQuatX[64] = 0.0131
//TrafficCarQuatY[64] = -0.0124
//TrafficCarQuatZ[64] = 0.8186
//TrafficCarQuatW[64] = -0.5740
//TrafficCarRecording[64] = 66
//TrafficCarStartime[64] = 117744.0000
//TrafficCarModel[64] = dilettante

TrafficCarPos[65] = <<182.6559, 202.3705, 105.2015>>
TrafficCarQuatX[65] = 0.0073
TrafficCarQuatY[65] = -0.0159
TrafficCarQuatZ[65] = 0.8193
TrafficCarQuatW[65] = -0.5731
TrafficCarRecording[65] = 67
TrafficCarStartime[65] = 118008.0000
TrafficCarModel[65] = INFERNUS

TrafficCarPos[66] = <<182.7200, 197.1533, 104.9540>>
TrafficCarQuatX[66] = -0.0042
TrafficCarQuatY[66] = -0.0163
TrafficCarQuatZ[66] = 0.8250
TrafficCarQuatW[66] = -0.5649
TrafficCarRecording[66] = 68
TrafficCarStartime[66] = 118140.0000
TrafficCarModel[66] = coquette

//TrafficCarPos[67] = <<182.2283, 192.6395, 105.1002>>
//TrafficCarQuatX[67] = 0.0117
//TrafficCarQuatY[67] = -0.0026
//TrafficCarQuatZ[67] = 0.8225
//TrafficCarQuatW[67] = -0.5687
//TrafficCarRecording[67] = 69
//TrafficCarStartime[67] = 118206.0000
//TrafficCarModel[67] = dilettante

TrafficCarPos[68] = <<192.5483, 204.6786, 105.0900>>
TrafficCarQuatX[68] = 0.0007
TrafficCarQuatY[68] = -0.0009
TrafficCarQuatZ[68] = 0.5527
TrafficCarQuatW[68] = 0.8334
TrafficCarRecording[68] = 70
TrafficCarStartime[68] = 118602.0000
TrafficCarModel[68] = dilettante

TrafficCarPos[69] = <<217.3717, 179.4344, 104.9837>>
TrafficCarQuatX[69] = -0.0034
TrafficCarQuatY[69] = -0.0075
TrafficCarQuatZ[69] = 0.8023
TrafficCarQuatW[69] = -0.5969
TrafficCarRecording[69] = 71
TrafficCarStartime[69] = 120450.0000
TrafficCarModel[69] = dilettante

TrafficCarPos[70] = <<229.7712, 185.4740, 104.6763>>
TrafficCarQuatX[70] = 0.0113
TrafficCarQuatY[70] = -0.0001
TrafficCarQuatZ[70] = 0.5758
TrafficCarQuatW[70] = 0.8175
TrafficCarRecording[70] = 72
TrafficCarStartime[70] = 121638.0000
TrafficCarModel[70] = coquette

TrafficCarPos[71] = <<251.2794, 182.8351, 104.5498>>
TrafficCarQuatX[71] = 0.0068
TrafficCarQuatY[71] = 0.0047
TrafficCarQuatZ[71] = 0.5690
TrafficCarQuatW[71] = 0.8223
TrafficCarRecording[71] = 73
TrafficCarStartime[71] = 123024.0000
TrafficCarModel[71] = akuma

TrafficCarPos[72] = <<286.9730, 174.3262, 103.9083>>
TrafficCarQuatX[72] = 0.0076
TrafficCarQuatY[72] = 0.0054
TrafficCarQuatZ[72] = 0.5696
TrafficCarQuatW[72] = 0.8218
TrafficCarRecording[72] = 74
TrafficCarStartime[72] = 124674.0000
TrafficCarModel[72] = nemesis

//TrafficCarPos[73] = <<221.9222, 177.9926, 104.6689>>
//TrafficCarQuatX[73] = -0.0145
//TrafficCarQuatY[73] = -0.0167
//TrafficCarQuatZ[73] = 0.8099
//TrafficCarQuatW[73] = -0.5861
//TrafficCarRecording[73] = 75
//TrafficCarStartime[73] = 125334.0000
//TrafficCarModel[73] = coquette

TrafficCarPos[74] = <<299.8036, 169.1703, 103.5907>>
TrafficCarQuatX[74] = 0.0063
TrafficCarQuatY[74] = 0.0046
TrafficCarQuatZ[74] = 0.5717
TrafficCarQuatW[74] = 0.8204
TrafficCarRecording[74] = 76
TrafficCarStartime[74] = 125466.0000
TrafficCarModel[74] = dilettante

TrafficCarPos[75] = <<305.4707, 152.1204, 103.4018>>
TrafficCarQuatX[75] = 0.0051
TrafficCarQuatY[75] = -0.0074
TrafficCarQuatZ[75] = 0.8248
TrafficCarQuatW[75] = -0.5653
TrafficCarRecording[75] = 77
TrafficCarStartime[75] = 125994.0000
TrafficCarModel[75] = dilettante

TrafficCarPos[76] = <<308.3471, 147.0124, 103.2927>>
TrafficCarQuatX[76] = 0.0037
TrafficCarQuatY[76] = -0.0082
TrafficCarQuatZ[76] = 0.8101
TrafficCarQuatW[76] = -0.5862
TrafficCarRecording[76] = 78
TrafficCarStartime[76] = 126258.0000
TrafficCarModel[76] = dilettante

TrafficCarPos[77] = <<337.7297, 136.0498, 102.7815>>
TrafficCarQuatX[77] = 0.0056
TrafficCarQuatY[77] = -0.0079
TrafficCarQuatZ[77] = 0.8150
TrafficCarQuatW[77] = -0.5794
TrafficCarRecording[77] = 79
TrafficCarStartime[77] = 128502.0000
TrafficCarModel[77] = taxi

TrafficCarPos[78] = <<342.0353, 144.3674, 102.5265>>
TrafficCarQuatX[78] = 0.0034
TrafficCarQuatY[78] = -0.0092
TrafficCarQuatZ[78] = 0.8153
TrafficCarQuatW[78] = -0.5789
TrafficCarRecording[78] = 80
TrafficCarStartime[78] = 128568.0000
TrafficCarModel[78] = bati

TrafficCarPos[79] = <<348.4572, 119.5351, 102.5064>>
TrafficCarQuatX[79] = -0.0110
TrafficCarQuatY[79] = 0.0013
TrafficCarQuatZ[79] = 0.9856
TrafficCarQuatW[79] = 0.1686
TrafficCarRecording[79] = 81
TrafficCarStartime[79] = 129756.0000
TrafficCarModel[79] = dilettante

ParkedCarPos[5] = <<330.6659, 162.6515, 103.1006>>
ParkedCarQuatX[5] = 0.0083
ParkedCarQuatY[5] = 0.0058
ParkedCarQuatZ[5] = 0.5634
ParkedCarQuatW[5] = 0.8261
ParkedCarModel[5] = TOURBUS

TrafficCarPos[80] = <<378.0202, 198.8629, 102.5501>>
TrafficCarQuatX[80] = -0.0025
TrafficCarQuatY[80] = 0.0004
TrafficCarQuatZ[80] = 0.9843
TrafficCarQuatW[80] = 0.1763
TrafficCarRecording[80] = 82
TrafficCarStartime[80] = 135630.0000
TrafficCarModel[80] = dilettante

TrafficCarPos[81] = <<224.3663, 192.6471, 105.0238>>
TrafficCarQuatX[81] = 0.0118
TrafficCarQuatY[81] = 0.0016
TrafficCarQuatZ[81] = 0.5762
TrafficCarQuatW[81] = 0.8172
TrafficCarRecording[81] = 83
TrafficCarStartime[81] = 143220.0000
TrafficCarModel[81] = dilettante

ENDPROC


FUNC MODEL_NAMES getRandomGoonModel()

	IF GET_RANDOM_BOOL()
		RETURN mnGoonModel
	ELSE
		RETURN mnGoonModel2
	ENDIF
	
ENDFUNC

PROC HANDLE_CINEMA_DOOR()
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<300.691, 203.6474, 104.6431>>, 1.0, PROP_GRUMANDOOR_L)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_GRUMANDOOR_L, <<300.691, 203.6474, 104.6431>>, TRUE, 0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets whether or not the tuxedos are available
/// PARAMS:
///    bAvailable - are they available?
PROC SET_TUXEDOS_AVAILABLE(BOOL bAvailable)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO, bAvailable)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ONE, COMP_TYPE_OUTFIT, OUTFIT_P1_TUXEDO, bAvailable)
	
	SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO, bAvailable)
		
ENDPROC

//PURPOSE: Starts or stops a cutscene. If TRUE is passed TIMERA() is set to Zero
PROC SET_CUTSCENE_RUNNING(BOOL isRunning, BOOL doGameCamInterp = FALSE, INT durationFromInterp = 2000, BOOL turnOffGadgets = TRUE)

//	SET_USE_HIGHDOF(isRunning)removed
	SET_WIDESCREEN_BORDERS(isRunning,0)
	IF turnOffGadgets = TRUE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning),  SPC_DEACTIVATE_GADGETS)
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
	ENDIF
	RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	
	CLEAR_HELP(TRUE)
	CLEAR_PRINTS()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(isRunning)
		
	DISABLE_CELLPHONE(isRunning)
	DISPLAY_HUD(NOT isRunning)
	DISPLAY_RADAR(NOT isRunning)
ENDPROC

PROC FADE_IN_IF_NEEDED()

	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
	//OR NOT IS_SCREEN_FADING_IN()
		//LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		DO_SCREEN_FADE_IN(500)
	ENDIF

ENDPROC

//Cleanup
PROC MISSION_CLEANUP()

	//For replays
	PRINTLN("iMissionTimer: ", iMissionTimer)
	
	LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, FALSE, FALSE)
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	IF iMissionTimer > MISSION_TIME_LIMIT - g_replay.iReplayInt[0]
		g_replay.iReplayInt[0] = 90000
	ELSE
		g_replay.iReplayInt[0] = iMissionTimer
	ENDIF

	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS("MIC4_TUXTEXT")
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS("SOL5_DEVTXT")

	TRIGGER_MUSIC_EVENT("SOL5_FAIL")

	#IF IS_DEBUG_BUILD
		DELETE_WIDGET_GROUP(sol5WidgetGroup)
	#ENDIF
	
	PAUSE_CLOCK(FALSE)
	
	STOP_GAMEPLAY_HINT(TRUE)
	
	STOP_CUTSCENE()
	
	DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, FALSE)
	
	//DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER), FALSE)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER))	
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER))
	ENDIF
	
	SET_CUTSCENE_RUNNING(FALSE)
	//SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	HANG_UP_AND_PUT_AWAY_PHONE() 
	
	//SET_PED_AS_NO_LONGER_NEEDED(pedJimmy)
	
	
	
	REMOVE_IPL("redCarpet")
	
	STOP_CUTSCENE(TRUE)
	
	//B* - 1978122
	IF intMichaelsHouse <> NULL
		UNPIN_INTERIOR(intMichaelsHouse)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
		RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())
	ENDIF
	
	SET_ROADS_BACK_TO_ORIGINAL(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
					
	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE)

	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
	
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_MAX_WANTED_LEVEL(5)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)

	SET_NIGHTVISION(FALSE)
	CLEAR_TIMECYCLE_MODIFIER()
	SET_TIME_SCALE(1.0)		
	
	SET_PLAYER_ANGRY(PLAYER_PED_ID(), FALSE)

	//SET_VEHICLE_DENSITY_MULTIPLIER(0.25)
		
	IF IS_SCREEN_FADED_OUT()
		//DO_SCREEN_FADE_IN(500)
	ENDIF
	
ENDPROC


PROC Mission_Passed()	
	
	//FADE_IN_IF_NEEDED()
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-802.70, 176.18, 76.89>>, 1.0, V_ILEV_MM_DOORW)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORW, <<-802.70, 176.18, 76.89>>, TRUE, 0.0)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
	#ENDIF
	SET_CUTSCENE_RUNNING(FALSE)
	TRIGGER_MISSION_STATS_UI(TRUE)
	Mission_Flow_Mission_Passed()
	CLEAR_PRINTS () 	
	Mission_Cleanup()
	TERMINATE_THIS_THREAD()
		
ENDPROC

//Mission Failed
PROC Mission_Failed()	
	
	TRIGGER_MUSIC_EVENT("SOL5_FAIL")
	
	KILL_ANY_CONVERSATION()
	
	#IF IS_DEBUG_BUILD
		
	#ENDIF

	STORE_FAIL_WEAPON(PLAYER_PED_ID(), 0)

	Mission_Flow_Mission_Failed()
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
	ENDIF

	SET_TUXEDOS_AVAILABLE(FALSE) // lock the tuxedos again
	
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
	RESTORE_MISSION_START_OUTFIT() // remove tuxedo from Michael

	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	Mission_Cleanup() // must only take 1 frame and terminate the thread
	
	TERMINATE_THIS_THREAD()
		
ENDPROC


PROC DELETE_ARRAY_OF_PEDS(PED_INDEX &thesePeds[], BOOL bSetAsNoLongerNeeded = FALSE, INT iMaxIndex = -1)

	INT i
	
	INT iArrayLength 
	
	IF iMaxIndex <> -1
		iArrayLength = iMaxIndex
	ELSE
		iArrayLength = COUNT_OF(thesePeds) -1
	ENDIF
	
	FOR i = 0 TO iArrayLength
		IF DOES_ENTITY_EXIST(thesePeds[i])
			IF NOT bSetAsNoLongerNeeded
				DELETE_PED(thesePeds[i])
				PRINTLN("deleted Ped in array at: ", i)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(thesePeds[i])
				PRINTLN("set Ped as no longer needed in array at: ", i)
			ENDIF
			
		ENDIF
	ENDFOR
	
ENDPROC

PROC DELETE_ARRAY_OF_OBJECTS(OBJECT_INDEX &theseObjects[], BOOL bSetAsNoLongerNeeded = FALSE, INT iMaxIndex = -1)

	INT i
	
	INT iArrayLength 
	
	IF iMaxIndex <> -1
		iArrayLength = iMaxIndex
	ELSE
		iArrayLength = COUNT_OF(theseObjects) -1
	ENDIF
	
	FOR i = 0 TO iArrayLength
		IF DOES_ENTITY_EXIST(theseObjects[i])
			IF NOT bSetAsNoLongerNeeded
				DELETE_OBJECT(theseObjects[i])
				PRINTLN("deleted object in array at: ", i)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(theseObjects[i])
				PRINTLN("set object as no longer needed in array at: ", i)
			ENDIF
			
		ENDIF
	ENDFOR
	
ENDPROC


PROC SET_MICHAELS_TUX()
	
	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO , FALSE)
	SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
	
ENDPROC

PROC RESET_GAME_CAMERA(FLOAT fHeading = 0.0)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(fHeading)

ENDPROC

/// PURPOSE: Requests assets for next stage also sets any mission variable states such as wanted level etc.
///    
/// PARAMS: MISSION_STAGE_FLAG stageAssetsToLoad
///    stageAssetsToLoad - 
///    
//INT iNewLoadSceneTimer
//SCALEFORM_INDEX sFormBreakingNewsOverlay
///    
PROC REQUEST_STAGE_ASSETS(MISSION_STAGE_FLAG stageAssetsToLoad)

	PRINTLN("MICHAEL 4: REQUESTING STAGE ASSETS FOR STAGE: ", ENUM_TO_INT(stageAssetsToLoad))

	SWITCH stageAssetsToLoad

		CASE STAGE_PICKUP_JIMMY
			REQUEST_MODEL(STRETCH)
			REQUEST_VEHICLE_ASSET(STRETCH)
			REQUEST_MODEL(IG_JIMMYDISANTO)
			REQUEST_ADDITIONAL_TEXT("SOL5", MISSION_TEXT_SLOT)
			REQUEST_VEHICLE_RECORDING(1, "MIC4")	
			REQUEST_MODEL(S_M_M_MovPrem_01)			
		BREAK
		
		CASE STAGE_GET_TO_MOVIE_PREMIERE
			REQUEST_ANIM_DICT("missmic4jimmy_limo")
			REQUEST_MODEL(Prop_Champ_01b)
			REQUEST_PTFX_ASSET()
			REQUEST_MODEL(PROP_CRATE_01A)
		BREAK

		CASE STAGE_MOVIE_PREMIERE_CUT
			REQUEST_ANIM_DICT("missmic4premiere")
		
			REQUEST_MODEL(IG_LAZLOW)
			REQUEST_MODEL(IG_JIMMYDISANTO)
			REQUEST_MODEL(U_M_Y_ANTONB)
			REQUEST_MODEL(IG_Milton)	
			REQUEST_MODEL(S_F_Y_MovPrem_01)
			REQUEST_MODEL(A_M_M_Paparazzi_01)
			REQUEST_MODEL(S_M_Y_Grip_01)
			REQUEST_MODEL(S_M_M_MovPrem_01)
			
			REQUEST_MODEL(Prop_V_Cam_01)
			REQUEST_MODEL(P_ING_MICROPHONEL_01)
			REQUEST_MODEL(prop_pap_camera_01)
			REQUEST_MODEL(STRETCH)
			REQUEST_MODEL(VACCA)
			
			REQUEST_MODEL(PROP_PHONE_ING_02)
			
			REQUEST_PTFX_ASSET()
			REQUEST_AMBIENT_AUDIO_BANK("Michael_4_Fireworks")
//			sFormBreakingNewsOverlay = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
		
		BREAK
	
		CASE STAGE_GET_TO_MICHAELS_HOUSE
			REQUEST_MODEL(IG_JIMMYDISANTO)
			REQUEST_MODEL(STRETCH)
			REQUEST_MODEL(VACCA)
			REQUEST_ANIM_DICT("missmic4ig_4")
			REQUEST_ANIM_SET("move_characters@jimmy@nervous@")
			REQUEST_ANIM_DICT("missmic4ig_4")	
			REQUEST_MODEL(A_M_M_Paparazzi_01)
			REQUEST_MODEL(prop_pap_camera_01)
			REQUEST_ANIM_DICT("missmic4premiere")
		BREAK

		CASE STAGE_SAVE_AMANDA
			REQUEST_MODEL(MESA3)
			REQUEST_MODEL(IG_AMANDATOWNLEY)
			REQUEST_MODEL(PROP_CS_AMANDA_SHOE)
			REQUEST_MODEL(mnGoonModel)
			REQUEST_MODEL(mnGoonModel2)
			REQUEST_WEAPON_ASSET(MichaelsWeapon)
	
			REQUEST_ANIM_DICT("misssolomon_5@stairs")
			REQUEST_NPC_VEH_MODEL(CHAR_TRACEY)
			//intMichaelsHouse = GET_INTERIOR_AT_COORDS(<< -824.2127, 182.2403, 70.7416 >>)
			//PIN_INTERIOR_IN_MEMORY(intMichaelsHouse)	
			//NEW_LOAD_SCENE_START_SPHERE(<<-809.8958, 179.9232, 73.9007>>, 1.0)
			//iNewLoadSceneTimer = GET_GAME_TIMER()
		BREAK
		
		CASE STAGE_SAVE_TRACEY
			REQUEST_MODEL(IG_TRACYDISANTO)
			REQUEST_MODEL(IG_AMANDATOWNLEY)
			REQUEST_MODEL(mnGoonModel)
			REQUEST_MODEL(mnGoonModel2)
			REQUEST_WEAPON_ASSET(MichaelsWeapon)
			REQUEST_ANIM_DICT("misssolomon_5@bedroom")
			REQUEST_ANIM_DICT("misssolomon_5@stairs")
			intMichaelsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.350, 172.900, 75.700 >>, "V_Michael")
			PIN_INTERIOR_IN_MEMORY(intMichaelsHouse)	
			
			//REQUEST_CUTSCENE("SOL_5_MCS_1")
			
		BREAK
		
		CASE STAGE_TRACEY_CAPTOR_SHOT
			REQUEST_MODEL(mnGoonModel)
			REQUEST_MODEL(mnGoonModel2)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
		//	REQUEST_MODEL(BUZZARD)
			REQUEST_VEHICLE_RECORDING(1, "Sol5")
			//REQUEST_RAPPEL_ASSETS()
			REQUEST_CUTSCENE("SOL_5_MCS_1")
		BREAK
		
		CASE STAGE_FIGHT_INCOMING_BAD_GUYS
			SET_MAX_WANTED_LEVEL(0)
			
			REQUEST_MODEL(IG_TRACYDISANTO)
			REQUEST_MODEL(IG_AMANDATOWNLEY)
			
			//Temp fix until this is sorted in code (needs to requested frame before the check)
			REQUEST_MODEL(mnGoonModel)
			REQUEST_MODEL(mnGoonModel2)
			
			REQUEST_MODEL(PROP_LD_TEST_01)
			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			
			REQUEST_VEHICLE_RECORDING(1, "Sol5")			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
			REQUEST_MODEL(MESA3)
			REQUEST_VEHICLE_RECORDING(3, "Sol5MW")
			REQUEST_VEHICLE_RECORDING(4, "Sol5MW")
			intMichaelsHouse = GET_INTERIOR_AT_COORDS(<< -824.2127, 182.2403, 70.7416 >>)
			
			PIN_INTERIOR_IN_MEMORY(intMichaelsHouse)	
		BREAK
		
		CASE STAGE_REUNITE_WITH_FAMILY
			SET_MAX_WANTED_LEVEL(0)
			//Temp fix until this is sorted in code (needs to requested frame before the check)
			REQUEST_MODEL(mnGoonModel)
			REQUEST_MODEL(mnGoonModel2)
		//	intMichaelsHouse = GET_INTERIOR_AT_COORDS(<< -824.2127, 182.2403, 70.7416 >>)
			//PIN_INTERIOR_IN_MEMORY(intMichaelsHouse)	
		BREAK
		
		CASE STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
			SET_MAX_WANTED_LEVEL(0)
			REQUEST_CUTSCENE("SOL_5_MCS_2")
//			REQUEST_MODEL(IG_TRACYDISANTO)
//			REQUEST_MODEL(IG_AMANDATOWNLEY)
//			REQUEST_MODEL(IG_JIMMYDISANTO)
			//REQUEST_ANIM_DICT("misssolomon_5@End")
//			intMichaelsHouse = GET_INTERIOR_AT_COORDS(<< -824.2127, 182.2403, 70.7416 >>)
//			PIN_INTERIOR_IN_MEMORY(intMichaelsHouse)	
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL HAVE_STAGE_ASSETS_LOADED(MISSION_STAGE_FLAG stageAssetsToLoad)

	PRINTLN("MICHAEL 4: checking assets have loaded: ", ENUM_TO_INT(stageAssetsToLoad))

	SWITCH stageAssetsToLoad

		CASE STAGE_PICKUP_JIMMY
			IF HAS_MODEL_LOADED(STRETCH)
			AND HAS_VEHICLE_ASSET_LOADED(STRETCH)
			AND HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "MIC4")			
			AND HAS_MODEL_LOADED(S_M_M_MovPrem_01)
				RETURN TRUE
			ENDIF			
		BREAK
		
		CASE STAGE_GET_TO_MOVIE_PREMIERE
			IF HAS_ANIM_DICT_LOADED("missmic4jimmy_limo")
			AND HAS_MODEL_LOADED(Prop_Champ_01b)
			AND HAS_PTFX_ASSET_LOADED()
			AND HAS_MODEL_LOADED(PROP_CRATE_01A)
				RETURN TRUE
			ENDIF
		BREAK

		CASE STAGE_MOVIE_PREMIERE_CUT		
			IF HAS_ANIM_DICT_LOADED("missmic4premiere")
			AND HAS_MODEL_LOADED(IG_LAZLOW)
			AND HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			AND HAS_MODEL_LOADED(U_M_Y_ANTONB)
			AND HAS_MODEL_LOADED(IG_Milton)
			AND HAS_MODEL_LOADED(S_F_Y_MovPrem_01)
			AND HAS_MODEL_LOADED(A_M_M_Paparazzi_01)
			AND HAS_MODEL_LOADED(S_M_Y_Grip_01)
			AND HAS_MODEL_LOADED(Prop_V_Cam_01)
			AND HAS_MODEL_LOADED(P_ING_MICROPHONEL_01)
			AND HAS_MODEL_LOADED(prop_pap_camera_01)
			AND HAS_MODEL_LOADED(STRETCH)
			AND HAS_MODEL_LOADED(VACCA)
			AND HAS_MODEL_LOADED(S_M_M_MovPrem_01)
			AND HAS_MODEL_LOADED(PROP_PHONE_ING_02)
			AND HAS_PTFX_ASSET_LOADED()
			AND REQUEST_AMBIENT_AUDIO_BANK("Michael_4_Fireworks")
//			AND HAS_SCALEFORM_MOVIE_LOADED(sFormBreakingNewsOverlay)
				RETURN TRUE
			ENDIF
		BREAK
			
		CASE STAGE_GET_TO_MICHAELS_HOUSE
			IF HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			AND HAS_MODEL_LOADED(STRETCH)
			AND HAS_MODEL_LOADED(VACCA)
			AND HAS_ANIM_DICT_LOADED("missmic4ig_4")
			AND HAS_ANIM_SET_LOADED("move_characters@jimmy@nervous@")
			AND HAS_MODEL_LOADED(A_M_M_Paparazzi_01)
			AND HAS_MODEL_LOADED(prop_pap_camera_01)
			AND HAS_ANIM_DICT_LOADED("missmic4premiere")
				RETURN TRUE
			ENDIF
		BREAK

		CASE STAGE_SAVE_AMANDA
			IF HAS_MODEL_LOADED(MESA3)
			AND HAS_MODEL_LOADED(IG_AMANDATOWNLEY)
			AND HAS_MODEL_LOADED(PROP_CS_AMANDA_SHOE)
			AND HAS_MODEL_LOADED(mnGoonModel)
			AND HAS_MODEL_LOADED(mnGoonModel2)
			AND HAS_WEAPON_ASSET_LOADED(MichaelsWeapon)
			AND HAS_ANIM_DICT_LOADED("misssolomon_5@stairs")
			AND HAS_NPC_VEH_MODEL_LOADED(CHAR_TRACEY)
			//AND IS_INTERIOR_READY(intMichaelsHouse)
				//IF IS_NEW_LOAD_SCENE_LOADED()	
				//OR GET_GAME_TIMER() - iNewLoadSceneTimer > 15000
					//NEW_LOAD_SCENE_STOP()
					RETURN TRUE
				//ENDIF
			ENDIF
		BREAK
		
		CASE STAGE_SAVE_TRACEY
			IF  HAS_MODEL_LOADED(IG_TRACYDISANTO)
			AND HAS_MODEL_LOADED(IG_AMANDATOWNLEY)
			AND HAS_MODEL_LOADED(mnGoonModel)
			AND HAS_MODEL_LOADED(mnGoonModel2)
			AND HAS_WEAPON_ASSET_LOADED(MichaelsWeapon)
			AND HAS_ANIM_DICT_LOADED("misssolomon_5@stairs")
			AND HAS_ANIM_DICT_LOADED("misssolomon_5@bedroom")
			//AND IS_INTERIOR_READY(intMichaelsHouse)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE STAGE_TRACEY_CAPTOR_SHOT
			//Temp fix until this is sorted in code (needs to requested frame before the check)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			IF HAS_MODEL_LOADED(mnGoonModel)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Sol5")
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)
			AND HAS_CUTSCENE_LOADED()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE STAGE_FIGHT_INCOMING_BAD_GUYS
			//Temp fix until this is sorted in code (needs to requested frame before the check)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			IF HAS_MODEL_LOADED(mnGoonModel)
			AND HAS_MODEL_LOADED(mnGoonModel2)
			AND HAS_MODEL_LOADED(IG_TRACYDISANTO)
			AND HAS_MODEL_LOADED(IG_AMANDATOWNLEY)
			AND HAS_MODEL_LOADED(PROP_LD_TEST_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Sol5")
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)			
			AND HAS_MODEL_LOADED(MESA3)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(3, "Sol5MW")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4, "Sol5MW")
			AND IS_INTERIOR_READY(intMichaelsHouse)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
				RETURN TRUE
			ENDIF
		BREAK
	
		CASE STAGE_REUNITE_WITH_FAMILY
			
			IF HAS_MODEL_LOADED(mnGoonModel)
			AND HAS_MODEL_LOADED(mnGoonModel2)
			//AND IS_INTERIOR_READY(intMichaelsHouse)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,0), 0, 1) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,2), 1, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,3), 7, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,5), 0, 0) //(hand)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,0), 0, 1) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,2), 2, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,3), 2, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,5), 0, 0) //(hand)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,6), 2, 0) //(feet)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,8), 1, 0) //(accs)
	
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,3), 3, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,5), 1, 0) //(hand)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,6), 0, 0) //(feet)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,8), 2, 0) //(accs)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,9), 1, 0) //(task)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,10), 0, 0)//(decl)
			SET_CUTSCENE_PED_PROP_VARIATION("Jimmy", INT_TO_ENUM(PED_PROP_POSITION,0), 0)
			
			IF HAS_CUTSCENE_LOADED()
//			AND HAS_MODEL_LOADED(IG_TRACYDISANTO)
//			AND HAS_MODEL_LOADED(IG_AMANDATOWNLEY)
//			AND HAS_MODEL_LOADED(IG_JIMMYDISANTO)
			//AND HAS_ANIM_DICT_LOADED("misssolomon_5@End")
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC

structTimelapse sTimelapse
//BOOL bRequestCutInTimeLapse
VECTOR vTtlScenePos
VECTOR vTtlSceneRot
INT sceneIdTL
INT iTimeLapseStage

BOOL bTimeLapseComplete

FUNC BOOL timeLapseCutscene()

	IF bTimeLapseComplete
		RETURN TRUE
	ENDIF	

	IF NOT IS_REPLAY_IN_PROGRESS()
	
			
		IF DO_TIMELAPSE(SP_MISSION_MICHAEL_4, sTimelapse, IS_REPEAT_PLAY_ACTIVE(), FALSE, FALSE, FALSE, TRUE)
			//mission_stage = STAGE_PICKUP_JIMMY
			//CLEAR_PED_TASKS(PLAYER_PED_ID())
			IF iTimeLapseStage = 0
				iTimeLapseStage = -1	
			ENDIF
		ENDIF	
		
		IF IS_SCREEN_FADED_OUT()//iTimeLapseStage = 0
		OR iTimeLapseStage = -1
			//SCRIPT_ASSERT("boom!")
			REQUEST_ANIM_DICT("missmic4")
			iTimeLapseStage = -1
			RETURN TRUE
		ENDIF
	
		SWITCH iTimeLapseStage
		
			CASE 0
				REQUEST_ANIM_DICT("SWITCH@MICHAEL@EXITS_FANCYSHOP")
				REQUEST_ANIM_DICT("missmic4")
				REQUEST_STAGE_ASSETS(STAGE_PICKUP_JIMMY)
				iTimeLapseStage++
			BREAK
		
			CASE 1
				IF HAS_ANIM_DICT_LOADED("SWITCH@MICHAEL@EXITS_FANCYSHOP")
				AND HAS_ANIM_DICT_LOADED("missmic4")
								
					/* START SYNCHRONIZED SCENE -  */
					vTtlScenePos = <<-715.500, -155.450, 37.410>>
					vTtlSceneRot = <<0.000, 0.000, 120.240>>

					sceneIdTL = CREATE_SYNCHRONIZED_SCENE(vTtlScenePos, vTtlSceneRot)
					
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-716.675,-155.420,37.675>>, 1.0, v_ilev_ch_glassdoor, sceneIdTL, "001405_01_mics3_8_exits_fancyshop_exit_l_door", "switch@michael@exits_fancyshop", INSTANT_BLEND_IN)
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-715.615,-157.256,37.675>>, 1.0, v_ilev_ch_glassdoor, sceneIdTL, "001405_01_mics3_8_exits_fancyshop_exit_r_door", "switch@michael@exits_fancyshop", INSTANT_BLEND_IN)
					
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIdTL, TRUE)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIdTL, 1.0)
		//			
					
					SETTIMERA(0)
					iTimeLapseStage++	
				ENDIF
			BREAK
		
		
			CASE 2
				SET_MICHAELS_TUX()
						
				IF TIMERA() > 6000			
							
					//v_ilev_ch_glassdoor (in 'int_lev_des.rpf') at -716.675,-155.420,37.675
					
					//v_ilev_ch_glassdoor (in 'int_lev_des.rpf') at -715.615,-157.256,37.675
					
					/* START SYNCHRONIZED SCENE -  */
					vTtlScenePos = <<-715.500, -155.450, 37.410>>
					vTtlSceneRot = <<0.000, 0.000, 120.240>>
					
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)			

					sceneIdTL = CREATE_SYNCHRONIZED_SCENE(vTtlScenePos, vTtlSceneRot)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
							
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneIdTL, "switch@michael@exits_fancyshop", "001405_01_mics3_8_exits_fancyshop_exit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-716.675,-155.420,37.675>>, 1.0, v_ilev_ch_glassdoor, sceneIdTL, "001405_01_mics3_8_exits_fancyshop_exit_l_door", "switch@michael@exits_fancyshop", INSTANT_BLEND_IN)
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-715.615,-157.256,37.675>>, 1.0, v_ilev_ch_glassdoor, sceneIdTL, "001405_01_mics3_8_exits_fancyshop_exit_r_door", "switch@michael@exits_fancyshop", INSTANT_BLEND_IN)
	//				
	//				CLEAR_ROOM_FOR_GAME_VIEWPORT()
	//				CLEAR_ROOM_FOR_ENTITY(PLAYER_PED_ID())				
					SETTIMERA(0)
					iTimeLapseStage++
				ENDIF
			BREAK
			
			CASE 3
			
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdTL) >= 0.95
				//IF TIMERA() > 7000	
					STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-716.675,-155.420,37.675>>, 1.0, v_ilev_ch_glassdoor, INSTANT_BLEND_IN)
					STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-715.615,-157.256,37.675>>, 1.0, v_ilev_ch_glassdoor, INSTANT_BLEND_IN)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-717.9023, -157.4854, 35.9895>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 163.1339)
					
					
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "missmic4", "michael_tux_fidget", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
					RESET_GAME_CAMERA()
					bTimeLapseComplete = TRUE
					iTimeLapseStage++
					
					RETURN TRUE
					
				ENDIF
			BREAK
						
		ENDSWITCH					
		
					
	ELSE
		RETURN TRUE
		//mission_stage = STAGE_PICKUP_JIMMY
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VEHICLE_INDEX CREATE_STRETCH()

	VEHICLE_INDEX tempIndex
	
	tempIndex = CREATE_VEHICLE(STRETCH, << 293.6189, 176.4069, 103.0985 >>, 69.5405)
	SET_VEHICLE_COLOURS(tempIndex, 0, 0)
	SET_VEHICLE_NUMBER_PLATE_TEXT(tempIndex, "V1N3W88D")	
	SET_VEHICLE_HAS_STRONG_AXLES(tempIndex, TRUE)
	FORCE_USE_AUDIO_GAME_OBJECT(tempIndex, "STRETCH_MICHAEL_4")
	SET_VEHICLE_EXTRA(tempIndex, 1, TRUE)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(tempIndex, FALSE)
	SET_VEH_RADIO_STATION(tempIndex, "OFF")
	
	RETURN tempIndex

ENDFUNC

FUNC VEHICLE_INDEX CREATE_SPORTS_CAR()
	
	VEHICLE_INDEX tempIndex
	
	tempIndex = CREATE_VEHICLE(VACCA, <<300.9331, 172.7484, 102.9464>>, 65.7440)
	SET_VEHICLE_HAS_STRONG_AXLES(tempIndex, TRUE)
	SET_VEHICLE_COLOURS(tempIndex, 28, 28)
	SET_VEHICLE_NUMBER_PLATE_TEXT(tempIndex, "JKW87N1D")
	SET_VEHICLE_AS_NO_LONGER_NEEDED(tempIndex)
		
	RETURN tempIndex
				
ENDFUNC

INT iCreateCars

/// PURPOSE: Creats the stretch and Vacca
FUNC BOOL CREATE_RED_CARPET_CARS_IF_DONT_EXIST()

	PRINTLN("CREATE_RED_CARPET_CARS_IF_DONT_EXIST", iCreateCars)

	SWITCH iCreateCars
	
		CASE 0
			REQUEST_MODEL(STRETCH)
			REQUEST_MODEL(VACCA)
			iCreateCars++
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(STRETCH)
			AND HAS_MODEL_LOADED(VACCA)
				iCreateCars++
			ENDIF
		BREAK
		
		CASE 2
			//IF NOT DOES_ENTITY_EXIST(carMichael)
				DELETE_VEHICLE(carMichael)
				carMichael = CREATE_STRETCH()
			//ENDIF
			IF NOT DOES_ENTITY_EXIST(carMichael2)
				carMichael2 = CREATE_SPORTS_CAR()
			ENDIF
			iCreateCars++
			RETURN TRUE
		BREAK
		
		CASE 3
			RETURN TRUE
		BREAK
		
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC


PROC SET_JIMMYS_TUX()

	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,1), 1, 0, 0) //(berd)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,10), 2, 0, 0) //(decl)
	
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedJImmy, "MICHAEL_4_JIMMY_GROUP")

	STOP_PED_SPEAKING(pedJImmy, TRUE)
	
ENDPROC


PROC SET_JIMMYS_ARMY_OUTFIT()

	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,3), 3, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,4), 2, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,5), 1, 0, 0) //(hand)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,6), 2, 0, 0) //(feet)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
	//SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
	SET_PED_COMPONENT_VARIATION(pedJimmy, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
	SET_PED_PROP_INDEX(pedJimmy, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)

ENDPROC


PROC SET_PED_AS_FRIENDLY(PED_INDEX &thisPed, BOOL bAddtoRelGroup = TRUE)

	SET_PED_CAN_BE_DRAGGED_OUT(thisPed, FALSE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(thisPed, FALSE)
	IF bAddtoRelGroup
		SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, RELGROUPHASH_PLAYER)
	ELSE
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
	ENDIF
	SET_PED_SUFFERS_CRITICAL_HITS(thisPed, FALSE)
	SET_PED_CAN_BE_TARGETTED(thisPed, FALSE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(thisPed, TRUE)
	
ENDPROC

FUNC BOOL IS_ENTITY_IN_FRONT_GARDEN_AREA(ENTITY_INDEX thisEntity)
	RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-835.364258,183.372330,76.094894>>, <<-823.341248,149.514069,63.662308>>, 33.000000)
ENDFUNC

FUNC BOOL IS_ENTITY_IN_DINING_ROOM(ENTITY_INDEX thisEntity)
	RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-799.556213,179.541992,71.834709>>, <<-793.838989,181.512985,75.084709>>, 11.750000)
ENDFUNC

FUNC BOOL IS_ENTITY_OUTSIDE(ENTITY_INDEX thisEntity)
	RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-817.345337,175.497055,70.260384>>, <<-794.602661,184.461853,82.473396>>, 12.000000)
ENDFUNC

FUNC BOOL IS_ENTITY_ON_GARAGE_ROOF(ENTITY_INDEX thisEntity)
	RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-800.376709,163.645340,75.224098>>, <<-811.319092,160.409439,80.538040>>, 7.500000)
ENDFUNC

PROC SET_PED_AS_MERRYWEATHER(PED_INDEX &thisPed, WEAPON_TYPE givenWeapon = WEAPONTYPE_CARBINERIFLE)

	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(thisPed)

	SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, relGroupMerryWeather)
	GIVE_WEAPON_TO_PED(thisPed, givenWeapon, INFINITE_AMMO, TRUE, TRUE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(thisPed, TRUE)
	SET_PED_CAN_COWER_IN_COVER(thisPed, FALSE)
	SET_PED_CAN_PEEK_IN_COVER(thisPed, TRUE)
	SET_PED_COMBAT_MOVEMENT(thisPed, CM_DEFENSIVE)
	
	SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_USE_VEHICLE, FALSE)
	SET_PED_AS_ENEMY(thisPed, TRUE)
			
	SET_COMBAT_FLOAT(thisPed, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.5)
	//SET_COMBAT_FLOAT(thisCop, CCF_TIME_BETWEEN_PEEKS, 10.00)
	
	SET_PED_COMBAT_RANGE(thisPed, CR_NEAR)
	
	ADD_ARMOUR_TO_PED(thisPed, 25)
	
	SET_PED_TARGET_LOSS_RESPONSE(thisPed, TLR_NEVER_LOSE_TARGET)
	
	SET_COMBAT_FLOAT(thisPed, CCF_BURST_DURATION_IN_COVER, 5.00)
	SET_COMBAT_FLOAT(thisPed, CCF_MAX_SHOOTING_DISTANCE, 150.00)	
	SET_COMBAT_FLOAT(thisPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 0.75)
	
	
	SET_PED_PATH_CAN_USE_CLIMBOVERS(thisPed, FALSE)
	
	SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
	
	SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_CAN_FLANK, FALSE)
	
	
	SET_PED_CONFIG_FLAG(thisPed, PCF_DisableHurt, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(thisPed,CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH,TRUE)
	
	SET_PED_DIES_WHEN_INJURED(thisPed, TRUE)
	
	SET_PED_ACCURACY(thisPed, 1)
	
	SET_PED_CHANCE_OF_FIRING_BLANKS(thisPed, 0.1, 0.2) 
		
	IF NOT IS_ENTITY_OUTSIDE(thisPed)
		SET_PED_CHANCE_OF_FIRING_BLANKS(thisPed, 0.5, 0.5)
	ELSE
		ADD_ARMOUR_TO_PED(thisPed, 50)
	ENDIF
	//SET_PED_ARMOUR(thisPed, 0)
	
	IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
		SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0), 1, 2)
	ENDIF
	
	IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
		SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 1)
	ENDIF

ENDPROC


PROC CREATE_AMANDA()

	pedAmanda = CREATE_PED(PEDTYPE_MISSION, IG_AMANDATOWNLEY, << -812.4667, 177.3469, 71.1530 >>, 103.1597)
	SET_PED_AS_FRIENDLY(pedAmanda, FALSE)		
	SET_PED_COMPONENT_VARIATION(pedAmanda, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedAmanda, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedAmanda, INT_TO_ENUM(PED_COMPONENT,3), 7, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedAmanda, INT_TO_ENUM(PED_COMPONENT,4), 2, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedAmanda, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
	SET_PED_COMPONENT_VARIATION(pedAmanda, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
	
	SET_PED_COMPONENT_VARIATION(pedAmanda, PED_COMP_DECL, 1, 0, 0) //(teef)
	
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedAmanda, "AMANDA")
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAmanda, TRUE)

	SET_PED_AS_FRIENDLY(pedAmanda, FALSE)
	SET_PED_CONFIG_FLAG(pedAmanda, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(pedAmanda, PCF_DisableExplosionReactions, TRUE)
	SET_PED_CONFIG_FLAG(pedAmanda, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(pedAmanda, PCF_DisableExplosionReactions, TRUE)
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedAmanda, "AMANDA")
	
ENDPROC

PROC CREATE_TRACY()
						
	pedTracey = CREATE_PED(PEDTYPE_MISSION, IG_TRACYDISANTO,  << -803.350, 172.900, 75.700 >>, 55.000)
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedTracey, "TRACEY")
	SET_PED_AS_FRIENDLY(pedTracey, FALSE)			
	SET_PED_SUFFERS_CRITICAL_HITS(pedTracey, FALSE)	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTracey, TRUE)
	
	SET_PED_CONFIG_FLAG(pedTracey, PCF_DisableExplosionReactions, TRUE)
	SET_PED_CONFIG_FLAG(pedTracey, PCF_RunFromFiresAndExplosions, FALSE)
	
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,4), 2, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,6), 2, 0, 0) //(feet)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
	SET_PED_COMPONENT_VARIATION(pedTracey, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)

ENDPROC

INT navBlockArea


CONST_INT MOVP_ANTON			0
CONST_INT MOVP_CAMMAN			1
CONST_INT MOVP_LAZLOW			2

CONST_INT MOVP_AELLA			3
CONST_INT MOVP_BENTON			4
CONST_INT MOVP_MOLLY			5
CONST_INT MOVP_RUBEN			6


CONST_INT MOVP_FAN_A			7
CONST_INT MOVP_PAP1				8
CONST_INT MOVP_PAP2				9
CONST_INT MOVP_PAP3				10
CONST_INT MOVP_STAR_A			11

CONST_INT MOVP_FEM_A			12
CONST_INT MOVP_MILTON			13
CONST_INT MOVP_PAP1_A			14
CONST_INT MOVP_PAP2_A			15
CONST_INT MOVP_PAP3_A			16
CONST_INT MOVP_EX_1				17
CONST_INT MOVP_EX_2				18
CONST_INT MOVP_EX_3				19
CONST_INT MOVP_PAPEX1			20
CONST_INT MOVP_PAPEX2			21
CONST_INT MOVP_PRODUCER			22
CONST_INT MOVP_EXTRAGIRL		23

CONST_INT MOVP_MAX_PEDS			24

PED_INDEX pedsMoviePremier[MOVP_MAX_PEDS]
OBJECT_INDEX oiRedCarpetObjects[13]

VEHICLE_INDEX carTracey

PICKUP_INDEX riflePickup
INT iriflePickupPlacementFlags

SEQUENCE_INDEX seqPhotographer

PROC TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(PED_INDEX thisPed, STRING thisDict, STRING thisAnim, VECTOR vPos, VECTOR vRot, FLOAT fBlendin, FLOAt fBlendOut, INT iTimeToPlay, ANIMATION_FLAGS afFlags, FLOAT fStartPhase)

	IF NOT IS_PED_INJURED(thisPed)
		TASK_PLAY_ANIM_ADVANCED(thisPed,  thisDict, thisAnim, vPos, vRot, fBlendin, fBlendOut, iTimeToPLay, afFlags, fStartPhase)
	ENDIF

ENDPROC

INT iCreateOverMultipleFrames

FUNC BOOL CREATE_STAGE_ASSETS(MISSION_STAGE_FLAG stageAssetsToLoad)

	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtCurentWeapon)

	PRINTLN("MICHAEL 4: CREATING STAGE ASSETS FOR STAGE: ", ENUM_TO_INT(stageAssetsToLoad))

	SWITCH stageAssetsToLoad
		
		CASE STAGE_PICKUP_JIMMY
			pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<-719.4904, -162.2617, 36.0158>>, 338.8036)
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedJimmy, "JIMMY")
			SET_JIMMYS_TUX()
			SET_PED_AS_FRIENDLY(pedJimmy)
			SET_PED_CONFIG_FLAG(pedJimmy, PCF_RunFromFiresAndExplosions, FALSE)	
			
			TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), INFINITE_TASK_TIME ,SLF_WHILE_NOT_IN_FOV)
			CLEAR_AREA_OF_VEHICLES(<<-730.7252, -144.1218, 36.2103>>, 10.0)
			carMichael = CREATE_STRETCH()
			SET_ENTITY_ROTATION(carMichael, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(1, 0.0, "MIC4"))
			SET_ENTITY_COORDS(carMichael, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 0.0, "MIC4"))
			SET_VEHICLE_ENGINE_ON(carMichael, TRUE, TRUE)
			SET_VEHICLE_LIGHTS(carMichael, SET_VEHICLE_LIGHTS_ON)
			SET_VEHICLE_RADIO_ENABLED(carMichael, FALSE)
			
			//FREEZE_ENTITY_POSITION(carMichael, TRUE)
			pedLimoDriver = CREATE_PED_INSIDE_VEHICLE(carMichael, PEDTYPE_MISSION, S_M_M_MovPrem_01)				
			SET_PED_CAN_BE_DRAGGED_OUT(pedLimoDriver, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLimoDriver, TRUE)	
			SET_PED_CONFIG_FLAG(pedLimoDriver, PCF_RunFromFiresAndExplosions, FALSE)		
		BREAK
		
		CASE STAGE_MOVIE_PREMIERE_CUT
		
			SWITCH iCreateOverMultipleFrames
			
				CASE 0
					//	SET_REDUCE_PED_MODEL_BUDGET(TRUE)
					SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
					SET_PED_POPULATION_BUDGET(1)
					SET_VEHICLE_POPULATION_BUDGET(1)
				
					//ADD_RELATIONSHIP_GROUP("Leadin", g_sTriggerSceneAssets.relGroup)
				
					g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
					SET_PED_NON_CREATION_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
					SET_ROADS_IN_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>, FALSE)
					CLEAR_AREA(<<297.8264, 191.4778, 103.3186>>, 20.0, TRUE)
						
					navBlockArea =  ADD_NAVMESH_BLOCKING_OBJECT(<<292.424530,172.560867,109.715904>>, <<61.000000,50.250000,8.500000>>, 0.0)
					
					REQUEST_IPL("redCarpet")
						
					//Lazlow interview Anton               
					pedsMoviePremier[MOVP_ANTON]  = CREATE_PED(PEDTYPE_MISSION, U_M_Y_ANTONB,<<300.5, 203.51, 103.4>>)
									
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_ANTON], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_ANTON], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_ANTON], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
		
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 1
					pedsMoviePremier[MOVP_CAMMAN] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Grip_01,<<300.5, 203.51, 103.4>>)
					//Camera men
					oiRedCarpetObjects[0] = CREATE_OBJECT(Prop_V_Cam_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[0],pedsMoviePremier[MOVP_CAMMAN], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_CAMMAN], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_CAMMAN], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_CAMMAN], INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_CAMMAN], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_CAMMAN], INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 2
					pedsMoviePremier[MOVP_LAZLOW] = CREATE_PED(PEDTYPE_MISSION, IG_LAZLOW,<<300.5, 203.51, 103.4>>)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					//SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,9), 0, 7, 0) //(task)
					//SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW], INT_TO_ENUM(PED_COMPONENT,10), 0, 7, 0) //(decl)
						
					//SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW],PED_COMP_SPECIAL2,0,7)// = ALL
					//Hair with no pony tail
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_LAZLOW],PED_COMP_HAIR,1,0)
			
					oiRedCarpetObjects[7] = CREATE_OBJECT(P_ING_MICROPHONEL_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[7],pedsMoviePremier[MOVP_LAZLOW], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_LAZLOW], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 3
					//4stars reacting to paps/crowd
					pedsMoviePremier[MOVP_AELLA]  = CREATE_PED(PEDTYPE_MISSION, S_F_Y_MovPrem_01,<<300.5, 203.51, 103.4>>)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(pedsMoviePremier[MOVP_AELLA], "mood_happy_1")
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 4
					pedsMoviePremier[MOVP_BENTON] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_MovPrem_01,<<300.5, 203.51, 103.4>>)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(pedsMoviePremier[MOVP_BENTON], "mood_happy_1")
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 5
					pedsMoviePremier[MOVP_MOLLY]  = CREATE_PED(PEDTYPE_MISSION, S_F_Y_MovPrem_01,<<300.5, 203.51, 103.4>>)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(pedsMoviePremier[MOVP_MOLLY], "mood_happy_1")
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 6
					pedsMoviePremier[MOVP_RUBEN]  = CREATE_PED(PEDTYPE_MISSION, S_F_Y_MovPrem_01,<<300.5, 203.51, 103.4>>)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(pedsMoviePremier[MOVP_RUBEN], "mood_happy_1")
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 7
					//Actress posing
					pedsMoviePremier[MOVP_FAN_A]  = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 8
					pedsMoviePremier[MOVP_PAP1]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
					//paps 1
					oiRedCarpetObjects[1] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[1],pedsMoviePremier[MOVP_PAP1], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 9
					pedsMoviePremier[MOVP_PAP2]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
					oiRedCarpetObjects[2] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[2],pedsMoviePremier[MOVP_PAP2], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 10
					pedsMoviePremier[MOVP_PAP3]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
					oiRedCarpetObjects[3] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[3],pedsMoviePremier[MOVP_PAP3], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP3], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 11
					pedsMoviePremier[MOVP_STAR_A] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_MovPrem_01,<<300.5, 203.51, 103.4>>)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 12
					pedsMoviePremier[MOVP_PAPEX1]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<289.7286, 181.3276, 103.3649>>, 248.7409)
					
					oiRedCarpetObjects[8] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[8],pedsMoviePremier[MOVP_PAPEX1], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAPEX1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
					TASK_PLAY_ANIM(pedsMoviePremier[MOVP_PAPEX1], "missmic4premiere", "Prem_Milton_Pap1_A",
														INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
														AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING, 0.462)			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 13
					pedsMoviePremier[MOVP_PAPEX2]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<290.0628, 183.0464, 103.3724>>, 244.2059)
				
					oiRedCarpetObjects[9] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[9],pedsMoviePremier[MOVP_PAPEX2], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAPEX2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
			
//					oiRedCarpetObjects[11] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[11],pedsMoviePremier[MOVP_PAP1_A], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAPEX2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//			
			
					TASK_PLAY_ANIM(pedsMoviePremier[MOVP_PAPEX2], "missmic4premiere", "prem_actress_pap1_a",
																	INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																	AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING, 0.462)
			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 14
					//Milton posing
					pedsMoviePremier[MOVP_FEM_A]  = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Grip_01,<<300.5, 203.51, 103.4>>)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 15
					pedsMoviePremier[MOVP_MILTON] = CREATE_PED(PEDTYPE_MISSION, IG_Milton,<<300.5, 203.51, 103.4>>)
						
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(pedsMoviePremier[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
				
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 16
					pedsMoviePremier[MOVP_PAP1_A] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<299.0112, 182.2089, 103.1925>>, 136.6234)
					//paps 2
					oiRedCarpetObjects[4] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[4],pedsMoviePremier[MOVP_PAP1_A], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP1_A], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					
				 	TASK_PLAY_ANIM(pedsMoviePremier[MOVP_PAP1_A], "missmic4premiere", "Prem_Milton_Pap1_A",
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																			AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING, 0.462)
					
		
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 17
					pedsMoviePremier[MOVP_PAP2_A] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
					oiRedCarpetObjects[5] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[5],pedsMoviePremier[MOVP_PAP2_A], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP2_A], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 18
					pedsMoviePremier[MOVP_PAP3_A] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
					oiRedCarpetObjects[6] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[6],pedsMoviePremier[MOVP_PAP3_A], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP3_A], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 19
					pedsMoviePremier[MOVP_EX_1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<302.6787, 190.2461, 103.0710>>, 81.0002)
					TASK_PLAY_ANIM(pedsMoviePremier[MOVP_EX_1], "missmic4premiere", "crowd_a_idle_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 20
					pedsMoviePremier[MOVP_EX_2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Grip_01, <<305.6718, 194.9252, 103.1558>>, 100.4348)
					TASK_PLAY_ANIM(pedsMoviePremier[MOVP_EX_2], "missmic4premiere", "crowd_b_idle_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 21
					pedsMoviePremier[MOVP_EX_3] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.6657, 180.2628, 102.9>>, 90.7576)
					//TASK_PLAY_ANIM(pedsMoviePremier[MOVP_EX_3], "missmic4premiere", "crowd_c_idle_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					oiRedCarpetObjects[10] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[10],pedsMoviePremier[MOVP_EX_3], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_EX_3], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
					
					OPEN_SEQUENCE_TASK(seqPhotographer)
						TASK_PLAY_ANIM(NULL, "missmic4premiere", "PAP_IDLE_ACTION_01",
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																			AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						TASK_PLAY_ANIM(NULL, "missmic4premiere", "PAP_IDLE_ACTION_02",
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																			AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						TASK_PLAY_ANIM(NULL, "missmic4premiere", "PAP_IDLE_ACTION_02",
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																			AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						TASK_PLAY_ANIM(NULL, "missmic4premiere", "PAP_IDLE_ACTION_02",
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																			AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
																			
						SET_SEQUENCE_TO_REPEAT(seqPhotographer, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seqPhotographer)
								
					TASK_PERFORM_SEQUENCE(pedsMoviePremier[MOVP_EX_3], seqPhotographer)
			
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 22
					pedsMoviePremier[MOVP_PRODUCER] = CREATE_PED(PEDTYPE_MISSION, S_M_M_MovPrem_01,<<297.0116, 180.6192, 103.2445>>, 127.4307)
					TASK_PLAY_ANIM(pedsMoviePremier[MOVP_PRODUCER], "missmic4premiere", "prem_producer_argue_a",
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																			AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_USE_MOVER_EXTRACTION)
					
					oiRedCarpetObjects[12] = CREATE_OBJECT(PROP_PHONE_ING_02, <<300.5, 203.51, 103.4>>)
					ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[12],pedsMoviePremier[MOVP_PRODUCER], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PRODUCER], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 23
					pedsMoviePremier[MOVP_EXTRAGIRL] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_MovPrem_01,<<292.7951, 181.5232, 103.3154>>, 88.9302)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(pedsMoviePremier[MOVP_EXTRAGIRL], "mood_happy_1")
					TASK_PLAY_ANIM(pedsMoviePremier[MOVP_EXTRAGIRL], "missmic4premiere", "Prem_4stars_A_Molly",
																	INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
																	AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING)
		
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 24
					
					iCreateOverMultipleFrames++
				BREAK
				
				CASE 25
					//Lazlow interview Anton  
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_ANTON], "missmic4premiere", "Interview_Short_Anton", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_CAMMAN], "missmic4premiere", "Interview_Short_Camman", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_LAZLOW], "missmic4premiere", "Interview_Short_Lazlow", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
				
				
					
					//4stars reacting to paps/crowd
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_AELLA], "missmic4premiere", "Prem_4stars_A_Aella", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_BENTON], "missmic4premiere", "Prem_4stars_A_Benton", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_MOLLY], "missmic4premiere", "Prem_4stars_A_Molly", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_RUBEN], "missmic4premiere", "Prem_4stars_A_Ruben", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
				
				
					//Actress posing
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_FAN_A], "missmic4premiere", "Prem_Actress_fan_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_PAP1], "missmic4premiere", "Prem_Actress_pap1_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_PAP2], "missmic4premiere", "Prem_Actress_pap2_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_PAP3], "missmic4premiere", "Prem_Actress_pap3_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_STAR_A], "missmic4premiere", "Prem_Actress_star_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					
					
					//Milton posing
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_FEM_A], "missmic4premiere", "Prem_Milton_Fem_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_MILTON], "missmic4premiere", "Prem_Milton_Milton_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
				                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
		//			TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_PAP1_A], "missmic4premiere", "Prem_Milton_Pap1_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
		//																	INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
		//		                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_PAP2_A], "missmic4premiere", "Prem_Milton_Pap2_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					TASK_PLAY_ANIM_ADVANCED_WITH_DEATH_CHECK(pedsMoviePremier[MOVP_PAP3_A], "missmic4premiere", "Prem_Milton_Pap3_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
																			INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
				                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
					
					
					INT i
					FOR i = 0 TO MOVP_MAX_PEDS - 1
						//SET_+(pedsMoviePremier[i], g_sTriggerSceneAssets.relGroup)
						IF NOT IS_ENTITY_DEAD(pedsMoviePremier[i])
							SET_PED_AS_FRIENDLY(pedsMoviePremier[i], TRUE)
							
							IF i <> MOVP_PRODUCER
								FREEZE_ENTITY_POSITION(pedsMoviePremier[i], TRUE)
							ENDIF
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsMoviePremier[i])
												
							#IF IS_DEBUG_BUILD
								debugPedName = "RedC:"
								debugPedName += i
								SET_PED_NAME_DEBUG(pedsMoviePremier[i], debugPedName)
							#ENDIF
						
						ENDIF
						
					ENDFOR
				
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, pedsMoviePremier[MOVP_PRODUCER], "PRODUCER")
				
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedsMoviePremier[MOVP_LAZLOW], "LAZLOW")
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, pedsMoviePremier[MOVP_ANTON], "ANTON")
					
					iCreateOverMultipleFrames = 0 //Reset for next time
					
					RETURN TRUE
					
				BREAK
				
			ENDSWITCH
			
			//Adjusted paps positions			
				
			RETURN FALSE
		
		BREAK
		
		CASE STAGE_GET_TO_MICHAELS_HOUSE
			carMichael = CREATE_STRETCH()
						
			pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, << 303.5163, 201.2443, 103.3570 >>, 158.4110)
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedJimmy, "JIMMY")
			SET_JIMMYS_TUX()
			SET_PED_AS_FRIENDLY(pedJimmy)
					
//			IF wtCurentWeapon = WEAPONTYPE_UNARMED
//				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), MichaelsWeapon, 1000, FALSE, FALSE)
//				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), FALSE)
//			ENDIF
			
			SET_MAX_WANTED_LEVEL(5)
			
		BREAK
		
		CASE STAGE_SAVE_AMANDA
		
			//CREATE_OBJECT( PROP_ , << -812.4667, 177.3469, 71.1530 >>)
		
			badguyCarOutsideHouse1 = CREATE_VEHICLE(MESA3, << -824.2127, 182.2403, 70.7416 >>, 340.1401)
			badguyCarOutsideHouse2 = CREATE_VEHICLE(MESA3, <<-818.2394, 185.0430, 71.2968>>, 311.4450)
			
//			SET_VEHICLE_DOOR_CONTROL(badguyCarOutsideHouse1, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.7)
//			SET_VEHICLE_DOOR_CONTROL(badguyCarOutsideHouse1, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 1.0)
			
			SET_VEHICLE_DOOR_OPEN(badguyCarOutsideHouse1, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
			SET_VEHICLE_DOOR_OPEN(badguyCarOutsideHouse1, SC_DOOR_FRONT_RIGHT, FALSE, TRUE)
			
			SET_VEHICLE_COLOURS(badguyCarOutsideHouse1, 0, 0)
			SET_VEHICLE_COLOURS(badguyCarOutsideHouse2, 0, 0)
			
			SET_VEHICLE_ON_GROUND_PROPERLY(badguyCarOutsideHouse1)
			SET_VEHICLE_ON_GROUND_PROPERLY(badguyCarOutsideHouse2)
			
			vAmandaScenePos = <<-807.239, 182.773, 74.000>>
			vAmandaSceneRot = <<0.0, 0.0, 20.0>>
			
			CREATE_AMANDA()
			
			oiAmandasShoe = CREATE_OBJECT(PROP_CS_AMANDA_SHOE, GET_ENTITY_COORDS(pedAmanda))
			ATTACH_ENTITY_TO_ENTITY(oiAmandasShoe, pedAmanda, GET_PED_BONE_INDEX(pedAmanda, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//			
			pedHostagetakerAmanda = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  << -809.6262, 179.8829, 71.1530 >>, 46.4666)
			GIVE_WEAPON_TO_PED(pedHostagetakerAmanda, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHostagetakerAmanda, TRUE)
			SET_PED_SUFFERS_CRITICAL_HITS(pedHostagetakerAmanda, FALSE)
			SET_ENTITY_HEALTH(pedHostagetakerAmanda, 1000)
		
			SET_PED_ARMOUR(pedHostagetakerAmanda, 0)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedHostagetakerAmanda, relGroupMerryWeather)
					
			SET_PED_CONFIG_FLAG(pedHostagetakerAmanda, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(pedHostagetakerAmanda, PCF_DisableExplosionReactions, TRUE)
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedHostagetakerAmanda, "MERRYWEATHER1")
			
			
			/* START SYNCHRONIZED SCENE -  */							
			iSceneId = CREATE_SYNCHRONIZED_SCENE(<<-807.239, 182.773, 74.000>>, <<0.0, 0.0, 20.0>>)
			
			TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_struggle_b_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
			TASK_SYNCHRONIZED_SCENE(pedHostagetakerAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_struggle_b_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, TRUE)

			//RETAIN_ENTITY_IN_INTERIOR(pedAmanda, intMichaelsHouse)
			//RETAIN_ENTITY_IN_INTERIOR(pedHostagetakerAmanda, intMichaelsHouse)
			
			IF wtCurentWeapon = WEAPONTYPE_UNARMED
				//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), MichaelsWeapon, 1000, TRUE, TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
			ENDIF
			SET_MAX_WANTED_LEVEL(0)
			
			//Stick Tracey's car nearby as cover.
			IF NOT DOES_ENTITY_EXIST(carTracey)
				CREATE_NPC_VEHICLE(carTracey, CHAR_TRACEY, <<-815.0861, 163.3637, 70.3941>>, 195.6004)
			ENDIF
			
			IF NOT DOES_PICKUP_EXIST(riflePickup)         
			AND NOT HAS_PED_GOT_FIREARM(PLAYER_PED_ID())
			
                  iriflePickupPlacementFlags = 0
                  SET_BIT(iriflePickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
                  SET_BIT(iriflePickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
                  SET_BIT(iriflePickupPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
                  riflePickup = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ASSAULTRIFLE, <<-819.0212, 179.4919, 71.5570>>,
                                                                        <<-13.2400, -84.9600, -61.2000>>, iriflePickupPlacementFlags)                                                                                       
																		
//					SET_ENTITY_COORDS_NO_OFFSET(entity, <<-819.0212, 179.4919, 71.5570>>)
//					SET_ENTITY_ROTATION(entity, <<-13.2400, -84.9600, -61.2000>>)
////					SET_ENTITY_QUATERNION(entity, -0.4147, -0.5341, -0.3059, 0.6702)
////
////					SET_ENTITY_COORDS_NO_OFFSET(entity, <<-819.0212, 179.4919, 71.5570>>)
////					SET_ENTITY_ROTATION(entity, <<-13.2400, -84.9600, -61.2000>>)
//					SET_ENTITY_QUATERNION(entity, -0.4147, -0.5341, -0.3059, 0.6702)
																		
            ENDIF

			
			
		BREAK
		
		CASE STAGE_SAVE_TRACEY
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carMichael)
			SET_MODEL_AS_NO_LONGER_NEEDED(STRETCH)
			
			CREATE_TRACY()
			
			pedHostagetakerTracey = CREATE_PED(PEDTYPE_MISSION, mnGoonModel,  << -803.350, 172.900, 75.700 >>, 55.000)
			GIVE_WEAPON_TO_PED(pedHostagetakerTracey, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHostagetakerTracey, TRUE)
						
			SET_PED_COMPONENT_VARIATION(pedHostagetakerTracey, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedHostagetakerTracey, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(pedHostagetakerTracey, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedHostagetakerTracey, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(pedHostagetakerTracey, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(pedHostagetakerTracey, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			SET_PED_PROP_INDEX(pedHostagetakerTracey, INT_TO_ENUM(PED_PROP_POSITION,0), 1, 2)
			//SET_PED_PROP_INDEX(pedHostagetakerTracey, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 1)
			
			SET_PED_CONFIG_FLAG(pedHostagetakerTracey, PCF_DisableExplosionReactions, TRUE)
			SET_PED_CONFIG_FLAG(pedHostagetakerTracey, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_ARMOUR(pedHostagetakerTracey, 0)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(pedHostagetakerTracey, relGroupMerryWeather)
		
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 5, pedHostagetakerTracey, "MERRYWEATHER2")
			SET_PED_SUFFERS_CRITICAL_HITS(pedHostagetakerTracey, FALSE)
			SET_ENTITY_HEALTH(pedHostagetakerTracey, 1000)
			
			intMichaelsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.350, 172.900, 75.700 >>, "V_Michael")
																
//			FORCE_ROOM_FOR_ENTITY(pedTracey, intMichaelsHouse, GET_ROOM_KEY_FROM_ENTITY(pedTracey))
//			RETAIN_ENTITY_IN_INTERIOR(pedTracey, intMichaelsHouse)
//						
//			FORCE_ROOM_FOR_ENTITY(pedHostagetakerTracey, intMichaelsHouse, GET_ROOM_KEY_FROM_ENTITY(pedHostagetakerTracey))
//			RETAIN_ENTITY_IN_INTERIOR(pedHostagetakerTracey, intMichaelsHouse)
//					
			
			//TASK_PLAY_ANIM_ADVANCED(pedTracey, "missprologueig_4@hold_head_base", "hold_head_loop_base_player0", << -803.350, 172.900, 75.700 >>, <<0.0, 0.0, 55.000>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
			//TASK_PLAY_ANIM_ADVANCED(pedHostagetakerTracey, "missprologueig_4@hold_head_base", "hold_head_loop_base_guard", << -803.350, 172.900, 75.700 >>, <<0.0, 0.0, 55.000>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)

			IF wtCurentWeapon = WEAPONTYPE_UNARMED
				//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), MichaelsWeapon, 1000, TRUE, TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
			ENDIF
			SET_MAX_WANTED_LEVEL(0)
			
			
			IF NOT DOES_ENTITY_EXIST(pedAmanda)
			
				CREATE_AMANDA()
				
				pedHostagetakerAmanda = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  << -809.6262, 179.8829, 71.1530 >>, 46.4666)
				GIVE_WEAPON_TO_PED(pedHostagetakerAmanda, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHostagetakerAmanda, TRUE)
				
			
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedHostagetakerAmanda, "MERRYWEATHER1")
			
				/* START SYNCHRONIZED SCENE -  */
				iSceneId = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
		
			
				IF NOT IS_PED_INJURED(pedAmanda)
					TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_shoot_merry_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
				ENDIF
				IF NOT IS_PED_INJURED(pedHostagetakerAmanda)
					TASK_SYNCHRONIZED_SCENE(pedHostagetakerAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_shoot_merry_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
			ENDIF
			
		BREAK
		
		CASE STAGE_FIGHT_INCOMING_BAD_GUYS
			CREATE_TRACY()
			CREATE_AMANDA()
			SET_ENTITY_COORDS(pedAmanda, << -803.350, 172.900, 75.700 >>)
		BREAK
		
		CASE STAGE_REUNITE_WITH_FAMILY
			//Dont create the guy if you are nearby
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -810.4206, 180.0989, 71.1530 >>, <<4.0, 4.0, 4.0>>)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.345337,175.497055,70.260384>>, <<-794.602661,184.461853,82.473396>>, 12.000000)
			AND NOT IS_SPHERE_VISIBLE(<< -810.4206, 180.0989, 71.1530 >>, 1.0)
				pedWritheGuy = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  << -810.4206, 180.0989, 71.1530 >>, 108.5870)
				GIVE_WEAPON_TO_PED(pedWritheGuy, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				//SET_ENTITY_HEALTH(pedWritheGuy, 99)
				TASK_WRITHE(pedWritheGuy, PLAYER_PED_ID(), 10, SHM_ONGROUND)
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedWritheGuy, TRUE)
			ENDIF
			
			IF wtCurentWeapon = WEAPONTYPE_UNARMED
				//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), MichaelsWeapon, 1000, TRUE, TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), TRUE)
			ENDIF
			
		BREAK
		
		CASE STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
//			pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<-807.8004, 171.8524, 75.7504>>, 315.4465 )
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
//			SET_JIMMYS_ARMY_OUTFIT()
//			
//			IF NOT DOES_ENTITY_EXIST(pedAmanda)
//				CREATE_AMANDA()
//			ENDIF
//			SET_ENTITY_COORDS(pedAmanda, <<-801.6101, 173.5810, 75.7407>>)
//			IF NOT DOES_ENTITY_EXIST(pedTracey)
//				CREATE_TRACY()
//			ENDIF
		BREAK
	
	ENDSWITCH

	RETURN TRUE

ENDFUNC


PROC REPLAY_OR_Z_SKIP_REPOSITION(BOOL bIsReplay, VECTOR vPos, FLOAT fHeading)

	IF bIsReplay
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
		START_REPLAY_SETUP(vPos, fHeading)		
	ELSE
		
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
		PRINTLN("REPLAY_OR_Z_SKIP_REPOSITION: not replay:", vPos)
		LOAD_SCENE(vPos)
	ENDIF

ENDPROC

WEAPON_TYPE beforeCutWeaponType = WEAPONTYPE_UNARMED

PROC EQUIP_REPLAY_WEAPON()

	IF Get_Fail_Weapon(0) = WEAPONTYPE_UNARMED
	OR Get_Fail_Weapon(0) = WEAPONTYPE_INVALID
				
		IF beforeCutWeaponType = WEAPONTYPE_UNARMED
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), FALSE)
		ELSE
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), beforeCutWeaponType, FALSE)
		ENDIF
	ELSE
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), Get_Fail_Weapon(0), FALSE)
	ENDIF

ENDPROC

PROC CREATE_POST_CUT_PAPS()

	IF NOT DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_PAP1])
		pedsMoviePremier[MOVP_PAP1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01, <<305.1440, 196.9411, 103.2161>>) 	
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_PAP2])
		pedsMoviePremier[MOVP_PAP2] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01, <<305.7792, 199.4462, 103.2793>>)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_PAP2])
		SET_ENTITY_COORDS(pedsMoviePremier[MOVP_PAP2], <<305.1440, 196.9411, 103.2161>>)
		SET_ENTITY_HEADING(pedsMoviePremier[MOVP_PAP2], 29.1979)
		TASK_PLAY_ANIM(pedsMoviePremier[MOVP_PAP2],	"missmic4premiere", "pap_check_pictures", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, 0.65)
	
		oiRedCarpetObjects[2] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
		ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[2],pedsMoviePremier[MOVP_PAP2], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_PAP1])
		SET_ENTITY_COORDS(pedsMoviePremier[MOVP_PAP1], <<305.7792, 199.4462, 103.2793>>)
		SET_ENTITY_HEADING(pedsMoviePremier[MOVP_PAP1], 106.7509)
		TASK_PLAY_ANIM(pedsMoviePremier[MOVP_PAP1],	"missmic4premiere", "pap_check_pictures", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		oiRedCarpetObjects[3] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
		ATTACH_ENTITY_TO_ENTITY(oiRedCarpetObjects[3],pedsMoviePremier[MOVP_PAP1], GET_PED_BONE_INDEX(pedsMoviePremier[MOVP_PAP1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		
	ENDIF

ENDPROC

BOOL bHaveAllStreamingRequestsCompletedAmanda

PROC MANAGE_SKIP(MISSION_STAGE_FLAG to_this_mission_stage, BOOL bIsReplay)
	
	#IF IS_DEBUG_BUILD
		iReturnStage = ENUM_TO_INT(to_this_mission_stage)
	#ENDIF
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	PRINTLN("-----------------------------------------")
	PRINTLN("SOL 5 MANAGE_SKIP selecting:",  iReturnStage)
	PRINTLN("-----------------------------------------")
	
	//For replays
	//IF g_replay.iReplayInt[0] = 0
		iMissionStartTime = GET_GAME_TIMER()	//Temp
	//ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
		SET_SYNCHRONIZED_SCENE_PHASE(iSceneId, 0.0)
	ENDIF
	//iSceneId = -1
	
	IF to_this_mission_stage > STAGE_PICKUP_JIMMY
		SET_MICHAELS_TUX()
	ENDIF
	
	PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
	
	//iMissionStartTime = GET_GAME_TIMER()	
	//iMissionStartTime-= (MISSION_TIME_LIMIT - (ENUM_TO_INT(to_this_mission_stage) * 10000) )
	
	//Do additional work when selecting stage
	SWITCH to_this_mission_stage
	
		CASE STAGE_PICKUP_JIMMY
			
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-718.7410, -158.0580, 35.9953>>, 296.3582)
						
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
			
			END_REPLAY_SETUP()
			
			SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_H_01_BH, TRUE)
			FORCE_SHOP_RESET(CLOTHES_SHOP_H_01_BH)
			
			WHILE NOT IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_H_01_BH)
				PRINTLN("Michael 4 replay waiting for the shop to open")
				WAIT(0)
			ENDWHILE
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
	
		CASE STAGE_GET_TO_MOVIE_PREMIERE
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
					
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-718.7188, -158.0137, 35.9951>>, 127.4268)
			
			REQUEST_STAGE_ASSETS(STAGE_PICKUP_JIMMY)
					
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_PICKUP_JIMMY)	
				WAIT(0)
			ENDWHILE
			
			CREATE_STAGE_ASSETS(STAGE_PICKUP_JIMMY)
			
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
			
			SET_PED_INTO_VEHICLE(pedJimmy, carMichael, VS_BACK_LEFT)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carMichael, VS_BACK_RIGHT)
			END_REPLAY_SETUP(carMichael)	
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			
			SET_VEHICLE_RADIO_ENABLED(carMichael, TRUE)
			SET_VEHICLE_DOORS_LOCKED(carMichael, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
			
			WAIT(0)
			
		BREAK
			
		CASE STAGE_MOVIE_PREMIERE_CUT	//1
		
			REQUEST_STAGE_ASSETS(STAGE_MOVIE_PREMIERE_CUT)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_MOVIE_PREMIERE_CUT)
				WAIT(0)
			ENDWHILE
			
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay,  <<294.0738, 178.6801, 103.2472>>, 349.5735)
				
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
			END_REPLAY_SETUP()
			
			WHILE NOT CREATE_STAGE_ASSETS(STAGE_MOVIE_PREMIERE_CUT)
				WAIT(0)
			ENDWHILE
			pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<299.6322, 176.0707, 103.1330>>, 344.7627)
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedJimmy, "JIMMY")
			SET_JIMMYS_TUX()
			SET_PED_AS_FRIENDLY(pedJimmy)
								
			TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), INFINITE_TASK_TIME ,SLF_WHILE_NOT_IN_FOV)
			
			carMichael = CREATE_STRETCH()
			SET_VEHICLE_ENGINE_ON(carMichael, TRUE, TRUE)
			SET_VEHICLE_LIGHTS(carMichael, SET_VEHICLE_LIGHTS_ON)
			SET_VEHICLE_RADIO_ENABLED(carMichael, FALSE)
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
		
		CASE STAGE_GET_TO_MICHAELS_HOUSE
			IF intMichaelsHouse <> NULL
				UNPIN_INTERIOR(intMichaelsHouse)
			ENDIF
			REQUEST_STAGE_ASSETS(STAGE_GET_TO_MICHAELS_HOUSE)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_GET_TO_MICHAELS_HOUSE)
				WAIT(0)
			ENDWHILE
			
			CREATE_POST_CUT_PAPS()
			
			CREATE_STAGE_ASSETS(STAGE_GET_TO_MICHAELS_HOUSE)
			
			CLEAR_AREA(<< 298.4175, 192.0289, 103.2638 >>, 100.0, TRUE)
			
			
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay,  << 298.4175, 192.0289, 103.2638 >>, 164.7207)
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
			
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000, 0.0, TRUE, TRUE)
			
			TRIGGER_MUSIC_EVENT("SOL5_GAMEPLAY_RT")
			
			//LOAD_SCENE(<< 298.4175, 192.0289, 103.2638 >>)
			END_REPLAY_SETUP()
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
				
		CASE STAGE_ENTER_THE_HOUSE
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, vMichaelsHouse, 331.9485)
		
			REQUEST_STAGE_ASSETS(STAGE_SAVE_AMANDA)
			REQUEST_STAGE_ASSETS(STAGE_GET_TO_MICHAELS_HOUSE)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_GET_TO_MICHAELS_HOUSE)
			OR NOT HAVE_STAGE_ASSETS_LOADED(STAGE_SAVE_AMANDA)
				WAIT(0)
			ENDWHILE
			
			//LOAD_SCENE(vMichaelsHouse)	
			
			CREATE_STAGE_ASSETS(STAGE_SAVE_AMANDA)
			CREATE_STAGE_ASSETS(STAGE_GET_TO_MICHAELS_HOUSE)
			
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				bHaveAllStreamingRequestsCompletedAmanda = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedAmanda)
			ENDIF
			
			WHILE NOT bHaveAllStreamingRequestsCompletedAmanda
				IF NOT IS_ENTITY_DEAD(pedAmanda)
					bHaveAllStreamingRequestsCompletedAmanda = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedAmanda)
				ENDIF
				WAIT(0)
			ENDWHILE
						
			
			IF IS_REPLAY_IN_PROGRESS()
			AND IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				
				DELETE_VEHICLE(carMichael)
				
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				
				WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					PRINTLN("MICHAEL 4: waiting for replay car")
					WAIT(0)
				ENDWHILE	
			
				CREATE_VEHICLE_FOR_REPLAY(carMichael, vMichaelsHouse, 331.9485)
			ELSE
			
				REQUEST_MODEL(STRETCH)
				
				WHILE NOT HAS_MODEL_LOADED(STRETCH)
					PRINTLN("MICHAEL 4: waiting for DEFAULT car")
					WAIT(0)
				ENDWHILE	
			
				carMichael = CREATE_STRETCH()
				SET_ENTITY_COORDS(carMichael, vMichaelsHouse)			
			ENDIF
						
						
			carMichaelIsIn = carMichael
			TRIGGER_MUSIC_EVENT("SOL5_ENTER_HOUSE_RT")
			
			END_REPLAY_SETUP(carMichael)
			
			//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carMichael)
			IF NOT IS_ENTITY_DEAD(pedJimmy)
			AND NOT IS_ENTITY_DEAD(carMichael)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carMichael)
				SET_PED_INTO_VEHICLE(pedJimmy, carMichael, VS_FRONT_RIGHT)
				SET_ENTITY_COORDS(carMichael, vMichaelsHouse)
				SET_ENTITY_HEADING(carMichael, 331.9485)
				SET_VEHICLE_ON_GROUND_PROPERLY(carMichael)
			ENDIF
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")	
		BREAK	
		
		CASE STAGE_SAVE_AMANDA
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, << -814.9095, 179.0777, 71.1530 >>,  308.0840)
		
			REQUEST_STAGE_ASSETS(STAGE_SAVE_AMANDA)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_SAVE_AMANDA)
				WAIT(0)
			ENDWHILE
			CREATE_STAGE_ASSETS(STAGE_SAVE_AMANDA)
			
			EQUIP_REPLAY_WEAPON()
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -814.9095, 179.0777, 71.1530 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 308.0840)
			TRIGGER_MUSIC_EVENT("SOL5_SAVE_A_RT")
			
			END_REPLAY_SETUP()
			
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				bHaveAllStreamingRequestsCompletedAmanda = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedAmanda)
			ENDIF
			
			WHILE NOT bHaveAllStreamingRequestsCompletedAmanda
				IF NOT IS_ENTITY_DEAD(pedAmanda)
					bHaveAllStreamingRequestsCompletedAmanda = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedAmanda)
				ENDIF
				PRINTLN("waiting on Amanda's variations")
				WAIT(0)
			ENDWHILE
			
			WHILE NOT CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_AMA", CONV_PRIORITY_VERY_HIGH)	
				PRINTLN("waiting on dialogue")
				WAIT(0)
			ENDWHILE
			
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
			
		BREAK
		
		CASE STAGE_SAVE_TRACEY
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, << -814.9095, 179.0777, 71.1530 >>,  308.0840)
		
		
			REQUEST_STAGE_ASSETS(STAGE_SAVE_TRACEY)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_SAVE_TRACEY)
				WAIT(0)
			ENDWHILE
			CREATE_STAGE_ASSETS(STAGE_SAVE_TRACEY)
			
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				bHaveAllStreamingRequestsCompletedAmanda = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedAmanda)
			ENDIF
			
			WHILE NOT bHaveAllStreamingRequestsCompletedAmanda
				IF NOT IS_ENTITY_DEAD(pedAmanda)
					bHaveAllStreamingRequestsCompletedAmanda = HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedAmanda)
				ENDIF
				WAIT(0)
			ENDWHILE
			
			EQUIP_REPLAY_WEAPON()
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -814.9095, 179.0777, 71.1530 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 308.0840)
			TRIGGER_MUSIC_EVENT("SOL5_SAVE_T_RT")
			
			END_REPLAY_SETUP()
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
		
		CASE STAGE_TRACEY_CAPTOR_SHOT
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, << -802.8893, 179.8200, 75.7408 >>,  308.0840)
		
		
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
			REQUEST_STAGE_ASSETS(STAGE_TRACEY_CAPTOR_SHOT)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_TRACEY_CAPTOR_SHOT)
				WAIT(0)
			ENDWHILE
			
			END_REPLAY_SETUP()
			SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		BREAK
		
		CASE STAGE_FIGHT_INCOMING_BAD_GUYS
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-802.83807, 179.84833, 75.7407>>,  308.0840)
		
		
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
			REQUEST_STAGE_ASSETS(STAGE_FIGHT_INCOMING_BAD_GUYS)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_FIGHT_INCOMING_BAD_GUYS)
				WAIT(0)
			ENDWHILE
			TRIGGER_MUSIC_EVENT("SOL5_FIGHT_BAD_RT")
			
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "AMANDA")
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, NULL, "TRACEY")
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, NULL, "MERRYWEATHER1")
			
			CREATE_STAGE_ASSETS(STAGE_FIGHT_INCOMING_BAD_GUYS)
			
			END_REPLAY_SETUP()
		BREAK
		
		CASE STAGE_REUNITE_WITH_FAMILY
		
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-820.8237, 176.6267, 70.5997>>,   291.0697)
		
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)

			END_REPLAY_SETUP()
			
			intMichaelsHouse = GET_INTERIOR_AT_COORDS(<< -824.2127, 182.2403, 70.7416 >>)
			PIN_INTERIOR_IN_MEMORY(intMichaelsHouse)	
			WHILE NOT IS_INTERIOR_READY(intMichaelsHouse)
				WAIT(0)
			ENDWHILE

		BREAK
		
		CASE STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
			
			REPLAY_OR_Z_SKIP_REPOSITION(bIsReplay, <<-805.6776, 182.4792, 74.2146>>,   291.0697)
			
			END_REPLAY_SETUP()
					
			REQUEST_STAGE_ASSETS(STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE)
				WAIT(0)
			ENDWHILE
			CREATE_STAGE_ASSETS(STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE)
			//LOAD_SCENE(<< -810.9391, 160.2689, 70.4956 >>)
			
			
			
		BREAK
		
		DEFAULT
			IF intMichaelsHouse <> NULL
				UNPIN_INTERIOR(intMichaelsHouse)
			ENDIF
		BREAK
	ENDSWITCH
	
	RESET_GAME_CAMERA()
	
ENDPROC

MISSION_STAGE_FLAG skip_mission_stage	//For debug and replays.
BOOL bResetFlashTimer

PROC initialiseMission()

	SET_AUDIO_FLAG("AllowPlayerAIOnMission", true)

	bHasChanged= GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)  //ensure bChange is set to the state of “player has changed outfit” at the start of the mission

	//Stop scenarios on red carpet.
	ADD_SCENARIO_BLOCKING_AREA(<< 290.7038, 178.1873, 101.3840 >>,<< 315.7695, 228.7553, 136.0489 >>)
	//SET_PED_NON_CREATION_AREA(<< 290.7038, 178.1873, 101.3840 >>, << 315.7695, 228.7553, 136.0489 >>)
	
	SET_PED_NON_CREATION_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
	
	bResetFlashTimer = FALSE
	
	DISABLE_TAXI_HAILING(TRUE)
	//SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 5, NULL, "DEVIN")
	
	REQUEST_IPL("redCarpet")
	
	REQUEST_ADDITIONAL_TEXT("SOL5", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("SOL5AUD", MISSION_DIALOGUE_TEXT_SLOT)
	
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
	
	REMOVE_RELATIONSHIP_GROUP(relGroupMerryWeather)
	ADD_RELATIONSHIP_GROUP("MERRYWEATHER", relGroupMerryWeather)

	SET_PED_RELATIONSHIP_GROUP_HASH(player_ped_id(), RELGROUPHASH_PLAYER)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupMerryWeather)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupMerryWeather, RELGROUPHASH_PLAYER)
	
	//INFORM_MISSION_STATS_OF_MISSION_START_SOLOMON_FIVE() 
	
	//SET_PED_NON_CREATION_AREA(<< -822.4180, 177.4556, 70.3269 >>, << -822.4180, 177.4556, 70.3269 >>)
	SET_PED_PATHS_IN_AREA(<< -832.4180, 167.4556, 60.3269 >>, << -812.4180, 187.4556, 80.3269 >>, FALSE)
	SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<< -820.7206, 178.5732, 70.5724 >>, 30.0, 0)
	
	DELETE_VEHICLE_GEN_VEHICLES_IN_AREA( << -825.1512, 178.9821, 70.3781 >>, 25.0)
	DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(STRETCH, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(VACCA, TRUE)
	
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BW))
		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BW),V_ILEV_MM_WINDOWWC, <<-802.73333, 167.5041, 77.5824>>)
	ENDIF
	
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_BW), 0.0)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BW), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
		
	
	DISABLE_TAXI_HAILING(TRUE)
		
	iMissionStartTime = GET_GAME_TIMER()	//Stop mission fail reason being set.
	
	mission_stage =  STAGE_PICKUP_JIMMY //STAGE_MOVIE_PREMIERE_CUT
	
	
	IF (Is_Replay_In_Progress())
		
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
		
		//SET_CLOCK_TIME(16, 00, 00)
			
        // Your mission is being replayed
        INT myStage = Get_Replay_Mid_Mission_Stage() //+ (ENUM_TO_INT(STAGE_GET_TO_MICHAELS_HOUSE))
        
		SWITCH myStage
			CASE 0
				skip_mission_stage = STAGE_PICKUP_JIMMY 
			BREAK
			CASE 1
				skip_mission_stage = STAGE_MOVIE_PREMIERE_CUT
			BREAK
			CASE 2
				skip_mission_stage = STAGE_GET_TO_MICHAELS_HOUSE
			BREAK
			CASE 3
				skip_mission_stage = STAGE_ENTER_THE_HOUSE
			BREAK
			CASE 4
				skip_mission_stage = STAGE_SAVE_AMANDA
			BREAK
			CASE 5
				skip_mission_stage = STAGE_SAVE_TRACEY
			BREAK
			CASE 6
				skip_mission_stage = STAGE_FIGHT_INCOMING_BAD_GUYS
			BREAK
			CASE 7
				skip_mission_stage = STAGE_REUNITE_WITH_FAMILY
			BREAK
		ENDSWITCH
				
		//Use the myStage variable to restart your mission at the correct stage
        //Warp the player to appropriate start coordinates (because the player won’t be at the contact point)
    	//Fade In (the game gets faded out when a replay is selected)
		
		//skip_mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, myStage)
		
		IF g_bShitskipAccepted = TRUE
            // player chose to shitskip
            // you need to skip to stage after Get_Replay_Mid_Mission_Stage()
			IF skip_mission_stage = STAGE_PICKUP_JIMMY
				skip_mission_stage = STAGE_MOVIE_PREMIERE_CUT
			ELIF skip_mission_stage = STAGE_MOVIE_PREMIERE_CUT
				skip_mission_stage = STAGE_GET_TO_MICHAELS_HOUSE
			ELIF skip_mission_stage = STAGE_GET_TO_MICHAELS_HOUSE
				skip_mission_stage = STAGE_ENTER_THE_HOUSE
			ELIF skip_mission_stage = STAGE_ENTER_THE_HOUSE
				skip_mission_stage = STAGE_SAVE_AMANDA
			ELIF skip_mission_stage = STAGE_SAVE_AMANDA
				skip_mission_stage = STAGE_SAVE_TRACEY
			ELIF skip_mission_stage = STAGE_SAVE_TRACEY
				skip_mission_stage = STAGE_FIGHT_INCOMING_BAD_GUYS
			ELIF skip_mission_stage = STAGE_FIGHT_INCOMING_BAD_GUYS
				skip_mission_stage = STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
			ENDIF
        ENDIF
		
		MANAGE_SKIP(skip_mission_stage, TRUE)
		mission_stage = skip_mission_stage
		DO_SCREEN_FADE_IN(500)
	ELSE
	
        // Your mission is being played normally, not being replayed
    	//Set_Replay_Mid_Mission_Stage(0) 
	ENDIF
		
ENDPROC

PROC REMOVE_BLIP_AND_CHECK_IF_EXISTS(BLIP_INDEX &thisBlip)

	IF DOES_BLIP_EXIST(thisBlip)
		REMOVE_BLIP(thisBlip)
	ENDIF

ENDPROC

BLIP_INDEX blipJimmy
BLIP_INDEX blipLimo
BLIP_INDEX blipTuxShop
BOOL bTuxTextSent

INT iMessingInterval = 25000
INT iTimeOfLastMessingComment
SEQUENCE_INDEX seqManageJimmy

//VECTOR vecRoadArray[42]
//FLOAT fTotalDuration

VECTOR vTuxLocation = <<-708.0601, -161.7995, 36.4152>>

INT iTimeOfExitingShop
BOOL bNotClearedTasks = FALSE
INT iLapseStartHour, iLapseEndHour

PROC stagePickUpJimmy()

//	INT i
//	VECTOR vSize

	SWITCH i_current_event

		CASE 0
			
			SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 15.0)
			
			REQUEST_STAGE_ASSETS(STAGE_PICKUP_JIMMY)
			
			IF IS_REPEAT_PLAY_ACTIVE()
				SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-712.0151, -154.0330, 36.4152>>)
				FORCE_SHOP_RESET(CLOTHES_SHOP_H_01_BH)
			ENDIF
			
			i_current_event++
		BREAK

		CASE 1
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_PICKUP_JIMMY)
			AND IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_H_01_BH)
				//SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_H_01_BH, TRUE)
				//SET_SHOP_IS_OPEN_FOR_BUSINESS(CLOTHES_SHOP_H_01_BH, TRUE)
				
				
				FADE_IN_IF_NEEDED()
										
				IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)					
					IF IS_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ZERO, COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
						PRINT_NOW("SOL5_TUX1", DEFAULT_GOD_TEXT_TIME, 1)		
					ELSE
						PRINT_NOW("SOL5_TUX2", DEFAULT_GOD_TEXT_TIME, 1)		
					ENDIF					
				ENDIF
				
				blipTuxShop = CREATE_BLIP_FOR_COORD(vTuxLocation, TRUE)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipJimmy)
				SETTIMERB(0)
				i_current_event++
			ENDIF
		BREAK

		CASE 2
		
			//Process Jimmy if you put your tux on then took it off again
			IF NOT IS_PED_INJURED(pedJimmy)
				IF NOT IS_PED_FACING_PED(pedJimmy, PLAYER_PED_ID(), 45.0)
				OR NOT IS_ENTITY_AT_COORD(pedJimmy, <<-719.4904, -162.2617, 36.0158>>, <<1.5, 1.5, 1.5>>)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedJImmy, SCRIPT_TASK_PERFORM_SEQUENCE)
						OPEN_SEQUENCE_TASK(seqManageJimmy)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-719.4904, -162.2617, 36.0158>>, PEDMOVE_WALK)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(seqManageJimmy)
						TASK_PERFORM_SEQUENCE(pedJimmy, seqManageJimmy)
						CLEAR_SEQUENCE_TASK(seqManageJimmy)
					ENDIF
				ENDIF
			ENDIF
		
		
			IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
			//AND NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_H_01_BH)
			AND HAVE_STAGE_ASSETS_LOADED(STAGE_PICKUP_JIMMY)	
				
				IF NOT DOES_ENTITY_EXIST(pedJimmy)					
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-726.119690,-158.435516,35.547009>>, <<-708.790283,-183.286514,37.872639>>, 5.750000, <<-730.7252, -144.1218, 36.2103>>, 30.4009, <<5.0, 7.0, 8.5>>, TRUE)
					REPOSITION_PLAYERS_VEHICLE(<<-730.7252, -144.1218, 36.2103>>, 30.4009)
					
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-730.7252, -144.1218, 36.2103>>, 30.4009)
							
					SET_ROADS_IN_ANGLED_AREA(<<-683.547852,-239.823624,34.704006>>, <<-772.906860,-86.623764,39.999840>>, 18.750000, FALSE, FALSE)
								
					CLEAR_AREA_OF_VEHICLES(vTuxLocation, 400.0, TRUE)
					STOP_FIRE_IN_RANGE(vTuxLocation, 150.00)
					CREATE_STAGE_ASSETS(STAGE_PICKUP_JIMMY)
					
									
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_PED_INJURED(pedJimmy)
					ENDIF
					WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedJimmy)
						IF NOT IS_PED_INJURED(pedJimmy)
						ENDIF
						WAIT(0)
					ENDWHILE

				ENDIF
					
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				IF NOT DOES_BLIP_EXIST(blipJimmy)
					IF NOT IS_PED_INJURED(pedJImmy)
						TASK_LOOK_AT_ENTITY(pedJimmy, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
					ENDIF
					blipJimmy = CREATE_BLIP_FOR_PED(pedJimmy, FALSE)
				ENDIF
				FREEZE_ENTITY_POSITION(carMichael, FALSE)
				REMOVE_BLIP(blipTuxShop)
				
				//IF IS_REPEAT_PLAY_ACTIVE()
					//SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-717.7643, -157.6015, 35.9892>>)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(), 150.1602)
//					//IF NOT IS_GAMEPLAY_CAM_RENDERING()
//						RESET_GAME_CAMERA()
//					//ENDIF
//					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 2000, TRUE, FALSE)
				//ENDIF
				
			
				i_current_event++	
			ENDIF
		BREAK
		
		CASE 3
				
			IF NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			AND IS_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)

				SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
				
			//IF NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_H_01_BH)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				CLEAR_PRINTS()
				
				CLEAR_AREA_OF_PEDS(<<-716.675,-155.420,37.675>>, 50.0)
				
				GET_SP_MISSION_TOD_WINDOW_TIME(SP_MISSION_MICHAEL_4, iLapseStartHour, iLapseEndHour)
								
				WHILE NOT timeLapseCutscene()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					WAIT(0)
				ENDWHILE
								
				IF NOT IS_GAMEPLAY_CAM_RENDERING()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					RESET_GAME_CAMERA()				
				ENDIF
				
				
				IF IS_SCREEN_FADED_OUT()		
					//SCRIPT_ASSERT("bazinga")
					REQUEST_ANIM_DICT("missmic4")
					WHILE NOT HAS_ANIM_DICT_LOADED("missmic4")
						WAIT(0)
					ENDWHILE
					
					STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-716.675,-155.420,37.675>>, 1.0, v_ilev_ch_glassdoor, INSTANT_BLEND_IN)
					STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-715.615,-157.256,37.675>>, 1.0, v_ilev_ch_glassdoor, INSTANT_BLEND_IN)
					CLEAR_AREA(<<-716.675,-155.420,37.675>>, 60.0, TRUE)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "missmic4", "michael_tux_fidget", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
					
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 2000, TRUE, TRUE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-717.8878, -157.4567, 35.9894>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 119.5472)
					REMOVE_ANIM_DICT("missmic4")					
					FADE_IN_IF_NEEDED()
					RESET_GAME_CAMERA()				
				ELSE					
					CLEAR_AREA(<<-716.675,-155.420,37.675>>, 60.0, TRUE)
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 2000, TRUE, FALSE)
					
					IF iTimeLapseStage < 3
						PRINTLN("@@@@@@@@@@@@ SET_ENTITY_INVISIBLE @@@@@@@@@@@@@")
						// 2301313, removed this line as it was causing the player to get stuck invisible
						// not noticed any pop returning as a result of this.
						//SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE) //Let shop script turn this basck on
					ENDIF
				ENDIF
								
				PRINT_NOW("SOL5_MTJIM", DEFAULT_GOD_TEXT_TIME, 1)
				i_current_event++	
			ENDIF
		BREAK
		
		CASE 4
		
			//If player doesn't move, clear tasks to stop fidget anim
			IF GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) < PEDMOVE_WALK
			AND GET_GAME_TIMER() - iTimeOfExitingShop > 1000
			AND bNotClearedTasks = FALSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				PRINTLN("@@@@@@@@@@@@ IS_ENTITY_VISIBLE @@@@@@@@@@@@@")
				IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
					PRINTLN("@@@@@@@@@@@@ SET_ENTITY_VISIBLE @@@@@@@@@@@@@")
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				//SCRIPT_ASSERT("bign!")
				bNotClearedTasks = TRUE
			ENDIF
			
		
			IF NOT IS_ENTITY_DEAD(pedJimmy)
				//IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<4.0, 4.0, 4.0>>)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-716.415894,-160.594025,35.991035>>, <<-719.482300,-155.132141,39.754093>>, 2.750000)
				OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				//OR IS_ENTITY_ON_SCREEN(pedJimmy)
					REMOVE_BLIP(blipJimmy)
					CLEAR_PRINTS()
					bNotClearedTasks = FALSE		
					
					IF NOT DOES_BLIP_EXIST(blipLimo)
						blipLimo = CREATE_BLIP_FOR_VEHICLE(carMichael)
					ENDIF
					i_current_event++		
				ENDIF
				
				IF NOT IS_PED_FACING_PED(pedJimmy, PLAYER_PED_ID(), 45.0)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedJImmy, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						TASK_TURN_PED_TO_FACE_ENTITY(pedJImmy, PLAYER_PED_ID())
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 5
		CASE 6
		CASE 7
		CASE 8
				
			//If player doesn't move, clear tasks to stop fidget anim
			IF GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) < PEDMOVE_WALK
			AND GET_GAME_TIMER() - iTimeOfExitingShop > 1000
			AND bNotClearedTasks = FALSE
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				//SCRIPT_ASSERT("bign!")
				bNotClearedTasks = TRUE
			ENDIF
			
				
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
			AND NOT IS_ENTITY_DEAD(carMichael)
			
				HANG_UP_AND_PUT_AWAY_PHONE()
				DISABLE_CELLPHONE(TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE)
				TASK_ENTER_VEHICLE(PLAYER_PED_ID(), carMichael, DEFAULT_TIME_BEFORE_WARP , VS_BACK_RIGHT)
			ENDIF
			
			//Jimmy tells Michael to stop messign around
			IF NOT IS_PED_INJURED(pedJimmy)		
			AND NOT IS_ENTITY_DEAD(carMichael)
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
				IF GET_GAME_TIMER() - iTimeOfLastMessingComment > iMessingInterval
					IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedJimmy)
					OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedJImmy) > 20.0
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MESS", CONV_PRIORITY_VERY_HIGH)
							iMessingInterval = iMessingInterval + 10000
							iTimeOfLastMessingComment = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
	
			IF NOT IS_ENTITY_DEAD(carMichael)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
					DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()		
				ENDIF
			ENDIF
		
			//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
					
			//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF i_current_event = 5
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_JGETIN", CONV_PRIORITY_VERY_HIGH)
					
					IF NOT IS_ENTITY_DEAD(carMichael)
					AND NOT IS_ENTITY_DEAD(pedJimmy)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
						SET_PED_CONFIG_FLAG(pedJimmy, PCF_ForceDirectEntry, TRUE)
						TASK_ENTER_VEHICLE(pedJimmy, carMichael, DEFAULT_TIME_BEFORE_WARP , VS_BACK_LEFT)
					ENDIF
					i_current_event = 6
				ENDIF
			ENDIF
						
			IF i_current_event = 6
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_NOW("SOL5_INLIMO", DEFAULT_GOD_TEXT_TIME, 1)
					i_current_event = 7
				ENDIF
			ENDIF
			
			IF i_current_event = 7
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_LIMO", CONV_PRIORITY_VERY_HIGH)
						i_current_event = 8
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(carMichael)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
				AND IS_PED_IN_VEHICLE(pedJImmy, carMichael)
				AND CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_WHERE", CONV_PRIORITY_VERY_HIGH)
					
					REPLAY_RECORD_BACK_FOR_TIME(8.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
				
					REMOVE_BLIP(blipLimo)
					SET_VEHICLE_RADIO_ENABLED(carMichael, TRUE)
					SET_VEHICLE_DOORS_LOCKED(carMichael, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
					DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()						
					i_current_event = 9
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 9
			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			//DISABLE_CELLPHONE(FALSE)
			SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
			i_current_event = 0
			mission_stage = STAGE_GET_TO_MOVIE_PREMIERE
		BREAK
		
	ENDSWITCH

	IF NOT IS_PED_INJURED(pedJimmy)
		SET_PED_DESIRED_MOVE_BLEND_RATIO(pedJimmy, PEDMOVE_WALK)
	ENDIF

	//If the player changes out of the tuxedo.
	IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
	AND NOT IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CLOTHES_SHOP_H_01_BH)
	AND i_current_event > 2
		i_current_event = 1
		iTimeLapseStage = 0
	ENDIF
	
	IF TIMERB() > 2000 
		IF bTuxTextSent = FALSE
		AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
			IF NOT DOES_ENTITY_EXIST(pedJimmy)	
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_JIMMY, "MIC4_TUXTEXT", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
				bTuxTextSent = TRUE
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC


//Author: Ross Wallace
//PURPOSE: Returns the percentage progress of a car recording...
FUNC FLOAT GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(VEHICLE_INDEX thisVehicle, STRING RecName)
	#IF NOT IS_DEBUG_BUILD	
	RecName = RecName
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(thisVehicle)
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisVehicle)
			
			RECORDING_ID rID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(thisVehicle)
									
			#IF IS_DEBUG_BUILD
				INT recordingNumber
				
				INT rePercentage = ROUND( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(thisVehicle) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(rID) ))
				TEXT_LABEL_63 debugName = RecName
				TEXT_LABEL_3 tlColon = ":"
				debugName += recordingNumber
				debugName += tlColon
				debugName += rePercentage
				SET_VEHICLE_NAME_DEBUG(thisVehicle, debugName)					
			#ENDIF
			
			RETURN ( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(thisVehicle) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(rID) ))// GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(recordingNumber, RecName) ) )

		ELSE
			PRINTSTRING("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: PLayback not going on!!!!!")
		ENDIF	
	ELSE
		PRINTSTRING("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: Vehicle dead!!!!!")
	ENDIF
	
	//SCRIPT_ASSERT("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: something went wrong!")
	//Fail safe as this indicates recording finished (though entity is likely dead)
	RETURN -1.0
	
ENDFUNC

INT iLimoCamStage
//INT iLimoCamTimer

//CAMERA_INDEX initialCam
//CAMERA_INDEX destinationCam
//
//VECTOR vAttachPositionsA[10]
//VECTOR vAttachRotationsA[10]
//FLOAT fAttachFOVA[10]
//
//VECTOR vAttachPositionsB[10]
//VECTOR vAttachRotationsB[10]
//FLOAT fAttachFOVB[10]

//INT iLimoCamIndex = 1
//INT iLastLimoCamIndex = 1
BOOL bLimoCamActive = TRUE
INT iJimmySunroof = 0
//INT iMaxAngles

FUNC BOOL LIMO_CAM_BUTTON()
	
	#IF IS_JAPANESE_BUILD
		RETURN IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	#ENDIF
	
	#IF NOT IS_JAPANESE_BUILD
		RETURN IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	#ENDIF
	
ENDFUNC

FUNC STRING LIMO_CAM_HELP_TEXT()

	#IF IS_JAPANESE_BUILD
		RETURN "MIC4_LIMOCAM_JAP"
	#ENDIF
	
	#IF NOT IS_JAPANESE_BUILD
		RETURN "MIC4_LIMOCAM"
	#ENDIF

ENDFUNC

CAMERA_INDEX camAnim
CAMERA_INDEX camAnim2

INT iCamSyncScene

PROC SWITCH_TO_ATTACH_CAM()

	SET_CAM_ACTIVE(camAnim, FALSE)	
	SET_CAM_ACTIVE(camAnim2, TRUE)	

ENDPROC

PROC SWITCH_TO_WIDE_CAM()

	SET_CAM_ACTIVE(camAnim, TRUE)	
	SET_CAM_ACTIVE(camAnim2, FALSE)	

ENDPROC

//#IF IS_DEBUG_BUILD
//BOOL bWideCamActive
//BOOL bAttachCamActive
//#ENDIF
///    

OBJECT_INDEX collisionBlocker1//prop_pap_camera_01
OBJECT_INDEX collisionBlocker2
OBJECT_INDEX collisionBlocker3
OBJECT_INDEX collisionBlocker4

VECTOR vOffset1 = <<-1.499496, 1.974216, -0.459954>>
VECTOR vOffset2 = <<1.499486,  1.974225, -0.477030>>
VECTOR vOffset3 = <<-1.497941, -2.836743, -0.451934>>
VECTOR vOffset4 = <<1.497952, -2.836504, -0.439115>>

PROC PROCESS_LIMO_CAM()

	DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	
		SWITCH iLimoCamStage
		
			CASE 0
				IF NOT IS_CINEMATIC_CAM_RENDERING()
				AND IS_PED_IN_VEHICLE(pedJimmy, carMichael)
							
					IF NOT DOES_ENTITY_EXIST(collisionBlocker1)
						PRINT_HELP(LIMO_CAM_HELP_TEXT())
						collisionBlocker1 = CREATE_OBJECT(PROP_CRATE_01A, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carMichael, vOffset1))
					ENDIF
					IF NOT DOES_ENTITY_EXIST(collisionBlocker2)
						collisionBlocker2 = CREATE_OBJECT(PROP_CRATE_01A, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carMichael, vOffset2))
					ENDIF
					IF NOT DOES_ENTITY_EXIST(collisionBlocker3)
						collisionBlocker3 = CREATE_OBJECT(PROP_CRATE_01A, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carMichael, vOffset3))
					ENDIF
					IF NOT DOES_ENTITY_EXIST(collisionBlocker4)
						collisionBlocker4 = CREATE_OBJECT(PROP_CRATE_01A, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(carMichael, vOffset4))
					ENDIF
								
					SET_ENTITY_COLLISION(collisionBlocker1, TRUE)
					SET_ENTITY_COLLISION(collisionBlocker2, TRUE)
					SET_ENTITY_COLLISION(collisionBlocker3, TRUE)
					SET_ENTITY_COLLISION(collisionBlocker4, TRUE)
					SET_ENTITY_VISIBLE(collisionBlocker1, FALSE)
					SET_ENTITY_VISIBLE(collisionBlocker2, FALSE)
					SET_ENTITY_VISIBLE(collisionBlocker3, FALSE)
					SET_ENTITY_VISIBLE(collisionBlocker4, FALSE)
					ATTACH_ENTITY_TO_ENTITY(collisionBlocker1, carMichael, 0, vOffset1, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
					ATTACH_ENTITY_TO_ENTITY(collisionBlocker2, carMichael, 0, vOffset2, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
					ATTACH_ENTITY_TO_ENTITY(collisionBlocker3, carMichael, 0, vOffset3, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
					ATTACH_ENTITY_TO_ENTITY(collisionBlocker4, carMichael, 0, vOffset4, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
											
					iLimoCamStage++			
//				ELSE
//					bLimoCamActive = TRUE
				ENDIF
			BREAK
		
			CASE 1
											
				IF LIMO_CAM_BUTTON() //IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)//IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				AND bLimoCamActive = FALSE											
					IF NOT IS_AUDIO_SCENE_ACTIVE("MI_4_LIMO_CAM_SCENE")
						START_AUDIO_SCENE("MI_4_LIMO_CAM_SCENE")
					ENDIF
					SET_ENTITY_COLLISION(collisionBlocker1, TRUE)
					SET_ENTITY_COLLISION(collisionBlocker2, TRUE)
					SET_ENTITY_COLLISION(collisionBlocker3, TRUE)
					SET_ENTITY_COLLISION(collisionBlocker4, TRUE)
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.0)
					
					bLimoCamActive = TRUE
				ENDIF
				
				ATTACH_ENTITY_TO_ENTITY(collisionBlocker1, carMichael, 0, vOffset1, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
				ATTACH_ENTITY_TO_ENTITY(collisionBlocker2, carMichael, 0, vOffset2, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
				ATTACH_ENTITY_TO_ENTITY(collisionBlocker3, carMichael, 0, vOffset3, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
				ATTACH_ENTITY_TO_ENTITY(collisionBlocker4, carMichael, 0, vOffset4, <<0.0, 0.0, 0.0>>, TRUE, FALSE, TRUE)
										
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CINEMATIC_SLOWMO)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_UP_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_DOWN_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_LR)

				
				FORCE_CINEMATIC_RENDERING_THIS_UPDATE(bLimoCamActive)
				
				IF LIMO_CAM_BUTTON()//IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)//IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				AND NOT IS_GAMEPLAY_CAM_RENDERING()
				AND bLimoCamActive
				OR IS_PHONE_ONSCREEN()
				
					SET_ENTITY_COLLISION(collisionBlocker1, FALSE)
					SET_ENTITY_COLLISION(collisionBlocker2, FALSE)
					SET_ENTITY_COLLISION(collisionBlocker3, FALSE)
					SET_ENTITY_COLLISION(collisionBlocker4, FALSE)
					
					DELETE_OBJECT(collisionBlocker1)
					DELETE_OBJECT(collisionBlocker2)
					DELETE_OBJECT(collisionBlocker3)
					DELETE_OBJECT(collisionBlocker4)
					
					CLEAR_HELP()
					//DESTROY_ALL_CAMS()
					iLimoCamStage = 0
					IF IS_AUDIO_SCENE_ACTIVE("MI_4_LIMO_CAM_SCENE")
						STOP_AUDIO_SCENE("MI_4_LIMO_CAM_SCENE")
					ENDIF
					IF i_current_event >= 3
						iLimoCamStage = 999	
					ENDIF
					bLimoCamActive = FALSE
				ENDIF

			BREAK
			
		ENDSWITCH

			
//	ELSE
//		
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	//ENDIF

ENDPROC


INT iSunroofTimer
INT iPartyInterval
//BOOL bLimoCamHelpPrinted
INT iWhichRandomIdle
STRING stAnim
OBJECT_INDEX oiChampagneBottle
PTFX_ID ptfxChampMess
FLOAT fChampEvoValue
VECTOR vChampRotation

PROC PROCESS_JIMMY_SUNROOF()

	SWITCH iJimmySunroof
	
		CASE 0
			//REQUEST_STAGE_ASSETS(STAGE_GET_TO_MOVIE_PREMIERE)
			iSunroofTimer = GET_GAME_TIMER()
			iPartyInterval = 5000
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_TALK1", CONV_PRIORITY_VERY_HIGH)
				iJimmySunroof++
			ENDIF
		BREAK
	
		CASE 1
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_GET_TO_MOVIE_PREMIERE)
			AND GET_GAME_TIMER() - iSunroofTimer > iPartyInterval
				iJimmySunroof++
			ENDIF
		BREAK
	
		CASE 2
			IF NOT IS_PED_INJURED(pedJimmy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJimmy, TRUE)
								
				oiChampagneBottle = CREATE_OBJECT(Prop_Champ_01b, GET_ENTITY_COORDS(pedJimmy))
				ATTACH_ENTITY_TO_ENTITY(oiChampagneBottle, pedJimmy, GET_PED_BONE_INDEX(pedJimmy, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
				iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2227677
				
				TASK_PLAY_ANIM(pedJImmy, "missmic4jimmy_limo", "intro", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME)
//				iLimoCamIndex = 0
			ENDIF
			stAnim = "Idle_01"
			iJimmySunroof++
		BREAK
		
		CASE 3

			IF NOT IS_PED_INJURED(pedJimmy)
			AND DOES_ENTITY_EXIST(oiChampagneBottle)
//				IF (GET_GAME_TIMER() - iSunroofTimer > iPartyInterval
//					OR GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carMichael, "MIC4") > 90.0 )
				IF IS_ENTITY_PLAYING_ANIM(pedJImmy, "missmic4jimmy_limo", stAnim)
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedJImmy, "missmic4jimmy_limo", stAnim) >= 0.99
				AND i_current_event > 2 // NOT IS_SCRIPTED_CONVERSATION_ONGOING()						
					iJimmySunroof++
					FALLTHRU
				ELSE		
					
//					IF bLimoCamHelpPrinted = FALSE
//					AND bLimoCamActive = FALSE
//						PRINT_HELP(LIMO_CAM_HELP_TEXT())
//						bLimoCamHelpPrinted = TRUE
//					ENDIF
					
					SET_PED_CAN_HEAD_IK(pedJImmy, FALSE)
					
					vChampRotation = GET_ENTITY_ROTATION(oiChampagneBottle)
					
					fChampEvoValue = 0.0
					
					IF ABSF(vChampRotation.x) > 5.0
					OR ABSF(vChampRotation.y) > 5.0
						fChampEvoValue = 0.5
					ENDIF
					
					IF ABSF(vChampRotation.x) > 15.0
					OR ABSF(vChampRotation.y) > 15.0
						fChampEvoValue = 1.0					
					ENDIF
					
										
					PRINTLN("vChampRotation.x", vChampRotation.x, "vChampRotation.y", vChampRotation.y, "fChampEvoValue: ", fChampEvoValue)
					
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxChampMess)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxChampMess, "flow", fChampEvoValue)
					ENDIF
					
					IF (IS_ENTITY_PLAYING_ANIM(pedJImmy, "missmic4jimmy_limo", "Idle_04") AND GET_ENTITY_ANIM_CURRENT_TIME(pedJImmy, "missmic4jimmy_limo", "Idle_04") >= 0.68)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ENDIF
					
					IF (IS_ENTITY_PLAYING_ANIM(pedJImmy, "missmic4jimmy_limo", "intro") AND GET_ENTITY_ANIM_CURRENT_TIME(pedJImmy, "missmic4jimmy_limo", "intro") >= 0.99)
					OR (IS_ENTITY_PLAYING_ANIM(pedJImmy, "missmic4jimmy_limo", stAnim) AND GET_ENTITY_ANIM_CURRENT_TIME(pedJImmy, "missmic4jimmy_limo", stAnim) >= 0.99)
						
						iWhichRandomIdle  = GET_RANDOM_INT_IN_RANGE(0, 5)
			
						SWITCH iWhichRandomIdle
						
							CASE 0
								stAnim = "Idle_01"
							BREAK
							
							CASE 1
								stAnim = "Idle_02"
							BREAK
							
							CASE 2
								stAnim = "Idle_03"
							BREAK
						
							CASE 3
								stAnim = "Idle_04"
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								ENDIF
							BREAK
							
							CASE 4
								stAnim = "Idle_05"
							BREAK
						ENDSWITCH
						
						IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxChampMess)
							ptfxChampMess = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_rcpap1_champ_slosh", oiChampagneBottle, <<0.0, 0.0, 0.37>>, <<0.0, 0.0, 0.0>>)
						ENDIF
						
						TASK_PLAY_ANIM(pedJImmy, "missmic4jimmy_limo", stAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
						iPartyInterval = GET_RANDOM_INT_IN_RANGE(5000, 20000)
						iSunroofTimer = GET_GAME_TIMER()
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
						//iLimoCamIndex = 0
					ENDIF
				ENDIF
			ENDIF
						
		BREAK
	
		CASE 4
			IF NOT IS_PED_INJURED(pedJimmy)
				TASK_PLAY_ANIM(pedJImmy, "missmic4jimmy_limo", "outro", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJimmy)
				iPartyInterval = GET_RANDOM_INT_IN_RANGE(20000, 30000)
				iSunroofTimer = GET_GAME_TIMER()
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxChampMess)
					STOP_PARTICLE_FX_LOOPED(ptfxChampMess)
				ENDIF
						
				iJimmySunroof++
			ENDIF
		BREAK
		
		CASE 5
//			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carMichael, "MIC4") < 90.0
//			AND IS_SCREEN_FADED_IN()	
//				IF GET_GAME_TIMER() - iSunroofTimer > iPartyInterval
//					iJimmySunroof = 2
//				ENDIF
//			ELSE
			IF NOT IS_PED_INJURED(pedJimmy)
				IF IS_ENTITY_PLAYING_ANIM(pedJImmy, "missmic4jimmy_limo", "outro")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedJImmy, "missmic4jimmy_limo", "outro") >= 0.99
					IF DOES_ENTITY_EXIST(oiChampagneBottle)
						DELETE_OBJECT(oiChampagneBottle)
						SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Champ_01b)
						REMOVE_ANIM_DICT("missmic4jimmy_limo")
						iJimmySunroof = 6
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC


INT iTimeOfLastFlash
INT iFlashInterval
VECTOR vCameraRotation

//BOOL bDisplayBanner

PROC PROCESS_CAMERA_FLASHES()

//Camera Flashes.
	INT i

	IF GET_GAME_TIMER() - iTimeOfLastFlash > iFlashInterval
	AND NOT IS_CUTSCENE_PLAYING()
		i = GET_RANDOM_INT_IN_RANGE(1, 12)
		IF i = 7
			i = 8
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiRedCarpetObjects[8])
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), oiRedCarpetObjects[8]) < 3.0
				IF GET_RANDOM_BOOL()
					i = 8
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(oiRedCarpetObjects[9])
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), oiRedCarpetObjects[9]) < 3.0
				IF GET_RANDOM_BOOL()
					i = 9
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiRedCarpetObjects[10])
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), oiRedCarpetObjects[10]) < 3.0
								
				PRINTLN("camera ROT:, ", vCameraRotation.x, " ", vCameraRotation.y)
				IF GET_RANDOM_BOOL()
					i = 10
				ENDIF
			ENDIF
						
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiRedCarpetObjects[11])
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), oiRedCarpetObjects[11]) < 3.0
				i = 11
			ENDIF
		ENDIF
		
		IF REQUEST_SCRIPT_AUDIO_BANK("Distant_Camera_Flash")
			PLAY_SOUND_FROM_ENTITY(-1, "MIC4_CAMERA_FLASH_master", oiRedCarpetObjects[i])
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiRedCarpetObjects[i])
			vCameraRotation =  GET_ENTITY_ROTATION(oiRedCarpetObjects[i])
		ENDIF
		
		IF vCameraRotation.x < 50.0
		AND vCameraRotation.y > -4.0
			IF DOES_ENTITY_EXIST(oiRedCarpetObjects[i])
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY ("scr_rcpap1_camera", oiRedCarpetObjects[i], <<0.0, 0.0, 0.0.>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			iFlashInterval = GET_RANDOM_INT_IN_RANGE(50, 250)
			iTimeOfLastFlash = GET_GAME_TIMER()	
		ENDIF
		
		
	ENDIF	

	IF NOT IS_PED_INJURED(pedsMoviePremier[MOVP_PRODUCER])
		SET_PED_RESET_FLAG(pedsMoviePremier[MOVP_PRODUCER], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
	ENDIF

	IF NOT IS_PED_INJURED(pedsMoviePremier[MOVP_PAP1_A])
		SET_PED_RESET_FLAG(pedsMoviePremier[MOVP_PAP1_A], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedsMoviePremier[MOVP_EX_3])
		SET_PED_RESET_FLAG(pedsMoviePremier[MOVP_EX_3], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
	ENDIF

ENDPROC

INT iTimeOfLastFirework
INT iFireworkInterval

PROC PROCESS_FIREWORKS()

	//Fireworks
	INT i
	VECTOR vFireworkPos
	VECTOR vFireworkRotation

	IF GET_GAME_TIMER() - iTimeOfLastFirework > iFireworkInterval
	AND NOT IS_CUTSCENE_PLAYING()
	
		i = GET_RANDOM_INT_IN_RANGE(0, 5)
		
		SWITCH i
		
			CASE 0
				vFireworkPos = <<323.3195, 226.5455, 121.6866>>
			BREAK

			CASE 1
				vFireworkPos = <<298.9352, 229.2652, 121.2983>>
			BREAK

			CASE 2
				vFireworkPos = <<302.1084, 237.7405, 122.2715>>
			BREAK

			CASE 3
				vFireworkPos = <<310.7933, 234.3421, 123.2060>>
			BREAK

			CASE 4
				vFireworkPos= <<318.3249, 234.6467, 123.0775>>
			BREAK
		
		ENDSWITCH
		
		vFireworkRotation.x = GET_RANDOM_FLOAT_IN_RANGE(-5.0, 5.0)
		vFireworkRotation.y = GET_RANDOM_FLOAT_IN_RANGE(-5.0, 5.0)
		vFireworkRotation.z = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
				
//		IF REQUEST_SCRIPT_AUDIO_BANK("Distant_Camera_Flash")
//			PLAY_SOUND_FROM_ENTITY(-1, "MIC4_CAMERA_FLASH_master", oiRedCarpetObjects[i])
//		ENDIF
		
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(0.0, 1.0))
		
		IF GET_RANDOM_BOOL()
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mich4_firework_starburst", vFireworkPos, vFireworkRotation, GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.6))
		ELSE
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_mich4_firework_trailburst", vFireworkPos, vFireworkRotation, GET_RANDOM_FLOAT_IN_RANGE(0.8, 1.6))			
		ENDIF
				
		PRINTLN("Fireworks", vFireworkPos, vFireworkRotation, iFireworkInterval)
		
		iFireworkInterval = GET_RANDOM_INT_IN_RANGE(250, 1200)
		iTimeOfLastFirework = GET_GAME_TIMER()
	ENDIF	

ENDPROC

///Creates a new load scene sphere and waits until it has finished loading or it times out.
PROC NEW_LOAD_SCENE_SPHERE_WITH_WAIT(VECTOR vPos, FLOAT fRadius, NEWLOADSCENE_FLAGS controlFlags = 0, INT iMaxWaitTime = 10000)
      INT iTimeOut = GET_GAME_TIMER()

      WHILE GET_GAME_TIMER() - iTimeOut < iMaxWaitTime
            IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
                  NEW_LOAD_SCENE_START_SPHERE(vPos, fRadius, controlFlags)
            ELIF IS_NEW_LOAD_SCENE_LOADED()
                  iTimeOut = 0
            ENDIF
            
            WAIT(0)
      ENDWHILE
      
      NEW_LOAD_SCENE_STOP()   
ENDPROC


INT iTimeOfTrigger 
INT iTimeOfTriggerMinutes
			
CAM_VIEW_MODE beforeCinematicCameraMode

PROC stageGetToMoviePremiere()

	HANDLE_CINEMA_DOOR()

	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T)
			i_current_event = 3
			SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(carMichael)
		ENDIF
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(carMichael)
	
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael, TRUE)
			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		ENDIF
	
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael, TRUE)
		AND IS_PED_IN_VEHICLE(pedJimmy, carMichael)
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			PROCESS_LIMO_CAM()
		ELSE
			//SCRIPT_ASSERT("boom")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		ENDIF
	ENDIF
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
	AND i_current_event = 2//< 3
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
		DO_SCREEN_FADE_OUT(500)
		WHILE IS_SCREEN_FADING_OUT()
			WAIT(0)
		ENDWHILE
		i_current_event = 3
		iJimmySunroof = 6
		IF NOT IS_ENTITY_DEAD(carMichael)
			SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(carMichael)
		ENDIF
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		IF NOT IS_PED_INJURED(pedJimmy)
			CLEAR_PED_TASKS(pedJimmy)
		ENDIF
		DELETE_OBJECT(oiChampagneBottle)
		CLEAR_HELP()
		NEW_LOAD_SCENE_SPHERE_WITH_WAIT(<<291.3156, 176.4981, 103.1394>>, 30.0)
				
	ENDIF
	

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	
//	IF NOT IS_ENTITY_DEAD(carMichael)
//	AND i_current_event < 3
//		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carMichael)
//			SET_ENTITY_ROTATION(carMichael, GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(1, 0.0, "MIC4"))
//			SET_ENTITY_COORDS(carMichael, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 0.0, "MIC4"))
//		ENDIF
//	ENDIF
	
	SWITCH i_current_event

		CASE 0
		
			DISABLE_CELLPHONE(TRUE)
		
			PAUSE_CLOCK(TRUE)		
		
			CLEAR_PRINTS()
			TRIGGER_MUSIC_EVENT("SOL5_LIMO_RADIO")
			SET_PED_NON_CREATION_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
				
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			IF NOT IS_AUDIO_SCENE_ACTIVE("MI_4_LIMO_DRIVE_SCENE")
				START_AUDIO_SCENE("MI_4_LIMO_DRIVE_SCENE")
			ENDIF
//			bLimoCamHelpPrinted = FALSE
			beforeCinematicCameraMode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
			
			camAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
			camAnim2 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
			
			iTimeOfTrigger = GET_CLOCK_HOURS()
			iTimeOfTriggerMinutes = GET_CLOCK_MINUTES()
			
			REQUEST_STAGE_ASSETS(STAGE_PICKUP_JIMMY)
			REQUEST_STAGE_ASSETS(STAGE_GET_TO_MOVIE_PREMIERE)
			i_current_event++
		BREAK
		
		CASE 1
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_PICKUP_JIMMY)
			AND HAVE_STAGE_ASSETS_LOADED(STAGE_GET_TO_MOVIE_PREMIERE)
				STOP_VEHICLE_FIRE(carMichael)
				SET_ENTITY_PROOFS(carMichael, FALSE, TRUE, FALSE, FALSE, FALSE)
				
				#IF IS_DEBUG_BUILD
					set_uber_parent_widget_group(sol5WidgetGroup)
				#ENDIF
				
				INITIALISE_UBER_PLAYBACK("MIC4", 1, FALSE)
				SETUP_TRAFFIC()	
				IF NOT IS_ENTITY_DEAD(carMichael)
					//START_PLAYBACK_RECORDED_VEHICLE_USING_AI(carMichael, 5, "Mic4Rec", 10, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					START_PLAYBACK_RECORDED_VEHICLE(carMichael, 1, "MIC4", FALSE)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carMichael, FALSE)
				ENDIF
								
				PLAY_CAM_ANIM(camAnim, "LimoDrive_Camera_freeCam", "MISSMIC4JIMMY_LIMO", GET_ENTITY_COORDS(carMichael) - <<0.0, 0.0, -0.5>>, GET_ENTITY_ROTATION(carMichael))
			
				iCamSyncScene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iCamSyncScene, carMichael, GET_ENTITY_BONE_INDEX_BY_NAME(carMichael, "chassis"))
				PLAY_SYNCHRONIZED_CAM_ANIM(camAnim2, iCamSyncScene, "LimoDrive_Camera_attachToCar", "MISSMIC4JIMMY_LIMO")
						
				SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
				FADE_IN_IF_NEEDED()
			
				SET_PED_NON_CREATION_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
				SET_ROADS_IN_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>, FALSE)
						
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
		CASE 3
			//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<291.3156, 176.4981, 103.1394>>, <<400.0, 400.0, 200.0>>)	
			IF iJimmySunroof > 0
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			AND i_current_event = 2
			
				DISABLE_CELLPHONE(FALSE)
			
				IF PLAYER_CALL_CHAR_CELLPHONE(myScriptedSpeech, CHAR_DEVIN, "SOL5AUD", "SOL5_CALL", CONV_PRIORITY_VERY_HIGH, TRUE,  DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
					i_current_event = 3
				ENDIF				
				
//				IF CHECK_CELLPHONE_LAST_CALL_REJECTED() 
//				OR WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
//					i_current_event = 3
//				ENDIF
			ENDIF			
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<291.3156, 176.4981, 103.1394>>, <<100.0, 100.0, 100.0>>)	
				REQUEST_STAGE_ASSETS(STAGE_MOVIE_PREMIERE_CUT)
				i_current_event = 4
			ENDIF			
		BREAK
		
		CASE 4
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_MOVIE_PREMIERE_CUT)
				IF CREATE_STAGE_ASSETS(STAGE_MOVIE_PREMIERE_CUT)
					FADE_IN_IF_NEEDED()
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
		
			PROCESS_CAMERA_FLASHES()
			PROCESS_FIREWORKS()
		
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carMichael, "MIC4") > 90.0
			AND CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_THERE", CONV_PRIORITY_VERY_HIGH)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 6
			PROCESS_CAMERA_FLASHES()
			PROCESS_FIREWORKS()
		BREAK
		
	ENDSWITCH
	
	
	
	//SET_CLOCK_TIME(iTimeOfTrigger, iTimeOfTriggerMinutes, 0)
	
		
	
	
	
	IF i_current_event > 1
		UPDATE_UBER_PLAYBACK(carMichael, 1.0)
		SET_PLAYBACK_SPEED(carMichael, 1.0)
		PROCESS_JIMMY_SUNROOF()
		STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(carMichael), 4.3)
	ENDIF
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
	
	//IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(sLocatesData, <<291.3156, 176.4981, 103.1394>>, <<3.0, 3.0, 12.0>>, FALSE, pedJimmy, NULL, NULL, "SOL5_MOVIE", "CMN_JLEAVE", "", "", "CMN_JLEAVE", FALSE, TRUE, TRUE) //, NULL, TRUE)	
	//IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carMichael, "Mic4Rec") >= 99.0
	IF NOT IS_ENTITY_DEAD(carMichael)
	AND i_current_event > 4
		IF IS_ENTITY_AT_COORD(carMichael,<<291.3156, 176.4981, 103.1394>>, <<4.0, 4.0, 4.0>>)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				carMichaelIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(carMichaelIsIn, 3.0, 5)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
							
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
						
					WAIT(0)
					
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Meltdown")
					
				ENDWHILE
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
			
			DELETE_OBJECT(collisionBlocker1)
			DELETE_OBJECT(collisionBlocker2)
			DELETE_OBJECT(collisionBlocker3)
			DELETE_OBJECT(collisionBlocker4)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CRATE_01A)
			
			SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(beforeCinematicCameraMode)
			
			STOP_PLAYBACK_RECORDED_VEHICLE(carMichael)
			SET_VEHICLE_DOORS_LOCKED(carMichael, VEHICLELOCK_UNLOCKED)
			
			DESTROY_ALL_CAMS()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			DELETE_OBJECT(oiChampagneBottle)
			DISABLE_CELLPHONE(FALSE)
			CLEANUP_UBER_PLAYBACK()
			
			SET_ENTITY_PROOFS(carMichael, FALSE, FALSE, FALSE, FALSE, FALSE)
			
			PAUSE_CLOCK(FALSE)
			
//			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			i_current_event = 0
			mission_stage = STAGE_MOVIE_PREMIERE_CUT
		ENDIF
	ENDIF
	
ENDPROC

INT iLazlowDialogue
INT iLazlowDialogueTimer

INT iRandomProducerLine

PROC PROCESS_LAZLOW_DIALOGUE()

	IF IS_PED_ON_FOOT(PLAYER_PED_ID())

		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedsMoviePremier[MOVP_PRODUCER]) > 4.0
		AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedsMoviePremier[MOVP_LAZLOW]) > 6.0
			SWITCH iLazlowDialogue
				CASE 0
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_INT_LI1", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)	
							iLazlowDialogueTimer = GET_GAME_TIMER()
							iLazlowDialogue++
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					AND GET_GAME_TIMER() - iLazlowDialogueTimer > 7500
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_INT_LI2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)	
							iLazlowDialogueTimer = GET_GAME_TIMER()
							iLazlowDialogue++		
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					AND GET_GAME_TIMER() - iLazlowDialogueTimer > 6500
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_INT_LI3", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)	
							iLazlowDialogueTimer = GET_GAME_TIMER()
							iLazlowDialogue++
						ENDIF
					ENDIF
				BREAK
				
				CASE 3
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					AND GET_GAME_TIMER() - iLazlowDialogueTimer > 2500
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_INT_LI4", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)	
							iLazlowDialogue++			
						ENDIF
					ENDIF
				BREAK
				
			ENDSWITCH
	
		ELSE
		
			IF GET_GAME_TIMER() - iRandomProducerLine > 5000
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_IG1_5", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)	
					iRandomProducerLine = GET_GAME_TIMER()		
				ENDIF
			ENDIF
		
		ENDIF
	
	ENDIF
	
ENDPROC


BLIP_INDEX blipRedCarpet
SEQUENCE_INDEX seqGetOutOfLimo
BOOL bJimmySaidHowProud = FALSE
//BOOL bGodTextDisplayed = FALSE
BOOL bAudioStreamPlaying = FALSE
BOOL bStopAudioStream = FALSE
BOOL bStopCrotchCam = FALSE	// url:bugstar:2077947

INT iPushInTimer
//INT iArrivalTime

PROC stageIntroMocapCutscene()

	HANDLE_CINEMA_DOOR()

	INT i
	
	IF i_current_event < 5
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
	
		IF HAS_ANIM_SET_LOADED("move_p_m_zero_slow")
			SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "move_p_m_zero_slow")
		ENDIF
		
		IF NOT IS_PED_INJURED(pedJimmy)
			SET_PED_MAX_MOVE_BLEND_RATIO(pedJimmy, PEDMOVE_WALK)			
			IF HAS_ANIM_SET_LOADED("move_characters@Jimmy@slow@")
				SET_PED_MOVEMENT_CLIPSET(pedJimmy,"move_characters@Jimmy@slow@")
			ENDIF			
		ENDIF	
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	
	IF NOT IS_ENTITY_DEAD(carMichael)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		ENDIF	
	ENDIF	
	
	PROCESS_CAMERA_FLASHES()
	
	#IF IS_NEXTGEN_BUILD
		PROCESS_FIREWORKS()
	#ENDIF
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
	
	SWITCH i_current_event

		CASE 0
			
			iTimeOfTrigger = GET_CLOCK_HOURS()
			iTimeOfTriggerMinutes = GET_CLOCK_MINUTES()
			
//			iArrivalTime = GET_CLOCK_HOURS()
			
			SET_PED_POPULATION_BUDGET(0)
			SET_VEHICLE_POPULATION_BUDGET(0)
			
			STOP_PLAYBACK_RECORDED_VEHICLE(carMichael)
		
			REQUEST_ANIM_SET("move_characters@Jimmy@slow@")
			REQUEST_ANIM_SET("move_p_m_zero_slow")
			
			REQUEST_ANIM_DICT("MISSMIC4IG_5")
			bJimmySaidHowProud = FALSE
//			bGodTextDisplayed = FALSE
			bAudioStreamPlaying = FALSE
			FADE_IN_IF_NEEDED()
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_MOVIE_PREMIERE_CUT")
			blipRedCarpet = CREATE_BLIP_FOR_COORD(<<298.3941, 192.8110, 103.3272>>)
			PRINT_NOW("SOL5_REDCARPET", DEFAULT_GOD_TEXT_TIME, 1)
			i_current_event++
		BREAK

		CASE 1
			
			IF bJimmySaidHowProud = FALSE
			AND IS_PED_ON_FOOT(pedJimmy)
				IF HAS_ANIM_DICT_LOADED("MISSMIC4IG_5")					
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_PROUD", CONV_PRIORITY_VERY_HIGH)
							bJimmySaidHowProud = TRUE
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_PROUD", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
							bJimmySaidHowProud = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			IF bJimmySaidHowProud = TRUE
//			AND bGodTextDisplayed = FALSE
//				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)					
//					bGodTextDisplayed = TRUE
//				ENDIF
//			ENDIF
			
			IF bAudioStreamPlaying = FALSE
				IF LOAD_STREAM("MIC_4_PREMIERE")
					PLAY_STREAM_FROM_POSITION(<<300, 194, 105>>)
					bAudioStreamPlaying = TRUE
				ENDIF
			ENDIF
			
			IF bJimmySaidHowProud = TRUE
				PROCESS_LAZLOW_DIALOGUE()
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(carMichael)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
				//AND IS_PED_IN_VEHICLE(pedJimmy, carMichael)
					IF NOT IS_PED_INJURED(pedJimmy)
					AND NOT IS_ENTITY_DEAD(carMichael)
					AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedJimmy, SCRIPT_TASK_PERFORM_SEQUENCE)
						OPEN_SEQUENCE_TASK(seqGetOutOfLimo)
							TASK_PAUSE(NULL, 1000)
							TASK_LEAVE_VEHICLE(NULL, carMichael)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<300.5360, 200.4727, 103.3838>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 256.7409)
							TASK_PLAY_ANIM(NULL, "missmic4premiere", "wave_b")
							TASK_PLAY_ANIM(NULL, "missmic4premiere", "wave_c")
							TASK_PLAY_ANIM(NULL, "missmic4premiere", "wave_d")
							TASK_PLAY_ANIM(NULL, "missmic4premiere", "wave_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seqGetOutOfLimo)
						TASK_PERFORM_SEQUENCE(pedJimmy, seqGetOutOfLimo)
						CLEAR_SEQUENCE_TASK(seqGetOutOfLimo)
						IF IS_AUDIO_SCENE_ACTIVE("MI_4_LIMO_DRIVE_SCENE")
							STOP_AUDIO_SCENE("MI_4_LIMO_DRIVE_SCENE")
						ENDIF				
					ENDIF
				ENDIF
			ENDIF
			
			IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<298.7313, 195.1057, 103.0034>>, <<1.5, 1.5, 1.5>>, FALSE)
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<296.364563,187.513870,102.539314>>, <<301.566162,203.298996,110.737076>>, 16.750000)
			
			
				SET_PED_AS_NO_LONGER_NEEDED(pedLimoDriver)
				SET_PED_AS_NO_LONGER_NEEDED(pedsMoviePremier[MOVP_PRODUCER])
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_MovPrem_01)
				
				SET_OBJECT_AS_NO_LONGER_NEEDED(oiRedCarpetObjects[12])
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING_02)			
							
				REMOVE_BLIP(blipRedCarpet)
			
				DESTROY_ALL_CAMS()
								
				IF NOT DOES_ENTITY_EXIST(pedJimmy)
					pedJimmy = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, << 303.5163, 201.2443, 103.3570 >>, 158.4110)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedJimmy, "JIMMY")
					SET_JIMMYS_TUX()
					SET_PED_AS_FRIENDLY(pedJimmy)
				ENDIF
				
				REMOVE_ANIM_DICT("MISSMIC4IG_5")
				REMOVE_ANIM_SET("move_characters@Jimmy@slow@")
				REMOVE_ANIM_SET("move_p_m_zero_slow")
				
				REQUEST_CUTSCENE("mic_4_int")
				//REQUEST_STAGE_ASSETS(STAGE_MOVIE_PREMIERE_CUT)
				
				SET_GAMEPLAY_COORD_HINT(GET_ENTITY_COORDS(pedsMoviePremier[MOVP_LAZLOW]), -1, 2000)
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.35)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.015)
				// url:bugstar:2077947
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.65)
					bStopCrotchCam = TRUE
				ELSE
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.02)
					bStopCrotchCam = FALSE
				ENDIF
				SET_GAMEPLAY_HINT_FOV(30.00)
				
				iPushInTimer = GET_GAME_TIMER()
				i_current_event++
			ENDIF
			
		BREAK
	 		
		CASE 2
			// url:bugstar:2077947
			IF NOT bStopCrotchCam
			AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.65)
				bStopCrotchCam = TRUE
			ELIF bStopCrotchCam
			AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.02)
				bStopCrotchCam = FALSE
			ENDIF
		
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())	
			
			IF DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_LAZLOW])
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_LAZLOW])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lazlow", pedsMoviePremier[MOVP_LAZLOW])	
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_FEM_A])
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_FEM_A])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Milton_Assistant", pedsMoviePremier[MOVP_FEM_A])	
				ENDIF
			ENDIF
			

			//Two paps
			IF DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_PAP2])
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_PAP2])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Paparazzi_01", pedsMoviePremier[MOVP_PAP2])	
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_PAP1])
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_PAP1])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Paparazzi_02", pedsMoviePremier[MOVP_PAP1])	
				ENDIF
			ENDIF			
			
			
			IF DOES_ENTITY_EXIST(pedsMoviePremier[MOVP_CAMMAN])
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_CAMMAN])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("TV_Camera_dude", pedsMoviePremier[MOVP_CAMMAN])	
				ENDIF
			ENDIF	
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF NOT IS_ENTITY_DEAD(pedJImmy)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedJImmy)	
				ELSE
					SCRIPT_ASSERT("jimmys dead")
				ENDIF
			ELSE
				//SCRIPT_ASSERT("Cannae set the components")
			ENDIF
			
			IF GET_GAME_TIMER() - iPushInTimer > 2500										
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				REQUEST_MODEL(STRETCH)
				REQUEST_MODEL(VACCA)
				i_current_event = 4
			ENDIF
		BREAK
	
		CASE 4
			
			IF HAS_CUTSCENE_LOADED()
				
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_LAZLOW])
					REGISTER_ENTITY_FOR_CUTSCENE(pedsMoviePremier[MOVP_LAZLOW], "Lazlow", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, IG_JIMMYDISANTO)					
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_FEM_A])
					REGISTER_ENTITY_FOR_CUTSCENE(pedsMoviePremier[MOVP_FEM_A], "Milton_Assistant", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_PAP2])
					REGISTER_ENTITY_FOR_CUTSCENE(pedsMoviePremier[MOVP_PAP2], "Paparazzo_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, IG_JIMMYDISANTO)					
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_PAP1])
					REGISTER_ENTITY_FOR_CUTSCENE(pedsMoviePremier[MOVP_PAP1], "Paparazzo_02", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, IG_JIMMYDISANTO)					
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedJImmy)
					REGISTER_ENTITY_FOR_CUTSCENE(pedJImmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//, IG_JIMMYDISANTO)					
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedJimmy, "JIMMY")
				ENDIF				
				
				IF NOT IS_ENTITY_DEAD(pedsMoviePremier[MOVP_CAMMAN])
					REGISTER_ENTITY_FOR_CUTSCENE(pedsMoviePremier[MOVP_CAMMAN], "TV_Camera_dude", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)//, IG_JIMMYDISANTO)					
				ENDIF
					
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
				SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")			
				
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
			
				i_current_event++
			ENDIF
		BREAK
	
		CASE 5
			IF IS_CUTSCENE_PLAYING()
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_MICHAEL_4)			
				STOP_GAMEPLAY_HINT(TRUE)
				IF NOT IS_PED_INJURED(pedJimmy)
					RESET_PED_MOVEMENT_CLIPSET(pedJimmy)
				ENDIF
				RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())
				
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_SPRINT)
				
			
				//REMOVE_ANIM_DICT("missmic4premiere")
				
			
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_LAZLOW)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_JIMMYDISANTO)
				SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_ANTONB)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_Milton)	
				SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_MovPrem_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Paparazzi_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_Grip_01)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_MovPrem_01)			
				
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING_02)

				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_V_Cam_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(P_ING_MICROPHONEL_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_pap_camera_01)
				
				FOR i = 0 TO MOVP_MAX_PEDS -1
					
					IF i <> MOVP_PAP1
					AND i <> MOVP_PAP2		
					AND i <> MOVP_FEM_A
					AND i <> MOVP_CAMMAN
						DELETE_PED(pedsMoviePremier[i])
					ENDIF
				ENDFOR
				
				FOR i = 0 TO 12
					DELETE_OBJECT(oiRedCarpetObjects[i])
				ENDFOR								
													
				SET_MICHAELS_TUX()
							
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<279.124939,193.901825,102.407852>>, <<312.717224,181.289932,108.537407>>, 33.000000, <<327.3437, 163.4836, 102.4060>>, 70.6959 , <<5.0, 7.0, 8.5>>, TRUE)
				REPOSITION_PLAYERS_VEHICLE(<<327.3437, 163.4836, 102.4060>>, 70.6959)
			
				SET_CLOCK_TIME(22, 00, 00)
			
				DELETE_PED(pedLimoDriver)
			
				FADE_IN_IF_NEEDED()
			
				//SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
				CLEAR_AREA_OF_PEDS(<<-826.457886,180.473969,71.133858>>, 100.00, TRUE)
				bStopAudioStream = FALSE
								
				i_current_event = 6
			ENDIF
		BREAK
	
		CASE 6
		CASE 7
			
			CREATE_RED_CARPET_CARS_IF_DONT_EXIST()
								
			IF GET_CUTSCENE_TIME() > 111500.0
			AND i_current_event = 6
				IF PREPARE_MUSIC_EVENT("SOL5_START")
					TRIGGER_MUSIC_EVENT("SOL5_START")
					//REQUEST_STAGE_ASSETS(STAGE_MOVIE_PREMIERE_CUT)
					i_current_event = 7
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 60000.00
			AND bStopAudioStream = FALSE
				STOP_STREAM()
				bStopAudioStream = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			OR WAS_CUTSCENE_SKIPPED()
				RESET_GAME_CAMERA()
			ENDIF
			
			IF WAS_CUTSCENE_SKIPPED()
				i_current_event = 8
			ELSE			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy")
					IF NOT IS_PED_INJURED(pedJimmy)
					//	TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedJImmy, PLAYER_PED_ID(), <<-1.5, -0.5, 0.0>>, PEDMOVE_RUN)
						SET_PED_AS_GROUP_MEMBER(pedJimmy, GET_PED_GROUP_INDEX(PLAYER_PED_ID()))
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedJimmy, VS_FRONT_RIGHT)
						SET_GROUP_FORMATION_SPACING(GET_PLAYER_GROUP(PLAYER_ID()), 3.5)
						
						FORCE_PED_MOTION_STATE(pedJImmy, MS_ON_FOOT_RUN)
					ENDIF
				ENDIF
																
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000, 0.0, TRUE, TRUE)
					i_current_event = 8
				ENDIF
			ENDIF
		BREAK
	
			
		CASE 8
		
			//1552342
			IF WAS_CUTSCENE_SKIPPED()
			AND PREPARE_MUSIC_EVENT("SOL5_START")
				TRIGGER_MUSIC_EVENT("SOL5_START")
				//SCRIPT_ASSERT("Skipped")
				NEW_LOAD_SCENE_START(<<300.004425,198.368469,105.073395>>, NORMALISE_VECTOR(<<299.849792,197.889984,105.048279>> - <<300.004425,198.368469,105.073395>>), 700.0)
				SETTIMERA(0)
				WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
				AND TIMERA() < 7000
					WAIT(0)					
				ENDWHILE
				iCreateCars = 0
				DELETE_VEHICLE(carMichael)
				WHILE NOT CREATE_RED_CARPET_CARS_IF_DONT_EXIST()
					WAIT(0)
				ENDWHILE
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<302.6840, 200.6019, 103.3724>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 171.7199 )
				
				IF NOT IS_ENTITY_DEAD(pedJimmy)
					IF NOT IS_PED_GROUP_MEMBER(pedJimmy, GET_PED_GROUP_INDEX(PLAYER_PED_ID()))
						SET_PED_AS_GROUP_MEMBER(pedJimmy, GET_PED_GROUP_INDEX(PLAYER_PED_ID()))
					ENDIF
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedJimmy, VS_FRONT_RIGHT)
					SET_GROUP_FORMATION_SPACING(GET_PLAYER_GROUP(PLAYER_ID()), 3.5)
					
					FORCE_PED_MOTION_STATE(pedJImmy, MS_ON_FOOT_RUN)
				ENDIF
				SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<298.6916, 193.0679, 103.3252>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 161.3360)
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000, 0.0, TRUE, TRUE)
				RESET_GAME_CAMERA()
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				//FADE_IN_IF_NEEDED()
				
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()	
				
				CREATE_POST_CUT_PAPS()
				
				REMOVE_ANIM_DICT("missmic4premiere")
				
				IF WAS_CUTSCENE_SKIPPED()
								
					WHILE NOT PREPARE_MUSIC_EVENT("SOL5_START")
						WAIT(0)
					ENDWHILE
					TRIGGER_MUSIC_EVENT("SOL5_START")
				ENDIF
				
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("Distant_Camera_Flash")
				
				iMissionStartTime = GET_GAME_TIMER()
				
				//RESET_GAME_CAMERA()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)				
								
				SET_PED_POPULATION_BUDGET(3)
				SET_VEHICLE_POPULATION_BUDGET(3)
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				IF IS_SCREEN_FADED_OUT()
					WHILE NOT CREATE_RED_CARPET_CARS_IF_DONT_EXIST()
						WAIT(0)
					ENDWHILE
					WAIT(0)
					DO_SCREEN_FADE_IN(500)
				ENDIF
				i_current_event = 0
				mission_stage = STAGE_GET_TO_MICHAELS_HOUSE
			ENDIF
		BREAK
	ENDSWITCH
	
	SET_CLOCK_TIME(iTimeOfTrigger, iTimeOfTriggerMinutes, 0)
	
ENDPROC


#IF IS_DEBUG_BUILD

PROC debugRoutines()

	
		
ENDPROC



#ENDIF


FUNC INT GET_REPLAY_STAGE_FROM_MISSION_STAGE(MISSION_STAGE_FLAG thisStage)
	
	INT iReplayNumber
	
	iReplayNumber = ENUM_TO_INT(thisStage) - ENUM_TO_INT(STAGE_GET_TO_MICHAELS_HOUSE)
	
	IF iReplayNumber < 0
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Sol5 - trying to set replay to a negative value")
		#ENDIF
		iReplayNumber = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("****** --- GET_REPLAY_STAGE_FROM_MISSION_STAGE: ", iReplayNumber)
	#ENDIF
	
	RETURN iReplayNumber

ENDFUNC

BOOL bKeepAmandaConditionsReturningTrue

FUNC BOOL HAVE_SAVE_AMANDA_CONDITIONS_TRIGGERED()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInsideHouseCoords, <<7.0, 7.0, 2.0>>)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.877625,181.520370,70.865524>>, <<-816.268616,178.350540,75.188087>>, 7.000000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.345337,175.497055,70.260384>>, <<-794.602661,184.461853,82.473396>>, 12.000000)
	OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedHostagetakerAmanda), 1.5)
	OR GET_ENTITY_HEALTH(pedHostagetakerAmanda) < 1000 //1978108
	OR IS_PED_INJURED(pedHostagetakerAmanda) //1978108
	OR bKeepAmandaConditionsReturningTrue
		bKeepAmandaConditionsReturningTrue = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

//***************************************************************
//****************** MISSION STAGES *****************************
//***************************************************************

BOOL bCareAboutLimo
BOOL bReachedDestination
BOOL bJimmyCommentsOnCars = FALSE
BOOL bGuysRunIn = FALSE

PED_INDEX pedMerryWeatherRunInHouse1
PED_INDEX pedMerryWeatherRunInHouse2

VEHICLE_INDEX viPlayersLastVehicle
BOOL bWarnedLeavingJimmy = FALSE

//INT iTimeOfLastJimmyAnim
//INT	iRandomIdle
//STRING rndString = "IDLE_D"

PROC stageGetToMichaelsHouse()

	HANDLE_CINEMA_DOOR()
		
	IF i_current_event > 0
	AND bCareAboutLimo = TRUE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(pedJimmy)
			//CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			TRIGGER_MUSIC_EVENT("SOL5_LIMO_ENTERED")
			
			START_AUDIO_SCENE("MI_4_GET_TO_THE_HOUSE")
			
			bCareAboutLimo = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedJimmy)
		SET_PED_DESIRED_MOVE_BLEND_RATIO(pedJimmy, PEDMOVE_RUN)
	ENDIF

	
	DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, TRUE)
	
	SWITCH i_current_event
		
		CASE 0
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, TRUE, FALSE)
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
		
			sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD(vMichaelsHouse)
			SET_BLIP_AS_FRIENDLY(sLocatesData.LocationBlip, TRUE)
			
			REQUEST_STAGE_ASSETS(STAGE_GET_TO_MICHAELS_HOUSE)
		
			IF navBlockArea <> 0
				REMOVE_NAVMESH_BLOCKING_OBJECT(navBlockArea)
			ENDIF
			g_replay.iReplayInt[0] = 0
			
			IF NOT IS_PED_INJURED(pedJimmy)
				SET_ENTITY_COORDS(pedJimmy, <<301.4036, 198.9978, 103.1659>>)
				SET_ENTITY_HEADING(pedJimmy, 167.8465)
			ENDIF
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_GET_TO_MICHAELS_HOUSE")
			IF NOT IS_PED_INJURED(pedJimmy)
				SET_PED_AS_FRIENDLY(pedJimmy)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(carMichael)
				REQUEST_MODEL(STRETCH)
				WHILE NOT HAS_MODEL_LOADED(STRETCH)
					WAIT(0)
				ENDWHILE
				carMichael = CREATE_STRETCH()
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(carMichael2)
				REQUEST_MODEL(VACCA)
				WHILE NOT HAS_MODEL_LOADED(VACCA)
					WAIT(0)
				ENDWHILE
				carMichael2 = CREATE_SPORTS_CAR()
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(STRETCH)
			SET_MODEL_AS_NO_LONGER_NEEDED(VACCA)
			
			IF NOT IS_PED_GROUP_MEMBER(pedJimmy, GET_PED_GROUP_INDEX(PLAYER_PED_ID()))
				SET_PED_AS_GROUP_MEMBER(pedJimmy, GET_PED_GROUP_INDEX(PLAYER_PED_ID()))
			ENDIF
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedJimmy, VS_FRONT_RIGHT)
			SET_GROUP_FORMATION_SPACING(GET_PLAYER_GROUP(PLAYER_ID()), 3.5)
				
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE)
			bCareAboutLimo = TRUE
			bReachedDestination = FALSE
			bJimmyCommentsOnCars = FALSE
			bGuysRunIn = FALSE
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING) 
						
			FADE_IN_IF_NEEDED()
			
			SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIC4_HOME_TIME)
						
			TRIGGER_MUSIC_EVENT("SOL5_GAMEPLAY_STARTS")
			bDisplayTimer = FALSE
			i_current_event++
		BREAK
		
		CASE 1
				
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_RUN", CONV_PRIORITY_VERY_HIGH)
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
				
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_GETIN", CONV_PRIORITY_VERY_HIGH)
					i_current_event++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 3
		
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND bDisplayTimer = FALSE
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_DEVIN, "SOL5_DEVTXT", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
				iMissionStartTime = GET_GAME_TIMER()
				bDisplayTimer = TRUE
			ENDIF
			
			IF (NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			AND IS_PED_IN_ANY_VEHICLE(pedJimmy)
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMichaelsHouse, <<120.0, 120.0, 120.0>>, FALSE, FALSE)
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_BANT", CONV_PRIORITY_VERY_HIGH)
					TRIGGER_MUSIC_EVENT("SOL5_LIMO_ENTERED")
					i_current_event++
				ENDIF
			ENDIF	
		BREAK
		
		CASE 4		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMichaelsHouse, <<250.0, 250.0, 120.0>>, FALSE, FALSE)
				
				DELETE_PED(pedsMoviePremier[MOVP_PAP2])
				DELETE_PED(pedsMoviePremier[MOVP_PAP1])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Paparazzi_01)
				
				DELETE_OBJECT(oiRedCarpetObjects[2])
				DELETE_OBJECT(oiRedCarpetObjects[3])
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_pap_camera_01)
				
				REMOVE_ANIM_DICT("missmic4premiere")
				
				REQUEST_STAGE_ASSETS(STAGE_SAVE_AMANDA)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 5
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_SAVE_AMANDA)
				CREATE_STAGE_ASSETS(STAGE_SAVE_AMANDA)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 6
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMichaelsHouse) < 90.00
				TRIGGER_MUSIC_EVENT("SOL5_IN_DRIVEWAY")
				i_current_event++
			ENDIF
		BREAK
						
	ENDSWITCH

	//Deal with pausing conversation
	IF i_current_event > 3
			
		//Pause conversation if you get a wanted level
		IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
		AND bReachedDestination = FALSE
			PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
		ELSE
			IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), pedJImmy)
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF i_current_event > 1
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE)
		IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(sLocatesData, vMichaelsHouse, <<12.0, 4.0, 12.0>>, FALSE, pedJimmy, NULL, NULL, "SOL5_GET2HSE", "CMN_JLEAVE", "", "", "CMN_JLEAVE", FALSE, TRUE, TRUE) //, NULL, TRUE)	
		OR (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-822.901489,171.583603,68.902214>>, <<-774.385681,170.937317,83.796524>>, 31.750000))
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInsideHouseCoords, <<7.0, 7.0, 2.0>>)
			SET_MAX_WANTED_LEVEL(0)
			bReachedDestination = TRUE
		ENDIF
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)	
			SET_BLIP_AS_FRIENDLY(sLocatesData.LocationBlip, TRUE)
		ENDIF
	ENDIF
		
	IF i_current_event >= 4
		IF TIMERA() > 17000
		AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 15.0
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND IS_CONVERSATION_STATUS_FREE()
			AND ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(), pedJimmy)
			AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)	
				IF GET_RANDOM_BOOL()
					//Jimmy
					IF GET_RANDOM_BOOL()
						CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL_IG4", CONV_PRIORITY_VERY_HIGH)
						SETTIMERA(0)		
					ELSE
						CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL_QUICK", CONV_PRIORITY_VERY_HIGH)
						SETTIMERA(0)		
					ENDIF
				ELSE
					//Michael
					CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_GO", CONV_PRIORITY_VERY_HIGH)
					SETTIMERA(0)		
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), MIC4_DAMAGE)
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> viPlayersLastVehicle
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MIC4_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) 
			
			viPlayersLastVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						
			IF viPlayersLastVehicle = carMichael
				INFORM_MISSION_STATS_OF_INCREMENT(MIC4_VEHICLE_CHOSEN, 0) 
			ELIF viPlayersLastVehicle = carMichael2
				INFORM_MISSION_STATS_OF_INCREMENT(MIC4_VEHICLE_CHOSEN, 1) 
			ELSE
				INFORM_MISSION_STATS_OF_INCREMENT(MIC4_VEHICLE_CHOSEN, 2) 
			ENDIF
			
		ENDIF
	ELSE
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, MIC4_CAR_DAMAGE)
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
	ENDIF
	
	IF bJimmyCommentsOnCars = FALSE
		IF IS_ENTITY_IN_ANGLED_AREA(pedJImmy, <<-839.350891,160.104462,65.927368>>, <<-814.017700,183.767914,82.079521>>, 20.250000)
			IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_CARS", CONV_PRIORITY_VERY_HIGH)
				bJimmyCommentsOnCars = TRUE
			ENDIF
						
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-812.868896,165.500397,70.477791>>, <<-848.297791,167.817780,62.378845>>, 24.750000)		
			IF bGuysRunIn = FALSE
				pedMerryWeatherRunInHouse1 = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-822.2332, 183.0674, 70.9162>>, 199.0781)
				pedMerryWeatherRunInHouse2 = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-823.2229, 181.3412, 70.7021>>, 261.9175)
				
				SET_PED_LEG_IK_MODE(pedMerryWeatherRunInHouse1, LEG_IK_FULL)
				SET_PED_LEG_IK_MODE(pedMerryWeatherRunInHouse2, LEG_IK_FULL)
				SET_PED_AS_MERRYWEATHER(pedMerryWeatherRunInHouse1)
				SET_PED_AS_MERRYWEATHER(pedMerryWeatherRunInHouse2)
			
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeatherRunInHouse1, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeatherRunInHouse2, TRUE)
			
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedMerryWeatherRunInHouse1, <<-806.4446, 174.9243, 71.8447>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP)
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedMerryWeatherRunInHouse2, <<-806.4446, 174.9243, 71.8447>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP)
								
				bGuysRunIn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bGuysRunIn = TRUE
		IF NOT IS_ENTITY_DEAD(pedMerryWeatherRunInHouse1)
			SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(pedMerryWeatherRunInHouse1, TRUE)
		ENDIF
		IF NOT IS_ENTITY_DEAD(pedMerryWeatherRunInHouse2)
			SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(pedMerryWeatherRunInHouse2, TRUE)
		ENDIF
	ENDIF	
	
	IF i_current_event > 5
	AND bReachedDestination
				
			TRIGGER_MUSIC_EVENT("SOL5_IN_DRIVEWAY")
					
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				carMichaelIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(carMichaelIsIn, 3.0, 5)
					WAIT(0)
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Meltdown")
				ENDWHILE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
//			KILL_FACE_TO_FACE_CONVERSATION()
//			WHILE IS_SCRIPTED_CONVERSATION_ONGOING()
//				WAIT(0)
//				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Meltdown")
//			ENDWHILE
			KILL_FACE_TO_FACE_CONVERSATION()
			REPLAY_RECORD_BACK_FOR_TIME(6.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(MESA3)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(badguyCarOutsideHouse1)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(badguyCarOutsideHouse2)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carMichael)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carMichael2)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carMichaelIsIn)
					
			SET_MODEL_AS_NO_LONGER_NEEDED(STRETCH)
			i_current_event = 0 
			mission_stage = STAGE_ENTER_THE_HOUSE
		
	ENDIF
		
ENDPROC

SEQUENCE_INDEX seqJimmy
BOOl bJimmyGotToHouseOk
BOOL bUnlockDoors = FALSE
BOOL bJimmySpoken = FALSE
BOOL bMichaelSaysDaddysHome = FALSE
VEHICLE_INDEX carPlayerIsIn

PROC stageEnterTheHouse()
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2195216
	
	//WEAPON_TYPE plWeaponType

	IF IS_PED_INJURED(pedJimmy)
	ELSE
		SET_PED_IS_IGNORED_BY_AUTO_OPEN_DOORS(pedJimmy, TRUE)
	ENDIF
		
	IF NOT IS_ENTITY_DEAD(pedAmanda)
		SET_PED_RESET_FLAG(pedAmanda, PRF_DisablePotentialBlastReactions, TRUE)
		SET_PED_RESET_FLAG(pedAmanda, PRF_BlockWeaponReactionsUnlessDead, TRUE)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
		SET_PED_RESET_FLAG(pedHostagetakerAmanda, PRF_DisablePotentialBlastReactions, TRUE)
		SET_PED_RESET_FLAG(pedHostagetakerAmanda, PRF_BlockWeaponReactionsUnlessDead, TRUE)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedMerryWeatherRunInHouse1)
	AND DOES_ENTITY_EXIST(pedMerryWeatherRunInHouse1)
		IF IS_ENTITY_AT_COORD(pedMerryWeatherRunInHouse1, <<-809.8790, 180.0593, 71.1531>>, <<3.0, 3.0, 3.0>>)
			DELETE_PED(pedMerryWeatherRunInHouse1)
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			carPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
			
			IF DOES_ENTITY_EXIST(pedMerryWeatherRunInHouse1)
				IF DOES_ENTITY_EXIST(carPlayerIsIn)
					IF IS_ENTITY_TOUCHING_ENTITY(pedMerryWeatherRunInHouse1, carPlayerIsIn)
						SET_ENTITY_HEALTH(pedMerryWeatherRunInHouse1, 0)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(pedMerryWeatherRunInHouse1)
				IF IS_ENTITY_TOUCHING_ENTITY(pedMerryWeatherRunInHouse1, PLAYER_PED_ID())
					SET_ENTITY_HEALTH(pedMerryWeatherRunInHouse1, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedMerryWeatherRunInHouse2)
	AND DOES_ENTITY_EXIST(pedMerryWeatherRunInHouse2)
		IF IS_ENTITY_AT_COORD(pedMerryWeatherRunInHouse2, <<-809.8790, 180.0593, 71.1531>>, <<3.0, 3.0, 3.0>>)
			DELETE_PED(pedMerryWeatherRunInHouse2)
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			carPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
			IF DOES_ENTITY_EXIST(pedMerryWeatherRunInHouse2)
				IF DOES_ENTITY_EXIST(carPlayerIsIn)				
					IF IS_ENTITY_TOUCHING_ENTITY(pedMerryWeatherRunInHouse2, carPlayerIsIn)
						SET_ENTITY_HEALTH(pedMerryWeatherRunInHouse2, 0)
					ENDIF				
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(pedMerryWeatherRunInHouse2)
				IF IS_ENTITY_TOUCHING_ENTITY(pedMerryWeatherRunInHouse2, PLAYER_PED_ID())
					SET_ENTITY_HEALTH(pedMerryWeatherRunInHouse2, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH i_current_event
		
		CASE 0
		
			IF (NOT IS_SCRIPTED_CONVERSATION_ONGOING() AND CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_JHIDE", CONV_PRIORITY_VERY_HIGH))
			OR HAVE_SAVE_AMANDA_CONDITIONS_TRIGGERED()	//1978108
				bDisplayTimer = TRUE
				FADE_IN_IF_NEEDED()
				SET_MAX_WANTED_LEVEL(0)
				blipAmanda = CREATE_BLIP_FOR_PED(pedAmanda, FALSE)
			
				//Stop randoms spawning in michaels garden.
				ADD_SCENARIO_BLOCKING_AREA(<<-835.060852,148.409912,62.360283>>, <<-773.088623,198.918427,75.155563>>)
				//SET_PED_NON_CREATION_AREA(<<-835.060852,148.409912,62.360283>>, <<-773.088623,198.918427,75.155563>>)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_VinDouche_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Vinewood_03)
								
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_ENTER_THE_HOUSE")
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				REMOVE_PED_FROM_GROUP(pedJimmy)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(pedJimmy, FALSE)
				
				IF NOT IS_ENTITY_DEAD(pedJimmy)
					//Wait until navmesh break is fixed.
					//TASK_FOLLOW_NAV_MESH_TO_COORD(pedJimmy, << -805.4232, 191.3491, 71.8352 >>, PEDMOVE_SPRINT)
					OPEN_SEQUENCE_TASK(seqJimmy)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -811.6075, 192.6608, 72.0095 >>, PEDMOVE_SPRINT)
						TASK_COWER(NULL, INFINITE_TASK_TIME)
					CLOSE_SEQUENCE_TASK(seqJimmy)
					TASK_PERFORM_SEQUENCE(pedJimmy, seqJimmy)
					CLEAR_SEQUENCE_TASK(seqJimmy)		
				ENDIF
				bJimmySpoken = FALSE
				SET_PLAYER_ANGRY(PLAYER_PED_ID(), TRUE)
				bMichaelSaysDaddysHome = FALSE
				bUnlockDoors = FALSE
				i_current_event++
			ENDIF
		BREAK
		
		CASE 1
		CASE 2	
		CASE 3
				
//			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
//			AND i_current_event = 1
//			//	IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_DOORSTP", CONV_PRIORITY_VERY_HIGH)	
//					
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "SOL5_ARAA", "MICHAEL", SPEECH_PARAMS_FORCE)
//					
//					i_current_event = 2
//			//	ENDIF
//			ENDIF
		
			
			IF bJimmySpoken = FALSE
				//IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_JRUN", CONV_PRIORITY_VERY_HIGH)	
				IF IS_PED_ON_FOOT(pedJimmy)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedJimmy, "SOL5_BGAA", "JIMMY", SPEECH_PARAMS_FORCE)
					bJimmySpoken = TRUE
				ENDIF
			ENDIF
		
		
			IF IS_PED_ON_FOOT(PLAYER_PED_ID())			
				IF NOT IS_AUDIO_SCENE_ACTIVE("MI_4_SAVE_FAMILY_MAIN")
					STOP_AUDIO_SCENE("MI_4_GET_TO_THE_HOUSE")
					START_AUDIO_SCENE("MI_4_SAVE_FAMILY_MAIN")
				ENDIF
			ENDIF
			
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInsideHouseCoords, <<14.0, 14.0, 3.0>>)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.345337,175.497055,70.260384>>, <<-794.602661,184.461853,82.473396>>, 12.000000)
			OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedHostagetakerAmanda), 1.5)	
				IF bUnlockDoors = FALSE
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
               			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
               			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
            		ENDIF
            		IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
            			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
               			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
            		ENDIF
//					KILL_ANY_CONVERSATION()
					bUnlockDoors = TRUE
				ENDIF			
			ENDIF
				
//			IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
//				SET_PED_RESET_FLAG(pedHostagetakerAmanda, PRF_BlockWeaponReactionsUnlessDead, TRUE)
//			ENDIF
			
			IF HAVE_SAVE_AMANDA_CONDITIONS_TRIGGERED()
				IF bUnlockDoors = FALSE
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
               			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, FALSE, FALSE)
               			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
            		ENDIF
            		IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
            			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, FALSE, FALSE)
               			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
            		ENDIF
					
					bUnlockDoors = TRUE
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("MI_4_SAVE_FAMILY_MAIN")
					STOP_AUDIO_SCENE("MI_4_GET_TO_THE_HOUSE")
					START_AUDIO_SCENE("MI_4_SAVE_FAMILY_MAIN")
				ENDIF
				
				IF bMichaelSaysDaddysHome = FALSE		
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "SOL5_ARAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
					bMichaelSaysDaddysHome = TRUE
				ENDIF
				
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_AMA", CONV_PRIORITY_VERY_HIGH)	
				OR HAVE_SAVE_AMANDA_CONDITIONS_TRIGGERED()
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_JIMMYDISANTO)
					IF NOT IS_ENTITY_ON_SCREEN(pedJimmy)
						DELETE_PED(pedJimmy)
					ELSE
						SET_PED_KEEP_TASK(pedJimmy, TRUE)
						SET_PED_AS_NO_LONGER_NEEDED(pedJimmy)
					ENDIF
					
					DELETE_PED(pedMerryWeatherRunInHouse1)
					DELETE_PED(pedMerryWeatherRunInHouse2)
							
					i_current_event = 0
					mission_stage = STAGE_SAVE_AMANDA
				ENDIF
			ENDIF	
		BREAK
		
	ENDSWITCH

ENDPROC

BOOL bDialogueDisplayed
BOOL bDialogueDisplayed2
BOOL bDialogueDisplayed3
INT iTimeOfLastMichaelAmbientSpeech
INT iTimeOfLastAmandaWhimper
INT iTimeOfLastTraceyWhimper
INT iTimeOfLastBaddyGrunt
BOOL bFailForFiringButMissing
BOOL bAmandasDialogue
INT iTimeOfScreaming

PROC stageSaveAmanda()

	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2195216
	
	PED_BONETAG pedBoneTag

	//Fail conditions
//	IF IS_ENTITY_DEAD(pedTracey)
//		SET_PED_RESET_FLAG(pedTracey, PRF_DisablePotentialBlastReactions, TRUE)
//	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedAmanda)
		SET_PED_RESET_FLAG(pedAmanda, PRF_BlockWeaponReactionsUnlessDead, TRUE)
		SET_PED_RESET_FLAG(pedAmanda, PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
	AND i_current_event < 99
		SET_PED_RESET_FLAG(pedHostagetakerAmanda, PRF_BlockWeaponReactionsUnlessDead, TRUE)
		SET_PED_RESET_FLAG(pedHostagetakerAmanda, PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
		
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() 
	
	SWITCH i_current_event
		
		CASE 0
			SPECIAL_ABILITY_CHARGE_ABSOLUTE(PLAYER_ID(), 30, TRUE)
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER))
				ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER),V_ILEV_MM_DOORDAUGHTER, <<-802.702,176.176,76.890>>)
			ENDIF
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER), 0.0)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER), DOORSTATE_LOCKED)
			
			SET_MAX_WANTED_LEVEL(0)
			START_AUDIO_SCENE("MI_4_SAVE_FAMILY_MAIN")
			START_AUDIO_SCENE("MI_4_SAVE_AMANDA")
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-816.72, 179.10, 72.83>>, 2.0, V_ILEV_MM_DOORM_L)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORM_L, <<-816.72, 179.10, 72.83>>, FALSE, 0.0)
			ENDIF	
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-816.11, 177.51, 72.83>>, 2.0, V_ILEV_MM_DOORM_R)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORM_R, <<-816.11, 177.51, 72.83>>, FALSE, 0.0)
			ENDIF
			bDisplayTimer = FALSE
			SET_CREATE_RANDOM_COPS(FALSE)
			SET_CREATE_RANDOM_COPS_ON_SCENARIOS(FALSE)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_SAVE_AMANDA")
			FADE_IN_IF_NEEDED()
			//blipPedHostagetaker = CREATE_BLIP_FOR_PED(pedHostagetakerAmanda, TRUE)
			//PRINT_NOW("SOL5_SAVEAM", DEFAULT_GOD_TEXT_TIME, 1)			
			SETTIMERA(0)
			SET_PLAYER_ANGRY(PLAYER_PED_ID(), TRUE)
			REQUEST_STAGE_ASSETS(STAGE_SAVE_TRACEY)
			
			SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 
			bAmandasDialogue = FALSE
			i_current_event++
		BREAK
		
		CASE 1		
		CASE 2
		
			IF i_current_event = 1
				IF PREPARE_MUSIC_EVENT("SOL5_FRONT_DOORS")
					TRIGGER_MUSIC_EVENT("SOL5_FRONT_DOORS")
					i_current_event = 2
				ENDIF
			ENDIF
					
			IF bAmandasDialogue = FALSE
				//IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_AMA", CONV_PRIORITY_VERY_HIGH)	
					bAmandasDialogue = TRUE
				//ENDIF
			ENDIF
			
			//IF i_current_event = 3
				//Pause conversation
				PRINTLN("GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAmanda):", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAmanda))
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAmanda) > 15.0
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ELSE
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
				ENDIF
				
			//ENDIF
			
			//Michael says let go of her etc..
			IF GET_GAME_TIMER() - iTimeOfLastMichaelAmbientSpeech > 4000
			AND IS_ENTITY_ON_SCREEN(pedAmanda)
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-812.758972,177.089920,71.171242>>, <<-814.894348,182.381500,75.253029>>, 5.000000)

			//AND IS_ENTITY_ON_SCREEN(pedAmanda)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "SOL5_BMAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
				//SCRIPT_ASSERT("say shit for fucks sake!")
				iTimeOfLastMichaelAmbientSpeech = GET_GAME_TIMER()
			ENDIF
						
			//Fail
			//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedHostagetakerAmanda) < 4.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.401855,182.468979,73.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_GRENADE, TRUE)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_SMOKEGRENADE, TRUE)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_MOLOTOV, TRUE)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_STICKYBOMB, TRUE)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_RPG, TRUE)
			OR (IS_WEAPON_VALID(WEAPONTYPE_DLC_FLAREGUN) AND IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_DLC_FLAREGUN, TRUE))
			OR (IS_WEAPON_VALID(WEAPONTYPE_DLC_FIREWORK) AND IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-811.401855,182.468979,71.153091>>, <<-805.193420,184.431122,76.270195>>, 3.250000, WEAPONTYPE_DLC_FIREWORK, TRUE))
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAmanda, PLAYER_PED_ID())
			OR bFailForFiringButMissing
			OR TIMERA() > 10000
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				i_current_event = 99
			ENDIF
	
			//PRINTLN("GET_ENTITY_HEALTH(pedHostagetakerAmanda):", GET_ENTITY_HEALTH(pedHostagetakerAmanda))
			
			IF GET_ENTITY_HEALTH(pedHostagetakerAmanda) < 1000.0
				
				REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
				
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				
				DETACH_ENTITY(oiAmandasShoe, TRUE, FALSE)
				SET_OBJECT_AS_NO_LONGER_NEEDED(oiAmandasShoe)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_AMANDA_SHOE)
				
				STOP_AUDIO_SCENE("MI_4_SAVE_AMANDA")
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//				WAIT(0)
//				IF NOT IS_ENTITY_DEAD(pedAmanda)
//					PLAY_PAIN(pedAmanda, AUD_DAMAGE_REASON_SCREAM_SHOCKED)
//				ENDIF
									
				GET_PED_LAST_DAMAGE_BONE(GET_PED_INDEX_FROM_ENTITY_INDEX(pedHostagetakerAmanda), pedBoneTag)
				IF pedBoneTag = BONETAG_HEAD
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIC4_HEADSHOT_RESCUE)
				ENDIF
				
				TRIGGER_MUSIC_EVENT("SOL5_AMANDA_SAVED")
				KILL_ANY_CONVERSATION()
				REMOVE_BLIP(blipPedHostagetaker)
				
				REMOVE_BLIP(blipAmanda)			
				/* START SYNCHRONIZED SCENE -  */
				iSceneId = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
								
				IF NOT IS_PED_INJURED(pedAmanda)
					TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_shoot_merry_at", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT )
				ENDIF
				IF NOT IS_PED_INJURED(pedHostagetakerAmanda)
					TASK_SYNCHRONIZED_SCENE(pedHostagetakerAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_shoot_merry_mw", NORMAL_BLEND_IN, SLOW_BLEND_OUT )
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAmanda)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerAmanda)
				
				iTimeOfScreaming = GET_GAME_TIMER()			
				
				bFailForFiringButMissing = FALSE				
				i_current_event = 4
			ENDIF
			
		BREAK
		
		CASE 4
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.05
					IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
						APPLY_DAMAGE_TO_PED(pedHostagetakerAmanda, 1000, TRUE)
						i_current_event = 5
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() - iTimeOfScreaming > 750
				IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
					APPLY_DAMAGE_TO_PED(pedHostagetakerAmanda, 1000, TRUE)
					i_current_event = 5
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_SAVE_TRACEY)
				CREATE_STAGE_ASSETS(STAGE_SAVE_TRACEY)
				i_current_event= 0
				bDialogueDisplayed = FALSE
				mission_stage = STAGE_SAVE_TRACEY
			ENDIF
		BREAK
						
		CASE 99
						
			/* START SYNCHRONIZED SCENE -  */
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_SCREAM_TERROR)
			ENDIF
			
			DETACH_ENTITY(oiAmandasShoe, TRUE, FALSE)
			SET_OBJECT_AS_NO_LONGER_NEEDED(oiAmandasShoe)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_AMANDA_SHOE)
			
			iSceneId = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
				
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
		
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_shoot_amanda_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
				TASK_SYNCHRONIZED_SCENE(pedHostagetakerAmanda, iSceneId, "misssolomon_5@stairs", "sol_5_stair_shoot_amanda_mw", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
			ENDIF
			
			i_current_event = 100	
		
		BREAK
		
		CASE 100
		
			//Kill Amanda causing fail
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.01
					TRIGGER_MUSIC_EVENT("SOL5_FAIL")
					IF NOT IS_PED_INJURED(pedHostagetakerAmanda)
					AND NOT IS_ENTITY_DEAD(pedAmanda)
						SET_PED_SHOOTS_AT_COORD(pedHostagetakerAmanda, GET_PED_BONE_COORDS(pedAmanda, BONETAG_HEAD, <<0.0, 0.0, 0.0>>))
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
					
					i_current_event = 101
				ENDIF
			ENDIF
		BREAK
		
		CASE 101
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.3					
					IF NOT IS_ENTITY_DEAD(pedAmanda)
						APPLY_DAMAGE_TO_PED(pedAmanda, 200, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedHostagetakerAmanda)
						SET_PED_LEG_IK_MODE(pedHostagetakerAmanda, LEG_IK_FULL)
						TASK_COMBAT_PED(pedHostagetakerAmanda, PLAYER_PED_ID())
					ENDIF
					i_current_event = 102
				ENDIF
			ENDIF
		
		BREAK
		
	ENDSWITCH

	IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedAmanda), 2.5)
		bFailForFiringButMissing = TRUE
	ENDIF
	
ENDPROC


FUNC BOOL IS_TRACEY_GOING_TO_GET_EXECUTED()

	BOOL bDLCProjectileWeaponUsed

	IF IS_WEAPON_VALID(WEAPONTYPE_DLC_FLAREGUN)
		IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_DLC_FLAREGUN, TRUE)
			bDLCProjectileWeaponUsed = TRUE
		ENDIF
	ENDIF
	
	IF IS_WEAPON_VALID(WEAPONTYPE_DLC_FIREWORK) 
		IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_DLC_FIREWORK, TRUE)
			bDLCProjectileWeaponUsed = TRUE
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedHostagetakerTracey) < 1.9
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_GRENADE, TRUE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_SMOKEGRENADE, TRUE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_MOLOTOV, TRUE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_STICKYBOMB, TRUE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-802.453857,176.164154,75.826729>>, <<-799.845032,169.110443,79.256020>>, 3.750000, WEAPONTYPE_RPG, TRUE)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedTracey, PLAYER_PED_ID())
	OR bDLCProjectileWeaponUsed
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
				
ENDFUNC

// To fix url:bugstar:2088835
FUNC BOOL DID_PLAYER_RUN_PAST_AMANDA()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.389404,178.807816,75.740738>>, <<-806.420288,176.971375,76.748322>>, 3.75)
	AND IS_SPHERE_VISIBLE(<<-802.889160,174.498154,76.240738>>, 1.5) 
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

INT iSceneIdAmandaIdle
INT iTraceyExecutionTimer

PROC stageSaveTracey()

	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2195216
	
	PED_BONETAG pedBoneTag

	IF NOT IS_ENTITY_DEAD(pedTracey)
		IF i_current_event < 6// 4
			//Fail
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedTracey, PLAYER_PED_ID())
			OR IS_PED_INJURED(pedTracey)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedAmanda) 
				WAIT(0)
				IF NOT IS_ENTITY_DEAD(pedAmanda)
					PLAY_PAIN(pedAmanda, AUD_DAMAGE_REASON_SCREAM_TERROR)
				ENDIF
				
				PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_SCREAM_TERROR)
				
				bFailForFiringButMissing = TRUE
				MISSION_FLOW_SET_FAIL_REASON("SOL5_TDIED")
				mission_Failed()
			ENDIF
		ENDIF
			
		
		//SET_PED_RESET_FLAG(pedTracey, PRF_BlockWeaponReactionsUnlessDead, TRUE)
		SET_PED_RESET_FLAG(pedTracey, PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-802.703125,176.358841,75.740738>>, <<-799.600098,168.648849,79.703346>>, 4.500000)
		//Fail
		IF i_current_event < 5
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedAmanda) 
			WAIT(0)
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				PLAY_PAIN(pedAmanda, AUD_DAMAGE_REASON_SCREAM_TERROR)
			ENDIF
			
			PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_SCREAM_TERROR)
			
			bFailForFiringButMissing = TRUE
			MISSION_FLOW_SET_FAIL_REASON("SOL5_TDIED")
			mission_Failed()
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedAmanda)
		SET_PED_RESET_FLAG(pedAmanda, PRF_BlockWeaponReactionsUnlessDead, TRUE)
		SET_PED_RESET_FLAG(pedAmanda, PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
	AND i_current_event < 99
		SET_PED_RESET_FLAG(pedHostagetakerTracey, PRF_BlockWeaponReactionsUnlessDead, TRUE)
		SET_PED_RESET_FLAG(pedHostagetakerTracey, PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	
//	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-802.702,176.176,76.890>>, 2.0, V_ILEV_MM_DOORDAUGHTER)
//		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORDAUGHTER, <<-802.702,176.176,76.890>>, TRUE, 1.0)
//	ENDIF
	
	IF GET_GAME_TIMER() - iTimeOfScreaming > 800
	OR IS_SCREEN_FADED_OUT()
		//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		//AND i_current_event > 0
			IF bDialogueDisplayed = FALSE
				STOP_PED_SPEAKING(pedAmanda, TRUE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//SCRIPT_ASSERT("speak for fucks sake")
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_SHOT", CONV_PRIORITY_VERY_HIGH)	
					STOP_PED_SPEAKING(pedAmanda, FALSE)
					bDialogueDisplayed2 = FALSE
					bDialogueDisplayed = TRUE
				ENDIF				
			ENDIF
		//ENDIF
	ENDIF
	
	IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF) 
	AND bDialogueDisplayed = TRUE
	AND bDialogueDisplayed2 = FALSE
	AND i_current_event = 1		
		IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_TRACE", CONV_PRIORITY_VERY_HIGH)	
			bDialogueDisplayed2 = TRUE
		ENDIF		
	ENDIF
	
	IF bDialogueDisplayed = TRUE
	AND bDialogueDisplayed3 = FALSE
		IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_TRAC2", CONV_PRIORITY_VERY_HIGH)	
			START_AUDIO_SCENE("MI_4_SAVE_TRACY")
			bDialogueDisplayed3 = TRUE
		ENDIF
	ENDIF
	
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() 
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAmanda) > 18.0
		IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
		ENDIF
		ELSE
		IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		ENDIF
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	
	SWITCH i_current_event
		
		CASE 0	
		
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER))
				ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER),V_ILEV_MM_DOORDAUGHTER, <<-802.702,176.176,76.890>>)
			ENDIF
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER),1.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
								
			SET_MAX_WANTED_LEVEL(0)
			START_AUDIO_SCENE("MI_4_SAVE_FAMILY_MAIN")
			bDisplayTimer = FALSE			
			REQUEST_STAGE_ASSETS(STAGE_TRACEY_CAPTOR_SHOT)
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_SAVE_TRACEY")
			SET_CREATE_RANDOM_COPS(FALSE)
			SET_CREATE_RANDOM_COPS_ON_SCENARIOS(FALSE)
			IF IS_SCREEN_FADED_OUT()
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
									
					IF NOT IS_PED_INJURED(pedHostagetakerAmanda)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedHostagetakerAmanda, BONETAG_HEAD, <<0.0, 0.25, 0.0>>), GET_PED_BONE_COORDS(pedHostagetakerAmanda, BONETAG_HEAD, <<0.0, -1.0, 0.0>>), 200, TRUE)
					ENDIF
										
					LOAD_SCENE(<<-814.9095, 179.0777, 71.1592>>)
					
					SET_SYNCHRONIZED_SCENE_PHASE(iSceneId, 0.35)
				ENDIF
			ENDIF
			SET_PLAYER_ANGRY(PLAYER_PED_ID(), TRUE)
			SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 
			bFailForFiringButMissing = FALSE
			FADE_IN_IF_NEEDED()
			i_current_event ++
		BREAK
		
		CASE 1
			//Wait until Amanda is at the top of the stairs
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
			
				SET_FORCE_FOOTSTEP_UPDATE(pedAmanda, TRUE)
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.094
					IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
						APPLY_DAMAGE_TO_PED(pedHostagetakerAmanda, 1000, TRUE)
					ENDIF
				ENDIF					
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 1.0
				OR DID_PLAYER_RUN_PAST_AMANDA()
		
					IF NOT IS_ENTITY_DEAD(pedHostagetakerAmanda)
						APPLY_DAMAGE_TO_PED(pedHostagetakerAmanda, 1000, TRUE)
					ENDIF
					
					intMichaelsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.350, 172.900, 75.700 >>, "V_Michael")
									
					/* START SYNCHRONIZED SCENE -  */
					vAmandaScenePos= << -803.998, 175.910, 75.745 >>
					vAmandaSceneRot = << 0.000, 0.000, 21.120 >>
					iSceneId = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
					IF NOT IS_PED_INJURED(pedAmanda)
						TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_into_at", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedTracey)
						TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_into_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
						FORCE_ROOM_FOR_ENTITY(pedTracey, intMichaelsHouse, GET_HASH_KEY("V_Michael_1_Daught"))
						RETAIN_ENTITY_IN_INTERIOR(pedTracey, intMichaelsHouse)
					ENDIF
					IF NOT IS_PED_INJURED(pedHostagetakerTracey)
						TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_into_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
						FORCE_ROOM_FOR_ENTITY(pedHostagetakerTracey, intMichaelsHouse, GET_HASH_KEY("V_Michael_1_Daught"))
						RETAIN_ENTITY_IN_INTERIOR(pedHostagetakerTracey, intMichaelsHouse)
					ENDIF			
					
					SET_SYNCHRONIZED_SCENE_PHASE(iSceneId, 0.1)
					
					SETTIMERA(0)
					
					CLEAR_PRINTS()
					
					PLAY_PAIN(pedTracey, AUD_DAMAGE_REASON_SCREAM_TERROR)

					//PRINT_NOW("SOL5_SAVETRC", DEFAULT_GOD_TEXT_TIME, 1)
					blipPedTracey = CREATE_BLIP_FOR_PED(pedTracey)
					i_current_event ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		CASE 3
		
			//Wait until "bedroom intro" has completed.
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
			//AND i_current_event = 2
				
				SET_FORCE_FOOTSTEP_UPDATE(pedAmanda, TRUE)
				
//				IF iRandomVariation = -1
//					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAmanda) < 5.0
//						IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) < 0.346
//							SET_SYNCHRONIZED_SCENE_RATE (iSceneId, 3.5)
//						ENDIF
//					ENDIF
//					
//					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.346
//						SET_SYNCHRONIZED_SCENE_RATE(iSceneId, 1.0)
//					ENDIF
//				ELSE
//					SET_SYNCHRONIZED_SCENE_RATE(iSceneId, 1.0)	
//				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.99
				
					intMichaelsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.350, 172.900, 75.700 >>, "V_Michael")
				
					/* START SYNCHRONIZED SCENE -  */														
					iSceneId = CREATE_SYNCHRONIZED_SCENE(vTraceyScenePos, vTraceySceneRot)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
					
					iRandomVariation = GET_RANDOM_INT_IN_RANGE(0, 3)
					
					SWITCH iRandomVariation
					
						CASE 0
							IF NOT IS_ENTITY_DEAD(pedAmanda)
								TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_a_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedTracey)
								TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_a_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
								TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_a_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
							ENDIF
						BREAK
						
						CASE 1
							IF NOT IS_ENTITY_DEAD(pedAmanda)
								TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_b_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedTracey)
								TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_b_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
								TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_b_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
							ENDIF
						BREAK
						
						CASE 2
							IF NOT IS_ENTITY_DEAD(pedAmanda)
								TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_c_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedTracey)
								TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_c_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
								TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_c_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
							ENDIF
						BREAK
					
					ENDSWITCH
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAmanda)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTracey)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerTracey)
										
					i_current_event = 4
				ENDIF
			ENDIF
			
						
//			IF NOT IS_ENTITY_DEAD(pedTracey)
//				FORCE_ROOM_FOR_ENTITY(pedTracey, intMichaelsHouse, GET_HASH_KEY("V_Michael_1_Daught"))
//			ENDIF
//			IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)			
//				FORCE_ROOM_FOR_ENTITY(pedHostagetakerTracey, intMichaelsHouse, GET_HASH_KEY("V_Michael_1_Daught"))				
//			ENDIF
			
			//
//			IF i_current_event = 3
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -802.7369, 174.3312, 75.7408 >>, <<3.0, 3.0, 2.0>>)
//				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHostagetakerTracey, PLAYER_PED_ID())
//				OR IS_TRACEY_GOING_TO_GET_EXECUTED()
//				//IF CREATE_CONVERSATION(myScriptedSpeech, "FAM1AUD", "FAM1_BANT", CONV_PRIORITY_VERY_HIGH)				
//					
//					GET_PED_LAST_DAMAGE_BONE(GET_PED_INDEX_FROM_ENTITY_INDEX(pedHostagetakerTracey), pedBoneTag)
//					IF pedBoneTag = BONETAG_HEAD
//						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIC4_HEADSHOT_RESCUE)
//					ENDIF
//					
//					i_current_event = 4
//				ENDIF	
//			ENDIF	
		BREAK
		
		CASE 4
			TRIGGER_MUSIC_EVENT("SOL5_HOSTAGE_TAKER")
			REMOVE_BLIP(blipPedTracey)
			blipPedHostagetaker = CREATE_BLIP_FOR_PED(pedHostagetakerTracey, TRUE)
			PRINT_NOW("SOL5_SAVEAM", DEFAULT_GOD_TEXT_TIME, 1)
			iTraceyExecutionTimer = GET_GAME_TIMER()
			iTimeOfLastTraceyWhimper = GET_GAME_TIMER()
			i_current_event++
		BREAK
		
		CASE 5
		CASE 6
		
			//Michael says let go of her etc..
			IF GET_GAME_TIMER() - iTimeOfLastMichaelAmbientSpeech > 4000
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "SOL5_BNAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
				//SCRIPT_ASSERT("aarrgh")
				iTimeOfLastMichaelAmbientSpeech = GET_GAME_TIMER()
			ENDIF
			
			IF GET_GAME_TIMER() - iTimeOfLastAmandaWhimper > 3000
				PLAY_PAIN(pedAmanda, AUD_DAMAGE_REASON_WHIMPER)
				iTimeOfLastAmandaWhimper = GET_GAME_TIMER()
			ENDIF
			
			IF GET_GAME_TIMER() - iTimeOfLastTraceyWhimper > 4000
				PLAY_PAIN(pedTracey, AUD_DAMAGE_REASON_SCREAM_SCARED)
				iTimeOfLastTraceyWhimper = GET_GAME_TIMER()
			ENDIF
			
			IF GET_GAME_TIMER() - iTimeOfLastBaddyGrunt > 3000
				PLAY_PAIN(pedHostagetakerTracey, AUD_DAMAGE_REASON_EXHALE)
				iTimeOfLastBaddyGrunt = GET_GAME_TIMER()
			ENDIF
			
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
			//AND i_current_event = 2
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 1.0
				
					intMichaelsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.350, 172.900, 75.700 >>, "V_Michael")
				
					/* START SYNCHRONIZED SCENE -  */
					vAmandaScenePos= << -803.998, 175.910, 75.745 >>
					vAmandaSceneRot = << 0.000, 0.000, 21.120 >>
					iSceneId = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
					
					iRandomVariation = GET_RANDOM_INT_IN_RANGE(0, 3)
					
					SWITCH iRandomVariation
					
						CASE 0
							IF NOT IS_ENTITY_DEAD(pedAmanda)
								TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_a_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAmanda, TRUE)
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedTracey)
								TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_a_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTracey, TRUE)
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
								TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_a_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerTracey, TRUE)
							ENDIF
						BREAK
						
						CASE 1
							IF NOT IS_ENTITY_DEAD(pedAmanda)
								TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_b_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAmanda, TRUE)
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedTracey)
								TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_b_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTracey, TRUE)
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
								TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_b_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerTracey, TRUE)
							ENDIF
						BREAK
						
						CASE 2
							IF NOT IS_ENTITY_DEAD(pedAmanda)
								TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_c_at", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAmanda, TRUE)
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedTracey)
								TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_c_tt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTracey, TRUE)
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
								TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_strug_c_mw", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerTracey, TRUE)
							ENDIF
						BREAK
					
					ENDSWITCH
				
				ENDIF
			ENDIF
			
			//Fail
			IF IS_TRACEY_GOING_TO_GET_EXECUTED()
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedTracey, PLAYER_PED_ID())
			OR IS_PED_INJURED(pedTracey)
			OR GET_GAME_TIMER() - iTraceyExecutionTimer > 12000
			OR bFailForFiringButMissing
				
				REPLAY_RECORD_BACK_FOR_TIME(7.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedAmanda) 
				WAIT(0)
				IF NOT IS_ENTITY_DEAD(pedAmanda)
					PLAY_PAIN(pedAmanda, AUD_DAMAGE_REASON_SCREAM_TERROR)
				ENDIF
				
				PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_SCREAM_TERROR)
				
				bFailForFiringButMissing = TRUE
				i_current_event = 99
			ENDIF
						
		BREAK
							
		CASE 99
						
			intMichaelsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -803.350, 172.900, 75.700 >>, "V_Michael")
		
			/* START SYNCHRONIZED SCENE -  */
			vAmandaScenePos= << -803.998, 175.910, 75.8 >>
			vAmandaSceneRot = << 0.000, 0.000, 21.120 >>
			iSceneId = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
			SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
			
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_shoot_at", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )						
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedTracey)
				TASK_SYNCHRONIZED_SCENE(pedTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_shoot_tt", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
				TASK_SYNCHRONIZED_SCENE(pedHostagetakerTracey, iSceneId, "misssolomon_5@bedroom", "sol_5_bedroom_shoot_mw", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT )
			ENDIF
						
			i_current_event = 100	
			
		BREAK
		
		CASE 100
			//Kill tracey causing fail
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.01 //0.7
					TRIGGER_MUSIC_EVENT("SOL5_FAIL")			
					
						
					IF NOT IS_PED_INJURED(pedHostagetakerTracey)
					AND NOT IS_ENTITY_DEAD(pedTracey)
						SET_PED_SHOOTS_AT_COORD(pedHostagetakerTracey, GET_PED_BONE_COORDS(pedTracey, BONETAG_HEAD, <<0.0, 0.0, 0.0>>))
					ENDIF
					
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(pedTracey, BONETAG_HEAD, <<0.0, 0.0, 0.0>>) + <<0.0, 0.5, 0.0>>, GET_PED_BONE_COORDS(pedTracey, BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
					
					i_current_event = 101
				ENDIF
			ENDIF
		BREAK
		
		CASE 101
		
			IF NOT IS_PED_INJURED(pedHostagetakerTracey)
				IF IS_ENTITY_PLAYING_ANIM(pedHostagetakerTracey, "misssolomon_5@bedroom", "sol_5_bedroom_shoot_mw")
					SET_PED_RESET_FLAG(pedHostagetakerTracey, PRF_InstantBlendToAim, TRUE)
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				
				PRINTLN("Tracey fail:", GET_SYNCHRONIZED_SCENE_PHASE(iSceneId))
				
				IF NOT IS_ENTITY_DEAD(pedTracey)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.32 //0.7
						
						APPLY_FORCE_TO_ENTITY(pedTracey, APPLY_TYPE_IMPULSE, <<0.0, 1.5, 0.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
										
						APPLY_DAMAGE_TO_PED(pedTracey, 200, TRUE)
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.7 //0.7
					IF NOT IS_PED_INJURED(pedHostagetakerTracey)
						TASK_COMBAT_PED(pedHostagetakerTracey, PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerTracey, TRUE)
					ENDIF
					i_current_event = 102
				ENDIF
								
			ELSE
				IF NOT IS_PED_INJURED(pedHostagetakerTracey)
					TASK_COMBAT_PED(pedHostagetakerTracey, PLAYER_PED_ID())
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostagetakerTracey, TRUE)
				ENDIF
				i_current_event = 102
			ENDIF
		BREAK
		
		CASE 102
		
			IF NOT IS_PED_INJURED(pedHostagetakerTracey)
				IF IS_ENTITY_PLAYING_ANIM(pedHostagetakerTracey, "misssolomon_5@bedroom", "sol_5_bedroom_shoot_mw")
					SET_PED_RESET_FLAG(pedHostagetakerTracey, PRF_InstantBlendToAim, TRUE)
				ENDIF
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(pedTracey)
				APPLY_DAMAGE_TO_PED(pedTracey, 200, TRUE)
			ENDIF
			i_current_event = 103
		BREAK
		
		CASE 103
		
			IF NOT IS_PED_INJURED(pedHostagetakerTracey)
				IF IS_ENTITY_PLAYING_ANIM(pedHostagetakerTracey, "misssolomon_5@bedroom", "sol_5_bedroom_shoot_mw")
					SET_PED_RESET_FLAG(pedHostagetakerTracey, PRF_InstantBlendToAim, TRUE)
				ENDIF
			ENDIF
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				PRINTLN("stuff:", GET_SYNCHRONIZED_SCENE_PHASE(iSceneId))
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.9
					vAmandaScenePos= << -803.998, 175.910, 75.8 >>
					vAmandaSceneRot = << 0.000, 0.000, 21.120 >>
					iSceneIdAmandaIdle = CREATE_SYNCHRONIZED_SCENE(vAmandaScenePos, vAmandaSceneRot)
						
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneIdAmandaIdle, FALSE)
					
					IF NOT IS_ENTITY_DEAD(pedAmanda)
						TASK_SYNCHRONIZED_SCENE(pedAmanda, iSceneIdAmandaIdle, "misssolomon_5@bedroom", "sol_5_bedroom_idle_AT", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )	
						SET_SYNCHRONIZED_SCENE_LOOPED(iSceneIdAmandaIdle, TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAmanda, TRUE)
					ENDIF
					i_current_event = 104
				ENDIF
			ENDIF
		BREAK
		
		CASE 999
							
			GET_PED_LAST_DAMAGE_BONE(GET_PED_INDEX_FROM_ENTITY_INDEX(pedHostagetakerTracey), pedBoneTag)
			IF pedBoneTag = BONETAG_HEAD
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIC4_HEADSHOT_RESCUE)
			ENDIF
					
			REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
							
			KILL_ANY_CONVERSATION()
			REMOVE_BLIP(blipPedHostagetaker)
			IF NOT IS_PED_INJURED(pedHostagetakerTracey)
				APPLY_DAMAGE_TO_PED(pedHostagetakerTracey, 200, TRUE)
			ENDIF

			REQUEST_STAGE_ASSETS(STAGE_TRACEY_CAPTOR_SHOT)
			bDialogueDisplayed = FALSE
			i_current_event = 0
					
			STOP_AUDIO_SCENE("MI_4_SAVE_TRACY")
					
			mission_stage = STAGE_TRACEY_CAPTOR_SHOT
		BREAK
		
	ENDSWITCH
	
//	IF NOT IS_ENTITY_DEAD(pedTracey)
//		SET_PED_RESET_FLAG(pedTracey, PRF_BlockWeaponReactionsUnlessDead, TRUE)
//	ENDIF
	
	
	
	//Advance		
	//IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedHostagetakerTracey, PLAYER_PED_ID())
	IF mission_stage <> STAGE_TRACEY_CAPTOR_SHOT
		IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
			IF GET_ENTITY_HEALTH(pedHostagetakerTracey) < 1000.0	
			AND bFailForFiringButMissing = FALSE
				//Advance
				i_current_event = 999
			ENDIF
			IF NOT IS_PED_INJURED(pedTracey)
				IF i_current_event > 4
					IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedTracey), 2.5)
					OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedTracey) - <<3.0, 3.0, 3.0>>, GET_ENTITY_COORDS(pedTracey) + <<3.0, 3.0, 3.0>>)
						bFailForFiringButMissing = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bFailForFiringButMissing = FALSE
				i_current_event = 999
			ENDIF
		ENDIF
	ENDIF

	//Set cutscene variations...
	IF NOT IS_ENTITY_DEAD(pedAmanda)
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Amanda", pedAmanda)	
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedTracey)
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tracy", pedTracey)	
	ENDIF
			
	IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("GOON_ATTACK_TRACY", pedHostagetakerTracey)	
	ENDIF

ENDPROC

COVERPOINT_INDEX cpHallway

BOOL bComingOutOfcutscene = FALSE

PROC stageMCS1()

	//SEQUENCE_INDEX seqGetTracey

	SWITCH i_current_event
	
		CASE 0
			
			REMOVE_BLIP(blipTraceysRoom)
			REMOVE_BLIP(blipPedTracey)
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), beforeCutWeaponType)
			
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Amanda", pedAmanda)	
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedTracey)
				
				APPLY_PED_DAMAGE_PACK(pedTracey, "SCR_TracySplash", 1.0, 1.0)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tracy", pedTracey)	
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("GOON_ATTACK_TRACY", pedHostagetakerTracey)	
			ENDIF
			
			REQUEST_WEAPON_ASSET(beforeCutWeaponType)
			
			IF HAS_CUTSCENE_LOADED()
				IF NOT IS_ENTITY_DEAD(pedTracey)
					REGISTER_ENTITY_FOR_CUTSCENE(pedTracey, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_CLONE_DAMAGE_TO_CS_MODEL )
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedAmanda)
					REGISTER_ENTITY_FOR_CUTSCENE(pedAmanda, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
					REGISTER_ENTITY_FOR_CUTSCENE(pedHostagetakerTracey, "GOON_ATTACK_TRACY", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_CLONE_DAMAGE_TO_CS_MODEL)
				ENDIF
				cpHallway = ADD_COVER_POINT(<<-802.83807, 179.84833, 75.7407>>, 16.5058, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_300TO0, TRUE)	
			
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				STOP_AUDIO_SCENE("MI_4_SAVE_FAMILY_MAIN")
				FADE_IN_IF_NEEDED()
								
				i_current_event++
			ENDIF
		BREAK
		CASE 1
		CASE 2
		
		
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy"))
				IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy")))
					APPLY_PED_DAMAGE_PACK(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tracy")), "SCR_TracySplash", 1.0, 1.0)
				ENDIF
			ENDIF
			
			IF i_current_event =1
				IF PREPARE_MUSIC_EVENT("SOL5_HOSTAGE_DEAD")
					TRIGGER_MUSIC_EVENT("SOL5_HOSTAGE_DEAD")
					i_current_event =2
				ENDIF	
			ENDIF	
			
//			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				//RESET_GAME_CAMERA()
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-78.031601)
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-11.594591)
//			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael") //HAS_CUTSCENE_FINISHED()
										
				IF beforeCutWeaponType = WEAPONTYPE_UNARMED
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), FALSE)
				ELSE
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), beforeCutWeaponType, FALSE)
				ENDIF
				
				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),  << -803.0251, 179.8324, 75.7408 >>, -1, TRUE, 0.5, TRUE, TRUE, cpHallway)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				IF WAS_CUTSCENE_SKIPPED()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-90.0)				
				ENDIF
				bDialogueDisplayed = FALSE
				i_current_event = 0		
				
				//WAIT(3000)
				
				REPLAY_STOP_EVENT()
				
				bComingOutOfcutscene = TRUE
				
				mission_stage = STAGE_FIGHT_INCOMING_BAD_GUYS 			
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC


STRUCT SET_PIECE_COP
	
	VEHICLE_INDEX thisCar
	PED_INDEX thisDriver
	PED_INDEX thisPassenger
	
	PED_INDEX thisPassengerBackLeft
	PED_INDEX thisPassengerBackRight
	
	AI_BLIP_STRUCT blipDriver
	AI_BLIP_STRUCT blipPassenger
	AI_BLIP_STRUCT blipPassengerBackLeft
	AI_BLIP_STRUCT blipPassengerBackRight
	 
ENDSTRUCT

//SET_PIECE_COP merryWeatherBike
SET_PIECE_COP merryWeatherCar
SET_PIECE_COP merryWeatherCar2

PROC INIT_SET_PIECE_CAR(SET_PIECE_COP &thisSetpiece, MODEL_NAMES carModel, INT iRec, STRING stRec, FLOAT fSkipTime = 3000.0, FLOAT fPlayBackSpeed = 1.0)

	MODEL_NAMES thisPedModel = mnGoonModel
			
	thisSetpiece.thisCar = CREATE_VEHICLE(carModel, <<0.0, 0.0, 0.0>>)
	SET_VEHICLE_ENGINE_ON(thisSetpiece.thisCar, TRUE, TRUE)
	SET_ENTITY_HEALTH(thisSetpiece.thiscar, 500)
	
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(thisSetpiece.thisCar,	"MI_4_ENEMY_CARS_GROUP")

	
	IF carModel = MESA3
		thisSetpiece.thisDriver = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel )
		thisSetpiece.thisPassenger = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel, VS_FRONT_RIGHT )
		thisSetpiece.thisPassengerBackLeft = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel, VS_BACK_LEFT )
		thisSetpiece.thisPassengerBackRight = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel, VS_BACK_RIGHT)
	ELSE
		thisSetpiece.thisDriver = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel )
		thisSetpiece.thisPassenger = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel, VS_FRONT_RIGHT )
	ENDIF
	
	START_PLAYBACK_RECORDED_VEHICLE(thisSetpiece.thisCar, iRec, stRec)
	SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisSetpiece.thisCar, fSkipTime)
	SET_PLAYBACK_SPEED(thisSetpiece.thisCar, fPlayBackSpeed)	
	SET_ENTITY_PROOFS(thisSetpiece.thisCar, FALSE, FALSE, FALSE, TRUE, FALSE)
	
	REMOVE_VEHICLE_RECORDING(iRec, stRec)	//Test, hopefully this will be held in memory when needed and then auto cleaned up this way.
	
	SET_VEHICLE_LIGHTS(thisSetpiece.thisCar, SET_VEHICLE_LIGHTS_ON)
	SET_VEHICLE_FULLBEAM(thisSetpiece.thisCar, TRUE)
	
	//SET_VEHICLE_CAN_BE_TARGETTED(thisSetpiece.thisCar, TRUE)
	//SET_ENTITY_IS_TARGET_PRIORITY(thisSetpiece.thisCar, TRUE)
	
	SET_PED_AS_MERRYWEATHER(thisSetpiece.thisDriver, WEAPONTYPE_CARBINERIFLE)
	SET_PED_AS_MERRYWEATHER(thisSetpiece.thisPassenger, WEAPONTYPE_PISTOL)
	
	IF DOES_ENTITY_EXIST(thisSetpiece.thisPassengerBackLeft)
		SET_PED_AS_MERRYWEATHER(thisSetpiece.thisPassengerBackLeft, WEAPONTYPE_CARBINERIFLE)
	ENDIF
	IF DOES_ENTITY_EXIST(thisSetpiece.thisPassengerBackRight)
		SET_PED_AS_MERRYWEATHER(thisSetpiece.thisPassengerBackRight, WEAPONTYPE_PISTOL)
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 debugName = stRec
		debugName += iRec
		SET_VEHICLE_NAME_DEBUG(thisSetpiece.thisCar, debugName)
		
		debugName += " Drv"
		SET_PED_NAME_DEBUG(thisSetpiece.thisDriver, debugName)
		
		IF DOES_ENTITY_EXIST(thisSetpiece.thisPassenger)
			debugName = stRec
			debugName += iRec
			debugName += " Pass"
			SET_PED_NAME_DEBUG(thisSetpiece.thisPassenger, debugName)
		ENDIF
		IF DOES_ENTITY_EXIST(thisSetpiece.thisPassengerBackLeft)
			debugName = stRec
			debugName += iRec
			debugName += " PassBL"
			SET_PED_NAME_DEBUG(thisSetpiece.thisPassengerBackLeft, debugName)
		ENDIF
		
		IF DOES_ENTITY_EXIST(thisSetpiece.thisPassengerBackRight)
			debugName = stRec
			debugName += iRec
			debugName += " PassBR"
			SET_PED_NAME_DEBUG(thisSetpiece.thisPassengerBackRight, debugName)
		ENDIF
		
	#ENDIF

ENDPROC

//FUNC BOOL CHECK_SET_PIECE_FOR_BLIP_REMOVAL(SET_PIECE_COP &thisSetpiece)
//
//	IF IS_PED_INJURED(thisSetpiece.thisDriver)
//		IF DOES_BLIP_EXIST(thisSetpiece.blipDriver)
//			REMOVE_BLIP(thisSetpiece.blipDriver)
//		ENDIF
//	ENDIF
//	IF IS_PED_INJURED(thisSetpiece.thisPassenger)
//		IF DOES_BLIP_EXIST(thisSetpiece.blipPassenger)
//			REMOVE_BLIP(thisSetpiece.blipPassenger)
//		ENDIF
//	ENDIF
//	
//	IF IS_PED_INJURED(thisSetpiece.thisPassengerBackLeft)
//		IF DOES_BLIP_EXIST(thisSetpiece.blipPassengerBackLeft)
//			REMOVE_BLIP(thisSetpiece.blipPassengerBackLeft)
//		ENDIF
//	ENDIF
//	
//	IF IS_PED_INJURED(thisSetpiece.thisPassengerBackRight)
//		IF DOES_BLIP_EXIST(thisSetpiece.blipPassengerBackRight)
//			REMOVE_BLIP(thisSetpiece.blipPassengerBackRight)
//		ENDIF
//	ENDIF
//	
//	IF DOES_ENTITY_EXIST(thisSetpiece.thisCar)
//		IF NOT IS_ENTITY_DEAD(thisSetpiece.thisCar)	
//			
//			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisSetpiece.thisCar)
//				SET_ENTITY_IS_TARGET_PRIORITY(thisSetpiece.thisCar, FALSE)
//				SET_VEHICLE_CAN_BE_TARGETTED(thisSetpiece.thisCar, FALSE)
//			ENDIF
//			
//			IF (IS_PED_INJURED(thisSetpiece.thisDriver)	OR NOT IS_PED_IN_VEHICLE(thisSetpiece.thisDriver, thisSetpiece.thisCar))
//			AND (IS_PED_INJURED(thisSetpiece.thisPassenger)	OR NOT IS_PED_IN_VEHICLE(thisSetpiece.thisPassenger, thisSetpiece.thisCar))
//			AND (IS_PED_INJURED(thisSetpiece.thisPassengerBackLeft)	OR NOT IS_PED_IN_VEHICLE(thisSetpiece.thisPassengerBackLeft, thisSetpiece.thisCar))
//			AND (IS_PED_INJURED(thisSetpiece.thisPassengerBackRight)	OR NOT IS_PED_IN_VEHICLE(thisSetpiece.thisPassengerBackRight, thisSetpiece.thisCar))
//				SET_ENTITY_IS_TARGET_PRIORITY(thisSetpiece.thisCar, FALSE)
//				SET_VEHICLE_CAN_BE_TARGETTED(thisSetpiece.thisCar, FALSE)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NOT DOES_BLIP_EXIST(thisSetpiece.blipDriver)
//	AND NOT DOES_BLIP_EXIST(thisSetpiece.blipPassenger)
//	AND NOT DOES_BLIP_EXIST(thisSetpiece.blipPassengerBackLeft)
//	AND NOT DOES_BLIP_EXIST(thisSetpiece.blipPassengerBackRight)
//		RETURN TRUE
//	ELSE
//		RETURN FALSE
//	ENDIF
//	
//ENDFUNC

PROC CLEANUP_SET_PIECE_CAR(SET_PIECE_COP &thisSetpiece, BOOL bDelete = FALSE)

	IF NOT bDelete
		SET_VEHICLE_AS_NO_LONGER_NEEDED(thisSetpiece.thisCar)
		SET_PED_AS_NO_LONGER_NEEDED(thisSetpiece.thisDriver)
		SET_PED_AS_NO_LONGER_NEEDED(thisSetpiece.thisPassenger)
		SET_PED_AS_NO_LONGER_NEEDED(thisSetpiece.thisPassengerBackLeft)
		SET_PED_AS_NO_LONGER_NEEDED(thisSetpiece.thisPassengerBackright)
	ELSE
		DELETE_VEHICLE(thisSetpiece.thisCar)
		DELETE_PED(thisSetpiece.thisDriver)
		DELETE_PED(thisSetpiece.thisPassenger)
		DELETE_PED(thisSetpiece.thisPassengerBackLeft)
		DELETE_PED(thisSetpiece.thisPassengerBackright)
	ENDIF
		
ENDPROC

PROC SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_ENTITY(PED_INDEX thisPed, VECTOR vRunTo, ENTITY_INDEX entityIndex, FLOAT fMoveBlend = PEDMOVE_RUN)

	SEQUENCE_INDEX runSeq
	
	IF NOT IS_PED_INJURED(thisPed)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
	
		SET_PED_SPHERE_DEFENSIVE_AREA(thisPed, vRunTo, 2.5)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
		
		CLEAR_PED_TASKS(thisPed)
		
		OPEN_SEQUENCE_TASK(runSeq)
			//TASK_PAUSE(NULL, iPauseTime)			
			IF NOT IS_ENTITY_DEAD(entityIndex)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vRunTo, entityIndex, fMoveBlend, TRUE)// DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)			
			ENDIF
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
		CLOSE_SEQUENCE_TASK(runSeq)
		
		TASK_PERFORM_SEQUENCE(thisPed, runSeq)
		
		CLEAR_SEQUENCE_TASK(runSeq)

	ENDIF
	
ENDPROC

//BOOL bBikeCombatting
BOOL bCarCombatting

BOOL bOutsideGuySpawned = FALSE
BOOL bGuyRunningUPDriveWayTriggered = FALSE
INT iTimeOfDriveWayGuysTriggered

COVERPOINT_INDEX cpLounge
COVERPOINT_INDEX cpKitchen
COVERPOINT_INDEX cpKitchen2
COVERPOINT_INDEX cpGarden1
COVERPOINT_INDEX cpGarden2
COVERPOINT_INDEX cpDiningRoom

//INT iSkyLightSmashSound

BOOL bMichaelWentInBathroom

FUNC BOOL DID_MICHAEL_GO_IN_BATHROOM()
	
	IF bMichaelWentInBathroom
		RETURN bMichaelWentInBathroom
	ENDIF
	
	bMichaelWentInBathroom = IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-802.5521, 168.2117, 75.7407>>, <<1.5, 1.5, 2.5>>)
	//IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.420715,167.887558,75.006653>>, <<-804.393738,172.011993,79.739891>>, 2.000000)	//bathroom 
				
	RETURN bMichaelWentInBathroom
			
ENDFUNC

INT  iOnlyPlayOnce

BOOL bMichaelOutside
INT iAmandaConvoCounter
INT iTimeOfStartShooting

PROC HANDLE_SHOOTOUT_DIALOGUE()

	IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
	AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
	AND i_current_event > 5
	AND TIMERA() > 6000
	AND TIMERB() > 4400
	
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
			OR (GET_GAME_TIMER() - iTimeOfStartShooting) < 3000
			//OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0)
			//OR IS_ANY_PED_NEAR_POINT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 15.0)	
				//SETTIMERA(0)
			
				iTimeOfStartShooting = GET_GAME_TIMER()
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -802.8893, 179.8200, 76.7408 >>, <<4.5, 4.0, 2.0>>)
				AND iAmandaConvoCounter < 3
					IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_COWER", CONV_PRIORITY_VERY_HIGH)	
						iAmandaConvoCounter++
						SETTIMERA(0)
					ENDIF		
				ELSE
					IF GET_RANDOM_BOOL()
					
						PED_INDEX piThisPed
					
						GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 4.0, FALSE, TRUE, piThisPed)
						
						IF DOES_ENTITY_EXIST(piThisPed)
							IF GET_RANDOM_BOOL()
								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, piThisPed, "MERRYWEATHER7")						
								IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MERRYDI", CONV_PRIORITY_VERY_HIGH)	
									SETTIMERB(0)
								ENDIF		
							ELSE
								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, piThisPed, "MERRYWEATHER8")						
								IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MERRYD2", CONV_PRIORITY_VERY_HIGH)	
									SETTIMERB(0)
								ENDIF		
							ENDIF
							
						ENDIF		
					ELSE				
						IF bMichaelOutside
							IF GET_RANDOM_INT_IN_RANGE(0, 10) > 7//Make play less often.
								IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_OUTSIDE", CONV_PRIORITY_VERY_HIGH)	
									SETTIMERB(0)
								ENDIF		
							ELSE
								SETTIMERB(0)
							ENDIF		
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MYHOUSE", CONV_PRIORITY_VERY_HIGH)	
								SETTIMERB(0)
							ENDIF		
						ENDIF		
					ENDIF
				ENDIF		
			ELSE
			
				IF NOT bMichaelOutside
			
					IF iOnlyPlayOnce = 0
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_DOOR1", CONV_PRIORITY_VERY_HIGH)	
							iOnlyPlayOnce++
							SETTIMERA(0)
						ENDIF		
					ENDIF		
					
					IF iOnlyPlayOnce = 1
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_DOOR2", CONV_PRIORITY_VERY_HIGH)	
							iOnlyPlayOnce++
							SETTIMERA(0)
						ENDIF		 
					ENDIF	
					
					IF iOnlyPlayOnce = 2
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_DOOR3", CONV_PRIORITY_VERY_HIGH)	
							iOnlyPlayOnce++
							SETTIMERA(0)
						ENDIF		
					ENDIF
					
					IF iOnlyPlayOnce = 3
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_DOOR4", CONV_PRIORITY_VERY_HIGH)	
							iOnlyPlayOnce++
							SETTIMERA(0)
						ENDIF		
					ENDIF
				
				//IF NOT bMichaelOutside
				
//					IF iOnlyPlayOnce = 4
//						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_JCOWER", CONV_PRIORITY_VERY_HIGH)	
//							iOnlyPlayOnce++
//							SETTIMERA(0)
//						ENDIF		
//					ENDIF
					
					IF iOnlyPlayOnce = 4
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_TCOWER", CONV_PRIORITY_VERY_HIGH)	
							iOnlyPlayOnce++
							SETTIMERA(0)
						ENDIF		
					ENDIF
					
					IF iOnlyPlayOnce = 5
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_COWER", CONV_PRIORITY_VERY_HIGH)	
							iOnlyPlayOnce = 6
							SETTIMERA(0)
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
		
	ENDIF

ENDPROC

BOOL bPedsNowAdvancing
BOOL bGuysInCarAdvancing
INT iTimeOfGuysSpawning
INT iTimeCarStopped
//BOOL bIsLocked
//FLOAT fOpenRatio1
//FLOAT fOpenRatio2
//FLOAT fOpenRatioLastFrame1
//FLOAT fOpenRatioLastFrame2
INT iTimeOfSeeingGuy4

SEQUENCE_INDEX seqGetOutOfMichaelsRoom

BOOL bGuy1MovedIntoDiningRoom = FALSE
//BOOL bGuy2MovedIntoDiningRoom = FALSE
SEQUENCE_INDEX seqMoveIntoDiningRoom
BOOL bAlternateSides
BOOL bDiningRoomGuyJustShot = TRUE

BOOL bLoungeGuyPoppedOut = FALSE

	
PROC MAKE_PED_COME_LOOKING(PED_INDEX thisPed)
	
	IF NOT IS_PED_INJURED(thisPed)
		SET_PED_COMBAT_RANGE(thisPed, CR_NEAR)
		REMOVE_PED_DEFENSIVE_AREA(thisPed)
		SET_PED_COMBAT_MOVEMENT(thisPed, CM_WILLADVANCE)
		SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_CAN_CHARGE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_AGGRESSIVE, TRUE)
	ENDIF
	
ENDPROC

BOOL bPed4WillCharge = FALSE	

INT iTimeOfGuys4and5
BOOL bGuys4And5ComeLooking = FALSE
//INT iTimeOf2ndCarArriving

//BOOL bExtraDiningRoomBusterGuy = FALSE
INT iTimeOfGuysRunningInDiningRoom

//INT iTimeOfExtraDiningGuySpawned
//BOOL bExtraDiningGuyAdvancing

/// PURPOSE: Check for entity in front garden of Michael's house
///    
/// PARAMS:
///    thisEntity - 
/// RETURNS:
///    

INT iDeathCount
INT iAliveCount

PROC UPDATE_BLIPS_AND_DEATH_COUNT()
	INT i
	iDeathCount = 0		
	iAliveCount = 0
	FOR i = 0 TO NUMBER_OF_MERRYWEATHER -1
		IF IS_PED_INJURED(pedMerryWeather[i])
		OR IS_PED_HURT(pedMerryWeather[i])
			iDeathCount++
		ELSE
			IF DOES_ENTITY_EXIST(pedMerryWeather[i])
				iAliveCount++
			ENDIF
			IF IS_PED_IN_WRITHE(pedMerryWeather[i])
				APPLY_DAMAGE_TO_PED(pedMerryWeather[i], 100, TRUE)
			ENDIF
		ENDIF
		UPDATE_AI_PED_BLIP(pedMerryWeather[i], blipPedMerryWeather[i], -1, NULL, (i = 4))
	ENDFOR

ENDPROC

BOOL bCloseFrontDoors
OBJECT_INDEX oiGrenadeFake
BOOL bForceAppliedToFakeGrenade
SEQUENCE_INDEX seqMoveThroughFrontDoor
INT iTimeOfSpawningExtraGuyComingInDoor

INT iExtraGuy2SpawnTimer = 3000
INT iSpawnTimerGuy13 = 6500
INT iExtraGuyCounter

OBJECT_INDEX oiCameraDummy
BOOL bObjectivePrinted = FALSE
BOOL bStartSpawningGuys13 = FALSE
//BOOL bSetBlanks = FALSE

BOOl bGoOutsideTextPrinted

PROC stageFightIncomingBadGuys()

	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2195216
	
	INT i

	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_BH1_48_BACKDOOR_L, <<-796.57, 177.22, 73.04>>, TRUE, 1.0)
	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_BH1_48_BACKDOOR_R, <<-794.51, 178.01, 73.04>>, TRUE, -1.0)

//	//Close Traceys door.
//	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS( <<-802.702,176.176,76.890>>, 2.0, V_ILEV_MM_DOORDAUGHTER)
//		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORDAUGHTER, <<-802.702,176.176,76.890>>, TRUE, 0.0)
//	ENDIF
		
	//Back doors
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-793.394,180.507,73.040>>, 2.0, prop_bh1_48_backdoor_l)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_bh1_48_backdoor_l, <<-793.394,180.507,73.040>>, TRUE, 1.0)
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-794.185,182.568,73.040>>, 2.0, prop_bh1_48_backdoor_r)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_bh1_48_backdoor_r, <<-794.185,182.568,73.040>>, TRUE, -1.0)
	ENDIF
		
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.345337,175.497055,70.260384>>, <<-794.602661,184.461853,82.473396>>, 12.000000)
		bMichaelOutside = FALSE
	ELSE
		bMichaelOutside = TRUE
	ENDIF	
	
	//Stick Tracey's car nearby as cover.
	IF NOT DOES_ENTITY_EXIST(carTracey)
		REQUEST_NPC_VEH_MODEL(CHAR_TRACEY)
		IF HAS_NPC_VEH_MODEL_LOADED(CHAR_TRACEY)
			CREATE_NPC_VEHICLE(carTracey, CHAR_TRACEY, <<-815.0861, 163.3637, 70.3941>>, 195.6004)
		ENDIF
	ENDIF
	
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME() 
	
	//PRINTLN("H:", GET_GAMEPLAY_CAM_RELATIVE_HEADING())
	//PRINTLN("P:", GET_GAMEPLAY_CAM_RELATIVE_PITCH())
	//[Script] [00876599] H:-78.031601
	//[Script] [00876599] P:-11.594591

	
	IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
		g_replay.iReplayInt[2] = 1
	ENDIF
		
	IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
	AND bObjectivePrinted = FALSE
	AND i_current_event >= 3
		PRINT_NOW("SOL5_KILLBAD", DEFAULT_GOD_TEXT_TIME, 0)
		bObjectivePrinted = TRUE
	ENDIF
		
	SWITCH i_current_event
		
		CASE 0
		
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, TRUE, FALSE)
		
			SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")
		
			oiCameraDummy = CREATE_OBJECT(PROP_LD_TEST_01, GET_GAMEPLAY_CAM_COORD())
			SET_ENTITY_COLLISION(oiCameraDummy, FALSE)
			SET_ENTITY_VISIBLE(oiCameraDummy, FALSE)
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING) 
		
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER))
				ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER),V_ILEV_MM_DOORDAUGHTER, <<-802.702,176.176,76.890>>)
			ENDIF
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER), 0.0)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
		
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BW))
				ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BW),V_ILEV_MM_WINDOWWC, <<-802.73333, 167.5041, 77.5824>>)
			ENDIF
			
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_BW), 0.0)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BW), DOORSTATE_UNLOCKED, TRUE, TRUE)
		
			IF DOES_ENTITY_EXIST(carMichaelIsIn)
				IF NOT IS_ENTITY_DEAD(carMichaelIsIn)
					IF NOT IS_VEHICLE_IN_PLAYERS_GARAGE(carMichaelIsIn, CHAR_MICHAEL, FALSE)
						SET_ENTITY_COORDS(carMichaelIsIn, <<-829.7350, 164.0143, 68.2444>>)
						SET_ENTITY_HEADING(carMichaelIsIn, 345.6341 )
						SET_VEHICLE_ON_GROUND_PROPERLY(carMichaelIsIn)
					ENDIF
				ENDIF
			ENDIF
			//bush in back gardne
			ADD_COVER_BLOCKING_AREA(<<-800.4777, 165.0952, 70.1682>>, <<-797.3882, 170.1375, 72.7098>>, TRUE, TRUE, TRUE)
			
			//bush in gap in front garden.
			ADD_COVER_BLOCKING_AREA(<<-821.9898, 150.2125, 70.0244>>, <<-819.2112, 154.3554, 72.0284>>, TRUE, TRUE, TRUE)
		
			START_AUDIO_SCENE("MI_4_DEFEND_HOUSE_MAIN")
		
			SPECIAL_ABILITY_CHARGE_ABSOLUTE(PLAYER_ID(), 30, TRUE)
		
			iAmandaConvoCounter = 0
			iExtraGuyCounter = 0 
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, NULL, "JIMMY")
			SET_PLAYER_ANGRY(PLAYER_PED_ID(), TRUE)
					
			SET_CREATE_RANDOM_COPS(FALSE)
			SET_CREATE_RANDOM_COPS_ON_SCENARIOS(FALSE)
		
			iExtraGuy2SpawnTimer = 4000
		
			bPedsNowAdvancing = FALSE
			bGuysInCarAdvancing = FALSE
			bOutsideGuySpawned = FALSE
			bGuyRunningUPDriveWayTriggered = FALSE
			//bGuy2MovedIntoDiningRoom = FALSE
			
			bPedsNowAdvancing = FALSE
			bGuysInCarAdvancing = FALSE
			
			bLoungeGuyPoppedOut = FALSE
			bPed4WillCharge = FALSE	
			bGuys4And5ComeLooking = FALSE
			bStartSpawningGuys13 = FALSE
			
			bGoOutsideTextPrinted = FALSE
			
			iExtraBadGuysComingIntoDiningRoomCount = 0
			
			FOR i = 0 TO NUMBER_OF_MERRYWEATHER -1
				bMWCharging[i] = FALSE
			ENDFOR
			
			RESET_MISSION_STATS_ENTITY_WATCH()
			IF NOT IS_ENTITY_DEAD(pedHostagetakerTracey)
				APPLY_DAMAGE_TO_PED(pedHostagetakerTracey, 1000, TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedHostagetakerAmanda)
			//bBikeCombatting = FALSE
			bCarCombatting = FALSE
			REQUEST_STAGE_ASSETS(STAGE_FIGHT_INCOMING_BAD_GUYS)
					
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_FIGHT_INCOMING_BAD_GUYS", TRUE)

			IF cpHallway <> NULL
				REMOVE_COVER_POINT(cpHallway)
			ENDIF
			
			cpHallway = ADD_COVER_POINT(<<-802.83807, 179.84833, 75.7407>>, 16.5058, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_300TO0, TRUE)	
			
			cpLounge = ADD_COVER_POINT(<<-806.61963, 176.69244, 71.8347>>, 30.2791 , COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_300TO0, FALSE)
			cpDiningRoom = ADD_COVER_POINT(<<-798.37781, 177.98271, 71.83471>>, 115.9965, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_300TO0, FALSE)
			
			cpkitchen = ADD_COVER_POINT(<<-798.1015, 185.0172, 71.6056 >>, 109.7301, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_300TO0, FALSE)
			cpkitchen2 = ADD_COVER_POINT(<<-803.3184, 183.6969, 71.6055>>, 120.6245 , COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_300TO0, FALSE)
			cpGarden1 = ADD_COVER_POINT(<< -792.2584, 158.9243, 69.6745 >>, 358.7681, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_300TO0, FALSE)
			cpGarden2 = ADD_COVER_POINT(<< -782.0049, 158.5722, 66.4747 >>, 120.1811, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_300TO0, FALSE)
		
			IF Get_Fail_Weapon(0) = WEAPONTYPE_UNARMED
			OR Get_Fail_Weapon(0) = WEAPONTYPE_INVALID
					
				IF beforeCutWeaponType = WEAPONTYPE_UNARMED
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID()), FALSE)
				ELSE
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), beforeCutWeaponType, FALSE)
				ENDIF
			ELSE
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), Get_Fail_Weapon(0), FALSE)
			ENDIF
			
			bCloseFrontDoors = FALSE
			bForceAppliedToFakeGrenade = FALSE
			
			bPed4WillCharge = FALSE	
			
			//Create first ped
			pedMerryWeather[16] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-809.6782, 182.3056, 72.2454>>, 340.7195)
			SET_PED_LEG_IK_MODE(pedMerryWeather[16], LEG_IK_FULL)
			SET_PED_AS_MERRYWEATHER(pedMerryWeather[16], WEAPONTYPE_PISTOL)
			
			OPEN_SEQUENCE_TASK(seqGetOutOfMichaelsRoom)
				TASK_PAUSE(NULL, 250)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-806.3740, 183.1262, 74.0027>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(seqGetOutOfMichaelsRoom)
			TASK_PERFORM_SEQUENCE(pedMerryWeather[16], seqGetOutOfMichaelsRoom)
			CLEAR_SEQUENCE_TASK(seqGetOutOfMichaelsRoom)
								
			SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[16], <<-806.8599, 183.7816, 74.0032>>, 1.0, TRUE)
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedMerryWeather[16], 50.0)
			
			IF bComingOutOfcutscene = FALSE //Comeing out of a cutscene skip.
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-802.83807, 179.84833, 75.7407>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 105.5291)			
				//WAIT(0)
				
				//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), << -802.8893, 179.8200, 75.7408 >>, 5000, FALSE, 0, TRUE, TRUE, cpHallway)
				//TASK_SEEK_COVER_TO_COVER_POINT(PLAYER_PED_ID(), cpHallway, <<-815.6069, 178.5089, 71.1531>>, 2000)
				//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),  << -803.0251, 179.8324, 75.7408 >>, -1, TRUE, 0.5, TRUE, TRUE, cpHallway)
			
				TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),  <<-802.83807, 179.84833, 75.7407>>, -1, TRUE, 0.5, TRUE, TRUE, cpHallway)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				
				SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-90.0)
				
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
			//intMichaelsHouse = GET_INTERIOR_AT_COORDS(<< -806.9643, 182.9719, 77.7416 >>)			
			FADE_IN_IF_NEEDED()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedMerryWeather[16], "MERRYWEATHER7")
			CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MERRYIN", CONV_PRIORITY_VERY_HIGH)
			bObjectivePrinted = FALSE
			TRIGGER_MUSIC_EVENT("SOL5_BAD_GUYS")
			iTimeOfGuysSpawning = GET_GAME_TIMER()
			i_current_event = 3
		BREAK
			
		CASE 3			
				
			UPDATE_BLIPS_AND_DEATH_COUNT()
		
			//Lock Open Front doors
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS( <<-816.72, 179.10, 72.83>>, 2.0, V_ILEV_MM_DOORM_L)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORM_L, <<-816.72, 179.10, 72.83>>, TRUE, 1.0)
			ENDIF
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS( <<-816.11, 177.51, 72.83>>, 2.0, V_ILEV_MM_DOORM_R)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORM_R, <<-816.11, 177.51, 72.83>>, TRUE, -1.0)
			ENDIF
		
			//Have the guys get more aggressive if you wait for ages.
			IF GET_GAME_TIMER() - iTimeOfGuysSpawning > 5000
				IF NOT IS_PED_INJURED(pedMerryWeather[16])
					MAKE_PED_COME_LOOKING(pedMerryWeather[16])
				ENDIF
				iTimeOfGuysSpawning = GET_GAME_TIMER()
			ENDIF
			
			//2nd guy runs out of Michaels bedroom
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMerryWeather[16], PLAYER_PED_ID())
			OR IS_PED_HURT(pedMerryWeather[16])
			OR IS_PED_INJURED(pedMerryWeather[16])
			OR IS_ENTITY_AT_ENTITY(pedMerryWeather[16], PLAYER_PED_ID(), <<2.0, 2.0, 2.0>>)
			OR (IS_SPHERE_VISIBLE(<<-809.2635, 177.0600, 75.7407>>, 0.5) AND GET_GAME_TIMER() - iTimeOfGuysSpawning > 1000)

				
				
				i_current_event++				
//				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_AMNWARN", CONV_PRIORITY_VERY_HIGH)	
//					
//					
////					IF NOT IS_PED_INJURED(pedMerryWeather[1])
//					pedMerryWeather[1] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-810.5667, 179.2355, 75.7407>>, 208.5543)
//						SET_PED_AS_MERRYWEATHER(pedMerryWeather[1], WEAPONTYPE_PISTOL)
//						SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[1], << -809.2069, 175.7380, 75.7407 >>, 1.0)	
//						//SET_PED_COMBAT_ATTRIBUTES(pedMerryWeather[1], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
//						SET_COMBAT_FLOAT(pedMerryWeather[1], CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[1], TRUE)
//												
//						OPEN_SEQUENCE_TASK(seqGetOutOfMichaelsRoom)
//							TASK_PAUSE(NULL, 500)//100)
//							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << -809.2069, 175.7380, 75.7407 >>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//						CLOSE_SEQUENCE_TASK(seqGetOutOfMichaelsRoom)
//						TASK_PERFORM_SEQUENCE(pedMerryWeather[1], seqGetOutOfMichaelsRoom)
//						CLEAR_SEQUENCE_TASK(seqGetOutOfMichaelsRoom)
//						
//						SET_PED_CHANCE_OF_FIRING_BLANKS(pedMerryWeather[1], 0.8, 0.8) 
//						MAKE_PED_COME_LOOKING(pedMerryWeather[1])
//										
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[1], "GENERIC_WAR_CRY", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
//					
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, NULL, "MERRYWEATHER6")
//										
//						//TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedMerryWeather[1], 50.0)
//				//	ENDIF
//					i_current_event++
//				ENDIF
			ENDIF
		BREAK
		
		CASE 4
		
//			IF NOT IS_ENTITY_DEAD(pedMerryWeather[1])		
//			
//				SET_PED_CAPSULE(pedMerryWeather[1], 0.4)	//stop him clipping through door
//			
//				IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedMerryWeather[1]), 0.5)
//					IF bSetBlanks = FALSE
//						SET_PED_CHANCE_OF_FIRING_BLANKS(pedMerryWeather[1], 0.6, 0.8) 
//						bSetBlanks = TRUE
//					ENDIF
//					PRINTLN("on screen")
//				ELSE
//					IF bSetBlanks = TRUE
//						PRINTLN("off screen")
//						bSetBlanks = FALSE
//					ENDIF
//					SET_PED_CHANCE_OF_FIRING_BLANKS(pedMerryWeather[1], 1.0, 1.0) 
//				ENDIF
//			ENDIF
		
			UPDATE_BLIPS_AND_DEATH_COUNT()
		
		//Spawn the door guys and the rest of the house guys.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-808.820801,184.610107,71.090591>>, <<-807.065613,179.283417,86.242363>>, 7.000000)
			OR DID_MICHAEL_GO_IN_BATHROOM()
			OR bMichaelOutside
			OR GET_GAME_TIMER() - iTimeOfGuysSpawning > 45000
				
				IF IS_REPLAY_IN_PROGRESS()					
				AND g_replay.iReplayInt[1] = 0	//Flag
				AND g_replay.iReplayInt[2] = 0  //Special ability been used.
				AND (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_MICHAEL_4].missionFailsNoProgress = 1
					OR g_savedGlobals.sFlow.missionSavedData[SP_MISSION_MICHAEL_4].missionFailsNoProgress = 2)

					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP("MIC4_SA_KM")
					ELSE
						PRINT_HELP("MIC4_SA")
					ENDIF
					
					g_replay.iReplayInt[1] = 1
				ENDIF		
			
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_GRENADE", CONV_PRIORITY_VERY_HIGH)	
//					pedMerryWeatherDummyGrenade = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-823.1418, 176.1851, 70.1579>>, 286.0137)
//					GIVE_WEAPON_TO_PED(pedMerryWeatherDummyGrenade, WEAPONTYPE_GRENADE, 1, TRUE, TRUE)
//					TASK_THROW_PROJECTILE(pedMerryWeatherDummyGrenade,<<-808.2316, 181.1697, 73.6507>>)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeatherDummyGrenade, TRUE)
				
					oiGrenadeFake = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_GRENADE), <<-817.6454, 177.7025, 73.1987>>)
					FREEZE_ENTITY_POSITION(oiGrenadeFake, TRUE)
													
					iTimeOfGuysSpawning	= GET_GAME_TIMER()
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
		
			UPDATE_BLIPS_AND_DEATH_COUNT()
		
			//"Throw" the fake grenade...
			IF bForceAppliedToFakeGrenade = FALSE
			AND DOES_ENTITY_HAVE_PHYSICS(oiGrenadeFake)
			AND GET_GAME_TIMER() - iTimeOfGuysSpawning > 1250
			AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(oiGrenadeFake), 0.5)
			OR DID_MICHAEL_GO_IN_BATHROOM()
				FREEZE_ENTITY_POSITION(oiGrenadeFake, FALSE)
				APPLY_FORCE_TO_ENTITY(oiGrenadeFake, APPLY_TYPE_IMPULSE, NORMALISE_VECTOR(<<-817.6454, 177.7025, 73.1987>> - <<-813.3746, 179.4353, 71.3900>>) * -5.0, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
				bForceAppliedToFakeGrenade = TRUE
			ENDIF
		
			IF GET_GAME_TIMER() - iTimeOfGuysSpawning > 2500
			AND bForceAppliedToFakeGrenade = TRUE
				SET_ENTITY_VISIBLE(oiGrenadeFake, FALSE)
				DELETE_OBJECT(oiGrenadeFake)
				ADD_EXPLOSION(<<-813.3746, 179.4353, 71.3900>>, EXP_TAG_GRENADE, 1.0, TRUE, FALSE, 1.0)
				i_current_event++
			ENDIF
			
		BREAK
		
		CASE 6
				
			UPDATE_BLIPS_AND_DEATH_COUNT()
		
			//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-808.820801,184.610107,71.090591>>, <<-807.065613,179.283417,86.242363>>, 7.000000)
			//OR DID_MICHAEL_GO_IN_BATHROOM()
			//OR bMichaelOutside
			//OR GET_GAME_TIMER() - iTimeOfGuysSpawning > 25000
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, <<-813.3746, 179.4353, 71.3900>>, 5.0)											
				iTimeOfGuysSpawning = GET_GAME_TIMER()
				
				START_AUDIO_SCENE("MI_4_ENEMIES_BREACH_DOOR")

				pedMerryWeather[2] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-818.7431, 174.8620, 70.6330>>, 318.7630)
				pedMerryWeather[3] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-824.0150, 179.4059, 70.4689>>, 265.8274)
			
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[2])
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[3], WEAPONTYPE_PISTOL)
				
				OPEN_SEQUENCE_TASK(seqMoveThroughFrontDoor)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-813.7005, 181.0271, 71.1531>>, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 0.5, 5.0, TRUE, ENAV_DONT_AVOID_OBJECTS)
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seqMoveThroughFrontDoor)
				TASK_PERFORM_SEQUENCE(pedMerryWeather[2], seqMoveThroughFrontDoor)
				CLEAR_SEQUENCE_TASK(seqMoveThroughFrontDoor)
			
				OPEN_SEQUENCE_TASK(seqMoveThroughFrontDoor)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-812.7673, 177.9856, 71.1592>>, PLAYER_PED_ID(), PEDMOVE_RUN, FALSE, 0.5, 5.0, TRUE, ENAV_DONT_AVOID_OBJECTS)
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(seqMoveThroughFrontDoor)
				TASK_PERFORM_SEQUENCE(pedMerryWeather[3], seqMoveThroughFrontDoor)
				CLEAR_SEQUENCE_TASK(seqMoveThroughFrontDoor)
			
				pedMerryWeather[4] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), << -800.0967, 178.5484, 71.8348 >>, 112.4967)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(pedMerryWeather[4],  <<-806.7888, 176.5762, 71.8347>>, INFINITE_TASK_TIME, TRUE, 0, TRUE, TRUE, cpLounge)
				
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[2], "COVER_ME", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
									
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[4])
				
				SET_PED_NAME_DEBUG(pedMerryWeather[2], "Merry: 2")
				SET_PED_NAME_DEBUG(pedMerryWeather[3], "Merry: 3")
				SET_PED_NAME_DEBUG(pedMerryWeather[4], "Merry: 4")
																
				MAKE_PED_COME_LOOKING(pedMerryWeather[2])
				MAKE_PED_COME_LOOKING(pedMerryWeather[3])
								
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[4], << -806.7888, 176.5762, 71.8347>>, 2.25)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[4], TRUE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[2], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[3], TRUE)
				
				iTimeOfSpawningExtraGuyComingInDoor = GET_GAME_TIMER()
				
				//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedMerryWeather[4], "MERRYWEATHER1")
				i_current_event++
			ENDIF
		BREAK
		
		CASE 7
		CASE 8
			
			IF i_current_event = 7
				IF NOT IS_PED_INJURED(pedMerryWeather[2])
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedMerryWeather[2], "MERRYWEATHER7")
					IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MERRYIN", CONV_PRIORITY_VERY_HIGH)	
						i_current_event = 8
					ENDIF
				ELSE
					i_current_event = 8
				ENDIF
			ENDIF
		
			//Keep the ped in the lounge in cover until further notice.
			IF NOT IS_PED_INJURED(pedMerryWeather[4])
				
				IF NOT IS_ENTITY_ON_SCREEN(pedMerryWeather[4])
				AND NOT bMichaelOutside
					SET_PED_RESET_FLAG(pedMerryWeather[4],	PRF_ForcePeekFromCover, TRUE)
					IF bLoungeGuyPoppedOut = FALSE
						iTimeOfSeeingGuy4 = GET_GAME_TIMER()
					ENDIF
				ELSE
				
					IF GET_GAME_TIMER() - iTimeOfSeeingGuy4 > 500 //ms //500
					AND bLoungeGuyPoppedOut = FALSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[4], FALSE)
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedMerryWeather[4], "MERRYWEATHER7")
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MERRYDI", CONV_PRIORITY_VERY_HIGH)	
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[4], FALSE)
							SET_COMBAT_FLOAT(pedMerryWeather[4], CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.25)
							TASK_COMBAT_PED(pedMerryWeather[4], PLAYER_PED_ID())
							
							MAKE_PED_COME_LOOKING(pedMerryWeather[4])
							bLoungeGuyPoppedOut = TRUE
						ENDIF
					ENDIF
					
					//Two seconds after he's been spotted by the player
					IF GET_GAME_TIMER() - iTimeOfSeeingGuy4 > 2000
					AND bPed4WillCharge = FALSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[4], FALSE)
						MAKE_PED_COME_LOOKING(pedMerryWeather[4])
						bPed4WillCharge = TRUE
					ENDIF
					
				ENDIF
				
				//If the player just fucks around
				IF GET_GAME_TIMER() - iTimeOfGuysSpawning > 15000
				AND bPed4WillCharge = FALSE
				AND IS_PED_INJURED(pedMerryWeather[2])
				AND IS_PED_INJURED(pedMerryWeather[3])
					//SCRIPT_ASSERT("WTF")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[4], FALSE)
					MAKE_PED_COME_LOOKING(pedMerryWeather[4])
					bPed4WillCharge = TRUE
				ENDIF
			ELSE
				IF bMWCharging[4] = FALSE
					iTimeOfSpawningExtraGuyComingInDoor = GET_GAME_TIMER()
					bMWCharging[4] = TRUE
				ENDIF
			ENDIF
			
			PRINTLN("fucksake: ", GET_GAME_TIMER() - iTimeOfGuysSpawning)
			
			//Set the two guys that run in the door to open combat
			
			IF GET_GAME_TIMER() - iTimeOfGuysSpawning > 4000	//Allow tasks to start...
				IF bMWCharging[2] = FALSE
				AND NOT IS_PED_INJURED(pedMerryWeather[2])
					IF GET_SCRIPT_TASK_STATUS(pedMerryWeather[2], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[2], FALSE)
						MAKE_PED_COME_LOOKING(pedMerryWeather[2])
						bMWCharging[2] = TRUE
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[2], TRUE)
					ENDIF
				ENDIF
				IF bMWCharging[3] = FALSE
				AND NOT IS_PED_INJURED(pedMerryWeather[3])
					IF GET_SCRIPT_TASK_STATUS(pedMerryWeather[3], SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[3], FALSE)
						MAKE_PED_COME_LOOKING(pedMerryWeather[3])
						bMWCharging[3] = TRUE
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[3], TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			
			UPDATE_BLIPS_AND_DEATH_COUNT()
			
			//Advance a stage.
			IF GET_ENTITY_HEADING(PLAYER_PED_ID()) > 210.0
			AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 310.0
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.018066,181.418915,71.098137>>, <<-812.141724,179.262512,73.659195>>, 3.500000)
			OR (IS_SPHERE_VISIBLE(<<-802.0311, 182.6728, 72.9004>>, 0.5) AND IS_PED_HURT(pedMerryWeather[4]) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-816.546997,178.490189,71.227776>>, <<-810.670471,180.819443,74.903091>>, 5.500000))//Bottom of stairs
			OR bMichaelOutside
			OR (IS_PED_INJURED(pedMerryWeather[2]) AND iExtraGuyCounter > 1)	//killed two guys coming in the doors
					
				STOP_AUDIO_SCENE("MI_4_ENEMIES_BREACH_DOOR")
				TRIGGER_MUSIC_EVENT("SOL5_GROUND_FLOOR")
							
				pedMerryWeather[5] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-801.3400, 179.5918, 71.8347>>, 335.4891 )
				pedMerryWeather[6] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-797.1436, 181.8869, 71.8453>>, 351.1270 )
				
				pedMerryWeather[14] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-798.37781, 177.98271, 71.83471>>, 18.7039)
				//-798.37781, 177.98271, 71.83471	- BOOM			
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[5])
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[6], WEAPONTYPE_PISTOL)
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[14], WEAPONTYPE_CARBINERIFLE)
					
				SET_PED_NAME_DEBUG(pedMerryWeather[6], "Merry: 5")
				SET_PED_NAME_DEBUG(pedMerryWeather[5], "Merry: 6")
				SET_PED_NAME_DEBUG(pedMerryWeather[14], "Merry: 14")
				
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[5], <<-803.3466, 183.4802, 71.6055>>, 3.0)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[6], << -798.2574, 185.4006, 71.6056 >>, 3.0)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[14], <<-798.4697, 177.8276, 71.8347>>, 3.0)
			
				SET_PED_COMBAT_ATTRIBUTES(pedMerryWeather[5], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(pedMerryWeather[6], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
				
				SET_PED_CONFIG_FLAG(pedMerryWeather[5], PCF_ForceInitialPeekInCover, TRUE)
				SET_PED_CONFIG_FLAG(pedMerryWeather[6], PCF_ForceInitialPeekInCover, TRUE)
				
				//SET_PED_COMBAT_ATTRIBUTES(pedMerryWeather[14], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
				
				TASK_COMBAT_PED(pedMerryWeather[5], PLAYER_PED_ID())
				TASK_COMBAT_PED(pedMerryWeather[6], PLAYER_PED_ID())
				
				TASK_COMBAT_PED(pedMerryWeather[14], PLAYER_PED_ID())
								
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 6, pedMerryWeather[5], "MERRYWEATHER1")
		
				SETTIMERB(0)
				iTimeOfGuys4and5 = GET_GAME_TIMER()
				bGuys4And5ComeLooking = FALSE
				i_current_event++
			ELSE
				//If timer create one more guy
				
				PRINTLN("Extra guy:", GET_GAME_TIMER() - iTimeOfSpawningExtraGuyComingInDoor)
				
				IF GET_GAME_TIMER() - iTimeOfSpawningExtraGuyComingInDoor >	iExtraGuy2SpawnTimer// 7000 shoot //13000
				AND NOT bMichaelOutside
				AND IS_PED_INJURED(pedMerryWeather[2])
				AND IS_PED_INJURED(pedMerryWeather[4])	//Killed the stairs guy...
				AND NOT DOES_ENTITY_EXIST(pedMerryWeather[5]) //Stop spawning these guys when the other guys get triggered.
				AND IS_SPHERE_VISIBLE(<<-817.4581, 177.9828, 71.2274>>, 0.5)
				
				
				//AND iExtraGuyCounter < 2
				
					SWITCH iExtraGuyCounter
						CASE 0	//technically second guy....
							iExtraGuy2SpawnTimer = GET_RANDOM_INT_IN_RANGE(4000, 8000)	
						BREAK
						
						CASE 1
							iExtraGuy2SpawnTimer = GET_RANDOM_INT_IN_RANGE(8500, 12000)	
						BREAK
						
						CASE 2
							iExtraGuy2SpawnTimer = GET_RANDOM_INT_IN_RANGE(12000, 15000)	
						BREAK
						
						DEFAULT
							iExtraGuy2SpawnTimer = 15000
						BREAK
						
					ENDSWITCH
					
					iExtraGuyCounter++
				
					SET_PED_AS_NO_LONGER_NEEDED(pedMerryWeather[2])
					IF iExtraGuyCounter % 2 = 0	
						pedMerryWeather[2] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-818.6957, 172.7013, 70.5827>>, 351.4070)
					ELSE
						pedMerryWeather[2] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-822.1360, 179.2826, 70.5304>>, 264.9692)
					ENDIF
					SET_PED_AS_MERRYWEATHER(pedMerryWeather[2], WEAPONTYPE_PISTOL)
								
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMerryWeather[2], TRUE)
				
					OPEN_SEQUENCE_TASK(seqMoveThroughFrontDoor)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-816.6824, 178.2494, 71.2278>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
						TASK_COMBAT_PED_TIMED(NULL, PLAYER_PED_ID(), 2000)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-813.7005, 181.0271, 71.1531>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seqMoveThroughFrontDoor)
					TASK_PERFORM_SEQUENCE(pedMerryWeather[2], seqMoveThroughFrontDoor)
					CLEAR_SEQUENCE_TASK(seqMoveThroughFrontDoor)
							
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[2], "GENERIC_WAR_CRY", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
					
					SET_PED_NAME_DEBUG(pedMerryWeather[2], "EXTRA: 2")
					iTimeOfSpawningExtraGuyComingInDoor = GET_GAME_TIMER()
					MAKE_PED_COME_LOOKING(pedMerryWeather[2])
				ENDIF		
				
			ENDIF
			
		BREAK
		
		CASE 9
			//IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_MERRYIN", CONV_PRIORITY_VERY_HIGH)
			IF NOT IS_PED_INJURED(pedMerryWeather[5])
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[5], "COVER_ME", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
			ENDIF
			i_current_event++
		BREAK
		
		CASE 10
		
			UPDATE_BLIPS_AND_DEATH_COUNT()
						
			IF (GET_GAME_TIMER() - iTimeOfGuys4and5) > 10000
			AND bGuys4And5ComeLooking = FALSE
				IF NOT IS_PED_INJURED(pedMerryWeather[5])
					MAKE_PED_COME_LOOKING(pedMerryWeather[5])
				ENDIF
				IF NOT IS_PED_INJURED(pedMerryWeather[6])
					MAKE_PED_COME_LOOKING(pedMerryWeather[6])
				ENDIF
				bGuys4And5ComeLooking = TRUE
			ENDIF
								
			IF bOutsideGuySpawned = FALSE
				IF IS_ENTITY_IN_DINING_ROOM(PLAYER_PED_ID())
				OR DID_MICHAEL_GO_IN_BATHROOM()
				OR iExtraBadGuysComingIntoDiningRoomCount >= 2
				//OR bMichaelOutside					
					pedMerryWeather[7] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-787.6755, 167.0518, 70.3636>>, 351.2527)
					SET_PED_AS_MERRYWEATHER(pedMerryWeather[7], WEAPONTYPE_CARBINERIFLE)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[7], <<-791.1288, 173.7254, 71.5669>>, 1.5)	
					SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_ENTITY(pedMerryWeather[7], <<-791.1288, 173.7254, 71.5669>>, PLAYER_PED_ID())	
					SET_PED_NAME_DEBUG(pedMerryWeather[7], "Merry 7")
					SET_PED_CHANCE_OF_FIRING_BLANKS(pedMerryWeather[7], 0.5, 0.5) 
					iTimeOfMerryWeatherSpawn[7] = GET_GAME_TIMER()
					bMWCharging[7] = FALSE
					bOutsideGuySpawned = TRUE
				ENDIF
			ENDIF
			
			IF bOutsideGuySpawned = TRUE
			AND GET_GAME_TIMER() - iTimeOfMerryWeatherSpawn[7] > 30000
			AND bMWCharging[7] = FALSE
				IF NOT IS_PED_INJURED(pedMerryWeather[7])
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[7], "GENERIC_WAR_CRY", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
					MAKE_PED_COME_LOOKING(pedMerryWeather[7])
				ENDIF
				FORCE_LIGHTNING_FLASH()
				bMWCharging[7] = TRUE
			ENDIF
			
			IF bMichaelOutside
			AND bOutsideGuySpawned = TRUE
			AND NOT DOES_ENTITY_EXIST(pedMerryWeather[8])
				pedMerryWeather[8] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-799.2374, 165.6819, 70.5143>>, 299.6070)
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[8], WEAPONTYPE_CARBINERIFLE)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[8], <<-791.1288, 173.7254, 71.5669>>, 1.2)
				SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_ENTITY(pedMerryWeather[8], <<-791.1288, 173.7254, 71.5669>>, PLAYER_PED_ID())
				SET_PED_NAME_DEBUG(pedMerryWeather[8], "Merry 8")
				SET_PED_CHANCE_OF_FIRING_BLANKS(pedMerryWeather[8], 0.5, 0.5) 
				iTimeOfGuysSpawning = GET_GAME_TIMER()
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedMerryWeather[8])
			AND bMWCharging[8] = FALSE
				//Let the two outside guys near the dining room open combat
				IF (GET_GAME_TIMER() - iTimeOfGuysSpawning) > 10000
					IF NOT IS_PED_INJURED(pedMerryWeather[8])
						MAKE_PED_COME_LOOKING(pedMerryWeather[8])
						bMWCharging[8] = TRUE
					ENDIF
				ENDIF
			ENDIF
						
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.983887,163.990921,70.538406>>, <<-788.171753,167.602310,73.864197>>, 6.000000)
			OR (DOES_ENTITY_EXIST(pedMerryWeather[8]) AND IS_PED_INJURED(pedMerryWeather[8]))
				IF bGuyRunningUPDriveWayTriggered = FALSE
				AND bMichaelOutside	
				
					//If player has been a dick and run around the wrong way, dont spawn this guy
					IF NOT IS_ENTITY_IN_FRONT_GARDEN_AREA(PLAYER_PED_ID())
						pedMerryWeather[9] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-811.7975, 163.0438, 70.5636>>, 23.6469)
						SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_ENTITY(pedMerryWeather[9], <<-801.5480, 161.9153, 70.5567>>, PLAYER_PED_ID())	
						SET_PED_AS_MERRYWEATHER(pedMerryWeather[9], WEAPONTYPE_CARBINERIFLE)				
						SET_PED_NAME_DEBUG(pedMerryWeather[9], "Merry 9")
						PRINTLN("merry 9 created")
						iTimeOfDriveWayGuysTriggered = GET_GAME_TIMER()
					ENDIF
					
					bGuyRunningUPDriveWayTriggered = TRUE
				ENDIF
			ENDIF
			
			IF bGuyRunningUPDriveWayTriggered = TRUE
			AND bMWCharging[9] = FALSE
			AND GET_GAME_TIMER() - iTimeOfMerryWeatherSpawn[9] > 5000
				IF NOT IS_PED_INJURED(pedMerryWeather[9])
					MAKE_PED_COME_LOOKING(pedMerryWeather[9])
				ENDIF
				bMWCharging[9] = TRUE
			ENDIF
			
			//Check if Michael has gone outside, then make any existing indoor bad guys follow him out.
			IF bPedsNowAdvancing = FALSE
				IF TIMERA() > 1000
					IF bMichaelOutside
						//Set the peds after him
						IF bPedsNowAdvancing = FALSE
							FOR i = 0 TO NUMBER_OF_MERRYWEATHER -1
								IF NOT IS_PED_INJURED(pedMerryWeather[i])
								AND IS_ENTITY_IN_ANGLED_AREA(pedMerryWeather[i], <<-817.345337,175.497055,70.260384>>, <<-794.602661,184.461853,82.473396>>, 12.000000)
									MAKE_PED_COME_LOOKING(pedMerryWeather[i])
								ENDIF
							ENDFOR
														
							bPedsNowAdvancing = TRUE
							PRINTLN("NOT IN interior!")
						ENDIF
					ELSE
						PRINTLN("IN interior!")
						SETTIMERA(0)
					ENDIF
				ENDIF
			ENDIF	
			
			//IF iDeathCount = NUMBER_OF_MERRYWEATHER
			IF bMichaelOutside
			AND bGuyRunningUPDriveWayTriggered = TRUE
			AND (GET_GAME_TIMER() - iTimeOfDriveWayGuysTriggered) > 20000
				//back paved area behind carport thing.
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.129639,157.925400,69.902512>>, <<-787.036804,167.599136,77.365692>>, 10.250000)
			
				INFORM_MISSION_STATS_OF_INCREMENT(MIC4_KILLS, iDeathCount)
				i_current_event++
			ENDIF
		
		BREAK
		
		CASE 11
		
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_FIGHT_INCOMING_BAD_GUYS)
			AND NOT IS_ENTITY_IN_FRONT_GARDEN_AREA(PLAYER_PED_ID())
			AND NOT IS_ENTITY_ON_GARAGE_ROOF(PLAYER_PED_ID())
				pedMerryWeather[12] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-821.4448, 153.0200, 69.0971>>, 339.5754)
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[12], WEAPONTYPE_CARBINERIFLE)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[12],  <<-811.7975, 163.0438, 70.5636>>, 1.2)	
				SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_ENTITY(pedMerryWeather[12], <<-811.7975, 163.0438, 70.5636>>, PLAYER_PED_ID())	
				MAKE_PED_COME_LOOKING(pedMerryWeather[12])
				SET_PED_NAME_DEBUG(pedMerryWeather[12], "Merry 12")
				TRIGGER_MUSIC_EVENT("SOL5_MORE_MERRY")
				//PRINT_NOW("SOL5_MORE", DEFAULT_GOD_TEXT_TIME, 1)
				INIT_SET_PIECE_CAR(merryWeatherCar, MESA3, 3, "Sol5MW", 3000.0, 1.0)
				SET_VEHICLE_COLOURS(merryWeatherCar.thisCar, 0, 0)
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(merryWeatherCar.thisCar, <<0.0, 0.0, 0.2559>>)
				//DELETE_VEHICLE(badguyCarOutsideHouse2)
				
				START_AUDIO_SCENE("MI_4_ENEMY_CAR_ARRIVES")

				//INIT_SET_PIECE_CAR(merryWeatherBike, XYZ4, 4, "Sol5MW", 3000.0, 1.0, TRUE)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 12
		
			//Front doors
			IF bCloseFrontDoors = FALSE
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS( <<-816.72, 179.10, 72.83>>, 2.0, V_ILEV_MM_DOORM_L)
				AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS( <<-816.11, 177.51, 72.83>>, 2.0, V_ILEV_MM_DOORM_R)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORM_L, <<-816.72, 179.10, 72.83>>, FALSE, 0.0)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORM_R, <<-816.11, 177.51, 72.83>>, FALSE, 0.0)
					bCloseFrontDoors = TRUE
				ENDIF
			ENDIF
		
			UPDATE_BLIPS_AND_DEATH_COUNT()
				
			//Open gate...
//			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-844.05, 155.96, 66.03>>, 2.0, PROP_LRGGATE_02_LD)
//				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_02_LD,<<-844.05, 155.96, 66.03>>, TRUE, 1.0)
//			ENDIF
			
			IF IS_PED_INJURED(merryWeatherCar.thisPassenger)
			AND (IS_PED_INJURED(merryWeatherCar.thisPassengerBackRight) OR IS_PED_INJURED(merryWeatherCar.thisDriver))
			AND NOT DOES_ENTITY_EXIST(merryWeatherCar2.thisCar)
			OR (NOT DOES_ENTITY_EXIST(merryWeatherCar2.thisCar) 
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-839.168457,149.884491,65.924622>>, <<-804.994263,163.102219,73.789093>>, 11.750000))
				//AND IS_ENTITY_IN_FRONT_GARDEN_AREA(PLAYER_PED_ID()))
				INIT_SET_PIECE_CAR(merryWeatherCar2, MESA3, 4, "Sol5MW", 1000.0, 1.0)
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(merryWeatherCar2.thisCar, <<0.0, 0.0, 0.2559>>)
				DELETE_PED(merryWeatherCar2.thisPassengerBackLeft)
				DELETE_PED(merryWeatherCar2.thisPassengerBackRight)
								
				SET_PED_ANGLED_DEFENSIVE_AREA(merryWeatherCar2.thisDriver, <<-822.301575,182.692917,69.383873>>, <<-827.917908,176.600449,72.692253>>, 3.500000)
				SET_PED_ANGLED_DEFENSIVE_AREA(merryWeatherCar2.thisPassenger, <<-822.301575,182.692917,69.383873>>, <<-827.917908,176.600449,72.692253>>, 3.500000)
				
				SET_VEHICLE_COLOURS(merryWeatherCar2.thisCar, 0, 0)
				START_AUDIO_SCENE("MI_4_ENEMY_CAR_ARRIVES")
				
				MAKE_PED_COME_LOOKING(merryWeatherCar2.thisDriver)
				MAKE_PED_COME_LOOKING(merryWeatherCar2.thisPassenger)
				
			ENDIF
					
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(merryWeatherCar.thisCar, "SOL5MW") > 90.0
			AND NOT IS_PED_INJURED(merryWeatherCar.thisPassengerBackRight)
				IF (GET_SCRIPT_TASK_STATUS(merryWeatherCar.thisPassengerBackRight, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK)
					//SET_PED_ANGLED_DEFENSIVE_AREA(merryWeatherCar.thisPassengerBackRight, <<-827.860046,160.394165,67.701981>>, <<-808.137329,157.933777,71.781845>>, 10.250000)
					FORCE_LIGHTNING_FLASH()
					MAKE_PED_COME_LOOKING(merryWeatherCar.thisPassengerBackRight)
					TASK_COMBAT_PED(merryWeatherCar.thisPassengerBackRight, PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(merryWeatherCar.thisCar, "SOL5MW") > 93.0
			AND NOT IS_PED_INJURED(merryWeatherCar.thisPassengerBackleft)
				IF (GET_SCRIPT_TASK_STATUS(merryWeatherCar.thisPassengerBackLeft, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK)
					//SET_PED_ANGLED_DEFENSIVE_AREA(merryWeatherCar.thisPassengerBackLeft, <<-827.860046,160.394165,67.701981>>, <<-808.137329,157.933777,71.781845>>, 10.250000)
					MAKE_PED_COME_LOOKING(merryWeatherCar.thisPassengerBackLeft)
					TASK_COMBAT_PED(merryWeatherCar.thisPassengerBackLeft, PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(merryWeatherCar.thisCar, "SOL5MW") > 95.5
			AND NOT IS_PED_INJURED(merryWeatherCar.thisPassenger)
				IF (GET_SCRIPT_TASK_STATUS(merryWeatherCar.thisPassenger, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK)
					//SET_PED_ANGLED_DEFENSIVE_AREA(merryWeatherCar.thisPassenger, <<-827.860046,160.394165,67.701981>>, <<-808.137329,157.933777,71.781845>>, 10.250000)
					MAKE_PED_COME_LOOKING(merryWeatherCar.thisPassenger)
					TASK_COMBAT_PED(merryWeatherCar.thisPassenger, PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(merryWeatherCar.thisCar, "SOL5MW") >= 96.9
			AND bCarCombatting = FALSE
				IF NOT IS_PED_INJURED(merryWeatherCar.thisDriver)
					//SET_PED_ANGLED_DEFENSIVE_AREA(merryWeatherCar.thisDriver, <<-827.860046,160.394165,67.701981>>, <<-808.137329,157.933777,71.781845>>, 10.250000)
					MAKE_PED_COME_LOOKING(merryWeatherCar.thisDriver)
					TASK_COMBAT_PED(merryWeatherCar.thisDriver, PLAYER_PED_ID())				
					IF IS_AUDIO_SCENE_ACTIVE("MI_4_ENEMY_CAR_ARRIVES")
						STOP_AUDIO_SCENE("MI_4_ENEMY_CAR_ARRIVES")
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(merryWeatherCar.thisCar)
						iTimeCarStopped = GET_GAME_TIMER()
					ENDIF
					bCarCombatting = TRUE
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("MI_4_ENEMY_CAR_ARRIVES")
						STOP_AUDIO_SCENE("MI_4_ENEMY_CAR_ARRIVES")
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(merryWeatherCar.thisCar)
						iTimeCarStopped = GET_GAME_TIMER()
						bCarCombatting = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Stop audio scene.
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(merryWeatherCar2.thisCar, "SOL5MW") >= 96.9
				IF IS_AUDIO_SCENE_ACTIVE("MI_4_ENEMY_CAR_ARRIVES")
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(merryWeatherCar2.thisCar)
					STOP_AUDIO_SCENE("MI_4_ENEMY_CAR_ARRIVES")
				ENDIF
			ENDIF
			
			UPDATE_BLIPS_AND_DEATH_COUNT()
		
			UPDATE_AI_PED_BLIP(merryWeatherCar.thisDriver, merryWeatherCar.blipDriver)
			UPDATE_AI_PED_BLIP(merryWeatherCar.thisPassenger, merryWeatherCar.blipPassenger)
			UPDATE_AI_PED_BLIP(merryWeatherCar.thisPassengerBackLeft, merryWeatherCar.blipPassengerBackLeft)
			UPDATE_AI_PED_BLIP(merryWeatherCar.thisPassengerBackRight, merryWeatherCar.blipPassengerBackRight)
			
			UPDATE_AI_PED_BLIP(merryWeatherCar2.thisDriver, merryWeatherCar2.blipDriver)
			UPDATE_AI_PED_BLIP(merryWeatherCar2.thisPassenger, merryWeatherCar2.blipPassenger)
			
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << -812.6817, 160.5256, 70.4072 >>) > 20.0
			AND (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),merryWeatherCar.thisCar) > 20.0)
				SET_PLAYBACK_SPEED(merryWeatherCar.thisCar, 0.21)
			ELSE
				SET_PLAYBACK_SPEED(merryWeatherCar.thisCar, 1.2 - (1.0 / GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << -812.6817, 160.5256, 70.4072 >>)))
			ENDIF
			
			IF (GET_GAME_TIMER() - iTimeCarStopped) > 10000
			AND bGuysInCarAdvancing = FALSE
			AND bCarCombatting = TRUE
				MAKE_PED_COME_LOOKING(merryWeatherCar.thisDriver)
				MAKE_PED_COME_LOOKING(merryWeatherCar.thisPassenger)
				MAKE_PED_COME_LOOKING(merryWeatherCar.thisPassengerBackLeft)
				MAKE_PED_COME_LOOKING(merryWeatherCar.thisPassengerBackRight)
				bGuysInCarAdvancing = TRUE
			ENDIF
			
			//Advance to next stage.
			IF IS_PED_INJURED(merryWeatherCar.thisDriver)
			AND IS_PED_INJURED(merryWeatherCar.thisPassenger)
			AND IS_PED_INJURED(merryWeatherCar.thisPassengerBackLeft)
			AND IS_PED_INJURED(merryWeatherCar.thisPassengerBackRight)
			AND IS_PED_INJURED(merryWeatherCar2.thisDriver)
			AND IS_PED_INJURED(merryWeatherCar2.thisPassenger)
			AND iDeathCount = NUMBER_OF_MERRYWEATHER
				SETTIMERA(0)
				i_current_event++
			ENDIF
			
		BREAK
		
		CASE 13
			IF TIMERA() > 1500
				DELETE_OBJECT(oiCameraDummy)
			
				i_current_event = 0
				STOP_AUDIO_SCENE("MI_4_DEFEND_HOUSE_MAIN")
				mission_stage = STAGE_REUNITE_WITH_FAMILY
			ENDIF
		BREAK
			
	ENDSWITCH
	
	//Spawn some extra guys if you stay inside for too long...
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-794.016357,181.931747,71.412010>>, <<-807.962341,173.754105,75.575302>>, 6.250000)
	
		//Move the kitchen guys into the dining area if you go into hte living room.
		IF NOT IS_PED_INJURED(pedMerryWeather[6])
		AND bGuy1MovedIntoDiningRoom = FALSE	
			OPEN_SEQUENCE_TASK(seqMoveIntoDiningRoom)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-799.7594, 181.2103, 71.8347>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(seqMoveIntoDiningRoom)
			TASK_PERFORM_SEQUENCE(pedMerryWeather[6], seqMoveIntoDiningRoom)
			CLEAR_SEQUENCE_TASK(seqMoveIntoDiningRoom)
								
			//SET_PED_SPHERE_DEFENSIVE_AREA(pedMerryWeather[5], <<-799.7594, 181.2103, 71.8347>>, 4.0, TRUE)	
			SETTIMERB(0)	
			bGuy1MovedIntoDiningRoom = TRUE
		ENDIF
		
	ENDIF
	
	//If player is in lounge
	//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.236755,179.579681,71.834709>>, <<-805.600037,176.659973,75.334709>>, 4.000000)
	//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.236755,179.579681,71.834709>>, <<-808.380981,175.258133,75.334709>>, 4.000000)
	
	IF NOT bMichaelOutside
	//AND NOT IS_ENTITY_IN_DINING_ROOM(PLAYER_PED_ID())
	AND bOutsideGuySpawned = FALSE
	AND DOES_ENTITY_EXIST(pedMerryWeather[5])
	AND IS_PED_INJURED(pedMerryWeather[13])
	AND bDiningRoomGuyJustShot = TRUE
	AND iAliveCount < 2
	
		PRINTLN("GET_GAME_TIMER() - iTimeOfGuysRunningInDiningRoom:", GET_GAME_TIMER() - iTimeOfGuysRunningInDiningRoom)
	
	
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.236755,179.579681,71.834709>>, <<-805.600037,176.659973,75.334709>>, 4.000000)
		OR IS_ENTITY_IN_DINING_ROOM(PLAYER_PED_ID())
			bStartSpawningGuys13 = TRUE
		ENDIF
		
		//Spawn guys if you take too long elsewhere in the house.
		IF ((IS_ENTITY_IN_DINING_ROOM(PLAYER_PED_ID()) OR IS_SPHERE_VISIBLE(<<-794.8332, 181.1352, 73.6243>>, 0.6))
			AND GET_GAME_TIMER() - iTimeOfGuysRunningInDiningRoom > iSpawnTimerGuy13
			AND IS_PED_INJURED(pedMerryWeather[5])
			AND IS_PED_INJURED(pedMerryWeather[6]))
			
		//OR if you hit a locate and are in the livign room area... spawn every so often.
		OR (bStartSpawningGuys13
			AND GET_GAME_TIMER() - iTimeOfGuysRunningInDiningRoom > iSpawnTimerGuy13
			AND IS_PED_INJURED(pedMerryWeather[7]))	//Guy who runs in from the garden.
		
			iSpawnTimerGuy13 = GET_RANDOM_INT_IN_RANGE(6000, 7000)
		
			IF NOT IS_PED_INJURED(pedMerryWeather[14])
				MAKE_PED_COME_LOOKING(pedMerryWeather[14])
			ENDIF
		
			SET_PED_AS_NO_LONGER_NEEDED(pedMerryWeather[13])	//Cleanup old ped
							
			//Update the game camera dummy object
			IF DOES_ENTITY_EXIST(oiCameraDummy)
				SET_ENTITY_COORDS(oiCameraDummy, GET_GAMEPLAY_CAM_COORD())
			ENDIF
					
			IF NOT IS_ENTITY_IN_DINING_ROOM(PLAYER_PED_ID())
			AND NOT bMichaelOutside				
			AND NOT IS_ENTITY_OUTSIDE(oiCameraDummy)
			//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-797.828613,180.159698,71.740204>>, <<-808.773438,175.866150,74.751839>>, 19.500000)
			//AND NOT bMichaelOutside				
			
				pedMerryWeather[13] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-792.9798, 183.6559, 71.8351>>, 167.9431)
				SET_PED_AS_MERRYWEATHER(pedMerryWeather[13], WEAPONTYPE_CARBINERIFLE)
				MAKE_PED_COME_LOOKING(pedMerryWeather[13])
				SET_PED_NAME_DEBUG(pedMerryWeather[13], "Merry13:EX-F")
				IF bAlternateSides = FALSE
					OPEN_SEQUENCE_TASK(seqMoveIntoDiningRoom)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-793.2159, 181.7920, 71.8351>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)//, ENAV_DONT_AVOID_OBJECTS)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-799.7575, 181.1908, 71.8347>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)//, ENAV_DONT_AVOID_OBJECTS)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					CLOSE_SEQUENCE_TASK(seqMoveIntoDiningRoom)
					bAlternateSides = TRUE
				ELSE
					OPEN_SEQUENCE_TASK(seqMoveIntoDiningRoom)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-793.2159, 181.7920, 71.8351>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)//, ENAV_DONT_AVOID_OBJECTS)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-798.4775, 177.8392, 71.8347>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)//, ENAV_DONT_AVOID_OBJECTS)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					CLOSE_SEQUENCE_TASK(seqMoveIntoDiningRoom)
					bAlternateSides = FALSE
				ENDIF
			
			ELSE
			
				IF bAlternateSides = FALSE
					//Spawn a guy outside running in...
					pedMerryWeather[13] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(),  <<-793.5820, 187.2418, 71.8353>>, 203.3005 )
					SET_PED_AS_MERRYWEATHER(pedMerryWeather[13], WEAPONTYPE_CARBINERIFLE)
					//MAKE_PED_COME_LOOKING(pedMerryWeather[13])
					
					SET_PED_COMBAT_RANGE(pedMerryWeather[13], CR_NEAR)
					REMOVE_PED_DEFENSIVE_AREA(pedMerryWeather[13])
					SET_PED_COMBAT_MOVEMENT(pedMerryWeather[13], CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(pedMerryWeather[13], CA_AGGRESSIVE, TRUE)
					
					SET_PED_NAME_DEBUG(pedMerryWeather[13], "Merry13:EX-BA1")
					OPEN_SEQUENCE_TASK(seqMoveIntoDiningRoom)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-793.2159, 181.7920, 71.8351>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)//, ENAV_DONT_AVOID_OBJECTS)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					CLOSE_SEQUENCE_TASK(seqMoveIntoDiningRoom)
					
					//Make the next guy coming from the front door come later.
					iSpawnTimerGuy13 = GET_RANDOM_INT_IN_RANGE(7000, 7500)
					
					bAlternateSides = TRUE
				ELSE
					//Spawn a guy up front / livign room
					pedMerryWeather[13] = CREATE_PED(PEDTYPE_MISSION, getRandomGoonModel(), <<-798.6892, 174.0237, 71.8349>>, 321.3347)// <<-807.4605, 177.0671, 71.8347>>, 200.9755)
					SET_PED_AS_MERRYWEATHER(pedMerryWeather[13], WEAPONTYPE_CARBINERIFLE)
					//MAKE_PED_COME_LOOKING(pedMerryWeather[13])	//Remiove charge flag for Les TODO
					SET_PED_COMBAT_RANGE(pedMerryWeather[13], CR_NEAR)
					REMOVE_PED_DEFENSIVE_AREA(pedMerryWeather[13])
					SET_PED_COMBAT_MOVEMENT(pedMerryWeather[13], CM_WILLADVANCE)					
					SET_PED_COMBAT_ATTRIBUTES(pedMerryWeather[13], CA_AGGRESSIVE, TRUE)
					
					SET_PED_NAME_DEBUG(pedMerryWeather[13], "Merry13:EX-BA2")	
					OPEN_SEQUENCE_TASK(seqMoveIntoDiningRoom)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-801.2900, 178.5022, 71.8347>>, PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 4.0, TRUE)//, ENAV_DONT_AVOID_OBJECTS)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					CLOSE_SEQUENCE_TASK(seqMoveIntoDiningRoom)
					iSpawnTimerGuy13 = GET_RANDOM_INT_IN_RANGE(5000, 7000)
										
					bAlternateSides = FALSE
				ENDIF
				
			ENDIF
			
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[13], "COVER_ME", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
		
			TASK_PERFORM_SEQUENCE(pedMerryWeather[13], seqMoveIntoDiningRoom)
			CLEAR_SEQUENCE_TASK(seqMoveIntoDiningRoom)
			
			SET_PED_CHANCE_OF_FIRING_BLANKS(pedMerryWeather[13], 0.6, 0.8)
					
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMerryWeather[13], "GENERIC_WAR_CRY", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
			
			iExtraBadGuysComingIntoDiningRoomCount++
			
			IF NOT bGoOutsideTextPrinted
			AND iExtraBadGuysComingIntoDiningRoomCount = 2
				PRINT_NOW("SOL5_KILLBAD2", DEFAULT_GOD_TEXT_TIME, 1)
				bGoOutsideTextPrinted = TRUE
			ENDIF
					
			bDiningRoomGuyJustShot = FALSE
		ENDIF
	ENDIF
		
	//IF DOES_ENTITY_EXIST(pedMerryWeather[13])
		IF IS_PED_INJURED(pedMerryWeather[13])
			IF bDiningRoomGuyJustShot = FALSE
				iTimeOfGuysRunningInDiningRoom = GET_GAME_TIMER()
				bDiningRoomGuyJustShot = TRUE	
			ENDIF
		ENDIF
	//
	
	//HANDLE_RAPPEL(pedsRappelData[0], TRUE, FALSE)
		
	HANDLE_SHOOTOUT_DIALOGUE()

ENDPROC

BOOL bLastStageASsetsCreated = FALSE

PROC stageReuniteWithFamily()

	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2195216
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-802.70, 176.18, 76.89>>, 1.0, V_ILEV_MM_DOORW)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_MM_DOORW, <<-802.70, 176.18, 76.89>>, TRUE, 0.2)
	ENDIF
		
	bMichaelOutside = IS_ENTITY_OUTSIDE(PLAYER_PED_ID())
			
	SWITCH i_current_event
		
		CASE 0
			IF PREPARE_MUSIC_EVENT("SOL5_BACK_TO_TRACEY")
				TRIGGER_MUSIC_EVENT("SOL5_BACK_TO_TRACEY")
				REMOVE_CUTSCENE()
				iOnlyPlayOnce = 4
				SET_CREATE_RANDOM_COPS(FALSE)
				SET_CREATE_RANDOM_COPS_ON_SCENARIOS(FALSE)
				
				FADE_IN_IF_NEEDED()
				START_AUDIO_SCENE("MI_4_GET_BACK_UPSTAIRS")
				bLastStageASsetsCreated = FALSE
				//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_REUNITE_WITH_FAMILY", TRUE)

				REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)

				i_current_event++
			ENDIF
		BREAK
		
		CASE 1
			PRINT_NOW("SOL5_RENDEZV", DEFAULT_GOD_TEXT_TIME, 0)
			blipTraceysRoom = CREATE_BLIP_FOR_COORD( << -802.7369, 174.3312, 75.7408 >>)
			SET_BLIP_NAME_FROM_TEXT_FILE(blipTraceysRoom, "MIC4_BLIPFAM")
			SET_BLIP_COLOUR(blipTraceysRoom, BLIP_COLOUR_BLUE)
			REQUEST_STAGE_ASSETS(STAGE_REUNITE_WITH_FAMILY)
			REQUEST_STAGE_ASSETS(STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE)
			i_current_event++
		BREAK
		
		CASE 2
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,0), 0, 1) //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,2), 1, 0) //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,3), 7, 0) //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,5), 0, 0) //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Amanda", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,0), 0, 1) //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,2), 2, 0) //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,3), 2, 0) //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,5), 0, 0) //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,6), 2, 0) //(feet)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Tracy", INT_TO_ENUM(PED_COMPONENT,8), 1, 0) //(accs)
							
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,3), 3, 0) //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,5), 1, 0) //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,6), 2, 0) //(feet)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,8), 2, 0) //(accs)
				//SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", INT_TO_ENUM(PED_COMPONENT,10), 0, 0) //(decl)
							
				i_current_event++
			ENDIF
		BREAK
		
		CASE 3
		CASE 4
		CASE 5
		
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_REUNITE_WITH_FAMILY)
			AND NOT DOES_ENTITY_EXIST(pedWritheGuy)
			AND bLastStageASsetsCreated = FALSE
				CREATE_STAGE_ASSETS(STAGE_REUNITE_WITH_FAMILY)
				bLastStageASsetsCreated = TRUE
			ENDIF
				
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND i_current_event = 3
			
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_RENDEZ", CONV_PRIORITY_VERY_HIGH)	
					i_current_event = 4
				ENDIF
			ENDIF
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND i_current_event = 4
			AND bMichaelOutside = FALSE
				IF CREATE_CONVERSATION(myScriptedSpeech, "SOL5AUD", "SOL5_RENDEZ2", CONV_PRIORITY_VERY_HIGH)	
					i_current_event = 5
				ENDIF				
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-805.6776, 182.4792, 75.7146>>, <<3.0, 3.0, 3.0>>)		
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			ENDIF	
			//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.770081,178.713364,75.493347>>, <<-803.683960,180.453369,79.740799>>, 1.250000)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.990295,179.395172,74.915062>>, <<-807.950012,176.674271,77.990738>>, 8.500000)
			AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				REMOVE_BLIP(blipTraceysRoom)
				SET_PED_AS_NO_LONGER_NEEDED(pedWritheGuy)
				i_current_event = 0
				STOP_AUDIO_SCENE("MI_4_GET_BACK_UPSTAIRS")
				mission_stage = STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
			ENDIF	
		BREAK
		
	ENDSWITCH
	
	//HANDLE_SHOOTOUT_DIALOGUE()

ENDPROC

BOOL bDontAlwaysLoadTheScene = FALSE
BOOL bDamagePackApplied = FALSE

PROC stageJimmySavesTheDayCutscene()

	SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)

	SWITCH i_current_event
		
		CASE 0				
			//REQUEST_STAGE_ASSETS(STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE)
			i_current_event++
		BREAK
		
		CASE 1								
			DELETE_VEHICLE(carTracey)
			SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_TRACEY)
			DELETE_PED(pedTracey)					
			IF NOT IS_ENTITY_DEAD(pedTracey)	
				REGISTER_ENTITY_FOR_CUTSCENE(pedTracey, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
				
			IF NOT IS_ENTITY_DEAD(pedAmanda)
				REGISTER_ENTITY_FOR_CUTSCENE(pedAmanda, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
//			
//			IF NOT IS_ENTITY_DEAD(pedJimmy)
//				REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			ENDIF
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
				
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			TRIGGER_MUSIC_EVENT("SOL5_ENDING_CS")
			START_CUTSCENE()
		
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
		
			bDontAlwaysLoadTheScene = FALSE
			bDamagePackApplied = FALSE
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)			
			i_current_event++
		BREAK
		
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		
		
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Tracy", CS_TRACYDISANTO))
			AND bDamagePackApplied = FALSE
				IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Tracy", CS_TRACYDISANTO))
					APPLY_PED_DAMAGE_PACK(GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Tracy", CS_TRACYDISANTO)), "SCR_TracySplash", 1.0, 1.0)
					bDamagePackApplied = TRUE
				ENDIF
			ENDIF
		
			IF IS_CUTSCENE_PLAYING()			
			AND bDontAlwaysLoadTheScene = FALSE
				FADE_IN_IF_NEEDED()
				DELETE_ARRAY_OF_PEDS(pedMerryWeather, FALSE)
				CLEAR_AREA(<<-804.5567, 179.0212, 75.7407>>, 30.0, TRUE)
				bDontAlwaysLoadTheScene = TRUE
			ENDIF
			
			IF i_current_event = 2
			AND GET_CUTSCENE_TIME() > 1950
				IF PREPARE_MUSIC_EVENT("SOL5_MICHAEL_CLOBBERED")
					TRIGGER_MUSIC_EVENT("SOL5_MICHAEL_CLOBBERED")
					
					//APPLY_PED_DAMAGE_DECAL(pedIndex,          Zone,     u,     v, rotation, scale, alpha, forceFrame, fadeIn, damageDecalName)
					APPLY_PED_DAMAGE_DECAL(PLAYER_PED_ID(),      PDZ_HEAD, 0.587, 0.782,  341.282, 1.000, 0.571,          3,   TRUE, "bruise") 
					APPLY_PED_DAMAGE_DECAL(PLAYER_PED_ID(),      PDZ_HEAD, 0.587, 0.782,  341.282, 1.000, 0.571,          4,   TRUE, "bruise") 
					APPLY_PED_DAMAGE_DECAL(PLAYER_PED_ID(),      PDZ_HEAD, 0.587, 0.782,  341.282, 1.000, 0.571,          1,   TRUE, "bruise") 
										
					i_current_event = 3
				ENDIF
			ENDIF			
			
			IF GET_CUTSCENE_TIME() > 9230.0
			AND i_current_event = 3
				SET_TIMECYCLE_MODIFIER("micheals_lightsOFF")
				i_current_event = 4
			ENDIF
					
			IF GET_CUTSCENE_TIME() > 10800.4//10834.4
			AND HAS_CUTSCENE_CUT_THIS_FRAME()
			//IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("NVON"))
			AND i_current_event = 4
				CLEAR_TIMECYCLE_MODIFIER()
				SET_NIGHTVISION(TRUE)
				RESET_ADAPTATION()
				i_current_event = 5
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 25766.334//21265.66// 26000.00
			//IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("NVOFF"))
			AND HAS_CUTSCENE_CUT_THIS_FRAME()
			AND i_current_event = 5
				SET_NIGHTVISION(FALSE)
				i_current_event = 6
			ENDIF
				
//			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
//				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
//			ENDIF
			
			IF GET_CUTSCENE_TIME() > 43990.0
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)		
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED() //CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael") //HAS_CUTSCENE_FINISHED()
				DELETE_PED(pedTracey)
				DELETE_PED(pedAmanda)
				DELETE_PED(pedJimmy)
				//RESET_GAME_CAMERA()
				bDialogueDisplayed = FALSE
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				SET_NIGHTVISION(FALSE)
				
				REPLAY_STOP_EVENT()
				
				i_current_event = 0
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				mission_stage = STAGE_MISSION_PASSED
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC DELETE_ALL_ENTITIES()

	bTimeLapseComplete = FALSE

	IF DOES_ENTITY_EXIST(pedJimmy)
		RESET_PED_MOVEMENT_CLIPSET(pedJimmy)
	ENDIF
	RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())

	REMOVE_ANIM_SET("move_characters@Jimmy@slow@")
	REMOVE_ANIM_SET("move_p_m_zero_slow")

	REMOVE_CUTSCENE()

	STOP_AUDIO_SCENES()
		
	bComingOutOfcutscene = FALSE
	
	bJimmyGotToHouseOk = FALSE
	
	bResetFlashTimer = FALSE

	bFailForFiringButMissing = FALSE
	
	iExtraBadGuysComingIntoDiningRoomCount = 0

	IF navBlockArea <> 0
		REMOVE_NAVMESH_BLOCKING_OBJECT(navBlockArea)
	ENDIF
	
	DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, FALSE)

	CLEANUP_UBER_PLAYBACK(TRUE)

	REMOVE_COVER_POINT(cpHallway)
	cpHallway = NULL
	REMOVE_COVER_POINT(cpLounge)
	REMOVE_COVER_POINT(cpDiningRoom)
	
	REMOVE_COVER_POINT(cpKitchen)
	REMOVE_COVER_POINT(cpKitchen2)
	REMOVE_COVER_POINT(cpGarden1)
	REMOVE_COVER_POINT(cpGarden2)
	

	SET_MODEL_AS_NO_LONGER_NEEDED(MESA3)
	DELETE_VEHICLE(badguyCarOutsideHouse1)
	DELETE_VEHICLE(badguyCarOutsideHouse2)
	
	
	DELETE_VEHICLE(carTracey)
	SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_TRACEY)
	DELETE_PED(pedAmanda)
	DELETE_PED(pedTracey)
	
	DELETE_VEHICLE(carMichael)
	DELETE_PED(pedJimmy)

	DELETE_PED(pedAmanda)
	DELETE_PED(pedTracey)
	DELETE_PED(pedHostagetakerTracey)
	DELETE_PED(pedHostagetakerAmanda)
	DELETE_PED(pedWritheGuy)
	
	DELETE_PED(pedLimoDriver)
	
	IF DOES_ENTITY_EXIST(oiCameraDummy)
		DELETE_OBJECT(oiCameraDummy)
	ENDIF
	//CLEANUP_RAPPEL(pedsRappelData[0])
	
	CLEANUP_SET_PIECE_CAR(merryWeatherCar, TRUE)
	CLEANUP_SET_PIECE_CAR(merryWeatherCar2, TRUE)

	//Camera men
	DELETE_ARRAY_OF_OBJECTS(oiRedCarpetObjects)
	DELETE_ARRAY_OF_PEDS(pedsMoviePremier)
		

	DELETE_ARRAY_OF_PEDS(pedMerryWeather)
	DELETE_ARRAY_OF_PEDS(pedMerryWeatherCutscene)
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipPedHostagetaker)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipInsideHouse)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipPedTracey)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTraceysRoom)
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipJimmy)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTuxShop)
			
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipInsideHouse)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipAmanda)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipJimmy)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLimo)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTuxShop)
			
	//iSceneId = -1
		
	//Clean up blip
	INT I
	FOR i = 0 TO NUMBER_OF_MERRYWEATHER -1
		UPDATE_AI_PED_BLIP(pedMerryWeather[i], blipPedMerryWeather[i])				
	ENDFOR
	
	UPDATE_AI_PED_BLIP(merryWeatherCar.thisDriver, merryWeatherCar.blipDriver)
	UPDATE_AI_PED_BLIP(merryWeatherCar.thisPassenger, merryWeatherCar.blipPassenger)
	UPDATE_AI_PED_BLIP(merryWeatherCar.thisPassengerBackLeft, merryWeatherCar.blipPassengerBackLeft)
	UPDATE_AI_PED_BLIP(merryWeatherCar.thisPassengerBackRight, merryWeatherCar.blipPassengerBackRight)
	bAlternateSides = FALSE	
	
ENDPROC

#IF IS_DEBUG_BUILD


PROC debugProcedures()

	iDebugMissionStage = ENUM_TO_INT(mission_stage)

	IF bDebugInitialised = FALSE
		//Init debug stuff.
		
		sol5WidgetGroup = START_WIDGET_GROUP("Michael 4 - Movie Premier")
			
			ADD_WIDGET_BOOL("bIsSuperDebugEnabled", bIsSuperDebugEnabled)
			ADD_WIDGET_BOOL("Enable Debug Processing", bDebugOn)
						
			//ADD_WIDGET_INT_READ_ONLY("iTimelapseCut", sTimelapse.iTimelapseCut)
			ADD_WIDGET_INT_READ_ONLY("i_current_event", i_current_event)
			ADD_WIDGET_INT_READ_ONLY("mission_stage", iDebugMissionStage)
			
			ADD_WIDGET_INT_READ_ONLY("iJimmySunroof", iJimmySunroof)
			ADD_WIDGET_INT_READ_ONLY("iLimoCamStage", iLimoCamStage)
			
			ADD_WIDGET_INT_SLIDER("iSpawnTimerGuy13", iSpawnTimerGuy13, 0, 999999, 500)
			
			ADD_WIDGET_INT_SLIDER("iMissionTimer", iMissionTimer, 0, 9999999, 1000)
			ADD_WIDGET_INT_SLIDER("g_replay.iReplayInt[0]", g_replay.iReplayInt[0], 0, 9999999, 1000)
			
			ADD_WIDGET_INT_READ_ONLY("iAliveCount", iAliveCount)
			ADD_WIDGET_INT_READ_ONLY("iDeathCount", iDeathCount)
						
			ADD_WIDGET_INT_READ_ONLY("iTimeLapseStage", iTimeLapseStage)
						
			ADD_WIDGET_VECTOR_SLIDER("vAmandaScenePos",vAmandaScenePos, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vAmandaSceneRot",vAmandaSceneRot, -360.0, 360.0, 0.5)
											
			ADD_WIDGET_VECTOR_SLIDER("vOffset1",vOffset1, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vOffset2",vOffset2, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vOffset3",vOffset3, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vOffset4",vOffset4, -3000.0, 3000.0, 0.1)
								
								
		STOP_WIDGET_GROUP()		
		
		SET_LOCATES_HEADER_WIDGET_GROUP(sol5WidgetGroup)
	
		//SkipMenuStruct[ENUM_TO_INT(STAGE_TIME_LAPSE)].bSelectable = FALSE
		SkipMenuStruct[ENUM_TO_INT(STAGE_INITIALISE)].bSelectable = FALSE
		SkipMenuStruct[ENUM_TO_INT(STAGE_PICKUP_JIMMY)].sTxtLabel 					= "Stage get Jimmy/Tux"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_GET_TO_MOVIE_PREMIERE)].sTxtLabel 			= "Stage get to Movie Premiere"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_MOVIE_PREMIERE_CUT)].sTxtLabel 				= "Cutscene: mic_4_int"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_GET_TO_MICHAELS_HOUSE)].sTxtLabel 			= "STAGE_GET_TO_MICHAELS_HOUSE" 
		SkipMenuStruct[ENUM_TO_INT(STAGE_ENTER_THE_HOUSE)].sTxtLabel 				= "STAGE_ENTER_THE_HOUSE" 			
		SkipMenuStruct[ENUM_TO_INT(STAGE_SAVE_AMANDA)].sTxtLabel 					= "STAGE_SAVE_AMANDA" 			
		SkipMenuStruct[ENUM_TO_INT(STAGE_SAVE_TRACEY)].sTxtLabel 					= "STAGE_SAVE_TRACEY" 			
		SkipMenuStruct[ENUM_TO_INT(STAGE_TRACEY_CAPTOR_SHOT)].sTxtLabel				= "Cutscene: SOL_5_MCS_1" 		
		SkipMenuStruct[ENUM_TO_INT(STAGE_FIGHT_INCOMING_BAD_GUYS)].sTxtLabel 		= "STAGE_FIGHT_INCOMING_BAD_GUYS" 		
		SkipMenuStruct[ENUM_TO_INT(STAGE_REUNITE_WITH_FAMILY)].sTxtLabel 			= "STAGE_REUNITE_WITH_FAMILY" 	
		SkipMenuStruct[ENUM_TO_INT(STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE)].sTxtLabel 	= "Cutscene: SOL_5_MCS_2" 	
		SkipMenuStruct[ENUM_TO_INT(STAGE_MISSION_PASSED)].sTxtLabel 				= "STAGE_MISSION_PASSED" 	
					
		SkipMenuStruct[ENUM_TO_INT(STAGE_DEBUG)].sTxtLabel 							= "STAGE_DEBUG"                
		
		bDebugInitialised = TRUE
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
		SET_SYNCHRONIZED_SCENE_ORIGIN(iSceneId, vAmandaScenePos, vAmandaSceneRot)
	ENDIF
	
	PRINTLN("stage: ", iDebugMissionStage, " Current event :", i_current_event, "TRIGGERED?:", bKeepAmandaConditionsReturningTrue)
		
IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)

	FLOAT fX, fY, fZ, fW
	VECTOR vCoords
	
	ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
	IF DOES_ENTITY_EXIST(thisCar)
		GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
		vCoords = GET_ENTITY_COORDS(thiscar)
	
		SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
		SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
		SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
		SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
		SAVE_STRING_TO_DEBUG_FILE(")")
		
		
		SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
		SAVE_STRING_TO_DEBUG_FILE(")")
	ENDIF
	
ENDIF
		
	
	IF bDebugOn
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE)
												
			STOP_CUTSCENE(TRUE)
			WHILE NOT HAS_CUTSCENE_FINISHED()
				WAIT(0)
			ENDWHILE
//			WHILE IS_CUTSCENE_ACTIVE()
//				WAIT(0)
//			ENDWHILE
			REMOVE_CUTSCENE()
			
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
			REMOVE_CUTSCENE()
			
			//DESTROY_CAM(sTimelapse.splineCamera)
			
			NEW_LOAD_SCENE_STOP()
			
			REMOVE_ANIM_DICT("misssolomon_5@stairs")				
			REMOVE_ANIM_DICT("misssolomon_5@bedroom")				
			
			REMOVE_BLIP(blipRedCarpet)
			
			DELETE_OBJECT(oiChampagneBottle)
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Champ_01b)
			REMOVE_ANIM_DICT("missmic4jimmy_limo")
			
			DESTROY_ALL_CAMS()
			
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_TRACYDISANTO)
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_JIMMYDISANTO)
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_AMANDATOWNLEY)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_02)
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			STOP_GAMEPLAY_HINT(TRUE)
			
			bKeepAmandaConditionsReturningTrue = FALSE
						
			DELETE_ALL_ENTITIES()
			
			//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), MichaelsWeapon, INFINITE_AMMO, FALSE, FALSE)
			
			KILL_ANY_CONVERSATION()
			
			SET_CUTSCENE_RUNNING(FALSE)
			iJimmySunroof = 0
	
//			iLimoCamIndex = 0
			iLimoCamStage = 0
			
			i_current_event = 0
			
			bMichaelWentInBathroom = FALSE
		
			bOutsideGuySpawned = FALSE
			
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			
			CLEAR_ALL_FLOATING_HELP()
			
			SET_CUTSCENE_RUNNING(FALSE)
								
			//initialiseMission()
			
			//Set stage enum to required stage
			skip_mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
						
			SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				
			//Do additional work when selecting stagePl
			MANAGE_SKIP(skip_mission_stage, FALSE)
			mission_stage = skip_mission_stage
			
			RESET_GAME_CAMERA()
			
			//When a stage has been selected from the menu LAUNCH_MISSION_STAGE_MENU() returns true it passes back the stage that has been selected as an INT iReturnStage
			//This INT can then be used to change the current mission stage using your J/P skip functions.
			// iReturnStage = 0 is returned if STAGE_1 is selected from the menu.
		ENDIF
	

		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)		
			MISSION_PASSED()
		ENDIF
	
		IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
			Mission_Failed()
		ENDIF
			
	ENDIF

ENDPROC

PROC stageRecordUber()	
//
//	INT i
//	VECTOR vSize = <<5.0, 5.0, 5.0>>
	
	SWITCH i_current_event
	
		CASE 0
			REQUEST_MODEL(STRETCH)
			REQUEST_VEHICLE_RECORDING(1, "MIC4")
			WHILE NOT HAS_MODEL_LOADED(STRETCH)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "MIC4")
				WAIT(0)
			ENDWHILE
			carMichael =  CREATE_STRETCH()
			
			SET_ENTITY_COORDS(carMichael, <<-714.0481, -173.6860, 35.8828>>)
			SET_ENTITY_HEADING(carMichael, 28.2506)
			
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carMichael, VS_DRIVER)
			
			INIT_UBER_RECORDING("MIC4")
			i_current_event++
			
		BREAK
		
		CASE 1
			
			UPDATE_UBER_RECORDING()
			
			IF IS_VEHICLE_DRIVEABLE(carMichael)
	            IF IS_RECORDING_GOING_ON_FOR_VEHICLE(carMichael)
	                IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carMichael)                                    
	                    START_PLAYBACK_RECORDED_VEHICLE(carMichael, 1, "MIC4")
	                ENDIF
	            ENDIF	        
			ENDIF
			
		BREAK
	
		
	ENDSWITCH

ENDPROC


#ENDIF	

FUNC BOOL MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN(STRING strThisString)

	IF IS_PLAYER_IN_ANY_SHOP()	//Fix for 1844264
	AND (mission_stage <> STAGE_INITIALISE)
		//SCRIPT_ASSERT("failing 1")
		MISSION_FLOW_SET_FAIL_REASON(strThisString)
		RETURN TRUE
	ENDIF
	
	//IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
	IF IS_SCREEN_FADED_IN()
	AND NOT IS_PLAYER_IN_ANY_SHOP()
		//SCRIPT_ASSERT("failing 2")
		MISSION_FLOW_SET_FAIL_REASON(strThisString)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

INT iTimeOfLeaving
BOOL bLeftLimo = FALSE
INT iTimerSound
INT iSoundCounter
INT iSoundFrequency = 1000

//PURPOSE:		Checks if the user has failed a level stage
FUNC BOOL HAS_MISSION_FAILED()

	IF bDisplayTimer
			
		SWITCH mission_stage
			CASE STAGE_GET_TO_MOVIE_PREMIERE
			CASE STAGE_GET_TO_MICHAELS_HOUSE
			CASE STAGE_ENTER_THE_HOUSE
			//CASE STAGE_SAVE_AMANDA
			//CASE STAGE_SAVE_TRACEY
				
				iMissionTimer = (GET_GAME_TIMER() - iMissionStartTime) //+ g_replay.iReplayInt[0]
	 			
				IF iMissionTimer <= MISSION_TIME_LIMIT - g_replay.iReplayInt[0]
					//Within time range
					IF iMissionTimer > (MISSION_TIME_LIMIT - 10000 - g_replay.iReplayInt[0])
						
						
						IF iMissionTimer > (MISSION_TIME_LIMIT - 10000 - g_replay.iReplayInt[0]) - GET_FRAME_TIME()							
							
							IF iMissionTimer > (MISSION_TIME_LIMIT - 5000 - g_replay.iReplayInt[0])							
								iSoundFrequency = 500
							ENDIF
							
							PRINTLN("TIMER:", (MISSION_TIME_LIMIT - iMissionTimer - g_replay.iReplayInt[0]), " - ", iSoundCounter, " > ", iSoundFrequency )
							
							IF ABSI((MISSION_TIME_LIMIT - iMissionTimer - g_replay.iReplayInt[0]) - iSoundCounter) > iSoundFrequency
								PLAY_SOUND_FRONTEND(iTimerSound, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
								iSoundCounter = (MISSION_TIME_LIMIT - iMissionTimer - g_replay.iReplayInt[0])
							ENDIF				
							
						ENDIF
													
						
						//Draw red
						IF bResetFlashTimer = FALSE
							//Start flashing
							
							DRAW_GENERIC_TIMER(MISSION_TIME_LIMIT - iMissionTimer - g_replay.iReplayInt[0], "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, 0,
											PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
							bResetFlashTimer = TRUE
						ELSE
							DRAW_GENERIC_TIMER(MISSION_TIME_LIMIT - iMissionTimer - g_replay.iReplayInt[0], "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, 10000,
											PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
						ENDIF
					ELSE
						//Draw time normally in red
						DRAW_GENERIC_TIMER(MISSION_TIME_LIMIT - iMissionTimer - g_replay.iReplayInt[0], "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, 0,
											PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_WHITE)
					ENDIF
				ELSE
				
					IF iTimerSound <> 0
						STOP_SOUND(iTimerSound)
						PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
					ENDIF
				
					//Below zero, cap at zero
					DRAW_GENERIC_TIMER(0, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, 110000,
											PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED)
				ENDIF
				
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				
				//PRINTLN("iMissionTimer", iMissionTimer)
							
				IF iMissionTimer + g_replay.iReplayInt[0] > MISSION_TIME_LIMIT + 2000   //iMissionTimer > MISSION_TIME_LIMIT + 3000
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_TIMEUP")
				ENDIF
				
			BREAK 
		ENDSWITCH
	ENDIF
	
	SWITCH mission_stage
		CASE STAGE_ENTER_THE_HOUSE
		CASE STAGE_SAVE_AMANDA
		CASE STAGE_SAVE_TRACEY
		CASE STAGE_FIGHT_INCOMING_BAD_GUYS
		CASE STAGE_REUNITE_WITH_FAMILY
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMichaelsHouse) > 150.0
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_LEAVE")
			ENDIF
		BREAK 
	ENDSWITCH
	
	SWITCH mission_stage

//		CASE STAGE_TIME_LAPSE
//		BREAK
		
		CASE STAGE_PICKUP_JIMMY
		
			IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
			AND i_current_event >= 1
				IF NOT IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_H_01_BH)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_FAILTUX")
						
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(carMichael)
				IF NOT IS_VEHICLE_DRIVEABLE(carMichael)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_LIMODEAD")
					
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedLimoDriver)
				IF IS_PED_INJURED(pedLimoDriver)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_DRIVERDEAD")
					
				ENDIF
			ENDIF
			
			IF GET_CLOCK_HOURS() > 3
			AND GET_CLOCK_HOURS() < 21
			AND IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
			AND DOES_ENTITY_EXIST(pedJimmy)
			AND NOT IS_PLAYER_IN_ANY_SHOP()
				IF NOT IS_ENTITY_DEAD(carMichael)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
						RETURN FALSE
					ENDIF
				ENDIF
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_MISSED")
				
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_DISRUPT")
			ENDIF	
			
			IF DOES_ENTITY_EXIST(pedJimmy)
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_PED_RESET_FLAG(pedJimmy, PRF_DisablePotentialBlastReactions, TRUE)
				
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<80.0, 80.0, 80.0>>)
						IF bWarnedLeavingJimmy = FALSE
							PRINT_NOW("CMN_JLEAVE", DEFAULT_GOD_TEXT_TIME, 1)
							bWarnedLeavingJimmy = TRUE
						ENDIF
					ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<70.0, 70.0, 70.0>>)
						bWarnedLeavingJimmy = FALSE
					ENDIF
				
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<100.0, 100.0, 100.0>>)
						RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("CMN_JLEFT")
						
					ENDIF
				ELSE
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_JDIED")
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE STAGE_GET_TO_MOVIE_PREMIERE
		
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_DISRUPT")
			ENDIF	
		
			IF DOES_ENTITY_EXIST(pedLimoDriver)
				IF IS_PED_INJURED(pedLimoDriver)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_DRIVERDEAD")
					
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(carMichael)
				IF NOT IS_VEHICLE_DRIVEABLE(carMichael)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_LIMODEAD")
					
				ENDIF
			ENDIF
		
			IF GET_CLOCK_HOURS() > 3 //Give them an hour after the arrive before they fail
			AND GET_CLOCK_HOURS() < 21
				IF NOT IS_ENTITY_DEAD(carMichael)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
						RETURN FALSE
					ENDIF
				ENDIF
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_MISSED")
				
			ENDIF
		
			IF DOES_ENTITY_EXIST(pedJimmy)
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_PED_RESET_FLAG(pedJimmy, PRF_DisablePotentialBlastReactions, TRUE)
				
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<80.0, 80.0, 80.0>>)
						IF bWarnedLeavingJimmy = FALSE
							PRINT_NOW("CMN_JLEAVE", DEFAULT_GOD_TEXT_TIME, 1)
							bWarnedLeavingJimmy = TRUE
						ENDIF
					ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<70.0, 70.0, 70.0>>)
						bWarnedLeavingJimmy = FALSE
					ENDIF
				
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<100.0, 100.0, 100.0>>)
						RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("CMN_JLEFT")
						
					ENDIF
				ELSE
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_JDIED")
					
				ENDIF
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(carMichael)
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carMichael)
					IF bLeftLimo = FALSE
						iTimeOfLeaving = GET_GAME_TIMER()
						bLeftLimo = TRUE
					ENDIF
				ELSE
					iTimeOfLeaving = GET_GAME_TIMER()
					bLeftLimo = FALSE
				ENDIF
								
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carMichael) > 100.0
				OR GET_GAME_TIMER() - iTimeOfLeaving > 5000
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_NOLIMO")
					
				ENDIF
			ENDIF
		
		BREAK
		
		CASE STAGE_MOVIE_PREMIERE_CUT
		
			INT i
			FOR i = 0 TO MOVP_MAX_PEDS - 1
				IF DOES_ENTITY_EXIST(pedsMoviePremier[i])
					IF IS_PED_INJURED(pedsMoviePremier[i])
						RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_DISRUPT")
							
					ENDIF
				ENDIF
			ENDFOR
			
			IF DOES_ENTITY_EXIST(pedJimmy)
				IF IS_PED_INJURED(pedJimmy)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_JDIED")
				ELSE
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<60.0, 60.0, 60.0>>)
						RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("CMN_JLEFT")			
					ENDIF
				ENDIF
			ENDIF
			
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_DISRUPT")
					
			ENDIF	
			
			
			
//			IF GET_CLOCK_HOURS() > iArrivalTime + 1
//			AND GET_CLOCK_HOURS() < 21
//				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_MISSED")
//				
//			ENDIF
		
		BREAK
		
		CASE STAGE_GET_TO_MICHAELS_HOUSE
				//Jimmy fail stuff
			IF DOES_ENTITY_EXIST(pedJimmy)
				IF NOT IS_PED_INJURED(pedJimmy)
					SET_PED_RESET_FLAG(pedJimmy, PRF_DisablePotentialBlastReactions, TRUE)
				
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<80.0, 80.0, 80.0>>)
						IF bWarnedLeavingJimmy = FALSE
							PRINT_NOW("CMN_JLEAVE", DEFAULT_GOD_TEXT_TIME, 1)
							bWarnedLeavingJimmy = TRUE
						ENDIF
					ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<70.0, 70.0, 70.0>>)
						bWarnedLeavingJimmy = FALSE
					ENDIF
				
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<100.0, 100.0, 100.0>>)
						MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("CMN_JLEFT")
						
					ENDIF
					
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMichaelsHouse, <<12.0, 4.0, 12.0>>)	
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-822.901489,171.583603,68.902214>>, <<-774.385681,170.937317,83.796524>>, 31.750000)
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_COPS")
						ENDIF
						
						
						IF NOT (IS_ENTITY_AT_COORD(pedJimmy, vMichaelsHouse, <<12.0, 4.0, 12.0>>)	
							OR IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-822.901489,171.583603,68.902214>>, <<-774.385681,170.937317,83.796524>>, 31.750000))
							
								IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<50.0, 50.0, 30.0>>)
									RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("CMN_JLEFT")
									
								ENDIF
						ENDIF
					ENDIF
					
					
				ELSE
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_JDIED")
					
				ENDIF
			ENDIF
		BREAK
		
		CASE STAGE_ENTER_THE_HOUSE
		
			IF NOT bJimmyGotToHouseOk
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMichaelsHouse, <<12.0, 4.0, 12.0>>)	
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-822.901489,171.583603,68.902214>>, <<-774.385681,170.937317,83.796524>>, 31.750000)
					IF NOT (IS_ENTITY_AT_COORD(pedJimmy, vMichaelsHouse, <<12.0, 4.0, 12.0>>)	
						OR IS_ENTITY_IN_ANGLED_AREA(pedJimmy, <<-822.901489,171.583603,68.902214>>, <<-774.385681,170.937317,83.796524>>, 31.750000))
							IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedJimmy, <<50.0, 50.0, 30.0>>)
								TRIGGER_MUSIC_EVENT("SOL5_FAIL")
								RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("CMN_JLEFT")
								
							ENDIF
					ELSE
						bJimmyGotToHouseOk = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("MIC4_COPS")
			ENDIF
			
			IF IS_PED_INJURED(pedJimmy)
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_JDIED")
				
			ENDIF
			
			IF IS_PED_INJURED(pedAmanda)
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_ADIED")
				
			ENDIF
			
		BREAK
		
		CASE STAGE_SAVE_AMANDA
			IF IS_PED_INJURED(pedAmanda)
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_ADIED")
				
			ENDIF
		BREAK
		
		
		CASE STAGE_SAVE_TRACEY
			IF IS_PED_INJURED(pedAmanda)
				RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_ADIED")
			ELSE
				IF IS_ENTITY_ON_FIRE(pedAmanda)
					RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_ADIED")
				ENDIF
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.9 //0.7
					IF IS_PED_INJURED(pedTracey)
						RETURN MISSION_FLOW_SET_FAIL_REASON_WHEN_FADED_IN("SOL5_TDIED")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE STAGE_TRACEY_CAPTOR_SHOT
		CASE STAGE_FIGHT_INCOMING_BAD_GUYS
		CASE STAGE_REUNITE_WITH_FAMILY
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

// ____________________________________ MISSION SCRIPT _____________________________________

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
        STORE_FAIL_WEAPON(PLAYER_PED_ID(), 0)
		SET_TUXEDOS_AVAILABLE(FALSE) // lock the tuxedos again
		Mission_Flow_Mission_Force_Cleanup()
		MISSION_CLEANUP()  
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// ____________________________________ MISSION LOOP _______________________________________
	
	SET_MISSION_FLAG(TRUE)
	//INFORM_MISSION_STATS_OF_MISSION_START_SOLOMON_5()
		
	//BREAK_ON_NATIVE_COMMAND("SET_ENTITY_COORDS", FALSE)
		
	// set tuxedos as available
	SET_TUXEDOS_AVAILABLE(TRUE)
	
	WHILE TRUE	
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Meltdown")
		
		IF iDisableReplayCameraTimer > GET_GAME_TIMER()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2227677
		ENDIF
		
		#IF IS_DEBUG_BUILD
			debugProcedures()
			//mission_stage = STAGE_RECORD_UBER //STAGE_DEBUG	//Comment out...
		#ENDIF
					
		SWITCH mission_stage 
		
			#IF IS_DEBUG_BUILD
		
				CASE STAGE_DEBUG
					debugRoutines()
				BREAK
			
			#ENDIF
		
			//Stage 0		
		
			CASE STAGE_INITIALISE	
				initialiseMission()		
			BREAK		
			
			CASE STAGE_PICKUP_JIMMY
				stagePickUpJimmy()		
			BREAK
			
			CASE STAGE_GET_TO_MOVIE_PREMIERE
				stageGetToMoviePremiere()
			BREAK
			
			CASE STAGE_MOVIE_PREMIERE_CUT
				stageIntroMocapCutscene()		
			BREAK		
						
			CASE STAGE_GET_TO_MICHAELS_HOUSE
				stageGetToMichaelsHouse()		
			BREAK		
			
			CASE STAGE_ENTER_THE_HOUSE
				stageEnterTheHouse()		
			BREAK	
			
			CASE STAGE_SAVE_AMANDA
				stageSaveAmanda()
			BREAK	
			
			CASE STAGE_SAVE_TRACEY
				stageSaveTracey()
			BREAK
			
			CASE STAGE_TRACEY_CAPTOR_SHOT
				stageMCS1()
			BREAK
						
			CASE STAGE_FIGHT_INCOMING_BAD_GUYS
				stageFightIncomingBadGuys()
			BREAK	
			
			CASE STAGE_REUNITE_WITH_FAMILY
				stageReuniteWithFamily()
			BREAK	
			
			CASE STAGE_JIMMY_SAVES_THE_DAY_CUTSCENE
				stageJimmySavesTheDayCutscene()
			BREAK	
			
			CASE STAGE_MISSION_PASSED
				mission_Passed()		
			BREAK	
			
			#IF IS_DEBUG_BUILD
			CASE STAGE_RECORD_UBER
				stageRecordUber()		
			BREAK	
			#ENDIF
			
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH	
		
		
		IF HAS_MISSION_FAILED()
			Mission_Failed()
		ENDIF
				
		WAIT(0)
		
	ENDWHILE
	
	//	should never reach here - always ends by going through the cleanup function

ENDSCRIPT					



