USING "Michael3_support.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_itemsets.sch"
USING "selector_public.sch"
USING "locates_public.sch"
USING "taxi_functions.sch"

ENUM	ESCAPE_MUSEUM_STATE
	ESCAPE_MUSEUM_INIT,
	ESCAPE_MUSEUM_STREAMING,
	ESCAPE_MUSEUM_WAIT_FOR_PREVIOUS_SWITCH_TO_FINISH,
	ESCAPE_MUSEUM_STARTING_FORCE_SWITCH,
	ESCAPE_MUSEUM_WAIT_FOR_FORCE_SWITCH,
	ESCAPE_MUSEUM_COMBAT,
	ESCAPE_MUSEUM_IDLE,
	ESCAPE_MUSEUM_COMPLETE
ENDENUM
ESCAPE_MUSEUM_STATE	eEscapeMuseumState = ESCAPE_MUSEUM_INIT
MISSION_MESSAGE		eEscapeMuseumMessages = MISSION_MESSAGE_01



TRIGGER_BOX			tb_ParkingLot
TRIGGER_BOX			tb_EntireMuseum

PROC CREATE_ESCAPE_MUSEUM_TRIGGERS()
	tb_ParkingLot = CREATE_TRIGGER_BOX(<<-2314.240723,296.678375,168.130051>>, <<-2350.230225,280.649719,172.546204>>, 50.000000)
	tb_EntireMuseum = CREATE_TRIGGER_BOX(<<-2183.566406,141.560059,202.299606>>, <<-2322.525146,433.020111,81.851501>>, 221.750000)
ENDPROC

FUNC BOOL IS_PLAYER_IN_ANY_ESCAPE_AREA()
	IF IS_PLAYER_AT_ANGLED_AREA_ANY_MEANS(s_locatesDataMike, v_blip_parking_lot_coord, tb_ParkingLot.vMin, tb_ParkingLot.vMax, tb_ParkingLot.flWidth, TRUE, "M3_OBJPLOT", TRUE, TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_ESCAPE_MUSEUM_CONVERSATIONS()

	IF m_bDavePlayGetInCarMessage
		m_bDavePlayGetInCarMessage = FALSE
		eEscapeMuseumMessages = MISSION_MESSAGE_10
	ENDIF

	SWITCH eEscapeMuseumMessages
		CASE MISSION_MESSAGE_01
			IF Play_Conversation("M3_EMDVGBYE")
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				eEscapeMuseumMessages = MISSION_MESSAGE_02
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF IS_SAFE_TO_START_CONVERSATION()
				CLEAR_MISSION_LOCATION_BLIP(s_locatesDataMike)
				eEscapeMuseumMessages = MISSION_MESSAGE_03
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_03
			IF Play_Conversation("M3_FOLLOW1")
				eEscapeMuseumMessages = MISSION_MESSAGE_04
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_04
			IF Play_Conversation("M3_FOLLOW2")
				eEscapeMuseumMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		
		CASE MISSION_MESSAGE_10
			IF Play_Conversation("M3_GETINCAR")
				eEscapeMuseumMessages = MISSION_MESSAGE_11
			ENDIF
		BREAK
	
		CASE MISSION_MESSAGE_11
			IF Play_Conversation("M3_BYEDAVE")
				eEscapeMuseumMessages = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

PROC MISSION_STAGE_ESCAPE_MUSEUM()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	IF eEscapeMuseumState > ESCAPE_MUSEUM_STREAMING
		UPDATE_ESCAPE_MUSEUM_CONVERSATIONS()
		UPDATE_OTHER_PLAYER_FLEEING_MUSEUM()
		RETASK_LAST_PARKING_LOT_PED()
		UPDATE_DAVE_FLEEING_MUSEUM()
		UPDATE_ALL_ENEMY_BLIPS()
		
		IF NOT IS_MISSION_EVENT_TRIGGERED(sEvents[mef_fight_parking_lot].sData)
			TRIGGER_MISSION_EVENT(sEvents[mef_fight_parking_lot].sData)
			TRIGGER_MISSION_EVENT(sEvents[mef_parking_lot_jeep].sData, 5000)
		ELSE
			UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_1].id, peds[mpf_pl_1_mw_1].stealthBlip, -1, NULL, TRUE)
			UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_2].id, peds[mpf_pl_1_mw_2].stealthBlip, -1, NULL, TRUE)
			UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_3].id, peds[mpf_pl_1_mw_3].stealthBlip, -1, NULL, TRUE)
			UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_4].id, peds[mpf_pl_1_mw_4].stealthBlip, -1, NULL, TRUE)
			UPDATE_AI_PED_BLIP(peds[mpf_pl_1_mw_5].id, peds[mpf_pl_1_mw_5].stealthBlip, -1, NULL, TRUE)
			UPDATE_AI_PED_BLIP(peds[mpf_mw_jeep_2_1].id, peds[mpf_mw_jeep_2_1].stealthBlip, -1, NULL, TRUE)
			UPDATE_AI_PED_BLIP(peds[mpf_mw_jeep_2_2].id, peds[mpf_mw_jeep_2_2].stealthBlip, -1, NULL, TRUE)
		ENDIF
	ENDIF
	
	IF m_bDoStairFires
		BLOCK_STAIRWAY_PATH()
	ENDIF
	
	SWITCH eEscapeMuseumState
	
		CASE ESCAPE_MUSEUM_INIT
			
			//Prep the mission progression system here
			Load_Asset_Model(sAssetData, mod_ped_fib)
			Load_Asset_Model(sAssetData, mod_ped_cia)
			Load_Asset_Model(sAssetData, mod_ped_mw)
			Load_Asset_Model(sAssetData, CARBONIZZARE)
			Load_Asset_Model(sAssetData, DOMINATOR)
			Load_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
			Load_Asset_Recording(sAssetData, rec_otherp_esc, str_carrecs)
			Load_Asset_Waypoint(sAssetData, m_strDaveWaypointRec)
			Load_Asset_Waypoint(sAssetData, m_strOtherPlayerWaypointRec)
			
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_escape_3].id)
			AND HAS_MODEL_LOADED(CARBONIZZARE)
				vehs[mvf_escape_3].id = CREATE_VEHICLE(CARBONIZZARE, << -2319.7976, 302.1291, 168.4667 >>, 115.6683)
			ENDIF
			CREATE_ESCAPE_MUSEUM_TRIGGERS()
			eDaveFleeState = DAVE_FLEE_INIT
			m_bDavePlayGetInCarMessage = FALSE
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_STREAMING")
			eEscapeMuseumState = ESCAPE_MUSEUM_STREAMING
		BREAK
		
		CASE ESCAPE_MUSEUM_STREAMING
			IF HAS_MODEL_LOADED(mod_ped_fib)
			AND HAS_MODEL_LOADED(mod_ped_cia)
			AND HAS_MODEL_LOADED(mod_ped_mw)
			AND HAS_MODEL_LOADED(CARBONIZZARE)
			AND HAS_MODEL_LOADED(DOMINATOR)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_dave_esc, str_carrecs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_otherp_esc, str_carrecs)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(m_strOtherPlayerWaypointRec)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(m_strDaveWaypointRec)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "ESCAPE MUSEUM", FALSE)
				TRIGGER_MISSION_EVENT(sEvents[mef_escape_vehicles].sData)
				eEscapeMuseumMessages = MISSION_MESSAGE_01
				DISABLE_TAXI_HAILING()
				Clear_And_Block_Area(TRUE)
				STOP_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
				STOP_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
				STOP_AUDIO_SCENE("MI_3_KILL_ENEMIES_IN_COURTYARD")
				START_AUDIO_SCENE("MI_3_ESCAPE_IN_CAR")
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_WAIT_FOR_PREVIOUS_SWITCH_TO_FINISH")
				eEscapeMuseumState = ESCAPE_MUSEUM_WAIT_FOR_PREVIOUS_SWITCH_TO_FINISH
			ENDIF
		BREAK
		
		CASE ESCAPE_MUSEUM_WAIT_FOR_PREVIOUS_SWITCH_TO_FINISH
			IF ePlayerSwitchState = PLAYER_SWITCH_SETUP
			OR ePlayerSwitchState = PLAYER_SWITCH_SELECTING
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				AND g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_ON_ROOF
					m_pedOtherPlayer = MIKE_PED_ID()
					Prep_Hotswap(TRUE, TRUE, TRUE)
					RESTART_TIMER_NOW(m_tmrDelayBeforeSwitch)
					CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_STARTING_FORCE_SWITCH")
					eEscapeMuseumState = ESCAPE_MUSEUM_STARTING_FORCE_SWITCH
				ELSE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						m_pedOtherPlayer = MIKE_PED_ID()
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						m_pedOtherPlayer = TREV_PED_ID()
					ENDIF
					
					CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_COMBAT")
					eEscapeMuseumState = ESCAPE_MUSEUM_COMBAT
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "Player is still switching, wait for this to complete")
				UPDATE_PLAYER_SWITCHING()
			ENDIF
		BREAK
	
		CASE ESCAPE_MUSEUM_STARTING_FORCE_SWITCH
			IF IS_TIMER_STARTED(m_tmrDelayBeforeSwitch)
				IF GET_TIMER_IN_SECONDS(m_tmrDelayBeforeSwitch) >= 1.0
					Prep_Hotswap()
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					sCamDetails.bSplineCreated 	= TRUE
					sCamDetails.pedTo 			= sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
					sCamDetails.bRun  			= TRUE
					CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_WAIT_FOR_FORCE_SWITCH")
					eEscapeMuseumState = ESCAPE_MUSEUM_WAIT_FOR_FORCE_SWITCH
				ENDIF
			ENDIF
		BREAK
	
		CASE ESCAPE_MUSEUM_WAIT_FOR_FORCE_SWITCH
			IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)
				IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					CLEANUP_PED_STRUCT_PEDS(m_psMWIG_8, TRUE)
					CLEANUP_PED_STRUCT_PEDS(m_psTrevorDangerPeds, TRUE)
					Prep_Hotswap(TRUE, TRUE, TRUE)
					SAFE_REMOVE_BLIP(blipOtherPlayer)
					SAFE_REMOVE_BLIP(peds[mpf_trev].blip)
					SAFE_REMOVE_BLIP(peds[mpf_mike].blip)
					IF IS_ENTITY_OK(PLAYER_PED_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					IF IS_ENTITY_OK(TREV_PED_ID())
						SET_ENTITY_PROOFS(TREV_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
					ENDIF
					CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_COMBAT")
					eEscapeMuseumState = ESCAPE_MUSEUM_COMBAT
				ENDIF
			ENDIF
		BREAK
	
		CASE ESCAPE_MUSEUM_COMBAT
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			IF IS_PLAYER_IN_ANY_ESCAPE_AREA()
			OR NOT IS_PLAYER_IN_TRIGGER_BOX(tb_EntireMuseum)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_MICHAEL
				ENDIF
				
				IF IS_ENTITY_IN_TRIGGER_BOX(m_tbLargeRoofVolume, TREV_PED_ID())
				OR GET_DISTANCE_BETWEEN_ENTITIES(TREV_PED_ID(), vehs[mvf_trev_heli].id) <= 30.0
					g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_ON_ROOF
				ELSE
					g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
				ENDIF
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - going to ESCAPE_MUSEUM_COMPLETE")
				eEscapeMuseumState = ESCAPE_MUSEUM_COMPLETE
			ENDIF
		BREAK
		
		CASE ESCAPE_MUSEUM_COMPLETE
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_ESCAPE_MUSEUM - Stage complete, going to ESCAPE_MUSEUM_IDLE")
			DELETE_PED(peds[mpf_mw_heli_dave_escape_pilot].id)
			SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_mw_heli_2_pilot].id)
			DELETE_VEHICLE(vehs[mvf_mw_heli_dave_escape].id)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mw_heli_2].id)
			CLEANUP_PED_STRUCT_PEDS(m_psTrevorDangerPeds)
			CLEANUP_PED_STRUCT_PEDS(m_psFinalWalkwayCIA)
			CLEANUP_PED_STRUCT_PEDS(m_psWarCIA)
			CLEANUP_PED_STRUCT_PEDS(m_psWarFIB)
			CLEANUP_PED_STRUCT_PEDS(m_psWarMWCourtyard)
			eEscapeMuseumState = ESCAPE_MUSEUM_IDLE
			Mission_Set_Stage(STAGE_VEHICLE_ESCAPE)
		BREAK
		
		CASE ESCAPE_MUSEUM_IDLE
		BREAK
	ENDSWITCH
ENDPROC
