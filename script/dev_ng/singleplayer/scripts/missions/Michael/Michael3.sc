/*
											 _     _  _______  _______  ______   _______  ______    ______ 
											(_)   (_)(_______)(_______)(______) (_______)(_____ \  / _____)
											 _______  _____    _______  _     _  _____    _____) )( (____  
											|  ___  ||  ___)  |  ___  || |   | ||  ___)  |  __  /  \____ \ 
											| |   | || |_____ | |   | || |__/ / | |_____ | |  \ \  _____) )
											|_|   |_||_______)|_|   |_||_____/  |_______)|_|   |_|(______/ 
*/                                                             

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_public_core_override.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"

USING "selector_public.sch"
USING "locates_public.sch"

USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_itemsets.sch"

USING "CompletionPercentage_public.sch"
USING "asset_management_public.sch"
USING "rgeneral_include.sch"
USING "mission_event_manager.sch"
USING "rgeneral_include.sch"
USING "clearMissionArea.sch"
USING "cheat_controller_public.sch"

USING "Michael3_support.sch"
USING "Michael3_stage_walk_to_dave.sch"
USING "Michael3_stage_firstarea.sch"
USING "Michael3_stage_heli_shootout.sch"
USING "Michael3_stage_trevor_save_dave.sch"
USING "Michael3_stage_dave_at_fountain.sch"
USING "Michael3_stage_dave_escapes.sch"
USING "Michael3_stage_escape_museum.sch"
USING "Michael3_stage_vehicle_escape.sch"
USING "commands_recording.sch"
USING "script_misc.sch"


/*
													 _______  _     _  _______  _______  _______  ______ 
													(_______)(_)   (_)(_______)(_______)(_______)/ _____)
													 _____    _     _  _____    _     _     _   ( (____  
													|  ___)  | |   | ||  ___)  | |   | |   | |   \____ \ 
													| |_____  \ \ / / | |_____ | |   | |   | |   _____) )
													|_______)  \___/  |_______)|_|   |_|   |_|  (______/ 
*/

PROC EVENT_manage_radar(MISSION_EVENT_DATA &sData)
	
	//Exit early if in vehicle escape and player in a vehicle so GPS shows on radar
	IF mission_stage = ENUM_TO_INT(STAGE_VEHICLE_ESCAPE)
		IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
			SET_RADAR_ZOOM_PRECISE(0)
			EXIT
		ENDIF
	ENDIF
	
	IF IS_MISSION_EVENT_RUNNING(sData)
		INT iLevel = -1
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2217.408447,162.607788,163.562927>>, <<-2340.652832,440.289642,178.9>>, 197.750000)
			iLevel = 0
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2217.408447,162.607788,178.9>>, <<-2340.652832,440.289642,188.7>>, 197.750000)
			iLevel = 1
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2217.408447,162.607788,188.7>>, <<-2340.652832,440.289642,201.0>>, 197.750000)
			iLevel = 2
		ENDIF
		
		IF iLevel >= 0
			SET_RADAR_ZOOM_PRECISE(60)
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakeKortzCenter"), -2250.0, 300.0, 0, iLevel)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Updates the area detection stuff for michael's ai
PROC EVENT_manage_ped_spatial_data(MISSION_EVENT_DATA &sData)

	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE_PED_ID(), v_blip_museum_entrance) > 250
		END_MISSION_EVENT(sData)
	ENDIF
	
	IF NOT IS_MISSION_EVENT_ENDED(sData)

		INT i, j
		BOOL bHasMovedOutOfCurrentArea
		
		// UPDATE the section
		IF eCurrentSection != MUS_NO_AREA
			eNextSection = sMuseumSections[eCurrentSection].eNextSection
			ePrevSection = sMuseumSections[eCurrentSection].ePrevSection
		
			IF NOT IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), sMuseumSections[eCurrentSection].v1, sMuseumSections[eCurrentSection].v2, sMuseumSections[eCurrentSection].fWidth)			
				IF eNextSection != MUS_NO_AREA
				AND IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), sMuseumSections[eNextSection].v1, sMuseumSections[eNextSection].v2, sMuseumSections[eNextSection].fWidth)
					eCurrentSection = eNextSection
					eNextSection = sMuseumSections[eCurrentSection].eNextSection
					ePrevSection = sMuseumSections[eCurrentSection].ePrevSection
					CDEBUG1LN(DEBUG_MISSION, "MIKE AI: Entered next section: ", enum_to_int(eCurrentSection))
				ELIF ePrevSection != MUS_NO_AREA
				AND IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), sMuseumSections[ePrevSection].v1, sMuseumSections[ePrevSection].v2, sMuseumSections[ePrevSection].fWidth)
					eCurrentSection = ePrevSection
					eNextSection = sMuseumSections[eCurrentSection].eNextSection
					ePrevSection = sMuseumSections[eCurrentSection].ePrevSection
					CDEBUG1LN(DEBUG_MISSION, "MIKE AI: Entered prev section: ", enum_to_int(eCurrentSection))
				ELSE
					// not in the current, next or prev area
					bHasMovedOutOfCurrentArea = TRUE
				ENDIF
				
				bHasMikeEnteredNewArea = TRUE
			ELSE
				bHasMikeEnteredNewArea = FALSE
			ENDIF
		ENDIF
		bHasMikeEnteredNewArea = bHasMikeEnteredNewArea
		// search all of the areas for michael
		IF bHasMovedOutOfCurrentArea
		OR eCurrentSection = MUS_NO_AREA
			i = 0
			WHILE i >= 0

				// DO NOT CHECK PREV AND NEXT AREAS AGAIN!!! wasteful!
				IF eCurrentSection = MUS_NO_AREA 
				OR (i != enum_to_int(eCurrentSection) 
				AND i != enum_to_int(sMuseumSections[eCurrentSection].eNextSection) 
				AND i != enum_to_int(sMuseumSections[eCurrentSection].ePrevSection))
					
					IF IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), sMuseumSections[i].v1, sMuseumSections[i].v2, sMuseumSections[i].fWidth)
						eCurrentSection = int_to_enum(MUSEUM_SECTIONS, i)
						eNextSection = sMuseumSections[eCurrentSection].eNextSection
						ePrevSection = sMuseumSections[eCurrentSection].ePrevSection
						CDEBUG1LN(DEBUG_MISSION, "MIKE AI: Found mike in area: ", enum_to_int(eCurrentSection))
						i = -1
					ENDIF
				ENDIF
				
				IF i != -1
					i++
					IF i > enum_to_int(MUS_NUM_SECTIONS)-1
						eCurrentSection = MUS_NO_AREA
						i = -1 // kills the loop
						CDEBUG1LN(DEBUG_MISSION, "MIKE AI: Mike is not in any area!!!")
					ENDIF
				ENDIF
				
			ENDWHILE
		ENDIF // UPDATE END!
		
	// <<<<<<<< ENEMIES >>>>>>>>>>>>>>>>>>>>>

		IF sData.iStage = 0
			sData.iStage = enum_to_int(mpf_andreas) + 1
		ENDIF
		
		INT iUpdateAttempts			= 0
		BOOL bUpdatedAnEnemy		= FALSE
		
		WHILE NOT bUpdatedAnEnemy
		
			i = sData.iStage
			
			IF DOES_ENTITY_EXIST(peds[i].id)
			AND NOT IS_PED_INJURED(peds[i].id)
			AND (GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_MW
				OR GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_CIA
				OR GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_FIB)
				
			// Update the area this particular ped is in
				IF peds[i].eSectionPedIsIn = MUS_NO_AREA
				OR NOT IS_ENTITY_IN_ANGLED_AREA(peds[i].id, sMuseumSections[peds[i].eSectionPedIsIn].v1, sMuseumSections[peds[i].eSectionPedIsIn].v2, sMuseumSections[peds[i].eSectionPedIsIn].fWidth)

					j = 0
					WHILE j < enum_to_int(MUS_NUM_SECTIONS)
						IF IS_ENTITY_IN_ANGLED_AREA(peds[i].id, sMuseumSections[j].v1, sMuseumSections[j].v2, sMuseumSections[j].fWidth)
							peds[i].eSectionPedIsIn 	= int_to_enum(MUSEUM_SECTIONS, j)
							j 							= enum_to_int(MUS_NUM_SECTIONS)
						ELSE
							j++
							IF j >= enum_to_int(MUS_NUM_SECTIONS)
								peds[i].eSectionPedIsIn = MUS_NO_AREA
							ENDIF
						ENDIF
					ENDWHILE
					
				ELSE
					bEnemyInArea[peds[i].eSectionPedIsIn] = TRUE
				ENDIF
				
			// Update peds distance from Michael
				IF DOES_ENTITY_EXIST(MIKE_PED_ID())
				AND NOT IS_PED_INJURED(MIKE_PED_ID())
					peds[i].fSquareDistFromMike = VDIST2(GET_ENTITY_COORDS(MIKE_PED_ID()), GET_ENTITY_COORDS(peds[i].id))
				ENDIF
				
				bUpdatedAnEnemy = TRUE
				
			// Ped was not valid for an update
			ELSE
				iUpdateAttempts++
			ENDIF
			
			sData.iStage++
			IF iUpdateAttempts > enum_to_int(mpf_num_peds)
				bUpdatedAnEnemy = TRUE
			ENDIF
			
			IF sData.iStage >= enum_to_int(mpf_num_peds)
				sData.iStage = enum_to_int(mpf_andreas) + 1
			ENDIF
			
		ENDWHILE
		
		
		
		// Check Trevor's LOS to each enemy
		// Calling on all peds each frame as CAN_PED_SEE_HATED_PED is asynchronous code side and must be called every frame in order to successfully work.
		DO_ALL_TREVOR_LOS_CHECKS()
		
		
		// Reset the area count
		REPEAT COUNT_OF(bEnemyInArea) i
			bEnemyInArea[i] = FALSE
		ENDREPEAT
		
		REPEAT mpf_num_peds i
			IF DOES_ENTITY_EXIST(peds[i].id)
			AND NOT IS_PED_INJURED(peds[i].id)
			AND (GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_MW
				OR GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_CIA
				OR GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_FIB)
				
				REPEAT MUS_NUM_SECTIONS j
					IF j = enum_to_int(peds[i].eSectionPedIsIn)
						bEnemyInArea[j] = TRUE
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


PROC EVENT_fight_parking_lot(MISSION_EVENT_DATA &sData)
	SWITCH sData.iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_ped_mw)
			Load_Asset_Model(sAssetData, mod_mw_jeep)
			Load_Asset_Model(sAssetData, mod_mw_van)
		
			IF HAS_MODEL_LOADED(mod_mw_jeep)
			AND HAS_MODEL_LOADED(mod_mw_van)
			AND HAS_MODEL_LOADED(mod_ped_mw)
			
				IF NOT DOES_ENTITY_EXIST(vehs[mvf_mw_jeep_1].id)
				AND NOT DOES_ENTITY_EXIST(vehs[mvf_mw_van_1].id)
		
					vehs[mvf_mw_jeep_1].id = CREATE_VEHICLE(mod_mw_jeep, << -2322.5149, 277.3885, 168.4671 >>, 310.9978)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_mw_jeep_1].id)
					//SET_VEHICLE_DOOR_OPEN(vehs[mvf_mw_jeep_1].id, SC_DOOR_REAR_LEFT, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_mw_jeep_1].id, FALSE)
					
					vehs[mvf_mw_van_1].id = CREATE_VEHICLE(mod_mw_van, << -2326.5037, 290.7654, 168.4671 >>, 5.9051)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_mw_van_1].id)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_mw_van_1].id, 0)
					SET_VEHICLE_MOD_KIT(vehs[mvf_mw_van_1].id, 0)
					SET_VEHICLE_WINDOW_TINT(vehs[mvf_mw_van_1].id, 1)
					//SET_VEHICLE_DOOR_OPEN(vehs[mvf_mw_van_1].id, SC_DOOR_REAR_RIGHT, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_mw_van_1].id, FALSE)
					
					Create_Mission_Ped(peds[mpf_pl_1_mw_1], mod_ped_mw, << -2323.1118, 280.3249, 168.4671 >>, 225.4840, "PL_1_MW_1", REL_MW, weap_cia_handgun, 1, 0, FALSE)
					Set_Ped_Combat_Params(peds[mpf_pl_1_mw_1], << -2307.38721, 269.81802, 168.60179 >>, 1.5, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE)
					SET_PED_TO_LOAD_COVER(peds[mpf_pl_1_mw_1].id, TRUE)
					
					// Van peds
					Create_Mission_Ped(peds[mpf_pl_1_mw_2], mod_ped_mw, << -2326.3679, 293.5865, 168.4667 >>, 225.8272, "PL_VAN_MW_2", REL_MW, weap_cia_handgun, 1, 0, FALSE)
					Set_Ped_Combat_Params(peds[mpf_pl_1_mw_2], GET_ENTITY_COORDS(vehs[mvf_mw_van_1].id), 5.0, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE)
					
					Create_Mission_Ped(peds[mpf_pl_1_mw_3], mod_ped_mw, << -2317.2009, 271.0336, 168.6018 >>, 204.3616, "PL_1_MW_3", REL_MW, weap_cia_handgun, 1, 0, FALSE)
					Set_Ped_Combat_Params(peds[mpf_pl_1_mw_3], << -2317.2009, 271.0336, 168.6018 >>, 1.5, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE)
					Create_Cover_Point_For_Ped(peds[mpf_pl_1_mw_3], << -2317.1096, 271.0735, 168.6018 >>, 207.7751, COVUSE_WALLTONEITHER, COVHEIGHT_TOOHIGH, COVARC_120)
					
					Create_Mission_Ped(peds[mpf_pl_1_mw_4], mod_ped_mw, (<<-2318.43604, 258.33582, 174.20219>>), 330, "PL_VAN_MW_4", REL_MW, weap_mw_rifle, 2, 0, FALSE)
					REMOVE_PED_DEFENSIVE_AREA(peds[mpf_pl_1_mw_4].id)
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_4].id, (<<-2311.73999, 253.59074, 169.63226>>), 2.5, TRUE)
					SET_PED_COMBAT_MOVEMENT(peds[mpf_pl_1_mw_4].id, CM_DEFENSIVE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_pl_1_mw_4].id, 200.0)
					
					Create_Mission_Ped(peds[mpf_pl_1_mw_5], mod_ped_mw, (<<-2304.39819, 295.17459, 173.90569>>), 203, "PL_VAN_MW_5", REL_MW, weap_mw_rifle, 1)
					Set_Ped_Combat_Params(peds[mpf_pl_1_mw_5], << -2307.19629, 290.05582, 173.61205 >>, 2.0, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, FALSE, 1.0, 0.0, TRUE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_pl_1_mw_5].id, 2.5)
					
					SET_PED_ACCURACY(peds[mpf_pl_1_mw_4].id, 1)
					SET_PED_ACCURACY(peds[mpf_pl_1_mw_5].id, 1)
					
					SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_pl_1_mw_1].id)
					SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_pl_1_mw_2].id)
					SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_pl_1_mw_3].id)
					SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_pl_1_mw_4].id)
					SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_pl_1_mw_5].id)
					sData.iStage++
					
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pl_1_mw_1].id, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_pl_1_mw_1].id, << -2322.2178, 279.8415, 168.4671 >>, -1, FALSE, 0, TRUE, TRUE)
			ENDIF
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pl_1_mw_3].id, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_pl_1_mw_3].id, << -2317.2009, 271.0336, 168.6018 >>, -1, FALSE, 0, TRUE, TRUE, peds[mpf_pl_1_mw_3].cov.id)
			ENDIF
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pl_1_mw_2].id, SCRIPT_TASK_AIM_GUN_AT_COORD)
				TASK_AIM_GUN_AT_COORD(peds[mpf_pl_1_mw_2].id, << -2297.8906, 264.5915, 170.2512 >>, -1, TRUE)
			ENDIF
		
		
			INT	iParkingLotApproach

			// Has come through the building down the steps
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2276.974121,274.887573,168.602112>>, <<-2288.430664,269.763885,177.664291>>, 5.000000)
				iParkingLotApproach = 1
			// Has come down the steps by the fountain
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2348.515869,228.353241,166.040985>>, <<-2272.677734,264.014008,173.991608>>, 18.312500)
				iParkingLotApproach = 2
			// Has come from the balcony down the stairs by the maze
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2318.887695,259.999664,173.602112>>, <<-2316.012695,253.149918,179.501999>>, 2.937500)
				iParkingLotApproach = 3
			// Has come from the balcony down the stairs by the parking lot
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2314.140625,308.140320,177.539291>>, <<-2308.618652,295.834442,184.463974>>, 4.187500)
				iParkingLotApproach = 4
			ENDIF
		
			// Multiple routes
			IF iParkingLotApproach != 0
			
				// Triggers the Jeep
				IF iParkingLotApproach = 3 OR iParkingLotApproach = 4
					TRIGGER_MISSION_EVENT(sEvents[mef_fight_parking_lot].sData)
//				ELIF sData.iStage >= 3
//					TRIGGER_MISSION_EVENT(sEvents[mef_parking_lot_jeep].sData, 5000)
				ENDIF
				
				// <<<<<<<<<FRONT LINE>>>>>>>>>>
				IF iParkingLotApproach = 1
				OR iParkingLotApproach = 2
					
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_1].id, << -2309.9324, 273.4493, 168.6018 >>, 3.0)
					TASK_COMBAT_PED(peds[mpf_pl_1_mw_1].id, MIKE_PED_ID())
					
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_3].id, << -2304.3999, 261.1549, 168.6018 >>, 3.0)
					TASK_COMBAT_PED(peds[mpf_pl_1_mw_3].id, MIKE_PED_ID())
				
				// Has come from the balcony down the stairs by the maze
				ELIF iParkingLotApproach = 3
					
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_1].id, <<-2322.053467,277.251678,168.932297>>, 5.0)
					TASK_COMBAT_PED(peds[mpf_pl_1_mw_1].id, MIKE_PED_ID())
									
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_3].id, <<-2307.807617,271.679871,168.601791>>, 3.0)
					TASK_COMBAT_PED(peds[mpf_pl_1_mw_3].id, MIKE_PED_ID())
				
				// Have come from the stairs by the parking lot
				ELIF iParkingLotApproach = 4
					
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_1].id, <<-2322.053467,277.251678,168.932297>>, 5.0)
					TASK_COMBAT_PED(peds[mpf_pl_1_mw_1].id, MIKE_PED_ID())
									
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_3].id, <<-2309.876953,275.908600,168.601791>>, 5.0)
					TASK_COMBAT_PED(peds[mpf_pl_1_mw_3].id, MIKE_PED_ID())
	
				ENDIF
				
				//<<<<<<< VAN GUYS >>>>>>>>>>
				IF iParkingLotApproach = 1
				OR iParkingLotApproach = 2

					// van peds
					IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_2].id)
						SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_2].id, <<-2317.113525,270.737335,168.601791>>, 3.500)
						TASK_COMBAT_PED(peds[mpf_pl_1_mw_2].id, MIKE_PED_ID())
					ENDIF
					
				ELIF iParkingLotApproach = 3
				OR iParkingLotApproach = 4
					// van peds
					IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_2].id)
						TASK_COMBAT_PED(peds[mpf_pl_1_mw_2].id, MIKE_PED_ID())
					ENDIF
				ENDIF
				
				sData.iStage++
			
			// none of the locates have been hit
			ELSE
				// Player is in a position to attack the peds from the balcony above
				BOOL bPlayerIsInArea
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2305.680664,264.703033,193.601166>>, <<-2329.129150,317.389557,168.467178>>, 37.000000)
					bPlayerIsInArea = TRUE
				ENDIF
			
				BOOL bTriggerReaction
				IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_1].id)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_pl_1_mw_1].id, MIKE_PED_ID())
					OR (bPlayerIsInArea AND HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_1].id, EVENT_SHOT_FIRED))
					OR HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_1].id, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_1].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						bTriggerReaction = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_3].id)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_pl_1_mw_3].id, MIKE_PED_ID())
					OR (bPlayerIsInArea AND HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_3].id, EVENT_SHOT_FIRED))
					OR HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_3].id, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_3].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						bTriggerReaction = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_2].id)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_pl_1_mw_2].id, MIKE_PED_ID())
					OR (bPlayerIsInArea AND HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_2].id, EVENT_SHOT_FIRED))
					OR HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_2].id, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(peds[mpf_pl_1_mw_2].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						bTriggerReaction = TRUE
					ENDIF
				ENDIF
				
				IF bTriggerReaction
				OR mission_stage = enum_to_int(STAGE_VEHICLE_ESCAPE)
					IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_1].id)
						SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_1].id, <<-2322.053467,277.251678,168.932297>>, 5.0)
						TASK_COMBAT_PED(peds[mpf_pl_1_mw_1].id, MIKE_PED_ID())
					ENDIF
					IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_3].id)
						SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_pl_1_mw_3].id, <<-2315.736816,267.926147,167.226791>>, 5.0)
						TASK_COMBAT_PED(peds[mpf_pl_1_mw_3].id, MIKE_PED_ID())
					ENDIF
					// van peds
					IF NOT IS_PED_INJURED(peds[mpf_pl_1_mw_2].id)
						TASK_COMBAT_PED(peds[mpf_pl_1_mw_2].id, MIKE_PED_ID())
					ENDIF
					
					sData.iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_PED_INJURED(peds[mpf_pl_1_mw_1].id)
			AND IS_PED_INJURED(peds[mpf_pl_1_mw_3].id)
			AND IS_PED_INJURED(peds[mpf_pl_1_mw_2].id)
				END_MISSION_EVENT(sData)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC EVENT_parking_lot_jeep(MISSION_EVENT_DATA &sData)
	SWITCH sData.iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_ped_mw)
			Load_Asset_Model(sAssetData, mod_mw_jeep)
			Load_Asset_Recording(sAssetData, rec_mw_jeep_2, str_carrecs)
			sData.iStage++
		BREAK
		CASE 2
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_mw_jeep_2, str_carrecs)
			AND HAS_MODEL_LOADED(mod_ped_mw)
			AND HAS_MODEL_LOADED(mod_mw_jeep)
			
				vehs[mvf_mw_jeep_2].id = CREATE_VEHICLE(mod_mw_jeep, << -2327.4641, 379.8782, 173.4668 >>, 115.3375)
				SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_mw_jeep_2].id, FALSE)
				Create_Mission_Ped_In_Vehicle(peds[mpf_mw_jeep_2_1], mod_ped_mw, vehs[mvf_mw_jeep_2], VS_DRIVER, 		"MW_JEEP_2_0", REL_MW, weap_cia_handgun, 2, 0, FALSE)
				Create_Mission_Ped_In_Vehicle(peds[mpf_mw_jeep_2_2], mod_ped_mw, vehs[mvf_mw_jeep_2], VS_FRONT_RIGHT, 	"MW_JEEP_2_1", REL_MW, weap_cia_handgun, 2, 0, FALSE)
				
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_jeep_2].id, rec_mw_jeep_2, str_carrecs)
				SAFE_BLIP_VEHICLE(vehs[mvf_mw_jeep_2].blip, vehs[mvf_mw_jeep_2].id, TRUE)
				
				SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_mw_jeep_2_1].id)
				SET_PED_TO_FIRE_MANY_BLANKS(peds[mpf_mw_jeep_2_2].id)
				
				sData.iStage++
			ENDIF
		BREAK
		CASE 3
			// driver killed let the car veer whereever
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_jeep_2].id)
			AND IS_PED_INJURED(peds[mpf_mw_jeep_2_1].id)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_jeep_2].id)
			ENDIF
			
			IF ((IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_jeep_2].id) AND GET_TIME_POSITION_IN_RECORDING(vehs[mvf_mw_jeep_2].id) > 2500)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_jeep_2].id)))
			AND IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_mw_jeep_2].id)
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mw_jeep_2].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mw_jeep_2].id)
				ENDIF
			
				IF NOT IS_PED_INJURED(peds[mpf_mw_jeep_2_1].id)
					Set_Ped_Combat_Params(peds[mpf_mw_jeep_2_1], (<<-2322.73486, 279.51361, 168.46666>>), 5.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(peds[mpf_mw_jeep_2_1].id, CA_USE_VEHICLE, false)
					TASK_COMBAT_PED(peds[mpf_mw_jeep_2_1].id, MIKE_PED_ID())
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_mw_jeep_2_2].id)
					Set_Ped_Combat_Params(peds[mpf_mw_jeep_2_2], (<<-2309.56372, 274.71063, 168.58304>>), 5.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(peds[mpf_mw_jeep_2_2].id, CA_USE_VEHICLE, false)
					TASK_COMBAT_PED(peds[mpf_mw_jeep_2_2].id, MIKE_PED_ID())
				ENDIF
				
				SAFE_REMOVE_BLIP(vehs[mvf_mw_jeep_2].blip)
				sData.iStage++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC







PROC EVENT_Add_Mission_Events()

	// [AI] 	= ... 
	// [SO] 	= shoot out
	// [PO] 	= pop out
	// [VEH] 	= vehicle controller
	// [IG_x]	= In-game animated scene controller
	
	ADD_NEW_MISSION_EVENT(sEvents[mef_manage_radar],						&event_manage_radar,	 					"Fake interior")
	ADD_NEW_MISSION_EVENT(sEvents[mef_manage_ped_spatial_data], 			&event_manage_ped_spatial_data, 			"Spatial Data")
	ADD_NEW_MISSION_EVENT(sEvents[mef_fight_parking_lot], 					&event_fight_parking_lot, 					"[SO] Parking lot")
	ADD_NEW_MISSION_EVENT(sEvents[mef_parking_lot_jeep], 					&event_parking_lot_jeep, 					"[VEH] Parking lot")
	ADD_NEW_MISSION_EVENT(sEvents[mef_escape_vehicles], 					&event_escape_vehicles, 					"Spawn esc vehs")
	ADD_NEW_MISSION_EVENT(sEvents[mef_heli_2], 								&event_heli_2, 								"[VEH] MW Heli 2")
	ADD_NEW_MISSION_EVENT(sEvents[mef_heli_3], 								&event_heli_3, 								"[VEH] MW Heli 3")
	
ENDPROC


	
/*
								 _______  _   ______  ______  _  _______  _______    _______  _       _______  _  _  _ 
								(_______)| | / _____)/ _____)| |(_______)(_______)  (_______)(_)     (_______)(_)(_)(_)
								 _  _  _ | |( (____ ( (____  | | _     _  _     _    _____    _       _     _  _  _  _ 
								| ||_|| || | \____ \ \____ \ | || |   | || |   | |  |  ___)  | |     | |   | || || || |
								| |   | || | _____) )_____) )| || |___| || |   | |  | |      | |_____| |___| || || || |
								|_|   |_||_|(______/(______/ |_| \_____/ |_|   |_|  |_|      |_______)\_____/  \_____/ 
*/


FUNC MISSION_STAGE_FLAG GET_STAGE_FROM_CHECKPOINT(INT iCheckpoint)
	SWITCH iCheckpoint
		DEFAULT 										FALLTHRU
		CASE 0
			RETURN STAGE_WALK_TO_DAVE
		BREAK
		
		CASE 1
			RETURN STAGE_FIRST_AREA
		BREAK
		
		CASE 2
			RETURN STAGE_TREVOR_SHOOTS_HELICOPTER
		BREAK
		
		CASE 3
			RETURN STAGE_TREVOR_SAVES_DAVE
		BREAK
		
		CASE 4
			RETURN STAGE_DAVE_BY_FOUNTAIN
		BREAK
		
		CASE 5
			RETURN STAGE_DAVE_ESCAPES
		BREAK
		
		CASE 6
			RETURN STAGE_ESCAPE_MUSEUM
		BREAK
		
		CASE 7
			RETURN STAGE_VEHICLE_ESCAPE
		BREAK
		
		CASE 8
			RETURN STAGE_END_CUTSCENE
		BREAK
		
		CASE 9
			RETURN STAGE_MISSION_PASSED
		BREAK
	ENDSWITCH
	
	RETURN STAGE_FIRST_AREA
ENDFUNC


//PURPOSE: Halts script progress until is ready
PROC IS_SKIP_OK_TO_FADE_IN()
	BOOL bFinished = FALSE

	WHILE NOT bFinished
		WAIT(0)
		
		bFinished = TRUE
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
		AND NOT IS_NEW_LOAD_SCENE_LOADED()
			bFinished = FALSE
		ENDIF
	
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) 
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_MISSION, "Player Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MISSION, "Player Ped: Safe for fade in")
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(TREV_PED_ID())
		AND NOT IS_PED_INJURED(TREV_PED_ID())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(TREV_PED_ID())
				CDEBUG1LN(DEBUG_MISSION, "Trevor Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MISSION, "Trevor Ped: Safe for fade in")
			ENDIF
		ENDIF
	ENDWHILE

	CDEBUG1LN(DEBUG_MISSION, "Skip safe to fade in now")
	Unload_Asset_NewLoadScene(sAssetData)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
ENDPROC

// Manages the swap between stages
PROC Mission_Stage_Management()
	SWITCH stageSwitch
		CASE STAGESWITCH_REQUESTED 
			PRINTLN("[StageManagement] mission_stage switch requested from mission_stage:", mission_stage, " to mission_stage:", requestedStage)
			stageSwitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			PRINTLN("[StageManagement] Exiting mission_stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			PRINTLN("[StageManagement] Entered mission_stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 2500
//				PRINTLN("[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

BOOL bVehStatsGrabbed = FALSE
PROC Mission_Checks()
	INT i
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		i_dialogue_timer = -1
	ELIF i_dialogue_timer = -1
		i_dialogue_timer = GET_GAME_TIMER()
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT bVehStatsGrabbed
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTemp, MIC3_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehTemp)
			bVehStatsGrabbed = TRUE
		ENDIF
	ELSE
		IF bVehStatsGrabbed
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, MIC3_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
			bVehStatsGrabbed = FALSE
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(peds) i
		IF DOES_ENTITY_EXIST(peds[i].id)
		
			// process stealth blips
			IF GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) != REL_FRIEND
			AND peds[i].id != PLAYER_PED_ID()
			AND peds[i].id != MIKE_PED_ID()
			AND peds[i].id != TREV_PED_ID()
			
				IF NOT IS_PED_INJURED(peds[i].id)
				AND ePinnedFromHere != MUS_NO_AREA
				AND ePinnedFromHere = peds[i].eSectionPedIsIn
					UPDATE_AI_PED_BLIP(peds[i].id, peds[i].stealthBlip, -1, NULL, TRUE)
				ELSE
					UPDATE_AI_PED_BLIP(peds[i].id, peds[i].stealthBlip)
				ENDIF
			ENDIF
		
			IF IS_PED_INJURED(peds[i].id)
				IF peds[i].id = MIKE_PED_ID()
					Mission_Failed(mff_michael_dead)
				ELIF peds[i].id = TREV_PED_ID()
					Mission_Failed(mff_trevor_dead)
				ELSE
					DEAL_WITH_DEAD_PED(peds[i])
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(peds[i].id)
					IF NOT DOES_BLIP_EXIST(peds[i].blip)
						//@RJP - B*1427777 - don't blip other player during STAGE_VEHICLE_ESCAPE
						IF NOT (mission_stage = ENUM_TO_INT(STAGE_VEHICLE_ESCAPE))
							IF i = enum_to_int(mpf_mike)	
								IF PLAYER_PED_ID() != peds[mpf_mike].id
									SAFE_BLIP_PED(peds[mpf_mike].blip, peds[mpf_mike].id, FALSE)
								ENDIF
							ELIF i = enum_to_int(mpf_trev)
								IF bCanBlipTrevor
								AND PLAYER_PED_ID() != peds[mpf_trev].id
									SAFE_BLIP_PED(peds[mpf_trev].blip, peds[mpf_trev].id, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			peds[i].bCanTrevorSeeThisPed = FALSE
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(m_pedAllyDave)
		IF IS_ENTITY_DEAD(m_pedAllyDave)
			Mission_Failed(mff_dave_dead)
		ENDIF
	ENDIF

	IF (mission_stage = ENUM_TO_INT(STAGE_FIRST_AREA)
	OR mission_stage = ENUM_TO_INT(STAGE_DAVE_BY_FOUNTAIN))
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbAbandonFailFirstArea1)
		OR IS_PLAYER_IN_TRIGGER_BOX(m_tbAbandonFailFirstArea2)
			Mission_Failed(mff_dave_abdn)
		ENDIF
	ENDIF
	
	IF mission_stage = ENUM_TO_INT(STAGE_FIRST_AREA)
		IF IS_PLAYER_IN_TRIGGER_BOX(m_tbAbandonFailOnStairs)
			Mission_Failed(mff_dave_abdn)
		ENDIF
	ENDIF
	
	IF (mission_stage = ENUM_TO_INT(STAGE_WALK_TO_DAVE))
		FAIL_MISSION_IF_DAVE_ABANDONED(250, 240)
	ELIF (mission_stage = ENUM_TO_INT(STAGE_FIRST_AREA))
		FAIL_MISSION_IF_DAVE_ABANDONED(85.0, 75.0)
	ELIF (mission_stage = ENUM_TO_INT(STAGE_DAVE_BY_FOUNTAIN)
	OR 	mission_stage = ENUM_TO_INT(STAGE_DAVE_ESCAPES)
	OR 	mission_stage = ENUM_TO_INT(STAGE_TREVOR_SAVES_DAVE)) // fix for bug 1955461
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			FAIL_MISSION_IF_DAVE_ABANDONED(100.0, 90.0)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			FAIL_MISSION_IF_DAVE_ABANDONED(85.0, 75.0)	
		ENDIF
	ELIF mission_stage = ENUM_TO_INT(STAGE_ESCAPE_MUSEUM)
		IF eDaveFleeState < DAVE_FLEE_TASK_GTFO
			FAIL_MISSION_IF_DAVE_ABANDONED(150.0, 140.0)
		ENDIF
	ENDIF
	
	IF m_bCheckFailureKillVolume
		
		IF ePlayerSwitchState != PLAYER_SWITCH_SETUP
		AND ePlayerSwitchState != PLAYER_SWITCH_COMPLETE
			IF IS_ENTITY_IN_TRIGGER_BOX(m_tbFailKillVolume, MIKE_PED_ID())
				CPRINTLN(DEBUG_MISSION, "Michael in the death volume, should die now??!?")
				SET_ENTITY_HEALTH(MIKE_PED_ID(), 0)
			ENDIF
		ENDIF
	
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			// dodgy fix for bug 1859843
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-2194.791992,237.496964,183.601883>>, <<-2196.416504,236.784653,186.119705>>, 13.375000 )
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_JUMP )
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(vehs) i
		IF DOES_ENTITY_EXIST(vehs[i].id)
		AND NOT IS_VEHICLE_DRIVEABLE(vehs[i].id)
		AND IS_ENTITY_DEAD(vehs[i].id)
			SAFE_REMOVE_BLIP(vehs[i].blip)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
			INFORM_MISSION_STATS_OF_INCREMENT(MIC3_VEHICLES_DESTROYED)
		ENDIF
	ENDREPEAT
ENDPROC


//PURPOSE: Processes any change from one stage to another that is not part of the mission flow.
PROC Mission_Stage_Skip()
	iSkipToStage = iSkipToStage
	
	// a skip has been made
	IF bDoSkip = TRUE
		
		// Begin the skip if the switching is idle
		IF stageSwitch = STAGESWITCH_IDLE
			
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(ROUND(DEFAULT_FADE_TIME * f_current_time_scale))
				ENDIF
			ELSE
				Mission_Set_Stage(msfStageSkip)
			ENDIF

		// needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_PLAYER_CONTROL(PLAYER_ID(), true)
			ENDIF
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			Mission_Reset_Cleanup()
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
			
			WEAPON_TYPE weapBest

			// Asset loading
			Start_Skip_Streaming(sAssetData)
			
			// Assets that always need to be loaded
			Load_Asset_Additional_Text(sAssetData, "MIC3", MISSION_TEXT_SLOT)
			
			DESTROY_ALL_MISSION_PEDS()
			// Stage specific assets
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
			
				CASE STAGE_WALK_TO_DAVE
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2291.8391, 367.5178, 173.6017>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 203)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2291.8286, 367.6647, 173.6017>>, 15.0)
					ENDIF
					Load_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_DAVE))
					Load_Asset_Model(sAssetData, P_CS_NEWSPAPER_S)
					Load_Asset_Model(sAssetData, FROGGER)
					Load_Asset_AnimDict(sAssetData, "missmic3")
				BREAK
				
				CASE STAGE_MEETING_CUTSCENE
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << -2156.9224, 234.4068, 183.6015 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 113.7745)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, << -2156.9224, 234.4068, 183.6015 >>, 113.7745)
					ENDIF
					Load_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_DAVE))
					Load_Asset_Model(sAssetData, P_CS_NEWSPAPER_S)
					Load_Asset_Model(sAssetData, FROGGER)
					Load_Asset_AnimDict(sAssetData, "missmic3")
				BREAK
				
				CASE STAGE_FIRST_AREA
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), csMichaelMIC3_INTCover.pos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 21.5258)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, << -2156.9224, 234.4068, 183.6015 >>, 15.0)
					ENDIF
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
					
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_ped_fib)
					Load_Asset_Model(sAssetData, mod_ped_cia)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Model(sAssetData, IG_PAPER)
					Load_Asset_Recording(sAssetData, rec_crash_heli,  str_carrecs)
					Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_fib_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_handgun)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
					Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
				BREAK
								
				CASE STAGE_TREVOR_SHOOTS_HELICOPTER
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2221.4248, 256.6886, 183.6041>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 109.6962)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2221.4248, 256.6886, 183.6041>>, 15.0)
					ENDIF
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
					
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_heli_crashed)
					Load_Asset_Model(sAssetData, mod_ped_fib)
					Load_Asset_Model(sAssetData, mod_ped_cia)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_fib_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_handgun)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
					Load_Asset_AnimDict(sAssetData, m_strAnimDict_HeliLeadout)
					Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
				BREAK
				
				CASE STAGE_TREVOR_SAVES_DAVE
					//SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2221.4248, 256.6886, 183.6041>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 109.6962)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2221.31421, 251.09691, 183.60408>>, 15.0)
					ENDIF
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
					
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_heli_crashed)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Model(sAssetData, mod_ped_fib)
					Load_Asset_Model(sAssetData, mod_ped_cia)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_fib_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_handgun)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
					Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
				BREAK
				
				CASE STAGE_DAVE_BY_FOUNTAIN
					SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), (<<-2219.2981, 247.3061, 183.6041>>))
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 40.5077)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2231.9397, 277.4730, 183.6041>>, 15.0)
					ENDIF
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
					
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_heli_crashed)
					Load_Asset_Model(sAssetData, mod_ped_fib)
					Load_Asset_Model(sAssetData, mod_ped_cia)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_fib_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_handgun)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
					Load_Asset_AnimDict(sAssetData, m_strAnimDict_IG1)
					Load_Asset_AnimDict(sAssetData, m_strAnimDict_IG6)
					Load_Asset_AnimDict(sAssetData, m_strAnimDict_IG7)
					Load_Asset_Recording(sAssetData, rec_mw_heli_2, str_carrecs)
					Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
				BREAK
				
				CASE STAGE_DAVE_ESCAPES
					SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2249.85522, 270.12097, 173.60196>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 24.2243)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2231.9397, 277.4730, 183.6041>>, 15.0)
					ENDIF
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
					
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_heli_crashed)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Model(sAssetData, mod_ped_fib)
					Load_Asset_Model(sAssetData, mod_ped_cia)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_fib_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_rifle)
					Load_Asset_Weapon_Asset(sAssetData, weap_cia_handgun)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
					Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
				BREAK
				
				CASE STAGE_ESCAPE_MUSEUM
					SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2249.85522, 270.12097, 173.60196>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 24.2243)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2231.9397, 277.4730, 183.6041>>, 15.0)
					ENDIF
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
					
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_heli_crashed)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
					Load_Asset_Model(sAssetData, CARBONIZZARE)
					Load_Asset_Model(sAssetData, DOMINATOR)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
					Load_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
					Load_Asset_Recording(sAssetData, rec_otherp_esc, str_carrecs)
					Load_Asset_Waypoint(sAssetData, m_strDaveWaypointRec)
					Load_Asset_Waypoint(sAssetData, m_strOtherPlayerWaypointRec)
					Load_Asset_Audiobank(sAssetData, str_AudioBankFIBShootout)
				BREAK
			
				CASE STAGE_VEHICLE_ESCAPE
					SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2295.5474, 264.1368, 168.6018>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 25.3686)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-2295.5474, 264.1368, 168.6018>>, 15.0)
					ENDIF
					
					weapBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
				
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_heli_crashed)
					Load_Asset_Model(sAssetData, mod_heli_mw)
					Load_Asset_Model(sAssetData, mod_ped_mw)
					Load_Asset_Model(sAssetData, mod_trev_heli)
					Load_Asset_Model(sAssetData, mod_ped_dave)
					Load_Asset_Model(sAssetData, DOMINATOR)
					Load_Asset_Model(sAssetData, CARBONIZZARE)
					Load_Asset_Model(sAssetData, VACCA)
					Load_Asset_Waypoint(sAssetData, m_strDaveWaypointRec)
					Load_Asset_Waypoint(sAssetData, m_strOtherPlayerWaypointRec)
					Load_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
					Load_Asset_Recording(sAssetData, rec_otherp_esc, str_carrecs)
					Load_Asset_Weapon_Asset(sAssetData, weapBest)
				BREAK
				
				CASE STAGE_END_CUTSCENE
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1473.1211, -396.4522, 37.1653>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 307.9418)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
//						Load_Asset_NewLoadScene_Sphere(sAssetData, <<-1473.1211, -396.4522, 37.1653>>, 15.0)
					ENDIF
				BREAK
				
				CASE STAGE_MISSION_PASSED
				BREAK
			ENDSWITCH
			
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)
			ENDWHILE
			
			STOP_AUDIO_SCENES()
			TRIGGER_MISSION_EVENT(sEvents[mef_manage_radar].sData)	// Trigger fake interior management again
			TRIGGER_MISSION_EVENT(sEvents[mef_manage_ped_spatial_data].sData)
			Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
			
			SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
			
			Clear_And_Block_Area(TRUE)
			weapBest = GET_BEST_PED_WEAPON(MIKE_PED_ID())
			SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weapBest)
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		
			// Set up the mission for that current stage
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE STAGE_WALK_TO_DAVE
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_WALK_TO_DAVE")
					m_bCheckFailureKillVolume = FALSE
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, 0, TRUE)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
					
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_L), 0.0, TRUE, FALSE)
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_R), 0.0, TRUE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
					
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_L), 0.0, TRUE, FALSE)
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_R), 0.0, TRUE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
					
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_L), DOORSTATE_UNLOCKED, TRUE, TRUE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTL_R), DOORSTATE_UNLOCKED, TRUE, TRUE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_L), DOORSTATE_UNLOCKED, TRUE, TRUE)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_KORTZ_FRONTR_R), DOORSTATE_UNLOCKED, TRUE, TRUE)
					eWalkToDaveState = WALK_TO_DAVE_CREATE_DAVE
				BREAK
				
				CASE STAGE_MEETING_CUTSCENE
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_MEETING_CUTSCENE")
					
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MIC_3_INT", CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5
																   | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 
																   | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13 
																   | CS_SECTION_14 | CS_SECTION_15 | CS_SECTION_16 | CS_SECTION_17)						
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (<<-2163.48901, 233.88806, 183.60187>>), 115.12, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_FIRST_AREA")
						WAIT(0)
					ENDWHILE
					objs[mof_daves_paper].id = CREATE_OBJECT(P_CS_NEWSPAPER_S, << -2150.729, 232.467, 183.606 >>)
					SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_NEWSPAPER_S)
				BREAK
				
				CASE STAGE_FIRST_AREA
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_FIRST_AREA")
					CREATE_TREVOR_HELICOPTER()
					m_bCheckFailureKillVolume = TRUE
					SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csMichaelMIC3_INTCover.pos, -1, TRUE, 0, TRUE, FALSE, csMichaelMIC3_INTCover.id, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE_PED_ID())
					
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (<<-2163.48901, 233.88806, 183.60187>>), 115.12, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_FIRST_AREA")
						WAIT(0)
					ENDWHILE
					
					SETUP_DAVE_ATTRIBUTES()
					FORCE_PED_MOTION_STATE(m_pedAllyDave, MS_ON_FOOT_SPRINT, FALSE)
					SAFE_OPEN_SEQUENCE()
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_STRAIGHT_TO_COORD(NULL, (<<-2205.56421, 215.19299, 183.60187>>), PEDMOVE_SPRINT, -1)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, m_flagDaveCoverPositions[1].pos, PEDMOVE_SPRINT)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
					SAFE_PERFORM_SEQUENCE(m_pedAllyDave)
					WAIT(2000)
					TRIGGER_MUSIC_EVENT("MIC3_FIGHT_RESTART")
				BREAK
			
				CASE STAGE_TREVOR_SHOOTS_HELICOPTER
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_TREVOR_SHOOTS_HELICOPTER")
					CREATE_TREVOR_HELICOPTER()
					CREATE_CRASHED_HELI()
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtMikeGunEnterCutscene)
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_SNIPERRIFLE, 10, TRUE, TRUE, FALSE)
					RESTART_TIMER_NOW(tmrTrevorConversation)
					vehs[mvf_mw_heli_1].id = CREATE_VEHICLE(mod_heli_mw, << -2149.5046, 240.5233, 187.5145 >>, 201.0944)
					Create_Mission_Ped_In_Vehicle(peds[mpf_mw_heli_1_pilot], mod_ped_mw, vehs[mvf_mw_heli_1], VS_DRIVER, "MW_HELI1_1", REL_MW)
					
					IF IS_ENTITY_OK(peds[mpf_mw_heli_1_pilot].id) AND IS_ENTITY_OK(vehs[mvf_mw_heli_1].id)
						CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Helicopter and pilot exist...ok......")
						SET_PED_COMBAT_ATTRIBUTES(peds[mpf_mw_heli_1_pilot].id, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
						SET_PED_TARGET_LOSS_RESPONSE(peds[mpf_mw_heli_1_pilot].id, TLR_NEVER_LOSE_TARGET)
						SET_PED_COMBAT_RANGE(peds[mpf_mw_heli_1_pilot].id, CR_FAR)
						SET_PED_SHOOT_RATE(peds[mpf_mw_heli_1_pilot].id, 100)
						SET_PED_FIRING_PATTERN(peds[mpf_mw_heli_1_pilot].id, FIRING_PATTERN_FULL_AUTO)
						SET_PED_ACCURACY(peds[mpf_mw_heli_1_pilot].id, 100)
						SET_HELI_BLADES_FULL_SPEED(vehs[mvf_mw_heli_1].id)
						SET_PED_CAN_BE_TARGETTED(peds[mpf_mw_heli_1_pilot].id, FALSE)
						SET_ENTITY_INVINCIBLE(peds[mpf_mw_heli_1_pilot].id, TRUE)
						SAFE_BLIP_VEHICLE(vehs[mvf_mw_heli_1].blip, vehs[mvf_mw_heli_1].id, TRUE)
						SET_VEHICLE_RADIO_ENABLED(vehs[mvf_mw_heli_1].id, FALSE)
						peds[mpf_mw_heli_1_pilot].iGenericFlag = 1
						
						SET_ENTITY_PROOFS(vehs[mvf_mw_heli_1].id, TRUE, TRUE, TRUE, FALSE, FALSE)
						SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_mw_heli_1].id, FALSE)
						SET_VEHICLE_CAN_LEAK_PETROL(vehs[mvf_mw_heli_1].id, FALSE)
						SET_VEHICLE_CAN_LEAK_OIL(vehs[mvf_mw_heli_1].id, FALSE)
						SET_VEHICLE_ENGINE_ON(vehs[mvf_mw_heli_1].id, TRUE, TRUE)
						SET_ENTITY_COORDS(vehs[mvf_mw_heli_1].id, (<<-2241.1, 263.6, 190.3>>))
						SET_ENTITY_HEADING(vehs[mvf_mw_heli_1].id, 235.00)
					ENDIF
					
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (m_flagDaveCoverPositions[2].pos), m_flagDaveCoverPositions[2].dir, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_TREVOR_SHOOTS_HELICOPTER")
						WAIT(0)
					ENDWHILE
					
					IF IS_ENTITY_OK(m_pedAllyDave)
						SETUP_DAVE_ATTRIBUTES()
						REMOVE_PED_DEFENSIVE_AREA(m_pedAllyDave)
						SET_PED_SPHERE_DEFENSIVE_AREA(m_pedAllyDave, m_flagDaveCoverPositions[2].pos, 2.0, TRUE)
						TASK_SEEK_COVER_TO_COORDS(m_pedAllyDave, m_flagDaveCoverPositions[2].pos, (<<-2231.01147, 263.63266, 173.60196>>), -1)
					ENDIF
					
					Prep_Hotswap(TRUE, TRUE, FALSE)
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Michael setup 8")
					SETUP_MICHAEL_FOR_HELICOPTER_SCENE()
					
					TRIGGER_MUSIC_EVENT("MIC3_TREV_HELI_RESTART")
					IF g_replay.iReplayInt[GENERIC_MISSION_FLAGS] = iCONST_HELI_CUT_WATCHED
						Prep_Hotswap(TRUE, TRUE, FALSE)
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR) 
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE)
						Prep_Hotswap(TRUE, TRUE, FALSE)
						peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
						peds[mpf_trev].id = PLAYER_PED_ID()
						IF IS_ENTITY_OK(PLAYER_PED_ID())
							SET_ENTITY_COORDS( PLAYER_PED_ID(), m_flagTrevorPosition.pos )
							SET_ENTITY_HEADING( PLAYER_PED_ID(), m_flagTrevorPosition.dir )
							SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
							SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
							DISABLE_CELLPHONE(TRUE)
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
							SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
							DISPLAY_SNIPER_SCOPE_THIS_FRAME()
							GIVE_WEAPON_TO_PED(peds[mpf_trev].id, WEAPONTYPE_SNIPERRIFLE, 10, TRUE)
							START_AUDIO_SCENE("MI_3_TREVOR_SHOOTS_HELICOPTER")
						ENDIF
						eTrevorShootsHeliState = TREVOR_SHOOTS_HELI_IN_GAME
					ELSE
						eTrevorShootsHeliState = TREVOR_SHOOTS_HELI_INIT
					ENDIF
					m_bCheckFailureKillVolume = TRUE
					m_bDoStairFires = TRUE
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				BREAK
				
				CASE STAGE_TREVOR_SAVES_DAVE
					CREATE_TREVOR_HELICOPTER()
					CREATE_CRASHED_HELI()
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_TREVOR_SAVES_DAVE")
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_SNIPERRIFLE, 10, TRUE, TRUE, FALSE)
					Prep_Hotswap(TRUE, TRUE, FALSE)
					m_bCheckFailureKillVolume = TRUE
					CREATE_TREVOR_SAVES_DAVE_FIB()
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR) 
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE)
					GIVE_WEAPON_TO_PED(peds[mpf_trev].id, WEAPONTYPE_SNIPERRIFLE, 10, TRUE)
					m_bDoStairFires = TRUE
					Prep_Hotswap(TRUE, TRUE, FALSE)
					
					// TODO: FILL THIS IN!!!!
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (<<-2219.03345, 246.49496, 173.60182>>), 20, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_TREVOR_SAVES_DAVE")
						WAIT(0)
					ENDWHILE
					SETUP_DAVE_ATTRIBUTES()
					TRIGGER_MUSIC_EVENT("MIC3_MT_FIGHT_RESTART")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				BREAK
				
				CASE STAGE_DAVE_BY_FOUNTAIN
					CREATE_TREVOR_HELICOPTER()
					CREATE_CRASHED_HELI()
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_DAVE_BY_FOUNTAIN")
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_SNIPERRIFLE, 10, TRUE, TRUE, FALSE)
					Prep_Hotswap(TRUE, TRUE, FALSE)
					m_bCheckFailureKillVolume = TRUE
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR) 
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE)
					GIVE_WEAPON_TO_PED(peds[mpf_trev].id, WEAPONTYPE_SNIPERRIFLE, 10, TRUE)
					m_bDoStairFires = TRUE
					Prep_Hotswap(TRUE, TRUE, FALSE)
					peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (<<-2219.32349, 247.25533, 173.60182>>), 20, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_DAVE_BY_FOUNTAIN")
						WAIT(0)
					ENDWHILE
					
					IF IS_ENTITY_OK(MIKE_PED_ID())
						FREEZE_ENTITY_POSITION(MIKE_PED_ID(), FALSE)
					ENDIF
					SETUP_DAVE_ATTRIBUTES()
//					CREATE_LAST_STRAGGLER_FOR_DAVE_TO_SHOOT()
					ePlayerSwitchState = PLAYER_SWITCH_SETUP
					TRIGGER_MUSIC_EVENT("MIC3_FOUNTAIN_RESTART")
					
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(MIKE_PED_ID(), "MI_3_MICHAEL_GROUP")
					STOP_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
					START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				BREAK
				
				CASE STAGE_DAVE_ESCAPES
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_DAVE_ESCAPES")
					CREATE_TREVOR_HELICOPTER()
					CREATE_CRASHED_HELI()
					m_bDoStairFires = TRUE
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_SNIPERRIFLE, 10, TRUE, TRUE, TRUE)
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (m_flagDaveCoverPositions[3].pos), m_flagDaveCoverPositions[3].dir, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_DAVE_ESCAPES")
						WAIT(0)
					ENDWHILE
					
					TASK_PUT_PED_DIRECTLY_INTO_COVER(m_pedAllyDave, csDaveFountainCover.pos, -1, TRUE, 0, FALSE, FALSE, csDaveFountainCover.id)
					
					IF g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
						Prep_Hotswap(FALSE, TRUE, FALSE)
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE)
						Prep_Hotswap(FALSE, TRUE, FALSE)
						peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
						peds[mpf_trev].id = PLAYER_PED_ID()
						
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(MIKE_PED_ID(), "MI_3_MICHAEL_GROUP")
						START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
						
						IF IS_ENTITY_OK(MIKE_PED_ID())
							SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
							SET_ENTITY_PROOFS(MIKE_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
							SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(MIKE_PED_ID(), m_tbDefensiveAreaAtFountain)
						ENDIF
						
						//	CASE #1 - We were Trevor at the start of this stage, and we were near Dave
						//		Trevor gets put into player cover by fountain
						//		Michael gets put into other player cover by fountain
						//		WORKED!
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							SET_ENTITY_COORDS(TREV_PED_ID(), csPlayerFountainCover.pos)
							SET_ENTITY_HEADING(TREV_PED_ID(), 113.2243)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), csPlayerFountainCover.pos, -1, FALSE, 0, TRUE, TRUE, csPlayerFountainCover.id)
							
							IF IS_ENTITY_OK(MIKE_PED_ID())
								SET_ENTITY_COORDS(MIKE_PED_ID(), <<-2246.66187, 271.64197, 173.60196>>)
								SET_ENTITY_HEADING(MIKE_PED_ID(), 24.2243)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csOtherPlayerFountainCover.pos, -1, TRUE, 0, FALSE, FALSE, csOtherPlayerFountainCover.id)
							ENDIF
							
						//	CASE #2 - We were Trevor at the start of this stage, but we were nowhere near Dave
						//		Trevor stays on the roof, don't do anything
						//		Michael gets put into player cover by fountain
						ELIF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_ON_ROOF
							IF IS_ENTITY_OK(MIKE_PED_ID())
								SET_ENTITY_COORDS(MIKE_PED_ID(), csPlayerFountainCover.pos)
								SET_ENTITY_HEADING(MIKE_PED_ID(), 24.2243)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csPlayerFountainCover.pos, -1, TRUE, 0, FALSE, FALSE, csPlayerFountainCover.id)
							ENDIF
							CREATE_IG_8_ENEMIES_FOR_TREVOR_DURING_COURTYARD_FIGHT()
						ENDIF
					ELSE
						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = PLAYER_PED_ID()
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trev].id
						peds[mpf_mike].id = PLAYER_PED_ID()
						
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(TREV_PED_ID(), "MI_3_TREVOR_GROUP")
						START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
						
						IF IS_ENTITY_OK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							CPRINTLN(DEBUG_MISSION, "STAGE_DAVE_ESCAPES restart - sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] is valid")
						ENDIF
						IF IS_ENTITY_OK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							CPRINTLN(DEBUG_MISSION, "STAGE_DAVE_ESCAPES restart - sSelectorPeds.pedID[SELECTOR_PED_TREVOR] is valid")
						ENDIF
						
						
						//	We were Michael at the start of the stage - No matter what, we're in player cover
						IF IS_ENTITY_OK(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), csPlayerFountainCover.pos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 113.2243)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), csPlayerFountainCover.pos, -1, TRUE, 0, TRUE, TRUE, csPlayerFountainCover.id)
						ENDIF
						
						//	If Trevor was at Dave, we put him in the other cover location.
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							IF IS_ENTITY_OK(TREV_PED_ID())
								SET_ENTITY_COORDS(TREV_PED_ID(), csOtherPlayerFountainCover.pos)
								SET_ENTITY_HEADING(TREV_PED_ID(), 24.2243)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(TREV_PED_ID(), csOtherPlayerFountainCover.pos, -1, TRUE, 0, FALSE, FALSE, csOtherPlayerFountainCover.id)
							ENDIF
						ELSE
							CREATE_IG_8_ENEMIES_FOR_TREVOR_DURING_COURTYARD_FIGHT()
							IF IS_ENTITY_OK(TREV_PED_ID())
								SET_PED_COMBAT_MOVEMENT(TREV_PED_ID(), CM_DEFENSIVE)
								REMOVE_PED_DEFENSIVE_AREA(TREV_PED_ID())
								SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(TREV_PED_ID(), m_tbDefensiveAreaTrevorOnRoof)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(TREV_PED_ID(), 200)
							ENDIF
						ENDIF
					ENDIF

					m_bCheckFailureKillVolume = FALSE
					ePlayerSwitchState = PLAYER_SWITCH_SETUP
					
					m_bFirstSwitch = FALSE
					SETUP_DAVE_ATTRIBUTES()
					CREATE_IG_8_COVER_POINTS()
					CREATE_POOL_NAVMESH_BLOCKERS()
					CREATE_ENEMY_WAVE(m_EnemyWaveFIB, 5, "WAR_FIB", &ENEMY_WAVE_DEFAULT_TASK)
					CREATE_ENEMY_WAVE(m_EnemyWaveMWCourt, 5, "WAR_MW", &ENEMY_WAVE_DEFAULT_TASK)
					UPDATE_ENEMY_WAVE(m_EnemyWaveFIB, m_psWarFIB, m_vecFIBSpawnPoints, mod_ped_fib, m_tbDefensiveAreaWarFIB, TRUE)
					UPDATE_ENEMY_WAVE(m_EnemyWaveMWCourt, m_psWarMWCourtyard, m_vecMWCourtSpawnPoints, mod_ped_mw, m_tbDefensiveAreaWarMWCourtyard)
					DELETE_PED(m_psWarFIB[0].id)
					DELETE_PED(m_psWarMWCourtyard[0].id)
					SETUP_WAR_PEDS_FOR_FINAL_SHOWDOWN_FROM_RESTART()
					MOVE_DAVE_TO_LOCATION_AGGRESSIVE(m_flagDaveCoverPositions[3].pos)
					TRIGGER_MUSIC_EVENT("MIC3_DAVE_ESCAPES_RESTART")
					
				BREAK
				
				CASE STAGE_ESCAPE_MUSEUM
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_SNIPERRIFLE, 10, TRUE, TRUE, TRUE)
					CREATE_TREVOR_HELICOPTER()
					CREATE_CRASHED_HELI()
					m_bDoStairFires = TRUE
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_ESCAPE_MUSEUM")
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (m_flagDaveCoverPositions[3].pos), m_flagDaveCoverPositions[3].dir, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_ESCAPE_MUSEUM")
						WAIT(0)
					ENDWHILE
					
					IF g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
						Prep_Hotswap(FALSE, TRUE, FALSE)
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE)
						Prep_Hotswap(FALSE, TRUE, FALSE)
						peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
						peds[mpf_trev].id = PLAYER_PED_ID()
						
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(MIKE_PED_ID(), "MI_3_MICHAEL_GROUP")
						START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
						
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							SET_ENTITY_COORDS(TREV_PED_ID(), <<-2248.9978, 268.9602, 173.6020>>)
							SET_ENTITY_HEADING(TREV_PED_ID(), 24.2243)
							
							SET_ENTITY_COORDS(MIKE_PED_ID(), <<-2246.66187, 271.64197, 173.60196>>)
							SET_ENTITY_HEADING(MIKE_PED_ID(), 24.2243)
						ELSE
							SET_ENTITY_COORDS(MIKE_PED_ID(), <<-2248.9978, 268.9602, 173.6020>>)
							SET_ENTITY_HEADING(MIKE_PED_ID(), 24.2243)
						ENDIF
						
						GIVE_WEAPON_TO_PED(MIKE_PED_ID(), WEAPONTYPE_COMBATMG, 1000, TRUE)
					ELSE
						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = PLAYER_PED_ID()
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trev].id
						peds[mpf_mike].id = PLAYER_PED_ID()
						
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(TREV_PED_ID(), "MI_3_TREVOR_GROUP")
						START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
	
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2248.9978, 268.9602, 173.6020>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 24.2243)
						
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							SET_ENTITY_COORDS(TREV_PED_ID(), <<-2246.66187, 271.64197, 173.60196>>)
							SET_ENTITY_HEADING(TREV_PED_ID(), 24.2243)
						ENDIF
					ENDIF
					
					m_bCheckFailureKillVolume = FALSE
					SETUP_DAVE_ATTRIBUTES()
					MOVE_DAVE_TO_LOCATION_AGGRESSIVE(m_flagDaveCoverPositions[3].pos)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					TRIGGER_MUSIC_EVENT("MIC3_ESCAPE_RESTART")
				BREAK
				
				CASE STAGE_VEHICLE_ESCAPE
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_VEHICLE_ESCAPE")
					// Create trevor and heli
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_SNIPERRIFLE, 10, TRUE, TRUE, TRUE)
					CREATE_TREVOR_HELICOPTER()
					CREATE_CRASHED_HELI()
					
					WHILE NOT CREATE_NPC_PED_ON_FOOT(m_pedAllyDave, CHAR_DAVE, (<<-2330.06396, 259.40601, 168.60179>>), 300, FALSE)
						CPRINTLN(DEBUG_MISSION, "Creating Dave for STAGE_ESCAPE_MUSEUM")
						WAIT(0)
					ENDWHILE
					
					IF g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
						
						Prep_Hotswap(FALSE, TRUE, FALSE)
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE)
						Prep_Hotswap(FALSE, TRUE, FALSE)
						peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
						peds[mpf_trev].id = PLAYER_PED_ID()	
						
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(MIKE_PED_ID(), "MI_3_MICHAEL_GROUP")
						START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_TREVOR")
						
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							SET_ENTITY_COORDS(TREV_PED_ID(), csPlayerParkingLotCover.pos)
							SET_ENTITY_HEADING(TREV_PED_ID(), 112.58)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(TREV_PED_ID(), csPlayerParkingLotCover.pos, -1, TRUE, 0, TRUE, TRUE, csPlayerParkingLotCover.id)
						ENDIF
						
						SET_ENTITY_COORDS(MIKE_PED_ID(), <<-2289.14331, 267.78027, 168.60179>>)
						SET_ENTITY_HEADING(MIKE_PED_ID(), 24.2243)
					ELSE
						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = PLAYER_PED_ID()
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trev].id
						peds[mpf_mike].id = PLAYER_PED_ID()
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(TREV_PED_ID(), "MI_3_TREVOR_GROUP")
						START_AUDIO_SCENE("MI_3_SHOOTOUT_PLAYER_IS_MICHAEL")
						
						SET_ENTITY_COORDS(PLAYER_PED_ID(), csPlayerParkingLotCover.pos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 112.58)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), csPlayerParkingLotCover.pos, -1, TRUE, 0, TRUE, TRUE, csPlayerParkingLotCover.id)
						
						IF g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_AT_DAVE
							SET_ENTITY_COORDS(TREV_PED_ID(), <<-2289.14331, 267.78027, 168.60179>>)
							SET_ENTITY_HEADING(TREV_PED_ID(), 24.2243)
						ENDIF
					ENDIF
				
					m_bCheckFailureKillVolume = FALSE
					IF IS_ENTITY_OK(MIKE_PED_ID())
						SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), TRUE)
					ENDIF
					
					IF IS_ENTITY_OK(TREV_PED_ID())
						SET_PED_USING_ACTION_MODE(TREV_PED_ID(), TRUE)
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						m_pedOtherPlayer = MIKE_PED_ID()
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						m_pedOtherPlayer = TREV_PED_ID()
					ENDIF
					
					
					TRIGGER_MISSION_EVENT(sEvents[mef_escape_vehicles].sData)
					TRIGGER_MISSION_EVENT(sEvents[mef_fight_parking_lot].sData)
					TRIGGER_MISSION_EVENT(sEvents[mef_parking_lot_jeep].sData, 5000)
					SETUP_DAVE_ATTRIBUTES()
					Prep_Hotswap(FALSE, TRUE, FALSE)
//					SET_SELECTOR_PED_ACTIVITY(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_ACTIVITY_SNIPER)
										
					IS_SKIP_OK_TO_FADE_IN()
					
					TRIGGER_MUSIC_EVENT("MIC3_VEHICLE_ESCAPE_RESTART")
				BREAK		
				
				CASE STAGE_END_CUTSCENE
					IF g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_MICHAEL
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Requesting the Michael leadin")
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MIC_3_EXT", CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5
																	   | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 
																	   | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13 
																	   | CS_SECTION_14 | CS_SECTION_15 | CS_SECTION_16 | CS_SECTION_17)
					ELIF g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
						CPRINTLN(DEBUG_MISSION, "UPDATE_CUTSCENE_STREAMING - Requesting the Trevor leadin")
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("MIC_3_EXT", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5
																	   | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9 
																	   | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13 
																	   | CS_SECTION_14 | CS_SECTION_15 | CS_SECTION_16 | CS_SECTION_17)
					ENDIF
				BREAK
				
				CASE STAGE_MISSION_PASSED
					CPRINTLN(DEBUG_MISSION, "Mission_Stage_Skip - Skipping to STAGE_MISSION_PASSED")
					IS_SKIP_OK_TO_FADE_IN()
				BREAK
			ENDSWITCH
			
			END_REPLAY_SETUP()
						
			IF IS_SCREEN_FADED_OUT()
			OR (NOT IS_SCREEN_FADING_IN())
				// Camera heading setup
				SWITCH int_to_enum(mission_stage_flag, mission_stage)
					CASE STAGE_WALK_TO_DAVE
						PRELOAD_CONVERSATION(sConvo, str_dialogue, "M3_INTL", CONV_PRIORITY_HIGH)
					BREAK
					
					CASE STAGE_FIRST_AREA
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					BREAK
					
					CASE STAGE_TREVOR_SHOOTS_HELICOPTER
						IF g_replay.iReplayInt[GENERIC_MISSION_FLAGS] != iCONST_HELI_CUT_WATCHED
							WHILE NOT LOAD_STREAM("MIC_3_HELICOPTER_SHOT_DOWN_MASTER")
								CPRINTLN(DEBUG_MISSION, "Loading MIC_3_HELICOPTER_SHOT_DOWN_MASTER")
								WAIT(0)
							ENDWHILE
						ELSE
							IF IS_ENTITY_OK(PLAYER_PED_ID())
								CPRINTLN(DEBUG_MISSION, "Setting Trevor's position/heading for restart")
								SET_ENTITY_COORDS(PLAYER_PED_ID(), m_flagTrevorPosition.pos)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), m_flagTrevorPosition.dir)
							ENDIF
						ENDIF
					BREAK
					
					CASE STAGE_TREVOR_SAVES_DAVE
					CASE STAGE_DAVE_BY_FOUNTAIN
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-30)
					BREAK
					
					CASE STAGE_DAVE_ESCAPES
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-90)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)

						IF g_replay.iReplayInt[PLAYER_AT_FOUNTAIN] = iCONST_TREVOR
						AND g_replay.iReplayInt[TREVOR_RESPAWN_LOCATION] = iCONST_ON_ROOF
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-30)
						ENDIF
					BREAK
					CASE STAGE_VEHICLE_ESCAPE
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-80)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					BREAK
				ENDSWITCH
				
				IF IS_ENTITY_OK(MIKE_PED_ID())
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), FALSE)
				ENDIF
				IF IS_ENTITY_OK(TREV_PED_ID())
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), FALSE)
				ENDIF
				IF INT_TO_ENUM(mission_stage_flag, mission_stage) <> STAGE_END_CUTSCENE
				AND INT_TO_ENUM(mission_stage_flag, mission_stage) <> STAGE_MEETING_CUTSCENE
				AND INT_TO_ENUM(mission_stage_flag, mission_stage) <> STAGE_TREVOR_SHOOTS_HELICOPTER 
					IS_SKIP_OK_TO_FADE_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				Unload_Asset_NewLoadScene(sAssetData)
			ENDIF
			bDoSkip = FALSE
		ENDIF
#IF IS_DEBUG_BUILD
	// Check is a skip is being asked for, and dont allow skip during setup stage
	ELIF LAUNCH_MISSION_STAGE_MENU(zMenuNames, iSkipToStage, mission_stage, FALSE, "Michael 3", TRUE, TRUE)
		//iSkipToStage += 1 	// add 1 as msf start on 1 and z menu starts on 0, keeps them in sync
		msfStageSkip = INT_TO_ENUM(MISSION_STAGE_FLAG, iSkipToStage)
		
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE(TRUE)
		ENDIF
		DO_SCREEN_FADE_OUT(ROUND(DEFAULT_FADE_TIME * f_current_time_scale))
		bDoSkip = TRUE
	ELSE
		#IF IS_DEBUG_BUILD 
			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)			
				Mission_Passed()
			ENDIF
			IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
				Mission_Failed(mff_debug_forced)
			ENDIF
			DONT_DO_J_SKIP(s_locatesDataMike)
		#ENDIF
#ENDIF
	ENDIF
	
ENDPROC

// setup the mission
PROC Mission_Setup()

	mission_stage = enum_to_int(STAGE_WALK_TO_DAVE)
	
	IF Is_Replay_In_Progress()	

		INT i_replay_skip_to_stage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted
			i_replay_skip_to_stage++
		ENDIF

		msfStageSkip = GET_STAGE_FROM_CHECKPOINT(i_replay_skip_to_stage)
		
		SWITCH msfStageSkip
			CASE STAGE_WALK_TO_DAVE
				START_REPLAY_SETUP( <<-2291.8286, 367.6647, 173.6017>>, 203.8096 )
			BREAK
			CASE STAGE_MEETING_CUTSCENE
				START_REPLAY_SETUP( << -2156.9224, 234.4068, 183.6015 >>, 113.7745 )
			BREAK
			CASE STAGE_FIRST_AREA
				START_REPLAY_SETUP( <<-2156.8623, 234.3435, 183.6019>>, 21.5258 )
			BREAK
			CASE STAGE_TREVOR_SHOOTS_HELICOPTER
				// fix for bug 1891163
				//START_REPLAY_SETUP( <<-2221.4248, 256.6886, 183.6041>>, 109.6962 )
				START_REPLAY_SETUP( <<-2262.4780, 235.2824, 193.6113>>, 320.1852 )
			BREAK
			CASE STAGE_TREVOR_SAVES_DAVE
				SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
				START_REPLAY_SETUP( <<-2221.31421, 251.09691, 183.60408>>, 320.1850 )
			BREAK
			CASE STAGE_DAVE_BY_FOUNTAIN
				SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
				START_REPLAY_SETUP( <<-2219.2981, 247.3061, 183.6041>>, 40.5077 )
			BREAK
			CASE STAGE_DAVE_ESCAPES
				SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
				START_REPLAY_SETUP( <<-2249.85522, 270.12097, 173.60196>>, 24.2243 )
			BREAK
			CASE STAGE_ESCAPE_MUSEUM
				SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
				START_REPLAY_SETUP( <<-2249.85522, 270.12097, 173.60196>>, 24.2243 )
			BREAK
			CASE STAGE_VEHICLE_ESCAPE
				SET_BUILDING_STATE( BUILDINGNAME_IPL_MIC3_HELI_DEBRIS, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
				START_REPLAY_SETUP( <<-2295.5474, 264.1368, 168.6018>>, 25.3686 )
			BREAK
			CASE STAGE_END_CUTSCENE
				START_REPLAY_SETUP( <<-1473.1211, -396.4522, 37.1653>>, 307.9418)
			BREAK
			CASE STAGE_MISSION_PASSED
				START_REPLAY_SETUP( <<-2295.5474, 264.1368, 168.6018>>, 25.3686 )
			BREAK
		ENDSWITCH
		
		bDoSkip = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	
	
	
		zMenuNames[0].sTxtLabel 	= "WALK TO DAVE"
		zMenuNames[1].sTxtLabel 	= "MEETING CUTSCENE"
		zMenuNames[1].bSelectable	= FALSE
		zMenuNames[2].sTxtLabel 	= "FIRST AREA"
		zMenuNames[3].sTxtLabel 	= "TREVOR SHOOTS HELICOPTER"
		zMenuNames[4].sTxtLabel 	= "TREVOR SAVES DAVE"
		zMenuNames[5].sTxtLabel 	= "DAVE AT FOUNTAIN"
		zMenuNames[6].sTxtLabel 	= "FINAL SHOWDOWN"
		zMenuNames[7].sTxtLabel 	= "ESCAPE THE MUSEUM"
		zMenuNames[8].sTxtLabel 	= "VEHICLE ESCAPE"
		zMenuNames[9].sTxtLabel 	= "MISSION PASSED"
		
		widget_debug = START_WIDGET_GROUP("Michael 3 - The Wrap Up") 
			
			START_WIDGET_GROUP("Helicopter Crash Scene")
				ADD_WIDGET_FLOAT_SLIDER("Start Time", flWidgetTimeToStartHeli, 0.0, 20.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Time to Skip", flWidgetTimeToSkipInHeliRecording, 0.0, 20000.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Speed", flWidgetRecordingSpeed, 0.0, 5.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Distance", flDistanceToBlowUpHeli, 0.0, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Explosion Pos", vWidgetExplosionPosition, -8000, 8000, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Focus push tweaks")
				ADD_WIDGET_VECTOR_SLIDER( "HINT ENTITY OFFSET", vEntityHint, -1.0, 1.0, 0.01 )
				ADD_WIDGET_FLOAT_SLIDER( "HINT SCALAR", fHintScalar, 0, 1.0, 0.01 )
				ADD_WIDGET_FLOAT_SLIDER( "HINT VERTICAL", fHintVOffset, -1.0, 1.0, 0.01 )
				ADD_WIDGET_FLOAT_SLIDER( "HINT HORIZONTAL", fHintHOffset, -1.0, 1.0, 0.01 )
				ADD_WIDGET_FLOAT_SLIDER( "HINT PITCH", fHintPitch, -90.0, 90.0, 1.0 )
				ADD_WIDGET_FLOAT_SLIDER( "HINT FOV", fHintFOV, 0, 50.0, 1.0 )
				ADD_WIDGET_BOOL( "RESET HINT", bResetFocusCam )
//				ADD_WIDGET_BOOL( "START CUTSCENE", bStartCutscene )
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Debug Info")
				ADD_WIDGET_FLOAT_READ_ONLY("GameCamRelHeading", f_debug_gam_cam_rel_heading)
				ADD_WIDGET_FLOAT_READ_ONLY("GameCamRelPitch", f_debug_gam_cam_rel_pitch)
				ADD_WIDGET_FLOAT_READ_ONLY("PlayerHeading", f_debug_player_heading)
				ADD_WIDGET_FLOAT_READ_ONLY("Vehicle Playback", f_debug_rec_phase)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Mike AI Controller")
				twi_mike_ai_section_current 	= ADD_TEXT_WIDGET("Current Section:")
				twi_mike_ai_section_next 		= ADD_TEXT_WIDGET("Next Section:")
				twi_mike_ai_section_next_alt 	= ADD_TEXT_WIDGET("Alt Section:")
				twi_mike_ai_section_pinned		= ADD_TEXT_WIDGET("Pinned Section:")
				twi_mike_ai_stage 				= ADD_TEXT_WIDGET("Mike AI Stage:")
				ADD_WIDGET_INT_READ_ONLY("Mike Escape AI Stage", i_MichaelEscapeAI)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Trev AI Controller")
				twi_trev_ai_stage 				= ADD_TEXT_WIDGET("Trev AI Stage:")
				ADD_WIDGET_INT_READ_ONLY("Trev Escape AI Stage", i_TrevorEscapeAI)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Enemy Area Detection")
				ADD_WIDGET_BOOL("YARD 1", 		bEnemyInArea[MUS_GND_YARD_ONE])
				ADD_WIDGET_BOOL("YARD 2",		bEnemyInArea[MUS_GND_YARD_TWO])
				ADD_WIDGET_BOOL("YARD 3",		bEnemyInArea[MUS_GND_YARD_THREE])
				ADD_WIDGET_BOOL("YARD END", 	bEnemyInArea[MUS_GND_END])
				ADD_WIDGET_BOOL("YARD TO PL", 	bEnemyInArea[MUS_GND_TO_PL])
				ADD_WIDGET_BOOL("YARD PL END", 	bEnemyInArea[MUS_GND_PL_END])
				ADD_WIDGET_BOOL("BALC START",	bEnemyInArea[MUS_BALC_START])
				ADD_WIDGET_BOOL("BALC 1", 		bEnemyInArea[MUS_BALC_ONE])
				ADD_WIDGET_BOOL("BALC 2", 		bEnemyInArea[MUS_BALC_TWO])
				ADD_WIDGET_BOOL("BALC 3", 		bEnemyInArea[MUS_BALC_THREE])
				ADD_WIDGET_BOOL("BALC 4", 		bEnemyInArea[MUS_BALC_FOUR])
				ADD_WIDGET_BOOL("BALC SMALL",	bEnemyInArea[MUS_BALC_BRIDGE_SMALL])
				ADD_WIDGET_BOOL("BALC BIG", 	bEnemyInArea[MUS_BALC_BRIDGE_BIG])
				ADD_WIDGET_BOOL("BALC END", 	bEnemyInArea[MUS_BALC_END])
				ADD_WIDGET_BOOL("BALC TO GND", 	bEnemyInArea[MUS_BALC_TO_GND_START])
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	#ENDIF
	
	ai_michael = ai_michael
	bEnemyInArea = bEnemyInArea
	
	Load_Asset_Additional_Text(sAssetData, "MIC3", MISSION_TEXT_SLOT)
	
	// selector ped stuff
	peds[mpf_mike].id = PLAYER_PED_ID()
	
	CREATE_INITIAL_SCENE(TRUE)
	
	// Init State Machines
	eWalkToDaveState = WALK_TO_DAVE_INIT
	eFirstAreaState = FIRST_AREA_INIT
	eTrevorShootsHeliState = TREVOR_SHOOTS_HELI_INIT
	eTrevorSavesDaveState = TREVOR_SAVES_DAVE_INIT
	eDaveFountainState = DAVE_AT_FOUNTAIN_INIT
	eEscapeMuseumState = ESCAPE_MUSEUM_INIT
	eVehicleEscapeState = VEHICLE_ESCAPE_INIT
	eDaveEscapesState = DAVE_ESCAPES_INIT

	eDaveFleeState = DAVE_FLEE_INIT
	eOtherPlayerFleeState = DAVE_FLEE_INIT

	m_iFirstAreaHelicopterState = 1
	
	eMessagesFirstArea = MISSION_MESSAGE_IDLE
	eDaveStairsMessages = MISSION_MESSAGE_01
	eTrevorShootsHeliMessages = MISSION_MESSAGE_IDLE
	eTrevorSavesDaveMessages = MISSION_MESSAGE_01
	eDaveFountainMessages = MISSION_MESSAGE_01
	eEscapeMuseumMessages = MISSION_MESSAGE_01
	eVehicleEscapeMessages = MISSION_MESSAGE_01
	eDaveEscapesMessages = MISSION_MESSAGE_01	
	
	eSniperAudioState = SSA_INIT
	
	// Setup relationship groups
	ADD_RELATIONSHIP_GROUP("FRIEND",		REL_FRIEND)
	ADD_RELATIONSHIP_GROUP("CIA",			REL_CIA)
	ADD_RELATIONSHIP_GROUP("FIB",			REL_FIB)
	ADD_RELATIONSHIP_GROUP("MERRYWEATHER",	REL_MW)
	ADD_RELATIONSHIP_GROUP("FIB_FINAL",		REL_FIB_FINAL)
	ADD_RELATIONSHIP_GROUP("MW_FINAL",		REL_MW_FINAL)
	ADD_RELATIONSHIP_GROUP("CIA_FINAL",		REL_CIA_FINAL)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_FIB, REL_FIB_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FIB, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FIB, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FIB, REL_MW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FIB, REL_CIA)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_MW, REL_MW_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_MW, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_MW, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_MW, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_MW, REL_CIA)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_CIA, REL_CIA_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_CIA, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_CIA, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_CIA, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_CIA, REL_MW)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_FRIEND, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FRIEND, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FRIEND, REL_FIB_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FRIEND, REL_MW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FRIEND, REL_MW_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FRIEND, REL_CIA)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FRIEND, REL_CIA_FINAL)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, REL_MW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, REL_CIA)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FIB_FINAL, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_FIB_FINAL, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_FIB_FINAL, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_FIB_FINAL, REL_MW_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_FIB_FINAL, REL_MW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_FIB_FINAL, REL_CIA)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_FIB_FINAL, REL_CIA_FINAL)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_MW_FINAL, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_MW_FINAL, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_MW_FINAL, REL_MW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_MW_FINAL, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_MW_FINAL, REL_FIB_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_MW_FINAL, REL_CIA)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_MW_FINAL, REL_CIA_FINAL)
	
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_CIA_FINAL, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, REL_CIA_FINAL, REL_FRIEND)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, REL_CIA_FINAL, REL_CIA)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_CIA_FINAL, REL_FIB)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_CIA_FINAL, REL_FIB_FINAL)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_CIA_FINAL, REL_MW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, REL_CIA_FINAL, REL_MW_FINAL)
	
	// Fill out museum section structs
	// Balcony Route
	sMuseumSections[MUS_BALC_START].v1						= <<-2161.130127,213.080200,182.601425>>
	sMuseumSections[MUS_BALC_START].v2						= <<-2179.092773,252.911896,189.351654>> 
	sMuseumSections[MUS_BALC_START].fWidth					= 45.25
	sMuseumSections[MUS_BALC_START].eNextSection			= MUS_BALC_TO_GND_START
	sMuseumSections[MUS_BALC_START].eAlternativeSection		= MUS_BALC_ONE
	sMuseumSections[MUS_BALC_START].ePrevSection			= MUS_NO_AREA
	sMuseumSections[MUS_BALC_START].vNavToCoord				= << -2188.7056, 223.0552, 183.6020 >>
	sMuseumSections[MUS_BALC_START].vDefensiveAreas[0] 		= <<-2178.0208, 230.5221, 183.6019>>
	sMuseumSections[MUS_BALC_START].vDefensiveAreas[1] 		= <<-2181.3125, 227.7930, 183.6019>>
	sMuseumSections[MUS_BALC_START].vDefensiveAreas[2] 		= <<-2187.9014, 219.9653, 183.6019>>
	sMuseumSections[MUS_BALC_START].vDefensiveAreas[3] 		= << -2198.3198, 244.6006, 183.6020 >>
	sMuseumSections[MUS_BALC_START].bCanBePinnedFromHere[MUS_BALC_ONE] 				= TRUE
	sMuseumSections[MUS_BALC_START].bCanBePinnedFromHere[MUS_BALC_TO_GND_START] 	= TRUE

	sMuseumSections[MUS_BALC_ONE].v1						= <<-2216.373047,240.029312,183.621872>>
	sMuseumSections[MUS_BALC_ONE].v2						= <<-2193.760742,250.063675,189.028519>>
	sMuseumSections[MUS_BALC_ONE].fWidth					= 6.875
	sMuseumSections[MUS_BALC_ONE].eNextSection				= MUS_BALC_TWO
	sMuseumSections[MUS_BALC_ONE].eAlternativeSection		= MUS_NO_AREA
	sMuseumSections[MUS_BALC_ONE].ePrevSection				= MUS_BALC_START
	sMuseumSections[MUS_BALC_ONE].vNavToCoord				= << -2214.1350, 243.0489, 183.6020 >>
	sMuseumSections[MUS_BALC_ONE].vDefensiveAreas[0] 		= <<-2204.50, 242.53, 183.61>>
	sMuseumSections[MUS_BALC_ONE].vDefensiveAreas[1] 		= <<-2209.78, 240.08, 183.61>>
	sMuseumSections[MUS_BALC_ONE].vDefensiveAreas[2] 		= <<-2212.04, 244.49, 183.61>>
	sMuseumSections[MUS_BALC_ONE].bCanBePinnedFromHere[MUS_BALC_TWO] 			= TRUE
	sMuseumSections[MUS_BALC_ONE].bCanBePinnedFromHere[MUS_BALC_TO_GND_START] 	= TRUE
	
	
	sMuseumSections[MUS_BALC_TWO].v1						= <<-2212.043701,238.477234,183.602036>>//<<-2230.824219,280.256287,183.601517>>
	sMuseumSections[MUS_BALC_TWO].v2						= <<-2230.359619,280.610931,188.664017>>//<<-2214.868652,244.444382,188.602036>> 
	sMuseumSections[MUS_BALC_TWO].fWidth					=  6.8125//6.5
	sMuseumSections[MUS_BALC_TWO].eNextSection				= MUS_BALC_BRIDGE_SMALL
	sMuseumSections[MUS_BALC_TWO].eAlternativeSection		= MUS_NO_AREA
	sMuseumSections[MUS_BALC_TWO].ePrevSection				= MUS_BALC_ONE
	sMuseumSections[MUS_BALC_TWO].vNavToCoord				= << -2227.6055, 278.7260, 183.6015 >>
	sMuseumSections[MUS_BALC_TWO].vDefensiveAreas[0] 		= << -2219.28, 248.13, 183.62 >>
	sMuseumSections[MUS_BALC_TWO].vDefensiveAreas[1] 		= << -2217.35, 254.84, 183.61 >>
	sMuseumSections[MUS_BALC_TWO].vDefensiveAreas[2] 		= << -2223.97, 258.68, 183.63 >>
	sMuseumSections[MUS_BALC_TWO].vDefensiveAreas[3] 		= << -2221.98, 265.41, 183.61 >>
	sMuseumSections[MUS_BALC_TWO].vDefensiveAreas[4] 		= << -2228.68, 269.30, 183.6 >>
	sMuseumSections[MUS_BALC_TWO].vDefensiveAreas[5] 		= << -2226.70, 276.02, 183.61 >>
	sMuseumSections[MUS_BALC_TWO].bCanBePinnedFromHere[MUS_BALC_BRIDGE_SMALL] 	= TRUE
	sMuseumSections[MUS_BALC_TWO].bCanBePinnedFromHere[MUS_BALC_THREE] 			= TRUE
	
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].v1					= <<-2218.750244,280.485138,189.280838>>//<<-2219.579102,280.116150,188.672806>>
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].v2					= <<-2229.987793,305.791199,183.351868>>//<<-2230.980957,305.597321,183.511810>> 
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].fWidth				= 14.25//15.0
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].eNextSection			= MUS_BALC_THREE
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].eAlternativeSection	= MUS_NO_AREA
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].ePrevSection			= MUS_BALC_TWO
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].vNavToCoord			= << -2229.5710, 301.1237, 183.6015 >>
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].vDefensiveAreas[0] 	= << -2227.92, 305.66, 183.68 >>
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].bCanBePinnedFromHere[MUS_BALC_THREE] 		= TRUE
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].bCanBePinnedFromHere[MUS_BALC_BRIDGE_BIG] 	= TRUE
	sMuseumSections[MUS_BALC_BRIDGE_SMALL].bCanBePinnedFromHere[MUS_BALC_FOUR] 			= TRUE
	

	sMuseumSections[MUS_BALC_THREE].v1						= <<-2239.687988,294.025696,183.601517>>//<<-2255.792480,327.547821,196.601547>>
	sMuseumSections[MUS_BALC_THREE].v2						= <<-2255.513672,330.176788,189.914047>>//<<-2240.262207,293.264557,183.601868>> 
	sMuseumSections[MUS_BALC_THREE].fWidth					= 14.312500//11.4375
	sMuseumSections[MUS_BALC_THREE].eNextSection			= MUS_BALC_BRIDGE_BIG
	sMuseumSections[MUS_BALC_THREE].eAlternativeSection		= MUS_NO_AREA
	sMuseumSections[MUS_BALC_THREE].ePrevSection			= MUS_BALC_BRIDGE_SMALL
	sMuseumSections[MUS_BALC_THREE].vNavToCoord 			= << -2257.1951, 319.2654, 183.6015 >>
	sMuseumSections[MUS_BALC_THREE].vDefensiveAreas[0] 		= << -2254.63, 315.64, 183.69 >>
	sMuseumSections[MUS_BALC_THREE].bCanBePinnedFromHere[MUS_BALC_BRIDGE_BIG] 		= TRUE
	sMuseumSections[MUS_BALC_THREE].bCanBePinnedFromHere[MUS_BALC_FOUR] 			= TRUE
	
	sMuseumSections[MUS_BALC_BRIDGE_BIG].v1						= <<-2258.347656,319.346100,183.601257>>
	sMuseumSections[MUS_BALC_BRIDGE_BIG].v2						= <<-2290.765869,304.791443,189.652206>> 
	sMuseumSections[MUS_BALC_BRIDGE_BIG].fWidth					= 11.437500
	sMuseumSections[MUS_BALC_BRIDGE_BIG].eNextSection			= MUS_BALC_FOUR
	sMuseumSections[MUS_BALC_BRIDGE_BIG].eAlternativeSection	= MUS_NO_AREA
	sMuseumSections[MUS_BALC_BRIDGE_BIG].ePrevSection			= MUS_BALC_THREE
	sMuseumSections[MUS_BALC_BRIDGE_BIG].vDefensiveAreas[0] 	= << -2283.55, 303.30, 183.61 >>
	sMuseumSections[MUS_BALC_BRIDGE_BIG].vNavToCoord 			= << -2285.8379, 304.1011, 183.6017 >>
	sMuseumSections[MUS_BALC_BRIDGE_BIG].bCanBePinnedFromHere[MUS_BALC_FOUR] 		= TRUE
	
	sMuseumSections[MUS_BALC_FOUR].v1						= <<-2282.464600,302.255646,183.601257>>
	sMuseumSections[MUS_BALC_FOUR].v2						= <<-2266.701904,265.928192,188.666321>> 
	sMuseumSections[MUS_BALC_FOUR].fWidth					= 11.4375
	sMuseumSections[MUS_BALC_FOUR].eNextSection				= MUS_BALC_END
	sMuseumSections[MUS_BALC_FOUR].eAlternativeSection		= MUS_NO_AREA
	sMuseumSections[MUS_BALC_FOUR].ePrevSection				= MUS_BALC_BRIDGE_BIG
	sMuseumSections[MUS_BALC_FOUR].vDefensiveAreas[0] 		= << -2279.45, 294.07, 183.61 >>
	sMuseumSections[MUS_BALC_FOUR].vDefensiveAreas[1] 		= << -2275.39, 284.87, 183.62 >>
	sMuseumSections[MUS_BALC_FOUR].vDefensiveAreas[2] 		= << -2271.29, 275.81, 183.61 >>
	sMuseumSections[MUS_BALC_FOUR].vNavToCoord				= << -2271.2834, 266.7107, 183.6017 >>
	sMuseumSections[MUS_BALC_FOUR].bCanBePinnedFromHere[MUS_BALC_END]			= TRUE
	
	sMuseumSections[MUS_BALC_END].v1						= <<-2271.871826,263.790710,183.617096>>
	sMuseumSections[MUS_BALC_END].v2						= <<-2288.754639,256.401764,188.668655>> 
	sMuseumSections[MUS_BALC_END].fWidth					= 11.437500
	sMuseumSections[MUS_BALC_END].eNextSection				= MUS_NO_AREA
	sMuseumSections[MUS_BALC_END].eAlternativeSection		= MUS_NO_AREA
	sMuseumSections[MUS_BALC_END].ePrevSection				= MUS_BALC_FOUR
	sMuseumSections[MUS_BALC_END].vNavToCoord				= << -2287.7336, 261.9569, 183.6017 >>
	
	// Ground route main
	sMuseumSections[MUS_BALC_TO_GND_START].v1										= <<-2210.674805,218.995331,173.601868>>
	sMuseumSections[MUS_BALC_TO_GND_START].v2										= <<-2192.228516,227.309570,188.914169>>
	sMuseumSections[MUS_BALC_TO_GND_START].fWidth									= 17.125000
	sMuseumSections[MUS_BALC_TO_GND_START].eNextSection								= MUS_GND_YARD_ONE
	sMuseumSections[MUS_BALC_TO_GND_START].eAlternativeSection						= MUS_NO_AREA
	sMuseumSections[MUS_BALC_TO_GND_START].ePrevSection								= MUS_BALC_START
	sMuseumSections[MUS_BALC_TO_GND_START].bCanBePinnedFromHere[MUS_GND_YARD_ONE] 	= TRUE
	sMuseumSections[MUS_BALC_TO_GND_START].vDefensiveAreas[0] 						= <<-2203.4861, 217.9068, 183.6019>>
	sMuseumSections[MUS_BALC_TO_GND_START].vDefensiveAreas[1]						= <<-2202.0701, 223.1243, 181.3021>>
	sMuseumSections[MUS_BALC_TO_GND_START].vDefensiveAreas[2]						= <<-2206.4873, 221.1523, 178.6019>>
	sMuseumSections[MUS_BALC_TO_GND_START].vDefensiveAreas[3]						= <<-2202.1895, 223.0681, 176.3020>>
	sMuseumSections[MUS_BALC_TO_GND_START].vNavToCoord								= sMuseumSections[MUS_BALC_TO_GND_START].vDefensiveAreas[3]
	
	sMuseumSections[MUS_GND_YARD_ONE].v1										= <<-2180.863281,223.929413,176.602051>>
	sMuseumSections[MUS_GND_YARD_ONE].v2										= <<-2248.429932,193.093094,173.602036>> 
	sMuseumSections[MUS_GND_YARD_ONE].fWidth									= 66.750000
	sMuseumSections[MUS_GND_YARD_ONE].eNextSection								= MUS_GND_YARD_TWO
	sMuseumSections[MUS_GND_YARD_ONE].eAlternativeSection						= MUS_NO_AREA
	sMuseumSections[MUS_GND_YARD_ONE].ePrevSection								= MUS_BALC_TO_GND_START
	sMuseumSections[MUS_GND_YARD_ONE].bCanBePinnedFromHere[MUS_GND_YARD_TWO] 	= TRUE
	sMuseumSections[MUS_GND_YARD_ONE].bCanBePinnedFromHere[MUS_BALC_ONE] 		= TRUE
	sMuseumSections[MUS_GND_YARD_ONE].vDefensiveAreas[0]						= <<-2209.0029, 238.6103, 173.6018>>
	sMuseumSections[MUS_GND_YARD_ONE].vDefensiveAreas[1]						= <<-2216.7817, 241.4975, 173.6018>>
	sMuseumSections[MUS_GND_YARD_ONE].vNavToCoord								= sMuseumSections[MUS_GND_YARD_ONE].vDefensiveAreas[1]
	
	sMuseumSections[MUS_GND_YARD_TWO].v1											= <<-2241.286377,278.579315,173.601883>>
	sMuseumSections[MUS_GND_YARD_TWO].v2											= <<-2224.195313,240.834213,176.601868>> 
	sMuseumSections[MUS_GND_YARD_TWO].fWidth										= 25.500000
	sMuseumSections[MUS_GND_YARD_TWO].eNextSection									= MUS_GND_YARD_THREE
	sMuseumSections[MUS_GND_YARD_TWO].eAlternativeSection							= MUS_NO_AREA
	sMuseumSections[MUS_GND_YARD_TWO].ePrevSection									= MUS_GND_YARD_ONE
	sMuseumSections[MUS_GND_YARD_TWO].bCanBePinnedFromHere[MUS_GND_YARD_THREE]		= TRUE
	sMuseumSections[MUS_GND_YARD_TWO].bCanBePinnedFromHere[MUS_GND_END]				= TRUE
	sMuseumSections[MUS_GND_YARD_TWO].bCanBePinnedFromHere[MUS_BALC_BRIDGE_SMALL]	= TRUE
	sMuseumSections[MUS_GND_YARD_TWO].bCanBePinnedFromHere[MUS_BALC_THREE]			= TRUE
	sMuseumSections[MUS_GND_YARD_TWO].bCanBePinnedFromHere[MUS_BALC_FOUR]			= TRUE
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[0]							= <<-2219.4102, 247.1894, 173.6018>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[1]							= <<-2221.7710, 252.4771, 173.6018>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[2]							= <<-2228.5688, 245.2584, 173.6018>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[3]							= <<-2228.4443, 252.7509, 173.6018>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[4]							= <<-2223.6389, 258.7863, 173.6017>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[5]							= <<-2226.0134, 264.1177, 173.6017>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[6]							= <<-2228.3813, 269.4354, 173.6017>>
	sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[7]							= <<-2246.1934, 272.7435, 173.6017>>
	sMuseumSections[MUS_GND_YARD_TWO].vNavToCoord									= sMuseumSections[MUS_GND_YARD_TWO].vDefensiveAreas[7]
	
	sMuseumSections[MUS_GND_YARD_THREE].v1											= <<-2242.791260,220.908005,178.601654>>
	sMuseumSections[MUS_GND_YARD_THREE].v2											= <<-2264.221191,268.353546,173.602020>> 
	sMuseumSections[MUS_GND_YARD_THREE].fWidth										= 25.000000
	sMuseumSections[MUS_GND_YARD_THREE].eNextSection								= MUS_GND_END
	sMuseumSections[MUS_GND_YARD_THREE].eAlternativeSection							= MUS_GND_TO_PL
	sMuseumSections[MUS_GND_YARD_THREE].ePrevSection								= MUS_GND_YARD_TWO
	sMuseumSections[MUS_GND_YARD_THREE].bCanBePinnedFromHere[MUS_BALC_FOUR]			= TRUE
	sMuseumSections[MUS_GND_YARD_THREE].bCanBePinnedFromHere[MUS_BALC_END]			= TRUE
	sMuseumSections[MUS_GND_YARD_THREE].bCanBePinnedFromHere[MUS_GND_END]			= TRUE
	sMuseumSections[MUS_GND_YARD_THREE].bCanBePinnedFromHere[MUS_GND_TO_PL]			= TRUE
	sMuseumSections[MUS_GND_YARD_THREE].vDefensiveAreas[0]							= <<-2255.8511, 255.9058, 173.6017>>
	sMuseumSections[MUS_GND_YARD_THREE].vDefensiveAreas[1]							= <<-2267.2986, 259.7054, 173.6021>>
	sMuseumSections[MUS_GND_YARD_THREE].vNavToCoord									= sMuseumSections[MUS_GND_YARD_THREE].vDefensiveAreas[1]
	
	sMuseumSections[MUS_GND_END].v1						= <<-2274.663818,291.783142,172.352020>>
	sMuseumSections[MUS_GND_END].v2						= <<-2264.221191,268.353546,178.602020>> 
	sMuseumSections[MUS_GND_END].fWidth					= 25.000000
	sMuseumSections[MUS_GND_END].eNextSection			= MUS_NO_AREA
	sMuseumSections[MUS_GND_END].eAlternativeSection	= MUS_NO_AREA
	sMuseumSections[MUS_GND_END].ePrevSection			= MUS_GND_YARD_THREE
	sMuseumSections[MUS_GND_END].vDefensiveAreas[0]	 	= <<-2264.8008, 278.0986, 173.6032>>
	sMuseumSections[MUS_GND_END].vDefensiveAreas[1]	 	= <<-2270.2839, 275.5808, 173.6021>>
	sMuseumSections[MUS_GND_END].vDefensiveAreas[2]	 	= <<-2274.4448, 284.8751, 173.6021>>
	sMuseumSections[MUS_GND_END].vDefensiveAreas[3]	 	= <<-2268.8806, 287.2551, 173.6021>>
	sMuseumSections[MUS_GND_END].vDefensiveAreas[4]	 	= <<-2277.7979, 277.7830, 173.6021>>
	sMuseumSections[MUS_GND_END].vDefensiveAreas[5]	 	= <<-2275.4072, 272.4052, 173.6021>>
	sMuseumSections[MUS_GND_END].vNavToCoord			= sMuseumSections[MUS_GND_END].vDefensiveAreas[5]
	
	sMuseumSections[MUS_GND_TO_PL].v1										= <<-2285.399170,237.370544,166.602158>>
	sMuseumSections[MUS_GND_TO_PL].v2										= <<-2255.168213,251.227219,173.601852>> 
	sMuseumSections[MUS_GND_TO_PL].fWidth									= 38.937500
	sMuseumSections[MUS_GND_TO_PL].eNextSection								= MUS_GND_PL_END
	sMuseumSections[MUS_GND_TO_PL].eAlternativeSection						= MUS_NO_AREA
	sMuseumSections[MUS_GND_TO_PL].ePrevSection								= MUS_GND_YARD_THREE
	sMuseumSections[MUS_GND_TO_PL].bCanBePinnedFromHere[MUS_BALC_END]		= TRUE
	sMuseumSections[MUS_GND_TO_PL].bCanBePinnedFromHere[MUS_GND_PL_END]		= TRUE
	
	sMuseumSections[MUS_GND_PL_END].v1					= <<-2317.221924,244.142166,157.289459>>
	sMuseumSections[MUS_GND_PL_END].v2					= <<-2325.363281,262.313263,175.102097>> 
	sMuseumSections[MUS_GND_PL_END].fWidth				= 90.000000
	sMuseumSections[MUS_GND_PL_END].eNextSection		= MUS_NO_AREA
	sMuseumSections[MUS_GND_PL_END].eAlternativeSection	= MUS_NO_AREA
	sMuseumSections[MUS_GND_PL_END].ePrevSection		= MUS_GND_TO_PL
	sMuseumSections[MUS_GND_PL_END].vNavToCoord			= << -2303.2505, 270.3268, 168.6017 >>
	
	//m_tbDefensiveAreaWarCIA = CREATE_TRIGGER_BOX(<<-2258.795410,294.818665,172.312164>>, <<-2268.384277,315.016876,175.612030>>, 40.000000)
	m_tbDefensiveAreaWarFIB = CREATE_TRIGGER_BOX(<<-2256.911621,295.781982,173.501846>>, <<-2232.675293,306.822601,176.557617>>, 40.000000)
	m_tbDefensiveAreaWarMWCourtyard = CREATE_TRIGGER_BOX(<<-2261.984131,293.610901,173.501862>>, <<-2279.061768,285.846069,176.602112>>, 40.000000)
	m_tbDefensiveAreaTrevorOnRoof = CREATE_TRIGGER_BOX(<<-2267.473145,231.023621,193.361282>>, <<-2267.465820,235.543716,196.329865>>, 10.000000)
	m_tbDefensiveAreaAtFountain = CREATE_TRIGGER_BOX(<<-2247.691650,267.826599,173.601959>>, <<-2249.232910,271.272949,175.560150>>, 5.500000)
	m_tbDefensiveAreaTrevorDangerPeds = CREATE_TRIGGER_BOX(<<-2287.470703,255.319504,183.351181>>, <<-2274.367188,264.838776,185.851410>>, 9.750000)
	m_tbLargeRoofVolume = CREATE_TRIGGER_BOX(<<-2272.863525,231.138000,193.097916>>, <<-2262.119141,231.012192,197.770889>>, 11.750000)
	m_tbMichaelTaskZone1 = CREATE_TRIGGER_BOX(<<-2212.655273,239.515213,183.354874>>, <<-2231.212891,280.063812,187.854080>>, 6.500000)
	m_tbMichaelTaskZone2 = CREATE_TRIGGER_BOX(<<-2222.624512,278.760040,187.530960>>, <<-2238.567627,313.440552,181.852371>>, 31.000000)
	m_tbMichaelTaskZone3 = CREATE_TRIGGER_BOX(<<-2249.864990,322.868134,186.111572>>, <<-2288.660645,306.753571,176.201416>>, 27.500000)
	m_tbAbandonFailFirstArea1 = CREATE_TRIGGER_BOX(<<-2199.787354,308.828979,168.612595>>, <<-2176.495361,256.921692,185.851273>>, 12.500000)
	m_tbAbandonFailFirstArea2 = CREATE_TRIGGER_BOX(<<-2136.136963,247.651474,152.789276>>, <<-2195.889160,234.701050,175.851822>>, 90.000000)
	m_tbAbandonFailOnStairs = CREATE_TRIGGER_BOX(<<-2214.474365,243.628143,183.354874>>, <<-2230.554932,278.377747,186.604080>>, 7.250000)
	m_tbFailKillVolume	= CREATE_TRIGGER_BOX(<<-2201.045654,181.609497,168.057159>>, <<-2294.546387,385.655060,175.716644>>, 100.000000)
	
	m_tbRoofObjective = CREATE_TRIGGER_BOX(<<-2174.450195,193.343414,178.399719>>, <<-2272.159912,235.890900,215.485733>>, 82.250000)
	m_tbParkingLotObjective = CREATE_TRIGGER_BOX(<<-2204.965088,183.093399,178.351822>>, <<-2294.659424,374.741333,137.601501>>, 198.250000)

	m_tbSpawnTrevorOnlyMW1 = CREATE_TRIGGER_BOX(<<-2263.992920,226.026657,196.861374>>, <<-2263.915771,220.879608,193.111633>>, 3.000000)
	m_tbSpawnTrevorOnlyMW2 = CREATE_TRIGGER_BOX(<<-2267.551025,194.600464,166.111740>>, <<-2273.162842,206.830124,171.861740>>, 12.750000)
	m_tbSpawnTrevorOnlyMW3 = CREATE_TRIGGER_BOX(<<-2275.641357,227.146103,166.101990>>, <<-2276.583740,233.524597,171.851990>>, 23.750000)
	m_tbSpawnTrevorOnlyMW4 = CREATE_TRIGGER_BOX(<<-2272.635986,223.866241,184.984772>>, <<-2270.081543,223.831055,189.051651>>, 4.000000)
	m_tbSpawnTrevorOnlyMW5 = CREATE_TRIGGER_BOX(<<-2241.806152,181.154144,173.351822>>, <<-2244.912842,188.461563,176.364777>>, 16.750000)

	m_tbDefensiveAreaFIBClose = CREATE_TRIGGER_BOX(<<-2237.574707,261.974731,173.351959>>, <<-2246.958008,281.166260,176.351959>>, 28.750000)
	m_tbDefensiveAreaMWClose = CREATE_TRIGGER_BOX(<<-2252.554199,258.327606,173.351959>>, <<-2260.777832,276.970276,176.353210>>, 28.750000)
//	m_tbDefensiveAreaClosest = CREATE_TRIGGER_BOX(<<-2245.432617,261.070618,173.351959>>, <<-2250.276367,273.027863,176.351959>>, 20.000000)

	m_tbDefensiveAreaFinalFightMW = CREATE_TRIGGER_BOX(<<-2253.704346,261.386078,173.101959>>, <<-2268.843750,293.805725,176.102112>>, 30.000000)
	m_tbDefensiveAreaFinalFightFIB = CREATE_TRIGGER_BOX(<<-2233.559570,265.309967,173.101959>>, <<-2253.279297,308.430359,176.101959>>, 40.000000)
	m_tbDefensiveAreaLargerFountainArea = CREATE_TRIGGER_BOX(<<-2245.666504,263.804840,173.351959>>, <<-2249.809814,273.284698,176.241623>>, 21.250000)
	
	m_bBlipManagementDuringAbandonChecks = FALSE
	m_bLastParkingLotGuyTasked = FALSE
	
	// FIB Spawn Points
	m_vecFIBSpawnPoints[0] = <<-2213.15723, 296.46368, 172.21460>>
	m_vecFIBSpawnPoints[1] = <<-2199.99072, 302.02350, 168.60205>>
	m_vecFIBSpawnPoints[2] = <<-2194.55542, 296.71420, 168.60205>>
	m_vecFIBSpawnPoints[3] = <<-2232.49390, 329.48697, 173.60196>>
	m_vecFIBSpawnPoints[4] = <<-2227.02783, 339.89551, 173.60196>>
	m_vecFIBSpawnPoints[5] = <<-2247.10327, 361.47083, 173.60167>>
	
	// CIA Spawn Points
	//m_vecCIASpawnPoints[0] = <<-2271.55151, 349.61700, 173.60211>>
	//m_vecCIASpawnPoints[1] = <<-2273.14624, 351.35309, 173.60211>>
	//m_vecCIASpawnPoints[2] = <<-2274.88818, 352.74890, 173.60211>>
	//m_vecCIASpawnPoints[3] = <<-2290.58057, 337.64474, 173.60211>>
	//m_vecCIASpawnPoints[4] = <<-2292.16797, 341.16794, 173.60211>>
	//m_vecCIASpawnPoints[5] = <<-2292.59668, 344.08057, 173.60211>>
	
	// MW Spawn Points
	m_vecMWCourtSpawnPoints[0] = <<-2291.25903, 273.24777, 168.60179>>
	m_vecMWCourtSpawnPoints[1] = <<-2300.87866, 272.53091, 168.60179>>
	m_vecMWCourtSpawnPoints[2] = <<-2292.92798, 275.47305, 168.60019>>
	m_vecMWCourtSpawnPoints[3] = <<-2286.20776, 266.10181, 168.60179>>
	m_vecMWCourtSpawnPoints[4] = <<-2290.03491, 263.46082, 168.60179>>
	m_vecMWCourtSpawnPoints[5] = <<-2287.38940, 262.14844, 168.60179>>
	
	//DAVE'S COVER POSITIONS
	//First stage
	CREATE_FLAG(m_flagDaveCoverPositions[0],(<<-2178.50537, 229.83009, 183.60187>>), 120.0000)
	CREATE_FLAG(m_flagDaveCoverPositions[1],(<<-2201.84619, 224.85478, 181.11182>>), 22.63)
	CREATE_FLAG(m_flagDaveCoverPositions[2],(<<-2214.79883, 236.53975, 173.60182>>), 30.0000)
	CREATE_FLAG(m_flagDaveCoverPositions[3],(<<-2248.5859, 270.7738, 173.6020>>), 30.0000)
	CREATE_FLAG(m_flagDaveCoverPositions[4],(<<-2265.4089, 276.9197, 173.6027>>), 30.0000)
	CREATE_FLAG(m_flagDaveCoverPositions[5],(<<-2260.6975, 317.4653, 173.6020>>), 344.7188)
	
	CREATE_FLAG(m_flagTrevorPosition, (<<-2262.4780, 235.2824, 193.6113>>), 320.1852)

	Create_Cover_Point(csFirstAreaIPLCover[0], (<<-2183.28809, 243.95781, 183.60187>>), 118.93, COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, COVARC_315TO0)
	Create_Cover_Point(csFirstAreaIPLCover[1], (<<-2183.50854, 243.26906, 183.60187>>), 22.93, COVUSE_WALLTOLEFT, COVHEIGHT_HIGH, COVARC_0TO45)
	
	Create_Cover_Point(csMichaelStartingCover, (<<-2218.97192, 247.25658, 183.60408>>), 23.4404, COVUSE_WALLTONEITHER, COVHEIGHT_HIGH, COVARC_90, FALSE)
	Create_Cover_Point(csPlayerFountainCover, (<<-2249.85522, 270.12097, 173.60196>>), 23, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
	Create_Cover_Point(csDaveFountainCover, (<<-2248.48120, 270.75278, 173.60196>>), 23.4404, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
	Create_Cover_Point(csOtherPlayerFountainCover, (<<-2246.73853, 271.60706, 173.60196>>), 23, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
	Create_Cover_Point(csMichaelMIC3_INTCover, (<<-2156.8828, 234.3567, 183.6019>>), 112.6611, COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_120)
	Create_Cover_Point(csPlayerParkingLotCover, (<<-2295.68, 264.06, 168.60179>>), 24, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_315TO0)
	Create_Cover_Point(csTrevorCoverSpot, (<<-2268.97192, 235.27168, 193.61137>>), 357, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_300TO0)
	Create_Cover_Point(csDaveBottomOfStairsCover, (<<-2214.79883, 236.53975, 173.60182>>), 23, COVUSE_WALLTONEITHER, COVHEIGHT_HIGH, COVARC_300TO0)
	
	m_bDoStairFires = FALSE
	m_bCheckFailureKillVolume = FALSE
	m_bDavePlayGetInCarMessage = FALSE
	m_psEMPTY[0] = m_psEMPTY[0]
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	
	EVENT_Add_Mission_Events()
	
	TRIGGER_MISSION_EVENT(sEvents[mef_manage_radar].sData)
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_CREATE_RANDOM_COPS(FALSE)
	SET_MAX_WANTED_LEVEL(0)
	CPRINTLN(DEBUG_MISSION, "RJM - Setting Max Wanted Level to 0")
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
	
	DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, TRUE) 
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, TRUE) 
	DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
	
	iTrevorPathNavmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT((<<-2242.038, 182.101, 173.969>>),(<<17.3, 20, 8>>), DEG_TO_RAD(23.000))
	
	IF mission_stage != ENUM_TO_INT(STAGE_WALK_TO_DAVE)
		CPRINTLN(DEBUG_MISSION, "Clearing and Blocking")
		Clear_And_Block_Area(TRUE)
	ENDIF
	
	CLOSE_MIKE_ONLY_FIB_DOORS(TRUE)
	CLOSE_IG_7_DOORS(TRUE)
	CLOSE_IG_8_DOORS(TRUE)
	ADD_PED_FOR_DIALOGUE(sConvo, 0, MIKE_PED_ID(), "Michael")
	
	g_bBlockShopRoberies = TRUE
ENDPROC

//BOOL bMikeExited

//PURPOSE: Mission Stage 3 - Mental bloody cutscene!
PROC MISSION_STAGE_MEETING_CUTSCENE()

	IF mission_substage >= 1
		
		ENTITY_INDEX tempEntity
		
		// Dave
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dave_FBI")
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_MEETING_CUTSCENE - Setting 'Dave_FBI' exit state")
			
			// Fix for Dave's blend out from cutscene
			// 1934876
			//TASK_DAVE_RUN_TO_COORD(m_flagDaveCoverPositions[1].pos, FALSE)
			TASK_DAVE_RUN_TO_COORD(m_flagDaveCoverPositions[1].pos, FALSE, FALSE)
			FORCE_PED_MOTION_STATE(m_pedAllyDave, MS_ON_FOOT_SPRINT, FALSE, FAUS_CUTSCENE_EXIT)
		ENDIF
		
		// Andreas
		IF NOT DOES_ENTITY_EXIST(peds[mpf_andreas].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Andreas_Sanchez", IG_ANDREAS)
			IF DOES_ENTITY_EXIST(tempEntity)
				peds[mpf_andreas].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
			
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Andreas_Sanchez", IG_ANDREAS)
		
			TASK_PLAY_ANIM_ADVANCED(peds[mpf_andreas].id, "Dead", "dead_g", 
							<<-2153.3, 235.581,184.623>>, <<0.000, 0.000, 41.50>>, 
							instant_blend_in, normal_blend_out, -1, 
							AF_ENDS_IN_DEAD_POSE | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE, 0.99)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_andreas].id, TRUE)
			
			decal_andreas = ADD_DECAL(int_to_enum(DECAL_RENDERSETTING_ID, 9001), <<-2153.70, 236.07, 183.60>>, <<0,0,-1.0>>, <<0.0,1.0,0.0>>, 0.5, 0.5, 0.196, 0, 0, 1.0, -1)
							
			SET_PED_COMPONENT_VARIATION(peds[mpf_andreas].id, PED_COMP_HEAD, 1, 0)
		
		ENDIF


		// CIA Lead
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[0].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Dreyfuss")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaCIA[0].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
			
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dreyfuss")
			
			// Create the Merryweather guys
			/*
			IF NOT DOES_ENTITY_EXIST(m_psFirstAreaMW[0].id)
				Create_Mission_Ped(m_psFirstAreaMW[0], mod_ped_mw, << -2200.4321, 223.9028, 181.1019 >>, 117.7441, "START_MW_1", REL_MW, weap_mw_rifle, 5, 0, FALSE)
				Set_Ped_Combat_Params(m_psFirstAreaMW[0], << -2185.2820, 218.5643, 183.6019 >>, 2.5, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 0.75, 0.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(m_psFirstAreaMW[0].id, <<-2178.1055, 232.9911, 183.6019>>, 15.0, FALSE, TRUE)
				Create_Cover_Point_For_Ped(m_psFirstAreaMW[0], << -2185.2820, 218.5643, 183.6019 >>, 293.1335, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(m_psFirstAreaMW[0].id, FALSE)
				SET_PED_PATH_CAN_DROP_FROM_HEIGHT(m_psFirstAreaMW[0].id, FALSE)
			ENDIF
			*/
			IF IS_ENTITY_OK(m_psFirstAreaCIA[0].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaCIA[0].id, "START_CIA_LEAD(CS)")
				SET_ENTITY_COORDS(m_psFirstAreaCIA[0].id, (<<-2170.68091, 244.19972, 183.60187>>))
				SET_ENTITY_HEADING(m_psFirstAreaCIA[0].id, 203)
				Set_Ped_Combat_Params(m_psFirstAreaCIA[0], <<-2168.56006, 239.71907, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaCIA[0].id, REL_CIA)
				GIVE_WEAPON_TO_PED(m_psFirstAreaCIA[0].id, WEAPONTYPE_PISTOL, 120, TRUE, TRUE)
				SET_PED_PROP_INDEX(m_psFirstAreaCIA[0].id, ANCHOR_EYES,0)
			ENDIF
			
//			START_DREYFUSS_SYNCHED_SCENE()
//			START_MW_SYNCHED_SCENE()
		ENDIF
		
		// CIA 2
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[1].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CIA_Goon_1")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaCIA[1].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
			
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("CIA_Goon_1")
			IF IS_ENTITY_OK(m_psFirstAreaCIA[1].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaCIA[1].id, "START_CIA_2(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaCIA[1], <<-2165.82080, 237.71999, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaCIA[1].id, REL_CIA)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaCIA[1].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaCIA[1].id, WEAPONTYPE_PISTOL, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaCIA[1].id, <<-2167.22290, 241.20326, 183.60187>>)
				SET_ENTITY_HEADING(m_psFirstAreaCIA[1].id, 294.7903)
				//Create_Cover_Point(m_psFirstAreaCIA[1].cov, <<-2163.5889, 235.5993, 183.6019>>, 294.7903, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
				//TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaCIA[1].id, <<-2163.5889, 235.5993, 183.6019>>, -1, FALSE, 1, TRUE, FALSE, m_psFirstAreaCIA[1].cov.id, TRUE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(m_psFirstAreaCIA[1].id, 100.0)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaCIA[1].id)
			ENDIF
		ENDIF
		
		// CIA 3
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[2].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CIA_Goon_2")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaCIA[2].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
		
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("CIA_Goon_2")
			IF IS_ENTITY_OK(m_psFirstAreaCIA[2].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaCIA[2].id, "START_CIA_3(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaCIA[2], <<-2177.97632, 233.94264, 183.60187>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaCIA[2].id, REL_CIA)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaCIA[2].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaCIA[2].id, WEAPONTYPE_PISTOL, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaCIA[2].id, <<-2179.18188, 236.76740, 183.60187>>)
				SET_ENTITY_HEADING(m_psFirstAreaCIA[2].id, 292.3021)
//				Create_Cover_Point(m_psFirstAreaCIA[2].cov, <<-2173.6719, 231.1608, 183.6019>>, 292.3021, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
//				TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaCIA[2].id, <<-2173.6719, 231.1608, 183.6019>>, -1, FALSE, 1, TRUE, FALSE, m_psFirstAreaCIA[2].cov.id, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaCIA[2].id)
			ENDIF
		ENDIF
		
		// CIA 4
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaCIA[3].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("CIA_Goon_3")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaCIA[3].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
			
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("CIA_Goon_3")	
			IF IS_ENTITY_OK(m_psFirstAreaCIA[3].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaCIA[3].id, "START_CIA_4(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaCIA[3], <<-2170.8118, 241.1140, 183.6019>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaCIA[3].id, REL_CIA)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaCIA[3].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaCIA[3].id, WEAPONTYPE_PISTOL, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaCIA[3].id, <<-2170.8118, 241.1140, 183.6019>>)
				SET_ENTITY_HEADING(m_psFirstAreaCIA[3].id, 204.9214)
				Create_Cover_Point(m_psFirstAreaCIA[3].cov, <<-2170.53760, 241.26949, 183.60187>>, 204.9214, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_315TO0)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaCIA[3].id, <<-2170.53760, 241.26949, 183.60187>>, -1, FALSE, 1, TRUE, FALSE, m_psFirstAreaCIA[3].cov.id, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaCIA[3].id)
			ENDIF
		ENDIF
		
		// FIB Lead
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[0].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lead_FBI")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaFIB[0].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lead_FBI")
			IF IS_ENTITY_OK(m_psFirstAreaFIB[0].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaFIB[0].id, "START_FIB_LEAD(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaFIB[0], <<-2164.5945, 223.9964, 183.6019>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaFIB[0].id, REL_FIB)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaFIB[0].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaFIB[0].id, WEAPONTYPE_CARBINERIFLE, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaFIB[0].id, <<-2164.5945, 223.9964, 183.6019>>)
				SET_ENTITY_HEADING(m_psFirstAreaFIB[0].id, 23.3085)
				Create_Cover_Point(m_psFirstAreaFIB[0].cov, <<-2164.5945, 223.9964, 183.6019>>, 23.3085, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaFIB[0].id, <<-2164.5945, 223.9964, 183.6019>>, -1, TRUE, 1, TRUE, FALSE, m_psFirstAreaFIB[0].cov.id, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaFIB[0].id)
			ENDIF
		ENDIF

		// FIB 1
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[1].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("FIB_Goon_1")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaFIB[1].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
		
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FIB_Goon_1")
			IF IS_ENTITY_OK(m_psFirstAreaFIB[1].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaFIB[1].id, "START_FIB_1(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaFIB[1], <<-2160.0496, 224.2791, 183.6019>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, TRUE)	
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaFIB[1].id, REL_FIB)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaFIB[1].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaFIB[1].id, WEAPONTYPE_CARBINERIFLE, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaFIB[1].id, <<-2160.0496, 224.2791, 183.6019>>)
				SET_ENTITY_HEADING(m_psFirstAreaFIB[1].id, 24.2236)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaFIB[1].id, <<-2160.0496, 224.2791, 183.6019>>, -1, FALSE, 1, TRUE, TRUE, NULL, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaFIB[1].id)
			ENDIF
		ENDIF
		
		// FIB 2
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[2].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("FIB_Goon_2")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaFIB[2].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
			
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FIB_Goon_2")
			IF IS_ENTITY_OK(m_psFirstAreaFIB[2].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaFIB[2].id, "START_FIB_2(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaFIB[2], <<-2170.1541, 221.6710, 183.6019>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaFIB[2].id, REL_FIB)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaFIB[2].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaFIB[2].id, WEAPONTYPE_CARBINERIFLE, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaFIB[2].id, <<-2170.1541, 221.6710, 183.6019>>)
				SET_ENTITY_HEADING(m_psFirstAreaFIB[2].id, 22.8487)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(m_psFirstAreaFIB[2].id, <<-2170.1541, 221.6710, 183.6019>>, -1, FALSE, 1, TRUE, FALSE, NULL, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaFIB[2].id)
			ENDIF
		ENDIF
		
		// FIB 3
		IF NOT DOES_ENTITY_EXIST(m_psFirstAreaFIB[3].id)
			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("FIB_Goon_3")
			IF DOES_ENTITY_EXIST(tempEntity)
				m_psFirstAreaFIB[3].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
			ENDIF
		
		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FIB_Goon_3")
			IF IS_ENTITY_OK(m_psFirstAreaFIB[3].id)
				SET_PED_NAME_DEBUG(m_psFirstAreaFIB[3].id, "START_FIB_3(CS)")
				Set_Ped_Combat_Params(m_psFirstAreaFIB[3], <<-2167.1433, 223.9884, 183.6019>>, 2.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.5, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(m_psFirstAreaFIB[3].id, REL_FIB)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_psFirstAreaFIB[3].id, TRUE)
				GIVE_WEAPON_TO_PED(m_psFirstAreaFIB[3].id, WEAPONTYPE_CARBINERIFLE, 120, TRUE, TRUE)
				SET_ENTITY_COORDS(m_psFirstAreaFIB[3].id, <<-2167.1433, 223.9884, 183.6019>>)
				SET_ENTITY_HEADING(m_psFirstAreaFIB[3].id, 50.9471)
				TASK_AIM_GUN_AT_COORD(m_psFirstAreaFIB[3].id, <<-2173.7585, 231.0308, 184.1056>>, -1, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(m_psFirstAreaFIB[3].id)
			ENDIF
		ENDIF
		
		// MICHAEL'S GUN
//		IF NOT DOES_ENTITY_EXIST(m_michaelsGun)
//			tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Security_guard_pistol")
//			IF DOES_ENTITY_EXIST(tempEntity)
//				CPRINTLN( DEBUG_MISSION, "GOT MICHAEL'S GUN" )
//				m_michaelsGun = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity)
//			ENDIF
//		ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Security_guard_pistol")
//			IF DOES_ENTITY_EXIST(m_michaelsGun)
//				CPRINTLN( DEBUG_MISSION, "DELETING CUTSCENE GUN" )
//				DELETE_OBJECT(m_michaelsGun)
//			ENDIF
//		ENDIF
			
		// Fix for weapon attachments not carrying across into cutscene
		// bug 1934876
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Security_guard_pistol")
			IF DOES_ENTITY_EXIST(m_michaelsGun)
				CPRINTLN( DEBUG_MISSION, "Giving gun back to player ped" )
				GIVE_WEAPON_OBJECT_TO_PED(m_michaelsGun, MIKE_PED_ID() )
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			CPRINTLN(DEBUG_MISSION, "Player exit state")
			
			SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
			FLOAT fBlend = 0.5
			// preventing pop if cutscene is skipped
			IF WAS_CUTSCENE_SKIPPED()
				fBlend = 0.0
				SET_ENTITY_COORDS(MIKE_PED_ID(), csMichaelMIC3_INTCover.pos)
				SET_ENTITY_HEADING(MIKE_PED_ID(), 21.5259)
			ENDIF
			
			TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), csMichaelMIC3_INTCover.pos, -1, TRUE, fBlend, TRUE, FALSE, csMichaelMIC3_INTCover.id, FALSE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE_PED_ID(), TRUE)

		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			b_cutscene_ready_for_exit = TRUE
		ENDIF
		
		IF b_cutscene_ready_for_exit
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
				SET_GAMEPLAY_CAM_WORLD_HEADING(70.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				CPRINTLN(DEBUG_MISSION, "Camera exit state first person")
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				CPRINTLN(DEBUG_MISSION, "Camera exit state any other mode")
			ENDIF
		ENDIF
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			IF HAS_CUTSCENE_LOADED()
			 	Load_Asset_Model(sAssetData, mod_trev_heli)
				Load_Asset_Model(sAssetData, mod_ped_mw)
				Load_Asset_Model(sAssetData, mod_ped_fib)
				Load_Asset_Model(sAssetData, mod_ped_cia)
				Load_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_PISTOL)
				Load_Asset_Weapon_Asset(sAssetData, weap_cia_handgun)
				Load_Asset_Weapon_Asset(sAssetData, weap_cia_rifle)
				Load_Asset_Weapon_Asset(sAssetData, weap_fib_rifle)
				Load_Asset_Weapon_Asset(sAssetData, weap_mw_rifle)
				Load_Asset_AnimDict(sAssetData, "Dead")
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				PREPARE_MUSIC_EVENT("MIC3_MISSION_START")
				
				IF NOT HAS_PED_GOT_WEAPON( MIKE_PED_ID(), WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(MIKE_PED_ID(), WEAPONTYPE_PISTOL, 120)
				ENDIF
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
				
				REGISTER_ENTITY_FOR_CUTSCENE(m_pedAllyDave, 		"Dave_FBI", 			CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_daves_paper].id, 	"Daves_Newspaper",		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(null, 						"Andreas_Sanchez", 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,		IG_ANDREAS)
				
				REGISTER_ENTITY_FOR_CUTSCENE(null, "Dreyfuss", 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	IG_PAPER)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "CIA_Goon_1", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_M_CIASEC_01)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "CIA_Goon_2", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_M_CIASEC_01)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "CIA_Goon_3", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_M_CIASEC_01)
				
				REGISTER_ENTITY_FOR_CUTSCENE(null, "Lead_FBI", 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_Y_SWAT_01)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "FIB_Goon_1", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_Y_SWAT_01)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "FIB_Goon_2", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_Y_SWAT_01)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "FIB_Goon_3", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	S_M_Y_SWAT_01)
				
				// Fix for weapon attachments not carrying across into cutscene
				// bug 1934876
				m_michaelsGun = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS( MIKE_PED_ID(), WEAPONTYPE_PISTOL )
				REGISTER_ENTITY_FOR_CUTSCENE(m_michaelsGun, "Security_guard_pistol", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL) )
				
				IF DOES_ENTITY_EXIST(m_vehPlayerStartingCar)
					CPRINTLN(DEBUG_MISSION, "Player car exists just before the mission, set as vehicle gen")
					SET_MISSION_VEHICLE_GEN_VEHICLE(m_vehPlayerStartingCar, (<<0,0,0>>), 0.0)
				ENDIF
				
				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)

				//bMikeExited = FALSE
				b_cutscene_ready_for_exit = FALSE
				mission_substage = 99
			ENDIF
		BREAK
		
		CASE 99
			CLEAR_AREA((<<-2155.38257, 237.16681, 183.60187>>), 50.0, FALSE)
			CLEAR_AREA((<<-2155.38257, 237.16681, 183.60187>>), 15.0, TRUE)
			CLEAR_AREA_OF_PEDS((<<-2155.38257, 237.16681, 183.60187>>), 50.0)
			CLEAR_AREA_OF_OBJECTS((<<-2155.38257, 237.16681, 183.60187>>), 50.0)
			CLEAR_AREA_OF_VEHICLES((<<-2155.38257, 237.16681, 183.60187>>), 50.0)
			
			IF IS_ENTITY_OK(m_vehPlayerStartingCar)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(m_vehPlayerStartingCar, (<<-2156.89746, 237.02055, 183.60187>>)) <= 100.0
					CPRINTLN(DEBUG_MISSION, "Had to move the player's starting car")
					CLEAR_AREA_OF_VEHICLES(<<-2284.70679, 406.94702, 173.46698>>, 20)
					SET_ENTITY_COORDS(m_vehPlayerStartingCar, (<<-2284.67725, 406.71301, 173.46698>>))
					SET_ENTITY_HEADING(m_vehPlayerStartingCar, 306.68)
					SET_VEHICLE_ON_GROUND_PROPERLY(m_vehPlayerStartingCar)
				ENDIF
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
			mission_substage = 1
		BREAK
		
		CASE 1
			IF IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() >= 67500
				IF PREPARE_MUSIC_EVENT("MIC3_INTRO")
					TRIGGER_MUSIC_EVENT("MIC3_INTRO")
				ENDIF
				
				mission_substage++
			ENDIF
		BREAK
		
		CASE 2		
			IF IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() >= 105000
				IF PREPARE_MUSIC_EVENT("MIC3_MISSION_START")
					TRIGGER_MUSIC_EVENT("MIC3_MISSION_START")
				ENDIF
				
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() >= 109000
				TRIGGER_MUSIC_EVENT("MIC3_STEVE_SHOT")
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			IF b_cutscene_ready_for_exit
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				mission_substage++
			ENDIF
		BREAK
		CASE 5
		
			IF NOT IS_CUTSCENE_PLAYING()
				REPLAY_STOP_EVENT()
				b_cutscene_ready_for_exit = FALSE
				Mission_Set_Stage(STAGE_FIRST_AREA)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

BOOL bMovedTrevHeli = FALSE
BOOL bEnteredAsTrevor = FALSE
BOOL bSwitchEffectPlayed = FALSE

PROC MISSION_STAGE_END_CUTSCENE()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			bEnteredAsTrevor = FALSE
			bSwitchEffectPlayed = FALSE
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				bEnteredAsTrevor = TRUE
			ENDIF
			mission_substage++
		BREAK
		
		CASE 1
			IF HAS_CUTSCENE_LOADED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				END_ALL_MISSION_EVENTS(sEvents)

				// Clean up peds
				INT i
				REPEAT COUNT_OF(peds) i
					IF i > 2
						IF DOES_ENTITY_EXIST(peds[i].id)
							SET_PED_AS_NO_LONGER_NEEDED(peds[i].id)
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF DOES_ENTITY_EXIST(m_pedAllyDave)
					SET_PED_AS_NO_LONGER_NEEDED(m_pedAllyDave)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(TREV_PED_ID())
					Create_Player_Ped(peds[mpf_trev], SELECTOR_PED_TREVOR, m_flagTrevorPosition.pos, m_flagTrevorPosition.dir, WEAPONTYPE_UNARMED, 10, TRUE, TRUE, FALSE)
				ENDIF
				
				Unload_Asset_Model(sAssetData, IG_DAVENORTON)
				Unload_Asset_Model(sAssetData, mod_ped_cia)
				Unload_Asset_Model(sAssetData, mod_ped_fib)
				Unload_Asset_Model(sAssetData, mod_ped_mw)
				Unload_asset_model(sAssetData, mod_trev_heli)
				Unload_asset_model(sAssetData, mod_heli_mw)
				Unload_asset_model(sAssetData, mod_mw_jeep)
				Unload_asset_model(sAssetData, mod_mw_van)
				
				// Clean up props
				REPEAT COUNT_OF(objs) i
					IF DOES_ENTITY_EXIST(objs[i].id)
						SET_OBJECT_AS_NO_LONGER_NEEDED(objs[i].id)
					ENDIF
				ENDREPEAT
				
				Unload_Asset_Recording(sAssetData, rec_mw_heli_1, str_carrecs)
				Unload_Asset_Recording(sAssetData, rec_mw_heli_2, str_carrecs)
				Unload_Asset_Recording(sAssetData, rec_mw_heli_3, str_carrecs)
				Unload_Asset_Recording(sAssetData, rec_mw_jeep_2, str_carrecs)
				Unload_Asset_Recording(sAssetData, rec_dave_esc, str_carrecs)
				
//				REGISTER_ENTITY_FOR_CUTSCENE(MIKE_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				REGISTER_ENTITY_FOR_CUTSCENE(TREV_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
//				IF PLAYER_PED_ID() = TREV_PED_ID()
//					REGISTER_ENTITY_FOR_CUTSCENE(MIKE_PED_ID(), "Michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
//				ELSE
//					REGISTER_ENTITY_FOR_CUTSCENE(TREV_PED_ID(), "Trevor", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
			
				TRIGGER_MUSIC_EVENT("MIC3_STOP_TRACK")
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				
				REDUCE_AMBIENT_MODELS(TRUE)
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				IF bEnteredAsTrevor
					CPRINTLN(DEBUG_MISSION, "Doing force switch back to Michael, 'cuz we entered as Trevor 3")
					Prep_Hotswap(FALSE, TRUE, FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
					UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE, FALSE)
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					Prep_Hotswap(TRUE, TRUE, TRUE)
				ENDIF
//				Load_Asset_NewLoadScene_Sphere(sAssetData, <<1274.1030, -1712.9059, 55.4589>>, 7.5, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			DESTROY_ALL_MISSION_PEDS(FALSE)
			mission_substage++
		BREAK
		
		CASE 3
			INT i
			IF IS_CUTSCENE_PLAYING()
			AND NOT bMovedTrevHeli
				IF DOES_ENTITY_EXIST(vehs[mvf_trev_heli].id)
					SET_ENTITY_COORDS(vehs[mvf_trev_heli].id, <<-1448.9089, -366.6817, 42.5278>>)
					SET_ENTITY_HEADING(vehs[mvf_trev_heli].id, 280.7440)
				ENDIF
				CLEAR_AREA_OF_PROJECTILES( v_blip_alley_coord, 100 )
				STOP_FIRE_IN_RANGE( v_blip_alley_coord, 100 )
				IF DOES_ENTITY_EXIST( vehPlayerCar )
					CPRINTLN( DEBUG_MISSION, "Player's car exists" )
					IF IS_ENTITY_ON_FIRE( vehPlayerCar )
						STOP_ENTITY_FIRE( vehPlayerCar )
						CPRINTLN( DEBUG_MISSION, "Car on fire, clearing fire" )
					ENDIF
				ENDIF
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT( <<-1475.917236,-400.301208,35.695808>>, <<-1441.553955,-374.867920,41.352779>>, 16.000000,
					<<-1481.2556, -403.6215, 36.5974>>, 55.3672, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				// Clean up vehicles
				REPEAT COUNT_OF(vehs) i
					IF i != ENUM_TO_INT(mvf_trev_heli)
						IF DOES_ENTITY_EXIST(vehs[i].id)
							IF GET_PLAYERS_LAST_VEHICLE() != vehs[i].id
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				bMovedTrevHeli = TRUE
			ENDIF
			
			IF bEnteredAsTrevor
				IF NOT bSwitchEffectPlayed
					IF GET_CUTSCENE_TIME() >= 138734
						ANIMPOSTFX_PLAY("SwitchSceneMichael", 1000, FALSE)
						PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bSwitchEffectPlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael") 
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT) 
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING() 
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()
				REDUCE_AMBIENT_MODELS(FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				Mission_Set_Stage(STAGE_MISSION_PASSED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MISSION_STAGE_MISSION_PASSED()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			Mission_Passed()
		BREAK
	ENDSWITCH
ENDPROC

// Handles the mission flow based on the mission_stage
PROC Mission_Flow()
	
	SWITCH int_to_enum(MISSION_STAGE_FLAG, mission_stage)
		CASE	STAGE_WALK_TO_DAVE					MISSION_STAGE_WALK_TO_DAVE()				BREAK
		CASE 	STAGE_MEETING_CUTSCENE				MISSION_STAGE_MEETING_CUTSCENE()			BREAK
		CASE	STAGE_FIRST_AREA					MISSION_STAGE_FIRST_AREA()					BREAK
		CASE	STAGE_TREVOR_SHOOTS_HELICOPTER		MISSION_STAGE_TREVOR_SHOOTS_HELICOPTER()	BREAK
		CASE	STAGE_TREVOR_SAVES_DAVE				MISSION_STAGE_TREVOR_SAVES_DAVE()			BREAK
		CASE	STAGE_DAVE_BY_FOUNTAIN				MISSION_STAGE_DAVE_AT_FOUNTAIN()			BREAK
		CASE 	STAGE_DAVE_ESCAPES					MISSION_STAGE_DAVE_ESCAPES()				BREAK
		CASE 	STAGE_ESCAPE_MUSEUM					MISSION_STAGE_ESCAPE_MUSEUM()				BREAK
		CASE 	STAGE_VEHICLE_ESCAPE				MISSION_STAGE_VEHICLE_ESCAPE()				BREAK
		CASE	STAGE_END_CUTSCENE					MISSION_STAGE_END_CUTSCENE()				BREAK
		CASE	STAGE_MISSION_PASSED				MISSION_STAGE_MISSION_PASSED()				BREAK
	ENDSWITCH
	
ENDPROC

#IF IS_DEBUG_BUILD
BOOL b_debug_draw_cover_points
//PURPOSE: 
PROC Mission_Debug()
	INT i
	TEXT_LABEL_63 str_debug_string
	MUSEUM_SECTIONS	eDebugSection
	
	FOR i = 0 TO 3
		SWITCH i
			CASE 0
				eDebugSection = eCurrentSection
			BREAK
			CASE 1
				IF eCurrentSection != MUS_NO_AREA
					eDebugSection = sMuseumSections[eCurrentSection].eNextSection
				ELSE
					eDebugSection = MUS_NO_AREA
				ENDIF
			BREAK
			CASE 2
				IF eCurrentSection != MUS_NO_AREA
					eDebugSection = sMuseumSections[eCurrentSection].eAlternativeSection
				ELSE
					eDebugSection = MUS_NO_AREA
				ENDIF
			BREAK
			CASE 3
				eDebugSection = ePinnedFromHere
			BREAK
		ENDSWITCH
		
		str_debug_string = ""
		SWITCH eDebugSection
			CASE MUS_NO_AREA 			str_debug_string += "NO_AREA"			BREAK
			CASE MUS_BALC_START			str_debug_string += "BALC_START"		BREAK
			CASE MUS_BALC_ONE			str_debug_string += "BALC_ONE"			BREAK
			CASE MUS_BALC_TWO			str_debug_string += "BALC_TWO"			BREAK
			CASE MUS_BALC_THREE			str_debug_string += "BALC_THREE"		BREAK
			CASE MUS_BALC_FOUR			str_debug_string += "BALC_FOUR"			BREAK
			CASE MUS_BALC_BRIDGE_BIG	str_debug_string += "BALC_BRIDGE_BIG"	BREAK
			CASE MUS_BALC_BRIDGE_SMALL	str_debug_string += "BALC_BRIDGE_SMALL"	BREAK
			CASE MUS_BALC_END			str_debug_string += "BALC_END"			BREAK
			
			CASE MUS_BALC_TO_GND_START	str_debug_string += "BALC_TO_GND_START" BREAK
			
			CASE MUS_GND_YARD_ONE		str_debug_string += "GND_YARD_ONE"		BREAK
			CASE MUS_GND_YARD_TWO		str_debug_string += "GND_YARD_TWO"		BREAK
			CASE MUS_GND_YARD_THREE		str_debug_string += "GND_YARD_THREE"	BREAK
			CASE MUS_GND_END			str_debug_string += "GND_END"			BREAK
			
			CASE MUS_GND_TO_PL			str_debug_string += "GND_TO_PL"			BREAK
			CASE MUS_GND_PL_END			str_debug_string += "GND_PL_END"		BREAK
		ENDSWITCH
		
		SWITCH i
			CASE 0
				SET_CONTENTS_OF_TEXT_WIDGET(twi_mike_ai_section_current, str_debug_string)
			BREAK
			CASE 1
				SET_CONTENTS_OF_TEXT_WIDGET(twi_mike_ai_section_next, str_debug_string)
			BREAK
			CASE 2
				SET_CONTENTS_OF_TEXT_WIDGET(twi_mike_ai_section_next_alt, str_debug_string)
			BREAK
			CASE 3
				SET_CONTENTS_OF_TEXT_WIDGET(twi_mike_ai_section_pinned, str_debug_string)
			BREAK
		ENDSWITCH
	ENDFOR
	
	str_debug_string = ""
	SWITCH ai_michael
		CASE AI_M_IDLE						str_debug_string += "IDLE"					BREAK
		CASE AI_M_MOVING_FORWARD			str_debug_string += "MOVING FORWARD"		BREAK
		CASE AI_M_MOVING_TO_COVER			str_debug_string += "MOVING TO COVER"		BREAK
		CASE AI_M_MOVING_TO_END				str_debug_string += "MOVING TO END"			BREAK
		CASE AI_M_PINNED_IN_COVER			str_debug_string += "PINNED"				BREAK
		CASE AI_M_WAITING_AT_END			str_debug_string += "WAITING"				BREAK
	ENDSWITCH
	SET_CONTENTS_OF_TEXT_WIDGET(twi_mike_ai_stage, str_debug_string)
	
	str_debug_string = ""
	SWITCH ai_trevor
		CASE AI_T_IDLE						str_debug_string += "IDLE"					BREAK
		CASE AI_T_MOVING_TO_SNIPING_POS		str_debug_string += "MOVING_TO_SNIP_POS"	BREAK
		CASE AI_T_IN_COVER_NO_TARGET		str_debug_string += "COVER(NO TARGET)"		BREAK
		CASE AI_T_IN_COVER_WITH_TARGET		str_debug_string += "COVER(WITH TARGET)"	BREAK
	ENDSWITCH
	SET_CONTENTS_OF_TEXT_WIDGET(twi_trev_ai_stage, str_debug_string)
	
	f_debug_gam_cam_rel_heading		= GET_GAMEPLAY_CAM_RELATIVE_HEADING()
	f_debug_gam_cam_rel_pitch		= GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	f_debug_player_heading			= GET_ENTITY_HEADING(PLAYER_PED_ID())
	
	
// Debug cover points
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
		b_debug_draw_cover_points = !b_debug_draw_cover_points
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(b_debug_draw_cover_points)
	ENDIF
	
	IF b_debug_draw_cover_points
		IF eCurrentSection != MUS_NO_AREA
			str_debug_string = GET_CONTENTS_OF_TEXT_WIDGET(twi_mike_ai_section_current)
		
			i = 0
			REPEAT I_NUM_DEF_SPOTS i
				IF NOT IS_VECTOR_ZERO(sMuseumSections[eCurrentSection].vDefensiveAreas[i])
					TEXT_LABEL_31 str_debug_string_2 = str_debug_string
					str_debug_string_2 += i
				
					DRAW_DEBUG_TEXT_WITH_OFFSET(str_debug_string_2, sMuseumSections[eCurrentSection].vDefensiveAreas[i] + <<0,0,2.25>>, 0,0, 0,0,255,255)
					DRAW_DEBUG_SPHERE(sMuseumSections[eCurrentSection].vDefensiveAreas[i], 2.0, 0, 255, 0, 150)
				ENDIF
			ENDREPEAT
			
			IF NOT IS_VECTOR_ZERO(sMuseumSections[eCurrentSection].vNavToCoord)
				DRAW_DEBUG_SPHERE(sMuseumSections[eCurrentSection].vNavToCoord, 2.0, 0, 255, 0, 150)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
		bDebugEnabled = !bDebugEnabled
	ENDIF
	
	IF bDebugEnabled
		
		// Removed by Kenneth as USE_TU_CHANGES version has additional param.
		//DRAW_MISSION_EVENT_DEBUG_INFO(sEvents)
		
		TEXT_LABEL_63 tlDebug 
		
		tlDebug = "iMocapIG6State: "
		tlDebug += iMocapIG6State
		DRAW_DEBUG_TEXT_2D_IN_SLOT( iDebugInfo, tlDebug )
		
		tlDebug = "iMocapIG7State: "
		tlDebug += iMocapIG7State
		DRAW_DEBUG_TEXT_2D_IN_SLOT( iDebugInfo, tlDebug )
		
		tlDebug = "iMocapMikeOnlyIG7State: "
		tlDebug += iMocapMikeOnlyIG7State
		DRAW_DEBUG_TEXT_2D_IN_SLOT( iDebugInfo, tlDebug )
		
		tlDebug = "bSpawnMikeOnlyIG7: "
		tlDebug += BOOL_TO_INT( bSpawnMikeOnlyIG7 )
		DRAW_DEBUG_TEXT_2D_IN_SLOT( iDebugInfo, tlDebug )
		
		tlDebug = "iMocapIG8State: "
		tlDebug += iMocapIG8State
		DRAW_DEBUG_TEXT_2D_IN_SLOT( iDebugInfo, tlDebug )
		
	ENDIF

	iDebugInfo = 0
	
ENDPROC

#ENDIF

PROC UPDATE_SNIPER_AUDIO_SCENE()
	SWITCH eSniperAudioState
		CASE SSA_INIT
			IF IS_AUDIO_SCENE_ACTIVE("MI_3_SHOOTOUT_SNIPER_SCOPE")
				STOP_AUDIO_SCENE("MI_3_SHOOTOUT_SNIPER_SCOPE")
			ENDIF
			CPRINTLN(DEBUG_MISSION, "UPDATE_SNIPER_AUDIO_SCENE - Going to SSA_NO_SCOPE_ACTIVE")
			eSniperAudioState = SSA_NO_SCOPE_ACTIVE
		BREAK
		
		CASE SSA_NO_SCOPE_ACTIVE
			IF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				START_AUDIO_SCENE("MI_3_SHOOTOUT_SNIPER_SCOPE")
				CPRINTLN(DEBUG_MISSION, "UPDATE_SNIPER_AUDIO_SCENE - In First Person mode, going to SSA_SCOPE_ACTIVE")
				eSniperAudioState = SSA_SCOPE_ACTIVE
			ENDIF
		BREAK
		
		CASE SSA_SCOPE_ACTIVE
			//REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2068260
			
			IF NOT IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				STOP_AUDIO_SCENE("MI_3_SHOOTOUT_SNIPER_SCOPE")
				CPRINTLN(DEBUG_MISSION, "UPDATE_SNIPER_AUDIO_SCENE - put down the sniper, going to SSA_NO_SCOPE_ACTIVE")
				eSniperAudioState = SSA_NO_SCOPE_ACTIVE
			ENDIF
		BREAK
		
		CASE SSA_DONE
		BREAK
	ENDSWITCH
ENDPROC


/*
									  ______  _______  ______   _  ______  _______    _       _______  _______  ______  
									 / _____)(_______)(_____ \ | |(_____ \(_______)  (_)     (_______)(_______)(_____ \ 
									( (____   _        _____) )| | _____) )   _       _       _     _  _     _  _____) )
									 \____ \ | |      |  __  / | ||  ____/   | |     | |     | |   | || |   | ||  ____/ 
									 _____) )| |_____ | |  \ \ | || |        | |     | |_____| |___| || |___| || |      
									(______/  \______)|_|   |_||_||_|        |_|     |_______)\_____/  \_____/ |_|      
*/

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Failed(mff_default)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Do everything to prepare the mission
	Mission_Setup()
	
	// Main loop
	WHILE (TRUE)
		
		IF iBlockReplayCameraTimer > GET_GAME_TIMER()	// For B*2226230
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	// To disable camera movement during a replay to stop players seeing undesireable content, i.e. pops and warps.
		ENDIF
		
		UPDATE_SNIPER_AUDIO_SCENE()						// We need to play a certain audio scene ANY time a sniper scope is up.
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Mission_Stage_Management()						// manage any stage switching stuff
		Mission_Stage_Skip()							// Manages skips; z,j,p,replays
		Mission_Checks()								// Mission scenario checks; fails, disables running at certain points, etc
		Manage_Cameras()								// manages custom camera interpolation (to work with slowing timescale)
		IF NOT bDoSkip
			UPDATE_MISSION_EVENTS(sEvents)				// Manages the events
			Mission_Flow()								// process the mission flow
		ENDIF
		
		#IF IS_DEBUG_BUILD
			Mission_Debug()
		#ENDIF
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheWrapUp")
	
		WAIT(0)
		
	ENDWHILE
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
	
