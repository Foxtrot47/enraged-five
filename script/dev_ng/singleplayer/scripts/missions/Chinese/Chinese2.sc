
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "flow_public_core_override.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "selector_public.sch"
using "dialogue_public.sch"
USING "commands_audio.sch"
USING "commands_graphics.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "replay_public.sch"
USING "select_mission_stage.sch"
USING "mission_stat_public.sch"
USING "clearMissionArea.sch"
USING "locates_public.sch"
USING "net_include.sch"
using "rGeneral_include.sch"
using "usefulCommands.sch"
USING "building_control_public.sch"
USING "script_blips.sch"
USING "taxi_functions.sch"
USING "commands_recording.sch"

string remlastConv

USING "ped_player_reactions.sch"


//USING "AI_Sweep.sch"
#IF IS_DEBUG_BUILD
USING "scripted_cam_editor_public.sch"
#ENDIF
vehicle_index cutscene_player_vehicle

// ================================ preset flag values ======================
CONST_INT TAO_EVENT_ANIM_PLAYING 900

CONST_INT MAX_TRAIL_POINTS 61
USING "gas_trails.sch"

// ================================================ DATA FOR PEDS ===============================================
CONST_INT MAX_PEDS 26

int iNextPedVariation

CONST_INT ped_taos_translator 10
CONST_INT ped_tao 11
CONST_INT ped_janet 12
CONST_INT ped_oldMan1 13
CONST_INT ped_oldMan2 14

vector vehicleSpeed

enum enumClenup
	CLEANUP_PASS,
	CLEANUP_FAIL,
	CLEANUP_FAIL_DEATH, //to make sure peds are not deleted on death fail
	CLEANUP_SKIP
endenum

ENUM enumAction
	action_none,							// 0
	action_spawn,							// 1
	action_roll_barrels,					// 2
	action_roll_barrels_at_front_of_house,
	action_shoot_cans,						// 3
	action_balcony_guard_patrol,			// 4
	action_balcony_guard_chat,				// 5
	action_chatting,						// 6
	action_guarding,						// 7
	action_patrol,							// 8
	action_respond_to_dead_ped,				// 9
	action_respond_to_bullet,				// 10
	action_alert_buddies,					// 11
	action_looking_about,					// 12
	action_smoke,							// 13
	action_binoculars,
	action_drinking,
	action_pissing,
	action_move_inside_house,				
	action_chicken,							// 14
	action_respond_to_nearby_alerted_ped,	// 15
	action_respond_to_adjacent_alerted_ped,	// 16
	action_alerted,							// 17
	action_advance_on_hill,					//18
	action_wait_on_player_advance_to_house,	//19
	action_sit,								//21
	action_combat,
	action_flee,
	action_cooking,
	action_drugs,
	action_meth_lab,
	action_meth_lab_respond_to_player,
	action_hum,
	action_get_in_car
ENDENUM

ENUM enumRole
	role_chicken,
	role_guard_outside,
	role_guard_house,
	role_worker,
	role_cook,
	role_innocent
ENDENUM

enum enumPlacement	
	pos_balcony_far_left,
	pos_balcony_doorway,
	pos_balcony_veranda_walk,
	pos_porch,
	pos_barrels_1,
	pos_barrels_2,
	pos_garden_chat_1,
	pos_garden_chat_2,
	pos_shooting_bottle_1,
	pos_shooting_bottle_2,
	pos_inside_right_window,
	pos_inside_utility_room,
	pos_inside_seated1,
	pos_inside_seated2,
	pos_inside_seated3,
	pos_inside_cards1,
	
	pos_inside_cards2,
	pos_inside_cards3,
	pos_inside_kitchen1,
	pos_inside_kitchen2,
	pos_inside_bystairs1,
	pos_inside_bystairs2,
	pos_innocent_chef,
	pos_innocent_drugden,
	pos_emergency_attacker,
	pos_none
ENDENUM







struct structPedData
	ped_index id
	enumRole role
	enumPlacement placement
	enumAction action
	enumAction lastAction
	int actionFlag = 0
	vector vpos, vPosB
	float floatA, floatB
	int intA, intB, pedOfInterest
	bool nearbyreactingPed
	WEAPON_TYPE weapon = DUMMY_MODEL_FOR_SCRIPT
	BLIP_INDEX blip
	OBJECT_INDEX obj
	bool sniped
	bool alerted
	bool registeredKilled
	REL_GROUP_HASH rel
	AI_BLIP_STRUCT aiBlip
//	enum_state myState = STATE_SPAWN
ENDSTRUCT

structPedData zPED[max_peds]

int iStagger




//relationship groups
REL_GROUP_HASH HASH_FRIEND_GROUP
REL_GROUP_HASH HASH_FOE_GROUP
REL_GROUP_HASH HASH_IGNORE_GROUP
REL_GROUP_HASH taorelgroup


// ped reactions
//ped reaction stuff
structCheckState pedReactionState[MAX_PEDS]
diaData diAlertAllSeen, diAlertAllUnseen, diSeePlayer, diHearPlayer,diHearPlayerAgain, diLostPlayer, diBullet, diDeadBody, diExplosion, diFoundPlayer
vector vDefAreas[21]

string convBlock = "CHI2AUD"


// final cutscene data


	bool editCutscene
	bool resetCutscene
	bool cutsceneRunning
	bool bOutputCut
	
	struct structCamShot
		vector camStartPos,camStartRot
		vector camEndPos,camEndRot
		float camDuration,totalDuration
		float camStartFOV,camEndFOV
		int camPanType
		int flameNode = -1
	endstruct
	
	struct StructcutEvent
		bool triggered
		int triggerShot
		float triggerTime
		string name
		int flag
		int timer
	endstruct
	
	StructcutEvent CutEvent[6]
	structCamShot camShot[4]
	
	float cutDurationTime
	int iCurrentCamShot
	bool bSetCamA[4],bSetCamB[4]
	
	#if is_debug_build
	WIDGET_GROUP_ID cutsceneWidget
	#endif

	vector basementFlames[13]
	int basementFlame
	
// =================================================== vehicles ====================================================
vehicle_index vehPlayer
VEHICLE_INDEX BarrelVan
VEHICLE_INDEX vehBlazer
VEHICLE_INDEX vehOneil

//navmesh bloc
INT navblockA

// ============================================== MISSION STAGES ====================================================

CONST_INT MAX_SKIP_MENU_LENGTH 11

ENUM mission_stage_flag 
	init_mission=0,
	intro_cut,
	drive_to_farm,
	farm_cutscene,
	snipe_from_hill,
	get_to_house,
	inside_house,
	do_petrol_trail,
	explosion_cut,
	end_mission,
	missionPassed,
	stage_null
ENDENUM

mission_stage_flag missionStage
int stageFlag


#IF IS_DEBUG_BUILD
MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]   
#ENDIF

string sFailReason
bool bFailMission

// ====================================================== Data for events ==============================================
enum EnumEvents
	events_resolve_vehicles_at_start,
	events_give_sniper,
	events_trevor_phonecall,
	events_trevor_rants,
	events_stop_and_leave_vehicle,
	events_spawn_house,
	events_reset_house_rayfire,
	events_release_bottle_shooting_anims,
	events_remove_vehicles_and_peds,
	events_control_dispatch,
	events_petrol_help,
	events_explode_petrol_can,
	events_house_explodes_rayfire,
	events_play_audio_stream,
	events_Trevor_walks_in_final_cutscene,
	events_kill_alerted_ped_help,
	events_check_petrol_trail,
	events_check_player_out_front,
	events_check_trail_burning_safe,
	events_check_trail_burning_fail,
	events_detach_barrels_from_peds,
	events_never_do_locates_skips,
	events_pin_interior,
	events_spawn_bottles,
	events_unfreeze_pallet,
	events_continuous_stats_tracking,
	events_animate_bar,
	events_force_farm_doors_drawing,
	events_set_Radio,
	events_fails
ENDENUM

struct structEvents
	EnumEvents event
	bool active
	int flag
	bool persists
	bool boolA
	int intA
ENDSTRUCT

CONST_INT max_events 20
CONST_INT CLEANUP -1

structEvents events[max_events]



// ====================================================== DIALOGUE STUFF ==========================================
enum enumConvProgress
	CONV_EMPTY,
	CONV_SEEK,
	CONV_PANC,
	CONV_PANC2
ENDENUM

//enumConvProgress LAST_CONVERSATION = CONV_EMPTY
//INT CONVERSATION_GAP
//int random_lines_played

//enumComments  nextSweepCommentToPlay = COMMENT_NONE

rel_group_hash relGroupBar

// ======================================================== cutscenes =============================================
CAMERA_INDEX cam1
int iCamSync
float fShakeValue
int cutChangeTime
int safeSkipTime
ped_index tempPed
ped_index ped_oneil1, ped_oneil2
//int sceneBOOM
int iCamFlag

bool bSneakyPass
bool bCutsceneWasSkipped

enum ENUM_CUTSCENE_FIRST_SHOT
	SHOT_NONE,
	SHOT_BASEMENT,
	SHOT_HOUSE_EXPLOSION,
	SHOT_NORMAL
endenum

ENUM_CUTSCENE_FIRST_SHOT CUTSCENE_FIRST_SHOT = SHOT_NONE

// =================================================== Debug variables ==========================================
#if IS_DEBUG_BUILD
bool bExplodeHouseLoop,bExplodeHouseLoopShort
bool bShowActionsA,bShowActionsB,bShowEvents,bSkipDrive,bShowFocus
bool bRecordPedData
int iRecordedPed
bool bKillOutsidePeds
bool bRecordPed
int iPedToRecord
bool bAllowShitskip
bool bScreenConditions,bScreenActionsA,bScreenInstructions,bSshowDialogue
#ENDIF

// =========================================================== Fuel can ==========================================



int gasInCan
PICKUP_INDEX health_pickup
PICKUP_INDEX piPickups[5]


// ===================================== shoot cans ===============================================
object_index oCans[10]



// ============================================================ fire trail ======================================

int iFireIgniteSound
vector vIgnitePos

// ========================================================= Rayfire
RAYFIRE_INDEX rfThisRayfire
bool bRayfireTriggered
float fSlowMoPhase

// ===================================================== All other variables =========================================

PICKUP_INDEX pickup_gas
int iRadSound = -1
BLIP_INDEX missionBlip
//INTERIOR_INSTANCE_INDEX houseInterior
INTERIOR_INSTANCE_INDEX int_farm
OBJECT_INDEX oGasCan
object_index obja,objb
object_index obj_cellphone

LOCATES_HEADER_DATA locatesData
structPedsForConversation dChinese2
int iSyncStandingLoop,iSyncStandUp,iSyncCower,iStandAlternative,iGrabGun

object_index objShotgun
bool bBlockShooterDialogue
bool bcutscenePLaying
bool bForceAnimTimeA,bForceAnimTimeB, bPlayerOnHill
#if IS_DEBUG_BUILD
WIDGET_GROUP_ID widget_debug
#ENDIF

int iNavBlock=-1

//int failReason =-1



int iBarrels1,iBarrels2
int thisSnipeCount
int iRandomCounter //counter for random rant lines from Trevor



vector cellar_explosions[9]

// ==================================================== define actions ====================================================
CONST_INT MAX_ACTIONS 20
CONST_INT MAX_CONDITIONS 27
CONST_INT MAX_INSTRUCTIONS 5
CONST_INT MAX_DIALOGUES 17

ENUM enumconditions
	COND_NULL,	
	
	COND_GO_TO_FARM_START,	
	COND_INITIAL_INSTRUCTIONS_PLAYED,
	COND_PLAYER_IN_ANY_CAR,
	COND_GO_TO_FARM_END,
	
	COND_START_FARM_ASSAULT,
	COND_BULLET_IMPACTED_BY_BARRELS,
	COND_SHOOTING_PED_DEAD,
	COND_ENEMY_PED_REACTING,
	COND_ALL_ENEMY_ALERTED,	
	COND_ALL_OUTSIDE_ENEMY_DEAD,
	COND_PLAYER_IN_METH_LAB,
	COND_PLAYER_IN_HOUSE,
	COND_PLAYER_DOWNSTAIRS,
	COND_PLAYER_APPROACHING_HOUSE,
	COND_BASEMENT_PED_DEAD_IN_BASEMENT,
	COND_ATTACKERS_ALL_DEAD,
	COND_TREVOR_NEAR_BASEMENT,
	COND_TREVOR_NEAR_STAIRS,
	COND_TREVOR_AT_STAIRS,
	COND_PISSING_DIALOGUE_CAN_PLAY,
	COND_TREVOR_SEEN,
	COND_RESPOND_TO_BULLET,
	COND_ONE_ATTACKER_LEFT_OUTSIDE,
	COND_FAIL_FOR_LEAVING_FARM,
	COND_BARREL_ROLL_PEDS_IN_VAN,
	COND_CARD_PLAYING_PEDS_STILL_NEAR_TABLE,
	COND_BASEMENT_PEDS_LEAVING_AND_OBSERVED,
	
	COND_START_PETROL_TRAIL,
	
	COND_PLAYER_HAS_PETROL_CAN,	
	COND_JERRY_CAN_DESTROYED, //allows this  condition to span across both mission stages	
	
	COND_END_FARM_ASSAULT,	
	
	COND_PLAYER_OUT_FRONT,
	COND_OUT_OF_GAS,
	COND_GAS_TRAIL_COMPLETE,
	COND_LAST_BLIP_POURED_ON,	
	COND_PLAYER_POURING_GAS,
	COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE,
	COND_GAS_TRAIL_REACHED_DOORWAY,
	COND_GAS_TRAIL_IGNITED_INCORRECTLY,
	COND_TREVOR_INSIDE_HOUSE,
	COND_GAS_IGNITED_IN_HOUSE_WILL_BURN_OK,
	COND_40M_FARM_WARNING,
	COND_60M_FARM_FAIL,
	COND_EXPLOSION_IN_BASEMENT,
	COND_END_PETROL_TRAIL,
	
	COND_FARM_LEAVE_START,
	COND_GET_WANTED_LEVEL,
	COND_PLAYER_300_FROM_FARM,
	COND_PLAYER_500_FROM_FARM,
	COND_GOT_WANTED_LEVEL,
	COND_BUILDING_STATE_SWAPPED,
	COND_FARM_LEAVE_END
	
ENDENUM

ENUM enumInstructions
	INS_NULL,
	INS_SPECIAL_ABILITY_BOOST,
	INS_DESTROY_METH_LAB,
	ins_enemy_reacting,
	INS_SPECIAL_ABILITY,
	ins_all_enemy_reacting,
	INS_QUICK_KILL_HELP,
	INS_PICK_UP_GAS_CAN,
	INS_USING_GAS_CAN,
	ins_missed_a_bit,
	ins_dont_abandon_farm,
	ins_fast_stealth,
	INS_GET_CLEAR_OF_FARM,
	INS_LOSE_WANTED_LEVEL
ENDENUM


enum enumDialogue
	DIA_NULL,
	//drive to meet
	dia_whatnow,
	dia_trevor_rage_comments,
	dia_stop_trevor_getting_to_basement,
	dia_cook_sees_ped_die,
	dia_dont_let_him_down,
	dia_hes_in_meth_lab,
	dia_trevor_see_basement_peds_leave,
	dia_pissing_guy,
	dia_trevor_spotted_outside,
	dia_find_trevor,
	dia_attacking_trevor_outside,
	dia_attacking_trevor_inside,
	DIA_RESPOND_TO_ALERT,
	dia_trevor_seen_inside_house,
	DIA_RESPOND_TO_BULLET,
	dia_last_man_standing,
	dia_trevor_approaches_house,
	dia_shooting_Barrels,
	Dia_TREVOR_SAYS_HARD_WAY,
	Dia_stay_in_house,
	DIA_PICKED_UP_JERRY_CAN,
	DIA_POURING_PETROL,
	DIA_LIGHT_FUEL,
	DIA_EXPLODE_HOUSE,
	DIA_FUEL_BURN,
	DIA_MISSED_A_BIT,
	DIA_BOOM
ENDENUM

enum enumActions
	act_null,
	act_get_stalls,
	ACT_AUDIO_SCENE_DRIVE_TO_FARMHOUSE,
	ACT_REMOVE_BAR_PEDS,
	ACT_LOAD_FRUSTROM,
	ACT_LOAD_SOUNDSET,	
	ACT_MAKE_BAR_PEDS_FLEE,
	ACT_BAR_PEDS_LOOK_AT_TREVOR,
	ACT_SLOW_DOWN_PLAYER_IN_CHOPPER,
	ACT_BLEND_OUT_OF_TREVOR_RAGE,
	ACT_LOAD_END_CUT_FRUSTROM,
	ACT_BOMB_IN_BASEMENT,
	act_shooting_peds_witness_impact_at_barrels,
	act_stat_double_kill,
	act_MUSIC_start_snipe,
	act_MUSIC_approach_house_or_alert,
	act_create_petrol_can_with_blip,
	act_stage_prep,
	act_snipe_cams,
	act_remove_initial_assets,
	act_outside_peds_flee,
	act_remove_upstairs_peds,
	act_MUSIC_enter_house,
	act_remaining_peds_flee,
	act_window_ped_walks_inside,
	act_put_player_in_action_mode,
	act_end_stealth_audio_Scene,
	act_enemy_alerted_audio_Scene,
	act_end_enemy_alerted_audio_Scene,
	act_modify_ped_seeing_range,
	act_spawn_blazer,
	ACT_PREP_PETROL_POUR,
	ACT_CONTROL_PETROL_BLIPS,
	ACT_PETROL_ASSIST_ROUTE,
	ACT_FUDGE_RADAR_ZOOM,
	ACT_MUSIC_PICKS_UP_JERRY_CAM,
	ACT_MUSIC_TRAIL_IGNITED,
	ACT_AUDIO_SCENE_WITH_CAN,
	ACT_PREP_EXPLOSION_AUDIO,
	act_remove_barrel_peds,
	act_player_got_to_basement_sneakily,
	ACT_ANIMATE_CHINESE,
	ACT_RADIO_SOUND,
	act_remove_van_peds,
	act_audio_scene_in_house,
	act_reset_bottle_shooting_ped_accuracy,
	act_basement_ped,
	ACT_WHIMPERING,
	ACT_GET_WANTED_LEVEL,
	ACT_CHANGE_BUILDING_STATE,
	ACT_STAT_POUR_GAS,
	ACT_FUDGE_GAS_AMMO,
	ACT_STOP_MUSIC,
	ACT_FIRE_AUDIO,
	ACT_ENABLE_TAXI,
	ACT_RESET_STUFF,
	ACT_REMOVE_SOME_ASSETS	
endenum	

ENUM enumFails
	FAIL_OUT_OF_GAS,
	FAIL_GAS_TRAIL_IGNITED_EARLY,
	FAIL_GAS_TRAIL_NOT_COMPLETE,
	fail_kill_cheng_or_translator,
	FAIL_ABANDONED_FARM,
	FAIL_JERRY_CAN_DESTROYED
ENDENUM

ENUM enumACTIONplayout
	PLAYOUT_ON_TRIGGER,
	PLAYOUT_ON_COND,
	SKIP_SET_TO_COMPLETE
endenum

STRUCT structactions
	enumActions action = ACT_NULL
	bool active
	bool ongoing
	bool completed
	bool needsCleanup
	bool trackCondition
	int flag
	int intA,intB
	float floatA
ENDSTRUCT

structactions actions[MAX_ACTIONS]

enum andOrEnum
	cFORCEtrue,
	cIGNORE,
	cIF,
	cIFnot,
	cAND,
	cAndNOT,
	cOR,
	cWasTrueNowFalse,
	cORbracket
endenum
bool conditionsTrue
bool bracketOpen

struct structConditions
	enumconditions condition = COND_NULL
	bool active
	bool returns
	bool wasTrue
	int flag
	int intA,intB
	float floatA
endstruct

structConditions conditions[MAX_CONDITIONS]

struct structInstructions
	enumInstructions ins = INS_NULL
	bool bCompleted
	int flag
	int intA
	#if IS_DEBUG_BUILD bool CONDTRUE #endif
endstruct

structInstructions instr[MAX_INSTRUCTIONS]



struct structDialogue
	enumDialogue dial = DIA_NULL
	bool bCompleted
	bool bStarted
	int flag
	int intA,intB,intC
	#if IS_DEBUG_BUILD bool condTrue #endif
endstruct

structDialogue dia[MAX_DIALOGUES]

struct convStoreStruct
	int speakerNo
	ped_index pedIndex
	string speakerLabel
endstruct
convStoreStruct convStore[4]
bool bLastConvoWithoutSubtitles
TEXT_LABEL_23 restartSubtitleLine,restartSubtitleRoot

PROC SAFEWAIT(int waitDebug,bool bAllowTerminateThread=TRUE)
	waitDebug=waitDebug
	bAllowTerminateThread=bAllowTerminateThread
	#if IS_DEBUG_BUILD
	
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		IF bAllowTerminateThread
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDIF
	#endif
	//cprintln(debug_trevor3,"wait: ",waitDebug)
	WAIT(0)
ENDPROC

FUNC BOOL IS_VEHICLE_VALID_AND_DRIVEABLE(vehicle_INDEX vehID)
	IF DOES_ENTITY_EXIST(vehID)
		IF IS_VEHICLE_DRIVEABLE(vehID)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ==================================================== Prep peds ==================================================

proc INIT_REL_GROUPS()
	ADD_RELATIONSHIP_GROUP("MYFRIEND",HASH_FRIEND_GROUP)
	ADD_RELATIONSHIP_GROUP("FOE",HASH_FOE_GROUP)	
	ADD_RELATIONSHIP_GROUP("IGNORE",HASH_IGNORE_GROUP)
	ADD_RELATIONSHIP_GROUP("BARpeds", relGroupBar)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE,relGroupBar,RELGROUPHASH_PLAYER)	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,GET_PED_RELATIONSHIP_GROUP_HASH(player_ped_id()),HASH_FOE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,HASH_FOE_GROUP,GET_PED_RELATIONSHIP_GROUP_HASH(player_ped_id()))
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_FRIEND_GROUP,HASH_IGNORE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_FOE_GROUP,HASH_IGNORE_GROUP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_IGNORE_GROUP,GET_PED_RELATIONSHIP_GROUP_HASH(player_ped_id()))
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,HASH_IGNORE_GROUP,HASH_FOE_GROUP)
	
	
	
	
	
ENDPROC

proc prep_ped(enumRole pedRole,enumAction pedAction,enumPlacement placement)
	int iPos = ENUM_TO_INT(placement)
	zPED[iPos].role = pedRole
	zPED[iPos].action = pedAction
	zPED[iPos].placement = placement
	zPED[iPos].actionFlag = 0	
ENDPROC

// shooting cans
FUNC BOOL FIND_CAN_TO_HIT(int &iCanToHit)

	iCanToHit = 0
	
	int iCount
	iCount = 0
	iCanToHit = GET_RANDOM_INT_IN_RANGE(0,10)
	bool bFound
	bFound=FALSE
	
	WHILE NOT bFound
		IF DOES_ENTITY_EXIST(ocans[iCanToHit])
		AND IS_ENTITY_IN_ANGLED_AREA( ocans[iCanToHit], <<2495.017090,4970.170410,44.218082>>, <<2493.500732,4971.556152,45.402481>>, 1.375000)
			RETURN TRUE
		ELSE
			iCanToHit++
			iCount++
			IF iCanToHit = COUNT_OF(oCans)
				iCanToHit = 0
			ENDIF
		ENDIF
		IF iCount = 10													
			RETURN FALSE
		ENDIF
	ENDWHILE
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PRESSING_A_CONTROL_BUTTON()
	IF GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_MOVE_LR) != 127
	OR GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_MOVE_UD) != 127
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_ATTACK)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_AIM)
	OR GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_LOOK_UD) != 127
	OR GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_LOOK_LR) != 127
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// ========================================================= FUEL TRAIL =======================================


PROC REMOVE_DEAD_PEDS_ON_TRAIL_ROUTE()
	INT i, j
	vector vpos

	REPEAT COUNT_OF(gasTrail) i
		IF NOT ARE_VECTORS_EQUAL(gasTrail[i].coord, <<0.0, 0.0, 0.0>>)
			STOP_FIRE_IN_RANGE(gasTrail[i].coord,4.0)
			clear_Area(gasTrail[i].coord,2.0,true)
			REPEAT COUNT_OF(zped) j
				IF DOES_ENTITY_EXIST(zped[j].id)
					vPos = GET_ENTITY_COORDS(zped[j].id, FALSE)
					IF VDIST2(vPos, gasTrail[i].coord) < 2.0
					AND vPos.z > 43.9
						
					//	IF NOT IS_ENTITY_ON_SCREEN(zped[j].id)						
						//	IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zped[j].id) > 10
								DELETE_PED(zped[j].id)
						//	ENDIF
					//	ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
ENDPROC

PROC PURGE_CONVERSATIONS(bool letConversationFinish)
	bLastConvoWithoutSubtitles = FALSE
	IF letConversationFinish
		KILL_FACE_TO_FACE_CONVERSATION()
	ELSe
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
ENDPROC

///INTERIOR GUBBINS
///    bool
INTERIOR_INSTANCE_INDEX pinnedInterior
bool bPinningInterior
PROC PIN_INTERIOR(vector vInterior)
	pinnedInterior = GET_INTERIOR_AT_COORDS(vInterior)
	bPinningInterior = TRUE
ENDPROC

PROC INTERIOR_PINNING()
	IF bPinningInterior
		IF IS_INTERIOR_READY(pinnedInterior)
			PIN_INTERIOR_IN_MEMORY(pinnedInterior)
			bPinningInterior = FALSE
		ENDIF
	ENDIF
ENDPROC

// ===================================== music ========================================

// music
bool bNewMusicRequest,bRequiresPreload,bPreloadRequest
string sMusicToPlay,sPreloadMusic

proc playMusic(string musicToPlay, bool bPreload, string musicToLoadNext)
	bNewMusicRequest = TRUE
	bRequiresPreload = bPreload
	sMusicToPlay = musictoPlay
	sPreloadMusic = musicToLoadNext
endproc

FUNC BOOL control_music()

	If bNewMusicRequest
		If bRequiresPreload
			////cprintln(debug_Trevor3,"Want to play ",sMusicToPlay," with preload")
			IF PREPARE_MUSIC_EVENT(sMusicToPlay)
				IF TRIGGER_MUSIC_EVENT(sMusicToPlay)
					bNewMusicRequest = FALSE
					bRequiresPreload = FALSE
					RETURN TRUE
				ENDIF
			ENDIf
		ELSE
			////cprintln(debug_Trevor3,"Want to play ",sMusicToPlay)
			IF TRIGGER_MUSIC_EVENT(sMusicToPlay)				
				bNewMusicRequest = FALSE
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		If bPreloadRequest
			IF PREPARE_MUSIC_EVENT(sPreloadMusic)
				bPreloadRequest=FALSE
			ENDIf
		ENDIF
	ENDIF
	return false
endfunc



// ======================================================== MISC PROCS ==========================================

PROC FAIL_MISSION(string thisFailReason)
	sFailReason = thisFailReason
	bFailMission = TRUE
endproc

func int findSeenDeadPed(int iPedLooking)
	int ie
	float fHeading
	int iClosest = -1
	float fClosestDistance = 999999.0
	float fDistance
	REPEAT COUNT_OF(zPED) ie
		if does_entity_exist(zPED[ie].id)
			if IS_PED_INJURED(zPED[ie].id)
				fHeading = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(zPED[iPedLooking].id,FALSE),GET_ENTITY_COORDS(zPED[ie].id,FALSE))
				if ABSF(fHeading-GET_ENTITY_HEADING(zPED[iPedLooking].id)) < 90.0
					fDistance = GET_DISTANCE_BETWEEN_ENTITIES(zPED[iPedLooking].id,zPED[ie].id)
					if fDistance < fClosestDistance
						fClosestDistance = fDistance
						iClosest = ie
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	if iClosest != -1
		return iClosest
	ENDIF
	return -1
ENDFUNC

proc set_event_int(EnumEvents eventToAdd, int iValue)
	int iE
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			if events[iE].event = eventToAdd
				events[iE].intA = iValue
				iE = COUNT_OF(events)
			ENDIF
		ENDIF
	ENDREPEAT
endproc

FUNC bool set_event_flag(enumEvents event, int iFlagValue)
	int i
	for i = 0 to MAX_EVENTS-1
		if events[i].event = event
			if events[i].active = true
				events[i].flag = iFlagValue
				RETURN TRUE
			ENDIF						
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

func bool add_event(EnumEvents eventToAdd, bool persists_stage_skipping=FALSE)
	int iE
	
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			if events[iE].event = eventToAdd
				return FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(events) iE
		if events[iE].active = FALSE
			events[iE].active = TRUE
			events[iE].event = eventToAdd
			events[iE].flag = 0
			events[iE].persists = persists_stage_skipping
			events[iE].boolA = FALSE
			events[iE].intA = 0
			return TRUE
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("Ran out of script events. Increase MAX_EVENTS")
	return FALSE
ENDFUNC

FUNC BOOL kill_event(EnumEvents eventToKill, bool bLetCleanup = TRUE)
	int iE
	REPEAT COUNT_OF(events) iE
		IF events[iE].event = eventToKill
			events[iE].flag = -1
			IF NOT bLetCleanup
				events[iE].active = FALSE				
			ENDIF
			return TRUE
		ENDIF
	ENDREPEAT
	return FALSE
endFUNC

func bool is_event_active(EnumEvents eventToCheck)
	int iE
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			if events[iE].event = eventToCheck
				return TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	return FALSE
endfunc

func int GET_EVENT_FLAG(EnumEvents eventToCheck)
	int iE
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			if events[iE].event = eventToCheck
				return events[iE].flag
			ENDIF
		ENDIF
	ENDREPEAT
	return -1
endfunc

FUNC STRING GET_ROLE_STRING(int iPEd)
	switch zPED[iPed].role
		case role_chicken RETURN("role_chicken") BREAK
		case role_guard_outside RETURN("role_guard_outside") BREAK
		case role_guard_house RETURN("role_guard_house") BREAK
		case role_worker RETURN("role_worker") BREAK
		case role_cook RETURN("role_cook") BREAK
		case role_innocent  RETURN("role_innocent") BREAK
	endswitch
	return ""
ENDFUNC

FUNC STRING GET_PLACEMENT_STRING(int iPed)
	switch zPED[iPed].placement
		case pos_none RETURN("pos_none") BREAK
		case pos_balcony_far_left RETURN "pos_balcony_far_left" BREAK
		case pos_balcony_doorway RETURN "pos_balcony_doorway" BREAK
		case pos_balcony_veranda_walk RETURN "pos_balcony_veranda_walk" BREAK
		case pos_porch RETURN "pos_porch" BREAK
		case pos_barrels_1 RETURN "pos_barrels_1" BREAK
		case pos_barrels_2 RETURN "pos_barrels_2" BREAK
		case pos_garden_chat_1 RETURN "pos_garden_chat_1" BREAK
		case pos_garden_chat_2 RETURN "pos_garden_chat_2" BREAK
		case pos_shooting_bottle_1 RETURN "pos_shooting_bottle_1" BREAK
		case pos_shooting_bottle_2 RETURN "pos_shooting_bottle_2" BREAK
		case pos_inside_right_window RETURN "pos_inside_right_window" BREAK		
		case pos_inside_utility_room RETURN "pos_inside_utility_room" BREAK
		case pos_inside_seated1 RETURN "pos_inside_seated1" BREAK
		case pos_inside_seated2 RETURN "pos_inside_seated2" BREAK
		case pos_inside_seated3 RETURN "pos_inside_seated3" BREAK
		case pos_inside_cards1 RETURN "pos_inside_cards1" BREAK
		case pos_inside_cards2 RETURN "pos_inside_cards2" BREAK
		case pos_inside_cards3 RETURN "pos_inside_cards3" BREAK
		case pos_inside_kitchen1 RETURN "pos_inside_kitchen1" BREAK
		case pos_inside_kitchen2 RETURN "pos_inside_kitchen2" BREAK
		case pos_inside_bystairs1 RETURN "pos_inside_bystairs1" BREAK
		case pos_inside_bystairs2 RETURN "pos_inside_bystairs2" BREAK	
		case pos_innocent_chef RETURN "pos_innocent_chef" BREAK
		case pos_innocent_drugden RETURN "pos_innocent_drugden" BREAK				
	ENDSWITCH
	return ""
ENDFUNC

FUNC STRING GET_THIS_STATE_STRING(int iPED)
	SWITCH pedReactionState[iPED].State
		CASE STATE_AMBIENT
			RETURN("STATE_AMBIENT")
		BREAK
		CASE STATE_COMBAT
			RETURN("STATE_COMBAT")
		BREAK
		CASE STATE_REACT
			RETURN("STATE_REACT")
		BREAK
		CASE STATE_SEEK
			RETURN("STATE_SEEK")
		BREAK
		CASE STATE_SPAWN
			RETURN("STATE_SPAWN")
		BREAK
	ENDSWITCH
	return("")
ENDFUNC


FUNC STRING GET_ACTION_STRING(int iPED)
	switch zPED[iPed].action 
		case action_none RETURN "action_none" BREAK							// 0
		case action_spawn RETURN "action_spawn" BREAK							// 1
		case action_roll_barrels RETURN "action_roll_barrels" BREAK					// 2
		case action_shoot_cans RETURN "action_shoot_cans" BREAK						// 3
		case action_balcony_guard_patrol RETURN "action_balcony_guard_patrol" BREAK			// 4
		case action_balcony_guard_chat RETURN "action_balcony_guard_chat" BREAK				// 5
		case action_chatting RETURN "action_chatting" BREAK						// 6
		case action_guarding RETURN "action_guarding" BREAK						// 7
		case action_patrol RETURN "action_patrol" BREAK							// 8
		case action_respond_to_dead_ped RETURN "action_respond_to_dead_ped" BREAK				// 9
		case action_respond_to_bullet RETURN "action_respond_to_bullet" BREAK				// 10
		case action_alert_buddies RETURN "action_alert_buddies" BREAK					// 11
		case action_looking_about RETURN "action_looking_about" BREAK					// 12
		case action_smoke RETURN "action_smoke" BREAK							// 13
		case action_move_inside_house RETURN "action_move_inside_house" BREAK
		case action_chicken RETURN "action_chicken" BREAK							// 14
		case action_respond_to_nearby_alerted_ped RETURN "action_respond_to_nearby_alerted_ped" BREAK	// 15
		case action_respond_to_adjacent_alerted_ped RETURN "action_respond_to_adjacent_alerted_ped" BREAK	// 16
		case action_alerted RETURN "action_alerted" BREAK							// 17
		case action_advance_on_hill RETURN "action_advance_on_hill" BREAK					//18
		case action_wait_on_player_advance_to_house RETURN "action_wait_on_player_advance_to_house" BREAK	//19
		case action_sit RETURN "action_sit" BREAK								//21
		case action_combat RETURN "action_combat" BREAK
		case action_flee RETURN "action_flee" BREAK
		case action_meth_lab RETURN "action_meth_lab" BREAK
		case action_meth_lab_respond_to_player RETURN "action_meth_lab_respond_to_player" BREAK 
		CASE action_hum RETURN "action_hum" BREAK
		case action_get_in_car RETURN "action_get_in_carm" BREAK
	endswitch
	return ""
endfunc

FUNC STRING GET_THIS_EVENT_STRING(int iEvent)
	switch events[iEvent].event
		case events_resolve_vehicles_at_start RETURN "events_resolve_vehicles_at_start" BREAK
		case events_give_sniper RETURN "events_give_sniper" BREAK
		case events_trevor_phonecall RETURN "events_trevor_phonecall" BREAK
		case events_trevor_rants RETURN "events_trevor_rants" BREAK
		case events_stop_and_leave_vehicle RETURN "events_stop_and_leave_vehicle" BREAK
		case events_spawn_house RETURN "events_spawn_house" BREAK
		case events_reset_house_rayfire RETURN "events_reset_house_rayfire" BREAK
		case events_release_bottle_shooting_anims RETURN "events_release_bottle_shooting_anims" BREAK
		case events_remove_vehicles_and_peds RETURN "events_remove_vehicles_and_peds" BREAK
		case events_control_dispatch RETURN "events_control_dispatch" BREAK
		case events_petrol_help RETURN "events_petrol_help" BREAK
		case events_explode_petrol_can RETURN "events_explode_petrol_can" BREAK
		case events_house_explodes_rayfire RETURN "events_house_explodes_rayfire" BREAK
		case events_Trevor_walks_in_final_cutscene RETURN "events_Trevor_walks_in_final_cutscene" BREAK
		case events_kill_alerted_ped_help RETURN "events_kill_alerted_ped_help" BREAK
		case events_check_petrol_trail RETURN "events_check_petrol_trail" BREAK
		case events_check_player_out_front RETURN "events_check_player_out_front" BREAK
		case events_detach_barrels_from_peds RETURN "events_detach_barrels_from_peds" BREAK
		case events_never_do_locates_skips RETURN "events_never_do_locates_skips" BREAK
		case events_pin_interior RETURN "events_pin_interior" BREAK
		case events_spawn_bottles RETURN "events_spawn_bottles" BREAK
		case events_unfreeze_pallet RETURN "events_unfreeze_pallet" BREAK
		case events_force_farm_doors_drawing RETURN "events_force_farm_doors_drawing" BREAK
		case events_fails RETURN "events_fails" BREAK
		case events_animate_bar RETURN "events_animate_bar" BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC


func int get_ped_with_action(enumAction findAction, int count=0, bool onlyReturnIfAlive=FALSE)
	int ie
	repeat COUNT_OF(zPED) ie
		if zPED[ie].action = findAction
			if count = 0
				IF onlyReturnIfAlive = FALSE OR (onlyReturnIfAlive=TRUE AND NOT IS_PED_INJURED(zPED[ie].id))
					return ie
				ENDIF
			ELSE
				IF onlyReturnIfAlive = FALSE OR (onlyReturnIfAlive=TRUE AND NOT IS_PED_INJURED(zPED[ie].id))
					count--
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT	
	return -1
ENDFUNC

func int get_ped_with_actions(enumAction findActionA, enumAction findActionB=action_none, enumAction findActionC=action_none)
	int ie
	repeat COUNT_OF(zPED) ie
		if zPED[ie].action = findActionA
		OR (zPED[ie].action = findActionB AND findActionB != action_none)
		OR (zPED[ie].action = findActionC AND findActionC != action_none)
			RETURN ie
		ENDIF
	ENDREPEAT	
	return -1
endfunc

func int get_ped_with_role(enumRole findRole, enumPlacement withPlacement=pos_none)
	int ie
	repeat COUNT_OF(zPED) ie
		if zPED[ie].role = findRole
			if withPlacement != pos_none
				if zPED[ie].placement = withPlacement
					return ie
				ENDIF
			ELSE
				return ie
			ENDIF
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("get_ped_with_role(): Couldn't find ped with specified action")
	return -1
ENDFUNC

func int get_ped_with_placement(enumPlacement placement)
	int iR
	REPEAT COUNT_OF(zPED) iR
		if zPED[iR].placement = placement
			return iR
		ENDIF
	ENDREPEAT
	return -1
endfunc

func bool FIND_CLOSEST_PED_OF_ROLE(int &iReturnSpeaker,enumRole findRole,bool onlyReturnLivingPeds = false)
	int i
	float fRange, fClosest = 9999.0
	iReturnSpeaker=-1
	repeat count_of(zPED) i
		IF zPED[i].role = findRole
			IF onlyReturnLivingPeds = FALSE
			OR (onlyReturnLivingPeds = TRUE AND NOT IS_PED_INJURED(zPED[i].id))
				fRange = GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zPED[i].id)
				If fRange < fClosest
					fClosest = fRange
					iReturnSpeaker = i
				endif
			ENDIF
		ENDIF
	ENDREPEAT
	IF iReturnSpeaker != -1
		RETURN TRUE
	ENDIF
	return FALSE
endfunc



PROC set_ped_action(int iPed, enumAction nextAction, int actionFlag=0, int intA = 0)

	IF NOT IS_PED_INJURED(zPED[iPed].id)
		IF iPed >= 0
			zPED[iPed].lastAction = zPED[iPed].action
			zPED[iPed].action = nextAction			
			zPED[iPed].actionFlag = actionFlag
			zPED[iPed].vpos = <<0,0,0>>
			zPED[iPed].vposB = <<0,0,0>>
			zPED[iPed].floatA = 0
			zPED[iPed].floatB = 0
			zPED[iPed].intA = intA
			zPED[iPed].intB = 0
			pedReactionState[iPed].State = STATE_AMBIENT
		ENDIF
	ENDIF
	
ENDPROC

func bool IS_PED_IN_VIEWING_ARC_FROM_PED(ped_index pedViewer, PED_INDEX pedToView, float viewingArc, float viewingRange, bool includeLOScheck=FALSE)
	vector vPedA, vPedB
	if not IS_ENTITY_DEAD(pedViewer)
		vPedA = GET_ENTITY_COORDS(pedViewer)
		if IS_ENTITY_DEAD(pedToView)
			vPedB = /*WITH DEADCHECK=FALSE*/ GET_ENTITY_COORDS(pedToView)
		ELSE
			vPedB = GET_ENTITY_COORDS(pedToView)
		ENDIF
		
		if ABSF(GET_ENTITY_HEADING(pedViewer) - GET_HEADING_FROM_COORDS(vPedA,vPedB,true)) < viewingArc/2
			if GET_DISTANCE_BETWEEN_COORDS(vPedB,vPedB) < viewingRange
				if includeLOScheck = TRUE					
					if HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedViewer,pedToView)
						return true
					ELSE
						return FALSE
					ENDIF
				ELSE
					return TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	return FALSE
ENDFUNC
/*
proc check_alerted_nearby_peds(int iAlertedPed)
	//if zPED[iAlertedPed].actionFlag = 0
	IF zPED[iAlertedPed].placement != pos_balcony_far_left
	AND zPED[iAlertedPed].placement != pos_porch
		int iR
		REPEAT COUNT_OF(zPED) iR
			if iR != iAlertedPed
				if DOES_ENTITY_EXIST(zPED[iR].id)
					IF zPED[iR].role = role_guard_outside
						if zPED[iR].action != action_alert_buddies and zPED[iR].action != action_alerted and zPED[iR].action != action_respond_to_bullet and zPED[iR].action != action_respond_to_dead_ped and zPED[iR].action != action_respond_to_nearby_alerted_ped and zPED[iR].action != action_respond_to_adjacent_alerted_ped
							if not IS_PED_INJURED(zPED[iR].id)
								if GET_DISTANCE_BETWEEN_ENTITIES(zPED[iAlertedPed].id,zPED[iR].id) < 3.0
									set_ped_action(iR,action_respond_to_adjacent_alerted_ped)
									zPED[iR].intA = iAlertedPed
								ELIF GET_DISTANCE_BETWEEN_ENTITIES(zPED[iAlertedPed].id,zPED[iR].id) < 19.0
								AND IS_PED_IN_VIEWING_ARC_FROM_PED(zPED[iR].id,zPED[iAlertedPed].id,60,19.0)
									vector v1,v2
									if not IS_PED_INJURED(zPED[iAlertedPed].id)
										v1 = GET_ENTITY_COORDS(zPED[iAlertedPed].id)
										v2 = GET_ENTITY_COORDS(zPED[iR].id)
										IF absf(v1.z-v2.z) < 2.0
											set_ped_action(iR,action_respond_to_nearby_alerted_ped)
										ENDIF
									ENDIF
								ELSE
									IF zPED[iAlertedPed].placement = pos_barrels_1
									OR zPED[iAlertedPed].placement = pos_barrels_2
									OR zPED[iAlertedPed].placement = pos_garden_chat_1
									OR zPED[iAlertedPed].placement = pos_garden_chat_2
										IF zPED[iR].placement = pos_barrels_1
										OR zPED[iR].placement = pos_barrels_2
										OR zPED[iR].placement = pos_garden_chat_1
										OR zPED[iR].placement = pos_garden_chat_2
											set_ped_action(iR,action_respond_to_nearby_alerted_ped)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

		
	//ENDIF
ENDPROC
*/
PROC LOOK_AT_SOMETHING(int iPed)

	zPED[iPed].intB += floor(timestep() * 1000	)

	SWITCH zped[iPed].placement
		CASE pos_garden_chat_2
			IF zPED[iPed].intB > 2000
			AND NOT IS_BIT_SET(zPED[iPed].intA,0)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2505.6335, 4996.9937, 46.6624>>,4000)
					SET_BIT(zPED[iPed].intA,0)
			
			ELIF zPED[iPed].intB > 8000
			AND NOT IS_BIT_SET(zPED[iPed].intA,1)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2472.2966, 4962.6865, 47.2297>>,6000)
					SET_BIT(zPED[iPed].intA,1)
			
			ENDIF
		BREAK
		
		CASE pos_barrels_2			
			IF zPED[iPed].intB > 3000
			AND NOT IS_BIT_SET(zPED[iPed].intA,0)
					IF NOT IS_PED_INJURED(zPed[enum_to_int(pos_garden_chat_1)].id)
						TASK_LOOK_AT_ENTITY(zPED[iPed].id,zPed[enum_to_int(pos_garden_chat_1)].id,4000)
						SET_BIT(zPED[iPed].intA,0)
					ENDIF					
			
			ELIF zPED[iPed].intB > 11000
			AND NOT IS_BIT_SET(zPED[iPed].intA,1)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2494.6335, 5001.9937, 46.6624>>,7000)
					SET_BIT(zPED[iPed].intA,1)
			
			ELIF zPED[iPed].intB > 20000
			AND NOT IS_BIT_SET(zPED[iPed].intA,2)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2477.6335, 4966.9937, 46.6624>>,7000)
					SET_BIT(zPED[iPed].intA,2)
					
			
			ENDIF
		BREAK
		
		CASE pos_barrels_1		
			cprintln(debug_Trevor3,"Time : ",zPED[iPed].intB)
			IF zPED[iPed].intB > 3000
			AND NOT IS_BIT_SET(zPED[iPed].intA,0)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2505.6335, 4996.9937, 46.6624>>,5000)
					SET_BIT(zPED[iPed].intA,0)
					cprintln(debug_Trevor3,"LOOK A")										
			ELIF zPED[iPed].intB > 10000
			AND NOT IS_BIT_SET(zPED[iPed].intA,1)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2461.4495, 4977.4805, 51.7483>>,6000)
					SET_BIT(zPED[iPed].intA,1)
					cprintln(debug_Trevor3,"LOOK B")
				
			ELIF zPED[iPed].intB > 18000
			AND NOT IS_BIT_SET(zPED[iPed].intA,2)
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2505.6335, 4996.9937, 46.6624>>,6000)
					SET_BIT(zPED[iPed].intA,2)						
				
			ENDIF
		BREAK
	ENDSWITCH

/*
	int iLook
	IF zPED[iPed].intB = 0
		IF GET_SCRIPT_TASK_STATUS(zPED[iPed].id,SCRIPT_TASK_LOOK_AT_COORD) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(zPED[iPed].id,SCRIPT_TASK_LOOK_AT_ENTITY) != PERFORMING_TASK
			zPED[iPed].intB = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000,7000)
		ENDIF
	ELSE
		IF GET_GAME_TIMER() > zPED[iPed].intB
			zPED[iPed].intB = 0
			iLook = GET_RANDOM_INT_IN_RANGE(0,3)
			SWITCH iLook
				CASE 0 //look left
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2505.6335, 4996.9937, 46.6624>>,GET_RANDOM_INT_IN_RANGE(2000,5000))
				BREAK
				CASE 1 //look left
					TASK_LOOK_AT_COORD(zPED[iPed].id,<<2472.2966, 4962.6865, 47.2297>>,GET_RANDOM_INT_IN_RANGE(2000,5000))
				BREAK
				CASE 2 //look at someone
					SWITCH zped[iPed].placement
						CASE pos_garden_chat_2
							TASK_LOOK_AT_COORD(zPED[iPed].id,<<2492.4648, 4977.3994, 46.8259>>,GET_RANDOM_INT_IN_RANGE(2000,5000))
						BREAK
						CASE pos_barrels_2
							IF NOT IS_PED_INJURED(zPed[enum_to_int(pos_garden_chat_2)].id)
								TASK_LOOK_AT_ENTITY(zPED[iPed].id,zPed[enum_to_int(pos_garden_chat_2)].id,GET_RANDOM_INT_IN_RANGE(2000,5000))
							ENDIF
						BREAK
						CASE pos_barrels_1
							IF NOT IS_PED_INJURED(zPed[enum_to_int(pos_barrels_2)].id)
								TASK_LOOK_AT_ENTITY(zPED[iPed].id,zPed[enum_to_int(pos_barrels_2)].id,GET_RANDOM_INT_IN_RANGE(2000,5000))
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		ENDIF
	ENDIF*/
ENDPROC

FUNC BOOL ped_aims_at_sphere(int iPed, vector vSphere, float radius, int duration=0)
	WEAPON_TYPE thisWeapon
	GET_CURRENT_PED_WEAPON(zped[iPed].id,thisWeapon)
	
	
	
	If thisWeapon != WEAPONTYPE_UNARMED	
		
		
		if duration !=0
			if zPED[iPed].intB = 0
				zPED[iPed].intB = GET_GAME_TIMER() + duration
			ENDIF
			if GET_GAME_TIMER() > zPED[iPed].intB
				zPED[iPed].intB = 0
				return TRUE
			ENDIF
		ENDIF
		bool bTrue
		IF NOT IS_PED_INJURED(zPED[iPed].id)
			if GET_SCRIPT_TASK_STATUS(zPED[iPed].id,script_task_perform_sequence) = FINISHED_TASK
			
				bTrue = TRUE
				
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(zPED[iPed].id,script_TASK_AIM_GUN_AT_COORD) = PERFORMING_TASK
	
				bTrue = FALSE
				
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(zPED[iPed].id,SCRIPT_TASK_SWAP_WEAPON) != FINISHED_TASK				
				bTrue = FALSE
				
			ENDIF
			
			
			
			IF bTrue
				
					
				zPED[iPed].vposB = GET_RANDOM_POINT_IN_DISC_UNIFORM(vSphere,radius,0.3) 
				vector vtemp
				vTemp = get_entity_coords(zped[iped].id)
				zPED[iPed].vposB.z = vTemp.z + 1.0
				if IS_VECTOR_ZERO(zPED[iPed].vPos)
					zPED[iPed].vPos = zPED[iPed].vposB
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[iped].id,zPED[iPed].vposB) < 2.0
					zPED[iPed].vposB.x += 2.0
				ENDIF
				
				
				zPED[iPed].vpos = zPED[iPed].vpos + ((zPED[iPed].vPosB - zPED[iPed].vpos) * TIMESTEP() * 2.0)
				TASK_AIM_GUN_AT_COORD(zPED[iPed].id,zPED[iPed].vpos,GET_RANDOM_INT_IN_RANGE(3000,5000))		
			ENDIF
		ENDIF
	else
		SET_CURRENT_PED_WEAPON(zPED[iPed].id,GET_BEST_PED_WEAPON(zPED[iPed].id),true)
	endif
	RETURN FALSE
ENDFUNC

FUNC BOOL PED_MOVE_AIMING_AT_SPHERE(int iPed, vector moveTo, vector vSphere, float radius)
	IF NOT IS_PED_INJURED(zPED[iPed].id)
		WEAPON_TYPE thisWeapon
		GET_CURRENT_PED_WEAPON(zped[iPed].id,thisWeapon)

		If thisWeapon != WEAPONTYPE_UNARMED	
	
			if GET_SCRIPT_TASK_STATUS(zPED[iPed].id,script_task_perform_sequence) = FINISHED_TASK
			and get_game_timer() > zPED[iPed].intB
				
				zPED[iPed].intB = get_game_timer() + GET_RANDOM_INT_IN_RANGE(4000,5000)
				zPED[iPed].vpos = GET_RANDOM_POINT_IN_DISC_UNIFORM(vSphere,radius,0.3) 

				TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(zPED[iPed].id,moveTo,zPED[iPed].vpos,PEDMOVE_RUN,FALSE,0.5,2.0)		
			ENDIF		
			
			if IS_ENTITY_AT_COORD(zPED[iPed].id,moveTo,<<0.5,0.5,2.0>>)
				return TRUE
			ENDIF
		ELSE
			SET_CURRENT_PED_WEAPON(zPED[iPed].id,GET_BEST_PED_WEAPON(zPED[iPed].id),true)
		ENDIF
	ENDIF
	
	return FALSE
ENDFUNC



								
proc SET_DEFENSIVE_AREA_BETWEEN_HOUSE_AND_PLAYER(int iPed)
	IF NOT IS_PED_INJURED(player_ped_id())
		float heading
		float playerDistToHouse
		playerDistToHouse = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2444.890381,4975.631836,56.160000>>,false)
		IF playerDistToHouse < 30.0 //player is within close range of the house 
			IF zped[iPed].intA = 0
				zped[iPed].intA = 1
				heading = GET_HEADING_FROM_COORDS(<<2444.890381,4975.631836,56.160000>>,GET_ENTITY_COORDS(player_ped_id()),FALSE)
		
				heading = GET_RANDOM_FLOAT_IN_RANGE(heading-30.0,heading+30.0)	
				SET_PED_SPHERE_DEFENSIVE_AREA(zped[iPed].id,<<2444.890381+(SIN(heading)*30.0),4975.631836+(COS(heading)*30.0),45.160000>>,5.0)				
			endif
		ELSE
			IF IS_VECTOR_ZERO(zped[iPed].vpos)
				heading = GET_HEADING_FROM_COORDS(<<2444.890381,4975.631836,56.160000>>,GET_ENTITY_COORDS(player_ped_id()),FALSE)
			
				heading = GET_RANDOM_FLOAT_IN_RANGE(heading-30.0,heading+30.0)
				
				playerDistToHouse = GET_RANDOM_FLOAT_IN_RANGE(30.0,playerDistToHouse)
				zped[iPed].vpos = <<2444.890381+(SIN(heading)*playerDistToHouse),4975.631836+(COS(heading)*playerDistToHouse),45.160000>>
				SET_PED_SPHERE_DEFENSIVE_AREA(zped[iPed].id,zped[iPed].vpos,8.0)
		
			ELSE
				//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2444.890381,4975.631836,56.160000>>,false) < GET_DISTANCE_BETWEEN_COORDS(<<2444.890381,4975.631836,56.160000>>,zped[iPed].vpos)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),zped[iPed].vpos) > 20.0
					heading = GET_HEADING_FROM_COORDS(<<2444.890381,4975.631836,56.160000>>,GET_ENTITY_COORDS(player_ped_id()),FALSE)
				
					heading = GET_RANDOM_FLOAT_IN_RANGE(heading-30.0,heading+30.0)
			
					playerDistToHouse = GET_RANDOM_FLOAT_IN_RANGE(30.0,playerDistToHouse)
					zped[iPed].vpos = <<2444.890381+(SIN(heading)*playerDistToHouse),4975.631836+(COS(heading)*playerDistToHouse),45.160000>>
			
				endif			
			endif
		endif
	endif
endproc

// ================================================== Manage Dialogue ==========================================

ped_index rpedInSlot[10]
structPedsForConversation CHI2CONV
bool convAlreadyCalledThisFrame

func PED_INDEX rped_in_slot(int slot)
	return rpedInSlot[slot]
ENDFUNC


PROC ADD_PED_FOR_DIALOGUE_EXTRA(int speakerIndex, ped_index speaker, string speakerLabel)
	//may be a problem if  aped gets added multiple times in different slots. Fix below:
	
	int iJ
	repeat count_of(rpedInSlot) iJ
		if rpedInSlot[iJ] = speaker
			//there should never be more than one situation for this
			REMOVE_PED_FOR_DIALOGUE(CHI2CONV,iJ)
			rpedInSlot[iJ] = null
		endif
	endrepeat
			
	if rpedInSlot[speakerIndex] != null
		REMOVE_PED_FOR_DIALOGUE(CHI2CONV,speakerIndex)
	ENDIF
	
	//if speakerIndex = 0
		//SCRIPT_ASSERT("Use a non-zero speaker index") //cos this function won't work otherwise
	//ENDIF
	
	ADD_PED_FOR_DIALOGUE(CHI2CONV,speakerIndex,speaker, speakerLabel)
	rpedInSlot[speakerIndex] = speaker

ENDPROC

/*
PROC REMOVE_INACTIVE_SPEAKERS(string thisConv, int iSp1,int iSp2, int iSp3, int iSp4)
	cprintln(debug_trevor3, isp1," ",isp2," ",isp3," ",isp4)
	int ij
	IF IS_STRING_NULL_OR_EMPTY(remlastConv)
	OR NOT ARE_STRINGS_EQUAL(remlastConv,thisConv)
		remlastConv = thisConv
		FOR ij = 0 to 15
			IF CHI2CONV.PedInfo[ij].ActiveInConversation
				IF ij != iSp1
				AND ij != iSp2
				AND ij != iSp3
				AND ij != isp4
					CPRINTLN(DEBUG_TREVOR3,"remove pexd at dialogue: ",Ij," label: ",CHI2CONV.PedInfo[ij].VoiceID)
					REMOVE_PED_FOR_DIALOGUE(CHI2CONV,ij)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC*/

func bool PLAYER_CALL_CHAR_CELLPHONE_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null)
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
		if rped_in_slot(speakerOne) != pedSpeakerOne or pedSpeakerOne = null
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		ENDIF
		if speakerTwo != -1
			if rped_in_slot(speakerTwo) != pedSpeakerTwo or pedSpeakerTwo = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
			ENDIF
		ENDIF
		if speakerThree != -1
			if rped_in_slot(speakerThree) != pedSpeakerThree or pedSpeakerThree = null
				ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
			ENDIF
		ENDIF

		IF CHAR_CALL_PLAYER_CELLPHONE(CHI2CONV,CHAR_ONEIL,"CHI2AUD",Label,CONV_PRIORITY_VERY_HIGH)
			return TRUE
		ENDIF
	ENDIF
	return FALSE
endfunc

func bool CREATE_CONVERSATION_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	
	IF NOT convAlreadyCalledThisFrame
		convAlreadyCalledThisFrame = TRUE
		convStore[0].speakerNo = speakerOne
		convStore[0].pedIndex = pedSpeakerOne
		convStore[0].speakerLabel = speakerOneLabel
		convStore[1].speakerNo = speakerTwo
		convStore[1].pedIndex = pedSpeakerTwo
		convStore[1].speakerLabel = speakerTwoLabel
		convStore[2].speakerNo = speakerThree
		convStore[2].pedIndex = pedSpeakerThree
		convStore[2].speakerLabel = speakerThreeLabel
		convStore[3].speakerNo = speakerFour
		convStore[3].pedIndex = pedSpeakerFour
		convStore[3].speakerLabel = speakerFourLabel
		
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
		
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF		
		
		IF NOT IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS()
			IF IS_MESSAGE_BEING_DISPLAYED()
			ANd IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
				if CREATE_CONVERSATION(CHI2CONV, "CHI2AUD", Label, convPriority,DO_NOT_DISPLAY_SUBTITLES)
		
					bLastConvoWithoutSubtitles = TRUE
				
					return true
				ENDIF
			else
				if CREATE_CONVERSATION(CHI2CONV, "CHI2AUD", Label, convPriority)
				
					bLastConvoWithoutSubtitles = FALSE
		
					return true
				ENDIF
			endif
		endif
	ENDIF
	return FALSE
ENDFUNC

PROC ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_MEDIUM)
	REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree)
		
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
	
	ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(CHI2CONV, "CHI2AUD", Label, convPriority)
endproc

func bool CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(string Label, string line, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null,int speakerFour=-1,ped_index pedSpeakerFour=null, string speakerFourLabel = null,enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR IS_SUBTITLE_PREFERENCE_SWITCHED_ON() = FALSE
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
		
		ADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
		if speakerTwo != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			ADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF
		IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(CHI2CONV, "CHI2AUD",Label,line,convPriority)		
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
endfunc

PROC CONTROL_CONVERSATION_SUBTITLES()
					 
	If bLastConvoWithoutSubtitles
		IF IS_STRING_NULL_OR_EMPTY(restartSubtitleRoot)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_SCRIPTED_CONVERSATION_ONGOING()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()															
						restartSubtitleLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						restartSubtitleRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						restartSubtitleRoot = ""
						////cprintln(DEBUG_TREVOR3,restartSubtitleLine) //trying to find why ped in basement repeats his "IM NEARLY FINSHED" line
						if not IS_STRING_NULL_OR_EMPTY(restartSubtitleLine)
						AND NOT ARE_STRINGS_EQUAL(restartSubtitleLine,"NULL")
							restartSubtitleRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							KILL_FACE_TO_FACE_CONVERSATION()
						ELSE
							bLastConvoWithoutSubtitles = FALSE
							restartSubtitleRoot = ""
							restartSubtitleLine = ""
						ENDIF		
						
						
					ENDIF
				ENDIF
			ELSE
				bLastConvoWithoutSubtitles = FALSE
				restartSubtitleRoot = ""
				restartSubtitleLine = ""
			ENDIF
		ELSE		
			IF IS_SCRIPTED_CONVERSATION_ONGOING()
				IF NOT IS_CONV_ROOT_PLAYING(restartSubtitleRoot)
					bLastConvoWithoutSubtitles = FALSE
					restartSubtitleRoot = ""
					restartSubtitleLine = ""
				ENDIF
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(restartSubtitleRoot) 
				////cprintln(DEBUG_TREVOR3,"REPLAY CONVERSATION WITH SUBS")
				convStore[0] = convStore[0]
				/*
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE_EXTRA(restartSubtitleRoot,restartSubtitleLine,convStore[0].speakerNo,convStore[0].pedIndex,convStore[0].speakerLabel,
					convStore[1].speakerNo,convStore[1].pedIndex,convStore[1].speakerLabel,
					convStore[2].speakerNo,convStore[2].pedIndex,convStore[2].speakerLabel,
					convStore[3].speakerNo,convStore[3].pedIndex,convStore[3].speakerLabel)
				
					restartSubtitleRoot = ""
					restartSubtitleLine = ""
					bLastConvoWithoutSubtitles = FALSE
				ENDIF
				*/
			ENDIF
		ENDIF
	ENDIF

ENDPROC
/*
PROC manage_dialogue()
			
	SWITCH AWARENESS_STATE
		CASE AWARE_AND_ALERTING
			// need bit in here to make another ped run to shout for backup if the ped doing it was killed.
			
		FALLTHRU
		CASE SEEKING_TREVOR
			enumComments currentSweepComment
			ped_index ped_speaking
			currentSweepComment =  SWEEP_DIALOGUE_ALERT(ped_speaking)
			if currentSweepComment != COMMENT_NONE
				nextSweepCommentToPlay = currentSweepComment
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			if nextSweepCommentToPlay != COMMENT_NONE				
				if not is_ped_injured(ped_speaking)
					switch nextSweepCommentToPlay
						case COMMENT_JUSTGONE
							if CREATE_CONVERSATION_EXTRA("FLEE",1,ped_speaking,"CHIN2goon1")
								nextSweepCommentToPlay = COMMENT_NONE
							ENDIF
						BREAK
						case COMMENT_NOT_HERE
							if CREATE_CONVERSATION_EXTRA("LOST",1,ped_speaking,"CHIN2goon1")
								nextSweepCommentToPlay = COMMENT_NONE
							ENDIF
						BREAK
						case COMMENT_SPOTTED
							if CREATE_CONVERSATION_EXTRA("SPOT",1,ped_speaking,"CHIN2goon1")
								nextSweepCommentToPlay = COMMENT_NONE
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					nextSweepCommentToPlay = COMMENT_NONE
				ENDIF
			ENDIF

			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			and currentSweepComment = COMMENT_NONE
				if GET_GAME_TIMER() > CONVERSATION_GAP
					int convPed1
					int convPed2
					convPed1 = get_ped_with_action(action_alerted,0)
					convPed2 = get_ped_with_action(action_alerted,1)
					if convPed1 >= 0 and convPed2 >= 0
						IF NOT IS_PED_INJURED(zPED[convPed1].id) AND NOT IS_PED_INJURED(zPED[convPed2].id)						
							SWITCH LAST_CONVERSATION						 
								CASE CONV_EMPTY
									IF CREATE_CONVERSATION_EXTRA("SEEK",1,zPED[convPed1].id,"CHIN2goon1",2,zPED[convPed2].id,"CHIN2goon2")
										LAST_CONVERSATION = CONV_SEEK
										add_event(events_trigger_investigate_treeline)
									ENDIF
								BREAK
								CASE CONV_SEEK
									IF CREATE_CONVERSATION_EXTRA("PANC",1,zPED[convPed1].id,"CHIN2goon1",2,zPED[convPed2].id,"CHIN2goon2")
										LAST_CONVERSATION = CONV_PANC
									ENDIF
								BREAK
								CASE CONV_PANC
									IF CREATE_CONVERSATION_EXTRA("PANC2",1,zPED[convPed1].id,"CHIN2goon1")
										random_lines_played++
										IF random_lines_played = 5
											random_lines_played = 0
											LAST_CONVERSATION = CONV_PANC2
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
					
				ENDIF
			ELSE
				CONVERSATION_GAP = GET_GAME_TIMER() + 2500
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC
*/


// ===================================================== Manage Events ==========================================

FUNC BOOL HAS_DIALOGUE_FINISHED(int iEntry,enumDialogue thisDia)
	IF dia[iEntry].dial = thisDia
		IF dia[iEntry].bCompleted
			RETURN TRUE
		ENDIF
	ELSE
		IF dia[iEntry].dial != DIA_NULL
			SCRIPT_ASSERT("HAS_DIALOGUE_FINISHED() has wrong entry value")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC delete_all_house()
	IF DOES_ENTITY_EXIST(BarrelVan)
		DELETE_VEHICLE(BarrelVan)
	ENDIF
	
	int i
	REPEAT count_of(zped) i
		IF DOES_ENTITY_EXIST(zped[i].id)
			delete_ped(zped[i].id)
		ENDIF
		IF DOES_ENTITY_EXIST(zped[i].obj)
			DELETE_OBJECT(zped[i].obj)
		ENDIF
		
		zPED[i].action = action_none
		zPED[i].lastaction = action_none
		zPED[i].vpos = <<0,0,0>>
		zPED[i].vPosB = <<0,0,0>>
		zPED[i].placement = pos_none
		zPED[i].actionFlag = 0
		zPED[i].floatA = 0
		zPED[i].floatB = 0
		zPED[i].intA = 0
		zPED[i].intB = 0
		zPED[i].nearbyreactingPed = FALSE
		zPED[i].pedOfInterest = 0
		zPED[i].sniped = FALSE
		zPED[i].alerted = FALSE
		pedReactionState[i].State = STATE_SPAWN
		
	ENDREPEAT
	
	IF DOES_PICKUP_EXIST(health_pickup)
		REMOVE_PICKUP(health_pickup)
	ENDIF
	
	REPEAT COUNT_OF(piPickups) i
		IF DOES_PICKUP_EXIST(piPickups[i])
			REMOVE_PICKUP(piPickups[i])
		ENDIF
	ENDREPEAT
	
	
	
	REPEAT count_of(oCans) i
		IF DOES_ENTITY_EXIST(oCans[i])
			delete_object(oCans[i])
		ENDIF
	ENDREPEAT
	
	RESET_REACT_ARRAY(pedReactionState,dChinese2,convBlock,12,<<2482.1965, 4975.3667, 44.7288>>,121,STATE_SPAWN)	
	
	
ENDPROC

proc do_spawn_house()
	//int j

	add_event(events_force_farm_doors_drawing)
	CLEAR_AREA(<<2479.874756,4980.867676,44.820522>>,60.0,TRUE)
	
	BarrelVan = CREATE_VEHICLE(BURRITO, << 2479.5789, 4980.5029, 44.8051 >>, 341.8158)
	SET_VEHICLE_COLOUR_COMBINATION(BarrelVan,1)
	SET_VEHICLE_LIVERY(BarrelVan,2)
	SET_VEHICLE_ON_GROUND_PROPERLY(BarrelVan)

	IF missionStage < get_to_house
		prep_ped(role_guard_outside,action_spawn,pos_barrels_1)
		prep_ped(role_guard_outside,action_spawn,pos_barrels_2)
	
		prep_ped(role_guard_outside,action_spawn,pos_balcony_veranda_walk) //patrolling balcony
		prep_ped(role_guard_outside,action_spawn,pos_balcony_doorway) //on balcony chats to patroling ped
		prep_ped(role_guard_outside,action_spawn,pos_garden_chat_1)
		prep_ped(role_guard_outside,action_spawn,pos_garden_chat_2)
		prep_ped(role_guard_outside,action_spawn,pos_porch) //on lower porch
		prep_ped(role_guard_outside,action_spawn,pos_balcony_far_left) //on balcony to left
		
		prep_ped(role_guard_outside,action_spawn,pos_shooting_bottle_1)
		prep_ped(role_guard_outside,action_spawn,pos_shooting_bottle_2)
		
//	prep_ped(role_guard_outside,action_spawn,pos_pissing_ped)
				
	endif

	
	if missionStage < inside_house
		prep_ped(role_guard_house,action_spawn,pos_inside_right_window)
		
		prep_ped(role_guard_house,action_spawn,pos_inside_utility_room)
		prep_ped(role_guard_house,action_spawn,pos_inside_kitchen1)
		prep_ped(role_guard_house,action_spawn,pos_inside_kitchen2)

	endif
	
	if missionStage < do_petrol_trail 
		prep_ped(role_guard_house,action_spawn,pos_inside_seated1)
		prep_ped(role_guard_house,action_spawn,pos_inside_seated2)
		prep_ped(role_guard_house,action_spawn,pos_inside_seated3)
		
		prep_ped(role_guard_house,action_spawn,pos_inside_cards1)
		prep_ped(role_guard_house,action_spawn,pos_inside_cards2)
		prep_ped(role_guard_house,action_spawn,pos_inside_cards3)
						
		prep_ped(role_guard_house,action_spawn,pos_inside_bystairs1)
	//	prep_ped(role_guard_house,action_spawn,pos_inside_bystairs2)
		prep_ped(role_innocent,action_spawn,pos_innocent_drugden)	
	//	prep_ped(role_innocent,action_spawn,pos_innocent_chef)
	endif
	
	
	if missionStage = do_petrol_trail
		REQUEST_ANIM_DICT("DEAD")
		
		WHILE NOT HAS_ANIM_DICT_LOADED("DEAD")
			safewait(323)
		ENDWHILE
		zped[15].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,  <<2436.9011, 4959.0996, 45.8106>>, 209.7559)
		TASK_PLAY_ANIM(zped[15].id,"DEAD","DEAD_C",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[15].id,true)
		DISABLE_PED_PAIN_AUDIO(zped[15].id,true)
		
		zped[16].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_01,<<2440.2661, 4970.7051, 45.8306>>, 15.7982)
		TASK_PLAY_ANIM(zped[16].id,"DEAD","DEAD_D",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[16].id,true)
		DISABLE_PED_PAIN_AUDIO(zped[16].id,true)
		
		zped[17].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,<<2443.4197, 4974.5234, 45.8106>>, 121.8340)
		TASK_PLAY_ANIM(zped[17].id,"DEAD","DEAD_E",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[17].id,true)
		DISABLE_PED_PAIN_AUDIO(zped[17].id,true)
		
	
		zped[18].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,<<2448.8877, 4971.5605, 45.8106>>, 194.7676)
		TASK_PLAY_ANIM(zped[18].id,"DEAD","DEAD_G",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[18].id,true)
		DISABLE_PED_PAIN_AUDIO(zped[18].id,true)
		
		
		WHILE NOT IS_ENTITY_PLAYING_ANIM(zped[15].id,"DEAD","DEAD_C")
		OR NOT IS_ENTITY_PLAYING_ANIM(zped[16].id,"DEAD","DEAD_D")
		OR NOT IS_ENTITY_PLAYING_ANIM(zped[17].id,"DEAD","DEAD_E")
		OR NOT IS_ENTITY_PLAYING_ANIM(zped[18].id,"DEAD","DEAD_G")
			safewait(54343)
			IF NOT IS_PED_INJURED(zped[15].id)
			AND NOT IS_PED_INJURED(zped[16].id) 
			AND NOT IS_PED_INJURED(zped[17].id)
			AND NOT IS_PED_INJURED(zped[18].id)
			ENDIF
		ENDWHILE
		
		SET_ENTITY_HEALTH(zped[15].id,0)
		SET_ENTITY_HEALTH(zped[16].id,0)
		SET_ENTITY_HEALTH(zped[17].id,0)
		SET_ENTITY_HEALTH(zped[18].id,0)
		
		REMOVE_ANIM_DICT("DEAD")
	endif
	//cans
	
	//CLEAR_AREA(<<2494.6641, 4970.2280, 44.5430>>,5.0,true)
	
	add_event(events_spawn_bottles)
	
	int PlacementFlags
	PlacementFlags = 0
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_UPRIGHT)) 
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_FIXED))
	SET_BIT(PlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES)) // You might not want this?
	SET_BIT(PlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))

	health_pickup = CREATE_PICKUP(PICKUP_HEALTH_STANDARD,<<2446.8962, 4990.9722, 45.5477>>,PlacementFlags)
	
	//navemsh block around NG bush
	
	IF NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST(navblockA)
		navblockA = ADD_NAVMESH_BLOCKING_OBJECT(<<2464.956299,4990.050781,44.484150>>,<<2.500000,3.125000,2.062500>>,0)
	ENDIF
	
	cprintln(debug_Trevor3,"Add pickup")
	piPickups[0] =	CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<2444.4974, 4975.9562, 50.5650>>, << 0,0,100 >>, PlacementFlags)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[0], "V_8_Bed1Rm")
	piPickups[1] =	CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<2443.4000, 4978.5199, 46.8107>>, << 0,0,100 >>, PlacementFlags)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[1], "V_8_KitchnRm")
	piPickups[3] =	CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, << 2441.0281, 4963.8511, 46.5605 >>, << 0, 0, -80.6 >>, PlacementFlags)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[3], "V_8_StudyRm")
	piPickups[4] =	CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<  2435.1650, 4971.4668, 45.9100 >>, << 0, 0, -50.4 >>, PlacementFlags)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[4], "V_8_Hall2Rm")
	
	// Set sawn-off placement flags
	PlacementFlags = 0
	SET_BIT(PlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES)) // You might not want this?
	SET_BIT(PlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	SET_BIT(PlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	CPRINTLN(debug_trevor3,"Add pickup shotgun")
	piPickups[2] = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_SAWNOFFSHOTGUN, <<2438.7878, 4970.7896, 50.5650>>, <<0,0,0>>, PlacementFlags)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piPickups[2], "V_8_Bed1Rm")

ENDPROC



proc manage_events()
	int iE
	REPEAT COUNT_OF(events) iE
		if events[iE].active = TRUE
			
			switch events[iE].event				 
				case events_resolve_vehicles_at_start
					switch events[iE].flag
						case 0
							IF IS_CUTSCENE_PLAYING()
								events[iE].flag++
							ENDIF
						BREAK
						CASE 1
							////cprintln(debug_Trevor3,"Resolve vehicles")
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1992.804077,3053.310303,45.777145>>, <<1989.681274,3055.295410,49.090008>>, 3.250000,<<1995.2018, 3062.1565, 46.0491>>, 59.7235,<<10,20,9>>) //position where chinese peds are after cutscene																
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1994.427368,3045.135986,45.340134>>, <<1981.902832,3053.235107,49.791527>>, 9.500000,<<1995.2018, 3062.1565, 46.0491>>, 59.7235,<<10,20,9>>) //inside the bar, incase player drives bike in
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1985.403687,3075.057129,44.849529>>, <<2017.101685,3057.434814,53.718960>>, 33.000000,<<1995.2018, 3062.1565, 46.0491>>, 59.7235,<<10,20,9>>) //car park area, as per bug 1001450
							
							vehPlayer = GET_PLAYERS_LAST_VEHICLE()
							IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
							AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehPlayer,<<1995.2018, 3062.1565, 46.0491>>) < 5.0
								REQUEST_VEHICLE_ASSET(get_entity_model(vehPlayer))
							ENDIF
						//	CLEAR_AREA(GET_ENTITY_COORDS(player_ped_id()),15,TRUE) //this was removing player car
						//	CLEAR_PED_TASKS_IMMEDIATELY(player_ped_id()) //removed this due to bug 1234882
							events[iE].active = FALSE
						break

					endswitch
				break
			
				case events_trevor_phonecall	
					IF missionStage >=farm_cutscene
						events[iE].flag = CLEANUP
					ENDIF
					switch events[iE].flag
						case CLEANUP
							KILL_ANY_CONVERSATION()
							KILL_PHONE_CONVERSATION()
							//////cprintln(DEBUG_TREVOR3,"KIL PHONE CALL")
						BREAK
						case 0							
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2442.55, 4967.51, 54.35>>) < 1300
							OR IS_CALLING_CONTACT(CHAR_ONEIL)
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2442.55, 4967.51, 54.35>>) > 500
								//If IS_MESSAGE_BEING_DISPLAYED()							
									events[iE].flag++																	
								ENDIF
							ENDIF
						BREAK
						case 1
							//If not IS_MESSAGE_BEING_DISPLAYED()		
								//need to set up speakers for this
								//////cprintln(DEBUG_TREVOR3,"LOADING PHONE CALL")
								if PLAYER_CALL_CHAR_CELLPHONE_EXTRA("PHN2",1,player_ped_id(),"Trevor",8,null,"ONEIL")								
									//////cprintln(DEBUG_TREVOR3,"PHONE CALL PLAYING")
									events[iE].flag++
								ENDIF
							//ENDIF
						BREAK
						case 2
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION_EXTRA("CHI2_ONEILPH",1,player_ped_id(),"Trevor") //CHI2_ONEILPH
									events[iE].active = FALSE
								ENDIF								
							ENDIF
						break
					ENDSWITCH
				BREAK
				
			
				
				case events_trevor_rants
					//IF NOT is_event_active(events_trevor_phonecall)
					IF missionStage >=farm_cutscene
						events[iE].flag = CLEANUP
						events[iE].active = FALSE
					ENDIF
						switch events[iE].flag
							
							case 0
								IF CREATE_CONVERSATION_EXTRA("CHI2_boom",1,player_ped_id(),"Trevor")
									events[iE].flag++
								ENDIF
								iRandomCounter = 0
								events[iE].intA = GET_GAME_TIMER() + 3000
								//events[iE].flag++
							break
			
							CASE 1
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									
									IF NOT IS_PLAYER_IN_ANY_SHOP()
										IF NOT (IS_PED_IN_ANY_VEHICLE(player_ped_id()))
											IF GET_GAME_TIMER() > events[iE].intA
												IF iRandomCounter < 13
													IF CREATE_CONVERSATION_EXTRA("DRI1",2,player_ped_id(),"Trevor")
														iRandomCounter++
														events[iE].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000,10000)													
													ENDIF
												ELSE
													events[iE].active = FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					//ENDIF
				break
				
				case events_stop_and_leave_vehicle
					if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX thisVeh
						thisVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF NOT IS_ENTITY_IN_AIR(thisVeh)
							if STOP_MY_VEHICLE(6,2)
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
								events[iE].active = FALSE
							ENDIF
						ENDIF
					ELSE
						events[iE].active = FALSE
					ENDIF
				BREAK
				case events_spawn_house
					switch events[iE].flag
						case -20
							if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 2460.0728, 4964.1758, 46.2405 >>) < 250.0
								REQUEST_MODEL(A_M_M_Hillbilly_01)	
								REQUEST_MODEL(PROP_PALLET_PILE_01)
								events[iE].flag++							
							endif						
						break
						case -19
							IF HAS_MODEL_LOADED(A_M_M_Hillbilly_01)
							AND HAS_MODEL_LOADED(PROP_PALLET_PILE_01)
								REQUEST_MODEL(A_M_M_Hillbilly_02)
								events[iE].flag++
							ENDIF
						break
						case -18
							IF HAS_MODEL_LOADED(A_M_M_Hillbilly_02)
								REQUEST_MODEL(BURRITO)
								events[iE].flag++
							ENDIF
						BREAK
						
						CASE -17
							IF HAS_MODEL_LOADED(BURRITO)																		
								REQUEST_MODEL(A_M_Y_MethHead_01)
								events[iE].flag++
							ENDIF
						BREAK
						
						CASE -16
							IF HAS_MODEL_LOADED(A_M_Y_MethHead_01)
								REQUEST_MODEL(PROP_LD_CAN_01)
								REQUEST_MODEL(Prop_CS_Fertilizer)							
								REQUEST_MODEL(PROP_CS_BEER_BOT_01)
								REQUEST_MODEL(PROP_PHONE_ING)
								REQUEST_MODEL(DUBSTA)								
								REQUEST_ANIM_DICT("misschinese2_bank5")
								REQUEST_ANIM_DICT("misschinese2_bank1")
								REQUEST_ANIM_DICT("reaction@male_stand@big_variations@b")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@left")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@right")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@backward")
								REQUEST_ANIM_DICT("misschinese2_barrelRoll")
								REQUEST_ANIM_SET("move_m@gangster@var_e")
								REQUEST_ANIM_SET("move_m@gangster@var_f")
								REQUEST_ANIM_SET("move_m@gangster@generic")
								events[iE].flag++
							ENDIF
						break
						
						CASE -15
							IF HAS_MODEL_LOADED(DUBSTA)
								add_event(events_pin_interior)		
								add_event(events_remove_vehicles_and_peds)
								SET_ROADS_IN_AREA(<<2469.9854, 4961.0449, 44.8113>>-<<100,100,100>>,<<2469.9854, 4961.0449, 44.8113>>+<<100,100,100>>,FALSE)
								events[iE].flag=1
							ENDIF
						BREAK
						
						
						case 0
							if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 2460.0728, 4964.1758, 46.2405 >>) < 250.0
								REQUEST_MODEL(A_M_M_Hillbilly_01)
								REQUEST_MODEL(A_M_M_Hillbilly_02)
								REQUEST_MODEL(A_M_Y_MethHead_01)
						
								request_model(PROP_LD_CAN_01)
								REQUEST_MODEL(Prop_CS_Fertilizer)	
								REQUEST_MODEL(BURRITO)	
								REQUEST_MODEL(PROP_CS_BEER_BOT_01)										
											
						
								IF missionStage <= farm_cutscene								
									REQUEST_MODEL(PROP_PHONE_ING)
									REQUEST_MODEL(DUBSTA)								
								ENDIF
									
								REQUEST_ANIM_DICT("misschinese2_bank5")
								REQUEST_ANIM_DICT("misschinese2_bank1")
								REQUEST_ANIM_DICT("reaction@male_stand@big_variations@b")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@left")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@right")
								REQUEST_ANIM_DICT("reaction@male_stand@big_intro@backward")
								REQUEST_ANIM_DICT("misschinese2_barrelRoll")
								REQUEST_ANIM_SET("move_m@gangster@var_e")
								REQUEST_ANIM_SET("move_m@gangster@var_f")
								REQUEST_ANIM_SET("move_m@gangster@generic")
								
								
								add_event(events_pin_interior)		
								add_event(events_remove_vehicles_and_peds)
								SET_ROADS_IN_AREA(<<2469.9854, 4961.0449, 44.8113>>-<<100,100,100>>,<<2469.9854, 4961.0449, 44.8113>>+<<100,100,100>>,FALSE)
								events[iE].flag++
							ENDIF
						BREAK
						
						case 1
							if HAS_MODEL_LOADED(A_M_M_Hillbilly_01)
							and HAS_MODEL_LOADED(A_M_M_Hillbilly_02)
							and HAS_MODEL_LOADED(A_M_Y_MethHead_01)
					
							and HAS_MODEL_LOADED(BURRITO)							
							and HAS_MODEL_LOADED(Prop_CS_Fertilizer)
							and HAS_MODEL_LOADED(PROP_CS_BEER_BOT_01)						
												
							and has_model_loaded(PROP_LD_CAN_01)
							and HAS_ANIM_DICT_LOADED("misschinese2_bank5")
							and HAS_ANIM_DICT_LOADED("misschinese2_bank1")
							and HAS_ANIM_DICT_LOADED("misschinese2_barrelRoll")		
							and HAS_ANIM_DICT_LOADED("reaction@male_stand@big_variations@b")
							and HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@left")
							and HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@right")
							and HAS_ANIM_DICT_LOADED("reaction@male_stand@big_intro@backward")
							and ((missionStage <= farm_cutscene AND has_model_loaded(PROP_PHONE_ING)) OR missionStage > farm_cutscene)
							and ((missionStage <= farm_cutscene AND has_model_loaded(DUBSTA)) OR missionStage > farm_cutscene)
																	
								events[iE].flag=4
							ENDIF
							
							if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 2460.0728, 4964.1758, 46.2405 >>) > 300.0
								SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Hillbilly_01)
								SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Hillbilly_02)
								SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_MethHead_01)
				
								SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO)							
								SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Fertilizer)
								SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_BEER_BOT_01)						
												
								SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CAN_01)
								REMOVE_ANIM_DICT("misschinese2_bank5")
								REMOVE_ANIM_DICT("misschinese2_bank1")
								REMOVE_ANIM_DICT("reaction@male_stand@big_variations@b")
								REMOVE_ANIM_DICT("reaction@male_stand@big_intro@left")
								REMOVE_ANIM_DICT("reaction@male_stand@big_intro@right")
								REMOVE_ANIM_DICT("reaction@male_stand@big_intro@backward")
								REMOVE_ANIM_DICT("misschinese2_barrelRoll")		
								REMOVE_ANIM_SET("move_m@gangster@var_e")
								REMOVE_ANIM_SET("move_m@gangster@var_f")
								REMOVE_ANIM_SET("move_m@gangster@generic")
								SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING)
								SET_MODEL_AS_NO_LONGER_NEEDED(DUBSTA)
					
								SET_ROADS_IN_AREA(<<2469.9854, 4961.0449, 44.8113>>-<<100,100,100>>,<<2469.9854, 4961.0449, 44.8113>>+<<100,100,100>>,TRUE)
								kill_event(events_pin_interior)		
								kill_event(events_remove_vehicles_and_peds)
								IF int_farm != NULL
									UNPIN_INTERIOR(int_farm)
									int_farm = null
								ENDIF
								events[iE].flag=-20

							ENDIF
							
							
							
							
						BREAK/*
						case 2
																						
								houseInterior = GET_INTERIOR_AT_COORDS(<< 2440.3916, 4967.5122, 52.3472 >>)
								IF IS_VALID_INTERIOR(houseInterior) //and IS_INTERIOR_READY(interior)								
									PIN_INTERIOR_IN_MEMORY(houseInterior)
									events[iE].flag++
								ENDIF									
							
						BREAK
						case 3
							IF IS_INTERIOR_READY(houseInterior)
								events[iE].flag++
							endif
						break*/
						case 4
							if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 2460.0728, 4964.1758, 46.2405 >>) > 300.0
								events[iE].flag=1
							ELSe
								IF missionStage >= farm_cutscene
								AND NOT is_event_active(events_pin_interior)
									do_spawn_house()
									
									
									//events[iE].active = FALSE
									//events[iE].flag=5
									events[iE].active = FALSE
								ENDIF
							ENDIF
						break
						case 5
							
						break
					ENDSWITCH
				BREAK
				
				
				case events_pin_interior
					//adding because of bug 499704
					
					switch events[iE].flag
						CASE CLEANUP
							IF int_farm != null
								UNPIN_INTERIOR(int_farm)
							endif
							int_farm = null
							
						BREAK
						case 0
							int_farm = GET_INTERIOR_AT_COORDS(<< 2440.4924, 4969.9482, 52.5247 >>)
							events[iE].flag++
						break
						case 1
							IF NOT IS_VALID_INTERIOR(int_farm)
								int_farm = GET_INTERIOR_AT_COORDS(<< 2440.4924, 4969.9482, 52.5247 >>)
							ELSE
								PIN_INTERIOR_IN_MEMORY(int_farm)
								events[iE].flag++
							ENDIF
						break
						case 2
						//	IF IS_INTERIOR_READY(int_farm)
						//		SET_INTERIOR_ACTIVE(int_farm, TRUE)	
								events[iE].active = false
						//	ENDIF
						break
					ENDSWITCH
					
				break

				case events_reset_house_rayfire
					SWITCH events[iE].flag
						CASE 0
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_NORMAL)
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_NORMAL)
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_NORMAL)
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_NORMAL)
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_DESTROYED)
							events[iE].flag++
						BREAK
						CASE 1
						BREAK
						/*
					rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
		  			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)    
						//SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_STARTING)
						
						
		               // SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_STARTING)
						////cprintln(DEBUG_TREVOR3,"RESET MODEL GLASS")
						REMOVE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_ret_fhglassfrm,false)
						REMOVE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGLASSFRMSML,false)
						REMOVE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGlassairfrm,false)
				
						events[iE].active = FALSE
					endif*/
					ENDSWITCH
				BREAK
				
				CASE events_release_bottle_shooting_anims
					IF events[iE].flag = 0 //so it won't run this in cleanup
						int iPed
						iPed = get_ped_with_placement(pos_shooting_bottle_1)
						IF iPed != -1
							IF NOT IS_PED_INJURED(zped[iPed].id)
								IF GET_SCRIPT_TASK_STATUS(zped[iPed].id,script_TASK_SYNCHRONIZED_SCENE) = FINISHED_TASK
									iPed = get_ped_with_placement(pos_shooting_bottle_2)
									IF iPed != -1
										IF NOT IS_PED_INJURED(zped[iPed].id)
											IF GET_SCRIPT_TASK_STATUS(zped[iPed].id,script_TASK_SYNCHRONIZED_SCENE) = FINISHED_TASK
												REMOVE_ANIM_DICT("misschinese2_bank5")
												events[iE].active = FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								iPed = get_ped_with_placement(pos_shooting_bottle_2)
								IF iPed != -1
									IF NOT IS_PED_INJURED(zped[iPed].id)
										IF GET_SCRIPT_TASK_STATUS(zped[iPed].id,script_TASK_SYNCHRONIZED_SCENE) = FINISHED_TASK
											REMOVE_ANIM_DICT("misschinese2_bank5")
											events[iE].active = FALSE
										ENDIF
									ELSE
										REMOVE_ANIM_DICT("misschinese2_bank5")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIf
				BREAK
				
				CASE events_remove_vehicles_and_peds	
					
					switch events[iE].flag
						case -1 //cleanup
							REMOVE_SCENARIO_BLOCKING_AREAS()							
						BREAK
						case 0
							ADD_SCENARIO_BLOCKING_AREA(<<2445.654053,4972.897461,58.617897>>-<<100,100,100>>,<<2445.654053,4972.897461,58.617897>>+<<100,100,100>>)
								
							events[iE].flag++
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(player_ped_id())						
								if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),<<2473.061035,4981.482422,24.645840>> ) < 250.0																													
									events[iE].flag++
								ENDIF
							ENDIF
						BREAK
						case 2
							SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
							SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
							SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0,0.0)
							if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),<<2473.061035,4981.482422,24.645840>> ) > 250.0																						
								events[iE].flag++
							ENDIF													
						BREAK
					ENDSWITCH
				BREAK
				
				
				
				CASE events_control_dispatch
					switch events[iE].flag
						case -1 //cleanup
							ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,TRUE)
								ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,TRUE)
								ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,TRUE)
								ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,TRUE)
						BREAK
						case 0
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<< 2578.5234, 4982.2837, 51.4416 >>) < 500.0
								ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,FALSE)
								ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,FALSE)
								ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,FALSE)
								ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,FALSE)
								events[iE].flag++
							endif
						break
						case 1
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<< 2578.5234, 4982.2837, 51.4416 >>) > 500.0
								ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,TRUE)
								ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT,TRUE)
								ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,TRUE)
								ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST,TRUE)
								events[iE].flag=0							
							endif
						break
					ENDSWITCH
				break	
				
				case events_continuous_stats_tracking
					switch events[iE].flag
						CASE 0
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(),CHI2_UNMARKED) 
							events[iE].flag++
						BREAK
						CASE 1
							IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
								vehicle_index aVeh
								aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
								INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(aVeh,CHI2_CAR_DAMAGE)
								INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(aVeh)
								events[iE].flag++							
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
								INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null,CHI2_CAR_DAMAGE)
								INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null)
								
								events[iE].flag=1
							ENDIF
						BREAK
					ENDSWITCH
				break
				
				case events_petrol_help
				/*
					switch events[iE].flag
						case 0
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("FRMPET2")
								events[iE].active=false
							endif
						break						
					ENDSWITCH	
					*/
				break
				
				
				case events_explode_petrol_can
				/*
					IF DOES_ENTITY_EXIST(oFuelCan)
						IF NOT IS_ENTITY_DEAD(oFuelCan)		
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(oFuelCan)
								ADD_EXPLOSION(GET_ENTITY_COORDS(oFuelCan),EXP_TAG_HI_OCTANE)
								SET_ENTITY_HEALTH(oFuelCan,0)
								events[iE].active = false
							ENDIF
						ENDIF
					ENDIF*/
				break
				
				
				case events_Trevor_walks_in_final_cutscene
					switch events[iE].flag
						case 0
							#if IS_DEBUG_BUILD
								IF bExplodeHouseLoopShort
									events[iE].intA = 0
								ELSE
									events[iE].intA = get_game_timer()+6800
								ENDIF
							#endif
							#IF IS_FINAL_BUILD
								events[iE].intA = get_game_timer()+7000
							#endif
							events[iE].flag++
						break
						case 1
							if get_game_timer() > events[iE].intA
								SET_ENTITY_COORDS(player_ped_id(),<< 2466.3850, 4953.0957, 44.1228 >>)
								SET_ENTITY_HEADING(player_ped_id(),221)
								TASK_GO_STRAIGHT_TO_COORD(player_ped_id(),<< 2475.0747, 4945.9653, 44.0228 >>,pedmove_walk)
								FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_WALK)
								events[iE].active = false
							endif
						break						
					ENDSWITCH
				break
				
				case events_play_audio_stream
					IF LOAD_STREAM("CHINESE2_FARMHOUSE_EXPLOSION_MOLOTOV_MASTER")
						PLAY_STREAM_FRONTEND()
						events[iE].active = false
					ENDIF
				BREAK
				
				case events_house_explodes_rayfire
					switch events[iE].flag
						CASE 0															
                  			rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
                  			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)                        
    		                    SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_STARTING)	                                                
								
                        		events[iE].flag++
							endif
														
						break
						
						case 1
							IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire) = RFMO_STATE_START
								SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_PRIMING)	                                                
                        		events[iE].flag++
							ENDIF
						break
						
						case 2
							IF bRayfireTriggered=TRUE
			               		 IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire) = RFMO_STATE_PRIMED
								
			                		SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_START_ANIM)
									START_AUDIO_SCENE("CHI_2_RAYFIRE")
									
									CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_ret_fhglassfrm,true)
									CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGLASSFRMSML,true)
									CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGlassairfrm,false)
									events[iE].flag++
				                	
								ENDIF
							ENDIF			                			                  
			            BREAK
						case 3
							playmusic("CHN2_EXPLODE",TRUE,"CHN2_EXPLODE")
							events[iE].active=false
							
						break
					ENDSWITCH
				BREAK
			
				
				
				case events_check_petrol_trail
					IF events[iE].flag != CLEANUP
						IF NOT HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)	
						OR NOT DOES_PICKUP_EXIST(pickup_gas)					
							oGasCan = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()),20.0,GET_WEAPONTYPE_MODEL(WEAPONTYPE_PETROLCAN),FALSE)
									
							IF oGasCan != null
								if does_blip_exist(missionBlip)
									REMOVE_BLIP(missionBlip)
								endif
								missionBlip = CREATE_BLIP_FOR_OBJECT(oGasCan)							
								HIDE_GAS_TRAIL()
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
							endif
						ELSE
							events[iE].intA = 0
							HIDE_GAS_TRAIL(FALSE)
							
							if does_blip_exist(missionBlip)
								REMOVE_BLIP(missionBlip)
							endif
							
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(CHI2_GAS_POUR_TIME)
							
							IF gasInCan = 0										
								gasInCan = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
							ELSE
								INFORM_MISSION_STATS_OF_INCREMENT(CHI2_GAS_USED,gasInCan - GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN),TRUE)
							ENDIF
						ENDIF											
					
						IF IS_GAS_TRAIL_COMPLETE()
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
							
							events[iE].active = false
					
							add_event(events_check_player_out_front)
						ENDIF
					ENDIF
				break
				
				case events_check_trail_burning_safe
					//gas trail is burning
					//player is not inside house
					//point from fire to cellar is a complete trail
					
				BREAK
				
				
			
				
				case events_check_player_out_front
					IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<2447.671387,4951.438477,43.255756>>, <<2454.663818,4957.855957,47.891174>>, 6.812500)
					and GET_GAME_TIMER() > events[iE].intA
						IF DOES_BLIP_EXIST(missionBlip)
							REMOVE_BLIP(missionBlip)
						endif
						events[iE].active = FALSE
					else
						switch events[iE].flag
							case 0
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PRINT("FRMSHP",DEFAULT_GOD_TEXT_TIME,1)								
									events[iE].intA = GET_GAME_TIMER() + 2000
									missionBlip = CREATE_BLIP_FOR_COORD(<<2450,4954,45>>)
									events[iE].flag++
								ENDIF
							break
						endswitch
					endif
				break
				
				case events_detach_barrels_from_peds	
					//0 = does barrel 1 ped exist
					//1 = does barrel 2 ped exist
					//0 = barrel ped 1 has barrel atttached and is playing anim
					//1 = barrel ped 2 has barrel attached and is playing anim
					
					//
					
					IF NOT IS_BIT_SET(events[iE].intA,0)														
						iBarrels1 = get_ped_with_placement(pos_barrels_1)
						If iBarrels1 != -1
							set_bit(events[iE].intA,0)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(events[iE].intA,1) //barrel not attached
							IF NOT IS_PED_INJURED(zped[iBarrels1].id)
							AND DOES_ENTITY_EXIST(zPED[iBarrels1].obj)							
								IF IS_ENTITY_PLAYING_ANIM(zPED[iBarrels1].id, "misschinese2_barrelroll", "barrel_roll_loop_B")
								AND IS_ENTITY_ATTACHED(zPED[iBarrels1].obj)
									set_bit(events[iE].intA,1)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(events[iE].intA,2) //barrel not attached
								IF IS_PED_INJURED(zPED[iBarrels1].id)
									SET_BIT(events[iE].intA,2)
								ENDIF
									
								IF NOT IS_PED_INJURED(zPED[iBarrels1].id)																
									IF NOT IS_ENTITY_PLAYING_ANIM(zPED[iBarrels1].id, "misschinese2_barrelroll", "barrel_roll_loop_B") //this alone won't work as peds are still considered playing an anim even when they're reactin
										SET_BIT(events[iE].intA,2)
									ENDIF
												
									IF GET_SCRIPT_TASK_STATUS(zPED[iBarrels1].id,script_task_play_anim) = DORMANT_TASK
									OR GET_SCRIPT_TASK_STATUS(zPED[iBarrels1].id,script_task_play_anim) = FINISHED_TASK							
										SET_BIT(events[iE].intA,2)
									ENDIF
												
									IF IS_PED_IN_COMBAT(zPED[iBarrels1].id,player_ped_id())
									OR pedReactionState[iBarrels1].state > state_ambient
										SET_BIT(events[iE].intA,2)
									ENDIF
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(zPED[iBarrels1].obj)
								AND IS_ENTITY_ATTACHED(zPED[iBarrels1].obj)
									////cprintln(debug_Trevor3,"detach barrel 1")
									DETACH_ENTITY(zPED[iBarrels1].obj,FALSE,FALSE)
									////cprintln(debug_Trevor3,"barrel 1 detached")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					
					
					IF NOT IS_BIT_SET(events[iE].intA,4)														
						iBarrels2 = get_ped_with_placement(pos_barrels_2)
						If iBarrels2 != -1
							set_bit(events[iE].intA,4)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(events[iE].intA,5) //barrel not attached
							IF NOT IS_PED_INJURED(zped[iBarrels2].id)							
								IF DOES_ENTITY_EXIST(zPED[iBarrels2].obj)									
									IF IS_ENTITY_PLAYING_ANIM(zPED[iBarrels2].id, "misschinese2_barrelroll", "barrel_roll_loop_A")									
										IF IS_ENTITY_ATTACHED(zPED[iBarrels2].obj)											
											set_bit(events[iE].intA,5)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(events[iE].intA,6) //barrel not attached
								IF IS_PED_INJURED(zPED[iBarrels2].id)
									SET_BIT(events[iE].intA,6)
								ENDIF
									
								IF NOT IS_PED_INJURED(zPED[iBarrels2].id)																
									IF NOT IS_ENTITY_PLAYING_ANIM(zPED[iBarrels2].id, "misschinese2_barrelroll", "barrel_roll_loop_A") //this alone won't work as peds are still considered playing an anim even when they're reactin
										SET_BIT(events[iE].intA,6)
									ENDIF
												
									IF GET_SCRIPT_TASK_STATUS(zPED[iBarrels2].id,script_task_play_anim) = DORMANT_TASK
									OR GET_SCRIPT_TASK_STATUS(zPED[iBarrels2].id,script_task_play_anim) = FINISHED_TASK							
										SET_BIT(events[iE].intA,6)
									ENDIF
												
									IF IS_PED_IN_COMBAT(zPED[iBarrels2].id,player_ped_id())
									OR pedReactionState[iBarrels2].state > state_ambient
										SET_BIT(events[iE].intA,6)
									ENDIF
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(zPED[iBarrels2].obj)
								AND IS_ENTITY_ATTACHED(zPED[iBarrels2].obj)
									////cprintln(debug_Trevor3,"detach barrel 2")
									DETACH_ENTITY(zPED[iBarrels2].obj,FALSE,FALSE)
									////cprintln(debug_Trevor3,"barrel 2 detached")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
								

																				
				break
				
				case events_never_do_locates_skips
					#IF IS_DEBUG_BUILD  DONT_DO_J_SKIP(locatesData) #endif
				break
				
				
				
				
				
				
				
				
				case events_spawn_bottles
					int j
					j=0
					switch events[iE].flag
						case 1
						
							oCans[0] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.4958, 4970.9160, 44.5112>>)
							SET_ENTITY_ROTATION(oCans[0],<<-3.4400, -0.0000, 38.8800>>)
							oCans[1] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.8630, 4970.5952, 44.5425>>)
							SET_ENTITY_ROTATION(oCans[1],<<-3.4400, -0.0000, 38.8800>>)
							oCans[2] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.8955, 4970.4175, 44.5475>>)
							SET_ENTITY_ROTATION(oCans[2],<<-3.4400, -0.0000, 38.8800>>)
							oCans[3] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.7954, 4970.2456, 44.5525>>)
							SET_ENTITY_ROTATION(oCans[3],<<-3.4400, -0.0000, 38.8800>>)
							oCans[4] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.2041, 4970.2690, 44.5201>>)
							SET_ENTITY_ROTATION(oCans[4],<<-3.4400, -0.0000, 38.8800>>)
							oCans[5] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.3716, 4970.7441, 44.5100>>)
							SET_ENTITY_ROTATION(oCans[5],<<-3.4400, -0.0000, 38.8800>>)
							oCans[6] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.3716, 4971.0918, 44.4987>>)
							SET_ENTITY_ROTATION(oCans[6],<<-3.4400, -0.0000, 38.8800>>)
							oCans[7] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2493.7695, 4971.0000, 44.6287>>)
							SET_ENTITY_ROTATION(oCans[7],<<3.9550, 0.0000, 139.6800>>)
							oCans[8] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2493.9797, 4971.2793, 44.6038>>)
							SET_ENTITY_ROTATION(oCans[8],<<3.9550, 0.0000, 139.6800>>)
							oCans[9] = CREATE_OBJECT_NO_OFFSET(prop_ld_can_01,<<2494.1042, 4971.4375, 44.5826>>)
							SET_ENTITY_ROTATION(oCans[9],<<3.9550, 0.0000, 139.6800>>)
							repeat count_of(oCans) j
								SET_ENTITY_DYNAMIC(oCans[j],TRUE)
								FREEZE_ENTITY_POSITION(oCans[j],TRUE)
							ENDREPEAT
							events[iE].flag++
						break
						
						case 0
							CPRINTLN(debug_Trevor3,"Attempt Pallets Frozen")
							IF NOT DOES_ENTITY_EXIST(obja)
							AND NOT DOES_ENTITY_EXIST(objb)
								IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<2493.5,4969.2,43.9>>,1.0,prop_barrel_pile_03)
								AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<2493.4,4971.8,43.6>>,1.0,PROP_PALLET_PILE_01)
									
									CPRINTLN(debug_Trevor3,"Pallets Frozen")
									obja = GET_CLOSEST_OBJECT_OF_TYPE(<<2493.5,4969.2,43.9>>,1.0,prop_barrel_pile_03)
									FREEZE_ENTITY_POSITION(obja,TRUE)
									
								//	objb = CREATE_OBJECT_NO_OFFSET(PROP_PALLET_PILE_01,<<2493.4,4971.8,43.6>>)
								//	GET_CLOSEST_OBJECT_OF_TYPE(<<2493.4,4971.8,43.6>>,1.0,PROP_PALLET_PILE_01)
								//	FREEZE_ENTITY_POSITION(objb,TRUE)
									
									CREATE_MODEL_HIDE(<<2493.4,4971.8,43.6>>,1.0,PROP_PALLET_PILE_01,false)
									objb = CREATE_OBJECT (prop_pallet_pile_01, <<2493.427490,4971.728516,43.543625>>)
									SET_ENTITY_ROTATION (objb, <<-1.436373,0.176147,-36.660099>>)
									FREEZE_ENTITY_POSITION(objb,TRUE)
									
									add_event(events_unfreeze_pallet)
									events[iE].flag++
							
								ENDIF
							ELSE
								cprintln(debug_Trevor3,"Recreated pallet")
								DELETE_OBJECT(objb)
								objb = CREATE_OBJECT (prop_pallet_pile_01, <<2493.427490,4971.728516,43.543625>>)
								SET_ENTITY_ROTATION (objb, <<-1.436373,0.176147,-36.660099>>)
								FREEZE_ENTITY_POSITION(objb,TRUE)
								add_event(events_unfreeze_pallet)
								events[iE].flag++
							
							ENDIF
						BREAK
						

						case 2
							
								//int counter
								j=0
								//counter=0
								repeat count_of(oCans) j
									IF DOES_ENTITY_EXIST(oCans[j])
										//IF DOES_ENTITY_HAVE_PHYSICS(oCans[j])
											FREEZE_ENTITY_POSITION(oCans[j],FALSE)
										//	counter++
										//ENDIF
									endif
								ENDREPEAT
								events[iE].active = FALSE
								//IF counter = count_of(oCans)
									//events[iE].flag++
									events[iE].active = FALSE
								//ENDIF
						break
					ENDSWITCH
				break
				
				case events_unfreeze_pallet
					IF NOT IS_BIT_SET(events[iE].intA,0)
						IF DOES_ENTITY_EXIST(obja)
							IF NOT IS_PED_INJURED(player_ped_id())
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(objA,player_ped_id())
								OR GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),objA) < 30.0
									FREEZE_ENTITY_POSITION(objA,FALSE)
									SET_BIT(events[iE].intA,0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(events[iE].intA,1)
						IF DOES_ENTITY_EXIST(objb)
							IF NOT IS_PED_INJURED(player_ped_id())
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(objb,player_ped_id())
								OR GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),objb) < 30.0
									FREEZE_ENTITY_POSITION(objb,FALSE)
									SET_BIT(events[iE].intA,1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_BIT_SET(events[iE].intA,0) AND IS_BIT_SET(events[iE].intA,1)
					OR missionStage = inside_house
						IF DOES_ENTITY_EXIST(obja)
							FREEZE_ENTITY_POSITION(objA,FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(objb)
							FREEZE_ENTITY_POSITION(objb,FALSE)
						ENDIF
						events[iE].active = FALSE
					ENDIF
				break
				
				case events_animate_bar					
					switch events[iE].flag
						CASE CLEANUP
							IF DOES_ENTITY_EXIST(zped[ped_janet].id)
								DELETE_PED(zped[ped_janet].id)
							ENDIF
							IF DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
								DELETE_PED(zped[ped_oldMan1].id)
							ENDIF
							IF DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
								DELETE_PED(zped[ped_oldMan2].id)
							ENDIF
							SET_MODEL_AS_NO_LONGER_NEEDED(ig_janet)
							SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN1A)
							SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN2)
							events[iE].active = FALSE
						BREAK
						CASE 0													
							REQUEST_ANIM_DICT("MISSChinese2_crystalMazeMCS1_IG")							
							events[iE].flag++							
						BREAK
						CASE 1								
							IF HAS_ANIM_DICT_LOADED("MISSChinese2_crystalMazeMCS1_IG")			
								IF HAS_CUTSCENE_FINISHED()	
								OR NOT IS_CUTSCENE_PLAYING()
									IF DOES_ENTITY_EXIST(zped[ped_oldMan1].id) AND NOT IS_PED_injured(zped[ped_oldMan1].id)
										SET_ENTITY_COORDS(zped[ped_oldMan1].id,<<1986.0602, 3051.7085, 46.2151>>)
									ENDIF
									IF DOES_ENTITY_EXIST(zped[ped_oldMan2].id) AND NOT IS_PED_injured(zped[ped_oldMan2].id)
										SET_ENTITY_COORDS(zped[ped_oldMan2].id,<<1986.1051, 3051.6592, 46.2151>>)
									ENDIF
									events[iE].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							
							IF DOES_SCENARIO_EXIST_IN_AREA(<<1984.89, 3052.46, 46.98>>,0.3,TRUE)
							AND DOES_SCENARIO_EXIST_IN_AREA(<<1985.3221, 3053.0872, 46.98>>,0.3,TRUE)
								IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
									zped[ped_oldMan1].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN1A,<<1987.231,3052.741,46.214>>)
								ENDIF
								IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
									zped[ped_oldMan2].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN2,<<1987.231,3052.741,46.214>>)
								ENDIF
								IF NOT DOES_ENTITY_EXIST(zped[ped_janet].id)
									cprintln(debug_trevor3,"Created Janet")
									zped[ped_janet].id = create_ped(PEDTYPE_MISSION,IG_JANET,<<1987.231,3052.741,46.214>>)
								ENDIF
								
								SET_BIT(events[iE].intA,1)
								TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[ped_oldMan2].id,<<1984.89, 3052.46, 46.98>>,0.3)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[ped_oldMan2].id,true,true)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_oldMan2].id,FALSE)
								cprintln(debug_trevor3,"END B")
								
								SET_BIT(events[iE].intA,0)
								TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[ped_oldMan1].id,<<1985.3221, 3053.0872, 46.98>>,0.3)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[ped_oldMan1].id,true,true)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_oldMan1].id,FALSE)
								cprintln(debug_trevor3,"END C")
								
								
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(events[iE].intA)
									events[iE].intA = CREATE_SYNCHRONIZED_SCENE(<<1987.231,3052.741,46.214>>,<<0,0,57.6>>)
									SET_SYNCHRONIZED_SCENE_LOOPED(events[iE].intA,TRUE)	
								ENDIF
								TASK_SYNCHRONIZED_SCENE(zped[ped_janet].id,events[iE].intA,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_action_janet",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[ped_janet].id,true,true)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_janet].id,FALSE)
								events[iE].intA = 0
								events[iE].flag++
							ENDIF
								
						BREAK
										/*
									
								IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Old_Man1A")
								OR (missionStage = drive_to_farm AND NOT DOES_ENTITY_EXIST(zped[ped_oldMan1].id))
									cprintln(debug_trevor3,"END C1")
									IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
										zped[ped_oldMan1].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN1A,<<1987.231,3052.741,46.214>>)
									ENDIF
									IF NOT IS_PED_INJURED(zPED[ped_oldMan1].id)
										cprintln(debug_trevor3,"END C2")
										IF DOES_SCENARIO_EXIST_IN_AREA(<<1984.89, 3052.46, 46.98>>,0.3,TRUE)																			
											SET_BIT(events[iE].intA,0)
											TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[ped_oldMan1].id,<<1984.89, 3052.46, 46.98>>,0.3)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[ped_oldMan1].id,true,true)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_oldMan1].id,FALSE)
											cprintln(debug_trevor3,"END C")
										ELSE
											SET_ENTITY_COORDS(zped[ped_oldMan1].id,<<1986.0602, 3051.7085, 46.2151>>)
										ENDIF
									ENDIF
								ENDIF
								
								IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Old_Man2")
								OR (missionStage = drive_to_farm AND NOT DOES_ENTITY_EXIST(zped[ped_oldMan2].id))
									IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
										zped[ped_oldMan2].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN2,<<1987.231,3052.741,46.214>>)
									ENDIF
									IF NOT IS_PED_INJURED(zPED[ped_oldMan2].id)
										IF DOES_SCENARIO_EXIST_IN_AREA(<<1985.3221, 3053.0872, 46.98>>,0.3,TRUE)									
											SET_BIT(events[iE].intA,1)
											TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[ped_oldMan2].id,<<1985.3221, 3053.0872, 46.98>>,0.3)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[ped_oldMan2].id,true,true)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_oldMan2].id,FALSE)
											cprintln(debug_trevor3,"END B")
										ENDIF
									ENDIF
								ENDIF
																
								IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Janet")
								OR (missionStage = drive_to_farm AND NOT DOES_ENTITY_EXIST(zped[ped_janet].id))
									cprintln(debug_trevor3,"Exit state hit Janet")
									IF NOT DOES_ENTITY_EXIST(zped[ped_janet].id)
										cprintln(debug_trevor3,"Created Janet")
										zped[ped_janet].id = create_ped(PEDTYPE_MISSION,IG_JANET,<<1987.231,3052.741,46.214>>)
									ENDIF
									IF NOT IS_PED_INJURED(zPED[ped_janet].id)
										////cprintln(debug_trevor3,"Janet tasked")
										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(events[iE].intA)
											events[iE].intA = CREATE_SYNCHRONIZED_SCENE(<<1987.231,3052.741,46.214>>,<<0,0,57.6>>)
											SET_SYNCHRONIZED_SCENE_LOOPED(events[iE].intA,TRUE)	
										ENDIF
										TASK_SYNCHRONIZED_SCENE(zped[ped_janet].id,events[iE].intA,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_action_janet",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zped[ped_janet].id,true,true)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_janet].id,FALSE)
										cprintln(debug_trevor3,"END A")
									ENDIF
								ENDIF
						
						
								IF HAS_CUTSCENE_FINISHED()	
								OR NOT IS_CUTSCENE_PLAYING()
									cprintln(debug_trevor3,"cut finished")
									events[iE].flag++							
								ENDIF
							
							ENDIF
						BREAK*/
					
						
						case 3
						
							IF HAS_CUTSCENE_FINISHED()
							OR NOT IS_CUTSCENE_PLAYING()
							/*	IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
									zped[ped_oldMan1].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN1A,<<1985.8699, 3051.3889, 46.2151>>)
								ELSE
									IF NOT IS_PED_INJURED(zped[ped_oldMan1].id)
										SET_ENTITY_COORDS(zped[ped_oldMan1].id,<<1985.8699, 3051.3889, 46.2151>>)
									ENDIF
								ENDIF								
							
							
								IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
									zped[ped_oldMan2].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN2,<<1986.9213, 3052.9172, 46.2152>>)
								ELSE
									IF NOT IS_PED_INJURED(zped[ped_oldMan2].id)
										SET_ENTITY_COORDS(zped[ped_oldMan2].id,<<1986.9213, 3052.9172, 46.2152>>)
									ENDIF
								ENDIF*/
					
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[ped_oldMan1].id)
							AND NOT IS_PED_INJURED(zped[ped_oldMan2].id)
								SET_PED_CONFIG_FLAG(zped[ped_oldMan1].id ,PCF_AlwaysRespondToCriesForHelp,true)
								SET_PED_CONFIG_FLAG(zped[ped_oldMan2].id ,PCF_AlwaysRespondToCriesForHelp,true)
								IF IS_BIT_SET(events[iE].intA,0)
								AND IS_BIT_SET(events[iE].intA,1)
									events[iE].flag++
								ELSE
			
									IF GET_SCRIPT_TASK_STATUS(zped[ped_oldMan1].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK
										IF DOES_SCENARIO_EXIST_IN_AREA(<<1984.89, 3052.46, 46.98>>,0.3,TRUE)																			
											SET_BIT(events[iE].intA,0)
											TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[ped_oldMan1].id,<<1984.89, 3052.46, 46.98>>,0.3)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_oldMan1].id,FALSE)
										ENDIF
									ELSE
										SET_BIT(events[iE].intA,0)
									ENDIF
				
									IF GET_SCRIPT_TASK_STATUS(zped[ped_oldMan2].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK								
										IF DOES_SCENARIO_EXIST_IN_AREA(<<1985.3221, 3053.0872, 46.98>>,0.3,TRUE)									
											SET_BIT(events[iE].intA,1)
											TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[ped_oldMan2].id,<<1985.3221, 3053.0872, 46.98>>,0.3)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_oldMan2].id,FALSE)
										ENDIF
									ELSE
										SET_BIT(events[iE].intA,1)
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 4/*
							IF NOT IS_PED_INJURED(zped[ped_janet].id)
								IF GET_SCRIPT_TASK_STATUS(zped[ped_janet].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
									IF GET_SCRIPT_TASK_STATUS(zped[ped_janet].id,SCRIPT_TASK_ANY) != PERFORMING_TASK
									
										events[iE].flag=5
										
									ENDIF
								ENDIF
							ELSE
								events[iE].flag=5
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_oldMan1].id)
								IF GET_SCRIPT_TASK_STATUS(zped[ped_oldMan1].id,SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) != PERFORMING_TASK
									IF GET_SCRIPT_TASK_STATUS(zped[ped_oldMan1].id,SCRIPT_TASK_ANY) != PERFORMING_TASK
									
										events[iE].flag=5
									ENDIF
								ENDIF
							ELSE
								events[iE].flag=5
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_oldMan2].id)
								IF GET_SCRIPT_TASK_STATUS(zped[ped_oldMan2].id,SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) != PERFORMING_TASK
									IF GET_SCRIPT_TASK_STATUS(zped[ped_oldMan2].id,SCRIPT_TASK_ANY) != PERFORMING_TASK
					
										events[iE].flag=5
									ENDIF
								ENDIF
							ELSE
								events[iE].flag=5
							ENDIF
							*/
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1991.4193, 3054.9150, 47.5330>>) > 150
								events[iE].flag=CLEANUP
							ENDIF
							
						BREAK
						case 6
							IF NOT IS_PED_INJURED(zped[ped_janet].id)
								TASK_SMART_FLEE_PED(zped[ped_janet].id,player_ped_id(),100,-1)
								SET_PED_KEEP_TASK(zped[ped_janet].id,TRUe)
								SET_PED_AS_NO_LONGER_NEEDED(zped[ped_janet].id)								
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_oldMan1].id)
								TASK_SMART_FLEE_PED(zped[ped_oldMan1].id,player_ped_id(),100,-1)
								SET_PED_KEEP_TASK(zped[ped_oldMan1].id,true)
								SET_PED_AS_NO_LONGER_NEEDED(zped[ped_oldMan1].id)
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_oldMan2].id)								
								TASK_SMART_FLEE_PED(zped[ped_oldMan2].id,player_ped_id(),100,-1)
								SET_PED_KEEP_TASK(zped[ped_oldMan2].id,true)
								SET_PED_AS_NO_LONGER_NEEDED(zped[ped_oldMan2].id)										
							ENDIF
							events[iE].flag=7
							
						break
						case 7
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1991.4193, 3054.9150, 47.5330>>) > 150
								events[iE].flag=CLEANUP
							ENDIF
						break
						
					ENDSWITCH
				break
				
				
				
			
				
				case events_force_farm_doors_drawing
					CREATE_FORCED_OBJECT(<<2444.176270,4974.076660,56.406605>>, 30, V_ILEV_FH_DOOR03, TRUE)
					CREATE_FORCED_OBJECT(<<2444.176270,4974.076660,56.406605>>, 30, V_ILEV_FH_FRNTDOOR, TRUE)
					events[iE].active = FALSE
				break
				
			
				
				case events_fails
				/*
					IF cleaningUp = FALSE
						IF missionStage = do_petrol_trail
					
						endif
						
						//distance fail
						IF missionStage >= snipe_from_hill
						OR missionStage = get_to_house AND ARE_VECTORS_EQUAL(GET_BLIP_COORDS(locatesData.LocationBlip),<< 2578.5234, 4982.2837, 51.4416 >>)
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(), << 2460.8640, 4983.4932, 47.7573 >>,FALSE) > 350.0
							//	vector vfind 
							//	vfind = GET_ENTITY_COORDS(player_ped_id())
								FAIL_MISSION("FRMFAR")
							ENDIF						
						ENDIF
						
						IF is_event_active(events_animate_chinese)
							IF DOES_ENTITY_EXIST(zPED[ped_tao].id)
							AND DOES_ENTITY_EXIST(zPED[ped_taos_translator].id)
								IF IS_ENTITY_DEAD(zPED[ped_tao].id)
									FAIL_MISSION("FRMCHI")
								ENDIF
								IF IS_ENTITY_DEAD(zPED[ped_taos_translator].id)
									FAIL_MISSION("FRMCHI2")
								ENDIF
							ENDIF
						ENDIF
						
						
					ENDIF
					*/
				BREAK
			ENDSWITCH			
			if events[iE].flag = CLEANUP
				events[iE].active = FALSE
				events[iE].flag = 0
				events[iE].persists = FALSE
				events[iE].boolA = FALSE
				events[iE].intA = 0
			ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC



FUNC bool CREATE_GANG_PED(int ind, MODEL_NAMES mGangPedModel, VECTOR vGangPedCoord, FLOAT fGangPedHead, WEAPON_TYPE wepGang, bool bSetWeaponAsCurrent=TRUE, bool bInInterior=FALSE)
	mGangPedModel=mGangPedModel
	bInInterior=bInInterior
	INTERIOR_INSTANCE_INDEX interior
	
	
	
	IF zped[ind].role != role_guard_outside AND zped[ind].role != role_chicken
		interior = GET_INTERIOR_AT_COORDS(vGangPedCoord)
		//PIN_INTERIOR_IN_MEMORY(interior)
		IF interior = null
		
			RETURN FALSE
		ELSE
			IF NOT IS_INTERIOR_READY(interior)
			
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	

	cprintln(debug_Trevor3,"Id : ",ind," variation: ",iNextPedVariation)
	
		
	SWITCH iNextPedVariation
		CASE 0
		
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(zped[ind].id, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(zped[ind].id, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(zped[ind].id, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
		BREAK
		CASE 1
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),1,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,1)
		BREAK
		CASE 2
			 zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
			 SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,0)
		BREAK
		CASE 3
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),1,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),1,0)
		BREAK
		CASE 4
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,0)
		BREAK
		CASE 5
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),1,2)
		BREAK
		CASE 6
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),1,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,1)
		BREAK
		CASE 7
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),2,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),2,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,2)
		BREAK
		CASE 8
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),1,0)			
		BREAK
		CASE 9
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,2)
		BREAK
		CASE 10
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,1)
		BREAK
		CASE 11
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),2,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),2,2)
		BREAK
		CASE 12
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),1,2)
		BREAK
		CASE 13
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),1,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,2)
		BREAK
		CASE 14
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,2)
		BREAK
		CASE 15
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),2,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,2)
		BREAK
		CASE 16
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),0,1)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),1,1)
		BREAK
		CASE 17
			zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_M_Hillbilly_02,vGangPedCoord,fGangPedHead)
			SET_PED_DEFAULT_COMPONENT_VARIATION(zped[ind].id)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,0),0,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,2),2,0)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,3),1,2)
			SET_PED_COMPONENT_VARIATION(zped[ind].id,INT_TO_ENUM(PED_COMPONENT,4),0,1)
		BREAK
	ENDSWITCH
	
	
	IF iNextPedVariation > 17
	
		cprintln(debug_Trevor3,"FUCKERS")
	
		zped[ind].id = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,vGangPedCoord,fGangPedHead)
		SET_PED_RANDOM_COMPONENT_VARIATION(zped[ind].id)		
	ENDIF
	iNextPedVariation++
	
	//zped[ind].id = CREATE_PED (PEDTYPE_MISSION, mGangPedModel, vGangPedCoord, fGangPedHead  )
	
	IF zped[ind].role = role_innocent
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ind].id,TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(zped[ind].id, HASH_IGNORE_GROUP)
	else
		GIVE_WEAPON_TO_PED(zped[ind].id, wepGang, 3000, bSetWeaponAsCurrent,TRUE)
		//cprintln(debug_Trevor3,"Give weapon to ped: ",GET_PLACEMENT_STRING(ind))

		SET_PED_RELATIONSHIP_GROUP_HASH(zped[ind].id, HASH_FOE_GROUP)
		SET_ped_AS_ENEMY(zped[ind].id, TRUE)
					
		SET_PED_COMBAT_ATTRIBUTES(zped[ind].id,CA_WILL_SCAN_FOR_DEAD_PEDS,TRUE)		
		SET_PED_HEARING_RANGE(zped[ind].id,120.0)
		SET_PED_SEEING_RANGE(zped[ind].id,22.0)
		
		SET_PED_ID_RANGE(zped[ind].id,22.0)
		//SET_PED_ID_RANGE(zped[ind].id,3.0)
		SET_PED_CONFIG_FLAG(zped[ind].id,PCF_RunFromFiresAndExplosions,false)
		SET_COMBAT_FLOAT(zped[ind].id,CCF_BULLET_IMPACT_DETECTION_RANGE,3.0)
			
		if zped[ind].role = role_guard_house
			SET_PED_ACCURACY(zped[ind].id,12)
		ELSE
			SET_PED_ACCURACY(zped[ind].id,45)
		endif				

		SET_PED_DIES_WHEN_INJURED (zped[ind].id, TRUE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(zped[ind].id, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(zped[ind].id,CA_CAN_USE_RADIO,FALSE)
		SET_PED_COMBAT_ATTRIBUTES(zped[ind].id,CA_ALWAYS_FLEE,FALSE)
		
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(zped[ind].id, TRUE)
	endif
	
	STOP_PED_WEAPON_FIRING_WHEN_DROPPED(zped[ind].id)

	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(zped[ind].id,TRUE)
	
	// JR - seems safer to leave this on.
	//SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(zped[ind].id, TRUE)
	SET_PED_SHOULD_PLAY_DIRECTED_NORMAL_SCENARIO_EXIT(zPED[ind].id, <<0,0,0>>)
	SET_PED_SHOULD_IGNORE_SCENARIO_NAV_CHECKS(zPED[ind].id, TRUE)
	SET_PED_SHOULD_PROBE_FOR_SCENARIO_EXITS_IN_ONE_FRAME(zPED[ind].id, TRUE)
	SET_ENTITY_HEALTH(zped[ind].id, 150)
	
	
	
	RETURN TRUE
ENDFUNC


// =================================================== NEW HANDLING OF GAME ====================================================================

FUNC BOOL SET_CONDITION_STATE(enumconditions thisCondition, bool setReturns)
	int j

	REPEAT COUNT_OF(conditions) j		
		IF thisCondition = conditions[j].condition
			IF setReturns = TRUE
				conditions[j].returns = TRUE				
			ELSE
				conditions[j].returns = FALSE
			ENDIF
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CONDITION_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 
	
	IF iArray >= 0 AND iArray < COUNT_OF(conditions)	
		IF conditions[iArray].condition = conditiontoCheck
			IF /*conditions[iArray].active
			AND */conditions[iArray].returns
				RETURN TRUE
			ENDIF
		ELSE
			IF conditions[iArray].condition != COND_NULL
				SCRIPT_ASSERT("SCRIPT: IS_CONDITION_TRUE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CONDITION_FALSE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 
	IF iArray >= 0 AND iArray < COUNT_OF(conditions)
		IF conditions[iArray].condition = conditiontoCheck
			IF /*conditions[iArray].active
			AND */NOT conditions[iArray].returns
				RETURN TRUE
			ENDIF
		ELSE
			IF conditions[iArray].condition != COND_NULL
				SCRIPT_ASSERT("SCRIPT: IS_CONDITION_FALSE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL WAS_CONDITION_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		IF /*conditions[iArray].active
		AND */conditions[iArray].wasTrue
			RETURN TRUE
		ENDIF
	ELSE		
		SCRIPT_ASSERT("SCRIPT: IS_CONDITION_TRUE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CONDITION_WAS_TRUE(enumconditions conditiontoCheck)
	int iArray = enum_to_int(conditiontoCheck) - enum_to_int(conditions[0].condition) 

	IF conditions[iArray].condition = conditiontoCheck
		conditions[iArray].wasTrue = TRUE		
	ELSE		
		SCRIPT_ASSERT("SCRIPT: IS_CONDITION_TRUE() : condition stored in array is not equal to value stored in conditionArrayEntry[]")
	ENDIF
	
ENDPROC

FUNC BOOL IS_ACTION_COMPLETE(int iEntry, enumActions actionToCheck)
	IF actions[iEntry].action = actionToCheck
		IF actions[iEntry].completed = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC bool andOrReturns(bool &pconditionsTrue, bool &pbracketOpen, andorEnum andOr, enumconditions cond)
	//IF NOT pbracketOpen 
	//OR (NOT pconditionsTrue AND pbracketOpen) 
		SWITCH andOr
			CASE cFORCEtrue
				pconditionsTrue = TRUE
			BREAK
			CASE cIGNORE
				RETURN FALSE
			BREAK
			CASE cIF
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))					
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cIFnot
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_FALSE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cOR
				IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
					pconditionsTrue = TRUE
				ENDIF
			BREAK
			CASE cORbracket
				IF pconditionsTrue
					RETURN FALSE
				ELSE
					pbracketOpen = TRUE
					IF (cond = COND_NULL OR (cond != COND_NULL AND IS_CONDITION_TRUE(cond)))
						pconditionsTrue = TRUE
					ENDIF
				ENDIF
			BREAK
			CASE cAND
				IF (cond != COND_NULL AND IS_CONDITION_FALSE(cond))
					pconditionsTrue = FALSE
				ENDIF
			BREAK
			CASE cANDNOT
				IF (cond != COND_NULL AND IS_CONDITION_TRUE(cond))					
					pconditionsTrue = FALSE
				ENDIF			
			BREAK

			CASE cWasTrueNowFalse
				IF WAS_CONDITION_TRUE(cond)
					IF IS_CONDITION_FALSE(COND)
						pconditionsTrue = TRUE
					ELSE
						pconditionsTrue = FALSE
					ENDIF
				ELSE
					IF IS_CONDITION_TRUE(cond)
						SET_CONDITION_WAS_TRUE(COND)						
					ENDIF
					pconditionsTrue = FALSE
				ENDIF
			BREAK		
		ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC bool checkANDOR(andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)
	conditionsTrue = FALSE
	bracketOpen = FALSE
	
	IF andOrReturns(conditionsTrue,bracketOpen,andOr1,cond1)
		IF andOrReturns(conditionsTrue,bracketOpen,andOr2,cond2)
			IF andOrReturns(conditionsTrue,bracketOpen,andOr3,cond3) 
				IF andOrReturns(conditionsTrue,bracketOpen,andOr4,cond4) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF conditionsTrue
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

//conditions
PROC checkConditions(enumconditions firstCondition, enumconditions lastCondition)
	
	int iLastArray = enum_to_int(lastCondition) - enum_to_int(firstCondition)
	IF conditions[0].condition != firstCondition
	AND conditions[iLastArray].condition != lastCondition
		int j
		int enumCounter = 0
		REPEAT COUNT_OF(conditions) j
			IF j <= iLastArray
				conditions[j].condition = int_to_enum(enumconditions,enum_to_int(firstCondition) + enumCounter)
				conditions[j].active = TRUE
				conditions[j].returns = FALSE
				conditions[j].wasTrue = FALSE
				enumCounter++
			ELSE
				conditions[j].active = FALSE
			ENDIF
		ENDREPEAT
		//reset which condition goes in to which array.
		
	ENDIF

	int i
	REPEAT COUNT_OF(conditions) i
		//IF conditions[i].active				
			SWITCH conditions[i].condition			
				CASE COND_INITIAL_INSTRUCTIONS_PLAYED
					IF NOT conditions[i].returns
						IF conditions[i].flag = 0
							IF IS_THIS_PRINT_BEING_DISPLAYED("FRMCHSE_1")
								conditions[i].flag = 1
							ENDIF
						ELSE
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("FRMCHSE_1")
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_IN_ANY_CAR
					conditions[i].returns = FALSE
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
				
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_BULLET_IMPACTED_BY_BARRELS
					IF NOT conditions[i].returns
						IF IS_CONDITION_FALSE(COND_SHOOTING_PED_DEAD)
							IF HAS_BULLET_IMPACTED_IN_AREA(<<2493.155029,4970.314941,43.880379>>,3.25)
							OR IS_BULLET_IN_AREA(<<2493.155029,4970.314941,43.880379>>,3.25)
								conditions[i].returns = TRUE
							ENDIF
						ELSE
							conditions[i].active = FALSE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_SHOOTING_PED_DEAD
					IF NOT conditions[i].returns
						switch conditions[i].flag
							CASE 0
								conditions[i].intA = get_ped_with_placement(pos_shooting_bottle_1)
								conditions[i].intB = get_ped_with_placement(pos_shooting_bottle_2)
								conditions[i].flag++
							break
							case 1
								IF conditions[i].intA != -1
								aND conditions[i].intB != -1								
									IF DOES_ENTITY_EXIST(zped[conditions[i].intA].id)
									AND DOES_ENTITY_EXIST(zped[conditions[i].intB].id) 
										IF IS_PED_INJURED(zped[conditions[i].intA].id)
										OR IS_PED_INJURED(zped[conditions[i].intB].id)
											conditions[i].returns = TRUE
										ENDIF
									ENDIF
								endif
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				
				
				CASE COND_ALL_ENEMY_ALERTED
					IF bAlertEveryone
						conditions[i].returns = TRUE
						conditions[i].active = FALSE
					ENDIF
				BREAK	
			
				
				CASE COND_PLAYER_IN_METH_LAB
					conditions[i].returns = FALSE
					IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("V_8_BasementRm")						
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_IN_HOUSE
					conditions[i].returns = FALSE
					IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) != null //player inside house
					AND IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2421.330811,4954.833008,25.328293>>, <<2466.796143,4996.822266,66.070175>>, 37.187500)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_DOWNSTAIRS
					conditions[i].returns = FALSE
					IF IS_CONDITION_TRUE(COND_PLAYER_IN_HOUSE)
						IF NOT IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2457.884766,4981.169434,50.005180>>, <<2441.579834,4965.088379,56.240055>>, 34.312500)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_PLAYER_APPROACHING_HOUSE
					IF NOT conditions[i].returns
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 2451.5173, 4973.1221, 44.2646 >>) < 50
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_BASEMENT_PED_DEAD_IN_BASEMENT
					switch conditions[i].flag
						CASE 0
							conditions[i].intA = get_ped_with_placement(pos_inside_cards3)
							IF conditions[i].intA != -1
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF DOES_ENTITY_EXIST(zped[conditions[i].intA].id)
								IF IS_PED_INJURED(zped[conditions[i].intA].id)
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[conditions[i].intA].id,<<2427.8506, 4964.9717, 43.1704>>) < 3.0
										conditions[i].returns = TRUE
										conditions[i].flag++
									ELSE
										conditions[i].active = FALSE
									ENDIF
								ENDIf
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_ATTACKERS_ALL_DEAD
					IF NOT conditions[i].returns
						switch conditions[i].flag
							CASE 0
								IF NOT is_event_active(events_spawn_house)	
									conditions[i].flag++
								ENDIF
							BREAK
							CASE 1
								int iR
								bool bEnemiesAreStillAlive
								bool bOutsideEnemiesAlive
						
					
								bEnemiesAreStillAlive = FALSE
								bOutsideEnemiesAlive = FALSE
								
								REPEAT COUNT_OF(zped) iR
									IF NOT IS_PED_INJURED(zped[iR].id)
										bEnemiesAreStillAlive = TRUE
										IF zped[iR].role = role_guard_outside
											bOutsideEnemiesAlive = TRUE
										
											iR = count_of(zped) //exit
										endif
									endif
								ENDREPEAT
								IF NOT bEnemiesAreStillAlive
									conditions[i].returns = TRUE
								else
									conditions[i].returns = false
								ENDIF
								
								IF bOutsideEnemiesAlive
									SET_CONDITION_STATE(COND_ALL_OUTSIDE_ENEMY_DEAD,false)
								else
									SET_CONDITION_STATE(COND_ALL_OUTSIDE_ENEMY_DEAD,true)
								endif
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				
				CASE COND_ONE_ATTACKER_LEFT_OUTSIDE
					//value for this set elsewhere in script using SET_CONDITION_STATE
					IF NOT conditions[i].returns
						IF IS_CONDITION_TRUE(COND_ALL_ENEMY_ALERTED)
							conditions[i].returns = true
							int iR
							int deadCount
							deadCount=0
							REPEAT COUNT_OF(zPED) iR
								if zPED[iR].role = role_guard_outside
									IF NOT IS_PED_INJURED(zPED[iR].id)
										deadCount++
										IF deadCount > 1
											conditions[i].returns = false
											iR = COUNT_OF(zPED)
										endif
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
					ENDIF
				
					
				BREAK
				
				CASE COND_FAIL_FOR_LEAVING_FARM
					switch conditions[i].flag
						case 0
							vehicle_index aVeh
							
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2446.6738, 4977.7876, 57.4583>>) < 250
								////cprintln(debug_Trevor3,"player inside 250m")
								conditions[i].flag++
							ENDIF
							
						BREAK
						case 1
							bool bCheckOnFoot
							IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
								aVeh = get_vehicle_ped_is_in(player_ped_id())
								IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(aVeh))
								OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(aVeh))
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2446.6738, 4977.7876, 57.4583>>,false) > 700									
										conditions[i].returns = TRUE
									ENDIF
								ELSE
								 	bCheckOnFoot = true
								ENDIF
							ELSE
								bCheckOnFoot = TRUE
							ENDIF
							IF bCheckOnFoot
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2446.6738, 4977.7876, 57.4583>>) > 250
									////cprintln(debug_Trevor3,"player inside 250m")
									conditions[i].returns = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
				BREAK
				
				case COND_CARD_PLAYING_PEDS_STILL_NEAR_TABLE										
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2441.064941,4968.819336,45.830746>>) < 2.75
						IF NOT IS_PED_INJURED(zped[ENUM_TO_INT(pos_inside_cards1)].id)
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[ENUM_TO_INT(pos_inside_cards1)].id,<<2439.385986,4962.049316,45.904697>>) < 3
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(zped[ENUM_TO_INT(pos_inside_cards2)].id)
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[ENUM_TO_INT(pos_inside_cards2)].id,<<2439.385986,4962.049316,45.904697>>) < 3
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(zped[ENUM_TO_INT(pos_inside_cards3)].id)
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[ENUM_TO_INT(pos_inside_cards3)].id,<<2439.385986,4962.049316,45.904697>>) < 3
								conditions[i].returns = TRUE
							ENDIF
						ENDIF	
						
						conditions[i].active = FALSE
						
					ENDIF
				break
				
				CASE COND_BASEMENT_PEDS_LEAVING_AND_OBSERVED
					SWITCH conditions[i].flag
						CASE 0
							IF NOT IS_PED_INJURED(zped[ENUM_TO_INT(pos_barrels_1)].id)
							AND IS_PED_SITTING_IN_ANY_VEHICLE(zped[ENUM_TO_INT(pos_barrels_1)].id)
								conditions[i].flag++
							ENDIF
							IF NOT IS_PED_INJURED(zped[ENUM_TO_INT(pos_barrels_2)].id)
							AND IS_PED_SITTING_IN_ANY_VEHICLE(zped[ENUM_TO_INT(pos_barrels_2)].id)
								conditions[i].flag++
							ENDIF
							IF NOT IS_PED_INJURED(zped[ENUM_TO_INT(pos_garden_chat_2)].id)
							AND IS_PED_SITTING_IN_ANY_VEHICLE(zped[ENUM_TO_INT(pos_garden_chat_2)].id)
								conditions[i].flag++
							ENDIF
							conditions[i].intA = GET_GAME_TIMER() + 8000
						BREAK
						CASE 1
							IF IS_VEHICLE_DRIVEABLE(BarrelVan)
								IF IS_POINT_VISIBLE(GET_ENTITY_COORDS(BarrelVan),5,150)
									conditions[i].intB += floor(timestep() * 1000)									
								ENDIF
							ENDIF
							IF conditions[i].intB > 1500
								conditions[i].returns = TRUE
							ENDIF
							IF GET_GAME_TIMER() > conditions[i].intA
								conditions[i].flag++
							ENDIF
						BREAK
					ENDSWITCH													
				BREAK
				
				CASE COND_JERRY_CAN_DESTROYED
					
						SWITCH conditions[i].flag
							CASE 0
								IF DOES_PICKUP_EXIST(pickup_gas)
									IF DOES_PICKUP_OBJECT_EXIST(pickup_gas)								
										conditions[i].flag++
									ENDIF
								ENDIF
							BREAK
							CASE 1
								IF DOES_PICKUP_EXIST(pickup_gas)
									IF NOT DOES_PICKUP_OBJECT_EXIST(pickup_gas)
										IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,<<2435.2524, 4966.7480, 41.3476>>,4.0)
											conditions[i].returns = TRUE
										ENDIF
										conditions[i].flag++
									ENDIF
								ENDIF
							BREAK	
						ENDSWITCH				
				BREAK
				
				CASE COND_ENEMY_PED_REACTING
					IF bAPedIsReacting
					AND NOT bAlertEveryone
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				
				
				CASE COND_TREVOR_NEAR_BASEMENT
					IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<2442.818604,4970.283203,45.248096>>, <<2439.625244,4966.861816,48.145962>>, 3.437500)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_TREVOR_NEAR_STAIRS
					IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<2434.671387,4959.104492,44.851093>>, <<2440.499756,4965.188965,48.123096>>, 3.625000)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				CASE COND_TREVOR_AT_STAIRS
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2432.467285,4959.472656,44.941334>>, <<2430.796875,4961.172852,48.309013>>, 1.562500)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				
				CASE COND_TREVOR_SEEN
					If bPlayerSeen
						conditions[i].returns = TRUE
						conditions[i].active = FALSE
					ENDIF
				BREAK
				
				//gas pouring
				CASE COND_PLAYER_OUT_FRONT
					conditions[i].returns = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<2447.671387,4951.438477,43.255756>>, <<2454.663818,4957.855957,47.891174>>, 6.812500)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_OUT_OF_GAS
					SWITCH conditions[i].flag
						CASE 0
							IF HAS_PED_GOT_WEAPON(player_peD_id(),WEAPONTYPE_PETROLCAN)							
								conditions[i].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT HAS_PED_GOT_WEAPON(player_peD_id(),WEAPONTYPE_PETROLCAN)	
								conditions[i].returns = TRUE
							ENDIF
						BREAK
					ENDSWITCH
					//	conditions[i].intA = 1 //ped has petrol can this frame
						
				//		conditions[i].intB = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
				//	
				//		IF conditions[i].intB <= 0							
				//			conditions[i].returns = TRUE
				//		ENDIF
					//ELSE
					//	IF conditions[i].intA = 1
					//		IF conditions[i].intB <= 6
					//			conditions[i].returns = TRUE
					//		ELSE
					//			conditions[i].intA = 0
					//		ENDIF
					//	ENDIF
					
				BREAK
				
				CASE COND_PLAYER_HAS_PETROL_CAN
					conditions[i].returns = FALSE
					
					SWITCH conditions[i].intA
						CASE 0
							IF HAS_PED_GOT_WEAPON(player_peD_id(),WEAPONTYPE_PETROLCAN)
								conditions[i].intA = 10
							ELSE
								conditions[i].intA = 20
							ENDIF
						BREAK
					ENDSWITCH
								
					
					IF HAS_PED_GOT_WEAPON(player_peD_id(),WEAPONTYPE_PETROLCAN)				
						IF conditions[i].intA = 20
						//	SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN) //commented out. Seemed to be forcing player to equip petrol cam. Why did I even add this?
						ENDIF
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_GAS_TRAIL_COMPLETE
					IF NOT conditions[i].returns = TRUE
						IF IS_GAS_TRAIL_COMPLETE()
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
							conditions[i].returns = TRUE															
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_LAST_BLIP_POURED_ON
					IF gasTrail[0].bPouredOn = TRUE
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_POURING_GAS
					conditions[i].returns = FALSE
					WEAPON_TYPE wep 
					wep = WEAPONTYPE_UNARMED
					IF GET_CURRENT_PED_WEAPON(player_ped_id(),wep)
						IF wep = WEAPONTYPE_PETROLCAN
							IF IS_CONTROL_PRESSED(player_control,INPUT_ATTACK)
								conditions[i].returns = TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				
				
				CASE COND_EXPLOSION_IN_BASEMENT
					IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<2435.775879,4963.066895,39.342651>>, <<2428.118408,4971.287109,43.673267>>, 11.875000)
						IF NOT IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_EXTINGUISHER,<<2435.775879,4963.066895,39.342651>>, <<2428.118408,4971.287109,43.673267>>, 11.875000)
						AND NOT IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_FLARE,<<2435.775879,4963.066895,39.342651>>, <<2428.118408,4971.287109,43.673267>>, 11.875000)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				/*
				CASE COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE
					IF NOT IS_GAS_TRAIL_LINK_MISSING(17)
						IF IS_GAS_TRAIL_BURNING()
							IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = null						
								vector vHere
								vHere = <<0,0,0>>
								IF GET_CLOSEST_FIRE_POS(vHere,<<2452.7, 4970.4, 45.8104>>)
									IF GET_DISTANCE_BETWEEN_COORDS(vHere,<<2452.7, 4970.4, 45.8104>>) < 1.5
										conditions[i].returns = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF						
					ENDIF				
				BREAK*/
				
				CASE COND_GAS_IGNITED_IN_HOUSE_WILL_BURN_OK
					SWITCH conditions[i].flag
						CASE 0
							IF IS_GAS_TRAIL_BURNING_AND_CAN_THE_FIRE_BURN_OUT()
								int iNode
								iNode = GET_GAS_TRAIL_IGNITE_NODE()
								IF iNode <= 17
									SET_CONDITION_STATE(COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE,TRUE)
								ELSE							
									conditions[i].returns = TRUE
								ENDIF
								conditions[i].flag++
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_GAS_TRAIL_REACHED_DOORWAY
					IF NOT conditions[i].returns
						IF IS_GAS_NODE_BURNING(17)
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_TREVOR_INSIDE_HOUSE
				
					conditions[i].returns = FALSE
					IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) != null
						conditions[i].returns = TRUE
					ENDIF				
				BREAK
				
				CASE COND_40M_FARM_WARNING					
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2439.52, 4969.67, 52.65>>) > 42.5
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_60M_FARM_FAIL
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2439.52, 4969.67, 52.65>>) > 60
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_GAS_TRAIL_IGNITED_INCORRECTLY														
					IF IS_GAS_TRAIL_BURNING()
						IF NOT IS_GAS_TRAIL_BURNING_AND_CAN_THE_FIRE_BURN_OUT()
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_GET_WANTED_LEVEL
					SWITCH conditions[i].flag
						CASE 0
							conditions[i].intA = GET_GAME_TIMER() + 30000
							conditions[i].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > conditions[i].intA
								conditions[i].returns = TRUE
								conditions[i].flag++
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE COND_PLAYER_300_FROM_FARM
					conditions[i].returns = FALSE
					IF NOT conditions[i].returns
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2448.25, 4974.92, 55.11>>) > 300
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE COND_BUILDING_STATE_SWAPPED
					IF IS_ACTION_COMPLETE(1,ACT_CHANGE_BUILDING_STATE)
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
				CASE COND_PLAYER_500_FROM_FARM
					conditions[i].returns = FALSE
					IF NOT conditions[i].returns
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2448.25, 4974.92, 55.11>>) > 500
							conditions[i].returns = TRUE
						ENDIF
					ENDIF
				BREAK
	
				CASE COND_GOT_WANTED_LEVEL
					
					conditions[i].returns = FALSE
					IF GET_PLAYER_WANTED_LEVEL(player_id()) > 0
						conditions[i].returns = TRUE
					ENDIF
				BREAK
				
			ENDSWITCH
		//ENDIF
	ENDREPEAT
ENDPROC

//actions
FUNC INT GET_ACTION_FLAG(int iAction, enumActions actionToCheck)
	IF actions[iAction].action = actionToCheck
		RETURN actions[iAction].flag
	ENDIF
	RETURN -1
endfunc



PROC SET_ACTION_INT(int iAction, enumActions actionToSet, int iNewIntValue)
	IF actions[iAction].action = actionToSet
		actions[iAction].intA = iNewIntValue
	ELSE
		SCRIPT_ASSERT("Incorrect value passed in to GET_ACTION_INTA()")
	ENDIF
endproc

PROC FORCE_ACTION_STATE(int thisI, enumActions thisAction, bool setCompleted=TRUE, bool setOngoing = TRUE)
	
	actions[thisI].action = thisAction
	actions[thisI].active = TRUE
	actions[thisI].ongoing = setOngoing
	actions[thisI].completed = setCompleted		
	actions[thisI].flag = 0
	actions[thisI].needsCleanup = FALSE
	actions[thisI].intA = 0
	actions[thisI].intB = 0
	actions[thisI].trackCondition=FALSE
	actions[thisI].floatA = 0
	
ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

PROC ACTION(int thisI, enumActions thisAction, enumACTIONplayout playout=PLAYOUT_ON_TRIGGER, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)			
	#if IS_DEBUG_BUILD
		IF thisI >= MAX_ACTIONS
			SCRIPT_ASSERT("SCRIPT: INCREASE MAX_ACTIONS")
		ENDIF
	#endif
	IF actions[thisI].action != thisAction
	OR actions[thisI].active = FALSE
		actions[thisI].action = thisAction
		actions[thisI].active = TRUE
		actions[thisI].completed = FALSE		
		actions[thisI].flag = 0
		actions[thisI].needsCleanup = FALSE
		actions[thisI].intA = 0
		actions[thisI].intB = 0
		actions[thisI].floatA = 0
	ENDIF
	bool actionConditionCheck
	
	IF actions[thisI].ongoing
	AND playout = PLAYOUT_ON_TRIGGER
		actionConditionCheck = TRUE
	ELSE
		actionConditionCheck = checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4)
	ENDIF
	
	IF playout = PLAYOUT_ON_COND
	AND NOT actionConditionCheck
		actions[thisI].ongoing = FALSE
	ENDIF
	
		


	
	IF actions[thisI].completed = FALSE
	OR actions[thisI].flag = CLEANUP
		IF actionConditionCheck = TRUE
			IF NOT actions[thisI].ongoing
				actions[thisI].ongoing = TRUE
			ENDIF
			SWITCH actions[thisI].action	
				//drive to alley		
				CASE act_get_stalls
				/*
					IF NOT IS_BIT_SET(actions[thisI].intA,0)									
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<1985.3221,3053.0872,46.2263>>,0.3, PROP_BAR_STOOL_01)								
							g_sTriggerSceneAssets.object[0] = GET_CLOSEST_OBJECT_OF_TYPE(<<1985.3221,3053.0872,46.2263>>,0.3,PROP_BAR_STOOL_01,FALSE)
							IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
								int scriptID								
								IF IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[0],scriptID))
									g_sTriggerSceneAssets.object[0] = GET_CLOSEST_OBJECT_OF_TYPE(<<1985.3221,3053.0872,46.2263>>,0.3,PROP_BAR_STOOL_01,TRUE)
									//cprintln(debug_Trevor3,"chinese 2 script id: ",GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[0],scriptID))
									SET_BIT(actions[thisI].intA,0)
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
					IF NOT IS_BIT_SET(actions[thisI].intA,1)									
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<1984.8481,3052.4856,46.2263>>,0.3, PROP_BAR_STOOL_01)								
							g_sTriggerSceneAssets.object[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<1984.8481,3052.4856,46.2263>>,0.3,PROP_BAR_STOOL_01,FALSE)
							IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
								int scriptID								
								IF IS_STRING_NULL_OR_EMPTY(GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[1],scriptID))
									g_sTriggerSceneAssets.object[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<1984.8481,3052.4856,46.2263>>,0.3,PROP_BAR_STOOL_01,TRUE)
									//cprintln(debug_Trevor3,"chinese 2 script id: ",GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[1],scriptID))
									SET_BIT(actions[thisI].intA,1)
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
					*/
				BREAK
				CASE ACT_AUDIO_SCENE_DRIVE_TO_FARMHOUSE
					SWITCH actions[thisI].flag
						CASE 0
							
							START_AUDIO_SCENE("CHI_2_DRIVE_TO_FARMHOUSE")
							//cprintln(debug_Trevor3,"CHI_2_DRIVE_TO_FARMHOUSE called")
							actions[thisI].flag++
						BREAK		
						CASE 1
							IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
								STOP_AUDIO_SCENE("CHI_2_DRIVE_TO_FARMHOUSE")
								actions[thisI].completed = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_REMOVE_BAR_PEDS
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_peD_id(),<<1991.0500, 3054.2625, 46.2147>>) > 250
						IF DOES_ENTITY_EXIST(zped[ped_tao].id)
							DELETE_PED(zped[ped_tao].id)
						ENDIF
						
						IF DOES_ENTITY_EXIST(zped[ped_taos_translator].id)
							DELETE_PED(zped[ped_taos_translator].id)
						ENDIF
						
						IF DOES_ENTITY_EXIST(zped[ped_janet].id)
							DELETE_PED(zped[ped_janet].id)
						ENDIF
						
						IF DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
							DELETE_PED(zped[ped_oldMan1].id)
						ENDIF
						
						IF DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
							DELETE_PED(zped[ped_oldMan2].id)
						ENDIF
												
						SET_MODEL_AS_NO_LONGER_NEEDED(ig_janet)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_JANET)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN1A)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN2)						
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_TAOCHENG)
						SET_MODEL_AS_NO_LONGER_NEEDED(IG_TAOSTRANSLATOR)
						REMOVE_ANIM_DICT("misschinese2_crystalmaze")
						REMOVE_ANIM_DICT("MISSChinese2_crystalMazeMCS1_IG")
						REMOVE_ANIM_DICT("missrampageintrooutro")
						//DISABLE_NAVMESH_IN_AREA(<<1993.638428,3053.134033,45.214718>>, <<1989.650635,3055.619385,47.840214>>,FALSE)
						IF iNavBlock != -1
							IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlock)
								REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlock)
							ENDIF
						ENDIf
						
						CLEAR_PED_NON_CREATION_AREA()
						
						cprintln(debug_Trevor3,"Remove bar peds")

						kill_event(events_animate_bar)
					
						
						actions[thisI].completed = TRUE		
					ENDIF
				BREAK
				
				CASE ACT_LOAD_SOUNDSET
					IF LOAD_STREAM("CHI_2_FARMHOUSE_INTRO","CHINESE2_FARMHOUSE_INTRODUCTION")
						actions[thisI].completed = TRUE	
					ENDIF
				BREAK
				
			
				
				CASE ACT_LOAD_FRUSTROM		
					
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_peD_id(),<<2457.489746,4977.094727,50.567833>> ) < 400
								add_event(events_force_farm_doors_drawing)
								SET_VEHICLE_POPULATION_BUDGET(1)
								SET_PED_POPULATION_BUDGET(1)
								PREFETCH_SRL("chinese2_farmhouse_cutscene")  
								actions[thisI].completed = TRUE								
							ENDIF
						
					
				BREAK
				
				CASE ACT_BAR_PEDS_LOOK_AT_TREVOR
					IF NOT IS_PED_INJURED(zped[ped_janet].id)							
					AND NOT IS_PED_INJURED(zped[ped_oldMan1].id)							
					AND NOT IS_PED_INJURED(zped[ped_oldMan2].id)
						IF GET_ACTION_FLAG(6,ACT_MAKE_BAR_PEDS_FLEE) < 2
							IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1981.256714,3050.721191,41.089954>>, <<1984.256958,3055.542725,49.473137>>, 3.062500)
								IF GET_SCRIPT_TASK_STATUS(zped[ped_janet].id,SCRIPT_TASK_LOOK_AT_ENTITY) = FINISHED_TASK
									cprintln(debug_Trevor3,"YES")
									TASK_LOOK_AT_ENTITY(zped[ped_oldMan1].id,player_ped_id(),-1)
									TASK_LOOK_AT_ENTITY(zped[ped_oldMan2].id,player_ped_id(),-1)
									TASK_LOOK_AT_ENTITY(zped[ped_janet].id,player_ped_id(),-1)							
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_SLOW_DOWN_PLAYER_IN_CHOPPER
					float fDist,fMulti,fThisSpeed
					fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2450.2136, 4979.2080, 60.8422>>)
					IF fDist < 900					
						IF IS_PED_IN_ANY_VEHICLE(player_ped_id())		
							vehicle_index aVeh
							aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
							IF IS_VEHICLE_DRIVEABLE(aVeh)								
								IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(aVeh))
								OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(aVeh))
																	
									fmulti = (900 - fDist)
									IF fmulti < 0
										fMulti = 0
									endif
									
									fmulti /= 900
									fmulti *= 10000
									
									fThisSpeed = GET_ENTITY_SPEED(aVeh)
									
									fmulti *= fThisSpeed/35
									
									IF fThisSpeed > 35
										APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(aVeh,apply_type_impulse,<<0,-fmulti,0>>,0,true,false)
										//showfloatOnScreen(0.1,0.1,fmulti)
										//showfloatOnScreen(0.1,0.15,fThisSpeed)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_BLEND_OUT_OF_TREVOR_RAGE
					
					IF IS_ENTITY_PLAYING_ANIM(player_ped_id(),"missrampageintrooutro","trvram_6_1h_run_outro")
						actions[thisI].flag = 1
						SET_PED_RESET_FLAG(player_ped_id(),PRF_SkipOnFootIdleIntro, TRUE)
						//IF GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(),"missrampageintrooutro","trvram_6_1h_run_outro") > 0.75
						if HAS_ANIM_EVENT_FIRED(player_ped_id(),GET_HASH_KEY("walkinterruptible"))
							FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_IDLE)
							IF IS_PLAYER_PRESSING_A_CONTROL_BUTTON()
								CLEAR_PED_TASKS(player_ped_id())
								actions[thisI].completed = TRUE
							ENDIF
						ENDIF
					ELSE
						IF actions[thisI].flag = 1							
							REMOVE_ANIM_DICT("missrampageintrooutro")
							actions[thisI].completed = TRUE													
						ENDIF
					ENDIF
				BREAK
				
				CASE ACT_MAKE_BAR_PEDS_FLEE
					switch actions[thisI].flag
						case 0						
							IF NOT IS_PED_INJURED(zped[ped_janet].id)							
							AND NOT IS_PED_INJURED(zped[ped_oldMan1].id)							
							AND NOT IS_PED_INJURED(zped[ped_oldMan2].id)								
								//IF NOT IS_PED_INJURED(
								SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_janet].id,relGroupBar)
								SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_oldMan1].id,relGroupBar)
								SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_oldMan2].id,relGroupBar)
				
								SET_PED_COMBAT_ATTRIBUTES(zped[ped_janet].id,CA_ALWAYS_FLEE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(zped[ped_oldMan1].id,CA_ALWAYS_FLEE,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(zped[ped_oldMan2].id,CA_ALWAYS_FLEE,TRUE)
								
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(zped[ped_janet].id)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(zped[ped_oldMan1].id)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(zped[ped_oldMan2].id)
								actions[thisI].flag++
							ENDIF
						BREAK
						
						case 1
							IF IS_PED_INJURED(zped[ped_janet].id)
							OR IS_PED_INJURED(zped[ped_oldMan1].id)
							OR IS_PED_INJURED(zped[ped_oldMan2].id)
								actions[thisI].flag++
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[ped_janet].id)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(zped[ped_janet].id)
									actions[thisI].flag=2
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[ped_oldMan1].id)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(zped[ped_oldMan1].id)
									actions[thisI].flag=2
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[ped_oldMan2].id)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(zped[ped_oldMan2].id)
									actions[thisI].flag=2
								ENDIF
							ENDIF
						break
						case 2
							IF NOT IS_PED_INJURED(zped[ped_janet].id)
								TASK_SMART_FLEE_PED(zped[ped_janet].id,player_ped_id(),100,-1)
								SET_PED_KEEP_TASK(zped[ped_janet].id,TRUE)
								SET_PED_AS_NO_LONGER_NEEDED(zped[ped_janet].id)
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_oldMan1].id)							
								TASK_SMART_FLEE_PED(zped[ped_oldMan1].id,player_ped_id(),100,-1)
								SET_PED_KEEP_TASK(zped[ped_oldMan1].id,TRUE)
								SET_PED_AS_NO_LONGER_NEEDED(zped[ped_oldMan1].id)
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_oldMan2].id)
								TASK_SMART_FLEE_PED(zped[ped_oldMan2].id,player_ped_id(),100,-1)
								SET_PED_KEEP_TASK(zped[ped_oldMan2].id,TRUE)
								SET_PED_AS_NO_LONGER_NEEDED(zped[ped_oldMan2].id)
								actions[thisI].completed = TRUE
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				CASE ACT_LOAD_END_CUT_FRUSTROM	
				
				//COND_GAS_TRAIL_COMPLETE
					switch actions[thisI].flag
						case 0
							PREFETCH_SRL("chinese2_explosion_cutscene")  
							STOP_STREAM()
							actions[thisI].flag++
						BREAK
						CASE 1
							IF CUTSCENE_FIRST_SHOT = SHOT_BASEMENT
								cprintln(debug_Trevor3,"A")
								IF LOAD_STREAM("CHINESE2_FARMHOUSE_EXPLOSION_MOLOTOV_MASTER")																						
									actions[thisI].flag++
								ENDIF
							ENDIF
							IF CUTSCENE_FIRST_SHOT = SHOT_HOUSE_EXPLOSION
								cprintln(debug_Trevor3,"B")
								IF LOAD_STREAM("CHINESE2_FARMHOUSE_EXPLOSION_STICKY_BOMB_MASTER")																						
									actions[thisI].flag++
								ENDIF
							ENDIF
							IF CUTSCENE_FIRST_SHOT = SHOT_NORMAL
								cprintln(debug_Trevor3,"C")
								IF LOAD_STREAM("CHINESE2_FARMHOUSE_EXPLOSION_SHOOT_GASOLINE_MASTER")																						
									actions[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF IS_SRL_LOADED()
								cprintln(debug_trevor3,"IS_SRL_LOADED")
								actions[thisI].flag = 10
							ENDIF
						BREAK
					endswitch
				BREAK
				
				case ACT_BOMB_IN_BASEMENT
					switch actions[thisI].flag
						case 1
							IF GET_GAME_TIMER() > actions[thisI].intA								 
								IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(player_ped_id()))
									/*float fHEading
									fHeading = GET_HEADING_FROM_COORDS(get_entity_coords(player_ped_id()),<<2442.7959, 4973.3228, 46.9416>>,false)
									float foMin, foMax
									foMin = fHeading - 45
									foMax = fHeading + 45
									//cprintln(debug_Trevor3,fHeading)
									fHEading = GET_RANDOM_FLOAT_IN_RANGE(foMin,foMax)
									//cprintln(debug_Trevor3,fHeading,"X: ",actions[thisI].floatA*sin(fHEading),"Y: ",actions[thisI].floatA*cos(fHEading))
									vector vMid
									vMid = get_entity_coords(player_ped_id())+<<actions[thisI].floatA*sin(fHEading),actions[thisI].floatA*cos(fHEading),0.3>>
									
									
									
									//cprintln(debug_Trevor3,vMid)
									*/
									ADD_EXPLOSION(get_entity_coords(player_ped_id())+<<GET_RANDOM_FLOAT_IN_RANGE(0,4),GET_RANDOM_FLOAT_IN_RANGE(0,4),0>>,EXP_TAG_SHIP_DESTROY,1.5)
									
									actions[thisI].floatA -= 1
									actions[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(250,520)
								ELSE
									actions[thisI].flag = 2
								ENDIF
							ENDIF
						break
						case 2
						break
						DEFAULT
						
							int iR
							REPEAT COUNT_OF(cellar_explosions) iR
								IF NOT IS_BIT_SET(actions[thisI].intA,iR)
									IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,cellar_explosions[iR],5)
										SET_BIT(actions[thisI].intA,iR)
										IF actions[thisI].flag = 0
											actions[thisI].flag = GET_GAME_TIMER() + 1500
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF GET_GAME_TIMER() > actions[thisI].intB
								actions[thisI].intB = 0
								REPEAT COUNT_OF(cellar_explosions) iR
									IF NOT IS_BIT_SET(actions[thisI].intA,iR+9)
									AND IS_BIT_SET(actions[thisI].intA,iR)
										ADD_EXPLOSION(cellar_explosions[iR],EXP_TAG_SHIP_DESTROY,1.5)
										SET_BIT(actions[thisI].intA,iR+9)
										iR = 8
										actions[thisI].intB = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(150,320)										
									ENDIF
								ENDREPEAT
							ENDIF
						
							IF actions[thisI].flag != 0
								IF GET_GAME_TIMER() > actions[thisI].flag
									actions[thisI].flag = 1
									actions[thisI].intA = GET_GAME_TIMER() + 300
									actions[thisI].floatA = 16
								ENDIF
							ENDIF
						
						BREAK
					ENDSWITCH												
				break
				
				CASE act_shooting_peds_witness_impact_at_barrels
					int peda,pedb
					peda = get_ped_with_placement(pos_shooting_bottle_1)
					pedb = get_ped_with_placement(pos_shooting_bottle_2)
					set_ped_action(peda,action_respond_to_nearby_alerted_ped)
					set_ped_action(pedb,action_respond_to_nearby_alerted_ped)
					actions[thisI].completed = TRUE
				BREAK
				
				
				
				CASE ACT_RADIO_SOUND
					switch actions[thisI].flag	
						case 0
							SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
							actions[thisI].flag++
						BREAK
						CASE 1
							IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
								IF DOES_PLAYER_VEH_HAVE_RADIO()
									vehPlayer = GET_VEHICLE_PED_IS_IN(player_ped_id())
									IF IS_VEHICLE_DRIVEABLE(vehPlayer)
										IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehPlayer)
											actions[thisI].flag++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF PREPARE_MUSIC_EVENT("CHN2_TREV_RADIO_1_FRTA")
								actions[thisI].flag++
							ENDIF
						BREAK
						//	IF DOES_ENTITY_EXIST(vehPlayer)
						//		IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
								/*	
									IF DOES_PLAYER_VEH_HAVE_RADIO()
										IF PREPARE_MUSIC_EVENT("CHN2_TREV_RADIO_1_FRTA")
											actions[thisI].flag++
										ENDIF
									ELSE
										actions[thisI].completed = TRUE
									ENDIF
								ELSE
									actions[thisI].completed = TRUE
								ENDIF
							ELSe
								actions[thisI].completed = TRUE
							ENDIF*/
						BREAK
						CASE 3
							//IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
							//	IF IS_PED_IN_VEHICLE(player_ped_id(),vehPlayer)
									IF TRIGGER_MUSIC_EVENT("CHN2_TREV_RADIO_1_FRTA")									
										actions[thisI].flag++
									ENDIF
							//	ENDIF
							//ENDIF
						BREAK
						CASE 4
							IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
								IF IS_PED_IN_VEHICLE(player_ped_id(),vehPlayer)									
									IF GET_IS_VEHICLE_ENGINE_RUNNING(vehPlayer)
										actions[thisI].intA += GET_GAME_TIMER() + 7000
										
										actions[thisI].flag++
										
									ENDIF									
								ENDIF
							ENDIF
						BREAK
						CASE 5
							IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
								IF IS_PED_IN_VEHICLE(player_ped_id(),vehPlayer)		
									IF GET_GAME_TIMER() > actions[thisI].intA
										IF GET_PLAYER_RADIO_STATION_GENRE() != enum_to_int(RADIO_GENRE_PUNK)
										AND GET_PLAYER_RADIO_STATION_GENRE() != enum_to_int(RADIO_GENRE_OFF)
											iF GET_MUSIC_VOL_SLIDER() != 0
												IF CREATE_CONVERSATION_EXTRA("CHI2_rad",1,player_ped_id(),"Trevor")
													actions[thisI].flag++
												ENDIF
											else
												actions[thisI].flag++
											endif
										ELSE
											actions[thisI].intA = GET_GAME_TIMER() + 5000
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 6
							IF IS_CONV_ROOT_PLAYING("CHI2_rad")
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 7
							IF NOT IS_CONV_ROOT_PLAYING("CHI2_rad")
								IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
									IF IS_PED_IN_VEHICLE(player_ped_id(),vehPlayer)			
										IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_VEH_RADIO_WHEEL)
											IF GET_PLAYER_RADIO_STATION_GENRE() != enum_to_int(RADIO_GENRE_PUNK)
												IF iRadSound = -1 
													iRadSound = GET_SOUND_ID()									
													PLAY_SOUND_FRONTEND(iRadSound,"Change_Station_Loud","Radio_Soundset")
													actions[thisI].flag++
												ENDIF
											ELSE
												actions[thisI].flag = 9
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						BREAK
						CASE 8	
							IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_VEH_RADIO_WHEEL)
								IF TRIGGER_MUSIC_EVENT("CHN2_TREV_RADIO_2_FRTA")
									START_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")		
									
									actions[thisI].flag++
								ENDIF								
							ENDIF
						BREAK
						
						CASE 9
							IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
								IF IS_PED_IN_VEHICLE(player_ped_id(),vehPlayer)		
									IF GET_PLAYER_RADIO_STATION_GENRE() = enum_to_int(RADIO_GENRE_PUNK)
										iF GET_MUSIC_VOL_SLIDER() != 0
											IF CREATE_CONVERSATION_EXTRA("CHI2_radb",1,player_ped_id(),"Trevor")
												actions[thisI].flag++					
											ENDIF						
										else
											actions[thisI].flag++	
										endif
									ENDIF
								ENDIF
							ENDIF
						/*	IF actions[thisI].flag = 9
								STOP_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")
								iRadSound = -1
								actions[thisI].flag = 11
							ENDIF*/
						BREAK
					/*	CASE 10 //in case the player changes the radio station
							IF GET_PLAYER_RADIO_STATION_GENRE() != enum_to_int(RADIO_GENRE_PUNK)
							OR NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())								
								STOP_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")							
								actions[thisI].flag++
							ENDIF
						BREAK
						case 11
							IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
							AND GET_PLAYER_RADIO_STATION_GENRE() = enum_to_int(RADIO_GENRE_PUNK)
								START_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")	
								actions[thisI].flag = 10
							ENDIF
						break*/
						
					endswitch
				BREAK
				
				CASE ACT_GET_WANTED_LEVEL
					kill_event(events_control_dispatch)
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_PLAYER_WANTED_LEVEL(player_id(),2)
					SET_PLAYER_WANTED_LEVEL_NOW(player_id())
					SET_MAX_WANTED_LEVEL(5)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_CHANGE_BUILDING_STATE
					IF NOT IS_POINT_VISIBLE(<<2448.25, 4974.92, 55.11>>,20,500)
					OR IS_CONDITION_TRUE(COND_PLAYER_500_FROM_FARM)
						IF int_farm != null
							UNPIN_INTERIOR(int_farm)
						ENDIF
						int_farm=null
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE,BUILDINGSTATE_CLEANUP, FALSE)
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1,BUILDINGSTATE_CLEANUP, FALSE)
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2,BUILDINGSTATE_CLEANUP, FALSE)
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3,BUILDINGSTATE_CLEANUP, FALSE)
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_DESTROYED)
						SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_OCCLUSION, BUILDINGSTATE_DESTROYED)
						actions[thisI].completed = TRUE
					ENDIF
				BREAK
				
				CASE ACT_STAT_POUR_GAS
					SWITCH actions[thisI].flag
						CASE 0
							IF HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)	
								actions[thisI].intA = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)	
								int iAmmo
								iAmmo = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
								
								IF iAmmo > actions[thisI].intA
									cprintln(debug_Trevor3,"more ammo")
									actions[thisI].intA = iAmmo
								ELIF iAmmo < actions[thisI].intA
									INFORM_MISSION_STATS_OF_INCREMENT(CHI2_GAS_USED,actions[thisI].intA - iAmmo,FALSE)
									cprintln(debug_Trevor3,"ammo added = ",actions[thisI].intA - iAmmo)
									actions[thisI].intA = iAmmo
								ENDIF
							ENDIF
							
							weapon_type thisWep
							IF GET_CURRENT_PED_WEAPON(player_ped_id(),thisWep)
								IF thisWep = WEAPONTYPE_PETROLCAN
									IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_ATTACK)
										IF actions[thisI].intB = 0
											INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(CHI2_GAS_POUR_TIME)
											actions[thisI].intB = 1
										ENDIF
									ELSE
										IF actions[thisI].intB = 1
											INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
											actions[thisI].intB = 0
										ENDIF
									ENDIF
								ELSE
									IF actions[thisI].intB = 1
										INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
										actions[thisI].intB = 0								
									ENDIF
								ENDIF
							ELSE
								IF actions[thisI].intB = 1
									INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
									actions[thisI].intB = 0
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
		
						
				BREAK
				
				CASE ACT_FUDGE_GAS_AMMO
					SWITCH actions[thisI].flag
						CASE 0
							actions[thisI].intA  = -1
							actions[thisI].flag++
						BREAK
						CASE 1
							weapon_type thisWep
							IF GET_CURRENT_PED_WEAPON(player_ped_id(),thisWep)
								IF thisWep = WEAPONTYPE_PETROLCAN
									IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_ATTACK)
										IF actions[thisI].intA = -1
											actions[thisI].intA = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
										ENDIF
										
										//int maxfuel
										
//										maxFuel = floor(to_float(actions[thisI].intA) * (0.2 + (0.8* (to_float(gasNodesRemaining) / 61.0))))
									
										
										IF gasNodesRemaining = 0
											actions[thisI].floatA += timestep()	* 2.0
										ELSE
											IF gasNodesRemaining < 30
											AND actions[thisI].floatA < 70
												actions[thisI].floatA += timestep() * 1.5
											ELSE
												actions[thisI].floatA += timestep()	
											ENDIF
										ENDIF
										
										int setAmmo 
										setAmmo = floor(actions[thisI].intA * (1.0 - (actions[thisI].floatA / 90.0)))
									/*	
										IF maxfuel < setAmmo
											IF maxFuel < GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
												SET_AMMO_IN_CLIP(player_ped_id(),thisWep,maxFuel)
											ENDIF
										ELSE*/
											IF setAmmo > 0
												SET_PED_AMMO(player_ped_id(),thisWep,setAmmo)
											Else
												SET_PED_AMMO(player_ped_id(),thisWep,0)
											//	REMOVE_WEAPON_FROM_PED(player_ped_id(),thisWep)
												
											ENDIF
									//	ENDIF
									ENDIF																											
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ACT_STOP_MUSIC
					playmusic("CHN2_STOP_TRACK",false,NULL_STRING())
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_FIRE_AUDIO
					SET_AMBIENT_ZONE_LIST_STATE("AZL_CHN2_METH_LAB_FARM_FIRE", TRUE, TRUE)
					DISABLE_TAXI_HAILING(TRUE)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_ENABLE_TAXI
					DISABLE_TAXI_HAILING(FALSE)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_RESET_STUFF
					SET_VEHICLE_POPULATION_BUDGET(3)
					SET_PED_POPULATION_BUDGET(3)
					actions[thisI].completed = TRUE
				BREAK
				
				CASE ACT_REMOVE_SOME_ASSETS
					switch actions[thisI].flag												
						case 0
							IF DOES_ENTITY_EXIST(zPED[enum_to_int(pos_barrels_1)].obj)
								SET_OBJECT_AS_NO_LONGER_NEEDED(zPED[enum_to_int(pos_barrels_1)].obj)
							ENDIF
							
							IF DOES_ENTITY_EXIST(zPED[enum_to_int(pos_barrels_2)].obj)
								SET_OBJECT_AS_NO_LONGER_NEEDED(zPED[enum_to_int(pos_barrels_2)].obj)
							ENDIF
							
							int ij
							repeat count_of(oCans) ij
								IF DOES_ENTITY_EXIST(oCans[ij])
									SET_OBJECT_AS_NO_LONGER_NEEDED(oCans[ij])
								ENDIF
							endrepeat
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_BEER_BOT_01)
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_CAN_01)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Fertilizer)
							actions[thisI].flag++
						BREAK
						CASE 1
							
							IF IS_PED_INJURED(zPED[enum_to_int(pos_innocent_drugden)].id)
								IF DOES_ENTITY_EXIST(objShotgun)
									SET_OBJECT_AS_NO_LONGER_NEEDED(objShotgun)
									REMOVE_WEAPON_ASSET(WEAPONTYPE_SAWNOFFSHOTGUN)
									actions[thisI].completed = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
				BREAK
				
				CASE ACT_WHIMPERING				
					IF NOT IS_PED_INJURED(zped[ped_tao].id)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(zped[ped_tao].id)
							IF GET_GAME_TIMER() > actions[thisI].intA
								IF NOT IS_PED_INJURED(zped[ped_tao].id)							
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zped[ped_tao].id,"WHIMPER","WAVELOAD_PAIN_MALE","SPEECH_PARAMS_FORCE_NORMAL")
									actions[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3500,5000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//CASE ACT_ANIMATE_CHINESE_AFTER_CUTSCENE
				//	switch actions[thisI].flag												
				//		case 0
				//BREAK
								
				
				CASE ACT_ANIMATE_CHINESE						
					switch actions[thisI].flag												
						case 0
							REQUEST_MODEL(IG_TAOCHENG)
							REQUEST_MODEL(IG_TAOSTRANSLATOR)
							REQUEST_ANIM_DICT("misschinese2_crystalmaze")
							
							IF iNavBlock = -1
							OR (iNavBlock != -1 AND NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlock))
								iNavBlock = ADD_NAVMESH_BLOCKING_OBJECT(<<1991.081299,3054.516602,46.714745>>,<<4,4,4>>,0,false,BLOCKING_OBJECT_WANDERPATH + BLOCKING_OBJECT_SHORTESTPATH)							
							ENDIF
							
							actions[thisI].flag++
						BREAK
						CASE 1
							IF HAS_ANIM_DICT_LOADED("misschinese2_crystalmaze")
							AND HAS_MODEL_LOADED(IG_TAOCHENG)
							AND HAS_MODEL_LOADED(IG_TAOSTRANSLATOR)
								IF NOT DOES_ENTITY_EXIST(zped[ped_tao].id )
									zped[ped_tao].id = CREATE_PED(PEDTYPE_MISSION,IG_TAOCHENG,<< 1991.988, 3054.510, 46.215 >>) //these were commented out but don't know why I did that because when player restarts from beginning of mission this would halt progress because these peds never exist.
									SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_tao].id,taorelgroup)
									SET_PED_CAN_BE_TARGETTED(zped[ped_tao].id,FALSE)
								ELSE
									IF NOT IS_PED_INJURED(zped[ped_tao].id)
										SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_tao].id,taorelgroup)
										SET_PED_CAN_BE_TARGETTED(zped[ped_tao].id,FALSE)
									ENDIF
								endif
								
								IF NOT DOES_ENTITY_EXIST(zped[ped_taos_translator].id )
									zped[ped_taos_translator].id = CREATE_PED(PEDTYPE_MISSION,IG_TAOSTRANSLATOR,<< 1991.988, 3054.510, 46.215 >>) //these were commented out but don't know why I did that because when player restarts from beginning of mission this would halt progress because these peds never exist.
									SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_taos_translator].id,taorelgroup)
									SET_PED_CAN_BE_TARGETTED(zped[ped_taos_translator].id,FALSE)
									SET_PED_PROP_INDEX(zped[ped_taos_translator].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 1)
								ELSE
									IF NOT IS_PED_INJURED(zped[ped_taos_translator].id)
										SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_taos_translator].id,taorelgroup)
										SET_PED_CAN_BE_TARGETTED(zped[ped_taos_translator].id,FALSE)
									ENDIF
								endif
								
								//	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("tao")
								IF NOT IS_PED_INJURED(zPED[ped_tao].id)
									////cprintln(debug_trevor3,"tao tasked")
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
										actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intA,TRUE)
									ENDIF
									TASK_SYNCHRONIZED_SCENE (zped[ped_tao].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_TaoCheng", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
								ENDIF
						//	ENDIF
							
						//	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator")
								IF NOT IS_PED_INJURED(zPED[ped_taos_translator].id)
									////cprintln(debug_trevor3,"tao trans tasked")
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
										actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intA,TRUE)
									ENDIF
									TASK_SYNCHRONIZED_SCENE (zped[ped_taos_translator].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_taotranslator", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
								ENDIF
									
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							
						
						//	ENDIF
							
						//	IF NOT IS_CUTSCENE_PLAYING()
						//	OR HAS_CUTSCENE_FINISHED()
								IF NOT IS_PED_INJURED(zPED[ped_taos_translator].id)
								AND NOT IS_PED_INJURED(zPED[ped_tao].id)
									
							//		zped[ped_tao].id = CREATE_PED(PEDTYPE_MISSION,IG_TAOCHENG,<< 1991.988, 3054.510, 46.215 >>)
							//		zped[ped_taos_translator].id = CREATE_PED(PEDTYPE_MISSION,IG_TAOSTRANSLATOR,<< 1991.988, 3054.510, 46.215 >>)
									SET_PED_COMPONENT_VARIATION(zPED[ped_taos_translator].id,ped_comp_head,0,0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_tao].id,true)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zped[ped_taos_translator].id,true)
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
										actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
										TASK_SYNCHRONIZED_SCENE (zped[ped_tao].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_TaoCheng", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
										TASK_SYNCHRONIZED_SCENE (zped[ped_taos_translator].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_taotranslator", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )								
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intA,TRUE)
									ENDIF
									SET_PED_CONFIG_FLAG(zped[ped_tao].id,PCF_RunFromFiresAndExplosions,FALSE)
									SET_PED_CONFIG_FLAG(zped[ped_tao].id,PCF_DisableExplosionReactions,TRUE)
									SET_PED_CONFIG_FLAG(zped[ped_taos_translator].id,PCF_RunFromFiresAndExplosions,FALSE)
									SET_PED_CONFIG_FLAG(zped[ped_taos_translator].id,PCF_DisableExplosionReactions,TRUE)
									actions[thisI].flag=TAO_EVENT_ANIM_PLAYING
								endif
						//	endif			
							
							
						break
						case TAO_EVENT_ANIM_PLAYING
						
							IF NOT IS_PED_INJURED(zped[ped_tao].id)
							
								IF NOT IS_PED_RAGDOLL(zped[ped_tao].id)							
								//	IF NOT IS_BIT_SET(actions[thisI].intB,0) //this seems wrong. If the translator runs off, Cheng stops his anims
										IF IS_SYNCHRONIZED_SCENE_RUNNING(actions[thisI].intA)
											IF GET_SYNCHRONIZED_SCENE_PHASE(actions[thisI].intA) = 1.0
												actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
												SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intA,TRUE)
												IF GET_RANDOM_INT_IN_RANGE(0,3) < 2										
													TASK_SYNCHRONIZED_SCENE (zped[ped_tao].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_Base_TaoCheng", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
													IF NOT IS_PED_INJURED(zped[ped_taos_translator].id)
														IF NOT IS_PED_RAGDOLL(zped[ped_taos_translator].id)
															IF NOT IS_BIT_SET(actions[thisI].intB,0)
																TASK_SYNCHRONIZED_SCENE (zped[ped_taos_translator].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_Base_taotranslator", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
															ENDIF
														ENDIF
													ENDIF
												ELSE
													TASK_SYNCHRONIZED_SCENE (zped[ped_tao].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_TaoCheng", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
													IF NOT IS_PED_INJURED(zped[ped_taos_translator].id)
														IF NOT IS_PED_RAGDOLL(zped[ped_taos_translator].id)
															IF NOT IS_BIT_SET(actions[thisI].intB,0)
																TASK_SYNCHRONIZED_SCENE (zped[ped_taos_translator].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_taotranslator", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
										
											actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
											TASK_SYNCHRONIZED_SCENE (zped[ped_tao].id, actions[thisI].intA, "misschinese2_crystalmaze", "2int_Loop_A_TaoCheng", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, RBF_PLAYER_IMPACT )						
											SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(actions[thisI].intA,TRUE)
										ENDIF
									ENDIF
								//ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[ped_taos_translator].id)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(zped[ped_taos_translator].id,player_ped_id())
								OR IS_PED_RAGDOLL(zped[ped_taos_translator].id)
								OR IS_PED_INJURED(zped[ped_tao].id)
								OR ((NOT IS_PED_INJURED(zped[ped_taos_translator].id)) AND GET_SCRIPT_TASK_STATUS(zped[ped_taos_translator].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) = DORMANT_TASK)
									IF NOT IS_BIT_SET(actions[thisI].intB,0)										
									//	SET_PED_TO_RAGDOLL(zped[ped_taos_translator].id, 5000, 6000, TASK_RELAX,FALSE,TRUE,TRUE) //a year. Is that long enough for you?
									//	SET_ENTITY_HEALTH(zped[ped_taos_translator].id,0)
									//	SET_PED_AS_NO_LONGER_NEEDED(zped[ped_taos_translator].id)
									//	zped[ped_taos_translator].id = null //so the fail condition for killing him won't be met
										CLEAR_PED_TASKS(zped[ped_taos_translator].id)
										startSeq()
										TASK_SMART_FLEE_PED(null,player_ped_id(),30,20000)
										TASK_COWER(null)
										endseq(zped[ped_taos_translator].id)
										
										SET_BIT(actions[thisI].intB,0)
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[ped_tao].id)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(zped[ped_tao].id,player_ped_id())
								OR IS_PED_RAGDOLL(zped[ped_tao].id)
								OR IS_BIT_SET(actions[thisI].intB,1)
								OR ((NOT IS_PED_INJURED(zped[ped_tao].id)) AND GET_SCRIPT_TASK_STATUS(zped[ped_tao].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) = DORMANT_TASK)
									SET_ENTITY_HEALTH(zped[ped_tao].id,0)
									IF NOT IS_PED_RAGDOLL(zped[ped_tao].id)
									//	SET_PED_TO_RAGDOLL(zped[ped_tao].id, 50000, 60000, TASK_RELAX,FALSE,TRUE,TRUE)
									//	SET_BIT(actions[thisI].intB,1)
									ENDIF
								ENDIF
							ENDIF
						
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 1991.988, 3054.510, 46.215 >>) > 200.0
								IF DOES_ENTITY_EXIST(zped[ped_tao].id)
								AND DOES_ENTITY_EXIST(zped[ped_taos_translator].id)
									DELETE_PED(zped[ped_tao].id)
									DELETE_PED(zped[ped_taos_translator].id)
								ENDIF
								SET_MODEL_AS_NO_LONGER_NEEDED(IG_TAOCHENG)
								SET_MODEL_AS_NO_LONGER_NEEDED(IG_TAOSTRANSLATOR)
								REMOVE_ANIM_DICT("misschinese2_crystalmaze")
							//	DISABLE_NAVMESH_IN_AREA(<<1993.638428,3053.134033,45.214718>>, <<1989.650635,3055.619385,47.840214>>,FALSE)
								IF iNavBlock != -1
									IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlock)
										REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlock)
									ENDIF
								ENDIF
								//cprintln(debug_trevor3,"REMOVE NAVMESH b")
								
								actions[thisI].completed = TRUE
							ENDIF
							
							
							
						break
					
					ENDSWITCH
					
					
						
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1995.1694, 3062.1624, 46.0490>>) < 7
							IF NOT IS_PED_INJURED(zped[ped_tao].id)
								IF GET_SCRIPT_TASK_STATUS(zped[ped_tao].id,SCRIPT_TASK_LOOK_AT_ENTITY) != PERFORMING_TASK
									TASK_LOOK_AT_ENTITY(zped[ped_tao].id,player_ped_id(),4000,SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT|SLF_WHILE_NOT_IN_FOV)										
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(zped[ped_taos_translator].id)
								IF GET_SCRIPT_TASK_STATUS(zped[ped_taos_translator].id,SCRIPT_TASK_LOOK_AT_ENTITY) != PERFORMING_TASK									
									TASK_LOOK_AT_ENTITY(zped[ped_taos_translator].id,player_ped_id(),4000,SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT|SLF_WHILE_NOT_IN_FOV)									
								ENDIF
							ENDIF
						ENDIF
				
					
				break
				
				
				case act_stat_double_kill	
					int iu
					iu=0
					
					thisSnipeCount = 0
					repeat count_of(zped) iu
						IF DOES_ENTITY_EXIST(zped[iu].id)
							IF IS_PED_INJURED(zped[iu].id)
								if zPED[iu].sniped = FALSE
									IF GET_PED_CAUSE_OF_DEATH(zped[iu].id) = WEAPONTYPE_DLC_ASSAULTSNIPER
									OR GET_PED_CAUSE_OF_DEATH(zped[iu].id) = WEAPONTYPE_HEAVYSNIPER
									OR GET_PED_CAUSE_OF_DEATH(zped[iu].id) = WEAPONTYPE_REMOTESNIPER
									OR GET_PED_CAUSE_OF_DEATH(zped[iu].id) = WEAPONTYPE_SNIPERRIFLE
									
										thisSnipeCount++
									ENDIF
									zPED[iu].sniped = TRUE
								ENDIF							
							endif
						endif
					ENDREPEAT
					
					
					
					IF thisSnipeCount > 0 
						cprintln(debug_trevor3,"SNIPED")
					ENDIF
					IF thisSnipeCount > 1
						cprintln(debug_trevor3,"SNIPED BAM")
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(CHI2_ONE_SHOT_TWO_KILLS)						
						actions[thisI].completed = true
					endif
				break
				
				case act_MUSIC_start_snipe
					PLAYMUSIC("CHN2_SNIPE_START",false,"CHN2_EXPLODE")
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
					
					actions[thisI].completed = true					
				BREAK
				
				CASE act_MUSIC_approach_house_or_alert		
					IF IS_CONDITION_TRUE(COND_ALL_ENEMY_ALERTED)
						PLAYMUSIC("CHN2_SPOTTED",false,"CHN2_EXPLODE")
					ELSE
						PLAYMUSIC("CHN2_TO_HOUSE",false,"CHN2_EXPLODE")
					ENDIF
					actions[thisI].completed = true										
				BREAK
				
				case act_create_petrol_can_with_blip
					IF DOES_PICKUP_EXIST(pickup_gas)
						REMOVE_PICKUP(pickup_Gas)
					ENDIF
		
					int PlacementFlags
					PlacementFlags = 0
					SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_SNAP_TO_GROUND))
					SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_UPRIGHT)) 
					SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_FIXED)) 
					
					IF NOT DOES_PICKUP_EXIST(pickup_gas)
						pickup_gas = CREATE_PICKUP(PICKUP_WEAPON_PETROLCAN,<< 2436.7747, 4967.4868, 41.4 >>,PlacementFlags)												
						ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(pickup_gas,"V_8_BasementRm")
						missionBlip = CREATE_BLIP_FOR_PICKUP(pickup_gas)
						SET_BLIP_NAME_FROM_TEXT_FILE(missionBlip, "FRMBLIP")
					ENDIF
					actions[thisI].completed = true
				break
				
				case act_stage_prep
					IF NOT is_event_active(events_spawn_house)			
					
						
						DISABLE_TAXI_HAILING(TRUE)
						START_AUDIO_SCENE("CHI_2_SHOOTOUT_STEALTH")
						SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
						actions[thisI].completed = true						
					ENDIF
				break
				
			
				
				case act_remove_initial_assets				
					REMOVE_ANIM_DICT("misschinese2_barrelRoll")
					REMOVE_ANIM_SET("move_m@gangster@var_e")
					REMOVE_ANIM_SET("move_m@gangster@var_f")
					REMOVE_ANIM_SET("move_m@gangster@generic")
					REMOVE_ANIM_DICT("misschinese2_bank5")
					actions[thisI].completed = true
				break
				
				case act_outside_peds_flee
					SWITCH actions[thisI].flag
						CASE 0
							actions[thisI].intA = GET_GAME_TIMER() + 4000
							actions[thisI].flag++
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > actions[thisI].intA
								repeat count_of(zped) actions[thisI].intA
									IF NOT IS_PED_INJURED(zped[actions[thisI].intA].id)
										IF GET_INTERIOR_FROM_ENTITY(zped[actions[thisI].intA].id) = null
											zped[actions[thisI].intA].role = role_innocent
											pedReactionState[actions[thisI].intA].state = STATE_FLEE
											pedReactionState[actions[thisI].intA].actionFlag=0
											pedReactionState[actions[thisI].intA].subFlag=0
											set_ped_action(actions[thisI].intA,action_flee)
											SET_PED_RELATIONSHIP_GROUP_HASH(zped[actions[thisI].intA].id,HASH_IGNORE_GROUP)										
											CLEANUP_AI_PED_BLIP(zped[actions[thisI].intA].aiBlip)
										endif
									endif
								endrepeat
						//		actions[thisI].completed = true	
							ENDIF
						BREAK
					ENDSWITCH
				BREAK	
				
				case act_remove_upstairs_peds				
					repeat count_of(zped) actions[thisI].intA
						IF NOT IS_PED_INJURED(zped[actions[thisI].intA].id)
							IF IS_ENTITY_IN_ANGLED_AREA(zped[actions[thisI].intA].id, <<2457.884766,4981.169434,50.005180>>, <<2441.579834,4965.088379,56.240055>>, 34.312500)
								delete_ped(zped[actions[thisI].intA].id)
							ENDIF
						ENDIF
					ENDREPEAT
					actions[thisI].completed = true	
				break
				
				case act_MUSIC_enter_house
					PLAYMUSIC("CHN2_ENTER_HOUSE",false,"CHN2_EXPLODE")
					actions[thisI].completed = true						
				break
				case act_remaining_peds_flee
					int iS
					repeat count_of(zped) iS
						IF NOT IS_PED_INJURED(zped[iS].id)
							IF GET_ROOM_KEY_FROM_ENTITY(zped[iS].id) != GET_HASH_KEY("V_8_BasementRm")
								set_ped_action(is,action_flee)
								zped[is].role = role_innocent
								pedReactionState[is].state = state_flee
								zped[is].actionFlag = 0
							ENDIF
						ENDIF
					endrepeat
					actions[thisI].completed = true	
				break
				case act_window_ped_walks_inside
					SWITCH actions[thisI].flag
						case 0					
							actions[thisI].intA = get_ped_with_placement(pos_inside_right_window)
							IF actions[thisI].intA != -1
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(zped[actions[thisI].intA].id)			
								set_ped_action(actions[thisI].intA,action_move_inside_house)
								actions[thisI].completed = true	
							ELSE
								actions[thisI].completed = true	
							ENDIF
						BREAK
					ENDSWITCH
				break
				case act_put_player_in_action_mode				
					IF IS_PED_SHOOTING(player_ped_id())
					OR IS_CONDITION_TRUE(COND_ALL_ENEMY_ALERTED)
						SET_PED_USING_ACTION_MODE(player_ped_id(),TRUE)
						actions[thisI].completed = true	
					ENDIF
				break
				case act_end_stealth_audio_Scene
					STOP_AUDIO_SCENE("CHI_2_SHOOTOUT_STEALTH")
					
					actions[thisI].completed = true	
				break
				case act_enemy_alerted_audio_Scene
					START_AUDIO_SCENE("CHI_2_SHOOTOUT_ENEMIES_ALERTED")
					actions[thisI].completed = true	
				break
				case act_end_enemy_alerted_audio_Scene
					IF IS_AUDIO_SCENE_ACTIVE("CHI_2_SHOOTOUT_ENEMIES_ALERTED")
						STOP_AUDIO_SCENE("CHI_2_SHOOTOUT_ENEMIES_ALERTED")
					ENDIF
					actions[thisI].completed = true
				break
				
				case act_audio_scene_in_house
					IF NOT IS_AUDIO_SCENE_ACTIVE("CHI_2_SHOOTOUT_IN_HOUSE")
						IF IS_CONDITION_TRUE(COND_PLAYER_IN_HOUSE)						
							START_AUDIO_SCENE("CHI_2_SHOOTOUT_IN_HOUSE")
						ENDIF
					ELSE
						IF IS_CONDITION_FALSE(COND_PLAYER_IN_HOUSE)						
							STOP_AUDIO_SCENE("CHI_2_SHOOTOUT_IN_HOUSE")
						ENDIF
					ENDIF
				break
				
				case act_reset_bottle_shooting_ped_accuracy
					IF IS_CONDITION_TRUE(COND_ALL_ENEMY_ALERTED)
					OR (NOT  IS_PED_INJURED(zped[enum_to_int(pos_shooting_bottle_1)].id) AND IS_PED_IN_COMBAT(zped[enum_to_int(pos_shooting_bottle_1)].id))
					OR (NOT  IS_PED_INJURED(zped[enum_to_int(pos_shooting_bottle_2)].id) AND IS_PED_IN_COMBAT(zped[enum_to_int(pos_shooting_bottle_2)].id))
						IF NOT IS_PED_INJURED(zped[enum_to_int(pos_shooting_bottle_1)].id)
							SET_PED_ACCURACY(zped[enum_to_int(pos_shooting_bottle_1)].id,45)
						ENDIF
						IF NOT IS_PED_INJURED(zped[enum_to_int(pos_shooting_bottle_2)].id)
							SET_PED_ACCURACY(zped[enum_to_int(pos_shooting_bottle_2)].id,45)
						ENDIF
						actions[thisI].completed = true
					ENDIF
				break
				
				case act_modify_ped_seeing_range
					IF IS_CONDITION_TRUE(COND_ALL_ENEMY_ALERTED)
						repeat count_of(zped) actions[thisI].intA
							IF NOT IS_PED_INJURED(zped[actions[thisI].intA].id)						
								SET_PED_SEEING_RANGE(zped[actions[thisI].intA].id,60.0)								 //increased from 19
							ENDIF
						ENDREPEAT
						actions[thisI].completed = TRUE
					ELSE
						switch actions[thisI].flag
							case 0
								IF IS_CONDITION_TRUE(COND_PLAYER_IN_HOUSE)
									repeat count_of(zped) actions[thisI].intA
										IF NOT IS_PED_INJURED(zped[actions[thisI].intA].id)
											IF zped[actions[thisI].intA].role = role_guard_outside
												SET_PED_SEEING_RANGE(zped[actions[thisI].intA].id,5.0)
											ENDIF
										ENDIF
									ENDREPEAT
									actions[thisI].flag++
								ENDIF
							BREAK
							CASe 1							
								IF IS_CONDITION_FALSE(COND_PLAYER_IN_HOUSE)
									repeat count_of(zped) actions[thisI].intA
										IF NOT IS_PED_INJURED(zped[actions[thisI].intA].id)
											IF zped[actions[thisI].intA].role = role_guard_outside
												SET_PED_SEEING_RANGE(zped[actions[thisI].intA].id,30.0)
											ENDIF
										ENDIF
									ENDREPEAT
									actions[thisI].flag=0
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				break
				case act_basement_ped
					switch actions[thisI].flag
						case 0
							REQUEST_ANIM_DICT("misschinese2_crystalmaze")
							actions[thisI].flag++						
						BREAK
						CASE 1
							IF HAS_ANIM_DICT_LOADED("misschinese2_crystalmaze")
								IF NOT IS_PED_INJURED(zped[enum_to_int(pos_innocent_drugden)].id)								
									actions[thisI].intA = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)
									SET_SYNCHRONIZED_SCENE_LOOPED(actions[thisI].intA,TRUE)
									TASK_SYNCHRONIZED_SCENE (zped[actions[thisI].intA].id, actions[thisI].intB, "misschinese2_crystalmaze", "_stand_to_aim", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )

									actions[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 2
						BREAK
					ENDSWITCH
					
				break
				case act_spawn_blazer
					switch actions[thisI].flag
						case 0
							request_model(blazer)
							actions[thisI].flag++
						break
						case 1
							if has_model_loaded(blazer)								
								vehBlazer = create_vehicle(Blazer,<<2478.5867, 4941.8032, 43.6736>>, 227.2903)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehBlazer)
								actions[thisI].completed=true
							endif
						break
					ENDSWITCH
				break
				CASE ACT_PREP_PETROL_POUR
					SET_PED_USING_ACTION_MODE(player_ped_id(),FALSE)
					RESET_GAS_TRAIL(FALSE)
					REMOVE_DEAD_PEDS_ON_TRAIL_ROUTE()
					actions[thisI].completed=true
				BREAK
				
				CASE ACT_CONTROL_PETROL_BLIPS	
					SWITCH actions[thisI].flag
						CASE 0
							IF IS_CONDITION_TRUE(COND_PLAYER_HAS_PETROL_CAN)
								if does_blip_exist(missionBlip)
									REMOVE_BLIP(missionBlip)
								endif
								
								HIDE_GAS_TRAIL(FALSE)
								
								IF IS_THIS_PRINT_BEING_DISPLAYED("FRMFLC")
									CLEAR_PRINTS()
								ENDIF
								
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(CHI2_GAS_POUR_TIME)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								IF gasInCan = 0										
									gasInCan = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
								ENDIF
								actions[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_CONDITION_TRUE(COND_PLAYER_HAS_PETROL_CAN)
								INFORM_MISSION_STATS_OF_INCREMENT(CHI2_GAS_USED,gasInCan - GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN),TRUE)
								
								oGasCan = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()),20.0,GET_WEAPONTYPE_MODEL(WEAPONTYPE_PETROLCAN),FALSE)
									
								IF oGasCan != null
									if does_blip_exist(missionBlip)
										REMOVE_BLIP(missionBlip)
									endif
									missionBlip = CREATE_BLIP_FOR_OBJECT(oGasCan)							
									HIDE_GAS_TRAIL()
									INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
								endif
								
								actions[thisI].flag=0
							ENDIF
						BREAK
					ENDSWITCH
					
					IF IS_GAS_TRAIL_COMPLETE()
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
							
						actions[thisI].completed = true
							
						add_event(events_check_player_out_front)
					ENDIF
								
		/*
						IF NOT HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)	
						OR NOT DOES_PICKUP_EXIST(pickup_gas)					
							oGasCan = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()),20.0,GET_WEAPONTYPE_MODEL(WEAPONTYPE_PETROLCAN),FALSE)
									
							IF oGasCan != null
								if does_blip_exist(missionBlip)
									REMOVE_BLIP(missionBlip)
								endif
								missionBlip = CREATE_BLIP_FOR_OBJECT(oGasCan)							
								HIDE_GAS_TRAIL()
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
							endif
						ELSE							
							HIDE_GAS_TRAIL(FALSE)
							
							if does_blip_exist(missionBlip)
								REMOVE_BLIP(missionBlip)
							endif
							
							IF IS_THIS_PRINT_BEING_DISPLAYED("FRMFLC")
								CLEAR_PRINTS()
							ENDIF
							
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(CHI2_GAS_POUR_TIME)
							
							IF gasInCan = 0										
								gasInCan = GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
							ELSE
								INFORM_MISSION_STATS_OF_INCREMENT(CHI2_GAS_USED,gasInCan - GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN),TRUE)
							ENDIF
						ENDIF											
					
						IF IS_GAS_TRAIL_COMPLETE()
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, CHI2_GAS_POUR_TIME)
							
							actions[thisI].completed = true
							
							add_event(events_check_player_out_front)
						ENDIF
					*/
				BREAK
				
				CASE ACT_PETROL_ASSIST_ROUTE
					switch actions[thisI].flag
						case 0
							REQUEST_WAYPOINT_RECORDING("arm2_30")
							actions[thisI].flag++
						break
						case 1
							IF GET_IS_WAYPOINT_RECORDING_LOADED("arm2_30")
								IF IS_CONDITION_TRUE(COND_PLAYER_HAS_PETROL_CAN)

									USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("arm2_30",TRUE)
									ASSISTED_MOVEMENT_SET_ROUTE_PROPERTIES("arm2_30", EASSISTED_ROUTE_ACTIVE_WHEN_STRAFING)

									actions[thisI].flag++
								endif
							endif
						break
						case 2
							IF NOT IS_CONDITION_TRUE(COND_PLAYER_HAS_PETROL_CAN)
								USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("arm2_30",FALSE)
								actions[thisI].flag=1
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				CASE ACT_FUDGE_RADAR_ZOOM
					IF NOT IS_PED_INJURED(player_ped_id())
						IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = null //outside
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 2451.0801, 4961.4634, 44.4392 >>) < 20.0
								SET_RADAR_AS_INTERIOR_THIS_FRAME()
							ENDIF
						endif
					ENDIF
				BREAK
				
				CASE ACT_MUSIC_PICKS_UP_JERRY_CAM
					PlAYMUSIC("CHN2_JERRY_CAN",false,"CHN2_EXPLODE")
					actions[thisI].completed = TRUE					
				BREAK
				
				CASE ACT_MUSIC_TRAIL_IGNITED
							
					playmusic("CHN2_EXIT_HOUSE",false,"CHN2_EXPLODE")
					//g_eFarmhouseFireState = FARMHOUSE_IGNITE_FIRE
					////cprintln(DEBUG_TREVOR3,"PLay trail sound at: ",vIgnitePos)
					PLAY_SOUND_FROM_COORD(iFireIgniteSound,"FarmhouseFire_Ignite",vIgnitePos,"CHINESE2_SOUNDS")
					actions[thisI].completed = TRUE					
				BREAK
				
				CASE ACT_AUDIO_SCENE_WITH_CAN
					START_AUDIO_SCENE("CHI_2_POUR_GAS")
					actions[thisI].completed = TRUE	
				BREAK
				
				CASE ACT_PREP_EXPLOSION_AUDIO
					SWITCH actions[thisI].flag
						CASE 0
							REQUEST_SCRIPT_AUDIO_BANK("FARMHOUSE_FIRE")
							REQUEST_SCRIPT_AUDIO_BANK("FARMHOUSE_FIRE_BG")
						BREAK
					ENDSWITCH
				BREAK
				
				case act_remove_barrel_peds
					SWITCH actions[thisI].flag
						CASE 0
							actions[thisI].intA = get_ped_with_placement(pos_barrels_1)
							actions[thisI].flag++
						BREAK
						CASE 1
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[actions[thisI].intA].id,<<2454.8953, 4950.8945, 44.1488>>) > 100
							and GET_DISTANCE_BETWEEN_ENTITIES(zped[actions[thisI].intA].id,player_ped_id()) > 100
								
								int iCleanupPEd
								iCleanupPEd = get_ped_with_placement(pos_barrels_1)
								CLEANUP_AI_PED_BLIP(zPED[iCleanupPEd].aiBlip)
								IF NOT IS_PED_INJURED(zPED[iCleanupPEd].id)
									SET_PED_KEEP_TASK(zPED[iCleanupPEd].id,TRUE)
									SET_PED_AS_NO_LONGER_NEEDED(zped[iCleanupPEd].id)
								ENDIF
								zPED[iCleanupPEd].id = null
								
								iCleanupPEd = get_ped_with_placement(pos_barrels_2)
								CLEANUP_AI_PED_BLIP(zPED[iCleanupPEd].aiBlip)
								IF NOT IS_PED_INJURED(zPED[iCleanupPEd].id)
									SET_PED_KEEP_TASK(zPED[iCleanupPEd].id,TRUE)
									SET_PED_AS_NO_LONGER_NEEDED(zped[iCleanupPEd].id)
								ENDIF
								zPED[iCleanupPEd].id = null	
															
								iCleanupPEd = get_ped_with_placement(pos_garden_chat_2)
								IF NOT IS_PED_INJURED(zPED[iCleanupPEd].id)
									SET_PED_KEEP_TASK(zped[iCleanupPEd].id,TRUE)
									SET_PED_AS_NO_LONGER_NEEDED(zped[iCleanupPEd].id)
								ENDIF
								CLEANUP_AI_PED_BLIP(zPED[iCleanupPEd].aiBlip)
								zPED[iCleanupPEd].id = null
								
								IF IS_VEHICLE_DRIVEABLE(BarrelVan)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(BarrelVan)
								ENDIF
								actions[thisI].completed = TRUE
						
							ENDIF
						break
					ENDSWITCH
				BREAK
				
				case act_player_got_to_basement_sneakily
				//	IF bPlayerSeen
						IF CREATE_GANG_PED(enum_to_int(pos_emergency_attacker),A_M_M_Hillbilly_01,<<2433.1226, 4960.4702, 45.8218>>,301,WEAPONTYPE_SMG,TRUE,TRUE)
							SET_PED_ACCURACY(zped[enum_to_int(pos_emergency_attacker)].id,100)
							IF NOT IS_PED_INJURED(zped[enum_to_int(pos_inside_cards1)].id)
								SET_PED_ACCURACY(zped[enum_to_int(pos_inside_cards1)].id,100)
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[enum_to_int(pos_inside_cards2)].id)
								SET_PED_ACCURACY(zped[enum_to_int(pos_inside_cards2)].id,100)
							ENDIF
							
							IF NOT IS_PED_INJURED(zped[enum_to_int(pos_inside_cards3)].id)
								SET_PED_ACCURACY(zped[enum_to_int(pos_inside_cards3)].id,100)
							ENDIF
							
							TASK_COMBAT_PED(zped[enum_to_int(pos_emergency_attacker)].id,player_ped_id())
							
							SET_PED_TARGET_LOSS_RESPONSE(zped[enum_to_int(pos_emergency_attacker)].id,TLR_NEVER_LOSE_TARGET)
							SET_ENTITY_HEALTH(zped[enum_to_int(pos_emergency_attacker)].id,200)
							
							actions[thisI].completed = TRUE
						ENDIF	
				//	endif
				break
			ENDSWITCH
		
			IF actions[thisI].flag = CLEANUP
				actions[thisI].completed = TRUE
				actions[thisI].flag = 0
				actions[thisI].active = FALSE
				//////cprintln(debug_trevor3,"a3")
			ENDIF
			
		ENDIF
	ENDIF

	
ENDPROC

PROC INSTRUCTIONS(int thisI, enumInstructions eInstruction, andorEnum andOr1=cFORCEtrue, enumconditions cond1=COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2=COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3=COND_NULL)
	//reset if instructino is new
	IF instr[thisI].ins != eInstruction
		instr[thisI].bCompleted = FALSE
		instr[thisI].ins = eInstruction
	ENDIF
	
	#if IS_DEBUG_BUILD instr[thisI].condTrue = FALSE #endif
	IF NOT instr[thisI].bCompleted
		IF checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3)
			#if IS_DEBUG_BUILD instr[thisI].condTrue = TRUE #endif
			SWITCH instr[thisI].ins
				//drive to alley
				CASE INS_NULL
				BREAK
				
				CASE INS_SPECIAL_ABILITY_BOOST
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					//	PRINT_HELP("FRMRGE")
						SET_SPECIAL_ABILITY_MULTIPLIER(2.0)
						instr[thisI].bCompleted = TRUE	
					ENDIF
				BREAK
				
				CASE INS_SPECIAL_ABILITY
					
					IF IS_SPECIAL_ABILITY_METER_FULL(PLAYER_ID())
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP("USESPEC_KM")	
						ELSE
							PRINT_HELP("USESPEC")
						ENDIF
					ENDIF
					instr[thisI].bCompleted = TRUE	
				BREAK
				
				CASE ins_enemy_reacting
					SWITCH instr[thisI].flag
						CASE 0
							PRINT_HELP("FRMFLE")
							instr[thisI].bCompleted = TRUE
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ins_all_enemy_reacting
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PRINT("FRMLK",DEFAULT_GOD_TEXT_TIME,1)
						instr[thisI].bCompleted = TRUE
					ENDIF
						
				BREAK
				
				
				
				CASE INS_DESTROY_METH_LAB
					PRINT("FRMSNP",DEFAULT_GOD_TEXT_TIME,1)
					instr[thisI].bCompleted = TRUE
				BREAK
				
				CASE ins_fast_stealth
					switch instr[thisI].flag
						case 0
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								instr[thisI].intA = GET_GAME_TIMER() + 1000
								instr[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF GET_GAME_TIMER() > instr[thisI].intA
								IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
									PRINT_HELP("FRMSTLTH_KM")
								ELSE
									PRINT_HELP("FRMSTLTH")
								ENDIF
								instr[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				
				case INS_QUICK_KILL_HELP
					switch instr[thisI].flag
						case 0					
							IF GET_PROFILE_SETTING(PROFILE_CONTROLLER_CINEMATIC_SHOOTING) != 0
								instr[thisI].intA = GET_GAME_TIMER() + 4000
								instr[thisI].flag++		
							ENDIF
						break
						
						case 1			
							IF GET_GAME_TIMER() > instr[thisI].intA
								weapon_type playerWeapon
								playerWeapon = WEAPONTYPE_UNARMED
								GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),playerWeapon)					
								if playerWeapon != WEAPONTYPE_SNIPERRIFLE						
								and playerWeapon != WEAPONTYPE_HEAVYSNIPER
								and playerWeapon != WEAPONTYPE_UNARMED
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
												PRINT_HELP("FRMSHOT")
												instr[thisI].bCompleted = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							endif
						break
					ENDSWITCH
				break
				CASE INS_PICK_UP_GAS_CAN
					PRINT("FRMFLC",DEFAULT_GOD_TEXT_TIME,1)
					SET_BLIP_NAME_FROM_TEXT_FILE(missionBlip, "FRMBLIPB")
					instr[thisI].bCompleted = TRUE
				BREAK
				CASE INS_USING_GAS_CAN
					switch instr[thisI].flag
						case 0	
							PRINT_HELP("FRMPET2")
							instr[thisI].flag++
						BREAK
						CASE 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PRINT_NOW("FRMTRL",DEFAULT_GOD_TEXT_TIME,1)								
								instr[thisI].flag++								
							ENDIF
						BREAK
						case 2
							WEAPON_TYPE wep 
							wep = WEAPONTYPE_UNARMED
							IF GET_CURRENT_PED_WEAPON(player_ped_id(),wep)
								IF wep = WEAPONTYPE_PETROLCAN
									IF IS_CONTROL_PRESSED(player_control,INPUT_ATTACK)
										clear_help()
									ENDIF
								ENDIF								
							ENDIF
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("FRMTRL2")
								instr[thisI].flag++	
							ENDIF
						break
						case 3
							IF IS_GAS_TRAIL_COMPLETE()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) != null //tell player to exit house										
										IF NOT DOES_BLIP_EXIST(missionBlip)
											CLEAR_PRINTS()
											PRINT("FRMSHP",DEFAULT_GOD_TEXT_TIME,1)
											missionBlip = CREATE_BLIP_FOR_COORD(<<2454.8953, 4950.8945, 44.1488>>)
										ENDIF
									ELSE
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2454.8953, 4950.8945, 44.1488>>) < 5
											CLEAR_PRINTS()
											IF DOES_BLIP_EXIST(missionBlip)
												remove_blip(missionBlip)
											ENDIF
											PRINT("FRMIGN",DEFAULT_GOD_TEXT_TIME,1) //shoot the gas trail to ignite it		
											START_AUDIO_SCENE("CHI_2_SHOOT_GAS")
											instr[thisI].flag++	
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 4
							IF IS_CONDITION_TRUE(COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE)
								IF IS_THIS_PRINT_BEING_DISPLAYED("FRMIGN")
									CLEAR_PRINTS()
								ENDIF
							ENDIF
							IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) != null
								instr[thisI].flag = 3
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ins_missed_a_bit
					PRINT_HELP("FRMMISS")
					instr[thisI].bCompleted = TRUE
				BREAK
				
				case ins_dont_abandon_farm
					IF IS_CONDITION_TRUE(COND_GAS_TRAIL_COMPLETE)
						PRINT("FRMFRH",DEFAULT_GOD_TEXT_TIME,1)
					ELSE
						PRINT("FRMFRG",DEFAULT_GOD_TEXT_TIME,1)
					ENDIF
					instr[thisI].bCompleted = TRUE
				break
				
				case INS_GET_CLEAR_OF_FARM
					PRINT("FRMLEAVE",1,1)
					IF IS_CONDITION_TRUE(COND_PLAYER_300_FROM_FARM)
						CLEAR_PRINTS()
						instr[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				CASE INS_LOSE_WANTED_LEVEL
					PRINT("LOSE_WANTED",DEFAULT_GOD_TEXT_TIME,1) 
					
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF			
ENDPROC

PROC SET_DIALOGUE_INT(int iDia, enumDialogue dialogueToSet, int iNewIntValue)
	IF dia[iDia].dial = dialogueToSet
		dia[iDia].intA = iNewIntValue
	ELSE
		SCRIPT_ASSERT("Incorrect value passed in to SET_DIALOGUE_INT()")
	ENDIF	
endproc

PROC SET_DIALOGUE_FLAG(int iDia, enumDialogue dialogueToSet, int iNewIntValue)
	IF dia[iDia].dial = dialogueToSet
		dia[iDia].flag = iNewIntValue
	ELSE
		SCRIPT_ASSERT("Incorrect value passed in to SET_DIALOGUE_FLAG()")
	ENDIF
ENDPROC

PROC FORCE_INSTRUCTION_STATE(int thisI, enumInstructions thisInstruction, bool setCompleted=TRUE)
	instr[thisI].bCompleted = setCompleted
	instr[thisI].ins = thisInstruction
ENDPROC

PROC FORCE_DIALOGUE_STATE(int thisI, enumDialogue thisDialogue, bool setCompleted=TRUE)
	dia[thisI].bCompleted = setCompleted
	dia[thisI].dial = thisDialogue
ENDPROC

PROC DIALOGUE(int thisI, enumDialogue thisDia, andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL, andorEnum andOr4=cIGNORE, enumconditions cond4 = COND_NULL)
	IF dia[thisI].dial != thisDia
		dia[thisI].bCompleted = FALSE
		dia[thisI].dial = thisDia
	ENDIF		
	
	#if IS_DEBUG_BUILD dia[thisI].condTrue = FALSE #endif
	
	IF NOT dia[thisI].bCompleted
		
		IF checkANDOR(andOr1,cond1,andOr2,cond2,andOr3,cond3,andOr4,cond4)
			#if IS_DEBUG_BUILD dia[thisI].condTrue = TRUE #endif
			IF NOT dia[thisI].bStarted dia[thisI].bStarted=TRUE ENDIF
			SWITCH dia[thisI].dial
				
				CASE DIA_NULL
				BREAK
				
				CASE dia_whatnow		
					IF CREATE_CONVERSATION_EXTRA("TRV_WTF",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUE
					ENDIF						
				break	
				
				case dia_trevor_rage_comments				
					SWITCH dia[thisI].flag
						CASE 0
							//leave a puse so spotted dialogue from AI can play out
							dia[thisI].intA = GET_GAME_TIMER() + 2000
							dia[thisI].flag++
						BREAK
						CASE 1
							IF get_Game_timer() > dia[thisI].intA	
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									dia[thisI].intA = get_Game_timer() + 2000
								ELSE
									int iSpeaker
									iSpeaker = 0
									IF FIND_CLOSEST_PED_OF_ROLE(iSpeaker,role_guard_house,true)	
										IF CREATE_CONVERSATION_EXTRA("RAGE",1,player_ped_id(),"Trevor")
											dia[thisI].intA = get_Game_timer() + GET_RANDOM_INT_IN_RANGE(8000,14000)
										ENDIF											
									ELSE
										dia[thisI].bCompleted=true
									ENDIF
								ENDIf
							ENDIF	
						BREAK
					ENDSWITCH
				break
				case dia_cook_sees_ped_die
					
					dia[thisI].intA = get_ped_with_placement(pos_innocent_drugden)
					IF dia[thisI].intA != -1
						IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
							IF CREATE_CONVERSATION_EXTRA("COOKSEE",3,zped[dia[thisI].intA].id,"ONEILCOOK")
								dia[thisI].bCompleted=true
							ENDIF
						ELSE
							dia[thisI].bCompleted=true
						ENDIF
					endif
				break
				case dia_stop_trevor_getting_to_basement
					SWITCH dia[thisI].flag
						CASE 0
							dia[thisI].intA = get_ped_with_placement(pos_inside_cards1)
							dia[thisI].intB = get_ped_with_placement(pos_inside_cards2)
							IF dia[thisI].intA != -1 and dia[thisI].intB != -1
								dia[thisI].flag++
							endif
						BREAK
						CASE 1
							IF IS_PED_INJURED(zped[dia[thisI].intA].id)
								dia[thisI].intA = dia[thisI].intB
								IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
									dia[thisI].flag++
								ELSE
									dia[thisI].bCompleted = TRUe
								ENDIF
							ELSE
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
								IF CREATE_CONVERSATION_EXTRA("ONEILGUARD8",5,zped[dia[thisI].intA].id,"ONEILGUARD1")
									dia[thisI].flag++
								ENDIF
							ELSE
								dia[thisI].bCompleted = TRUe
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
								IF CREATE_CONVERSATION_EXTRA("ONEILGUARD9",5,zped[dia[thisI].intA].id,"ONEILGUARD1")
									dia[thisI].flag++
								ENDIF
							ELSE
								dia[thisI].bCompleted = TRUe
							ENDIF
						BREAK
						case 4
							int ispeaker
							IF FIND_CLOSEST_PED_OF_ROLE(iSpeaker,role_guard_house,true)	
								IF iSpeaker != dia[thisI].intA
									dia[thisI].intA = iSpeaker
									dia[thisI].flag++
								ELSE
									dia[thisI].bCompleted = TRUe
								ENDIF
							ELSe
								dia[thisI].bCompleted = TRUe
							ENDIF
						break
						
						case 5
							IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
								IF CREATE_CONVERSATION_EXTRA("ONEILGUARD10",5,zped[dia[thisI].intA].id,"ONEILGUARD2")
									dia[thisI].bCompleted = TRUe
								ENDIF
							ELSE
								dia[thisI].bCompleted = TRUe
							ENDIf
						break
					ENDSWITCH						
				break
				case dia_dont_let_him_down
					dia[thisI].intA = get_ped_with_placement(pos_innocent_drugden)
					IF 	dia[thisI].intA != -1
						IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
							IF CREATE_CONVERSATION_EXTRA("ONEILGUARD11",6,zped[dia[thisI].intA].id,"ONEILGUARD2")
								dia[thisI].bCompleted = TRUe
							ENDIF
						ELSE
							dia[thisI].bCompleted = TRUe
						ENDIF
					ENDIf
				break
				case dia_hes_in_meth_lab
					dia[thisI].intA = get_ped_with_placement(pos_innocent_drugden)
					IF dia[thisI].intA != -1
						IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
							IF CREATE_CONVERSATION_EXTRA("ONEILGUARD12",6,zped[dia[thisI].intA].id,"ONEILGUARD2")
								dia[thisI].bCompleted = TRUe
							ENDIF
						ELSE
							dia[thisI].bCompleted = TRUe
						ENDIF
					ENDIF
				break
				
				case dia_trevor_see_basement_peds_leave
					IF CREATE_CONVERSATION_EXTRA("CHI2_drive",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUe
					ENDIF
				break
				
				case dia_trevor_spotted_outside
					//dia[thisI].intA is set elsewhere in script
					SWITCH dia[thisI].flag
						CASE 0
							IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id)
								dia[thisI].flag = GET_RANDOM_INT_IN_RANGE(1,3)
							ENDIF
						BREAK
						CASE 1						
							IF CREATE_CONVERSATION_EXTRA("SEETREVOR",3,zped[dia[thisI].intA].id,"CHIN2goon1")
								dia[thisI].bCompleted = TRUe
							ENDIF
						BREAK
						CASE 2
							IF CREATE_CONVERSATION_EXTRA("GOON2TREV",4,zped[dia[thisI].intA].id,"CHIN2goon2")
								dia[thisI].bCompleted = TRUe
							ENDIF							
						BREAK
					ENDSWITCH				
				break
				CASE dia_find_trevor
					SWITCH dia[thisI].flag
						CASE 0
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF FIND_CLOSEST_PED_OF_ROLE(dia[thisI].intB,role_guard_outside,true)
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(zped[dia[thisI].intB].id)
								IF dia[thisI].intC < 3
									IF CREATE_CONVERSATION_EXTRA("ONEILGUARD5",5,zped[dia[thisI].intB].id,"ONEILGUARD1")
										dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000,12000)
										dia[thisI].intC++
										dia[thisI].flag=0
									ENDIF
								ELIF dia[thisI].intC < 6
									IF CREATE_CONVERSATION_EXTRA("ONEILGUARD6",6,zped[dia[thisI].intB].id,"ONEILGUARD2")
										dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000,12000)
										dia[thisI].intC++
										dia[thisI].flag=0
									ENDIF
								ELSE
									dia[thisI].bCompleted = TRUE
								ENDIF
							ELSE
								dia[thisI].flag=0
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				case dia_attacking_trevor_outside
					//COOKATT oneilcook 3 (4)
					//GOONIN chin2goon1 3 (4)
					//GOON2ATTO chin2goon2 4 (4)
					SWITCH dia[thisI].flag
						CASE 0
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF FIND_CLOSEST_PED_OF_ROLE(dia[thisI].intB,role_guard_outside,true)
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(zped[dia[thisI].intB].id)
								SWITCH dia[thisI].intC
									CASE 0
									FALLTHRU
									CASE 2
									FALLTHRU
									CASE 5
									FALLTHRU									
									CASE 9
										IF CREATE_CONVERSATION_EXTRA("COOKATT",3,zped[dia[thisI].intB].id,"oneilcook")
											dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000,12000)
											dia[thisI].intC++
											dia[thisI].flag=0
										ENDIF
									BREAK
									
									CASE 1
									FALLTHRU
									CASE 6
									FALLTHRU
									CASE 8
									FALLTHRU
									CASE 11
										IF CREATE_CONVERSATION_EXTRA("GOONATT",3,zped[dia[thisI].intB].id,"chin2goon1")
											dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000,12000)
											dia[thisI].intC++
											dia[thisI].flag=0
										ENDIF
									BREAK
									
									CASE 3
									FALLTHRU
									CASE 4
									FALLTHRU
									CASE 7
									FALLTHRU
									CASE 10
										IF CREATE_CONVERSATION_EXTRA("GOON2ATTO",4,zped[dia[thisI].intB].id,"chin2goon2")
											dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000,12000)
											dia[thisI].intC++
											dia[thisI].flag=0
										ENDIF
									BREAK
									
									CASE 12
										dia[thisI].bCompleted = TRUE
									BREAK																	
								ENDSWITCH
							ELSE
								dia[thisI].flag=0
							ENDIF
						BREAK
					ENDSWITCH
				break
				
				
				
				case dia_attacking_trevor_inside
					SWITCH dia[thisI].flag
						CASE 0
							IF GET_GAME_TIMER() > dia[thisI].intA
								IF FIND_CLOSEST_PED_OF_ROLE(dia[thisI].intB,role_guard_house,true)
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(zped[dia[thisI].intB].id)
								SWITCH dia[thisI].intC
									CASE 0
									FALLTHRU
									CASE 2
									FALLTHRU
									CASE 5
									FALLTHRU									
									CASE 9
										IF CREATE_CONVERSATION_EXTRA("GOONIN",3,zped[dia[thisI].intB].id,"chin2goon1")
											dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000,10000)
											dia[thisI].intC++
											dia[thisI].flag=0
										ENDIF
									BREAK
									
									CASE 1
									FALLTHRU
									CASE 6
									FALLTHRU
									CASE 8
									FALLTHRU
									CASE 11
										IF CREATE_CONVERSATION_EXTRA("GOON2ATTI",4,zped[dia[thisI].intB].id,"chin2goon2")
											dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000,10000)
											dia[thisI].intC++
											dia[thisI].flag=0
										ENDIF
									BREAK
									
									CASE 3
									FALLTHRU
									CASE 4
									FALLTHRU
									CASE 7
									FALLTHRU
									CASE 10
										IF CREATE_CONVERSATION_EXTRA("ATTACKIN",3,zped[dia[thisI].intB].id,"ONEILCOOK")
											dia[thisI].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000,10000)
											dia[thisI].intC++
											dia[thisI].flag=0
										ENDIF
									BREAK
									
									CASE 12
										dia[thisI].bCompleted = TRUE
									BREAK																	
								ENDSWITCH
							ELSE
								dia[thisI].flag=0
							ENDIF
						BREAK
					ENDSWITCH
				break
				case dia_trevor_seen_inside_house
					SWITCH dia[thisI].flag
						CASE 0
							int ij
							repeat count_of(zped) ij
								IF zped[ij].role = role_guard_house
									IF NOT IS_PED_INJURED(zped[ij].id)
										IF IS_PED_IN_COMBAT(zped[ij].id)
											IF CAN_PED_IN_COMBAT_SEE_TARGET(zped[ij].id,player_ped_id())
												dia[thisI].intA = ij
	//							IF FIND_CLOSEST_PED_OF_ROLE(dia[thisI].intA,role_guard_house,true)								
	//								
												dia[thisI].flag++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						BREAK
						CASE 1
							IF CREATE_CONVERSATION_EXTRA("G2TREVIN",4,zped[dia[thisI].intA].id,"chin2goon2")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				break
				case DIA_RESPOND_TO_BULLET
					SWITCH dia[thisI].flag
						CASE 0
							IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id) //intA set elsewhere in script using SET_DIA_INT()
								if CREATE_CONVERSATION_EXTRA("ONEILGUARD3",5,zped[dia[thisI].intA].id,"ONEILGUARD1")								
									dia[thisI].flag++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(zped[dia[thisI].intA].id) //intA set elsewhere in script using SET_DIA_INT()
								if CREATE_CONVERSATION_EXTRA("ONEILGUARD1",5,zped[dia[thisI].intA].id,"ONEILGUARD1")
									dia[thisI].bCompleted = TRUE
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				break
				
				case dia_last_man_standing
					SWITCH dia[thisI].flag
						CASE 0
							IF FIND_CLOSEST_PED_OF_ROLE(dia[thisI].intA,role_guard_outside,true)
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
						
							IF CREATE_CONVERSATION_EXTRA("ONEILGUARD7",5,zped[dia[thisI].intA].id,"oneilguard2")
								dia[thisI].flag++
							ENDIF
						BREAK
						case 2
							IF CREATE_CONVERSATION_EXTRA("CHI2_app",1,PLAYER_PED_ID(),"Trevor")
								dia[thisI].bCompleted = TRUE
							ENDIF
						break
					ENDSWITCH
				break
				
				case dia_trevor_approaches_house
					IF CREATE_CONVERSATION_EXTRA("CHI2_app",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUE
					ENDIF
				break
				
				case Dia_TREVOR_SAYS_HARD_WAY
					SWITCH dia[thisI].flag
						CASE 0
							IF IS_CONV_ROOT_PLAYING("ONEILGUARD4")
							OR IS_CONV_ROOT_PLAYING("SEETREVOR")
								dia[thisI].flag++
							ENDIF
						BREAK
						CASE 1
							IF CREATE_CONVERSATION_EXTRA("CHI2_HARD",1,player_ped_id(),"Trevor")
								dia[thisI].bCompleted = TRUE
							ENDIF
						BREAK
					ENDSWITCH
				break
				
				case Dia_stay_in_house
					SWITCH dia[thisI].flag
						CASE 0
							IF GET_GAME_TIMER() > dia[thisI].intB
								int iR
								float fClose
								fclose = 999
								float fRange							
								dia[thisI].intA = -1
								REPEAT COUNT_OF(zPed) iR
									IF zPed[iR].role = role_guard_house	
										IF NOT IS_PED_INJURED(zPed[iR].id)
											fRange = GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zPed[iR].id)
											If fRange < fClose
												fClose = fRange
												dia[thisI].intA = iR
											endif
										endif
									endif
								ENDREPEAT
								
								IF dia[thisI].intA = -1
									dia[thisI].bCompleted = TRUE
								ELSE
									dia[thisI].flag++
								endif
							endif
						BREAK
						CASE 1
							IF NOT IS_PED_INJURED(zPed[dia[thisI].intA].id)
								IF CREATE_CONVERSATION_EXTRA("CHI2_outs",5,zPed[dia[thisI].intA].id,"Oneilguard1")
									dia[thisI].bCompleted = TRUE
								ENDIF
							ELSE
								dia[thisI].intB = GET_GAME_TIMER() + 6000
								dia[thisI].flag=0
							ENDIF
						BREAK
					ENDSWITCH
				break
				
				CASE dia_shooting_Barrels
					SWITCH dia[thisI].flag
						CASE 0
							int iR
							REPEAT COUNT_OF(zPed) iR
								IF zPed[iR].placement = pos_shooting_bottle_1	
									dia[thisI].intA = iR
								ENDIF
								IF zPed[iR].placement = pos_shooting_bottle_2	
									dia[thisI].intB = iR
								ENDIF
							ENDREPEAT
							dia[thisI].flag++
						BREAK
						CASE 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								If IS_SYNCHRONIZED_SCENE_RUNNING(zPED[dia[thisI].intA].intA)
									IF GET_SYNCHRONIZED_SCENE_PHASE(zPED[dia[thisI].intA].intA) > 0.15
										IF GET_SYNCHRONIZED_SCENE_PHASE(zPED[dia[thisI].intA].intA) < 0.65										
											IF GET_DISTANCE_BETWEEN_ENTITIES(zPED[dia[thisI].intA].id,player_ped_id()) < 30
												dia[thisI].flag=3
											ENDIF											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF NOT IS_PED_INJURED(zPED[dia[thisI].intA].id)							
							//	IF CREATE_CONVERSATION_EXTRA("CHI2_gncht",6,zPED[dia[thisI].intA].id,"ONEILGUARD1")
							//		dia[thisI].flag++
							//	ENDIF
								dia[thisI].flag++
							ELSE
								dia[thisI].flag = 1
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_PED_INJURED(zPED[dia[thisI].intB].id)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zPED[dia[thisI].intB].id, "generic_cheer", "A_M_M_Hillbilly_01_White_mini_03", "SPEECH_PARAMS_FORCE_SHOUTED")
								//IF CREATE_CONVERSATION_EXTRA("CHI2_gnchtb",7,zPED[dia[thisI].intB].id,"ONEILGUARD2")
									dia[thisI].flag++
								//ENDIF
							ELSE
								dia[thisI].flag = 1
							ENDIF
						BREAK
						CASE 4
							IF NOT IS_PED_INJURED(zPED[dia[thisI].intA].id)
								If IS_SYNCHRONIZED_SCENE_RUNNING(zPED[dia[thisI].intA].intA)
									IF GET_SYNCHRONIZED_SCENE_PHASE(zPED[dia[thisI].intA].intA) > 0.75
										dia[thisI].flag=1
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
								
				BREAK
				
				case DIA_PICKED_UP_JERRY_CAN
					IF CREATE_CONVERSATION_EXTRA("CHI2_find",1,player_ped_id(),"TREVOR")
						
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						dia[thisI].bCompleted = TRUE
					ENDIF
				break
				
				CASE DIA_MISSED_A_BIT
					IF HAVE_TRAIL_BLIPS_BEEN_MISSED()
						IF CREATE_CONVERSATION_EXTRA("CHI2_pourmis",1,player_ped_id(),"TREVOR")
							dia[thisI].bCompleted = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE DIA_BOOM
					IF CREATE_CONVERSATION_EXTRA("CHI2_boom",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUE
					ENDIF
				BREAK
				
				case DIA_POURING_PETROL
					
					
					switch dia[thisI].flag
						case 0
							dia[thisI].intA = 2500
							dia[thisI].intB = 0
							dia[thisI].flag = 1							
						break
						
						case 1		
							dia[thisI].intA -= floor(TIMESTEP()*1000)
							IF dia[thisI].intA	 <= 0	
								dia[thisI].flag = 2							
							ENDIF							
						break
						case 2
							If dia[thisI].intB < 14
								IF CREATE_CONVERSATION_EXTRA("CHI2_pour",1,player_ped_id(),"TREVOR")
									dia[thisI].flag=1
									dia[thisI].intA = 12000 + GET_RANDOM_INT_IN_RANGE(3000,5000)
									dia[thisI].intB++
								ENDIF
							ENDIF
						break

					endswitch
				break
				
				case DIA_LIGHT_FUEL
					IF CREATE_CONVERSATION_EXTRA("CHI2_light",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUE
					ENDIF
					
				BREAK
				
				CASE DIA_EXPLODE_HOUSE
					IF CREATE_CONVERSATION_EXTRA("CHI2_boom",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUE
					ENDIF					
				BREAK
				
				CASE DIA_FUEL_BURN
					IF CREATE_CONVERSATION_EXTRA("CHI2_burn",1,player_ped_id(),"TREVOR")
						dia[thisI].bCompleted = TRUE
					ENDIF					
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF		
ENDPROC

PROC DIALOGUE_ON_DIALOGUE(int thisI, enumDialogue thisDia, int diaEntry, enumDialogue checkDia, andorEnum andOr1=cIGNORE, enumconditions cond1 = COND_NULL, andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL, andorEnum andOr3=cIGNORE, enumconditions cond3 = COND_NULL)
	IF NOT dia[thisI].bCompleted
		IF HAS_DIALOGUE_FINISHED(diaEntry,checkDia)
			DIALOGUE(thisI,thisDia,andOr1,cond1,andOr2,cond2,andOr3,cond3)
		ENDIF
	ENDIF
ENDPROC

// ==================================================== MISSION END ==============================================

PROC RESET_CONDITIONS()
	int i
	REPEAT COUNT_OF(conditions) i
		conditions[i].condition = COND_NULL
		conditions[i].active = FALSE
		conditions[i].returns = FALSE
		conditions[i].wasTrue = FALSE
		conditions[i].flag = 0
		conditions[i].intA = 0
	ENDREPEAT
ENDPROC

PROC RESET_ACTIONS()
	int i
	REPEAT COUNT_OF(actions) i
		//tell action to play its cleanup flag
		
			IF actions[i].needsCleanup
				actions[i].active = TRUE
				actions[i].flag = CLEANUP
				ACTION(i,actions[i].action,PLAYOUT_ON_TRIGGER)
			ENDIF
			
			//reset action
			actions[i].action = ACT_NULL
			actions[i].active = FALSE
			actions[i].ongoing = FALSE
			actions[i].completed = FALSE
			actions[i].flag = 0
			actions[i].needsCleanup = FALSE
			actions[i].trackCondition = FALSE
			actions[i].intA = 0
			actions[i].intB = 0
			actions[i].floatA = 0	
		
	ENDREPEAT
ENDPROC

PROC RESET_DIALOGUE()
	int i
	REPEAT COUNT_OF(dia) i
		dia[i].dial = DIA_NULL
		dia[i].bCompleted = FALSE
		dia[i].flag = 0
		dia[i].bStarted = FALSE
		dia[i].intA = 0
		dia[i].intB = 0
		dia[i].intC = 0
		#if IS_DEBUG_BUILD
		dia[i].condTrue = FALSE
		#endif
	ENDREPEAT
ENDPROC



PROC RESET_INSTRUCTIONS()
	int i 
	REPEAT COUNT_OF(instr) i
		instr[i].ins = INS_NULL
		instr[i].bCompleted = FALSE
		instr[i].flag = 0
		instr[i].intA = 0
	ENDREPEAT
ENDPROC

proc delete_everything(enumClenup cleanupState)
	//cprintln(debug_Trevor3,"Delete everything")
	RESET_ACTIONS()
	RESET_CONDITIONS()
	RESET_INSTRUCTIONS()
	RESET_DIALOGUE()
	
	END_SRL()
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	
	bLastConvoWithoutSubtitles = FALSE
	gasInCan = 0
	iCamFlag=0
	stageflag=0
	iStagger=0
	int i
	bSneakyPass = FALSE
	bBlockShooterDialogue = FALSE
	bcutscenePLaying = FALSE
	WHILE IS_CUTSCENE_ACTIVE()	
	  IF IS_CUTSCENE_PLAYING()
	      STOP_CUTSCENE(TRUE)		 
	  ELSE
	      REMOVE_CUTSCENE ()
	   ENDIF
	   SAFEWAIT(0,FALSE)
	ENDWHILE
	 
	
	IF DOES_PICKUP_EXIST(health_pickup)
		REMOVE_PICKUP(health_pickup)
	ENDIF
	
	REPEAT COUNT_OF(piPickups) i
		IF DOES_PICKUP_EXIST(piPickups[i])
			REMOVE_PICKUP(piPickups[i])
		ENDIF
	ENDREPEAT
	 
	bNewMusicRequest=FALSE
	bRequiresPreload=FALSe
	bPreloadRequest=FALSe
	sMusicToPlay=""
	sPreloadMusic=""
	
	CLEANUP_GAS_TRAILS()
	
	alertBulletFlag = 0
	alertBuddyFlag = 0
	alertSeenFlag = 0
	
	SET_SPECIAL_ABILITY_MULTIPLIER(1.0)
	RESET_MISSION_STATS_ENTITY_WATCH()
   			
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DESTROY_ALL_CAMS()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	safewait(32,FALSE)
	////cprintln(debug_trevor3,"HERE_f")
	SET_TIME_SCALE(1.0)
	CLEAR_PRINTS()
	CLEAR_MISSION_LOCATE_STUFF(locatesData,true)
	KILL_ANY_CONVERSATION()
	KILL_PHONE_CONVERSATION()
	//cprintln(debug_Trevor3,"Delete everything got here")
	//reset alert messages
	

	REMOVE_SCENARIO_BLOCKING_AREAS()
	

	  SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	SET_ROADS_BACK_TO_ORIGINAL(<<2469.9854, 4961.0449, 44.8113>>-<<100,100,100>>,<<2469.9854, 4961.0449, 44.8113>>+<<100,100,100>>)
	
	IF cleanupState = CLEANUP_PASS
		CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_ret_fhglassfrm,true)
		CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGLASSFRMSML,true)
		CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGlassairfrm,false)
		
		//SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_DESTROYED, FALSE) //removing as this was being set during the mission final stage.
		
		// Kenneth R.
		// Removing these calls as we dont want the player to see it pop
		/*SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_DESTROYED)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_DESTROYED)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_DESTROYED)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_DESTROYED)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP4, BUILDINGSTATE_DESTROYED)*/
		g_bBuildingScenarioBlocksUpdateThisFrame = TRUE
		
		
	ENDIF
	
	SET_AMBIENT_ZONE_LIST_STATE("AZL_CHN2_METH_LAB_FARM_FIRE", FALSE, TRUE)
	
	IF cleanupState != CLEANUP_SKIP	
		SET_SCENARIO_GROUP_ENABLED("Chinese2_Lunch",TRUE)
		SET_SCENARIO_GROUP_ENABLED("CHINESE2_HILLBILLIES",false)
	ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)	
	IF iNavBlock != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlock)
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlock)	
		ENDIF
	ENDIF
	
	IF cleanupState = CLEANUP_FAIL
	OR cleanupState = CLEANUP_FAIL_DEATH	
		STORE_FAIL_WEAPON(player_ped_id(),2)
	ENDIF
	

/*
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sTriggerSceneAssets.object[0])
	//	IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[0])
			//cprintln(debug_Trevor3,"chinese 2: h set stall 0 visible")
			SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.object[0],TRUE)
			SET_ENTITY_COLLISION(g_sTriggerSceneAssets.object[0],TRUE)
			SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.object[0])
			int blah
			//cprintln(debug_Trevor3,"release stall. Owner = ",GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[0],blah))
			g_sTriggerSceneAssets.object[0] = null
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sTriggerSceneAssets.object[1])
	//	IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[1])
			//cprintln(debug_Trevor3,"chinese 2: i set stall 1 visible")
			SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.object[1],TRUE)
			SET_ENTITY_COLLISION(g_sTriggerSceneAssets.object[1],TRUE)
			SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.object[1])
			g_sTriggerSceneAssets.object[1] = null
		ENDIF
	ENDIF
	*/
	

	REPEAT COUNT_OF(zPED) i
		If i = ped_oldMan2
			////cprintln(debug_Trevor3,"cleanup old man 2")
		endif
		if DOES_ENTITY_EXIST(zPED[i].id)
			if not is_ped_injured(zPED[i].id)
				IF cleanupState = CLEANUP_SKIP
					CLEAR_PED_TASKS_IMMEDIATELY(zPED[i].id) //to help with cleanup of sync scenes.
				ENDIF

			endif
			IF cleanupState = CLEANUP_PASS
			OR cleanupState = CLEANUP_FAIL_DEATH
				If i = ped_oldMan2
					////cprintln(debug_Trevor3,"set old man 2 not needed")
				endif
				if not is_ped_injured(zPED[i].id)
					SET_PED_KEEP_TASK(zPED[i].id,TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(zPED[i].id)
				ENDIF
			ELSE
				If i = ped_oldMan2
					////cprintln(debug_Trevor3,"set old man 2 not deleted")
				endif
				DELETE_PED(zPED[i].id)
			ENDIf
		ENDIF
		zPED[i].action = action_none
		zPED[i].lastaction = action_none
		zPED[i].vpos = <<0,0,0>>
		zPED[i].vPosB = <<0,0,0>>
		zPED[i].placement = pos_none
		zPED[i].actionFlag = 0
		zPED[i].floatA = 0
		zPED[i].floatB = 0
		zPED[i].intA = 0
		zPED[i].intB = 0
		zPED[i].nearbyreactingPed = FALSE
		zPED[i].pedOfInterest = 0
		zPED[i].sniped = FALSE
		zPED[i].alerted = FALSE
		pedReactionState[i].State = STATE_SPAWN

		if does_entity_exist(zPED[i].obj)
			DELETE_OBJECT(zPED[i].obj)
		ENDIF
		
		REMOVE_RELATIONSHIP_GROUP(zPED[i].rel) 
	ENDREPEAT

	IF cleanupState != CLEANUP_SKIP
		TERMINATE_REACTIONS()
	ELSE
		RESET_REACT_ARRAY(pedReactionState,dChinese2,convBlock,12,<<2482.1965, 4975.3667, 44.7288>>,121,STATE_SPAWN)	
	ENDIF


	IF DOES_ENTITY_EXIST(objShotgun)
		DELETE_OBJECT(objShotgun)
	ENDIF
		
	IF cleanupState != CLEANUP_PASS
	AND cleanupState != CLEANUP_FAIL_DEATH
		CLEAR_AREA(<< 2438.4326, 4970.4155, 53.1778 >>,450.0,TRUE,FALSE)
		STOP_FIRE_IN_RANGE(<< 2442.6897, 4970.3477, 46.3300 >>,30.0)
	ENDIF
		
	REPEAT COUNT_OF(events) i
		if (events[i].active = TRUE and events[i].persists = FALSE) // OR cleanupState = CLEANUP_PASS)
		or events[i].persists = FALSE
			//NOTE: FIX FOR BUILD THIS VALUE IS NOT USED BY ANYTHING. 
			//string thiosEvent = GET_THIS_EVENT_STRING(i)			
			//active is set to false when cleanup runs
			events[i].flag = CLEANUP
			//events[i].active = FALSE		
			events[i].boolA = FALSE
			events[i].intA = 0
			events[i].active = false
		ENDIF
	ENDREPEAT

	////cprintln(debug_trevor3,"HERE_e")

	manage_events()

	////cprintln(debug_trevor3,"HERE_d")	
	
	IF DOES_BLIP_EXIST(missionblip)
		remove_blip(missionblip)
	endif
	
	//remove petrol trail blips
	
	
	
//	LAST_CONVERSATION = CONV_EMPTY
	//random_lines_played = 0

	
	int iC
	REPEAT COUNT_OF(oCans) iC
		IF DOES_ENTITY_EXIST(oCans[iC])
			DELETE_OBJECT(oCans[iC])
		ENDIF
	ENDREPEAT
	
	
		
	
	

	if DOES_ENTITY_EXIST(vehPlayer) 
		IF cleanupState != CLEANUP_PASS
		AND cleanupState != CLEANUP_FAIL_DEATH
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayer)
					IF IS_PED_INJURED(player_ped_id())
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayer)	
					ELSe
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayer) //should only get removed if the player hasn't passed the mission
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	if DOES_ENTITY_EXIST(BarrelVan) 
		IF cleanupState != CLEANUP_PASS
			IF IS_PED_INJURED(player_ped_id())
				SET_VEHICLE_AS_NO_LONGER_NEEDED(BarrelVan)	
			ELSe
				SET_VEHICLE_AS_NO_LONGER_NEEDED(BarrelVan) //should only get removed if the player hasn't passed the mission
			ENDIF
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(BarrelVan)			
		ENDIF
	ENDIF
	
	if DOES_ENTITY_EXIST(vehBlazer)
		IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehBlazer)				
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBlazer)
		ENDIF
	ENDIF
	
	IF DOES_PICKUP_EXIST(pickup_gas)
		//////cprintln(DEBUG_TREVOR3,"Remove blip for pickup B ",GET_GAME_TIMER())
		REMOVE_PICKUP(pickup_gas)
	ENDIF
	
	IF cleanupState = CLEANUP_PASS
		IF DOES_ENTITY_EXIST(obja)
			FREEZE_ENTITY_POSITION(objA,FALSE)
			SET_OBJECT_AS_NO_LONGER_NEEDED(obja)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objb)
			FREEZE_ENTITY_POSITION(objb,FALSE)
			SET_OBJECT_AS_NO_LONGER_NEEDED(objb)
		ENDIF
	ENDIF
	
	if DOES_ENTITY_EXIST(tempPed)  delete_ped(tempPed) endif
	
	//remove petrol trails and data
	////cprintln(debug_trevor3,"HERE_c")	

	REMOVE_DECALS_IN_RANGE(<< 2439.6868, 4971.5405, 45.9442 >>,150.0)

	
	
	
	IF cleanupState != CLEANUP_FAIL_DEATH
		IF DOES_ENTITY_EXIST(zped[ped_janet].id)
			DELETE_PED(zped[ped_janet].id)
		ENDIF
		IF DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
			DELETE_PED(zped[ped_oldMan1].id)
		ENDIF
		IF DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
			DELETE_PED(zped[ped_oldMan2].id)
		ENDIF
		
		IF DOES_ENTITY_EXIST(zped[ped_tao].id)
			DELETE_PED(zped[ped_tao].id)
		ENDIF
		
		IF DOES_ENTITY_EXIST(zped[ped_taos_translator].id)
			DELETE_PED(zped[ped_taos_translator].id)
		ENDIF
	ENDIF
	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Hillbilly_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Hillbilly_02)
							
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Fertilizer)	
	SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO)
	SET_MODEL_AS_NO_LONGER_NEEDED(ig_janet)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN1A)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN2)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_TAOCHENG)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_TAOSTRANSLATOR)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_JANET)
	//REMOVE_ANIM_DICT("gestures@male")
	REMOVE_ANIM_SET("move_m@gangster@var_e")
	REMOVE_ANIM_SET("move_m@gangster@var_f")
	REMOVE_ANIM_SET("move_m@gangster@generic")
	REMOVE_ANIM_DICT("misschinese2_barrelRoll")	
	REMOVE_ANIM_DICT("reaction@male_stand@big_variations@b")
	REMOVE_ANIM_DICT("reaction@male_stand@big_intro@left")
	REMOVE_ANIM_DICT("reaction@male_stand@big_intro@right")
	REMOVE_ANIM_DICT("reaction@male_stand@big_intro@backward")

	REMOVE_ANIM_DICT("misschinese2_bank1")	


	REMOVE_ANIM_DICT("misschinese2_bank5")
	REMOVE_ANIM_DICT("misschinese2_crystalmazemcs1_ig")
	
	IF cleanupState != CLEANUP_SKIP
	
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PETROLCAN)
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),WEAPONTYPE_PETROLCAN)
		ENDIF
	ENDIF
	


	safeSkipTime=0
	iNextPedVariation = 0
	
	//clear all audio scenes
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_DRIVE_TO_FARMHOUSE")
		STOP_AUDIO_SCENE("CHI_2_DRIVE_TO_FARMHOUSE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_DRIVE_ROCK_RADIO")
		STOP_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_FARMHOUSE_OVERVIEW")
		STOP_AUDIO_SCENE("CHI_2_FARMHOUSE_OVERVIEW")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_SHOOTOUT_STEALTH")
		STOP_AUDIO_SCENE("CHI_2_SHOOTOUT_STEALTH")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_SHOOTOUT_ENEMIES_ALERTED")
		STOP_AUDIO_SCENE("CHI_2_SHOOTOUT_ENEMIES_ALERTED")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_POUR_GAS")
		STOP_AUDIO_SCENE("CHI_2_POUR_GAS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_SHOOT_GAS")
		STOP_AUDIO_SCENE("CHI_2_SHOOT_GAS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_GAS_TRAIL_FIRE")
		STOP_AUDIO_SCENE("CHI_2_GAS_TRAIL_FIRE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("CHI_2_RAYFIRE")
		STOP_AUDIO_SCENE("CHI_2_RAYFIRE")
	ENDIf
	////cprintln(debug_trevor3,"HERE_A")
	STOP_STREAM()
	
	IF cleanupState != CLEANUP_PASS
	//AND cleanupState != CLEANUP_FAIL_DEATH
		PLAYMUSIC("CHN2_STOP_TRACK",false,NULL_STRING())
		WHILE NOT control_music()
			SAFEWAIT(0,FALSE)
		endwhile
	ELSE
	//	PLAYMUSIC("CHN2_MISSION_FAIL",false,NULL_STRING())
	//	WHILE NOT control_music()
	//		SAFEWAIT(1)
	//	ENDWHILE
	ENDIF
	
	IF cleanupState != CLEANUP_SKIP
		DISABLE_TAXI_HAILING(FALSE)
	ENDIF
	
	IF cleanupState != CLEANUP_PASS
		rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
	    IF bRayfireTriggered
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)
		    	SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_STARTING)
			ENDIF
			bRayfireTriggered = FALSE
		ENDIF 
	ENDIF

ENDPROC

PROC MISSION_CLEANUP(enumClenup cleanupState)	
	delete_everything(cleanupState)
	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT,true)
	DISABLE_CELLPHONE(FALSE)
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()

	//SET_VEHICLE_MODEL_IS_SUPPRESSED(bodhi2,FALSE)		
	
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(navblockA)
		REMOVE_NAVMESH_BLOCKING_OBJECT(navblockA)
	ENDIF
	
	IF NOT IS_PED_INJURED(player_ped_id())
		REMOVE_WEAPON_FROM_PED(player_ped_id(),WEAPONTYPE_PETROLCAN)
		CLEAR_PED_TASKS(player_ped_id())
	endif

	//Move player outside house
	IF NOT IS_PED_INJURED(player_ped_id())
		IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) != NULL
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2444.0984, 4977.7949, 48.9959>>) < 100
				SET_ENTITY_COORDS(player_ped_id(),<<2463.8108, 4961.7471, 44.1760>>)
				SET_ENTITY_HEADING(player_ped_id(),20)
			ENDIF
		ENDIF
	ENDIF
	
	//Reset building IPLs to default
	IF cleanupState = CLEANUP_FAIL OR cleanupState = CLEANUP_FAIL_DEATH
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_NORMAL)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_OCCLUSION, BUILDINGSTATE_NORMAL)
	ENDIF
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC Mission_Failed(string sFail = null)

	WHILE NOT TRIGGER_MUSIC_EVENT("CHN2_MISSION_FAIL")
		safewait(21)
	ENDWHILE

	IF NOT IS_STRING_NULL_OR_EMPTY(sFail)
		sFailReason = sFail
	ENDIF

	If IS_STRING_NULL(sFailReason)
		sFailReason = "CHFAIL"
	endif
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		SAFEWAIT(2,FALSE)
	ENDWHILE
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here

	Mission_Cleanup(CLEANUP_FAIL) // must only take 1 frame and terminate the thread
ENDPROC


PROC Mission_Passed()	
	CLEAR_PRINTS()	
		
	Mission_Flow_Mission_Passed()	

	Mission_Cleanup(CLEANUP_PASS)
ENDPROC

PROC FAIL(enumFails thisFailToCheck,andorEnum andOr1=cFORCEtrue, enumconditions cond1 = COND_NULL,andorEnum andOr2=cIGNORE, enumconditions cond2 = COND_NULL)



	IF checkANDOR(andOr1,cond1,andOr2,cond2)
		SWITCH thisFailToCheck
			CASE FAIL_OUT_OF_GAS						
				Mission_Failed("CHFAIL1")
			BREAK
			CASE FAIL_GAS_TRAIL_IGNITED_EARLY
				Mission_Failed("FRMSOON")
			BREAK
			CASE FAIL_GAS_TRAIL_NOT_COMPLETE
				Mission_Failed("FRMTRAI")
			BREAK
			CASE fail_kill_cheng_or_translator
				IF DOES_ENTITY_EXIST(zped[ped_tao].id)
					IF IS_PED_INJURED(zped[ped_tao].id)
						Mission_Failed("FRMCHI")
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(zped[ped_taos_translator].id)
					IF IS_PED_INJURED(zped[ped_taos_translator].id)
						Mission_Failed("FRMCHI2")
					ENDIF
				ENDIF
			BREAK
			CASE FAIL_ABANDONED_FARM
				Mission_Failed("CHFAIL2")
			BREAK
			CASE FAIL_JERRY_CAN_DESTROYED
				 Mission_Failed("FRMGASF")				
			BREAK
		ENDSWITCH
	ENDIF		
ENDPROC

PROC CONTROL_BURN_PLAYER()
	//front of house
	vector vPlayer = GET_ENTITY_COORDS(player_ped_id())
	IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2432.475586,4953.241699,43.055450>>, <<2466.340088,4986.149902,57.253029>>, 14.437500)
		IF vPlayer.z < 50
			IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2436.761719,4956.010742,44.490093>>, <<2443.379639,4962.624512,48.223301>>, 1.500000)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2447.669434,4966.553711,44.951408>>, <<2452.496582,4969.824707,48.890739>>, 2.312500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2454.863037,4968.685547,44.951408>>, <<2460.504883,4974.408691,48.826408>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2459.058350,4977.545898,44.951408>>, <<2460.410645,4979.302246,48.826408>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2457.602051,4984.746094,44.736694>>, <<2461.976563,4989.490234,48.478436>>, 1.687500)
				IF NOT IS_ENTITY_ON_FIRE(player_ped_id())
					START_ENTITY_FIRE(player_ped_id())
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2454.179199,4995.281250,44.541508>>, <<2453.095947,4993.905273,48.320435>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2449.401123,4990.354004,44.929241>>, <<2447.717041,4988.864746,48.821293>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2443.906250,4990.104004,44.444595>>, <<2439.006592,4985.271484,48.304581>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2439.601563,4982.387207,44.951412>>, <<2440.796875,4981.365234,48.826416>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2440.258789,4980.671387,44.951416>>, <<2434.768799,4975.055176,48.826416>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2432.164551,4972.585938,44.299091>>, <<2430.113770,4970.545410,48.210857>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2428.460205,4968.569336,44.815968>>, <<2426.632324,4966.602539,48.293419>>, 1.687500)
				IF NOT IS_ENTITY_ON_FIRE(player_ped_id())
					START_ENTITY_FIRE(player_ped_id())
				ENDIF
			ENDIF
		ENDIF
	ELIF 	IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2455.486816,4997.566406,43.110104>>, <<2423.653320,4962.837402,57.348885>>, 14.437500)
	//back of house
		IF vPlayer.z < 50
			IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2442.913086,4965.273438,50.156872>>, <<2444.489502,4967.409668,53.817825>>, 1.687500)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2445.238525,4969.070313,49.942844>>, <<2450.365234,4972.188477,54.032505>>, 2.375000)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2452.331543,4971.817383,49.942825>>, <<2457.977295,4977.582520,53.880974>>, 2.375000)
				IF NOT IS_ENTITY_ON_FIRE(player_ped_id())
					START_ENTITY_FIRE(player_ped_id())
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2457.620605,4978.462402,49.942825>>, <<2453.931885,4982.161133,53.817825>>, 2.375000)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2452.054199,4984.156250,49.942825>>, <<2448.119873,4987.808594,53.813160>>, 2.375000)
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2442.013916,4980.538574,49.942825>>, <<2435.795654,4971.944824,53.903877>>, 2.375000)	
				IF NOT IS_ENTITY_ON_FIRE(player_ped_id())
					START_ENTITY_FIRE(player_ped_id())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


int iStoredLastReplay
PROC SET_REPLAY_STAGE()
	switch iStoredLastReplay
		case 0
			IF missionstage >= snipe_from_hill
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Snipe from hill")
				cprintln(DEBUG_TREVOR3,"Replay stage : ",1)
				iStoredLastReplay++
			ENDIF
		BREAK
		case 1
			IF missionstage >= snipe_from_hill and missionstage < do_petrol_trail
				//IF IS_CONDITION_TRUE(COND_ALL_ENEMY_ALERTED) //took this out as the replay setup would mean this is not true
				IF IS_CONDITION_TRUE(COND_PLAYER_IN_HOUSE)
				OR missionstage = inside_house
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Inside House")				
					cprintln(DEBUG_TREVOR3,"Replay stagea : ",2)
					iStoredLastReplay++
				ENDIF
			else
				IF missionstage = do_petrol_trail
					IF HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Inside House")				
						cprintln(DEBUG_TREVOR3,"Replay stageb : ",2)
						iStoredLastReplay++ //stage must be at petrol trail
					ENDIF
				elif missionstage > do_petrol_trail
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Inside House")				
					cprintln(DEBUG_TREVOR3,"Replay stagec : ",2)
					iStoredLastReplay++
				endif
			endif
		BREAK
		CASE 2
			IF missionstage >= do_petrol_trail
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Pour petrol trail")
				cprintln(DEBUG_TREVOR3,"Replay stage : ",3)
				iStoredLastReplay++
			ENDIF
		BREAK	
		CASE 3
			IF missionstage = end_mission
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Leave the farm house",TRUE)
				cprintln(DEBUG_TREVOR3,"Replay stage : ",4)
				iStoredLastReplay++
			ENDIF
		BREAK	
	ENDSWITCH	
ENDPROC

// ==================================================== DEBUGGING =============================================

PROC PREP_REPLAY(bool firstStage=false, bool letReplayPutPlayerInVehicle = FALSE, bool isRepeatPlay=FALSE)
	IF firstStage
		IF IS_REPLAY_START_VEHICLE_AVAILABLE()	
			IF IS_REPLAY_VEHICLE_MODEL_UNDER_SIZE_LIMIT(GET_REPLAY_START_VEHICLE_MODEL(),<<10,20,9>>)
				REQUEST_REPLAY_START_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_START_VEHICLE_LOADED()
					safewait(54455)
				ENDWHILE
				
				IF isRepeatPlay
					vehPlayer = CREATE_REPLAY_START_VEHICLE(<<1994.3872, 3061.9397, 46.0491>>,50)
				ELSE
					vehPlayer = CREATE_REPLAY_START_VEHICLE(<<0,0,0>>,0)
				ENDIF
	
				
			ENDIF
								
		ENDIF					
	ELSE
		//cprintln(debug_Trevor3,"IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()")
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()			
			//cprintln(debug_Trevor3,"YES")
			REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
				safewait(544)
			ENDWHILE

			vehPlayer = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<0,0,0>>,0)
		
		ENDIF
	ENDIF
	
	IF letReplayPutPlayerInVehicle
		IF WAS_PLAYER_IN_VEHICLE_AT_CHECKPOINT()
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				SET_PED_INTO_VEHICLE(player_ped_id(),vehPlayer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

proc do_skip(mission_stage_flag skipToStage, bool bIsReplay=false, bool bIsRepeatPlay=FALSE)
	//////cprintln(DEBUG_TREVOR3,"Start cleanup")
	delete_everything(CLEANUP_SKIP)
	add_event(events_reset_house_rayfire)
	
	#if IS_DEBUG_BUILD
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 50, FALSE,FALSE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 30, FALSE,FALSE)
		GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
		GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02) 
	#endif
	stageFlag = 0
	missionstage = skipToStage
	//////cprintln(DEBUG_TREVOR3,"Skip to stage : ",missionstage)

	

	switch skipToStage
		case init_mission
		BREAK
		case intro_cut
			request_model(IG_OLD_MAN2)
			request_model(IG_OLD_MAN1A)
			request_model(IG_JANET)
			request_model(IG_TAOCHENG)
			request_model(IG_TAOSTRANSLATOR)					
			REQUEST_CUTSCENE("chinese_2_int")
			
			WHILE NOT HAS_MODEL_LOADED(IG_OLD_MAN2)
			OR NOT HAS_MODEL_LOADED(IG_OLD_MAN1A)
			OR NOT HAS_MODEL_LOADED(IG_JANET)
			OR NOT HAS_MODEL_LOADED(IG_TAOCHENG)
			OR NOT HAS_MODEL_LOADED(IG_TAOSTRANSLATOR)
			OR NOT HAS_CUTSCENE_LOADED()
				safewait(323)
			endwhile
			
			WHILE NOT CREATE_NPC_PED_ON_FOOT(zped[ped_tao].id,CHAR_CHENG,<<1985.6656, 3052.6606, 46.2151>>)
				wait(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<<1992.8784, 3057.8464, 46.0568>>)
			SET_ENTITY_HEADING(player_ped_id(),311)	
			
			SET_PED_COMPONENT_VARIATION(zped[ped_tao].id, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
		break
		case drive_to_farm
			IF bIsReplay
				START_REPLAY_SETUP(<<1992.8784, 3057.8464, 46.0568>>,311.0)
			ELSE
				IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
					SET_ENTITY_COORDS(vehPlayer,<<1994.3872, 3061.9397, 46.0491>>)
					SET_ENTITY_HEADING(vehPlayer,50.7724)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				LOAD_SCENE(<<1992.8784, 3057.8464, 46.0568>>)				
			ENDIF
			//add_event(events_animate_chinese)
			
		//	set_event_flag(events_animate_chinese,150)
		//	////cprintln(debug_trevor3,"spawn replay vehicle")
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 1997.1208, 3063.6338, 46.0493 >>, 57.9218,TRUE,FALSE,TRUE,TRUE,FALSE)					
			ADD_SCENARIO_BLOCKING_AREA(<<1991.219604,3054.884277,50.277412>>-<<40,40,40.000000>>,<<1991.219604,3054.884277,50.277412>>+<<40,40,40.000000>>)
			SET_PED_NON_CREATION_AREA(<<1991.219604,3054.884277,50.277412>>-<<40,40,40>>,<<1991.219604,3054.884277,50.277412>>+<<40,40,0.000000>>)
				
			
			CLEAR_ArEA(<<1984.9971, 3052.9048, 46.8556>>,10,TRUE)//to ensure peds at bar in scenario are removed
			PREP_REPLAY(true,false,bIsRepeatPlay)
			REQUEST_MODEL(IG_TAOCHENG)
			REQUEST_MODEL(IG_TAOSTRANSLATOR)
			REQUEST_MODEL(IG_OLD_MAN1A)
			REQUEST_MODEL(IG_OLD_MAN2)
			REQUEST_MODEL(IG_JANET)
			
			REQUEST_ANIM_DICT("misschinese2_crystalmaze")
			
			WHILE NOT HAS_MODEL_LOADED(IG_TAOCHENG)
			OR NOT HAS_MODEL_LOADED(IG_TAOSTRANSLATOR)
			OR NOT HAS_MODEL_LOADED(IG_OLD_MAN1A)
			OR NOT HAS_MODEL_LOADED(IG_OLD_MAN2)
			OR NOT HAS_MODEL_LOADED(IG_JANET)
			OR NOT HAS_ANIM_DICT_LOADED("misschinese2_crystalmaze")
				safewait(3)
			ENDWHILE
			
			IF NOT bIsReplay
				IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
					SET_PED_INTO_VEHICLE(player_ped_id(),vehPlayer)
					SET_ENTITY_COORDS(vehPlayer,<<1994.3872, 3061.9397, 46.0491>>)
					SET_ENTITY_HEADING(vehPlayer,50.7724)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<1992.8784, 3057.8464, 46.0568>>)
					SET_ENTITY_HEADING(player_ped_id(),311)				
				ENDIF
			ENDIF
			
			
			safewait(85)
			action(2,ACT_ANIMATE_CHINESE)
			WHILE GET_ACTION_FLAG(2,ACT_ANIMATE_CHINESE) != TAO_EVENT_ANIM_PLAYING
				action(2,ACT_ANIMATE_CHINESE)
				safeWAIT(4)
			ENDWHILE
			
		
			
			
			add_event(events_animate_bar)
		//	while GET_EVENT_FLAG(events_animate_bar) < 4
		//		manage_events()
		//		safewait(5)
		//	endwhile
			
			IF bIsReplay
				IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
					END_REPLAY_SETUP(vehPlayer)
				ELSE
					END_REPLAY_SETUP()
				ENDIF
			ENDIF
			
		break
		case farm_cutscene
			IF bIsReplay
				
			
				START_REPLAY_SETUP(<< 2574.0486, 4981.8237, 50.4400 >>,50.0)
			ELSE
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2570.5430, 4982.0020, 50.6795 >>,false,true)			//<< 2572.0095, 4981.6782, 50.6698 >>
				SET_ENTITY_HEADING(PLAYER_PED_ID(),98.21)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				LOAD_SCENE(<< 2458.5076, 4984.5039, 52.3461 >>)
				safewait(323)
			ENDIF
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 2602.4050, 4950.2280, 39.0651 >>, 274.5783,FALSE,FALSE,TRUE,TRUE,FALSE)	
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			
		//	SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			PREP_REPLAY()
			PREFETCH_SRL("chinese2_farmhouse_cutscene") 
			
			SAFEWAIT(34)
		
			add_event(events_spawn_house)
			while is_event_active(events_spawn_house)			
				manage_events()
				safewait(5)
			endwhile
			WHILE NOT CAN_PLAYER_START_CUTSCENE()
				safewait(6)
			endwhile
			PLAYMUSIC("CHN2_MISSION_START",true,"CHN2_EXPLODE")	
			WHILE NOT control_music()
				safewait(7)
			ENDWHILE
			WHILE NOT IS_SRL_LOADED()
				safewait(878)
			endwhile
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
			
			ACTION(10,ACT_LOAD_SOUNDSET)
			WHILE NOT IS_ACTION_COMPLETE(10,ACT_LOAD_SOUNDSET)
				ACTION(10,ACT_LOAD_SOUNDSET)
				SAFEWAIT(234)
			ENDWHILE
		BREAK
		
		case snipe_from_hill
			IF bIsReplay								
				START_REPLAY_SETUP(GET_REPLAY_CHECKPOINT_PLAYER_POSITION(),GET_REPLAY_CHECKPOINT_PLAYER_HEADING())
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2570.5430, 4982.0020, 50.6795 >>,false,true)			//<< 2572.0095, 4981.6782, 50.6698 >>
				SET_ENTITY_HEADING(PLAYER_PED_ID(),98.21)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				LOAD_SCENE(<< 2570.5430, 4982.0020, 50.6795 >>)
				safewait(323)
			ENDIF
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<0,0,0>>, 0,FALSE,FALSE,TRUE,TRUE,FALSE)	
		
			//TASK_TOGGLE_DUCK(player_ped_id(),TOGGLE_DUCK_ON)
			//SET_PED_DUCKING(player_ped_id(),TRUE)
			
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			
			PREP_REPLAY(false,true)			
						
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			
			add_event(events_spawn_house)
			while is_event_active(events_spawn_house)			
				manage_events()
				safewait(8)
			endwhile
	
	
	
			weapon_type aWep
			
			aWep = GET_FAIL_WEAPON(2)
			IF aWep != WEAPONTYPE_INVALID
			AND aWep != WEAPONTYPE_UNARMED
				IF HAS_PED_GOT_WEAPON(player_ped_Id(),aWep)
					SET_CURRENT_PED_WEAPON(player_ped_id(),aWep,TRUE)
				ENDIF
			ENDIF
		
			
			 
			PLAYMUSIC("CHN2_MISSION_START",true,"CHN2_EXPLODE")	
			WHILE NOT control_music()
				safewait(9)
			ENDWHILE
						
			IF bIsReplay
				
				IF DOES_ENTITY_EXIST(vehplayer) 
				AND IS_VEHICLE_DRIVEABLE(vehplayer)
				AND WAS_PLAYER_IN_VEHICLE_AT_CHECKPOINT()
					END_REPLAY_SETUP(vehplayer)
					IF IS_VEHICLE_DRIVEABLE(vehplayer)
						SET_VEHICLE_ENGINE_ON(vehplayer,true,true)
						IF IS_THIS_MODEL_A_PLANE(get_entity_model(vehplayer))
							SET_VEHICLE_FORWARD_SPEED(vehplayer,15)
						ENDIF
						IF IS_THIS_MODEL_A_HELI(get_entity_model(vehplayer))			
							SET_HELI_BLADES_FULL_SPEED(vehplayer)
							SET_ENTITY_VELOCITY(vehplayer,<<1,1,1>>)
						ENDIF
					ENDIF
				ELSE
					END_REPLAY_SETUP()
				ENDIF
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(GET_REPLAY_CHECKPOINT_PLAYER_POSITION(),<<2570.5376, 4982.0313, 50.6239>>) < 10.0
				set_entity_coords(player_ped_id(),<<2570.5376, 4982.0313, 50.6239>>)
				set_entity_heading(player_ped_id(),98)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
				SET_ENTITY_COORDS_GROUNDED(player_ped_id(),GET_ENTITY_COORDS(player_ped_id()))
			ENDIF
			
					
		BREAK	
		
		case get_to_house
			
			
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<0,0,0>>, 0,FALSE,FALSE,TRUE,TRUE,FALSE)	
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2572.0095, 4981.6782, 50.6698 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),90.3911)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			add_event(events_spawn_house)
			DISABLE_TAXI_HAILING(TRUE)
			while is_event_active(events_spawn_house)			
				manage_events()
				safewait(10)
			endwhile

			PREP_REPLAY()
			
			
			PLAYMUSIC("CHN2_TO_HOUSE_RESTART",false,"CHN2_EXPLODE")	
			WHILE NOT control_music()
				safewait(11)
			ENDWHILE
			
			FORCE_DIALOGUE_STATE(0,dia_whatnow)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
			
		break
		
					
		
		case inside_house
			cprintln(debug_Trevor3,"Start replay: ",get_Game_timer())
			IF bIsReplay
				START_REPLAY_SETUP(<< 2450.8713, 4960.9985, 44.3759 >>,351.0)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2450.8713, 4960.9985, 44.3759 >>,false,true)			
				SET_ENTITY_HEADING(PLAYER_PED_ID(),351.3911)
				load_scene(<< 2432.9817, 4964.8232, 41.3476 >>)
				safewait(325)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
	
			ENDIF
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<0,0,0>>, 0,FALSE,FALSE,TRUE,TRUE,FALSE)	
			PREP_REPLAY()
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			//SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_PUMPSHOTGUN,TRUE)
			WAIT(1)
			add_event(events_spawn_house)
			DISABLE_TAXI_HAILING(TRUE)
			cprintln(debug_Trevor3,"wait for spawn house event: ",get_Game_timer())
			while is_event_Active(events_spawn_house)
				manage_events()
				safewait(12)
			ENDWHILE		
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			cprintln(debug_Trevor3,"completed spawn house event: ",get_Game_timer())
			
			 cprintln(debug_Trevor3,"Load music : ",get_Game_timer())
			PLAYMUSIC("CHN2_ENTER_HOUSE_RESTART",false,"CHN2_EXPLODE")	
			WHILE NOT control_music()
				safewait(13)
			ENDWHILE
			cprintln(debug_Trevor3,"Loaded music : ",get_Game_timer())
			FORCE_DIALOGUE_STATE(0,dia_whatnow)
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
				SET_ENTITY_COORDS_GROUNDED(player_ped_id(),GET_ENTITY_COORDS(player_ped_id()))
			ENDIF
			cprintln(debug_Trevor3,"Loaded replay : ",get_Game_timer())
		break
		
		case do_petrol_trail
			IF bIsReplay
				START_REPLAY_SETUP(<<2435.3193, 4966.1528, 41.3476>>, 104.2964)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<<2435.3193, 4966.1528, 41.3476>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 104.2964)
				load_scene(<< 2432.9817, 4964.8232, 41.3476 >>)
				WAIT(1)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
	
			ENDIF
			
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<0,0,0>>, 0,FALSE,FALSE,TRUE,TRUE,FALSE)	
			DISABLE_TAXI_HAILING(TRUE)
			PREP_REPLAY()
		
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
			
			
			int_farm = GET_INTERIOR_AT_COORDS(<< 2432.9817, 4964.8232, 41.3476 >>)
			
			while not IS_VALID_INTERIOR(int_farm)
				int_farm = GET_INTERIOR_AT_COORDS(<< 2432.9817, 4964.8232, 41.3476 >>)
				SAFEWAIT(14)
			ENDWHILE
			
		//	PIN_INTERIOR_IN_MEMORY(int_farm)
			WHILE NOT IS_INTERIOR_READY(int_farm)
				SAFEWAIT(15)
			ENDWHILE
			SET_INTERIOR_ACTIVE(int_farm, TRUE)	
			UNPIN_INTERIOR(int_farm)
			int_farm = null
		
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PETROLCAN)
			REQUEST_ANIM_DICT("misschinese2_crystalmaze")
			WHILE NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PETROLCAN)
			OR NOT HAS_ANIM_DICT_LOADED("misschinese2_crystalmaze")
				SAFEWAIT(16)
			ENDWHILE
			

			add_event(events_explode_petrol_can)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			
			add_event(events_spawn_house)
			while is_event_Active(events_spawn_house)
				manage_events()
				safewait(17)
			ENDWHILE		
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			
		
		
			request_model(blazer)
			while NOT HAS_MODEL_LOADED(blazer)
				safewait(18)
			ENDWHILE
			
			FORCE_INSTRUCTION_STATE(0,INS_PICK_UP_GAS_CAN)
		//	action(0,act_create_petrol_can_with_blip)
		//	reset_actions()
		
		//	IF NOT bIsReplay
		//		GIVE_WEAPON_TO_PED(player_ped_id(),WEAPONTYPE_PETROLCAN,4500,true)
		//	ELSE
				//IF HAS_PED_GOT_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN)
					//SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_PETROLCAN,TRUE)
				//ELSE
					GIVE_WEAPON_TO_PED(player_ped_id(),WEAPONTYPE_PETROLCAN,4500,true)
				//ENDIF
		//	ENDIF
		
			RESET_GAS_TRAIL(FALSE)
			 
			PLAYMUSIC("CHN2_PETROL_RESTART",false,"CHN2_EXPLODE")	
			WHILE NOT control_music()
				safewait(19)
			ENDWHILE
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		break
		
		case explosion_cut
			IF bIsReplay
				START_REPLAY_SETUP(<< 2453.124, 4952.071, 45.125 >>,285.0)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2453.124, 4952.071, 45.125 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),285.8625)
				load_scene(<< 2453.124, 4952.071, 45.125 >>)
				WAIT(1)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
	
			ENDIF
			PREP_REPLAY()
			
			IF NOT IS_SRL_LOADED()
				PREFETCH_SRL("chinese2_explosion_cutscene") 
			ENDIF
			
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<0,0,0>>, 0,FALSE,FALSE,TRUE,TRUE,FALSE)	
			
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)

			request_model(blazer)
			
			while not has_model_loaded(blazer)
				safewait(20)
			ENDWHILE
			
			vehBlazer = create_vehicle(Blazer,<<2478.5867, 4941.8032, 43.6736>>, 227.2903)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehBlazer)
			
			
			
			int_farm = GET_INTERIOR_AT_COORDS(<< 2432.9817, 4964.8232, 41.3476 >>)
	
			while not IS_VALID_INTERIOR(int_farm)
				int_farm = GET_INTERIOR_AT_COORDS(<< 2432.9817, 4964.8232, 41.3476 >>)
				safewait(21)
			ENDWHILE
			
			//PIN_INTERIOR_IN_MEMORY(int_farm)
			WHILE NOT IS_INTERIOR_READY(int_farm)
				safewait(22)
			ENDWHILE
			
			
			CUTSCENE_FIRST_SHOT = SHOT_NORMAL
			SET_INTERIOR_ACTIVE(int_farm, TRUE)	
			
			UNPIN_INTERIOR(int_farm)
			int_farm = null
			
			while NOT HAS_MODEL_LOADED(blazer)
			OR NOT IS_SRL_LOADED()
				safewait(24)
			ENDWHILE
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
			
			WHILE NOT LOAD_STREAM("CHINESE2_FARMHOUSE_EXPLOSION_SHOOT_GASOLINE_MASTER")
			
				safewait(23)
			ENDWHILE
		break
		
		case end_mission
			IF bIsReplay
				START_REPLAY_SETUP(<<2472.3, 4948.1807, 44.0937>>,220.0)
			ELSE
				SET_ENTITY_COORDS(player_ped_id(), <<2472.3, 4948.1807, 44.0937>>)
				SET_ENTITY_HEADING(player_ped_id(), 220.8569)
				load_scene(<< 2453.124, 4952.071, 45.125 >>)
				WAIT(1)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
	
			ENDIF
			kill_event(events_reset_house_rayfire,false)
			//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<0,0,0>>, 0,FALSE,FALSE,TRUE,TRUE,FALSE)	
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
			request_model(blazer)
			

			
			while not has_model_loaded(blazer)
			OR NOT REQUEST_SCRIPT_AUDIO_BANK("FARMHOUSE_FIRE")
			OR NOT REQUEST_SCRIPT_AUDIO_BANK("FARMHOUSE_FIRE_BG")
				safewait(25)
			ENDWHILE
			
			vehBlazer = create_vehicle(Blazer,<<2478.5867, 4941.8032, 43.6736>>, 227.2903)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehBlazer)
			PREP_REPLAY()
			load_scene(<< 2453.124, 4952.071, 45.125 >>)
			
			
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_OCCLUSION, BUILDINGSTATE_DESTROYED)
							
			rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
  			WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)    
				
				safewait(161)
			endwhile
			
			SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_ENDING)
								         
			REMOVE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_ret_fhglassfrm,false)
			REMOVE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGLASSFRMSML,false)
			REMOVE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGlassairfrm,false)
	
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
		CASE missionPassed
			kill_event(events_reset_house_rayfire,false)
			IF bIsReplay
				START_REPLAY_SETUP(<<2625.7759, 4802.2173, 32.5747>>,206.0)
			ELSE
				SET_ENTITY_COORDS(player_ped_id(),<<2625.7759, 4802.2173, 32.5747>>)
				SET_ENTITY_HEADING(player_ped_id(), 206.4254)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				LOAD_SCENE(<<2625.7759, 4802.2173, 32.5747>>)
			ENDIF
			
			/* this is causing mission to freeze
			rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
  			WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)    
				
				safewait(161)
			endwhile */
			
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
			SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_ENDING)
			
			
			
			/*
			WHILE GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire) != RFMO_STATE_END
				safewait(162)
			ENDWHILE
			*/
			
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE,BUILDINGSTATE_CLEANUP, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP1,BUILDINGSTATE_CLEANUP, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP2,BUILDINGSTATE_CLEANUP, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_CLNUP3,BUILDINGSTATE_CLEANUP, FALSE)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_INTERIOR, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FARM_HOUSE_OCCLUSION, BUILDINGSTATE_DESTROYED)
			IF bIsReplay
				END_REPLAY_SETUP()
			ENDIF
		BREAK
	ENDSWITCH
	
		
	
ENDPROC

#if IS_DEBUG_BUILD

proc debug_check_keyboard()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)			
		MISSION_PASSED()
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
		Mission_Failed()
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
		missionStage = INT_TO_ENUM(mission_stage_flag,enum_to_int(missionStage)+1)
		do_skip(missionStage)
	ENDIF
	IF NOT 	IS_KEYBOARD_KEY_PRESSED(KEY_J)
		INT iCurrentStage = ENUM_TO_INT(missionStage)			
		int iReturnStage	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage,iCurrentStage,TRUE) //MenuTitlePassed defaults to â€œMission Stagesâ€ if NULL is passed.
			missionStage = INT_TO_ENUM(mission_stage_flag, iReturnStage)
			do_skip(missionStage)
		ENDIF 
	ENDIF
ENDPROC



PROC PRINT_CONDITION_DATA(int iCon)
	string sCond
	enumConditions thisCond = conditions[iCon].condition
	SWITCH thisCond
		CASE COND_NULL sCond = "COND_NULL" BREAK	
	
	case cond_GO_TO_FARM_START scond = "cond_GO_TO_FARM_START" BREAK	
	case cond_INITIAL_INSTRUCTIONS_PLAYED scond = "cond_INITIAL_INSTRUCTIONS_PLAYED" BREAK
	case cond_PLAYER_IN_ANY_CAR scond = "cond_PLAYER_IN_ANY_CAR" BREAK
	case cond_GO_TO_FARM_END scond = "cond_GO_TO_FARM_END" BREAK
	
	case cond_START_FARM_ASSAULT scond = "cond_START_FARM_ASSAULT" BREAK
	case cond_BULLET_IMPACTED_BY_BARRELS scond = "cond_BULLET_IMPACTED_BY_BARRELS" BREAK
	case cond_SHOOTING_PED_DEAD scond = "cond_SHOOTING_PED_DEAD" BREAK
	case cond_ENEMY_PED_REACTING scond = "cond_ENEMY_PED_REACTING" BREAK
	case cond_ALL_ENEMY_ALERTED scond = "cond_ALL_ENEMY_ALERTED" BREAK	
	case cond_PLAYER_IN_METH_LAB scond = "cond_PLAYER_IN_METH_LAB" BREAK
	case cond_PLAYER_IN_HOUSE scond = "cond_PLAYER_IN_HOUSE" BREAK
	case cond_PLAYER_DOWNSTAIRS scond = "cond_PLAYER_DOWNSTAIRS" BREAK
	case cond_PLAYER_APPROACHING_HOUSE scond = "cond_PLAYER_APPROACHING_HOUSE" BREAK
	case cond_BASEMENT_PED_DEAD_IN_BASEMENT scond = "cond_BASEMENT_PED_DEAD_IN_BASEMENT" BREAK
	case cond_ATTACKERS_ALL_DEAD scond = "cond_ATTACKERS_ALL_DEAD" BREAK
	case cond_TREVOR_NEAR_BASEMENT scond = "cond_TREVOR_NEAR_BASEMENT" BREAK
	case cond_TREVOR_NEAR_STAIRS scond = "cond_TREVOR_NEAR_STAIRS" BREAK
	case cond_TREVOR_AT_STAIRS scond = "cond_TREVOR_AT_STAIRS" BREAK
	case cond_PISSING_DIALOGUE_CAN_PLAY scond = "cond_PISSING_DIALOGUE_CAN_PLAY" BREAK
	case cond_TREVOR_SEEN scond = "cond_TREVOR_SEEN" BREAK
	case cond_RESPOND_TO_BULLET scond = "cond_RESPOND_TO_BULLET" BREAK
	case cond_ONE_ATTACKER_LEFT_OUTSIDE scond = "cond_ONE_ATTACKER_LEFT_OUTSIDE" BREAK
	case COND_FAIL_FOR_LEAVING_FARM scond = "COND_FAIL_FOR_LEAVING_FARM" BREAK
	
	
	case cond_START_PETROL_TRAIL scond = "cond_START_PETROL_TRAIL" BREAK
	
	case cond_JERRY_CAN_DESTROYED scond = "cond_JERRY_CAN_DESTROYED" BREAK //allows this  case condition to span across both mission stages
	
	case cond_END_FARM_ASSAULT scond = "cond_END_FARM_ASSAULT" BREAK
	
	case cond_PLAYER_OUT_FRONT scond = "cond_PLAYER_OUT_FRONT" BREAK
	case cond_OUT_OF_GAS scond = "cond_OUT_OF_GAS" BREAK
	case cond_GAS_TRAIL_COMPLETE scond = "cond_GAS_TRAIL_COMPLETE" BREAK
	case cond_LAST_BLIP_POURED_ON scond = "cond_LAST_BLIP_POURED_ON" BREAK
	case cond_PLAYER_HAS_PETROL_CAN scond = "cond_PLAYER_HAS_PETROL_CAN" BREAK
	case cond_PLAYER_POURING_GAS scond = "cond_PLAYER_POURING_GAS" BREAK
	case COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE scond = "COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE" BREAK
	case cond_GAS_TRAIL_IGNITED_INCORRECTLY scond = "cond_GAS_TRAIL_IGNITED_INCORRECTLY" BREAK
	case cond_END_PETROL_TRAIL scond = "cond_END_PETROL_TRAIL" BREAK
	DEFAULT scond = " -NOT DEFINED-" BREAK
	ENDSWITCH
	
		
	SET_TEXT_SCALE(0.25,0.25)
	IF conditions[iCon].returns		
		SET_TEXT_COLOUR(0,0,255,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iCon*0.02),"STRING",sCond)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.25,0.25)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iCon*0.02),"STRING","TRUE")
	ELSE
		
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iCon*0.02),"STRING",sCond)
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.25,0.25)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iCon*0.02),"STRING","FALSE")
	ENDIF

ENDPROC


PROC PRINT_ACTION_DATA(int iData,bool shiftUp=FALSE)
	string sAct
	enumActions thisAct = actions[iData].action
	SWITCH thisAct
		CASE ACT_NULL  sAct = "ACT_NULL" BREAK
		//snipe from hill
		CASE act_shooting_peds_witness_impact_at_barrels  sAct = "act_shooting_peds_witness_impact_at_barrels" BREAK
		case act_stat_double_kill sAct = "act_stat_double_kill" BREAK
		case act_MUSIC_start_snipe sAct = "act_MUSIC_start_snipe" BREAK
		case act_MUSIC_approach_house_or_alert sAct = "act_MUSIC_approach_house_or_alert" BREAK
		case act_create_petrol_can_with_blip sAct = "act_create_petrol_can_with_blip" BREAK
		case act_stage_prep sAct = "act_stage_prep" BREAK
		case act_snipe_cams sAct = "act_snipe_cams" BREAK
		case act_remove_initial_assets sAct = "act_remove_initial_assets" BREAK
		case act_outside_peds_flee sAct = "act_outside_peds_flee" BREAK
		case act_remove_upstairs_peds sAct = "act_remove_upstairs_peds" BREAK
		case act_MUSIC_enter_house sAct = "act_MUSIC_enter_house" BREAK
		case act_remaining_peds_flee sAct = "act_remaining_peds_flee" BREAK
		case act_window_ped_walks_inside sAct = "act_window_ped_walks_inside" BREAK
		DEFAULT sAct = " -NOT DEFINED-" BREAK
	ENDSWITCH
	
	float shiftY
	IF shiftUp
		shiftY = -0.36
	ENDIF
	
	IF actions[iData].ongoing
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.1+(iData*0.03)+shiftY,"STRING","TRUE")		
	ELSE
		
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.1+(iData*0.03)+shiftY,"STRING","FALSE")
	ENDIF
	
	IF actions[iData].completed
		SET_TEXT_COLOUR(255,0,0,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1+(iData*0.03)+shiftY,"STRING","TRUE")		
	ELSE
		
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03)+shiftY,"STRING",sAct)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1+(iData*0.03)+shiftY,"STRING","FALSE")
	ENDIF
		
	showIntOnScreen(0.57,0.1+(iData*0.03)+shiftY,actions[iData].flag,"",0.35)
	showIntOnScreen(0.62,0.1+(iData*0.03)+shiftY,actions[iData].intA,"",0.35)
	
	
ENDPROc



PROC PRINT_INSTRUCTION_DATA(int iData)
		string sInst
		enumInstructions thisInst = instr[iData].ins
		SWITCH thisInst		 
			CASE INS_NULL sInst = "INS_NULL" BREAK
			CASE ins_enemy_reacting sInst = "ins_enemy_reacting" BREAK
			DEFAULT sInst = " -NOT DEFINED-" BREAK
		ENDSWITCH
		
		
		IF instr[iData].bCompleted
			SET_TEXT_COLOUR(255,0,0,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03),"STRING",sInst)
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.39,0.1+(iData*0.03),"STRING","TRUE")		
		ELSE
			
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iData*0.03),"STRING",sInst)
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.39,0.1+(iData*0.03),"STRING","FALSE")
		ENDIF
		
		SET_TEXT_SCALE(0.35,0.35)
		IF instr[iData].condTrue		
			SET_TEXT_SCALE(0.35,0.35)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iData*0.03),"STRING","TRUE")
		ELSE
			SET_TEXT_COLOUR(0,0,255,255)
			SET_TEXT_SCALE(0.35,0.35)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iData*0.03),"STRING","FALSE")
		ENDIF
			
		showIntOnScreen(0.5,0.1+(iData*0.03),instr[iData].flag,"",0.35)
		showIntOnScreen(0.55,0.1+(iData*0.03),instr[iData].intA,"",0.35)
		
	ENDPROC

PROC PRINT_DIALOGUE_DATA(int iDia)
	string sDia
	enumDialogue thisDia = dia[iDia].dial
	SWITCH thisDia		 
		CASE DIA_NULL sDia = "DIA_NULL" BREAK
		//drive to alley
		case dia_whatnow sDia = "dia_whatnow" BREAK
		case dia_trevor_rage_comments sDia = "dia_trevor_rage_comments" BREAK
		case dia_stop_trevor_getting_to_basement sDia = "dia_stop_trevor_getting_to_basement" BREAK
		case dia_cook_sees_ped_die sDia = "dia_cook_sees_ped_die" BREAK
		case dia_dont_let_him_down sDia = "dia_dont_let_him_down" BREAK
		case dia_hes_in_meth_lab sDia = "dia_hes_in_meth_lab" BREAK
		case dia_trevor_spotted_outside sDia = "dia_trevor_spotted_outside" BREAK
		case dia_find_trevor sDia = "dia_find_trevor" BREAK
		case dia_attacking_trevor_outside sDia = "dia_attacking_trevor_outside" BREAK
		case dia_attacking_trevor_inside sDia = "dia_attacking_trevor_inside" BREAK
		case dia_trevor_seen_inside_house sDia = "dia_trevor_seen_inside_house" BREAK
		case dia_RESPOND_TO_BULLET sDia = "dia_RESPOND_TO_BULLET" BREAK
		case dia_last_man_standing sDia = "dia_last_man_standing" BREAK
		DEFAULT sDia = " -NOT DEFINED-" BREAK
	ENDSWITCH
	
	SET_TEXT_SCALE(0.35,0.35)
	IF dia[iDia].bCompleted		
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iDia*0.03),"STRING",sDia)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iDia*0.03),"STRING","TRUE")
	ELSE
		
		SET_TEXT_COLOUR(0,0,255,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.1,0.1+(iDia*0.03),"STRING",sDia)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.1+(iDia*0.03),"STRING","FALSE")
	ENDIF
	
	SET_TEXT_SCALE(0.35,0.35)
	IF dia[iDia].bStarted		
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iDia*0.03),"STRING","TRUE")
	ELSE
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_SCALE(0.35,0.35)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.45,0.1+(iDia*0.03),"STRING","FALSE")
	ENDIF
	
	SET_TEXT_COLOUR(255,0,0,255)
	showIntOnScreen(0.5,0.1+(iDia*0.03),dia[iDia].flag,"",0.35)
	
ENDPROC

proc SHOW_DEBUG_SCRIPT_DATA()
	int iR
	
	IF bAllowShitskip
		g_savedGlobals.sFlow.missionSavedData[ENUM_TO_INT(SP_MISSION_CHINESE_2)].missionFailsNoProgress = 3
		
		bAllowShitskip = FALSE
		FAIL_MISSION("")
	ENDIF
	
	IF bScreenConditions		
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4,0.05,"STRING","Returns")
		
		REPEAT COUNT_OF(conditions) iR
		//	IF conditions[iR].active
				PRINT_CONDITION_DATA(iR)
		//	ENDIF
		ENDREPEAT
	ENDIF
	

	IF bScreenActionsA
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.43,0.05,"STRING","Ongoing")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.05,"STRING","Complete")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.57,0.05,"STRING","Flag")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(0,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.62,0.05,"STRING","IntA")
		REPEAT COUNT_OF(actions) iR
			IF actions[iR].active	
				PRINT_ACTION_DATA(iR)
			ENDIF
		ENDREPEAT
	ENDIF	
	
	IF bScreenInstructions
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.38,0.05,"STRING","Complete")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.44,0.05,"STRING","Cond")
		REPEAT COUNT_OF(instr) iR
			PRINT_INSTRUCTION_DATA(iR)
		ENDREPEAT
	ENDIF
	
	IF bSshowDialogue
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.42,0.05,"STRING","Complete")
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_COLOUR(255,0,255,255)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.47,0.05,"STRING","started")
		REPEAT COUNT_OF(dia) iR
			PRINT_DIALOGUE_DATA(iR)
		ENDREPEAT
	endif
	
endproc 


proc manage_debug()
	int iR
	if not DOES_WIDGET_GROUP_EXIST(widget_debug)
		widget_debug = START_WIDGET_GROUP("Chinese 2 widget") 	
			ADD_WIDGET_BOOL("Do Shit skip",bAllowShitskip)
			ADD_WIDGET_BOOL("Show Conditions",bScreenConditions)
			ADD_WIDGET_BOOL("Show Actions 0-12",bScreenActionsA)
			ADD_WIDGET_BOOL("Show Instructions",bScreenInstructions)
			ADD_WIDGET_BOOL("Show Dialogue",bSshowDialogue)
			ADD_WIDGET_BOOL("TRIGGER AND LOOP HOUSE EXPLOSION",bExplodeHouseLoop)
			ADD_WIDGET_BOOL("TRIGGER AND LOOP HOUSE EXPLOSION SHORT",bExplodeHouseLoopShort)
			ADD_WIDGET_BOOL("Ped Actions 1-15",bShowActionsA)
			ADD_WIDGET_BOOL("Ped Actions 16-30",bShowActionsB)
			ADD_WIDGET_BOOL("Show Focus",bShowFocus)
			ADD_WIDGET_BOOL("bShowAlerts",bShowAlerts)
			ADD_WIDGET_BOOL("Record ped alerts...",bRecordPedData)
			
			START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("none")
			ADD_TO_WIDGET_COMBO("balcony_far_left")
			ADD_TO_WIDGET_COMBO("balcony_doorway")
			ADD_TO_WIDGET_COMBO("balcony_veranda_walk")
			ADD_TO_WIDGET_COMBO("porch")
			ADD_TO_WIDGET_COMBO("barrels_1")
			ADD_TO_WIDGET_COMBO("barrels_2")
			ADD_TO_WIDGET_COMBO("garden_chat_1")
			ADD_TO_WIDGET_COMBO("garden_chat_2")
			ADD_TO_WIDGET_COMBO("shooting_bottle_1")
			ADD_TO_WIDGET_COMBO("shooting_bottle_2")
			ADD_TO_WIDGET_COMBO("inside_right_window")
			ADD_TO_WIDGET_COMBO("inside_top_window")
			ADD_TO_WIDGET_COMBO("inside_utility_room")
			ADD_TO_WIDGET_COMBO("inside_seated1")
			ADD_TO_WIDGET_COMBO("inside_seated2")
			ADD_TO_WIDGET_COMBO("inside_seated3")
			ADD_TO_WIDGET_COMBO("inside_cards1")
			ADD_TO_WIDGET_COMBO("inside_cards2")
			ADD_TO_WIDGET_COMBO("inside_cards3")
			ADD_TO_WIDGET_COMBO("inside_kitchen1")
			ADD_TO_WIDGET_COMBO("inside_kitchen2")
			ADD_TO_WIDGET_COMBO("inside_bystairs1")
			ADD_TO_WIDGET_COMBO("inside_bystairs2")
			ADD_TO_WIDGET_COMBO("innocent_chef")
			ADD_TO_WIDGET_COMBO("innocent_drugden")
			ADD_TO_WIDGET_COMBO("pissing_ped")
			
			STOP_WIDGET_COMBO("Recorded Ped", iRecordedPed)

			ADD_WIDGET_BOOL("Kill Outside Peds",bKillOutsidePeds)
			ADD_WIDGET_BOOL("Record Ped",bRecordPed)
			ADD_WIDGET_INT_SLIDER("Ped to rec",iPedToRecord,0,MAX_PEDS-1,1)
			ADD_WIDGET_BOOL("Show events",bShowEvents)
			ADD_WIDGET_INT_READ_ONLY("stageFlag",stageFlag)
			ADD_WIDGET_BOOL("Skip Drive",bSkipDrive)
			
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	ENDIF
	
	SHOW_DEBUG_SCRIPT_DATA()
	
	if bShowActionsA
	and bShowActionsB
		bShowActionsA=false
	endif

	float fx,fy,fdist,fclosest
		int iClosePed

	IF bShowFocus
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		TEXT_LABEL_31 txtlbl
		REPEAT COUNT_OF(zPED) iR
			txtlbl = ""
			if DOES_ENTITY_EXIST(zPED[iR].id)
				txtlbl = GET_PLACEMENT_STRING(iR)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(zPED[iR].id,txtlbl,1.4)		
				txtlbl = ""
				txtlbl = GET_ROLE_STRING(iR)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(zPED[iR].id,txtlbl,0.4)
			ENDIF
		ENDREPEAT	

	ENDIF
	
	
	if bShowActionsA
	or bShowActionsB
		int iPedMin,iPedMax
		IF bShowActionsA
			iPedMin = 0
			iPedMax = 15
		else
			iPedMin = 16
			iPedMax = 30
		endif
		//find which ped is closest to middle of screen
		
		
		
		REPEAT COUNT_OF(zPED) iR
			
			if DOES_ENTITY_EXIST(zPED[iR].id)
				IF GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(zPED[ir].id,FALSE),fx,fy)
					fx -= 0.5
					fy -= 0.5
					fDist = SQRT((fx*fx)+(fy*fy))
					if fDist < fClosest
						fClosest = fDist
						iClosePed = iR
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		
		
		
		
		REPEAT COUNT_OF(zPED) iR
			IF iR >= iPedMin and iR <= iPedMax
				SET_TEXT_SCALE(0.4,0.4)
				IF iClosePed = iR
					SET_TEXT_COLOUR(0,255,0,255)
				ELSE
					IF DOES_ENTITY_EXIST(zPED[iR].id)
						IF IS_PED_INJURED(zPED[iR].id)
							SET_TEXT_COLOUR(255,0,0,255)
						ELSE
							SET_TEXT_COLOUR(255,255,255,255)
						ENDIF
					ENDIF
				ENDIF
				
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.1+(iR * 0.025),"STRING",GET_PLACEMENT_STRING(iR))
				
				
				SET_TEXT_SCALE(0.4,0.4)
				IF iClosePed = iR
					SET_TEXT_COLOUR(0,255,0,255)
				ELSE
					IF DOES_ENTITY_EXIST(zPED[iR].id)
						IF IS_PED_INJURED(zPED[iR].id)
							SET_TEXT_COLOUR(255,0,0,255)
						ELSE
							SET_TEXT_COLOUR(255,255,255,255)
						ENDIF
					ENDIF
				ENDIF
				
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.24,0.1+(iR * 0.025),"STRING",GET_ACTION_STRING(iR))
				
				
				IF iClosePed = iR
					SET_TEXT_COLOUR(0,255,0,255)
				ELSE
					IF DOES_ENTITY_EXIST(zPED[iR].id)
						IF IS_PED_INJURED(zPED[iR].id)
							SET_TEXT_COLOUR(255,0,0,255)
						ELSE
							SET_TEXT_COLOUR(255,255,255,255)
						ENDIF
					ENDIF
				ENDIF
				showIntOnScreen(0.47,0.1+(iR * 0.025),zPED[iR].actionFlag)
				showIntOnScreen(0.5,0.1+(iR * 0.025),zPED[iR].intA)
				showIntOnScreen(0.53,0.1+(iR * 0.025),enum_to_int(zPED[iR].role))
			endif
		ENDREPEAT
	ENDIF
	
	IF bKillOutsidePeds
		bKillOutsidePeds = FALSE
		repeat count_of(zped) iR
			if zped[iR].role = role_guard_outside
				IF DOES_ENTITY_EXIST(zped[iR].id)
					IF NOT is_ped_injured(zped[iR].id)
						SET_ENTITY_HEALTH(zped[iR].id,0)
					ENDIF
				endif
			endif				
		endrepeat
	ENDIF
	
	IF bRecordPed
		PRINTLN("Role: ",zped[iPedToRecord].role,"Pos: ",GET_PLACEMENT_STRING(iPedToRecord)," action: ",GET_ACTION_STRING(iPedToRecord)," flag: ",zped[iPedToRecord].actionFlag," int: ",zped[iPedToRecord].intA)
	ENDIF
	
	IF bShowEvents
		REPEAT COUNT_OF(events) iR			
			SET_TEXT_SCALE(0.4,0.4)								
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.05,0.1+(iR * 0.025),"STRING",GET_THIS_EVENT_STRING(iR))
			SET_TEXT_SCALE(0.4,0.4)
			showIntOnScreen(0.26,0.1+(iR * 0.025),events[iR].flag)
			SET_TEXT_SCALE(0.4,0.4)
			showIntOnScreen(0.29,0.1+(iR * 0.025),events[iR].intA)
			SET_TEXT_SCALE(0.4,0.4)
			IF events[iR].boolA DISPLAY_TEXT_WITH_LITERAL_STRING(0.32,0.1+(iR * 0.025),"STRING","TRUE") ELSE DISPLAY_TEXT_WITH_LITERAL_STRING(0.32,0.1+(iR * 0.025),"STRING","FALSE") ENDIF			
			SET_TEXT_SCALE(0.4,0.4)
			IF events[iR].active DISPLAY_TEXT_WITH_LITERAL_STRING(0.38,0.1+(iR * 0.025),"STRING","ACTIVE") ELSE DISPLAY_TEXT_WITH_LITERAL_STRING(0.38,0.1+(iR * 0.025),"STRING","INACTIVE") ENDIF			
		ENDREPEAT
	ENDIF
	
	IF bSkipDrive
		bSkipDrive = FALSE
		IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
			SET_ENTITY_COORDS(vehPlayer,<< 2622.2361, 4804.1313, 32.5715 >>)
			SET_ENTITY_HEADING(vehPlayer,44)
		ELSE
			REQUEST_MODEL(BODHI2)
			WHILE NOT HAS_MODEL_LOADED(BODHI2)
				safewait(26)
			ENDWHILE
			vehPlayer = CREATE_VEHICLE(BODHI2,<<2623.5288, 4804.7842, 32.6049>>,43)
			SET_PED_INTO_VEHICLE(player_ped_id(),vehPlayer)			
			
		ENDIF
	ENDIF
	
	IF bExplodeHouseLoop
	AND bExplodeHouseLoopShort
		bExplodeHouseLoop = FALSE
	ENDIF
	
	IF bExplodeHouseLoop
	OR bExplodeHouseLoopShort
		IF missionStage != explosion_cut
			do_skip(explosion_cut)
		ENDIf
	ENDIF
	debug_check_keyboard()
ENDPROC

proc recordData(string debugText,int iPedCompare=-1)
	int iPedsDetails
	IF bRecordPedData
	
		iPedsDetails = get_ped_with_placement(int_to_enum(enumPlacement,iRecordedPed))
	
		IF iPedCompare = -1
		OR iPedCompare = iPedsDetails
			IF iRecordedPed > 0
				cprintln(DEBUG_TREVOR3,debugText," State: ",GET_THIS_STATE_STRING(iPedsDetails)," Alerted: ",GET_ALERTED_STRING(iPedsDetails)," last Alert: ",GET_ALERTED_STRING(iPedsDetails,TRUE)," action Flag: ",zped[iPedsDetails].actionFlag," intA: ",zped[iPedsDetails].intA," vpos: ",zped[iPedsDetails].vpos)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF



// ================================================== Manage Peds ===============================================

FUNC bool checkAllEnemyDead()
	int i
	FOR i = 0 to MAX_PEDS-1
		IF DOES_BLIP_EXIST(zped[i].blip)
		OR zped[i].action = action_spawn
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
endfunc

FUNC BOOL NAVIGATE_NEAR_TO_THIS_POINT(ped_index pedID, vector vGoto, FLOAT pedmoveSpeed = pedmove_run)
	IF NOT IS_PED_INJURED(pedID)
		IF GET_SCRIPT_TASK_STATUS(pedID,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedID,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(pedID,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != DORMANT_TASK
			TASK_FOLLOW_NAV_MESH_TO_COORD(pedID,vGoto,pedmoveSpeed)
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedID,vGoto) < 2.5			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC





PROC CONTROL_PED_ACTIONS()
	int i
	REPEAT COUNT_OF(pedReactionState) i
		IF (NOT IS_PED_INJURED(zped[i].id)
		AND NOT IS_PED_DEAD_OR_DYING(zped[i].id))
		OR pedReactionState[i].State = STATE_SPAWN
			
			
			
			SWITCH pedReactionState[i].State
				CASE STATE_SPAWN
					SWITCH zPED[i].role
						CASE role_guard_outside
							SWITCH zPED[i].placement
								case pos_balcony_veranda_walk
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2451.8179, 4985.7041, 50.5678 >>,227.8420, WEAPONTYPE_ASSAULTRIFLE) 										
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,true)
										set_ped_action(i,action_balcony_guard_patrol)
									ENDIF
								BREAK
								case pos_balcony_doorway 
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01,  << 2455.0850, 4974.4658, 50.5677 >>, 246.0510, WEAPONTYPE_ASSAULTRIFLE, TRUE)
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,true)
										set_ped_action(i,action_smoke)
									ENDIF
								BREAK
								case pos_garden_chat_1
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2461.6089, 4993.6748, 44.9821 >>, -90.0, WEAPONTYPE_ASSAULTRIFLE,TRUE)									
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE)
										set_ped_action(i,action_chatting)
										zPED[i].lastAction = action_chatting //when action_chatting ends it reverts to last action
									ENDIF
								BREAK
								case pos_garden_chat_2
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2462.8872, 4993.6138, 44.9474 >>, 89.8899, WEAPONTYPE_ASSAULTRIFLE, TRUE)										
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE)
										set_ped_action(i,action_chatting,1)
										zPED[i].lastAction = action_chatting //when action_chatting ends it reverts to last action
									ENDIF
								BREAK
								case pos_porch
									IF CREATE_GANG_PED(i,A_M_Y_MethHead_01, <<2460.6506, 4970.8135, 45.5765>>,240.2054, WEAPONTYPE_ASSAULTRIFLE, FALSE) // 2463.9058, 4973.9673, 45.5764 >>,240.2054, WEAPONTYPE_MICROSMG, FALSE)										
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,TRUE)
										set_ped_action(i,action_drinking)
									ENDIF
								BREAK
								case pos_balcony_far_left
									IF CREATE_GANG_PED(i,A_M_Y_MethHead_01, << 2443.1384, 4966.7090, 50.5677 >>,246.0510, WEAPONTYPE_ASSAULTRIFLE, TRUE)
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,TRUE,true)
										set_ped_action(i,action_binoculars)
									ENDIF
								BREAK
								case pos_barrels_1	
									IF missionStage = farm_cutscene
										IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, <<2457.0979, 4954.2827, 44.1304>>, 316.5498,WEAPONTYPE_PISTOL,FALSE)	
											zPED[i].obj = CREATE_OBJECT(Prop_CS_Fertilizer, <<2460.0979, 4957.2827, 44.1304>>)
											zPED[i].actionFlag = 1
											set_ped_action(i,action_roll_barrels_at_front_of_house)
										ENDIF
									else
										IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2478.3201, 4982.3247, 44.8913 >>, 49.5021,WEAPONTYPE_PISTOL,FALSE)									
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE)
											zPED[i].obj = CREATE_OBJECT(Prop_CS_Fertilizer, << 2478.4443, 4976.9214, 44.5614 >>)
											zPED[i].actionFlag = 1
											set_ped_action(i,action_roll_barrels)
										ENDIF
									ENDIF
								BREAK
								case pos_barrels_2
									IF missionStage = farm_cutscene
										IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, <<2454.7527, 4951.7188, 44.1445>>, 316.5541,WEAPONTYPE_PISTOL,FALSE)	
											zPED[i].obj = CREATE_OBJECT(Prop_CS_Fertilizer, <<2455.7527, 4952.7188, 44.1445>>)
											zPED[i].actionFlag = 1
											set_ped_action(i,action_roll_barrels_at_front_of_house)
										ENDIF
									ELSE
										IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2475.8733, 4984.4263, 45.0916 >>, 45.8993,WEAPONTYPE_PISTOL,FALSE)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE)
											zPED[i].obj = CREATE_OBJECT(Prop_CS_Fertilizer, << 2476.5815, 4978.4917, 44.5734 >>)	
											zPED[i].actionFlag++
											set_ped_action(i,action_roll_barrels)
										ENDIF	
									ENDIF
								BREAK
								case pos_shooting_bottle_1
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2505.451, 4970.663, 43.548 >>, 49.5021,WEAPONTYPE_PISTOL,TRUE)
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE)
										zPED[i].intA = CREATE_SYNCHRONIZED_SCENE(<< 2505.451, 4970.663, 43.548 >>, << -0.000, -0.000, 88.725 >>)									
										
										SET_PED_VISUAL_FIELD_PROPERTIES(zPED[i].id,20,5,90,-90,60)
										SET_AMBIENT_VOICE_NAME(zPED[i].id,"A_M_M_HillBilly_02_WHITE_MINI_02")
										TASK_SYNCHRONIZED_SCENE (zPED[i].id, zPED[i].intA, "misschinese2_bank5", "peds_shootcans_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
										SET_SYNCHRONIZED_SCENE_LOOPED(zPED[i].intA,TRUE)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
										set_ped_action(i,action_shoot_cans,0,zPED[i].intA)
										add_event(events_release_bottle_shooting_anims)
									ENDIF
								BREAK
								case pos_shooting_bottle_2
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2505.451, 4970.663, 43.548 >>, 49.5021,WEAPONTYPE_PISTOL,TRUE)												
										zPED[i].intA = CREATE_SYNCHRONIZED_SCENE(<< 2507.879, 4970.188, 43.500 >>, << -0.000, -0.000, 93.42 >>)
										SET_SYNCHRONIZED_SCENE_LOOPED(zPED[i].intA,TRUE)																																						
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE)
								
										
										TASK_SYNCHRONIZED_SCENE (zPED[i].id, zPED[i].intA, "misschinese2_bank5", "peds_shootcans_b", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )									
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
										set_ped_action(i,action_shoot_cans,0,zPED[i].intA)											
									ENDIF
								BREAK
								
													
							ENDSWITCH
							IF DOES_ENTITY_EXIST(zPED[i].id)
								SET_ENTITY_LOAD_COLLISION_FLAG(zPED[i].id, TRUE)		
								SET_PED_TO_LOAD_COVER(zPED[i].id, TRUE)
							ENDIF
						BREAK						
						
						case role_guard_house								
							SWITCH zPED[i].placement
								case pos_inside_right_window
									IF missionStage < get_to_house
										IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2458.5696, 4987.584, 45.8107 >>,233.8420, WEAPONTYPE_PISTOL,false)
											set_ped_action(i,action_smoke)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,TRUE)
										ENDIF
									else
										IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2454.6272, 4995.7261, 45.2011 >>,233.8420, WEAPONTYPE_PISTOL,false)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,TRUE)
											set_ped_action(i,action_advance_on_hill)
										ENDIF
									endif
									
								BREAK
								
								case pos_inside_utility_room
									if missionStage < get_to_house
										If CREATE_GANG_PED(i,A_M_Y_MethHead_01, << 2442.9937, 4985.1636, 45.8103 >>, 303.409, WEAPONTYPE_MICROSMG,false)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,TRUE,TRUE)
											set_ped_action(i,action_smoke)
										ENDIF
									else
										IF CREATE_GANG_PED(i,A_M_Y_MethHead_01, << 2450.3706, 4995.1030, 44.9282 >>, 303.409, WEAPONTYPE_MICROSMG,false)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,TRUE,TRUE)
											set_ped_action(i,action_advance_on_hill)
										ENDIf
									endif
									
								BREAK	
								case pos_inside_seated1
									If CREATE_GANG_PED(i,A_M_Y_MethHead_01, << 2433.124, 4967.804, 46.293 >>, 63.7673, WEAPONTYPE_PUMPSHOTGUN,false,TRUE)
										set_ped_action(i,action_sit) 
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,TRUE)
									ENDIF
									
								break
								case pos_inside_seated2
									If CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2432.091, 4968.833, 46.298 >>, 63.7673, WEAPONTYPE_PISTOL,false,TRUE)
										set_ped_action(i,action_sit) 
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,TRUE)
									ENDIF
								break
								case pos_inside_seated3
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, <<2431.4849, 4964.7793, 45.8106>>, -137.500, WEAPONTYPE_PISTOL,false,TRUE)
										set_ped_action(i,action_sit) 
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,TRUE)
									ENDIf
								break
								case pos_inside_cards1
									
							//		If CREATE_GANG_PED(i,A_M_Y_MethHead_01, << 2439.697, 4960.996, 46.323 >>, 63.7673, WEAPONTYPE_PUMPSHOTGUN,false,TRUE)
									If CREATE_GANG_PED(i,A_M_Y_MethHead_01, <<2440.3855, 4965.4287, 45.8106>>, 63.7673, WEAPONTYPE_PUMPSHOTGUN,false,TRUE)
										set_ped_action(i,action_sit) 
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,true)
									ENDIF
								break
								case pos_inside_cards2
									//IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2440.229, 4963.038, 46.323 >>, 63.7673, WEAPONTYPE_MICROSMG,false,TRUE)
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, <<2439.7410, 4964.8896, 45.8106>>, 130.3988, WEAPONTYPE_MICROSMG,false,TRUE)
										set_ped_action(i,action_sit) 
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,true)
									ENDIf
								break
								case pos_inside_cards3																		
									//IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2438.9434, 4962.6670, 45.8106 >>, 63.7673, WEAPONTYPE_PISTOL,false,TRUE)
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, <<2438.7068, 4964.0938, 45.8106>>, 127.6523, WEAPONTYPE_PISTOL,false,TRUE)
										SET_PED_ACCURACY(zped[i].id,5)
										set_ped_action(i,action_sit)
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,true)
									ENDIf
								break
								case pos_inside_kitchen1									
									if missionStage < get_to_house
										If CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2440.9287, 4976.9214, 45.8106 >>, -130.0, WEAPONTYPE_PISTOL,false,TRUE)
											set_ped_action(i,action_chatting)
											zPED[i].lastAction = action_chatting
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,FALSE)
										ENDIF
									else
										If CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2452.0525, 4956.6890, 43.8957 >>, 230.7673, WEAPONTYPE_PISTOL,false,TRUE)
											//set_ped_action(i,action_wait_on_player_advance_to_house) 
											set_ped_action(i,action_advance_on_hill)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,FALSE)
										ENDIf
									endif
								break
								case pos_inside_kitchen2
									if missionStage < get_to_house
										If CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2442.9260, 4975.1484, 45.8106 >>, 46.7673, WEAPONTYPE_PISTOL,false,TRUE)
											set_ped_action(i,action_chatting) 
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,FALSE)
											zPED[i].lastAction = action_chatting
										ENDIF
									else 
										If CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2449.3384, 4954.1270, 43.9299 >>, 46.7673, WEAPONTYPE_PISTOL,false,TRUE)										
											//set_ped_action(i,action_wait_on_player_advance_to_house)
											ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,FALSE,FALSE)
											set_ped_action(i,action_advance_on_hill)
										ENDIf
									endif
								break	
								case pos_inside_bystairs1
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01, << 2449.1138, 4981.9839, 45.8102 >>, 130.7673, WEAPONTYPE_MICROSMG,false,TRUE)
										set_ped_action(i,action_chatting) 
										zPED[i].lastAction = action_hum
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,TRUE,FALSE)
									ENDIf
								break
								case pos_inside_bystairs2
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_02, << 2447.5527, 4980.5151, 45.8096 >>, 311.7673, WEAPONTYPE_MICROSMG,false,TRUE)
										set_ped_action(i,action_chatting) 
										zPED[i].lastAction = action_chatting
										ADD_PED_TO_REACT_ARRAY_AT_ENTRY(pedReactionState,i,zped[i].id,TRUE,FALSE)
									ENDIF
								break										
								//setting up skip to this point BUT now we don't need it.									
							ENDSWITCH
							IF NOT IS_PED_INJURED(zPED[i].id)
								//SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_WILL_SCAN_FOR_DEAD_PEDS,FALSE)		//commented out because, why not?							
								SET_ENTITY_LOAD_COLLISION_FLAG(zPED[i].id, TRUE)
							ENDIF
						break
						case role_innocent
							SWITCH zPED[i].placement
								case pos_innocent_chef
									IF CREATE_GANG_PED(i,A_M_M_Hillbilly_01,<< 2439.2576, 4976.2007, 45.8106 >>,116.8150,WEAPONTYPE_UNARMED,false,TRUE)						
										set_ped_action(i,action_cooking)
									ENDIF
								break
								case pos_innocent_drugden											
									IF CREATE_GANG_PED(i,A_M_Y_METHHEAD_01,<< 2433.292, 4968.534, 42.348 >>,105.500,WEAPONTYPE_UNARMED,false,true)									
										set_ped_action(i,action_meth_lab)
									ENDIF
								BREAK
							ENDSWITCH
						BREAK							
					ENDSWITCH						
	
					if NOT IS_PED_INJURED(zPED[i].id) //removed check for only entity exists due to bug 1230243
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(zPED[i].id,TRUE)	
						pedReactionState[i].State = STATE_AMBIENT
						i -= 1 //force loop to backtrack by one so ped will immediately run the ambient task
					ENDIF
				BREAK
				CASE STATE_AMBIENT
					IF pedReactionState[i].state <= STATE_AMBIENT
						SWITCH zPED[i].action
							case action_roll_barrels_at_front_of_house
								switch zPED[i].actionFlag
									case 0	
										cprintln(debug_trevor3,"RUN A")
										IF DOES_ENTITY_EXIST(zPED[i].obj)
											cprintln(debug_trevor3,"RUN B")
											IF DOES_ENTITY_HAVE_DRAWABLE(zPED[i].obj)
											AND DOES_ENTITY_HAVE_PHYSICS(zPED[i].obj)
												ATTACH_ENTITY_TO_ENTITY(zPED[i].obj, zPED[i].id, GET_PED_BONE_INDEX(zPED[i].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)									
												zPED[i].actionFlag++
											ENDIF
											
											IF DOES_ENTITY_EXIST(zPED[i].obj)																					
												IF zPED[i].placement = pos_barrels_1
													TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B", INSTANT_BLEND_IN, -1, -1, AF_LOOPING)																																
												ELSE
													TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A", INSTANT_BLEND_IN, -1, -1, AF_LOOPING)		
												ENDIF
												SET_ENTITY_LOAD_COLLISION_FLAG(zPED[i].id,TRUE)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
												zPED[i].actionFlag++											
											ENDIF
										ENDIF										
									BREAK
									CASE 1
										//start rolling barrels
										
									BREAK
									
								ENDSWITCH
							break
							case action_roll_barrels
								IF zPED[i].actionFlag < 7
									UPDATE_AI_PED_BLIP(zPED[i].id,zped[i].aiBlip,-1,null,true)
								ENDIF
															
								if IS_ENTITY_PLAYING_ANIM(zPED[i].id,"misschinese2_barrelroll", "barrel_roll_loop_B")
									SET_ENTITY_ANIM_SPEED(zPED[i].id,"misschinese2_barrelroll", "barrel_roll_loop_B",1.4)
								ENDIF
								IF IS_ENTITY_PLAYING_ANIM(zPED[i].id,"misschinese2_barrelroll", "barrel_roll_loop_A") 
									SET_ENTITY_ANIM_SPEED(zPED[i].id,"misschinese2_barrelroll", "barrel_roll_loop_A",1.4)
								ENDIF
											
								
								IF DOES_ENTITY_EXIST(zPED[i].obj)
									IF zPED[i].actionFlag > 2
									AND zPED[i].actionFlag <= 6
										IF IS_ENTITY_ATTACHED(zPED[i].obj)
								//		IF DOES_ENTITY_HAVE_DRAWABLE(zPED[i].obj)
								//		AND DOES_ENTITY_HAVE_PHYSICS(zPED[i].obj)
											zPED[i].floatA = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(zPED[i].id),zPED[i].vpos,TRUE) - GET_ENTITY_HEADING(zPED[i].id)
											zPED[i].floatA = CLAMP(zPED[i].floatA/4.0,-15.0,15.0)
											IF NOT IS_PED_RAGDOLL(zPED[i].id)
												SET_ENTITY_HEADING(zPED[i].id,GET_ENTITY_HEADING(zPED[i].id)+(TIMESTEP()*zPED[i].floatA))
											ENDIF
											/*IF zPED[i].placement = pos_barrels_1
												if not IS_ENTITY_PLAYING_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B")
													TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)														
												else
													if not IS_ENTITY_ATTACHED_TO_ANY_PED(zPED[i].obj)
														ATTACH_ENTITY_TO_ENTITY(zPED[i].obj, zPED[i].id, GET_PED_BONE_INDEX(zPED[i].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, FALSE)																					
													endif
													
												ENDIF
											ELSE
												if not IS_ENTITY_PLAYING_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A")
													TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)														
												else
													if not IS_ENTITY_ATTACHED_TO_ANY_PED(zPED[i].obj)
														ATTACH_ENTITY_TO_ENTITY(zPED[i].obj, zPED[i].id, GET_PED_BONE_INDEX(zPED[i].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, FALSE)																					
													endif
													
												ENDIF
											ENDIF	*/																													
										ENDIF
									ENDIF
								ENDIF
								
								switch zPED[i].actionFlag
									case 0								
										IF DOES_ENTITY_EXIST(zPED[i].obj)
											IF DOES_ENTITY_HAVE_DRAWABLE(zPED[i].obj)
											AND DOES_ENTITY_HAVE_PHYSICS(zPED[i].obj)
												ATTACH_ENTITY_TO_ENTITY(zPED[i].obj, zPED[i].id, GET_PED_BONE_INDEX(zPED[i].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)									
									
												add_event(events_detach_barrels_from_peds)
												zPED[i].actionFlag++
											ENDIF
										ENDIF										
									FALLTHRU // 1999874
									CASE 1
										//start rolling barrels
										IF DOES_ENTITY_EXIST(zPED[i].obj)
										//	if IS_POINT_VISIBLE(<< 2478.3201, 4982.3247, 44.8913 >>,3.0)
												if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zPED[i].id,<<2495.778564,5004.714844,43.873367>>) < 99.0															
//													IF zPED[i].placement = pos_barrels_1
//														TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B", NORMAL_BLEND_IN, -1, -1, AF_LOOPING)																																
//													ELSE
//														TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A", NORMAL_BLEND_IN, -1, -1, AF_LOOPING)		
//													ENDIF

													// 1999874
													IF zPED[i].placement = pos_barrels_1
														TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B", INSTANT_BLEND_IN, -1, -1, AF_LOOPING)																																
													ELSE
														TASK_PLAY_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A", INSTANT_BLEND_IN, -1, -1, AF_LOOPING)		
													ENDIF
													FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
													SET_ENTITY_LOAD_COLLISION_FLAG(zPED[i].id,TRUE)
													zPED[i].actionFlag++
												ENDIF
	//										ENDIF
										ENDIF
									BREAK
									case 2
										if IS_ENTITY_PLAYING_ANIM(zPED[i].id,"misschinese2_barrelroll", "barrel_roll_loop_B")
										OR IS_ENTITY_PLAYING_ANIM(zPED[i].id,"misschinese2_barrelroll", "barrel_roll_loop_A") 										
											zPED[i].vpos = << 2466.9026, 4996.2539, 45.5443 >>
											zPED[i].actionFlag++									
										ENDIF
									BREAK
									case 3												
										IF IS_ENTITY_AT_COORD(zPED[i].id, zPED[i].vpos, <<1.5,1.5,2>>)
											zPED[i].vpos = << 2461.3323, 4998.9399, 45.3590 >>
											zPED[i].actionFlag++
										ENDIF
									BREAK
									case 4												
										IF IS_ENTITY_AT_COORD(zPED[i].id, zPED[i].vpos, <<1.5,1.5,2>>)
											zPED[i].vpos = << 2455.9841, 4997.4043, 45.2645 >>
											zPED[i].actionFlag++
										ENDIF
									BREAK
									case 5										
										IF IS_ENTITY_AT_COORD(zPED[i].id, zPED[i].vpos, <<1.5,1.5,2>>)
											if zPED[i].placement = pos_barrels_2
												zPED[i].vpos = << 2452.0906, 4993.6738, 45.1404 >>
												zPED[i].actionFlag++
											ELSe
												zPED[i].vpos = << 2454.0906, 4995.6738, 45.1404 >>
												zPED[i].actionFlag++
											endif
										ENDIF
									BREAK																		
									case 6									
										IF IS_ENTITY_AT_COORD(zPED[i].id, zPED[i].vpos, <<1.5,1.5,2>>)
											IF IS_ENTITY_PLAYING_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B")									
												STOP_ANIM_TASK(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_B",-1.5)
											ENDIF
											
											IF IS_ENTITY_PLAYING_ANIM(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A")									
												STOP_ANIM_TASK(zPED[i].id, "misschinese2_barrelroll", "barrel_roll_loop_A",-1.5)									
											ENDIF
											zPED[i].vpos = <<0,0,0>>
											IF DOES_ENTITY_EXIST(zPED[i].obj)		
												DETACH_ENTITY(zPED[i].obj,FALSE,TRUE)
											//	if zPED[i].placement = pos_barrels_1
											//		SET_ENTITY_COORDS (zPED[i].obj, <<2451.871582,4992.753418,45.442631>>)
											//	ELSE
											//		SET_ENTITY_COORDS (zPED[i].obj,<<2452.400146,4993.188965,45.454922>>)
											//	ENDIF
											//	SET_ENTITY_ROTATION (zPED[i].obj, <<-0.000000,-0.000000,-119.748169>>)
											//	SET_OBJECT_AS_NO_LONGER_NEEDED(zPED[i].obj)
											ENDIF
											if IS_VEHICLE_VALID_AND_DRIVEABLE(BarrelVan)
												if zPED[i].placement = pos_barrels_1
													////cprintln(DEBUG_TREVOR3,"NEW MOVEMENT")
													SET_PED_MOVEMENT_CLIPSET(zPED[i].id,"move_m@gangster@var_e")
													TASK_ENTER_VEHICLE(zPED[i].id, BarrelVan, -1, VS_DRIVER, pedmove_walk)
													set_ped_Action(get_ped_with_placement(pos_garden_chat_1),action_smoke)
													set_ped_action(get_ped_with_placement(pos_garden_chat_2),action_get_in_car)
												ELSE
													SET_PED_MOVEMENT_CLIPSET(zPED[i].id,"move_m@gangster@var_f")
													TASK_ENTER_VEHICLE(zPED[i].id, BarrelVan, -1, VS_FRONT_RIGHT, pedmove_walk)
												ENDIF
											ENDIF							
											zPED[i].actionFlag++
											zPED[i].intA = 0
											zPED[i].intB = 0
										ENDIF
									BREAK
									case 7
										LOOK_AT_SOMETHING(i)										
									
										if IS_PED_IN_ANY_VEHICLE(zPED[i].id)
											SET_ENTITY_LOAD_COLLISION_FLAG(zPED[i].id,FALSE)
											zPED[i].actionFlag++								
										ENDIF
									BREAK
									case 8
										if IS_VEHICLE_VALID_AND_DRIVEABLE(BarrelVan)
											if not IS_VEHICLE_SEAT_FREE(BarrelVan,VS_DRIVER)									
												if zPED[i].placement = pos_barrels_1												
													IF not IS_VEHICLE_SEAT_FREE(BarrelVan,VS_FRONT_RIGHT)
														int iSeatPed
														iSeatPed = get_ped_with_placement(pos_barrels_2)
														IF iSeatPed >= 0
															IF NOT IS_PED_INJURED(zPED[iSeatPed].id)
																IF IS_PED_SITTING_IN_ANY_VEHICLE(zPED[iSeatPed].id)
																	startSeq()
																	TASK_VEHICLE_DRIVE_TO_COORD(null,BarrelVan,<<2367.6677, 5098.6445, 46.8185>>,12.0,DRIVINGSTYLE_NORMAL,BURRITO,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,5.0,5.0)
																	TASK_VEHICLE_DRIVE_WANDER(null,BarrelVan,19,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
																	endseq(zPED[i].id)
																	set_condition_state(COND_BARREL_ROLL_PEDS_IN_VAN,TRUE)
																	zPED[i].actionFlag++
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												
											ENDIF
											IF not IS_VEHICLE_SEAT_FREE(BarrelVan,VS_FRONT_RIGHT)
												if zPED[i].placement = pos_barrels_2
													zPED[i].actionFlag++
												ENDIF									
											ENDIF
										ENDIF
									BREAK
									case 9					
																
									BREAK
								ENDSWITCH
							break
							
							
							
							case action_balcony_guard_patrol
								switch zPED[i].actionFlag
									case 0
										startSeq()
										
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2459.6462, 4977.7354, 50.5678 >>,PEDMOVE_WALK)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2456.2209, 4974.6851, 50.5678 >>,PEDMOVE_WALK)
										TASK_PED_SLIDE_TO_COORD(null,<< 2456.2209, 4974.6851, 50.5678 >>,92.5678)
										TASK_ACHIEVE_HEADING(null,92.5678)
										endSeq(zPED[i].id)
										zPED[i].actionFlag++							
									BREAK
									case 1
										if GET_SCRIPT_TASK_STATUS(zPED[i].id,script_task_perform_sequence) = FINISHED_TASK
											set_ped_action(i,action_chatting,0,9000)
											int itemp
											itemp = enum_to_int(pos_balcony_doorway)
											if itemp != -1
											//	zPED[i].intA = itemp
												set_ped_action(itemp,action_chatting,0,9000)
											//	zPED[itemp].intA = i
											ENDIF								
										ENDIF
									BREAK
									case 2
										startSeq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2450.4912, 4986.8296, 50.5677 >>,PEDMOVE_WALK)
										TASK_ACHIEVE_HEADING(null,309.5567)
										TASK_PAUSE(null,2000)
										endSeq(zPED[i].id)
										zPED[i].actionFlag++
									BREAK
									case 3
										if GET_SCRIPT_TASK_STATUS(zPED[i].id,script_task_perform_sequence) = FINISHED_TASK
											zPED[i].actionFlag = 0
										ENDIF
									BREAK
								ENDSWITCH
							BREAK
							
							case action_balcony_guard_chat
								IF GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = FINISHED_TASK
								AND zPED[i].actionFlag > 1
									zPED[i].actionFlag = 0
								ENDIF
								switch zPED[i].actionFlag
									case 0
							
											SET_CURRENT_PED_WEAPON(zPED[i].id,WEAPONTYPE_UNARMED,TRUE)
											TASK_ACHIEVE_HEADING(zPED[i].id,219,1500)
											zPED[i].actionFlag++
									BREAK
									CASE 1
										IF GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_ACHIEVE_HEADING) = FINISHED_TASK
											TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_GUARD_STAND_ARMY")
											zPED[i].actionFlag++
										ENDIF

									BREAK

								ENDSWITCH
							BREAK
							
							case action_hum
								IF NOT IS_PED_INJURED(zPED[i].id)
									IF zPED[i].role = role_guard_outside
									OR GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zPED[i].id) < 20.0
										UPDATE_AI_PED_BLIP(zPED[i].id,zped[i].aiBlip,-1,null,true)
									ENDIF
									
									//make reaction if ped hears player.
									IF zPED[i].actionFlag < 10
									//	IF CAN_PED_HEAR_PLAYER(player_id(),zPED[i].id)
									//		zPED[i].actionFlag = 10
									//	ENDIF
									ENDIF
									
									switch zPED[i].actionFlag							
										CASE 0
											TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_AA_SMOKE",zPED[i].intA)
											zPED[i].actionFlag++
										BREAK
										case 1
											IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zPED[i].id) < 10.0
												int iRand
												iRand = GET_RANDOM_INT_IN_RANGE(0,3)
												IF iRand = 0
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zPED[i].id, "GENERIC_CURSE_MED", "A_M_M_Hillbilly_01_White_mini_03", "SPEECH_PARAMS_FORCE_SHOUTED")
												ELIF iRand = 1
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zPED[i].id, "GENERIC_INSULT_MED", "A_M_M_Hillbilly_01_White_mini_03", "SPEECH_PARAMS_FORCE_SHOUTED")
												ELSE
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zPED[i].id, "CHAT_STATE", "A_M_M_Hillbilly_01_White_mini_03", "SPEECH_PARAMS_FORCE_SHOUTED")
												ENDIF
												
												zPED[i].actionFlag++
												zPED[i].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000,10000)												
											ENDIF
										break
										case 2
											IF GET_GAME_TIMER() > zPED[i].intA
												zPED[i].actionFlag=1
											ENDIF
										break
										case 10 //reaction for hearing player										
											IF CREATE_CONVERSATION_EXTRA("CHI2_hear",8,zPED[i].id,"ONEILGUARD1")
												zPED[i].actionFlag++
												zPED[i].intA = GET_GAME_TIMER() + 1000
											ENDIF
											
										BREAK
										CASE 11
											IF GET_GAME_TIMER() > zPED[i].intA
												TASK_TURN_PED_TO_FACE_COORD(zped[i].id,GET_ENTITY_COORDS(player_ped_id()),5000)
												zPED[i].actionFlag++
												zPED[i].intA = GET_GAME_TIMER() + 5000
											ENDIF
										BREAK
										CASE 12
											IF CAN_PED_HEAR_PLAYER(player_id(),zPED[i].id)
											AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[i].id,<<2448.0027, 4982.3052, 45.8519>>) < 10.0
												IF GET_GAME_TIMER() > zPED[i].intA - 3000
													TASK_FOLLOW_NAV_MESH_TO_COORD(zped[i].id,GET_ENTITY_COORDS(player_ped_id()),pedmove_walk)
													zPED[i].actionFlag=15
												ENDIF
											ELSE
												IF GET_GAME_TIMER() > zPED[i].intA						
													IF CREATE_CONVERSATION_EXTRA("CHI2_nope",8,zPED[i].id,"ONEILGUARD1")
														zPED[i].actionFlag++
													ENDIF										
												ENDIF
											ENDIF
										BREAK
										CASE 13
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2449.1138, 4981.9839, 45.8102 >>,pedmove_walk)
											TASK_ACHIEVE_HEADING(null,127.6328,3000)
											endseq(zped[i].id)
											zPED[i].intA = GET_GAME_TIMER() + 3000
											zPED[i].actionFlag++
										BREAK
										CASE 14
											IF GET_GAME_TIMER() > zPED[i].intA
											OR GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK						
												zPED[i].actionFlag=0											
											ENDIF
										BREAK
										case 15
											IF CREATE_CONVERSATION_EXTRA("CHI2_hear",8,zPED[i].id,"ONEILGUARD1")
												zPED[i].actionFlag++										
											ENDIF
										BREAK
										CASE 16
											IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
												zPED[i].intA = GET_GAME_TIMER() + 3000
												zPED[i].actionFlag=12
											ENDIF
										break
										
									ENDSWITCH
								endif
									
							BREAK
							
							case action_get_in_car
								switch zPED[i].actionFlag								
									CASE 0
										zPED[i].intA = GET_GAME_TIMER() + 3000
										zPED[i].actionFlag++
									BREAK
									CASE 1
										IF GET_GAME_TIMER() > zPED[i].intA
											IF IS_VEHICLE_VALID_AND_DRIVEABLE(BarrelVan)
												TASK_ENTER_VEHICLE(zped[i].id,BarrelVan,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,pedmove_walk)										
												zPED[i].actionFlag++
												zPED[i].intA = 0
												zPED[i].intB = 0
											ENDIF
										ENDIF
									BREAK
									CASE 2
										LOOK_AT_SOMETHING(i)
									BREAK
								ENDSWITCH
							break
							
							case action_chatting
								//force blips on if peds set to be chatting and out side or within 20m
								IF zPED[i].role = role_guard_outside
								OR GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zPED[i].id) < 20.0
									UPDATE_AI_PED_BLIP(zPED[i].id,zped[i].aiBlip,-1,null,true)
								ENDIF
								
								switch zPED[i].actionFlag
									
									CASE 0
										WEAPON_TYPE wep
										wep = WEAPONTYPE_UNARMED
										IF GET_CURRENT_PED_WEAPON(zPED[i].id,wep)
											TASK_SWAP_WEAPON(zPED[i].id,FALSE)
										ENDIF
										zPED[i].actionFlag++
										
									BREAK
									
									CASE 1
										IF NOT GET_CURRENT_PED_WEAPON(zPED[i].id,wep)
											startSeq()
												If i = enum_to_int(pos_balcony_doorway)
													
													if not is_ped_injured(zPED[enum_to_int(pos_balcony_veranda_walk)].id)
														TASK_TURN_PED_TO_FACE_ENTITY(null,zPED[enum_to_int(pos_balcony_veranda_walk)].id,2000)
													endif
												elif i = enum_to_int(pos_balcony_veranda_walk)
													if not is_ped_injured(zPED[enum_to_int(pos_balcony_doorway)].id)
														TASK_TURN_PED_TO_FACE_ENTITY(null,zPED[enum_to_int(pos_balcony_doorway)].id,2000)
													endif
												elif i = enum_to_int(pos_garden_chat_1)
													if not is_ped_injured(zPED[enum_to_int(pos_garden_chat_2)].id)
														TASK_TURN_PED_TO_FACE_ENTITY(null,zPED[enum_to_int(pos_garden_chat_2)].id,2000)
													endif
												elif i = enum_to_int(pos_garden_chat_2)
													if not is_ped_injured(zPED[enum_to_int(pos_garden_chat_1)].id)
														TASK_TURN_PED_TO_FACE_ENTITY(null,zPED[enum_to_int(pos_garden_chat_1)].id,2000)
													endif
												endif
											
												IF zPED[i].intA != 0												
													TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_HANG_OUT_STREET",zPED[i].intA)
												ELSE
													TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_HANG_OUT_STREET")
												ENDIF
											endseq(zPED[i].id)
											zPED[i].actionFlag++
											zPED[i].intB = GET_GAME_TIMER() + 15000
										ENDIF
									BREAK
									case 2
										if GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
										OR GET_GAME_TIMER() > zPED[i].intB
											CLEAR_PED_TASKS(zPED[i].id)
											zPED[i].actionFlag++
										ENDIF
									BREAK
									CASE 3
										if GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
											if zPED[i].lastAction = action_balcony_guard_patrol
												TASK_SWAP_WEAPON(zPED[i].id,TRUE)
												zPED[i].actionFlag++
											ELSE
												zPED[i].actionFlag++
											ENDIF
										ENDIf
									BREAK
									CASE 4
										if GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_SWAP_WEAPON) = FINISHED_TASK
											
											if zPED[i].lastAction = action_balcony_guard_patrol
												set_ped_action(i,zPED[i].lastAction,2)
											ELSE
												set_ped_action(i,zPED[i].lastAction)
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							BREAK						
								
							case action_looking_about
								switch zPED[i].actionFlag	
									case 0
										TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_GUARD_STAND")	
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
										zPED[i].actionFlag++
									break
									
								ENDSWITCH
							BREAK
							case action_smoke
								switch zPED[i].actionFlag	
									case 0
										TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_AA_SMOKE")
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
										//startSeq()
										//TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_d",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										//TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_e",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										///TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_f",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										//endSeq(zPED[i].id,TRUE)
										zPED[i].actionFlag++
									BREAK	
									case 1
										IF GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = FINISHED_TASK
											zPED[i].actionFlag = 0
										ENDIF
									break
									
								ENDSWITCH
							BREAK
							
							case action_binoculars
								switch zPED[i].actionFlag	
									case 0
										TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_BINOCULARS")
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
										//startSeq()
										//TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_d",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										//TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_e",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										///TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_f",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										//endSeq(zPED[i].id,TRUE)
										zPED[i].actionFlag++
									BREAK						
									
								ENDSWITCH
							break
							
							case action_drinking
								switch zPED[i].actionFlag	
									case 0
										TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_DRINKING")
										FORCE_PED_AI_AND_ANIMATION_UPDATE(zPED[i].id)
										//startSeq()
										//TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_d",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										//TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_e",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										///TASK_PLAY_ANIM(NULL, "amb@smoking@standing@male@idle_b", "idle_f",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
										//endSeq(zPED[i].id,TRUE)
										zPED[i].actionFlag++
									BREAK						
									
								ENDSWITCH
							break
							
						
							case action_pissing
							
								//make reaction if ped hears player.
								IF zPED[i].actionFlag < 10
									IF CAN_PED_HEAR_PLAYER(player_id(),zPED[i].id)
										zPED[i].actionFlag = 10
									ENDIF
								ENDIF
							
								switch zPED[i].actionFlag	
									case 0
										REQUEST_ANIM_DICT("misscarsteal2peeing")
										zPED[i].actionFlag++
										zPED[i].intA = 0 //set to -1 later on to indicate than the pissing timer has finished
									BREAK
									CASE 1
										IF HAS_ANIM_DICT_LOADED("misscarsteal2peeing")	
											TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_DRINKING",0,TRUE)										
											zPED[i].actionFlag++
										ENDIF
									BREAK
									CASE 2
										IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),zPED[i].id) < 21.6
											TASK_PLAY_ANIM(zPED[i].id,"misscarsteal2peeing","peeing_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,25000,AF_LOOPING)
											zPED[i].actionFlag++
											zPED[i].intA = GET_GAME_TIMER() + 23000
										ENDIF
									BREAK
									CASE 3
										IF GET_GAME_TIMER() > zPED[i].intA
											zPED[i].intA = -1
											TASK_START_SCENARIO_IN_PLACE(zPED[i].id,"WORLD_HUMAN_DRINKING",0,TRUE)
											zPED[i].actionFlag++
										ENDIF
									BREAK
									/*
									case 10 //reaction for hearing player										
										IF CREATE_CONVERSATION_EXTRA("CHI2_hear",8,zPED[i].id,"ONEILGUARD1")
											zPED[i].actionFlag++
											zPED[i].intB = GET_GAME_TIMER() + 1500
										ENDIF
										
									BREAK
									CASE 11
										IF GET_GAME_TIMER() > zPED[i].intB
											zPED[i].actionFlag++
											zPED[i].intB = GET_GAME_TIMER() + 1500
										ENDIF
									BREAK
									CASE 12
										IF GET_GAME_TIMER() < zPED[i].intB //keep guy pissing for 1.5 seconds then turn if he hears any more
											IF CAN_PED_HEAR_PLAYER(player_id(),zPED[i].id)								
												TASK_TURN_PED_TO_FACE_COORD(zped[i].id,GET_ENTITY_COORDS(player_ped_id()),5000)
												zPED[i].actionFlag++
												zPED[i].intB = GET_GAME_TIMER() + 5000
											ENDIF
										ELSE
											zPED[i].actionFlag=20
											
										ENDIF
									BREAK
									CASE 13
										IF CAN_PED_HEAR_PLAYER(player_id(),zPED[i].id)
										AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[i].id,<<2528.2170, 4986.8818, 43.8933>>) < 10.0
											IF GET_GAME_TIMER() > zPED[i].intB - 3000
												TASK_FOLLOW_NAV_MESH_TO_COORD(zped[i].id,GET_ENTITY_COORDS(player_ped_id()),pedmove_walk)
												zPED[i].actionFlag=15
											ENDIF
										ELSE
											IF GET_GAME_TIMER() > zPED[i].intB						
												zPED[i].actionFlag=20										
											ENDIF
										ENDIF
									BREAK
									
									case 15
										IF CREATE_CONVERSATION_EXTRA("CHI2_hear",8,zPED[i].id,"ONEILGUARD1")
											zPED[i].actionFlag++										
										ENDIF
									BREAK
									CASE 16
										IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK
											vector v1,v2
											v1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(zped[i].id,<<-5,0.5,0>>)
											v2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(zped[i].id,<<5,0.5,0>>)
											////cprintln(debug_Trevor3,v1," ",v2)
										
											startSeq()											
												TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(zped[i].id,<<-5,0.5,0>>),2500)
												TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(zped[i].id,<<5,0.5,0>>),2500)
											endseq(zped[i].id)
											zPED[i].intB = GET_GAME_TIMER() + 5000
											zPED[i].actionFlag=13
										ENDIF
									break
									case 20 //return to task
										IF CREATE_CONVERSATION_EXTRA("CHI2_nope",8,zPED[i].id,"ONEILGUARD1")
											zPED[i].actionFlag++									
										ENDIF	
									break
									CASE 21
										startSeq()
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<2528.2170, 4986.8818, 43.8933>>,pedmove_walk)
										TASK_ACHIEVE_HEADING(null,28.6328,3000)
										endseq(zped[i].id)
										
										zPED[i].actionFlag++
									BREAK
									CASE 22							
										IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK						
											IF zPED[i].intA != -1
												zPED[i].actionFlag = 3
											ELIF zPED[i].intA = 0
												zPED[i].actionFlag = 2
											ELSE
												zPED[i].actionFlag = 4
											ENDIF											
										ENDIF
									BREAK
									*/
								ENDSWITCH
							break
							
							CASE action_move_inside_house
								switch zPED[i].actionFlag	
									case 0
										startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<2434.8911, 4973.8638, 50.5679>>,pedmove_walk)				
											TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_BINOCULARS")
											
											//TASK_START_SCENARIO_IN_PLACE(null,"WORLD_HUMAN_HANG_OUT_STREET")
										endSeq(zPED[i].id)
										zPED[i].actionFlag++
									BREAK
								ENDSWITCH
							BREAK												
							
							case action_sit
								switch zPED[i].actionFlag
									CASE 0
										
										//INT sceneId
										switch zPED[i].placement
											case pos_inside_seated1
												//sceneId = CREATE_SYNCHRONIZED_SCENE(<< 2433.124, 4967.804, 46.293 >>, 135.043 >>)
												IF DOES_SCENARIO_EXIST_IN_AREA(<<2433.04, 4967.87, 46.28>>,0.8,FALSE)
													TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[i].id,<<2433.04, 4967.87, 46.28>>, 0.8)
													zPED[i].actionFlag++
												ENDIF
											break
											case pos_inside_seated2
												IF DOES_SCENARIO_EXIST_IN_AREA(<<2432.56, 4968.38, 46.27>>,0.8,FALSE)
													TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[i].id,<<2432.56, 4968.38, 46.27>>, 0.8)
													zPED[i].actionFlag++
												ENDIF
											break
											case pos_inside_seated3
												IF DOES_SCENARIO_EXIST_IN_AREA(<<2433.12, 4965.59, 46.27>>,0.8,FALSE)
													TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[i].id,<<2433.12, 4965.59, 46.27>>, 0.8)
												
													zPED[i].actionFlag++
												ENDIF
											break
											
											case pos_inside_cards1
												IF DOES_SCENARIO_EXIST_IN_AREA(<<2439.61, 4960.99, 46.27>>,0.4,FALSE)  
													
													TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[i].id,<< 2439.697, 4960.996, 46.28 >>,0.4)
													////cprintln(debug_trevor3,"SEATED A")
													zPED[i].actionFlag++
												ENDIF

												//TASK_START_SCENARIO_AT_POSITION(zped[i].id,"PROP_HUMAN_SEAT_CHAIR",<< 2439.697, 4960.996, 46.323 >>,-0.680)
											break
											case pos_inside_cards2
												IF DOES_SCENARIO_EXIST_IN_AREA(<<2440.16, 4962.99, 46.27>>,0.4,FALSE)
													TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[i].id,<<2440.16, 4962.99, 46.27>>,0.4) //<< 2440.229, 4963.038, 46.323 >>,0.4)
													////cprintln(debug_trevor3,"SEATED B")
													zPED[i].actionFlag++
												ENDIF
												//TASK_START_SCENARIO_AT_POSITION(zped[i].id,"PROP_HUMAN_SEAT_CHAIR",<< 2440.229, 4963.038, 46.323 >>, 134.070)
											break
											case pos_inside_cards3
												IF DOES_SCENARIO_EXIST_IN_AREA(<<2438.23, 4962.25, 46.27>>,0.4,FALSE)
													////cprintln(debug_trevor3,"SEATED C")
													TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(zped[i].id,<<2438.23, 4962.25, 46.27>>,0.4)//<< 2438.562, 4962.325, 46.323 >>,0.4)
													zPED[i].actionFlag++
												ENDIF
												//TASK_START_SCENARIO_AT_POSITION(zped[i].id,"PROP_HUMAN_SEAT_CHAIR",<< 2438.562, 4962.325, 46.323 >>, -93.960)
											break
										endswitch
										//TASK_SYNCHRONIZED_SCENE (zped[i].id, sceneId, "amb@sitchair@male@idle_a", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
										
									break
									case 1
										cprintln(debug_Trevor3,"Check A")
										IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) = FINISHED_TASK
											zPED[i].actionFlag=0
										ENDIF
									break
								ENDSWITCH
							break
							
							
					
							case action_cooking
							//	IF AWARENESS_STATE != UNAWARE
									switch zPED[i].actionFlag
									/*
										CASE 0
											startSeq()
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
											TASK_ACHIEVE_HEADING( null,288.9182)
											TASK_COWER(null,-1)
											endseq(zPED[i].id)
											zPED[i].actionFlag++
										break*/
										
										case 0
											IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),zPED[i].id)
											OR (IS_POINT_VISIBLE(GET_ENTITY_COORDS(zPED[i].id),1.0)
											AND IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2448.583252,4969.291992,45.185604>>, <<2439.538330,4978.061035,46.827618>>, 5.750000))
												TASK_HANDS_UP(zPED[i].id,20000)
												zPED[i].actionFlag=2
											ENDIF
										break
										case 2
											IF CREATE_CONVERSATION_EXTRA("WRK1",1,zPED[i].id,"CHIN2Goon2")
												zPED[i].actionFlag++
											ENDIF
										break
										case 3
											IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),zPED[i].id)
											OR (IS_POINT_VISIBLE(GET_ENTITY_COORDS(zPED[i].id),1.0)
											AND IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2448.583252,4969.291992,45.185604>>, <<2439.538330,4978.061035,46.827618>>, 5.750000))
												IF CREATE_CONVERSATION_EXTRA("TRV1",1,player_ped_id(),"TREVOR")
													zPED[i].actionFlag++
													zPED[i].intA = get_Game_timer() + 2000
												ENDIF
											ELSE
												zPED[i].actionFlag++
												zPED[i].intA = get_Game_timer() + 2000
											ENDIF
										BREAK
										case 4
											if get_Game_timer() > zPED[i].intA
												TASK_SMART_FLEE_PED(zPED[i].id,player_ped_id(),200,200000)
												SET_PED_KEEP_TASK(zPED[i].id,true)
												SET_PED_AS_NO_LONGER_NEEDED(zPED[i].id)
												zPED[i].actionFlag++
											endif
										break
									ENDSWITCH
								//ENDIF
							break
							case action_meth_lab	
							
								IF NOT IS_PED_INJURED(zPED[i].id)
									IF zPED[i].actionFlag < 8
										IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(zPED[i].id,WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYWEAPON)
											
											IF NOT HAS_PED_GOT_WEAPON(zPED[i].id,WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYWEAPON)
												GIVE_WEAPON_TO_PED(zPED[i].id,WEAPONTYPE_PISTOL,-1,false,true)
												TASK_COMBAT_PED(zPED[i].id,player_ped_id())
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zPED[i].id,FALSE)
												TASK_COMBAT_PED(zped[i].id,player_ped_id())
												SET_PED_RELATIONSHIP_GROUP_HASH(zped[i].id,HASH_FOE_GROUP)
												zPED[i].actionFlag = 999
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							
								switch zPED[i].actionFlag
									
									case 0
										REQUEST_ANIM_DICT("misschinese2_crystalmaze")
										REQUEST_WEAPON_ASSET(WEAPONTYPE_SAWNOFFSHOTGUN)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zPED[i].id,TRUE)
										SET_PED_RELATIONSHIP_GROUP_HASH(zped[i].id,HASH_FOE_GROUP)
										zPED[i].actionFlag++						
									BREAK
									CASE 1
										IF HAS_ANIM_DICT_LOADED("misschinese2_crystalmaze")
										AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SAWNOFFSHOTGUN)
											IF NOT IS_PED_INJURED(zPED[i].id)								
												iSyncCower = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)
												SET_SYNCHRONIZED_SCENE_LOOPED(iSyncCower,TRUE)
												TASK_SYNCHRONIZED_SCENE (zPED[i].id,iSyncCower, "misschinese2_crystalmaze", "_cower_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
												IF NOT DOES_ENTITY_EXIST(objShotgun)
													objShotgun = CREATE_WEAPON_OBJECT(WEAPONTYPE_SAWNOFFSHOTGUN,40,<<2433.59, 4969.70, 42.1854>>,true)
													CPRINTLN(debug_trevor3,"Created shotgun")
													SET_ENTITY_ROTATION(objShotgun,<<90,0,0>>)
												ENDIF
												TASK_LOOK_AT_ENTITY(zPED[i].id,player_ped_id(),-1)
	
												zped[i].actionFlag++
											ENDIF
										ENDIF
									BREAK
									
									case 2 
										IF missionStage = snipe_from_hill
										OR missionStage = get_to_house
										OR missionstage = inside_house										
											IF IS_CONDITION_TRUE(COND_PLAYER_IN_METH_LAB)
												zped[i].actionFlag++
											ENDIF
										ENDIF
										
										IF missionstage > inside_house
											zped[i].actionFlag++
										ENDIF
									break
									case 3
										iSyncStandUp = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)										
										TASK_SYNCHRONIZED_SCENE (zPED[i].id,iSyncStandUp, "misschinese2_crystalmaze", "_cower_to_stand", SLOW_BLEND_IN, NORMAL_BLEND_OUT )
										TASK_LOOK_AT_ENTITY(zPED[i].id,PLAYER_PED_ID(),-1)
										zped[i].actionFlag++
									break
									case 4
										IF CREATE_CONVERSATION_EXTRA("CHI2_DALE",4,zPED[i].id,"ONEIL")
											zped[i].actionFlag++
										ENDIF																				
										
										//in case the dialogue can't play for some reason...
										IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSyncStandUp) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncStandUp) > 0.98)
										OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncStandUp)
											zped[i].actionFlag = 5
										ENDIF
									break
									case 5
										IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSyncStandUp) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncStandUp) > 0.98)
										OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncStandUp)											
											iSyncStandingLoop = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)												
											SET_SYNCHRONIZED_SCENE_LOOPED(iSyncStandingLoop,TRUE)
											TASK_SYNCHRONIZED_SCENE (zPED[i].id,iSyncStandingLoop, "misschinese2_crystalmaze", "_stand_loop", SLOW_BLEND_IN, NORMAL_BLEND_OUT )
											zped[i].actionFlag++																																										
											zped[i].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000,2000)											
										ENDIF
									break
									case 6
										
										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iStandAlternative)
										OR iStandAlternative = 0
											IF GET_GAME_TIMER() > zped[i].intA
												iStandAlternative = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)																							
												IF GET_RANDOM_INT_IN_RANGE(0,2) = 1
													TASK_SYNCHRONIZED_SCENE (zPED[i].id,iStandAlternative, "misschinese2_crystalmaze", "_stand_loop_a", SLOW_BLEND_IN, NORMAL_BLEND_OUT )
												ELSE
													TASK_SYNCHRONIZED_SCENE (zPED[i].id,iStandAlternative, "misschinese2_crystalmaze", "_stand_loop_b", SLOW_BLEND_IN, NORMAL_BLEND_OUT )
												ENDIF
											//	zped[i].actionFlag++																																										
											//	zped[i].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,3000)
											ENDIF
										ENDIF
										
										IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncStandingLoop) 
										OR iSyncStandingLoop = 0
											IF (IS_SYNCHRONIZED_SCENE_RUNNING(iStandAlternative) AND GET_SYNCHRONIZED_SCENE_PHASE(iStandAlternative) > 0.98)
											OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iStandAlternative)
												iSyncStandingLoop = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)												
												SET_SYNCHRONIZED_SCENE_LOOPED(iSyncStandingLoop,TRUE)
												TASK_SYNCHRONIZED_SCENE (zPED[i].id,iSyncStandingLoop, "misschinese2_crystalmaze", "_stand_loop", SLOW_BLEND_IN, NORMAL_BLEND_OUT )																																								
												zped[i].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000,2000)
											ENDIF
										ENDIF
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										OR (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2430.09, 4965.93, 41.55>>) > 3.438 AND NOT IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2427.078857,4964.970703,41.811687>>, <<2432.588867,4959.331055,48.754795>>, 3.312500))
											KILL_FACE_TO_FACE_CONVERSATION()
											zped[i].actionFlag++
										ENDIF
									break
									case 7
										IF CREATE_CONVERSATION_EXTRA("CHI2_DALEG",4,zPED[i].id,"ONEIL")
											zped[i].actionFlag++
										ENDIF
									break
									case 8
										iGrabGun = CREATE_SYNCHRONIZED_SCENE(<< 2435.312, 4967.087, 41.350 >>, << -0.000, 0.000, 11.400 >>)																		
										TASK_SYNCHRONIZED_SCENE (zPED[i].id,iGrabGun, "misschinese2_crystalmaze", "_stand_to_aim", SLOW_BLEND_IN, NORMAL_BLEND_OUT )																										
										GIVE_WEAPON_OBJECT_TO_PED(objShotgun,zPED[i].id)
										
										zped[i].actionFlag++
									break
									case 9
										IF (IS_SYNCHRONIZED_SCENE_RUNNING(iGrabGun) AND GET_SYNCHRONIZED_SCENE_PHASE(iGrabGun) > 0.98)
										OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iGrabGun)											
											SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
											CLEAR_PED_TASKS(zPED[i].id)
											TASK_AIM_GUN_AT_ENTITY(zPED[i].id,player_ped_id(),-1,TRUE)
											zped[i].actionFlag++
										ENDIF
									break
									CASE 10
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION_EXTRA("CHI2_DALEA",4,zPED[i].id,"ONEIL")
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(zPED[i].id,FALSE)
												TASK_COMBAT_PED(zped[i].id,player_ped_id())
												
												zped[i].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,4000)
												zped[i].intB=0
												zped[i].actionFlag++
											ENDIF											
										ENDIF
									BREAK
									CASE 11
										IF GET_GAME_TIMER() > zped[i].intA
											IF zped[i].intB < 4
												IF CREATE_CONVERSATION_EXTRA("CHI2_DALEA",4,zPED[i].id,"ONEIL")
													zped[i].actionFlag++
													zped[i].intA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000,4000)
													zped[i].intB++
												ENDIF										
											ENDIf
										ENDIF
									BREAK
									case 999
										IF CREATE_CONVERSATION_EXTRA("CHI2_DALEA",4,zPED[i].id,"ONEIL")
											zped[i].actionFlag++
											
										ENDIF
									break
								ENDSWITCH		
								/*
								IF zPED[i].actionFlag != CLEANUP
									int iCard3
									iCard3 = get_ped_with_placement(pos_inside_cards3)
									If iCard3 = -1
										iCard3 = 0
									ENDIF
									IF GET_ROOM_KEY_FROM_ENTITY(player_ped_id()) = GET_HASH_KEY("V_8_BasementRm")						
									AND IS_PED_INJURED(zped[iCard3].id)
									OR get_ped_with_placement(pos_inside_cards3) = -1					
										IF zPED[i].actionFlag = 2
											set_ped_action(get_ped_with_action(action_meth_lab),action_meth_lab_respond_to_player)
										endif
									endif
								endif*/
							break		
							
							case action_meth_lab_respond_to_player
								IF NOT IS_PED_INJURED(zPED[i].id)
									IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(zPED[i].id),2.0,TRUE)								
										zPED[i].actionFlag = 5
									ENDIF
								ENDIF
								switch zPED[i].actionFlag
									case 0
										IF IS_THIS_PRINT_BEING_DISPLAYED("FRMFLC")
											zPED[i].actionFlag++
										ENDIF
									BREAK
									CASE 1
										if CREATE_CONVERSATION_EXTRA("WRK2",1,zPED[i].id,"oneilcook",2,player_ped_id(),"trevor")	
												//////cprintln(DEBUG_TREVOR3,"REACT TO TREVOR : ",GET_GAME_TIMER())
											zPED[i].actionFlag++
											zPED[i].intA = get_game_timer() + 1300
										endif
									break
									case 2
										IF get_game_timer() > zPED[i].intA
											TASK_HANDS_UP(zPED[i].id,20000)
											zPED[i].actionFlag++	
										ENDIF
									BREAK
									CASE 3
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											zPED[i].actionFlag++								
										endif 
									break
									case 4
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											//////cprintln(DEBUG_TREVOR3,"Meth worker flees cos conversatonn with player over")
											set_ped_action(i,action_flee)
										ENDIF
									break
									case 5 //player shoots near cook
										TASK_SMART_FLEE_PED(zPED[i].id,player_ped_id(),50,1000)
										zPED[i].actionFlag++
									break
									case 6
										IF IS_CONV_ROOT_PLAYING("WRK2")
											IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = 1
											OR GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = 5
												////cprintln(DEBUG_TREVOR3,"A")
												PURGE_CONVERSATIONS(FALSE)																
											ELSE
												////cprintln(DEBUG_TREVOR3,"B")
												PURGE_CONVERSATIONS(TRUE)							
											ENDIF									
										ENDIF
										set_ped_action(i,action_flee)								
									break
								ENDSWITCH
							break
							
							case action_flee
								switch zPED[i].actionFlag
									CASE 0
										startSeq()
										TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
										TASK_SMART_FLEE_PED(null,player_ped_id(),200.0,999999)
										task_cower(null,-1)
										endseq(zPED[i].id)
										SET_PED_KEEP_TASK(zPED[i].id,true)
										SET_PED_AS_NO_LONGER_NEEDED(zPED[i].id)
										IF DOES_BLIP_EXIST(zPED[i].blip)
											REMOVE_BLIP(zPED[i].blip)
										endif
										zPED[i].actionFlag++
									break
								ENDSWITCH
							break
							
							case action_shoot_cans							
								
								/*
								int iC
								REPEAT COUNT_OF(oCans) iC
									IF DOES_ENTITY_EXIST(oCans[iC])
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(oCans[iC],player_ped_id())
											set_ped_action(i,action_alerted)
										ENDIF
									ENDIF
								ENDREPEAT
								*/
								IF bForceAnimTimeA = TRUE
									IF IS_SYNCHRONIZED_SCENE_RUNNING(zPED[i].intA)										
										SET_SYNCHRONIZED_SCENE_PHASE(zPED[i].intA,0.69)
									ENDIF
									
									IF i = enum_to_int(pos_shooting_bottle_2)
										bForceAnimTimeA = FALSE
									ENDIF
								ENDIF
								
								IF bForceAnimTimeB = TRUE
									IF IS_SYNCHRONIZED_SCENE_RUNNING(zPED[i].intA)										
										SET_SYNCHRONIZED_SCENE_PHASE(zPED[i].intA,0.0)									
									ENDIF
									
									IF i = enum_to_int(pos_shooting_bottle_2)
										bForceAnimTimeB = FALSE
									ENDIF
									zPED[i].actionFlag = 0
								ENDIF
								
								IF zPed[i].placement = pos_shooting_bottle_1							
									If IS_SYNCHRONIZED_SCENE_RUNNING(zPED[i].intA)
										IF zPED[i].actionFlag = 0
											IF NOT is_event_active(events_spawn_bottles)
											OR missionStage = farm_cutscene
												IF GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
													IF GET_SYNCHRONIZED_SCENE_PHASE(zPED[i].intA) > 0.024
													AND GET_SYNCHRONIZED_SCENE_PHASE(zPED[i].intA) < 0.033
													//	IF IS_POINT_VISIBLE(GET_ENTITY_COORDS(zPED[i].id),5.0,100) //commented out because gun won't shoot if not on screen
													//	OR IS_POINT_VISIBLE(<<2494.2041, 4970.2690, 44.5201>>,5.0,100)
															int iCanToHit
															iCanToHit = 0
															IF NOT IS_PED_INJURED(zPED[i].id)
																weapon_type HisWep
																HisWep = weapontype_unarmed
																IF GET_CURRENT_PED_WEAPON(zPED[i].id,HisWep)
																
																	SET_PED_ACCURACY(zPED[i].id,100)
																	IF HisWep = WEAPONTYPE_PISTOL
																		IF FIND_CAN_TO_HIT(iCanToHit)
																			//IF HAS_PED_GOT_FIREARM(zPED[i].id)	
																			cprintln(debug_trevor3,"BANG")
																				SET_AMMO_IN_CLIP(zPED[i].id,WEAPONTYPE_PISTOL,12)
																				SET_PED_SHOOTS_AT_COORD(zPED[i].id,GET_ENTITY_COORDS(oCans[iCanToHit]))
																				UPDATE_AI_PED_BLIP(zPED[i].id,zped[i].aiBlip,-1,null,true)
																				//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<< 2502.0161, 4970.4639, 44.9824 >>,GET_ENTITY_COORDS(oCans[iCanToHit]),5,TRUE,WEAPONTYPE_PISTOL,zped[i].id)
																			//ENDIF
																			zPED[i].actionFlag = 1
																		ELSE
																			//IF HAS_PED_GOT_FIREARM(zPED[i].id)	
																				SET_AMMO_IN_CLIP(zPED[i].id,WEAPONTYPE_PISTOL,12)															
																				SET_PED_SHOOTS_AT_COORD(zPED[i].id,<<2493.1,4971,44.9>>)
																				cprintln(debug_trevor3,"BANG B")
																				UPDATE_AI_PED_BLIP(zPED[i].id,zped[i].aiBlip,-1,null,true)
																			//	SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<< 2502.0161, 4970.4639, 44.9824 >>,GET_ENTITY_COORDS(oCans[0]),5,TRUE,WEAPONTYPE_PISTOL,zped[i].id)
																				zPED[i].actionFlag = 1
																			//ENDIF
																		ENDIF
																	ENDIf
																ENDIF
															ENDIF
														//ELSE
														//	zPED[i].actionFlag = 1
														//ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELIF zPED[i].actionFlag = 1
											IF GET_SYNCHRONIZED_SCENE_PHASE(zPED[i].intA) > 0.04
												//check if any more cans in area
											//	int iAnyMore
											//	iAnyMore = 0
											//	IF NOT FIND_CAN_TO_HIT(iAnyMore)
											//		zPED[i].actionFlag = 2
											//	ELSE
													zPED[i].actionFlag = 2
													IF bBlockShooterDialogue = FALSE
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zPED[i].id, "generic_cheer", "A_M_M_Hillbilly_01_White_mini_02", "SPEECH_PARAMS_FORCE_SHOUTED")
													ENDIF
													////cprintln(debug_Trevor3,"cheer")
													//PLAY_PED_AMBIENT_SPEECH_NATIVE(zPED[i].id,"GENERIC_CHEER_01","SPEECH_PARAMS_FORCE_NORMAL") 
													
													
											//	ENDIF
											ENDIF
										elif zPED[i].actionFlag = 2
											IF GET_SYNCHRONIZED_SCENE_PHASE(zPED[i].intA) > 0.6
												zPED[i].actionFlag = 0
											endif
										ENDIF
							
									ENDIF
								ENDIF
								IF zPED[i].actionFlag = 3
									IF GET_SCRIPT_TASK_STATUS(zPED[i].id,SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
										set_ped_action(i,action_chatting)
									ENDIF
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				case STATE_SEEK
				fallthru
				case STATE_COMBAT
					
					iF pedReactionState[i].scriptControlledCombat
					//	IF zPED[i].placement = pos_inside_right_window
					//	OR zPED[i].placement = pos_inside_kitchen1
					//	OR zPED[i].placement = pos_inside_kitchen2
					//	OR zPED[i].placement = pos_inside_utility_room
					//		////cprintln(debug_trevor3," i = ",i," act flag = ",pedReactionState[i].actionFlag)
					//	endif
						switch zPED[i].placement
							case pos_inside_bystairs1
							FALLTHRU
							CAse pos_inside_bystairs2
							Fallthru						
							case pos_inside_cards2
							fallthru						
							case pos_inside_seated1
							fallthru
							case pos_inside_seated2
							fallthru
							case pos_inside_seated3						
								switch pedReactionState[i].actionFlag
									case 1	
									//	SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2431.704102,4961.619629,45.331356>>, <<2455.376465,4985.933594,48.685551>>, 13.562500)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_CAN_CHARGE,TRUE)
										pedReactionState[i].actionFlag++
									BREAK
								ENDSWITCH
							BREAK
							
							case pos_inside_cards3 //in basement
								switch pedReactionState[i].actionFlag
									case 1
									//	SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2431.704102,4961.619629,45.331356>>, <<2455.376465,4985.933594,48.685551>>, 13.562500)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_CAN_CHARGE,TRUE)
										pedReactionState[i].actionFlag++
									//	IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_START_SCENARIO_AT_POSITION) = PERFORMING_TASK
										//	CLEAR_PED_TASKS_IMMEDIATELY(zped[i].id)
										//	SET_ENTITY_COORDS(zped[i].id,<<2437.5564, 4962.0776, 45.8106>>)
										/*	SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2429.921631,4968.741211,40.597553>>, <<2433.283203,4972.045898,43.873558>>, 5.312500)
											startSeq()
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2432.2642, 4959.6460, 45.8131 >>,pedmove_walk)
											TASK_GO_STRAIGHT_TO_COORD(null,<< 2427.9617, 4964.3984, 42.9630 >>,pedmove_walk)
											TASK_COMBAT_PED(null,PLAYER_PED_ID())
											
											endseq(zped[i].id)
											SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
											SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_CAN_CHARGE,TRUE)
											pedReactionState[i].actionFlag++*/
									//	ENDIF
									break
									
								ENDSWITCH
							break
							
							case pos_inside_cards1 //behind sofa
								switch pedReactionState[i].actionFlag
									case 1
									//	IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_START_SCENARIO_AT_POSITION) = PERFORMING_TASK
									//		SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id,<<2437.981934,4959.020996,45.310574>>, <<2432.929932,4961.941406,48.692562>>, 3.187500)
											TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
											SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
											pedReactionState[i].actionFlag++										
									//	ENDIF
									break
									case 2
										IF IS_CONDITION_TRUE(COND_TREVOR_NEAR_STAIRS)
											REMOVE_PED_DEFENSIVE_AREA(zPED[i].id)
											pedReactionState[i].actionFlag++
										ENDIF
									break
								endswitch
							BREAK
								/*
								switch pedReactionState[i].actionFlag
									case 1							
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2447.756592,4981.289063,44.934750>>, <<2446.409180,4982.652832,48.309746>>, 2.375000)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									break
									case 2
										IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2452.965820,4985.248047,45.591969>>, <<2448.317871,4990.217773,47.936779>>, 3.500000)
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id,<<2443.332520,4981.746582,45.497406>>, <<2446.810059,4978.089844,48.271076>>, 2.687500)
											pedReactionState[i].actionFlag++
										ENDIF
										IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2455.334473,4970.357422,45.498104>>, <<2451.068359,4974.604492,48.247440>>, 4.625000)									
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2448.737061,4975.442871,44.955708>>, <<2454.806641,4969.708008,48.413578>>, 2.375000)				
											pedReactionState[i].actionFlag++
										ENDIF
									BREAK
									case 3
										IF IS_ENTITY_IN_ANGLED_AREA( zped[i].id, <<2445.876709,4978.528809,44.948814>>, <<2443.826660,4980.604492,48.310200>>, 2.375000)
											pedReactionState[i].actionFlag++
										ENDIF
									BREAK
									CASE 4
										SET_PED_MAX_MOVE_BLEND_RATIO(zped[i].id,PEDMOVE_WALK)
									BREAK
									
								ENDSWITCH
							break
							case pos_inside_bystairs2 //goes in to bathroom
								switch pedReactionState[i].actionFlag
									case 1												
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2449.954590,4977.896973,45.131977>>, <<2451.540039,4976.274902,48.247337>>, 1.875000)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									break
									case 2										
										IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2442.321289,4982.283203,45.497093>>, <<2446.957031,4977.627930,48.247665>>, 2.187500)
										OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2455.334473,4970.357422,45.498104>>, <<2451.068359,4974.604492,48.247440>>, 4.625000)
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id,<<2445.289063,4969.894043,43.998104>>, <<2448.634521,4973.506348,48.248104>>, 6.000000)
											pedReactionState[i].actionFlag=3
										ENDIF
										IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2435.310791,4986.616699,44.128063>>, <<2423.034424,4974.076172,47.449322>>, 15.875000)
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2438.158936,4980.710449,44.701595>>, <<2432.191406,4974.761719,48.014095>>, 3.437500)
											pedReactionState[i].actionFlag=4
										ENDIF
									BREAK
									case 4
										IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2435.310791,4986.616699,44.128063>>, <<2423.034424,4974.076172,47.449322>>, 15.875000)
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2449.954590,4977.896973,45.131977>>, <<2451.540039,4976.274902,48.247337>>, 1.875000)
											pedReactionState[i].actionFlag=2
										ENDIF
									break
								ENDSWITCH										
							break
							
							case pos_inside_cards2 //behind wall
								switch pedReactionState[i].actionFlag
									case 1
									//	IF GET_SCRIPT_TASK_STATUS(zped[i].id,SCRIPT_TASK_START_SCENARIO_AT_POSITION) = PERFORMING_TASK
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2433.289307,4962.226563,44.939003>>, <<2434.555176,4960.793457,47.377789>>, 3.125000)
											TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
											SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
											pedReactionState[i].actionFlag++									
									//	ENDIF
									break
								endswitch
							break
							
								
							case pos_inside_seated1
								switch pedReactionState[i].actionFlag
									case 1
									//	CLEAR_PED_TASKS_IMMEDIATELY(zped[i].id)
									//	SET_ENTITY_COORDS(zped[i].id,<< 2431.0010, 4965.7803, 45.8106 >>)
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2432.015137,4965.860840,44.935604>>, <<2433.186523,4964.659668,47.310604>>, 1.937500)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									break
								ENDSWITCH
							break	
							case pos_inside_seated2
								switch pedReactionState[i].actionFlag
									case 1
										
									//	CLEAR_PED_TASKS_IMMEDIATELY(zped[i].id)
									//	SET_ENTITY_COORDS(zped[i].id,<< 2429.3208, 4967.4536, 45.8106 >>)
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2449.900146,4971.117188,44.935604>>, <<2448.908447,4970.079590,48.310604>>, 1.25000)
										SET_PED_DEFENSIVE_AREA_DIRECTION(zped[i].id,<< 2571.9097, 4981.6089, 50.6529 >>)
										SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(zPed[i].id)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									break
									case 2
										IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2449.354248,4969.492676,44.748611>>, <<2454.811768,4964.115723,48.136688>>, 4.875000)
											startSeq()
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,TRUE)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<< 2448.1270, 4971.7231, 45.8106 >>,player_ped_id(),pedmove_run,true,0.2,0.4)
											TASK_SHOOT_AT_ENTITY(null,PLAYER_PED_ID(),4000,FIRING_TYPE_CONTINUOUS)																										
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,FALSE)
											endSeq(zped[i].id)
											pedReactionState[i].actionFlag++
										ENDIF
									break																						
									case 3
										IF GET_SCRIPT_TASK_STATUS(zped[i].id,script_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
										OR IS_PED_IN_COMBAT(zped[i].id)
											SET_PED_ANGLED_DEFENSIVE_AREA(zped[i].id, <<2444.830566,4970.917480,44.935604>>, <<2447.187744,4969.138672,48.310604>>, 2.375000)
											TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
											pedReactionState[i].actionFlag++
										ENDIF
									break
								ENDSWITCH
							break	
							//case pos_inside_seated3
							//	switch pedReactionState[i].actionFlag
							//		case 0												
							//			SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2436.569580,4960.183594,44.935604>>, <<2438.262451,4958.681152,46.827202>>, 1.625000)
							//			pedReactionState[i].actionFlag++
							//		break
							//	ENDSWITCH
							//br/eak	
							case pos_inside_seated3
								switch pedReactionState[i].actionFlag
									case 1
									//	CLEAR_PED_TASKS_IMMEDIATELY(zped[i].id)
									//	set_entity_coords(zped[i].id,<< 2435.2253, 4965.4336, 45.8106 >>)
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2439.414063,4972.592773,44.935604>>, <<2440.392090,4971.685547,47.373104>>, 1.625000)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									break
									case 2
										//int iSeated 
										//iSeated = get_ped_with_placement(pos_inside_seated2)
										IF (IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2441.253906,4979.531738,44.935604>>, <<2449.545654,4971.246094,47.373104>>, 1.812500)
										AND IS_POINT_VISIBLE(<< 2443.1968, 4972.5894, 47.2911 >>,2.0))
										OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<2448.055420,4968.519531,44.935604>>, <<2439.032715,4978.250977,47.495625>>, 2.875000)												
											pedReactionState[i].actionFlag++
											SET_PED_COMBAT_ATTRIBUTES(zped[i].id,CA_CLEAR_PRIMARY_DEFENSIVE_AREA_WHEN_REACHED,TRUE)
											SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2442.379883,4973.768555,44.935604>>, <<2440.732910,4975.336426,47.310604>>, 1.625000)
										ENDIF
									break
								endswitch
							break*/
							case pos_inside_right_window
							FALLTHRU
							case pos_inside_kitchen1
							FALLTHRU
							case pos_inside_kitchen2
							FALLTHRU
							case pos_inside_utility_room
								switch pedReactionState[i].actionFlag
									case 1
										////cprintln(debug_trevor3,"peds inside to outside")
										
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_CAN_CHARGE,TRUE)
										//SET_PED_COMBAT_ATTRIBUTES(zPED[i].id,CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,TRUE)
										IF iStagger = 0
											REMOVE_PED_DEFENSIVE_AREA(zped[i].id)
											SET_PED_SPHERE_DEFENSIVE_AREA(zPED[i].id,<<2451.6775, 4954.2515, 44.9880>>,3.0)
											iStagger++
											pedReactionState[i].actionFlag++
										ELSE
											pedReactionState[i].actionFlag=GET_GAME_TIMER() + (iStagger * 4000)
											iStagger++
										ENDIF
										
										zped[i].role = role_guard_outside
									BREAK
									CASE 2
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zPED[i].id,<<2451.6775, 4954.2515, 44.9880>>) < 3.0
											REMOVE_PED_DEFENSIVE_AREA(zPED[i].id)
											
										
											pedReactionState[i].actionFlag++
										ENDIF
									BREAK
									CASE 3
									BREAK
									default
										IF GET_GAME_TIMER() > pedReactionState[i].actionFlag
											REMOVE_PED_DEFENSIVE_AREA(zped[i].id)
											SET_PED_SPHERE_DEFENSIVE_AREA(zPED[i].id,<<2451.6775, 4954.2515, 44.9880>>,3.0)
											pedReactionState[i].actionFlag=2
										ENDIF
									break
								ENDSWITCH
							BREAK
							
							
			
							CASE pos_balcony_doorway	
								switch pedReactionState[i].actionFlag
									case 1
										SET_PED_ANGLED_DEFENSIVE_AREA( zPED[i].id, <<2453.083984,4969.896973,50.442852>>, <<2460.995605,4977.821777,53.380352>>, 2.937500)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									BREAK
								ENDSWITCH
							BREAK
							CASE pos_balcony_far_left										
								switch pedReactionState[i].actionFlag
									case 1
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2442.963379,4964.217285,50.442852>>, <<2449.250732,4970.534180,53.380352>>, 2.000000)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									BREAK
								ENDSWITCH																	
							BREAK
							CASE pos_balcony_veranda_walk	
								switch pedReactionState[i].actionFlag
									case 1
										////cprintln(DEBUG_TREVOR3,"pos_balcony_veranda_walk : DEF AREA SET")
										SET_PED_ANGLED_DEFENSIVE_AREA( zped[i].id, <<2461.362305,4976.083984,50.442852>>, <<2448.743164,4988.774414,53.601433>>, 2.000000)
										TASK_COMBAT_PED(zped[i].id,PLAYER_PED_ID())
										SET_PED_TARGET_LOSS_RESPONSE(zPED[i].id,TLR_NEVER_LOSE_TARGET)
										pedReactionState[i].actionFlag++
									BREAK
								ENDSWITCH
							BREAK
						ENDSWITCH
					endif
				break
			ENDSWITCH

		ENDIF
	ENDREPEAT
ENDPROC



proc manage_peds()
	IF missionStage >= farm_cutscene				
		IF missionStage > farm_cutscene
			handlePedReactions(pedReactionState,vDefAreas,diHearPlayer,diHearPlayerAgain,diLostPlayer,diBullet,diSeePlayer,diFoundPlayer,diAlertAllSeen,diAlertAllUnSeen,diDeadBody,diExplosion)
		ENDIF
		//RESPOND_TO_ALERTS()
		CONTROL_PED_ACTIONS()
	ENDIF
endproc



	// =================================================== MISSION PROGRES =====================================

	proc STAGE_ENDS(mission_stage_flag nextStage = stage_null)
		RESET_INSTRUCTIONS()
		RESET_CONDITIONS()
		RESET_ACTIONS()
		RESET_DIALOGUE()
		
		IF nextStage = stage_null
			missionStage =  int_to_enum(mission_stage_flag,ENUM_TO_INT(missionStage)+1)
		ELSE
			missionStage = nextStage
		ENDIF
	ENDPROC

	proc stage_init()
		SWITCH stageFlag
			case 0
				#if IS_DEBUG_BUILD
				SkipMenuStruct[0].sTxtLabel = "init_mission"  
				SkipMenuStruct[1].sTxtLabel = "intro_cut (chinese_2_int)"  
				SkipMenuStruct[2].sTxtLabel = "drive_to_farm"
				SkipMenuStruct[3].sTxtLabel = "farm_cutscene"
				SkipMenuStruct[4].sTxtLabel = "snipe_from_hill"
				SkipMenuStruct[5].sTxtLabel = "get_to_house" 
				SkipMenuStruct[6].sTxtLabel = "inside_house" 
				SkipMenuStruct[7].sTxtLabel = "do_petrol_trail" 
				SkipMenuStruct[8].sTxtLabel = "explosion_cut" 
				SkipMenuStruct[9].sTxtLabel = "end_mission" 
				#ENDIF
				REQUEST_ADDITIONAL_TEXT("FRMCHSE", MISSION_TEXT_SLOT)
				WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
					safewait(27)
				ENDWHILE
				ADD_PED_FOR_DIALOGUE(dChinese2, 0, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(dChinese2, 8, NULL, "PED8")
				
				
			
				add_event(events_control_dispatch,TRUE)			
				add_event(events_fails,TRUE)
				add_Event(events_never_do_locates_skips,TRUE)
		//		add_event(events_create_bullet_collision_on_house,TRUE)
				
				INIT_REL_GROUPS()
				STOP_FIRE_IN_RANGE(<< 2442.6897, 4970.3477, 46.3300 >>,30.0)
				
				SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY",120)	
				 
	cellar_explosions[0] = <<2428.47, 4968.91, 42.11>>
	cellar_explosions[1] = <<2431.14, 4972.06, 42.19>>
	cellar_explosions[2] = <<2433.05, 4969.22, 42.26>>
	cellar_explosions[3] = <<2437.71, 4968.13, 42.45>>
	cellar_explosions[4] = <<2436.12, 4964.76, 42.19>>
	cellar_explosions[5] = <<2432.13, 4961.97, 41.35>>
	cellar_explosions[6] = <<2430.07, 4963.78, 41.35>>		
	cellar_explosions[7] = <<2434.00, 4963.04, 41.53>>
	cellar_explosions[8] = <<2435.63721, 4969.69922, 42.19639>>


	gasTrail[0].coord = <<2450.3765, 4955.8296, 43.9394>>
	gasTrail[1].coord = <<2449.8149, 4956.6782, 43.9894>>
	gasTrail[2].coord = <<2449.6060, 4957.6753, 44.0646>>
	gasTrail[3].coord = <<2449.7390, 4958.7314, 44.1544>>
	gasTrail[4].coord = <<2450.1409, 4959.7451, 44.2487>>
	gasTrail[5].coord = <<2450.6851, 4960.5815, 44.3219>>
	gasTrail[6].coord = <<2451.4924, 4961.3330, 44.4439>>
	gasTrail[7].coord = <<2452.1736, 4962.0986, 44.5787>>
	gasTrail[8].coord = <<2452.7166, 4962.9058, 44.9141>>
	gasTrail[9].coord = <<2453.3499, 4963.5786, 45.3558>>
	gasTrail[10].coord = <<2453.7539, 4964.5654, 45.5766>>
	gasTrail[11].coord = <<2453.2256, 4965.5391, 45.5766>>
	gasTrail[12].coord = <<2452.4724, 4966.2930, 45.5766>>
	gasTrail[13].coord = <<2451.8691, 4967.1040, 45.5766>>
	gasTrail[14].coord = <<2451.5291, 4968.0825, 45.5766>>
	gasTrail[15].coord = <<2452.3271, 4968.7124, 45.5766>>
	gasTrail[16].coord = <<2452.7961, 4969.6426, 45.8107>>
	gasTrail[17].coord = <<2453.2607, 4970.5410, 45.8106>>
	gasTrail[18].coord = <<2453.5803, 4971.5483, 45.8104>>
	gasTrail[19].coord = <<2452.8076, 4972.2666, 45.8306>>
	gasTrail[20].coord = <<2452.0493, 4972.9282, 45.8306>>
	gasTrail[21].coord = <<2451.2407, 4973.5991, 45.8306>>
	gasTrail[22].coord = <<2450.2480, 4973.8887, 45.8306>>
	gasTrail[23].coord = <<2449.2073, 4973.7837, 45.8105>>
	gasTrail[24].coord = <<2448.1724, 4973.8140, 45.8106>>
	gasTrail[25].coord = <<2447.1526, 4973.7715, 45.8106>>
	gasTrail[26].coord = <<2446.1379, 4973.7241, 45.8106>>
	gasTrail[27].coord = <<2445.1282, 4973.6895, 45.8106>>
	gasTrail[28].coord = <<2444.2559, 4973.0557, 45.8106>>
	gasTrail[29].coord = <<2443.6694, 4972.1938, 45.8106>>
	gasTrail[30].coord = <<2443.1191, 4971.2749, 45.8106>>
	gasTrail[31].coord = <<2442.6606, 4970.3594, 45.8106>>
	gasTrail[32].coord = <<2442.2668, 4969.4253, 45.8106>>
	gasTrail[33].coord = <<2441.7085, 4968.5024, 45.8306>>
	gasTrail[34].coord = <<2441.2637, 4967.5337, 45.8106>>
	gasTrail[35].coord = <<2440.8728, 4966.5376, 45.8106>>
	gasTrail[36].coord = <<2440.3818, 4965.6343, 45.8106>>
	gasTrail[37].coord = <<2439.7104, 4964.8823, 45.8106>>
	gasTrail[38].coord = <<2438.9329, 4964.1455, 45.8106>>
	gasTrail[39].coord = <<2438.1663, 4963.5024, 45.8106>>
	gasTrail[40].coord = <<2437.5554, 4962.7095, 45.8106>>
	gasTrail[41].coord = <<2436.9011, 4961.9502, 45.8106>>
	gasTrail[42].coord = <<2436.0752, 4961.2573, 45.8106>>
	gasTrail[43].coord = <<2435.0796, 4961.0039, 45.8118>>
	gasTrail[44].coord = <<2434.0664, 4960.7134, 45.8181>>
	gasTrail[45].coord = <<2433.0449, 4960.5938, 45.8192>>
	gasTrail[46].coord = <<2432.0647, 4960.2495, 45.8130>>
	gasTrail[47].coord = <<2431.2329, 4960.8091, 45.8089>>
	gasTrail[48].coord = <<2430.5747, 4961.5737, 45.5917>>
	gasTrail[49].coord = <<2429.8887, 4962.0278, 44.9345>>
	gasTrail[50].coord = <<2429.2600, 4962.5947, 44.2774>>
	gasTrail[51].coord = <<2428.6914, 4963.2793, 43.6202>>
	gasTrail[52].coord = <<2428.0730, 4963.9106, 42.9631>>
	gasTrail[53].coord = <<2428.2388, 4964.9219, 42.9631>>
	gasTrail[54].coord = <<2428.9614, 4965.4546, 42.3059>>
	gasTrail[55].coord = <<2429.4500, 4966.2236, 41.8679>>
	gasTrail[56].coord = <<2430.0728, 4966.8198, 41.3476>>
	gasTrail[57].coord = <<2431.0969, 4967.0430, 41.3476>>
	gasTrail[58].coord = <<2431.8904, 4966.4028, 41.3476>>
	gasTrail[59].coord = <<2432.7676, 4965.9019, 41.3476>>
	gasTrail[60].coord = <<2433.7761, 4965.6133, 41.3476>>
	
	
		CutEvent[0].name = "Table Flames"
		CutEvent[0].triggered = FALSE
		CutEvent[1].name = "Table Explosion"
		CutEvent[1].triggered = FALSE
		CutEvent[2].name = "House Explosion"
		CutEvent[2].triggered = FALSE
		CutEvent[3].name = "Cam Shake"
		CutEvent[3].triggered = FALSE
		CutEvent[4].name = "Position player"
		CutEvent[4].triggered = FALSE
		CutEvent[5].name = "Slow mo"
		CutEvent[5].triggered = FALSE
		
		CutEvent[0].triggerShot	=	2				
CutEvent[0].triggerTime	=	1000				
CutEvent[1].triggerShot	=	2				
CutEvent[1].triggerTime	=	4200				
CutEvent[2].triggerShot	=	3				
CutEvent[2].triggerTime	=	0				
CutEvent[3].triggerShot	=	3				
CutEvent[3].triggerTime	=	0
CutEvent[4].triggerShot	=	3				
CutEvent[4].triggerTime	=	3200
CutEvent[5].triggerShot	=	3				
CutEvent[5].triggerTime	=	700
	
		camShot[0].camStartPos	=	<<	2454.83,	4974.96,	46.4489	>>
camShot[0].camStartRot	=	<<	2.9627,	0.077,	115.874	>>
camShot[0].camStartFOV	=	39.702999				
camShot[0].camEndPos	=	<<	2454.41,	4974.93,	46.4691	>>
camShot[0].camEndRot	=	<<	2.6238,	0.077,	114.001	>>
camShot[0].camEndFOV	=	39.702999				
camShot[0].camDuration	=	2500				
camShot[0].totalDuration	=	2500				
camShot[0].camPanType	=	0				
camShot[0].flameNode	=	18				
camShot[1].camStartPos	=	<<	2435.41,	4960.94,	45.9568	>>
camShot[1].camStartRot	=	<<	6.677,	0,	-49.4853	>>
camShot[1].camStartFOV	=	58.9114				
camShot[1].camEndPos	=	<<	2435.11,	4960.62,	45.9441	>>
camShot[1].camEndRot	=	<<	6.3596,	0,	-44.8248	>>
camShot[1].camEndFOV	=	58.9114				
camShot[1].camDuration	=	3200				
camShot[1].totalDuration	=	1900				
camShot[1].camPanType	=	0				
camShot[1].flameNode	=	34				
camShot[2].camStartPos	=	<<	2434.77,	4969.56,	42.3654	>>
camShot[2].camStartRot	=	<<	5.2773,	-0.1511,	132.167	>>
camShot[2].camStartFOV	=	35.1297				
camShot[2].camEndPos	=	<<	2434.66,	4969.68,	42.3657	>>
camShot[2].camEndRot	=	<<	5.2773,	-0.1511,	132.167	>>
camShot[2].camEndFOV	=	35.1297				
camShot[2].camDuration	=	3500				
camShot[2].totalDuration	=	4500				
camShot[2].camPanType	=	3				
camShot[2].flameNode	=	49				
camShot[3].camStartPos	=	<<	2472.1,	4943.1,	45.3	>>
camShot[3].camStartRot	=	<<	6,	0,	25.3	>>
camShot[3].camStartFOV	=	33.1297				
camShot[3].camEndPos	=	<<	2472.1,	4943.1,	45.3	>>
camShot[3].camEndRot	=	<<	5.5,	0,	35.6	>>
camShot[3].camEndFOV	=	33.1297				
camShot[3].camDuration	=	3500				
camShot[3].totalDuration	=	3500				
camShot[3].camPanType	=	0				
camShot[3].flameNode	=	-1	
fShakeValue = 0.06
				
			
				
			
				
			
		DISABLE_TAXI_HAILING(TRUE)		

		
		basementFlames[0] = <<2432.74, 4963.43, 41.35>>
		basementFlames[1] = <<2432.02, 4962.61, 41.35>>
		basementFlames[2] = <<2431.06, 4961.83, 41.35>>
		
		basementFlames[3] = <<2430.84, 4961.08, 42.09>>
		basementFlames[4] = <<2430.53, 4962.47, 42.00>>
		basementFlames[5] = <<2430.81, 4961.10, 42.80>>
		basementFlames[6] = <<2429.74, 4961.98, 42.32>>
	
		basementFlames[7] = <<2433.50, 4966.76, 41.35>>
		basementFlames[8] = <<2433.24, 4967.42, 41.35>>
		basementFlames[9] = <<2433.15, 4967.43, 42.19>>
		basementFlames[10] = <<2432.53, 4967.89, 42.40>>
		basementFlames[11] = <<2432.48, 4969.07, 42.19>>
		basementFlames[12] = <<2433.36, 4969.44, 42.31>>
		
	//gasTrail[61].coord = <<2434.2747, 4965.7827, 41.3476>>
				

				vDefAreas[0] = <<2552.692383,4978.566406,45.323090>> 
				vDefAreas[1] = <<2562.533691,5013.501953,47.463024>> 
				vDefAreas[2] = <<2553.884766,4945.660645,47.493053>> 
				vDefAreas[3] = <<2515.545166,4936.428223,42.883816>> 
				vDefAreas[4] = <<2497.318359,4967.817871,43.595566>> 
				vDefAreas[5] = <<2481.376221,4951.766113,43.998589>> 
				vDefAreas[6] = <<2522.044434,4956.371582,43.716049>> 
				vDefAreas[7] = <<2529.048096,4985.627441,43.868477>> 
				vDefAreas[8] = <<2502.503418,4987.245117,46.626690>> 
				vDefAreas[9] = <<2464.891113,4939.948730,44.256462>> 
				vDefAreas[10] = <<2447.160156,4940.272461,44.157661>> 
				vDefAreas[11] = <<2376.536865,4946.399414,41.772324>> 
				vDefAreas[12] = <<2395.231934,4995.629883,44.689095>> 
				vDefAreas[13] = <<2419.819092,4991.935547,45.369762>> 
				vDefAreas[14] = <<2436.319336,5011.725098,45.846943>> 
				vDefAreas[15] = <<2407.764648,5023.078613,47.770912>> 
				vDefAreas[16] = <<2479.015869,5028.829102,42.988060>> 
				vDefAreas[17] = <<2434.704102,5042.712891,45.344620>> 
				vDefAreas[18] = <<2513.929932,5039.332520,51.021214>> 
				vDefAreas[19] = <<2477.882324,4986.437012,44.995113>> 
				vDefAreas[20] = <<2450.058105,5011.857910,44.890316>>
				
				
				RESET_REACT_ARRAY(pedReactionState,dChinese2,convBlock,12,<<2482.1965, 4975.3667, 44.7288>>,121,STATE_SPAWN)		
	
				setReactDialogue(diAlertAllSeen,"ONEILGUARD4",5,"ONEILGUARD1")
				setReactDialogue(diAlertAllUnSeen,"ONEILGUARD4",5,"ONEILGUARD1")
				setReactDialogue(diLostPlayer,"CHI2_nope",8,"ONEILGUARD1")
				setReactDialogue(diSeePlayer,"SEETREVOR",3,"chin2goon1")
				setReactDialogue(diHearPlayer,"CHI2_hear",8,"ONEILGUARD1")
				setReactDialogue(diHearPlayerAgain,"CHI2_hear",8,"ONEILGUARD1")
				setReactDialogue(diBullet,"ONEILGUARD3",5,"ONEILGUARD1")
				setReactDialogue(diDeadBody,"ONEILGUARD2",5,"ONEILGUARD1")
				setReactDialogue(diExplosion,"ONEILGUARD4",5,"ONEILGUARD1")
				setReactDialogue(diFoundPlayer,"GOON2TREV",4,"CHIN2goon2")
				
				add_event(events_reset_house_rayfire,true)
				add_event(events_continuous_stats_tracking,true)
				
				vehPlayer = GET_LAST_DRIVEN_VEHICLE()
				
				#if IS_DEBUG_BUILD
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 50, FALSE,FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 30, FALSE,FALSE)
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_SCOPE_MAX)
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_SNIPERRIFLE,WEAPONCOMPONENT_AT_AR_SUPP_02)
				#endif
				SET_VEHICLE_MODEL_IS_SUPPRESSED(bodhi2, TRUE)		
				
			
				ADD_RELATIONSHIP_GROUP("TAOGROUP",taorelgroup)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,taorelgroup,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,taorelgroup)
				
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL) 
//				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(),CHI2_UNMARKED) 
				
				//INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehPlayer,CHI2_CAR_DAMAGE) //this needs to be directed at trev's truck
				SET_SCENARIO_GROUP_ENABLED("Chinese2_Lunch",FALSE)
				SET_SCENARIO_GROUP_ENABLED("CHINESE2_HILLBILLIES",false)
				
				//INFORM_MISSION_STATS_OF_MISSION_START_CHINESE_TWO()
			
				IF (Is_Replay_In_Progress())
					int myStage
					myStage = Get_Replay_Mid_Mission_Stage()
					IF g_bShitskipAccepted = TRUE
	        			myStage++
	        		ENDIF
					cprintln(DEBUG_TREVOR3,"replay in progress to stage : ",myStage)
					switch myStage
						case  0
							do_skip(drive_to_farm,true)
							DO_SCREEN_FADE_IN(1000)
							
						BREAK
						CASE  1
							IF g_bShitskipAccepted = TRUE							
								do_skip(farm_cutscene,true)
							ELSE
								do_skip(snipe_from_hill,true)
								DO_SCREEN_FADE_IN(1000)
							ENDIF
							
						BREAK
						CASE  2
							do_skip(inside_house,true)
							DO_SCREEN_FADE_IN(1000)
						BREAK
						CASE 3
							do_skip(do_petrol_trail,true)
							DO_SCREEN_FADE_IN(1000)						
						BREAK
						CASE 4
							IF g_bShitskipAccepted = TRUE
								do_skip(explosion_cut,true)
							ELSE
								do_skip(end_mission,TRUE)
								DO_SCREEN_FADE_IN(1000)
							ENDIF							
						BREAK
						CASE 5
							do_skip(missionPassed,true)
							
							DO_SCREEN_FADE_IN(1000)
							Mission_Passed()
						BREAK
					ENDSWITCH
				elIF IS_REPEAT_PLAY_ACTIVE()
					do_skip(intro_cut,false,true)
					
				ELSE
					STAGE_ENDS()
				ENDIF												
			BREAK

		ENDSWITCH	
		

		
	ENDPROC



	int iFrameDelay
	proc stage_intro_cut()
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0,0.0)
		
		SWITCH stageFlag
			case 0
				
				vehPlayer = GET_PLAYERS_LAST_VEHICLE()
					
				
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE() 
//				AND ((DOES_ENTITY_EXIST(zPED[ped_oldMan1].id) AND DOES_ENTITY_EXIST(zPED[ped_oldMan2].id)) 
//				OR (DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1]) AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])))
					
					REQUEST_ANIM_DICT("MISSChinese2_crystalMazeMCS1_IG")
			
			
					
					request_model(IG_OLD_MAN2)
					request_model(IG_OLD_MAN1A)
					request_model(IG_JANET)
					request_model(IG_TAOCHENG)
					request_model(IG_TAOSTRANSLATOR)
					REQUEST_ANIM_DICT("misschinese2_crystalmaze")
					REQUEST_ANIM_DICT("missrampageintrooutro")
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
				
			    	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						
						zPED[ped_oldMan2].id = g_sTriggerSceneAssets.ped[2]					
						zPED[ped_oldMan1].id = g_sTriggerSceneAssets.ped[1]
						zPED[ped_tao].id = g_sTriggerSceneAssets.ped[4]
						zPED[ped_taos_translator].id = g_sTriggerSceneAssets.ped[3]
						zPED[ped_janet].id = g_sTriggerSceneAssets.ped[0]
						
						SET_ENTITY_AS_MISSION_ENTITY(zPED[ped_oldMan2].id,TRUE,TRUE)
						SET_ENTITY_AS_MISSION_ENTITY(zPED[ped_oldMan1].id,TRUE,TRUE)
						SET_ENTITY_AS_MISSION_ENTITY(zPED[ped_tao].id,TRUE,TRUE)
						SET_ENTITY_AS_MISSION_ENTITY(zPED[ped_taos_translator].id,TRUE,TRUE)
						SET_ENTITY_AS_MISSION_ENTITY(zPED[ped_janet].id,TRUE,TRUE)
						
						IF NOT IS_PED_INJURED(zPED[ped_oldMan1].id) REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_oldMan1].id,"Old_Man1A",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,ig_OLD_MAN1A) ENDIF
						IF NOT IS_PED_INJURED(zPED[ped_oldMan2].id) REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_oldMan2].id,"Old_Man2",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_OLD_MAN2) ENDIF
						IF NOT IS_PED_INJURED(zPED[ped_tao].id) REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_tao].id,"tao",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_TAOCHENG) ENDIF
						IF NOT IS_PED_INJURED(zPED[ped_taos_translator].id) REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_taos_translator].id,"Taos_Translator",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_TAOSTRANSLATOR) ENDIF
						IF NOT IS_PED_INJURED(zPED[ped_janet].id) REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_janet].id,"Janet",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,ig_JANET) ENDIF
					ELSE
						//Dan H: changed to just create then just animate
						zped[ped_oldMan1].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN1A,<<1987.231,3052.741,46.214>>)
						REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_oldMan1].id,"Old_Man1A",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,ig_OLD_MAN1A)
						zped[ped_oldMan2].id = create_ped(PEDTYPE_MISSION,IG_OLD_MAN2,<<1987.231,3052.741,46.214>>)
						REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_oldMan2].id,"Old_Man2",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_OLD_MAN2)
						IF DOES_ENTITY_EXIST(zPED[ped_tao].id) //this ped can be created during the skip
							IF NOT IS_PED_INJURED(zPED[ped_tao].id) 
								REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_tao].id,"tao",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_TAOCHENG)
							ENDIF
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_tao].id,"tao",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,IG_TAOCHENG)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_taos_translator].id,"Taos_Translator",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,IG_TAOSTRANSLATOR)
						zped[ped_janet].id = create_ped(PEDTYPE_MISSION,ig_JANET,<<1987.231,3052.741,46.214>>) //added for bug 1709355
						REGISTER_ENTITY_FOR_CUTSCENE(zPED[ped_janet].id,"Janet",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,ig_JANET)
					ENDIF
													
					CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
			   		START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					CLEAR_AREA_OF_PEDS(<<1991.219604,3054.884277,50.277412>>,87)
					CLEAR_AREA_OF_PROJECTILES(<<1991.219604,3054.884277,50.277412>>,87)
					
					ADD_SCENARIO_BLOCKING_AREA(<<1991.219604,3054.884277,50.277412>>-<<40,40,40.000000>>,<<1991.219604,3054.884277,50.277412>>+<<40,40,40.000000>>)
					SET_PED_NON_CREATION_AREA(<<1991.219604,3054.884277,50.277412>>-<<40,40,40>>,<<1991.219604,3054.884277,50.277412>>+<<40,40,0.000000>>)
					
					add_event(events_resolve_vehicles_at_start)
					add_event(events_animate_bar)	
					iFrameDelay = 0
					stageFlag++
				ENDIF
			BREAK
			 
			case 1
				IF IS_CUTSCENE_PLAYING()
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(1000)						
					ENDIF
					stageFlag++
				ENDIF
			BREAK
			
			CASE 2
				
				IF IS_CUTSCENE_PLAYING()

					If iFrameDelay = 0
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_CHINESE_2)
						
					
						iFrameDelay++
					ENDIF
				
				ENDIF
					IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan1].id)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Old_Man1A")
							zped[ped_oldMan1].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Old_Man1A"))
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(zped[ped_oldMan2].id)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Old_Man2")
							zped[ped_oldMan2].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Old_Man2"))
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(zped[ped_tao].id)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("tao")
							zped[ped_tao].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("tao"))
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(zped[ped_taos_translator].id)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator")
							zped[ped_taos_translator].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Taos_Translator"))
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(zped[ped_janet].id)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Janet")
							zped[ped_janet].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Janet"))
						ENDIF
					ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("tao")
							IF NOT IS_PED_INJURED(zped[ped_tao].id)
								SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_tao].id,taorelgroup)
								SET_PED_CAN_BE_TARGETTED(zped[ped_tao].id,FALSE)
								int iTempSync
								iTempSync = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
								TASK_SYNCHRONIZED_SCENE (zped[ped_tao].id, iTempSync, "misschinese2_crystalmaze", "2int_Loop_A_TaoCheng", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )						
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator")
							IF NOT IS_PED_INJURED(zped[ped_taos_translator].id)
								SET_PED_RELATIONSHIP_GROUP_HASH(zped[ped_taos_translator].id,taorelgroup)
								SET_PED_CAN_BE_TARGETTED(zped[ped_taos_translator].id,FALSE)
								int iTempSyncB
								iTempSyncB = CREATE_SYNCHRONIZED_SCENE(<< 1991.988, 3054.510, 46.215 >>, << -0.000, 0.000, 50.760 >>)
								TASK_SYNCHRONIZED_SCENE (zped[ped_taos_translator].id, iTempSyncB, "misschinese2_crystalmaze", "2int_Loop_A_taotranslator", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )												
							ENDIF
						ENDIF
	

			
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					vehPlayer = GET_PLAYERS_LAST_VEHICLE()
					//cprintln(debug_Trevor3,"EXIT PLAYER")
					IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehPlayer)
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehPlayer,<<1995.2018, 3062.1565, 46.0491>>) < 5.0
						//IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayer) //1267215
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())	
							REMOVE_VEHICLE_ASSET(get_entity_model(vehPlayer))
						//ENDIF
					//	////cprintln(debug_trevor3,"AH?")
						
					ELSE
						IF HAS_ANIM_DICT_LOADED("missrampageintrooutro")
							SET_ENTITY_COORDS(player_ped_id(),<<1992.1351, 3057.4666, 46.0600>>)
							SET_ENTITY_HEADING(player_ped_id(),325.4678)
							TASK_PLAY_ANIM(player_ped_id(),"missrampageintrooutro","trvram_6_1h_run_outro",instant_blend_in,SLOW_blend_out,-1,af_default,0.1)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_id(),TRUE)
							
						ELSE
							SET_ENTITY_COORDS(player_ped_id(),<<1992.9188, 3057.9063, 46.0567>>)
							SET_ENTITY_HEADING(player_ped_id(),337.2537)
							FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_WALK,FALSE,FAUS_DEFAULT)																		
							SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_id(),TRUE)
						ENDIF
					ENDIF
				ENDIF
			
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()			
					RESET_ADAPTATION()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
			
					IF IS_SCREEN_FADED_OUT()
						LOAD_SCENE(<<1992.1351, 3057.4666, 46.0600>>)
					ENDIF
										
					REPLAY_STOP_EVENT()
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 7.0)
					
					STAGE_ENDS()
				ENDIF
			BREAK
		ENDSWITCH
	
ENDPROC

proc stage_drive_to_farm()
	
//	float fAng
//	vector vplayer
//	vplayer = GET_ENTITY_COORDS(player_ped_id())
//	fAng = GET_HEADING_FROM_COORDS(<<2458.222656,4986.041504,49.052414>>,vPLayer,true)
//	IF fAng > 180 fAng -= 360 endif
//	//cprintln(debug_Trevor3,"ang: ",fAng," rad: ",DEG_TO_RAD(fAng))
	
	checkConditions(COND_GO_TO_FARM_START,COND_GO_TO_FARM_END)

	ACTION(0,ACT_AUDIO_SCENE_DRIVE_TO_FARMHOUSE,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_ANY_CAR)
	ACTION(1,ACT_REMOVE_BAR_PEDS)	
	ACTION(2,ACT_ANIMATE_CHINESE)
	action(3,ACT_RADIO_SOUND)
	action(4,ACT_WHIMPERING)
	ACTION(5,ACT_LOAD_FRUSTROM)
	ACTION(6,ACT_MAKE_BAR_PEDS_FLEE)
	ACTION(7,ACT_BAR_PEDS_LOOK_AT_TREVOR)
	ACTION(8,ACT_SLOW_DOWN_PLAYER_IN_CHOPPER)
	ACTION(9,ACT_BLEND_OUT_OF_TREVOR_RAGE)
	ACTION(10,ACT_LOAD_SOUNDSET)


	INSTRUCTIONS(0,INS_SPECIAL_ABILITY_BOOST,cIF,COND_INITIAL_INSTRUCTIONS_PLAYED)
	fail(fail_kill_cheng_or_translator)
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<2458.222656,4986.041504,49.052414>>,FALSE) < 107.9
	OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2374.441406,4930.563965,34.737843>>, <<2478.099121,5077.488281,60.632458>>, 185.000000,false,false)
	OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<2396.725830,4887.434082,33.853291>>, <<2510.539551,5003.355957,58.011837>>, 158.250000,false,false)
		IF DOES_BLIP_EXIST(locatesData.LocationBlip)						
			SET_BLIP_ROUTE(locatesData.GPSBlipID, FALSE)
		ENDIF
		STAGE_ENDS()
	ELSE
	
		SWITCH stageFlag
			case 0
				add_event(events_trevor_phonecall)	
				add_Event(events_trevor_rants)
				add_event(events_give_sniper)
				add_event(events_spawn_house)
				set_event_flag(events_spawn_house,-20)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				
				stageFlag++
			BREAK
			case 1			
				if IS_PLAYER_AT_LOCATION_ANY_MEANS(locatesData,<< 2608.1511, 4949.7539, 39.4 >>,<<9,9,LOCATE_SIZE_HEIGHT>>,TRUE,"FRMCHSE_1",TRUE)
					
					REPLAY_RECORD_BACK_FOR_TIME(6.0, 6.0)
					
					stageFlag++
					CLEAR_MISSION_LOCATE_STUFF(locatesData,true)
					kill_event(events_trevor_rants)
					IF IS_AUDIO_SCENE_ACTIVE("CHI_2_DRIVE_TO_FARMHOUSE")
						STOP_AUDIO_SCENE("CHI_2_DRIVE_TO_FARMHOUSE")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("CHI_2_DRIVE_ROCK_RADIO")
						STOP_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")
					ENDIF
				endif
			break
			case 2
				IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
				
					VEHICLE_INDEX vehTemp 
					vehTemp = GET_VEHICLE_PED_IS_USING(player_ped_id())
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTemp,10.0,2)
						IF GET_EVENT_FLAG(events_spawn_house) > 2				
						//	SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp,<<0,0,0>>, 285.2500 )
							
							stageFlag++
						ENDIF
					endif
				ELSe
			
					stageFlag++
				ENDIF
			break
			case 3
				
				if IS_PLAYER_AT_LOCATION_ANY_MEANS(locatesData,<<2573.6301, 4983.6768, 50.6978>>,<<12,12,LOCATE_SIZE_HEIGHT>>,FALSE,"FRMCHSE_5",TRUE)			
					IF IS_AUDIO_SCENE_ACTIVE("CHI_2_DRIVE_ROCK_RADIO")
						STOP_AUDIO_SCENE("CHI_2_DRIVE_ROCK_RADIO")
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					STAGE_ENDS()
				endif
				IF DOES_BLIP_EXIST(locatesData.LocationBlip)						
					SET_BLIP_ROUTE(locatesData.GPSBlipID, FALSE)
				ENDIF
			break
		ENDSWITCH
	ENDIF
ENDPROC



proc stage_farm_cutscene()
	
	
	//ACTION(5,ACT_LOAD_FRUSTROM)
	

		SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<<2004.4471, 3076.8066, 46.6069>>,10)
		SET_CAR_GENERATORS_CAN_UPDATE_DURING_CUTSCENE(true)
		

		
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() and stageFlag > 0 and stageflag < 7 and get_game_timer() > safeSkipTime
			//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 2570.1631, 4980.8589, 50.5336 >>)
			
			stageFlag = 10
			cutChangeTime = 0
			bCutsceneWasSkipped = true
		ENDIF
		
		IF stageFlag < 10
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
		
		IF bcutscenePLaying
			
			SET_SRL_TIME(to_float(timera()))
		ENDIF
		
			
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SWITCH stageFlag
			case 0	
				IF IS_SRL_LOADED()
					REQUEST_CUTSCENE("CHI_2_MCS_5")
					bBlockShooterDialogue = TRUE
					bCutsceneWasSkipped = FALSE
					SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
					BEGIN_SRL()
					
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
				
					bPlayerOnHill	 = FALSE
					bcutscenePLaying = TRUE
					
					
					
					//for recording, comment out from here...
					
					
					
					vector vPlayer
					vPLayer = get_entity_coords(player_ped_id())
					
					
					
					float fAng
					fAng = GET_HEADING_FROM_COORDS(<<2458.222656,4986.041504,49.052414>>,vPLayer,true)
					IF fAng > 180 fAng -= 360 endif
					
					vehicle_index vehTemp
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())					
						vehTemp = GET_VEHICLE_PED_IS_IN(player_ped_id())
					ENDIF
					
					IF (IS_VEHICLE_DRIVEABLE(vehTemp) AND IS_THIS_MODEL_A_HELI(get_entity_model(vehTemp)) AND IS_ENTITY_IN_AIR(vehTemp))
					OR (IS_VEHICLE_DRIVEABLE(vehTemp) AND IS_THIS_MODEL_A_PLANE(get_entity_model(vehTemp)) AND IS_ENTITY_IN_AIR(vehTemp))
						//in air
						IF fAng < RAD_TO_DEG(-2.5)
						AND fAng > RAD_TO_DEG(0.5)
							stageFlag = 6 //fly south to north
							settimera(30000)
							cprintln(debug_Trevor3,"set time b:",timera())
							SET_SRL_TIME(30000.0)
							
							cutChangeTime = 36000
						ELSE
							stageFlag = 7
							settimera(36000)
							cprintln(debug_Trevor3,"set time c:",timera())
							SET_SRL_TIME(36000.0)
							
							cutChangeTime = 42000
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(vehTemp)
							IF IS_VEHICLE_DRIVEABLE(vehTemp)
								IF NOT (IS_THIS_MODEL_A_HELI(get_entity_model(vehTemp)) AND IS_ENTITY_IN_AIR(vehTemp))
								AND NOT (IS_THIS_MODEL_A_PLANE(get_entity_model(vehTemp)) AND IS_ENTITY_IN_AIR(vehTemp))
									SET_VEHICLE_ON_GROUND_PROPERLY(vehTemp)
								ENDIF
							ENDIF
						ENDIF
						//on hill
						IF fAng < RAD_TO_DEG(-1.33)
						AND fAng > RAD_TO_DEG(-1.92)
							stageFlag = 1 //hill shots
							settimera(0)
							cprintln(debug_Trevor3,"set time d:",timera())
							SET_SRL_TIME(0.0)
							
							cutChangeTime = 6000
						ELSE
							IF fAng < RAD_TO_DEG(0.96)
							AND fAng > RAD_TO_DEG(-1.33)				
								stageFlag = 2 //back right
								settimera(6000)
								cprintln(debug_Trevor3,"set time e:",timera())
								SET_SRL_TIME(6000.0)
								
								cutChangeTime = 12000
							ELSE
								IF fAng < RAD_TO_DEG(2.29)
								AND fAng > RAD_TO_DEG(0.96)
									stageFlag = 3 //orchard
									settimera(12000)
									cprintln(debug_Trevor3,"set time f:",timera())
									SET_SRL_TIME(12000.0)
									
									cutChangeTime = 18000
								ELSE
									IF fAng < RAD_TO_DEG(-2.81)
									OR fAng > RAD_TO_DEG(2.29)
										stageFlag = 4 //left
										settimera(18000)
										cprintln(debug_Trevor3,"set time g:",timera())
										SET_SRL_TIME(18000.0)
										
										cutChangeTime = 24000
									ELSE
										//front
										//-2.81
										//-1.92
										settimera(24000)
										cprintln(debug_Trevor3,"set time h:",timera())
										SET_SRL_TIME(24000.0)
										
										cutChangeTime = 30000
										stageFlag = 5
									ENDIF
								ENDIF
							ENDIF
						ENDIF												
					ENDIF
					
					//to here
					
				
				
					bForceAnimTimeA = TRUE
				
					DESTROY_ALL_CAMS()
					CLEAR_MISSION_LOCATE_STUFF(locatesData,true)
					
					IF IS_PED_IN_ANY_VEHICLE(player_ped_id())				
						cutscene_player_vehicle = GET_VEHICLE_PED_IS_IN(player_ped_id())
						vehicleSpeed = GET_ENTITY_VELOCITY(cutscene_player_vehicle)
						FREEZE_ENTITY_POSITION(cutscene_player_vehicle,TRUE)
						SET_ENTITY_VISIBLE(cutscene_player_vehicle,FALSE)
						cprintln(debug_Trevor3,"CHOPPER FROZEN")
					ENDIF
					
					PLAY_STREAM_FRONTEND()
					
					IF stageFlag = 1
					
						bPlayerOnHill = TRUE
						
						
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<2559.012207,4958.518066,36.029594>>, <<2585.446777,4993.435059,64.625648>>, 48.00000,<<2594.6506, 4948.1138, 38.0899>>, 283.7287,<<22,22,15>>)
						
						IF DOES_ENTITY_EXIST(vehTemp)
							IF IS_VEHICLE_DRIVEABLE(vehTemp)
								SET_VEHICLE_ENGINE_ON(vehTemp,FALSE,TRUE)
								
							ENDIF
						ENDIF
						
						CLEAR_AREA(<< 2445.1387, 4978.6421, 52.1489 >>,150.0,true,true)
						CLEAR_AREA(<< 2578.5234, 4982.2837, 51.4416 >>,13.0,true)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 2578.4187, 4981.9014, 50.5870 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 49.2184)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())	
						
						SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_WALK)
						sequence_index tempSeq
						
						//SET_PED_DUCKING(PLAYER_PED_ID(),TRUE)
						OPEN_SEQUENCE_TASK(tempSeq)
							TASK_GO_STRAIGHT_TO_COORD(NULL, << 2570.5430, 4982.0020, 50.6795 >>,PEDMOVE_WALK) /*<< 2570.1631, 4980.8589, 50.5336 >>*/
							TASK_ACHIEVE_HEADING(NULL, 88.7065)
						CLOSE_SEQUENCE_TASK(tempSeq)
						 
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), tempSeq)
						CLEAR_SEQUENCE_TASK(tempSeq)
						
						

						//BEGIN_SRL()
						
						cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
						ADD_CAM_SPLINE_NODE(cam1,<<2538.6, 4976.9, 50.6>>, <<-1.8, -0.0000, 93.2>>,5000)
						ADD_CAM_SPLINE_NODE(cam1,<<2536.1531, 4976.7002, 50.5267>>, <<-1.8, -0.0000, 93.2>>,6000)
						SET_CAM_FOV(cam1,34.9)	
						SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)
					ELSE
				
									
						
						SWITCH stageFlag
							CASE 2 //back right						
								cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
								ADD_CAM_SPLINE_NODE(cam1,<<2426.8,5088.3,51.2>>, <<0,0,-169>>,5000)
								ADD_CAM_SPLINE_NODE(cam1,<<2427.8,5087.2,51.2>>, <<0,0,-169.7>>,6000)
								SET_CAM_FOV(cam1,28.5)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)					
							BREAK
							CASE 3 //orchard					
								cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
								ADD_CAM_SPLINE_NODE(cam1,<<2326.3,4985.6,51.5>>, <<0,0,-89.1>>,5000)
								ADD_CAM_SPLINE_NODE(cam1,<<2328,4985.4,52.1>>, <<0,0,-89.1>>,6000)
								SET_CAM_FOV(cam1,28.5)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)					
							BREAK
							CASE 4 //left					
								cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
								ADD_CAM_SPLINE_NODE(cam1,<<2403.6,4889.7,47.5>>, <<3.4,0,-19.5>>,5000)
								ADD_CAM_SPLINE_NODE(cam1,<<2403.6,4889.7,47.5>>+<<0.8,2.15,0.07>>, <<3.4,0,-19.5>>,6000)
								SET_CAM_FOV(cam1,29.8)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)					
							BREAK
							CASE 5 //front					
								cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
								ADD_CAM_SPLINE_NODE(cam1,<<2493.4,4934.8,45.7>>, <<5.2,0,48>>,5000)
								ADD_CAM_SPLINE_NODE(cam1,<<2493.4,4934.8,45.7>>+<<-1,1,0.0>>, <<5.2,0,48>>,6000)
								SET_CAM_FOV(cam1,28.5)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)					
							BREAK
							CASE 6 //air
								cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
								ADD_CAM_SPLINE_NODE(cam1,<<2570.6,4982,151.6>>, <<-47.9,0,88.1>>,5000)
								ADD_CAM_SPLINE_NODE(cam1,<<2487.8,4896.9,151.9>>, <<-47.2,0,28.8>>,15000)
								SET_CAM_FOV(cam1,28.5)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)					
							BREAK
							CASE 7 //air
								cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
								ADD_CAM_SPLINE_NODE(cam1,<<2487.8,4896.9,151.9>>, <<-47.2,0,28.8>>,5000)
								ADD_CAM_SPLINE_NODE(cam1,<<2570.6,4982,151.6>>, <<-47.9,0,88.1>>,15000)						
								SET_CAM_FOV(cam1,28.5)	
								SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)					
							BREAK
						ENDSWITCH
						
					ENDIF
					
					IF stageFlag < 6
						SHAKE_CAM(cam1,"Hand_shake",0.2)
					ELSE
						SHAKE_CAM(cam1,"Hand_shake",0.7)
					ENDIF
					SET_CAM_ACTIVE(cam1, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					START_AUDIO_SCENE("CHI_2_FARMHOUSE_OVERVIEW")			
					DISABLE_CELLPHONE(TRUE)
					SET_WANTED_LEVEL_MULTIPLIER(0.0)		
					SET_MAX_WANTED_LEVEL(0)
					CLEAR_PRINTS()
					CLEAR_HELP()
						
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REMOVE_PROJECTILES)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)																									
							
					SET_PED_STEALTH_MOVEMENT(player_ped_id(),FALSE)	

					safeSkipTime = GET_GAME_TIMER()+1000
					
					int iw
					REPEAT COUNT_OF(zped) iw
						IF NOT IS_PED_INJURED(zped[iw].id)
							STOP_PED_SPEAKING(zped[iw].id,TRUE)
						ENDIF
					ENDREPEAT
					
					//create temp ped just for cutscene
					tempPed = CREATE_PED(PEDTYPE_MISSION,A_M_Y_MethHead_01,<< 2460.4495, 4976.9380, 45.5765 >>, 243.4917)
					GIVE_WEAPON_TO_PED(tempPed,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					
					IF NOT IS_PED_INJURED(tempPed)
						startSeq()
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2463.0156, 4974.9702, 45.5765 >> , PEDMOVE_WALK, -1, 0.5, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2452.8335, 4964.7852, 45.5765 >> , PEDMOVE_WALK, -1, 0.5, ENAV_NO_STOPPING)
							TASK_PAUSE(NULL, 500)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 2460.4495, 4976.9380, 45.5765 >> , PEDMOVE_WALK, -1, 0.5)
						endseq(tempPed,true)
					ENDIF
					
					
					vehOneil = CREATE_VEHICLE(DUBSTA,<<2479.9612,4990,45.8>>, -5.37)
		
						

					PLAYMUSIC("CHN2_MISSION_START",true,"CHN2_EXPLODE")
					stageFlag=3
					//endif
				ENDIF
			break

			case 3 //shot of binocular guy
				if timera() > cutChangeTime
					//IF NOT IS_PED_INJURED(zped[enum_to_int(pos_shooting_bottle_1)].id)
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(zPED[enum_to_int(pos_shooting_bottle_1)].id, "generic_cheer", "A_M_M_Hillbilly_01_White_mini_02", "SPEECH_PARAMS_FORCE_SHOUTED")
					//ENDIF
						DESTROY_ALL_CAMS()
						cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
						ADD_CAM_SPLINE_NODE(cam1,<<2445.9443, 4962.9863, 51.4782>>, <<7.9159, -0.0000, 34.5909>>,5000)
						ADD_CAM_SPLINE_NODE(cam1,<<2445.6843, 4963.3677, 51.6659>>, <<3.6033, 0.0000, 34.5909>>,3500)
						SET_CAM_FOV(cam1,34)	
						SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)
						SHAKE_CAM(cam1,"Hand_shake",0.5)
						cutChangeTime += 3500
						
						stageFlag++
				ENDIF
			break
			case 4 //barrel peds
				if timera() > cutChangeTime
					
					DESTROY_ALL_CAMS()
					
					cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
					ADD_CAM_SPLINE_NODE(cam1,<<2465.0703, 4946.6885, 45.3372>>, <<7.1461, 0.0000, 30.9281>>,5000)
					ADD_CAM_SPLINE_NODE(cam1,<<2466.0112, 4947.3770, 45.3509>>, <<7.1461, 0.0000, 32.3761>>,3000)
					SET_CAM_FOV(cam1,34)	
					SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)
					SHAKE_CAM(cam1,"Hand_shake",0.5)
					cutChangeTime += 3000
					
					stageFlag++
				ENDIF
			break
			case 5 //bottle peds
				bForceAnimTimeB = TRUE
				if timera() > cutChangeTime
					bForceAnimTimeB = TRUE
					
					
					DESTROY_ALL_CAMS()
					cam1 = CREATE_CAM("DEFAULT_SPLINE_CAMERA", true)	
					ADD_CAM_SPLINE_NODE(cam1,<<2509.6912, 4971.0239, 44.1325>>, <<7.2578, 0.0000, 88.5226>>,5000)
					ADD_CAM_SPLINE_NODE(cam1,<<2509.4146, 4971.1631, 44.1674>>, <<6.6554, 0.0000, 89.4229>>,2500)
					SET_CAM_FOV(cam1,37.34)	
					
					SET_CAM_SPLINE_SMOOTHING_STYLE(cam1,CAM_SPLINE_NO_SMOOTH)
					SHAKE_CAM(cam1,"Hand_shake",0.5)
					cutChangeTime += 2500
					stageFlag++
				ENDIF
			break
			case 6
				if timera() > cutChangeTime
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						CLEAR_PRINTS()
						CLEAR_HELP()
						
						SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(6.818)
						
						
						IF IS_VEHICLE_DRIVEABLE(vehOneil)
							REGISTER_ENTITY_FOR_CUTSCENE(vehOneil,"EXL_2_abandoned_car",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY,DUBSTA) 
						ENDIF
						
						START_CUTSCENE()
						bBlockShooterDialogue = FALSE
					//	CLEAR_AREA(<<2493.9172, 4971.3428, 44.1748>>,3.0,true)

						stageFlag=11
					endif
				endif
			break
			
			
			
			case 10 //do fade
				IF DOES_ENTITY_EXIST(cutscene_player_vehicle)
				AND IS_VEHICLE_DRIVEABLE(cutscene_player_vehicle)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(cutscene_player_vehicle)
				ENDIF
				
				IF 	NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
				
				IF (IS_SCREEN_FADED_OUT() AND NOT IS_CUTSCENE_PLAYING())
				OR (IS_CUTSCENE_ACTIVE() and CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE))					
					stageFlag++
				ENDIF				
			break
	
			CASE 11		
			
				IF DOES_ENTITY_EXIST(cutscene_player_vehicle)
				AND IS_VEHICLE_DRIVEABLE(cutscene_player_vehicle)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(cutscene_player_vehicle)
				ENDIF
			
				IF (IS_SCREEN_FADED_OUT() AND NOT IS_CUTSCENE_PLAYING())
				OR (IS_CUTSCENE_ACTIVE() and CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE))
					DESTROY_ALL_CAMS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					//add flash back to first person
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					AND NOT bCutsceneWasSkipped
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					ENDIF

					
					REPLAY_STOP_EVENT()
						
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 2570.5430, 4982.0020, 50.6795 >>) < 15
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 2570.5430, 4982.0020, 50.6795 >>) > 1
					
						SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2570.5430, 4982.0020, 50.6795 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(),98.21)				
					ELSE
						IF NOT IS_PED_IN_ANY_VEHICLE(player_ped_id())
							SET_ENTITY_HEADING(PLAYER_PED_ID(),GET_HEADING_FROM_COORDS(get_entity_coords(player_ped_id()),<<2458.222656,4986.041504,49.052414>>))	
						ENDIF
					ENDIF
				//	CLEAR_AREA(<<2493.9172, 4971.3428, 44.1748>>,3.0,true)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()				
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6.3)
					SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(6.818)
					//SET_PED_DUCKING(PLAYER_PED_ID(),TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID()) 
					
					

		
					STOP_STREAM()
					STOP_AUDIO_SCENE("CHI_2_FARMHOUSE_OVERVIEW")
					
					CLEAR_PRINTS()
					CLEAR_HELP()
				//	DESTROY_ALL_CAMS()
				//	RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					DISABLE_CELLPHONE(FALSE)		
		
								
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING()				
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6.3)
					//SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(6.818)
					IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
						//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, TRUE)
						IF GET_AMMO_IN_PED_WEAPON(player_ped_id(),WEAPONTYPE_SNIPERRIFLE) < 15
							ADD_AMMO_TO_PED(player_ped_id(),WEAPONTYPE_SNIPERRIFLE,50)
						ENDIF
					ELSE
						#if IS_DEBUG_BUILD
						GIVE_WEAPON_TO_PED(player_ped_id(),WEAPONTYPE_SNIPERRIFLE,70,FALSE)
						#endif
					ENDIF        
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					IF DOES_ENTITY_EXIST(tempPed)
						DELETE_PED(tempPed)
					ENDIF
					
				
					iBarrels1 = get_ped_with_placement(pos_barrels_1)
					IF iBarrels1 != -1
						//set_ped_Action(get_ped_with_placement(pos_barrels_1),action_spawn)
						IF NOT IS_PED_INJURED(zped[iBarrels1].id)
							DELETE_PED(zped[iBarrels1].id)
						ENDIF
						IF DOES_ENTITY_EXIST(zped[iBarrels1].obj)
							DELETE_OBJECT(zped[iBarrels1].obj)
						ENDIF
						pedReactionState[iBarrels1].state = STATE_SPAWN
						zPed[iBarrels1].actionFlag = 0
						zPed[iBarrels1].intA = 0
						zPed[iBarrels1].action = action_roll_barrels
					ENDIF
					
			
					iBarrels2 = get_ped_with_placement(pos_barrels_2)
					IF iBarrels2 != -1
						//set_ped_Action(get_ped_with_placement(pos_barrels_1),action_spawn)
						IF NOT IS_PED_INJURED(zped[iBarrels2].id)
							DELETE_PED(zped[iBarrels2].id)
						ENDIF
						IF DOES_ENTITY_EXIST(zped[iBarrels2].obj)
							DELETE_OBJECT(zped[iBarrels2].obj)
						ENDIF
						pedReactionState[iBarrels2].state = STATE_SPAWN
						zPed[iBarrels2].actionFlag = 0
						zPed[iBarrels2].intA = 0
						zPed[iBarrels2].action = action_roll_barrels
					ENDIF
					
					kill_event(events_detach_barrels_from_peds,false)
					
					IF NOT IS_PED_INJURED(ped_oneil1)
						DELETE_PED(ped_oneil1)
					ENDIF
					
					IF NOT IS_PED_INJURED(ped_oneil2)
						DELETE_PED(ped_oneil2)
					ENDIF
					
					IF IS_VEHICLE_VALID_AND_DRIVEABLE(vehOneil)
						DELETE_VEHICLE(vehOneil)
					ENDIf
					
					IF DOES_ENTITY_EXIST(obj_cellphone)
						DELETE_OBJECT(obj_cellphone)
					ENDIF
					
					delete_all_house()				
					iNextPedVariation=0
					do_spawn_house()
					
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING)
					SET_MODEL_AS_NO_LONGER_NEEDED(DUBSTA)
		
					
					int ij
					REPEAT COUNT_OF(zped) ij
						IF NOT IS_PED_INJURED(zped[ij].id)
							STOP_PED_SPEAKING(zped[ij].id,FALSE)
						ENDIF
					ENDREPEAT
					
					
				//	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<< 2570.5430, 4982.0020, 50.6795 >>) < 15
					
				//		SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 2570.5430, 4982.0020, 50.6795 >>)
				//		SET_ENTITY_HEADING(PLAYER_PED_ID(),98.21)				

				//	ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()				
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6.3)

					cprintln(debug_trevor3,"heading: ",GET_HEADING_FROM_COORDS(get_entity_coords(player_ped_id()),<<2458.222656,4986.041504,49.052414>>,false))
					IF DOES_ENTITY_EXIST(cutscene_player_vehicle)				
						IF IS_VEHICLE_DRIVEABLE(cutscene_player_vehicle)
							FREEZE_ENTITY_POSITION(cutscene_player_vehicle,FALSE)
							IF IS_THIS_MODEL_A_PLANE(get_entity_model(cutscene_player_vehicle))						
								SET_ENTITY_VELOCITY(cutscene_player_vehicle,vehicleSpeed)
								SET_PED_INTO_VEHICLE(player_ped_id(),cutscene_player_vehicle)
							ENDIF
							SET_VEHICLE_ENGINE_ON(cutscene_player_vehicle,true,true)
							IF IS_THIS_MODEL_A_HELI(get_entity_model(cutscene_player_vehicle))
								SET_ENTITY_ROTATION(cutscene_player_vehicle,<<0,0,GET_ENTITY_HEADING(cutscene_player_vehicle)>>)
								SET_HELI_BLADES_FULL_SPEED(cutscene_player_vehicle)
								SET_ENTITY_VELOCITY(cutscene_player_vehicle,<<1,1,1>>)
								SET_PED_INTO_VEHICLE(player_ped_id(),cutscene_player_vehicle)
							ENDIF
							IF ( bPlayerOnHill = FALSE )
								SET_PED_INTO_VEHICLE(player_ped_id(),cutscene_player_vehicle)
							ENDIF
							SET_ENTITY_VISIBLE(cutscene_player_vehicle,TRUE)
						ENDIF
					ENDIF
					SET_ENTITY_VISIBLE(player_ped_id(),TRUE)
					SPECIAL_ABILITY_FILL_METER(player_id(),true)
					cprintln(debug_trevor3,"END CUT")
					END_SRL()
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
					STAGE_ENDS()
				ENDIF
				//ENDIF
			BREAK
		
			
		ENDSWITCH
		
	
	
	
ENDPROC

proc stage_get_to_meth_lab()
	

	
	checkConditions(COND_START_FARM_ASSAULT,COND_END_FARM_ASSAULT)

	INSTRUCTIONS(0,INS_DESTROY_METH_LAB)
	INSTRUCTIONS(1,ins_enemy_reacting,cIF,COND_ENEMY_PED_REACTING)
	INSTRUCTIONS(2,ins_all_enemy_reacting,cIF,COND_ALL_ENEMY_ALERTED)
	INSTRUCTIONS(3,ins_fast_stealth)
	INSTRUCTIONS(4,ins_special_ability,cIF,COND_PLAYER_IN_HOUSE)
	
	//INSTRUCTIONS(2,INS_QUICK_KILL_HELP,cIF,COND_ALL_ENEMY_ALERTED)
	
	action(0,act_stage_prep)
	//action(0,act_shooting_peds_witness_impact_at_barrels,PLAYOUT_ON_TRIGGER,cIF,COND_BULLET_IMPACTED_BY_BARRELS,cANDNOT,COND_SHOOTING_PED_DEAD)
	action(1,act_stat_double_kill)
	action(2,act_MUSIC_start_snipe)
	action(3,act_create_petrol_can_with_blip)
	action(4,act_MUSIC_approach_house_or_alert,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_APPROACHING_HOUSE,cOR,COND_ALL_ENEMY_ALERTED)
	//action(6,act_snipe_cams,PLAYOUT_ON_COND,cIFNOT,COND_ALL_ENEMY_ALERTED,cOR,COND_DEATH_CAM_IN_PROGRESS) //removed due to bug 968106
	action(7,act_remove_initial_assets,PLAYOUT_ON_TRIGGER,cIF,COND_ALL_ENEMY_ALERTED)
	action(8,act_outside_peds_flee,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_HOUSE,cAND,COND_ALL_ENEMY_ALERTED)
	action(9,act_remove_upstairs_peds,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_HOUSE,cAND,COND_PLAYER_DOWNSTAIRS)
	action(10,act_MUSIC_enter_house,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_HOUSE)
	action(11,act_remaining_peds_flee,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_METH_LAB)
	action(12,act_window_ped_walks_inside,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_APPROACHING_HOUSE)
//	action(13,act_put_player_in_action_mode)
	action(5,act_end_stealth_audio_Scene,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_METH_LAB,cOR,COND_ATTACKERS_ALL_DEAD,cOR,COND_ALL_ENEMY_ALERTED)
	action(6,act_enemy_alerted_audio_Scene,PLAYOUT_ON_TRIGGER,cIF,COND_ALL_ENEMY_ALERTED)
	action(14,act_player_got_to_basement_sneakily,PLAYOUT_ON_TRIGGER,cIF,COND_CARD_PLAYING_PEDS_STILL_NEAR_TABLE)
	action(13,act_remove_barrel_peds,PLAYOUT_ON_TRIGGER,cIF,COND_BARREL_ROLL_PEDS_IN_VAN)
	action(15,act_end_enemy_alerted_audio_Scene,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_IN_METH_LAB,cOR,COND_ATTACKERS_ALL_DEAD)
	action(16,act_modify_ped_seeing_range)
	action(17,act_remove_van_peds)
	action(18,act_audio_scene_in_house,PLAYOUT_ON_TRIGGER)
	action(19,act_reset_bottle_shooting_ped_accuracy)
	
	dialogue(0,dia_whatnow)
//	dialogue(1,dia_cook_sees_ped_die,cIF,COND_BASEMENT_PED_DEAD_IN_BASEMENT)
	dialogue(2,dia_trevor_rage_comments,cIF,COND_TREVOR_SEEN,cAndNOT,COND_ATTACKERS_ALL_DEAD)
	dialogue(3,dia_stop_trevor_getting_to_basement,cIF,COND_PLAYER_IN_HOUSE,cAND,COND_TREVOR_NEAR_BASEMENT)
	dialogue(4,dia_dont_let_him_down,cIF,COND_TREVOR_NEAR_STAIRS)
	dialogue(5,dia_hes_in_meth_lab,cIF,COND_TREVOR_AT_STAIRS)
	dialogue(6,dia_trevor_see_basement_peds_leave,cIF,COND_BASEMENT_PEDS_LEAVING_AND_OBSERVED,cANDNOT,COND_ALL_ENEMY_ALERTED)
	//dialogue(6,dia_trevor_spotted_outside,cIF,COND_TREVOR_SEEN, cANDNOT, COND_PLAYER_IN_HOUSE) //this is ref'd elsewhere in script as value 6
	dialogue(7,dia_find_trevor,cIF,COND_ALL_ENEMY_ALERTED,cANDNOT,COND_TREVOR_SEEN,cANDNOT,COND_PLAYER_IN_HOUSE)
	dialogue(8,dia_attacking_trevor_outside,cIF,COND_ALL_ENEMY_ALERTED,cAND,COND_TREVOR_SEEN,cANDNOT,COND_PLAYER_IN_HOUSE)
	dialogue(9,dia_trevor_seen_inside_house,cIF,COND_ALL_ENEMY_ALERTED,cAND,COND_PLAYER_IN_HOUSE)
	dialogue(10,dia_attacking_trevor_inside,cIF,COND_ALL_ENEMY_ALERTED,cAND,COND_PLAYER_IN_HOUSE)
	//dialogue(11,DIA_RESPOND_TO_BULLET,cIF,COND_RESPOND_TO_BULLET)
	dialogue(11,DIA_RESPOND_TO_ALERT)
	dialogue(12,dia_last_man_standing,cIF,COND_ALL_ENEMY_ALERTED,cAND,COND_ONE_ATTACKER_LEFT_OUTSIDE,cANDNOT,COND_PLAYER_IN_HOUSE)
	dialogue(13,dia_trevor_approaches_house,cIF,COND_PLAYER_APPROACHING_HOUSE,cANDNOT,COND_PLAYER_IN_HOUSE,cAND,COND_ALL_ENEMY_ALERTED)
	dialogue(14,dia_shooting_Barrels,cIFNOT,COND_PISSING_DIALOGUE_CAN_PLAY)
	
	DIALOGUE(15,Dia_TREVOR_SAYS_HARD_WAY)
	DIALOGUE(16,Dia_stay_in_house,cIF,COND_ALL_ENEMY_ALERTED,cAND,COND_ALL_OUTSIDE_ENEMY_DEAD,cAND,COND_PLAYER_APPROACHING_HOUSE,cANDNOT,COND_PLAYER_IN_HOUSE)
	
	fail(FAIL_ABANDONED_FARM,cIF,COND_FAIL_FOR_LEAVING_FARM)
	fail(FAIL_JERRY_CAN_DESTROYED,cIF,COND_JERRY_CAN_DESTROYED)
	
	IF IS_CONDITION_TRUE(COND_PLAYER_IN_METH_LAB)	
	AND IS_CONDITION_TRUE(COND_PLAYER_HAS_PETROL_CAN)
		
			STAGE_ENDS(do_petrol_trail) 
	
	ENDIF
ENDPROC

	

proc stage_petrol_trail()
	SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME()
	checkConditions(COND_START_PETROL_TRAIL,COND_END_PETROL_TRAIL)
	
	instructions(0,INS_PICK_UP_GAS_CAN)
	instructions(1,INS_USING_GAS_CAN,cIF,COND_PLAYER_HAS_PETROL_CAN)
	instructions(2,ins_missed_a_bit,cIF,COND_LAST_BLIP_POURED_ON,cANDNOT,COND_GAS_TRAIL_COMPLETE)
	instructions(3,ins_dont_abandon_farm,cIF,COND_40M_FARM_WARNING)
	
	dialogue(0,DIA_PICKED_UP_JERRY_CAN,cIF,COND_PLAYER_HAS_PETROL_CAN)
	dialogue(1,DIA_POURING_PETROL,cIF,COND_PLAYER_POURING_GAS,cANDNOT,COND_GAS_TRAIL_COMPLETE)
	dialogue(2,DIA_MISSED_A_BIT)
	dialogue(3,DIA_LIGHT_FUEL,cIF,COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE)
	DIALOGUE_ON_DIALOGUE(4,DIA_FUEL_BURN,3,DIA_LIGHT_FUEL)
	
	
	action(0,ACT_SPAWN_BLAZER)
	action(1,ACT_PREP_PETROL_POUR)
	action(2,ACT_CONTROL_PETROL_BLIPS,PLAYOUT_ON_COND,cIFNOT,COND_GAS_TRAIL_COMPLETE)
	action(3,ACT_PETROL_ASSIST_ROUTE)
	action(4,ACT_FUDGE_RADAR_ZOOM,PLAYOUT_ON_COND,cIFNOT,COND_GAS_TRAIL_COMPLETE)
	action(5,ACT_MUSIC_PICKS_UP_JERRY_CAM,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_HAS_PETROL_CAN)
	action(6,ACT_MUSIC_TRAIL_IGNITED,PLAYOUT_ON_TRIGGER,cIF,COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE)
	action(7,ACT_AUDIO_SCENE_WITH_CAN,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_HAS_PETROL_CAN)	
	action(8,ACT_PREP_EXPLOSION_AUDIO)
	action(9,ACT_REMOVE_SOME_ASSETS)
	
	action(10,ACT_BOMB_IN_BASEMENT,PLAYOUT_ON_TRIGGER,cIF,COND_EXPLOSION_IN_BASEMENT)
	action(11,ACT_STAT_POUR_GAS)
	action(12,ACT_FUDGE_GAS_AMMO)
	//13 taken below
	
	fail(FAIL_OUT_OF_GAS,cIF,COND_OUT_OF_GAS,cANDNOT,COND_GAS_TRAIL_COMPLETE)
	fail(FAIL_ABANDONED_FARM,cIF,COND_60M_FARM_FAIL)

	FAIL(FAIL_GAS_TRAIL_NOT_COMPLETE,cIF,COND_GAS_TRAIL_IGNITED_INCORRECTLY) //this was commented out but not sure why
	
	IF CUTSCENE_FIRST_SHOT = SHOT_NONE	
		IF IS_CONDITION_TRUE(COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE)
		AND NOT IS_CONDITION_TRUE(COND_EXPLOSION_IN_BASEMENT)
			CUTSCENE_FIRST_SHOT = SHOT_NORMAL
		ENDIF
		
		IF IS_CONDITION_TRUE(COND_GAS_IGNITED_IN_HOUSE_WILL_BURN_OK)
		AND NOT IS_CONDITION_TRUE(COND_EXPLOSION_IN_BASEMENT)
			bSneakyPass = TRUE
			CUTSCENE_FIRST_SHOT = SHOT_BASEMENT
		ENDIF
		
		IF GET_ACTION_FLAG(10,ACT_BOMB_IN_BASEMENT) = 2
			//player sticky bombed basement most likely
			CUTSCENE_FIRST_SHOT = SHOT_HOUSE_EXPLOSION
			bSneakyPass = TRUE
		ENDIF
	ENDIF
	
	action(13,ACT_LOAD_END_CUT_FRUSTROM)
	
	IF ((IS_CONDITION_TRUE(COND_GAS_TRAIL_IGNITED_FROM_OUTSIDE) AND IS_CONDITION_TRUE(COND_GAS_TRAIL_REACHED_DOORWAY))	
	OR bSneakyPass)
	AND GET_ACTION_FLAG(13,ACT_LOAD_END_CUT_FRUSTROM) = 10
	AND NOT IS_CONV_ROOT_PLAYING("CHI2_light")
		FAIL(FAIL_GAS_TRAIL_IGNITED_EARLY,cIF,COND_TREVOR_INSIDE_HOUSE)
		IF DOES_BLIP_EXIST(missionBlip)
			REMOVE_BLIP(missionblip)
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
		
		HIDE_GAS_TRAIL()
		STAGE_ENDS()
	ENDIF
		
	
	
endproc

int iLoadFXflag
FUNC BOOL LOAD_FX_MODELS()
	SWITCH iLoadFXflag
		CASE 0
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT001)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT002)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT003)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT004)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT005)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT006)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT007)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT008)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT009)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT010)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT011)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT012)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT013)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT014)
			REQUEST_MODEL(CS2_03_FMHSE_VFX_PARENT015)
			iLoadFXflag++
		BREAK
		CASE 1
	
			IF HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT001)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT002)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT003)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT004)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT005)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT006)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT007)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT008)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT009)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT010)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT011)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT012)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT013)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT014)
			AND HAS_MODEL_LOADED(CS2_03_FMHSE_VFX_PARENT015) 
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
	
proc stage_explosion_cut()
	//FUDGE_FIRE_TRAIL()

	SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME()
	

	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() and stageFlag > 0 and get_game_timer() > safeSkipTime
	
		WHILE NOT IS_SCREEN_FADED_OUT()				
			DO_SCREEN_FADE_OUT(1000)
			SAFEWAIT(28)
		ENDWHILE
		iLoadFXflag = 0
		WHILE NOT LOAD_FX_MODELS()
			safewait(32322)
		ENDWHILE
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << 2474.9697, 4946.0474, 44.0297 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 230.6937)	
		SET_PED_STEALTH_MOVEMENT(player_ped_id(),FALSE)
		SET_PED_USING_ACTION_MODE(player_ped_id(),FALSE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,true)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID()) 
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_PLAYER_CONTROL(player_id(),true)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		CLEAR_PRINTS()
		DEACTIVATE_AUDIO_SLOWMO_MODE("SLOW_MO_METH_HOUSE_RAYFIRE") 
		//#if IS_DEBUG_BUILD IF NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) #endif
		//STAGE_ENDS()
		//#if IS_DEBUG_BUILD endif #endif
		
		rfThisRayfire = GET_RAYFIRE_MAP_OBJECT(<<2457.15, 4968.79, 48.677>>, 45, "DES_FarmHs")            
     //   IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfThisRayfire)                        
    		//SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_ENDING)	                                                
		//endif
		kill_event(events_house_explodes_rayfire)
		
		cutChangeTime=0
		icamflag=icamflag
		icamflag=99
		stageFlag = 99
		
		iCurrentCamShot=3
		cutDurationTime = 10000
		////cprintln(DEBUG_TREVOR3,"HIDE MODEL GLASS")
		CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_ret_fhglassfrm,true)
		CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGLASSFRMSML,true)
		CREATE_MODEL_HIDE(<<2457.15, 4968.79, 48.677>>, 100,V_RET_FHGlassairfrm,false)
		STOP_STREAM()

		REPLAY_STOP_EVENT()
		bCutsceneWasSkipped = TRUE
	ENDIF

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SWITCH stageFlag
		case 0
			LOAD_FX_MODELS()
			bCutsceneWasSkipped = FALSE
			//reset basement stair gas trail coords so flame will appear higher up
			//nodes 49 - 55 raised by 30cm
			int iNode
			for iNode = 49 to 55
				CPRINTLN(debug_trevor3,"iNode b = ",iNode)
				gasTrail[iNode].coord += <<0,0,0.6>>
			ENDFOR
			
			IF IS_AUDIO_SCENE_ACTIVE("CHI_2_POUR_GAS")
				STOP_AUDIO_SCENE("CHI_2_POUR_GAS")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("CHI_2_SHOOT_GAS")
				STOP_AUDIO_SCENE("CHI_2_SHOOT_GAS")
			ENDIF
			
			START_AUDIO_SCENE("CHI_2_GAS_TRAIL_FIRE")
			
			//REQUEST_PTFX_ASSET()
			safeSkipTime = GET_GAME_TIMER() + 1000
			CLEAR_AREA(<<2570.5300, 4982.0571, 50.6819>>,100,true) //to remove a car that player may have left in the cutscene trigger zone
			SET_PLAYER_CONTROL(PLAYER_ID(),false)
			SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_UNARMED,true)
			PURGE_CONVERSATIONS(FALSE)
			bLastConvoWithoutSubtitles = FALSE
			PURGE_CONVERSATIONS(TRUE)
			
			
			CLEAR_PRINTS()
			
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<2467.774902,4954.414551,42.878033>>, <<2477.088867,4945.079102,46.166538>>, 4.062500,<<2486.8542, 4939.0859, 43.2578>>,230)
			
			IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
				//vehicle_index vehTemp
				//vehTemp = GET_VEHICLE_PED_IS_USING(player_ped_id())
				//SET_ENTITY_AS_MISSION_ENTITY(vehTemp)
				//SET_ENTITY_COORDS(vehTemp,<<2471.9463, 4949.7852, 44.1375>>)
				//SET_ENTITY_HEADING(vehTemp,222.7150)
				SET_ENTITY_COORDS(player_ped_id(),<<2455.9414, 4952.8013, 44.1120>>)
				//SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTemp)
			ENDIF
			
		
		//	add_event(events_Trevor_walks_in_final_cutscene)
		//	add_event(events_house_explodes_rayfire)
		//	add_event(events_play_audio_stream)
			
			SET_TIME_SCALE(1.0)
			
			
			//clean out memory
			REMOVE_ANIM_DICT("misschinese2_barrelRoll")
			REMOVE_ANIM_SET("move_m@gangster@var_e")
			REMOVE_ANIM_SET("move_m@gangster@var_f")
			REMOVE_ANIM_SET("move_m@gangster@generic")
			REMOVE_ANIM_DICT("misschinese2_bank5")
			REMOVE_ANIM_DICT("misschinese2_bank1")
			REMOVE_ANIM_DICT("reaction@male_stand@big_variations@b")
			REMOVE_ANIM_DICT("reaction@male_stand@big_intro@left")
			REMOVE_ANIM_DICT("reaction@male_stand@big_intro@right")
			REMOVE_ANIM_DICT("reaction@male_stand@big_intro@backward")
			
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Hillbilly_02)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_MethHead_01)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Fertilizer)
			SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_BEER_BOT_01)
	
			SET_PED_STEALTH_MOVEMENT(player_ped_id(),FALSE)
			int ij
			repeat count_of(zped) iJ
				if DOES_ENTITY_EXIST(zped[ij].id)
					DELETE_PED(zped[ij].id)
				endif
				IF DOES_ENTITY_EXIST(zped[ij].obj)
					DELETE_OBJECT(zped[ij].obj)
				ENDIF
			ENDREPEAT
									
			stageFlag++									
			
		break
		case 1		
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			stageFlag++			
		break
		
	ENDSWITCH
	
	//--------------------------- CUTSCENE EDITOR OUTPUT -----------------------------
	
	
	
	#if IS_DEBUG_BUILD
	IF NOT DOES_WIDGET_GROUP_EXIST(cutsceneWidget)
		int iWidget
		TEXT_LABEL_15 txtlbs
		SET_CURRENT_WIDGET_GROUP(widget_debug)
		cutsceneWidget = start_widget_group("Final Cutscene Edit")
			add_widget_bool("Edit Cutscene",editCutscene)
			add_widget_bool("Reset Cutscene",resetCutscene)
			add_widget_bool("Output Data",bOutputCut)
			REPEAT COUNT_OF(camShot) iWidget
				txtlbs = "Cut "
				txtlbs += iWidget
				start_widget_group(txtlbs)
					add_widget_bool("position cam start",bSetCamA[iWidget])
					add_widget_bool("Position can End",bSetCamB[iWidget])
					ADD_WIDGET_FLOAt_SLIDER("Cut Duration",camShot[iWidget].totalDuration,0,10000,100)
					ADD_WIDGET_FLOAT_SLIDER("Cam pan Duration",camShot[iWidget].camDuration,0,10000,100)				
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("Linear")
						ADD_TO_WIDGET_COMBO("Sin accel decel")
						ADD_TO_WIDGET_COMBO("Accel")
						ADD_TO_WIDGET_COMBO("Decel")
					STOP_WIDGET_COMBO("Pan type", camShot[iWidget].camPanType)
					
					ADD_WIDGET_INT_SLIDER("Move fire to node",camShot[iWidget].flameNode,-1,MAX_TRAIL_POINTS,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT
			REPEAT COUNT_OF(CutEvent) iWidget
			
				start_widget_group(CutEvent[iWidget].name)					
					ADD_WIDGET_INT_SLIDER("Trigger shot",CutEvent[iWidget].triggerShot,0,3,1)
					ADD_WIDGET_FLOAT_SLIDER("Trigger time",CutEvent[iWidget].triggerTime,0,10000.0,100.0)
					IF iWidget = 3
						ADD_WIDGET_FLOAT_SLIDER("Cam shake",fShakeValue,0,1.0,0.01)
					ENDIF
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		CLEAR_CURRENT_WIDGET_GROUP(widget_debug)
	ENDIF
	#endif

	int iCams
	IF bOutputCut
		bOutputCut=FALSE
		REPEAT COUNT_OF(camShot) icams
			PRINTLN("camShot[",iCams,"].camStartPos = ",camShot[iCams].camStartPos)
			PRINTLN("camShot[",iCams,"].camStartRot = ",camShot[iCams].camStartRot)
			PRINTLN("camShot[",iCams,"].camStartFOV = ",camShot[iCams].camStartFOV)
			PRINTLN("camShot[",iCams,"].camEndPos = ",camShot[iCams].camEndPos)
			PRINTLN("camShot[",iCams,"].camEndRot = ",camShot[iCams].camEndRot)
			PRINTLN("camShot[",iCams,"].camEndFOV = ",camShot[iCams].camEndFOV)
			PRINTLN("camShot[",iCams,"].camDuration = ",camShot[iCams].camDuration)
			PRINTLN("camShot[",iCams,"].totalDuration = ",camShot[iCams].totalDuration)
			PRINTLN("camShot[",iCams,"].camPanType = ",camShot[iCams].camPanType)
			PRINTLN("camShot[",iCams,"].flameNode = ",camShot[iCams].flameNode)
			
		ENDREPEAT
		
		REPEAT COUNT_OF(CutEvent) icams
			PRINTLN("CutEvent[",iCams,"].triggerShot = ",CutEvent[iCams].triggerShot)
			PRINTLN("CutEvent[",iCams,"].triggerTime = ",CutEvent[iCams].triggerTime)
		ENDREPEAT
	ENDIF

	
	REPEAT COUNT_OF(camShot) iCams
		If bSetCamA[iCams]
			bSetCamA[iCams]=FALSE
			camShot[iCams].camStartPos = GET_CAM_COORD(GET_RENDERING_CAM())
			camShot[iCams].camStartRot = GET_CAM_ROT(GET_RENDERING_CAM())
			camShot[iCams].camStartFOV = GET_CAM_FOV(GET_RENDERING_CAM())
			
		ENDIF
		If bSetCamB[iCams]
			bSetCamB[iCams]=FALSE
			camShot[iCams].camEndPos = GET_CAM_COORD(GET_RENDERING_CAM())
			camShot[iCams].camEndRot = GET_CAM_ROT(GET_RENDERING_CAM())
			camShot[iCams].camEndFOV = GET_CAM_FOV(GET_RENDERING_CAM())
		ENDIF
	ENDREPEAT
	
	IF resetCutscene
		resetCutscene = FALSE
		cutsceneRunning = FALSE
		bRayfireTriggered = FALSE
		CLEAR_AREA(<<2431.0969, 4967.0430, 41.3476>>,20,TRUE)
		CLEANUP_GAS_TRAILS()
	
		
	ENDIF
	
	IF NOT cutsceneRunning
		IF IS_SRL_LOADED()
			cprintln(debug_trevor3,"BEGIN_SRL")
			SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
			BEGIN_SRL()
			
			iCurrentCamShot = -1
			DESTROY_ALL_CAMS()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			REQUEST_ANIM_DICT("misschinese2_crystalmaze")
			
			cutsceneRunning = true
			CutEvent[0].triggered = FALSE		
			CutEvent[1].triggered = FALSE		
			CutEvent[2].triggered = FALSE		
			CutEvent[3].triggered = FALSE
			
			CutEvent[0].flag = 0
			CutEvent[0].timer = 0
			CutEvent[1].flag = 0
			CutEvent[1].timer = 0
			CutEvent[2].flag = 0
			CutEvent[2].timer = 0
			CutEvent[3].flag = 0
			CutEvent[3].timer = 0
			add_event(events_house_explodes_rayfire)
			
			PLAY_STREAM_FRONTEND()
			cprintln(debug_trevor3,"PLAY EXPLOSION SOUNDS")
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST) //added for NG
			SWITCH CUTSCENE_FIRST_SHOT
				CASE SHOT_NORMAL
					SETTIMERA(0)
					
				BREAK
				CASE SHOT_HOUSE_EXPLOSION
					CutEvent[0].triggered = TRUE
					CutEvent[1].triggered = TRUE
					SETTIMERA(10900)
					iCurrentCamShot=2
					cutDurationTime = 10000
				BREAK
				CASE SHOT_BASEMENT
					CutEvent[0].triggered = TRUE
					SETTIMERA(6400)
					iCurrentCamShot=1
					cutDurationTime = 10000
				BREAK
			ENDSWITCH				
			
			basementflame = 0
		ENDIF
	ENDIF
	
	IF cutsceneRunning	
		cprintln(debug_trevor3,"TIMER = ",to_float(timera()))
		SET_SRL_TIME(to_float(timera()))
	ENDIF
	
	IF cutsceneRunning
		cutDurationTime += TIMESTEP()
		cprintln(debug_Trevor3,"Fire = ",bUsePetrolFlameFX)
		////cprintln(debug_trevor3,"start: ",cutDurationTime," iCurrentCamShot: ",iCurrentCamShot)
		IF iCurrentCamShot = -1
		OR (iCurrentCamShot != -1 AND iCurrentCamShot < 4 AND cutDurationTime > (camShot[iCurrentCamShot].totalDuration / 1000.0))
			iCurrentCamShot++
			IF iCurrentCamShot = 3
				IF HAS_ANIM_DICT_LOADED("misschinese2_crystalmaze")
					cam1 = CREATE_CAMERA(CAMTYPE_ANIMATED,true)
					
					iCamSync = CREATE_SYNCHRONIZED_SCENE(<<2452.914,4962.096,45.585>>,<<0,0,45>>)
					PLAY_SYNCHRONIZED_CAM_ANIM(cam1,icamsync,"trevor_barn_walk_cam","misschinese2_crystalmaze")
					
					IF NOT IS_PED_INJURED(player_ped_id())
						TASK_SYNCHRONIZED_SCENE(player_ped_id(),iCamSync,"misschinese2_crystalmaze","trevor_barn_walk",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					ENDIF
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					CLEANUP_GAS_TRAILS()
					//remove all script fires
					
					SET_GAME_PAUSES_FOR_STREAMING(TRUE) // bug 2008155
					
					
					
					cutDurationTime = 0	
				ENDIF
			ELIF iCurrentCamShot < 4
				////cprintln(debug_trevor3,"next cam shot")
				DESTROY_ALL_CAMS()
				cam1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",  camShot[iCurrentCamShot].camStartPos,camShot[iCurrentCamShot].camStartRot, camShot[iCurrentCamShot].camStartFOV, TRUE)
				SET_CAM_PARAMS(cam1, camShot[iCurrentCamShot].camEndPos,camShot[iCurrentCamShot].camEndRot, camShot[iCurrentCamShot].camEndFOV, FLOOR(camShot[iCurrentCamShot].camDuration), int_to_enum(CAMERA_GRAPH_TYPE,camShot[iCurrentCamShot].camPanType))
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				IF iCurrentCamShot < 2
					SET_PETROL_FLAME_TO_NODE(camShot[iCurrentCamShot].flameNode,TRUE,TRUE)
				ELSE
					SET_PETROL_FLAME_TO_NODE(camShot[iCurrentCamShot].flameNode,TRUE,FALSE)
				ENDIF
				cutDurationTime = 0		
			
			ELSE
				IF NOT editCutscene
					DESTROY_ALL_CAMS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					AND NOT bCutsceneWasSkipped
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					ENDIF
					
					SET_PLAYER_CONTROL(player_id(),true)									
					SET_GAME_PAUSES_FOR_STREAMING(FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()		
					
					SET_MULTIHEAD_SAFE(FALSE)
					
					STOP_AUDIO_SCENE("CHI_2_GAS_TRAIL_FIRE")
					//g_eFarmhouseFireState = FARMHOUSE_FIRE_BURNING
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire, RFMO_STATE_ENDING)
					
					SET_TIME_SCALE(1.0)
					cprintln(debug_trevor3,"END_SRL()")
					END_SRL()
					
					IF IS_SCREEN_FADED_OUT()
						WHILE GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfThisRayfire) != RFMO_STATE_END						
							SAFEWAIT(29)
						ENDWHILE
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					cprintln(debug_trevor3,"END_stage")
					REPLAY_STOP_EVENT()
										
					STAGE_ENDS(end_mission)
				
				ENDIF
			ENDIF
		ENDIF
	
		////cprintln(debug_trevor3,"end: ",cutDurationTime," iCurrentCamShot: ",iCurrentCamShot)
		int iEvent
		REPEAT COUNT_OF(CutEvent) iEvent
			IF NOT cutEvent[iEvent].triggered
				IF iCurrentCamShot = cutEvent[iEvent].triggerShot
				AND cutDurationTime > (cutEvent[iEvent].triggerTime / 1000.0)
					
					SWITCH iEvent
						CASE 0
							SWITCH cutEvent[iEvent].flag
								CASE 0 //table flames
									basementFlame = 0
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD(basementFlames[basementFlame],false)
										cutEvent[iEvent].timer = GET_GAME_TIMER() + 200
										cutEvent[iEvent].flag++
									ENDIF
								BREAK
								CASE 1
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD((basementFlames[basementFlame] + basementFlames[basementFlame+1])/2.0,false)
										basementFlame++				
										IF basementFlame >= 12
											cutEvent[iEvent].flag = 10
											cutEvent[iEvent].triggered = TRUE
										ELSe
											cutEvent[iEvent].timer = GET_GAME_TIMER() + 200								
											cutEvent[iEvent].flag=2
										ENDIF
									ENDIF
								BREAK
								CASE 2
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD(basementFlames[basementFlame],false)
										cutEvent[iEvent].timer = GET_GAME_TIMER() + 200
										IF basementFlame = 2
											cutEvent[iEvent].flag=3
										ELSE
											cutEvent[iEvent].flag=1
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD((basementFlames[basementFlame] + basementFlames[basementFlame+1])/2.0,false)
										ADD_FLAME_AT_COORD((basementFlames[basementFlame] + basementFlames[basementFlame+2])/2.0,false)
										cutEvent[iEvent].timer = GET_GAME_TIMER() + 200
										cutEvent[iEvent].flag=4
										basementFlame=3
									ENDIF						
								BREAK
								CASE 4
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD(basementFlames[basementFlame],false)
										ADD_FLAME_AT_COORD(basementFlames[basementFlame+1],false)
										cutEvent[iEvent].timer = GET_GAME_TIMER() + 200
										cutEvent[iEvent].flag=5
									ENDIF						
								BREAK
								CASE 5
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD((basementFlames[basementFlame] + basementFlames[basementFlame+2])/2.0,false)
										ADD_FLAME_AT_COORD((basementFlames[basementFlame+1] + basementFlames[basementFlame+3])/2.0,false)
										cutEvent[iEvent].timer = GET_GAME_TIMER() + 200
										cutEvent[iEvent].flag=6
										basementFlame=5
									ENDIF						
								BREAK
								CASE 6
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										ADD_FLAME_AT_COORD(basementFlames[basementFlame],false)
										ADD_FLAME_AT_COORD(basementFlames[basementFlame+1],false)
										cutEvent[iEvent].timer = GET_GAME_TIMER() + 200
										cutEvent[iEvent].flag=2
										basementFlame = 7
									ENDIF						
								BREAK
							ENDSWITCH
								
									
							
						BREAK
						CASE 1 //table explosino
							ADD_EXPLOSION(<<2432.5339, 4968.3828, 42.2389>>,EXP_TAG_ROCKET)
							cutEvent[iEvent].triggered = TRUE
						BREAK
						CASE 2 //house rayfire
							bRayfireTriggered = TRUE
							cutEvent[iEvent].triggered = TRUE
						BREAK
						CASE 3 //cam explosion shake
							IF DOES_CAM_EXIST(cam1)
								SHAKE_CAM(cam1, "LARGE_EXPLOSION_SHAKE", fShakeValue)
								SET_CAM_MOTION_BLUR_STRENGTH(cam1, 0.1000)
								cutEvent[iEvent].triggered = TRUE
							ENDIF
						BREAK
						CASE 4
							SWITCH cutEvent[iEvent].flag
								CASE 0 
									SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2473.2080, 4947.6245, 44.0664>>)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), 223.4930)	
									SET_GAMEPLAY_CAM_RELATIVE_HEADING()
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
									
									//TASK_FOLLOW_NAV_MESH_TO_COORD(player_ped_id(),<<2476.3506, 4945.6670, 43.9052>>,pedmove_Walk)
									IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
										FORCE_PED_MOTION_STATE(player_ped_id(), MS_AIMING, true, FAUS_DEFAULT )
										SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 3000)
									ELSE
										FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_WALK, true, FAUS_DEFAULT )
										SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 0500)
									ENDIF
									//cutEvent[iEvent].triggered = TRUE
									cutEvent[iEvent].flag++
									cprintln(Debug_Trevor3,"Force walk triggered b")
								BREAK
								CASE 1
									//FORCE_PED_MOTION_STATE(player_ped_id(), MS_AIMING, true, FAUS_DEFAULT )
									//SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 0500)
								BREAK
							ENDSWITCH
						BREAK
						CASE 5
							SWITCH cutEvent[iEvent].flag
								CASE 0 
									fSlowMoPhase = 1.0
									cutEvent[iEvent].flag++
									cutEvent[iEvent].timer = GET_GAME_TIMER() + 1300
									ACTIVATE_AUDIO_SLOWMO_MODE("SLOW_MO_METH_HOUSE_RAYFIRE")
								BREAK
								CASE 1
									fSlowMoPhase -= timestep() * 1.0
									IF fSlowMoPhase < 0.2
										fSlowMoPhase = 0.2
									ENDIF
									SET_TIME_SCALE(fSlowMoPhase)
									IF GET_GAME_TIMER() > cutEvent[iEvent].timer
										cutEvent[iEvent].flag++
									ENDIF
								BREAK
								CASE 2
									fSlowMoPhase += timestep() * 1.0
									IF fSlowMoPhase > 1.0
										fSlowMoPhase = 1.0
										cutEvent[iEvent].flag++
										cutEvent[iEvent].triggered = TRUE
									ENDIF
									DEACTIVATE_AUDIO_SLOWMO_MODE("SLOW_MO_METH_HOUSE_RAYFIRE") 
									SET_TIME_SCALE(fSlowMoPhase)
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDREPEAT
	endif
	
	
	
endproc



proc stage_end_mission()

	checkConditions(COND_FARM_LEAVE_START,COND_FARM_LEAVE_END)

	//action(0,ACT_GET_WANTED_LEVEL,PLAYOUT_ON_TRIGGER,cIF,COND_GET_WANTED_LEVEL)
	action(1,ACT_CHANGE_BUILDING_STATE,PLAYOUT_ON_TRIGGER,cIF,COND_PLAYER_300_FROM_FARM)
	action(2,ACT_STOP_MUSIC)
	action(3,ACT_FIRE_AUDIO)
	action(4,ACT_ENABLE_TAXI)
	action(5,ACT_RESET_STUFF)
	
	DIALOGUE(0,DIA_BOOM)
	
	INSTRUCTIONS(0,INS_GET_CLEAR_OF_FARM)
	//INSTRUCTIONS(1,INS_LOSE_WANTED_LEVEL,cIF,COND_BUILDING_STATE_SWAPPED,cAND,COND_GOT_WANTED_LEVEL)
	
	CONTROL_BURN_PLAYER()
	
	IF IS_ACTION_COMPLETE(1,ACT_CHANGE_BUILDING_STATE)
//	AND IS_CONDITION_FALSE(COND_GOT_WANTED_LEVEL)	
		Mission_Passed()
	ENDIF
		
endproc

PROC conrol_mission()

	mission_stage_flag lastMissionStage = missionStage //track if a stage has changed
	SET_REPLAY_STAGE()
	SWITCH missionStage
		case init_mission
			stage_init()
		BREAK
		case intro_cut
			stage_intro_cut()
		BREAK		
		case drive_to_farm
			stage_drive_to_farm()
		break
		case farm_cutscene
			stage_farm_cutscene()
		BREAK
		case snipe_from_hill
			stage_get_to_meth_lab()
		BREAK		
		case get_to_house
			stage_get_to_meth_lab()
		break
		case inside_house
			stage_get_to_meth_lab()
		break
		case do_petrol_trail
			stage_petrol_trail()
		break
		case explosion_cut
			stage_explosion_cut()
		break
		case end_mission
			stage_end_mission()			
		break
	ENDSWITCH
	
	if missionStage != lastMissionStage stageFlag = 0 ENDIF
	
	IF bFailMission
		Mission_Failed()
	endif

ENDPROC

proc updateAIBlips()
	int i
	
	IF missionStage >= snipe_from_hill

		REPEAt count_of(zped) i
			IF DOES_ENTITY_EXIST(zped[i].id)
				IF NOT zped[i].registeredKilled
					IF IS_PED_INJURED(zped[i].id)					
						zped[i].registeredKilled = TRUE
						INFORM_MISSION_STATS_OF_INCREMENT(CHI2_KILLS)
					ENDIF
				ENDIF
				IF GET_PED_RELATIONSHIP_GROUP_HASH(zped[i].id) = HASH_FOE_GROUP									
					UPDATE_AI_PED_BLIP(zped[i].id, zped[i].aiBlip,-1,null)//,IS_PLAYER_USING_TREVORS_SPECIAL_ABILITY())
				ENDIF
			ELSE
				IF zped[i].id != null
					CLEANUP_AI_PED_BLIP(zPED[i].aiBlip)
					zped[i].id = null
				ENDIF
			ENDIF
		endrepeat
	ENDIF
endproc



SCRIPT	


	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()		
		MISSION_CLEANUP(CLEANUP_FAIL_DEATH)
	ENDIF

	fireNodeJumpTime = 160.0

	WHILE (TRUE)
		IF NOT IS_PED_INJURED(player_peD_id()) //never used to have to do this. Something's fucked.
			conrol_mission()
			updateAIBLips()
		
			#if IS_DEBUG_BUILD manage_debug() #ENDIF		
			
			IF missionStage >= do_petrol_trail
				CONTROL_GAS_TRAILS(#if is_debug_build widget_debug #endif)
			ENDIF
			CONTROL_CONVERSATION_SUBTITLES()
			manage_peds()
			manage_events()	
			control_music()
			INTERIOR_PINNING()
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("m_Chinese2")
		ENDIF
		
		convAlreadyCalledThisFrame=FALSE
		
		#if is_Debug_build  pedReactionsDebug(pedReactionState,widget_debug) #endif
		
		WAIT(0)
	endwhile
ENDSCRIPT

