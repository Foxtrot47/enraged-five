
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//Uber car chase data
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					1
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					1	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				4	
// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		1
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		4					

// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		1
//const_int max_attached_objects							30
//const_int maximum_real_setpiece_vehicles				4


const_int total_number_of_wave_0_enemies 9
const_int total_number_of_wave_0_inside_enemies 4
const_int total_number_of_wave_0_vehicles 3

const_int total_number_of_wave_2_enemies 20
const_int total_number_of_wave_2_vehicles 8
// @SBA - to track rushing enemies
CONST_INT kiNumberOfWaveRushersAllowed 2

const_int total_number_of_wave_3_enemies 4
const_int total_number_of_wave_3_vehicles 1

const_int total_number_of_ambient_enemies 4
const_int total_number_of_ambient_enemy_vehicles 2

const_int total_number_of_the_parked_cars 4


CONST_INT MAX_SKIP_MENU_LENGTH 6        // number of stages in mission



USING "rage_builtins.sch"
USING "globals.sch"
using "script_heist.sch"
USING "flow_public_core_override.sch"
using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "commands_misc.sch"
using "commands_recording.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "model_enums.sch"
using "cellphone_public.sch"
using "selector_public.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
using "chase_hint_cam.sch"
using "locates_public.sch"
using "script_blips.sch" 
using "traffic.sch"
using "select_mission_stage.sch"
using "replay_public.sch"
using "commands_cutscene.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "CompletionPercentage_public.sch"
using "building_control_public.sch"
using "clearmissionarea.sch"
using "emergency_call.sch"
USING "taxi_functions.sch"
USING "script_misc.sch"


#IF IS_DEBUG_BUILD
	USING "rerecord_recording.sch"
#ENDIF

struct ped_structure
	ped_index ped 
	model_names model 
	blip_index blip
	vector pos 
	vector run_to_pos
	float heading 
	int health 
	int accuracy
	bool damaged_by_player
	bool allow_body_armour
	bool created 
	BOOL bRushInside 		// @SBA - flag for tracking when an enemy has been tasked inside
	BOOL been_killed
	weapon_type weapon
	text_label name
endstruct 


struct vehicle_struct
	vehicle_index veh
	model_names model
	blip_index blip
	vector pos 
	float heading
	float speed
	float skip_time
	int health
	float engine_health
	float petrol_tank_health
	int colour
	int recording_number
	bool been_destroyed 
endstruct

struct object_struct 
	object_index obj
	blip_index blip
	model_names model
	vector pos 
	vector offset_pos
	vector offset_pos_2
	vector rot
	vector offset_rot
	float heading
	float scale 
	float health
	string room_name
	int time
	bool been_created
endstruct 

struct quaternion
	float x
	float y
	float z
	float w
endstruct

struct decal
	DECAL_ID the_decal_id
	DECAL_RENDERSETTING_ID decal_texture_id
	vector pos
	vector direction
	vector side
	float width
	float height
	float fAlpha
	float life
	float wash_amount
	blip_index blip
	bool decal_removed
endstruct

struct cover_point_struct
	vector pos
	vector cover_from_pos
	float heading
	coverpoint_index cover_point
endstruct 


enum main_mission_flow
	intro_mocap = 0, 
	get_to_meth_lab,
	wave_0_system, 
//	wave_1_system,  @SBA - NO LONGER ACTIVE
	wave_2_system,
	wave_3_system,
	mission_passed_mocap, 
	vehicle_recording, 
	load_stage_selector_assets, 
	mission_fail_stage
endenum 

enum main_mission_shootout
	initial_attack_phase = 0,
	get_into_pos, 
	run_to_other_side_of_building, 
	get_to_other_side_of_building, 
	run_to_front_of_building,
	get_to_front_of_building,
	run_into_shop, 
	get_into_shop,
	charge_back_stairs,		// @SBA - added this one to handle survivors 
	combat_player,
	run_away,
	attack_from_pos_2,
	grenade_launcher_explosion,
	do_nothing
endenum

main_mission_flow mission_flow = intro_mocap
main_mission_shootout wave_0_status[total_number_of_wave_0_enemies] 
main_mission_shootout wave_2_status[total_number_of_wave_2_enemies] 
main_mission_shootout wave_3_status[total_number_of_wave_3_enemies] 
main_mission_shootout ambient_enemy_status[total_number_of_ambient_enemies]

ped_structure buddy
ped_structure translator
ped_structure cheng
ped_structure wave_0_enemy[total_number_of_wave_0_enemies]
ped_structure wave_2_enemy[total_number_of_wave_2_enemies]
ped_structure wave_3_enemy[total_number_of_wave_3_enemies]
ped_structure wave_0_inside_enemy[total_number_of_wave_0_inside_enemies]
ped_structure ambient_enemy[total_number_of_ambient_enemies]

// enemies
MODEL_NAMES	GoonModel1	= G_M_Y_MexGoon_02
MODEL_NAMES	GoonModel2	= G_M_Y_Azteca_01

vehicle_struct trevors_truck
vehicle_struct wave_0_enemy_vehicle[total_number_of_wave_0_vehicles]
vehicle_struct wave_2_enemy_vehicle[total_number_of_wave_2_vehicles]
vehicle_struct wave_3_enemy_vehicle[total_number_of_wave_3_vehicles]
vehicle_struct ambient_enemy_vehicle[total_number_of_ambient_enemy_vehicles]

object_struct explosive_barrels[4]
object_struct bins[3]
object_struct c4[3]
object_struct cover_box

cover_point_struct buddy_cover_point_data[6]

//decal blood[2]

// @SBA - stole this from Bob (RJM). This is a structure that's designed to give a name to angled areas. 
// I find it difficult to look at an angled-area check, and know where that is, and what its purpose is.
// With this, you can create a "Trigger Box" variable with a name, and you can easily tell by the name, the purpose of it.
STRUCT TRIGGER_BOX
      VECTOR            vMin
      VECTOR            vMax
      FLOAT     		flWidth
ENDSTRUCT


bool stop_mission_fail_checks = false
bool wave_0_complete = false 
bool wave_2_complete = false 
bool wave_3_complete = false
bool allow_buddy_ai_system = true 
bool ram_cutscene_finished = false
bool allow_ambient_enemy_blips_to_flash = false
bool display_blips_as_friendly = false
bool no_vehicle_recording_skip = false 
bool fence_shot = false
bool grenade_launcher_cutscene_playing = false
bool stop_player_proofing = false
bool use_main_cover_point = true
bool refresh_the_task = false
bool grenade_launcher_first_person_cam_active = false


int i
int original_time 
int waypoint_node_pos = 0
int total_number_of_points = 0
int remaining_enemies = 0
int player_waypoint_status = 22
int dialogue_time
int dialogue_time_2
int buddy_waypoint_target_node = 0
int buddy_time
int player_grenade_launcher_cutscene_status = 0
int cutscene_index	
int uber_vehicle_status = 0
int cutscene_trigger_time 
int wave_0_inside_enemy_status[total_number_of_wave_0_inside_enemies]
int get_to_meth_lab_status = 0
int intro_mocap_status = 0
int vehicle_recording_system_status = 0
int get_to_meth_lab_dialogue_system_status = 0
int mission_passed_mocap_status = 0
int i_triggered_text_hashes[50]
int flash_blip_time = 0
int proofing_time = 0
int attack_time = 0
int get_to_meth_lab_audio_scene_system_status = 0
int wave_0_audio_scene_system_status = 0
int wave_2_audio_scene_system_status = 0
int wave_3_audio_scene_system_status = 0

#IF IS_DEBUG_BUILD
	int get_to_meth_lab_skip_status = 0
	int wave_0_system_skip_status = 0
	int wave_3_system_skip_status
	int p_skip_time
#endif 

//*****not present in initialise_mission_variables()
int launch_mission_stage_menu_status = 0

//wave 0
INT iWave0SetupStage = 0
int wave_0_master_flow_system_status = 0
int wave_0_dialogue_system_status = 0
int wave_0_buddy_ai_system_status = 0
int ramming_gates_cutscene_status = 0
int wave_0_assisted_nodes_system_status = 0

//wave 1 - REMOVED @SBA

//wave2
int wave_2_master_flow_system_status = 0
int wave_2_dialogue_system_status = 0
int wave_2_buddy_ai_system_status = 0
int wave_2_assisted_nodes_system_status = 0

//wave3
int wave_3_master_flow_system_status = 0
int wave_3_dialogue_system_status = 0
int wave_3_buddy_ai_system_status = 0
int wave_3_assisted_nodes_system_status = 0

vector player_position
vector target_waypoint_pos 
vector cutscene_pos
vector cutscene_rot


string buddy_waypoint_string
string mission_failed_text

vehicle_index parked_cars[total_number_of_the_parked_cars]
vehicle_index wave_1_uber_vehicle
vehicle_index ambient_car[5]

object_index players_weapon_obj

camera_index camera_a
camera_index camera_b
//camera_index camera_c
//camera_index camera_d

sequence_index seq

interior_instance_index meth_interior

coverpoint_index cpWaterHeaterCover // @SBA - added for wave 0 rushers
coverpoint_index buddy_cover_point[2]
coverpoint_index players_cover_point
coverpoint_index players_cover_point_2
coverpoint_index wave_3_0_cover_point
coverpoint_index wave_3_1_cover_point
coverpoint_index cover_point_back_roof_AC


blip_index outside_meth_lab_blip
blip_index meth_lab_blip
blip_index ice_box_blip

int remaining_enemy_status[2]
ped_index remaining_enemy[2]

//ped_index blood_ped

structPedsForConversation scripted_speech[2]

LOCATES_HEADER_DATA locates_data

#IF IS_DEBUG_BUILD
widget_group_id chinese_1_widget_group
MissionStageMenuTextStruct menu_stage_selector[MAX_SKIP_MENU_LENGTH]
int menu_return_stage = 0
#endif 

// *** @SBA - additional variables***

// to remember if player received the grenade launcher during the mission
CONST_INT 		PLAYER_RECEIVED_GRENADE_LAUNCHER 	0	//Array positino for replay globals
// to remember which vehicles the player destroyed 
CONST_INT 		PLAYER_DESTROYED_VEHICLES 			1	//Array positino for replay globals
	
CONST_INT 		NOT_GOT_GLAUNCHER 			0
CONST_INT 		HAS_GOT_GLAUNCHER 			1

CONST_INT		WAVE0_BOTH_CARS_DEAD		-1
CONST_INT		WAVE0_ONLY_CAR0_ALIVE		0
CONST_INT		WAVE0_ONLY_CAR1_ALIVE		1
CONST_INT		WAVE0_BOTH_CARS_ALIVE		2

// The ammount of ammo in the grenade launcher
CONST_INT		kiGrenadeLauncherAmmo	5 // @SBA - changed to 5 pere Leslie's feedback.  OLD = 3

BOOL 			bPlayerInVehicle

// bar
PED_INDEX		piBarLady
PED_INDEX		piJosef
PED_INDEX		piOldMan1A
PED_INDEX		piOldMan2
PED_INDEX		piDrunk
BOOL			bBarPatronsCreated
BOOL			bIsBarCleanedUp

SCENARIO_BLOCKING_INDEX		sbiBarBlockingArea

// For timing the conversation
INT				iGetToMethLabDialogTimer

// For counting hits to the Blue SUV (init'd as needed)
INT				iSuvDamageCount = 0

// timer for wave rushes
INT				iWave_Rush_Time = 0
// Duration of wait before rush
INT				iWaitTimeBeforeRush = 0
// Rush counter
INT				iWaveRushersSoFar = 0

// IDs for navmesh blocking objects
INT				iNavBlockingUpstairsDoor
INT				iNavBlockingUpstairsDoorway
INT				iNavBlockingBackYardTrailer

// Timer to delay the order for the next rusher to rush 
structtimer		tmrNextRusherDelay

BOOL			bClearCarGen = FALSE

// Checking player's vehicle for suitability
BOOL			bTrevorsVehicleIsOkay = FALSE

// gating a load
BOOL			bLoadedLastShootoutStuff = FALSE

// For gating a check to to break the window
BOOL			bPlayerInOpeningCover = FALSE

// Gating the start of the initial wave
BOOL			bStartedInitialWave = FALSE
BOOL			bInitialWavePropsMade = FALSE

BOOL			bSetTaxiDropoff = FALSE

// chef run back stairs
BOOL			bChefRunsToBackStairs = FALSE
structtimer		tmrChefRunsDelay

// gate for door
BOOL			bBackStairsDoorClosed = FALSE

// Has player used special ability
BOOL 			bPlayerUsedAbility = FALSE

// sound played yet?
BOOL 			bPlayGunFire = FALSE

// Meth lab arrival cutscene resolve volume
TRIGGER_BOX		tbArrivalCutResolveVol
// Volume outside of front doors
TRIGGER_BOX		tbFrontDoorVolume
// Volume for upstairs in meth lab for wave 0 fight
TRIGGER_BOX		tbUpstairsRushVolume
// In the room where Trevor takes cover
TRIGGER_BOX		tbUpstairsCloserRushVolume
// Volume for alley to the back stairs (outside)
TRIGGER_BOX		tbBackStairsAlleyVolume
// Volume on the back stairs, starting several steps up (outside)
TRIGGER_BOX		tbOnBackStairsVolume
// Volume for top of the back stairs (outside)
TRIGGER_BOX		tbTopBackStairsVolume
// Volume that covers all the back stairs and the landing (outside)
TRIGGER_BOX		tbFullBackStairsVolume
// Volume for area outside gates, by gas station 
TRIGGER_BOX		tbOutsideMainGatesVolume
// Volumes for the back yard, back roof, and just beyond the back gate(wave 2)
TRIGGER_BOX		tbBackYardVolume
TRIGGER_BOX		tbBackRoofVolume
TRIGGER_BOX		tbBackFullRoofVolume  	// abandonment volume
TRIGGER_BOX		tbBackUpstairsRoomVolume
TRIGGER_BOX		tbBackGateVolume
// Volume inside the liquor store (wave 3)
TRIGGER_BOX		tbLiquorStoreVolume
// Volumes for final cutscene trigger
TRIGGER_BOX		tbIceBoxInnerVol
TRIGGER_BOX		tbIceBoxOuterVol
TRIGGER_BOX		tbIceBoxCameraVol


// Arrival Lead-in variables
ENUM ARRIVAL_LEAD_IN_STATE
	ALI_HALT_VEHICLE,
	ALI_CAM_INTERP,
	ALI_FINISH_DRIVE,
	ALI_EXIT_VEHICLE,
	ALI_VEHICLE_STOP,
	ALI_FINISH_INTERP,
	ALI_WAIT_FOR_TIMER
ENDENUM
ARRIVAL_LEAD_IN_STATE ArivialLeadInState = ALI_HALT_VEHICLE

VECTOR			vArrivalMethLab		= <<1406.26929, 3598.70752, 34.9>>	// <<1403.1777, 3597.2070, 34.5>>
structTimer		tmrArrival

VECTOR			vVehicleLeadinLocR	= <<1425.60095, 3604.64648, 33.89210>>
VECTOR			vVehicleLeadinLocL	= <<1382.39490, 3580.00366, 34.01978>>

VECTOR			vTrevorsVehicleMethLabLoc = <<1409.9990, 3600.4973, 33.8676>>
FLOAT			fTrevorsVehicleMethLabHead = 108.7487

// Vector for Trevor's start pos in window (cover)
VECTOR 			vTrevorWindowStartPos = <<1389.7571, 3599.7686, 37.9419>>
// Vector for Trevor's pos on the back deck (cover)
VECTOR 			vTrevorBackDeckStartPos = <<1409.21973, 3612.25244, 38.005>> //<<1409.44043, 3611.84180, 38.00555>>
// Vector for Trevor's pos in the liquor store (cover)
VECTOR 			vTrevorLiquorStartPos = <<1391.1561, 3607.2720, 33.9809>>
// Back door to roof
VECTOR			vBackDoorToRoof = <<1399.7, 3607.8, 39.1>>
// Door to back stairs
VECTOR			vDoorToBackStairs = <<1388.5, 3614.8, 39.1>>
// Door between liquor store and interior stairs
VECTOR			vInteriorDoorStore = <<1395.61, 3609.33, 35.13>>
// Cover point used by teh chef in 2nd part of wave 0
VECTOR			vChefCoverPointBackStairs = <<1384.0, 3615.31, 37.93>>
VECTOR			vChefCoverPointBackStairs2 = <<1383.3361, 3617.2415, 37.9259>>

// chef wave 3
VECTOR			vChefCoverPointLiquorStore = <<1393.85, 3608.19, 33.98>>

// Timer used to determine when an abandoned chef message should play
structtimer		tmrAbandonChef
// Timer used to determine when an LOS check should happen (if not in LOS when Chef should die, kill him)
structtimer		tmrCheckChefForLOS
//For timing the outbursts uttered by the Chef when in danger
structtimer		tmrChefOutburstTimer
// Outburst time frequency
FLOAT			fOutburstTime
// For timing when the Chef should die when abandoned 
structtimer		tmrChefDeathTimer
// Time from when being in danger to when the Chef dies
CONST_FLOAT		kfChefDeathTime	8.000
// Timer used for a reminder if player takes too long getting Cheng out of ice box
structtimer		tmrIceBoxReminder
// Timer used for the blend-in to the final cutscene
structtimer		tmrIceBoxBlendin
// Timer for Chef's move from the roof to the shop
structtimer		tmrStartChefMove
// Timer for Chef's move from the roof to the shop
structtimer		tmrWave3ChefFailsafe

// For enemy dialogue after player uses grenade launcher
INT 			iEnemyGrenLaunchDialogue = 0
structTIMER 	tmrGrenLaunchTimer
CONST_FLOAT 	kfGrenLaunchWaitTime 1.500
PED_INDEX 		piEnemyToSpeak

// Gren launcher pickup
PICKUP_INDEX	puiGrenadeLauncher
// Flag for whether to skip the grn launcher cutscene (if played already got the launcher)
BOOL			bSkipGrenLaunchCutscene = FALSE
// Flag for setting pickup ammo
BOOL			bPickupAmmoSet = FALSE

// blend-in to final cutscene
INT		 		CHENG_CUTSCENE_INDEX
INT		 		TRANSLATOR_CUTSCENE_INDEX

MODEL_NAMES		mnIceBox = P_ICE_BOX_01_S //prop_ice_box_01
MODEL_NAMES		mnIceBoxProxy = P_Ice_Box_Proxy_col 
OBJECT_INDEX 	oiIceBox
OBJECT_INDEX 	oiIceBoxProxy
VECTOR			vIceBoxPos = <<1388.6, 3599.4, 34.8>>
VECTOR			vIceBoxRot = <<0, 0, 290.0>>

// @SBA - new dialogue
// Enemies have gotten inside
STRING			sChefWarn1 = "attack_int"
// Enemies are in the same/adjacent room
STRING			sChefWarn2 = "They_Here"
// Enemies are approaching the back stairs after truck ram 
STRING			sChefBackStairsWarn = "They_Coming"
// Chef is in danger, needs help (death timer)
STRING			sChefHelp1 = "Need_Help"
// Chef makes a periodic short exclamation about being in danger
STRING			sChefOutburst = "Outburst"
// Chef took enough damage
STRING			sChefHurt = "chin_killed"

BOOL			bChefComeOn = FALSE
BOOL			bTellChefGetInside = FALSE
BOOL			bSetIceBoxBlip = FALSE

// Cheng car dances
STRING			sCrazyDanceDict = "MISSChinese1CrazyDance"
STRING 			sCrazyDanceBase = "Crazy_Dance_BASE"
STRING 			sCrazyDance1 = "Crazy_Dance_1"
STRING 			sCrazyDance2 = "Crazy_Dance_2"
STRING 			sCrazyDance3 = "Crazy_Dance_3"	// craziest
STRING 			sDanceMove = sCrazyDanceBase

BOOL			bDanceAnimDictUnloaded = FALSE

// Timer for Cheng starting/stopping dance
INT			iChengDanceTime
INT			iChengDanceStopTime = -1

ENUM CHENG_DANCE_STATE
	eCDS_BaseAnim,
	eCDS_FlavorAnim
ENDENUM
CHENG_DANCE_STATE	eCDS_DanceState

// ending
BOOL			bOutroShitSkip = FALSE
BOOL			bStartTranslatorEmergeBox = FALSE
BOOL			bMissionPassed = FALSE
VEHICLE_INDEX	viGetAwayCar


// *** End additional variables ***


// *** @SBA - Trigger Box Functions ***
// I stole these from Bob (RJM).  They proved a means of naming an angled area and using that name
// to create and check the area
FUNC TRIGGER_BOX CREATE_TRIGGER_BOX(VECTOR vMin, VECTOR vMax, FLOAT flWidth)
      TRIGGER_BOX tbRetVal
      
      tbRetVal.vMin = vMin
      tbRetVal.vMax = vMax
      tbRetVal.flWidth = flWidth

      RETURN tbRetVal
ENDFUNC

FUNC BOOL IS_ENTITY_IN_TRIGGER_BOX(ENTITY_INDEX ent, TRIGGER_BOX trigger, BOOL bDoDeadCheck = TRUE)
    IF NOT DOES_ENTITY_EXIST(ent)
	  	RETURN FALSE
	ENDIF
	IF bDoDeadCheck
	AND IS_ENTITY_DEAD(ent)
		RETURN FALSE
	ENDIF
    IF IS_ENTITY_IN_ANGLED_AREA(ent, trigger.vMin, trigger.vMax, trigger.flWidth)
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

// @SBA - Returns TRUE if any ped in the given ped structure is in the given trigger box
FUNC BOOL IS_ANY_PED_IN_TRIGGER_BOX(PED_STRUCTURE &psPedInfo[], TRIGGER_BOX trigger)
	INT idx
	FOR idx = 0 TO (COUNT_OF(psPedInfo) - 1)
		IF IS_ENTITY_IN_TRIGGER_BOX(psPedInfo[idx].ped, trigger)
			//CPRINTLN(DEBUG_MISSION, "IS_ANY_PED_IN_TRIGGER_BOX: This Ped is in a trigger box: ", psPedInfo[idx].name)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

// @SBA - Returns the PED_INDEX of the first ped in the given ped structure that is found alive in the given trigger box
FUNC PED_INDEX GET_FIRST_LIVING_PED_IN_TRIGGER_BOX(PED_STRUCTURE &psPedInfo[], TRIGGER_BOX trigger)
	INT idx
	FOR idx = 0 TO (COUNT_OF(psPedInfo) - 1)
		IF IS_ENTITY_IN_TRIGGER_BOX(psPedInfo[idx].ped, trigger)
		    #IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "GET_FIRST_LIVING_PED_IN_TRIGGER_BOX: This Ped is alive in a trigger box: ", psPedInfo[idx].name)
			#ENDIF
			RETURN psPedInfo[idx].ped
		ENDIF
	ENDFOR
	RETURN NULL
ENDFUNC

FUNC BOOL IS_PLAYER_IN_TRIGGER_BOX(TRIGGER_BOX trigger)
      RETURN IS_ENTITY_IN_TRIGGER_BOX(PLAYER_PED_ID(), trigger)
ENDFUNC

// @SBA - only remake truck, then set attributes
PROC RECREATE_PLAYERS_VEHICLE(VECTOR vPos, FLOAT vHead, BOOL bSetVehicleLocked = TRUE)
	
	CREATE_VEHICLE_FOR_REPLAY(trevors_truck.veh, vPos, vHead, FALSE, FALSE, FALSE, FALSE, TRUE, BODHI2, 32, CHAR_TREVOR)

	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(trevors_truck.veh,CHI1_CAR_DAMAGE)
	
	IF IS_VEHICLE_DRIVEABLE(trevors_truck.veh)
		IF bSetVehicleLocked
			set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_locked)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, false)
		ENDIF
		set_vehicle_engine_on(trevors_truck.veh, false, true)
	ENDIF
ENDPROC

//*** @SBA - Grenade Launcher Related Functions ***
// @SBA - set ammo in grenade launcher to desired ammount
PROC SET_GRENADE_LAUNCHER_AMMO()

	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), weapontype_grenadelauncher)
		SET_AMMO_IN_CLIP(PLAYER_PED_ID(), weapontype_grenadelauncher, 0)
		SET_PED_AMMO(PLAYER_PED_ID(), weapontype_grenadelauncher, kiGrenadeLauncherAmmo)
		CPRINTLN(DEBUG_MISSION,"SET_GRENADE_LAUNCHER_AMMO: Setting ammo to ", kiGrenadeLauncherAmmo)
	ENDIF
	
ENDPROC

// @SBA - create the grenade launcher pickup (which Chef will throw to player)
PROC SETUP_GRENADE_LAUNCHER_PICKUP()

	// @SBA - remove the pickup on the roof to ensure we get a new one on restart
	IF DOES_PICKUP_EXIST(puiGrenadeLauncher)
		REMOVE_PICKUP(puiGrenadeLauncher)
	ENDIF
	
	// set placement flags
	INT PlacementFlags
	PlacementFlags = 0
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_FIXED))
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_LOCAL_ONLY)) 

	puiGrenadeLauncher = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_GRENADELAUNCHER, (<<1407.3669, 3617.3169, 38.4050>>), (<<16.6000, 90.0000, 21.8000>>), PlacementFlags)
	//PRINTLN("SETUP_GRENADE_LAUNCHER_PICKUP")
ENDPROC

// @SBA - returns TRUE if the grenade launcher has less ammo than the required ammount
FUNC BOOL DOES_GRENADE_LAUNCHER_NEED_MORE_AMMO(BOOL bSubtractPickupAmmo = FALSE)
	
	// bail if player doesn't have weapon
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), weapontype_grenadelauncher)
		RETURN FALSE
	ENDIF
	
	// check total ammo
	INT iClipAmmo, iPickupAmmo, iTotalAmmo
	GET_AMMO_IN_CLIP(PLAYER_PED_ID(), weapontype_grenadelauncher, iClipAmmo)
	iTotalAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), weapontype_grenadelauncher) + iClipAmmo
	//@SBA - if player has gotten more ammo from the pickup, don't include it in the calculation
	IF bSubtractPickupAmmo
		iPickupAmmo = GET_DEFAULT_AMMO_FOR_WEAPON_PICKUP(PICKUP_WEAPON_GRENADELAUNCHER)
		#IF IS_DEBUG_BUILD
			PRINTLN("DOES_GRENADE_LAUNCHER_NEED_MORE_AMMO: Pickup ammo = ", iPickupAmmo)
			PRINTLN("DOES_GRENADE_LAUNCHER_NEED_MORE_AMMO: Subtotal ammo = ", iTotalAmmo)
		#ENDIF
		iTotalAmmo = iTotalAmmo - iPickupAmmo
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("DOES_GRENADE_LAUNCHER_NEED_MORE_AMMO: Total ammo = ", iTotalAmmo)
	#ENDIF
	IF iTotalAmmo < kiGrenadeLauncherAmmo
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// @SBA - check if player has gotten grenade launcher pickup, then give appropriate ammo if needed
PROC SET_AMMO_FOR_PICKED_UP_GRENADE_LAUNCHER()

	// Have we set the ammo once?
	IF bPickupAmmoSet
		EXIT
	ENDIF
	IF NOT DOES_PICKUP_EXIST(puiGrenadeLauncher)
		EXIT
	ENDIF
	
	// If player got pickup, check ammo level and set minimum ammount if needed
	IF HAS_PICKUP_BEEN_COLLECTED(puiGrenadeLauncher)
		// set flag to skip cutscene because player has pickup
		IF NOT bSkipGrenLaunchCutscene
			bSkipGrenLaunchCutscene = TRUE
			g_replay.iReplayInt[PLAYER_RECEIVED_GRENADE_LAUNCHER] = HAS_GOT_GLAUNCHER
		ENDIF
		IF DOES_GRENADE_LAUNCHER_NEED_MORE_AMMO(TRUE)
			SET_GRENADE_LAUNCHER_AMMO()
		ENDIF
		bPickupAmmoSet = TRUE
	ENDIF
ENDPROC

//*** @SBA - Wave Rusher Related Functions ***

// @SBA - set some attributes for rushers
PROC SET_ENEMY_RUSHER_ATTRIBUTES(PED_INDEX piRusher, INT iNewAccuracy, FLOAT fBlanksCloseChance, FLOAT fBlanksFarChance)
	IF PRIVATE_isEntityAlive(piRusher)
		SET_PED_CHANCE_OF_FIRING_BLANKS(piRusher, fBlanksCloseChance, fBlanksFarChance)
		SET_PED_ACCURACY(piRusher, iNewAccuracy)
	ENDIF
ENDPROC

// @SBA - Returns TRUE if the passed in ped info shows the ped is ok and hasn't rushed yet
FUNC BOOL IS_ENEMY_VALID_FOR_RUSHING(PED_STRUCTURE &psPedInfo, main_mission_shootout &PedStatus, BOOL bCheckStatus = FALSE)
	
	// @SBA - Check if the ped is in the correct state to process a rush.
	// In some cases we don't want to do this as it would inturrupt the peds' strict rushing order.
	IF bCheckStatus
		IF PedStatus != run_into_shop
		AND PedStatus != run_to_front_of_building // @SBA - wave0 uses this one
			#IF IS_DEBUG_BUILD
				PRINTLN("IS_ENEMY_VALID_FOR_RUSHING: This Ped is not in the correct state: ", psPedInfo.name)
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(psPedInfo.ped)
		IF NOT psPedInfo.bRushInside
			#IF IS_DEBUG_BUILD
				PRINTLN("IS_ENEMY_VALID_FOR_RUSHING: Ped is VALID, setting rush flag for: ", psPedInfo.name)
			#ENDIF
			psPedInfo.bRushInside = TRUE
			RETURN TRUE
		ENDIF
	ELSE
		// If the ped is injured, set this flag true anyway for tracking
		IF NOT psPedInfo.bRushInside
			#IF IS_DEBUG_BUILD
				PRINTLN("IS_ENEMY_VALID_FOR_RUSHING: Ped is INJURED, setting rush flag for: ", psPedInfo.name)
			#ENDIF
			psPedInfo.bRushInside = TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

// @SBA - determines and returns the index for the next valid enemy in wave 0
// Lets the enemies be checked in a specific order
FUNC INT GET_NEXT_WAVE_0_INDEX()

	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[3], wave_0_status[3], TRUE)
		RETURN 3
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[1], wave_0_status[1], TRUE)
		RETURN 1
	ENDIF	
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[4], wave_0_status[4], TRUE)
		RETURN 4
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[0], wave_0_status[0], TRUE)
		RETURN 0
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[5], wave_0_status[5], TRUE)
		RETURN 5
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[2], wave_0_status[2], TRUE)
		RETURN 2
	ENDIF	
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[6], wave_0_status[6], TRUE)
		RETURN 6
	ENDIF

	// @SBA - don't currently need the last two, since the truck smashing the gate should intervene
	// when two (or fewer) enemies are left
//	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[7], wave_0_status[7])
//		RETURN 7
//	ENDIF
//		
//	IF IS_ENEMY_VALID_FOR_RUSHING(wave_0_enemy[8], wave_0_status[8])
//		RETURN 8
//	ENDIF
	
	// No valid enemy found
	RETURN -1
	
ENDFUNC

// @SBA - determines and returns the index for the next valid enemy in wave 2
// Lets the enemies be checked in a specific order, but will skip a ped that's not in the correct state
FUNC INT GET_NEXT_WAVE_2_INDEX()


	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[5], wave_2_status[5], TRUE)
		RETURN 5
	ENDIF	
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[4], wave_2_status[4], TRUE)
		RETURN 4
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[1], wave_2_status[1], TRUE)
		RETURN 1
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[0], wave_2_status[0], TRUE)
		RETURN 0
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[2], wave_2_status[2], TRUE)
		RETURN 2
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[18], wave_2_status[18], TRUE)
		RETURN 18
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[3], wave_2_status[3], TRUE)
		RETURN 3
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[19], wave_2_status[19], TRUE)
		RETURN 19
	ENDIF
	
	// @SBA - the enemies below are the second part of wave 2
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[6], wave_2_status[6], TRUE)
		RETURN 6
	ENDIF

	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[7], wave_2_status[7], TRUE)
		RETURN 7
	ENDIF
		
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[8], wave_2_status[8], TRUE)
		RETURN 8
	ENDIF

	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[9], wave_2_status[9], TRUE)
		RETURN 9
	ENDIF

	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[10], wave_2_status[10], TRUE)
		RETURN 10
	ENDIF
		
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[11], wave_2_status[11], TRUE)
		RETURN 11
	ENDIF
		
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[12], wave_2_status[12], TRUE)
		RETURN 12
	ENDIF

	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[13], wave_2_status[13], TRUE)
		RETURN 13
	ENDIF
		
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[14], wave_2_status[14], TRUE)
		RETURN 14
	ENDIF

	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[15], wave_2_status[15], TRUE)
		RETURN 15
	ENDIF

	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[16], wave_2_status[16], TRUE)
		RETURN 16
	ENDIF
		
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_2_enemy[17], wave_2_status[17], TRUE)
		RETURN 17
	ENDIF
	
	// No valid enemy found
	RETURN -1
	
ENDFUNC

// @SBA Returns a randomized time to wait between rush attempts for wave 2
//FUNC INT GET_RANDOM_WAIT_TIME_FOR_WAVE_2(INT iEnemyRushCount)
//	
//	// Return a "long" time after two rushes
//	IF iEnemyRushCount % 2 = 0
//		RETURN GET_RANDOM_INT_IN_RANGE(15000, 20000)	
//	ENDIF
//	
//	// Otherwise return a "normal" time
//	RETURN GET_RANDOM_INT_IN_RANGE(2500, 4000)
//ENDFUNC

// @SBA - checks if enemies have been set to rush and are still alive.  If we have too few, it's ok to request more.
FUNC BOOL ARE_WE_READY_FOR_A_NEW_RUSHER(PED_STRUCTURE &psPedInfo[], INT iMaxNumRushers = kiNumberOfWaveRushersAllowed)

	INT k, iCounter
	// @SBA - count up the number of enemies who have been told to rush and are uninjured
	FOR k = 0 TO COUNT_OF(psPedInfo) - 1
		IF psPedInfo[k].bRushInside
			IF NOT IS_PED_INJURED(psPedInfo[k].ped)
				//CPRINTLN(DEBUG_MISSION, "*** ARE_WE_READY_FOR_A_NEW_RUSHER: Counting enemy ", k)
				iCounter ++
			ENDIF
		ENDIF
	ENDFOR
	
//	CPRINTLN(DEBUG_MISSION, "*** ARE_WE_READY_FOR_A_NEW_RUSHER: Number of active rushers = ", iCounter)
//	CPRINTLN(DEBUG_MISSION, "*** ARE_WE_READY_FOR_A_NEW_RUSHER: Max Number of rushers = ", iMaxNumRushers)
	
	// @SBA - if we're below the max number of allowed rushers at one time, it's ok to request a new one
	IF iCounter < iMaxNumRushers
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC 

// @SBA - determines and returns the index for the next valid enemy in wave 3
// Lets the enemies be checked in a specific order
FUNC INT GET_NEXT_WAVE_3_INDEX()
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_3_enemy[2], wave_3_status[2])
		RETURN 2
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_3_enemy[3], wave_3_status[3])
		RETURN 3
	ENDIF
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_3_enemy[1], wave_3_status[1])
		RETURN 1
	ENDIF	
	
	IF IS_ENEMY_VALID_FOR_RUSHING(wave_3_enemy[0], wave_3_status[0])
		RETURN 0
	ENDIF

	// No valid enemy found
	RETURN -1	
ENDFUNC


// @SBA Returns a randomized time to wait between rush attempts for wave 3
FUNC INT GET_RANDOM_WAIT_TIME_FOR_WAVE_3()
	
	// Return a shorter time after the third rush
	IF wave_3_enemy[1].bRushInside
		RETURN GET_RANDOM_INT_IN_RANGE(4000, 6000)	
	// Or return a longer time after first two rushes
	ELIF wave_3_enemy[2].bRushInside
	AND wave_3_enemy[3].bRushInside
		RETURN GET_RANDOM_INT_IN_RANGE(20000, 25000)	
	ENDIF
	
	// Otherwise return a "normal" time
	RETURN GET_RANDOM_INT_IN_RANGE(6000, 8000)
ENDFUNC


//*** @SBA - Other General Functions ***

// @SBA - sets the given ped's health, if not already set to that value
PROC SET_HEALTH_FOR_PED(PED_INDEX piPed, INT iNewHealth)
	IF DOES_ENTITY_EXIST(piPed)
		IF NOT IS_ENTITY_DEAD(piPed)
			IF GET_ENTITY_HEALTH(piPed) != iNewHealth
				SET_ENTITY_HEALTH(piPed, iNewHealth)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// @SBA - creates a blip for the ped in the passed-in struct if he's not in a vehicle
PROC HANDLE_ENEMY_BLIP_CREATION(PED_STRUCTURE &psPedInfo)

	IF NOT DOES_BLIP_EXIST(psPedInfo.blip)
		IF NOT IS_PED_IN_ANY_VEHICLE(psPedInfo.ped)
			psPedInfo.blip = CREATE_BLIP_FOR_PED(psPedInfo.ped, TRUE)
			SET_BLIP_DISPLAY(psPedInfo.blip, DISPLAY_BLIP)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE_BLIP(vehicle_struct &vsVeh)

	IF DOES_BLIP_EXIST(vsVeh.blip)
		REMOVE_BLIP(vsVeh.blip)
	ENDIF
ENDPROC

PROC REMOVE_DEAD_VEHICLE_BLIPS(vehicle_struct &vsVeh[])
	INT idx
	REPEAT COUNT_OF(vsVeh) idx
		IF DOES_BLIP_EXIST(vsVeh[idx].blip)
			IF DOES_ENTITY_EXIST(vsVeh[idx].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(vsVeh[idx].veh)
					REMOVE_BLIP(vsVeh[idx].blip)
				ELSE IS_VEHICLE_EMPTY(vsVeh[idx].veh, TRUE)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vsVeh[idx].veh)
						CPRINTLN(DEBUG_MISSION, "REMOVE_DEAD_VEHICLE_BLIPS: Vehicle has no living ped in it, removing blip.")
						REMOVE_BLIP(vsVeh[idx].blip)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// @SBA -  bit of a hack to blow up player if he's silly
PROC KILL_INVINCIBLE_PLAYER_IF_IN_EXPLOSION()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 5.0)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, FALSE, FALSE, TRUE, TRUE)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), 1000, TRUE)
		ENDIF
	ENDIF
ENDPROC

// so we can recreate the live ones, if we want
PROC STORE_ENEMY_VEHICLES_STILL_ALIVE()
	INT iCounter = 0
	g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_BOTH_CARS_DEAD
	
	IF DOES_ENTITY_EXIST(wave_0_enemy_vehicle[0].veh)
		IF NOT IS_ENTITY_DEAD(wave_0_enemy_vehicle[0].veh)
			g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_ONLY_CAR0_ALIVE
			iCounter ++
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(wave_0_enemy_vehicle[1].veh)
		IF NOT IS_ENTITY_DEAD(wave_0_enemy_vehicle[1].veh)
			g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_ONLY_CAR1_ALIVE
			iCounter ++
		ENDIF
	ENDIF
	
	IF iCounter = 2
		g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_BOTH_CARS_ALIVE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"STORE_ENEMY_VEHICLES_STILL_ALIVE: setting to ", g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES])
	
ENDPROC


PROC HANDLE_VEHICLE_TOO_NEAR_ICE_BOX()

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxOuterVol)
		EXIT
	ENDIF

	IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 4.0)
		TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
	ENDIF
ENDPROC

proc remove_all_blips()
	
	if does_blip_exist(outside_meth_lab_blip)
		remove_blip(outside_meth_lab_blip)
	endif 
	
	if does_blip_exist(meth_lab_blip)
		remove_blip(meth_lab_blip)
	endif 
	
	if does_blip_exist(buddy.blip)
		remove_blip(buddy.blip)
	endif 
	
	if does_blip_exist(cheng.blip)
		remove_blip(cheng.blip)
	endif 
	
	if does_blip_exist(translator.blip)
		remove_blip(translator.blip)
	endif 
	
	// @SBA - remove ice box blip as well
	if does_blip_exist(ice_box_blip)
		remove_blip(ice_box_blip)
	endif 
	
	for i = 0 to count_of(wave_0_enemy) - 1
		if does_blip_exist(wave_0_enemy[i].blip)
			remove_blip(wave_0_enemy[i].blip)
		endif 
	endfor 
	
	for i = 0 to count_of(wave_0_inside_enemy) - 1
		if does_blip_exist(wave_0_inside_enemy[i].blip)
			remove_blip(wave_0_inside_enemy[i].blip)
		endif 
	endfor 
	
	for i = 0 to count_of(wave_2_enemy) - 1
		if does_blip_exist(wave_2_enemy[i].blip)
			remove_blip(wave_2_enemy[i].blip)
		endif 
	endfor 
	
	for i = 0 to count_of(wave_3_enemy) - 1
		if does_blip_exist(wave_3_enemy[i].blip)
			remove_blip(wave_3_enemy[i].blip)
		endif 
	endfor 
	
	// vehicles too
	for i = 0 to count_of(wave_0_enemy_vehicle) - 1
		REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[i])
	ENDFOR
	for i = 0 to count_of(wave_2_enemy_vehicle) - 1
		REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[i])
	ENDFOR
	for i = 0 to count_of(wave_3_enemy_vehicle) - 1
		REMOVE_VEHICLE_BLIP(wave_3_enemy_vehicle[i])
	ENDFOR	
endproc

PROC RECORD_ENEMY_IN_ARRAY_KILLED(ped_structure &enemyStruct[])
	INT k
	REPEAT COUNT_OF(enemyStruct) k
		IF DOES_ENTITY_EXIST(enemyStruct[k].ped)
			IF NOT enemyStruct[k].been_killed
				IF IS_ENTITY_DEAD(enemyStruct[k].ped)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(enemyStruct[k].ped, PLAYER_PED_ID())
						CPRINTLN(DEBUG_MISSION, "RECORD_ENEMY_IN_ARRAY_KILLED")
						enemyStruct[k].been_killed = TRUE
						INFORM_MISSION_STATS_OF_INCREMENT(CHI1_BODY_COUNT)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(enemyStruct[k].ped, weapontype_grenadelauncher)
							CPRINTLN(DEBUG_MISSION, "CHI1_GRENADE_LAUNCHER_KILLS")
							INFORM_MISSION_STATS_OF_INCREMENT(CHI1_GRENADE_LAUNCHER_KILLS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC RECORD_VEHICLE_IN_ARRAY_DESTROYED(vehicle_struct &vehStruct[])
	INT k
	REPEAT COUNT_OF(vehStruct) k
		IF DOES_ENTITY_EXIST(vehStruct[k].veh)
			IF NOT vehStruct[k].been_destroyed
				IF IS_ENTITY_DEAD(vehStruct[k].veh)
					CPRINTLN(DEBUG_MISSION, "RECORD_VEHICLE_IN_ARRAY_DESTROYED")
					vehStruct[k].been_destroyed = TRUE
					INFORM_MISSION_STATS_OF_INCREMENT(CHI1_VEHICLES_DESTROYED)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC UPDATE_DAMAGE_WATCH_STATS()
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF bPlayerInVehicle
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			CPRINTLN(DEBUG_MISSION, "UPDATE_DAMAGE_WATCH_STATS: Player is NOT in vehicle.")
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), CHI1_UNMARKED)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(player_ped_id())
			bPlayerInVehicle = FALSE
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(trevors_truck.veh)
			IF IS_VEHICLE_DRIVEABLE(trevors_truck.veh)
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), trevors_truck.veh)
					CPRINTLN(DEBUG_MISSION, "UPDATE_DAMAGE_WATCH_STATS: Player IS in vehicle.")
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(trevors_truck.veh, CHI1_CAR_DAMAGE)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(trevors_truck.veh)
					bPlayerInVehicle = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Update stats
PROC HANDLE_CHINESE1_STATS()
	RECORD_ENEMY_IN_ARRAY_KILLED(wave_0_enemy)
	RECORD_ENEMY_IN_ARRAY_KILLED(wave_0_inside_enemy)
	RECORD_ENEMY_IN_ARRAY_KILLED(wave_2_enemy)
	RECORD_ENEMY_IN_ARRAY_KILLED(wave_3_enemy)
	RECORD_ENEMY_IN_ARRAY_KILLED(ambient_enemy)

	RECORD_VEHICLE_IN_ARRAY_DESTROYED(wave_0_enemy_vehicle)
	RECORD_VEHICLE_IN_ARRAY_DESTROYED(wave_2_enemy_vehicle)
	RECORD_VEHICLE_IN_ARRAY_DESTROYED(wave_3_enemy_vehicle)
	RECORD_VEHICLE_IN_ARRAY_DESTROYED(ambient_enemy_vehicle)
	
	UPDATE_DAMAGE_WATCH_STATS()
ENDPROC

//**************************************************LAWRENCE SDK**************************************************

proc disable_dispatch_services()
		
	enable_dispatch_service(dt_fire_department, false)
	enable_dispatch_service(dt_police_automobile, false)
	enable_dispatch_service(dt_police_helicopter, false)
	enable_dispatch_service(dt_ambulance_department, false)

endproc 

proc enable_dispatch_services()

	enable_dispatch_service(dt_fire_department, true)
	enable_dispatch_service(dt_police_automobile, true)
	enable_dispatch_service(dt_police_helicopter, true)
	enable_dispatch_service(dt_ambulance_department, true)

endproc 

func bool HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(ped_index miss_ped)
	
	if does_entity_exist(miss_ped)
		if not is_ped_injured(miss_ped)
			if HAVE_ALL_STREAMING_REQUESTS_COMPLETED(miss_ped)
				return true 
			endif 
		endif 
	endif 
	
	return false 

endfunc 

func bool start_new_cutscene_no_fade(bool clear_players_tasks = true, bool hide_weapon_for_cutscene = true, bool kill_conversation_line_immediately = true, BOOL dDisplayRadarHud = FALSE, bool ignore_can_player_start_cutscene = false)

	if can_player_start_cutscene() or ignore_can_player_start_cutscene 
	
		SPECIAL_ABILITY_DEACTIVATE(player_id())
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
		
		clear_prints()
		clear_help()
		if kill_conversation_line_immediately
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
		else 
			kill_any_conversation()
		endif 
		
		display_hud(dDisplayRadarHud)
		display_radar(dDisplayRadarHud)
		set_widescreen_borders(true, 500)
		
		if clear_players_tasks
			set_player_control(player_id(), false, spc_clear_tasks)
		else 
			set_player_control(player_id(), false)
		endif 

		if hide_weapon_for_cutscene
			hide_ped_weapon_for_scripted_cutscene(player_ped_id(), true)
		endif 
		
		disable_dispatch_services()
		
		return true
	endif 
		
	return false

endfunc


proc end_cutscene(bool clear_tasks = true, float interp_heading = 0.0, float interp_pitch = 0.0, bool enable_emergency_services = true)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	
	if is_player_playing(player_id())
		
		render_script_cams(false, false)
		set_gameplay_cam_relative_heading(interp_heading)
		set_gameplay_cam_relative_pitch(interp_pitch)
			
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
	endif 

	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 
	
//	if mission_car_marked 
//		mark_vehicle_as_no_longer_needed(current_car)
//		mission_car_marked = false
//	endif 
	
	//allow_emergency_services(true)
	do_screen_fade_in(DEFAULT_FADE_TIME)
	
endproc

proc end_cutscene_no_fade(bool clear_tasks = true, bool interpolate_behind_player = false, float interp_heading = 0.0, float interp_pitch = 0.0, int interp_to_game_time = 3000, bool enable_emergency_services = true, bool update_game_camera = true)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500) // 500
	
	if is_player_playing(player_id())
		
		destroy_all_cams()

		if update_game_camera
			if interpolate_behind_player
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
				render_script_cams(false, true, interp_to_game_time)
			else 
				render_script_cams(false, false)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			endif
		endif 
		
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
	endif 
	
	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 

endproc 


// @SBA - no frills
PROC END_CUTSCENE_BASIC()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500) // 500

	if is_player_playing(player_id())
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
	ENDIF

ENDPROC

func bool lk_timer(int &start_time, int time) 
	int current_time 
	current_time = get_game_timer()
	
	if ((current_time - start_time) > time) 
		return true
	endif 
	
	return false
endfunc

func bool is_skip_button_pressed()
	
	return is_control_just_pressed(frontend_control, input_frontend_accept)
	
endfunc 

func bool skip_scripted_cut(int &original_time_temp, int cutscene_skip_time)
	if lk_timer(original_time_temp, cutscene_skip_time)
		if is_screen_faded_in()
			if IS_CUTSCENE_SKIP_BUTTON_PRESSED()	
				return true 
			endif 
		endif 
	endif 
	
	return false 
endfunc 

func bool mission_ped_created_and_injured(bool &mission_ped_created, ped_index &mission_ped)

	if mission_ped_created
		return is_ped_injured(mission_ped)
	endif 
	
	return false 

endfunc 

func bool mission_ped_injured(ped_index &this_ped)

	if DOES_ENTITY_EXIST(this_ped)
		return is_ped_injured(this_ped)
	endif 
	
	return false
	
endfunc 

func bool mission_vehicle_injured(vehicle_index &this_vehicle)
	
	if DOES_ENTITY_EXIST(this_vehicle)
		if not is_vehicle_driveable(this_vehicle)
			return true
		endif 
	endif 
	
	return false
	
endfunc 

func bool is_mission_entity_attacked(ped_index &mission_ped, bool clear_damage_entity = false)
	
	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_ped, PLAYER_ped_ID())
				if clear_damage_entity
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mission_ped)
				endif 
				return true
			endif 
		else 
			return true
		endif
	endif 
	
	return false
endfunc 

func bool has_char_task_finished_2(ped_index ped, script_task_name taskname)
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

//gets the currenent car the player is in
func bool get_current_player_vehicle(vehicle_index &test_car)
	
	if is_ped_sitting_in_any_vehicle(player_ped_id())
		test_car = get_players_last_vehicle()
		if DOES_ENTITY_EXIST(test_car)				
			if is_vehicle_driveable(test_car)
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc

PROC STOP_PLAYER_vehicle()

	VEHICLE_INDEX player_car
	
	if get_current_player_vehicle(player_car)
		
		FLOAT player_car_speed
			
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		SET_ENTITY_PROOFS(player_car, true, true, true, true, true)

		player_car_speed = GET_ENTITY_SPEED(player_car)
			 
		WHILE player_car_speed > 0.2
			WAIT(0)
			if not IS_ENTITY_DEAD(player_car)
				player_car_speed = GET_ENTITY_SPEED(player_car)
			endif 
		ENDWHILE

		SET_ENTITY_PROOFS(player_car, false, false, false, false, false)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	ENDIF
ENDPROC

func bool add_ped_to_players_group(ped_index &this_ped, group_index &players_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if not is_ped_injured(this_ped)
	
			players_group = get_player_group(player_id())
			
			set_ped_as_group_member(this_ped, players_group)
			
			return true
		endif 
	endif 

	return false
	
endfunc 

func float distance_from_player_to_ped(ped_index &mission_ped)

	vector player_pos
	vector mission_ped_pos 

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			
			player_pos = GET_ENTITY_COORDS(player_ped_id()) 
			mission_ped_pos = GET_ENTITY_COORDS(mission_ped)
			
			return get_distance_between_coords(player_pos, mission_ped_pos)
		endif 
	endif 
	
	return -1.0

endfunc 

func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence)
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

func bool is_ped_playing_anim_at_phase(string anim_dict_name, string anim_name, float phase_target)
		
//	printstring("test 0")
//	printnl()
	if IS_ENTITY_PLAYING_ANIM(player_ped_id(), anim_dict_name, anim_name) 
//		printstring("test 1")
//		printnl()
		if GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(), anim_dict_name, anim_name) > phase_target 
//			script_assert("test 2")
			return true 
		endif 
	endif 
	
	return false 
	
endfunc

func bool are_vectors_alomost_equal(vector vector_a, vector vector_b, vector unit_vector_x_multiplier)

	//vec b is the target vector
	//vec a is the current positon of the object
	//unit_vector_x_multiplier is the unitvector * frame time * multiplier
	
	vector vector_ba
	vector future_vector

	vector_ba = vector_b - vector_a
	
	if (vmag(vector_ba) < 0.2)
		return true
	endif
	
	future_vector = (vector_b - (vector_a + unit_vector_x_multiplier))
	if (vmag(future_vector) > vmag(vector_ba))
		return true
	endif 
	
	return false

endfunc 

//The players group does not include the player himself. RELGROUPHASH_PLAYER is the player
REL_GROUP_HASH player_group
REL_GROUP_HASH enemy_group

    
proc add_relationship_groups()

	ADD_RELATIONSHIP_GROUP("players group", player_group)
	ADD_RELATIONSHIP_GROUP("enemy group", enemy_group)
	
endproc 

proc setup_relationship_contact(ped_index &this_ped, bool block_temporary_events = false)

	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			SET_PED_RELATIONSHIP_GROUP_HASH(this_ped, player_group)
			
			SET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_ped_combat_attributes(this_ped, CA_DISABLE_PINNED_DOWN, true)
			set_ped_combat_attributes(this_ped, CA_DISABLE_PIN_DOWN_OTHERS, true)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
			
		endif 
	endif 

endproc

proc setup_relationship_enemy(ped_index &this_ped, bool block_temporary_events = false)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			
			SET_ped_RELATIONSHIP_GROUP_hash(this_ped, enemy_group) 

			set_ped_combat_attributes(this_ped, ca_will_scan_for_dead_peds, false) 
			SET_PED_COMBAT_ATTRIBUTES(this_ped, ca_use_vehicle, false)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_LEAVE_VEHICLES, true)
			
			set_ped_combat_attributes(this_ped, CA_DISABLE_PINNED_DOWN, true)
			set_ped_combat_attributes(this_ped, CA_DISABLE_PIN_DOWN_OTHERS, true)
			
			set_ped_combat_movement(this_ped, cm_defensive)

			SET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)

			set_entity_is_target_priority(this_ped, true)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
		endif 
	endif 
	
endproc

proc setup_enemy_attributes(ped_structure &mission_entity)
		
	set_ped_dies_when_injured(mission_entity.ped, true)
	set_ped_as_enemy(mission_entity.ped, true)
	set_entity_is_target_priority(mission_entity.ped, true)
	set_ped_keep_task(mission_entity.ped, true)
	set_ped_can_evasive_dive(mission_entity.ped, true)
	set_blocking_of_non_temporary_events(mission_entity.ped, true)

	SET_PED_CONFIG_FLAG(mission_entity.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	set_ped_config_flag(mission_entity.ped, PCF_DisableHurt, true)

	give_weapon_to_ped(mission_entity.ped, mission_entity.weapon, infinite_ammo, true)
	set_ped_accuracy(mission_entity.ped, mission_entity.accuracy)
	
	SET_ENTITY_HEALTH(mission_entity.ped, mission_entity.health)
	set_ped_max_health(mission_entity.ped, mission_entity.health)

	if mission_entity.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mission_entity.ped, true)
	endif 
	
	if mission_entity.allow_body_armour 
		add_armour_to_ped(mission_entity.ped, 100)
	endif 
	
	mission_entity.created = true 
	
	set_ped_name_debug(mission_entity.ped, mission_entity.name)
	
	set_ped_hearing_range(mission_entity.ped, 250.00)
	set_ped_seeing_range(mission_entity.ped, 250.00)
	set_ped_id_range(mission_entity.ped, 250.00)
	
	mission_entity.blip = create_blip_for_ped(mission_entity.ped, true)
	set_blip_display(mission_entity.blip, DISPLAY_BLIP) 
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mission_entity.ped)

endproc 

proc setup_enemy(ped_structure &enemy)

	clear_area(enemy.pos, 2.0, true)
		
	enemy.ped = create_ped(pedtype_mission, enemy.model, enemy.pos, enemy.heading)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	set_ped_config_flag(enemy.ped, PCF_DisableHurt, true)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	enemy.blip = CREATE_BLIP_FOR_PED(enemy.ped, true)
	set_blip_display(enemy.blip, DISPLAY_BLIP) 

	set_ped_can_evasive_dive(enemy.ped, true)
	
	setup_relationship_enemy(enemy.ped, true)
	
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)
	//CHI1_BODY_COUNT stats 
	
	enemy.created = true 

endproc

proc setup_buddy(ped_structure &mission_buddy, BOOL bCreateBlip = TRUE)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped(pedtype_mission, mission_buddy.model, mission_buddy.pos, mission_buddy.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	set_ped_can_ragdoll(mission_buddy.ped, false)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mission_buddy.ped, false)
	
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_RunFromFiresAndExplosions, FALSE)
	set_ped_config_flag(mission_buddy.ped, PCF_CanActivateRagdollWhenVehicleUpsideDown, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableHurt, true)
	
	set_ped_name_debug(mission_buddy.ped, mission_buddy.name)
	
	// @SBA - sometimes we don't want a blip
	IF bCreateBlip
		mission_buddy.blip = CREATE_BLIP_FOR_PED(mission_buddy.ped, false)
		set_blip_display(mission_buddy.blip, DISPLAY_BLIP)
		// Also will set Cheng and translator, but shouldn't hurt since there are no enemies, etc. while they exist.
		SET_BLIP_PRIORITY(mission_buddy.blip, BLIPPRIORITY_HIGHEST)
	ENDIF
	
	IF mission_buddy.weapon != WEAPONTYPE_INVALID
		give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	ENDIF
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	// give glasses
	IF mission_buddy.model = GET_NPC_PED_MODEL(CHAR_CHEF)
	OR mission_buddy.model = GET_NPC_PED_MODEL(CHAR_cheng)
	OR mission_buddy.model = IG_TaosTranslator
		SET_PED_PROP_INDEX(mission_buddy.ped, ANCHOR_EYES, 0) //Glasses prop.
	ENDIF
	
	setup_relationship_contact(mission_buddy.ped, true)

endproc

// @SBA - create Chef for initial cutscene
PROC HANDLE_SETUP_INITIAL_CHEF()
	IF NOT DOES_ENTITY_EXIST(buddy.ped)
		IF HAS_MODEL_LOADED(buddy.model)
			setup_buddy(buddy, FALSE)
			CPRINTLN(DEBUG_MISSION, "HANDLE_SETUP_INITIAL_CHEF: Chef has been created")
		ENDIF
	ENDIF
ENDPROC

proc setup_buddy_attributes(ped_structure &mission_buddy)
	
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	set_ped_can_ragdoll(mission_buddy.ped, false)
	
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_RunFromFiresAndExplosions, FALSE)
	set_ped_config_flag(mission_buddy.ped, PCF_CanActivateRagdollWhenVehicleUpsideDown, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableHurt, true)
	
	set_ped_name_debug(mission_buddy.ped, mission_buddy.name)
	
	IF mission_buddy.weapon != WEAPONTYPE_INVALID
		give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	ENDIF
	
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)

endproc 

PROC SET_START_VERSIONS_OF_BUDDIES()
	
	IF NOT IS_PED_INJURED(cheng.ped)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_TORSO, 2, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_JBIB, 1, 0)
		SET_PED_PROP_INDEX(cheng.ped, ANCHOR_EYES,0)
	ENDIF

	IF NOT IS_PED_INJURED(translator.ped)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_TORSO, 1, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_LEG, 1, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_SPECIAL2, 1, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_JBIB, 1, 0)
		SET_PED_PROP_INDEX(translator.ped, ANCHOR_EYES,0)
	ENDIF
ENDPROC

PROC INIT_CHENGS_CRAZY_CAR_DANCING(INT iTime = 4000)
	eCDS_DanceState = eCDS_BaseAnim
	sDanceMove = sCrazyDanceBase
	iChengDanceTime = GET_GAME_TIMER() + iTime
	iChengDanceStopTime = -1
ENDPROC

PROC HANDLE_CHENGS_CRAZY_CAR_DANCING()
	
	IF NOT DOES_ENTITY_EXIST(cheng.ped)
		EXIT
	ELIF IS_PED_INJURED(cheng.ped)
		EXIT
	ENDIF
	
	// don't dance after aproaching meth lab
	IF get_to_meth_lab_status > 0
		IF GET_SCRIPT_TASK_STATUS(cheng.ped, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
			CLEAR_PED_TASKS(cheng.ped)
		ENDIF
		EXIT
	ENDIF
	
	// if player has left car clear peds tasks if needed so Cheng can exit
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_SCRIPT_TASK_STATUS(cheng.ped, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
				CLEAR_PED_TASKS(cheng.ped)
			ENDIF
			INIT_CHENGS_CRAZY_CAR_DANCING()
			EXIT
		ENDIF
	ENDIF
	
	// Only dance to certain music
	IF GET_PLAYER_RADIO_STATION_GENRE() != ENUM_TO_INT(RADIO_GENRE_DANCE)
	AND GET_PLAYER_RADIO_STATION_GENRE() != ENUM_TO_INT(RADIO_GENRE_POP)
		IF iChengDanceStopTime = -1
			iChengDanceStopTime = GET_GAME_TIMER() + 1000
		ENDIF
	ENDIF
	IF iChengDanceStopTime != -1
		IF GET_GAME_TIMER() > iChengDanceStopTime
			IF GET_SCRIPT_TASK_STATUS(cheng.ped, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
				CLEAR_PED_TASKS(cheng.ped)
			ENDIF
			INIT_CHENGS_CRAZY_CAR_DANCING(2000)
			EXIT
		ENDIF
	ENDIF
	
	// don't dance if not in a vehicle
	IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(cheng.ped)
		IF GET_SCRIPT_TASK_STATUS(cheng.ped, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
			CLEAR_PED_TASKS(cheng.ped)
		ENDIF
		INIT_CHENGS_CRAZY_CAR_DANCING()
		EXIT
	ELSE
		// only dance in front passenger seat
		IF GET_SEAT_PED_IS_IN(cheng.ped) != VS_FRONT_RIGHT
			EXIT
		ENDIF
		// only dance in trevors truck
		VEHICLE_INDEX viChengVeh = GET_VEHICLE_PED_IS_IN(cheng.ped)
		IF NOT IS_VEHICLE_MODEL(viChengVeh, trevors_truck.model)
			EXIT
		ENDIF
	ENDIF

	IF GET_GAME_TIMER() < iChengDanceTime
		//CPRINTLN(DEBUG_MISSION, "HANDLE_CHENGS_CRAZY_CAR_DANCING: timer ", iChengDanceTime - GET_GAME_TIMER())
		EXIT
	ENDIF
	
	// If ped is in a vehicle play base anim
	// after a timer, check where we are in the anim phase.  If we're low or high, transition to a flavor anim
	ANIMATION_FLAGS AF_LOCAL_FLAG = AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_ABORT_ON_WEAPON_DAMAGE
	FLOAT fChance
	
	SWITCH eCDS_DanceState
		CASE eCDS_BaseAnim
			IF NOT IS_ENTITY_PLAYING_ANIM(cheng.ped, sCrazyDanceDict, sDanceMove)
				TASK_PLAY_ANIM(cheng.ped, sCrazyDanceDict, sDanceMove, SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOCAL_FLAG)
			ELSE
				IF GET_ENTITY_ANIM_CURRENT_TIME(cheng.ped, sCrazyDanceDict, sDanceMove) >= 0.99
					fChance = GET_RANDOM_FLOAT_IN_RANGE()
					
					IF fChance < 0.5
						fChance = GET_RANDOM_FLOAT_IN_RANGE()
						IF fChance <= 0.6
							sDanceMove = sCrazyDance1
						ELIF fChance <= 0.9
							sDanceMove = sCrazyDance2
						ELSE
							sDanceMove = sCrazyDance3
						ENDIF
						AF_LOCAL_FLAG = AF_DEFAULT | AF_NOT_INTERRUPTABLE | AF_ABORT_ON_WEAPON_DAMAGE
						TASK_PLAY_ANIM(cheng.ped, sCrazyDanceDict, sDanceMove, SLOW_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_LOCAL_FLAG)
						eCDS_DanceState = eCDS_FlavorAnim
					ENDIF
					CPRINTLN(DEBUG_MISSION, "HANDLE_CHENGS_CRAZY_CAR_DANCING: Choosing ", sDanceMove)
				ENDIF
			ENDIF
			
		BREAK
		CASE eCDS_FlavorAnim
			IF IS_ENTITY_PLAYING_ANIM(cheng.ped, sCrazyDanceDict, sDanceMove)
				IF GET_ENTITY_ANIM_CURRENT_TIME(cheng.ped, sCrazyDanceDict, sDanceMove) >= 0.99
					sDanceMove = sCrazyDanceBase
					CPRINTLN(DEBUG_MISSION, "HANDLE_CHENGS_CRAZY_CAR_DANCING: Going to ", sDanceMove)
					TASK_PLAY_ANIM(cheng.ped, sCrazyDanceDict, sDanceMove, DEFAULT, SLOW_BLEND_OUT, DEFAULT, AF_LOCAL_FLAG)
					eCDS_DanceState = eCDS_BaseAnim
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "HANDLE_CHENGS_CRAZY_CAR_DANCING: We dropped out? Going to ", sDanceMove)
				sDanceMove = sCrazyDanceBase
				eCDS_DanceState = eCDS_BaseAnim
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SETUP_ENEMY_VEHICLE(vehicle_struct &vsVeh, FLOAT fDirtLevel = 13.0)
	vsVeh.veh = create_vehicle(vsVeh.model, vsVeh.pos, vsVeh.heading)
	IF DOES_ENTITY_EXIST(vsVeh.veh)
		SET_VEHICLE_DIRT_LEVEL(vsVeh.veh, fDirtLevel)
		SET_VEHICLE_DOORS_LOCKED(vsVeh.veh, vehiclelock_locked)
		vsVeh.blip = CREATE_BLIP_FOR_VEHICLE(vsVeh.veh, TRUE)
		IF DOES_BLIP_EXIST(vsVeh.blip)
			SET_BLIP_PRIORITY(vsVeh.blip, BLIPPRIORITY_HIGHEST)
		ENDIF
		SET_ENTITY_AS_MISSION_ENTITY(vsVeh.veh)
	ENDIF
ENDPROC

// @SBA - if the passed in ped is damaged by the player whilst driving, kill the ped
PROC KILL_PED_HIT_BY_PLAYERS_VEHICLE(PED_INDEX piPed)
	
	IF NOT DOES_ENTITY_EXIST(piPed)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(piPed)
		EXIT
	ENDIF
	
	VEHICLE_INDEX viPlayersVehicle
	IF get_current_player_vehicle(viPlayersVehicle)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piPed, PLAYER_PED_ID())
			CPRINTLN(DEBUG_MISSION, "KILL_PED_HIT_BY_PLAYERS_VEHICLE: Player's vehicle has damaged ped!  Killing ped.")
			APPLY_DAMAGE_TO_PED(piPed, 1000, TRUE)
		ENDIF
	ENDIF
ENDPROC

// break glass at Trevor's first position
PROC IN_CASE_OF_ATTACK_BREAK_GLASS()

	// @SBA - crack the glass so Tevor gets a good first shot
	// Just firing the shot was causing a crash - hoping to circumvent that with this approach
	IF NOT bPlayerInOpeningCover
		CPRINTLN(DEBUG_MISSION, "Breaking the window!")
		object_index glass_window
		IF NOT DOES_ENTITY_EXIST(glass_window)
			glass_window = get_closest_object_of_type(<<1389.11, 3600.43, 39.51>>, 2.0, v_ret_ml_win5)
		ENDIF
		break_entity_glass(glass_window, (<<1388.92908, 3600.90259, 40.0>>), 1.0, (<<0.0, 1.0, 0.0>>), 1100.00, 1, TRUE)
		// SHOOT_SINGLE_BULLET_BETWEEN_COORDS((<<1380.0, 3599.5, 38.2>>), (<<1388.92908, 3600.90259, 40.0>>), 0, TRUE)
		SET_OBJECT_AS_NO_LONGER_NEEDED(glass_window)
		
		bPlayerInOpeningCover = TRUE
	ENDIF
ENDPROC

proc setup_enemy_in_vehicle(ped_structure &enemy, vehicle_index &mission_veh, vehicle_seat veh_seat = vs_driver)

	//clear_area(enemy.pos, 2.0, true)
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	
	enemy.ped = create_ped_inside_vehicle(mission_veh, pedtype_mission, enemy.model, veh_seat)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	set_ped_config_flag(enemy.ped, PCF_DisableHurt, true)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	set_current_ped_weapon(enemy.ped, enemy.weapon, true)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_hearing_range(enemy.ped, 250.00)
	
	// @SBA - quick fix.  Only blip the driver so we don't have multiple blips on the vehicle
	IF veh_seat = vs_driver
		enemy.blip = CREATE_BLIP_FOR_PED(enemy.ped, true)
		set_blip_display(enemy.blip, DISPLAY_BLIP)
	ENDIF
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	setup_relationship_enemy(enemy.ped, true)
	
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)

endproc

func bool ped_structure_are_all_enemies_dead(ped_structure &enemy_array[], bool set_ped_no_longer_needed = true)
	
	int peds_dead = 0
	int k = 0
	
	for k = 0 to (count_of(enemy_array) - 1)
		
		if DOES_ENTITY_EXIST(enemy_array[k].ped)
			if is_ped_injured(enemy_array[k].ped)
			
				//INFORM_MISSION_STATS_OF_INCREMENT(CHI1_BODY_COUNT)
				
				if set_ped_no_longer_needed
					SET_PED_AS_NO_LONGER_NEEDED(enemy_array[k].ped) 
				endif 
				
				if does_blip_exist(enemy_array[k].blip)
					REMOVE_BLIP(enemy_array[k].blip)
				endif 
				
				if is_special_ability_active(player_id())
					//INFORM_STAT_CHINESE_ONE_SPECIAL_ABILITY_KILL()
				endif 
				
				//INFORM_STAT_CHINESE_ONE_ENEMY_KILLED()
				
				peds_dead ++
			endif 
		
		else 
		
			if enemy_array[k].created
				peds_dead ++
			endif 
			
		endif 
		
	endfor 
	
	if peds_dead = count_of(enemy_array) 
		return TRUE
	endif 
	
	return FALSE
endfunc

//*****call this function underneath ped_structure_are_all_enemies_dead
func bool ped_structure_are_specific_number_enemies_dead(ped_structure &enemy_array[], int number_of_dead_peds)
	
	int peds_dead = 0
	int k = 0
	
	for k = 0 to count_of(enemy_array) - 1
		if enemy_array[k].created
			if is_ped_injured(enemy_array[k].ped)
				peds_dead++
			endif
		endif 
	endfor 
	
	if peds_dead >= number_of_dead_peds
		return TRUE
	endif 
	
	return FALSE
endfunc

func int ped_structure_get_total_number_of_enemies_dead(ped_structure &enemy_array[])

	int peds_dead = 0
	int k = 0
	
//	printstring("peds dead: ")
//	printint(peds_dead)
//	printnl()
	
	for k = 0 to count_of(enemy_array) - 1
		if enemy_array[k].created
			if is_ped_injured(enemy_array[k].ped)
//				printint(k)
//				printnl()
				peds_dead++
			endif
		endif 
	endfor 
	
	return peds_dead

endfunc 


func bool is_coord_in_area_2d(vector test_coord, vector top_left, vector bottom_right)
				
	vector tl
	vector br

	if top_left.x < bottom_right.x	
		tl.x = top_left.x
		br.x = bottom_right.x
	else
		tl.x = bottom_right.x
		br.x = top_left.x
	endif
	
	if top_left.y < bottom_right.y	
		tl.y = top_left.y
		br.y = bottom_right.y	
	else
		tl.y = bottom_right.y
		br.y = top_left.y
	endif

	if (test_coord.x > tl.x) and (test_coord.x < br.x)
		if (test_coord.y > tl.y) and (test_coord.y < br.y)
			return true
		endif 
	endif 

	return false 
endfunc

func bool has_ped_been_harmed(ped_index &mission_ped, int &original_health)

	int current_health
	
	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			
			current_health = GET_ENTITY_HEALTH(mission_ped)
			
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_ped , player_ped_id())		
			or (current_health < original_health) 
			or is_ped_responding_to_event(mission_ped, EVENT_DAMAGE)	
				
				return true
			
			endif 
			
		else 
		
			return true
		
		endif  
	
	endif 
	
	return false

endfunc 

func bool has_peds_been_harmed(ped_structure &ped_struct[])

	for i = 0 to (count_of(ped_struct) - 1)
		
		if has_ped_been_harmed(ped_struct[i].ped, ped_struct[i].health)
			return true 
		endif 
		
	endfor 
	
	return false
	
endfunc 


func bool has_player_antagonised_ped(ped_index &mission_ped, float distance, bool distance_check_on = true)

	if not is_ped_injured(mission_ped)
		
		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_ped)) < distance
			//if has_char_spotted_char(this_ped, player_char_id())
			
			if CAN_PED_SEE_HATED_PED(mission_ped, PLAYER_PED_ID())
				
				IF IS_ped_ARMED(PLAYER_ped_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
					
					if IS_PLAYER_TARGETTING_ENTITY(player_id(), mission_ped)
						return TRUE
					endif 
					
					if is_player_free_aiming_at_entity(player_id(), mission_ped)
						return TRUE
					endif
				endif 
			endif 
			
			if is_ped_shooting(player_ped_id())
				return true
			endif	
			
		endif
		
		if is_bullet_in_area(get_entity_coords(mission_ped), 4.0)
			return true
		endif 
		
		if distance_check_on
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_ped)) < 1.5
				return true
			endif
		endif 

	endif 

	return FALSE
endfunc 

func bool is_player_targeting_ped(ped_index &mission_ped)

	if not is_ped_injured(mission_ped)
		if is_player_targetting_entity(player_id(), mission_ped)
		or is_player_free_aiming_at_entity(player_id(), mission_ped)
			return true
		endif 
	endif 

	return false

endfunc 

func bool is_player_targeting_any_peds(ped_structure &mission_ped_array[])

	int k = 0
	
	for k = 0 to count_of(mission_ped_array) - 1
		if is_player_targeting_ped(mission_ped_array[k].ped)
			return true
		endif 
	endfor 
	
	return false 

endfunc 


func bool has_vehicle_been_harmed(vehicle_index &mission_vehicle, int &original_car_health)

	int vehicle_health
	
	if DOES_ENTITY_EXIST(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
			
			vehicle_health = GET_ENTITY_HEALTH(mission_vehicle)
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < original_car_health
			or get_vehicle_engine_health(mission_vehicle) < original_car_health
			or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_vehicle, player_ped_id())
			or vehicle_health < original_car_health
			
				return true
			
			endif 
		
		else 
		
			return true 
			
		endif 
	
	endif 
	
	return false
		
endfunc

func bool has_vehicles_been_harmed(vehicle_struct &veh_struct[])

	for i = 0 to (count_of(veh_struct) - 1)
		
		if has_vehicle_been_harmed(veh_struct[i].veh, veh_struct[i].health)
			return true 
		endif 
		
	endfor 
	
	return false
endfunc 

func bool is_ped_at_coords(ped_index &mission_ped, vector pos, vector locate_size)
	
	if not is_ped_injured(mission_ped)
		if IS_ENTITY_AT_COORD(mission_ped, pos, locate_size, false, true)
			return true 
		endif 
	endif 

	return false 

endfunc 

func bool is_ped_in_angled_area_lk(ped_index &mission_ped, vector vec1, vector vec2, float distance_from_point)
	
	if not is_ped_injured(mission_ped)
		if IS_ENTITY_IN_ANGLED_AREA(mission_ped, vec1, vec2, distance_from_point)
			return true 
		endif 
	endif 

	return false 

endfunc 

func bool is_vehicle_in_angled_area_lk(vehicle_index &mission_vehicle, vector vec1, vector vec2, float distance_from_point)
	
	if DOES_ENTITY_EXIST(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
			if is_point_in_angled_area(GET_ENTITY_COORDS(mission_vehicle), vec1, vec2, distance_from_point)
				return true 
			endif 
		endif 
	endif 
	
	return false 

endfunc 

func bool is_ped_in_specific_room(ped_index mission_ped, vector interior_pos, string room_name)

	interior_instance_index interior
	interior_instance_index ped_current_interior
	
	int ped_current_room_hash_key
	int room_hash_key
	
	if not is_ped_injured(mission_ped)

		interior = get_interior_at_coords(interior_pos)	
		ped_current_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
		
		if not (interior = null)
			if (interior = ped_current_interior)
				
				ped_current_room_hash_key = GET_KEY_FOR_ENTITY_IN_ROOM(mission_ped)
				room_hash_key = get_hash_key(room_name)
		
				if (ped_current_room_hash_key != 0)
					if ped_current_room_hash_key = room_hash_key
						return true 
					endif 
				endif 
			endif 
		endif 
		
	endif 
	
	return false 
	
endfunc 

//func bool is_ped_inside_interior(ped_index mission_ped, vector interior_pos)
//
//	interior_instance_index interior
//	interior_instance_index mission_ped_interior
//	
//	interior = get_interior_at_coords(interior_pos)
//	mission_ped_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
//	
//	if not (mission_ped_interior = null)
//		if (mission_ped_interior = interior)
//			return true 
//		endif 
//	endif 
//	
//	return false 
//
//endfunc 

func bool is_ped_inside_interior(ped_index mission_ped, vector interior_pos, string interior_name)

	interior_instance_index interior
	interior_instance_index mission_ped_interior
	
	interior = get_interior_at_coords_with_type(interior_pos, interior_name)
	mission_ped_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
	
	if not (mission_ped_interior = null)
		if (mission_ped_interior = interior)
			return true 
		endif 
	endif 
	
	return false 

endfunc 

func bool is_vehicle_stuck_every_check(vehicle_index &vehicle)
	
	if DOES_ENTITY_EXIST(vehicle)

		if is_vehicle_driveable(vehicle)
			
			if is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_JAMMED, JAMMED_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_SIDE, SIDE_TIME)
			
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc 

func bool allow_dialogue_to_continue(ped_index mission_ped, vehicle_index mission_veh, int instruct_text_time, bool allow_wanted_level_check = false)

	if not is_ped_injured(mission_ped)
		if is_ped_in_group(mission_ped)
			
			if allow_wanted_level_check
				if is_player_wanted_level_greater(player_id(), 0) 
					instruct_text_time = get_game_timer()
					return false
				endif 
				
				//if you get a wanted level and the god text renders. If you then lose the wanted level and get
				//instruction text this ensures the instruction text is rendered for 3 seconds before the 
				//dialogue is unpaused
				if not lk_timer(instruct_text_time, 3000) 
					return false
				endif 
			endif 
		
			if (is_ped_on_foot(mission_ped) and is_ped_on_foot(player_ped_id()))
				return true 
			endif 
			
			if does_entity_exist(mission_veh)
				if is_vehicle_driveable(mission_veh)
					if (is_ped_sitting_in_vehicle(mission_ped, mission_veh) and is_ped_sitting_in_vehicle(player_ped_id(), mission_veh))
						return true 
					endif 
				endif 
			endif 
		endif 
	endif 
	
	return false
endfunc 


proc repostion_players_last_vehicle(vehicle_index &mission_veh, vector area_pos_check, vector area_dimensions, vector veh_pos, float veh_heading)
	
	mission_veh = get_players_last_vehicle()

	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)
			if not is_big_vehicle(mission_veh)
				if not (get_entity_model(mission_veh) = taxi)
					if is_entity_at_coord(mission_veh, area_pos_check, area_dimensions)
					
						if not is_entity_a_mission_entity(mission_veh)
							set_entity_as_mission_entity(mission_veh)
						endif 
						
						if not is_vehicle_in_players_garage(mission_veh, get_current_player_ped_enum(), false)
						
							clear_area(veh_pos, 10.0, true)
							set_entity_coords(mission_veh, veh_pos)  
							set_entity_heading(mission_veh, veh_heading)
							set_vehicle_on_ground_properly(mission_veh)
							
						endif 
					endif 
				endif 
			endif 
		endif
	endif 
	
endproc 


func bool is_players_last_vehicle_present_and_acceptable(vehicle_index player_vehicle, vector pos, int number_of_seats, vector secondary_pos, float secondary_heading)
	
	if does_entity_exist(player_vehicle)
		
		if is_vehicle_driveable(player_vehicle)
			
			if is_entity_at_coord(player_vehicle, pos, <<10.0, 10.0, 10.0>>, false)//<<25.0, 25.0, 10.0>>
				
				INT iNumVehSeats = get_vehicle_max_number_of_passengers(player_vehicle)
				CPRINTLN(DEBUG_MISSION, "Number of passenger seats = ", iNumVehSeats)

				if iNumVehSeats >= number_of_seats

					return true 
					
				else 
			
					clear_area(secondary_pos, 10.0, true)
					set_entity_coords(player_vehicle, secondary_pos)
					set_entity_heading(player_vehicle, secondary_heading)
					set_vehicle_engine_on(player_vehicle, false, true)
					set_vehicle_on_ground_properly(player_vehicle)
					set_vehicle_as_no_longer_needed(player_vehicle)

				endif
				
			endif 
		
		endif 
		
	endif 
	
	CPRINTLN(DEBUG_MISSION,"Need a different vehicle")
	return false
	
endfunc 

func bool is_enemy_near_player(ped_structure &mission_struct[], float distance)
				
	for i = 0 to count_of(mission_struct) - 1
		if not is_ped_injured(mission_struct[i].ped)
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_struct[i].ped)) < distance
				return true 
			endif 
		endif 
	endfor 
	
	return false 
	
endfunc 

func bool is_direction_stick_pushed()

	int left_stick_x
	int left_stick_y
	int right_stick_x
	int right_stick_y
	int stick_dead_zone = 127

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

	IF NOT IS_LOOK_INVERTED()
		right_stick_y *= -1
	ENDIF
	
	IF (left_stick_y > (STICK_DEAD_ZONE + 28))
	OR (left_stick_y < (STICK_DEAD_ZONE - 28))	

		return true
	
	endif 
	
	return false
	
endfunc 

//PARAM NOTES:
//PURPOSE: If a players fires a weapon during a cutscene and you want to ensure he has the weapon
// and enough ammo during the cutscene. Also for the ammo to be reset at the end so that when the
//player goes back into gameplay he has the same amount of ammo or decent amount.
//At the end of a cutscene ensure you set the players ammo to ammo_in_players_weapon
proc setup_players_weapon_for_cutscene(weapon_type mission_weapon, int ammo_amount, int &ammo_in_players_weapon)

	if not has_ped_got_weapon(player_ped_id(), mission_weapon)
	
		ammo_in_players_weapon = ammo_amount
		give_weapon_to_ped(player_ped_id(), mission_weapon, ammo_in_players_weapon, true, true)

	else 
		
		ammo_in_players_weapon = get_ammo_in_ped_weapon(player_ped_id(), mission_weapon)
		
		weapon_type players_weapon
		
		get_current_ped_weapon(player_ped_id(), players_weapon)
		
		if players_weapon != mission_weapon
			set_current_ped_weapon(player_ped_id(), mission_weapon, true)
		endif 

		if ammo_in_players_weapon < ammo_amount
			ammo_in_players_weapon = ammo_amount
			set_ped_ammo(player_ped_id(), mission_weapon, ammo_in_players_weapon) 
		endif 
	endif 
	
endproc

/// PURPOSE:
///    Sorts the triggered label array such that all the empty elements are at the end.
PROC REMOVE_LABEL_ARRAY_SPACES()
    INT k = 0
 	REPEAT (COUNT_OF(i_triggered_text_hashes) - 1) k
	    IF i_triggered_text_hashes[k] = 0
	    	IF i_triggered_text_hashes[k+1] != 0
	        	i_triggered_text_hashes[k] = i_triggered_text_hashes[k+1]
	            i_triggered_text_hashes[k+1] = 0
	        ENDIF
	    ENDIF
    ENDREPEAT
ENDPROC

/// PURPOSE:
///    Finds the array index of a particular triggered label, or -1 if the label hasn't been added.
FUNC INT GET_LABEL_INDEX(INT i_label_hash)
    INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
		IF i_triggered_text_hashes[k] = 0 //We've reached the end of the filled section of the array, no need to continue.
        	RETURN -1
        ELIF i_triggered_text_hashes[k] = i_label_hash
        	RETURN k
        ENDIF
    ENDREPEAT
      
    RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given label has been triggered.  
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING str_label)
	RETURN (GET_LABEL_INDEX(GET_HASH_KEY(str_label)) != -1)
ENDFUNC

/// PURPOSE:
///    Adds/removes a text label to/from the list of labels that have been triggered.
PROC SET_LABEL_AS_TRIGGERED(STRING str_label, BOOL b_trigger)
	
	INT i_hash = GET_HASH_KEY(str_label)
	INT k = 0

	IF b_trigger
	    BOOL b_added = FALSE
	    
	    WHILE NOT b_added AND k < COUNT_OF(i_triggered_text_hashes)
	    	
			IF i_triggered_text_hashes[k] = i_hash //The label is already in the array, don't add it again.
	        	b_added = TRUE 
	        ELIF i_triggered_text_hashes[k] = 0
	        	i_triggered_text_hashes[k] = i_hash
	            b_added = TRUE
	        ENDIF
	          
	        k++
	    ENDWHILE

	    #IF IS_DEBUG_BUILD
	    	IF NOT b_added
	        	SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Failed to add label, array is full.")
	        ENDIF
	    #ENDIF
	ELSE
	    
		INT i_index = GET_LABEL_INDEX(i_hash)
	    IF i_index != -1
			i_triggered_text_hashes[i_index] = 0
	        REMOVE_LABEL_ARRAY_SPACES()
	    ENDIF
		
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
   		i_triggered_text_hashes[k] = 0
	ENDREPEAT
ENDPROC


proc flash_ambient_enemy_blips(int &flash_timer)

	if allow_ambient_enemy_blips_to_flash
	
		for i = 0 to count_of(ambient_enemy) - 1
			if not is_ped_injured(ambient_enemy[i].ped)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(ambient_enemy[i].ped)) > 50.00
					if does_blip_exist(ambient_enemy[i].blip)
						remove_blip(ambient_enemy[i].blip)
					endif 
				endif 
			ELSE
				if does_blip_exist(ambient_enemy[i].blip)
					remove_blip(ambient_enemy[i].blip)
				endif 
			endif 
		endfor 
				

		if lk_timer(flash_timer, 500)

			for i = 0 to count_of(ambient_enemy) - 1
			
				if does_blip_exist(ambient_enemy[i].blip)

					if display_blips_as_friendly
						set_blip_as_friendly(ambient_enemy[i].blip, true)
					else 
						set_blip_as_friendly(ambient_enemy[i].blip, false)
					endif 

				endif 
				
			endfor
			
			if display_blips_as_friendly
				display_blips_as_friendly = false
			else 
				display_blips_as_friendly = true
			endif 
			
			flash_timer = get_game_timer()
			
		endif 

	endif 
	
endproc 

func bool is_player_in_cover_pos(cover_point_struct cover_data)
						
	if is_entity_at_coord(player_ped_id(), cover_data.pos, <<1.0, 1.0, 2.0>>)
		if is_ped_in_cover(player_ped_id())
			return true 
		endif 
	endif 
	
	return false 

endfunc 

/// PURPOSE:
///    Checks if the player is currently hanging around where the given ped is trying to get to. This is useful for checking if peds 
///    playing synced scenes need to break into AI due to the player getting in the way.
/// PARAMS:
///    ped - The ped moving towards the destination.
///    v_destination - The destination
///    f_max_dist_before_check - The ped must be within this distance of the destination before we start checking what the player is doing.
///    b_player_must_be_in_cover - If TRUE then the player must be in cover for them to be considered in the way.  
FUNC BOOL IS_PLAYER_STEALING_PEDS_DESTINATION(PED_INDEX ped, VECTOR destination, FLOAT distance_check, BOOL player_must_be_in_cover = true)
	
	IF NOT IS_PED_INJURED(ped)
		
		VECTOR player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR ped_pos = GET_ENTITY_COORDS(ped)

		FLOAT dist_from_ped_to_dest = VDIST(ped_pos, destination)
		FLOAT dist_from_player_to_dest = VDIST(player_pos, destination)

		IF dist_from_ped_to_dest < distance_check
		
			//CPRINTLN(DEBUG_MISSION, "IS_PLAYER_STEALING_PEDS_DESTINATION: test 0")	
		
			IF dist_from_player_to_dest < dist_from_ped_to_dest
			and is_entity_at_coord(player_ped_id(), destination, <<1.0, 1.0, 2.0>>) 
				
				CPRINTLN(DEBUG_MISSION, "IS_PLAYER_STEALING_PEDS_DESTINATION: test 1")	
				
				IF player_must_be_in_cover
					IF IS_PED_IN_COVER(PLAYER_PED_ID())
					//or is_ped_going_into_cover(player_ped())
						CPRINTLN(DEBUG_MISSION, "IS_PLAYER_STEALING_PEDS_DESTINATION: test 2")	
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//put_ped_into
proc get_buddy_into_cover(ped_index &mission_ped, cover_point_struct &first_cover_data, cover_point_struct &second_cover_data, bool &go_to_main_cover_point, bool &refresh_task)

	vector cover_pos

	if not is_ped_in_cover(mission_ped)
	
		if go_to_main_cover_point
		
			cover_pos = first_cover_data.pos
	
			if IS_PLAYER_STEALING_PEDS_DESTINATION(mission_ped, first_cover_data.pos, 3.0, false) 	
				cover_pos = second_cover_data.pos
				refresh_task = true
				go_to_main_cover_point = false
				exit
			endif 
			
		else 
		
			cover_pos = second_cover_data.pos
	
			if IS_PLAYER_STEALING_PEDS_DESTINATION(mission_ped, second_cover_data.pos, 3.0, false) 	
				cover_pos = first_cover_data.pos
				refresh_task = true
				go_to_main_cover_point = true
				exit
			endif 
			
		endif 

			
		if is_entity_at_coord(mission_ped, cover_pos, <<2.0, 2.0, 2.0>>)
	
			IF (NOT IS_PED_IN_COVER(mission_ped)
			AND NOT IS_PED_GOING_INTO_COVER(mission_ped)
			AND GET_SCRIPT_TASK_STATUS(mission_ped, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) != PERFORMING_TASK
			and GET_SCRIPT_TASK_STATUS(mission_ped, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT) != PERFORMING_TASK
			and GET_SCRIPT_TASK_STATUS(mission_ped, SCRIPT_TASK_SEEK_COVER_TO_COORDS) != performing_task)
			or refresh_task
			
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mission_ped, TRUE)

				TASK_PUT_PED_DIRECTLY_INTO_COVER(mission_ped, cover_pos, -1, FALSE, 0, false, false, null, true)
			
				refresh_task = false
				//script_assert("test 1")
			endif 
			
		else 
		
			if get_script_task_status(mission_ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != performing_task
			or refresh_task
		
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mission_ped, TRUE)
				
				task_follow_nav_mesh_to_coord(mission_ped, cover_pos, pedmove_run, -1)
				
				refresh_task = false
				
				//script_assert("test 3")
			
			endif 
			
		ENDIF
		
	endif 
	
endproc 



PROC EQUIP_BEST_PLAYER_WEAPON(ped_index mission_ped, BOOL bForceIntoHand = true)
  
  IF NOT IS_PED_INJURED(mission_ped)
       
		WEAPON_TYPE bestWeapon = GET_BEST_PED_WEAPON(mission_ped)
        
		IF bestWeapon != WEAPONTYPE_UNARMED
             SET_CURRENT_PED_WEAPON(mission_ped, bestWeapon, bForceIntoHand)
        ENDIF
  ENDIF
  
ENDPROC

proc setup_player_fail_weapon(ped_index mission_ped)

	weapon_type players_weapon

	players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
	
	if players_weapon != WEAPONTYPE_INVALID
	and players_weapon != WEAPONTYPE_UNARMED 
		set_current_ped_weapon(mission_ped, players_weapon, true)
	endif 
	
endproc

proc setup_weapon_for_ped(ped_index mission_ped, weapon_type ped_weapon, int num_of_bullets, bool force_weapon_into_hand = true, bool equip_weapon = true, bool equipe_best_weapon = true)
	
	IF not HAS_PED_GOT_FIREARM(mission_ped, false)
		
		give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
		exit
	
	else 
	
		weapon_type players_weapon

		if equipe_best_weapon
		
			EQUIP_BEST_PLAYER_WEAPON(mission_ped, force_weapon_into_hand)
			get_current_ped_weapon(mission_ped, players_weapon)
			
		else 
		
			players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
				
			if players_weapon = WEAPONTYPE_INVALID
			or players_weapon = WEAPONTYPE_UNARMED 
				
				if not HAS_PED_GOT_WEAPON(mission_ped, ped_weapon)
					give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
					exit
				else 
					players_weapon = ped_weapon 
				endif 
				
				//script_assert("test 0")

			endif 

		endif 
		
//		printint(enum_to_int(players_weapon))
//		printnl()
//		printint(enum_to_int(weapontype_pistol))
//		printnl()
//		script_assert("test 1")
		
		if players_weapon = WEAPONTYPE_GRENADELAUNCHER
		or players_weapon = WEAPONTYPE_RPG
			
			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < 5
				set_ped_ammo(mission_ped, players_weapon, 5)
			endif 
			
		else 

			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < num_of_bullets
				set_ped_ammo(mission_ped, players_weapon, num_of_bullets)
			endif 
			
		endif 
		
		set_current_ped_weapon(mission_ped, players_weapon, force_weapon_into_hand) 

	endif 
	
endproc 

func int get_first_alive_ped(ped_structure &mission_ped[])
							
	int k

	for k = 0 to count_of(mission_ped) - 1
	
		if not is_ped_injured(mission_ped[i].ped)
		
			return k 
			
		endif 
		
	endfor 
	
	return -1
	
endfunc 


func bool dialogue_monitoring_system(blip_index target_blip)

	if does_blip_exist(target_blip)
	
		// Unpause conversations if the locate blip is active
		IF IS_CELLPHONE_CONVERSATION_PAUSED()
			PAUSE_CELLPHONE_CONVERSATION(FALSE)
		ELIF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
			PAUSE_FACE_TO_FACE_CONVERSATION(false)
		endif
		
		if not is_any_text_being_displayed(locates_data)
			return true 
		endif 
	
	// If there's no blip, is there locate-related text on screen?
	ELIF IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
	
		// then pause any covnersation
		if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT IS_CELLPHONE_CONVERSATION_PAUSED()
				PAUSE_CELLPHONE_CONVERSATION(TRUE)
			ELIF not IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(true)
			endif 
		endif 
	ELSE
		// If there's still no locate blip, but the text is gone (or ignored), unpause the conversation so it can continue
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF IS_CELLPHONE_CONVERSATION_PAUSED()
				PAUSE_CELLPHONE_CONVERSATION(FALSE)
			ELIF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(false)
			ENDIF
		ENDIF
	
	endif 
	
	return false 

endfunc 

//PURPOSE: repositions the players last vehicle if it exists via calling GET_MISSION_START_VEHICLE_INDEX()
//		   and sets the vehicle as a mission entity so that clear area will not clear it. Remember to 

proc reposition_players_last_vehicle(vector car_pos, float car_heading)

	vehicle_index players_last_car 
	
	players_last_car = GET_MISSION_START_VEHICLE_INDEX()
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
		
			SET_ENTITY_AS_MISSION_ENTITY(players_last_car, true, true)
			
			clear_area(car_pos, 5.0, true)
			set_entity_coords(players_last_car, car_pos)
			set_entity_heading(players_last_car, car_heading)
			set_vehicle_on_ground_properly(players_last_car)
			SET_VEHICLE_DOORS_SHUT(players_last_car)
			set_entity_as_mission_entity(players_last_car)
			
			// @SBA - make sure we have anims for whatever the vehicle is
			request_vehicle_asset(GET_ENTITY_MODEL(players_last_car))
			
		endif 
	endif 
	
endproc 


// @SBA - kill enemies that have gotten away from the mission
PROC KILL_ANY_ERRANT_ENEMY(PED_STRUCTURE &psPedInfo[])
	INT idx
	BOOL bYouDieNow = FALSE
	
	FOR idx = 0 TO COUNT_OF(psPedInfo) - 1
		IF DOES_ENTITY_EXIST(psPedInfo[idx].ped)
			IF NOT IS_PED_INJURED(psPedInfo[idx].ped)
				// do we want to check this?  All examples so far, the ped was on foot.
				IF NOT IS_PED_IN_ANY_VEHICLE(psPedInfo[idx].ped)
					VECTOR vPedCoords = GET_ENTITY_COORDS(psPedInfo[idx].ped, FALSE)
					// If ped is far enough from meth lab and from player, he dies
					IF GET_DISTANCE_BETWEEN_COORDS(vArrivalMethLab, vPedCoords) > 200.0
					AND distance_from_player_to_ped(psPedInfo[idx].ped) > 50.0
						bYouDieNow = TRUE
					ELSE
						// see if enemy is below ground
						IF vPedCoords.z < 29.0
							bYouDieNow = TRUE
						ENDIF
					ENDIF
					
					IF bYouDieNow
						// remove flag
						IF psPedInfo[idx].bRushInside
							psPedInfo[idx].bRushInside = FALSE
						ENDIF
						// kill him
						EXPLODE_PED_HEAD(psPedInfo[idx].ped)
						APPLY_DAMAGE_TO_PED(psPedInfo[idx].ped, 1000, FALSE)
						CPRINTLN(DEBUG_MISSION,"KILL_ANY_ERRANT_ENEMY: *** ENEMY ", psPedInfo[idx].name," HAS BUGGERED OFF.  KILLING HIM! ***.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//Focus hint cam variables
VECTOR vEntityHint = <<-0.5, -1.00, 0.40>>
FLOAT fHintScalar = 0.41
FLOAT fHintHOffset = 0.12
FLOAT fHintVOffset = 0.11
//FLOAT fHintPitch = 0.0
FLOAT fHintFOV = 40.0
BOOL bResetFocusCam = FALSE

PROC HANDLE_HINT_CAM_FOR_LEAD_IN()

	IF bResetFocusCam
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT(TRUE)
		ENDIF
		SET_GAMEPLAY_ENTITY_HINT( oiIceBox, vEntityHint, TRUE, -1, 2500 )
		//SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( fHintPitch )
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( fHintHOffset )
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( fHintVOffset )
		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( fHintScalar )
		SET_GAMEPLAY_HINT_FOV( fHintFOV )
		bResetFocusCam = FALSE
	ENDIF
	

	// exit if we're coming from a shit skip, as the Ice box won't exist
	// and we won't need the hint cam (unless I'll need to show the lead in - I sure hope not)
	IF bOutroShitSkip
		EXIT
	ENDIF

	// is player in lead in trigger
	IF IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxCameraVol)
		// exit if player is also inside
		IF IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")
			EXIT
		ENDIF
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			SET_GAMEPLAY_ENTITY_HINT(oiIceBox, vEntityHint, TRUE, -1, 2500)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintScalar)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintHOffset)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVOffset)
	        SET_GAMEPLAY_HINT_FOV(fHintFOV)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
		ENDIF
	ELSE
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT()
		ENDIF
	ENDIF
	
	// if player is in cutscene trigger, halt him there
//	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
//		IF IS_GAMEPLAY_HINT_ACTIVE()
//			IF IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxInnerVol)
//				CPRINTLN(DEBUG_MISSION, "HANDLE_HINT_CAM_FOR_LEAD_IN: Locking player")
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
//				IF NOT IS_ENTITY_DEAD(translator.ped)
//					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), translator.ped)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

// ***********************************************************************************
// @SBA - Chef Abandonment functions
// ***********************************************************************************


// @SBA - iterate over ped array and see if any ped is in the given room
FUNC BOOL IS_ANY_PED_IN_SPECIFIC_ROOM(PED_STRUCTURE &psPedInfo[], VECTOR interior_pos, STRING room_name)
	INT idx
	FOR idx = 0 TO COUNT_OF(psPedInfo) - 1
		// Is enemy rush flag set (so we can ignore the others)
		IF psPedInfo[idx].bRushInside
			//
			IF is_ped_in_specific_room(psPedInfo[idx].ped, interior_pos, room_name)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

// @SBA - play dialogue if player is close enough
FUNC BOOL PLAY_CHEF_CONVERSATION(STRING sChefConv)
	// is player close enough
	IF distance_from_player_to_ped(buddy.ped) < 100
		IF CREATE_CONVERSATION(scripted_speech[0], "methaud", sChefConv, CONV_PRIORITY_HIGH) 
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

// @SBA - check for abandonment with specifics for the opening fight
FUNC BOOL HAS_PLAYER_ABANDONED_CHEF_WAVE_0()

	// See if player has left the upstairs rooms
	IF NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1392.2, 3614.1, 39.0>>), "v_39_UpperRm1")		
	AND NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1397.1, 3608.4, 38.5>>), "v_39_UpperRm2")
	AND NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1390.91, 3608.21, 38.91>>), "v_39_UpperRm3")	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// @SBA - check for abandonment with specifics for the back stairs fight
FUNC BOOL HAS_PLAYER_ABANDONED_CHEF_WAVE_0_PART2()

	IF distance_from_player_to_ped(buddy.ped) > 17.0
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_IN_TRIGGER_BOX(tbOutsideMainGatesVolume)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_TRIGGER_BOX(tbBackRoofVolume)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_TRIGGER_BOX(tbBackUpstairsRoomVolume)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1390.91, 3608.21, 38.91>>), "v_39_UpperRm3")		
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1393.0, 3612.2, 35.0>>), "v_39_StairsRm")		
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")		
		RETURN TRUE
	ENDIF	
	// Overlaps with tbBackUpstairsRoomVolume, added for fringes
	IF IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1397.1, 3608.4, 38.5>>), "v_39_UpperRm2")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// @SBA - check for abandonment with specifics for the back roof fight
FUNC BOOL HAS_PLAYER_ABANDONED_CHEF_WAVE_2()

	IF NOT IS_PLAYER_IN_TRIGGER_BOX(tbBackFullRoofVolume)
	AND NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1397.1, 3608.4, 38.5>>), "v_39_UpperRm2")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// @SBA - check for abandonment with specifics for the (final) liquor store fight
FUNC BOOL HAS_PLAYER_ABANDONED_CHEF_WAVE_3()

	IF NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")
	AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), (<<1394.18225, 3599.79858, 34.01332>>), (<<5.0, 5.0, 2.0>>))
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// @SBA - displays the passed message string if it hasn't been displayed before, and no text is on screen
PROC DISPLAY_MESSAGE_WHEN_ABLE(STRING sMessage)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sMessage)
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(LOCATES_DATA)
			PRINT_NOW(sMessage, DEFAULT_GOD_TEXT_TIME, 1)
			SET_LABEL_AS_TRIGGERED(sMessage, TRUE)
		ENDIF 
	ENDIF 
ENDPROC

// @SBA - displays a message to get back to Chef if you get too far away.  Repeats after a timer.
PROC HANDLE_ABANDONMENT_MESSAGE(BOOL bIsChefAbandoned)

	IF bIsChefAbandoned
		DISPLAY_MESSAGE_WHEN_ABLE("METH_GOD_18") // Return to Chef
		IF NOT IS_TIMER_STARTED(tmrAbandonChef)
			CPRINTLN(DEBUG_MISSION,"HANDLE_ABANDONMENT_MESSAGE: Player has buggered off - start timer.")
			START_TIMER_NOW(tmrAbandonChef)
		ELIF TIMER_DO_WHEN_READY(tmrAbandonChef, (default_god_text_time*2)/1000)
			CPRINTLN(DEBUG_MISSION, "HANDLE_ABANDONMENT_MESSAGE: Print Message.")
			//reset's the god text
			SET_LABEL_AS_TRIGGERED("METH_GOD_18", FALSE)
			CANCEL_TIMER(tmrAbandonChef)
		ENDIF
	ELSE
		// clear and reset the god text
		IF IS_THIS_PRINT_BEING_DISPLAYED("METH_GOD_18")
			CLEAR_THIS_PRINT("METH_GOD_18")
		ENDIF
		IF HAS_LABEL_BEEN_TRIGGERED("METH_GOD_18")
			SET_LABEL_AS_TRIGGERED("METH_GOD_18", FALSE)
		ENDIF
		IF IS_TIMER_STARTED(tmrAbandonChef)
			CPRINTLN(DEBUG_MISSION, "HANDLE_ABANDONMENT_MESSAGE: Player is back - cancel timer.")
			CANCEL_TIMER(tmrAbandonChef)
		ENDIF
	ENDIF
ENDPROC

// @SBA - resets stuff if Chef is no longer about to die
PROC RESET_ABANDONMENT_STUFF()
	IF IS_TIMER_STARTED(tmrCheckChefForLOS)
		CANCEL_TIMER(tmrCheckChefForLOS)
	ENDIF
	IF IS_TIMER_STARTED(tmrChefDeathTimer)
		CANCEL_TIMER(tmrChefDeathTimer)
	ENDIF
ENDPROC

// @SBA - kills the chef if he can't be seen
PROC KILL_THE_CHEF()
	
	IF NOT IS_PED_INJURED(buddy.ped)
		BOOL bKillChef = FALSE
		VECTOR vChefPos = GET_ENTITY_COORDS(buddy.ped)
		
		IF NOT IS_SPHERE_VISIBLE(vChefPos, 1.0)
			CPRINTLN(DEBUG_MISSION, "KILL_THE_CHEF: Chef's sphere not visible!")
			bKillChef = TRUE
		ELSE
			IF NOT IS_TIMER_STARTED(tmrCheckChefForLOS)
				START_TIMER_NOW(tmrCheckChefForLOS)
			ELIF TIMER_DO_WHEN_READY(tmrCheckChefForLOS, 1.000)
				IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), buddy.ped)
					CPRINTLN(DEBUG_MISSION, "KILL_THE_CHEF: Chef's not in LOS!")
					bKillChef = TRUE
					RESET_ABANDONMENT_STUFF()
				ENDIF
			ENDIF
		ENDIF

		IF bKillChef
			CPRINTLN(DEBUG_MISSION,"KILL_THE_CHEF: Killing the Chef Now!")
			// Do we need a sound?
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS((<<vChefPos.x + 0.75, vChefPos.y, vChefPos.z +1.0>>), vChefPos, 10000, TRUE)
			EXPLODE_PED_HEAD(buddy.ped)
			APPLY_DAMAGE_TO_PED(buddy.ped, 10000, TRUE)
		ENDIF
	ENDIF
ENDPROC

// @SBA - for first part of wave 0, handles killing the chef if he's overrun by bad guys and the player isn't there
PROC HANDLE_WAVE_0_CHEF_DEMISE()

	// Is Chef already on the outs?
	IF IS_PED_INJURED(buddy.ped)
		EXIT
	ENDIF

	// Is Player close enough to Chef?
	IF NOT HAS_PLAYER_ABANDONED_CHEF_WAVE_0()
		RESET_ABANDONMENT_STUFF()
		EXIT
	ENDIF
	
	// @SBA - check if bad guys are in the vicinity
	IF IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1391.1970, 3601.0510, 37.9418>>), "v_39_UpperRm3")
	OR IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1397.1, 3608.4, 38.5>>), "v_39_UpperRm2")
		IF NOT IS_TIMER_STARTED(tmrChefDeathTimer)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_0_CHEF_DEMISE: Chef is in danger - starting death timer!")
			START_TIMER_NOW(tmrChefDeathTimer)
			// play Chef's help dialogue
			IF PLAY_CHEF_CONVERSATION(sChefHelp1)
				CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_0_CHEF_DEMISE: Playing Chef's help dialogue!")
			ENDIF
		ENDIF
		// If timer is up, ok to kill the Chef
		IF TIMER_DO_WHEN_READY(tmrChefDeathTimer, kfChefDeathTime)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_0_CHEF_DEMISE: The Chef must die!")
			KILL_THE_CHEF()
		ENDIF
	ELSE
		RESET_ABANDONMENT_STUFF()
	ENDIF
ENDPROC

// @SBA - for second part of wave 0, handles killing the chef if he's overrun by bad guys and the player isn't there
PROC HANDLE_WAVE_0_PART_2_CHEF_DEMISE()
	
	// Is Chef already on the outs?
	IF IS_PED_INJURED(buddy.ped)
		EXIT
	ENDIF

	// Is Player close enough to Chef?
	IF NOT HAS_PLAYER_ABANDONED_CHEF_WAVE_0_PART2()
		RESET_ABANDONMENT_STUFF()
		EXIT
	ENDIF
	
	// @SBA - check if bad guys are in the vicinity
	IF IS_ANY_PED_IN_TRIGGER_BOX(wave_0_enemy, tbTopBackStairsVolume)
	OR IS_ANY_PED_IN_TRIGGER_BOX(wave_0_inside_enemy, tbTopBackStairsVolume)		
		IF NOT IS_TIMER_STARTED(tmrChefDeathTimer)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_0_PART_2_CHEF_DEMISE: Chef is in danger - starting death timer!")
			START_TIMER_NOW(tmrChefDeathTimer)
			// play Chef's help dialogue
			IF PLAY_CHEF_CONVERSATION(sChefHelp1) 
				CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_0_PART_2_CHEF_DEMISE: Playing Chef's help dialogue!")
			ENDIF
		ENDIF
		// If timer is up, ok to kill the Chef
		IF TIMER_DO_WHEN_READY(tmrChefDeathTimer, kfChefDeathTime)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_0_PART_2_CHEF_DEMISE: The Chef must die!")
			KILL_THE_CHEF()
		ENDIF
	ELSE
		RESET_ABANDONMENT_STUFF()
	ENDIF
ENDPROC

// @SBA - for wave 2, handles killing the chef if he's overrun by bad guys and the player isn't there
PROC HANDLE_WAVE_2_CHEF_DEMISE()
	
	// Is Chef already on the outs?
	IF IS_PED_INJURED(buddy.ped)
		EXIT
	ENDIF

	// Is Player close enough to Chef?
	IF NOT HAS_PLAYER_ABANDONED_CHEF_WAVE_2()
		RESET_ABANDONMENT_STUFF()
		EXIT
	ENDIF
	
	// @SBA - check if bad guys are in the vicinity
	IF IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackRoofVolume)
		IF NOT IS_TIMER_STARTED(tmrChefDeathTimer)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_2_CHEF_DEMISE: Chef is in danger - starting death timer!")
			START_TIMER_NOW(tmrChefDeathTimer)
			// play Chef's help dialogue
			IF PLAY_CHEF_CONVERSATION(sChefHelp1) 
				CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_2_CHEF_DEMISE: Playing Chef's help dialogue!")
			ENDIF
		ENDIF
		// If timer is up, ok to kill the Chef
		IF TIMER_DO_WHEN_READY(tmrChefDeathTimer, kfChefDeathTime)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_2_CHEF_DEMISE: The Chef must die!")
			KILL_THE_CHEF()
		ENDIF
	ELSE
		RESET_ABANDONMENT_STUFF()
	ENDIF
ENDPROC

// @SBA - for wave 2, handles killing the chef if he's overrun by bad guys and the player isn't there
PROC HANDLE_WAVE_3_CHEF_DEMISE()
	
	// Is Chef already on the outs?
	IF IS_PED_INJURED(buddy.ped)
		EXIT
	ENDIF

	// Is Player close enough to Chef?
	IF NOT HAS_PLAYER_ABANDONED_CHEF_WAVE_3()
		RESET_ABANDONMENT_STUFF()
		EXIT
	ENDIF	
	// @SBA - check if bad guys are in the vicinity
	IF IS_ANY_PED_IN_TRIGGER_BOX(wave_3_enemy, tbLiquorStoreVolume)
		IF NOT IS_TIMER_STARTED(tmrChefDeathTimer)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_3_CHEF_DEMISE: Chef is in danger - starting death timer!")
			START_TIMER_NOW(tmrChefDeathTimer)
			// play Chef's help dialogue
			IF PLAY_CHEF_CONVERSATION(sChefHelp1) 
				CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_3_CHEF_DEMISE: Playing Chef's help dialogue!")
			ENDIF
		ENDIF
		// If timer is up, ok to kill the Chef
		IF TIMER_DO_WHEN_READY(tmrChefDeathTimer, kfChefDeathTime)
			CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_3_CHEF_DEMISE: The Chef must die!")
			KILL_THE_CHEF()
		ENDIF
	ELSE
		RESET_ABANDONMENT_STUFF()
	ENDIF
ENDPROC

/// PURPOSE:
///    Chef will say a line of dialogue if he's taken enough damage
PROC HANDLE_CHEF_DAMAGED_DIALOGUE()
	
	// Is Chef already gone?
	IF IS_PED_INJURED(buddy.ped)
		EXIT
	ENDIF
	
	// only play once until reset
	IF HAS_LABEL_BEEN_TRIGGERED(sChefHurt)
		EXIT
	ENDIF
	
	// quit bellyaching about every little gun shot wound
	IF GET_ENTITY_HEALTH(buddy.ped) > 150
		EXIT
	ENDIF
	
	// ignore friendly fire (which should be fatal anyway)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(buddy.ped, PLAYER_PED_ID())
			EXIT
		ENDIF
	ENDIF

	// play Chef's owie dialogue
	IF PLAY_CHEF_CONVERSATION(sChefHurt) 
		CPRINTLN(DEBUG_MISSION,"HANDLE_WAVE_3_CHEF_DEMISE: Playing Chef's I'm hurt dialogue!")
		SET_LABEL_AS_TRIGGERED(sChefHurt, TRUE)
	ENDIF

ENDPROC

// @SBA - specific test for after back porch wave, on the way to last wave
FUNC BOOL IS_PLAYER_NEAR_CHEF_ON_STAIRS()
	
	VECTOR vplayer_position = GET_ENTITY_COORDS(player_ped_id()) 

	// IF player is already on ground floor, continue
	IF (vplayer_position.z < 37.00)
		RETURN TRUE
	ENDIF
	
	// Check distance to Chef if player isn't on other side of wall from him
	IF NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1390.91, 3608.21, 38.91>>), "v_39_UpperRm3")	
		// If the player is, is he close enough to Chef
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(buddy.ped), vplayer_position) < 5.5
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

// ***********************************************************************************
								

//*****WIDGETS
#IF IS_DEBUG_BUILD

bool widget_reset_cutscene = false
bool widget_create_object = false
bool widget_modify = false
bool widget_vehicle_camera_active = false
bool widget_reset_deformation = false
bool widget_apply_deformation = false
bool widget_activate_ptfx = false

int widget_blip_alpha

float widget_temp_width
float widget_temp_height
float widget_temp_depth
float widget_angled_length
float widget_anim_speed = 1.0
float widget_heading
float widget_fov = 45.00
float widget_blip_scale
float widget_vehicle_damage
float widget_vehicle_deformation

vector widget_offset_bl
vector widget_offset_br
vector widget_bl
vector widget_br
vector widget_object_pos
vector widget_object_rot
vector widget_object_pos_2
vector widget_object_rot_2
vector widget_cam_attach_offset
vector widget_cam_point_offset
vector widget_blip_pos
vector widget_veh_offset
vector widget_ptfx_pos
vector widget_ptfx_rot
	

camera_index widget_camera

//mission specific widget variables
int x_squares = 1
int y_squares = 1


proc locate_widget()

	vector widget_player_pos
			
	widget_player_pos = GET_ENTITY_COORDS(player_ped_id())
	IS_ENTITY_AT_COORD(player_ped_id(), widget_player_pos, <<widget_temp_width, widget_temp_depth, widget_temp_height>>, false, true)		

endproc

proc angled_area_locate_widget()

	widget_bl = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_bl) 
	widget_br = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_br)

	IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), widget_bl, widget_br, widget_angled_length, false)	
	
	if widget_offset_br.z > 0.0
		SET_ENTITY_HEADING(player_ped_id(), widget_heading)
	endif 

endproc

proc attach_object_to_ped_and_move(ped_index mission_ped, object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if DOES_ENTITY_EXIST(mission_object)
				ATTACH_ENTITY_TO_ENTITY(mission_object, mission_ped, bonetag_ph_r_hand, widget_object_pos, widget_object_rot)
			endif 
		endif 
	endif
	
endproc 

proc set_the_ped_anim_speed(ped_index mission_ped, string dict_name, string anim_name, float anim_speed)

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if IS_ENTITY_PLAYING_ANIM(mission_ped, dict_name, anim_name)
				SET_ENTITY_ANIM_SPEED(mission_ped, dict_name, anim_name, anim_speed)
			endif 
		endif 
	endif 
	
endproc

proc create_and_move_object(object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_object)
	
		SET_ENTITY_COORDS_NO_OFFSET(mission_object, widget_object_pos)
		SET_ENTITY_ROTATION(mission_object, widget_object_rot)  
	endif 
		
endproc 

proc create_and_move_object_2(object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_object)
		SET_ENTITY_COORDS_NO_OFFSET(mission_object, widget_object_pos_2)
		SET_ENTITY_ROTATION(mission_object, widget_object_rot_2)  
	endif 
		
endproc 

proc attach_object_to_object_and_move(object_index &parent_object, object_index &mission_object)

	if DOES_ENTITY_EXIST(parent_object)
		if DOES_ENTITY_EXIST(mission_object)
			ATTACH_ENTITY_TO_ENTITY(mission_object, parent_object, -1, widget_object_pos_2, widget_object_rot_2)
		endif 
	endif 
	
endproc 

proc create_and_move_pickup_widget(pickup_index &widget_mission_pickup, pickup_type pick_type, vector &pos, vector &rot)

	if widget_create_object
		if not widget_modify
			widget_mission_pickup = create_pickup_rotate(pick_type, pos, rot, true)
			
			widget_object_pos.x = pos.x 
			widget_object_pos.y = pos.y
			widget_object_pos.z = pos.z 
			widget_object_rot.x = rot.x
			widget_object_rot.y = rot.y
			widget_object_rot.z = rot.z
			
			widget_modify = true 
		else 
			
			if lk_timer(original_time, 500)
				if does_pickup_exist(widget_mission_pickup)
					remove_pickup(widget_mission_pickup)
				else
					widget_mission_pickup = create_pickup_rotate(pick_type, widget_object_pos, widget_object_rot, true)
				endif
				
				original_time = get_game_timer()
			endif 
		endif 
	endif 
	
endproc 

			
proc camera_attached_to_vehicle_widget(vehicle_index &mission_veh)

	if widget_vehicle_camera_active
		if not does_cam_exist(widget_camera)

			widget_camera = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(widget_camera, mission_veh, widget_cam_attach_offset)
			POINT_CAM_AT_ENTITY(widget_camera, mission_veh, widget_cam_point_offset)

			set_cam_active(widget_camera, true)
								
			render_script_cams(true, false)
			
		else 
		
			ATTACH_CAM_TO_ENTITY(widget_camera, mission_veh, widget_cam_attach_offset)
			
			POINT_CAM_AT_ENTITY(widget_camera, mission_veh, widget_cam_point_offset)
			
			set_cam_fov(widget_camera, widget_fov)
			
		endif 

	endif 

endproc 

proc blip_pos_scale_and_alpha_widget(blip_index &mission_blip)

	if does_blip_exist(mission_blip)
	
		set_blip_coords(mission_blip, widget_blip_pos)
		set_blip_scale(mission_blip, widget_blip_scale)
		set_blip_alpha(mission_blip, widget_blip_alpha)
	
	else 
	
		mission_blip = add_blip_for_coord(widget_blip_pos)
		set_blip_as_friendly(mission_blip, true)
		
	endif 
		
endproc 

proc vehicle_offset_system(vehicle_index miss_veh)
			
	if does_entity_exist(miss_veh)
		if is_vehicle_driveable(miss_veh)

			DRAW_MARKER(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(miss_veh, widget_offset_br), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<widget_blip_scale, widget_blip_scale, widget_blip_scale>>, 255, 0, 128, 178)
			
		endif 
	endif 

endproc 

proc vehicle_damage_widget(vehicle_index mission_vehicle)

	if does_entity_exist(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
		
			if widget_reset_deformation
				set_vehicle_fixed(mission_vehicle)
				widget_reset_deformation = false 
				widget_apply_deformation = false
			endif 
			
			if widget_apply_deformation
				SET_VEHICLE_DAMAGE(mission_vehicle, widget_veh_offset, widget_vehicle_damage, widget_vehicle_deformation, TRUE)
				widget_apply_deformation = false
			endif 
			
		endif 
	endif 
	
endproc 

proc pfx_widget()

	if widget_activate_ptfx
	
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_chin1_freezer_gust", widget_ptfx_pos, widget_ptfx_rot)
		
//		START_PARTICLE_FX_LOOPED_AT_COORD("cs_dry_ice_freezer_door", widget_ptfx_pos, widget_ptfx_rot)
		
//		cs_dry_ice_freezer_door
//		scr_chin1_freezer_gust
		
		widget_activate_ptfx = false
	
	endif 

endproc 


//**************************************************WIDGETS**************************************************

proc load_lk_widgets()
	
	chinese_1_widget_group = start_widget_group("Chinese1")
	
		// @SBA - uncomment to be able to switch whether to play the greneade cutscene or not
		//ADD_WIDGET_INT_SLIDER("Flip Grenade Launcher cutscene", g_replay.iReplayInt[PLAYER_RECEIVED_GRENADE_LAUNCHER], NOT_GOT_GLAUNCHER, HAS_GOT_GLAUNCHER, 1)
	
		#IF IS_DEBUG_BUILD
		start_widget_group("locate widget")
			add_widget_float_slider("width", widget_temp_width, -3000.0, 3000.0, 0.1)
			add_widget_float_slider("depth", widget_temp_depth, -3000.0, 3000.0, 0.1)
			add_widget_float_slider("height", widget_temp_height, -3000.0, 3000.0, 0.1)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD 
		start_widget_group("angled_area")
			add_widget_float_slider("left offset x", widget_offset_bl.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("left offset y", widget_offset_bl.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("left offset z", widget_offset_bl.z, -2000.0, 2000.0, 0.1)
			
			add_widget_float_slider("right offset x", widget_offset_br.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("right offset y", widget_offset_br.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("right offset z", widget_offset_br.z, -2000.0, 2000.0, 0.1)
			
			add_widget_float_slider("angled area length", widget_angled_length, -2000.0, 2000.0, 0.1)
			
			add_widget_float_slider("heading", widget_heading, -2000.0, 2000.0, 0.1)
			
			add_widget_float_read_only("widget_bl x", widget_bl.x)
			add_widget_float_read_only("widget_bl y", widget_bl.y)
			add_widget_float_read_only("widget_bl z", widget_bl.z)
			
			add_widget_float_read_only("widget_br x", widget_br.x)
			add_widget_float_read_only("widget_br y", widget_br.y)
			add_widget_float_read_only("widget_br z", widget_br.z)
			
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("object attach to ped widget")
			add_widget_float_slider("pos offset x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot offset x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("grid square widget")
			add_widget_float_slider("bl x", widget_bl.x, -5000.0, 5000.0, 0.01)
			add_widget_float_slider("bl y", widget_bl.y, -5000.0, 5000.0, 0.01)
			add_widget_float_slider("bl z", widget_bl.z, -5000.0, 5000.0, 0.01)
			
			add_widget_float_slider("tr x", widget_br.x, -5000.0, 5000.0, 0.01)
			add_widget_float_slider("tr y", widget_br.y, -5000.0, 5000.0, 0.01)
			add_widget_float_slider("tr z", widget_br.z, -5000.0, 5000.0, 0.01)
			
			add_widget_int_slider("x_squares", x_squares, 1, 500, 1)
			add_widget_int_slider("y_squares", y_squares, 1, 500, 1)
				
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("anim speed widget")
			add_widget_float_slider("anim speed", widget_anim_speed, 0.1, 10.0, 0.01)
		stop_widget_group()
		#endif
		
		
		#IF IS_DEBUG_BUILD
		start_widget_group("reset cutscene widget")
			add_widget_bool("reset cutscene", widget_reset_cutscene)
		stop_widget_group()
		#endif
		
		
		#IF IS_DEBUG_BUILD
		start_widget_group("object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos.x, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos.y, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos.z, -4000.0, 4000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot.x, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot.y, -4000.0, 4000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot.z, -4000.0, 4000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("object pos and rot widget 2")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("attach object to object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD 
		start_widget_group("create and move PICKUP inside interior")
			ADD_WIDGET_bool("create_object", widget_create_object)
			
			ADD_WIDGET_FLOAT_SLIDER("object pos x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("object pos y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("object pos z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("object rotation x", widget_object_rot.x, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("object rotation y", widget_object_rot.y, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("object rotation z", widget_object_rot.z, 0.0, 360.0, 0.1)
		stop_widget_group()
		#endif 
		
		start_widget_group("camaera attached to vehicle widget")
			add_widget_bool("create camera", widget_vehicle_camera_active)
			
			ADD_WIDGET_FLOAT_SLIDER("pos x", widget_cam_attach_offset.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos y", widget_cam_attach_offset.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos z", widget_cam_attach_offset.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("point offset x", widget_cam_point_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("point offset y", widget_cam_point_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("point offset z", widget_cam_point_offset.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("FOV", widget_fov, -3000.0, 3000.0, 0.1)
		stop_widget_group()
		
		
		start_widget_group("blip position, scale and alpha widget")
			
			ADD_WIDGET_FLOAT_SLIDER("pos x", widget_blip_pos.x, -4000.0, 4000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos y", widget_blip_pos.y, -4000.0, 4000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos z", widget_blip_pos.z, -4000.0, 4000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("scale", widget_blip_scale, -1.0, 40.0, 0.01)
			
			ADD_WIDGET_int_SLIDER("alpha", widget_blip_alpha, -1, 400, 1)
			
		stop_widget_group()
		
		
		start_widget_group("vehicle offset sphere")
			
			ADD_WIDGET_FLOAT_SLIDER("offset x", widget_offset_br.x, -400.0, 400.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("offset y", widget_offset_br.y, -400.0, 400.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("offset z", widget_offset_br.z, -400.0, 400.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("sphere radius", widget_blip_scale, -1.0, 40.0, 0.01)
			
		stop_widget_group()
		
		start_widget_group("vehicle damage")
			ADD_WIDGET_bool("Reset vehicle deformation", widget_reset_deformation)
			ADD_WIDGET_bool("Apply vehicle deformation", widget_apply_deformation)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle offset x", widget_veh_offset.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("vehicle offset y", widget_veh_offset.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("vehicle offset z", widget_veh_offset.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle damage", widget_vehicle_damage, 0.0, 2000, 1000)
			ADD_WIDGET_FLOAT_SLIDER("vehicle deformation", widget_vehicle_deformation, 0.0, 2000, 1000)

		stop_widget_group()
		
		START_WIDGET_GROUP("Focus push tweaks")
			ADD_WIDGET_VECTOR_SLIDER( "HINT ENTITY OFFSET", vEntityHint, -1.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER( "HINT SCALAR", fHintScalar, 0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER( "HINT HORIZONTAL", fHintHOffset, -1.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER( "HINT VERTICAL", fHintVOffset, -1.0, 1.0, 0.01 )
			//ADD_WIDGET_FLOAT_SLIDER( "HINT PITCH", fHintPitch, -90.0, 90.0, 1.0 )
			ADD_WIDGET_FLOAT_SLIDER( "HINT FOV", fHintFOV, 0, 50.0, 1.0 )
			ADD_WIDGET_BOOL( "RESET HINT", bResetFocusCam )
		STOP_WIDGET_GROUP()
		
		start_WIDGET_GROUP("ptfx widget")
			add_widget_bool("toggle ptfx",  widget_activate_ptfx)
			add_widget_float_slider("widget_ptfx_pos x", widget_ptfx_pos.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_pos y", widget_ptfx_pos.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_pos z", widget_ptfx_pos.z, -4000.0, 4000.0, 0.1)
			
			add_widget_float_slider("widget_ptfx_rot x", widget_ptfx_rot.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_rot y", widget_ptfx_rot.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_rot z", widget_ptfx_rot.z, -4000.0, 4000.0, 0.1)
			
		stop_widget_group()

	stop_widget_group()

endproc

#endif



//**************************************************LOCAL MISSION FUNCTIONS**************************************************

// @SBA - reset some values for Chef's warning system
PROC RESET_CHEF_WARNINGS_FOR_NEXT_WAVE()
	SET_LABEL_AS_TRIGGERED(sChefWarn1, FALSE)
	SET_LABEL_AS_TRIGGERED(sChefWarn2, FALSE)
	SET_LABEL_AS_TRIGGERED(sChefBackStairsWarn, FALSE)
	SET_LABEL_AS_TRIGGERED(sChefHurt, FALSE)
	CANCEL_TIMER(tmrChefOutburstTimer)
	RESET_ABANDONMENT_STUFF()
ENDPROC

// @SBA - clean up the script-created cover points
PROC REMOVE_ALL_COVERPOINTS()
	INT idx
	
	REMOVE_COVER_POINT(players_cover_point)
	REMOVE_COVER_POINT(players_cover_point_2)
	REMOVE_COVER_POINT(wave_3_0_cover_point)
	REMOVE_COVER_POINT(wave_3_1_cover_point)
	REMOVE_COVER_POINT(cpWaterHeaterCover)
	REMOVE_COVER_POINT(cover_point_back_roof_AC)
	
	REPEAT COUNT_OF(buddy_cover_point) idx
		REMOVE_COVER_POINT(buddy_cover_point[idx])
	ENDREPEAT
	REPEAT COUNT_OF(buddy_cover_point_data) idx
		REMOVE_COVER_POINT(buddy_cover_point_data[idx].cover_point)
	ENDREPEAT
	
ENDPROC

PROC REMOVE_ALL_RECORDINGS()

	REMOVE_VEHICLE_RECORDING(001, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(002, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(003, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(007, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(008, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(009, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(010, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(011, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(012, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(017, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(018, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(019, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(020, "lkmethlab")
	REMOVE_VEHICLE_RECORDING(021, "lkmethlab")
	
	REMOVE_WAYPOINT_RECORDING("methlab1")
	REMOVE_WAYPOINT_RECORDING("methlab3")
	REMOVE_WAYPOINT_RECORDING("methlab4")
	REMOVE_WAYPOINT_RECORDING("methlab5")
	REMOVE_WAYPOINT_RECORDING("methlab7")
ENDPROC

PROC UNLOCK_ALL_MISSION_VEHICLES()

	if does_entity_exist(trevors_truck.veh)
		if is_vehicle_driveable(trevors_truck.veh)
			CPRINTLN(DEBUG_MISSION, "UNLOCK_ALL_MISSION_VEHICLES: unlocking player's vehicle")
			set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_unlocked)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, true)
		endif 
	endif 

	for i = 0 to count_of(parked_cars) - 1
		if does_entity_exist(parked_cars[i])
			if is_vehicle_driveable(parked_cars[i])
				set_vehicle_doors_locked(parked_cars[i], vehiclelock_unlocked) 
			endif
		endif 
	endfor 

	for i = 0 to count_of(wave_0_enemy_vehicle) - 1
		if does_entity_exist(wave_0_enemy_vehicle[i].veh)
			if not is_entity_dead(wave_0_enemy_vehicle[i].veh)
				CPRINTLN(DEBUG_MISSION, "wave_0_enemy_vehicle not dead: ", i)
				if is_vehicle_driveable(wave_0_enemy_vehicle[i].veh)
					set_vehicle_doors_locked(wave_0_enemy_vehicle[i].veh, vehiclelock_unlocked) 
					CPRINTLN(DEBUG_MISSION, "doors unlocked vehicle driveable: ", i)
				else 
					if i = 1 //blue jeep
						if get_vehicle_petrol_tank_health(wave_0_enemy_vehicle[i].veh) < 500
							set_vehicle_petrol_tank_health(wave_0_enemy_vehicle[i].veh, 1000.00)
						endif 
						SET_VEHICLE_CAN_LEAK_PETROL(wave_0_enemy_vehicle[i].veh, FALSE)
						set_vehicle_doors_locked(wave_0_enemy_vehicle[i].veh, vehiclelock_unlocked) 
					endif 
				endif 
			else 
				CPRINTLN(DEBUG_MISSION, "wave_0_enemy_vehicle DEAD: ", i)
			endif
		endif 
	endfor 
		
	for i = 0 to count_of(wave_2_enemy_vehicle) - 1
		if does_entity_exist(wave_2_enemy_vehicle[i].veh)
			if is_vehicle_driveable(wave_2_enemy_vehicle[i].veh)
				set_vehicle_doors_locked(wave_2_enemy_vehicle[i].veh, vehiclelock_unlocked) 
			endif
		endif 
	endfor 
	
	for i = 0 to count_of(wave_3_enemy_vehicle) - 1
		if does_entity_exist(wave_3_enemy_vehicle[i].veh)
			if is_vehicle_driveable(wave_3_enemy_vehicle[i].veh)
				set_vehicle_doors_locked(wave_3_enemy_vehicle[i].veh, vehiclelock_unlocked) 
			endif
		endif 
	endfor
	
	// @SBA - added this
	for i = 0 to count_of(ambient_enemy_vehicle) - 1
		if does_entity_exist(ambient_enemy_vehicle[i].veh)
			if is_vehicle_driveable(ambient_enemy_vehicle[i].veh)
				set_vehicle_doors_locked(ambient_enemy_vehicle[i].veh, vehiclelock_unlocked)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(ambient_enemy_vehicle[i].veh, true)
			endif
		endif 
	endfor 
ENDPROC

/// PURPOSE:
///     @SBA - trying to free up more space for the cutscene.  Set FALSE to reset to normal.
PROC REDUCE_AMBIENT_MODELS(BOOL bTurnOn)
	SET_REDUCE_VEHICLE_MODEL_BUDGET(bTurnOn) 
	SET_REDUCE_PED_MODEL_BUDGET(bTurnOn)
	IF bTurnOn
		CPRINTLN(DEBUG_MISSION, "REDUCE_AMBIENT_MODELS: Setting ON")
		SET_VEHICLE_POPULATION_BUDGET(0) 
		SET_PED_POPULATION_BUDGET(0)
	ELSE
		CPRINTLN(DEBUG_MISSION, "REDUCE_AMBIENT_MODELS: Setting OFF")
		SET_VEHICLE_POPULATION_BUDGET(3) 
		SET_PED_POPULATION_BUDGET(3)
	ENDIF

ENDPROC

PROC CLEAN_UP_NAVMESH_BLOCKERS()

	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlockingUpstairsDoorway)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockingUpstairsDoorway)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlockingUpstairsDoor)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockingUpstairsDoor)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavBlockingBackYardTrailer)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockingBackYardTrailer)
	ENDIF
ENDPROC


PROC CLEANUP_BAR()

	IF DOES_ENTITY_EXIST(piBarLady)
		SET_PED_AS_NO_LONGER_NEEDED(piBarLady)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_JANET)
	IF DOES_ENTITY_EXIST(piJosef)
		SET_PED_AS_NO_LONGER_NEEDED(piJosef)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOSEF)
	IF DOES_ENTITY_EXIST(piOldMan1A)
		SET_PED_AS_NO_LONGER_NEEDED(piOldMan1A)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN1A)
	IF DOES_ENTITY_EXIST(piOldMan2)
		SET_PED_AS_NO_LONGER_NEEDED(piOldMan2)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN2)
	IF DOES_ENTITY_EXIST(piDrunk)
		SET_PED_AS_NO_LONGER_NEEDED(piDrunk)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_RUSSIANDRUNK)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiBarBlockingArea)
ENDPROC

proc mission_cleanup()

	kill_any_conversation()

	set_fake_wanted_level(0)
	//g_allowmaxwantedlevelcheck = true
	// @SBA set to 6?  Below, CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN sets it to 5.
	//set_max_wanted_level(6)  

	set_create_random_cops(true)
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), false)
	
	REDUCE_AMBIENT_MODELS(FALSE)
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, true)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, true)

	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	// Spawn Chef
	IF bMissionPassed
		SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA((<<1389.77686, 3600.51782, 38.0>>), 3.0, 1)
		bMissionPassed = FALSE
	ENDIF

	ASSISTED_MOVEMENT_remove_ROUTE("meth1")
	ASSISTED_MOVEMENT_remove_ROUTE("Meth1_new")
	ASSISTED_MOVEMENT_remove_ROUTE("Meth2")
	ASSISTED_MOVEMENT_remove_ROUTE("methrew_newA")
	ASSISTED_MOVEMENT_remove_ROUTE("methrew_newB")
	ASSISTED_MOVEMENT_remove_ROUTE("Wave01457s")
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, false, 0.0, 0.0, 0.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, false, 0.0, 0.0, 0.0)

	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
				
	//stops the buddy from attacking the player when the mission cleansup
	if DOES_ENTITY_EXIST(buddy.ped)
		if not is_ped_injured(buddy.ped)
			SET_PED_RELATIONSHIP_GROUP_HASH(buddy.ped, RELGROUPHASH_PLAYER)
		endif 
	endif 
	
	enable_dispatch_services()
	
	clear_mission_locate_stuff(locates_data, true)
	
	set_time_scale(1.0)
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	if is_player_playing(player_id())
		set_entity_proofs(player_ped_id(), false, false, false, false, false)
		SET_PED_USING_ACTION_MODE(player_ped_id(), false)
	ENDIF
	
	CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
		CPRINTLN(DEBUG_MISSION, "mission_cleanup: calling NEW_LOAD_SCENE_STOP.")
	ENDIF
	
	// clean up srl if loaded
	END_SRL()
	
	// @SBA - allow player into vehicles again
	UNLOCK_ALL_MISSION_VEHICLES()
	
	// @SBA - remove tactical points
	CLEAR_TACTICAL_NAV_MESH_POINTS()
	
	// @SBA - reset door states
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, FALSE, 0.0, 0.0, 0.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, FALSE, 0.0, 0.0, 0.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_door04, vInteriorDoorStore, FALSE, 0.0, 0.0, 0.0)

	// @SBA - clean up stuff I added
	IF DOES_PICKUP_EXIST(puiGrenadeLauncher)
		REMOVE_PICKUP(puiGrenadeLauncher)
	ENDIF
	IF DOES_ENTITY_EXIST(oiIceBox)
		DELETE_OBJECT(oiIceBox)
	ENDIF
	IF DOES_ENTITY_EXIST(oiIceBoxProxy)
		DELETE_OBJECT(oiIceBoxProxy)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(mnIceBox)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnIceBoxProxy)
	REMOVE_MODEL_HIDE(vIceBoxPos, 2.0, prop_ice_box_01)
	
	// @SBA - these didn't appear to be getting cleaned up
	REMOVE_ANIM_DICT(sCrazyDanceDict)
	REMOVE_ANIM_DICT("MissChinese1")
	REMOVE_ANIM_DICT("MissChinese1LeadInOutCHI_1_MCS_4")
	// Same with recordings - maybe it's automatic?  But doesn't hurt.
	REMOVE_ALL_RECORDINGS()
	
	REMOVE_ALL_COVERPOINTS()
	
	CLEANUP_BAR()
	
	remove_all_blips()

	RESET_CHEF_WARNINGS_FOR_NEXT_WAVE()
	
	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
	
	STOP_AUDIO_SCENES()
	RELEASE_SCRIPT_AUDIO_BANK()
	
	trigger_music_event("CHN1_FAIL")

	terminate_this_thread()

endproc 
		
proc mission_passed()

	Mission_Flow_Mission_Passed()
	
	REMOVE_ALL_COVERPOINTS()
	
	clear_player_wanted_level(player_id())
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
	// @SBA - reset player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	bMissionPassed = TRUE
	mission_cleanup()

endproc 

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(veh)
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF IS_ENTITY_A_MISSION_ENTITY(veh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh, false)
			IF b_force_delete
				DELETE_VEHICLE(veh)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
			ENDIF
		endif 
		
	ENDIF
ENDPROC

proc remove_all_vehicles()

	REMOVE_VEHICLE(trevors_truck.veh, true)

endproc 

proc delete_assets_for_mission_fail()

	//remove_all_vehicles()

	if does_entity_exist(buddy.ped)
		DELETE_PED(buddy.ped)
	endif 
	set_model_as_no_longer_needed(buddy.model)
	
	if does_entity_exist(cheng.ped)
		DELETE_PED(cheng.ped)
	endif 
	set_model_as_no_longer_needed(cheng.model)
	
	if does_entity_exist(translator.ped)
		DELETE_PED(translator.ped)
	endif 
	set_model_as_no_longer_needed(translator.model)


	for i = 0 to count_of(wave_0_enemy) - 1
		if does_entity_exist(wave_0_enemy[i].ped)
			DELETE_PED(wave_0_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_0_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_2_enemy) - 1
		if does_entity_exist(wave_2_enemy[i].ped)
			DELETE_PED(wave_2_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_2_enemy[i].model)
	endfor 
	
	for i = 0 to count_of(wave_3_enemy) - 1
		if does_entity_exist(wave_3_enemy[i].ped)
			DELETE_PED(wave_3_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_3_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(ambient_enemy) - 1
		if does_entity_exist(ambient_enemy[i].ped)
			DELETE_PED(ambient_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(ambient_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_0_inside_enemy) - 1
		if does_entity_exist(wave_0_inside_enemy[i].ped)
			DELETE_PED(wave_0_inside_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_0_inside_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_0_enemy_vehicle) - 1
		if does_entity_exist(wave_0_enemy_vehicle[i].veh)
			DELETE_VEHICLE(wave_0_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(wave_0_enemy_vehicle[i].model)
	endfor 

	
	for i = 0 to count_of(wave_2_enemy_vehicle) - 1
		if does_entity_exist(wave_2_enemy_vehicle[i].veh)
			DELETE_VEHICLE(wave_2_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(wave_2_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_3_enemy_vehicle) - 1
		if does_entity_exist(wave_3_enemy_vehicle[i].veh)
			DELETE_VEHICLE(wave_3_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(wave_3_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(ambient_enemy_vehicle) - 1
		if does_entity_exist(ambient_enemy_vehicle[i].veh)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), ambient_enemy_vehicle[i].veh)
					DELETE_VEHICLE(ambient_enemy_vehicle[i].veh)
				ENDIF
			ENDIF
		endif 
		set_model_as_no_longer_needed(ambient_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(parked_cars) - 1
		if does_entity_exist(parked_cars[i])
			DELETE_VEHICLE(parked_cars[i])
		endif 
	endfor 
	set_model_as_no_longer_needed(phoenix)
	
	for i = 0 to count_of(explosive_barrels) - 1
		if does_entity_exist(explosive_barrels[i].obj)
			DELETE_OBJECT(explosive_barrels[i].obj)
		endif 
		set_model_as_no_longer_needed(explosive_barrels[i].model)
	endfor 
	
	for i = 0 to count_of(bins) - 1
		if does_entity_exist(bins[i].obj)
			DELETE_OBJECT(bins[i].obj)
		endif 
		set_model_as_no_longer_needed(bins[i].model)
	endfor 
	
	for i = 0 to count_of(c4) - 1
		if does_entity_exist(c4[i].obj)
			DELETE_OBJECT(c4[i].obj)
		endif 
		set_model_as_no_longer_needed(c4[i].model)
	endfor 
	
	if does_entity_exist(cover_box.obj)
		delete_object(cover_box.obj)
		set_model_as_no_longer_needed(cover_box.model)
	endif 

endproc 

proc mission_failed()

	RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	//
	//CREATE_VEHICLE_FOR_REPLAY
				
	MISSION_CLEANUP() 
					
	delete_assets_for_mission_fail()
endproc 

func bool mission_fail_checks()

	switch mission_flow
	
		case intro_mocap
		
			if mission_ped_injured(cheng.ped)
				mission_failed_text = "METH_FAIL_0"
				return true
			endif 
			
			if mission_ped_injured(translator.ped)
				mission_failed_text = "METH_FAIL_1"
				return true
			endif 
			
			mission_vehicle_injured(trevors_truck.veh) 
		
		break 
	
		case get_to_meth_lab
		
			if mission_ped_injured(cheng.ped)
				mission_failed_text = "METH_FAIL_0"
				return true
			endif 
			
			if mission_ped_injured(translator.ped)
				mission_failed_text = "METH_FAIL_1"
				return true
			endif 

			IF does_entity_exist(cheng.ped)
			AND does_entity_exist(translator.ped)

				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(cheng.ped)) > 200.00
				and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(translator.ped)) > 200.00
					if not is_entity_on_screen(cheng.ped)
					and not is_entity_on_screen(translator.ped)
						//ADD OBANDONED CHENG TEXT
						mission_failed_text = "METH_FAIL_5"
						return true
					endif 
				endif
			ENDIF
			
			if does_entity_exist(cheng.ped)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(cheng.ped)) > 200.00
					if not is_entity_on_screen(cheng.ped)
						//ADD OBANDONED CHENG TEXT
						mission_failed_text = "METH_FAIL_6"
						return true
					endif 
				endif 
			endif 

			
			if does_entity_exist(translator.ped)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(translator.ped)) > 200.00
					if not is_entity_on_screen(translator.ped)
						//ADD OBANDONED TRANSLATOR TEXT
						mission_failed_text = "METH_FAIL_7"
						return true
					endif
				endif 
			endif 

		break 
		
		case wave_0_system 
		
			if mission_ped_injured(buddy.ped)
				mission_failed_text = "METH_FAIL_3"
				return true
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<1389.5963, 3600.4568, 37.9419>>) > 200.00
				mission_failed_text = "METH_FAIL_5"
				return true 
			endif 
		
		break 
				
		case wave_2_system 
		
			if mission_ped_injured(buddy.ped)
				mission_failed_text = "METH_FAIL_3"
				return true
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<1389.5963, 3600.4568, 37.9419>>) > 200.00
				mission_failed_text = "METH_FAIL_5"
				return true 
			endif 
			
		break 
		
		case wave_3_system
		case mission_passed_mocap
		
			if mission_ped_injured(buddy.ped)
				mission_failed_text = "METH_FAIL_3"
				return true
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<1389.5963, 3600.4568, 37.9419>>) > 200.00
				mission_failed_text = "METH_FAIL_5"
				return true 
			endif 

			if mission_ped_injured(cheng.ped)
				mission_failed_text = "METH_FAIL_0"
				return true
			endif 
			
			if mission_ped_injured(translator.ped)
				mission_failed_text = "METH_FAIL_1"
				return true
			endif 

			IF does_entity_exist(cheng.ped)
			AND does_entity_exist(translator.ped)

				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(cheng.ped)) > 200.00
				and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(translator.ped)) > 200.00
					if not is_entity_on_screen(cheng.ped)
					and not is_entity_on_screen(translator.ped)
						mission_failed_text = "METH_FAIL_5"
						return true
					endif 
				endif
			ENDIF
			
			if does_entity_exist(cheng.ped)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(cheng.ped)) > 200.00
					if not is_entity_on_screen(cheng.ped)
						mission_failed_text = "METH_FAIL_6"
						return true
					endif 
				endif 
			endif 

			if does_entity_exist(translator.ped)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(translator.ped)) > 200.00
					if not is_entity_on_screen(translator.ped)
						mission_failed_text = "METH_FAIL_7"
						return true
					endif
				endif 
			endif 

		break 
		
	endswitch 

	return false 

endfunc 


proc setup_mission_fail()

	STORE_ENEMY_VEHICLES_STILL_ALIVE()
	
	remove_all_blips()
	
	stop_mission_fail_checks = true
	
	trigger_music_event("CHN1_FAIL")
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(mission_failed_text)
	
	mission_flow = mission_fail_stage

endproc 

proc remove_all_mission_assets()

	if IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
	else 
		REMOVE_CUTSCENE()
	endif 

	while is_cutscene_active()
		wait(0)
	endwhile
	
	REMOVE_CUTSCENE()
	
	// @SBA - make sure to reset to game cam if needed and clean cameras
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	IF DOES_CAM_EXIST(camera_a)
		SET_CAM_ACTIVE(camera_a, FALSE)
		DESTROY_CAM(camera_a)
	ENDIF
	IF DOES_CAM_EXIST(camera_b)
		SET_CAM_ACTIVE(camera_b, FALSE)
		DESTROY_CAM(camera_b)
	ENDIF
	
	IF DOES_PICKUP_EXIST(puiGrenadeLauncher)
		REMOVE_PICKUP(puiGrenadeLauncher)
	ENDIF
			
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
		CPRINTLN(DEBUG_MISSION, "remove_all_mission_assets: calling NEW_LOAD_SCENE_STOP.")
	ENDIF
	
	clear_area(<<1403.1777, 3597.2070, 33.7417>>, 200.00, true)
	clear_area(<<1948.1082, 3842.8328, 31.2855>>, 200.00, true)

	clear_ped_tasks_immediately(player_ped_id())

	if does_entity_exist(trevors_truck.veh)
		delete_vehicle(trevors_truck.veh)
	endif

	clear_mission_locate_stuff(locates_data, true)
	
	REMOVE_SCENARIO_BLOCKING_AREAS()

	ASSISTED_MOVEMENT_remove_ROUTE("meth1")
	ASSISTED_MOVEMENT_remove_ROUTE("Meth1_new")
	ASSISTED_MOVEMENT_remove_ROUTE("Meth2")
	ASSISTED_MOVEMENT_remove_ROUTE("methrew_newA")
	ASSISTED_MOVEMENT_remove_ROUTE("methrew_newB")
	ASSISTED_MOVEMENT_remove_ROUTE("Wave01457s")
	
	REPLAY_CANCEL_EVENT()
	
	remove_all_blips()
	
	if does_entity_exist(buddy.ped)
		DELETE_PED(buddy.ped)
	endif 
	set_model_as_no_longer_needed(buddy.model)
	
	if does_entity_exist(cheng.ped)
		DELETE_PED(cheng.ped)
	endif 
	set_model_as_no_longer_needed(cheng.model)
	
	if does_entity_exist(translator.ped)
		DELETE_PED(translator.ped)
	endif 
	set_model_as_no_longer_needed(translator.model)


	for i = 0 to count_of(wave_0_enemy) - 1
		if does_entity_exist(wave_0_enemy[i].ped)
			DELETE_PED(wave_0_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_0_enemy[i].model)
	endfor 
	
	for i = 0 to count_of(wave_2_enemy) - 1
		if does_entity_exist(wave_2_enemy[i].ped)
			DELETE_PED(wave_2_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_2_enemy[i].model)
	endfor 
	
	for i = 0 to count_of(wave_3_enemy) - 1
		if does_entity_exist(wave_3_enemy[i].ped)
			DELETE_PED(wave_3_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_3_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(ambient_enemy) - 1
		if does_entity_exist(ambient_enemy[i].ped)
			DELETE_PED(ambient_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(ambient_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_0_inside_enemy) - 1
		if does_entity_exist(wave_0_inside_enemy[i].ped)
			DELETE_PED(wave_0_inside_enemy[i].ped)
		endif 
		set_model_as_no_longer_needed(wave_0_inside_enemy[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_0_enemy_vehicle) - 1
		if does_entity_exist(wave_0_enemy_vehicle[i].veh)
			DELETE_VEHICLE(wave_0_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(wave_0_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_2_enemy_vehicle) - 1
		if does_entity_exist(wave_2_enemy_vehicle[i].veh)
			DELETE_VEHICLE(wave_2_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(wave_2_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(wave_3_enemy_vehicle) - 1
		if does_entity_exist(wave_3_enemy_vehicle[i].veh)
			DELETE_VEHICLE(wave_3_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(wave_3_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(ambient_enemy_vehicle) - 1
		if does_entity_exist(ambient_enemy_vehicle[i].veh)
			DELETE_VEHICLE(ambient_enemy_vehicle[i].veh)
		endif 
		set_model_as_no_longer_needed(ambient_enemy_vehicle[i].model)
	endfor 
	
	
	for i = 0 to count_of(parked_cars) - 1
		if does_entity_exist(parked_cars[i])
			DELETE_VEHICLE(parked_cars[i])
		endif 
	endfor 
	set_model_as_no_longer_needed(phoenix)
	
	for i = 0 to count_of(explosive_barrels) - 1
		if does_entity_exist(explosive_barrels[i].obj)
			DELETE_OBJECT(explosive_barrels[i].obj)
		endif 
		set_model_as_no_longer_needed(explosive_barrels[i].model)
	endfor 
	
	for i = 0 to count_of(bins) - 1
		if does_entity_exist(bins[i].obj)
			DELETE_OBJECT(bins[i].obj)
		endif 
		set_model_as_no_longer_needed(bins[i].model)
	endfor 
	
	for i = 0 to count_of(c4) - 1
		if does_entity_exist(c4[i].obj)
			DELETE_OBJECT(c4[i].obj)
		endif 
		set_model_as_no_longer_needed(c4[i].model)
	endfor 
	
	if does_entity_exist(cover_box.obj)
		delete_object(cover_box.obj)
		set_model_as_no_longer_needed(cover_box.model)
	endif 
	
	REMOVE_ALL_RECORDINGS()
	
	CLEANUP_BAR()

endproc 

proc initialise_mission_variables()

	stop_mission_fail_checks = false
	wave_0_complete = false 
 	wave_2_complete = false 
	wave_3_complete = false
 	allow_buddy_ai_system = true 
// 	plant_bomb_dialogue_reminder = false
	ram_cutscene_finished = false
	//player_message_printed = false
	//buddy_message_printed = false
	//is_vehicle_exploded = false
	allow_ambient_enemy_blips_to_flash = false
	display_blips_as_friendly = false
	no_vehicle_recording_skip = false 
	fence_shot = false
	grenade_launcher_cutscene_playing = false
	stop_player_proofing = false
	use_main_cover_point = true
	refresh_the_task = false
	grenade_launcher_first_person_cam_active = false

	waypoint_node_pos = 0
	total_number_of_points = 0
	remaining_enemies = 0
	player_waypoint_status = 22
	buddy_waypoint_target_node = 0
	player_grenade_launcher_cutscene_status = 0
	uber_vehicle_status = 0
	get_to_meth_lab_status = 0
	intro_mocap_status = 0
	get_to_meth_lab_dialogue_system_status = 0
	mission_passed_mocap_status = 0
	get_to_meth_lab_audio_scene_system_status = 0
	wave_0_audio_scene_system_status = 0
	wave_3_audio_scene_system_status = 0

	
	#IF IS_DEBUG_BUILD
		get_to_meth_lab_skip_status = 0
		wave_0_system_skip_status = 0
		wave_3_system_skip_status = 0
	#endif 
	
	#IF IS_DEBUG_BUILD
		menu_stage_selector[0].sTxtLabel = "Outside Hicks Bar"                  
		menu_stage_selector[1].sTxtLabel = "Outside Meth Lab - CHI_1_MCS_1"  
		menu_stage_selector[2].sTxtLabel = "Start of Wave 0 shootout"  
		menu_stage_selector[3].sTxtLabel = "Wave 0 outside enemies"  
		menu_stage_selector[4].sTxtLabel = "Wave 2 after grenade launcher cutscene"  
		menu_stage_selector[5].sTxtLabel = "Wave 3 CHI_1_MCS_4"  
	#endif 
	
//	blood[0].decal_texture_id = DECAL_RSID_BLOOD_DIRECTIONAL
//	blood[0].pos = <<1984.59, 3050.58, 47.533>> //47.43
//	blood[0].direction = <<0.0, 0.0, -1.0>>
//	blood[0].side = normalise_vector(<<0.0, 1.0, 0.0>>) //heading 
//	blood[0].width = 0.2
//	blood[0].height = 0.2
//	blood[0].fAlpha = 1.0
//	blood[0].life = -1
//	blood[0].wash_amount = 0.3
//	blood[0].decal_removed = false
//	
//
//	blood[1].decal_texture_id = DECAL_RSID_BLOOD_SPLATTER 
//	blood[1].pos = <<1984.64, 3050.62, 47.423>> //47.32
//	blood[1].direction = <<0.0, -1.0, 0.0>>//<<0.0, -1.0, 0.0>>
//	blood[1].side = normalise_vector(<<1.0, 0.0, 0.0>>) //heading //<<1.0, 1.0, 0.0>>
//	blood[1].width = 0.2
//	blood[1].height = 0.2
//	blood[1].fAlpha = 1.0
//	blood[1].life = -1
//	blood[1].wash_amount = 0.3
//	blood[1].decal_removed = false

	buddy_cover_point_data[0].pos = vChefCoverPointBackStairs
	buddy_cover_point_data[0].cover_from_pos = <<1372.99622, 3617.98950, 33.89206>>
	buddy_cover_point_data[0].heading = 105.0

	buddy_cover_point_data[1].pos = vChefCoverPointBackStairs2
	buddy_cover_point_data[1].cover_from_pos = <<1372.99622, 3617.98950, 33.89206>>
	buddy_cover_point_data[1].heading = 105.0
	
	buddy_cover_point_data[2].pos = <<1406.77, 3615.64, 38.01>>
	buddy_cover_point_data[2].cover_from_pos = <<1420.11, 3617.82, 36.26>>
	buddy_cover_point_data[2].heading = 290.8229
	
	buddy_cover_point_data[3].pos = <<1408.31, 3614.82, 38.00>>
	buddy_cover_point_data[3].cover_from_pos = <<1420.11, 3617.82, 36.26>>
	buddy_cover_point_data[3].heading = 290.8229
	
	// wave 3 - liquor store
	buddy_cover_point_data[4].pos = vChefCoverPointLiquorStore
	buddy_cover_point_data[4].cover_from_pos = <<1394.83398, 3601.50195, 33.98091>>
	buddy_cover_point_data[4].heading = 197.4313
	buddy_cover_point_data[5].pos = vTrevorLiquorStartPos
	buddy_cover_point_data[5].cover_from_pos = <<1394.83398, 3601.50195, 33.98091>>
	buddy_cover_point_data[5].heading = 197.4313
			
	//wave 0
	wave_0_master_flow_system_status = 0
	wave_0_dialogue_system_status = 0
	wave_0_buddy_ai_system_status = 0
	ramming_gates_cutscene_status = 0
	wave_0_assisted_nodes_system_status = 0

	//wave 1 - REMOVED @SBA

	//wave2
	wave_2_master_flow_system_status = 0
	wave_2_dialogue_system_status = 0
	wave_2_buddy_ai_system_status = 0
	wave_2_assisted_nodes_system_status = 0

	//wave3
	wave_3_master_flow_system_status = 0
	wave_3_dialogue_system_status = 0
	wave_3_buddy_ai_system_status = 0
	wave_3_assisted_nodes_system_status = 0
	
	trevors_truck.pos = <<1960.0896, 3838.5664, 31.2088>>
	trevors_truck.heading = 300.0665
	trevors_truck.model = get_player_veh_model(char_trevor)

	cheng.pos = <<1995.13013, 3058.72192, 46.05180>> //<<1996.9528, 3058.3777, 46.0490>>
	cheng.heading = 0.0//179.1673
	cheng.model = get_npc_ped_model(char_cheng)    //IG_TaoCheng
	cheng.weapon = WEAPONTYPE_INVALID // WEAPONTYPE_pistol
	cheng.health = 500
	cheng.name = "cheng"

	translator.pos = <<2000.2826, 3056.5327, 46.0492>> 
	translator.heading = 323.0890//280.00
	translator.model = IG_TaosTranslator
	translator.weapon = WEAPONTYPE_INVALID // WEAPONTYPE_PUMPSHOTGUN 
	translator.health = 500
	translator.name = "translator"

	buddy.pos = <<1388.3929, 3607.6262, 37.9419>> 
	buddy.heading = 114.5685
	buddy.model = GET_NPC_PED_MODEL(CHAR_CHEF) // @SBA - ensure we get correct model //A_M_Y_GenStreet_01 ig_cook
	buddy.weapon = WEAPONTYPE_carbinerifle
	buddy.health = 300 // @SBA - making the Chef more vulnerable.  OLD = 1000

	//WAVE 0
	iWave0SetupStage = 0

	wave_0_enemy[0].model = GoonModel2//A_M_Y_BeachVesp_01 //U_M_M_CHEMSEC_01
	wave_0_enemy[0].pos = <<1353.6407, 3603.9707, 33.9350>>
	wave_0_enemy[0].heading = 215.4769
	wave_0_enemy[0].run_to_pos = <<1362.16003, 3590.75684, 33.93283>>
	wave_0_enemy[0].health = 200
	wave_0_enemy[0].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_0_enemy[0].accuracy = 2
	wave_0_enemy[0].damaged_by_player = true
	wave_0_enemy[0].name = "wave_0 0" 

	wave_0_enemy[1].model = GoonModel1
	wave_0_enemy[1].pos = <<1349.3075, 3607.7500, 33.9041>>
	wave_0_enemy[1].heading = 229.00
	wave_0_enemy[1].run_to_pos = <<1364.8840, 3600.9895, 33.8943>>
	wave_0_enemy[1].health = 200
	wave_0_enemy[1].weapon = weapontype_pistol
	wave_0_enemy[1].accuracy = 2
	wave_0_enemy[1].name = "wave_0 1" 
	
	wave_0_enemy[2].model = GoonModel2
	wave_0_enemy[2].run_to_pos = <<1363.7, 3585.7, 34.3>>//<<1367.6045, 3587.3115, 33.9385>>//<<1363.6156, 3584.8359, 33.9350>>
	wave_0_enemy[2].health = 200
	wave_0_enemy[2].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_0_enemy[2].accuracy = 2
	wave_0_enemy[2].damaged_by_player = true
	wave_0_enemy[2].name = "wave_0 2" 
	
	wave_0_enemy[3].model = GoonModel1
	wave_0_enemy[3].run_to_pos = <<1374.11609, 3588.79761, 33.92868>> 
	wave_0_enemy[3].health = 200
	wave_0_enemy[3].weapon = weapontype_pistol
	wave_0_enemy[3].accuracy = 2
	wave_0_enemy[3].name = "wave_0 3" 
	
	wave_0_enemy[4].model = GoonModel2
	wave_0_enemy[4].run_to_pos = <<1369.97827, 3596.78906, 33.89490>>
	wave_0_enemy[4].health = 200
	wave_0_enemy[4].weapon = weapontype_pistol
	wave_0_enemy[4].accuracy = 2
	wave_0_enemy[4].damaged_by_player = true
	wave_0_enemy[4].name = "wave_0 4" 
	
	wave_0_enemy[5].model = GoonModel1
	wave_0_enemy[5].run_to_pos = <<1375.2, 3602.02, 34.0>> // doors = <<1394.700, 3598.500, 35.1>> // @SBA - want this guy to run to doors. OLD = <<1372.86182, 3594.96143, 33.85409>>//<<1370.9216, 3596.7520, 33.8951>>
	wave_0_enemy[5].health = 200
	wave_0_enemy[5].weapon = WEAPONTYPE_PUMPSHOTGUN
	wave_0_enemy[5].accuracy = 2
	wave_0_enemy[5].name = "wave_0 5" 
	
	wave_0_enemy[6].model = GoonModel2
	wave_0_enemy[6].run_to_pos = <<1373.23328, 3589.86597, 34.06313>>  
	wave_0_enemy[6].health = 200
	wave_0_enemy[6].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_0_enemy[6].accuracy = 2
	wave_0_enemy[6].damaged_by_player = true
	wave_0_enemy[6].name = "wave_0 6" 

	wave_0_enemy[7].model = GoonModel1
	wave_0_enemy[7].run_to_pos = <<1367.63049, 3601.10352, 33.90302>>//<<1366.8850, 3596.2249, 33.8948>>
	wave_0_enemy[7].health = 200
	wave_0_enemy[7].weapon = weapontype_pistol
	wave_0_enemy[7].accuracy = 2
	wave_0_enemy[7].name = "wave_0 7" 
	
	wave_0_enemy[8].model = GoonModel2
	wave_0_enemy[8].run_to_pos = <<1365.79187, 3601.94165, 33.90197>>
	wave_0_enemy[8].health = 200
	wave_0_enemy[8].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_0_enemy[8].accuracy = 2
	wave_0_enemy[8].name = "wave_0 8" 

//	wave_0_enemy[9].pos = <<1351.1438, 3609.5547, 33.8506>>
//	wave_0_enemy[9].model = GoonModel1
//	wave_0_enemy[9].run_to_pos = <<1367.3512, 3595.8589, 33.8949>>
//	wave_0_enemy[9].health = 200
//	wave_0_enemy[9].weapon = weapontype_pistol
//	wave_0_enemy[9].accuracy = 2
//	wave_0_enemy[9].damaged_by_player = true
//	wave_0_enemy[9].name = "wave_0 9" 


	wave_0_enemy_vehicle[0].pos = <<1274.0476, 3643.2988, 32.3214>>
	wave_0_enemy_vehicle[0].heading = 287.7983
	wave_0_enemy_vehicle[0].model = dubsta2
	wave_0_enemy_vehicle[0].recording_number = 001
	
	wave_0_enemy_vehicle[1].pos = <<1313.1727, 3658.6948, 32.0836>>
	wave_0_enemy_vehicle[1].heading = 181.0355
	wave_0_enemy_vehicle[1].model = dubsta2
	wave_0_enemy_vehicle[1].recording_number = 002
	
	wave_0_enemy_vehicle[2].pos = <<930.5292, 3564.9253, 32.8149>>
	wave_0_enemy_vehicle[2].heading = 189.5711
	wave_0_enemy_vehicle[2].model = sadler
	wave_0_enemy_vehicle[2].recording_number = 003

	//WAVE 0 - INSIDE ENEMIES
	wave_0_inside_enemy[0].model = GoonModel2
	wave_0_inside_enemy[0].run_to_pos = <<1385.6793, 3619.9067, 37.9258>> 
	wave_0_inside_enemy[0].health = 200
	wave_0_inside_enemy[0].weapon = weapontype_pistol 
	wave_0_inside_enemy[0].accuracy = 2
	//wave_0_inside_enemy[0].damaged_by_player = true
	wave_0_inside_enemy[0].name = "wave_0 I 0" 
	
	wave_0_inside_enemy[1].model = GoonModel1
	wave_0_inside_enemy[1].run_to_pos = <<1392.2399, 3617.5964, 37.9258>> 
	wave_0_inside_enemy[1].health = 200
	wave_0_inside_enemy[1].weapon = weapontype_smg 
	wave_0_inside_enemy[1].accuracy = 2
	wave_0_inside_enemy[1].name = "wave_0 I 1" 
	
	wave_0_inside_enemy[2].model = GoonModel2
	wave_0_inside_enemy[2].run_to_pos = <<1385.6793, 3619.9067, 37.9258>> 
	wave_0_inside_enemy[2].health = 200
	wave_0_inside_enemy[2].weapon = weapontype_pistol
	wave_0_inside_enemy[2].accuracy = 2
	//wave_0_inside_enemy[2].damaged_by_player = true
	wave_0_inside_enemy[2].name = "wave_0 I 2" 
	
	wave_0_inside_enemy[3].model = GoonModel1
	wave_0_inside_enemy[3].run_to_pos = <<1392.4437, 3617.7678, 37.9258>> 
	wave_0_inside_enemy[3].health = 200
	wave_0_inside_enemy[3].weapon = WEAPONTYPE_PUMPSHOTGUN
	wave_0_enemy[3].accuracy = 2
	wave_0_inside_enemy[3].name = "wave_0 I 3" 


	//WAVE 1 - REMOVED @SBA

	//WAVE 2
	
	//ENEMIES ON FOOT

	wave_2_enemy[0].pos = <<1478.80, 3653.09, 33.79>>//<<1473.9318, 3632.6509, 33.8312>>
	wave_2_enemy[0].heading = 109.3158
	wave_2_enemy[0].model = GoonModel2
	wave_2_enemy[0].run_to_pos = <<1433.0659, 3622.7234, 34.7774>>
	wave_2_enemy[0].health = 200
	wave_2_enemy[0].weapon = weapontype_pistol
	wave_2_enemy[0].accuracy = 2
	wave_2_enemy[0].damaged_by_player = true
	wave_2_enemy[0].name = "wave_2 0" 
	
	wave_2_enemy[1].pos = <<1497.8187, 3597.0610, 34.4572>>//<<1472.5101, 3635.0879, 33.7732>>
	wave_2_enemy[1].heading = 61.9718 //129.7310
	wave_2_enemy[1].model = GoonModel1
	wave_2_enemy[1].run_to_pos = <<1434.63062, 3610.20190, 33.86340>>
	wave_2_enemy[1].health = 200
	wave_2_enemy[1].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[1].accuracy = 2
	wave_2_enemy[1].name = "wave_2 1"
	
	wave_2_enemy[2].pos = <<1500.47, 3642.82, 33.87>>
	wave_2_enemy[2].heading = 110.4908//119.1874
	wave_2_enemy[2].model = GoonModel2
	wave_2_enemy[2].run_to_pos = <<1424.91467, 3626.70874, 33.84133>>
	wave_2_enemy[2].health = 200
	wave_2_enemy[2].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_2_enemy[2].accuracy = 2
	wave_2_enemy[2].damaged_by_player = true
	wave_2_enemy[2].name = "wave_2 2" 
	
	wave_2_enemy[3].pos = <<1481.33, 3655.46, 33.87>>//<<1477.9222, 3637.6001, 33.8724>>
	wave_2_enemy[3].heading = 120.00
	wave_2_enemy[3].model = GoonModel1
	wave_2_enemy[3].run_to_pos = <<1446.2058, 3614.8169, 33.8532>>
	wave_2_enemy[3].health = 200
	wave_2_enemy[3].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[3].accuracy = 2
	wave_2_enemy[3].name = "wave_2 3"
	
	wave_2_enemy[4].heading = 127.3335
	wave_2_enemy[4].model = GoonModel2
	wave_2_enemy[4].run_to_pos = <<1420.5912, 3625.5984, 33.8292>>// @SBA - run to stairs pos = <<1404.1445, 3628.6165, 33.8927>>
	wave_2_enemy[4].health = 200
	wave_2_enemy[4].weapon = weapontype_pistol 
	wave_2_enemy[4].accuracy = 2
	wave_2_enemy[4].damaged_by_player = true
	wave_2_enemy[4].name = "wave_2 4" 
	
	wave_2_enemy[5].heading = 121.0459
	wave_2_enemy[5].model = GoonModel1
	wave_2_enemy[5].run_to_pos = <<1433.7379, 3613.1348, 33.9168>>// @SBA - run to stairs pos = <<1404.1445, 3628.6165, 33.8927>>
	wave_2_enemy[5].health = 200
	wave_2_enemy[5].weapon = weapontype_pistol
	wave_2_enemy[5].accuracy = 2
	wave_2_enemy[5].damaged_by_player = true		// @SBA - keep Chef from killing this guy before he can rush
	wave_2_enemy[5].name = "wave_2 5" 

	
	//ENEMIES IN CARS
	wave_2_enemy[6].model = GoonModel2
	wave_2_enemy[6].run_to_pos = <<1425.9056, 3619.8916, 33.9196>>
	wave_2_enemy[6].health = 200
	wave_2_enemy[6].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[6].accuracy = 2
	wave_2_enemy[6].name = "wave_2 6" 
	
	wave_2_enemy[7].model = GoonModel1
	wave_2_enemy[7].run_to_pos = <<1421.1597, 3623.3892, 33.8677>>
	wave_2_enemy[7].health = 200
	wave_2_enemy[7].weapon = weapontype_pistol
	wave_2_enemy[7].accuracy = 2
	wave_2_enemy[7].damaged_by_player = true
	wave_2_enemy[7].name = "wave_2 7" 
	
	wave_2_enemy[8].model = GoonModel2
	wave_2_enemy[8].run_to_pos = <<1422.8757, 3617.6958, 33.9261>>
	wave_2_enemy[8].health = 200
	wave_2_enemy[8].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_2_enemy[8].accuracy = 2
	wave_2_enemy[8].name = "wave_2 8" 
	
	wave_2_enemy[9].model = GoonModel1
	wave_2_enemy[9].run_to_pos = <<1427.4917, 3615.0203, 33.9644>>
	wave_2_enemy[9].health = 200
	wave_2_enemy[9].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[9].accuracy = 2
	wave_2_enemy[9].name = "wave_2 9" 
	
	wave_2_enemy[10].model = GoonModel2
	wave_2_enemy[10].run_to_pos = <<1431.5154, 3614.6177, 33.9824>>
	wave_2_enemy[10].health = 200
	wave_2_enemy[10].weapon = weapontype_pistol
	wave_2_enemy[10].accuracy = 2
	wave_2_enemy[10].damaged_by_player = true
	wave_2_enemy[10].name = "wave_2 10" 
	
	wave_2_enemy[11].model = GoonModel1
	wave_2_enemy[11].run_to_pos = <<1429.3192, 3610.2139, 33.8893>>
	wave_2_enemy[11].health = 200
	wave_2_enemy[11].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_2_enemy[11].accuracy = 2
	wave_2_enemy[11].name = "wave_2 11" 
	
	
	wave_2_enemy[12].model = GoonModel2
	wave_2_enemy[12].run_to_pos = <<1425.9056, 3619.8916, 33.9196>>
	wave_2_enemy[12].health = 200
	wave_2_enemy[12].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[12].accuracy = 2
	wave_2_enemy[12].name = "wave_2 12" 
	
	wave_2_enemy[13].model = GoonModel1
	wave_2_enemy[13].run_to_pos = <<1421.1597, 3623.3892, 33.8677>>
	wave_2_enemy[13].health = 200
	wave_2_enemy[13].weapon = weapontype_pistol
	wave_2_enemy[13].accuracy = 2
	wave_2_enemy[13].damaged_by_player = true
	wave_2_enemy[13].name = "wave_2 13" 
	
	wave_2_enemy[14].model = GoonModel2
	wave_2_enemy[14].run_to_pos = <<1422.8757, 3617.6958, 33.9261>>
	wave_2_enemy[14].health = 200
	wave_2_enemy[14].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_2_enemy[14].accuracy = 2
	wave_2_enemy[14].name = "wave_2 14" 
	
	wave_2_enemy[15].model = GoonModel1
	wave_2_enemy[15].run_to_pos = <<1427.4917, 3615.0203, 33.9644>>
	wave_2_enemy[15].health = 200
	wave_2_enemy[15].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[15].accuracy = 2
	wave_2_enemy[15].name = "wave_2 15" 
	
	wave_2_enemy[16].model = GoonModel2
	wave_2_enemy[16].run_to_pos = <<1431.5154, 3614.6177, 33.9824>>
	wave_2_enemy[16].health = 200
	wave_2_enemy[16].weapon = weapontype_pistol
	wave_2_enemy[16].accuracy = 2
	wave_2_enemy[16].name = "wave_2 16" 
	
	wave_2_enemy[17].model = GoonModel1
	wave_2_enemy[17].run_to_pos = <<1429.3192, 3610.2139, 33.8893>>
	wave_2_enemy[17].health = 200
	wave_2_enemy[17].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_2_enemy[17].accuracy = 2
	wave_2_enemy[17].damaged_by_player = true
	wave_2_enemy[17].name = "wave_2 17" 
	
	
	//more running enemies
	wave_2_enemy[18].pos = <<1499.98, 3646.73, 33.78>>
	wave_2_enemy[18].heading = 117.6692
	wave_2_enemy[18].model = GoonModel2
	wave_2_enemy[18].run_to_pos = <<1420.90442, 3623.09033, 33.86813>> 
	wave_2_enemy[18].health = 200
	wave_2_enemy[18].weapon = WEAPONTYPE_PUMPSHOTGUN 
	wave_2_enemy[18].accuracy = 2
	wave_2_enemy[18].name = "wave_2 18" 
	
	wave_2_enemy[19].pos = <<1492.34, 3667.31, 33.71>>
	wave_2_enemy[19].heading = 117.3643
	wave_2_enemy[19].model = GoonModel1
	wave_2_enemy[19].run_to_pos = <<1444.5588, 3622.1567, 33.7832>>
	wave_2_enemy[19].health = 200
	wave_2_enemy[19].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_2_enemy[19].accuracy = 2
	wave_2_enemy[19].damaged_by_player = true
	wave_2_enemy[19].name = "wave_2 19" 
	

	wave_2_enemy_vehicle[0].pos = <<1183.3048, 3559.6870, 34.4826>>
	wave_2_enemy_vehicle[0].heading = 274.0203
	wave_2_enemy_vehicle[0].model = phoenix
	wave_2_enemy_vehicle[0].recording_number = 020
	
	wave_2_enemy_vehicle[1].pos = <<1633.7235, 3618.2314, 35.3348>>
	wave_2_enemy_vehicle[1].heading = 274.0203
	wave_2_enemy_vehicle[1].model = sadler 
	wave_2_enemy_vehicle[1].recording_number = 009
	
	wave_2_enemy_vehicle[2].pos = <<1564.2393, 3729.5574, 34.0375>>
	wave_2_enemy_vehicle[2].heading = 31.0876
	wave_2_enemy_vehicle[2].model = dubsta2 
	wave_2_enemy_vehicle[2].recording_number = 021

	wave_2_enemy_vehicle[3].pos = <<1586.9836, 3695.9495, 33.9163>>
	wave_2_enemy_vehicle[3].heading = 274.0203
	wave_2_enemy_vehicle[3].model = pcj
	wave_2_enemy_vehicle[3].recording_number = 017
	
	wave_2_enemy_vehicle[4].pos = <<1630.4724, 3622.4307, 34.9559 >>
	wave_2_enemy_vehicle[4].heading = 274.0203
	wave_2_enemy_vehicle[4].model = pcj
	wave_2_enemy_vehicle[4].recording_number = 018
	
	wave_2_enemy_vehicle[5].pos = <<1570.2612, 3720.8474, 34.3386>>
	wave_2_enemy_vehicle[5].heading = 274.0203
	wave_2_enemy_vehicle[5].model = phoenix
	wave_2_enemy_vehicle[5].recording_number = 019
	
	wave_2_enemy_vehicle[6].pos = <<1510.2731, 3555.9585, 34.3625>>
	wave_2_enemy_vehicle[6].heading = 106.7575
	wave_2_enemy_vehicle[6].model = phoenix
	wave_2_enemy_vehicle[6].recording_number = 010

	wave_2_enemy_vehicle[7].pos = <<1191.8964, 3532.9456, 34.1179>>
	wave_2_enemy_vehicle[7].heading = 274.0203
	wave_2_enemy_vehicle[7].model = phoenix
	wave_2_enemy_vehicle[7].recording_number = 008
	

	//WAVE 3 - @SBA - they all can be damaged by Chef
	wave_3_enemy[0].model = GoonModel2
	wave_3_enemy[0].run_to_pos = <<1392.9648, 3598.3315, 34.0295>>
	wave_3_enemy[0].health = 200
	wave_3_enemy[0].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_3_enemy[0].accuracy = 2
	//wave_3_enemy[0].damaged_by_player = true
	wave_3_enemy[0].name = "wave3_ortega" 
	
	wave_3_enemy[1].model = GoonModel1
	wave_3_enemy[1].run_to_pos = <<1395.8434, 3599.8474, 34.0031>>
	wave_3_enemy[1].health = 200
	wave_3_enemy[1].weapon = weapontype_smg
	wave_3_enemy[1].accuracy = 2
	wave_3_enemy[1].name = "wave_3 1" 
	
	wave_3_enemy[2].model = GoonModel2
	wave_3_enemy[2].run_to_pos = <<1394.6, 3597.5, 35.0>> // @SBA - setting a position nearer the front door.  OLD = <<1395.7645, 3594.6904, 33.9130>>
	wave_3_enemy[2].health = 200
	wave_3_enemy[2].weapon = WEAPONTYPE_ASSAULTRIFLE 
	wave_3_enemy[2].accuracy = 2
	//wave_3_enemy[2].damaged_by_player = true
	wave_3_enemy[2].name = "wave_3 2" 

	wave_3_enemy[3].model = GoonModel1
	wave_3_enemy[3].run_to_pos = <<1398.4982, 3593.2292, 33.8867>>
	wave_3_enemy[3].health = 200
	wave_3_enemy[3].weapon = weapontype_pistol
	wave_3_enemy[3].accuracy = 2
	wave_3_enemy[3].name = "wave_3 3" 
	
	ambient_enemy[0].model = GoonModel1
	ambient_enemy[0].run_to_pos = <<1397.9803, 3592.6746, 33.7258>>
	ambient_enemy[0].health = 200
	ambient_enemy[0].weapon = weapontype_pistol
	ambient_enemy[0].accuracy = 5
	ambient_enemy[0].name = "ambient 0" 
	
	ambient_enemy[1].model = GoonModel1
	ambient_enemy[1].run_to_pos = <<1386.96, 3575.22, 33.97>>
	ambient_enemy[1].health = 200
	ambient_enemy[1].weapon = weapontype_pistol
	ambient_enemy[1].accuracy = 5
	ambient_enemy[1].name = "ambient 1" 
	
	ambient_enemy[2].model = GoonModel1
	ambient_enemy[2].run_to_pos = <<1387.1794, 3585.0732, 33.8415>>
	ambient_enemy[2].health = 200
	ambient_enemy[2].weapon = weapontype_pistol
	ambient_enemy[2].accuracy = 5
	ambient_enemy[2].name = "ambient 2" 
	
	ambient_enemy[3].model = GoonModel1
	ambient_enemy[3].run_to_pos = <<1377.14, 3580.02, 34.02>>
	ambient_enemy[3].health = 200
	ambient_enemy[3].weapon = weapontype_pistol
	ambient_enemy[3].accuracy = 5
	ambient_enemy[3].name = "ambient 3" 
	
	wave_3_enemy_vehicle[0].pos = <<1184.7596, 3558.3193, 34.2744>>
	wave_3_enemy_vehicle[0].heading = 189.00
	wave_3_enemy_vehicle[0].model = dubsta2
	wave_3_enemy_vehicle[0].recording_number = 007
	
	ambient_enemy_vehicle[0].pos = <<1169.8923, 3537.2869, 34.0337>>
	ambient_enemy_vehicle[0].heading = 269.5452
	ambient_enemy_vehicle[0].model = phoenix
	ambient_enemy_vehicle[0].recording_number = 011

	ambient_enemy_vehicle[1].pos = <<1128.6812, 3539.9797, 33.7354>>
	ambient_enemy_vehicle[1].heading = 262.7058
	ambient_enemy_vehicle[1].model = phoenix
	ambient_enemy_vehicle[1].recording_number = 012
	
	
	c4[0].model = GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB)
	c4[1].model = GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB)
	c4[2].model = GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB)
	
//	c4_nodes[0].pos = <<1378.857, 3601.824, 34.900>>
//	c4_nodes[0].heading = 180.00
//	c4_nodes[0].planted = false
//	
//	c4_nodes[1].pos = <<1362.4718, 3591.5798, 33.9255>>
//	c4_nodes[1].heading = 180.00
//	c4_nodes[1].planted = false
//
//	c4_nodes[2].pos = <<1373.4763, 3602.8335, 33.8946>>
//	c4_nodes[2].heading = 180.00
//	c4_nodes[2].planted = false
//
//	c4_nodes[3].pos = <<1374.6613, 3585.6296, 33.929>>
//	c4_nodes[3].heading = 180.00
//	c4_nodes[3].planted = false
//
//	c4_nodes[4].pos = <<1381.0747, 3597.1887, 33.8747>>
//	c4_nodes[4].heading = 180.00
//	c4_nodes[4].planted = false
//	
//	c4_nodes[5].pos = <<1381.2036, 3590.4878, 33.8771>>
//	c4_nodes[5].heading = 180.00
//	c4_nodes[5].planted = false
//	
//	c4_nodes[6].pos = <<1375.9454, 3588.1265, 33.8806>>
//	c4_nodes[6].heading = 180.00
//	c4_nodes[6].planted = false
//
//	c4_nodes[7].pos = <<1370.5957, 3590.5942, 33.9001>>
//	c4_nodes[7].heading = 180.00
//	c4_nodes[7].planted = false
//	
//	c4_nodes[8].pos = <<1362.4718, 3591.5798, 33.9255>>
//	c4_nodes[8].heading = 180.00
//	c4_nodes[8].planted = false
//	
//	c4_nodes[9].pos = <<1359.7563, 3598.7205, 33.8910>>
//	c4_nodes[9].heading = 180.00
//	c4_nodes[9].planted = false
	
	
	bins[0].model = PROP_CS_DUMPSTER_01A
	bins[0].pos = <<1365.95, 3601.94, 34.43>>
	bins[0].rot = <<0.0, 0.0, 120.00>>

//	bins[1].model = PROP_CS_DUMPSTER_01A
//	bins[1].pos = <<1367.00, 3593.720, 34.430>> 
//	bins[1].rot = <<0.0, 0.0, 80.0>>

	bins[1].model = PROP_CS_DUMPSTER_01A
	bins[1].pos = <<1374.370, 3589.740, 34.430>>
	bins[1].rot = <<0.0, 0.0, 122.000>>
	
	bins[2].model = PROP_CS_DUMPSTER_01A
	bins[2].pos = <<1424.570, 3625.710, 34.370>>
	bins[2].rot = <<0.0, 0.0, 305.00>>
	
	
	explosive_barrels[0].pos = <<1362.49011, 3603.48438, 34.270>>
	explosive_barrels[0].rot = <<0.0, 0.0, 0.0>>
	explosive_barrels[0].model = PROP_GASCYL_01A

	explosive_barrels[1].pos = <<1368.92529, 3606.08716, 34.3>>
	explosive_barrels[1].rot = <<0.0, 0.0, 0.0>>
	explosive_barrels[1].model = PROP_GASCYL_01A
	
	explosive_barrels[2].pos = <<1361.08, 3595.01, 34.3>>
	explosive_barrels[2].rot = <<0.0, 0.0, 0.0>>
	explosive_barrels[2].model = PROP_GASCYL_01A
	
	explosive_barrels[3].pos = <<1419.650, 3627.000, 34.180>>
	explosive_barrels[3].rot = <<0.0, 0.0, 0.0>>
	explosive_barrels[3].model = PROP_GASCYL_01A
	
	cover_box.model = Prop_boxpile_02b//Prop_cratepile_07a
	cover_box.pos = <<1409.990, 3623.510, 34.020>>
	cover_box.rot = <<0.0, 0.0, 70.00>>
	
//	explosive_barrels[3].pos = <<1421.310, 3627.840, 34.250>>
//	explosive_barrels[3].rot = <<0.0, 0.0, 0.0>>
//	explosive_barrels[3].model = Prop_Gas_Tank_01a
	
	

	for i = 0 to count_of(wave_0_enemy) - 1
		wave_0_enemy[i].created = false
		wave_0_enemy[i].bRushInside = FALSE		// @SBA - init this bool
		wave_0_enemy[i].been_killed = FALSE
		wave_0_status[i] = initial_attack_phase
	endfor 
	
	for i = 0 to count_of(wave_0_inside_enemy_status) - 1
		wave_0_inside_enemy[i].created = false
		wave_0_inside_enemy[i].bRushInside = FALSE		// @SBA - init this bool
		wave_0_inside_enemy[i].been_killed = FALSE
		wave_0_inside_enemy_status[i] = 0
	endfor 
	
	for i = 0 to count_of(wave_2_enemy) - 1
		wave_2_enemy[i].created = false
		wave_2_enemy[i].bRushInside = FALSE		// @SBA - init this bool
		wave_2_enemy[i].been_killed = FALSE
		wave_2_status[i] = initial_attack_phase
	endfor 
	
	for i = 0 to count_of(wave_3_enemy) - 1
		wave_3_enemy[i].created = false
		wave_3_enemy[i].bRushInside = FALSE		// @SBA - init this bool
		wave_3_enemy[i].been_killed = FALSE
		wave_3_status[i] = initial_attack_phase
	endfor 
	
	// @SBA - Making sure this is initialized
	for i = 0 to count_of(ambient_enemy) - 1
		ambient_enemy[i].created = false
		ambient_enemy[i].bRushInside = FALSE		// @SBA - init this bool
		ambient_enemy[i].been_killed = FALSE
		ambient_enemy_status[i] = initial_attack_phase
	endfor 
	
	for i = 0 to count_of(remaining_enemy) - 1
		remaining_enemy_status[0] = 0
	endfor 
	
	for i = 0 to count_of(wave_0_enemy_vehicle) - 1
		wave_0_enemy_vehicle[i].been_destroyed = false
	endfor 
	for i = 0 to count_of(wave_2_enemy_vehicle) - 1
		wave_2_enemy_vehicle[i].been_destroyed = false
	endfor 	
	for i = 0 to count_of(wave_3_enemy_vehicle) - 1
		wave_3_enemy_vehicle[i].been_destroyed = false
	endfor 	
	for i = 0 to count_of(ambient_enemy_vehicle) - 1
		ambient_enemy_vehicle[i].been_destroyed = false
	endfor 
	
	// @SBA - initializing trigger volumes
	tbArrivalCutResolveVol = CREATE_TRIGGER_BOX(<<1380.224609,3593.346436,33.850239>>, <<1414.539917,3604.536865,35.898521>>, 13.000000)
	tbFrontDoorVolume = CREATE_TRIGGER_BOX((<<1394.211304,3599.770264,33.519615>>), (<<1395.355469,3596.857910,35.675140>>), 4.0)
	// This is an area behind the Chef: tbUpstairsRushVolume = CREATE_TRIGGER_BOX((<<1395.018799,3604.429932,37.441910>>), (<<1391.960327,3603.366455,39.691910>> ), 6.0)
	tbUpstairsRushVolume = CREATE_TRIGGER_BOX((<<1391.724731,3601.888916,37.95>>), (<<1399.914551,3605.850098,39.691910>>), 4.75)
	tbUpstairsCloserRushVolume = CREATE_TRIGGER_BOX((<<1395.802002,3603.719238,37.941910>>), <<1391.546997,3602.099609,39.691910>>, 3.50)
	tbTopBackStairsVolume = CREATE_TRIGGER_BOX((<<1387.596436,3621.771484,37.832703>>), (<<1389.516235,3616.653320,39.675831>> ), 8.5)
	tbBackStairsAlleyVolume = CREATE_TRIGGER_BOX((<<1387.938843,3626.496338,33.891834>>), (<<1379.320801,3623.467285,35.641552>>), 3.50)
	tbOnBackStairsVolume = CREATE_TRIGGER_BOX((<<1388.321, 3622.040, 36.061>>), (<<1389.153, 3619.789, 41.061>>), 6.0)
	tbFullBackStairsVolume = CREATE_TRIGGER_BOX(<<1388.212646,3622.047607,34.011982>>, <<1390.500977,3615.757324,39.675831>>, 13.500000)
	tbOutsideMainGatesVolume = CREATE_TRIGGER_BOX((<<1377.952759,3608.572510,33.864216>>), (<<1384.160156,3591.729004,37.661396>>), 15.0)
	tbBackYardVolume = CREATE_TRIGGER_BOX((<<1420.578491,3615.409424,33.436520>>), (<<1435.515747,3624.327637,35.786892>>), 29.0)
	tbBackRoofVolume = CREATE_TRIGGER_BOX((<<1399.85681, 3610.41724,37.905733>>), (<<1409.257202,3613.888916,39.666866>>), 16.0)
	tbBackFullRoofVolume = CREATE_TRIGGER_BOX((<<1398.830200,3625.614502,38.049397>>), (<<1407.572876,3602.470703,43.135361>>), 14.75)
	tbBackUpstairsRoomVolume = CREATE_TRIGGER_BOX((<<1394.697021,3616.749023,38.10>>), (<<1399.886963,3602.334229,39.691910>>), 7.0)
	tbBackGateVolume = CREATE_TRIGGER_BOX((<<1405.966675,3629.890869,33.642715>>), (<<1397.764038,3626.875977,35.645195>>), 4.5)
	tbLiquorStoreVolume = CREATE_TRIGGER_BOX((<<1398.361328,3607.276123,33.680907>>), (<<1388.642090,3603.685547,35.730907>>), 8.0)
	tbIceBoxInnerVol = CREATE_TRIGGER_BOX(<<1389.272461,3599.501709,33.895496>>, <<1382.610962,3597.052734,37.864140>>, 7.500000)
	// @SBA smaller version tbIceBoxInnerVol = CREATE_TRIGGER_BOX(<<1388.118286,3599.011475,33.895119>>, <<1384.076221,3597.669922,37.895111>>, 5.00000)
	tbIceBoxOuterVol = CREATE_TRIGGER_BOX(<<1392.669678,3600.705566,33.980907>>, <<1378.677124,3595.678711,37.389488>>, 17.000000)
	tbIceBoxCameraVol = CREATE_TRIGGER_BOX(<<1390.041626,3600.490967,33.980907>>, <<1380.214233,3596.944092,37.389095>>, 14.500000)
	
	//@SBA - initializing additional variables
	bBarPatronsCreated = FALSE
	bIsBarCleanedUp = FALSE
	bClearCarGen = FALSE
	bPlayerInVehicle = FALSE
	bTrevorsVehicleIsOkay = FALSE
	bDanceAnimDictUnloaded = FALSE
	bLoadedLastShootoutStuff = FALSE
	bPlayerInOpeningCover = FALSE
	bStartedInitialWave = FALSE
	bInitialWavePropsMade = FALSE
	bSetTaxiDropoff = FALSE
	bChefRunsToBackStairs = FALSE
	bPlayGunFire = FALSE
	iWaitTimeBeforeRush = 0
	iWaveRushersSoFar = 0
	iEnemyGrenLaunchDialogue = 0
	bPickupAmmoSet = FALSE
	bSkipGrenLaunchCutscene = FALSE
	bOutroShitSkip = FALSE
	bStartTranslatorEmergeBox = FALSE
	
	bChefComeOn = FALSE
	bTellChefGetInside = FALSE
	bSetIceBoxBlip = FALSE
	
	CANCEL_TIMER(tmrChefRunsDelay)
	ArivialLeadInState = ALI_HALT_VEHICLE
	
	// @SBA - Reset label
	SET_LABEL_AS_TRIGGERED("attack_int", FALSE)
		
endproc 

proc load_text_and_dialogue()
	
	register_script_with_audio()

	add_ped_for_dialogue(scripted_speech[0], 2, player_ped_id(), "trevor")
	request_additional_text("MethAud", mission_dialogue_text_slot)
	request_additional_text("METH", mission_text_slot)
	
	while not has_additional_text_loaded(mission_text_slot)
	or not has_additional_text_loaded(mission_dialogue_text_slot)
		wait(0)	
	endwhile

endproc 

proc get_to_meth_lab_dialogue_system()
	
	switch get_to_meth_lab_dialogue_system_status 
	
		case 0

			if create_conversation(scripted_speech[0], "methaud", "GETIN", conv_priority_medium)
				original_time = get_game_timer()
				get_to_meth_lab_dialogue_system_status++
			endif 

		break 
		
		case 1

			if not is_any_text_being_displayed(locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				
//				printstring("no text being displayed")
//				printnl()
				if dialogue_monitoring_system(locates_data.LocationBlip)
					if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
					
						// @SBA - swapping in-car conversatiion with phone conversation per feedback
						if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech[0], char_chef, "MethAud", "CH1CAL", CONV_PRIORITY_high)
							
							//Ross W - Recording phone call
							
							REPLAY_RECORD_BACK_FOR_TIME(0.5, 5.0)
							
							// @SBA - init timer for following conversation
							iGetToMethLabDialogTimer = 0
							get_to_meth_lab_dialogue_system_status++
						endif 
						
					else
					
						if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech[0], char_chef, "MethAud", "CH1CAL2", CONV_PRIORITY_high) 
							// @SBA - init timer for following conversation
							iGetToMethLabDialogTimer = 0
							get_to_meth_lab_dialogue_system_status++
						endif 
					
					endif

				endif 
				
			else 
			
//				printstring("text being displayed")
//				printnl()
			
			endif 

		break 
		
		case 2
			if dialogue_monitoring_system(locates_data.LocationBlip)
				// @SBA - setting a timer for following conversation
				IF iGetToMethLabDialogTimer = 0
					iGetToMethLabDialogTimer = get_game_timer()
				ENDIF
				// @SBA - start next conversation once timer is up and if player is close to translator
				IF lk_timer(iGetToMethLabDialogTimer, 2000)
					IF distance_from_player_to_ped(translator.ped) < 8.0
						// @SBA - swapping in-car conversation with phone conversation per feedback
						//PRINTLN("CONVERSATION STARTING")
						if create_conversation(scripted_speech[0], "methaud", "BANT1AV1", conv_priority_medium)
							//script_assert("dialogue")
							get_to_meth_lab_dialogue_system_status++
						endif
					ENDIF
				endif
			ENDIF
		break 
		
		case 3
			// We're done if conversation is over
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				get_to_meth_lab_dialogue_system_status++
			ENDIF
			// Pause conversation if player gets too far away
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				IF distance_from_player_to_ped(translator.ped) < 8.0
				AND distance_from_player_to_ped(cheng.ped) < 8.0
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ELIF distance_from_player_to_ped(translator.ped) >= 8.0
			OR distance_from_player_to_ped(cheng.ped) >= 8.0
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
			ENDIF
		break 
	
	endswitch 
	
endproc 

proc setup_parked_cars()
	parked_cars[0] = create_vehicle(phoenix, <<1362.9155, 3592.5757, 33.9161>>, 20.8352) 
	parked_cars[1] = create_vehicle(phoenix, <<1368.5109, 3603.1089, 33.9014>>, 22.1103)
	parked_cars[2] = create_vehicle(phoenix, <<1419.4980, 3623.8757, 33.8507>>, 18.4636)  
	parked_cars[3] = create_vehicle(phoenix, <<1433.0057, 3611.3215, 33.8856>>, 15.7737) 
	//parked_cars[4] = create_vehicle(phoenix, <<1432.0254, 3616.4756, 33.9694>>, 105.1350) 
	for i = 0 to 3
		set_vehicle_doors_locked(parked_cars[i], vehiclelock_locked)
		SET_VEHICLE_DIRT_LEVEL(parked_cars[i], 13)
		set_vehicle_colours(parked_cars[i], (i*5), (i*5))
	endfor
	CPRINTLN(DEBUG_MISSION, "setup_parked_cars: CREATED")
	
endproc 


proc setup_wave_3_enemies()

	//no_vehicle_recording_skip = true

	for i = 0 to count_of(wave_3_enemy_vehicle) - 1	
	
		SETUP_ENEMY_VEHICLE(wave_3_enemy_vehicle[i])
		start_playback_recorded_vehicle(wave_3_enemy_vehicle[i].veh,  wave_3_enemy_vehicle[i].recording_number, "lkmethlab")
		//CHI1_CAR_DAMAGE

		if i = 0
			if no_vehicle_recording_skip 
				skip_time_in_playback_recorded_vehicle(wave_3_enemy_vehicle[i].veh, 1000) //1000
			else 
				skip_time_in_playback_recorded_vehicle(wave_3_enemy_vehicle[i].veh, 10500) //11000
			endif 
		endif 
		
	endfor
	
	if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
		create_npc_ped_inside_vehicle(wave_3_enemy[0].ped, char_ortega, wave_3_enemy_vehicle[0].veh)
		setup_enemy_attributes(wave_3_enemy[0])
		setup_relationship_enemy(wave_3_enemy[0].ped, true)
		add_ped_for_dialogue(scripted_speech[0], 6, wave_3_enemy[0].ped, "ortega")
	else 
		setup_enemy_in_vehicle(wave_3_enemy[0], wave_3_enemy_vehicle[0].veh) 
	endif
	SET_PED_BLOCKS_PATHING_WHEN_DEAD(wave_3_enemy[0].ped, FALSE)

	setup_enemy_in_vehicle(wave_3_enemy[1], wave_3_enemy_vehicle[0].veh, vs_front_right) 
	add_ped_for_dialogue(scripted_speech[0], 7, wave_3_enemy[1].ped, "azteca1")
	SET_PED_BLOCKS_PATHING_WHEN_DEAD(wave_3_enemy[1].ped, FALSE)
			
	setup_enemy_in_vehicle(wave_3_enemy[2], wave_3_enemy_vehicle[0].veh, vs_back_left) 
	add_ped_for_dialogue(scripted_speech[0], 8, wave_3_enemy[2].ped, "azteca2")
	SET_PED_BLOCKS_PATHING_WHEN_DEAD(wave_3_enemy[2].ped, FALSE)
		
	setup_enemy_in_vehicle(wave_3_enemy[3], wave_3_enemy_vehicle[0].veh, vs_back_right) 
	add_ped_for_dialogue(scripted_speech[0], ConvertSingleCharacter("B"), wave_3_enemy[3].ped, "azteca4")
	SET_PED_BLOCKS_PATHING_WHEN_DEAD(wave_3_enemy[3].ped, FALSE)
	
	
	//*****ambient enemies which the player does not have to kill to pass the mission*****
	
	for i = 0 to count_of(ambient_enemy_vehicle) - 1
	
		ambient_enemy_vehicle[i].veh = create_vehicle(ambient_enemy_vehicle[i].model, ambient_enemy_vehicle[i].pos, ambient_enemy_vehicle[i].heading)
		SET_VEHICLE_DIRT_LEVEL(ambient_enemy_vehicle[i].veh, 13)
		set_vehicle_doors_locked(ambient_enemy_vehicle[i].veh, vehiclelock_lockout_player_only) 
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(ambient_enemy_vehicle[i].veh, FALSE)
		start_playback_recorded_vehicle(ambient_enemy_vehicle[i].veh,  ambient_enemy_vehicle[i].recording_number, "lkmethlab")
		
		if no_vehicle_recording_skip 
			skip_time_in_playback_recorded_vehicle(ambient_enemy_vehicle[i].veh, 0) 
		else 
			skip_time_in_playback_recorded_vehicle(ambient_enemy_vehicle[i].veh, 5000)//2000
		endif 

	endfor 

	setup_enemy_in_vehicle(ambient_enemy[0], ambient_enemy_vehicle[0].veh) 
	set_blocking_of_non_temporary_events(ambient_enemy[0].ped, true)
	SET_PED_CONFIG_FLAG(ambient_enemy[0].ped, PCF_GetOutBurningVehicle, FALSE)
	SET_PED_CONFIG_FLAG(ambient_enemy[0].ped, PCF_RunFromFiresAndExplosions, FALSE)
	remove_blip(ambient_enemy[0].blip)
	
	setup_enemy_in_vehicle(ambient_enemy[1], ambient_enemy_vehicle[0].veh, vs_front_right) 
	set_blocking_of_non_temporary_events(ambient_enemy[1].ped, true)
	//remove_blip(ambient_enemy[1].blip)
	
	
	setup_enemy_in_vehicle(ambient_enemy[2], ambient_enemy_vehicle[1].veh) 
	set_blocking_of_non_temporary_events(ambient_enemy[2].ped, true)
	SET_PED_CONFIG_FLAG(ambient_enemy[0].ped, PCF_GetOutBurningVehicle, FALSE)
	SET_PED_CONFIG_FLAG(ambient_enemy[0].ped, PCF_RunFromFiresAndExplosions, FALSE)
	remove_blip(ambient_enemy[2].blip)
	
	setup_enemy_in_vehicle(ambient_enemy[3], ambient_enemy_vehicle[1].veh, vs_front_right) 
	set_blocking_of_non_temporary_events(ambient_enemy[3].ped, true)
	//remove_blip(ambient_enemy[3].blip)
	
	// piggy back in here
	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
	
endproc 

func bool are_all_ambient_entities_dead()
				
	if is_ped_injured(ambient_enemy[0].ped)
	and is_ped_injured(ambient_enemy[1].ped)
	and is_ped_injured(ambient_enemy[2].ped)
	and is_ped_injured(ambient_enemy[3].ped)
	and not is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
	and not is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
	
		return true 
		
	endif 
	
	return false

endfunc
				
proc skip_system()

	#IF IS_DEBUG_BUILD
	
		dont_do_j_skip(locates_data)

		if is_keyboard_key_just_pressed(key_j)
			CPRINTLN(DEBUG_MISSION,"skip_system: *** J KEY PRESSED ***")
			
			// @SBA - make sure to reset to game cam if needed and clean cameras
			IF NOT IS_GAMEPLAY_CAM_RENDERING()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
			IF DOES_CAM_EXIST(camera_a)
				SET_CAM_ACTIVE(camera_a, FALSE)
				DESTROY_CAM(camera_a)
			ENDIF
			IF DOES_CAM_EXIST(camera_b)
				SET_CAM_ACTIVE(camera_b, FALSE)
				DESTROY_CAM(camera_b)
			ENDIF
			
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			IF DOES_PICKUP_EXIST(puiGrenadeLauncher)
				REMOVE_PICKUP(puiGrenadeLauncher)
			ENDIF

			switch mission_flow
			
			
				case intro_mocap
				
					if is_cutscene_active()
						stop_cutscene()
					endif 
				
				break 
			
				case get_to_meth_lab
				
					switch get_to_meth_lab_skip_status
					
						case 0
							
							// @SBA - if player is not at meth lab yet
							IF get_to_meth_lab_status = 0
								clear_ped_tasks_immediately(player_ped_id())
								clear_ped_tasks_immediately(cheng.ped)
								clear_ped_tasks_immediately(translator.ped)
								
								set_entity_coords(player_ped_id(), <<1403.1777, 3597.2070, 33.9251>>)
								set_entity_coords(cheng.ped, <<1402.1777, 3597.2070, 33.9251>>)
								set_entity_coords(translator.ped, <<1401.1777, 3597.2070, 33.9251>>)
			
								get_to_meth_lab_skip_status++
							// @SBA - if cutscene is active, stop it and goto exit state
							ELIF get_to_meth_lab_status < 5
								// @SBA - stop cutscene if playing
								IF IS_CUTSCENE_ACTIVE()
									IF IS_CUTSCENE_PLAYING()
										CPRINTLN(DEBUG_MISSION,"*** SHOULD BE SKIPPING CUTSCENE ***")
										STOP_CUTSCENE()
									ELSE
										CPRINTLN(DEBUG_MISSION,"*** SHOULD BE RELEASING CUTSCENE ***")
										REMOVE_CUTSCENE()
									ENDIF
								ENDIF
								// get interior if not gotten yet
								IF get_to_meth_lab_status < 3
									meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
									pin_interior_in_memory(meth_interior)
								ENDIF
								if is_audio_scene_active("CHI_1_DRIVE_TO_LAB")
									stop_audio_scene("CHI_1_DRIVE_TO_LAB")
								endif
								set_entity_coords(player_ped_id(), vTrevorWindowStartPos)
								get_to_meth_lab_status = 5
								get_to_meth_lab_skip_status++
							ENDIF
							
						break
						
						case 1
							// @SBA - if player J-skips twice in this checkpoint, handle the second one to skip the cutscene
							IF get_to_meth_lab_status < 5
								// @SBA - stop cutscene if playing
								IF IS_CUTSCENE_ACTIVE()
									IF IS_CUTSCENE_PLAYING()
										CPRINTLN(DEBUG_MISSION,"*** SHOULD BE SKIPPING CUTSCENE ***")
										STOP_CUTSCENE()
									ELSE
										CPRINTLN(DEBUG_MISSION,"*** SHOULD BE RELEASING CUTSCENE ***")
										REMOVE_CUTSCENE()
									ENDIF
								ENDIF
								// get interior if not gotten yet
								IF get_to_meth_lab_status < 3
									meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
									pin_interior_in_memory(meth_interior)
								ENDIF
								if is_audio_scene_active("CHI_1_DRIVE_TO_LAB")
									stop_audio_scene("CHI_1_DRIVE_TO_LAB")
								endif
								set_entity_coords(player_ped_id(), vTrevorWindowStartPos)
								get_to_meth_lab_status = 5
								get_to_meth_lab_skip_status++
							ENDIF					
						break 
						
						case 2
						
						break 
						
					endswitch 
				
				break 
				
				case wave_0_system
				
					switch wave_0_system_skip_status
					
						case 0
						
							for i = 0 to count_of(wave_0_enemy) - 1
								
								if not is_ped_injured(wave_0_enemy[i].ped)
									SET_ENTITY_HEALTH(wave_0_enemy[i].ped, 2)
								else 
									wave_0_enemy[i].created = true
								endif 
								
							endfor 
							
							wave_0_system_skip_status++
							
						break 
						
						case 1
						
							if wave_0_master_flow_system_status > 2

								// @SBA - get vehicle to destination, set stuff as if the cutscene finished
								IF IS_VEHICLE_DRIVEABLE(wave_0_enemy_vehicle[2].veh)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(wave_0_enemy_vehicle[2].veh)
										SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(wave_0_enemy_vehicle[2].veh)
										SET_ENTITY_PROOFS(wave_0_enemy_vehicle[2].veh, false, false, false, false, false)
										if is_audio_scene_active("CHI_1_TRUCK_ARRIVES")	
											stop_audio_scene("CHI_1_TRUCK_ARRIVES")
										endif
										REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[2].veh)
										original_time = get_game_timer()
										REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[2])
										ram_cutscene_finished = true
									ELSE
										
									ENDIF
								ENDIF

								for i = 0 to count_of(wave_0_inside_enemy) - 1
									if not is_ped_injured(wave_0_inside_enemy[i].ped)
										SET_ENTITY_HEALTH(wave_0_inside_enemy[i].ped, 2)
									endif 
								endfor
								
								CLEAR_TACTICAL_NAV_MESH_POINTS()
								
								clear_ped_tasks_immediately(player_ped_id())
								SET_ENTITY_COORDS(player_ped_id(), <<1386.0081, 3616.5083, 37.9258>>)
								SET_ENTITY_HEADING(player_ped_id(), 326.3752) 
								
								set_blocking_of_non_temporary_events(buddy.ped, true)
								clear_ped_tasks_immediately(buddy.ped)
								SET_ENTITY_COORDS(buddy.ped, <<1385.1, 3615.6, 37.9258>>) // @SBA - updated coord.  OLD = <<1386.9521, 3618.3870, 37.9258>>
								SET_ENTITY_HEADING(buddy.ped, 264.0776)  // @SBA updated heading.  OLD = 164.0776
								
								wave_0_dialogue_system_status = 2
								wave_0_buddy_ai_system_status = 1
								wave_0_assisted_nodes_system_status = 1
								wave_0_master_flow_system_status = 5
								
								wave_0_system_skip_status++
									
							endif 
						
						break 
						
					endswitch 

				break 
				
								
				case wave_2_system
				
					for i = 0 to count_of(wave_2_enemy) - 1
						if not is_ped_injured(wave_2_enemy[i].ped)
							SET_ENTITY_HEALTH(wave_2_enemy[i].ped, 2)
						else 
							wave_2_enemy[i].created = true
						endif 
					endfor 
					ped_structure_are_all_enemies_dead(wave_2_enemy)
					
					clear_ped_tasks_immediately(player_ped_id())
					SET_ENTITY_COORDS(player_ped_id(), <<1407.5627, 3612.4297, 38.0055>>)
					SET_ENTITY_HEADING(player_ped_id(), 278.8889)
					
					clear_ped_tasks_immediately(buddy.ped)
					SET_ENTITY_COORDS(buddy.ped, <<1407.3434, 3614.1377, 38.0055>>)
					SET_ENTITY_HEADING(buddy.ped, 272.0829)

					wave_2_master_flow_system_status = 8
				
				break 
				
				case wave_3_system
				
					switch wave_3_system_skip_status
					
						case 0
							INT iPoint
							clear_ped_tasks_immediately(player_ped_id())
							SET_ENTITY_COORDS(player_ped_id(), <<1395.8505, 3610.0105, 33.9808>>)
							SET_ENTITY_HEADING(player_ped_id(), 79.1915)
							
							if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
								IF WAYPOINT_RECORDING_GET_NUM_POINTS("methlab3", iPoint)
									SET_PED_WAYPOINT_PROGRESS(buddy.ped, iPoint - 1 )
								ENDIF
							ENDIF
							clear_ped_tasks_immediately(buddy.ped)
//							SET_ENTITY_COORDS(buddy.ped, <<1393.5751, 3609.3645, 33.9808>>)
//							SET_ENTITY_HEADING(buddy.ped, 140.3637)
							buddy_time = get_game_timer()
							
							wave_3_system_skip_status++
						
						break 
						
						// get Chef into cover after skip
						CASE 1
							TASK_PUT_PED_DIRECTLY_INTO_COVER(buddy.ped, vChefCoverPointLiquorStore, -1, TRUE, NORMAL_BLEND_DURATION,  FALSE, FALSE, buddy_cover_point_data[4].cover_point, TRUE)
							wave_3_buddy_ai_system_status = 4
							wave_3_system_skip_status++
						
						BREAK
						
						case 2
						
							for i = 0 to count_of(wave_3_enemy) - 1
								
								if not is_ped_injured(wave_3_enemy[i].ped)
									set_entity_health(wave_3_enemy[i].ped, 2)
								else 
									wave_3_enemy[i].created = true
								endif 
									
							endfor 
							
							ped_structure_are_all_enemies_dead(wave_3_enemy)
							
							wave_3_system_skip_status++
						
						break 
						
						case 3
						
							set_entity_coords(player_ped_id(), <<1387.9937, 3598.5166, 33.8954>>)
							if not does_blip_exist(ice_box_blip)
								ice_box_blip = create_blip_for_coord(<<1388.0475, 3598.6162, 33.8954>>)
							endif 
							wave_3_master_flow_system_status = 4
						
							wave_3_system_skip_status++
						break 
						
						case 4
						
						break 
						
					endswitch 

				break 
				
				CASE mission_passed_mocap
				
					// @SBA - stop final cutscene if playing
					IF IS_CUTSCENE_ACTIVE()
						CPRINTLN(DEBUG_MISSION,"*** SHOULD BE SKIPPING CUTSCENE ***")
						STOP_CUTSCENE()
					ENDIF

				BREAK
				
			endswitch 
		
		elif is_keyboard_key_just_pressed(key_p)
		
			
			switch mission_flow

				case get_to_meth_lab
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					launch_mission_stage_menu_status = 0
				
				break 
			
				case wave_0_system
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets

					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 2	
					else
						launch_mission_stage_menu_status = 1
					endif 
						
					p_skip_time = get_game_timer()
					
				break 
				
				
				case wave_2_system
				case wave_3_system
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 4
					else
						launch_mission_stage_menu_status = 3
					endif 
						
					p_skip_time = get_game_timer()
				
				break 

			endswitch 
		
		endif 
	
		if LAUNCH_MISSION_STAGE_MENU(menu_stage_selector, menu_return_stage) 
			
			remove_all_mission_assets()

			launch_mission_stage_menu_status = menu_return_stage
			
			mission_flow = load_stage_selector_assets
		endif
	
	#endif 

endproc 


//PURPOSE: makes sure the player's current vehicle is acceptible for the buddy locate.  If not, create Trevor's truck.
proc setup_vehicle_outside_bar()

	IF bTrevorsVehicleIsOkay
		EXIT
	ENDIF
	
	IF not does_entity_exist(trevors_truck.veh)
	
		if has_model_loaded(trevors_truck.model)

			if not is_players_last_vehicle_present_and_acceptable(GET_PLAYERS_LAST_VEHICLE(), <<2000.1245, 3059.5662, 46.0491>>, 2, <<2008.9219, 3054.0513, 46.0528>>, 326.3415)
				CPRINTLN(DEBUG_MISSION, "Player needs an appropriate vehicle.  Making one.")
				WHILE NOT CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<2000.1245, 3059.5662, 46.0491>>, 59.1994, false)
					WAIT(0)
					CPRINTLN(DEBUG_MISSION, "Waiting on new vehicle...")
				ENDWHILE
				OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(trevors_truck.veh)
			ELSE
				//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(trevors_truck.veh), (<<2000.1245, 3059.5662, 46.0491>>)) > 20.0
					
					trevors_truck.veh = GET_PLAYERS_LAST_VEHICLE()
					
					CPRINTLN(DEBUG_MISSION, "Need to relocate player's vehicle")
					SET_ENTITY_COORDS(trevors_truck.veh, (<<2000.1245, 3059.5662, 46.0491>>))
					SET_ENTITY_HEADING(trevors_truck.veh, 59.1994)
					SET_VEHICLE_ON_GROUND_PROPERLY(trevors_truck.veh)
				//ENDIF
			endif
			
			CPRINTLN(DEBUG_MISSION, "Player should have vehicle")
			bTrevorsVehicleIsOkay = TRUE
			
		endif 

	ENDIF 
	
endproc 

PROC CLEAR_OLD_CAR_GEN()
	DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(vTrevorsVehicleMethLabLoc, 10.0)
	CLEAR_AREA(vTrevorsVehicleMethLabLoc, 10.0, TRUE)
ENDPROC

func bool is_player_infront_of_door()

	vector vec_BA
	vector door_forward_vec
	
	vec_BA = (<<1398.80, 3607.94, 39.19>> - GET_ENTITY_COORDS(player_ped_id()))
	door_forward_vec = <<1402.35, 3609.46, 39.35>> - <<1398.80, 3607.94, 39.19>>
	
	if dot_product(vec_BA, door_forward_vec) < 0.0
		#IF IS_DEBUG_BUILD
		printstring("infront")
		printnl()
		#endif
		return true
	endif 

	return false 

endfunc 

proc request_start_of_mission_assets()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(cheng.model)
	request_model(translator.model)
	request_model(get_player_veh_model(char_trevor))
	request_vehicle_asset(trevors_truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS))
	request_model(bins[0].model)
	request_anim_dict(sCrazyDanceDict) 
	
endproc 

func bool is_start_of_mission_assets_requested()

	if has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	and has_model_loaded(translator.model)
	and has_model_loaded(cheng.model)
	and has_model_loaded(get_player_veh_model(char_trevor))
	and has_vehicle_asset_loaded(trevors_truck.model)
	and has_model_loaded(bins[0].model)
	and has_anim_dict_loaded(sCrazyDanceDict)
	
		return true 
		
	endif 
	
	return false
	
endfunc 

PROC REQUEST_BAR_PATRON_ASSETS()
	request_model(IG_JANET)
	request_model(IG_JOSEF)
	request_model(IG_OLD_MAN1A)
	request_model(IG_OLD_MAN2)
	request_model(IG_RUSSIANDRUNK)
ENDPROC

FUNC BOOL HAVE_BAR_PATRON_ASSETS_LOADED()
	IF has_model_loaded(IG_JANET)
	and has_model_loaded(IG_JOSEF)
	and has_model_loaded(IG_OLD_MAN1A)
	and has_model_loaded(IG_OLD_MAN2)
	and has_model_loaded(IG_RUSSIANDRUNK)
		return true 
	endif 
	
	return false
ENDFUNC

// @SBA - splitting out the loads
PROC REQUEST_METH_ARRIVAL_ASSETS()
	REQUEST_MODEL(buddy.model)
	REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_CARBINERIFLE))
	REQUEST_WEAPON_ASSET(WEAPONTYPE_carbinerifle)
ENDPROC

FUNC BOOL HAVE_METH_ARRIVAL_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(buddy.model)
	AND HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_carbinerifle))
	AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_carbinerifle)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// @SBA - assets for fight, post arrival cutscene
PROC REQUEST_FIGHT_ASSETS()
	REQUEST_MODEL(buddy.model)
	request_model(wave_0_enemy[0].model)
	request_model(wave_0_enemy[1].model)
	request_model(phoenix)
	request_model(sadler)
	request_model(dubsta2)
	
	request_model(explosive_barrels[0].model)
	request_model(bins[0].model)
	request_model(cover_box.model)
	
	request_model(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
	request_weapon_asset(WEAPONTYPE_ASSAULTRIFLE)
	
	request_vehicle_recording(001, "lkmethlab")
	request_vehicle_recording(002, "lkmethlab")
	request_vehicle_recording(003, "lkmethlab")
	
	REQUEST_WAYPOINT_RECORDING("methlab1")
	REQUEST_WAYPOINT_RECORDING("methlab3")
	REQUEST_WAYPOINT_RECORDING("methlab4")
	REQUEST_WAYPOINT_RECORDING("methlab5")
	REQUEST_WAYPOINT_RECORDING("methlab7")
	
	request_anim_dict("MissChinese1") 

ENDPROC

FUNC BOOL HAVE_FIGHT_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(buddy.model)
	AND HAS_MODEL_LOADED(wave_0_enemy[0].model)
	AND HAS_MODEL_LOADED(wave_0_enemy[1].model)
	AND HAS_MODEL_LOADED(sadler)
	AND HAS_MODEL_LOADED(dubsta2)
	AND HAS_MODEL_LOADED(phoenix)
	and has_model_loaded(explosive_barrels[0].model)
	and has_model_loaded(bins[0].model)
	and has_model_loaded(cover_box.model)
	and has_model_loaded(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
	and has_weapon_asset_loaded(WEAPONTYPE_ASSAULTRIFLE)
	and has_vehicle_recording_been_loaded(001, "lkmethlab")
	and has_vehicle_recording_been_loaded(002, "lkmethlab")
	and has_vehicle_recording_been_loaded(003, "lkmethlab")
	and get_is_waypoint_recording_loaded("methlab1")
	and get_is_waypoint_recording_loaded("methlab3")
	and get_is_waypoint_recording_loaded("methlab4")
	and get_is_waypoint_recording_loaded("methlab5")
	and get_is_waypoint_recording_loaded("methlab7")
	and has_anim_dict_loaded("MissChinese1")
		RETURN TRUE 
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

proc request_shootout_assets(BOOL bRequestChefModel = TRUE)

//	// @SBA - per B*1121357, requesting chef model later
	IF bRequestChefModel
		request_model(buddy.model)
	ENDIF
//	request_model(wave_0_enemy[0].model)
//	request_model(wave_0_enemy[1].model)
//	
//	request_model(sadler)
//	request_model(dubsta2)
//	request_model(phoenix)
	request_model(pcj)
	
	request_model(explosive_barrels[0].model)
	request_model(cover_box.model)
	
//	request_model(get_weapontype_model(weapontype_carbinerifle))
	request_model(get_weapontype_model(weapontype_grenadelauncher))
//	request_model(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
//	
//	request_weapon_asset(WEAPONTYPE_carbinerifle)
	request_weapon_asset(WEAPONTYPE_grenadelauncher)
//	request_weapon_asset(WEAPONTYPE_ASSAULTRIFLE)
	
//	request_vehicle_recording(001, "lkmethlab")
//	request_vehicle_recording(002, "lkmethlab")
//	request_vehicle_recording(003, "lkmethlab")
//	request_vehicle_recording(004, "lkmethlab")
//	request_vehicle_recording(005, "lkmethlab")
//	request_vehicle_recording(006, "lkmethlab")
	request_vehicle_recording(007, "lkmethlab")
	request_vehicle_recording(008, "lkmethlab")
	request_vehicle_recording(009, "lkmethlab")
	request_vehicle_recording(010, "lkmethlab")
	request_vehicle_recording(011, "lkmethlab")
	request_vehicle_recording(012, "lkmethlab")
//	request_vehicle_recording(013, "lkmethlab")
//	request_vehicle_recording(014, "lkmethlab")
//	request_vehicle_recording(015, "lkmethlab")
//	request_vehicle_recording(016, "lkmethlab")
	request_vehicle_recording(017, "lkmethlab")
	request_vehicle_recording(018, "lkmethlab")
	request_vehicle_recording(019, "lkmethlab")
	request_vehicle_recording(020, "lkmethlab")
	request_vehicle_recording(021, "lkmethlab")
//	request_vehicle_recording(030, "lkmethlab")
//	request_vehicle_recording(031, "lkmethlab")
//	request_vehicle_recording(032, "lkmethlab")
	
//	REQUEST_WAYPOINT_RECORDING("methlab1")
////	REQUEST_WAYPOINT_RECORDING("methlab2")
//	REQUEST_WAYPOINT_RECORDING("methlab3")
//	REQUEST_WAYPOINT_RECORDING("methlab4")
//	REQUEST_WAYPOINT_RECORDING("methlab5")
////	REQUEST_WAYPOINT_RECORDING("methlab6")
//	REQUEST_WAYPOINT_RECORDING("methlab7")
////	REQUEST_WAYPOINT_RECORDING("methlab8")
////	REQUEST_WAYPOINT_RECORDING("methlab9")
//	
//	request_anim_dict("MissChinese1") 
//
endproc 

func bool is_shootout_assets_loaded()

	if has_model_loaded(buddy.model)
//	and has_model_loaded(wave_0_enemy[0].model)
//	and has_model_loaded(wave_0_enemy[1].model)
//	and has_model_loaded(sadler)
//	and has_model_loaded(dubsta2)
//	and has_model_loaded(phoenix)
	and has_model_loaded(pcj)
	and has_model_loaded(explosive_barrels[0].model)
	and has_model_loaded(cover_box.model)
//	and has_model_loaded(get_weapontype_model(WEAPONTYPE_carbinerifle))
	and has_model_loaded(get_weapontype_model(weapontype_grenadelauncher))
//	and has_model_loaded(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
//	and has_weapon_asset_loaded(WEAPONTYPE_carbinerifle)
	and has_weapon_asset_loaded(WEAPONTYPE_grenadelauncher)
//	and has_weapon_asset_loaded(WEAPONTYPE_ASSAULTRIFLE)
//	and has_vehicle_recording_been_loaded(001, "lkmethlab")
//	and has_vehicle_recording_been_loaded(002, "lkmethlab")
//	and has_vehicle_recording_been_loaded(003, "lkmethlab")
//	and has_vehicle_recording_been_loaded(004, "lkmethlab")
//	and has_vehicle_recording_been_loaded(005, "lkmethlab")
//	and has_vehicle_recording_been_loaded(006, "lkmethlab")
	and has_vehicle_recording_been_loaded(007, "lkmethlab")
	and has_vehicle_recording_been_loaded(008, "lkmethlab")
	and has_vehicle_recording_been_loaded(009, "lkmethlab")
	and has_vehicle_recording_been_loaded(010, "lkmethlab")
	and has_vehicle_recording_been_loaded(011, "lkmethlab")
	and has_vehicle_recording_been_loaded(012, "lkmethlab")
//	and has_vehicle_recording_been_loaded(013, "lkmethlab")
//	and has_vehicle_recording_been_loaded(014, "lkmethlab")
//	and has_vehicle_recording_been_loaded(015, "lkmethlab")
//	and has_vehicle_recording_been_loaded(016, "lkmethlab")
	and has_vehicle_recording_been_loaded(017, "lkmethlab")
	and has_vehicle_recording_been_loaded(018, "lkmethlab")
	and has_vehicle_recording_been_loaded(019, "lkmethlab")
	and has_vehicle_recording_been_loaded(020, "lkmethlab")
	and has_vehicle_recording_been_loaded(021, "lkmethlab")
//	and has_vehicle_recording_been_loaded(030, "lkmethlab")
//	and has_vehicle_recording_been_loaded(031, "lkmethlab")
//	and has_vehicle_recording_been_loaded(032, "lkmethlab")
//	and get_is_waypoint_recording_loaded("methlab1")
////	and get_is_waypoint_recording_loaded("methlab2")
//	and get_is_waypoint_recording_loaded("methlab3")
//	and get_is_waypoint_recording_loaded("methlab4")
//	and get_is_waypoint_recording_loaded("methlab5")
////	and get_is_waypoint_recording_loaded("methlab6")
//	and get_is_waypoint_recording_loaded("methlab7")
////	and get_is_waypoint_recording_loaded("methlab8")
////	and get_is_waypoint_recording_loaded("methlab9")
//	and has_anim_dict_loaded("MissChinese1")
	
		return true 
		
	endif
	
	return false

endfunc 

proc create_start_of_mission_entities(bool playing_mocap = true)

	if not playing_mocap
	
		clear_area(<<1998.2826, 3058.0435, 46.0491>>, 10000.00, true)
		remove_decals_in_range(<<1998.2826, 3058.0435, 46.0491>>, 10000.00)
		delete_vehicle_gen_vehicles_in_area(<<1401.5897, -2053.6978, 50.9983>>, 10.0)
		
		RECREATE_PLAYERS_VEHICLE(<<2000.1245, 3059.5662, 46.0491>>, 59.1994, FALSE)
		
		setup_vehicle_outside_bar()
		SET_INITIAL_PLAYER_STATION("RADIO_07_DANCE_01")
		
		setup_buddy(cheng)
		setup_buddy(translator)
		
		SET_START_VERSIONS_OF_BUDDIES()
		
		setup_relationship_contact(cheng.ped, true)
		set_ped_can_ragdoll(cheng.ped, true)
		remove_blip(cheng.blip)
		add_ped_for_dialogue(scripted_speech[0], 4, cheng.ped, "cheng")
		task_enter_vehicle(cheng.ped, trevors_truck.veh, 30000, vs_front_right, PEDMOVE_walk)
		force_ped_motion_state(cheng.ped, ms_on_foot_walk, false)//, faus_cutscene_exit)
		force_ped_ai_and_animation_update(cheng.ped)
		SET_PED_LOD_MULTIPLIER(cheng.ped, 4.0)
		
		setup_relationship_contact(translator.ped, true)
		set_ped_can_ragdoll(translator.ped, true)
		remove_blip(translator.blip)
		add_ped_for_dialogue(scripted_speech[0], 3, translator.ped, "translator")
		task_enter_vehicle(translator.ped, trevors_truck.veh, 30000, vs_back_left, PEDMOVE_walk)
		force_ped_motion_state(translator.ped, ms_on_foot_walk, false)//, faus_cutscene_exit)
		force_ped_ai_and_animation_update(translator.ped)
		SET_PED_LOD_MULTIPLIER(translator.ped, 4.0)

		clear_ped_tasks_immediately(player_ped_id())
		IF NOT IS_REPLAY_BEING_SET_UP()
			set_entity_coords(player_ped_id(), <<1998.04980, 3055.90698, 46.05226>>)//<<1998.19702, 3056.11426, 46.05203>>)
			set_entity_heading(player_ped_id(), 328.0059)
			force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
			SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 700) 
			force_ped_ai_and_animation_update(player_ped_id())
			//task_enter_vehicle(player_ped_id(), trevors_truck.veh, -1, VS_DRIVER, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT) 
		ENDIF
	endif 
	
	
	SET_VEHICLE_DIRT_LEVEL(trevors_truck.veh, 13)
	SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(locates_data, trevors_truck.veh)
	//set_vehicle_as_no_longer_needed(trevors_truck.veh)
		

	add_ped_for_dialogue(scripted_speech[0], 0, null, "cook") //added for phone call(@SBA - was 5 but ID num is 0 in D*.)
	
	get_to_meth_lab_dialogue_system()
	

endproc 

PROC HANDLE_CREATE_BAR_PATRONS()
	
	IF bBarPatronsCreated
		EXIT
	ENDIF
	
	REQUEST_BAR_PATRON_ASSETS()
	
	IF NOT HAVE_BAR_PATRON_ASSETS_LOADED()
		CPRINTLN(DEBUG_MISSION, "CREATE_BAR_PATRONS: Loading assets...")
		EXIT
	ENDIF
	
	CLEAR_AREA_OF_PEDS(<<1984.136353,3051.933838,46.215084>> , 5.5, TRUE)
	sbiBarBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<1984.9971, 3052.9048, 46.8556>>+<<5,5,2>>,<<1984.9971, 3052.9048, 46.8556>>-<<5,5,2>>)
	
	CPRINTLN(DEBUG_MISSION, "CREATE_BAR_PATRONS: Creating patrons")
	piBarLady = CREATE_PED(PEDTYPE_MISSION, IG_JANET, (<<1983.43469, 3054.87573, 47.00919>>))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(piBarLady, player_group)
	if DOES_SCENARIO_EXIST_IN_AREA(<<1983.43469, 3054.87573, 47.00919>>, 0.5, false)//true
		TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(piBarLady, (<<1983.43469, 3054.87573, 47.00919>>), 0.5, -1)
	endif 

	piJosef = CREATE_PED(PEDTYPE_MISSION, IG_JOSEF, (<<1987.3, 3048.7, 46.2>>))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(piJosef, player_group)
	TASK_WANDER_STANDARD(piJosef)
	
	piOldMan1A = CREATE_PED(PEDTYPE_MISSION, IG_OLD_MAN1A, (<<1985.32666, 3052.07471, 46.2>>))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(piOldMan1A, player_group)
	SET_PED_PROP_INDEX(piOldMan1A, ANCHOR_HEAD,0)
	if DOES_SCENARIO_EXIST_IN_AREA(<<1985.32666, 3053.07471, 46.97862>>, 0.75, true)
		TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(piOldMan1A, (<<1985.32666, 3053.07471, 46.97862>>), 0.75, 15000)
	endif 

	piOldMan2 = CREATE_PED(PEDTYPE_MISSION, IG_OLD_MAN2, (<<1984.87280, 3052.09902, 46.2>>))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(piOldMan2, player_group)
	if DOES_SCENARIO_EXIST_IN_AREA(<<1984.87280, 3052.49902, 46.97861>>, 0.75, true)
		TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(piOldMan2, (<<1984.87280, 3052.49902, 46.97861>>), 0.75, 15000)
	endif 
	
	piDrunk = CREATE_PED(PEDTYPE_MISSION, IG_RUSSIANDRUNK, (<<1985.552, 3050.480, 47.215>>), -30.26)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(piDrunk, player_group)
	APPLY_PED_BLOOD_SPECIFIC(piDrunk, ENUM_TO_INT(PDZ_HEAD), 0.502, 0.730, 0.0, 0.7, 0, 0.0, "BasicSlash")
	TASK_START_SCENARIO_IN_PLACE(piDrunk, "WORLD_HUMAN_STUPOR", -1)  //"WORLD_HUMAN_BUM_SLUMPED"
	
	bBarPatronsCreated = TRUE
ENDPROC

//PURPOSE: Stores any near by vehicles and sets them as a mission entity. Means they won't be cleared on clear area
//call release_obtained_nearby_vehicles() to release them.
proc obtain_nearby_vehicles_and_store_them()
				
	vehicle_index stored_last_vehicle 
	
	get_ped_nearby_vehicles(player_ped_id(), ambient_car)
	
	stored_last_vehicle = get_players_last_vehicle()
	
	for i = 0 to count_of(ambient_car) - 1

		if ambient_car[i] != stored_last_vehicle
		
			if does_entity_exist(ambient_car[i])
			
				if is_vehicle_driveable(ambient_car[i])
					//player pos and heading data = 1997.9380, 3056.4143, 46.0516>>, 328.5000
					if is_entity_in_angled_area(ambient_car[i], <<1993.303, 3065.119, 46.052>>, <<2007.798, 3056.236, 50.052>>, 5.0)
				
						//delete_vehicle(ambient_car[i])
						clear_area(get_entity_coords(ambient_car[i]), 1.0, true)
				
					else 
					
						//delete cars driving downt he road with drivers.
						if does_entity_exist(GET_PED_IN_VEHICLE_SEAT(ambient_car[i]))
							
							clear_area(get_entity_coords(ambient_car[i]), 1.0, true) //clear area rther than deleting to avoid "vehicle is not a script entity"
							
						else 
							set_entity_as_mission_entity(ambient_car[i], true, true)
						endif 

					endif

				else 
					clear_area(get_entity_coords(ambient_car[i]), 1.0, false)
				endif 
				
			endif 
			
		endif 

	endfor 
	
endproc 

//PURPOSE: Releases stored near by vehicles. 
proc release_obtained_nearby_vehicles()
				
	for i = 0 to count_of(ambient_car) - 1
		if does_entity_exist(ambient_car[i])
			set_vehicle_as_no_longer_needed(ambient_car[i])
		endif 
	endfor 
	
endproc 

proc set_players_last_vehicle_to_vehicle_gen(vector car_pos, float car_heading)

	vehicle_index players_last_car 
	
	players_last_car = GET_PLAYERS_LAST_VEHICLE() //get_players_last_vehicle
	
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
		
			model_names players_last_vehicle_model 
		
			players_last_vehicle_model = get_entity_model(players_last_car)
			
			if not is_this_model_a_plane(players_last_vehicle_model )
		
				SET_MISSION_VEHICLE_GEN_VEHICLE(players_last_car, car_pos, car_heading) 
				
				//SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN
				//GET_MISSION_START_VEHICLE_INDEX()
				
			endif 
			
		endif 
	endif 
	
endproc 

proc meth_lab_intro_mocap()

	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2170997

	switch intro_mocap_status
	
		case 0
	
			interior_instance_index interior_hicksbar

			REQUEST_CUTSCENE("chinese_1_int")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("trevor", PLAYER_PED_ID())
			SET_CUTSCENE_PED_PROP_VARIATION("Tao", ANCHOR_EYES, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Taos_Translator", ANCHOR_EYES, 0)

			interior_hicksbar = GET_INTERIOR_AT_COORDS_with_type(<<1986.2722, 3051.9758, 47.1256>>, "v_hicksbar")
			PIN_INTERIOR_IN_MEMORY(interior_hicksbar)

			IF is_interior_ready(interior_hicksbar)
				if HAS_CUTSCENE_LOADED_WITH_FAILSAFE() 
				
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Tao", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, cheng.model)
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Taos_Translator", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, translator.model)

					START_CUTSCENE()
										
					unpin_interior(interior_hicksbar)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

					intro_mocap_status++
				ENDIF
			endif 
		
		break 
		
		case 1
		
			IF IS_REPEAT_PLAY_ACTIVE()
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					ENDIF
				ENDIF
			ENDIF
				
			if is_cutscene_playing()
			
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
				request_start_of_mission_assets()

				delete_vehicle_gen_vehicles_in_area(<<2000.1245, 3059.5662, 46.0491>>, 10.0)
				
				set_players_last_vehicle_to_vehicle_gen(<<2000.1245, 3059.5662, 46.0491>>, 59.1994)
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1980.730, 3073.228, 46.049>>, <<2014.615, 3051.972, 50.049>>, 26.700, <<2000.1245, 3059.5662, 46.0491>>, 59.1994, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())//, false)//, false) //2.5, 5.5, 2.0
				
				
					
//				trevors_truck.veh = GET_MISSION_START_VEHICLE_INDEX()
//				if does_entity_exist(trevors_truck.veh)
//					if is_vehicle_driveable(trevors_truck.veh)
//					
//						SET_ENTITY_AS_MISSION_ENTITY(trevors_truck.veh, true, true)
//						SET_VEHICLE_DOORS_SHUT(trevors_truck.veh)
//						SET_VEHICLE_DOORS_LOCKED(trevors_truck.veh, vehiclelock_unlocked)
//						
//						// @SBA - make sure we have anims for whatever the vehicle is
//						request_vehicle_asset(GET_ENTITY_MODEL(trevors_truck.veh))
//						
//					endif 
//				endif
				SET_INITIAL_PLAYER_STATION("RADIO_07_DANCE_01")
				
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_CHINESE_1)
				
				intro_mocap_status++
	
			endif 
		
		break 
		
		case 2
		
			request_start_of_mission_assets()
		
			if not WAS_CUTSCENE_SKIPPED()

				if IS_CUTSCENE_PLAYING()
				
					INT iCutTime
					iCutTime = get_cutscene_time()
					CPRINTLN(DEBUG_MISSION, "Cutscene Time = ", iCutTime)

					// @SBA - load in scene out to the mountains
					IF iCutTime > 85000
					AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
						IF NEW_LOAD_SCENE_START((<<1997.36, 3054.05, 47.81>>), NORMALISE_VECTOR(<<0.53, 0.85, -0.05>>), 3500.0)
							CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Cutscene ending: Calling NEW_LOAD_SCENE_START().")
						ENDIF
					ENDIF

					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tao", cheng.model))
						cheng.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tao", cheng.model))
						SET_PED_PROP_INDEX(cheng.ped, ANCHOR_EYES, 0)
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Taos_Translator", translator.model))
						translator.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Taos_Translator", translator.model))
						SET_PED_PROP_INDEX(translator.ped, ANCHOR_EYES, 0)
					ENDIF
					
					setup_vehicle_outside_bar()
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tao", cheng.model)

						set_entity_coords(cheng.ped, cheng.pos)
						set_entity_heading(cheng.ped, cheng.heading)
						setup_relationship_contact(cheng.ped, true)
						setup_buddy_attributes(cheng)
						set_ped_can_ragdoll(cheng.ped, true)
						add_ped_for_dialogue(scripted_speech[0], 4, cheng.ped, "cheng")
						task_enter_vehicle(cheng.ped, trevors_truck.veh, 30000, vs_front_right, PEDMOVE_walk)
						force_ped_motion_state(cheng.ped, ms_on_foot_walk, false)//, faus_cutscene_exit)
						force_ped_ai_and_animation_update(cheng.ped)
						
					endif
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator", translator.model)
						
						set_entity_coords(translator.ped, translator.pos)
						set_entity_heading(translator.ped, translator.heading)
						setup_relationship_contact(translator.ped, true)
						setup_buddy_attributes(translator)
						set_ped_can_ragdoll(translator.ped, true)
						add_ped_for_dialogue(scripted_speech[0], 3, translator.ped, "translator")
						task_enter_vehicle(translator.ped, trevors_truck.veh, 30000, vs_back_left, PEDMOVE_walk)
						force_ped_motion_state(translator.ped, ms_on_foot_walk, false)//, faus_cutscene_exit)
						force_ped_ai_and_animation_update(translator.ped)
		
					endif 
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))
						CPRINTLN(DEBUG_MISSION, "Player exit state.")
				
						//clear_ped_tasks_immediately(player_ped_id())
						set_entity_coords(player_ped_id(), <<1998.19702, 3056.11426, 46.05203>>)  //<<1998.04980, 3055.90698, 46.05226>>)
						set_entity_heading(player_ped_id(), 328.0059)
						force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
						SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 300) 
						force_ped_ai_and_animation_update(player_ped_id())
						
					endif
				
					if CAN_SET_EXIT_STATE_FOR_CAMERA()
						CPRINTLN(DEBUG_MISSION, "Camera exit state.")
						set_gameplay_cam_relative_heading(0)
						set_gameplay_cam_relative_pitch(0)

					endif
					
				else 
				
					if is_start_of_mission_assets_requested()
						CPRINTLN(DEBUG_MISSION, "starting assets loaded.")
						IF IS_NEW_LOAD_SCENE_LOADED()

							NEW_LOAD_SCENE_STOP()
							CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Cutsene ending: calling NEW_LOAD_SCENE_STOP.")
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
						
							initialise_mission_variables()
			
							create_start_of_mission_entities()
							
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Mission start") 
							
							END_CUTSCENE_BASIC()
	
							if is_screen_faded_out()
								do_screen_fade_in(DEFAULT_FADE_TIME)
							endif
							
							INIT_CHENGS_CRAZY_CAR_DANCING()
							REPLAY_STOP_EVENT()
							
							REPLAY_RECORD_BACK_FOR_TIME(0.0, 12.0)
							
							mission_flow = get_to_meth_lab
						ENDIF
						
					endif
					
				endif 
			
			else 
	
				SET_CUTSCENE_FADE_VALUES(false, false, true, false)
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					IF NEW_LOAD_SCENE_START((<<1997.36, 3054.05, 47.81>>), NORMALISE_VECTOR(<<0.53, 0.85, -0.05>>), 3500.0)
						CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Cutscene skipped: Calling NEW_LOAD_SCENE_START().")
					ENDIF
				ENDIF
				intro_mocap_status++
	
			endif 

		
		break 
		
		
		// SKIPPED
		case 3
			
			request_start_of_mission_assets()
		
			if IS_CUTSCENE_PLAYING()
			
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tao", cheng.model))
					cheng.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tao", cheng.model))
				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Taos_Translator", translator.model))
					translator.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Taos_Translator", translator.model))
				ENDIF
				
				
				//hack fix to stop the translator from playing a secondary reaction anim. This played because the 
				//translator was to close to the player when the mocap ended and the script was waiting on
				//assets to load so the physics would have activated on the peds, this would then fire off
				//a reaction which did not occur till after clear_ped_tasks_immediately()
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tao", cheng.model)
					IF NOT IS_PED_INJURED(cheng.ped)
						set_entity_coords(cheng.ped, cheng.pos)
						set_entity_heading(cheng.ped, cheng.heading)
					ENDIF
				endif 
				
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator", translator.model)
					IF NOT IS_PED_INJURED(translator.ped)
						set_entity_coords(translator.ped, translator.pos)
						set_entity_heading(translator.ped, translator.heading)
					ENDIF
				endif 
				
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))
					IF NOT IS_PED_INJURED(player_ped_id())
						set_entity_coords(player_ped_id(), <<1998.19702, 3056.11426, 46.05203>>)//<<1998.04980, 3055.90698, 46.05226>>)
						set_entity_heading(player_ped_id(), 328.0059)
					ENDIF
				endif
				
			else
			
				while not is_start_of_mission_assets_requested() 
				OR NOT IS_NEW_LOAD_SCENE_LOADED()
					CPRINTLN(DEBUG_MISSION, "Waiting on assets and load scene...")
					
					REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2170997
					
					WAIT(0)
				endwhile 
				
				mission_fail_checks()//added because of above while loop
				
				setup_vehicle_outside_bar()
				
				NEW_LOAD_SCENE_STOP()
				CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Cutsene skipped: calling NEW_LOAD_SCENE_STOP.")
			
				IF NOT IS_PED_INJURED(cheng.ped)
					clear_ped_tasks_immediately(cheng.ped)
					set_entity_coords(cheng.ped, cheng.pos)
					set_entity_heading(cheng.ped, cheng.heading)
					setup_buddy_attributes(cheng)
					setup_relationship_contact(cheng.ped, true)
					set_ped_can_ragdoll(cheng.ped, true)
					add_ped_for_dialogue(scripted_speech[0], 4, cheng.ped, "cheng")
					task_enter_vehicle(cheng.ped, trevors_truck.veh, 30000, vs_front_right, PEDMOVE_walk)
					force_ped_motion_state(cheng.ped, ms_on_foot_walk, false)//, faus_cutscene_exit)
					force_ped_ai_and_animation_update(cheng.ped)
				ENDIF
				IF NOT IS_PED_INJURED(translator.ped)
					clear_ped_tasks_immediately(translator.ped)
					set_entity_coords(translator.ped, translator.pos)
					set_entity_heading(translator.ped, translator.heading)
					setup_buddy_attributes(translator)
					setup_relationship_contact(translator.ped, true)
					set_ped_can_ragdoll(translator.ped, true)
					add_ped_for_dialogue(scripted_speech[0], 3, translator.ped, "translator")
					task_enter_vehicle(translator.ped, trevors_truck.veh, 30000, vs_back_left, PEDMOVE_walk)
					force_ped_motion_state(translator.ped, ms_on_foot_walk, false)//, faus_cutscene_exit)
					force_ped_ai_and_animation_update(translator.ped)
				ENDIF
				
				IF NOT IS_PED_INJURED(player_ped_id())
					clear_ped_tasks_immediately(player_ped_id())
					set_entity_coords(player_ped_id(), <<1998.19702, 3056.11426, 46.05203>>) //<<1998.04980, 3055.90698, 46.05226>>)
					set_entity_heading(player_ped_id(), 328.0059)
					force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
					SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 300) 
					force_ped_ai_and_animation_update(player_ped_id())
				ENDIF
				
				create_start_of_mission_entities()
				SET_START_VERSIONS_OF_BUDDIES()
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Mission start") 

				if is_screen_faded_out()
					end_cutscene()
				else 
					end_cutscene_no_fade()
				endif 
				
				REPLAY_STOP_EVENT()
				
				INIT_CHENGS_CRAZY_CAR_DANCING()

				mission_flow = get_to_meth_lab

			endif 
			
		break 
		
	endswitch
	
endproc 

func bool area_system()

	//stop_mission_fail_checks = true 

	return false 

endfunc 

proc wave_0_inside_enemy_0_attack()
							
	open_sequence_task(seq)
		//task_pause(null, 1000)
		task_set_blocking_of_non_temporary_events(null, false)
		task_set_sphere_defensive_area(null, <<1370.4471, 3618.1826, 33.8917>>, 8.0)//2.0
		task_combat_ped(null, player_ped_id())
	close_sequence_task(seq)
	task_perform_sequence(wave_0_inside_enemy[0].ped, seq)
	clear_sequence_task(seq)
	
	attack_time = get_game_timer()
	
endproc 

proc wave_0_inside_enemy_0_system()

	if not is_ped_injured(wave_0_inside_enemy[0].ped)
	
		// @SBA - make blip for enemy if needed
		HANDLE_ENEMY_BLIP_CREATION(wave_0_inside_enemy[0])

		switch wave_0_inside_enemy_status[0]
		
			case 0
			
				if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
					if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
						if ((get_time_position_in_recording(wave_0_enemy_vehicle[2].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_0_enemy_vehicle[2].recording_number, "lkmethlab"))
									
							stop_playback_recorded_vehicle(wave_0_enemy_vehicle[2].veh)
							REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[2])
							
							wave_0_inside_enemy_status[0]++
						endif 
					
					else 
						wave_0_inside_enemy_status[0]++
					endif 
				
				else 
					wave_0_inside_enemy_status[0]++
				endif 
				
			break 
			
			case 1
				REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[2])
			
				vector player_pos
						
				player_pos = GET_ENTITY_COORDS(player_ped_id())
				
				if lk_timer(original_time, 25000)
				or player_pos.z < 37.00
				or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)
				or (is_entity_in_angled_area(player_ped_id(), <<1390.858, 3599.007, 37.980>>, <<1389.319, 3603.235, 40.680>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
				or (is_entity_in_angled_area(player_ped_id(), <<1388.832, 3604.578, 37.747>>, <<1387.122, 3609.276, 40.847>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
				or is_entity_in_angled_area(player_ped_id(), <<1400.469, 3607.616, 38.006>>, <<1399.769, 3609.490, 40.506>>, 1.2) 
				
					wave_0_inside_enemy_0_attack()
					
					wave_0_inside_enemy_status[0]++
					
				endif 
				
			break 
			
			case 2
				IF wave_0_buddy_ai_system_status >= 3
				OR NOT is_ped_inside_interior(player_ped_id(), <<1392.7360, 3602.5989, 38.5>>, "v_methlab")
					if lk_timer(attack_time, 10000) // @SBA - have this guy head up the stairs quicker (previous time = 15000)
					
						// @SBA - set rush flag and defensive area up the stairs
						//set_ped_sphere_defensive_area(wave_0_inside_enemy[0].ped, wave_0_inside_enemy[0].run_to_pos, 2.0)
						SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_inside_enemy[0].ped, tbTopBackStairsVolume.vMin, tbTopBackStairsVolume.vMax, tbTopBackStairsVolume.flWidth, TRUE)
						wave_0_inside_enemy[0].bRushInside = TRUE
						// @SBA - make more agressive
						REGISTER_TARGET(wave_0_inside_enemy[0].ped, buddy.ped)
						
						SET_ENEMY_RUSHER_ATTRIBUTES(wave_0_inside_enemy[0].ped, 1, 10, 50)
						
						attack_time = get_game_timer()
				
						wave_0_inside_enemy_status[0]++
						
					endif 
				ELSE
					// reset timer
					attack_time = get_game_timer()
				ENDIF
			
			break 
			
			case 3
			
			break 
			
		endswitch 
		
	endif 

endproc 

proc wave_0_inside_enemy_1_attack()
							
	open_sequence_task(seq)
		task_pause(null, 250)
		task_set_blocking_of_non_temporary_events(null, false)
		task_set_sphere_defensive_area(null, <<1366.0543, 3623.9988, 33.7900>>, 8.0)//2.0
		task_combat_ped(null, player_ped_id())
	close_sequence_task(seq)
	task_perform_sequence(wave_0_inside_enemy[1].ped, seq)
	clear_sequence_task(seq)
	
endproc 

proc wave_0_inside_enemy_1_system()

	if not is_ped_injured(wave_0_inside_enemy[1].ped)

		// @SBA - make blip for enemy if needed
		HANDLE_ENEMY_BLIP_CREATION(wave_0_inside_enemy[1])
		
		switch wave_0_inside_enemy_status[1]
		
			case 0
			
				if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
					if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
						
						vector player_pos
						
						player_pos = GET_ENTITY_COORDS(player_ped_id())
					
						if lk_timer(original_time, 25000)
						or player_pos.z < 37.00
						or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)
						or (is_entity_in_angled_area(player_ped_id(), <<1390.858, 3599.007, 37.980>>, <<1389.319, 3603.235, 40.680>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
						or (is_entity_in_angled_area(player_ped_id(), <<1388.832, 3604.578, 37.747>>, <<1387.122, 3609.276, 40.847>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
						or is_entity_in_angled_area(player_ped_id(), <<1400.469, 3607.616, 38.006>>, <<1399.769, 3609.490, 40.506>>, 1.2) 
							
							wave_0_inside_enemy_1_attack()
							
							wave_0_inside_enemy_status[1]++

						endif 
						
					endif 
				else 
				
					wave_0_inside_enemy_1_attack()
					
					wave_0_inside_enemy_status[1]++
					
				endif 
				
			break 
			
			case 1
			
				// @SBA - getting this guy to head for the stairs quicker
				IF lk_timer(attack_time, 15000)
				OR is_ped_injured(wave_0_inside_enemy[0].ped)
				
					
					// @SBA - set rush flag and defensive area up the stairs
					//set_ped_sphere_defensive_area(wave_0_inside_enemy[1].ped, wave_0_inside_enemy[1].run_to_pos, 2.0)
					SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_inside_enemy[1].ped, tbTopBackStairsVolume.vMin, tbTopBackStairsVolume.vMax, tbTopBackStairsVolume.flWidth, TRUE)
					wave_0_inside_enemy[1].bRushInside = TRUE
					// @SBA - make more agressive
					REGISTER_TARGET(wave_0_inside_enemy[1].ped, buddy.ped)
					
					attack_time = get_game_timer()
					SET_ENEMY_RUSHER_ATTRIBUTES(wave_0_inside_enemy[1].ped, 1, 10, 50)
					
					wave_0_inside_enemy_status[1]++
					
				endif 
			
			break 
			
			case 2
			
			break 
			
		endswitch 
	
	endif 
	
endproc 

proc wave_0_inside_enemy_2_attack()
							
	open_sequence_task(seq)
		task_pause(null, 1250)
		task_set_blocking_of_non_temporary_events(null, false)
		task_set_sphere_defensive_area(null, <<1372.5642, 3614.3271, 33.8927>>, 8.0)//2.0
		task_combat_ped(null, player_ped_id())
	close_sequence_task(seq)
	task_perform_sequence(wave_0_inside_enemy[2].ped, seq)
	clear_sequence_task(seq)
	
endproc 

proc wave_0_inside_enemy_2_system()

	if not is_ped_injured(wave_0_inside_enemy[2].ped)

		// @SBA - make blip for enemy if needed
		HANDLE_ENEMY_BLIP_CREATION(wave_0_inside_enemy[2])
		
		switch wave_0_inside_enemy_status[2]
		
			case 0
			
				if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
					if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
						
						vector player_pos
						
						player_pos = GET_ENTITY_COORDS(player_ped_id())
					
						if lk_timer(original_time, 25000)
						or player_pos.z < 37.00
						or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)
						or (is_entity_in_angled_area(player_ped_id(), <<1390.858, 3599.007, 37.980>>, <<1389.319, 3603.235, 40.680>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
						or (is_entity_in_angled_area(player_ped_id(), <<1388.832, 3604.578, 37.747>>, <<1387.122, 3609.276, 40.847>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
						or is_entity_in_angled_area(player_ped_id(), <<1400.469, 3607.616, 38.006>>, <<1399.769, 3609.490, 40.506>>, 1.2) 
			
							wave_0_inside_enemy_2_attack()
							
							wave_0_inside_enemy_status[2]++
						endif 
						
					endif 
				else 
				
					wave_0_inside_enemy_2_attack()
					
					wave_0_inside_enemy_status[2]++
					
				endif 
				
			break 
			
			case 1
			
				if lk_timer(attack_time, 10000)  // @SBA - shortened timer.  OLD = 15000
				and is_ped_injured(wave_0_inside_enemy[1].ped)
				
					// @SBA - set rush flag and defensive area up the stairs
					//set_ped_sphere_defensive_area(wave_0_inside_enemy[2].ped, wave_0_inside_enemy[2].run_to_pos, 2.0)
					SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_inside_enemy[2].ped, tbTopBackStairsVolume.vMin, tbTopBackStairsVolume.vMax, tbTopBackStairsVolume.flWidth, TRUE)
					wave_0_inside_enemy[2].bRushInside = TRUE
					// @SBA - make more agressive
					REGISTER_TARGET(wave_0_inside_enemy[2].ped, buddy.ped)
					
					attack_time = get_game_timer()
					SET_ENEMY_RUSHER_ATTRIBUTES(wave_0_inside_enemy[2].ped, 1, 0, 50)
					
					wave_0_inside_enemy_status[2]++
					
				endif 

			break 
			
			case 2

			break 
			
			
		endswitch 
	
	endif 
	
endproc 

proc wave_0_inside_enemy_3_attack()
							
	open_sequence_task(seq)
		task_pause(null, 1500)
		task_set_blocking_of_non_temporary_events(null, false)
		task_set_sphere_defensive_area(null, <<1364.3922, 3616.7683, 33.8913>>, 8.0)//2.0
		task_combat_ped(null, player_ped_id())
	close_sequence_task(seq)
	task_perform_sequence(wave_0_inside_enemy[3].ped, seq)
	clear_sequence_task(seq)
	
endproc 

proc wave_0_inside_enemy_3_system()

	if not is_ped_injured(wave_0_inside_enemy[3].ped)

		// @SBA - make blip for enemy if needed
		HANDLE_ENEMY_BLIP_CREATION(wave_0_inside_enemy[3])
		
		switch wave_0_inside_enemy_status[3]
		
			case 0
			
				if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
					if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
						
						vector player_pos
						
						player_pos = GET_ENTITY_COORDS(player_ped_id())
					
						if lk_timer(original_time, 25000)
						or player_pos.z < 37.00
						or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)
						or (is_entity_in_angled_area(player_ped_id(), <<1390.858, 3599.007, 37.980>>, <<1389.319, 3603.235, 40.680>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
						or (is_entity_in_angled_area(player_ped_id(), <<1388.832, 3604.578, 37.747>>, <<1387.122, 3609.276, 40.847>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
						or is_entity_in_angled_area(player_ped_id(), <<1400.469, 3607.616, 38.006>>, <<1399.769, 3609.490, 40.506>>, 1.2) 

							wave_0_inside_enemy_3_attack()

							wave_0_inside_enemy_status[3]++
							
						endif 
						
					endif  
					
				else 
				
					wave_0_inside_enemy_3_attack()
				
					wave_0_inside_enemy_status[3]++
					
				endif 
				
			break 
			
			case 1
			
				if lk_timer(attack_time, 15000)
				or is_ped_injured(wave_0_inside_enemy[2].ped)
				
					// @SBA - set rush flag and defensive area up the stairs
					//set_ped_sphere_defensive_area(wave_0_inside_enemy[3].ped, wave_0_inside_enemy[3].run_to_pos, 2.0)
					SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_inside_enemy[3].ped, tbTopBackStairsVolume.vMin, tbTopBackStairsVolume.vMax, tbTopBackStairsVolume.flWidth, TRUE)
					wave_0_inside_enemy[3].bRushInside = TRUE
					// @SBA - make more agressive
					REGISTER_TARGET(wave_0_inside_enemy[3].ped, buddy.ped)
					
					attack_time = get_game_timer()
					SET_ENEMY_RUSHER_ATTRIBUTES(wave_0_inside_enemy[3].ped, 1, 0, 50)
					
					wave_0_inside_enemy_status[3]++
					
				endif 
				
			break 
			
			case 2
			
			break 
			
		endswitch 
	
	endif 
	
endproc 


//if is_entity_in_angled_area(wave_2_enemy[i].ped, <<1433.399, 3636.853, 33.922>>, <<1449.164, 3595.132, 37.922>>, 58.00) 
//	
//			
func bool is_grenade_launcher_explosion_in_car_park()

	if IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADELAUNCHER, <<1424.980, 3633.671, 33.922>>, <<1436.609, 3602.895, 37.922>>, 40.9) 
		//cache original grenade launcher ammo
		//if original_ammo - current_ammo > clip size (5)
		
		return true 
	
	endif 
	
	return false
	
endfunc 

//PURPOSE:Force enemies on foot or in a vehicle to flee if the player fires grenade launcher explosion into the 
//area of the car park
proc make_enemy_flee_when_grenade_launcher_explosion_in_area()
						
//	printstring("test 0")
//	printnl()

	is_entity_in_angled_area(player_ped_id(), <<1433.399, 3636.853, 33.922>>, <<1449.164, 3595.132, 37.922>>, 58.00) 

	if wave_2_status[i] != grenade_launcher_explosion

		if is_entity_in_angled_area(wave_2_enemy[i].ped, <<1433.399, 3636.853, 33.922>>, <<1449.164, 3595.132, 37.922>>, 58.00) 

	//		printstring("test 1")
	//		printnl()

			is_entity_in_angled_area(player_ped_id(), <<1424.980, 3633.671, 32.922>>, <<1436.609, 3602.895, 37.922>>, 40.9)
			

			if IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADELAUNCHER, <<1424.980, 3633.671, 32.922>>, <<1436.609, 3602.895, 37.922>>, 40.9) 
				
				//added for bug 986050
				//fix for bug 1107853
				//if is_explosion_in_sphere(EXP_TAG_GRENADELAUNCHER, get_entity_coords(wave_2_enemy[i].ped), 20.00)
					
					if is_ped_sitting_in_any_vehicle(wave_2_enemy[i].ped)
						
						vehicle_index mission_car = GET_VEHICLE_PED_IS_IN(wave_2_enemy[i].ped)

						ped_index driver_ped = GET_PED_IN_VEHICLE_SEAT(mission_car)
							
						if wave_2_enemy[i].ped = driver_ped

							if not is_entity_dead(mission_car)
								if is_playback_going_on_for_vehicle(mission_car)
									stop_playback_recorded_vehicle(mission_car)
								endif 
								set_ped_flee_attributes(wave_2_enemy[i].ped, fa_use_vehicle, true)
								
								set_ped_flee_attributes(wave_2_enemy[i].ped, FA_DISABLE_EXIT_VEHICLE, true)   
								set_ped_flee_attributes(wave_2_enemy[i].ped, FA_DISABLE_REVERSE_IN_VEHICLE, true)
								set_ped_flee_attributes(wave_2_enemy[i].ped, FA_DISABLE_ACCELERATE_IN_VEHICLE, true)
							endif 
							
							set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, true)
							clear_ped_tasks(wave_2_enemy[i].ped)
							task_smart_flee_ped(wave_2_enemy[i].ped, player_ped_id(), 300, -1)
							
							wave_2_status[i] = grenade_launcher_explosion
							
						else 
						
							//make ped flee if the driver is alive. Otherwise have them attack. 
							if not is_ped_injured(driver_ped)
							
								set_ped_flee_attributes(wave_2_enemy[i].ped, fa_use_vehicle, true)
								set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, true)
								clear_ped_tasks(wave_2_enemy[i].ped)
								task_smart_flee_ped(wave_2_enemy[i].ped, player_ped_id(), 300, -1)
								wave_2_status[i] = grenade_launcher_explosion
								
							else 

								//fix for bug 1107853
								//allow normal ai to progress so that he will get out and shoot when the dirver is dead.
								//then when he is on foot is there is another explosion he will flee
							
							endif 

						endif 
						
					else 
					
						set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, true)
						clear_ped_tasks(wave_2_enemy[i].ped)
						task_smart_flee_ped(wave_2_enemy[i].ped, player_ped_id(), 300, -1)
						
						wave_2_status[i] = grenade_launcher_explosion

					endif 
					
//					set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, true)
//
//					clear_ped_tasks(wave_2_enemy[i].ped)
//					
//					task_smart_flee_ped(wave_2_enemy[i].ped, player_ped_id(), 300, -1)
//				
//					wave_2_status[i] = grenade_launcher_explosion
					
				//endif 
				
			endif 
			
		endif 
	endif 
		
endproc 



func bool wave_2_ai_system()

	if not wave_2_complete
		
		//if activate_wave_2

			if ped_structure_are_all_enemies_dead(wave_2_enemy)
				wave_2_complete = true  
				return true 
			endif 
			
			// @SBA - initializing local variable
			INT iWave_2_Index

			
			for i = 0 to count_of(wave_2_enemy) - 1
				
				if not is_ped_injured(wave_2_enemy[i].ped)
				
					// @SBA - make blip for enemy if needed
					HANDLE_ENEMY_BLIP_CREATION(wave_2_enemy[i])
					
					// @SBA - going to try without the fleeing and see how it is
					//make_enemy_flee_when_grenade_launcher_explosion_in_area()

					switch wave_2_status[i] 
					
						case initial_attack_phase
						
							switch i
							
								case 0
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[2].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[2].veh)
											if ((get_time_position_in_recording(wave_2_enemy_vehicle[2].veh) + 250) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[2].recording_number, "lkmethlab"))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[2].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[2])

												set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
												set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 2.0)
												task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
												// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
												//wave_2_status[i] = do_nothing
												wave_2_status[i] = run_into_shop
												
											endif 
										
										else 
											
											set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
											set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 2.0)
											task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
											// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
											//wave_2_status[i] = do_nothing
											wave_2_status[i] = run_into_shop
			
										endif 
									
									else 
										
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 2.0)
										task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
										//wave_2_status[i] = do_nothing
										wave_2_status[i] = run_into_shop
							
									endif 
								
								break 
								
								case 1
								case 2
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[2].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[2].veh)
										
											open_sequence_task(seq)
												task_pause(null, (250 * i))
												task_set_blocking_of_non_temporary_events(null, false)
												task_set_sphere_defensive_area(null, wave_2_enemy[i].run_to_pos, 4.0)
												task_combat_ped(null, player_ped_id())
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
											//wave_2_status[i] = do_nothing
											wave_2_status[i] = run_into_shop
											
										endif 
										
									else 
									
										open_sequence_task(seq)
											task_pause(null, (250 * i))
											task_set_blocking_of_non_temporary_events(null, false)
											task_set_sphere_defensive_area(null, wave_2_enemy[i].run_to_pos, 4.0)
											task_combat_ped(null, player_ped_id())
										close_sequence_task(seq)
										task_perform_sequence(wave_2_enemy[i].ped, seq)
										clear_sequence_task(seq)
										
										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
										//wave_2_status[i] = do_nothing
										wave_2_status[i] = run_into_shop
									
									endif 
								
								break 
								
								
								case 3
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[2].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[2].veh)
										
											open_sequence_task(seq)
												task_pause(null, (250 * i))
												task_set_blocking_of_non_temporary_events(null, false)
												task_set_sphere_defensive_area(null, wave_2_enemy[i].run_to_pos, 4.0) // @SBA - expanding sphere. OLD = 2.0
												task_combat_ped(null, player_ped_id())
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											wave_2_status[i] = attack_from_pos_2
											
										endif 
										
									else 
									
										open_sequence_task(seq)
											task_pause(null, (250 * i))
											task_set_blocking_of_non_temporary_events(null, false)
											task_set_sphere_defensive_area(null, wave_2_enemy[i].run_to_pos, 4.0) // @SBA - expanding sphere. OLD = 2.0
											task_combat_ped(null, player_ped_id())
										close_sequence_task(seq)
										task_perform_sequence(wave_2_enemy[i].ped, seq)
										clear_sequence_task(seq)
										
										wave_2_status[i] = attack_from_pos_2
									
									endif 

								break 

								case 4
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[3].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[3].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[3].recording_number, "lkmethlab") - 250) < (get_time_position_in_recording(wave_2_enemy_vehicle[3].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[3].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[3])
												
//												SET_PED_COMBAT_ATTRIBUTES(wave_2_enemy[i].ped, ca_use_vehicle, false)
//												SET_PED_COMBAT_ATTRIBUTES(wave_2_enemy[i].ped, CA_LEAVE_VEHICLES, true)
												
//												set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
//												set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
//												task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
												//wave_2_status[i] = do_nothing
												
												// @SBA - have enemy exit vehicle and run to the back stairs
												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[3].veh, ECF_USE_RIGHT_ENTRY)
													TASK_SWAP_WEAPON(NULL, TRUE)
													//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, wave_2_enemy[i].run_to_pos, pedmove_run, -1)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
													TASK_SET_SPHERE_DEFENSIVE_AREA(null, wave_2_enemy[i].run_to_pos, 4.0)
													task_combat_ped(null, player_ped_id())
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												// @SBA - enemies will try to rush inside, so setting this as the next stage
//												wave_2_status[i] = get_into_pos
												wave_2_status[i] = run_into_shop
										
							
											endif 
										
										else 
											
											set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
											set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
											task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
//											// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
//											//wave_2_status[i] = do_nothing
//											wave_2_status[i] = run_into_shop

											// @SBA - have enemy exit vehicle and run to the back stairs
//											open_sequence_task(seq)
//												// @SBA - replace this with a TASK_FOLLOW_NAV_MESH_TO_COORD once T-posing is fixed
//												//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
//												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, wave_2_enemy[i].run_to_pos, pedmove_run, -1)
//											close_sequence_task(seq)
//											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											// @SBA - enemies will try to rush inside, so setting this as the next stage
											wave_2_status[i] = run_into_shop
												
										endif 
									
									else 
										
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
										task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
//										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
//										//wave_2_status[i] = do_nothing
//										wave_2_status[i] = run_into_shop

										// @SBA - have enemy exit vehicle and run to the back stairs
//										open_sequence_task(seq)
//											// @SBA - replace this with a TASK_FOLLOW_NAV_MESH_TO_COORD once T-posing is fixed
//											//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
//											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, wave_2_enemy[i].run_to_pos, pedmove_run, -1)
//										close_sequence_task(seq)
//										task_perform_sequence(wave_2_enemy[i].ped, seq)
//										clear_sequence_task(seq)
										// @SBA - enemies will try to rush inside, so setting this as the next stage
										wave_2_status[i] = run_into_shop
											
									endif 
									
								break 

								case 5

									if is_vehicle_driveable(wave_2_enemy_vehicle[4].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[4].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[4].recording_number, "lkmethlab") - 500) < (get_time_position_in_recording(wave_2_enemy_vehicle[4].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[4].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[4])
											
												// @SBA - have enemy exit vehicle and run to the back stairs
												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[4].veh, ECF_USE_RIGHT_ENTRY)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
													task_set_blocking_of_non_temporary_events(null, false)
													TASK_SET_SPHERE_DEFENSIVE_AREA(null, wave_2_enemy[i].run_to_pos, 4.0)
													task_combat_ped(null, player_ped_id())
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
	
												// @SBA - enemies will try to rush inside, so setting this as the next stage
												//wave_2_status[i] = do_nothing
												wave_2_status[i] = run_into_shop
		
											endif 
										
										else 
											
											set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
											set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 4.0)
											task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
											// @SBA - have enemy exit vehicle and run to the back stairs
//											open_sequence_task(seq)
//												// @SBA - replace this with a TASK_FOLLOW_NAV_MESH_TO_COORD once T-posing is fixed
//												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
//											close_sequence_task(seq)
//											task_perform_sequence(wave_2_enemy[i].ped, seq)
//											clear_sequence_task(seq)
										
											// @SBA - enemies will try to rush inside, so setting this as the next stage
											//wave_2_status[i] = do_nothing
											wave_2_status[i] = run_into_shop
							
										endif 
									
									else 
										
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 4.0)
										task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
										// @SBA - have enemy exit vehicle and run to the back stairs
//										open_sequence_task(seq)
//											// @SBA - replace this with a TASK_FOLLOW_NAV_MESH_TO_COORD once T-posing is fixed
//											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
//										close_sequence_task(seq)
//										task_perform_sequence(wave_2_enemy[i].ped, seq)
//										clear_sequence_task(seq)
										
										// @SBA - enemies will try to rush inside, so setting this as the next stage
										//wave_2_status[i] = do_nothing
										wave_2_status[i] = run_into_shop
							
									endif 
									
								break 
								
								case 6
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[0].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[0].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[0].recording_number, "lkmethlab") - 650) < (get_time_position_in_recording(wave_2_enemy_vehicle[0].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[0].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[0])

												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[0].veh, ecf_dont_close_door)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												
												wave_2_status[i] = get_into_pos
												
											else 
											
//												if is_entity_in_angled_area(wave_2_enemy[i].ped, <<1433.399, 3636.853, 33.922>>, <<1449.164, 3595.132, 37.922>>, 58.00) 
//
//													if is_grenade_launcher_explosion_in_car_park()
//													
//													endif 
//													
//												endif 
											
											endif 
										else 
											wave_2_status[i] = get_into_pos
										endif 
									
									else 
										wave_2_status[i] = get_into_pos	
									endif 
									
								break 
							
								case 7

									if is_vehicle_driveable(wave_2_enemy_vehicle[0].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[0].veh)
											
											open_sequence_task(seq)
												task_pause(null, 250)
												task_leave_vehicle(null, wave_2_enemy_vehicle[0].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
										
											wave_2_status[i] = get_into_pos
											
										endif 
									else 
									
										
										wave_2_status[i] = get_into_pos
									endif 
									
								break
								
								case 8

									if is_vehicle_driveable(wave_2_enemy_vehicle[1].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[1].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[1].recording_number, "lkmethlab") - 1000) < (get_time_position_in_recording(wave_2_enemy_vehicle[1].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[1].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[1])

												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[1].veh, ecf_dont_close_door)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												
												wave_2_status[i] = get_into_pos
											endif 
										else 
											wave_2_status[i] = get_into_pos
										endif 
									
									else 
										wave_2_status[i] = get_into_pos	
									endif 
									
								break 
							
								case 9

									if is_vehicle_driveable(wave_2_enemy_vehicle[1].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[1].veh)
											
											open_sequence_task(seq)
												task_pause(null, 250)
												task_leave_vehicle(null, wave_2_enemy_vehicle[1].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											wave_2_status[i] = get_into_pos
											
										endif 
									else 
										wave_2_status[i] = get_into_pos
									endif 
									
								break
								
								case 10

									if is_vehicle_driveable(wave_2_enemy_vehicle[2].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[2].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[2].recording_number, "lkmethlab") - 1000) < (get_time_position_in_recording(wave_2_enemy_vehicle[2].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[2].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[2])

												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[2].veh, ecf_dont_close_door)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												
												wave_2_status[i] = get_into_pos
											endif 
										else 
											wave_2_status[i] = get_into_pos
										endif 
									
									else 
										wave_2_status[i] = get_into_pos	
									endif 
									
								break 
								
								case 11
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[2].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[2].veh)
											
											open_sequence_task(seq)
												task_pause(null, 250)
												task_leave_vehicle(null, wave_2_enemy_vehicle[2].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											wave_2_status[i] = get_into_pos
											
										endif 
									else 
										wave_2_status[i] = get_into_pos
									endif 
								
								break
								
								
								
								case 12

									if is_vehicle_driveable(wave_2_enemy_vehicle[5].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[5].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[5].recording_number, "lkmethlab") - 500) < (get_time_position_in_recording(wave_2_enemy_vehicle[5].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[5].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[5])

												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[5].veh, ecf_dont_close_door | ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												
												wave_2_status[i] = get_into_pos
											endif 
										else 
											wave_2_status[i] = get_into_pos
										endif 
									
									else 
										wave_2_status[i] = get_into_pos	
									endif 
									
								break 
								
								case 13
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[5].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[5].veh)
											
											open_sequence_task(seq)
												task_pause(null, 250)
												task_leave_vehicle(null, wave_2_enemy_vehicle[5].veh, ecf_dont_close_door | ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											
											wave_2_status[i] = get_into_pos
											
										endif 
									else 
										wave_2_status[i] = get_into_pos
									endif 
								
								break
								
								
								case 14

									if is_vehicle_driveable(wave_2_enemy_vehicle[6].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[6].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[6].recording_number, "lkmethlab") - 500) < (get_time_position_in_recording(wave_2_enemy_vehicle[6].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[6].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[6])

												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[6].veh, ecf_dont_close_door)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												
												wave_2_status[i] = get_into_pos
											endif 
										else 
											wave_2_status[i] = get_into_pos
										endif 
									
									else 
										wave_2_status[i] = get_into_pos	
									endif 
									
								break 
								
								case 15
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[6].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[6].veh)
											
											open_sequence_task(seq)
												task_pause(null, 250)
												task_leave_vehicle(null, wave_2_enemy_vehicle[6].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											
											wave_2_status[i] = get_into_pos
											
										endif 
									else 
										wave_2_status[i] = get_into_pos
									endif 
								
								break
								
								
								case 16
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[7].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[7].veh)
											if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[7].recording_number, "lkmethlab") - 2000) < (get_time_position_in_recording(wave_2_enemy_vehicle[7].veh))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[7].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[7])

												open_sequence_task(seq)
													task_leave_vehicle(null, wave_2_enemy_vehicle[7].veh, ecf_dont_close_door)
													TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
												close_sequence_task(seq)
												task_perform_sequence(wave_2_enemy[i].ped, seq)
												clear_sequence_task(seq)
												
												wave_2_status[i] = get_into_pos
											endif 
										else 
											wave_2_status[i] = get_into_pos
										endif 
									
									else 
										wave_2_status[i] = get_into_pos	
									endif 
									
								break 
							
								case 17

									if is_vehicle_driveable(wave_2_enemy_vehicle[7].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[7].veh)
											
											open_sequence_task(seq)
												task_pause(null, 250)
												task_leave_vehicle(null, wave_2_enemy_vehicle[7].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											wave_2_status[i] = get_into_pos
											
										endif 
									else 
										wave_2_status[i] = get_into_pos
									endif 
	
								
								break
								
								case 18
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[6].veh)
										if is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[6].veh)
											if ((get_time_position_in_recording(wave_2_enemy_vehicle[6].veh) + 250) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_2_enemy_vehicle[6].recording_number, "lkmethlab"))
											
												stop_playback_recorded_vehicle(wave_2_enemy_vehicle[6].veh)
												REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[6])
												
												set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
												set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
												task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
												// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
												//wave_2_status[i] = do_nothing
												wave_2_status[i] = run_into_shop
							
											endif 
										
										else 
											
											set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
											set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
											task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
											// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
											//wave_2_status[i] = do_nothing
											wave_2_status[i] = run_into_shop
			
										endif 
									
									else 
										
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
										task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
										//wave_2_status[i] = do_nothing
										wave_2_status[i] = run_into_shop
							
									endif 
												
								break
								
								case 19
								
									if is_vehicle_driveable(wave_2_enemy_vehicle[6].veh)
										if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[6].veh)
										
											open_sequence_task(seq)
												task_pause(null, 250)
												task_set_blocking_of_non_temporary_events(null, false)
												task_set_sphere_defensive_area(null, wave_2_enemy[i].run_to_pos, 3.0) // @SBA - expanding sphere. OLD = 2.0
												task_combat_ped(null, player_ped_id())
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											wave_2_status[i] = attack_from_pos_2
											
										endif 
										
									else 
									
										open_sequence_task(seq)
											task_pause(null, 250)
											task_set_blocking_of_non_temporary_events(null, false)
											task_set_sphere_defensive_area(null, wave_2_enemy[i].run_to_pos, 3.0) // @SBA - expanding sphere. OLD = 2.0
											task_combat_ped(null, player_ped_id())
										close_sequence_task(seq)
										task_perform_sequence(wave_2_enemy[i].ped, seq)
										clear_sequence_task(seq)
										
										wave_2_status[i] = attack_from_pos_2
									
									endif 
								
								break 

								
								case 20
								
									SET_PED_COMBAT_ATTRIBUTES(wave_2_enemy[i].ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
									set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 2.0)
									task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
									wave_2_status[i] = attack_from_pos_2
								
								break 
								

							endswitch 
							
							// @SBA - set intial wait time before the first enemy rushes (if not set yet)
							IF iWaitTimeBeforeRush != 50000
								iWaitTimeBeforeRush = 50000
								// @SBA - start rush timer
								iWave_Rush_Time = GET_GAME_TIMER()
							ENDIF
							
						break 
						
						case get_into_pos
					
							switch i 
							
								//wave 2 second wave car enemies
								case 6
								case 7 
								case 8 
								case 9 
								case 10 
								case 11
								case 12
								case 13
								case 14
								case 15
								case 16
								case 17
						
									if not IS_ENTITY_AT_COORD(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, <<1.0, 1.0, 1.6>>, false, true)
										if has_ped_task_finished_2(wave_2_enemy[i].ped, script_task_perform_sequence)
											
											set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, true)
											clear_ped_tasks(wave_2_enemy[i].ped)
								
											open_sequence_task(seq)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_2_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_2_enemy[i].ped, seq)
											clear_sequence_task(seq)
										endif 
									
									else 
										
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										// @SBA - give enemy a wider range for the AI to use
										//set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0)
										SET_PED_ANGLED_DEFENSIVE_AREA(wave_2_enemy[i].ped, tbBackYardVolume.vMin, tbBackYardVolume.vMax, tbBackYardVolume.flWidth)
										task_combat_ped(wave_2_enemy[i].ped, player_ped_id())
										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
										//wave_2_status[i] = do_nothing
										wave_2_status[i] = run_into_shop
										
									endif 
									
								break 

								
							endswitch 
							
						break 
						
						case attack_from_pos_2
						
							switch i 
									
								case 3
								
									if is_ped_injured(wave_2_enemy[1].ped)
									
										wave_2_enemy[i].run_to_pos = wave_2_enemy[1].run_to_pos
								
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 4.0) // @SBA - trying a larger sphere.  OLD VALUE = 1.0
										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
										//wave_2_status[i] = do_nothing
										wave_2_status[i] = run_into_shop
									
									// @SBA - commenting this out one since enemy 5 is an early rusher
//									elif is_ped_injured(wave_2_enemy[5].ped)
//									
//										wave_2_enemy[i].run_to_pos = wave_2_enemy[5].run_to_pos
//								
//										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
//										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0) // @SBA - trying a larger sphere.  OLD VALUE = 1.0
//										// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
//										//wave_2_status[i] = do_nothing
//										wave_2_status[i] = run_into_shop
									
									endif 
									
								break 
							
								case 19
										
									if is_ped_injured(wave_2_enemy[2].ped)
									
										wave_2_enemy[i].run_to_pos = wave_2_enemy[2].run_to_pos
								
										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 4.0) // @SBA - trying a larger sphere.  OLD VALUE = 1.0
										wave_2_status[i] = run_into_shop
										
									// @SBA - commenting this out one since enemy 4 is an early rusher
//									elif is_ped_injured(wave_2_enemy[4].ped)
//									
//										wave_2_enemy[i].run_to_pos = wave_2_enemy[4].run_to_pos
//								
//										set_blocking_of_non_temporary_events(wave_2_enemy[i].ped, false)
//										set_ped_sphere_defensive_area(wave_2_enemy[i].ped, wave_2_enemy[i].run_to_pos, 3.0) // @SBA - trying a larger sphere.  OLD VALUE = 1.0
//										wave_2_status[i] = do_nothing
										
									endif 
								
								break 
								
								
							endswitch 
							
						break
						
						// @SBA - adding this case for the rush inside
						CASE run_into_shop
							INT iRushersAtOnce
							iRushersAtOnce = 1
							IF iWaveRushersSoFar > 3
								iRushersAtOnce = 2
							ENDIF
						
							// @SBA - allow a new rush only when the greneade launcher cutscene isn't playing
							IF NOT grenade_launcher_cutscene_playing
								// @SBA - check the rush timer
								IF lk_timer(iWave_Rush_Time, iWaitTimeBeforeRush)
								
									// @SBA - When the wait timer is ready, check if we want a new rusher
									IF ARE_WE_READY_FOR_A_NEW_RUSHER(wave_2_enemy, iRushersAtOnce)
										// @SBA - get the index of the next valid rushers
										iWave_2_Index = GET_NEXT_WAVE_2_INDEX()
										IF iWave_2_Index != -1
											CPRINTLN(DEBUG_MISSION,"*** Sending in a Wave 2 enemy to rush.  Index = ", iWave_2_Index)
											// @SBA - set defensive area on the roof to move the enemy there
											SET_PED_ANGLED_DEFENSIVE_AREA(wave_2_enemy[iWave_2_Index].ped, tbBackRoofVolume.vMin, tbBackRoofVolume.vMax, tbBackRoofVolume.flWidth)
											// @SBA - Reset this so buddy can shoot these guys
											SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(wave_2_enemy[iWave_2_Index].ped, FALSE)
											
											// @SBA - set a new, random wait time then restart rush timer
											iWaitTimeBeforeRush = GET_RANDOM_INT_IN_RANGE(7999, 9999)
											iWave_Rush_Time = GET_GAME_TIMER()
											
											SET_ENEMY_RUSHER_ATTRIBUTES(wave_2_enemy[iWave_2_Index].ped, 1, 0, 50)
											
											iWaveRushersSoFar++
											
											// @SBA - set new state
											wave_2_status[iWave_2_Index] = combat_player
										ELSE
											// @SBA - If we didn't find a valid enemy it could be because no one was in the right state yet, 
											// so reset the clock to try again
											//CPRINTLN(DEBUG_MISSION,"*** Found no valid Wave 2 enemy to rush.  Reset the timer to try again later.")
											iWaitTimeBeforeRush = 10000
											iWave_Rush_Time = GET_GAME_TIMER()
										ENDIF
									ELSE
										// @SBA - Not ready for a new rusher (max number still in play).
										//CPRINTLN(DEBUG_MISSION,"*** Don't need a new rusher yet.  Reset the timer to try again later.")
										iWaitTimeBeforeRush =  GET_RANDOM_INT_IN_RANGE(7000, 9999)
										iWave_Rush_Time = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ELSE
								// ** Not sure we'll need this now, commenting out
								// @SBA - when the greneade launcher cutscene starts reset the wait time
								// this gives more time for the next enemies to arrive
	//							IF iWaitTimeBeforeRush != 22500
	//								iWaitTimeBeforeRush = 22500
	//							ENDIF
							ENDIF
							
						BREAK

						// @SBA - adding this case for the rush inside
						CASE combat_player
							// Rushers will now have pistols - does enemy rusher need one?
							IF wave_2_enemy[i].weapon != WEAPONTYPE_PISTOL
								IF IS_PED_INSIDE_INTERIOR(wave_2_enemy[i].ped, <<1392.7360, 3602.5989, 38.5>>, "v_methlab")
									// checking again for safety
									IF GET_CURRENT_PED_WEAPON(wave_2_enemy[i].ped, wave_2_enemy[i].weapon)
										IF wave_2_enemy[i].weapon != WEAPONTYPE_PISTOL
											GIVE_WEAPON_TO_PED(wave_2_enemy[i].ped, WEAPONTYPE_PISTOL, INFINITE_AMMO)
											OPEN_SEQUENCE_TASK(seq)
												TASK_SWAP_WEAPON(NULL, TRUE)
												TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(wave_2_enemy[i].ped, seq)
											CLEAR_SEQUENCE_TASK(seq)
											SET_PED_CAN_SWITCH_WEAPON(wave_2_enemy[i].ped, FALSE)
											wave_2_enemy[i].weapon = WEAPONTYPE_PISTOL
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							// @SBA - if enemy has reached position, let him fight there
							IF IS_ENTITY_IN_TRIGGER_BOX(wave_2_enemy[i].ped, tbBackRoofVolume)
								REGISTER_TARGET(wave_2_enemy[i].ped, buddy.ped)
								//TASK_COMBAT_HATED_TARGETS_AROUND_PED(wave_2_enemy[i].ped, 20.0)
								// @SBA - send this enemy's status to the bone yard
								wave_2_status[i] = do_nothing
							ENDIF
						BREAK
					
						case grenade_launcher_explosion
						
							switch i 
						
								case 0
								case 1
								case 2
								case 3
								case 4
								case 5
								case 6
								case 7 
								case 8 
								case 9 
								case 10 
								case 11
								case 12
								case 13
								case 14
								case 15
								case 16
								case 17
								case 18
								case 19
								
									if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(wave_2_enemy[i].ped)) > 70
								
										remove_blip(wave_2_enemy[i].blip)
										
										set_ped_as_no_longer_needed(wave_2_enemy[i].ped)
										
									endif 

								break 
								
							endswitch 
						
						break 
						
						case do_nothing
						
							switch i 
						
								case 0
								case 1
								case 2
								case 3
								case 4
								case 5
								case 6
								case 7 
								case 8 
								case 9 
								case 10 
								case 11
								case 12
								case 13
								case 14
								case 15
								case 16
								case 17
								case 18
								case 19
								
								break 
								
							endswitch 

						break 
						
					endswitch 
					
				endif 
				
			endfor 
			
			
		//endif 
		
	else 
	
		return true 
		
	endif 

	return false 

endfunc

func bool remaining_enemy_0_system()

	if not is_ped_injured(remaining_enemy[0])

		switch remaining_enemy_status[0]
		
			case 0
			
				set_blocking_of_non_temporary_events(remaining_enemy[0], true)
				clear_ped_tasks(remaining_enemy[0])
		
				open_sequence_task(seq)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1395.7225, 3600.0715, 33.9803>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
				close_sequence_task(seq)
				task_perform_sequence(remaining_enemy[0], seq)
				clear_sequence_task(seq)
					
				remaining_enemy_status[0]++
			
			break 
			
			case 1
			
				if not IS_ENTITY_AT_COORD(remaining_enemy[0], <<1395.7225, 3600.0715, 33.9803>>, <<1.0, 1.0, 1.6>>, false, true)
					if has_ped_task_finished_2(remaining_enemy[0], script_task_perform_sequence)
						
						set_blocking_of_non_temporary_events(remaining_enemy[0], true)
						clear_ped_tasks(remaining_enemy[0])
			
						open_sequence_task(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1395.7225, 3600.0715, 33.9803>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
						close_sequence_task(seq)
						task_perform_sequence(remaining_enemy[0], seq)
						clear_sequence_task(seq)
					endif 
				
				else 
				
					set_blocking_of_non_temporary_events(remaining_enemy[0], false)
					set_ped_sphere_defensive_area(remaining_enemy[0], <<1395.7225, 3600.0715, 33.9803>>, 1.0)
					task_combat_ped(remaining_enemy[0], player_ped_id())
					remaining_enemy_status[0]++
				
				endif 
			
			break 
			
			case 2
			
				if is_ped_injured(wave_3_enemy[1].ped)
					
					set_blocking_of_non_temporary_events(remaining_enemy[0], true)
					clear_ped_tasks(remaining_enemy[0])
			
					open_sequence_task(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1394.5330, 3605.6438, 33.9809>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
					close_sequence_task(seq)
					task_perform_sequence(remaining_enemy[0], seq)
					clear_sequence_task(seq)
					
					remaining_enemy_status[0]++
					
				endif 

			break 
			
			case 3
			
				if not IS_ENTITY_AT_COORD(remaining_enemy[0], <<1394.5330, 3605.6438, 33.9809>>, <<1.0, 1.0, 1.6>>, false, true)
					if has_ped_task_finished_2(remaining_enemy[0], script_task_perform_sequence)
						
						set_blocking_of_non_temporary_events(remaining_enemy[0], true)
						clear_ped_tasks(remaining_enemy[0])
			
						open_sequence_task(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1394.5330, 3605.6438, 33.9809>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
						close_sequence_task(seq)
						task_perform_sequence(remaining_enemy[0], seq)
						clear_sequence_task(seq)
					endif 
				
				else 
				
					set_blocking_of_non_temporary_events(remaining_enemy[0], false)
					set_ped_sphere_defensive_area(remaining_enemy[0], <<1394.5330, 3605.6438, 33.9809>>, 2.0)
					task_combat_ped(remaining_enemy[0], player_ped_id())
					remaining_enemy_status[0]++
				
				endif 
			
			break 
			
			case 4
			
			break 
			
		endswitch 
		
	else 
	
		return true 
	
	endif 
	
	return false 

endfunc

func bool remaining_enemy_1_system()

	if not is_ped_injured(remaining_enemy[1])

		switch remaining_enemy_status[1]
		
			case 0
			
				set_blocking_of_non_temporary_events(remaining_enemy[1], true)
				clear_ped_tasks(remaining_enemy[1])
		
				open_sequence_task(seq)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1392.7656, 3598.7432, 33.9803>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
				close_sequence_task(seq)
				task_perform_sequence(remaining_enemy[1], seq)
				clear_sequence_task(seq)
				
				remaining_enemy_status[1]++
			
			break 
			
			case 1
			
				if not IS_ENTITY_AT_COORD(remaining_enemy[1], <<1392.7656, 3598.7432, 33.9803>>, <<1.0, 1.0, 1.6>>, false, true)
					if has_ped_task_finished_2(remaining_enemy[1], script_task_perform_sequence)
						
						set_blocking_of_non_temporary_events(remaining_enemy[1], true)
						clear_ped_tasks(remaining_enemy[1])
			
						open_sequence_task(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1392.7656, 3598.7432, 33.9803>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
						close_sequence_task(seq)
						task_perform_sequence(remaining_enemy[1], seq)
						clear_sequence_task(seq)
					endif 
				
				else 
				
					set_blocking_of_non_temporary_events(remaining_enemy[1], false)
					set_ped_sphere_defensive_area(remaining_enemy[1], <<1392.7656, 3598.7432, 33.9803>>, 1.0)
					task_combat_ped(remaining_enemy[1], player_ped_id())
					remaining_enemy_status[1]++
				
				endif 
				
			break 
			
			case 2
			
				if is_ped_injured(wave_3_enemy[2].ped)
					
					set_blocking_of_non_temporary_events(remaining_enemy[1], true)
					clear_ped_tasks(remaining_enemy[1])
			
					open_sequence_task(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1396.5658, 3602.8413, 33.9809>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
					close_sequence_task(seq)
					task_perform_sequence(remaining_enemy[1], seq)
					clear_sequence_task(seq)
					
					remaining_enemy_status[1]++
					
				endif 

			break 
			
			case 3
			
				if not IS_ENTITY_AT_COORD(remaining_enemy[1], <<1396.5658, 3602.8413, 33.9809>>, <<1.0, 1.0, 1.6>>, false, true)
					if has_ped_task_finished_2(remaining_enemy[1], script_task_perform_sequence)
						
						set_blocking_of_non_temporary_events(remaining_enemy[1], true)
						clear_ped_tasks(remaining_enemy[1])
			
						open_sequence_task(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1396.5658, 3602.8413, 33.9809>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
						close_sequence_task(seq)
						task_perform_sequence(remaining_enemy[1], seq)
						clear_sequence_task(seq)
					endif 
				
				else 
				
					set_blocking_of_non_temporary_events(remaining_enemy[1], false)
					set_ped_sphere_defensive_area(remaining_enemy[1], <<1396.5658, 3602.8413, 33.9809>>, 2.0)
					task_combat_ped(remaining_enemy[1], player_ped_id())
					remaining_enemy_status[1]++
				
				endif 

			break 
			
			case 4
			
			break 
			
		endswitch 
		
	else 
	
		return true 
		
	endif 
	
	return false 

endfunc 

func bool wave_3_ai_system()

	if not wave_3_complete
		
		//if activate_wave_3

		if ped_structure_are_all_enemies_dead(wave_3_enemy) //and remaining_enemy_0_system() and remaining_enemy_1_system()
			wave_3_complete = true  
			return true 
		endif 

		// @SBA - initializing local variable
		INT iWave_3_Index
			
		for i = 0 to count_of(wave_3_enemy) - 1
			
			if not is_ped_injured(wave_3_enemy[i].ped)

				// @SBA - make blip for enemy if needed
				HANDLE_ENEMY_BLIP_CREATION(wave_3_enemy[i])
					
				switch wave_3_status[i] 
				
					case initial_attack_phase
					
						switch i 
						
							case 0
							
								if is_vehicle_driveable(wave_3_enemy_vehicle[0].veh)
									if is_playback_going_on_for_vehicle(wave_3_enemy_vehicle[0].veh)
										if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_3_enemy_vehicle[0].recording_number, "lkmethlab") - 1000) < (get_time_position_in_recording(wave_3_enemy_vehicle[0].veh))
										
											stop_playback_recorded_vehicle(wave_3_enemy_vehicle[0].veh)
											REMOVE_VEHICLE_BLIP(wave_3_enemy_vehicle[0])

											open_sequence_task(seq)
												task_leave_vehicle(null, wave_3_enemy_vehicle[0].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_3_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_3_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											wave_3_status[i] = get_into_pos
										endif 
									endif 
								
								else 
									
									wave_3_status[i] = get_into_pos
									
								endif 
								
							break 
						
							case 1
							case 2
							case 3

								if is_vehicle_driveable(wave_3_enemy_vehicle[0].veh)
									if not is_playback_going_on_for_vehicle(wave_3_enemy_vehicle[0].veh)
										
										player_position = GET_ENTITY_COORDS(player_ped_id()) 
										
										if (player_position.z < 37.00)
										
											open_sequence_task(seq)
												task_pause(null, (250 * i))
												task_leave_vehicle(null, wave_3_enemy_vehicle[0].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_3_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_3_enemy[i].ped, seq)
											clear_sequence_task(seq)
											wave_3_status[i] = get_into_pos
										endif 
										
									endif 
									
								else 

									wave_3_status[i] = get_into_pos
									
								endif 
								
							break

						endswitch 
					
					break 

					case get_into_pos
					
						switch i 
							
							case 0
							case 1 
							case 2 

								if not IS_ENTITY_AT_COORD(wave_3_enemy[i].ped, wave_3_enemy[i].run_to_pos, <<1.0, 1.0, 1.6>>, false, true)
									if has_ped_task_finished_2(wave_3_enemy[i].ped, script_task_perform_sequence)
										
										set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, true)
										clear_ped_tasks(wave_3_enemy[i].ped)
							
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_3_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_3_enemy[i].ped, seq)
										clear_sequence_task(seq)
									endif 
								
								else 
									
									// @SBA - Start rushing timer (make sure it gives enough time to get enemies into position)
									IF iWaitTimeBeforeRush != 2000
										iWaitTimeBeforeRush = 2000
										iWave_Rush_Time = GET_GAME_TIMER()
									ENDIF
									
									set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_3_enemy[i].ped, wave_3_enemy[i].run_to_pos, 1.0)
									task_combat_ped(wave_3_enemy[i].ped, player_ped_id())
									// @SBA - they'll all get a chance to rush, so goto run_into_shop
									//wave_3_status[i] = do_nothing
									wave_3_status[i] = run_into_shop
								endif 
								
							break 
							
							case 3
							
								if not IS_ENTITY_AT_COORD(wave_3_enemy[i].ped, wave_3_enemy[i].run_to_pos, <<1.0, 1.05, 1.6>>, false, true)
									if has_ped_task_finished_2(wave_3_enemy[i].ped, script_task_perform_sequence)
										
										set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, true)
										clear_ped_tasks(wave_3_enemy[i].ped)
							
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_3_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_3_enemy[i].ped, seq)
										clear_sequence_task(seq)
									endif 
								
								else 

									
									set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_3_enemy[i].ped, wave_3_enemy[i].run_to_pos, 3.0)
									task_combat_ped(wave_3_enemy[i].ped, player_ped_id())
									wave_3_status[i] = run_into_shop
									
								endif 
							
							break 

						endswitch 

					break
					
					case run_into_shop
						
						// @SBA - have all 4 enemies eventually run in (not just enemy 3)
//						switch i 
//						
//							case 3
//					
//								if is_ped_injured(wave_3_enemy[1].ped)
//								
//									set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, true)
//									clear_ped_tasks(wave_3_enemy[i].ped)
//									
//									wave_3_enemy[i].run_to_pos = wave_3_enemy[1].run_to_pos
//								
//									open_sequence_task(seq)
//										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_3_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
//									close_sequence_task(seq)
//									task_perform_sequence(wave_3_enemy[i].ped, seq)
//									clear_sequence_task(seq)
//									
//									wave_3_status[i] = get_into_shop
//									
//								endif 
//								
//							break 
//							
//						endswitch 		

						// @SBA - allow a rush only when the player is inside
//						IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1400.846, 3602.507, 28.946>>, <<1394.636, 3619.827, 48.946>>, 23.1)      
//							player_position = GET_ENTITY_COORDS(player_ped_id()) 
//							// @SBA - make sure player isn't still upstairs
//							IF (player_position.z < 37.00)
						
								// @SBA - When the wait timer is ready, send a chosen enemy inside
								IF lk_timer(iWave_Rush_Time, iWaitTimeBeforeRush)
									iWave_3_Index = GET_NEXT_WAVE_3_INDEX()
									IF iWave_3_Index != -1
										IF wave_3_status[iWave_3_Index] = run_into_shop
											CPRINTLN(DEBUG_MISSION,"*** Wave 3 rushing enemy is: ", iWave_3_Index)
											// @SBA - set defensive area on the roof to move the enemy there
											SET_PED_ANGLED_DEFENSIVE_AREA(wave_3_enemy[iWave_3_Index].ped, tbLiquorStoreVolume.vMin, tbLiquorStoreVolume.vMax, tbLiquorStoreVolume.flWidth)
											// @SBA - set a new, random wait time then restart rush timer
											iWaitTimeBeforeRush = GET_RANDOM_WAIT_TIME_FOR_WAVE_3()
											iWave_Rush_Time = GET_GAME_TIMER()
											// @SBA - have this enemy's status make like the US Congress
											wave_3_status[iWave_3_Index] = do_nothing
										ELSE
											// @SBA - If the enemy isn't in the proper state yet (due to short initial timer), reset the flag and clock to try again
											IF wave_3_enemy[iWave_3_Index].bRushInside
												//CPRINTLN(DEBUG_MISSION,"*** Wave 3 rushing enemy isn't in proper state.  Reset for enemy: ", iWave_3_Index)
												iWaitTimeBeforeRush = 1500
												wave_3_enemy[iWave_3_Index].bRushInside = FALSE
												iWave_Rush_Time = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
//							ELSE
//								// @SBA - If the player goes back upstairs, reset the rush timer
//								IF iWaitTimeBeforeRush != 17500
//									iWaitTimeBeforeRush = 17500
//								ENDIF
//							ENDIF
//						ELSE
//							// @SBA - If the player leaves the liquor store, reset the rush timer
//							IF iWaitTimeBeforeRush != 17500
//								iWaitTimeBeforeRush = 17500
//							ENDIF
//						ENDIF

					break 
					
					case get_into_shop
					
						switch i 
						
							case 3
					
								if not IS_ENTITY_AT_COORD(wave_3_enemy[i].ped, wave_3_enemy[i].run_to_pos, <<1.0, 1.0, 1.6>>, false, true)
									if has_ped_task_finished_2(wave_3_enemy[i].ped, script_task_perform_sequence)
										
										set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, true)
										clear_ped_tasks(wave_3_enemy[i].ped)
							
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_3_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_3_enemy[i].ped, seq)
										clear_sequence_task(seq)
									endif 
								
								else 
									
									set_blocking_of_non_temporary_events(wave_3_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_3_enemy[i].ped, wave_3_enemy[i].run_to_pos, 1.0)
									task_combat_ped(wave_3_enemy[i].ped, player_ped_id())
									wave_3_status[i] = do_nothing
									
								endif 
								
							break 
							
						endswitch 
					
					break 
					
					case do_nothing 
					
					break 
					
				endswitch 
				
			endif 
			
		endfor 

		//endif 
		
	else 
	
		return true 
		
	endif 

	return false 

endfunc


func bool ambient_enemy_system()

	if ped_structure_are_all_enemies_dead(ambient_enemy)
		return true 
	endif 
			
	for i = 0 to count_of(ambient_enemy) - 1
		
		if not is_ped_injured(ambient_enemy[i].ped)
		
			switch ambient_enemy_status[i] 
			
				case initial_attack_phase
				
					switch i 
					
						case 0
					
							if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
								if not is_playback_going_on_for_vehicle(ambient_enemy_vehicle[0].veh)
									ambient_enemy_status[i] = run_away
								endif 
							else 
								ambient_enemy_status[i] = run_away
							endif 
							
						break
						
						case 1
						
							if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
								if not is_playback_going_on_for_vehicle(ambient_enemy_vehicle[0].veh)
										
									open_sequence_task(seq)
										task_leave_vehicle(null, ambient_enemy_vehicle[0].veh, ecf_dont_close_door)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, ambient_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
									close_sequence_task(seq)
									task_perform_sequence(ambient_enemy[i].ped, seq)
									clear_sequence_task(seq)
									
									ambient_enemy_status[i] = get_into_pos
									
								endif 
								
							else 
								
								ambient_enemy_status[i] = get_into_pos
								
							endif
							
						break 
						
						case 2
						
							if is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
								if not is_playback_going_on_for_vehicle(ambient_enemy_vehicle[1].veh)
									ambient_enemy_status[i] = run_away
								endif 
							else 
								ambient_enemy_status[i] = run_away
							endif 
							
						break 
						
						case 3

							if is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
								if not is_playback_going_on_for_vehicle(ambient_enemy_vehicle[1].veh)
										
									open_sequence_task(seq)
										task_leave_vehicle(null, ambient_enemy_vehicle[1].veh, ecf_dont_close_door)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, ambient_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
									close_sequence_task(seq)
									task_perform_sequence(ambient_enemy[i].ped, seq)
									clear_sequence_task(seq)
									
									ambient_enemy_status[i] = get_into_pos
									
								endif 
								
							else 
								
								ambient_enemy_status[i] = get_into_pos
								
							endif
							
						break 
						
					endswitch 
					
				break 
				
				case get_into_pos
				
					switch i 
					
						case 1

							if not IS_ENTITY_AT_COORD(ambient_enemy[i].ped, ambient_enemy[i].run_to_pos, <<1.5, 1.5, 1.6>>, false, true)
								if has_ped_task_finished_2(ambient_enemy[i].ped, script_task_perform_sequence)
									
									set_blocking_of_non_temporary_events(ambient_enemy[i].ped, true)
									clear_ped_tasks(ambient_enemy[i].ped)
						
									open_sequence_task(seq)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, ambient_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
									close_sequence_task(seq)
									task_perform_sequence(ambient_enemy[i].ped, seq)
									clear_sequence_task(seq)
								endif 
								
							else 
							
								if has_ped_task_finished_2(ambient_enemy[i].ped, script_task_perform_sequence)

									set_ped_sphere_defensive_area(ambient_enemy[i].ped, ambient_enemy[i].run_to_pos, 1.75) 
									set_blocking_of_non_temporary_events(ambient_enemy[i].ped, false)
									task_combat_ped(ambient_enemy[i].ped, player_ped_id())
									
									ambient_enemy_status[i] = combat_player
									
								endif 
		
							endif 

							
							//peds will instead flee if all wave 3 enemies are dead or buddy is outside the lab
							if ped_structure_are_all_enemies_dead(wave_3_enemy)
					
								ambient_enemy_status[i] = run_away
								
							endif 
	
						break 
								
						case 3
							
							if not IS_ENTITY_AT_COORD(ambient_enemy[i].ped, ambient_enemy[i].run_to_pos, <<1.5, 1.5, 1.6>>, false, true)
								if has_ped_task_finished_2(ambient_enemy[i].ped, script_task_perform_sequence)
									
									set_blocking_of_non_temporary_events(ambient_enemy[i].ped, true)
									clear_ped_tasks(ambient_enemy[i].ped)
						
									open_sequence_task(seq)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, ambient_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
									close_sequence_task(seq)
									task_perform_sequence(ambient_enemy[i].ped, seq)
									clear_sequence_task(seq)
								endif 
								
							else 
							
								if has_ped_task_finished_2(ambient_enemy[i].ped, script_task_perform_sequence)

									set_ped_sphere_defensive_area(ambient_enemy[i].ped, ambient_enemy[i].run_to_pos, 1.75) 
									set_blocking_of_non_temporary_events(ambient_enemy[i].ped, false)
									task_combat_ped(ambient_enemy[i].ped, player_ped_id()) 
									
									ambient_enemy_status[i] = combat_player
									
								endif 
								
							endif 

							
							//peds will instead flee if all wave 3 enemies are dead or buddy is outside the lab
							if ped_structure_are_all_enemies_dead(wave_3_enemy)

								ambient_enemy_status[i] = run_away
								
							endif 

						break 

					endswitch 
				
				break 
				
				case combat_player
				
					// If last wave is dead, proceed to run away
					if ped_structure_are_all_enemies_dead(wave_3_enemy)
					
						set_blocking_of_non_temporary_events(ambient_enemy[i].ped, true)
						clear_ped_tasks(ambient_enemy[i].ped)
			
						ambient_enemy_status[i] = run_away
						
					endif 
					
				break 

				case run_away
				
					switch i 
					
						case 0

							if not is_ped_injured(ambient_enemy[1].ped)
								if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
									if ped_structure_are_all_enemies_dead(wave_3_enemy)
										if is_ped_sitting_in_vehicle(ambient_enemy[1].ped, ambient_enemy_vehicle[0].veh)
										//if is_ped_in_vehicle(ambient_enemy[1].ped, ambient_enemy_vehicle[0].veh)

											open_sequence_task(seq)
												TASK_VEHICLE_MISSION_COORS_TARGET(null, ambient_enemy_vehicle[0].veh, <<1740.78235, 3746.83301, 33.88214>>, mission_goto, 22.00, drivingmode_avoidcars, -1, -1)     
											close_sequence_task(seq)
											task_perform_sequence(ambient_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											ambient_enemy_status[i] = do_nothing
											
										endif 
									endif 
								
								else
									
									task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
									
									ambient_enemy_status[i] = do_nothing
								
								endif 
							
							else 
							
								if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
									open_sequence_task(seq)
										TASK_VEHICLE_MISSION_COORS_TARGET(null, ambient_enemy_vehicle[0].veh, <<1740.78235, 3746.83301, 33.88214>>, mission_goto, 22.00, drivingmode_avoidcars, -1, -1)     
									close_sequence_task(seq)
									task_perform_sequence(ambient_enemy[i].ped, seq)
									clear_sequence_task(seq)
								
									ambient_enemy_status[i] = do_nothing
									
								else 
								
									task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
									
									ambient_enemy_status[i] = do_nothing
								
								endif 
							
							endif 
							
						break 
				
						case 1
						
							if not is_ped_injured(ambient_enemy[0].ped)
								if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
									if not is_ped_sitting_in_vehicle(ambient_enemy[1].ped, ambient_enemy_vehicle[0].veh)
										if has_ped_task_finished_2(ambient_enemy[i].ped, script_task_perform_sequence)
							
											set_blocking_of_non_temporary_events(ambient_enemy[i].ped, true)
											clear_ped_tasks(ambient_enemy[i].ped)
								
											open_sequence_task(seq)
												task_enter_vehicle(null, ambient_enemy_vehicle[0].veh, -1, vs_front_right) 
											close_sequence_task(seq)
											task_perform_sequence(ambient_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
										endif 
									endif 
									
								else 
								
									task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
									
									ambient_enemy_status[i] = do_nothing
									
								endif 
								
							else 
							
								task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
								
								ambient_enemy_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 2
						
							if not is_ped_injured(ambient_enemy[3].ped)
								if is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
									if ped_structure_are_all_enemies_dead(wave_3_enemy)
									AND is_ped_sitting_in_vehicle(ambient_enemy[3].ped, ambient_enemy_vehicle[1].veh)
									
										open_sequence_task(seq)
											TASK_VEHICLE_MISSION_COORS_TARGET(null, ambient_enemy_vehicle[1].veh, <<1741.78235, 3745.83301, 33.88214>>, mission_goto, 22.00, drivingmode_avoidcars, -1, -1)     
										close_sequence_task(seq)
										task_perform_sequence(ambient_enemy[i].ped, seq)
										clear_sequence_task(seq)
										
										ambient_enemy_status[i] = do_nothing

									endif 
								
								else
									
									task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
									
									ambient_enemy_status[i] = do_nothing
								
								endif 
							
							else 
							
								if is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
							
									open_sequence_task(seq)
										TASK_VEHICLE_MISSION_COORS_TARGET(null, ambient_enemy_vehicle[1].veh, <<1741.78235, 3745.83301, 33.88214>>, mission_goto, 22.00, drivingmode_avoidcars, -1, -1)     
									close_sequence_task(seq)
									task_perform_sequence(ambient_enemy[i].ped, seq)
									clear_sequence_task(seq)
									
								else 
								
									task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
									
								endif 
									
								ambient_enemy_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 3

							if not is_ped_injured(ambient_enemy[2].ped)
								if is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
									if not is_ped_sitting_in_vehicle(ambient_enemy[3].ped, ambient_enemy_vehicle[1].veh)
										if has_ped_task_finished_2(ambient_enemy[i].ped, script_task_perform_sequence)
							
											set_blocking_of_non_temporary_events(ambient_enemy[i].ped, true)
											clear_ped_tasks(ambient_enemy[i].ped)
								
											open_sequence_task(seq)
												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), 1000)
												ENDIF
												task_enter_vehicle(null, ambient_enemy_vehicle[1].veh, -1, vs_front_right) 
											close_sequence_task(seq)
											task_perform_sequence(ambient_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
										endif 
									endif 
									
								else 
								
									task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
									
									ambient_enemy_status[i] = do_nothing
									
								endif 
								
							else 
							
								task_smart_flee_ped(ambient_enemy[i].ped, player_ped_id(), 200.00, -1)
								
								ambient_enemy_status[i] = do_nothing
								
							endif 
							
						break 
						
					endswitch 

				
				break 
				
				case do_nothing 
				
				break 				
			endswitch 
			
		endif 
		
	endfor 
	
	flash_ambient_enemy_blips(flash_blip_time) 
	
	return false 

endfunc  

func bool obtain_2_remaining_enemies()

	//de_activates all wave enemy AI
	for i = 0 to count_of(wave_0_enemy) - 1
		wave_0_status[i] = do_nothing
	endfor 
	
	for i = 0 to count_of(wave_2_enemy) - 1
		wave_2_status[i] = do_nothing
	endfor 


	for i = 0 to count_of(wave_0_enemy) - 1
		
		if not is_ped_injured(wave_0_enemy[i].ped)
			
			if remaining_enemies = 0
				remaining_enemy[0] = wave_0_enemy[i].ped
			
			elif remaining_enemies = 1
				remaining_enemy[1] = wave_0_enemy[i].ped
			endif 
			
			remaining_enemies++
			
			if remaining_enemies >= 2
				return true 
			endif 
			
		endif 
		
	endfor 
	
	
	for i = 0 to count_of(wave_2_enemy) - 1
		
		if not is_ped_injured(wave_2_enemy[i].ped)
			
			if remaining_enemies = 0
				remaining_enemy[0] = wave_2_enemy[i].ped
			
			elif remaining_enemies = 1
				remaining_enemy[1] = wave_2_enemy[i].ped
			endif 
			
			remaining_enemies++
			
			if remaining_enemies >= 2
				return true 
			endif 
			
		endif 
		
	endfor 
	

	return false 

endfunc 

proc load_uber_data()

	SetPieceCarPos[0] = <<1117.2610, 3546.8918, 34.0889>>
	SetPieceCarQuatX[0] = -0.0026
	SetPieceCarQuatY[0] = 0.0156
	SetPieceCarQuatZ[0] = 0.9813
	SetPieceCarQuatW[0] = -0.1920
	SetPieceCarRecording[0] = 14
	SetPieceCarStartime[0] = 0.0000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = pcj
	
	SetPieceCarPos[1] = <<1113.4513, 3550.6558, 34.0715>>
	SetPieceCarQuatX[1] = -0.0072
	SetPieceCarQuatY[1] = 0.0138
	SetPieceCarQuatZ[1] = 0.9729
	SetPieceCarQuatW[1] = -0.2308
	SetPieceCarRecording[1] = 15
	SetPieceCarStartime[1] = 0.0000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = pcj

	SetPieceCarPos[2] = <<1116.6305, 3553.0454, 34.0862>>
	SetPieceCarQuatX[2] = -0.0034
	SetPieceCarQuatY[2] = 0.0162
	SetPieceCarQuatZ[2] = 0.9675
	SetPieceCarQuatW[2] = -0.2525
	SetPieceCarRecording[2] = 16
	SetPieceCarStartime[2] = 0.0000
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = pcj

endproc 


func bool buddy_on_rooftop()
			
	if IS_ENTITY_IN_ANGLED_AREA(buddy.ped, <<1403.115, 3619.935, 37.949>>, <<1408.469, 3605.388, 40.945>>, 6.6)  
		return true 
	endif 
	
	return false 

endfunc 

 

proc player_waypoint_system()

	//ADD ASSISTED MOVEMENT WHICH JOHN ADDED INTO GAME WHICH IS TRIGGERED IN THE MAIN CONTROLLER HERE
	
	switch player_waypoint_status
	
		case 0

			//USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("methlab6", true, 0.9)
			//ASSISTED_MOVEMENT_remove_ROUTE("meth3")
			player_waypoint_status++

		break 
	
		case 1
		
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1401.366, 3619.451, 37.947>>, <<1406.805, 3604.510, 39.947>>, 10.900)  
				
				ASSISTED_MOVEMENT_remove_ROUTE("meth1")
				
				player_waypoint_status++
			endif 
		
		break 
		
		case 2
		
			//player runs from rooftop downstairs 
//			if enemy_creation_and_flow_status > 3
//				if IS_ENTITY_IN_ANGLED_AREA(<<1401.366, 3619.451, 37.947>>, <<1406.805, 3604.510, 39.947>>, 10.900)  
//						
//					ASSISTED_MOVEMENT_remove_ROUTE("Meth2")
//						
//					player_waypoint_status++
//				endif 
//			endif 
		
		break 
		
		case 3
		
		break 
		
		case 22
		
		break 
		
	endswitch 

endproc 

proc create_second_wave_of_wave_2_enemies()

	if not wave_2_enemy[6].created
				
		SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[0])
		start_playback_recorded_vehicle(wave_2_enemy_vehicle[0].veh, wave_2_enemy_vehicle[0].recording_number, "lkmethlab")
		skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[0].veh, 8000)
			
		setup_enemy_in_vehicle(wave_2_enemy[6], wave_2_enemy_vehicle[0].veh) 
		set_ped_flee_attributes(wave_2_enemy[6].ped, fa_use_vehicle, true)
		
		setup_enemy_in_vehicle(wave_2_enemy[7], wave_2_enemy_vehicle[0].veh, vs_front_right) 
				
		wave_2_enemy[10].created = true
		wave_2_enemy[11].created = true
		
	endif 
	
endproc 

proc create_third_wave_of_wave_2_enemies()

	if not wave_2_enemy[8].created
				
		SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[1])
		start_playback_recorded_vehicle(wave_2_enemy_vehicle[1].veh, wave_2_enemy_vehicle[1].recording_number, "lkmethlab")
		skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[1].veh, 5500)
			
		setup_enemy_in_vehicle(wave_2_enemy[8], wave_2_enemy_vehicle[1].veh) 
		set_ped_flee_attributes(wave_2_enemy[8].ped, fa_use_vehicle, true)
		
		setup_enemy_in_vehicle(wave_2_enemy[9], wave_2_enemy_vehicle[1].veh, vs_front_right) 
		
	endif 
	
endproc 

func bool player_grenade_launcher_cutscene_system()

	
	if player_grenade_launcher_cutscene_status != 99
		if player_grenade_launcher_cutscene_status > 0
			if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				player_grenade_launcher_cutscene_status = 22
			endif 
		endif
	endif 

	switch player_grenade_launcher_cutscene_status 
	
		case 0
		
			if lk_timer(cutscene_trigger_time, 1500)
				//if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1403.115, 3619.935, 37.949>>, <<1408.469, 3605.388, 40.945>>, 6.6)
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1401.353, 3619.360, 38.011>>, <<1406.672, 3604.589, 40.511>>, 10.7)  
				and is_entity_in_angled_area(buddy.ped, <<1401.353, 3619.360, 38.011>>, <<1406.672, 3604.589, 40.511>>, 10.7)  
				and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(buddy.ped)) < 5.0
					// @SBA - Don't toss the weapon yet if enemies are there
					IF NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackRoofVolume)
					AND NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackUpstairsRoomVolume) 
					AND NOT IS_ANY_PED_IN_SPECIFIC_ROOM(wave_2_enemy, (<<1392.2, 3614.1, 39.0>>), "v_39_UpperRm1")
					
						if start_new_cutscene_no_fade(false)
						
							REPLAY_START_EVENT()
							
							REPLAY_RECORD_BACK_FOR_TIME(1.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
							
							CREATE_CONVERSATION(scripted_speech[0], "methaud", "g_launcher", CONV_PRIORITY_HIGH)

							REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_GRENADE)
							REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_SMOKEGRENADE)
							REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_MOLOTOV)

							clear_area(<<1403.115, 3619.935, 37.949>>, 100.00, false)
							
							clear_ped_tasks_immediately(player_ped_id())
							// @SBA - ensure player gets 3 rounds
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), weapontype_grenadelauncher)
								SET_GRENADE_LAUNCHER_AMMO()
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weapontype_grenadelauncher, TRUE)
							ELSE
								give_weapon_to_ped(player_ped_id(), weapontype_grenadelauncher, kiGrenadeLauncherAmmo, true)
							ENDIF
							REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())

							set_blocking_of_non_temporary_events(buddy.ped, true)
							clear_ped_tasks_immediately(buddy.ped)
							hide_ped_weapon_for_scripted_cutscene(buddy.ped, true)
							
							//fix for moving the players truck infront of the door  @SBA - may need this for skip too?
							if is_vehicle_driveable(trevors_truck.veh)
								set_entity_coords(trevors_truck.veh, <<1409.9990, 3600.4973, 33.8676>>, false)
								set_entity_heading(trevors_truck.veh, 108.7487)
								set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_locked)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, false)
							ELIF DOES_ENTITY_EXIST(trevors_truck.veh)
								set_entity_coords(trevors_truck.veh, <<1409.9990, 3600.4973, 33.8676>>, false)
								set_entity_heading(trevors_truck.veh, 108.7487)
								//delete_vehicle(trevors_truck.veh) 
							endif 
							
							// define cameras
							camera_a = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
							
							if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
								camera_b = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<1407.336914,3612.038818,39.557838>>,<<-17.864130,-1.011106,-82.419159>>, 44.023628)
							else 
								camera_b = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", (<<1407.0337, 3612.1533, 39.8239>>), (<<-19.0003, -0.0000, -72.7912>>), 50.0)
							endif 

							/* START SYNCHRONIZED SCENE */
							cutscene_pos = <<1408.814, 3611.670, 38.000>>
							cutscene_rot = <<0.000, 0.000, 29.000>> //22
							
							cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
							
							TASK_SYNCHRONIZED_SCENE(buddy.ped, cutscene_index, "misschinese1", "throwgrenl_cook", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							TASK_SYNCHRONIZED_SCENE(player_ped_id(), cutscene_index, "misschinese1", "throwgrenl_player", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
							PLAY_SYNCHRONIZED_CAM_ANIM(camera_a, cutscene_index, "throwGrenL_CAM", "misschinese1")
							RENDER_SCRIPT_CAMS(TRUE, FALSE)

							set_synchronized_scene_phase(cutscene_index, 0.186) //0.359
							
							// @SBA - remove the pickup on the roof
							IF DOES_PICKUP_EXIST(puiGrenadeLauncher)
								REMOVE_PICKUP(puiGrenadeLauncher)
							ENDIF
							
							trigger_music_event("CHN1_G_LAUNCHER")
					
							allow_buddy_ai_system = false //turns off buddy ai system
							
							grenade_launcher_cutscene_playing = true 
							g_replay.iReplayInt[PLAYER_RECEIVED_GRENADE_LAUNCHER] = HAS_GOT_GLAUNCHER
							
							// If coming from replay
							IF NOT IS_SCREEN_FADED_IN()
								IF NOT IS_SCREEN_FADING_IN()
									DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
								ENDIF
							ENDIF					
							
							player_grenade_launcher_cutscene_status++
							
						endif
					ELSE
						// @SBA - restart short delay so there's a pause after upstairs enemies have been cleared out
						cutscene_trigger_time = get_game_timer()
					ENDIF
				
				else 

					if not has_label_been_triggered("chin_roof_0")
						if not is_any_text_being_displayed(locates_data)
							if create_conversation(scripted_speech[0], "methaud", "chin_roof_0", conv_priority_medium)
								set_label_as_triggered("chin_roof_0", true)
								SET_LABEL_AS_TRIGGERED("METH_GOD_18G", FALSE)
							endif 
						endif 
					
					else 
					
						if not has_label_been_triggered("METH_GOD_18G")
							if not is_any_text_being_displayed(locates_data)
								print_now("METH_GOD_18G", default_god_text_time, 1)
								set_label_as_triggered("METH_GOD_18G", true)
							endif 
						endif 

					endif 
				
				endif 
			endif 
	
		break 
		
		// @SBA - adding this case to pull the chef out early
		CASE 1
			
			// @SBA - once Chef has tossed the weapon, have him do something other than stand there
			IF (GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.65)
				clear_ped_tasks(buddy.ped)
				for i = 0 to count_of(wave_2_enemy) - 1
					if not is_ped_injured(wave_2_enemy[i].ped)
						task_combat_ped(buddy.ped, wave_2_enemy[i].ped)
						i = total_number_of_wave_2_enemies
					endif 
				endfor 
				wave_2_buddy_ai_system_status = 2
				allow_buddy_ai_system = true
				//TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[2].cover_point, (<<1423.0, 3624.8, 35.9>>), -1)
				player_grenade_launcher_cutscene_status++
			ENDIF		
		
		BREAK
		
		case 2
		
			//for first person camera we need to cut out of the animated camera early before it moves backwards as it gives a 3rd person feel 
			if not grenade_launcher_first_person_cam_active
				if (get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person)
					
					IF (GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.68) 
						
						vector cam_a_pos
						vector cam_a_rot 
						float cam_a_fov
						
						cam_a_pos = get_cam_coord(camera_a)
						cam_a_rot = get_cam_rot(camera_a)
						cam_a_fov = get_cam_fov(camera_a)
						
						destroy_cam(camera_a)
						
						camera_a = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", cam_a_pos, cam_a_rot, cam_a_fov, TRUE)
						set_cam_active_with_interp(camera_b, camera_a, 1300, graph_type_linear)
					
						grenade_launcher_first_person_cam_active = true 
					endif 
				
				endif 
			endif 
			
			if (GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.99) 
			
				REPLAY_STOP_EVENT()
			
				if (get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person)

					IF DOES_CAM_EXIST(camera_b)
					AND DOES_CAM_EXIST(camera_a)
						CPRINTLN(DEBUG_MISSION, "ACTIVATING camera_b")	
						SET_CAM_ACTIVE_WITH_INTERP(camera_b, camera_a, 100) 
						SET_CAM_ACTIVE(camera_a, FALSE)
					ENDIF
					
				endif 
				
				create_second_wave_of_wave_2_enemies()
				
				//clear_ped_tasks_immediately(player_ped_id())
				//SET_ENTITY_COORDS(player_ped_id(), vTrevorBackDeckStartPos) //<<1409.2817, 3611.7595, 38.0056>>
				//SET_ENTITY_HEADING(player_ped_id(), 325.6850)
				task_put_ped_directly_into_cover(player_ped_id(), vTrevorBackDeckStartPos, -1, true, 0.5, true, true, players_cover_point_2)
				force_ped_ai_and_animation_update(player_ped_id())

				hide_ped_weapon_for_scripted_cutscene(buddy.ped, false)
				set_blocking_of_non_temporary_events(buddy.ped, false)

				// @SBA -  recycle this
				SET_LABEL_AS_TRIGGERED("METH_GOD_18", FALSE)
				
				//create_conversation(scripted_speech[0], "methaud", "THANKS", conv_priority_medium)
	
				player_grenade_launcher_cutscene_status ++
			endif 
		
		break 
		
		CASE 3
			// handler for next vehicle full o' bad guys
			create_third_wave_of_wave_2_enemies()
	
			proofing_time = get_game_timer()
			
			grenade_launcher_cutscene_playing = false
			
			player_grenade_launcher_cutscene_status = 99
			
			// stop and destroy cam, reset stuff
			if (get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person)
				end_cutscene_no_fade(false, TRUE, -91.8, -19.0, 500, false)
			else 
				end_cutscene_no_fade(false, TRUE, -91.8, -19.0, 750, false)
			endif 
	
			return true 
		BREAK
		
		case 22
		
			if not is_screen_faded_out()
				if ((not is_screen_fading_in()) and (not is_screen_fading_out()))
					do_screen_fade_out(500)
				endif 
			
			else 
		
				create_second_wave_of_wave_2_enemies()
				create_third_wave_of_wave_2_enemies()

				clear_ped_tasks_immediately(player_ped_id())
				SET_ENTITY_COORDS(player_ped_id(), <<1408.9497, 3612.3738, 38.0057>>)//<<1408.0160, 3612.2808, 38.0056>>)
				SET_ENTITY_HEADING(player_ped_id(), 293.1200)
				//give_weapon_to_ped(player_ped_id(), weapontype_grenadelauncher, 22, true)
				//set_current_ped_weapon(player_ped_id(), weapontype_grenadelauncher, true)
				//task_seek_cover_to_coords(player_ped_id(), <<1408.0160, 3612.2808, 38.0056>>, <<1419.32, 3617.24, 39.92>>, 3000)
				task_put_ped_directly_into_cover(player_ped_id(), <<1408.0160, 3612.2808, 38.0056>>, -1)
				force_ped_ai_and_animation_update(player_ped_id(), true)
				
				clear_ped_tasks(buddy.ped)
				SET_ENTITY_COORDS(buddy.ped, <<1406.4009, 3615.2783, 38.0056>>)
				SET_ENTITY_HEADING(buddy.ped, 292.8793)
				hide_ped_weapon_for_scripted_cutscene(buddy.ped, false)
				set_blocking_of_non_temporary_events(buddy.ped, false)
				
				for i = 0 to count_of(wave_2_enemy) - 1
					if not is_ped_injured(wave_2_enemy[i].ped)
						task_combat_ped(buddy.ped, wave_2_enemy[i].ped)
						i = total_number_of_wave_2_enemies
					endif 
				endfor 

				allow_buddy_ai_system = true
				// @SBA -  recycle this
				SET_LABEL_AS_TRIGGERED("METH_GOD_18", FALSE)
				
				// stop cam, reset stuff
				end_cutscene(false)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "methaud", "THANKS", CONV_PRIORITY_MEDIUM)
				
				grenade_launcher_cutscene_playing = false
				
				player_grenade_launcher_cutscene_status = 99
				
				end_cutscene_no_fade(false, false, 0, 0, 3000, false)
		
				return true 
				
			endif 
		
		break 
		
		case 99
		break 
		
	endswitch 

	return false 
	
endfunc 


proc uber_vehicle_system()

	switch uber_vehicle_status 
	
		case 0
		
			update_uber_playback(wave_1_uber_vehicle, 1.0)
		
		break 
		
		case 1
	
		break 
		
		case 2
		
		
		break 
		
	endswitch 

endproc 

proc reset_wave_0_vehicles_for_ram_gates_cutscene(bool allow_vehicle_warp = false)
					
	//clears enemy cars off the vehicle recording line 
	float recording_time
	
	for i = 0 to 1
	
		if is_vehicle_driveable(wave_0_enemy_vehicle[i].veh)
		and is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[i].veh)
			
			skip_to_end_and_stop_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh)
			
		else 
		
			if not is_entity_dead(wave_0_enemy_vehicle[i].veh)
				if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[i].veh)
					stop_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh)
				endif 
			endif 
			
			//commenting out as this is causing vehicles to visibly warp. This cutscene has been made ingame.
			
			if allow_vehicle_warp
			
				if does_entity_exist(wave_0_enemy_vehicle[i].veh)
			
					wave_0_enemy_vehicle[i].pos = get_entity_coords(wave_0_enemy_vehicle[i].veh, false)
					
					if is_point_in_angled_area(wave_0_enemy_vehicle[i].pos, <<1376.030, 3597.157, 33.900>>, <<1383.560, 3599.545, 36.900>>, 27.700)	
					or is_point_in_angled_area(wave_0_enemy_vehicle[i].pos, <<1370.034, 3570.068, 33.969>>, <<1365.436, 3582.441, 34.969>>, 31.5)  
							
						recording_time = (get_total_duration_of_vehicle_recording(wave_0_enemy_vehicle[i].recording_number, "lkmethlab") - 50)
					
						set_entity_coords(wave_0_enemy_vehicle[i].veh, get_position_of_vehicle_recording_at_time(wave_0_enemy_vehicle[i].recording_number, recording_time, "lkmethlab"), false)
						set_entity_rotation(wave_0_enemy_vehicle[i].veh, get_rotation_of_vehicle_recording_at_time(wave_0_enemy_vehicle[i].recording_number, recording_time, "lkmethlab"), euler_yxz, false)
						
					endif 
					
				endif 
				
			endif 
			
		endif 
		
	endfor 
		
	for i = 0 to count_of(parked_cars) - 1
		if does_entity_exist(parked_cars[i])
			if is_entity_in_angled_area(parked_cars[i], <<1376.030, 3597.157, 33.900>>, <<1383.560, 3599.545, 36.900>>, 27.700, false, false) 
			or is_entity_in_angled_area(parked_cars[i], <<1370.034, 3570.068, 33.969>>, <<1365.436, 3582.441, 34.969>>, 31.5, false, false)
				delete_vehicle(parked_cars[i])
			endif 
		endif 
	endfor  
	
endproc

func bool ramming_gates_cutscene()

	FLOAT fRecTime

	if does_entity_exist(wave_0_enemy_vehicle[2].veh)
		if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
			if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
				fRecTime = (get_time_position_in_recording(wave_0_enemy_vehicle[2].veh))
				CPRINTLN(DEBUG_MISSION, "rec time = ", fRecTime)
			endif 
		ELSE
			CPRINTLN(DEBUG_MISSION, "Ramming Truck has been disabled: Bailing and cleaning up.")
			ramming_gates_cutscene_status = 99
		endif 
	endif 
				
	switch ramming_gates_cutscene_status 
	
		case 0
		
			//kill_any_conversation()//forces any dialogue to clean up specific or shooting dialogue.
			
			// @SBA - have this start quicker
			if lk_timer(cutscene_trigger_time, 250) 
			and prepare_music_event("CHN1_CAR_ARRIVES")
			//and not is_any_text_being_displayed(locates_data)
				
				// @SBA - trying this without the cutscene
				
					clear_area_of_vehicles(<<1402.2213, 3597.3352, 33.8866>>, 1000.00)

					reset_wave_0_vehicles_for_ram_gates_cutscene()
						
					if is_vehicle_driveable(trevors_truck.veh)
						set_entity_coords(trevors_truck.veh, <<1409.9990, 3600.4973, 33.8676>>)
						set_entity_heading(trevors_truck.veh, 108.7487)
						set_vehicle_on_ground_properly(trevors_truck.veh)
					endif 
					
					
					set_state_of_closest_door_of_type(prop_arm_gate_l, <<1372.36, 3609.02, 35.62>>, true, 0.0)
					set_state_of_closest_door_of_type(prop_arm_gate_l, <<1377.97, 3611.06, 35.62>>, true, 0.0)
					set_state_of_closest_door_of_type(prop_arm_gate_l, <<1372.36, 3609.02, 35.62>>, false, 0.0)
					set_state_of_closest_door_of_type(prop_arm_gate_l, <<1377.97, 3611.06, 35.62>>, false, 0.0)
				
					SETUP_ENEMY_VEHICLE(wave_0_enemy_vehicle[2])
					set_entity_proofs(wave_0_enemy_vehicle[2].veh, true, true, true, true, true)
					start_playback_recorded_vehicle(wave_0_enemy_vehicle[2].veh, wave_0_enemy_vehicle[2].recording_number, "lkmethlab") 
					// @SBA - setting the recording skip to be earlier, so it doesn't pop in so close
					// OLD VALUE = 13500
					skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[2].veh, 8000) 
					force_playback_recorded_vehicle_update(wave_0_enemy_vehicle[2].veh)

					setup_enemy_in_vehicle(wave_0_inside_enemy[0], wave_0_enemy_vehicle[2].veh) 			
					setup_enemy_in_vehicle(wave_0_inside_enemy[1], wave_0_enemy_vehicle[2].veh, vs_front_right) 
					setup_enemy_in_vehicle(wave_0_inside_enemy[2], wave_0_enemy_vehicle[2].veh, vs_back_left) 
					setup_enemy_in_vehicle(wave_0_inside_enemy[3], wave_0_enemy_vehicle[2].veh, vs_back_right) 
					set_entity_proofs(wave_0_inside_enemy[0].ped, true, true, true, true, true)
					set_entity_proofs(wave_0_inside_enemy[1].ped, true, true, true, true, true)
					set_entity_proofs(wave_0_inside_enemy[2].ped, true, true, true, true, true)
					set_entity_proofs(wave_0_inside_enemy[3].ped, true, true, true, true, true)
					
					// @SBA - add this truck guy for dialogue, ensure lable is cleared
					ADD_PED_FOR_DIALOGUE(scripted_speech[0], 8, wave_0_inside_enemy[1].ped, "VAGOS")
					SET_LABEL_AS_TRIGGERED("chin_RAM", FALSE)
					
					for i = 0 to count_of(wave_0_inside_enemy) - 1
						SET_COMBAT_FLOAT(wave_0_inside_enemy[i].ped, CCF_WEAPON_ACCURACY, 0.10)
					endfor 
					
					TASK_DRIVE_BY(wave_0_inside_enemy[1].ped, player_ped_id(), null, <<0.0, 0.0, 2.0>>, 300.00, 100)
						
					//setup_players_weapon_for_cutscene(weapontype_carbinerifle, 200, players_ammo)
					
					//locate check for forecourt of meth lab

					// @SBA - removing cameras, trying this without them
//					set_cam_active(camera_a, true)
//					render_script_cams(true, false)
//					set_cam_active_with_interp(camera_b, camera_a, 3000, graph_type_accel)

					if is_audio_scene_active("CHI_1_SHOOTOUT_01")
						stop_audio_scene("CHI_1_SHOOTOUT_01")
					endif 
					
					trigger_music_event("CHN1_CAR_ARRIVES")
					
					start_audio_scene("CHI_1_TRUCK_ARRIVES")
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[2].veh, "CHI_1_TRUCK_01")
					
					ramming_gates_cutscene_status++
 
//				endif 
			endif 
		
		break
		
		case 1
		
			// @SBA - making this one variable
			
			if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
				if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
					if  fRecTime > 15700
						
					
						reset_wave_0_vehicles_for_ram_gates_cutscene()
						
				
						original_time = get_game_timer()
			
						ramming_gates_cutscene_status++
					//@SBA - Have a guy in the truck say a dialogue line before they hit the gates
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("chin_RAM")
						// We don't want this to trigger too late
						IF fRecTime > 13000
						AND fRecTime < 13500
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
								IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_RAM", conv_priority_medium)
									//PRINTLN("*** Saying truck ramming dialogue now.")
									SET_LABEL_AS_TRIGGERED("chin_RAM", TRUE)
								ENDIF 
							ENDIF
						ENDIF
					ENDIF
					 
				endif 
			endif 

		break 
		
		case 2
		
			if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
				if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
					if (get_time_position_in_recording(wave_0_enemy_vehicle[2].veh) > 17700)
					
						for i = 0 to count_of(wave_0_inside_enemy) - 1
							if not is_ped_injured(wave_0_inside_enemy[i].ped)
								set_entity_proofs(wave_0_inside_enemy[i].ped, false, false, false, false, false)
							endif
						endfor
						
						if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
							set_entity_proofs(wave_0_enemy_vehicle[2].veh, false, false, false, false, false)
						endif 
						
						if not is_ped_injured(wave_0_inside_enemy[1].ped)
							clear_ped_tasks(wave_0_inside_enemy[1].ped)
						endif
						
						if is_audio_scene_active("CHI_1_TRUCK_ARRIVES")	
							stop_audio_scene("CHI_1_TRUCK_ARRIVES")
						endif 
						
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[2].veh)
						
						
						original_time = get_game_timer()
	
						ram_cutscene_finished = true 
						
						return true 
					endif 
				endif 
				
			endif 
		
		break 
		
		case 22
		
			if not is_screen_faded_out()
				if ((not is_screen_fading_in()) and (not is_screen_fading_out()))
					do_screen_fade_out(500)
				endif 
			
			else 
			
				if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
					if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[2].veh)
						skip_to_end_and_stop_playback_recorded_vehicle(wave_0_enemy_vehicle[2].veh)
					endif 
					set_entity_proofs(wave_0_enemy_vehicle[2].veh, false, false, false, false, false)
				endif 
				
				
				for i = 0 to count_of(wave_0_inside_enemy) - 1
					if not is_ped_injured(wave_0_inside_enemy[i].ped)
						set_entity_proofs(wave_0_inside_enemy[i].ped, false, false, false, false, false)
					endif
				endfor 
				
				if not is_ped_injured(wave_0_inside_enemy[1].ped)
					clear_ped_tasks(wave_0_inside_enemy[1].ped)
				endif
				
				if is_audio_scene_active("CHI_1_TRUCK_ARRIVES")	
					stop_audio_scene("CHI_1_TRUCK_ARRIVES")
				endif 
				
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[2].veh)
				
				original_time = get_game_timer()
				
				if is_ped_in_specific_room(player_ped_id(), <<1391.2273, 3601.6284, 38.5>>, "V_39_UpperRm3") 
					end_cutscene(false, -12.0, 0, false)
				else 
					end_cutscene(false, -12.0, 0, false)
				endif 

				return true 

			endif 

		break 
		
		// @SBA - Clean up if the truck got disbaled somehow
		CASE 99
		
			for i = 0 to count_of(wave_0_inside_enemy) - 1
				if not is_ped_injured(wave_0_inside_enemy[i].ped)
					set_entity_proofs(wave_0_inside_enemy[i].ped, false, false, false, false, false)
				endif
			endfor
			
			if is_vehicle_driveable(wave_0_enemy_vehicle[2].veh)
				set_entity_proofs(wave_0_enemy_vehicle[2].veh, false, false, false, false, false)
			endif 
			
			if not is_ped_injured(wave_0_inside_enemy[1].ped)
				clear_ped_tasks(wave_0_inside_enemy[1].ped)
			endif
			
			if is_audio_scene_active("CHI_1_TRUCK_ARRIVES")	
				stop_audio_scene("CHI_1_TRUCK_ARRIVES")
			endif 
			
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[2].veh)
			
			original_time = get_game_timer()

			ram_cutscene_finished = true 
			return true 
		BREAK
		
	endswitch 
	
	return false 

endfunc 


func bool wave_0_master_flow_system()

	// @SBA = loading for next waves
	IF NOT bLoadedLastShootoutStuff
		request_shootout_assets(FALSE)
		IF is_shootout_assets_loaded()
			bLoadedLastShootoutStuff = TRUE
		ENDIF
	ENDIF
			
	switch wave_0_master_flow_system_status 
	
		case 0
		
			set_state_of_closest_door_of_type(prop_arm_gate_l, <<1372.36, 3609.02, 35.62>>, true, 0.0)
			set_state_of_closest_door_of_type(prop_arm_gate_l, <<1377.97, 3611.06, 35.62>>, true, 0.0)
			set_state_of_closest_door_of_type(prop_arm_gate_l, <<1372.36, 3609.02, 35.62>>, false, 0.0)
			set_state_of_closest_door_of_type(prop_arm_gate_l, <<1377.97, 3611.06, 35.62>>, false, 0.0)
			
			
			// @SBA - opening outer doors to help player follow Chef
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, TRUE, 1.0)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, TRUE, -1.0)
			SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_door04, vInteriorDoorStore, TRUE, 0, 0, -1.0)
			
			// @SBA - try to get the rushers into better positions
			ADD_TACTICAL_NAV_MESH_POINT(<<1398.7, 3603.9, 37.95>>)
			ADD_TACTICAL_NAV_MESH_POINT(<<1394.5, 3603.5, 37.95>>)
		
			RESET_CHEF_WARNINGS_FOR_NEXT_WAVE()
			
			// there's gonna be a shootout
			SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
				
			wave_0_master_flow_system_status++
			
		break 
	
		case 1
			
			// clean up blend cam
			IF DOES_CAM_EXIST(camera_a)
				IF IS_GAMEPLAY_CAM_RENDERING()
					SET_CAM_ACTIVE(camera_a, FALSE)
					DESTROY_CAM(camera_a)
				ENDIF
			ENDIF
			
			// fade in if needed (replay)
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			
			// @SBA - let the ram cutscene happen if there are 2 or fewer guys left from first group
			IF ped_structure_get_total_number_of_enemies_dead(wave_0_enemy) >= ABSI( 2 - COUNT_OF(wave_0_enemy))
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0)
				
				wave_0_master_flow_system_status++
				cutscene_trigger_time = get_game_timer()
			ENDIF 
//			if ped_structure_are_all_enemies_dead(wave_0_enemy)
//				wave_0_master_flow_system_status++
//				cutscene_trigger_time = get_game_timer()
//			endif 
			
		break
		
		case 2
		
			// @SBA - now that there are two guys left when this wave "ends" and the truck comes,
			// I'm switching out Trevor's dialogue to the lines written for B*846594, which contextually
			// now make more sense.  OLD Label = "chin_cut_0"
			if create_conversation(scripted_speech[0], "methaud", "chin_LEFT", conv_priority_medium) 
				wave_0_master_flow_system_status++
			endif 
		
		break 
		
		case 3

			if ramming_gates_cutscene()
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "Wave 0 outside enemies")
				CLEAR_TACTICAL_NAV_MESH_POINTS()
				wave_0_master_flow_system_status++
			endif 
		
		break 

		case 4
		
			if is_ped_in_angled_area_lk(wave_0_inside_enemy[0].ped, <<1383.042, 3613.064, 33.900>>, <<1378.663, 3625.411, 44.900>>, 22.6)    
			or is_ped_in_angled_area_lk(wave_0_inside_enemy[1].ped, <<1383.042, 3613.064, 33.900>>, <<1378.663, 3625.411, 44.900>>, 22.6)    
			or is_vehicle_in_angled_area_lk(wave_0_enemy_vehicle[2].veh, <<1383.042, 3613.064, 33.900>>, <<1378.663, 3625.411, 44.900>>, 22.6)    
			OR NOT IS_VEHICLE_DRIVEABLE(wave_0_enemy_vehicle[2].veh)  // @SBA - skip through if vehicle got disabled

				wave_0_dialogue_system_status = 2
				wave_0_buddy_ai_system_status = 1
				wave_0_assisted_nodes_system_status = 1
				
				// piggy back in here
				SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
				
				wave_0_master_flow_system_status++
				
			endif 
		
		break 

		case 5
		
			bool wave_0_enemies_dead
			bool wave_0_inside_enemies_dead
			
		    wave_0_enemies_dead = ped_structure_are_all_enemies_dead(wave_0_enemy)
			wave_0_inside_enemies_dead = ped_structure_are_all_enemies_dead(wave_0_inside_enemy)
		
			if wave_0_enemies_dead
			and wave_0_inside_enemies_dead

				if is_audio_scene_active("CHI_1_SHOOTOUT_02")
					stop_audio_scene("CHI_1_SHOOTOUT_02")
				endif 
				
				START_AUDIO_SCENE("CHI_1_GET_TO_SHOOTOUT_03")
			
				//stops any text or dialogue once all the enemies are dead.
				//clear_prints()
				wave_0_dialogue_system_status = 22
			
				original_time = get_game_timer()
				
				wave_0_master_flow_system_status++
				
			endif 
				
		break 
		
		case 6
		
			//if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(buddy.ped)) < 20
				if create_conversation(scripted_speech[0], "methaud", "buddy_wv2_0", conv_priority_medium)
					original_time = get_game_timer()
					wave_0_master_flow_system_status++
				endif 
//			else 
//				wave_2_dialogue_system_status++
//			endif 
			
		break 
		
		case 7
		
			if (not is_any_text_being_displayed(locates_data) and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(buddy.ped)) < 15 and is_ped_facing_ped(player_ped_id(), buddy.ped, 50.00))
			or lk_timer(original_time, 5000)
			or is_player_infront_of_door() //stops player running to the front of lab and seeing enemies spawn

				return true 
				
			endif 
		
		break 
		
		// For replay
		CASE 50
			// delay fade in until player is actually in action mode
			IF  lk_timer(original_time, 750)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				wave_0_master_flow_system_status = 5
				wave_0_dialogue_system_status = 2
			ENDIF
		BREAK
		
	endswitch 
	
	return false
		
endfunc 

func bool are_wave_0_enemies_coming_up_stairs()
					
	for i = 0 to count_of(wave_0_inside_enemy) - 1
	
		if not is_ped_injured(wave_0_inside_enemy[i].ped)
		
			if IS_ENTITY_IN_ANGLED_AREA(wave_0_inside_enemy[i].ped, <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.20)
			or IS_ENTITY_IN_ANGLED_AREA(wave_0_inside_enemy[i].ped, <<1388.321, 3622.040, 34.061>>, <<1389.153, 3619.789, 41.061>>, 9.10)    
			
				return true 
				
			endif 
		endif 
	endfor 
	
	return false 
			
endfunc 

// Plays the Chef's outburst line when ready, then resets to do it again
PROC PLAY_AND_RESET_OUTBURST()
	IF TIMER_DO_ONCE_WHEN_READY(tmrChefOutburstTimer, fOutburstTime)
		IF PLAY_CHEF_CONVERSATION(sChefOutburst) 
			CPRINTLN(DEBUG_MISSION,"PLAY_AND_RESET_OUTBURST: Playing Chef Outburst.")
		ENDIF
		fOutburstTime = GET_RANDOM_FLOAT_IN_RANGE(8.000, 12.000)
		START_TIMER_NOW(tmrChefOutburstTimer)
	ENDIF
ENDPROC


// @SBA - handles playing Chef's dialogue about enemies getting inside for wave 0
PROC HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_0()
	BOOL bPlayDialogue = FALSE

	// If an enemy is downstairs, play warning 
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sChefWarn1)
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(LOCATES_DATA)
			IF IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1393.0, 3612.2, 35.0>>), "v_39_StairsRm")
			OR IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")
				// play Chef's dialogue, but only once (skip if too player's too far)
				IF PLAY_CHEF_CONVERSATION(sChefWarn1) 
					CPRINTLN(DEBUG_MISSION,"HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_0: Playing Chef WARNING 1.")
				ENDIF
				SET_LABEL_AS_TRIGGERED(sChefWarn1, TRUE)
			ENDIF
		ENDIF
	ENDIF

	// Check if enemy is upstairs
	IF wave_0_master_flow_system_status < 5
		IF IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1391.1970, 3601.0510, 37.9418>>), "v_39_UpperRm3")
		OR IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1397.1, 3608.4, 38.5>>), "v_39_UpperRm2")
		OR IS_ANY_PED_IN_SPECIFIC_ROOM(wave_0_enemy, (<<1392.2, 3614.1, 39.0>>), "v_39_UpperRm1")
			bPlayDialogue = TRUE
		ENDIF
	ENDIF

	// If clear to play the next warning, do it
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sChefWarn2)
		IF bPlayDialogue 
			// play Chef's dialogue if player is close enough
			IF PLAY_CHEF_CONVERSATION(sChefWarn2) 
				CPRINTLN(DEBUG_MISSION,"HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_0: Playing Chef WARNING 2.")
			ENDIF
			// Start outburst timer
			START_TIMER_NOW(tmrChefOutburstTimer)
			fOutburstTime = GET_RANDOM_FLOAT_IN_RANGE(8.000, 12.000)
			SET_LABEL_AS_TRIGGERED(sChefWarn2, TRUE)
		ENDIF 
	ENDIF
	
	// Enemies at the back stairs
	IF wave_0_master_flow_system_status = 5
		// Warning when enemies head to back stairs
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sChefBackStairsWarn)
			// Wait for any other enemies to clear the stairs
			IF NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_0_inside_enemy, tbFullBackStairsVolume)
			AND NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_0_enemy, tbFullBackStairsVolume)
				IF IS_ANY_PED_IN_TRIGGER_BOX(wave_0_inside_enemy, tbBackStairsAlleyVolume)
					// play warning if player is close enough
					IF PLAY_CHEF_CONVERSATION(sChefBackStairsWarn) 
						CPRINTLN(DEBUG_MISSION,"HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_0: Playing Chef BACK STAIRS WARNING.")
					ENDIF
					// Start outburst timer
					START_TIMER_NOW(tmrChefOutburstTimer)
					fOutburstTime = GET_RANDOM_FLOAT_IN_RANGE(8.000, 12.000)
					SET_LABEL_AS_TRIGGERED(sChefBackStairsWarn, TRUE)
				ENDIF
			ENDIF
		ENDIF
		// for back stairs outbursts
		IF IS_ANY_PED_IN_TRIGGER_BOX(wave_0_inside_enemy, tbTopBackStairsVolume)
		OR IS_ANY_PED_IN_TRIGGER_BOX(wave_0_inside_enemy, tbOnBackStairsVolume)
		OR IS_ANY_PED_IN_TRIGGER_BOX(wave_0_enemy, tbTopBackStairsVolume)
		OR IS_ANY_PED_IN_TRIGGER_BOX(wave_0_enemy, tbOnBackStairsVolume)
			bPlayDialogue = TRUE
		ENDIF
	ENDIF
	
	// Handle the Chef's outbursts
	IF bPlayDialogue
		PLAY_AND_RESET_OUTBURST()
	// reset the second warning after a timer and if the coast is currently clear
	ELIF IS_TIMER_STARTED(tmrChefOutburstTimer)
		IF TIMER_DO_ONCE_WHEN_READY(tmrChefOutburstTimer, fOutburstTime)
			SET_LABEL_AS_TRIGGERED(sChefWarn2, FALSE)
			CANCEL_TIMER(tmrChefOutburstTimer)
		ENDIF
	ENDIF
ENDPROC

// @SBA - handles playing Chef's dialogue about enemies getting inside for wave 2
PROC HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_2()
	BOOL bPlayDialogue = FALSE

	// If an enemy is coming upstairs, play warning 
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sChefWarn1)
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(LOCATES_DATA)
				IF IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackGateVolume)
				OR IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbTopBackStairsVolume)
				OR IS_ANY_PED_IN_SPECIFIC_ROOM(wave_2_enemy, (<<1392.2, 3614.1, 39.0>>), "v_39_UpperRm1")
				// play Chef's dialogue, but only once (if player's close)
				IF PLAY_CHEF_CONVERSATION(sChefWarn1) 
					CPRINTLN(DEBUG_MISSION,"HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_2: Playing Chef WARNING 1.")
				ENDIF
				SET_LABEL_AS_TRIGGERED(sChefWarn1, TRUE)
			ENDIF
		ENDIF
	ENDIF

	// Check if enemy is upstairs
	IF IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackRoofVolume)
	OR IS_ANY_PED_IN_SPECIFIC_ROOM(wave_2_enemy, (<<1397.1, 3608.4, 38.5>>), "v_39_UpperRm2")
		bPlayDialogue = TRUE
	ENDIF

	// If clear to play the next warning, do it
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sChefWarn2)
		//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(LOCATES_DATA)
			IF bPlayDialogue 
				// play Chef's dialogue if player's close
				IF PLAY_CHEF_CONVERSATION(sChefWarn2) 
					CPRINTLN(DEBUG_MISSION,"HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_2: Playing Chef WARNING 2.")
				ENDIF
				// Start outburst timer
				START_TIMER_NOW(tmrChefOutburstTimer)
				fOutburstTime = GET_RANDOM_FLOAT_IN_RANGE(8.000, 12.000)
				SET_LABEL_AS_TRIGGERED(sChefWarn2, TRUE)
			ENDIF 
		//ENDIF
	ENDIF
	
	// Handle the Chef's outbursts
	IF bPlayDialogue
		PLAY_AND_RESET_OUTBURST()
	// reset the second warning after a timer and if the coast is currently clear
	ELIF IS_TIMER_STARTED(tmrChefOutburstTimer)
		IF TIMER_DO_ONCE_WHEN_READY(tmrChefOutburstTimer, fOutburstTime)
			SET_LABEL_AS_TRIGGERED(sChefWarn2, FALSE)
			CANCEL_TIMER(tmrChefOutburstTimer)
		ENDIF
	ENDIF
ENDPROC

proc wave_0_dialogue_system()
	
	// @SBA- in case enemies get inside
	HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_0()
			
	switch wave_0_dialogue_system_status
	
		case 0
		
			if create_conversation(scripted_speech[0], "methaud", "buddy_info0", conv_priority_medium) 
				wave_0_dialogue_system_status++
			endif 
			
		break 
		
		case 1
		
			if not is_scripted_conversation_ongoing()
				print_now("METH_GOD_0", default_god_text_time, 1)
				wave_0_dialogue_system_status = 22
			endif 
		
		break 
		
		case 2
		
			if create_conversation(scripted_speech[0], "methaud", "ram_gates", conv_priority_medium) 
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0)
				
				buddy_time = get_game_timer()
				wave_0_dialogue_system_status++
			endif
		
		break 
		
		case 3
		
			if not is_scripted_conversation_ongoing()
			AND wave_0_buddy_ai_system_status >= 2  //@SBA - make sure Chef is ready to be followed first
			
				dialogue_time_2 = get_game_timer()
			
				dialogue_time = get_game_timer()
				
				print_now("METH_GOD_1", default_god_text_time, 1)  // Follow Chef outside
				
				wave_0_dialogue_system_status++
				
			endif
		
		break 
		
		case 4
		
			if lk_timer(dialogue_time_2, 5000)
				if not is_any_text_being_displayed(locates_data)
					if is_entity_in_angled_area(player_ped_id(), <<1388.600, 3602.066, 37.942>>, <<1396.070, 3604.638, 40.942>>, 7.2)   
						if create_conversation(scripted_speech[0], "methaud", "HERE", conv_priority_low) 
							dialogue_time_2 = get_game_timer()
						endif 
					endif 
				endif 
			endif 

			//ensures the "enemies coming from the left" dialogue only plays when the player is walking through the
			//correct door to make the dialogue make sense orientation wise. 
			//area on porch and small area running through door
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)

				if is_entity_in_angled_area(player_ped_id(), <<1386.186, 3614.861, 37.943>>, <<1389.025, 3615.835, 39.942>>, 1.8)   
					if not are_wave_0_enemies_coming_up_stairs()

						clear_prints()
				
						if create_conversation(scripted_speech[0], "methaud", "chin_to_left", conv_priority_medium)
				
							trigger_music_event("CHN1_FROM_LEFT")
						
							wave_0_dialogue_system_status++
							
						endif 
						
					else 
					
						wave_0_dialogue_system_status++
					
					endif 
				
				else 
				
					wave_0_dialogue_system_status++
				
				endif 
				
			else 
				
				vector player_pos
			
				player_pos = get_entity_coords(player_ped_id())
			
				//some of the conditions for making gang enemies attack.
				if lk_timer(original_time, 25000)
				or player_pos.z < 37.00
				//or is_entity_in_angled_area(player_ped_id(), <<1400.469, 3607.616, 38.006>>, <<1399.769, 3609.490, 40.506>>, 1.2) 
		
					wave_0_dialogue_system_status++
					
				endif 

			endif 	
		
		break 
		
		case 5

			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)
				//if create_conversation(scripted_speech[0], "methaud", "chin_left_0", conv_priority_medium) 
				if create_conversation(scripted_speech[0], "methaud", "ram_gates_2", conv_priority_medium) 
				
					wave_0_dialogue_system_status++
					
				endif 
			endif 
		
		break 
		
		case 6
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				SPECIAL_ABILITY_CHARGE_SMALL(PLAYER_ID(), TRUE, TRUE)
				
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("METH_help_T_KM",DEFAULT_HELP_TEXT_TIME)
				ELSE
					PRINT_HELP("METH_help_T",DEFAULT_HELP_TEXT_TIME)
				ENDIF
				wave_0_dialogue_system_status++
			ENDIF
		break 
		
		case 22
		
		break 
		
	endswitch 
	
	if lk_timer(dialogue_time, 13000)
		if wave_0_master_flow_system_status != 3 //stops the dialogue during the cutscene.
			if is_ped_shooting(player_ped_id())
				if not is_any_text_being_displayed(locates_data)
					if is_enemy_near_player(wave_0_enemy, 50.00)
					or is_enemy_near_player(wave_0_inside_enemy, 50.00)
						if create_conversation(scripted_speech[0], "methaud", "attack_0", conv_priority_low) 
							dialogue_time = get_game_timer()
						endif 
					endif 
				endif 
			endif 
		endif 
	endif 

endproc 

proc wave_0_buddy_ai_system()

	if allow_buddy_ai_system
	
		IF NOT wave_0_complete  // @SBA - this is combined for both parts of wave 0. If we get errant abandon messages, this may be why.
			// Chef at window position
			IF IS_ENTITY_AT_COORD(buddy.ped, <<1387.4102, 3607.4644, 37.9418>>, <<3.0, 3.0, 2.0>>, FALSE)	
				HANDLE_WAVE_0_CHEF_DEMISE()
				HANDLE_ABANDONMENT_MESSAGE(HAS_PLAYER_ABANDONED_CHEF_WAVE_0())
			// Chef outside, top of stairs postion
			ELIF IS_ENTITY_AT_COORD(buddy.ped, vChefCoverPointBackStairs, <<3.0, 3.0, 2.0>>, FALSE)
				HANDLE_WAVE_0_PART_2_CHEF_DEMISE()
				HANDLE_ABANDONMENT_MESSAGE(HAS_PLAYER_ABANDONED_CHEF_WAVE_0_PART2())
			ENDIF
			HANDLE_CHEF_DAMAGED_DIALOGUE()
		ENDIF
			
		switch wave_0_buddy_ai_system_status 

			case 0
			
				// @SBA - make sure Chef's health is set to max for this wave
				SET_HEALTH_FOR_PED(buddy.ped, buddy.health)

				if not IS_ENTITY_AT_COORD(buddy.ped, <<1387.4102, 3607.4644, 37.9418>>, <<2.0, 2.0, 2.0>>, false, true)
				
					if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
					
						clear_ped_tasks(buddy.ped)
						set_blocking_of_non_temporary_events(buddy.ped, true)
					
						open_sequence_task(seq)
							task_seek_cover_to_cover_point(null, buddy_cover_point[0], <<1372.01, 3596.31, 37.97>>, -1) 
						close_sequence_task(seq)
						task_perform_sequence(buddy.ped, seq)
						clear_sequence_task(seq)
						
					endif
					
				else 
				
					if is_ped_in_cover(buddy.ped) or lk_timer(buddy_time, 4500) 
				
						if lk_timer(buddy_time, 4500)

							SET_PED_CAN_PEEK_IN_COVER(buddy.ped, TRUE)
							set_blocking_of_non_temporary_events(buddy.ped, false)
							set_ped_sphere_defensive_area(buddy.ped, <<1387.4447, 3607.4478, 37.9418>>, 2.0)
		
							SET_PED_ACCURACY(buddy.ped, 25)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(buddy.ped, 75.0)
						
							wave_0_buddy_ai_system_status = 22 
							
						endif 
						
					endif 

				endif

			break
			
			case 1
			
				// When Chef is visible, wait a little extra to help player see him run out
				IF NOT IS_ENTITY_OCCLUDED(buddy.ped)
					START_TIMER_NOW(tmrChefRunsDelay)
				ELSE
					CANCEL_TIMER(tmrChefRunsDelay)
				ENDIF
				
				// @SBA - Don't move yet if enemies are right there, or player isn't looking
				IF NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_0_enemy, tbUpstairsRushVolume)
					if not is_entity_in_angled_area(player_ped_id(), <<1389.055, 3600.813, 37.947>>, <<1396.021, 3603.307, 40.947>>, 4.500)   
					OR TIMER_DO_WHEN_READY(tmrChefRunsDelay, 1.500)
					or lk_timer(original_time, 10000)	// failsafe to keep things moving
						bChefRunsToBackStairs = TRUE
					ENDIF
				ENDIF
						
				IF bChefRunsToBackStairs
					set_blocking_of_non_temporary_events(buddy.ped, true)
					clear_ped_tasks(buddy.ped)
				
					open_sequence_task(seq)
						task_clear_look_at(null)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "methlab4", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
					
					CANCEL_TIMER(tmrChefRunsDelay)
					wave_0_buddy_ai_system_status++
				endif 			
			break 
			
			case 2

				// @SBA - make sure Chef's health is set to max for this wave
				SET_HEALTH_FOR_PED(buddy.ped, buddy.health)

				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(buddy.ped, 2.0) //1.9
				endif 
				
				// @SBA - trying an earlier waypoint and different cover, also so he avoids getting stuck on door.  OLD = 15
				if waypoint_recording_get_coord("methlab4", 10, target_waypoint_pos)  
					if not IS_ENTITY_AT_COORD(buddy.ped, target_waypoint_pos, <<1.0, 1.0, 1.6>>, false, true)
						if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
						
							if waypoint_recording_get_closest_waypoint("methlab4", GET_ENTITY_COORDS(buddy.ped), waypoint_node_pos)
							
								waypoint_node_pos += 1
							
								// @SBA - trying an earlier point.  OLD = 15
								if waypoint_node_pos > 10
									waypoint_node_pos = 10
								endif 
							
								set_blocking_of_non_temporary_events(buddy.ped, true)
								clear_ped_tasks(buddy.ped)
								
								open_sequence_task(seq)
									task_follow_waypoint_recording(null, "methlab4", waypoint_node_pos, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
							endif 
						endif 
					
					else 

						IF IS_PLAYER_STEALING_PEDS_DESTINATION(buddy.ped, buddy_cover_point_data[0].pos, 5.0, false)
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[1].cover_point, buddy_cover_point_data[1].cover_from_pos, -1)
							SET_PED_CAN_PEEK_IN_COVER(buddy.ped, FALSE)
						ELSE
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[0].cover_point, buddy_cover_point_data[0].cover_from_pos, -1)
							SET_PED_CAN_PEEK_IN_COVER(buddy.ped, TRUE)
						ENDIF

						wave_0_buddy_ai_system_status++
					
					endif 
					
				endif 

			break 
			
			case 3
			
				if not is_ped_in_cover(buddy.ped) 

					buddy_time = get_game_timer()
					
					IF has_ped_task_finished_2(buddy.ped, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						IF IS_PLAYER_STEALING_PEDS_DESTINATION(buddy.ped, buddy_cover_point_data[0].pos, 5.0, false)
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[1].cover_point, buddy_cover_point_data[1].cover_from_pos, -1)
							SET_PED_CAN_PEEK_IN_COVER(buddy.ped, FALSE)
						ELSE
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[0].cover_point, buddy_cover_point_data[0].cover_from_pos, -1)
							SET_PED_CAN_PEEK_IN_COVER(buddy.ped, TRUE)
						ENDIF
					ENDIF
				else 
				
					if lk_timer(buddy_time, 1000)
				
						SET_PED_PATH_CAN_USE_CLIMBOVERS(buddy.ped, false)  

						set_ped_sphere_defensive_area(buddy.ped, GET_ENTITY_COORDS(buddy.ped), 1.5) // @SBA - OLD = <<1389.67, 3619.39, 38.5>> and 1.0 //<<1390.00, 3618.03, 38.3>>
						set_blocking_of_non_temporary_events(buddy.ped, false)
						task_combat_hated_targets_around_ped(buddy.ped, 100)
					
						wave_0_buddy_ai_system_status = 22
					
					ENDIF
					
				endif 
			
			break
			
			case 4
			
				if ped_structure_are_all_enemies_dead(wave_0_inside_enemy)
				
					set_ped_accuracy(buddy.ped, 10)
				
					set_blocking_of_non_temporary_events(buddy.ped, true)
					clear_ped_tasks(buddy.ped)
					SET_PED_CAN_PEEK_IN_COVER(buddy.ped, TRUE)
					
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<1390.00, 3618.03, 37.93>>, pedmove_run, -1)
						TASK_TURN_PED_TO_FACE_ENTITY(null, player_ped_id())
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
					
					wave_0_buddy_ai_system_status++
					
				endif 
				
			break 
			
			case 5
				
				if not IS_ENTITY_AT_COORD(buddy.ped, <<1390.00, 3618.03, 37.93>>, <<1.0, 1.0, 1.6>>, false, true) 
					if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
				
						set_blocking_of_non_temporary_events(buddy.ped, true)
						clear_ped_tasks(buddy.ped)
					
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<1390.00, 3618.03, 37.93>>, pedmove_run, -1)
						close_sequence_task(seq)
						task_perform_sequence(buddy.ped, seq)
						clear_sequence_task(seq)
						
					endif 
					
				else 
				
					if not is_ped_facing_ped(buddy.ped, player_ped_id(), 30.00)
						if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
							
							set_blocking_of_non_temporary_events(buddy.ped, true)
							clear_ped_tasks(buddy.ped)
							
							open_sequence_task(seq)
								TASK_TURN_PED_TO_FACE_ENTITY(null, player_ped_id())
							close_sequence_task(seq)
							task_perform_sequence(buddy.ped, seq)
							clear_sequence_task(seq)
						endif 
					endif 
					
					
				endif 
				
			break 
			
			case 22
			
			break 
			
		endswitch 
		
	endif 

endproc 


func bool wave_0_ai_system()

	if not wave_0_complete 
	
		bool wave_0_enemies_dead = ped_structure_are_all_enemies_dead(wave_0_enemy)
		bool wave_0_inside_enemies_dead = ped_structure_are_all_enemies_dead(wave_0_inside_enemy)
		// @SBA - declare local variable
		INT iWave_0_Index

		if wave_0_enemies_dead and wave_0_inside_enemies_dead
			wave_0_complete = true  
			return true 
		endif 
		
		for i = 0 to count_of(wave_0_enemy) - 1
			
			if not is_ped_injured(wave_0_enemy[i].ped)

				// @SBA - make blip for enemy if needed
				HANDLE_ENEMY_BLIP_CREATION(wave_0_enemy[i])
						
				switch wave_0_status[i] 
				
					case initial_attack_phase
					
						switch i 

							case 0
							case 1
							
								open_sequence_task(seq)
									task_pause(null, 500)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(wave_0_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
								wave_0_status[i] = get_into_pos
								
							break 
							
							case 2
							
								if is_vehicle_driveable(wave_0_enemy_vehicle[0].veh)
									if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[0].veh)
										if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_0_enemy_vehicle[0].recording_number, "lkmethlab") - 1000) < (get_time_position_in_recording(wave_0_enemy_vehicle[0].veh))
										
											stop_playback_recorded_vehicle(wave_0_enemy_vehicle[0].veh)
											REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[0])

											open_sequence_task(seq)
												task_leave_vehicle(null, wave_0_enemy_vehicle[0].veh, ecf_dont_close_door)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											close_sequence_task(seq)
											task_perform_sequence(wave_0_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											wave_0_status[i] = get_into_pos
										endif 
									endif 
								
								else 
								
									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 3.0)
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									wave_0_status[i] =  run_to_other_side_of_building 
									
								endif 
								
							break 
							
							case 3
							case 4
							
								if is_vehicle_driveable(wave_0_enemy_vehicle[0].veh)
									if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[0].veh)
										
										open_sequence_task(seq)
											task_pause(null, (150 * i))
											//task_leave_vehicle(null, wave_0_enemy_vehicle[0].veh, ecf_dont_close_door)
											//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
											TASK_SET_SPHERE_DEFENSIVE_AREA(null, wave_0_enemy[i].run_to_pos, 3.0)
											task_set_blocking_of_non_temporary_events(null, false)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 100)
										close_sequence_task(seq)
										task_perform_sequence(wave_0_enemy[i].ped, seq)
										clear_sequence_task(seq)
										wave_0_status[i] = get_into_pos
										
									endif 
								else 
									
									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0) //3.0
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									wave_0_status[i] =  run_to_other_side_of_building 
									
								endif 

							break
							
							case 5
							
								if is_vehicle_driveable(wave_0_enemy_vehicle[1].veh)
									if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[1].veh)
										if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_0_enemy_vehicle[1].recording_number, "lkmethlab") - 100) < (get_time_position_in_recording(wave_0_enemy_vehicle[1].veh))
										
											stop_playback_recorded_vehicle(wave_0_enemy_vehicle[1].veh)
											REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[1])

											open_sequence_task(seq)
												task_leave_vehicle(null, wave_0_enemy_vehicle[1].veh, ecf_dont_close_door)
												// @SBA - tasking this guy to just run (he comes inside first)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 1.0)
												//TASK_SWAP_WEAPON(NULL, TRUE)
												//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, wave_0_enemy[i].run_to_pos, pedmove_run, -1)
											close_sequence_task(seq)
											task_perform_sequence(wave_0_enemy[i].ped, seq)
											clear_sequence_task(seq)
											
											wave_0_status[i] = get_into_pos
										endif 
									endif 
								
								else 

									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0) //3.0
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									wave_0_status[i] =  run_to_other_side_of_building 
									
								endif 
								
							break 
							
							case 6
							
								if is_vehicle_driveable(wave_0_enemy_vehicle[1].veh)
									if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[1].veh)
										
										open_sequence_task(seq)
											task_pause(null, (i * i * 15))
											task_leave_vehicle(null, wave_0_enemy_vehicle[1].veh, ecf_dont_close_door)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_0_enemy[i].ped, seq)
										clear_sequence_task(seq)
										wave_0_status[i] = get_into_pos
										
									endif 
								else 
									
									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0)//2.0
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									wave_0_status[i] =  run_to_other_side_of_building 
									
								endif 
							
							break 
							
							case 7
							case 8
							
								if is_vehicle_driveable(wave_0_enemy_vehicle[1].veh)
									if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[1].veh)
										
										open_sequence_task(seq)
											task_pause(null, (i * i * 15))
											task_leave_vehicle(null, wave_0_enemy_vehicle[1].veh, ecf_dont_close_door)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_0_enemy[i].ped, seq)
										clear_sequence_task(seq)
										wave_0_status[i] = get_into_pos
										
									endif 
								else 
									
									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0) //3.0
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									wave_0_status[i] =  run_to_other_side_of_building 
									
								endif 
								
							break 
							
							case 9
							
								open_sequence_task(seq)
									task_pause(null, 500) 
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(wave_0_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
								wave_0_status[i] = get_into_pos
								
							break 
							
						endswitch
						
					break 
					
					case get_into_pos 
					
						switch i
						
							case 0
							case 1
							case 2
							case 3
							case 4
							case 5  // @SBA - no longer special casing this one, see below
							case 6
							case 7
							case 8 
							
								if not IS_ENTITY_AT_COORD(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, <<1.5, 1.5, 1.6>>, false, true)
									if has_ped_task_finished_2(wave_0_enemy[i].ped, script_task_perform_sequence)
										
										set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, true)
										clear_ped_tasks(wave_0_enemy[i].ped)
							
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_0_enemy[i].ped, seq)
										clear_sequence_task(seq)
									endif 
								
								else 
									
									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0)//3.0
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									
									// @SBA - enemies will try to rush inside periodically, so setting this as the next stage
									wave_0_status[i] =  run_to_front_of_building 
									
								endif 
							
							break 
							
							// @SBA - special case for this enemy so he runs right inside
							// Mothballing...
//							CASE 5
//							
//								if not IS_ENTITY_AT_COORD(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, <<1.5, 1.5, 1.6>>, false, true)
//									//PRINTLN("*** ENEMY 5 - Not at coord ***")
//
//									if has_ped_task_finished_2(wave_0_enemy[i].ped, script_task_perform_sequence)
//										//PRINTLN("*** ENEMY 5 - Resetting task ***")
//
//										set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, true)
//										clear_ped_tasks(wave_0_enemy[i].ped)
//							
//										open_sequence_task(seq)
//											// @SBA - tasking this guy to just run (he comes inside first)
//											//TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8)
//											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, wave_0_enemy[i].run_to_pos, pedmove_run, -1)
//										close_sequence_task(seq)
//										task_perform_sequence(wave_0_enemy[i].ped, seq)
//										clear_sequence_task(seq)
//									endif 
//								
//								else 
//									//PRINTLN("*** ENEMY 5 - Going to  run_into_shop ***")
//
//									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
//									// @SBA - set defensive area just outside the door to move the enemy there
//									//set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0)//3.0
//									SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_enemy[i].ped, tbFrontDoorVolume.vMin, tbFrontDoorVolume.vMax, tbFrontDoorVolume.flWidth, TRUE)
//									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
//									// set this flag
//									wave_0_enemy[i].bRushInside = TRUE
//									
//									// @SBA - setting this as the next stage so he'll go upstairs
//									wave_0_status[i] =  run_into_shop 
//									
//								endif								
							
							BREAK 
							
							case 9 // @SBA - not used?
							
								if not IS_ENTITY_AT_COORD(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, <<1.5, 1.5, 1.6>>, false, true)
									if has_ped_task_finished_2(wave_0_enemy[i].ped, script_task_perform_sequence)
										
										set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, true)
										clear_ped_tasks(wave_0_enemy[i].ped)
							
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, wave_0_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(wave_0_enemy[i].ped, seq)
										clear_sequence_task(seq)
									endif 
								
								else 
									
									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 8.0)//3.0
									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
									wave_0_status[i] =  attack_from_pos_2
									
								endif 
							
							break 

						endswitch
						
						// @SBA - set intial wait time before the first enemy rushes (if not set yet)
						IF iWaitTimeBeforeRush = 0
							CANCEL_TIMER(tmrNextRusherDelay)
							iWaitTimeBeforeRush = 15000  
							// @SBA - start rush timer
							iWave_Rush_Time = GET_GAME_TIMER()
						ENDIF
					break 
					
					case attack_from_pos_2
					
						switch i 
						
							case 9

//								if is_ped_injured(wave_0_enemy[1].ped)
//								
//									wave_0_enemy[9].run_to_pos = wave_0_enemy[1].run_to_pos
//
//									set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
//									set_ped_sphere_defensive_area(wave_0_enemy[i].ped, wave_0_enemy[i].run_to_pos, 3.0)
//									task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
//									wave_0_status[i] =  do_nothing
//									
//								endif 
								
							break 
							
						endswitch 

					break 
					
					// @SBA - adding this case for the rush inside
					// adding this intermediate waypoint so the enemies go through the front door
					// (otherwise they open the gate to go up the back stairs)
					CASE run_to_front_of_building
						
						// @SBA - if the gate has been rammed, switch to the appropriate case
						IF ram_cutscene_finished
							wave_0_status[i] = run_to_other_side_of_building
							CANCEL_TIMER(tmrNextRusherDelay)
							RETURN FALSE
						// @SBA - otherwise, when the wait timer is ready, check if it's ok to send a rusher
						ELIF lk_timer(iWave_Rush_Time, iWaitTimeBeforeRush)
							IF ARE_WE_READY_FOR_A_NEW_RUSHER(wave_0_enemy, 0)  // @SBA - don't want rushers unless Chef is abandoned
								START_TIMER_NOW(tmrNextRusherDelay)
								// delay checking for the need for a new rusher until after one has been deployed
								iWaitTimeBeforeRush =  10500
								iWave_Rush_Time = GET_GAME_TIMER()
							ELSE
								// @SBA - Not ready for a new rusher (max number still in play).
								//CPRINTLN(DEBUG_MISSION,"*** Don't need a new rusher yet.  Reset the timer to try again later.")
								iWaitTimeBeforeRush =  GET_RANDOM_INT_IN_RANGE(2000, 3999)
								iWave_Rush_Time = GET_GAME_TIMER()
							ENDIF
						ENDIF
						
						// Delay the rusher so there's a breather after last one was killed
						IF TIMER_DO_WHEN_READY(tmrNextRusherDelay, 10.000)
						OR HAS_PLAYER_ABANDONED_CHEF_WAVE_0()
							// still need rusher?
							IF ARE_WE_READY_FOR_A_NEW_RUSHER(wave_0_enemy, 1)
								// send a chosen enemy to the front doors
								iWave_0_Index = GET_NEXT_WAVE_0_INDEX()
								IF iWave_0_Index != -1
									CPRINTLN(DEBUG_MISSION,"Sending in a new rusher (wave 0)")
									// @SBA - set defensive area just outside the door to move the enemy there
									SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_enemy[iWave_0_Index].ped, tbFrontDoorVolume.vMin, tbFrontDoorVolume.vMax, tbFrontDoorVolume.flWidth, TRUE)
									// @SBA - set a new, random wait time and restart rush timer
									// Note: Don't set time to 18000ms as it's checking against that for wave 2
									iWaitTimeBeforeRush = GET_RANDOM_INT_IN_RANGE(2000, 3999)
									iWave_Rush_Time = GET_GAME_TIMER()
									
									SET_ENEMY_RUSHER_ATTRIBUTES(wave_0_enemy[iWave_0_Index].ped, 1, 0, 50)
									CANCEL_TIMER(tmrNextRusherDelay)
									
									wave_0_status[iWave_0_Index] = run_into_shop
								ENDIF
							ENDIF
						ENDIF
					BREAK

					// @SBA - adding this case for the rush inside
					// This sends the enemies upstairs to fight
					CASE run_into_shop
					
						// @SBA - if the gate has been rammed, switch to the appropriate case
						IF ram_cutscene_finished
							wave_0_status[i] = run_to_other_side_of_building
						// @SBA - otherwise have them continue upstairs
						ELIF IS_ENTITY_IN_TRIGGER_BOX(wave_0_enemy[i].ped, tbFrontDoorVolume)
							// @SBA - set defensive area upstairs to move the enemy there
							SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_enemy[i].ped, tbUpstairsRushVolume.vMin, tbUpstairsRushVolume.vMax, tbUpstairsRushVolume.flWidth)
							// @SBA - Reset this so buddy can shoot these guys
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(wave_0_enemy[i].ped, FALSE)
							
							wave_0_status[i] = combat_player	
						ENDIF 

					BREAK
					
					// @SBA - adding this case for the rush inside
					CASE combat_player
						// @SBA - if enemy has already reached position, let him fight there
						IF IS_ENTITY_IN_TRIGGER_BOX(wave_0_enemy[i].ped, tbUpstairsRushVolume)
							SET_PED_ENABLE_WEAPON_BLOCKING(wave_0_enemy[i].ped, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(wave_0_enemy[i].ped, CA_ENABLE_TACTICAL_POINTS_WHEN_DEFENSIVE, TRUE)
							REGISTER_TARGET(wave_0_enemy[i].ped, buddy.ped)
							PLAY_PED_AMBIENT_SPEECH_NATIVE(wave_0_enemy[i].ped, "GENERIC_WAR_CRY", "SPEECH_PARAMS_FORCE")

							// Make them go into the other room if it's free
							IF NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_0_enemy, tbUpstairsCloserRushVolume)
								IF IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1390.91, 3608.21, 38.91>>), "v_39_UpperRm3")
									SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_enemy[i].ped, tbUpstairsCloserRushVolume.vMin, tbUpstairsCloserRushVolume.vMax, tbUpstairsCloserRushVolume.flWidth)
								ENDIF
							ENDIF
							
							
							wave_0_status[i] = do_nothing
						// otherwise, have him take up a position at the top of the back stairs
						ELIF ram_cutscene_finished
							wave_0_status[i] = charge_back_stairs
						ENDIF
					BREAK
					
					case run_to_other_side_of_building 
					
						if ram_cutscene_finished
							
							set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, true)
							clear_ped_tasks(wave_0_enemy[i].ped)
							// @SBA - Reset this flag so buddy can shoot these guys, and kill any previous defensive area
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(wave_0_enemy[i].ped, FALSE)
							REMOVE_PED_DEFENSIVE_AREA(wave_0_enemy[i].ped)
						
							open_sequence_task(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1395.6790, 3629.6826, 33.9204>>, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
							close_sequence_task(seq)
							task_perform_sequence(wave_0_enemy[i].ped, seq)
							clear_sequence_task(seq)
						
							wave_0_status[i] = get_to_other_side_of_building
							
						endif 
						
					break 
					
					case get_to_other_side_of_building
					
						if not IS_ENTITY_AT_COORD(wave_0_enemy[i].ped, <<1395.6790, 3629.6826, 33.9204>>, <<1.0, 1.0, 1.6>>, false, true)
							if has_ped_task_finished_2(wave_0_enemy[i].ped, script_task_perform_sequence)
								
								set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, true)
								clear_ped_tasks(wave_0_enemy[i].ped)
					
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1395.6790, 3629.6826, 33.9204>>, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(wave_0_enemy[i].ped, seq)
								clear_sequence_task(seq)
							endif 
						
						else 
						
							set_blocking_of_non_temporary_events(wave_0_enemy[i].ped, false)
							set_ped_sphere_defensive_area(wave_0_enemy[i].ped, <<1395.6790, 3629.6826, 33.9204>>, 8.0) //@SBA - setting a bigger area to accomodate more enemies (previous 1.5)
							task_combat_ped(wave_0_enemy[i].ped, player_ped_id())
							wave_0_status[i] = charge_back_stairs
						
						endif 
					
					break 
					
					// @SBA - Added this case to have any survivors charge the stairs if the truck guys are dead
					CASE charge_back_stairs
						IF ped_structure_are_all_enemies_dead(wave_0_inside_enemy, FALSE)
						OR wave_0_enemy[i].bRushInside
							// @SBA - Make sure rush flag is set
							wave_0_enemy[i].bRushInside = TRUE
							SET_PED_ANGLED_DEFENSIVE_AREA(wave_0_enemy[i].ped, tbTopBackStairsVolume.vMin, tbTopBackStairsVolume.vMax, tbTopBackStairsVolume.flWidth, TRUE)
							// @SBA - ensure buddy AI can kill them
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(wave_0_enemy[i].ped, FALSE)
							wave_0_status[i] = do_nothing
						ENDIF
					BREAK

					case do_nothing
					

					break 
					
				endswitch 
				
			endif 
		
		endfor 
		
		
		
		wave_0_inside_enemy_0_system()
		wave_0_inside_enemy_1_system()
		wave_0_inside_enemy_2_system()
		wave_0_inside_enemy_3_system()
		
	else 
	
		return true 
		
	endif 
	

	return false 

endfunc 

proc wave_0_assisted_nodes_system()

	switch wave_0_assisted_nodes_system_status
	
		case 0
		
		break 
		
		case 1

			ASSISTED_MOVEMENT_REQUEST_ROUTE("Wave0")
			
			wave_0_assisted_nodes_system_status++
		
		break 
		
		case 2
		
			//if not is_ped_inside_interior(player_ped_id(), <<1391.6487, 3608.3596, 37.9418>>) 
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1383.533, 3615.497, 37.931>>, <<1394.681, 3619.662, 40.931>>, 4.400)      
			
				ASSISTED_MOVEMENT_remove_ROUTE("Wave0")
				
				wave_0_assisted_nodes_system_status++
				
			endif 
		
		break 
		
		case 3
		
		break 
	
	endswitch 

endproc 

func bool wave_0_explode_vehicle_system()
	
//	if not is_vehicle_exploded
//		
//		for i = 0 to count_of(wave_0_enemy_vehicle) - 1
//			
//			if is_vehicle_driveable(wave_0_enemy_vehicle[i].veh)
//				if not is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[i].veh)
//					if get_vehicle_petrol_tank_health(wave_0_enemy_vehicle[i].veh) < 300
//					or get_vehicle_engine_health(wave_0_enemy_vehicle[i].veh) < 300
//					or get_entity_health(wave_0_enemy_vehicle[i].veh) < 300
//					
//						explode_vehicle(wave_0_enemy_vehicle[i].veh)
//						
//						is_vehicle_exploded = true 
//						
//						return true 
//						
//					endif 
//				endif 
//			endif 
//		endfor 
//		
//	endif 
	
	if does_entity_exist(parked_cars[3])
		
		if is_vehicle_driveable(parked_cars[3])
			
			if get_entity_health(parked_cars[3]) < 700
			or get_vehicle_engine_health(parked_cars[3]) < 700.00
			or get_vehicle_petrol_tank_health(parked_cars[3]) < 700.00
			
				explode_vehicle(parked_cars[3])
				
				set_vehicle_as_no_longer_needed(parked_cars[3])
				
			endif 
			
		endif 
		
	endif 
	
	return false 

endfunc 

proc wave_0_vehicle_destruction_system()
	
	vector bullet_front_left_offset = <<-0.78, 1.32, -0.4>>  
	vector bullet_front_right_offset = <<0.78, 1.32, -0.4>> 
	vector bullet_back_right_offset = <<0.78, -1.64, -0.4>> 
	vector bullet_back_left_offset = <<-0.78, -1.64, -0.4>> 
	float bullet_radius_check = 1.55
	
	vector deformation_front_left_offset = <<0.900, 1.340, 0.15>>
	vector deformation_front_right_offset = <<-0.900, 1.30, 0.15>>
	vector deformation_back_left_offset = <<-0.900, -1.3, 0.15>>
	vector deformation_back_right_offset = <<0.9, -1.3, 0.15>>
	float damage = 200.00 //140.00
	float deformation = 200.00 //140.00

	
	if is_player_control_on(player_id())
		
		for i = 0 to count_of(wave_0_enemy_vehicle) - 1
			
			if not is_entity_dead(wave_0_enemy_vehicle[i].veh)
			
				//POSSIBLY MAKE A FUNCTION OUT OF THIS AND PASS IN 8 PARAMETERS
				if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_0_enemy_vehicle[i].veh, bullet_front_left_offset), bullet_radius_check)	
					if not is_vehicle_tyre_burst(wave_0_enemy_vehicle[i].veh, sc_wheel_car_front_left)
						SET_VEHICLE_TYRE_BURST(wave_0_enemy_vehicle[i].veh, sc_wheel_car_front_left)
						SET_VEHICLE_DAMAGE(wave_0_enemy_vehicle[i].veh, deformation_front_left_offset, damage, deformation, true) 
						SMASH_VEHICLE_WINDOW(wave_0_enemy_vehicle[i].veh, sc_window_front_left)
					endif 
				endif 
				
				if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_0_enemy_vehicle[i].veh, bullet_front_right_offset), bullet_radius_check)	
					if not is_vehicle_tyre_burst(wave_0_enemy_vehicle[i].veh, sc_wheel_car_front_right)
						SET_VEHICLE_TYRE_BURST(wave_0_enemy_vehicle[i].veh, sc_wheel_car_front_right)
						SET_VEHICLE_DAMAGE(wave_0_enemy_vehicle[i].veh, deformation_front_right_offset, damage, deformation, true) 
						SMASH_VEHICLE_WINDOW(wave_0_enemy_vehicle[i].veh, sc_window_front_right)
					endif
				endif 
				
				if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_0_enemy_vehicle[i].veh, bullet_back_left_offset), bullet_radius_check)	
					if not is_vehicle_tyre_burst(wave_0_enemy_vehicle[i].veh, sc_wheel_car_rear_left)
						SET_VEHICLE_TYRE_BURST(wave_0_enemy_vehicle[i].veh, sc_wheel_car_rear_left)
						SET_VEHICLE_DAMAGE(wave_0_enemy_vehicle[i].veh, deformation_back_left_offset, damage, deformation, true) 
						SMASH_VEHICLE_WINDOW(wave_0_enemy_vehicle[i].veh, sc_window_rear_left)
					endif 
				endif 
				
				if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_0_enemy_vehicle[i].veh, bullet_back_right_offset), bullet_radius_check)	
					if not is_vehicle_tyre_burst(wave_0_enemy_vehicle[i].veh, sc_wheel_car_rear_right)
						SET_VEHICLE_TYRE_BURST(wave_0_enemy_vehicle[i].veh, sc_wheel_car_rear_right)
						SET_VEHICLE_DAMAGE(wave_0_enemy_vehicle[i].veh, deformation_back_right_offset, damage, deformation, true) 
						SMASH_VEHICLE_WINDOW(wave_0_enemy_vehicle[i].veh, sc_window_rear_right)
					endif 	
				endif 
				
			endif 
			
		endfor 
		
	endif 
		
endproc

proc wave_0_leaking_petrol_system()
	
	if does_entity_exist(wave_0_enemy_vehicle[1].veh)
		if is_vehicle_driveable(wave_0_enemy_vehicle[1].veh)
		
			if is_playback_going_on_for_vehicle(wave_0_enemy_vehicle[1].veh)
				if (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(wave_0_enemy_vehicle[1].recording_number, "lkmethlab") - 3000) < (get_time_position_in_recording(wave_0_enemy_vehicle[1].veh))
					if GET_VEHICLE_PETROL_TANK_HEALTH(wave_0_enemy_vehicle[1].veh) > 150//150
					
						SET_VEHICLE_CAN_LEAK_PETROL(wave_0_enemy_vehicle[1].veh, TRUE)
						SET_VEHICLE_PETROL_TANK_HEALTH(wave_0_enemy_vehicle[1].veh, 150)
						iSuvDamageCount = 0
						CPRINTLN(DEBUG_MISSION,"wave_0_leaking_petrol_system: Setting Blue SUV tank health.")
					endif 
				endif
			// @SBA - gas trail not working properly, so hacking in something to get this vehicle ignited
			ELSE
				IF NOT IS_ENTITY_ON_FIRE(wave_0_enemy_vehicle[1].veh)
					FLOAT fEngineHealth = GET_VEHICLE_ENGINE_HEALTH(wave_0_enemy_vehicle[1].veh)

					// @SBA light it up if it's taken enough damage
					IF fEngineHealth < 850.0
					OR iSuvDamageCount >= 5 // Some hits register no damage to the engine, so this gets around that
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(wave_0_enemy_vehicle[1].veh, buddy.ped)
						// start fire
						SET_VEHICLE_PETROL_TANK_HEALTH(wave_0_enemy_vehicle[1].veh, -1.0)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(wave_0_enemy_vehicle[1].veh)
						iSuvDamageCount = 0
						CPRINTLN(DEBUG_MISSION,"wave_0_leaking_petrol_system: Setting Blue SUV on fire.")
					ENDIF
					// Check if anyone has damaged SUV, increase count
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(wave_0_enemy_vehicle[1].veh)
						CPRINTLN(DEBUG_MISSION,"wave_0_enemy_vehicle[1] health = ", fEngineHealth)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(wave_0_enemy_vehicle[1].veh)
						iSuvDamageCount ++
					ENDIF
				ENDIF
			endif 
		
		endif 
	endif 
	
endproc 

proc wave_0_fence_destruction()

	if not fence_shot

		if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)

			if is_ped_in_angled_area_lk(wave_0_inside_enemy[0].ped, <<1368.573, 3616.663, 33.898>>, <<1382.762, 3621.828, 36.898>>, 12.700)    
			or is_ped_in_angled_area_lk(wave_0_inside_enemy[1].ped, <<1368.573, 3616.663, 33.898>>, <<1382.762, 3621.828, 36.898>>, 12.700) 
			or is_ped_in_angled_area_lk(wave_0_inside_enemy[2].ped, <<1368.573, 3616.663, 33.898>>, <<1382.762, 3621.828, 36.898>>, 12.700) 
			or is_ped_in_angled_area_lk(wave_0_inside_enemy[3].ped, <<1368.573, 3616.663, 33.898>>, <<1382.762, 3621.828, 36.898>>, 12.700) 
			
				shoot_single_bullet_between_coords(<<1374.2, 3616.7, 36.2>>, <<1381.3, 3621.5, 38.3>>, 100, true, WEAPONTYPE_ASSAULTRIFLE)  
				shoot_single_bullet_between_coords(<<1374.2, 3616.7, 36.2>>, <<1381.5, 3620.9, 38.2>>, 100, true, WEAPONTYPE_ASSAULTRIFLE)  
				shoot_single_bullet_between_coords(<<1374.2, 3616.7, 36.2>>, <<1381.7, 3620.0, 38.3>>, 100, true, WEAPONTYPE_ASSAULTRIFLE)  
				shoot_single_bullet_between_coords(<<1374.2, 3616.7, 36.2>>, <<1382.1, 3619.3, 38.3>>, 100, true, WEAPONTYPE_ASSAULTRIFLE)  
				shoot_single_bullet_between_coords(<<1374.2, 3616.7, 36.2>>, <<1382.2, 3618.5, 38.3>>, 100, true, WEAPONTYPE_ASSAULTRIFLE)  
				
				fence_shot = true 
				
			endif 
		endif 
		
	endif 

endproc 

proc wave_0_audio_scene_system()
			
	switch wave_0_audio_scene_system_status 
	
		case 0
		
			IF wave_0_master_flow_system_status > 0
			
				IF DOES_ENTITY_EXIST(wave_0_enemy_vehicle[0].veh)
					IF IS_VEHICLE_DRIVEABLE(wave_0_enemy_vehicle[0].veh)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[0].veh, "CHI_1_FIRST_TRUCKS_ARRIVE_GROUP")
					ENDIF 
				ENDIF
				IF DOES_ENTITY_EXIST(wave_0_enemy_vehicle[1].veh)
					IF IS_VEHICLE_DRIVEABLE(wave_0_enemy_vehicle[1].veh)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[1].veh, "CHI_1_FIRST_TRUCKS_ARRIVE_GROUP")
					ENDIF 
				ENDIF				
				wave_0_audio_scene_system_status++
			ENDIF
		break 
		
		case 1
			if wave_0_master_flow_system_status > 3

				vector player_pos
						
				player_pos = GET_ENTITY_COORDS(player_ped_id())
				
				if lk_timer(original_time, 25000)
				or player_pos.z < 37.00
				or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1390.113, 3615.582, 37.931>>, <<1386.932, 3624.107, 41.431>>, 12.200)
				or (is_entity_in_angled_area(player_ped_id(), <<1390.858, 3599.007, 37.980>>, <<1389.319, 3603.235, 40.680>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
				or (is_entity_in_angled_area(player_ped_id(), <<1388.832, 3604.578, 37.747>>, <<1387.122, 3609.276, 40.847>>, 2.3) and is_sphere_visible(<<1373.8, 3615.3, 35.4>>, 1.0))
				or is_entity_in_angled_area(player_ped_id(), <<1400.469, 3607.616, 38.006>>, <<1399.769, 3609.490, 40.506>>, 1.2) 

					start_audio_scene("CHI_1_SHOOTOUT_02")
					
					IF DOES_ENTITY_EXIST(wave_0_enemy_vehicle[0].veh)
						IF IS_VEHICLE_DRIVEABLE(wave_0_enemy_vehicle[0].veh)
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[0].veh)
						ELSE 
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[0].veh)
						ENDIF
					ENDIF

					IF DOES_ENTITY_EXIST(wave_0_enemy_vehicle[1].veh)
						IF IS_VEHICLE_DRIVEABLE(wave_0_enemy_vehicle[1].veh)
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[1].veh)
						ELSE 
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(wave_0_enemy_vehicle[1].veh)
						ENDIF
					ENDIF
					
					wave_0_audio_scene_system_status++
					
				endif 
			endif 
		break 
		
		case 2
		
		break 
		
	endswitch 
			
endproc 

func bool chinese_wave_0_system()

	// @SBA - B*1195400 - rarely, a ped falls through the world and cannot be killed in game play
	// this kills him if he is below ground
	KILL_ANY_ERRANT_ENEMY(wave_0_enemy)
	KILL_ANY_ERRANT_ENEMY(wave_0_inside_enemy)
	
	// @SBA - handle dead vehicle blips
	REMOVE_DEAD_VEHICLE_BLIPS(wave_0_enemy_vehicle)

	if wave_0_master_flow_system()
		mission_flow = wave_2_system
		return true
	endif 
	
	wave_0_dialogue_system()

	wave_0_buddy_ai_system()
	
	wave_0_explode_vehicle_system()
	
	wave_0_leaking_petrol_system()
	
//	wave_0_vehicle_destruction_system()
	
	//wave_0_assisted_nodes_system()

	wave_0_ai_system()

	wave_0_fence_destruction()
	
	wave_0_audio_scene_system()
	
	if is_ped_inside_interior(player_ped_id(), <<1392.7360, 3602.5989, 38.5>>, "v_methlab")
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	endif 
	
	
	//set_radar_zoom()
	
	//DEBUG - comment back in when doing uber recording
	//uber_vehicle_system()
	
	return false 

endfunc



proc setup_final_wave_of_wave_2()

	for i = 5 to 5 //6 7			

		SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[i])
		start_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, wave_2_enemy_vehicle[i].recording_number, "lkmethlab")
		
		if i = 5
			skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, 3000)
		endif 
		
		//DISPLAY_PLAYBACK_RECORDED_VEHICLE(wave_2_enemy_vehicle[i].veh, rdm_wholeline)
		
	endfor

	setup_enemy_in_vehicle(wave_2_enemy[12], wave_2_enemy_vehicle[5].veh) 
	set_ped_flee_attributes(wave_2_enemy[12].ped, fa_use_vehicle, true)
	
	setup_enemy_in_vehicle(wave_2_enemy[13], wave_2_enemy_vehicle[5].veh, vs_front_right) 
	
	wave_2_enemy[14].created = true
	wave_2_enemy[15].created = true
	
	wave_2_enemy[16].created = true
	wave_2_enemy[17].created = true
	
	//create_conversation(scripted_speech[0], "methaud", "CHIN1_CAR", conv_priority_medium)
	
	//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
	KILL_FACE_TO_FACE_CONVERSATION()
	ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "methaud", "CHIN1_CAR", CONV_PRIORITY_MEDIUM)

	set_entity_proofs(player_ped_id(), false, false, false, false, false)
	
	//reset's the god text
	SET_LABEL_AS_TRIGGERED("METH_GOD_18", false)
	
	// piggy back in here
	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )

endproc 

///PURPOSE: Proofs the player for 1.5 seconds after the grenade launcher cutscene incase they get shot whilst going into cover
proc proofing_system_after_grenade_launcher_cutscene()

	if not stop_player_proofing
		if player_grenade_launcher_cutscene_status = 99
	
			if lk_timer(proofing_time, 1500)
				set_entity_proofs(player_ped_id(), false, false, false, false, false)
				stop_player_proofing = true
			else 
				set_entity_proofs(player_ped_id(), true, true, true, true, true)
			endif 
		endif 
	endif

endproc 

func bool wave_2_master_flow_system()

//	printint(wave_2_master_flow_system_status)
//	printnl()

	// @SBA - if player picks up grenade launcher himself, set correct ammo ammount
	SET_AMMO_FOR_PICKED_UP_GRENADE_LAUNCHER()
	
	switch wave_2_master_flow_system_status 
	
		case 0
			CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
			//truck arriving at the back
			SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[2])
			start_playback_recorded_vehicle(wave_2_enemy_vehicle[2].veh, wave_2_enemy_vehicle[2].recording_number, "lkmethlab")
			
			setup_enemy_in_vehicle(wave_2_enemy[0], wave_2_enemy_vehicle[2].veh)
			setup_enemy_in_vehicle(wave_2_enemy[1], wave_2_enemy_vehicle[2].veh, vs_front_right)
			setup_enemy_in_vehicle(wave_2_enemy[2], wave_2_enemy_vehicle[2].veh, vs_back_left)
			setup_enemy_in_vehicle(wave_2_enemy[3], wave_2_enemy_vehicle[2].veh, vs_back_right)
						
			// @SBA - make grenade launcher pickup
			SETUP_GRENADE_LAUNCHER_PICKUP()
			
			RESET_CHEF_WARNINGS_FOR_NEXT_WAVE()
			
			// piggy back in here
			SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
					
			wave_2_master_flow_system_status++
			CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)

		break 

		// Splitting up the enemy creation somewhat
		CASE 1
			SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[3])
			start_playback_recorded_vehicle(wave_2_enemy_vehicle[3].veh, wave_2_enemy_vehicle[3].recording_number, "lkmethlab")
		
			setup_enemy_in_vehicle(wave_2_enemy[4], wave_2_enemy_vehicle[3].veh)
			wave_2_master_flow_system_status++
			CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
		BREAK 
		CASE 2
			SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[4])
			start_playback_recorded_vehicle(wave_2_enemy_vehicle[4].veh, wave_2_enemy_vehicle[4].recording_number, "lkmethlab")
			skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[4].veh, 4500)
			
			setup_enemy_in_vehicle(wave_2_enemy[5], wave_2_enemy_vehicle[4].veh)
			wave_2_master_flow_system_status++
			CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
		BREAK 

		case 3
		
			if is_player_infront_of_door() //or lk_timer(original_time, 15000)
			
				// @SBA - canceling this here - it was causing the script to hang on a z-menu restart from earlier checkpoints
				// if the event had never triggered
				CANCEL_MUSIC_EVENT("CHN1_G_LAUNCHER")
				trigger_music_event("CHN1_BACK_ROOF")

				SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[6])
				start_playback_recorded_vehicle(wave_2_enemy_vehicle[6].veh, wave_2_enemy_vehicle[6].recording_number, "lkmethlab")

				setup_enemy_in_vehicle(wave_2_enemy[18], wave_2_enemy_vehicle[6].veh)
				setup_enemy_in_vehicle(wave_2_enemy[19], wave_2_enemy_vehicle[6].veh, vs_front_right)
		
				kill_face_to_face_conversation()
				wave_2_dialogue_system_status = 3
				
				bBackStairsDoorClosed = FALSE
				
				wave_2_master_flow_system_status++
				CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
			ELIF NOT bChefComeOn
				IF buddy_on_rooftop()
					wave_2_dialogue_system_status = 2
				ENDIF
			endif 
		
		break 
		
		case 4
		
			// Shutting door to improve frame rate
			IF NOT bBackStairsDoorClosed
				BOOL bShutDoor
				IF buddy_on_rooftop()
					IF IS_PLAYER_IN_TRIGGER_BOX(tbBackRoofVolume)
						bShutDoor = TRUE
					ELIF NOT IS_SPHERE_VISIBLE(vDoorToBackStairs, 1.0)
						bShutDoor = TRUE
					ENDIF
					IF bShutDoor
						SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, FALSE, 0.0, 0.0, 0.0)
						bBackStairsDoorClosed = TRUE
					ENDIF
				ENDIF
			ENDIF

			if (ped_structure_get_total_number_of_enemies_dead(wave_2_enemy) >= 4) //5 
				//CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: Enough enemies are dead!")
				IF prepare_music_event("CHN1_G_LAUNCHER")
					//CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: music prepared!")
					// @SBA - if the player has already gotten the grenade launcher, don't bother with the cutscene
					IF NOT bSkipGrenLaunchCutscene
						// @SBA - also ensure no living enemey is on the roof or back room (may need a wider check than this?)
						IF NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackRoofVolume)
							IF NOT IS_ANY_PED_IN_TRIGGER_BOX(wave_2_enemy, tbBackUpstairsRoomVolume) 
							AND NOT IS_ANY_PED_IN_SPECIFIC_ROOM(wave_2_enemy, (<<1392.2, 3614.1, 39.0>>), "v_39_UpperRm1")
								CPRINTLN(DEBUG_MISSION,"*** GOING TO GRENADE LAUNCHER CUTSCENE ***")
								// @SBA - we don't want the player to pick up the launcher while waiting for the cutscene to play
								//SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(PLAYER_ID(), PICKUP_WEAPON_GRENADELAUNCHER, FALSE)
								cutscene_trigger_time = get_game_timer()
								set_label_as_triggered("chin_roof_0", FALSE)
								wave_2_master_flow_system_status++
								CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
							ENDIF
						ENDIF
					// @SBA - skipping cutscene
					ELSE
						CPRINTLN(DEBUG_MISSION,"*** SKIPPING THE GRENADE LAUNCHER CUTSCENE ***")
						wave_2_master_flow_system_status++
						CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
					ENDIF
				endif
			ENDIF
		
		break 
		
		case 5
		
			// @SBA - are we to play the cutscene?
			IF NOT bSkipGrenLaunchCutscene
				if player_grenade_launcher_cutscene_system()
				
					//ram cutscene is done ingame. We can not see the vehicles warping if they need to be reset. Adding this here as it is a safe place after the camera cut.
					reset_wave_0_vehicles_for_ram_gates_cutscene(true)
					
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Wave 2 after grenade launcher cutscene")
					wave_2_master_flow_system_status++
					CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: FROM CUTSCENE, GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
				
				else 
				
					//create peds when player is not near the cook to get the grenade launcher
					if not grenade_launcher_cutscene_playing
						if lk_timer(cutscene_trigger_time, 2000)
							create_second_wave_of_wave_2_enemies()
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Wave 2 after grenade launcher cutscene")
							wave_2_master_flow_system_status++
							CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: FROM WAITING FOR CUTSCENE, GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
						endif
					endif 
				
				endif
			// @SBA - if cutscene skipped, just do this stuff and advance
			ELSE 
//				if lk_timer(cutscene_trigger_time, 2000)
					create_second_wave_of_wave_2_enemies()
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Wave 2 after grenade launcher cutscene")
					wave_2_master_flow_system_status++
					CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: FROM SKIPPED CUTSCENE, GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
//				endif
			ENDIF

		break 

		case 6
			// fade in if needed (replay)
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			
			// @SBA - are we playing the cutscene?
			IF NOT bSkipGrenLaunchCutscene
				player_grenade_launcher_cutscene_system()
				proofing_system_after_grenade_launcher_cutscene()
			ENDIF
			
			// handler for next vehicle full o' bad guys
			create_third_wave_of_wave_2_enemies()

			//if enemy 6, 7, 8, 9 are dead elif not vehicle driveable 0 1 or car recording not going on
		
			if is_vehicle_driveable(wave_2_enemy_vehicle[0].veh)
				if not is_playback_going_on_for_vehicle(wave_2_enemy_vehicle[0].veh)
			
					setup_final_wave_of_wave_2()
					
					wave_2_master_flow_system_status++
					
				endif 
			
			 else   
			 	
				setup_final_wave_of_wave_2()
			
				wave_2_master_flow_system_status++
				CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
			endif 

		break 
		
		case 7
		
			// @SBA - are we playing the cutscene?
			IF NOT bSkipGrenLaunchCutscene
				player_grenade_launcher_cutscene_system()
				proofing_system_after_grenade_launcher_cutscene()
			ENDIF
			
			if not grenade_launcher_cutscene_playing
				if ped_structure_are_all_enemies_dead(wave_2_enemy)  //@SBA should we set to TRUE to keep dead bodies?
					//if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(buddy.ped)) < 30.00
				
						//request ortega model if ortega has not been killed in trevor 1
						if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
							request_model(get_npc_ped_model(char_ortega))
						endif 
						
						clear_prints()
						
						// Clean up last arriving vehicle blip, if needed
						REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[5])
						
						//create_conversation(scripted_speech[0], "methaud", "CHIN1_GO", conv_priority_medium)
						KILL_FACE_TO_FACE_CONVERSATION()
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "methaud", "CHIN1_GO", CONV_PRIORITY_MEDIUM)
			
						original_time = get_game_timer()
						
						wave_2_master_flow_system_status++
						CPRINTLN(DEBUG_MISSION,"wave_2_master_flow_system: GOING TO wave_2_master_flow_system_status = ", wave_2_master_flow_system_status)
						
//					else 
//					
//						if not has_label_been_triggered("METH_GOD_18")
//							print_now("METH_GOD_18", default_god_text_time, 1) 
//							SET_LABEL_AS_TRIGGERED("METH_GOD_18", true)
//						endif 
					
					//endif 
					
				endif 
			endif 
			
		break 
		
		case 8
		
			if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
				request_model(get_npc_ped_model(char_ortega))
			endif 
		
			if lk_timer(original_time, 500)
					
//				if (not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) and has_model_loaded(get_npc_ped_model(char_ortega)))
//				or GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
//					script_assert("test")
//					return true 
//				endif 
				
				if (not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) and has_model_loaded(get_npc_ped_model(char_ortega)))
				or GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)

					if is_audio_scene_active("CHI_1_SHOOTOUT_03")
						stop_audio_scene("CHI_1_SHOOTOUT_03")
					endif 
				
					if is_audio_scene_active("CHI_1_SHOOTOUT_GRENADE_LAUNCHER")
						stop_audio_scene("CHI_1_SHOOTOUT_GRENADE_LAUNCHER")
					endif 
					
					start_audio_scene("CHI_1_GET_TO_SHOOTOUT_04")

					trigger_music_event("CHN1_FIRST_FLOOR")
					
					return true 
				endif 
				
			endif 
			
			//EQUIP_BEST_PLAYER_WEAPON()
		
		break 
		
		case 22
		
		break 
		
	endswitch 
	
	return false 

endfunc 


//@SBA - plays a line from an enemy (if one's alive) when a grenade goes off
PROC HANDLE_ENEMY_GREN_LAUNCH_DIALOGUE()

	SWITCH iEnemyGrenLaunchDialogue
		CASE 0
		
			// check for grenade explosion
			IF is_grenade_launcher_explosion_in_car_park()
				// start a timer so dialogue doesn't happen during the explosion
				START_TIMER_NOW(tmrGrenLaunchTimer)
				iEnemyGrenLaunchDialogue ++
				CPRINTLN(DEBUG_MISSION,"HANDLE_ENEMY_GREN_LAUNCH_DIALOGUE: Boom!  Going to: ", iEnemyGrenLaunchDialogue)
			ENDIF
		
		BREAK
		
		CASE 1
		
			// When timer is up, try to find a living ped to say the dialogue
			IF TIMER_DO_ONCE_WHEN_READY(tmrGrenLaunchTimer, kfGrenLaunchWaitTime)
				// Check backyard for a living enemy
				piEnemyToSpeak = GET_FIRST_LIVING_PED_IN_TRIGGER_BOX(wave_2_enemy,tbBackYardVolume)
				// another bite at the apple to find a living enenmy
				IF piEnemyToSpeak = NULL
					piEnemyToSpeak = GET_FIRST_LIVING_PED_IN_TRIGGER_BOX(wave_2_enemy,tbBackGateVolume)
				ENDIF
				// Remove old data and assign a speaker, if there is one available
				IF piEnemyToSpeak != NULL
					ADD_PED_FOR_DIALOGUE(scripted_speech[0], 8, piEnemyToSpeak, "VAGOS")
				ENDIF
				// Canceling timer for some additional safety
				CANCEL_TIMER(tmrGrenLaunchTimer)
				iEnemyGrenLaunchDialogue ++
				CPRINTLN(DEBUG_MISSION,"HANDLE_ENEMY_GREN_LAUNCH_DIALOGUE: Going to: ", iEnemyGrenLaunchDialogue)
			ENDIF
			
		BREAK
		
		CASE 2
			
			// Play conversation if we have a valid ped and if nothing else is displayed
			IF piEnemyToSpeak != NULL
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
					IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_GREN", CONV_PRIORITY_HIGH) 
						iEnemyGrenLaunchDialogue ++
						CPRINTLN(DEBUG_MISSION,"HANDLE_ENEMY_GREN_LAUNCH_DIALOGUE: Going to: ", iEnemyGrenLaunchDialogue)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
			// nothing
		BREAK
	
	ENDSWITCH
	
ENDPROC

proc wave_2_dialogue_system()

	// @SBA - in case enemies get inside
	HANDLE_CHEF_INFILTRATION_WARNINGS_WAVE_2()
	
	// @SBA - enemy line after first explosion
	HANDLE_ENEMY_GREN_LAUNCH_DIALOGUE()

	switch wave_2_dialogue_system_status
	
		case 0
		
			prepare_music_event("CHN1_HEAD_TO_BACK")
			
			IF NOT bPlayGunFire
				PLAY_SOUND_FROM_COORD(-1, "CR_WEAPONS_BURST_SHORT", (<<1586.9836, 3695.9495, 33.9163>>))
				CPRINTLN(DEBUG_MISSION, "Should be playing gunfire sound")
				bPlayGunFire = TRUE
			elif create_conversation(scripted_speech[0], "methaud", "chang_info0", conv_priority_medium)
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0)
				
				trigger_music_event("CHN1_HEAD_TO_BACK")
				wave_2_dialogue_system_status++
			endif 

		break 
		
		case 1
		
			if not is_scripted_conversation_ongoing()
				// @SBA - give player instruction instead
//				if create_conversation(scripted_speech[0], "methaud", "chin_follow0", conv_priority_medium)
					PRINT_NOW("METH_GOD_3", DEFAULT_GOD_TEXT_TIME, 1) // Follow Chef
					wave_2_dialogue_system_status = 22
//				endif 
			endif 
		
		break 
		
		CASE 2
			IF IS_PLAYER_IN_TRIGGER_BOX(tbBackFullRoofVolume)
				wave_2_dialogue_system_status = 22
				bChefComeOn = TRUE
				EXIT
			ENDIF

			if not is_any_text_being_displayed(locates_data)
				IF create_conversation(scripted_speech[0], "methaud", "chin_comeon", conv_priority_medium) 
					wave_2_dialogue_system_status = 22
					bChefComeOn = TRUE
				ENDIF
			ENDIF
		BREAK
		
		case 3
		
			if create_conversation(scripted_speech[0], "methaud", "buddy_cover", conv_priority_medium) 
				wave_2_dialogue_system_status++
			endif 
		
		break 
		
		case 4
		
			if not is_scripted_conversation_ongoing()
				
				wave_2_dialogue_system_status++
			
			endif 
		
		break 
		
		case 5
		
			
		
		break 
		
	endswitch 
	
	if lk_timer(dialogue_time, 13000)
		if is_ped_shooting(player_ped_id())
			if not is_any_text_being_displayed(locates_data)
				if is_enemy_near_player(wave_2_enemy, 50.00)
					if create_conversation(scripted_speech[0], "methaud", "attack_0", conv_priority_low) 
						dialogue_time = get_game_timer()
					endif 
				endif 
			endif 
		endif 
	endif 

endproc 

proc wave_2_buddy_ai_system()

	if allow_buddy_ai_system
	
		// @SBA - handle Chef adandonment during wave 2 fight
		IF NOT wave_2_complete
			IF buddy_on_rooftop()
				HANDLE_WAVE_2_CHEF_DEMISE()
				HANDLE_ABANDONMENT_MESSAGE(HAS_PLAYER_ABANDONED_CHEF_WAVE_2())
			ENDIF
			HANDLE_CHEF_DAMAGED_DIALOGUE()
		ENDIF
	
		switch wave_2_buddy_ai_system_status 
		
			case 0
 
  				// @SBA - make sure Chef's health is set to max for this wave
				SET_HEALTH_FOR_PED(buddy.ped, buddy.health)

				SET_PED_PATH_CAN_USE_CLIMBOVERS(buddy.ped, true)
				SET_PED_CAN_PEEK_IN_COVER(buddy.ped, TRUE)
				
				set_ped_accuracy(buddy.ped, 40)
				
				set_blocking_of_non_temporary_events(buddy.ped, true)
				clear_ped_tasks(buddy.ped)
				
				if is_ped_in_specific_room(buddy.ped, <<1390.91, 3608.21, 38.91>>, "V_39_UpperRm3")
					buddy_waypoint_target_node = 16
					buddy_waypoint_string = "methlab1"
					
					open_sequence_task(seq)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, buddy_waypoint_string, 2, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS) // EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | 
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
					
				else 
					buddy_waypoint_target_node = 19//23
					buddy_waypoint_string = "methlab5"
					
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<1387.2665, 3615.9451, 37.9259>>, PEDMOVE_WALK, -1, default_navmesh_radius, enav_no_stopping) 
						TASK_FOLLOW_WAYPOINT_RECORDING(null, buddy_waypoint_string, 2, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS) // EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | 
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
					
				endif
				
				buddy_time = get_game_timer()
				
				wave_2_buddy_ai_system_status++
				
			break 
		
			case 1
				SET_PED_RESET_FLAG(buddy.ped, PRF_UseKinematicPhysics, TRUE)
			
				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(buddy.ped, 2.0) 
				endif 
				
				if waypoint_recording_get_coord(buddy_waypoint_string, buddy_waypoint_target_node, target_waypoint_pos)  
					if not IS_ENTITY_AT_COORD(buddy.ped, target_waypoint_pos, <<1.0, 1.0, 1.6>>, false, true)
						if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
						
							if waypoint_recording_get_closest_waypoint(buddy_waypoint_string, GET_ENTITY_COORDS(buddy.ped), waypoint_node_pos)
							
								waypoint_node_pos += 1
							
								if waypoint_node_pos > buddy_waypoint_target_node
									waypoint_node_pos = buddy_waypoint_target_node
								endif 
							
								set_blocking_of_non_temporary_events(buddy.ped, true)
								clear_ped_tasks(buddy.ped)
								
								open_sequence_task(seq)
									task_follow_waypoint_recording(null, buddy_waypoint_string, waypoint_node_pos, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
							endif 
						
						// failsafe in case Chef get stuck somewhere
						ELIF lk_timer(buddy_time, 15000)
						OR IS_PLAYER_IN_TRIGGER_BOX(tbBackRoofVolume)
							IF NOT buddy_on_rooftop()
								wave_2_buddy_ai_system_status = 3
							ENDIF
						endif 
					
					else 
					
						use_main_cover_point = true 
						refresh_the_task = false
					
//						if not IS_PLAYER_STEALING_PEDS_DESTINATION(buddy.ped, buddy_cover_point_data[2].pos, 2.0)
//						
//							open_sequence_task(seq)
//								task_put_ped_directly_into_cover(null, buddy_cover_point_data[2].pos, -1)
//							close_sequence_task(seq)
//							task_perform_sequence(buddy.ped, seq)
//							clear_sequence_task(seq)
//							
//						else 
//						
//							open_sequence_task(seq)
//								task_follow_nav_mesh_to_coord(null, buddy_cover_point_data[3].pos, pedmove_run, -1, 0.5)
//								//task_put_ped_directly_into_cover(null, <<1408.0914, 3614.9163, 37.9421>>, -1)
//							close_sequence_task(seq)
//							task_perform_sequence(buddy.ped, seq)
//							clear_sequence_task(seq)
//						
//						endif 

						wave_2_buddy_ai_system_status++
					
					endif 
					
				endif

			break 
			
			case 2
			
				if buddy_on_rooftop()
					if not is_ped_in_cover(buddy.ped)

						buddy_time = get_game_timer()
						
						//could make function return true once in cover for 1000ms 
						get_buddy_into_cover(buddy.ped, buddy_cover_point_data[2], buddy_cover_point_data[3], use_main_cover_point, refresh_the_task)

					else 
					
						if lk_timer(buddy_time, 1000)
					
							set_blocking_of_non_temporary_events(buddy.ped, false)
							set_ped_sphere_defensive_area(buddy.ped, <<1406.70, 3615.67, 38.01>>, 2.0)
							
							//use is check for buddy_cover_point_data[2] and buddy_cover_point_data[3]
							//for setting sphere
							
							if not is_ped_injured(wave_2_enemy[6].ped)
								task_combat_ped(buddy.ped, wave_2_enemy[6].ped)
								
							elif not is_ped_injured(wave_2_enemy[7].ped)
								task_combat_ped(buddy.ped, wave_2_enemy[7].ped)
							endif 
							
							wave_2_buddy_ai_system_status++
							
						endif 
						
					endif 
					
				else
				
					set_blocking_of_non_temporary_events(buddy.ped, true)
					clear_ped_tasks(buddy.ped)
					
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<1406.70, 3615.67, 38.01>>, pedmove_run, -1)
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
					
					wave_2_buddy_ai_system_status = 4

				endif 
					
			break 
			
			case 3 
			
				if not buddy_on_rooftop()
					
					set_blocking_of_non_temporary_events(buddy.ped, true)
					clear_ped_tasks(buddy.ped)
					
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<1406.70, 3615.67, 38.01>>, pedmove_run, -1)
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
				
					wave_2_buddy_ai_system_status = 4

				endif 
			
			break 
			
			case 4
			
				if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
					if buddy_on_rooftop()

						wave_2_buddy_ai_system_status = 2
					
					else 
				
						wave_2_buddy_ai_system_status = 3
						
					endif 
				
				endif 
			
			break 
		
		endswitch 
		
	endif 
	
endproc 


proc wave_2_assisted_nodes_system()

	switch wave_2_assisted_nodes_system_status
	
		case 0
			
			ASSISTED_MOVEMENT_REQUEST_ROUTE("Wave01457s")
			
			wave_2_assisted_nodes_system_status++

		break 
		
		case 1
		
			if wave_2_master_flow_system_status > 7
				
				ASSISTED_MOVEMENT_remove_ROUTE("Wave01457s")
				
				wave_2_assisted_nodes_system_status++
			endif
		
		break 
		
		case 2
		

		break 
		
		case 3
		
		
		break 
		
		case 22
		
		break 
	
	endswitch 

endproc 

proc wave_2_vehicle_destruction_system()
	
//	vector front_left_offset = <<-0.78, 1.4, -0.42>>  
//	vector front_right_offset = <<0.78, 1.4, -0.42>> 
//	vector rear_right_offset = <<0.78, -1.47, -0.42>> 
//	vector rear_left_offset = <<-0.78, -1.47, -0.42>> 
//	float radius_check = 1.22
	
	vector bullet_front_left_offset = <<-0.78, 1.32, -0.4>>  
	vector bullet_front_right_offset = <<0.78, 1.32, -0.4>> 
	vector bullet_back_right_offset = <<0.78, -1.64, -0.4>> 
	vector bullet_back_left_offset = <<-0.78, -1.64, -0.4>> 
	float bullet_radius_check = 1.55
	
	vector deformation_front_left_offset = <<0.900, 1.340, 0.15>>
	vector deformation_front_right_offset = <<-0.900, 1.30, 0.15>>
	vector deformation_back_left_offset = <<-0.900, -1.3, 0.15>>
	vector deformation_back_right_offset = <<0.9, -1.3, 0.15>>
	float damage = 140.00
	float deformation = 140.00 

	
	if is_player_control_on(player_id())
	
		for i = 0 to count_of(wave_2_enemy_vehicle) - 1
			
			if i <> 3
			and i <> 4
			
				if is_vehicle_driveable(wave_2_enemy_vehicle[i].veh)
				
					//POSSIBLY MAKE A FUNCTION OUT OF THIS AND PASS IN 8 PARAMETERS
					if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_2_enemy_vehicle[i].veh, bullet_front_left_offset), bullet_radius_check)	
						if not is_vehicle_tyre_burst(wave_2_enemy_vehicle[i].veh, sc_wheel_car_front_left)
							SET_VEHICLE_TYRE_BURST(wave_2_enemy_vehicle[i].veh, sc_wheel_car_front_left)
							SET_VEHICLE_DAMAGE(wave_2_enemy_vehicle[i].veh, deformation_front_left_offset, damage, deformation, true) 
							SMASH_VEHICLE_WINDOW(wave_2_enemy_vehicle[i].veh, sc_window_front_left)
						endif 
					endif 
					
					if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_2_enemy_vehicle[i].veh, bullet_front_right_offset), bullet_radius_check)	
						if not is_vehicle_tyre_burst(wave_2_enemy_vehicle[i].veh, sc_wheel_car_front_right)
							SET_VEHICLE_TYRE_BURST(wave_2_enemy_vehicle[i].veh, sc_wheel_car_front_right)
							SET_VEHICLE_DAMAGE(wave_2_enemy_vehicle[i].veh, deformation_front_right_offset, damage, deformation, true) 
							SMASH_VEHICLE_WINDOW(wave_2_enemy_vehicle[i].veh, sc_window_front_right)
						endif
					endif 
					
					if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_2_enemy_vehicle[i].veh, bullet_back_left_offset), bullet_radius_check)	
						if not is_vehicle_tyre_burst(wave_2_enemy_vehicle[i].veh, sc_wheel_car_rear_left)
							SET_VEHICLE_TYRE_BURST(wave_2_enemy_vehicle[i].veh, sc_wheel_car_rear_left)
							SET_VEHICLE_DAMAGE(wave_2_enemy_vehicle[i].veh, deformation_back_left_offset, damage, deformation, true) 
							SMASH_VEHICLE_WINDOW(wave_2_enemy_vehicle[i].veh, sc_window_rear_left)
						endif 
					endif 
					
					if HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(wave_2_enemy_vehicle[i].veh, bullet_back_right_offset), bullet_radius_check)	
						if not is_vehicle_tyre_burst(wave_2_enemy_vehicle[i].veh, sc_wheel_car_rear_right)
							SET_VEHICLE_TYRE_BURST(wave_2_enemy_vehicle[i].veh, sc_wheel_car_rear_right)
							SET_VEHICLE_DAMAGE(wave_2_enemy_vehicle[i].veh, deformation_back_right_offset, damage, deformation, true) 
							SMASH_VEHICLE_WINDOW(wave_2_enemy_vehicle[i].veh, sc_window_rear_right)
						endif 	
					endif 
					
				endif 
				
			endif 
			
		endfor 
		
	endif 
	
endproc

proc wave_2_audio_scene_system()
		
	switch wave_2_audio_scene_system_status 
	
		case 0
		
			if is_player_infront_of_door()
			
				if is_audio_scene_active("CHI_1_GET_TO_SHOOTOUT_03")
					stop_AUDIO_SCENE("CHI_1_GET_TO_SHOOTOUT_03")
				endif 
				
				start_audio_scene("CHI_1_SHOOTOUT_03")
				
				wave_2_audio_scene_system_status++
				
			endif 
		
		break 
		
		case 1
		
			if grenade_launcher_cutscene_playing
			
				if is_audio_scene_active("CHI_1_SHOOTOUT_03")
					stop_AUDIO_SCENE("CHI_1_SHOOTOUT_03")
				endif 
				
				start_audio_scene("CHI_1_CATCH_GRENADE_LAUNCHER")
				
				wave_2_audio_scene_system_status++
				
			endif 	
		
		break 
		
		case 2
		
			if not grenade_launcher_cutscene_playing
			
				if is_audio_scene_active("CHI_1_CATCH_GRENADE_LAUNCHER")
					stop_AUDIO_SCENE("CHI_1_CATCH_GRENADE_LAUNCHER")
				endif 
				
				start_audio_scene("CHI_1_SHOOTOUT_GRENADE_LAUNCHER")
				
				wave_2_audio_scene_system_status++
				
			endif 
		
		break 
		
		case 3
		
			
		break 
		
	endswitch 
	
endproc 

func bool chinese_wave_2_system()

	// @SBA - B*1195400 - rarely, a ped falls through the world and cannot be killed in game play
	// this kills him if he is below ground
	KILL_ANY_ERRANT_ENEMY(wave_2_enemy)
	
	// @SBA - handle dead vehicle blips
	REMOVE_DEAD_VEHICLE_BLIPS(wave_2_enemy_vehicle)

	if wave_2_master_flow_system()
		mission_flow = wave_3_system
		return true 
	endif 
	
	wave_2_dialogue_system()

	wave_2_buddy_ai_system()
	

	//wave_2_vehicle_destruction_system()
	
	wave_2_assisted_nodes_system()

	wave_2_ai_system()
	
	wave_2_audio_scene_system()
	
	if is_ped_inside_interior(player_ped_id(), <<1392.7360, 3602.5989, 38.5>>, "v_methlab")
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	endif 

	return false  

endfunc 

PROC DO_ICE_BOX_OBJECTIVE_STUFF()
	// Print objective
	IF NOT HAS_LABEL_BEEN_TRIGGERED("METH_GOD_20")
		PRINT_NOW("METH_GOD_20", default_god_text_time, 1)  // Go To Icebox
		SET_LABEL_AS_TRIGGERED("METH_GOD_20", TRUE)
	ENDIF
	// remove buddy blip
	IF DOES_BLIP_EXIST(buddy.blip)
		REMOVE_BLIP(buddy.blip)
	ENDIF
	// display ice box blip
	IF NOT does_blip_exist(ice_box_blip)
		ice_box_blip = create_blip_for_coord(<<1388.0475, 3598.6162, 33.8954>>)
	ENDIF
	// Start timer for next dialogue
	CANCEL_TIMER(tmrIceBoxReminder)
	START_TIMER_NOW(tmrIceBoxReminder)
ENDPROC


PROC SET_ICY_VERSIONS_OF_PEDS()
	IF NOT IS_PED_INJURED(cheng.ped)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_HEAD, 0, 1)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_HAIR, 0, 1)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_TORSO, 2, 1)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_LEG, 0, 1)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_HAND, 0, 1)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(cheng.ped, PED_COMP_JBIB, 1, 1)
		SET_PED_PROP_INDEX(cheng.ped, ANCHOR_EYES,0)
	ENDIF

	IF NOT IS_PED_INJURED(translator.ped)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_HEAD, 0, 1)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_HAIR, 0, 1)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_TORSO, 1, 1)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_LEG, 1, 1)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_HAND, 0, 1)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_SPECIAL2, 1, 1)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(translator.ped, PED_COMP_JBIB, 1, 0)
		SET_PED_PROP_INDEX(translator.ped, ANCHOR_EYES,0)
	ENDIF
ENDPROC

func bool wave_3_master_flow_system()

	HANDLE_VEHICLE_TOO_NEAR_ICE_BOX()

	switch wave_3_master_flow_system_status 
	
		case 0
		
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1400.846, 3602.507, 28.946>>, <<1394.636, 3619.827, 48.946>>, 23.1) 
				// @SBA - expanded this box to get Chef engaged sooner (else he stands around until player gets in)
				//if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1395.901, 3609.122, 33.986>>, <<1395.250, 3610.800, 36.986>>, 1.5)  
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1396.791016,3609.446289,33.980907>>, <<1395.188354,3613.933838,35.480907>>, 2.250000)
					//create_vehicle and skip
					
					setup_wave_3_enemies()
					
					// @SBA - Shutting outer doors to help with frame rate
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, FALSE, 0.0, 0.0, 0.0)
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, FALSE, 0.0, 0.0, 0.0)

					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "Wave 3", true)

					wave_3_master_flow_system_status++
					
					// @SBA - grrrr
					REQUEST_MODEL(cheng.model)
					REQUEST_MODEL(translator.model)
					REQUEST_MODEL(mnIceBox)
					REQUEST_MODEL(mnIceBoxProxy)
					REQUEST_ANIM_DICT("MissChinese1LeadInOutCHI_1_MCS_4") 

				endif 
					
			else 
			
				player_position = GET_ENTITY_COORDS(player_ped_id()) 
				
				if (player_position.z < 37.00)
				
					//create vehicle and let it run normally - add extra flag to function no_vehicle_recording_skip = false set as true
					
					no_vehicle_recording_skip = true 
					
					setup_wave_3_enemies()
					
					// @SBA - Shutting outer doors to help with frame rate
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, FALSE, 0.0, 0.0, 0.0)
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, FALSE, 0.0, 0.0, 0.0)
					
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "Wave 3", true)

					wave_3_master_flow_system_status++
					
					// @SBA - grrrr
					REQUEST_MODEL(cheng.model)
					REQUEST_MODEL(translator.model)
					REQUEST_MODEL(mnIceBox)
					REQUEST_MODEL(mnIceBoxProxy)
					REQUEST_ANIM_DICT("MissChinese1LeadInOutCHI_1_MCS_4") 

				endif 

			endif 
			
		break 
		
		case 1
		
			// @SBA - make sure assests have loaded
			IF NOT HAS_MODEL_LOADED(cheng.model)
			OR NOT HAS_MODEL_LOADED(translator.model)
			OR NOT HAS_MODEL_LOADED(mnIceBox)
			OR NOT HAS_MODEL_LOADED(mnIceBoxProxy)
			OR NOT HAS_ANIM_DICT_LOADED("MissChinese1LeadInOutCHI_1_MCS_4")
				CPRINTLN(DEBUG_MISSION, "Final assets not loaded")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			
			if ped_structure_are_all_enemies_dead(wave_3_enemy)
			
				REPLAY_RECORD_BACK_FOR_TIME(3.0)
			
				IF IS_THIS_PRINT_BEING_DISPLAYED("METH_GOD_18")
					CLEAR_THIS_PRINT("METH_GOD_18")
				ENDIF
				
				// @SBA - cleanup navmesh blocking as we no longer need it
				CLEAN_UP_NAVMESH_BLOCKERS()
			
				for i = 0 to count_of(ambient_enemy) - 1
					if not is_ped_injured(ambient_enemy[i].ped)
						if not does_blip_exist(ambient_enemy[i].blip)
							ambient_enemy[i].blip = create_blip_for_ped(ambient_enemy[i].ped, true)
						endif 
					endif 
				endfor

				allow_ambient_enemy_blips_to_flash = true 

				wave_3_buddy_ai_system_status = 5
				
				//make sure no vehicles are blocking the front door of the lab
				for i = 0 to count_of(parked_cars) - 1
					if does_entity_exist(parked_cars[i])
						if is_entity_in_angled_area(parked_cars[i], <<1393.118, 3598.737, 33.986>>, <<1395.846, 3599.719, 36.986>>, 2.500, false, false) 
							delete_vehicle(parked_cars[i])
						endif 
					endif 
				endfor  
						
				for i = 0 to count_of(wave_0_enemy_vehicle) - 1
					if does_entity_exist(wave_0_enemy_vehicle[i].veh)
						if is_entity_in_angled_area(wave_0_enemy_vehicle[i].veh, <<1393.118, 3598.737, 33.986>>, <<1395.846, 3599.719, 36.986>>, 2.500, false, false) 
							delete_vehicle(wave_0_enemy_vehicle[i].veh)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(wave_0_enemy_vehicle[i].veh)
						endif 
					endif 
				endfor 
				
				for i = 0 to count_of(wave_2_enemy_vehicle) - 1
					if does_entity_exist(wave_2_enemy_vehicle[i].veh)
						if is_entity_in_angled_area(wave_2_enemy_vehicle[i].veh, <<1393.118, 3598.737, 33.986>>, <<1395.846, 3599.719, 36.986>>, 2.500, false, false) 
							delete_vehicle(wave_2_enemy_vehicle[i].veh)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(wave_2_enemy_vehicle[i].veh)
						endif 
					endif 
				endfor 
				
				for i = 0 to count_of(wave_3_enemy_vehicle) - 1
					if does_entity_exist(wave_3_enemy_vehicle[i].veh)
						if is_entity_in_angled_area(wave_3_enemy_vehicle[i].veh, <<1393.118, 3598.737, 33.986>>, <<1395.846, 3599.719, 36.986>>, 2.500, false, false) 
							delete_vehicle(wave_3_enemy_vehicle[i].veh)
						endif
					endif 
				endfor 
				
				if does_entity_exist(trevors_truck.veh)
					if is_entity_in_angled_area(trevors_truck.veh, <<1393.118, 3598.737, 33.986>>, <<1395.846, 3599.719, 36.986>>, 2.500, false, false) 
						if is_vehicle_driveable(trevors_truck.veh)
							set_entity_coords(trevors_truck.veh, <<1409.9990, 3600.4973, 33.8676>>, false)
							set_entity_heading(trevors_truck.veh, 108.7487)
						else 
							delete_vehicle(trevors_truck.veh)
						endif 
					endif 
				endif
				
				// @SBA additional clean up to save some memory
				// Ambient enemies now use only GoonModel1, and the peds using GoonModel2 are set to clean up as they die
				SET_MODEL_AS_NO_LONGER_NEEDED(GoonModel2)
				SET_MODEL_AS_NO_LONGER_NEEDED(pcj)
				SET_MODEL_AS_NO_LONGER_NEEDED(Sadler)
				IF IS_INTERIOR_READY(meth_interior)
					UNPIN_INTERIOR(meth_interior)
				ENDIF
				
				// @SBA - allow player access to vehicles again
				UNLOCK_ALL_MISSION_VEHICLES()
				
				// @SBA - re-create Cheng and Translator, create box for blend-in
				// Then do synchronized scene
				setup_buddy(cheng, FALSE)
				setup_buddy(translator, FALSE)
				// icy version
				SET_ICY_VERSIONS_OF_PEDS()

				// @SBA - hide old ice box, create new one that can be animated
				CREATE_MODEL_HIDE(vIceBoxPos, 2.0, prop_ice_box_01, FALSE)
				oiIceBox = CREATE_OBJECT_NO_OFFSET(mnIceBox, vIceBoxPos)
				oiIceBoxProxy = CREATE_OBJECT_NO_OFFSET(mnIceBoxProxy, vIceBoxPos)
				SET_LABEL_AS_TRIGGERED("METH_GOD_20", FALSE)
				
				cutscene_pos = <<1388.108, 3599.101, 33.89>>
				cutscene_rot = <<0.0, 0.0, 110.480>>

				CHENG_CUTSCENE_INDEX = CREATE_SYNCHRONIZED_SCENE ( cutscene_pos, cutscene_rot)
				TRANSLATOR_CUTSCENE_INDEX = CREATE_SYNCHRONIZED_SCENE ( cutscene_pos, cutscene_rot)
				SET_SYNCHRONIZED_SCENE_LOOPED(CHENG_CUTSCENE_INDEX, TRUE)
				SET_SYNCHRONIZED_SCENE_LOOPED(TRANSLATOR_CUTSCENE_INDEX, TRUE)
				
				IF NOT IS_PED_INJURED(cheng.ped)
					CPRINTLN(DEBUG_MISSION, "Starting Cheng's synched scene: initial loop")
					TASK_SYNCHRONIZED_SCENE(cheng.ped, CHENG_CUTSCENE_INDEX, "MissChinese1LeadInOutCHI_1_MCS_4", "CHI_1_MCS_4_TAO_IDLE_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(translator.ped)
					CPRINTLN(DEBUG_MISSION, "Starting translator's synched scene: initial loop")
					TASK_SYNCHRONIZED_SCENE(translator.ped, TRANSLATOR_CUTSCENE_INDEX, "MissChinese1LeadInOutCHI_1_MCS_4", "CHI_1_MCS_4_TRANSLATOR_IDLE_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF DOES_ENTITY_EXIST(oiIceBox)
					CPRINTLN(DEBUG_MISSION, "THE ICE BOX EXISTS!")
					SET_ENTITY_ROTATION(oiIceBox, vIceBoxRot)

					CPRINTLN(DEBUG_MISSION, "Starting ice box anims: initial loop")
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiIceBox, CHENG_CUTSCENE_INDEX, "CHI_1_MCS_4_TAO_IDLE_1_FREEZER", "MissChinese1LeadInOutCHI_1_MCS_4", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					SET_ENTITY_COLLISION(oiIceBox, TRUE)
				ENDIF
				IF DOES_ENTITY_EXIST(oiIceBoxProxy)
					CPRINTLN(DEBUG_MISSION, "THE ICE BOX PROXY EXISTS!")
					SET_ENTITY_ROTATION(oiIceBoxProxy, vIceBoxRot)
					SET_ENTITY_COLLISION(oiIceBoxProxy, FALSE)
				ENDIF
				
				// Enemies are dead - turn off now
				SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
				
				//request_cutscene("chi_1_mcs_4_concat") 
				
				prepare_music_event("CHN1_FINAL_CS")
				
				START_TIMER_NOW(tmrWave3ChefFailsafe)
				
				REQUEST_SCRIPT_AUDIO_BANK("CHINESE1_ICEBOX")
				
				wave_3_master_flow_system_status++
				
			endif 
		
		break 
		
		case 2
			
			if wave_3_buddy_ai_system_status >= 8
				bTellChefGetInside = TRUE
				CANCEL_TIMER(tmrWave3ChefFailsafe)
				wave_3_master_flow_system_status++
			ELSE
				// failsafe in case Chef gets stuck
				IF are_all_ambient_entities_dead()
				OR TIMER_DO_WHEN_READY(tmrWave3ChefFailsafe, 25.000)
					CLEAR_PED_TASKS(buddy.ped)
					CANCEL_TIMER(tmrWave3ChefFailsafe)
					wave_3_buddy_ai_system_status = 9
					wave_3_master_flow_system_status++
				ENDIF
			endif 
			
		break 
		
		case 3
			IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
			ENDIF
			// Don't bother with blip and objective if player is already in position
			IF IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxOuterVol)
				// make sure player is not in the store
				IF NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")
					// @SBA - starting reminder timer (ensure timer isn't still running first)
					// in case player leaves box before we've moved on in next state
					CANCEL_TIMER(tmrIceBoxReminder)
					START_TIMER_NOW(tmrIceBoxReminder)
					wave_3_master_flow_system_status++
				// If in the trigger box but also inside, print objective and blip
				ELSE
					bSetIceBoxBlip = TRUE
					wave_3_master_flow_system_status++
				ENDIF
			ELSE
				bSetIceBoxBlip = TRUE
				wave_3_master_flow_system_status++
			endif 

		break 
		
		case 4 
		
			// check larger outer volume around box
			IF IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxOuterVol)
				
				// remove buddy blip
				IF DOES_BLIP_EXIST(buddy.blip)
					REMOVE_BLIP(buddy.blip)
				ENDIF

				// make sure player is not in the store
				IF NOT IS_PED_IN_SPECIFIC_ROOM(PLAYER_PED_ID(), (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")
					IF not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						// @SBA - play Trevor line, need to correct speaker number
						ADD_PED_FOR_DIALOGUE(scripted_speech[0], 0, player_ped_id(), "trevor")
						IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_M4_LI2", CONV_PRIORITY_HIGH)
							RETURN TRUE
						ENDIF 
					ENDIF
				ENDIF
				
			// @SBA = for B*926673.  Adding a reminder.
			// removing due to some dialogue rearranging.  Might return?
//			ELSE
//				// timer is done
//				IF TIMER_DO_ONCE_WHEN_READY(tmrIceBoxReminder, 25.000)
//					// Play dialogue, if you can
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_cookice")
//						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
//							IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_cookice", conv_priority_medium)
//								// Don't replay the line after this.
//								SET_LABEL_AS_TRIGGERED("chin_cookice", TRUE)
//								//wave_3_master_flow_system_status = 3
//							ENDIF
//						ENDIF
//					// Otherwise just repeat objective @SBA - removed per B*1357967, but keeping in case
//					//ELSE
//						// Go back up to redisplay objective
//						//wave_3_master_flow_system_status = 3
//					ENDIF
//				ENDIF
				
			endif 

		break 
		
	endswitch 

	return false 

endfunc  

proc wave_3_dialogue_system()

	switch wave_3_dialogue_system_status 
	
		case 0
		
			if create_conversation(scripted_speech[0], "methaud", "meth_run1", conv_priority_medium) 
				
				wave_3_dialogue_system_status++
				
			endif 
		
		break 
		
		case 1 
		
			if create_conversation(scripted_speech[0], "methaud", "TRESP", conv_priority_medium) 
				
				wave_3_dialogue_system_status++
				
			endif 
		
		break 
		
		case 2
		
			if not is_scripted_conversation_ongoing()
				print_now("METH_GOD_3", default_god_text_time, 1)
				wave_3_dialogue_system_status++
			endif 
		
		break 
		
		case 3
			
			prepare_music_event("CHN1_LAST_GUYS")
		
			if wave_3_enemy[0].created
				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech[0], "methaud", "chin_doorcov", conv_priority_medium)
						trigger_music_event("CHN1_LAST_GUYS")
						wave_3_dialogue_system_status++
					endif 
				endif 
			endif 
		
		break
		
		CASE 4
			IF NOT bPlayerUsedAbility
				SPECIAL_ABILITY_CHARGE_SMALL(PLAYER_ID(), TRUE, TRUE)
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("METH_help_T_KM",DEFAULT_HELP_TEXT_TIME)
				ELSE
					PRINT_HELP("METH_help_T",DEFAULT_HELP_TEXT_TIME)
				ENDIF
				wave_3_dialogue_system_status++
			ELSE
				wave_3_dialogue_system_status++
			ENDIF
			dialogue_time_2 = GET_GAME_TIMER()
		BREAK
		
		case 5
			// slight wait after help
			IF NOT lk_timer(dialogue_time_2, 1000)
				EXIT
			ENDIF
		
			if not is_any_text_being_displayed(locates_data)
			
				if not has_label_been_triggered("chin_seeort")
					if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) 
						if not is_ped_injured(wave_3_enemy[0].ped)
							if is_ped_on_foot(wave_3_enemy[0].ped)
								if create_conversation(scripted_speech[0], "methaud", "chin_seeort", conv_priority_medium)																	
									set_label_as_triggered("chin_seeort", true)
								endif
							ENDIF
						ENDIF
					endif
				endif
				
				if not has_label_been_triggered("chin_ortega")
					if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) 
						if not is_ped_injured(wave_3_enemy[0].ped)
							if is_ped_on_foot(wave_3_enemy[0].ped)
								if create_conversation(scripted_speech[0], "methaud", "chin_ortega", conv_priority_medium)
									set_label_as_triggered("chin_ortega", true)
								endif 
							endif 
						endif 
					endif 
				endif 
				
				if (not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) and not is_ped_injured(wave_3_enemy[0].ped))
					if not has_label_been_triggered("chin_alive")
						if not is_ped_injured(wave_3_enemy[1].ped)
							if is_ped_on_foot(wave_3_enemy[1].ped)
								if create_conversation(scripted_speech[0], "methaud", "chin_alive", conv_priority_medium)
									set_label_as_triggered("chin_alive", true)
								endif
							endif
						endif
					endif
				else
					if not has_label_been_triggered("chin_dead")
						if not is_ped_injured(wave_3_enemy[1].ped)
							if is_ped_on_foot(wave_3_enemy[1].ped)
								if create_conversation(scripted_speech[0], "methaud", "chin_dead", conv_priority_medium)
									set_label_as_triggered("chin_dead", true)
								endif
							endif
						ELSE
							// have Chef say Ortega killed if enemy doesn't
							IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
								IF IS_PED_INJURED(wave_3_enemy[0].ped)
									IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_killort")
										IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_killort", CONV_PRIORITY_HIGH)
											SET_LABEL_AS_TRIGGERED("chin_killort", TRUE)
											SET_LABEL_AS_TRIGGERED("chin_dead", TRUE)
										ENDIF 
									ENDIF
								ENDIF
							ENDIF
						endif
					endif
				endif

				if (not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) and not is_ped_injured(wave_3_enemy[0].ped))
					if not has_label_been_triggered("chin_alive2")
						if not is_ped_injured(wave_3_enemy[2].ped)
							if is_ped_on_foot(wave_3_enemy[2].ped)
								if create_conversation(scripted_speech[0], "methaud", "chin_alive2", conv_priority_medium)
									set_label_as_triggered("chin_alive2", true)
								endif 
							endif 
						endif 
					endif 
				else 
					if not has_label_been_triggered("chin_dead2")
						if not is_ped_injured(wave_3_enemy[1].ped)
							if is_ped_on_foot(wave_3_enemy[1].ped)
								if create_conversation(scripted_speech[0], "methaud", "chin_dead2", conv_priority_medium)
									set_label_as_triggered("chin_dead2", true)
								endif 
							endif 
						endif 
					endif 
				endif 

				if not has_label_been_triggered("chin_attack2")
					if not is_any_text_being_displayed(locates_data)
						if not is_ped_injured(wave_3_enemy[3].ped)
							if wave_3_status[3] = do_nothing //if enemy is standing right of shop door 
								if create_conversation(scripted_speech[0], "methaud", "chin_attack2", conv_priority_medium)
									set_label_as_triggered("chin_attack2", true)
								endif 
							endif 
						endif 
					endif 
				endif 

			endif 
			
			if wave_3_master_flow_system_status > 1
				set_label_as_triggered("chin_end", FALSE)
				set_label_as_triggered("chin_cookice", FALSE)
				set_label_as_triggered("chin_shout", FALSE)
				wave_3_dialogue_system_status++
			endif 
			
		break 
		
		case 6
		
			// Trevor orders Chef back inside
			IF bTellChefGetInside
				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech[0], "methaud", "chin_end", conv_priority_medium)
						bTellChefGetInside = FALSE
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
						
						set_label_as_triggered("chin_end", true)
					ENDIF
				ENDIF
			ELSE
				// Chef yells at retreating enemies (if any)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_shout")
					IF NOT are_all_ambient_entities_dead()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_end")
							if NOT is_ped_in_specific_room(buddy.ped, (<<1392.3, 3602.9, 35.0>>), "v_39_ShopRm")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
									IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_shout", conv_priority_medium)
										set_label_as_triggered("chin_shout", true)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						set_label_as_triggered("chin_shout", true)
					ENDIF
				ENDIF
			ENDIF
			
			// Trevor says he'll let Cheng and translator out
			IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_cookice")
				IF HAS_LABEL_BEEN_TRIGGERED("chin_end")
				OR wave_3_master_flow_system_status > 2
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
						IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_cookice", conv_priority_medium)
							set_label_as_triggered("chin_cookice", true)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// handle ice box blip, objecitve text, and stuff when told to
			IF bSetIceBoxBlip
				IF HAS_LABEL_BEEN_TRIGGERED("chin_cookice")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
						DO_ICE_BOX_OBJECTIVE_STUFF()
						bSetIceBoxBlip = FALSE
						wave_3_dialogue_system_status++
					ENDIF
				ENDIF
			ENDIF
		break
		
		case 7
		break 
		
	endswitch 
	
//	printint(wave_3_buddy_ai_system_status)
//	printnl()
	
	if lk_timer(dialogue_time, 13000)
		if is_ped_shooting(player_ped_id())
			if not is_any_text_being_displayed(locates_data)
				if is_enemy_near_player(wave_3_enemy, 50.00)
					if create_conversation(scripted_speech[0], "methaud", "attack_0", conv_priority_low) 
						dialogue_time = get_game_timer()
					endif 
				endif 
			endif 
		endif 
	endif 


endproc 

proc wave_3_buddy_ai_system()

	if allow_buddy_ai_system
	
		// @SBA - handle if player leaves Chef during fight
		IF NOT wave_3_complete
			IF IS_ENTITY_AT_COORD(buddy.ped, (<<1393.76355, 3608.57373, 34.00>>), (<<1.5, 1.5, 2.0>>))
			AND wave_3_buddy_ai_system_status > 4
				HANDLE_WAVE_3_CHEF_DEMISE()
				HANDLE_ABANDONMENT_MESSAGE(HAS_PLAYER_ABANDONED_CHEF_WAVE_3())
			ENDIF
			HANDLE_CHEF_DAMAGED_DIALOGUE()
		ENDIF
	
		switch wave_3_buddy_ai_system_status 
		
			case 0
				
				// @SBA - we want to help ensure the player sees the chef run off
				START_TIMER_NOW(tmrStartChefMove)
				
				IF IS_ENTITY_ON_SCREEN(buddy.ped)
				OR TIMER_DO_WHEN_READY(tmrStartChefMove, 4.000)
					CANCEL_TIMER(tmrStartChefMove)
					// @SBA - make sure Chef's health is set to max for this wave
					SET_HEALTH_FOR_PED(buddy.ped, buddy.health)
					
					SET_PED_CAN_PEEK_IN_COVER(buddy.ped, TRUE)
					set_blocking_of_non_temporary_events(buddy.ped, true)
					clear_ped_tasks(buddy.ped)
					
					open_sequence_task(seq)
						// @SBA - run to first waypoint, don't worry about navmesh or closest point (cleans up the transition)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "methlab3", 0, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS) // EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT |
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)

					wave_3_buddy_ai_system_status++
				ENDIF
			break 
			
			case 1

				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(buddy.ped, 1.60) // @SBA OLD = 2.0
				endif 
				
				if not IS_ENTITY_IN_ANGLED_AREA(buddy.ped, <<1387.209, 3612.450, 37.946>>, <<1384.973, 3611.582, 40.948>>, 2.5, false)   
					if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
					
						if waypoint_recording_get_closest_waypoint("methlab3", GET_ENTITY_COORDS(buddy.ped), waypoint_node_pos)
			
							waypoint_node_pos += 1

							if (waypoint_node_pos > 18)
								waypoint_node_pos = 18
							endif 
						
							set_blocking_of_non_temporary_events(buddy.ped, true)
							clear_ped_tasks(buddy.ped)
							
							open_sequence_task(seq)
								task_follow_waypoint_recording(null, "methlab3", waypoint_node_pos, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
							close_sequence_task(seq)
							task_perform_sequence(buddy.ped, seq)
							clear_sequence_task(seq)
						endif 
					endif 

				else 
					
					if get_distance_between_coords(GET_ENTITY_COORDS(buddy.ped), GET_ENTITY_COORDS(player_ped_id())) > 8.0 //6.0
						
						if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
							WAYPOINT_PLAYBACK_PAUSE(buddy.ped, true)
						endif 
						
						wave_3_buddy_ai_system_status++
					
					else 
					
						wave_3_buddy_ai_system_status = 3
					
					endif 
				endif
				
			break
			
			case 2
			
				if not has_label_been_triggered("chin_cook")
					if create_conversation(scripted_speech[0], "methaud", "chin_cook", conv_priority_medium)
									 
						set_label_as_triggered("chin_cook", true)
					endif 
				endif 
			
				IF IS_PLAYER_NEAR_CHEF_ON_STAIRS()
					
					if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
						WAYPOINT_PLAYBACK_RESUME(buddy.ped)
					endif 

					REPLAY_RECORD_BACK_FOR_TIME(5.0, 13.0)

					wave_3_buddy_ai_system_status++
				endif 
			
			break 
			
			case 3
				
				waypoint_recording_get_num_points("methlab3", total_number_of_points)
				total_number_of_points -= 3

				if waypoint_recording_get_coord("methlab3", total_number_of_points, target_waypoint_pos)  
					//CPRINTLN(DEBUG_MISSION, "Chef waypoint number is ", total_number_of_points)
					//CPRINTLN(DEBUG_MISSION, "Chef waypoint vector is ", target_waypoint_pos)
					if not IS_ENTITY_AT_COORD(buddy.ped, target_waypoint_pos, <<1.0, 1.0, 1.6>>, false, true)
						if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
						
							if waypoint_recording_get_closest_waypoint("methlab3", GET_ENTITY_COORDS(buddy.ped), waypoint_node_pos)
			
								waypoint_node_pos += 1

								if (waypoint_node_pos > total_number_of_points)
									waypoint_node_pos = total_number_of_points
								endif 
							
								set_blocking_of_non_temporary_events(buddy.ped, true)
								clear_ped_tasks(buddy.ped)
								
								open_sequence_task(seq)
									task_follow_waypoint_recording(null, "methlab3", waypoint_node_pos, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
							endif 
						endif 
					
					else 

						IF IS_PLAYER_STEALING_PEDS_DESTINATION(buddy.ped, buddy_cover_point_data[4].pos, 5.0, false)
							CPRINTLN(DEBUG_MISSION, "Chef to right cover")
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[5].cover_point, buddy_cover_point_data[5].cover_from_pos, -1, TRUE)
						ELSE
							CPRINTLN(DEBUG_MISSION, "Chef to left cover")
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[4].cover_point, buddy_cover_point_data[4].cover_from_pos, -1, TRUE)
						ENDIF
						
						buddy_time = get_game_timer()

						wave_3_buddy_ai_system_status++
					
					endif 
					
				endif
			
			break 
			
			case 4
			
				IF NOT IS_PED_IN_COVER(buddy.ped)
				
					IF has_ped_task_finished_2(buddy.ped, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						IF IS_PLAYER_STEALING_PEDS_DESTINATION(buddy.ped, buddy_cover_point_data[4].pos, 5.0, false)
							CPRINTLN(DEBUG_MISSION, "Chef to right cover")
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[5].cover_point, buddy_cover_point_data[5].cover_from_pos, -1, TRUE)
						ELSE
							CPRINTLN(DEBUG_MISSION, "Chef to left cover")
							TASK_SEEK_COVER_TO_COVER_POINT(buddy.ped, buddy_cover_point_data[4].cover_point, buddy_cover_point_data[4].cover_from_pos, -1, TRUE)
						ENDIF
					ENDIF
			
				ELIF lk_timer(buddy_time, 2000) //@SBA OLD = 4000
					// Wait for final vehicle to be in position
					IF IS_VEHICLE_DRIVEABLE(wave_3_enemy_vehicle[0].veh)
						IF NOT is_playback_going_on_for_vehicle(wave_3_enemy_vehicle[0].veh)
							CPRINTLN(DEBUG_MISSION, "Chef should be in cover and in combat")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(buddy.ped, false)
							SET_PED_SPHERE_DEFENSIVE_AREA(buddy.ped, GET_ENTITY_COORDS(buddy.ped), 1.25)
							
							SET_PED_ACCURACY(buddy.ped, 5)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(buddy.ped, 50.0)
							
							wave_3_buddy_ai_system_status = 22
						ENDIF
					ENDIF
				ENDIF 		
			
			break 
			
			case 5
			
				//if waypoint_recording_get_closest_waypoint("methlab7", GET_ENTITY_COORDS(buddy.ped), waypoint_node_pos)
			
					set_blocking_of_non_temporary_events(buddy.ped, true)
					clear_ped_tasks(buddy.ped)
					
					set_entity_proofs(buddy.ped, false, true, false, false, false)

					open_sequence_task(seq)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, "methlab7", waypoint_node_pos,  EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)

					wave_3_buddy_ai_system_status++
					
				//endif 
			
			break 
			
			case 6
			
				if not has_label_been_triggered("buddy_runout")
					if not is_any_text_being_displayed(locates_data)
						if create_conversation(scripted_speech[0], "methaud", "buddy_runout", conv_priority_medium)
							set_label_as_triggered("buddy_runout", true)
						endif 
					endif 
				endif 

				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(buddy.ped)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(buddy.ped, 2.0) 
				endif 
				
				if waypoint_recording_get_coord("methlab7", 11, target_waypoint_pos)  
					if not IS_ENTITY_AT_COORD(buddy.ped, target_waypoint_pos, <<1.5, 1.5, 1.6>>, false, true)
						if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
						
							if waypoint_recording_get_closest_waypoint("methlab7", GET_ENTITY_COORDS(buddy.ped), waypoint_node_pos)
			
								waypoint_node_pos += 1

								if (waypoint_node_pos > 11)
									waypoint_node_pos = 11
								endif 
							
								set_blocking_of_non_temporary_events(buddy.ped, true)
								clear_ped_tasks(buddy.ped)
								
								open_sequence_task(seq)
									task_follow_waypoint_recording(null, "methlab7", waypoint_node_pos, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
							endif 
						endif 
					
					else 

						if not is_ped_injured(ambient_enemy[0].ped)
						or not is_ped_injured(ambient_enemy[1].ped)
						or not is_ped_injured(ambient_enemy[2].ped)
						or not is_ped_injured(ambient_enemy[3].ped)
						
							if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
							
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1406.8160, 3595.9963, 33.6496>>, ambient_enemy_vehicle[0].veh, pedmove_run, true, 0.2, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
								
							elif is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
							
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1406.8160, 3595.9963, 33.6496>>, ambient_enemy_vehicle[1].veh, pedmove_run, true, 0.2, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
								
							else 
							
								open_sequence_task(seq)
									task_go_to_coord_while_aiming_at_coord(null, <<1406.8160, 3595.9963, 33.6496>>, <<1426.58, 3592.1, 35.42>>, pedmove_run, false, 0.2, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(buddy.ped, seq)
								clear_sequence_task(seq)
							
							endif
							
						else 
							
							open_sequence_task(seq)
								task_go_to_coord_while_aiming_at_coord(null, <<1406.8160, 3595.9963, 33.6496>>, <<1426.58, 3592.1, 35.42>>, pedmove_run, false, 0.2, 0.8) 
							close_sequence_task(seq)
							task_perform_sequence(buddy.ped, seq)
							clear_sequence_task(seq)
						
						endif 
							
						wave_3_buddy_ai_system_status++

					endif 
					
				endif

			break 
			
			case 7
			
				if not IS_ENTITY_AT_COORD(buddy.ped, <<1406.8160, 3595.9963, 33.6496>>, <<1.5, 1.5, 1.6>>, false, true)
					if has_ped_task_finished_2(buddy.ped, script_task_perform_sequence)
						
						set_blocking_of_non_temporary_events(buddy.ped, true)
						clear_ped_tasks(buddy.ped)
						
						if is_vehicle_driveable(ambient_enemy_vehicle[0].veh)
						
							open_sequence_task(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1406.8160, 3595.9963, 33.6496>>, ambient_enemy_vehicle[0].veh, pedmove_run, true, 0.2, 0.8) 
							close_sequence_task(seq)
							task_perform_sequence(buddy.ped, seq)
							clear_sequence_task(seq)
							
						elif is_vehicle_driveable(ambient_enemy_vehicle[1].veh)
						
							open_sequence_task(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<1406.8160, 3595.9963, 33.6496>>, ambient_enemy_vehicle[1].veh, pedmove_run, true, 0.2, 0.8) 
							close_sequence_task(seq)
							task_perform_sequence(buddy.ped, seq)
							clear_sequence_task(seq)
							
						else 
						
							open_sequence_task(seq)
								task_go_to_coord_while_aiming_at_coord(null, <<1406.8160, 3595.9963, 33.6496>>, <<1426.58, 3592.1, 35.42>>, pedmove_run, false, 0.2, 0.8) 
							close_sequence_task(seq)
							task_perform_sequence(buddy.ped, seq)
							clear_sequence_task(seq)
						
						endif 
						
					endif 
					
				else 
				
					set_blocking_of_non_temporary_events(buddy.ped, false)
					set_ped_sphere_defensive_area(buddy.ped, <<1406.8160, 3595.9963, 33.6496>>, 4.0)
					task_combat_hated_targets_around_ped(buddy.ped, 200)
					
					wave_3_buddy_ai_system_status++
					
					original_time = get_game_timer()
				
				endif 

			break 
			
			case 8

				if lk_timer(original_time, 1500)
				or are_all_ambient_entities_dead()
				
					set_blocking_of_non_temporary_events(buddy.ped, true)

					clear_ped_tasks(buddy.ped)

					open_sequence_task(seq)
						TASK_AIM_GUN_AT_COORD(NULL, (<<1426.58, 3592.1, 35.42>>), 750)
						task_follow_nav_mesh_to_coord(null, <<1395.9012, 3611.6748, 33.9809>>, PEDMOVE_WALK, -1, default_navmesh_radius, enav_no_stopping)
						task_follow_nav_mesh_to_coord(null, <<1391.8004, 3612.1050, 33.9809>>, PEDMOVE_WALK, -1, default_navmesh_radius, enav_no_stopping)
						task_follow_nav_mesh_to_coord(null, <<1391.1970, 3601.0510, 37.9419>>, pedmove_run, -1, default_navmesh_radius) 
					close_sequence_task(seq)
					task_perform_sequence(buddy.ped, seq)
					clear_sequence_task(seq)
					
					wave_3_buddy_ai_system_status++
					
				endif 
			
			break 
			
			case 9
			
				if not is_entity_at_coord(buddy.ped, <<1391.1970, 3601.0510, 37.9419>>, <<1.5, 1.5, 2.0>>)
					if has_ped_task_finished_2(buddy.ped)

						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, <<1391.1970, 3601.0510, 37.9419>>, pedmove_run, -1, default_navmesh_radius) 
						close_sequence_task(seq)
						task_perform_sequence(buddy.ped, seq)
						clear_sequence_task(seq)
						
					endif 
					
				else 
					// Have Chef do something until the cutscene starts
					TASK_LOOK_AT_ENTITY(buddy.ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					OPEN_SEQUENCE_TASK(seq)
						TASK_SWAP_WEAPON(NULL, FALSE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, (<<1390.0, 3600.510, 37.9419>>), 2.5, -1)
						//TASK_WANDER_IN_AREA(NULL, (<<1393.49988, 3603.23877, 37.94191>>), 2.0, 2, 4)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(buddy.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					wave_3_buddy_ai_system_status++

				endif 

			break 
			
			CASE 10
				// zilch
			BREAK
			
		endswitch 
		
	endif 

endproc 


proc wave_3_assisted_nodes_system()

	switch wave_3_assisted_nodes_system_status
	
		case 0
		
			ASSISTED_MOVEMENT_remove_ROUTE("Wave2")
			
			ASSISTED_MOVEMENT_REQUEST_ROUTE("Wave3")
			
			wave_3_assisted_nodes_system_status++

		break 
		
		case 1
		
//			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1401.366, 3619.451, 37.947>>, <<1406.805, 3604.510, 39.947>>, 10.900)  
//				
//				ASSISTED_MOVEMENT_remove_ROUTE("Meth2")
//				
//				wave_3_assisted_nodes_system_status++
//			endif
		
		break 
		
		case 2
		

		break 
		
		case 3
		
		
		break 
		
		case 22
		
		break 
	
	endswitch 

endproc 

proc wave_3_audio_scene_system()

	switch wave_3_audio_scene_system_status 
	
		case 0
		
			if wave_3_enemy[1].created
			
				if does_entity_exist(wave_3_enemy_vehicle[0].veh)
					if is_vehicle_driveable(wave_3_enemy_vehicle[0].veh)
						add_entity_to_audio_mix_group(wave_3_enemy_vehicle[0].veh, "CHI_1_TRUCK_02")
					endif 
				endif 

				if is_audio_scene_active("CHI_1_GET_TO_SHOOTOUT_04")
					stop_audio_scene("CHI_1_GET_TO_SHOOTOUT_04")
				endif 

				wave_3_audio_scene_system_status++
				
			endif 
		
		break 
		
		case 1
		
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1400.846, 3602.507, 28.946>>, <<1394.636, 3619.827, 48.946>>, 23.1)      
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<1395.035, 3608.389, 33.981>>, <<1394.215, 3610.644, 36.5>>, 1.5)  

					start_audio_scene("CHI_1_SHOOTOUT_04")

				endif 
					
			else 
			
				player_position = GET_ENTITY_COORDS(player_ped_id()) 
				
				if (player_position.z < 37.00)
				
					start_audio_scene("CHI_1_SHOOTOUT_04")
				
					wave_3_audio_scene_system_status++
					
				endif 
				
			endif 

		break 
		
		case 2
		
			if does_entity_exist(wave_3_enemy_vehicle[0].veh)
				if is_vehicle_driveable(wave_3_enemy_vehicle[0].veh)
					if not is_playback_going_on_for_vehicle(wave_3_enemy_vehicle[0].veh)
						remove_entity_from_audio_mix_group(wave_3_enemy_vehicle[0].veh)
						wave_3_audio_scene_system_status++
					endif 
				else 
					remove_entity_from_audio_mix_group(wave_3_enemy_vehicle[0].veh)
					wave_3_audio_scene_system_status++
				endif 
			endif 
		
		break 
		
		case 3
		
			if ped_structure_are_all_enemies_dead(wave_3_enemy)
			
				trigger_music_event("CHN1_ENEMIES_FLEE")
			
				if is_audio_scene_active("CHI_1_SHOOTOUT_04")
					stop_audio_scene("CHI_1_SHOOTOUT_04")
				endif 
				
				start_audio_scene("CHI_1_ENEMIES_ESCAPE")

				wave_3_audio_scene_system_status++
				
			endif 
			
		break 
		
		case 4
			
			if does_blip_exist(ice_box_blip)
			
				if is_audio_scene_active("CHI_1_ENEMIES_ESCAPE")
					stop_audio_scene("CHI_1_ENEMIES_ESCAPE")
				endif 
			
				start_audio_scene("CHI_1_GO_TO_ICEBOX")
				
				wave_3_audio_scene_system_status++
				
			endif 

		break 
		
	endswitch 

endproc
				
func bool chinese_wave_3_system()

	// @SBA - B*1195400 - rarely, a ped falls through the world and cannot be killed in game play
	// this kills him if he is below ground
	KILL_ANY_ERRANT_ENEMY(wave_3_enemy)
	KILL_ANY_ERRANT_ENEMY(ambient_enemy)
	
	// @SBA - handle dead vehicle blips
	REMOVE_DEAD_VEHICLE_BLIPS(wave_3_enemy_vehicle)

	if wave_3_master_flow_system()
		mission_flow = mission_passed_mocap
		return true 
	endif 
	
	wave_3_dialogue_system()

	wave_3_buddy_ai_system()
	
	wave_3_ai_system()
	
	//wave_3_assisted_nodes_system()
	
	ambient_enemy_system()
	
	wave_3_audio_scene_system()

	if is_ped_inside_interior(player_ped_id(), <<1392.7360, 3602.5989, 38.5>>, "v_methlab")
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	endif 
	
	return false 

endfunc  

PROC HAVE_PED_EXIT_VEHICLE_THEN_WALK(PED_INDEX piPed, VECTOR vLoc, INT iPause = 0)
	SEQUENCE_INDEX siSeq
	IF IS_VEHICLE_DRIVEABLE(trevors_truck.veh)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		OPEN_SEQUENCE_TASK(siSeq)
			TASK_PAUSE(NULL, iPause)
			TASK_LEAVE_VEHICLE(NULL, trevors_truck.veh)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLoc, PEDMOVE_WALK)
		CLOSE_SEQUENCE_TASK(siSeq)
		TASK_PERFORM_SEQUENCE(piPed, siSeq)
		CLEAR_SEQUENCE_TASK(siSeq)
	ENDIF
ENDPROC

FLOAT fSRLTime

// @SBA - handle the lead in for the arrival cutscene, return when finished
FUNC BOOL HANDLE_LEAD_IN_SHOT_OF_ARRIVAL()

	IF DOES_ENTITY_EXIST(trevors_truck.veh)
		IF NOT IS_ENTITY_DEAD(trevors_truck.veh)
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(trevors_truck.veh)
		ENDIF
	ENDIF
	
	IF ArivialLeadInState > ALI_HALT_VEHICLE
		fSRLTime += GET_FRAME_TIME()
		IF fSRLTime < 0.0
			fSRLTime = 0.0
		ENDIF
		SET_SRL_TIME(fSRLTime)
	ENDIF
	
	SWITCH ArivialLeadInState
	
		CASE ALI_HALT_VEHICLE
		
			SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
			BEGIN_SRL()
		
			IF NOT IS_CAM_RENDERING(camera_a)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				display_hud(false)
				display_radar(false)
			ENDIF
			
			IF IS_PED_INJURED(PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: Player is injured?!")
			ENDIF
			
			IF IS_TIMER_STARTED(tmrArrival)
				CANCEL_TIMER(tmrArrival)
			ENDIF
			
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_CAM_INTERP (helicopter)")
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ArivialLeadInState = ALI_CAM_INTERP
			ELIF get_current_player_vehicle(trevors_truck.veh)
				//IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(trevors_truck.veh, 6, 1, 12.0)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(trevors_truck.veh)
					CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_CAM_INTERP")
					ArivialLeadInState = ALI_CAM_INTERP

				//ENDIF
			ELSE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_CAM_INTERP (no vehicle)")
				ArivialLeadInState = ALI_CAM_INTERP
			ENDIF

		BREAK

		CASE ALI_CAM_INTERP
			// start interp
			IF DOES_CAM_EXIST(camera_a)
			AND DOES_CAM_EXIST(camera_b)
				SET_CAM_ACTIVE_WITH_INTERP(camera_b, camera_a, 5000)
				SHAKE_CAM(camera_b, "HAND_SHAKE")
				CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_FINISH_DRIVE")
				ArivialLeadInState = ALI_FINISH_DRIVE
			ENDIF
		BREAK
		
		CASE ALI_FINISH_DRIVE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// helicopter?
				IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_FINISH_INTERP (helicopter)")
					ArivialLeadInState = ALI_FINISH_INTERP
				// If we're in a vehicle go to final spot
				// Hopefully this alleviates badly-parked vehicles 
				ELIF get_current_player_vehicle(trevors_truck.veh)
					VECTOR vVehCoord
					FLOAT fVehHead
					vVehCoord = GET_ENTITY_COORDS(trevors_truck.veh)
					IF vVehCoord.x > 1402.7
						CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: Trevor's vehicle coming from the RIGHT")
						vVehCoord = vVehicleLeadinLocR
						fVehHead = fTrevorsVehicleMethLabHead
					ELSE
						CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: Trevor's vehicle coming from the LEFT")
						vVehCoord = vVehicleLeadinLocL
						fVehHead = fTrevorsVehicleMethLabHead - 180
					ENDIF
					CLEAR_AREA(vVehCoord, 3.0, TRUE)
					SET_ENTITY_COORDS(trevors_truck.veh, vVehCoord)
					SET_ENTITY_HEADING(trevors_truck.veh, fVehHead)
					SET_VEHICLE_ON_GROUND_PROPERLY(trevors_truck.veh)

					TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), trevors_truck.veh, vArrivalMethLab, 15, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 2.0, 20)
					CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_VEHICLE_STOP")
					START_TIMER_NOW(tmrArrival)
					ArivialLeadInState = ALI_VEHICLE_STOP
				// if we're on foot, just wait for the interp to end
				ELSE
					CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_FINISH_INTERP (on foot)")
					ArivialLeadInState = ALI_FINISH_INTERP
				ENDIF
			ENDIF
		BREAK

		CASE ALI_VEHICLE_STOP
			IF IS_VEHICLE_DRIVEABLE(trevors_truck.veh)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())

				IF IS_VEHICLE_STOPPED(trevors_truck.veh)
				AND (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK)
					HAVE_PED_EXIT_VEHICLE_THEN_WALK(PLAYER_PED_ID(), (<<1402.6, 3598.8, 34.0>>))
					HAVE_PED_EXIT_VEHICLE_THEN_WALK(translator.ped, (<<1402.3, 3598.6, 34.0>>), 300)
					CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_EXIT_VEHICLE")
					ArivialLeadInState = ALI_EXIT_VEHICLE
				ELIF TIMER_DO_WHEN_READY(tmrArrival, 16.000)
					IF IS_ENTITY_OCCLUDED(trevors_truck.veh)
						CANCEL_TIMER(tmrArrival)
						CLEAR_AREA(vVehicleLeadinLocR, 3.0, TRUE)
						SET_ENTITY_COORDS(trevors_truck.veh, vVehicleLeadinLocR)
						SET_ENTITY_HEADING(trevors_truck.veh, fTrevorsVehicleMethLabHead)
						ArivialLeadInState = ALI_FINISH_DRIVE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ALI_EXIT_VEHICLE
			CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: Leaving vehicle")
			RETURN TRUE
		BREAK
		
		CASE ALI_FINISH_INTERP
			// letting the interp finish if we're on foot
			IF DOES_CAM_EXIST(camera_b)
				IF IS_CAM_ACTIVE(camera_b)
					IF NOT IS_CAM_INTERPOLATING(camera_b)
						START_TIMER_NOW(tmrArrival)
						CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: TO ALI_WAIT_FOR_TIMER (no vehicle)")
						ArivialLeadInState = ALI_WAIT_FOR_TIMER
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE ALI_WAIT_FOR_TIMER
			IF TIMER_DO_ONCE_WHEN_READY(tmrArrival, 0.200)
				CPRINTLN(DEBUG_MISSION, "HANDLE_LEAD_IN_SHOT_OF_ARRIVAL: Returning TRUE")
				RETURN TRUE
			ENDIF
		BREAK

	ENDSWITCH
	
	
	RETURN FALSE

ENDFUNC

PROC SETUP_PLAYER_POST_ARRIVAL_CUTSCENE()

//	clear_ped_tasks_immediately(player_ped_id())
//	SET_ENTITY_COORDS(player_ped_id(), vTrevorWindowStartPos)//@SBA - adjusting starting cover position slightly - OLD VALUE <<1389.5963, 3600.4568, 37.9419>>)  
//	SET_ENTITY_HEADING(player_ped_id(), 21.5294)//72.3301)
	// @SBA - adjusting starting cover position slightly - OLD VALUE <<1389.63, 3600.41, 37.94>>
	task_put_ped_directly_into_cover(player_ped_id(), vTrevorWindowStartPos, -1, false, 0.5, true, false, players_cover_point)//, false, instant_blend_in)
	force_ped_ai_and_animation_update(player_ped_id(), FALSE)
	
	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, true, true)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
			set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
		endif 
		set_current_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE, true)
	endif 
	//SET_GAMEPLAY_CAM_RELATIVE_HEADING(90.00)//*************
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
ENDPROC

// @SBA HACK split up the creation of the wave a bit for normal game flow
FUNC BOOL SETUP_INITIAL_ENEMY_WAVE_IN_STAGES()

	SWITCH iWave0SetupStage
	
		// Enemies on foot
		CASE 0
			setup_enemy(wave_0_enemy[0])
			setup_enemy(wave_0_enemy[1])
			iWave0SetupStage ++
		BREAK
		// first SUV
		CASE 1
			SETUP_ENEMY_VEHICLE(wave_0_enemy_vehicle[0])
			set_vehicle_colours(wave_0_enemy_vehicle[0].veh, 0, 0)
			
			setup_enemy_in_vehicle(wave_0_enemy[2], wave_0_enemy_vehicle[0].veh) 
			setup_enemy_in_vehicle(wave_0_enemy[3], wave_0_enemy_vehicle[0].veh, vs_front_right) 
			setup_enemy_in_vehicle(wave_0_enemy[4], wave_0_enemy_vehicle[0].veh, vs_back_left) 
			
			start_playback_recorded_vehicle(wave_0_enemy_vehicle[0].veh, wave_0_enemy_vehicle[0].recording_number, "lkmethlab") 
			skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[0].veh, 3000)
			iWave0SetupStage ++
		BREAK
		// second SUV
		CASE 2
			SETUP_ENEMY_VEHICLE(wave_0_enemy_vehicle[1])
			set_vehicle_colours(wave_0_enemy_vehicle[1].veh, 10, 10)
			
			setup_enemy_in_vehicle(wave_0_enemy[5], wave_0_enemy_vehicle[1].veh) 
			setup_enemy_in_vehicle(wave_0_enemy[6], wave_0_enemy_vehicle[1].veh, vs_front_right) 
			setup_enemy_in_vehicle(wave_0_enemy[7], wave_0_enemy_vehicle[1].veh, vs_back_left) 
			setup_enemy_in_vehicle(wave_0_enemy[8], wave_0_enemy_vehicle[1].veh, vs_back_right) 

			start_playback_recorded_vehicle(wave_0_enemy_vehicle[1].veh, wave_0_enemy_vehicle[1].recording_number, "lkmethlab") 
			skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[1].veh, 4500)
			iWave0SetupStage ++
		BREAK
		CASE 3
			iWave0SetupStage ++
			RETURN TRUE
		BREAK
		
		
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC

// creating wave 0 all at once for restarts, etc.
PROC SETUP_INITIAL_ENEMY_WAVE()

	IF bStartedInitialWave
		CPRINTLN(DEBUG_MISSION, "SETUP_INITIAL_ENEMY_WAVE: Already started - exiting.")
		EXIT
	ENDIF
	
	INT idx
	for idx = 0 to 1	
		SETUP_ENEMY_VEHICLE(wave_0_enemy_vehicle[idx])
		//set_entity_only_damaged_by_player(wave_0_enemy_vehicle[idx].veh, true)
		start_playback_recorded_vehicle(wave_0_enemy_vehicle[idx].veh, wave_0_enemy_vehicle[idx].recording_number, "lkmethlab") 
		
		if idx = 0
			skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[idx].veh, 3000) //4000
			set_vehicle_colours(wave_0_enemy_vehicle[idx].veh, 0, 0)
		elif idx = 1
			skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[idx].veh, 4500) //5500
			set_vehicle_colours(wave_0_enemy_vehicle[idx].veh, 10, 10)
		endif 
			
	endfor

	setup_enemy(wave_0_enemy[0])
					
	setup_enemy(wave_0_enemy[1])
			
	setup_enemy_in_vehicle(wave_0_enemy[2], wave_0_enemy_vehicle[0].veh) 
	
	setup_enemy_in_vehicle(wave_0_enemy[3], wave_0_enemy_vehicle[0].veh, vs_front_right) 
		
	setup_enemy_in_vehicle(wave_0_enemy[4], wave_0_enemy_vehicle[0].veh, vs_back_left) 
		
	setup_enemy_in_vehicle(wave_0_enemy[5], wave_0_enemy_vehicle[1].veh) 
		
	setup_enemy_in_vehicle(wave_0_enemy[6], wave_0_enemy_vehicle[1].veh, vs_front_right) 
		
	setup_enemy_in_vehicle(wave_0_enemy[7], wave_0_enemy_vehicle[1].veh, vs_back_left) 
		
	setup_enemy_in_vehicle(wave_0_enemy[8], wave_0_enemy_vehicle[1].veh, vs_back_right) 
ENDPROC

PROC SETUP_INITIAL_ENEMY_WAVE_PROPS()

	IF bInitialWavePropsMade
		CPRINTLN(DEBUG_MISSION, "SETUP_INITIAL_ENEMY_WAVE_PROPS: Already started - exiting.")
		EXIT
	ENDIF
	
	for i = 0 to count_of(bins) - 1
		bins[i].obj = create_object_no_offset(bins[i].model, bins[i].pos)
		SET_ENTITY_ROTATION(bins[i].obj, bins[i].rot)
		FREEZE_ENTITY_POSITION(bins[i].obj, true)
	endfor 
	
	for i = 0 to count_of(explosive_barrels) - 1
		explosive_barrels[i].obj = create_object_no_offset(explosive_barrels[i].model, explosive_barrels[i].pos)
		SET_ENTITY_ROTATION(explosive_barrels[i].obj, explosive_barrels[i].rot)
	endfor 
	
	cover_box.obj = create_object_no_offset(cover_box.model, cover_box.pos)
	freeze_entity_position(cover_box.obj, true)
	SET_ENTITY_ROTATION(cover_box.obj, cover_box.rot)

ENDPROC

proc setup_start_of_shootout()

	if does_entity_exist(trevors_truck.veh)
		if is_vehicle_driveable(trevors_truck.veh)
			set_entity_coords(trevors_truck.veh, <<1409.9990, 3600.4973, 33.8676>>)
			set_entity_heading(trevors_truck.veh, 108.7487)
			set_vehicle_engine_on(trevors_truck.veh, false, true)
		endif 
	endif 

	IF NOT DOES_ENTITY_EXIST(buddy.ped)
		setup_buddy(buddy)
	ENDIF
	IF NOT IS_PED_INJURED(buddy.ped)
		SET_PED_HIGHLY_PERCEPTIVE(buddy.ped, true)
		set_current_ped_weapon(buddy.ped, buddy.weapon, true)
	ENDIF
	add_ped_for_dialogue(scripted_speech[0], 5, buddy.ped, "cook")
	IF NOT DOES_BLIP_EXIST(buddy.blip)
		buddy.blip = CREATE_BLIP_FOR_PED(buddy.ped)
		set_blip_display(buddy.blip, DISPLAY_BLIP)
		SET_BLIP_PRIORITY(buddy.blip, BLIPPRIORITY_HIGHEST)
	ENDIF
//				*****TEMP REMOVE
	//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(buddy.ped, true)
//				*****
	
	// @SBA - this is where cheng and translator get deleted
	if does_entity_exist(translator.ped)
		DELETE_PED(translator.ped)
	endif 
	set_model_as_no_longer_needed(translator.model)
	
	if does_entity_exist(cheng.ped)
		DELETE_PED(cheng.ped)
	endif 
	set_model_as_no_longer_needed(cheng.model)
	
	open_sequence_task(seq)
		task_seek_cover_to_cover_point(null, buddy_cover_point[0], <<1372.01, 3596.31, 37.97>>, -1) 
	close_sequence_task(seq)
	task_perform_sequence(buddy.ped, seq)
	clear_sequence_task(seq)

	SETUP_INITIAL_ENEMY_WAVE()
	
	// @SBA - create parked cars if not already created
	IF NOT DOES_ENTITY_EXIST(parked_cars[3])
		IF HAS_MODEL_LOADED(phoenix)
			setup_parked_cars()
		ENDIF
	ENDIF

	SETUP_INITIAL_ENEMY_WAVE_PROPS()
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, true, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, false, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, false, 0.0, 0.0, -1.0)
	
	original_time = get_game_timer() 
	
	if is_screen_faded_in()
		//end_cutscene_no_fade(false, false, 0, 0, 3000, false)
		//end_cutscene_no_fade(false, true, 90.0, 0, 3000, false) 
		//end_cutscene_no_fade(false, false, (127.0963 - GET_ENTITY_HEADING(PLAYER_PED_ID())), -4.7568, 2000, false)
	else 
		end_cutscene(false, 90.00, -12.0, false)
	endif


	trigger_music_event("CHN1_GAMEPLAY_STARTS")
	
	start_audio_scene("CHI_1_SHOOTOUT_01")
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "Start of Wave 0 shootout")
	
	mission_flow = wave_0_system 

endproc 

proc request_mocap_data_for_CHI_1_MCS_1()

	// @SBA - trying a larger distance to improve streaming at the destination //DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_UNLOAD_DIST,
	//mocap_streaming_system(<<1403.1777, 3597.2070, 34.00>>, 100, 120, "CHI_1_MCS_1")
	
	
	// force the load here
//	IF (GET_DISTANCE_BETWEEN_COORDS(<<1403.1777, 3597.2070, 34.00>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 60)
//		//IF NOT IS_CUTSCENE_ACTIVE() OR NOT HAS_CUTSCENE_LOADED()
//			//CPRINTLN(DEBUG_MISSION, "PLAYER < 60m: REQUESTING ARRIVAL CUTSCENE.")
//	        REQUEST_CUTSCENE("CHI_1_MCS_1")
//		//ENDIF
//	ELSE
//		//  longe range load if in a vehicle
//		IF (GET_DISTANCE_BETWEEN_COORDS(<<1403.1777, 3597.2070, 34.00>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 300)
//		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			//CPRINTLN(DEBUG_MISSION, "PLAYER < 300m: REQUESTING ARRIVAL CUTSCENE.")
//	        REQUEST_CUTSCENE("CHI_1_MCS_1")
//	    ELSE
//			// unload if farther away or not in a vehicle
//	        IF IS_CUTSCENE_ACTIVE() OR HAS_CUTSCENE_LOADED()
//	            IF (GET_DISTANCE_BETWEEN_COORDS(<<1403.1777, 3597.2070, 34.00>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 320)
//	            OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					//CPRINTLN(DEBUG_MISSION, "REMOVING ARRIVAL CUTSCENE.")
//	                REMOVE_CUTSCENE()
//	            ENDIF 
//	        ENDIF 
//	    ENDIF
//	ENDIF
	
	REQUEST_CUTSCENE("CHI_1_MCS_1")
	
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		CPRINTLN(DEBUG_MISSION, "Requesting arrival component variations")
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("trevor", PLAYER_PED_ID())
		ENDIF
		IF NOT IS_PED_INJURED(cheng.ped)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tao", cheng.ped)
		ENDIF
		IF NOT IS_PED_INJURED(translator.ped)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Taos_Translator", translator.ped)
		ENDIF
		IF NOT IS_PED_INJURED(buddy.ped)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("cook", buddy.ped)
		ENDIF
		SET_CUTSCENE_PED_PROP_VARIATION("Cook", ANCHOR_EYES, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Tao", ANCHOR_EYES, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Taos_Translator", ANCHOR_EYES, 0)
	ENDIF
	
endproc 

PROC HANDLE_STUFF_AS_PLAYER_APPROACHES_METH_LAB()
	FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), (<<1403.1777, 3597.2070, 34.5>>))
	
//	IF fDistance < 60.0
//		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
//			IF NEW_LOAD_SCENE_START((<<1410.99, 3587.20, 34.0>>), NORMALISE_VECTOR(<<-0.58, 0.69, 0.45>>), 2750.0)
//				CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Meth Lab Arrival: Calling NEW_LOAD_SCENE_START.")
//			ENDIF
//		ENDIF
//	ELIF fDistance < 200.0
//	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
//			//IF NEW_LOAD_SCENE_START((<<1418.64, 3581.70, 35.85>>), NORMALISE_VECTOR(<<-0.69, 0.72, 0.03>>), 2750.0)
//			IF NEW_LOAD_SCENE_START((<<1410.99, 3587.20, 34.0>>), NORMALISE_VECTOR(<<-0.58, 0.69, 0.45>>), 2750.0)
//				CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Meth Lab Arrival: Calling NEW_LOAD_SCENE_START.")
//			ENDIF
//		ENDIF
//	ELSE
//		IF fDistance > 225.0
//		OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			// stop load scene if needed so it doesn't stay loaded if player is joy riding, or out of vehicle
//			IF IS_NEW_LOAD_SCENE_ACTIVE()
//				NEW_LOAD_SCENE_STOP()
//				CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Meth Lab Arrival (too far): calling NEW_LOAD_SCENE_STOP.")
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF NOT bClearCarGen
		IF fDistance < 225
			CPRINTLN(DEBUG_MISSION, "DELETING OLD CAR GEN.  Fetching SRL.")
			CLEAR_OLD_CAR_GEN()
			PREFETCH_SRL("chi_1_mcs_1")
			bClearCarGen = TRUE
		ENDIF
	ENDIF
	
	IF fDistance < 250
		IF NOT IS_INTERIOR_READY(meth_interior)
			pin_interior_in_memory(meth_interior)
		ENDIF
	ELIF fDistance > 270
		IF IS_INTERIOR_READY(meth_interior)
			UNPIN_INTERIOR(meth_interior)
		ENDIF
	ENDIF

ENDPROC

proc get_to_meth_lab_audio_scene_system()
			
	switch get_to_meth_lab_audio_scene_system_status 
	
		case 0
		
			if is_ped_sitting_in_any_vehicle(player_ped_id())
				start_audio_scene("CHI_1_DRIVE_TO_LAB")
				get_to_meth_lab_audio_scene_system_status++
			endif 
		
		break 
		
		case 1
		
		break 
		
	endswitch 
	
endproc 

/// PURPOSE: returns TRUE if the player is using third person view whilst in cover.    
func bool is_player_third_person_in_cover()
	
	if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person
	
		return true
		
	else 
	
		if get_is_using_fps_third_person_cover()
		
			return true 
			
		endif 
		
	endif 
	
	return false 
	
endfunc 

proc chinese_get_to_meth_lab()

	KILL_PED_HIT_BY_PLAYERS_VEHICLE(cheng.ped)
	KILL_PED_HIT_BY_PLAYERS_VEHICLE(translator.ped)
	HANDLE_CHENGS_CRAZY_CAR_DANCING()
	
	HANDLE_CREATE_BAR_PATRONS()

	switch get_to_meth_lab_status 
	
		case 0
			FLOAT fDistance
			
			// clean up any loaded bar assets
			IF NOT bIsBarCleanedUp
				fDistance =	GET_DISTANCE_BETWEEN_COORDS((<<2000.1245, 3059.5662, 46.0491>>), GET_ENTITY_COORDS(PLAYER_PED_ID()))
				IF fDistance > 50
					CLEANUP_BAR()
					bIsBarCleanedUp = TRUE
				ELSE
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2170997 / 2195354
				ENDIF
			ENDIF

			// @SBA - Need to request early so Chef can be properly registered with cutscene with his props in tact
			//REQUEST_METH_ARRIVAL_ASSETS()
			REQUEST_MODEL(buddy.model)
			HANDLE_SETUP_INITIAL_CHEF()
			
			//request_mocap_data_for_CHI_1_MCS_1()
			
			SET_DOOR_STATE(DOORNAME_METHLAB_F_L, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_METHLAB_F_R, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_METHLAB_R, DOORSTATE_FORCE_LOCKED_THIS_FRAME)

			meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 

			IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				if is_player_at_location_with_buddies_any_means(locates_data, vArrivalMethLab, <<35.0, 35.0, 3.0>>, 
					true, cheng.ped, translator.ped, null, "METH_GOD_8", "METH_GOD_12", "METH_GOD_13", "", 
					"METH_GOD_14", false, true)
				
					// safety
					CLEANUP_BAR()
				
					get_to_meth_lab_status = 50
				// not arrived yet
				ELSE
					HANDLE_STUFF_AS_PLAYER_APPROACHES_METH_LAB()
				ENDIF
			// in helicopter
			ELSE
				HANDLE_STUFF_AS_PLAYER_APPROACHES_METH_LAB()
			ENDIF 
			
			//--DEBUG 
//				if IS_INTERIOR_READY(meth_interior)
//					printstring("meth_interior READY")
//					printnl()
//				else 
//					printstring("meth_interior NOT READY")
//					printnl()
//				endif 
					
			//--END DEBUG 
			
			get_to_meth_lab_dialogue_system()
			
			get_to_meth_lab_audio_scene_system()
			
			IF NOT bSetTaxiDropoff
				IF DOES_BLIP_EXIST(locates_data.LocationBlip)
					CPRINTLN(DEBUG_MISSION, "Meth Lab Arrival: Taxi dropoff set")
					// trying to reduce memory on drive to meth lab
					SET_REDUCE_PED_MODEL_BUDGET(TRUE)
					SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
					SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(locates_data.LocationBlip, <<1447.9056, 3611.7273, 33.8160>>, 104.8207)
					bSetTaxiDropoff = TRUE
				ENDIF
			ENDIF
		
		break 
		
		CASE 50
			// separated this out from previous case so locate won't spam
			IF IS_SRL_LOADED()
				kill_any_conversation()

				clear_mission_locate_stuff(locates_data)
				
				// start lead-in cam
				camera_a = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<1413.1581, 3586.2705, 35.3191>>, <<14.4926, -0.0000, 51.7688>>, 40.0, TRUE)
				camera_b = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<1415.4323, 3585.3635, 35.4866>>, <<5.5924, -0.0000, 49.1407>>, 40.0)
			
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				
				request_mocap_data_for_CHI_1_MCS_1()
				
				get_to_meth_lab_status = 1
			ENDIF
		BREAK 
		
		case 1
			// stop load scene if needed
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF IS_NEW_LOAD_SCENE_LOADED()
					NEW_LOAD_SCENE_STOP()
					CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Meth Lab Arrival: calling NEW_LOAD_SCENE_STOP.")
				ENDIF
			ENDIF
			// @SBA - unload dance dict
			IF NOT bDanceAnimDictUnloaded
				IF NOT IS_ENTITY_DEAD(cheng.ped)
					IF GET_SCRIPT_TASK_STATUS(cheng.ped, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
						CPRINTLN(DEBUG_MISSION, "Removing ", sCrazyDanceDict, " for lead in.")
						REMOVE_ANIM_DICT(sCrazyDanceDict)
						bDanceAnimDictUnloaded = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// @SBA - load just what we need
			REQUEST_METH_ARRIVAL_ASSETS()
			
			HANDLE_SETUP_INITIAL_CHEF()
			request_mocap_data_for_CHI_1_MCS_1()
		
			IF HANDLE_LEAD_IN_SHOT_OF_ARRIVAL()
				END_SRL()
				REDUCE_AMBIENT_MODELS(TRUE)
				get_to_meth_lab_status++
			ENDIF
		break 

		case 2
			IF DOES_ENTITY_EXIST(trevors_truck.veh)
				IF NOT IS_ENTITY_DEAD(trevors_truck.veh)
					IF IS_ENTITY_ON_SCREEN(trevors_truck.veh)
						REQUEST_VEHICLE_HIGH_DETAIL_MODEL(trevors_truck.veh)
					ENDIF
				ENDIF
			ENDIF
	
			// @SBA - load just what we need
			//request_shootout_assets()
			REQUEST_METH_ARRIVAL_ASSETS()
			
			HANDLE_SETUP_INITIAL_CHEF()
			request_mocap_data_for_CHI_1_MCS_1()
			
//			if is_shootout_assets_loaded()
			// @SBA - keep an eye on this in case it causes the lead in to take too long
			IF HAVE_METH_ARRIVAL_ASSETS_LOADED()
			and not is_any_text_being_displayed(locates_data)
				if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				
					if start_new_cutscene_no_fade(false, true, false)
						CPRINTLN(DEBUG_MISSION, "*** ARRIVAL CUTSCENE STARTING ***")
						clear_prints()
												
						players_weapon_obj = CREATE_WEAPON_OBJECT(WEAPONTYPE_CARBINERIFLE, 500, <<1389.4, 3613.0, 39.7>>, true)
						GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(players_weapon_obj, WEAPONCOMPONENT_CARBINERIFLE_CLIP_01)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							register_entity_for_cutscene(PLAYER_PED_ID(), "trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
						ENDIF
						IF NOT IS_PED_INJURED(cheng.ped)
							register_entity_for_cutscene(cheng.ped, "Tao", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_PED_INJURED(translator.ped)
							register_entity_for_cutscene(translator.ped, "Taos_Translator", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_PED_INJURED(buddy.ped)
							register_entity_for_cutscene(buddy.ped, "cook", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF DOES_ENTITY_EXIST(players_weapon_obj)
							register_entity_for_cutscene(players_weapon_obj, "Trevors_Weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					
						//Ross W
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0)
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
					
						start_cutscene()
												
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, false)
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, false)
						
						add_scenario_blocking_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>)
						add_scenario_blocking_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>)
						
						set_ped_paths_in_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>, false)
						
						set_roads_in_area(<<434.6, 3477.0, 36.3>>, <<1605.5, 4247.2, -100.00>>, FALSE)
						set_roads_in_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>, false)

						meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
						pin_interior_in_memory(meth_interior)

						clear_player_wanted_level(player_id())
						//g_allowmaxwantedlevelcheck = false
						set_max_wanted_level(0)
						set_create_random_cops(false)
						disable_dispatch_services()
						
						//SET_ALL_RANDOM_PEDS_FLEE(player_id(), true) //forces random hell billies not to attack the palyer as they can become violent.

						remove_blip(meth_lab_blip)
						
						SET_FRONTEND_RADIO_ACTIVE(TRUE)
						
						trigger_music_event("CHN1_START")
						
						original_time = get_game_timer()
						
						buddy_time = get_game_timer()
												
						get_to_meth_lab_status++
						
					endif 
				ELSE 
					CPRINTLN(DEBUG_MISSION, "*** WAITING ON CUTSCENE TO LOAD ***")
				endif
			ELSE
				CPRINTLN(DEBUG_MISSION, "*** WAITING ON ASSEST/DIALOGUE ***")
					
			endif 

		break 
		
		case 3
		
			if is_cutscene_playing()


				// @SBA - this is where cheng and translator get deleted
				if does_entity_exist(translator.ped)
					CPRINTLN(DEBUG_MISSION, "Unloading Translator")
					DELETE_PED(translator.ped)
					set_model_as_no_longer_needed(translator.model)
				endif 
				
				if does_entity_exist(cheng.ped)
					CPRINTLN(DEBUG_MISSION, "Unloading Cheng")
					DELETE_PED(cheng.ped)
					set_model_as_no_longer_needed(cheng.model)					
				endif
				
				clear_area(<<1403.1777, 3597.2070, 33.7417>>, 400.00, true)
			
				//repostion_players_last_vehicle(players_last_vehicle, <<1402.2213, 3597.3352, 33.8866>>, <<50.00, 50.00, 50.00>>, <<1402.2213, 3597.3352, 33.8866>>, 108.7487)
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(tbArrivalCutResolveVol.vMin, tbArrivalCutResolveVol.vMax, tbArrivalCutResolveVol.flWidth,
					vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
				SET_MISSION_VEHICLE_GEN_VEHICLE(trevors_truck.veh, vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
				
				if is_audio_scene_active("CHI_1_DRIVE_TO_LAB")
					stop_audio_scene("CHI_1_DRIVE_TO_LAB")
				endif
				
				// clean up cameras
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				IF DOES_CAM_EXIST(camera_a)
					SET_CAM_ACTIVE(camera_a, FALSE)
					DESTROY_CAM(camera_a)
				ENDIF
				IF DOES_CAM_EXIST(camera_b)
					SET_CAM_ACTIVE(camera_b, FALSE)
					DESTROY_CAM(camera_b)
				ENDIF

				// stop load scene if needed
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
					CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), ": Meth Lab Arrival (cutscene has started): calling NEW_LOAD_SCENE_STOP.")
				ENDIF
				
				// new cam
				camera_a = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<1391.3059, 3601.0103, 39.5685>>, <<-12.6533, 0.0000, 111.6413>>, 50.0)

			
				get_to_meth_lab_status++
			
			endif 
		
		break 

		case 4
			INT iCutTime
			iCutTime = get_cutscene_time()
			CPRINTLN(DEBUG_MISSION, "Cutscene Time = ", iCutTime)
			
			// @SBA - trying to free up some more memory for the cutscene
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)

		
			IF IS_CUTSCENE_PLAYING()
				if not WAS_CUTSCENE_SKIPPED()
				
					if not has_label_been_triggered("CHN1_ICE_BIN")
						if prepare_music_event("CHN1_ICE_BIN")
							if iCutTime >= 23498
								trigger_music_event("CHN1_ICE_BIN")
								set_label_as_triggered("CHN1_ICE_BIN", true)
								//script_assert("test")
							endif 
						endif 
					
					else 
					
						if not IS_MUSIC_ONESHOT_PLAYING()
							prepare_music_event("CHN1_GAMEPLAY_STARTS")
						endif 
						
					endif
					
					// request assets
					IF iCutTime >= 40000
						REQUEST_FIGHT_ASSETS()
						IN_CASE_OF_ATTACK_BREAK_GLASS()
					ENDIF

					if iCutTime >= 41680
						if not has_label_been_triggered("CHN1_NOW")
							trigger_music_event("CHN1_NOW")
							set_label_as_triggered("CHN1_NOW", true)
						endif
					endif
					
					// @SBA set these up here, as they are now visible out the upstairs window
					IF iCutTime > 45000
						IF NOT DOES_ENTITY_EXIST(parked_cars[3])
							IF HAS_MODEL_LOADED(phoenix)
								setup_parked_cars()
							ENDIF
						ENDIF
					ENDIF

					IF iCutTime > 45100
					AND NOT bInitialWavePropsMade
						IF HAVE_FIGHT_ASSETS_LOADED()
							CPRINTLN(DEBUG_MISSION, "Starting Wave Props Made")
							SETUP_INITIAL_ENEMY_WAVE_PROPS()
							SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
							bInitialWavePropsMade = TRUE
						ELSE
							CPRINTLN(DEBUG_MISSION, "PROPS: WAITING FOR STARTING WAVE ASSESTS TO FINISH LOADING...")
						ENDIF
					ENDIF
					
					IF iCutTime > 45250
					AND NOT bStartedInitialWave
						IF HAVE_FIGHT_ASSETS_LOADED()
							IF SETUP_INITIAL_ENEMY_WAVE_IN_STAGES()
								CPRINTLN(DEBUG_MISSION, "Starting Wave Made")
								bStartedInitialWave = TRUE
							ENDIF
						ELSE
							CPRINTLN(DEBUG_MISSION, "ENEMIES: WAITING FOR STARTING WAVE ASSESTS TO FINISH LOADING...")
						ENDIF
					ENDIF
						
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_Weapon")
						if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
							give_weapon_object_to_ped(players_weapon_obj, player_ped_id())
						else 
							if does_entity_exist(players_weapon_obj)
								delete_object(players_weapon_obj)
							endif 
							if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
								set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
							endif 
						endif 
					endif 

					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)) 
						CPRINTLN(DEBUG_MISSION, "Arrival cutscene: exit for Trevor")
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(127.0963 - GET_ENTITY_HEADING(PLAYER_PED_ID())) 
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4.7568) 

						SETUP_PLAYER_POST_ARRIVAL_CUTSCENE()

					endif 
					
					// Final cutscene cam seems to be too far from where the game cam can be
					// so doing a blend to smooth the pop
					if CAN_SET_EXIT_STATE_FOR_CAMERA()
					
						CPRINTLN(DEBUG_MISSION, "Arrival cutscene: exit for camera")

						if is_player_third_person_in_cover()
			
							IF DOES_CAM_EXIST(camera_a)
								CPRINTLN(DEBUG_MISSION, "Starting blend cam")
								set_cam_active(camera_a, true)
								render_script_cams(true, false)
							ENDIF
						ENDIF
						
						if is_player_third_person_in_cover()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(113.3477 - GET_ENTITY_HEADING(PLAYER_PED_ID())) 
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-12.500) 
						ELSE
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(102.8477 - GET_ENTITY_HEADING(PLAYER_PED_ID())) 
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.600) 
						ENDIF
						
						//IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
						//first person camera with third person in cover 
						//the gameplay camera will not update when player control is off. 
						//fps_with_thrid_person_in_cover the camera exit state is early and trevors exit state 
						//triggers 0.5 seconds later with his cover task.
						//if player control is on. Trevors cover task updates the gameplay camera thus 
						//the relative gameplay camera heading returns to 0. As a result we can only unlock the 
						//player control if in full first person mode. 
						if not is_player_third_person_in_cover()
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						ENDIF

					endif 
					
				else 
			
					if does_entity_exist(translator.ped)
						CPRINTLN(DEBUG_MISSION, "SKIPPED: Unloading Translator")
						DELETE_PED(translator.ped)
					endif 
					set_model_as_no_longer_needed(translator.model)
					
					if does_entity_exist(cheng.ped)
						CPRINTLN(DEBUG_MISSION, "SKIPPED: Unloading Cheng")
						DELETE_PED(cheng.ped)
					endif
					set_model_as_no_longer_needed(cheng.model)					
						
					set_cutscene_fade_values(false, false, true, false)
					
					REQUEST_FIGHT_ASSETS()	
			
					get_to_meth_lab_status++
					
				endif
			ELSE
				SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
				get_to_meth_lab_status = 22

			endif 
			
			if not is_player_third_person_in_cover()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(102.8477 - GET_ENTITY_HEADING(PLAYER_PED_ID())) 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.600) 
			endif 
		
		break 
		
		// IF SKIPPED
		case 5
		
			REQUEST_FIGHT_ASSETS()
		
			if IS_CUTSCENE_PLAYING()
			
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_Weapon")
					if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
						give_weapon_object_to_ped(players_weapon_obj, player_ped_id())
					else 
						if does_entity_exist(players_weapon_obj)
							delete_object(players_weapon_obj)
						endif 
						if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
							set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
						endif 
					endif 
				endif 
				
			else 
				SETUP_PLAYER_POST_ARRIVAL_CUTSCENE()
				IN_CASE_OF_ATTACK_BREAK_GLASS()
			
				while not is_interior_ready(meth_interior)
				or not prepare_music_event("CHN1_GAMEPLAY_STARTS")
					CPRINTLN(DEBUG_MISSION, "*** WAITING FOR INTERIOR AND MUSIC TO FINISH LOADING ***")
				
					wait(0)
					
				endwhile
				
				// shouldn't need to do this, but just in case
				IF DOES_CAM_EXIST(camera_a)
					SET_CAM_ACTIVE(camera_a, FALSE)
					DESTROY_CAM(camera_a)
				ENDIF
				
				trigger_music_event("CHN1_CS_SKIP")
					
				mission_fail_checks()//checks all mission entities alive.
								
				get_to_meth_lab_status = 23

			endif 
		
		break 
		
		// NO SKIP
		CASE 22
//			SETUP_PLAYER_POST_ARRIVAL_CUTSCENE()
			END_CUTSCENE_BASIC()
			IF DOES_CAM_EXIST(camera_a)
				
				DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				
				
				//-- first person camera with third person in cover 
				//blends out earlier from the mocap cutscene. Therefore the camera exit will trigger before the 
				//player exit so the relative game play camera will change when the palyer is given the cover task. 
				//The relative gameplay cam will now be 0. As a result we need to set it again before the interp
				//back to gameplay. 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(102.8477 - GET_ENTITY_HEADING(PLAYER_PED_ID())) 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.600) 
				//--
				
				render_script_cams(FALSE, TRUE, 750)
				
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			get_to_meth_lab_status++
		BREAk
		
		// @SBA - ensure assets have loaded before continuing
		CASE 23
			REQUEST_FIGHT_ASSETS()
			CPRINTLN(DEBUG_MISSION, "*** WAITING FOR FIGHT ASSETS TO FINISH LOADING ***")
			IF HAVE_FIGHT_ASSETS_LOADED()
				// @SBA - return to normal
				REDUCE_AMBIENT_MODELS(FALSE)
				setup_start_of_shootout()
			ENDIF
		BREAK
		
	endswitch 

endproc 

proc meth_lab_vehicle_recording()

	switch vehicle_recording_system_status
	
		case 0

//			for i = 0 to 1			
//				wave_0_enemy_vehicle[i].veh = create_vehicle(wave_0_enemy_vehicle[i].model, wave_0_enemy_vehicle[i].pos, wave_0_enemy_vehicle[i].heading)
//				set_vehicle_doors_locked(wave_0_enemy_vehicle[i].veh, vehiclelock_locked)
//				start_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh, wave_0_enemy_vehicle[i].recording_number, "lkmethlab") 
//				skip_to_end_and_stop_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh)
//			endfor
//			

			
//			for i = 5 to 6		
//				wave_2_enemy_vehicle[i].veh = create_vehicle(wave_2_enemy_vehicle[i].model, wave_2_enemy_vehicle[i].pos, wave_2_enemy_vehicle[i].heading)
//				set_vehicle_doors_locked(wave_2_enemy_vehicle[i].veh, vehiclelock_locked)
//				start_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, wave_2_enemy_vehicle[i].recording_number, "lkmethlab") 
//				skip_to_end_and_stop_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh)
//			endfor

			for i = 5 to 5
				wave_2_enemy_vehicle[i].veh = create_vehicle(wave_2_enemy_vehicle[i].model, wave_2_enemy_vehicle[i].pos, wave_2_enemy_vehicle[i].heading)
				SET_VEHICLE_DIRT_LEVEL(wave_2_enemy_vehicle[i].veh, 13)
				set_vehicle_doors_locked(wave_2_enemy_vehicle[i].veh, vehiclelock_locked)
				start_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, wave_2_enemy_vehicle[i].recording_number, "lkmethlab") 
				skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(19, "lkmethlab") - 100))
				set_entity_collision(wave_2_enemy_vehicle[i].veh, false) 
			endfor
			
			wait(0)
			
			pause_playback_recorded_vehicle(wave_2_enemy_vehicle[5].veh)
			
			
								
		//	comment back in for recording uber vehicles for wave 1
		//	wave_1_uber_vehicle = create_vehicle(pcj, <<1118.8160, 3541.5474, 34.1372>>, 272.0098)
		//	SET_ENTITY_VISIBLE(wave_1_uber_vehicle, false)
		//	SET_ENTITY_COLLISION(wave_1_uber_vehicle, false)
		//	SET_ENTITY_PROOFS(wave_1_uber_vehicle, true, true, true, true, true)
		//				
		//	INITIALISE_UBER_PLAYBACK("lkmethlab", true)
		//	load_uber_data()
		//	bCreateAllWaitingCars = TRUE
		//	start_playback_recorded_vehicle(wave_1_uber_vehicle, 013, "lkmethlab")
		//			
			setup_parked_cars()
			
			for i = 0 to count_of(bins) - 1
				bins[i].obj = create_object_no_offset(bins[i].model, bins[i].pos)
				SET_ENTITY_ROTATION(bins[i].obj, bins[i].rot)
				FREEZE_ENTITY_POSITION(bins[i].obj, true)
			endfor 
			
			vehicle_recording_system_status++
			
		break 
		
		case 1
		
			#if is_debug_build
			DISPLAY_PLAYBACK_RECORDED_VEHICLE(wave_2_enemy_vehicle[2].veh, rdm_wholeline)
			#endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch 
	
endproc 


//outside hicks bar
proc load_trip_skip_data_0()

	request_start_of_mission_assets()
	//REQUEST_BAR_PATRON_ASSETS()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1999.1615, 3057.8936, 46.049>>)
	ENDIF
	
	while not is_start_of_mission_assets_requested()
	//AND NOT HAVE_BAR_PATRON_ASSETS_LOADED()
	
		CPRINTLN(DEBUG_MISSION, "load_trip_skip_data_0 not loaded")
		wait(0)
		
	endwhile 	

	create_start_of_mission_entities(false)
	
	END_REPLAY_SETUP()
	
	
	IF NOT IS_PED_INJURED(player_ped_id())
		clear_ped_tasks_immediately(player_ped_id())
		force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
		SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 300) 
	ENDIF

	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Mission start") 

	if is_screen_faded_out()
		end_cutscene()
	else 
		end_cutscene_no_fade()
	endif 
	
	INIT_CHENGS_CRAZY_CAR_DANCING()
	// @SBA - not creating these guys on restart - was causing asserts I couldn't seem to fix
	bBarPatronsCreated = TRUE

	mission_flow = get_to_meth_lab

endproc 

//wave 0
proc load_trip_skip_data_1()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(buddy.model)
	request_model(wave_0_enemy[0].model)
	request_model(wave_0_enemy[1].model)
	request_model(get_player_veh_model(char_trevor))
	request_model(sadler)
	request_model(dubsta2)
	request_model(phoenix)
	request_model(explosive_barrels[0].model)
	request_model(bins[0].model)
	request_model(cover_box.model)
	request_model(pcj)
	
	request_model(get_weapontype_model(weapontype_carbinerifle))
	request_model(get_weapontype_model(weapontype_grenadelauncher))
	request_model(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
	
	request_weapon_asset(WEAPONTYPE_carbinerifle)
	request_weapon_asset(WEAPONTYPE_grenadelauncher)
	request_weapon_asset(WEAPONTYPE_ASSAULTRIFLE)
	
	request_vehicle_recording(001, "lkmethlab")
	request_vehicle_recording(002, "lkmethlab")
	request_vehicle_recording(003, "lkmethlab")
//	request_vehicle_recording(004, "lkmethlab")
//	request_vehicle_recording(005, "lkmethlab")
//	request_vehicle_recording(006, "lkmethlab")
	request_vehicle_recording(007, "lkmethlab")
	request_vehicle_recording(008, "lkmethlab")
	request_vehicle_recording(009, "lkmethlab")
	request_vehicle_recording(010, "lkmethlab")
	request_vehicle_recording(011, "lkmethlab")
	request_vehicle_recording(012, "lkmethlab")
//	request_vehicle_recording(013, "lkmethlab")
//	request_vehicle_recording(014, "lkmethlab")
//	request_vehicle_recording(015, "lkmethlab")
//	request_vehicle_recording(016, "lkmethlab")
	request_vehicle_recording(017, "lkmethlab")
	request_vehicle_recording(018, "lkmethlab")
	request_vehicle_recording(019, "lkmethlab")
	request_vehicle_recording(020, "lkmethlab")
	request_vehicle_recording(021, "lkmethlab")
//	request_vehicle_recording(030, "lkmethlab")
//	request_vehicle_recording(031, "lkmethlab")
//	request_vehicle_recording(032, "lkmethlab")
	
	REQUEST_WAYPOINT_RECORDING("methlab1")
//	REQUEST_WAYPOINT_RECORDING("methlab2")
	REQUEST_WAYPOINT_RECORDING("methlab3")
	REQUEST_WAYPOINT_RECORDING("methlab4")
	REQUEST_WAYPOINT_RECORDING("methlab5")
//	REQUEST_WAYPOINT_RECORDING("methlab6")
	REQUEST_WAYPOINT_RECORDING("methlab7")
//	REQUEST_WAYPOINT_RECORDING("methlab8")
//	REQUEST_WAYPOINT_RECORDING("methlab9")

	request_anim_dict("MissChinese1") 
	
	meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
	pin_interior_in_memory(meth_interior)


	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(buddy.model)
	or not has_model_loaded(wave_0_enemy[0].model)
	or not has_model_loaded(wave_0_enemy[1].model)
	or not has_model_loaded(get_player_veh_model(char_trevor))
	or not has_model_loaded(sadler)
	or not has_model_loaded(dubsta2)
	or not has_model_loaded(phoenix)
	or not has_model_loaded(explosive_barrels[0].model)
	or not has_model_loaded(bins[0].model)
	or not has_model_loaded(cover_box.model)
	or not has_model_loaded(pcj)
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_carbinerifle))
	or not has_model_loaded(get_weapontype_model(weapontype_grenadelauncher))
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
	or not has_weapon_asset_loaded(WEAPONTYPE_carbinerifle)
	or not has_weapon_asset_loaded(WEAPONTYPE_grenadelauncher)
	or not has_weapon_asset_loaded(WEAPONTYPE_ASSAULTRIFLE)
	or not has_vehicle_recording_been_loaded(001, "lkmethlab")
	or not has_vehicle_recording_been_loaded(002, "lkmethlab")
	or not has_vehicle_recording_been_loaded(003, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(004, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(005, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(006, "lkmethlab")
	or not has_vehicle_recording_been_loaded(007, "lkmethlab")
	or not has_vehicle_recording_been_loaded(008, "lkmethlab")
	or not has_vehicle_recording_been_loaded(009, "lkmethlab")
	or not has_vehicle_recording_been_loaded(010, "lkmethlab")
	or not has_vehicle_recording_been_loaded(011, "lkmethlab")
	or not has_vehicle_recording_been_loaded(012, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(013, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(014, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(015, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(016, "lkmethlab")
	or not has_vehicle_recording_been_loaded(017, "lkmethlab")
	or not has_vehicle_recording_been_loaded(018, "lkmethlab")
	or not has_vehicle_recording_been_loaded(019, "lkmethlab")
	or not has_vehicle_recording_been_loaded(020, "lkmethlab")
	or not has_vehicle_recording_been_loaded(021, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(030, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(031, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(032, "lkmethlab")
	or not get_is_waypoint_recording_loaded("methlab1")
//	or not get_is_waypoint_recording_loaded("methlab2")
	or not get_is_waypoint_recording_loaded("methlab3")
	or not get_is_waypoint_recording_loaded("methlab4")
	or not get_is_waypoint_recording_loaded("methlab5")
//	or not get_is_waypoint_recording_loaded("methlab6")
	or not get_is_waypoint_recording_loaded("methlab7")
//	or not get_is_waypoint_recording_loaded("methlab8")
//	or not get_is_waypoint_recording_loaded("methlab9")
	or not has_anim_dict_loaded("MissChinese1")
	or not is_interior_ready(meth_interior)
	
		CPRINTLN(DEBUG_MISSION, "load_trip_skip_data_1 not loaded")
		wait(0)
		
	endwhile

	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)
	disable_dispatch_services()
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, false)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, false)
	
	add_scenario_blocking_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>)
	add_scenario_blocking_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>)
	
	set_ped_paths_in_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>, false)
	
	set_roads_in_area(<<434.6, 3477.0, 36.3>>, <<1605.5, 4247.2, -100.00>>, FALSE)
	set_roads_in_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>, false)
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, true, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, false, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, false, 0.0, 0.0, -1.0)

	// @SBA - opening outer doors to help player follow Chef
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, TRUE, 0.0, 0.0, 1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, TRUE, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_door04, vInteriorDoorStore, TRUE, 0.0, 0.0, -1.0)
	
	clear_area(<<1998.2826, 3058.0435, 46.0491>>, 10000.00, true)
	remove_decals_in_range(<<1998.2826, 3058.0435, 46.0491>>, 10000.00)

	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(player_ped_id(), vTrevorWindowStartPos)  // @SBA - adjusting start position slightly. OLD VALUE = <<1389.5963, 3600.4568, 37.9418>>) 
		SET_ENTITY_HEADING(player_ped_id(), 21.5294)
	ENDIF
	
	//player should alway have the carbine rifle here
	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, false)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
			set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
		endif 
	endif 
	
	set_current_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE, true)
		
	// @SBA - player vehicle
	RECREATE_PLAYERS_VEHICLE(vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
	
	setup_buddy(buddy)
	add_ped_for_dialogue(scripted_speech[0], 5, buddy.ped, "cook")
	set_current_ped_weapon(buddy.ped, buddy.weapon, true)
//				*****TEMP REMOVE
	//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(buddy.ped, true)
//				*****
	
	open_sequence_task(seq)
		task_seek_cover_to_cover_point(null, buddy_cover_point[0], <<1372.01, 3596.31, 37.97>>, -1) 
	close_sequence_task(seq)
	task_perform_sequence(buddy.ped, seq)
	clear_sequence_task(seq)

	
	for i = 0 to 1
		SETUP_ENEMY_VEHICLE(wave_0_enemy_vehicle[i])
		start_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh, wave_0_enemy_vehicle[i].recording_number, "lkmethlab") 
		
		if i = 0
			skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh, 3000) //4000
			set_vehicle_colours(wave_0_enemy_vehicle[i].veh, 0, 0)
		elif i = 1
			skip_time_in_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh, 4500) //5500
			set_vehicle_colours(wave_0_enemy_vehicle[i].veh, 10, 10)
		endif 
		
	endfor

	setup_enemy(wave_0_enemy[0])
					
	setup_enemy(wave_0_enemy[1])
			
	setup_enemy_in_vehicle(wave_0_enemy[2], wave_0_enemy_vehicle[0].veh) 
	
	setup_enemy_in_vehicle(wave_0_enemy[3], wave_0_enemy_vehicle[0].veh, vs_front_right) 
		
	setup_enemy_in_vehicle(wave_0_enemy[4], wave_0_enemy_vehicle[0].veh, vs_back_left) 
		
	setup_enemy_in_vehicle(wave_0_enemy[5], wave_0_enemy_vehicle[1].veh) 
		
	setup_enemy_in_vehicle(wave_0_enemy[6], wave_0_enemy_vehicle[1].veh, vs_front_right) 
		
	setup_enemy_in_vehicle(wave_0_enemy[7], wave_0_enemy_vehicle[1].veh, vs_back_left) 
		
	setup_enemy_in_vehicle(wave_0_enemy[8], wave_0_enemy_vehicle[1].veh, vs_back_right) 
			
	setup_parked_cars()

	for i = 0 to count_of(bins) - 1
		bins[i].obj = create_object_no_offset(bins[i].model, bins[i].pos)
		SET_ENTITY_ROTATION(bins[i].obj, bins[i].rot)
		FREEZE_ENTITY_POSITION(bins[i].obj, true)
	endfor 
	
	for i = 0 to count_of(explosive_barrels) - 1
		explosive_barrels[i].obj = create_object_no_offset(explosive_barrels[i].model, explosive_barrels[i].pos)
		SET_ENTITY_ROTATION(explosive_barrels[i].obj, explosive_barrels[i].rot)
	endfor 
	
	cover_box.obj = create_object_no_offset(cover_box.model, cover_box.pos)
	freeze_entity_position(cover_box.obj, true)
	SET_ENTITY_ROTATION(cover_box.obj, cover_box.rot)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1389.5963, 3600.4568, 37.9419>>)
	ENDIF
	
	END_REPLAY_SETUP()
	
	task_put_ped_directly_into_cover(player_ped_id(), vTrevorWindowStartPos, -1, false, 0.0, true, false, players_cover_point) // @SBA - adjusting start position slightly. OLD VALUE = <<1389.63, 3600.41, 37.94>>
	force_ped_ai_and_animation_update(player_ped_id(), true)
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)

	IN_CASE_OF_ATTACK_BREAK_GLASS()
		
	original_time = get_game_timer() 
	
	buddy_time = get_game_timer()
	
	trigger_music_event("CHN1_WAVE_ZERO_RT")
	
	start_audio_scene("CHI_1_SHOOTOUT_01")
	
	end_cutscene_no_fade(false, TRUE, 90, -12, 500, false)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "Start of Wave 0 shootout")
	
	mission_flow = wave_0_system 
	
endproc

//wave 0 outside enemies
proc load_trip_skip_data_2()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(buddy.model)
	request_model(wave_0_enemy[0].model)
	request_model(wave_0_enemy[1].model)
	request_model(get_player_veh_model(char_trevor))
	request_model(sadler)
	request_model(dubsta2)
	request_model(phoenix)
	request_model(explosive_barrels[0].model)
	request_model(bins[0].model)
	request_model(cover_box.model)
	request_model(pcj)
	request_model(get_weapontype_model(weapontype_carbinerifle))
	request_model(get_weapontype_model(weapontype_grenadelauncher))
	request_model(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
	
	request_weapon_asset(WEAPONTYPE_carbinerifle)
	request_weapon_asset(WEAPONTYPE_grenadelauncher)
	request_weapon_asset(WEAPONTYPE_ASSAULTRIFLE)
	
	request_vehicle_recording(001, "lkmethlab")
	request_vehicle_recording(002, "lkmethlab")
	request_vehicle_recording(003, "lkmethlab")
//	request_vehicle_recording(004, "lkmethlab")
//	request_vehicle_recording(005, "lkmethlab")
//	request_vehicle_recording(006, "lkmethlab")
	request_vehicle_recording(007, "lkmethlab")
	request_vehicle_recording(008, "lkmethlab")
	request_vehicle_recording(009, "lkmethlab")
	request_vehicle_recording(010, "lkmethlab")
	request_vehicle_recording(011, "lkmethlab")
	request_vehicle_recording(012, "lkmethlab")
//	request_vehicle_recording(013, "lkmethlab")
//	request_vehicle_recording(014, "lkmethlab")
//	request_vehicle_recording(015, "lkmethlab")
//	request_vehicle_recording(016, "lkmethlab")
	request_vehicle_recording(017, "lkmethlab")
	request_vehicle_recording(018, "lkmethlab")
	request_vehicle_recording(019, "lkmethlab")
	request_vehicle_recording(020, "lkmethlab")
	request_vehicle_recording(021, "lkmethlab")
//	request_vehicle_recording(030, "lkmethlab")
//	request_vehicle_recording(031, "lkmethlab")
//	request_vehicle_recording(032, "lkmethlab")
	
	REQUEST_WAYPOINT_RECORDING("methlab1")
//	REQUEST_WAYPOINT_RECORDING("methlab2")
	REQUEST_WAYPOINT_RECORDING("methlab3")
	REQUEST_WAYPOINT_RECORDING("methlab4")
	REQUEST_WAYPOINT_RECORDING("methlab5")
//	REQUEST_WAYPOINT_RECORDING("methlab6")
	REQUEST_WAYPOINT_RECORDING("methlab7")
//	REQUEST_WAYPOINT_RECORDING("methlab8")
//	REQUEST_WAYPOINT_RECORDING("methlab9")
	
	request_anim_dict("MissChinese1") 
	
	meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
	pin_interior_in_memory(meth_interior)


	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(buddy.model)
	or not has_model_loaded(wave_0_enemy[0].model)
	or not has_model_loaded(wave_0_enemy[1].model)
	or not has_model_loaded(get_player_veh_model(char_trevor))
	or not has_model_loaded(sadler)
	or not has_model_loaded(dubsta2)
	or not has_model_loaded(phoenix)
	or not has_model_loaded(explosive_barrels[0].model)
	or not has_model_loaded(bins[0].model)
	or not has_model_loaded(cover_box.model)
	or not has_model_loaded(pcj)
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_carbinerifle))
	or not has_model_loaded(get_weapontype_model(weapontype_grenadelauncher))
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_ASSAULTRIFLE))
	or not has_weapon_asset_loaded(WEAPONTYPE_carbinerifle)
	or not has_weapon_asset_loaded(WEAPONTYPE_grenadelauncher)
	or not has_weapon_asset_loaded(WEAPONTYPE_ASSAULTRIFLE)
	or not has_vehicle_recording_been_loaded(001, "lkmethlab")
	or not has_vehicle_recording_been_loaded(002, "lkmethlab")
	or not has_vehicle_recording_been_loaded(003, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(004, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(005, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(006, "lkmethlab")
	or not has_vehicle_recording_been_loaded(007, "lkmethlab")
	or not has_vehicle_recording_been_loaded(008, "lkmethlab")
	or not has_vehicle_recording_been_loaded(009, "lkmethlab")
	or not has_vehicle_recording_been_loaded(010, "lkmethlab")
	or not has_vehicle_recording_been_loaded(011, "lkmethlab")
	or not has_vehicle_recording_been_loaded(012, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(013, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(014, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(015, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(016, "lkmethlab")
	or not has_vehicle_recording_been_loaded(017, "lkmethlab")
	or not has_vehicle_recording_been_loaded(018, "lkmethlab")
	or not has_vehicle_recording_been_loaded(019, "lkmethlab")
	or not has_vehicle_recording_been_loaded(020, "lkmethlab")
	or not has_vehicle_recording_been_loaded(021, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(030, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(031, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(032, "lkmethlab")
	or not get_is_waypoint_recording_loaded("methlab1")
//	or not get_is_waypoint_recording_loaded("methlab2")
	or not get_is_waypoint_recording_loaded("methlab3")
	or not get_is_waypoint_recording_loaded("methlab4")
	or not get_is_waypoint_recording_loaded("methlab5")
//	or not get_is_waypoint_recording_loaded("methlab6")
	or not get_is_waypoint_recording_loaded("methlab7")
//	or not get_is_waypoint_recording_loaded("methlab8")
//	or not get_is_waypoint_recording_loaded("methlab9")
	or not has_anim_dict_loaded("MissChinese1")
	or not is_interior_ready(meth_interior)
	
		CPRINTLN(DEBUG_MISSION, "load_trip_skip_data_2 not loaded")
		wait(0)
		
	endwhile
	
	clear_player_wanted_level(player_id())
	//g_allowmaxwantedlevelcheck = false
	set_max_wanted_level(0)
	set_create_random_cops(false)
	disable_dispatch_services()
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, false)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, false)
	
	add_scenario_blocking_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>)
	add_scenario_blocking_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>)
	
	set_roads_in_area(<<434.6, 3477.0, 36.3>>, <<1605.5, 4247.2, -100.00>>, FALSE)
	set_roads_in_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>, false)
		
	clear_area(<<1390.8401, 3601.1016, 37.9419>>, 1000, true)
	
	//front of shop doors
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, true, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, false, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, false, 0.0, 0.0, -1.0)

	//gates outside methlab
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_arm_gate_l, <<1372.27, 3608.99, 35.51>>, true, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_arm_gate_l, <<1378.05, 3611.09, 35.51>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_arm_gate_l, <<1372.27, 3608.99, 35.51>>, false, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_arm_gate_l, <<1378.05, 3611.09, 35.51>>, false, 0.0, 0.0, -1.0)
		
	// @SBA - opening outer doors to help player follow Chef
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, TRUE, 0.0, 0.0, 1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, TRUE, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_door04, vInteriorDoorStore, TRUE, 0.0, 0.0, -1.0)
	
	setup_parked_cars()

	for i = 0 to count_of(bins) - 1
		bins[i].obj = create_object_no_offset(bins[i].model, bins[i].pos)
		SET_ENTITY_ROTATION(bins[i].obj, bins[i].rot)
		FREEZE_ENTITY_POSITION(bins[i].obj, true)
	endfor 
	
	for i = 0 to count_of(explosive_barrels) - 1
		explosive_barrels[i].obj = create_object_no_offset(explosive_barrels[i].model, explosive_barrels[i].pos)
		SET_ENTITY_ROTATION(explosive_barrels[i].obj, explosive_barrels[i].rot)
	endfor 
	
	cover_box.obj = create_object_no_offset(cover_box.model, cover_box.pos)
	freeze_entity_position(cover_box.obj, true)
	SET_ENTITY_ROTATION(cover_box.obj, cover_box.rot)

	IF g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_BOTH_CARS_DEAD
	OR g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_ONLY_CAR1_ALIVE
		wave_0_enemy_vehicle[0].been_destroyed = TRUE
	ENDIF
	
	IF g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_BOTH_CARS_DEAD
	OR g_replay.iReplayInt[PLAYER_DESTROYED_VEHICLES] = WAVE0_ONLY_CAR0_ALIVE
		wave_0_enemy_vehicle[1].been_destroyed = TRUE
	ENDIF		
	
	for i = 0 to 2
		IF NOT wave_0_enemy_vehicle[i].been_destroyed
			SETUP_ENEMY_VEHICLE(wave_0_enemy_vehicle[i])
			start_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh, wave_0_enemy_vehicle[i].recording_number, "lkmethlab") 
			skip_to_end_and_stop_playback_recorded_vehicle(wave_0_enemy_vehicle[i].veh)	
			// @SBA - set colors
			if i = 0
				set_vehicle_colours(wave_0_enemy_vehicle[i].veh, 0, 0)
				REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[i])
			elif i = 1
				set_vehicle_colours(wave_0_enemy_vehicle[i].veh, 10, 10)
				REMOVE_VEHICLE_BLIP(wave_0_enemy_vehicle[i])
			endif
		ENDIF
	endfor
	
	setup_enemy_in_vehicle(wave_0_inside_enemy[0], wave_0_enemy_vehicle[2].veh) 			
	setup_enemy_in_vehicle(wave_0_inside_enemy[1], wave_0_enemy_vehicle[2].veh, vs_front_right) 
	setup_enemy_in_vehicle(wave_0_inside_enemy[2], wave_0_enemy_vehicle[2].veh, vs_back_left) 
	setup_enemy_in_vehicle(wave_0_inside_enemy[3], wave_0_enemy_vehicle[2].veh, vs_back_right) 
	
	for i = 0 to count_of(wave_0_inside_enemy) - 1
		SET_COMBAT_FLOAT(wave_0_inside_enemy[i].ped, CCF_WEAPON_ACCURACY, 0.10)
	endfor 

	clear_ped_tasks_immediately(player_ped_id())
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(player_ped_id(), <<1390.8401, 3601.1016, 37.9419>>)
		SET_ENTITY_HEADING(player_ped_id(), 323.8041)
	ENDIF
	
	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, false)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
			set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
		endif 
	endif 
	
	setup_weapon_for_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, true, true, false)

	
	// @SBA - trying this
//	CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<1409.9990, 3600.4973, 33.8676>>, 108.7487, false)
//	set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_locked)
//	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, false)
//	set_vehicle_engine_on(trevors_truck.veh, false, true)
	RECREATE_PLAYERS_VEHICLE(vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)

	buddy.pos = <<1387.5570, 3607.2000, 37.9419>>
	buddy.heading = 244.5938
	setup_buddy(buddy)
	set_current_ped_weapon(buddy.ped, weapontype_carbinerifle, true)
	add_ped_for_dialogue(scripted_speech[0], 5, buddy.ped, "cook")
	task_look_at_entity(buddy.ped, player_ped_id(), -1)

	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1389.5963, 3600.4568, 37.9419>>)
	ENDIF
	
	END_REPLAY_SETUP()
	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	
	// re-break glass
	object_index glass_window
	IF NOT DOES_ENTITY_EXIST(glass_window)
		glass_window = get_closest_object_of_type(<<1389.11, 3600.43, 39.51>>, 2.0, v_ret_ml_win5)
	ENDIF
	
	//crack type 
	//2 = shoutgun
	//3 = explosion
	break_entity_glass(glass_window, <<1389.16, 3600.3, 39.5>>, 2.0, <<0.0, 1.0, 0.0>>, 1100.00, 3, TRUE)
	break_entity_glass(glass_window, <<1388.8, 3600.9, 39.0>>, 2.0, <<0.0, 1.0, 0.0>>, 1100, 2, TRUE)
	break_entity_glass(glass_window, <<1389.3, 3600.1, 39.7>>, 2.0, <<0.0, 1.0, 0.0>>, 1100.00, 2, TRUE)

	SET_OBJECT_AS_NO_LONGER_NEEDED(glass_window)

	//fakes all the wave_0_enemy as dead.
	for i = 0 to count_of(wave_0_enemy) - 1
		wave_0_enemy[i].created = true
	endfor 
	
	wave_0_dialogue_system_status = 22
	wave_0_buddy_ai_system_status = 1
	wave_0_assisted_nodes_system_status = 1
	wave_0_master_flow_system_status = 50  // dialogue set properly here
	
	bChefRunsToBackStairs = TRUE

	mission_flow = wave_0_system 
	
	original_time = get_game_timer()

	trigger_music_event("CHN1_OUTSIDE_RT")
				
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(30)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "Wave 0 outside enemies")
	
endproc 

//wave 2 after grenade launcher cutscene
proc load_trip_skip_data_3()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(buddy.model)
	request_model(wave_0_enemy[0].model)
	request_model(wave_0_enemy[1].model)
	request_model(get_player_veh_model(char_trevor))
	request_model(sadler)
	request_model(dubsta2)
	request_model(phoenix)
	request_model(explosive_barrels[0].model)
	request_model(bins[0].model)
	request_model(cover_box.model)
	request_model(pcj)
	request_model(c4[0].model)
	request_model(get_weapontype_model(weapontype_carbinerifle))
	request_weapon_asset(weapontype_carbinerifle)
	request_model(get_weapontype_model(weapontype_grenadelauncher))
	request_weapon_asset(WEAPONTYPE_grenadelauncher)

	request_vehicle_recording(001, "lkmethlab")
	request_vehicle_recording(002, "lkmethlab")
	request_vehicle_recording(003, "lkmethlab")
//	request_vehicle_recording(004, "lkmethlab")
//	request_vehicle_recording(005, "lkmethlab")
//	request_vehicle_recording(006, "lkmethlab")
	request_vehicle_recording(007, "lkmethlab")
	request_vehicle_recording(008, "lkmethlab")
	request_vehicle_recording(009, "lkmethlab")
	request_vehicle_recording(010, "lkmethlab")
	request_vehicle_recording(011, "lkmethlab")
	request_vehicle_recording(012, "lkmethlab")
//	request_vehicle_recording(013, "lkmethlab")
//	request_vehicle_recording(014, "lkmethlab")
//	request_vehicle_recording(015, "lkmethlab")
//	request_vehicle_recording(016, "lkmethlab")
	request_vehicle_recording(017, "lkmethlab")
	request_vehicle_recording(018, "lkmethlab")
	request_vehicle_recording(019, "lkmethlab")
	request_vehicle_recording(020, "lkmethlab")
	request_vehicle_recording(021, "lkmethlab")
//	request_vehicle_recording(030, "lkmethlab")
//	request_vehicle_recording(031, "lkmethlab")
//	request_vehicle_recording(032, "lkmethlab")
	
	REQUEST_WAYPOINT_RECORDING("methlab1")
//	REQUEST_WAYPOINT_RECORDING("methlab2")
	REQUEST_WAYPOINT_RECORDING("methlab3")
	REQUEST_WAYPOINT_RECORDING("methlab4")
	REQUEST_WAYPOINT_RECORDING("methlab5")
//	REQUEST_WAYPOINT_RECORDING("methlab6")
	REQUEST_WAYPOINT_RECORDING("methlab7")
//	REQUEST_WAYPOINT_RECORDING("methlab8")
//	REQUEST_WAYPOINT_RECORDING("methlab9")
	
	request_anim_dict("MissChinese1") 
	
	meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
	pin_interior_in_memory(meth_interior)

	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(buddy.model)
	or not has_model_loaded(wave_0_enemy[0].model)
	or not has_model_loaded(wave_0_enemy[1].model)
	or not has_model_loaded(get_player_veh_model(char_trevor))
	or not has_model_loaded(sadler)
	or not has_model_loaded(dubsta2)
	or not has_model_loaded(phoenix)
	or not has_model_loaded(explosive_barrels[0].model)
	or not has_model_loaded(bins[0].model)
	or not has_model_loaded(cover_box.model)
	or not has_model_loaded(pcj)
	or not has_model_loaded(c4[0].model)
	or not has_model_loaded(get_weapontype_model(weapontype_carbinerifle))
	or not has_weapon_asset_loaded(weapontype_carbinerifle)
	or not has_model_loaded(get_weapontype_model(weapontype_grenadelauncher))
	or not has_weapon_asset_loaded(weapontype_grenadelauncher)
	or not has_vehicle_recording_been_loaded(001, "lkmethlab")
	or not has_vehicle_recording_been_loaded(002, "lkmethlab")
	or not has_vehicle_recording_been_loaded(003, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(004, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(005, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(006, "lkmethlab")
	or not has_vehicle_recording_been_loaded(007, "lkmethlab")
	or not has_vehicle_recording_been_loaded(008, "lkmethlab")
	or not has_vehicle_recording_been_loaded(009, "lkmethlab")
	or not has_vehicle_recording_been_loaded(010, "lkmethlab")
	or not has_vehicle_recording_been_loaded(011, "lkmethlab")
	or not has_vehicle_recording_been_loaded(012, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(013, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(014, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(015, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(016, "lkmethlab")
	or not has_vehicle_recording_been_loaded(017, "lkmethlab")
	or not has_vehicle_recording_been_loaded(018, "lkmethlab")
	or not has_vehicle_recording_been_loaded(019, "lkmethlab")
	or not has_vehicle_recording_been_loaded(020, "lkmethlab")
	or not has_vehicle_recording_been_loaded(021, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(030, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(031, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(032, "lkmethlab")
	or not get_is_waypoint_recording_loaded("methlab1")
//	or not get_is_waypoint_recording_loaded("methlab2")
	or not get_is_waypoint_recording_loaded("methlab3")
	or not get_is_waypoint_recording_loaded("methlab4")
	or not get_is_waypoint_recording_loaded("methlab5")
//	or not get_is_waypoint_recording_loaded("methlab6")
	or not get_is_waypoint_recording_loaded("methlab7")
//	or not get_is_waypoint_recording_loaded("methlab8")
//	or not get_is_waypoint_recording_loaded("methlab9")
	or not has_anim_dict_loaded("MissChinese1")
	or not is_interior_ready(meth_interior)
	
		CPRINTLN(DEBUG_MISSION, "load_trip_skip_data_3 not loaded")
	
		wait(0)
		
	endwhile
	
	//setup dead ped flags from before grenade launcher cutscene 
	//kids dead system on peds have been created
	for i = 2 to 5	
		wave_2_enemy[i].created = true
	endfor 	
	wave_2_enemy[10].created = true
	wave_2_enemy[11].created = true
	wave_2_enemy[19].created = true 


	clear_player_wanted_level(player_id())
	//g_allowmaxwantedlevelcheck = false
	set_max_wanted_level(0)
	set_create_random_cops(false)
	disable_dispatch_services()
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, false)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, false)
	
	add_scenario_blocking_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>)
	add_scenario_blocking_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>)
	
	set_ped_paths_in_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>, false)
	
	set_roads_in_area(<<434.6, 3477.0, 36.3>>, <<1605.5, 4247.2, -100.00>>, FALSE)
	set_roads_in_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>, false)
	
//	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, true, 0.0, 0.0, 1.0) 
//	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, false, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, false, 0.0, 0.0, -1.0)

	// @SBA - opening outer doors to help player follow Chef
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, TRUE, 0.0, 0.0, 1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_door04, vInteriorDoorStore, TRUE, 0.0, 0.0, -1.0)
	// shutting this door for frame rate
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, FALSE, 0.0, 0.0, 0.0)
	
	clear_area(<<1998.2826, 3058.0435, 46.0491>>, 10000.00, true)
	remove_decals_in_range(<<1998.2826, 3058.0435, 46.0491>>, 10000.00)
	
	setup_parked_cars()

	for i = 0 to count_of(bins) - 1
		bins[i].obj = create_object_no_offset(bins[i].model, bins[i].pos)
		SET_ENTITY_ROTATION(bins[i].obj, bins[i].rot)
		FREEZE_ENTITY_POSITION(bins[i].obj, true)
	endfor 
	
	for i = 0 to count_of(explosive_barrels) - 1
		explosive_barrels[i].obj = create_object_no_offset(explosive_barrels[i].model, explosive_barrels[i].pos)
		SET_ENTITY_ROTATION(explosive_barrels[i].obj, explosive_barrels[i].rot)
	endfor 
	
	cover_box.obj = create_object_no_offset(cover_box.model, cover_box.pos)
	freeze_entity_position(cover_box.obj, true)
	SET_ENTITY_ROTATION(cover_box.obj, cover_box.rot)
	
	// @SBA - trying this
//	CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<1409.9990, 3600.4973, 33.8676>>, 108.7487, false)
//	set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_locked)
//	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, false)
//	set_vehicle_engine_on(trevors_truck.veh, false, true)
	RECREATE_PLAYERS_VEHICLE(vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
	
	for i = 2 to 6
		if i != 5
			SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[i])
			start_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, wave_2_enemy_vehicle[i].recording_number, "lkmethlab")
			skip_to_end_and_stop_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh)
			REMOVE_VEHICLE_BLIP(wave_2_enemy_vehicle[i])
		endif 
	endfor 
	
	for i = 0 to 1
		wave_2_enemy[i].pos = wave_2_enemy[i].run_to_pos
		setup_enemy(wave_2_enemy[i])
		TASK_AIM_GUN_AT_ENTITY(wave_2_enemy[i].ped, PLAYER_PED_ID(), -1, TRUE)
		SET_PED_RESET_FLAG(wave_2_enemy[i].ped, PRF_InstantBlendToAim, TRUE)
	endfor 
									
	for i = 18 to 18//19 
		wave_2_enemy[i].pos = wave_2_enemy[i].run_to_pos
		setup_enemy(wave_2_enemy[i])
		TASK_AIM_GUN_AT_ENTITY(wave_2_enemy[i].ped, PLAYER_PED_ID(), -1, TRUE)
		SET_PED_RESET_FLAG(wave_2_enemy[i].ped, PRF_InstantBlendToAim, TRUE)
	endfor 
	
//***************** AFTER CUTSCENE


	for i = 0 to 1
		SETUP_ENEMY_VEHICLE(wave_2_enemy_vehicle[i])
		start_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, wave_2_enemy_vehicle[i].recording_number, "lkmethlab")
		
		if i = 0
			skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, 8000)
		elif i = 1
			skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, 5500)
		elif i = 2
			skip_time_in_playback_recorded_vehicle(wave_2_enemy_vehicle[i].veh, 500)
		endif 
		
	endfor

	setup_enemy_in_vehicle(wave_2_enemy[6], wave_2_enemy_vehicle[0].veh) 
	setup_enemy_in_vehicle(wave_2_enemy[7], wave_2_enemy_vehicle[0].veh, vs_front_right) 
	
	setup_enemy_in_vehicle(wave_2_enemy[8], wave_2_enemy_vehicle[1].veh) 
	setup_enemy_in_vehicle(wave_2_enemy[9], wave_2_enemy_vehicle[1].veh, vs_front_right) 
	
	
	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, false, false)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
			set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
		endif 
	endif
	
	// standard resets
	player_grenade_launcher_cutscene_status = 99 //DEACTIVATES grenade launcher cutscene. 
	wave_2_buddy_ai_system_status = 3
	wave_2_dialogue_system_status = 22
	wave_2_assisted_nodes_system_status = 0
	wave_2_master_flow_system_status =  6
	wave_2_audio_scene_system_status = 3 

	
	// @SBA - play grenade launcher cutscene if needed
	IF g_replay.iReplayInt[PLAYER_RECEIVED_GRENADE_LAUNCHER] = NOT_GOT_GLAUNCHER
		CPRINTLN(DEBUG_MISSION, "load_trip_skip_data_3: ARE playing gren launch cutscene")
		set_current_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE, true)
		// update resets
		player_grenade_launcher_cutscene_status = 0	
		wave_2_master_flow_system_status =  5
		wave_2_audio_scene_system_status = 1
	// Don't need to see it
	ELSE
		CPRINTLN(DEBUG_MISSION, "load_trip_skip_data_3: NOT playing gren launch cutscene")
		if not has_ped_got_weapon(player_ped_id(), weapontype_grenadelauncher)
			give_weapon_to_ped(player_ped_id(), weapontype_grenadelauncher, kiGrenadeLauncherAmmo, false)
		else 
			// @SBA - ensure player gets just 3 rounds
			SET_GRENADE_LAUNCHER_AMMO() 
		endif 
		set_current_ped_weapon(player_ped_id(), weapontype_grenadelauncher, true)
		REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
		
		start_audio_scene("CHI_1_SHOOTOUT_GRENADE_LAUNCHER")
	ENDIF
	
	clear_ped_tasks_immediately(player_ped_id())
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(player_ped_id(), vTrevorBackDeckStartPos)//<<1408.0160, 3612.2808, 38.0056>>)
		SET_ENTITY_HEADING(player_ped_id(), 19.5667)
	ENDIF
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)


	buddy.pos = <<1406.4009, 3615.2783, 38.0056>>
	buddy.heading = 292.8793
	setup_buddy(buddy)
	add_ped_for_dialogue(scripted_speech[0], 5, buddy.ped, "cook")
	set_blocking_of_non_temporary_events(buddy.ped, false)
	set_ped_sphere_defensive_area(buddy.ped, <<1406.70, 3615.67, 38.01>>, 2.0)
	task_combat_hated_targets_around_ped(buddy.ped, 100.00)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1408.9497, 3612.3738, 38.0057>>)
	ENDIF
	
	END_REPLAY_SETUP()
	
	task_put_ped_directly_into_cover(player_ped_id(), vTrevorBackDeckStartPos, -1, true, 0, true, true, players_cover_point_2)
	force_ped_ai_and_animation_update(player_ped_id(), true)
	
	original_time = get_game_timer()
	// bypasses a timer in the gren launcher cutscene
	cutscene_trigger_time = get_game_timer() - 5000
		
	mission_flow = wave_2_system
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Wave 2 after grenade launcher cutscene")

	trigger_music_event("CHN1_AFTER_GRENADE_RT")
	
	IF g_replay.iReplayInt[PLAYER_RECEIVED_GRENADE_LAUNCHER] = HAS_GOT_GLAUNCHER
		end_cutscene_no_fade(false, TRUE, -77.0, -19.0, 500, false)
	ENDIF
	
	print_now("METH_GOD_0", default_god_text_time, 1)

endproc 

//wave 3
proc load_trip_skip_data_4()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(buddy.model)
	request_model(wave_0_enemy[0].model)
	request_model(wave_0_enemy[1].model)
	if not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED)
		request_model(get_npc_ped_model(char_ortega))
	endif 
	request_model(cheng.model)
	request_model(translator.model)
	REQUEST_MODEL(mnIceBox)
	REQUEST_MODEL(mnIceBoxProxy)
	request_model(get_player_veh_model(char_trevor))
	request_model(sadler)
	request_model(dubsta2)
	request_model(phoenix)
	request_model(explosive_barrels[0].model)
	request_model(bins[0].model)
	request_model(cover_box.model)
	request_model(get_weapontype_model(weapontype_carbinerifle))
	request_weapon_asset(weapontype_carbinerifle)
	
	request_vehicle_recording(001, "lkmethlab")
	request_vehicle_recording(002, "lkmethlab")
	request_vehicle_recording(003, "lkmethlab")
//	request_vehicle_recording(004, "lkmethlab")
//	request_vehicle_recording(005, "lkmethlab")
//	request_vehicle_recording(006, "lkmethlab")
	request_vehicle_recording(007, "lkmethlab")
	request_vehicle_recording(008, "lkmethlab")
	request_vehicle_recording(009, "lkmethlab")
	request_vehicle_recording(010, "lkmethlab")
	request_vehicle_recording(011, "lkmethlab")
	request_vehicle_recording(012, "lkmethlab")
//	request_vehicle_recording(013, "lkmethlab")
//	request_vehicle_recording(014, "lkmethlab")
//	request_vehicle_recording(015, "lkmethlab")
//	request_vehicle_recording(016, "lkmethlab")
	request_vehicle_recording(017, "lkmethlab")
	request_vehicle_recording(018, "lkmethlab")
	request_vehicle_recording(019, "lkmethlab")
	request_vehicle_recording(020, "lkmethlab")
	request_vehicle_recording(021, "lkmethlab")
//	request_vehicle_recording(030, "lkmethlab")
//	request_vehicle_recording(031, "lkmethlab")
//	request_vehicle_recording(032, "lkmethlab")
	
	REQUEST_WAYPOINT_RECORDING("methlab1")
//	REQUEST_WAYPOINT_RECORDING("methlab2")
	REQUEST_WAYPOINT_RECORDING("methlab3")
	REQUEST_WAYPOINT_RECORDING("methlab4")
	REQUEST_WAYPOINT_RECORDING("methlab5")
//	REQUEST_WAYPOINT_RECORDING("methlab6")
	REQUEST_WAYPOINT_RECORDING("methlab7")
//	REQUEST_WAYPOINT_RECORDING("methlab8")
//	REQUEST_WAYPOINT_RECORDING("methlab9")
	
	request_anim_dict("MissChinese1") 
	request_anim_dict("MissChinese1LeadInOutCHI_1_MCS_4") 
	
	meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
	pin_interior_in_memory(meth_interior)

	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(buddy.model)
	or not has_model_loaded(wave_0_enemy[0].model)
	or not has_model_loaded(wave_0_enemy[1].model)
	or (not GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ORTEGA_KILLED) and not has_model_loaded(get_npc_ped_model(char_ortega)))
	or not has_model_loaded(cheng.model)
	or not has_model_loaded(translator.model)
	or not has_model_loaded(mnIceBox)
	or not has_model_loaded(mnIceBoxProxy)
	or not has_model_loaded(get_player_veh_model(char_trevor))
	or not has_model_loaded(sadler)
	or not has_model_loaded(dubsta2)
	or not has_model_loaded(phoenix)
	or not has_model_loaded(explosive_barrels[0].model)
	or not has_model_loaded(bins[0].model)
	or not has_model_loaded(cover_box.model)
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_carbinerifle))
	or not has_weapon_asset_loaded(weapontype_carbinerifle)
	or not has_vehicle_recording_been_loaded(001, "lkmethlab")
	or not has_vehicle_recording_been_loaded(002, "lkmethlab")
	or not has_vehicle_recording_been_loaded(003, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(004, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(005, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(006, "lkmethlab")
	or not has_vehicle_recording_been_loaded(007, "lkmethlab")
	or not has_vehicle_recording_been_loaded(008, "lkmethlab")
	or not has_vehicle_recording_been_loaded(009, "lkmethlab")
	or not has_vehicle_recording_been_loaded(010, "lkmethlab")
	or not has_vehicle_recording_been_loaded(011, "lkmethlab")
	or not has_vehicle_recording_been_loaded(012, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(013, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(014, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(015, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(016, "lkmethlab")
	or not has_vehicle_recording_been_loaded(017, "lkmethlab")
	or not has_vehicle_recording_been_loaded(018, "lkmethlab")
	or not has_vehicle_recording_been_loaded(019, "lkmethlab")
	or not has_vehicle_recording_been_loaded(020, "lkmethlab")
	or not has_vehicle_recording_been_loaded(021, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(030, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(031, "lkmethlab")
//	or not has_vehicle_recording_been_loaded(032, "lkmethlab")
	or not get_is_waypoint_recording_loaded("methlab1")
//	or not get_is_waypoint_recording_loaded("methlab2")
	or not get_is_waypoint_recording_loaded("methlab3")
	or not get_is_waypoint_recording_loaded("methlab4")
	or not get_is_waypoint_recording_loaded("methlab5")
//	or not get_is_waypoint_recording_loaded("methlab6")
	or not get_is_waypoint_recording_loaded("methlab7")
//	or not get_is_waypoint_recording_loaded("methlab8")
//	or not get_is_waypoint_recording_loaded("methlab9")
	or not has_anim_dict_loaded("MissChinese1")
	or not has_anim_dict_loaded("MissChinese1LeadInOutCHI_1_MCS_4")
	or not is_interior_ready(meth_interior)
	
		wait(0)
		
	endwhile

	clear_player_wanted_level(player_id())
	//g_allowmaxwantedlevelcheck = false
	set_max_wanted_level(0)
	set_create_random_cops(false)
	disable_dispatch_services()
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, false)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, false)
	
	add_scenario_blocking_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>)
	add_scenario_blocking_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>)
	
	set_ped_paths_in_area(<<1587.6, 4259.42, -100.00>>, <<646.61, 3118.71, 100.00>>, false)
	
	set_roads_in_area(<<434.6, 3477.0, 36.3>>, <<1605.5, 4247.2, -100.00>>, FALSE)
	set_roads_in_area(<<1829.64, 3625.69, -100.00>>, <<1538.96, 3710.29, 100.00>>, false)
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, true, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, true, 0.0, 0.0, -1.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1392.93, 3599.47, 35.13>>, false, 0.0, 0.0, 1.0) 
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ml_door1, <<1395.37, 3600.36, 35.13>>, false, 0.0, 0.0, -1.0)
		
	// @SBA - Shutting outer doors to help with frame rate
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vBackDoorToRoof, FALSE, 0.0, 0.0, 0.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_doorext, vDoorToBackStairs, FALSE, 0.0, 0.0, 0.0)
	// @SBA - opening outer doors to help player follow Chef
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_ss_door04, vInteriorDoorStore, TRUE, 0.0, 0.0, -1.0)
	
	clear_area(<<1998.2826, 3058.0435, 46.0491>>, 10000.00, true)
	remove_decals_in_range(<<1998.2826, 3058.0435, 46.0491>>, 10000.00)
	
	setup_parked_cars()

	for i = 0 to count_of(bins) - 1
		bins[i].obj = create_object_no_offset(bins[i].model, bins[i].pos)
		SET_ENTITY_ROTATION(bins[i].obj, bins[i].rot)
		FREEZE_ENTITY_POSITION(bins[i].obj, true)
	endfor 
	
	for i = 0 to count_of(explosive_barrels) - 1
		explosive_barrels[i].obj = create_object_no_offset(explosive_barrels[i].model, explosive_barrels[i].pos)
		SET_ENTITY_ROTATION(explosive_barrels[i].obj, explosive_barrels[i].rot)
	endfor 
	
	cover_box.obj = create_object_no_offset(cover_box.model, cover_box.pos)
	freeze_entity_position(cover_box.obj, true)
	SET_ENTITY_ROTATION(cover_box.obj, cover_box.rot)
	
	setup_wave_3_enemies()
	
	// @SBA - trying this
//	CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<1409.9990, 3600.4973, 33.8676>>, 108.7487, false)
//	set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_locked)
//	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, false)
//	set_vehicle_engine_on(trevors_truck.veh, false, true)
	RECREATE_PLAYERS_VEHICLE(vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
	
	clear_ped_tasks_immediately(player_ped_id())
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(player_ped_id(), <<1391.2151, 3607.2073, 33.9809>>)
		SET_ENTITY_HEADING(player_ped_id(), 109.0587) 
	ENDIF
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)

	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, false, false)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_CARBINERIFLE) < 500
			set_ped_ammo(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500)
		endif 
	endif
	
	if not has_ped_got_weapon(player_ped_id(), weapontype_grenadelauncher)
		give_weapon_to_ped(player_ped_id(), weapontype_grenadelauncher, kiGrenadeLauncherAmmo, false, FALSE)
	else 
		// @SBA - ensure player gets just 3 rounds
		SET_GRENADE_LAUNCHER_AMMO()
	endif 
	
	// Since player is in cover inside, let's make sure he has the default rifle
	//setup_weapon_for_ped(player_ped_id(), WEAPONTYPE_CARBINERIFLE, 500, true, true, false)
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
	
	buddy.pos = vChefCoverPointLiquorStore	
	buddy.heading = 197.22
	setup_buddy(buddy)
	add_ped_for_dialogue(scripted_speech[0], 5, buddy.ped, "cook")
	task_put_ped_directly_into_cover(buddy.ped, vChefCoverPointLiquorStore, 8000)
	force_ped_ai_and_animation_update(buddy.ped)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1391.4332, 3607.9443, 33.9809>>)
	ENDIF

	END_REPLAY_SETUP()
	
	task_put_ped_directly_into_cover(player_ped_id(), vTrevorLiquorStartPos, -1, true, 0, true, false)
	force_ped_ai_and_animation_update(player_ped_id(), true)
	
	wave_3_master_flow_system_status = 1
	wave_3_dialogue_system_status = 5
	wave_3_buddy_ai_system_status = 4
	
	end_cutscene_no_fade(false, TRUE, 90, -12, 500, false) //-45
	
	print_now("METH_GOD_0", default_god_text_time, 1)
	
	dialogue_time_2 = get_game_timer()
	buddy_time = get_game_timer()
	
	trigger_music_event("CHN1_WAVE_3_RT")
	
	start_audio_scene("CHI_1_SHOOTOUT_04")
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "Wave 3", true)

	REMOVE_MODEL_HIDE(vIceBoxPos, 2.0, prop_ice_box_01)
	
	mission_flow = wave_3_system
	
endproc 

// Outro cutscene
proc load_trip_skip_data_5()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(get_player_veh_model(char_trevor))
	request_model(cheng.model)
	request_model(translator.model)
	request_model(buddy.model)
	
	meth_interior = get_interior_at_coords_with_type(<<1392.5117, 3603.1851, 38.5>>, "v_methlab") 
	pin_interior_in_memory(meth_interior)
	
	END_REPLAY_SETUP()

	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(get_player_veh_model(char_trevor))
	or not has_model_loaded(cheng.model)
	or not has_model_loaded(translator.model)
	or not has_model_loaded(buddy.model)
	or not is_interior_ready(meth_interior)
	
		wait(0)
		
	endwhile 
	
	
	clear_ped_tasks_immediately(player_ped_id())
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(player_ped_id(), <<1384.9, 3598.2172, 33.8896>>)
		set_entity_heading(player_ped_id(), 282.5231)
	ENDIF
	
	buddy.pos = <<1378.2903, 3591.9409, 33.8590>>
	setup_buddy(buddy, false)
	
	cheng.pos = <<1380.3240, 3595.5015, 33.8913>>
	setup_buddy(cheng, FALSE)
	
	translator.pos = <<1380.7184, 3599.5544, 33.8578>>
	setup_buddy(translator, FALSE)
	
	SET_ICY_VERSIONS_OF_PEDS()
	
	wait(0) //must wait a frame to ensure the outfit variaitons have been applied before checking the streaming requests for the player
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(cheng.ped)
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(translator.ped)
	
		wait(0)
		
	endwhile
	
	
	// @SBA - trying this
//	CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, <<1409.9990, 3600.4973, 33.8676>>, 108.7487, false)
//	set_vehicle_doors_locked(trevors_truck.veh, vehiclelock_locked)
//	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(trevors_truck.veh, false)
//	set_vehicle_engine_on(trevors_truck.veh, false, true)
	RECREATE_PLAYERS_VEHICLE(vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1386.4717, 3594.1172, 33.8896>>)
	ENDIF
	
	
	request_cutscene("chi_1_mcs_4_concat")
	
	while not has_cutscene_loaded()
	
		CPRINTLN(DEBUG_MISSION, "Waiting for outro assets")
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			CPRINTLN(DEBUG_MISSION, "Requesting outro component variations")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("trevor", PLAYER_PED_ID())
			
			if not is_ped_injured(buddy.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cook", buddy.ped)
			endif 
			
			if not is_ped_injured(cheng.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tao", cheng.ped)
			endif 
			
			if not is_ped_injured(translator.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Taos_Translator", translator.ped)
			endif 
			
			SET_CUTSCENE_PED_PROP_VARIATION("Cook", ANCHOR_EYES, 0, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Tao", ANCHOR_EYES, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Taos_Translator", ANCHOR_EYES, 0)
		ENDIF	
		
		wait(0)
		
	endwhile
	
	
	
	start_new_cutscene_no_fade(false, FALSE, TRUE, TRUE)
	CLEAR_PRINTS()
	SET_LABEL_AS_TRIGGERED("METH_GOD_14", FALSE)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	// don't update Chef any more
	allow_buddy_ai_system = FALSE
	

	if not is_ped_injured(cheng.ped)
		register_entity_for_cutscene(cheng.ped, "Tao", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		CPRINTLN(DEBUG_MISSION,"cheng registered for outro")
	endif 
	
	IF not is_ped_injured(translator.ped)
		register_entity_for_cutscene(translator.ped, "Taos_Translator", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		CPRINTLN(DEBUG_MISSION,"translator registered for outro")
	ENDIF
	
	IF not is_ped_injured(buddy.ped)
		register_entity_for_cutscene(buddy.ped, "Cook", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		CPRINTLN(DEBUG_MISSION,"Chef registered for outro")
	ENDIF
	
	register_entity_for_cutscene(PLAYER_PED_ID(), "trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
	REGISTER_ENTITY_FOR_CUTSCENE(NULL, "getaway_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
	
	
	start_cutscene()
	
	trigger_music_event("CHN1_FINAL_CS")

	REDUCE_AMBIENT_MODELS(TRUE)
	
	mission_passed_mocap_status = 5 //@SBA OLD = 2
	bOutroShitSkip = TRUE
	
	mission_flow = mission_passed_mocap

endproc 

proc load_outside_methlab_skip()

	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(cheng.model)
	request_model(translator.model)
	request_model(buddy.model)
	request_model(trevors_truck.model)
	set_vehicle_model_is_suppressed(trevors_truck.model, true)
	request_model(get_weapontype_model(WEAPONTYPE_carbineRIFLE))
	PREFETCH_SRL("CHI1_Methlab_Arrival")
	
	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(cheng.model)
	or not has_model_loaded(translator.model)
	or not has_model_loaded(buddy.model)
	or not has_model_loaded(trevors_truck.model)
	or not has_model_loaded(get_weapontype_model(WEAPONTYPE_carbinerifle))
	or not IS_SRL_LOADED()
	
		wait(0)
		
		CPRINTLN(DEBUG_MISSION,"Loading for load_outside_methlab_skip")
		
	endwhile
	
//	// @SBA - trying this to remake vehicle
//	RECREATE_PLAYERS_VEHICLE( vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
//	
//	if not is_players_last_vehicle_present_and_acceptable(trevors_truck.veh, vTrevorsVehicleMethLabLoc, 2, <<2008.9219, 3054.0513, 46.0528>>, 326.3415)
//		CPRINTLN(DEBUG_MISSION, "Player needs an appropriate vehicle.  Making one.")
//		WHILE NOT CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead, false)
//			WAIT(0)
//			CPRINTLN(DEBUG_MISSION, "Waiting on new vehicle...")
//		ENDWHILE
//	ENDIF


	CREATE_PLAYER_VEHICLE(trevors_truck.veh, CHAR_TREVOR, vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead, false)

	SET_INITIAL_PLAYER_STATION("OFF")
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		set_ped_into_vehicle(player_ped_id(), trevors_truck.veh)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(cheng.ped)
		cheng.ped = create_ped_inside_vehicle(trevors_truck.veh, pedtype_mission, cheng.model, vs_front_right)
	ELSE
		SET_PED_INTO_VEHICLE(cheng.ped, TREVORS_TRUCK.VEH, vs_front_right)
	ENDIF
	add_ped_for_dialogue(scripted_speech[0], 4, cheng.ped, "cheng")
	//RETAIN_ENTITY_IN_INTERIOR(buddy.ped, get_interior_at_coords(buddy.pos))
	setup_buddy_attributes(cheng)
	setup_relationship_contact(cheng.ped, true)
//*****TEMP REMOVE
	//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(cheng.ped, true)
//*****

	IF NOT DOES_ENTITY_EXIST(translator.ped)
		translator.ped = create_ped_inside_vehicle(trevors_truck.veh, pedtype_mission, translator.model, vs_back_right)
	ELSE
		SET_PED_INTO_VEHICLE(translator.ped, TREVORS_TRUCK.VEH, vs_back_right)
	ENDIF
	//RETAIN_ENTITY_IN_INTERIOR(translator.ped, get_interior_at_coords(translator.pos))
	add_ped_for_dialogue(scripted_speech[0], 3, translator.ped, "translator")
	setup_buddy_attributes(translator)
	setup_relationship_contact(translator.ped, true)
	
	SET_START_VERSIONS_OF_BUDDIES()

	HANDLE_SETUP_INITIAL_CHEF()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		load_scene(<<1403.1777, 3597.2070, 33.7417>>)
	ENDIF
	
	END_REPLAY_SETUP(TREVORS_TRUCK.VEH, VS_DRIVER, FALSE)
	if is_vehicle_driveable(trevors_truck.veh)
		SET_VEH_RADIO_STATION(trevors_truck.veh,"OFF")
	endif 

	bPlayerInVehicle = TRUE
	
	end_cutscene()
	request_mocap_data_for_CHI_1_MCS_1()
	
	kill_any_conversation()

	clear_mission_locate_stuff(locates_data)
	
	SET_DOOR_STATE(DOORNAME_METHLAB_F_L, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_METHLAB_F_R, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_METHLAB_R, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	
	bBarPatronsCreated = TRUE
	cleanup_bar()

	// start lead-in cam
	camera_a = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<1413.1581, 3586.2705, 35.3191>>, <<14.4926, -0.0000, 51.7688>>, 40.0, TRUE)
	camera_b = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<1415.4323, 3585.3635, 35.4866>>, <<5.5924, -0.0000, 49.1407>>, 40.0)

	HANDLE_LEAD_IN_SHOT_OF_ARRIVAL()
	mission_flow = get_to_meth_lab
	get_to_meth_lab_status = 1
	
endproc

proc mission_setup()

	#IF IS_DEBUG_BUILD
		load_lk_widgets()
		SET_LOCATES_HEADER_WIDGET_GROUP(chinese_1_widget_group)
	#endif 
	
	if is_player_playing(player_id())
		clear_player_wanted_level(player_id())
	endif
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	set_wanted_level_multiplier(0.8)
	
	SUPPRESS_EMERGENCY_CALLS()
	
	//INFORM_MISSION_STATS_OF_MISSION_START_chinese_one()
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(),CHI1_UNMARKED)
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(player_ped_id()) 
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(CHI1_SPECIAL_ABILITY_TIME) 
	//INFORM_MISSION_STATS_OF_INCREMENT(CHI1_GRENADE_LAUNCHER_KILLS)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1357.68, 3556.56, -100.00>>, <<1436.65, 3672.06, 100.00>>, false)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1335.97, 3598.28, -100.00>>, <<1368.15, 3575.16, 100.00>>, false)
	
	add_scenario_blocking_area(<<1349.96, 3547.31, 100.00>>, <<1423.99, 3658.75, -100.00>>)
	SET_PED_PATHS_IN_AREA(<<1349.96, 3547.31, 100.00>>, <<1423.99, 3658.75, -100.00>>, false)
	
	load_text_and_dialogue()

	add_relationship_groups()
	
	
	// set weather
	SET_WEATHER_TYPE_PERSIST("extrasunny")
	
	
	// @SBA - adjusting start position slightly. OLD VALUE = <<1389.63, 3600.41, 37.94>>
	players_cover_point = add_cover_point(vTrevorWindowStartPos, 112.4765, COVUSE_WALLTOBOTH, COVHEIGHT_low, COVARC_180)  //COVUSE_WALLTOBOTH covuse_walltoleft
	players_cover_point_2 = add_cover_point(vTrevorBackDeckStartPos, 290.00, COVUSE_WALLTOBOTH, COVHEIGHT_low, COVARC_180) // covuse_walltoright
		
	buddy_cover_point[0] = add_cover_point(<<1387.12, 3607.50, 37.94>>, 118.00, COVUSE_WALLTOBOTH, COVHEIGHT_low, COVARC_120)
	// @SBA - trying Chef in a different postion at the top of the stairs
	//buddy_cover_point[1] = add_cover_point(<<1388.84, 3619.31, 37.93>>, 37.00, covuse_walltoright, COVHEIGHT_low, COVARC_180)
	buddy_cover_point[1] = add_cover_point(vChefCoverPointBackStairs, 105.00, covuse_walltoleft, COVHEIGHT_low, COVARC_180)
	
	wave_3_0_cover_point = wave_3_0_cover_point 
	wave_3_1_cover_point = wave_3_1_cover_point
	
	//wave_3_0_cover_point = add_cover_point(<<1392.83228, 3598.94385, 34.02530>>, 19.00, covuse_walltoleft, COVHEIGHT_TOOHIGH, COVARC_180)  
	wave_3_0_cover_point = add_cover_point(<<1392.67334, 3598.85669, 34.02655>>, 19.00, covuse_walltoleft, COVHEIGHT_TOOHIGH, COVARC_180)  
	//wave_3_1_cover_point = add_cover_point(<<1395.67151, 3599.85791, 34.00392>>, 19.9438, covuse_walltoright, COVHEIGHT_TOOHIGH, COVARC_180)
	wave_3_1_cover_point = add_cover_point(<<1395.83069, 3599.75, 34.00454>>, 19.9438, covuse_walltoright, COVHEIGHT_TOOHIGH, COVARC_180)
	
	cpWaterHeaterCover = ADD_COVER_POINT((<<1397.4, 3602.6, 37.94>>), 108.0, COVUSE_WALLTOLEFT, COVHEIGHT_low, COVARC_180)
	
	cover_point_back_roof_AC = ADD_COVER_POINT((<<1404.8580, 3611.8057, 38.0058>>), 108.0, COVUSE_WALLTORIGHT, COVHEIGHT_low, COVARC_120, TRUE)
	
	add_ped_for_dialogue(scripted_speech[0], 2, player_ped_id(), "trevor")
	
	initialise_mission_variables()
	
	// Back stairs
	buddy_cover_point_data[0].cover_point = buddy_cover_point[1]
	buddy_cover_point_data[1].cover_point = add_cover_point(buddy_cover_point_data[1].pos, buddy_cover_point_data[1].heading, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_180)

	// Back roof
	buddy_cover_point_data[2].cover_point = add_cover_point(buddy_cover_point_data[2].pos, buddy_cover_point_data[2].heading, covuse_walltoleft, COVHEIGHT_LOW, COVARC_180)  //covheight_toohigh
	buddy_cover_point_data[3].cover_point = add_cover_point(buddy_cover_point_data[3].pos, buddy_cover_point_data[3].heading, covuse_walltoleft, covheight_low, COVARC_180)  

	// liquor store
	buddy_cover_point_data[4].cover_point = add_cover_point(buddy_cover_point_data[4].pos, buddy_cover_point_data[4].heading, COVUSE_WALLTOBOTH, COVHEIGHT_low, COVARC_180) 
	buddy_cover_point_data[5].cover_point = add_cover_point(buddy_cover_point_data[5].pos, buddy_cover_point_data[5].heading, COVUSE_WALLTOBOTH, COVHEIGHT_low, COVARC_180) 
	

	// @SBA - Navmesh blocking for upstairs doors, so rushers use the large doorway behind the player
	iNavBlockingUpstairsDoorway = ADD_NAVMESH_BLOCKING_OBJECT((<<1394.03613, 3608.93579, 37.94191>>), (<<0.75, 0.5, 0.5>>), -1.2)
	iNavBlockingUpstairsDoor = ADD_NAVMESH_BLOCKING_OBJECT((<<1392.08325, 3611.16699, 37.94191>>), (<<0.6, 0.4, 0.5>>), 0.36)	
	iNavBlockingBackYardTrailer = ADD_NAVMESH_BLOCKING_OBJECT((<<1432.98425, 3626.76880, 33.93124>>), (<<2.5, 3.75, 1.0>>), 1.95)	
	
	IF (Is_Replay_In_Progress())
	
		INT iReplayStage = Get_Replay_Mid_Mission_Stage()
	
		IF g_bShitskipAccepted
			iReplayStage++ // player is skipping this stage
		ENDIF
	
		SWITCH iReplayStage
		
			case 0
				//outside hicks bar
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 1")
				START_REPLAY_SETUP((<<1998.04980, 3055.90698, 46.05226>>),328.0059)
				load_trip_skip_data_0()	
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 1 loaded")
			break 
			
			case 1
			
				IF g_bShitskipAccepted
					// arrival at meth lab, prior to wave 0
					CPRINTLN(DEBUG_MISSION, "Replay checkpoint 2 (shit skip)")
					START_REPLAY_SETUP(<<1423.0, 3603.0, 33.8866>>, 108.7487, FALSE)
					load_outside_methlab_skip()
				ELSE
					//wave 0
					CPRINTLN(DEBUG_MISSION, "Replay checkpoint 2")
					START_REPLAY_SETUP(vTrevorWindowStartPos, 21.5294, FALSE)
					load_trip_skip_data_1()
				ENDIF
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 2 loaded")
			
			break 
			
			case 2
				//wave 0 outside enemies
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 3")
				START_REPLAY_SETUP((<<1390.8401, 3601.1016, 37.9419>>), 323.8041, FALSE)
				load_trip_skip_data_2()
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 3 loaded")
				
			break 
			
			case 3
				//wave 2 after grenade launcher cutscene
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 4")
				START_REPLAY_SETUP(vTrevorBackDeckStartPos, 19.5667, FALSE)
				load_trip_skip_data_3()
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 4 loaded")
				
			break 
			
			case 4
				//wave 3
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 5")
				START_REPLAY_SETUP(vTrevorLiquorStartPos, 113.4341, FALSE)
				load_trip_skip_data_4()
				CPRINTLN(DEBUG_MISSION, "Replay checkpoint 5 loaded")
			
			break
			
			case 5
				// outro
				CPRINTLN(DEBUG_MISSION, "Replay (skip to) outro")
				START_REPLAY_SETUP((<<1384.9, 3598.2172, 33.8896>>), 282.5231, FALSE)  // Don't change vector! Pputs player in place to start cutscene.
				load_trip_skip_data_5()
				CPRINTLN(DEBUG_MISSION, "Replay outro loaded")
			
			break
			
			DEFAULT
				SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected!")
			BREAK
			
		endswitch 
	
	// not a replay
	else 
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Mission start")
		
	endif 

endproc 

proc fbi4_load_stage_selector_assets()

	initialise_mission_variables()
	
	switch launch_mission_stage_menu_status
	
		case 0
		
			load_trip_skip_data_0()
		
		break 
		
		case 1
		
			load_outside_methlab_skip()

		break 
	
		case 2
		
			load_trip_skip_data_1()
		
		break 
		
		case 3
		
			load_trip_skip_data_2()
		
		break 
		
		case 4
		
			load_trip_skip_data_3()
		
		break 
		
		case 5
		
			load_trip_skip_data_4()
		
		break 
		
	endswitch 

endproc 

proc check_mission_vehicles_for_chi_1_mcs_4_mocap()

	// @SBA - using a trigger box for all
	TRIGGER_BOX	tbOutroCutscene
	TRIGGER_BOX	tbOutroCutsceneRoad
	tbOutroCutscene = CREATE_TRIGGER_BOX(<<1396.244995,3594.819824,33.912655>>, <<1380.003784,3590.047852,37.925877>>, 22.000000)
	tbOutroCutsceneRoad = CREATE_TRIGGER_BOX(<<1345.245605,3568.100342,33.996071>>, <<1402.66, 3590.93,35.993843>>, 11.000000)

	for i = 0 to count_of(parked_cars) - 1
		if does_entity_exist(parked_cars[i])
			if is_entity_in_angled_area(parked_cars[i], <<1393.118, 3598.737, 33.986>>, <<1395.846, 3599.719, 36.986>>, 2.500, false, false) 
			OR IS_ENTITY_IN_TRIGGER_BOX(parked_cars[i], tbOutroCutscene, FALSE)
			OR IS_ENTITY_IN_TRIGGER_BOX(parked_cars[i], tbOutroCutsceneRoad, FALSE)
				delete_vehicle(parked_cars[i])
			endif 
		endif 
	endfor

	for i = 0 to count_of(wave_0_enemy_vehicle) - 1
		if does_entity_exist(wave_0_enemy_vehicle[i].veh)
			IF IS_ENTITY_IN_TRIGGER_BOX(wave_0_enemy_vehicle[i].veh, tbOutroCutscene, FALSE)
			OR IS_ENTITY_IN_TRIGGER_BOX(wave_0_enemy_vehicle[i].veh, tbOutroCutsceneRoad, FALSE)
				delete_vehicle(wave_0_enemy_vehicle[i].veh)
			endif 
		endif 
	endfor 
	
	for i = 0 to count_of(wave_2_enemy_vehicle) - 1
		if does_entity_exist(wave_2_enemy_vehicle[i].veh)
			IF IS_ENTITY_IN_TRIGGER_BOX(wave_2_enemy_vehicle[i].veh, tbOutroCutscene, FALSE)
			OR IS_ENTITY_IN_TRIGGER_BOX(wave_2_enemy_vehicle[i].veh, tbOutroCutsceneRoad, FALSE)
				delete_vehicle(wave_2_enemy_vehicle[i].veh)
			endif 
		endif 
	endfor 
	
	for i = 0 to count_of(wave_3_enemy_vehicle) - 1
		if does_entity_exist(wave_3_enemy_vehicle[i].veh)
			IF IS_ENTITY_IN_TRIGGER_BOX(wave_3_enemy_vehicle[i].veh, tbOutroCutscene, FALSE)
			OR IS_ENTITY_IN_TRIGGER_BOX(wave_3_enemy_vehicle[i].veh, tbOutroCutsceneRoad, FALSE)
				// @SBA - just move car 0, don't delete (currently that's the only wave 3 veh)
				IF i = 0
					// to thwart asserts (don't actually care if it's dead)
					IS_ENTITY_DEAD(wave_3_enemy_vehicle[i].veh)
					SET_ENTITY_COORDS(wave_3_enemy_vehicle[i].veh, <<1397.67297, 3594.00537, 33.93133>>, false)
					SET_ENTITY_HEADING(wave_3_enemy_vehicle[i].veh, -1.7487)
				ELSE
					delete_vehicle(wave_3_enemy_vehicle[i].veh)
				ENDIF
			endif 
		endif 
	endfor 
	
	for i = 0 to count_of(ambient_enemy_vehicle) - 1
		IF does_entity_exist(ambient_enemy_vehicle[i].veh)
			IF IS_ENTITY_IN_TRIGGER_BOX(ambient_enemy_vehicle[i].veh, tbOutroCutscene, FALSE)
			OR IS_ENTITY_IN_TRIGGER_BOX(ambient_enemy_vehicle[i].veh, tbOutroCutsceneRoad, FALSE)
			OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(ambient_enemy_vehicle[i].veh, FALSE), vIceBoxPos) > 75
				DELETE_VEHICLE(ambient_enemy_vehicle[i].veh)
			endif 
		ENDIF
	endfor 
	
	IF NOT DOES_ENTITY_EXIST(trevors_truck.veh)
		CPRINTLN(DEBUG_MISSION, "TREVOR'S VEHICLE DOES NOT EXIST")
	ELSE
		CPRINTLN(DEBUG_MISSION, "TREVOR'S VEHICLE DOES EXIST")
		IF IS_ENTITY_IN_TRIGGER_BOX(trevors_truck.veh, tbOutroCutscene, FALSE)
		OR IS_ENTITY_IN_TRIGGER_BOX(trevors_truck.veh, tbOutroCutsceneRoad, FALSE)
			// to thwart asserts (don't actually care if it's dead)
			IS_ENTITY_DEAD(trevors_truck.veh)
			SET_ENTITY_COORDS(trevors_truck.veh, vTrevorsVehicleMethLabLoc, false)
			SET_ENTITY_HEADING(trevors_truck.veh, fTrevorsVehicleMethLabHead)
		ENDIF
	ENDIF
	
	SET_MISSION_VEHICLE_GEN_VEHICLE(trevors_truck.veh, vTrevorsVehicleMethLabLoc, fTrevorsVehicleMethLabHead)
	
endproc 

proc chinese_mission_passed_mocap()

	// @SBA - kill those asserts
	IF IS_PED_INJURED(cheng.ped)
	OR IS_PED_INJURED(translator.ped)
		//CPRINTLN(DEBUG_MISSION, "chinese_mission_passed_mocap: Cheng or the Translator is NOT ok!")
	ENDIF
	
	// @SBA - Disable some player controls during lead in
	// But let him die in explosion
	IF mission_passed_mocap_status > 0
	AND mission_passed_mocap_status < 5
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
		KILL_INVINCIBLE_PLAYER_IF_IN_EXPLOSION()
		HANDLE_HINT_CAM_FOR_LEAD_IN()
		HANDLE_VEHICLE_TOO_NEAR_ICE_BOX()
		// just in case
		wave_3_buddy_ai_system()
	ENDIF
	
	// continue to update fleeing enemies (also updates their blips)
	ambient_enemy_system()

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		CPRINTLN(DEBUG_MISSION, "Requesting outro component variations")
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tao", cheng.ped)
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Taos_Translator", translator.ped)
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("trevor", PLAYER_PED_ID())
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cook", buddy.ped)
		SET_CUTSCENE_PED_PROP_VARIATION("Cook", ANCHOR_EYES, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Tao", ANCHOR_EYES, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Taos_Translator", ANCHOR_EYES, 0)
	ENDIF	
	
	if mission_passed_mocap_status < 6
		disable_on_foot_first_person_view_this_update() //don't process for mocap playing and exit
	endif 
	
	switch mission_passed_mocap_status 
	
		case 0
		
			// @SBA - start the timer, reset label
			CANCEL_TIMER(tmrIceBoxBlendin)
			START_TIMER_NOW(tmrIceBoxBlendin)
			SET_LABEL_AS_TRIGGERED("chin_M4_LI1", FALSE)
			
			// get rid of blip
			if does_blip_exist(ice_box_blip)
				remove_blip(ice_box_blip)
			endif 

			request_cutscene("chi_1_mcs_4_concat")
			
			BLOCK_PLAYER_FOR_LEAD_IN(FALSE)
			
			// collision proxy on
			IF DOES_ENTITY_EXIST(oiIceBoxProxy)
				SET_ENTITY_COLLISION(oiIceBoxProxy, TRUE)
			ENDIF

			STOP_FIRE_IN_RANGE(vIceBoxPos, 5.0)
			
			mission_passed_mocap_status++
		
		break 
		
		// @SBA - adding this case
		CASE 1
				
			IF TIMER_DO_ONCE_WHEN_READY(tmrIceBoxBlendin, 1.200)
			
				remove_decals_in_range(<<1388.4, 3598.8, 35.1>>, 0.5) //right door
			
				/* @SBA - START SYNCHRONIZED SCENE */
				cutscene_pos = <<1388.108, 3599.101, 33.89>>
				cutscene_rot = <<0.0, 0.0, 110.480>>
				// @SBA - get Cheng out of the ice box
				CHENG_CUTSCENE_INDEX = CREATE_SYNCHRONIZED_SCENE ( cutscene_pos, cutscene_rot)
//				CLEAR_PED_TASKS(cheng.ped)
				CPRINTLN(DEBUG_MISSION, "Playing Cheng anim: CHI_1_MCS_4_TAO_ACTION")
				TASK_SYNCHRONIZED_SCENE(cheng.ped, CHENG_CUTSCENE_INDEX, "MissChinese1LeadInOutCHI_1_MCS_4", "CHI_1_MCS_4_TAO_ACTION", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)

				// @SBA - animate the door
				IF DOES_ENTITY_EXIST(oiIceBox)
					CPRINTLN(DEBUG_MISSION, "Playing Icebox anim: CHI_1_MCS_4_TAO_ACTION_FREEZER")
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiIceBox, CHENG_CUTSCENE_INDEX, "CHI_1_MCS_4_TAO_ACTION_FREEZER", "MissChinese1LeadInOutCHI_1_MCS_4", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					SET_ENTITY_COLLISION(oiIceBox, TRUE)
				ENDIF	
			
				// pesky Molotovs
				STOP_FIRE_IN_RANGE(vIceBoxPos, 5.0)
				
				prepare_music_event("CHN1_FINAL_CS")
				
				mission_passed_mocap_status++
			ENDIF
			
		BREAK
		
		// @SBA - adding this case
		CASE 2
			// @SBA - play Cheng's first line as he exits the box
			IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_M4_LI1_1")
				IF GET_SYNCHRONIZED_SCENE_PHASE(CHENG_CUTSCENE_INDEX) >= 0.12
					ADD_PED_FOR_DIALOGUE(scripted_speech[0], 5, cheng.ped, "cheng")
					//IF CREATE_CONVERSATION(scripted_speech[0], "methaud", "chin_M4_LI1", CONV_PRIORITY_MEDIUM)
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech[0], "methaud", "chin_M4_LI1", "chin_M4_LI1_1", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("chin_M4_LI1_1", TRUE)
					ENDIF
				ENDIF
			ELSE
				// play second line from conv if player is not in trigger
				IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_M4_LI1_2")
					IF NOT IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxInnerVol)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech[0], "methaud", "chin_M4_LI1", "chin_M4_LI1_2", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("chin_M4_LI1_2", TRUE)
							ENDIF
						ENDIF
					ELSE
						// set as triggered if the player is right there
						SET_LABEL_AS_TRIGGERED("chin_M4_LI1_2", TRUE)
					ENDIF
				ENDIF
			ENDIF
			// @SBA - while Cheng is getting out of the box, get the translator out
			IF NOT bStartTranslatorEmergeBox
				IF GET_SYNCHRONIZED_SCENE_PHASE(CHENG_CUTSCENE_INDEX) >= 0.687
				
					remove_decals_in_range(<<1388.1, 3599.6, 35.1>>, 0.5) //left door
				
					//CLEAR_PED_TASKS(translator.ped)
					CPRINTLN(DEBUG_MISSION, "Playing translator anim: CHI_1_MCS_4_TRANSLATOR_ACTION")
					TRANSLATOR_CUTSCENE_INDEX = CREATE_SYNCHRONIZED_SCENE ( cutscene_pos, cutscene_rot)
					TASK_SYNCHRONIZED_SCENE(translator.ped, TRANSLATOR_CUTSCENE_INDEX, "MissChinese1LeadInOutCHI_1_MCS_4", "CHI_1_MCS_4_TRANSLATOR_ACTION", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					// @SBA - door
					IF DOES_ENTITY_EXIST(oiIceBox)
						CPRINTLN(DEBUG_MISSION, "Playing Icebox anim: CHI_1_MCS_4_TRANSLATOR_ACTION_FREEZER")
						PLAY_SYNCHRONIZED_ENTITY_ANIM(oiIceBox, TRANSLATOR_CUTSCENE_INDEX, "CHI_1_MCS_4_TRANSLATOR_ACTION_FREEZER", "MissChinese1LeadInOutCHI_1_MCS_4", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						SET_ENTITY_COLLISION(oiIceBox, TRUE)
					ENDIF
					bStartTranslatorEmergeBox = TRUE
				ENDIF
			ENDIF
			
			// @SBA - when Cheng is out of the box, start idle loop
			IF GET_SYNCHRONIZED_SCENE_PHASE(CHENG_CUTSCENE_INDEX) >= 0.99
				//CLEAR_PED_TASKS(cheng.ped)
				CPRINTLN(DEBUG_MISSION, "Playing Cheng's anim: CHI_1_MCS_4_TAO_IDLE_2")
				CHENG_CUTSCENE_INDEX = CREATE_SYNCHRONIZED_SCENE ( cutscene_pos, cutscene_rot)
				SET_SYNCHRONIZED_SCENE_LOOPED(CHENG_CUTSCENE_INDEX, TRUE)
				TASK_SYNCHRONIZED_SCENE(cheng.ped, CHENG_CUTSCENE_INDEX, "MissChinese1LeadInOutCHI_1_MCS_4", "CHI_1_MCS_4_TAO_IDLE_2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				// @SBA - don't need to do door here, previous anim still applies

				mission_passed_mocap_status++
			ENDIF
			
		BREAK
		
		// @SBA - adding this case
		CASE 3
			IF HAS_LABEL_BEEN_TRIGGERED("chin_M4_LI1_1")
				// play second line from conv if player is not in trigger
				IF NOT HAS_LABEL_BEEN_TRIGGERED("chin_M4_LI1_2")
					IF NOT IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxInnerVol)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech[0], "methaud", "chin_M4_LI1", "chin_M4_LI1_2", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("chin_M4_LI1_2", TRUE)
							ENDIF
						ENDIF
					ELSE
						// set as triggered if the player is right there
						SET_LABEL_AS_TRIGGERED("chin_M4_LI1_2", TRUE)
					ENDIF
				ENDIF
			ENDIF
		
			// @SBA - Translator out of box? put into idle loop.
			IF GET_SYNCHRONIZED_SCENE_PHASE(TRANSLATOR_CUTSCENE_INDEX) >= 0.99
			
				CPRINTLN(DEBUG_MISSION, "Playing trnaslator's anim: CHI_1_MCS_4_TRANSLATOR_IDLE_2")
				TRANSLATOR_CUTSCENE_INDEX = CREATE_SYNCHRONIZED_SCENE ( cutscene_pos, cutscene_rot)
				SET_SYNCHRONIZED_SCENE_LOOPED(TRANSLATOR_CUTSCENE_INDEX, TRUE)
				TASK_SYNCHRONIZED_SCENE(translator.ped, TRANSLATOR_CUTSCENE_INDEX, "MissChinese1LeadInOutCHI_1_MCS_4", "CHI_1_MCS_4_TRANSLATOR_IDLE_2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				// @SBA - door
				IF DOES_ENTITY_EXIST(oiIceBox)
					CPRINTLN(DEBUG_MISSION, "Playing Icebox anim: CHI_1_MCS_4_TRANSLATOR_IDLE_2_FREEZER")
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiIceBox, TRANSLATOR_CUTSCENE_INDEX, "CHI_1_MCS_4_TRANSLATOR_IDLE_2_FREEZER", "MissChinese1LeadInOutCHI_1_MCS_4", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_ENTITY_COLLISION(oiIceBox, TRUE)
				ENDIF
				
				CANCEL_TIMER(tmrIceBoxBlendin)
				mission_passed_mocap_status++
			// Go to cutscene earlier if player is already in position
				
			ELIF IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxInnerVol)
				//IF GET_SYNCHRONIZED_SCENE_PHASE(TRANSLATOR_CUTSCENE_INDEX) >= 0.8
				
				IF HAS_ANIM_EVENT_FIRED(translator.ped, get_hash_key("START_CUTSCENE"))
				//AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CPRINTLN(DEBUG_MISSION, "STARTING OUTRO CUTSENE EARLY")
					CANCEL_TIMER(tmrIceBoxBlendin)
					mission_passed_mocap_status++
				ENDIF
			ENDIF
		BREAK
		
		case 4
			
			// @SBA - Is player not around?  exit, otherwise start cutscene
			// Also check player still has control (if not, start cutscene)
			IF NOT IS_PLAYER_IN_TRIGGER_BOX(tbIceBoxInnerVol)
			AND IS_PLAYER_CONTROL_ON(PLAYER_ID())

				// show objective, if you can
				IF NOT HAS_LABEL_BEEN_TRIGGERED("METH_GOD_14")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locates_data)
						PRINT_NOW("METH_GOD_14", default_god_text_time, 1)
						SET_LABEL_AS_TRIGGERED("METH_GOD_14", TRUE)
						CANCEL_TIMER(tmrIceBoxReminder)
					ENDIF
				ENDIF
				// blip them (or just one since they're next to each other)
				IF NOT DOES_BLIP_EXIST(cheng.blip)
					cheng.blip = CREATE_BLIP_FOR_PED(cheng.ped)
				ENDIF
				EXIT
			ENDIF
			
			// Check conversations are done
			if not IS_SPECIAL_ABILITY_ACTIVE(player_id()) //fix for bug 1854979
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					EXIT
				enDIF
			endif 
	
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				
				if start_new_cutscene_no_fade(false, FALSE, TRUE, TRUE, true)
					
					CLEAR_PRINTS()
					SET_LABEL_AS_TRIGGERED("METH_GOD_14", FALSE)
					
					RELEASE_SCRIPT_AUDIO_BANK()
					
					// don't update Chef any more
					allow_buddy_ai_system = FALSE
					
					IF DOES_BLIP_EXIST(cheng.blip)
						REMOVE_BLIP(cheng.blip)
					ENDIF 
		
					IF DOES_ENTITY_EXIST(cheng.ped)
						register_entity_for_cutscene(cheng.ped, "Tao", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						CPRINTLN(DEBUG_MISSION,"cheng registered for outro")
					ENDIF
					IF DOES_ENTITY_EXIST(translator.ped)
						register_entity_for_cutscene(translator.ped, "Taos_Translator", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						CPRINTLN(DEBUG_MISSION,"translator registered for outro")
					ENDIF
					IF DOES_ENTITY_EXIST(buddy.ped)
						register_entity_for_cutscene(buddy.ped, "Cook", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						CPRINTLN(DEBUG_MISSION,"Chef registered for outro")
					ENDIF
					register_entity_for_cutscene(PLAYER_PED_ID(), "trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "getaway_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
					
					if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
						
						start_cutscene(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH) //supress flash. we are bledning out of the cutscene. Code will perform flash during blend out. 
					
					else 
						
						start_cutscene()
						
					endif 

					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					trigger_music_event("CHN1_FINAL_CS")
				
					if is_audio_scene_active("CHI_1_GO_TO_ICEBOX")
						stop_audio_scene("CHI_1_GO_TO_ICEBOX")
					endif 
					
					REDUCE_AMBIENT_MODELS(TRUE)
										
					mission_passed_mocap_status++
				
				ENDIF
			endif

		break 
		
		case 5
		
			if is_cutscene_playing()
				
				// fade in if coming from skip
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					ENDIF
				ENDIF
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
				
				// do this here so it's not a frame early
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				//might cycle through and switch all enemy vehicles off. 
				if is_vehicle_driveable(trevors_truck.veh)
					set_vehicle_engine_on(trevors_truck.veh, false, true)
				endif 

				check_mission_vehicles_for_chi_1_mcs_4_mocap()
		
				clear_area(<<1403.1777, 3597.2070, 33.7417>>, 1000.00, true, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(GoonModel1)
			
				// @SBA - Don't need the ice box anymore
				IF DOES_ENTITY_EXIST(oiIceBox)
					DELETE_OBJECT(oiIceBox)
				ENDIF
				IF DOES_ENTITY_EXIST(oiIceBoxProxy)
					DELETE_OBJECT(oiIceBoxProxy)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(mnIceBox)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnIceBoxProxy)
				
				object_index glass_window
				IF NOT DOES_ENTITY_EXIST(glass_window)
					glass_window = get_closest_object_of_type(<<1389.11, 3600.43, 39.51>>, 2.0, v_ret_ml_win5)
				ENDIF
				
				//crack type 
				//2 = shoutgun
				//3 = explosion
//				break_entity_glass(glass_window, <<1388.9, 3601.0, 40.2>>, 2.0, <<0.0, 1.0, 0.0>>, 1100.00, 3)
//				break_entity_glass(glass_window, <<1389.6, 3599.1, 40.2>>, 2.0, <<0.0, 1.0, 0.0>>, 1100.00, 3)
				break_entity_glass(glass_window, <<1389.16, 3600.3, 39.5>>, 2.0, <<0.0, 1.0, 0.0>>, 1100.00, 3, TRUE)
				break_entity_glass(glass_window, <<1388.8, 3600.9, 39.0>>, 2.0, <<0.0, 1.0, 0.0>>, 1100, 2, TRUE)
				break_entity_glass(glass_window, <<1389.3, 3600.1, 39.7>>, 2.0, <<0.0, 1.0, 0.0>>, 1100.00, 2, TRUE)

				SET_OBJECT_AS_NO_LONGER_NEEDED(glass_window)
				
				// turn cutscene vehicle's lights on if it's night
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("getaway_car"))
					viGetAwayCar = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("getaway_car"))
					CPRINTLN(DEBUG_MISSION, "Getaway car grabbed")
					IF NOT IS_DAYLIGHT_HOURS()
					AND DOES_ENTITY_EXIST(viGetAwayCar)
						CPRINTLN(DEBUG_MISSION, "Getaway car lights should be on")
						SET_VEHICLE_LIGHTS(viGetAwayCar, SET_VEHICLE_LIGHTS_ON)
					ENDIF
				ENDIF

				// Some clean up
				IF DOES_ENTITY_EXIST(cheng.ped)
					IF NOT IS_ENTITY_ON_SCREEN(cheng.ped)
						SET_PED_AS_NO_LONGER_NEEDED(cheng.ped)
						SET_MODEL_AS_NO_LONGER_NEEDED(cheng.model)
						CPRINTLN(DEBUG_MISSION,"Cheng no longer needed")
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(translator.ped)
					IF NOT IS_ENTITY_ON_SCREEN(translator.ped)
						SET_PED_AS_NO_LONGER_NEEDED(translator.ped)
						SET_MODEL_AS_NO_LONGER_NEEDED(translator.model)
						CPRINTLN(DEBUG_MISSION,"Translator no longer needed")
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(buddy.ped)
					SET_PED_AS_NO_LONGER_NEEDED(buddy.ped)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(buddy.model)
				
				SET_SRL_POST_CUTSCENE_CAMERA(<<1386.7813, 3597.9500, 35.6490>>, NORMALISE_VECTOR(<<-3.0000, 0.0000, -169.0608>>))
				
				mission_passed_mocap_status++
				
			endif 

		break 
		
		case 6
		
			if is_cutscene_playing()
				
				if not was_cutscene_skipped()
				
					printstring("test")
					printnl()
				
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)) 
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500)
					endif
					
					if CAN_SET_EXIT_STATE_FOR_CAMERA()
					
						set_gameplay_cam_relative_heading(0)
						set_gameplay_cam_relative_pitch(0)
						
						if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
							
							camera_a = create_cam_with_params("default_scripted_camera", <<1387.450195,3596.558350,35.691887>>, <<-8.343739,0.054248,-171.329453>>, 50.000000)
							set_cam_active(camera_a, true)
							render_script_cams(true, false)
							
						endif 

					endif
					
				else 
				
					trigger_music_event("CHN1_FINAL_CS_SKIP")
					
					SET_CUTSCENE_FADE_VALUES(false, false, true, false)
					
					mission_passed_mocap_status++
				
				endif
				
			else
			
				if does_entity_exist(buddy.ped)
					delete_ped(buddy.ped)
				endif 
				IF DOES_ENTITY_EXIST(viGetAwayCar)
					DELETE_VEHICLE(viGetAwayCar)
				ENDIF
				
				REPLAY_STOP_EVENT()

				if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
					end_cutscene_no_fade(true, true, 0, 0, 1000)
				else 
					end_cutscene_no_fade()
				endif 
				
				mission_passed()

			endif 
		
		break 
		
		case 7
		
			if is_screen_faded_out()
				if not is_cutscene_active()
				
					if does_entity_exist(buddy.ped)
						delete_ped(buddy.ped)
					endif 
					IF DOES_ENTITY_EXIST(viGetAwayCar)
						DELETE_VEHICLE(viGetAwayCar)
					ENDIF
					REPLAY_STOP_EVENT()
										
					end_cutscene()
				
					mission_passed()
					
				endif 
				
			endif 
					
		break 
		
	endswitch 

endproc 

proc chinese_mission_failed_system()

	KILL_INVINCIBLE_PLAYER_IF_IN_EXPLOSION()
				
	if GET_MISSION_FLOW_SAFE_TO_CLEANUP()

		mission_failed()
		
	endif 

endproc 

SCRIPT
	
	set_mission_flag(true)
	
	if has_force_cleanup_occurred()
		Store_Fail_Weapon(player_ped_id(), enum_to_int(get_current_player_ped_enum()))
		STORE_ENEMY_VEHICLES_STILL_ALIVE()
		Mission_Flow_Mission_Force_Cleanup()
		mission_cleanup()
	endif
	
	mission_setup()
	
	while true

		wait(0)
		
		if not stop_mission_fail_checks
			if mission_fail_checks()
				setup_mission_fail()
			endif 
		endif
		
		skip_system()
		
		HANDLE_CHINESE1_STATS()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TPIndustries") 
		
		IF NOT bPlayerUsedAbility
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				bPlayerUsedAbility = TRUE
			ENDIF
		ENDIF
		
		// help frames when there are exploding vehicles
		IF mission_flow >= wave_0_system
		AND mission_flow < wave_3_system
			DISABLE_VEHICLE_EXPLOSION_BREAK_OFF_PARTS()
		ENDIF

		switch mission_flow 
		
			case intro_mocap
			
				meth_lab_intro_mocap()
				
				//mission_flow = bikers_cutscene
			break 
			
			case get_to_meth_lab
			
				chinese_get_to_meth_lab()
				
			break 

			case wave_0_system 
			
				chinese_wave_0_system()
					
			break 
			
			// WAVE 1 - REMOVED @SBA
			
			case wave_2_system
			
				chinese_wave_2_system()
			
			break 
			
			case wave_3_system
			
				chinese_wave_3_system()
			
			break 
			
			case mission_passed_mocap
			
				chinese_mission_passed_mocap()
			
			break 
			
			case vehicle_recording
			
				meth_lab_vehicle_recording()
				
			break 

			case load_stage_selector_assets

				fbi4_load_stage_selector_assets()
				
			break 
			
			case mission_fail_stage
			
				chinese_mission_failed_system()

			break 
	
		endswitch 
		

		#IF IS_DEBUG_BUILD

			is_entity_in_angled_area(player_ped_id(), <<1980.730, 3073.228, 46.049>>, <<2014.615, 3051.972, 50.049>>, 26.700)
			
			angled_area_locate_widget()

//			request_ptfx_asset()
//			
//			if has_ptfx_asset_loaded()
//				if is_keyboard_key_just_pressed(key_t)
//					printstring("ptfx loaded")
//					printnl()
//				endif 
//			else 
//				if is_keyboard_key_just_pressed(key_t)
//					printstring("ptfx NOT loaded")
//					printnl()
//				endif 
//			endif 
//			
//			pfx_widget()

			if is_keyboard_key_just_pressed(key_s)
				mission_passed()
			endif
		
			if is_keyboard_key_just_pressed(key_f)
				mission_failed_text = ""
				setup_mission_fail()
			endif
			
			if is_keyboard_key_just_pressed(key_k)
				for i = 0 to count_of(wave_0_inside_enemy) - 1
					if does_entity_exist(wave_0_inside_enemy[i].ped)
						set_entity_health(wave_0_inside_enemy[i].ped, 2)
					endif 
				endfor 
			endif 
			

		#endif 

	endwhile 

endscript 
//lkmethlab waypoints
//waypoint 4 = buddy running from inside lab to outside to shoot inside enemy guys
//waypoint 5 = buddy running from outside lab to other side of rooftop 
//waypoint 3 = buddy running from rooftop down stairs

//player_running_from
//waypoint 6 = player running from inside lab to outside. 
//waypoint 7 = player running from outside lab to other side of rooftop

