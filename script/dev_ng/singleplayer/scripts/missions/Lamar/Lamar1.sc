// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Lamar1.sc
//		AUTHOR			:	Matthew Booton
//		DESCRIPTION		:	Franklin, Lamar and Stretch go to a deal at a recycling plant,
//							SWAT show up and they have to fight their way out.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_player.sch"
USING "commands_recording.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_cutscene.sch"
USING "commands_itemsets.sch"

USING "code_control_public.sch"
USING "blip_control_public.sch"
USING "replay_public.sch"
USING "dialogue_public.sch"
USING "family_public.sch"
USING "flow_public_core_override.sch"
USING "flow_special_event_checks.sch"
USING "flow_public_game.sch"
USING "gunclub_shop_private.sch"
USING "script_player.sch"
USING "script_blips.sch"
USING "script_misc.sch"
USING "shop_public.sch"
USING "chase_hint_cam.sch"
USING "locates_public.sch"
USING "selector_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "taxi_functions.sch"
USING "CompletionPercentage_public.sch"
USING "vehicle_gen_public.sch"
USING "clearMissionArea.sch"
USING "TimeLapse.sch"
USING "buddy_head_track_public.sch"
USING "building_control_public.sch"
USING "cheat_controller_public.sch"


#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

ENUM MISSION_STAGE
	STAGE_OPENING_CUTSCENE = 0,
	STAGE_GO_TO_GUN_SHOP,
	STAGE_SHOP_INTRO_CUTSCENE,
	STAGE_BUY_GRENADES,
	STAGE_GO_TO_PLANT,
	STAGE_MEETING_CUTSCENE,
	STAGE_ESCAPE_PRE_WAREHOUSE,
	STAGE_ESCAPE_FROM_WAREHOUSE,
	STAGE_GO_BACK_HOME,
	STAGE_DROPOFF_CUTSCENE
ENDENUM

ENUM MISSION_REQUIREMENT
	REQ_LAMAR,
	REQ_STRETCH,
	REQ_UNLOCK_SHOP,
	REQ_BREAK_ROOF,
	REQ_ASSISTED_ROUTES,
	REQ_OPEN_GUN_SHOP,
	REQ_GUN_SHOP_IN_MEMORY,
	REQ_RECYCLING_INTERIOR_IN_MEMORY,
	REQ_STRETCH_AND_LAMAR_READY_TO_FIGHT,
	REQ_AMBIENCE_REMOVED_FOR_FIGHT,
	REQ_AMBIENT_SIRENS
ENDENUM

ENUM FAILED_REASON
	FAILED_GENERIC = 0,
	FAILED_LAMAR_DIED,
	FAILED_STRETCH_DIED,
	FAILED_FRANKLINS_CAR_DESTROYED,
	FAILED_FRANKLINS_CAR_STUCK,
	FAILED_LEFT_LAMAR,
	FAILED_LEFT_STRETCH,
	FAILED_LEFT_LAMAR_AND_STRETCH,
	FAILED_LAMAR_AND_STRETCH_DIED,
	FAILED_SHOP_CLOSED,
	FAILED_MISSED_DEAL,
	FAILED_ABANDONED_CAR,
	FAILED_DIDNT_BUY_SHOTGUN
ENDENUM

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP
ENDENUM

ENUM STYLE_STATUS
	STYLE_STATUS_NO_CHANGE,
	STYLE_STATUS_BAD_CHANGE,
	STYLE_STATUS_GOOD_CHANGE,
	STYLE_STATUS_DONT_CHECK
ENDENUM

ENUM TASK_TO_PERFORM_AFTER_MOCAP
	POST_MOCAP_COVER_THEN_COMBAT,
	POST_MOCAP_COVER,
	POST_MOCAP_COMBAT,
	POST_MOCAP_AIM_AT_COORDS
ENDENUM

ENUM SYNCED_SCENE_TRANSITION_STATE
	SST_READY_TO_START_PLAYING,
	SST_TRAVELLING_TO_START_POINT,
	SST_PLAYER_IS_BLOCKING_START_POINT
ENDENUM

ENUM TRIGGERED_TEXT_LABEL
	LEM1_BREACH1,
	LEM1_KNOCK,
	LEM1_BREACHAMB,
	LEM1_HELIC,
	LEM1_FIGHT1,  
	LEM1_AHEAD,
	LEM1_BREACH2,
	LEM1_BREACH3,
	LEM1_GOGO1,
	LEM1_HELIROOM,
	LEM1_GOGO2,
	LEM1_STAIRS,
	LEM1_FIRST1, 
	LEM1_WIN2,
	LEM1_FIRSTSTAIRS,
	LEM1_DELAYSTAIRS, 
	LEM1_FIRSTSMOKE,
	LEM1_DELAYSMOKE,
	LAM1_FCHAT1, 
	LEM1_FIRSTWARE, 
	LEM1_GOGOWARE,
	LEM1_FIRST2, 
	LEM1_EXIT,
	LEM1_FIRSTEXT,
	LEM1_GOGOEXT,
	LEM1_DELAYEXT,
	LEM1_WAYOUT, 
	LEM1_EXT,
	LEM1_DELAYEXT2,
	LEM1_CINEHELP,
	LAM1_FCHAT2, 
	LEM1_LADR,
	LEM1_BRIDGE,
	LEM1_TOP,
	LEM1_GOLAD,
	LEM1_ROOF,
	LEM1_MOVE_GANTRY,
	LEM1_KEEPUP_RUN,
	LEM1_SLIP,
	LEM1_GOGOSMOKE, 
	VICTIM_SHOCKED,
	LEM1_COPS1,
	LEM1_JACK, 
	LEM1_JAC2, 
	LEM1_DRIVE,
	LEM1_LEFTBOTH,
	LEM1_HAIR,
	LEM1_TOGUN,
	LEM1_PRESHOP,
	LEM1_BROKE_1,
	LEM1_BUYN,
	LEM1_LMONEY,
	LEM1_BUYALL, 
	LEM1_SHOP_1, 
	LEM1_BACKSHOP,
	LEM1_SFOUND,
	LEM1_SFLASH,
	LEM1_SMOAN3,
	LEM1_SHOP1,
	LEM1_SHOP2,
	LEM1_SHOP3,
	LEM1_SHOP6,
	LEM1_SRETURN,
	LEM1_STUFF,
	LEM1_SAMMO,
	LEM1_COLOR,
	LEM1_SHOP5, 
	LEM1_LEFTMEET,
	LEM1_REC,
	LEM1_REC2,
	LEM1_ARRIVE,
	LEM1_STAIR,
	LEM1_STOP, 
	LEM1_DOOR, 
	LEM1_KILLSWAT, 
	LEM1_GOGO, 
	LEM1_DOOR2, 
	LEM1_FLAHELP, 
	LEM1_COVHELP,
	LEM1_ACCSTAT1, 
	LEM1_ACCSTAT2, 
	LEM1_WAIT1, 
	LEM1_RIOT,
	LEM1_GRENHELP2,
	LEM1_GRENHELP1,
	LEM1_GRGUNHELP,
	LEM1_WEAPHELP1,
	LEM1_WEAPHELP2,
	LEM1_COPSARR,
	LEM1_KILLCHOP,
	LEM1_GOWALL, 
	LEM1_ROOF2,
	LEM1_WAITSTRET,
	LEM1_WAITLEMAR,
	LEM1_WAITBOTH,
	LEM1_SMOKEANY,
	LEM1_GOGOS, 
	LEM1_TRIES,
	LEM1_1GUY_1,
	LEM1_NOLIFT,
	LEM1_FIRE,
	LEM1_TWOLEFTWARE, 
	LEM1_RIOT2, 
	LEM1_EXITH, 
	LEM1_ROOF3, 
	LEM1_KEEPUP_END,
	LEM1_KEEPUPS,
	LEM1_COPS4,
	LEM1_HOUSE,
	LEM1_SPECHELP,
	LEM1_ALLEYHELP,
	LEM1_LOOKHELP,
	LEM1_COPS2b,
	LEM1_BACK,
	LEM1_CHAT2,
	LEM1_HOME,
	LEM1_HOME2,
	LEM1_CHAT1,
	LEM1_FCONV1,
	LEM1_FCONV2,
	LEM1_FCONV3,
	LEM1_FCONV4,
	LEM1_FCONV5,
	LEM1_FCONV6,
	LEM1_FCONV7,
	LEM1_FCONV8,
	LEM1_FCONV9,
	LEM1_FCONV10,
	CMN_GENGETIN
ENDENUM

STRUCT ROPE_DATA
	ROPE_INDEX rope
	FLOAT f_length
	INT i_num_segments
	FLOAT f_length_per_segment
ENDSTRUCT

STRUCT ENEMY_PED
	PED_INDEX ped
	BLIP_INDEX blip
	BOOL b_is_created
	BOOL b_has_started_rope_jump
	COVERPOINT_INDEX cover
	INT i_event
	INT i_rope_event
	INT i_timer
	INT i_sync_scene
	INT i_bitset
	ROPE_DATA s_rope
	VECTOR v_dest //Used to store the current destination of a ped.
	AI_BLIP_STRUCT s_blip_data
ENDSTRUCT

STRUCT LAMAR_DATA
	PED_INDEX ped
	BLIP_INDEX blip
	COVERPOINT_INDEX cover
	INT i_event
	INT i_timer
	INT i_secondary_timer
	INT i_sync_scene
	INT i_health
	VECTOR v_dest
	BOOL b_refresh_tasks
	BOOl b_is_using_secondary_cover
ENDSTRUCT

STRUCT FIRE_DATA
	PTFX_ID ptfx
	FLOAT f_scale
	FLOAT f_start_threshold
	INT i_timer
	INT i_triggered_by
	VECTOR v_pos
	BOOL b_use_alternate_fire
ENDSTRUCT

STRUCT AIM_HELPER_DATA
	OBJECT_INDEX obj
	VECTOR v_pos
	VECTOR v_ped_pos
	VECTOR v_desired_pos
	INT i_timer
ENDSTRUCT

STRUCT DOOR_LOCK_DATA //Groups together variables used for locking various doors in the mission.
	BOOL b_is_locked
	INT i_lock_update_timer
ENDSTRUCT

STRUCT MISSION_VEH
	VEHICLE_INDEX veh
	BLIP_INDEX blip
ENDSTRUCT

BOOL b_is_jumping_directly_to_stage			= FALSE
BOOL b_skipped_mocap						= FALSE
BOOL b_set_vehicle_lights_for_cutscene		= FALSE
BOOL b_lamar_has_reached_exit				= FALSE
BOOL b_player_escaped						= FALSE
BOOL b_lamar_on_the_ground					= FALSE
BOOL b_lamar_escaped						= FALSE
BOOL b_stretch_escaped						= FALSE
BOOL b_lamar_climbed_ladder					= FALSE
BOOL b_player_climbed_ladder				= FALSE
BOOL b_stretch_climbed_ladder				= FALSE
BOOL b_rappel_audio_scene_active			= FALSE
BOOL b_play_grenade_kill_dialogue			= FALSE
BOOl b_mission_failed						= FALSE
BOOL b_lamar_alternate_run_active			= FALSE
BOOL b_stretch_alternate_run_active			= FALSE
BOOL b_player_alternate_run_active			= FALSE
BOOl b_already_set_chopper_to_explode		= FALSE
BOOL b_machine_swat_ragdoll_on				= FALSE
BOOL b_used_a_checkpoint					= FALSE
BOOL b_cops_relationship_set				= FALSE
BOOL b_blocked_shop_dialogue				= FALSE
BOOL b_lamar_started_climbing_ladder		= FALSE
BOOL b_stretch_started_climbing_ladder		= FALSE
BOOL b_taxi_drop_off_updated				= FALSE
BOOL b_player_has_browsed_shop				= FALSE
BOOL b_player_has_bought_something			= FALSE
BOOL b_frozen_breach_doors					= FALSE
BOOL b_player_started_climbing_ladder		= FALSE
BOOL b_player_reached_exit					= FALSE
BOOL b_wanted_level_difficulty_lowered		= FALSE
BOOL b_triggered_police_report				= FALSE
//BOOL b_lamar_reached_ladder				= FALSE
BOOL b_stretch_reached_ladder				= FALSE
BOOL b_already_been_in_a_car				= FALSE
BOOL b_buddies_need_to_switch_to_group		= FALSE
BOOL b_shit_skipped_weapons_shop			= FALSE
BOOL b_carjack_vehicle_asset_requested		= FALSE
BOOL b_mission_passed						= FALSE
BOOL b_player_left_shop						= FALSE
BOOL b_given_player_money					= FALSE
BOOL b_need_to_create_start_vehicle			= FALSE
BOOL b_getaway_car_driver_is_dead			= FALSE
BOOL b_player_already_has_colour_mod		= FALSE
BOOL b_lamar_is_closest_to_ladder			= FALSE
BOOL b_force_cover_help_to_trigger			= FALSE
BOOL b_player_already_has_shotgun			= FALSE
BOOL b_applied_balla_blood					= FALSE
BOOL b_did_a_timelapse_at_plant				= FALSE
BOOl b_player_rode_bike_into_plant			= FALSE
BOOl b_safe_for_lamar_to_run_gantry_first	= FALSE
BOOL b_trigger_stretch_run_alternate_anim	= FALSE
BOOL b_already_checked_for_cars_at_shop		= FALSE
BOOL b_player_inside_plant					= FALSE
BOOl b_gun_shops_were_already_open			= FALSE
BOOL b_locker_doors_opened					= FALSE
BOOL b_stretch_locker_tasks_updated			= FALSE
BOOL b_skipped_to_lose_cops_stage			= FALSE
BOOL b_player_been_in_car					= FALSE
BOOL b_enemy_in_first_room_set_to_charge	= FALSE
BOOL b_glass_smashed_in_first_room			= FALSE
BOOL b_glass_smashed_in_heli_room			= FALSE
BOOL b_shootout_started						= FALSE
BOOL b_player_left_shop_too_early			= FALSE
BOOL b_buddies_set_to_attract_cops			= FALSE
BOOL b_players_tyres_set_to_not_burst		= FALSE
BOOL b_cops_triggered_due_to_griefing		= FALSE
BOOL b_override_cam_anim					= FALSE
BOOL b_lamars_lead_out_triggered_early		= FALSE
BOOL b_replay_event_started					= FALSE
BOOL b_light_ropes_are_attached[4]
BOOL b_has_text_label_triggered[140]

//BOOL b_current_car_added_to_mix_group		= FALSE

CONST_FLOAT FIRE_DEBRIS_START_TIME		0.25
FLOAT f_chopper_playback_speed			= 1.0
FLOAT f_gantry_chopper_playback_speed	= 1.0
FLOAT f_lift_door_open_ratio			= 0.0
FLOAT f_lamar_heading_in_shop			= 301.9804
FLOAT f_stretch_heading_in_shop			= 69.4093
FLOAT f_rope_length						= 2.6
FLOAT f_current_fastest_vehicle_speed	= 0.0

CONST_INT CARREC_RAPPEL_1					1
CONST_INT CARREC_INDOOR_CHOPPER				2
CONST_INT CARREC_INDOOR_CHOPPER_2			10
CONST_INT CARREC_GANTRY_CHOPPER				11
CONST_INT CHECKPOINT_GO_INSIDE_GUN_SHOP		1
CONST_INT CHECKPOINT_GO_TO_PLANT			2
CONST_INT CHECKPOINT_INTERIOR_START			3
CONST_INT CHECKPOINT_INTERIOR_MIDDLE		4
CONST_INT CHECKPOINT_LOSE_COPS				5
CONST_INT WAREHOUSE_ENEMIES_ARE_IN_COMBAT	5
CONST_INT BUDDY_EVENT_MID_FIGHT_CHECKPOINT	50
CONST_INT SWAT_SPEAKER_1					3
CONST_INT SWAT_SPEAKER_2					4
CONST_INT SWAT_SPEAKER_3					8
CONST_INT DENISE_ID							0
CONST_INT MAGENTA_ID						1
CONST_INT FRIEND_ID_1						2
CONST_INT FRIEND_ID_2						3
CONST_INT ENEMY_FLAG_EXPLOSION_PROOF		1
CONST_INT ENEMY_FLAG_CANCELLED_CHARGE		2
CONST_INT ENEMY_FLAG_REFRESH_TASKS			3
INT i_current_event							= 0
INT i_meeting_dialogue_event				= 0
INT i_shop_banter_timer						= 0
INT i_interior_fail_check_event				= 0
INT i_interior_fail_timer					= 0
INT i_interior_fail_timer_2					= 0
INT i_fire_timer							= 0
INT i_player_combat_dialogue_timer			= 0
INT i_player_combat_dialogue_timer_2		= 0
INT i_buddy_combat_dialogue_timer			= 0
INT i_combat_dialogue_wait_time				= 0
INT i_rappel_sound_ids[4]
INT i_fail_timer							= 0
INT i_lose_cops_dialogue_timer				= 0
INT i_smoke_dialogue_timer					= 0
INT i_swat_dialogue_timer					= 0
INT i_chopper_death_counter					= 0
INT i_chopper_death_timer					= 0
INT i_stretch_shop_moan_timer				= 0
INT i_previous_funds						= 0
INT i_wait_dialogue_timer					= 0
INT i_fire_sound
INT i_grenade_help_timer					= 0
INT i_clear_grenade_help_timer				= 0
INT i_grenade_help_event					= 0
INT i_lose_cops_music_event					= 0
INT i_carjack_event							= 0
INT i_carjack_timer							= 0
INT i_warehouse_dialogue_timer				= 0
INT i_hooker_event							= 0
INT i_hooker_timer							= 0
INT i_exit_warehouse_music_timer			= 0
INT i_current_music_event					= 0
INT i_time_of_last_grenade_thrown 			= 0
INT i_num_grenade_kills_before_last_grenade = 0
INT i_prev_grenade_ammo 					= 0
INT i_shooting_stat_help_timer				= 0
INT i_cops_arrival_timer					= 0
INT i_armour_on_mission_start				= 0
INT i_num_lose_cops_lines_played_1			= 0
INT i_num_lose_cops_lines_played_2			= 0
INT i_haircut_dialogue_timer				= 0
INT i_meeting_dialogue_timer				= 0
INT i_hurry_up_exterior_dialogue_timer		= 0
INT i_num_times_played_ladder_hurry_line	= 0
INT i_audio_scene_event_drive_to_shop		= 0
INT i_audio_scene_event_shootout			= 0
INT i_time_shootout_started					= 0
INT i_time_fire_started						= 0
INT i_stretch_leave_warehouse_timer			= 0
INT i_lamar_leave_warehouse_timer			= 0
INT i_chopper_dialogue_timer				= 0
INT i_drive_to_plant_dialogue_timer			= 0
INT i_warehouse_fire_counter_1				= 0
INT i_warehouse_fire_counter_2				= 0
INT i_cover_in_doorway_index				= 0
//INT i_new_load_scene_timer					= 0
INT i_tyre_burst_timer						= 0
INT i_time_exterior_cop_cars_created		= 0
INT i_sync_scene_timelapse					= 0
INT i_time_lamar_and_stretch_escaped		= 0
INT i_gas_explosion_timer					= 0
//INT i_time_since_player_reached_exit		= 0
INT iFrontDoor = HASH("DOORHASH_F_HOUSE_SC_F")	//This is the map front door, so we can open it to make the interior render
INT iWarehouseDoorLeft = HASH("WAREHOUSE_LEFT")
INT iWarehouseDoorRight = HASH("WAREHOUSE_RIGHT")
INT iReplayCameraSpawnTimer	//Fix for bug 2222067

STRING str_swat_anims					= "misslamar1swat_rappel"
STRING str_shop_anims					= "misslamar1ig_1"
STRING str_text_block					= "LEM1AUD"
STRING str_carrec						= "mattlemar"
STRING str_intro_cutscene				= "lamar_1_int"
STRING str_timelapse_anims				= "misstimelapse@franklinold_home"
STRING str_door_breach_ptfx				= "scr_lamar1_door_breach"
STRING str_waypoint_interior			= "lamar1_interior"
STRING str_waypoint_carjack				= "lamar1_carjack"
STRING str_waypoint_stairs				= "lamar1_01"
STRING str_fail_label 					= ""
STRING str_hooker_anims					= "oddjobs@assassinate@vice@hooker"
STRING str_balla_dead_anim				= "misslamar1dead_body"

//VECTOR v_plant_pos						= <<-615.6497, -1600.8878, 25.7517>> //<<-609.6092, -1607.7295, 25.8475>>
VECTOR v_meeting_pos					= <<-617.2782, -1618.6831, 32.0105>>
VECTOR v_end_pos						= <<-25.0062, -1435.9220, 29.6542>>
//VECTOR v_riot_attach_offset				= <<0.292, 0.784, -0.292>>
//VECTOR v_riot_attach_rot				= <<0.0, 0.0, 155.880>>
VECTOR v_gun_shop_pos					= <<28.9547, -1110.9738, 28.2798>>
VECTOR v_inside_gun_shop_pos			= <<18.4291, -1113.3621, 28.7970>>
VECTOR v_lamar_pos_in_shop				= <<22.3429, -1109.2799, 28.7970>>
VECTOR v_stretch_pos_in_shop			= <<17.8331, -1111.8099, 28.7970>>
VECTOR v_interior_entrance_l			= <<-614.4, -1620.3, 33.2>>//<<-608.7, -1610.3, 27.2>>
VECTOR v_interior_entrance_r			= <<-614.1, -1617.7, 33.2>>//<<-611.3, -1610.1, 27.2>>
VECTOR v_interior_after_chopper_l		= <<-566.3, -1630.5, 33.2>>
VECTOR v_interior_after_chopper_r		= <<-566.1, -1627.9, 33.2>>
VECTOR v_breach_door_1_pos				= <<-591.5, -1621.4, 33.2>>
VECTOR v_breach_door_2_pos				= <<-588.9, -1621.6, 33.2>>
VECTOR v_shop_blip_pos					= <<22.1, -1106.6, 29.8>>

BLIP_INDEX blip_current_destination

CAMERA_INDEX cam_cutscene
//CAMERA_INDEX cam_interp_helper
//CAMERA_INDEX cam_attach_helper

COVERPOINT_INDEX cover_player
COVERPOINT_INDEX cover_locker
COVERPOINT_INDEX cover_shootout[15]

DECAL_ID decal_blood

FIRE_DATA s_warehouse_fire[15]
FIRE_DATA s_warehouse_fire_2[19]
MISSION_VEH s_lost_bikes[4]

INTERIOR_INSTANCE_INDEX interior_recycle
INTERIOR_INSTANCE_INDEX interior_gun_shop
INTERIOR_INSTANCE_INDEX interior_franklins_house

OBJECT_INDEX obj_warehouse_gas
OBJECT_INDEX obj_breach_door
OBJECT_INDEX obj_breach_doors[2]
OBJECT_INDEX obj_wooden_box
OBJECT_INDEX obj_lights[4]
OBJECT_INDEX obj_rope_base[4]
OBJECT_INDEX obj_franklins_front_door
OBJECT_INDEX objFrontDoor	//This is the map front door, so we can just make it invisible

PED_INDEX ped_carjack_victim
PED_INDEX ped_hooker
PED_INDEX ped_hooker_driver
PED_INDEX ped_ballas_og
PED_INDEX ped_shop_owner

PICKUP_INDEX pickup_health[2]
PICKUP_INDEX piWeaponPickup

PTFX_ID ptfxDebris[2]

//GROUP_INDEX group_player
ROPE_INDEX rope_lights[4]

VEHICLE_INDEX veh_start_car
VEHICLE_INDEX veh_chopper_indoor_final
VEHICLE_INDEX veh_chopper_gantry
VEHICLE_INDEX veh_hooker_car
VEHICLE_INDEX veh_cops_setpiece_1
VEHICLE_INDEX veh_cops_setpiece_1b
VEHICLE_INDEX veh_cops_setpiece_1c
VEHICLE_INDEX veh_cops_setpiece_2[2]
VEHICLE_INDEX veh_cops_setpiece_3
VEHICLE_INDEX veh_cops_setpiece_4[2]
VEHICLE_INDEX veh_weapons_shop_car			//The car the player arrives at the weapons shop in (can be the same as veh_start_car or an ambient car).
VEHICLE_INDEX veh_final_cutscene_car
VEHICLE_INDEX veh_carjack
VEHICLE_INDEX veh_lamar_stretch_exit
//VEHICLE_INDEX veh_current_stolen_car

LAMAR_DATA s_lamar
LAMAR_DATA s_stretch
LOCATES_HEADER_DATA s_locates_data
structPedsForConversation s_conversation_peds
structTimelapse sTimelapse

ENEMY_PED s_swat_indoor_lift[2]
ENEMY_PED s_swat_indoor_breach[2]
ENEMY_PED s_swat_indoor_window_1[3]
ENEMY_PED s_swat_indoor_heli[5]
ENEMY_PED s_swat_indoor_pre_warehouse[4]
ENEMY_PED s_swat_indoor_roof_1[5]
ENEMY_PED s_swat_indoor_warehouse[6]
ENEMY_PED s_swat_indoor_breach_2[2]
ENEMY_PED s_swat_indoor_window_2[1]
ENEMY_PED s_swat_indoor_chopper_final[3]

ENEMY_PED s_cops_chopper_gantry[1]
ENEMY_PED s_cops_setpiece_1[2]
ENEMY_PED s_cops_setpiece_1b[2]
ENEMY_PED s_cops_setpiece_1c[2]

MISSION_STAGE e_mission_stage 			= STAGE_OPENING_CUTSCENE
SECTION_STAGE e_section_stage 			= SECTION_STAGE_SETUP
STYLE_STATUS e_hair_status
STYLE_STATUS e_clothes_status
LADDER_INPUT_STATE e_lamar_ladder_state
LADDER_INPUT_STATE e_stretch_ladder_state

MODEL_NAMES model_swat 					= S_M_Y_SWAT_01
MODEL_NAMES model_gang					= G_M_Y_BALLAEAST_01
MODEL_NAMES model_gang_alternate		= G_M_Y_BALLASOUT_01
//MODEL_NAMES model_noose_van			= NOOSEVAN
MODEL_NAMES model_chopper				= POLMAV
MODEL_NAMES model_left_lift_door 		= V_ILEV_RC_DOOREL_L
MODEL_NAMES model_right_lift_door 		= V_ILEV_RC_DOOREL_R
MODEL_NAMES model_interior_entrance_l	= V_ILEV_RC_DOOR1 //V_ILEV_SS_DOOR5_L
MODEL_NAMES model_interior_entrance_r	= V_ILEV_RC_DOOR1 //V_ILEV_SS_DOOR5_R
MODEL_NAMES model_escape_car			= JACKAL
MODEL_NAMES model_escape_car_2			= CAVALCADE2
MODEL_NAMES model_breach_door 			= V_ILEV_RC_DOOR1_ST
MODEL_NAMES model_cop 					= S_M_Y_COP_01
MODEL_NAMES model_cop_car 				= POLICE
MODEL_NAMES model_carjack_victim		= A_M_M_GENFAT_01
MODEL_NAMES model_hooker				= S_F_Y_Hooker_02
MODEL_NAMES model_gas_can 				= Prop_GasCyl_01A
MODEL_NAMES model_ballas_og				= IG_BALLASOG
MODEL_NAMES model_light 				= PROP_RECYCLE_LIGHT
MODEL_NAMES model_light_to_remove 		= PROP_RECYCLE_LIGHT
MODEL_NAMES model_lamars_car			= EMPEROR2
MODEL_NAMES model_franklins_front_door	= V_ILEV_FA_FRONTDOOR

REL_GROUP_HASH rel_group_swat
REL_GROUP_HASH rel_group_neutral
REL_GROUP_HASH rel_group_bikers

SCENARIO_BLOCKING_INDEX sbi_recycling_plant[3]
SCENARIO_BLOCKING_INDEX sbi_weapons_shop

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widget_debug
	PTFX_ID ptfx_debug
	
	VECTOR v_debug_riot_shield_offset
	VECTOR v_debug_riot_shield_rot
	
	BOOL b_debug_draw_bullets
	BOOL b_debug_enable_forces
	BOOL b_debug_reset_values
	BOOL b_debug_draw_rappel_state
	BOOL b_debug_display_mission_debug
	BOOL b_debug_play_god_text			= FALSE
	BOOL b_debug_play_convo_subs		= FALSE
	BOOL b_debug_play_convo_no_subs		= FALSE
	BOOL b_debug_clear_all_text			= FALSE
	BOOL b_debug_break_glass			= FALSE
	BOOL b_debug_clear_glass			= FALSE
	BOOL b_debug_show_glass_debug		= FALSE
	BOOL b_debug_output_glass_info		= FALSE
	BOOL b_debug_trigger_door_ptfx		= FALSE
	BOOL b_debug_trigger_smoke_ptfx		= FALSE
	
	VECTOR v_debug_glass_break_pos		= <<-563.850891,-1632.201660,30.240250>>
	VECTOR v_debug_glass_break_force	= <<0.0, 7.0, 0.0>>
	VECTOR v_debug_smoke_ptfx_pos		= <<-586.1, -1605.4, 27.6>>
	VECTOR v_debug_smoke_ptfx_rot		= <<0.0, 0.0, 0.0>>
	
	FLOAT f_debug_glass_break_damage	= 20.0
	FLOAT f_debug_glass_break_radius	= 0.5	
	FLOAT f_debug_smoke_ptfx_smoke_evo	= 1.0
	FLOAT f_debug_smoke_ptfx_cinder_evo	= 1.0
	FLOAT f_debug_smoke_ptfx_debris_evo	= 1.0
	FLOAT f_debug_smoke_ptfx_speed_evo	= 1.0
	FLOAT f_debug_smoke_ptfx_strength_evo = 1.0

	INT i_debug_break_glass_type			= 0
	INT i_debug_num_prints_this_frame		= 0
	INT i_debug_jump_stage					= 0
	INT i_debug_cutscene_timer				= 0
	MissionStageMenuTextStruct s_skip_menu[10]    
	
	STRUCT FORCE_DATA
		INT i_force_type
		INT i_duration
		INT i_time_started
		FLOAT f_start_time
		VECTOR v_force
		VECTOR v_offset
		BOOL b_local_force
		BOOL b_local_offset	
	ENDSTRUCT
	
	FORCE_DATA s_button_forces[4]
	
	PROC CREATE_WIDGETS()
		INT i = 0
	
		widget_debug = START_WIDGET_GROUP("Lamar1")	
			ADD_WIDGET_BOOL("Trigger door explosion", b_debug_trigger_door_ptfx)
			ADD_WIDGET_INT_READ_ONLY("Cutscene timer", i_debug_cutscene_timer)
			ADD_WIDGET_BOOL("Show rappel state", b_debug_draw_rappel_state)
			ADD_WIDGET_BOOL("Force cover help on", b_force_cover_help_to_trigger)
			
			START_WIDGET_GROUP("Nextgen Smoke")
				ADD_WIDGET_BOOL("Trigger effect", b_debug_trigger_smoke_ptfx)
				ADD_WIDGET_VECTOR_SLIDER("Pos", v_debug_smoke_ptfx_pos, -2000.0, 100.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rot", v_debug_smoke_ptfx_rot, -2000.0, 100.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Smoke evo", f_debug_smoke_ptfx_smoke_evo, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Cinder evo", f_debug_smoke_ptfx_cinder_evo, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Debris evo", f_debug_smoke_ptfx_debris_evo, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Speed evo", f_debug_smoke_ptfx_speed_evo, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Strength evo", f_debug_smoke_ptfx_strength_evo, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Variable debug")
				ADD_WIDGET_INT_READ_ONLY("i_current_music_event", i_current_music_event)
				ADD_WIDGET_INT_READ_ONLY("i_carjack_event", i_carjack_event)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Break glass test")
				ADD_WIDGET_BOOL("Break glass", b_debug_break_glass)
				ADD_WIDGET_BOOL("Reset glass", b_debug_clear_glass)
				ADD_WIDGET_BOOL("Show debug", b_debug_show_glass_debug)
				ADD_WIDGET_BOOL("Output", b_debug_output_glass_info)
				ADD_WIDGET_VECTOR_SLIDER("Glass pos", v_debug_glass_break_pos, -5000.0, 5000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Radius", f_debug_glass_break_radius, 0.0, 10.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Force dir", v_debug_glass_break_force, -10.0, 10.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Damage", f_debug_glass_break_damage, 0.0, 200.0, 0.01)
				ADD_WIDGET_INT_SLIDER("Type", i_debug_break_glass_type, 0, 10, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Subtitles test")
				ADD_WIDGET_BOOL("Play god text", b_debug_play_god_text)
				ADD_WIDGET_BOOL("Play conversation with subtitles", b_debug_play_convo_subs)
				ADD_WIDGET_BOOL("Play conversation without subtitles", b_debug_play_convo_no_subs)
				ADD_WIDGET_BOOL("Clear all text", b_debug_clear_all_text)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Variables")
				ADD_WIDGET_INT_READ_ONLY("i_chopper_death_counter", i_chopper_death_counter)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Riot shield")
				ADD_WIDGET_VECTOR_SLIDER("Attach offset", v_debug_riot_shield_offset, -2.0, 2.0, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("Attach rotation", v_debug_riot_shield_rot, -180.0, 180.0, 0.001)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Helicopter gunner")
				ADD_WIDGET_BOOL("Show bullet positions", b_debug_draw_bullets)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Vehicle forces")
				ADD_WIDGET_BOOL("Enable forces", b_debug_enable_forces)
				ADD_WIDGET_BOOL("Reset values", b_debug_reset_values) 
				
				REPEAT COUNT_OF(s_button_forces) i
					START_WIDGET_GROUP("Force Slot")
						ADD_WIDGET_INT_SLIDER("Duration", s_button_forces[i].i_duration, 0, 10000, 1)
						ADD_WIDGET_INT_SLIDER("Force type", s_button_forces[i].i_force_type, 0, 2, 1)
						ADD_WIDGET_VECTOR_SLIDER("Force", s_button_forces[i].v_force, -100.0, 100.0, 0.01)
						ADD_WIDGET_VECTOR_SLIDER("Offset", s_button_forces[i].v_offset, -4.0, 4.0, 0.01)
						ADD_WIDGET_BOOL("Local force", s_button_forces[i].b_local_force)
						ADD_WIDGET_BOOL("Local offset", s_button_forces[i].b_local_offset)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	ENDPROC
	
	PROC DESTROY_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widget_debug)
			DELETE_WIDGET_GROUP(widget_debug)
		ENDIF
	ENDPROC
	
	PROC DRAW_STRING_TO_DEBUG_DISPLAY(STRING str_debug)			
		IF b_debug_display_mission_debug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			DRAW_DEBUG_TEXT_2D(str_debug, <<0.05, 0.05 + (0.05 * i_debug_num_prints_this_frame), 0.0>>, 0, 0, 255, 255)
			i_debug_num_prints_this_frame++
		ENDIF
	ENDPROC
	
	PROC DRAW_INT_TO_DEBUG_DISPLAY(INT i_debug, STRING str_label)
		IF b_debug_display_mission_debug
			TEXT_LABEL str_concat = str_label
			str_concat += i_debug
		
			DRAW_STRING_TO_DEBUG_DISPLAY(str_concat)
		ENDIF
	ENDPROC
	
	PROC DRAW_FLOAT_TO_DEBUG_DISPLAY(FLOAT f_debug, STRING str_label)
		IF b_debug_display_mission_debug
			TEXT_LABEL str_concat = str_label
			str_concat += GET_STRING_FROM_FLOAT(f_debug)
		
			DRAW_STRING_TO_DEBUG_DISPLAY(str_concat)
		ENDIF
	ENDPROC
	
	PROC DUMP_MISSION_STATE_TO_CONSOLE()
		INT i = 0
		INT iCurrentTime = GET_GAME_TIMER()
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "Dumping script state, current game time = ", iCurrentTime)
		
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "e_mission_stage = ", e_mission_stage)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "e_section_stage = ", e_section_stage)
		
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "i_current_event = ", i_current_event)
		
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_mission_failed = ", b_mission_failed)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_lamar_has_reached_exit = ", b_lamar_has_reached_exit)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_player_escaped = ", b_player_escaped)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_lamar_escaped = ", b_lamar_escaped)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_lamar_climbed_ladder = ", b_lamar_climbed_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_player_climbed_ladder = ", b_player_climbed_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_stretch_climbed_ladder = ", b_stretch_climbed_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_lamar_started_climbing_ladder = ", b_lamar_started_climbing_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_stretch_started_climbing_ladder = ", b_stretch_started_climbing_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_player_started_climbing_ladder = ", b_player_started_climbing_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_player_reached_exit = ", b_player_reached_exit)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_stretch_reached_ladder = ", b_stretch_reached_ladder)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_lamar_is_closest_to_ladder = ", b_lamar_is_closest_to_ladder)
		
		REPEAT COUNT_OF(b_has_text_label_triggered) i
			IF b_has_text_label_triggered[i]
				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "b_has_text_label_triggered[", i, "] = ", b_has_text_label_triggered[i])
			ENDIF
		ENDREPEAT
			
		REPEAT COUNT_OF(s_conversation_peds.PedInfo) i
			CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_conversation_peds.PedInfo[", i, "].VoiceID = ", s_conversation_peds.PedInfo[i].VoiceID)
		ENDREPEAT
		
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_lamar.i_event = ", s_lamar.i_event)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_lamar.v_dest = ", s_lamar.v_dest)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_lamar.b_refresh_tasks = ", s_lamar.b_refresh_tasks)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_lamar.b_is_using_secondary_cover = ", s_lamar.b_is_using_secondary_cover)
		
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_stretch.i_event = ", s_stretch.i_event)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_stretch.v_dest = ", s_stretch.v_dest)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_stretch.b_refresh_tasks = ", s_stretch.b_refresh_tasks)
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_stretch.b_is_using_secondary_cover = ", s_stretch.b_is_using_secondary_cover)
	ENDPROC
#ENDIF

PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(obj)
		IF IS_ENTITY_ATTACHED(obj)
			DETACH_ENTITY(obj)
		ENDIF

		IF b_force_delete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(veh)
		IF NOT IS_ENTITY_DEAD(veh)
			STOP_SYNCHRONIZED_ENTITY_ANIM(veh, NORMAL_BLEND_OUT, TRUE)
		
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF b_force_delete
			DELETE_VEHICLE(veh)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PED(PED_INDEX &ped, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			STOP_SYNCHRONIZED_ENTITY_ANIM(ped, NORMAL_BLEND_OUT, TRUE)
		
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
			AND NOT IS_PED_GETTING_INTO_A_VEHICLE(ped)
				IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_PED(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(ped)
					DETACH_ENTITY(ped)
				ENDIF
				
				FREEZE_ENTITY_POSITION(ped, FALSE)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
				SET_ENTITY_COLLISION(ped, TRUE)
			ENDIF
			
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PED_KEEP_TASK(ped, TRUE)
			ENDIF
		ENDIF

		IF b_force_delete
			DELETE_PED(ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(ped)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Converts a rotation vector into a normalised direction vector (basically the direction an entity would move if it moved forward at the current rotation).
///    For example: a rotation of <<0.0, 0.0, 45.0>> gives a direction vector of <<-0.707107, 0.707107, 0.0>>
/// PARAMS:
///    v_rot - The rotation vector
/// RETURNS:
///    The direction vector
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR v_rot)
	RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

PROC CONVERGE_VALUE(FLOAT &val, FLOAT desired_val, FLOAT amount_to_converge, BOOL adjust_for_framerate = FALSE)
	IF val != desired_val
		FLOAT converge_amount_this_frame = amount_to_converge
		IF adjust_for_framerate
			converge_amount_this_frame = 0.0 +@ amount_to_converge
		ENDIF
	
		IF val - desired_val > converge_amount_this_frame
			val -= converge_amount_this_frame
		ELIF val - desired_val < -converge_amount_this_frame
			val += converge_amount_this_frame
		ELSE
			val = desired_val
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT i = 0
	REPEAT COUNT_OF(b_has_text_label_triggered) i
		b_has_text_label_triggered[i] = FALSE
	ENDREPEAT
ENDPROC

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC DO_FADE_IN_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the clip ammo in a ped's current weapon to the max value. This is useful for stopping peds from reloading at key moments.
PROC REFILL_PED_AMMO_CLIP(PED_INDEX &ped)
	WEAPON_TYPE weapon 
	GET_CURRENT_PED_WEAPON(ped, weapon)
	SET_AMMO_IN_CLIP(ped, weapon, GET_MAX_AMMO_IN_CLIP(ped, weapon))
ENDPROC

FUNC INT GET_NUM_WEAPONS_PED_OWNS(PED_INDEX ped)
	INT i_num_weapons = 0
	INT i
	
	IF NOT IS_PED_INJURED(ped)
		FOR i = 0 TO ENUM_TO_INT(NUM_WEAPONSLOTS) - 1
			WEAPON_SLOT e_slot = GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(i)
			
			IF e_slot != WEAPONSLOT_INVALID
				IF GET_PED_WEAPONTYPE_IN_SLOT(ped, e_slot) != WEAPONTYPE_INVALID
				AND GET_PED_WEAPONTYPE_IN_SLOT(ped, e_slot) != WEAPONTYPE_UNARMED
					i_num_weapons++
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

    RETURN i_num_weapons
ENDFUNC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &veh, FLOAT f_time)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, f_time - GET_TIME_POSITION_IN_RECORDING(veh))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL STORE_SHOP_OWNER()
	IF NOT DOES_ENTITY_EXIST(ped_shop_owner)
		PED_INDEX peds[10]
	
		GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), peds)
		
		INT i = 0
		REPEAT COUNT_OF(peds) i
			IF NOT IS_PED_INJURED(peds[i])
				IF GET_ENTITY_MODEL(peds[i]) = S_M_Y_AMMUCITY_01
					ped_shop_owner = peds[i]
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: triggers multiple bullet hits around the player at given intervals.
//AUTHOR: Ross Wallace
PROC DO_EXCITING_NEAR_BULLET_MISS_ON_PED(PED_INDEX pedToMiss, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, 
										 INT timeBetweenBullets = 60, FLOAT minXrange = -3.9, FLOAT maxXrange = -1.0, FLOAT minYRange = -2.9, FLOAT maxYrange = 3.9)
    //Fire bullets at the player as from the bad  guy...
    INT currentBulletTime = GET_GAME_TIMER()
    VECTOR bulletHit
    VECTOR bulletOrigin
    
    IF ((currentBulletTime - iControlTimer) > timeBetweenBullets)
        IF NOT IS_ENTITY_DEAD(pedToMiss)
        AND NOT IS_ENTITY_DEAD(sourceOfBullets)            
            bulletHit = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedToMiss, <<GET_RANDOM_FLOAT_IN_RANGE(minXrange, maxXrange), GET_RANDOM_FLOAT_IN_RANGE(minYRange, maxYrange), 0.0>>)
            bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
            GET_GROUND_Z_FOR_3D_COORD(bulletHit, bulletHit.z)           
            SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, bulletHit, 1)
            iControlTimer = currentBulletTime
			
			//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			//DRAW_DEBUG_SPHERE(bulletOrigin, 0.2)
			//DRAW_DEBUG_SPHERE(bulletHit, 0.2)
        ELSE
			//PRINTSTRING("SHOOTER DEAD")
			//PRINTNL()
		ENDIF
    ELSE
		//PRINTSTRING("TIMER NOT READY")
		//PRINTNL()
	ENDIF
ENDPROC

FUNC BOOL MOVE_LAST_CAR_AWAY_FROM_SHOP()
	VEHICLE_INDEX veh_last_car = GET_PLAYERS_LAST_VEHICLE()
	
	IF NOT IS_ENTITY_DEAD(veh_last_car)
		IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(veh_last_car))
		OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(veh_last_car))
			FLOAT f_dist_from_gun_shop = VDIST2(v_gun_shop_pos, GET_ENTITY_COORDS(veh_last_car))

			IF f_dist_from_gun_shop < 10000.0
				IF IS_ENTITY_IN_ANGLED_AREA(veh_last_car, <<17.350292, -1115.116943, 27.796766>>, <<15.640124, -1119.682861, 31.820522>>, 6.0)	//<<17.293930,-1115.083130,27.314375>>, <<15.971560,-1120.743164,31.831848>>, 9.250000)
				OR f_dist_from_gun_shop > 2500.0
					IF NOT IS_ENTITY_ON_SCREEN(veh_last_car)
						SET_ENTITY_COORDS(veh_last_car, v_gun_shop_pos)
						SET_ENTITY_HEADING(veh_last_car, 273.6953)
						SET_VEHICLE_ON_GROUND_PROPERLY(veh_last_car)
						
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF veh_last_car != veh_start_car
					veh_weapons_shop_car = veh_last_car
					SET_ENTITY_AS_MISSION_ENTITY(veh_weapons_shop_car, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED(MODEL_NAMES model, VECTOR v_pos, FLOAT f_heading, REL_GROUP_HASH group, 
INT i_health = 200, INT i_armour = 0, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, PED_TYPE e_ped_type = PEDTYPE_MISSION)
	PED_INDEX ped = CREATE_PED(e_ped_type, model, v_pos, f_heading)
	GIVE_WEAPON_TO_PED(ped, weapon, INFINITE_AMMO, TRUE)
	SET_PED_INFINITE_AMMO(ped, TRUE, weapon)
	SET_PED_MAX_HEALTH(ped, i_health)
	SET_ENTITY_HEALTH(ped, i_health)
	//Need to reset the armour first, in case the ped already has armour
	ADD_ARMOUR_TO_PED(ped, -GET_PED_ARMOUR(ped))
	ADD_ARMOUR_TO_PED(ped, i_armour)
	SET_PED_DIES_WHEN_INJURED(ped, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped, group)
	SET_PED_AS_ENEMY(ped, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(ped, TLR_NEVER_LOSE_TARGET)
	SET_PED_CONFIG_FLAG(ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(ped, PCF_DisableExplosionReactions, TRUE)
	SET_PED_CONFIG_FLAG(ped, PCF_DisableHurt, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_BLIND_FIRE_IN_COVER, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_CAN_INVESTIGATE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(ped, CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
	SET_PED_MONEY(ped, 0)
	
	iReplayCameraSpawnTimer = GET_GAME_TIMER() + 500	//Fix for bug 2222067
	
	RETURN ped
ENDFUNC

FUNC PED_INDEX CREATE_ENEMY_PED_IN_VEHICLE(MODEL_NAMES model, VEHICLE_INDEX veh, VEHICLE_SEAT e_seat, REL_GROUP_HASH group, 
										   INT i_health = 200, INT i_armour = 0, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, PED_TYPE e_ped_type = PEDTYPE_MISSION)	
	PED_INDEX ped
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		ped = CREATE_ENEMY_PED(model, GET_ENTITY_COORDS(veh) + <<0.0, 0.0, 3.0>>, 0.0, group, i_health, i_armour, weapon, e_ped_type)
		SET_PED_INTO_VEHICLE(ped, veh, e_seat)
	ENDIF
	
	RETURN ped
ENDFUNC

PROC SET_PED_COMBAT_PARAMS(PED_INDEX &ped, INT i_accuracy, COMBAT_MOVEMENT e_movement, COMBAT_ABILITY_LEVEL e_ability, COMBAT_RANGE e_range, 
						   COMBAT_TARGET_LOSS_RESPONSE e_tlr)
	SET_PED_COMBAT_MOVEMENT(ped, e_movement)
	SET_PED_COMBAT_ABILITY(ped, e_ability)
	SET_PED_COMBAT_RANGE(ped, e_range)
	SET_PED_ACCURACY(ped, i_accuracy)
	SET_PED_TARGET_LOSS_RESPONSE(ped, e_tlr)
ENDPROC

PROC SET_PED_PREFERRED_COVER_POINT(PED_INDEX &ped, COVERPOINT_INDEX &cover)
	IF NOT IS_PED_INJURED(ped)
		ITEMSET_INDEX item_cover = CREATE_ITEMSET(TRUE)
		ADD_TO_ITEMSET(cover, item_cover)
	
		SET_PED_PREFERRED_COVER_SET(ped, item_cover)
		DESTROY_ITEMSET(item_cover)
	ENDIF
ENDPROC

/// PURPOSE:
///    General function to set up a ped's personal cover point and defensive sphere.
PROC SET_PED_COVER_POINT_AND_SPHERE(PED_INDEX ped, COVERPOINT_INDEX &cover, VECTOR v_pos, FLOAT f_heading, FLOAT f_sphere_radius,
											COVERPOINT_USAGE cov_usage, COVERPOINT_HEIGHT cov_height, COVERPOINT_ARC cov_arc, BOOL bGotoCentreOfSphere = TRUE)
	IF NOT IS_PED_INJURED(ped)
		IF cover != NULL
			REMOVE_COVER_POINT(cover)
			cover = NULL
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(v_pos)
			cover = ADD_COVER_POINT(v_pos, f_heading, cov_usage, cov_height, cov_arc)
		ENDIF
		
		SET_PED_SPHERE_DEFENSIVE_AREA(ped, v_pos, f_sphere_radius, bGotoCentreOfSphere)
	ENDIF
ENDPROC

///NOTE: this needs to be rewritten: remove all the attribute settings.
PROC SET_PED_COMBAT_AI(PED_INDEX &ped, INT accuracy, COMBAT_MOVEMENT movement, COMBAT_ABILITY_LEVEL ability, COMBAT_RANGE range, COMBAT_ATTRIBUTE active_attributes,
					   VECTOR v_defensive_sphere_pos, FLOAT f_defensive_sphere_radius = 0.0)
	IF NOT IS_PED_INJURED(ped)
		SET_PED_COMBAT_MOVEMENT(ped, movement)
		SET_PED_COMBAT_ABILITY(ped, ability)
		SET_PED_COMBAT_RANGE(ped, range)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_USE_COVER, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_USE_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_DO_DRIVEBYS, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_CAN_INVESTIGATE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, active_attributes, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_ACCURACY(ped, accuracy)
		
		IF f_defensive_sphere_radius != 0.0
			SET_PED_SPHERE_DEFENSIVE_AREA(ped, v_defensive_sphere_pos, f_defensive_sphere_radius)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a ped as "ignored" in combat, this means they won't be targetable and buddies won't fire at them. 
///    This is useful for when creating peds in advance for shootouts, you can prevent buddies from firing at them until the right time.
/// PARAMS:
///    ped - The ped to ignore
///    b_ignore - If TRUE, the ped will be ignored.
///    rgh_neutral - If the ped is being ignored they will be placed in this group
///    rgh_enemy - If the ped is being treated as an enemy they will be placed in this group
PROC SET_PED_AS_IGNORED_IN_COMBAT(PED_INDEX ped, BOOL b_ignore, REL_GROUP_HASH &rgh_neutral, REL_GROUP_HASH &rgh_enemy)
	SET_PED_CAN_BE_TARGETTED(ped, NOT b_ignore)
	
	IF b_ignore
		SET_PED_RELATIONSHIP_GROUP_HASH(ped, rgh_neutral)
	ELSE
		SET_PED_RELATIONSHIP_GROUP_HASH(ped, rgh_enemy)
	ENDIF
ENDPROC

PROC INITIALISE_ENEMY_GROUP(ENEMY_PED &s_enemies[], STRING name)
	INT i = 0

	REPEAT COUNT_OF(s_enemies) i
		IF NOT IS_PED_INJURED(s_enemies[i].ped)
			TASK_STAND_STILL(s_enemies[i].ped, -1)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_enemies[i].ped, TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(s_enemies[i].ped)
			s_enemies[i].b_is_created = TRUE
			s_enemies[i].i_event = 0
			s_enemies[i].i_timer = 0
			s_enemies[i].i_bitset = 0
			
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_31 debug_name = name
				debug_name += i
				SET_PED_NAME_DEBUG(s_enemies[i].ped, debug_name)
			#ENDIF
			
			#IF NOT IS_DEBUG_BUILD
				name = name	//	to prevent an Unreferenced variable error
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(ENEMY_PED &s_enemies[], VECTOR v_pos_1, VECTOR v_pos_2, FLOAT f_width, BOOL bCheckDestinationVectors = FALSE)
	INT i = 0
	REPEAT COUNT_OF(s_enemies) i
		IF NOT IS_PED_INJURED(s_enemies[i].ped)
			IF IS_ENTITY_IN_ANGLED_AREA(s_enemies[i].ped, v_pos_1, v_pos_2, f_width)
			OR (bCheckDestinationVectors AND IS_POINT_IN_ANGLED_AREA(s_enemies[i].v_dest, v_pos_1, v_pos_2, f_width))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

PROC CLEAN_UP_ENEMY_PED(ENEMY_PED &s_enemy, BOOL b_reset_variables = FALSE)
	IF DOES_BLIP_EXIST(s_enemy.blip)
		REMOVE_BLIP(s_enemy.blip)
	ENDIF
	
	CLEANUP_AI_PED_BLIP(s_enemy.s_blip_data)
	
	IF NOT IS_PED_INJURED(s_enemy.ped)
		STOP_SYNCHRONIZED_ENTITY_ANIM(s_enemy.ped, NORMAL_BLEND_OUT, TRUE)
	ELSE
		//Increment the kill stat if this ped was killed by the player.
		IF DOES_ENTITY_EXIST(s_enemy.ped)
			ENTITY_INDEX entity_death_source = GET_PED_SOURCE_OF_DEATH(s_enemy.ped)
		
			IF DOES_ENTITY_EXIST(entity_death_source)
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(entity_death_source) = PLAYER_PED_ID()
					REL_GROUP_HASH e_enemy_hash = GET_PED_RELATIONSHIP_GROUP_HASH(s_enemy.ped)
				
					IF e_enemy_hash = rel_group_swat
					OR e_enemy_hash = RELGROUPHASH_COP
						INFORM_MISSION_STATS_OF_INCREMENT(LAM1_KILLS)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_PED_AS_NO_LONGER_NEEDED(s_enemy.ped)
	REMOVE_COVER_POINT(s_enemy.cover)
	
	IF b_reset_variables
		s_enemy.b_is_created = FALSE
		s_enemy.b_has_started_rope_jump = FALSE
		s_enemy.i_event = 0
		s_enemy.i_rope_event = 0
		s_enemy.i_timer = 0
		s_enemy.i_sync_scene = -1
	ENDIF
ENDPROC


PROC KILL_ENEMY_GROUP_INSTANTLY(ENEMY_PED &s_enemies[])
	INT i = 0

	REPEAT COUNT_OF(s_enemies) i
		s_enemies[i].b_is_created = TRUE
	
		IF NOT IS_PED_INJURED(s_enemies[i].ped)
			SET_ENTITY_HEALTH(s_enemies[i].ped, 0)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks to see if a group of enemies are dead. If they haven't been created yet, they will be treated as still alive.
/// PARAMS:
///    s_enemies - The enemy group
/// RETURNS:
///    BOOL indicating if the enemies are dead or not.
FUNC BOOL IS_ENEMY_GROUP_DEAD(ENEMY_PED &s_enemies[])
	IF s_enemies[0].b_is_created
		INT i = 0
		REPEAT COUNT_OF(s_enemies) i
			IF NOT IS_PED_INJURED(s_enemies[i].ped)
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ELSE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUM_ENEMIES_ALIVE_IN_GROUP(ENEMY_PED &s_enemies[])
	INT i_num_alive = 0
	INT i = 0
	
	REPEAT COUNT_OF(s_enemies) i
		IF s_enemies[i].b_is_created
			IF NOT IS_PED_INJURED(s_enemies[i].ped)
				i_num_alive++
			ENDIF
		ELSE
			i_num_alive++
		ENDIF
	ENDREPEAT
	
	RETURN i_num_alive
ENDFUNC

///Returns the first alive ped in a group, or the player if every ped in the group is dead.
///This is useful for situations where a ped needs to target fire a group of enemies before attacking the player.
FUNC PED_INDEX GET_NEXT_TARGET_FROM_ENEMY_GROUP(ENEMY_PED &s_enemies[])
	INT i = 0

	REPEAT COUNT_OF(s_enemies) i
		IF NOT IS_PED_INJURED(s_enemies[i].ped)
			RETURN s_enemies[i].ped
		ENDIF
	ENDREPEAT

	RETURN PLAYER_PED_ID()
ENDFUNC

FUNC BOOL ARE_PEDS_IN_ANGLED_AREA(PED_INDEX ped_1, PED_INDEX ped_2, PED_INDEX ped_3, VECTOR v_pos_1, VECTOR v_pos_2, FLOAT f_width)
	IF NOT IS_PED_INJURED(ped_1)
		IF IS_ENTITY_IN_ANGLED_AREA(ped_1, v_pos_1, v_pos_2, f_width, FALSE, TRUE, TM_ON_FOOT)
			RETURN TRUE
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(ped_2)
		IF IS_ENTITY_IN_ANGLED_AREA(ped_2, v_pos_1, v_pos_2, f_width, FALSE, TRUE, TM_ON_FOOT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(ped_3)
		IF IS_ENTITY_IN_ANGLED_AREA(ped_3, v_pos_1, v_pos_2, f_width, FALSE, TRUE, TM_ON_FOOT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// Tells a ped to go to a location (directly or using navmesh) and then go into combat.
PROC SEQ_GO_TO_COORD_THEN_COMBAT(PED_INDEX ped, VECTOR v_destination, FLOAT move_speed, INT i_time = DEFAULT_TIME_BEFORE_WARP, BOOL b_use_navmesh = TRUE, 
					 			 FLOAT f_radius = DEFAULT_NAVMESH_RADIUS, ENAV_SCRIPT_FLAGS nav_flags = ENAV_DEFAULT)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq
			
		OPEN_SEQUENCE_TASK(seq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
		
			IF b_use_navmesh
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, v_destination, move_speed, i_time, f_radius, nav_flags)
			ELSE
				TASK_GO_STRAIGHT_TO_COORD(NULL, v_destination, move_speed, i_time)
			ENDIF
			
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(ped, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF								  
ENDPROC

/// PURPOSE:
///    Tells a ped to go to cover while aiming at either a coord or a given entity, and then go into general combat.
PROC SEQ_GO_TO_COVER_WHILE_AIMING_THEN_COMBAT(PED_INDEX ped, VECTOR v_destination, FLOAT move_state, INT i_time_spent_in_cover, BOOL b_shoot, VECTOR v_aim_pos,
								  			  FLOAT f_target_distance = 0.5, FLOAT f_slow_distance = 4.0, BOOL b_use_navmesh = TRUE, ENTITY_INDEX entity_aim = NULL)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq
			
		OPEN_SEQUENCE_TASK(seq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			IF IS_ENTITY_DEAD(entity_aim)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, v_destination, v_aim_pos, move_state, b_shoot, f_target_distance, f_slow_distance, b_use_navmesh)
				TASK_SEEK_COVER_TO_COORDS(NULL, v_destination, v_aim_pos, i_time_spent_in_cover)
			ELSE
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, v_destination, entity_aim, move_state, b_shoot, f_target_distance, f_slow_distance, b_use_navmesh) 
				TASK_SEEK_COVER_TO_COORDS(NULL, v_destination, GET_ENTITY_COORDS(entity_aim), i_time_spent_in_cover)
			ENDIF
			
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50.0)
		CLOSE_SEQUENCE_TASK(seq)

		TASK_PERFORM_SEQUENCE(ped, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs a generic sequence where a ped will go to a coord while aiming at something (either a coord or entity), then transition into combat
PROC SEQ_GO_TO_COORD_WHILE_AIMING_THEN_COMBAT(PED_INDEX ped, VECTOR v_destination, FLOAT move_speed, VECTOR v_aim_pos, ENTITY_INDEX entity_aim = NULL, BOOL b_shoot = TRUE,
											 FLOAT f_target_dist = 0.5, FLOAT f_slow_dist = 0.5, BOOL b_use_navmesh = TRUE, FLOAT f_combat_range = 100.0)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq
			
		OPEN_SEQUENCE_TASK(seq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
		
			IF IS_ENTITY_DEAD(entity_aim)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, v_destination, v_aim_pos, move_speed, b_shoot, f_target_dist, f_slow_dist, b_use_navmesh)
			ELSE
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, v_destination, entity_aim, move_speed, b_shoot, f_target_dist, f_slow_dist, b_use_navmesh) 
			ENDIF

			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, f_combat_range)
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(ped, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to go to cover while aiming at either a coord or a given entity.
PROC SEQ_GO_TO_COVER_WHILE_AIMING(PED_INDEX ped, VECTOR v_destination, FLOAT move_speed, INT i_time_spent_in_cover, BOOL b_shoot, VECTOR v_aim_pos,
								  FLOAT f_target_distance = 0.5, FLOAT f_slow_distance = 4.0, BOOL b_use_navmesh = TRUE, ENTITY_INDEX entity_aim = NULL)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq
			
		OPEN_SEQUENCE_TASK(seq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
		
			IF IS_ENTITY_DEAD(entity_aim)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, v_destination, v_aim_pos, move_speed, b_shoot, f_target_distance, f_slow_distance, b_use_navmesh)
				TASK_SEEK_COVER_TO_COORDS(NULL, v_destination, v_aim_pos, i_time_spent_in_cover)
			ELSE
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, v_destination, entity_aim, move_speed, b_shoot, f_target_distance, f_slow_distance, b_use_navmesh) 
				TASK_SEEK_COVER_TO_COORDS(NULL, v_destination, GET_ENTITY_COORDS(entity_aim), i_time_spent_in_cover)
			ENDIF
		CLOSE_SEQUENCE_TASK(seq)
		
		TASK_PERFORM_SEQUENCE(ped, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to run to a location and then puts them into cover there
PROC SEQ_GO_TO_COVER(PED_INDEX ped, VECTOR v_destination, FLOAT move_speed, INT i_time_spent_in_cover, BOOL b_block_non_temp_events = TRUE, BOOL b_use_navmesh = TRUE, 
					 INT i_time = DEFAULT_TIME_BEFORE_WARP, FLOAT f_radius = DEFAULT_NAVMESH_RADIUS, ENAV_SCRIPT_FLAGS nav_flags = ENAV_DEFAULT)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq
			
		OPEN_SEQUENCE_TASK(seq)
			IF b_use_navmesh
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, v_destination, move_speed, i_time, f_radius, nav_flags)
			ELSE
				TASK_GO_STRAIGHT_TO_COORD(NULL, v_destination, move_speed, i_time)
			ENDIF
			
			TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, v_destination, i_time_spent_in_cover)
		CLOSE_SEQUENCE_TASK(seq)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, b_block_non_temp_events)
		TASK_PERFORM_SEQUENCE(ped, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC


/// PURPOSE:
///    Performs a generic sequence where a ped will go to a coord while aiming at something (either a coord or entity), then either continue aiming or shoot at the target.
PROC SEQ_GO_TO_COORD_WHILE_AIMING_THEN_AIM(PED_INDEX ped, VECTOR v_destination, FLOAT move_speed, VECTOR v_aim_pos, ENTITY_INDEX entity_aim = NULL, BOOL b_shoot = TRUE,
										   FLOAT f_target_dist = 0.5, FLOAT f_slow_dist = 0.5, BOOL b_use_navmesh = TRUE, BOOL b_block_non_temp_events = TRUE,
										   BOOL b_shoot_when_stopped = TRUE, FIRING_TYPE fire_type = FIRING_TYPE_RANDOM_BURSTS)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq
			
		OPEN_SEQUENCE_TASK(seq)
			IF IS_ENTITY_DEAD(entity_aim)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, v_destination, v_aim_pos, move_speed, b_shoot, f_target_dist, f_slow_dist, b_use_navmesh)
				
				IF b_shoot_when_stopped
					TASK_SHOOT_AT_COORD(NULL, v_aim_pos, -1, fire_type)
				ELSE
					TASK_AIM_GUN_AT_COORD(NULL, v_aim_pos, -1)
				ENDIF
			ELSE
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, v_destination, entity_aim, move_speed, b_shoot, f_target_dist, f_slow_dist, b_use_navmesh) 
				
				IF b_shoot_when_stopped
					TASK_SHOOT_AT_ENTITY(NULL, entity_aim, -1, fire_type)
				ELSE
					TASK_AIM_GUN_AT_ENTITY(NULL, entity_aim, -1)
				ENDIF
			ENDIF
		CLOSE_SEQUENCE_TASK(seq)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, b_block_non_temp_events)
		TASK_PERFORM_SEQUENCE(ped, seq)
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC

FUNC INT GET_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | 
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING)
ENDFUNC

//Grenade stat hasbeen removed.
PROC CALCULATE_GRENADE_MULTIKILL_STAT()
	INT i_current_num_grenade_kills, i_current_grenade_ammo, i_num_kills_for_last_grenade
	
	//If the player's grenade ammo goes down then they've thrown a grenade, check how many grenade kills have occurred before this point.
	i_current_grenade_ammo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_GRENADE)
	
	IF i_current_grenade_ammo < i_prev_grenade_ammo
		i_time_of_last_grenade_thrown = GET_GAME_TIMER()
		STAT_GET_INT(SP1_GRENADE_KILLS, i_num_grenade_kills_before_last_grenade)

		//This is a good time to increment the grenades thrown stat.
		//INFORM_MISSION_STATS_OF_INCREMENT(LAM1_GRENADES_THROWN)

		#IF IS_DEBUG_BUILD
			PRINTLN("GRENADE MULTI-KILL: i_num_grenade_kills_before_last_grenade ", i_num_grenade_kills_before_last_grenade)
		#ENDIF
	ENDIF
	
	//After a few seconds (enough time for the grenade to blow up) check again how many grenade kills have occurred.
	IF i_time_of_last_grenade_thrown != 0
		IF GET_GAME_TIMER() - i_time_of_last_grenade_thrown > 8000
			STAT_GET_INT(SP1_GRENADE_KILLS, i_current_num_grenade_kills)
			i_num_kills_for_last_grenade = i_current_num_grenade_kills - i_num_grenade_kills_before_last_grenade

			#IF IS_DEBUG_BUILD
				PRINTLN("GRENADE MULTI-KILL: i_num_kills_for_last_grenade ", i_num_kills_for_last_grenade)
			#ENDIF
			
			IF i_num_kills_for_last_grenade > 0
				//INFORM_MISSION_STATS_OF_INCREMENT(LAM1_MULTIKILL, i_num_kills_for_last_grenade)
				b_play_grenade_kill_dialogue = TRUE
			ENDIF
			
			i_time_of_last_grenade_thrown = 0
		ENDIF
	ENDIF
	
	i_prev_grenade_ammo = i_current_grenade_ammo
ENDPROC

PROC SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(BOOL bActive)
	INT i

	IF bActive
		IF cover_shootout[0] = NULL
			cover_shootout[0] = ADD_COVER_POINT(<<-611.5913, -1628.0930, 32.0105>>, 276.8619, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[1] = NULL
			cover_shootout[1] = ADD_COVER_POINT(<<-613.2361, -1633.8844, 32.0105>>, 276.5077, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[2] = NULL
			cover_shootout[2] = ADD_COVER_POINT(<<-607.7554, -1634.3656, 32.0303>>, 86.4219, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[3] = NULL
			cover_shootout[3] = ADD_COVER_POINT(<<-573.8100, -1611.3314, 26.0108>>, 103.7234, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[4] = NULL
			cover_shootout[4] = ADD_COVER_POINT(<<-589.1723, -1621.6759, 32.0106>>, 262.9417, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60, TRUE)
			i_cover_in_doorway_index = 4
		ENDIF
		
		IF cover_shootout[5] = NULL
			cover_shootout[5] = ADD_COVER_POINT(<<-602.7028, -1607.0862, 26.0108>>, 268.2972, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[6] = NULL
			cover_shootout[6] = ADD_COVER_POINT(<<-598.4998, -1607.2037, 26.0108>>, 81.2509, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[7] = NULL
			cover_shootout[7] = ADD_COVER_POINT(<<-607.9058, -1627.8060, 32.0106>>, 1.1009, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60, TRUE)
		ENDIF
		
		IF cover_shootout[8] = NULL
			cover_shootout[8] = ADD_COVER_POINT(<<-575.5695, -1599.4375, 27.1608>>, 83.5327, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[9] = NULL
			cover_shootout[9] = ADD_COVER_POINT(<<-587.3282, -1626.7545, 32.0106>>, 275.1829, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[10] = NULL
			cover_shootout[10] = ADD_COVER_POINT(<<-587.3992, -1625.9774, 32.0530>>, 275.1821, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[11] = NULL
			cover_shootout[11] = ADD_COVER_POINT(<<-576.1132, -1603.0667, 26.1021>>, 99.7303, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[12] = NULL
			cover_shootout[12] = ADD_COVER_POINT(<<-576.2527, -1605.1746, 26.0108>>, 90.3217, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[13] = NULL
			cover_shootout[13] = ADD_COVER_POINT(<<-562.7417, -1616.3969, 26.0110>>, 356.9735, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
		
		IF cover_shootout[14] = NULL
			cover_shootout[14] = ADD_COVER_POINT(<<-560.3149, -1611.9880, 26.0110>>, 356.9735, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120, TRUE)
		ENDIF
	ELSE
		REPEAT COUNT_OF(cover_shootout) i
			IF cover_shootout[i] != NULL
				REMOVE_COVER_POINT(cover_shootout[i])
				cover_shootout[i] = NULL
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC INITIALISE_WAREHOUSE_FIRE()
	s_warehouse_fire[0].v_pos = <<-589.7524, -1601.6586, 26.7179>>
	s_warehouse_fire[0].i_triggered_by = 0
	s_warehouse_fire[0].f_start_threshold = 0.1
	s_warehouse_fire[0].b_use_alternate_fire = TRUE
	
	s_warehouse_fire[1].v_pos = <<-589.6934, -1600.4192, 27.9798>>
	s_warehouse_fire[1].i_triggered_by = 0
	s_warehouse_fire[1].f_start_threshold = 0.7
	
	s_warehouse_fire[2].v_pos = <<-591.6019, -1601.3491, 26.7896>>
	s_warehouse_fire[2].i_triggered_by = 1
	s_warehouse_fire[2].f_start_threshold = 0.2
	
	s_warehouse_fire[3].v_pos = <<-591.2652, -1600.3153, 27.9268>>
	s_warehouse_fire[3].i_triggered_by = 2
	s_warehouse_fire[3].f_start_threshold = 0.4
	
	s_warehouse_fire[4].v_pos = <<-591.6990, -1602.3751, 26.7570>>
	s_warehouse_fire[4].i_triggered_by = 3
	s_warehouse_fire[4].f_start_threshold = 0.2
	
	s_warehouse_fire[5].v_pos = <<-589.5444, -1599.3856, 27.9523>>
	s_warehouse_fire[5].i_triggered_by = 4
	s_warehouse_fire[5].f_start_threshold = 0.2
	
	s_warehouse_fire[6].v_pos = <<-591.2639, -1603.7347, 26.8366>>
	s_warehouse_fire[6].i_triggered_by = 5
	s_warehouse_fire[6].f_start_threshold = 0.2
	
	s_warehouse_fire[7].v_pos = <<-591.1995, -1599.2474, 27.9672>>
	s_warehouse_fire[7].i_triggered_by = 6
	s_warehouse_fire[7].f_start_threshold = 0.2
	
	s_warehouse_fire[8].v_pos = <<-589.4444, -1598.2511, 27.9685>>
	s_warehouse_fire[8].i_triggered_by = 7
	s_warehouse_fire[8].f_start_threshold = 0.2
	
	s_warehouse_fire[9].v_pos = <<-591.0514, -1598.0443, 29.1302>>
	s_warehouse_fire[9].i_triggered_by = 8
	s_warehouse_fire[9].f_start_threshold = 0.2
	
	s_warehouse_fire[10].v_pos = <<-594.8785, -1603.1409, 27.0172>>
	s_warehouse_fire[10].i_triggered_by = 7
	s_warehouse_fire[10].f_start_threshold = 0.7
	
	s_warehouse_fire[11].v_pos = <<-596.2939, -1604.3033, 25.9614>>
	s_warehouse_fire[11].i_triggered_by = 10
	s_warehouse_fire[11].f_start_threshold = 0.7
	
	s_warehouse_fire[12].v_pos = <<-596.6607, -1602.7202, 26.0841>>
	s_warehouse_fire[12].i_triggered_by = 11
	s_warehouse_fire[12].f_start_threshold = 0.4
	s_warehouse_fire[12].b_use_alternate_fire = TRUE
	
	s_warehouse_fire[13].v_pos = <<-597.8643, -1601.6106, 26.0368>>
	s_warehouse_fire[13].i_triggered_by = 12
	s_warehouse_fire[13].f_start_threshold = 0.4
	
	s_warehouse_fire[14].v_pos = <<-597.0824, -1598.9607, 25.8901>>
	s_warehouse_fire[14].i_triggered_by = 13
	s_warehouse_fire[14].f_start_threshold = 0.7
	
	INT i = 0
	REPEAT COUNT_OF(s_warehouse_fire) i
		s_warehouse_fire[i].f_scale = 0.0
		s_warehouse_fire[i].i_timer = 0
	ENDREPEAT
ENDPROC

PROC INITIALISE_WAREHOUSE_FIRE_2()
	s_warehouse_fire_2[0].v_pos = <<-583.4873, -1601.0438, 26.7010>>
	s_warehouse_fire_2[0].i_triggered_by = 0
	s_warehouse_fire_2[0].f_start_threshold = 0.1
	s_warehouse_fire_2[0].b_use_alternate_fire = TRUE
	
	s_warehouse_fire_2[1].v_pos = <<-581.7587, -1601.2323, 26.6951>>
	s_warehouse_fire_2[1].i_triggered_by = 0
	s_warehouse_fire_2[1].f_start_threshold = 0.7
	
	s_warehouse_fire_2[2].v_pos = <<-582.0659, -1602.4412, 26.6555>>
	s_warehouse_fire_2[2].i_triggered_by = 1
	s_warehouse_fire_2[2].f_start_threshold = 0.7
	
	s_warehouse_fire_2[3].v_pos = <<-581.7474, -1600.0072, 27.6408>>
	s_warehouse_fire_2[3].i_triggered_by = 1
	s_warehouse_fire_2[3].f_start_threshold = 0.6
	
	s_warehouse_fire_2[4].v_pos = <<-581.9272, -1604.2128, 26.8116>>
	s_warehouse_fire_2[4].i_triggered_by = 2
	s_warehouse_fire_2[4].f_start_threshold = 0.7
	
	s_warehouse_fire_2[5].v_pos = <<-581.5225, -1598.9573, 27.6963>>
	s_warehouse_fire_2[5].i_triggered_by = 3
	s_warehouse_fire_2[5].f_start_threshold = 0.5
	
	s_warehouse_fire_2[6].v_pos = <<-580.6136, -1604.2695, 26.5611>>
	s_warehouse_fire_2[6].i_triggered_by = 4
	s_warehouse_fire_2[6].f_start_threshold = 0.8
	
	s_warehouse_fire_2[7].v_pos = <<-579.8723, -1600.1908, 28.7132>>
	s_warehouse_fire_2[7].i_triggered_by = 3
	s_warehouse_fire_2[7].f_start_threshold = 0.4
	
	s_warehouse_fire_2[8].v_pos = <<-579.7776, -1599.2750, 29.8253>>
	s_warehouse_fire_2[8].i_triggered_by = 5
	s_warehouse_fire_2[8].f_start_threshold = 0.4
	
	s_warehouse_fire_2[9].v_pos = <<-578.4561, -1600.3146, 27.7598>>
	s_warehouse_fire_2[9].i_triggered_by = 7
	s_warehouse_fire_2[9].f_start_threshold = 0.5
	
	s_warehouse_fire_2[10].v_pos = <<-578.2825, -1599.1992, 28.8074>>
	s_warehouse_fire_2[10].i_triggered_by = 8
	s_warehouse_fire_2[10].f_start_threshold = 0.4

	s_warehouse_fire_2[11].v_pos = <<-576.6120, -1600.4410, 26.6787>>
	s_warehouse_fire_2[11].i_triggered_by = 9
	s_warehouse_fire_2[11].f_start_threshold = 0.7
	s_warehouse_fire_2[11].b_use_alternate_fire = TRUE
	
	s_warehouse_fire_2[12].v_pos = <<-576.6265, -1599.3842, 27.5946>>
	s_warehouse_fire_2[12].i_triggered_by = 10
	s_warehouse_fire_2[12].f_start_threshold = 0.5
	
	s_warehouse_fire_2[13].v_pos = <<-576.6396, -1601.5969, 26.5078>>
	s_warehouse_fire_2[13].i_triggered_by = 11
	s_warehouse_fire_2[13].f_start_threshold = 0.5
	
	s_warehouse_fire_2[14].v_pos = <<-575.0870, -1599.9966, 26.6606>>
	s_warehouse_fire_2[14].i_triggered_by = 11
	s_warehouse_fire_2[14].f_start_threshold = 0.6
	
	s_warehouse_fire_2[15].v_pos = <<-576.9461, -1602.9968, 26.4396>>
	s_warehouse_fire_2[15].i_triggered_by = 13
	s_warehouse_fire_2[15].f_start_threshold = 0.7
	
	s_warehouse_fire_2[16].v_pos = <<-573.2653, -1600.1333, 26.6599>>
	s_warehouse_fire_2[16].i_triggered_by = 14
	s_warehouse_fire_2[16].f_start_threshold = 0.7
	
	s_warehouse_fire_2[17].v_pos = <<-577.1041, -1605.0720, 26.5755>>
	s_warehouse_fire_2[17].i_triggered_by = 15
	s_warehouse_fire_2[17].f_start_threshold = 0.8
	
	s_warehouse_fire_2[18].v_pos = <<-572.7047, -1601.7343, 26.5865>>
	s_warehouse_fire_2[18].i_triggered_by = 16
	s_warehouse_fire_2[18].f_start_threshold = 0.8
	
	INT i = 0
	REPEAT COUNT_OF(s_warehouse_fire_2) i
		s_warehouse_fire_2[i].f_scale = 0.0
		s_warehouse_fire_2[i].i_timer = 0
	ENDREPEAT
ENDPROC

PROC UPDATE_WAREHOUSE_FIRE(FIRE_DATA &s_fire_data[], INT &i_fire_counter, INT iDebrisEffectID)
	CONST_FLOAT MAX_FIRE_SIZE		1.8
	VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	BOOL b_player_is_on_fire = IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
	BOOL b_player_ragdoll = IS_PED_RAGDOLL(PLAYER_PED_ID())
	BOOL b_player_getting_up = IS_PED_GETTING_UP(PLAYER_PED_ID())

	REQUEST_PTFX_ASSET()
	
	IF HAS_PTFX_ASSET_LOADED()
		INT i = 0
		INT i_total_fire_elements = COUNT_OF(s_fire_data)
		INT i_num_fires_checked_this_frame = 0
		
		IF i_fire_counter >= i_total_fire_elements - 1
			i_fire_counter = 0
		ENDIF
		
		FLOAT fTotalScales = 0.0
		
		REPEAT COUNT_OF(s_fire_data) i
			IF i >= i_fire_counter
			AND i_num_fires_checked_this_frame < 4
				IF s_fire_data[i].f_scale = 0.0
					//Trigger the fire when the adjacent fire has reached a certain size.
					IF s_fire_data[s_fire_data[i].i_triggered_by].f_scale > s_fire_data[i].f_start_threshold
						IF s_fire_data[i].b_use_alternate_fire
							s_fire_data[i].f_scale = 0.01
						ELSE
							s_fire_data[i].f_scale = 0.1
						ENDIF
					ENDIF
				ELSE
					//TEXT_LABEL strScale = FLOAT_TO_STRING(s_fire_data[i].f_scale)
					//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					//DRAW_DEBUG_TEXT_ABOVE_COORDS(s_fire_data[i].v_pos, strScale, 1.0)
				
					IF s_fire_data[i].ptfx = NULL
						IF s_fire_data[i].b_use_alternate_fire
							s_fire_data[i].ptfx = START_PARTICLE_FX_LOOPED_AT_COORD("scr_lamar1_fire_spread1", s_fire_data[i].v_pos, <<0.0, 0.0, 0.0>>, s_fire_data[i].f_scale)
						ELSE
							s_fire_data[i].ptfx = START_PARTICLE_FX_LOOPED_AT_COORD("scr_lamar1_fire_spread", s_fire_data[i].v_pos, <<0.0, 0.0, 0.0>>, s_fire_data[i].f_scale)
						ENDIF
					ELSE		
						IF s_fire_data[i].f_scale < MAX_FIRE_SIZE //Don't need to set scale anymore once it reaches max: saves 200-300 excecution time on the profiler.
							s_fire_data[i].f_scale = CLAMP(s_fire_data[i].f_scale +@ 0.9, 0.0, MAX_FIRE_SIZE)
							SET_PARTICLE_FX_LOOPED_SCALE(s_fire_data[i].ptfx, s_fire_data[i].f_scale)
						ENDIF
					
						//If the player comes into contact with the fire, set them alight.
						IF s_fire_data[i].f_scale > 0.4
						AND ((VDIST2(s_fire_data[i].v_pos, v_player_pos) < 2.0 AND s_fire_data[i].f_scale > 0.9) OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), s_fire_data[i].v_pos + <<0.0, 0.0, 1.0>>, <<0.75,0.75,1.0>>))
						AND NOT b_player_is_on_fire
						AND NOT b_player_ragdoll
						AND NOT b_player_getting_up
							START_ENTITY_FIRE(PLAYER_PED_ID())
							i_fire_timer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
				
				i_fire_counter = i + 1
				i_num_fires_checked_this_frame++
			ENDIF
			
			fTotalScales += s_fire_data[i].f_scale
		ENDREPEAT
		
		//NG 1763587 - Add an extra effect of debris that increases as the fire grows.
		FLOAT fOverallProgress = fTotalScales / (TO_FLOAT(COUNT_OF(s_fire_data)) * MAX_FIRE_SIZE)
		VECTOR vDebrisStartPos = <<-578.854, -1601.3403, 27.8260>>
		
		IF iDebrisEffectID != 0
			vDebrisStartPos = <<-590.665, -1603.005, 27.950>>		
		ENDIF
		
		IF ptfxDebris[iDebrisEffectID] = NULL
			IF fOverallProgress > FIRE_DEBRIS_START_TIME
				ptfxDebris[iDebrisEffectID] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_env_agency3b_smoke", vDebrisStartPos, <<0.0, 0.0, 0.0>>, 1.0)
			ENDIF
		ELSE
			FLOAT fEvoRatio = (fOverallProgress - FIRE_DEBRIS_START_TIME) / (1.0 - FIRE_DEBRIS_START_TIME)
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxDebris[iDebrisEffectID], "smoke", 1.0 - fEvoRatio)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxDebris[iDebrisEffectID], "cinder", 1.0 - fEvoRatio)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxDebris[iDebrisEffectID], "debris", 1.0 - fEvoRatio)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxDebris[iDebrisEffectID], "speed", 1.0)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxDebris[iDebrisEffectID], "smoke_strength", 1.0)
			
			#IF IS_DEBUG_BUILD
				DRAW_FLOAT_TO_DEBUG_DISPLAY(fEvoRatio, "Debris: ")
			#ENDIF
		ENDIF
	ENDIF
	
	IF i_fire_timer != 0
		IF GET_GAME_TIMER() - i_fire_timer > 2000
		AND b_player_is_on_fire
			STOP_ENTITY_FIRE(PLAYER_PED_ID())
			i_fire_timer = 0
		ENDIF
	ENDIF
ENDPROC

///Creates a rope and initialises the rope struct info (the extra info is used for attaching ropes to rappelling peds, it's not needed for other uses).
PROC INITIALISE_ROPE(ROPE_DATA &s_rope, VECTOR v_pos, VECTOR v_rot, FLOAT f_length, PHYSICS_ROPE_TYPE physics_type)
	s_rope.rope = ADD_ROPE(v_pos, v_rot, f_length, physics_type)
	s_rope.f_length = f_length
	s_rope.i_num_segments = GET_ROPE_VERTEX_COUNT(s_rope.rope)
	s_rope.f_length_per_segment = s_rope.f_length / TO_FLOAT(s_rope.i_num_segments)
ENDPROC

PROC ATTACH_ROPE_TO_SINGLE_POINT(ROPE_DATA &s_rope, VECTOR v_pos)
	INT i = 0
	REPEAT s_rope.i_num_segments i
		IF i = 0
			PIN_ROPE_VERTEX(s_rope.rope, i, v_pos)
		ELSE
			UNPIN_ROPE_VERTEX(s_rope.rope, i)
		ENDIF
	ENDREPEAT
ENDPROC

PROC ATTACH_ROPE_TO_RAPPELLING_PED(ROPE_DATA &s_rope, PED_INDEX ped, VECTOR v_rope_start_pos)
	VECTOR v_hand_pos, v_prev_vertex_pos, v_next_vertex_pos, v_dir_to_hand
	FLOAT f_horizontal_dist_to_hand_squared, f_z_dist_from_prev_vertex
	INT i_current_vertex, j
	BOOL b_has_rope_reached_hand

	IF NOT IS_PED_INJURED(ped)
		v_hand_pos = GET_PED_BONE_COORDS(ped, BONETAG_L_HAND, <<0,0,0>>)
		
		//Calculate the directional vector from the rope start to the ped's hand
		v_dir_to_hand = (v_hand_pos - v_rope_start_pos) / VMAG(v_hand_pos - v_rope_start_pos)
		
		//Each vertex before the hand is pinned such that they form a line leading to the hand
		i_current_vertex = 0
		b_has_rope_reached_hand = FALSE
		v_prev_vertex_pos = <<0.0, 0.0, 0.0>>
		
		WHILE i_current_vertex < s_rope.i_num_segments AND NOT b_has_rope_reached_hand
			IF i_current_vertex = 0
				v_prev_vertex_pos = v_rope_start_pos
			ELSE
				v_prev_vertex_pos = v_rope_start_pos + (v_dir_to_hand * (s_rope.f_length_per_segment * i_current_vertex))
			ENDIF
			
			PIN_ROPE_VERTEX(s_rope.rope, i_current_vertex, v_prev_vertex_pos)
			
			IF VDIST2(v_prev_vertex_pos, v_hand_pos) < (s_rope.f_length_per_segment * s_rope.f_length_per_segment)
				b_has_rope_reached_hand = TRUE
			ENDIF
			
			i_current_vertex++
		ENDWHILE
		
		//The next vertex is positioned directly under the hand (x,y coords are the same as the hand x,y).
		//The segment length must stay consistent, so z may vary. We know the segment length and can find the horizontal distance between the previous vertex
		//and the hand.
		//Using these values we can work out where z should be for the next vertex.
		v_next_vertex_pos = v_hand_pos
		f_horizontal_dist_to_hand_squared = VDIST2(v_prev_vertex_pos * <<1.0,1.0,0.0>>, v_hand_pos * <<1.0,1.0,0.0>>) 
		f_z_dist_from_prev_vertex = SQRT((s_rope.f_length_per_segment * s_rope.f_length_per_segment) - (f_horizontal_dist_to_hand_squared))								
		v_next_vertex_pos.z = v_prev_vertex_pos.z - f_z_dist_from_prev_vertex
		PIN_ROPE_VERTEX(s_rope.rope, i_current_vertex, v_next_vertex_pos)
		i_current_vertex++
		
		//The rest of the vertices are set to hang straight down
		j = 1
		WHILE i_current_vertex < s_rope.i_num_segments
			PIN_ROPE_VERTEX(s_rope.rope, i_current_vertex, v_next_vertex_pos - <<0.0, 0.0, s_rope.f_length_per_segment * j>>)
			i_current_vertex++
			j++
		ENDWHILE
	ENDIF
ENDPROC

FUNC VECTOR GET_ROPE_OFFSET_FROM_CHOPPER(VEHICLE_INDEX veh_chopper, BOOL b_is_left_side)
	IF IS_VEHICLE_DRIVEABLE(veh_chopper)
		IF b_is_left_side
			RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_chopper, <<-0.75, 0.0, 1.25>>)
		ELSE
			RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_chopper, <<0.75, 0.0, 1.25>>)
		ENDIF
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Checks whether a rappelling ped has reached the end of the rope.
///    NOTE: This will also return true if the ped was knocked off the rope (e.g. by shooting them) and they're still alive.
FUNC BOOL HAS_PED_REACHED_END_OF_ROPE(ROPE_DATA &s_rope, PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		VECTOR v_hand_pos, v_rope_end_pos, v_ground_pos
		v_hand_pos = GET_PED_BONE_COORDS(ped, BONETAG_L_HAND, <<0.0, 0.0, 0.0>>)
		v_rope_end_pos = GET_ROPE_VERTEX_COORD(s_rope.rope, s_rope.i_num_segments - 1)
		v_ground_pos = v_hand_pos
		GET_GROUND_Z_FOR_3D_COORD(v_hand_pos, v_ground_pos.z)
		
		IF v_hand_pos.z < v_rope_end_pos.z + 1.0
		OR v_hand_pos.z < v_ground_pos.z + 2.0
		OR IS_PED_RAGDOLL(ped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE_RAPPELLING_ENEMY(ENEMY_PED &enemy, VECTOR v_rope_pos)
	IF NOT IS_PED_INJURED(enemy.ped)
		FLOAT f_ground_z = 0.0
		GET_GROUND_Z_FOR_3D_COORD(v_rope_pos - <<0.0, 0.0, 10.0>>, f_ground_z)
					
		INITIALISE_ROPE(enemy.s_rope, v_rope_pos, <<0.0, 90.0, 0.0>>, v_rope_pos.z - f_ground_z - 0.2, PHYSICS_ROPE_DEFAULT)
		PIN_ROPE_VERTEX(enemy.s_rope.rope, 0, v_rope_pos)
		
		SET_PED_GRAVITY(enemy.ped, FALSE)
		TASK_PLAY_ANIM(enemy.ped, str_swat_anims, "rappel_intro_player", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.0)	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemy.ped, TRUE)
		
		enemy.i_rope_event = 0
		enemy.b_has_started_rope_jump = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Generic function for handling a ped rappelling down a static rope.
/// PARAMS:
///    enemy - 
///    v_rope_pos - 
/// RETURNS:
///    
FUNC BOOL MANAGE_RAPPELLING_ENEMY(ENEMY_PED &enemy)
	//Rope position should always be constant
	VECTOR v_rope_pos = GET_ROPE_VERTEX_COORD(enemy.s_rope.rope, 0)

	IF NOT IS_PED_INJURED(enemy.ped)
		SWITCH enemy.i_rope_event
			CASE 0
				IF IS_ENTITY_PLAYING_ANIM(enemy.ped, str_swat_anims, "rappel_intro_player")
					ATTACH_ROPE_TO_RAPPELLING_PED(enemy.s_rope, enemy.ped, v_rope_pos)
					
					IF enemy.b_has_started_rope_jump
						SET_ENTITY_ANIM_SPEED(enemy.ped, str_swat_anims, "rappel_intro_player", 1.0)
						enemy.i_rope_event++
					ELSE
						SET_ENTITY_ANIM_SPEED(enemy.ped, str_swat_anims, "rappel_intro_player", 0.0)
					ENDIF
				ELSE
					ATTACH_ROPE_TO_SINGLE_POINT(enemy.s_rope, v_rope_pos)
				ENDIF
				
				//Kill the enemy instantly if they're shot before they jump
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(enemy.ped, PLAYER_PED_ID())
					SET_PED_GRAVITY(enemy.ped, TRUE)
					SET_PED_TO_RAGDOLL(enemy.ped, 250, 1000, TASK_NM_BALANCE)
					SET_ENTITY_HEALTH(enemy.ped, 0)
				ENDIF
			BREAK
			
			CASE 1
				IF IS_ENTITY_PLAYING_ANIM(enemy.ped, str_swat_anims, "rappel_intro_player")
					ATTACH_ROPE_TO_RAPPELLING_PED(enemy.s_rope, enemy.ped, v_rope_pos)
				
					IF GET_ENTITY_ANIM_CURRENT_TIME(enemy.ped, str_swat_anims, "rappel_intro_player") > 0.9
						SET_ENTITY_ANIM_SPEED(enemy.ped, str_swat_anims, "rappel_intro_player", 0.0)
						SET_ENTITY_VELOCITY(enemy.ped, <<0.0, 0.0, -8.0>>)
						
						enemy.i_rope_event++
					ELSE
						//Kill the enemy instantly if they're shot in the early jump stages
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(enemy.ped, PLAYER_PED_ID())
							SET_PED_GRAVITY(enemy.ped, TRUE)
							SET_PED_TO_RAGDOLL(enemy.ped, 250, 1000, TASK_NM_BALANCE)
							SET_ENTITY_HEALTH(enemy.ped, 0)
						ENDIF
					ENDIF
				ELSE
					//If the ped was knocked out of the anim (e.g. by shooting them) just progress to the final checks.
					enemy.i_rope_event++
				ENDIF
			BREAK
			
			CASE 2
				IF HAS_PED_REACHED_END_OF_ROPE(enemy.s_rope, enemy.ped)
					ATTACH_ROPE_TO_SINGLE_POINT(enemy.s_rope, v_rope_pos)
					enemy.i_rope_event = 100
					RETURN TRUE
				ELSE
					ATTACH_ROPE_TO_RAPPELLING_PED(enemy.s_rope, enemy.ped, v_rope_pos)
					SET_ENTITY_VELOCITY(enemy.ped, <<0.0, 0.0, -8.0>>)
				ENDIF
			BREAK
			
			CASE 100 //Ped has disconnected from the rope, just return TRUE
				RETURN TRUE
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			IF b_debug_draw_rappel_state
				TEXT_LABEL str_index = ""
				str_index += enemy.i_rope_event
				
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				DRAW_DEBUG_TEXT(str_index, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(enemy.ped, <<0.0, 0.0, 2.0>>))
			ENDIF
		#ENDIF
	ELSE
		//If the ped was killed before they left the rope, make sure it's attached correctly
		IF enemy.i_rope_event < 100
			ATTACH_ROPE_TO_SINGLE_POINT(enemy.s_rope, v_rope_pos)
			enemy.i_rope_event = 100
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Tells an enemy that is currently waiting to jump from a chopper to start jumping. This allows the script to control when peds jump 
///    (e.g. at a particular point in a recording).
/// PARAMS:
///    enemy - 
PROC TELL_RAPPELLING_ENEMY_TO_JUMP(ENEMY_PED &enemy)
	enemy.b_has_started_rope_jump = TRUE
ENDPROC

FUNC BOOL IS_PLAYER_USING_A_VENDING_MACHINE()
	IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@sprunk", "plyr_buy_drink_pt1")
	OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "mini@sprunk", "plyr_buy_drink_pt2")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the buddy behaviour inside the shop, they walk to a specific location then look around at the weapons.
/// PARAMS:
///    s_buddy - The buddy struct
///    v_destination - the place they will stand
///    f_dest_heading - the heading they'll face when standing
///    v_look_at_pos - the place they'll look at
PROC UPDATE_BUDDY_INSIDE_SHOP(LAMAR_DATA &s_buddy, VECTOR v_destination, FLOAT f_dest_heading, STRING str_idle_anim)
	IF NOT IS_PED_INJURED(s_buddy.ped)
		FLOAT f_heading = GET_ENTITY_HEADING(s_buddy.ped)
		FLOAT f_heading_diff = ABSF(f_heading - f_dest_heading)
		
		IF f_heading_diff > 180.0
			f_heading_diff = ABSF(f_heading_diff - 360.0)
		ENDIF
	
		BOOL b_is_playing_one_shot_anim = GET_SCRIPT_TASK_STATUS(s_buddy.ped, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK AND NOT IS_ENTITY_PLAYING_ANIM(s_buddy.ped, str_shop_anims, str_idle_anim)
		BOOL b_buddy_reached_shop = VDIST(GET_ENTITY_COORDS(s_buddy.ped), v_destination) < 1.4 AND (f_heading_diff < 20.0 OR b_is_playing_one_shot_anim)
		
		REQUEST_ANIM_DICT(str_shop_anims)
		
		IF HAS_ANIM_DICT_LOADED(str_shop_anims)
			IF NOT b_buddy_reached_shop
				TASK_CLEAR_LOOK_AT(s_buddy.ped)
				
				IF GET_SCRIPT_TASK_STATUS(s_buddy.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					SEQUENCE_INDEX seq
					OPEN_SEQUENCE_TASK(seq)
						IF IS_VEHICLE_DRIVEABLE(veh_start_car)
							IF IS_PED_IN_VEHICLE(s_buddy.ped, veh_start_car) //Fix for #329846 - stagger the starts of the buddies when the player parks by the shop.
								IF s_buddy.ped = s_lamar.ped
									TASK_PAUSE(NULL, 500)
								ENDIF
								TASK_LEAVE_VEHICLE(NULL, veh_start_car)
							ENDIF
						ENDIF
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, v_destination, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.25, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION)//, f_dest_heading)
						TASK_ACHIEVE_HEADING(NULL, f_dest_heading, 0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_buddy.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
			ELSE
				//Play an idle anim here, if they're not playing a bespoke anim while talking
				IF GET_SCRIPT_TASK_STATUS(s_buddy.ped, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(s_buddy.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(s_buddy.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
					TASK_PLAY_ANIM(s_buddy.ped, str_shop_anims, str_idle_anim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///The shop anims can only play at a certain point in the shop idle, otherwise there are blending issues. If the player is browsing
///then the peds aren't visible so it's okay to the play the anims any time.
FUNC BOOL IS_IT_SAFE_TO_PLAY_SHOP_ANIMS(BOOL b_is_player_on_shop_menu)
	IF NOT IS_PED_INJURED(s_lamar.ped)
	AND NOT IS_PED_INJURED(s_stretch.ped)
		IF b_is_player_on_shop_menu
			RETURN TRUE
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(s_lamar.ped, str_shop_anims, "idle_lamar")
			AND IS_ENTITY_PLAYING_ANIM(s_stretch.ped, str_shop_anims, "idle_stretch")
				FLOAT f_lamar_idle_time = GET_ENTITY_ANIM_CURRENT_TIME(s_lamar.ped, str_shop_anims, "idle_lamar")
				FLOAT f_stretch_idle_time = GET_ENTITY_ANIM_CURRENT_TIME(s_stretch.ped, str_shop_anims, "idle_stretch")
			
				IF (f_lamar_idle_time >= 0.885 OR f_lamar_idle_time <= 0.4)
				AND (f_stretch_idle_time >= 0.885 OR f_stretch_idle_time <= 0.6)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if it's safe to play the in-shop anims for Lamar and Stretch, then plays them.
///    The anims are only allowed to play if Lamar and Stretch are in the right positions. If the player
///    is browsing the shop the buddies can be warped off-camera if needed.
PROC PLAY_BUDDY_SHOP_ANIMS(STRING str_lamar_anim, STRING str_stretch_anim, BOOL b_is_player_on_shop_menu)
	SEQUENCE_INDEX seq

	IF b_is_player_on_shop_menu
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			SET_ENTITY_COORDS(s_lamar.ped, v_lamar_pos_in_shop)
			SET_ENTITY_HEADING(s_lamar.ped, f_lamar_heading_in_shop)
			
			SET_ENTITY_COORDS(s_stretch.ped, v_stretch_pos_in_shop)
			SET_ENTITY_HEADING(s_stretch.ped, f_stretch_heading_in_shop)
		ENDIF
	ENDIF
		
	OPEN_SEQUENCE_TASK(seq)
		TASK_PLAY_ANIM(NULL, str_shop_anims, str_lamar_anim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
		TASK_PLAY_ANIM(NULL, str_shop_anims, "idle_lamar", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
	CLOSE_SEQUENCE_TASK(seq)
	TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
	CLEAR_SEQUENCE_TASK(seq)
	
	OPEN_SEQUENCE_TASK(seq)
		TASK_PLAY_ANIM(NULL, str_shop_anims, str_stretch_anim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
		TASK_PLAY_ANIM(NULL, str_shop_anims, "idle_stretch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
	CLOSE_SEQUENCE_TASK(seq)
	TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
	CLEAR_SEQUENCE_TASK(seq)
ENDPROC

/// PURPOSE:
///    There are three slots being used for random swat ped speakers. This adds a ped into a currently free slot. Once a ped dies the slot is cleared.
///    If i_speaker is set to something other than -1 it'll try to place the ped in the given slot.
PROC ADD_SWAT_PED_FOR_DIALOGUE(PED_INDEX ped, INT i_speaker = -1, BOOL b_force_add = FALSE)
	IF NOT IS_PED_INJURED(ped)
		IF s_conversation_peds.PedInfo[SWAT_SPEAKER_1].Index != ped
		AND s_conversation_peds.PedInfo[SWAT_SPEAKER_2].Index != ped
		AND s_conversation_peds.PedInfo[SWAT_SPEAKER_3].Index != ped
			IF i_speaker = -1
				IF NOT s_conversation_peds.PedInfo[SWAT_SPEAKER_1].ActiveInConversation
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_1, ped, "Lam1Swat1")
				ELIF NOT s_conversation_peds.PedInfo[SWAT_SPEAKER_2].ActiveInConversation
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_2, ped, "Lam1Swat2")
				ELIF NOT s_conversation_peds.PedInfo[SWAT_SPEAKER_3].ActiveInConversation
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_3, ped, "Lam1Swat3")
				ENDIF	
			ELSE
				IF NOT s_conversation_peds.PedInfo[i_speaker].ActiveInConversation
				OR b_force_add
					IF i_speaker = SWAT_SPEAKER_1
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, i_speaker, ped, "Lam1Swat1")
					ELIF i_speaker = SWAT_SPEAKER_2
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, i_speaker, ped, "Lam1Swat2")
					ELIF i_speaker = SWAT_SPEAKER_3
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, i_speaker, ped, "Lam1Swat3")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Kills off buddies if the player launches explosive weapons at them (e.g. grenades). This has to be handled in script due to the
///    high health of the buddies.
PROC HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(PED_INDEX &pedBuddy, INT &iBuddyHealthTracker)
	IF NOT IS_PED_INJURED(pedBuddy)
		BOOL bSafeToGrabHealth = TRUE
	
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBuddy, PLAYER_PED_ID(), TRUE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedBuddy)
			
			IF IS_PED_RAGDOLL(pedBuddy)
				IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedBuddy), 5.0)
					IF NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_GAS_CANISTER, GET_ENTITY_COORDS(pedBuddy), 5.0)
						SET_RAGDOLL_BLOCKING_FLAGS(pedBuddy, RBF_NONE)
						SET_ENTITY_HEALTH(pedBuddy, 0)
						bSafeToGrabHealth = FALSE
					ENDIF
				ELIF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					//If the buddy was smashed into by the player's car then kill them.
					IF iBuddyHealthTracker - GET_ENTITY_HEALTH(pedBuddy) > 100
						SET_RAGDOLL_BLOCKING_FLAGS(pedBuddy, RBF_NONE)
						SET_ENTITY_HEALTH(pedBuddy, 0)
						bSafeToGrabHealth = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bSafeToGrabHealth
			iBuddyHealthTracker = GET_ENTITY_HEALTH(pedBuddy)
		ENDIF
	ENDIF
ENDPROC

PROC TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(PED_INDEX &ped, INT &i_synced_scene, INT &i_timer, VECTOR v_cover_pos, FLOAT f_phase_to_switch = 1.0, FLOAT f_blend_duration = 0.0,
												BOOL b_force_cover_direction = FALSE, BOOL b_cover_direction_is_left = FALSE, COVERPOINT_INDEX cover_index = NULL,
												FLOAT fBlendOutData = NORMAL_BLEND_OUT)
	IF NOT IS_PED_INJURED(ped)
		SEQUENCE_INDEX seq

		IF IS_SYNCHRONIZED_SCENE_RUNNING(i_synced_scene)
			i_timer = GET_GAME_TIMER()
		
			IF GET_SYNCHRONIZED_SCENE_PHASE(i_synced_scene) >= f_phase_to_switch
				STOP_SYNCHRONIZED_ENTITY_ANIM(ped, fBlendOutData, TRUE)
				i_synced_scene = -1
			
				OPEN_SEQUENCE_TASK(seq)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					IF VDIST2(GET_ENTITY_COORDS(ped), v_cover_pos) < 2.0
						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, v_cover_pos, 1500, FALSE, f_blend_duration, b_force_cover_direction, b_cover_direction_is_left, cover_index)
					ENDIF
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50.0)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(ped, seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				IF b_force_cover_direction
					SET_PED_CONFIG_FLAG(ped, PCF_BlockPedFromTurningInCover, TRUE)
				ENDIF
			ENDIF
		ELSE
			//If the ped was knocked out of the scene just go into combat.
			IF GET_GAME_TIMER() - i_timer > 2500
				IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
				AND NOT IS_PED_IN_COMBAT(ped)
					STOP_SYNCHRONIZED_ENTITY_ANIM(ped, fBlendOutData, TRUE)
					i_synced_scene = -1
				
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped, 50.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRANSITION_FROM_MOCAP_TO_COVER(PED_INDEX &ped, INT &i_synced_scene, VECTOR v_cover_pos, FLOAT f_phase_to_switch = 1.0, 
									FLOAT f_blend_duration = 0.0, BOOL b_force_cover_direction = FALSE, BOOL b_cover_direction_is_left = FALSE, COVERPOINT_INDEX cover_index = NULL,
									FLOAT fBlendOutData = NORMAL_BLEND_OUT)
	IF NOT IS_PED_INJURED(ped)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(i_synced_scene) 
			IF GET_SYNCHRONIZED_SCENE_PHASE(i_synced_scene) >= f_phase_to_switch
				STOP_SYNCHRONIZED_ENTITY_ANIM(ped, fBlendOutData, TRUE)
				i_synced_scene = -1
			
				TASK_PUT_PED_DIRECTLY_INTO_COVER(ped, v_cover_pos, -1, FALSE, f_blend_duration, b_force_cover_direction, b_cover_direction_is_left, cover_index)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
				
				IF b_force_cover_direction
					SET_PED_CONFIG_FLAG(ped, PCF_BlockPedFromTurningInCover, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF i_synced_scene != -1
				i_synced_scene = -1
				
				IF VDIST2(GET_ENTITY_COORDS(ped), v_cover_pos) < 4.0
					TASK_PUT_PED_DIRECTLY_INTO_COVER(ped, v_cover_pos, -1, FALSE, f_blend_duration, b_force_cover_direction, b_cover_direction_is_left, cover_index, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRANSITION_FROM_MOCAP_TO_COMBAT(PED_INDEX &ped, INT &i_synced_scene, FLOAT f_phase_to_switch = 0.99, BOOL b_instant_blend_to_aim = FALSE, FLOAT fBlendOutData = NORMAL_BLEND_OUT)
	IF NOT IS_PED_INJURED(ped)
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(i_synced_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(i_synced_scene) >= f_phase_to_switch)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(i_synced_scene)
			BOOL b_just_stopped_sync_scene = FALSE
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(i_synced_scene)
				STOP_SYNCHRONIZED_ENTITY_ANIM(ped, fBlendOutData, TRUE)
				i_synced_scene = -1
				b_just_stopped_sync_scene = TRUE
			ENDIF
		
			IF NOT IS_PED_IN_COMBAT(ped)
			AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
				IF b_just_stopped_sync_scene
				AND b_instant_blend_to_aim
					SET_PED_RESET_FLAG(ped, PRF_InstantBlendToAim, TRUE)
				ENDIF
			
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped, 50.0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRANSITION_FROM_MOCAP_TO_AIM(PED_INDEX &ped, INT &i_synced_scene, VECTOR v_aim_pos, FLOAT f_phase_to_switch = 1.0, BOOL bInstantBlendToAim = TRUE, FLOAT fBlendOutData = NORMAL_BLEND_OUT)
	IF NOT IS_PED_INJURED(ped)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(i_synced_scene)
		AND GET_SYNCHRONIZED_SCENE_PHASE(i_synced_scene) >= f_phase_to_switch
			STOP_SYNCHRONIZED_ENTITY_ANIM(ped, fBlendOutData, TRUE)
			i_synced_scene = -1
		
			TASK_AIM_GUN_AT_COORD(ped, v_aim_pos, -1, bInstantBlendToAim)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a ped is in the correct position to play the given seamless mocap anim.
FUNC BOOL IS_PED_SAFE_TO_PLAY_SEAMLESS_ANIM(PED_INDEX ped, VECTOR v_desired_pos, FLOAT f_desired_heading, 
											FLOAT f_position_tolerance, FLOAT f_heading_tolerance, BOOL b_needs_to_be_aiming = FALSE, BOOL b_needs_to_be_in_cover = FALSE)
	IF NOT IS_PED_INJURED(ped)
		VECTOR v_pos = GET_ENTITY_COORDS(ped)
		
		IF VDIST(v_pos, v_desired_pos) < f_position_tolerance
			FLOAT f_heading = GET_ENTITY_HEADING(ped)
			FLOAT f_heading_diff = ABSF(f_heading - f_desired_heading)
			
			IF f_heading_diff > 180.0
				f_heading_diff = ABSF(f_heading_diff - 360.0)
			ENDIF
			
			IF f_heading_diff < f_heading_tolerance
				IF b_needs_to_be_aiming
					IF IS_PED_SHOOTING(ped)
						RETURN TRUE
					ENDIF
				ELIF b_needs_to_be_in_cover
					IF IS_PED_IN_COVER(ped, TRUE)
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is currently hanging around where the given ped is trying to get to. This is useful for checking if peds 
///    playing synced scenes need to break into AI due to the player getting in the way.
/// PARAMS:
///    ped - The ped moving towards the destination.
///    v_destination - The destination
///    f_max_dist_before_check - The ped must be within this distance of the destination before we start checking what the player is doing.
///    b_player_must_be_in_cover - If TRUE then the player must be in cover for them to be considered in the way.  
FUNC BOOL IS_PLAYER_STEALING_PEDS_DESTINATION(PED_INDEX ped, VECTOR v_destination, FLOAT f_max_dist_before_check, FLOAT f_max_player_dist_from_point = 2.0, BOOL b_player_must_be_in_cover = FALSE, 
											  BOOL b_player_must_be_closer = TRUE)
	IF NOT IS_PED_INJURED(ped)
		VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR v_ped_pos = GET_ENTITY_COORDS(ped)

		FLOAT f_dist_from_ped_to_dest = VDIST(v_ped_pos, v_destination)
		FLOAT f_dist_from_player_to_dest = VDIST(v_player_pos, v_destination)

		IF f_dist_from_ped_to_dest < f_max_dist_before_check
			IF (f_dist_from_player_to_dest < f_dist_from_ped_to_dest OR NOT b_player_must_be_closer)
			AND f_dist_from_player_to_dest < f_max_player_dist_from_point
				IF b_player_must_be_in_cover
					IF IS_PED_IN_COVER(PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Tasks a ped to prepare for the start of a synced scene by getting into cover and staying there, then checks if it's safe to play the synced scene.
///    Will also check if the player is blocking the start position of the scene, if this occurs the script needs to have an alternate task for the ped.
FUNC SYNCED_SCENE_TRANSITION_STATE GET_PED_IN_COVER_FOR_SYNCED_SCENE(PED_INDEX &ped, INT &i_timer, VECTOR v_cover_pos, VECTOR v_cover_from_pos, COVERPOINT_INDEX cover_index = NULL,
																	 BOOL b_aim_at_cover_from_pos = FALSE, BOOL b_put_directly_in_cover = FALSE, FLOAT f_cover_blend_duration = 0.25, 
																	 BOOL b_force_cover_direction = FALSE, BOOL b_force_face_left = FALSE)																 
	SYNCED_SCENE_TRANSITION_STATE e_current_state = SST_TRAVELLING_TO_START_POINT
	SEQUENCE_INDEX seq
	FLOAT f_dist_from_cover = VDIST2(v_cover_pos, GET_ENTITY_COORDS(ped))
		
	IF NOT IS_PED_INJURED(ped)
		IF NOT IS_PED_IN_COVER(ped, TRUE)
		OR f_dist_from_cover > 4.0
			i_timer = 0
		
			IF (GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK)
			OR IS_PED_IN_COMBAT(ped) //This means they reached the end of the sequence without being successfully put in cover.
				OPEN_SEQUENCE_TASK(seq)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					
					IF f_dist_from_cover > 4.0
						IF b_aim_at_cover_from_pos
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, v_cover_pos, v_cover_from_pos, PEDMOVE_RUN, FALSE, 0.25, 1.0, TRUE)
						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, v_cover_pos, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.25)
						ENDIF
					ENDIF
					
					IF b_put_directly_in_cover
						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, v_cover_pos, 10000, FALSE, f_cover_blend_duration, b_force_cover_direction, b_force_face_left, cover_index, TRUE)
					ELSE
						IF cover_index = NULL
							TASK_SEEK_COVER_TO_COORDS(NULL, v_cover_pos, v_cover_from_pos, 10000)
						ELSE
							TASK_SEEK_COVER_TO_COVER_POINT(NULL, cover_index, v_cover_from_pos, 10000, FALSE)
						ENDIF
					ENDIF
					
					//These tasks are just here so the sequence doesn't terminate immediately.
					//TASK_PAUSE(NULL, 5000)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0) 
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(ped, seq)
				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
		ELIF i_timer = 0 OR GET_GAME_TIMER() - i_timer > 1500 //The second check is in case the ped happened to already be in cover, so the timer was never reset.
			i_timer = GET_GAME_TIMER()
		ELIF GET_GAME_TIMER() - i_timer > 500
			e_current_state = SST_READY_TO_START_PLAYING
		ENDIF
		
		IF IS_PLAYER_STEALING_PEDS_DESTINATION(ped, v_cover_pos, 4.0)
			e_current_state = SST_PLAYER_IS_BLOCKING_START_POINT
		ENDIF
	ENDIF

	RETURN e_current_state
ENDFUNC

///New version of the shootout, with gang members instead of SWAT.
PROC MANAGE_INTERIOR_ENEMIES_GANG_VERSION(VECTOR v_player_pos)
	INT i = 0
	VECTOR v_cover_pos = <<0.0, 0.0, 0.0>>
	VECTOR v_lamar_pos
	VECTOR v_stretch_pos
	BOOL b_force_swat_dialogue = FALSE
	WEAPON_TYPE e_players_weapon
	SEQUENCE_INDEX seq

	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), e_players_weapon, FALSE)

	//Keep the swat model in memory until the player reaches the end of the shootout
	IF s_swat_indoor_breach_2[i].i_event = 0
		REQUEST_MODEL(model_gang)
		REQUEST_MODEL(model_gang_alternate)
	ENDIF
	REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
	
	IF NOT IS_PED_INJURED(s_lamar.ped)
		v_lamar_pos = GET_ENTITY_COORDS(s_lamar.ped)
	ENDIF
	
	IF NOT IS_PED_INJURED(s_stretch.ped)
		v_stretch_pos = GET_ENTITY_COORDS(s_stretch.ped)
	ENDIF
	
	IF NOT b_shootout_started
		IF NOT b_is_jumping_directly_to_stage //Don't prepare this if the music hasn't been restarted after a Z-skip yet
			PREPARE_MUSIC_EVENT("LM1_TERMINADOR_DOORS_OPEN")
		ENDIF
		
		BOOL b_shot_open_doors = FALSE
		//BOOL b_door_locked_left, b_door_locked_right
		//FLOAT f_door_ratio_left, f_door_ratio_right
	
		//NOTE: currently don't need this stuff for checking the door as Lamar and Stretch always go first anyway.
		/*IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-623.0, -1624.7, 33.2>>, 2.0, V_ILEV_RC_DOOR1)
			GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-623.0, -1624.7, 33.2>>, b_door_locked_left, f_door_ratio_left)
		ENDIF
		
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-625.6, -1624.5, 33.2>>, 2.0, V_ILEV_RC_DOOR1)
			GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-625.6, -1624.5, 33.2>>, b_door_locked_right, f_door_ratio_right)
		ENDIF
		
		b_shot_open_doors = ABSF(f_door_ratio_left) > 0.1 OR ABSF(f_door_ratio_right) > 0.1*/
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-626.096558,-1625.245972,32.034725>>, <<-622.516418,-1625.457031,34.010490>>, 1.25)
		OR b_shot_open_doors
		OR (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.1)
		OR (i_time_shootout_started > 0 AND GET_GAME_TIMER() - i_time_shootout_started > 750)
			TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_DOORS_OPEN")
			b_shootout_started = TRUE	
		ENDIF
	ENDIF
	
	//Gang members come out of the lift
	IF NOT s_swat_indoor_lift[COUNT_OF(s_swat_indoor_lift) - 1].b_is_created
		IF HAS_MODEL_LOADED(model_gang)
		AND HAS_MODEL_LOADED(model_gang_alternate)
			IF NOT DOES_ENTITY_EXIST(s_swat_indoor_lift[0].ped)
				s_swat_indoor_lift[0].ped = CREATE_ENEMY_PED(model_gang, <<-613.8455, -1625.8945, 32.0106>>, 175.6571, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
			ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_lift[1].ped)
				s_swat_indoor_lift[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-612.4139, -1623.9155, 32.0106>>, 180.2247, rel_group_neutral, 115, 0, WEAPONTYPE_PISTOL)
				
				f_lift_door_open_ratio = 0.0
				
				INITIALISE_ENEMY_GROUP(s_swat_indoor_lift, "Lift ")
			ENDIF
		ENDIF
	ELSE
		INT i_timer_threshold

		//Open the lift doors
		IF b_shootout_started
			CONVERGE_VALUE(f_lift_door_open_ratio, 1.0, 0.5, TRUE)
			
			IF f_lift_door_open_ratio < 1.0
			AND f_lift_door_open_ratio != 0.0
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_left_lift_door, <<-614.7, -1626.8, 32.0>>, TRUE, 0.0, 0.0, -f_lift_door_open_ratio)
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_right_lift_door, <<-612.1, -1627.0, 32.0>>, TRUE, 0.0, 0.0, f_lift_door_open_ratio)
				
				/*IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-614.7, -1626.8, 32.0>>, 2.0, model_left_lift_door)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(model_left_lift_door, <<-614.7, -1626.8, 32.0>>, TRUE, -f_lift_door_open_ratio)
				ENDIF
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-612.1, -1627.0, 32.0>>, 2.0, model_right_lift_door)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(model_right_lift_door, <<-612.1, -1627.0, 32.0>>, TRUE, f_lift_door_open_ratio)
				ENDIF*/
			ENDIF
		ENDIF
	
		REPEAT COUNT_OF(s_swat_indoor_lift) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_lift[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_lift[i].ped)
					SWITCH s_swat_indoor_lift[i].i_event
						CASE 0 //Trigger just after the first window guy breaches.
							IF b_shootout_started
								s_swat_indoor_lift[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_lift[i].i_event++
							ENDIF
						BREAK
						
						CASE 1
							IF i = 0
								i_timer_threshold = 100
								s_swat_indoor_lift[i].v_dest = <<-614.2863, -1630.5717, 32.0106>>
							ELIF i = 1
								i_timer_threshold = 500
								s_swat_indoor_lift[i].v_dest = <<-609.6615, -1631.9216, 32.0106>>
							ENDIF
							
							IF GET_GAME_TIMER() - s_swat_indoor_lift[i].i_timer > i_timer_threshold
								SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_lift[i].ped, rel_group_swat)
								SET_PED_COMBAT_PARAMS(s_swat_indoor_lift[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								
								IF i = 0
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_lift[i].ped, s_swat_indoor_lift[i].v_dest, 2.0, TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_lift[i].ped, 100.0)
								ELSE
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_lift[i].ped, s_swat_indoor_lift[i].v_dest, 2.0, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_lift[i].ped, 100.0)
								ENDIF
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_lift[i].ped, FALSE) //TODO 719785: use SYNCED_SCENE_DONT_INTERRUPT instead so behaviour when shot isn't interfered with.
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_lift[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_lift[i].ped, CA_CAN_CHARGE, TRUE)
								SET_COMBAT_FLOAT(s_swat_indoor_lift[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								
								PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_lift[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
								
								//s_swat_indoor_lift[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_lift[i].ped, TRUE)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_lift[i].ped, TRUE)
								s_swat_indoor_lift[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_lift[i].i_event++
							ENDIF
						BREAK						
						
						CASE 2
							IF GET_GAME_TIMER() - s_swat_indoor_lift[i].i_timer > 17000
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_lift[i].ped, FALSE)
								s_swat_indoor_lift[i].i_event++
							ENDIF
						BREAK
					ENDSWITCH
					
					//Set one enemy to charge once there are only two enemies left.
					IF NOT b_enemy_in_first_room_set_to_charge
						INT i_num_enemies_left_in_room
						i_num_enemies_left_in_room = GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_lift) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_window_1)
					
						IF i_num_enemies_left_in_room <= 2
							SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_lift[i].ped, <<-614.2863, -1630.5717, 32.0106>>, 3.0)
							SET_PED_COMBAT_MOVEMENT(s_swat_indoor_lift[i].ped, CM_DEFENSIVE)
							SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_lift[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_lift[i].ped, CA_CAN_CHARGE, TRUE)
							b_enemy_in_first_room_set_to_charge = TRUE
						ENDIF
					ENDIF
					
					IF s_swat_indoor_lift[i].i_event > 1
						UPDATE_AI_PED_BLIP(s_swat_indoor_lift[i].ped, s_swat_indoor_lift[i].s_blip_data)
					ENDIF
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_lift[i])
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
	
	//First enemies: (used to be SWAT through windows)
	IF NOT s_swat_indoor_window_1[COUNT_OF(s_swat_indoor_window_1) - 1].b_is_created
		IF HAS_MODEL_LOADED(model_gang)
		AND HAS_MODEL_LOADED(model_gang_alternate)
			IF s_swat_indoor_lift[0].i_event > 0
				IF NOT DOES_ENTITY_EXIST(s_swat_indoor_window_1[0].ped)
					s_swat_indoor_window_1[0].ped = CREATE_ENEMY_PED(model_gang, <<-605.7524, -1625.3763, 32.0106>>, 90.1695, rel_group_neutral, 115, 0, WEAPONTYPE_PISTOL)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_window_1[1].ped)
					s_swat_indoor_window_1[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-605.5994, -1624.1635, 32.0106>>, 90.2147, rel_group_neutral, 115, 0, WEAPONTYPE_PISTOL)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_window_1[2].ped)
					s_swat_indoor_window_1[2].ped = CREATE_ENEMY_PED(model_gang, <<-605.3604, -1621.9790, 32.0106>>, 90.2147, rel_group_neutral, 115, 0, WEAPONTYPE_PISTOL)

					INITIALISE_ENEMY_GROUP(s_swat_indoor_window_1, "Window ")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		BOOL b_time_to_trigger = FALSE
	
		REPEAT COUNT_OF(s_swat_indoor_window_1) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_window_1[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_window_1[i].ped)
					SWITCH s_swat_indoor_window_1[i].i_event
						CASE 0 //NOTE: these enemies used to trigger straight away along with the audio etc. The enemies now trigger later but the audio trigger is the same.												
							IF b_shootout_started
								s_swat_indoor_window_1[i].i_timer = 0
								s_swat_indoor_window_1[i].i_event++
							ENDIF
						BREAK
						
						CASE 1 //Stagger starts of some of the peds
							b_time_to_trigger = FALSE
							
							IF i = 0
								//IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_lift) <= 1
								//OR (NOT IS_PED_INJURED(s_swat_indoor_lift[0].ped) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(s_swat_indoor_lift[0].ped))
								//OR (NOT IS_PED_INJURED(s_swat_indoor_lift[1].ped) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(s_swat_indoor_lift[1].ped))
									IF s_swat_indoor_window_1[i].i_timer = 0
										s_swat_indoor_window_1[i].i_timer = GET_GAME_TIMER()
									ELIF GET_GAME_TIMER() - s_swat_indoor_window_1[i].i_timer > 1000
										b_time_to_trigger = TRUE
									ENDIF
								//ENDIF
							ELIF i = 1 OR i = 2
								IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_lift) <= 1
								OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_window_1) <= 2
									IF s_swat_indoor_window_1[i].i_timer = 0
										s_swat_indoor_window_1[i].i_timer = GET_GAME_TIMER()
									ELIF GET_GAME_TIMER() - s_swat_indoor_window_1[i].i_timer > 1000
										b_time_to_trigger = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-616.514465,-1630.977539,31.760536>>, <<-605.383301,-1631.432617,35.260498>>, 7.750000)
								b_time_to_trigger = TRUE
							ENDIF
							
							IF b_time_to_trigger
								IF i = 0
									PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_window_1[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_window_1[i].ped, s_swat_indoor_window_1[i].cover, <<-607.4794, -1633.2985, 32.0303>>, 80.4127, 
																   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
								ELIF i = 1
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_window_1[i].ped, s_swat_indoor_window_1[i].cover, <<-609.4595, -1631.6287, 32.0106>>, 89.7347, 
																   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
								ELIF i = 2
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_window_1[i].ped, s_swat_indoor_window_1[i].cover, <<-608.9896, -1628.0059, 32.0105>>, 97.5261, 
																   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
								ENDIF
								
								SET_PED_COMBAT_PARAMS(s_swat_indoor_window_1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_window_1[i].ped, rel_group_swat)
								SET_PED_CAN_BE_TARGETTED(s_swat_indoor_window_1[i].ped, TRUE)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_window_1[i].ped, TRUE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_window_1[i].ped, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_window_1[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_window_1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_window_1[i].ped, CA_CAN_CHARGE, TRUE)
								SET_COMBAT_FLOAT(s_swat_indoor_window_1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								
								s_swat_indoor_window_1[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_window_1[i].i_event++
							ENDIF
						BREAK
						
						CASE 2
							IF GET_GAME_TIMER() - s_swat_indoor_window_1[i].i_timer > 15000
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_window_1[i].ped, FALSE)
								
								s_swat_indoor_window_1[i].i_event++
							ENDIF
						BREAK
					ENDSWITCH
					
					IF NOT b_enemy_in_first_room_set_to_charge
						INT i_num_enemies_left_in_room
						i_num_enemies_left_in_room = GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_window_1) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_lift)
					
						IF i_num_enemies_left_in_room <= 2
							SET_PED_COMBAT_MOVEMENT(s_swat_indoor_window_1[i].ped, CM_DEFENSIVE)
							SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_window_1[i].ped, <<-616.7226, -1630.4847, 32.0106>>, 3.0)
							SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_window_1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_window_1[i].ped, CA_CAN_CHARGE, TRUE)
							b_enemy_in_first_room_set_to_charge = TRUE
						ENDIF
					ENDIF
					
					IF s_swat_indoor_window_1[i].i_event > 1
						UPDATE_AI_PED_BLIP(s_swat_indoor_window_1[i].ped, s_swat_indoor_window_1[i].s_blip_data)
					ENDIF
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_window_1[i])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//Enemies breach at end of corridor
	IF NOT s_swat_indoor_breach[0].b_is_created
		//Delay creation a bit, as it's causing spikes at the start of the shootout.
		IF NOT s_swat_indoor_window_1[0].b_is_created
			s_swat_indoor_breach[0].i_timer = GET_GAME_TIMER()
		ELIF GET_GAME_TIMER() - s_swat_indoor_breach[0].i_timer > 3000
			REQUEST_PTFX_ASSET()
			REQUEST_MODEL(model_breach_door)
			
			IF HAS_MODEL_LOADED(model_gang)
			AND HAS_MODEL_LOADED(model_gang_alternate)
			AND HAS_MODEL_LOADED(model_breach_door)
			AND HAS_PTFX_ASSET_LOADED()
			AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\LAMAR1_01")
				s_swat_indoor_breach[0].ped = CREATE_ENEMY_PED(model_gang, <<-590.1174, -1622.8175, 32.0106>>, 19.5823, rel_group_neutral, 110, 0, WEAPONTYPE_PUMPSHOTGUN)
				s_swat_indoor_breach[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-590.9761, -1623.5693, 32.0106>>, 342.5562, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
				
				REMOVE_OBJECT(obj_breach_doors[0], TRUE)
				REMOVE_OBJECT(obj_breach_doors[1], TRUE)
				
				obj_breach_door = CREATE_OBJECT_NO_OFFSET(model_breach_door, <<-591.5, -1621.4, 100.0>>)
				SET_ENTITY_COORDS_NO_OFFSET(obj_breach_door, <<-591.5, -1621.4, 100.0>>)
				SET_ENTITY_ROTATION(obj_breach_door, <<0.0, 0.0, 180.0>>)
				SET_ENTITY_VISIBLE(obj_breach_door, FALSE)
				FREEZE_ENTITY_POSITION(obj_breach_door, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_breach_door)
				
				//These doors are made invisible later, so make sure they're reset here (in case of z-skips etc).
				REMOVE_MODEL_HIDE(<<-591.5, -1621.4, 33.2>>, 0.5, V_ILEV_RC_DOOR1_ST)
				REMOVE_MODEL_HIDE(<<-588.9, -1621.6, 33.2>>, 0.5, V_ILEV_RC_DOOR1_ST)
				CLEAR_AREA(<<-588.9, -1621.6, 33.2>>, 5.0, FALSE)
															
				REPEAT COUNT_OF(s_swat_indoor_breach) i
					SET_ENTITY_INVINCIBLE(s_swat_indoor_breach[i].ped, TRUE)
					SET_ENTITY_PROOFS(s_swat_indoor_breach[i].ped, TRUE, TRUE, TRUE, TRUE, TRUE)
					
					IF i = 1
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_breach[i].ped, TRUE)
						SET_PED_SUFFERS_CRITICAL_HITS(s_swat_indoor_breach[i].ped, FALSE)
						SET_PED_MAX_HEALTH(s_swat_indoor_breach[i].ped, 1000)
						SET_ENTITY_HEALTH(s_swat_indoor_breach[i].ped, 1000)
					ENDIF
				ENDREPEAT
															
				INITIALISE_ENEMY_GROUP(s_swat_indoor_breach, "Breach ")
				b_frozen_breach_doors = FALSE
			ENDIF
		ENDIF
	ELSE
		REPEAT COUNT_OF(s_swat_indoor_breach) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_breach[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_breach[i].ped)
					SWITCH s_swat_indoor_breach[i].i_event
						CASE 0
							//Lock the door to be breached
							IF NOT b_frozen_breach_doors
							
								IF NOT DOES_ENTITY_EXIST(obj_breach_doors[0])
									obj_breach_doors[0] = GET_CLOSEST_OBJECT_OF_TYPE(v_breach_door_1_pos, 1.0, V_ILEV_RC_DOOR1_ST, FALSE)
								ENDIF
								
								IF NOT DOES_ENTITY_EXIST(obj_breach_doors[1])
									obj_breach_doors[1] = GET_CLOSEST_OBJECT_OF_TYPE(v_breach_door_2_pos, 1.0, V_ILEV_RC_DOOR1_ST, FALSE)
								ENDIF
							
								IF DOES_ENTITY_EXIST(obj_breach_doors[0])
								AND DOES_ENTITY_EXIST(obj_breach_doors[1])
									SET_ENTITY_VISIBLE(obj_breach_doors[0], TRUE)
									SET_ENTITY_COLLISION(obj_breach_doors[0], TRUE)
									FREEZE_ENTITY_POSITION(obj_breach_doors[0], TRUE)
									SET_ENTITY_INVINCIBLE(obj_breach_doors[0], TRUE)
									
									SET_ENTITY_VISIBLE(obj_breach_doors[1], TRUE)
									SET_ENTITY_COLLISION(obj_breach_doors[1], TRUE)
									FREEZE_ENTITY_POSITION(obj_breach_doors[1], TRUE)
									SET_ENTITY_INVINCIBLE(obj_breach_doors[1], TRUE)
									
									b_frozen_breach_doors = TRUE
								ENDIF
							ENDIF
						
							IF NOT IS_PED_INJURED(s_lamar.ped)
								IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-590.109009,-1621.447510,32.011780>>, <<-589.890015,-1617.484619,34.378773>>, 2.000000)
									IF i = 0		
										v_cover_pos = <<-589.5699, -1619.7706, 32.0106>>							

										START_PARTICLE_FX_NON_LOOPED_AT_COORD(str_door_breach_ptfx, <<-590.1724, -1621.4822, 33.1749>>, <<0.0, 0.0, -5.84>>, 1.0)
										
										FLOAT f_dist_from_player 
										f_dist_from_player = VDIST2(v_player_pos, <<-590.1724, -1621.4822, 33.1749>>)
										
										IF f_dist_from_player < 100.0
											SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.5 - (f_dist_from_player / 100.0))
										ENDIF
										
										PLAY_SOUND_FROM_COORD(-1, "LAMAR1_BustDoorOpen1", <<-590.1724, -1621.4822, 33.1749>>)
										TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_1ST_DOOR_EXPLODES")
									ELSE
										v_cover_pos = <<-592.0153, -1619.4905, 32.0106>>
									ENDIF
								
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_breach[i].ped, rel_group_swat)
									//SET_ENTITY_INVINCIBLE(s_swat_indoor_breach[i].ped, TRUE)
									SET_ENTITY_INVINCIBLE(s_swat_indoor_breach[i].ped, FALSE)
									SET_ENTITY_PROOFS(s_swat_indoor_breach[i].ped, FALSE, FALSE, FALSE, FALSE, FALSE)
									SET_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_breach[i].ped, RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_FIRE)
									SET_PED_COMBAT_AI(s_swat_indoor_breach[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, CA_USE_COVER, v_cover_pos, 1.0)
									
									s_swat_indoor_breach[i].i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-599.100, -1620.098, 32.001>>, <<0.000, 0.000, -91.000>>)
									
									
									IF i = 0
										OPEN_SEQUENCE_TASK(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-589.4663, -1620.3263, 32.0106>>, <<-589.6, -1617.7, 32.3>>, PEDMOVE_WALK,
																			   	   FALSE, 0.5, 4.0, FALSE)
											TASK_AIM_GUN_AT_COORD(NULL, <<-589.6, -1617.7, 32.3>>, -1)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(s_swat_indoor_breach[i].ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
									
										//TASK_SYNCHRONIZED_SCENE(s_swat_indoor_breach[i].ped, s_swat_indoor_breach[i].i_sync_scene, "misslamar1ig_5", "p3_swat_b", 
										//						NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, NORMAL_BLEND_IN)
										//SET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[i].i_sync_scene, 0.04)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_breach[i].ped, TRUE)
									ELSE
										//TASK_SYNCHRONIZED_SCENE(s_swat_indoor_breach[i].ped, s_swat_indoor_breach[i].i_sync_scene, "misslamar1ig_5", "p3_swat_a", 
										//						NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, NORMAL_BLEND_IN)
										//SET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[i].i_sync_scene, 0.12)
										//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_breach[i].ped, FALSE)
										
										s_swat_indoor_breach[i].v_dest = <<-591.5299, -1619.2393, 32.0105>>
										OPEN_SEQUENCE_TASK(seq)
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, s_swat_indoor_breach[i].v_dest, PLAYER_PED_ID(), PEDMOVE_RUN,
																			   	   FALSE, 0.5, 4.0, TRUE)
											TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(s_swat_indoor_breach[i].ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
										
										SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_breach[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_breach[i].ped, CA_CAN_CHARGE, TRUE)
										SET_COMBAT_FLOAT(s_swat_indoor_breach[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
										
										SET_ENTITY_INVINCIBLE(s_swat_indoor_breach[i].ped, TRUE)
									ENDIF

									s_swat_indoor_breach[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_breach[i].ped, TRUE)
									s_swat_indoor_breach[i].i_timer = GET_GAME_TIMER()
									s_swat_indoor_breach[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF i = 0
								IF DOES_ENTITY_EXIST(obj_breach_doors[0])
									REMOVE_DECALS_FROM_OBJECT(obj_breach_doors[0])
									SET_ENTITY_VISIBLE(obj_breach_doors[0], FALSE)
									SET_ENTITY_COLLISION(obj_breach_doors[0], FALSE)
									SET_ENTITY_INVINCIBLE(obj_breach_doors[0], FALSE)
								ENDIF
								
								IF DOES_ENTITY_EXIST(obj_breach_doors[1])
									REMOVE_DECALS_FROM_OBJECT(obj_breach_doors[1])
									SET_ENTITY_VISIBLE(obj_breach_doors[1], FALSE)
									SET_ENTITY_COLLISION(obj_breach_doors[1], FALSE)
									SET_ENTITY_INVINCIBLE(obj_breach_doors[1], FALSE)
								ENDIF
								
								SET_ENTITY_COORDS_NO_OFFSET(obj_breach_door, <<-590.7942, -1621.2830, 33.1598>>)
								SET_ENTITY_ROTATION(obj_breach_door, <<0.0000, 0.0000, -5.0000>>)
								FREEZE_ENTITY_POSITION(obj_breach_door, FALSE)
								SET_ENTITY_VISIBLE(obj_breach_door, TRUE)
								ACTIVATE_PHYSICS(obj_breach_door)
								//APPLY_FORCE_TO_ENTITY(obj_breach_door, APPLY_TYPE_IMPULSE, <<0.0, 5.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
								APPLY_FORCE_TO_ENTITY(obj_breach_door, APPLY_TYPE_IMPULSE, <<-0.5, 5.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE, FALSE, FALSE)
							ENDIF
							
							//Make the player ragdoll if they stood by the door.
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-589.850769,-1621.564453,31.760536>>, <<-589.239075,-1616.516113,34.762459>>, 3.500000)
								//SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 1000, 3000, TASK_NM_BALANCE)
								SET_PED_TO_RAGDOLL_WITH_FALL(PLAYER_PED_ID(), 1000, 5000, TYPE_DOWN_STAIRS, <<0.0, 1.0, 0.0>>, 32.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE, <<0.0, 1.0, 0.25>>, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
								APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), (GET_ENTITY_HEALTH(PLAYER_PED_ID()) - 100) / 3, TRUE)
								SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.5)
							ENDIF
							
							s_swat_indoor_breach[i].i_event++
						BREAK
					
						CASE 2						
							IF GET_GAME_TIMER() - s_swat_indoor_breach[i].i_timer > 500
								SET_ENTITY_INVINCIBLE(s_swat_indoor_breach[i].ped, FALSE)
								CLEAR_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_breach[i].ped)
								SET_PED_CAN_RAGDOLL(s_swat_indoor_breach[i].ped, TRUE)
							
								IF i = 0
									IF NOT b_has_text_label_triggered[LEM1_BREACH1]
									AND b_has_text_label_triggered[LEM1_KNOCK]
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_breach[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
											b_has_text_label_triggered[LEM1_BREACH1] = TRUE
										ENDIF
									ELSE
										s_swat_indoor_breach[i].i_timer = 0
										s_swat_indoor_breach[i].i_event++
									ENDIF
								ELSE
									s_swat_indoor_breach[i].i_timer = 0
									s_swat_indoor_breach[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 3
							IF i = 0
								//If the player doesn't kill this ped in time he shoots Lamar.
								IF s_swat_indoor_breach[i].i_timer = 0
									s_swat_indoor_breach[i].i_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - s_swat_indoor_breach[i].i_timer > 3000
									IF NOT IS_PED_RAGDOLL(s_swat_indoor_breach[i].ped)
										SET_PED_ACCURACY(s_lamar.ped, 100)
									ENDIF
									
									OPEN_SEQUENCE_TASK(seq)
										IF NOT IS_PED_RAGDOLL(s_swat_indoor_breach[i].ped)
											IF NOT IS_PED_INJURED(s_lamar.ped)
												TASK_SHOOT_AT_ENTITY(NULL, s_lamar.ped, 800, FIRING_TYPE_CONTINUOUS)
											ENDIF
										ENDIF
										TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(s_swat_indoor_breach[i].ped, seq)
									CLEAR_SEQUENCE_TASK(seq)
									
									IF NOT IS_PED_RAGDOLL(s_swat_indoor_breach[i].ped)
										IF NOT IS_PED_INJURED(s_lamar.ped)
											SET_RAGDOLL_BLOCKING_FLAGS(s_lamar.ped, RBF_NONE)
											SET_PED_TO_RAGDOLL(s_lamar.ped, 1000, 2000, TASK_NM_BALANCE)
											APPLY_DAMAGE_TO_PED(s_lamar.ped, GET_ENTITY_HEALTH(s_lamar.ped) + 50, TRUE)
										ENDIF
									ENDIF
									
									s_swat_indoor_breach[i].i_timer = 0
									s_swat_indoor_breach[i].i_event++
								ENDIF
								
								IF IS_PED_INJURED(s_swat_indoor_breach[1].ped)
									SET_BIT(s_swat_indoor_breach[i].i_bitset, ENEMY_FLAG_REFRESH_TASKS)
									s_swat_indoor_breach[i].i_timer = 0
									s_swat_indoor_breach[i].i_event = 10
								ENDIF
							ELSE
								//Second ped breaks out and fights the player
								IF IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_breach[i].i_sync_scene)
									IF NOT b_has_text_label_triggered[LEM1_BREACHAMB]
										IF GET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[i].i_sync_scene) > 0.21
											PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_breach[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
											b_has_text_label_triggered[LEM1_BREACHAMB] = TRUE
										ENDIF
									ENDIF
								
									IF GET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[i].i_sync_scene) > 0.25
										//Break the ped into combat if the player gets in his face.
										IF GET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[i].i_sync_scene) > 0.27
										OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.241882,-1619.370483,32.010517>>, <<-593.077332,-1618.892700,34.760517>>, 5.250000)											
											s_swat_indoor_breach[i].i_timer = 0
											s_swat_indoor_breach[i].i_event = 10
										ENDIF
									ENDIF
								ELSE
									s_swat_indoor_breach[i].i_timer = 0
									s_swat_indoor_breach[i].i_event = 10
								ENDIF
							ENDIF
						BREAK
						
						CASE 10 //Just go into combat
							//Reset health and any ragdoll blocking.
							IF GET_PED_MAX_HEALTH(s_swat_indoor_breach[i].ped) > 200
								SET_PED_MAX_HEALTH(s_swat_indoor_breach[i].ped, 200)
								
								IF GET_ENTITY_HEALTH(s_swat_indoor_breach[i].ped) > 135
									SET_ENTITY_HEALTH(s_swat_indoor_breach[i].ped, 135)
								ENDIF
							ENDIF
							
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_breach[i].ped, TRUE)
							SET_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_breach[i].ped, RBF_NONE)
							CLEAR_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_breach[i].ped)
						
							IF (GET_SCRIPT_TASK_STATUS(s_swat_indoor_breach[i].ped, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(s_swat_indoor_breach[i].ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK)
							OR IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_breach[i].i_sync_scene)
							OR IS_BIT_SET(s_swat_indoor_breach[i].i_bitset, ENEMY_FLAG_REFRESH_TASKS)
								IF i = 0
									s_swat_indoor_breach[i].v_dest = <<-591.5299, -1619.2393, 32.0105>>
								ELIF i = 1
									s_swat_indoor_breach[i].v_dest = <<-591.5299, -1619.2393, 32.0105>>
								ENDIF
							
								SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_breach[i].ped, s_swat_indoor_breach[i].v_dest, 2.0)								
								
								BOOL b_anim_playing
								
								IF IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_breach[i].i_sync_scene)
									STOP_SYNCHRONIZED_ENTITY_ANIM(s_swat_indoor_breach[i].ped, SLOW_BLEND_OUT, TRUE)
									s_swat_indoor_breach[i].i_sync_scene = -1
									b_anim_playing = TRUE
								ENDIF
								
								OPEN_SEQUENCE_TASK(seq)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								
									IF b_anim_playing
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, s_swat_indoor_breach[i].v_dest, PLAYER_PED_ID(), PEDMOVE_WALK, TRUE)
									ENDIF
									
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID(), COMBAT_PED_PREVENT_CHANGING_TARGET)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(s_swat_indoor_breach[i].ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
								
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_breach[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_breach[i].ped, CA_CAN_CHARGE, TRUE)
								SET_COMBAT_FLOAT(s_swat_indoor_breach[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								
								CLEAR_BIT(s_swat_indoor_breach[i].i_bitset, ENEMY_FLAG_REFRESH_TASKS)
							ENDIF
						BREAK
					ENDSWITCH
					
					//PRINTLN(i, " ", s_swat_indoor_breach[i].i_event, " ", GET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[i].i_sync_scene))
					
					//Kill instantly if the player shoots them
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_swat_indoor_breach[i].ped, PLAYER_PED_ID())
						STOP_SYNCHRONIZED_ENTITY_ANIM(s_swat_indoor_breach[i].ped, SLOW_BLEND_OUT, TRUE)
						s_swat_indoor_breach[i].i_sync_scene = -1
						
						SET_ENTITY_INVINCIBLE(s_swat_indoor_breach[i].ped, FALSE)
						SET_PED_CAN_RAGDOLL(s_swat_indoor_breach[i].ped, TRUE)
						SET_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_breach[i].ped, RBF_NONE)
						
						IF GET_PED_MAX_HEALTH(s_swat_indoor_breach[i].ped) > 200
							SET_PED_MAX_HEALTH(s_swat_indoor_breach[i].ped, 200)
							
							IF GET_ENTITY_HEALTH(s_swat_indoor_breach[i].ped) > 135
								SET_ENTITY_HEALTH(s_swat_indoor_breach[i].ped, 135)
							ENDIF
						ENDIF

						//APPLY_DAMAGE_TO_PED(s_swat_indoor_breach[i].ped, GET_ENTITY_HEALTH(s_swat_indoor_breach[i].ped) + 50, TRUE)
					ENDIF
				ELSE				
					CLEAN_UP_ENEMY_PED(s_swat_indoor_breach[i])
					REMOVE_OBJECT(obj_breach_door)
					
					REMOVE_PTFX_ASSET()
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\LAMAR1_01")
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
	
	//Enemies attack from the other side of the (former) heli room.
	IF NOT s_swat_indoor_heli[0].b_is_created
		IF s_swat_indoor_breach[0].i_event > 0
			IF HAS_MODEL_LOADED(model_gang)
				IF NOT DOES_ENTITY_EXIST(s_swat_indoor_heli[0].ped)
					s_swat_indoor_heli[0].ped = CREATE_ENEMY_PED(model_gang, <<-562.0791, -1627.7186, 31.8098>>, 86.8494, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_heli[1].ped)
					s_swat_indoor_heli[1].ped = CREATE_ENEMY_PED(model_gang, <<-562.1100, -1626.7261, 31.2098>>, 86.8494, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_heli[2].ped)
					s_swat_indoor_heli[2].ped = CREATE_ENEMY_PED(model_gang, <<-561.9381, -1625.9612, 30.6098>>, 95.2498, rel_group_neutral, 175, 0, WEAPONTYPE_MICROSMG)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_heli[3].ped)
					s_swat_indoor_heli[3].ped = CREATE_ENEMY_PED(model_gang, <<-561.6803, -1624.1124, 30.0093>>, 95.2498, rel_group_neutral, 135, 0, WEAPONTYPE_PUMPSHOTGUN)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_heli[4].ped)
					s_swat_indoor_heli[4].ped = CREATE_ENEMY_PED(model_gang, <<-574.1028, -1625.1156, 32.0106>>, 81.3094, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
					
					INITIALISE_ENEMY_GROUP(s_swat_indoor_heli, "heliroom ")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		REPEAT COUNT_OF(s_swat_indoor_heli) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_heli[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_heli[i].ped)
					SWITCH s_swat_indoor_heli[i].i_event
						CASE 0
							IF i < 3
								SET_PED_RESET_FLAG(s_swat_indoor_heli[i].ped, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, TRUE)
							
								IF ARE_PEDS_IN_ANGLED_AREA(PLAYER_PED_ID(), s_lamar.ped, s_stretch.ped, <<-586.092224,-1625.472412,32.189663>>, <<-592.102356,-1624.910400,34.510536>>, 6.850000)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_heli[i].ped, 10, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									
									IF i = 0
										SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_heli[i].ped, s_swat_indoor_heli[i].cover, <<-573.5095, -1627.3381, 32.0258>>, 79.2489, 
																	   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
									ELIF i = 1
										SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_heli[i].ped, s_swat_indoor_heli[i].cover, <<-577.9146, -1623.5862, 32.0106>>, 89.1467,
																	   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
									ELSE
										SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_heli[i].ped, <<-578.3687, -1628.8259, 32.0106>>, 2.0, TRUE) 
									ENDIF
									
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_heli[i].ped, rel_group_swat)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_heli[i].ped, 100.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_heli[i].ped, TRUE)
									IF i != 2
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_heli[i].ped, TRUE)
									ENDIF
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_heli[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									
									IF i = 0
										PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_heli[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
									ENDIF
																	
									//s_swat_indoor_heli[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_heli[i].ped, TRUE)
									s_swat_indoor_heli[i].i_event++
									s_swat_indoor_heli[i].i_timer = GET_GAME_TIMER()
								ENDIF
							ELIF i = 3
								IF (IS_PED_INJURED(s_swat_indoor_heli[0].ped) AND IS_PED_INJURED(s_swat_indoor_heli[1].ped))
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-566.992249,-1627.380859,31.796909>>, <<-580.061096,-1626.554077,34.575512>>, 9.500000)
								OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_heli) <= 3
									SET_PED_COMBAT_PARAMS(s_swat_indoor_heli[i].ped, 10, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_heli[i].ped, <<-573.6948, -1627.8458, 32.0258>>, 2.0, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_heli[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_heli[i].ped, rel_group_swat)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_heli[i].ped, 100.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_heli[i].ped, TRUE)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_heli[i].ped, TRUE)
									
									PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_heli[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
																	
									s_swat_indoor_heli[i].i_event++
									s_swat_indoor_heli[i].i_timer = GET_GAME_TIMER()
								ENDIF
							ELIF i = 4
								IF ARE_PEDS_IN_ANGLED_AREA(s_lamar.ped, s_stretch.ped, NULL, <<-586.092224,-1625.472412,32.189663>>, <<-592.102356,-1624.910400,34.510536>>, 6.850000)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-589.359314,-1628.687622,32.130856>>, <<-589.501770,-1619.567261,34.760559>>, 5.250000)
									IF IS_PED_INJURED(s_swat_indoor_breach[0].ped)
									OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-589.359314,-1628.687622,32.130856>>, <<-589.501770,-1619.567261,34.760559>>, 5.250000)
										s_swat_indoor_heli[i].v_dest = <<-581.6903, -1624.3286, 32.0106>>
										SET_PED_COMBAT_PARAMS(s_swat_indoor_heli[i].ped, 10, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
										SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_heli[i].ped, s_swat_indoor_heli[i].v_dest, 2.0, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_CAN_CHARGE, TRUE)
										SET_COMBAT_FLOAT(s_swat_indoor_heli[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
										
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_heli[i].ped, TRUE)
										SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_heli[i].ped, rel_group_swat)
										
										OPEN_SEQUENCE_TASK(seq)
											//TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
											//TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-573.1476, -1625.6941, 32.0106>>, <<-588.9735, -1623.2454, 33.6092>>, 
											//									   PEDMOVE_RUN, TRUE, 0.25, 4.0, TRUE, ENAV_NO_STOPPING)
											TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(s_swat_indoor_heli[i].ped, seq)
										CLEAR_SEQUENCE_TASK(seq)

										PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_heli[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
																		
										s_swat_indoor_heli[i].i_event++
										s_swat_indoor_heli[i].i_timer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF GET_GAME_TIMER() - s_swat_indoor_heli[i].i_timer > 13000
							OR (i = 3 AND GET_GAME_TIMER() - s_swat_indoor_heli[i].i_timer > 8000)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_heli[i].ped, FALSE)
								
								s_swat_indoor_heli[i].i_event++
								s_swat_indoor_heli[i].i_timer = GET_GAME_TIMER()
							ENDIF
						BREAK
					ENDSWITCH
					
					//Once the player enters have this guy go straight for him.
					IF i = 4
						IF ARE_VECTORS_ALMOST_EQUAL(s_swat_indoor_heli[i].v_dest, <<-581.6903, -1624.3286, 32.0106>>, 0.1)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-586.092224,-1625.472412,32.189663>>, <<-592.102356,-1624.910400,34.510536>>, 6.850000)
								SET_PED_COMBAT_MOVEMENT(s_swat_indoor_heli[i].ped, CM_DEFENSIVE)
								SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(s_swat_indoor_heli[i].ped, PLAYER_PED_ID(), <<0.0, 0.0, 0.0>>, 4.0)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_heli[i].ped, CA_CAN_CHARGE, TRUE)
								
								s_swat_indoor_heli[i].v_dest = <<0.0, 0.0, 0.0>>
							ENDIF
						ENDIF
					ENDIF
					
					IF s_swat_indoor_heli[i].i_event > 0
						UPDATE_AI_PED_BLIP(s_swat_indoor_heli[i].ped, s_swat_indoor_heli[i].s_blip_data)
					ENDIF
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_heli[i])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	//Enemy runs up the stairs.
	IF NOT s_swat_indoor_window_2[0].b_is_created
		IF s_swat_indoor_breach[0].i_event > 0
			IF HAS_MODEL_LOADED(model_gang)
				s_swat_indoor_window_2[0].ped = CREATE_ENEMY_PED(model_gang, <<-562.1794, -1628.9116, 28.0096>>, 359.9586, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
				INITIALISE_ENEMY_GROUP(s_swat_indoor_window_2, "window_2 ")
			ENDIF
		ENDIF
	ELSE
		REPEAT COUNT_OF(s_swat_indoor_window_2) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_window_2[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_window_2[i].ped)
					SWITCH s_swat_indoor_window_2[i].i_event
						CASE 0
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-564.710022,-1628.201172,28.009590>>, <<-564.254578,-1622.007446,31.759422>>, 2.250000)		
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-560.612366,-1623.894775,30.009325>>, <<-565.556641,-1623.528564,32.318893>>, 3.000000)
								SET_PED_COMBAT_PARAMS(s_swat_indoor_window_2[i].ped, 10, CM_WILLADVANCE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
								SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_window_2[i].ped, <<-563.7205, -1630.9292, 28.0096>>, 2.0, TRUE)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_window_2[i].ped, rel_group_swat)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_window_2[i].ped, 50.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_window_2[i].ped, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_window_2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_window_2[i].ped, CA_CAN_CHARGE, TRUE)
								SET_COMBAT_FLOAT(s_swat_indoor_window_2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								
								PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_window_2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
																
								//s_swat_indoor_window_2[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_window_2[i].ped, TRUE)
								s_swat_indoor_window_2[i].i_event++
								s_swat_indoor_window_2[i].i_timer = GET_GAME_TIMER()
							ENDIF
						BREAK
						
						CASE 1
							IF GET_GAME_TIMER() - s_swat_indoor_window_2[i].i_timer > 2500
								PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_window_2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
								s_swat_indoor_window_2[i].i_event++
							ENDIF
						BREAK
					ENDSWITCH
					
					IF s_swat_indoor_window_2[i].i_event > 0
						UPDATE_AI_PED_BLIP(s_swat_indoor_window_2[i].ped, s_swat_indoor_window_2[i].s_blip_data)
					ENDIF
				ELSE
					//There are some triggers that are based on this enemy running up the stairs, if the player somehow manages to kill them early make sure
					//the enemy is treated as if it had already started running.
					IF s_swat_indoor_window_2[i].i_event <= 0
						s_swat_indoor_window_2[i].i_event = 1
					ENDIF
					
					CLEAN_UP_ENEMY_PED(s_swat_indoor_window_2[i])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//SWAT members enter the corridor before the warehouse room.
	IF NOT s_swat_indoor_pre_warehouse[0].b_is_created
		IF s_swat_indoor_window_2[0].i_event > 0
			IF VDIST2(<<-561.4490, -1603.4106, 26.0109>>, v_player_pos) < 2500.0
				IF HAS_MODEL_LOADED(model_gang)
				AND HAS_MODEL_LOADED(model_gang_alternate)
					IF NOT DOES_ENTITY_EXIST(s_swat_indoor_pre_warehouse[0].ped)
						s_swat_indoor_pre_warehouse[0].ped = CREATE_ENEMY_PED(model_gang, <<-564.2737, -1601.3719, 26.0108>>, 182.8773, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_pre_warehouse[1].ped)
						s_swat_indoor_pre_warehouse[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-563.2382, -1602.5646, 26.0108>>, 183.3599, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_pre_warehouse[2].ped)
						s_swat_indoor_pre_warehouse[2].ped = CREATE_ENEMY_PED(model_gang, <<-564.2366, -1602.4486, 26.0108>>, 183.3599, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_pre_warehouse[3].ped)
						s_swat_indoor_pre_warehouse[3].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-565.6711, -1601.5125, 26.0108>>, 177.4956, rel_group_neutral, 135, 0, WEAPONTYPE_MICROSMG)
						
						//Freeze the boxes in the room these guys enter (B*946604)
						OBJECT_INDEX objBoxes[4]
						
						objBoxes[0] = GET_CLOSEST_OBJECT_OF_TYPE(<<-559.99, -1610.07, 26.01>>, 2.0, PROP_BOX_WOOD03A, FALSE)
						objBoxes[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<-560.0, -1611.25, 26.01>>, 2.0, PROP_BOX_WOOD03A, FALSE)
						objBoxes[2] = GET_CLOSEST_OBJECT_OF_TYPE(<<-562.69, -1614.53, 26.01>>, 2.0, PROP_BOX_WOOD03A, FALSE)
						objBoxes[3] = GET_CLOSEST_OBJECT_OF_TYPE(<<-562.79, -1615.71, 26.01>>, 2.0, PROP_BOX_WOOD03A, FALSE)
						
						REPEAT COUNT_OF(objBoxes) i
							IF DOES_ENTITY_EXIST(objBoxes[i])
								FREEZE_ENTITY_POSITION(objBoxes[i], TRUE)
							ENDIF
						ENDREPEAT
						
						INITIALISE_ENEMY_GROUP(s_swat_indoor_pre_warehouse, "Pre-warehouse ")
						i_smoke_dialogue_timer = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		BOOL b_time_to_trigger = FALSE
		BOOL b_checked_trigger = FALSE
	
		REPEAT COUNT_OF(s_swat_indoor_pre_warehouse) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_pre_warehouse[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_pre_warehouse[i].ped)
					SWITCH s_swat_indoor_pre_warehouse[i].i_event
						CASE 0
							IF NOT b_checked_trigger //All the enemies are triggered off the same locate, so only need to do this once per loop.
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-560.26,-1619.91,26.01>>, <<-563.71,-1619.67,28.01>>, 1.0)
									b_time_to_trigger = TRUE
								ELIF s_swat_indoor_pre_warehouse[i].i_timer = 0
									IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-560.26,-1619.91,26.01>>, <<-563.71,-1619.67,28.01>>, 1.0)
										s_swat_indoor_pre_warehouse[i].i_timer = GET_GAME_TIMER()
									ENDIF
								ELIF GET_GAME_TIMER() - s_swat_indoor_pre_warehouse[i].i_timer > 5000
									b_time_to_trigger = TRUE
								ENDIF

								b_checked_trigger = TRUE
							ENDIF
						
							IF b_time_to_trigger
								//IF i = 0
								//	ADD_EXPLOSION(<<-560.6339, -1601.8541, 26.0110>>, EXP_TAG_SMOKE_GRENADE)
								//	i_smoke_dialogue_timer = GET_GAME_TIMER()
								//ENDIF

								//s_swat_indoor_pre_warehouse[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_pre_warehouse[i].ped, TRUE)
								s_swat_indoor_pre_warehouse[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_pre_warehouse[i].i_event++
							ENDIF
						BREAK
						
						CASE 1
							REQUEST_ANIM_DICT("misslamar1lam_1_ig_11")
							
							IF i < 2
								IF GET_GAME_TIMER() - s_swat_indoor_pre_warehouse[i].i_timer > 500
								AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_11")
									IF i = 0
										s_swat_indoor_pre_warehouse[i].v_dest = <<-559.9400, -1606.5057, 26.0110>>
										SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].v_dest, 2.0)
									ELIF i = 1
										s_swat_indoor_pre_warehouse[i].v_dest = <<-561.5538, -1607.4127, 26.0110>>
										SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].v_dest, 2.0)
										//SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_PED(s_swat_indoor_pre_warehouse[i].ped, PLAYER_PED_ID(), <<0.0, 0.0, 0.0>>, 4.0)
									ENDIF
								
									SET_PED_COMBAT_PARAMS(s_swat_indoor_pre_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_pre_warehouse[i].ped, rel_group_swat)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_pre_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									
									s_swat_indoor_pre_warehouse[i].i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-559.968, -1610.005, 26.030>>, <<0.000, 0.000, 88.000>>)
										
									IF i = 0
										TASK_SYNCHRONIZED_SCENE(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].i_sync_scene, "misslamar1lam_1_ig_11", "lam_1_ig_11_swat_a", 
																NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
										SET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_pre_warehouse[i].i_sync_scene, 0.05)
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_pre_warehouse[i].ped, TRUE)
										SET_PED_FIRING_PATTERN(s_swat_indoor_pre_warehouse[i].ped, FIRING_PATTERN_FULL_AUTO)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_pre_warehouse[i].ped, TRUE)
									ELSE
										//TASK_SYNCHRONIZED_SCENE(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].i_sync_scene, "misslamar1lam_1_ig_11", "lam_1_ig_11_swat_b", 
										//						NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
										//SET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_pre_warehouse[i].i_sync_scene, 0.05)
										
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_pre_warehouse[i].ped, 100.0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_pre_warehouse[i].ped, FALSE)
									ENDIF
									
									IF i = 0
										PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_pre_warehouse[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
									ELSE
										SET_PED_CONFIG_FLAG(s_swat_indoor_pre_warehouse[i].ped, PCF_ShouldChargeNow, TRUE)
									ENDIF
									
									s_swat_indoor_pre_warehouse[i].i_timer = 0
									s_swat_indoor_pre_warehouse[i].i_event++
								ENDIF
							ELIF i = 2 //Charges forward after one guy is dead.
								IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_pre_warehouse) < 4
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-561.162231,-1611.551758,25.760828>>, <<-560.371948,-1601.496460,32.260826>>, 5.000000)
									s_swat_indoor_pre_warehouse[i].v_dest = <<-561.5297, -1609.8085, 26.0108>>
									
									SET_PED_COMBAT_PARAMS(s_swat_indoor_pre_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].v_dest, 2.0, TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_pre_warehouse[i].ped, rel_group_swat)
									
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_pre_warehouse[i].ped, 100.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_pre_warehouse[i].ped, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_pre_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_pre_warehouse[i].ped, TRUE)
									
									s_swat_indoor_pre_warehouse[i].i_timer = 0
									s_swat_indoor_pre_warehouse[i].i_event++
								ENDIF
							ELIF i = 3
								IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_pre_warehouse) < 3
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-561.162231,-1611.551758,25.760828>>, <<-560.371948,-1601.496460,32.260826>>, 5.000000)
									s_swat_indoor_pre_warehouse[i].v_dest = <<-560.7100, -1601.6761, 26.0108>>
									
									SET_PED_COMBAT_PARAMS(s_swat_indoor_pre_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].v_dest, 1.0, TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_pre_warehouse[i].ped, rel_group_swat)
									
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_pre_warehouse[i].ped, 100.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_pre_warehouse[i].ped, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_DISABLE_PINNED_DOWN, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_CAN_USE_PEEKING_VARIATIONS, FALSE)
									SET_COMBAT_FLOAT(s_swat_indoor_pre_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_pre_warehouse[i].ped, TRUE)
									
									s_swat_indoor_pre_warehouse[i].i_timer = 0
									s_swat_indoor_pre_warehouse[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF i < 2
								IF i = 0
									TRANSITION_FROM_MOCAP_TO_COMBAT(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].i_sync_scene, 0.9, TRUE)
								ELIF i = 1
									TRANSITION_FROM_MOCAP_TO_COMBAT(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].i_sync_scene, 0.77, TRUE)
								ENDIF
							ENDIF
						
							IF s_swat_indoor_pre_warehouse[i].i_timer = 0
								s_swat_indoor_pre_warehouse[i].i_timer = GET_GAME_TIMER()
							ELSE
								IF i < 2
									INT i_timer_threshold
									
									IF i = 0
										i_timer_threshold = 6000
									ELSE
										i_timer_threshold = 9000
									ENDIF
									
									IF GET_GAME_TIMER() - s_swat_indoor_pre_warehouse[i].i_timer > i_timer_threshold
									AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_pre_warehouse[i].i_sync_scene)
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_pre_warehouse[i].ped, FALSE)
										s_swat_indoor_pre_warehouse[i].i_event++
									ENDIF
								ELSE
									IF GET_GAME_TIMER() - s_swat_indoor_pre_warehouse[i].i_timer > 6000
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_pre_warehouse[i].ped, FALSE)
										
										IF i = 3
											SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_pre_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
										ENDIF
										
										s_swat_indoor_pre_warehouse[i].i_event++
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					IF s_swat_indoor_pre_warehouse[i].i_event > 1
						UPDATE_AI_PED_BLIP(s_swat_indoor_pre_warehouse[i].ped, s_swat_indoor_pre_warehouse[i].s_blip_data)
					ENDIF
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_pre_warehouse[i])
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
	
	
	IF NOT s_swat_indoor_warehouse[0].b_is_created
		IF s_swat_indoor_pre_warehouse[0].i_event > 0
		OR s_lamar.i_event >= BUDDY_EVENT_MID_FIGHT_CHECKPOINT //In case player jumps to the middle of the fight checkpoint
			IF HAS_MODEL_LOADED(model_gang)
			AND HAS_MODEL_LOADED(model_gang_alternate)
				IF NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[0].ped)
					s_swat_indoor_warehouse[0].ped = CREATE_ENEMY_PED(model_gang, <<-604.6722, -1614.3717, 26.0110>>, 1.7272, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[1].ped)
					s_swat_indoor_warehouse[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-604.7084, -1616.1466, 26.0110>>, 1.7272, rel_group_neutral, 165, 0, WEAPONTYPE_ASSAULTRIFLE)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[2].ped)
					s_swat_indoor_warehouse[2].ped = CREATE_ENEMY_PED(model_gang, <<-604.7386, -1617.9944, 26.0110>>, 1.7272, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[3].ped)
					s_swat_indoor_warehouse[3].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-605.5866, -1615.7777, 26.0393>>, 336.7832, rel_group_neutral, 135, 0, WEAPONTYPE_MICROSMG)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[4].ped)
					s_swat_indoor_warehouse[4].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-603.8036, -1605.4093, 26.6105>>, 267.3067, rel_group_neutral, 135, 0, WEAPONTYPE_MICROSMG)
				ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[5].ped)
					s_swat_indoor_warehouse[5].ped = CREATE_ENEMY_PED(model_gang, <<-605.4207, -1614.5223, 26.0108>>, 330.9266, rel_group_neutral, 135, 0, WEAPONTYPE_PUMPSHOTGUN)
					
					INITIALISE_ENEMY_GROUP(s_swat_indoor_warehouse, "Warehouse ")
					
					REPEAT COUNT_OF(s_swat_indoor_warehouse) i
						IF NOT IS_PED_INJURED(s_swat_indoor_warehouse[i].ped)
							SET_PED_PATH_CAN_USE_CLIMBOVERS(s_swat_indoor_warehouse[i].ped, FALSE)
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ELSE
		BOOL b_performed_area_check = FALSE
		BOOL b_time_to_trigger = FALSE
		BOOL b_player_explosion_in_area = FALSE
		VECTOR v_ped_pos

		REPEAT COUNT_OF(s_swat_indoor_warehouse) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_warehouse[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_warehouse[i].ped)
					v_ped_pos = GET_ENTITY_COORDS(s_swat_indoor_warehouse[i].ped)
				
					SWITCH s_swat_indoor_warehouse[i].i_event
						CASE 0
							IF NOT b_performed_area_check
								b_time_to_trigger = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-565.84,-1601.32,25.80>>, <<-571.33,-1600.83,29.01>>, 3.0)
													OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-568.24,-1607.67,30.48>>, <<-577.45,-1606.81,25.76>>, 16.75)
								b_performed_area_check = TRUE
							ENDIF
						
							IF b_time_to_trigger
								s_swat_indoor_warehouse[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_warehouse[i].i_event++
							ENDIF
						BREAK
					
						CASE 1
							//Have the peds enter in a SWAT line once the buddies are in the room.
							IF i < 3
								REQUEST_ANIM_DICT("misslamar1lam_1_ig_13")
							
								IF GET_GAME_TIMER() - s_swat_indoor_warehouse[i].i_timer > 1000
								AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_13")
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_warehouse[i].ped, rel_group_swat)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								
									IF i = 0
										s_swat_indoor_warehouse[i].v_dest = <<-586.1633, -1604.6453, 26.0110>>
										SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].cover, s_swat_indoor_warehouse[i].v_dest,
																	   270.8263, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
									ELIF i = 1
										s_swat_indoor_warehouse[i].v_dest = <<-590.7108, -1610.3392, 26.0110>>
										SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].cover, s_swat_indoor_warehouse[i].v_dest,
																	   272.9712, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
									ELIF i = 2
										s_swat_indoor_warehouse[i].v_dest = <<-584.5994, -1601.2083, 26.0110>>
										SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].cover, s_swat_indoor_warehouse[i].v_dest,
																	   270.8263, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
									ENDIF
									
									s_swat_indoor_warehouse[i].i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-589.947, -1610.480, 26.043>>, <<0.000, 0.000, 90.000>>)
									
									IF i = 0
										TASK_SYNCHRONIZED_SCENE(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].i_sync_scene, "misslamar1lam_1_ig_13", "lam_1_ig_13_a", 
																NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
									ELIF i = 1
										TASK_SYNCHRONIZED_SCENE(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].i_sync_scene, "misslamar1lam_1_ig_13", "lam_1_ig_13_b", 
																NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
									ELSE
										TASK_SYNCHRONIZED_SCENE(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].i_sync_scene, "misslamar1lam_1_ig_13", "lam_1_ig_13_c", 
																NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
									ENDIF
									
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_warehouse[i].ped, FALSE)
									
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									
									//s_swat_indoor_warehouse[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_warehouse[i].ped, TRUE)
									s_swat_indoor_warehouse[i].i_timer = 0
									s_swat_indoor_warehouse[i].i_event++
								ENDIF
							ELIF i = 3 //Second guy to come from exit.
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.693542,-1609.531250,26.010809>>, <<-588.661255,-1609.119873,31.010809>>, 6.750000)
								OR GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_warehouse) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_roof_1) < 4
									s_swat_indoor_warehouse[i].v_dest = <<-598.8683, -1611.3479, 26.0109>>

									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_warehouse[i].ped, rel_group_swat)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].v_dest, 2.0, TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_warehouse[i].ped, 50.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_warehouse[i].ped, FALSE)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_warehouse[i].ped, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_warehouse[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
									
									//s_swat_indoor_warehouse[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_warehouse[i].ped, TRUE)
									s_swat_indoor_warehouse[i].i_timer = 0
									s_swat_indoor_warehouse[i].i_event++
								ENDIF
							ELIF i = 4 //Stairs guy
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-606.036743,-1609.490234,25.297455>>, <<-584.571594,-1609.026245,31.760801>>, 7.000000)
									s_swat_indoor_warehouse[i].v_dest = <<-593.4334, -1608.6034, 26.0108>>
								
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_warehouse[i].ped, rel_group_swat)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].v_dest, 2.0, TRUE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_warehouse[i].ped, 100.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_warehouse[i].ped, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_ENTITY_INVINCIBLE(s_swat_indoor_warehouse[i].ped, FALSE)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_warehouse[i].ped, TRUE)
									PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_warehouse[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
									
									//s_swat_indoor_warehouse[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_warehouse[i].ped, TRUE)
									s_swat_indoor_warehouse[i].i_timer = 0
									s_swat_indoor_warehouse[i].i_event++
								ENDIF
							ELIF i = 5 //First guy to come from exit replaces warehouse 1 when he pushes forwards.
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.693542,-1609.531250,26.010809>>, <<-588.661255,-1609.119873,31.010809>>, 6.750000)
								OR (IS_PED_INJURED(s_swat_indoor_roof_1[3].ped) AND s_swat_indoor_roof_1[3].b_is_created)
									s_swat_indoor_warehouse[i].v_dest = <<-590.7108, -1610.3392, 26.0110>>
									
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_warehouse[i].ped, rel_group_swat)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_warehouse[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].v_dest, 2.0, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_warehouse[i].ped, 100.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_warehouse[i].ped, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_warehouse[i].ped, TRUE)
									
									//s_swat_indoor_warehouse[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_warehouse[i].ped, TRUE)
									s_swat_indoor_warehouse[i].i_timer = 0
									s_swat_indoor_warehouse[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF i < 3
								IF i = 0
									TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].i_sync_scene, s_swat_indoor_warehouse[i].i_timer, 
																  			   s_swat_indoor_warehouse[i].v_dest, 0.9, 0.5, TRUE, TRUE)
								ELIF i = 1
									TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].i_sync_scene, s_swat_indoor_warehouse[i].i_timer, 
																  			   s_swat_indoor_warehouse[i].v_dest, 0.7, 0.5, TRUE, TRUE)								   
								ELSE
									TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].i_sync_scene, s_swat_indoor_warehouse[i].i_timer, 
																  			   s_swat_indoor_warehouse[i].v_dest, 0.99, 0.5, TRUE, FALSE)
								ENDIF
								IF i = 1
									//Have this ped push forward if the ped in front dies.
									IF NOT ARE_VECTORS_ALMOST_EQUAL(s_swat_indoor_warehouse[i].v_dest, <<-584.7054, -1608.4663, 26.0108>>, 0.5)
										IF IS_PED_INJURED(s_swat_indoor_roof_1[3].ped)
										AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_warehouse[i].i_sync_scene)
											s_swat_indoor_warehouse[i].v_dest = <<-584.7054, -1608.4663, 26.0108>>
											SET_PED_COMBAT_MOVEMENT(s_swat_indoor_warehouse[i].ped, CM_DEFENSIVE)
											SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].v_dest, 2.0, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_CAN_CHARGE, TRUE)
											SET_COMBAT_FLOAT(s_swat_indoor_warehouse[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
											PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_warehouse[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//For the peds at the back: after a certain amount of time set them to damageable by anyone.
								IF s_swat_indoor_warehouse[i].i_timer = 0
									s_swat_indoor_warehouse[i].i_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - s_swat_indoor_warehouse[i].i_timer > 10000
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_warehouse[i].ped, FALSE)
									s_swat_indoor_warehouse[i].i_timer = GET_GAME_TIMER()
								ENDIF
							ENDIF
							
							//If any of the peds are caught charging while the buddies are pushing forwards then make sure they go back to their spots.
							IF NOT ARE_VECTORS_ALMOST_EQUAL(s_swat_indoor_warehouse[i].v_dest, v_ped_pos, 1.0)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
								OR IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
									IF NOT IS_BIT_SET(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)
									AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_warehouse[i].i_sync_scene)
										//Don't go back to the sphere if it involves passing Lamar and Stretch.
										IF VDIST2(v_ped_pos, s_swat_indoor_warehouse[i].v_dest) < VDIST2(v_lamar_pos, s_swat_indoor_warehouse[i].v_dest)
										AND VDIST2(v_ped_pos, s_swat_indoor_warehouse[i].v_dest) < VDIST2(v_stretch_pos, s_swat_indoor_warehouse[i].v_dest)
											SET_PED_COMBAT_MOVEMENT(s_swat_indoor_warehouse[i].ped, CM_DEFENSIVE)
											SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].v_dest, 2.0, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_warehouse[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_warehouse[i].ped, FALSE)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_warehouse[i].ped, 100.0)
											SET_BIT(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)
										CLEAR_BIT(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					b_player_explosion_in_area = FALSE
					
					IF IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, v_ped_pos - <<5.0, 5.0, 5.0>>, v_ped_pos + <<5.0, 5.0, 5.0>>)
					AND NOT IS_EXPLOSION_IN_AREA(EXP_TAG_GAS_CANISTER, v_ped_pos - <<5.0, 5.0, 5.0>>, v_ped_pos + <<5.0, 5.0, 5.0>>)
						IF GET_GAME_TIMER() - i_gas_explosion_timer > 1000
							b_player_explosion_in_area = TRUE
						ENDIF
					ENDIF
					
					//Make the peds invincible to the gas explosion, except if the player has grenades.
					IF NOT IS_BIT_SET(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_EXPLOSION_PROOF)
						IF s_warehouse_fire[0].f_scale < 0.5
							IF NOT b_player_explosion_in_area
								SET_ENTITY_PROOFS(s_swat_indoor_warehouse[i].ped, FALSE, FALSE, TRUE, FALSE, FALSE)
								CLEAR_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_warehouse[i].ped)
								
								SET_BIT(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_EXPLOSION_PROOF)
							ENDIF
						ENDIF
					ELSE	
						IF s_warehouse_fire[0].f_scale > 0.5
						OR b_player_explosion_in_area
							SET_ENTITY_PROOFS(s_swat_indoor_warehouse[i].ped, FALSE, FALSE, FALSE, FALSE, FALSE)
							CLEAR_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_warehouse[i].ped)
							
							CLEAR_BIT(s_swat_indoor_warehouse[i].i_bitset, ENEMY_FLAG_EXPLOSION_PROOF)
						ENDIF
					ENDIF
					
					IF s_swat_indoor_warehouse[i].i_event > 1
						BOOL bForceBlip = IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_warehouse[i].i_sync_scene)
						UPDATE_AI_PED_BLIP(s_swat_indoor_warehouse[i].ped, s_swat_indoor_warehouse[i].s_blip_data, -1, NULL, bForceBlip)
					ENDIF
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_warehouse[i])
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF	
		
	//Formerly skylight SWAT, these gang members are already in the room.
	IF NOT s_swat_indoor_roof_1[0].b_is_created
		IF s_swat_indoor_pre_warehouse[0].i_event > 0
		OR s_lamar.i_event >= BUDDY_EVENT_MID_FIGHT_CHECKPOINT //In case player jumps to the middle of the fight checkpoint
			IF VDIST2(<<-579.9283, -1602.4014, 34.2005>>, v_player_pos) < 2500.0
				REQUEST_MODEL(model_gas_can)
				REQUEST_CLIP_SET("move_ped_strafing")
			
				IF HAS_MODEL_LOADED(model_gang)
				AND HAS_MODEL_LOADED(model_gang_alternate)
				AND HAS_MODEL_LOADED(model_gas_can)
				AND HAS_CLIP_SET_LOADED("move_ped_strafing")
					IF NOT DOES_ENTITY_EXIST(s_swat_indoor_roof_1[0].ped)
						s_swat_indoor_roof_1[0].ped = CREATE_ENEMY_PED(model_gang, <<-578.6725, -1606.9213, 26.0108>>, 298.2048, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_roof_1[1].ped)
						s_swat_indoor_roof_1[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-593.8203, -1597.9493, 26.0108>>, 271.5309, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_roof_1[2].ped)
						s_swat_indoor_roof_1[2].ped = CREATE_ENEMY_PED(model_gang, <<-601.3184, -1598.0675, 29.4102>>, 264.0741, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_roof_1[3].ped)
						s_swat_indoor_roof_1[3].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-584.7054, -1608.4663, 26.0108>>, 270.8387, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_roof_1[4].ped)
						s_swat_indoor_roof_1[4].ped = CREATE_ENEMY_PED(model_gang, <<-588.8279, -1608.2739, 26.0108>>, 284.8407, rel_group_neutral, 115, 0, WEAPONTYPE_PUMPSHOTGUN)
					
						INITIALISE_ENEMY_GROUP(s_swat_indoor_roof_1, "Roof ")
						
						REPEAT COUNT_OF(s_swat_indoor_roof_1) i 	
							IF NOT IS_PED_INJURED(s_swat_indoor_roof_1[i].ped)
								IF i = 2
									SET_PED_SUFFERS_CRITICAL_HITS(s_swat_indoor_roof_1[i].ped, FALSE)
									SET_PED_STRAFE_CLIPSET(s_swat_indoor_roof_1[i].ped, "move_ped_strafing")
								ENDIF
								
								SET_PED_PATH_CAN_USE_CLIMBOVERS(s_swat_indoor_roof_1[i].ped, FALSE)
							ENDIF
						ENDREPEAT
						
						IF NOT IS_PED_INJURED(s_swat_indoor_roof_1[4].ped)
							TASK_AIM_GUN_AT_COORD(s_swat_indoor_roof_1[4].ped, <<-567.5737, -1601.1451, 27.5852>>, -1)
						ENDIF
						
						obj_warehouse_gas = CREATE_OBJECT(model_gas_can, <<-588.2625, -1600.5623, 26.0109>>) //<<-589.9410, -1602.7491, 26.0109>>)
						FREEZE_ENTITY_POSITION(obj_warehouse_gas, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_gas_can)
						s_warehouse_fire[0].i_timer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//All of the enemies are triggered off the same locate, so do outside the loop.
		BOOL b_time_to_trigger = FALSE
		BOOL b_checked_trigger = FALSE
		BOOL b_player_explosion_in_area = FALSE
		VECTOR v_ped_pos
	
		REPEAT COUNT_OF(s_swat_indoor_roof_1) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_roof_1[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_roof_1[i].ped)
					v_ped_pos = GET_ENTITY_COORDS(s_swat_indoor_roof_1[i].ped)
				
					SWITCH s_swat_indoor_roof_1[i].i_event
						CASE 0
							IF NOT b_checked_trigger
								b_time_to_trigger = ARE_PEDS_IN_ANGLED_AREA(PLAYER_PED_ID(), s_lamar.ped, s_stretch.ped, <<-565.84,-1601.32,25.80>>, <<-571.33,-1600.83,29.01>>, 3.0)
													OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-568.24,-1607.67,30.48>>, <<-577.45,-1606.81,25.76>>, 16.75)
								b_checked_trigger = TRUE
							ENDIF
						
							IF b_time_to_trigger
								IF i = 0
									s_swat_indoor_roof_1[i].v_dest = <<-577.7428, -1603.1418, 26.0108>>
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].cover, s_swat_indoor_roof_1[i].v_dest, 276.7884,
																   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_roof_1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								ELIF i = 1
									s_swat_indoor_roof_1[i].v_dest = <<-592.6339, -1602.4424, 26.0108>>
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].cover, s_swat_indoor_roof_1[i].v_dest, 264.6638,
																   2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_roof_1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								ELIF i = 2
									s_swat_indoor_roof_1[i].v_dest = <<-601.3184, -1598.0675, 29.4102>>
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].v_dest, 1.0, TRUE)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_roof_1[i].ped, 10, CM_STATIONARY, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
								ELIF i = 3
									s_swat_indoor_roof_1[i].v_dest = <<-584.7054, -1608.4663, 26.0108>>
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].cover, s_swat_indoor_roof_1[i].v_dest, 270.8387,
																   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_roof_1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
								ELIF i = 4
									s_swat_indoor_roof_1[i].v_dest = <<-575.7058, -1606.3470, 26.0108>>
									SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].v_dest, 2.0, TRUE)
									SET_PED_COMBAT_PARAMS(s_swat_indoor_roof_1[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
									SET_ENTITY_IS_TARGET_PRIORITY(s_swat_indoor_roof_1[i].ped, TRUE)
								ENDIF
								
								SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_roof_1[i].ped, rel_group_swat)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_roof_1[i].ped, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_roof_1[i].ped, 100.0)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_roof_1[i].ped, TRUE)
								
								IF i = 0 OR i = 4
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_roof_1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_roof_1[i].ped, CA_CAN_CHARGE, TRUE)
									SET_COMBAT_FLOAT(s_swat_indoor_roof_1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								ENDIF
								
								IF i = 2
									//Block ragdoll on the machine ped so they can be animated later when shot.		
									SET_PED_CAN_RAGDOLL(s_swat_indoor_roof_1[i].ped, FALSE)
									SET_PED_SUFFERS_CRITICAL_HITS(s_swat_indoor_roof_1[i].ped, FALSE)
								ENDIF

								b_machine_swat_ragdoll_on = FALSE
								s_swat_indoor_roof_1[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_roof_1[i].i_event++
							ENDIF	
						BREAK
												
						CASE 1 
							//Switch off disabling buddy damage.
							IF GET_GAME_TIMER() - s_swat_indoor_roof_1[i].i_timer > 10000
							AND (i != 4 OR (i = 4 AND e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE))						
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_roof_1[i].ped, FALSE)
								
								IF i = 0
									s_swat_indoor_roof_1[i].i_timer = 0
									s_swat_indoor_roof_1[i].i_event++
								ENDIF
							ENDIF
						
							IF i = 1 //Have the ped behind the fire move to a different location after the explosion.
								IF s_warehouse_fire[0].f_scale > 0.0
									s_swat_indoor_roof_1[i].v_dest = <<-591.0416, -1605.2275, 26.0108>>
									SET_PED_COVER_POINT_AND_SPHERE(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].cover, s_swat_indoor_roof_1[i].v_dest, 329.4291,
																   2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120, TRUE)
									SET_PED_PATH_CAN_USE_CLIMBOVERS(s_swat_indoor_roof_1[i].ped, FALSE)
									
									s_swat_indoor_roof_1[i].i_event++
								ENDIF
							ELIF i = 2 //Send the ped standing on the machine over the rails when shot.
								REQUEST_ANIM_DICT("misslamar1lam_1_ig_16")
							
								//If the player goes near the machine ped turn ragdoll on, so they can be shot normally without a synced scene.
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-598.544556,-1604.523315,25.770239>>, <<-605.583984,-1604.193970,31.827965>>, 15.000000)
									IF NOT b_machine_swat_ragdoll_on
										SET_PED_CAN_RAGDOLL(s_swat_indoor_roof_1[i].ped, TRUE)
										b_machine_swat_ragdoll_on = TRUE
									ENDIF
								ELIF b_machine_swat_ragdoll_on
									SET_PED_CAN_RAGDOLL(s_swat_indoor_roof_1[i].ped, FALSE)	
									b_machine_swat_ragdoll_on = FALSE
								ENDIF
							
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_swat_indoor_roof_1[i].ped, PLAYER_PED_ID())
									IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_16")
									AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-598.544556,-1604.523315,25.770239>>, <<-605.583984,-1604.193970,31.827965>>, 15.000000)
									AND VDIST(GET_ENTITY_COORDS(s_swat_indoor_roof_1[i].ped), GET_ANIM_INITIAL_OFFSET_POSITION("misslamar1lam_1_ig_16", "lam_1_ig_16_swat", 
										<<-594.450, -1602.850, 26.000>>, <<0.000, 0.000, 10.000>>)) < 1.25
										s_swat_indoor_roof_1[i].i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-594.450, -1602.850, 26.000>>, <<0.000, 0.000, 10.000>>)
									
										TASK_SYNCHRONIZED_SCENE(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].i_sync_scene, "misslamar1lam_1_ig_16", "lam_1_ig_16_swat", 
																NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_roof_1[i].ped, TRUE)
									ELSE
										//If we can't play the anim just switch to ragdoll.
										CLEAR_PED_TASKS(s_swat_indoor_roof_1[i].ped)
										SET_PED_CAN_RAGDOLL(s_swat_indoor_roof_1[i].ped, TRUE)
										SET_PED_TO_RAGDOLL_WITH_FALL(s_swat_indoor_roof_1[i].ped, 10000, 20000, TYPE_FROM_HIGH, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, -90.0>>), 
																	 26.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
										APPLY_FORCE_TO_ENTITY(s_swat_indoor_roof_1[i].ped, APPLY_TYPE_IMPULSE, <<2.0, 0.0, 0.25>>, <<0.0, 0.0, 0.0>>, 
														  	  GET_PED_RAGDOLL_BONE_INDEX(s_swat_indoor_roof_1[i].ped, RAGDOLL_CLAVICLE_L), FALSE, TRUE, TRUE)
									ENDIF
									
									IF DOES_BLIP_EXIST(s_swat_indoor_roof_1[i].blip)
										REMOVE_BLIP(s_swat_indoor_roof_1[i].blip)
									ENDIF
									
									SET_PED_CAN_BE_TARGETTED(s_swat_indoor_roof_1[i].ped, FALSE)
									
									s_swat_indoor_roof_1[i].i_timer = GET_GAME_TIMER()
									s_swat_indoor_roof_1[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF i = 2
								IF IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_roof_1[i].i_sync_scene)
									IF GET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_roof_1[i].i_sync_scene) > 0.945
										SET_PED_CAN_RAGDOLL(s_swat_indoor_roof_1[i].ped, TRUE)
										SET_PED_TO_RAGDOLL_WITH_FALL(s_swat_indoor_roof_1[i].ped, 10000, 20000, TYPE_FROM_HIGH, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, -90.0>>), 
																	 26.0, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
										
										SET_ENTITY_HEALTH(s_swat_indoor_roof_1[i].ped, 0)
									
										s_swat_indoor_roof_1[i].i_timer = 0
										s_swat_indoor_roof_1[i].i_event++
									ENDIF
								ELIF GET_GAME_TIMER() - s_swat_indoor_roof_1[i].i_timer > 3000							
									SET_ENTITY_HEALTH(s_swat_indoor_roof_1[i].ped, 0)
									
									s_swat_indoor_roof_1[i].i_timer = 0
									s_swat_indoor_roof_1[i].i_event++
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					//Sometimes the ped will just walk off the ledge, if this happens just kill the ped for now as it causes issues.
					IF i = 2
						VECTOR v_machine_ped_pos = GET_ENTITY_COORDS(s_swat_indoor_roof_1[i].ped) 
						
						IF v_machine_ped_pos.z < 28.0
							APPLY_DAMAGE_TO_PED(s_swat_indoor_roof_1[i].ped, 200, TRUE)
						ENDIF
					ENDIF
					
					//If any of the peds are caught charging while the buddies are pushing forwards then make sure they go back to their spots.	
					IF i != 2
						IF NOT IS_BIT_SET(s_swat_indoor_roof_1[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)						
							IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							OR IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)							
								IF NOT ARE_VECTORS_ALMOST_EQUAL(s_swat_indoor_roof_1[i].v_dest, v_ped_pos, 1.0)
									IF VDIST2(v_ped_pos, s_swat_indoor_roof_1[i].v_dest) < VDIST2(v_lamar_pos, s_swat_indoor_roof_1[i].v_dest)
									AND VDIST2(v_ped_pos, s_swat_indoor_roof_1[i].v_dest) < VDIST2(v_stretch_pos, s_swat_indoor_roof_1[i].v_dest)
										SET_PED_COMBAT_MOVEMENT(s_swat_indoor_roof_1[i].ped, CM_DEFENSIVE)
										SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].v_dest, 2.0, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_roof_1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
										SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_roof_1[i].ped, FALSE)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_roof_1[i].ped, 100.0)
										SET_BIT(s_swat_indoor_roof_1[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
								CLEAR_BIT(s_swat_indoor_roof_1[i].i_bitset, ENEMY_FLAG_CANCELLED_CHARGE)
							ENDIF
						ENDIF
					ENDIF
					
					b_player_explosion_in_area = FALSE
					
					IF IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, v_ped_pos - <<5.0, 5.0, 5.0>>, v_ped_pos + <<5.0, 5.0, 5.0>>)
					AND NOT IS_EXPLOSION_IN_AREA(EXP_TAG_GAS_CANISTER, v_ped_pos - <<5.0, 5.0, 5.0>>, v_ped_pos + <<5.0, 5.0, 5.0>>)
						IF GET_GAME_TIMER() - i_gas_explosion_timer > 1000
							b_player_explosion_in_area = TRUE
						ENDIF
					ENDIF
					
					//Make the peds invincible to the gas explosion, except if the player has grenades.
					IF NOT IS_BIT_SET(s_swat_indoor_roof_1[i].i_bitset, ENEMY_FLAG_EXPLOSION_PROOF)
						IF s_warehouse_fire[0].f_scale < 0.5
							IF NOT b_player_explosion_in_area
								SET_ENTITY_PROOFS(s_swat_indoor_roof_1[i].ped, FALSE, FALSE, TRUE, FALSE, FALSE)
								SET_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_roof_1[i].ped, RBF_EXPLOSION)
								SET_BIT(s_swat_indoor_roof_1[i].i_bitset, ENEMY_FLAG_EXPLOSION_PROOF)
								
								CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_swat_indoor_roof_1 set to explosion proof. ", i)
							ENDIF
						ENDIF
					ELSE					
						IF s_warehouse_fire[0].f_scale >= 0.5
						OR b_player_explosion_in_area
							SET_ENTITY_PROOFS(s_swat_indoor_roof_1[i].ped, FALSE, FALSE, FALSE, FALSE, FALSE)
							SET_RAGDOLL_BLOCKING_FLAGS(s_swat_indoor_roof_1[i].ped, RBF_NONE)
							CLEAR_BIT(s_swat_indoor_roof_1[i].i_bitset, ENEMY_FLAG_EXPLOSION_PROOF)
							
							IF i = 1 OR i = 3
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_roof_1[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_roof_1[i].ped, CA_CAN_CHARGE, TRUE)
								SET_COMBAT_FLOAT(s_swat_indoor_roof_1[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								
								CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "s_swat_indoor_roof_1 set to die from explosions. ", i)
							ENDIF
						ENDIF
					ENDIF
					
					
					IF s_swat_indoor_roof_1[i].i_event > 0
						UPDATE_AI_PED_BLIP(s_swat_indoor_roof_1[i].ped, s_swat_indoor_roof_1[i].s_blip_data)
					ENDIF
				ELSE				
					CLEAN_UP_ENEMY_PED(s_swat_indoor_roof_1[i])
					
					IF i = 2
						REMOVE_CLIP_SET("move_ped_strafing")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
		
		//If the gas can is shot, start paper fire: use a bigger locate once the swat ped lands behind it.
		IF s_warehouse_fire[0].f_scale = 0.0
			BOOL bBulletInLargerArea = IS_BULLET_IN_ANGLED_AREA(<<-588.902283,-1600.487915,26.017223>>, <<-588.291809,-1600.517700,28.750704>>, 2.7500000, FALSE)
			BOOL bPlayerBulletInArea = IS_BULLET_IN_ANGLED_AREA(<<-588.902283,-1600.487915,26.017223>>, <<-588.291809,-1600.517700,27.50704>>, 1.500000, TRUE)
		
			IF (bPlayerBulletInArea AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-582.857605,-1605.419312,25.760828>>, <<-568.185852,-1607.004272,31.547653>>, 17.500000))
			OR (bBulletInLargerArea AND NOT bPlayerBulletInArea AND GET_GAME_TIMER() - s_warehouse_fire[0].i_timer > 4000)
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_warehouse_gas, <<0.5, 0.0, 0.0>>), GET_ENTITY_COORDS(obj_warehouse_gas), 
												   1, TRUE, WEAPONTYPE_PUMPSHOTGUN, PLAYER_PED_ID(), FALSE)
			ENDIF

			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(obj_warehouse_gas, PLAYER_PED_ID())
				ADD_EXPLOSION(GET_ENTITY_COORDS(obj_warehouse_gas), EXP_TAG_GAS_CANISTER, 0.1, TRUE, FALSE, 0.1)
				//DELETE_OBJECT(obj_warehouse_gas)
				PLAY_SOUND_FROM_COORD(i_fire_sound, "LAMAR1_WAREHOUSE_FIRE", <<-591.8, -1602.6, 27.3>>)
				s_warehouse_fire[0].f_scale = 0.3
				s_warehouse_fire_2[0].f_scale = 0.1
				i_gas_explosion_timer = GET_GAME_TIMER()
				
				//Record footage of the gas explosion.
				REPLAY_RECORD_BACK_FOR_TIME(1.0)
				
				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "Triggered gas can explosion.")
				
				//Kill off the ped behind the gas can.
				IF NOT IS_PED_INJURED(s_swat_indoor_roof_1[1].ped)
					APPLY_DAMAGE_TO_PED(s_swat_indoor_roof_1[1].ped, 500, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//SWAT members breach another door as the player enters the corridor
	IF NOT s_swat_indoor_breach_2[0].b_is_created
		IF s_swat_indoor_roof_1[0].i_event > 0
			REQUEST_PTFX_ASSET()
		
			IF VDIST2(<<-605.9154, -1628.3444, 26.0109>>, v_player_pos) < 10000.0
				IF HAS_MODEL_LOADED(model_gang)
				AND HAS_MODEL_LOADED(model_gang_alternate)
				AND HAS_PTFX_ASSET_LOADED()
				AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\LAMAR1_01")
					IF NOT DOES_ENTITY_EXIST(s_swat_indoor_breach_2[0].ped)
						s_swat_indoor_breach_2[0].ped = CREATE_ENEMY_PED(model_gang, <<-605.9154, -1628.3444, 26.0109>>, 355.7177, rel_group_neutral, 135, 0, WEAPONTYPE_ASSAULTRIFLE)
					ELIF NOT DOES_ENTITY_EXIST(s_swat_indoor_breach_2[1].ped)
						s_swat_indoor_breach_2[1].ped = CREATE_ENEMY_PED(model_gang_alternate, <<-604.5666, -1630.3938, 26.0109>>, 359.9206, rel_group_neutral, 135, 0, WEAPONTYPE_PISTOL)
											
						REPEAT COUNT_OF(s_swat_indoor_breach_2) i
							IF NOT IS_PED_INJURED(s_swat_indoor_breach_2[i].ped)
								SET_ENTITY_INVINCIBLE(s_swat_indoor_breach_2[i].ped, TRUE)
								SET_PED_CAN_RAGDOLL(s_swat_indoor_breach_2[i].ped, FALSE)
							ENDIF
						ENDREPEAT
																	
						INITIALISE_ENEMY_GROUP(s_swat_indoor_breach_2, "Breach 2 ")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//All of the enemies are triggered off the same locate, so do outside the loop.
		BOOL b_time_to_trigger = FALSE
		IF s_swat_indoor_breach_2[0].i_event = 0
			IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse)
			AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.650085,-1626.159424,26.010805>>, <<-604.425659,-1612.358643,28.647781>>, 3.500000)
				OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-605.610901,-1626.215576,25.762581>>, <<-605.393494,-1623.640869,28.760939>>, 4.000000)
					b_time_to_trigger = TRUE
				ENDIF
			ENDIF
		ENDIF
	
		REPEAT COUNT_OF(s_swat_indoor_breach_2) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_breach_2[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_breach_2[i].ped)
					IF s_swat_indoor_breach_2[i].i_event = 0
						IF i = 0
							IF GET_GAME_TIMER() - s_swat_indoor_breach_2[0].i_timer > 1000
								SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-604.3, -1626.4, 27.2>>, TRUE, 0.0)
								SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-606.9, -1626.2, 27.2>>, TRUE, 0.0)
								
								s_swat_indoor_breach_2[0].i_timer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					
						IF b_time_to_trigger
							IF i = 0							
								s_swat_indoor_breach_2[i].v_dest = <<-606.2178, -1621.5380, 26.0108>>
								
								SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-604.3, -1626.4, 27.2>>, FALSE, 0.0)
								SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-606.9, -1626.2, 27.2>>, FALSE, 0.0)
								
								OBJECT_INDEX obj_door_1 = GET_CLOSEST_OBJECT_OF_TYPE(<<-604.3, -1626.4, 27.2>>, 2.0, V_ILEV_RC_DOOR1, FALSE)
								OBJECT_INDEX obj_door_2 = GET_CLOSEST_OBJECT_OF_TYPE(<<-606.9, -1626.2, 27.2>>, 2.0, V_ILEV_RC_DOOR1, FALSE)
								
								APPLY_FORCE_TO_ENTITY(obj_door_1, APPLY_TYPE_IMPULSE, <<0.0, 16.0, 0.0>>, <<1.3077, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
								APPLY_FORCE_TO_ENTITY(obj_door_2, APPLY_TYPE_IMPULSE, <<0.0, 16.0, 0.0>>, <<1.3077, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
								
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<-605.6500, -1627.2021, 27.2032>>, <<-605.5878, -1625.3854, 27.0974>>, 1, TRUE, WEAPONTYPE_PUMPSHOTGUN, NULL, FALSE)
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<-605.6500, -1627.2021, 27.6032>>, <<-605.5878, -1625.3854, 27.6974>>, 1, TRUE, WEAPONTYPE_PUMPSHOTGUN, NULL, FALSE)
								START_PARTICLE_FX_NON_LOOPED_AT_COORD(str_door_breach_ptfx, <<-605.65, -1626.35, 27.24>>, <<0.0, 0.0, -5.23>>, 1.0)
								
								PLAY_SOUND_FROM_COORD(-1, "LAMAR1_BustDoorOpen1", <<-605.65, -1626.35, 27.24>>)
								PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_breach_2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
								
								//Record footage of the door explosion.
								REPLAY_RECORD_BACK_FOR_TIME(1.0)
								
								//Now is the time to unlock the exit.
								SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_UNLOCKED)
								SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_UNLOCKED)
							ELIF i = 1
								s_swat_indoor_breach_2[i].v_dest = <<-604.7348, -1622.4652, 26.0108>>
							ENDIF							
							
							SET_ENTITY_INVINCIBLE(s_swat_indoor_breach_2[i].ped, FALSE)
							SET_PED_CAN_RAGDOLL(s_swat_indoor_breach_2[i].ped, TRUE)
							SET_PED_CAN_BE_TARGETTED(s_swat_indoor_breach_2[i].ped, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_breach_2[i].ped, rel_group_swat)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_breach_2[i].ped, TRUE)
							
							SET_PED_COMBAT_PARAMS(s_swat_indoor_breach_2[i].ped, 10, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_NEAR, TLR_NEVER_LOSE_TARGET)
							SET_PED_SPHERE_DEFENSIVE_AREA(s_swat_indoor_breach_2[i].ped, s_swat_indoor_breach_2[i].v_dest , 2.0, TRUE)
							
							SEQ_GO_TO_COORD_WHILE_AIMING_THEN_AIM(s_swat_indoor_breach_2[i].ped, s_swat_indoor_breach_2[i].v_dest , PEDMOVE_RUN, <<0.0, 0.0, 0.0>>, PLAYER_PED_ID(), FALSE,
																  0.5, 1.0, TRUE, TRUE, FALSE)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(model_gang)
							SET_MODEL_AS_NO_LONGER_NEEDED(model_gang_alternate)
														
							//s_swat_indoor_breach_2[i].blip = CREATE_BLIP_FOR_PED(s_swat_indoor_breach_2[i].ped, TRUE)
							b_force_swat_dialogue = TRUE
							s_swat_indoor_breach_2[i].i_timer = 0
							s_swat_indoor_breach_2[i].i_event++
						ENDIF
					ELSE
						UPDATE_AI_PED_BLIP(s_swat_indoor_breach_2[i].ped, s_swat_indoor_breach_2[i].s_blip_data)
						
						IF s_swat_indoor_breach_2[i].i_event = 1
							IF s_swat_indoor_breach_2[i].i_timer = 0
								IF VDIST2(s_swat_indoor_breach_2[i].v_dest, GET_ENTITY_COORDS(s_swat_indoor_breach_2[i].ped)) < 9.0
									s_swat_indoor_breach_2[i].i_timer = GET_GAME_TIMER()
								ENDIF
							ELIF GET_GAME_TIMER() - s_swat_indoor_breach_2[i].i_timer > 250
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_swat_indoor_breach_2[i].ped, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_breach_2[i].ped, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_breach_2[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(s_swat_indoor_breach_2[i].ped, CA_CAN_CHARGE, TRUE)
								SET_COMBAT_FLOAT(s_swat_indoor_breach_2[i].ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
								
								IF i = 1
									PLAY_PED_AMBIENT_SPEECH(s_swat_indoor_breach_2[i].ped, "GENERIC_WAR_CRY", SPEECH_PARAMS_FORCE)
								ENDIF
								
								s_swat_indoor_breach_2[i].i_timer = GET_GAME_TIMER()
								s_swat_indoor_breach_2[i].i_event++
							ENDIF
						ELIF s_swat_indoor_breach_2[i].i_event = 2
							IF GET_GAME_TIMER() - s_swat_indoor_breach_2[i].i_timer > 4000
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_breach_2[i].ped, FALSE)
								s_swat_indoor_breach_2[i].i_event++
							ENDIF
						ENDIF
					ENDIF
					
					//Kill them instantly if the buddies shoot them.
					/*IF NOT IS_PED_INJURED(s_lamar.ped)
					AND NOT IS_PED_INJURED(s_stretch.ped)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_swat_indoor_breach_2[i].ped, s_lamar.ped)
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_swat_indoor_breach_2[i].ped, s_stretch.ped)
							SET_ENTITY_HEALTH(s_swat_indoor_breach_2[i].ped, 0)
						ENDIF
					ENDIF*/
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_breach_2[i])
					
					REMOVE_PTFX_ASSET()
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\LAMAR1_01")
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
	
	//Final chopper: overlooks the player as they leave the building. Has to be killed.
	IF NOT s_swat_indoor_chopper_final[0].b_is_created
		IF s_swat_indoor_breach_2[0].i_event > 0
			REQUEST_MODEL(model_chopper)
			REQUEST_MODEL(model_swat)
			REQUEST_VEHICLE_RECORDING(CARREC_INDOOR_CHOPPER_2, str_carrec)
			
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222082
			
			IF HAS_MODEL_LOADED(model_swat)
			AND HAS_MODEL_LOADED(model_chopper)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_INDOOR_CHOPPER_2, str_carrec)
				veh_chopper_indoor_final = CREATE_VEHICLE(model_chopper, <<-531.7855, -1650.4025, 46.0368>>, 0.0)
				SET_VEHICLE_LIVERY(veh_chopper_indoor_final, 0)
				SET_VEHICLE_ENGINE_ON(veh_chopper_indoor_final, TRUE, TRUE)
				SET_VEHICLE_SIREN(veh_chopper_indoor_final, TRUE)
				SET_VEHICLE_LIGHTS(veh_chopper_indoor_final, FORCE_VEHICLE_LIGHTS_ON)
				SET_ENTITY_COLLISION(veh_chopper_indoor_final, FALSE)
				SET_HELI_BLADES_FULL_SPEED(veh_chopper_indoor_final)
				START_PLAYBACK_RECORDED_VEHICLE(veh_chopper_indoor_final, CARREC_INDOOR_CHOPPER_2, str_carrec)
				SET_ENTITY_ALWAYS_PRERENDER(veh_chopper_indoor_final, TRUE)
				SET_PLAYBACK_SPEED(veh_chopper_indoor_final, 0.0)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_chopper)
				f_chopper_playback_speed = 1.0
				b_already_set_chopper_to_explode = FALSE
				i_chopper_death_counter = 0
				
				//Pilot
				s_swat_indoor_chopper_final[0].ped = CREATE_ENEMY_PED(model_swat, <<-531.7855, -1650.4025, 47.0368>>, 0.0, rel_group_neutral, 200, 0, WEAPONTYPE_UNARMED)
				SET_PED_INTO_VEHICLE(s_swat_indoor_chopper_final[0].ped, veh_chopper_indoor_final)
				SET_ENTITY_INVINCIBLE(s_swat_indoor_chopper_final[0].ped, TRUE) //The chopper recording starts inside collision, this prevents the ped dying from this.
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_chopper_final[0].ped, TRUE)
				
				s_swat_indoor_chopper_final[1].ped = CREATE_ENEMY_PED(model_swat, <<-531.7855, -1650.4025, 48.0368>>, 0.0, rel_group_neutral, 200, 0, WEAPONTYPE_CARBINERIFLE)
				SET_PED_INTO_VEHICLE(s_swat_indoor_chopper_final[1].ped, veh_chopper_indoor_final, VS_BACK_RIGHT)	
				SET_ENTITY_INVINCIBLE(s_swat_indoor_chopper_final[1].ped, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_chopper_final[1].ped, TRUE)
				
				s_swat_indoor_chopper_final[2].ped = CREATE_ENEMY_PED(model_swat, <<-531.7855, -1650.4025, 49.0368>>, 0.0, rel_group_neutral, 200, 0, WEAPONTYPE_CARBINERIFLE)
				SET_PED_INTO_VEHICLE(s_swat_indoor_chopper_final[2].ped, veh_chopper_indoor_final, VS_BACK_LEFT)	
				SET_ENTITY_INVINCIBLE(s_swat_indoor_chopper_final[2].ped, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_swat_indoor_chopper_final[2].ped, TRUE)
				
				INITIALISE_ENEMY_GROUP(s_swat_indoor_chopper_final, "Chopper-")
			ENDIF
		ENDIF
	ELSE
		BOOL b_trigger_now = FALSE
	
		REPEAT COUNT_OF(s_swat_indoor_chopper_final) i
			IF DOES_ENTITY_EXIST(s_swat_indoor_chopper_final[i].ped)
				IF NOT IS_PED_INJURED(s_swat_indoor_chopper_final[i].ped)
					SWITCH s_swat_indoor_chopper_final[i].i_event
						CASE 0 
							IF i = 0 //Only need to do the angled area check once for all peds in this group.
								b_trigger_now = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.51,-1631.69,25.98>>, <<-592.32,-1629.18,28.23>>, 1.000000, FALSE, TRUE, TM_ON_FOOT)
												OR IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-592.51,-1631.69,25.98>>, <<-592.32,-1629.18,28.23>>, 1.000000, FALSE, TRUE, TM_ON_FOOT)
												OR IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-592.51,-1631.69,25.98>>, <<-592.32,-1629.18,28.23>>, 1.000000, FALSE, TRUE, TM_ON_FOOT)
							ENDIF
						
							IF b_trigger_now
								SET_ENTITY_INVINCIBLE(s_swat_indoor_chopper_final[i].ped, FALSE)
								SET_PED_SUFFERS_CRITICAL_HITS(s_swat_indoor_chopper_final[i].ped, FALSE)
							
								IF i = 0
									IF IS_VEHICLE_DRIVEABLE(veh_chopper_indoor_final)
									AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_indoor_final)
									AND NOT IS_PED_INJURED(s_lamar.ped)
										SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh_chopper_indoor_final, 4500.0)
										FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(veh_chopper_indoor_final)
										SET_ENTITY_COLLISION(veh_chopper_indoor_final, TRUE)
										SET_VEHICLE_SEARCHLIGHT(veh_chopper_indoor_final, TRUE)
										SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_chopper_final[i].ped, rel_group_swat)
										TASK_VEHICLE_AIM_AT_PED(s_swat_indoor_chopper_final[i].ped, s_lamar.ped)
										ADD_ENTITY_TO_AUDIO_MIX_GROUP(veh_chopper_indoor_final, "LAMAR_1_HELI")
										
										s_swat_indoor_chopper_final[i].blip = CREATE_BLIP_FOR_ENTITY(veh_chopper_indoor_final, TRUE)
										s_swat_indoor_chopper_final[i].i_event++
									ENDIF
								ELSE
									//Gunner aims at player
									SET_PED_RELATIONSHIP_GROUP_HASH(s_swat_indoor_chopper_final[i].ped, rel_group_swat)
									SET_PED_COMBAT_AI(s_swat_indoor_chopper_final[i].ped, 1, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_USE_COVER, <<0.0, 0.0, 0.0>>, 0.0)
									SET_CURRENT_PED_WEAPON(s_swat_indoor_chopper_final[i].ped, WEAPONTYPE_CARBINERIFLE, TRUE)
									
									s_swat_indoor_chopper_final[i].i_timer = GET_GAME_TIMER()
									s_swat_indoor_chopper_final[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF i = 1 //The gunner seems to be losing his task, so keep it updated here.
								IF GET_GAME_TIMER() - s_swat_indoor_chopper_final[i].i_timer > 2000
								AND GET_SCRIPT_TASK_STATUS(s_swat_indoor_chopper_final[i].ped, SCRIPT_TASK_DRIVE_BY) != PERFORMING_TASK
									TASK_DRIVE_BY(s_swat_indoor_chopper_final[i].ped, PLAYER_PED_ID(), NULL, <<0.0, 0.0, 0.0>>, 200.0, 100, FALSE, FIRING_PATTERN_FULL_AUTO)
									SET_PED_SHOOT_RATE(s_swat_indoor_chopper_final[i].ped, 80)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_chopper_final[i].ped, TRUE)
								ENDIF
							ELIF i = 2
								IF IS_PED_INJURED(s_swat_indoor_chopper_final[1].ped)
									IF IS_VEHICLE_DRIVEABLE(veh_chopper_indoor_final)
										IF GET_PED_IN_VEHICLE_SEAT(veh_chopper_indoor_final, VS_BACK_RIGHT) != s_swat_indoor_chopper_final[i].ped
											IF IS_PED_IN_VEHICLE(s_swat_indoor_chopper_final[i].ped, veh_chopper_indoor_final)
											AND NOT IS_PED_RAGDOLL(s_swat_indoor_chopper_final[i].ped)
											AND IS_VEHICLE_SEAT_FREE(veh_chopper_indoor_final, VS_BACK_RIGHT)
												IF GET_SCRIPT_TASK_STATUS(s_swat_indoor_chopper_final[i].ped, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
												AND GET_SCRIPT_TASK_STATUS(s_swat_indoor_chopper_final[i].ped, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
													IF GET_GAME_TIMER() - s_swat_indoor_chopper_final[i].i_timer > 250
														TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(s_swat_indoor_chopper_final[i].ped, veh_chopper_indoor_final)
														SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_chopper_final[i].ped, TRUE)
														s_swat_indoor_chopper_final[i].i_timer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF
										ELSE
											s_swat_indoor_chopper_final[i].i_timer = GET_GAME_TIMER()
											s_swat_indoor_chopper_final[i].i_event++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF IS_VEHICLE_DRIVEABLE(veh_chopper_indoor_final)
								IF GET_SCRIPT_TASK_STATUS(s_swat_indoor_chopper_final[i].ped, SCRIPT_TASK_DRIVE_BY) != PERFORMING_TASK
									TASK_DRIVE_BY(s_swat_indoor_chopper_final[i].ped, PLAYER_PED_ID(), NULL, <<0.0, 0.0, 0.0>>, 200.0, 100, FALSE, FIRING_PATTERN_FULL_AUTO)
									SET_PED_SHOOT_RATE(s_swat_indoor_chopper_final[i].ped, 80)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_swat_indoor_chopper_final[i].ped, TRUE)
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					CLEAN_UP_ENEMY_PED(s_swat_indoor_chopper_final[i])
				ENDIF
			ENDIF
		ENDREPEAT
			
			
		IF NOT IS_ENTITY_DEAD(veh_chopper_indoor_final)
			//Handle the chopper: it hovers in different positions depending on where the player is.
			//Comes around corner at 4500.0
			//First hover start: 10000.0
			//First hover end: 20000.0
			IF IS_VEHICLE_DRIVEABLE(veh_chopper_indoor_final)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_indoor_final)
					//Keep the chopper hover recording looping by reversing it once it reaches the end, then playing it forwards again once it reaches the start.
					IF s_swat_indoor_chopper_final[0].i_event > 0
						FLOAT f_playback_time
						f_playback_time = GET_TIME_POSITION_IN_RECORDING(veh_chopper_indoor_final)

						IF NOT b_player_climbed_ladder
						AND NOT IS_PLAYER_CLIMBING(PLAYER_ID())
							IF f_playback_time < 16000.0
								CONVERGE_VALUE(f_chopper_playback_speed, 1.0, 0.5, TRUE)
							ENDIF

							IF f_playback_time > 22000.0
								CONVERGE_VALUE(f_chopper_playback_speed, -1.0, 0.5, TRUE)
							ENDIF
						ELSE
							IF f_playback_time < 33000.0
								CONVERGE_VALUE(f_chopper_playback_speed, 1.0, 0.5, TRUE)
							ENDIF

							IF f_playback_time > 38000.0
								CONVERGE_VALUE(f_chopper_playback_speed, -1.0, 0.5, TRUE)
							ENDIF
						ENDIF
						
						SET_PLAYBACK_SPEED(veh_chopper_indoor_final, f_chopper_playback_speed)
						
						#IF IS_DEBUG_BUILD
							IF b_debug_display_mission_debug
								DRAW_FLOAT_TO_DEBUG_DISPLAY(f_playback_time, "Heli: ")
							ENDIF
						#ENDIF
					ENDIF
					
					//If both gunners are injured, the chopper flies off.
					IF IS_PED_INJURED(s_swat_indoor_chopper_final[1].ped)
					AND IS_PED_INJURED(s_swat_indoor_chopper_final[2].ped)
						STOP_PLAYBACK_RECORDED_VEHICLE(veh_chopper_indoor_final)
					
						IF NOT IS_PED_INJURED(s_swat_indoor_chopper_final[0].ped)
							TASK_HELI_MISSION(s_swat_indoor_chopper_final[0].ped, veh_chopper_indoor_final, 
											  NULL, NULL, <<-165.91, -1771.0, 200.0>>, MISSION_GOTO, 30.0, 5, 0.0, 200, 100)	
							SET_PED_KEEP_TASK(s_swat_indoor_chopper_final[0].ped, TRUE)
						ENDIF
						
						IF DOES_BLIP_EXIST(s_swat_indoor_chopper_final[0].blip)
							REMOVE_BLIP(s_swat_indoor_chopper_final[0].blip)
						ENDIF
						
						//Record footage of the chopper blowing up.
						REPLAY_RECORD_BACK_FOR_TIME(3.0)
					ENDIF
				ELSE
					//Remove the chopper if it gets far away.
					IF VDIST2(GET_ENTITY_COORDS(veh_chopper_indoor_final), v_player_pos) > 40000.0
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(veh_chopper_indoor_final)
						CLEAN_UP_ENEMY_PED(s_swat_indoor_chopper_final[0])
						REMOVE_VEHICLE(veh_chopper_indoor_final)
					ENDIF
				ENDIF
			ELSE
				//If the chopper is destroyed kill the pilot.
				IF NOT IS_PED_INJURED(s_swat_indoor_chopper_final[0].ped)
					SET_ENTITY_HEALTH(s_swat_indoor_chopper_final[0].ped, 0)
				ENDIF
			ENDIF
			
			//Spin the chopper out if the pilot is killed or the chopper is shot a few times.
			IF NOT b_already_set_chopper_to_explode
				IF NOT IS_ENTITY_DEAD(veh_chopper_indoor_final)
					IF i_chopper_death_counter <= 3
						IF GET_ENTITY_HEALTH(veh_chopper_indoor_final) < 800
						OR GET_VEHICLE_PETROL_TANK_HEALTH(veh_chopper_indoor_final) < 800
						OR GET_VEHICLE_ENGINE_HEALTH(veh_chopper_indoor_final) < 800
							i_chopper_death_counter = 10
							i_chopper_death_timer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				
					IF (i_chopper_death_counter > 3 AND GET_GAME_TIMER() - i_chopper_death_timer > 400)
					OR IS_PED_INJURED(s_swat_indoor_chopper_final[0].ped)
					OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_chopper_indoor_final, <<0.0, 5.0, -5.0>>),
																	 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_chopper_indoor_final, <<0.0, -5.0, 5.0>>), 8.0)
						IF NOT IS_PED_INJURED(s_swat_indoor_chopper_final[0].ped)
							SET_ENTITY_HEALTH(s_swat_indoor_chopper_final[0].ped, 0)
						ENDIF
					
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_indoor_final)
							STOP_PLAYBACK_RECORDED_VEHICLE(veh_chopper_indoor_final)
						ENDIF
						
						SET_ENTITY_HEALTH(veh_chopper_indoor_final, 1000)
						SET_VEHICLE_ENGINE_HEALTH(veh_chopper_indoor_final, 1000.0)
						SET_ENTITY_INVINCIBLE(veh_chopper_indoor_final, TRUE)
						SET_ENTITY_PROOFS(veh_chopper_indoor_final, TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_VEHICLE_OUT_OF_CONTROL(veh_chopper_indoor_final)
						
						IF i_chopper_death_counter > 3
							SET_VEHICLE_PETROL_TANK_HEALTH(veh_chopper_indoor_final, -50.0)
						ELSE
							SET_VEHICLE_PETROL_TANK_HEALTH(veh_chopper_indoor_final, 200.0)
						ENDIF
						
						//Record footage of the chopper blowing up.
						REPLAY_RECORD_BACK_FOR_TIME(3.0)
						
						b_already_set_chopper_to_explode = TRUE
						i_chopper_death_timer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELSE
				//Apply a small force.
				IF GET_GAME_TIMER() - i_chopper_death_timer < 2000
					IF NOT IS_ENTITY_DEAD(veh_chopper_indoor_final)
						IF i_chopper_death_counter > 3
							SET_VEHICLE_PETROL_TANK_HEALTH(veh_chopper_indoor_final, -50.0)
						ELSE
							SET_VEHICLE_PETROL_TANK_HEALTH(veh_chopper_indoor_final, 200.0)
						ENDIF
					
						APPLY_FORCE_TO_ENTITY(veh_chopper_indoor_final, APPLY_TYPE_FORCE, <<20.0, 0.0, 9.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, FALSE, TRUE)
						APPLY_FORCE_TO_ENTITY(veh_chopper_indoor_final, APPLY_TYPE_FORCE, <<1.5, 0.0, 0.0>>, <<0.0, -5.0, 0.0>>, 0, TRUE, TRUE, TRUE)
					ENDIF
				ELSE
					IF NOT IS_ENTITY_DEAD(veh_chopper_indoor_final)
						SET_ENTITY_INVINCIBLE(veh_chopper_indoor_final, FALSE)
						SET_ENTITY_PROOFS(veh_chopper_indoor_final, FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_VEHICLE_OUT_OF_CONTROL(veh_chopper_indoor_final)
					ENDIF
				
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(veh_chopper_indoor_final)
					CLEAN_UP_ENEMY_PED(s_swat_indoor_chopper_final[0])
					REMOVE_VEHICLE(veh_chopper_indoor_final)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(veh_chopper_indoor_final)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(veh_chopper_indoor_final)
				REMOVE_VEHICLE(veh_chopper_indoor_final)
			ENDIF
		ENDIF
	ENDIF
	
	//Gantry chopper: this doesn't have to be killed, just puts pressure on player as they run across the gantry.
	//Final chopper: overlooks the player as they leave the building. Doesn't have to be killed.
	IF NOT s_cops_chopper_gantry[0].b_is_created
		IF s_swat_indoor_breach_2[0].i_event > 0
			REQUEST_MODEL(model_chopper)
			REQUEST_MODEL(model_swat)
			REQUEST_VEHICLE_RECORDING(CARREC_GANTRY_CHOPPER, str_carrec)
			
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222082
			
			IF HAS_MODEL_LOADED(model_swat)
			AND HAS_MODEL_LOADED(model_chopper)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_GANTRY_CHOPPER, str_carrec)
				veh_chopper_gantry = CREATE_VEHICLE(model_chopper, <<-531.7855, -1650.4025, 46.0368>>, 0.0)
				SET_VEHICLE_LIVERY(veh_chopper_gantry, 0)
				SET_VEHICLE_ENGINE_ON(veh_chopper_gantry, TRUE, TRUE)
				SET_VEHICLE_SIREN(veh_chopper_gantry, TRUE)
				SET_VEHICLE_LIGHTS(veh_chopper_gantry, FORCE_VEHICLE_LIGHTS_ON)
				SET_ENTITY_COLLISION(veh_chopper_gantry, FALSE)
				SET_HELI_BLADES_FULL_SPEED(veh_chopper_gantry)
				START_PLAYBACK_RECORDED_VEHICLE(veh_chopper_gantry, CARREC_GANTRY_CHOPPER, str_carrec)
				SET_ENTITY_ALWAYS_PRERENDER(veh_chopper_gantry, TRUE)
				SET_ENTITY_COLLISION(veh_chopper_gantry, FALSE)
				SET_PLAYBACK_SPEED(veh_chopper_gantry, 0.0)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(veh_chopper_gantry, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_chopper)
				
				//Create the pilot
				s_cops_chopper_gantry[0].ped = CREATE_PED_INSIDE_VEHICLE(veh_chopper_gantry, PEDTYPE_COP, model_swat, VS_DRIVER)
				s_cops_chopper_gantry[0].i_event = 0
				s_cops_chopper_gantry[0].b_is_created = TRUE
			ENDIF
		ENDIF
	ELSE
		REPEAT COUNT_OF(s_cops_chopper_gantry) i
			IF DOES_ENTITY_EXIST(s_cops_chopper_gantry[i].ped)
				IF NOT IS_PED_INJURED(s_cops_chopper_gantry[i].ped)
					SWITCH s_cops_chopper_gantry[i].i_event
						CASE 0 
							IF IS_VEHICLE_DRIVEABLE(veh_chopper_gantry)
							AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_gantry)
								IF b_player_climbed_ladder
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-593.126221,-1643.062622,23.0>>, <<-592.603455,-1642.084351,26.957701>>, 1.500000)
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-592.958435,-1642.801514,23.456200>>,<<3.000000,3.000000,4.500000>>)
										SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh_chopper_gantry, 6500.0)
									ELSE
										SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh_chopper_gantry, 0.0)
									ENDIF
									
									FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(veh_chopper_gantry)
									SET_VEHICLE_SEARCHLIGHT(veh_chopper_gantry, TRUE)
									SET_ENTITY_COLLISION(veh_chopper_gantry, TRUE)
									TASK_VEHICLE_AIM_AT_PED(s_cops_chopper_gantry[i].ped, PLAYER_PED_ID())
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_chopper_gantry[i].ped, TRUE)
									BLIP_SIREN(veh_chopper_gantry)

									s_cops_chopper_gantry[0].blip = CREATE_BLIP_FOR_ENTITY(veh_chopper_gantry, TRUE)
									SET_BLIP_SPRITE(s_cops_chopper_gantry[0].blip, RADAR_TRACE_POLICE_HELI_SPIN)
									
									f_gantry_chopper_playback_speed = 1.0
									
									s_cops_chopper_gantry[i].i_timer = 0
									s_cops_chopper_gantry[i].i_event++
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							IF i = 0
								IF NOT b_has_text_label_triggered[LEM1_HELIC]
								AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF s_conversation_peds.PedInfo[6].Index != s_cops_chopper_gantry[i].ped
										ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, s_cops_chopper_gantry[i].ped, "Lam1Swat5")
									ELIF b_player_climbed_ladder
										IF VDIST2(v_player_pos, GET_ENTITY_COORDS(s_cops_chopper_gantry[i].ped)) < 6400.0
											IF s_cops_chopper_gantry[i].i_timer = 0
												IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_HELIC", CONV_PRIORITY_MEDIUM)
													s_cops_chopper_gantry[i].i_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 2000)
												ENDIF
											ELIF GET_GAME_TIMER() - s_cops_chopper_gantry[i].i_timer > 8000
												IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_HELIC", CONV_PRIORITY_MEDIUM)
													b_has_text_label_triggered[LEM1_HELIC] = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					CLEAN_UP_ENEMY_PED(s_cops_chopper_gantry[i])
					
					IF s_conversation_peds.PedInfo[6].Index = s_cops_chopper_gantry[i].ped
						REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 6)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
			
		IF IS_VEHICLE_DRIVEABLE(veh_chopper_gantry)
			IF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_gantry)
					//Keep the chopper hover recording looping by reversing it once it reaches the end, then playing it forwards again once it reaches the start.
					IF s_cops_chopper_gantry[0].i_event > 0
						FLOAT f_playback_time, f_max_time
						f_playback_time = GET_TIME_POSITION_IN_RECORDING(veh_chopper_gantry)
						f_max_time = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(CARREC_GANTRY_CHOPPER, str_carrec)
						
						IF f_playback_time < 15500.0
							CONVERGE_VALUE(f_gantry_chopper_playback_speed, 1.0, 0.5, TRUE)
						ELIF f_playback_time > f_max_time - 3000.0
							CONVERGE_VALUE(f_gantry_chopper_playback_speed, -1.0, 0.5, TRUE)
						ENDIF
						
						SET_PLAYBACK_SPEED(veh_chopper_gantry, f_gantry_chopper_playback_speed)
					ENDIF
					
					//If the pilot is killed, the chopper flies out of control
					IF IS_PED_INJURED(s_cops_chopper_gantry[0].ped)
					OR GET_ENTITY_HEALTH(veh_chopper_gantry) < 200
					OR GET_VEHICLE_ENGINE_HEALTH(veh_chopper_gantry) < 200
					OR GET_VEHICLE_PETROL_TANK_HEALTH(veh_chopper_gantry) < 200
						STOP_PLAYBACK_RECORDED_VEHICLE(veh_chopper_gantry)
						
						SET_VEHICLE_OUT_OF_CONTROL(veh_chopper_gantry)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(veh_chopper_gantry, FALSE)
					ENDIF
				ELSE
					//If somehow the recording stops then just kill the driver.
					IF NOT IS_PED_INJURED(s_cops_chopper_gantry[0].ped)
						APPLY_DAMAGE_TO_PED(s_cops_chopper_gantry[0].ped, 0, TRUE)
						
						SET_VEHICLE_OUT_OF_CONTROL(veh_chopper_gantry)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(veh_chopper_gantry, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(veh_chopper_gantry)
				//If the chopper is destroyed, kill the pilot
				IF NOT IS_PED_INJURED(s_cops_chopper_gantry[0].ped)
					APPLY_DAMAGE_TO_PED(s_cops_chopper_gantry[0].ped, 0, TRUE)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_gantry)
						STOP_PLAYBACK_RECORDED_VEHICLE(veh_chopper_gantry)
					ENDIF
					
					SET_VEHICLE_OUT_OF_CONTROL(veh_chopper_gantry)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(veh_chopper_gantry, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Manage the random dialogue for SWAT peds: three SWAT peds can be added for dialogue simultaneously.
	//Remove peds if they no longer exist, and randomly play dialogue on those that do exist.
	//First clean up any dead/removed speakers.
	IF s_conversation_peds.PedInfo[SWAT_SPEAKER_1].ActiveInConversation
		IF IS_PED_INJURED(s_conversation_peds.PedInfo[SWAT_SPEAKER_1].Index)
			REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_1)
		ENDIF
	ENDIF
	
	IF s_conversation_peds.PedInfo[SWAT_SPEAKER_2].ActiveInConversation
		IF IS_PED_INJURED(s_conversation_peds.PedInfo[SWAT_SPEAKER_2].Index)
			REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_2)
		ENDIF
	ENDIF
	
	IF s_conversation_peds.PedInfo[SWAT_SPEAKER_3].ActiveInConversation
		IF IS_PED_INJURED(s_conversation_peds.PedInfo[SWAT_SPEAKER_3].Index)
			REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_3)
		ENDIF
	ENDIF

	//Then play dialogue on a random alive SWAT ped.
	IF b_has_text_label_triggered[LEM1_FIGHT1] //Don't play until all of the lines after bursting through 
		IF GET_GAME_TIMER() - i_swat_dialogue_timer > 0
		OR b_force_swat_dialogue
			INT i_speaker = -1
			INT i_random = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF i_random = 0
				IF s_conversation_peds.PedInfo[SWAT_SPEAKER_1].ActiveInConversation
					i_speaker = SWAT_SPEAKER_1
				ELIF s_conversation_peds.PedInfo[SWAT_SPEAKER_2].ActiveInConversation
					i_speaker = SWAT_SPEAKER_2
				ELIF s_conversation_peds.PedInfo[SWAT_SPEAKER_3].ActiveInConversation
					i_speaker = SWAT_SPEAKER_3
				ENDIF
			ELIF i_random = 1
				IF s_conversation_peds.PedInfo[SWAT_SPEAKER_2].ActiveInConversation
					i_speaker = SWAT_SPEAKER_2
				ELIF s_conversation_peds.PedInfo[SWAT_SPEAKER_3].ActiveInConversation
					i_speaker = SWAT_SPEAKER_3
				ELIF s_conversation_peds.PedInfo[SWAT_SPEAKER_1].ActiveInConversation
					i_speaker = SWAT_SPEAKER_1
				ENDIF
			ELIF i_random = 2
				IF s_conversation_peds.PedInfo[SWAT_SPEAKER_3].ActiveInConversation
					i_speaker = SWAT_SPEAKER_3
				ELIF s_conversation_peds.PedInfo[SWAT_SPEAKER_1].ActiveInConversation
					i_speaker = SWAT_SPEAKER_1
				ELIF s_conversation_peds.PedInfo[SWAT_SPEAKER_2].ActiveInConversation
					i_speaker = SWAT_SPEAKER_2
				ENDIF
			ENDIF
			
			IF i_speaker != -1
				IF NOT IS_PED_INJURED(s_conversation_peds.PedInfo[i_speaker].Index)
					IF IS_PED_IN_COMBAT(s_conversation_peds.PedInfo[i_speaker].Index)
					OR IS_PED_SHOOTING(s_conversation_peds.PedInfo[i_speaker].Index)
					OR b_force_swat_dialogue
//						TEXT_LABEL str_label
//						IF i_speaker = SWAT_SPEAKER_1
//							str_label = "LEM1_GENSW1"
//						ELIF i_speaker = SWAT_SPEAKER_2
//							str_label = "LEM1_GENSW2"
//						ELIF i_speaker = SWAT_SPEAKER_3
//							str_label = "LEM1_GENSW3"
//						ENDIF
					
						//IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, str_label, CONV_PRIORITY_MEDIUM) //SWAT lines need replacing with gang equivalents.
							i_swat_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 13000)
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
		b_has_text_label_triggered[LEM1_FIGHT1] = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Chooses one of the random conversation blocks to play (if safe to do so), returns TRUE once the conversation has been created. 
FUNC BOOL PLAY_RANDOM_FIGHT_CONVERSATION(VECTOR v_player_pos, VECTOR v_lamar_pos, VECTOR v_stretch_pos)
	IF VDIST2(v_player_pos, v_lamar_pos) < 400.0
	AND VDIST2(v_player_pos, v_stretch_pos) < 400.0
	AND ABSF(v_player_pos.z - v_stretch_pos.z) < 5.0
		INT i = GET_RANDOM_INT_IN_RANGE(0, 10)
		TEXT_LABEL str_label = ""
		TRIGGERED_TEXT_LABEL e_label
		
		SWITCH i
			CASE 0 		str_label += "LEM1_FCONV1"			e_label = LEM1_FCONV1 		BREAK
			CASE 1 		str_label += "LEM1_FCONV2" 			e_label = LEM1_FCONV2 		BREAK
			CASE 2 		str_label += "LEM1_FCONV3" 			e_label = LEM1_FCONV3 		BREAK
			CASE 3 		str_label += "LEM1_FCONV4" 			e_label = LEM1_FCONV4 		BREAK
			CASE 4 		str_label += "LEM1_FCONV5" 			e_label = LEM1_FCONV5 		BREAK
			CASE 5 		str_label += "LEM1_FCONV6" 			e_label = LEM1_FCONV6 		BREAK
			CASE 6 		str_label += "LEM1_FCONV7" 			e_label = LEM1_FCONV7 		BREAK
			CASE 7 		str_label += "LEM1_FCONV8" 			e_label = LEM1_FCONV8 		BREAK
			CASE 8 		str_label += "LEM1_FCONV9" 			e_label = LEM1_FCONV9 		BREAK
			CASE 9 		str_label += "LEM1_FCONV10" 		e_label = LEM1_FCONV10 		BREAK
		ENDSWITCH
		
		IF NOT b_has_text_label_triggered[e_label]
			IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, str_label, CONV_PRIORITY_MEDIUM)
				b_has_text_label_triggered[e_label] = TRUE
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC STORE_WHICH_BUDDY_IS_CLOSEST_TO_LADDER()
	VECTOR v_ladder_pos = <<-592.8243, -1642.2301, 18.9575>>
	
	IF NOT IS_PED_INJURED(s_lamar.ped)
	AND NOT IS_PED_INJURED(s_lamar.ped)
		VECTOR v_lamar_pos  = GET_ENTITY_COORDS(s_lamar.ped)
		VECTOR v_stretch_pos = GET_ENTITY_COORDS(s_stretch.ped)
		BOOL b_already_set = FALSE
		
		IF b_lamar_climbed_ladder 
		OR (b_lamar_started_climbing_ladder AND v_lamar_pos.z > 22.0) //21.5
			b_lamar_is_closest_to_ladder = FALSE
			b_already_set = TRUE
		ENDIF
		
		IF b_stretch_climbed_ladder 
		OR (b_stretch_started_climbing_ladder AND v_stretch_pos.z > 22.0) //21.5
			b_lamar_is_closest_to_ladder = TRUE
			b_already_set = TRUE
		ENDIF
		
		IF NOT b_already_set
			b_lamar_is_closest_to_ladder = (VDIST2(v_ladder_pos, v_lamar_pos) < VDIST2(v_ladder_pos, v_stretch_pos))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the player from climbing into the buddy on the ladder.
PROC STOP_PLAYER_INTERSECTING_BUDDY_ON_LADDER(VECTOR v_player_pos, VECTOR v_buddy_pos, BOOL b_buddy_is_playing_custom_anim)
	IF IS_PED_CLIMBING(PLAYER_PED_ID())
		FLOAT f_height_diff = v_buddy_pos.z - v_player_pos.z
		
		IF f_height_diff > 0.0 //Player must be below the buddy.
			IF f_height_diff < 3.0
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			
				IF f_height_diff < 1.8
				OR (b_buddy_is_playing_custom_anim AND f_height_diff < 2.0)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops the player from jumping onto the ladder at the same time as the buddy.
PROC STOP_PLAYER_CLIMBING_LADDER_WITH_BUDDIES(VECTOR vPlayerPos, VECTOR vBuddyPos, VECTOR vBuddyPos2, PED_INDEX &pedBuddy, PED_INDEX &pedBuddy2)
	IF NOT IS_PED_INJURED(pedBuddy)
	AND NOT IS_PED_INJURED(pedBuddy2)
		//IF IS_PED_CLIMBING(pedBuddy)
			IF VDIST2(vPlayerPos, <<-593.0, -1642.5, 19.7>>) < 25.0
			AND (VDIST2(vBuddyPos, <<-593.0, -1642.5, 19.7>>) < 25.0 OR VDIST2(vBuddyPos2, <<-593.0, -1642.5, 19.7>>) < 25.0)
				IF (vBuddyPos.z < 22.0 AND vPlayerPos.z < vBuddyPos.z AND VDIST2(vBuddyPos, <<-593.0, -1642.5, 21.5>>) < VDIST2(vPlayerPos, <<-593.0, -1642.5, 21.5>>))
				OR (vBuddyPos2.z < 22.0 AND vPlayerPos.z < vBuddyPos2.z AND VDIST2(vBuddyPos2, <<-593.0, -1642.5, 21.5>>) < VDIST2(vPlayerPos, <<-593.0, -1642.5, 21.5>>))
					SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(), FALSE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, TRUE)
				ELSE
					SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(), TRUE)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, FALSE)
				ENDIF
			ELSE
				SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(), TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, FALSE)
			ENDIF
		//ELSE
		//	SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(), TRUE)
		//ENDIF
	ENDIF
ENDPROC

PROC MANAGE_LAMAR_IN_INTERIOR(BOOL bForceStart = FALSE)
	IF NOT IS_PED_INJURED(s_lamar.ped)
		SEQUENCE_INDEX seq
		//VECTOR v_aim_helper_pos = <<0.0, 0.0, 0.0>>
		VECTOR v_lamar_pos = GET_ENTITY_COORDS(s_lamar.ped)
		VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		BOOL b_player_stole_cover
		BOOL b_time_to_progress
		BOOL b_is_in_combat = (IS_PED_IN_COMBAT(s_lamar.ped) OR GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		INT i_next_node = 0
		INT i_prev_event = s_lamar.i_event
		FLOAT f_anim_phase
		FLOAT f_dist_to_destination
		FLOAT f_dist_from_player = VDIST2(v_lamar_pos, v_player_pos)
		SYNCED_SCENE_TRANSITION_STATE e_current_transition_state
	
		REQUEST_WAYPOINT_RECORDING(str_waypoint_interior)
		
		SET_PED_RESET_FLAG(s_lamar.ped, PRF_DisableFriendlyGunReactAudio, TRUE)
	
		SWITCH s_lamar.i_event
			CASE 0
				b_lamar_has_reached_exit = FALSE
			
				REQUEST_ANIM_DICT("misslamar1ig_2_p1")
			
				IF HAS_ANIM_DICT_LOADED("misslamar1ig_2_p1")
					IF NOT IS_SCREEN_FADED_OUT()
					OR bForceStart
						s_lamar.v_dest = <<-616.5454, -1633.3084, 32.0106>>
						SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 261.3191, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)	
						
						IF NOT b_lamars_lead_out_triggered_early
						OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-617.689, -1629.484, 32.038>>, <<0.000, 0.000, 175.000>>)
							TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_2_p1", "ig_2_p1_lamar", SLOW_BLEND_IN, NORMAL_BLEND_OUT, 
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
													RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_MELEE, SLOW_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.0)
						ENDIF
						
						SET_SYNCHRONIZED_SCENE_RATE(s_lamar.i_sync_scene, 1.0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						
						s_lamar.b_refresh_tasks = FALSE
						s_lamar.b_is_using_secondary_cover = FALSE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 //Pushes forward if gang members die first
				REQUEST_ANIM_DICT("misslamar1ig_4")
				
				IF NOT s_lamar.b_refresh_tasks				
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_lamar.ped, s_lamar.v_dest, 5.0)
						IF NOT b_time_to_progress
							IF s_lamar.b_is_using_secondary_cover
								s_lamar.v_dest = <<-616.5454, -1633.3084, 32.0106>>
								SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 261.3191, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
								
								s_lamar.b_is_using_secondary_cover = FALSE
							ELSE
								s_lamar.v_dest = <<-621.3306, -1632.2926, 32.0105>>
								SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 279.3376, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
								
								s_lamar.b_is_using_secondary_cover = TRUE
							ENDIF
							
							b_player_stole_cover = TRUE
							s_lamar.b_refresh_tasks = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
							s_lamar.i_sync_scene = -1
						ENDIF
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_lamar_pos, s_lamar.v_dest)
				

				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_lift)
				AND IS_PED_INJURED(s_swat_indoor_window_1[0].ped)
				AND HAS_ANIM_DICT_LOADED("misslamar1ig_4")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						TRANSITION_FROM_MOCAP_TO_COVER(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.v_dest, 0.96, 0.5, TRUE, TRUE, s_lamar.cover)
					ELSE
						/*IF NOT IS_PED_INJURED(s_swat_indoor_window_1[1].ped)
						AND IS_PED_IN_COVER(s_lamar.ped, TRUE)
						AND NOT s_lamar.b_is_using_secondary_cover
							s_lamar.v_dest = <<-611.5114, -1627.8892, 32.0106>>
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 261.3082, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)	

							s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-617.587, -1629.446, 32.006>>, <<0.000, 0.000, 175.000>>)
							TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_4", "lamar", NORMAL_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_MELEE, NORMAL_BLEND_IN)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						ENDIF*/

						s_lamar.b_is_using_secondary_cover = FALSE
						s_lamar.b_refresh_tasks = FALSE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					AND NOT s_lamar.b_refresh_tasks
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.i_timer, s_lamar.v_dest, 0.96, 0.5, TRUE, TRUE, s_lamar.cover)
					ELSE
						//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
						IF s_lamar.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
							OPEN_SEQUENCE_TASK(seq)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_lamar.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2 //Once the first batch are dead, go into the next room
				TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.i_timer, s_lamar.v_dest, 1.0, 0.5, TRUE, FALSE, s_lamar.cover)
			
				IF NOT IS_PED_INJURED(s_swat_indoor_window_1[1].ped)
				AND IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
				AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.45 
				AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.5
					SET_PED_SHOOTS_AT_COORD(s_lamar.ped, <<-605.0, -1631.61, 33.68>>)
				ENDIF
			
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_window_1)
				AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_lift)
					s_lamar.v_dest = <<-601.2477, -1617.8696, 32.0105>>
					
					REMOVE_COVER_POINT(s_lamar.cover)
					SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
					
					OPEN_SEQUENCE_TASK(seq)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, s_lamar.v_dest, PEDMOVE_RUN)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-599.0, -1618.0, 33.5>>, PEDMOVE_RUN, FALSE)
						TASK_AIM_GUN_AT_COORD(NULL, <<-589.2117, -1619.2445, 33.7295>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)	
					
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 3 //Lamar starts running down the corridor
				//Dialogue to fill the gap
				IF s_lamar.i_timer = 0
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_PED_INJURED(s_stretch.ped)
					IF PLAY_RANDOM_FIGHT_CONVERSATION(v_player_pos, v_lamar_pos, GET_ENTITY_COORDS(s_stretch.ped))
						s_lamar.i_timer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.068542,-1619.128906,32.033398>>, <<-606.657349,-1617.368164,34.510597>>, 5.500000)
					s_lamar.v_dest = <<-589.94, -1620.08, 32.0106>>
					SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0)
					
					OPEN_SEQUENCE_TASK(seq)
						IF NOT IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-588.068542,-1619.128906,32.033398>>, <<-606.657349,-1617.368164,34.510597>>, 5.500000)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-603.2654, -1617.9110, 32.0105>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 1.0, ENAV_NO_STOPPING)
						ENDIF
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-594.5961, -1618.2328, 32.0105>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 1.0, ENAV_NO_STOPPING)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-590.2297, -1621.9689, 33.5192>>, PEDMOVE_RUN, FALSE, 0.25, 2.0, TRUE)
						TASK_AIM_GUN_AT_COORD(NULL, <<-590.2297, -1621.9689, 33.5192>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					//SEQ_GO_TO_COORD_WHILE_AIMING_THEN_AIM(s_lamar.ped, s_lamar.v_dest, PEDMOVE_RUN, <<-590.2297, -1621.9689, 33.5192>>, NULL, FALSE, 
					//									  0.25, 2.0, TRUE, TRUE, FALSE)
					
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_lamar.ped, FALSE)
					
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_CORRIDOR")
					
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_START")
						STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_START")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_COVER_LAMAR")
						START_AUDIO_SCENE("LAMAR_1_SHOOTOUT_COVER_LAMAR")
					ENDIF
					
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
		
			CASE 4 //When the breach occurs play the anim
				//Dialogue indicating lamar is going ahead
				IF NOT b_has_text_label_triggered[LEM1_AHEAD]
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-588.068542,-1619.128906,32.033398>>, <<-606.657349,-1617.368164,34.510597>>, 5.500000)
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_AHEAD", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_AHEAD] = TRUE
					ENDIF	
				ENDIF
			
				REQUEST_ANIM_DICT("misslamar1ig_5")
			
				IF HAS_ANIM_DICT_LOADED("misslamar1ig_5")
				AND s_swat_indoor_breach[0].i_event > 0
					s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-599.100, -1619.598, 32.001>>, <<0.000, 0.000, -95.000>>)
					TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_5", "lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
											SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.675)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
					
					SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), TRUE)
					
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_COVER_LAMAR")
						STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_COVER_LAMAR")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_SAVE_LAMAR")
						START_AUDIO_SCENE("LAMAR_1_SHOOTOUT_SAVE_LAMAR")
					ENDIF
					
					//Record footage of Lamar falling.
					REPLAY_RECORD_BACK_FOR_TIME(2.0)
					
					b_lamar_on_the_ground = TRUE
					
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 5 //Lamar transitions to on-floor anim
				//Play a shout from Lamar while he falls to the ground.
				IF NOT b_has_text_label_triggered[LEM1_KNOCK]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_KNOCK", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_KNOCK] = TRUE
						ENDIF
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.9
						s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-599.100, -1619.598, 32.001>>, <<0.000, 0.000, -95.000>>)
						TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_5", "p3_lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, NORMAL_BLEND_IN)
												
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_breach[0].i_sync_scene)
							SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, GET_SYNCHRONIZED_SCENE_PHASE(s_swat_indoor_breach[0].i_sync_scene))
						ENDIF
						
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 6 //lamar gets back up
				REQUEST_ANIM_DICT("misslamar1ig_6")
			
				//Player can cause the breach enemy to go into combat against him instead. If this happens keep the reload section
				//of the synced scene going (don't play the death part).
				IF NOT IS_PED_INJURED(s_swat_indoor_breach[1].ped)
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_swat_indoor_breach[1].i_sync_scene)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.45
							SET_SYNCHRONIZED_SCENE_RATE(s_lamar.i_sync_scene, -0.8)
						ELIF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.28
							SET_SYNCHRONIZED_SCENE_RATE(s_lamar.i_sync_scene, 0.8)
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach)
				AND HAS_ANIM_DICT_LOADED("misslamar1ig_6")
					IF s_lamar.i_timer = 0
						s_lamar.i_timer = GET_GAME_TIMER()
					
						//Don't do a delay if Lamar is already about to get up.
						IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.35)
							s_lamar.i_timer -= 500
						ENDIF
					ELIF GET_GAME_TIMER() - s_lamar.i_timer > 500
						s_lamar.v_dest = <<-588.95, -1620.83, 32.0105>>
						
						s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-599.100, -1619.598, 32.001>>, <<0.000, 0.000, 175.000>>)
						TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_6", "ig_6_lamar", NORMAL_BLEND_IN, SLOW_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, NORMAL_BLEND_IN)
						SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.0)
						SET_SYNCHRONIZED_SCENE_RATE(s_lamar.i_sync_scene, 1.0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						
						SET_CAN_ATTACK_FRIENDLY(PLAYER_PED_ID(), FALSE)
						
						b_lamar_on_the_ground = FALSE
						
						i_wait_dialogue_timer = 0
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ELSE
					//Kill Lamar if they're lying on the ground and the breach enemy shoots them.
					IF (NOT IS_PED_INJURED(s_swat_indoor_breach[0].ped) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_lamar.ped, s_swat_indoor_breach[0].ped))
					OR (NOT IS_PED_INJURED(s_swat_indoor_breach[1].ped) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_lamar.ped, s_swat_indoor_breach[1].ped))
						SET_RAGDOLL_BLOCKING_FLAGS(s_lamar.ped, RBF_NONE)
						SET_PED_TO_RAGDOLL(s_lamar.ped, 1000, 2000, TASK_NM_BALANCE)
						APPLY_DAMAGE_TO_PED(s_lamar.ped, GET_ENTITY_HEALTH(s_lamar.ped) + 50, TRUE)
					ENDIF
				ENDIF
			BREAK
			
			CASE 7 //Run into chopper room
				//Play dialogue while getting up.
				IF NOT b_has_text_label_triggered[LEM1_BREACH2]
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_BREACH2", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_BREACH2] = TRUE
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_BREACH3]
					//Dialogue from Stretch about the gang members having explosives.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_BREACH3", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_BREACH3] = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				TRANSITION_FROM_MOCAP_TO_AIM(s_lamar.ped, s_lamar.i_sync_scene, <<-589.4374, -1628.9316, 33.7582>>)
				
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_23_b")
			
				IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.185303,-1619.153809,32.010502>>, <<-592.520569,-1618.994507,34.760502>>, 4.250000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-566.410706,-1627.290649,32.056114>>, <<-592.112061,-1624.948608,34.760475>>, 7.250000))
				AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_23_b")
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.6)
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						s_lamar.v_dest = <<-585.6258, -1628.1801, 32.0105>>
						REMOVE_COVER_POINT(s_lamar.cover)
						SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0)
						
						IF VDIST2(v_player_pos, <<-589.4396, -1621.6244, 32.0106>>) < 3.0
							OPEN_SEQUENCE_TASK(seq)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-583.7613, -1628.2813, 33.6858>>, PEDMOVE_RUN, FALSE, 0.5, 4.0)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						ELSE
							s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-589.133, -1620.554, 33.016>>, <<0.000, -0.000, 168.000>>)
							TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_23_b", "lam_1_ig_23_b", SLOW_BLEND_IN, SLOW_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, SLOW_BLEND_IN)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						ENDIF
						
						s_lamar.i_timer = GET_GAME_TIMER()
						s_lamar.i_event++
					ENDIF
				ELSE
					//Lamar waits at the door for Franklin, play some dialogue telling Franklin to hurry up.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF b_has_text_label_triggered[LEM1_BREACH3]
							IF NOT b_has_text_label_triggered[LEM1_GOGO1]
							AND VDIST2(v_lamar_pos, s_lamar.v_dest) < 2.0
								IF i_wait_dialogue_timer = 0
									i_wait_dialogue_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 5000
									IF VDIST2(v_lamar_pos, v_player_pos) < 400.0
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGO", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_GOGO1] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 8 //Fight the peds in the heli room
				//Play some dialogue (this used to be the heli lines, just play some random lines instead).
				IF NOT b_has_text_label_triggered[LEM1_HELIROOM]
					IF s_swat_indoor_heli[0].i_event > 0
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GENL2", CONV_PRIORITY_HIGH)
							b_has_text_label_triggered[LEM1_HELIROOM] = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				//Handle the transition to combat
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)					
					TRANSITION_FROM_MOCAP_TO_COMBAT(s_lamar.ped, s_lamar.i_sync_scene, 0.99, TRUE, SLOW_BLEND_OUT)
				ENDIF
				
				IF IS_PED_INJURED(s_swat_indoor_heli[0].ped)
				AND IS_PED_INJURED(s_swat_indoor_heli[1].ped)
				AND s_swat_indoor_heli[0].b_is_created
					s_lamar.v_dest = <<-578.9801, -1628.9391, 32.0105>>
					REMOVE_COVER_POINT(s_lamar.cover)
					SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 3.0)
					
					s_lamar.b_is_using_secondary_cover = FALSE
					s_lamar.b_refresh_tasks = TRUE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
				
				IF NOT b_glass_smashed_in_heli_room
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						IF IS_PED_SHOOTING(s_lamar.ped)
							SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<-568.386475,-1630.185669,33.548122>>, <<-568.450012,-1631.328979,33.607136>>, 0, TRUE, 
															   WEAPONTYPE_PUMPSHOTGUN, PLAYER_PED_ID(), FALSE)
												   
							b_glass_smashed_in_heli_room = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 9 //Push forward.
				//Handle the transition to combat in case it's still going
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)					
					TRANSITION_FROM_MOCAP_TO_COMBAT(s_lamar.ped, s_lamar.i_sync_scene, 0.99, TRUE, SLOW_BLEND_OUT)
				ENDIF
				
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_heli)
					s_lamar.v_dest = <<-564.5422, -1628.3492, 32.0098>>
					REMOVE_COVER_POINT(s_lamar.cover)
					SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0)
					
					s_lamar.b_is_using_secondary_cover = FALSE
					s_lamar.b_refresh_tasks = TRUE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 10 //Walk to the top of the stairs and wait for the player.
				FLOAT f_dist_to_staircase, f_lamar_heading
				f_dist_to_staircase = VDIST2(v_lamar_pos, s_lamar.v_dest)
				f_lamar_heading = GET_ENTITY_HEADING(s_lamar.ped)
			
				REQUEST_ANIM_DICT("misslamar1ig_9")
				
				//Play some more dialogue
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF s_lamar.i_timer = 0
						IF NOT IS_PED_INJURED(s_stretch.ped)
							IF PLAY_RANDOM_FIGHT_CONVERSATION(v_player_pos, v_lamar_pos, GET_ENTITY_COORDS(s_stretch.ped))
								s_lamar.i_timer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Make sure Lamar is in the right spot on the stairs.
				IF f_dist_to_staircase > 1.2
				AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) OR s_lamar.b_refresh_tasks)
					IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					OR s_lamar.b_refresh_tasks
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
							s_lamar.i_sync_scene = -1
						ENDIF
					
						OPEN_SEQUENCE_TASK(seq)
							IF f_dist_to_staircase > 25.0
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-566.4057, -1629.3054, 32.0105>>, <<-565.2, -1632.1, 33.6>>, PEDMOVE_RUN, FALSE, 0.5, 2.0, TRUE, ENAV_NO_STOPPING)
							ENDIF
						
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-565.2, -1632.1, 33.6>>, PEDMOVE_WALK, FALSE, 0.05, 1.0, TRUE, 
																   ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION) 
							TASK_AIM_GUN_AT_COORD(NULL, <<-580.0, -1682.0, 38.5>>, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)

						s_lamar.b_refresh_tasks = FALSE
					ENDIF
				ENDIF
			
				//Go on to the next scene once the player is done in the chopper room.
				IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND f_dist_to_staircase < 1.2 AND ABSF(f_lamar_heading - 160.0) < 20.0 AND GET_ENTITY_SPEED(s_lamar.ped) < 0.1)
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) >= 1.0)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-561.081909,-1629.639282,32.053299>>, <<-570.052429,-1628.872437,34.260502>>, 3.250000)
					OR v_player_pos.z < 32.0
						IF HAS_ANIM_DICT_LOADED("misslamar1ig_9")
							s_lamar.v_dest = <<-561.9443, -1623.5027, 30.0091>>
							SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0)

							s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-587.039, -1626.984, 32.008>>, <<0.000, 0.000, 175.000>>)
							TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_9", "lamar_stairs", SLOW_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
													RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, NORMAL_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.01)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
							
							i_wait_dialogue_timer = 0
							s_lamar.i_timer = 0
							s_lamar.i_event++
						ENDIF
					ELSE
						//Play some "hurry up" dialogue if the player hangs around here.
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT b_has_text_label_triggered[LEM1_GOGO2]
							IF i_wait_dialogue_timer = 0
								i_wait_dialogue_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 3000
								IF VDIST2(v_lamar_pos, v_player_pos) < 400.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGO", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_GOGO2] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If the stairs enemies already triggered just have Lamar go down the stairs.
				IF s_swat_indoor_window_2[0].i_event > 0
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						i_wait_dialogue_timer = 0
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 11
				TRANSITION_FROM_MOCAP_TO_AIM(s_lamar.ped, s_lamar.i_sync_scene, <<-580.0, -1682.0, 38.5>>)
			
				IF s_swat_indoor_window_2[0].i_event > 0
				OR IS_PED_INJURED(s_swat_indoor_window_2[0].ped)
					IF IS_PED_INJURED(s_swat_indoor_window_2[0].ped)
					OR (NOT IS_PED_INJURED(s_swat_indoor_window_2[0].ped) AND IS_ENTITY_IN_ANGLED_AREA(s_swat_indoor_window_2[0].ped, <<-563.84,-1632.11,25.76>>, <<-563.15,-1626.45,30.89>>, 5.0))
						s_lamar.v_dest = <<-563.8240, -1622.4832, 30.0094>>
						SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 2.0)
						
						BOOL bAimDownStairs
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, WALK_BLEND_OUT, TRUE)
							bAimDownStairs = TRUE
							s_lamar.i_sync_scene = -1
						ENDIF
						
						OPEN_SEQUENCE_TASK(seq)
							IF bAimDownStairs
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-564.2987, -1631.5826, 30.0554>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_STOP_EXACTLY)
							ENDIF
							
							IF NOT IS_PED_INJURED(s_swat_indoor_window_2[0].ped)
								TASK_COMBAT_PED(NULL, s_swat_indoor_window_2[0].ped)
							ENDIF
								
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ELSE
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF VDIST2(v_lamar_pos, v_player_pos) < 400.0
							IF NOT b_has_text_label_triggered[LEM1_STAIRS] //Play some dialogue while going down the stairs.
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_STAIRS", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_STAIRS] = TRUE
								ENDIF
							ELIF NOT b_has_text_label_triggered[LEM1_FIRST1] //Play some dialogue if the player hangs around.
								IF i_wait_dialogue_timer = 0
									i_wait_dialogue_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 3000
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRST", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_FIRST1] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 12 //Once stairs guys are dead Lamar waits for the player to move down then follows.
				//This is a good point to remove any anim dictionaries from earlier, as buddies are guaranteed to not be running synced scenes.
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					REMOVE_ANIM_DICT("misslamar1ig_9")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_23")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_23_b")
					REMOVE_ANIM_DICT("misslamar1ig_6")
					REMOVE_ANIM_DICT("misslamar1ig_5")
					REMOVE_ANIM_DICT("misslamar1ig_4")
					REMOVE_ANIM_DICT("misslamar1ig_2_p1")
				ENDIF
				
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_window_2)
					IF v_player_pos.z < 29.1
					OR s_swat_indoor_pre_warehouse[0].i_event > 0
						s_lamar.v_dest = <<-564.3741, -1630.4517, 28.0096>>
					
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-561.7, -1626.3, 29.3>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_STOP_EXACTLY)
							TASK_AIM_GUN_AT_COORD(NULL, <<-561.7, -1626.3, 29.3>>, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_lamar.b_refresh_tasks = FALSE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ELSE
						IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_AIM_GUN_AT_COORD) != PERFORMING_TASK
							TASK_AIM_GUN_AT_COORD(s_lamar.ped, <<-564.2987, -1631.5826, 30.0554>>, -1)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 13 //Once the player gets to the bottom of the stairs then push further
				//Play a "let's go" line here if the player is nearby (after the player plays his line about the guy smashing through the window).
				IF b_has_text_label_triggered[LEM1_WIN2]
					IF NOT b_has_text_label_triggered[LEM1_FIRSTSTAIRS]
						IF VDIST2(v_lamar_pos, v_player_pos) < 625.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRST", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_FIRSTSTAIRS] = TRUE
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			
				IF v_player_pos.z < 27.1
				OR s_swat_indoor_pre_warehouse[0].i_event > 0
					s_lamar.v_dest = <<-561.3693, -1626.7592, 27.2101>>
				
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-560.4027, -1602.1191, 27.3222>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_STOP_EXACTLY)
						TASK_AIM_GUN_AT_COORD(NULL, <<-560.4027, -1602.1191, 27.3222>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					i_wait_dialogue_timer = 0
					s_lamar.b_refresh_tasks = FALSE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 14 //Once the player triggers the enemies in the next room have Lamar progress.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_26_alt1")
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_26")
			
				IF NOT b_has_text_label_triggered[LEM1_DELAYSTAIRS] //Dialogue once Lamar is in position
					IF i_wait_dialogue_timer = 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							i_wait_dialogue_timer = GET_GAME_TIMER()
						ENDIF
					ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 5000
						IF VDIST2(v_lamar_pos, v_player_pos) < 625.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DELAY", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_DELAYSTAIRS] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_26_alt1")
				AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_26")
					IF s_swat_indoor_pre_warehouse[0].i_event > 0 
					OR s_swat_indoor_pre_warehouse[0].b_is_created AND IS_PED_INJURED(s_swat_indoor_pre_warehouse[0].ped)
						s_lamar.b_refresh_tasks = TRUE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 15 //Handle the transition from running down the stairs to playing the mocap of Lamar entering the smoke grenade room.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_26_alt1")
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_26")
				
				IF VDIST2(v_lamar_pos, s_lamar.v_dest) < 0.15
					//Once Lamar hits the mocap start point then progress.
					IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_26_alt1")
					AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_26")
						s_lamar.v_dest = <<-560.3149, -1611.9880, 26.0110>> 
						SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 353.8994, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
						
						//Alternate anim ("misslamar1lam_1_ig_26", "lam_1_ig_26] has a rotation of 0.0, 0.0, 2.0
						s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-562.360, -1621.636, 27.012>>, <<0.000, 0.000, -6.000>>)
						TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_26_alt1", "lam_1_ig_26_alt_1", SLOW_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, NORMAL_BLEND_IN)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ELSE //Make sure Lamar gets close enough to the mocap trigger point, and doesn't lose his tasks if the player gets in the way.
					SCRIPTTASKSTATUS e_task_status 
					e_task_status = GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE)
				
					IF e_task_status != PERFORMING_TASK 
					OR (e_task_status = PERFORMING_TASK AND GET_SEQUENCE_PROGRESS(s_lamar.ped) >= 3)
					OR s_lamar.b_refresh_tasks
						s_lamar.v_dest = GET_ANIM_INITIAL_OFFSET_POSITION("misslamar1lam_1_ig_26_alt1", "lam_1_ig_26_alt_1", <<-562.360, -1621.636, 27.012>>, <<0.000, 0.000, -6.000>>)

						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_interior, s_lamar.v_dest, i_next_node)
						
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_FOLLOW_WAYPOINT_RECORDING(NULL, str_waypoint_interior, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, i_next_node - 2)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-560.4027, -1602.1191, 27.3222>>, PEDMOVE_RUN, FALSE, 0.1, 4.0, TRUE, ENAV_STOP_EXACTLY)
							TASK_AIM_GUN_AT_COORD(NULL, <<-560.4027, -1602.1191, 27.3222>>, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_lamar.b_refresh_tasks = FALSE
					ENDIF
					
					//B*252192 - Slow down Lamar on the stairs (do it based on player's position, if player is far ahead speed him up)
					INT i_lamars_node, i_players_node
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_interior, v_lamar_pos, i_lamars_node)
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_interior, GET_ENTITY_COORDS(PLAYER_PED_ID()), i_players_node)
					
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(s_lamar.ped)
						WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(s_lamar.ped, <<-562.5, -1626.1, 28.4>>, FALSE)
					ENDIF
				ENDIF
			BREAK
			
			CASE 16 //Once Lamar enters the room progress to in-room behaviour.									
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					IF s_lamar.i_timer = 0
						s_lamar.i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_lamar.i_timer > 1000
					OR NOT IS_PED_IN_COVER(s_lamar.ped)
						IF NOT ARE_VECTORS_ALMOST_EQUAL(s_lamar.v_dest, <<-560.3149, -1611.9880, 26.0110>>)
							s_lamar.v_dest = <<-560.3149, -1611.9880, 26.0110>> 
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 356.9735, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
						ENDIF
					
						s_lamar.b_is_using_secondary_cover = FALSE
						s_lamar.b_refresh_tasks = TRUE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.7
						SET_PED_CAPSULE(s_lamar.ped, 1.0)
					ENDIF
				
					TRANSITION_FROM_MOCAP_TO_COVER(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.v_dest, 0.98, 0.5, TRUE, TRUE)
					
					//Break Lamar out of the anim if the player steals his destination point.
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_lamar.ped, s_lamar.v_dest, 5.0)
						STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
						
						s_lamar.v_dest = <<-562.7417, -1616.3969, 26.0110>>
						SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 356.9735, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
						
						s_lamar.b_is_using_secondary_cover = TRUE
						s_lamar.b_refresh_tasks = TRUE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 17 //Fight the enemies in the corridor.
				IF NOT s_lamar.b_refresh_tasks
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_lamar.ped, s_lamar.v_dest, 5.0, 2.0, FALSE, FALSE)
						IF s_lamar.b_is_using_secondary_cover
							s_lamar.v_dest = <<-560.3149, -1611.9880, 26.0110>> 
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 356.9735, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
							
							s_lamar.b_is_using_secondary_cover = FALSE
						ELSE
							s_lamar.v_dest = <<-562.7417, -1616.3969, 26.0110>>
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 356.9735, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
							
							s_lamar.b_is_using_secondary_cover = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
							s_lamar.i_sync_scene = -1
						ENDIF

						b_player_stole_cover = TRUE
						s_lamar.b_refresh_tasks = TRUE
					ENDIF
				ENDIF
			
				f_dist_to_destination = VDIST2(v_lamar_pos, s_lamar.v_dest)
			
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_pre_warehouse)
					IF s_lamar.i_timer = 0 //Delay movement
						s_lamar.i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_lamar.i_timer > 500
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							IF f_dist_to_destination > 9.0
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-562.0, -1602.3, 27.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
							ENDIF
							
							TASK_AIM_GUN_AT_COORD(NULL, <<-562.0, -1602.3, 27.6>>, -1)
						CLOSE_SEQUENCE_TASK(seq)
						
						TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_lamar.b_refresh_tasks = FALSE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ELSE
					//Maintain Lamar's cover behaviour in case he gets knocked out of tasks or the player is in the way.
					//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
					IF s_lamar.b_refresh_tasks
					OR (GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
					AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
						IF s_swat_indoor_pre_warehouse[0].i_event > 0
							OPEN_SEQUENCE_TASK(seq)							
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_lamar.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 18 //Once the player progresses to near the warehouse entrance move forwards.
				IF NOT b_has_text_label_triggered[LEM1_FIRSTSMOKE] //Play some dialogue if the player hangs around.
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRST", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_FIRSTSMOKE] = TRUE
						i_wait_dialogue_timer = 0
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_DELAYSMOKE] //Dialogue if the player hangs around.
					IF i_wait_dialogue_timer = 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							i_wait_dialogue_timer = GET_GAME_TIMER()
						ENDIF
					ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 5000
						IF VDIST2(v_lamar_pos, v_player_pos) < 625.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DELAY", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_DELAYSMOKE] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-567.390686,-1601.247925,26.010801>>, <<-563.415588,-1601.657959,28.260801>>, 3.750000)
				OR s_swat_indoor_roof_1[0].i_event > 0 
				OR IS_PED_INJURED(s_swat_indoor_roof_1[0].ped)
					s_lamar.v_dest = <<-562.7458, -1602.2040, 26.0108>>
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-566.3, -1601.3, 27.6159>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)						
						TASK_AIM_GUN_AT_COORD(NULL, <<-566.3, -1601.3, 27.6159>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					s_lamar.b_refresh_tasks = FALSE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 19 //Once the player enters the warehouse room then follow.
				IF s_swat_indoor_roof_1[0].i_event > 0 
				OR IS_PED_INJURED(s_swat_indoor_roof_1[0].ped)
					s_lamar.b_refresh_tasks = FALSE
					s_lamar.i_timer = 0
					s_lamar.i_event = 50 //This stage is used for skips too, so contains all the task info.
				ENDIF
			BREAK
			
			CASE 50 //In-room behaviour: takes cover at a free location, then goes back into aiming once the enemies are dead.			
				IF s_lamar.i_timer = 0 //Delay movement
					s_lamar.i_timer = GET_GAME_TIMER()
				ELIF GET_GAME_TIMER() - s_lamar.i_timer > 500
					s_lamar.v_dest = <<-575.0826, -1610.5575, 26.0440>>
					
					SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 64.3911, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
					SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_BlockPedFromTurningInCover, TRUE)
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						IF VDIST2(v_lamar_pos, s_lamar.v_dest) > 9.0
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-564.2, -1601.4, 26.01>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.5, ENAV_NO_STOPPING)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-564.2, -1601.4, 26.01>>, <<-566.3, -1601.3, 27.6159>>, 
															   	   PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
						ENDIF
						
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-590.1738, -1606.6678, 27.6159>>, PEDMOVE_RUN, FALSE, 1.0, 1.0, TRUE, ENAV_NO_STOPPING)
						//TASK_SEEK_COVER_TO_COORDS(NULL, s_lamar.v_dest, <<-590.1738, -1606.6678, 27.0159>>, -1)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_lamar.v_dest, 1000, FALSE, 0.0, TRUE, FALSE, s_lamar.cover, TRUE)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50.0)
					CLOSE_SEQUENCE_TASK(seq)
					
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					SET_PED_ACCURACY(s_lamar.ped, 1) //1147265 - Half buddy accuracy in this room.
					
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 51 //Lamar pushes forward through warehouse once enemies are clear.	
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_14")
				
				IF NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_roof_1, <<-582.356567,-1606.782959,26.010801>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
				AND NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_warehouse, <<-582.356567,-1606.782959,26.010801>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
					e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_lamar.ped, s_lamar.i_timer, s_lamar.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																				   s_lamar.cover, TRUE, TRUE, 0.75)
					IF e_current_transition_state = SST_READY_TO_START_PLAYING
						IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_14")
							s_lamar.v_dest = <<-579.7391, -1604.3739, 26.0308>>
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 77.199, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)

							s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-576.169, -1610.406, 26.032>>, <<0.000, 0.000, -120.000>>)
							TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_14", "lam_1_ig_14_lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
							
							s_lamar.b_is_using_secondary_cover = FALSE
							s_lamar.b_refresh_tasks = FALSE
							s_lamar.i_timer = 0
							s_lamar.i_event++
						ENDIF
					ELIF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
						s_lamar.v_dest = <<-579.7391, -1604.3739, 26.0308>>
						SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 77.199, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
						
						s_lamar.b_is_using_secondary_cover = FALSE
						s_lamar.b_refresh_tasks = TRUE
						s_lamar.i_timer = 0
						s_lamar.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 52 //Lamar pushes forward even more once there's only the back enemies left.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_15")
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					//Play some gunshots if he's playing the mocap
					f_anim_phase = GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene)
					
					IF f_anim_phase > 0.34 AND f_anim_phase < 0.36
						REFILL_PED_AMMO_CLIP(s_lamar.ped)
					
						IF NOT (IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1) AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse))
							IF DOES_ENTITY_EXIST(obj_warehouse_gas)
								SET_PED_SHOOTS_AT_COORD(s_lamar.ped, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_warehouse_gas, <<0.0, 0.0, 1.5>>))
							ELSE
								SET_PED_SHOOTS_AT_COORD(s_lamar.ped, <<-594.93, -1607.64, 27.8>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Progress condition.
				b_time_to_progress = NOT IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-572.628967,-1612.546509,26.010874>>, <<-577.249023,-1609.106323,28.010874>>, 3.250000, FALSE, TRUE, TM_ON_FOOT)
									 AND NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_roof_1, <<-592.978271,-1606.411621,25.671156>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
									 AND NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_warehouse, <<-592.978271,-1606.411621,25.671156>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
				
				//Check if the player is stealing Lamar's destination: if so stop any synchronized scene, pick the alternate cover and force a refresh of tasks.
				IF NOT s_lamar.b_refresh_tasks
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_lamar.ped, s_lamar.v_dest, 5.0)
						IF s_lamar.b_is_using_secondary_cover
							s_lamar.v_dest = <<-579.7391, -1604.3739, 26.0308>>
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 77.199, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
							
							s_lamar.b_is_using_secondary_cover = FALSE
						ELSE
							s_lamar.v_dest = <<-580.6209, -1601.4830, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover,  s_lamar.v_dest, 81.6555, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
							
							s_lamar.b_is_using_secondary_cover = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
							s_lamar.i_sync_scene = -1
						ENDIF

						b_player_stole_cover = TRUE
						s_lamar.b_refresh_tasks = TRUE
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_lamar_pos, s_lamar.v_dest)
				
				//Handle what Lamar should be doing based on the above information about where the player is and the shootout state.
				IF b_time_to_progress
					IF NOT ARE_VECTORS_ALMOST_EQUAL(s_lamar.v_dest, <<-579.7391, -1604.3739, 26.0308>>)
						s_lamar.v_dest = <<-579.7391, -1604.3739, 26.0308>>
						SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 77.199, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
					ENDIF
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						TRANSITION_FROM_MOCAP_TO_COVER(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.v_dest, 1.0, 0.5, TRUE, TRUE, s_lamar.cover)
					ELSE
						e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_lamar.ped, s_lamar.i_timer, s_lamar.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																				       s_lamar.cover, TRUE, TRUE, 0.75)
						IF e_current_transition_state = SST_READY_TO_START_PLAYING
						AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_15")
							s_lamar.v_dest = <<-587.1434, -1606.1481, 26.0108>> 
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 94.3660, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
							
							s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-576.169, -1610.406, 26.032>>, <<0.000, 0.000, -120.000>>)
							TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_15", "lam_1_ig_15_lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS, 
													RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
							
							s_lamar.i_secondary_timer = 0
							s_lamar.b_is_using_secondary_cover = FALSE
							s_lamar.b_refresh_tasks = FALSE
							s_lamar.i_timer = 0
							s_lamar.i_event++
						ELIF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
							s_lamar.v_dest = <<-587.1434, -1606.1481, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 94.3660, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
							
							s_lamar.i_secondary_timer = 0
							s_lamar.b_is_using_secondary_cover = FALSE
							s_lamar.b_refresh_tasks = TRUE
							s_lamar.i_timer = 0
							s_lamar.i_event++
						ENDIF
					ENDIF
				ELSE
					//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					AND NOT s_lamar.b_refresh_tasks
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.i_timer, s_lamar.v_dest, 1.0, 0.5, TRUE, TRUE, s_lamar.cover)
					ELSE
						IF s_lamar.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
							OPEN_SEQUENCE_TASK(seq)
								IF f_dist_to_destination > 4.0
								AND NOT b_player_stole_cover
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-590.1738, -1606.6678, 27.6159>>, PEDMOVE_RUN, FALSE, 1.0, 1.0, TRUE, ENAV_NO_STOPPING)
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_lamar.v_dest, 1000, FALSE, SLOW_BLEND_DURATION, TRUE, FALSE, s_lamar.cover, TRUE)
								ENDIF
								
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_lamar.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 53 //Lamar runs to warehouse exit and waits
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_17")
			
				INT i_total_enemies
				i_total_enemies = GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_roof_1) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_warehouse)
			
				//Random dialogue
				IF NOT b_has_text_label_triggered[LAM1_FCHAT1] //NOTE: this isn't the real text label, the real label is chosen at random.
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_PED_INJURED(s_stretch.ped)
					IF PLAY_RANDOM_FIGHT_CONVERSATION(v_player_pos, v_lamar_pos, GET_ENTITY_COORDS(s_stretch.ped))
						b_has_text_label_triggered[LAM1_FCHAT1] = TRUE
					ENDIF
				ENDIF
			
				//Shoot bullets if playing the run mocap.
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					f_anim_phase = GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene)
					
					IF f_anim_phase > 0.48 AND f_anim_phase < 0.75
						REFILL_PED_AMMO_CLIP(s_lamar.ped)
					
						IF i_total_enemies > 0
							SET_PED_SHOOTS_AT_COORD(s_lamar.ped, <<-594.93, -1607.64, 27.8>>)
							SET_AMMO_IN_CLIP(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN, 6)
						ENDIF
					ENDIF
				ENDIF
				
				IF i_total_enemies = 0
					b_time_to_progress = TRUE
				ELSE
					IF i_total_enemies = 1
					AND NOT IS_PED_INJURED(s_swat_indoor_warehouse[3].ped) 
					AND IS_ENTITY_IN_ANGLED_AREA(s_swat_indoor_warehouse[3].ped, <<-605.636597,-1608.953491,26.010830>>, <<-594.884644,-1609.297485,29.010830>>, 5.250000)
						IF i_lamar_leave_warehouse_timer = 0
							i_lamar_leave_warehouse_timer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - i_lamar_leave_warehouse_timer > 5000
							b_time_to_progress = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT s_lamar.b_refresh_tasks
					BOOL b_player_stole_dest
					
					IF NOT s_lamar.b_is_using_secondary_cover
						b_player_stole_dest = IS_PLAYER_STEALING_PEDS_DESTINATION(s_lamar.ped, s_lamar.v_dest, 5.0)
					ELSE
						b_player_stole_dest = IS_PLAYER_STEALING_PEDS_DESTINATION(s_lamar.ped, s_lamar.v_dest, 4.0, 1.0)
					ENDIF	
				
					IF b_player_stole_dest
						IF NOT b_time_to_progress
							IF s_lamar.b_is_using_secondary_cover
								s_lamar.v_dest = <<-587.1434, -1606.1481, 26.0108>>
								SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 94.3660, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
								
								s_lamar.b_is_using_secondary_cover = FALSE
							ELSE
								s_lamar.v_dest = <<-586.5000, -1604.0411, 26.0108>>
								REMOVE_COVER_POINT(s_lamar.cover)
								SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0, TRUE)
								
								s_lamar.b_is_using_secondary_cover = TRUE
							ENDIF
							
							b_player_stole_cover = TRUE
							s_lamar.b_refresh_tasks = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
							s_lamar.i_sync_scene = -1
						ENDIF
					ENDIF
				ENDIF
				
				BOOL b_use_aim_task
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					//If the last enemy alive is the one that has to be triggered by the player then switch the buddy to an aim task.
					IF i_total_enemies = 1
						IF NOT IS_PED_INJURED(s_swat_indoor_warehouse[4].ped)
							IF NOT IS_PED_IN_COMBAT(s_swat_indoor_warehouse[4].ped)
								b_use_aim_task = TRUE
								
								IF b_is_in_combat
									s_lamar.b_refresh_tasks = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//If the final enemy came out while the buddy was doing an aim task then make sure that they go back to combat.
					IF i_total_enemies = 1
						IF NOT b_use_aim_task
						AND NOT b_is_in_combat
						AND NOT b_time_to_progress
							s_lamar.b_refresh_tasks = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Play some extra dialogue if Lamar is waiting for the final enemy to come out.
				IF b_use_aim_task
					IF NOT b_has_text_label_triggered[LEM1_FIRSTWARE] //Play dialogue to tell Franklin to go first.
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRST", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_FIRSTWARE] = TRUE
							i_wait_dialogue_timer = 0
						ENDIF
					ELSE //Play dialogue if Franklin hangs around.
						IF NOT b_has_text_label_triggered[LEM1_GOGOWARE]
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF i_wait_dialogue_timer = 0
									i_wait_dialogue_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 7000
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGO", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_GOGOWARE] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_lamar_pos, s_lamar.v_dest)
				
				//progress either if all the enemies are dead or if the last enemy remains and has entered the room.
				IF b_time_to_progress
					IF s_lamar.v_dest.x != -587.1434
						s_lamar.v_dest = <<-587.1434, -1606.1481, 26.0108>>
								SET_PED_COVER_POINT_AND_SPHERE(s_lamar.ped, s_lamar.cover, s_lamar.v_dest, 94.3660, 1.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
					ENDIF
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						TRANSITION_FROM_MOCAP_TO_COVER(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.v_dest, 0.99, 0.5, TRUE, TRUE, s_lamar.cover)
					ELSE
						e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_lamar.ped, s_lamar.i_timer, s_lamar.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																				       s_lamar.cover, TRUE, TRUE, 0.75)
						IF e_current_transition_state = SST_READY_TO_START_PLAYING
						AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_17")
							IF s_lamar.i_secondary_timer = 0
								s_lamar.i_secondary_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - s_lamar.i_secondary_timer > 1000
							OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-595.363525,-1609.287476,27.010801>>,<<8.250000,2.750000,2.000000>>)
								s_lamar.v_dest = <<-602.9, -1607.08, 26.0109>>
								SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 2.0, TRUE)
								
								s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-576.169, -1610.406, 26.032>>, <<0.000, 0.000, -120.000>>)
								TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_17", "lam_1_ig_17_lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
														SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
														RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
								
								s_lamar.b_is_using_secondary_cover = FALSE
								s_lamar.b_refresh_tasks = FALSE
								s_lamar.i_timer = 0
								s_lamar.i_event++
							ENDIF
						ELSE
							IF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
								s_lamar.v_dest = <<-602.9, -1607.08, 26.0109>>
								SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 2.0, TRUE)
								
								s_lamar.b_is_using_secondary_cover = FALSE
								s_lamar.b_refresh_tasks = TRUE
								s_lamar.i_timer = 0
								s_lamar.i_event++
							ENDIF
						ENDIF
					ENDIF
				ELSE				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					AND NOT s_lamar.b_refresh_tasks
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_lamar.ped, s_lamar.i_sync_scene, s_lamar.i_timer, s_lamar.v_dest, 0.99, 0.5, TRUE, TRUE, s_lamar.cover)
					ELSE
						//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
						IF s_lamar.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
						AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
							OPEN_SEQUENCE_TASK(seq)
								IF f_dist_to_destination > 4.0
								AND NOT b_player_stole_cover
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-597.1738, -1606.1678, 27.6159>>, PEDMOVE_RUN, FALSE, 1.0, 1.0, TRUE, ENAV_NO_STOPPING)
								ENDIF
								
								IF b_use_aim_task
									TASK_AIM_GUN_AT_COORD(NULL, <<-597.1738, -1606.1678, 27.6159>>, -1)
								ELSE
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
								ENDIF
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_lamar.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 54 //Once the final enemies breach, Lamar runs in shooting
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF s_lamar.i_timer = 0
						//Random dialogue while moving to cover
						IF NOT IS_PED_INJURED(s_stretch.ped)
						AND PLAY_RANDOM_FIGHT_CONVERSATION(v_player_pos, v_lamar_pos, GET_ENTITY_COORDS(s_stretch.ped))
							s_lamar.i_timer = GET_GAME_TIMER()
							i_wait_dialogue_timer = 0
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_FIRST2] //Dialogue once Lamar is in position
						IF i_wait_dialogue_timer = 0
							i_wait_dialogue_timer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 3000
							IF VDIST2(v_lamar_pos, v_player_pos) < 400.0
							AND ABSF(v_lamar_pos.z - v_player_pos.z) < 5.0
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRST", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_FIRST2] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Fire bullets at the last swat guy if he's still alive.
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					TRANSITION_FROM_MOCAP_TO_AIM(s_lamar.ped, s_lamar.i_sync_scene, <<-605.60, -1624.94, 27.54>>)
				
					IF NOT IS_PED_INJURED(s_swat_indoor_warehouse[3].ped) 
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.125 AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.2
							SET_PED_SHOOTS_AT_COORD(s_lamar.ped, GET_ENTITY_COORDS(s_swat_indoor_warehouse[3].ped))
						ENDIF
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.16
							SET_ENTITY_HEALTH(s_swat_indoor_warehouse[3].ped, 0)
						ENDIF
					ENDIF
				ELSE
					//If Lamar was knocked out of the anim early have him task to the end of the room.
					IF VDIST2(v_lamar_pos, s_lamar.v_dest) > 4.0
						IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						OR (GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK AND GET_SEQUENCE_PROGRESS(s_lamar.ped) >= 1)
							OPEN_SEQUENCE_TASK(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, <<-605.60, -1624.94, 27.54>>, PEDMOVE_RUN, FALSE, 0.1, 1.0, TRUE, ENAV_STOP_EXACTLY)
								TASK_AIM_GUN_AT_COORD(NULL, <<-605.60, -1624.94, 27.54>>, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
					ENDIF
				ENDIF
				
				IF s_swat_indoor_breach_2[0].i_event > 0
					s_lamar.v_dest = <<-604.0372, -1618.1508, 26.0108>>
					
					REMOVE_COVER_POINT(s_lamar.cover)
					SET_PED_SPHERE_DEFENSIVE_AREA(s_lamar.ped, s_lamar.v_dest, 1.0)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_lamar.ped, 50.0)
					
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 55 //Once the final breach enemies are dead, Lamar runs to the exit
				//This is a good point to remove any anim dictionaries from earlier, as buddies are guaranteed to not be running synced scenes.
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_17")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_16")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_15")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_14")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_13")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_11")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_12")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_26_alt1")
					REMOVE_ANIM_DICT("misslamar1lam_1_ig_26")
				ENDIF
			
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach_2)					
					s_lamar.b_refresh_tasks = TRUE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 56 //Lamar follows the player out of the building: calculate where Lamar should be based on the player's position (he follows behind).
				VECTOR v_lamar_new_pos, v_new_pos_1, v_new_pos_2, v_new_pos_3, v_exit_pos, v_aim_pos
				v_new_pos_1 = <<-604.0372, -1618.1508, 26.0108>>
				v_new_pos_2 = <<-605.6624, -1628.9808, 26.0107>>
				v_new_pos_3 = <<-596.8221, -1629.5259, 26.0108>>
				v_exit_pos = <<-591.7455, -1630.4834, 27.2158>>
				
				v_aim_pos = <<-601.0, -1630.2, 27.2>>
				
				IF VDIST2(v_player_pos, v_exit_pos) < VDIST2(v_new_pos_3, v_exit_pos)
					v_lamar_new_pos = v_new_pos_3
					v_aim_pos = <<-591.7455, -1630.4834, 27.2158>>
				ELIF VDIST2(v_player_pos, v_new_pos_3) < VDIST2(v_new_pos_2, v_new_pos_3)
					IF NOT ARE_VECTORS_ALMOST_EQUAL(s_lamar.v_dest, v_new_pos_3)
						v_lamar_new_pos = v_new_pos_2
						v_aim_pos = <<-591.7455, -1630.4834, 27.2158>>
					ENDIF
				ELSE
					IF NOT ARE_VECTORS_ALMOST_EQUAL(s_lamar.v_dest, v_new_pos_2)
					AND NOT ARE_VECTORS_ALMOST_EQUAL(s_lamar.v_dest, v_new_pos_1)
						v_lamar_new_pos = v_new_pos_1
						v_aim_pos = <<-601.0, -1630.2, 27.2>>
					ENDIF
				ENDIF
				
				IF NOT ARE_VECTORS_ALMOST_EQUAL(s_lamar.v_dest, v_lamar_new_pos)
				AND NOT ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, 0.0>>, v_lamar_new_pos)
					s_lamar.v_dest = v_lamar_new_pos
					s_lamar.b_refresh_tasks = TRUE
				ENDIF
				
				IF s_lamar.b_refresh_tasks
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_lamar.v_dest, v_aim_pos, PEDMOVE_RUN, FALSE, 0.5, 2.0)
						TASK_AIM_GUN_AT_COORD(NULL, v_aim_pos, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)

					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
					s_lamar.b_refresh_tasks = FALSE
				ENDIF
				
				IF b_player_reached_exit
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(s_lamar.ped, <<-589.4766, -1639.8903, 18.8855>>, <<-587.2, -1631.2, 26.0>>, PEDMOVE_RUN, FALSE, 1.0, 1.0)
					
					//976891 - Reset the label for warning the player to return to buddies.
					b_has_text_label_triggered[LEM1_LEFTBOTH] = FALSE
					
					b_lamar_has_reached_exit = TRUE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ELSE
					//Play some dialogue.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND VDIST2(v_lamar_pos, v_player_pos) < 400.0
					AND ABSF(v_lamar_pos.z - v_player_pos.z) < 5.0
						IF NOT b_has_text_label_triggered[LEM1_EXIT]
							IF NOT b_has_text_label_triggered[LEM1_FIRSTEXT] //Play dialogue to tell Franklin to go first.
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRST", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_FIRSTEXT] = TRUE
									i_wait_dialogue_timer = GET_GAME_TIMER()
								ENDIF
							ELSE //Play dialogue if Franklin hangs around.
								IF NOT b_has_text_label_triggered[LEM1_GOGOEXT]
									IF GET_GAME_TIMER() - i_wait_dialogue_timer > 7000
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGO", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_GOGOEXT] = TRUE
											i_wait_dialogue_timer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELIF NOT b_has_text_label_triggered[LEM1_DELAYEXT]
									IF GET_GAME_TIMER() - i_wait_dialogue_timer > 7000
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DELAY", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_DELAYEXT] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT b_has_text_label_triggered[LEM1_WAYOUT] //Play dialogue for the fire exit.
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_WAYOUT", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_WAYOUT] = TRUE
									i_wait_dialogue_timer = GET_GAME_TIMER()
								ENDIF
							ELSE //Play dialogue if Franklin hangs around.
								IF NOT b_has_text_label_triggered[LEM1_EXT]
									IF GET_GAME_TIMER() - i_wait_dialogue_timer > 7000
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_EXT", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_EXT] = TRUE
											i_wait_dialogue_timer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELIF NOT b_has_text_label_triggered[LEM1_DELAYEXT2]
									IF GET_GAME_TIMER() - i_wait_dialogue_timer > 7000
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DELAY", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_DELAYEXT2] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			/*CASE 56 //Lamar waits until the player goes near the exit (or has already gone ahead), then proceeds.
				IF b_player_reached_exit
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.929688,-1630.875854,26.023642>>, <<-601.413757,-1630.211304,29.010828>>, 4.000000)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(s_lamar.ped, <<-589.4766, -1639.8903, 18.8855>>, <<-587.2, -1631.2, 26.0>>, PEDMOVE_RUN, FALSE, 1.0, 1.0)
					
					b_lamar_has_reached_exit = TRUE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ELSE
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT b_has_text_label_triggered[LAM1_FCHAT2] //NOTE: this isn't the real text label, the real label is chosen at random.
							IF NOT IS_PED_INJURED(s_stretch.ped)
								IF PLAY_RANDOM_FIGHT_CONVERSATION(v_player_pos, v_lamar_pos, GET_ENTITY_COORDS(s_stretch.ped))
									b_has_text_label_triggered[LAM1_FCHAT2] = TRUE
									i_wait_dialogue_timer = 0
								ENDIF
							ENDIF
						ELIF NOT b_has_text_label_triggered[LEM1_EXT] //Dialogue if the player is hanging around
							IF i_wait_dialogue_timer = 0
								i_wait_dialogue_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - i_wait_dialogue_timer > 5000
								IF VDIST2(v_lamar_pos, v_player_pos) < 400.0
								AND ABSF(v_lamar_pos.z - v_player_pos.z) < 5.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_EXT", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_EXT] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK*/
			
			CASE 57 //Once Lamar exits the building, he takes aim at the helicopter up ahead
				IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-592.567871,-1630.367554,28.238453>>, <<-587.906555,-1630.824463,23.511858>>, 2.500000)
					//If the helicopter is alive get him shooting
					IF IS_VEHICLE_DRIVEABLE(veh_chopper_indoor_final)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(s_lamar.ped, <<-589.4766, -1639.8903, 18.8855>>,<<-563.72, -1649.0, 29.8111>>, PEDMOVE_RUN, TRUE, 1.0, 1.0)
					ENDIF
					
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 58 //Once Lamar reaches the bottom of the stairs, go to the shortcut
				//IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-580.508545,-1631.194824,18.438997>>, <<-574.990356,-1632.636475,21.721424>>, 3.250000)
				IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-592.567871,-1630.367554,28.238453>>, <<-587.906555,-1630.824463,23.511858>>, 2.500000)
					STORE_WHICH_BUDDY_IS_CLOSEST_TO_LADDER()
					
					IF b_lamar_is_closest_to_ladder
						s_lamar.v_dest = <<-592.8024, -1642.5282, 18.9567>>
					ELSE
						s_lamar.v_dest = <<-593.2680, -1639.5262, 18.9594>>
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-580.1240, -1634.6755, 18.5787>>, <<-563.72, -1649.0, 29.8111>>, 
															   PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, s_lamar.v_dest, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.25, ENAV_STOP_EXACTLY, 150.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					b_lamar_started_climbing_ladder = FALSE
					//b_lamar_reached_ladder = FALSE
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
			
			CASE 59 //Have Lamar climb the ladder if the player isn't hanging around underneath
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_18")

				IF VDIST2(v_lamar_pos, <<-592.8024, -1642.5282, 18.9567>>) < 1.3 
				//IF IS_PED_SAFE_TO_PLAY_SEAMLESS_ANIM(s_lamar.ped, s_lamar.v_dest, 150.0, 1.25, 45.0)
					//b_lamar_reached_ladder = TRUE
					
					VECTOR v_stretch_pos 
					IF NOT IS_PED_INJURED(s_stretch.ped)
						v_stretch_pos = GET_ENTITY_COORDS(s_stretch.ped)
					ENDIF
					
					IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_18")
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.221619,-1642.937744,18.924829>>, <<-593.818970,-1641.983887,22.709436>>, 1.250000)
						IF (b_stretch_started_climbing_ladder AND v_stretch_pos.z > 22.0) //21.5
						OR NOT b_stretch_reached_ladder
							TASK_CLIMB_LADDER(s_lamar.ped, TRUE)
							
							e_lamar_ladder_state = LIS_CLIMB_UP
							
							b_safe_for_lamar_to_run_gantry_first = TRUE
							b_lamar_started_climbing_ladder = TRUE
							s_lamar.i_timer = 0
							s_lamar.i_event++	
						ENDIF
					ENDIF
				ELSE
					//Constantly check if Lamar is in the right position to climb the ladder.
					IF GET_GAME_TIMER() - s_lamar.i_timer > 1000
						STORE_WHICH_BUDDY_IS_CLOSEST_TO_LADDER()
						
						IF b_lamar_is_closest_to_ladder
							s_lamar.v_dest = <<-592.8024, -1642.5282, 18.9567>>
						ELSE
							s_lamar.v_dest = <<-593.2680, -1639.5262, 18.9594>>
						ENDIF
					
						f_dist_to_destination = VDIST2(v_lamar_pos, s_lamar.v_dest)
					
						IF f_dist_to_destination >= 1.3
							IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
								IF f_dist_to_destination > 16.0
									TASK_FOLLOW_NAV_MESH_TO_COORD(s_lamar.ped, s_lamar.v_dest, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.25, ENAV_STOP_EXACTLY, 150.0)
								ELSE
									TASK_GO_STRAIGHT_TO_COORD(s_lamar.ped, s_lamar.v_dest, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 150.0, 0.1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If the helicopter is dead and the player hasn't started climbing the ladder yet play some dialogue here.
				IF NOT b_has_text_label_triggered[LEM1_LADR]
				AND b_has_text_label_triggered[LEM1_BRIDGE]
					IF NOT b_player_started_climbing_ladder
					AND f_dist_to_destination < 64.0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_LADR", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_LADR] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 60 //Play a mocap of Lamar climbing the top of the ladder and getting into position.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_18")
				
				BOOL b_player_blocking_anim
				VECTOR v_anim_offset
				
				IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_18")
					v_anim_offset = GET_ANIM_INITIAL_OFFSET_POSITION("misslamar1lam_1_ig_18", "lam_1_ig_18_lamar", <<-593.060, -1642.835, 19.004>>, <<0.0, 0.0, 0.0>>, 0.454)
				ENDIF

				IF IS_PED_CLIMBING(PLAYER_PED_ID())
					IF v_player_pos.z > v_lamar_pos.z
						b_player_blocking_anim = TRUE
					ENDIF
				ENDIF
				
				/*PRINTLN(ABSF(v_lamar_pos.z - v_anim_offset.z))
				
				IF b_player_blocking_anim
					PRINTLN("block]
				ENDIF*/

				IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_CLIMB_LADDER) = PERFORMING_TASK
					STOP_PLAYER_INTERSECTING_BUDDY_ON_LADDER(v_player_pos, v_lamar_pos, FALSE)
				
					IF IS_PED_CLIMBING(s_lamar.ped)
						IF b_player_blocking_anim
							IF e_lamar_ladder_state != LIS_NOTHING
								SET_LADDER_CLIMB_INPUT_STATE(s_lamar.ped, LIS_NOTHING)
								e_lamar_ladder_state = LIS_NOTHING
							ENDIF
						ELSE
							IF e_lamar_ladder_state != LIS_CLIMB_UP
								SET_LADDER_CLIMB_INPUT_STATE(s_lamar.ped, LIS_CLIMB_UP)
								e_lamar_ladder_state = LIS_CLIMB_UP
							ENDIF
						ENDIF
					ENDIF
				
					IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_18")
						IF ABSF(v_lamar_pos.z - v_anim_offset.z) < 0.2
							IF NOT b_player_blocking_anim
								s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-593.060, -1642.835, 19.004>>, <<0.000, 0.000, 0.0000>>)
								TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_18", "lam_1_ig_18_lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
														SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
								SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.454)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
								SET_CURRENT_PED_WEAPON(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN, TRUE)
								
								s_lamar.i_timer = 0
								s_lamar.i_event++	
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF ABSF(v_lamar_pos.z - v_anim_offset.z) > 0.2
						//Lamar has probably fallen off the ladder, go back to previous section.
						b_lamar_started_climbing_ladder = FALSE
						s_lamar.i_timer = 0
						s_lamar.i_event--
					ELSE
						//Lamar is probably paused on the ladder, waiting for the player to move.
						IF NOT b_player_blocking_anim
							TASK_CLIMB_LADDER(s_lamar.ped, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				//If the helicopter is dead and the player hasn't started climbing the ladder yet play some dialogue here.
				IF NOT b_has_text_label_triggered[LEM1_LADR]
				AND b_has_text_label_triggered[LEM1_BRIDGE]
					IF NOT b_player_started_climbing_ladder
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_LADR", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_LADR] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 61 //Once Lamar finishes the synced scene have him aim.
				REQUEST_ANIM_DICT("misslamar1ig_20")
			
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.82)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					IF s_lamar.i_sync_scene != -1
						STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
						s_lamar.i_sync_scene = -1
					ENDIF
				
					//If the chopper is alive shoot at it, otherwise just aim.
					IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) >= 2
						IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_SHOOT_AT_ENTITY) != PERFORMING_TASK
						AND NOT IS_ENTITY_DEAD(veh_chopper_indoor_final)
							TASK_SHOOT_AT_ENTITY(s_lamar.ped, veh_chopper_indoor_final, -1, FIRING_TYPE_RANDOM_BURSTS)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_AIM_GUN_AT_COORD) != PERFORMING_TASK
							IF NOT IS_ENTITY_PLAYING_ANIM(s_lamar.ped, "misslamar1ig_20", "Lamar_Call_Hurry_A")
							AND NOT IS_ENTITY_PLAYING_ANIM(s_lamar.ped, "misslamar1ig_20", "Lamar_Call_Hurry_B")
							AND NOT IS_ENTITY_PLAYING_ANIM(s_lamar.ped, "misslamar1ig_20", "Lamar_Call_Hurry_C")
							AND NOT IS_ENTITY_PLAYING_ANIM(s_lamar.ped, "misslamar1ig_20", "Lamar_Call_Hurry_D")
							AND NOT IS_ENTITY_PLAYING_ANIM(s_lamar.ped, "misslamar1ig_20", "Lamar_Call_Hurry_E")
								TASK_AIM_GUN_AT_COORD(s_lamar.ped, <<-563.72, -1649.0, 29.8111>>, -1)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
							ENDIF
						ENDIF
						
						//Play a sequence of conversations here if Lamar/Stretch have climbed the ladder but the player hasn't.
						IF NOT b_player_climbed_ladder
							INT i_random
							TEXT_LABEL_23 str_current_label
							
							str_current_label  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF f_dist_from_player > 25.0
								IF f_dist_from_player < 625.0
									IF i_hurry_up_exterior_dialogue_timer = 0
										i_hurry_up_exterior_dialogue_timer = GET_GAME_TIMER() - 4000
									ELIF GET_GAME_TIMER() - i_hurry_up_exterior_dialogue_timer > 10000
										IF i_num_times_played_ladder_hurry_line < 2
											IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_KEEPUP", CONV_PRIORITY_MEDIUM)
												IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
												AND HAS_ANIM_DICT_LOADED("misslamar1ig_20")
													i_random = GET_RANDOM_INT_IN_RANGE(0, 5)
												
													OPEN_SEQUENCE_TASK(seq)
														IF i_random = 0
															TASK_PLAY_ANIM(NULL, "misslamar1ig_20", "Lamar_Call_Hurry_A", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
														ELIF i_random = 1
															TASK_PLAY_ANIM(NULL, "misslamar1ig_20", "Lamar_Call_Hurry_B", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
														ELIF i_random = 2
															TASK_PLAY_ANIM(NULL, "misslamar1ig_20", "Lamar_Call_Hurry_C", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
														ELIF i_random = 3
															TASK_PLAY_ANIM(NULL, "misslamar1ig_20", "Lamar_Call_Hurry_D", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
														ELSE
															TASK_PLAY_ANIM(NULL, "misslamar1ig_20", "Lamar_Call_Hurry_E", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
														ENDIF
														
														TASK_AIM_GUN_AT_COORD(NULL, <<-563.72, -1649.0, 29.8111>>, -1)
													CLOSE_SEQUENCE_TASK(seq)
													TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
													CLEAR_SEQUENCE_TASK(seq)
												ENDIF
											
												i_hurry_up_exterior_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 4000)
												i_num_times_played_ladder_hurry_line++
											ENDIF
										ELSE
											IF NOT b_has_text_label_triggered[LEM1_TOP]
												IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_TOP", CONV_PRIORITY_MEDIUM)
													b_has_text_label_triggered[LEM1_TOP] = TRUE
													i_hurry_up_exterior_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 4000)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//If the player is getting close then make sure the long conversation (LEM1_TOP) is killed.
								IF ARE_STRINGS_EQUAL(str_current_label, "LEM1_TOP")
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
							
							//If a hurry up line is playing then we need to trigger Lamar dialogue.
							
						ENDIF
					ENDIF
				
					REQUEST_ANIM_DICT("misslamar1lam_1_ig_19")
				
					//Once the helicopter is dead check that Stretch has started running across the rooftops.
					IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) < 2
						BOOL b_time_to_climb
					
						IF b_safe_for_lamar_to_run_gantry_first
						OR VDIST2(GET_ENTITY_COORDS(s_stretch.ped), <<-596.36, -1649.46, 25.14>>) < 4.0
							IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.82)
							OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
								IF b_player_climbed_ladder 
								OR b_player_escaped 
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-593.126221,-1643.062622,23.0>>, <<-592.603455,-1642.084351,26.957701>>, 1.500000)
									b_time_to_climb = TRUE
								ENDIF
							ENDIF
						ENDIF
					
						//Start running if Stretch is about to start or Lamar is a good distance ahead of Stretch.
						IF NOT IS_PED_INJURED(s_stretch.ped)
							IF s_stretch.i_event >= 100
							OR b_time_to_climb
								IF s_lamar.i_timer = 0
									IF s_stretch.i_event >= 100
										s_lamar.i_timer = GET_GAME_TIMER()
									ELSE
										s_lamar.i_timer = GET_GAME_TIMER() - 1000
									ENDIF
								ELIF GET_GAME_TIMER() - s_lamar.i_timer > 500
									s_lamar.i_timer = 0
									s_lamar.i_event = 100
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				IF ((IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.82))
				OR GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_CLIMB_LADDER) = PERFORMING_TASK
					STOP_PLAYER_INTERSECTING_BUDDY_ON_LADDER(v_player_pos, v_lamar_pos, TRUE)
					
					//Play some dialogue as Lamar climbs the ladder.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF b_has_text_label_triggered[LEM1_GOLAD]
						AND NOT b_has_text_label_triggered[LEM1_ROOF]
							IF NOT b_player_escaped
							AND f_dist_from_player < 625.0
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_ROOF", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_ROOF] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		
			CASE 100
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_19")
			
				IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_19")
					s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-593.078, -1642.909, 18.951>>, <<0.000, 0.000, 0.0000>>)
					TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1lam_1_ig_19", "lam_1_ig_19_lamar", WALK_BLEND_IN, WALK_BLEND_OUT,
											SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING | RBF_VEHICLE_IMPACT, 
											WALK_BLEND_IN) //1717229 - Added vehicle blocking for ragdoll as it's possible for a helicopter to knock the ped off the roofs and break the mission.
					SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
					SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_BlockPedFromTurningInCover, TRUE)
					
					s_lamar.i_timer = 0
					s_lamar.i_event++	
				ENDIF
			BREAK
			
			CASE 101
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					//Play a "let's go" line as Lamar starts running.
					IF NOT b_has_text_label_triggered[LEM1_MOVE_GANTRY]
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.2 AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.5
							IF f_dist_from_player < 900.0
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_MOVE", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_MOVE_GANTRY] = TRUE
									i_hurry_up_exterior_dialogue_timer = GET_GAME_TIMER() - 3000
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Lamar shouts at the player if they're lagging behind.
						IF NOT b_has_text_label_triggered[LEM1_KEEPUP_RUN]
						AND i_num_times_played_ladder_hurry_line < 3
							IF GET_GAME_TIMER() - i_hurry_up_exterior_dialogue_timer > 10000
								IF f_dist_from_player < 900.0
								AND f_dist_from_player > 400.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_KEEPUP", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_KEEPUP_RUN] = TRUE
										i_num_times_played_ladder_hurry_line++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					//Play some dialogue at the moment Lamar slips over in the anim.
					IF NOT b_has_text_label_triggered[LEM1_SLIP]
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.55 AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.62
						AND f_dist_from_player < 900.0
							PLAY_PED_AMBIENT_SPEECH(s_lamar.ped, "GENERIC_CURSE_MED", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)

							b_has_text_label_triggered[LEM1_SLIP] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					//If the player already escaped then skip the scene forwards.
					IF b_player_escaped AND b_stretch_escaped
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.7
							IF NOT IS_SPHERE_VISIBLE(<<-603.1, -1696.5, 24.9>>, 1.0)
							AND NOT IS_SPHERE_VISIBLE(v_lamar_pos, 1.0)
								SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.7)
							ENDIF
						ENDIF
					ENDIF
				
					//Block player climbing if the buddy is currently climbing.
					IF GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.713 AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) < 0.9
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-603.100952,-1698.411377,24.043583>>, <<-601.630859,-1696.323730,26.977901>>, 1.000000)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
						ENDIF
					ENDIF
				ENDIF
			
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.99)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-603.5973, -1701.3000, 22.9511>>, <<-596.5918, -1714.4611, 23.8392>>, PEDMOVE_WALK, FALSE, 0.5, 0.5)
						TASK_AIM_GUN_AT_COORD(NULL, <<-596.5918, -1714.4611, 23.8392>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					
					STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
					TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					s_lamar.i_sync_scene = -1
					s_lamar.i_timer = 0
					s_lamar.i_event++
				ENDIF
			BREAK
		ENDSWITCH
		
		//Kill instantly if player throws an explosive at them
		HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(s_lamar.ped, s_lamar.i_health)
		
		IF i_prev_event != s_lamar.i_event
			#IF IS_DEBUG_BUILD
				PRINTLN("Lamar1.sc - Lamar's event has changed to ", s_lamar.i_event)
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF b_debug_display_mission_debug
				DRAW_INT_TO_DEBUG_DISPLAY(s_lamar.i_event, "Lamar: ")
			ENDIF
		#ENDIF
	ENDIF
ENDPROC	

PROC MANAGE_STRETCH_IN_INTERIOR(BOOL bForceStart = FALSE)
	IF NOT IS_PED_INJURED(s_stretch.ped)
		BOOL b_player_stole_cover
		BOOL b_time_to_progress
		BOOL b_is_in_combat = (IS_PED_IN_COMBAT(s_stretch.ped) OR GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK)
		//INT i_next_node = 0
		FLOAT f_dist_to_destination
		VECTOR v_stretch_pos = GET_ENTITY_COORDS(s_stretch.ped)
		VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		SEQUENCE_INDEX seq
		SYNCED_SCENE_TRANSITION_STATE e_current_transition_state
	
		REQUEST_WAYPOINT_RECORDING(str_waypoint_interior)
	
		SET_PED_RESET_FLAG(s_stretch.ped, PRF_DisableFriendlyGunReactAudio, TRUE)
	
		SWITCH s_stretch.i_event
			CASE 0
				REQUEST_ANIM_DICT("misslamar1ig_2_p1")
			
				//Stretch is frozen at the start, unfreeze him if something goes wrong.
				IF IS_PED_RAGDOLL(s_stretch.ped)
					FREEZE_ENTITY_POSITION(s_stretch.ped, FALSE)
				ENDIF
			
				IF HAS_ANIM_DICT_LOADED("misslamar1ig_2_p1")
					IF NOT IS_SCREEN_FADED_OUT() //Buddies start straight away now.
					OR bForceStart
						FREEZE_ENTITY_POSITION(s_stretch.ped, FALSE)
					
						s_stretch.v_dest = <<-617.8926, -1628.9664, 32.0106>>
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 253.2876, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
						
						#IF IS_DEBUG_BUILD
							PRINTVECTOR(GET_ANIM_INITIAL_OFFSET_POSITION("misslamar1ig_2_p1", "ig_2_p1_stretch", <<-617.689, -1629.484, 32.038>>, <<0.000, 0.000, 175.000>>))
							PRINTVECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION("misslamar1ig_2_p1", "ig_2_p1_stretch", <<-617.689, -1629.484, 32.038>>, <<0.000, 0.000, 175.000>>))
							PRINTNL()
						#ENDIF
						
						s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-617.689, -1629.484, 32.038>>, <<0.000, 0.000, 175.000>>)
						TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1ig_2_p1", "ig_2_p1_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_MELEE, WALK_BLEND_IN)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
						
						SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_BLIND_FIRE_IN_COVER, TRUE)
						SET_COMBAT_FLOAT(s_stretch.ped, CCF_BLIND_FIRE_CHANCE, 1.0)
						SET_COMBAT_FLOAT(s_stretch.ped, CCF_MIN_DISTANCE_TO_TARGET, 0.5)
						
						b_stretch_locker_tasks_updated = FALSE
						s_stretch.b_is_using_secondary_cover = FALSE
						s_stretch.b_refresh_tasks = FALSE
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 //Stretch takes cover behind a locker and waits (NOTE: Stretch will be staggered relative to Lamar)			
				//Check if the player is stealing Stretch's destination: if so stop any synchronized scene, pick the alternate cover and force a refresh of tasks.
				IF NOT s_stretch.b_refresh_tasks
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 4.0, 2.0, FALSE, FALSE)
						IF s_stretch.b_is_using_secondary_cover
							s_stretch.v_dest = <<-617.8926, -1628.9664, 32.0106>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 253.2876, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
							
							s_stretch.b_is_using_secondary_cover = FALSE
						ELSE
							s_stretch.v_dest = <<-620.7198, -1627.3146, 32.0303>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 269.1433, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
							
							s_stretch.b_is_using_secondary_cover = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
							s_stretch.b_refresh_tasks = TRUE //Don't need to refresh if Stretch was already in combat.
						ENDIF

						b_player_stole_cover = TRUE
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
				
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_window_1)
				AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_lift)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						TRANSITION_FROM_MOCAP_TO_COVER(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.v_dest, 1.0, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						s_stretch.v_dest = <<-601.8096, -1615.8409, 32.0102>>
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 267.2701, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
						
						SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_BLIND_FIRE_IN_COVER, FALSE)
						SET_COMBAT_FLOAT(s_stretch.ped, CCF_BLIND_FIRE_CHANCE, 0.05)
						SET_COMBAT_FLOAT(s_stretch.ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
						
						s_stretch.b_is_using_secondary_cover = FALSE
						s_stretch.b_refresh_tasks = TRUE
						s_stretch.i_timer = GET_GAME_TIMER()
						s_stretch.i_event++
					ENDIF
				ELSE
					//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					AND NOT s_stretch.b_refresh_tasks
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.i_timer, s_stretch.v_dest, 1.0, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						IF s_stretch.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
							OPEN_SEQUENCE_TASK(seq)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_stretch.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
					
					IF NOT b_glass_smashed_in_first_room
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							IF IS_PED_SHOOTING(s_stretch.ped)
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<-606.347473,-1634.252197,33.663982>>, <<-606.561218,-1638.024536,33.714024>>, 0, TRUE, 
																   WEAPONTYPE_PUMPSHOTGUN, PLAYER_PED_ID(), FALSE)
													   
								b_glass_smashed_in_first_room = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2 //Stretch starts fighting once he and the player reach the room
				REQUEST_ANIM_DICT("misslamar1ig_6")

				//Stretch needs to adjust cover points depending on where the player is
				IF NOT s_stretch.b_refresh_tasks
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-601.8096, -1615.8409, 32.0102>>) < 2.0
						IF ARE_VECTORS_ALMOST_EQUAL(s_stretch.v_dest, <<-601.8954, -1615.9567, 32.0105>>)
							IF VDIST2(v_stretch_pos, <<-601.8954, -1615.9567, 32.0105>>) > 2.0 //Stretch hasn't reached the cover point yet, so the player could be blocking it.
								s_stretch.v_dest = <<-603.3610, -1619.3937, 32.0105>>
								REMOVE_COVER_POINT(cover_locker)
								SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 267.2701, 2.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
								s_stretch.b_refresh_tasks = TRUE
							ENDIF
						ENDIF
					ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-603.3610, -1619.3937, 32.0105>>) < 2.0
						IF ARE_VECTORS_ALMOST_EQUAL(s_stretch.v_dest, <<-603.3610, -1619.3937, 32.0105>>)
							IF VDIST2(v_stretch_pos, <<-603.3610, -1619.3937, 32.0105>>) > 2.0 //Stretch hasn't reached the cover point yet, so the player could be blocking it.
								s_stretch.v_dest = <<-601.8954, -1615.9567, 32.0105>>
								SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 267.2701, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
								s_stretch.b_refresh_tasks = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach)
					IF s_stretch.v_dest.x != -601.8096
						s_stretch.v_dest = <<-601.8096, -1615.8409, 32.0102>>
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 267.2701, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
					ENDIF
					
					e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_stretch.ped, s_stretch.i_timer, s_stretch.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																			       s_stretch.cover, TRUE, TRUE, 0.75)
																			   
					IF e_current_transition_state = SST_READY_TO_START_PLAYING
						IF HAS_ANIM_DICT_LOADED("misslamar1ig_6")
							s_stretch.v_dest = <<-589.083, -1619.854, 32.0105>>
							REMOVE_COVER_POINT(s_stretch.cover)
						
							s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-599.100, -1619.598, 32.001>>, <<0.000, 0.000, 175.000>>)
							TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1ig_6", "ig_6_stretch", NORMAL_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, 
													RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_MELEE, NORMAL_BLEND_IN)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
							
							s_stretch.b_is_using_secondary_cover = FALSE
							s_stretch.b_refresh_tasks = FALSE
							s_stretch.i_timer = 0
							s_stretch.i_event++
						ENDIF
					ELIF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
						s_stretch.v_dest = <<-589.083, -1619.854, 32.0105>>
						REMOVE_COVER_POINT(s_stretch.cover)
						
						s_stretch.b_is_using_secondary_cover = FALSE
						s_stretch.b_refresh_tasks = TRUE
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ELSE
					IF NOT b_stretch_locker_tasks_updated
						IF s_swat_indoor_breach[0].i_event > 0
							s_stretch.b_refresh_tasks = TRUE
							b_stretch_locker_tasks_updated = TRUE
						ENDIF
					ENDIF
					
					IF s_stretch.b_refresh_tasks = TRUE
					AND GET_GAME_TIMER() - s_stretch.i_timer > 500
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							
							IF VDIST2(v_stretch_pos, s_stretch.v_dest) > 4.0
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-599.0, -1618.0, 33.5>>, PEDMOVE_RUN, FALSE, 1.0, 4.0, TRUE, ENAV_NO_STOPPING)
							ENDIF
							
							IF s_swat_indoor_breach[0].i_event > 0
								IF NOT IS_PED_IN_COVER(s_stretch.ped)
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_stretch.v_dest, 1000, FALSE, 0.5, FALSE, FALSE, s_stretch.cover, TRUE)
								ENDIF
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							ELSE
								TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_stretch.v_dest, -1, FALSE, 0.5, FALSE, FALSE, s_stretch.cover, TRUE)
							ENDIF
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_stretch.b_refresh_tasks = FALSE
					ENDIF
				ENDIF
			BREAK
		
			CASE 3 //Once player enters heli room, follow him.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_23")
				
				f_dist_to_destination = VDIST2(s_stretch.v_dest, v_stretch_pos)
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					TRANSITION_FROM_MOCAP_TO_AIM(s_stretch.ped, s_stretch.i_sync_scene, <<-589.4374, -1628.9316, 33.7582>>, 0.99, FALSE)

					//Break stretch out of the anim if Franklin gets in the way.
					IF VDIST(v_player_pos, v_stretch_pos) < 2.0
						IF f_dist_to_destination > VDIST2(v_player_pos, s_stretch.v_dest)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, WALK_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF
					ENDIF
				ELSE
					IF f_dist_to_destination > 2.0
					AND GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-589.4374, -1628.9316, 33.7582>>, PEDMOVE_RUN, FALSE, 0.1, 4.0, TRUE, ENAV_DONT_ADJUST_TARGET_POSITION)
							TASK_AIM_GUN_AT_COORD(NULL, <<-589.4374, -1628.9316, 33.7582>>, -1, FALSE)
						CLOSE_SEQUENCE_TASK(seq)
						CLEAR_PED_TASKS(s_stretch.ped)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
			
				IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.185303,-1619.153809,32.010502>>, <<-592.520569,-1618.994507,34.760502>>, 4.250000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-566.410706,-1627.290649,32.056114>>, <<-592.112061,-1624.948608,34.760475>>, 7.250000))
				AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_23")
					IF (f_dist_to_destination < 2.0 AND GET_ENTITY_SPEED(s_stretch.ped) < 0.3)
					OR (NOT IS_ENTITY_ON_SCREEN(s_stretch.ped) AND NOT IS_SPHERE_VISIBLE(s_stretch.v_dest, 1.0))
						IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.95)
						OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							IF s_stretch.i_timer = 0
								s_stretch.i_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - s_stretch.i_timer > 500
								s_stretch.v_dest = <<-584.9334, -1622.8121, 32.0105>>
								SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 272.4262, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
								
								s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-589.083, -1619.854, 33.016>>, <<0.000, -0.000, 168.000>>)
								TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_23", "lam_1_ig_23", WALK_BLEND_IN, WALK_BLEND_OUT, 
														SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON, 
														RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_MELEE, WALK_BLEND_IN)
								SET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene, 0.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
								
								s_stretch.b_is_using_secondary_cover = FALSE
								s_stretch.b_refresh_tasks = FALSE
								s_stretch.i_timer = 0
								s_stretch.i_event++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4 //Fight the peds in the heli room
				//Check if the player is stealing Stretch's destination: if so stop any synchronized scene, pick the alternate cover and force a refresh of tasks.
				IF NOT s_stretch.b_refresh_tasks
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 5.0, 2.0, FALSE, FALSE)
						IF s_stretch.b_is_using_secondary_cover
							s_stretch.v_dest = <<-584.9334, -1622.8121, 32.0105>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 272.4262, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
							
							s_stretch.b_is_using_secondary_cover = FALSE
						ELSE
							s_stretch.v_dest = <<-587.5033, -1625.9580, 32.0530>>
							REMOVE_COVER_POINT(s_stretch.cover)
							SET_PED_SPHERE_DEFENSIVE_AREA(s_stretch.ped, s_stretch.v_dest, 2.0)
							//SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 262.8596, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
							
							s_stretch.b_is_using_secondary_cover = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, WALK_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF

						b_player_stole_cover = TRUE
						s_stretch.b_refresh_tasks = TRUE
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
				
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_heli)
					IF s_stretch.i_timer = 0
						s_stretch.i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_stretch.i_timer > 500
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, WALK_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF
					
						s_stretch.v_dest = <<-562.4127, -1631.9725, 32.0105>>
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 180.1320, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
					
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-576.3644, -1624.4769, 32.0106>>, <<-562.0, -1627.9, 33.6>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_NO_STOPPING)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-562.0, -1627.9, 33.6>>, PEDMOVE_RUN, FALSE, 0.5, 4.0)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-561.9711, -1632.0319, 32.0105>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 1.0, ENAV_NO_STOPPING)
							TASK_SEEK_COVER_TO_COVER_POINT(NULL, s_stretch.cover, <<-564.5787, -1652.9934, 34.1345>>, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_stretch.b_is_using_secondary_cover = FALSE
						s_stretch.b_refresh_tasks = FALSE
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ELSE
					//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
					IF s_stretch.b_refresh_tasks
					OR (GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
					AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))						
						OPEN_SEQUENCE_TASK(seq)
							IF f_dist_to_destination > 4.0
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-583.0, -1626.8, 33.6>>, PEDMOVE_RUN, FALSE)
							ENDIF
						
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
					
						s_stretch.b_refresh_tasks = FALSE
					ENDIF
				
					//Handle the transition to combat
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)	
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.i_timer, s_stretch.v_dest, 0.99, 0.0, TRUE, FALSE, s_stretch.cover, WALK_BLEND_OUT)
					ENDIF
				ENDIF
			BREAK
			
			CASE 5 //When ped bursts through window, run down to attack him.
				f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
			
				IF s_swat_indoor_window_2[0].i_event > 0
				OR IS_PED_INJURED(s_swat_indoor_window_2[0].ped)
					IF f_dist_to_destination < 100.0
						s_stretch.v_dest = <<-561.5094, -1623.1287, 30.0093>>
						
						SET_PED_SPHERE_DEFENSIVE_AREA(s_stretch.ped, s_stretch.v_dest, 2.0)
						
						IF NOT IS_PED_INJURED(s_swat_indoor_window_2[0].ped)
							TASK_COMBAT_PED(s_stretch.ped, s_swat_indoor_window_2[0].ped)
						ENDIF
						
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK
						
			CASE 6 //Once stairs guy is dead and player has headed down the stairs then continue.
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_window_2)
					IF v_player_pos.z < 29.1
					OR s_swat_indoor_pre_warehouse[0].i_event > 0
						IF s_stretch.i_timer = 0
							s_stretch.i_timer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - s_stretch.i_timer > 650
							s_stretch.v_dest = <<-564.3552, -1626.5706, 29.0099>>
						
							OPEN_SEQUENCE_TASK(seq)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-561.7, -1626.3, 29.3>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_STOP_EXACTLY)
								TASK_AIM_GUN_AT_COORD(NULL, <<-561.7, -1626.3, 29.3>>, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
							
							s_stretch.b_refresh_tasks = FALSE
							s_stretch.i_timer = 0
							s_stretch.i_event++
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_AIM_GUN_AT_COORD) != PERFORMING_TASK
							TASK_AIM_GUN_AT_COORD(s_stretch.ped, <<-564.2987, -1631.5826, 30.0554>>, -1)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 7 //Once the player reaches the bottom of the stairs continue.
				IF v_player_pos.z < 27.1
				OR s_swat_indoor_pre_warehouse[0].i_event > 0
					IF s_stretch.i_timer = 0
						s_stretch.i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_stretch.i_timer > 650
						s_stretch.v_dest = <<-562.5714, -1628.3652, 28.0096>>
					
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-560.4027, -1602.1191, 27.3222>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_STOP_EXACTLY)
							TASK_AIM_GUN_AT_COORD(NULL, <<-560.4027, -1602.1191, 27.3222>>, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_stretch.b_refresh_tasks = FALSE
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 8 //Once the enemies are active in the next room then continue
				IF s_swat_indoor_pre_warehouse[0].i_event > 0 
				OR s_swat_indoor_pre_warehouse[0].b_is_created AND IS_PED_INJURED(s_swat_indoor_pre_warehouse[0].ped)
					s_stretch.b_refresh_tasks = FALSE
					s_stretch.i_timer = 0
					s_stretch.i_event++
				ENDIF
			BREAK
			
			CASE 9 //Once the enemies are dead go to position and aim.
				//Get Stretch aiming on the stairs and slow him down if he gets too far ahead.				
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(s_stretch.ped)
					INT i_stretch_node
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_interior, v_stretch_pos, i_stretch_node)
				
					IF i_stretch_node > 55
						WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(s_stretch.ped, <<-562.5, -1626.1, 28.4>>, FALSE)
					ELSE
						WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(s_stretch.ped, <<-565.7, -1627.7, 29.6>>, FALSE)
					ENDIF
				ENDIF
			
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_pre_warehouse)
					s_stretch.v_dest = <<-560.9681, -1617.0406, 26.0110>>
					f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						IF f_dist_to_destination > 9.0
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-562.0, -1602.3, 27.6>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
						ENDIF
						
						TASK_AIM_GUN_AT_COORD(NULL, <<-562.0, -1602.3, 27.6>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					
					TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					s_stretch.b_refresh_tasks = FALSE
					s_stretch.i_timer = 0
					s_stretch.i_event++
				ELSE
					//Send Stretch into combat once the enemies are active.
					IF s_swat_indoor_pre_warehouse[0].i_event > 0
						IF NOT IS_PED_IN_COMBAT(s_stretch.ped)
						AND GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
							IF s_stretch.i_timer = 0
								s_stretch.i_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - s_stretch.i_timer > 2000
								SET_PED_SPHERE_DEFENSIVE_AREA(s_stretch.ped, <<-560.9681, -1617.0406, 26.0110>>, 1.5, TRUE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_stretch.ped, 100.0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
								
								s_stretch.i_timer = 0
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 10 //Once the player progresses to near the warehouse entrance move forwards.
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-567.456726, -1601.174316, 26.010801>>, <<-558.682495, -1601.874268, 29.510801>>, 7.0)	//<<-567.390686,-1601.247925,26.010801>>, <<-563.415588,-1601.657959,28.260801>>, 3.750000)
				OR s_swat_indoor_roof_1[0].i_event > 0 
				OR IS_PED_INJURED(s_swat_indoor_roof_1[0].ped)
					s_stretch.v_dest = <<-560.7259, -1600.8618, 26.0603>>
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-566.3, -1601.3, 27.6159>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)						
						TASK_AIM_GUN_AT_COORD(NULL, <<-566.3, -1601.3, 27.6159>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					
					TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					s_stretch.b_refresh_tasks = FALSE
					s_stretch.i_timer = 0
					s_stretch.i_event++
				ENDIF
			BREAK
			
			CASE 11 //Once the player enters the warehouse room then follow.
				IF NOT b_has_text_label_triggered[LEM1_GOGOSMOKE] //Dialogue if the player hangs around.
					IF s_stretch.i_timer = 0
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							s_stretch.i_timer = GET_GAME_TIMER()
						ENDIF
					ELIF GET_GAME_TIMER() - s_stretch.i_timer > 5000
						IF VDIST2(v_stretch_pos, v_player_pos) < 625.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGO2", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_GOGOSMOKE] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iWarehouseDoorLeft)
					ADD_DOOR_TO_SYSTEM(iWarehouseDoorLeft, V_ILEV_RC_DOOR1, <<-567.6380, -1602.4773, 27.1608>>)
				ENDIF
				
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iWarehouseDoorLeft)
					ADD_DOOR_TO_SYSTEM(iWarehouseDoorRight, V_ILEV_RC_DOOR1, <<-567.4113, -1599.8862, 27.1608>>)
				ENDIF
				
				IF s_swat_indoor_roof_1[0].i_event > 0 
				OR IS_PED_INJURED(s_swat_indoor_roof_1[0].ped)
				OR (IS_DOOR_REGISTERED_WITH_SYSTEM(iWarehouseDoorLeft) AND DOOR_SYSTEM_GET_OPEN_RATIO(iWarehouseDoorLeft) <> 0.0)
				OR (IS_DOOR_REGISTERED_WITH_SYSTEM(iWarehouseDoorRight) AND DOOR_SYSTEM_GET_OPEN_RATIO(iWarehouseDoorRight) <> 0.0)
					INT iPlacementFlags
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
					
					piWeaponPickup = CREATE_PICKUP(PICKUP_WEAPON_SMG, <<-601.9845, -1602.4429, 29.60>>, iPlacementFlags)
					ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(piWeaponPickup, "v_recycle")
					
					s_stretch.b_refresh_tasks = FALSE
					s_stretch.i_timer = 0
					s_stretch.i_event = 50 //This stage is used for skips too, so contains all the task info.
				ENDIF
			BREAK
			
			CASE 50 //Runs into warehouse room
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_pre_warehouse)
					IF s_stretch.i_timer = 0
						IF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
							s_stretch.i_timer = GET_GAME_TIMER() - 10000
						ELSE
							s_stretch.i_timer = GET_GAME_TIMER()
						ENDIF
					ELIF GET_GAME_TIMER() - s_stretch.i_timer > 500
						s_stretch.v_dest = <<-572.0428, -1600.6907, 26.0108>> //<<-571.9313, -1600.1042, 26.0110>>
						
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 84.6418, 1.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_120)
						SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_BlockPedFromTurningInCover, TRUE)
						
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							IF VDIST2(GET_ENTITY_COORDS(s_stretch.ped), s_stretch.v_dest) > 9.0
								//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-568.49, -1601.26, 26.01>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.25, ENAV_NO_STOPPING)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-564.2, -1601.4, 26.01>>, <<-566.3, -1601.3, 27.6159>>, 
																	   	   PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
							ENDIF
							
							IF VDIST2(GET_ENTITY_COORDS(s_stretch.ped), s_stretch.v_dest) > 2.0
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-590.1738, -1606.6678, 27.6159>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, TRUE, ENAV_NO_STOPPING)
							ENDIF
							
							//TASK_SEEK_COVER_TO_COORDS(NULL, s_stretch.v_dest, <<-604.2, -1603.5314, 27.5187>>, 1000)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_stretch.v_dest, 1000, FALSE, 0.5, TRUE, TRUE, s_stretch.cover, TRUE)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						SET_PED_ACCURACY(s_stretch.ped, 1) //1147265 - Half buddy accuracy in this room.
						SET_COMBAT_FLOAT(s_stretch.ped, CCF_MIN_DISTANCE_TO_TARGET, 3.0)
						
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 51 //Stretch pushes forward through warehouse					
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_14")
				
				IF NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_roof_1, <<-582.356567,-1606.782959,26.010801>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
				AND NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_warehouse, <<-582.356567,-1606.782959,26.010801>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
					e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_stretch.ped, s_stretch.i_timer, s_stretch.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																				   s_stretch.cover, TRUE, TRUE, 0.75)
					IF e_current_transition_state = SST_READY_TO_START_PLAYING
						IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_14")
							s_stretch.v_dest = <<-580.3932, -1608.8782, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.2920, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)

							s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-576.169, -1610.406, 26.032>>, <<0.000, 0.000, -120.000>>)
							TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_14", "lam_1_ig_14_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
													SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
													RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
							
							s_stretch.i_timer = 0
							s_stretch.i_event++
						ENDIF
					ELIF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
						s_stretch.v_dest = <<-580.3932, -1608.8782, 26.0108>>
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.2920, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
						
						s_stretch.b_refresh_tasks = TRUE
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 52 //Continue to push
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_15")
				
				//Play some gunshots while Stretch runs to his cover.
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					FLOAT f_anim_phase
					f_anim_phase = GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene)
					
					IF f_anim_phase > 0.64 AND f_anim_phase < 0.8
						REFILL_PED_AMMO_CLIP(s_stretch.ped)
					
						IF NOT (IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1) AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse))
							IF DOES_ENTITY_EXIST(obj_warehouse_gas)
								SET_PED_SHOOTS_AT_COORD(s_stretch.ped, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_warehouse_gas, <<0.0, 0.0, 2.0>>))
								
								IF f_anim_phase > 0.7
									SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_warehouse_gas, <<0.5, 0.0, 0.0>>), GET_ENTITY_COORDS(obj_warehouse_gas), 
													   1, TRUE, WEAPONTYPE_PUMPSHOTGUN, PLAYER_PED_ID(), FALSE)
								ENDIF
							ELSE
								SET_PED_SHOOTS_AT_COORD(s_stretch.ped, <<-594.93, -1607.64, 27.8>>)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(obj_warehouse_gas)
						IF NOT IS_PED_IN_COMBAT(s_stretch.ped)
							f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
						
							IF f_dist_to_destination > 4.0
							AND f_dist_to_destination < 100.0
								SET_PED_SHOOTS_AT_COORD(s_stretch.ped, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_warehouse_gas, <<0.0, 0.0, 2.0>>))
								
								//If the gas canister hasn't been blown up yet then shoot more bullets.
								IF f_dist_to_destination < 49.0
								AND s_warehouse_fire[0].f_scale = 0.0
									SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_warehouse_gas, <<0.5, 0.0, 0.0>>), GET_ENTITY_COORDS(obj_warehouse_gas), 
													   1, TRUE, WEAPONTYPE_PUMPSHOTGUN, PLAYER_PED_ID(), FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Progress condition.
				b_time_to_progress = NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_roof_1, <<-592.362671,-1606.239014,25.860518>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
									 AND NOT ARE_ANY_ALIVE_ENEMY_PEDS_IN_ANGLED_AREA(s_swat_indoor_warehouse, <<-592.362671,-1606.239014,25.860518>>, <<-569.629028,-1607.288940,30.270933>>, 15.500000)
			
				//Check if the player is stealing Stretch's destination: if so stop any synchronized scene, pick the alternate cover and force a refresh of tasks.
				IF NOT s_stretch.b_refresh_tasks
					IF IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 5.0)
						IF s_stretch.b_is_using_secondary_cover
							s_stretch.v_dest = <<-580.3932, -1608.8782, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.2920, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
							
							s_stretch.b_is_using_secondary_cover = FALSE
						ELSE
							s_stretch.v_dest = <<-576.3989, -1605.4574, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.2920, 2.0, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
							
							s_stretch.b_is_using_secondary_cover = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF

						b_player_stole_cover = TRUE
						s_stretch.b_refresh_tasks = TRUE
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
				
				IF b_time_to_progress
					//If the cover point was changed due to the player stealing it then revert back (the ped needs to attempt to get to the original cover point).
					IF s_stretch.v_dest.x != -580.3932
						s_stretch.v_dest = <<-580.3932, -1608.8782, 26.0108>>
						SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.2920, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
					ENDIF
				
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						TRANSITION_FROM_MOCAP_TO_COVER(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.v_dest, 1.0, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_stretch.ped, s_stretch.i_timer, s_stretch.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																				       s_stretch.cover, TRUE, TRUE, 0.75, TRUE, FALSE)
						IF e_current_transition_state = SST_READY_TO_START_PLAYING
							IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_15")
								s_stretch.v_dest = <<-589.0823, -1610.1108, 26.0110>>
								SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.3195, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
								
								s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-576.169, -1610.406, 26.032>>, <<0.000, 0.000, -120.000>>)
								TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_15", "lam_1_ig_15_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
														SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
														RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
								REFILL_PED_AMMO_CLIP(s_stretch.ped)
								
								s_stretch.b_is_using_secondary_cover = FALSE
								s_stretch.b_refresh_tasks = FALSE
								s_stretch.i_timer = 0
								s_stretch.i_event++
							ENDIF
						ELIF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
							s_stretch.v_dest = <<-589.0823, -1610.1108, 26.0110>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.3195, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
							
							s_stretch.b_is_using_secondary_cover = FALSE
							s_stretch.b_refresh_tasks = TRUE
							s_stretch.i_timer = 0
							s_stretch.i_event++
						ENDIF
					ENDIF
				ELSE
					//Handle the transition from mocap to combat if the scene is still playing.
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					AND NOT s_stretch.b_refresh_tasks
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.i_timer, s_stretch.v_dest, 1.0, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
						IF s_stretch.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
							OPEN_SEQUENCE_TASK(seq)
								IF f_dist_to_destination > 4.0
								AND NOT b_player_stole_cover
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-590.1738, -1606.6678, 27.6159>>, PEDMOVE_RUN, FALSE, 1.0, 1.0, TRUE, ENAV_NO_STOPPING)
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_stretch.v_dest, 1000, FALSE, SLOW_BLEND_DURATION, TRUE, FALSE, s_stretch.cover, TRUE)
								ENDIF
								
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_stretch.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 53 //Once all enemies are dead Stretch runs to warehouse exit and waits
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_17")
			
				BOOL b_use_aim_task
				INT i_total_enemies
				i_total_enemies = GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_roof_1) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_warehouse)
			
				//Play some gunshots if he's playing the previous mocap
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					FLOAT f_anim_phase
					f_anim_phase = GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene)
					
					IF f_anim_phase > 0.25 AND f_anim_phase < 0.4
						REFILL_PED_AMMO_CLIP(s_stretch.ped)
					
						IF NOT IS_PED_INJURED(s_swat_indoor_warehouse[2].ped)
							SET_PED_SHOOTS_AT_COORD(s_stretch.ped, GET_ENTITY_COORDS(s_swat_indoor_warehouse[2].ped))
							SET_ENTITY_HEALTH(s_swat_indoor_warehouse[2].ped, 0)
						ELSE
							IF i_total_enemies > 0
								SET_PED_SHOOTS_AT_COORD(s_stretch.ped, <<-594.93, -1607.64, 27.8>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				//Progress condition.
				IF i_total_enemies = 0
					b_time_to_progress = TRUE
				ELSE
					IF i_total_enemies = 1
					AND NOT IS_PED_INJURED(s_swat_indoor_warehouse[3].ped) 
					AND IS_ENTITY_IN_ANGLED_AREA(s_swat_indoor_warehouse[3].ped, <<-605.636597,-1608.953491,26.010830>>, <<-594.884644,-1609.297485,29.010830>>, 5.250000)
						IF i_stretch_leave_warehouse_timer = 0
							i_stretch_leave_warehouse_timer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - i_stretch_leave_warehouse_timer > 5000
							b_time_to_progress = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Check if the player is stealing Stretch's destination: if so stop any synchronized scene, pick the alternate cover and force a refresh of tasks.
				IF NOT s_stretch.b_refresh_tasks
					BOOL b_player_stole_dest
				
					//If Stretch moved out to the open secondary point then make the steal check less likely to trigger.
					IF NOT s_stretch.b_is_using_secondary_cover
						b_player_stole_dest = IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 5.0)
					ELSE
						b_player_stole_dest = IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 4.0, 1.0)
					ENDIF	
				
					IF b_player_stole_dest
						IF NOT b_time_to_progress
							IF s_stretch.b_is_using_secondary_cover
								s_stretch.v_dest = <<-589.0823, -1610.1108, 26.0110>>
								SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.3195, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
								
								s_stretch.b_is_using_secondary_cover = FALSE
							ELSE
								s_stretch.v_dest = <<-588.1877, -1608.4750, 26.0108>>
								REMOVE_COVER_POINT(s_stretch.cover)
								SET_PED_SPHERE_DEFENSIVE_AREA(s_stretch.ped, s_stretch.v_dest, 1.0, TRUE)
								
								s_stretch.b_is_using_secondary_cover = TRUE
							ENDIF
							
							b_player_stole_cover = TRUE
							s_stretch.b_refresh_tasks = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					//If the last enemy alive is the one that has to be triggered by the player then switch the buddy to an aim task.
					IF i_total_enemies = 1
						IF NOT IS_PED_INJURED(s_swat_indoor_warehouse[4].ped)
							IF NOT IS_PED_IN_COMBAT(s_swat_indoor_warehouse[4].ped)
								b_use_aim_task = TRUE
								
								IF b_is_in_combat
									s_stretch.b_refresh_tasks = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//If the final enemy came out while the buddy was doing an aim task then make sure that they go back to combat.
					IF i_total_enemies = 1
						IF NOT b_use_aim_task
						AND NOT b_is_in_combat
						AND NOT b_time_to_progress
							s_stretch.b_refresh_tasks = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
				
				IF b_time_to_progress
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						TRANSITION_FROM_MOCAP_TO_COVER(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.v_dest, 0.99, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						IF s_stretch.v_dest.x != -589.0823
							s_stretch.v_dest = <<-589.0823, -1610.1108, 26.0110>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover,  s_stretch.v_dest, 90.3195, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
						ENDIF
					
						e_current_transition_state = GET_PED_IN_COVER_FOR_SYNCED_SCENE(s_stretch.ped, s_stretch.i_timer, s_stretch.v_dest, <<-604.2, -1603.5314, 27.5187>>,
																				       s_stretch.cover, TRUE, TRUE, 0.75)
						IF e_current_transition_state = SST_READY_TO_START_PLAYING
							IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_17")
								s_stretch.v_dest = <<-602.9832, -1611.9431, 26.0108>> 
								SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 188.2352, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
								
								//s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-590.020, -1610.470, 26.030>>, <<0.000, 0.000, 90.0000>>)
								s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-576.169, -1610.406, 26.032>>, <<0.000, 0.000, -120.000>>)
								TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_17", "lam_1_ig_17_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
														SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
														RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
								
								s_stretch.b_is_using_secondary_cover = FALSE
								s_stretch.b_refresh_tasks = FALSE
								s_stretch.i_timer = 0
								s_stretch.i_event++
							ENDIF
						ELIF e_current_transition_state = SST_PLAYER_IS_BLOCKING_START_POINT
							s_stretch.v_dest = <<-602.9832, -1611.9431, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 188.2352, 1.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
						
							s_stretch.b_is_using_secondary_cover = FALSE
							s_stretch.b_refresh_tasks = TRUE
							s_stretch.i_timer = 0
							s_stretch.i_event++
						ENDIF
					ENDIF
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					AND NOT s_stretch.b_refresh_tasks 
						TRANSITION_FROM_MOCAP_TO_COVER_THEN_COMBAT(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.i_timer, s_stretch.v_dest, 1.0, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
						IF s_stretch.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						AND (f_dist_to_destination > 4.0 OR NOT b_is_in_combat))	
							OPEN_SEQUENCE_TASK(seq)
								IF f_dist_to_destination > 4.0
								AND NOT b_player_stole_cover
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-597.1738, -1606.1678, 27.6159>>, PEDMOVE_RUN, FALSE, 1.0, 1.0, TRUE, ENAV_NO_STOPPING)
								ENDIF
								
								IF b_use_aim_task
									TASK_AIM_GUN_AT_COORD(NULL, <<-597.1738, -1606.1678, 27.6159>>, -1)
								ELSE
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
								ENDIF
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_stretch.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 54 //Go to the exit and wait for the enemies to breach.
				//Fire bullets at the last swat guy if he's still alive.
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
				AND NOT IS_PED_INJURED(s_swat_indoor_warehouse[3].ped) 
					IF GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.25 AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) < 0.36
						SET_PED_SHOOTS_AT_COORD(s_stretch.ped, GET_ENTITY_COORDS(s_swat_indoor_warehouse[3].ped))
					ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.34
						SET_ENTITY_HEALTH(s_swat_indoor_warehouse[3].ped, 0)
					ENDIF
				ENDIF
			
				//Check if the player is stealing cover, if so flag the tasks to be updated and change destinations.
				IF NOT s_stretch.b_refresh_tasks
					BOOL b_player_stole_dest
				
					//If Stretch moved out to the open secondary point then make the steal check less likely to trigger.
					IF NOT s_stretch.b_is_using_secondary_cover
						b_player_stole_dest = IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 5.0)
					ELSE
						b_player_stole_dest = IS_PLAYER_STEALING_PEDS_DESTINATION(s_stretch.ped, s_stretch.v_dest, 4.0, 1.0)
					ENDIF	
				
					IF b_player_stole_dest
						IF s_stretch.b_is_using_secondary_cover
							s_stretch.v_dest = <<-602.9832, -1611.9431, 26.0108>>
							SET_PED_COVER_POINT_AND_SPHERE(s_stretch.ped, s_stretch.cover, s_stretch.v_dest, 188.2352, 2.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
							
							s_stretch.b_is_using_secondary_cover = FALSE
						ELSE
							s_stretch.v_dest = <<-602.3002, -1609.1710, 26.0108>>
							REMOVE_COVER_POINT(s_stretch.cover)
							SET_PED_SPHERE_DEFENSIVE_AREA(s_stretch.ped, s_stretch.v_dest, 1.0, TRUE)
							
							s_stretch.b_is_using_secondary_cover = TRUE
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF

						b_player_stole_cover = TRUE
						s_stretch.b_refresh_tasks = TRUE
					ENDIF
				ENDIF
				
				f_dist_to_destination = VDIST2(v_stretch_pos, s_stretch.v_dest)
			
				IF s_swat_indoor_breach_2[0].i_event > 0
					s_stretch.v_dest = <<-605.6207, -1615.9396, 26.0395>>
				
					SET_PED_SPHERE_DEFENSIVE_AREA(s_stretch.ped, s_stretch.v_dest, 2.0)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_stretch.ped, 50.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
					
					s_stretch.i_timer = 0
					s_stretch.i_event++
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					AND NOT s_stretch.b_refresh_tasks
						TRANSITION_FROM_MOCAP_TO_COVER(s_stretch.ped, s_stretch.i_sync_scene, s_stretch.v_dest, 1.0, 0.5, TRUE, FALSE, s_stretch.cover)
					ELSE
						//Update the tasks if a refresh is required (player stole cover or ped was knocked out of tasks in some way)
						IF s_stretch.b_refresh_tasks
						OR (GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						AND f_dist_to_destination > 4.0)	
							OPEN_SEQUENCE_TASK(seq)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							
								IF f_dist_to_destination > 4.0
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-605.60, -1624.94, 27.54>>, PEDMOVE_RUN, FALSE, 1.0, 1.0, TRUE, ENAV_NO_STOPPING)
								ENDIF
								
								IF s_stretch.b_is_using_secondary_cover
									TASK_AIM_GUN_AT_COORD(NULL, <<-605.60, -1624.94, 27.54>>, -1)
								ELSE
									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, s_stretch.v_dest, -1, FALSE, 0.5, TRUE, FALSE, s_stretch.cover, TRUE)
								ENDIF
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)
						
							s_stretch.b_refresh_tasks = FALSE
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 55 //Once the final breach enemies die, Stretch runs to the exit and waits
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach_2)
					s_stretch.v_dest = <<-605.6207, -1615.9396, 26.0395>>

					s_stretch.b_refresh_tasks = TRUE
					s_stretch.i_timer = 0
					s_stretch.i_event++
				ENDIF
			BREAK
			
			CASE 56 //Stretch follows the player out of the building: calculate where Stretch should be based on the player's position (he follows behind).
				VECTOR v_stretch_new_pos, v_new_pos_1, v_new_pos_2, v_new_pos_3, v_exit_pos, v_aim_pos
				v_new_pos_1 = <<-605.6207, -1615.9396, 26.0395>>
				v_new_pos_2 = <<-605.9904, -1630.7469, 26.0108>>
				v_new_pos_3 = <<-597.8649, -1631.3746, 26.0108>>
				v_exit_pos = <<-591.7455, -1630.4834, 27.2158>>
				
				v_aim_pos = <<-601.0, -1630.2, 27.2>>
				
				IF VDIST2(v_player_pos, v_exit_pos) < VDIST2(v_new_pos_3, v_exit_pos)
					v_stretch_new_pos = v_new_pos_3
					v_aim_pos = <<-591.7455, -1630.4834, 27.2158>>
				ELIF VDIST2(v_player_pos, v_new_pos_3) < VDIST2(v_new_pos_2, v_new_pos_3)
					IF NOT ARE_VECTORS_ALMOST_EQUAL(s_stretch.v_dest, v_new_pos_3)
						v_stretch_new_pos = v_new_pos_2
						v_aim_pos = <<-591.7455, -1630.4834, 27.2158>>
					ENDIF
				ELSE
					IF NOT ARE_VECTORS_ALMOST_EQUAL(s_stretch.v_dest, v_new_pos_2)
					AND NOT ARE_VECTORS_ALMOST_EQUAL(s_stretch.v_dest, v_new_pos_1)
						v_stretch_new_pos = v_new_pos_1
						v_aim_pos = <<-601.0, -1630.2, 27.2>>
					ENDIF
				ENDIF
				
				IF NOT ARE_VECTORS_ALMOST_EQUAL(s_stretch.v_dest, v_stretch_new_pos)
				AND NOT ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, 0.0>>, v_stretch_new_pos)
					s_stretch.v_dest = v_stretch_new_pos
					s_stretch.b_refresh_tasks = TRUE
				ENDIF
				
				IF s_stretch.b_refresh_tasks
					OPEN_SEQUENCE_TASK(seq)
						IF VDIST2(<<-598.5, -1608.4, 26.0109>>, <<-605.5658, -1628.2056, 26.0109>>) < VDIST2(v_stretch_pos, <<-605.5658, -1628.2056, 26.0109>>)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-598.5, -1608.4, 26.0109>>, <<-601.0, -1630.2, 27.2>>, PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
						ENDIF
						
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, v_aim_pos, PEDMOVE_RUN, FALSE, 0.5, 2.0)
						TASK_AIM_GUN_AT_COORD(NULL, v_aim_pos, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)

					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
					s_stretch.b_refresh_tasks = FALSE
				ENDIF
				
				IF b_player_reached_exit
					//Stagger Stretch moving so he doesn't go at exactly the same time as Lamar.
					IF s_stretch.i_timer = 0
						s_stretch.i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_stretch.i_timer > 500
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(s_stretch.ped, <<-589.4766, -1639.8903, 18.8855>>, <<-587.2, -1631.2, 26.0>>, PEDMOVE_RUN, FALSE, 1.0, 1.0)
						
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK
			
			/*CASE 56 //Stretch waits until the player goes near the exit (or has already gone ahead), then proceeds.			
				IF b_player_reached_exit
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.929688,-1630.875854,26.023642>>, <<-601.413757,-1630.211304,29.010828>>, 4.000000)
					//Stagger Stretch moving so he doesn't go at exactly the same time as Lamar.
					IF s_stretch.i_timer = 0
						s_stretch.i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_stretch.i_timer > 500
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(s_stretch.ped, <<-589.4766, -1639.8903, 18.8855>>, <<-587.2, -1631.2, 26.0>>, PEDMOVE_RUN, FALSE, 1.0, 1.0)
						
						s_stretch.i_timer = 0
						s_stretch.i_event++
					ENDIF
				ENDIF
			BREAK*/
			
			CASE 57 //Once Stretch exits the building, he takes aim at the helicopter up ahead
				IF IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-592.567871,-1630.367554,28.238453>>, <<-587.906555,-1630.824463,23.511858>>, 2.500000)
					STORE_WHICH_BUDDY_IS_CLOSEST_TO_LADDER()
					
					IF b_lamar_is_closest_to_ladder
						s_stretch.v_dest = <<-593.2680, -1639.5262, 18.9594>>
					ELSE
						s_stretch.v_dest = <<-592.8024, -1642.5282, 18.9567>>
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
					
					//Can maybe remove this? it's being constantly checked next section anyway.
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-580.1240, -1634.6755, 18.5787>>, <<-563.72, -1649.0, 29.8111>>, 
															   PEDMOVE_RUN, FALSE, 0.5, 0.5, TRUE, ENAV_NO_STOPPING)
						//TASK_GO_STRAIGHT_TO_COORD(NULL, s_stretch.v_dest, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, 150.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, s_stretch.v_dest, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.25, ENAV_STOP_EXACTLY, 150.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					b_stretch_reached_ladder = FALSE
					b_stretch_started_climbing_ladder = FALSE
					s_stretch.i_timer = GET_GAME_TIMER()
					s_stretch.i_event++
				ENDIF
			BREAK
			
			CASE 58 //Climbs the ladder once he reaches the bottom.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_18")
			
				IF VDIST2(v_stretch_pos, <<-592.8024, -1642.5282, 18.9567>>) < 1.3
				//IF IS_PED_SAFE_TO_PLAY_SEAMLESS_ANIM(s_stretch.ped, s_stretch.v_dest, 150.0, 1.25, 45.0)
					b_stretch_reached_ladder = TRUE
					
					VECTOR v_lamar_pos 
					IF NOT IS_PED_INJURED(s_lamar.ped)
						v_lamar_pos = GET_ENTITY_COORDS(s_lamar.ped)
					ENDIF
					
					IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_18")
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.221619,-1642.937744,18.924829>>, <<-593.818970,-1641.983887,22.709436>>, 1.250000)
						IF (b_lamar_started_climbing_ladder AND v_lamar_pos.z > 22.0) //21.5
						OR NOT b_lamar_started_climbing_ladder //Stretch takes priority if both buddies are at the ladder.
							TASK_CLIMB_LADDER(s_stretch.ped, TRUE)
							
							TASK_LOOK_AT_ENTITY(s_lamar.ped, s_stretch.ped, 8000)
							
							e_stretch_ladder_state = LIS_CLIMB_UP
							
							b_stretch_started_climbing_ladder = TRUE
							s_stretch.i_timer = 0
							s_stretch.i_event++	
						ENDIF
					ENDIF
				ELSE
					//Constantly check if Stretch is in the right position to climb the ladder.
					IF GET_GAME_TIMER() - s_stretch.i_timer > 1000
						STORE_WHICH_BUDDY_IS_CLOSEST_TO_LADDER()
						
						IF b_lamar_is_closest_to_ladder
							s_stretch.v_dest = <<-593.2680, -1639.5262, 18.9594>>
						ELSE
							s_stretch.v_dest = <<-592.8024, -1642.5282, 18.9567>>
						ENDIF
					
						IF VDIST2(v_stretch_pos, s_stretch.v_dest) >= 1.3
							IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
								IF VDIST2(v_stretch_pos, s_stretch.v_dest) > 16.0
									TASK_FOLLOW_NAV_MESH_TO_COORD(s_stretch.ped, s_stretch.v_dest, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.25, ENAV_STOP_EXACTLY, 150.0)
								ELSE
									TASK_GO_STRAIGHT_TO_COORD(s_stretch.ped, s_stretch.v_dest, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 150.0, 0.1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 59
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_18")
				
				BOOL b_player_blocking_anim
				VECTOR v_anim_offset
				
				IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_18")
					v_anim_offset = GET_ANIM_INITIAL_OFFSET_POSITION("misslamar1lam_1_ig_18", "lam_1_ig_18_stretch", <<-593.060, -1642.835, 19.004>>, <<0.0, 0.0, 0.0>>, 0.421)
				ENDIF
				
				IF IS_PED_CLIMBING(PLAYER_PED_ID())
					IF v_player_pos.z > v_stretch_pos.z
						b_player_blocking_anim = TRUE
					ENDIF
				ENDIF
				
				IF IS_PED_CLIMBING(s_stretch.ped)
					IF b_player_blocking_anim
						IF e_stretch_ladder_state != LIS_NOTHING
							SET_LADDER_CLIMB_INPUT_STATE(s_stretch.ped, LIS_NOTHING)
							e_stretch_ladder_state = LIS_NOTHING
						ENDIF
					ELSE
						IF e_stretch_ladder_state != LIS_CLIMB_UP
							SET_LADDER_CLIMB_INPUT_STATE(s_stretch.ped, LIS_CLIMB_UP)
							e_stretch_ladder_state = LIS_CLIMB_UP
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_CLIMB_LADDER) = PERFORMING_TASK
					STOP_PLAYER_INTERSECTING_BUDDY_ON_LADDER(v_player_pos, v_stretch_pos, FALSE)
				
					IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_18")
						s_stretch.v_dest = v_anim_offset
						
						IF ABSF(v_stretch_pos.z - v_anim_offset.z) < 0.2
							IF NOT b_player_blocking_anim
								s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-593.060, -1642.835, 19.004>>, <<0.000, 0.000, 0.0000>>)
								TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_18", "lam_1_ig_18_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
														SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
														RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING, WALK_BLEND_IN)
								SET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene, 0.421)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
								SET_CURRENT_PED_WEAPON(s_stretch.ped, WEAPONTYPE_PISTOL, TRUE)
								
								s_stretch.i_timer = 0
								s_stretch.i_event++	
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF ABSF(v_stretch_pos.z - v_anim_offset.z) > 0.2
						//If Lamar has been knocked out of the ladder task then go back.
						b_stretch_started_climbing_ladder = FALSE
						b_stretch_reached_ladder = FALSE
						s_stretch.i_timer = 0
						s_stretch.i_event--
					ELSE
						//Lamar is probably paused on the ladder, waiting for the player to move.
						IF NOT b_player_blocking_anim
							TASK_CLIMB_LADDER(s_stretch.ped, TRUE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 60 //Waits for Franklin to kill the chopper and climb up
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_19")
			
				s_stretch.v_dest = <<-596.36, -1649.46, 25.14>>
			
				//Stretch's gantry run is faster than Lamar's, but Lamar could potentially climb the ladder earlier. This check determines if it's safe for Lamar to run
				//first if he happened to climb the ladder first (i.e. Stretch's anim won't catch up). By default this is TRUE, then becomes FALSE once Stretch has nearly
				//finished climbing.
				IF b_safe_for_lamar_to_run_gantry_first
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.7)
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
						b_safe_for_lamar_to_run_gantry_first = FALSE
					ENDIF
				ENDIF
				
				BOOL b_player_in_trigger_position
				
				IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) < 2
					IF b_player_climbed_ladder 
					OR b_player_escaped 
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-593.126221,-1643.062622,23.0>>, <<-592.603455,-1642.084351,26.957701>>, 1.500000)
						b_player_in_trigger_position = TRUE
					ENDIF
				ENDIF
			
				//Go into an aim task once finishing the climb anim.
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.9)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					IF b_player_in_trigger_position
						IF VDIST2(v_stretch_pos, s_stretch.v_dest) < 2.0
						OR (WOULD_ENTITY_BE_OCCLUDED(GET_ENTITY_MODEL(s_stretch.ped), v_stretch_pos)
						AND WOULD_ENTITY_BE_OCCLUDED(GET_ENTITY_MODEL(s_stretch.ped), s_stretch.v_dest))
							b_trigger_stretch_run_alternate_anim = FALSE
							s_stretch.i_timer = 0
							s_stretch.i_event = 100
						ELIF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
							b_trigger_stretch_run_alternate_anim = TRUE
							s_stretch.i_timer = 0
							s_stretch.i_event = 100
						ENDIF
					ELSE
						IF s_stretch.i_sync_scene != -1
							STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
							s_stretch.i_sync_scene = -1
						ENDIF
					
						//Stretch's mocap now starts from a later point, so move him over after climbing the ladder.
						IF VDIST2(v_stretch_pos, s_stretch.v_dest) > 2.0
							IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
								OPEN_SEQUENCE_TASK(seq)
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, s_stretch.v_dest, <<-563.72, -1649.0, 29.8111>>, PEDMOVE_RUN, FALSE, 0.25, 0.5, TRUE, ENAV_STOP_EXACTLY)
									
									IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) >= 2
										TASK_SHOOT_AT_ENTITY(NULL, veh_chopper_indoor_final, -1, FIRING_TYPE_RANDOM_BURSTS)
									ELSE
										TASK_AIM_GUN_AT_COORD(NULL, <<-563.72, -1649.0, 29.8111>>, -1)
									ENDIF
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					STOP_PLAYER_INTERSECTING_BUDDY_ON_LADDER(v_player_pos, v_stretch_pos, TRUE)
				ENDIF
			BREAK
		
			CASE 100 //Split off from the previous stage as there's a J-skip that goes here.
				REQUEST_ANIM_DICT("misslamar1lam_1_ig_19")
			
				IF HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_19")
					s_stretch.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-593.078, -1642.909, 18.951>>, <<0.000, 0.000, 0.0000>>)
					IF b_trigger_stretch_run_alternate_anim
						TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_19", "LAM_1_IG_19_by_ladder_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING | RBF_VEHICLE_IMPACT, 
												WALK_BLEND_IN)
					ELSE
						TASK_SYNCHRONIZED_SCENE(s_stretch.ped, s_stretch.i_sync_scene, "misslamar1lam_1_ig_19", "lam_1_ig_19_stretch", WALK_BLEND_IN, WALK_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_IMPACT_OBJECT | RBF_FALLING | RBF_VEHICLE_IMPACT, 
												WALK_BLEND_IN) //1717229 - Added vehicle blocking for ragdoll as it's possible for a helicopter to knock the ped off the roofs and break the mission.
					ENDIF
					SET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene, 0.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
					SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_BlockPedFromTurningInCover, FALSE)
					
					s_stretch.i_timer = 0
					s_stretch.i_event++	
				ENDIF
			BREAK
			
			CASE 101
				IF IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					//Block player climbing if the buddy is currently climbing.
					IF (b_trigger_stretch_run_alternate_anim AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.755 AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) < 0.892)
					OR (NOT b_trigger_stretch_run_alternate_anim AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.594 AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) < 0.71)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-603.746277,-1697.936157,24.043583>>, <<-602.353516,-1695.986938,26.901522>>, 1.000000)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
						ENDIF
					ENDIF
				
					//If the player already escaped then skip the scene forwards.
					IF b_player_escaped AND b_lamar_escaped
						IF GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) < 0.7
							IF NOT IS_SPHERE_VISIBLE(<<-603.1, -1696.5, 24.9>>, 1.0)
							AND NOT IS_SPHERE_VISIBLE(v_stretch_pos, 1.0)
								SET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene, 0.7)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.99)
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.75 AND VDIST2(v_stretch_pos, <<-605.1, -1699.6, 23.3>>) < 4.0)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-606.7747, -1700.0148, 23.0618>>, <<-625.4585, -1699.5992, 24.8108>>, PEDMOVE_WALK, FALSE, 0.5, 0.5)
						TASK_AIM_GUN_AT_COORD(NULL, <<-625.4585, -1699.5992, 24.8108>>, -1)
					CLOSE_SEQUENCE_TASK(seq)
					
					STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
					TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					s_stretch.i_sync_scene = -1
					s_stretch.i_timer = 0
					s_stretch.i_event++
				ENDIF
			BREAK
		ENDSWITCH
		
		//Kill instantly if player throws an explosive at them
		HANDLE_BUDDY_DEATHS_FROM_EXPLOSIONS(s_stretch.ped, s_stretch.i_health)
		
		#IF IS_DEBUG_BUILD
			IF b_debug_display_mission_debug
				DRAW_INT_TO_DEBUG_DISPLAY(s_stretch.i_event, "Stretch: ")
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds some random cop set-pieces around the location where the player escapes the plant.
PROC UPDATE_SETPIECE_COPS()
	IF b_player_escaped
		INT i = 0
		SEQUENCE_INDEX seq
		VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())

		//Keep the cops model in memory 
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			IF NOT s_cops_setpiece_1[0].b_is_created
			OR NOT s_cops_setpiece_1b[0].b_is_created
			OR NOT s_cops_setpiece_1c[0].b_is_created
				REQUEST_MODEL(model_cop)
				REQUEST_MODEL(model_cop_car)
			ENDIF
		ENDIF
		
		IF NOT s_cops_setpiece_1[0].b_is_created
			i_time_exterior_cop_cars_created = 0
		
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF HAS_MODEL_LOADED(model_cop)
				AND HAS_MODEL_LOADED(model_cop_car)
					IF s_cops_setpiece_1[0].i_timer = 0
						s_cops_setpiece_1[0].i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_cops_setpiece_1[0].i_timer > 3000
					OR b_skipped_to_lose_cops_stage
						veh_cops_setpiece_1 = CREATE_VEHICLE(model_cop_car,  <<-477.9164, -1776.6316, 19.8944 >>, 89.4953)
					
						s_cops_setpiece_1[0].ped = CREATE_PED_INSIDE_VEHICLE(veh_cops_setpiece_1, PEDTYPE_COP, model_cop, VS_DRIVER) 
						s_cops_setpiece_1[1].ped = CREATE_PED_INSIDE_VEHICLE(veh_cops_setpiece_1, PEDTYPE_COP, model_cop, VS_FRONT_RIGHT) 
						
						REPEAT COUNT_OF(s_cops_setpiece_1) i
							GIVE_WEAPON_TO_PED(s_cops_setpiece_1[i].ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(s_cops_setpiece_1[i].ped, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1[i].ped, TRUE)
							
							#IF IS_DEBUG_BUILD
								SET_PED_NAME_DEBUG(s_cops_setpiece_1[i].ped, "1")
							#ENDIF
							
							s_cops_setpiece_1[i].b_is_created = TRUE
							s_cops_setpiece_1[i].i_event = 0
						ENDREPEAT
						
						i_time_exterior_cop_cars_created = GET_GAME_TIMER()
						SET_VEHICLE_ENGINE_ON(veh_cops_setpiece_1, TRUE, TRUE)
						SET_VEHICLE_LIGHTS(veh_cops_setpiece_1, FORCE_VEHICLE_LIGHTS_ON)
						SET_VEHICLE_SIREN(veh_cops_setpiece_1, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(s_cops_setpiece_1) i
				IF DOES_ENTITY_EXIST(s_cops_setpiece_1[i].ped)
					IF NOT IS_PED_INJURED(s_cops_setpiece_1[i].ped)
						IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
							SWITCH s_cops_setpiece_1[i].i_event
								CASE 0
									s_cops_setpiece_1[i].v_dest = <<-579.4522, -1728.0026, 21.7140>>
								
									IF i = 0
										IF IS_VEHICLE_DRIVEABLE(veh_cops_setpiece_1)
											TASK_VEHICLE_DRIVE_TO_COORD(s_cops_setpiece_1[i].ped, veh_cops_setpiece_1, s_cops_setpiece_1[i].v_dest, 
																		20.0, DRIVINGSTYLE_NORMAL, model_cop_car, DRIVINGMODE_AVOIDCARS, 4.0, 5.0)
										ENDIF
									ENDIF
									
									s_cops_setpiece_1[i].i_timer = 0
									s_cops_setpiece_1[i].i_event++
								BREAK
								
								CASE 1
									IF VDIST2(GET_ENTITY_COORDS(s_cops_setpiece_1[i].ped), s_cops_setpiece_1[i].v_dest) < 25.0
									OR VDIST2(GET_ENTITY_COORDS(s_cops_setpiece_1[i].ped), v_player_pos) < 100.0
										IF i = 0
											IF IS_VEHICLE_DRIVEABLE(veh_cops_setpiece_1)
												TASK_VEHICLE_TEMP_ACTION(s_cops_setpiece_1[i].ped, veh_cops_setpiece_1, TEMPACT_SWERVELEFT_STOP, 1000)
											ENDIF
										ENDIF

										s_cops_setpiece_1[i].i_timer = GET_GAME_TIMER()
										s_cops_setpiece_1[i].i_event++
									ENDIF
								BREAK
								
								CASE 2
									IF GET_GAME_TIMER() - s_cops_setpiece_1[i].i_timer > 2000
										OPEN_SEQUENCE_TASK(seq)
											TASK_LEAVE_ANY_VEHICLE(NULL, i * 200, ECF_DONT_CLOSE_DOOR)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(s_cops_setpiece_1[i].ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1[i].ped, FALSE)
										s_cops_setpiece_1[i].i_timer = 0
										s_cops_setpiece_1[i].i_event++
									ENDIF
								BREAK
								
								CASE 3
									IF IS_PED_IN_COMBAT(s_cops_setpiece_1[i].ped)
										CLEAN_UP_ENEMY_PED(s_cops_setpiece_1[i])
									ENDIF
								BREAK
							ENDSWITCH
						ELSE
							CLEAR_PED_TASKS(s_cops_setpiece_1[i].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1[i].ped, FALSE)
							CLEAN_UP_ENEMY_PED(s_cops_setpiece_1[i])
						ENDIF
					ELSE
						CLEAN_UP_ENEMY_PED(s_cops_setpiece_1[i])
					ENDIF
				ELSE
					REMOVE_VEHICLE(veh_cops_setpiece_1)
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		IF NOT s_cops_setpiece_1b[0].b_is_created
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF HAS_MODEL_LOADED(model_cop)
				AND HAS_MODEL_LOADED(model_cop_car)
					IF s_cops_setpiece_1b[0].i_timer = 0
						s_cops_setpiece_1b[0].i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_cops_setpiece_1b[0].i_timer > 3000
					OR b_skipped_to_lose_cops_stage
						veh_cops_setpiece_1b = CREATE_VEHICLE(model_cop_car,  <<-471.3610, -1782.5648, 19.8860>>, 94.4439)
						
						s_cops_setpiece_1b[0].ped = CREATE_PED_INSIDE_VEHICLE(veh_cops_setpiece_1b, PEDTYPE_COP, model_cop, VS_DRIVER) 
						s_cops_setpiece_1b[1].ped = CREATE_PED_INSIDE_VEHICLE(veh_cops_setpiece_1b, PEDTYPE_COP, model_cop, VS_FRONT_RIGHT) 
						
						REPEAT COUNT_OF(s_cops_setpiece_1b) i
							GIVE_WEAPON_TO_PED(s_cops_setpiece_1b[i].ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(s_cops_setpiece_1b[i].ped, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1b[i].ped, TRUE)
							
							#IF IS_DEBUG_BUILD
								SET_PED_NAME_DEBUG(s_cops_setpiece_1b[i].ped, "1b")
							#ENDIF
							
							s_cops_setpiece_1b[i].b_is_created = TRUE
							s_cops_setpiece_1b[i].i_event = 0
						ENDREPEAT
						
						SET_VEHICLE_ENGINE_ON(veh_cops_setpiece_1b, TRUE, TRUE)
						SET_VEHICLE_LIGHTS(veh_cops_setpiece_1b, FORCE_VEHICLE_LIGHTS_ON)
						SET_VEHICLE_SIREN(veh_cops_setpiece_1b, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(s_cops_setpiece_1b) i
				IF DOES_ENTITY_EXIST(s_cops_setpiece_1b[i].ped)
					IF NOT IS_PED_INJURED(s_cops_setpiece_1b[i].ped)
						IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
							SWITCH s_cops_setpiece_1b[i].i_event
								CASE 0
									s_cops_setpiece_1b[i].v_dest = <<-569.6553, -1740.9001, 21.3580>>
								
									IF i = 0
										IF IS_VEHICLE_DRIVEABLE(veh_cops_setpiece_1b)
											TASK_VEHICLE_DRIVE_TO_COORD(s_cops_setpiece_1b[i].ped, veh_cops_setpiece_1b, s_cops_setpiece_1b[i].v_dest, 
																		20.0, DRIVINGSTYLE_NORMAL, model_cop_car, DRIVINGMODE_AVOIDCARS, 4.0, 5.0)
										ENDIF
									ENDIF
									
									s_cops_setpiece_1b[i].i_timer = 0
									s_cops_setpiece_1b[i].i_event++
								BREAK
								
								CASE 1
									IF VDIST2(GET_ENTITY_COORDS(s_cops_setpiece_1b[i].ped), s_cops_setpiece_1b[i].v_dest) < 25.0
									OR VDIST2(GET_ENTITY_COORDS(s_cops_setpiece_1b[i].ped), v_player_pos) < 100.0
										IF i = 0
											IF IS_VEHICLE_DRIVEABLE(veh_cops_setpiece_1b)
												TASK_VEHICLE_TEMP_ACTION(s_cops_setpiece_1b[i].ped, veh_cops_setpiece_1b, TEMPACT_SWERVELEFT_STOP, 1000)
											ENDIF
										ENDIF

										s_cops_setpiece_1b[i].i_timer = GET_GAME_TIMER()
										s_cops_setpiece_1b[i].i_event++
									ENDIF
								BREAK
								
								CASE 2
									IF GET_GAME_TIMER() - s_cops_setpiece_1b[i].i_timer > 2000
										OPEN_SEQUENCE_TASK(seq)
											TASK_LEAVE_ANY_VEHICLE(NULL, i * 200, ECF_DONT_CLOSE_DOOR)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(s_cops_setpiece_1b[i].ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1b[i].ped, FALSE)
										s_cops_setpiece_1b[i].i_timer = 0
										s_cops_setpiece_1b[i].i_event++
									ENDIF
								BREAK
								
								CASE 3
									IF IS_PED_IN_COMBAT(s_cops_setpiece_1b[i].ped)
										CLEAN_UP_ENEMY_PED(s_cops_setpiece_1b[i])
									ENDIF
								BREAK
							ENDSWITCH
						ELSE
							CLEAR_PED_TASKS(s_cops_setpiece_1b[i].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1b[i].ped, FALSE)
							CLEAN_UP_ENEMY_PED(s_cops_setpiece_1b[i])
						ENDIF
					ELSE
						CLEAN_UP_ENEMY_PED(s_cops_setpiece_1b[i])
					ENDIF
				ELSE
					REMOVE_VEHICLE(veh_cops_setpiece_1b)
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		IF NOT s_cops_setpiece_1c[0].b_is_created
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF HAS_MODEL_LOADED(model_cop)
				AND HAS_MODEL_LOADED(model_cop_car)
				AND s_cops_setpiece_1[0].b_is_created
				AND s_cops_setpiece_1b[0].b_is_created
					IF s_cops_setpiece_1c[0].i_timer = 0
						s_cops_setpiece_1c[0].i_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - s_cops_setpiece_1c[0].i_timer > 3000
					OR b_skipped_to_lose_cops_stage
						veh_cops_setpiece_1c = CREATE_VEHICLE(model_cop_car,  <<-649.9347, -1632.2976, 23.9329>>, 153.8572)
						
						s_cops_setpiece_1c[0].ped = CREATE_PED_INSIDE_VEHICLE(veh_cops_setpiece_1c, PEDTYPE_COP, model_cop, VS_DRIVER) 
						s_cops_setpiece_1c[1].ped = CREATE_PED_INSIDE_VEHICLE(veh_cops_setpiece_1c, PEDTYPE_COP, model_cop, VS_FRONT_RIGHT) 
						
						REPEAT COUNT_OF(s_cops_setpiece_1c) i
							GIVE_WEAPON_TO_PED(s_cops_setpiece_1c[i].ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(s_cops_setpiece_1c[i].ped, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1c[i].ped, TRUE)
							
							#IF IS_DEBUG_BUILD
								SET_PED_NAME_DEBUG(s_cops_setpiece_1c[i].ped, "1c")
							#ENDIF
							
							s_cops_setpiece_1c[i].b_is_created = TRUE
							s_cops_setpiece_1c[i].i_event = 0
						ENDREPEAT
						
						SET_VEHICLE_ENGINE_ON(veh_cops_setpiece_1c, TRUE, TRUE)
						SET_VEHICLE_LIGHTS(veh_cops_setpiece_1c, FORCE_VEHICLE_LIGHTS_ON)
						SET_VEHICLE_SIREN(veh_cops_setpiece_1c, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(model_cop)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_cop_car)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REPEAT COUNT_OF(s_cops_setpiece_1c) i
				IF DOES_ENTITY_EXIST(s_cops_setpiece_1c[i].ped)
					IF NOT IS_PED_INJURED(s_cops_setpiece_1c[i].ped)
						IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
							SWITCH s_cops_setpiece_1c[i].i_event
								CASE 0
									s_cops_setpiece_1c[i].v_dest = <<-626.8539, -1699.0125, 23.4229>>
								
									IF i = 0
										IF IS_VEHICLE_DRIVEABLE(veh_cops_setpiece_1c)
											TASK_VEHICLE_DRIVE_TO_COORD(s_cops_setpiece_1c[i].ped, veh_cops_setpiece_1c, s_cops_setpiece_1c[i].v_dest, 
																		20.0, DRIVINGSTYLE_NORMAL, model_cop_car, DRIVINGMODE_AVOIDCARS, 4.0, 5.0)
										ENDIF
									ENDIF
									
									s_cops_setpiece_1c[i].i_timer = 0
									s_cops_setpiece_1c[i].i_event++
								BREAK
								
								CASE 1
									IF VDIST2(GET_ENTITY_COORDS(s_cops_setpiece_1c[i].ped), s_cops_setpiece_1c[i].v_dest) < 25.0
									OR VDIST2(GET_ENTITY_COORDS(s_cops_setpiece_1c[i].ped), v_player_pos) < 100.0
										IF i = 0
											IF IS_VEHICLE_DRIVEABLE(veh_cops_setpiece_1c)
												TASK_VEHICLE_TEMP_ACTION(s_cops_setpiece_1c[i].ped, veh_cops_setpiece_1c, TEMPACT_SWERVELEFT_STOP, 1000)
											ENDIF
										ENDIF

										s_cops_setpiece_1c[i].i_timer = GET_GAME_TIMER()
										s_cops_setpiece_1c[i].i_event++
									ENDIF
								BREAK
								
								CASE 2
									IF GET_GAME_TIMER() - s_cops_setpiece_1c[i].i_timer > 2000
										OPEN_SEQUENCE_TASK(seq)
											TASK_LEAVE_ANY_VEHICLE(NULL, i * 200, ECF_DONT_CLOSE_DOOR)
											TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(s_cops_setpiece_1c[i].ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
										
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1c[i].ped, FALSE)
										s_cops_setpiece_1c[i].i_timer = 0
										s_cops_setpiece_1c[i].i_event++
									ENDIF
								BREAK
								
								CASE 3
									IF IS_PED_IN_COMBAT(s_cops_setpiece_1c[i].ped)
										CLEAN_UP_ENEMY_PED(s_cops_setpiece_1c[i])
									ENDIF
								BREAK
							ENDSWITCH
						ELSE
							CLEAR_PED_TASKS(s_cops_setpiece_1c[i].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_setpiece_1c[i].ped, FALSE)
							CLEAN_UP_ENEMY_PED(s_cops_setpiece_1c[i])
						ENDIF
					ELSE
						CLEAN_UP_ENEMY_PED(s_cops_setpiece_1c[i])
					ENDIF
				ELSE
					REMOVE_VEHICLE(veh_cops_setpiece_1c)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CARJACK_SEQUENCE()
	SEQUENCE_INDEX seq
	BOOL b_took_alternate_route

	SWITCH i_carjack_event
		CASE 0 //Setup the car: where the car is created depends on how the player reached this section and what the buddies are doing at the time.
			REQUEST_MODEL(model_escape_car)
			REQUEST_VEHICLE_ASSET(model_escape_car, ENUM_TO_INT(VRF_REQUEST_ENTRY_ANIMS))
			REQUEST_MODEL(model_carjack_victim)
			REQUEST_WAYPOINT_RECORDING(str_waypoint_carjack)
		
			IF HAS_MODEL_LOADED(model_escape_car)
			AND HAS_MODEL_LOADED(model_carjack_victim)
			AND HAS_VEHICLE_ASSET_LOADED(model_escape_car)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(str_waypoint_carjack)
				IF e_mission_stage = STAGE_GO_BACK_HOME
					//Skipped to the lose cops stage: buddies will already be there. Just create the car already jacked.
					veh_carjack = CREATE_VEHICLE(model_escape_car, <<-607.0482, -1707.6165, 22.8813>>, 42.6940)
					ped_carjack_victim = CREATE_PED_INSIDE_VEHICLE(veh_carjack, PEDTYPE_MISSION, model_carjack_victim, VS_DRIVER)
					SET_ENTITY_HEALTH(ped_carjack_victim, 0)
					SET_VEHICLE_DOOR_OPEN(veh_carjack, SC_DOOR_FRONT_LEFT)
					b_getaway_car_driver_is_dead = FALSE
					
					/*IF NOT IS_PED_INJURED(s_lamar.ped)
						TASK_AIM_GUN_AT_COORD(s_lamar.ped, <<-583.6, -1724.0, 23.6>>, -1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(s_stretch.ped)
						TASK_AIM_GUN_AT_COORD(s_stretch.ped, <<-617.7, -1707.8, 24.6>>, -1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
					ENDIF*/
					
					i_carjack_event = 100
				ELSE
					b_took_alternate_route = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-626.875427,-1679.488770,24.420506>>, <<-625.316833,-1677.468262,49.420506>>, 19.250000)
				
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-601.646606,-1696.296753,24.135693>>, <<-600.000854,-1693.806274,27.227722>>, 6.750000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-609.115173,-1690.960327,20.590954>>, <<-604.617249,-1693.887451,24.212742>>, 4.750000)
					OR b_took_alternate_route
						IF b_lamar_escaped
						AND b_stretch_escaped
							//Player was lagging behind, just create the car already in place with a dead ped (B*525244).
							IF NOT DOES_ENTITY_EXIST(veh_carjack)
								veh_carjack = CREATE_VEHICLE(model_escape_car, <<-607.0482, -1707.6165, 22.8813>>, 42.6940)
								ped_carjack_victim = CREATE_PED_INSIDE_VEHICLE(veh_carjack, PEDTYPE_MISSION, model_carjack_victim, VS_DRIVER)
								SET_ENTITY_HEALTH(ped_carjack_victim, 0)
								SET_VEHICLE_DOOR_OPEN(veh_carjack, SC_DOOR_FRONT_LEFT)
								b_getaway_car_driver_is_dead = TRUE
							ENDIF
							
							i_carjack_event = 100
						ELSE
							IF NOT b_took_alternate_route
								//Player is in front of buddies or near to them: create a car driving up on a waypoint recording.
								veh_carjack = CREATE_VEHICLE(model_escape_car, <<-537.2055, -1757.0132, 20.5996>>, 53.6758)
								ped_carjack_victim = CREATE_PED_INSIDE_VEHICLE(veh_carjack, PEDTYPE_MISSION, model_carjack_victim, VS_DRIVER)
								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_carjack_victim, TRUE)
								
								b_getaway_car_driver_is_dead = FALSE
								i_carjack_event++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(veh_carjack)
					SET_VEHICLE_COLOURS(veh_carjack, 0, 0)
					SET_VEHICLE_EXTRA_COLOURS(veh_carjack, 0, 0)
					SET_VEHICLE_STRONG(veh_carjack, TRUE)
					SET_VEHICLE_LIGHTS(veh_carjack, FORCE_VEHICLE_LIGHTS_ON)
					SET_VEH_RADIO_STATION(veh_carjack, "RADIO_01_CLASS_ROCK")
				ENDIF
				
				IF i_carjack_event != 0
					SET_MODEL_AS_NO_LONGER_NEEDED(model_escape_car)
					SET_MODEL_AS_NO_LONGER_NEEDED(model_carjack_victim)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(model_escape_car, TRUE)
				ENDIF
				
				b_carjack_vehicle_asset_requested = TRUE
				i_carjack_timer = 0
			ENDIF
		BREAK
		
		CASE 1 //Start the car playback once the player has climbed over
			REQUEST_WAYPOINT_RECORDING(str_waypoint_carjack)
		
			IF IS_VEHICLE_DRIVEABLE(veh_carjack)
			AND NOT IS_PED_INJURED(ped_carjack_victim)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(str_waypoint_carjack)
				IF b_player_escaped
				OR (IS_PLAYER_CLIMBING(PLAYER_ID()) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.898621,-1696.711060,24.816715>>, <<-600.363525,-1700.791504,28.066715>>, 2.500000))
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(ped_carjack_victim, veh_carjack, str_waypoint_carjack, DRIVINGMODE_STOPFORCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_carjack_victim, TRUE)
					
					s_lamar.b_refresh_tasks = TRUE
					s_stretch.b_refresh_tasks = TRUE
					
					i_carjack_timer = 0
					i_carjack_event++
				ENDIF
			ENDIF
			
			//Keep Lamar and Stretch ticking over until everyone has climbed the wall.
			IF NOT IS_PED_INJURED(s_lamar.ped)
			
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
			
			ENDIF
		BREAK
		
		CASE 2 //Have the buddies hijack the car.
			IF NOT IS_PED_INJURED(ped_carjack_victim)
			AND NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_INJURED(s_stretch.ped)
				BOOL b_lamar_aiming, b_stretch_aiming
				VECTOR v_victim_pos
				v_victim_pos = GET_ENTITY_COORDS(ped_carjack_victim)
				b_lamar_aiming = (GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK)
				b_stretch_aiming = (GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK)
				
				//Have the buddies aim at the victim.
				IF (b_lamar_escaped OR e_mission_stage = STAGE_GO_BACK_HOME)
				AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene)
				OR IS_SYNCHRONIZED_SCENE_RUNNING(s_lamar.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene) > 0.99)
					//If the cops have arrived and we're still waiting on another buddy then just go straight to group behaviour.
					IF b_player_escaped
					AND s_cops_setpiece_1[0].b_is_created 
					AND i_time_exterior_cop_cars_created != 0 
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_PED_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
							CLEAR_PED_TASKS(s_lamar.ped)
							SET_PED_AS_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, FALSE)
						ENDIF
					ELIF NOT b_lamar_aiming 
					OR s_lamar.b_refresh_tasks
						//If we're still waiting for the player/cops then just aim.
						IF IS_PED_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
							REMOVE_PED_FROM_GROUP(s_lamar.ped)
						ENDIF
					
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-607.9194, -1703.6476, 22.8549>>, ped_carjack_victim, PEDMOVE_RUN, FALSE)
							TASK_AIM_GUN_AT_ENTITY(NULL, ped_carjack_victim, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_lamar.ped, seq)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_lamar.i_sync_scene = -1
						s_lamar.b_refresh_tasks = FALSE
					ENDIF
				ENDIF
				
				
				IF (b_stretch_escaped OR e_mission_stage = STAGE_GO_BACK_HOME)
				AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene)
				OR IS_SYNCHRONIZED_SCENE_RUNNING(s_stretch.i_sync_scene) AND GET_SYNCHRONIZED_SCENE_PHASE(s_stretch.i_sync_scene) > 0.99)
					IF b_player_escaped
					AND s_cops_setpiece_1[0].b_is_created 
					AND i_time_exterior_cop_cars_created != 0 
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_PED_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
							CLEAR_PED_TASKS(s_stretch.ped)
							SET_PED_AS_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
						ENDIF
					ELIF NOT b_stretch_aiming 
					OR s_stretch.b_refresh_tasks
						IF IS_PED_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
							REMOVE_PED_FROM_GROUP(s_stretch.ped)
						ENDIF
					
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-611.5765, -1705.5597, 23.0669>>, ped_carjack_victim, PEDMOVE_RUN, FALSE)
							TASK_AIM_GUN_AT_ENTITY(NULL, ped_carjack_victim, -1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(s_stretch.ped, seq)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
						CLEAR_SEQUENCE_TASK(seq)
						
						s_stretch.i_sync_scene = -1
						s_stretch.b_refresh_tasks = FALSE
					ENDIF
				ENDIF
				
				//If anyone aims at the victim, make them flee.
				IF i_carjack_timer = 0
					IF ((IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped_carjack_victim) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped_carjack_victim)) 
						AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_victim_pos) < 100.0)
					OR (b_lamar_aiming AND VDIST2(GET_ENTITY_COORDS(s_lamar.ped), v_victim_pos) < 100.0)
					OR (b_stretch_aiming AND VDIST2(GET_ENTITY_COORDS(s_stretch.ped), v_victim_pos) < 100.0)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200.0, -1)
						CLOSE_SEQUENCE_TASK(seq)
					
						SET_PED_FLEE_ATTRIBUTES(ped_carjack_victim, FA_DISABLE_HANDS_UP, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped_carjack_victim, FA_USE_VEHICLE, FALSE)
						
						TASK_PERFORM_SEQUENCE(ped_carjack_victim, seq)
						CLEAR_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(ped_carjack_victim, TRUE)
						
						
						i_carjack_timer = GET_GAME_TIMER()
					ENDIF
				ELSE
					IF NOT IS_PED_IN_ANY_VEHICLE(ped_carjack_victim)
						IF NOT b_has_text_label_triggered[VICTIM_SHOCKED]
							PLAY_PED_AMBIENT_SPEECH(ped_carjack_victim, "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE)
							b_has_text_label_triggered[VICTIM_SHOCKED] = TRUE
						ENDIF
					ENDIF
				
					IF GET_GAME_TIMER() - i_carjack_timer > 2500				
						i_carjack_event = 99
					ENDIF
				ENDIF
				
				//If the player leaves the area or loses the wanted level while the peds are waiting then just progress.
				BOOL b_cops_have_arrived
				
				IF i_time_exterior_cop_cars_created != 0
				AND GET_GAME_TIMER() - i_time_exterior_cop_cars_created > 6500
					b_cops_have_arrived = TRUE
				ENDIF
				
				IF b_lamar_aiming AND b_stretch_aiming
					IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					OR (b_player_escaped AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-603.3, -1699.8, 23.3>>) > (35.0*35.0))
					OR (b_player_escaped AND b_cops_have_arrived)
						i_carjack_event = 99
					ENDIF
				ENDIF
				
				//Dialogue
				IF b_has_text_label_triggered[LEM1_COPS1]
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT b_has_text_label_triggered[LEM1_JACK] //lamar tells the driver to get out.
						IF NOT IS_PED_INJURED(ped_carjack_victim)
						AND IS_PED_IN_ANY_VEHICLE(ped_carjack_victim)
						AND VDIST2(GET_ENTITY_COORDS(s_lamar.ped), v_victim_pos) < 400.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_JACK", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_JACK] = TRUE
							ENDIF
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_JAC2] //Stretch tells the driver to get out.
						IF NOT IS_PED_INJURED(ped_carjack_victim)
						AND VDIST2(GET_ENTITY_COORDS(s_stretch.ped), v_victim_pos) < 400.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_JAC2", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_JAC2] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If the player gets into a different car just ignore this section.
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					i_carjack_event = 99
				ENDIF
			ELSE
				i_carjack_event = 99
			ENDIF
		BREAK
		
		CASE 99 //Cleanup
			IF e_mission_stage = STAGE_GO_BACK_HOME
				CLEAR_PED_TASKS(s_lamar.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, FALSE)
				IF NOT IS_PED_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
					SET_PED_AS_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
				ENDIF
				
				CLEAR_PED_TASKS(s_stretch.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
				IF NOT IS_PED_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
					SET_PED_AS_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(veh_carjack)
					SET_VEHICLE_LIGHTS(veh_carjack, NO_VEHICLE_LIGHT_OVERRIDE)
				ENDIF
				
				REMOVE_VEHICLE(veh_carjack)
				REMOVE_PED(ped_carjack_victim)
				
				i_carjack_timer = 0
				i_carjack_event = 100
			ENDIF
		BREAK
		
		CASE 100 //Car is ready to use. 
			//If we skipped straight here then remove the vehicle.
			IF NOT IS_ENTITY_DEAD(veh_carjack)
				SET_VEHICLE_LIGHTS(veh_carjack, NO_VEHICLE_LIGHT_OVERRIDE)
				
				REMOVE_VEHICLE(veh_carjack)
				REMOVE_PED(ped_carjack_victim)
			ENDIF
		
			//Play some dialogue if the player isn't getting into the car yet.
			IF NOT b_has_text_label_triggered[LEM1_DRIVE]
				IF b_has_text_label_triggered[LEM1_COPS1]
				AND (IS_BIT_SET(s_locates_data.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE) OR IS_BIT_SET(s_locates_data.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL))
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DRIVE", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_DRIVE] = TRUE
						ENDIF
					ELSE
						b_has_text_label_triggered[LEM1_DRIVE] = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC UPDATE_HOOKER_SEQUENCE()
	//INT i_random
	VECTOR v_car_pos
	SEQUENCE_INDEX seq

	SWITCH i_hooker_event
		CASE 0 //Set up the peds and car
			REQUEST_MODEL(model_escape_car_2)
			REQUEST_MODEL(model_hooker)
			REQUEST_MODEL(model_carjack_victim)
			//REQUEST_ANIM_DICT(str_hooker_anims)
			
			IF HAS_MODEL_LOADED(model_escape_car_2)
			AND HAS_MODEL_LOADED(model_hooker)
			AND HAS_MODEL_LOADED(model_carjack_victim)
			//AND HAS_ANIM_DICT_LOADED(str_hooker_anims)
				veh_hooker_car = CREATE_VEHICLE(model_escape_car_2, <<-634.4861, -1696.8950, 23.4894>>, 228.7563)
				SET_VEHICLE_COLOURS(veh_hooker_car, 4, 4)
				SET_VEHICLE_EXTRA_COLOURS(veh_hooker_car, 111, 156)
				SET_VEHICLE_NUMBER_PLATE_TEXT(veh_hooker_car, "34MTA729")
				SET_VEHICLE_STRONG(veh_hooker_car, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(veh_hooker_car)
				SET_VEHICLE_ENGINE_ON(veh_hooker_car, TRUE, TRUE)
				SET_VEHICLE_LIGHTS(veh_hooker_car, FORCE_VEHICLE_LIGHTS_ON)
				SET_VEH_RADIO_STATION(veh_hooker_car, "RADIO_01_CLASS_ROCK")
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(veh_hooker_car, TRUE)
				
				ped_hooker_driver = CREATE_PED_INSIDE_VEHICLE(veh_hooker_car, PEDTYPE_MISSION, model_carjack_victim)
				ped_hooker = CREATE_PED(PEDTYPE_MISSION, model_hooker, <<-633.0969, -1695.2395, 23.5702>>, 135.7340)
				
				//TASK_PLAY_ANIM(ped_hooker, str_hooker_anims, "hooker_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				TASK_START_SCENARIO_IN_PLACE(ped_hooker, "WORLD_HUMAN_PROSTITUTE_LOW_CLASS")
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_hooker, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ped_hooker, TRUE)
				
				TASK_LOOK_AT_ENTITY(ped_hooker_driver, ped_hooker, -1, SLF_WHILE_NOT_IN_FOV)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_hooker_driver, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ped_hooker_driver, TRUE)
					
				SET_MODEL_AS_NO_LONGER_NEEDED(model_escape_car_2)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_hooker)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_carjack_victim)
				
				i_hooker_event++
			ENDIF
		BREAK
		
		CASE 1 //Update the hooker anims, if anything happens around the car make them flee.
			IF b_player_escaped
				v_car_pos = GET_ENTITY_COORDS(veh_hooker_car, FALSE)
			
				//Update hooker anims
				/*IF NOT IS_PED_INJURED(ped_hooker)
					IF IS_ENTITY_PLAYING_ANIM(ped_hooker, str_hooker_anims, "hooker_b]
						IF GET_ENTITY_ANIM_CURRENT_TIME(ped_hooker, str_hooker_anims, "hooker_b] > 0.98
							i_random = GET_RANDOM_INT_IN_RANGE(0, 2)
							
							OPEN_SEQUENCE_TASK(seq)
								IF i_random = 0
									TASK_PLAY_ANIM(NULL, str_hooker_anims, "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
								ELSE
									TASK_PLAY_ANIM(NULL, str_hooker_anims, "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
								ENDIF
								
								TASK_PLAY_ANIM(NULL, str_hooker_anims, "hooker_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seq)
							
							TASK_PERFORM_SEQUENCE(ped_hooker, seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
					ENDIF
				ENDIF*/
			
				//Flee if the player goes nearby, the player aims or shoots at them, or when the cops arrive right next to them.
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_car_pos) < 225.0
				OR (NOT IS_PED_INJURED(ped_hooker_driver) AND (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped_hooker_driver) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped_hooker_driver)))
				OR (NOT IS_PED_INJURED(ped_hooker) AND (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped_hooker) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped_hooker)))
				OR IS_BULLET_IN_AREA(v_car_pos, 20.0, FALSE)
				OR (NOT IS_ENTITY_DEAD(veh_cops_setpiece_1c) AND VDIST2(GET_ENTITY_COORDS(veh_cops_setpiece_1c), v_car_pos) < 400.0)
					IF NOT IS_PED_INJURED(ped_hooker_driver)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200.0, -1)
						CLOSE_SEQUENCE_TASK(seq)
						
						SET_PED_FLEE_ATTRIBUTES(ped_hooker_driver, FA_DISABLE_HANDS_UP, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped_hooker_driver, FA_USE_VEHICLE, FALSE)
						
						TASK_PERFORM_SEQUENCE(ped_hooker_driver, seq)
						TASK_CLEAR_LOOK_AT(ped_hooker_driver)
						CLEAR_SEQUENCE_TASK(seq)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_hooker_driver, FALSE)
						SET_PED_KEEP_TASK(ped_hooker_driver, TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(ped_hooker)
						//CLEAR_PED_TASKS(ped_hooker)
						TRIGGER_PED_SCENARIO_PANICEXITTOFLEE(ped_hooker, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						//TASK_SMART_FLEE_PED(ped_hooker, PLAYER_PED_ID(), 200.0, -1)
						SET_PED_FLEE_ATTRIBUTES(ped_hooker, FA_DISABLE_HANDS_UP, TRUE)
						SET_PED_FLEE_ATTRIBUTES(ped_hooker, FA_USE_VEHICLE, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_hooker, FALSE)
						SET_PED_KEEP_TASK(ped_hooker, TRUE)
					ENDIF
					
					i_hooker_timer = GET_GAME_TIMER()
					i_hooker_event = 99
				ENDIF
			ENDIF
		BREAK
		
		CASE 99 //Cleanup
			BOOL bCleanup
			bCleanup = FALSE
		
			IF NOT IS_PED_INJURED(ped_hooker_driver)
				IF NOT IS_PED_IN_ANY_VEHICLE(ped_hooker_driver)
					bCleanup = TRUE
				ENDIF
			ELSE
				bCleanup = TRUE
			ENDIF
			
			IF bCleanup
			OR GET_GAME_TIMER() - i_hooker_timer > 20000
				IF NOT IS_PED_INJURED(ped_hooker_driver)
					RESET_PED_LAST_VEHICLE(ped_hooker_driver)
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(veh_hooker_car)
					SET_VEHICLE_LIGHTS(veh_hooker_car, NO_VEHICLE_LIGHT_OVERRIDE)
				ENDIF
			
				REMOVE_PED(ped_hooker)
				REMOVE_PED(ped_hooker_driver)
				REMOVE_VEHICLE(veh_hooker_car)
				REMOVE_ANIM_DICT(str_hooker_anims)
				
				i_hooker_event++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC REMOVE_ALL_BLIPS()	
	IF DOES_BLIP_EXIST(blip_current_destination)
		REMOVE_BLIP(blip_current_destination)
	ENDIF
	
	IF DOES_BLIP_EXIST(s_lamar.blip)
		REMOVE_BLIP(s_lamar.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(s_stretch.blip)
		REMOVE_BLIP(s_stretch.blip)
	ENDIF
	
	INT i = 0
	REPEAT COUNT_OF(s_lost_bikes) i
		IF DOES_BLIP_EXIST(s_lost_bikes[i].blip)
			REMOVE_BLIP(s_lost_bikes[i].blip)
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_OBJECTS(BOOL b_force_delete = FALSE)
	REMOVE_OBJECT(obj_warehouse_gas, b_force_delete)
	REMOVE_OBJECT(obj_breach_door, b_force_delete)
	REMOVE_OBJECT(obj_wooden_box, FALSE)
	REMOVE_OBJECT(obj_franklins_front_door, TRUE)
	
	INT i = 0
	REPEAT COUNT_OF(obj_lights) i
		REMOVE_OBJECT(obj_lights[i], b_force_delete)
	ENDREPEAT
	
	i = 0
	REPEAT COUNT_OF(obj_rope_base) i
		REMOVE_OBJECT(obj_rope_base[i], b_force_delete)
	ENDREPEAT
	
ENDPROC

//Shortcut proc for removing a group of peds defined using the ENEMY_PED struct.
//Also handles cleanup of any elements related to this ped (e.g. cover points, blips, etc)
PROC REMOVE_ENEMY_GROUP(ENEMY_PED &peds[], BOOL b_force_delete = FALSE)
	INT i = 0
	REPEAT COUNT_OF(peds) i
		IF DOES_BLIP_EXIST(peds[i].blip)
			REMOVE_BLIP(peds[i].blip)
		ENDIF
		
		CLEANUP_AI_PED_BLIP(peds[i].s_blip_data)
	
		IF peds[i].s_rope.rope != NULL
			DELETE_ROPE(peds[i].s_rope.rope)
			peds[i].s_rope.rope = NULL
		ENDIF
	
		REMOVE_COVER_POINT(peds[i].cover)
	
		//Give them combat in most cases
		IF NOT IS_PED_INJURED(peds[i].ped)
			STOP_SYNCHRONIZED_ENTITY_ANIM(peds[i].ped, NORMAL_BLEND_OUT, TRUE)
		
			IF NOT IS_PED_IN_ANY_VEHICLE(peds[i].ped)
			AND GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].ped) = rel_group_swat
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					TASK_COMBAT_PED(peds[i].ped, PLAYER_PED_ID())
				ELIF NOT IS_PED_INJURED(s_lamar.ped)
					TASK_COMBAT_PED(peds[i].ped, s_lamar.ped)
				ELIF NOT IS_PED_INJURED(s_stretch.ped)
					TASK_COMBAT_PED(peds[i].ped, s_stretch.ped)
				ENDIF	
				//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[i].ped, TRUE)
				SET_PED_KEEP_TASK(peds[i].ped, TRUE)
			ENDIF
		ENDIF
	
		REMOVE_PED(peds[i].ped, b_force_delete)

		peds[i].b_is_created = FALSE
		peds[i].i_event = 0
		peds[i].i_timer = 0
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_PEDS(BOOL b_force_delete = FALSE)
	IF NOT b_mission_passed //If the player passed the mission Lamar and Stretch are handed over to the family system, so they shouldn't be removed.
		IF NOT (b_lamar_on_the_ground AND NOT b_force_delete) //Don't remove Lamar if the mission failed and he was lying on the ground during the breach.
			REMOVE_PED(s_lamar.ped, b_force_delete)
		ENDIF
		REMOVE_PED(s_stretch.ped, b_force_delete)
	ENDIF
	
	REMOVE_PED(ped_carjack_victim, b_force_delete)
	REMOVE_PED(ped_hooker, b_force_delete)
	REMOVE_PED(ped_hooker_driver, b_force_delete)
	REMOVE_PED(ped_ballas_og, b_force_delete)
	//REMOVE_PED(ped_shop_owner, FALSE)
	
	REMOVE_PED(g_sTriggerSceneAssets.ped[DENISE_ID], TRUE)
	REMOVE_PED(g_sTriggerSceneAssets.ped[MAGENTA_ID], TRUE)
	REMOVE_PED(g_sTriggerSceneAssets.ped[FRIEND_ID_1], TRUE)
	REMOVE_PED(g_sTriggerSceneAssets.ped[FRIEND_ID_2], TRUE)
	
	REMOVE_ENEMY_GROUP(s_swat_indoor_lift, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_breach, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_window_1, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_heli, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_pre_warehouse, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_roof_1, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_breach_2, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_window_2, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_warehouse, b_force_delete)
	REMOVE_ENEMY_GROUP(s_swat_indoor_chopper_final, b_force_delete)
	
	REMOVE_ENEMY_GROUP(s_cops_chopper_gantry, b_force_delete)
	REMOVE_ENEMY_GROUP(s_cops_setpiece_1, b_force_delete)
	REMOVE_ENEMY_GROUP(s_cops_setpiece_1b, b_force_delete)
	REMOVE_ENEMY_GROUP(s_cops_setpiece_1c, b_force_delete)
ENDPROC

PROC REMOVE_ALL_VEHICLES(BOOL b_force_delete = FALSE)
	REMOVE_VEHICLE(veh_start_car, b_force_delete)
	REMOVE_VEHICLE(veh_chopper_indoor_final, b_force_delete)
	REMOVE_VEHICLE(veh_hooker_car, b_force_delete)
	REMOVE_VEHICLE(veh_cops_setpiece_1, b_force_delete)
	REMOVE_VEHICLE(veh_cops_setpiece_1b, b_force_delete)
	REMOVE_VEHICLE(veh_cops_setpiece_1c, b_force_delete)
	REMOVE_VEHICLE(veh_cops_setpiece_3, b_force_delete)
	REMOVE_VEHICLE(veh_chopper_gantry, b_force_delete)
	REMOVE_VEHICLE(veh_final_cutscene_car, b_force_delete)
	REMOVE_VEHICLE(veh_carjack, b_force_delete)
	IF b_force_delete
		REMOVE_VEHICLE(veh_lamar_stretch_exit, b_force_delete)
	ENDIF
	REMOVE_VEHICLE(veh_weapons_shop_car, b_force_delete)
	
	INT i = 0
	REPEAT COUNT_OF(veh_cops_setpiece_2) i
		REMOVE_VEHICLE(veh_cops_setpiece_2[i], TRUE)
	ENDREPEAT
	
	REPEAT COUNT_OF(veh_cops_setpiece_4) i
		REMOVE_VEHICLE(veh_cops_setpiece_4[i], TRUE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_lost_bikes) i
		REMOVE_VEHICLE(s_lost_bikes[i].veh, b_force_delete)
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_CAMERAS()
	DESTROY_ALL_CAMS()

	DISPLAY_RADAR(TRUE) 
	DISPLAY_HUD(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC


PROC REMOVE_ALL_SHOOTOUT_SYNCED_SCENE_ANIMS()
	IF NOT IS_PED_INJURED(s_lamar.ped)
		STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
	ENDIF

	IF NOT IS_PED_INJURED(s_stretch.ped)
		STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
	ENDIF

	REMOVE_ANIM_DICT("misslamar1lam_1_ig_19")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_18")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_17")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_16")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_15")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_14")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_13")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_12")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_11")
	REMOVE_ANIM_DICT("misslamar1ig_9")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_23")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_23_b")
	REMOVE_ANIM_DICT("misslamar1ig_6")
	REMOVE_ANIM_DICT("misslamar1ig_5")
	REMOVE_ANIM_DICT("misslamar1ig_4")
	REMOVE_ANIM_DICT("misslamar1ig_2_p1")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_26_alt1")
	REMOVE_ANIM_DICT("misslamar1lam_1_ig_26")
	REMOVE_ANIM_DICT("misslamar1ig_20")
	
	REMOVE_CLIP_SET("move_ped_strafing")
ENDPROC

PROC CANCEL_ALL_PREPARED_MUSIC_EVENTS()
	CANCEL_MUSIC_EVENT("LM1_TERMINADOR_DOORS_OPEN")
	CANCEL_MUSIC_EVENT("LM1_TERMINADOR_2ND_DOOR_EXPLODES")
	CANCEL_MUSIC_EVENT("LM1_TERMINADOR_EXIT_WAREHOUSE")
	CANCEL_MUSIC_EVENT("LM1_TERMINADOR_HEAD_SHOT")
ENDPROC

PROC REMOVE_ALL_AUDIO_SCENES()
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_AMMUNATION")
		STOP_AUDIO_SCENE("LAMAR_1_GO_TO_AMMUNATION")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_AMMUNATION_INTRO_CUTSCENE")
		STOP_AUDIO_SCENE("LAMAR_1_AMMUNATION_INTRO_CUTSCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_RECYCLING_CENTRE")
		STOP_AUDIO_SCENE("LAMAR_1_GO_TO_RECYCLING_CENTRE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_MEETING")
		STOP_AUDIO_SCENE("LAMAR_1_GO_TO_MEETING")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_START")
		STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_START")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_COVER_LAMAR")
		STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_COVER_LAMAR")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_SAVE_LAMAR")
		STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_SAVE_LAMAR")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_STAIRWELL")
		STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_STAIRWELL")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_WAREHOUSE")
		STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_WAREHOUSE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_GET_OUTSIDE")
		STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_GET_OUTSIDE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_ESCAPE_FIGHT_HELI")
		STOP_AUDIO_SCENE("LAMAR_1_ESCAPE_FIGHT_HELI")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_ESCAPE_THE_JUNKYARD")
		STOP_AUDIO_SCENE("LAMAR_1_ESCAPE_THE_JUNKYARD")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_STEAL_CAR")
		STOP_AUDIO_SCENE("LAMAR_1_STEAL_CAR")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_LOSE_COPS")
		STOP_AUDIO_SCENE("LAMAR_1_LOSE_COPS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_DRIVE_HOME")
		STOP_AUDIO_SCENE("LAMAR_1_DRIVE_HOME")
	ENDIF
ENDPROC

PROC SET_CUTSCENE_PED_COMPONENT_VARIATIONS(STRING strCutsceneName)
	INT iNameHash = GET_HASH_KEY(strCutsceneName)

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF iNameHash = HASH("lamar_1_int")
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise", PED_COMP_HEAD, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise", PED_COMP_TORSO, 0, 1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise", PED_COMP_DECL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise", PED_COMP_LEG, 0, 0)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend", PED_COMP_HEAD, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend", PED_COMP_TORSO, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend", PED_COMP_LEG, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend", PED_COMP_HAIR, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend", PED_COMP_HAND, 0, 0)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend^1", PED_COMP_HEAD, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend^1", PED_COMP_TORSO, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend^1", PED_COMP_LEG, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend^1", PED_COMP_HAIR, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Denise_Friend^1", PED_COMP_HAND, 0, 1)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_HAIR, 2, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_TORSO, 2, 2)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_LEG, 5, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_HAND, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_SPECIAL, 0, 0)
			//SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_BERD, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_BERD, 0, 0)
			
			SET_CUTSCENE_PED_PROP_VARIATION("Stretch", ANCHOR_HEAD, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Stretch", PED_COMP_SPECIAL, 1, 0)
		ELIF iNameHash = HASH("LAM_1_MCS_1_CONCAT")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(s_lamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lamar", s_lamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_BERD, 0, 0)
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Stretch", s_stretch.ped)
				SET_CUTSCENE_PED_PROP_VARIATION("Stretch", ANCHOR_HEAD, 0, 0)
			ENDIF
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Ballas_OG", PED_COMP_TORSO, 0, 1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Ballas_OG", PED_COMP_LEG, 0, 1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Ballas_OG", PED_COMP_DECL, 0, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("Ballas_OG", ANCHOR_HEAD, 1, 0)
		ELIF iNameHash = HASH("lam_1_mcs_2")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_INJURED(s_stretch.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lamar", s_lamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_BERD, 0, 0)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Stretch", s_stretch.ped)
				SET_CUTSCENE_PED_PROP_VARIATION("Stretch", ANCHOR_HEAD, 0, 0)
			ENDIF
		ELIF iNameHash = HASH("lam_1_mcs_3")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(s_lamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lamar", s_lamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_BERD, 0, 0)
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Stretch", s_stretch.ped)
				SET_CUTSCENE_PED_PROP_VARIATION("Stretch", ANCHOR_HEAD, 0, 0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SET_WAREHOUSE_LIGHTS_VISIBLE(BOOL bIsVisible)
	IF bIsVisible
		/*OBJECT_INDEX obj_map_lights[4]
		INT i = 0
		BOOL b_found_map_lights = FALSE
		
		obj_map_lights[0] = GET_CLOSEST_OBJECT_OF_TYPE(<<-572.119141,-1609.381836,30.294912>>, 10.0, model_light_to_remove, FALSE)
		obj_map_lights[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<-579.196167,-1603.944946,30.294912>>, 10.0, model_light_to_remove, FALSE)
		obj_map_lights[2] = GET_CLOSEST_OBJECT_OF_TYPE(<<-594.831909,-1607.377808,30.294914>>, 10.0, model_light_to_remove, FALSE)
		obj_map_lights[3] = GET_CLOSEST_OBJECT_OF_TYPE(<<-602.400146,-1609.924438,29.732662>>, 10.0, model_light_to_remove, FALSE)
		
		REPEAT COUNT_OF(obj_map_lights) i
			IF DOES_ENTITY_EXIST(obj_map_lights[0])
				IF NOT IS_ENTITY_A_MISSION_ENTITY(obj_map_lights[i])
					FREEZE_ENTITY_POSITION(obj_map_lights[i], TRUE)
					SET_ENTITY_COLLISION(obj_map_lights[i], TRUE)
				ENDIF
				
				b_found_map_lights = TRUE
			ENDIF
		ENDREPEAT
	
		RETURN b_found_map_lights*/
	
		REMOVE_MODEL_HIDE(<<-572.119141,-1609.381836,30.294912>>, 10.0, model_light_to_remove, TRUE)
		REMOVE_MODEL_HIDE(<<-579.196167,-1603.944946,30.294912>>, 10.0, model_light_to_remove, TRUE)
		REMOVE_MODEL_HIDE(<<-594.831909,-1607.377808,30.294914>>, 10.0, model_light_to_remove, TRUE)
		REMOVE_MODEL_HIDE(<<-602.400146,-1609.924438,29.732662>>, 10.0, model_light_to_remove, TRUE)
		
		RETURN TRUE
	ELSE
		CREATE_MODEL_HIDE(<<-572.119141,-1609.381836,30.294912>>, 10.0, model_light_to_remove, TRUE)
		CREATE_MODEL_HIDE(<<-579.196167,-1603.944946,30.294912>>, 10.0, model_light_to_remove, TRUE)
		CREATE_MODEL_HIDE(<<-594.831909,-1607.377808,30.294914>>, 10.0, model_light_to_remove, TRUE)
		CREATE_MODEL_HIDE(<<-602.400146,-1609.924438,29.732662>>, 10.0, model_light_to_remove, TRUE)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_WAREHOUSE_LIGHTS()
	INT i = 0

	VECTOR v_rope_pos[4]
	v_rope_pos[0] = <<-572.119141,-1609.381836,32.894912>>
	v_rope_pos[1] = <<-579.196167,-1603.944946,32.894912>>
	v_rope_pos[2] = <<-594.831909,-1607.377808,32.894914>>
	v_rope_pos[3] = <<-602.400146,-1609.924438,32.332662>>
	
	IF NOT ROPE_ARE_TEXTURES_LOADED()
		ROPE_LOAD_TEXTURES()
	ELIF NOT DOES_ENTITY_EXIST(obj_lights[0])
		REQUEST_MODEL(model_light)
	
		IF HAS_MODEL_LOADED(model_light)
		AND SET_WAREHOUSE_LIGHTS_VISIBLE(FALSE)
		
			obj_lights[0] = CREATE_OBJECT_NO_OFFSET(model_light, <<-572.119141,-1609.381836,30.294912>>)
			obj_lights[1] = CREATE_OBJECT_NO_OFFSET(model_light, <<-579.196167,-1603.944946,30.294912>>)
			obj_lights[2] = CREATE_OBJECT_NO_OFFSET(model_light, <<-594.831909,-1607.377808,30.294914>>)
			obj_lights[3] = CREATE_OBJECT_NO_OFFSET(model_light, <<-602.400146,-1609.924438,29.732662>>)
			
			obj_rope_base[0] = CREATE_OBJECT(prop_ld_test_01, v_rope_pos[0])
			obj_rope_base[1] = CREATE_OBJECT(prop_ld_test_01, v_rope_pos[1])
			obj_rope_base[2] = CREATE_OBJECT(prop_ld_test_01, v_rope_pos[2])
			obj_rope_base[3] = CREATE_OBJECT(prop_ld_test_01, v_rope_pos[3])
			
			REPEAT COUNT_OF(obj_lights) i
				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(obj_lights[i], TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(obj_lights[i], FALSE)
				SET_ENTITY_DYNAMIC(obj_lights[i], TRUE)
				ACTIVATE_PHYSICS(obj_lights[i])
				FREEZE_ENTITY_POSITION(obj_lights[i], TRUE)
				
				FREEZE_ENTITY_POSITION(obj_rope_base[i], TRUE)
				
				b_light_ropes_are_attached[i] = FALSE
			ENDREPEAT
			
			SET_MODEL_AS_NO_LONGER_NEEDED(model_light)
		ENDIF
	ELIF NOT DOES_ROPE_EXIST(rope_lights[0])
		REPEAT COUNT_OF(rope_lights) i
			rope_lights[i] = ADD_ROPE(v_rope_pos[i], <<0.0, 90.0, 0.0>>, f_rope_length, PHYSICS_ROPE_DEFAULT_WIRE, f_rope_length, f_rope_length)
			PIN_ROPE_VERTEX(rope_lights[i], GET_ROPE_VERTEX_COUNT(rope_lights[i]) - 1, v_rope_pos[i])
			
			b_light_ropes_are_attached[i] = FALSE
		ENDREPEAT
	ELSE
		INT i_num_ropes_not_attached = 0
	
		REPEAT COUNT_OF(obj_lights) i
			IF NOT b_light_ropes_are_attached[i]
				IF DOES_ENTITY_HAVE_PHYSICS(obj_lights[i])
				AND DOES_ENTITY_HAVE_PHYSICS(obj_rope_base[i])
				
					SET_ENTITY_COORDS_NO_OFFSET(obj_lights[i], v_rope_pos[i] + <<0.0, 0.0, -f_rope_length>>)
					FREEZE_ENTITY_POSITION(obj_lights[i], FALSE)
					
					ATTACH_ENTITIES_TO_ROPE(rope_lights[i], obj_rope_base[i], obj_lights[i],
									 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_rope_base[i],<<0, 0, 0>>)
									,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(obj_lights[i], <<0.0, 0.0, -0.1>>), f_rope_length, 0, 0 )
					
					b_light_ropes_are_attached[i] = TRUE
				ENDIF
				
				i_num_ropes_not_attached++
			ENDIF
		ENDREPEAT
		
		IF i_num_ropes_not_attached = 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MISSION_SETUP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	REQUEST_ADDITIONAL_TEXT("LEM1", MISSION_TEXT_SLOT)

	//group_player = GET_PLAYER_GROUP(PLAYER_ID())

	ADD_RELATIONSHIP_GROUP("SWAT", rel_group_swat)
	ADD_RELATIONSHIP_GROUP("DRIVERS", rel_group_neutral)
	ADD_RELATIONSHIP_GROUP("BIKERS", rel_group_bikers)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, rel_group_swat)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rel_group_swat, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_group_swat, rel_group_swat)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_group_swat, rel_group_neutral)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rel_group_swat, rel_group_bikers)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_group_neutral, rel_group_swat)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_group_neutral, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_group_neutral, rel_group_neutral)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rel_group_bikers, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rel_group_bikers, rel_group_swat)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_group_bikers, rel_group_bikers)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(model_lamars_car, TRUE)
	
	SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, FALSE)
	
	SET_ROADS_IN_ANGLED_AREA(<<-73.974548,-1548.606445,23.770067>>, <<7.875402,-1450.703613,39.286743>>, 126.750000, FALSE, FALSE)
	SET_ROADS_IN_AREA(<<-672.15, -1670.86, 18.61>>, <<-410.67, -1748.43, 26.83>>, FALSE) 
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<17.2, -1115.4, 20.0>>, <<50.0, -1083.3, 40.0>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-630.0, -1617.0, 20.0>>, <<-600.0, -1590.0, 40.0>>, FALSE)
	

	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 0, PLAYER_PED_ID(), "FRANKLIN")

	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_SAVEHOUSE_CAR, FALSE)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_SAVEHOUSE_BIKE, TRUE)
	
	SET_SHOP_HAS_RUN_ENTRY_INTRO(GUN_SHOP_01_DT, TRUE)

	INT i = 0
	REPEAT COUNT_OF(i_rappel_sound_ids) i
		i_rappel_sound_ids[i] = GET_SOUND_ID()
	ENDREPEAT

	i_fire_sound = GET_SOUND_ID()
	g_eLam1PrestreamLamStretch = ARM1_PD_3_release
	
	b_mission_passed = FALSE
	b_gun_shops_were_already_open = IS_SHOP_AVAILABLE(GUN_SHOP_02_SS)
	i_armour_on_mission_start = GET_PED_ARMOUR(PLAYER_PED_ID())
	
	sbi_recycling_plant[0] = ADD_SCENARIO_BLOCKING_AREA(<<-572.7031, -1672.0083, 0.0>>, <<-509.0245, -1618.3895, 34.4510>>)
	sbi_recycling_plant[1] = ADD_SCENARIO_BLOCKING_AREA(<<-563.9064, -1761.4708, 0.0>>, <<-384.2186, -1635.0127, 34.4510>>)
	sbi_recycling_plant[2] = ADD_SCENARIO_BLOCKING_AREA(<<-697.5707, -1686.8453, 0.0951>>, <<-584.6719, -1596.8103, 60.0951>>)
	sbi_weapons_shop = ADD_SCENARIO_BLOCKING_AREA(<<4.3425, -1128.4681, 0.0951>>, <<38.8711, -1098.1051, 60.0951>>)
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
		
		s_skip_menu[0].sTxtLabel = "OPENING_CUTSCENE" 
		s_skip_menu[1].sTxtLabel = "GO_TO_GUN_SHOP"       
		s_skip_menu[2].sTxtLabel = "SHOP_INTRO_CUTSCENE (LAM_1_MCS_3)"   
		s_skip_menu[3].sTxtLabel = "BUY_GRENADES" 
		s_skip_menu[4].sTxtLabel = "GO_TO_PLANT"
		s_skip_menu[5].sTxtLabel = "MEETING_CUTSCENE (LAM_1_MCS_1_concat)"
		s_skip_menu[6].sTxtLabel = "ESCAPE_PRE_WAREHOUSE"  
		s_skip_menu[7].sTxtLabel = "ESCAPE_FROM_WAREHOUSE" 
		s_skip_menu[8].sTxtLabel = "GO_BACK_HOME"   
		s_skip_menu[9].sTxtLabel = "DROPOFF_CUTSCENE (LAM_1_MCS_2)" 
	#ENDIF
ENDPROC

PROC MISSION_CLEANUP()
	CLEAR_HELP()
	CLEAR_ALL_FLOATING_HELP()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	DISABLE_CELLPHONE(FALSE)
	CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_INVALID)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_POLICE_RADAR_BLIPS(TRUE)
	SET_FAKE_WANTED_LEVEL(0)
	PAUSE_CLOCK(FALSE)
	NEW_LOAD_SCENE_STOP()
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
    DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
	
	IF b_buddies_set_to_attract_cops
		//SET_RELATIONSHIP_GROUP_AFFECTS_WANTED_LEVEL(RELGROUPHASH_PLAYER, FALSE)
		b_buddies_set_to_attract_cops = FALSE
	ENDIF

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
				DETACH_ENTITY(PLAYER_PED_ID())
			ENDIF
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
		SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, FALSE)
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_NONE)
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
		SET_PLAYER_SPRINT(PLAYER_ID(), TRUE)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
		SET_POLICE_RADAR_BLIPS(TRUE)
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
		
		IF b_is_jumping_directly_to_stage
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
	ELSE
		IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
		OR e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
			PED_INDEX peds[5]
			BOOL bEnemiesNearby = FALSE
		
			IF NOT IS_PED_INJURED(s_lamar.ped)
				GET_PED_NEARBY_PEDS(s_lamar.ped, peds)
			ENDIF
			
			INT i = 0
			REPEAT COUNT_OF(peds) i
				IF NOT IS_PED_INJURED(peds[i])
					IF peds[i] != s_lamar.ped
					AND peds[i] != s_stretch.ped
					AND GET_PED_RELATIONSHIP_GROUP_HASH(peds[i]) = rel_group_swat
						bEnemiesNearby = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		
			//Have the buddies flee if the player dies and they were just standing around in the shootout.
			IF NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_IN_COMBAT(s_lamar.ped)
				IF NOT b_lamar_on_the_ground
					STOP_SYNCHRONIZED_ENTITY_ANIM(s_lamar.ped, NORMAL_BLEND_OUT, TRUE)
					IF bEnemiesNearby
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_lamar.ped, 100.0)
					ELSE
						TASK_SMART_FLEE_COORD(s_lamar.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
					ENDIF
					SET_PED_KEEP_TASK(s_lamar.ped, TRUE)
				ELSE	
					SET_RAGDOLL_BLOCKING_FLAGS(s_lamar.ped, RBF_NONE)
					SET_PED_TO_RAGDOLL(s_lamar.ped, 1000, 2000, TASK_NM_BALANCE)
					APPLY_DAMAGE_TO_PED(s_lamar.ped, GET_ENTITY_HEALTH(s_lamar.ped) + 50, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
			AND NOT IS_PED_IN_COMBAT(s_stretch.ped)
				FREEZE_ENTITY_POSITION(s_stretch.ped, FALSE)
				STOP_SYNCHRONIZED_ENTITY_ANIM(s_stretch.ped, NORMAL_BLEND_OUT, TRUE)
				IF bEnemiesNearby
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_stretch.ped, 100.0)
				ELSE
					TASK_SMART_FLEE_COORD(s_stretch.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
				ENDIF
				SET_PED_KEEP_TASK(s_stretch.ped, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_PICKUP_EXIST(piWeaponPickup)
		REMOVE_PICKUP(piWeaponPickup)
	ENDIF
	
	REMOVE_COVER_POINT(cover_player)
	REMOVE_COVER_POINT(s_lamar.cover)
	REMOVE_COVER_POINT(cover_locker)
	
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Lemar1")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Lemar2")
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(model_escape_car, FALSE)
	//RELEASE_MISSION_AUDIO_BANK()
	
	SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_01_DT, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, FALSE)
	b_blocked_shop_dialogue = FALSE
	
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 5)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 6)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 7)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_1)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_2)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_3)
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, FALSE)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, FALSE)
	
	IF b_locker_doors_opened
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_rc_door1, <<-603.8043, -1620.3005, 33.1606>>, FALSE)
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_rc_door1, <<-606.3953, -1620.0739, 33.1606>>, FALSE)
	ENDIF
	
	IF interior_recycle != NULL
		IF IS_INTERIOR_READY(interior_recycle)
			UNPIN_INTERIOR(interior_recycle)
			interior_recycle = NULL
		ENDIF
	ENDIF
	
	IF interior_gun_shop != NULL
		IF IS_INTERIOR_READY(interior_gun_shop)
			UNPIN_INTERIOR(interior_gun_shop)
			interior_gun_shop = NULL
		ENDIF
	ENDIF
	
	IF interior_franklins_house != NULL
		IF IS_INTERIOR_READY(interior_franklins_house)
			UNPIN_INTERIOR(interior_franklins_house)
			interior_franklins_house = NULL
		ENDIF
	ENDIF

	INT i = 0
	REPEAT COUNT_OF(i_rappel_sound_ids) i
		IF NOT HAS_SOUND_FINISHED(i_rappel_sound_ids[i])
			STOP_SOUND(i_rappel_sound_ids[i])
		ENDIF
	ENDREPEAT

	IF b_rappel_audio_scene_active
		STOP_AUDIO_SCENE("LAMAR_NPC_RAPPELLING")
		b_rappel_audio_scene_active = FALSE
	ENDIF
	
	IF NOT HAS_SOUND_FINISHED(i_fire_sound)
		STOP_SOUND(i_fire_sound)
	ENDIF

	RESET_MISSION_STATS_ENTITY_WATCH()

	//CANCEL_MUSIC_EVENT

	TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_MISSION_FAIL")
	RELEASE_SCRIPT_AUDIO_BANK()
	
	SET_AUDIO_FLAG("SpeechDucksScore", FALSE)
	SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", FALSE)

	IF NOT HAS_CUTSCENE_FINISHED()
		STOP_CUTSCENE()
	ENDIF
 	
	REMOVE_CUTSCENE()
	
	IF b_is_jumping_directly_to_stage
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	//If we started a replay recording then make sure it's cancelled if we're performing cleanup.
	IF b_replay_event_started
		REPLAY_STOP_EVENT()
		b_replay_event_started = FALSE
	ENDIF

	REMOVE_ALL_BLIPS()
	REMOVE_ALL_CAMERAS()
	REMOVE_ALL_OBJECTS(b_is_jumping_directly_to_stage)
	REMOVE_ALL_PEDS(b_is_jumping_directly_to_stage)
	REMOVE_ALL_VEHICLES(b_is_jumping_directly_to_stage)

	SET_WAREHOUSE_LIGHTS_VISIBLE(TRUE)

	REMOVE_ALL_SHOOTOUT_SYNCED_SCENE_ANIMS()
	
	ROPE_UNLOAD_TEXTURES()
	
	REMOVE_VEHICLE_ASSET(model_escape_car)
	REMOVE_VEHICLE_ASSET(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR))
	
	DISTANT_COP_CAR_SIRENS(FALSE)
	
	//Taxi
	DISABLE_TAXI_HAILING(FALSE)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
		IF DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor) != DOORSTATE_UNLOCKED
			DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ENDIF
		
		REMOVE_DOOR_FROM_SYSTEM(iFrontDoor)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objFrontDoor)
		SET_ENTITY_VISIBLE(objFrontDoor, TRUE)
		
		SET_OBJECT_AS_NO_LONGER_NEEDED(objFrontDoor)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iWarehouseDoorLeft)
		REMOVE_DOOR_FROM_SYSTEM(iWarehouseDoorLeft)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iWarehouseDoorRight)
		REMOVE_DOOR_FROM_SYSTEM(iWarehouseDoorRight)
	ENDIF
	
	REMOVE_MODEL_HIDE(<<-614.7736, -1633.7233, 32.4919>>, 2.0, PROP_CRATE_05A, TRUE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

	SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(FALSE)

	REPEAT COUNT_OF(rope_lights) i
		IF rope_lights[i] != NULL
			DELETE_ROPE(rope_lights[i])
			rope_lights[i] = NULL
		ENDIF
	ENDREPEAT	

	IF NOT b_is_jumping_directly_to_stage
		//Clean up anything that was created in the initial setup (this stuff must not be cleaned up if p-skipping)
		#IF IS_DEBUG_BUILD
			DESTROY_WIDGETS()
		#ENDIF
		
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_RECYCLE, TRUE)

		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)

		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_SAVEHOUSE_CAR, TRUE)
		KILL_FACE_TO_FACE_CONVERSATION()
		
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-73.974548,-1548.606445,23.770067>>, <<7.875402,-1450.703613,39.286743>>, 126.750000)
		SET_ROADS_BACK_TO_ORIGINAL(<<-672.15, -1670.86, 18.61>>, <<-410.67, -1748.43, 26.83>>) 
		SET_VEHICLE_MODEL_IS_SUPPRESSED(model_lamars_car, FALSE)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<17.2, -1115.4, 20.0>>, <<50.0, -1083.3, 40.0>>, TRUE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-630.0, -1617.0, 20.0>>, <<-600.0, -1590.0, 40.0>>, TRUE)

		REMOVE_SCENARIO_BLOCKING_AREA(sbi_recycling_plant[0])
		REMOVE_SCENARIO_BLOCKING_AREA(sbi_recycling_plant[1])
		REMOVE_SCENARIO_BLOCKING_AREA(sbi_recycling_plant[2])
		REMOVE_SCENARIO_BLOCKING_AREA(sbi_weapons_shop)

		RELEASE_SOUND_ID(i_fire_sound)
		
		// B*1543767 - Ensure Chop is present at Franklin's aunt's house
		REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")
		TERMINATE_THIS_THREAD()
	ELSE
		CLEAR_TRIGGERED_LABELS()

		CLEAR_PRINTS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		RESET_MISSION_STATS_ENTITY_WATCH()
		b_mission_failed = FALSE
		
		g_eLam1PrestreamLamStretch = ARM1_PD_3_release
		
		REPEAT COUNT_OF(s_warehouse_fire) i
			IF s_warehouse_fire[i].ptfx != NULL
				STOP_PARTICLE_FX_LOOPED(s_warehouse_fire[i].ptfx)
				s_warehouse_fire[i].ptfx = NULL
			ENDIF
			
			s_warehouse_fire[i].f_scale = 0.0
		ENDREPEAT
		
		REPEAT COUNT_OF(s_warehouse_fire_2) i
			IF s_warehouse_fire_2[i].ptfx != NULL
				STOP_PARTICLE_FX_LOOPED(s_warehouse_fire_2[i].ptfx)
				s_warehouse_fire_2[i].ptfx = NULL
			ENDIF
			
			s_warehouse_fire_2[i].f_scale = 0.0
		ENDREPEAT
		
		REPEAT COUNT_OF(ptfxDebris) i
			IF ptfxDebris[i] != NULL
				STOP_PARTICLE_FX_LOOPED(ptfxDebris[i])
				ptfxDebris[i] = NULL
			ENDIF
		ENDREPEAT
		
		IF decal_blood != NULL
			REMOVE_DECAL(decal_blood)
			decal_blood = NULL
		ENDIF
		
		FORCE_SHOP_RESET(GUN_SHOP_01_DT)
		
		SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
		SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
		SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
		SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
		
		CANCEL_ALL_PREPARED_MUSIC_EVENTS()
		STOP_AUDIO_SCENES()
		
		UNLOAD_SHOP_INTRO_ASSETS(GUN_SHOP_01_DT)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(model_swat)				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_gang)				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_gang_alternate)				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_chopper)			
		SET_MODEL_AS_NO_LONGER_NEEDED(model_escape_car)			
		SET_MODEL_AS_NO_LONGER_NEEDED(model_escape_car_2)				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_cop) 					
		SET_MODEL_AS_NO_LONGER_NEEDED(model_cop_car) 				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_carjack_victim)		
		SET_MODEL_AS_NO_LONGER_NEEDED(model_hooker)				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_gas_can) 				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_ballas_og)				
		SET_MODEL_AS_NO_LONGER_NEEDED(model_lamars_car)	
		SET_MODEL_AS_NO_LONGER_NEEDED(model_franklins_front_door)
		
		REMOVE_ANIM_DICT(str_shop_anims)
		REMOVE_ANIM_DICT(str_swat_anims)
		REMOVE_ANIM_DICT(str_hooker_anims)
		REMOVE_ANIM_DICT(str_balla_dead_anim)
		REMOVE_ANIM_DICT(str_timelapse_anims)
		
		REMOVE_WAYPOINT_RECORDING(str_waypoint_carjack)
		REMOVE_WAYPOINT_RECORDING(str_waypoint_interior)
		REMOVE_WAYPOINT_RECORDING(str_waypoint_stairs)
		
		REMOVE_PTFX_ASSET()
		
		IF DOES_PICKUP_EXIST(pickup_health[0])
			REMOVE_PICKUP(pickup_health[0])
		ENDIF
		
		IF DOES_PICKUP_EXIST(pickup_health[1])
			REMOVE_PICKUP(pickup_health[1])
		ENDIF
		
		CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000.0, TRUE, TRUE)
		REMOVE_DECALS_IN_RANGE(<<0.0, 0.0, 0.0>>, 10000.0)
	ENDIF
ENDPROC

PROC MISSION_PASSED(BOOL bIsDebugPass = FALSE)
	IF bIsDebugPass
		IF NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE()
		ENDIF
	 	
		REMOVE_CUTSCENE()

		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
		
		DO_FADE_IN_WITH_WAIT()
	ENDIF

	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_ACTIVE, FALSE) //Haircut has been changed and checked, don't do it in future missions.

	//Set_Current_Event_For_FamilyMember(FM_FRANKLIN_LAMAR, FAMILY_MEMBER_BUSY)
	//Set_Current_Event_For_FamilyMember(FM_FRANKLIN_STRETCH, FAMILY_MEMBER_BUSY)

	Mission_Flow_Mission_Passed()
	
	b_mission_passed = TRUE
	
	MISSION_CLEANUP()
ENDPROC


PROC MISSION_FAILED(FAILED_REASON reason)
	IF NOT b_mission_failed
		CLEAR_PRINTS()
		CLEAR_HELP()
		REMOVE_ALL_BLIPS()
		CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
		KILL_ANY_CONVERSATION()

		//STORE_FAIL_WEAPON(PLAYER_PED_ID(), ENUM_TO_INT(CHAR_FRANKLIN))

		//If this was a debug fail stop any cutscenes that could be running.
		IF reason = FAILED_GENERIC
			IF NOT HAS_CUTSCENE_FINISHED()
				STOP_CUTSCENE()
			ENDIF
		 	
			REMOVE_CUTSCENE()

			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
		ENDIF

		SWITCH reason
			CASE FAILED_GENERIC
				str_fail_label = ""
			BREAK
			
			CASE FAILED_LAMAR_DIED
				str_fail_label = "LEM1_LEMDEAD"
			BREAK
			
			CASE FAILED_STRETCH_DIED
				str_fail_label = "LEM1_STRETDEAD"
			BREAK
			
			CASE FAILED_LEFT_LAMAR
				str_fail_label = "LEM1_ABLAMAR"
			BREAK
			
			CASE FAILED_LEFT_STRETCH
				str_fail_label = "LEM1_ABSTRETCH"
			BREAK
			
			CASE FAILED_LEFT_LAMAR_AND_STRETCH
				str_fail_label = "LEM1_ABBOTH"
			BREAK
			
			CASE FAILED_SHOP_CLOSED
				str_fail_label = "LEM1_FAILSHOP"
			BREAK
			
			CASE FAILED_FRANKLINS_CAR_DESTROYED
				str_fail_label = "CMN_GENDEST"
			BREAK
			
			CASE FAILED_FRANKLINS_CAR_STUCK
				str_fail_label = "LEM1_FRASTUCK"
			BREAK
			
			CASE FAILED_LAMAR_AND_STRETCH_DIED
				str_fail_label = "LEM1_BOTHDEAD"
			BREAK
			
			CASE FAILED_MISSED_DEAL
				str_fail_label = "LEM1_MISSDEAL"
			BREAK
			
			CASE FAILED_ABANDONED_CAR
				str_fail_label = "LEM1_ABANCAR"
			BREAK
			
			CASE FAILED_DIDNT_BUY_SHOTGUN
				str_fail_label = "LEM1_NOMON"
			BREAK
		ENDSWITCH
		
		b_mission_failed = TRUE
	ENDIF

	TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_MISSION_FAIL")
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(str_fail_label)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
		OR e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
			MANAGE_INTERIOR_ENEMIES_GANG_VERSION(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	//Deal with group members and same vehicle cleanup.
	IF NOT IS_ENTITY_DEAD(s_lamar.ped)
		IF IS_PED_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
		OR ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
			REMOVE_PED(s_lamar.ped, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(s_stretch.ped)
		IF IS_PED_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
		OR ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
			REMOVE_PED(s_stretch.ped, TRUE)
		ENDIF
	ENDIF
	
	//Warp the player if they fail inside the plant.
	IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE 
	OR e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
	OR (e_mission_stage = STAGE_GO_TO_PLANT AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<-612.8, -1619.0, 28.6>>) < 400.0)
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-664.9598, -1645.1869, 23.9818>>, 283.7155)
	ELIF e_mission_stage = STAGE_BUY_GRENADES
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<15.9802, -1119.6460, 27.8296>>, 192.4578)
	ENDIF
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
ENDPROC

PROC JUMP_TO_STAGE(MISSION_STAGE stage, BOOL bIsDebugJumping = FALSE)
	DO_FADE_OUT_WITH_WAIT()
	b_is_jumping_directly_to_stage = TRUE
	i_current_event = 0
	e_mission_stage = stage 
	e_section_stage = SECTION_STAGE_SETUP
	MISSION_CLEANUP()
	
	//Update mission checkpoint in case they skipped the stages where it gets set.
	IF bIsDebugJumping
		IF e_mission_stage >= STAGE_GO_BACK_HOME
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_LOSE_COPS, "LOSE_COPS", TRUE)
		ELIF e_mission_stage >= STAGE_ESCAPE_FROM_WAREHOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_INTERIOR_MIDDLE, "ESCAPE_FROM_WAREHOUSE")
		ELIF e_mission_stage >= STAGE_ESCAPE_PRE_WAREHOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_INTERIOR_START, "ESCAPE_PRE_WAREHOUSE")
		ELIF e_mission_stage >= STAGE_GO_TO_PLANT
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_GO_TO_PLANT, "GO_TO_PLANT")
		ELIF e_mission_stage >= STAGE_BUY_GRENADES
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_GO_INSIDE_GUN_SHOP, "BUY_GRENADES")
		ELSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "GO_TO_GUN_SHOP")
		ENDIF
	ENDIF
ENDPROC

PROC DO_BUDDY_FAILS()
	IF NOT IS_PED_INJURED(s_stretch.ped)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_stretch.ped)) > 22500.0
			IF NOT IS_PED_INJURED(s_lamar.ped)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_lamar.ped)) > 10000.0
					MISSION_FAILED(FAILED_LEFT_LAMAR_AND_STRETCH)
				ELSE
					MISSION_FAILED(FAILED_LEFT_STRETCH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(s_lamar.ped)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_lamar.ped)) > 22500.0
			IF NOT IS_PED_INJURED(s_stretch.ped)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_stretch.ped)) > 10000.0
					MISSION_FAILED(FAILED_LEFT_LAMAR_AND_STRETCH)
				ELSE
					MISSION_FAILED(FAILED_LEFT_LAMAR)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Generic proc for handling fails for all the drive sections of the mission.
PROC DO_CAR_AND_BUDDY_FAILS()
	IF IS_VEHICLE_DRIVEABLE(veh_start_car)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_start_car)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(veh_start_car)) > 40000.0
				MISSION_FAILED(FAILED_ABANDONED_CAR)
			ENDIF
		ELSE
			DO_BUDDY_FAILS()
		ENDIF
	ENDIF
ENDPROC

///Sets the availability of all of the gun shops except the one that's used in the mission (that one is open from the start).
PROC SET_ALL_NON_MISSION_GUN_SHOPS_OPEN(BOOL bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_02_SS, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_03_HW, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_04_ELS, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_05_PB, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_06_LS, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_07_MW, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_08_CS, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_09_GOH, bIsOpen)
	SET_SHOP_IS_AVAILABLE(GUN_SHOP_10_VWH, bIsOpen)
ENDPROC

FUNC BOOL SETUP_BALLA(BOOL bOnlyRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(ped_ballas_og)
		REQUEST_MODEL(model_ballas_og)
		
		IF HAS_MODEL_LOADED(model_ballas_og)
			IF NOT bOnlyRequestAssets
				ped_ballas_og = CREATE_PED(PEDTYPE_MISSION, model_ballas_og, <<-622.82, -1619.93, 32.21>>, -1.28)
				SET_PED_COMPONENT_VARIATION(ped_ballas_og, PED_COMP_TORSO, 0, 1)
				SET_PED_COMPONENT_VARIATION(ped_ballas_og, PED_COMP_LEG, 0, 1)
				SET_PED_COMPONENT_VARIATION(ped_ballas_og, PED_COMP_DECL, 0, 0)
				SET_PED_PROP_INDEX(ped_ballas_og, ANCHOR_HEAD, 1, 0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_ballas_og, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(model_ballas_og)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

//Sets up the dead Balla who's shot during the meeting cutscene.
FUNC BOOL SETUP_DEAD_BALLA(BOOL bStartInvisible, BOOL bOnlyRequestAssets = FALSE, BOOL bForceUpdate = FALSE)
	IF SETUP_BALLA(bOnlyRequestAssets)
		REQUEST_ANIM_DICT(str_balla_dead_anim)
		
		IF HAS_ANIM_DICT_LOADED(str_balla_dead_anim)
			IF NOT bOnlyRequestAssets
				IF NOT IS_PED_INJURED(ped_ballas_og)
				AND (NOT IS_ENTITY_PLAYING_ANIM(ped_ballas_og, str_balla_dead_anim, "dead_idle")
				OR bForceUpdate)
					SET_ENTITY_COORDS(ped_ballas_og, <<-622.82, -1619.93, 32.21>>)
					SET_ENTITY_HEADING(ped_ballas_og, -1.28)
					TASK_PLAY_ANIM(ped_ballas_og, str_balla_dead_anim, "dead_idle", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
					IF bForceUpdate
						FORCE_PED_AI_AND_ANIMATION_UPDATE(ped_ballas_og)
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_ballas_og, TRUE)
					SET_ENTITY_VISIBLE(ped_ballas_og, NOT bStartInvisible)
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up the trigger scene peds in case we Z-skipped to the cutscene.
FUNC BOOL SETUP_TRIGGER_SCENE_PEDS(BOOL bOnlyRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[DENISE_ID])
		REQUEST_MODEL(IG_DENISE)
		REQUEST_MODEL(IG_MAGENTA)
		REQUEST_MODEL(A_F_Y_YOGA_01)
		REQUEST_MODEL(CSB_DENISE_FRIEND)
	
		IF HAS_MODEL_LOADED(IG_DENISE)
		AND HAS_MODEL_LOADED(IG_MAGENTA)
		AND HAS_MODEL_LOADED(A_F_Y_YOGA_01)
		AND HAS_MODEL_LOADED(CSB_DENISE_FRIEND)
			IF bOnlyRequestAssets
				RETURN TRUE
			ELSE
				g_sTriggerSceneAssets.ped[DENISE_ID] = CREATE_PED(PEDTYPE_MISSION, IG_DENISE, <<-10.088, -1440.670, 30.357>>, 0.0)		
				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[DENISE_ID], PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[DENISE_ID], PED_COMP_TORSO, 0, 1)
				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[DENISE_ID], PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[DENISE_ID], PED_COMP_LEG, 0, 0)
				
				g_sTriggerSceneAssets.ped[MAGENTA_ID] = CREATE_PED(PEDTYPE_MISSION, IG_MAGENTA, <<-9.088, -1439.670, 30.357>>, 0.0)				
				g_sTriggerSceneAssets.ped[FRIEND_ID_1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_YOGA_01, <<-9.088, -1441.670, 30.357>>, 0.0)				
				g_sTriggerSceneAssets.ped[FRIEND_ID_2] = CREATE_PED(PEDTYPE_MISSION, CSB_DENISE_FRIEND, <<-10.088, -1441.670, 30.357>>, 0.0)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_DENISE)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_MAGENTA)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_YOGA_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(CSB_DENISE_FRIEND)
				
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_FRANKLINS_CAR(VECTOR v_pos, FLOAT f_heading = 0.0, BOOL b_create_last_car_instead = FALSE)
	IF NOT DOES_ENTITY_EXIST(veh_start_car)
		IF b_create_last_car_instead
			IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
				
				IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
					veh_start_car = CREATE_REPLAY_CHECKPOINT_VEHICLE(v_pos, f_heading)
					
					//1933816 - Make sure that the checkpoint vehicle has enough seats, if it doesn't the script should disregard it create a new one.
					IF DOES_VEHICLE_HAVE_ENOUGH_SEATS(veh_start_car, s_locates_data, 2)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh_start_car, FALSE)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(veh_start_car, FALSE)
						
						RETURN TRUE
					ELSE
						REMOVE_VEHICLE(veh_start_car, TRUE)
						b_create_last_car_instead = FALSE
					ENDIF
				ENDIF
			ELSE
				b_create_last_car_instead = FALSE
			ENDIF
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(veh_start_car) //If grabbing the snapshot vehicle failed then just create Lamar's car instead.
		AND NOT b_create_last_car_instead
			REQUEST_NPC_VEH_MODEL(CHAR_LAMAR, VEHICLE_TYPE_SECONDARY_CAR)
			REQUEST_VEHICLE_ASSET(GET_NPC_VEH_MODEL(CHAR_LAMAR), ENUM_TO_INT(VRF_REQUEST_ENTRY_ANIMS))
			
			IF HAS_NPC_VEH_MODEL_LOADED(CHAR_LAMAR, VEHICLE_TYPE_SECONDARY_CAR)
			AND HAS_VEHICLE_ASSET_LOADED(GET_NPC_VEH_MODEL(CHAR_LAMAR))
				CREATE_NPC_VEHICLE(veh_start_car, CHAR_LAMAR, v_pos, f_heading, TRUE, VEHICLE_TYPE_SECONDARY_CAR)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh_start_car, FALSE)
				SET_VEHICLE_INFLUENCES_WANTED_LEVEL(veh_start_car, FALSE)			
				
				#IF IS_DEBUG_BUILD
					SET_VEHICLE_NAME_DEBUG(veh_start_car, "Lamar car")
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_FRANKLINS_HOUSE_INTERIOR_READY()
	IF interior_franklins_house = NULL
		interior_franklins_house = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-14.4, -1437.0, 31.5>>, "v_franklins")
	ELSE
		IF IS_INTERIOR_READY(interior_franklins_house)
			RETURN TRUE
		ELSE
			PIN_INTERIOR_IN_MEMORY(interior_franklins_house)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up a given mission requirement: a mission requirement is something that needs to be created or set during a particular part of the mission e.g. creating a buddy or vehicle, 
///    opening/closing doors etc. Many of these requirements carry over to multiple mission stages, so this helps to reduce duplicate code when skipping directly to a stage.
FUNC BOOL SETUP_MISSION_REQUIREMENT_WITH_LOCATION(MISSION_REQUIREMENT e_requirement, VECTOR v_pos, FLOAT f_heading = 0.0, BOOL b_just_request_assets = FALSE)
	SWITCH e_requirement
		CASE REQ_LAMAR
			IF NOT DOES_ENTITY_EXIST(s_lamar.ped)
				REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
			
				IF HAS_NPC_PED_MODEL_LOADED(CHAR_LAMAR)
					IF b_just_request_assets
						RETURN TRUE
					ELIF CREATE_NPC_PED_ON_FOOT(s_lamar.ped, CHAR_LAMAR, v_pos, f_heading)
						SET_PED_CAN_BE_TARGETTED(s_lamar.ped, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(s_lamar.ped, RELGROUPHASH_PLAYER)
//						SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
//						SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_DisableExplosionReactions, TRUE)
						SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
						s_lamar.i_event = 0
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, s_lamar.ped, "LAMAR")
						
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_HAIR, 2, 0)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_TORSO, 2, 2)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_LEG, 5, 0)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_FEET, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_HAND, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_SPECIAL, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_BERD, 1, 0)
						
						GIVE_WEAPON_TO_PED(s_lamar.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
						GIVE_WEAPON_TO_PED(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, FALSE)
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				//Need to have this here for the first cutscene: the peds are created by the cutscene so the above script won't trigger.
				IF NOT IS_PED_INJURED(s_lamar.ped)
					SET_PED_CAN_BE_TARGETTED(s_lamar.ped, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(s_lamar.ped, RELGROUPHASH_PLAYER)
//					SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
//					SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_DisableExplosionReactions, TRUE)
					SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
					s_lamar.i_event = 0
					
					IF s_conversation_peds.PedInfo[5].Index != s_lamar.ped
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, s_lamar.ped, "LAMAR")
					ENDIF
					
					IF NOT HAS_PED_GOT_WEAPON(s_lamar.ped, WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(s_lamar.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
					ENDIF
					
					IF NOT HAS_PED_GOT_WEAPON(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN)
						GIVE_WEAPON_TO_PED(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, FALSE)
					ENDIF
				ENDIF
			
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_STRETCH
			IF NOT DOES_ENTITY_EXIST(s_stretch.ped)
				REQUEST_NPC_PED_MODEL(CHAR_STRETCH)
			
				IF HAS_NPC_PED_MODEL_LOADED(CHAR_STRETCH)
					IF b_just_request_assets
						RETURN TRUE
					ELIF CREATE_NPC_PED_ON_FOOT(s_stretch.ped, CHAR_STRETCH, v_pos, f_heading)
						SET_PED_CAN_BE_TARGETTED(s_stretch.ped, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(s_stretch.ped, RELGROUPHASH_PLAYER)
//						SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_RunFromFiresAndExplosions, FALSE)
//						SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_DisableExplosionReactions, TRUE)
						SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
						s_stretch.i_event = 0
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, s_stretch.ped, "STRETCH")
						
						SET_PED_PROP_INDEX(s_stretch.ped, ANCHOR_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_stretch.ped, PED_COMP_SPECIAL, 0, 0)
						GIVE_WEAPON_TO_PED(s_stretch.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(s_stretch.ped)
					SET_PED_CAN_BE_TARGETTED(s_stretch.ped, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(s_stretch.ped, RELGROUPHASH_PLAYER)
//					SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_RunFromFiresAndExplosions, FALSE)
//					SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_DisableExplosionReactions, TRUE)
					SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
					s_stretch.i_event = 0
					
					IF s_conversation_peds.PedInfo[7].Index != s_lamar.ped
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, s_stretch.ped, "STRETCH")
					ENDIF
					
					IF NOT HAS_PED_GOT_WEAPON(s_stretch.ped, WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(s_stretch.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
					ENDIF
				ENDIF
			
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_ASSISTED_ROUTES
			ASSISTED_MOVEMENT_REQUEST_ROUTE("Lemar1")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("Lemar2")
			
			RETURN TRUE
		BREAK
		
		CASE REQ_OPEN_GUN_SHOP
			SET_SHOP_IS_AVAILABLE(GUN_SHOP_01_DT, TRUE)
			
			RETURN TRUE
		BREAK
		
		CASE REQ_GUN_SHOP_IN_MEMORY
			IF interior_gun_shop = NULL
				interior_gun_shop = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<20.3, -1109.7, 30.5>>, "v_gun")
			ELSE
				IF IS_INTERIOR_READY(interior_gun_shop)
					RETURN TRUE
				ELSE
					PIN_INTERIOR_IN_MEMORY(interior_gun_shop)
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_RECYCLING_INTERIOR_IN_MEMORY
			IF interior_recycle = NULL
				interior_recycle = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-624.2677, -1619.1552, 34.0105>>, "v_recycle")
			ENDIF
		
			IF IS_INTERIOR_READY(interior_recycle)
				RETURN TRUE
			ELSE
				PIN_INTERIOR_IN_MEMORY(interior_recycle)
			ENDIF
		BREAK
		
		CASE REQ_STRETCH_AND_LAMAR_READY_TO_FIGHT
			//Setup Lamar
			IF NOT IS_PED_INJURED(s_lamar.ped)
				REMOVE_PED_FROM_GROUP(s_lamar.ped)
				GIVE_WEAPON_TO_PED(s_lamar.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
				GIVE_WEAPON_TO_PED(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(s_lamar.ped, RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FIRE | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_lamar.ped, FALSE)
				//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_lamar.ped, TRUE) //TEMP: lamar should be killable in final version if player hangs around too much
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
				SET_ENTITY_PROOFS(s_lamar.ped, FALSE, TRUE, FALSE, FALSE, FALSE)
				SET_PED_ALLOWED_TO_DUCK(s_lamar.ped, FALSE)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(s_lamar.ped, FALSE)
				SET_PED_MAX_HEALTH(s_lamar.ped, 1800)
				SET_ENTITY_HEALTH(s_lamar.ped, 1800)
				SET_PED_SUFFERS_CRITICAL_HITS(s_lamar.ped, FALSE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_DisableExplosionReactions, TRUE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_DisableHurt, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(s_lamar.ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(s_lamar.ped, CA_DISABLE_PINNED_DOWN, TRUE)
				SET_PED_CAN_COWER_IN_COVER(s_lamar.ped, FALSE)
				SET_PED_ENABLE_WEAPON_BLOCKING(s_lamar.ped, TRUE)
				SET_PED_CHANCE_OF_FIRING_BLANKS(s_lamar.ped, 0.5, 0.8)
				
				IF NOT DOES_BLIP_EXIST(s_lamar.blip)
					s_lamar.blip = CREATE_BLIP_FOR_PED(s_lamar.ped)
				ENDIF
				s_lamar.i_event = 0
				s_lamar.i_timer = 0
				s_lamar.i_sync_scene = -1
				SET_PED_COMBAT_AI(s_lamar.ped, 2, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_USE_COVER, <<0.0, 0.0, 0.0>>)
				//SET_PED_COMBAT_ATTRIBUTES(s_lamar.ped, CA_BLIND_FIRE_IN_COVER, TRUE)
			ENDIF
			
			//Setup Stretch
			IF NOT IS_PED_INJURED(s_stretch.ped)
				REMOVE_PED_FROM_GROUP(s_stretch.ped)
				GIVE_WEAPON_TO_PED(s_stretch.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(s_stretch.ped, RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_FIRE | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_FALLING | RBF_IMPACT_OBJECT)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_stretch.ped, FALSE)
				//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_stretch.ped, TRUE) //TEMP: Stretch should be killable in final version if player hangs around too much
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
				SET_ENTITY_PROOFS(s_stretch.ped, FALSE, TRUE, FALSE, FALSE, FALSE)
				SET_PED_ALLOWED_TO_DUCK(s_stretch.ped, FALSE)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(s_stretch.ped, FALSE)
				SET_PED_MAX_HEALTH(s_stretch.ped, 1800)
				SET_ENTITY_HEALTH(s_stretch.ped, 1800)
				SET_PED_SUFFERS_CRITICAL_HITS(s_stretch.ped, FALSE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_DisableExplosionReactions, TRUE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_DisableHurt, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_DISABLE_PINNED_DOWN, TRUE)
				SET_PED_CAN_COWER_IN_COVER(s_stretch.ped, FALSE)
				SET_PED_ENABLE_WEAPON_BLOCKING(s_stretch.ped, TRUE)
				SET_PED_CHANCE_OF_FIRING_BLANKS(s_stretch.ped, 0.5, 0.8)
				
				IF NOT DOES_BLIP_EXIST(s_stretch.blip)
					s_stretch.blip = CREATE_BLIP_FOR_PED(s_stretch.ped)
				ENDIF
				s_stretch.i_event = 0
				s_stretch.i_timer = 0
				s_stretch.i_sync_scene = -1
				SET_PED_COMBAT_AI(s_stretch.ped, 2, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, CA_USE_COVER, <<0.0, 0.0, 0.0>>)
				//SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_BLIND_FIRE_IN_COVER, TRUE)
			ENDIF
			
			RETURN TRUE
		BREAK
		
		CASE REQ_AMBIENCE_REMOVED_FOR_FIGHT			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
			SET_CREATE_RANDOM_COPS(FALSE)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
			RETURN TRUE
		BREAK
			
		CASE REQ_AMBIENT_SIRENS
			DISTANT_COP_CAR_SIRENS(TRUE)

			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

///Requests a given mission requirement, this will load any assets associated with that requirement and return TRUE once the assets are loaded.
FUNC BOOL REQUEST_MISSION_REQUIREMENT(MISSION_REQUIREMENT e_requirement)
	RETURN SETUP_MISSION_REQUIREMENT_WITH_LOCATION(e_requirement, <<0.0, 0.0, 0.0>>, 0.0, TRUE)
ENDFUNC

FUNC BOOL SETUP_MISSION_REQUIREMENT(MISSION_REQUIREMENT e_requirement)
	RETURN SETUP_MISSION_REQUIREMENT_WITH_LOCATION(e_requirement, <<0.0, 0.0, 0.0>>)
ENDFUNC

PROC GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
	WEAPON_TYPE e_fail_weapon = Get_Fail_Weapon(ENUM_TO_INT(CHAR_FRANKLIN))
	
	IF e_fail_weapon = WEAPONTYPE_INVALID 
	OR e_fail_weapon = WEAPONTYPE_UNARMED
	OR NOT b_used_a_checkpoint //We debug skipped here, just give the shotgun
	OR NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), e_fail_weapon)
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
		AND GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN) > 0
			e_fail_weapon = WEAPONTYPE_PUMPSHOTGUN
		ELSE
			e_fail_weapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
		ENDIF
	ENDIF
		
	IF e_fail_weapon != WEAPONTYPE_INVALID 
	AND e_fail_weapon != WEAPONTYPE_UNARMED
		INT i_ammo_per_clip = GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), e_fail_weapon)
	
		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), e_fail_weapon) < i_ammo_per_clip
		OR GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), e_fail_weapon) = 0
			IF i_ammo_per_clip != 0
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), e_fail_weapon, i_ammo_per_clip * 2)
				SET_AMMO_IN_CLIP(PLAYER_PED_ID(), e_fail_weapon, i_ammo_per_clip)
			ELSE
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), e_fail_weapon, 1)
			ENDIF
		ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), e_fail_weapon, TRUE)
	ENDIF	
ENDPROC

FUNC BOOL SETUP_REQ_FRANKLIN_HAS_WEAPONS_ON_Z_SKIP()
	IF NOT b_used_a_checkpoint
	OR b_shit_skipped_weapons_shop		
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 100)
		ENDIF
		
		IF NOT HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH)
			GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH)
		ENDIF
		
		b_shit_skipped_weapons_shop = FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC OPENING_CUTSCENE()
	IF e_section_stage = SECTION_STAGE_SETUP
		IF b_is_jumping_directly_to_stage
			b_is_jumping_directly_to_stage = FALSE
		ELSE
			REQUEST_CUTSCENE(str_intro_cutscene)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS(str_intro_cutscene)
			
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)	//Fix for 1430093
			SET_FRANKLINS_HOUSE_INTERIOR_READY()
			
			IF i_current_event < 99
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			
				//Wait for Franklin to fully enter the house before starting the timelapse.
				IF NOT IS_SCREEN_FADED_OUT()
					REQUEST_ANIM_DICT(str_timelapse_anims)
					REQUEST_MODEL(model_franklins_front_door)
					
					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
						ADD_DOOR_TO_SYSTEM(iFrontDoor, V_ILEV_FA_FRONTDOOR, <<-14.8689, -1441.1821, 31.1932>>)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(objFrontDoor)
						objFrontDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-14.8689, -1441.1821, 31.1932>>, 1.0, V_ILEV_FA_FRONTDOOR)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objFrontDoor)
					AND IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
					AND HAS_MODEL_LOADED(model_franklins_front_door)
					AND HAS_ANIM_DICT_LOADED(str_timelapse_anims)
					AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID())
						IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<0.0, 0.0, 0.0>>, TRUE)
							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-3.2086, -1465.9180, 29.4404>>, 274.0793)
						ENDIF
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-16.075600,-1468.568848,29.098814>>, <<-17.915562,-1431.504395,36.052402>>, 20.00000, <<-3.2086, -1465.9180, 29.4404>>, 274.0793,
																		    GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						
						BOOL b_player_is_already_by_door
						VECTOR v_player_pos
						
						v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						
						IF v_player_pos.z > 31.0
							b_player_is_already_by_door = TRUE
						ELSE
							b_player_is_already_by_door = FALSE
						ENDIF
						
						
						IF DOOR_SYSTEM_GET_OPEN_RATIO(iFrontDoor) <> 1.0
						OR DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
							DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, 1.0, FALSE, FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
						ENDIF
						
						SET_ENTITY_VISIBLE(objFrontDoor, FALSE)
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						i_sync_scene_timelapse = CREATE_SYNCHRONIZED_SCENE(<<-14.862, -1441.208, 31.180>>, <<0.000, 0.000, 180.000>>)
						//CREATE_MODEL_HIDE(<<-14.8689, -1441.1821, 31.1932>>, 2.0, model_franklins_front_door, FALSE)
						obj_franklins_front_door = CREATE_OBJECT(model_franklins_front_door, <<-14.8689, -1441.1821, 31.1932>>)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_franklins_front_door)
						
						//cam_cutscene = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						//PLAY_SYNCHRONIZED_CAM_ANIM(cam_cutscene, i_sync_scene_timelapse, "franklin_enters_old_home_cam", str_timelapse_anims)
						cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-7.205015,-1471.655396,31.161417>>,<<9.377599,-0.000000,11.473698>>,38.526501, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						DISPLAY_HUD(FALSE)
						DISPLAY_RADAR(FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
						
						IF NOT IS_REPLAY_IN_PROGRESS()
							IF DOES_ENTITY_EXIST(GET_MISSION_START_VEHICLE_INDEX())
							AND NOT IS_ENTITY_DEAD(GET_MISSION_START_VEHICLE_INDEX())
								SET_ENTITY_AS_MISSION_ENTITY(GET_MISSION_START_VEHICLE_INDEX(), TRUE, TRUE)
							ENDIF
						ENDIF
						
						CLEAR_AREA_OF_PEDS(<<-17.1615, -1439.0176, 30.1015>>, 400.0)
						CLEAR_AREA_OF_VEHICLES(<<-17.1615, -1439.0176, 30.1015>>, 400.0, TRUE)
						CLEAR_AREA_OF_OBJECTS(<<-17.1615, -1439.0176, 30.1015>>, 400.0)
						CLEAR_AREA_OF_PROJECTILES(<<-17.1615, -1439.0176, 30.1015>>, 400.0)
						REMOVE_PARTICLE_FX_IN_RANGE(<<-17.1615, -1439.0176, 30.1015>>, 50.0)
						
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), i_sync_scene_timelapse, str_timelapse_anims, "franklin_enters_old_home", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_FALLING)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(obj_franklins_front_door, i_sync_scene_timelapse, "franklin_enters_old_home_door", str_timelapse_anims, INSTANT_BLEND_IN)
						
						IF b_player_is_already_by_door
							SET_SYNCHRONIZED_SCENE_PHASE(i_sync_scene_timelapse, 0.3)
						ELSE
							SET_SYNCHRONIZED_SCENE_PHASE(i_sync_scene_timelapse, 0.15)
						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(obj_franklins_front_door)
						
						//2001932 - Call timelapse shadow setup commands early to prevent pop.
						CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)
						CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.6)
						CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
						
						b_override_cam_anim = FALSE
						SETTIMERB(0)
						i_current_event = 99
					ENDIF
				ELSE
					SETTIMERB(0)
					i_current_event = 99
				ENDIF
			ELIF i_current_event = 99
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				//CLEAR_AREA(<<-17.1615, -1439.0176, 30.1015>>, 5000.0, TRUE)
				REQUEST_SCRIPT_AUDIO_BANK("LAMAR1_INT")
				
				IF NOT b_override_cam_anim
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(i_sync_scene_timelapse) AND GET_SYNCHRONIZED_SCENE_PHASE(i_sync_scene_timelapse) > 0.67)
						DESTROY_ALL_CAMS()
						cam_cutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-7.205015,-1471.655396,31.161417>>,<<9.377599,-0.000000,11.473698>>,38.526501, TRUE)
						b_override_cam_anim = TRUE
					ENDIF
				ENDIF
				
				//Continue to check that the real door to Franklin's house is kept open during the timelapse.
				IF DOOR_SYSTEM_GET_OPEN_RATIO(iFrontDoor) <> 1.0
				OR DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
					DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, 1.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(i_sync_scene_timelapse) AND GET_SYNCHRONIZED_SCENE_PHASE(i_sync_scene_timelapse) > 0.85)
				OR (NOT IS_SYNCHRONIZED_SCENE_RUNNING(i_sync_scene_timelapse) AND TIMERB() > 5000)
				OR IS_SCREEN_FADED_OUT()
					IF DO_TIMELAPSE(SP_MISSION_LAMAR,sTimelapse, TRUE, FALSE, FALSE, FALSE, TRUE)
						PLAY_SOUND_FRONTEND(-1, "LAMAR1_PARTYGIRLS_master")
					
						SETTIMERB(0)
						i_current_event++
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					ENDIF
				ENDIF
			ELIF i_current_event = 100
				IF TIMERB() > 2500
				OR IS_SCREEN_FADED_OUT()
					i_current_event++
				ENDIF
			ELSE
				IF HAS_CUTSCENE_LOADED() //This shouldn't need the failsafe as we can wait indefinitely here?
				 	SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_ASSISTED_ROUTES, <<0.0, 0.0, 0.0>>)
					
					REGISTER_ENTITY_FOR_CUTSCENE(s_lamar.ped, "Lamar", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_LAMAR))
					REGISTER_ENTITY_FOR_CUTSCENE(s_stretch.ped, "Stretch", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_STRETCH))
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, TRUE)
					
					b_need_to_create_start_vehicle = FALSE					
					b_skipped_mocap = FALSE
					i_current_event = 0
					e_section_stage = SECTION_STAGE_RUNNING
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		IF NOT b_skipped_mocap
			IF WAS_CUTSCENE_SKIPPED()
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
				
				b_skipped_mocap = TRUE
			ENDIF
		ENDIF
	
		SWITCH i_current_event
			CASE 0
				IF IS_CUTSCENE_PLAYING()
					SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TRACKSUIT_JEANS, FALSE)
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
					CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
					CASCADE_SHADOWS_INIT_SESSION()
					
					b_need_to_create_start_vehicle = TRUE
					
					IF DOES_CAM_EXIST(cam_cutscene)
						DESTROY_ALL_CAMS()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
					
					//Clean up the timelapse anims
					REMOVE_OBJECT(obj_franklins_front_door, TRUE)
					//REMOVE_MODEL_HIDE(<<-14.8689, -1441.1821, 31.1932>>, 2.0, model_franklins_front_door, FALSE)
					REMOVE_ANIM_DICT(str_timelapse_anims)
					
					IF DOES_ENTITY_EXIST(objFrontDoor)
						SET_ENTITY_VISIBLE(objFrontDoor, TRUE)
						
						SET_OBJECT_AS_NO_LONGER_NEEDED(objFrontDoor)
					ENDIF
					
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
						DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, 0.0, FALSE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
					ENDIF
					
					MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_LAMAR)
					
					CLEAR_AREA_OF_PEDS(<<-12.7896, -1458.1570, 29.4592>>, 200.0)

					DO_FADE_IN_WITH_WAIT()

					i_current_event++
				ENDIF
			BREAK
			
			CASE 1
				//If it failed to grab a vehicle then just create Franklin's car there.
				//IF NOT IS_AREA_OCCUPIED(<<-13.7896, -1459.1570, 29.4592>>, <<-11.7896, -1457.1570, 29.4592>>, FALSE, TRUE, FALSE, FALSE, FALSE)
				IF b_need_to_create_start_vehicle
					SETUP_FRANKLINS_CAR(<<-12.7896, -1458.1570, 29.4592>>, 273.6953)
				ENDIF
			BREAK
		ENDSWITCH
	
		//Check for the closest vehicle: this will be used for Lamar/Stretch's tasks on exit state.
		VEHICLE_INDEX veh_closest[1]
		GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), veh_closest)
		
		IF DOES_ENTITY_EXIST(veh_start_car)
			veh_closest[0] = veh_start_car
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(s_lamar.ped)
			ENTITY_INDEX entity_lamar = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lamar")
			IF DOES_ENTITY_EXIST(entity_lamar)
				s_lamar.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_lamar)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(s_stretch.ped)
			ENTITY_INDEX entity_stretch = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Stretch")
			IF DOES_ENTITY_EXIST(entity_stretch)
				s_stretch.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_stretch)
			ENDIF
		ENDIF

		//Exit states
		IF NOT IS_PED_INJURED(s_lamar.ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(s_lamar.ped, PEDMOVE_WALK)
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar", GET_ENTITY_MODEL(s_lamar.ped))
				//TEMP: Warp Lamar as the cutscene is placing him somewhere else.
				SET_ENTITY_COORDS(s_lamar.ped, <<-10.1510, -1456.4628, 29.4131>>)
				SET_ENTITY_HEADING(s_lamar.ped, 212.6173)
				SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_BERD, 1, 0)
				
				IF IS_VEHICLE_DRIVEABLE(veh_closest[0])
				AND DOES_VEHICLE_HAVE_ENOUGH_SEATS(veh_closest[0], s_locates_data, 2)
					TASK_ENTER_VEHICLE(s_lamar.ped, veh_closest[0], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
				ENDIF
				
				FORCE_PED_MOTION_STATE(s_lamar.ped, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(s_stretch.ped)
			SET_PED_MAX_MOVE_BLEND_RATIO(s_stretch.ped, PEDMOVE_WALK)
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Stretch")
				SET_PED_PROP_INDEX(s_stretch.ped, ANCHOR_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(s_stretch.ped, PED_COMP_SPECIAL, 0, 0)
			
				IF IS_VEHICLE_DRIVEABLE(veh_closest[0])
				AND DOES_VEHICLE_HAVE_ENOUGH_SEATS(veh_closest[0], s_locates_data, 2)
					SET_ENTITY_COORDS(s_stretch.ped, <<-13.9305, -1454.9846, 29.4717>>)
					SET_ENTITY_HEADING(s_stretch.ped, 130.2734)
					TASK_ENTER_VEHICLE(s_stretch.ped, veh_closest[0], DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT, PEDMOVE_WALK)
				ENDIF
				
				FORCE_PED_MOTION_STATE(s_stretch.ped, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
			ENDIF
			
			SET_PED_RESET_FLAG(s_stretch.ped, PRF_UseKinematicPhysics, TRUE)
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			IF NOT b_skipped_mocap
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500, -157.0, FALSE)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
		OR b_skipped_mocap
		
			REPLAY_STOP_EVENT()
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF

		IF NOT IS_CUTSCENE_ACTIVE()
			e_section_stage = SECTION_STAGE_CLEANUP
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		REMOVE_OBJECT(obj_franklins_front_door, TRUE)
		//REMOVE_MODEL_HIDE(<<-14.8689, -1441.1821, 31.1932>>, 2.0, model_franklins_front_door, FALSE)
		REMOVE_ANIM_DICT(str_timelapse_anims)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("LAMAR1_INT")
		
		IF DOES_ENTITY_EXIST(objFrontDoor)
			SET_ENTITY_VISIBLE(objFrontDoor, TRUE)
			
			SET_OBJECT_AS_NO_LONGER_NEEDED(objFrontDoor)
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
			IF DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor) != DOORSTATE_UNLOCKED
				DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
			ENDIF
		ENDIF
	
		IF interior_franklins_house != NULL
			IF IS_INTERIOR_READY(interior_franklins_house)
				UNPIN_INTERIOR(interior_franklins_house)
				interior_franklins_house = NULL
			ENDIF
		ENDIF
	
		If b_skipped_mocap
			//If the player skipped the mocap the peds need to be warped.
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE

			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-14.2794, -1453.4706, 29.5179>>, FALSE)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 201.1911)
		
			//Make sure Lamar and Stretch were created.
			WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<-9.5493, -1458.8500, 29.5055>>, 133.2693)
			OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<-12.3130, -1452.9957, 29.5358>>, 178.3208)
				WAIT(0)
			ENDWHILE
			
			//Create the car if needed.
			IF b_need_to_create_start_vehicle
				WHILE NOT SETUP_FRANKLINS_CAR(<<-12.7896, -1458.1570, 29.4592>>, 273.6953)
					WAIT(0)
				ENDWHILE
			ENDIF
		
			IF NOT IS_PED_INJURED(s_lamar.ped)
				SET_ENTITY_COORDS(s_lamar.ped, <<-11.2779, -1455.2010, 29.5488>>)
				SET_ENTITY_HEADING(s_lamar.ped, 158.1405)
			ENDIF
		
			IF NOT IS_PED_INJURED(s_stretch.ped)				
				SET_ENTITY_COORDS(s_stretch.ped, <<-14.3457, -1456.4288, 29.4137>>)
				SET_ENTITY_HEADING(s_stretch.ped, -166.9394)
			ENDIF
			
			WHILE DOES_ENTITY_EXIST(s_lamar.ped) AND NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped)
				WAIT(0)
			ENDWHILE
			
			WHILE DOES_ENTITY_EXIST(s_stretch.ped) AND NOT IS_PED_INJURED(s_stretch.ped)
			AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped)
				WAIT(0)
			ENDWHILE
			
			WAIT(0)
			
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 750, -157.0, FALSE)
			
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
		
		//Setup buddy relationship groups etc.
		SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<-9.5493, -1458.8500, 29.505>>, 133.2693)
		SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<-14.6866, -1455.3466, 29.4362>>, 212.5621)
	
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
		i_current_event = 0
		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_GO_TO_GUN_SHOP
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		DO_FADE_OUT_WITH_WAIT()
		STOP_CUTSCENE()

		b_skipped_mocap = TRUE
		
		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

PROC GO_TO_GUN_SHOP()
	IF e_section_stage = SECTION_STAGE_SETUP
		IF b_is_jumping_directly_to_stage
			IF b_used_a_checkpoint
				START_REPLAY_SETUP(<<-14.2794, -1453.4706, 29.5179>>, 210.5592, FALSE)  
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-14.2794, -1453.4706, 29.5179>>, FALSE)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 210.5592)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				WAIT(0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_ASSISTED_ROUTES, <<0.0, 0.0, 0.0>>)
			
			IF NOT IS_AREA_OCCUPIED(<<-13.7896, -1459.1570, 29.4592>>, <<-11.7896, -1457.1570, 29.4592>>, FALSE, TRUE, FALSE, FALSE, FALSE)
				WHILE NOT SETUP_FRANKLINS_CAR(<<-12.7896, -1458.1570, 29.4592>>, 273.6953)
					WAIT(0)
				ENDWHILE
			ENDIF
			
			WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<-12.0409, -1460.0720, 29.5347>>, 31.2574)
			OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<-12.3130, -1452.9957, 29.5358>>, 178.3208)
				WAIT(0)
			ENDWHILE
		
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, TRUE)
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERB(0)
				
			//Wait for clothes to stream in
			WHILE TIMERB() < 10000
				IF (NOT IS_PED_INJURED(s_lamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped))
				AND (NOT IS_PED_INJURED(s_stretch.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped))
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					SETTIMERB(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			i_current_event = 99
			b_is_jumping_directly_to_stage = FALSE
		ELSE		
			//Set up buddies again, just in case the cutscene streaming didn't work out
			IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<-12.0409, -1460.0720, 29.5347>>, 31.2574)
			AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<-12.3130, -1452.9957, 29.5358>>, 178.3208)
			AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
				DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(s_locates_data, TRUE)
				SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(s_locates_data)
			
				//Load scene in case of trip skip: there have been issues with buddies disappearing on skips.
				IF IS_SCREEN_FADED_OUT()	
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-14.2794, -1453.4706, 29.5179>>, FALSE)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 201.1911)
					
					IF NOT IS_PED_INJURED(s_lamar.ped)
					AND NOT IS_PED_INJURED(s_stretch.ped)
						SET_ENTITY_COORDS(s_lamar.ped, <<-11.2779, -1455.2010, 29.5488>>)
						SET_ENTITY_HEADING(s_lamar.ped, 158.1405)
						
						SET_ENTITY_COORDS(s_stretch.ped, <<-14.0287, -1456.5962, 29.4127>>)
						SET_ENTITY_HEADING(s_stretch.ped, 188.2834)
						
						VEHICLE_INDEX vehClosest[1]
						GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehClosest)
						
						IF DOES_ENTITY_EXIST(veh_start_car)
							vehClosest[0] = veh_start_car
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehClosest[0])
						AND DOES_VEHICLE_HAVE_ENOUGH_SEATS(vehClosest[0], s_locates_data, 2)
							TASK_ENTER_VEHICLE(s_lamar.ped, vehClosest[0], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
							TASK_ENTER_VEHICLE(s_stretch.ped, vehClosest[0], DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT, PEDMOVE_WALK)
						ENDIF
					ENDIF
					
					WAIT(500)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				ENDIF		
								
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, TRUE)
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, TRUE)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
				
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					WHILE NOT IS_SCREEN_FADED_IN()
						IF NOT IS_PED_INJURED(s_lamar.ped)
							SET_PED_MAX_MOVE_BLEND_RATIO(s_lamar.ped, PEDMOVE_WALK)
						ENDIF
						
						IF NOT IS_PED_INJURED(s_stretch.ped)
							SET_PED_MAX_MOVE_BLEND_RATIO(s_stretch.ped, PEDMOVE_WALK)
						ENDIF
					
						WAIT(0)
					ENDWHILE
				ENDIF
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				i_current_event = 0
				i_haircut_dialogue_timer = 0
				i_audio_scene_event_drive_to_shop = 0
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		#IF IS_DEBUG_BUILD
			DONT_DO_J_SKIP(s_locates_data)
		#ENDIF
		
		HANDLE_BUDDY_PERMANENT_HEAD_TRACK(s_lamar.ped)
		HANDLE_BUDDY_PERMANENT_HEAD_TRACK(s_stretch.ped)
		
		SWITCH i_current_event
			CASE 0 
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GUN", "LEM1_GUN_1", CONV_PRIORITY_MEDIUM)
					i_current_event++
				ENDIF
			BREAK
			
			CASE 1
				//FUDGE: we can return to this stage from later stages if the player gets a wanted level. If in those later stages it printed a "Don't leave x behind message" then don't display it again here.
				TEXT_LABEL str_leave_behind_text
			
				IF NOT b_has_text_label_triggered[LEM1_LEFTBOTH]
					str_leave_behind_text = "LEM1_LEFTBOTH"
				ELSE
					str_leave_behind_text = ""
				ENDIF
			
				//IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(s_locates_data, v_gun_shop_pos + <<0.0, 0.0, 1.5>>, <<0.001, 0.001, 0.001>>, TRUE, s_lamar.ped, s_stretch.ped, NULL, veh_start_car,
				//							     	"LEM1_GOGUN", "LEM1_LEFTLEMAR", "LEM1_LEFTSTRET", "", str_leave_behind_text, "CMN_GENGETIN", "CMN_GENGETBCK", FALSE, TRUE)
				//	e_section_stage = SECTION_STAGE_CLEANUP											
				//ENDIF
				
				//NOTE: the blip position is on the side of the road for GPS only, this blip is hidden and another one created inside the shop.
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ON_FOOT(s_locates_data, <<14.9916, -1126.0948, 27.7036>>, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, FALSE, s_lamar.ped, s_stretch.ped, NULL, 
																"LEM1_GOGUN", "LEM1_LEFTLEMAR", "LEM1_LEFTSTRET", "", str_leave_behind_text, FALSE, TRUE)
					e_section_stage = SECTION_STAGE_CLEANUP											
				ENDIF
			BREAK
		ENDSWITCH
		
		
		
		//Dialogue: only trigger if the player is in a car with both buddies.
		IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
		AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
		AND ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
			IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			ENDIF
			
			//Play the dialogue
			IF i_haircut_dialogue_timer = 0
				i_haircut_dialogue_timer = GET_GAME_TIMER()
			ELIF GET_GAME_TIMER() - i_haircut_dialogue_timer > 1500
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF) AND TIMERA() > 1000
					IF NOT b_has_text_label_triggered[LEM1_HAIR]
						//Check that franklin got his hair cut and it hasn't been commented on in a previous mission.
						IF Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_ACTIVE)
							IF Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_COMPLETE)
								PED_COMP_STYLE_ENUM e_hair_style = GET_PLAYABLE_PED_COMPONENT_STYLE(PLAYER_PED_ID(), COMP_TYPE_HAIR)
								
								IF e_hair_style = COMP_STYLE_SENSIBLE OR e_hair_style = COMP_STYLE_STYLISH
									e_hair_status = STYLE_STATUS_GOOD_CHANGE
								ELSE
									e_hair_status = STYLE_STATUS_BAD_CHANGE
								ENDIF
							ELSE
								e_hair_status = STYLE_STATUS_NO_CHANGE
							ENDIF
						ELSE
							e_hair_status = STYLE_STATUS_DONT_CHECK
						ENDIF
						
						//Check if Franklin changed his clothes and it hasn't been commented on already.
						IF FLOWEVENT_HAS_FRANKLIN_CHANGED_CLOTHES_SINCE_LAMAR_REQUEST()
							PED_COMP_STYLE_ENUM e_clothes_style = GET_PLAYABLE_PED_COMPONENT_STYLE(PLAYER_PED_ID(), COMP_TYPE_TORSO)
							
							IF e_clothes_style = COMP_STYLE_SENSIBLE OR e_clothes_style = COMP_STYLE_STYLISH
								e_clothes_status = STYLE_STATUS_GOOD_CHANGE
							ELSE
								e_clothes_status = STYLE_STATUS_BAD_CHANGE
							ENDIF
						ELSE
							e_clothes_status = STYLE_STATUS_NO_CHANGE
						ENDIF
						
						//Work out which dialogue should be played: Lamar talks about Franklin's clothes and haircut.
						TEXT_LABEL str_car_text_block = ""
						
						IF GET_FAILS_COUNT_WITHOUT_PROGRESS_FOR_THIS_MISSION_SCRIPT() = 0
						AND e_hair_status != STYLE_STATUS_DONT_CHECK
							IF e_hair_status = STYLE_STATUS_NO_CHANGE
								IF e_clothes_status = STYLE_STATUS_NO_CHANGE
									str_car_text_block = "LEM1_HAIR1"
								ELIF e_clothes_status = STYLE_STATUS_BAD_CHANGE
									str_car_text_block = "LEM1_HAIR2"
								ELIF e_clothes_status = STYLE_STATUS_GOOD_CHANGE
									str_car_text_block = "LEM1_HAIR3"
								ENDIF
							ELIF e_hair_status = STYLE_STATUS_BAD_CHANGE
								IF e_clothes_status = STYLE_STATUS_NO_CHANGE
									str_car_text_block = "LEM1_HAIR4"
								ELIF e_clothes_status = STYLE_STATUS_BAD_CHANGE
									str_car_text_block = "LEM1_HAIR5"
								ELIF e_clothes_status = STYLE_STATUS_GOOD_CHANGE
									str_car_text_block = "LEM1_HAIR6"
								ENDIF
							ELIF e_hair_status = STYLE_STATUS_GOOD_CHANGE
								IF e_clothes_status = STYLE_STATUS_NO_CHANGE
									str_car_text_block = "LEM1_HAIR7"
								ELIF e_clothes_status = STYLE_STATUS_BAD_CHANGE
									str_car_text_block = "LEM1_HAIR8"
								ELIF e_clothes_status = STYLE_STATUS_GOOD_CHANGE
									str_car_text_block = "LEM1_HAIR9"
								ENDIF	
							ENDIF
							
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, str_car_text_block, CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_HAIR] = TRUE
							ENDIF
						ELSE
							b_has_text_label_triggered[LEM1_HAIR] = TRUE
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_TOGUN]
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_TOGUN", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_TOGUN] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//If everyone is near the shop then just play the walk to shop dialogue, otherwise pause the in-car convo for later.
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_shop_blip_pos) < 1600.0
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_lamar.ped)) < 400.0
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_stretch.ped)) < 400.0
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
				
				IF NOT b_has_text_label_triggered[LEM1_PRESHOP]
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_PRESHOP", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_PRESHOP] = TRUE
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
			ELSE
				IF b_has_text_label_triggered[LEM1_HAIR]
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			DO_BUDDY_FAILS()
		ENDIF
		
		//1113905 - GPS takes an odd route so force it to the front of the shop.
		IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
			SET_BLIP_ALPHA(s_locates_data.LocationBlip, 0)
			
			IF NOT DOES_BLIP_EXIST(blip_current_destination)
				blip_current_destination = CREATE_BLIP_FOR_COORD(v_shop_blip_pos)
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blip_current_destination)
				REMOVE_BLIP(blip_current_destination)
			ENDIF
		ENDIF
		
		//Continue once the player enters the shop
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<16.693903,-1117.036865,28.796833>>, <<18.689972,-1111.739380,31.797018>>, 3.500000)
		OR IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT)
			//If the shop cutscene didn't activate then progress only if the buddies are nearby.
			IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
			AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF
		
		//Pre-stream the gunshop assets.
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<17.2, -1114.9, 29.6>>) < 22500.0
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_GUN_SHOP_IN_MEMORY, <<0.0, 0.0, 0.0>>)
			REQUEST_ANIM_DICT(str_shop_anims)
			LOAD_SHOP_INTRO_ASSETS(GUN_SHOP_01_DT)
		ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<17.2, -1114.9, 29.6>>) > 40000.0
			IF interior_gun_shop != NULL
				IF IS_INTERIOR_READY(interior_gun_shop)
					UNPIN_INTERIOR(interior_gun_shop)
					interior_gun_shop = NULL
				ENDIF
			ENDIF
			
			REMOVE_ANIM_DICT(str_shop_anims)
			UNLOAD_SHOP_INTRO_ASSETS(GUN_SHOP_01_DT)
		ENDIF
		
		//Pre-stream the cutscene (separated from the other assets for NG B*1845567)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<16.6953, -1116.3615, 28.7962>>) < 400.0
			REQUEST_CUTSCENE("lam_1_mcs_3")
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON)
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS("lam_1_mcs_3")
		ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<16.6953, -1116.3615, 28.7962>>) > 900.0
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
		ENDIF
		
		SWITCH i_audio_scene_event_drive_to_shop
			CASE 0 //Start the scene when the player gets into a car.
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_AMMUNATION")
						START_AUDIO_SCENE("LAMAR_1_GO_TO_AMMUNATION")
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_AMMUNATION")
						STOP_AUDIO_SCENE("LAMAR_1_GO_TO_AMMUNATION")
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		//Fail if the player enters the gun shop with a wanted level.
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<17.634806,-1113.574219,29.814344>>, <<1.500000,1.250000,1.000000>>)
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<21.2064, -1104.5675, 29.6342>>, <<21.0047, -1105.2529, 29.5447>>, 0, TRUE, WEAPONTYPE_PISTOL, PLAYER_PED_ID(), FALSE, FALSE)
				MISSION_FAILED(FAILED_SHOP_CLOSED)
			ENDIF
		ENDIF
		
		//Disable running when near the shop.
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<17.252968,-1116.788330,30.296766>>,<<10.000000,8.000000,2.750000>>)
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP	
		CLEAR_PRINTS()
		
		REMOVE_ALL_BLIPS()
		
		CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
		KILL_FACE_TO_FACE_CONVERSATION()
		
		b_already_checked_for_cars_at_shop = FALSE

		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_SHOP_INTRO_CUTSCENE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_SHOP_INTRO_CUTSCENE)
	ENDIF
ENDPROC

PROC SHOP_INTRO_CUTSCENE()
	IF e_section_stage = SECTION_STAGE_SETUP	
		IF b_is_jumping_directly_to_stage
			IF i_current_event != 99
				IF b_used_a_checkpoint
					START_REPLAY_SETUP(v_inside_gun_shop_pos + <<0.0, 0.0, 0.5>>, 327.4432, FALSE)
					
					i_current_event = 99
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), v_inside_gun_shop_pos + <<0.0, 0.0, 0.5>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 327.4432)
					
					IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_GUN_SHOP_IN_MEMORY, <<0.0, 0.0, 0.0>>)
						LOAD_SCENE(v_inside_gun_shop_pos)
						
						i_current_event = 99
					ENDIF
				ENDIF
			ELSE
				IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, v_lamar_pos_in_shop, f_lamar_heading_in_shop)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, v_stretch_pos_in_shop, f_stretch_heading_in_shop)
				AND SETUP_FRANKLINS_CAR(<<15.3563, -1126.2040, 27.7251>>, 273.6953, TRUE)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_ASSISTED_ROUTES, <<0.0, 0.0, 0.0>>)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
					FORCE_SHOP_RESET(GUN_SHOP_01_DT)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, TRUE)
					
					WAIT(100)
					
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, TRUE)
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, TRUE)
					SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
					SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
					
					END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), v_inside_gun_shop_pos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 327.4432)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					
					b_already_checked_for_cars_at_shop = TRUE
					b_is_jumping_directly_to_stage = FALSE
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
			ENDIF
		
			REQUEST_CUTSCENE("lam_1_mcs_3")
			REQUEST_ANIM_DICT(str_shop_anims)
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS("lam_1_mcs_3")
		
			#IF IS_DEBUG_BUILD
				IF NOT LOAD_SHOP_INTRO_ASSETS(GUN_SHOP_01_DT)
					PRINTLN("lamar1.sc - waiting for shop assets.")
				ENDIF
				
				IF NOT STORE_SHOP_OWNER()
					PRINTLN("lamar1.sc - waiting for shop owner.")
				ENDIF
				
				IF NOT HAS_CUTSCENE_LOADED()
					PRINTLN("lamar1.sc - waiting for cutscene.")
				ENDIF
			#ENDIF
		
			IF LOAD_SHOP_INTRO_ASSETS(GUN_SHOP_01_DT)
			AND STORE_SHOP_OWNER()
			AND HAS_ANIM_DICT_LOADED(str_shop_anims)
			AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			//AND IS_SRL_LOADED()
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
				CANCEL_BUDDY_PERMANENT_HEAD_TRACK(s_lamar.ped)
				CANCEL_BUDDY_PERMANENT_HEAD_TRACK(s_stretch.ped)
			
				IF NOT IS_PED_INJURED(s_lamar.ped)
					IF IS_PED_IN_GROUP(s_lamar.ped)
						REMOVE_PED_FROM_GROUP(s_lamar.ped)
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(s_lamar.ped, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(s_stretch.ped)
					IF IS_PED_IN_GROUP(s_stretch.ped)
						REMOVE_PED_FROM_GROUP(s_stretch.ped)
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(s_stretch.ped, "Stretch", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF				
				
				IF NOT IS_PED_INJURED(ped_shop_owner)
					REGISTER_ENTITY_FOR_CUTSCENE(ped_shop_owner, "GunShopKeeper", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				START_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				REMOVE_ALL_AUDIO_SCENES()
				
				b_skipped_mocap = FALSE
				i_current_event = 0
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING		
		IF NOT b_skipped_mocap
			IF WAS_CUTSCENE_SKIPPED()
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				b_skipped_mocap = TRUE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	
		IF IS_CUTSCENE_PLAYING()
			#IF IS_DEBUG_BUILD
				i_debug_cutscene_timer = GET_CUTSCENE_TIME()
			#ENDIF	
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_AMMUNATION_INTRO_CUTSCENE")
				START_AUDIO_SCENE("LAMAR_1_AMMUNATION_INTRO_CUTSCENE")
			ENDIF
		
			IF NOT b_already_checked_for_cars_at_shop
				b_already_checked_for_cars_at_shop = MOVE_LAST_CAR_AWAY_FROM_SHOP()
			ENDIF
		
			SWITCH i_current_event
				CASE 0
					VEHICLE_INDEX veh_last_car
					veh_last_car = GET_PLAYERS_LAST_VEHICLE()
					
					IF IS_VEHICLE_DRIVEABLE(veh_last_car)
						SET_VEHICLE_DOORS_SHUT(veh_last_car)
						OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(veh_last_car)
						
						//IF NOT IS_ENTITY_A_MISSION_ENTITY(veh_last_car)
						//	SET_ENTITY_AS_MISSION_ENTITY(veh_last_car, TRUE, TRUE)
						//ENDIF
						
						//SET_MISSION_VEHICLE_GEN_VEHICLE(veh_last_car, <<-700.7425, 5270.6050, 74.4262>>, 229.0238)
					ENDIF
				
					STOP_FIRE_IN_RANGE(<<17.9, -1115.0, 29.3>>, 50.0)
					CLEAR_AREA_OF_PROJECTILES(<<17.9, -1115.0, 29.3>>, 50.0)
					CLEAR_AREA_OF_VEHICLES(<<17.9, -1115.0, 29.3>>, 50.0, TRUE)
					REMOVE_PARTICLE_FX_IN_RANGE(<<17.9, -1115.0, 29.3>>, 50.0)
					
					//IF NOT IS_PED_INJURED(ped_shop_owner)
					//	SET_ENTITY_VISIBLE(ped_shop_owner, FALSE)
					//ENDIF
					
					//If the area is still blocked after checking for cars then check the player's car too.
					IF IS_VEHICLE_DRIVEABLE(veh_start_car)
						SET_VEHICLE_DOORS_SHUT(veh_start_car)
					
						IF IS_ENTITY_IN_ANGLED_AREA(veh_start_car, <<17.350292, -1115.116943, 27.796766>>, <<15.640124, -1119.682861, 31.820522>>, 6.0)
							SET_ENTITY_HEADING(veh_start_car, 229.0238)
							SET_ENTITY_COORDS(veh_start_car, <<-700.7425, 5270.6050, 74.4262>>)
						ENDIF
					ENDIF
					
					IF GET_CLOCK_HOURS() < 21 AND GET_CLOCK_HOURS() > 4
						SET_CLOCK_TIME(20, 45, 0)
					ENDIF
					
					SETTIMERB(0)
					
					DO_FADE_IN_WITH_WAIT()
					i_current_event++
				BREAK
				
				CASE 1
					DISPLAY_SHOP_INTRO_MESSAGE(GUN_SHOP_01_DT, "SHOP_INTRO")
				
					IF TIMERB() > 8000
						SETTIMERB(0)
						i_current_event++
					ENDIF
				BREAK
				
				CASE 2
					DISPLAY_SHOP_INTRO_MESSAGE(GUN_SHOP_01_DT, "GS_INTRO_0")
					
					IF TIMERB() > 8000
						SETTIMERB(0)
						i_current_event++
					ENDIF
				BREAK
				
				CASE 3
					DISPLAY_SHOP_INTRO_MESSAGE(GUN_SHOP_01_DT, "GS_INTRO_1")
					
					IF TIMERB() > 8000
						CLEAR_HELP()
						SETTIMERB(0)
					
						i_current_event++
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	
		IF NOT IS_CUTSCENE_ACTIVE()
			e_section_stage = SECTION_STAGE_CLEANUP
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar")
			IF NOT IS_PED_INJURED(s_lamar.ped)
				//SET_ENTITY_COORDS(s_lamar.ped, v_lamar_pos_in_shop)
				//SET_ENTITY_HEADING(s_lamar.ped, f_lamar_heading_in_shop)
				TASK_PLAY_ANIM(s_lamar.ped, str_shop_anims, "idle_lamar", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(s_lamar.ped)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Stretch")
			IF NOT IS_PED_INJURED(s_stretch.ped)
				SET_ENTITY_COORDS(s_stretch.ped, v_stretch_pos_in_shop)
				SET_ENTITY_HEADING(s_stretch.ped, f_stretch_heading_in_shop)
				TASK_PLAY_ANIM(s_stretch.ped, str_shop_anims, "idle_stretch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(s_stretch.ped)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("GunShopKeeper")
			IF NOT IS_PED_INJURED(ped_shop_owner)
				SET_ENTITY_COORDS(ped_shop_owner, <<23.7600, -1105.6639, 28.7970>>) //<<23.530, -1105.3840, 28.797>>)
				SET_ENTITY_HEADING(ped_shop_owner, 142.49)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			CLEAR_HELP()
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		If b_skipped_mocap
			//If the player skipped the mocap the peds need to be warped.
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			//Shouldn't need this, since the exit states are always hit if you skip a cutscene
			IF NOT IS_PED_INJURED(s_lamar.ped)
				SET_ENTITY_COORDS(s_lamar.ped, v_lamar_pos_in_shop)
				SET_ENTITY_HEADING(s_lamar.ped, f_lamar_heading_in_shop)
				TASK_PLAY_ANIM(s_lamar.ped, str_shop_anims, "idle_lamar", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
				SET_ENTITY_COORDS(s_stretch.ped, v_stretch_pos_in_shop)
				SET_ENTITY_HEADING(s_stretch.ped, f_stretch_heading_in_shop)
				TASK_PLAY_ANIM(s_stretch.ped, str_shop_anims, "idle_stretch", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
			ENDIF
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<19.5879, -1109.1584, 28.7970>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 338.6990)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF	
		
		IF NOT IS_PED_INJURED(ped_shop_owner)
			SET_ENTITY_VISIBLE(ped_shop_owner, TRUE)
			//REMOVE_PED(ped_shop_owner)
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		UNLOAD_SHOP_INTRO_ASSETS(GUN_SHOP_01_DT)

		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_BUY_GRENADES
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP	
		STOP_CUTSCENE()
		b_skipped_mocap = TRUE
		
		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

PROC BUY_GRENADES()
	IF e_section_stage = SECTION_STAGE_SETUP	
		IF b_is_jumping_directly_to_stage
			IF b_used_a_checkpoint
				WHILE IS_SHOP_BIT_SET(GUN_SHOP_01_DT, SHOP_NS_FLAG_FORCE_RESET, TRUE)
				OR NOT IS_SHOP_OPEN_FOR_BUSINESS(GUN_SHOP_01_DT)
					PRINTLN("BUY_GRENADES() - waiting for shop to initialise.")
					WAIT(0)
				ENDWHILE
				
				START_REPLAY_SETUP(<<19.6422, -1109.1816, 28.7970>>, 324.8243, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<19.6422, -1109.1816, 28.7970>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 324.8243)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				SETTIMERA(0)
				
				//Load the shop interior: should only need a small sphere and high detail only.
				WHILE NOT NEW_LOAD_SCENE_START_SPHERE(<<20.3, -1110.2, 30.4>>, 10.0)
					WAIT(0)
				ENDWHILE
				
				WHILE TIMERA() < 10000
					IF IS_NEW_LOAD_SCENE_LOADED()
						#IF IS_DEBUG_BUILD
							PRINTLN("New load scene completed: ", TIMERA())
						#ENDIF
					
					    SETTIMERA(100000)
					ENDIF

					WAIT(0)
				ENDWHILE	
			ENDIF
			
			WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_GUN_SHOP_IN_MEMORY, <<0.0, 0.0, 0.0>>)
			OR NOT DOES_ENTITY_EXIST(s_lamar.ped)
			OR NOT DOES_ENTITY_EXIST(s_stretch.ped)
			OR NOT DOES_ENTITY_EXIST(veh_start_car)
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, v_lamar_pos_in_shop, f_lamar_heading_in_shop)
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, v_stretch_pos_in_shop, f_stretch_heading_in_shop)
				SETUP_FRANKLINS_CAR(<<15.3563, -1126.2040, 27.7251>>, 273.6953, TRUE)
			
				WAIT(0)
			ENDWHILE
			
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_ASSISTED_ROUTES, <<0.0, 0.0, 0.0>>)
			
			FORCE_SHOP_RESET(GUN_SHOP_01_DT)
			SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, TRUE)
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERB(0)
				
			//Wait for clothes to stream in
			WHILE TIMERB() < 10000
				IF (NOT IS_PED_INJURED(s_lamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped))
				AND (NOT IS_PED_INJURED(s_stretch.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped))
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					SETTIMERB(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			UPDATE_BUDDY_INSIDE_SHOP(s_lamar, v_lamar_pos_in_shop, f_lamar_heading_in_shop, "idle_lamar")
			UPDATE_BUDDY_INSIDE_SHOP(s_stretch, v_stretch_pos_in_shop, f_stretch_heading_in_shop, "idle_stretch")
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<19.6422, -1109.1816, 28.7970>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 324.8243)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			SET_CLOCK_TIME(20, 45, 0)
			
			WAIT(1000) //Wait for the shop props to appear.
			
			b_already_checked_for_cars_at_shop = TRUE
			b_is_jumping_directly_to_stage = FALSE
		ELSE
			i_previous_funds = GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM())
			
			IF NOT IS_PED_INJURED(s_lamar.ped)
				IF IS_PED_IN_GROUP(s_lamar.ped)
					REMOVE_PED_FROM_GROUP(s_lamar.ped)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
				IF IS_PED_IN_GROUP(s_stretch.ped)
					REMOVE_PED_FROM_GROUP(s_stretch.ped)
				ENDIF
			ENDIF
			
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
				IF IS_PLAYER_PED_WEAPON_COMP_PURCHASED(CHAR_FRANKLIN, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH)
					GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH)
				ENDIF
			ENDIF
			
			b_player_has_browsed_shop = FALSE
			b_player_has_bought_something = FALSE
			b_given_player_money = FALSE
			b_player_already_has_colour_mod = FALSE
			b_player_left_shop_too_early = FALSE
			b_player_already_has_shotgun = HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
			
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
				IF GET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN) > 0
					b_player_already_has_colour_mod = TRUE
				ENDIF
			ENDIF
			
			KILL_ANY_CONVERSATION() //Fix for some dialogue getting left over if the player goes to the clothes shop first.
			
			SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, TRUE)
			SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, TRUE)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
			
			b_has_text_label_triggered[LEM1_LEFTBOTH] = FALSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_GO_INSIDE_GUN_SHOP, "BUY_GRENADES")
			
			NEW_LOAD_SCENE_STOP() //From Z-skip.
			
			IF IS_SCREEN_FADED_OUT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
				WAIT(250)
				
				DO_FADE_IN_WITH_WAIT()
			ENDIF
			
			IF HAS_SHOP_RUN_ENTRY_INTRO(GUN_SHOP_01_DT)	
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			i_current_event = 0
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		UPDATE_BUDDY_INSIDE_SHOP(s_lamar, v_lamar_pos_in_shop, f_lamar_heading_in_shop, "idle_lamar")
		UPDATE_BUDDY_INSIDE_SHOP(s_stretch, v_stretch_pos_in_shop, f_stretch_heading_in_shop, "idle_stretch")
	
		STRING str_buy_god_text, str_go_back_god_text, str_buy_convo
		INT i_min_cost, i_shotgun_cost, i_flashlight_cost, i_shotgun_ammo_cost
		BOOL b_cash_text_just_triggered
		
		BOOL bDummy
		GET_GUNSHOP_WEAPON_COST(WEAPONTYPE_KNIFE, i_min_cost, i_min_cost, bDummy)
		GET_GUNSHOP_WEAPON_COST(WEAPONTYPE_PUMPSHOTGUN, i_shotgun_cost, i_shotgun_ammo_cost, bDummy)
		i_flashlight_cost = GET_GUNSHOP_WEAPON_COMPONENT_COST(WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH)

		IF HAS_SHOP_RUN_ENTRY_INTRO(GUN_SHOP_01_DT)			
			SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_PISTOL)
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_AMMUNATION_INTRO_CUTSCENE")
					STOP_AUDIO_SCENE("LAMAR_1_AMMUNATION_INTRO_CUTSCENE")
				ENDIF
			ENDIF			
			
			//Checks for if the player has bought weapons and is in the shop: these are used to trigger a number of different events.
			BOOL b_has_bought_shotgun = HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
			BOOl b_has_bought_flashlight = HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH)
			BOOL b_player_is_in_shop = IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT)
			BOOL b_lamar_is_in_shop = IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<17.558983,-1114.357544,28.797029>>, <<22.788731,-1104.444336,32.098419>>, 6.250000)
			BOOL b_stretch_is_in_shop = IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<17.558983,-1114.357544,28.797029>>, <<22.788731,-1104.444336,32.098419>>, 6.250000)
			BOOL b_player_is_browsing = b_player_is_in_shop AND IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_01_DT)
			INT i_current_funds = GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM())
			
			//Disable subtitles if the player is browsing.
			IF b_player_is_browsing
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
				OR NOT IS_MESSAGE_BEING_DISPLAYED()
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				ENDIF
			ENDIF
			
			//Flag once the player has browsed the shop at least once: this indicates we can continue the mission.
			IF NOT b_player_has_browsed_shop
			AND b_player_is_browsing
				b_player_has_browsed_shop = TRUE
			ENDIF
			
			//Flag if the player leaves the shop too early, this is used to play dialogue later once they return.
			IF NOT b_player_left_shop_too_early
			AND NOT b_player_is_in_shop
				b_player_left_shop_too_early = TRUE
			ENDIF
			
			//Move any cars blocking the shop away (if we didn't successfully do it in the previous stage).
			IF NOT b_already_checked_for_cars_at_shop
				b_already_checked_for_cars_at_shop = MOVE_LAST_CAR_AWAY_FROM_SHOP()
			ENDIF
			
			//Check once the player spends any money in the shop at all.
			IF NOT b_player_has_bought_something
				IF b_player_is_browsing AND i_current_funds < i_previous_funds
					b_player_has_bought_something = TRUE
				ENDIF
			ENDIF
			
			//Work out how much cash the player needs to progress the mission, based on which weapons they already have.
			INT i_required_cash = 0
			
			IF NOT b_has_bought_shotgun
				i_required_cash += i_shotgun_cost 
			ENDIF
			
			IF NOT b_has_bought_flashlight
				i_required_cash += i_flashlight_cost
			ENDIF
			
			IF NOT b_player_has_bought_something
				i_required_cash += i_min_cost
			ENDIF
			
			//Work out which text labels need to be used for dialogue/god text based on which weapons the player has
			IF b_has_bought_flashlight
				IF b_has_bought_shotgun
					str_buy_god_text = "LEM1_BUYANY"
					str_go_back_god_text = "LEM1_BACKSHOP"
					str_buy_convo = "LEM1_BUYN"
				ELSE
					str_buy_god_text = ""
					str_go_back_god_text = "LEM1_BACKSHOP2"
					str_buy_convo = "LEM1_BUYN"
				ENDIF
			ELSE
				IF b_has_bought_shotgun
					str_buy_god_text = "LEM1_BUYFLSH"
					str_go_back_god_text = "LEM1_BACKSHOP3"
					str_buy_convo = "LEM1_SHOTTY2"
				ELSE
					str_buy_god_text = "LEM1_BUYSHOT"
					str_go_back_god_text = "LEM1_BACKSHOP2"
					str_buy_convo = "LEM1_SHOTTY"
				ENDIF
			ENDIF
			
			//Dialogue and god text
			IF NOT b_has_text_label_triggered[LEM1_BROKE_1]
				IF NOT b_has_text_label_triggered[LEM1_BUYN] //Lamar tells Franklin what to buy.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, str_buy_convo, CONV_PRIORITY_MEDIUM)
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 3.5)
							b_has_text_label_triggered[LEM1_BUYN] = TRUE							
						ENDIF
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_LMONEY]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_PLAYER_USING_A_VENDING_MACHINE()
							IF i_current_funds < 1500
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_LMONEY", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_LMONEY] = TRUE		
									b_cash_text_just_triggered = TRUE
								ENDIF
							ELSE
								b_has_text_label_triggered[LEM1_LMONEY] = TRUE	
								b_given_player_money = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_BUYALL] //God text tells Franklin what to buy, varies based on weapons the player already has.
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						IF b_has_bought_flashlight
						AND b_has_bought_shotgun
						AND b_player_has_bought_something
							b_has_text_label_triggered[LEM1_BUYALL] = TRUE
						ELSE
							PRINT_NOW(str_buy_god_text, DEFAULT_GOD_TEXT_TIME, 0)
							b_has_text_label_triggered[LEM1_BUYALL] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Give the player money if they don't have enough: coincide this with the right dialogue line.
			IF NOT b_given_player_money
				IF b_has_text_label_triggered[LEM1_LMONEY]
					TEXT_LABEL_23 str_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
					IF ARE_STRINGS_EQUAL(str_root, "LEM1_LMONEY")
						TEXT_LABEL_23 str_label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
					
						IF ARE_STRINGS_EQUAL(str_label, "LEM1_LMONEY_2")
							CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 1500 - i_current_funds)
							PRINTLN("Lamar 1 - Player is given money ... i_current_funds = ", i_current_funds)
							i_current_funds = 1500
							b_given_player_money = TRUE
						ENDIF
					ELSE
						IF NOT b_cash_text_just_triggered
							CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 1500 - i_current_funds)
							PRINTLN("Lamar 1 - Player is given money ... i_current_funds = ", i_current_funds)
							i_current_funds = 1500
							b_given_player_money = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF b_player_has_bought_something
			AND b_has_bought_flashlight
			AND b_has_bought_shotgun
				//Turn off ragdoll on buddies so they don't get knocked over on leaving (this causes issues with the next stage).
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_lamar.ped, FALSE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_stretch.ped, FALSE)
			
				IF NOT b_has_text_label_triggered[LEM1_SHOP_1] //Dialogue from Lamar once player buys all the weapons.
					IF b_player_is_browsing
						IF HAS_ANIM_DICT_LOADED(str_shop_anims)
						AND IS_IT_SAFE_TO_PLAY_SHOP_ANIMS(b_player_is_browsing)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SHOP", CONV_PRIORITY_HIGH)
								b_has_text_label_triggered[LEM1_SHOP_1] = TRUE
								
								PLAY_BUDDY_SHOP_ANIMS("lam_ig_1_p4_lamar", "lam_ig_1_p4_stretch", b_player_is_browsing)
							ENDIF
						ENDIF
					ELSE
						//If the player already quit the menu then just progress the mission instead of waiting.
						KILL_FACE_TO_FACE_CONVERSATION()
						b_has_text_label_triggered[LEM1_SHOP_1] = TRUE
					ENDIF
				ELIF NOT b_player_is_browsing	
					IF NOT DOES_BLIP_EXIST(blip_current_destination)
						blip_current_destination = CREATE_BLIP_FOR_COORD(v_meeting_pos, TRUE)
					ENDIF
				
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data)
						e_section_stage = SECTION_STAGE_CLEANUP
					ENDIF
					
					//If the player leaves really quickly, just progress the mission.
					IF NOT IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT)
						KILL_FACE_TO_FACE_CONVERSATION()
						e_section_stage = SECTION_STAGE_CLEANUP
					ENDIF
				ENDIF
			ELSE
				//Handle events if the player leaves the weapons shop and hasn't bought the goods yet.
				IF NOT b_player_is_in_shop
				AND i_current_funds >= i_required_cash
					IF NOT IS_PED_INJURED(s_lamar.ped)
					AND NOT IS_PED_INJURED(s_stretch.ped)
						FLOAT f_lamar_dist = VDIST2(GET_ENTITY_COORDS(s_lamar.ped), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						FLOAT f_stretch_dist = VDIST2(GET_ENTITY_COORDS(s_stretch.ped), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						
						IF f_lamar_dist > 2500.0
						OR f_stretch_dist > 2500.0
							//If the player leaves the area tell them to not abandon the buddies.
							IF NOT DOES_BLIP_EXIST(s_lamar.blip)
								s_lamar.blip = CREATE_BLIP_FOR_ENTITY(s_lamar.ped)
							ENDIF
							
							IF NOT DOES_BLIP_EXIST(s_stretch.blip)
								s_stretch.blip = CREATE_BLIP_FOR_ENTITY(s_stretch.ped)
							ENDIF
							
							IF DOES_BLIP_EXIST(blip_current_destination)
								REMOVE_BLIP(blip_current_destination)
							ENDIF
							
							IF NOT b_has_text_label_triggered[LEM1_LEFTBOTH]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									PRINT_NOW("LEM1_LEFTBOTH", DEFAULT_GOD_TEXT_TIME, 0)
									b_has_text_label_triggered[LEM1_LEFTBOTH] = TRUE
								ENDIF
							ENDIF
						ELSE
							//Tell the player to get back in the shop
							IF f_lamar_dist < 1600.0
							AND f_stretch_dist < 1600.0
								IF DOES_BLIP_EXIST(s_lamar.blip)
									REMOVE_BLIP(s_lamar.blip)
								ENDIF
							
								IF DOES_BLIP_EXIST(s_stretch.blip)
									REMOVE_BLIP(s_stretch.blip)
								ENDIF
								
								IF IS_THIS_PRINT_BEING_DISPLAYED("LEM1_LEFTBOTH")	
								OR (NOT IS_STRING_NULL_OR_EMPTY(str_buy_god_text) AND IS_THIS_PRINT_BEING_DISPLAYED(str_buy_god_text))
									CLEAR_PRINTS()
								ENDIF
								
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									IF NOT b_has_text_label_triggered[LEM1_BACKSHOP]
										PRINT_NOW(str_go_back_god_text, DEFAULT_GOD_TEXT_TIME, 0)
										b_has_text_label_triggered[LEM1_BACKSHOP] = TRUE
									ENDIF
								ENDIF
								
								IF NOT DOES_BLIP_EXIST(blip_current_destination)
									blip_current_destination = CREATE_BLIP_FOR_COORD(<<22.1, -1106.6, 29.8>>, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blip_current_destination)
						REMOVE_BLIP(blip_current_destination)
					ENDIF
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("LEM1_BACKSHOP")
						CLEAR_PRINTS()
					ENDIF
				ENDIF
				
				IF b_player_is_browsing
					//Clear the "go back in" god text if the player re-enters the shop and goes to the counter
					IF IS_THIS_PRINT_BEING_DISPLAYED(str_go_back_god_text)
						CLEAR_PRINTS()
					ENDIF
					
					//Play a line when the player correctly highlights the shotgun.
					IF NOT b_has_bought_shotgun
					AND NOT b_has_text_label_triggered[LEM1_SFOUND]
						IF GET_SELECTED_GUNSHOP_WEAPON() = WEAPONTYPE_PUMPSHOTGUN
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SFOUND", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_SFOUND] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Play a line when the player buys the shotgun and needs the flashlight.
					IF NOT b_player_already_has_shotgun
					AND b_has_bought_shotgun
					AND NOT b_has_bought_flashlight
					AND NOT b_has_text_label_triggered[LEM1_SFLASH]
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SFLASH", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_SFLASH] = TRUE
						ENDIF
					ENDIF
					
					//Stretch moans at the player if he takes ages.
					IF GET_GAME_TIMER() - i_stretch_shop_moan_timer > 15000
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND i_current_funds >= i_required_cash
						//IF NOT b_has_bought_shotgun
						//	IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SMOAN1", CONV_PRIORITY_MEDIUM)
						//		i_stretch_shop_moan_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(-2000, 2000)
						//	ENDIF
						//ELSE
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SMOAN3", CONV_PRIORITY_MEDIUM)
								i_stretch_shop_moan_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(-2000, 2000)
							ENDIF
						//ENDIF
					ENDIF
				ELSE
					//Have a reminder if the player goes to leave.
					IF i_current_funds >= i_required_cash
					AND NOT b_has_text_label_triggered[LEM1_SMOAN3]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<19.939650,-1114.849731,28.797018>>, <<15.612720,-1113.192383,31.334002>>, 4.000000) OR NOT b_player_is_in_shop)
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SMOAN3", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_SMOAN3] = TRUE
						ENDIF
					ENDIF
				
					i_stretch_shop_moan_timer = GET_GAME_TIMER()

					//Clear any floating help
					IF IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
						CLEAR_ALL_FLOATING_HELP()
					ENDIF
				ENDIF
				
				//Keep track of changes in money each frame, as well as if the player bought one of the required weapons this frame.
				i_previous_funds = i_current_funds
			
				//Do banter if everyone is in the shop
				IF NOT IS_PED_INJURED(s_lamar.ped)
				AND NOT IS_PED_INJURED(s_stretch.ped)
					IF b_player_is_in_shop
					AND b_lamar_is_in_shop
					AND b_stretch_is_in_shop
					AND HAS_ANIM_DICT_LOADED(str_shop_anims)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF GET_GAME_TIMER() - i_shop_banter_timer > 10000
							AND b_has_text_label_triggered[LEM1_LMONEY] //Don't do banter until after the give money line.
								IF IS_IT_SAFE_TO_PLAY_SHOP_ANIMS(b_player_is_browsing)
									IF NOT b_has_text_label_triggered[LEM1_SHOP1]
										IF NOT IS_PLAYER_USING_A_VENDING_MACHINE()
											IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SHOP1", CONV_PRIORITY_MEDIUM)
												b_has_text_label_triggered[LEM1_SHOP1] = TRUE
											ENDIF
										ENDIF
									ELIF NOT b_has_text_label_triggered[LEM1_SHOP2]
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SHOP2", CONV_PRIORITY_MEDIUM)
											PLAY_BUDDY_SHOP_ANIMS("lam_ig_1_p1_lamar", "lam_ig_1_p1_stretch", b_player_is_browsing)
										
											b_has_text_label_triggered[LEM1_SHOP2] = TRUE
										ENDIF
									ELIF NOT b_has_text_label_triggered[LEM1_SHOP3]
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SHOP3", CONV_PRIORITY_MEDIUM)
											PLAY_BUDDY_SHOP_ANIMS("lam_ig_1_p2_lamar", "lam_ig_1_p2_stretch", b_player_is_browsing)
										
											b_has_text_label_triggered[LEM1_SHOP3] = TRUE
										ENDIF
									ELIF NOT b_has_text_label_triggered[LEM1_SHOP6]
										IF NOT IS_PLAYER_USING_A_VENDING_MACHINE()
											IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SHOP6", CONV_PRIORITY_MEDIUM)
												PLAY_BUDDY_SHOP_ANIMS("lam_ig_1_p3_lamar", "lam_ig_1_p3_stretch", b_player_is_browsing)
											
												b_has_text_label_triggered[LEM1_SHOP6] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//Play some dialogue when the player returns after leaving the shop too early.
							IF NOT b_has_text_label_triggered[LEM1_SRETURN]
								IF b_player_left_shop_too_early
								AND NOT b_player_is_browsing
								AND i_current_funds >= i_required_cash
									IF b_has_bought_shotgun
									AND NOT b_has_bought_flashlight
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SRETURN", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_SRETURN] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//Random "look at this" dialogue from Stretch.
							IF NOT b_has_text_label_triggered[LEM1_STUFF]
								IF IS_ENTITY_PLAYING_ANIM(s_lamar.ped, str_shop_anims, "idle_stretch")
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_STUFF", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_STUFF] = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							//Play some dialogue after the player buys a gun without ammo.
							/*IF NOT b_has_text_label_triggered[LEM1_SAMMO]
								IF b_has_bought_shotgun
								AND i_current_funds >= i_required_cash
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SAMMO", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_SAMMO] = TRUE
									ENDIF
								ENDIF
							ENDIF*/
						ELIF b_has_text_label_triggered[LEM1_SHOP1] //Only start using the timer after the first line
							i_shop_banter_timer = GET_GAME_TIMER() - GET_RANDOM_INT_IN_RANGE(2000, 4000)
						ENDIF
					ELIF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//Kill conversations and stop anims if the player leaves the area
						IF b_has_text_label_triggered[LEM1_SHOP1]
							KILL_FACE_TO_FACE_CONVERSATION()
							CLEAR_PED_TASKS(s_lamar.ped)
							CLEAR_PED_TASKS(s_stretch.ped)
						ENDIF	
					ENDIF
				
					IF b_has_text_label_triggered[LEM1_BUYALL]
						//If the player doesn't have enough cash there are two options:
						// - Fail the mission if the player didn't buy the shotgun and flashlight.
						// - Progress with dialogue if the player already had those items but didn't buy anything extra for the tutorial.
						IF i_current_funds < i_required_cash
						AND b_given_player_money
							IF b_has_bought_shotgun AND b_has_bought_flashlight
								IF NOT b_has_text_label_triggered[LEM1_BROKE_1]
									IF HAS_ANIM_DICT_LOADED(str_shop_anims)
									AND IS_IT_SAFE_TO_PLAY_SHOP_ANIMS(b_player_is_browsing)
									AND NOT IS_PLAYER_USING_A_VENDING_MACHINE()
									AND CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_BROKE", CONV_PRIORITY_HIGH)
										b_has_text_label_triggered[LEM1_BROKE_1] = TRUE
										
										PLAY_BUDDY_SHOP_ANIMS("lam_ig_1_p5_lamar", "lam_ig_1_p5_stretch", b_player_is_browsing)
									ENDIF
								ELIF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data)
									IF NOT b_player_is_browsing
										e_section_stage = SECTION_STAGE_CLEANUP
									ENDIF
								ENDIF
							ELSE
								MISSION_FAILED(FAILED_DIDNT_BUY_SHOTGUN)
							ENDIF
						ENDIF
					ENDIF
					
					
					//Play a line if you change the weapon colour.
					IF NOT b_player_already_has_colour_mod
					AND NOT b_has_text_label_triggered[LEM1_COLOR]
						IF b_player_is_browsing
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
								IF GET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN) > 0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COLOR", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_COLOR] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//If the player gets a wanted level at this stage then go back to the first stage and force them to lose their wanted level.
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			AND i_current_funds >= i_required_cash
				CLEAR_PRINTS()
				REMOVE_ALL_BLIPS()
				KILL_FACE_TO_FACE_CONVERSATION()
				CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
				DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(s_locates_data, TRUE)
				
				i_current_event = 1
				e_mission_stage = STAGE_GO_TO_GUN_SHOP
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
			
			//If the player is using a vending machine then kill any conversations that might involve franklin.
			IF IS_PLAYER_USING_A_VENDING_MACHINE()
				TEXT_LABEL_23 str_current_label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
				IF ARE_STRINGS_EQUAL(str_current_label, "LEM1_LMONEY")
				OR ARE_STRINGS_EQUAL(str_current_label, "LEM1_SHOP1")
				OR ARE_STRINGS_EQUAL(str_current_label, "LEM1_SHOP6")
				OR ARE_STRINGS_EQUAL(str_current_label, "LEM1_BROKE")
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
		
		DO_BUDDY_FAILS()
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		//CLEAR_PRINTS()
		CLEAR_HELP()
		CLEAR_ALL_FLOATING_HELP()
		REMOVE_ALL_BLIPS()

		IF interior_gun_shop != NULL
			IF IS_INTERIOR_READY(interior_gun_shop)
				UNPIN_INTERIOR(interior_gun_shop)
				interior_gun_shop = NULL
			ENDIF
		ENDIF

		SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_INVALID)
		REMOVE_ALL_AUDIO_SCENES()

		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_GO_TO_PLANT
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP	
		JUMP_TO_STAGE(STAGE_GO_TO_PLANT)
	ENDIF
ENDPROC

PROC GO_TO_PLANT()
	IF e_section_stage = SECTION_STAGE_SETUP	
		IF b_is_jumping_directly_to_stage
			IF b_used_a_checkpoint
				START_REPLAY_SETUP(<<16.1696, -1119.9182, 27.8357>>, 182.5737)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<16.1696, -1119.9182, 27.8357>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 182.5737)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				WAIT(0)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			WHILE NOT DOES_ENTITY_EXIST(s_lamar.ped)
			OR NOT DOES_ENTITY_EXIST(s_stretch.ped)
			OR NOT DOES_ENTITY_EXIST(veh_start_car)
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<14.6420, -1120.8765, 27.7931>>, 321.7365)
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<18.2288, -1120.6470, 27.9195>>, 88.0996)
				SETUP_FRANKLINS_CAR(<<15.3563, -1126.2040, 27.7251>>, 273.6953, TRUE)
				
				WAIT(0)
			ENDWHILE
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERB(0)
				
			//Wait for clothes to stream in
			WHILE TIMERB() < 10000
				IF (NOT IS_PED_INJURED(s_lamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped))
				AND (NOT IS_PED_INJURED(s_stretch.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped))
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					SETTIMERB(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<16.1696, -1119.9182, 27.8357>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 182.5737)
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_ASSISTED_ROUTES, <<0.0, 0.0, 0.0>>)
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
			SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, TRUE)
			SETUP_REQ_FRANKLIN_HAS_WEAPONS_ON_Z_SKIP()
			
			SET_CLOCK_TIME(21, 30, 0)
			
			i_current_event = 99
			b_is_jumping_directly_to_stage = FALSE
		ELSE
			//If jumped to this stage don't do first bit, as everyone will already be outside the weapons shop.
			IF IS_SCREEN_FADED_OUT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				CLEAR_AREA_OF_PEDS(<<16.1696, -1119.9182, 27.8357>>, 100.0)
			
				b_player_left_shop = TRUE
			ELSE
				b_player_left_shop = FALSE
			ENDIF
		
			b_player_rode_bike_into_plant = FALSE
			b_did_a_timelapse_at_plant = FALSE
			b_player_been_in_car = FALSE
			i_meeting_dialogue_event = 0
			i_drive_to_plant_dialogue_timer = 0
		
			b_has_text_label_triggered[CMN_GENGETIN] = FALSE
			
			//Unlock the gun shops at this stage of the mission
			SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_GUN_01_DT, FALSE)
			SET_ALL_NON_MISSION_GUN_SHOPS_OPEN(TRUE)
			
			IF NOT b_gun_shops_were_already_open
				//1770823 - The weapons shop now always opens before this mission so there's no reason to display this help text.
				//PRINT_HELP("LEM1_SHOPOPEN")
			ENDIF
		
			SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, TRUE)
			SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, TRUE)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_UNLOCKED)
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_GO_TO_PLANT, "GO_TO_PLANT")
		
			DO_FADE_IN_WITH_WAIT()
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			SETTIMERB(0)
			i_current_event = 0
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		#IF IS_DEBUG_BUILD
			DONT_DO_J_SKIP(s_locates_data)
		#ENDIF
		
		HANDLE_BUDDY_PERMANENT_HEAD_TRACK(s_lamar.ped)
		HANDLE_BUDDY_PERMANENT_HEAD_TRACK(s_stretch.ped)
		
		REQUEST_WAYPOINT_RECORDING(str_waypoint_stairs)
		
		IF NOT IS_PED_INJURED(s_lamar.ped)
		AND NOT IS_PED_INJURED(s_stretch.ped)
			IF IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT)
				SET_PED_MAX_MOVE_BLEND_RATIO(s_lamar.ped, PEDMOVE_WALK)
				SET_PED_MAX_MOVE_BLEND_RATIO(s_stretch.ped, PEDMOVE_WALK)
			ELSE
				IF NOT b_player_left_shop
					SET_PED_MAX_MOVE_BLEND_RATIO(s_lamar.ped, PEDMOVE_RUN)
					SET_PED_MAX_MOVE_BLEND_RATIO(s_stretch.ped, PEDMOVE_RUN)
				
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_lamar.ped, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_stretch.ped, TRUE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
					
					b_player_left_shop = TRUE
				ENDIF
			ENDIF
		
			FLOAT f_dist_to_lamar = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_lamar.ped))
			FLOAT f_dist_to_stretch = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_stretch.ped))
		
			//Before the god text say a line about where to go.
			IF NOT b_has_text_label_triggered[LEM1_SHOP5] 
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SHOP5", CONV_PRIORITY_MEDIUM)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0)
						b_has_text_label_triggered[LEM1_SHOP5] = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			SWITCH i_current_event
				CASE 0 //Drive to the plant.	
					IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ON_FOOT(s_locates_data, <<-614.2678, -1602.9640, 25.7517>>, <<3.0, 3.0, LOCATE_SIZE_HEIGHT>>, 
													  FALSE, s_lamar.ped, s_stretch.ped, NULL, 
													  "LEM1_GOMEET", "LEM1_LEFTLEMAR", "LEM1_LEFTSTRET", "", "LEM1_LEFTBOTH", FALSE, TRUE)
					
					//1113905 - GPS takes an odd route so force it to the front of the plant.
					IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
						SET_BLIP_ALPHA(s_locates_data.LocationBlip, 0)
						
						IF DOES_BLIP_EXIST(blip_current_destination)
						AND NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blip_current_destination), v_meeting_pos)
							REMOVE_BLIP(blip_current_destination)
						ENDIF
						
						IF NOT DOES_BLIP_EXIST(blip_current_destination)
							blip_current_destination = CREATE_BLIP_FOR_COORD(v_meeting_pos)
						ENDIF
						
						CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "Locates blip exists.")
					ELSE
						IF DOES_BLIP_EXIST(blip_current_destination)
							REMOVE_BLIP(blip_current_destination)
						ENDIF
					ENDIF
					
					IF NOT b_player_been_in_car
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly, TRUE)
						
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							b_player_been_in_car = TRUE
						ENDIF
					ENDIF
				
					//2260076 - There's an issue where the buddies seem to be removed from the group when parking in a certain way.
					//Seems to be locates header related but it's too risky to make changes to the header at this stage.
					BOOL bOverrideLocatesCheck
					
					IF NOT DOES_BLIP_EXIST(s_locates_data.LocationBlip)
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.753296, -1601.941528, 23.058146>>, <<-627.648376, -1599.525757, 31.501303>>, 22.0)
						AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
						AND f_dist_to_lamar < 100.0
						AND f_dist_to_stretch < 100.0
						AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
							CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "Overriding locates check.")
						
							bOverrideLocatesCheck = TRUE
						ENDIF
					ENDIF
				
					IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
					OR bOverrideLocatesCheck
						IF DOES_BLIP_EXIST(s_locates_data.GPSBlipID)
							IF NOT DOES_BLIP_HAVE_GPS_ROUTE(s_locates_data.GPSBlipID)
								SET_BLIP_ROUTE(s_locates_data.GPSBlipID, TRUE)
							ENDIF
						ENDIF
					
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.753296, -1601.941528, 23.058146>>, <<-627.648376, -1599.525757, 31.501303>>, 22.0)
							CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "Player in angled area.")
						
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] ", "Player exited car.")
							
								CLEAR_MISSION_LOCATE_STUFF(s_locates_data, FALSE)
								REMOVE_BLIP(blip_current_destination)
								
								s_lamar.i_timer = 0
								s_stretch.i_timer = 0
								i_current_event++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1 //Walk inside the plant.	
					//New locates call just for the player.
					IS_PLAYER_AT_LOCATION_ON_FOOT(s_locates_data, v_meeting_pos, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, TRUE, "", TRUE)
					
					INT i_player_closest_node, i_lamar_closest_node, i_stretch_closest_node
					VECTOR v_lamar_dest, v_stretch_dest
					v_lamar_dest = <<-611.8230, -1618.2036, 32.0105>>
					v_stretch_dest = <<-613.2916, -1620.0579, 32.0105>>
					
					//Keep track of who is closest to the destination (using waypoint nodes).
					IF GET_IS_WAYPOINT_RECORDING_LOADED(str_waypoint_stairs)
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_stairs, GET_ENTITY_COORDS(PLAYER_PED_ID()), i_player_closest_node)
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_stairs, GET_ENTITY_COORDS(s_lamar.ped), i_lamar_closest_node)
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(str_waypoint_stairs, GET_ENTITY_COORDS(s_stretch.ped), i_stretch_closest_node)
					ENDIF
					
					//Lamar behaviour
					IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-611.257874, -1609.935425, 26.035994>>, <<-612.242920, -1620.899536, 35.437653>>, 6.0)
						SET_PED_RESET_FLAG(s_lamar.ped, PRF_UseProbeSlopeStairsDetection, TRUE)
					ENDIF
					
					IF s_lamar.i_event = 0
						IF IS_PED_IN_GROUP(s_lamar.ped)
							REMOVE_PED_FROM_GROUP(s_lamar.ped)
						ENDIF
					
						//Lamar walks up the stairs and leads the way.
						IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
							IF VDIST2(GET_ENTITY_COORDS(s_lamar.ped), v_lamar_dest) > 4.0
								SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_OpenDoorArmIK, TRUE)
								TASK_FOLLOW_NAV_MESH_TO_COORD(s_lamar.ped, v_lamar_dest, PEDMOVE_WALK, 40000, 0.25, ENAV_DEFAULT, 349.0120)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
								s_lamar.i_event = 0
							ENDIF
						ELSE
							SET_PED_RESET_FLAG(s_lamar.ped, PRF_SearchForClosestDoor, TRUE)
						
							/*IF i_lamar_closest_node - i_player_closest_node < 3
								SET_PED_MIN_MOVE_BLEND_RATIO(s_lamar.ped, PEDMOVE_RUN)
							ELSE
								SET_PED_MAX_MOVE_BLEND_RATIO(s_lamar.ped, PEDMOVE_WALK)
							ENDIF*/
						ENDIF
					
						IF VDIST2(GET_ENTITY_COORDS(s_lamar.ped), v_lamar_dest) < 4.0
							TASK_LOOK_AT_ENTITY(s_lamar.ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
							s_lamar.i_event++
						ENDIF
					ELIF s_lamar.i_event = 1
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-611.707947,-1620.938477,31.260536>>, <<-611.278015,-1616.510010,35.185593>>, 5.000000)
							IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
							AND VDIST2(GET_ENTITY_COORDS(s_lamar.ped), <<-613.2521, -1619.0509, 32.0105>>) > 3.0
								TASK_FOLLOW_NAV_MESH_TO_COORD(s_lamar.ped, <<-613.2521, -1619.0509, 32.0105>>, PEDMOVE_WALK, 40000, 0.25, ENAV_DEFAULT, 349.0120)
								TASK_CLEAR_LOOK_AT(s_lamar.ped)
							ENDIF
						ENDIF
					ENDIF
					
					//Stretch behaviour
					IF IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-611.257874, -1609.935425, 26.035994>>, <<-612.242920, -1620.899536, 35.437653>>, 6.0)
						SET_PED_RESET_FLAG(s_stretch.ped, PRF_UseProbeSlopeStairsDetection, TRUE)
					ENDIF
					
					IF IS_PED_IN_GROUP(s_stretch.ped)
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(s_lamar.ped)
					AND NOT IS_PED_IN_ANY_VEHICLE(s_stretch.ped)
						REMOVE_PED_FROM_GROUP(s_stretch.ped)
					ENDIF
					
					IF s_stretch.i_event = 0
						IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
							IF VDIST2(GET_ENTITY_COORDS(s_stretch.ped), v_stretch_dest) > 4.0
								SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_OpenDoorArmIK, TRUE)
								
								IF s_stretch.i_timer = 0
								AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(s_lamar.ped)
									s_stretch.i_timer = GET_GAME_TIMER()
								ENDIF
								
								IF GET_GAME_TIMER() - s_stretch.i_timer > 700
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									AND NOT IS_PED_IN_ANY_VEHICLE(s_stretch.ped)
										TASK_FOLLOW_NAV_MESH_TO_COORD(s_stretch.ped, v_stretch_dest, PEDMOVE_WALK, 40000, 0.25, ENAV_DEFAULT, 300.2354)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
										s_stretch.i_event = 0
									ENDIF
								ENDIF
							ENDIF
						ELSE
							SET_PED_RESET_FLAG(s_stretch.ped, PRF_SearchForClosestDoor, TRUE)
						ENDIF
					
						IF VDIST2(GET_ENTITY_COORDS(s_stretch.ped), v_stretch_dest) < 4.0
							TASK_LOOK_AT_ENTITY(s_stretch.ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
							s_stretch.i_event++
						ENDIF
					ENDIF
				
					//If the player gets a wanted level then go back to the previous stage.
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)		
						CLEAR_PED_TASKS(s_lamar.ped)
						CLEAR_PED_TASKS(s_stretch.ped)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
					
						i_current_event--
					ENDIF
					
					//Check if the player tries to leave the meet.
					IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_meeting_pos) > 10000.0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								IF NOT b_has_text_label_triggered[LEM1_LEFTMEET]
									PRINT_NOW("LEM1_LEFTMEET", DEFAULT_GOD_TEXT_TIME, 0)
									b_has_text_label_triggered[LEM1_LEFTMEET] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Fail if the player went to the meeting location, then tried to leave.
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_meeting_pos) > 40000.0
						MISSION_FAILED(FAILED_MISSED_DEAL)
					ENDIF
				BREAK
			ENDSWITCH		
			
			//As part of B*1096871 the blip for the plant may have been placed early, once the locates blip appears delete the fudged blip.
//			IF DOES_BLIP_EXIST(blip_current_destination)
//				IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
//					REMOVE_BLIP(blip_current_destination)
//				ENDIF
//			ENDIF
		
			//Request cutscene in advance
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_meeting_pos) < (DEFAULT_CUTSCENE_LOAD_DIST * DEFAULT_CUTSCENE_LOAD_DIST)
				REQUEST_CUTSCENE("LAM_1_MCS_1_CONCAT")
				SETUP_BALLA(TRUE)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATIONS("LAM_1_MCS_1_CONCAT")
			ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_meeting_pos) > (DEFAULT_CUTSCENE_UNLOAD_DIST * DEFAULT_CUTSCENE_UNLOAD_DIST)
				IF IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
				ENDIF
			ENDIF
			
			
			//Progress once the player reaches the top of the stairs.
			IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-609.949890,-1619.683838,32.010139>>, <<-614.033020,-1618.932495,34.509949>>, 2.750000)
				OR VDIST2(GET_ENTITY_COORDS(s_lamar.ped), <<-613.2521, -1619.0509, 32.0105>>) < 2.0
					e_section_stage = SECTION_STAGE_CLEANUP
				ENDIF
			ELSE
				DO_BUDDY_FAILS()
			ENDIF
		
			BOOL b_lamar_inside_building, b_stretch_inside_building, b_player_inside_building
		
			//Dialogue: there's in-car dialogue on the way there and dialogue while walking up to the meeting room.
			SWITCH i_meeting_dialogue_event
				CASE 0 //In-car dialogue
					IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
					AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
					AND ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
						
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF i_drive_to_plant_dialogue_timer = 0
								i_drive_to_plant_dialogue_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - i_drive_to_plant_dialogue_timer > 2500
								IF GET_FAILS_COUNT_WITHOUT_PROGRESS_FOR_THIS_MISSION_SCRIPT() = 0
									IF NOT b_has_text_label_triggered[LEM1_REC]
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_REC", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_REC] = TRUE
										ENDIF
									ENDIF
								ELSE
									IF NOT b_has_text_label_triggered[LEM1_REC2]
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_REC2", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_REC2] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF b_has_text_label_triggered[LEM1_REC]
						OR b_has_text_label_triggered[LEM1_REC2]
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-624.420654,-1608.071777,25.751478>>, <<-607.613831,-1609.759277,37.398098>>, 26.750000)
						KILL_FACE_TO_FACE_CONVERSATION()
						i_meeting_dialogue_event++
					ENDIF
				BREAK
				
				CASE 1 //Meeting dialogue - only play after the timelapse is done.
					b_player_inside_building = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-611.333618,-1610.304565,25.760519>>, <<-611.702698,-1620.920776,35.109859>>, 5.250000)
					b_lamar_inside_building = IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-611.333618,-1610.304565,25.760519>>, <<-611.702698,-1620.920776,35.109859>>, 5.250000)
					b_stretch_inside_building = IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-611.333618,-1610.304565,25.760519>>, <<-611.702698,-1620.920776,35.109859>>, 5.250000)
					
					IF b_player_inside_building
						SETUP_BALLA(TRUE)
					ENDIF
					
					//Play a line when the player arrives: if there was a timelapse here do the second part of the timelapse convo.
					IF NOT b_has_text_label_triggered[LEM1_ARRIVE]
						IF b_did_a_timelapse_at_plant
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_T2", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_ARRIVE] = TRUE
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_ARRIVE", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_ARRIVE] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF f_dist_to_lamar < 625.0
					AND f_dist_to_stretch < 625.0
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-624.420654,-1608.071777,25.751478>>, <<-607.613831,-1609.759277,37.398098>>, 26.750000)
					AND ((b_player_inside_building AND b_lamar_inside_building AND b_stretch_inside_building) OR NOT b_lamar_inside_building OR NOT b_stretch_inside_building)
					AND NOT IS_PED_IN_ANY_VEHICLE(s_stretch.ped)
					AND NOT IS_PED_IN_ANY_VEHICLE(s_lamar.ped)
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND DOES_BLIP_EXIST(s_locates_data.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT b_has_text_label_triggered[LEM1_STAIR]
								IF b_has_text_label_triggered[LEM1_ARRIVE]
									IF GET_FAILS_COUNT_WITHOUT_PROGRESS_FOR_THIS_MISSION_SCRIPT() = 0
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_STAIR", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_STAIR] = TRUE
										ENDIF
									ELSE
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_STAIR2", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_STAIR] = TRUE
											i_meeting_dialogue_timer = 0
										ENDIF
									ENDIF
								ENDIF
							ELIF NOT b_has_text_label_triggered[LEM1_STOP] //Play a line if the player hangs around
								IF i_meeting_dialogue_timer = 0
									i_meeting_dialogue_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - i_meeting_dialogue_timer > 10000
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_STOP", CONV_PRIORITY_MEDIUM)
										b_has_text_label_triggered[LEM1_STOP] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF b_has_text_label_triggered[LEM1_STAIR]
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					//Kill the stairs conversation early to help with avoiding delays when triggering the cutscene.
					IF b_has_text_label_triggered[LEM1_STAIR]
						TEXT_LABEL_23 str_root
						str_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF ARE_STRINGS_EQUAL(str_root, "LEM1_STAIR")
						OR ARE_STRINGS_EQUAL(str_root, "LEM1_STAIR2")
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-611.803772,-1620.893433,35.010536>>, <<-611.145996,-1612.274536,30.025726>>, 5.000000)
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			IF b_blocked_shop_dialogue
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data)
					SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_01_DT, FALSE)
					b_blocked_shop_dialogue = FALSE
				ENDIF
			ENDIF
			
			IF NOT b_player_inside_building
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_RECYCLING_CENTRE")
						START_AUDIO_SCENE("LAMAR_1_GO_TO_RECYCLING_CENTRE")
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_RECYCLING_CENTRE")
						STOP_AUDIO_SCENE("LAMAR_1_GO_TO_RECYCLING_CENTRE")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_GO_TO_MEETING")
					START_AUDIO_SCENE("LAMAR_1_GO_TO_MEETING")
				ENDIF
			ENDIF
			
			/*//Do a streaming volume for the interior
			IF NOT b_player_inside_building
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-610.0187, -1613.3081, 26.0105>>) < 2500.0
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START_SPHERE(<<-610.0187, -1613.3081, 26.0105>>, 5.0)
				ENDIF
				
				i_new_load_scene_timer = GET_GAME_TIMER()
			ELSE
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF GET_GAME_TIMER() - i_new_load_scene_timer > 1000
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ENDIF
			ENDIF*/
		ENDIF
		
		//Prevent the player from trying to ride bikes into the building.
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-611.333618,-1610.304565,25.760519>>, <<-611.702698,-1620.920776,35.109859>>, 5.250000)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					b_player_rode_bike_into_plant = TRUE
					
					IF TIMERA() > 1000					
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ELSE
					SETTIMERA(0)
				ENDIF
			ENDIF
		ELSE
			SETTIMERA(0)
		
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND b_player_rode_bike_into_plant
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				b_player_rode_bike_into_plant = FALSE
			ENDIF
			
			//Prevent the player from getting into vehicles inside the building.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-611.333618,-1610.304565,25.760519>>, <<-611.702698,-1620.920776,35.109859>>, 5.250000)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ENDIF
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		CLEAR_PRINTS()
		CLEAR_HELP()
		REMOVE_ALL_BLIPS()
		CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
		KILL_FACE_TO_FACE_CONVERSATION()
		
		IF NOT IS_ENTITY_DEAD(veh_start_car)
			SET_ENTITY_AS_MISSION_ENTITY(veh_start_car, TRUE, TRUE)
			REMOVE_VEHICLE(veh_start_car, FALSE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(veh_weapons_shop_car)
			SET_ENTITY_AS_MISSION_ENTITY(veh_weapons_shop_car, TRUE, TRUE)
			REMOVE_VEHICLE(veh_weapons_shop_car, FALSE)
		ENDIF

		REMOVE_ANIM_DICT(str_shop_anims)
		NEW_LOAD_SCENE_STOP()

		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_MEETING_CUTSCENE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP		
		JUMP_TO_STAGE(STAGE_MEETING_CUTSCENE)
	ENDIF
ENDPROC

PROC MEETING_CUTSCENE()
	IF e_section_stage = SECTION_STAGE_SETUP
		IF b_is_jumping_directly_to_stage
			IF i_current_event != 99
				IF b_used_a_checkpoint
					START_REPLAY_SETUP(v_meeting_pos + <<0.0, 0.0, 0.5>>, 0.0, FALSE)
					
					i_current_event = 99
				ELSE
					IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_RECYCLING_INTERIOR_IN_MEMORY, <<0.0, 0.0, 0.0>>)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), v_meeting_pos + <<0.0, 0.0, 0.5>>)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						
						LOAD_SCENE(v_meeting_pos)
						
						i_current_event = 99
					ENDIF
				ENDIF
			ELSE
				IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_RECYCLING_INTERIOR_IN_MEMORY, <<0.0, 0.0, 0.0>>)
					IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, v_meeting_pos - <<0.0, 3.0, 0.0>>)
					AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, v_meeting_pos - <<0.0, 2.0, 0.0>>)
					AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
					AND SETUP_REQ_FRANKLIN_HAS_WEAPONS_ON_Z_SKIP()
						END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
						
						SET_ENTITY_COORDS(PLAYER_PED_ID(), v_meeting_pos + <<0.0, 0.0, 0.5>>)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						
						b_is_jumping_directly_to_stage = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Turn control off early if the player tries to walk through the door.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-614.123047,-1619.021729,32.010517>>, <<-611.439221,-1619.122681,34.260010>>, 3.250000)
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				ENDIF
			ENDIF
			
			REQUEST_CUTSCENE("LAM_1_MCS_1_CONCAT")
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS("LAM_1_MCS_1_CONCAT")

			IF SETUP_BALLA()
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data)
			AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				CANCEL_BUDDY_PERMANENT_HEAD_TRACK(s_lamar.ped)
				CANCEL_BUDDY_PERMANENT_HEAD_TRACK(s_stretch.ped)
				
				IF NOT IS_PED_INJURED(s_lamar.ped)
					SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_OpenDoorArmIK, FALSE)
					REMOVE_PED_FROM_GROUP(s_lamar.ped)
					TASK_CLEAR_LOOK_AT(s_lamar.ped)
					REGISTER_ENTITY_FOR_CUTSCENE(s_lamar.ped, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(s_stretch.ped)
					SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_OpenDoorArmIK, FALSE)
					REMOVE_PED_FROM_GROUP(s_stretch.ped)
					TASK_CLEAR_LOOK_AT(s_stretch.ped)
					REGISTER_ENTITY_FOR_CUTSCENE(s_stretch.ped, "Stretch", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF NOT IS_PED_INJURED(ped_ballas_og)
					REGISTER_ENTITY_FOR_CUTSCENE(ped_ballas_og, "Ballas_OG", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				START_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				REMOVE_ALL_AUDIO_SCENES()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, FALSE)
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, FALSE)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
				
				s_lamar.i_event = 0
				s_lamar.i_timer = 0
				s_stretch.i_event = 0
				s_stretch.i_timer = 0
				
				b_set_vehicle_lights_for_cutscene = FALSE
				b_lamars_lead_out_triggered_early = FALSE
				b_skipped_mocap = FALSE
				b_applied_balla_blood = FALSE
				i_time_shootout_started = 0
				i_current_event = 0
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		IF NOT b_skipped_mocap
			IF WAS_CUTSCENE_SKIPPED()
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				b_skipped_mocap = TRUE
			ENDIF
		ENDIF
	
		IF NOT IS_CUTSCENE_ACTIVE()
			e_section_stage = SECTION_STAGE_CLEANUP
		ELSE		
			IF IS_CUTSCENE_PLAYING()
				#IF IS_DEBUG_BUILD
					i_debug_cutscene_timer = GET_CUTSCENE_TIME()
				#ENDIF
				
				//Request dead Balla anims for use on his exit state.
				SETUP_DEAD_BALLA(FALSE, TRUE)
				
				IF NOT b_applied_balla_blood
				AND NOT IS_PED_INJURED(ped_ballas_og)
					IF GET_CUTSCENE_TIME() > 42500
						PED_INDEX peds[5]
						GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), peds)
					
						INT i = 0
						REPEAT COUNT_OF(peds) i
							IF NOT IS_PED_INJURED(peds[i])
								IF GET_ENTITY_MODEL(peds[i]) = CSB_BALLASOG
									APPLY_PED_BLOOD_SPECIFIC(peds[i], ENUM_TO_INT(PDZ_HEAD), 0.534, 0.556, 16.352, 1.000, 1, 0.00, "BulletLarge")
									APPLY_PED_BLOOD_SPECIFIC(peds[i], ENUM_TO_INT(PDZ_HEAD), 0.534, 0.556, 16.352, 1.000, 1, 0.00, "BulletLarge")
									APPLY_PED_BLOOD_SPECIFIC(peds[i], ENUM_TO_INT(PDZ_HEAD), 0.556, 0.530, 23.682, 1.000, 5, 0.00, "BulletLarge")
									APPLY_PED_BLOOD_SPECIFIC(peds[i], ENUM_TO_INT(PDZ_HEAD), 0.504, 0.576, 6.120, 1.000, 0, 0.00, "ShotgunLarge") 
									
									//Record footage of D getting shot.
//									IF NOT b_applied_balla_blood
//									AND NOT b_skipped_mocap
//										REPLAY_RECORD_BACK_FOR_TIME(4.0)
//									ENDIF
									
									b_applied_balla_blood = TRUE
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF	
				
				//NG 1744158 - Turn the lights on the vans that show up outside.
				IF NOT b_set_vehicle_lights_for_cutscene
					IF GET_CUTSCENE_TIME() > 33000
						VEHICLE_INDEX vehs[10]
						GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehs)
						
						INT i = 0
						REPEAT COUNT_OF(vehs) i
							IF NOT IS_ENTITY_DEAD(vehs[i])
								IF GET_ENTITY_MODEL(vehs[i]) = SPEEDO
								OR GET_ENTITY_MODEL(vehs[i]) = LANDSTALKER
									SET_VEHICLE_LIGHTS(vehs[i], FORCE_VEHICLE_LIGHTS_ON)
								ENDIF
							ENDIF
						ENDREPEAT
						
						b_set_vehicle_lights_for_cutscene = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			SWITCH i_current_event
				CASE 0
					IF IS_CUTSCENE_PLAYING()
						//1669866 - If the player parked a car outside the plant then set it as a vehgen.
						VEHICLE_INDEX veh_last_car
						veh_last_car = GET_PLAYERS_LAST_VEHICLE()
						
						IF IS_VEHICLE_DRIVEABLE(veh_last_car)		
							IF IS_ENTITY_AT_COORD(veh_last_car, <<-612.188171,-1594.531006,29.251713>>,<<22.750000,16.000000,4.000000>>)
								SET_MISSION_VEHICLE_GEN_VEHICLE(veh_last_car, <<-700.7425, 5270.6050, 74.4262>>, 229.0238)
							ENDIF
						ENDIF
					
						CLEAR_AREA(v_meeting_pos, 200.0, TRUE, TRUE)
						SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH_AND_LAMAR_READY_TO_FIGHT, <<0.0, 0.0, 0.0>>)
						
						SET_CURRENT_PED_WEAPON(s_lamar.ped, WEAPONTYPE_UNARMED, TRUE)
						
						IF NOT IS_PED_INJURED(s_stretch.ped)
							SET_CURRENT_PED_WEAPON(s_stretch.ped, WEAPONTYPE_UNARMED, TRUE)
						ENDIF
						
						//TODO 989990 - Set the time here if it hasn't reached 12:00 yet.
						SET_CLOCK_TIME(0, 0, 0)
						
						DO_FADE_IN_WITH_WAIT()
						
						i_current_event++
					ENDIF
				BREAK
				
				CASE 1 //Music event as they walk through the doors.
					IF GET_CUTSCENE_TIME() > 1500
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_CS_DOORS")
						i_current_event++
					ENDIF
				BREAK
			
				CASE 2 //Music when they realise it's a trap.
					IF GET_CUTSCENE_TIME() > 26000
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_MISSION_START")
						i_current_event++
					ENDIF
				BREAK
				
				CASE 3 //Music event when Stretch shoots D.
					IF NOT b_skipped_mocap
						PREPARE_MUSIC_EVENT("LM1_TERMINADOR_HEAD_SHOT")
					
						IF GET_CUTSCENE_TIME() > 42500
							TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_HEAD_SHOT")
							i_current_event++
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			
		ENDIF
		
		//Request some assets in advance
		REQUEST_ACTION_MODE_ASSET("FRANKLIN_ACTION")
		REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
		REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
		REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
		REQUEST_ANIM_DICT("misslamar1ig_2_p1")
		
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar")
			SET_ENTITY_COORDS(s_lamar.ped, <<-622.6625, -1624.3663, 32.0105>>, TRUE, TRUE)
			SET_ENTITY_HEADING(s_lamar.ped, 80.0)
			SET_CURRENT_PED_WEAPON(s_lamar.ped, WEAPONTYPE_PUMPSHOTGUN, TRUE)
			SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_BERD, 1, 0)
			
			//s_lamar.cover = ADD_COVER_POINT(<<-622.6625, -1624.3663, 32.0105>>, 170.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
			//s_lamar.cover = ADD_COVER_POINT(<<-622.5257, -1624.8983, 32.0105>>, 170.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
			//TASK_PUT_PED_DIRECTLY_INTO_COVER_FROM_TARGET(s_lamar.ped, NULL, NULL, <<-624.3, -1632.3, 33.0>>, <<-622.5257, -1624.8983, 32.0105>>, -1, FALSE, 0.0, TRUE, FALSE, s_lamar.cover)
			
			//Trigger Lamar's in-game anim early and pause it, to stop on-screen pops when going from the cover pose to this anim.
			IF NOT b_skipped_mocap
				IF HAS_ANIM_DICT_LOADED("misslamar1ig_2_p1")
					s_lamar.i_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<-617.689, -1629.484, 32.038>>, <<0.000, 0.000, 175.000>>)
					TASK_SYNCHRONIZED_SCENE(s_lamar.ped, s_lamar.i_sync_scene, "misslamar1ig_2_p1", "ig_2_p1_lamar", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
											SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, 
											RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_ELECTROCUTION | RBF_MELEE, INSTANT_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_PHASE(s_lamar.i_sync_scene, 0.0)
					SET_SYNCHRONIZED_SCENE_RATE(s_lamar.i_sync_scene, 0.0)
					b_lamars_lead_out_triggered_early = TRUE
				ENDIF
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(s_lamar.ped)
				
				s_lamar.i_timer = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Stretch")
			IF NOT IS_PED_INJURED(s_stretch.ped)
				IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_AIM_GUN_AT_COORD) != PERFORMING_TASK
					SET_ENTITY_COORDS(s_stretch.ped, <<-622.458, -1623.38, 32.0105>>) 
					SET_ENTITY_HEADING(s_stretch.ped, 175.0071)
				
					SET_CURRENT_PED_WEAPON(s_stretch.ped, WEAPONTYPE_PISTOL, TRUE)
				
					TASK_AIM_GUN_AT_COORD(s_stretch.ped, <<-625.3885, -1626.4886, 33.2392>>, -1, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, TRUE)
					FORCE_PED_MOTION_STATE(s_stretch.ped, MS_AIMING, FALSE, FAUS_CUTSCENE_EXIT)
					
					s_stretch.i_timer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			IF HAS_ACTION_MODE_ASSET_LOADED("FRANKLIN_ACTION")
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ACTIONMODE_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, TRUE)
				
				REPLAY_STOP_EVENT()
				
			ENDIF
		ENDIF
		
		IF NOT b_skipped_mocap
			IF NOT IS_PED_INJURED(s_lamar.ped)
				IF s_lamar.i_timer != 0
					IF GET_GAME_TIMER() - s_lamar.i_timer > 2500
						MANAGE_LAMAR_IN_INTERIOR()
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
				IF s_stretch.i_timer != 0
					IF GET_GAME_TIMER() - s_stretch.i_timer > 1500
						MANAGE_STRETCH_IN_INTERIOR()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ballas_OG")
			SETUP_DEAD_BALLA(FALSE)
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		If b_skipped_mocap
			CANCEL_ALL_PREPARED_MUSIC_EVENTS()
			TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_MISSION_FAIL")
		
			//If the player skipped the mocap the peds need to be warped.
			WHILE IS_CUTSCENE_ACTIVE()
			OR NOT SETUP_DEAD_BALLA(FALSE)
				WAIT(0)
			ENDWHILE
		
			IF NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_INJURED(s_stretch.ped)
				IF s_lamar.i_event = 0
					SET_ENTITY_COORDS(s_lamar.ped, <<-622.6625, -1624.3663, 32.0105>>)
					SET_ENTITY_HEADING(s_lamar.ped, 185.6249)
				ENDIF
				
				IF s_stretch.i_event = 0
					SET_ENTITY_COORDS(s_stretch.ped, <<-622.6948, -1623.1971, 32.0105>>)
					SET_ENTITY_HEADING(s_stretch.ped, 175.0071)
				ENDIF
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF		
	
		IF NOT IS_PED_INJURED(s_lamar.ped)
			IF NOT IS_PED_IN_COVER(s_lamar.ped)
			AND GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) != PERFORMING_TASK
			AND s_lamar.i_event = 0
				s_lamar.cover = ADD_COVER_POINT(<<-622.6625, -1624.3663, 32.0105>>, 185.6249, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(s_lamar.ped, <<-622.6625, -1624.3663, 32.0105>>, -1, FALSE, 0.5, TRUE, FALSE, s_lamar.cover)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(s_stretch.ped)
			IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_AIM_GUN_AT_COORD) != PERFORMING_TASK
			AND s_stretch.i_event = 0
				TASK_AIM_GUN_AT_COORD(s_stretch.ped, <<-625.3885, -1626.4886, 33.2392>>, -1)
			ENDIF
		ENDIF
		
		SETUP_DEAD_BALLA(FALSE, FALSE, TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
		i_current_event = 0
		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		STOP_CUTSCENE()
		CANCEL_ALL_PREPARED_MUSIC_EVENTS()
		b_skipped_mocap = TRUE
		
		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC


PROC ESCAPE_INTERIOR()
	INT i = 0

	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF e_section_stage = SECTION_STAGE_SETUP
		IF b_is_jumping_directly_to_stage
			IF i_current_event != 99
				IF b_used_a_checkpoint
					IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
						START_REPLAY_SETUP(<<-624.0677, -1621.1552, 32.0105>>, 179.3006, FALSE)
					ELSE
						START_REPLAY_SETUP(<<-571.7395, -1601.8956, 26.0108>>, 100.1658, FALSE)
					ENDIF

					i_current_event = 99
				ELSE
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ENDIF
					
					IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-624.0677, -1621.1552, 32.0105>>)
					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-571.7395, -1601.8956, 26.0108>>)
					ENDIF
					
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
					//Need to load the scene first in this case, otherwise everything falls through the ground.
					IF SETUP_MISSION_REQUIREMENT(REQ_RECYCLING_INTERIOR_IN_MEMORY)
						IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
							LOAD_SCENE(<<-624.0677, -1621.1552, 33.0105>>)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-624.0677, -1621.1552, 32.0105>>)
						ELSE
							LOAD_SCENE(<<-571.7395, -1601.8956, 26.0108>>)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-571.7395, -1601.8956, 26.0108>>)
						ENDIF

						i_current_event = 99
					ENDIF
				ENDIF
			ELSE
				IF SETUP_MISSION_REQUIREMENT(REQ_RECYCLING_INTERIOR_IN_MEMORY)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<-622.6625, -1624.3663, 32.0105>>, 80.6249)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<-622.458, -1623.38, 32.0105>>, 175.0071)
				AND SETUP_DEAD_BALLA(FALSE)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH_AND_LAMAR_READY_TO_FIGHT, <<0.0, 0.0, 0.0>>)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_ASSISTED_ROUTES, <<0.0, 0.0, 0.0>>)
				AND SETUP_REQ_FRANKLIN_HAS_WEAPONS_ON_Z_SKIP()			
					i_time_shootout_started = 0
					b_lamars_lead_out_triggered_early = FALSE
				
					IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
						END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
					
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-624.0677, -1621.1552, 32.0105>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 179.3006)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						
						b_applied_balla_blood = FALSE
						b_is_jumping_directly_to_stage = FALSE
					ELSE
						REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
						REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
						REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
						REQUEST_ANIM_DICT("misslamar1ig_2_p1")
			
						IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
						AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PUMPSHOTGUN)
						AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
						AND HAS_ANIM_DICT_LOADED("misslamar1ig_2_p1")	
						AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_AMBIENCE_REMOVED_FOR_FIGHT, <<0.0, 0.0, 0.0>>)
							IF NOT IS_PED_INJURED(s_lamar.ped)
								SET_ENTITY_COORDS(s_lamar.ped, <<-573.7945, -1611.4336, 26.0110>>, FALSE)
								SET_ENTITY_HEADING(s_lamar.ped, 93.6465)
							ENDIF
							
							IF NOT IS_PED_INJURED(s_stretch.ped)
								SET_ENTITY_COORDS(s_stretch.ped, <<-572.0372, -1600.0889, 26.0110>>, FALSE)
								SET_ENTITY_HEADING(s_stretch.ped, 89.2657)
							ENDIF
							
							KILL_ENEMY_GROUP_INSTANTLY(s_swat_indoor_breach)
							KILL_ENEMY_GROUP_INSTANTLY(s_swat_indoor_pre_warehouse)
							KILL_ENEMY_GROUP_INSTANTLY(s_swat_indoor_window_1)
							KILL_ENEMY_GROUP_INSTANTLY(s_swat_indoor_window_2)
							KILL_ENEMY_GROUP_INSTANTLY(s_swat_indoor_lift)
							
							s_lamar.i_event = BUDDY_EVENT_MID_FIGHT_CHECKPOINT
							s_stretch.i_event = BUDDY_EVENT_MID_FIGHT_CHECKPOINT
							
							//Need to get the shootout in place before the screen fades back
							WHILE NOT DOES_ENTITY_EXIST(s_swat_indoor_warehouse[0].ped)
							OR NOT DOES_ENTITY_EXIST(s_swat_indoor_roof_1[0].ped)
							OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
							OR (NOT IS_PED_INJURED(s_lamar.ped) AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped))
							OR (NOT IS_PED_INJURED(s_stretch.ped) AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped))
								MANAGE_INTERIOR_ENEMIES_GANG_VERSION(GET_ENTITY_COORDS(PLAYER_PED_ID()))								
								MANAGE_LAMAR_IN_INTERIOR()
								MANAGE_STRETCH_IN_INTERIOR()
								
								WAIT(0)
							ENDWHILE
							
							END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
							
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-571.7395, -1601.8956, 26.0108>>, FALSE)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 100.1658)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							
							CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.0, TRUE, TRUE)
							
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(90.0 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)	
							
							b_is_jumping_directly_to_stage = FALSE
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ELSE
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
		
			IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PUMPSHOTGUN)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
				b_player_escaped = FALSE
				b_lamar_escaped = FALSE
				b_stretch_escaped = FALSE
				b_player_climbed_ladder = FALSE
				b_lamar_climbed_ladder = FALSE
				b_stretch_climbed_ladder = FALSE
				b_play_grenade_kill_dialogue = FALSE
				b_player_started_climbing_ladder = FALSE
				b_lamar_alternate_run_active = FALSE
				b_stretch_alternate_run_active = FALSE
				b_player_alternate_run_active = FALSE
				b_player_reached_exit = FALSE
				b_locker_doors_opened = FALSE
				b_skipped_to_lose_cops_stage = FALSE
				b_enemy_in_first_room_set_to_charge = FALSE
				b_glass_smashed_in_first_room = FALSE
				b_glass_smashed_in_heli_room = FALSE
				b_cops_triggered_due_to_griefing = FALSE
				b_triggered_police_report = FALSE
				b_shootout_started = FALSE
				b_lamar_on_the_ground = FALSE
				b_light_ropes_are_attached[0] = FALSE
				b_light_ropes_are_attached[1] = FALSE
				b_light_ropes_are_attached[2] = FALSE
				b_light_ropes_are_attached[3] = FALSE
			
				i_fail_timer = 0
				i_interior_fail_timer = 0
				i_carjack_event = 0
				i_hooker_event = 0
				i_shooting_stat_help_timer = 0
				i_num_times_played_ladder_hurry_line = 0
				i_hurry_up_exterior_dialogue_timer = 0
				i_stretch_leave_warehouse_timer = 0
				i_lamar_leave_warehouse_timer = 0
				i_gas_explosion_timer = 0
				
				b_has_text_label_triggered[LEM1_LEFTBOTH] = FALSE //This label may have been used in previous stages.
			
				//Make sure the player has the correct weapon equipped.
				GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				SETUP_MISSION_REQUIREMENT(REQ_AMBIENCE_REMOVED_FOR_FIGHT)
				
				INITIALISE_WAREHOUSE_FIRE()
				INITIALISE_WAREHOUSE_FIRE_2()
				
				DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
      			DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
				
				//Kill the fake dead Balla ped here if they weren't killed in the cutscene (e.g. if skipping).
				IF NOT IS_PED_INJURED(ped_ballas_og)				
					SET_ENTITY_VISIBLE(ped_ballas_og, TRUE)
					IF NOT b_applied_balla_blood
						
						b_applied_balla_blood = TRUE
					ENDIF
					
					APPLY_PED_BLOOD_SPECIFIC(ped_ballas_og, ENUM_TO_INT(PDZ_HEAD), 0.534, 0.556, 16.352, 1.000, 1, 0.00, "BulletLarge")
					APPLY_PED_BLOOD_SPECIFIC(ped_ballas_og, ENUM_TO_INT(PDZ_HEAD), 0.534, 0.556, 16.352, 1.000, 1, 0.00, "BulletLarge")
					APPLY_PED_BLOOD_SPECIFIC(ped_ballas_og, ENUM_TO_INT(PDZ_HEAD), 0.556, 0.530, 23.682, 1.000, 5, 0.00, "BulletLarge")
					APPLY_PED_BLOOD_SPECIFIC(ped_ballas_og, ENUM_TO_INT(PDZ_HEAD), 0.504, 0.576, 6.120, 1.000, 0, 0.00, "ShotgunLarge") 
					
					SET_PED_TO_RAGDOLL(ped_ballas_og, 2000, 5000, TASK_RELAX)
					
					IF IS_SCREEN_FADED_OUT()
						WAIT(500)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(ped_ballas_og)
					//Add a blood pool by the Balla ped.
					IF decal_blood = NULL
						//VECTOR v_ped_pos = GET_PED_BONE_COORDS(ped_ballas_og, BONETAG_HEAD, <<0.0, 0.0, 0.0>>)
						VECTOR v_ped_pos = GET_ENTITY_COORDS(ped_ballas_og)
						decal_blood = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<v_ped_pos.x, v_ped_pos.y, 32.1>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
					ENDIF
					
					SET_ENTITY_HEALTH(ped_ballas_og, 0)
				ENDIF
								
				SET_POLICE_RADAR_BLIPS(FALSE)
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
				
				CREATE_MODEL_HIDE(<<-614.7736, -1633.7233, 32.4919>>, 2.0, PROP_CRATE_05A, TRUE)
				
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_entrance_l, TRUE)
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_entrance_r, TRUE)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_l, v_interior_after_chopper_l, TRUE, 0.0, 0.0, 1.0)
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(model_interior_entrance_r, v_interior_after_chopper_r, TRUE, 0.0, 0.0, -1.0)
				
				//If skipped here, do a wait to make sure everything is set up nicely before it fades back in.
				IF IS_SCREEN_FADED_OUT()
					SET_CLOCK_TIME(0, 0, 0)
					
					IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-623.0, -1624.7, 33.2>>, 2.0, V_ILEV_RC_DOOR1)
							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-623.0, -1624.7, 33.2>>, TRUE, 0.0)
							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-623.0, -1624.7, 33.2>>, FALSE, 0.0)
						ENDIF
						
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-623.0, -1624.7, 33.2>>, 2.0, V_ILEV_RC_DOOR1)
							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-625.6, -1624.5, 33.2>>, TRUE, 0.0)
							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_RC_DOOR1, <<-625.6, -1624.5, 33.2>>, FALSE, 0.0)
						ENDIF
						
						//Get peds into position (only do if they didn't start their run in the mocap).
						IF NOT IS_PED_INJURED(s_lamar.ped)
						AND s_lamar.i_event = 0
							REMOVE_COVER_POINT(s_lamar.cover)
							s_lamar.cover = ADD_COVER_POINT(<<-622.6625, -1624.3663, 32.0105>>, 170.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(s_lamar.ped, <<-622.6625, -1624.3663, 32.0105>>, -1, FALSE, 0.0, TRUE, FALSE, s_lamar.cover)
						ENDIF
						
						IF NOT IS_PED_INJURED(s_stretch.ped)
						AND s_stretch.i_event = 0
							TASK_AIM_GUN_AT_COORD(s_stretch.ped, <<-625.3885, -1626.4886, 33.2392>>, -1)
						ENDIF
						
						WAIT(1000)
					
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_GAMEPLAY_BEGINS_RESTART")						
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ELSE
						SETTIMERB(0)
						
						s_lamar.b_refresh_tasks = TRUE
						s_stretch.b_refresh_tasks = TRUE
						
						WHILE NOT SETUP_REQ_WAREHOUSE_LIGHTS()
							WAIT(0)
						ENDWHILE
						
						//Get the shootout going during the fade.
						WHILE TIMERB() < 1000
							MANAGE_INTERIOR_ENEMIES_GANG_VERSION(GET_ENTITY_COORDS(PLAYER_PED_ID()))								
							MANAGE_LAMAR_IN_INTERIOR()
							MANAGE_STRETCH_IN_INTERIOR()
							
							WAIT(0)
						ENDWHILE
					
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_ENTER_WAREHOUSE_RESTART")
					ENDIF
				ELSE
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_GAMEPLAY_BEGINS")
				ENDIF
				
				
				//990268 - Add a cover point by the lockers to help the player.
				IF cover_locker = NULL
					cover_locker = ADD_COVER_POINT(<<-603.3557, -1619.3582, 32.0105>>, 275.4310, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_300TO0, TRUE)
				ENDIF
				
				SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(TRUE)
				
				//Create some health pickups: remove any old ones first.
				VECTOR vPickupPos
				
				IF DOES_PICKUP_EXIST(pickup_health[0])
					vPickupPos = GET_PICKUP_COORDS(pickup_health[0])
					REMOVE_PICKUP(pickup_health[0])
					CLEAR_AREA(vPickupPos, 2.0, TRUE)
				ENDIF
				
				IF DOES_PICKUP_EXIST(pickup_health[1])
					vPickupPos = GET_PICKUP_COORDS(pickup_health[1])
					REMOVE_PICKUP(pickup_health[1])
					CLEAR_AREA(vPickupPos, 2.0, TRUE)
				ENDIF				
				
				INT i_placement_flags = 0
				SET_BIT(i_placement_flags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
				SET_BIT(i_placement_flags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
				
				pickup_health[0] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<-602.3171, -1612.2581, 26.8207>>, <<0.0000, 0.0000, -3.9600>>, i_placement_flags)
				pickup_health[1] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<-595.6300, -1620.1801, 32.1100>>, <<0.0000, 0.0000, 25.2000>>, i_placement_flags)
				
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
				
				//Checkpoint
				IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE		
					i_audio_scene_event_shootout = 0
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_INTERIOR_START, "ESCAPE_PRE_WAREHOUSE")
					CLEAR_TRIGGERED_LABELS()
				ELIF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
					i_audio_scene_event_shootout = 50
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_INTERIOR_MIDDLE, "ESCAPE_FROM_WAREHOUSE")
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					IF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
						cover_player = ADD_COVER_POINT(<<-571.7395, -1601.8956, 26.0108>>, 100.1658, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_300TO0)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), <<-571.7395, -1601.8956, 26.0108>>, -1, FALSE, 0.0, TRUE, TRUE, cover_player)
						
						WAIT(500)
					ENDIF
					
					SETTIMERB(0)
					
					//Get everything going before the fade in.
					WHILE TIMERB() < 400
						PAUSE_CLOCK(TRUE)
						MANAGE_INTERIOR_ENEMIES_GANG_VERSION(GET_ENTITY_COORDS(PLAYER_PED_ID()))								
						MANAGE_LAMAR_IN_INTERIOR(TRUE)
						MANAGE_STRETCH_IN_INTERIOR(TRUE)
						
						WAIT(0)
					ENDWHILE
				
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					WHILE NOT IS_SCREEN_FADED_IN()
						PAUSE_CLOCK(TRUE)
						MANAGE_INTERIOR_ENEMIES_GANG_VERSION(GET_ENTITY_COORDS(PLAYER_PED_ID()))								
						MANAGE_LAMAR_IN_INTERIOR(TRUE)
						MANAGE_STRETCH_IN_INTERIOR(TRUE)
						
						WAIT(0)
					ENDWHILE
				ENDIF		
				
				SETTIMERA(0)
				SETTIMERB(0)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_PLAYER_SPRINT(PLAYER_ID(), FALSE)
				i_time_fire_started = 0
				i_time_shootout_started = GET_GAME_TIMER()
				//i_time_since_player_reached_exit = 0
				i_cops_arrival_timer = 0
				i_exit_warehouse_music_timer = 0
				i_chopper_dialogue_timer = 0
				i_interior_fail_check_event = 0
				i_current_music_event = 0
				i_warehouse_fire_counter_1 = 0
				i_warehouse_fire_counter_2 = 0
				i_grenade_help_event = 0
				i_current_event = 0
				e_section_stage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR v_lamar_pos, v_stretch_pos
		FLOAT f_lamar_height_diff, f_stretch_height_diff, f_dist_to_lamar, f_dist_to_stretch
		
		IF NOT IS_PED_INJURED(s_lamar.ped)
			v_lamar_pos = GET_ENTITY_COORDS(s_lamar.ped)
		ENDIF
		
		IF NOT IS_PED_INJURED(s_stretch.ped)
			v_stretch_pos = GET_ENTITY_COORDS(s_stretch.ped)
		ENDIF
		
		f_dist_to_lamar = VDIST2(v_lamar_pos, v_player_pos)
		f_dist_to_stretch = VDIST2(v_stretch_pos, v_player_pos)
		f_lamar_height_diff = ABSF(v_player_pos.z - v_lamar_pos.z)
		f_stretch_height_diff = ABSF(v_player_pos.z - v_stretch_pos.z)
		
		MANAGE_INTERIOR_ENEMIES_GANG_VERSION(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		MANAGE_LAMAR_IN_INTERIOR()
		MANAGE_STRETCH_IN_INTERIOR()
		UPDATE_WAREHOUSE_FIRE(s_warehouse_fire, i_warehouse_fire_counter_1, 0)
		UPDATE_WAREHOUSE_FIRE(s_warehouse_fire_2, i_warehouse_fire_counter_2, 1)
		SETUP_REQ_WAREHOUSE_LIGHTS()
		//CALCULATE_GRENADE_MULTIKILL_STAT()
		
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
		SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), v_player_pos)
		REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
		
		//989990 - Freeze the time to 12:00am
		PAUSE_CLOCK(TRUE)
				
		//Music events
		SWITCH i_current_music_event
			CASE 0 //Change the event when the player walks into the heli room.
				IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
					IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.185303,-1619.153809,32.010502>>, <<-592.520569,-1618.994507,34.760502>>, 4.250000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-566.410706,-1627.290649,32.056114>>, <<-592.112061,-1624.948608,34.760475>>, 7.250000)
							TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_ENTERED_ROOM")
							i_current_music_event++
						ENDIF
					ENDIF
				ELSE
					i_current_music_event++
				ENDIF
			BREAK
			
			CASE 1 //New event when reaching the stairwell.
				IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
					IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-565.486328,-1631.969360,32.090351>>, <<-565.017029,-1627.817139,34.777401>>, 1.750000)
							TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_CLUMSY_ASS")
							i_current_music_event++
						ENDIF
					ENDIF
				ELSE
					i_current_music_event++
				ENDIF
			BREAK
			
			CASE 2 //New event when smoke grenade guys die.
				IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
					IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_pre_warehouse)
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_SMOKE")
						i_current_music_event++
					ENDIF
				ELSE
					i_current_music_event++
				ENDIF
			BREAK
			
			CASE 3 //New event when entering the warehouse
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-605.415222,-1604.123047,26.010815>>, <<-568.352051,-1606.940674,31.550049>>, 16.000000)
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_ENTER_WAREHOUSE")
					i_current_music_event++
				ENDIF
			BREAK
			
			CASE 4 //New event when half of the enemies are dead in the warehouse.
				IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_warehouse) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_roof_1) <= (COUNT_OF(s_swat_indoor_warehouse) + COUNT_OF(s_swat_indoor_roof_1)) / 2
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_HALF_WAREHOUSE")
					i_current_music_event++
				ENDIF
			BREAK
			
			CASE 5 //New event when all enemies in the warehouse are dead.
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse) AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1)
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_ALL_WAREHOUSE")
					i_current_music_event++
				ENDIF
			BREAK
			
			CASE 6 //New event when door after warehouse explodes.
				IF s_swat_indoor_breach_2[0].b_is_created
					PREPARE_MUSIC_EVENT("LM1_TERMINADOR_2ND_DOOR_EXPLODES")
					
					IF s_swat_indoor_breach_2[0].i_event > 0
					OR IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach_2)
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_2ND_DOOR_EXPLODES")
					
						i_exit_warehouse_music_timer = 0
						i_current_music_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 7 //Exit warehouse event.
				IF i_exit_warehouse_music_timer = 0
					IF NOT IS_MUSIC_ONESHOT_PLAYING()
						i_exit_warehouse_music_timer = GET_GAME_TIMER()
					ENDIF
				ELSE 
					PREPARE_MUSIC_EVENT("LM1_TERMINADOR_EXIT_WAREHOUSE")
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.51,-1631.69,25.98>>, <<-592.32,-1629.18,28.23>>, 1.000000, FALSE, TRUE, TM_ON_FOOT)
					OR b_player_reached_exit
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_EXIT_WAREHOUSE")
						i_current_music_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 8 //Climbs the ladder.
				IF b_player_started_climbing_ladder
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-591.039063,-1639.498169,18.962502>>, <<-593.219055,-1642.996948,26.020140>>, 3.500000)
					IF NOT IS_MUSIC_ONESHOT_PLAYING()					
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_CLIMB_LADDER")
						i_current_music_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 9 //Player reaches outside the plant.
				IF b_player_escaped
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_LANDED")
					i_current_music_event = 100
				ENDIF
			BREAK
		ENDSWITCH
		
		//There's a box in the first room that can be knocked by mocap, just freeze it.
		IF NOT DOES_ENTITY_EXIST(obj_wooden_box)
			obj_wooden_box = GET_CLOSEST_OBJECT_OF_TYPE(<<-610.31, -1631.93, 32.01>>, 2.0, prop_box_wood01a)
			
			IF DOES_ENTITY_EXIST(obj_wooden_box)
				FREEZE_ENTITY_POSITION(obj_wooden_box, TRUE)
			ENDIF
		ENDIF
		
		//Fail checks
		IF NOT IS_PED_INJURED(s_lamar.ped)
		AND NOT IS_PED_INJURED(s_stretch.ped)
			//Global check (bug #280264) - If the player gets really far away just kill the peds off.
			IF f_dist_to_lamar > 40000.0
				SET_ENTITY_HEALTH(s_lamar.ped, 0)
				
				IF f_dist_to_stretch > 10000.0
					SET_ENTITY_HEALTH(s_stretch.ped, 0)
				ENDIF
			ELIF f_dist_to_stretch > 40000.0
				SET_ENTITY_HEALTH(s_stretch.ped, 0)
				
				IF f_dist_to_lamar > 10000.0
					SET_ENTITY_HEALTH(s_lamar.ped, 0)
				ENDIF
			ENDIF
		
			IF NOT b_lamar_has_reached_exit
				//Split the fail checks over multiple frames.
				IF i_interior_fail_check_event = 0
					b_player_inside_plant = FALSE
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-614.23,-1607.38,25.75>>, <<-616.93,-1640.62,38.11>>, 23.25)
						b_player_inside_plant = TRUE
					ENDIF
					
					i_interior_fail_check_event++
				ELIF i_interior_fail_check_event = 1
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-582.29,-1597.29,18.15>>, <<-585.14,-1632.1, 37.29>>, 50.0)
						b_player_inside_plant = TRUE
					ENDIF
				
					i_interior_fail_check_event++
				ELIF i_interior_fail_check_event = 2
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-593.004456,-1630.942261,26.010828>>, <<-607.757080,-1629.696289,28.260826>>, 4.000000)
						b_player_inside_plant = TRUE
					ENDIF
					
					//Bug #196866 - Fail if the player finds a way out of the interior and abandons the buddies.
					IF NOT b_player_inside_plant
						IF NOT b_has_text_label_triggered[LEM1_LEFTBOTH]
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							PRINT_NOW("LEM1_LEFTBOTH", DEFAULT_GOD_TEXT_TIME, 0)
							b_has_text_label_triggered[LEM1_LEFTBOTH] = TRUE
						ENDIF
						
						IF i_interior_fail_timer = 0
							IF (v_player_pos.z < 27.0 OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							AND IS_ENTITY_OCCLUDED(s_lamar.ped)
								i_interior_fail_timer = GET_GAME_TIMER()
							ENDIF
						ELIF GET_GAME_TIMER() - i_interior_fail_timer > 3000
							SET_ENTITY_HEALTH(s_lamar.ped, 0)
							SET_ENTITY_HEALTH(s_stretch.ped, 0)
						ENDIF
					ELSE
						i_interior_fail_timer = 0
					ENDIF
				
					i_interior_fail_check_event++
				ELIF i_interior_fail_check_event = 3
					//Bug #212044 - Fail if the player leaves Stretch and Lamar fighting some SWAT and continues through the building.
					IF NOT IS_PED_INJURED(s_stretch.ped)
					AND NOT IS_PED_INJURED(s_lamar.ped)
					AND (IS_PED_IN_COMBAT(s_lamar.ped) OR IS_PED_IN_COMBAT(s_stretch.ped))
						IF f_dist_to_lamar > 500.0
						AND IS_ENTITY_OCCLUDED(s_lamar.ped)	
							IF f_dist_to_stretch > 225.0
							AND IS_ENTITY_OCCLUDED(s_stretch.ped)	
								IF NOT b_has_text_label_triggered[LEM1_LEFTBOTH]
								AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
									PRINT_NOW("LEM1_LEFTBOTH", DEFAULT_GOD_TEXT_TIME, 0)
									b_has_text_label_triggered[LEM1_LEFTBOTH] = TRUE
								ENDIF
							ENDIF
							
							IF f_dist_to_lamar > 1250.0
								IF i_interior_fail_timer_2 = 0
									i_interior_fail_timer_2 = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - i_interior_fail_timer_2 > 5000
									SET_ENTITY_HEALTH(s_lamar.ped, 0)
									
									IF IS_ENTITY_OCCLUDED(s_stretch.ped)
										SET_ENTITY_HEALTH(s_stretch.ped, 0)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							i_interior_fail_timer_2 = 0
						ENDIF
					ENDIF
				
					i_interior_fail_check_event = 0
				ENDIF
			ELSE
				IF NOT b_has_text_label_triggered[LEM1_LEFTBOTH]
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					IF (f_dist_to_lamar > 2500.0 AND f_dist_to_stretch > 900.0)
					OR (f_dist_to_lamar > 900.0 AND f_dist_to_stretch > 2500.0)
						PRINT_NOW("LEM1_LEFTBOTH", DEFAULT_GOD_TEXT_TIME, 0)
						b_has_text_label_triggered[LEM1_LEFTBOTH] = TRUE
					ENDIF
				ENDIF
			
				//Once Lamar leaves just do the standard fail.
				DO_BUDDY_FAILS()
			ENDIF
		ENDIF
		
		IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE			
			//Open the doors to the locker room (do this separately to the other door commands to help execution time).
			IF NOT b_locker_doors_opened
				IF TIMERB() > 500
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_rc_door1, <<-603.8043, -1620.3005, 33.1606>>, TRUE, 0.0, 0.0, 1.1000)
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_rc_door1, <<-606.3953, -1620.0739, 33.1606>>, TRUE, 0.0, 0.0, -1.1000)
					
					b_locker_doors_opened = TRUE
				ENDIF
			ENDIF
			
			//1356152 - The cover point in the exploded doorway can cause issues in the next room, remove it once the player enters.
			IF cover_shootout[i_cover_in_doorway_index] != NULL
				IF s_swat_indoor_heli[0].i_event > 0
				AND v_player_pos.y < -1622.0
					REMOVE_COVER_POINT(cover_shootout[i_cover_in_doorway_index])
					cover_shootout[i_cover_in_doorway_index] = NULL
				ENDIF
			ENDIF
		
			//Dialogue and god text for before the first swat.
			IF NOT b_has_text_label_triggered[LEM1_DOOR] //Play a line to get Franklin moving.
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DOOR", CONV_PRIORITY_MEDIUM)
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						b_has_text_label_triggered[LEM1_DOOR] = TRUE
					ENDIF
				ENDIF
			ELIF NOT b_has_text_label_triggered[LEM1_KILLSWAT] //God text
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_NOW("LEM1_KILLSWAT", DEFAULT_GOD_TEXT_TIME, 0)
					b_has_text_label_triggered[LEM1_KILLSWAT] = TRUE
					i_wait_dialogue_timer = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF s_swat_indoor_window_1[0].i_event = 0
					IF NOT b_has_text_label_triggered[LEM1_GOGO] //"Hurry up" dialogue if the player doesn't move
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF GET_GAME_TIMER() - i_wait_dialogue_timer > 8500
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGO", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_GOGO] = TRUE
									i_wait_dialogue_timer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_DOOR2] //Lamar repeats line.
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF GET_GAME_TIMER() - i_wait_dialogue_timer > 8500
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DOOR", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_DOOR2] = TRUE
									i_wait_dialogue_timer = 0
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Clear the first god text once the first swat bursts in
			//IF IS_THIS_PRINT_BEING_DISPLAYED("LEM1_KILLSWAT]
			//AND s_swat_indoor_window_1[0].i_event > 0
			//	CLEAR_PRINTS()
			//ENDIF
			
			//Help text for the flashlight and shooting stat: comes up after going into the first room.
			IF s_swat_indoor_window_1[0].i_event > 0
				IF i_shooting_stat_help_timer = 0
					i_shooting_stat_help_timer = GET_GAME_TIMER()
				ELIF GET_GAME_TIMER() - i_shooting_stat_help_timer > 500
					IF NOT b_has_text_label_triggered[LEM1_CINEHELP] //2002804 - New help text explaining how to use cinematic shooting.
						//IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						//	PRINT_HELP("LEM1_CINEHELP", DEFAULT_HELP_TEXT_TIME)
							b_has_text_label_triggered[LEM1_CINEHELP] = TRUE
						//ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_FLAHELP] //Explain how to turn on flashlight.
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							WEAPON_TYPE e_current_weapon
							GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), e_current_weapon)
							
							IF e_current_weapon != WEAPONTYPE_UNARMED
							AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), e_current_weapon, WEAPONCOMPONENT_AT_AR_FLSH)
							AND NOT IS_PED_WEAPON_COMPONENT_ACTIVE(PLAYER_PED_ID(), e_current_weapon, WEAPONCOMPONENT_AT_AR_FLSH)
								PRINT_HELP_FOREVER("LEM1_FLAHELP")
								b_has_text_label_triggered[LEM1_FLAHELP] = TRUE
							ELSE
								b_has_text_label_triggered[LEM1_FLAHELP] = TRUE
							ENDIF
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_COVHELP]
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_FLAHELP")
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.068542,-1619.128906,32.033398>>, <<-606.657349,-1617.368164,34.510597>>, 5.500000)
								CLEAR_HELP()
							ELSE
								WEAPON_TYPE e_current_weapon
								GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), e_current_weapon)
								
								IF e_current_weapon != WEAPONTYPE_UNARMED
								AND HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), e_current_weapon, WEAPONCOMPONENT_AT_AR_FLSH)
									IF IS_PED_WEAPON_COMPONENT_ACTIVE(PLAYER_PED_ID(), e_current_weapon, WEAPONCOMPONENT_AT_AR_FLSH)
										IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
											CLEAR_HELP()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_2)
							OR b_force_cover_help_to_trigger
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								AND GET_GAME_TIMER() - i_shooting_stat_help_timer > 4000
									//PRINT_HELP("LEM1_COVHELP") //This help text was moved to another mission.
									b_has_text_label_triggered[LEM1_COVHELP] = TRUE
								ENDIF
							ELSE
								b_has_text_label_triggered[LEM1_COVHELP] = TRUE
								b_has_text_label_triggered[LEM1_WEAPHELP1] = TRUE
								b_has_text_label_triggered[LEM1_WEAPHELP2] = TRUE
							ENDIF
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_ACCSTAT1] //Explain how the stat affects accuracy.
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						AND GET_GAME_TIMER() - i_shooting_stat_help_timer > 4000
							PRINT_HELP("LEM1_ACCSTAT1")
							b_has_text_label_triggered[LEM1_ACCSTAT1] = TRUE
						ENDIF
					ELIF NOT b_has_text_label_triggered[LEM1_ACCSTAT2] //Explain that the shooting range affects the stat (only if the player hasn't been there).
						IF NOT g_savedGlobals.sRangeData[0].m_bSeenTutorial
						    IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("LEM1_ACCSTAT2")
								b_has_text_label_triggered[LEM1_ACCSTAT2] = TRUE
							ENDIF
						ELSE
							b_has_text_label_triggered[LEM1_ACCSTAT2] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Mid-fight checkpoint
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-568.24,-1607.67,30.48>>, <<-577.45,-1606.81,25.76>>, 16.75)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_INTERIOR_MIDDLE, "ESCAPE_FROM_WAREHOUSE")
				e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
			ENDIF
		ENDIF

		IF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE		
			//Grenade help text: triggers when player fights in the warehouse
			IF (NOT IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse) OR NOT IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1))
			AND b_has_text_label_triggered[LEM1_WAIT1] //Don't play until after Lamar's line to get in cover.
			AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_GRENADE)
				IF NOT b_has_text_label_triggered[LEM1_RIOT]
					IF NOT IS_PED_INJURED(s_lamar.ped)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_lamar.ped)) < 400.0 
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-571.895386,-1598.943115,25.497215>>, <<-573.733032,-1614.018188,31.410519>>, 6.750000)
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_RIOT", CONV_PRIORITY_MEDIUM)
								i_grenade_help_timer = GET_GAME_TIMER()
								b_has_text_label_triggered[LEM1_RIOT] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					WEAPON_TYPE current_weapon
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), current_weapon)
				
					IF i_grenade_help_event = 0 //2009070 - New help text for throwing grenades while aiming.
						IF current_weapon = WEAPONTYPE_GRENADE
							i_clear_grenade_help_timer = 0
							i_grenade_help_event++
						ELSE
							IF GET_GAME_TIMER() - i_grenade_help_timer > 2000
								PRINT_HELP("LEM1_GRGUNHELP", DEFAULT_HELP_TEXT_TIME)
								i_clear_grenade_help_timer = 0
								i_grenade_help_event++
							ENDIF
						ENDIF
					ELIF i_grenade_help_event = 1 //Help text on how to select grenades.
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_GRGUNHELP")
							IF i_clear_grenade_help_timer = 0
								IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
								OR IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
									IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_THROW_GRENADE)
										IF GET_WEAPONTYPE_GROUP(current_weapon) != WEAPONGROUP_HEAVY
										AND GET_WEAPONTYPE_GROUP(current_weapon) != WEAPONGROUP_SNIPER 
											i_clear_grenade_help_timer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ELIF GET_GAME_TIMER() - i_clear_grenade_help_timer > 1500
								CLEAR_HELP()
							ENDIF
						ELSE
							IF current_weapon = WEAPONTYPE_GRENADE
								//Timer protects against situation where just as the previous help text ends the player throws a grenade while aiming (this would be classed as equipping grenades).
								IF i_clear_grenade_help_timer = 0
									i_clear_grenade_help_timer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - i_clear_grenade_help_timer > 1500
									i_clear_grenade_help_timer = 0
									i_grenade_help_event++
								ENDIF
							ELSE
								//If the player hasn't equipped a grenade we can go straight to the next help message.
								PRINT_HELP("LEM1_GRENHELP1", DEFAULT_HELP_TEXT_TIME)
								
								i_clear_grenade_help_timer = 0
								i_grenade_help_event++
							ENDIF
						ENDIF
					ELIF i_grenade_help_event = 2 //Help text on how to throw grenades, display once the player has equipped them.
						IF current_weapon = WEAPONTYPE_GRENADE
							IF GET_GAME_TIMER() - i_clear_grenade_help_timer > 1500 //This is to prevent the help text getting triggered by throwing grenades while aiming.
								PRINT_HELP("LEM1_GRENHELP2", DEFAULT_HELP_TEXT_TIME)
								i_grenade_help_event++
							ENDIF
						ELSE
							i_clear_grenade_help_timer = GET_GAME_TIMER()
						ENDIF
					ELIF i_grenade_help_event = 3 //Clear help once the player attacks
						IF current_weapon = WEAPONTYPE_GRENADE
						AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_GRENHELP2")
							IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//Do a check to see when the player hits the exit: this is used for buddy AI in case the player is far ahead.
			IF NOT b_player_reached_exit
				b_player_reached_exit = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.567871,-1630.367554,28.238453>>, <<-587.906555,-1630.824463,23.511858>>, 2.500000)
										OR b_player_escaped
			ELSE
				//Start streaming in the cars outside the plant for the next stage.
				UPDATE_CARJACK_SEQUENCE()
				UPDATE_HOOKER_SEQUENCE()
				
				//Handle the ladder: block the player trying to climb the ladder at the same time as buddies.
				STOP_PLAYER_CLIMBING_LADDER_WITH_BUDDIES(v_player_pos, v_lamar_pos, v_stretch_pos, s_lamar.ped, s_stretch.ped)
				
				//Switch weapons help: triggers when the player reaches outside.
				IF NOT b_has_text_label_triggered[LEM1_WEAPHELP1]
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF GET_NUM_WEAPONS_PED_OWNS(PLAYER_PED_ID()) > 1
							PRINT_HELP("LEM1_WEAPHELP1")
							b_has_text_label_triggered[LEM1_WEAPHELP1] = TRUE
						ENDIF
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_WEAPHELP2]
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_WEAPHELP1")
							CLEAR_HELP()
						ENDIF
					
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("LEM1_WEAPHELP2")
							b_has_text_label_triggered[LEM1_WEAPHELP2] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Climbing ladders help.
				IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_LADDER_HELP)
					IF VDIST2(v_player_pos, <<-592.4, -1641.9, 20.0>>) < 16.0
				   		SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_LADDER_HELP)
					ENDIF
				ENDIF
			ENDIF
			
			IF b_lamar_has_reached_exit				
				//Checks for escaping and climbing the ladder: once it returns true don't check again.
				IF NOT b_player_escaped
					b_player_escaped = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-602.868469,-1699.255005,22.651754>>, <<-607.142883,-1705.014648,26.962690>>, 11.750000)
									   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-630.723877,-1678.827148,23.907534>>, <<-625.278259,-1683.397583,26.224281>>, 3.000000)
									   
					//Allow sprinting again once the player escapes
					IF b_player_escaped
						SET_PLAYER_SPRINT(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
				
				IF NOT b_lamar_escaped
					b_lamar_escaped = IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-608.429260,-1697.496704,22.651754>>, <<-599.133301,-1704.504028,26.062690>>, 3.750000)
									  AND NOT IS_PED_RAGDOLL(s_lamar.ped)
				ENDIF
				
				IF NOT b_stretch_escaped
					b_stretch_escaped = IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-608.429260,-1697.496704,22.651754>>, <<-599.133301,-1704.504028,26.062690>>, 3.750000)
									 	AND NOT IS_PED_RAGDOLL(s_stretch.ped)
				ENDIF
				
				IF NOT b_player_climbed_ladder
					b_player_climbed_ladder = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-588.156738,-1682.519287,18.001923>>, <<-622.193542,-1662.529785,27.975555>>, 62.750000)
				ENDIF
				
				IF NOT b_lamar_climbed_ladder
					b_lamar_climbed_ladder = IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-588.156738,-1682.519287,18.001923>>, <<-622.193542,-1662.529785,27.975555>>, 62.750000)
				ENDIF
				
				IF NOT b_stretch_climbed_ladder
					b_stretch_climbed_ladder = IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-588.156738,-1682.519287,18.001923>>, <<-622.193542,-1662.529785,27.975555>>, 62.750000)
				ENDIF
				
				//Fail if the player escapes without killing everything, leaving the buddies behind.
				IF b_player_escaped
					UPDATE_SETPIECE_COPS()
				
					IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) >= 2
						IF i_fail_timer = 0
							i_fail_timer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - i_fail_timer > 3000
							SET_ENTITY_HEALTH(s_lamar.ped, 0)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT b_player_started_climbing_ladder
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-592.361755,-1642.958374,20.430986>>, <<-593.512878,-1642.292114,25.708050>>, 1.250000)
					OR b_player_climbed_ladder
						b_player_started_climbing_ladder = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//2206541 - Need to disable replay cam movement during the roof run due to Lamar/Stretch getting warped if the player is ahead.
			IF b_player_climbed_ladder
			OR b_player_escaped
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				
				#IF IS_DEBUG_BUILD
					IF b_debug_display_mission_debug
						DRAW_RECT(0.5, 0.5, 0.02, 0.02, 255, 0, 0, 255)
					ENDIF
				#ENDIF
			ENDIF
			
			//B*947896 - Disable climbdowns on the player during the roof run section.
			IF b_player_started_climbing_ladder
			AND NOT b_player_escaped
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableDropDowns, TRUE)
			ENDIF
			
			//B*944434 - Have the wanted level start once the warehouse room has been cleared, Lamar/Stretch say a line.
			IF NOT b_has_text_label_triggered[LEM1_COPSARR]
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse)
				AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1)
					IF i_cops_arrival_timer = 0
						SET_MAX_WANTED_LEVEL(5)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SETUP_MISSION_REQUIREMENT(REQ_AMBIENT_SIRENS)
						PLAY_SOUND_FROM_ENTITY(-1, "LAMAR1_FAKE_POLICE_SIREN2_MASTER", PLAYER_PED_ID())
						SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
						
						i_cops_arrival_timer = GET_GAME_TIMER()
					ELIF GET_GAME_TIMER() - i_cops_arrival_timer > 4000
						IF f_dist_to_lamar < 625.0
							IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COPSARR", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_COPSARR] = TRUE
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SIREN", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_COPSARR] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Play a police report once everyone is outside.
			IF NOT b_triggered_police_report
				IF b_player_started_climbing_ladder
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_LAMAR_1_01")
					b_triggered_police_report = TRUE
				ENDIF
			ENDIF
		ENDIF

		SWITCH i_current_event
			CASE 0
				//Fix for 487089: Clear the exit and ladder in case enything is in the way.
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-607.475403,-1626.929321,26.010830>>, <<-604.238525,-1627.288940,28.510830>>, 1.500000)				
					CLEAR_AREA_OF_OBJECTS(<<-580.4233, -1631.7578, 18.6651>>, 5.0)
					CLEAR_AREA(<<-592.8243, -1642.2301, 18.9575>>, 8.0, TRUE)
					
					i_current_event++
				ENDIF
			BREAK
		
			CASE 1
				//Display "kill heli" god text after Lamar says his line.
				/*IF b_has_text_label_triggered[LEM1_BRIDGE]
					IF NOT b_has_text_label_triggered[LEM1_KILLCHOP]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_NOW("LEM1_KILLCHOP", DEFAULT_GOD_TEXT_TIME, 0)
						b_has_text_label_triggered[LEM1_KILLCHOP] = TRUE
					ENDIF
				ENDIF*/	
				
				//Progress once chopper is dead.
				IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) < 2
					CLEAR_PRINTS()
					
					i_current_event++
				ENDIF
			BREAK
			
			CASE 2
				IF NOT b_player_climbed_ladder
				AND NOT b_player_escaped 
					IF NOT DOES_BLIP_EXIST(blip_current_destination)
						blip_current_destination = CREATE_BLIP_FOR_COORD(<<-593.2421, -1643.4407, 25.1788>>)
					ENDIF
					
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						IF NOT b_has_text_label_triggered[LEM1_GOLAD]
							PRINT_NOW("LEM1_GOLAD", DEFAULT_GOD_TEXT_TIME, 0)
							b_has_text_label_triggered[LEM1_GOLAD] = TRUE
						ENDIF
					ENDIF
				ELSE
					//Player has climbed the ladder (or already escaped), so progress.
					REMOVE_BLIP(blip_current_destination)
					CLEAR_PRINTS()
					i_current_event++
				ENDIF
			BREAK
			
			CASE 3
				IF NOT b_player_escaped
					//Add a blip if the player falls off
					IF NOT DOES_BLIP_EXIST(blip_current_destination)
						IF v_player_pos.z < 21.7459
							blip_current_destination = CREATE_BLIP_FOR_COORD(<<-603.4348, -1696.6965, 25.0436>>)
						ENDIF
					ENDIF
				
					IF v_player_pos.z > 21.7459
						//Print go over wall text: don't print if the alternate route was already revealed.
						//NOTE: currently disabled for 818693.
						IF NOT b_has_text_label_triggered[LEM1_GOWALL] 
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							AND (NOT b_lamar_climbed_ladder OR (b_lamar_climbed_ladder AND b_has_text_label_triggered[LEM1_ROOF2]))
								//PRINT_NOW("LEM1_GOWALL", DEFAULT_GOD_TEXT_TIME, 0)
								b_has_text_label_triggered[LEM1_GOWALL] = TRUE
							ENDIF
						ENDIF
					ELSE
						//No longer need to check this (818693).
						//IF IS_THIS_PRINT_BEING_DISPLAYED("LEM1_GOWALL")
						//	CLEAR_PRINTS()
						//ENDIF
					ENDIF					
				ELSE
					//Wait for buddies to arrive
					IF DOES_BLIP_EXIST(blip_current_destination)
						//This is a good moment to enable the AI scanner.
						SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", FALSE)
					
						REMOVE_BLIP(blip_current_destination)
					ENDIF
					
					IF b_lamar_escaped
						IF b_stretch_escaped
							//If the player escapes last there's a chance that the final music event won't play in time, so play it here if that's the case.
							IF i_current_music_event < 100
								TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_LANDED")
								i_current_music_event = 100
							ENDIF
						
							e_section_stage = SECTION_STAGE_CLEANUP
						ELIF NOT b_has_text_label_triggered[LEM1_WAITSTRET]
							PRINT_NOW("LEM1_WAITSTRET", DEFAULT_GOD_TEXT_TIME, 0)
							b_has_text_label_triggered[LEM1_WAITSTRET] = TRUE
						ENDIF
					ELSE
						IF b_stretch_escaped
							IF NOT b_has_text_label_triggered[LEM1_WAITLEMAR]
								PRINT_NOW("LEM1_WAITLEMAR", DEFAULT_GOD_TEXT_TIME, 0)
								b_has_text_label_triggered[LEM1_WAITLEMAR] = TRUE
							ENDIF
						ELSE
							IF NOT b_has_text_label_triggered[LEM1_WAITBOTH]
								PRINT_NOW("LEM1_WAITBOTH", DEFAULT_GOD_TEXT_TIME, 0)
								b_has_text_label_triggered[LEM1_WAITBOTH] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_ESCAPE_THE_JUNKYARD")
						STOP_AUDIO_SCENE("LAMAR_1_ESCAPE_THE_JUNKYARD")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_STEAL_CAR")
						START_AUDIO_SCENE("LAMAR_1_STEAL_CAR")
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		//Lamar, Stretch and the player play upper-body anims and say a line when going through the smoke.
		IF i_smoke_dialogue_timer != 0
		AND GET_GAME_TIMER() - i_smoke_dialogue_timer < 26000 //This is the current time it takes for a smoke grenade to clear.
			REQUEST_ANIM_DICT("misslamar1lam_1_ig_12")
				
			IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_pre_warehouse)
				IF IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-560.453979,-1600.034424,25.010954>>, <<-560.752319,-1604.481567,28.010954>>, 3.750000)
					IF NOT b_has_text_label_triggered[LEM1_SMOKEANY]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SMOKE2", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_SMOKEANY] = TRUE
					ENDIF
					
					IF NOT b_lamar_alternate_run_active
					AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_12")
					AND NOT IS_PED_INJURED(s_lamar.ped)
						IF IS_PED_RUNNING(s_lamar.ped)
							TASK_PLAY_ANIM(s_lamar.ped, "misslamar1lam_1_ig_12", "run_lamar", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ELIF IS_PED_WALKING(s_lamar.ped)
							TASK_PLAY_ANIM(s_lamar.ped, "misslamar1lam_1_ig_12", "walk_lamar", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ELSE
							TASK_PLAY_ANIM(s_lamar.ped, "misslamar1lam_1_ig_12", "idle_lamar", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ENDIF
						
						b_lamar_alternate_run_active = TRUE
					ENDIF
				ENDIF

				IF IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-560.453979,-1600.034424,25.010954>>, <<-560.752319,-1604.481567,28.010954>>, 3.750000)
					IF NOT b_has_text_label_triggered[LEM1_SMOKEANY]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SMOKE3", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_SMOKEANY] = TRUE
					ENDIF
					
					IF NOT b_stretch_alternate_run_active
					AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_12")
					AND NOT IS_PED_INJURED(s_stretch.ped)
						IF IS_PED_RUNNING(s_stretch.ped)
							TASK_PLAY_ANIM(s_stretch.ped, "misslamar1lam_1_ig_12", "run_stretch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ELIF IS_PED_WALKING(s_stretch.ped)
							TASK_PLAY_ANIM(s_stretch.ped, "misslamar1lam_1_ig_12", "walk_stretch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ELSE
							TASK_PLAY_ANIM(s_stretch.ped, "misslamar1lam_1_ig_12", "idle_stretch", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ENDIF

						b_stretch_alternate_run_active = TRUE
					ENDIF
					
					
				ENDIF

				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-560.453979,-1600.034424,25.010954>>, <<-560.752319,-1604.481567,28.010954>>, 3.750000)
					IF NOT b_has_text_label_triggered[LEM1_SMOKEANY]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					AND CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_SMOKE4", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_SMOKEANY] = TRUE
					ENDIF
					
					IF NOT b_player_alternate_run_active
					AND HAS_ANIM_DICT_LOADED("misslamar1lam_1_ig_12")
						IF IS_PED_RUNNING(PLAYER_PED_ID())
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "misslamar1lam_1_ig_12", "run_franklin", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ELIF IS_PED_WALKING(PLAYER_PED_ID())
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "misslamar1lam_1_ig_12", "walk_franklin", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ELSE
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "misslamar1lam_1_ig_12", "idle_franklin", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, 
										   AF_UPPERBODY | AF_SECONDARY)
						ENDIF
						
						b_player_alternate_run_active = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Dialogue
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
				IF NOT b_has_text_label_triggered[LEM1_FIGHT1] //Franklin, Lamar and Stretch argue.
					IF s_swat_indoor_window_1[0].i_event > 0
						IF NOT b_has_text_label_triggered[LEM1_AHEAD] //Just skip this line if Lamar is already in the locker room.
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIGHT1", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_FIGHT1] = TRUE
							ENDIF
						ELSE						
							b_has_text_label_triggered[LEM1_AHEAD] = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT b_has_text_label_triggered[LEM1_AHEAD]
						IF NOT b_has_text_label_triggered[LEM1_WIN2] //Franklin comments on second window guy
						AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_window_2)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_WIN2", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_WIN2] = TRUE
							ENDIF
						ENDIF
						
						IF NOT b_has_text_label_triggered[LEM1_GOGOS] //Quick comment after corridor guys (NOTE: used to be a separate line, now just use a Lamar line)
						AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_pre_warehouse)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GOGOS", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_GOGOS] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//Play a line if Franklin goes to try the locked door.
					IF NOT b_has_text_label_triggered[LEM1_TRIES]
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-614.366638,-1619.024048,32.010616>>, <<-615.108582,-1618.954102,34.260616>>, 2.500000)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_TRIES", CONV_PRIORITY_MEDIUM)
								
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								b_has_text_label_triggered[LEM1_TRIES] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//"One more left" line: plays in the first room when just one enemy is left.
					IF NOT b_has_text_label_triggered[LEM1_1GUY_1]
						INT i_num_enemies_in_first_room = GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_window_1) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_lift)
					
						IF i_num_enemies_in_first_room = 1
							IF NOT IS_PED_INJURED(s_lamar.ped)
								IF f_dist_to_lamar < 400.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_1GUY", CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
										
										b_has_text_label_triggered[LEM1_1GUY_1] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELIF i_num_enemies_in_first_room = 0
							b_has_text_label_triggered[LEM1_1GUY_1] = TRUE
						ENDIF
					ENDIF
					
					//Play a line from Stretch if Franklin goes to use the elevator.
					IF NOT b_has_text_label_triggered[LEM1_NOLIFT]
						IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_window_1)
						AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_lift)
							IF NOT IS_PED_INJURED(s_stretch.ped)
							AND IS_ENTITY_IN_ANGLED_AREA(s_stretch.ped, <<-613.545105,-1628.704956,32.010616>>, <<-613.048096,-1622.706299,34.268475>>, 3.000000)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-626.348145,-1629.435669,32.010559>>, <<-604.700989,-1630.900879,35.074707>>, 8.250000)
								AND f_dist_to_stretch < 400.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_NOLIFT", CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
										
										b_has_text_label_triggered[LEM1_NOLIFT] = TRUE
									ENDIF
								ENDIF
							ELSE
								//Stretch already left the area, so stop checking this line.
								b_has_text_label_triggered[LEM1_NOLIFT] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
				//Lamar plays a line once the SWAT rappel through the ceiling
				IF NOT b_has_text_label_triggered[LEM1_WAIT1] //Lamar tells player to take cover
				AND s_swat_indoor_roof_1[0].i_event > 0
				AND f_dist_to_lamar < 900.0
				AND f_lamar_height_diff < 5.0
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-571.895386,-1598.943115,25.497215>>, <<-573.733032,-1614.018188,31.410519>>, 6.750000)
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_WAIT1", CONV_PRIORITY_HIGH)
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
																
						b_has_text_label_triggered[LEM1_WAIT1] = TRUE
					ENDIF	
				ENDIF
				
				//Play some dialogue about the fire.
				IF NOT b_has_text_label_triggered[LEM1_FIRE]
					IF i_time_fire_started = 0
						IF s_warehouse_fire[0].f_scale > 0.1
							i_time_fire_started = GET_GAME_TIMER()
						ENDIF
					ELIF GET_GAME_TIMER() - i_time_fire_started > 4000
					AND GET_GAME_TIMER() - i_time_fire_started < 15000
						IF f_dist_to_stretch < 900.0
						AND f_stretch_height_diff < 5.0
							INT i_random = GET_RANDOM_INT_IN_RANGE(0, 3)
							
							IF i_random = 0
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRE", CONV_PRIORITY_HIGH)
									b_has_text_label_triggered[LEM1_FIRE] = TRUE
								ENDIF	
							ELIF i_random = 1
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRE2", CONV_PRIORITY_HIGH)
									b_has_text_label_triggered[LEM1_FIRE] = TRUE
								ENDIF	
							ELSE
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FIRE3", CONV_PRIORITY_HIGH)
									b_has_text_label_triggered[LEM1_FIRE] = TRUE
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				//Play some dialogue when there are a couple of guys left in 
				IF NOT b_has_text_label_triggered[LEM1_TWOLEFTWARE] 
					INT i_num_enemies_in_warehouse = GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_warehouse) + GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_roof_1)
				
					IF i_num_enemies_in_warehouse = 2
						IF i_warehouse_dialogue_timer = 0
							i_warehouse_dialogue_timer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - i_warehouse_dialogue_timer > 1000
							IF f_dist_to_lamar < 400.0
							AND f_lamar_height_diff < 5.0
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_LGUY", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_TWOLEFTWARE] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELIF i_num_enemies_in_warehouse < 2
						b_has_text_label_triggered[LEM1_TWOLEFTWARE] = TRUE
					ELSE
						i_warehouse_dialogue_timer = 0
					ENDIF
				ENDIF
			
				//Play some dialogue if Franklin hits a ped with a grenade
				IF NOT b_has_text_label_triggered[LEM1_RIOT2] 
				AND b_has_text_label_triggered[LEM1_RIOT]
				AND b_play_grenade_kill_dialogue
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_RIOT2", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_RIOT2] = TRUE
					ENDIF
				ENDIF
			
				//Franklin notices the exit.
				IF NOT b_has_text_label_triggered[LEM1_EXIT] 
				AND f_dist_to_lamar < 900.0
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-593.004456,-1630.942261,26.010828>>, <<-607.757080,-1629.696289,28.260826>>, 4.000000)
						IF IS_SPHERE_VISIBLE(<<-593.0891, -1630.2500, 27.3027>>, 1.0)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-593.077393,-1630.831787,26.010954>>, <<-604.387695,-1630.007935,28.510954>>, 4.000000)
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_EXIT", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_EXIT] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT b_has_text_label_triggered[LEM1_EXITH] 
					//Franklin spots the chopper outside: play dialogue and a music event
					IF s_swat_indoor_chopper_final[0].i_event > 0
					AND IS_VEHICLE_DRIVEABLE(veh_chopper_indoor_final)
						IF NOT IS_ENTITY_OCCLUDED(veh_chopper_indoor_final)
							IF i_chopper_dialogue_timer = 0
								i_chopper_dialogue_timer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - i_chopper_dialogue_timer > 800
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_EXITH", CONV_PRIORITY_MEDIUM)
									
									REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
									
									b_has_text_label_triggered[LEM1_EXITH] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_BRIDGE]
					//Lamar tells Franklin to kill the chopper if he hasn't already.
					IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) >= 2
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_BRIDGE", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_BRIDGE] = TRUE
						ENDIF
					ELSE
						b_has_text_label_triggered[LEM1_BRIDGE] = TRUE
					ENDIF
				ENDIF
				
				//Lamar leads the way over the rooftops once the chopper is dead
				IF NOT b_lamar_escaped
				AND NOT b_player_escaped
					IF NOT b_has_text_label_triggered[LEM1_ROOF2] 
					AND (b_player_climbed_ladder OR s_lamar.i_event = 100)
					AND b_lamar_climbed_ladder 
					AND GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) < 2
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_ROOF2", CONV_PRIORITY_MEDIUM)
							
							REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
							
							b_has_text_label_triggered[LEM1_ROOF2] = TRUE
						ENDIF
					ENDIF
					
					//Lamar points out the wall as he gets near to it
					IF NOT b_has_text_label_triggered[LEM1_ROOF3] 
					AND f_dist_to_lamar < 400.0
					AND IS_ENTITY_IN_ANGLED_AREA(s_lamar.ped, <<-602.194092,-1697.792480,24.043589>>, <<-599.153015,-1692.315308,27.310705>>, 7.000000)
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_ROOF3", CONV_PRIORITY_MEDIUM)
							
							REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
							
							b_has_text_label_triggered[LEM1_ROOF3] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Stretch complains if the player is lagging behind right at the end.
				IF b_lamar_escaped
				AND b_stretch_escaped
				AND NOT b_player_escaped
					IF f_dist_to_lamar < 625.0
						IF NOT b_has_text_label_triggered[LEM1_KEEPUP_END]
							IF i_num_times_played_ladder_hurry_line < 4
								IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_KEEPUP", CONV_PRIORITY_MEDIUM)
									b_has_text_label_triggered[LEM1_KEEPUP_END] = TRUE
									i_num_times_played_ladder_hurry_line++
								ENDIF
							ELSE	
								b_has_text_label_triggered[LEM1_KEEPUP_END] = TRUE
							ENDIF
						ELIF NOT b_has_text_label_triggered[LEM1_KEEPUPS]
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_KEEPUPS", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_KEEPUPS] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//If the player just hangs around without climbing the wall then eventually trigger the cops anyway.
					IF NOT b_cops_triggered_due_to_griefing
						IF GET_GAME_TIMER() - i_time_lamar_and_stretch_escaped > 60000
							SET_FAKE_WANTED_LEVEL(0)
							SET_WANTED_LEVEL_MULTIPLIER(1.0)
							SET_MAX_WANTED_LEVEL(5)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
							UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							SET_POLICE_RADAR_BLIPS(TRUE)
							SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
							ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
							
							b_cops_triggered_due_to_griefing = TRUE
						ENDIF
					ELSE
						SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
						SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						
						//If Lamar are Stretch finished their carjack sequence then get them into combat.
						IF i_carjack_event = 100
							IF GET_GAME_TIMER() - i_time_lamar_and_stretch_escaped > 90000
								IF NOT IS_PED_INJURED(s_lamar.ped)
								AND GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_lamar.ped, 200.0)
								ENDIF
								
								IF NOT IS_PED_INJURED(s_stretch.ped)
								AND GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(s_stretch.ped, 200.0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					i_time_lamar_and_stretch_escaped = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
			//Generic lines that happen if the player/buddies are in combat.
			IF (GET_GAME_TIMER() - i_player_combat_dialogue_timer > 0 OR GET_GAME_TIMER() - i_buddy_combat_dialogue_timer > 0)
			AND b_has_text_label_triggered[LEM1_FIGHT1] //Don't start until after the first conversation plays after running into the room.
			AND NOT IS_PED_INJURED(s_lamar.ped) 
			AND NOT IS_PED_INJURED(s_stretch.ped)
			AND NOT b_cops_triggered_due_to_griefing
				IF NOT IS_PED_RAGDOLL(s_lamar.ped) //This stops generic lines getting in the way of the breach SWAT dialogue (Lamar ragdolls during the breach).
				AND (IS_PED_IN_COMBAT(s_lamar.ped) OR IS_PED_SHOOTING(s_lamar.ped) OR IS_PED_IN_COMBAT(s_stretch.ped) OR IS_PED_SHOOTING(s_stretch.ped))
					IF GET_GAME_TIMER() - i_combat_dialogue_wait_time > 0
						//Player dialogue when shooting.
						IF GET_GAME_TIMER() - i_player_combat_dialogue_timer > 0
						AND IS_PED_SHOOTING(PLAYER_PED_ID())
						AND f_dist_to_lamar < 625.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GENCOM", CONV_PRIORITY_MEDIUM)
								i_player_combat_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 11000)
							ENDIF
						ENDIF
						
						//More lines for the player, he doesn't need to be shooting for these.
						IF GET_GAME_TIMER() - i_player_combat_dialogue_timer_2 > 0
						AND f_dist_to_lamar < 625.0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_FRAK", CONV_PRIORITY_MEDIUM)
								i_player_combat_dialogue_timer_2 = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(14000, 17000)
							ENDIF
						ENDIF
						
						//Buddy dialogue
						IF GET_GAME_TIMER() - i_buddy_combat_dialogue_timer > 0								
							IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0 
								//Lamar speaks
								IF (IS_PED_IN_COMBAT(s_lamar.ped) OR IS_PED_SHOOTING(s_lamar.ped))
								AND f_dist_to_lamar < 625.0
								AND f_lamar_height_diff < 5.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GENL2", CONV_PRIORITY_MEDIUM)
										i_buddy_combat_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 11000)
									ENDIF
								ENDIF
							ELSE 
								//Stretch speaks
								IF (IS_PED_IN_COMBAT(s_stretch.ped) OR IS_PED_SHOOTING(s_stretch.ped))
								AND f_dist_to_stretch < 625.0
								AND f_stretch_height_diff < 5.0
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GENSTR", CONV_PRIORITY_MEDIUM)
										i_buddy_combat_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 11000)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						i_combat_dialogue_wait_time = 0
					ENDIF
				ELSE
					//Have a grace period so peds don't suddenly start firing dialogue as soon as they enter combat.
					i_combat_dialogue_wait_time = GET_GAME_TIMER() + 1000
				ENDIF
			ENDIF
		ENDIF
		
		//Audio events
		SWITCH i_audio_scene_event_shootout
			CASE 0
				IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_START")
					START_AUDIO_SCENE("LAMAR_1_SHOOTOUT_START")
				ENDIF
				
				i_audio_scene_event_shootout++
			BREAK
			
			CASE 1
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_breach)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-565.486328,-1631.969360,32.090351>>, <<-565.017029,-1627.817139,34.777401>>, 1.750000)
						IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_START")
							STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_START")
						ENDIF
						
						IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_SAVE_LAMAR")
							STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_SAVE_LAMAR")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_STAIRWELL")
							START_AUDIO_SCENE("LAMAR_1_SHOOTOUT_STAIRWELL")
						ENDIF
						
						i_audio_scene_event_shootout = 50
					ENDIF
				ENDIF
			BREAK
			
			CASE 50
				IF e_mission_stage = STAGE_ESCAPE_FROM_WAREHOUSE
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_STAIRWELL")
						STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_STAIRWELL")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_WAREHOUSE")
						START_AUDIO_SCENE("LAMAR_1_SHOOTOUT_WAREHOUSE")
					ENDIF
					
					i_audio_scene_event_shootout++
				ENDIF
			BREAK
			
			CASE 51
				IF IS_ENEMY_GROUP_DEAD(s_swat_indoor_warehouse)
				AND IS_ENEMY_GROUP_DEAD(s_swat_indoor_roof_1)
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_WAREHOUSE")
						STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_WAREHOUSE")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_GET_OUTSIDE")
						START_AUDIO_SCENE("LAMAR_1_SHOOTOUT_GET_OUTSIDE")
					ENDIF
					
					i_audio_scene_event_shootout++
				ENDIF
			BREAK
			
			CASE 52
				IF s_swat_indoor_chopper_final[0].i_event > 0
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_SHOOTOUT_GET_OUTSIDE")
						STOP_AUDIO_SCENE("LAMAR_1_SHOOTOUT_GET_OUTSIDE")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_ESCAPE_FIGHT_HELI")
						START_AUDIO_SCENE("LAMAR_1_ESCAPE_FIGHT_HELI")
					ENDIF
					
					i_audio_scene_event_shootout++
				ENDIF
			BREAK
			
			CASE 53
				IF GET_NUM_ENEMIES_ALIVE_IN_GROUP(s_swat_indoor_chopper_final) < 2
					IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_ESCAPE_FIGHT_HELI")
						STOP_AUDIO_SCENE("LAMAR_1_ESCAPE_FIGHT_HELI")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_ESCAPE_THE_JUNKYARD")
						START_AUDIO_SCENE("LAMAR_1_ESCAPE_THE_JUNKYARD")
					ENDIF
					
					i_audio_scene_event_shootout++
				ENDIF
			BREAK
		ENDSWITCH
		
		//Delete D once the player moves away from him.
		IF DOES_ENTITY_EXIST(ped_ballas_og)
			IF VDIST2(v_player_pos, GET_ENTITY_COORDS(ped_ballas_og, FALSE)) > 2500.0
				REMOVE_PED(ped_ballas_og)
			ENDIF
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		CLEAR_PRINTS()
		CLEAR_HELP()
		REMOVE_ALL_BLIPS()
		
		PAUSE_CLOCK(FALSE)

		//Remove objects from shootout
		REMOVE_OBJECT(obj_warehouse_gas)
		REMOVE_OBJECT(obj_breach_door)
		REMOVE_OBJECT(obj_wooden_box, FALSE)

		SET_MODEL_AS_NO_LONGER_NEEDED(model_swat)
		SET_MODEL_AS_NO_LONGER_NEEDED(model_gang)
		REMOVE_ALL_SHOOTOUT_SYNCED_SCENE_ANIMS()
		
		//Remove any enemies that could be lingering
		REMOVE_ENEMY_GROUP(s_swat_indoor_chopper_final)
		REMOVE_VEHICLE(veh_chopper_indoor_final)	

		REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_1)
		REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_2)
		REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, SWAT_SPEAKER_3)
		
		SET_SCRIPTED_SHOOTOUT_COVER_ACTIVE(FALSE)

		IF interior_recycle != NULL
			IF IS_INTERIOR_READY(interior_recycle)
				UNPIN_INTERIOR(interior_recycle)
				interior_recycle = NULL
			ENDIF
		ENDIF

		REPEAT COUNT_OF(s_warehouse_fire) i
			IF s_warehouse_fire[i].ptfx != NULL
				STOP_PARTICLE_FX_LOOPED(s_warehouse_fire[i].ptfx)
				s_warehouse_fire[i].ptfx = NULL
			ENDIF
			
			s_warehouse_fire[i].f_scale = 0.0
		ENDREPEAT
		
		REPEAT COUNT_OF(s_warehouse_fire_2) i
			IF s_warehouse_fire_2[i].ptfx != NULL
				STOP_PARTICLE_FX_LOOPED(s_warehouse_fire_2[i].ptfx)
				s_warehouse_fire_2[i].ptfx = NULL
			ENDIF
			
			s_warehouse_fire_2[i].f_scale = 0.0
		ENDREPEAT
		
		REPEAT COUNT_OF(ptfxDebris) i
			IF ptfxDebris[i] != NULL
				STOP_PARTICLE_FX_LOOPED(ptfxDebris[i])
				ptfxDebris[i] = NULL
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(obj_lights) i
			REMOVE_OBJECT(obj_lights[i], TRUE)
		ENDREPEAT
		
		REPEAT COUNT_OF(obj_rope_base) i
			REMOVE_OBJECT(obj_rope_base[i], TRUE)
		ENDREPEAT
		
		REPEAT COUNT_OF(rope_lights) i
			IF rope_lights[i] != NULL
				DELETE_ROPE(rope_lights[i])
				rope_lights[i] = NULL
			ENDIF
		ENDREPEAT
		
		REMOVE_PED(ped_ballas_og)
		
		//Do an update on the carjack sequences in case the final frame didn't trigger them.
		//UPDATE_CARJACK_SEQUENCE()
		//UPDATE_HOOKER_SEQUENCE()
		
		REMOVE_PTFX_ASSET()
		REMOVE_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
		REMOVE_WEAPON_ASSET(WEAPONTYPE_SMG)

		REMOVE_COVER_POINT(s_lamar.cover)
		REMOVE_COVER_POINT(s_stretch.cover)
		REMOVE_COVER_POINT(cover_player)
		REMOVE_COVER_POINT(cover_locker)
		
		ASSISTED_MOVEMENT_REMOVE_ROUTE("Lemar1")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("Lemar2")
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		
		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_GO_BACK_HOME
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		IF e_mission_stage = STAGE_ESCAPE_PRE_WAREHOUSE
			JUMP_TO_STAGE(STAGE_ESCAPE_FROM_WAREHOUSE)
		ELSE
			JUMP_TO_STAGE(STAGE_GO_BACK_HOME)
		ENDIF
	ENDIF
ENDPROC

PROC GO_BACK_HOME()
	IF e_section_stage = SECTION_STAGE_SETUP
		IF b_is_jumping_directly_to_stage
			IF b_used_a_checkpoint
				START_REPLAY_SETUP(<<-604.4547, -1700.7247, 22.9864>>, 153.9423, FALSE)
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-604.4547, -1700.7247, 22.9864>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 153.9423)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				WAIT(0)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			WHILE NOT DOES_ENTITY_EXIST(s_lamar.ped)
			OR NOT DOES_ENTITY_EXIST(s_stretch.ped)
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, <<-607.1035, -1701.2744, 23.0408>>, 280.0961)
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, <<-601.7869, -1701.0348, 22.9121>>, 160.0113)
				
				WAIT(0)
			ENDWHILE
			
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_AMBIENCE_REMOVED_FOR_FIGHT, <<0.0, 0.0, 0.0>>)
			SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH_AND_LAMAR_READY_TO_FIGHT, <<0.0, 0.0, 0.0>>)
			SETUP_MISSION_REQUIREMENT(REQ_AMBIENT_SIRENS)
			
			//Setup the cars used in the carjacking
			i_carjack_event = 0
			i_hooker_event = 0
			
			WHILE i_carjack_event < 100
				UPDATE_CARJACK_SEQUENCE()
				UPDATE_HOOKER_SEQUENCE()
				
				WAIT(0)
			ENDWHILE
			
			END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
			SETTIMERB(0)
				
			//Wait for clothes to stream in
			WHILE TIMERB() < 10000
				IF (NOT IS_PED_INJURED(s_lamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped))
				AND (NOT IS_PED_INJURED(s_stretch.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped))
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					SETTIMERB(100000)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			WAIT(500)	//Let the carjack dead body settle.
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			#IF IS_DEBUG_BUILD
				SET_CLOCK_TIME(2, 0, 0)
			#ENDIF
			
			i_current_event = 99
			b_player_escaped = TRUE
			b_getaway_car_driver_is_dead = FALSE
			b_is_jumping_directly_to_stage = FALSE
			b_skipped_to_lose_cops_stage = TRUE
		ELSE
			//Reset multipliers to defaults, and give the player a proper wanted level.			
			SET_FAKE_WANTED_LEVEL(0)
			SET_MAX_WANTED_LEVEL(5)
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
			UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_POLICE_RADAR_BLIPS(TRUE)
			SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
			SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
		
			SET_CREATE_RANDOM_COPS(FALSE)
			
			//Reset some of the shootout changes for buddies: e.g. no ragdoll etc
			IF NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_INJURED(s_stretch.ped)
				//CLEAR_PED_TASKS(s_lamar.ped)
				SET_RAGDOLL_BLOCKING_FLAGS(s_lamar.ped, RBF_NONE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_lamar.ped, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_lamar.ped, FALSE)
				SET_ENTITY_PROOFS(s_lamar.ped, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(s_lamar.ped, CA_DO_DRIVEBYS, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(s_lamar.ped, CA_RESTRICT_IN_VEHICLE_AIMING_TO_CURRENT_SIDE, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_lamar.ped, FALSE)
				SET_PED_ALLOWED_TO_DUCK(s_lamar.ped, TRUE)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(s_lamar.ped, TRUE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_UseKinematicModeWhenStationary, FALSE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_RunFromFiresAndExplosions, TRUE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_DisableExplosionReactions, FALSE)
				SET_PED_CONFIG_FLAG(s_lamar.ped, PCF_DisableHurt, FALSE)
				
				
				//CLEAR_PED_TASKS(s_stretch.ped)
				SET_RAGDOLL_BLOCKING_FLAGS(s_stretch.ped, RBF_NONE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(s_stretch.ped, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_stretch.ped, FALSE)
				SET_ENTITY_PROOFS(s_stretch.ped, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_DO_DRIVEBYS, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(s_stretch.ped, CA_RESTRICT_IN_VEHICLE_AIMING_TO_CURRENT_SIDE, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(s_stretch.ped, FALSE)
				SET_PED_ALLOWED_TO_DUCK(s_stretch.ped, TRUE)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(s_stretch.ped, TRUE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_UseKinematicModeWhenStationary, FALSE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_RunFromFiresAndExplosions, TRUE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_DisableExplosionReactions, FALSE)
				SET_PED_CONFIG_FLAG(s_stretch.ped, PCF_DisableHurt, FALSE)
			ENDIF
		
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
			
			IF NOT b_buddies_set_to_attract_cops
				//SET_RELATIONSHIP_GROUP_AFFECTS_WANTED_LEVEL(RELGROUPHASH_PLAYER, TRUE)
				b_buddies_set_to_attract_cops = TRUE
			ENDIF
		
			//If the gantry chopper is still around from the previous section switch it to normal cop behaviour.
			IF IS_VEHICLE_DRIVEABLE(veh_chopper_gantry)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_chopper_gantry)
					STOP_PLAYBACK_RECORDED_VEHICLE(veh_chopper_gantry)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(s_cops_chopper_gantry[0].ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_cops_chopper_gantry[0].ped, FALSE)
				CLEAR_PED_TASKS(s_cops_chopper_gantry[0].ped)
				//TASK_COMBAT_PED(s_cops_chopper_gantry[0].ped, PLAYER_PED_ID())
				//SET_PED_KEEP_TASK(s_cops_chopper_gantry[0].ped, TRUE)
			ENDIF
			
			IF DOES_BLIP_EXIST(s_cops_chopper_gantry[0].blip)
				REMOVE_BLIP(s_cops_chopper_gantry[0].blip)
			ENDIF
			
			REMOVE_PED(s_cops_chopper_gantry[0].ped, FALSE)
			REMOVE_VEHICLE(veh_chopper_gantry, FALSE)
		
			REMOVE_ALL_AUDIO_SCENES()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_STEAL_CAR")
				START_AUDIO_SCENE("LAMAR_1_STEAL_CAR")
			ENDIF
			
			SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", FALSE)
			
			IF IS_SCREEN_FADED_OUT()				
				GIVE_PLAYER_CORRECT_WEAPON_AFTER_FAIL()
				
				//Restart the music
				TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_CLIMB_LADDER_RESTART")
				
				b_buddies_need_to_switch_to_group = TRUE //Use this to force locates header straight away if skipping.
			ELSE
				b_buddies_need_to_switch_to_group = FALSE
			ENDIF
			
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_R_R, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_RECYCLING_PLANT_F_R, DOORSTATE_LOCKED)
		
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				WHILE NOT IS_SCREEN_FADED_IN()
					UPDATE_CARJACK_SEQUENCE()
					UPDATE_HOOKER_SEQUENCE()
					
					WAIT(0)
				ENDWHILE
			ENDIF
		
			DO_FADE_IN_WITH_WAIT()
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			b_cops_relationship_set = FALSE
			b_taxi_drop_off_updated = FALSE
			b_wanted_level_difficulty_lowered = FALSE
			b_already_been_in_a_car = FALSE
			b_players_tyres_set_to_not_burst = FALSE
			
			//b_current_car_added_to_mix_group = FALSE
			i_lose_cops_music_event = 0
			i_current_event = 0
			i_num_lose_cops_lines_played_1 = 0
			i_num_lose_cops_lines_played_2 = 0
			i_tyre_burst_timer = 0
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(LAM1_COP_LOSS_TIME)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_LOSE_COPS, "LOSE_COPS", TRUE)
			
			b_has_text_label_triggered[LEM1_WAITLEMAR] = FALSE //Just in case a previous section used this god text
			b_already_been_in_a_car = TRUE
			
			//Taxi
			DISABLE_TAXI_HAILING(TRUE)
			DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
      		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
			
			SETTIMERA(0)
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		#IF IS_DEBUG_BUILD
			DONT_DO_J_SKIP(s_locates_data)
		#ENDIF

		PED_INDEX peds[10]
		INT i = 0

		//SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_RUN)
		
		HANDLE_BUDDY_PERMANENT_HEAD_TRACK(s_lamar.ped)
		HANDLE_BUDDY_PERMANENT_HEAD_TRACK(s_stretch.ped)
		UPDATE_SETPIECE_COPS()
		
		BOOL b_lose_cops_dialogue_played_this_frame = FALSE
		BOOL b_go_home_dialogue_played_this_frame = FALSE
		
		//Remove the ambient cop noises once the player loses the cops
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			SETTIMERA(0)
		
			IF b_wanted_level_difficulty_lowered
				RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
				b_wanted_level_difficulty_lowered = FALSE
			ENDIF
			
			//If the wanted level was somehow already lost then just mark this label as triggered so the mission can progress.
			IF NOT b_has_text_label_triggered[LEM1_COPS1]
				b_has_text_label_triggered[LEM1_COPS1] = TRUE
			ENDIF
		
			IF b_cops_relationship_set
				CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
				b_cops_relationship_set = FALSE
				
				IF NOT IS_PED_INJURED(s_lamar.ped)
					IF GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_DRIVE_BY) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(s_lamar.ped, SCRIPT_TASK_COMBAT) = PERFORMING_TASK
						CLEAR_PED_TASKS(s_lamar.ped)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(s_stretch.ped)
					IF GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_DRIVE_BY) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(s_stretch.ped, SCRIPT_TASK_COMBAT) = PERFORMING_TASK
						CLEAR_PED_TASKS(s_stretch.ped)
					ENDIF
				ENDIF
			ENDIF
		
			DISTANT_COP_CAR_SIRENS(FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(model_cop)
			SET_MODEL_AS_NO_LONGER_NEEDED(model_cop_car)
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT b_has_text_label_triggered[LEM1_COPS4]
					IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COPS4", CONV_PRIORITY_MEDIUM)
						b_has_text_label_triggered[LEM1_COPS4] = TRUE
						b_go_home_dialogue_played_this_frame = TRUE
					ENDIF
				ELIF NOT b_has_text_label_triggered[LEM1_HOUSE]
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_HOUSE", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_HOUSE] = TRUE
							b_go_home_dialogue_played_this_frame = TRUE
						ENDIF
					ELSE
						b_has_text_label_triggered[LEM1_HOUSE] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_LOSE_COPS")
				STOP_AUDIO_SCENE("LAMAR_1_LOSE_COPS")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_DRIVE_HOME")
				START_AUDIO_SCENE("LAMAR_1_DRIVE_HOME")
			ENDIF
		ELSE
			//Lower the wanted level difficulty after some time.
			IF NOT b_wanted_level_difficulty_lowered
				IF TIMERA() > 60000
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_GOLF_COURSE)
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_AIRPORT_AIRSIDE)
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_PRISON)
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_BIOTECH)
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_DOCKS)
				AND NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MOVIE_STUDIO)
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0.0)
					b_wanted_level_difficulty_lowered = TRUE
				ENDIF
			ENDIF
			
			//Print some help on how to lose cops after a few seconds.
			IF NOT b_has_text_label_triggered[LEM1_SPECHELP]
				IF TIMERA() < 7000
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
				
				IF TIMERA() > 7000
				AND ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
				AND ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
					IF IS_REPLAY_IN_PROGRESS()
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP("LEM1_SPECHELP_KM")
						ELSE
							PRINT_HELP("LEM1_SPECHELP")
						ENDIF
					ENDIF
					
					b_has_text_label_triggered[LEM1_SPECHELP] = TRUE
				ENDIF
			ELIF NOT b_has_text_label_triggered[LEM1_ALLEYHELP]	//After the special ability help, prompt help for hiding in alleys
				
				BOOL bPCHelpShown = FALSE
				
				// Need to separate this out as PC text isn't included in console builds.
				IF IS_PC_VERSION()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_SPECHELP_KM")
						bPCHelpShown = TRUE
					ENDIF
				ENDIF
				
				IF NOT b_has_text_label_triggered[LEM1_ALLEYHELP]
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_SPECHELP")
					AND NOT bPCHelpShown
					AND ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
					AND ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
						PRINT_HELP("LEM1_ALLEYHELP")
						b_has_text_label_triggered[LEM1_ALLEYHELP] = TRUE
					ENDIF
				ENDIF
			ELSE	//After the alley help, prompt help for looking behind
				/*IF NOT b_has_text_label_triggered[LEM1_LOOKHELP]
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LEM1_ALLEYHELP")
					AND ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
					AND ARE_PEDS_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
						PRINT_HELP("LEM1_LOOKHELP")
						b_has_text_label_triggered[LEM1_LOOKHELP] = TRUE
					ENDIF
				ENDIF*/
			ENDIF
			
			//If the buddies have been left behind, make sure they influence the wanted level. 
			IF NOT IS_PED_INJURED(s_lamar.ped)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_lamar.ped)) > 2500.0
					IF IS_PED_IN_COMBAT(s_lamar.ped)
						UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
						SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
						SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(s_stretch.ped)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_stretch.ped)) > 2500.0
					IF IS_PED_IN_COMBAT(s_stretch.ped)
						UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
						SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
						SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT b_cops_relationship_set
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_COP)
				b_cops_relationship_set = TRUE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_AUDIO_SCENE_ACTIVE("LAMAR_1_LOSE_COPS")
					START_AUDIO_SCENE("LAMAR_1_LOSE_COPS")
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_DRIVE_HOME")
					STOP_AUDIO_SCENE("LAMAR_1_DRIVE_HOME")
				ENDIF				
			ENDIF
		
			//Dialogue while losing the cops
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT b_has_text_label_triggered[LEM1_COPS1]
					//If the carjack guy spawned already dead then play a line explaining it.
					IF b_getaway_car_driver_is_dead
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_DEAD", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_COPS1] = TRUE
							b_lose_cops_dialogue_played_this_frame = TRUE
						ENDIF
					ELSE
						//Randomly pick Lamar or Stretch to say the cops line
						IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COPS1", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_COPS1] = TRUE
								b_lose_cops_dialogue_played_this_frame = TRUE
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_STRCOPS", CONV_PRIORITY_MEDIUM)
								b_has_text_label_triggered[LEM1_COPS1] = TRUE
								b_lose_cops_dialogue_played_this_frame = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_GAME_TIMER() - i_lose_cops_dialogue_timer > 13000 + (i_num_lose_cops_lines_played_1 * 500)
						IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
						AND ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
						AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
							IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
								IF GET_RANDOM_INT_IN_RANGE(0, 8) = 0
									//One line in particular only makes sense if the player is driving.
									IF NOT b_has_text_label_triggered[LEM1_COPS2b]
										VEHICLE_INDEX veh_player
										veh_player = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										
										IF NOT IS_ENTITY_DEAD(veh_player)
											IF GET_ENTITY_SPEED(veh_player) > 10.0
												IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COPS2B", CONV_PRIORITY_MEDIUM)
													b_has_text_label_triggered[LEM1_COPS2B] = TRUE
													i_lose_cops_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(-3000, 2000)
													b_lose_cops_dialogue_played_this_frame = TRUE
													i_num_lose_cops_lines_played_1++
												ENDIF
											ENDIF
										ENDIF
									ENDIF	
								ELSE
									IF i_num_lose_cops_lines_played_1 < 7
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COPS2", CONV_PRIORITY_MEDIUM)
											i_lose_cops_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(-3000, 2000)
											b_lose_cops_dialogue_played_this_frame = TRUE
											i_num_lose_cops_lines_played_1++
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF i_num_lose_cops_lines_played_2 < 7
									IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_COPS3", CONV_PRIORITY_MEDIUM)
										i_lose_cops_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(-3000, 2000)
										b_lose_cops_dialogue_played_this_frame = TRUE
										i_num_lose_cops_lines_played_2++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(s_lamar.blip)
			REMOVE_ALL_BLIPS()
		ENDIF
		
		UPDATE_CARJACK_SEQUENCE()
		UPDATE_HOOKER_SEQUENCE()
		
		IF b_has_text_label_triggered[LEM1_COPS1]
		AND NOT b_lose_cops_dialogue_played_this_frame
			//If the "let's go back home" line hasn't printed yet after losing the cops then don't allow the god text to print before it.
			TEXT_LABEL str_go_home = "LEM1_GOFRANKLIN"
			IF NOT b_has_text_label_triggered[LEM1_HOUSE]
			OR b_go_home_dialogue_played_this_frame
				str_go_home = ""
				CLEAR_BIT(s_locates_data.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
			ENDIF
		
			SWITCH i_current_event
				CASE 0
					IF i_carjack_event = 100 //Lamar and Stretch have finished their carjack tasks.
						IF NOT b_already_been_in_a_car
							IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
							AND ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
								b_already_been_in_a_car = TRUE
							ENDIF
						ENDIF
					
						IF NOT b_buddies_need_to_switch_to_group
							GET_PED_NEARBY_PEDS(s_lamar.ped, peds)
							
							REPEAT COUNT_OF(peds) i
								IF NOT IS_PED_INJURED(peds[i])
									IF (GET_ENTITY_MODEL(peds[i]) = S_M_Y_COP_01 OR GET_ENTITY_MODEL(peds[i]) = S_M_Y_SWAT_01)
										IF VDIST2(GET_ENTITY_COORDS(peds[i]), GET_ENTITY_COORDS(s_lamar.ped, FALSE)) < 10000.0
											b_buddies_need_to_switch_to_group = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
								b_buddies_need_to_switch_to_group = TRUE
							ENDIF
						ELSE
							//Once they get in a car allow them to reach the locate by any means from then on.
							IF b_already_been_in_a_car
								IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(s_locates_data, v_end_pos, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, TRUE, s_lamar.ped, s_stretch.ped, NULL,
																			 	str_go_home, "LEM1_LEFTLEMAR", "LEM1_LEFTSTRET", "", "LEM1_LEFTBOTH", FALSE, TRUE)
								ENDIF
							ELSE
								IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_ANY_VEHICLE(s_locates_data, v_end_pos, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, TRUE, s_lamar.ped, s_stretch.ped, NULL,
																			 		 str_go_home, "LEM1_LEFTLEMAR", "LEM1_LEFTSTRET", "", "LEM1_LEFTBOTH", "LEM1_FINDCAR", "LEM1_BACKVEH", FALSE,
																			 		 TRUE, 3)
								ENDIF
							ENDIF
						ENDIF

						//Progress once reaching the garage.
						IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-23.994032,-1431.218994,29.655334>>, <<-23.948074,-1443.376343,32.404202>>, 4.750000)
								CLEAR_MISSION_LOCATE_STUFF(s_locates_data)
								
								IF IS_MOBILE_PHONE_CALL_ONGOING()
									HANG_UP_AND_PUT_AWAY_PHONE()
								ENDIF
								
								REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
								
								i_current_event++	
							ENDIF
						ENDIF
						
						//Dialogue after losing the cops: locates god text takes priority so check this after checking the locate.
						IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
						AND ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_lamar.ped)
						AND ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), s_stretch.ped)
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
							
							IF NOT b_has_text_label_triggered[LEM1_BACK]
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT b_has_text_label_triggered[LEM1_CHAT2]
										IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_CHAT2", CONV_PRIORITY_MEDIUM)
											b_has_text_label_triggered[LEM1_CHAT2] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF b_has_text_label_triggered[LEM1_HOME]
							OR b_has_text_label_triggered[LEM1_HOME2]
							OR b_has_text_label_triggered[LEM1_CHAT1]
							OR b_has_text_label_triggered[LEM1_CHAT2]
								IF NOT b_has_text_label_triggered[LEM1_BACK]
									IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
										PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					BOOL b_has_stopped
					VEHICLE_INDEX veh_final_car
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						veh_final_car = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(veh_final_car)
					AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
					
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(veh_final_car, DEFAULT, 5)
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
							ENDIF
							b_has_stopped = TRUE
						ENDIF
					ELSE
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE)
						ENDIF
						b_has_stopped = TRUE
					ENDIF
										
					IF NOT b_has_text_label_triggered[LEM1_BACK]
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					
						IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_BACK", CONV_PRIORITY_MEDIUM)
							b_has_text_label_triggered[LEM1_BACK] = TRUE
						ELSE
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ELSE
						IF b_has_stopped
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
							e_section_stage = SECTION_STAGE_CLEANUP
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		//Play a final conversation when getting near the exit.
		IF NOT b_has_text_label_triggered[LEM1_BACK]
			IF (DOES_BLIP_EXIST(s_locates_data.LocationBlip)
			OR i_current_event > 0) //Fix for 1410802 - in case you reach the location before the last line of previous covnersation has ended.
			AND NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_INJURED(s_stretch.ped)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_end_pos) < 1225.0
					//Kill the main conversation early.
					IF IS_SCRIPTED_CONVERSATION_ONGOING()
						TEXT_LABEL str_conv_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						
						IF ARE_STRINGS_EQUAL(str_conv_root, "LEM1_CHAT2")
						OR ARE_STRINGS_EQUAL(str_conv_root, "LEM1_CHAT1")
						OR ARE_STRINGS_EQUAL(str_conv_root, "LEM1_HOME")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Handle the dynamic music
		SWITCH i_lose_cops_music_event
			CASE 0
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(s_lamar.ped)
				AND NOT IS_PED_INJURED(s_stretch.ped)
				AND IS_PED_IN_ANY_VEHICLE(s_lamar.ped)
				AND IS_PED_IN_ANY_VEHICLE(s_stretch.ped)
					TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_ENTER_CAR")
					i_lose_cops_music_event++
				ELSE
					//Player already lost wanted level on foot, just progress.
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						i_lose_cops_music_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				BOOL b_progress_event
			
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						PREPARE_MUSIC_EVENT("LM1_COPS_LOST_RADIO")
					
						IF b_has_text_label_triggered[LEM1_COPS4]
							SET_AUDIO_FLAG("SpeechDucksScore", TRUE)
							TRIGGER_MUSIC_EVENT("LM1_COPS_LOST_RADIO")
							b_progress_event = TRUE
						ENDIF
					ELSE
						TRIGGER_MUSIC_EVENT("LM1_TERMINADOR_LOST_ON_FOOT")
						b_progress_event = TRUE
					ENDIF
				ENDIF
				
				IF b_progress_event
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					PLAY_POLICE_REPORT("LAMAR_1_POLICE_LOST")
					
					i_lose_cops_music_event++
				ENDIF
			BREAK
		ENDSWITCH
		
		//Audio scene for stealing cars.
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_AUDIO_SCENE_ACTIVE("LAMAR_1_STEAL_CAR")
				STOP_AUDIO_SCENE("LAMAR_1_STEAL_CAR")
			ENDIF
		ENDIF
		
		//Set the player's tyres to not burst for a small period of time.
		IF NOT b_players_tyres_set_to_not_burst
			IF i_tyre_burst_timer = 0
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					IF NOT IS_ENTITY_DEAD(veh)
						SET_VEHICLE_TYRES_CAN_BURST(veh, FALSE)
						i_tyre_burst_timer = GET_GAME_TIMER()
						b_players_tyres_set_to_not_burst = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_GAME_TIMER() - i_tyre_burst_timer > 30000
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					IF NOT IS_ENTITY_DEAD(veh)
						SET_VEHICLE_TYRES_CAN_BURST(veh, TRUE)
						b_players_tyres_set_to_not_burst = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Taxi drop-off point fix: road outside Franklin's house is blocked so we need a custom point.
		IF DOES_BLIP_EXIST(s_locates_data.LocationBlip)
			IF NOT b_taxi_drop_off_updated
				SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(s_locates_data.LocationBlip, <<-13.3369, -1457.3818, 29.4549>>, 274.2880)
				b_taxi_drop_off_updated = TRUE
			ENDIF
		ELSE
			b_taxi_drop_off_updated = FALSE
			
			DO_BUDDY_FAILS()
		ENDIF
		
		//Request mocap cutscene in advance.
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_end_pos) < (DEFAULT_CUTSCENE_LOAD_DIST * DEFAULT_CUTSCENE_LOAD_DIST)
			REQUEST_CUTSCENE("LAM_1_MCS_2")
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS("LAM_1_MCS_2")
		ENDIF
		
		//Vehicle assets were requested for the car sitting outside, so make sure they're removed once no longer needed.
		IF b_carjack_vehicle_asset_requested
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND (NOT IS_PED_INJURED(s_lamar.ped) AND IS_PED_SITTING_IN_ANY_VEHICLE(s_lamar.ped))
			AND (NOT IS_PED_INJURED(s_lamar.ped) AND IS_PED_SITTING_IN_ANY_VEHICLE(s_stretch.ped))
				REMOVE_VEHICLE_ASSET(model_escape_car)
				b_carjack_vehicle_asset_requested = FALSE
			ENDIF
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP	
		REMOVE_ALL_BLIPS()
		CLEAR_MISSION_LOCATE_STUFF(s_locates_data)

		REMOVE_ENEMY_GROUP(s_cops_setpiece_1)
		REMOVE_ENEMY_GROUP(s_cops_setpiece_1b)
		REMOVE_ENEMY_GROUP(s_cops_setpiece_1c)
		REMOVE_PED(ped_carjack_victim)
		REMOVE_PED(ped_hooker)
		REMOVE_PED(ped_hooker_driver)
		
		REMOVE_VEHICLE(veh_cops_setpiece_1)
		REMOVE_VEHICLE(veh_cops_setpiece_1b)
		REMOVE_VEHICLE(veh_cops_setpiece_1c)
		REMOVE_VEHICLE(veh_carjack)
		REMOVE_VEHICLE(veh_hooker_car)
		
		REMOVE_ANIM_DICT(str_hooker_anims)

		DISTANT_COP_CAR_SIRENS(FALSE)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		//Taxi
		DISABLE_TAXI_HAILING(FALSE)
		
		SETTIMERA(0)
		e_section_stage = SECTION_STAGE_SETUP
		e_mission_stage = STAGE_DROPOFF_CUTSCENE
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		SET_ENTITY_COORDS(PLAYER_PED_ID(), v_end_pos)
		
		IF NOT IS_PED_INJURED(s_lamar.ped)
			SET_ENTITY_COORDS(s_lamar.ped, v_end_pos + <<0.0, 1.0, 0.0>>)
		ENDIF
		
		IF NOT IS_PED_INJURED(s_stretch.ped)
			SET_ENTITY_COORDS(s_stretch.ped, v_end_pos + <<1.0, 0.0, 0.0>>)
		ENDIF
	
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
		e_section_stage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC


PROC DROPOFF_CUTSCENE()
	IF e_section_stage = SECTION_STAGE_SETUP
		IF b_is_jumping_directly_to_stage
			IF i_current_event != 99
				IF b_used_a_checkpoint
					START_REPLAY_SETUP(v_end_pos, 0.0, FALSE)
				ENDIF
				
				i_current_event = 99
			ELSE
				IF SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_LAMAR, v_end_pos + <<0.0, 2.0, 0.0>>)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_STRETCH, v_end_pos + <<0.0, 3.0, 0.0>>)
				AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_OPEN_GUN_SHOP, <<0.0, 0.0, 0.0>>)
					END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
				
					SETTIMERB(0)
				
					//Wait for clothes to stream in
					WHILE TIMERB() < 10000
						IF (NOT IS_PED_INJURED(s_lamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_lamar.ped))
						AND (NOT IS_PED_INJURED(s_stretch.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(s_stretch.ped))
						AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
							SETTIMERB(100000)
						ENDIF
						
						WAIT(0)
					ENDWHILE
				
					SET_ENTITY_COORDS(PLAYER_PED_ID(), v_end_pos)

					#IF IS_DEBUG_BUILD
						SET_CLOCK_TIME(2, 0, 0)
					#ENDIF

					b_is_jumping_directly_to_stage = FALSE
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE)
			ENDIF
		
			IF NOT IS_PED_INJURED(s_lamar.ped)
			AND NOT IS_PED_INJURED(s_stretch.ped)
				REQUEST_CUTSCENE("LAM_1_MCS_2")
				REQUEST_MODEL(JACKAL)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATIONS("LAM_1_MCS_2")
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), s_lamar.ped, -1, SLF_WHILE_NOT_IN_FOV)
				
				IF IS_PED_SITTING_IN_ANY_VEHICLE(s_lamar.ped)
					TASK_LEAVE_ANY_VEHICLE(s_lamar.ped, 800)
					TASK_LOOK_AT_ENTITY(s_lamar.ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				ENDIF
				
				IF IS_PED_SITTING_IN_ANY_VEHICLE(s_stretch.ped)
					TASK_LEAVE_ANY_VEHICLE(s_stretch.ped, 1100)
					TASK_LOOK_AT_ENTITY(s_stretch.ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				ENDIF
				
				IF TIMERA() > 1550
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_LOADED()
				AND HAS_MODEL_LOADED(JACKAL)
				AND TIMERA() > 1500
				AND (NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data) OR TIMERA() > 5000)
					CLEAR_PRINTS()
					CLEAR_HELP()
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					
					CANCEL_BUDDY_PERMANENT_HEAD_TRACK(s_lamar.ped)
					CANCEL_BUDDY_PERMANENT_HEAD_TRACK(s_stretch.ped)
					
					REGISTER_ENTITY_FOR_CUTSCENE(s_lamar.ped, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(s_stretch.ped, "Stretch", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					REMOVE_ALL_AUDIO_SCENES()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						g_eLam1PrestreamLamStretch = ARM1_PD_1_mission_requested_prestream
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

					i_current_event = 0
					b_skipped_mocap = FALSE
					e_section_stage = SECTION_STAGE_RUNNING
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_RUNNING
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_SKIP
		ENDIF
	
		IF NOT IS_CUTSCENE_ACTIVE()
			e_section_stage = SECTION_STAGE_CLEANUP
		ENDIF
		
		IF IS_CUTSCENE_PLAYING()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
		
		/*IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF*/
		
		SWITCH i_current_event
			CASE 0
				IF IS_CUTSCENE_PLAYING()
					//Move the last vehicle somewhere if it's in the way.
					veh_final_cutscene_car = GET_PLAYERS_LAST_VEHICLE()
					
					IF NOT IS_ENTITY_DEAD(veh_final_cutscene_car)
						SET_ENTITY_AS_MISSION_ENTITY(veh_final_cutscene_car, TRUE, TRUE)
					
						IF IS_ENTITY_AT_COORD(veh_final_cutscene_car, v_end_pos, <<20.0, 20.0, 10.0>>)
						AND GET_ENTITY_MODEL(veh_final_cutscene_car) != GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
							PED_INDEX ped_driver 
							IF NOT IS_VEHICLE_SEAT_FREE(veh_final_cutscene_car)
								ped_driver = GET_PED_IN_VEHICLE_SEAT(veh_final_cutscene_car)
							ENDIF
							
							//Don't warp if we arrived as a passenger (e.g. in a taxi ride)
							IF ped_driver = NULL OR ped_driver = PLAYER_PED_ID()
								SET_ENTITY_AS_MISSION_ENTITY(veh_final_cutscene_car)
								SET_ENTITY_COORDS(veh_final_cutscene_car, v_end_pos)
								SET_ENTITY_HEADING(veh_final_cutscene_car, 359.9619)
								SET_VEHICLE_ON_GROUND_PROPERLY(veh_final_cutscene_car)
								SET_VEHICLE_DOORS_SHUT(veh_final_cutscene_car)
								SET_VEHICLE_ENGINE_ON(veh_final_cutscene_car, FALSE, FALSE)
								SET_VEHICLE_LIGHTS(veh_final_cutscene_car, FORCE_VEHICLE_LIGHTS_OFF)
							ENDIF
						ENDIF
					ENDIF
					
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1)
					
					CLEAR_AREA(<<-17.8292, -1458.0117, 29.4598>>, 200.0, TRUE, TRUE)
					
					//Create the car for Stretch and Lamar to drive off in.
					veh_lamar_stretch_exit = CREATE_VEHICLE(JACKAL, <<-38.2037, -1459.5112, 30.3994>>, 93.9805)
					SET_VEHICLE_ON_GROUND_PROPERLY(veh_lamar_stretch_exit)
					SET_MODEL_AS_NO_LONGER_NEEDED(JACKAL)
					
					DO_FADE_IN_WITH_WAIT()
					
					i_current_event++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
			
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		ENDIF
	ENDIF

	IF e_section_stage = SECTION_STAGE_CLEANUP
		IF NOT IS_PED_INJURED(s_lamar.ped)
			TASK_CLEAR_LOOK_AT(s_lamar.ped)
			
			IF IS_PED_GROUP_MEMBER(s_lamar.ped, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(s_lamar.ped)
			ENDIF
		
			IF IS_VEHICLE_DRIVEABLE(veh_lamar_stretch_exit)
				SET_PED_INTO_VEHICLE(s_lamar.ped, veh_lamar_stretch_exit, VS_DRIVER)
			ENDIF
			//SET_ENTITY_COORDS(s_lamar.ped, <<-37.4415, -1461.1023, 30.4540>>)
			//SET_ENTITY_HEADING(s_lamar.ped, 32.5122)
			SET_PED_COMPONENT_VARIATION(s_lamar.ped, PED_COMP_BERD, 1, 0)
		ENDIF
		
		IF NOT IS_PED_INJURED(s_stretch.ped)
			TASK_CLEAR_LOOK_AT(s_stretch.ped)
		
			IF IS_PED_GROUP_MEMBER(s_stretch.ped, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(s_stretch.ped)
			ENDIF
		
			IF IS_VEHICLE_DRIVEABLE(veh_lamar_stretch_exit)
				SET_PED_INTO_VEHICLE(s_stretch.ped, veh_lamar_stretch_exit, VS_FRONT_RIGHT)
			ENDIF
		
			//SET_ENTITY_COORDS(s_stretch.ped, <<-36.5986, -1456.8732, 30.3162>>)
			//SET_ENTITY_HEADING(s_stretch.ped, 105.0295)
		ENDIF
	
		//REMOVE_PED(s_lamar.ped, TRUE)
		//REMOVE_PED(s_stretch.ped, TRUE)
		RELEASE_PED_TO_FAMILY_SCENE(s_lamar.ped)
		RELEASE_PED_TO_FAMILY_SCENE(s_stretch.ped)
		//REMOVE_VEHICLE(veh_final_cutscene_car, FALSE)
	
		IF b_skipped_mocap		
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-24.5866, -1452.8698, 29.7865>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 2.6628)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			WAIT(250)
			
			DO_FADE_IN_WITH_WAIT()
		ENDIF
	
		REPLAY_STOP_EVENT()
	
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
		MISSION_PASSED()
	ENDIF
		
	IF e_section_stage = SECTION_STAGE_SKIP
		STOP_CUTSCENE()
		
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
		
		b_skipped_mocap = TRUE
		e_section_stage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC UPDATE_WIDGETS()
		IF b_debug_enable_forces
			INT i = 0
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		
			IF b_debug_reset_values
				REPEAT COUNT_OF(s_button_forces) i
					s_button_forces[i].i_time_started = 0
				ENDREPEAT
			
				b_debug_reset_values = FALSE
			ENDIF
		
			IF IS_VEHICLE_DRIVEABLE(veh)
				APPLY_FORCE_TYPE force_type
				CONTROL_ACTION button
				
				REPEAT COUNT_OF(s_button_forces) i
					IF i = 0
						button = INPUT_FRONTEND_UP
					ELIF i = 1
						button = INPUT_FRONTEND_DOWN
					ELIF i = 2
						button = INPUT_FRONTEND_LEFT
					ELSE
						button = INPUT_FRONTEND_RIGHT
					ENDIF
				
					IF s_button_forces[i].i_force_type = 0
						force_type = APPLY_TYPE_EXTERNAL_FORCE
					ELIF s_button_forces[i].i_force_type = 1
						force_type = APPLY_TYPE_EXTERNAL_IMPULSE
					ELSE
						force_type = APPLY_TYPE_TORQUE
					ENDIF
					
					IF s_button_forces[i].i_time_started = 0
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, button)
							s_button_forces[i].i_time_started = GET_GAME_TIMER()

							APPLY_FORCE_TO_ENTITY(veh, force_type, s_button_forces[i].v_force, s_button_forces[i].v_offset, 0, 
												  s_button_forces[i].b_local_force, s_button_forces[i].b_local_offset, TRUE)
						ENDIF
					ELSE
						IF GET_GAME_TIMER() - s_button_forces[i].i_time_started < s_button_forces[i].i_duration
							APPLY_FORCE_TO_ENTITY(veh, force_type, s_button_forces[i].v_force, s_button_forces[i].v_offset, 0, 
												  s_button_forces[i].b_local_force, s_button_forces[i].b_local_offset, TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		
		IF b_debug_play_god_text
			PRINT_NOW("LEM1_GOGUN", DEFAULT_GOD_TEXT_TIME, 0)
		
			b_debug_play_god_text = FALSE
		ENDIF
		
		IF b_debug_play_convo_subs
			IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GUN", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
				b_debug_play_convo_subs = FALSE
			ENDIF
		ENDIF
		
		IF b_debug_play_convo_no_subs
			IF CREATE_CONVERSATION(s_conversation_peds, str_text_block, "LEM1_GUN", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				b_debug_play_convo_no_subs = FALSE
			ENDIF
		ENDIF
		
		IF b_debug_clear_all_text
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			b_debug_clear_all_text = FALSE
		ENDIF
		
		IF b_debug_break_glass
			OBJECT_INDEX obj_window_1 = GET_CLOSEST_OBJECT_OF_TYPE(v_debug_glass_break_pos, 5.0, PROP_RCYL_WIN_01, FALSE)
			
			IF DOES_ENTITY_EXIST(obj_window_1)
				BREAK_ENTITY_GLASS(obj_window_1, v_debug_glass_break_pos, f_debug_glass_break_radius, v_debug_glass_break_force, f_debug_glass_break_damage, i_debug_break_glass_type)
			ENDIF
			
			b_debug_break_glass = FALSE
		ENDIF
		
		IF b_debug_clear_glass
			CLEAR_AREA(v_debug_glass_break_pos, 20.0, FALSE)
		
			b_debug_clear_glass = FALSE
		ENDIF
		
		IF b_debug_show_glass_debug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			DRAW_DEBUG_SPHERE(v_debug_glass_break_pos, f_debug_glass_break_radius, 0, 0, 255, 64)
		ENDIF
		
		IF b_debug_output_glass_info
			SAVE_NEWLINE_TO_DEBUG_FILE()
	                
	        SAVE_STRING_TO_DEBUG_FILE("BREAK_ENTITY_GLASS(obj, ")
	        SAVE_VECTOR_TO_DEBUG_FILE(v_debug_glass_break_pos)
	        SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(f_debug_glass_break_radius)
	        SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(v_debug_glass_break_force)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_FLOAT_TO_DEBUG_FILE(f_debug_glass_break_damage)
			SAVE_STRING_TO_DEBUG_FILE(", 0)")
	        
			b_debug_output_glass_info = FALSE
		ENDIF
		
		IF b_debug_trigger_door_ptfx
			START_PARTICLE_FX_NON_LOOPED_AT_COORD(str_door_breach_ptfx, <<-590.1724, -1621.4822, 33.1749>>, <<0.0, 0.0, -5.84>>, 1.0)
			b_debug_trigger_door_ptfx = FALSE
		ENDIF
		
		IF b_debug_trigger_smoke_ptfx
			REQUEST_PTFX_ASSET()
		
			IF HAS_PTFX_ASSET_LOADED()
				IF ptfx_debug != NULL
					STOP_PARTICLE_FX_LOOPED(ptfx_debug)
					ptfx_debug = NULL
				ENDIF
				
				ptfx_debug = START_PARTICLE_FX_LOOPED_AT_COORD("scr_env_agency3b_smoke", v_debug_smoke_ptfx_pos, v_debug_smoke_ptfx_rot)
				
				b_debug_trigger_smoke_ptfx = FALSE
			ENDIF
		ENDIF
		
		IF ptfx_debug != NULL
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_debug, "smoke", f_debug_smoke_ptfx_smoke_evo)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_debug, "cinder", f_debug_smoke_ptfx_cinder_evo)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_debug, "debris", f_debug_smoke_ptfx_debris_evo)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_debug, "speed", f_debug_smoke_ptfx_speed_evo)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_debug, "smoke_strength", f_debug_smoke_ptfx_smoke_evo)
		ENDIF
	ENDPROC

	PROC DO_DEBUG()
		UPDATE_WIDGETS()
		
		IF NOT b_is_jumping_directly_to_stage		
			IF e_section_stage = SECTION_STAGE_RUNNING
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					MISSION_PASSED(TRUE)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
					MISSION_FAILED(FAILED_GENERIC)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					e_section_stage = SECTION_STAGE_SKIP
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
					//Work out which stage we want to reach based on the current stage					
					INT i_current_stage = ENUM_TO_INT(e_mission_stage)
					
					IF i_current_stage > 0
						MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, i_current_stage - 1)
						
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
					OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(NULL)
				ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
					b_debug_display_mission_debug = NOT b_debug_display_mission_debug
				ENDIF
				
				IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, i_debug_jump_stage)
					JUMP_TO_STAGE(e_stage)
				ENDIF
				
				i_debug_num_prints_this_frame = 0
			ENDIF
		ENDIF
		
		//If someone presses the button to add a bug then dump a bunch of mission info to the console.
		IF IS_KEYBOARD_KEY_PRESSED(KEY_SPACE)
		OR IS_KEYBOARD_KEY_PRESSED(KEY_RBRACKET)	
			DUMP_MISSION_STATE_TO_CONSOLE()
		ENDIF
	ENDPROC
	
	
	
#ENDIF

SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		
		b_is_jumping_directly_to_stage = FALSE //If the mission is in the middle of a debug skip cleanup (e.g. waiting for a cutscene to finish) Force cleanup could break.
		g_eLam1PrestreamLamStretch = ARM1_PD_3_release
		//STORE_FAIL_WEAPON(PLAYER_PED_ID(), ENUM_TO_INT(CHAR_FRANKLIN))
		MISSION_CLEANUP()
		
		// B*1543767 - Ensure Chop is present at Franklin's aunt's house
		REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")
		TERMINATE_THIS_THREAD() //Just in case.
	ENDIF

	MISSION_SETUP()
	
	//Mission replay
	IF Is_Replay_In_Progress()
		INT i_stage = Get_Replay_Mid_Mission_Stage()
        
		IF g_bShitskipAccepted
			IF i_stage = CHECKPOINT_GO_INSIDE_GUN_SHOP
				b_shit_skipped_weapons_shop = TRUE
			ENDIF
		
			i_stage++
		ENDIF
		
		IF i_stage = 0
			JUMP_TO_STAGE(STAGE_GO_TO_GUN_SHOP)
		ELIF i_stage = CHECKPOINT_GO_INSIDE_GUN_SHOP
			JUMP_TO_STAGE(STAGE_BUY_GRENADES)
		ELIF i_stage = CHECKPOINT_GO_TO_PLANT
			JUMP_TO_STAGE(STAGE_GO_TO_PLANT)
		ELIF i_stage = CHECKPOINT_INTERIOR_START
			JUMP_TO_STAGE(STAGE_ESCAPE_PRE_WAREHOUSE)
		ELIF i_stage = CHECKPOINT_INTERIOR_MIDDLE
			JUMP_TO_STAGE(STAGE_ESCAPE_FROM_WAREHOUSE)
		ELIF i_stage = CHECKPOINT_LOSE_COPS
			JUMP_TO_STAGE(STAGE_GO_BACK_HOME)
		ELIF i_stage > CHECKPOINT_LOSE_COPS
			JUMP_TO_STAGE(STAGE_DROPOFF_CUTSCENE)
		ENDIF
		
		//Need to make sure outfit carries over, it may be reset by the snapshot system due to it getting set during a cutscene.
		IF e_mission_stage <= STAGE_GO_TO_GUN_SHOP
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_TRACKSUIT_JEANS, FALSE) 
		ENDIF
		
		b_used_a_checkpoint = TRUE
	ELSE
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "GO_TO_GUN_SHOP")
	ENDIF
	
	WHILE (TRUE)
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheLongStretch")
		
		IF iReplayCameraSpawnTimer > GET_GAME_TIMER()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222067
		ENDIF
		
		//Reset the checkpoint flag once the mission is running again. This flag is used to determine if we jumped using a checkpoint or via Z-skip,
		//as some bits of setup are only done when debug skipping.
		IF b_used_a_checkpoint
			IF e_section_stage = SECTION_STAGE_RUNNING
				b_used_a_checkpoint = FALSE
			ENDIF
		ENDIF
		
		//Prevent shop dialogue before a certain point in the mission
		IF NOT b_blocked_shop_dialogue
			IF e_mission_stage < STAGE_GO_TO_PLANT
				SET_SHOP_DIALOGUE_IS_BLOCKED(GUN_SHOP_01_DT, TRUE)
				b_blocked_shop_dialogue = TRUE
			ENDIF
		ENDIF
		
		//Gun shops are unlocked during this mission: keep track of what state they need to be in based on stage to avoid Z-skip/checkpoint issues.
		IF e_mission_stage < STAGE_GO_TO_PLANT
			IF IS_SHOP_AVAILABLE(GUN_SHOP_02_SS)
				SET_ALL_NON_MISSION_GUN_SHOPS_OPEN(FALSE)
			ENDIF
		ELIF e_mission_stage > STAGE_GO_TO_PLANT
			IF NOT IS_SHOP_AVAILABLE(GUN_SHOP_02_SS)
				SET_ALL_NON_MISSION_GUN_SHOPS_OPEN(TRUE)
			ENDIF
		ELIF e_section_stage = SECTION_STAGE_RUNNING
			IF NOT IS_SHOP_AVAILABLE(GUN_SHOP_02_SS)
				SET_ALL_NON_MISSION_GUN_SHOPS_OPEN(TRUE)
			ENDIF
		ENDIF
		
		//Generic fails
		IF e_section_stage = SECTION_STAGE_RUNNING AND NOT b_mission_failed
			IF e_mission_stage != STAGE_OPENING_CUTSCENE
				IF DOES_ENTITY_EXIST(s_lamar.ped)
					IF IS_PED_INJURED(s_lamar.ped)
						IF IS_PED_INJURED(s_stretch.ped)
							MISSION_FAILED(FAILED_LAMAR_AND_STRETCH_DIED)
						ELSE
							MISSION_FAILED(FAILED_LAMAR_DIED)
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(s_stretch.ped)
					IF IS_PED_INJURED(s_stretch.ped)
						IF IS_PED_INJURED(s_lamar.ped)
							MISSION_FAILED(FAILED_LAMAR_AND_STRETCH_DIED)
						ELSE
							MISSION_FAILED(FAILED_STRETCH_DIED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Fail if the player causes the shop to close before they've bought grenades.
			IF e_mission_stage = STAGE_GO_TO_GUN_SHOP 
			OR e_mission_stage = STAGE_BUY_GRENADES
				IF IS_PLAYER_KICKING_OFF_IN_SHOP(GUN_SHOP_01_DT)
					MISSION_FAILED(FAILED_SHOP_CLOSED)
				ENDIF
			ENDIF
		ENDIF
		
		//Stats
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//Track the vehicle speed/damage stat.
				VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF NOT IS_ENTITY_DEAD(veh)	
					FLOAT f_speed = GET_ENTITY_SPEED(veh)
				
					IF f_speed > f_current_fastest_vehicle_speed
						f_current_fastest_vehicle_speed = f_speed
						
						INFORM_MISSION_STATS_OF_INCREMENT(LAM1_MAX_SPEED, ROUND(f_current_fastest_vehicle_speed), TRUE)
					ENDIF
				
					IF NOT DOES_ENTITY_EXIST(g_MissionStatSingleDamageWatchEntity)
					OR NOT IS_ENTITY_A_VEHICLE(g_MissionStatSingleDamageWatchEntity)
					OR (IS_ENTITY_A_VEHICLE(g_MissionStatSingleDamageWatchEntity) AND GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleDamageWatchEntity) != veh)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(veh, LAM1_CAR_DAMAGE)
					ENDIF
				ENDIF
			ELSE
				//Track the player damage stat.
				IF NOT DOES_ENTITY_EXIST(g_MissionStatSingleDamageWatchEntity)
				OR NOT IS_ENTITY_A_PED(g_MissionStatSingleDamageWatchEntity)
				OR (IS_ENTITY_A_PED(g_MissionStatSingleDamageWatchEntity) AND GET_PED_INDEX_FROM_ENTITY_INDEX(g_MissionStatSingleDamageWatchEntity) != PLAYER_PED_ID())
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), LAM1_UNMARKED)
				ENDIF
			ENDIF
		ENDIF
		
		//Track the buying body armour stat (only count it before the shootout).
		IF e_mission_stage = STAGE_BUY_GRENADES
		OR e_mission_stage = STAGE_GO_TO_PLANT
			INT i_armour = GET_PED_ARMOUR(PLAYER_PED_ID())
			
			IF i_armour > i_armour_on_mission_start
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(LAM1_BODY_ARMOR)
				i_armour_on_mission_start = i_armour
			ENDIF
		ENDIF		

		SWITCH e_mission_stage
			CASE STAGE_OPENING_CUTSCENE
				OPENING_CUTSCENE()
				//CAMERA_TEST()
			BREAK

			CASE STAGE_GO_TO_GUN_SHOP
				GO_TO_GUN_SHOP()
			BREAK
			
			CASE STAGE_SHOP_INTRO_CUTSCENE
				SHOP_INTRO_CUTSCENE()
			BREAK

			CASE STAGE_BUY_GRENADES
				BUY_GRENADES()
			BREAK

			CASE STAGE_GO_TO_PLANT
				GO_TO_PLANT()
			BREAK

			CASE STAGE_MEETING_CUTSCENE
				MEETING_CUTSCENE()
			BREAK

			CASE STAGE_ESCAPE_PRE_WAREHOUSE
			CASE STAGE_ESCAPE_FROM_WAREHOUSE
				//Both stages use the same code, the only major difference being the setup.
				ESCAPE_INTERIOR()
			BREAK
			
			CASE STAGE_GO_BACK_HOME
				GO_BACK_HOME()
			BREAK
			
			CASE STAGE_DROPOFF_CUTSCENE
				DROPOFF_CUTSCENE()
			BREAK
		ENDSWITCH

		#IF IS_DEBUG_BUILD
			DO_DEBUG()
		#ENDIF	

		//Used for marking spheres to aim choppers at when doing recordings
		//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		//DRAW_DEBUG_SPHERE(<<-539.9962, -1657.4783, 42.5749>>, 2.0)
	ENDWHILE
ENDSCRIPT		
