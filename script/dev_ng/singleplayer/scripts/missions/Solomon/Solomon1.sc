// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Soloman 1
//		AUTHOR			:	Craig Vincent
//		DESCRIPTION		:	Take the heli from the roof
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"
USING "commands_event.sch"
USING "commands_stats.sch"//BUG FIX 1399395

USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
using "script_ped.sch"
using "asset_management_public.sch"
using "RC_helper_functions.sch" 
using "cheat_controller_public.sch"
using "building_control_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	mpf_mike,
	
	mpf_Anton,
	mpf_milton,
	
	mpf_rocco,
	mpf_pilot,
	mpf_Gianni,
	
	mpf_sol,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_heli,
	mvf_Replay_car,
	
	mvf_coquette,
	mvf_CAVALCADE2,
	mvf_car1,
	
	mvf_mixer,
	mvf_forklift,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM eEnemies_Mafia	
		
	maf_car,
	maf_car_talking,
		
	maf_walkway_door,
	maf_worker_walkway,
	maf_worker_vent,	
	maf_worker_top,
	maf_worker_door,	
	maf_worker_alone,
	
	NUM_OF_Mafia_PEDS
endenum
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_get_to_warehouse,
	msf_1_Stealth_to_roof,	
	msf_2_fight_rocco,
	msf_3_get_to_sol,
	msf_4_passed,
	
	MSF_NUM_OF_STAGES
ENDENUM
ENUM MFF_MISSION_FAIL_FLAGS
	mff_debug_fail,
	mff_rocco_alerted,
	mff_rocco_died,
	mff_actor_died,
	mff_director_died,
	mff_player_died,
	mff_heli_dead,
	mff_scared_peds,
	mff_sol_died,
	mff_left_Area,
	mff_left_peds,
	mff_default,	
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
enum STEALTH_STAGE	
	AI_NORM,
	AI_CHECKS,
	AI_REACTION,	
	AI_PHONE,
	AI_ALARM
endenum
ENUM STEALTH_SITUATION
	SSIT_AI_NONE,
	SSIT_AI_SPOTTED,
	SSIT_AI_GUN,
	SSIT_AI_HURT,
	SSIT_AI_HEAT
endenum
ENUM DOOR_ENUM
	mdf_Walkway_door,
	mdf_mid_door,
	mdf_Top_door,
	mdf_Helipad_door,
	mdf_FRONT_GATE_L,
	mdf_FRONT_GATE_R,
	mdf_Garage_door,
	
	MDF_NUM_OF_DOORS
endenum
ENUM SCARE_TYPE
	SCARE_NONE,
	SCARE_BRIDGE,
	SCARE_CRASH,
	SCARE_LOW_FAST,
	SCARE_PITCH,
	SCARE_HEADING_CHANGE,
	SCARE_HEIGHT_DROP,
	SCARE_SPEED	
endenum
enum ENUM_CONS_OBJS
//	cobj_floor_bricks,
	cobj_floor_pipes1,
	cobj_floor_wood1,
	cobj_floor_wood2,	
	cobj_floor_pipes2,
	cobj_floor_gen,
	cobj_floor_bitsbox,
	cobj_floor_cementbags,
	cobj_floor_toolbox,
	cobj_floor_torch,
	cobj_floor_hat1,
	cobj_floor_hat2,
	cobj_floor_hat3,
	
	cobj_roof1_toolbox,
	cobj_roof1_wirecoil1,
	cobj_roof1_wirecoil2,
	cobj_roof1_light1,
	cobj_roof1_light2,
	cobj_roof1_gen,
	cobj_roof1_light3,
	cobj_roof1_hat,
	
	cobj_ally_light,
	cobj_ally_lightbox,
	cobj_ally_gen,
	
	cobj_floor2_pipes,
	cobj_floor2_barrier1,
	cobj_floor2_barrier2,
	
	cobj_roof2_light1,
	cobj_roof2_bricks2,
	cobj_roof2_gen,
	cobj_roof2_light2,
	cobj_roof2_light3,
	
	cobj_roof3_light1,
	cobj_roof3_lightbox,
	cobj_roof3_hardhat1,
	cobj_roof3_cementMixer,
	cobj_roof3_cementStack,
	cobj_roof3_gen,
	cobj_roof3_light2,
	cobj_roof3_light3,
	
	cobj_roof4_light1,
//	cobj_roof4_gen,
	cobj_roof4_bitsbox,
	cobj_roof4_toolbox,
	
	MAX_NUM_CONST_OBJS
endenum
enum CONSTRUCT_DUP_TYPE
	dups_none,
	dups_PIPES,
	dups_hat,
	dups_gen,
	dups_partsbox,
	dups_toolbox01,
	dups_light3b,
	dups_light2a,
	dups_gen1a,
	dups_light4a,
	dups_barrier,
	dups_gen2a,
	dups_light4c,
	dups_light4d
	
endenum
//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
	BLIP_INDEX			blip
	INT					iExitingThisRoom = -1
ENDSTRUCT
struct ENEMY_STRUCT
	ped_index				id
	OBJECT_INDEX			obj	
	OBJECT_INDEX			objphone	
	AI_BLIP_STRUCT			blipstruct
	bool					bkilled	
	vector 					vStartLoc
	vector					vtarget
	float					fheading		
	float					fBlipRange		
	float					fHearingRange 	= 12.0	
	ped_index				partner			= null
	bool					bObjAttached	= false	
	bool					bOnphone		= false
	bool					bForceBlip		= false
	STEALTH_SITUATION		sit 			= SSIT_AI_NONE	
	MODEL_NAMES				model 			= G_M_M_ARMGOON_01	
	INT						iExitingThisRoom = -1		
	int						sDebug 
	
	int						iVoiceID
	string					sVoiceName
	bool					bHeardNoise 	= false
	
endstruct
struct CONSTRUCTION_STRUCT	
	object_index		id
	model_names			model
	bool				bloaded		= false
	bool				bNeeded		= true
	float				fcreateDist = 40.0
	vector				vpos
	vector				vRot
	CONSTRUCT_DUP_TYPE	dupType		= dups_none
endstruct
// Struct used to hold info about doors
	STRUCT DOOR_STRUCT
		INT				iHash
		BOOL			bChangingState
		FLOAT			fDesiredOpenRatio
		FLOAT			fStartOpenRatio
		BOOL			bDesiredLockState
		INT				iInterpTime
		INT				iTimeStamp
		BOOL			bExistingDoor
	ENDSTRUCT
//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY		0
CONST_INT				STAGE_EXIT		-1 

CONST_FLOAT 			DOORS_KEEP_CURRENT_RATIO 	100.0

CONST_FLOAT				HIGH_SPEED_TRIGGER			50.0
CONST_FLOAT				LOW_HEIGHT_MAX				9.0
CONST_FLOAT				LOW_HEIGHT_SPEED			20.0//23.0 BUG FIX 1399435 reduced speed check
CONST_FLOAT				DROP_SPEED				   -15.0
const_float				PITCH_CHANGE				0.9
const_float				HEADING_CHANGE				0.9

Vector				vMafiaCar		= <<956.9126, -1708.0422, 28.9836>>
float				hMafiaCar		= 355.3881

Vector				vheliRoof		= <<909.9578, -1680.8170, 50.8435>> 
float				hHeliRoof		= 200.8677

vector 				vBOXPOS			= <<902.91626, -1691.21155, 46.13420>>
vector 				vBIGVENT 		= <<884.57086, -1688.17114, 46.13421>>
vector 				vCROUCHVENT 	= <<890.91174, -1684.75647, 46.13421>>

vector 				vMIDVENTSMALL	= <<910.14313, -1708.57007, 41.97026>>
vector 				vMIDVENTBIG		= <<901.90808, -1709.95081, 41.97071>>

vector				vGianni			= <<916.49890, -1699.61768, 50.13412>>
float				hGianni			= 1.2

//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------
//BUG FIX 1399354 and 1399344 adding a vector for the players position so we can check for his hieght to stop dialogue playing if player is on ground below.
VECTOR vPlayerCoords
VECTOR vMafiaPedCoords

INT iBridgeCountAtStart, iBridgeCountDuringMission, iLastIncrementTimer //BUG FIX 1399435
INT iHeliHealthStart, iHeliHealthNow
INT iHelpTextTimer

BOOL bDoneFightHelp1, bDoneFightHelp2

BOOL bVehStatsGrabbed 	//PD - adding this to monitor damage and speed watch on latest player's car
BOOL bHeliStatsGrabbed 	//PD - added this to check if the Frogger has been added to stats watch
				
//

LOCATES_HEADER_DATA 		sLocatesData
VEHICLE_STRUCT				Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT					peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level
ENEMY_STRUCT				Mafia[NUM_OF_Mafia_PEDS]
DOOR_STRUCT					sDoors[MDF_NUM_OF_DOORS]			// door array

CONSTRUCTION_STRUCT			constObjs[MAX_NUM_CONST_OBJS]
int 						iconstructStage = 0

structPedsForConversation 		convo_struct			//holds peds in the conversation
BLIP_INDEX						blip_objective			//blip for mission objective
Camera_index					cameraIndex				//in game cut scene camera
CAMERA_INDEX					camFollow
SEQUENCE_INDEX					seq						//used to create AI sequence
int 							i
int 							idelay
int 							iskipDelay

MODEL_NAMES						modelrocco 		= IG_ROCCOPELOSI
model_names						modelpilot		= S_M_Y_PILOT_01
MODEL_NAMES						modelActor 		= IG_milton		
MODEL_NAMES						modelDirector 	= U_M_Y_ANTONB 
MODEL_NAMES						modelWorker 	= S_M_Y_CONSTRUCT_01
MODEL_NAMES						modelclipboard	= P_AMB_CLIPBOARD_01
Model_names						modelPhone		= p_amb_phone_01
MODEL_NAMES						modelGianni		= U_M_Y_GUIDO_01

//------------------------------ RELATIONSHIPS --------------------------------------------------
REL_GROUP_HASH 			rel_group_enemies
REL_GROUP_HASH			rel_friends

//-------------- heli -----------------
bool 			bsearchlights 	= false

int 			iScareStage		= 0
int				iScareInt 		= 0
int 			iScareTimer2
bool			bPlayChat	 	= false

bool			bScareHeliWarn	= false

SHAPETEST_INDEX	shapetestheli = null
INT iShapetestResult
VECTOR vecShapetestResult
VECTOR vecShapetestNormal
ENTITY_INDEX hitEntity

//--------------- AI --------------------

STEALTH_STAGE				eAI_stage

PED_INDEX 					Active_ped
WEAPON_TYPE					wcurrent

int 						iRand 
int 						iflinch = 0
int							iseen 	= 0

int							IphoneTimer
int 						iPhoningChat 
int 						iPhoneCheckBackupTimer
bool						bAloneWorkerActive		= false

string 						AnimDict_gestures 		= "amb@world_human_hang_out_Street@male_a@idle_a"
string						animDict_kneel_enter	= "amb@medic@standing@kneel@enter"
string						animDict_kneel_Idle		= "amb@medic@standing@kneel@idle_a"
string						animDict_kneel_exit		= "amb@medic@standing@kneel@exit"
string						AnimDict_flinch	  		= "REACTION@MALE_STAND@BIG_INTRO@FORWARD"
string						AnimDict_ClipBoard		= "amb@world_human_clipboard@male@idle_a"
string						AnimDict_PHONE			= "cellphone@str"
string						animPhone				= "cellphone_call_listen_c"

int 						iAimDelay
int 						iGunOutDelay
int							ifloorspot = 0
int							ifloortimer
//------------ALARM--------------
bool	bFirstAlerted		= false
//------------- FAIL ------------------
bool	bLeftfight
bool	bfailWarning 		= false
bool	bfailAntonWarning 	= FALSE
bool	bfailMiltonWarning 	= FALSE
//------------AUDIO--------------
int  	i_dialogueTimer
int 	iDialogue_Stage		= 0
int 	iCarSpeechStage		= 0
int 	iwalkwaychat 		= 0 
int		iGianniChat			= 0
bool	bpauseconv 			= false
bool 	bDoWalkwayChat		= false

bool	bMusic_1stEnemy		= false
bool	bMusic_HeliTrigger	= false
bool	bMusic_Ends			= false
bool	bcueTakeOff 		= false

int 	iambdialoguetimer 
bool 	bChooseActor		= true
//---------------CS--------------------
bool 	b_cutscene_loaded 	= false
bool	bcs_cam				= false
bool	bcs_mike 		  	= false
bool	bcs_actor		 	= false
bool	bcs_director		= false
bool	bcs_rocco			= false
bool	bcs_goon			= false
bool	bcs_heli			= false

bool 	bPlaceholder		= false
int		iskipCS				= 0
//------- lead in ---------
bool 	bSolomonCreated	= false
//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
CUTSCENE_PED_VARIATION 	sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]
bool					bStageLoaded 	= false
bool					bStageLoaded2 	= false
bool					bloadCar1		= false
bool					bCreateCar1		= false
bool					bloadMixer		= false
bool					bCreateMixer	= false
bool					bloadForklift	= false
bool					bCreateForklift	= false
bool					bStartConstructionChecks = false
bool 					bstreamloaded 				= false
bool 					bLoadMusicStream 			= false
//----------------

//----------------Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
//----------------//----------------//----------------//----------------

#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID widget_debug
	float debug_noise
	int wAI_stage 
#endif

// ===========================================================================================================
//		Termination
// ===========================================================================================================
func bool isentityalive(entity_index entity)
  	if does_entity_exist(entity)
		if is_entity_a_vehicle(entity)
			if is_vehicle_driveable(get_vehicle_index_from_entity_index(entity))
				return true 
			endif
		elif is_entity_a_ped(entity)
			if not is_ped_injured(get_ped_index_from_entity_index(entity))
	          	return true 
	    	endif	
		elif IS_ENTITY_AN_OBJECT(entity)
			return true
		endif  		
	endif
      return false
endfunc
func ped_index mike()
	return peds[mpf_mike].id
endfunc
func ped_index Milton()
	return peds[mpf_milton].id
endfunc
func ped_index Anton()
	return peds[mpf_Anton].id
endfunc
func ped_index rocco()
	return peds[mpf_rocco].id
endfunc
func ped_index sol()
	return peds[mpf_sol].id
endfunc
func ped_index Gianni()
	return peds[mpf_Gianni].id
endfunc
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC
// -----------------------------------------------------------------------------------------------------------
//		LOADING
// -----------------------------------------------------------------------------------------------------------
PROC LOAD_ASSET_STAGE(MSF_MISSION_STAGE_FLAGS LOADSTAGE)	
	SWITCH LOADSTAGE			
		CASE msf_0_get_to_warehouse
			Load_Asset_Model(sAssetData,CAVALCADE2)
			load_Asset_model(sAssetdata,COQUETTE)			
		BREAK		
		CASE msf_1_Stealth_to_roof
			LOAD_ASSET_MODEL(sAssetData,FROGGER)
			LOAD_ASSET_MODEL(sAssetData,G_M_M_ARMGOON_01)
			LOAD_ASSET_MODEL(sAssetData,modelGianni)
			LOAD_ASSET_MODEL(sAssetData,modelWorker)
			load_asset_model(sAssetData,modelActor)
			load_asset_model(sAssetData,modelDirector)
			Load_Asset_Model(sAssetData,modelclipboard)
			Load_Asset_Model(sAssetData,modelPhone)	
			Load_Asset_AnimDict(sAssetData,animDict_kneel_enter)
			Load_Asset_AnimDict(sAssetData,animDict_kneel_Idle)
			Load_Asset_AnimDict(sAssetData,animDict_kneel_exit)
			Load_Asset_AnimDict(sAssetData,AnimDict_gestures)			
			Load_Asset_AnimDict(sAssetData,AnimDict_flinch)
			Load_Asset_AnimDict(sAssetData,	AnimDict_ClipBoard)
			Load_Asset_AnimDict(sAssetData,	AnimDict_PHONE)
		BREAK		
		case msf_2_fight_rocco
			LOAD_ASSET_MODEL(sAssetData,FROGGER)
			load_asset_model(sAssetData,modelrocco)
			load_asset_model(sAssetData,modelpilot)
			load_asset_model(sAssetData,modelActor)
			load_asset_model(sAssetData,modelDirector)
			Load_Asset_Model(sAssetData,modelclipboard)
			Load_Asset_AnimDict(sAssetData,AnimDict_flinch)			
		break
		case msf_3_get_to_sol			
			LOAD_ASSET_MODEL(sAssetData,FROGGER)
			load_asset_model(sAssetData,modelActor)
			load_asset_model(sAssetData,modelDirector)
			Load_Asset_Model(sAssetData,modelclipboard)
		break
		CASE msf_4_passed		
		break
	ENDSWITCH
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
proc init_enemy_data()
//--------------------------START LOCATION----------------------------------
	//Mafia peds
	
	Mafia[maf_car].vStartLoc 				= <<956.28088, -1704.83215, 28.77425>>//next to car
	Mafia[maf_car].fheading					= 329.4887//-23.67 BUG FIX 1399373
	Mafia[maf_car].fBlipRange				= 24
	Mafia[maf_car].fHearingRange			= 20.5	
	Mafia[maf_car_talking].vStartLoc 		= <<957.24054, -1705.40430, 28.83121>>//next to car talking	
	Mafia[maf_car_talking].fheading			= 12.8746//66.44 BUG FIX 1399373
	Mafia[maf_car_talking].fBlipRange		= 29
	Mafia[maf_car_talking].fHearingRange	= 20.5
	
	Mafia[maf_worker_top].vStartLoc 		= <<904.58276, -1686.67871, 46.35751>>//top door
	Mafia[maf_worker_top].fheading			= 83.33
	Mafia[maf_worker_top].fBlipRange		= 29
	
	Mafia[maf_walkway_door].vStartLoc 		= <<915.91370, -1692.07397, 42.15242>>//walkway door
	Mafia[maf_walkway_door].fheading		= -95.93	
	Mafia[maf_walkway_door].fBlipRange		= 30
	Mafia[maf_worker_walkway].vStartLoc 	= <<914.42017, -1692.38306, 42.15268>> //walkwayroof inside door
	Mafia[maf_worker_walkway].fheading		= -93.72
	Mafia[maf_worker_walkway].fBlipRange	= 30
	
	Mafia[maf_worker_alone].vStartLoc 		= <<924.50854, -1720.03943, 37.06301>>//construction worker on roof	
	Mafia[maf_worker_alone].fheading		= 88.84	
	Mafia[maf_worker_alone].fBlipRange		= 27
	Mafia[maf_worker_alone].fHearingRange	= 7
		
	Mafia[maf_worker_vent].vStartLoc 		= <<888.13782, -1714.93701, 37.06277>>//alone next to vent
	Mafia[maf_worker_vent].fheading			= -87.91
	Mafia[maf_worker_vent].fHearingRange	= 4.7
	Mafia[maf_worker_vent].fBlipRange		= 16
	
	Mafia[maf_worker_door].vStartLoc 		= <<906.9767, -1696.8409, 42.1162>> //mid door	
	Mafia[maf_worker_door].fheading			= 171.9180
	Mafia[maf_worker_door].fBlipRange		= 26
	
	//VOICES     CONSTRUCTION1 = 4   CONSTRUCTION2 = 3   CONSTRUCTION3 = 5
	Mafia[maf_car].iVoiceID					= 3
	Mafia[maf_car].sVoiceName				= "CONSTRUCTION2"		
	Mafia[maf_car_talking].iVoiceID			= 4
	Mafia[maf_car_talking].sVoiceName		= "CONSTRUCTION1"
	
	Mafia[maf_worker_alone].iVoiceID		= 3
	Mafia[maf_worker_alone].sVoiceName		= "CONSTRUCTION2"
	
	Mafia[maf_worker_door].iVoiceID			= 5
	Mafia[maf_worker_door].sVoiceName		= "CONSTRUCTION3"
	
	Mafia[maf_worker_walkway].iVoiceID		= 3
	Mafia[maf_worker_walkway].sVoiceName	= "CONSTRUCTION2"
	Mafia[maf_walkway_door].iVoiceID		= 5
	Mafia[maf_walkway_door].sVoiceName		= "CONSTRUCTION3"
	
	Mafia[maf_worker_vent].iVoiceID			= 4
	Mafia[maf_worker_vent].sVoiceName		= "CONSTRUCTION1"
	
	Mafia[maf_worker_top].iVoiceID			= 3
	Mafia[maf_worker_top].sVoiceName		= "CONSTRUCTION2"
endproc
// ===================================================== DOOR MANAGEMENT =======================================================================
//PURPOSE: Adds a new door to the doors array
PROC Doors_Add_New(DOOR_ENUM slot, VECTOR vCoord, MODEL_NAMES model, BOOL bResetDoor = FALSE)
	TEXT_LABEL_23 strDoorName = "SOL1_DOOR_"
	strDoorName += ENUM_TO_INT(slot)
	sDoors[slot].iHash	= GET_HASH_KEY(strDoorName)
	
	ADD_DOOR_TO_SYSTEM(sDoors[slot].iHash, model, vCoord, FALSE)
	
	IF bResetDoor
		DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[slot].iHash, 1.0)
		DOOR_SYSTEM_SET_DOOR_STATE(sDoors[slot].iHash, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
		DOOR_SYSTEM_SET_DOOR_STATE(sDoors[slot].iHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	ENDIF
ENDPROC

FUNC BOOL Is_Door_LockState_Locked(DOOR_STATE_ENUM eDoorState)
	IF eDoorState = DOORSTATE_LOCKED
	OR eDoorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
	OR eDoorState = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//BUG FIX 1399435
FUNC INT CHECK_BRIDGE_COMPLETION()
	INT completeCount, iTemp
	VECTOR vClosestNode, vHeliCoords
	FLOAT fHeliMaxRange_X, fHeliMinRange_X, fHeliMaxRange_Y, fHeliMinRange_Y
	
	REPEAT 32 iTemp
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, iTemp)
			completeCount++
		ENDIF
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, iTemp)
			completeCount++
		ENDIF
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, iTemp)
			completeCount++
		ENDIF
	ENDREPEAT
	
	//Check to see if there is a vehicle node above the player. This will suggest that the player is flying under a road.
	IF GET_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_heli].id, <<0,0,5>>), vClosestNode)
		vHeliCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		fHeliMaxRange_X = (vHeliCoords.x + 4)
		fHeliMinRange_X = (vHeliCoords.x - 4)

		fHeliMaxRange_Y = (vHeliCoords.y + 4)
		fHeliMinRange_Y = (vHeliCoords.y - 4)	
		
		IF VclosestNode.z > vHeliCoords.z
			IF VclosestNode.x <= fHeliMaxRange_X
			AND VclosestNode.x >= fHeliMinRange_X
			AND VclosestNode.y <= fHeliMaxRange_Y
			AND VclosestNode.y >= fHeliMinRange_Y
			AND GET_GAME_TIMER() > (iLastIncrementTimer + 8000)
				iLastIncrementTimer = GET_GAME_TIMER()
				PRINTSTRING("player passing under road bridge")
				completeCount++
			ENDIF
		ENDIF
	ENDIF
	
	RETURN completeCount
ENDFUNC

//PURPOSE: Changes the state of the door
PROC Doors_Set_State(DOOR_ENUM eDoor, BOOL lockState, FLOAT openRatio, INT iTime = 0)
	IF sDoors[eDoor].iHash != 0
		// No update over time, do NOW!
		IF iTime <= 0
			IF openRatio != DOORS_KEEP_CURRENT_RATIO
				DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[eDoor].iHash, openRatio, FALSE, TRUE)
			ENDIF
			
			IF lockState
				DOOR_SYSTEM_SET_DOOR_STATE(sDoors[eDoor].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(sDoors[eDoor].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
			ENDIF
			
			sDoors[eDoor].iTimeStamp = 0
			sDoors[eDoor].iInterpTime = 0
			
		// Needs updating over time, mark for frame by frame update
		ELIF openRatio != DOORS_KEEP_CURRENT_RATIO 
			sDoors[eDoor].bChangingState = TRUE
			sDoors[eDoor].bDesiredLockState = lockState
			sDoors[eDoor].fDesiredOpenRatio = openRatio
			
			sDoors[eDoor].iTimeStamp = GET_GAME_TIMER()
			sDoors[eDoor].iInterpTime = iTime
			
			sDoors[eDoor].fStartOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(sDoors[eDoor].iHash)
		ELSE
			SCRIPT_ASSERT("DOOR_SYSTEM: CANNOT OPEN DOOR OVER TIME TO KEEP THE SAME DOOR RATIO, POINTLESS!")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Obtains the state of the door
PROC Doors_Get_State(DOOR_ENUM eDoor, BOOL &bLockState, FLOAT &fOpenRatio)
	IF sDoors[eDoor].iHash != 0
		fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(sDoors[eDoor].iHash)
		DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(sDoors[eDoor].iHash)
		
		bLockState = Is_Door_LockState_Locked(eDoorState)
	ENDIF
ENDPROC
//
//PURPOSE: Initialises the door structs 
PROC Doors_Initialise()
	
	Doors_Add_New(mdf_Walkway_door, 		<< 918.1691,-1693.1252,43.2220 >>, 			PROP_MAP_DOOR_01, TRUE)
	Doors_Add_New(mdf_mid_door, 			<< 905.9557,-1699.7339,43.3106 >>, 			PROP_PRIS_DOOR_02, TRUE)
	Doors_Add_New(mdf_Top_door,				<< 901.2440,-1687.1495,47.4301 >>, 			PROP_MAP_DOOR_01, TRUE)
	Doors_Add_New(mdf_Helipad_door,			<< 916.4495,-1700.7896,51.3335 >>, 			PROP_MAP_DOOR_01, TRUE)
	Doors_Add_New(mdf_FRONT_GATE_R,			<< 965.4849,-1738.3374,31.3419 >>, 			PROP_FACGATE_03_L, TRUE)
	Doors_Add_New(mdf_FRONT_GATE_L,			<< 955.4418,-1737.4674,31.3485 >>, 			PROP_FACGATE_03_L, TRUE)
	Doors_Add_New(mdf_Garage_door,			<< 950.7746,-1698.2271,31.4447 >>, 			PROP_GAR_DOOR_04, TRUE)
	
ENDPROC

//PURPOSE: Resets all the doors streamed in
PROC Doors_Reset_All()
	INT x
	REPEAT COUNT_OF(sDoors) x
		IF sDoors[x].iHash != 0
			DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[x].iHash, 0.0, FALSE, TRUE)
			DOOR_SYSTEM_SET_DOOR_STATE(sDoors[x].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ENDIF
		
		sDoors[x].bChangingState 	= FALSE
		sDoors[x].iInterpTime 		= 0
		sDoors[x].iTimeStamp		= 0
		sDoors[x].fDesiredOpenRatio	= 0.0
		sDoors[x].bDesiredLockState = FALSE
		sDoors[x].fStartOpenRatio	= 0.0
	ENDREPEAT
ENDPROC


//PURPOSE: Manage locking and opening of doors
PROC Manage_Doors()
	INT idoor
	FOR idoor = 0 TO enum_to_int(MDF_NUM_OF_DOORS) -1
		IF sDoors[idoor].iHash != 0
		AND sDoors[idoor].bChangingState
		
			BOOL bLockState = FALSE
			FLOAT fOpenRatio = 0.0
			Doors_Get_State(int_to_enum(DOOR_ENUM, idoor), bLockState, fOpenRatio)
	
			// If state does not match then update to new state
			IF bLockState != sDoors[idoor].bDesiredLockState
			OR fOpenRatio != sDoors[idoor].fDesiredOpenRatio

				IF sDoors[idoor].iInterpTime > 0
				AND sDoors[idoor].iTimeStamp > 0

					FLOAT fInterpProg = CLAMP((GET_GAME_TIMER() - sDoors[idoor].iTimeStamp)/TO_FLOAT(sDoors[idoor].iInterpTime), 0.0, 1.0)
					IF fInterpProg != 1.0
						
						// change in progress
						fOpenRatio = sDoors[idoor].fStartOpenRatio + (fInterpProg * (sDoors[idoor].fDesiredOpenRatio - sDoors[idoor].fStartOpenRatio))
						bLockState = TRUE
						
					ELSE
						
						// door finished
						bLockState = sDoors[idoor].bDesiredLockState
						fOpenRatio = sDoors[idoor].fDesiredOpenRatio
						sDoors[idoor].bChangingState = FALSE

						sDoors[idoor].iInterpTime		= 0
						sDoors[idoor].iTimeStamp 		= 0
						sDoors[idoor].bChangingState 	= FALSE
					ENDIF
				ELSE
					// Set to the desired values as no time was present
					IF sDoors[idoor].bDesiredLockState
						bLockState = TRUE
					ELSE
						bLockState = FALSE
					ENDIF
					fOpenRatio = sDoors[idoor].fDesiredOpenRatio
					
					sDoors[idoor].iInterpTime		= 0
					sDoors[idoor].iTimeStamp 		= 0
					sDoors[idoor].bChangingState 	= FALSE
				ENDIF
				
				IF fOpenRatio != DOORS_KEEP_CURRENT_RATIO
					DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[idoor].iHash, fOpenRatio, FALSE, TRUE)
				ENDIF
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[idoor].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[idoor].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
			ELSE
			
				DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[idoor].iHash, fOpenRatio, FALSE, TRUE)
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[idoor].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[idoor].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
			
				// door already in the desired stage
				sDoors[idoor].iInterpTime		= 0
				sDoors[idoor].iTimeStamp 		= 0
				sDoors[idoor].bChangingState 	= FALSE
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//PURPOSE: Manages locking and unlocking the doors of the fake rooms once peds have left/entered the room. Additionally it also blips peds as they exit the fake rooms and into the main room.
PROC Manage_Stealth_Door_Locks()
	INT x	
	
	BOOL bUnlockWalkWay, bUnlockMid, bUnlockTop, bUnlockHelipad
	
	REPEAT COUNT_OF(mafia) x
		IF isentityalive(mafia[x].id)
		
			// walkway
			IF IS_ENTITY_IN_ANGLED_AREA(mafia[x].id, <<918.52936, -1691.67383, 42.11221>>, <<913.47546, -1691.28894, 45.00836>>, 3.25)	// ROOM LOCATE
			or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<918.52936, -1691.67383, 42.11221>>, <<913.47546, -1691.28894, 45.00836>>, 3.25)	// ROOM LOCATE				
				mafia[x].iExitingThisRoom = 1
				bUnlockWalkWay = TRUE
			
			ELIF mafia[x].iExitingThisRoom = 1
				// still too close to the door 
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mafia[x].id), <<918.23669, -1692.44238, 42.15291>>, false) <= 2.0  // DOORWAY LOCATE						
					bUnlockWalkWay = TRUE
				ELSE
					// reset
					mafia[x].iExitingThisRoom = -1
				ENDIF
			ENDIF
			
			// Mid
			IF IS_ENTITY_IN_ANGLED_AREA(mafia[x].id, <<906.56635, -1700.03198, 44.46795>>, <<906.90106, -1695.90137, 42.11657>>, 1.6)	// ROOM LOCATE
			or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<906.56635, -1700.03198, 44.46795>>, <<906.90106, -1695.90137, 42.11657>>, 1.6)
				mafia[x].iExitingThisRoom = 2
				bUnlockMid = TRUE
			
			ELIF mafia[x].iExitingThisRoom = 2
				// still too close to the door 
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mafia[x].id), <<906.58875, -1699.84241, 42.15248>>, false) <= 2.0 // DOORWAY LOCATE	
					bUnlockMid = TRUE
				ELSE
					// reset
					mafia[x].iExitingThisRoom = -1
				ENDIF
			ENDIF
			
			// top
			IF IS_ENTITY_IN_ANGLED_AREA(mafia[x].id, <<900.85522, -1687.26392, 46.14490>>, <<905.75562, -1687.79346, 49.13612>>, 3.5)
			or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<900.85522, -1687.26392, 46.14490>>, <<905.75562, -1687.79346, 49.13612>>, 3.5)
				mafia[x].iExitingThisRoom = 3
				bUnlockTop = TRUE
			
			ELIF mafia[x].iExitingThisRoom = 3
				// still too close to the door 
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mafia[x].id), <<901.11060, -1686.51807, 46.35753>> , false) <= 2.5  			// DOORWAY LOCATE	
				or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()), <<901.11060, -1686.51807, 46.35753>> , false) <= 3.0  			// DOORWAY LOCATE
					bUnlockTop = TRUE
				ELSE
					// reset
					mafia[x].iExitingThisRoom = -1
				ENDIF
			ENDIF
		ENDIF
			if mafia[x].iExitingThisRoom <> -1
				if isentityalive(mafia[x].id)
					SET_PED_RESET_FLAG(mafia[x].id,PRF_SearchForClosestDoor,true)
					SET_PED_RESET_FLAG(mafia[x].id,PRF_IgnoreNavigationForDoorArmIK ,true)
				endif
			endif
	ENDREPEAT
	
	// Main doors
	if isentityalive(peds[mpf_rocco].id)
		IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_rocco].id, <<915.59467, -1703.85645, 50.26351>>, <<915.77411, -1700.83813, 53.61048>>, 5.562500)
			peds[mpf_rocco].iExitingThisRoom = 4
			bUnlockHelipad = TRUE
		
		ELIF peds[mpf_rocco].iExitingThisRoom = 4
			// still too close to the door 
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(peds[mpf_rocco].id), <<915.84570, -1700.80469, 50.26309>> , false) <= 2.5  			// DOORWAY LOCATE	
				bUnlockHelipad = TRUE
			ELSE
				// reset
				peds[mpf_rocco].iExitingThisRoom = -1
			ENDIF
		ENDIF
	endif
	if isentityalive(peds[mpf_pilot].id)
		IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_pilot].id, <<915.59467, -1703.85645, 50.26351>>, <<915.77411, -1700.83813, 53.61048>>, 2.11)
			peds[mpf_pilot].iExitingThisRoom = 4
			bUnlockHelipad = TRUE
		
		ELIF peds[mpf_pilot].iExitingThisRoom = 4
			// still too close to the door 
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(peds[mpf_pilot].id), <<915.84570, -1700.80469, 50.26309>> , false) <= 2.5  			// DOORWAY LOCATE	
				bUnlockHelipad = TRUE
			ELSE
				// reset
				peds[mpf_pilot].iExitingThisRoom = -1
			ENDIF
		ENDIF
	endif
	
	IF bUnlockWalkWay
		Doors_Set_State(mdf_Walkway_door, FALSE, DOORS_KEEP_CURRENT_RATIO)
	ELSE
		Doors_Set_State(mdf_Walkway_door, TRUE, 0.0, 500)
	ENDIF
	
	IF bUnlockmid
		Doors_Set_State(mdf_mid_door, FALSE, DOORS_KEEP_CURRENT_RATIO)
	ELSE
		Doors_Set_State(mdf_mid_door, TRUE, 0.0, 500)
	ENDIF
	
	IF bUnlockTop
		Doors_Set_State(mdf_Top_door, FALSE, DOORS_KEEP_CURRENT_RATIO)
	ELSE//
		Doors_Set_State(mdf_Top_door, TRUE, 0.0, 600)
	ENDIF
	
	IF bUnlockHelipad
		Doors_Set_State(mdf_Helipad_door, FALSE, DOORS_KEEP_CURRENT_RATIO)
	ELSE
		Doors_Set_State(mdf_Helipad_door, TRUE, 0.0, 500)
	ENDIF
	
	Doors_Set_State(mdf_FRONT_GATE_R,true,0,500)
	Doors_Set_State(mdf_FRONT_GATE_L,true,0,500)
	Doors_Set_State(mdf_Garage_door,true,0,500)
	
ENDPROC

proc Task_perform_Normal_seq(eEnemies_Mafia mafped)
//=====================	===================== =====================	=====================	
	if 	mafped = maf_car
		OPEN_SEQUENCE_TASK(seq)
			if not IS_ENTITY_AT_COORD(mafia[maf_car].id,mafia[maf_car].vStartLoc,<<1,1,1>>)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,mafia[maf_car].vStartLoc,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_SUPPRESS_EXACT_STOP)
			endif
//			if not IS_PED_HEADING_TOWARDS_POSITION(mafia[maf_car].id,<<956.77148, -1703.71277, 28.70344>>,40)
//				TASK_TURN_PED_TO_FACE_COORD(null,<<956.77148, -1703.71277, 28.70344>>)					//BUG FIX 1399373
//			endif
			if GET_ENTITY_HEADING(mafia[maf_car].id) <> Mafia[maf_car].fheading
				TASK_ACHIEVE_HEADING(NULL, Mafia[maf_car].fheading)								//BUG FIX 1399373
			endif
			if not IS_PED_HEADTRACKING_PED(mafia[maf_car].id,mafia[maf_car].partner)
				TASK_LOOK_AT_ENTITY(null,mafia[maf_car].partner,-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			endif
			TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b",WALK_BLEND_IN)
			TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_d")
			TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_e",NORMAL_BLEND_IN,WALK_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_car].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
		
	elif 	mafped = maf_car_talking
		OPEN_SEQUENCE_TASK(seq)
			if not IS_ENTITY_AT_COORD(mafia[maf_car_talking].id,mafia[maf_car_talking].vStartLoc,<<1,1,1>>)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,mafia[maf_car_talking].vStartLoc,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_SUPPRESS_EXACT_STOP)
			endif
//			if not IS_PED_FACING_PED(mafia[maf_car_talking].id,mafia[maf_car_talking].partner,40)
//				//TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_car_talking].partner)						//BUG FIX 1399373
//				TASK_ACHIEVE_HEADING(NULL, Mafia[maf_car_talking].fheading)
//			endif
			if GET_ENTITY_HEADING(mafia[maf_car_talking].id) <> Mafia[maf_car_talking].fheading
				TASK_ACHIEVE_HEADING(NULL, Mafia[maf_car_talking].fheading)								//BUG FIX 1399373
			endif
			if not IS_PED_HEADTRACKING_PED(mafia[maf_car_talking].id,mafia[maf_car_talking].partner)
				TASK_LOOK_AT_ENTITY(null,mafia[maf_car_talking].partner,-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			endif
			TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_a",WALK_BLEND_IN)
			TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_c")
			TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b",NORMAL_BLEND_IN,WALK_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_car_talking].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
		
//=====================	===================== =====================	=====================	
	elif mafped = maf_walkway_door
		OPEN_SEQUENCE_TASK(seq)
			TASK_LOOK_AT_ENTITY(null,mafia[maf_walkway_door].partner,-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			//at right vent
			if IS_ENTITY_AT_COORD(mafia[maf_walkway_door].id,<<943.27026, -1680.55713, 41.06182>>,<<4,4,4>>)
				SET_PED_RESET_FLAG(mafia[maf_walkway_door].id,PRF_SearchForClosestDoor,FALSE)
				if IS_ENTITY_AT_COORD(mafia[maf_walkway_door].partner,<<945.01746, -1679.36267, 41.06182>>,<<4,4,4>>)				
					
					if not IS_PED_HEADING_TOWARDS_POSITION(mafia[maf_walkway_door].id,<<945.01746, -1679.36267, 41.06182>>,30)
						TASK_TURN_PED_TO_FACE_COORD(null,<<945.01746, -1679.36267, 41.06182>>)
					endif
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b",WALK_BLEND_in)
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_a")	
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_c")
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_a")	
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b",NORMAL_BLEND_IN,WALK_BLEND_OUT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<939.12335, -1692.30664, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)
					Mafia[maf_walkway_door].vtarget = <<939.12335, -1692.30664, 41.06182>>
					bDoWalkwayChat = true
				else
					TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_walkway_door].partner)	
				endif
				 
			//at stairs
			elif IS_ENTITY_AT_COORD(mafia[maf_walkway_door].id,<<937.74927, -1693.96704, 41.06182>>,<<3,3,3>>)
				if IS_ENTITY_AT_COORD(mafia[maf_walkway_door].partner,<<937.74927, -1693.96704, 41.06182>>,<<3,3,3>>)
					
					if not IS_PED_FACING_PED(mafia[maf_walkway_door].id,mafia[maf_walkway_door].partner,30)
						TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_walkway_door].partner)
					endif
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_c",WALK_BLEND_in)
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_a")						
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b",NORMAL_BLEND_IN,WALK_BLEND_OUT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<940.02069, -1698.03931, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)		
					Mafia[maf_walkway_door].vtarget = <<940.02069, -1698.03931, 41.06182>>
					bDoWalkwayChat = true
				else
					TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_walkway_door].partner)	
				endif
			//at left vent
			elif IS_ENTITY_AT_COORD(mafia[maf_walkway_door].id,<<937.99335, -1700.34326, 41.06182>>,<<4,4,4>>)
				if IS_ENTITY_AT_COORD(mafia[maf_walkway_door].partner,<<937.99335, -1700.34326, 41.06182>>,<<4,4,4>>)
					
					if not IS_PED_FACING_PED(mafia[maf_walkway_door].id,mafia[maf_walkway_door].partner,30)
						TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_walkway_door].partner)
					endif
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_e",WALK_BLEND_in)
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_a")	
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_c")
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b")	
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_d",NORMAL_BLEND_IN,WALK_BLEND_OUT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<943.27026, -1680.55713, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)	
					Mafia[maf_walkway_door].vtarget = <<943.27026, -1680.55713, 41.06182>>
					bDoWalkwayChat = true
				else
					TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_walkway_door].partner)	
				endif			
			else	
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,Mafia[maf_walkway_door].vtarget,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)		
			endif
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_walkway_door].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
//=====================	===================== =====================	=====================	
	
	elif 	mafped = maf_worker_walkway	
		OPEN_SEQUENCE_TASK(seq)
			TASK_LOOK_AT_ENTITY(null,mafia[maf_worker_walkway].partner,-1)
			//at right vent
			if IS_ENTITY_AT_COORD(mafia[maf_worker_walkway].id,<<945.01746, -1679.36267, 41.06182>>,<<4,4,4>>)
				SET_PED_RESET_FLAG(mafia[maf_worker_walkway].id,PRF_SearchForClosestDoor,FALSE)
				if IS_ENTITY_AT_COORD(mafia[maf_worker_walkway].partner,<<943.27026, -1680.55713, 41.06182>>,<<4,4,4>>)				
					TASK_TURN_PED_TO_FACE_COORD(null,<<945.24200, -1676.69360, 41.06182>>)
					TASK_PLAY_ANIM(null,animDict_kneel_enter,"enter",walk_BLEND_IN,NORMAL_BLEND_OUT)
					TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_b",normal_BLEND_IN,NORMAL_BLEND_OUT)
					TASK_PLAY_ANIM(null,animDict_kneel_exit,"exit",normal_BLEND_IN,walk_BLEND_OUT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<937.41699, -1694.07996, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)
					Mafia[maf_worker_walkway].vtarget = <<937.41699, -1694.07996, 41.06182>>
				else
					TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_worker_walkway].partner)	
				endif
			//at stairs
			elif IS_ENTITY_AT_COORD(mafia[maf_worker_walkway].id,<<937.74927, -1693.96704, 41.06182>>,<<3,3,3>>)
				if IS_ENTITY_AT_COORD(mafia[maf_worker_walkway].partner,<<937.74927, -1693.96704, 41.06182>>,<<3,3,3>>)
					
					if not IS_PED_FACING_PED(mafia[maf_worker_walkway].id,mafia[maf_worker_walkway].partner,30)
						TASK_TURN_PED_TO_FACE_ENTITY(null,mafia[maf_worker_walkway].partner)
					endif
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_d",walk_BLEND_IN)
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_c")		
					TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_e",normal_BLEND_IN,walk_BLEND_OUT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<936.80951, -1701.27502, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)		
					Mafia[maf_worker_walkway].vtarget = <<936.80951, -1701.27502, 41.06182>>
				else
					TASK_TURN_PED_TO_FACE_ENTITY(null,Mafia[maf_worker_walkway].id)	
				endif
			//at left vent
			elif IS_ENTITY_AT_COORD(mafia[maf_worker_walkway].id,<<937.99335, -1700.34326, 41.06182>>,<<4,4,4>>)
				if IS_ENTITY_AT_COORD(mafia[maf_worker_walkway].partner,<<937.99335, -1700.34326, 41.06182>>,<<4,4,4>>)
					TASK_TURN_PED_TO_FACE_COORD(null,<<937.13586, -1699.16711, 42.38544>>)
					TASK_PLAY_ANIM(null,animDict_kneel_enter,"enter",walk_BLEND_IN,NORMAL_BLEND_OUT)
					TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_a",normal_BLEND_IN,NORMAL_BLEND_OUT)
					TASK_PLAY_ANIM(null,animDict_kneel_exit,"exit",normal_BLEND_IN,walk_BLEND_OUT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<945.01746, -1679.36267, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)
					Mafia[maf_worker_walkway].vtarget = <<945.01746, -1679.36267, 41.06182>>
				else
					TASK_TURN_PED_TO_FACE_ENTITY(null,Mafia[maf_worker_walkway].partner)	
				endif			
			else	
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,Mafia[maf_worker_walkway].vtarget,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT)		
			endif
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_worker_walkway].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
//=====================	===================== =====================	=====================			
	elif mafped = maf_worker_top
	OPEN_SEQUENCE_TASK(seq)		
		//big vent
		if IS_ENTITY_AT_COORD(mafia[maf_worker_top].id,vBIGVENT,<<2,2,2>>)
			SET_PED_RESET_FLAG(mafia[maf_worker_top].id,PRF_SearchForClosestDoor,FALSE)
			CLEAR_PED_SECONDARY_TASK(mafia[maf_worker_top].id)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_a",WALK_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,walk_BLEND_OUT)		
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_a",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)	
			TASK_FOLLOW_NAV_MESH_TO_COORD(null,vCROUCHVENT,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,2.27)
			Mafia[maf_worker_top].vtarget = vCROUCHVENT
		//crouch
		elif IS_ENTITY_AT_COORD(mafia[maf_worker_top].id,vCROUCHVENT,<<2,2,2>>)
			CLEAR_PED_SECONDARY_TASK(mafia[maf_worker_top].id)
			TASK_PLAY_ANIM(null,animDict_kneel_enter,"enter",walk_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_a",normal_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,animDict_kneel_exit,"exit",normal_BLEND_IN,walk_BLEND_OUT)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(null,vBOXPOS,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-3.24)
			Mafia[maf_worker_top].vtarget = vBOXPOS
		//box	
		elif IS_ENTITY_AT_COORD(mafia[maf_worker_top].id,vBOXPOS,<<2,2,2>>)
			CLEAR_PED_SECONDARY_TASK(mafia[maf_worker_top].id)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",WALK_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_a",NORMAL_BLEND_IN,walk_BLEND_OUT)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING	)
			TASK_FOLLOW_NAV_MESH_TO_COORD(null,vBIGVENT,PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,127.67)
			Mafia[maf_worker_top].vtarget = vBIGVENT		
		else
			if 	Mafia[maf_worker_top].vtarget.x = vCROUCHVENT.x
			and Mafia[maf_worker_top].vtarget.y = vCROUCHVENT.y
			and Mafia[maf_worker_top].vtarget.z = vCROUCHVENT.z
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING	)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,vCROUCHVENT,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,2.27)
			elif Mafia[maf_worker_top].vtarget.x = vBOXPOS.x
			or Mafia[maf_worker_top].vtarget.y = vBOXPOS.y
			or Mafia[maf_worker_top].vtarget.z = vBOXPOS.z
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING	)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,vBOXPOS,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-3.24)
			else
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING	)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,vBIGVENT,PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,127.67)
			endif
		endif
		
	CLOSE_SEQUENCE_TASK(seq)
	TASK_PERFORM_SEQUENCE(mafia[maf_worker_top].id,seq)
	CLEAR_SEQUENCE_TASK(seq)
	
//=====================	===================== =====================	=====================		
	elif 	mafped = maf_worker_vent
		OPEN_SEQUENCE_TASK(seq)					
			if not IS_PED_HEADING_TOWARDS_POSITION(mafia[maf_worker_vent].id,<<889.07233, -1714.90295, 38.47169>>,40)			
				TASK_TURN_PED_TO_FACE_COORD(null,<<889.07233, -1714.90295, 38.47169>>)
			endif			
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_a",WALK_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,WALK_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_worker_vent].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
//=====================	===================== =====================	=====================	
	elif 	mafped = maf_worker_door
		OPEN_SEQUENCE_TASK(seq)	
			if IS_ENTITY_AT_COORD(mafia[maf_worker_door].id,vMIDVENTSMALL,<<2,2,2>>)	
				SET_PED_RESET_FLAG(mafia[maf_worker_door].id,PRF_SearchForClosestDoor,FALSE)
				CLEAR_PED_SECONDARY_TASK(mafia[maf_worker_door].id)
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_a",WALK_BLEND_IN)
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,walk_BLEND_OUT)	
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",WALK_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,vMIDVENTBIG,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,151.41)
				Mafia[maf_worker_door].vtarget = vMIDVENTBIG				
				
			elif IS_ENTITY_AT_COORD(mafia[maf_worker_door].id,vMIDVENTBIG,<<2,2,2>>)				
				CLEAR_PED_SECONDARY_TASK(mafia[maf_worker_door].id)
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,WALK_BLEND_OUT)	
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",WALK_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,vMIDVENTSMALL,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-163.74)			
				Mafia[maf_worker_door].vtarget = vMIDVENTSMALL
			elif IS_ENTITY_IN_ANGLED_AREA(mafia[maf_worker_door].id,<<906.62262, -1699.76147, 44.40839>>,<<907.29437, -1691.58569, 42.11664>>,3)	
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",WALK_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
				TASK_GO_STRAIGHT_TO_COORD(null,<<906.34631, -1701.62073, 41.97026>>,PEDMOVE_WALK)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,vMIDVENTSMALL,PEDMOVE_WALK)
			else
				if Mafia[maf_worker_door].vtarget.x = vMIDVENTSMALL.x
				or  Mafia[maf_worker_door].vtarget.y = vMIDVENTSMALL.y
				or  Mafia[maf_worker_door].vtarget.z = vMIDVENTSMALL.z
					TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING	)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, vMIDVENTSMALL,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-163.74)			
				else
					TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",walk_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING	)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,vMIDVENTBIG,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,151.41)
				endif
			endif
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_worker_door].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
//=====================	===================== =====================	=====================	
		
	elif 	mafped = maf_worker_alone
		OPEN_SEQUENCE_TASK(seq)
			if bAloneWorkerActive
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<924.75250, -1720.31702, 37.06301>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,79.87)
				if not IS_PED_HEADING_TOWARDS_POSITION(mafia[maf_worker_alone].id,<<923.66852, -1720.09009, 38.26234>>,40)			
					TASK_TURN_PED_TO_FACE_COORD(null,<<923.66852, -1720.09009, 38.26234>>)
				endif
				TASK_PLAY_ANIM(null,animDict_kneel_enter,"enter",WALK_BLEND_IN,NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_a",normal_BLEND_IN,NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_b",normal_BLEND_IN,NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_b",normal_BLEND_IN,NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(null,animDict_kneel_exit,"exit",NORMAL_BLEND_IN,WALK_BLEND_OUT)
			endif
			if not IS_ENTITY_AT_COORD(mafia[maf_worker_alone].id,<<938.72675, -1717.90356, 37.06301>>,<<2,2,2>>)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<938.72675, -1717.90356, 37.06301>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,79.87)
			endif
			if not IS_PED_HEADING_TOWARDS_POSITION(mafia[maf_worker_alone].id,<<936.85815, -1716.89111, 38.38630>>,30)			
				TASK_TURN_PED_TO_FACE_COORD(null,<<936.85815, -1716.89111, 38.38630>>)
			endif
			TASK_PLAY_ANIM(null,animDict_kneel_enter,"enter",WALK_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_c",normal_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_b",normal_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,animDict_kneel_Idle,"idle_c",normal_BLEND_IN,NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(null,animDict_kneel_exit,"exit",NORMAL_BLEND_IN,WALK_BLEND_OUT)
		CLOSE_SEQUENCE_TASK(seq)
		TASK_PERFORM_SEQUENCE(mafia[maf_worker_alone].id,seq)
		CLEAR_SEQUENCE_TASK(seq)
		
	endif	
endproc
proc set_enemy_standards(ped_index ped)
	set_ped_relationship_group_hash(ped, rel_group_enemies)
	set_ped_as_enemy(ped, true)
	set_ped_path_can_use_ladders(ped, true)
	set_ped_money(ped,0)
	set_ped_alertness(ped,as_not_alert)
	set_ped_flee_attributes(ped,fa_never_flee,true)
	stop_ped_speaking(ped,true)	
	set_entity_load_collision_flag(ped,true)
	
	//combat attributes		
	set_ped_combat_attributes(ped,ca_can_shoot_without_los ,false)
	set_ped_combat_attributes(ped, ca_use_cover,true)
	set_ped_combat_attributes(ped,ca_can_investigate,true)
	set_ped_combat_attributes(ped, ca_use_vehicle, false)
	set_ped_combat_attributes(ped, CA_ALWAYS_FIGHT, false)
	
	set_ped_config_flag(ped,pcf_runfromfiresandexplosions,false)
	set_ped_accuracy(ped,25)
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(ped,true)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped,true)
	
//		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(Mafia[ped].id)	
endproc
proc create_MafiaEnemy(eEnemies_Mafia ped,float fseeingrange = 60.0)
		
	if ped = maf_worker_alone
	or ped = maf_worker_walkway
	or ped = maf_worker_top
	or ped = maf_worker_door
	or ped = maf_worker_vent
	or ped = maf_car
	or ped = maf_car_talking
		Mafia[ped].model = modelWorker	
	endif
	Mafia[ped].id = create_ped(PEDTYPE_MISSION,Mafia[ped].model,Mafia[ped].vStartLoc,Mafia[ped].fheading)
			
	set_ped_seeing_range(Mafia[ped].id,fseeingrange)		
	SET_PED_VISUAL_FIELD_CENTER_ANGLE(Mafia[ped].id,55)
	SET_PED_VISUAL_FIELD_MAX_ANGLE(Mafia[ped].id,85)
	SET_PED_VISUAL_FIELD_MIN_ANGLE(Mafia[ped].id,-85)
	SET_PED_KEEP_TASK(Mafia[ped].id,true)
	
	if ped = maf_worker_vent
	or ped = maf_worker_top
	or ped = maf_worker_door
		mafia[ped].obj = CREATE_OBJECT(modelclipboard,Mafia[ped].vStartLoc) //1399385 removing this as it isn't being held correctly. 
		Mafia[ped].bObjAttached = false		
		SET_ENTITY_HEALTH(Mafia[ped].id,120)		
		SET_PED_WEAPON_MOVEMENT_CLIPSET(Mafia[ped].id,"MOVE_M@CLIPBOARD")
	elif ped = maf_worker_alone
		SET_ENTITY_HEALTH(Mafia[ped].id,120)
	else
		SET_ENTITY_HEALTH(Mafia[ped].id,130)
	endif
		
	mafia[ped].sit	= SSIT_AI_NONE
	
	set_enemy_standards(Mafia[ped].id)	
endproc
proc clear_players_task_on_control_input(script_task_name task_name)
	if IS_SCRIPT_TASK_RUNNING_OR_STARTING(player_ped_id(), task_name)
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28	
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
		IF NOT IS_LOOK_INVERTED()
			right_stick_y *= -1
		ENDIF		
		// invert the vertical
		IF (left_stick_y > STICK_DEAD_ZONE)
		OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
		or (left_stick_x > STICK_DEAD_ZONE)
		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
		//or is_control_pressed(player_control, input_sprint) 		
			clear_ped_tasks(player_ped_id())			
		endif 		
	endif 
endproc 
proc point_gameplay_cam_at_coord(float targetheading)	
	set_gameplay_cam_relative_heading(targetheading - get_entity_heading(player_ped_id()))
endproc
PROC SETUP_ROCCO()
	if isentityalive(peds[mpf_rocco].id)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_rocco].id,true)
		set_ped_relationship_group_hash(peds[mpf_rocco].id, rel_group_enemies)
		SET_ENTITY_HEALTH(rocco(),GET_ENTITY_HEALTH(rocco())*2)
		SET_PED_COMBAT_ABILITY(rocco(),CAL_PROFESSIONAL)
		SET_PED_COMBAT_ATTRIBUTES(rocco(),CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED,true)
		SET_PED_COMBAT_ATTRIBUTES(rocco(),CA_ALWAYS_FIGHT,true)
		SET_PED_COMBAT_ATTRIBUTES(rocco(),CA_USE_COVER,false)
		SET_PED_COMBAT_MOVEMENT(rocco(),CM_WILLADVANCE)
		SET_PED_SUFFERS_CRITICAL_HITS(rocco(),FALSE)		
		SET_PED_CONFIG_FLAG(rocco(),PCF_OpenDoorArmIK,true)	
	endif
ENDPROC
PROC SETUP_Milton()
	if isentityalive(peds[mpf_milton].id)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Milton(),true)
		SET_PED_CONFIG_FLAG(Milton(),PCF_ForcedToUseSpecificGroupSeatIndex,true)
		set_ped_relationship_group_hash(Milton(),rel_friends)
		SET_PED_COMPONENT_VARIATION(Milton(),PED_COMP_JBIB,1,0)
		SET_PED_COMPONENT_VARIATION(Milton(),PED_COMP_LEG,1,0)
		SET_PED_COMPONENT_VARIATION(Milton(),PED_COMP_TORSO,1,0)
		SET_PED_COMPONENT_VARIATION(Milton(),PED_COMP_FEET,1,0)
//		SET_PED_PROP_INDEX(milton(),ANCHOR_EYES,0,0)
		SET_PED_CAN_BE_TARGETTED(Milton(),false)
	endif
ENDPROC
PROC SETUP_Anton()
	if isentityalive(peds[mpf_Anton].id)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_Anton].id,true)
		SET_PED_CONFIG_FLAG(peds[mpf_Anton].id,PCF_ForcedToUseSpecificGroupSeatIndex,true)
		SET_PED_COMPONENT_VARIATION(Anton(),PED_COMP_LEG,0,0)
		SET_PED_COMPONENT_VARIATION(Anton(),PED_COMP_TORSO,0,0)
//		SET_PED_PROP_INDEX(anton(),ANCHOR_EYES,0,0)
		set_ped_relationship_group_hash(peds[mpf_Anton].id,rel_friends)
		SET_PED_CAN_BE_TARGETTED(Anton(),false)
	endif
ENDPROC
PROC SETUP_Gianni()
	set_ped_seeing_range(peds[mpf_Gianni].id,35)		
	SET_PED_VISUAL_FIELD_CENTER_ANGLE(peds[mpf_Gianni].id,55)
	SET_PED_VISUAL_FIELD_MAX_ANGLE(peds[mpf_Gianni].id,60)
	SET_PED_VISUAL_FIELD_MIN_ANGLE(peds[mpf_Gianni].id,-60)
	SET_PED_KEEP_TASK(peds[mpf_Gianni].id,true)
	set_ped_relationship_group_hash(peds[mpf_Gianni].id, rel_friends)
	set_ped_path_can_use_ladders(peds[mpf_Gianni].id, true)
	set_ped_money(peds[mpf_Gianni].id,0)
	set_ped_alertness(peds[mpf_Gianni].id,as_not_alert)	
	stop_ped_speaking(peds[mpf_Gianni].id,true)	
	set_entity_load_collision_flag(peds[mpf_Gianni].id,true)
		
	set_ped_config_flag(peds[mpf_Gianni].id,pcf_runfromfiresandexplosions,false)
	set_ped_accuracy(peds[mpf_Gianni].id,25)
	SET_PED_DROPS_WEAPONS_WHEN_DEAD(peds[mpf_Gianni].id,true)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_Gianni].id,true)
endproc	
proc Display_Placeholder()
	if bPlaceholder		
		#IF IS_DEBUG_BUILD
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1,"STRING","PLACEHOLDER")
		#ENDIF
	endif
endproc

// purpose:off radar/ hud / player control
proc prep_start_cutscene(bool bplayercontrol,vector varea,bool brender = true,bool binterp = false,bool bclearprj = true,int interptime = default_interp_to_from_game,set_player_control_flags controlflag = 0,bool bphoneaway = true)
	display_radar(false)
	display_hud(false)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	clear_prints()
	clear_help()
	render_script_cams(brender,binterp,interptime)	
	if bclearprj
		clear_area_of_projectiles(varea,500)
	endif
	STOP_FIRE_IN_RANGE(varea,200)
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(player_ped_id()),"OFF")
	endif
	SET_PED_STEALTH_MOVEMENT(player_peD_id(),false)
	if bphoneaway
		hang_up_and_put_away_phone()
	endif
endproc
// purpose:on radar/ hud / player control
proc prep_stop_cutscene(bool bplayercontrol,bool brender = false,bool binterp = false,int iduration = default_interp_to_from_game, set_player_control_flags controlflag = 0)
	display_radar(true)
	display_hud(true)	
	set_player_control(player_id(),bplayercontrol,controlflag)	
	render_script_cams(brender,binterp,iduration)	
	if not brender	
		destroy_all_cams()
	endif
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),true)
	endif
	bPlaceholder = false
endproc

proc SET_ALL_EXIT_STATES(bool bstate = false)
	 		
	bcs_mike 			= bstate
	bcs_cam				= bstate
	bcs_actor		 	= bstate
	bcs_heli			= bstate
	bcs_goon			= bstate
	bcs_director		= bstate
	//bcs_rocco			= bstate
	
endproc
proc set_peds_CS_OUTFITS()

IF ISENTITYALIVE(MIKE())
	Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister,MIKE(),"MICHAEL")
	REMOVE_PED_HELMET(MIKE(),true)
ENDIF
IF ISENTITYALIVE(peds[mpf_milton].id)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_TORSO,1,0,CS_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_LEG,1,0,CS_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_feet,1,0,CS_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_JBIB,1,0,CS_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_TORSO,1,0,IG_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_LEG,1,0,IG_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_feet,1,0,IG_MILTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_JBIB,1,0,IG_MILTON)
	CLEAR_ALL_PED_PROPS(peds[mpf_milton].id)
ENDIF
IF ISENTITYALIVE(peds[mpf_Anton].id)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_TORSO,0,0,CSB_ANTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_LEG,0,0,CSB_ANTON)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_TORSO,0,0,U_M_Y_ANTONB)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_LEG,0,0,U_M_Y_ANTONB)
	CLEAR_ALL_PED_PROPS(peds[mpf_Anton].id)
ENDIF
IF ISENTITYALIVE(peds[mpf_rocco].id)
	Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister,rocco(),"Rocco")
	CLEAR_ALL_PED_PROPS(peds[mpf_rocco].id)
ENDIF		
	
endproc
func bool Check_Replay_Car(vector pos, float heading)
	//check 
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		if IS_CHECKPOINT_VEHICLE_SUITABLE()
			REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			while not HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
				wait(0)
			endwhile			
			CLEAR_AREA_OF_VEHICLES(pos,2)
			vehs[mvf_Replay_car].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(pos,heading)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_Replay_car].id,true)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_Replay_car].id,true)
			return true
		else
			return true
		endif
	else
		return true
	endif
return false
endfunc
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC RESET_EVERYTHING()
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
//======================================= Destroy ======================================
	//all peds are deleted
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_entity_COORDS(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif	
			if peds[i].id != player_ped_id()
				SAFE_DELETE_PED(peds[i].id)
			endif
		endif
	endfor		
	//enemies and their properties
	for i = 0 TO Enum_to_int(NUM_OF_Mafia_PEDS)-1
		Mafia[i].bkilled 		= false
		mafia[i].sit 			= SSIT_AI_NONE
		Mafia[i].bForceBlip 	= false
		Mafia[i].bHeardNoise 	= false
		cleanup_ai_ped_blip(Mafia[i].blipstruct)
		if DOES_ENTITY_EXIST(mafia[i].obj)
			if IS_ENTITY_ATTACHED(mafia[i].obj)
				DETACH_ENTITY(mafia[i].obj)
			endif
			SAFE_DELETE_OBJECT(mafia[i].obj)
		endif
		if DOES_ENTITY_EXIST(Mafia[i].id)
			SAFE_DELETE_PED(Mafia[i].id)	
		endif
	endfor	
	
	//all vehicles are deleted
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)
			SAFE_DELETE_VEHICLE(vehs[i].id)
		endif
	endfor	
	
//--------------------- objects -----------------------	
	for i = 0 to enum_to_int(MAX_NUM_CONST_OBJS) -1
		constObjs[i].bloaded = false 
		if DOES_ENTITY_EXIST(constObjs[i].id)
			SAFE_DELETE_OBJECT(constObjs[i].id)
		endif
	endfor	
	
	//locates stuff
	clear_mission_locate_stuff(slocatesdata)
	clear_mission_location_text_and_blips(slocatesdata)
	
	if DOES_BLIP_EXIST(blip_objective)
		remove_blip(blip_objective)
	endif
	
	destroy_all_cams()
	clear_prints()
	kill_any_conversation()
	STOP_AUDIO_SCENES()
//======================================= RESETS ======================================
	STOP_GAMEPLAY_HINT()
	g_replay.iReplayInt[0] = 0
	g_replay.iReplayInt[1] = 0	
	clear_area(GET_ENTITY_COORDS(player_ped_id()),400,true)
	Doors_Reset_All()
	SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, TRUE)
//wanted level	
	CLEAR_PLAYER_WANTED_LEVEL(player_id())
//reset player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//clear AI tasks
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
	endif	
	prep_stop_cutscene(true)
	bAloneWorkerActive 	= false
	bDoWalkwayChat		= false
//stages
	eAI_stage	= AI_CHECKS
//AUDIO
	STOP_AUDIO_SCENES()
	iDialogue_Stage 	= 0
	iCarSpeechStage		= 0
	iwalkwaychat 		= 0 
	iconstructStage		= 0
	iScareStage			= 0
	
	shapetestheli = null
	
	bpauseconv 			= false
	bMusic_1stEnemy		= false
	bMusic_HeliTrigger	= false
	bMusic_Ends			= false
	b_cutscene_loaded 	= false
	iskipCS 			= 0
ENDPROC
PROC MISSION_CLEANUP()
	PRINTln("...SOLOMON 1 MISSION CLEANUP")
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()	
	ENDIF
	
//======================================= Destroy ======================================		
		
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if IsEntityAlive(peds[i].id)
			if peds[i].id != player_ped_id()
				SET_PED_KEEP_TASK(peds[i].id,true)
				if IS_PED_IN_GROUP(peds[i].id)
					REMOVE_PED_FROM_GROUP(peds[i].id)
				endif
				SAFE_RELEASE_PED(peds[i].id)
			endif
			if DOES_BLIP_EXIST(peds[i].blip)
				REMOVE_BLIP(peds[i].blip)
			endif
		endif
	endfor		
//-------------------- enemies-----------------------
	//Mafia
	for i = 0 to ENUM_TO_INT(NUM_OF_Mafia_PEDS) -1	
		if IsEntityAlive(Mafia[i].id)	
			SAFE_RELEASE_PED(Mafia[i].id)			
			if DOES_ENTITY_EXIST(mafia[i].obj)
				if IS_ENTITY_ATTACHED(mafia[i].obj)
					DETACH_ENTITY(mafia[i].obj)
				endif
				SAFE_RELEASE_OBJECT(mafia[i].obj)
			endif
			CLEANUP_AI_PED_BLIP(Mafia[i].blipstruct)
		endif
		Mafia[i].bForceBlip = false
		Mafia[i].bkilled = false
		mafia[i].sit = SSIT_AI_NONE
	endfor	
//---------------------vehs-----------------------
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if IsEntityAlive(vehs[i].id)		 
			SAFE_RELEASE_VEHICLE(vehs[i].id)
		endif
		if DOES_BLIP_EXIST(vehs[i].blip)
			REMOVE_BLIP(vehs[i].blip)
		endif
	endfor	
	
//--------------------- objects -----------------------	
	for i = 0 to enum_to_int(MAX_NUM_CONST_OBJS) -1
		constObjs[i].bloaded = false 
		if DOES_ENTITY_EXIST(constObjs[i].id)
			SAFE_RELEASE_OBJECT(constObjs[i].id)
		endif
	endfor	
	
	RENDER_SCRIPT_CAMS(false,false,DEFAULT_INTERP_TO_FROM_GAME,true,true)	
//====================================  Reset  ======================================
	if IsEntityAlive(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
		CLEAR_PLAYER_WANTED_LEVEL(player_id())				
	endif
	
	
	//reset player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
	SET_WANTED_LEVEL_MULTIPLIER(1)	//wanted level for mission reset to normal
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)	
		
	SET_ALL_RANDOM_PEDS_FLEE(player_id(),false)	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	if DOES_SCENARIO_GROUP_EXIST("SOLOMON_GATE")
		if not IS_SCENARIO_GROUP_ENABLED("SOLOMON_GATE")
			SET_SCENARIO_GROUP_ENABLED("SOLOMON_GATE",true)
		endif
	endif
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1037.91162, -495.88333, 34>>,<<-1016.92767, -471.04614, 37>>,true)
	
	enable_dispatch_service(dt_ambulance_department,false)
	enable_dispatch_service(dt_fire_department, 	false)
	
	set_vehicle_model_is_suppressed(FUGITIVE,false)
	set_vehicle_model_is_suppressed(FROGGER,false)
	
	STOP_AUDIO_SCENES()
	SET_AMBIENT_ZONE_STATE("AZ_SOL_1_FACTORY_AREA_CONSTRUCTIONS", FALSE, FALSE)
	
	DISABLE_TAXI_HAILING(FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE,false)
	CLEAR_PRINTS()
	REMOVE_TEXT_MESSAGE_FEED_ENTRY("SOL1_TEXT")
	
	if IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	endif
	
	CLEAR_WEATHER_TYPE_PERSIST()
	TRIGGER_MUSIC_EVENT("SOL1_FAIL")
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		MISSION PASS
// -----------------------------------------------------------------------------------------------------------

PROC MISSION_PASSED()

	PRINTSTRING("...SOLOMON 1 MISSION PASSED")
	PRINTNL()
	MISSION_FLOW_MISSION_PASSED()
	MISSION_CLEANUP()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC MISSION_FAILED(MFF_MISSION_FAIL_FLAGS FAIL_CONDITION = MFF_DEFAULT)
	TRIGGER_MUSIC_EVENT("SOL1_FAIL")
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()	
	ENDIF
	
	STRING strReason = ""
		
	//show fail message
	SWITCH fail_condition
		CASE mff_debug_fail
			strReason = ""
		BREAK	
		case mff_rocco_alerted
			strReason = "SOL1_FAILALRM"
		break
		case mff_rocco_died
			strReason = "SOL1_FAILROC"
		break
		case mff_actor_died
			strReason = "SOL1_FAILACT"
		break
		case mff_director_died
			strReason = "SOL1_FAILDD"
		break
		case mff_heli_dead
			strReason = "SOL1_FAILHE"
		break
		case Mff_scared_peds
			strReason = "SOL1_FAILSCRD"
		break
		case MFF_sol_died
			strReason = "SOL1_SOLDIED"
		break
		case mff_left_Area
			strReason = "SOl1_FAILLF"
		break
		case mff_left_peds
			strReason = "SOL1_FAILABND"
		break
		DEFAULT
			strReason = ""
		BREAk
	ENDSWITCH
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)
	
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS ("SOL1_TEXT")
	
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
		
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	if IsEntityAlive(player_ped_id())
		IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<913.46051, -1691.32263, 42.20993>>, <<918.42084, -1691.80945, 47.00544>>, 5.0)
			SET_ENTITY_COORDS(player_ped_id(),<<919.98071, -1693.58923, 42.58901>>)
		ENDIF		
		IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<906.63867, -1699.76306, 44.40080>>, <<907.28394, -1691.60486, 42.11665>>, 3.0)
			SET_ENTITY_COORDS(player_ped_id(),<<906.32739, -1701.81006, 41.97026>>)
		ENDIF		
		IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<905.60992, -1687.81653, 46.35753>>, <<901.36200, -1687.39539, 49.84843>>, 5.0)
			SET_ENTITY_COORDS(player_ped_id(),<<896.90826, -1687.11780, 46.13421>>)
		ENDIF		
	endif

	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
	TERMINATE_THIS_THREAD()
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

proc mission_stage_management()
	switch stageswitch
		case stageswitch_requested
			println("[stagemanagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedstage)
			stageswitch 		= stageswitch_exiting
			mission_substage 	= stage_exit
		break
		case stageswitch_exiting
			println("[stagemanagement] exiting mission_stage: ", mission_stage)
			stageswitch 		= stageswitch_entering
			mission_substage 	= stage_entry
			mission_stage 		= requestedstage
		break
		case stageswitch_entering
			println("[stagemanagement] entered mission_stage: ", mission_stage)
			iDialogue_Stage		= 0
			requestedstage  	= -1
			stageswitch 		= stageswitch_idle
		break
		case stageswitch_idle
			if (get_game_timer() - istagetimer) > 2500
			println("[stagemanagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage, " iDialogue_Stage: ",iDialogue_Stage)
				istagetimer = get_game_timer()
			endif
		break
	endswitch
endproc
func bool mission_set_stage(msf_mission_stage_flags newstage)
	if stageswitch = stageswitch_idle
		requestedstage = enum_to_int(newstage)
		stageswitch = stageswitch_requested
		return true
	else
		return false
	endif
endfunc

PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH eStage
		CASE msf_0_get_to_warehouse		vCoord = <<-1034.74194, -497.20056, 35.37177>>		fHeading = 90		BREAK
		CASE msf_1_Stealth_to_roof		vCoord = <<968.7196, -1751.8749, 30.1595>>			fHeading = 38.2203 	BREAK
		CASE msf_2_fight_rocco			vCoord = <<910.1216, -1685.2484, 50.1341>>			fHeading = 185.5396	BREAK
		CASE msf_3_get_to_sol			vCoord = <<909.5594, -1685.8384, 50.1341>>			fHeading = 357.7214	BREAK	
		CASE msf_4_Passed				vCoord = <<-1034.74194, -497.20056, 35.37177>>		fHeading = 90		BREAK	
	ENDSWITCH

ENDPROC
PROC SU_GENERAL()
	RESTORE_PLAYER_PED_VARIATIONS(Player_ped_id())
	SET_PED_CONFIG_FLAG(Player_ped_id(),PCF_WillFlyThroughWindscreen,false)
	clear_area(GET_ENTITY_COORDS(player_ped_id()),500,true)
	SET_CURRENT_PED_WEAPON(player_ped_id(),WEAPONTYPE_UNARMED,true)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
endproc
PROC SU_msf_0_get_to_warehouse()
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	SET_WANTED_LEVEL_MULTIPLIER(0.3)
endproc
proc SU_msf_1_Stealth_to_roof()

	vehs[mvf_heli].id 		= CREATE_VEHICLE(FROGGER,vheliRoof, hHeliRoof)
	SET_FORCE_HD_VEHICLE(vehs[mvf_heli].id, true)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id,0)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_heli].id,false)
	
	peds[mpf_milton].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelActor,VS_BACK_LEFT)
	SETUP_Milton()
	peds[mpf_Anton].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelDirector,VS_BACK_RIGHT)
	SETUP_Anton()
	peds[mpf_Gianni].id = CREATE_PED(pedtype_mission,modelGianni,vGianni,hGianni)
	SETUP_Gianni()	 
	
	REQUEST_CLIP_SET("MOVE_M@CLIPBOARD")
	while not HAS_CLIP_SET_LOADED("MOVE_M@CLIPBOARD")
		wait(0)
	endwhile
	
	init_enemy_data()
	create_Mafiaenemy(maf_car,11)					
	create_Mafiaenemy(maf_car_talking,11)
	create_Mafiaenemy(maf_worker_vent,14)
	create_Mafiaenemy(maf_worker_alone,24)
	
	while not Check_Replay_Car(<<971.4699, -1754.8250, 30.0953>>, 87.1515)
		wait(0)
	endwhile
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	bStartConstructionChecks = true
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	TRIGGER_MUSIC_EVENT("SOL1_STEALTH_RT")
endproc
proc SU_msf_2_fight_rocco()	

	vehs[mvf_heli].id = CREATE_VEHICLE(FROGGER,vheliRoof, hHeliRoof)
	SET_FORCE_HD_VEHICLE(vehs[mvf_heli].id, true)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id,0)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_heli].id,false)
	
	peds[mpf_milton].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelActor,VS_BACK_LEFT)
	SETUP_Milton()
	peds[mpf_Anton].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelDirector,VS_BACK_RIGHT)
	SETUP_Anton()
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	bStartConstructionChecks = true
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	TRIGGER_MUSIC_EVENT("SOL1_FIGHT_RT")
endproc
proc SU_msf_3_get_to_sol()

	vehs[mvf_heli].id = CREATE_VEHICLE(FROGGER,vheliRoof, hHeliRoof)
	SET_FORCE_HD_VEHICLE(vehs[mvf_heli].id, true)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id,0)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_heli].id,false)
	
	peds[mpf_milton].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelActor,VS_BACK_LEFT)
	SETUP_Milton()
	peds[mpf_Anton].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelDirector,VS_BACK_RIGHT)
	SETUP_Anton()
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	bStartConstructionChecks = true
	SET_WANTED_LEVEL_MULTIPLIER(0.3)
	TRIGGER_MUSIC_EVENT("SOL1_GET_SOL_RT")
endproc
proc SU_PASSED()
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
endproc
proc mission_stage_SKIP()
//a skip has been made
	if bdoskip = true
		
		//begin the skip if the switching is idle
		if stageswitch = stageswitch_idle
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(1000)
				endif
			else
				mission_set_stage(int_to_enum(msf_mission_stage_flags, iskiptostage))
			endif
		//needs to be carried out before states own entering stage
		elif stageswitch = stageswitch_entering
			
			render_script_cams(false,false)
			set_player_control(player_id(), true)
			
			reset_everything()
			Start_Skip_Streaming(sAssetData)
			
			// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading)				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 25.0)
			ENDIF
			// --------------------------------------------------------------------
						
			Load_asset_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))			
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE					
			
			peds[mpf_mike].id = player_ped_id()			
			switch int_to_enum(msf_mission_stage_flags, mission_stage)			
				case msf_0_get_to_warehouse	su_msf_0_get_to_warehouse()	break				
				case msf_1_Stealth_to_roof	su_msf_1_Stealth_to_roof()	break	
				case msf_2_fight_rocco		su_msf_2_fight_rocco() 		break
				case msf_3_get_to_sol		su_msf_3_get_to_sol()		break
				case msf_4_passed			SU_PASSED()					break
			endswitch				
			
			SU_GENERAL()	
			bdoskip = false
			IF NOT IS_REPLAY_BEING_SET_UP()	
				NEW_LOAD_SCENE_STOP()
			endif
			
		endif
#if is_debug_build
	//check is a skip being asked for, dont allow skip during setup stage
	elif launch_mission_stage_menu(zmenunames, iskiptostage, mission_stage, true)
		if iskiptostage > enum_to_int(msf_num_of_stages)-1
			mission_passed()
		else
			iskiptostage = clamp_int(iskiptostage, 0, enum_to_int(msf_num_of_stages)-1)
			if is_screen_faded_in()
				do_screen_fade_out(1000)
				bdoskip = true
			endif
		endif
#endif
	endif
endproc

proc death_checks()
//========================== death checks ==================================

	for i = 0 to enum_to_int(mpf_num_of_peds) -1
		if does_entity_exist(peds[i].id)
			if is_ped_injured(peds[i].id)
				if peds[i].id = player_ped_id()
					MISSION_FAILED(mff_player_died)
				endif
				if peds[i].id = peds[mpf_milton].id				
					MISSION_FAILED(mff_actor_died)
				endif
				if peds[i].id = peds[mpf_Anton].id
					MISSION_FAILED(mff_director_died)
				endif
				if peds[i].id = peds[mpf_sol].id
					MISSION_FAILED(mff_sol_died)
				endif
				if peds[i].id = peds[mpf_rocco].id
					if DOES_BLIP_EXIST(peds[mpf_rocco].blip)
						REMOVE_BLIP(peds[mpf_rocco].blip)
					endif
					MISSION_FAILED(mff_rocco_died)
				endif
				if peds[i].id = peds[mpf_Gianni].id 
					if isentityalive(Anton())
					and isentityalive(milton())
						if GET_RANDOM_BOOL()
							ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
							CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE",CONV_PRIORITY_MEDIUM)							
							if not IS_PED_FLEEING(Anton())
								TASK_SMART_FLEE_PED(Anton(),mike(),200,-1)
							endif
							if not IS_PED_FLEEING(Milton())
								TASK_SMART_FLEE_PED(Milton(),mike(),200,-1)
							endif
							MISSION_FAILED(Mff_scared_peds)
						else
							ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1Actor")
							CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE2",CONV_PRIORITY_MEDIUM)							
							if not IS_PED_FLEEING(Anton())
								TASK_SMART_FLEE_PED(Anton(),mike(),200,-1)
							endif
							if not IS_PED_FLEEING(Milton())
								TASK_SMART_FLEE_PED(Milton(),mike(),200,-1)
							endif
							MISSION_FAILED(Mff_scared_peds)
						endif
					endif
				endif
			endif
		else
			if peds[i].id = peds[mpf_rocco].id
				if DOES_BLIP_EXIST(peds[mpf_rocco].blip)
					REMOVE_BLIP(peds[mpf_rocco].blip)
				endif
			endif
		endif
	endfor
	for i = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1		
	
		update_ai_ped_blip(Mafia[i].id,Mafia[i].blipstruct,-1,null,Mafia[i].bForceBlip)		
		
		if does_entity_exist(Mafia[i].id)
			if is_ped_injured(Mafia[i].id)					
				if WAS_PED_KILLED_BY_STEALTH(Mafia[i].id)				
				or (Mafia[i].sit = SSIT_AI_NONE
				and HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Mafia[i].id,mike()))
					INFORM_MISSION_STATS_OF_INCREMENT(SOL1_SILENT_TAKEDOWNS)
				endif
				if DOES_ENTITY_EXIST(mafia[i].obj)
					if IS_ENTITY_ATTACHED(mafia[i].obj)
						DETACH_ENTITY(mafia[i].obj)
					endif
					SAFE_RELEASE_OBJECT(mafia[i].obj)
				endif
				if DOES_ENTITY_EXIST(mafia[i].objphone)
					if IS_ENTITY_ATTACHED(mafia[i].objphone)
						DETACH_ENTITY(mafia[i].objphone)
					endif
					SAFE_RELEASE_OBJECT(mafia[i].objphone)
				endif
				Mafia[i].bkilled = true	
				Mafia[i].bForceBlip = false
				SAFE_RELEASE_PED(Mafia[i].id)	
			endif
		endif
	endfor
	for i = 0 to enum_to_int(mvf_num_of_veh) -1
		if does_entity_exist(vehs[i].id)			
			if not is_vehicle_driveable(vehs[i].id)	
				if vehs[i].id = vehs[mvf_heli].id
					if mission_stage = enum_to_int(msf_3_get_to_sol)
					 	if mission_substage < 4
							MISSION_FAILED(mff_heli_dead)
						endif
					else
						MISSION_FAILED(mff_heli_dead)
					endif
					SAFE_RELEASE_VEHICLE(vehs[i].id)
				endif
			else
				if vehs[i].id = vehs[mvf_heli].id
					if IS_VEHICLE_PERMANENTLY_STUCK(vehs[i].id)
						if mission_stage = enum_to_int(msf_3_get_to_sol)
						 	if mission_substage < 4
								MISSION_FAILED(mff_heli_dead)
							endif
						else
							MISSION_FAILED(mff_heli_dead)
						endif
						SAFE_RELEASE_VEHICLE(vehs[i].id)
					endif
				endif
			endif
		endif
	endfor
	
//==========================================================================	
endproc
FUNC BOOL TRIGGER_WALKWAY_PEDS()
	IF ISENTITYALIVE(MIKE())
		IF IS_ENTITY_IN_ANGLED_AREA(MIKE(),<<933.48407, -1706.96594, 40.41132>>,<<937.31335, -1663.64905, 48>>,35)
			create_MafiaEnemy(maf_walkway_door,24)
			create_MafiaEnemy(maf_worker_walkway,24)
			
			OPEN_SEQUENCE_TASK(seq)
//				TASK_GO_STRAIGHT_TO_COORD(null,<<938.29431, -1694.26501, 41.06182>>,PEDMOVE_WALK)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<943.27026, -1680.55713, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(mafia[maf_walkway_door].id,seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_CONFIG_FLAG(mafia[maf_walkway_door].id,PCF_OpenDoorArmIK,true)
			SET_PED_RESET_FLAG(mafia[maf_walkway_door].id,PRF_SearchForClosestDoor,true)
			Mafia[maf_walkway_door].vtarget = <<943.27026, -1680.55713, 41.06182>>
			
			OPEN_SEQUENCE_TASK(seq)
//				TASK_GO_STRAIGHT_TO_COORD(null,<<939.29596, -1694.40991, 41.06182>>,PEDMOVE_WALK)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<945.01746, -1679.36267, 41.06182>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(mafia[maf_worker_walkway].id,seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_CONFIG_FLAG(mafia[maf_worker_walkway].id,PCF_OpenDoorArmIK,true)
			SET_PED_RESET_FLAG(mafia[maf_worker_walkway].id,PRF_SearchForClosestDoor,true)
			Mafia[maf_worker_walkway].vtarget = <<945.01746, -1679.36267, 41.06182>>
			bDoWalkwayChat = true
			RETURN TRUE
		ENDIF	
	ENDIF
RETURN FALSE
ENDFUNC
FUNC BOOL TRIGGER_SECOND_DOOR_PED()	
	IF ISENTITYALIVE(MIKE())
		IF IS_ENTITY_IN_ANGLED_AREA(MIKE(),<<901.04163, -1721.98486, 39.74014>>,<<902.49176, -1699.63672, 46.05842>>,22.16)
			create_MafiaEnemy(maf_worker_door,24)
			OPEN_SEQUENCE_TASK(seq)
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)	
				TASK_GO_STRAIGHT_TO_COORD(null,<<906.23120, -1703.19482, 41.97026>>,PEDMOVE_SPRINT)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<910.14313, -1708.57007, 41.97026>>,PEDMOVE_RUN,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-163.74)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(mafia[maf_worker_door].id,seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_CONFIG_FLAG(mafia[maf_worker_door].id,PCF_OpenDoorArmIK,true)
			SET_PED_RESET_FLAG(mafia[maf_worker_door].id,PRF_SearchForClosestDoor,true)
			Mafia[maf_worker_door].vtarget = <<910.14313, -1708.57007, 41.97026>>			
			RETURN TRUE
		ENDIF	
	ENDIF
RETURN FALSE
ENDFUNC
FUNC BOOL TRIGGER_THIRD_DOOR_PED()	
	IF ISENTITYALIVE(MIKE())
		IF IS_ENTITY_IN_ANGLED_AREA(MIKE(),<<880.97339, -1682.67957, 46.08658>>,<<905.58136, -1684.29175, 53.31669>>,30)
			create_MafiaEnemy(maf_worker_top,18)
			OPEN_SEQUENCE_TASK(seq)
				TASK_PLAY_ANIM(null,AnimDict_ClipBoard,"idle_c",NORMAL_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)	
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<884.57086, -1688.17114, 46.13421>>,PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,127.67)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(mafia[maf_worker_top].id,seq)
			CLEAR_SEQUENCE_TASK(seq)	
			SET_PED_CONFIG_FLAG(mafia[maf_worker_top].id,PCF_OpenDoorArmIK,true)
			SET_PED_RESET_FLAG(mafia[maf_worker_top].id,PRF_SearchForClosestDoor,true)
			Mafia[maf_worker_top].vtarget = <<884.57086, -1688.17114, 46.13421>>
			RETURN TRUE
		ENDIF	
	ENDIF
RETURN FALSE
ENDFUNC
FUNC BOOL TRIGGER_ALONE_WORKER_TASKS()	
	IF ISENTITYALIVE(MIKE())
		IF IS_ENTITY_IN_ANGLED_AREA(MIKE(),<<912.46344, -1714.39941, 41.46995>>,<<950.94952, -1717.65479, 36.37867>>,30)						
			bAloneWorkerActive = true
			RETURN TRUE
		ENDIF	
	ENDIF
RETURN FALSE
ENDFUNC
func string FLINCH_REACTION()
	switch iflinch
		case 0
			iflinch = 1
			return "reaction_forward_big_intro_a"
		break
		case 1		
			iflinch = 0
			return "reaction_forward_big_intro_c"
		break
	endswitch	
return ""	
endfunc
func string SEEN_REACTION()
	switch iseen
		case 0
			iseen = 1
			return "reaction_forward_big_intro_b"
		break
		case 1		
			iseen = 0
			return "reaction_forward_big_intro_d"
		break
	endswitch	
return ""	
endfunc
func STEALTH_SITUATION get_ped_Sit(PED_INDEX ped)
	int mafped
	for mafped = 0 to ENUM_TO_INT(NUM_OF_Mafia_PEDS) - 1
		if mafia[mafped].id = ped
			return mafia[mafped].sit
		endif
	endfor
	return SSIT_AI_NONE
endfunc
func string get_ped_Voice_string(PED_INDEX ped)
	int mafped
	for mafped = 0 to ENUM_TO_INT(NUM_OF_Mafia_PEDS) - 1
		if mafia[mafped].id = ped
			return mafia[mafped].sVoiceName
		endif
	endfor
	return ""
endfunc
proc set_ped_situations(PED_INDEX ped, STEALTH_SITUATION sit)	
	int mafped
	for mafped = 0 to ENUM_TO_INT(NUM_OF_Mafia_PEDS) - 1
		if mafia[mafped].id = ped
			mafia[mafped].sit = sit
		endif
	endfor
endproc
PROC MANAGE_STEALTH_STAGE()
int mafped
PED_INDEX clseped
GET_CURRENT_PED_WEAPON(mike(),wcurrent)
//╞════════════════════════════════════════════════════╡ TRIGGER PEDS ╞════════════════════════════════════════════════════╡	
	if mission_stage = enum_to_int(msf_1_Stealth_to_roof)
		if not isentityalive(mafia[maf_walkway_door].id)
		and not mafia[maf_walkway_door].bkilled
		and not isentityalive(mafia[maf_worker_walkway].id)
		and not mafia[maf_worker_walkway].bkilled
			trigger_walkway_peds()						
		endif	
		if not isentityalive(mafia[maf_worker_door].id)
		and not mafia[maf_worker_door].bkilled
			trigger_second_door_ped()
		endif		
		if not isentityalive(mafia[maf_worker_top].id)
		and not mafia[maf_worker_top].bkilled	
			trigger_third_door_ped()
		endif
		if not bAloneWorkerActive
			TRIGGER_ALONE_WORKER_TASKS()
		endif
	endif
//╞═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╡	
	switch eAI_stage
		case AI_NORM
//╞════════════════════════════════════════════════════╡ Normal tasks ╞════════════════════════════════════════════════════╡		
			eAI_stage = AI_CHECKS
		break
		case AI_CHECKS		
//╞════════════════════════════════════════════════════╡ Check if seen/heard ╞════════════════════════════════════════════════════╡
			if IS_AUDIO_SCENE_ACTIVE("SOL_1_GET_TO_HELI_ALERT")
				STOP_AUDIO_SCENE("SOL_1_GET_TO_HELI_ALERT")
			endif			
			for mafped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1
				if isentityalive(mafia[mafped].id)				
					//============================= combat check =============================					
					if IS_PED_IN_COMBAT(mafia[mafped].id)
					or mafia[mafped].sit = SSIT_AI_HEAT
						active_ped = mafia[mafped].id
						mafia[mafped].sit = SSIT_AI_HEAT
						if DOES_Entity_EXIST(mafia[mafped].partner)
							set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
						endif
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						eAI_stage 	= AI_REACTION	
//						PRINTSTRING("eAI_stage UPDATE due to - combat check is TRUE") PRINTNL()
					//============================= hurt ped =============================
					elif HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mafia[mafped].id,mike())
					or IS_PED_BEING_STEALTH_KILLED(mafia[mafped].id)
					or IS_PED_BEING_STUNNED(mafia[mafped].id)
					or mafia[mafped].sit = SSIT_AI_HURT									
						if mafia[mafped].id = mafia[maf_car].id
						or mafia[mafped].id = mafia[maf_car_talking].id
						or mafia[mafped].id = mafia[maf_walkway_door].id							
						or mafia[mafped].id = mafia[maf_worker_walkway].id
							if DOES_ENTITY_EXIST(mafia[mafped].partner)
								if isentityalive(mafia[mafped].partner)
									active_ped = mafia[mafped].partner
								else
									active_ped = mafia[mafped].id
								endif								
							endif
						else						
							active_ped = mafia[mafped].id
						endif												
						mafia[mafped].sit	= SSIT_AI_HURT
						if DOES_Entity_EXIST(mafia[mafped].partner)
							set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
						endif
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mafia[mafped].id)			
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						eAI_stage = AI_REACTION		
//						PRINTSTRING("eAI_stage UPDATE due to - Hurt ped check is TRUE") PRINTNL()
					//============================= gun checks ============================= 					
					elif HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_WHIZZED_BY) 
					OR 	 HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
					or 	 mafia[mafped].sit	= SSIT_AI_GUN
						
						active_ped = mafia[mafped].id
						mafia[mafped].sit	= SSIT_AI_GUN
						if DOES_Entity_EXIST(mafia[mafped].partner)
							set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
						endif
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						eAI_stage = AI_REACTION	
//						PRINTSTRING("eAI_stage UPDATE due to - Gun check 1 is TRUE") PRINTNL()
					elif HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED)					
						if not IS_PED_CURRENT_WEAPON_SILENCED(mike())							
							GET_CLOSEST_PED(GET_ENTITY_COORDS(mike()),500,false,true,clseped,false,true) 
							if clseped = mafia[mafped].id	
								active_ped = mafia[mafped].id								
							endif
							mafia[mafped].sit	= SSIT_AI_GUN
							if DOES_Entity_EXIST(mafia[mafped].partner)
								set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
							endif
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							eAI_stage = AI_REACTION
//							PRINTSTRING("eAI_stage UPDATE due to - Gun check 2 is TRUE") PRINTNL()
						endif
					//============================= hear explosion ============================= 
					elif HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_EXPLOSION_HEARD)	
					or IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<919.02515, -1736.73352, 28>>,<<925.47205, -1649.41968, 65>>,120)
					or mafia[mafped].sit = SSIT_AI_HEAT					
						if GET_CLOSEST_PED(GET_ENTITY_COORDS(mike()),80,false,true,mafia[mafped].id,false,true)						
							active_ped = mafia[mafped].id				
						endif						
						mafia[mafped].sit	= SSIT_AI_HEAT		
						if DOES_Entity_EXIST(mafia[mafped].partner)
							set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
						endif
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						eAI_stage = AI_REACTION		
//						PRINTSTRING("eAI_stage UPDATE due to - hear explosion check is TRUE") PRINTNL()
					//============================= seen mike ============================= 
					elif CAN_PED_SEE_HATED_PED(mafia[mafped].id,player_ped_id())					
					or mafia[mafped].sit = SSIT_AI_SPOTTED	
					and not IS_ENTITY_IN_ANGLED_AREA(mafia[mafped].id,<<907.27173, -1692.09302, 41>>,<<906.43494, -1701.65735, 45>>,1.6) 
						vMafiaPedCoords = GET_ENTITY_COORDS(mafia[mafped].id) //BUG FIX 	1399344
						vPlayerCoords = GET_ENTITY_COORDS(player_ped_id()) //BUG FIX 	1399344
						IF vPlayerCoords.z > (vMafiaPedCoords.z - 5)  //BUG FIX 	1399344  check to make sure the players is not too low down. If lower than 5m below the ped that can see them, don't do anything.
							active_ped 			= mafia[mafped].id						
							mafia[mafped].sit	= SSIT_AI_SPOTTED	
							if DOES_Entity_EXIST(mafia[mafped].partner)
								set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
							endif		
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							eAI_stage 	= AI_REACTION	
//							PRINTSTRING("eAI_stage UPDATE due to - seen Mike check is TRUE") PRINTNL()
						ENDIF
					//============================= hear something ============================= 
					elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mafia[mafped].id)) < mafia[mafped].fHearingRange
					and GET_PLAYER_CURRENT_STEALTH_NOISE(player_id()) > 9.0 //BUG FIX 	1399344
					and CAN_PED_HEAR_PLAYER(PLAYER_ID(), mafia[mafped].id) //BUG FIX 	1399344
					and not IS_ENTITY_IN_ANGLED_AREA(mafia[mafped].id,<<901.35358, -1687.50378, 49.18065>>,<<905.75061, -1687.85010, 46.37356>>,3.11) 	
					and not IS_ENTITY_IN_ANGLED_AREA(mafia[mafped].id,<<918.04565, -1691.64111, 42.15205>>,<<913.72107, -1691.21619, 44.52736>>,3) 
					and not IS_ENTITY_IN_ANGLED_AREA(mafia[mafped].id,<<907.27173, -1692.09302, 41>>,<<906.43494, -1701.65735, 45>>,1.6) 
					and NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) //BUG FIX 	1399344
						
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(mafia[mafped].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								TASK_TURN_PED_TO_FACE_COORD(mafia[mafped].id,GET_ENTITY_COORDS(mike()),4000)
								TASK_LOOK_AT_COORD(mafia[mafped].id,GET_ENTITY_COORDS(mike()),4000)
								Mafia[mafped].bHeardNoise = true
								ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,mafia[mafped].id,mafia[mafped].sVoiceName)											
								if  ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
									if mafia[mafped].id = mafia[maf_car_talking].id
									
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR1",CONV_PRIORITY_MEDIUM) 	
									else
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR1b",CONV_PRIORITY_MEDIUM) 	
									endif
								elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR2",CONV_PRIORITY_MEDIUM) 											
								elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR3",CONV_PRIORITY_MEDIUM) 											
								endif
								i_dialogueTimer = get_Game_timer()
							else
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							endif
						endif
						PRINTSTRING("eAI_stage UPDATE due to - hear something check 1 is TRUE") PRINTNL()
						
					elif IS_FLASH_LIGHT_ON(PLAYER_PED_ID())
					and( GET_CLOCK_HOURS() >= 19 or GET_CLOCK_HOURS() < 6)
					and GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mafia[mafped].id) < 25
					and( 	IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),mafia[mafped].id) 
					and not IS_ENTITY_IN_ANGLED_AREA(mafia[mafped].id,<<907.27173, -1692.09302, 41>>,<<906.43494, -1701.65735, 45>>,1.6) 
						 or IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),mafia[mafped].id))
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(mafia[mafped].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								TASK_TURN_PED_TO_FACE_COORD(mafia[mafped].id,GET_ENTITY_COORDS(mike()),4000)
								TASK_LOOK_AT_COORD(mafia[mafped].id,GET_ENTITY_COORDS(mike()),4000)
								Mafia[mafped].bHeardNoise = true
								ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,mafia[mafped].id,mafia[mafped].sVoiceName)											
								if  ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
									if mafia[mafped].id = mafia[maf_car_talking].id
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR1",CONV_PRIORITY_MEDIUM) 	
									else
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR1b",CONV_PRIORITY_MEDIUM) 	
									endif
								elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR2",CONV_PRIORITY_MEDIUM) 											
								elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR3",CONV_PRIORITY_MEDIUM) 											
								endif
								i_dialogueTimer = get_Game_timer()
							else
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							endif	
						endif
//						PRINTSTRING("eAI_stage UPDATE due to - hear something check 2 is TRUE") PRINTNL()
					elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mafia[mafped].id)) < mafia[mafped].fHearingRange 
					and IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					and not IS_ENTITY_IN_ANGLED_AREA(mafia[mafped].id,<<907.27173, -1692.09302, 41>>,<<906.43494, -1701.65735, 45>>,1.6) 
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(mafia[mafped].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								TASK_TURN_PED_TO_FACE_COORD(mafia[mafped].id,GET_ENTITY_COORDS(mike()),4000)
								TASK_LOOK_AT_COORD(mafia[mafped].id,GET_ENTITY_COORDS(mike()),4000)
								Mafia[mafped].bHeardNoise = true
								ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,mafia[mafped].id,mafia[mafped].sVoiceName)											
								if  ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
									if mafia[mafped].id = mafia[maf_car_talking].id
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR1",CONV_PRIORITY_MEDIUM) 	
									else
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR1b",CONV_PRIORITY_MEDIUM) 	
									endif
								elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR2",CONV_PRIORITY_MEDIUM) 											
								elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR3",CONV_PRIORITY_MEDIUM) 											
								endif
								i_dialogueTimer = get_Game_timer()
							else
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							endif
						endif
//						PRINTSTRING("eAI_stage UPDATE due to - hear something check 3 is TRUE") PRINTNL()
					//============================= standing IDLE =============================	
					else						
//						PRINTSTRING("eAI_stage UPDATE - standing IDLE") PRINTNL()
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(mafia[mafped].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
						and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(mafia[mafped].id,SCRIPT_TASK_PERFORM_SEQUENCE) 
						and mafia[mafped].sit = SSIT_AI_NONE						
							if mafia[mafped].id =  mafia[maf_car].id
							or mafia[mafped].id =  mafia[maf_car_talking].id
							or mafia[mafped].id =  mafia[maf_walkway_door].id
							or mafia[mafped].id =  mafia[maf_worker_walkway].id
								if isentityalive(mafia[mafped].partner)
									Task_perform_Normal_seq(int_to_enum(eEnemies_Mafia, mafped))
									if Mafia[mafped].bHeardNoise
										ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,mafia[mafped].id,mafia[mafped].sVoiceName)											
										if  ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_NOT1",CONV_PRIORITY_MEDIUM) 											
										elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR2B",CONV_PRIORITY_MEDIUM) 											
										elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_NOT3",CONV_PRIORITY_MEDIUM) 											
										endif
										i_dialogueTimer = get_Game_timer()
										Mafia[mafped].bHeardNoise = false
									endif
								endif
							else
								Task_perform_Normal_seq(int_to_enum(eEnemies_Mafia, mafped))
								if Mafia[mafped].bHeardNoise
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,mafia[mafped].id,mafia[mafped].sVoiceName)											
									if  ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_NOT1",CONV_PRIORITY_MEDIUM) 										
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEAR2B",CONV_PRIORITY_MEDIUM) 											
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_NOT3",CONV_PRIORITY_MEDIUM) 											
									endif
									i_dialogueTimer = get_Game_timer()
									Mafia[mafped].bHeardNoise = false
								endif
							endif							
						endif
					endif	
				elif not isentityalive(mafia[mafped].id)
				and mafia[mafped].bkilled
					//============================= partner dead check =============================
					if 	mafia[mafped].id = mafia[maf_car].id
					or  mafia[mafped].id = mafia[maf_car_talking].id
					or  mafia[mafped].id = mafia[maf_walkway_door].id
					or  mafia[mafped].id = mafia[maf_worker_walkway].id					
						if DOES_ENTITY_EXIST(mafia[mafped].partner)
							if not IS_PED_INJURED(mafia[mafped].partner)
								active_ped = mafia[mafped].partner						
								mafia[mafped].sit	= SSIT_AI_HURT			
								set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								eAI_stage = AI_REACTION
//								PRINTSTRING("eAI_stage UPDATE due to - partner dead check is TRUE") PRINTNL()
							endif
						endif
					endif			
				endif
			endfor			
		break
//╞════════════════════════════════════════════════════╡ Start reactions ╞════════════════════════════════════════════════════╡
		case AI_REACTION		
			if not bMusic_1stEnemy
				TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
				bMusic_1stEnemy = true
			endif			
			if not IS_AUDIO_SCENE_ACTIVE("SOL_1_GET_TO_HELI_ALERT")
				START_AUDIO_SCENE("SOL_1_GET_TO_HELI_ALERT") 
			endif
			REPLAY_RECORD_BACK_FOR_TIME(3.0)
			for mafped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1				
				if isentityalive(mafia[mafped].id)
					//drop clipboard
					if DOES_ENTITY_EXIST(mafia[mafped].obj)
					and IS_ENTITY_ATTACHED(mafia[mafped].obj)
						DETACH_ENTITY(mafia[mafped].obj)
						RESET_PED_WEAPON_MOVEMENT_CLIPSET(mafia[mafped].id)
						SAFE_RELEASE_OBJECT(mafia[mafped].obj)
					endif
					
					if IS_AMBIENT_SPEECH_PLAYING(mafia[mafped].id)
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(mafia[mafped].id)
					endif	
					if isentityalive(mafia[mafped].partner)
						if IS_AMBIENT_SPEECH_PLAYING(mafia[mafped].partner)
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(mafia[mafped].partner)
						endif
					endif
				//=================================== Dead / hurt / gun react ===========================================
					if mafia[mafped].sit = SSIT_AI_HURT
					or mafia[mafped].sit = SSIT_AI_GUN	
						if not IS_PED_FLEEING(mafia[mafped].id)
							CLEAR_PED_TASKS(mafia[mafped].id)
							CLEAR_SEQUENCE_TASK(seq)
							OPEN_SEQUENCE_TASK(seq)
								if mafia[mafped].id = Active_ped		
									TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
									TASK_PLAY_ANIM(null,AnimDict_flinch,FLINCH_REACTION(),NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)	
									TASK_PED_SLIDE_TO_COORD_HDG_RATE(null,GET_ENTITY_COORDS(mafia[mafped].id),
																	 GET_HEADING_FROM_ENTITIES_LA(mafia[mafped].id,mike()),0.5,250)
														
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,active_ped,mafia[mafped].sVoiceName)
									if   ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
										if GET_RANDOM_BOOL()
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PAN1",CONV_PRIORITY_MEDIUM)
										else
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCAR1",CONV_PRIORITY_MEDIUM)
										endif
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
										if GET_RANDOM_BOOL()
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PAN2",CONV_PRIORITY_MEDIUM)
										else
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCAR2",CONV_PRIORITY_MEDIUM)
										endif
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
										if GET_RANDOM_BOOL()
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PAN3",CONV_PRIORITY_MEDIUM)
										else
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCAR3",CONV_PRIORITY_MEDIUM)
										endif
									endif
																		
								else					
									TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)	
									TASK_PED_SLIDE_TO_COORD_HDG_RATE(null,GET_ENTITY_COORDS(mafia[mafped].id),
																	 GET_HEADING_FROM_ENTITIES_LA(mafia[mafped].id,mike()),0.5,240)
									TASK_SMART_FLEE_PED(null,mike(),300,-1)	
								endif
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(mafia[mafped].id,seq)
							CLEAR_SEQUENCE_TASK(seq)
						endif
						eAI_stage = AI_PHONE
				//=================================== Explosion / combat ===========================================	
					elif mafia[mafped].sit = SSIT_AI_HEAT
					Println("AI_REACTION: maf ",mafped +1," : SSIT_AI_HEAT")
						if not IS_PED_FLEEING(mafia[mafped].id)
							CLEAR_PED_TASKS(mafia[mafped].id)
							OPEN_SEQUENCE_TASK(seq)							
								if mafia[mafped].id = Active_ped	
									TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
									TASK_PLAY_ANIM(null,AnimDict_flinch,FLINCH_REACTION(),NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
//									TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),500)	
									TASK_PED_SLIDE_TO_COORD_HDG_RATE(null,GET_ENTITY_COORDS(mafia[mafped].id),
																	 GET_HEADING_FROM_ENTITIES_LA(mafia[mafped].id,mike()),0.5,250)
									
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,active_ped,mafia[mafped].sVoiceName)
									if   ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PAN1",CONV_PRIORITY_MEDIUM)										
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PAN2",CONV_PRIORITY_MEDIUM)										
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
										CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PAN3",CONV_PRIORITY_MEDIUM)										
									endif
								else
									TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
									TASK_PED_SLIDE_TO_COORD_HDG_RATE(null,GET_ENTITY_COORDS(mafia[mafped].id),
																	 GET_HEADING_FROM_ENTITIES_LA(mafia[mafped].id,mike()),0.5,235)
									TASK_SMART_FLEE_PED(null,mike(),300,-1)								
								endif
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(mafia[mafped].id,seq)
							CLEAR_SEQUENCE_TASK(seq)
						endif
						eAI_stage = AI_PHONE
				//=================================== Spotted ===========================================		
					elif mafia[mafped].sit = SSIT_AI_SPOTTED
							// ============= floor =============
						if mafia[mafped].id = mafia[maf_car].id
						or mafia[mafped].id = mafia[maf_car_talking].id
							if IS_ENTITY_IN_ANGLED_AREA(mike(),<<960.49347, -1737.89136, 29>>,<<966.67963, -1653.53516, 46>>,45)
								switch ifloorspot
									case 0
										if isentityalive(mafia[maf_car].id)
										and isentityalive(mafia[maf_car_talking].id)
											
											Println("AI_REACTION: maf ",mafped +1," : SSIT_AI_SPOTTED")
											CLEAR_PED_TASKS(mafia[maf_car].id)
											OPEN_SEQUENCE_TASK(seq)													
												TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
												TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),-1)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(mafia[maf_car].id,seq)										
											CLEAR_SEQUENCE_TASK(seq)
											
											CLEAR_PED_TASKS(mafia[maf_car_talking].id)
											OPEN_SEQUENCE_TASK(seq)													
												TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
												TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),-1)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(mafia[maf_car_talking].id,seq)										
											CLEAR_SEQUENCE_TASK(seq)											
											
											ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car_talking].iVoiceID,mafia[maf_car_talking].id,mafia[maf_car_talking].sVoiceName)
											CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SEE1",CONV_PRIORITY_MEDIUM)									
											ifloortimer = get_Game_timer()
											ifloorspot++
										else
											mafia[mafped].sit = SSIT_AI_HURT
										endif
									break
									case 1
										if isentityalive(mafia[maf_car].id)
										and isentityalive(mafia[maf_car_talking].id)
											if get_game_timer() - ifloortimer > 8000
											and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
												CLEAR_PED_TASKS(mafia[maf_car].id)
												OPEN_SEQUENCE_TASK(seq)													
													TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
													TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),-1)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(mafia[maf_car].id,seq)										
												CLEAR_SEQUENCE_TASK(seq)
												
												CLEAR_PED_TASKS(mafia[maf_car_talking].id)
												OPEN_SEQUENCE_TASK(seq)													
													TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
													TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),-1)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(mafia[maf_car_talking].id,seq)										
												CLEAR_SEQUENCE_TASK(seq)
												
												ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car_talking].iVoiceID,mafia[maf_car_talking].id,mafia[maf_car_talking].sVoiceName)
												CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SEE2",CONV_PRIORITY_MEDIUM)	
												ifloorspot++
												ifloortimer = get_Game_timer()
											endif
										else
											mafia[mafped].sit = SSIT_AI_HURT
										endif
									break
									case 2
										if isentityalive(mafia[maf_car].id)
										and isentityalive(mafia[maf_car_talking].id)
											if get_game_timer() - ifloortimer > 8000
											and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												CLEAR_PED_TASKS(mafia[maf_car].id)
												OPEN_SEQUENCE_TASK(seq)													
													TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
													TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),-1)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(mafia[maf_car].id,seq)										
												CLEAR_SEQUENCE_TASK(seq)
												
												CLEAR_PED_TASKS(mafia[maf_car_talking].id)
												OPEN_SEQUENCE_TASK(seq)													
													TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
													TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),0)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(mafia[maf_car_talking].id,seq)										
												CLEAR_SEQUENCE_TASK(seq)
												
												ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car_talking].iVoiceID,mafia[maf_car_talking].id,mafia[maf_car_talking].sVoiceName)
												CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_HEY",CONV_PRIORITY_MEDIUM)	
												
												eAI_stage = AI_PHONE
											endif
										else
											mafia[mafped].sit = SSIT_AI_HURT
										endif
									break
								endswitch
							else
								if isentityalive(mafia[maf_car].id)								
									CLEAR_PED_TASKS(mafia[maf_car].id)
								endif
								if isentityalive(mafia[maf_car_talking].id)
									CLEAR_PED_TASKS(mafia[maf_car_talking].id)
								endif
								Active_ped = null
								eAI_stage = AI_CHECKS	
								mafia[mafped].sit = SSIT_AI_NONE
							endif
							//       gun checks      
							if isentityalive(mafia[mafped].id)
								if not IS_PED_IN_COMBAT(mafia[mafped].id)
									if HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED)
									or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
									or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_WHIZZED_BY)
									or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_EXPLOSION_HEARD)						
									or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mafia[mafped].id,PLAYER_PED_ID())									
										if not IS_PED_FLEEING(mafia[mafped].id)										
											CLEAR_PED_TASKS(mafia[mafped].id)
											CLEAR_SEQUENCE_TASK(seq)
											OPEN_SEQUENCE_TASK(seq)		
												TASK_PLAY_ANIM(null,AnimDict_flinch,FLINCH_REACTION())
												TASK_SMART_FLEE_PED(null,mike(),300,-1)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(mafia[mafped].id,seq)
											CLEAR_SEQUENCE_TASK(seq)
										endif
										mafia[mafped].sit = SSIT_AI_GUN		
										if isentityalive(mafia[mafped].partner)
											if not IS_PED_FLEEING(mafia[mafped].partner)
												TASK_SMART_FLEE_PED(mafia[mafped].partner,mike(),300,-1)	
											endif	
											set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
										endif
										eAI_stage = AI_PHONE
									elif HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(mafia[mafped].id,player_ped_id())
									and (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),mafia[mafped].id)or IS_PLAYER_TARGETTING_ENTITY(player_id(),mafia[mafped].id))									
									 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
									 	and wcurrent != WEAPONTYPE_UNARMED
											if not IS_PED_FLEEING(mafia[mafped].id)
												CLEAR_PED_TASKS(mafia[mafped].id)
												CLEAR_SEQUENCE_TASK(seq)
												OPEN_SEQUENCE_TASK(seq)		
													TASK_PLAY_ANIM(null,AnimDict_flinch,FLINCH_REACTION())
													TASK_SMART_FLEE_PED(null,mike(),300,-1)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(mafia[mafped].id,seq)
												CLEAR_SEQUENCE_TASK(seq)
											endif
											mafia[mafped].sit = SSIT_AI_GUN		
											if isentityalive(mafia[mafped].partner)
												if not IS_PED_FLEEING(mafia[mafped].partner)
													TASK_SMART_FLEE_PED(mafia[mafped].partner,mike(),300,-1)	
												endif	
												set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
											endif
											eAI_stage = AI_PHONE
										endif										
									endif
								endif						
							endif
							// ============= roof =============
						else
							Println("AI_REACTION: maf ",mafped +1," : SSIT_AI_SPOTTED")
							CLEAR_PED_TASKS(mafia[mafped].id)
							OPEN_SEQUENCE_TASK(seq)
								if mafia[mafped].id = Active_ped		
									TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
									TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),0)								
									
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[mafped].iVoiceID,active_ped,mafia[mafped].sVoiceName)
									if   ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION1")
										iRand = GET_RANDOM_INT_IN_RANGE(0,3)
										if irand = 0
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOL1_SEE1","SOL1_SEE1_1","SOL1_CALL1","SOL1_CALL1_1",CONV_PRIORITY_MEDIUM)
										elif irand = 1
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOL1_SEE1","SOL1_SEE1_2","SOL1_CALL1","SOL1_CALL1_2",CONV_PRIORITY_MEDIUM)
										else
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOL1_SEE1","SOL1_SEE1_3","SOL1_CALL1","SOL1_CALL1_3",CONV_PRIORITY_MEDIUM)
										endif
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION2")
										iRand = GET_RANDOM_INT_IN_RANGE(0,3)
										if irand = 0
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOL1_SEE3","SOL1_SEE3_1","SOL1_CALL2","SOL1_CALL2_3",CONV_PRIORITY_MEDIUM)
										elif irand = 1
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOL1_SEE3","SOL1_SEE3_2","SOL1_CALL2","SOL1_CALL2_2",CONV_PRIORITY_MEDIUM)
										else
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOL1_SEE3","SOL1_SEE3_3","SOL1_CALL2","SOL1_CALL2_1",CONV_PRIORITY_MEDIUM)
										endif
									elif ARE_STRINGS_EQUAL(mafia[mafped].sVoiceName,"CONSTRUCTION3")
										iRand = GET_RANDOM_INT_IN_RANGE(0,3)
										if irand = 0
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOl1_SEE5","SOl1_SEE5_1","SOl1_CHECK2","SOl1_CHECK2_1",CONV_PRIORITY_MEDIUM)
										elif irand = 1
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOl1_SEE5","SOl1_SEE5_2","SOl1_CHECK2","SOl1_CHECK2_2",CONV_PRIORITY_MEDIUM)
										else
											CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"SOL1AUD","SOl1_SEE5","SOl1_SEE5_3","SOl1_CHECK2","SOl1_CHECK2_3",CONV_PRIORITY_MEDIUM)
										endif
									endif
								else
									TASK_LOOK_AT_ENTITY(null,mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
									TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),0)
									TASK_SMART_FLEE_PED(null,mike(),300,-1)								
								endif
							CLOSE_SEQUENCE_TASK(seq)
							if not IS_PED_INJURED(mafia[mafped].id)
								TASK_PERFORM_SEQUENCE(mafia[mafped].id,seq)
							endif
							CLEAR_SEQUENCE_TASK(seq)	
							eAI_stage = AI_PHONE
						endif
					endif
				elif not isentityalive(mafia[mafped].id)
				and mafia[mafped].bkilled
					if not isentityalive(Active_ped)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						Active_ped = null
						eAI_stage = AI_CHECKS
					endif
				endif
			endfor
		break					
		case AI_PHONE
			if isentityalive(active_ped)	
				if not bFirstAlerted
					print_help("SOL1_ALARM")
					bFirstAlerted = true
				endif
				if not IS_SCRIPTED_CONVERSATION_ONGOING()					
					if not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)
						if not IS_PED_GETTING_UP(Active_ped)
						and not IS_PED_RAGDOLL(Active_ped)
						and not IS_ENTITY_PLAYING_ANIM(Active_ped,AnimDict_flinch,"reaction_forward_big_intro_c")
						and not IS_ENTITY_PLAYING_ANIM(Active_ped,AnimDict_flinch,"reaction_forward_big_intro_a")
							println("PHONE STAGE 1")
							CLEAR_SEQUENCE_TASK(seq)
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(null,AnimDict_PHONE,animPhone,SLOW_BLEND_IN,Normal_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
								if get_ped_Sit(active_ped) =  SSIT_AI_GUN 
								or get_ped_Sit(active_ped) =  SSIT_AI_HEAT 
								or get_ped_Sit(active_ped) =  SSIT_AI_HURT
									TASK_SMART_FLEE_PED(null,mike(),300,-1)
								endif
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(active_ped,seq)
							CLEAR_SEQUENCE_TASK(seq)
							ADD_PED_FOR_DIALOGUE(convo_struct,1,active_ped,"MAFIAGOON")
							iRand = GET_RANDOM_INT_IN_RANGE(0,4)
							iPhoneCheckBackupTimer = get_Game_timer()							
						endif
					else
						for mafped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1
							if mafia[mafped].id = active_ped
							println("PHONE STAGE 2 ")
								if mafia[mafped].sit = SSIT_AI_SPOTTED	
									println("PHONE STAGE 3 ")
								 	if get_game_timer() - iPhoneCheckBackupTimer > 1125
										Println("AI_phone: SSIT_AI_SPOTTED")
										eAI_stage = AI_ALARM
										iPhoningChat = 0
									endif
								else
									println("PHONE STAGE 4 ")
									if get_game_timer() - iPhoneCheckBackupTimer > 1125
										Println("AI_phone: SSIT_AI_guns")
										eAI_stage = AI_ALARM
										iPhoningChat = 1	
									endif
								endif
							endif
						endfor
					endif
				endif				
			else
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				Active_ped = null
				eAI_stage = AI_CHECKS
			endif
			for mafped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1				
				if isentityalive(mafia[mafped].id)
					if not IS_PED_IN_COMBAT(mafia[mafped].id)
						if HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED)
						or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_WHIZZED_BY)
						or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_EXPLOSION_HEARD)						
						or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mafia[mafped].id,PLAYER_PED_ID())	
							if not IS_PED_FLEEING(mafia[mafped].id)
							and not IS_ENTITY_PLAYING_ANIM(mafia[mafped].id,AnimDict_flinch,"reaction_forward_big_intro_c")
							and not IS_ENTITY_PLAYING_ANIM(mafia[mafped].id,AnimDict_flinch,"reaction_forward_big_intro_a")
							and mafia[mafped].sit = SSIT_AI_SPOTTED
								if mafia[mafped].id != Active_ped								
									TASK_SMART_FLEE_PED(mafia[mafped].id,mike(),300,-1)									
								else
									CLEAR_PED_TASKS(Active_ped)
									TASK_PLAY_ANIM(active_ped,AnimDict_flinch,FLINCH_REACTION())
								endif
							endif
							mafia[mafped].sit = SSIT_AI_GUN
							if isentityalive(mafia[mafped].partner)
								if not IS_PED_FLEEING(mafia[mafped].partner)
									TASK_SMART_FLEE_PED(mafia[mafped].partner,mike(),300,-1)	
								endif	
								set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
							endif
						elif HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(mafia[mafped].id,player_ped_id())
						and (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),mafia[mafped].id)or IS_PLAYER_TARGETTING_ENTITY(player_id(),mafia[mafped].id))									
						 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
						 	and wcurrent != WEAPONTYPE_UNARMED									
								if not IS_PED_FLEEING(mafia[mafped].id)
								and not IS_ENTITY_PLAYING_ANIM(mafia[mafped].id,AnimDict_flinch,"reaction_forward_big_intro_c")
								and not IS_ENTITY_PLAYING_ANIM(mafia[mafped].id,AnimDict_flinch,"reaction_forward_big_intro_a")
								and mafia[mafped].sit = SSIT_AI_SPOTTED
									if mafia[mafped].id != Active_ped								
										TASK_SMART_FLEE_PED(mafia[mafped].id,mike(),300,-1)									
									else
										CLEAR_PED_TASKS(Active_ped)
										TASK_PLAY_ANIM(active_ped,AnimDict_flinch,FLINCH_REACTION())
									endif
								endif
								mafia[mafped].sit = SSIT_AI_GUN
								if isentityalive(mafia[mafped].partner)
									if not IS_PED_FLEEING(mafia[mafped].partner)
										TASK_SMART_FLEE_PED(mafia[mafped].partner,mike(),300,-1)	
									endif	
									set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
								endif
							endif								
						endif
					endif						
				endif				
			endfor				
		break
		case AI_ALARM	
			if isentityalive(active_ped)
				switch iPhoningChat
					case 0 // spotted
						println("alarm:0: ",irand)
						if IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)
						and not IS_PED_RAGDOLL(Active_ped)
						and not IS_PED_GETTING_UP(Active_ped)
							if get_game_timer() - iPhoneCheckBackupTimer > 2000	
								println("alarm:0:1")
								if not IS_SCRIPTED_CONVERSATION_ONGOING()									
									if  ARE_STRINGS_EQUAL(get_ped_Voice_string(Active_ped),"CONSTRUCTION1")
										ADD_PED_FOR_DIALOGUE(convo_struct,4,active_ped,"CONSTRUCTION1")
										
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PHN",CONV_PRIORITY_MEDIUM) 
											iphonetimer = get_Game_timer()
											iPhoningChat = 2
										endif
									elif ARE_STRINGS_EQUAL(get_ped_Voice_string(Active_ped),"CONSTRUCTION2")
										ADD_PED_FOR_DIALOGUE(convo_struct,3,active_ped,"CONSTRUCTION2")
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_PHN2",CONV_PRIORITY_MEDIUM) 
											iphonetimer = get_Game_timer()
											iPhoningChat = 2
										endif
									elif ARE_STRINGS_EQUAL(get_ped_Voice_string(Active_ped),"CONSTRUCTION3")
										ADD_PED_FOR_DIALOGUE(convo_struct,5,active_ped,"CONSTRUCTION3")
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_CALL3",CONV_PRIORITY_MEDIUM) 
											iphonetimer = get_Game_timer()
											iPhoningChat = 2
										endif
									endif									
								endif	
							endif
						else
							println("alarm:0:2")
							if not IS_PED_RAGDOLL(Active_ped)
							and not IS_PED_GETTING_UP(Active_ped)
								if not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)
									println("alarm:0:3")
									CLEAR_PED_TASKS(active_ped)
									CLEAR_SEQUENCE_TASK(seq)
									OPEN_SEQUENCE_TASK(seq)
										TASK_PLAY_ANIM(null,AnimDict_PHONE,animPhone,SLOW_BLEND_IN,Normal_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
										if get_ped_Sit(active_ped) =  SSIT_AI_GUN 
										or get_ped_Sit(active_ped) =  SSIT_AI_HEAT 
										or get_ped_Sit(active_ped) =  SSIT_AI_HURT
											TASK_SMART_FLEE_PED(null,mike(),300,-1)
										endif
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(active_ped,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
						endif
					break
					case 1 // guns
						println("alarm:1")
						if IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)
						and not IS_PED_RAGDOLL(Active_ped)
						and not IS_PED_GETTING_UP(Active_ped)
							if get_game_timer() - iPhoneCheckBackupTimer > 2000							
								println("alarm:1:1")
								if not IS_SCRIPTED_CONVERSATION_ONGOING()									
									if  ARE_STRINGS_EQUAL(get_ped_Voice_string(Active_ped),"CONSTRUCTION1")
										ADD_PED_FOR_DIALOGUE(convo_struct,4,active_ped,"CONSTRUCTION1")
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CALLS1",CONV_PRIORITY_MEDIUM) 
											iphonetimer = get_Game_timer()
											iPhoningChat = 2
										endif
									elif ARE_STRINGS_EQUAL(get_ped_Voice_string(Active_ped),"CONSTRUCTION2")
										ADD_PED_FOR_DIALOGUE(convo_struct,3,active_ped,"CONSTRUCTION2")
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CALLS2",CONV_PRIORITY_MEDIUM) 
											iphonetimer = get_Game_timer()
											iPhoningChat = 2
										endif
									elif ARE_STRINGS_EQUAL(get_ped_Voice_string(Active_ped),"CONSTRUCTION3")
										ADD_PED_FOR_DIALOGUE(convo_struct,5,active_ped,"CONSTRUCTION3")
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_CALLS3",CONV_PRIORITY_MEDIUM) 
											iphonetimer = get_Game_timer()
											iPhoningChat = 2
										endif
									endif	
								ENDIF
							endif
						else
							println("alarm:1:2")
							if not IS_PED_RAGDOLL(Active_ped)
							and not IS_PED_GETTING_UP(Active_ped)
							and not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_flinch,"reaction_forward_big_intro_c")
							and not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_flinch,"reaction_forward_big_intro_a")
								if not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)
									println("alarm:1:3")
									CLEAR_PED_TASKS(active_ped)
									CLEAR_SEQUENCE_TASK(seq)
									OPEN_SEQUENCE_TASK(seq)
										TASK_PLAY_ANIM(null,AnimDict_PHONE,animPhone,SLOW_BLEND_IN,Normal_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
										if get_ped_Sit(active_ped) =  SSIT_AI_GUN 
										or get_ped_Sit(active_ped) =  SSIT_AI_HEAT 
										or get_ped_Sit(active_ped) =  SSIT_AI_HURT
											TASK_SMART_FLEE_PED(null,mike(),300,-1)
										endif
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(active_ped,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
						endif 
					break					
					case 2		
						println("alarm:2")
						if not IS_SCRIPTED_CONVERSATION_ONGOING()
							iPhoningChat++
						ELSE
							if get_Game_timer() - iphonetimer > 2000
								iPhoningChat = 3
							endif
							if IS_PED_RAGDOLL(active_ped)
							or IS_PED_GETTING_UP(Active_ped)
							or not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)			
								CLEAR_PED_TASKS(active_ped)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								iRand = GET_RANDOM_INT_IN_RANGE(0,4)
								Println("Interrupted send back to start convo again gun")
								eAI_stage = AI_ALARM
								iPhoningChat = 1									
							endif
							if not IS_PED_FLEEING(active_ped)
							and get_ped_Sit(active_ped) = SSIT_AI_SPOTTED
								if HAS_PED_RECEIVED_EVENT(active_ped, EVENT_SHOT_FIRED)
								or HAS_PED_RECEIVED_EVENT(active_ped, EVENT_SHOT_FIRED_BULLET_IMPACT)
								or HAS_PED_RECEIVED_EVENT(active_ped, EVENT_SHOT_FIRED_WHIZZED_BY)
								or HAS_PED_RECEIVED_EVENT(active_ped, EVENT_EXPLOSION_HEARD)		
									CLEAR_PED_TASKS(active_ped)
									TASK_PLAY_ANIM(active_ped,AnimDict_flinch,FLINCH_REACTION())
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									iRand = GET_RANDOM_INT_IN_RANGE(0,4)
									Println("Interrupted send back to start convo again gun")
									eAI_stage = AI_ALARM
									iPhoningChat = 1									
								elif HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(active_ped,player_ped_id())
								and (	IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),active_ped)
									 or IS_PLAYER_TARGETTING_ENTITY(player_id(),active_ped))
									if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
						 			and wcurrent != WEAPONTYPE_UNARMED
										CLEAR_PED_TASKS(active_ped)
										TASK_PLAY_ANIM(active_ped,AnimDict_flinch,FLINCH_REACTION())
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										iRand = GET_RANDOM_INT_IN_RANGE(0,4)
										Println("Interrupted send back to start convo again gun")
										eAI_stage = AI_ALARM
										iPhoningChat = 1	
									endif
								endif
							endif
						endif
					break
					case 3
						println("alarm:3")
						if not IS_ENTITY_PLAYING_ANIM(active_ped,AnimDict_PHONE,animPhone)							
							OPEN_SEQUENCE_TASK(seq)	
								if get_ped_Sit(active_ped) =  SSIT_AI_GUN 
								or get_ped_Sit(active_ped) =  SSIT_AI_HEAT 
								or get_ped_Sit(active_ped) =  SSIT_AI_HURT								
									TASK_SMART_FLEE_PED(null,mike(),300,-1)								
								else
									TASK_WANDER_STANDARD(null)
								endif
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(active_ped,seq)
							CLEAR_SEQUENCE_TASK(Seq)
							MISSION_FAILED(mff_rocco_alerted)
							iPhoningChat++
						else
							CLEAR_PED_SECONDARY_TASK(active_ped)														
						endif
					break
				endswitch				
			else
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				Active_ped = null
				eAI_stage = AI_CHECKS
			endif
			for mafped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1				
				if isentityalive(mafia[mafped].id)
					if not IS_PED_IN_COMBAT(mafia[mafped].id)
						if HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED)
						or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_SHOT_FIRED_WHIZZED_BY)
						or HAS_PED_RECEIVED_EVENT(mafia[mafped].id, EVENT_EXPLOSION_HEARD)						
						or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mafia[mafped].id,PLAYER_PED_ID())								
							if mafia[mafped].id != Active_ped
								TASK_SMART_FLEE_PED(mafia[mafped].id,mike(),300,-1)		
							endif
							mafia[mafped].sit = SSIT_AI_GUN
							if isentityalive(mafia[mafped].partner)
								if not IS_PED_FLEEING(mafia[mafped].partner)
									TASK_SMART_FLEE_PED(mafia[mafped].partner,mike(),300,-1)	
								endif
								set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
							endif
						elif HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(mafia[mafped].id,player_ped_id())
						and (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),mafia[mafped].id)or IS_PLAYER_TARGETTING_ENTITY(player_id(),mafia[mafped].id))									
						 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
						 	and wcurrent != WEAPONTYPE_UNARMED
								if mafia[mafped].id != Active_ped
									TASK_SMART_FLEE_PED(mafia[mafped].id,mike(),300,-1)		
								endif
								mafia[mafped].sit = SSIT_AI_GUN
								if isentityalive(mafia[mafped].partner)
									if not IS_PED_FLEEING(mafia[mafped].partner)
										TASK_SMART_FLEE_PED(mafia[mafped].partner,mike(),300,-1)	
									endif	
									set_ped_situations(mafia[mafped].partner,mafia[mafped].sit)
								endif
							endif								
						endif
					endif						
				endif
			endfor	
		break
	endswitch	
endproc
PROC manage_GIANNI()
	//=====================	===================== =====================	=====================	
	if IS_ENTITY_IN_ANGLED_AREA(mike(),<<908.51294, -1699.99683, 65>>,<<910.62000, -1663.53870, 50>>,18) 
		//============================= hear explosion ============================= 	
		if HAS_PED_RECEIVED_EVENT(Gianni(), EVENT_EXPLOSION_HEARD)	
		or IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,<<910.30920, -1681.37939, 50.14197>>,30)			
		or IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<910.59302, -1671.71436, 49.13342>>,<<909.09564, -1689.48096, 54.12972>>,12.0,WEAPONTYPE_STICKYBOMB)
			if isentityalive(Anton())
			and isentityalive(milton())		
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					int rand = GET_RANDOM_INT_IN_RANGE(0,3)
					if   rand = 0
						ADD_PED_FOR_DIALOGUE(convo_struct,4,active_ped,"GIANNI")
						CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR",CONV_PRIORITY_MEDIUM)	
					elif rand = 1
						ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
						CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE",CONV_PRIORITY_MEDIUM)
					else
						ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1Actor")
						CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE2",CONV_PRIORITY_MEDIUM)	
					endif
				endif
				if not IS_PED_FLEEING(Anton())
					TASK_SMART_FLEE_PED(Anton(),mike(),200,-1)
				endif
				if not IS_PED_FLEEING(Milton())
					TASK_SMART_FLEE_PED(Milton(),mike(),200,-1)
				endif
				if not IS_PED_FLEEING(Gianni())
					TASK_SMART_FLEE_PED(Milton(),mike(),200,-1)
				endif
				MISSION_FAILED(Mff_scared_peds)
			endif
		endif	
		//============================= hear something ============================= 	
		if  GET_PLAYER_CURRENT_STEALTH_NOISE(player_id()) > 9.0 
		and NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())	
			if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_PED_FOR_DIALOGUE(convo_struct,4,active_ped,"GIANNI")
					CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR",CONV_PRIORITY_MEDIUM)	
				endif
				TASK_TURN_PED_TO_FACE_COORD(Gianni(),GET_ENTITY_COORDS(mike()),4000)			
			endif
		elif IS_FLASH_LIGHT_ON(PLAYER_PED_ID())
		and( GET_CLOCK_HOURS() >= 19 or GET_CLOCK_HOURS() < 6)
		and( 	IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),Gianni()) 
		or 	 IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),Gianni()))
			if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_PED_FOR_DIALOGUE(convo_struct,4,active_ped,"GIANNI")
					CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR",CONV_PRIORITY_MEDIUM)	
				endif	
				TASK_TURN_PED_TO_FACE_COORD(Gianni(),GET_ENTITY_COORDS(mike()),4000)
			endif
		elif IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_PED_FOR_DIALOGUE(convo_struct,4,active_ped,"GIANNI")
					CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HEAR",CONV_PRIORITY_MEDIUM)	
				endif
				TASK_TURN_PED_TO_FACE_COORD(Gianni(),GET_ENTITY_COORDS(mike()),4000)
			endif
		endif	
		
	else
		if HAS_PED_RECEIVED_EVENT(Gianni(), EVENT_EXPLOSION_HEARD)	
		or IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<919.02515, -1736.73352, 28>>,<<925.47205, -1649.41968, 65>>,120)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
		endif
	endif
		//============================= standing IDLE =============================	
	if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
	and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_PERFORM_SEQUENCE)
		if not IS_ENTITY_AT_COORD(Gianni(),<<913.27625, -1683.50452, 50.13412>>,<<2,2,2>>)
		or not IS_PED_FACING_PED(Gianni(),Milton(),40)	
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<913.27625, -1683.50452, 50.13412>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,53.21)
				if not IS_PED_FACING_PED(Gianni(),Milton(),40)			
					TASK_TURN_PED_TO_FACE_ENTITY(null,Milton())
				endif
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(Gianni(),seq)
			CLEAR_SEQUENCE_TASK(seq)
		endif					
	endif
endproc

FUNC SCARE_TYPE GET_SCARE_TYPE()
	if isentityalive(vehs[mvf_heli].id)
	and isentityalive(Milton())
		if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
			// ============== UNDER BRIDGE =================
			iBridgeCountDuringMission = CHECK_BRIDGE_COMPLETION() //BUG FIX 1399435 check this every frame to see if player has gone under any bridges
			PRINTSTRING("iBridgeCountDuringMission = ") PRINTINT(iBridgeCountDuringMission) PRINTNL()
			PRINTSTRING("iBridgeCountAtStart = ") PRINTINT(iBridgeCountAtStart) PRINTNL()
			IF iBridgeCountAtStart < iBridgeCountDuringMission //BUG FIX 1399435
//			if g_bUnderTheBridgeTriggered //BUG FIX 1399435
				CPRINTLN(DEBUG_MISSION,"SCARE_BRIDGE")
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_BR",CONV_PRIORITY_MEDIUM)
						g_bUnderTheBridgeTriggered = false
						iBridgeCountAtStart = CHECK_BRIDGE_COMPLETION() //BUG FIX 1399435 reset the start counter now
						return SCARE_BRIDGE	
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_BRIDGE",CONV_PRIORITY_MEDIUM)
						g_bUnderTheBridgeTriggered = false
						iBridgeCountAtStart = CHECK_BRIDGE_COMPLETION() //BUG FIX 1399435 reset the start counter now
						return SCARE_BRIDGE	
					endif
				endif			
			endif
			// ================= CRASH ====================
			iHeliHealthNow = GET_ENTITY_HEALTH(vehs[mvf_heli].id) //BUG FIX 1399448 get the heli's health at the start then if it drops down by 50 return SCARE_CRASH
			IF iHeliHealthNow < (iHeliHealthStart - 50)		//BUG FIX 1399448	
//			if Has_Vehicle_Been_Damaged()//BUG FIX 1399448
				CPRINTLN(DEBUG_MISSION,"SCARE_CRASH")
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_CR",CONV_PRIORITY_MEDIUM)
						iHeliHealthStart = GET_ENTITY_HEALTH(vehs[mvf_heli].id)//BUG FIX 1399448
						return SCARE_CRASH
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CRASH",CONV_PRIORITY_MEDIUM)
						iHeliHealthStart = GET_ENTITY_HEALTH(vehs[mvf_heli].id)//BUG FIX 1399448
						return SCARE_CRASH
					endif
				endif
			endif
			// ================= LOW and FAST ====================
			if GET_ENTITY_SPEED(vehs[mvf_heli].id) >= LOW_HEIGHT_SPEED
			and GET_ENTITY_HEIGHT_ABOVE_GROUND(vehs[mvf_heli].id) <= LOW_HEIGHT_MAX
				CPRINTLN(DEBUG_MISSION,"SCARE_LOW_FAST: speed: ",GET_ENTITY_SPEED(vehs[mvf_heli].id)," height: ",GET_ENTITY_HEIGHT_ABOVE_GROUND(vehs[mvf_heli].id))	
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_LOW1",CONV_PRIORITY_MEDIUM)
						return SCARE_LOW_FAST
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_LOW2",CONV_PRIORITY_MEDIUM)
						return SCARE_LOW_FAST
					endif
				endif
			endif
			// ================= NEAR MISS ==================== //Bug fix 1399440 have sent to code as the below function is returning true all the time.	
			if GET_ENTITY_SPEED(vehs[mvf_heli].id) >= LOW_HEIGHT_SPEED
				if shapetestheli = null
					shapetestheli = START_SHAPE_TEST_BOX(GET_ENTITY_COORDS(vehs[mvf_heli].id), <<15,15,6>>, GET_ENTITY_ROTATION(vehs[mvf_heli].id), DEFAULT, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE, vehs[mvf_heli].id)
				endif							
				
				if GET_SHAPE_TEST_RESULT(shapetestheli,iShapetestResult,vecShapetestResult,vecShapetestNormal,hitEntity) != SHAPETEST_STATUS_RESULTS_NOTREADY
					shapetestheli = null
					if iShapetestResult = 1
						CPRINTLN(DEBUG_MISSION,"SCARE_CRASH from NEAR MISS")
						if GET_RANDOM_BOOL()						
							ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
							if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_GENM",CONV_PRIORITY_MEDIUM)
								return SCARE_CRASH
							endif
						else
							ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
							if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_GENA",CONV_PRIORITY_MEDIUM)
								return SCARE_CRASH
							endif
						endif
					endif			
				endif
			endif
			// ================= PITCH ====================
			vector vRotSpeed = GET_ENTITY_ROTATION_VELOCITY(vehs[mvf_heli].id)
			if vRotSpeed.x <= -PITCH_CHANGE
			or vRotSpeed.x >= PITCH_CHANGE
				CPRINTLN(DEBUG_MISSION,"SCARE_PITCH: ", vRotSpeed.x)
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_PH",CONV_PRIORITY_MEDIUM)
						return SCARE_PITCH
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_PITCH",CONV_PRIORITY_MEDIUM)
						return SCARE_PITCH
					endif
				endif
			endif
			// ================= HEADING ====================
			if vRotSpeed.z <= -HEADING_CHANGE
			or vRotSpeed.z >= HEADING_CHANGE
				CPRINTLN(DEBUG_MISSION,"SCARE_heading: ", vRotSpeed.z)
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_PH",CONV_PRIORITY_MEDIUM)	
						return SCARE_HEADING_CHANGE
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_PITCH",CONV_PRIORITY_MEDIUM)
						return SCARE_HEADING_CHANGE	
					endif
				endif
			endif
			// ================= HEIGHT ====================
			vector vSpeedVector = GET_ENTITY_SPEED_VECTOR(vehs[mvf_heli].id)		
			if vSpeedVector.z <= DROP_SPEED
				CPRINTLN(DEBUG_MISSION,"SCARE_DROP: ", vRotSpeed.z)
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_GENM",CONV_PRIORITY_MEDIUM)	
						return SCARE_HEIGHT_DROP
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton",true)
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_GENA",CONV_PRIORITY_MEDIUM)
						return SCARE_HEIGHT_DROP	
					endif
				endif
			endif
			// ================= SPEED ====================
			if GET_ENTITY_SPEED(vehs[mvf_heli].id) >= HIGH_SPEED_TRIGGER
				CPRINTLN(DEBUG_MISSION,"SCARE_SPEED: ", GET_ENTITY_SPEED(vehs[mvf_heli].id))	
				if GET_RANDOM_BOOL()
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_SP",CONV_PRIORITY_MEDIUM)	
						return SCARE_SPEED
					endif
				else
					ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton",true)
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_FAST",CONV_PRIORITY_MEDIUM)
						return SCARE_SPEED	
					endif
				endif	
			endif			
		endif
	endif
return SCARE_NONE
endfunc
func bool Scared_Milton()
	switch iScareStage		
		case 0
			if IS_SAFE_TO_DISPLAY_GODTEXT()
				PRINT_NOW("CMN_GENGETINHE",DEFAULT_GOD_TEXT_TIME,1)
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_heli].id)
				endif		
				bScareHeliWarn = false
				
				//Set flags for bridge check at start
				iBridgeCountAtStart = CHECK_BRIDGE_COMPLETION() //BUG FIX 1399435 Get the start count for how many bridges have been flown under
				iLastIncrementTimer = 0
				iHeliHealthStart = GET_ENTITY_HEALTH(vehs[mvf_heli].id)
				
				iScareStage++
			endif
		break
		case 1
			if isentityalive(vehs[mvf_heli].id)
				if GET_DISTANCE_BETWEEN_ENTITIES(mike(),vehs[mvf_heli].id) < 5
				and IS_SAFE_TO_START_CONVERSATION()
					ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
					ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
					if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_NOGO",CONV_PRIORITY_MEDIUM)
						bpauseconv = false
						iScareStage++		
					endif
				endif
			endif
		break
		case 2			
			if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
			and not IS_SCRIPTED_CONVERSATION_ONGOING()
			and IS_SAFE_TO_START_CONVERSATION()
				ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
				ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
				if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_INCHOP",CONV_PRIORITY_MEDIUM)
					bpauseconv = false
					iScareStage++		
				endif				
			elif IS_SCRIPTED_CONVERSATION_ONGOING()
				PAUSE_FACE_TO_FACE_CONVERSATION(false)	
			endif
		break
		case 3			
			if IS_SCRIPTED_CONVERSATION_ONGOING()
				PAUSE_FACE_TO_FACE_CONVERSATION(false)	
			else
				if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
				and IS_SAFE_TO_DISPLAY_GODTEXT()
					PRINT_NOW("SOL1_SCARE",DEFAULT_GOD_TEXT_TIME,1)
					iScareInt = 0
					bPlayChat = true
					iScareTimer2 = get_Game_timer()
					iScareStage++
				endif
			endif
		break
		case 4
			if iScareInt < 5			
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				and get_Game_timer() - iScareTimer2 > 2000
					if GET_SCARE_TYPE()	<> SCARE_NONE	
						PRINTSTRING("GET_SCARE_TYPE <> SCARE_NONE") PRINTNL()
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Milton(),SCRIPT_TASK_PERFORM_SEQUENCE)
						and HAS_ANIM_DICT_LOADED("misssolomon_1")
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM(null,"misssolomon_1","anton_heli_loop",walk_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
								TASK_PLAY_ANIM(null,"misssolomon_1","anton_heli_exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							CLOSE_SEQUENCE_TASK(seq)					
							TASK_PERFORM_SEQUENCE(Milton(),seq)
							CLEAR_SEQUENCE_TASK(seq)
						endif
						iScareTimer2 = get_Game_timer()
						bPlayChat = true
						REPLAY_RECORD_BACK_FOR_TIME(5.0)	
						iScareInt++
					else
						switch iScareInt
							case 0
								if bPlayChat
									PRINT_HELP("SOL1_SCAREHLP")
									ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
									ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
									ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"ANTON",true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CHAT1",CONV_PRIORITY_MEDIUM)
										bPlayChat = false
										iScareTimer2 = get_Game_timer()
									endif
								else
									if get_Game_timer() - iScareTimer2 > 20000
										if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
											if IS_PED_IN_VEHICLE(Milton(),GET_VEHICLE_PED_IS_IN(mike()))
											and IS_PED_IN_VEHICLE(Anton(),GET_VEHICLE_PED_IS_IN(mike()))												
												if GET_RANDOM_BOOL()										
													ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
													if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_ID",CONV_PRIORITY_MEDIUM)
														iScareTimer2 = get_Game_timer()
													endif
												else
													ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton",true)
													if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_NOSCAR",CONV_PRIORITY_MEDIUM)
														iScareTimer2 = get_Game_timer()
													endif
												endif
											endif	
										endif
									endif
								endif
							break
							case 1								
								if get_Game_timer() - iScareTimer2 > 12000
									ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_SCARY",CONV_PRIORITY_MEDIUM)
										iScareTimer2 = get_Game_timer()
									endif
								endif								
							break
							case 2
								if bPlayChat
									ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
									ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
									ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"ANTON",true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CHAT3",CONV_PRIORITY_MEDIUM)
										bPlayChat = false
										iScareTimer2 = get_Game_timer()
									endif
								else
									if get_Game_timer() - iScareTimer2 > 25000
										if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
											if IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
											and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
												if GET_RANDOM_BOOL()										
													ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
													if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_SCARY",CONV_PRIORITY_MEDIUM)
														iScareTimer2 = get_Game_timer()
													endif
												else
													if GET_RANDOM_BOOL()										
														ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
														if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_ID",CONV_PRIORITY_MEDIUM)
															iScareTimer2 = get_Game_timer()
														endif
													else
														ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton",true)
														if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_NOSCAR",CONV_PRIORITY_MEDIUM)
															iScareTimer2 = get_Game_timer()
														endif
													endif
												endif
											endif
										endif
									endif
								endif
							break
							case 3							
							case 4
								if get_Game_timer() - iScareTimer2 > 25000
									if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
											if IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
											and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
												
											if GET_RANDOM_BOOL()										
												ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL",true)
												if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_SCARY",CONV_PRIORITY_MEDIUM)
													iScareTimer2 = get_Game_timer()
												endif
											else
												if GET_RANDOM_BOOL()										
													ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR",true)
													if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_HELI_ID",CONV_PRIORITY_MEDIUM)
														iScareTimer2 = get_Game_timer()
													endif
												else
													ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton",true)
													if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_NOSCAR",CONV_PRIORITY_MEDIUM)
														iScareTimer2 = get_Game_timer()
													endif
												endif
											endif
										endif
									endif
								endif
							break
						endswitch
						
					endif
				endif
			else
				if DOES_BLIP_EXIST(blip_objective)
					REMOVE_BLIP(blip_objective)
				endif
				iScareStage++
			endif
		break
		case 5
			
		break
	endswitch
	
	if iScareStage > 2
	and  iScareStage < 5
		if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_heli].id)
			if DOES_BLIP_EXIST(blip_objective)
				REMOVE_BLIP(blip_objective)
			endif	
			if IS_THIS_PRINT_BEING_DISPLAYED("CMN_GENGETBCKHE")
				CLEAR_PRINTS()
			endif
		else
			if not DOES_BLIP_EXIST(blip_objective)
				blip_objective = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_heli].id)
			endif	
			if not bScareHeliWarn
				print_now("CMN_GENGETBCKHE",DEFAULT_GOD_TEXT_TIME,1)
				bScareHeliWarn = true
			endif
		endif
	endif
	
	if iScareStage = 5	
		return true
	endif
return false	
endfunc
proc AUDIO_dialogue(MSF_MISSION_STAGE_FLAGS mstage)	
SWITCH mstage			
		CASE msf_0_get_to_warehouse
			switch iDialogue_Stage
				case 0
					if mission_substage > 1
						i_dialogueTimer = get_Game_timer()
						iDialogue_Stage++
					endif
				break
				case 1
					if get_game_timer() - i_dialogueTimer > 38000
					and not IS_MESSAGE_BEING_DISPLAYED()
					and mission_substage > 1
						if CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(convo_struct,CHAR_SOLOMON,"SOL1AUD","SOL1_SOL",CONV_PRIORITY_MEDIUM,DISPLAY_SUBTITLES,DO_ADD_TO_BRIEF_SCREEN,true)
							iDialogue_Stage++
						endif
					endif
				break	
				case 2
					if WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
						i_dialogueTimer = get_Game_timer()
						ADD_CONTACT_TO_PHONEBOOK(CHAR_SOLOMON,MICHAEL_BOOK,true)
						iDialogue_Stage++										
					ELif HAS_CELLPHONE_CALL_FINISHED()
						ADD_CONTACT_TO_PHONEBOOK(CHAR_SOLOMON,MICHAEL_BOOK,true)
						iDialogue_Stage = 4
					ENDIF
				break
				case 3
					if get_game_timer() - i_dialogueTimer > 3000
						if SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_SOLOMON,"SOL1_TEXT",TXTMSG_UNLOCKED,
															TXTMSG_NOT_CRITICAL,TXTMSG_AUTO_UNLOCK_AFTER_READ,
															NO_REPLY_REQUIRED,ICON_STANDARD_TEXTMSG,
															true,NO_BARTER_REQUIRED,CANNOT_CALL_SENDER)
															
						iDialogue_Stage++
						endif
					endif
				break
				case 4
					if mission_substage >= 4
//						if PLAYER_CALL_CHAR_CELLPHONE(convo_struct,CHAR_SOLOMON,"SOL1AUD","SOL1_SOL2",CONV_PRIORITY_MEDIUM)
//							iDialogue_Stage++
//						endif
					endif
				break
			endswitch
		break 
		case msf_1_Stealth_to_roof
			if eAI_stage = AI_CHECKS
			and mission_substage < 2
				//the closest ped for convo
				float closeDist 
				closeDist = 200
				ped_index clsePed
				clsePed = mafia[maf_Car].id
				int mafped
				for mafped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) - 1
					if isentityalive(mafia[mafped].id)
						if GET_DISTANCE_BETWEEN_ENTITIES(mafia[mafped].id,mike()) < closedist
							closedist = GET_DISTANCE_BETWEEN_ENTITIES(mafia[mafped].id,mike())
							clsePed = mafia[mafped].id
						endif
					endif
				endfor
				if isentityalive(Gianni())
					if GET_DISTANCE_BETWEEN_ENTITIES(Gianni(),mike()) < closedist
						closedist = GET_DISTANCE_BETWEEN_ENTITIES(Gianni(),mike())
						clsePed = Gianni()
					endif
				endif
								
					// talking by the car
					if isentityalive(mafia[maf_car].id)
					and isentityalive(mafia[maf_car_talking].id)
						switch iCarSpeechStage
							case 0
								if IS_ENTITY_IN_ANGLED_AREA(mike(),<<947.18097, -1717.85010, 38.68729>>,<<970.51593, -1720.15210, 29.54870>>, 22)
									if not bMusic_1stEnemy
										TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
										bMusic_1stEnemy = true
									endif
									if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car_talking].iVoiceID,mafia[maf_car_talking].id,mafia[maf_car_talking].sVoiceName,true)
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_AMB2",CONV_PRIORITY_MEDIUM)
											iCarSpeechStage++
										endif
									endif						
								endif	
							break
							case 1
								if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									i_dialogueTimer = get_Game_timer()
									bpauseconv = false
									iCarSpeechStage++
								else
									if not IS_ENTITY_IN_ANGLED_AREA(mike(),<<947.18097, -1717.85010, 38.68729>>,<<970.51593, -1720.15210, 29.54870>>, 22)								
										KILL_FACE_TO_FACE_CONVERSATION()
									endif
								endif
							break
							case 2	
								if clseped = mafia[maf_Car].id
								or clseped = mafia[maf_car_talking].id
									if get_game_timer() - i_dialogueTimer > 6000
									and GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_car_talking].id) < 17
									and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car_talking].iVoiceID,mafia[maf_car_talking].id,mafia[maf_car_talking].sVoiceName,true)
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_AMB3",CONV_PRIORITY_MEDIUM)
											println("play ped Amb speech: mafia[maf_car].id ")
											i_dialogueTimer = get_Game_timer()
											iCarSpeechStage = 3
										endif
									endif
								endif
							break
							case 3
								if clseped = mafia[maf_Car].id
								or clseped = mafia[maf_car_talking].id									
									if get_game_timer() - i_dialogueTimer > 12000
									and GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_car_talking].id) < 16
									and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car].iVoiceID,mafia[maf_car].id,mafia[maf_car].sVoiceName,true)
										if CREATE_CONVERSATION(convo_Struct,"SOL1AUD","SOL1_FIX1",CONV_PRIORITY_MEDIUM)
											println("play ped Amb speech: mafia[maf_car_talking].id ")
											i_dialogueTimer = get_Game_timer()
											iCarSpeechStage = 4
										endif								
									endif
								endif
							break
							case 4
								if clseped = mafia[maf_Car].id
								or clseped = mafia[maf_car_talking].id									
									if get_game_timer() - i_dialogueTimer > 10000
									and GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_car_talking].id) < 16
									and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_car_talking].iVoiceID,mafia[maf_car_talking].id,mafia[maf_car_talking].sVoiceName,true)
										if CREATE_CONVERSATION(convo_Struct,"SOL1AUD","SOL1_AMB4",CONV_PRIORITY_MEDIUM)
											println("play ped Amb speech: mafia[maf_car].id ")
											i_dialogueTimer = get_Game_timer()
											iCarSpeechStage = 3
										endif								
									endif
								endif
							break
						endswitch
					endif
					
				
				if clseped = mafia[maf_worker_alone].id
					if isentityalive(mafia[maf_worker_alone].id)		
						if GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_worker_alone].id) < 18
							vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID()) //BUG FIX 1399354 adding a hieght check on the player so dialogue doesn't play here if player is to low down.
							IF vPlayerCoords.z > 35.5
								if not bMusic_1stEnemy
									TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
									bMusic_1stEnemy = true
								endif
								if get_game_timer() - i_dialogueTimer > 12000							
									if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_worker_alone].iVoiceID,mafia[maf_worker_alone].id,mafia[maf_worker_alone].sVoiceName,true)
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_FIX2",CONV_PRIORITY_MEDIUM)									
											println("play ped Amb speech: mafia[maf_worker_alone].id ")
											i_dialogueTimer = get_Game_timer()
										endif									
									endif
								endif
							ENDIF
						endif
					endif
				elif clseped = mafia[maf_worker_vent].id
					if isentityalive(mafia[maf_worker_vent].id)
						if GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_worker_vent].id) < 17
							if not bMusic_1stEnemy
								TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
								bMusic_1stEnemy = true
							endif
							if get_game_timer() - i_dialogueTimer > 15000							
								if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_worker_vent].iVoiceID,mafia[maf_worker_vent].id,mafia[maf_worker_vent].sVoiceName,true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_AMB4",CONV_PRIORITY_MEDIUM)									
										println("play ped Amb speech: mafia[maf_worker_vent].id ")
										i_dialogueTimer = get_Game_timer()
									endif									
								endif
							endif
						endif
					endif
				elif clseped = mafia[maf_worker_door].id
					if isentityalive(mafia[maf_worker_door].id)
						if GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_worker_door].id) < 17
							if not bMusic_1stEnemy
								TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
								bMusic_1stEnemy = true
							endif
							if get_game_timer() - i_dialogueTimer > 30000							
								if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_worker_door].iVoiceID,mafia[maf_worker_door].id,mafia[maf_worker_door].sVoiceName,true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CHECK",CONV_PRIORITY_MEDIUM)									
										println("play ped Amb speech: mafia[maf_worker_vent].id ")
										i_dialogueTimer = get_Game_timer()
									endif									
								endif
							endif
						endif
					endif	
				elif clseped = mafia[maf_worker_top].id
					if isentityalive(mafia[maf_worker_top].id)
						if GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_worker_top].id) < 17
							if not bMusic_1stEnemy
								TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
								bMusic_1stEnemy = true
							endif
							if get_game_timer() - i_dialogueTimer > 10000
								if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_worker_top].iVoiceID,mafia[maf_worker_top].id,mafia[maf_worker_top].sVoiceName,true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_FIX2",CONV_PRIORITY_MEDIUM)								
										println("play ped Amb speech: mafia[maf_worker_top].id ")		
										i_dialogueTimer = get_Game_timer()
									endif									
								endif
							endif
						endif
					endif
				elif clseped = mafia[maf_worker_walkway].id
				or clseped = mafia[maf_walkway_door].id
					//walkway peds
					if isentityalive(mafia[maf_worker_walkway].id)
					and isentityalive(mafia[maf_walkway_door].id)	
						if GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[maf_walkway_door].id) < 25
							if bDoWalkwayChat	
								if not bMusic_1stEnemy
									TRIGGER_MUSIC_EVENT("SOL1_1ST_ENEMY")
									bMusic_1stEnemy = true
								endif
								switch iwalkwayChat
									case 0
										if get_game_timer() - i_dialogueTimer > 3000
											if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_walkway_door].iVoiceID,mafia[maf_walkway_door].id,mafia[maf_walkway_door].sVoiceName,true)
												if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_WLKTLK",CONV_PRIORITY_MEDIUM)									
													println("play ped Amb speech: mafia[maf_walkway_door].id ")
													iwalkwayChat++
												endif									
											endif
										endif
									break
									case 1
										if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											if get_game_timer() - i_dialogueTimer > 5000
												ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_walkway_door].iVoiceID,mafia[maf_walkway_door].id,mafia[maf_walkway_door].sVoiceName,true)											
												if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_WLKTLK2",CONV_PRIORITY_MEDIUM)								
													println("play ped Amb speech: mafia[maf_walkway_door].id ")
													iwalkwayChat++
												endif
											endif
										else
											i_dialogueTimer = get_Game_timer()
										endif
									break
									case 2
										if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											if get_game_timer() - i_dialogueTimer > 3000
												ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_walkway_door].iVoiceID,mafia[maf_walkway_door].id,mafia[maf_walkway_door].sVoiceName,true)
												if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_WLKTLK",CONV_PRIORITY_MEDIUM)									
													println("play ped Amb speech: mafia[maf_walkway_door].id ")
													iwalkwayChat++
												endif	
											endif
										else
											i_dialogueTimer = get_Game_timer()
										endif
									break
									case 3
										if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()										
											if get_game_timer() - i_dialogueTimer > 4000
												ADD_PED_FOR_DIALOGUE(convo_struct,mafia[maf_walkway_door].iVoiceID,mafia[maf_walkway_door].id,mafia[maf_walkway_door].sVoiceName,true)
												if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_WLKTLK2",CONV_PRIORITY_MEDIUM)
													println("play ped Amb speech: mafia[maf_walkway_door].id ")
													i_dialogueTimer = get_Game_timer()
													iwalkwayChat = 0
													bDoWalkwayChat = false
												endif											
											endif
										else
											i_dialogueTimer = get_Game_timer()
										endif	
									break
								endswitch
							endif
						endif	
					endif
				elif clseped = Gianni()
					if GET_DISTANCE_BETWEEN_ENTITIES(mike(),Gianni()) < 14
					and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
						switch iGianniChat
							case 0 
								if get_game_timer() - i_dialogueTimer > 12000							
									if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,4,Gianni(),"GIANNI",true)
										ADD_PED_FOR_DIALOGUE(convo_struct,2,peds[mpf_milton].id,"SOL1Actor",true)								
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_TALK1",CONV_PRIORITY_MEDIUM)
											if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
											and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_PERFORM_SEQUENCE)
												OPEN_SEQUENCE_TASK(seq)			
													if not IS_PED_FACING_PED(Gianni(),Milton(),40)
														TASK_TURN_PED_TO_FACE_ENTITY(NULL, Milton())
													endif
													if not IS_PED_HEADTRACKING_PED(Gianni(),Milton())
														TASK_LOOK_AT_ENTITY(null,Milton(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
													endif
													TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_b",WALK_BLEND_IN,WALK_BLEND_OUT)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(Gianni(),seq)
												CLEAR_SEQUENCE_TASK(seq)
											endif
											i_dialogueTimer = get_Game_timer()
											iGianniChat++
										endif									
									endif
								endif
							break
							case 1
								if get_game_timer() - i_dialogueTimer > 12000							
									if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,4,Gianni(),"GIANNI",true)
										ADD_PED_FOR_DIALOGUE(convo_struct,2,peds[mpf_milton].id,"SOL1Actor",true)		
										ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_Anton].id,"ANTON",true)
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_TALK2",CONV_PRIORITY_MEDIUM)
											if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
											and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_PERFORM_SEQUENCE)
												OPEN_SEQUENCE_TASK(seq)			
													if not IS_PED_FACING_PED(Gianni(),Milton(),40)
														TASK_TURN_PED_TO_FACE_ENTITY(NULL, Milton())
													endif
													if not IS_PED_HEADTRACKING_PED(Gianni(),Milton())
														TASK_LOOK_AT_ENTITY(null,Milton(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
													endif
													TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_d",WALK_BLEND_IN,WALK_BLEND_OUT)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(Gianni(),seq)
												CLEAR_SEQUENCE_TASK(seq)
											endif
											i_dialogueTimer = get_Game_timer()
											iGianniChat++
										endif									
									endif
								endif
							break
							case 2
								if get_game_timer() - i_dialogueTimer > 12000							
									if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,4,Gianni(),"GIANNI",true)
										ADD_PED_FOR_DIALOGUE(convo_struct,2,peds[mpf_milton].id,"SOL1Actor",true)		
										ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_Anton].id,"ANTON",true)	
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_TALK3",CONV_PRIORITY_MEDIUM)
											if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
											and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_PERFORM_SEQUENCE)
												OPEN_SEQUENCE_TASK(seq)			
													if not IS_PED_FACING_PED(Gianni(),Milton(),40)
														TASK_TURN_PED_TO_FACE_ENTITY(NULL, Milton())
													endif
													if not IS_PED_HEADTRACKING_PED(Gianni(),Milton())
														TASK_LOOK_AT_ENTITY(null,Milton(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
													endif
													TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_e",WALK_BLEND_IN,WALK_BLEND_OUT)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(Gianni(),seq)
												CLEAR_SEQUENCE_TASK(seq)
											endif
											i_dialogueTimer = get_Game_timer()
											iGianniChat++
										endif									
									endif
								endif
							break
							case 3
								if get_game_timer() - i_dialogueTimer > 15000							
									if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										ADD_PED_FOR_DIALOGUE(convo_struct,4,Gianni(),"GIANNI",true)
										if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_SIT",CONV_PRIORITY_MEDIUM)
											if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 
											and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Gianni(),SCRIPT_TASK_PERFORM_SEQUENCE)
												OPEN_SEQUENCE_TASK(seq)			
													if not IS_PED_FACING_PED(Gianni(),Milton(),40)
														TASK_TURN_PED_TO_FACE_ENTITY(NULL, Milton())
													endif
													if not IS_PED_HEADTRACKING_PED(Gianni(),Milton())
														TASK_LOOK_AT_ENTITY(null,Milton(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
													endif
													TASK_PLAY_ANIM(null,AnimDict_gestures,"idle_c",WALK_BLEND_IN,WALK_BLEND_OUT)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(Gianni(),seq)
												CLEAR_SEQUENCE_TASK(seq)
											endif
											i_dialogueTimer = get_Game_timer()
										endif									
									endif
								endif
							break
						endswitch
					endif	
				endif
			endif
		break
				
		case msf_2_fight_rocco	
			switch iDialogue_Stage
				case 0
					if not IS_MESSAGE_BEING_DISPLAYED()
					and mission_substage > 3
						ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL")
//						if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_ROC",CONV_PRIORITY_MEDIUM)
//							TASK_LOOK_AT_ENTITY(mike(),vehs[mvf_heli].id,-1,SLF_WHILE_NOT_IN_FOV)
							iDialogue_Stage++
//						endif
					endif
				break
				case 1
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						iDialogue_Stage++
					endif
				break
				case 2
					if isentityalive(rocco())
						if not IS_PED_HEADTRACKING_PED(mike(),rocco())
							TASK_LOOK_AT_ENTITY(mike(),rocco(),-1,SLF_WHILE_NOT_IN_FOV)
						endif
					else
						if isentityalive(vehs[mvf_heli].id)
							if not IS_PED_HEADTRACKING_ENTITY(mike(),vehs[mvf_heli].id)
								TASK_LOOK_AT_ENTITY(mike(),vehs[mvf_heli].id,-1,SLF_WHILE_NOT_IN_FOV)
							endif
						endif
					endif
				break
			endswitch
		break
		
		case msf_3_get_to_sol
			switch iDialogue_Stage
				case 0
					if IS_PED_IN_ANY_VEHICLE(mike())
						if IS_PED_IN_VEHICLE(Milton(),GET_VEHICLE_PED_IS_IN(mike()))
						and IS_PED_IN_VEHICLE(Anton(),GET_VEHICLE_PED_IS_IN(mike()))
							i_dialogueTimer = get_Game_timer()
							iDialogue_Stage++
						endif
					endif
				break
				case 1
					if  not IS_MESSAGE_BEING_DISPLAYED()
					and mission_substage = 2
						if isentityalive(mike())	
						and isentityalive(Milton())
						and isentityalive(Anton())
							if IS_PED_IN_ANY_VEHICLE(mike())
								if IS_PED_IN_VEHICLE(Milton(),GET_VEHICLE_PED_IS_IN(mike()))
								and IS_PED_IN_VEHICLE(Anton(),GET_VEHICLE_PED_IS_IN(mike()))
									ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"Michael",true)
									add_ped_for_dialogue(convo_struct,2,Milton(),"SOL1ACTOR",true)
									add_ped_for_dialogue(convo_struct,3,Anton(),"Anton",true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_trav",CONV_PRIORITY_MEDIUM)
										bpauseconv = false
										iDialogue_Stage++
									endif
								endif
							endif
						endif
					endif
				break		
				case 2
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						i_dialogueTimer = get_game_timer()
						iDialogue_Stage++	
					endif					
				break
				case 3
					if  not IS_MESSAGE_BEING_DISPLAYED()
						if isentityalive(mike())	
						and isentityalive(Milton())
						and isentityalive(Anton())
							if IS_PED_IN_ANY_VEHICLE(mike())
								if IS_PED_IN_VEHICLE(Milton(),GET_VEHICLE_PED_IS_IN(mike()))
								and IS_PED_IN_VEHICLE(Anton(),GET_VEHICLE_PED_IS_IN(mike()))
									ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"Michael",true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_BACK",CONV_PRIORITY_MEDIUM)
										bpauseconv = false
										iDialogue_Stage++
									endif
								endif
							endif
						endif
					endif
				break		
				case 4
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						i_dialogueTimer = get_game_timer()
						iDialogue_Stage++	
					endif					
				break
				case 5
					if isentityalive(mike())	
					and isentityalive(Milton())
					and isentityalive(Anton())	
					and get_game_timer() - i_dialogueTimer > 5000
						if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
						and IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
						and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
						and not IS_MESSAGE_BEING_DISPLAYED()
							ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"Michael",true)
							add_ped_for_dialogue(convo_struct,2,Milton(),"SOL1actor",true)
							add_ped_for_dialogue(convo_struct,3,Anton(),"Anton",true)
							if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CLUB",CONV_PRIORITY_MEDIUM)
								bpauseconv = false
								iDialogue_Stage++
							endif
						endif	
					endif
				break
				case 6
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
						i_dialogueTimer = get_game_timer()
						iDialogue_Stage++	
					else
						if IS_PED_IN_GROUP(Milton())
						and IS_PED_IN_GROUP(Anton())
							if bpauseconv
								PAUSE_FACE_TO_FACE_CONVERSATION(false)	
								bpauseconv = false
							endif
						else
							if not bPauseconv
								PAUSE_FACE_TO_FACE_CONVERSATION(true)
								bpauseconv = true
							endif
						endif
					endif					
				break
				case 7
					if isentityalive(mike())	
					and isentityalive(Milton())
					and isentityalive(Anton())	
						if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
						and IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
						and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
							if not IS_MESSAGE_BEING_DISPLAYED()
								ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"Michael",true)
								add_ped_for_dialogue(convo_struct,2,Milton(),"SOL1actor",true)
								add_ped_for_dialogue(convo_struct,3,Anton(),"Anton",true)
								if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_CLUB2",CONV_PRIORITY_MEDIUM)
									bpauseconv = false
									iDialogue_Stage++
								endif
							endif
						else
							bpauseconv = false
							iDialogue_Stage++
						endif
					endif
				break
				case 8
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
						i_dialogueTimer = get_game_timer()
						iDialogue_Stage++	
					else
						if IS_PED_IN_GROUP(Milton())
						and IS_PED_IN_GROUP(Anton())
							if bpauseconv
								PAUSE_FACE_TO_FACE_CONVERSATION(false)	
								bpauseconv = false
							endif
						else
							if not bPauseconv
								PAUSE_FACE_TO_FACE_CONVERSATION(true)
								bpauseconv = true
							endif
						endif
					endif	
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mike()),<<-1034.74194, -497.20056, 35.37177>>) < 180
						KILL_FACE_TO_FACE_CONVERSATION()
					endif
				break
				// closer
				case 9
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mike()),<<-1034.74194, -497.20056, 35.37177>>) < 180
					and not IS_MESSAGE_BEING_DISPLAYED()	
						if isentityalive(mike())	
						and isentityalive(Milton())
						and isentityalive(Anton())
							if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
							and IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
							and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
								ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"Michael",true)
								add_ped_for_dialogue(convo_struct,2,Milton(),"SOL1ACTOR",true)
								add_ped_for_dialogue(convo_struct,3,Anton(),"Anton",true)
								if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_LAND",CONV_PRIORITY_MEDIUM)
									bpauseconv = false
									iDialogue_Stage++
								endif
							else
								bpauseconv = false
								iDialogue_Stage++
							endif
						endif						
					endif
				break
				case 10
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
						iDialogue_Stage++	
					else
						if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
						and IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
						and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
							if bpauseconv
								PAUSE_FACE_TO_FACE_CONVERSATION(false)	
								bpauseconv = false
							endif
						else							
							KILL_FACE_TO_FACE_CONVERSATION()
						endif
					endif					
				break
				case 11
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mike()),<<-1098.04102, -458.76242, 34.38025>>) < 60
					and not IS_MESSAGE_BEING_DISPLAYED()	
						if isentityalive(mike())	
						and isentityalive(Milton())
						and isentityalive(Anton())
							if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
							and IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
							and IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
								if not IS_MESSAGE_BEING_DISPLAYED()
									ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"Michael",true)
									add_ped_for_dialogue(convo_struct,3,Anton(),"Anton",true)
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_LAND2",CONV_PRIORITY_MEDIUM)
										iDialogue_Stage++
									endif
								endif
							else
								iDialogue_Stage++
							endif
						endif						
					endif
				break				
				case 12
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
						iDialogue_Stage++
					endif
				break
				case 13
					if not IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
					and not IS_MESSAGE_BEING_DISPLAYED()
					and mission_substage = 4
						ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"Michael")
						if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_OFFICE",CONV_PRIORITY_MEDIUM)
							iDialogue_Stage++
						endif
					endif
				break
				case 14
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
						if isentityalive(mike())	
						and isentityalive(Milton())
						and isentityalive(Anton())
							if not IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
							and not IS_PED_IN_VEHICLE(Milton(),vehs[mvf_heli].id)
							and not IS_PED_IN_VEHICLE(Anton(),vehs[mvf_heli].id)
							and not IS_MESSAGE_BEING_DISPLAYED()
								ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"Michael")
								add_ped_for_dialogue(convo_struct,3,Anton(),"Anton")
								if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_ANTON",CONV_PRIORITY_MEDIUM)
									iDialogue_Stage++
								endif
							endif							
						endif
					endif	
				break
				case 16
				break
			endswitch
		break
endswitch
endproc
PROC PLACE_CONSTRUCTION()
	switch iconstructStage
		case 0
//			constObjs[cobj_floor_bricks].model		=	prop_conc_blocks01a
//			constObjs[cobj_floor_bricks].vpos		=	<<954.371094,-1740.741577,30.032013>>	
//			constObjs[cobj_floor_bricks].vRot		=	<<-0.507340,2.175111,19.144281>>
			
			constObjs[cobj_floor_pipes1].model		=	prop_pipes_conc_02
			constObjs[cobj_floor_pipes1].vpos		=	<<964.50, -1734.80, 31.12>>
			constObjs[cobj_floor_pipes1].vRot		=	<<0.77, 1.55, -46.19>>
			
			constObjs[cobj_floor_wood1].model		= 	prop_woodpile_04b
			constObjs[cobj_floor_wood1].vpos		=	<<966.680298,-1726.700073,29.804991>>
			constObjs[cobj_floor_wood1].vRot		=	<<-0.000000,0.000000,-8.021410>>
			
			constObjs[cobj_floor_wood2].model		= 	prop_woodpile_01a
			constObjs[cobj_floor_wood2].vpos		=	<<967.535339,-1716.915161,29.452488>>
			constObjs[cobj_floor_wood2].vRot		= 	<<-0.000000,0.000000,24.064224>>
			
			constObjs[cobj_floor_toolbox].model		=	prop_tool_box_04
			constObjs[cobj_floor_toolbox].vpos		=	<<967.513000,-1717.480957,30.432888>>
			constObjs[cobj_floor_toolbox].vRot		=	<<0.000442,0.000436,-0.177949>>
			
			constObjs[cobj_floor_torch].model		= 	prop_tool_torch
			constObjs[cobj_floor_torch].vpos		= 	<<966.909607,-1716.346802,30.542349>>
			constObjs[cobj_floor_torch].vRot		= 	<<-0.037208,-0.043627,29.224613>>
			
			constObjs[cobj_floor_hat1].model		= 	prop_tool_hardhat
			constObjs[cobj_floor_hat1].vpos			= 	<<966.425232,-1715.816284,30.449116>>
			constObjs[cobj_floor_hat1].vRot			= 	<<6.111341,0.458651,0.062687>>
			
			constObjs[cobj_floor_hat2].model		= 	prop_tool_hardhat
			constObjs[cobj_floor_hat2].vpos			= 	<<966.772095,-1715.110229,30.449089>>
			constObjs[cobj_floor_hat2].vRot			= 	<<5.993145,-0.263961,-78.341858>>
			
			constObjs[cobj_floor_pipes2].model		=	prop_pipes_conc_02
			constObjs[cobj_floor_pipes2].vpos		=	<<966.067078,-1712.865356,30.47>>
			constObjs[cobj_floor_pipes2].vRot		=	<<-0.000000,0.000000,-78.495216>>
			
			constObjs[cobj_floor_gen].model			=	prop_generator_03b
			constObjs[cobj_floor_gen].vpos			=	<<956.564636,-1717.747681,29.522371>>
			constObjs[cobj_floor_gen].vRot			=	<<-2.104723,-1.473127,-22.815186>>
			
			constObjs[cobj_floor_bitsbox].model		=	prop_partsbox_01
			constObjs[cobj_floor_bitsbox].vpos		=	<<952.706726,-1722.553223,29.788635>>
			constObjs[cobj_floor_bitsbox].vRot		=	<<-1.702785,0.017533,-0.143250>>
			
			constObjs[cobj_floor_cementbags].model	=	prop_cementbags01
			constObjs[cobj_floor_cementbags].vpos	=	<<952.230164,-1724.708130,29.852713>>
			constObjs[cobj_floor_cementbags].vRot	=	<<-1.654111,-0.375194,-18.207163>>
			
			constObjs[cobj_floor_hat3].model		= 	prop_tool_hardhat
			constObjs[cobj_floor_hat3].vpos			= 	<<952.183899,-1724.966431,31.089499>>
			constObjs[cobj_floor_hat3].vRot			= 	<<3.540115,-1.973789,-30.968338>>
			
			constObjs[cobj_roof1_toolbox].model		=	prop_tool_box_01
			constObjs[cobj_roof1_toolbox].vpos		=	<<937.017273,-1717.222290,38.383656>>
			constObjs[cobj_roof1_toolbox].vRot		=	<<-0.326517,-0.000184,20.948112>>
			
			constObjs[cobj_roof1_wirecoil1].model	=	prop_cablespool_06
			constObjs[cobj_roof1_wirecoil1].vpos	=	<<935.325073,-1717.042847,37.059296>>
			constObjs[cobj_roof1_wirecoil1].vRot	=	<<0.000117,0.000050,2.848160>>
			
			constObjs[cobj_roof1_wirecoil2].model	=	prop_cablespool_02
			constObjs[cobj_roof1_wirecoil2].vpos	=	<<933.29, -1717.13, 38.05>>
			constObjs[cobj_roof1_wirecoil2].vRot	=	<<-166.80, 89.97, 0.00>>
			
			constObjs[cobj_roof1_light1].model		= 	prop_worklight_03b
			constObjs[cobj_roof1_light1].vpos		=	<<934.624329,-1719.507080,37.058407>>
			constObjs[cobj_roof1_light1].vRot		=	<<0.081973,0.089925,115.900665>>
			
			constObjs[cobj_roof1_light2].model		=	prop_worklight_02a
			constObjs[cobj_roof1_light2].vpos		=	<<924.997070,-1722.984375,37.069111>>
			constObjs[cobj_roof1_light2].vRot		=	<<-0.000294,0.000053,-155.938309>>
			
			constObjs[cobj_roof1_gen].model			=	prop_generator_01a
			constObjs[cobj_roof1_gen].vpos			=	<<923.544678,-1722.781006,37.057270>>
			constObjs[cobj_roof1_gen].vRot			=	<<-0.000000,-0.000000,-142.093521>>
			
			constObjs[cobj_roof1_light3].model		=	prop_worklight_01a
			constObjs[cobj_roof1_light3].vpos		=	<<922.297913,-1722.262329,37.063000>>
			constObjs[cobj_roof1_light3].vRot		=	<<0.000000,0.000000,150.114929>>
			
			constObjs[cobj_roof1_hat].model			=	prop_tool_hardhat
			constObjs[cobj_roof1_hat].vpos			=	<<923.213501,-1720.317993,38.599242>>
			constObjs[cobj_roof1_hat].vRot			=	<<6.002445,-0.533969,37.316181>>
				
			constObjs[cobj_ally_light].model		=	prop_worklight_03b
			constObjs[cobj_ally_light].vpos			=	<<885.808472,-1716.161987,37.058754>>
			constObjs[cobj_ally_light].vRot			=	<<0.097367,0.172507,110.473656>>
			
			constObjs[cobj_ally_lightbox].model		=	prop_worklight_04a
			constObjs[cobj_ally_lightbox].vpos		=	<<886.306396,-1717.104004,37.495296>>
			constObjs[cobj_ally_lightbox].vRot		=	<<-0.000160,0.000082,131.737686>>
			
			constObjs[cobj_ally_gen].model			=	prop_generator_01a
			constObjs[cobj_ally_gen].vpos			=	<<886.096436,-1718.347168,37.063030>>
			constObjs[cobj_ally_gen].vRot			=	<<-0.000000,0.000000,85.943665>>
			
			constObjs[cobj_floor2_pipes].model		=	prop_pipes_conc_02
			constObjs[cobj_floor2_pipes].vpos		=	<<868.939148,-1720.341064,30.067484>>
			constObjs[cobj_floor2_pipes].vRot		=	<<-0.000000,0.000000,-39.534081>>
			
			constObjs[cobj_floor2_barrier1].model	=	prop_barrier_work06a
			constObjs[cobj_floor2_barrier1].vpos	=	<<873.640320,-1724.027954,29.139503>>
			constObjs[cobj_floor2_barrier1].vRot	=	<<-0.000000,0.000000,-36.669292>>
			
			constObjs[cobj_floor2_barrier2].model	=	prop_barrier_work06a
			constObjs[cobj_floor2_barrier2].vpos	=	<<872.932312,-1721.566162,29.182261>>
			constObjs[cobj_floor2_barrier2].vRot	=	<<-0.000000,0.000000,-80.787041>>
			
			constObjs[cobj_roof2_light1].model		=	prop_worklight_03a
			constObjs[cobj_roof2_light1].vpos		=	<<912.577271,-1707.198120,41.968742>>
			constObjs[cobj_roof2_light1].vRot		=	<<0.042037,0.105869,-25.761972>>
			
			constObjs[cobj_roof2_bricks2].model		=	prop_conc_blocks01b
			constObjs[cobj_roof2_bricks2].vpos		=	<<908.721130,-1709.194336,41.939037>>
			constObjs[cobj_roof2_bricks2].vRot		=	<<-0.000000,0.000000,22.345350>>
			
			constObjs[cobj_roof2_gen].model			=	prop_generator_02a
			constObjs[cobj_roof2_gen].vpos			=	<<903.916809,-1713.888428,41.744926>>
			constObjs[cobj_roof2_gen].vRot			=	<<0.370807,-0.019272,-32.074944>>
			
			constObjs[cobj_roof2_light2].model		=	prop_worklight_04c
			constObjs[cobj_roof2_light2].vpos		=	<<903.121155,-1712.610107,42.679977>>
			constObjs[cobj_roof2_light2].vRot		=	<<0.015474,-0.008173,-66.029747>>
			
			constObjs[cobj_roof2_light3].model		=	prop_worklight_04d
			constObjs[cobj_roof2_light3].vpos		=	<<898.710571,-1712.316895,41.928101>>
			constObjs[cobj_roof2_light3].vRot		=	<<0.000234,-0.010211,40.727936>>
			
			constObjs[cobj_roof3_light1].model		=	prop_worklight_04b
			constObjs[cobj_roof3_light1].vpos		=	<<939.225891,-1703.688599,42.425728>>
			constObjs[cobj_roof3_light1].vRot		=	<<-0.096283,-0.013501,-163.893478>>
			
			constObjs[cobj_roof3_lightbox].model	=	prop_worklight_04a
			constObjs[cobj_roof3_lightbox].vpos		=	<<940.551025,-1702.150024,41.494343>>
			constObjs[cobj_roof3_lightbox].vRot		=	<<0.000029,0.000288,-94.455887>>
			
			constObjs[cobj_roof3_hardhat1].model	= 	prop_tool_hardhat
			constObjs[cobj_roof3_hardhat1].vpos		=	<<937.240662,-1699.237061,42.398090>>
			constObjs[cobj_roof3_hardhat1].vRot		=	<<4.651952,-0.133757,18.810469>>
			
			constObjs[cobj_roof3_cementMixer].model	=	prop_cementmixer_02a
			constObjs[cobj_roof3_cementMixer].vpos	=	<<937.366394,-1688.932861,41.061703>>
			constObjs[cobj_roof3_cementMixer].vRot	=	<<-0.068064,-0.057211,-125.490036>>
			
			constObjs[cobj_roof3_cementStack].model	=	prop_cons_cements01
			constObjs[cobj_roof3_cementStack].vpos	=	<<938.679077,-1687.393433,41.143913>>
			constObjs[cobj_roof3_cementStack].vRot	=	<<-0.000137,0.000294,-26.411013>>
			
			constObjs[cobj_roof3_gen].model			=	prop_generator_01a
			constObjs[cobj_roof3_gen].vpos			=	<<947.172974,-1676.223389,41.062077>>
			constObjs[cobj_roof3_gen].vRot			=	<<-0.000000,0.000000,21.199436>>
			
			constObjs[cobj_roof3_light2].model		=	prop_worklight_04d
			constObjs[cobj_roof3_light2].vpos		=	<<947.355164,-1677.839844,41.520042>>
			constObjs[cobj_roof3_light2].vRot		=	<<-0.000237,-0.026868,-67.621597>>
			
			constObjs[cobj_roof3_light3].model		=	prop_worklight_02a
			constObjs[cobj_roof3_light3].vpos		=	<<942.718811,-1678.550171,42.045441>>
			constObjs[cobj_roof3_light3].vRot		=	<<0.039819,-0.023036,80.240295>>
			
			constObjs[cobj_roof4_light1].model		=	prop_worklight_04c
			constObjs[cobj_roof4_light1].vpos		=	<<883.07269, -1686.47632, 46.13421>>
			constObjs[cobj_roof4_light1].vRot		=	<<0.085501,-0.010684,41.900021>>
			
//			constObjs[cobj_roof4_gen].model			=	prop_generator_02a
//			constObjs[cobj_roof4_gen].vpos			=	<<883.917236,-1685.532593,46.408543>>
//			constObjs[cobj_roof4_gen].vRot			=	<<0.347746,0.007220,0.011423>>
			
			constObjs[cobj_roof4_bitsbox].model		=	prop_partsbox_01
			constObjs[cobj_roof4_bitsbox].vpos		=	<<888.877502,-1684.219604,46.130379>>
			constObjs[cobj_roof4_bitsbox].vRot		=	<<0.000367,0.028443,26.748081>>
			
			constObjs[cobj_roof4_toolbox].model		=	prop_tool_box_01
			constObjs[cobj_roof4_toolbox].vpos		=	<<893.762451,-1681.357422,47.102341>>
			constObjs[cobj_roof4_toolbox].vRot		=	<<0.029734,0.001302,29.545460>>
			
			int objs
			for objs = 0 to enum_to_int(MAX_NUM_CONST_OBJS) - 1		
				if constObjs[objs].model 	= prop_pipes_conc_02
					constObjs[objs].dupType	= dups_PIPES
				endif
				if constObjs[objs].model 	= prop_tool_hardhat
					constObjs[objs].dupType	= dups_hat
				endif			
				if constObjs[objs].model 	= prop_partsbox_01
					constObjs[objs].dupType	= dups_partsbox
				endif
				if constObjs[objs].model 	= prop_tool_box_01
					constObjs[objs].dupType	= dups_toolbox01
				endif
				if constObjs[objs].model 	= PROP_WORKLIGHT_03B
					constObjs[objs].dupType	= dups_light3b
				endif
				if constObjs[objs].model 	= PROP_WORKLIGHT_02a
					constObjs[objs].dupType	= dups_light2a
				endif
				if constObjs[objs].model 	= prop_generator_01a
					constObjs[objs].dupType	= dups_gen1a
				endif
				if constObjs[objs].model 	= prop_worklight_04a
					constObjs[objs].dupType	= dups_light4a
				endif
				if constObjs[objs].model 	= prop_barrier_work06a
					constObjs[objs].dupType	= dups_barrier
				endif
				if constObjs[objs].model 	= prop_generator_02a
					constObjs[objs].dupType	= dups_gen2a
				endif
				if constObjs[objs].model 	= prop_worklight_04c
					constObjs[objs].dupType	= dups_light4c
				endif
				if constObjs[objs].model 	= prop_worklight_04d
					constObjs[objs].dupType	= dups_light4d
				endif
			endfor
			bloadCar1		= false
			bCreateCar1		= false
			bloadMixer		= false
			bCreateMixer	= false
			bloadForklift	= false
			bCreateForklift	= false
			iconstructStage++			
		break 
		case 1
			if bStartConstructionChecks
				int obj1
				for obj1 = 0 to enum_to_int(MAX_NUM_CONST_OBJS) - 1				
					if not constObjs[obj1].bloaded
						if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),constObjs[obj1].vpos) <= constObjs[obj1].fcreateDist
							REQUEST_MODEL(constObjs[obj1].model)
							constObjs[obj1].bloaded = true
						endif
					else					
						if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),constObjs[obj1].vpos) > (constObjs[obj1].fcreateDist + 20)
							constObjs[obj1].bloaded = false						
						elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),constObjs[obj1].vpos) <= constObjs[obj1].fcreateDist			
							if not DOES_ENTITY_EXIST(constObjs[obj1].id)
								if HAS_MODEL_LOADED(constObjs[obj1].model)						
									if GET_CLOSEST_OBJECT_OF_TYPE(constObjs[obj1].vpos,0.3,constObjs[obj1].model) = null
										if obj1 = enum_to_int(cobj_floor_hat1)
										or obj1 = enum_to_int(cobj_floor_hat2)
										or obj1 = enum_to_int(cobj_floor_toolbox)
										or obj1 = enum_to_int(cobj_floor_torch)									
											if DOES_ENTITY_EXIST(constObjs[cobj_floor_wood2].id)										
												constObjs[obj1].id = CREATE_OBJECT_NO_OFFSET(constObjs[obj1].model,constObjs[obj1].vpos)
												SET_ENTITY_ROTATION(constObjs[obj1].id,constObjs[obj1].vRot)
												ACTIVATE_PHYSICS(constObjs[obj1].id)
											endif
										else
											constObjs[obj1].id = CREATE_OBJECT_NO_OFFSET(constObjs[obj1].model,constObjs[obj1].vpos)
											SET_ENTITY_ROTATION(constObjs[obj1].id,constObjs[obj1].vRot)
											ACTIVATE_PHYSICS(constObjs[obj1].id)
										endif
									else
										constObjs[obj1].id = GET_CLOSEST_OBJECT_OF_TYPE(constObjs[obj1].vpos,2,constObjs[obj1].model)
									endif
								endif
							endif
						endif
					endif
				endfor
				for obj1 = 0 to enum_to_int(MAX_NUM_CONST_OBJS) - 1
					if not constObjs[obj1].bloaded 
					and GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),constObjs[obj1].vpos) > (constObjs[obj1].fcreateDist + 10)	
						if constObjs[obj1].dupType = dups_none
							SET_MODEL_AS_NO_LONGER_NEEDED(constObjs[obj1].model)
							SAFE_RELEASE_OBJECT(constObjs[obj1].id)
						else
							bool bcleanup
							bcleanup = true
							int obj2
							for obj2 = 0 to enum_to_int(MAX_NUM_CONST_OBJS) - 1
								if obj2 != obj1
									if constObjs[obj1].dupType = constObjs[obj2].dupType
									and constObjs[obj2].bloaded
										bcleanup = false
									endif
								endif
							endfor
							if bcleanup
								SET_MODEL_AS_NO_LONGER_NEEDED(constObjs[obj1].model)
								SAFE_RELEASE_OBJECT(constObjs[obj1].id)
							else
								SAFE_RELEASE_OBJECT(constObjs[obj1].id)
							endif
						endif
					endif
				endfor
				
				if not bLoadCar1
					if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vMafiaCar) < 55
						request_model(FUGITIVE)
						bLoadCar1 = true
						bCreateCar1 = true					
					endif
				else
					if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vMafiaCar) < 55
						if bCreateCar1
							if HAS_MODEL_LOADED(FUGITIVE)
								if not isentityalive(vehs[mvf_car1].id)
									CLEAR_AREA_OF_VEHICLES(vMafiaCar,2)
									vehs[mvf_car1].id = CREATE_VEHICLE(FUGITIVE,vMafiaCar, hMafiaCar)
									SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_car1].id,0)	
									bCreateCar1 = false
								endif
							endif
						endif
					elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vMafiaCar) > 60
						SET_MODEL_AS_NO_LONGER_NEEDED(FUGITIVE)
						SAFE_RELEASE_VEHICLE(vehs[mvf_car1].id)
						bLoadCar1 = false
					endif
				endif
				
				if not bLoadMixer
					if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<956.9180, -1693.6169, 28.2963>>) <= 60
						REQUEST_MODEL(MIXER)
						bLoadMixer = true
						bCreateMixer = true
					endif
				else
					if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<956.9180, -1693.6169, 28.2963>>) <= 60
						if bCreateMixer
							if HAS_MODEL_LOADED(mixer)
								if not isentityalive(vehs[mvf_mixer].id)
									CLEAR_AREA_OF_VEHICLES(<<956.9180, -1693.6169, 28.2963>>,2)
									vehs[mvf_mixer].id = CREATE_VEHICLE(MIXER,<<956.9180, -1693.6169, 28.2963>>, 177.4760)
									bCreateMixer = false
								endif
							endif
						endif					
					elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<956.9180, -1693.6169, 28.2963>>) > 65
						SET_MODEL_AS_NO_LONGER_NEEDED(mixer)
						SAFE_RELEASE_VEHICLE(vehs[mvf_mixer].id)
						bLoadMixer = false
					endif
				endif
				
				if not bLoadForklift
					if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<966.4474, -1720.8872, 29.6126>>) <= 45
						request_model(Forklift)
						bLoadForkLift = true
						bCreateForkLift = true
					endif
				else
					if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<966.4474, -1720.8872, 29.6126>>) <=45
						if bCreateForklift
							if HAS_MODEL_LOADED(Forklift)
								if not isentityalive(vehs[mvf_forklift].id)
									CLEAR_AREA_OF_VEHICLES(<<966.4474, -1720.8872, 29.6126>>,2)
									vehs[mvf_forklift].id = CREATE_VEHICLE(FORKLIFT,<<966.4474, -1720.8872, 29.6126>>, 65.3199)
									bCreateForkLift = false
								endif	
							endif	
						endif		
					elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<966.4474, -1720.8872, 29.6126>>) > 50
						SET_MODEL_AS_NO_LONGER_NEEDED(Forklift)
						SAFE_RELEASE_VEHICLE(vehs[mvf_Forklift].id)
						bLoadForklift = false
					endif
				endif
			endif
		break
	endswitch
endproc
proc mission_checks()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT bVehStatsGrabbed
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF vehTemp != vehs[mvf_heli].id
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTemp, SOL1_CAR_DAMAGE)
			ENDIF
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehTemp, SOL1_MAX_SPEED)
			bVehStatsGrabbed = TRUE
		ENDIF
	ELSE
		IF bVehStatsGrabbed
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, SOL1_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, SOL1_MAX_SPEED)
			bVehStatsGrabbed = FALSE
		ENDIF
	ENDIF
	
	IF NOT bHeliStatsGrabbed
		IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
		AND IS_VEHICLE_DRIVEABLE(vehs[mvf_heli].id)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[mvf_heli].id, SOL1_HELI_DAMAGE)
			bHeliStatsGrabbed = TRUE
		ENDIF
	ENDIF
	
	death_checks()
	//audio 
	if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		bpauseconv = false
	endif
	if isentityalive(mike())
		AUDIO_dialogue(int_to_enum(MSF_MISSION_STAGE_FLAGS,mission_stage))
	endif
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(player_id())
	
	if mission_stage = enum_to_int(msf_1_Stealth_to_roof)
	or( mission_stage = enum_to_int(msf_0_get_to_warehouse) and mission_substage > 2)	
	and not IS_CUTSCENE_PLAYING()
		manage_Stealth_stage()			
	endif
	
	if mission_stage = enum_to_int(msf_1_Stealth_to_roof)
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_SERIOUS_DANGER)
		SUPPRESS_SHOCKING_EVENT_TYPE_NEXT_FRAME(SHOCKING_EVENT_DANGEROUS)		
		manage_GIANNI()
	endif
	
	Manage_Doors()
	Manage_Stealth_Door_Locks()
	
	if mission_stage >= enum_to_int(msf_1_Stealth_to_roof)
	and mission_stage < enum_to_int(msf_3_get_to_sol)
		if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vheliRoof) >	130
			MISSION_FAILED(mff_left_Area)
		elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(), vheliRoof) > 120
			if not bFailWarning
				PRINT_NOW("SOL1_RTNM",DEFAULT_GOD_TEXT_TIME,1)
				bfailWarning = true
			endif
		else
			bfailWarning = false
		endif
	elif  mission_stage = enum_to_int(msf_3_get_to_sol)
		if not IS_CUTSCENE_PLAYING()
		and mission_substage < 6
			if GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),Milton()) >	80		
			or GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),anton())  >	80		
				MISSION_FAILED(mff_left_peds)
			elif GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),Milton()) >	60	
				if not bfailMiltonWarning	
					PRINT_NOW("SOl1_RTNMILT",DEFAULT_GOD_TEXT_TIME,1)
					bfailMiltonWarning = true
				endif
			elif GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),anton())  >	60
				if not bfailAntonWarning
					PRINT_NOW("SOl1_RTNANT",DEFAULT_GOD_TEXT_TIME,1)
					bfailAntonWarning = true
				endif
			else
				bfailAntonWarning = FALSE
				bfailMiltonWarning = FALSE
			endif
		endif
	endif
	
	// update partners id's
	Mafia[maf_car].partner				= mafia[maf_car_talking].id
	Mafia[maf_car_talking].partner		= mafia[maf_car].id		
	
	Mafia[maf_walkway_door].partner		= mafia[maf_worker_walkway].id
	Mafia[maf_worker_walkway].partner	= mafia[maf_walkway_door].id
		
	vector playerCoord = GET_ENTITY_COORDS(mike())
	
	int ped
	for ped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) -1
		if isentityalive(mafia[ped].id)
			//attach clipboards and objects to the peds left hand
			if DOES_ENTITY_EXIST(mafia[ped].obj)
			and not mafia[ped].bObjAttached
				if DOES_ENTITY_HAVE_PHYSICS(mafia[ped].obj)
					ATTACH_ENTITY_TO_ENTITY(mafia[ped].obj,mafia[ped].id
											,GET_PED_BONE_INDEX(mafia[ped].id,BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
					mafia[ped].bObjAttached = true						
				endif
			endif
			
			if IS_ENTITY_PLAYING_ANIM(mafia[ped].id,AnimDict_PHONE,animPhone)
				if not mafia[ped].bOnPhone 					
					mafia[ped].objphone = CREATE_OBJECT(modelPhone, GET_ENTITY_COORDS(mafia[ped].id))
					ATTACH_ENTITY_TO_ENTITY(mafia[ped].objPhone,mafia[ped].id
											,GET_PED_BONE_INDEX(mafia[ped].id,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)											
					mafia[ped].bOnPhone = true						
				endif
			endif
			
			//blip the peds that close to mike			
			vector pedCoord = GET_ENTITY_COORDS(mafia[ped].id)
			float fzDif = pedcoord.z - playerCoord.z
			if fzDif < 0
				fzDif *= -1
			endif
			
			if GET_DISTANCE_BETWEEN_ENTITIES(mike(),mafia[ped].id) < mafia[ped].fBlipRange
			and fzDif < 4.5				
				mafia[ped].bForceBlip = true				
			else
				mafia[ped].bForceBlip = false
			endif
		endif
		#if is_debug_build
			mafia[ped].sDebug = enum_to_int(mafia[ped].sit)
		#endif
	endfor
	
	if isentityalive(player_ped_id())
		PLACE_CONSTRUCTION()
	endif
	
	#if	is_debug_build
		wAI_stage = enum_to_int(eAI_stage)	
		debug_noise = GET_PLAYER_CURRENT_STEALTH_NOISE(player_id())
		Display_Placeholder()
	#endif
	
endproc
proc mission_setup()
	
	peds[mpf_mike].id = PLAYER_PED_ID()
	
	remove_relationship_group(rel_group_enemies)	
	remove_relationship_group(rel_friends)
	add_relationship_group("enemies", rel_group_enemies)
	ADD_RELATIONSHIP_GROUP("rel_Friends", REL_friends)	
	
//haters		
	set_relationship_between_groups(acquaintance_type_ped_hate,relgrouphash_player,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_enemies,relgrouphash_player)
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_friends,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_enemies,rel_friends)
//friends
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_friends)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_friends,RELGROUPHASH_PLAYER)
	set_relationship_between_groups(acquaintance_type_ped_like,rel_group_enemies,rel_group_enemies)	
	request_additional_text("SOL1", mission_text_slot)
	
	if is_player_playing(player_id())
		set_player_control(player_id(), true)	
	endif			
	
	enable_dispatch_service(dt_ambulance_department,false)
	enable_dispatch_service(dt_fire_department, 	false)
	set_wanted_level_multiplier(0.3)
	CLEAR_PLAYER_WANTED_LEVEL(player_id())
	
	set_vehicle_model_is_suppressed(FUGITIVE,true)
	set_vehicle_model_is_suppressed(FROGGER,true)
	
	DISABLE_TAXI_HAILING()
	
	DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE,true)	
		
	// Initialise all the doors we're gonna be using
	Doors_Initialise()	
		
	if DOES_SCENARIO_GROUP_EXIST("SOLOMON_GATE")
		if IS_SCENARIO_GROUP_ENABLED("SOLOMON_GATE")
			SET_SCENARIO_GROUP_ENABLED("SOLOMON_GATE",false)
		endif
	endif
	
	SET_AMBIENT_ZONE_STATE("AZ_SOL_1_FACTORY_AREA_CONSTRUCTIONS", TRUE, TRUE)
	
	SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, FALSE)	
	SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
	
	ADD_SCENARIO_BLOCKING_AREA(<<1038.43750, -1769.24170, 10>>,<<794.32159, -1989.40137, 60>>)
	if isentityalive(GET_MISSION_START_VEHICLE_INDEX())
		if IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_MISSION_START_VEHICLE_INDEX()))
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1106.3323, -443.0989, 34.3837>>, 116.5404)
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1034.93652, -497.61383, 35.33385>>,<<-1009.55530, -483.44385, 42.02816>>,20,<<-1106.3323, -443.0989, 34.3837>>, 116.5404, FALSE) //BUG FIX 1399301 Added FALSE to clean up area
		ELSE
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1024.6364, -497.7582, 35.9242>>, 86.6830)
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1034.93652, -497.61383, 35.33385>>,<<-1009.55530, -483.44385, 42.02816>>,20,<<-1024.5975, -496.4365, 35.9291>>, 86.4749, FALSE) //BUG FIX 1399301 Added FALSE to clean up area
		ENDIF
	ENDIF
	
	if is_replay_in_progress()	
	or IS_REPEAT_PLAY_ACTIVE()
		if IS_REPLAY_IN_PROGRESS()
			iskiptostage = get_replay_mid_mission_stage()
			if g_bshitskipaccepted 
				iskiptostage++			
			endif
			if iskiptostage >= 4
				iSkipToStage = 4
			endif			
		elif IS_REPEAT_PLAY_ACTIVE()
			iSkipToStage = 0
		endif
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
		ENDIF
		bdoskip = true		
	else		
		PRINTLN("Start mission Loading")
		WHILE NOT Update_Skip_Streaming(sAssetData)
			WAIT(0)				
		ENDWHILE						
		PRINTLN("End mission Loading")	
		
		g_replay.iReplayInt[0] = 0
		g_replay.iReplayInt[1] = 0		
		
		set_replay_mid_mission_stage_with_name(0,"stage 0: Get Heli")
		mission_stage = enum_to_int(msf_0_get_to_warehouse)	
	endif
	
	mission_substage = stage_entry
	
	if is_player_playing(player_id())
		set_player_control(player_id(), true)	
	endif		
		
endproc

// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
Proc Run_Init_CutScene()

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	#ENDIF
	
	IF NOT b_cutscene_loaded
		if NOT IS_CUTSCENE_ACTIVE()
			SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, FALSE)	
			REQUEST_CUTSCENE("SOL_1_INT_ALT")		
			WAIT(0)
		endif
		IF HAS_CUTSCENE_LOADED()
			//pre mission setup			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
			SET_ALL_EXIT_STATES()
			set_peds_CS_OUTFITS()
			START_CUTSCENE()			
			b_cutscene_loaded = TRUE			
		ENDIF	
	ENDIF
	
endproc
proc ST_0_GET_TO_WAREHOUSE()
	
	switch mission_substage
		case STAGE_ENTRY	
			if IS_REPLAY_IN_PROGRESS()		
				Load_Asset_Model(sAssetData,CAVALCADE2)
				load_Asset_model(sAssetdata,COQUETTE)
				bStartConstructionChecks = false
				mission_substage++
			else
				Run_Init_CutScene()
				IF b_cutscene_loaded
					if IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					endif
					Load_Asset_Model(sAssetData,CAVALCADE2)
					load_Asset_model(sAssetData,COQUETTE)
					bStartConstructionChecks = false	
					mission_substage++
				endif	
			endif			
		break
		case 1	
			IF b_cutscene_loaded
			and IS_CUTSCENE_PLAYING()
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SOLOMON_1)//BUG FIX 1402789
				SET_SRL_POST_CUTSCENE_CAMERA(<<-1014.6,-481.4,37.9>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0,121.2896>>))		
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1037.91162, -495.88333, 34>>,<<-1016.92767, -471.04614, 37>>,false)
				CLEAR_AREA_OF_VEHICLES(<<-1028.38049, -486.24539, 37.13475>>,15)				
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				b_cutscene_loaded = false
			endif
			
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")				 
			
				REPLAY_STOP_EVENT()
			
				println("mike")				
				bcs_Mike = true				
			endif			
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				println("cam")	
				bcs_cam = true
			endif
			
			if (bcs_Mike and bcs_cam)
			or IS_REPLAY_IN_PROGRESS()	
				if IS_SCREEN_FADED_OUT()
				or IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				
				//CAVALCADE2
				if not IS_ANY_VEHICLE_NEAR_POINT(<<-1036.98743, -491.78458, 35.57412>>,1.8)
					vehs[mvf_cavalcade2].id = CREATE_VEHICLE(CAVALCADE2,<<-1036.5834, -492.1475, 35.5822>>, 209.0078)
				endif
				Unload_Asset_Model(sAssetData,cavalcade2)
				//COQUETTE
				if not IS_ANY_VEHICLE_NEAR_POINT(<<-1033.80017, -489.91208, 35.83315>>,1.8)
					vehs[mvf_coquette].id = create_vehicle(coquette,<<-1033.6208, -490.2622, 35.8286>>, 210.3952)
				endif
				unload_asset_model(sAssetData,coquette)
				
				SET_ENTITY_COORDS(mike(),<<-1016.53870, -482.88400, 36.07111>>)
				SET_ENTITY_HEADING(mike(),121.31)	
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()	
				ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"SOLOMON")
				ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL")	
				
				bStageLoaded = false
				bStageLoaded2 = false
				
				TASK_GO_STRAIGHT_TO_COORD(MIKE(),<<-1021.03033, -485.61594, 36.13899 >>,PEDMOVE_WALK)
				FORCE_PED_MOTION_STATE(MIKE(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
				SET_PED_MIN_MOVE_BLEND_RATIO(MIKE(),PEDMOVE_WALK)
				
				prep_stop_cutscene(true)
				RENDER_SCRIPT_CAMS(false,false,0,false,true)
				
				NEW_LOAD_SCENE_STOP()
				REMOVE_CUTSCENE()			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)				
				SET_ALL_EXIT_STATES()
				DISPLAY_RADAR(true)
				display_hud(true)
				b_cutscene_loaded = false
				bstreamloaded = false
				bLoadMusicStream = false
				ADD_CONTACT_TO_PHONEBOOK(CHAR_SOLOMON,MICHAEL_BOOK,true)
				init_enemy_data()
				mission_substage++				
			endif
		break 
		case 2 // *dependent for playin AI / sol phone call*
			
			if not bStageLoaded
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<970.6778, -1754.9475, 30.0644>>)	< 200		
					SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, TRUE)
					LOAD_ASSET_MODEL(sAssetData,S_M_Y_CONSTRUCT_01)
					Load_Asset_AnimDict(sAssetData,animDict_kneel_enter)
					Load_Asset_AnimDict(sAssetData,animDict_kneel_Idle)
					Load_Asset_AnimDict(sAssetData,animDict_kneel_exit)
					Load_Asset_AnimDict(sAssetData,AnimDict_gestures)
					Load_Asset_AnimDict(sAssetData,AnimDict_flinch)
					Load_Asset_AnimDict(sAssetData,	AnimDict_ClipBoard)
					Load_Asset_AnimDict(sAssetData,	AnimDict_PHONE)
					LOAD_ASSET_RECORDING(sAssetData,001,"SOL1REC")
					REQUEST_CLIP_SET("MOVE_M@CLIPBOARD")
					bLoadMusicStream = true
					if isentityalive(vehs[mvf_coquette].id)
						if not IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_coquette].id)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_coquette].id)
						endif
					endif
					if isentityalive(vehs[mvf_cavalcade2].id)
						if not IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_cavalcade2].id)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_cavalcade2].id)
						endif
					endif
					bStageLoaded = true
				endif
			else
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<970.6778, -1754.9475, 30.0644>>)	< 150
				and HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)	
				and HAS_CLIP_SET_LOADED("MOVE_M@CLIPBOARD")
					if not isentityalive(mafia[maf_car].id)
					and not mafia[maf_car].bkilled
						create_Mafiaenemy(maf_car,24)					
					endif
					if not isentityalive(mafia[maf_car_talking].id)
					and not mafia[maf_car_talking].bkilled
						create_Mafiaenemy(maf_car_talking,24)
					endif		
					bStartConstructionChecks = true
				elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<970.6778, -1754.9475, 30.0644>>)> 300
					SAFE_DELETE_PED(mafia[maf_car].id)
					SAFE_DELETE_PED(mafia[maf_car_talking].id)
					LOAD_ASSET_MODEL(sAssetData,S_M_Y_CONSTRUCT_01)
					Unload_Asset_Anim_Dict(sAssetData,animDict_kneel_enter)
					unLoad_Asset_Anim_Dict(sAssetData,animDict_kneel_Idle)
					unLoad_Asset_Anim_Dict(sAssetData,animDict_kneel_exit)
					unLoad_Asset_Anim_Dict(sAssetData,AnimDict_gestures)
					unLoad_Asset_Anim_Dict(sAssetData,AnimDict_flinch)
					unLoad_Asset_Anim_Dict(sAssetData,AnimDict_ClipBoard)
					unLoad_Asset_Anim_Dict(sAssetData,AnimDict_PHONE)
					unLOAD_ASSET_RECORDING(sAssetData,001,"SOL1REC")
					REMOVE_CLIP_SET("MOVE_M@CLIPBOARD")
					bStageLoaded = false
				endif
			endif
			if not bStageLoaded2
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<970.6778, -1754.9475, 30.0644>>)	< DEFAULT_CUTSCENE_LOAD_DIST
					LOAD_ASSET_MODEL(sAssetData,FROGGER)
					LOAD_ASSET_MODEL(sAssetData,G_M_M_ARMGOON_01)					
					load_asset_model(sAssetData,modelpilot)
					load_asset_model(sAssetData,modelrocco)
					load_asset_model(sAssetData,modelActor)
					load_asset_model(sAssetData,modelDirector)
					load_Asset_model(sAssetData,modelGianni)
					Load_Asset_Model(sAssetData,modelclipboard)					
					NEW_LOAD_SCENE_START(<<966.79,-1749.25,35.66>>,<<-0.58,0.81,-0.13>>,65)
					bStageLoaded2 = true
				endif
			else
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<970.6778, -1754.9475, 30.0644>>)	> DEFAULT_CUTSCENE_UNLOAD_DIST
					unLOAD_ASSET_MODEL(sAssetData,FROGGER)
					unLOAD_ASSET_MODEL(sAssetData,G_M_M_ARMGOON_01)					
					unload_asset_model(sAssetData,modelpilot)
					unload_asset_model(sAssetData,modelrocco)
					unload_asset_model(sAssetData,modelActor)
					unload_asset_model(sAssetData,modelDirector)
					unload_Asset_model(sAssetData,modelGianni)
					unLoad_Asset_Model(sAssetData,modelclipboard)
					NEW_LOAD_SCENE_STOP()
					bStageLoaded2 = false
				endif
			endif
			
			if not IS_AUDIO_SCENE_ACTIVE("SOL_1_DRIVE_TO_EAST_LS")
				if IS_PED_IN_ANY_VEHICLE(mike())
					START_AUDIO_SCENE("SOL_1_DRIVE_TO_EAST_LS")
				endif
			endif
			
			if bLoadMusicStream 
				if LOAD_STREAM("Helicopter_Stream","SOL_1_MR_RICHARDS_SOUNDS")
					bstreamloaded = true
					bLoadMusicStream = false
				endif
			endif
			
			if IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData,<<970.6778, -1754.9475, 30.0644>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,"SOL1_ELS",true)
			or (DOES_BLIP_EXIST(sLocatesData.LocationBlip) and IS_ENTITY_IN_ANGLED_AREA(mike(),<<976.58124, -1753.84753, 33>>,<<965.19244, -1753.20862, 28>>,8))
				if IS_PED_IN_ANY_VEHICLE(mike())
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(mike()))				
				endif				
				CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
				if IS_AUDIO_SCENE_ACTIVE("SOL_1_DRIVE_TO_EAST_LS")
					STOP_AUDIO_SCENE("SOL_1_DRIVE_TO_EAST_LS")
				endif
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1037.91162, -495.88333, 34>>,<<-1016.92767, -471.04614, 37>>,true)
				HANG_UP_AND_PUT_AWAY_PHONE()
				iDialogue_Stage = 4
				iskipCS 		= 0
				set_replay_mid_mission_stage_with_name(1,"stage 1:  Stealth to roof")
				mission_substage++
			endif
		break
		case 3
			if IS_PED_IN_ANY_VEHICLE(mike())
				if IS_VEHICLE_ALMOST_STOPPED(GET_VEHICLE_PED_IS_IN(mike()))
					vehs[mvf_Replay_car].id = GET_VEHICLE_PED_IS_IN(mike())
					SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_Replay_car].id)
					idelay = get_game_timer()
					mission_substage++
				endif
			else	
				idelay = get_game_timer()
				mission_substage++
			endif
		break
		case 4			
			if not bstreamloaded
				if LOAD_STREAM("Helicopter_Stream","SOL_1_MR_RICHARDS_SOUNDS")
					bstreamloaded = true
				endif
			endif
			
			if (get_Game_timer() - idelay > 1000	and bstreamloaded)
			or get_Game_timer() - idelay > 	40000
				
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 13.0, REPLAY_IMPORTANCE_HIGHEST)
				SET_HD_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()),10)
				//cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<968.5402, -1753.5344, 32.8193>>, <<-2.5886, 0.0000, 31.2658>>,30.0347)
				cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<964.352600,-1745.761597,35.290886>>,<<5.967328,0.174521,36.580875>>,30.034700) //BUG FIX 1201813
				set_cam_active(cameraIndex,true)
				//SET_CAM_PARAMS(cameraIndex,<<969.5601, -1752.7874, 39.0872>>, <<5.7838, -0.0000, 40.9145>>,30.0347,10000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(cameraIndex,<<963.869385,-1745.322632,37.706657>>,<<9.215145,0.174522,34.265663>>,30.034700,9000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR) //BUG FIX 1201813
				prep_start_cutscene(false,<<973.6251, -1761.7423, 37.0542>>,true,false,true,0,SPC_NONE,false)			
				CLEAR_AREA(<<970.6778, -1754.9475, 30.0644>>,150,true)
				CLEAR_AREA(vheliRoof,150,true)
				//heli
				vehs[mvf_heli].id = CREATE_VEHICLE(FROGGER,<<977, -1658.4874,60>>, 74)
				SET_FORCE_HD_VEHICLE(vehs[mvf_heli].id, true)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id,0)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_heli].id,false)
				PLAY_STREAM_FROM_VEHICLE(vehs[mvf_heli].id)
				
				//rocco
				peds[mpf_rocco].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,modelrocco,VS_FRONT_RIGHT)
				// pilot
				peds[mpf_pilot].id = create_ped_inside_vehicle(vehs[mvf_heli].id,PEDTYPE_MISSION,modelpilot)
				if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id,1,"SOL1REC")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id,14500) //15000 BUG FIX 1201813
					SET_PLAYBACK_SPEED(vehs[mvf_heli].id,-0.8)
					SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)
				endif
				//more peds
				if not isentityalive(Gianni())
					peds[mpf_Gianni].id = CREATE_PED(pedtype_mission,modelGianni,vGianni,hGianni)
					SETUP_Gianni()					
				endif
				if not isentityalive(mafia[maf_worker_vent].id)
					create_Mafiaenemy(maf_worker_vent,14)
				endif
				if not isentityalive(mafia[maf_worker_alone].id)
					create_Mafiaenemy(maf_worker_alone,24)
				endif
				//actor
				peds[mpf_milton].id = CREATE_PED(PEDTYPE_MISSION,modelActor, <<914.52319, -1699.65991, 50.13501>> ) //<<915.56940, -1698.89111, 50.13412>> //BUG FIX 1201813  swapping their start coords round so they don't cross each other.
				SETUP_Milton()
				//director
				peds[mpf_Anton].id = CREATE_PED(PEDTYPE_MISSION,modelDirector, <<916.29724, -1699.75378, 50.13508>>) //<<913.85162, -1698.85193, 50.13412>> //BUG FIX 1201813
				SETUP_Anton()
				//veh 
				if isentityalive(vehs[mvf_Replay_car].id)
					SET_ENTITY_COORDS(vehs[mvf_Replay_car].id,<<971.4699, -1754.8250, 30.0953>>)
					set_entity_heading(vehs[mvf_Replay_car].id, 87.1515)
					SET_MISSION_LAST_VEHICLE_AS_VEHICLE_GEN(<<971.4699, -1754.8250, 30.0953>>,87.1515)
				endif
				if not IS_AUDIO_SCENE_ACTIVE("SOL_1_COMPOUND_OVERVIEW_CUTSCENE")
					START_AUDIO_SCENE("SOL_1_COMPOUND_OVERVIEW_CUTSCENE")
				endif				
				TRIGGER_MUSIC_EVENT("SOL1_BEGIN")
				REQUEST_CUTSCENE("Sol_1_mcs_1_concat")
				SET_ALL_EXIT_STATES()
				set_peds_CS_OUTFITS()
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_TORSO,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_LEG,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_feet,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_JBIB,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_TORSO,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_LEG,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_feet,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_JBIB,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_TORSO,0,0,CSB_ANTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_LEG,0,0,CSB_ANTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_TORSO,0,0,U_M_Y_ANTONB)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_LEG,0,0,U_M_Y_ANTONB)
				idelay = get_game_timer()
				iskipDelay = get_game_timer() 
				CLEAR_PLAYER_WANTED_LEVEL(player_id())			
				set_wanted_level_multiplier(0.0)
				mission_substage++	
			else
				SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
			endif
		break
		case 5			
			if get_game_timer() - idelay > 8000
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_milton].id,<<908.91437, -1689.93665, 50.13943>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_SUPPRESS_EXACT_STOP)
				TASK_LOOK_AT_ENTITY(peds[mpf_milton].id,vehs[mvf_heli].id,-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_Anton].id,<<912.44019, -1691.37793, 50.14446>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_SUPPRESS_EXACT_STOP)
				TASK_LOOK_AT_ENTITY(peds[mpf_Anton].id,vehs[mvf_heli].id,-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
				
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<914.64410, -1689.74878, 50.14659>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, vehs[mvf_heli].id, -1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(Gianni(), seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				SET_CAM_PARAMS(cameraIndex,<<876.181946,-1710.500854,46.637653>>,<<9.872696,0.003638,-59.444042>>,26.791100,0,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)    //BUG FIX 1201813
				camFollow =	CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<874.506042,-1707.566650,48.956787>>,<<5.026361,0.003638,-57.228275>>,26.791100)    //BUG FIX 1201813
				POINT_CAM_AT_ENTITY(camFollow,vehs[mvf_heli].id,<<0,0,0>>)
				SET_CAM_ACTIVE_WITH_INTERP(camFollow,cameraIndex,8000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				idelay = get_game_timer()
				NEW_LOAD_SCENE_STOP()
				mission_substage++
			endif
		break
		case 6
			if get_game_timer() - idelay > 5000
				STOP_CAM_POINTING(cameraIndex)				
				SET_CAM_PARAMS(cameraIndex,<<898.3975, -1664.2729, 52.9379>>, <<-2.6793, 0.8462, -148.0579>>,19.8393,0,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_PARAMS(cameraIndex,<<898.3975, -1664.2687, 52.7121>>, <<-2.6793, 0.8462, -148.0579>>,19.8393,10000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				SET_CAM_ACTIVE(cameraIndex,true)
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
					SET_PLAYBACK_SPEED(vehs[mvf_heli].id,-0.9)
				endif
				idelay = get_game_timer()
				mission_substage++
			endif
		break
		case 7
			IF NOT IS_ENTITY_IN_AIR(vehs[mvf_heli].id)  // BUG FIX 1201813
				idelay = get_game_timer()
				mission_substage++
			ENDIF
		break
		case 8
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()  // BUG FIX 1201813
				STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id) // BUG FIX 1201813
				SAFE_DELETE_PED(peds[mpf_pilot].id) // temp need Gianni
				REGISTER_ENTITY_FOR_CUTSCENE(rocco(),"rocco",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_heli].id,"Main_heli",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(Milton(),"Milton",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(Anton(),"Anton",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(Gianni(),"Rocco_Goon",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				HANG_UP_AND_PUT_AWAY_PHONE()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
		break
		case 9
			if IS_CUTSCENE_PLAYING()				
				STOP_STREAM()
				Unload_Asset_Model(sAssetData,modelrocco)
				Unload_Asset_Recording(sAssetData,1,"SOL1REC")
				prep_stop_cutscene(true)
				STOP_AUDIO_SCENES()
				SET_SRL_POST_CUTSCENE_CAMERA(<<970.5688, -1753.4752, 30.1716>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0,56.0997>>))
				SET_ENTITY_HEADING(player_ped_id(), 38.2203)
				mission_substage++
			endif
		break
		case 10	
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Milton")
				println("MILTON")
				SET_PED_INTO_VEHICLE(Milton(),vehs[mvf_heli].id,VS_BACK_LEFT)
				bcs_actor = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Anton")
				SET_PED_INTO_VEHICLE(Anton(),vehs[mvf_heli].id,VS_BACK_RIGHT)
				bcs_director = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Rocco_Goon")
				println("GOON")
				bcs_goon = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Main_heli")
				SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, FALSE, FALSE) 
				println("HELI")
				bcs_heli = true
			endif
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				println("CAM")
								
				bcs_cam = true
			endif
			
			if 	bcs_actor
			and bcs_goon
			and bcs_heli
			and bcs_cam
			and bcs_director
				
				REPLAY_STOP_EVENT()
				CLEAR_HD_AREA()
				SET_ENTITY_COORDS(player_ped_id(), <<968.7196, -1751.8749, 30.1595>>)
				SET_ENTITY_HEADING(player_ped_id(), 38.2203)
				FORCE_PED_MOTION_STATE(player_ped_id(), MS_AIMING)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_iD(), true)
			
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()				
				mission_set_stage(msf_1_Stealth_to_roof)
				mission_substage = STAGE_ENTRY
			endif
		break
	endswitch
	
	if mission_substage > 4
	and mission_substage < 10
	and not IS_CUTSCENE_PLAYING()
	and get_game_timer() - iskipDelay > 1500
	and IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	or iskipCS = 1
		switch iskipCS
			case 0
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				iskipCS++
			break
			case 1
				if IS_SCREEN_FADED_OUT()
					//plyaer
					SET_ENTITY_COORDS(player_ped_id(), <<968.7196, -1751.8749, 30.1595>>)
					SET_ENTITY_HEADING(player_ped_id(), 38.2203)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(player_ped_iD())
					//heli
					if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id)
					endif					
					SET_ENTITY_COORDS(vehs[mvf_heli].id,vheliRoof)
					SET_ENTITY_HEADING(vehs[mvf_heli].id,hHeliRoof)
					SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, FALSE, FALSE) 
					//peds
					SAFE_DELETE_PED(peds[mpf_rocco].id)
					SAFE_DELETE_PED(peds[mpf_pilot].id)
					SET_PED_INTO_VEHICLE(Milton(),vehs[mvf_heli].id,VS_BACK_LEFT)
					SET_PED_INTO_VEHICLE(Anton(),vehs[mvf_heli].id,VS_BACK_RIGHT)
					//CS
					IF IS_CUTSCENE_ACTIVE()			
						STOP_CUTSCENE_IMMEDIATELY()
						REMOVE_CUTSCENE()	
					ENDIF
					//misc
					Unload_Asset_Model(sAssetData,modelrocco)
					Unload_Asset_Recording(sAssetData,1,"SOL1REC")
					prep_stop_cutscene(true)
					STOP_AUDIO_SCENES()
					STOP_STREAM()
					DESTROY_ALL_CAMS()					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					NEW_LOAD_SCENE_STOP()
					CLEAR_HD_AREA()
					mission_set_stage(msf_1_Stealth_to_roof)
					mission_substage = STAGE_ENTRY
					iskipCS = 0
				endif
			break
		endswitch
	endif
	
clear_players_task_on_control_input(SCRIPT_TASK_GO_STRAIGHT_TO_COORD)

endproc
proc ST_1_STEALTH_TO_ROOF()
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()				
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif		
			if IS_SCREEN_FADED_IN()
				print_now("SOL1_HELI",DEFAULT_GOD_TEXT_TIME,1)
				
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehs[mvf_heli].id)) 
				endif
				
				NEW_LOAD_SCENE_STOP()
				TRIGGER_MUSIC_EVENT("SOL1_GAMEPLAY")
				bFirstAlerted		= false
				ifloorspot 			= 0
				iGianniChat			= 0
				CLEAR_PLAYER_WANTED_LEVEL(player_id())		
				set_wanted_level_multiplier(0.0)
				set_replay_mid_mission_stage_with_name(1,"stage 1:  Stealth to roof")
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
		break
		case 1
			//load rocco or unload on distance
			if GET_DISTANCE_BETWEEN_ENTITIES(mike(),vehs[mvf_heli].id) < 35
				Load_Asset_Model(sAssetData,modelrocco)
				Load_Asset_Model(sAssetData,modelpilot)				
			else
				Unload_Asset_Model(sAssetData,modelrocco)
				unLoad_Asset_Model(sAssetData,modelpilot)
			endif
			
			if GET_DISTANCE_BETWEEN_ENTITIES(mike(),vehs[mvf_heli].id) < 16
			and not bMusic_heliTrigger
				TRIGGER_MUSIC_EVENT("SOL1_HELI_ROOF")
				bMusic_heliTrigger = true
			endif
			if IS_PED_IN_ANY_VEHICLE(player_ped_id())
				if IS_AUDIO_SCENE_ACTIVE("SOL_1_GET_TO_HELI_STEALTH")
					STOP_AUDIO_SCENE("SOL_1_GET_TO_HELI_STEALTH")
				endif	
			else
				if not IS_AUDIO_SCENE_ACTIVE("SOL_1_GET_TO_HELI_STEALTH")
					START_AUDIO_SCENE("SOL_1_GET_TO_HELI_STEALTH")
				endif	
			endif
			
			
			if IS_ENTITY_IN_ANGLED_AREA(mike(),<<911.31165, -1671.00183, 50.13412>>,<<909.20807, -1695.13318, 56.13356>>,18)	
			or IS_ENTITY_IN_ANGLED_AREA(mike(),<<917.82196, -1698.05920, 50.13412>>,<<905.07593, -1696.87073, 56.13356>>,6)//stairs trigger
			or( IS_ENTITY_IN_ANGLED_AREA(mike(),<<902.98291, -1667.34106, 50.13412>>,<<920.77441, -1664.76794, 56.13356>>,8)and not IS_PED_CLIMBING(mike()))	
			or(HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(Gianni(),mike()) 
				and IS_PED_FACING_PED(Gianni(),mike(),50) 
				and IS_ENTITY_IN_ANGLED_AREA(mike(),<<908.51294, -1699.99683, 65>>,<<910.62000, -1663.53870, 50.1>>,18))
				if DOES_BLIP_EXIST(blip_objective)
					REMOVE_BLIP(blip_objective)
				endif
				STOP_AUDIO_SCENES()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				idelay = get_Game_timer()
				mission_substage++
			endif
		break
		case 2
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(convo_struct,0,mike(),"MICHAEL")
				if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_ROC",CONV_PRIORITY_MEDIUM)					
					TASK_LOOK_AT_ENTITY(Gianni(),mike(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)					
					TASK_TURN_PED_TO_FACE_ENTITY(Gianni(),mike(),-1)					
					TASK_LOOK_AT_ENTITY(Anton(),mike(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
					TASK_LOOK_AT_ENTITY(milton(),mike(),-1,SLF_WHILE_NOT_IN_FOV| SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
					
					mission_substage++
				endif
			endif
		break
		case 3
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(convo_struct,4,Gianni(),"GIANNI",true)
				if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_SEES",CONV_PRIORITY_MEDIUM)
					mission_set_stage(msf_2_fight_rocco)
					mission_substage = STAGE_ENTRY
				endif
			endif
		break
	endswitch
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2163422
	
	if mission_substage > 1
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
		if get_game_timer() - idelay > 2500
		//--------------	PUSH IN    --------------	
		
			FLOAT fT2HintCamFollow
			FLOAT fT2HintCamOffset
			FLOAT fT2HintCamFove			
			fT2HintCamFollow 	= 0.45
			fT2HintCamOffset 	= -0.78
			fT2HintCamFove 		= 30.00
			SET_GAMEPLAY_COORD_HINT(<<912.16638, -1683.00232, 51.71074>>,-1,2000)
		    SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fT2HintCamFollow)
		    SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fT2HintCamOffset)
		    SET_GAMEPLAY_HINT_FOV(fT2HintCamFove)		  	
			SET_PLAYER_CONTROL(player_id(),false)
			if IS_ENTITY_IN_ANGLED_AREA(mike(),<<909.97729, -1687.18030, 50.12949>>,<<910.85028, -1676.78320, 53.29437>>,18)
				if IS_SCRIPT_TASK_RUNNING_OR_STARTING(mike(),SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					CLEAR_PED_TASKS(mike())
				endif
			else
				if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(mike(),SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					TASK_FOLLOW_NAV_MESH_TO_COORD(mike(),<<912.34351, -1682.78552, 50.13306>>,PEDMOVE_WALK)
				endif
			endif
			
		//---------------------------------------------	
		endif
	endif
	if isentityalive(Milton())
	and isentityalive(Anton())	
		if mission_substage < 3
			if not IS_PED_HEADTRACKING_PED(Milton(),Gianni())
				TASK_LOOK_AT_ENTITY(Milton(),Gianni(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			endif
			if not IS_PED_HEADTRACKING_PED(Anton(),Gianni())
				TASK_LOOK_AT_ENTITY(Anton(),Gianni(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			endif
		else
			if not IS_PED_HEADTRACKING_PED(Milton(),mike())
				TASK_LOOK_AT_ENTITY(Milton(),mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			endif
			if not IS_PED_HEADTRACKING_PED(Anton(),mike())
				TASK_LOOK_AT_ENTITY(Anton(),mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
			endif
		endif
	endif
endproc
proc ST_2_FIGHT_ROCCO()
	switch mission_substage
		case STAGE_ENTRY								
			// rocco			
			peds[mpf_rocco].id = create_ped(PEDTYPE_MISSION,modelrocco,<<916.03021, -1702.74951, 50.26364>>,-3.90)
			SETUP_ROCCO()
			//create pilot
			peds[mpf_pilot].id = create_ped(PEDTYPE_MISSION,modelpilot,<<916.28003, -1703.59143, 50.26377>>,-3.90)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_pilot].id,true)
			
			if isentityalive(vehs[mvf_Replay_car].id)
				SAFE_RELEASE_VEHICLE(vehs[mvf_Replay_car].id)
			endif
			//cs
			REQUEST_CUTSCENE("sol_1_mcs_2")
			Load_Asset_AnimDict(sAssetData,"misssolomon_1")
			SET_ALL_EXIT_STATES()
			set_peds_CS_OUTFITS()
			//heli 
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_heli].id,false)
			idelay = get_Game_timer()
			iskipDelay = GET_GAME_TIMER()
			mission_substage++
		break
		case 1
			if HAS_CUTSCENE_LOADED()	
			and not IS_SCRIPTED_CONVERSATION_ONGOING()			
				REGISTER_ENTITY_FOR_CUTSCENE(rocco(),"rocco",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)			
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_pilot].id,"Rocco_Goon",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)	
				HANG_UP_AND_PUT_AWAY_PHONE()
				start_cutscene(CUTSCENE_PLAYER_TARGETABLE)			
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
		break
		case 2
			if IS_CUTSCENE_PLAYING()
				STOP_GAMEPLAY_HINT()
				SET_PLAYER_CONTROL(player_id(),true)
				set_replay_mid_mission_stage_with_name(2,"stage 2: Fight Rocco")	
				CLEAR_AREA_OF_PROJECTILES(<<916.03021, -1702.74951, 50.26364>>,50)	
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				TRIGGER_MUSIC_EVENT("SOL1_FIST_FIGHT")
				STOP_AUDIO_SCENES()
				if not IS_AUDIO_SCENE_ACTIVE("SOL_1_ROCCO_ARRIVES_CUTSCENE")
					START_AUDIO_SCENE("SOL_1_ROCCO_ARRIVES_CUTSCENE")
				endif					
				SAFE_DELETE_PED(peds[mpf_Gianni].id)
				mission_substage++
			endif
		break	
		case 3
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				
				REPLAY_STOP_EVENT()
				SET_CURRENT_PED_WEAPON(mike(),WEAPONTYPE_UNARMED,true)
				println("MIKE")
				bcs_mike = true
			else
				point_gameplay_cam_at_coord(220.1630)
			endif			
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("rocco")				
				REGISTER_TARGET(rocco(),mike())
				TASK_PUT_PED_DIRECTLY_INTO_MELEE(rocco(),mike(),0.0,-1,0.0,COMBAT_PED_PREVENT_CHANGING_TARGET)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(rocco())
				println("rocco")
				bcs_rocco = true
			endif
			
			if  bcs_mike
			and bcs_rocco
				//clean up the other peds
				int ped
				for ped = 0 to enum_to_int(NUM_OF_Mafia_PEDS) - 1	
					if isentityalive(mafia[ped].id)
						SAFE_DELETE_PED(mafia[ped].id)
					endif
				endfor
				//rocco
				if not DOES_BLIP_EXIST(peds[mpf_rocco].blip)
					peds[mpf_rocco].blip = CREATE_BLIP_FOR_PED(rocco(),true)
				endif
				
				//bug:2033220
				if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_ENTITY_FACING(mike(),GET_ENTITY_COORDS(rocco()))				
				endif
				
				print_now("SOL1_BEAT",DEFAULT_GOD_TEXT_TIME,1)
							
				if IS_AUDIO_SCENE_ACTIVE("SOL_1_ROCCO_ARRIVES_CUTSCENE")
					STOP_AUDIO_SCENE("SOL_1_ROCCO_ARRIVES_CUTSCENE")
				endif			
				TRIGGER_MUSIC_EVENT("SOL1_START_FIGHT")
				bChooseActor		= true
				Unload_Asset_Model(sAssetData,modelrocco)
				Unload_Asset_Model(sAssetData,modelWorker)
				Unload_Asset_Model(sAssetData,G_M_M_ARMGOON_01)
				
				iHelpTextTimer = GET_GAME_TIMER() //BUG FIX 1422945
				bDoneFightHelp1 = FALSE
				bDoneFightHelp2 = FALSE
				
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(mike(),SOL1_BRAWL_DAMAGE)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST) 
				mission_substage++
			endif
		break
		case 4 //*Stage number being used in stage checks*
			
			//Wait 3 seconds before printing help text BUG FIX 1422945
			IF bDoneFightHelp1 = FALSE
			and g_replay.iReplayInt[0] = 0		
				IF GET_GAME_TIMER() - iHelpTextTimer > 3000
					CLEAR_HELP()
					PRINT_HELP("SOL1_HELP1",6000)//~s~Increasing the strength stat will allow more damage to be inflicted when performing a melee attack, ladders to be climbed faster, more damage to be taken, and better sports performance.
					bDoneFightHelp1 = TRUE
					g_replay.iReplayInt[0] = 1	
				ENDIF
			ENDIF
			IF bDoneFightHelp2 = FALSE
			and g_replay.iReplayInt[1] = 0
				IF bDoneFightHelp1 = TRUE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SOL1_HELP1")
						PRINT_HELP("SOL1_HELP2",6000)//~s~The strength stat can be increased by engaging in melee combat and playing sports.
						bDoneFightHelp2 = TRUE
						g_replay.iReplayInt[1] = 1
					ENDIF
				ENDIF
			ENDIF
			
			if isentityalive(mike())
			and isentityalive(rocco())
				if GET_DISTANCE_BETWEEN_ENTITIES(mike(),rocco()) > 120
					MISSION_FAILED(mff_left_Area)
				elif GET_DISTANCE_BETWEEN_ENTITIES(mike(),rocco()) > 100
					if not IS_MESSAGE_BEING_DISPLAYED()
					and not bLeftfight
						print_Now("SOl1_FIGHTRT",DEFAULT_GOD_TEXT_TIME,1)
						bLeftFight = true
					endif
				else
					bLeftFight = false
				endif
			endif
			
			GET_CURRENT_PED_WEAPON(mike(),wcurrent)
			if isentityalive(rocco())
			
				//bugfix: 1935310				
				IF GET_GAME_TIMER() - iHelpTextTimer < 3000
					SET_PED_RESET_FLAG(rocco(),PRF_ForcePedToStrafe,TRUE)	
				ENDIF				
				//----
				
				//check if rocco out of the area. if fallen dead, if not high get back.
				if not IS_ENTITY_IN_ANGLED_AREA(rocco(),<<919.09357, -1682.95520, 50.22886>>,<<901.45612, -1681.41113,  56.31160>>,36.14)					
					//dead area
					if IS_ENTITY_IN_ANGLED_AREA(rocco(),<<913.99536, -1648.59717,0>>,<<908.99036, -1713.31812,43.73081>>,50)					
						if GET_RANDOM_BOOL()
							ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
							CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE",CONV_PRIORITY_MEDIUM)							
							SET_ENTITY_HEALTH(rocco(),0)						
							MISSION_FAILED(mff_rocco_died)
						else
							ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1Actor")
							CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE2",CONV_PRIORITY_MEDIUM)							
							SET_ENTITY_HEALTH(rocco(),0)						
							MISSION_FAILED(mff_rocco_died)
						endif
					//not dead area get back to roof.
					else
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(rocco(),<<911.46997, -1690.09949, 50.13412>>,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP)
						endif					
					endif
				//combat mike unless holding hands up
				else
					if IS_ENTITY_IN_ANGLED_AREA(mike(),<<919.09357, -1682.95520, 50.22886>>,<<901.45612, -1681.41113,  56.31160>>,36.14)
						if not IS_PED_IN_COMBAT(rocco(),mike())
						and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_HANDS_UP)	
						and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE)
							TASK_COMBAT_PED(rocco(),mike(),COMBAT_PED_PREVENT_CHANGING_TARGET)
						endif
					endif
				endif
				
				if not IS_ENTITY_IN_ANGLED_AREA(mike(),<<919.09357, -1682.95520, 50.22886>>,<<901.45612, -1681.41113,  56.31160>>,36.14)
					//rocco shouting at mike as you have left the fight area
					if get_game_timer() - i_dialogueTimer > 15000
					and iDialogue_Stage > 1
						ADD_PED_FOR_DIALOGUE(convo_struct,1,rocco(),"ROCCO")
						if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_GETBK",CONV_PRIORITY_MEDIUM)
							i_dialogueTimer = get_game_timer()
						endif
					endif
					// move rocco back into position then look at mike.
					if not IS_ENTITY_AT_COORD(rocco(),<<911.46997, -1690.09949, 50.13412>>,<<2,2,2>>)
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(rocco(),<<911.46997, -1690.09949, 50.13412>>,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP)
						endif
					else						
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
							TASK_TURN_PED_TO_FACE_ENTITY(rocco(),mike(),-1)
						endif
					endif
				else //Mike in fight area
					if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Milton(),SCRIPT_TASK_PERFORM_SEQUENCE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(null,"misssolomon_1","anton_heli_intro",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							TASK_PLAY_ANIM(null,"misssolomon_1","anton_heli_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							TASK_PLAY_ANIM(null,"misssolomon_1","anton_heli_exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
						CLOSE_SEQUENCE_TASK(seq)					
						TASK_PERFORM_SEQUENCE(Milton(),seq)
						CLEAR_SEQUENCE_TASK(seq)
					endif
					if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(Anton(),SCRIPT_TASK_PERFORM_SEQUENCE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_PLAY_ANIM(null,"misssolomon_1","milton_heli_intro",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							TASK_PLAY_ANIM(null,"misssolomon_1","milton_heli_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
							TASK_PLAY_ANIM(null,"misssolomon_1","milton_heli_exit",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_UPPERBODY)
						CLOSE_SEQUENCE_TASK(seq)					
						TASK_PERFORM_SEQUENCE(Anton(),seq)
						CLEAR_SEQUENCE_TASK(seq)
					endif
						// check for spooking the actor and director when in the area
					if  wcurrent != WEAPONTYPE_UNARMED
					and GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
					and not IS_PHONE_ONSCREEN()
						if IS_PLAYER_TARGETTING_ENTITY(player_id(),Milton())
						or IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),milton())
						or IS_PLAYER_TARGETTING_ENTITY(player_id(),anton())
						or IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),anton())
							if get_game_timer() - iAimDelay > 1200
							and iDialogue_Stage > 1
								if GET_RANDOM_BOOL()
									ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE",CONV_PRIORITY_MEDIUM)							
									iAimDelay = get_game_timer()
									mission_substage = 101
								else
									ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1Actor")
									CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE2",CONV_PRIORITY_MEDIUM)							
									iAimDelay = get_game_timer()
									mission_substage = 101
								endif
							endif
						else
							iAimDelay = get_game_timer()
						endif
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_HANDS_UP)
							TASK_HANDS_UP(rocco(),-1,mike(),-1)
						endif
						if get_game_timer() - iGunOutDelay > 5000
						and iDialogue_Stage > 1
							ADD_PED_FOR_DIALOGUE(convo_struct,1,rocco(),"ROCCO")
							if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_GUNOUT",CONV_PRIORITY_MEDIUM)							
								iGunOutDelay = get_game_timer()
							endif
						endif
					else
						if get_game_timer() - iGunOutDelay > 1200							
							if IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_HANDS_UP)
								CLEAR_PED_TASKS(rocco())
							endif							
							iGunOutDelay = get_game_timer()
						endif
					endif
					if not IS_SCRIPTED_CONVERSATION_ONGOING()					
						if get_game_Timer() - iambdialoguetimer > 7000
						and not IS_MESSAGE_BEING_DISPLAYED()
						and iDialogue_Stage > 1
							if GET_RANDOM_BOOL()
								ADD_PED_FOR_DIALOGUE(convo_struct,4,rocco(),"ROCCO")
								CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_RGA2",CONV_PRIORITY_MEDIUM)								
							else
								if bChooseActor
									ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_FIGHTM",CONV_PRIORITY_MEDIUM)
										bChooseActor = false
									endif
								else								
									ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1ACTOR")
									if CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOL1_FIGHTA",CONV_PRIORITY_MEDIUM)
										bChooseActor = true
									endif
								endif
							endif
						endif
					else	
						iambDialogueTimer = get_Game_timer()
					endif
				endif	
				if  wcurrent != WEAPONTYPE_UNARMED
					if DOES_ENTITY_EXIST(rocco())					
						if HAS_entity_BEEN_DAMAGED_BY_WEAPON(rocco(),wcurrent)		
							if GET_RANDOM_BOOL()
								ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
								CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE",CONV_PRIORITY_MEDIUM)							
								iAimDelay = get_game_timer()
								mission_substage = 102
							else
								ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1Actor")
								CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE2",CONV_PRIORITY_MEDIUM)							
								iAimDelay = get_game_timer()
								mission_substage = 102
							endif
						endif
					endif
					if isentityalive(Milton())
					and isentityalive(Anton())
						if HAS_PED_RECEIVED_EVENT(Milton(),EVENT_SHOT_FIRED_BULLET_IMPACT)
						or HAS_PED_RECEIVED_EVENT(Anton(),EVENT_SHOT_FIRED_BULLET_IMPACT)
							if GET_RANDOM_BOOL()
								ADD_PED_FOR_DIALOGUE(convo_struct,3,Anton(),"Anton")
								CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE",CONV_PRIORITY_MEDIUM)							
								iAimDelay = get_game_timer()
								mission_substage = 101
							else
								ADD_PED_FOR_DIALOGUE(convo_struct,2,Milton(),"SOL1Actor")
								CREATE_CONVERSATION(convo_struct,"SOL1AUD","SOl1_SCARE2",CONV_PRIORITY_MEDIUM)							
								iAimDelay = get_game_timer()
								mission_substage = 101
							endif
						endif
					endif				
				endif
			endif		
			
			if not IS_CUTSCENE_ACTIVE()
				//cs
				REQUEST_CUTSCENE("sol_1_mcs_3")
				SET_ALL_EXIT_STATES()
				set_peds_CS_OUTFITS()
			endif
			
			if not IS_AUDIO_SCENE_ACTIVE("SOL_1_FIGHT_ROCCO")
				START_AUDIO_SCENE("SOL_1_FIGHT_ROCCO")
			endif
			
			if isentityalive(rocco())
				SET_PED_RESET_FLAG(rocco(),PRF_PreventAllMeleeTakedowns,true)				
				if get_entity_health(rocco()) < 150
				and not IS_PED_FALLING(rocco())
					SET_ENTITY_HEALTH(rocco(),200)
					CLEAR_PED_TASKS(Milton())					
					CLEAR_PED_TASKS(Anton())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
					CLEAR_HELP()
					SET_PED_TO_RAGDOLL(rocco(),5000,6000,TASK_RELAX,false,false,true)
					idelay = get_Game_timer()
					
								
					REPLAY_STOP_EVENT()			
					
					
					mission_substage++			
				endif				
			endif				
		break
		case 5
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()				
			and get_game_timer() - idelay > 3000
				REGISTER_ENTITY_FOR_CUTSCENE(rocco(),"rocco",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(milton(),"milton",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(anton(),"anton",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_heli].id,"Main_heli",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				HANG_UP_AND_PUT_AWAY_PHONE()
				start_cutscene()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++	
			endif
		break
		case 6
			if IS_CUTSCENE_PLAYING()
				if DOES_BLIP_EXIST(peds[mpf_rocco].blip)
					REMOVE_BLIP(peds[mpf_rocco].blip)
				endif					
				TRIGGER_MUSIC_EVENT("SOL1_FIGHT_DONE")
				if IS_AUDIO_SCENE_ACTIVE("SOL_1_FIGHT_ROCCO")
					STOP_AUDIO_SCENE("SOL_1_FIGHT_ROCCO")
				endif					
				mission_substage++
			endif
		break		
		case 7			
						
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")	
				if WAS_CUTSCENE_SKIPPED()
					SET_ENTITY_COORDS(mike(),<<911.39081, -1686.13513, 50.13412>>)
					SET_ENTITY_HEADING(mike(),-4.11)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				endif
				
				REPLAY_STOP_EVENT()
				
//				TASK_FOLLOW_NAV_MESH_TO_COORD(mike(),<<912.93494, -1682.35657, 50.13412>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
				FORCE_PED_MOTION_STATE(MIKE(),MS_ON_FOOT_WALK,false,FAUS_CUTSCENE_EXIT)
				SET_PED_MIN_MOVE_BLEND_RATIO(MIKE(),PEDMOVEBLENDRATIO_WALK)	
				SIMULATE_PLAYER_INPUT_GAIT(player_id(),PEDMOVE_WALK,2000)
				println("MIKE")
				bcs_mike = true
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			endif				
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Anton")
				SET_PED_INTO_VEHICLE(Anton(),vehs[mvf_heli].id,VS_BACK_RIGHT)
				bcs_director = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Milton")
				SET_PED_INTO_VEHICLE(Milton(),vehs[mvf_heli].id,VS_BACK_LEFT)
				bcs_actor = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Main_Heli")				
				bcs_heli = true
			endif
			
			if  bcs_mike
			and bcs_actor
			and bcs_director
			and bcs_heli				
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_heli].id,true)				
				mission_set_stage(msf_3_get_to_sol)
				mission_substage = STAGE_ENTRY	
			endif
		break
		
		case 101
			if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(rocco(),SCRIPT_TASK_HANDS_UP)
				TASK_HANDS_UP(rocco(),-1,mike(),-1)
			endif
			if not IS_PED_FLEEING(Anton())
				TASK_SMART_FLEE_PED(Anton(),mike(),200,-1)
			endif
			if not IS_PED_FLEEING(Milton())
				TASK_SMART_FLEE_PED(Milton(),mike(),200,-1)
			endif
			if GET_GAME_TIMER() - iAimDelay > 1000
				MISSION_FAILED(Mff_scared_peds)	
			endif
		break
		case 102			
			if isentityalive(rocco())
				SET_ENTITY_HEALTH(rocco(),0)
			endif
			if not IS_PED_FLEEING(Anton())
				TASK_SMART_FLEE_PED(Anton(),mike(),200,-1)
			endif
			if not IS_PED_FLEEING(Milton())
				TASK_SMART_FLEE_PED(Milton(),mike(),200,-1)
			endif
			if GET_GAME_TIMER() - iAimDelay > 1000			
				MISSION_FAILED(mff_rocco_died)
			endif
		break
	endswitch
	
	if isentityalive(Milton())
	and isentityalive(Anton())			
		if isentityalive(mike())
			if not IS_PED_HEADTRACKING_PED(Milton(),mike())
				TASK_LOOK_AT_ENTITY(Milton(),mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT,SLF_LOOKAT_VERY_HIGH)
			endif
			if not IS_PED_HEADTRACKING_PED(Anton(),mike())
				TASK_LOOK_AT_ENTITY(Anton(),mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT,SLF_LOOKAT_VERY_HIGH)
			endif
		endif
	endif
	
endproc
proc ST_3_GET_TO_SOL()
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif		
			if not IS_AUDIO_SCENE_ACTIVE("SOL_1_GET_TO_STUDIO")
				START_AUDIO_SCENE("SOL_1_GET_TO_STUDIO")
			endif
			if DOES_BLIP_EXIST(blip_objective)
				REMOVE_BLIP(blip_objective)
			endif
			set_wanted_level_multiplier(0.3)
			Load_Asset_AnimDict(sAssetData,"misssolomon_1")
			set_replay_mid_mission_stage_with_name(3,"stage 3:  Get to Sol")
			SET_ALL_EXIT_STATES()	
			bsearchlights 	= false
			iScareStage		= 0
			bcueTakeOff 	= false
			
			REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
			
			mission_substage++
		break
		case 1
		
			if not bcueTakeOff
				if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id)
				and IS_ENTITY_IN_AIR(vehs[mvf_heli].id)
					TRIGGER_MUSIC_EVENT("SOL1_TAKE_OFF")
					bcueTakeOff = true
				endif
			endif
			
			if Scared_Milton()	
				b_cutscene_loaded = false
				TRIGGER_MUSIC_EVENT("SOL1_SCARED_THEM")
				STAT_INCREMENT(SP0_FLYING_ABILITY,2)
				CLEAR_AREA(<<-1107.32568, -461.81781, 30.17446>>,50,true)
				unLoad_Asset_Anim_Dict(sAssetData,"misssolomon_1")
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
		break		
		case 2
			if iDialogue_Stage > 4
				if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<-1098.04102, -458.76242, 34.38025>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,
							true,null,peds[mpf_milton].id,peds[mpf_Anton].id,vehs[mvf_heli].id,"SOL1_STUDIO","","","","","CMN_GENGETINHE","CMN_GENGETBCKHE",false,true)
				or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And IS_ENTITY_IN_ANGLED_AREA(mike(),<<-1107.32568, -461.81781, 30.17446>>,<<-1089.89941, -452.28159, 36.5>>,20)
					and not IS_ENTITY_IN_AIR(vehs[mvf_heli].id))
					and IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_heli].id)
					SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)	
					BRING_VEHICLE_TO_HALT(vehs[mvf_heli].id,5,1)
					bStartConstructionChecks = false
					mission_substage++
				endif
			endif
		break
		case 3
			if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_heli].id)
			and not IS_ENTITY_IN_AIR(vehs[mvf_heli].id)
				// stats update
				if GET_ENTITY_HEALTH(vehs[mvf_heli].id) = GET_ENTITY_MAX_HEALTH(vehs[mvf_heli].id)
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(SOL1_PERFECT_LANDING)
				endif
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id,false,false)
				if not bMusic_ends
					TRIGGER_MUSIC_EVENT("SOL1_ENDS")
					bMusic_ends = true
				endif
				
				TASK_LEAVE_ANY_VEHICLE(mike(),300)
				
				OPEN_SEQUENCE_TASK(seq)
					TASK_LEAVE_ANY_VEHICLE(null,1800)//200 BUG FIX 1399455 
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1078.97095, -467.68033, 35.60899>>,PEDMOVE_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),5000)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(Milton(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				OPEN_SEQUENCE_TASK(seq)
					TASK_LEAVE_ANY_VEHICLE(null,1500)//50 BUG FIX 1399455
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-1077.96936, -470.29300, 35.62410>>,PEDMOVE_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(null,mike(),6000)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(anton(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_heli].id,false)
				SET_PLAYER_CONTROL(player_id(),true)	
				KILL_FACE_TO_FACE_CONVERSATION()
				iDialogue_Stage = 6
				mission_substage++
			else
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(mike()))
			endif
		break
		case 4	//audio using this stage number 	
			if not b_cutscene_loaded
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<-1034.74194, -497.20056, 35.37177>>) < DEFAULT_CUTSCENE_LOAD_DIST						
					REQUEST_CUTSCENE("SOL_1_EXT")
					request_Model(IG_SOLOMON)
					set_peds_CS_OUTFITS()
					b_cutscene_loaded = true
					bSolomonCreated = false
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-1028.38049, -486.24539, 37.13475>>,20)
					CLEAR_AREA_OF_VEHICLES(<<-1028.38049, -486.24539, 37.13475>>,20)
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1037.91162, -495.88333, 30>>,<<-1016.92767, -471.04614, 39>>,false)
				endif
			else
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<-1034.74194, -497.20056, 35.37177>>) > DEFAULT_CUTSCENE_UNLOAD_DIST		
					REMOVE_CUTSCENE()
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_SOLOMON)
					b_cutscene_loaded = false
					if bSolomonCreated
						SAFE_DELETE_PED(peds[mpf_sol].id)
					endif
				elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mike(),<<-1034.74194, -497.20056, 35.37177>>) < DEFAULT_CUTSCENE_LOAD_DIST	
					if not bSolomonCreated					
					and HAS_MODEL_LOADED(IG_SOLOMON)
						peds[mpf_sol].id = CREATE_PED(PEDTYPE_MISSION,IG_SOLOMON,<<-1011.29340, -480.40060, 38.97574>>,118.24)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sol(),true)
						SET_PED_COMPONENT_VARIATION(sol(),PED_COMP_TORSO,0,0)
						SET_PED_COMPONENT_VARIATION(sol(),PED_COMP_LEG,0,0)
						SET_PED_COMPONENT_VARIATION(sol(),PED_COMP_SPECIAL,0,0)
						SET_ENTITY_HEALTH(sol(),110)
						SET_PED_RELATIONSHIP_GROUP_HASH(sol(),rel_friends)
						bSolomonCreated = true
						DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-1028.38049, -486.24539, 37.13475>>,20)
						CLEAR_AREA_OF_VEHICLES(<<-1028.38049, -486.24539, 37.13475>>,20)
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1037.91162, -495.88333, 30>>,<<-1016.92767, -471.04614, 39>>,false)
					endif
				endif
			endif
			
			if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(sLocatesData,<<-1022.89069, -490.86011, 35.97233>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,false,
															null,peds[mpf_milton].id,peds[mpf_Anton].id,"SOL1_TLKSOL","","SOl1_RTNMILT","SOl1_RTNANT","",false,true)
			or IS_ENTITY_IN_ANGLED_AREA(mike(),<<-1055.51233, -505.69760, 34>>,<<-1009.98639, -483.01324, 45.93766>>,26)
				if GET_PLAYER_WANTED_LEVEL(player_id()) > 0
					CLEAR_PLAYER_WANTED_LEVEL(player_id())
				endif
				if IS_PED_IN_ANY_VEHICLE(mike())
					if IS_VEHICLE_ALMOST_STOPPED(GET_VEHICLE_PED_IS_IN(mike()))
					and not IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(mike()))
					
						TASK_FOLLOW_NAV_MESH_TO_COORD(sol(),<<-1015.59894, -482.73679, 36.34386>>,PEDMOVE_WALK)
						KILL_FACE_TO_FACE_CONVERSATION()
						mission_substage = 5
					else
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(mike()))
					endif
				else
					TASK_FOLLOW_NAV_MESH_TO_COORD(sol(),<<-1015.59894, -482.73679, 36.34386>>,PEDMOVE_WALK)
					KILL_FACE_TO_FACE_CONVERSATION()
					mission_substage = 5
				endif
			endif
		break
		case 5 
			//--------------	PUSH IN    --------------	
			FLOAT fT2HintCamFollow
			FLOAT fT2HintCamOffset
			FLOAT fT2HintCamFove			
			fT2HintCamFollow = 0.35
			fT2HintCamOffset = -0.78
			fT2HintCamFove = 30.00
			SET_GAMEPLAY_ENTITY_HINT(SOL(),<<0,0,0>>,true,-1,3000)
            SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fT2HintCamFollow)
            SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fT2HintCamOffset)
            SET_GAMEPLAY_HINT_FOV(fT2HintCamFove)
	      	SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
			//---------------------------------------------
			
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			and IS_ENTITY_IN_ANGLED_AREA(sol(),<<-1017.31689, -483.44772, 35>>,<<-1014.30426, -481.67487, 39>>,30)
				if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				endif
				
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				REGISTER_ENTITY_FOR_CUTSCENE(sol(),"Soloman",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				STOP_AUDIO_SCENES()
				HANG_UP_AND_PUT_AWAY_PHONE()
				START_CUTSCENE()	
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				iDialogue_Stage = 16	// make sure no conversations are started during the cutscene
				mission_substage++
			endif
		break
		case 6
			if IS_CUTSCENE_PLAYING()
			 	STOP_GAMEPLAY_HINT()
				REMOVE_PED_HELMET(mike(),true)
				SET_PED_COMP_ITEM_CURRENT_SP(mike(),COMP_TYPE_PROPS,PROPS_HEAD_NONE)
				if isentityalive(Milton())
				 	safe_delete_ped(peds[mpf_milton].id)
				endif
				if isentityalive(Anton())
					safe_delete_ped(peds[mpf_Anton].id)
				endif
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_heli].id,true)
				mission_substage++
			endif
		break
		case 7
			if IS_CUTSCENE_PLAYING()
			and GET_CUTSCENE_TIME() > 63300
				SET_FORCE_FOOTSTEP_UPDATE(mike(),true)
			endif
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				bcs_cam = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				bcs_mike = true
			endif
			
			if bcs_cam
			and bcs_mike
				REPLAY_STOP_EVENT()
				SET_FORCE_FOOTSTEP_UPDATE(mike(),false)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				MISSION_PASSED()
			endif
		break
	endswitch
	
	clear_players_task_on_control_input(SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
	if not bSearchlights
		if isentityalive(mike())
		and isentityalive(vehs[mvf_heli].id)
			if IS_PED_IN_VEHICLE(mike(),vehs[mvf_heli].id,true)
			and	IS_VEHICLE_SEARCHLIGHT_ON(vehs[mvf_heli].id)
				SET_VEHICLE_SEARCHLIGHT(vehs[mvf_heli].id,false,false)
				bsearchlights = true
			endif
		endif
	endif
	
	if isentityalive(milton())
	and not IS_PED_HEADTRACKING_PED(milton(),mike())
		TASK_LOOK_AT_ENTITY(milton(),mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT,SLF_LOOKAT_VERY_HIGH)
	endif 
	if isentityalive(Anton())
	and not IS_PED_HEADTRACKING_PED(Anton(),mike())
		TASK_LOOK_AT_ENTITY(anton(),mike(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT,SLF_LOOKAT_VERY_HIGH)
	endif 	
	
endproc
PROC ST_4_PASSED()
	switch mission_substage
		case STAGE_ENTRY
			if IS_CUTSCENE_ACTIVE()
				if HAS_CUTSCENE_LOADED()			
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					mission_substage++
				endif
			else
			
				REQUEST_CUTSCENE("SOL_1_EXT")
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_TORSO,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_LEG,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_feet,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_JBIB,1,0,CS_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_TORSO,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_LEG,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_feet,1,0,IG_MILTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Milton",PED_COMP_JBIB,1,0,IG_MILTON)				
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_TORSO,0,0,CSB_ANTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_LEG,0,0,CSB_ANTON)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_TORSO,0,0,U_M_Y_ANTONB)
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister,"Anton",PED_COMP_LEG,0,0,U_M_Y_ANTONB)
			endif			
		break
		case 1
			if IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				mission_substage++
			endif
		break
		case 2
			if IS_CUTSCENE_PLAYING()
			and GET_CUTSCENE_TIME() > 63300
				SET_FORCE_FOOTSTEP_UPDATE(mike(),true)
			endif
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				bcs_cam = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				REPLAY_STOP_EVENT()
			
				bcs_mike = true
			else
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
			endif
			
			if bcs_cam
			and bcs_mike
				SET_FORCE_FOOTSTEP_UPDATE(mike(),false)
				MISSION_PASSED()
			endif
		break
	endswitch
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		case 	msf_0_get_to_warehouse 			ST_0_GET_TO_WAREHOUSE() 	break
		case 	msf_1_Stealth_to_roof 			ST_1_STEALTH_TO_ROOF() 		break
		case 	msf_2_fight_rocco				ST_2_FIGHT_ROCCO()			break
		case 	msf_3_get_to_sol				ST_3_GET_TO_SOL()			break
		case 	msf_4_passed					ST_4_PASSED()				Break
	endswitch
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED(mff_debug_fail)		
		ENDIF
	ENDPROC
#endif
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...solomon 1 Launched")
	PRINTNL()

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		TRIGGER_MUSIC_EVENT("SOL1_FAIL")
		PRINTSTRING("...solomon 1 Force Cleanup")
		PRINTNL()
		
		Mission_Flow_Mission_Force_Cleanup()
		if IS_CUTSCENE_ACTIVE()
			SET_CUTSCENE_FADE_VALUES()	
		endif
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)

#if IS_DEBUG_BUILD
	//z menu for skipping stages
	zMenuNames[msf_0_get_to_warehouse].sTxtLabel 		=	"Stage 0: Get to Warehouse"
	zMenuNames[msf_1_Stealth_to_roof].sTxtLabel 		=	"Stage 1: stealth to roof"
	zMenuNames[msf_2_fight_rocco].sTxtLabel 			=	"Stage 2: fight rocco"
	zMenuNames[msf_3_get_to_sol].sTxtLabel 				=	"Stage 3: Get to sol"
	zMenuNames[msf_4_passed].sTxtLabel 					=	"---------- PASSED ----------"
	
	widget_debug = START_WIDGET_GROUP("Sol 1 debug")
		ADD_WIDGET_INT_READ_ONLY("stealth stage: ",wAI_stage)
		
		ADD_WIDGET_INT_READ_ONLY("maf sit car: ",mafia[maf_car].sDebug)
		ADD_WIDGET_INT_READ_ONLY("maf sit car talk: ",mafia[maf_car_talking].sDebug)
		
		ADD_WIDGET_INT_READ_ONLY("maf sit work wlk: ",mafia[maf_worker_walkway].sDebug)
		ADD_WIDGET_INT_READ_ONLY("maf sit wlk: ",mafia[maf_walkway_door].sDebug)
		
		ADD_WIDGET_INT_READ_ONLY("maf sit worker top: ",mafia[maf_worker_top].sDebug)
		
		ADD_WIDGET_INT_READ_ONLY("maf sit wrk alone: ",mafia[maf_worker_alone].sDebug)
		ADD_WIDGET_INT_READ_ONLY("maf sit vent: ",mafia[maf_worker_vent].sDebug)
		ADD_WIDGET_INT_READ_ONLY("maf sit mid door: ",mafia[maf_worker_door].sDebug)
		ADD_WIDGET_INT_READ_ONLY("Gianni: ",mafia[mpf_Gianni].sDebug)
		
		ADD_WIDGET_FLOAT_READ_ONLY("Player noise: ",debug_noise)
		
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
#endif

	//initialize mission
	MISSION_SETUP()
	
	WHILE (TRUE)
		
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)	
		
		Mission_stage_management()
		MISSION_STAGE_SKIP()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_MrRichards") 
		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif 	
				
#IF IS_DEBUG_BUILD
	DO_DEBUG()
#ENDIF	
	
		WAIT(0)
	
	ENDWHILE
ENDSCRIPT
