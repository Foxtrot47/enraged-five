
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"

USING "CompletionPercentage_public.sch"

using "dialogue_public.sch"

USING "flow_public_core_override.sch"
USING "flow_public_GAME.sch"

using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "replay_public.sch"

USING "script_blips.sch"
USING "script_heist.sch"
USING "script_ped.sch"

USING "selector_public.sch"

USING "flow_special_event_checks.sch"

USING "RC_Area_public.sch"
USING "RC_helper_functions.sch"
USING "RC_threat_public.sch"
USING "taxi_functions.sch"
USING "chase_hint_cam.sch"
USING "commands_recording.sch"


#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 6
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Template.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_KILLEDROCO	                1 //following beverly down the road


//-----------------------
//		CONSTANTS
//-----------------------

//POLY TEST CONST
CONST_INT MAX_POLY_TEST_VERTS		8		//poly test
//Ambient encounter speech
CONST_INT MAX_AMBIENT_CONV_LINES	8

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
	MS_INTRO,					//1
	MS_CHASE,					//2
	MS_LEAVE_AREA,				//3
	MS_PHONE_CALL,				//4
	MS_RETURN_TO_STUDIO,		//5
	MS_OUTRO,					//6
	MS_FAILED					//7
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

/// PURPOSE: Mission requirements used for loading 
///    and creating mission assets
ENUM MISSION_REQ
	RQ_NONE,
	RQ_TEXT,
	RQ_CUTSCENE_INTRO,
	RQ_ROCO,
	RQ_GUN_MAN,
	RQ_SOLOMON, 
	RQ_PLAYER_CAR,
	RQ_WAYPOINT,
	RQ_CAR_ROCO,
	RQ_INJURED_CLIP
ENDENUM

/// PURPOSE: Fail reason enums for picking correct fail reason
ENUM FAILED_REASONS
	FR_NONE,
	FR_ROCO_ESCAPED,
	FR_SOLOMON_KILLED
ENDENUM


ENUM ABM_CONV_STATES
	ACS_START_LINE,
	ACS_WAIT_LINE_FIN,
	ACS_PICK_NEXT_LINE,
	ACS_FIN
ENDENUM

ENUM ROCO_STATES
	RS_NULL,
	RS_BURNOUT,
	RS_BURNOUT_GOTO,
	RS_GO_TO_GATE,
	RS_FLEE_IN_CAR,
	RS_FLEE_ON_FOOT,
	RS_FIGHT,
	RS_DRIVING_DOWN_PEIR
ENDENUM

ENUM GUNMAN_STATES
	GS_NULL,
	GS_PERFORMING_DRIVEBY,
	GS_WAITING,
	GS_COMBAT_ONFOOT
ENDENUM

ENUM COP_MONITOR
	CM_MONITER,
	CM_INIT_WANTED,
	CM_LOSING_WANTED	
ENDENUM


ENUM CAMERA_CUT
	CC_INIT,
	CC_START,
	CC_WAIT,
	CC_END
ENDENUM

///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages

COP_MONITOR eCopMonitor = CM_MONITER

ROCO_STATES eRocoState = RS_NULL
GUNMAN_STATES eGunmanState = GS_NULL

CAMERA_CUT eCamCut = CC_INIT
//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************

/// PURPOSE: Holds data used to create a ped 
///    Contains ped specific variables for the
///    perception system
STRUCT MYPED
	PED_INDEX 			id
	VECTOR 				vPos
	FLOAT 				fDir
	MODEL_NAMES 		Mod = DUMMY_MODEL_FOR_SCRIPT
	BLIP_INDEX			blip
ENDSTRUCT

/// PURPOSE: Holds data for creating a 
///    vehicle in an encounter
STRUCT MYVEHICLE
	VEHICLE_INDEX 		id
	VECTOR 				vPos
	FLOAT 				fDir
	MODEL_NAMES 		Mod = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

/// PURPOSE: holds data for createing an
///    object in an encounter
STRUCT MYOBJECT
	OBJECT_INDEX 	id
	VECTOR			vPos
	VECTOR 			vRot	
	MODEL_NAMES 	Mod = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT AMBIENT_DIALOGUE_LINE
	STRING sConvLine
	PED_INDEX ped
	STRING sVoice
	INT iStartTime = 0
	INT iLineTime = 0
	BOOL bDone = FALSE
ENDSTRUCT

STRUCT AMBIENT_DIALOGUE_CONV
	AMBIENT_DIALOGUE_LINE mConvLine[MAX_AMBIENT_CONV_LINES]
	INT iIndex = 0
	ABM_CONV_STATES eAmbConv = ACS_START_LINE
	BOOL bIsRandom = FALSE //Doesnt do anything yet 
ENDSTRUCT

/// PURPOSE: Holds a string and a int 
///    String is used to play a conversation
///    the int is used to expire a bit in a bit field
///    
STRUCT DIALOGUE_HOLDER
	STRING 	sConv
	INT 	iExpireBit
ENDSTRUCT


TEST_POLY mAreaCheck1		//Poly area 1
TEST_POLY mAreaCheck2		//Poly area 2

//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints
BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state

//Fail vars
STRING sFailReason = NULL				//String to display when mission is failed

//GOD TEXT
STRING sGodText = "SOL2"
BOOL bObjectiveShown = FALSE			//Has an objective been shown

//BLIPs
BLIP_INDEX biBlip						//Mission blip - mainly used for go to objectives

//Cutscene
BOOL bLoadingFinCutscene = FALSE

//ScriptCamera
CAMERA_INDEX camMain					//Script camera used in place holder cutscenes

VECTOR vSafeVec = <<0,0,0>>				//safe vector used when a proper position isnt needed 

VECTOR vDropOffLoc = <<-1011.5279, -480.0313, 38.9757>>
VECTOR vDropOffGPSLoc = << -1016.13, -481.69, 38.9757 >>
VECTOR vPositionToLeave = vSafeVec
BOOL bStartedDriveBys = FALSE

INT iMissionTimer = -1
INT iCutsceneDelay = -1
///MISSION PED VARS SCRIPT AI

BOOL bFailConvSet = FALSE

BOOL bTriggeredReplayRecord = FALSE

BOOL bRoccoBlipedInVehicle = FALSE

BOOL bVehStatsGrabbed 	//PD - adding this to monitor damage and speed watch on latest player's car
BOOL bPlayerStatsGrabbed //PD - adding this so I can throw the player damage wtch into the main body
BOOL bIsCustomGPSRouteActive = FALSE	// needed to make gps route go inside Studio for return stage
BOOL bRocoSpeadUp = FALSE
BOOL bCleanedupSolomon = FALSE
BOOL bCleanedupPlayerCar = FALSE
INT iSolomonChatDelay = -1
INT iDelayCall = -1
INT iDelayChaseSpeech = -1

INT iReplayStage = 0 
///==============| GOD TEXT BOOLS |============


///===============| models |===================


/// ===============| START VECTORS |==============

/// ==============| START HEADINGS |===============

/// ==============| PED INDICES |================
MYPED mRocco
MYPED mGunMan
MYVEHICLE mCarRoco
MYVEHICLE mPlayerCar
PED_INDEX piSolomon

/// ==============| OBJECT
OBJECT_INDEX oiCone

SCENARIO_BLOCKING_INDEX sbiMovieSetScenarioBlocker

/// ==============| VEHICLE INDICES |===========

/// ===============| GROUPS |========================
REL_GROUP_HASH HASH_SOLOMON			//Relationship 

/// ===============| DIALOGUE |======================
STRING sTextBlock = "SOL2AUD"			//The Dialogue block for the mission
structPedsForConversation s_conversation_peds		//conversation struct
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

STRING 	sInitialRoots = "PHONECALL"
STRING sYesRoots = "PHONEYES"
STRING sNoRoots = "PHONENO"

BOOL bKilledRoccoConv = FALSE
BOOL bKilledGunManConv = FALSE
BOOL bSolCallDone = FALSE
BOOL bMikeShout = FALSE
BOOL bTriggeredVehicleMusic = FALSE
BOOL bExpireGunmanShout = FALSE

//Used for tracking an interupted conversation
//TEXT_LABEL_23 sResumeRoot = "NONE"
//TEXT_LABEL_23 sResumeLine = "NONE"

/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()
	bKilledRoccoConv = FALSE
	bSolCallDone = FALSE
	bMikeShout = FALSE	
	bIsCustomGPSRouteActive = FALSE
	bRocoSpeadUp = FALSE
	bLoadingFinCutscene = FALSE
	bStartedDriveBys = FALSE
	bTriggeredVehicleMusic = FALSE
	bKilledGunManConv = FALSE
	bRoccoBlipedInVehicle = FALSE
	eCamCut = CC_INIT
	iDelayCall = -1
	bFailConvSet = FALSE
ENDPROC

///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD

	BOOL bShowDebugText = TRUE
	WIDGET_GROUP_ID widgetGroup
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	
	ENDPROC
	
	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
//	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)
//
//		VECTOR vBottom[2]
//		VECTOR vTop[2]
//		
//		vBottom[0] = vec1
//		vBottom[1] = vec2
//		
//		vTop[0] = vec1
//		vTop[1] = vec2
//		
//		IF vec1.z > vec2.z
//			vBottom[0].z = vec2.z
//			vBottom[1].z = vec2.z
//			vTop[0].z = vec1.z
//			vTop[1].z = vec1.z
//		ELSE
//			vBottom[0].z = vec1.z
//			vBottom[1].z = vec1.z
//			vTop[0].z = vec2.z
//			vTop[1].z = vec2.z
//		ENDIF
//
//		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
//		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
//		VECTOR w = side * (width / 2.0)
//
//		// Bottom points
//		VECTOR c1 = vBottom[0] - w  // base left
//		VECTOR c2 = vBottom[0] + w  // base right              
//		VECTOR c3 = vBottom[1] + w  // top rt
//		VECTOR c4 = vBottom[1] - w  // top lt
//
//		// Top points
//		VECTOR d1 = vTop[0] - w  // base left
//		VECTOR d2 = vTop[0] + w  // base right              
//		VECTOR d3 = vTop[1] + w  // top rt
//		VECTOR d4 = vTop[1] - w  // top lt
//
//		// Draw bottom lines
//		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
//		// Draw top lines
//		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
//		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
//		// Draw uprights
//		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
//		
//	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Set a peds position and heading safely ie check its ok then move it and set its heading
/// PARAMS:
///    index - The ped to move
///    pos - The position to move it to
///    dir - The Heading to set
PROC SET_PED_POS(PED_INDEX index, VECTOR pos, FLOAT dir)
	IF IS_PED_UNINJURED(index)
		CLEAR_PED_TASKS(index)
	ENDIF

	SAFE_TELEPORT_ENTITY(index, pos, dir)
ENDPROC

/// PURPOSE:
///    Adds a blip to a location safly checking to see that its not in existance before creatinging it
/// PARAMS:
///    blipIn - The blip index to write the newly created blip to
///    pos - the position to set the blip at
///    route - should the blip have a GPS route
PROC ADD_BLIP_LOCATION(BLIP_INDEX &blipIn, VECTOR pos, BOOL route = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIn)
		blipIn = CREATE_COORD_BLIP(pos, BLIPPRIORITY_MED, route)
	ENDIF
ENDPROC


/// PURPOSE:
///    Adds blip to Peds first checking that the blip doesnt exist then if the ped is ok. Also can set the ped to be enemy blip or not
/// PARAMS:
///    blipIn - The blip index to write the newly created blip to
///    pedindex - The ped to add the blip to 
///    bFriendly - Is the ped an enemy if TRUE the blip is blue
PROC ADD_SAFE_BLIP_TO_PED(BLIP_INDEX &blipIn, PED_INDEX pedindex, BOOL bFriendly = FALSE, BOOL bCritical = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIn)
		blipIn = CREATE_PED_BLIP(pedindex, bCritical, bFriendly)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds blip to vehicle checking blip doesnt exist then that the vehicle does
/// PARAMS:
///    blipIn - The blip index to write the newly created blip to
///    vhc - The vehicle to add the blip to 
///    benemy - Is the vehicle an enemy if false the blip is blue
PROC ADD_SAFE_BLIP_TO_VEHICLE(BLIP_INDEX &blipIn, VEHICLE_INDEX vhc, BOOL bFriendly = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIn)
		blipIn = CREATE_VEHICLE_BLIP(vhc, bFriendly)
	ENDIF
ENDPROC



/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  SK_PRINT_INT(_debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD
			SK_PRINT("MODEL LOADED")
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn - If true we unload the model after spawning the ped
///    bFreeze - if true we freeze the ped after spawning
///    bTempEvents - if true we block temp events
///    bTargetable - if false we set the ped to not be targetted
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir,   BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bTempEvents = TRUE, BOOL bTargetable = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			IF IS_PED_UNINJURED(pedindex)
				#IF IS_DEBUG_BUILD SK_PRINT("PED CREATED !") #ENDIF
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, bTempEvents)
				ENDIF
				
				IF NOT bTargetable
					SET_PED_CAN_BE_TARGETTED(pedindex, bTargetable)
				ENDIF
				
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(pedindex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_MODEL_IS_SUPPRESSED(model, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_OK(vehicleindex)
			SET_ENTITY_HEADING(vehicleindex, dir)
			SET_ENTITY_COORDS(vehicleindex, pos)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehicleindex)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a ped inside a vehicle
/// PARAMS:
///    pedindex - The index to write the newly created ped back to (ref)
///    Car - The vehicle to create the ped in
///    modelName - The model of the ped to create
///    seat - the seat to create the ped in 
///    bUnloadAfterSpawn - should we unload the ped model after creation
/// RETURNS:
///    TRUE if the ped was spawned correctly
FUNC BOOL SPAWN_PED_IN_VEHICLE(PED_INDEX &pedindex, VEHICLE_INDEX Car, MODEL_NAMES modelName, VEHICLE_SEAT seat = VS_DRIVER, BOOL bUnloadAfterSpawn = TRUE)
	IF IS_VEHICLE_OK(Car)
		IF NOT DOES_ENTITY_EXIST(pedindex)
			IF REQUEST_AND_CHECK_MODEL(modelName,"Loading")
				pedindex = CREATE_PED_INSIDE_VEHICLE(Car, PEDTYPE_MISSION, modelName, seat)
				
				IF DOES_ENTITY_EXIST(pedindex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(modelName)
					ENDIF
					RETURN TRUE
				ENDIF

			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Spawns an object. And activates its physics
/// PARAMS:
///    objectindex - The OBJECT_INDEX to write the newly create object to 
///    model - The model to load and use to create the object
///    pos - The position the object should be created
///    dir - The heading the object should have when created
/// RETURNS:
///    TRUE if the object was created
FUNC BOOL SPAWN_OBJECT(OBJECT_INDEX &objectindex, MODEL_NAMES model, VECTOR pos, FLOAT dir = 0.0, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(objectindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			objectindex = CREATE_OBJECT(model, pos)
			
			IF DOES_ENTITY_EXIST(objectindex)
				SET_ENTITY_HEADING(objectindex, dir)
				ACTIVATE_PHYSICS(objectindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: 
/// PURPOSE:
///    Checkes to see if a ped is in a given vehicle, also does alive checks. 
///    Returns TRUE if the given ped is in the given vehicle and they are all alive and well
/// PARAMS:
///    ped - Ped to check
///    veh - The vehicle to check they are in
/// RETURNS:
///    TRUE if the ped is in the vehicle and they are both alive and well
FUNC BOOL IS_SAFE_PED_IN_VEHICLE(PED_INDEX ped, VEHICLE_INDEX veh)
	IF veh != NULL
		IF IS_VEHICLE_OK(veh)
			IF IS_PED_UNINJURED(ped)
				IF IS_PED_IN_VEHICLE(ped,veh)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Prints objective to the screen if the objective hasnt already been printed
///    Checks and expired bObjectiveShown
/// PARAMS:
///    objstr - The text key to print
PROC PRINT_OBJ(String objstr)
	IF NOT bObjectiveShown
		PRINT_NOW(objstr, DEFAULT_GOD_TEXT_TIME,0)
		bObjectiveShown = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Print a warning God text string and expires a bool when the warning has been shown
/// PARAMS:
///    WarnObj - the text key of the wanrning
///    expire - the bool to change
PROC PRINT_WARNING_OBJ(STRING WarnObj, BOOL &expire)
	IF NOT expire
		PRINT_NOW(WarnObj, DEFAULT_GOD_TEXT_TIME,0)
		expire = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns and sets up the ped with a debug name
/// PARAMS:
///    index - The PED_INDEX to write the newly created ped to 
///    pos - The postion to create the ped at
///    fDir - The heading to give the new ped
///    modelName - The model to create the ped with
///    i - Debug number for Debug name
///    _name - The Debug name 
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SETUP_PED(PED_INDEX &index, VECTOR pos, FLOAT fDir, MODEL_NAMES modelName, INT i, STRING _name) 
	
	IF SPAWN_PED(index, modelName, pos, fDir)
		#IF IS_DEBUG_BUILD
			SK_PRINT("SPAWNED")
		#ENDIF
		
		IF IS_PED_UNINJURED(index)
			TEXT_LABEL tDebugName = _name
			tDebugName += i
			#IF IS_DEBUG_BUILD
				SK_PRINT(tDebugName)
			#ENDIF
			SET_PED_NAME_DEBUG(index, tDebugName)
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the heading between to vectors
/// PARAMS:
///    V1 - First vector
///    V2 - Second vector
/// RETURNS:
///    FLOAT -  the heading between the two
FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x,V2.y-V1.y)
ENDFUNC


///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Tells a ped to leave a specified vehicle 
///    performs dead checks on the ped and vehicle
/// PARAMS:
///    ped - The ped to leave the vechicle
///    veh - The vehicle said ped is leaving
PROC GIVE_LEAVE_VEHICLE_ORDER(PED_INDEX ped, VEHICLE_INDEX veh)
	IF IS_VEHICLE_OK(veh)
		IF IS_PED_UNINJURED(ped)
			TASK_LEAVE_VEHICLE(ped, veh)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to enter a specified vehicle 
///    performs dead checks on the ped and vehicle
/// PARAMS:
///    ped - The ped to leave the vechicle
///    veh - The vehicle said ped is leaving
///    seat - Seate the ped should enter the car from
PROC GIVE_ENTER_VEHICLE_ORDER(PED_INDEX ped, VEHICLE_INDEX veh, VEHICLE_SEAT seat = VS_DRIVER)
	IF IS_VEHICLE_OK(veh)
		IF IS_PED_UNINJURED(ped)
			TASK_ENTER_VEHICLE(ped, veh, DEFAULT_TIME_BEFORE_WARP, seat)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped in vehicle a waypoint task
/// PARAMS:
///    ped - The ped to give the task to 
///    waypoint - The name of the waypoint rec to give the ped
PROC GIVE_WAYPOINT_TASK(PED_INDEX ped, STRING waypoint, INT iStartPoint = 0 , EWAYPOINT_FOLLOW_FLAGS eFollow = EWAYPOINT_DEFAULT)
	IF IS_PED_UNINJURED(ped)
		
		FREEZE_ENTITY_POSITION(ped, FALSE)
		IF IS_VEHICLE_OK(mCarRoco.id)
		AND GET_IS_WAYPOINT_RECORDING_LOADED(waypoint)
			TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(ped, mCarRoco.id, waypoint, DRIVINGMODE_AVOIDCARS_RECKLESS, iStartPoint, eFollow)
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_NAVMESH_TASK(PED_INDEX ped, VECTOR vGoto, FLOAT moveBlend = PEDMOVEBLENDRATIO_WALK, INT warpTime = DEFAULT_TIME_BEFORE_WARP)
	IF IS_PED_UNINJURED(ped)
		FREEZE_ENTITY_POSITION(ped, FALSE)
		CLEAR_PED_TASKS(ped)
		TASK_FOLLOW_NAV_MESH_TO_COORD(ped, vGoto, moveBlend, warpTime)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped an attack order after the player has been spotted
///    Only called when the player fails the mission for getting spotted or attacking a ped
///    and possibly when the player has completed the sex scene section
/// PARAMS:
///    ped - The ped to give the order to
PROC GIVE_PED_ATTACK_ORDER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_COMBAT)
			FREEZE_ENTITY_POSITION(ped, FALSE)
			CLEAR_PED_TASKS(ped)
			TASK_COMBAT_PED(ped, PLAYER_PED_ID())
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to smart flee 200m from the player
///    Checks to see if the task is already being performed
///    Checks the ped is alive
/// PARAMS:
///    ped - the ped to flee
PROC GIVE_PED_FLEE_ORDER(PED_INDEX ped, BOOL bClearTasks = TRUE)
	IF IS_PED_UNINJURED(ped)
		IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_SMART_FLEE_PED) 
			FREEZE_ENTITY_POSITION(ped, FALSE)
			IF bClearTasks
				CLEAR_PED_TASKS(ped)
			ENDIF

			SET_PED_FLEE_ATTRIBUTES(ped, FA_USE_VEHICLE, FALSE)
			TASK_SMART_FLEE_PED(ped, PLAYER_PED_ID(), 550, -1, TRUE)
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks that 2 entities are at the same height - within
///    1.5m
/// PARAMS:
///    e1 - first entity
///    e2 - second entity
/// RETURNS:
///    TRUE if the entities are at the same height
FUNC BOOL ARE_ENTITYS_AT_SAME_HEIGHT(ENTITY_INDEX e1, ENTITY_INDEX e2)
	VECTOR vE1 = GET_ENTITY_COORDS(e1)
	VECTOR vE2 = GET_ENTITY_COORDS(e2)
	FLOAT fDiff = ABSF(vE1.z - vE2.z) //want the absolute difference so it doesnt matter which entitys height was taken away first 
	IF fDiff <= 1.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks that the player is with in range of the hunter used when playing dialogue
/// PARAMS:
///    distance - The distance the player must be with in - defaults to 10.0
/// RETURNS:
///    TRUE if the player is with in the distance 
FUNC BOOL IS_IN_CONV_DISTANCE(PED_INDEX ped, FLOAT distance = 10.0)
	IF ped = NULL
		RETURN TRUE
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(ped)
//		#IF IS_DEBUG_BUILD SK_PRINT_FLOAT(" DISTANCE BETWEEN CONVO  === ", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE))#ENDIF
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE) <= distance
		AND ARE_ENTITYS_AT_SAME_HEIGHT(ped, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates a conversation and then displays an objective
///    Expires a bool when the conversation has been created
///    assume the player is talking to another ped
///    kills the conversation if its started and the peds are too far away
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The bool to expire when the conversation has been created (ref)
///    sConvo - the string of the conversation to play
///    sObj - the text key for the obj
///    distance - the distance the player and ped need to be in before the convostation happens
PROC DO_CONVO_WITH_OBJ(PED_INDEX ped, BOOL &bExpire, STRING sConvo, STRING sObj, FLOAT distance = 10.0)
	
	IF NOT bObjectiveShown
//		#IF IS_DEBUG_BUILD SK_PRINT(sConvo) #ENDIF
//		#IF IS_DEBUG_BUILD SK_PRINT(" NOT bObjectiveShown") #ENDIF
//
		IF NOT bExpire
//			#IF IS_DEBUG_BUILD SK_PRINT("NOT bExpire") #ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				IF IS_IN_CONV_DISTANCE(ped, distance)
					bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_IN_CONV_DISTANCE(ped, distance)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				PRINT_OBJ(sObj) 
		 	ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

/// PURPOSE:
///    Creates a conversation and then displays an objective
///    And then creates another conversation
///    Expires a bool when each of the conversation have been created
///    assume the player is talking to another ped
///    kills the conversation if its started and the peds are too far away
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The expiry bool for the first conversation (ref)
///    bExpireAfterConv - bool for the second conversation (ref)
///    sConvo - the string of the 1st conversation to play
///    sConvToPlayAfterObj - the string 2nd of the conversation to play
///    sObj - the text key for the obj
///    distance - the distance the player and ped need to be in before the convostation happens
PROC DO_TWO_CONVO_WITH_OBJ(PED_INDEX ped, BOOL &bExpire, BOOL &bExpireAfterConv, STRING sConvo, STRING sConvToPlayAfterObj, STRING sObj, FLOAT distance = 10.0)
	
	IF NOT bObjectiveShown
//		#IF IS_DEBUG_BUILD SK_PRINT(" NOT bObjectiveShown") #ENDIF
		
		IF NOT bExpire
//			#IF IS_DEBUG_BUILD SK_PRINT(sConvo) #ENDIF
//			#IF IS_DEBUG_BUILD SK_PRINT("NOT bExpire") #ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				IF IS_IN_CONV_DISTANCE(ped, distance)
					bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_IN_CONV_DISTANCE(ped, distance)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF

			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				PRINT_OBJ(sObj) 
		 	ENDIF
		ENDIF
	ELSE
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sObj)
		OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
			IF NOT IS_STRING_NULL_OR_EMPTY(sConvToPlayAfterObj)
				IF NOT bExpireAfterConv
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
						IF IS_IN_CONV_DISTANCE(ped, distance)
							bExpireAfterConv = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvToPlayAfterObj, CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_IN_CONV_DISTANCE(ped, distance)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			ELSE
				bExpireAfterConv = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a conversation then expires a bool when the conversation has been created
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The expiry bool for the first conversation (ref)
///    sConvo - the string of the conversation to play
///    distance - the distance the player and ped need to be in before the convostation happens
/// RETURNS:
///    true when the bool is expired
FUNC BOOL DO_CONVO(PED_INDEX ped, BOOL &bExpire, STRING sConvo, FLOAT distance = 10.0, enumConversationPriority priority = CONV_PRIORITY_MEDIUM)
	
	IF NOT bExpire
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND IS_SCREEN_FADED_IN()
			IF IS_IN_CONV_DISTANCE(ped, distance)
				bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, priority)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_IN_CONV_DISTANCE(ped, distance)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
	RETURN bExpire
ENDFUNC


PROC CONTROL_ABM_CONV(AMBIENT_DIALOGUE_CONV &conv)
	SWITCH conv.eAmbConv
		CASE ACS_START_LINE
			IF IS_PED_UNINJURED(conv.mConvLine[conv.iIndex].ped)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(conv.mConvLine[conv.iIndex].ped, conv.mConvLine[conv.iIndex].sConvLine, conv.mConvLine[conv.iIndex].sVoice, "SPEECH_PARAMS_STANDARD")
				conv.mConvLine[conv.iIndex].iStartTime = GET_GAME_TIMER()
				#IF IS_DEBUG_BUILD SK_PRINT("ACS_START_LINE === ") #ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT(conv.mConvLine[conv.iIndex].sConvLine) #ENDIF
				
				conv.eAmbConv = ACS_WAIT_LINE_FIN
			ENDIF
		BREAK
		
		CASE ACS_WAIT_LINE_FIN
			IF (GET_GAME_TIMER() - conv.mConvLine[conv.iIndex].iStartTime) > conv.mConvLine[conv.iIndex].iLineTime
				conv.mConvLine[conv.iIndex].bDone = TRUE
				conv.iIndex++
				#IF IS_DEBUG_BUILD SK_PRINT_INT("NEXT INDEX ===", conv.iIndex) #ENDIF
				conv.eAmbConv = ACS_PICK_NEXT_LINE
			ENDIF
		BREAK
		
		CASE ACS_PICK_NEXT_LINE
			#IF IS_DEBUG_BUILD SK_PRINT("ACS_PICK_NEXT_LINE") #ENDIF
			IF NOT conv.bIsRandom
				IF NOT ARE_STRINGS_EQUAL("END", conv.mConvLine[conv.iIndex].sConvLine)
					conv.eAmbConv = ACS_START_LINE
				ELSE
					conv.eAmbConv = ACS_FIN
				ENDIF
			ELSE
				//not implemented 
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail = FR_NONE)
	TRIGGER_MUSIC_EVENT("SOL2_FAIL")
	SAFE_REMOVE_BLIP(biBlip)
	// B*1911335 - needs custom route to navigate player inside the studio
	IF bIsCustomGPSRouteActive
		SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		CLEAR_GPS_MULTI_ROUTE()
		bIsCustomGPSRouteActive = FALSE
		#IF IS_DEBUG_BUILD  SK_PRINT(" bIsCustomGPSRouteActive cleaned up")  #ENDIF
	ENDIF
	SAFE_REMOVE_BLIP(mRocco.blip)
	SAFE_REMOVE_BLIP(mGunMan.blip)	
	IF NOT bFailConvSet
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
	ENDIF

	SWITCH fail		
		CASE FR_NONE
			
		BREAK
		
		CASE FR_ROCO_ESCAPED
			sFailReason = "SOL2_FGOTAWAY"
			IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "SOL2_MLT", CONV_PRIORITY_MEDIUM)
				bFailConvSet = TRUE
			ENDIF
		BREAK
		
		CASE FR_SOLOMON_KILLED
			sFailReason = "SOL2_FSOLDEAD"
		BREAK
	ENDSWITCH
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the mission check point 
PROC SET_CHECKPOINT()
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILLEDROCO, "Phone call", TRUE)
	#IF IS_DEBUG_BUILD  SK_PRINT("Set checkpoint to CP_KILLEDROCO")  #ENDIF
ENDPROC

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads a scene around a point used to the player 
///    seeing the map stream in when starting a replay or debug skipping
/// PARAMS:
///    pos - the position to stream the scene around at
///    rad - the radius of the scene to load
PROC LOAD_SCENE_SKIP(VECTOR pos, FLOAT rad)
	NEW_LOAD_SCENE_START_SPHERE(pos, rad)
	INT i_load_scene_timer = GET_GAME_TIMER()
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED()) AND (GET_GAME_TIMER() - i_load_scene_timer < 12000)
		WAIT(0)
	ENDWHILE
	NEW_LOAD_SCENE_STOP()
ENDPROC

/// PURPOSE:
///    Adds peds for dialogue based off the Mission state
PROC ADD_PEDS_FOR_DIALOGUE()
ENDPROC

PROC CREATE_RELATIONSHIP_GROUP()
	#IF IS_DEBUG_BUILD  SK_PRINT("CREATE_RELATIONSHIP_GROUP")  #ENDIF
	ADD_RELATIONSHIP_GROUP("SolomonREL", HASH_SOLOMON)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, HASH_SOLOMON, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, HASH_SOLOMON)
	IF IS_PED_UNINJURED(piSolomon)
		SET_PED_RELATIONSHIP_GROUP_HASH(piSolomon, HASH_SOLOMON)
	ENDIF
ENDPROC



/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	SWITCH missionReq
		CASE RQ_NONE
			IF ARE_VECTORS_ALMOST_EQUAL(pos, vSafeVec)
			AND dir = 0.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_TEXT
			IF ARE_STRINGS_EQUAL(sGodText, "NONE")
			AND ARE_STRINGS_EQUAL(sTextBlock, "NONE")
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TEXT NOT INITIALISED sGodText AND sTextBlock = NONE")
				#ENDIF
				RETURN TRUE
			ENDIF

			REQUEST_ADDITIONAL_TEXT(sGodText, MISSION_TEXT_SLOT)

			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TEXT")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("TEXT FAILED")
			#ENDIF
		BREAK
		
		CASE RQ_CUTSCENE_INTRO
			IF NOT HAS_CUTSCENE_LOADED()
				RC_REQUEST_CUTSCENE("SOL_2_INT_ALT1")
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_CAR_ROCO
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
				mCarRoco.id = g_sTriggerSceneAssets.veh[0]
				
				IF IS_VEHICLE_OK(mCarRoco.id)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mCarRoco.Mod, TRUE)
					SET_VEHICLE_COLOUR_COMBINATION(mCarRoco.id, 4)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_CAR_ROCO")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_VEHICLE(mCarRoco.id, mCarRoco.Mod, mCarRoco.vPos, mCarRoco.fDir)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mCarRoco.Mod, TRUE)
					SET_VEHICLE_COLOUR_COMBINATION(mCarRoco.id, 4)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_CAR_ROCO")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_CAR_ROCO FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_PLAYER_CAR
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[1])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
				mPlayerCar.id = g_sTriggerSceneAssets.veh[1]

				IF IS_VEHICLE_OK(mPlayerCar.id)
					SET_VEHICLE_COLOUR_COMBINATION(mPlayerCar.id, 1)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mPlayerCar.Mod, TRUE)
					REQUEST_VEHICLE_ASSET(mPlayerCar.Mod)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_PLAYER_CAR")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_VEHICLE(mPlayerCar.id, mPlayerCar.Mod, mPlayerCar.vPos, mPlayerCar.fDir)
					SET_VEHICLE_COLOUR_COMBINATION(mPlayerCar.id, 1)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mPlayerCar.Mod, TRUE)
					REQUEST_VEHICLE_ASSET(mPlayerCar.Mod)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_PLAYER_CAR")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_PLAYER_CAR FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_ROCO
			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[0])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
				mRocco.id = g_sTriggerSceneAssets.ped[0]
				IF IS_PED_UNINJURED(mRocco.id)
					STOP_PED_SPEAKING(mRocco.id, TRUE)
					SET_PED_CONFIG_FLAG(mRocco.id, PCF_AllowToBeTargetedInAVehicle, FALSE)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, mRocco.id, "ROCCO")
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_ROCO")
					#ENDIF
					SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(mRocco.id, TRUE)
					SET_PED_MODEL_IS_SUPPRESSED(IG_ROCCOPELOSI, TRUE)
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_PED(mRocco.id, mRocco.Mod, mRocco.vPos, mRocco.fDir)
					STOP_PED_SPEAKING(mRocco.id, TRUE)
					SET_PED_CONFIG_FLAG(mRocco.id, PCF_AllowToBeTargetedInAVehicle, FALSE)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, mRocco.id, "ROCCO")
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_ROCO")
					#ENDIF
					SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(mRocco.id, TRUE)
					SET_PED_MODEL_IS_SUPPRESSED(mRocco.Mod, TRUE)
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_ROCO FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_GUN_MAN
			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[1])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
				mGunMan.id = g_sTriggerSceneAssets.ped[1]
				
				IF IS_PED_UNINJURED(mGunMan.id)
					STOP_PED_SPEAKING(mGunMan.id, TRUE)
					SET_PED_CONFIG_FLAG(mGunMan.id, PCF_AllowToBeTargetedInAVehicle, FALSE)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, mGunMan.id, "GIANNI")
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_GUN_MAN")
					#ENDIF
					GIVE_WEAPON_TO_PED(mGunMan.id, WEAPONTYPE_PISTOL, INFINITE_AMMO)
					SET_PED_MODEL_IS_SUPPRESSED(mGunMan.Mod, TRUE)
					SET_PED_ACCURACY(mGunMan.id, 30)
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_PED(mGunMan.id, mGunMan.Mod, mGunMan.vPos, mGunMan.fDir)
					STOP_PED_SPEAKING(mGunMan.id, TRUE)
					SET_PED_CONFIG_FLAG(mGunMan.id, PCF_AllowToBeTargetedInAVehicle, FALSE)
					GIVE_WEAPON_TO_PED(mGunMan.id, WEAPONTYPE_PISTOL, INFINITE_AMMO)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, mGunMan.id, "GIANNI")
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_GUN_MAN")
					#ENDIF
					SET_PED_ACCURACY(mGunMan.id, 30)
					SET_PED_MODEL_IS_SUPPRESSED(mGunMan.Mod, TRUE)
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_GUN_MAN FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_SOLOMON
			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[2])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2], TRUE, TRUE)
				piSolomon = g_sTriggerSceneAssets.ped[2]
	
				IF IS_PED_UNINJURED(piSolomon)
					STOP_PED_SPEAKING(piSolomon, TRUE)
					SET_PED_CAN_BE_TARGETTED(piSolomon, FALSE)
					SET_ENTITY_PROOFS(piSolomon, TRUE, FALSE, FALSE, FALSE, TRUE)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_SOLOMON")
					#ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_PED(piSolomon, IG_SOLOMON, <<-1031.94, -544.25, 35.29>>, 40.97)
					STOP_PED_SPEAKING(piSolomon, TRUE)
					SET_PED_DEFAULT_COMPONENT_VARIATION(piSolomon)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_SOLOMON")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_SOLOMON FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_WAYPOINT
			REQUEST_WAYPOINT_RECORDING("Sol1Roco1")
			IF GET_IS_WAYPOINT_RECORDING_LOADED("Sol1Roco1")
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_WAYPOINT")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_WAYPOINT FAILED")
			#ENDIF
		BREAK
		
		CASE RQ_INJURED_CLIP
			REQUEST_ANIM_SET("move_injured_generic")
			IF HAS_ANIM_SET_LOADED("move_injured_generic")
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_INJURED_CLIP")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_INJURED_CLIP FAILED")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC TELL_ROCO_BURNOUT(FLOAT StartSpeed = 5.0, FLOAT StartCruise = 10.0)
	IF IS_VEHICLE_OK(mCarRoco.id)
	AND IS_PED_UNINJURED(mRocco.id)
		SET_VEHICLE_ENGINE_ON(mCarRoco.id, TRUE, TRUE)
		SET_VEHICLE_FORWARD_SPEED(mCarRoco.id, StartSpeed)
		SET_ENTITY_LOAD_COLLISION_FLAG(mRocco.id, TRUE)
		TASK_VEHICLE_DRIVE_TO_COORD(mRocco.id, mCarRoco.id, <<-1106.3654, -579.0142, 31.1366>>, StartCruise, DRIVINGSTYLE_ACCURATE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS|DF_UseStringPullingAtJunctions|DF_UseSwitchedOffNodes,  2, 0.5)
		SET_VEHICLE_TYRES_CAN_BURST(mCarRoco.id, FALSE)
		iMissionTimer = GET_GAME_TIMER()
		eRocoState = RS_BURNOUT
	ENDIF
ENDPROC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	ADD_PEDS_FOR_DIALOGUE()
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_TEXT, vSafeVec)
					IF NOT IS_REPLAY_IN_PROGRESS()
						IF SETUP_STAGE_REQUIREMENTS(RQ_ROCO, mRocco.vPos, mRocco.fDir)
						AND SETUP_STAGE_REQUIREMENTS(RQ_GUN_MAN, mGunMan.vPos, mGunMan.fDir)
						AND SETUP_STAGE_REQUIREMENTS(RQ_CAR_ROCO, mCarRoco.vPos, mCarRoco.fDir)
						AND SETUP_STAGE_REQUIREMENTS(RQ_SOLOMON, mCarRoco.vPos, mCarRoco.fDir)
						AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, mPlayerCar.vPos, mPlayerCar.fDir)
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SOLOMON_2)
							bCleanedupSolomon = FALSE
							bCleanedupPlayerCar = FALSE
							IF IS_PED_UNINJURED(mRocco.id)
								INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mRocco.id)
							ENDIF
	
							IF IS_PED_UNINJURED(mGunMan.id)
								INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mGunMan.id)
							ENDIF

							RC_REQUEST_CUTSCENE("SOL_2_INT_ALT1")
							#IF IS_DEBUG_BUILD  
								SK_PRINT("MS_SET_UP") 
							#ENDIF

							RETURN TRUE //all mission stage requirements set up return true and activate stage
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD  
							SK_PRINT(" Replay is active just loaded text MS_SET_UP") 
						#ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED") 
			#ENDIF
		BREAK

		CASE MS_INTRO
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				RESET_ALL()
				IF SETUP_STAGE_REQUIREMENTS(RQ_CUTSCENE_INTRO, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_ROCO, mRocco.vPos, mRocco.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_GUN_MAN, mGunMan.vPos, mGunMan.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CAR_ROCO, mCarRoco.vPos, mCarRoco.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_INJURED_CLIP, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SOLOMON, mCarRoco.vPos, mCarRoco.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_WAYPOINT, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, mPlayerCar.vPos, mPlayerCar.fDir)
					SET_VEHICLE_COLOUR_COMBINATION(mPlayerCar.id, 1)
					#IF IS_DEBUG_BUILD  
						SK_PRINT("MS_INTRO") 
					#ENDIF
					bCleanedupSolomon = FALSE
					bCleanedupPlayerCar = FALSE
					IF NOT IS_REPLAY_IN_PROGRESS()
						WAIT_FOR_WORLD_TO_LOAD(	mRocco.vPos, 75 )
					ENDIF
					IF IS_PED_UNINJURED(mRocco.id)
						INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mRocco.id)
					ENDIF

					IF IS_PED_UNINJURED(mGunMan.id)
						INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mGunMan.id)
					ENDIF
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
//					RC_END_Z_SKIP()
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_INJURED_CLIP, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_WAYPOINT, vSafeVec)
					RETURN TRUE //all mission stage requirements set up return true and activate stage
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_INTRO, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_CHASE
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_ROCO, mRocco.vPos, mRocco.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_GUN_MAN, mGunMan.vPos, mGunMan.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CAR_ROCO, mCarRoco.vPos, mCarRoco.fDir)
				AND SPAWN_VEHICLE(mPlayerCar.id, mPlayerCar.Mod, mPlayerCar.vPos, mPlayerCar.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_INJURED_CLIP, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SOLOMON, mCarRoco.vPos, mCarRoco.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_WAYPOINT, vSafeVec)
					SET_VEHICLE_COLOUR_COMBINATION(mPlayerCar.id, 1)
					bCleanedupSolomon = FALSE
					bCleanedupPlayerCar = FALSE
					RESET_ALL()

					IF IS_PED_UNINJURED(piSolomon)
						SET_PED_MOVEMENT_CLIPSET(piSolomon, "move_injured_generic")
						TASK_FOLLOW_NAV_MESH_TO_COORD(piSolomon, vDropOffLoc, PEDMOVEBLENDRATIO_WALK, 60000)
					ENDIF
					
					IF IS_PED_UNINJURED(mRocco.id)
						SET_PED_INTO_VEHICLE(mRocco.id, mCarRoco.id)
					ENDIF

					IF IS_PED_UNINJURED(mGunMan.id)
						SET_PED_INTO_VEHICLE(mGunMan.id, mCarRoco.id, VS_FRONT_RIGHT)
					ENDIF

					SET_ENTITY_COORDS(mCarRoco.id, <<-1106.3654, -579.0142, 31.1366>>, TRUE, TRUE)
					SET_ENTITY_HEADING(mCarRoco.id, 114.9979)

					IF NOT IS_REPLAY_IN_PROGRESS()
						WAIT_FOR_WORLD_TO_LOAD(	<<-1036.3180, -546.2224, 34.1445>>, 75)
						SET_PED_POS(PLAYER_PED_ID(), <<-1038.9562, -547.5014, 34.0505>>, 125.2668)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					ENDIF
					

					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_PED_UNINJURED(mRocco.id)
							INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mRocco.id)
						ENDIF

						IF IS_PED_UNINJURED(mGunMan.id)
							INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mGunMan.id)
						ENDIF
						sbiMovieSetScenarioBlocker = ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS(<<-1176.52490, -550.91449, 27.73062>>, 15)
						CLEAR_AREA_OF_PEDS(<<-1176.52490, -550.91449, 27.73062>>, 9.22)
						bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_RUN)
						SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						TELL_ROCO_BURNOUT()
						RC_END_Z_SKIP()
					ELSE
						END_REPLAY_SETUP()
					ENDIF
					
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_PHONE_CALL, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_LEAVE_AREA
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bCleanedupSolomon = TRUE
				bCleanedupPlayerCar = FALSE
				IF NOT IS_REPLAY_BEING_SET_UP()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ELSE
					END_REPLAY_SETUP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_PHONE_CALL, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_PHONE_CALL
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SPAWN_VEHICLE(mPlayerCar.id, mPlayerCar.Mod, <<-1198.4720, -604.7609, 25.4568>>, 41.5163)
					SET_VEHICLE_COLOUR_COMBINATION(mPlayerCar.id, 1)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1, "DEFAULT_ACTION")
					bCleanedupSolomon = TRUE
					bCleanedupPlayerCar = FALSE
					
					IF NOT IS_REPLAY_IN_PROGRESS()
						WAIT_FOR_WORLD_TO_LOAD(	<<-1198.9615, -597.6102, 25.9498>>, 75)
						SET_PED_POS(PLAYER_PED_ID(), <<-1198.9615, -597.6102, 25.9498>>, 138.1940)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					ENDIF
					IF NOT IS_REPLAY_BEING_SET_UP()
						bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
						RC_END_Z_SKIP()
					ELSE
						END_REPLAY_SETUP()
					ENDIF
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_PHONE_CALL, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_RETURN_TO_STUDIO
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1, "DEFAULT_ACTION")
				bCleanedupSolomon = TRUE
				IF NOT IS_REPLAY_BEING_SET_UP()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ELSE
					END_REPLAY_SETUP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_RETURN_TO_STUDIO, FAILED") 
			#ENDIF
		BREAK

		CASE MS_OUTRO
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, FALSE)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1, "DEFAULT_ACTION")
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_PED_POS(PLAYER_PED_ID(), <<-1017.7021, -484.0121, 36.0903>>, 114.1774)
					WAIT_FOR_WORLD_TO_LOAD(	<<-1017.7021, -484.0121, 36.0903>>, 75)
				ENDIF
				bCleanedupSolomon = TRUE
				
				#IF IS_DEBUG_BUILD  SK_PRINT("Loading outro in skip setup")  #ENDIF
				IF NOT IS_REPLAY_BEING_SET_UP()
				ELSE
					END_REPLAY_SETUP()
				ENDIF

				REQUEST_CUTSCENE("sol_2_ext_concat")
				WHILE NOT HAS_CUTSCENE_LOADED()
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
					ENDIF
					#IF IS_DEBUG_BUILD  SK_PRINT("Loading outro in skip setup")  #ENDIF
					WAIT(0)
				ENDWHILE
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_OUTRO, FAILED") 
			#ENDIF
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sits in the NEXT_STAGE() function and moniters for assest that
///    should be unloaded based on the mission state
/// PARAMS:
///    state - the current state we should evaluate
PROC MONITER_UNLOAD_ASSETS(MISSION_STATE state)
	SWITCH state
		CASE MS_SET_UP
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Unloads all models
PROC UNLOAD_ALL_MODELS()
ENDPROC

/// PURPOSE:
///    Unloads all waypoints 
PROC UNLOAD_ALL_WAYPOINTS()
	REMOVE_WAYPOINT_RECORDING("Sol1Roco1")
ENDPROC

/// PURPOSE:
///    Unloads all anims
PROC UNLOAD_ANIMS()
	REMOVE_VEHICLE_ASSET(mPlayerCar.Mod)
ENDPROC

/// PURPOSE:
///    Unload all vehicle recordings 
PROC UNLOAD_ALL_CAR_RECS()
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBlip)
	SAFE_REMOVE_BLIP(mRocco.blip)
	SAFE_REMOVE_BLIP(mGunMan.blip)	
ENDPROC


/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
	SAFE_DELETE_PED(mRocco.id)
	SAFE_DELETE_PED(mGunMan.id)
	SAFE_DELETE_PED(piSolomon)
	SAFE_DELETE_VEHICLE(mCarRoco.id)
	SAFE_DELETE_VEHICLE(mPlayerCar.id)
	SAFE_RELEASE_OBJECT(oiCone) //grabbed from the world so cant be deleted
ENDPROC

PROC RELEASE_ALL()
	SAFE_RELEASE_PED(mRocco.id)
	SAFE_RELEASE_PED(piSolomon, TRUE, TRUE)
	SAFE_RELEASE_PED(mGunMan.id, TRUE, TRUE)
	SAFE_RELEASE_VEHICLE(mCarRoco.id)
	SAFE_RELEASE_VEHICLE(mPlayerCar.id)
	SAFE_RELEASE_OBJECT(oiCone)
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF

	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()
	WAIT_FOR_CUTSCENE_TO_STOP()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	// B*1911335 - needs custom route to navigate player inside the studio
	IF bIsCustomGPSRouteActive
		SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		CLEAR_GPS_MULTI_ROUTE()
		bIsCustomGPSRouteActive = FALSE
		#IF IS_DEBUG_BUILD  SK_PRINT(" bIsCustomGPSRouteActive cleaned up")  #ENDIF
	ENDIF
	REMOVE_BLIPS()
	UNLOAD_ALL_MODELS()
	UNLOAD_ALL_WAYPOINTS()
	UNLOAD_ANIMS()
	UNLOAD_ALL_CAR_RECS()
	REMOVE_RELATIONSHIP_GROUP(HASH_SOLOMON)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1, "DEFAULT_ACTION")
	ENDIF
	IF bDelAll
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiMovieSetScenarioBlocker)
	
	IF DOES_CAM_EXIST(camMain)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(camMain)
	ENDIF
ENDPROC

PROC TOGGLE_PLAYER_SAFE_FOR_CAMERA_CUT(BOOL bOn)
	IF bOn
		CPRINTLN(DEBUG_MISSION, "Replay Controller is setting player as invincible.")
		
		IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)

			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				SET_PED_CAN_BE_SHOT_IN_VEHICLE(PLAYER_PED_ID(), FALSE) // this command is SP only
			ENDIF
			SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)

			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(PLAYER_PED_ID())
		ENDIF
		
		SET_MAX_WANTED_LEVEL(0)
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		AND NOT IS_PLAYER_DEAD(PLAYER_ID())
			#IF IS_DEBUG_BUILD SK_PRINT("Wanted level cleared ") #ENDIF
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
	ELSE
		IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)

			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				SET_PED_CAN_BE_SHOT_IN_VEHICLE(PLAYER_PED_ID(), TRUE) // this command is SP only
			ENDIF
			SET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID(), TRUE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		ENDIF
		SET_MAX_WANTED_LEVEL(5)
	ENDIF
ENDPROC


/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()
//	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT,TRUE)
	
	SERVICES_TOGGLE(TRUE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(mCarRoco.Mod, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(mPlayerCar.Mod, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(mGunMan.Mod, FALSE)	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	IF IS_AUDIO_SCENE_ACTIVE("SOL_2_CAR_CHASE")
		STOP_AUDIO_SCENE("SOL_2_CAR_CHASE")
	ENDIF
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
//	IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_2_FOCUS_CAM")
//		START_AUDIO_SCENE("SOL_2_FOCUS_CAM")
//	ENDIF
	SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, TRUE)
	IF IS_REPEAT_PLAY_ACTIVE()
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO, FALSE)
	ENDIF
	TOGGLE_PLAYER_SAFE_FOR_CAMERA_CUT(FALSE)
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	MONITER_UNLOAD_ASSETS(eMissionState)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	
	Mission_Flow_Mission_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Checks to see if the player fail reason needs updating
///    Ranked in order of importance with most important at the top
PROC UPDATE_FAIL()

ENDPROC

//PROC SET_FAIL_WARP()
//	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 758.9492, -973.7745, 24.3280 >>, 179.8099)
//	SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< 758.9210, -977.2075, 24.3189 >>, 282.4565)
//ENDPROC

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT
			IF bFailConvSet
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					bFailConvSet = FALSE
				ENDIF
			ELSE
				CLEAR_PRINTS()
				CLEAR_HELP()
				REMOVE_BLIPS()		

				IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
					MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)  
				ELSE
					MISSION_FLOW_MISSION_FAILED()
				ENDIF

				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//
				
				DELETE_ALL()
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
//	bLoadingFinCutscene = FALSE
	bObjectiveShown = FALSE
	eState = SS_INIT

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) // get out of vehicle
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
			ENDIF
		ENDIF
	ENDIF

	CLEANUP() //delete everything
ENDPROC

/// PURPOSE:
///    Fills an new MYPED
/// PARAMS:
///    pos - position the ped is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    a newly initialised MYPED
FUNC MYPED FILL_PED(VECTOR pos, FLOAT dir, MODEL_NAMES mod)
	MYPED mTempPed
	
	mTempPed.vPos = pos
	mTempPed.fDir = dir
	mTempPed.Mod = mod
	
	RETURN mTempPed 
ENDFUNC

/// PURPOSE:
///    Fills a new MYVEHICLE
/// PARAMS:
///    pos - the position the vehicle is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    A newly initialised MYVEHICLE
FUNC MYVEHICLE FILL_VEHICLE(VECTOR pos, FLOAT dir, MODEL_NAMES mod)
	MYVEHICLE mTempVeh
	
	mTempVeh.vPos = pos
	mTempVeh.fDir = dir
	mTempVeh.Mod = mod
	
	RETURN mTempVeh 
ENDFUNC


/// PURPOSE:
///    Fills a new MYOBJECT
/// PARAMS:
///    pos - the position the object is spawned at
///    rot - the rotation the object is spawned with 
///    mod - the model used to create the object
/// RETURNS:
///    A newly initialised MYOBJECT
FUNC MYOBJECT FILL_OBJECT(VECTOR pos, VECTOR rot, MODEL_NAMES mod)
	MYOBJECT mTempObj
	
	mTempObj.vPos = pos
	mTempObj.vRot = rot
	mTempObj.Mod = mod
	
	RETURN mTempObj 
ENDFUNC


/// PURPOSE:
///    Adds points to a poly 
/// PARAMS:
///    polys - The points to be added to the poly 
///    count - the number of points to be added to the poly 
PROC POPULATE_POLY_CHECKS(VECTOR &polys[], INT count = MAX_POLY_TEST_VERTS)
	 

	OPEN_TEST_POLY(mAreaCheck1)
	INT i
	
	FOR i = 0 TO (count-1)
		ADD_TEST_POLY_VERT(mAreaCheck1, polys[i])
	ENDFOR
	CLOSE_TEST_POLY(mAreaCheck1)
	
	COPY_EXPANDED_POLY(mAreaCheck2, mAreaCheck1, 50)

ENDPROC

/// PURPOSE:
///    Creates a poly check area using a local array
PROC CREATE_POLY_CHECKS()
	VECTOR polys[MAX_POLY_TEST_VERTS]
	polys[0] = << -1063.24060, -473.99036, 37.17500 >>
	polys[1] = << -1022.24738, -548.65704, 35.35321 >>
	polys[2] = << -1174.41553, -628.83820, 22.34404 >>
	polys[3] = << -1241.92639, -550.53107, 28.28967 >>
	POPULATE_POLY_CHECKS(polys, 4)
ENDPROC

/// PURPOSE:
///    Fill in a new dialogue holder
/// PARAMS:
///    sConv - the coversation lable to store in this holder
///    iExpireBit - the bit that will be used to expire the conversation
/// RETURNS:
///    a newly initialised DIALOGUE_HOLDER
FUNC DIALOGUE_HOLDER FILL_DIALOGUE(STRING sConv, INT iExpireBit)
	DIALOGUE_HOLDER TempDial
	
	TempDial.sConv = sConv
	TempDial.iExpireBit = iExpireBit
	
	RETURN TempDial
	
ENDFUNC

PROC POPULATE_VEHICLES()
	mCarRoco = FILL_VEHICLE(<<-1041.64, -545.08, 34.81>>, 116.57, FUGITIVE)
	mPlayerCar = FILL_VEHICLE(<<-1046.1864, -554.2908, 33.4636>>, 101.5845, BALLER2)
ENDPROC

PROC POPULATE_PEDS()
	mRocco = FILL_PED(<<-1032.97, -544.14, 35.30>>, -103.17, IG_ROCCOPELOSI)
	mGunMan = FILL_PED(<<-1031.72, -543.42, 35.26>>, 110.43, U_M_Y_GUIDO_01) //G_M_Y_AZTECA_01 U_M_Y_GUIDO_01
ENDPROC

/// PURPOSE:
///    Initialises all variables and structs
PROC POPULATE_STUFF()
	POPULATE_VEHICLES()
	POPULATE_PEDS()
	CREATE_POLY_CHECKS()
	ADD_CONTACT_TO_PHONEBOOK (CHAR_SOLOMON, MICHAEL_BOOK)
ENDPROC


/// PURPOSE:
///    Sets the Game world time after a replay/shit skip
PROC SET_TIME_FOR_REPLAY()
	IF g_bShitskipAccepted
	ELSE
	ENDIF

ENDPROC

///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			//DO_SCREEN_FADE_OUT(0)
//			#IF IS_DEBUG_BUILD
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
//			#ENDIF
			
			
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF
			
			IF SETUP_MISSION_STAGE(eMissionState) 			
				
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "MS_INTRO"
					s_skip_menu[1].sTxtLabel = "MS_CHASE"
					s_skip_menu[2].sTxtLabel = "MS_PHONE_CALL"
					s_skip_menu[3].sTxtLabel = "MS_LEAVE_AREA"
					s_skip_menu[4].sTxtLabel = "MS_RETURN_TO_STUDIO"
					s_skip_menu[5].sTxtLabel = "MS_OUTRO"
				#ENDIF


	//			ADD_RELATIONSHIP_GROUP("MYFRIEND", HASH_FRIENDS)
				
				SERVICES_TOGGLE(FALSE)
				
				
				IF IS_REPLAY_IN_PROGRESS()
					iReplayStage = GET_REPLAY_MID_MISSION_STAGE()	
					
					IF g_bShitskipAccepted = TRUE
						iReplayStage++ // player is skipping this stage
					ENDIF
					SWITCH iReplayStage
						CASE 0
							JUMP_TO_STAGE(MS_CHASE)
						BREAK
						
						CASE CP_KILLEDROCO
							JUMP_TO_STAGE(MS_PHONE_CALL)
						BREAK
						
						CASE 3
							JUMP_TO_STAGE(MS_OUTRO)
						BREAK
					ENDSWITCH
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
						SET_PED_POS(PLAYER_PED_ID(), <<-1036.3180, -546.2224, 34.0824>>, 103.5358)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						WAIT_FOR_WORLD_TO_LOAD(	<<-1036.3180, -546.2224, 34.1445>>, 75)
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO, TRUE)
						bJumpSkip = TRUE
					ENDIF
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC

PROC MONITOR_ROCCO_SPEED()
	IF eRocoState > RS_GO_TO_GATE
	AND eRocoState <> RS_FLEE_ON_FOOT
	AND eRocoState <> RS_DRIVING_DOWN_PEIR
		IF IS_VEHICLE_OK(mCarRoco.id)
			IF NOT bRocoSpeadUp
				IF IS_ENTITY_IN_RANGE_ENTITY(mRocco.id, PLAYER_PED_ID(), 120)
					TASK_VEHICLE_MISSION_PED_TARGET(mRocco.id, mCarRoco.id, PLAYER_PED_ID(), MISSION_FLEE, 50, DRIVINGMODE_AVOIDCARS_RECKLESS|DF_UseStringPullingAtJunctions, 200, 2)
					bRocoSpeadUp = TRUE
				ENDIF
			ELSE
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(mRocco.id, PLAYER_PED_ID(), 145)
					TASK_VEHICLE_MISSION_PED_TARGET(mRocco.id, mCarRoco.id, PLAYER_PED_ID(), MISSION_FLEE, 35, DRIVINGMODE_AVOIDCARS_RECKLESS|DF_UseStringPullingAtJunctions, 200, 2)
					bRocoSpeadUp = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///     Checks to see if the mRoutedVehicle car has burst tyres
/// RETURNS:
///    TRUE if the mRoutedVehicle car has burst tyres.
FUNC BOOL HAS_PLAYER_BURST_ROCCO_TYRES()
	INT i
	INT iDamagedTyres = 0 
	
	FOR i = (ENUM_TO_INT(SC_WHEEL_CAR_FRONT_LEFT)) TO (ENUM_TO_INT(SC_WHEEL_CAR_REAR_RIGHT))
		IF IS_VEHICLE_TYRE_BURST(mCarRoco.id, INT_TO_ENUM(SC_WHEEL_LIST, i))
			iDamagedTyres++
		ENDIF
	ENDFOR
	IF iDamagedTyres >= 2
		CPRINTLN(DEBUG_MISSION, "ROCCO vehicle damaged out tryes: ", iDamagedTyres)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Monitors the hunters car for damage and plays hunter dialogue if appropriate.
FUNC BOOL MONITER_PLAYER_DAMAGING_ROCCO_CAR()
	IF IS_VEHICLE_OK(mCarRoco.id)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mCarRoco.id, PLAYER_PED_ID())
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mCarRoco.id)
			IF HAS_PLAYER_BURST_ROCCO_TYRES()
			OR GET_ENTITY_HEALTH(mCarRoco.id) < 450.0
			OR GET_VEHICLE_ENGINE_HEALTH(mCarRoco.id) < 450.0
			OR GET_VEHICLE_PETROL_TANK_HEALTH(mCarRoco.id) < 450.0
			OR IS_VEHICLE_STUCK_TIMER_UP(mCarRoco.id, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(mCarRoco.id, VEH_STUCK_JAMMED, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(mCarRoco.id, VEH_STUCK_HUNG_UP, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(mCarRoco.id, VEH_STUCK_ON_SIDE, ROOF_TIME)
				CPRINTLN(DEBUG_MISSION, "ROCCO vehicle damaged out values to follow: ")
				CPRINTLN(DEBUG_MISSION, "vehicle damaged out Health: ", GET_ENTITY_HEALTH(mCarRoco.id))
				CPRINTLN(DEBUG_MISSION, "vehicle damaged out Engine: ", GET_VEHICLE_ENGINE_HEALTH(mCarRoco.id))
				CPRINTLN(DEBUG_MISSION, "vehicle damaged out Tank: ", GET_VEHICLE_PETROL_TANK_HEALTH(mCarRoco.id))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC 

PROC MONITOR_DAMAGE()
	IF IS_VEHICLE_OK(mCarRoco.id)
		IF MONITER_PLAYER_DAMAGING_ROCCO_CAR()
			GIVE_PED_FLEE_ORDER(mRocco.id)
			eRocoState = RS_FLEE_ON_FOOT
		ENDIF
	ELSE
		GIVE_PED_FLEE_ORDER(mRocco.id)
		eRocoState = RS_FLEE_ON_FOOT
	ENDIF
ENDPROC

PROC MONITOR_ROCCO_BLIP()
	IF IS_VEHICLE_OK(mCarRoco.id)
		IF IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id, TRUE)
			IF DOES_BLIP_EXIST(mRocco.blip)
				IF NOT bRoccoBlipedInVehicle
					SAFE_REMOVE_BLIP(mRocco.blip)
				ENDIF
			ELSE
				IF NOT bRoccoBlipedInVehicle
					ADD_SAFE_BLIP_TO_VEHICLE(mRocco.blip, mCarRoco.id)
					bRoccoBlipedInVehicle = TRUE
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(mRocco.blip)
				IF bRoccoBlipedInVehicle
					SAFE_REMOVE_BLIP(mRocco.blip)
					bRoccoBlipedInVehicle = FALSE
				ENDIF
			ELSE
				ADD_SAFE_BLIP_TO_PED(mRocco.blip, mRocco.id)
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(mRocco.blip)
			IF bRoccoBlipedInVehicle
				SAFE_REMOVE_BLIP(mRocco.blip)
				bRoccoBlipedInVehicle = FALSE
			ENDIF
		ELSE
			ADD_SAFE_BLIP_TO_PED(mRocco.blip, mRocco.id)
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_ROCCO_ENTER_PEIR()
	IF eRocoState <> RS_DRIVING_DOWN_PEIR
		IF IS_ENTITY_IN_ANGLED_AREA(mRocco.id, <<-1737.93518, -1125.93896, 12.01733>>, <<-1751.79639, -1114.70862, 16.01720>>, 8.77)
			IF IS_VEHICLE_OK(mCarRoco.id)
				TASK_VEHICLE_MISSION_COORS_TARGET(mRocco.id, mCarRoco.id, <<-1844.91626, -1224.45081, 12.01705>>, MISSION_GOTO, 50, DRIVINGMODE_AVOIDCARS|DF_UseSwitchedOffNodes, 5, 2)
				eRocoState = RS_DRIVING_DOWN_PEIR
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ROCO_STATE()
	IF IS_PED_UNINJURED(mRocco.id)
		MONITOR_ROCCO_BLIP()
		MONITOR_ROCCO_ENTER_PEIR()
		MONITOR_ROCCO_SPEED()
		SWITCH eRocoState
			CASE RS_BURNOUT
				IF (GET_GAME_TIMER() - iMissionTimer) > 250
					IF IS_VEHICLE_OK(mCarRoco.id)
						SET_VEHICLE_BURNOUT(mCarRoco.id, FALSE)
					ENDIF
					iMissionTimer = -1
					eRocoState = RS_BURNOUT_GOTO
				ENDIF
			BREAK

			CASE RS_BURNOUT_GOTO
				IF IS_VEHICLE_OK(mCarRoco.id)
					IF IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id)
						IF IS_ENTITY_IN_RANGE_COORDS(mRocco.id, <<-1106.3654, -579.0142, 31.1366>>, 8)
							//TASK_VEHICLE_DRIVE_TO_COORD(mRocco.id, mCarRoco.id, <<-1216.03638, -579.46979, 26.22016>>, 15, DRIVINGSTYLE_ACCURATE, DUMMY_MODEL_FOR_SCRIPT, DF_UseStringPullingAtJunctions|DF_UseSwitchedOffNodes,  5, 0.5)
							GIVE_WAYPOINT_TASK(mRocco.id, "Sol1Roco1", 0, EWAYPOINT_START_FROM_CLOSEST_POINT|EWAYPOINT_USE_TIGHTER_TURN_SETTINGS)
							//VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED()
							eRocoState = RS_GO_TO_GATE
						ENDIF
					ELSE
					
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
						GIVE_PED_FLEE_ORDER(mRocco.id)
//						IF DOES_BLIP_EXIST(mRocco.blip)
//							SET_BLIP_SCALE(mRocco.blip, BLIP_SIZE_PED)
//						ENDIF
						eRocoState = RS_FLEE_ON_FOOT
					ENDIF
				ENDIF
			BREAK
			
			CASE RS_GO_TO_GATE
				IF IS_VEHICLE_OK(mCarRoco.id)
					IF IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id)
//						IF IS_ENTITY_IN_RANGE_COORDS(mRocco.id, <<-1216.03638, -579.46979, 26.22016>>, 6)
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(mCarRoco.id)
							IF GET_VEHICLE_WAYPOINT_PROGRESS(mCarRoco.id) > 54 //
								TASK_VEHICLE_MISSION_PED_TARGET(mRocco.id, mCarRoco.id, PLAYER_PED_ID(), MISSION_FLEE, 35, DRIVINGMODE_AVOIDCARS_RECKLESS|DF_UseStringPullingAtJunctions, 550, 2)
								SET_VEHICLE_TYRES_CAN_BURST(mCarRoco.id, TRUE)
								eRocoState = RS_FLEE_IN_CAR
							ENDIF
						ENDIF
					ELSE
						GIVE_PED_FLEE_ORDER(mRocco.id)
//						IF DOES_BLIP_EXIST(mRocco.blip)
//							SET_BLIP_SCALE(mRocco.blip, BLIP_SIZE_PED)
//						ENDIF
						eRocoState = RS_FLEE_ON_FOOT
					ENDIF
				ENDIF
			BREAK

			CASE RS_FLEE_IN_CAR
				MONITOR_DAMAGE()
				IF IS_VEHICLE_OK(mCarRoco.id)
					IF NOT IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id)
					OR IS_ENTITY_UPSIDEDOWN(mCarRoco.id)
					
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
						GIVE_PED_FLEE_ORDER(mRocco.id)
				
//						IF DOES_BLIP_EXIST(mRocco.blip)
//							SET_BLIP_SCALE(mRocco.blip, BLIP_SIZE_PED)
//						ENDIF
//
						eRocoState = RS_FLEE_ON_FOOT
					ENDIF
				ELSE
					GIVE_PED_FLEE_ORDER(mRocco.id)
//					IF DOES_BLIP_EXIST(mRocco.blip)
//						SET_BLIP_SCALE(mRocco.blip, BLIP_SIZE_PED)
//					ENDIF
					eRocoState = RS_FLEE_ON_FOOT
				ENDIF
			BREAK

			CASE RS_FLEE_ON_FOOT
				IF IS_PED_IN_ANY_VEHICLE(mRocco.id)
				AND NOT IsPedPerformingTask(mRocco.id, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
					TASK_LEAVE_ANY_VEHICLE(mRocco.id)
				ELSE
					GIVE_PED_FLEE_ORDER(mRocco.id, FALSE)
				ENDIF
			BREAK

			CASE RS_FIGHT
				
			BREAK
			
			CASE RS_DRIVING_DOWN_PEIR
				IF IS_ENTITY_IN_RANGE_COORDS(mRocco.id, <<-1844.91626, -1224.45081, 12.01705>>, 6)
					GIVE_PED_FLEE_ORDER(mRocco.id)
//					IF DOES_BLIP_EXIST(mRocco.blip)
//						SET_BLIP_SCALE(mRocco.blip, BLIP_SIZE_PED)
//					ENDIF
					eRocoState = RS_FLEE_ON_FOOT
				ENDIF
			BREAK
		ENDSWITCH
		
		IF eRocoState <> RS_FLEE_ON_FOOT
			IF IS_PED_BEING_JACKED(mRocco.id)
				GIVE_PED_FLEE_ORDER(mRocco.id)

//				IF DOES_BLIP_EXIST(mRocco.blip)
//					SET_BLIP_SCALE(mRocco.blip, BLIP_SIZE_PED)
//				ENDIF

				eRocoState = RS_FLEE_ON_FOOT
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(mGunMan.id)
		AND DOES_ENTITY_EXIST(mRocco.id)
			IF IS_ENTITY_IN_RANGE_ENTITY(mRocco.id, mGunMan.id, 10)
			AND IS_ENTITY_IN_RANGE_ENTITY(mGunMan.id, PLAYER_PED_ID(), 10)
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, "SOL2_GRK", CONV_PRIORITY_MEDIUM)
			ENDIF
		ENDIF

		SAFE_REMOVE_BLIP(mRocco.blip)
		SAFE_RELEASE_PED(mRocco.id)
	ENDIF
ENDPROC



/// PURPOSE:
///    State for the mocap at the safe
PROC MOCAP_INTRO()
	IF eState = SS_ACTIVE
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() 
		AND bLoadingFinCutscene
		AND NOT bJumpSkip
			#IF IS_DEBUG_BUILD  SK_PRINT_INT("Player skiped cutscene at time = ", GET_CUTSCENE_TIME())  #ENDIF
//			eState = SS_SKIPPED
		ENDIF
	ENDIF
	
	//Load everything for the next stage while the cutscene is playing 
	IF NOT bLoadingFinCutscene
		SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
		IF SETUP_MISSION_STAGE(MS_INTRO, bJumpSkip)
			SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
			bLoadingFinCutscene = TRUE //loading done
		ENDIF
	ENDIF

	ROCO_STATE()

	SWITCH eState
		CASE SS_INIT
			IF NOT bJumpSkip
				SAFE_REMOVE_BLIP(biBlip)
				CLEAR_PRINTS()
				IF CAN_PLAYER_START_CUTSCENE(TRUE)
					#IF IS_DEBUG_BUILD  SK_PRINT("CAN_PLAYER_START_CUTSCENE(TRUE)")  #ENDIF
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
					ENDIF

					IF RC_IS_CUTSCENE_OK_TO_START()
						#IF IS_DEBUG_BUILD  SK_PRINT("RC_IS_CUTSCENE_OK_TO_START()")  #ENDIF
						
						IF IS_PED_UNINJURED(piSolomon)
						AND IS_VEHICLE_OK(mCarRoco.id)
						AND IS_PED_UNINJURED(mRocco.id)
						AND IS_PED_UNINJURED(mGunMan.id)
							REGISTER_ENTITY_FOR_CUTSCENE(piSolomon, "Soloman", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, CS_SOLOMON)
							REGISTER_ENTITY_FOR_CUTSCENE(mCarRoco.id, "getaway_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(mRocco.id, "Rocco", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(mGunMan.id, "Rocco_Goon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF

	//					IF IS_REPEAT_PLAY_ACTIVE()
	//						SAFE_FADE_SCREEN_IN_FROM_BLACK()
	//					ENDIF

						START_CUTSCENE()
						
						REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheBalladOfRocco")
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						WAIT(0)
						STOP_GAMEPLAY_HINT(TRUE)
						sbiMovieSetScenarioBlocker = ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS(<<-1176.52490, -550.91449, 27.73062>>, 15)
						CLEAR_AREA_OF_PEDS(<<-1176.52490, -550.91449, 27.73062>>, 9.22)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1026.33521, -539.10645, 100.62094>>, <<-1143.49854, -596.59631, 20.25008>>, 36.78, <<-1050.4880, -511.0545, 35.0386>>, 159.3100 , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1164.44482, -542.15021, 100.08894>>, <<-1214.17761, -581.91797, 22.32694>>, 83.46, <<-1050.4880, -511.0545, 35.0386>>, 159.3100 , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1050.4880, -511.0545, 35.0386>>, 159.3100)
						RC_START_CUTSCENE_MODE(<<-1046.6174, -516.3990, 35.0386>>)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)

						#IF IS_DEBUG_BUILD  SK_PRINT("INIT  MOCAP_INTRO")  #ENDIF
						eState = SS_ACTIVE	
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD
					SK_PRINT("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Michael)") 
				#ENDIF
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_RUN)
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Rocco")
				IF IS_PED_UNINJURED(mRocco.id)
				AND IS_VEHICLE_OK(mCarRoco.id)
					SET_PED_INTO_VEHICLE(mRocco.id, mCarRoco.id)
					#IF IS_DEBUG_BUILD
						SK_PRINT("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Rocco)") 
					#ENDIF
					FORCE_PED_AI_AND_ANIMATION_UPDATE(mRocco.id)
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Rocco_Goon")
				IF IS_PED_UNINJURED(mGunMan.id)
				AND IS_VEHICLE_OK(mCarRoco.id)
					SET_PED_INTO_VEHICLE(mGunMan.id, mCarRoco.id, VS_FRONT_RIGHT)
					#IF IS_DEBUG_BUILD
						SK_PRINT("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Rocco_Goon)") 
					#ENDIF
					FORCE_PED_AI_AND_ANIMATION_UPDATE(mGunMan.id)
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("getaway_car")
				IF IS_VEHICLE_OK(mCarRoco.id)
					IF NOT WAS_CUTSCENE_SKIPPED()
						#IF IS_DEBUG_BUILD	SK_PRINT("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(getaway_car)") #ENDIF
						TELL_ROCO_BURNOUT()
					ELSE
						// B*1689771 - removed IF NOT IS_SCREEN_FADED_OUT() check as screen might not be fully faded out so this wasn't getting set.
						#IF IS_DEBUG_BUILD	SK_PRINT("skipped cutscene rocco car warp exit state") #ENDIF
						SET_ENTITY_COORDS(mCarRoco.id, <<-1106.3654, -579.0142, 31.1366>>, TRUE, TRUE)
						SET_ENTITY_HEADING(mCarRoco.id, 114.9979)
						eRocoState = RS_BURNOUT_GOTO
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(7.3137)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4.2223)
			ENDIF

			IF NOT IS_CUTSCENE_PLAYING()
			AND bLoadingFinCutscene
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			bLoadingFinCutscene = FALSE
			RC_END_CUTSCENE_MODE()
			IF IS_PED_UNINJURED(piSolomon)
				SET_PED_MOVEMENT_CLIPSET(piSolomon, "move_injured_generic")
				TASK_FOLLOW_NAV_MESH_TO_COORD(piSolomon, vDropOffLoc, 0.2, 60000)
			ENDIF

			REPLAY_STOP_EVENT()
			
			NEXT_STAGE() //advance stage
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP  MOCAP_INTRO")  #ENDIF
		BREAK

		CASE SS_SKIPPED
				#IF IS_DEBUG_BUILD  SK_PRINT("SS_SKIPPED  MOCAP_INTRO")  #ENDIF
				IF IS_PED_UNINJURED(mRocco.id)
				AND IS_VEHICLE_OK(mCarRoco.id)
					IF NOT IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id)
						SET_PED_INTO_VEHICLE(mRocco.id, mCarRoco.id)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mRocco.id)
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(mGunMan.id)
				AND IS_VEHICLE_OK(mCarRoco.id)
					IF NOT IS_PED_IN_VEHICLE(mGunMan.id, mCarRoco.id)
						SET_PED_INTO_VEHICLE(mGunMan.id, mCarRoco.id, VS_FRONT_RIGHT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mGunMan.id)
					ENDIF
				ENDIF

				IF IS_VEHICLE_OK(mCarRoco.id)
					#IF IS_DEBUG_BUILD  SK_PRINT("warp rocco")  #ENDIF
					SET_ENTITY_COORDS(mCarRoco.id, <<-1106.3654, -579.0142, 31.1366>>, TRUE, TRUE)
					SET_ENTITY_HEADING(mCarRoco.id, 114.9979)
					eRocoState = RS_BURNOUT_GOTO
				ENDIF

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-4.2781)	
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT )
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_RUN)
				eState = SS_CLEANUP

		BREAK
	ENDSWITCH
ENDPROC



PROC GUNMAN_STATE()

	IF IS_PED_UNINJURED(mGunMan.id)
		IF eMissionState = MS_PHONE_CALL
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mGunMan.id, 200)
				IF NOT IS_ENTITY_ON_SCREEN(mGunMan.id)
					SAFE_REMOVE_BLIP(mGunMan.blip)
					SAFE_RELEASE_PED(mGunMan.id)
				ENDIF
			ENDIF
		ENDIF
		IF eGunmanState <> GS_COMBAT_ONFOOT
			IF NOT IS_PED_IN_ANY_VEHICLE(mGunman.id)
				TASK_COMBAT_PED(mGunMan.id, PLAYER_PED_ID())
				ADD_SAFE_BLIP_TO_PED(mGunMan.blip, mGunMan.id)
				eGunmanState = GS_COMBAT_ONFOOT
			ENDIF
		
			IF NOT IS_PED_UNINJURED(mRocco.id)
				TASK_COMBAT_PED(mGunMan.id, PLAYER_PED_ID())
				ADD_SAFE_BLIP_TO_PED(mGunMan.blip, mGunMan.id)
				eGunmanState = GS_COMBAT_ONFOOT
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(mRocco.id)
					TASK_COMBAT_PED(mGunMan.id, PLAYER_PED_ID())
					ADD_SAFE_BLIP_TO_PED(mGunMan.blip, mGunMan.id)
					eGunmanState = GS_COMBAT_ONFOOT
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH eGunmanState
			CASE GS_PERFORMING_DRIVEBY
				IF NOT IsPedPerformingTask(mGunMan.id, SCRIPT_TASK_DRIVE_BY)
					eGunmanState = GS_WAITING
				ENDIF
			BREAK

			CASE GS_WAITING
				IF NOT bStartedDriveBys
					IF NOT IS_POINT_IN_POLY_2D(mAreaCheck1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					AND NOT IS_POINT_IN_POLY_2D(mAreaCheck1, GET_ENTITY_COORDS(mGunMan.id))
						bStartedDriveBys = TRUE
					ENDIF
				ELSE
					IF IS_ENTITY_IN_RANGE_ENTITY(mGunMan.id, PLAYER_PED_ID(), 20)
						TASK_DRIVE_BY(mGunMan.id, PLAYER_PED_ID(), NULL, vSafeVec, 35, 65)
					ENDIF
				ENDIF
			BREAK

			CASE GS_COMBAT_ONFOOT
				IF NOT bExpireGunmanShout
					IF NOT IS_PED_IN_ANY_VEHICLE(mGunMan.id)
						DO_CONVO(mGunMan.id, bExpireGunmanShout, "SOL2_GSG", 20)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF NOT bKilledGunManConv
			DO_CONVO(NULL, bKilledGunManConv, "SOL2_MKT")
			IF IS_PED_UNINJURED(mRocco.id)
			AND DOES_ENTITY_EXIST(mGunMan.id)
				IF IS_ENTITY_IN_RANGE_ENTITY(mGunMan.id, mRocco.id, 10)
				AND IS_ENTITY_IN_RANGE_ENTITY(mRocco.id, PLAYER_PED_ID(), 10)
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, "SOL2_GGK", CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ENDIF
		SAFE_REMOVE_BLIP(mGunMan.blip)
		SAFE_RELEASE_PED(mGunMan.id)
	ENDIF
ENDPROC

PROC SETUP_DYNAMIC_MIXING()
	IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_2_CAR_CHASE")
		IF eMissionState = MS_CHASE
			IF IS_VEHICLE_OK(mCarRoco.id)
			AND IS_PED_UNINJURED(mRocco.id)
			AND IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mCarRoco.id, "SOL_2_ROCCOS_CAR")  
				START_AUDIO_SCENE("SOL_2_CAR_CHASE")
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(mRocco.id)
			IF IS_VEHICLE_OK(mCarRoco.id)
				IF NOT IS_PED_IN_ANY_VEHICLE(mRocco.id)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(mCarRoco.id) 
				ENDIF
			ENDIF
		ELSE
			IF iMissionTimer <> -1
				IF (GET_GAME_TIMER() - iMissionTimer) > 2000
					IF IS_VEHICLE_OK(mCarRoco.id)
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(mCarRoco.id)  
						STOP_AUDIO_SCENE("SOL_2_CAR_CHASE")
					ENDIF
				ENDIF
			ELSE
				iMissionTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_2_FOCUS_CAM")
		IF IS_GAMEPLAY_HINT_ACTIVE()
			START_AUDIO_SCENE("SOL_2_FOCUS_CAM")
		ENDIF
	ELSE
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			STOP_AUDIO_SCENE("SOL_2_FOCUS_CAM")
		ENDIF
	ENDIF


//	IF eMissionState = MS_CHASE
//		IF IS_AUDIO_SCENE_ACTIVE("SOL_2_CAR_CHASE")
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				IF IS_VEHICLE_OK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//					ADD_ENTITY_TO_AUDIO_MIX_GROUP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), "SOL_2_ROCCOS_CAR")  
//				ENDIF
//			ELSE
//				IF IS_PED_UNINJURED(mRocco.id)
//					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(GET_PLAYERS_LAST_VEHICLE())
//					ENDIF
//				ELSE
//					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(GET_PLAYERS_LAST_VEHICLE())  
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

FUNC FLOAT GET_SAFE_DISTANCE_BETWEEN_SOL_PLAYER()
	IF IS_PED_UNINJURED(piSolomon)
		
		RETURN GET_DISTANCE_BETWEEN_ENTITIES(piSolomon, PLAYER_PED_ID())
		
	ENDIF
	
	RETURN 50.0
ENDFUNC

PROC MONITOR_CHASE_SPEECH()
	IF iDelayChaseSpeech != -1
		IF GET_SAFE_DISTANCE_BETWEEN_SOL_PLAYER() > 20
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 30)
					IF (GET_GAME_TIMER() - iDelayChaseSpeech) > 5000
						IF GET_RANDOM_BOOL()
							IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "SOL2_CHASE", CONV_PRIORITY_MEDIUM)
								iDelayChaseSpeech = -1
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "SOL2_RGOA", CONV_PRIORITY_MEDIUM)
								iDelayChaseSpeech = -1
							ENDIF
						ENDIF
					ENDIF
				ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 365.0)
				AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 480.0)
					IF (GET_GAME_TIMER() - iDelayChaseSpeech) > 5000
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "SOL2_LOSE", CONV_PRIORITY_MEDIUM)
							iDelayChaseSpeech = -1
						ENDIF
					ENDIF
				ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 260.0)
				AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 365.0)
				AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 480.0)
					IF (GET_GAME_TIMER() - iDelayChaseSpeech) > 12000
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "SOL2_MLC", CONV_PRIORITY_MEDIUM)
							iDelayChaseSpeech = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDelayChaseSpeech = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_CLEANUP_SOLOMON_AND_PLAYER_CAR()
	IF NOT bCleanedupSolomon
		IF IS_PED_UNINJURED(piSolomon)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piSolomon, 100)
				bCleanedupSolomon = TRUE
				SAFE_DELETE_PED(piSolomon)
			ELSE
				IF NOT IS_PED_UNINJURED(mRocco.id)
					IF NOT IS_ENTITY_ON_SCREEN(piSolomon)
						bCleanedupSolomon = TRUE
						SAFE_DELETE_PED(piSolomon)
					ENDIF
				ELSE
					IF eState = SS_ACTIVE
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piSolomon, 15)
							IF iSolomonChatDelay = -1
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									iSolomonChatDelay = GET_GAME_TIMER()
								ENDIF
							ELSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF (GET_GAME_TIMER() - iSolomonChatDelay) > 15000
									AND NOT IS_MESSAGE_BEING_DISPLAYED()
										IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "SOL2_GO", CONV_PRIORITY_MEDIUM)
											iSolomonChatDelay = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
	ENDIF

	IF NOT bCleanedupPlayerCar
		IF IS_VEHICLE_OK(mPlayerCar.id)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mPlayerCar.id, 100)
				SAFE_DELETE_VEHICLE(mPlayerCar.id)
			ELSE
				IF NOT IS_VEHICLE_OK(mPlayerCar.id)
					SAFE_RELEASE_VEHICLE(mPlayerCar.id)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MONITOR_LOAD_OUTRO()
	IF eMissionState = MS_CHASE
		IF IS_POINT_IN_POLY_2D(mAreaCheck1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			REQUEST_CUTSCENE("sol_2_ext_concat")
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, FALSE)
			ENDIF
		ELSE
			IF HAS_CUTSCENE_LOADED()
				REMOVE_CUTSCENE()
				SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, TRUE)
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, DEFAULT_CUTSCENE_LOAD_DIST)
			REQUEST_CUTSCENE("sol_2_ext_concat")
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
			ENDIF
		ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, (DEFAULT_CUTSCENE_UNLOAD_DIST))
			IF HAS_CUTSCENE_LOADED()
				REMOVE_CUTSCENE()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHASE()
	SETUP_DYNAMIC_MIXING()
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 0, PLAYER_PED_ID(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, piSolomon, "SOLOMON")
//				DO_CONVO_WITH_OBJ(NULL, bMikeShout, "STOPROCCO", "SOL2_KILL")
				DISABLE_TAXI_HAILING()
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT CHASE")  #ENDIF
				
				IF IS_ENTITY_ALIVE(oiCone)
					SET_ENTITY_VISIBLE(oiCone, FALSE)
					SET_ENTITY_COLLISION(oiCone, FALSE)
					#IF IS_DEBUG_BUILD  SK_PRINT("platform alive it shall be released")  #ENDIF
				ELSE
					oiCone = GET_CLOSEST_OBJECT_OF_TYPE(<<-1172.76685, -555.19043, 27.75178 >>, 6, PROP_ROADCONE02A) //B*754248
				ENDIF

				eGunmanState = GS_WAITING
				IF IS_REPLAY_IN_PROGRESS()
					TRIGGER_MUSIC_EVENT("SOL2_RESTART1")
				ELSE
					TRIGGER_MUSIC_EVENT("SOL2_START")
				ENDIF

				IF DO_CONVO(NULL, bMikeShout, "STOPROCCO")
					SETTIMERA(0)
					#IF IS_DEBUG_BUILD SK_PRINT("BMIKE SHOUT") #ENDIF
					CREATE_RELATIONSHIP_GROUP()
					SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_DRIVING)
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1, "DEFAULT_ACTION")
					CLEAR_HD_AREA()
					
					REPLAY_RECORD_BACK_FOR_TIME(1.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					eState = SS_ACTIVE
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
			MONITOR_CLEANUP_SOLOMON_AND_PLAYER_CAR()
			ROCO_STATE()
			GUNMAN_STATE()
			MONITOR_LOAD_OUTRO()
			IF NOT bTriggeredVehicleMusic
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TRIGGER_MUSIC_EVENT("SOL2_CAR")
					bTriggeredVehicleMusic = TRUE
				ENDIF
			ENDIF

			IF TIMERA() < 1250
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_RUN)
			ELSE
				RESET_PLAYER_INPUT_GAIT(PLAYER_ID())
			ENDIF


			IF NOT IS_PED_UNINJURED(mRocco.id)
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				IF NOT bKilledRoccoConv

					IF DO_CONVO(NULL, bKilledRoccoConv, "SOL2_MKT")
						IF PREPARE_MUSIC_EVENT("SOL2_STOP")
							#IF IS_DEBUG_BUILD  SK_PRINT("TRIGGER_MUSIC_EVENT(SOL2_STOP)")  #ENDIF
							TRIGGER_MUSIC_EVENT("SOL2_STOP")
						ENDIF

						vPositionToLeave = GET_ENTITY_COORDS(PLAYER_PED_ID())
	//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_OK(mCarRoco.id)
								IF DOES_ENTITY_EXIST(mRocco.id)
									IF IS_PED_IN_VEHICLE(mRocco.id, mCarRoco.id)
										START_VEHICLE_HORN(mCarRoco.id, 6000, HASH("HELDDOWN"))
									ENDIF
								ENDIF
							ENDIF
	//					ENDIF
	
						SAFE_REMOVE_BLIP(mRocco.blip)
					ENDIF
				ENDIF

				IF NOT bTriggeredReplayRecord
					IF bKilledRoccoConv
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						bTriggeredReplayRecord = TRUE								
					ENDIF
				ENDIF
				
				IF NOT IS_PED_UNINJURED(mGunman.id)
				AND bKilledGunManConv
				AND bKilledRoccoConv
					SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
					eState = SS_CLEANUP
				ENDIF
			ELSE
				MONITOR_CHASE_SPEECH()
				UPDATE_CHASE_BLIP(mRocco.blip, mRocco.id, 550, 0.65)	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_OBJ("SOL2_KILL")
				ENDIF
				
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct,  mRocco.id )
			ENDIF
		BREAK

		CASE SS_CLEANUP
			
			// B*1908366 - if we are heading to the cutscene, ensure we don't jump to the next stage instead
			IF iCutsceneDelay != -1
				IF (GET_GAME_TIMER() - iCutsceneDelay) > 1000
					eState = SS_INIT
					eMissionState = MS_OUTRO
					#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP CHASE - heading to MS_OUTRO")  #ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP CHASE")  #ENDIF
				
				IF PREPARE_MUSIC_EVENT("SOL2_STOP")
					#IF IS_DEBUG_BUILD  SK_PRINT("TRIGGER_MUSIC_EVENT(SOL2_STOP)")  #ENDIF
					TRIGGER_MUSIC_EVENT("SOL2_STOP")
				ENDIF
				
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, -1, "DEFAULT_ACTION")
				
				IF IS_POINT_IN_POLY_2D(mAreaCheck1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						iCutsceneDelay = GET_GAME_TIMER()
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REENABLE_CONTROL_ON_DEATH)
						SAFE_FADE_SCREEN_OUT_TO_BLACK()
					ENDIF
				ELSE
					NEXT_STAGE()
				ENDIF
			ENDIF
		BREAK

		CASE SS_SKIPPED
//			SET_PED_POS(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mStealCar.id, <<-5,0,0>>), 0)
			vPositionToLeave = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF IS_PED_UNINJURED(mRocco.id)
				EXPLODE_PED_HEAD(mRocco.id)
			ENDIF
			IF IS_PED_UNINJURED(mGunMan.id)
				EXPLODE_PED_HEAD(mGunMan.id)
			ENDIF
			SAFE_RELEASE_PED(mRocco.id)
			SAFE_REMOVE_BLIP(mRocco.blip)
			SAFE_REMOVE_BLIP(mGunMan.blip)
			SAFE_RELEASE_PED(mGunMan.id)
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_AREA()
	
	SETUP_DYNAMIC_MIXING()
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_AREA")  #ENDIF
				iMissionTimer = GET_GAME_TIMER()
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND (GET_GAME_TIMER() - iMissionTimer) > 3000
				PRINT_OBJ("SOL2_LEAVEA")
			ENDIF

			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPositionToLeave, 60)
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP LEAVE_AREA")  #ENDIF
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

PROC PHONE_CALL_DECISION()
	SETUP_DYNAMIC_MIXING()
	SWITCH eState
		CASE SS_INIT	
			SET_CHECKPOINT()
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT STEAL_VAN")  #ENDIF
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 0, PLAYER_PED_ID(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, NULL, "SOLOMON")
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF NOT bSolCallDone
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF iDelayCall != -1
						IF (GET_GAME_TIMER() - iDelayCall) > 2000
							IF PLAYER_CALL_CHAR_CELLPHONE_WITH_REPLIES(s_conversation_peds, CHAR_SOLOMON, sTextBlock, sInitialRoots, CONV_PRIORITY_MEDIUM, "SOL2_PHONE", sYesRoots, sNoRoots)
								bSolCallDone = TRUE
							ENDIF
						ENDIF
					ELSE
						iDelayCall = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TEXT_LABEL_23 sRoot
					sRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					IF ARE_STRINGS_EQUAL(sRoot, sYesRoots)
						eState = SS_CLEANUP
//					ELIF ARE_STRINGS_EQUAL(sRoot, sNoRoots)
//						REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(CT_FLOW, BIT_MICHAEL, CHAR_SOLOMON, "SOL2_PASS", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, 60000)
//						Script_Passed()
					ENDIF
				ELSE
					REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(TEXT_SOL2_END, CT_FLOW, BIT_MICHAEL, CHAR_SOLOMON, 60000, 10000)
					Script_Passed()
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP STEAL_VAN")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

PROC MONITER_PLAYER_WANTED()
	SWITCH eCopMonitor
		CASE CM_MONITER
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				eCopMonitor = CM_INIT_WANTED
			ENDIF
		BREAK
		
		CASE CM_INIT_WANTED
			SAFE_REMOVE_BLIP(biBlip)
			// B*1911335 - needs custom route to navigate player inside the studio
			IF bIsCustomGPSRouteActive
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				CLEAR_GPS_MULTI_ROUTE()
				bIsCustomGPSRouteActive = FALSE
				#IF IS_DEBUG_BUILD  SK_PRINT(" bIsCustomGPSRouteActive cleaned up")  #ENDIF
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CLEAR_PRINTS()
				PRINT_NOW("LOSE_WANTED", DEFAULT_GOD_TEXT_TIME, 0)
				eCopMonitor = CM_LOSING_WANTED
			ENDIF
		BREAK
		
		CASE CM_LOSING_WANTED
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
				CLEAR_PRINTS()
				eCopMonitor = CM_MONITER
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CAMERA_CUT_STATES()
	
	SWITCH eCamCut
		CASE CC_INIT
			TOGGLE_PLAYER_SAFE_FOR_CAMERA_CUT(TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<<-1011.15521, -479.77164, 38.97575>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_FINAL_HEADING, 0.2)
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			CPRINTLN(DEBUG_MISSION,"eCamCut -> CC_START")
			eCamCut = CC_START
		BREAK
		
		// B*2050595 - need to cut to scripted cam 1 frame after first call to DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		CASE CC_START			
			camMain = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-1017.0,-483.5,40.7>>,<<49.6,-0,-78.9>>,50)
			SHAKE_CAM(camMain,"hand_shake",0.5)
			SET_CAM_ACTIVE(camMain,TRUE)
			RENDER_SCRIPT_CAMS(TRUE, TRUE, 3500, TRUE, TRUE)	
			iMissionTimer = GET_GAME_TIMER()
			CPRINTLN(DEBUG_MISSION,"eCamCut -> CC_WAIT")
			eCamCut = CC_WAIT
		BREAK
		
		CASE CC_WAIT
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
			IF (GET_GAME_TIMER() - iMissionTimer) > 4000
				CPRINTLN(DEBUG_MISSION,"eCamCut -> CC_END")
				eCamCut = CC_END
			ENDIF
		BREAK
		
		CASE CC_END
			CPRINTLN(DEBUG_MISSION,"eCamCut = CC_END  RETURN TRUE")
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC




PROC RETURN_TO_STUDIO()
	SETUP_DYNAMIC_MIXING()
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT RETURN_TO_STUDIO")  #ENDIF
				DISABLE_TAXI_HAILING(FALSE)
				SET_INTERIOR_DISABLED( INTERIOR_V_58_SOL_OFFICE, FALSE)
				SERVICES_TOGGLE(TRUE)
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITER_PLAYER_WANTED()
			MONITOR_LOAD_OUTRO()
			IF eCopMonitor = CM_MONITER
			OR eCamCut != CC_INIT	// if the camera cut has trigger we need to ensure it will complete
//				IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffLoc, <<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>, FALSE)
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, LOCATE_SIZE_ON_FOOT_ONLY)
				OR eCamCut != CC_INIT	// if the camera cut has trigger we need to ensure it will complete

					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF STOP_MY_VEHICLE(5,6)
							IF CAMERA_CUT_STATES()
								eState = SS_CLEANUP
							ENDIF
						ENDIF
					ELSE
						IF CAMERA_CUT_STATES()
							eState = SS_CLEANUP
						ENDIF
					ENDIF
				ELSE
					IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffLoc, <<LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_ON_FOOT_ONLY,LOCATE_SIZE_HEIGHT>>, TRUE)
					ADD_BLIP_LOCATION(biBlip, vDropOffLoc, FALSE)	// gps route handled below
					// B*1911335 - needs custom route to navigate player inside the studio
					IF bIsCustomGPSRouteActive = FALSE
						START_GPS_MULTI_ROUTE(HUD_COLOUR_YELLOW)
						ADD_POINT_TO_GPS_MULTI_ROUTE(vDropOffGPSLoc)
						SET_GPS_MULTI_ROUTE_RENDER(TRUE)
						#IF IS_DEBUG_BUILD  SK_PRINT("bIsCustomGPSRouteActive set")  #ENDIF
						bIsCustomGPSRouteActive = TRUE
					ENDIF
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_OBJ("SOL2_GOTOSTU")
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP RETURN_TO_STUDIO")  #ENDIF

			SAFE_REMOVE_BLIP(biBlip)
			// B*1911335 - needs custom route to navigate player inside the studio
			IF bIsCustomGPSRouteActive
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				CLEAR_GPS_MULTI_ROUTE()
				bIsCustomGPSRouteActive = FALSE
				#IF IS_DEBUG_BUILD  SK_PRINT(" bIsCustomGPSRouteActive cleaned up")  #ENDIF
			ENDIF

			NEXT_STAGE()
		BREAK

		CASE SS_SKIPPED
			REQUEST_CUTSCENE("sol_2_ext_concat")
			WHILE NOT HAS_CUTSCENE_LOADED()
				WAIT(0)
			ENDWHILE

			SET_PED_POS(PLAYER_PED_ID(), vDropOffLoc, 0)
			eState = SS_ACTIVE
			RC_END_Z_SKIP()
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Checks whether the model is under an allowed size
/// PARAMS:
///    vAllowableSize - how large the vehicle is allowed to be
///    bUseDefaultSize - just use the default size check? (ok for most average vehicles)
/// RETURNS:
///    TRUE if the vehicle is of allowable size. FALSE otherwise
FUNC BOOL IS_PLAYER_VEHICLE_MODEL_UNDER_SIZE_LIMIT()
	
	IF NOT IS_VEHICLE_OK(mPlayerCar.id)
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eMod = GET_ENTITY_MODEL(mPlayerCar.id)

	IF eMod = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE // no vehicle available
	ENDIF
	
	VECTOR vVehicleSizeMin, vVehicleSizeMax
	VECTOR	vAllowableSize = GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()
	
	GET_MODEL_DIMENSIONS(eMod, vVehicleSizeMin, vVehicleSizeMax)
			
	IF vVehicleSizeMax.x - vVehicleSizeMin.x > vAllowableSize.x
		RETURN FALSE // vehicle too large
	ELIF vVehicleSizeMax.y - vVehicleSizeMin.y > vAllowableSize.y
		RETURN FALSE // vehicle too large
	ELIF vVehicleSizeMax.z - vVehicleSizeMin.z > vAllowableSize.z
		RETURN FALSE // vehicle too large
	ENDIF
	RETURN TRUE //  vehicle is under the size limit	
ENDFUNC


PROC MOCAP_OUTRO()
	IF eState = SS_ACTIVE
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() 
		AND bLoadingFinCutscene
		AND NOT bJumpSkip
			eState = SS_SKIPPED
		ENDIF
	ENDIF
	
	//Load everything for the next stage while the cutscene is playing 
	IF NOT bLoadingFinCutscene
		IF SETUP_MISSION_STAGE(MS_OUTRO, bJumpSkip)
			bLoadingFinCutscene = TRUE //loading done
		ENDIF
	ENDIF

	SWITCH eState
		CASE SS_INIT
			SAFE_REMOVE_BLIP(biBlip)
			// B*1911335 - needs custom route to navigate player inside the studio
			IF bIsCustomGPSRouteActive
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				CLEAR_GPS_MULTI_ROUTE()
				bIsCustomGPSRouteActive = FALSE
				#IF IS_DEBUG_BUILD  SK_PRINT(" bIsCustomGPSRouteActive cleaned up")  #ENDIF
			ENDIF
			IF CAN_PLAYER_START_CUTSCENE(TRUE)
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				ENDIF

				IF RC_IS_CUTSCENE_OK_TO_START()
					IF PREPARE_MUSIC_EVENT("SOL2_STOP")
						#IF IS_DEBUG_BUILD  SK_PRINT("TRIGGER_MUSIC_EVENT(SOL2_STOP)")  #ENDIF
						TRIGGER_MUSIC_EVENT("SOL2_STOP")
					ENDIF
					START_CUTSCENE()
					WAIT(0)
					
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheBalladOfRocco")
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					SAFE_FADE_SCREEN_IN_FROM_BLACK(500)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					mPlayerCar.id = GET_PLAYERS_LAST_VEHICLE()
					
					ADD_TO_CLOCK_TIME(2,0,0)
					
					IF IS_PLAYER_VEHICLE_MODEL_UNDER_SIZE_LIMIT()
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1009.55701, -483.55057, 45.05663>>, <<-1066.98389, -514.54846, 29.91063>>, 42.12, <<-1024.2708, -485.3149, 35.9804>>, 208.2410 , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					ELSE 
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1009.55701, -483.55057, 45.05663>>, <<-1066.98389, -514.54846, 29.91063>>, 42.12, <<-1030.7389, -496.2591, 35.6675>>, 295.7484)
					ENDIF
					RC_START_CUTSCENE_MODE(vDropOffLoc)
					IF IS_SCREEN_FADED_OUT()
					    IF NOT IS_SCREEN_FADING_IN()
					    	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD  SK_PRINT("INIT  MOCAP_OUTRO")  #ENDIF
					eState = SS_ACTIVE
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				TOGGLE_PLAYER_SAFE_FOR_CAMERA_CUT(FALSE)

//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF

			IF NOT IS_CUTSCENE_PLAYING()
			AND bLoadingFinCutscene
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
		
			REPLAY_STOP_EVENT()
		
			bLoadingFinCutscene = FALSE
			RC_END_CUTSCENE_MODE()
			Script_Passed()
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP  MOCAP_OUTRO")  #ENDIF
		BREAK

		CASE SS_SKIPPED
			IF WAS_CUTSCENE_SKIPPED()
				SET_PED_POS(PLAYER_PED_ID(), <<-1017.7021, -484.0121, 36.0903>>, 114.1774)
			ENDIF
			eState = SS_ACTIVE
//			RC_END_Z_SKIP()
		BREAK
	ENDSWITCH
	
ENDPROC

///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					DELETE_ALL()
					WAIT_FOR_CUTSCENE_TO_STOP()
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_POS(PLAYER_PED_ID(), <<-1017.7021, -484.0121, 36.0903>>, 114.1774)
					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED(FR_NONE)
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
//					IF i_debug_jump_stage >= 3
//						SWITCH i_debug_jump_stage
//							CASE 3 //MS_RETURN_TO_STUDIO
//								i_debug_jump_stage = 4
//							BREAK
//							
//							CASE 4 //MS_OUTRO
//								i_debug_jump_stage = 5
//							BREAK
//
//						ENDSWITCH
//					ELSE
						i_debug_jump_stage++
//					ENDIF
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF

PROC CHECK_ROCO_DISTANCE()
	IF IS_PED_UNINJURED(mRocco.id)
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRocco.id, 550)
			MISSION_FAILED(FR_ROCO_ESCAPED)
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_SOLOMON_ALIVE()
	IF NOT bCleanedupSolomon
		IF NOT IS_PED_UNINJURED(piSolomon)
			MISSION_FAILED(FR_SOLOMON_KILLED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Holds functions that moniter for the player failing
PROC CHECK_FOR_FAIL()
	IF NOT bJumpSkip
	AND eMissionState <> MS_FAILED
		IF eMissionState > MS_INTRO
			CHECK_ROCO_DISTANCE()
			CHECK_SOLOMON_ALIVE()
		ENDIF
	ENDIF
ENDPROC

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINTSTRING("FORCE CLEAN UP") PRINTNL()
		sFailReason = NULL
//		SET_FAIL_WARP()
		TRIGGER_MUSIC_EVENT("SOL2_FAIL")
		Mission_Flow_Mission_Force_Cleanup()
        Script_Cleanup()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF
	POPULATE_STUFF()
	
	IF IS_REPLAY_IN_PROGRESS()
		VECTOR pos
		FLOAT dir
		iReplayStage = GET_REPLAY_MID_MISSION_STAGE()	
		
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF
		SWITCH iReplayStage
			CASE 0
				pos = <<-1038.9562, -547.5014, 34.0505>>
				dir = 125.2668
			BREAK
			
			CASE CP_KILLEDROCO
				pos = <<-1198.9615, -597.6102, 25.9498>>
				dir = 138.1940
			BREAK

			CASE 3
				pos = <<-1017.7021, -484.0121, 36.0903>> 
				dir = 114.1774
			BREAK
			
		ENDSWITCH
		START_REPLAY_SETUP(pos, dir)
	ENDIF
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TBOR")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			CHECK_FOR_FAIL()
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SWITCH eMissionState
			
				CASE MS_SET_UP
					INITMISSION()
				BREAK

				CASE MS_INTRO
					MOCAP_INTRO()
				BREAK
				
				CASE MS_CHASE
					CHASE()
				BREAK
				
				CASE MS_LEAVE_AREA
					LEAVE_AREA()
				BREAK
				
				CASE MS_PHONE_CALL
					PHONE_CALL_DECISION()
				BREAK
				
				CASE MS_RETURN_TO_STUDIO
					RETURN_TO_STUDIO()
				BREAK
				
				CASE MS_OUTRO
					MOCAP_OUTRO()
				BREAK
				
				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK
			ENDSWITCH
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT bVehStatsGrabbed
					VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTemp, SOL2_CAR_DAMAGE)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehTemp, SOL2_MAX_SPEED)
					bVehStatsGrabbed = TRUE
				ENDIF
			ELSE
				IF bVehStatsGrabbed
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, SOL2_CAR_DAMAGE)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, SOL2_MAX_SPEED)
					bVehStatsGrabbed = FALSE
				ENDIF
			ENDIF
			
			IF NOT bPlayerStatsGrabbed
				IF IS_PLAYER_PLAYING(PLAYER_ID())
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), SOL2_DAMAGE)
					bPlayerStatsGrabbed = TRUE
				ENDIF
			ENDIF

			IF eMissionState <> MS_FAILED
				IF eMissionState >= MS_SET_UP
				AND NOT bJumpSkip
				ENDIF

				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
