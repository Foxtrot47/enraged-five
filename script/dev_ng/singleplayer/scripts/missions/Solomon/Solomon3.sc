
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "commands_fire.sch"
USING "commands_clock.sch"
USING "dialogue_public.sch"
USING "replay_public.sch"
USING "script_ped.sch"
USING "cam_recording_public.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "mission_stat_public.sch"
USING "script_player.sch"
USING "CompletionPercentage_public.sch"
USING "flow_public_GAME.sch"
USING "clearMissionArea.sch"
USING "vehicle_gen_public.sch"
USING "area_checks.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "Overlay_Effects.sch"
USING "taxi_functions.sch"
USING "script_misc.sch"

USING "commands_recording.sch"

//USING "commands_hud.sch"

/////═══════════════════════════════╡ CONSTANTS FOR UBER PLAYBACK  ╞══════════════════════════════════
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS                      175
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS                       15    
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS                    35    

CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK           12  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK         14                             

CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK            6

USING "traffic.sch" 


/////══════════════════════════════════════╡ DEBUG VARIABLES  ╞════════════════════════════════════════
#IF IS_DEBUG_BUILD

USING "select_mission_stage.sch"

CONST_INT MAX_SKIP_MENU_LENGTH 4                      	// number of stages in mission + 2 (for menu )

INT iReturnStage 										// mission stage to jump to

BOOL vehicle_recording_started
BOOL vehicle_recording_stopped

//BOOL jSkipReady										= FALSE 
//BOOL PSkipReady										= FALSE

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

//WIDGET_ID widgetDebug

//CAM_RECORDING_DATA CamRagData


#ENDIF
/////═════════════════════════════════════╡ END OF DEBUG   ╞═══════════════════════════════════════════


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Airport_Chase.sc
//		AUTHOR			:	Ben Barclay
//		DESCRIPTION		:	A high speed car chase involving lots of police cars infront 
//							and behind the player all chasing after the bad guy. Will 
//							involve lots of set peices and explosions.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// ___________________________________ ENUMS ______________________________________________

ENUM MISSION_FLOW
	STAGE_INIT_MISSION,
	STAGE_OPENING_CUTSCENE,
	STAGE_GET_TO_CHASE,
	STAGE_CHASE_START,
	STAGE_MAIN_CHASE,
	STAGE_ON_FOOT_CHASE,
	STAGE_PED_RUNS_TOWARDS_ENGINE,
	STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE,
	STAGE_EVADE_POLICE,
	STAGE_DEBUG_PLAYBACK_UBER,
	STAGE_DEBUG_RECORD_UBER,
	STAGE_DEBUG_TEST
ENDENUM
MISSION_FLOW missionStage = STAGE_INIT_MISSION

// ===========================================================================================================
//		Variables
// ===========================================================================================================

//DEBUG Variables
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID Airport_Chase_Widget_Group
	
	BOOL jSkipReady = FALSE
#ENDIF

//Integers
INT iControlFlag
INT ChaseStage
INT icutsceneStage
INT screenRT
INT iSubStage
INT iCurrentText
INT RandomInt
//INT iskip_cutscene_flag
INT MissionFailTimer
INT icount
INT iSetPieceCount
INT iEnemiesKilled
INT iBloodSplatTimer
INT iplayerwantedLevelTimer
INT moveStageOnTimer
INT failOnFootTimer
INT vehicleChaseTimer
//INT vehicleChaseTimer2
INT iFailWrongWayTimer
INT ivehicleUpsideDownTimer
INT iPolice5ChatTimer
INT iMollyRunChatTimer
INT iScrollTimer
INT iFreezeFilmReelTimer
INT iTimeScaleTimer
INT iMichaelChatTimer
INT iMikeCopChatTimer
INT iScriptFlashTimer
INT iDisableReplayCameraTimer	//Fix for bug 2269554

//Sync'd Scenes
INT MollyRunsFromCarSyncdScene
INT SuckedThroughJetSyncdScene
INT WorkerSyncScene

//INT SOUND ID's
INT JetEngineSoundID
INT SuckedInSoundID
INT CarExplodeSoundID1
INT CarExplodeSoundID2
INT CarExplodeSoundID3
INT CarExplodeSoundID4
INT loudFireSoundID
INT PropPlaneSoundID
INT NewsHeliSoundID

//Doors
INT iDoor

//Blocking navmesh areas
INT NavBlockingArea1

//Peds
PED_INDEX EnemyPed
PED_INDEX Fireman[4]
PED_INDEX AirWorker
PED_INDEX RoadBlockCop[3]
PED_INDEX EndCop[11]
PED_INDEX truckDriver
PED_INDEX DivingWorker1
PED_INDEX DivingWorker2
PED_INDEX Cop14
PED_INDEX Cop16
PED_INDEX Cop17
PED_INDEX AIPoliceCop[2] 
PED_INDEX setPiecePlanePilot[2]
PED_INDEX NearestCop

//Vehicles
VEHICLE_INDEX EnemyCar
VEHICLE_INDEX PlayerCar
VEHICLE_INDEX SetPiecePlane[2]
//VEHICLE_INDEX RoadBlockCopCar[3]
VEHICLE_INDEX SetpieceCopCar//[3]
VEHICLE_INDEX FireEngine
VEHICLE_INDEX Trailer
VEHICLE_INDEX EndPlane
VEHICLE_INDEX GetAwayPlane
VEHICLE_INDEX endpoliceCar[2]
VEHICLE_INDEX VehCam
VEHICLE_INDEX AIPoliceCar[2] 
VEHICLE_INDEX PlayerSpeedStatVehicle
VEHICLE_INDEX PlayerCar2
VEHICLE_INDEX PlayerCar3
VEHICLE_INDEX PlayerStartCar
VEHICLE_INDEX GetAwayCar[2]

//Objects
OBJECT_INDEX objBloodSplat
OBJECT_INDEX ObjFilmReel
OBJECT_INDEX WorkersClipBoard

//Blips
BLIP_INDEX EnemyPedBlip
BLIP_INDEX playerCarBlip
BLIP_INDEX airportBlip
BLIP_INDEX filmReelBlip
//BLIP_INDEX FBICopBlip[3]

//Cameras
CAMERA_INDEX heliChaseCam
CAMERA_INDEX CamDest
CAMERA_INDEX CamInit

//CAMERA_INDEX CamStatic

//Sequences
SEQUENCE_INDEX seq

//Interiors
//INTERIOR_INSTANCE_INDEX RSinteriorGroup

//For news heli print
SCALEFORM_INDEX ScaleformMovie

//Vectors
VECTOR PlayerCarStartingCoords = <<-1024.1000, -485.3321, 35.9816>>
VECTOR EnemyCarChaseStartingCoords = << -428.0263, -2153.5769, 9.2997 >>
VECTOR vAirportBlip = <<-498.7, -2136.5, 8.4>>
//VECTOR heli_chase_cam_offset = <<-0.056, 3.194, -1.282>>
VECTOR PlayerCoords 
VECTOR AirWorkerCoords
VECTOR EnemyPedCoords
//VECTOR playerscoords
VECTOR PlayerCarChaseStartingCoords = <<-272.8615, -2186.9316, 9.3174>>//<< -338.3881, -2146.9309, 9.2513 >>	
VECTOR vcar12Coords
VECTOR vPlayerCoords

//Floats
FLOAT fPlayerCarStartingHeading = 209.7233
FLOAT fEnemyCarChaseStartingHeading = 90.9470
FLOAT fPlaybackSpeed
FLOAT fCatchUPMultiplyer
FLOAT fPlayerCarChaseStartingHeading = 46.7161//94.9371
FLOAT fPlayerMaxMoveSpeed

//Bools
BOOL setpiecePlane1Setup
BOOL setpiecePlane2Setup
//BOOL enginefx1done
//BOOL enginefx2done
//BOOL enginefx3done
//BOOL enginefx4done
BOOL enginefx5done
BOOL Car8HasBeenBlasted
BOOL Car11HasBeenBlasted
BOOL Car12HasBeenBlasted
//BOOL Car10Blasted
BOOL FiremenSetup
BOOL trailerdetached
BOOL has_cam_been_created
BOOL heli_chase_cam_active
BOOL b_heliCamHelp
BOOL bIsCutSceneRunning
//BOOL InterpCamActive
BOOL EndCopsSetUp
BOOL AirWorkerTaskGiven
BOOL AirWorkerBlasted
BOOL playerBlasted
//BOOL AirWorkerAnimStarted
//BOOL enemypedWaypointPlaybackResumed
BOOL doneChaseAfterBadGuyText
BOOL AirWorkerTask2Given
BOOL MissionStageBeingSkippedTo
BOOL endcops6and7tasksgiven
BOOL endcops8and9tasksgiven
BOOL endcop10TaskGiven
BOOL camrecordingstarted			
BOOL doneGetInCarText
//BOOL doneChat5
//BOOL doneChat6
BOOL GameCanFail = FALSE
BOOL distanceCheckRequired = FALSE
//BOOL heliChaseCamActivated
//BOOL camsRendered
BOOL PlayersCarAddedToVehicleUpSideDownCheck
BOOL setUpScrollTextsForScaleForm
BOOL NewsPartOneTextCalled
BOOL NewsPartTwoTextCalled
BOOL NewsPartThreeTextCalled
BOOL NewsPartFourTextCalled
BOOL playerComingFromWrongSide
BOOL checkForEnemyPedDeath
BOOL entityProofsNeedsSet 
BOOL nextStageStuffRequested
BOOL vehicleColoursSet
//BOOL hintcamHelpTextDone
BOOL donechat7
//BOOL doneChat8
//BOOL doneShout2Chat
//BOOL doneChat10
//BOOL doneChat11
//BOOL doneChat12
BOOL AudioSceneStarted[3]
BOOL AudioSceneStopped[2]
BOOL CarExplodeSoundID2Released
BOOL timeScale1BackToNormal
//BOOL timeScale2BackToNormal
BOOL copsPlaybackStarted
BOOL copsInCar0ReadyForAction
BOOL copsInCar1ReadyForAction
BOOL playbackspeedChangedForTruck
BOOL donePoliceReport
BOOL doneWarningText
BOOL playbackStoppedForTruck
BOOL propPlaneSoundReleased
BOOL CarExplodeSoundID3Released
BOOL CarExplodeSoundID1Released
BOOL CarExplodeSoundID4Released
BOOL CarExplodeSoundID1Played
BOOL CarExplodeSoundID3Played
BOOL CarExplodeSoundID4Played
BOOL loudFireSoundIDPlayed
BOOL PropPlaneSoundIDPlayed
BOOL CarExplodeSoundID2Played
BOOL PropPlaneSoundIDCreated
BOOL CarExplodeSoundID1Created
BOOL CarExplodeSoundID2Created
BOOL CarExplodeSoundID3Created
BOOL CarExplodeSoundID4Created
BOOL loudFireSoundIDCreated
BOOL NewsHeliSoundIDCreated
BOOL pedsCreated
BOOL DivingPedsAnimsDone
BOOL bPlayerHasTakenControl
BOOL nextStageAssetsRequested
BOOL helisBladesSetup
BOOL EnemyCarIsOffScreen
BOOL SetPieceCarIDisOffScreen[25]
BOOL donePoliceChat1
BOOL donePoliceChat2
BOOL Cop14TaskGiven					
BOOL CopIdsGrabbed 
//BOOL playerFailTimerStarted
BOOL AIPoliceSetup
BOOL haveCarAndTrailerBeenExploded
BOOL playerNeedsWarned
BOOL bMissionFailed
BOOL EverythingSetUpForMissionStart
BOOL MusicTriggered
BOOL MusicPrepared
BOOL MusicTriggered2
BOOL GpsRouteTurnedOn
BOOL PlayerAddedForHealthStat
BOOL PlayerCarAddedForSpeedStat
BOOL KilledStatDone[19]
BOOL LoseCopsStatTimerStarted
BOOL EnemyPedTaskGiven
BOOL vehiclesMovedAtStartLocation
BOOL enemyPedWaypointTask
BOOL getawayCar0Setup
BOOL getawayCar1Setup
//BOOL doneHangerChat
BOOL EnginesStarted
BOOL bloodSplatterEffectStarted
BOOL filmReelDropped
BOOL jetMusicTriggered
BOOL ShortCutActive 
BOOL ShortCutDone 	
BOOL timerStarted
BOOL planeSetInvincible
BOOL SuckedThroughEngineAudioRequested
BOOL AIChaseTaskGiven[2]
BOOL AIChaseBehavioursSet[2]
BOOL car13Blasted
BOOL trailerCreated
BOOL musicStarted
BOOL DonePoliceChat
BOOL PoliceCarAddedToAudio[7]
BOOL PoliceCarRemovedFromAudio[7]
BOOL vehiclesMovedForStart
BOOL failWrongWayTimerSet
BOOL playerHasGotCloseEnough
BOOL vehicleUpsideDownTimerSet
BOOL wantedLevelAirport
BOOL playerSetToWalk
BOOL SuckedThroughAnimStarted
BOOL hintStarted
BOOL hintStopped
//BOOL doneChat1
BOOL godtextUpdated
BOOL splutterFXStopped[3]
BOOL splutterFXStarted[2] 
BOOL doneDevinPhoneCall
BOOL donePoliceMegaPhone[4]
BOOL doneMollyChat
BOOL done1stMollyRunChat
BOOL doneSol3Past
BOOL doneSol3Sucks
BOOL doneSol3Watch
//BOOL airworkerFleeing
BOOL startedMainAnim
BOOL playedWorkerShout
BOOL startedEndLoop
BOOL filmReelFrozen
BOOL filmReelFrozenTimerStarted
BOOL fleeTaskGiven
BOOL slowMoStopped
BOOL slowMoStarted
BOOL UnfrozeEndPlane
BOOL firemanCowerTaskSet[3]
BOOL FirstAnimStarted[3]
BOOL hornSoundStarted
BOOL doorSoundPlayed
BOOL doneChase4Chat
BOOL doneBoomChat
BOOL doneBlowChat
BOOL doneReactChat
BOOL doneChase2Chat
BOOL doneChase3Chat
BOOl doneChase5Chat
BOOL doneChase6Chat
BOOL doneChatPolice5
BOOL doneChase7Chat
BOOl MichaelsTurnToSpeak
BOOL mollysTurnToSpeak
BOOL doneSmushChat
BOOL doneSmush2Chat
BOOL doneCopsChat
BOOL effectsStopped
BOOL doneAnnoChat
BOOL hasPedBeenShot
BOOL bVideoRecorded[4]

//Groups
REL_GROUP_HASH SecGuardGroup

//Weapons

//Strings
STRING FailReason

//Structs
structPedsForConversation MyLocalPedStruct
structTimelapse sToD

//Particles
PTFX_ID C4[3]
PTFX_ID FarLeftEngine     
PTFX_ID NearLeftEngine      
PTFX_ID FarRightEngine       
PTFX_ID	NearRightEngine
PTFX_ID EngineDamagePTFX
PTFX_ID EngineSplutterEffectsID

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(EnemyPedBlip)
		REMOVE_BLIP(EnemyPedBlip)
	ENDIF	
	
	IF DOES_BLIP_EXIST(playerCarBlip)
		REMOVE_BLIP(playerCarBlip)
	ENDIF	
	
	IF DOES_BLIP_EXIST(airportBlip)
		REMOVE_BLIP(airportBlip)
	ENDIF	
	
	IF DOES_BLIP_EXIST(filmReelBlip)
		REMOVE_BLIP(filmReelBlip)	
	ENDIF
	
ENDPROC

//PURPOSE: Deletes all mission entities
PROC DELETE_ALL_ENTITIES()

	PRINTSTRING("DELETE_ALL_ENTITIES BEING CALLED") PRINTNL()

	//Stop and dialogue and clear all text	
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	SET_IGNORE_NO_GPS_FLAG(FALSE)
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoor)
		REMOVE_DOOR_FROM_SYSTEM(iDoor)
	ENDIF	
	
	//Navmesh blocking objects
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(NavBlockingArea1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(NavBlockingArea1)
	ENDIF	
	
	//Move player out an vehicle before calling this.
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1019.5793, -484.8720, 36.0795>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 93.7701)
	ENDIF
	
	IF DOES_ENTITY_EXIST(PlayerCar)
		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PlayerCar)
				STOP_PLAYBACK_RECORDED_VEHICLE(PlayerCar)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(EnemyCar)
		IF IS_VEHICLE_DRIVEABLE(EnemyCar)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(EnemyCar)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
				STOP_PLAYBACK_RECORDED_VEHICLE(EnemyCar)
			ENDIF
		ENDIF
	ENDIF
	
	//Stop all Particle Effects
	IF DOES_PARTICLE_FX_LOOPED_EXIST(FarLeftEngine)
		STOP_PARTICLE_FX_LOOPED(FarLeftEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(NearLeftEngine)
		STOP_PARTICLE_FX_LOOPED(NearLeftEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(FarRightEngine)
		STOP_PARTICLE_FX_LOOPED(FarRightEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(NearRightEngine)
		STOP_PARTICLE_FX_LOOPED(NearRightEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(EngineDamagePTFX)
		STOP_PARTICLE_FX_LOOPED(EngineDamagePTFX)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[0])
		STOP_PARTICLE_FX_LOOPED(C4[0])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[1])
		STOP_PARTICLE_FX_LOOPED(C4[1])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[2])
		STOP_PARTICLE_FX_LOOPED(C4[2])
	ENDIF	
	
	REMOVE_PTFX_ASSET()
	
	//Delete Peds first
	IF DOES_ENTITY_EXIST(EnemyPed)
		DELETE_PED(EnemyPed)
	ENDIF
	IF DOES_ENTITY_EXIST(AirWorker)
		DELETE_PED(AirWorker)
	ENDIF
	IF DOES_ENTITY_EXIST(truckDriver)
		DELETE_PED(truckDriver)
	ENDIF
	IF DOES_ENTITY_EXIST(DivingWorker1)
		DELETE_PED(DivingWorker1)
	ENDIF
	IF DOES_ENTITY_EXIST(DivingWorker2)
		DELETE_PED(DivingWorker2)
	ENDIF
	IF DOES_ENTITY_EXIST(Cop14)
		DELETE_PED(Cop14)
	ENDIF
	IF DOES_ENTITY_EXIST(Cop16)
		DELETE_PED(Cop16)
	ENDIF
	IF DOES_ENTITY_EXIST(Cop17)
		DELETE_PED(Cop17)
	ENDIF
	IF DOES_ENTITY_EXIST(Fireman[0])
		DELETE_PED(Fireman[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(Fireman[1])
		DELETE_PED(Fireman[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(Fireman[2])
		DELETE_PED(Fireman[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(Fireman[3])
		DELETE_PED(Fireman[3])
	ENDIF
	IF DOES_ENTITY_EXIST(RoadBlockCop[0])
		DELETE_PED(RoadBlockCop[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(RoadBlockCop[1])
		DELETE_PED(RoadBlockCop[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(RoadBlockCop[2])
		DELETE_PED(RoadBlockCop[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[0])
		DELETE_PED(EndCop[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[1])
		DELETE_PED(EndCop[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[2])
		DELETE_PED(EndCop[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[3])
		DELETE_PED(EndCop[3])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[4])
		DELETE_PED(EndCop[4])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[5])
		DELETE_PED(EndCop[5])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[6])
		DELETE_PED(EndCop[6])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[7])
		DELETE_PED(EndCop[7])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[8])
		DELETE_PED(EndCop[8])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[9])
		DELETE_PED(EndCop[9])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[10])
		DELETE_PED(EndCop[10])
	ENDIF
	IF DOES_ENTITY_EXIST(AIPoliceCop[0])
		DELETE_PED(AIPoliceCop[0])
	ENDIF	
//	IF DOES_ENTITY_EXIST(AIPoliceCop[1])
//		DELETE_PED(AIPoliceCop[1])
//	ENDIF	
	IF DOES_ENTITY_EXIST(setPiecePlanePilot[0])
		DELETE_PED(setPiecePlanePilot[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(setPiecePlanePilot[1])
		DELETE_PED(setPiecePlanePilot[1])
	ENDIF	

	//Delete all vehicles
	IF DOES_ENTITY_EXIST(EnemyCar)
		DELETE_VEHICLE(EnemyCar)
	ENDIF	
	IF DOES_ENTITY_EXIST(PlayerCar)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(PlayerCar)
			SET_ENTITY_AS_MISSION_ENTITY(PlayerCar)
		ENDIF
		DELETE_VEHICLE(PlayerCar)
	ENDIF	
	IF DOES_ENTITY_EXIST(FireEngine)
		DELETE_VEHICLE(FireEngine)
	ENDIF	
	IF DOES_ENTITY_EXIST(Trailer)
		DELETE_VEHICLE(Trailer)
	ENDIF	
	IF DOES_ENTITY_EXIST(EndPlane)
		DELETE_VEHICLE(EndPlane)
	ENDIF	
	IF DOES_ENTITY_EXIST(GetAwayPlane)
		DELETE_VEHICLE(GetAwayPlane)
	ENDIF	
	IF DOES_ENTITY_EXIST(VehCam)
		DELETE_VEHICLE(VehCam)
	ENDIF
	IF DOES_ENTITY_EXIST(PlayerSpeedStatVehicle)
		DELETE_VEHICLE(PlayerSpeedStatVehicle)
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[0])
		DELETE_VEHICLE(GetAwayCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[1])
		DELETE_VEHICLE(GetAwayCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(SetPiecePlane[0])
		DELETE_VEHICLE(SetPiecePlane[0])
	ENDIF
	IF DOES_ENTITY_EXIST(SetPiecePlane[1])
		DELETE_VEHICLE(SetPiecePlane[1])
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[0])
		DELETE_VEHICLE(GetAwayCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[1])
		DELETE_VEHICLE(GetAwayCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(endpoliceCar[0])
		PRINTSTRING("endpoliceCar[0] exists and is being deleted")
		DELETE_VEHICLE(endpoliceCar[0])
	ELSE
		PRINTSTRING("endpoliceCar[0] doesn't exist")
	ENDIF
	IF DOES_ENTITY_EXIST(endpoliceCar[1])
		DELETE_VEHICLE(endpoliceCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(AIPoliceCar[0])
		DELETE_VEHICLE(AIPoliceCar[0])
	ENDIF
//	IF DOES_ENTITY_EXIST(AIPoliceCar[1])
//		DELETE_VEHICLE(AIPoliceCar[1])
//	ENDIF
//	IF DOES_ENTITY_EXIST(RoadBlockCopCar[0])
//		DELETE_VEHICLE(RoadBlockCopCar[0])
//	ENDIF
//	IF DOES_ENTITY_EXIST(RoadBlockCopCar[1])
//		DELETE_VEHICLE(RoadBlockCopCar[1])
//	ENDIF
//	IF DOES_ENTITY_EXIST(RoadBlockCopCar[2])
//		DELETE_VEHICLE(RoadBlockCopCar[2])
//	ENDIF
//	IF DOES_ENTITY_EXIST(SetpieceCopCar[0])
//		DELETE_VEHICLE(SetpieceCopCar[0])
//	ENDIF
	IF DOES_ENTITY_EXIST(SetpieceCopCar)
		DELETE_VEHICLE(SetpieceCopCar)
	ENDIF
//	IF DOES_ENTITY_EXIST(SetpieceCopCar[2])
//		DELETE_VEHICLE(SetpieceCopCar[2])
//	ENDIF

	//Objects
	IF DOES_ENTITY_EXIST(objBloodSplat)
		DELETE_OBJECT(objBloodSplat)
	ENDIF
	IF DOES_ENTITY_EXIST(ObjFilmReel)
		DELETE_OBJECT(ObjFilmReel)
	ENDIF
	IF DOES_ENTITY_EXIST(WorkersClipBoard)
		DELETE_OBJECT(WorkersClipBoard)
	ENDIF

	RELEASE_SCRIPT_AUDIO_BANK()

	//Audio
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_DRIVE_TO_AIRPORT")
		STOP_AUDIO_SCENE("SOL_3_DRIVE_TO_AIRPORT")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_MAIN_CHASE")
		STOP_AUDIO_SCENE("SOL_3_MAIN_CHASE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CHOPPER_CAM")
		STOP_AUDIO_SCENE("SOL_3_CHOPPER_CAM")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_HELI_TAKEOFF")
		STOP_AUDIO_SCENE("SOL_3_HELI_TAKEOFF")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_TANKER_CRASH")
		STOP_AUDIO_SCENE("SOL_3_EVENT_TANKER_CRASH")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
		STOP_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
	ENDIF	
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_PLANE_ON_FIRE")
		STOP_AUDIO_SCENE("SOL_3_PLANE_ON_FIRE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_JET_LANDING")
		STOP_AUDIO_SCENE("SOL_3_EVENT_JET_LANDING")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_PLANE_TAXIING_EXPLOSION")
		STOP_AUDIO_SCENE("SOL_3_EVENT_PLANE_TAXIING_EXPLOSION")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_ARRIVE_AT_HANGAR")
		STOP_AUDIO_SCENE("SOL_3_ARRIVE_AT_HANGAR")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_ON_FOOT_CHASE")
		STOP_AUDIO_SCENE("SOL_3_ON_FOOT_CHASE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS")
		STOP_AUDIO_SCENE("SOL_3_LOSE_COPS")
	ENDIF	
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_PLANE")
		STOP_AUDIO_SCENE("SOL_3_LOSE_COPS_PLANE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_VEHICLE")
		STOP_AUDIO_SCENE("SOL_3_LOSE_COPS_VEHICLE")
	ENDIF	
	IF IS_AUDIO_SCENE_ACTIVE("SOL_3_ENGINE_DEATH_SCENE")
		STOP_AUDIO_SCENE("SOL_3_ENGINE_DEATH_SCENE")
	ENDIF	

	STOP_SOUND(JetEngineSoundID)
	STOP_SOUND(CarExplodeSoundID1)
	STOP_SOUND(CarExplodeSoundID2)
	STOP_SOUND(CarExplodeSoundID3)
	STOP_SOUND(CarExplodeSoundID4)
	STOP_SOUND(loudFireSoundID)
	STOP_SOUND(PropPlaneSoundID)
	STOP_SOUND(NewsHeliSoundID)
	STOP_SOUND(SuckedInSoundID)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF DOES_CAM_EXIST(heliChaseCam)
		DESTROY_CAM(heliChaseCam)
	ENDIF
	IF DOES_CAM_EXIST(CamDest)
		DESTROY_CAM(CamDest)
	ENDIF
	IF DOES_CAM_EXIST(CamInit)
		DESTROY_CAM(CamInit)
	ENDIF
	
	CLEANUP_UBER_PLAYBACK(TRUE, TRUE)
	
	REMOVE_ANIM_DICT("misssolomon_3")
	REMOVE_ANIM_DICT("amb@world_human_clipboard@male@base")
	REMOVE_ANIM_DICT("missheist_agency3aig_lift_waitped_a")
	
	//Car recordings
	REMOVE_VEHICLE_RECORDING(1, "BB_CHASE")
	REMOVE_VEHICLE_RECORDING(2, "BB_Chase")
	REMOVE_VEHICLE_RECORDING(9, "BB_CHASE")
	REMOVE_VEHICLE_RECORDING(35, "BB_Chase")
	REMOVE_VEHICLE_RECORDING(45, "BB_Chase")
	REMOVE_VEHICLE_RECORDING(54, "BB_Chase")
	REMOVE_VEHICLE_RECORDING(55, "BB_Chase")
	REMOVE_VEHICLE_RECORDING(61, "BB_CHASE")
	REMOVE_VEHICLE_RECORDING(63, "BB_CHASE")
	REMOVE_VEHICLE_RECORDING(64, "BB_CHASE")
	REMOVE_VEHICLE_RECORDING(67, "BB_Chase")
	REMOVE_VEHICLE_RECORDING(68, "BB_CHASE")
	REMOVE_VEHICLE_RECORDING(69, "BB_Chase")
	
	//Scaleform for heli cam
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformMovie)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("digitalOverlay")
	
ENDPROC



//PURPOSE: Requests and loads everything required for uber stage
PROC REQUEST_EVERYTHING_FOR_UBER_CHASE()	
	
		//Request models and recordings for uber chase.
		ScaleformMovie = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
		REQUEST_MODEL(JET)
		REQUEST_MODEL(POLICE3)
		REQUEST_MODEL(MAVERICK)
		REQUEST_MODEL(BISON)
		REQUEST_MODEL(BLISTA)
		REQUEST_MODEL(IG_MOLLY)
		REQUEST_MODEL(COGCABRIO)
		REQUEST_MODEL(RAPIDGT)
		REQUEST_VEHICLE_RECORDING(45, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(35, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(2, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(54, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(55, "BB_Chase")	
		REQUEST_VEHICLE_RECORDING(67, "BB_Chase")
		REQUEST_CAM_RECORDING(1, "BB_Chase")
			
		//Request and load checkpoint vehicle if on a replay	
		IF IS_REPLAY_IN_PROGRESS()
			IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
				AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
					REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
					WHILE NOT  HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
						PRINTSTRING("Waiting for replay checkpoint vehicle loading")
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
		ENDIF

		//Models
		WHILE NOT HAS_MODEL_LOADED(JET)
		OR NOT HAS_MODEL_LOADED(POLICE3)
		OR NOT HAS_MODEL_LOADED(MAVERICK)
		OR NOT HAS_MODEL_LOADED(BISON)
		OR NOT HAS_MODEL_LOADED(BLISTA)
		OR NOT HAS_MODEL_LOADED(IG_MOLLY)
		OR NOT HAS_MODEL_LOADED(COGCABRIO)
		OR NOT HAS_MODEL_LOADED(RAPIDGT)
			PRINTSTRING("Waiting on models to load icontrol flag = 0") PRINTNL()
			WAIT(0)
		ENDWHILE
		
		//Other assets
		WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie)
		OR NOT HAS_CAM_RECORDING_LOADED(1, "BB_Chase")
			PRINTSTRING("Waiting on Assets to load icontrol flag = 0") PRINTNL()
			WAIT(0)
		ENDWHILE		
		
		//Car recordings
		WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(45, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(35, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(54, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(55, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(67, "BB_Chase")
			WAIT(0)
			PRINTSTRING("Waiting on Vehicle recording 67 to load icontrol flag = 0") PRINTNL()
		ENDWHILE
	
ENDPROC


////PURPOSE: Creates road block for end of chase
//PROC CREATE_ROAD_BLOCK()
//
//		REQUEST_MODEL(POLICE3)
//		REQUEST_MODEL(S_M_Y_COP_01)
//		WHILE NOT HAS_MODEL_LOADED(POLICE3)
//		OR NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
//			WAIT(0)
//		ENDWHILE
//
//		CLEAR_AREA(<< -929.6240, -2910.3096, 12.9449 >>, 15, TRUE)
//		
//		//Create cop cars
//		IF NOT DOES_ENTITY_EXIST(RoadBlockCopCar[0])
//			RoadBlockCopCar[0] = CREATE_VEHICLE(POLICE3, << -929.6240, -2910.3096, 12.9449 >>, 154.9424)
//		ENDIF
//		IF NOT DOES_ENTITY_EXIST(RoadBlockCopCar[1])
//			RoadBlockCopCar[1] = CREATE_VEHICLE(POLICE3, << -928.4073, -2905.4690, 12.9449 >>, 315.3437)
//		ENDIF
//		IF NOT DOES_ENTITY_EXIST(RoadBlockCopCar[2])
//			RoadBlockCopCar[2] = CREATE_VEHICLE(POLICE3, << -925.1010, -2900.8804, 12.9447 >>, 154.2905)
//		ENDIF
//		
//		//Create cop peds
//		IF NOT DOES_ENTITY_EXIST(RoadBlockCop[0])
//			RoadBlockCop[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, << -928.1075, -2903.4807, 12.9450 >>, 206.6598)
//			GIVE_WEAPON_TO_PED(RoadBlockCop[0], WEAPONTYPE_PISTOL, 1000, TRUE)
//			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(RoadBlockCop[0])
//		ENDIF
//		IF NOT DOES_ENTITY_EXIST(RoadBlockCop[1])
//			RoadBlockCop[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, << -931.7083, -2909.7185, 12.9450 >>, 217.8968)
//			GIVE_WEAPON_TO_PED(RoadBlockCop[1], WEAPONTYPE_PISTOL, 1000, TRUE)
//			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, RoadBlockCop[1], "TREV4POLICE3")
//			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(RoadBlockCop[1])
//		ENDIF
//		IF NOT DOES_ENTITY_EXIST(RoadBlockCop[2])
//			RoadBlockCop[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, << -926.1341, -2900.1794, 12.9447 >>, 191.8948)
//			GIVE_WEAPON_TO_PED(RoadBlockCop[2], WEAPONTYPE_PISTOL, 1000, TRUE)
//			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(RoadBlockCop[2])
//		ENDIF
//		roadBlockCopsCreated = TRUE
//		
//ENDPROC


//Contains all the traffic and parked cars to be played back during the uber recording.
PROC FILL_UBER_RECORDING2()

ParkedCarPos[0] = <<-583.6427, -2157.4971, 5.7947>>
ParkedCarQuatX[0] = -0.0003
ParkedCarQuatY[0] = -0.0085
ParkedCarQuatZ[0] = 0.9998
ParkedCarQuatW[0] = 0.0158
ParkedCarModel[0] = BLISTA

//TrafficCarPos[3] = <<-800.6000, -2521.1997, 13.6789>>
//TrafficCarQuatX[3] = 0.0023
//TrafficCarQuatY[3] = 0.0023
//TrafficCarQuatZ[3] = -0.1173
//TrafficCarQuatW[3] = 0.9931
//TrafficCarRecording[3] = 49
//TrafficCarStartime[3] = 18779.0000
//TrafficCarModel[3] = buffalo

TrafficCarPos[4] = <<-827.2035, -2575.6968, 13.5688>>
TrafficCarQuatX[4] = 0.0004
TrafficCarQuatY[4] = -0.0003
TrafficCarQuatZ[4] = -0.2526
TrafficCarQuatW[4] = 0.9676
TrafficCarRecording[4] = 50
TrafficCarStartime[4] = 20310.0000
TrafficCarModel[4] = manana

//TrafficCarPos[6] = <<-911.4415, -2709.6912, 20.2483>>
//TrafficCarQuatX[6] = 0.0018
//TrafficCarQuatY[6] = 0.0003
//TrafficCarQuatZ[6] = -0.3260
//TrafficCarQuatW[6] = 0.9454
//TrafficCarRecording[6] = 52
//TrafficCarStartime[6] = 24433.0000
//TrafficCarModel[6] = buffalo// Changed from uranus to buaffalo to fix the build error. |
//
//TrafficCarPos[7] = <<-943.0522, -2734.2688, 20.1081>>
//TrafficCarQuatX[7] = 0.0021
//TrafficCarQuatY[7] = 0.0023
//TrafficCarQuatZ[7] = -0.5553
//TrafficCarQuatW[7] = 0.8317
//TrafficCarRecording[7] = 53
//TrafficCarStartime[7] = 25378.0000
//TrafficCarModel[7] = buffalo// Changed from rhapsody to buaffalo to fix the build error. |

ParkedCarPos[1] = <<-1033.7168, -2949.0144, 13.4922>>
ParkedCarQuatX[1] = 0.0032
ParkedCarQuatY[1] = -0.0003
ParkedCarQuatZ[1] = -0.0255
ParkedCarQuatW[1] = 0.9997
ParkedCarModel[1] = bison2

SetPieceCarPos[0] = <<-1178.3645, -2845.9807, 13.9027>>
SetPieceCarQuatX[0] = 0.0018
SetPieceCarQuatY[0] = 0.0123
SetPieceCarQuatZ[0] = 0.9644
SetPieceCarQuatW[0] = 0.2642
SetPieceCarRecording[0] = 9
SetPieceCarStartime[0] = 22357
SetPieceCarRecordingSpeed[0] = 1.0000
SetPieceCarModel[0] = buzzard

//Only create this set piece if it's not been seen before
//IF g_bSetPieceTruckExplodeSeen = FALSE
	SetPieceCarPos[1] = <<-1376.1969, -2755.1675, 14.0646>>
	SetPieceCarQuatX[1] = -0.0012
	SetPieceCarQuatY[1] = 0.0013
	SetPieceCarQuatZ[1] = 0.8439
	SetPieceCarQuatW[1] = -0.5365
	SetPieceCarRecording[1] = 10
	SetPieceCarStartime[1] = 27135
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = packer
//ENDIF

//reversing plane
SetPieceCarPos[2] = <<-1342.5435, -2688.4822, 22.8254>>
SetPieceCarQuatX[2] = 0.0057
SetPieceCarQuatY[2] = -0.0014
SetPieceCarQuatZ[2] = -0.2451
SetPieceCarQuatW[2] = 0.9695
SetPieceCarRecording[2] = 11
SetPieceCarStartime[2] = 35135
SetPieceCarRecordingSpeed[2] = 1.0000
SetPieceCarModel[2] = jet

SetPieceCarPos[3] = <<-898.3506, -2698.9648, 20.1590>>
SetPieceCarQuatX[3] = 0.0065
SetPieceCarQuatY[3] = 0.0191
SetPieceCarQuatZ[3] = -0.2629
SetPieceCarQuatW[3] = 0.9646
SetPieceCarRecording[3] = 3
SetPieceCarStartime[3] = 6764.0000
SetPieceCarRecordingSpeed[3] = 1.0000
SetPieceCarModel[3] = bus

SetPieceCarPos[4] = <<-576.6550, -2231.9053, 5.2834>>
SetPieceCarQuatX[4] = 0.0043
SetPieceCarQuatY[4] = 0.0331
SetPieceCarQuatZ[4] = -0.3125
SetPieceCarQuatW[4] = 0.9493
SetPieceCarRecording[4] = 4
SetPieceCarStartime[4] = 2102.0000
SetPieceCarRecordingSpeed[4] = 1.0000
SetPieceCarModel[4] = bison

SetPieceCarPos[5] = <<-994.9925, -3154.1252, 23.1145>>
SetPieceCarQuatX[5] = 0.0056
SetPieceCarQuatY[5] = 0.0029
SetPieceCarQuatZ[5] = 0.4915
SetPieceCarQuatW[5] = 0.8708
SetPieceCarRecording[5] = 21
SetPieceCarStartime[5] = 92500.0000
SetPieceCarRecordingSpeed[5] = 1.0000
SetPieceCarModel[5] = jet

//SetPieceCarPos[6] = <<-1124.6035, -1958.0247, 136.5831>>
//SetPieceCarQuatX[6] = -0.0031
//SetPieceCarQuatY[6] = -0.0107
//SetPieceCarQuatZ[6] = 0.9610
//SetPieceCarQuatW[6] = 0.2765
//SetPieceCarRecording[6] = 17
//SetPieceCarStartime[6] = 64371.0000
//SetPieceCarRecordingSpeed[6] = 1.0000
//SetPieceCarModel[6] = stunt

//Only create this set piece if it's not been seen before
//IF g_bSetPieceTruckExplodeSeen = FALSE
	SetPieceCarPos[7] = <<-553.3315, -2073.9514, 6.8909>>
	SetPieceCarQuatX[7] = 0.0072
	SetPieceCarQuatY[7] = 0.0176
	SetPieceCarQuatZ[7] = 0.9739
	SetPieceCarQuatW[7] = -0.2263
	SetPieceCarRecording[7] = 54
	SetPieceCarStartime[7] = 0.0000
	SetPieceCarRecordingSpeed[7] = 1.0000
	SetPieceCarModel[7] = POLICE3
//ENDIF

SetPieceCarPos[8] = <<-561.7264, -2076.7615, 6.7664>>
SetPieceCarQuatX[8] = -0.0270
SetPieceCarQuatY[8] = 0.0016
SetPieceCarQuatZ[8] = 0.9169
SetPieceCarQuatW[8] = -0.3981
SetPieceCarRecording[8] = 108//55
SetPieceCarStartime[8] = 150.000 //0.00
SetPieceCarRecordingSpeed[8] = 1.0000
SetPieceCarModel[8] = POLICE3

//SetPieceCarPos[9] = <<-848.1229, -2360.5200, 16.8948>>
//SetPieceCarQuatX[9] = 0.0726
//SetPieceCarQuatY[9] = -0.0209
//SetPieceCarQuatZ[9] = 0.9971
//SetPieceCarQuatW[9] = -0.0111
//SetPieceCarRecording[9] = 56
//SetPieceCarStartime[9] = 10000.0000
//SetPieceCarRecordingSpeed[9] = 1.0000
//SetPieceCarModel[9] = police

//SetPieceCarPos[10] = <<-1028.8761, -2328.6292, 13.5678>>
//SetPieceCarQuatX[10] = -0.0083
//SetPieceCarQuatY[10] = -0.0056
//SetPieceCarQuatZ[10] = 0.5575
//SetPieceCarQuatW[10] = 0.8301
//SetPieceCarRecording[10] = 57
//SetPieceCarStartime[10] = 60000.0000
//SetPieceCarRecordingSpeed[10] = 1.0000
//SetPieceCarModel[10] = POLICE3

SetPieceCarPos[11] = <<-1563.9180, -2745.4094, 13.5643>>
SetPieceCarQuatX[11] = -0.0022
SetPieceCarQuatY[11] = -0.0019
SetPieceCarQuatZ[11] = 0.9280
SetPieceCarQuatW[11] = -0.3726
SetPieceCarRecording[11] = 111//59
SetPieceCarStartime[11] = 36000.0000
SetPieceCarRecordingSpeed[11] = 1.0000
SetPieceCarModel[11] = POLICE3

SetPieceCarPos[12] = <<-1026.0242, -2336.3955, 13.5678>>
SetPieceCarQuatX[12] = -0.0083
SetPieceCarQuatY[12] = -0.0054
SetPieceCarQuatZ[12] = 0.5500
SetPieceCarQuatW[12] = 0.8351
SetPieceCarRecording[12] = 112//58
SetPieceCarStartime[12] = 60000.0000
SetPieceCarRecordingSpeed[12] = 1.0000
SetPieceCarModel[12] = POLICE3

SetPieceCarPos[13] = <<-1013.3156, -2340.4570, 13.5678>>
SetPieceCarQuatX[13] = -0.0086
SetPieceCarQuatY[13] = -0.0050
SetPieceCarQuatZ[13] = 0.5052
SetPieceCarQuatW[13] = 0.8629
SetPieceCarRecording[13] = 113//60
SetPieceCarStartime[13] = 60000.0000
SetPieceCarRecordingSpeed[13] = 1.0000
SetPieceCarModel[13] = POLICE3

SetPieceCarPos[14] = <<-1221.0494, -2731.1687, 13.5681>>
SetPieceCarQuatX[14] = -0.0083
SetPieceCarQuatY[14] = -0.0057
SetPieceCarQuatZ[14] = 0.6469
SetPieceCarQuatW[14] = 0.7625
SetPieceCarRecording[14] = 61
SetPieceCarStartime[14] = 80000.0000
SetPieceCarRecordingSpeed[14] = 1.0000
SetPieceCarModel[14] = POLICE3

SetPieceCarPos[15] = <<-1227.2032, -2735.8723, 13.5680>>
SetPieceCarQuatX[15] = -0.0041
SetPieceCarQuatY[15] = -0.0091
SetPieceCarQuatZ[15] = 0.9133
SetPieceCarQuatW[15] = 0.4072
SetPieceCarRecording[15] = 62
SetPieceCarStartime[15] = 80000.0000
SetPieceCarRecordingSpeed[15] = 1.0000
SetPieceCarModel[15] = POLICE3

//SetPieceCarPos[16] = <<-1200.8358, -2705.1064, 13.5681>>
//SetPieceCarQuatX[16] = -0.0037
//SetPieceCarQuatY[16] = -0.0093
//SetPieceCarQuatZ[16] = 0.9591
//SetPieceCarQuatW[16] = 0.2828
//SetPieceCarRecording[16] = 63
//SetPieceCarStartime[16] = 90000.0000
//SetPieceCarRecordingSpeed[16] = 1.0000
//SetPieceCarModel[16] = POLICE3

//SetPieceCarPos[17] = <<-1194.2898, -2710.5107, 13.5680>>
//SetPieceCarQuatX[17] = -0.0017
//SetPieceCarQuatY[17] = -0.0098
//SetPieceCarQuatZ[17] = 0.9855
//SetPieceCarQuatW[17] = 0.1694
//SetPieceCarRecording[17] = 64
//SetPieceCarStartime[17] = 90000.0000
//SetPieceCarRecordingSpeed[17] = 1.0000
//SetPieceCarModel[17] = POLICE3

//SetPieceCarPos[18] = <<-1231.5563, -2196.7808, 14.0508>>
//SetPieceCarQuatX[18] = 0.0087
//SetPieceCarQuatY[18] = 0.0023
//SetPieceCarQuatZ[18] = 0.9819
//SetPieceCarQuatW[18] = -0.1890
//SetPieceCarRecording[18] = 65
//SetPieceCarStartime[18] = 55125.0000
//SetPieceCarRecordingSpeed[18] = 1.0000
//SetPieceCarModel[18] = firetruk

SetPieceCarPos[19] = <<-1240.0482, -2203.4565, 14.0347>>
SetPieceCarQuatX[19] = 0.0002
SetPieceCarQuatY[19] = 0.0018
SetPieceCarQuatZ[19] = 0.9611
SetPieceCarQuatW[19] = -0.2763
SetPieceCarRecording[19] = 66
SetPieceCarStartime[19] = 55125.0000
SetPieceCarRecordingSpeed[19] = 1.0000
SetPieceCarModel[19] = firetruk

SetPieceCarPos[20] = <<-438.0192, -2060.7515, 73.6354>>
SetPieceCarQuatX[20] = -0.0005
SetPieceCarQuatY[20] = 0.0018
SetPieceCarQuatZ[20] = 0.9559
SetPieceCarQuatW[20] = 0.2938
SetPieceCarRecording[20] = 67
SetPieceCarStartime[20] = 0.0000
SetPieceCarRecordingSpeed[20] = 1.0000
SetPieceCarModel[20] = MAVERICK

SetPieceCarPos[21] = <<-546.8047, -2187.9248, 5.8819>>
SetPieceCarQuatX[21] = -0.0252
SetPieceCarQuatY[21] = -0.0034
SetPieceCarQuatZ[21] = 0.9369
SetPieceCarQuatW[21] = 0.3487
SetPieceCarRecording[21] = 5
SetPieceCarStartime[21] = 3834.0000
SetPieceCarRecordingSpeed[21] = 1.0000
SetPieceCarModel[21] = BLISTA

SetPieceCarPos[22] = <<-1001.6743, -2947.7324, 13.3947>>
SetPieceCarQuatX[22] = 0.0010
SetPieceCarQuatY[22] = -0.0004
SetPieceCarQuatZ[22] = 0.8616
SetPieceCarQuatW[22] = 0.5075
SetPieceCarRecording[22] = 6
SetPieceCarStartime[22] = 20176.0000
SetPieceCarRecordingSpeed[22] = 1.0000
SetPieceCarModel[22] = airtug

SetPieceCarPos[23] = <<-1249.5879, -2525.9001, 13.3942>>
SetPieceCarQuatX[23] = 0.0010
SetPieceCarQuatY[23] = -0.0001
SetPieceCarQuatZ[23] = 0.9749
SetPieceCarQuatW[23] = -0.2228
SetPieceCarRecording[23] = 7
SetPieceCarStartime[23] = 48538.0000
SetPieceCarRecordingSpeed[23] = 1.0000
SetPieceCarModel[23] = airtug

SetPieceCarPos[24] = <<-1362.5382, -2625.6089, 13.3895>>
SetPieceCarQuatX[24] = -0.0001
SetPieceCarQuatY[24] = 0.0018
SetPieceCarQuatZ[24] = 0.5344
SetPieceCarQuatW[24] = 0.8452
SetPieceCarRecording[24] = 8
SetPieceCarStartime[24] = 75112.0000
SetPieceCarRecordingSpeed[24] = 1.0000
SetPieceCarModel[24] = airtug

ENDPROC


//PURPOSE: sets up end cops that player needs to kill/avoid in order to complete mission.
PROC SET_UP_END_COPS()

	IF EndCopsSetUp = FALSE
		
		IF NOT DOES_ENTITY_EXIST(endpoliceCar[0])
			endpoliceCar[0] = CREATE_VEHICLE(POLICE3,<< -994.5278, -2903.2852, 12.9447 >>, 158.7951)
			SET_VEHICLE_CAN_SAVE_IN_GARAGE(endpoliceCar[0], FALSE)
		ENDIF		

		IF NOT DOES_ENTITY_EXIST(EndCop[0])
			EndCop[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, << -969.0600, -2955.1858, 12.9450 >>, 241.1703)
			GIVE_WEAPON_TO_PED(EndCop[0], WEAPONTYPE_PISTOL, 1000, TRUE)
			SET_PED_ACCURACY(EndCop[0], 20)
			SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[0], TRUE)
			TASK_AIM_GUN_AT_ENTITY(EndCop[0], PLAYER_PED_ID(), -1, TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[0])
		ENDIF		
		
		IF NOT DOES_ENTITY_EXIST(GetAwayPlane)
			GetAwayPlane = CREATE_VEHICLE(SHAMAL,<< -968.3718, -2952.2976, 12.9451 >>, 114.9439)
			SET_VEHICLE_ON_GROUND_PROPERLY(GetAwayPlane)
			SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
			SET_ENTITY_HEALTH(GetAwayPlane, 2000)
			SET_VEHICLE_DOOR_OPEN(GetAwayPlane, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
		ENDIF

		IF NOT DOES_ENTITY_EXIST(EndCop[6])
			EndCop[6] = CREATE_PED_INSIDE_VEHICLE(endpoliceCar[0],PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
			GIVE_WEAPON_TO_PED(EndCop[6], WEAPONTYPE_PISTOL, 1000, TRUE)
			SET_PED_ACCURACY(EndCop[6], 20)
			SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[6], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[6], TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[6])
		ENDIF	
	
		IF NOT DOES_ENTITY_EXIST(EndCop[7])
			EndCop[7] = CREATE_PED_INSIDE_VEHICLE(endpoliceCar[0],PEDTYPE_COP, S_M_Y_COP_01)
			GIVE_WEAPON_TO_PED(EndCop[7], WEAPONTYPE_PISTOL, 1000, TRUE)
			SET_PED_ACCURACY(EndCop[7], 20)
			SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[7], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[7], TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[7])
		ENDIF

		IF NOT DOES_ENTITY_EXIST(endpoliceCar[1])
			endpoliceCar[1] = CREATE_VEHICLE(POLICE3,<< -962.2432, -2864.1602, 12.9447 >>, 149.5586)
			SET_VEHICLE_CAN_SAVE_IN_GARAGE(endpoliceCar[1], FALSE)
		ENDIF	
		
		IF NOT DOES_ENTITY_EXIST(EndCop[8])
			EndCop[8] = CREATE_PED_INSIDE_VEHICLE(endpoliceCar[1],PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
			GIVE_WEAPON_TO_PED(EndCop[8], WEAPONTYPE_PISTOL, 1000, TRUE)
			SET_PED_ACCURACY(EndCop[8], 20)
			SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[8], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[8], TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[8])
		ENDIF	
	
		IF NOT DOES_ENTITY_EXIST(EndCop[9])
			EndCop[9] = CREATE_PED_INSIDE_VEHICLE(endpoliceCar[1],PEDTYPE_COP, S_M_Y_COP_01)
			GIVE_WEAPON_TO_PED(EndCop[9], WEAPONTYPE_PISTOL, 1000, TRUE)
			SET_PED_ACCURACY(EndCop[9], 20)
			SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[9], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[9], TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[9])
		ENDIF	
		
		IF NOT DOES_ENTITY_EXIST(EndCop[10])
			EndCop[10] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, << -922.1185, -2939.5947, 12.9451 >>, 156.1408)
			GIVE_WEAPON_TO_PED(EndCop[10], WEAPONTYPE_PISTOL, 1000, TRUE)
			SET_PED_ACCURACY(EndCop[10], 20)
			SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[10], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[10], TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[10])
			SET_PED_DUCKING(EndCop[10], TRUE)
		ENDIF
		
		EndCopsSetUp = TRUE
		
	ENDIF
ENDPROC


PROC EQUIP_BEST_PLAYER_WEAPON(BOOL bForceIntoHand = true)
  
  IF NOT IS_PED_INJURED(PLAYER_PED_ID())
       
        WEAPON_TYPE bestWeapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
    
        IF bestWeapon != WEAPONTYPE_UNARMED
          	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), bestWeapon, bForceIntoHand)
    	ENDIF
		
  ENDIF
  
ENDPROC

//PURPOSE: Creates all entities required for the specific stage
PROC CREATE_ENTITIES_REQUIRED()
	
//	RSinteriorGroup = GET_INTERIOR_AT_COORDS(<<-937.1, -2969.5, 15>>)
	
	SWITCH missionStage
	
		CASE STAGE_GET_TO_CHASE
		
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
			CLEAR_AREA(<<-1026.8, -492.1, 36>>, 18, TRUE)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1042.5, -500, 41>>, <<-1019.5, -475.0, 34>>, FALSE)
			REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-1042.5, -500, 41>>, <<-1019.5, -475.0, 34>>)		
		
			REQUEST_MODEL(RAPIDGT)
			REQUEST_MODEL(SURANO)
			REQUEST_MODEL(CARBONIZZARE)
		
			WHILE NOT HAS_MODEL_LOADED(RAPIDGT)
			OR NOT HAS_MODEL_LOADED(SURANO)
			OR NOT HAS_MODEL_LOADED(CARBONIZZARE)
				PRINTSTRING("Waiting on assets 1 loading") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			//Create Players car
			IF NOT DOES_ENTITY_EXIST(PlayerCar)
				CLEAR_AREA(PlayerCarStartingCoords, 5, TRUE)
				PlayerCar = CREATE_VEHICLE(RAPIDGT, PlayerCarStartingCoords, fPlayerCarStartingHeading)
				SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
				SET_ENTITY_HEALTH(PlayerCar, 3000)
				SET_VEHICLE_STRONG(PlayerCar, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT, TRUE)	
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
			ENDIF	
			IF NOT DOES_ENTITY_EXIST(PlayerCar2)
				IF HAS_MODEL_LOADED(SURANO)
					CLEAR_AREA(<<-1037.3977, -491.6539, 35.5545>>, 5, TRUE)
					PlayerCar2 = CREATE_VEHICLE(SURANO, <<-1037.3977, -491.6539, 35.5545>>, 208.8890)
					SET_VEHICLE_COLOURS(PlayerCar2, 0, 0)
					SET_ENTITY_HEALTH(PlayerCar2, 3000)
					SET_VEHICLE_STRONG(PlayerCar2, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(SURANO)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(SURANO, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar2, TRUE)
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(PlayerCar3)
				IF HAS_MODEL_LOADED(CARBONIZZARE)
					CLEAR_AREA(PlayerCarStartingCoords, 5, TRUE)
					PlayerCar3 = CREATE_VEHICLE(CARBONIZZARE, <<-1033.9381, -489.7475, 35.8323>>, 207.1758)
					SET_VEHICLE_COLOURS(PlayerCar3, 89, 89)
					SET_ENTITY_HEALTH(PlayerCar3, 3000)
					SET_VEHICLE_STRONG(PlayerCar3, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(CARBONIZZARE)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(CARBONIZZARE, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar3, TRUE)
				ENDIF
			ENDIF
			
//			IF IS_INTERIOR_ENTITY_SET_ACTIVE(RSinteriorGroup, "Hanger_Blood_Splat")
//				DEACTIVATE_INTERIOR_ENTITY_SET(RSinteriorGroup, "Hanger_Blood_Splat")
//			ENDIF			
		
		BREAK
		
		CASE STAGE_CHASE_START
			
			PREPARE_MUSIC_EVENT("TRV4_CHASE")
			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())			
			
			REQUEST_EVERYTHING_FOR_UBER_CHASE()
		
			CLEAR_AREA(EnemyCarChaseStartingCoords, 200, TRUE)		
		
			//Create enemy car
			IF NOT DOES_ENTITY_EXIST(EnemyCar)
				EnemyCar = CREATE_VEHICLE(COGCABRIO, EnemyCarChaseStartingCoords, fEnemyCarChaseStartingHeading)
				SET_ENTITY_PROOFS(EnemyCar, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
				SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyCar, "SOL_3_MOLLY_CAR_Group")
			ENDIF
			
			//Create enemy ped
			IF NOT DOES_ENTITY_EXIST(EnemyPed)
				EnemyPed = CREATE_PED_INSIDE_VEHICLE(EnemyCar, PEDTYPE_MISSION, IG_MOLLY)
				SET_ENTITY_PROOFS(EnemyPed, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_ENTITY_INVINCIBLE(EnemyPed, TRUE)
				SET_PED_CAN_BE_TARGETTED(EnemyPed, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
				SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed, RELGROUPHASH_PLAYER)
			ENDIF		
		
			//Create players car and put him in it.
			IF IS_REPLAY_IN_PROGRESS()
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
							PlayerCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(PlayerCarChaseStartingCoords, fPlayerCarChaseStartingHeading)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
							SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
						ELSE
							PlayerCar = CREATE_VEHICLE(RAPIDGT, PlayerCarChaseStartingCoords, fPlayerCarChaseStartingHeading)
							SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
						ENDIF
					ELSE
						PlayerCar = CREATE_VEHICLE(RAPIDGT, PlayerCarChaseStartingCoords, fPlayerCarChaseStartingHeading)
						SET_VEHICLE_COLOURS(PlayerCar, 112, 112)							
					ENDIF	
					SET_ENTITY_HEALTH(PlayerCar, 3000)
					SET_VEHICLE_STRONG(PlayerCar, TRUE)
					SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, FALSE, FALSE, FALSE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					PlayerCar = CREATE_VEHICLE(RAPIDGT, PlayerCarChaseStartingCoords, fPlayerCarChaseStartingHeading)
					SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
					SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
					SET_ENTITY_HEALTH(PlayerCar, 3000)
					SET_VEHICLE_STRONG(PlayerCar, TRUE)
					SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, FALSE, FALSE, FALSE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
				ENDIF
			ENDIF
//			
//			IF IS_INTERIOR_ENTITY_SET_ACTIVE(RSinteriorGroup, "Hanger_Blood_Splat")
//				DEACTIVATE_INTERIOR_ENTITY_SET(RSinteriorGroup, "Hanger_Blood_Splat")
//			ENDIF		
		
		BREAK
		
//		CASE STAGE_END_OF_VEHICLE_CHASE_CUTSCENE
//		
//			//Flags
//			istageskip = 0
//			doneChaseAfterBadGuyText 	= TRUE
//			b_heliCamHelp 				= TRUE
//			camrecordingstarted 		= TRUE
//			FiremenSetup 				= TRUE
//			doneChat6 					= TRUE
//			doneChat5 					= TRUE
//			setpiecePlane2Setup 		= TRUE
//			trailerdetached 			= TRUE
//			setpiecePlane1Setup 		= TRUE
//			roadBlockCopsCreated 		= TRUE
//			distanceCheckRequired 		= FALSE
//			camsRendered				= FALSE
//			heliChaseCamActivated 		= FALSE
//			checkForEnemyPedDeath		= TRUE
//			
//			//Request assets for uber chase
//			REQUEST_MODEL(POLICE)
//			REQUEST_MODEL(POLMAV)	
//			REQUEST_MODEL(JET)
//			REQUEST_MODEL(IG_MOLLY)
//			REQUEST_MODEL(COGCABRIO)
//			REQUEST_MODEL(RAPIDGT)
//			REQUEST_VEHICLE_RECORDING(45, "BB_Chase")
//			REQUEST_VEHICLE_RECORDING(67, "BB_Chase")//heli set peice id 20
//			REQUEST_VEHICLE_RECORDING(61, "BB_Chase")//set piece cop car
//			REQUEST_VEHICLE_RECORDING(63, "BB_Chase")//set piece cop car
//			REQUEST_VEHICLE_RECORDING(64, "BB_Chase")//set piece cop car
//			REQUEST_PTFX_ASSET()
//			ScaleformMovie = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
//			
//			WHILE NOT HAS_MODEL_LOADED(JET)
//			OR NOT HAS_MODEL_LOADED(POLICE)
//			OR NOT HAS_MODEL_LOADED(POLMAV)
//			OR NOT HAS_MODEL_LOADED(IG_MOLLY)
//			OR NOT HAS_MODEL_LOADED(COGCABRIO)
//			OR NOT HAS_MODEL_LOADED(RAPIDGT)
//			OR NOT HAS_PTFX_ASSET_LOADED()
//			OR NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie)
//			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(45, "BB_Chase")
//			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(67, "BB_Chase")
//			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(61, "BB_Chase")
//			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(63, "BB_Chase")
//			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(64, "BB_Chase")
//				PRINTSTRING("Waiting on uber chase stuff loading")
//				WAIT(0)
//			ENDWHILE
//			
//			//Request assets for cutscene
//			REQUEST_VEHICLE_RECORDING(38, "BB_Chase")
//			REQUEST_WAYPOINT_RECORDING("Trev4_5")
//			
//			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(38, "BB_Chase")
//			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
//				PRINTSTRING("Waiting on waypoint/vehicle recordings loading")
//				WAIT(0)
//			ENDWHILE
//			
//			CLEAR_AREA(<<-927, -2917, 14>>, 50, TRUE)			
//			
//			CREATE_ROAD_BLOCK()
//
//			//Create enemy car
//			IF NOT DOES_ENTITY_EXIST(EnemyCar)
//				EnemyCar = CREATE_VEHICLE(COGCABRIO, EnemyCarChaseStartingCoords, fEnemyCarChaseStartingHeading)
//				SET_ENTITY_PROOFS(EnemyCar, TRUE, TRUE, TRUE, TRUE, TRUE)
//				SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
//				SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
//			ENDIF
//			
//			//Create enemy ped
//			IF NOT DOES_ENTITY_EXIST(EnemyPed)
//				EnemyPed = CREATE_PED_INSIDE_VEHICLE(EnemyCar, PEDTYPE_MISSION, IG_MOLLY)
//				SET_ENTITY_INVINCIBLE(EnemyPed, TRUE)
//				SET_PED_CAN_BE_TARGETTED(EnemyPed, FALSE)
//				SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed, TRUE)
//				SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed, FriendGroup)
//			ENDIF				
//
//			
//			IF DOES_ENTITY_EXIST(EnemyCar)
//				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//						
//						IF istageskip = 0
//							INITIALISE_UBER_PLAYBACK("BB_Chase", 45, TRUE)
//							PRINTSTRING("*************INITIALISE_UBER_PLAYBACK CALLED***************") PRINTNL()
//							istageskip = 1
//						ENDIF
//						IF istageskip = 1
//							FILL_UBER_RECORDING2()
//							PRINTSTRING("*************FILL_UBER_RECORDING CALLED***************") PRINTNL()
//							istageskip = 2
//						ENDIF
//						IF istageskip = 2
//							START_PLAYBACK_RECORDED_VEHICLE(EnemyCar, 45, "BB_Chase")
//							istageskip = 3
//						ENDIF
//						IF istageskip = 3
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
//								PRINTSTRING("Playback is going on for enemycar")
//								SET_UBER_PLAYBACK_TO_TIME_NOW(EnemyCar, 122000)
//								PRINTSTRING("*************PLAYBACK CALLED AND TIME SKIPPED***************")
//								iControlFlag = 0
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//calculate the uber recording play back speed.
//			IF NOT IS_ENTITY_DEAD(EnemyCar)
//				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//						PRINTSTRING("EnemyCar Playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(EnemyCar)) PRINTNL()
//						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 100, 2, 1)
//						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
//					ELSE
//						PRINTSTRING("Playback is not going on for enemycar") PRINTNL()
//					ENDIF
//				ENDIF		
//			ENDIF				
//			
//			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE)
//			
//			IF IS_INTERIOR_ENTITY_SET_ACTIVE(RSinteriorGroup, "Hanger_Blood_Splat")
//				DEACTIVATE_INTERIOR_ENTITY_SET(RSinteriorGroup, "Hanger_Blood_Splat")
//			ENDIF
//			
//			//Create players car and put him in it.
//			IF IS_REPLAY_IN_PROGRESS()
//				IF NOT DOES_ENTITY_EXIST(PlayerCar)
//					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
//						PlayerCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(<< -876.7273, -3012.7397, 12.9450 >>, 351.3571)
//					ELSE
//						PlayerCar = CREATE_VEHICLE(RAPIDGT, << -876.7273, -3012.7397, 12.9450 >>, 351.3571)
//						SET_VEHICLE_COLOURS(PlayerCar, 112, 112)							
//					ENDIF	
//					SET_ENTITY_HEALTH(PlayerCar, 3000)
//					SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, TRUE, FALSE, FALSE)
//				ENDIF
//			ELSE
//				IF NOT DOES_ENTITY_EXIST(PlayerCar)
//					PlayerCar = CREATE_VEHICLE(RAPIDGT, << -876.7273, -3012.7397, 12.9450 >>, 351.3571)
//					SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
//					SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
//					SET_ENTITY_HEALTH(PlayerCar, 3000)
//					SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, TRUE, FALSE, FALSE)
//				ENDIF
//			ENDIF					
//			
//		BREAK
	
		CASE STAGE_ON_FOOT_CHASE
		
			//Flags
//			enemypedWaypointPlaybackResumed 		= FALSE
			distanceCheckRequired 					= FALSE		
		
			CLEAR_AREA(<<-973.3, -2967.7, 13.5>>, 100, TRUE)
//			NEW_LOAD_SCENE_START(<< -939.2419, -2935.1624, 12.9449 >>, <<-937.2, -2930.0, 13.9>>, 20)
			
			SETTIMERA(0)

//			WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			AND TIMERA() < 6000 
//				PRINTSTRING("Waiting on new load scene ") PRINTNL()
//			    WAIT(0)
//			ENDWHILE	
			
//			NEW_LOAD_SCENE_STOP()

			REQUEST_MODEL(JET)
			REQUEST_MODEL(SHAMAL)
			REQUEST_WAYPOINT_RECORDING("BB_MOLLY_2")
			REQUEST_MODEL(S_M_Y_AirWorker)
			REQUEST_MODEL(S_M_Y_COP_01)
			REQUEST_MODEL(POLICE3)
			REQUEST_MODEL(IG_MOLLY)
			REQUEST_MODEL(COGCABRIO)
			REQUEST_MODEL(RAPIDGT)
			REQUEST_MODEL(prop_cs_film_reel_01)
			REQUEST_VEHICLE_RECORDING(68, "BB_Chase")
			REQUEST_VEHICLE_RECORDING(69, "BB_Chase")
			REQUEST_WAYPOINT_RECORDING("Trev4_5")
			REQUEST_PTFX_ASSET()
//			REQUEST_CUTSCENE("TRV_4_MCS_2")
			REQUEST_ANIM_DICT("move_f@film_reel_arms")
			REQUEST_ANIM_DICT("misssolomon_3")
			
			WHILE NOT HAS_MODEL_LOADED(JET)
			OR NOT HAS_MODEL_LOADED(SHAMAL)
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("BB_MOLLY_2")
			OR NOT HAS_MODEL_LOADED(S_M_Y_AirWorker)
			OR NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
			OR NOT HAS_MODEL_LOADED(POLICE3)
			OR NOT HAS_MODEL_LOADED(IG_MOLLY)
			OR NOT HAS_MODEL_LOADED(COGCABRIO)
			OR NOT HAS_MODEL_LOADED(RAPIDGT)
			OR NOT HAS_MODEL_LOADED(prop_cs_film_reel_01)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(68, "BB_Chase")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(69, "BB_Chase")
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
			OR NOT HAS_PTFX_ASSET_LOADED()
			OR NOT HAS_ANIM_DICT_LOADED("move_f@film_reel_arms")
			OR NOT HAS_ANIM_DICT_LOADED("misssolomon_3")
				PRINTSTRING("waiting for assets to load STAGE_ON_FOOT") PRINTNL()
				WAIT(0)
			ENDWHILE
//			
//			WHILE NOT HAS_CUTSCENE_LOADED()
//				WAIT(0)
//				PRINTSTRING("Waiting on cutscene TRV_4_MCS_2 loading")
//			ENDWHILE
			
			//Move the enemy ped into the correct place so as not to be too close to the player
			IF NOT DOES_ENTITY_EXIST(EnemyPed)
				EnemyPed = CREATE_PED(PEDTYPE_MISSION, IG_MOLLY,<< -920.5093, -2943.9299, 12.9451 >>, 157.6203)
				SET_ENTITY_INVINCIBLE(EnemyPed, TRUE)
				SET_PED_CAN_BE_TARGETTED(EnemyPed, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
				SET_ENTITY_LOAD_COLLISION_FLAG(EnemyPed, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed, RELGROUPHASH_PLAYER)
			ENDIF
			
			ObjFilmReel = CREATE_OBJECT(prop_cs_film_reel_01, <<-929, -2917, 13>>)
			ATTACH_ENTITY_TO_ENTITY(ObjFilmReel, EnemyPed, GET_PED_BONE_INDEX(EnemyPed, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)		

			//Set up end plane
			IF NOT DOES_ENTITY_EXIST(EndPlane)
				EndPlane = CREATE_VEHICLE(JET, << -952.1345, -2990.2688, 12.9451 >>, 240.7726)
				SET_VEHICLE_LIVERY(EndPlane, 2)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EndPlane, FALSE)
				FREEZE_ENTITY_POSITION(EndPlane, TRUE)
				SET_ENTITY_PROOFS(EndPlane, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(JET)
				SET_ENTITY_LOD_DIST(EndPlane, 500)
			ENDIF		
			
//			//Create cops outside
//			CREATE_ROAD_BLOCK()
			
			//Request and load checkpoint vehicle if on a replay	
			IF IS_REPLAY_IN_PROGRESS()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
					WHILE NOT  HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
						PRINTSTRING("Waiting for replay checkpoint vehicle loading")
						WAIT(0)
					ENDWHILE
				ENDIF

				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						PlayerCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-918.6263, -2926.6309, 12.9666>>, 43.1705)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
					ELSE
						PlayerCar = CREATE_VEHICLE(RAPIDGT, <<-918.6263, -2926.6309, 12.9666>>, 43.1705)
						SET_VEHICLE_COLOURS(PlayerCar, 112, 112)	
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
						SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					PlayerCar = CREATE_VEHICLE(RAPIDGT, <<-918.6263, -2926.6309, 12.9666>>, 43.1705)
					SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
					SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
				ENDIF
			ENDIF			
			
			//Set up translators car
			IF NOT DOES_ENTITY_EXIST(EnemyCar)
				EnemyCar = CREATE_VEHICLE(COGCABRIO, << -928.3279, -2916.1604, 12.9450 >>, 61.7688)
				SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
				SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
			ENDIF
			
			//Set up cop cars 
//			IF NOT DOES_ENTITY_EXIST(SetpieceCopCar[0])
//				SetpieceCopCar[0] = CREATE_VEHICLE(POLICE3, << -923.8265, -2919.5461, 12.9450 >>, 63.1086)
//			ENDIF
			IF NOT DOES_ENTITY_EXIST(SetpieceCopCar)
				SetpieceCopCar = CREATE_VEHICLE(POLICE3, << -924.2796, -2914.3708, 12.9450 >>, 86.6188)
			ENDIF
//			IF NOT DOES_ENTITY_EXIST(SetpieceCopCar[2])
//				SetpieceCopCar[2] = CREATE_VEHICLE(POLICE3, << -910.3781, -2919.8718, 12.9450 >>, 20.2950)
//				SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
//			ENDIF
			
			//Create cops on foot
			IF NOT DOES_ENTITY_EXIST(cop14)
				cop14 = CREATE_PED(PEDTYPE_COP ,S_M_Y_COP_01, << -922.9420, -2912.5088, 12.9450 >>, 156.2652)
				GIVE_WEAPON_TO_PED(cop14, WEAPONTYPE_PISTOL, 1000, TRUE)
				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(Cop14)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
			ENDIF	
//			IF NOT DOES_ENTITY_EXIST(cop16)
//				cop16 = CREATE_PED(PEDTYPE_COP ,S_M_Y_COP_01, << -908.0948, -2917.7725, 12.9450 >>, 95.3482)
//				GIVE_WEAPON_TO_PED(cop16, WEAPONTYPE_PISTOL, 1000, TRUE)
//				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(Cop16)
//			ENDIF	
//			IF NOT DOES_ENTITY_EXIST(cop17)
//				cop17 = CREATE_PED(PEDTYPE_COP ,S_M_Y_COP_01, << -907.9593, -2921.3430, 12.9450 >>, 115.8312)
//				GIVE_WEAPON_TO_PED(cop17, WEAPONTYPE_PISTOL, 1000, TRUE)
//				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
//				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(Cop17)
//			ENDIF			

			SETTIMERA(0)
			
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE)
			
//			IF IS_INTERIOR_ENTITY_SET_ACTIVE(RSinteriorGroup, "Hanger_Blood_Splat")
//				DEACTIVATE_INTERIOR_ENTITY_SET(RSinteriorGroup, "Hanger_Blood_Splat")
//			ENDIF			
			
			checkForEnemyPedDeath		= TRUE			
			
		BREAK
		
		CASE STAGE_EVADE_POLICE	
			
			CLEAR_AREA(<<-973.3, -2967.7, 13.5>>, 100, TRUE)
			PRINTNL() PRINTSTRING("CLEAR AREA BEING CALLED IN STAGE_EVADE_POLICE") PRINTNL()
//			NEW_LOAD_SCENE_START(<< -937.7, -2968, 14 >>, <<-945.4, -2968.4, 13.5>>, 20)
			
			SETTIMERA(0)
//
//			WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//			AND TIMERA() < 6000 
//				PRINTSTRING("Waiting on new load scene ") PRINTNL()
//			    WAIT(0)
//			ENDWHILE
			
//			NEW_LOAD_SCENE_STOP()
			
			//Flags
			distanceCheckRequired 		= FALSE
			checkForEnemyPedDeath		= FALSE		
			EndCopsSetUp 				= FALSE
			
			REQUEST_MODEL(JET)
			REQUEST_MODEL(S_M_Y_AirWorker)
			REQUEST_MODEL(SHAMAL)
			REQUEST_MODEL(S_M_Y_COP_01)
			REQUEST_MODEL(POLICE3)
			REQUEST_MODEL(COGCABRIO)
			REQUEST_MODEL(RAPIDGT)
			REQUEST_MODEL(Prop_Jet_Bloodsplat_01)
			REQUEST_VEHICLE_RECORDING(68, "BB_Chase")
			REQUEST_VEHICLE_RECORDING(69, "BB_Chase")
			REQUEST_PTFX_ASSET()	
		
			WHILE NOT HAS_MODEL_LOADED(JET)
			OR NOT HAS_MODEL_LOADED(S_M_Y_AirWorker)
			OR NOT HAS_MODEL_LOADED(SHAMAL)
			OR NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
			OR NOT HAS_MODEL_LOADED(POLICE3)
			OR NOT HAS_MODEL_LOADED(COGCABRIO)
			OR NOT HAS_MODEL_LOADED(RAPIDGT)
			OR NOT HAS_MODEL_LOADED(Prop_Jet_Bloodsplat_01)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(68, "BB_Chase")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(69, "BB_Chase")
			OR NOT HAS_PTFX_ASSET_LOADED()
				PRINTSTRING("waiting for assets to load STAGE_EVADE_POLICE") PRINTNL()
				WAIT(0)
			ENDWHILE		
			
			SET_UP_END_COPS()
			
			//Request and load checkpoint vehicle if on a replay	
			IF IS_REPLAY_IN_PROGRESS()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
					WHILE NOT  HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
						PRINTSTRING("Waiting for replay checkpoint vehicle loading")
						WAIT(0)
					ENDWHILE
				ENDIF

				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						PlayerCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-918.6263, -2926.6309, 12.9666>>, 43.1705)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
					ELSE
						PlayerCar = CREATE_VEHICLE(RAPIDGT, <<-918.6263, -2926.6309, 12.9666>>, 43.1705)
						SET_VEHICLE_COLOURS(PlayerCar, 112, 112)	
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
						SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					PlayerCar = CREATE_VEHICLE(RAPIDGT, <<-918.6263, -2926.6309, 12.9666>>, 43.1705)
					SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
					SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
				ENDIF
			ENDIF			
			
			//Set up translators car
			IF NOT DOES_ENTITY_EXIST(EnemyCar)
				EnemyCar = CREATE_VEHICLE(COGCABRIO, << -928.3279, -2916.1604, 12.9450 >>, 61.7688)
				SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
				SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
			ENDIF			
			
			//cop car
			IF NOT DOES_ENTITY_EXIST(SetpieceCopCar)
				SetpieceCopCar = CREATE_VEHICLE(POLICE3, << -924.2796, -2914.3708, 12.9450 >>, 86.6188)
			ENDIF	
			
			//Create cop on foot
			IF NOT DOES_ENTITY_EXIST(cop14)
				cop14 = CREATE_PED(PEDTYPE_COP ,S_M_Y_COP_01, << -922.9420, -2912.5088, 12.9450 >>, 156.2652)
				GIVE_WEAPON_TO_PED(cop14, WEAPONTYPE_PISTOL, 1000, TRUE)
				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(Cop14)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
			ENDIF	
			
			//Create the blood splat on the ground
			IF NOT DOES_ENTITY_EXIST(objBloodSplat)
				objBloodSplat = CREATE_OBJECT(Prop_Jet_Bloodsplat_01, <<-946.4231, -2979.8259, 12.9264>>)
				SET_ENTITY_ROTATION(objBloodSplat, <<-0.0000, -0.0000, -121.5948>>)
				SET_ENTITY_QUATERNION(objBloodSplat, -0.0000, 0.0000, 0.8729, -0.4879)
				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Jet_Bloodsplat_01)
			ENDIF			
			
			//Set up end plane
			IF NOT DOES_ENTITY_EXIST(EndPlane)
				EndPlane = CREATE_VEHICLE(JET, << -952.1345, -2990.2688, 12.9451 >>, 240.7726)
				SET_VEHICLE_LIVERY(EndPlane, 2)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EndPlane, FALSE)
				FREEZE_ENTITY_POSITION(EndPlane, TRUE)
				SET_ENTITY_PROOFS(EndPlane, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(JET)
				EngineDamagePTFX = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_engine_damage", EndPlane, <<-11.956, 10.528, -7.657>>, <<0,2,0>>)
				SET_ENTITY_LOD_DIST(EndPlane, 500)
			ENDIF
			
			//Set up airworker
			IF NOT DOES_ENTITY_EXIST(AirWorker)
				AirWorker = CREATE_PED(PEDTYPE_MISSION, S_M_Y_AirWorker, <<-933.8942, -2965.7854, 12.9451>>, 331.7339)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, AirWorker, "HangerWorker")
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_AirWorker)
				SET_PED_CAN_BE_TARGETTED(AirWorker, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, TRUE)
				TASK_SMART_FLEE_PED(AirWorker, PLAYER_PED_ID(), 150, -1)
				SET_PED_KEEP_TASK(AirWorker, TRUE)
				SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(AirWorker)
			ENDIF			
			
			IF GET_FAIL_WEAPON(0) = WEAPONTYPE_INVALID
			OR GET_FAIL_WEAPON(0) = WEAPONTYPE_UNARMED
				EQUIP_BEST_PLAYER_WEAPON(TRUE)	
			ENDIF
			
//			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(RSinteriorGroup, "Hanger_Blood_Splat")
//				ACTIVATE_INTERIOR_ENTITY_SET(RSinteriorGroup, "Hanger_Blood_Splat")
//			ENDIF				
			
		BREAK
		
	ENDSWITCH

ENDPROC

//PURPOSE: Handles the players positioning for each stage
PROC SET_PLAYER_COORDS()

	SWITCH missionStage		
		
		CASE STAGE_GET_TO_CHASE
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1019.5793, -484.8720, 36.0795>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 93.7701)
		BREAK
		
		CASE STAGE_CHASE_START
			
			IF IS_VEHICLE_DRIVEABLE(PlayerCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PlayerCar)
				ENDIF	
				SET_ENTITY_HEALTH(PlayerCar, 3000)
				SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, FALSE, FALSE, FALSE)
			ENDIF
			
		BREAK
		
		CASE STAGE_ON_FOOT_CHASE
		
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-935.4413, -2928.0610, 12.9451>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 178.4660)
			SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)			
		
		BREAK
		
		CASE STAGE_EVADE_POLICE	
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -937.5466, -2968.7126, 12.9451 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 111.5016)
			SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)	
			
		BREAK
		
	ENDSWITCH

ENDPROC


PROC STATS_CONTROLER()
	
	//SOL3_TIME is for recorded outwith this script to see how long the player takes to complete the mission
	
	//SOL3_HEADSHOTS - All scripted cops have been added to the head shot checklist for this mission 
	
	//SOL3_DAMAGE - Checks how much damage the player takes throughout mission.
	IF PlayerAddedForHealthStat = FALSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), SOL3_DAMAGE)
			PlayerAddedForHealthStat = TRUE
		ENDIF
	ENDIF
	
	//SOL3_MAX_SPEED - Fastest speed reached in any vehicle on the mission.
	//AND
	//SOL3_CAR_DAMAGE - Total car damage taken by all vehicle used by the player throughout the mission.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF PlayerCarAddedForSpeedStat = FALSE
				PlayerSpeedStatVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(PlayerSpeedStatVehicle)
					IF IS_VEHICLE_DRIVEABLE(PlayerSpeedStatVehicle)
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(PlayerSpeedStatVehicle)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PlayerSpeedStatVehicle, SOL3_CAR_DAMAGE)
						PlayerCarAddedForSpeedStat = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, SOL3_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
			IF PlayerCarAddedForSpeedStat = TRUE
				PlayerCarAddedForSpeedStat = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//SOL3_ACCURACY - This is tracked outwith the mission script. Gets the players accuracy
	
	//SOL3_BULLETS_FIRED - This is tracked outwith the mission script. Gives the total number of bullets fired on mission.
	
	//SOL3_KILLS - Total number of enemies killed on this mission.
	IF KilledStatDone[0] = FALSE
		IF DOES_ENTITY_EXIST(RoadBlockCop[0])
			IF IS_PED_INJURED(RoadBlockCop[0])
				iEnemiesKilled++
				KilledStatDone[0] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[1] = FALSE
		IF DOES_ENTITY_EXIST(RoadBlockCop[1])
			IF IS_PED_INJURED(RoadBlockCop[1])
				iEnemiesKilled++
				KilledStatDone[1] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[2] = FALSE
		IF DOES_ENTITY_EXIST(RoadBlockCop[2])
			IF IS_PED_INJURED(RoadBlockCop[2])
				iEnemiesKilled++
				KilledStatDone[2] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[3] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[0])
			IF IS_PED_INJURED(EndCop[0])
				iEnemiesKilled++
				KilledStatDone[3] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[4] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[1])
			IF IS_PED_INJURED(EndCop[1])
				iEnemiesKilled++
				KilledStatDone[4] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[5] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[2])
			IF IS_PED_INJURED(EndCop[2])
				iEnemiesKilled++
				KilledStatDone[5] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[6] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[3])
			IF IS_PED_INJURED(EndCop[3])
				iEnemiesKilled++
				KilledStatDone[6] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[7] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[4])
			IF IS_PED_INJURED(EndCop[4])
				iEnemiesKilled++
				KilledStatDone[7] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[8] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[5])
			IF IS_PED_INJURED(EndCop[5])
				iEnemiesKilled++
				KilledStatDone[8] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[9] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[6])
			IF IS_PED_INJURED(EndCop[6])
				iEnemiesKilled++
				KilledStatDone[9] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[10] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[7])
			IF IS_PED_INJURED(EndCop[7])
				iEnemiesKilled++
				KilledStatDone[10] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[11] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[8])
			IF IS_PED_INJURED(EndCop[8])
				iEnemiesKilled++
				KilledStatDone[11] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[12] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[9])
			IF IS_PED_INJURED(EndCop[9])
				iEnemiesKilled++
				KilledStatDone[12] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[13] = FALSE
		IF DOES_ENTITY_EXIST(EndCop[10])
			IF IS_PED_INJURED(EndCop[10])
				iEnemiesKilled++
				KilledStatDone[13] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[14] = FALSE
		IF DOES_ENTITY_EXIST(Cop14)
			IF IS_PED_INJURED(Cop14)
				iEnemiesKilled++
				KilledStatDone[14] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[15] = FALSE
		IF DOES_ENTITY_EXIST(Cop16)
			IF IS_PED_INJURED(Cop16)
				iEnemiesKilled++
				KilledStatDone[15] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[16] = FALSE
		IF DOES_ENTITY_EXIST(Cop17)
			IF IS_PED_INJURED(Cop17)
				iEnemiesKilled++
				KilledStatDone[16] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF KilledStatDone[17] = FALSE
		IF DOES_ENTITY_EXIST(AIPoliceCop[0])
			IF IS_PED_INJURED(AIPoliceCop[0])
				iEnemiesKilled++
				KilledStatDone[17] = TRUE
			ENDIF
		ENDIF
	ENDIF
//	IF KilledStatDone[18] = FALSE
//		IF DOES_ENTITY_EXIST(AIPoliceCop[1])
//			IF IS_PED_INJURED(AIPoliceCop[1])
//				iEnemiesKilled++
//				KilledStatDone[18] = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	//SOL3_COP_LOSS_TIME - Time taken to lose the wanted level.
	IF missionStage = STAGE_EVADE_POLICE
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
			IF LoseCopsStatTimerStarted = FALSE
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(SOL3_COP_LOSS_TIME)
				LoseCopsStatTimerStarted = TRUE
			ENDIF
		ENDIF
	ENDIF
	//Note the timer is stopped in the mission stage when the player has lost his wanted level to pass the mission.
	
	//SOL3_INNOCENT_KILLS - Innocent peds killed, controlled outwith my script.
	
	//SOL3_NEWS_HELI_CAM_TIME - Total time in ms which the player views the news heli cam.
	//This is checked in the missionstage STAGE_MAIN_CHASE
	
		
ENDPROC

PROC Mission_Cleanup()

//	REMOVE_PARTICLE_FX(C4[0])
//	REMOVE_PARTICLE_FX(C4[1])
//	REMOVE_PARTICLE_FX(C4[2])
	
//	STOP_CAM_RECORDING(CamRagData)
	
	SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, TRUE)
	
	SET_AUDIO_FLAG("PoliceScannerDisabled", FALSE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1042.5, -500, 41>>, <<-1019.5, -475.0, 34>>, TRUE)
	
	//Reset list of head shot people from stats
	RESET_MISSION_STATS_ENTITY_WATCH()

	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	SET_IGNORE_NO_GPS_FLAG(FALSE)
	
	//Release all audio
	RELEASE_SCRIPT_AUDIO_BANK()
	
	IF DOES_ENTITY_EXIST(VehCam)
		DELETE_VEHICLE(VehCam)
	ENDIF	
	
	//Re-enable taxi hailing
	DISABLE_TAXI_HAILING(FALSE)
	
	//Navmesh blocking objects
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(NavBlockingArea1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(NavBlockingArea1)
	ENDIF	
	
	//clean up all Peds 
	IF DOES_ENTITY_EXIST(EnemyPed)
		IF NOT IS_PED_INJURED(EnemyPed)
			SET_PED_KEEP_TASK(EnemyPed, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EnemyPed)
	ENDIF
	IF DOES_ENTITY_EXIST(AirWorker)
		IF NOT IS_PED_INJURED(AirWorker)
			SET_PED_KEEP_TASK(AirWorker, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(AirWorker)
	ENDIF
	IF DOES_ENTITY_EXIST(truckDriver)
		IF NOT IS_PED_INJURED(truckDriver)
			SET_PED_KEEP_TASK(truckDriver, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(truckDriver)
	ENDIF
	IF DOES_ENTITY_EXIST(DivingWorker1)
		IF NOT IS_PED_INJURED(DivingWorker1)
			SET_PED_KEEP_TASK(DivingWorker1, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(DivingWorker1)
	ENDIF
	IF DOES_ENTITY_EXIST(DivingWorker2)
		IF NOT IS_PED_INJURED(DivingWorker2)
			SET_PED_KEEP_TASK(DivingWorker2, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(DivingWorker2)
	ENDIF
	IF DOES_ENTITY_EXIST(Cop14)
		IF NOT IS_PED_INJURED(Cop14)
			SET_PED_KEEP_TASK(Cop14, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Cop14)
	ENDIF
	IF DOES_ENTITY_EXIST(Cop16)
		IF NOT IS_PED_INJURED(Cop16)
			SET_PED_KEEP_TASK(Cop16, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Cop16)
	ENDIF
	IF DOES_ENTITY_EXIST(Cop17)
		IF NOT IS_PED_INJURED(Cop17)
			SET_PED_KEEP_TASK(Cop17, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Cop17)
	ENDIF
	IF DOES_ENTITY_EXIST(Fireman[0])
		IF NOT IS_PED_INJURED(Fireman[0])
			SET_PED_KEEP_TASK(Fireman[0], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Fireman[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(Fireman[1])
		IF NOT IS_PED_INJURED(Fireman[1])
			SET_PED_KEEP_TASK(Fireman[1], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Fireman[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(Fireman[2])
		IF NOT IS_PED_INJURED(Fireman[2])
			SET_PED_KEEP_TASK(Fireman[2], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Fireman[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(Fireman[3])
		IF NOT IS_PED_INJURED(Fireman[3])
			SET_PED_KEEP_TASK(Fireman[3], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(Fireman[3])
	ENDIF
	IF DOES_ENTITY_EXIST(RoadBlockCop[0])
		IF NOT IS_PED_INJURED(RoadBlockCop[0])
			SET_PED_KEEP_TASK(RoadBlockCop[0], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(RoadBlockCop[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(RoadBlockCop[1])
		IF NOT IS_PED_INJURED(RoadBlockCop[1])
			SET_PED_KEEP_TASK(RoadBlockCop[1], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(RoadBlockCop[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(RoadBlockCop[2])
		IF NOT IS_PED_INJURED(RoadBlockCop[2])
			SET_PED_KEEP_TASK(RoadBlockCop[2], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(RoadBlockCop[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[0])
		IF NOT IS_PED_INJURED(EndCop[0])
			SET_PED_KEEP_TASK(EndCop[0], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[1])
		IF NOT IS_PED_INJURED(EndCop[1])
			SET_PED_KEEP_TASK(EndCop[1], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[2])
		IF NOT IS_PED_INJURED(EndCop[2])
			SET_PED_KEEP_TASK(EndCop[2], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[3])
		IF NOT IS_PED_INJURED(EndCop[3])
			SET_PED_KEEP_TASK(EndCop[3], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[3])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[4])
		IF NOT IS_PED_INJURED(EndCop[4])
			SET_PED_KEEP_TASK(EndCop[4], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[4])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[5])
		IF NOT IS_PED_INJURED(EndCop[5])
			SET_PED_KEEP_TASK(EndCop[5], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[5])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[6])
		IF NOT IS_PED_INJURED(EndCop[6])
			SET_PED_KEEP_TASK(EndCop[6], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[6])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[7])
		IF NOT IS_PED_INJURED(EndCop[7])
			SET_PED_KEEP_TASK(EndCop[7], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[7])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[8])
		IF NOT IS_PED_INJURED(EndCop[8])
			SET_PED_KEEP_TASK(EndCop[8], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[8])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[9])
		IF NOT IS_PED_INJURED(EndCop[9])
			SET_PED_KEEP_TASK(EndCop[9], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[9])
	ENDIF	
	IF DOES_ENTITY_EXIST(EndCop[10])
		IF NOT IS_PED_INJURED(EndCop[10])
			SET_PED_KEEP_TASK(EndCop[10], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(EndCop[10])
	ENDIF
	IF DOES_ENTITY_EXIST(AIPoliceCop[0])
		IF NOT IS_PED_INJURED(AIPoliceCop[0])
			SET_PED_KEEP_TASK(AIPoliceCop[0], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(AIPoliceCop[0])
	ENDIF	
//	IF DOES_ENTITY_EXIST(AIPoliceCop[1])
//		SET_PED_KEEP_TASK(AIPoliceCop[1], TRUE)
//		SET_PED_AS_NO_LONGER_NEEDED(AIPoliceCop[1])
//	ENDIF	
	IF DOES_ENTITY_EXIST(setPiecePlanePilot[0])
		IF NOT IS_PED_INJURED(setPiecePlanePilot[0])
			SET_PED_KEEP_TASK(setPiecePlanePilot[0], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(setPiecePlanePilot[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(setPiecePlanePilot[1])
		IF NOT IS_PED_INJURED(setPiecePlanePilot[1])
			SET_PED_KEEP_TASK(setPiecePlanePilot[1], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(setPiecePlanePilot[1])
	ENDIF	
	
	//clean up all vehicles
	IF DOES_ENTITY_EXIST(EnemyCar)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(EnemyCar)
	ENDIF
	IF DOES_ENTITY_EXIST(PlayerCar)
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerCar, FALSE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(FireEngine)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(FireEngine)
	ENDIF	
	IF DOES_ENTITY_EXIST(Trailer)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(Trailer)
	ENDIF	
	IF DOES_ENTITY_EXIST(EndPlane)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(EndPlane)
	ENDIF	
	IF DOES_ENTITY_EXIST(GetAwayPlane)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayPlane)
	ENDIF	
	IF DOES_ENTITY_EXIST(VehCam)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(VehCam)
	ENDIF
	IF DOES_ENTITY_EXIST(PlayerSpeedStatVehicle)
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerSpeedStatVehicle, FALSE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerSpeedStatVehicle)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[1])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(SetPiecePlane[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPiecePlane[0])
	ENDIF
	IF DOES_ENTITY_EXIST(SetPiecePlane[1])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPiecePlane[1])
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(GetAwayCar[1])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(endpoliceCar[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(endpoliceCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(endpoliceCar[1])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(endpoliceCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(AIPoliceCar[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(AIPoliceCar[0])
	ENDIF
//	IF DOES_ENTITY_EXIST(AIPoliceCar[1])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(AIPoliceCar[1])
//	ENDIF
//	IF DOES_ENTITY_EXIST(RoadBlockCopCar[0])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(RoadBlockCopCar[0])
//	ENDIF
//	IF DOES_ENTITY_EXIST(RoadBlockCopCar[1])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(RoadBlockCopCar[1])
//	ENDIF
//	IF DOES_ENTITY_EXIST(RoadBlockCopCar[2])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(RoadBlockCopCar[2])
//	ENDIF
//	IF DOES_ENTITY_EXIST(SetpieceCopCar[0])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(SetpieceCopCar[0])
//	ENDIF
	IF DOES_ENTITY_EXIST(SetpieceCopCar)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(SetpieceCopCar)
	ENDIF
//	IF DOES_ENTITY_EXIST(SetpieceCopCar[2])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(SetpieceCopCar[2])
//	ENDIF	
	
	CANCEL_MUSIC_EVENT("TRV4_START")
	CANCEL_MUSIC_EVENT("TRV4_GAMEPLAY_START")
	CANCEL_MUSIC_EVENT("TRV4_START_CS_SKIP")
	CANCEL_MUSIC_EVENT("TRV4_START_RT")
	CANCEL_MUSIC_EVENT("TRV4_FOOT_CHASE_RT")
	CANCEL_MUSIC_EVENT("TRV4_EVADE_RT")
	CANCEL_MUSIC_EVENT("TRV4_CAR_ENTERED")
	CANCEL_MUSIC_EVENT("TRV4_AIRPORT_ENTERED")
	CANCEL_MUSIC_EVENT("TRV4_CHASE")
	CANCEL_MUSIC_EVENT("TRV4_JET_ENTERED")	
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoor)
		REMOVE_DOOR_FROM_SYSTEM(iDoor)
	ENDIF	
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(EngineDamagePTFX)
		REMOVE_PARTICLE_FX(EngineDamagePTFX)
	ENDIF
	
	CLEAR_GPS_MULTI_ROUTE()
	
//	CLEAR_TIMECYCLE_MODIFIER()
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformMovie)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("digitalOverlay")
	
	SET_ROADS_IN_AREA(<<-617, -2274, 0.0>>, <<-406.01, -2044, 15.0>>, TRUE)
	SET_ROADS_IN_AREA(<<-482, -2118, 6>>, <<-1072, -2724, 17>>, TRUE)
	
	IF GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
		USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("Trev4_5", FALSE, 0.5, 1)
	ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	SET_TIME_SCALE(1)
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	REMOVE_ANIM_DICT("missheist_agency3aig_lift_waitped_a")
	
	DISABLE_CELLPHONE(FALSE)
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	IF IS_RESTRICTED_AREA_WANTED_LEVEL_SUPPRESSED(AC_AIRPORT_AIRSIDE)
		RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
	ENDIF
//	IF IS_VALID_INTERIOR(InteriorHanger)
//		UNPIN_INTERIOR(InteriorHanger)
//		SET_INTERIOR_ACTIVE(InteriorHanger, FALSE)
//	ENDIF
	
	PRINTSTRING("...Placeholder Mission Cleanup")
	PRINTNL()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Mission Passed")
	PRINTNL()
	
	INFORM_MISSION_STATS_OF_INCREMENT(SOL3_KILLS, iEnemiesKilled)
	
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed()
	
	STORE_FAIL_WEAPON(PLAYER_PED_ID(), 0)
	
	PRINTSTRING("...Placeholder Mission Failed")
	PRINTNL()

	Mission_Cleanup() // must only take 1 frame and terminate the thread
ENDPROC



//═════════════════════════════════╡ DEBUG ╞═══════════════════════════════════

//DEBUG STAGE SELECTOR NAMING
#IF IS_DEBUG_BUILD
PROC SET_DEBUG_STAGE_NAMES()

	SkipMenuStruct[0].sTxtLabel = "STAGE_START_MISSION"			// Stage 0 Name
	SkipMenuStruct[1].sTxtLabel = "STAGE_START_CHASE"  			// Stage 1 Name                
	SkipMenuStruct[2].sTxtLabel = "STAGE_ON_FOOT_CHASE"  		// Stage 2 Name
//	SkipMenuStruct[3].sTxtLabel = "CUTSCENE - TRV_4_MCS_2" 		// Stage 4 Name
//	SkipMenuStruct[3].bSelectable = FALSE
	SkipMenuStruct[3].sTxtLabel = "STAGE_EVADE_POLICE"  		// Stage 3 Name

ENDPROC
#ENDIF

//PURPOSE: Handles all skipping/warping/replays. Including any debugging.
PROC STAGE_SELECTOR_MISSION_SETUP()
	
	SET_WEATHER_TYPE_NOW("EXTRASUNNY")
	
	REMOVE_BLIPS()
	
	DELETE_ALL_ENTITIES()			
	
	CREATE_ENTITIES_REQUIRED()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_PLAYER_COORDS()	
	ENDIF
	
	GameCanFail = TRUE
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)	
	
ENDPROC
//#ENDIF

//Skip cutscenes if the player wishes to.
PROC SkipCutscene()

	IF	IS_CUTSCENE_SKIP_BUTTON_PRESSED()
		SWITCH missionStage
			
			
		ENDSWITCH
	ENDIF

ENDPROC

PROC  CLEANUP_PARTICLE_EFFECTS()

	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[0])
		STOP_PARTICLE_FX_LOOPED(C4[0])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[1])
		STOP_PARTICLE_FX_LOOPED(C4[1])
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[2])
		STOP_PARTICLE_FX_LOOPED(C4[2])
	ENDIF
//	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[3])
//		STOP_PARTICLE_FX_LOOPED(C4[3])
//	ENDIF
//	IF DOES_PARTICLE_FX_LOOPED_EXIST(C4[4])
//		STOP_PARTICLE_FX_LOOPED(C4[4])
//	ENDIF
	
	IF loudFireSoundIDPlayed = TRUE
		RELEASE_SOUND_ID(loudFireSoundID)
		loudFireSoundIDCreated = FALSE
	ENDIF	
	
ENDPROC

//PURPOSE: Returns a unit vector and multiplies it if you so wish
FUNC VECTOR GET_UNIT_VECTOR(VECTOR v_start,VECTOR v_end)
        VECTOR v_unit
		
			v_unit.x = v_end.x - v_start.x
			v_unit.y = v_end.y - v_start.y
			v_unit.z = v_end.z - v_start.z
			
        RETURN v_unit
ENDFUNC

//Sets up the end of the mission for skipping to end
PROC SET_UP_MISSION_PASSED()
	
	IF iControlFlag = 0
	
		CLEAR_AREA(<< -920.0547, -2744.6616, 12.8434 >>, 20, TRUE)
					
		//Move player
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -920.0547, -2744.6616, 12.8434 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 357.1467)
//		ENDIF
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		iControlFlag ++
		
	ENDIF
	
	IF iControlFlag = 1
		
		LOAD_SCENE(<< -920.0547, -2744.6616, 12.8434 >>)
		
		IF NOT IS_SCREEN_FADED_IN()
			END_REPLAY_SETUP()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		Mission_Passed()
	
	ENDIF
	
ENDPROC

PROC DO_STAGE_INIT_MISSION()
	
	PRINTSTRING("INITIALISE") PRINTNL()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT Is_Replay_In_Progress()
		//Create the 3 players cars if on repeat play
		IF IS_REPEAT_PLAY_ACTIVE()
			PRINTSTRING("THIS IS A REPEAT PLAY") PRINTNL()	
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
			REQUEST_MODEL(RAPIDGT)
			REQUEST_MODEL(SURANO)
			REQUEST_MODEL(CARBONIZZARE)
		
			WHILE NOT HAS_MODEL_LOADED(RAPIDGT)
			OR NOT HAS_MODEL_LOADED(SURANO)
			OR NOT HAS_MODEL_LOADED(CARBONIZZARE)
				PRINTSTRING("Waiting on assets for repeat play loading") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			//Create Players car
			IF NOT DOES_ENTITY_EXIST(PlayerCar)
				CLEAR_AREA(PlayerCarStartingCoords, 5, TRUE)
				PlayerCar = CREATE_VEHICLE(RAPIDGT, PlayerCarStartingCoords, fPlayerCarStartingHeading)
				SET_VEHICLE_COLOURS(PlayerCar, 112, 112)
				SET_ENTITY_HEALTH(PlayerCar, 3000)
				SET_VEHICLE_STRONG(PlayerCar, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT, TRUE)	
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PlayerCar, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
			ENDIF	
			IF NOT DOES_ENTITY_EXIST(PlayerCar2)
				IF HAS_MODEL_LOADED(SURANO)
					CLEAR_AREA(<<-1037.3977, -491.6539, 35.5545>>, 5, TRUE)
					PlayerCar2 = CREATE_VEHICLE(SURANO, <<-1037.3977, -491.6539, 35.5545>>, 208.8890)
					SET_VEHICLE_COLOURS(PlayerCar2, 0, 0)
					SET_ENTITY_HEALTH(PlayerCar2, 3000)
					SET_VEHICLE_STRONG(PlayerCar2, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(SURANO)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(SURANO, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar2, TRUE)
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(PlayerCar3)
				IF HAS_MODEL_LOADED(CARBONIZZARE)
					CLEAR_AREA(PlayerCarStartingCoords, 5, TRUE)
					PlayerCar3 = CREATE_VEHICLE(CARBONIZZARE, <<-1033.9381, -489.7475, 35.8323>>, 207.1758)
					SET_VEHICLE_COLOURS(PlayerCar3, 89, 89)
					SET_ENTITY_HEALTH(PlayerCar3, 3000)
					SET_VEHICLE_STRONG(PlayerCar3, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(CARBONIZZARE)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(CARBONIZZARE, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar3, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTSTRING("THIS IS NOT A REPLAY") PRINTNL()	
		
		//Flags
		iControlFlag 					= 0
		iCutsceneStage 					= 0
		MissionStageBeingSkippedTo 		= FALSE	
		missionStage 					= STAGE_OPENING_CUTSCENE			
			
	ELSE
		
		PRINTSTRING("REPLAY IS IN PROGRESS") PRINTNL()
		
		//Set everything up to initialise mission
		//Sort out groups
	  	ADD_RELATIONSHIP_GROUP("SecGuards", SecGuardGroup)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, SecGuardGroup)
	  	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, SecGuardGroup, RELGROUPHASH_PLAYER)		
	
		//Check if player chose to shitskip
        IF g_bShitskipAccepted = TRUE
			IF GET_REPLAY_MID_MISSION_STAGE() = 0
				START_REPLAY_SETUP(PlayerCarChaseStartingCoords, fPlayerCarChaseStartingHeading)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_START_RT")
				missionStage = STAGE_CHASE_START
			ENDIF				
			IF GET_REPLAY_MID_MISSION_STAGE() = 1
				START_REPLAY_SETUP(<<-935.4413, -2928.0610, 12.9451>>, 178.4660)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_FOOT_CHASE_RT")
				missionStage = STAGE_ON_FOOT_CHASE
			ENDIF			
			IF GET_REPLAY_MID_MISSION_STAGE() = 2
				START_REPLAY_SETUP(<< -937.5466, -2968.7126, 12.9451 >>, 111.5016)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_EVADE_RT")
				missionStage = STAGE_EVADE_POLICE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 3
				START_REPLAY_SETUP(<<-920.0547, -2744.6614, 12.9322>>, 357.1374)
				SET_UP_MISSION_PASSED()
			ENDIF		
		ELSE
			IF GET_REPLAY_MID_MISSION_STAGE() = 0
				START_REPLAY_SETUP(<<-1019.5793, -484.8720, 36.0795>>, 93.7701)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_START_RT")
				missionStage = STAGE_GET_TO_CHASE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 1
				START_REPLAY_SETUP(PlayerCarChaseStartingCoords, fPlayerCarChaseStartingHeading)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_START_RT")
				missionStage = STAGE_CHASE_START
			ENDIF			
			IF GET_REPLAY_MID_MISSION_STAGE() = 2
				START_REPLAY_SETUP(<<-935.4413, -2928.0610, 12.9451>>, 178.4660)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_FOOT_CHASE_RT")
				missionStage = STAGE_ON_FOOT_CHASE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 3
				START_REPLAY_SETUP(<< -937.5466, -2968.7126, 12.9451 >>, 111.5016)
				iControlFlag = 0
				MissionStageBeingSkippedTo = TRUE
				TRIGGER_MUSIC_EVENT("TRV4_EVADE_RT")
				missionStage = STAGE_EVADE_POLICE
			ENDIF
		ENDIF
		
		//Load text 
		REQUEST_ADDITIONAL_TEXT("TREV4", MISSION_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			WAIT(0)
		ENDWHILE		
	ENDIF	
	
ENDPROC


/// PURPOSE:
///    Sets bMissionFailed to be True so that we start fading for screen to fade out
///  	Sets the fail reason string and calls the flow mission failed function.
///    This tells the replay controller to start displaying the fail reason + fading
///    (If bMissionFailed is already true, this function doesn't do anything- as we're already waiting for fail fade to finish)
/// PARAMS:
///    sFailReason - text label of the fail reason
PROC SetMissionFailed(String sFailReason)
	IF bMissionFailed = FALSE
		
		//Kill the chopper_cam()
  		IF heli_chase_cam_active
    		// turn it off
			SET_MULTIHEAD_SAFE(FALSE,TRUE)
    		SET_CAM_ACTIVE(heliChaseCam, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DISPLAY_RADAR(TRUE)
			IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CHOPPER_CAM")
				STOP_AUDIO_SCENE("SOL_3_CHOPPER_CAM")
			ENDIF
			STOP_SOUND(NewsHeliSoundID)
			//Stats for how long the player views the heli cam
			INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()

			SET_FRONTEND_RADIO_ACTIVE(TRUE)
			
			//Reset Flags
			AudioSceneStarted[2] 	= FALSE
    		heli_chase_cam_active 	= FALSE
  		ENDIF		
		
		TRIGGER_MUSIC_EVENT("TRV4_FAIL")
		
		FailReason = sFailReason
		
		CLEAR_PRINTS()
		
		KILL_ANY_CONVERSATION()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
			
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(FailReason)
		bMissionFailed = TRUE
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
//PURPOSE: Handles all j-skips and p-skips
PROC Debug_Options()

	// Press S on keyboard to pass the mission
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		//MISSION_STAT_SYSTEM_ALERT_TIME_WARP()
		Mission_Passed()
	ENDIF
	
	// Press F on keyboard to fail the mission
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		GameCanFail = TRUE
		SetMissionFailed("")
	ENDIF
	
	// Press 8 on keyboard to fail the mission
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD8)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		missionStage = STAGE_DEBUG_TEST
		iControlFlag = 0
	ENDIF	
	
	// Press J to skip through stages of the mission
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
		GameCanFail = FALSE
		
		IF jSkipReady = TRUE
			
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF		
			
			//Tells the stat system that the player is warping so invalidates the mission completion time stat.
			//MISSION_STAT_SYSTEM_ALERT_TIME_WARP()
			
			SWITCH missionStage
				
				CASE STAGE_GET_TO_CHASE
		
					iControlFlag = 0
					missionStage = STAGE_CHASE_START
					MissionStageBeingSkippedTo = TRUE						

				BREAK
				
				CASE STAGE_MAIN_CHASE	
					
//					REQUEST_WAYPOINT_RECORDING("BB_MOLLY_1")
//					
//					WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("BB_MOLLY_1")
//						WAIT(0)
//					ENDWHILE
//					
//					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//						ENDIF
//						SET_ENTITY_COORDS(PlayerCar, <<-1197.9634, -3030.3376, 12.9444>>)
//						SET_ENTITY_HEADING(PlayerCar, 239.3040)
//						SET_VEHICLE_ENGINE_ON(PlayerCar, TRUE, TRUE)
//						SET_VEHICLE_FORWARD_SPEED(PlayerCar, 40)
//					ENDIF
//					
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
//							PRINTSTRING("Playback is going on for enemycar")
//							SET_UBER_PLAYBACK_TO_TIME_NOW(EnemyCar, 110000)
//							PRINTSTRING("*************PLAYBACK CALLED AND TIME SKIPPED***************")
//						ENDIF
//					ENDIF
//					
//					CREATE_ROAD_BLOCK()
//					
//					IF NOT IS_SCREEN_FADED_IN()
//						LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
//						DO_SCREEN_FADE_IN(0)
//					ENDIF
//					
//					ChaseStage = 1
//					iControlFlag = 0

					iControlFlag = 0
					missionStage = STAGE_ON_FOOT_CHASE
					MissionStageBeingSkippedTo = TRUE

				BREAK				
				
				CASE STAGE_ON_FOOT_CHASE

					iControlFlag = 0
					missionStage = STAGE_EVADE_POLICE
					MissionStageBeingSkippedTo = TRUE
				
				BREAK	

				CASE STAGE_EVADE_POLICE
					
					iControlFlag = 0
					SET_UP_MISSION_PASSED()
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
	
	ENDIF
	
	// Press P to skip back through stages of the mission
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
		GameCanFail = FALSE
		
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(0)
		ENDIF
		
		SWITCH missionStage
			
			CASE STAGE_GET_TO_CHASE
	
				iControlFlag = 0
				missionStage = STAGE_GET_TO_CHASE
				MissionStageBeingSkippedTo = TRUE						

			BREAK
			
			CASE STAGE_MAIN_CHASE	
			
				iControlFlag = 0
				missionStage = STAGE_GET_TO_CHASE
				MissionStageBeingSkippedTo = TRUE					
			
			BREAK				
			
			CASE STAGE_ON_FOOT_CHASE

				iControlFlag = 0
				missionStage = STAGE_CHASE_START
				MissionStageBeingSkippedTo = TRUE
			
			BREAK	

			CASE STAGE_EVADE_POLICE
				
				iControlFlag = 0
				missionStage = STAGE_ON_FOOT_CHASE
				MissionStageBeingSkippedTo = TRUE
				
			BREAK
				
		ENDSWITCH
			
	ENDIF
	
ENDPROC
#ENDIF

//PURPOSE: adds particle effects to any jet vehicle to make the engines appear to be on.
PROC ATTACH_ENGINE_PTFX_TO_JET(VEHICLE_INDEX &veh)
      IF NOT IS_ENTITY_DEAD(veh)
            VECTOR vFarLeftEngine   = << 22.6057, -2.10018, -6.22841 >>
            VECTOR vNearLeftEngine = << 12.6349, 7.89711, -7.09742 >>
            VECTOR vFarRightEngine = << -22.5879, -2.2931, -6.22542 >>
            VECTOR vNearRightEngine = << -12.6675, 8.15519, -6.98833 >>
            VECTOR vRot = <<180.0, 0.0, 0.0 >>

            START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_jet_engine", veh, vFarLeftEngine, vRot)       
            START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_jet_engine", veh, vNearLeftEngine, vRot)       
            START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_jet_engine", veh, vFarRightEngine, vRot)       
			START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_jet_engine", veh, vNearRightEngine, vRot)       
      ENDIF
ENDPROC


//PURPOSE: adds particle effect to end jet only. To seem like the engines have just been turned on.
PROC ATTACH_ENGINE_PTFX_TO_END_JET(VEHICLE_INDEX &veh)
      IF NOT IS_ENTITY_DEAD(veh)
            VECTOR vFarLeftEngine   = << 22.6057, -2.10018, -6.22841 >>
            VECTOR vNearLeftEngine = << 12.6349, 7.89711, -7.09742 >>
            VECTOR vFarRightEngine = << -22.5879, -2.2931, -6.22542 >>
            VECTOR vNearRightEngine = << -12.6675, 8.15519, -6.98833 >>
            VECTOR vRot = <<180.0, 0.0, 0.0 >>

            FarLeftEngine = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_engine_heathaze", veh, vFarLeftEngine, vRot)       
            NearLeftEngine = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_engine_heathaze", veh, vNearLeftEngine, vRot)       
            FarRightEngine = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_engine_heathaze", veh, vFarRightEngine, vRot)       
			NearRightEngine = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_engine_heathaze", veh, vNearRightEngine, vRot)       
      ENDIF
ENDPROC


//PURPOSE: Removes particle effects for endplane. Makes it appear that the engines have been turned off.
PROC DETACH_ENGINE_PTFX_TO_JET()
	
	PRINTSTRING("engine effects being turned off now ")
	IF DOES_PARTICLE_FX_LOOPED_EXIST(FarLeftEngine)
		STOP_PARTICLE_FX_LOOPED(FarLeftEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(NearLeftEngine)
		STOP_PARTICLE_FX_LOOPED(NearLeftEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(FarRightEngine)
		STOP_PARTICLE_FX_LOOPED(FarRightEngine)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(NearRightEngine)
		STOP_PARTICLE_FX_LOOPED(NearRightEngine)
	ENDIF
		
ENDPROC

PROC BLAST_VEHICLE(VEHICLE_INDEX veh, VECTOR vBlastTo,  VECTOR vForceOff, FLOAT fMag = 1.0, BOOL bIsOffset = TRUE)
		VECTOR vWorldPos
		IF NOT IS_ENTITY_DEAD(veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(veh)
			ENDIF

			VECTOR vCarPos = GET_ENTITY_COORDS(veh)
			IF bIsOffset
				vWorldPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vBlastTo)  
			ELSE
				vWorldPos = vBlastTo
			ENDIF
			VECTOR vDir = NORMALISE_VECTOR(vWorldPos - vCarPos)
			vDir *= fMag
			APPLY_FORCE_TO_ENTITY(veh, APPLY_TYPE_EXTERNAL_IMPULSE, vDir, vForceOff, 0, false, true, true) 
		ENDIF
ENDPROC

PROC BLAST_PED(PED_INDEX ped, VECTOR vBlastTo,  VECTOR vForceOff, FLOAT fMag = 1.0, BOOL bIsOffset = TRUE)
		VECTOR vWorldPos
		IF NOT IS_ENTITY_DEAD(ped)

			VECTOR vPedPos = GET_ENTITY_COORDS(ped)
			IF bIsOffset
				vWorldPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped, vBlastTo)  
			ELSE
				vWorldPos = vBlastTo
			ENDIF
			VECTOR vDir = NORMALISE_VECTOR(vWorldPos - vPedPos)
			vDir *= fMag
//			SET_PED_TO_RAGDOLL(ped, 0, 10000, TASK_RELAX, FALSE)
			APPLY_FORCE_TO_ENTITY(ped, APPLY_TYPE_IMPULSE, vDir, vForceOff, 0, false, true, true) 
		ENDIF
ENDPROC

//#IF IS_DEBUG_BUILD
//
//	INT iWdBlastProg
//	BOOL bWdIgnoreThisForce[2], bWdForceGetOffFromCam[2], bWdUsePlayerCarForForce, bWdForceBlastCar
//	VECTOR vWdForceOffset[2], vWdForceApply[2]
//	FLOAT fWdForceMult[2]
//
//	FUNC VECTOR GET_DEBUG_CAM_OFFSET_FROM_ENTITY(VEHICLE_INDEX veh = NULL)
//		VECTOR vCamOff, vCamLoc
//		CAMERA_INDEX camDb = GET_DEBUG_CAM()
//		vCamLoc = GET_CAM_COORD(camDb)
//		IF NOT IS_ENTITY_DEAD(veh)
//			vCamOff = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, vCamLoc)
//		ENDIF
//
//		RETURN vCamOff
//	ENDFUNC
//
//	PROC DO_CAR_BLAST_WIDGET(VEHICLE_INDEX vehToBlast = NULL)
//		INT i
//		SWITCH iWdBlastProg
//			CASE 0
//				Airport_Chase_Widget_Group = START_WIDGET_GROUP("Airport Chase")
//					START_WIDGET_GROUP("Blast Vehicles")
////						SET_CURRENT_WIDGET_GROUP(Airport_Chase_Widget_Group)
//						START_WIDGET_GROUP("Force 0")						  
//							ADD_WIDGET_BOOL("Ignore this force", bWdIgnoreThisForce[0])
//							ADD_WIDGET_VECTOR_SLIDER("Force dir", vWdForceOffset[0], -100.0, 100.0, 0.5)  
//							ADD_WIDGET_BOOL("Get offset from debug cam", bWdForceGetOffFromCam[0])						 
//							ADD_WIDGET_VECTOR_SLIDER("Force apply", vWdForceApply[0], -100.0, 100.0, 0.5)
//							ADD_WIDGET_FLOAT_SLIDER("Force mult", fWdForceMult[0], 0.0, 100.0, 0.1)
//						STOP_WIDGET_GROUP()	
//						START_WIDGET_GROUP("Force 1")	
//							ADD_WIDGET_BOOL("Ignore this force", bWdIgnoreThisForce[1])																 
//							ADD_WIDGET_VECTOR_SLIDER("Force dir", vWdForceOffset[1], -100.0, 100.0, 0.5)  
//							ADD_WIDGET_BOOL("Get offset from debug cam", bWdForceGetOffFromCam[1])
//							ADD_WIDGET_VECTOR_SLIDER("Force apply", vWdForceApply[1], -100.0, 100.0, 0.5)
//							ADD_WIDGET_FLOAT_SLIDER("Force mult", fWdForceMult[1], 0.0, 100.0, 0.1)
//						STOP_WIDGET_GROUP()	
//						ADD_WIDGET_BOOL("Set player's car as blast car", bWdUsePlayerCarForForce)
//						ADD_WIDGET_BOOL("Blast car", bWdForceBlastCar)
////						CLEAR_CURRENT_WIDGET_GROUP(Airport_Chase_Widget_Group)
//					STOP_WIDGET_GROUP()
//					fWdForceMult[0] = 1.0
//					fWdForceMult[1] = 1.0
//					iWdBlastProg++
//				STOP_WIDGET_GROUP()
//			BREAK
//			
//			CASE 1
//				IF bWdUsePlayerCarForForce
//
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//
//						vehToBlast = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())				
//					ELSE
//						SCRIPT_ASSERT("Blast widget...player not in any vehicle!")
//					ENDIF
//				ENDIF
//				
//				REPEAT COUNT_OF(bWdForceGetOffFromCam) i
//					IF bWdForceGetOffFromCam[i]
//						bWdForceGetOffFromCam[i] = FALSE
//						IF NOT IS_ENTITY_DEAD(vehToBlast)
//							vWdForceOffset[i] = GET_DEBUG_CAM_OFFSET_FROM_ENTITY(vehToBlast)
//						ELSE
//							SCRIPT_ASSERT("Get offset...car is dead!")
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				
//				IF bWdForceBlastCar
//					bWdForceBlastCar = FALSE
//					IF IS_VEHICLE_DRIVEABLE(vehToBlast)
//						IF NOT bWdIgnoreThisForce[0]
//							BLAST_VEHICLE(vehToBlast, vWdForceOffset[0],  vWdForceApply[0], fWdForceMult[0])	
//						ENDIF
//						IF NOT bWdIgnoreThisForce[1]
//							BLAST_VEHICLE(vehToBlast, vWdForceOffset[1],  vWdForceApply[1], fWdForceMult[1])
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//		ENDSWITCH
//
//	ENDPROC
//
//#ENDIF

#IF IS_DEBUG_BUILD

	INT iWdBlastProg
	BOOL bWdIgnoreThisForce[2], bWdForceGetOffFromCam[2], bWdUsePlayerPedForForce, bWdForceBlastPed, bWdResetPlayerPosition, bWdStartPlayback, bWdGetDistanceBetweenPeds
	VECTOR vWdForceOffset[2], vWdForceApply[2]
	FLOAT fWdForceMult[2]

	FUNC VECTOR GET_DEBUG_CAM_OFFSET_FROM_ENTITY(PED_INDEX ped = NULL)
		VECTOR vCamOff, vCamLoc
		CAMERA_INDEX camDb = GET_DEBUG_CAM()
		vCamLoc = GET_CAM_COORD(camDb)
		IF NOT IS_ENTITY_DEAD(ped)
			vCamOff = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(ped, vCamLoc)
		ENDIF

		RETURN vCamOff
	ENDFUNC

	PROC DO_PED_BLAST_WIDGET(PED_INDEX pedToBlast = NULL)
		INT i
		SWITCH iWdBlastProg
			CASE 0
				Airport_Chase_Widget_Group = START_WIDGET_GROUP("Airport Chase")
					START_WIDGET_GROUP("Blast Peds")
						START_WIDGET_GROUP("Force 0")						  
							ADD_WIDGET_BOOL("Ignore this force", bWdIgnoreThisForce[0])
							ADD_WIDGET_VECTOR_SLIDER("Force dir", vWdForceOffset[0], -100.0, 100.0, 0.5)  
							ADD_WIDGET_BOOL("Get offset from debug cam", bWdForceGetOffFromCam[0])						 
							ADD_WIDGET_VECTOR_SLIDER("Force apply", vWdForceApply[0], -100.0, 100.0, 0.5)
							ADD_WIDGET_FLOAT_SLIDER("Force mult", fWdForceMult[0], 0.0, 100.0, 0.1)
						STOP_WIDGET_GROUP()	
						START_WIDGET_GROUP("Force 1")	
							ADD_WIDGET_BOOL("Ignore this force", bWdIgnoreThisForce[1])																 
							ADD_WIDGET_VECTOR_SLIDER("Force dir", vWdForceOffset[1], -100.0, 100.0, 0.5)  
							ADD_WIDGET_BOOL("Get offset from debug cam", bWdForceGetOffFromCam[1])
							ADD_WIDGET_VECTOR_SLIDER("Force apply", vWdForceApply[1], -100.0, 100.0, 0.5)
							ADD_WIDGET_FLOAT_SLIDER("Force mult", fWdForceMult[1], 0.0, 100.0, 0.1)
						STOP_WIDGET_GROUP()	
						ADD_WIDGET_BOOL("Set player as blast ped", bWdUsePlayerPedForForce)
						ADD_WIDGET_BOOL("Blast ped", bWdForceBlastPed)
						ADD_WIDGET_BOOL("Reset Player position", bWdResetPlayerPosition)
						ADD_WIDGET_BOOL("Start playback on ped", bWdStartPlayback)
						ADD_WIDGET_BOOL("Display Distance Between Peds", bWdGetDistanceBetweenPeds)
						
//						CLEAR_CURRENT_WIDGET_GROUP(Airport_Chase_Widget_Group)
					STOP_WIDGET_GROUP()
					fWdForceMult[0] = 1.0
					fWdForceMult[1] = 1.0
					iWdBlastProg++
				STOP_WIDGET_GROUP()
			BREAK
			
			CASE 1
				IF bWdUsePlayerPedForForce
					pedToBlast = PLAYER_PED_ID()			
				ENDIF
				
				REPEAT COUNT_OF(bWdForceGetOffFromCam) i
					IF bWdForceGetOffFromCam[i]
						bWdForceGetOffFromCam[i] = FALSE
						IF NOT IS_ENTITY_DEAD(pedToBlast)
							vWdForceOffset[i] = GET_DEBUG_CAM_OFFSET_FROM_ENTITY(pedToBlast)
						ELSE
							SCRIPT_ASSERT("Get offset...ped is dead!")
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF bWdForceBlastPed
					bWdForceBlastPed = FALSE
					bWdGetDistanceBetweenPeds = FALSE
					IF NOT IS_PED_INJURED(pedToBlast)
						IF NOT bWdIgnoreThisForce[0]
							BLAST_PED(pedToBlast, vWdForceOffset[0],  vWdForceApply[0], fWdForceMult[0])	
						ENDIF
						IF NOT bWdIgnoreThisForce[1]
							BLAST_PED(pedToBlast, vWdForceOffset[1],  vWdForceApply[1], fWdForceMult[1])
						ENDIF
					ENDIF
				ENDIF
				
				IF bWdResetPlayerPosition
					bWdResetPlayerPosition = FALSE
					IF NOT IS_PED_INJURED(pedToBlast)
						CLEAR_PED_TASKS_IMMEDIATELY(pedToBlast)
						SET_TIME_SCALE(1.0)
						SET_ENTITY_COORDS(pedToBlast, << -935.0500, -2969.3247, 12.9450 >>)
						SET_ENTITY_HEADING(pedToBlast, 349.5287)
					ENDIF
					IF NOT IS_PED_INJURED(AirWorker)
						CLEAR_PED_TASKS_IMMEDIATELY(AirWorker)
						SET_ENTITY_COORDS(AirWorker, << -934.0022, -2958.5308, 13.0631 >>)
						SET_ENTITY_HEADING(AirWorker, 168.3365)
					ENDIF
					
				ENDIF 
				
				IF bWdStartPlayback
					bWdStartPlayback = FALSE
					SET_TIME_SCALE(0.3)
					IF NOT IS_PED_INJURED(AirWorker)
						CLEAR_PED_TASKS(AirWorker)
						TASK_FOLLOW_NAV_MESH_TO_COORD(AirWorker, <<-936, -2970, 14>>, PEDMOVE_RUN)
					ENDIF
					IF NOT IS_PED_INJURED(pedToBlast)
						CLEAR_PED_TASKS(pedToBlast)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedToBlast, AirWorker, <<0.166, 2.715, -0.290>>, PEDMOVE_RUN)
					ENDIF
				ENDIF
				
				IF bWdGetDistanceBetweenPeds
					PlayerCoords = GET_ENTITY_COORDS(pedToBlast)
				 	AirWorkerCoords = GET_ENTITY_COORDS(AirWorker)
					
//					PRINTSTRING("Distance Between Peds is ") PRINTFLOAT(GET_DISTANCE_BETWEEN_COORDS(PlayerCoords, AirWorkerCoords)) PRINTNL()
				ENDIF
				
			BREAK
		ENDSWITCH

	ENDPROC

#ENDIF
	
#IF IS_DEBUG_BUILD
	PROC DO_DEBUG_CAR_NAMES()
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[0])
			TEXT_LABEL tDebugName = "CarID 0"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[0], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[1])
			TEXT_LABEL tDebugName = "CarID 1"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[1], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[2])
			TEXT_LABEL tDebugName = "CarID 2"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[2], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[3])
			TEXT_LABEL tDebugName = "CarID 3"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[3], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[4])
			TEXT_LABEL tDebugName = "CarID 4"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[4], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[5])
			TEXT_LABEL tDebugName = "CarID 5"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[5], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[6])
			TEXT_LABEL tDebugName = "CarID 6"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[6], tDebugName)
		ENDIF		
		IF DOES_ENTITY_EXIST(SetPieceCarID[7])
			TEXT_LABEL tDebugName = "CarID 7"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[7], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[8])
			TEXT_LABEL tDebugName = "CarID 8"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[8], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[9])
			TEXT_LABEL tDebugName = "CarID 9"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[9], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[10])
			TEXT_LABEL tDebugName = "CarID 10"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[10], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[11])
			TEXT_LABEL tDebugName = "CarID 11"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[11], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[12])
			TEXT_LABEL tDebugName = "CarID 12"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[12], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[13])
			TEXT_LABEL tDebugName = "CarID 13"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[13], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[14])
			TEXT_LABEL tDebugName = "CarID 14"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[14], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[15])
			TEXT_LABEL tDebugName = "CarID 15"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[15], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[16])
			TEXT_LABEL tDebugName = "CarID 16"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[16], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[17])
			TEXT_LABEL tDebugName = "CarID 17"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[17], tDebugName)
		ENDIF
//		IF DOES_ENTITY_EXIST(SetPieceCarID[18])
//			TEXT_LABEL tDebugName = "CarID 18"
//			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[18], tDebugName)
//		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[19])
			TEXT_LABEL tDebugName = "CarID 19"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[19], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[20])
			TEXT_LABEL tDebugName = "CarID 20"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[20], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[21])
			TEXT_LABEL tDebugName = "CarID 21"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[21], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(SetPieceCarID[22])
			TEXT_LABEL tDebugName = "CarID 22"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[22], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[23])
			TEXT_LABEL tDebugName = "CarID 23"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[23], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[24])
			TEXT_LABEL tDebugName = "CarID 24"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[24], tDebugName)
		ENDIF	
		IF DOES_ENTITY_EXIST(SetPieceCarID[25])
			TEXT_LABEL tDebugName = "CarID 25"
			SET_VEHICLE_NAME_DEBUG(SetPieceCarID[25], tDebugName)
		ENDIF	
		
	ENDPROC
#ENDIF

PROC ADD_PARTICLE_FX_TO_PLANES_ENGINES()
		
//		//Plane 1
//		IF enginefx1done = FALSE
//			IF DOES_ENTITY_EXIST(SetPieceCarID[2])
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[2])
//					ATTACH_ENGINE_PTFX_TO_JET(SetPieceCarID[2])
//					enginefx1done = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//Plane 2
//		IF enginefx2done = FALSE
//			IF DOES_ENTITY_EXIST(SetPieceCarID[5])
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[5])
//					ATTACH_ENGINE_PTFX_TO_JET(SetPieceCarID[5])
//					enginefx2done = TRUE
//				ENDIF
//			ENDIF
//		ENDIF		
//		
//		//Plane 3
//		IF enginefx3done = FALSE
//			IF DOES_ENTITY_EXIST(SetPiecePlane[0])
//				IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[0])
//					ATTACH_ENGINE_PTFX_TO_JET(SetPiecePlane[0])
//					enginefx3done = TRUE
//				ENDIF
//			ENDIF
//		ENDIF		
//		
//		//Plane 4
//		IF enginefx4done = FALSE
//			IF DOES_ENTITY_EXIST(SetPiecePlane[1])
//				IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[1])
//					ATTACH_ENGINE_PTFX_TO_JET(SetPiecePlane[1])
//					enginefx4done = TRUE
//				ENDIF
//			ENDIF
//		ENDIF	
				
		//Plane 5
		IF enginefx5done = FALSE
			IF DOES_ENTITY_EXIST(EndPlane)
				IF IS_VEHICLE_DRIVEABLE(EndPlane)
					PRINTSTRING("ATTACH_ENGINE_PTFX_TO_END_JET is being called") PRINTNL()
					ATTACH_ENGINE_PTFX_TO_END_JET(EndPlane)
					enginefx5done = TRUE
				ENDIF
			ENDIF
		ENDIF
		
ENDPROC

//PURPOSE: Releases sound id's at appropriate times
PROC RELEASE_SOUNDS()

		//Release PropPlaneSoundID
		IF PropPlaneSoundID <> -1
			IF PropPlaneSoundIDPlayed = TRUE
				IF propPlaneSoundReleased = FALSE
					RELEASE_SOUND_ID(PropPlaneSoundID)
					PropPlaneSoundIDCreated = FALSE
					propPlaneSoundReleased = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Release CarExplodeSoundID1
		IF CarExplodeSoundID1 <> -1
			IF CarExplodeSoundID1Played = TRUE
				IF CarExplodeSoundID1Released = FALSE
					RELEASE_SOUND_ID(CarExplodeSoundID1)
					CarExplodeSoundID1Created = FALSE
					CarExplodeSoundID1Released = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Release CarExplodeSoundID2
		IF CarExplodeSoundID2 <> -1
			IF CarExplodeSoundID2Played = TRUE
				IF CarExplodeSoundID2Released = FALSE
					RELEASE_SOUND_ID(CarExplodeSoundID2)
					CarExplodeSoundID2Created = FALSE
					CarExplodeSoundID2Released = TRUE
				ENDIF
			ENDIF	
		ENDIF
		
		//Release CarExplodeSoundID3
		IF CarExplodeSoundID3 <> -1
			IF CarExplodeSoundID3Played = TRUE
				IF CarExplodeSoundID3Released = FALSE
					RELEASE_SOUND_ID(CarExplodeSoundID3)
					CarExplodeSoundID3Created = FALSE
					CarExplodeSoundID3Released = TRUE
				ENDIF		
			ENDIF
		ENDIF
		
		//Release CarExplodeSoundID4
		IF CarExplodeSoundID4 <> -1
			IF CarExplodeSoundID4Played = TRUE
				IF CarExplodeSoundID4Released = FALSE
					RELEASE_SOUND_ID(CarExplodeSoundID4)
					CarExplodeSoundID4Created = FALSE
					CarExplodeSoundID4Released = TRUE
				ENDIF	
			ENDIF
		ENDIF

ENDPROC


//PURPOSE: This sets up and controls the AI police cars that will chase after the player
PROC AI_COP_CAR_CONTROLLER()

	IF AIPoliceSetup = FALSE
		REQUEST_MODEL(POLICE3)
		REQUEST_MODEL(S_M_Y_COP_01)
		REQUEST_VEHICLE_RECORDING(1,"BB_AIChase")
//		REQUEST_VEHICLE_RECORDING(2,"BB_AIChase")
		
		IF HAS_MODEL_LOADED(POLICE3)
		AND HAS_MODEL_LOADED(S_M_Y_COP_01)
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 15500
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())						
						
						AIPoliceCar[0] = CREATE_VEHICLE(POLICE3, << -847.2593, -2379.9868, 15.9304 >>, 196.4921)
						AIPoliceCop[0] = CREATE_PED_INSIDE_VEHICLE(AIPoliceCar[0], PEDTYPE_COP, S_M_Y_COP_01)
//						AIPoliceCar[1] = CREATE_VEHICLE(POLICE3, <<-1029.8545, -3026.2664, 12.9444>>, 34.6780)
//						AIPoliceCop[1] = CREATE_PED_INSIDE_VEHICLE(AIPoliceCar[1], PEDTYPE_COP, S_M_Y_COP_01)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AIPoliceCop[0], TRUE)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AIPoliceCop[1], TRUE)
						
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(AIPoliceCar[0], "SOL_3_POLICE_CARS_Group")
//						ADD_ENTITY_TO_AUDIO_MIX_GROUP(AIPoliceCar[1], "SOL_3_POLICE_CARS_Group")
						
						INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(AIPoliceCop[0])
//						INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(AIPoliceCop[1])
						
						SET_VEHICLE_ENGINE_ON(AIPoliceCar[0], TRUE, TRUE)
//						SET_VEHICLE_ENGINE_ON(AIPoliceCar[1], TRUE, TRUE)
						
//						SET_VEHICLE_FORWARD_SPEED(AIPoliceCar[0], 5)
//						SET_VEHICLE_FORWARD_SPEED(AIPoliceCar[1], 5)
						
						START_PLAYBACK_RECORDED_VEHICLE(AIPoliceCar[0], 1, "BB_AIChase")
//						START_PLAYBACK_RECORDED_VEHICLE(AIPoliceCar[1], 2, "BB_AIChase")
						
						GIVE_WEAPON_TO_PED(AIPoliceCop[0], WEAPONTYPE_PISTOL, 1000, TRUE)
//						GIVE_WEAPON_TO_PED(AIPoliceCop[1], WEAPONTYPE_PISTOL, 1000, TRUE)
						
						SET_VEHICLE_SIREN(AIPoliceCar[0], TRUE)
//						SET_VEHICLE_SIREN(AIPoliceCar[1], TRUE)

//						TASK_COMBAT_PED(AIPoliceCop[0], PLAYER_PED_ID())
//						TASK_COMBAT_PED(AIPoliceCop[1], PLAYER_PED_ID())
//						TASK_VEHICLE_CHASE(AIPoliceCop[0], PLAYER_PED_ID())
//						TASK_VEHICLE_CHASE(AIPoliceCop[1], PLAYER_PED_ID())
//						TASK_VEHICLE_MISSION_PED_TARGET(AIPoliceCop[0], AIPoliceCar[0], PLAYER_PED_ID(), MISSION_ATTACK, 40, DRIVINGMODE_PLOUGHTHROUGH, -1, 5)
//						TASK_VEHICLE_MISSION_PED_TARGET(AIPoliceCop[1], AIPoliceCar[1], PLAYER_PED_ID(), MISSION_ATTACK, 40, DRIVINGMODE_PLOUGHTHROUGH, -1, 5)
						
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_VEHICLE_MISSION_COORS_TARGET(NULL, AIPoliceCar[0], <<-875.8, -2627.4, 13.4>>, MISSION_GOTO, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 10, 10)
//							TASK_VEHICLE_MISSION(NULL, AIPoliceCar[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 1.0, 1.0)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(AIPoliceCop[0], seq)
//						CLEAR_SEQUENCE_TASK(seq)
//						
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_VEHICLE_MISSION_COORS_TARGET(NULL, AIPoliceCar[1], <<-875.8, -2627.4, 13.4>>, MISSION_GOTO, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 10, 10)
//							TASK_VEHICLE_MISSION(NULL, AIPoliceCar[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 1.0, 1.0)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(AIPoliceCop[1], seq)
//						CLEAR_SEQUENCE_TASK(seq)
						
//						TASK_VEHICLE_MISSION(AIPoliceCop[0], AIPoliceCar[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 1.0, 1.0)
//						TASK_VEHICLE_MISSION(AIPoliceCop[1], AIPoliceCar[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 1.0, 1.0)
						
//						SET_PED_KEEP_TASK(AIPoliceCop[0], TRUE)
//						SET_PED_KEEP_TASK(AIPoliceCop[1], TRUE)
						
//						SET_PED_AS_NO_LONGER_NEEDED(AIPoliceCop[0])
//						SET_PED_AS_NO_LONGER_NEEDED(AIPoliceCop[1])
//						SET_VEHICLE_AS_NO_LONGER_NEEDED(AIPoliceCar[0])
//						SET_VEHICLE_AS_NO_LONGER_NEEDED(AIPoliceCar[1])
						SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
						AIPoliceSetup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF AIPoliceSetup = TRUE
		IF AIChaseTaskGiven[0] = FALSE
			IF DOES_ENTITY_EXIST(AIPoliceCar[0])
				IF IS_VEHICLE_DRIVEABLE(AIPoliceCar[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(AIPoliceCar[0])
						SET_PLAYBACK_SPEED(AIPoliceCar[0], fPlaybackSpeed)
//						PRINTSTRING("play back position for AIPoliceCar[0] is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(AIPoliceCar[0])) PRINTNL()
						IF GET_TIME_POSITION_IN_RECORDING(AIPoliceCar[0]) > 13000
							STOP_PLAYBACK_RECORDED_VEHICLE(AIPoliceCar[0])
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(AIPoliceCop[0])
							IF NOT IS_PED_INJURED(AIPoliceCop[0])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AIPoliceCop[0], TRUE)
		//						TASK_VEHICLE_MISSION(AIPoliceCop[0], AIPoliceCar[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 50, DRIVINGMODE_AVOIDCARS_RECKLESS, 1.0, 1.0)
		//						TASK_COMBAT_PED(AIPoliceCop[0], PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									TASK_VEHICLE_CHASE(AIPoliceCop[0], PLAYER_PED_ID())
									SET_PED_KEEP_TASK(AIPoliceCop[0], TRUE)
								ELSE
									TASK_COMBAT_PED(AIPoliceCop[0], PLAYER_PED_ID())
									SET_PED_KEEP_TASK(AIPoliceCop[0], TRUE)
								ENDIF
								REMOVE_VEHICLE_RECORDING(1,"BB_AIChase")
								vehicleChaseTimer = GET_GAME_TIMER()
								AIChaseTaskGiven[0] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF AIChaseBehavioursSet[0] = FALSE
			IF AIChaseTaskGiven[0] = TRUE
				IF GET_GAME_TIMER() > (vehicleChaseTimer + 500)
					IF DOES_ENTITY_EXIST(AIPoliceCop[0])
						IF NOT IS_PED_INJURED(AIPoliceCop[0])
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(AIPoliceCop[0], VEHICLE_CHASE_CANT_SPIN_OUT, TRUE)
								SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(AIPoliceCop[0], VEHICLE_CHASE_CANT_BLOCK, TRUE)
								AIChaseBehavioursSet[0] = TRUE
							ELSE
								AIChaseBehavioursSet[0] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
//		IF AIChaseTaskGiven[1] = FALSE
//			IF DOES_ENTITY_EXIST(AIPoliceCar[1])
//				IF IS_VEHICLE_DRIVEABLE(AIPoliceCar[1])
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 36000
//								SET_VEHICLE_FORWARD_SPEED(AIPoliceCar[1], 25)
//								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AIPoliceCop[1], FALSE)
////								TASK_VEHICLE_MISSION(AIPoliceCop[1], AIPoliceCar[1], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), MISSION_ATTACK, 50, DRIVINGMODE_PLOUGHTHROUGH, 1.0, 1.0)
//								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//									TASK_VEHICLE_CHASE(AIPoliceCop[1], PLAYER_PED_ID())
//									SET_PED_KEEP_TASK(AIPoliceCop[1], TRUE)
//								ELSE
//									TASK_COMBAT_PED(AIPoliceCop[1], PLAYER_PED_ID())
//									SET_PED_KEEP_TASK(AIPoliceCop[1], TRUE)
//								ENDIF
//								vehicleChaseTimer2 = GET_GAME_TIMER()
//								AIChaseTaskGiven[1] = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		IF AIChaseBehavioursSet[1] = FALSE
//			IF AIChaseTaskGiven[1] = TRUE
//				IF GET_GAME_TIMER() > (vehicleChaseTimer2 + 2000)
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(AIPoliceCop[1], VEHICLE_CHASE_CANT_SPIN_OUT, TRUE)
//						SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(AIPoliceCop[1], VEHICLE_CHASE_CANT_BLOCK, TRUE)
//						AIChaseBehavioursSet[1] = TRUE
//					ELSE
//						AIChaseBehavioursSet[1] = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
	ENDIF	
	
//	ELSE
//		IF AIPoliceTasksGiven = FALSE
//			IF NOT IS_PED_INJURED(AIPoliceCop[0])
//				IF IS_VEHICLE_DRIVEABLE(AIPoliceCar[0])
//					TASK_VEHICLE_MISSION_PED_TARGET(AIPoliceCop[0], AIPoliceCar[0], PLAYER_PED_ID(), MISSION_RAM, 35, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
//					SET_PED_KEEP_TASK(AIPoliceCop[0], TRUE)
//				ENDIF
//			ENDIF
//			IF NOT IS_PED_INJURED(AIPoliceCop[1])
//				IF IS_VEHICLE_DRIVEABLE(AIPoliceCar[1])
//					TASK_VEHICLE_MISSION_PED_TARGET(AIPoliceCop[1], AIPoliceCar[1], PLAYER_PED_ID(), MISSION_RAM, 35, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
//					SET_PED_KEEP_TASK(AIPoliceCop[1], TRUE)
//				ENDIF
//			ENDIF	
//			AIPoliceTasksGiven = TRUE
////		ENDIF
//	ENDIF

ENDPROC


PROC CREATE_AMBIENT_FIRE_SCENE()
		
	IF FiremenSetup = FALSE	
		IF DOES_ENTITY_EXIST(EnemyCar)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
	//			PRINTSTRING("Playback position for EnemyCar is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(EnemyCar)) PRINTNL()
				IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 55000
					
					REQUEST_MODEL(FIRETRUK)
					REQUEST_MODEL(S_M_Y_FIREMAN_01)
					REQUEST_ANIM_DICT("missheist_agency3aig_lift_waitped_a")
					
					//set up firetruck
					IF HAS_MODEL_LOADED(FIRETRUK)
					AND HAS_MODEL_LOADED(S_M_Y_FIREMAN_01)
					AND HAS_ANIM_DICT_LOADED("missheist_agency3aig_lift_waitped_a")
						IF NOT DOES_ENTITY_EXIST(FireEngine)
							CLEAR_AREA(<< -1129.5664, -2319.8877, 12.9445 >>, 10, FALSE)
							FireEngine = CREATE_VEHICLE(FIRETRUK, << -1129.5664, -2319.8877, 12.9445 >>, 16.5423)
						ENDIF
					
						C4[0] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev4_trailer_fire",<<-1151, -2311, 15>>,<<0,0,0>>)
						IF loudFireSoundIDPlayed = FALSE
							PLAY_SOUND_FROM_COORD(loudFireSoundID, "Trevor_4_747_Loud_Fire", <<-1151, -2311, 15>>)
							loudFireSoundIDPlayed = TRUE
						ENDIF
						C4[1] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev4_trailer_fire",<<-1140, -2328, 18>>,<<0,0,0>>)
						C4[2] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_trev4_trailer_fire",<<-1150, -2327, 19>>,<<0,0,0>>)
					
						//Set up firemen
						IF NOT DOES_ENTITY_EXIST(Fireman[0])
							Fireman[0] = CREATE_PED_INSIDE_VEHICLE(FireEngine, PEDTYPE_FIRE, S_M_Y_FIREMAN_01)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[0], TRUE)
							SET_PED_KEEP_TASK(Fireman[0], TRUE)
							TASK_VEHICLE_SHOOT_AT_COORD(Fireman[0], <<-1147.07, -2320.65, 18.93>>)
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(Fireman[1])
							Fireman[1] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, << -1138.3129, -2313.2773, 12.9445 >>, 155.4271)
							TASK_PLAY_ANIM(Fireman[1], "missheist_agency3aig_lift_waitped_a", "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[1], TRUE)
							SET_PED_KEEP_TASK(Fireman[1], TRUE)
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(Fireman[2])
							Fireman[2] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, << -1145.3617, -2309.3870, 12.9445 >>, 96.0786)
							TASK_PLAY_ANIM(Fireman[2], "missheist_agency3aig_lift_waitped_a", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[2], TRUE)
							SET_PED_KEEP_TASK(Fireman[2], TRUE)
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(Fireman[3])
							Fireman[3] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, << -1147.4609, -2314.5107, 13.1518 >>, 57.8707)
							TASK_PLAY_ANIM(Fireman[3], "missheist_agency3aig_lift_waitped_a", "idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[3], TRUE)
							SET_PED_KEEP_TASK(Fireman[3], TRUE)
						ENDIF				
					
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_FIREMAN_01)
						SET_MODEL_AS_NO_LONGER_NEEDED(FIRETRUK)
						
						FiremenSetup = TRUE
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF FiremenSetup = TRUE
		IF firemanCowerTaskSet[0] = FALSE
			IF DOES_ENTITY_EXIST(Fireman[1])
				IF NOT IS_PED_INJURED(Fireman[1])
					IF FirstAnimStarted[0] = FALSE
						IF IS_ENTITY_PLAYING_ANIM(Fireman[1], "missheist_agency3aig_lift_waitped_a", "idle_b")
							FirstAnimStarted[0] = TRUE
						ENDIF
					ENDIF
					IF FirstAnimStarted[0] = TRUE
						IF NOT IS_ENTITY_PLAYING_ANIM(Fireman[1], "missheist_agency3aig_lift_waitped_a", "idle_b")
							TASK_COWER(Fireman[1], -1)
							SET_PED_KEEP_TASK(Fireman[1], TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[1], FALSE)
							firemanCowerTaskSet[0] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF firemanCowerTaskSet[1] = FALSE
			IF DOES_ENTITY_EXIST(Fireman[2])
				IF NOT IS_PED_INJURED(Fireman[2])
					IF FirstAnimStarted[1] = FALSE
						IF IS_ENTITY_PLAYING_ANIM(Fireman[2], "missheist_agency3aig_lift_waitped_a", "idle_a")
							FirstAnimStarted[1] = TRUE
						ENDIF
					ENDIF
					IF FirstAnimStarted[1] = TRUE
						IF NOT IS_ENTITY_PLAYING_ANIM(Fireman[2], "missheist_agency3aig_lift_waitped_a", "idle_a")
							TASK_COWER(Fireman[2], -1)
							SET_PED_KEEP_TASK(Fireman[2], TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[2], FALSE)
							firemanCowerTaskSet[1] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF firemanCowerTaskSet[2] = FALSE
			IF DOES_ENTITY_EXIST(Fireman[3])
				IF NOT IS_PED_INJURED(Fireman[3])
					IF FirstAnimStarted[2] = FALSE
						IF IS_ENTITY_PLAYING_ANIM(Fireman[3], "missheist_agency3aig_lift_waitped_a", "idle_c")
							FirstAnimStarted[2] = TRUE
						ENDIF
					ENDIF
					IF FirstAnimStarted[2] = TRUE
						IF NOT IS_ENTITY_PLAYING_ANIM(Fireman[3], "missheist_agency3aig_lift_waitped_a", "idle_c")
							TASK_COWER(Fireman[3], -1)
							SET_PED_KEEP_TASK(Fireman[3], TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Fireman[3], FALSE)
							firemanCowerTaskSet[2] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC


PROC TURN_POLICE_SIRENS_ON()

	//Fire engines
//	IF DOES_ENTITY_EXIST(SetPieceCarID[18])
//		IF NOT IS_ENTITY_DEAD(SetPieceCarID[18])
//			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[18])
//				SET_VEHICLE_SIREN(SetPieceCarID[18], TRUE)
//			ENDIF
//		ENDIF
//	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[19])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[19])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[19])
				SET_VEHICLE_SIREN(SetPieceCarID[19], TRUE)
			ENDIF
		ENDIF
	ENDIF

	
	//Police cars
	IF DOES_ENTITY_EXIST(SetPieceCarID[7])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[7])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[7])
				SET_VEHICLE_SIREN(SetPieceCarID[7], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[8])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[8])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[8])
				SET_VEHICLE_SIREN(SetPieceCarID[8], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[9])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[9])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[9])
				SET_VEHICLE_SIREN(SetPieceCarID[9], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[10])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[10])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[10])
				SET_VEHICLE_SIREN(SetPieceCarID[10], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[11])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[11])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[11])
				SET_VEHICLE_SIREN(SetPieceCarID[11], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[13])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[13])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[13])
				SET_VEHICLE_SIREN(SetPieceCarID[13], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[14])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[14])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[14])
				SET_VEHICLE_SIREN(SetPieceCarID[14], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[15])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[15])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[15])
				SET_VEHICLE_SIREN(SetPieceCarID[15], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[16])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[16])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[16])
				SET_VEHICLE_SIREN(SetPieceCarID[16], TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[17])
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[17])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[17])
				SET_VEHICLE_SIREN(SetPieceCarID[17], TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC

//PROC BLAST_PLAYERCAR_IF_TOO_CLOSE_TO_ENGINES(VEHICLE_INDEX Plane)
//
//	PRINTSTRING("**********Blast any vehicle proc is running********") PRINTNL()
//	
//	BOOL FrontOfCarIsClosest[4], BackOfCarIsClosest[4], LeftOfCarIsClosest[4], RightOfCarIsClosest[4]
//	
//	VECTOR VForceOffFront = <<0,5,0>>
//	VECTOR VForceOffBack = <<0,-5,0>>
//	VECTOR VForceOffLeft = <<-1,0,0>>
//	VECTOR VForceOffRight = <<1,0,0>>
//	
//	FLOAT fMagFront = 3.0
//	FLOAT fMagBack = 3.0
//	FLOAT fMagLeft = 2.0
//	FLOAT fMagRight = 2.0
//	
//	
//	IF IS_VEHICLE_DRIVEABLE(Plane)
//		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//			
//			//Get coords for where blast should come from from each engine.
//			//Engine 1 coords
//			VECTOR Engine1Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<21.165, -5.634, -6.935>>)
//			//Engine 2 coords
//			VECTOR Engine2Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<12.007, 2.85, -7.62>>)
//			//Engine 3 coords
//			VECTOR Engine3Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<-11.92, 3.16, -7.49>>)
//			//Engine 4 coords
//			VECTOR Engine4Coords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<-21.260, -5.961, -6.81>>)
//			
//			//Get coords where air resistance should be applied to warn player is too close.
//			//Engine 1 coords
//			VECTOR Engine1WarningCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<20.881, -21.593, -6.893>>)
//			//Engine 2 coords
//			VECTOR Engine2WarningCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<12.710, -19.717, -7.024>>)
//			//Engine 3 coords
//			VECTOR Engine3WarningCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<-11.131, -19.678, -6.999>>)
//			//Engine 4 coords
//			VECTOR Engine4WarningCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<-20.832, -25.608, -6.547>>)
//			
//			
//			//Coords for where to apply the force to
//			//Engine 1
//			VECTOR vBlastTo1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<20.197, -72.552, -6.227>>)
//			//Engine 2
//			VECTOR vBlastTo2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<12.568, -71.357, -6.927>>)
//			//Engine 3
//			VECTOR vBlastTo3 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<-6.987, -68.960, -6.951>>)
//			//Engine 4
//			VECTOR vBlastTo4 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Plane, <<-17.945, -67.619, -6.967>>)
//			
//			//Determine which side of the car is closest to the engine
//			//Front of car
//			VECTOR FrontOfCarCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<0.021, 1.43, 2.55>>)
//			//Back of car
//			VECTOR BackOfCarCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<0.003, -1.179, 2.5>>)
//			//Left of car
//			VECTOR LeftOfCarCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<-0.399, -0.273, 2.51>>)
//			//Right of car
//			VECTOR RightOfCarCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PlayerCar, <<0.287, -0.278, 2.51>>)
//			
//			//For Engine 1
//			FLOAT fFrontCarDistanceToEngine1 = GET_DISTANCE_BETWEEN_COORDS(FrontOfCarCoords, Engine1Coords)
//			FLOAT fBackCarDistanceToEngine1 = GET_DISTANCE_BETWEEN_COORDS(BackOfCarCoords, Engine1Coords)
//			FLOAT fLeftCarDistanceToEngine1 = GET_DISTANCE_BETWEEN_COORDS(LeftOfCarCoords, Engine1Coords)
//			FLOAT fRightCarDistanceToEngine1 = GET_DISTANCE_BETWEEN_COORDS(RightOfCarCoords, Engine1Coords)
//			
//			//For Engine 2
//			FLOAT fFrontCarDistanceToEngine2 = GET_DISTANCE_BETWEEN_COORDS(FrontOfCarCoords, Engine2Coords)
//			FLOAT fBackCarDistanceToEngine2 = GET_DISTANCE_BETWEEN_COORDS(BackOfCarCoords, Engine2Coords)
//			FLOAT fLeftCarDistanceToEngine2 = GET_DISTANCE_BETWEEN_COORDS(LeftOfCarCoords, Engine2Coords)
//			FLOAT fRightCarDistanceToEngine2 = GET_DISTANCE_BETWEEN_COORDS(RightOfCarCoords, Engine2Coords)
//			
//			//For Engine 3
//			FLOAT fFrontCarDistanceToEngine3 = GET_DISTANCE_BETWEEN_COORDS(FrontOfCarCoords, Engine3Coords)
//			FLOAT fBackCarDistanceToEngine3 = GET_DISTANCE_BETWEEN_COORDS(BackOfCarCoords, Engine3Coords)
//			FLOAT fLeftCarDistanceToEngine3 = GET_DISTANCE_BETWEEN_COORDS(LeftOfCarCoords, Engine3Coords)
//			FLOAT fRightCarDistanceToEngine3 = GET_DISTANCE_BETWEEN_COORDS(RightOfCarCoords, Engine3Coords)
//			
//			//For Engine 4
//			FLOAT fFrontCarDistanceToEngine4 = GET_DISTANCE_BETWEEN_COORDS(FrontOfCarCoords, Engine4Coords)
//			FLOAT fBackCarDistanceToEngine4 = GET_DISTANCE_BETWEEN_COORDS(BackOfCarCoords, Engine4Coords)
//			FLOAT fLeftCarDistanceToEngine4 = GET_DISTANCE_BETWEEN_COORDS(LeftOfCarCoords, Engine4Coords)
//			FLOAT fRightCarDistanceToEngine4 = GET_DISTANCE_BETWEEN_COORDS(RightOfCarCoords, Engine4Coords)
//			
//			//ENGINE 1
//			//Check to see if front of car is closest to engine1
//			IF fFrontCarDistanceToEngine1 < fBackCarDistanceToEngine1
//			AND fFrontCarDistanceToEngine1 < fLeftCarDistanceToEngine1
//			AND fFrontCarDistanceToEngine1 < fRightCarDistanceToEngine1
//				FrontOfCarIsClosest[0] = TRUE
//				BackOfCarIsClosest[0] = FALSE
//				LeftOfCarIsClosest[0] = FALSE
//				RightOfCarIsClosest[0] = FALSE
//			ENDIF
//			
//			//Check to see if back of car is closest to engine1
//			IF fBackCarDistanceToEngine1 < fFrontCarDistanceToEngine1
//			AND fBackCarDistanceToEngine1 < fLeftCarDistanceToEngine1
//			AND fBackCarDistanceToEngine1 < fRightCarDistanceToEngine1
//				BackOfCarIsClosest[0] = TRUE
//				FrontOfCarIsClosest[0] = FALSE
//				LeftOfCarIsClosest[0] = FALSE
//				RightOfCarIsClosest[0] = FALSE
//			ENDIF
//			
//			//Check to see if left of car is closest to engine1
//			IF fLeftCarDistanceToEngine1 < fBackCarDistanceToEngine1
//			AND fLeftCarDistanceToEngine1 < fFrontCarDistanceToEngine1
//			AND fLeftCarDistanceToEngine1 < fRightCarDistanceToEngine1
//				LeftOfCarIsClosest[0] = TRUE
//				BackOfCarIsClosest[0] = FALSE
//				FrontOfCarIsClosest[0] = FALSE
//				RightOfCarIsClosest[0] = FALSE
//			ENDIF
//			
//			//Check to see if right of car is closest to engine1
//			IF fRightCarDistanceToEngine1 < fBackCarDistanceToEngine1
//			AND fRightCarDistanceToEngine1 < fLeftCarDistanceToEngine1
//			AND fRightCarDistanceToEngine1 < fFrontCarDistanceToEngine1
//				RightOfCarIsClosest[0] = TRUE
//				BackOfCarIsClosest[0] = FALSE
//				LeftOfCarIsClosest[0] = FALSE
//				FrontOfCarIsClosest[0] = FALSE
//			ENDIF			
//			
//			//ENGINE 2
//			//Check to see if front of car is closest to engine2
//			IF fFrontCarDistanceToEngine2 < fBackCarDistanceToEngine2
//			AND fFrontCarDistanceToEngine2 < fLeftCarDistanceToEngine2
//			AND fFrontCarDistanceToEngine2 < fRightCarDistanceToEngine2
//				FrontOfCarIsClosest[1] = TRUE
//				BackOfCarIsClosest[1] = FALSE
//				LeftOfCarIsClosest[1] = FALSE
//				RightOfCarIsClosest[1] = FALSE
//			ENDIF
//			
//			//Check to see if back of car is closest to engine2
//			IF fBackCarDistanceToEngine2 < fFrontCarDistanceToEngine2
//			AND fBackCarDistanceToEngine2 < fLeftCarDistanceToEngine2
//			AND fBackCarDistanceToEngine2 < fRightCarDistanceToEngine2
//				BackOfCarIsClosest[1] = TRUE
//				FrontOfCarIsClosest[1] = FALSE
//				LeftOfCarIsClosest[1] = FALSE
//				RightOfCarIsClosest[1] = FALSE
//			ENDIF
//			
//			//Check to see if left of car is closest to engine2
//			IF fLeftCarDistanceToEngine2 < fBackCarDistanceToEngine2
//			AND fLeftCarDistanceToEngine2 < fFrontCarDistanceToEngine2
//			AND fLeftCarDistanceToEngine2 < fRightCarDistanceToEngine2
//				LeftOfCarIsClosest[1] = TRUE
//				BackOfCarIsClosest[1] = FALSE
//				FrontOfCarIsClosest[1] = FALSE
//				RightOfCarIsClosest[1] = FALSE
//			ENDIF
//			
//			//Check to see if right of car is closest to engine2
//			IF fRightCarDistanceToEngine2 < fBackCarDistanceToEngine2
//			AND fRightCarDistanceToEngine2 < fLeftCarDistanceToEngine2
//			AND fRightCarDistanceToEngine2 < fFrontCarDistanceToEngine2
//				RightOfCarIsClosest[1] = TRUE
//				BackOfCarIsClosest[1] = FALSE
//				LeftOfCarIsClosest[1] = FALSE
//				FrontOfCarIsClosest[1] = FALSE
//			ENDIF	
//			
//			//ENGINE 3
//			//Check to see if front of car is closest to engine3
//			IF fFrontCarDistanceToEngine3 < fBackCarDistanceToEngine3
//			AND fFrontCarDistanceToEngine3 < fLeftCarDistanceToEngine3
//			AND fFrontCarDistanceToEngine3 < fRightCarDistanceToEngine3
//				FrontOfCarIsClosest[2] = TRUE
//				BackOfCarIsClosest[2] = FALSE
//				LeftOfCarIsClosest[2] = FALSE
//				RightOfCarIsClosest[2] = FALSE
//			ENDIF
//			
//			//Check to see if back of car is closest to engine3
//			IF fBackCarDistanceToEngine3 < fFrontCarDistanceToEngine3
//			AND fBackCarDistanceToEngine3 < fLeftCarDistanceToEngine3
//			AND fBackCarDistanceToEngine3 < fRightCarDistanceToEngine3
//				BackOfCarIsClosest[2] = TRUE
//				FrontOfCarIsClosest[2] = FALSE
//				LeftOfCarIsClosest[2] = FALSE
//				RightOfCarIsClosest[2] = FALSE
//			ENDIF
//			
//			//Check to see if left of car is closest to engine3
//			IF fLeftCarDistanceToEngine3 < fBackCarDistanceToEngine3
//			AND fLeftCarDistanceToEngine3 < fFrontCarDistanceToEngine3
//			AND fLeftCarDistanceToEngine3 < fRightCarDistanceToEngine3
//				LeftOfCarIsClosest[2] = TRUE
//				BackOfCarIsClosest[2] = FALSE
//				FrontOfCarIsClosest[2] = FALSE
//				RightOfCarIsClosest[2] = FALSE
//			ENDIF
//			
//			//Check to see if right of car is closest to engine3
//			IF fRightCarDistanceToEngine3 < fBackCarDistanceToEngine3
//			AND fRightCarDistanceToEngine3 < fLeftCarDistanceToEngine3
//			AND fRightCarDistanceToEngine3 < fFrontCarDistanceToEngine3
//				RightOfCarIsClosest[2] = TRUE
//				BackOfCarIsClosest[2] = FALSE
//				LeftOfCarIsClosest[2] = FALSE
//				FrontOfCarIsClosest[2] = FALSE
//			ENDIF	
//			
//			//ENGINE 4
//			//Check to see if front of car is closest to engine4
//			IF fFrontCarDistanceToEngine4 < fBackCarDistanceToEngine4
//			AND fFrontCarDistanceToEngine4 < fLeftCarDistanceToEngine4
//			AND fFrontCarDistanceToEngine4 < fRightCarDistanceToEngine4
//				FrontOfCarIsClosest[3] = TRUE
//				BackOfCarIsClosest[3] = FALSE
//				LeftOfCarIsClosest[3] = FALSE
//				RightOfCarIsClosest[3] = FALSE
//			ENDIF
//			
//			//Check to see if back of car is closest to engine4
//			IF fBackCarDistanceToEngine4 < fFrontCarDistanceToEngine4
//			AND fBackCarDistanceToEngine4 < fLeftCarDistanceToEngine4
//			AND fBackCarDistanceToEngine4 < fRightCarDistanceToEngine4
//				BackOfCarIsClosest[3] = TRUE
//				FrontOfCarIsClosest[3] = FALSE
//				LeftOfCarIsClosest[3] = FALSE
//				RightOfCarIsClosest[3] = FALSE
//			ENDIF
//			
//			//Check to see if left of car is closest to engine4
//			IF fLeftCarDistanceToEngine4 < fBackCarDistanceToEngine4
//			AND fLeftCarDistanceToEngine4 < fFrontCarDistanceToEngine4
//			AND fLeftCarDistanceToEngine4 < fRightCarDistanceToEngine4
//				LeftOfCarIsClosest[3] = TRUE
//				BackOfCarIsClosest[3] = FALSE
//				FrontOfCarIsClosest[3] = FALSE
//				RightOfCarIsClosest[3] = FALSE
//			ENDIF
//			
//			//Check to see if right of car is closest to engine1
//			IF fRightCarDistanceToEngine4 < fBackCarDistanceToEngine4
//			AND fRightCarDistanceToEngine4 < fLeftCarDistanceToEngine4
//			AND fRightCarDistanceToEngine4 < fFrontCarDistanceToEngine4
//				RightOfCarIsClosest[3] = TRUE
//				BackOfCarIsClosest[3] = FALSE
//				LeftOfCarIsClosest[3] = FALSE
//				FrontOfCarIsClosest[3] = FALSE
//			ENDIF				
//			
//			
//			//APPLY AIR RESISTANCE IF HE IS APPROACHING ENGINES
//			IF IS_ENTITY_AT_COORD(PlayerCar, Engine1WarningCoords, <<8,8,6>>)
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)		
//			ELIF IS_ENTITY_AT_COORD(PlayerCar, Engine2WarningCoords, <<8,8,6>>)
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)	
//			ELIF IS_ENTITY_AT_COORD(PlayerCar, Engine3WarningCoords, <<8,8,6>>)
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)		
//			ELIF IS_ENTITY_AT_COORD(PlayerCar, Engine4WarningCoords, <<8,8,6>>)
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.9)	
//			ELSE
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1)	
//			ENDIF	
//			
//			//APPLY BLASTS TO PLAYER CAR IF HE GETS TOO CLOSE 
//			//Apply force to the vehicle if it gets too close to engine 1.
//			IF IS_ENTITY_AT_COORD(PlayerCar, Engine1Coords, <<4,4,6>>)
//				IF FrontOfCarIsClosest[0] = TRUE
//					BLAST_VEHICLE(PlayerCar, FrontOfCarCoords, VForceOffFront, fMagFront)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo1, <<0,0,0>>, 30)	
//				ENDIF
//				IF BackOfCarIsClosest[0] = TRUE
//					BLAST_VEHICLE(PlayerCar, BackOfCarCoords, VForceOffBack, fMagBack)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo1, <<0,0,0>>, 30)	
//				ENDIF
//				IF LeftOfCarIsClosest[0] = TRUE
//					BLAST_VEHICLE(PlayerCar, LeftOfCarCoords, VForceOffLeft, fMagLeft)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo1, <<0,0,0>>, 30)	
//				ENDIF
//				IF RightOfCarIsClosest[0] = TRUE
//					BLAST_VEHICLE(PlayerCar, RightOfCarCoords, VForceOffRight, fMagRight)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo1, <<0,0,0>>, 30)	
//				ENDIF
//			ENDIF
//			
//			//Apply force to the vehicle if it gets too close to engine 2.
//			IF IS_ENTITY_AT_COORD(PlayerCar, Engine2Coords, <<4,4,6>>)
//				IF FrontOfCarIsClosest[1] = TRUE
//					BLAST_VEHICLE(PlayerCar, FrontOfCarCoords, VForceOffFront, fMagFront)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo2, <<0,0,0>>, 30)	
//				ENDIF
//				IF BackOfCarIsClosest[1] = TRUE
//					BLAST_VEHICLE(PlayerCar, BackOfCarCoords, VForceOffBack, fMagBack)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo2, <<0,0,0>>, 30)	
//				ENDIF
//				IF LeftOfCarIsClosest[1] = TRUE
//					BLAST_VEHICLE(PlayerCar, LeftOfCarCoords, VForceOffLeft, fMagLeft)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo2, <<0,0,0>>, 30)	
//				ENDIF
//				IF RightOfCarIsClosest[1] = TRUE
//					BLAST_VEHICLE(PlayerCar, RightOfCarCoords, VForceOffRight, fMagRight)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo2, <<0,0,0>>, 30)	
//				ENDIF
//			ENDIF
//			
//			//Apply force to the vehicle if it gets too close to engine 3.
//			IF IS_ENTITY_AT_COORD(PlayerCar, Engine3Coords, <<4,4,6>>)
//				IF FrontOfCarIsClosest[2] = TRUE
//					BLAST_VEHICLE(PlayerCar, FrontOfCarCoords, VForceOffFront, fMagFront)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo3, <<0,0,0>>, 30)	
//				ENDIF
//				IF BackOfCarIsClosest[2] = TRUE
//					BLAST_VEHICLE(PlayerCar, BackOfCarCoords, VForceOffBack, fMagBack)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo3, <<0,0,0>>, 30)	
//				ENDIF
//				IF LeftOfCarIsClosest[2] = TRUE
//					BLAST_VEHICLE(PlayerCar, LeftOfCarCoords, VForceOffLeft, fMagLeft)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo3, <<0,0,0>>, 30)	
//				ENDIF
//				IF RightOfCarIsClosest[2] = TRUE
//					BLAST_VEHICLE(PlayerCar, RightOfCarCoords, VForceOffRight, fMagRight)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo3, <<0,0,0>>, 30)	
//				ENDIF
//			ENDIF
//			
//			//Apply force to the vehicle if it gets too close to engine 4.
//			IF IS_ENTITY_AT_COORD(PlayerCar, Engine4Coords, <<4,4,6>>)
//				IF FrontOfCarIsClosest[3] = TRUE
//					BLAST_VEHICLE(PlayerCar, FrontOfCarCoords, VForceOffFront, fMagFront)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo4, <<0,0,0>>, 30)	
//				ENDIF
//				IF BackOfCarIsClosest[3] = TRUE
//					BLAST_VEHICLE(PlayerCar, BackOfCarCoords, VForceOffBack, fMagBack)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo4, <<0,0,0>>, 30)	
//				ENDIF
//				IF LeftOfCarIsClosest[3] = TRUE
//					BLAST_VEHICLE(PlayerCar, LeftOfCarCoords, VForceOffLeft, fMagLeft)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo4, <<0,0,0>>, 30)	
//				ENDIF
//				IF RightOfCarIsClosest[3] = TRUE
//					BLAST_VEHICLE(PlayerCar, RightOfCarCoords, VForceOffRight, fMagRight)	
//					BLAST_VEHICLE(PlayerCar, vBlastTo4, <<0,0,0>>, 30)	
//				ENDIF
//			ENDIF
//			
//		ENDIF
//	ENDIF
//ENDPROC

PROC CHANGE_NEWS_HEADLINE()
      	
  	IF iCurrentText > 4           
  		iCurrentText = 0        
	ENDIF
  	BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "DISPLAY_SCROLL_TEXT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentText)
  	END_SCALEFORM_MOVIE_METHOD()
	
	iCurrentText++
		
ENDPROC


PROC CHOPPER_CAM()      

		// if we haven't created the camera yet
      	IF NOT has_cam_been_created

            IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
				
          		// create the heli cam
          		heliChaseCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
   	  
		  		// attach it to the helicopter
//          		ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[26], heli_chase_cam_offset, TRUE)

         		// have it point at the player
//          		POINT_CAM_AT_ENTITY(heliChaseCam, EnemyCar, <<0.0,0.0,0.0>>, FALSE)

          		// activate it
          		SET_CAM_ACTIVE(heliChaseCam, TRUE)    
          		
				heli_chase_cam_active = FALSE        
          		has_cam_been_created = TRUE

            ENDIF 

      	ELSE
        	// check for button press and suitable conditions                 
//        	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct, TRUE, FALSE)
			AND bMissionFailed = FALSE
				
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP(TRUE)
				ENDIF
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CLEAR_PRINTS()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				
				IF AudioSceneStarted[2] = FALSE
					IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_CHOPPER_CAM")
						START_AUDIO_SCENE("SOL_3_CHOPPER_CAM")
					ENDIF
					//Play audio for the heli cam
					PLAY_SOUND_FRONTEND(NewsHeliSoundID, "Trevor_4_747_TV")
					AudioSceneStarted[2] = TRUE
				ENDIF						
					
				OVERLAY_SET_SECURITY_CAM(TRUE,TRUE)
//  				SET_TIMECYCLE_MODIFIER("blacknwhite")
				
				
				//FOR SCALEFORM NEWS HELI
				//Draw the BREAKING NEWS: AIRPORT CAR CHASE	
				IF setUpScrollTextsForScaleForm = FALSE
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING(ScaleformMovie, "SET_TEXT", "TRV4_NEWS1", "")//BREAKING NEWS!!
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS1")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
					END_SCALEFORM_MOVIE_METHOD()	
					setUpScrollTextsForScaleForm = TRUE
				ENDIF
				
				//Draw the scrolling text at different parts of the chase
				IF NewsPartOneTextCalled = FALSE
					//Set up the breaking news top line of scrolling text 
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 0, 0, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS2")//GLOBOPOL STING LEADS TO CAR CHASE THROUGH LOS SANTOS
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS2")
					END_SCALEFORM_MOVIE_METHOD()	
					//Set up the other news stories that will scroll along the bottom of the screen
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 1, 0, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS6")//POLICE FIND MISSING SUMBARINE AND UNREFINED URANIUM INTACT
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS6")
					END_SCALEFORM_MOVIE_METHOD()	
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 1, 1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS7")//CLAY “PG” JACKSON STATES “I AM NOT AN EPSILONIST” AT OPENING OF EPSILONIST CRECHE 
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS7")
					END_SCALEFORM_MOVIE_METHOD()				
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 1, 2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS8")//CHINESE FEEL BUYER’S REMORSE AFTER ACQUIRING LAST PIECE OF US ECONOMY
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS8")
					END_SCALEFORM_MOVIE_METHOD()				
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 1, 3, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS9")//NEW VIDEO GAME FAILS TO SHOCK EVEN MOST SKITISH POLITICIAN
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS9")
					END_SCALEFORM_MOVIE_METHOD()				
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 1, 4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS10")//4 OF 6 GAYS POLLED DON’T WANT TO GET MARRIED ANYWAY
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(4)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS10")
					END_SCALEFORM_MOVIE_METHOD()				
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "DISPLAY_SCROLL_TEXT", 0, 2)
//					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "DISPLAY_SCROLL_TEXT")
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
//					END_SCALEFORM_MOVIE_METHOD()				
		//			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "DISPLAY_SCROLL_TEXT", 1, 2)
					BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "DISPLAY_SCROLL_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					END_SCALEFORM_MOVIE_METHOD()	
					iScrollTimer = (GET_GAME_TIMER() - 5000)
					NewsPartOneTextCalled = TRUE
				ENDIF					
				
				//Change the top line every 5seconds
				IF GET_GAME_TIMER() > (iScrollTimer + 5000)
					CHANGE_NEWS_HEADLINE()
					iScrollTimer = GET_GAME_TIMER()
				ENDIF
				
				IF NewsPartTwoTextCalled = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 25000
							AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 50000
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "CLEAR_SCROLL_TEXT", 0)
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 0, 0, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS5")//CAR CHASE TEARAWAYS LEAVE CHAOS IN THEIR WAKE
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "DISPLAY_SCROLL_TEXT", 0, 3)
//								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "CLEAR_SCROLL_TEXT")
//									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
//								END_SCALEFORM_MOVIE_METHOD()	
								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS5")
								END_SCALEFORM_MOVIE_METHOD()
								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "DISPLAY_SCROLL_TEXT")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
								END_SCALEFORM_MOVIE_METHOD()
								NewsPartTwoTextCalled = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NewsPartThreeTextCalled = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 77600
							AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 115000
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "CLEAR_SCROLL_TEXT", 0)				
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 0, 0, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS3")//CAR CHASE CRAZIES NOW ON RUNWAY AT LS INTERNATIONAL
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "DISPLAY_SCROLL_TEXT", 0, 3)
//								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "CLEAR_SCROLL_TEXT")
//									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//								END_SCALEFORM_MOVIE_METHOD()	
								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS3")
								END_SCALEFORM_MOVIE_METHOD()
								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "DISPLAY_SCROLL_TEXT")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
								END_SCALEFORM_MOVIE_METHOD()								
								NewsPartThreeTextCalled = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NewsPartFourTextCalled = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 115000
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "CLEAR_SCROLL_TEXT", 0)					
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(ScaleformMovie, "SET_SCROLL_TEXT", 0, 0, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "TRV4_NEWS4")//OFFICERS CLOSE IN ON AIRPORT CAR CHASE MAD MEN
//								CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(ScaleformMovie, "DISPLAY_SCROLL_TEXT", 0, 3)
//								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "CLEAR_SCROLL_TEXT")
//									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//								END_SCALEFORM_MOVIE_METHOD()	
								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "SET_SCROLL_TEXT")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TRV4_NEWS4")
								END_SCALEFORM_MOVIE_METHOD()
								BEGIN_SCALEFORM_MOVIE_METHOD(ScaleformMovie, "DISPLAY_SCROLL_TEXT")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
								END_SCALEFORM_MOVIE_METHOD()								
								NewsPartFourTextCalled = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
				
				//Call every frame to draw to screen when player has button pressed.
				SET_TEXT_RENDER_ID (screenRT)
//				DRAW_SCALEFORM_MOVIE(ScaleformMovie, 0.5, 0.5, 0.95, 1.0, 255,255,255,0)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(ScaleformMovie, 255,255,255,0)
				
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//            		SET_CAM_COORD(heliChaseCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SetPieceCarID[20], heli_chase_cam_offset ))
//					SET_CAM_ROT(heliChaseCam, GET_ENTITY_ROTATION(SetPieceCarID[20]))				
//				ENDIF
				
				IF heli_chase_cam_active = FALSE
				
					// turn on the camera
//					SET_CAM_FOV(heliChaseCam, 20)
					SET_MULTIHEAD_SAFE(TRUE,TRUE)
					SHAKE_CAM(heliChaseCam, "ROAD_VIBRATION_SHAKE", 1)
					SET_CAM_ACTIVE(heliChaseCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					DISPLAY_RADAR(FALSE)
					SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
					//Stats for how long the player views the heli cam
					INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_START() 
            		heli_chase_cam_active = TRUE
						
				ENDIF
				
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2159773
				
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
				
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				
        	ELSE
          		IF heli_chase_cam_active

            		// turn it off
					SET_MULTIHEAD_SAFE(FALSE,TRUE)
            		SET_CAM_ACTIVE(heliChaseCam, FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DISPLAY_RADAR(TRUE)
//					CLEAR_TIMECYCLE_MODIFIER()
					IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CHOPPER_CAM")
						STOP_AUDIO_SCENE("SOL_3_CHOPPER_CAM")
					ENDIF
					STOP_SOUND(NewsHeliSoundID)
					//Stats for how long the player views the heli cam
					INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
//					RELEASE_SOUND_ID(NewsHeliSoundID)

					SET_FRONTEND_RADIO_ACTIVE(TRUE)
					
					//Reset Flags
					AudioSceneStarted[2] 	= FALSE
            		heli_chase_cam_active 	= FALSE

          		ENDIF

        	ENDIF
			
//			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vtunnel, <<10.0, 10.0, 10.0>>)
//				SET_CAM_ACTIVE(heliChaseCam, FALSE)
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			ENDIF

      	ENDIF

ENDPROC

//PURPOSE: Checks if all uber chase recordings are off screen.
FUNC BOOL IS_UBER_CHASE_OFF_SCREEN()

	//Check for main car being on screen
	IF DOES_ENTITY_EXIST(EnemyCar)
		IF IS_VEHICLE_DRIVEABLE(EnemyCar)
			IF NOT IS_ENTITY_ON_SCREEN(EnemyCar)
				EnemyCarIsOffScreen = TRUE
			ELSE
				EnemyCarIsOffScreen = FALSE
			ENDIF
		ELSE
			EnemyCarIsOffScreen = TRUE
		ENDIF
	ELSE
		EnemyCarIsOffScreen = TRUE
	ENDIF
	
	//Check for set piece cars being on screen
	FOR iSetPieceCount = 0 TO 24
		IF DOES_ENTITY_EXIST(SetPieceCarID[iSetPieceCount])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[iSetPieceCount])
				IF NOT IS_ENTITY_ON_SCREEN(SetPieceCarID[iSetPieceCount])
					SetPieceCarIDisOffScreen[iSetPieceCount] = TRUE
				ELSE
					SetPieceCarIDisOffScreen[iSetPieceCount] = FALSE
				ENDIF
			ELSE
				SetPieceCarIDisOffScreen[iSetPieceCount] = TRUE
			ENDIF
		ELSE
			SetPieceCarIDisOffScreen[iSetPieceCount] = TRUE
		ENDIF
	ENDFOR
	
	IF EnemyCarIsOffScreen = TRUE
	AND SetPieceCarIDisOffScreen[0] = TRUE
	AND SetPieceCarIDisOffScreen[1] = TRUE
	AND SetPieceCarIDisOffScreen[2] = TRUE
	AND SetPieceCarIDisOffScreen[3] = TRUE
	AND SetPieceCarIDisOffScreen[4] = TRUE
	AND SetPieceCarIDisOffScreen[5] = TRUE
	AND SetPieceCarIDisOffScreen[6] = TRUE
	AND SetPieceCarIDisOffScreen[7] = TRUE
	AND SetPieceCarIDisOffScreen[8] = TRUE
	AND SetPieceCarIDisOffScreen[9] = TRUE
	AND SetPieceCarIDisOffScreen[10] = TRUE
	AND SetPieceCarIDisOffScreen[11] = TRUE
	AND SetPieceCarIDisOffScreen[12] = TRUE
	AND SetPieceCarIDisOffScreen[13] = TRUE
	AND SetPieceCarIDisOffScreen[14] = TRUE
	AND SetPieceCarIDisOffScreen[15] = TRUE
	AND SetPieceCarIDisOffScreen[16] = TRUE
	AND SetPieceCarIDisOffScreen[17] = TRUE
	AND SetPieceCarIDisOffScreen[18] = TRUE
	AND SetPieceCarIDisOffScreen[19] = TRUE
	AND SetPieceCarIDisOffScreen[20] = TRUE
	AND SetPieceCarIDisOffScreen[21] = TRUE
	AND SetPieceCarIDisOffScreen[22] = TRUE
	AND SetPieceCarIDisOffScreen[23] = TRUE
	AND SetPieceCarIDisOffScreen[24] = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

//PURPOSE: controls the Uber playback speed. Takes into account different times of the chase to keep set pieces out the way of the player.
PROC UBER_PLAYBACK_SPEED_CONTROLLER()
	
	
	//Calculate the catch up speed multiplyer. Speed it up if player is in a slow vehicle, so that mission will fail, as the set peices will look bad if played in a slow vehicle.
		
		IF IS_UBER_CHASE_OFF_SCREEN()
//			PRINTSTRING("all uber chase recordings are off screen fCatchUPMultiplyer = 0.3 ") PRINTNL()
			fCatchUPMultiplyer = 0.5
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_VEHICLE_ESTIMATED_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 40
					fCatchUPMultiplyer = 0.85
				ELSE
					fCatchUPMultiplyer = 0.9
				ENDIF
			ELSE
				fCatchUPMultiplyer = 0.9
			ENDIF
		ENDIF
		
		//calculate the uber recording play back speed.
		IF NOT IS_ENTITY_DEAD(EnemyCar)
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					
					//Start
					IF playerComingFromWrongSide = FALSE
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 10000
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyCar) < 100
							OR IS_ENTITY_ON_SCREEN(EnemyCar)
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 40, 55, 100, 1.7, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
							ELSE
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 0.6, 40, 50, 60, 1.7, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
							ENDIF
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyCar) > 175
							fPlaybackSpeed = 0.6
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
						ELSE
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 10000
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 40, 180, 1.7, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
							ENDIF
						ENDIF
					ENDIF
					
					//1st Stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 10000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 23000
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[8], PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 30, 50, 1.7, 0.5, fCatchUPMultiplyer, TRUE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
						ELSE
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 180, 1.7, 0.5, fCatchUPMultiplyer, TRUE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
						ENDIF
					ENDIF
					
					//2nd stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 23000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 32000
						IF DOES_ENTITY_EXIST(SetPieceCarID[7])
							IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[7], PLAYER_PED_ID(), fPlaybackSpeed, 1, 20, 30, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
							ELSE
								IF DOES_ENTITY_EXIST(SetPieceCarID[8])
									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
										CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[8], PLAYER_PED_ID(), fPlaybackSpeed, 1, 10, 20, 40, 1.8, 1, fCatchUPMultiplyer, FALSE)
										SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
										UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
									ELSE
										CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 40, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
										SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
										UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
									ENDIF
								ELSE
									CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 40, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
									SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
									UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(SetPieceCarID[8])
								IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
									CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[8], PLAYER_PED_ID(), fPlaybackSpeed, 1, 10, 20, 40, 1.8, 1, fCatchUPMultiplyer, FALSE)
									SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
									UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
								ELSE
									CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 40, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
									SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
									UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
								ENDIF
							ELSE
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 40, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
							ENDIF								
						ENDIF
					ENDIF					
					
					//3rd stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 32000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 39000
						IF DOES_ENTITY_EXIST(SetPieceCarID[7])
							IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[7], PLAYER_PED_ID(), fPlaybackSpeed, 1, 22, 25, 100, 1.8, 0.5, fCatchUPMultiplyer, TRUE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
							ELSE
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 150, 1.8, 0.5, fCatchUPMultiplyer, TRUE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
							ENDIF
						ELSE
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 150, 1.8, 0.5, fCatchUPMultiplyer, TRUE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
						ENDIF
					ENDIF			
					
					//4th stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 39000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 56000
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[8], PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 40, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
						ELSE
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 150, 1.8, 1, fCatchUPMultiplyer, FALSE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
						ENDIF
					ENDIF	
					
					//5th stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 56000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 65000
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[11], PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 30, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
						ELSE
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 100, 1.8, 1, fCatchUPMultiplyer, FALSE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
						ENDIF
					ENDIF			
					
					//6th stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 65000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 90000
						IF DOES_ENTITY_EXIST(SetPieceCarID[13])
							IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(SetPieceCarID[13], PLAYER_PED_ID(), fPlaybackSpeed, 1, 30, 35, 50, 1.8, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
							ELSE
								CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 45, 55, 150, 1.8, 1, fCatchUPMultiplyer, FALSE)
								SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
								UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
							ENDIF
						ELSE
							CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 45, 55, 150, 1.8, 1, fCatchUPMultiplyer, FALSE)
							SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
							UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)	
						ENDIF
					ENDIF	
					
					//7th stage
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 90000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 110000
						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 60, 70, 150, 1.5, 0.5, fCatchUPMultiplyer, TRUE)
						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
					ENDIF	
					
					//2nd Last stage gradually increase the distance between them.
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 110000
//					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 113000
						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 100, 120, 150, 1.6, 1, fCatchUPMultiplyer, FALSE)
						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
					ENDIF	
					
//					//2nd Last stage gradually increase the distance between them.
//					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 113000
//					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 116000
//						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 150, 170, 180, 1.7, 1, fCatchUPMultiplyer, FALSE)
//						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
//					ENDIF
//					
//					//Last stage need the enemycar to be at least 60metres ahead of player to allow time for Molly to run into the hanger.
//					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 116000
//					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 118000
//						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 230, 240, 250, 1.9, 1, fCatchUPMultiplyer, FALSE)
//						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
//					ENDIF	
//
//					//Last stage need the enemycar to be at least 60metres ahead of player to allow time for Molly to run into the hanger.
//					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 118000
//						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1.25, 300, 320, 330, 2, 1.3, fCatchUPMultiplyer, FALSE)
//						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)		
//					ENDIF						
					
				ENDIF
			ENDIF		
		ENDIF

ENDPROC

//PURPOSE: Sets up enemy ped and enemy car
PROC SET_UP_ENEMY_PED()

	REQUEST_MODEL(COGCABRIO)
	REQUEST_MODEL(IG_MOLLY)	

	WHILE NOT HAS_MODEL_LOADED(COGCABRIO)
	OR NOT HAS_MODEL_LOADED(IG_MOLLY)
		WAIT(0)
	ENDWHILE
	
	//Create enemy car
	IF NOT DOES_ENTITY_EXIST(EnemyCar)
		EnemyCar = CREATE_VEHICLE(COGCABRIO, EnemyCarChaseStartingCoords, fEnemyCarChaseStartingHeading)
		SET_ENTITY_PROOFS(EnemyCar, TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
		SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyCar, "SOL_3_MOLLY_CAR_Group")
	ELSE
		IF IS_VEHICLE_DRIVEABLE(EnemyCar)
			SET_ENTITY_COORDS(EnemyCar, EnemyCarChaseStartingCoords)
			SET_ENTITY_HEADING(EnemyCar, fEnemyCarChaseStartingHeading)
			SET_ENTITY_PROOFS(EnemyCar, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
		ENDIF
	ENDIF		
	SET_VEHICLE_MODEL_IS_SUPPRESSED(COGCABRIO, TRUE)
	
	//Create enemy ped
	IF NOT DOES_ENTITY_EXIST(EnemyPed)
		EnemyPed = CREATE_PED_INSIDE_VEHICLE(EnemyCar, PEDTYPE_MISSION, IG_MOLLY)
//		GIVE_WEAPON_TO_PED(EnemyPed, WEAPONTYPE_PISTOL, 100, TRUE)
		SET_ENTITY_INVINCIBLE(EnemyPed, TRUE)
		SET_PED_CAN_BE_TARGETTED(EnemyPed, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
		SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed, RELGROUPHASH_PLAYER)
	ELSE
		IF NOT IS_PED_INJURED(EnemyPed)
			IF DOES_ENTITY_EXIST(EnemyCar)
				SET_PED_INTO_VEHICLE(EnemyPed, EnemyCar)
//				GIVE_WEAPON_TO_PED(EnemyPed, WEAPONTYPE_PISTOL, 100, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
			ENDIF
		ENDIF
	ENDIF	
	
	IF NOT IS_PED_INJURED(EnemyPed)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed, TRUE)
	ENDIF

ENDPROC

//PURPOSE: Runs a check if the player is in the airport then keeps the wanted level on all the time
PROC POLICE_CHECK()
	
	IF missionStage = STAGE_MAIN_CHASE
	OR missionStage = STAGE_ON_FOOT_CHASE
	OR missionStage = STAGE_EVADE_POLICE
	OR missionStage = STAGE_PED_RUNS_TOWARDS_ENGINE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_AIRPORT_AIRSIDE, 200)
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
	IF wantedLevelAirport = FALSE
		IF missionStage = STAGE_MAIN_CHASE	
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_AIRPORT_AIRSIDE, 200)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					wantedLevelAirport = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Handles all mission dialogue
PROC MISSION_DIALOGUE()
	
	
	SWITCH missionStage
	
		CASE STAGE_GET_TO_CHASE
			
			IF iControlFlag > 0
			
				IF iSubStage = 0
					//Flags
					donechat7 = FALSE
					doneDevinPhoneCall = FALSE
					doneReactChat = FALSE
					doneChase2Chat = FALSE
				
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, NULL, "Devin")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "Michael")
					iSubStage = 1
				ENDIF
				
				IF iSubStage = 1
					IF donechat7 = FALSE
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 2
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", CONV_PRIORITY_AMBIENT_MEDIUM)	
									//Get to the airport, talk to her, that's it.
									//Okay, okay, the airport. 
									donechat7 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//Have phone call from Solomon once player has got over 100m's from the office
					IF doneDevinPhoneCall = FALSE
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(MyLocalPedStruct, CHAR_DEVIN, "T1M4AUD", "SOL3_DEVCALL", CONV_PRIORITY_MEDIUM)
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									
									//Slow it down, slick. You're not thinking. 
									doneDevinPhoneCall = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF doneReactChat = FALSE
					AND doneDevinPhoneCall = TRUE
						IF HAS_CELLPHONE_CALL_FINISHED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_REACT", CONV_PRIORITY_AMBIENT_MEDIUM)	
									//Ahhh.
									doneReactChat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//Michael should comment as soon as he sees Molly 
					IF doneChase2Chat = FALSE
						IF DOES_ENTITY_EXIST(EnemyCar)
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyCar) < 200
								AND IS_ENTITY_ON_SCREEN(EnemyCar)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE2", CONV_PRIORITY_AMBIENT_MEDIUM)	
												
												REPLAY_RECORD_BACK_FOR_TIME(3.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
																					
												//What's she doing?
												//What's she up to?
												doneChase2Chat = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
		
			ENDIF
			
		BREAK
		
		CASE STAGE_MAIN_CHASE
			
			IF iControlFlag >= 0
			
				IF iSubStage = 0
					//Flags
					donePoliceMegaPhone[0] 	= FALSE
					donePoliceMegaPhone[1]	= FALSE
					donePoliceMegaPhone[2] 	= FALSE
					donePoliceMegaPhone[3] 	= FALSE
					doneChase4Chat 			= FALSE
					doneBoomChat 			= FALSE
					doneBlowChat			= FALSE
					doneChase3Chat 			= FALSE
					doneChase5Chat 			= FALSE
					doneChase6Chat 			= FALSE
					doneChatPolice5 		= FALSE
					doneChase7Chat 			= FALSE
					
					iPolice5ChatTimer = GET_GAME_TIMER()
				
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "Michael")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "SOL3COP")
					PRINTSTRING("iSubStage being set to 1 in mission Dialogue controller ") PRINTNL()
					iSubStage = 1
				ENDIF		
				
				IF iSubStage = 1
					//Michael should comment as soon as he sees Molly 
					IF doneChase2Chat = FALSE
						IF DOES_ENTITY_EXIST(EnemyCar)
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyCar) < 200
								AND IS_ENTITY_ON_SCREEN(EnemyCar)
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE2", CONV_PRIORITY_AMBIENT_MEDIUM)	
												//What's she doing?
												//What's she up to?
												doneChase2Chat = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								PRINTSTRING("iSubStage = 1 and playback is going on for enemycar ") PRINTNL()
								IF donePoliceMegaPhone[0] = FALSE
									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])
									OR IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])								
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 7000
										AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 28000
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE", CONV_PRIORITY_MEDIUM)
														//Ms. Shultz, please calm down.  We're here to escort you to the hangar.
														//Ms. Shultz, there is no need to panic. The LSPD are here to escort you to the hangar. 
														PRINTSTRING("T1M4_POLICE IS BEING CALLED") PRINTNL()
														donePoliceMegaPhone[0] = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF donePoliceMegaPhone[1] = FALSE
									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])
									OR IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 28000
										AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 34500
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE2", CONV_PRIORITY_MEDIUM)
														
														REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
														
														//You don't need to go that fast. Please relax. 
														//There is no need to drive erratically. Please calm down.
														donePoliceMegaPhone[1] = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF donePoliceMegaPhone[2] = FALSE
									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])
									OR IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 36000
										AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 41000
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE3", CONV_PRIORITY_MEDIUM)
														
														REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
														
														//You went past the hangar!
														//The Devin Weston hangar was back there on your left! 
														donePoliceMegaPhone[2] = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneBoomChat = FALSE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 41000
									AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 43500
										IF DOES_ENTITY_EXIST(Trailer)
										AND IS_ENTITY_ON_SCREEN(Trailer)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_BOOM", CONV_PRIORITY_MEDIUM)
														
														REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
														
														//Whoa! Fuck!
														//Holy shit!
														doneBoomChat = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneBlowChat = FALSE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50509
									AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 50872
										IF DOES_ENTITY_EXIST(Trailer)
										AND IS_ENTITY_ON_SCREEN(Trailer)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_BLOW", CONV_PRIORITY_MEDIUM)
														//Oh man!
														//Fuck me. 
														doneBlowChat = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF donePoliceMegaPhone[3] = FALSE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 58000
									AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 68000
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE4", CONV_PRIORITY_MEDIUM)
													//Ms. Shultz, you are endangering yourself and my fellow officers.
													//Ms. Shultz, you are in the path of aircraft, this is not a safe place to drive. 
													donePoliceMegaPhone[3] = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneChase3Chat = FALSE
								AND donePoliceMegaPhone[3] = TRUE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 76000
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE3", CONV_PRIORITY_MEDIUM)
													//You're telling me, she's gone crazy!
													//Say it like it is, she's totally flipped!
													doneChase3Chat = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneChase4Chat = FALSE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 80000
									AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 82000
										IF DOES_ENTITY_EXIST(SetPiecePlane[0])
											IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[0])
												IF IS_ENTITY_ON_SCREEN(SetPiecePlane[0])
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF NOT IS_MESSAGE_BEING_DISPLAYED()
														OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
															IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE4", CONV_PRIORITY_MEDIUM)
																
																REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
																
																//Oh, oh, oh - the plane!
																//You wanna take a plane full a tourists with ya?
																doneChase4Chat = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneChase5Chat = FALSE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82100
									AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 83500
										IF DOES_ENTITY_EXIST(SetPieceCarID[11])
											IF IS_ENTITY_ON_SCREEN(SetPieceCarID[11])
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF NOT IS_MESSAGE_BEING_DISPLAYED()
													OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
														IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE5", CONV_PRIORITY_MEDIUM)
															//Shit!
															//Whoa!
															doneChase5Chat = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELIF DOES_ENTITY_EXIST(SetPieceCarID[12])
											IF IS_ENTITY_ON_SCREEN(SetPieceCarID[12])
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF NOT IS_MESSAGE_BEING_DISPLAYED()
													OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
														IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE5", CONV_PRIORITY_MEDIUM)
															//Shit!
															//Whoa!
															doneChase5Chat = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneChase7Chat = FALSE
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 101891
									AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 105000
									AND IS_ENTITY_ON_SCREEN(EnemyCar)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE7", CONV_PRIORITY_MEDIUM)
													
													REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
																													
													//Nah, nah, nah, nah.
													//You're playing chicken!
													doneChase7Chat = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Do random comments for the rest of the chase from the police megaphones
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 105500
								AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 119000
								AND GET_GAME_TIMER() > (iPolice5ChatTimer + 7000)
								AND doneChatPolice5 = FALSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE5", CONV_PRIORITY_MEDIUM)
												//Ms. Shultz, stop the car!
												//Pull over now, Ms. Shultz! 
												//We'll protect you, just pull over. 
												//Stop your car now, Ms. Shultz. 
												//Stop your car!
												//Get off the runways, now!
												iPolice5ChatTimer = GET_GAME_TIMER()
												doneChatPolice5 = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF doneChase6Chat = FALSE
								AND doneChatPolice5 = TRUE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE6", CONV_PRIORITY_MEDIUM)
												//Stop the car, lady. 
												//Pull it over already. 
												doneChase6Chat = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								//Do random comments for the rest of the chase from the police megaphones
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 105500
								AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 119000
								AND GET_GAME_TIMER() > (iPolice5ChatTimer + 7000)
								AND doneChase6Chat = TRUE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE5", CONV_PRIORITY_MEDIUM)
												//Ms. Shultz, stop the car!
												//Pull over now, Ms. Shultz! 
												//We'll protect you, just pull over. 
												//Stop your car now, Ms. Shultz. 
												//Stop your car!
												//Get off the runways, now!
												iPolice5ChatTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
										
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF	
									
		BREAK
		
		CASE STAGE_ON_FOOT_CHASE
		
			IF iControlFlag > 0
		
				IF iSubStage = 0
					//Flags
					done1stMollyRunChat = FALSE
					MichaelsTurnToSpeak	= FALSE
					doneAnnoChat 		= FALSE
					mollysTurnToSpeak 	= TRUE
					
					iMichaelChatTimer = GET_GAME_TIMER()
					
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "Michael")
					IF NOT IS_PED_INJURED(EnemyPed)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, EnemyPed, "MOLLY")
					ENDIF
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 7, NULL, "AIRPORTANNO")
					
					iSubStage = 1
				ENDIF		
				
				IF iSubStage = 1
					
					//Do loud speaker announcement
					IF doneAnnoChat = FALSE
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(NULL, "T1M4_BBAA", "AIRPORTANNO", SPEECH_PARAMS_FORCE_FRONTEND)
						PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE("T1M4_BBAA", "AIRPORTANNO", <<-939.5, -2932.2, 16>>, "SPEECH_PARAMS_FORCE_FRONTEND")
						PRINTSTRING("PLAY_PED_AMBIENT_SPEECH_WITH_VOICE for T1M4_BBAA being called")
						doneAnnoChat = TRUE
					ENDIF
					
					IF mollysTurnToSpeak = TRUE
						IF done1stMollyRunChat = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_RUNS", CONV_PRIORITY_MEDIUM)
										//Stop him! Stop him, he's a killer!
										//He kills people! Help me!
										//He's a psychopath!  
										//Save me from this murderer!
										//I'm just a white collar criminal, but he's a real one!
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
																			
										iMollyRunChatTimer = GET_GAME_TIMER()
										done1stMollyRunChat = TRUE
										MichaelsTurnToSpeak = TRUE
										mollysTurnToSpeak = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF GET_GAME_TIMER() > (iMollyRunChatTimer + 5000)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_RUNS", CONV_PRIORITY_MEDIUM)
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
										
										//Stop him! Stop him, he's a killer!
										//He kills people! Help me!
										//He's a psychopath!  
										//Save me from this murderer!
										//I'm just a white collar criminal, but he's a real one!
										iMollyRunChatTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF MichaelsTurnToSpeak = TRUE
					AND GET_GAME_TIMER() > (iMichaelChatTimer + 4000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_SHOUT1", CONV_PRIORITY_MEDIUM)
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
									
									//Molly! Molly!
									//Hey, come on!
									//I think you need some help right now!
									//Lady, you need a friend!
									//Molly!
									iMichaelChatTimer = GET_GAME_TIMER()
									mollysTurnToSpeak = TRUE
									MichaelsTurnToSpeak = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF	
		
		BREAK
		
		CASE STAGE_PED_RUNS_TOWARDS_ENGINE
		
			IF iControlFlag > 0
		
				IF iSubStage = 0
					//Flags
					doneSol3Past 	= FALSE
					doneSol3Sucks 	= FALSE
					doneSol3Watch 	= FALSE
					doneSmushChat 	= FALSE
					doneSmush2Chat 	= FALSE

					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "Michael")
					IF NOT IS_PED_INJURED(EnemyPed)
						ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, EnemyPed, "MOLLY")
					ENDIF
					iSubStage = 1
				ENDIF		
				
				IF iSubStage = 1
				
					IF doneSol3Past = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_PAST", CONV_PRIORITY_MEDIUM)
									//Out of my way!
									//Move! A mad man's coming!
									doneSol3Past = TRUE
								ENDIF
							ENDIF
						ENDIF					
					ENDIF
					IF doneSol3Past = TRUE
					AND doneSol3Watch = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_WATCH", CONV_PRIORITY_MEDIUM)
									//Whoa! Stop! Look out!
									doneSol3Watch = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF doneSol3Sucks = FALSE
						IF DOES_ENTITY_EXIST(EnemyPed)
							IF NOT IS_PED_INJURED(EnemyPed)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(SuckedThroughJetSyncdScene)
									IF GET_SYNCHRONIZED_SCENE_PHASE(SuckedThroughJetSyncdScene) > 0.65
									AND GET_SYNCHRONIZED_SCENE_PHASE(SuckedThroughJetSyncdScene) < 0.835
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_SUCKS", CONV_PRIORITY_MEDIUM)
													
													REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
													
													//Argghhh!
													doneSol3Sucks = TRUE
												ENDIF
											ENDIF
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF doneSmushChat = FALSE
						IF NOT DOES_ENTITY_EXIST(EnemyPed)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()	
									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_SMUSH", CONV_PRIORITY_MEDIUM)
										//Eurrrrrr. 
										//Eugh. 
										doneSmushChat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF doneSmushChat = TRUE
					AND doneSmush2Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()	
								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_SMUSH2", CONV_PRIORITY_MEDIUM)
									//That didn't have to happen.
									//That was nasty, and needless. 
									doneSmush2Chat = TRUE	//Changed from 'doneSmushChat' - Ross W
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				
				ENDIF
				
			ENDIF	
		
		BREAK
		
		CASE STAGE_EVADE_POLICE
		
			IF iControlFlag > 0
		
				IF iSubStage = 0
					//Flags
					doneCopsChat = FALSE
					iMikeCopChatTimer = GET_GAME_TIMER()

					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "Michael")
					iSubStage = 1
				ENDIF		
				
				IF iSubStage = 1
				
					IF doneCopsChat = FALSE
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							//Run a check to see if there's any cops near the player before he shouts anything
							NearestCop = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), RELGROUPHASH_COP, 0, FALSE)
							IF DOES_ENTITY_EXIST(NearestCop)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NearestCop) < 40
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_COPS", CONV_PRIORITY_MEDIUM)
												//I ain't done anything!
												//She's the one behind this! 
												//All this mayhem - nothing to do with me!
												//I got what I came for, I'm leaving!
												//I'm just a witness, okay!?
												//Trespassing - that's the most you got on me!
												//You got some mess to clean up, leave me alone!
												doneCopsChat = TRUE
											ENDIF
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF doneCopsChat = TRUE
					AND GET_GAME_TIMER() > (iMikeCopChatTimer + 5000)
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							//Run a check to see if there's any cops near the player before he shouts anything
							NearestCop = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), RELGROUPHASH_COP, 0, FALSE)
							IF DOES_ENTITY_EXIST(NearestCop)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NearestCop) < 40
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_COPS", CONV_PRIORITY_MEDIUM)
												//I ain't done anything!
												//She's the one behind this! 
												//All this mayhem - nothing to do with me!
												//I got what I came for, I'm leaving!
												//I'm just a witness, okay!?
												//Trespassing - that's the most you got on me!
												//You got some mess to clean up, leave me alone!
												iMikeCopChatTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
					
				ENDIF
			
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
ENDPROC

//PURPOSE: Handles the adding and removal of audio mix groups for the police vehicles through out the chase
PROC POLICE_CAR_AUDIO_MIX_GROUPS()

	//Add the cars to the mix group as and when they are created through out the chase
	IF PoliceCarAddedToAudio[0] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[7])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[7], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[0] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[0] = FALSE
		IF PoliceCarAddedToAudio[0] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[7])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[7])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[7])
					PoliceCarRemovedFromAudio[0] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[7])
						PoliceCarRemovedFromAudio[0] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[7])
				PoliceCarRemovedFromAudio[0] = TRUE	
			ENDIF
		ENDIF
	ENDIF
		
	IF PoliceCarAddedToAudio[1] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[8])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[8], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[1] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[1] = FALSE
		IF PoliceCarAddedToAudio[1] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[8])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[8])
					PoliceCarRemovedFromAudio[1] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[8])
						PoliceCarRemovedFromAudio[1] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[8])
				PoliceCarRemovedFromAudio[1] = TRUE	
			ENDIF
		ENDIF
	ENDIF	

	IF PoliceCarAddedToAudio[2] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[11])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[11], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[2] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[2] = FALSE
		IF PoliceCarAddedToAudio[2] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[11])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[11])
					PoliceCarRemovedFromAudio[2] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[11])
						PoliceCarRemovedFromAudio[2] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[11])
				PoliceCarRemovedFromAudio[2] = TRUE	
			ENDIF
		ENDIF
	ENDIF
	
	IF PoliceCarAddedToAudio[3] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[12])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[12], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[3] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[3] = FALSE
		IF PoliceCarAddedToAudio[3] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[12])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[12])
					PoliceCarRemovedFromAudio[3] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[12])
						PoliceCarRemovedFromAudio[3] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[12])
				PoliceCarRemovedFromAudio[3] = TRUE	
			ENDIF
		ENDIF
	ENDIF	
	
	IF PoliceCarAddedToAudio[4] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[13])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[13], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[4] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[4] = FALSE
		IF PoliceCarAddedToAudio[4] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[13])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[13])
					PoliceCarRemovedFromAudio[4] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[13])
						PoliceCarRemovedFromAudio[4] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[13])
				PoliceCarRemovedFromAudio[4] = TRUE	
			ENDIF
		ENDIF
	ENDIF	
	
	IF PoliceCarAddedToAudio[5] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[14])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[14])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[14], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[5] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[5] = FALSE
		IF PoliceCarAddedToAudio[5] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[14])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[14])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[14])
					PoliceCarRemovedFromAudio[5] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[14])
						PoliceCarRemovedFromAudio[5] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[14])
				PoliceCarRemovedFromAudio[5] = TRUE	
			ENDIF
		ENDIF
	ENDIF	
	
	IF PoliceCarAddedToAudio[6] = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[15])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[15])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(SetPieceCarID[15], "SOL_3_POLICE_CARS_Group")
				PoliceCarAddedToAudio[6] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF PoliceCarRemovedFromAudio[6] = FALSE
		IF PoliceCarAddedToAudio[6] = TRUE
			IF DOES_ENTITY_EXIST(SetPieceCarID[15])
				IF NOT IS_VEHICLE_DRIVEABLE(SetPieceCarID[15])	
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[15])
					PoliceCarRemovedFromAudio[6] = TRUE
				ELSE
					IF missionStage = STAGE_ON_FOOT_CHASE
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[15])
						PoliceCarRemovedFromAudio[6] = TRUE
					ENDIF
				ENDIF
			ELSE
//				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(SetPieceCarID[15])
				PoliceCarRemovedFromAudio[6] = TRUE	
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: deals with all fail checks throughout mission.
PROC FAIL_CHECKS()

	IF GameCanFail = TRUE
		
		//Fail the mission if the player goes straight on instead of following the translator to the left at the end of the chase
		IF missionStage = STAGE_MAIN_CHASE
			//Only allow it after this stage and if the cutscene hasn't started.
			IF ChaseStage > 1
			AND ShortCutActive = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-828.698730,-2938.073242,12.217451>>, <<-807.691101,-2905.147705,16.356258>>, 28.000000)
					SetMissionFailed("TRV4_FAIL1") //Molly escaped.
					PRINTSTRING("Player went straight on instead of following Molly") PRINTNL()	
				ENDIF
			ENDIF
		ENDIF
		
		IF missionStage = STAGE_PED_RUNS_TOWARDS_ENGINE
			IF DOES_ENTITY_EXIST(ObjFilmReel)
				IF DOES_BLIP_EXIST(filmReelBlip)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ObjFilmReel) > 100
						SetMissionFailed("TRV4_FAIL6") //Michael abandoned the film reel.
						PRINTSTRING("Player has gone more than 100m's away from the film reel") PRINTNL()	
					ENDIF
				ENDIF
			ENDIF
		ENDIF

//		//Fail mission if players car gets destroyed
//		IF CheckPlayerCarHealthForFail = TRUE
//			IF DOES_ENTITY_EXIST(PlayerCar)
//				IF NOT IS_VEHICLE_DRIVEABLE(PlayerCar)
//					CLEAR_PRINTS()
//					PRINT_NOW("TRV4_FAIL2", DEFAULT_GOD_TEXT_TIME, 1)//~r~Your car was destroyed.
//					Mission_Failed()
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Fail mission here
		IF missionStage = STAGE_MAIN_CHASE
			IF ChaseStage = 2
				IF GET_GAME_TIMER() > (moveStageOnTimer + 7000)
				AND playerHasGotCloseEnough = FALSE
					SetMissionFailed("TRV4_FAIL1") //Molly escaped.
					PRINTSTRING("Player is too far behind Molly after she has stopped") PRINTNL()	
				ENDIF
			ENDIF
		ENDIF

		
		//Fail mission if player tries to take a big short cut during the chase.
		IF missionStage = STAGE_MAIN_CHASE
			IF DOES_ENTITY_EXIST(EnemyCar)
				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 62000
						AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 77000
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1219, -2570, 13>>) < 210
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 200
									AND GET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 125
										IF failWrongWayTimerSet = FALSE
											iFailWrongWayTimer = GET_GAME_TIMER()
											failWrongWayTimerSet = TRUE
										ENDIF
										IF failWrongWayTimerSet = TRUE
										AND GET_GAME_TIMER() > (iFailWrongWayTimer + 4000)
											SetMissionFailed("TRV4_FAIL1") //Molly escaped.
											PRINTSTRING("Player is Beagan and is trying to take a short cut") PRINTNL()	
										ENDIF
									ELSE
										//reset flag for timer being set
										IF failWrongWayTimerSet = TRUE
											failWrongWayTimerSet = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Fail the mission if player stops for a can of juice when chasing Molly through the hanger.
		IF missionStage = STAGE_ON_FOOT_CHASE
		OR missionStage = STAGE_PED_RUNS_TOWARDS_ENGINE
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-928.9, -2935, 13>>) < 5
			AND NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SetMissionFailed("TRV4_FAIL1") //Molly escaped.
				PRINTSTRING("Player is Beagan and is having a can of juice") PRINTNL()		
			ENDIF
		ENDIF
		
		//handle things if the player gets in a heli/plane before or during the car chase.
		IF missionStage = STAGE_GET_TO_CHASE
		OR missionStage = STAGE_MAIN_CHASE
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_AIRPORT_AIRSIDE, 200)
					SetMissionFailed("TRV4_FAIL2") //Michael flew into a restricted airspace.
					PRINTSTRING("Michael flew into a restricted airspace.") PRINTNL()
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV4_HELP3")
						PRINT_HELP_FOREVER("TRV4_HELP3")//The airport has restricted airspace. Get another vehicle.
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRV4_HELP3")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
		
		//If the translators car is destroyed then kill the translator if he is inside the vehicle and then fail the mission.
		IF DOES_ENTITY_EXIST(EnemyCar)
			IF NOT IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF DOES_ENTITY_EXIST(EnemyPed)
					IF NOT IS_PED_INJURED(EnemyPed)
						PRINTSTRING("Killing the translator as his car has been destroyde mission will fail now") PRINTNL()
						SET_ENTITY_INVINCIBLE(EnemyPed, FALSE)
						SET_ENTITY_HEALTH(EnemyPed, 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF checkForEnemyPedDeath = TRUE
			IF DOES_ENTITY_EXIST(EnemyPed)
				IF IS_PED_INJURED(EnemyPed)
					SetMissionFailed("TRV4_FAIL3") //Molly died.
					PRINTSTRING("The translator has died mission failed") PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		
		//Fail if player gets too far away from translator
		IF missionStage = STAGE_MAIN_CHASE
			IF distanceCheckRequired = TRUE
				IF NOT IS_PED_INJURED(EnemyPed)
					IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EnemyPed) > 300	
						IF playerNeedsWarned = TRUE
							CLEAR_PRINTS()
							PRINT_NOW("TRV4_WARN1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Molly is getting away.
							playerNeedsWarned = FALSE
						ENDIF
					ELSE
						IF playerNeedsWarned = FALSE
							playerNeedsWarned = TRUE
						ENDIF
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 40000
								IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EnemyPed) > 400
									SetMissionFailed("TRV4_FAIL1") //Molly escaped.
									PRINTSTRING("Player is more than 400m's away from the translator mission failed") PRINTNL()
								ENDIF
							ELSE
								IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EnemyPed) > 350
									SetMissionFailed("TRV4_FAIL1") //Molly escaped.
									PRINTSTRING("Player is more than 350m's away from the translator mission failed") PRINTNL()
								ENDIF
							ENDIF
						ENDIF
					ENDIF			
				ENDIF	
			ENDIF
			
			IF ChaseStage = 3
				PRINTSTRING("FAIL CHECK AREA ChaseStage = 3 STAGE_MAIN_CHASE") PRINTNL()
				IF ShortCutDone = TRUE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-934.4, -2927.2, 13.2>>) > 25
					OR GET_GAME_TIMER() > (failOnFootTimer + 12000)
						SetMissionFailed("TRV4_FAIL1") //Molly escaped.
						PRINTSTRING("Player is more than 15metres away from the hanger entrance or the failOnFootimer went passed 12seconds") PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Fail if player gets in a cab
		IF missionStage = STAGE_GET_TO_CHASE
		OR missionstage = STAGE_MAIN_CHASE
			IF IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
				SetMissionFailed("TRV4_FAIL1") //Molly escaped.
				PRINTSTRING("Player is in a taxi mission failed") PRINTNL()
			ENDIF
//			//Fail if players car is stuck
//			IF PlayersCarAddedToVehicleUpSideDownCheck = TRUE
//				IF DOES_ENTITY_EXIST(PlayerCar)
//					IF IS_VEHICLE_STUCK_ON_ROOF(PlayerCar)
//						SetMissionFailed("TRV4_FAIL1")
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
		ENDIF
		
		//Fail if player tries to enter hanger before end of chase cutscene
		IF missionStage = STAGE_MAIN_CHASE
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 111509 
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1070.837646,-2942.241211,12.195338>>, <<-1009.038025,-2977.608887,17.444427>>, 89.500000)
							SetMissionFailed("TRV4_FAIL1") //Molly escaped.
							PRINTSTRING("Playerscar is in wrong area mission failed") PRINTNL()
						ENDIF
					ENDIF
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 122000 
//						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1004.428467,-3145.664795,9.694427>>, <<-886.709167,-2939.042725,20.194931>>, 141.000000)
//							SetMissionFailed("TRV4_FAIL1") //Molly escaped.
//							PRINTSTRING("Playerscar is too far away from the enemycar mission failed") PRINTNL()
//						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF vehicleUpsideDownTimerSet = FALSE
									ivehicleUpsideDownTimer = GET_GAME_TIMER()
									vehicleUpsideDownTimerSet = TRUE
								ENDIF
								IF vehicleUpsideDownTimerSet = TRUE
									IF GET_GAME_TIMER() > (ivehicleUpsideDownTimer + 4000)
										SetMissionFailed("TRV4_FAIL1") //Molly escaped.
										PRINTSTRING("Playerscar is upside down mission failed") PRINTNL()
									ENDIF
								ENDIF
							ELSE
								vehicleUpsideDownTimerSet = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Fail if player tries to go wrong way to get to translator
		IF missionStage = STAGE_GET_TO_CHASE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.410767,-2173.416992,-1.911488>>, <<-605.684570,-2358.228516,27.328419>>, 250.000000)
				SetMissionFailed("TRV4_FAIL1")  //Molly escaped.
				PRINTSTRING("Player went wrong way mission failed") PRINTNL()				
			ENDIF
		ENDIF
		
		
		IF bMissionFailed = TRUE
			
			PRINTSTRING("bMissionFailed = TRUE") PRINTNL()
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Mission_Failed()
			ELSE
				PRINTSTRING("GET_MISSION_FLOW_SAFE_TO_CLEANUP() IS RETURNING FALSE") PRINTNL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Checks what vehicle the player is using for replays.
PROC PLAYER_VEHICLE()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			PlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
ENDPROC

////PURPOSE: Opening cutscene for the mission.
PROC DO_STAGE_OPENING_CUTSCENE()

	// Do opening cutscene
	IF iControlFlag = 0
		
		bIsCutSceneRunning 				= TRUE
		vehiclesMovedAtStartLocation 	= FALSE
		musicStarted 					= FALSE
		vehiclesMovedForStart 			= FALSE

		WHILE bIsCutSceneRunning = TRUE
		
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_LegalTrouble") 
		
			SWITCH iCutsceneStage
			
				CASE 0 	
					
					REQUEST_CUTSCENE("sol_3_int")
					
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					
						//Do a time of day skip cutscene if we are out of mission hours.
						WHILE NOT DO_TIMELAPSE(SP_MISSION_SOLOMON_3, sToD, FALSE, TRUE)
							WAIT(0)
						ENDWHILE
						
	//					WHILE NOT HAS_CUTSCENE_LOADED()
	//						WAIT(0)
	//						PRINTSTRING("Stuck waiting for cutscene sol_3_int to load..") PRINTNL()
	//					ENDWHILE
						
						//Flags
						EverythingSetUpForMissionStart 	= FALSE
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
											
				      	//Configure script systems into a safe mode.
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						
						//Grab the players cars from lead in scene.
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
							IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
								SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
								PlayerCar = g_sTriggerSceneAssets.veh[0]
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
							IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
								SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
								PlayerCar2 = g_sTriggerSceneAssets.veh[1]
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
							IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[2])
								SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
								PlayerCar3 = g_sTriggerSceneAssets.veh[2]
							ENDIF
						ENDIF
						
						//Get the car the player arrived in 
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[3])
							IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[3])
								SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[3], TRUE, TRUE)
								PlayerStartCar = g_sTriggerSceneAssets.veh[3]
							ENDIF
						ENDIF					
						
	//					IF NOT DOES_CAM_EXIST(StaticCam)
	//						StaticCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	//					ENDIF
						
	//					SET_CAM_PARAMS(StaticCam,<<-1023.903442,-486.551208,40.256775>>,<<-11.164144,-0.000000,-61.239964>>,50.000000)
	//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
	//						
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
	//					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-1158.0, -1530.7, 4>>, 15)				
						
						INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
						
						//Fade the game in
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						ENDIF
						
						iCutsceneStage ++
					ENDIF
					
				BREAK	
				
				CASE 1		
						
					IF vehiclesMovedAtStartLocation = FALSE		
						IF IS_CUTSCENE_ACTIVE()
							//Move vehicles to a safe area	
							IF vehiclesMovedForStart = FALSE
								IF DOES_ENTITY_EXIST(PlayerStartCar)
									IF IS_VEHICLE_DRIVEABLE(PlayerStartCar)
										IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(PlayerStartCar)) 
											//SET_ENTITY_COORDS(PlayerStartCar, <<-1049.1498, -510.0068, 35.0386>>)
											//SET_ENTITY_HEADING(PlayerStartCar, 357.1288)
											//SET_VEHICLE_ON_GROUND_PROPERLY(PlayerStartCar)
											//SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1049.1498, -510.0068, 35.0386>>, 357.1288)
											CONTROL_LANDING_GEAR(PlayerStartCar, LGC_DEPLOY_INSTANT)
											SET_ENTITY_COORDS(PlayerStartCar, <<-1048.7219, -506.8387, 35.0386>>)
											SET_ENTITY_HEADING(PlayerStartCar, 357.1288)
											SET_VEHICLE_DOORS_SHUT(PlayerStartCar, TRUE)
											SET_VEHICLE_ON_GROUND_PROPERLY(PlayerStartCar)
											SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1048.7219, -506.8387, 35.0386>>, 357.1288)
											vehiclesMovedForStart = TRUE
										ENDIF
										IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(PlayerStartCar))
										OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(PlayerStartCar))
										OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(PlayerStartCar))
										OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(PlayerStartCar))
											IF GET_ENTITY_MODEL(PlayerStartCar) != BUS
											AND GET_ENTITY_MODEL(PlayerStartCar) != POUNDER
											AND GET_ENTITY_MODEL(PlayerStartCar) != PACKER
											AND GET_ENTITY_MODEL(PlayerStartCar) != AIRBUS
											AND GET_ENTITY_MODEL(PlayerStartCar) != AMBULANCE
											AND GET_ENTITY_MODEL(PlayerStartCar) != BARRACKS
											AND GET_ENTITY_MODEL(PlayerStartCar) != BARRACKS2
											AND GET_ENTITY_MODEL(PlayerStartCar) != BENSON
											AND GET_ENTITY_MODEL(PlayerStartCar) != BIFF
												SET_ENTITY_COORDS(PlayerStartCar, <<-1027.6575, -486.6945, 35.9571>>)
												SET_ENTITY_HEADING(PlayerStartCar, 207.7515)
												SET_VEHICLE_ON_GROUND_PROPERLY(PlayerStartCar)
												SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1027.6575, -486.6945, 35.9571>>, 207.7515)
												vehiclesMovedForStart = TRUE
											ENDIF
										ENDIF
										IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(PlayerStartCar)) 
										AND NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(PlayerStartCar)) 
										AND NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(PlayerStartCar)) 
										AND NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(PlayerStartCar)) 
										AND NOT IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(PlayerStartCar)) 
											DELETE_VEHICLE(PlayerStartCar)
											vehiclesMovedForStart = TRUE
										ENDIF
									ENDIF
								ENDIF											
							ENDIF							
							//Cut away from the time of day cutscene if it's running.
							SET_TODS_CUTSCENE_RUNNING(sToD, FALSE)
							vehiclesMovedAtStartLocation = TRUE
						ENDIF
					ENDIF
								
					//Set everything up to initialise mission
					IF EverythingSetUpForMissionStart = FALSE
						//Sort out groups
					  	ADD_RELATIONSHIP_GROUP("SecGuards", SecGuardGroup)
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, SecGuardGroup)
					  	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, SecGuardGroup, RELGROUPHASH_PLAYER)
						
						//Load text 
						REQUEST_ADDITIONAL_TEXT("TREV4", MISSION_TEXT_SLOT)						
						
						EverythingSetUpForMissionStart = TRUE
					ENDIF	
					
					IF musicStarted = FALSE
						IF IS_CUTSCENE_PLAYING()
						
							CLEAR_PED_WETNESS(PLAYER_PED_ID())
							CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
							RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
						
							IF GET_CUTSCENE_TIME() > 120000
								TRIGGER_MUSIC_EVENT("TRV4_START")
								PRINTSTRING("TRV4_START music event triggered")
								musicStarted = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1012.2676, -480.0742, 38.9757>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 121.4087)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000) 
        				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN) 
					ENDIF
					
					IF CAN_SET_EXIT_STATE_FOR_CAMERA()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					ENDIF
					
					IF NOT IS_CUTSCENE_ACTIVE()
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_SOLOMON_3)
						IF musicStarted = FALSE
							TRIGGER_MUSIC_EVENT("TRV4_START_CS_SKIP")
							PRINTSTRING("TRV4_START_CS_SKIP music event triggered")
							musicStarted = TRUE
						ENDIF					
					
						REPLAY_STOP_EVENT()
					
						//Return script systems to normal.
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						icutsceneStage++
					ENDIF	
					
				BREAK
				
				CASE 2
				
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					IF DOES_ENTITY_EXIST(PlayerCar)
						IF IS_VEHICLE_DRIVEABLE(PlayerCar)
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerCar)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(PlayerCar2)
						IF IS_VEHICLE_DRIVEABLE(PlayerCar2)
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerCar2)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar2)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(PlayerCar3)
						IF IS_VEHICLE_DRIVEABLE(PlayerCar3)
							IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerCar3)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar3)
							ENDIF
						ENDIF
					ENDIF
					
					INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
					
					#IF IS_DEBUG_BUILD
						jSkipReady = TRUE
					#ENDIF			
					
					//Flags 
					bIsCutSceneRunning 	= FALSE
					iCutsceneStage		= 0
					iControlFlag 		= 0
					missionStage 		= STAGE_GET_TO_CHASE
					
				BREAK
			
			ENDSWITCH
			
			WAIT(0)
		ENDWHILE

	ENDIF
	
ENDPROC


////PURPOSE: Handles all god text and dialogue 
//PROC Handle_Dialogue_And_God_Text()
//		
//		//Check if player is armed for dialogue
//		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), PlayersWeapon)
//		
//		IF PlayersWeapon != WEAPONTYPE_UNARMED
//			playerArmed = TRUE
//		ELSE
//			playerArmed = FALSE
//		ENDIF	
//		
//		//Check multiple options for the cops opening line of dialogue.
//		IF doneChat1 = FALSE
//			//Do random conversation line from cop 1 if player is shooting near the cops and hasn't already been warned.
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1163, -1516.9, 4.2>>) < 20
//				IF IS_PED_SHOOTING(PLAYER_PED_ID())
//					KILL_ANY_CONVERSATION()
//					IF NOT IS_MESSAGE_BEING_DISPLAYED()
//					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//						PRINTSTRING("Adding ped for speaker 3_4") PRINTNL()
//						IF IS_FIB_COP_ADDED_TO_DIALOGUE(3)
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_DROP", CONV_PRIORITY_AMBIENT_MEDIUM)
//								//Special task force! Drop your weapon!
//								//Drop it! Globopol!
//								//Globopol task force! Drop the weapon!
//								SETTIMERA(0)
//								doneChat1 = TRUE
//								doneChat3 = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		
//			//Do random line of dialogue if player tries to attack any of the cops.
//			IF playerArmed = FALSE
//				FOR icount = 0 TO 2
//					IF NOT IS_PED_INJURED(FBICop[icount])
//						IF IS_PED_IN_COMBAT(PLAYER_PED_ID(), FBICop[icount])
//							KILL_ANY_CONVERSATION()
//							IF NOT IS_MESSAGE_BEING_DISPLAYED()
//							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//								PRINTSTRING("Adding ped for speaker 3_3") PRINTNL()
//								IF IS_FIB_COP_ADDED_TO_DIALOGUE(3)
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_BUST", CONV_PRIORITY_AMBIENT_MEDIUM)
//										//Globopol. Get down!
//										//Globopol. Give it up!
//										//We're with Globopol! You're under arrest!
//										SETTIMERA(0)
//										doneChat1 = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDFOR
//			ENDIF
//			
//			//Do random line of dialogue if player is at any of the below coords, check if player is armed or not.
//			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1150.8344, -1522.0084, 3.3305 >>, <<2.5, 2.5, 2.5>>)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1158.94, -1513.135, 3.4 >>, <<11.06, 9.025, 5>>)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1153.39, -1533.395, 3 >>, <<12.36, 7.585, 5>>)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1146.015, -1523.205, 3.785 >>, <<1.875, 2.835, 0.785>>)
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1160, -1520, 4>>, <<12.5, 6.5, 3>>)
//				IF playerArmed = FALSE
//					KILL_ANY_CONVERSATION()
//					IF NOT IS_MESSAGE_BEING_DISPLAYED()
//					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//						PRINTSTRING("Adding ped for speaker 3_2") PRINTNL()
//						IF IS_FIB_COP_ADDED_TO_DIALOGUE(3)
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_BUST", CONV_PRIORITY_AMBIENT_MEDIUM)
//								//Globopol. Get down!
//								//Globopol. Give it up!
//								//We're with Globopol! You're under arrest!
//								SETTIMERA(0)
//								doneChat1 = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					KILL_ANY_CONVERSATION()
//					IF NOT IS_MESSAGE_BEING_DISPLAYED()
//					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//						PRINTSTRING("Adding ped for speaker 3_1") PRINTNL()
//						IF IS_FIB_COP_ADDED_TO_DIALOGUE(3)
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_DROP", CONV_PRIORITY_AMBIENT_MEDIUM)
//								//Special task force! Drop your weapon!
//								//Drop it! Globopol!
//								//Globopol task force! Drop the weapon!
//								SETTIMERA(0)
//								doneChat1 = TRUE
//								doneChat3 = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF		
//
//		ENDIF
//				
//		//Do more dialogue
//		IF doneChat1 = TRUE
//			
//			IF doneChat2 = FALSE
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//					IF IS_FIB_COP_ADDED_TO_DIALOGUE(5)
//						IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_AIRPORT", CONV_PRIORITY_AMBIENT_MEDIUM)
//							//The informant's running for the airport!
//							//Informant's headed for the airport!
//							//Stop the informant! He's going to the airport!
//							SETTIMERA(0)
//							doneChat2 = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF PlayerArmed = TRUE
//				IF doneChat2 = TRUE
//					IF doneChat3 = FALSE
//						IF NOT IS_MESSAGE_BEING_DISPLAYED()
//						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//							IF IS_FIB_COP_ADDED_TO_DIALOGUE(4)
//								IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_BUST2", CONV_PRIORITY_AMBIENT_MEDIUM)
//									//Globopol! Time's up!
//									//Globopol! You got nowhere to run!
//									//Freeze, asshole! Globopol!
//									doneChat3 = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//		ENDIF
//		
//ENDPROC

////PURPOSE: Everything that is to be handled during this scripted shootout.
//PROC SCRIPT_SHOOTOUT()
//
//	//Once FREEZE Globopol has been shouted do the following
//	IF doneChat1 = TRUE
//		IF CopTasksGiven = FALSE
//			//Check if player has fired weapon, if so then cops should instantly combat back.
//			IF IS_PED_SHOOTING(PLAYER_PED_ID())
//				IF NOT IS_PED_INJURED(FBICop[0])
//					CLEAR_PED_TASKS(FBICop[0])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000, FIRING_TYPE_1_THEN_AIM)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1159.4823, -1513.2112, 3.2403 >>, 1000)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(FBICop[0], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//				IF NOT IS_PED_INJURED(FBICop[1])
//					CLEAR_PED_TASKS(FBICop[1])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1162.4137, -1515.2358, 3.2792 >>, 750)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(FBICop[1], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//				IF NOT IS_PED_INJURED(FBICop[2])
//					CLEAR_PED_TASKS(FBICop[2])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1170.2686, -1516.2545, 3.4170 >>, 500)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(FBICop[2], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF	
//				CopTasksGiven = TRUE
//			ENDIF
//			
//			//Check for player getting too close to cops
//			IF NOT IS_PED_INJURED(FBICop[0])
//				IF NOT IS_PED_INJURED(FBICop[1])
//					IF NOT IS_PED_INJURED(FBICop[2])
//						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), FBICop[0]) < 8
//						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), FBICop[1]) < 8
//						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), FBICop[2]) < 8
//							IF NOT IS_PED_INJURED(FBICop[0])
//								CLEAR_PED_TASKS(FBICop[0])
//								OPEN_SEQUENCE_TASK(seq)
//									TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000, FIRING_TYPE_1_THEN_AIM)
//									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1159.4823, -1513.2112, 3.2403 >>, 1000)
//									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//								CLOSE_SEQUENCE_TASK(seq)
//								TASK_PERFORM_SEQUENCE(FBICop[0], seq)
//								CLEAR_SEQUENCE_TASK(seq)
//							ENDIF
//							IF NOT IS_PED_INJURED(FBICop[1])
//								CLEAR_PED_TASKS(FBICop[1])
//								OPEN_SEQUENCE_TASK(seq)
//									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1162.4137, -1515.2358, 3.2792 >>, 750)
//									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//								CLOSE_SEQUENCE_TASK(seq)
//								TASK_PERFORM_SEQUENCE(FBICop[1], seq)
//								CLEAR_SEQUENCE_TASK(seq)
//							ENDIF
//							IF NOT IS_PED_INJURED(FBICop[2])
//								CLEAR_PED_TASKS(FBICop[2])
//								OPEN_SEQUENCE_TASK(seq)
//									TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1170.2686, -1516.2545, 3.4170 >>, 500)
//									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//								CLOSE_SEQUENCE_TASK(seq)
//								TASK_PERFORM_SEQUENCE(FBICop[2], seq)
//								CLEAR_SEQUENCE_TASK(seq)
//							ENDIF
//							CopTasksGiven = TRUE
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//IF player hasn't fired weapon then check for timer before giving cops tasks to combat.
//			IF TIMERA() > 7000
//				IF NOT IS_PED_INJURED(FBICop[0])
//					CLEAR_PED_TASKS(FBICop[0])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000, FIRING_TYPE_1_THEN_AIM)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1159.4823, -1513.2112, 3.2403 >>, 1000)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(FBICop[0], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//				IF NOT IS_PED_INJURED(FBICop[1])
//					CLEAR_PED_TASKS(FBICop[1])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1162.4137, -1515.2358, 3.2792 >>, 750)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(FBICop[1], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//				IF NOT IS_PED_INJURED(FBICop[2])
//					CLEAR_PED_TASKS(FBICop[2])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, << -1170.2686, -1516.2545, 3.4170 >>, 500)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(FBICop[2], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//				CopTasksGiven = TRUE
//			ELSE
//				IF copsAimingAtPlayer = FALSE
//					IF NOT IS_PED_INJURED(FBICop[0])
//						CLEAR_PED_TASKS(FBICop[0])
//						TASK_AIM_GUN_AT_ENTITY(FBICop[0], PLAYER_PED_ID(), INFINITE_TASK_TIME)
//					ENDIF
//					IF NOT IS_PED_INJURED(FBICop[1])
//						CLEAR_PED_TASKS(FBICop[1])
//						TASK_AIM_GUN_AT_ENTITY(FBICop[1], PLAYER_PED_ID(), INFINITE_TASK_TIME)
//					ENDIF
//					IF NOT IS_PED_INJURED(FBICop[2])
//						CLEAR_PED_TASKS(FBICop[2])
//						TASK_AIM_GUN_AT_ENTITY(FBICop[2], PLAYER_PED_ID(), INFINITE_TASK_TIME)
//					ENDIF
//					copsAimingAtPlayer = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//
//ENDPROC


PROC checkForPlayerInputThenClearTAsks()

	INT padLX, padLy, padRX, padRy

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(padLX, padLy, padRX, padRy)

	IF ABSI(padLX) > 75
	OR ABSI(padLY) > 75
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
		IF bPlayerHasTakenControl = FALSE
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			bPlayerHasTakenControl = TRUE
		ENDIF
	ENDIF

ENDPROC


////PURPOSE: Stage kill the cops and head off in the Austin Martin.
//PROC DO_STAGE_KILL_COPS()
//
//
//	IF iControlFlag = 0
//		
//		IF MissionStageBeingSkippedTo = TRUE
//			STAGE_SELECTOR_MISSION_SETUP()
//		ENDIF			
//		
////**************RESET FLAGS*************************
//		doneChat1 					= FALSE
//		doneChat2 					= FALSE
//		doneChat3 					= FALSE
//		playerarmed					= FALSE
//		doneGetInCarText 			= FALSE
//		GameCanFail 				= TRUE
////		CheckPlayerCarHealthForFail = TRUE
//		copsAimingAtPlayer 			= FALSE
//		CopTasksGiven 				= FALSE
//		doneWarningText				= FALSE
//		bPlayerHasTakenControl		= FALSE
////		RunAwayPedTaskGiven[0]	 	= FALSE
////		RunAwayPedTaskGiven[1] 		= FALSE
//		wantedLevelSetTo2 			= FALSE
//		
//		FOR icount = 0 TO 2
//			copdead[icount] 		= FALSE
//		ENDFOR
//		
////		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "STAGE_KILL_COPS")
//		
//		//Create enemy car and driver
//		IF NOT DOES_ENTITY_EXIST(EnemyCar)
//			REQUEST_MODEL(COGCABRIO)
//			WHILE NOT HAS_MODEL_LOADED(COGCABRIO)
//				PRINTSTRING("Waiting on cogcabrio loading") PRINTNL()
//				WAIT(0)
//			ENDWHILE
//			EnemyCar = CREATE_VEHICLE(COGCABRIO, << -428.0226, -2153.5579, 9.2992 >>, 90.9098)
//			SET_ENTITY_PROOFS(EnemyCar, FALSE, TRUE, TRUE, TRUE, FALSE)
//			SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
//			SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
//		ENDIF
//		IF NOT DOES_ENTITY_EXIST(EnemyPed)
//			REQUEST_MODEL(IG_MOLLY)
//			WHILE NOT HAS_MODEL_LOADED(IG_MOLLY)
//				PRINTSTRING("Waiting on IG_MOLLY loading") PRINTNL()
//				WAIT(0)
//			ENDWHILE			
//			IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
//				EnemyPed = CREATE_PED_INSIDE_VEHICLE(EnemyCar, PEDTYPE_MISSION, IG_MOLLY)
////				GIVE_WEAPON_TO_PED(EnemyPed, WEAPONTYPE_PISTOL, 100, TRUE)
//				SET_ENTITY_INVINCIBLE(EnemyPed, TRUE)
//				SET_PED_CAN_BE_TARGETTED(EnemyPed, FALSE)
//				SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
//				SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
//				SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed, FriendGroup)
//			ENDIF
//		ENDIF		
//
//
//		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//			IF NOT DOES_BLIP_EXIST(playerCarBlip)
//				playerCarBlip = ADD_BLIP_FOR_ENTITY(PlayerCar)
//				SET_BLIP_AS_FRIENDLY(playerCarBlip, TRUE)
//				PRINT_NOW("CMN_GENGETIN", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get in the ~b~car.
//			ENDIF
//		ENDIF
//		
//		MissionFailTimer = GET_GAME_TIMER()	
//		
//		IF NOT IS_SCREEN_FADED_IN()
//			LOAD_SCENE(PlayerCarStartingCoords)
//			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//		ENDIF
//		
//		//Set the max wanted level to zero at start.
//		SET_MAX_WANTED_LEVEL(0)
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//		
//		//Run this every frame to check for player taking control.
//		checkForPlayerInputThenClearTAsks()
//		
////		playerscoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)
//		
//		Handle_Dialogue_And_God_Text()
//		
//		SCRIPT_SHOOTOUT()
//			
////		Handle_Run_Away_Ped()	
//			
//		//Move stage on if player gets in his car	
//		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//				SET_ENTITY_HEALTH(PlayerCar, 3000)
//				SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, TRUE, FALSE, FALSE)
//				SET_VEHICLE_TYRES_CAN_BURST(PlayerCar, FALSE)
//				iControlFlag = 2
//			ENDIF
//		ELSE
//			IF DOES_BLIP_EXIST(playerCarBlip)
//				REMOVE_BLIP(playerCarBlip)
//				CLEAR_PRINTS()
//				PRINT_NOW("TRV4_CAR1", DEFAULT_GOD_TEXT_TIME, 1) //~s~Get in a vehicle.
//			ENDIF
//		ENDIF
//		
//		//Move stage on if player gets in any other car
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			PlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//			SET_ENTITY_HEALTH(PlayerCar, 3000)
//			SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, TRUE, FALSE, FALSE)
//			SET_VEHICLE_TYRES_CAN_BURST(PlayerCar, FALSE)
//			iControlFlag = 2
//		ENDIF
//		
//		//Check for player leaving the house. Give player a limited amount of time before he has to leave this area before failing mission.
//		IF doneWarningText = FALSE
//			IF GET_GAME_TIMER() > (MissionFailTimer + 45000)
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//					CLEAR_PRINTS()
//					PRINT_NOW("CMN_GENGETIN", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get in the car
//					MissionFailTimer = GET_GAME_TIMER()					
//					doneWarningText = TRUE
//				ENDIF
//			ENDIF
//		ELSE
//			//Fail mission if player still hasn't got in a vehicle
//			IF GET_GAME_TIMER() > (MissionFailTimer + 15000)
//				SetMissionFailed("TRV4_FAIL1") //~r~You lost the Translator.
//			ENDIF
//		ENDIF
//		
//		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//			IF GET_DISTANCE_BETWEEN_ENTITIES(PlayerCar, PLAYER_PED_ID()) > 100
//				iControlFlag = 3
//			ENDIF
//		ENDIF
//		
//	ENDIF
//	
//	IF iControlFlag = 2
//		
//		Handle_Dialogue_And_God_Text()	
//	
//		IF NOT IS_PED_INJURED(FBICop[0])
//			TASK_COMBAT_PED(FBICop[0], PLAYER_PED_ID())
//		ENDIF
//		IF NOT IS_PED_INJURED(FBICop[1])
//			TASK_COMBAT_PED(FBICop[1], PLAYER_PED_ID())
//		ENDIF
//		IF NOT IS_PED_INJURED(FBICop[2])
//			TASK_COMBAT_PED(FBICop[2], PLAYER_PED_ID())
//		ENDIF
//	
//		IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//				
//				IF PlayersCarAddedToVehicleUpSideDownCheck = FALSE
//					ADD_VEHICLE_UPSIDEDOWN_CHECK(PlayerCar)
//					PlayersCarAddedToVehicleUpSideDownCheck = TRUE
//				ENDIF
//				
//				IF DOES_BLIP_EXIST(playerCarBlip)
//					REMOVE_BLIP(playerCarBlip)
//				ENDIF
//				
//				IF NOT DOES_BLIP_EXIST(airportBlip)
//					airportBlip = CREATE_BLIP_FOR_COORD(<<-498.7, -2136.5, 8.4>>)
//					START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
//						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-244, -1842.74, 28.48>>)
//						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1.5, -2014.4, 11.5>>)
//						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-498.7, -2136.5, 8.4>>)
//					SET_GPS_MULTI_ROUTE_RENDER(TRUE)
//					IF doneGetInCarText = FALSE
//						IF NOT IS_MESSAGE_BEING_DISPLAYED()
//							PRINT("TRV4_GO1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Head to the ~y~airport ~s~and find the translator.
//							doneGetInCarText = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//				iControlFlag = 0
//				missionStage = STAGE_GET_TO_CHASE
//			
//			ENDIF
//		ENDIF
//	
//	ENDIF
//	
//	IF iControlFlag = 3
//		
//		IF DOES_BLIP_EXIST(playerCarBlip)
//			REMOVE_BLIP(playerCarBlip)
//		ENDIF		
//		
//		IF NOT DOES_BLIP_EXIST(airportBlip)
//			airportBlip = CREATE_BLIP_FOR_COORD(<<-498.7, -2136.5, 8.4>>)
//			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
//				ADD_POINT_TO_GPS_MULTI_ROUTE(<<-244, -1842.74, 28.48>>)
//				ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1.5, -2014.4, 11.5>>)
//				ADD_POINT_TO_GPS_MULTI_ROUTE(<<-426.88, -2153.52, 9.77>>)
//			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
//			IF doneGetInCarText = FALSE
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
//					PRINT("TRV4_GO1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Head to the ~y~airport ~s~and find the translator.
//					doneGetInCarText = TRUE
//				ENDIF
//			ENDIF
//		ENDIF	
//		
//		iControlFlag = 0
//		missionStage = STAGE_GET_TO_CHASE
//	
//	ENDIF
//ENDPROC

PROC MAKE_SURE_EVERYTHING_IS_LOADED()

		//Other assets
//		WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie)
//		OR NOT HAS_CAM_RECORDING_LOADED(1, "BB_Chase")
//			PRINTSTRING("Waiting on Assets to load icontrol flag = 0") PRINTNL()
//			WAIT(0)
//		ENDWHILE		
		
		//Models
		WHILE NOT HAS_MODEL_LOADED(POLICE3)
		OR NOT HAS_MODEL_LOADED(MAVERICK)
		OR NOT HAS_MODEL_LOADED(BISON)
		OR NOT HAS_MODEL_LOADED(BLISTA)
			PRINTSTRING("Waiting on models to load icontrol flag = 0") PRINTNL()
			WAIT(0)
		ENDWHILE		
		
		//Car recordings
		WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(45, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(35, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(54, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(55, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(67, "BB_Chase")
			WAIT(0)
			PRINTSTRING("Waiting on Vehicle recording 67 to load icontrol flag = 0") PRINTNL()
		ENDWHILE	


ENDPROC

//PURPOSE: Stage where MICHAEL drives to meet the bad guy.
PROC DO_STAGE_GET_TO_CHASE()
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(key_9)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<3.6219, -2073.8535, 9.3513>>)
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 178.0302)
			ENDIF
		ENDIF
	#ENDIF

	IF iControlFlag = 0
			
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 14.0, REPLAY_IMPORTANCE_HIGHEST)
			
		#IF IS_DEBUG_BUILD
			PRINTSTRING("STAGE_GET_TO_CHASE") PRINTNL()
		#ENDIF
			
		//STAGE SELECTOR FOR REPLAY CHECKPOINT and debug skipping
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF			
			
//**********************************************RESET FLAGS*********************************************
		playerComingFromWrongSide		= FALSE
		checkForEnemyPedDeath			= FALSE
		nextStageStuffRequested 		= FALSE
		donechat7 						= FALSE
		doneWarningText 				= FALSE
		setpiecePlane1Setup             = FALSE
		setpiecePlane2Setup				= FALSE
//		enginefx1done					= FALSE
//		enginefx2done					= FALSE
//		enginefx3done					= FALSE
//		enginefx4done					= FALSE
//		roadBlockCopsCreated			= FALSE
		Car11HasBeenBlasted				= FALSE
//		Car10Blasted					= FALSE
		FiremenSetup					= FALSE
		trailerdetached					= FALSE
		has_cam_been_created			= FALSE
		heli_chase_cam_active			= FALSE	
		b_heliCamHelp					= FALSE
		bIsCutSceneRunning				= FALSE
		doneChaseAfterBadGuyText 		= FALSE
		distanceCheckRequired 			= FALSE
		doneGetInCarText 				= FALSE
		MusicTriggered 					= FALSE
		GpsRouteTurnedOn 				= FALSE
		doneDevinPhoneCall 				= FALSE
		donePoliceReport 				= FALSE
		GameCanFail						= TRUE
		iSubStage 						= 0
		
		#IF IS_DEBUG_BUILD
			jSkipReady 						= TRUE
		#ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			entityProofsNeedsSet 	= TRUE
		ELSE
			entityProofsNeedsSet 	= FALSE
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")					
		
		SET_IGNORE_NO_GPS_FLAG(TRUE)
		
		//close roads at start of chase
		SET_ROADS_IN_AREA(<<-617, -2274, 0.0>>, <<-406.01, -2044, 15.0>>, FALSE)		
		
		MissionFailTimer = GET_GAME_TIMER()
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP()
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar2)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar3)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF			
		
		IF NOT DOES_BLIP_EXIST(airportBlip)
			airportBlip = CREATE_BLIP_FOR_COORD(vAirportBlip)
		ENDIF			
		
		TRIGGER_MUSIC_EVENT("TRV4_GAMEPLAY_START")
		
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
		
		IF doneGetInCarText = FALSE
		AND donechat7 = TRUE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
				PRINT_NOW("TRV4_GO1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to the ~y~airport ~s~and find Molly.
				doneGetInCarText = TRUE
			ENDIF
		ENDIF		
		
		//Do police scanner when player gets close to the start of the chase
		IF donePoliceReport = FALSE
		AND NOT IS_CELLPHONE_CONVERSATION_PLAYING()
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAirportBlip) < 800
				RandomInt = GET_RANDOM_INT_IN_RANGE(0, 2)
				IF RandomInt = 0
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_Sol_3_01", 0.0)
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0, REPLAY_IMPORTANCE_HIGH)
					
					donePoliceReport = TRUE
				ENDIF
				IF RandomInt = 1
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_Sol_3_02", 0.0)
					
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0, REPLAY_IMPORTANCE_HIGH)
					
					donePoliceReport = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Create enemy car and driver.
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAirportBlip) < 600
			IF NOT DOES_ENTITY_EXIST(EnemyCar)
				REQUEST_MODEL(COGCABRIO)
				IF HAS_MODEL_LOADED(COGCABRIO)
					EnemyCar = CREATE_VEHICLE(COGCABRIO, << -428.0226, -2153.5579, 9.2992 >>, 90.9098)
					SET_ENTITY_PROOFS(EnemyCar, FALSE, TRUE, TRUE, TRUE, FALSE)
					SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
					SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyCar, "SOL_3_MOLLY_CAR_Group")
				ELSE
					PRINTSTRING("Waiting on cogcabrio loading") PRINTNL()
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(EnemyPed)
				REQUEST_MODEL(IG_MOLLY)
				IF HAS_MODEL_LOADED(IG_MOLLY)	
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							EnemyPed = CREATE_PED_INSIDE_VEHICLE(EnemyCar, PEDTYPE_MISSION, IG_MOLLY)
							SET_ENTITY_INVINCIBLE(EnemyPed, TRUE)
							SET_PED_CAN_BE_TARGETTED(EnemyPed, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
							SET_MODEL_AS_NO_LONGER_NEEDED(IG_MOLLY)
							SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed, RELGROUPHASH_PLAYER)
							checkForEnemyPedDeath = TRUE
						ENDIF
					ENDIF
				ELSE
					PRINTSTRING("Waiting on IG_MOLLY loading") PRINTNL()	
			 	ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(EnemyPed)
				DELETE_PED(EnemyPed)
			ENDIF
			IF DOES_ENTITY_EXIST(EnemyCar)
				DELETE_VEHICLE(EnemyCar)
			ENDIF
		ENDIF
		//request everything for start of uber chase
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vAirportBlip) < 500
			IF nextStageStuffRequested = FALSE
				//Request models and recordings for uber chase.
				ScaleformMovie = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
				REQUEST_MODEL(POLICE3)
				REQUEST_MODEL(MAVERICK)
				REQUEST_MODEL(BISON)
				REQUEST_MODEL(BLISTA)
				REQUEST_VEHICLE_RECORDING(45, "BB_Chase")
				REQUEST_VEHICLE_RECORDING(35, "BB_Chase")
				REQUEST_VEHICLE_RECORDING(2, "BB_Chase")
				REQUEST_VEHICLE_RECORDING(54, "BB_Chase")
				REQUEST_VEHICLE_RECORDING(55, "BB_Chase")	
				REQUEST_VEHICLE_RECORDING(67, "BB_Chase")
				REQUEST_CAM_RECORDING(1, "BB_Chase")
				PREPARE_MUSIC_EVENT("TRV4_CHASE")
				nextStageStuffRequested = TRUE
			ENDIF	
		ELSE
			IF nextStageStuffRequested = TRUE
				SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
				SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
				SET_MODEL_AS_NO_LONGER_NEEDED(BISON)
				SET_MODEL_AS_NO_LONGER_NEEDED(BLISTA)
				REMOVE_VEHICLE_RECORDING(45, "BB_Chase")
				REMOVE_VEHICLE_RECORDING(35, "BB_Chase")
				REMOVE_VEHICLE_RECORDING(2, "BB_Chase")
				REMOVE_VEHICLE_RECORDING(54, "BB_Chase")
				REMOVE_VEHICLE_RECORDING(55, "BB_Chase")
				REMOVE_VEHICLE_RECORDING(67, "BB_Chase")
				REMOVE_CAM_RECORDING(1, "BB_Chase")
				CANCEL_MUSIC_EVENT("TRV4_CHASE")
				nextStageStuffRequested = FALSE
			ENDIF
		ENDIF
		
		//Give player a limited amount of time before he has to have caught up with the Translator.
		IF doneWarningText = FALSE
			IF GET_GAME_TIMER() > (MissionFailTimer + 135000)
				CLEAR_PRINTS()
				PRINT_NOW("TRV4_WARN1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Molly is getting away.
				MissionFailTimer = GET_GAME_TIMER()					
				doneWarningText = TRUE
			ENDIF
		ENDIF
		IF doneWarningText = TRUE
			//Fail mission if player takes too long to get to the Translator
			IF GET_GAME_TIMER() > (MissionFailTimer + 135000)
				SetMissionFailed("TRV4_FAIL1") //~r~You lost the translator.
			ENDIF
		ENDIF		
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_DRIVE_TO_AIRPORT")
				START_AUDIO_SCENE("SOL_3_DRIVE_TO_AIRPORT")
			ENDIF
			IF MusicTriggered = FALSE
				TRIGGER_MUSIC_EVENT("TRV4_CAR_ENTERED")
				MusicTriggered = TRUE
			ENDIF
			IF NOT DOES_ENTITY_EXIST(PlayerCar)
				PlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				SET_ENTITY_PROOFS(PlayerCar, FALSE, TRUE, FALSE, FALSE, FALSE)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(PlayerCar)
				SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
			ENDIF	
		ELSE
			IF DOES_ENTITY_EXIST(PlayerCar)
				IF IS_VEHICLE_DRIVEABLE(PlayerCar)
					IF entityProofsNeedsSet = FALSE
						SET_ENTITY_PROOFS(PlayerCar, FALSE, FALSE, FALSE, FALSE, FALSE)
						entityProofsNeedsSet = TRUE
					ENDIF
					IF PlayersCarAddedToVehicleUpSideDownCheck = TRUE
						REMOVE_VEHICLE_UPSIDEDOWN_CHECK(PlayerCar)
						PlayersCarAddedToVehicleUpSideDownCheck = FALSE
					ENDIF
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(PlayerCar)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		//Handle GPS multi route.
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GpsRouteTurnedOn = FALSE
				START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
					ADD_POINT_TO_GPS_MULTI_ROUTE(<<-244, -1842.74, 28.48>>)
					ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1.5, -2014.4, 11.5>>)
					ADD_POINT_TO_GPS_MULTI_ROUTE(<<-498.7, -2136.5, 8.4>>)
				SET_GPS_MULTI_ROUTE_RENDER(TRUE)
				GpsRouteTurnedOn = TRUE
			ENDIF
		ELSE
			IF GpsRouteTurnedOn = TRUE
				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
				GpsRouteTurnedOn = FALSE
			ENDIF
		ENDIF			
		
		//MOVE STAGE ON AND START UBER PLAYBACK IF ANY OF THE FOLLOWING OCCUR
		
		IF nextStageStuffRequested = TRUE	
		AND DOES_ENTITY_EXIST(EnemyCar)
		AND DOES_ENTITY_EXIST(EnemyPed)
			
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(EnemyCar, GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()))
					MAKE_SURE_EVERYTHING_IS_LOADED()
					iControlFlag = 0
					icutsceneStage = 0
					missionStage = STAGE_CHASE_START
				ENDIF
			ENDIF		
			
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), EnemyCar)
					MAKE_SURE_EVERYTHING_IS_LOADED()
					iControlFlag = 0
					icutsceneStage = 0
					missionStage = STAGE_CHASE_START
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(EnemyCar), <<200, 132, 100>>)
					
					playerComingFromWrongSide = FALSE
					
					MAKE_SURE_EVERYTHING_IS_LOADED()
					iControlFlag = 0
					icutsceneStage = 0
					missionStage = STAGE_CHASE_START
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-577, -2051.5, 7>>, <<84, 87.5, 15>>)
					
					playerComingFromWrongSide = TRUE
					
					MAKE_SURE_EVERYTHING_IS_LOADED()
					iControlFlag = 0
					icutsceneStage = 0
					missionStage = STAGE_CHASE_START
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-703.5, -1969.5, 10>>, <<53.5, 62.5, 10>>)
					
					//Turn off GPS route now
					CLEAR_GPS_MULTI_ROUTE()
					SET_IGNORE_NO_GPS_FLAG(FALSE)
					playerComingFromWrongSide = TRUE
					
					MAKE_SURE_EVERYTHING_IS_LOADED()
					iControlFlag = 0
					icutsceneStage = 0
					missionStage = STAGE_CHASE_START
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
		
ENDPROC

PROC CONTROL_OUTSIDE_COPS()
	
	IF CopIdsGrabbed = FALSE
		IF DOES_ENTITY_EXIST(SetPieceCarID[14])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[14])
				Cop14 = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[14])
				GIVE_WEAPON_TO_PED(Cop14, WEAPONTYPE_PISTOL, 1000, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(Cop14)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Cop14, TRUE)
				SET_PED_ACCURACY(Cop14, 5)
				SET_ENTITY_HEALTH(Cop14, 150)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, Cop14, "TREV4POLICE3")
			ENDIF
		ENDIF
		CopIdsGrabbed = TRUE
	ELSE
		IF Cop14TaskGiven = FALSE
			IF DOES_ENTITY_EXIST(Cop14)
				IF NOT IS_PED_INJURED(Cop14)
					CLEAR_PED_TASKS(Cop14)
					SET_PED_SPHERE_DEFENSIVE_AREA(Cop14, GET_ENTITY_COORDS(Cop14), 5)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_SEEK_COVER_FROM_PED(NULL, PLAYER_PED_ID(), 5000, TRUE)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(Cop14, seq)
					CLEAR_SEQUENCE_TASK(seq)
					Cop14TaskGiven = TRUE
				ENDIF
			ENDIF
		ELSE
			IF DonePoliceChat = FALSE
				IF DOES_ENTITY_EXIST(Cop14)
					IF NOT IS_PED_INJURED(Cop14)
						IF NOT IS_PED_IN_ANY_VEHICLE(Cop14)
						AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Cop14) < 20
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, Cop14, "SOL3COP")
//									IF CREATE_CONVERSATION(MyLocalPedStruct,"T1M4AUD", "T1M4_POLICE", CONV_PRIORITY_MEDIUM)
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_MOLPOL", "SOL3_MOLPOL_2", CONV_PRIORITY_MEDIUM)
										//Both of you, on the ground.
										DonePoliceChat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//Starts the playback of the uberrecoring.
PROC DO_STAGE_CHASE_START()

	IF iControlFlag = 0
		
//		//AUDIO assets for cop cars being blown up / flying through the air - needed throughout the chase
//		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Cop_Cars")
//			PRINTSTRING("LOADING Trv_4_747_Cop_Cars")
//			PRINTNL()
//			WAIT(0)
//		ENDWHILE
//		//AUDIO assets for the small prop plane hitting the cop car - can unload after crash
//		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Plane_Explosion")
//			PRINTSTRING("LOADING Trv_4_747_Plane_Explosion")
//			PRINTNL()
//			WAIT(0)
//		ENDWHILE
//		//AUDIO assets for the tanker exploding - can unload after the tanker explodes
//		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Tanker_Explosion")
//			PRINTSTRING("LOADING Trv_4_747_Tanker_Explosion")
//			PRINTNL()
//			WAIT(0)
//		ENDWHILE
//		//AUDIO Tv sounds for the view from the chopper
//		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TV_Monitors")
//			PRINTSTRING("LOADING SCRIPT\\TV_Monitors")
//			PRINTNL()
//			WAIT(0)
//		ENDWHILE	
		
		//AUDIO assets for cop cars being blown up / flying through the air - needed throughout the chase
		REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Cop_Cars")
		//AUDIO assets for the small prop plane hitting the cop car - can unload after crash
	 	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Plane_Explosion")
		//AUDIO assets for the tanker exploding - can unload after the tanker explodes
		REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Tanker_Explosion")
		//AUDIO Tv sounds for the view from the chopper
		REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\TV_Monitors")		
		
		IF CarExplodeSoundID1Created = FALSE
			CarExplodeSoundID1 	= GET_SOUND_ID()
			CarExplodeSoundID1Created = TRUE
		ENDIF
		IF CarExplodeSoundID2Created = FALSE
			CarExplodeSoundID2 	= GET_SOUND_ID()
			CarExplodeSoundID2Created = TRUE
		ENDIF
		IF CarExplodeSoundID3Created = FALSE
			CarExplodeSoundID3 	= GET_SOUND_ID()
			CarExplodeSoundID3Created = TRUE
		ENDIF
		IF CarExplodeSoundID4Created = FALSE
			CarExplodeSoundID4 	= GET_SOUND_ID()
			CarExplodeSoundID4Created = TRUE
		ENDIF
		IF loudFireSoundIDCreated = FALSE
			loudFireSoundID 	= GET_SOUND_ID()
			loudFireSoundIDCreated = TRUE
		ENDIF
		IF PropPlaneSoundIDCreated = FALSE
			PropPlaneSoundID 	= GET_SOUND_ID()
			PropPlaneSoundIDCreated = TRUE
		ENDIF
		IF NewsHeliSoundIDCreated = FALSE
			NewsHeliSoundID 	= GET_SOUND_ID()		
			NewsHeliSoundIDCreated = TRUE
		ENDIF
		
		//STAGE SELECTOR FOR REPLAY CHECKPOINT
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF
		
		IF NOT DOES_CAM_EXIST(heliChaseCam)
			heliChaseCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			SET_CAM_FOV(heliChaseCam, 40)
		ENDIF
		
		//Flags
		doneChaseAfterBadGuyText 	= FALSE
		b_heliCamHelp 				= FALSE
		camrecordingstarted 		= FALSE
		FiremenSetup 				= FALSE
//		doneChat6 					= FALSE
//		doneChat5 					= FALSE
		setpiecePlane2Setup 		= FALSE
		trailerdetached 			= FALSE
		setpiecePlane1Setup 		= FALSE
//		roadBlockCopsCreated 		= FALSE
		vehicleColoursSet 			= FALSE
//		hintcamHelpTextDone 		= FALSE
//		doneChat8	 				= FALSE
//		doneChat10 					= FALSE
//		doneChat11 					= FALSE
//		doneChat12 					= FALSE
		AudioSceneStarted[0] 		= FALSE
		AudioSceneStopped[0] 		= FALSE
		AudioSceneStarted[1] 		= FALSE
		AudioSceneStopped[1] 		= FALSE
		AudioSceneStarted[2] 		= FALSE

		CarExplodeSoundID2Released 	= FALSE
		timeScale1BackToNormal 		= FALSE
//		timeScale2BackToNormal 		= FALSE
		Car11HasBeenBlasted 		= FALSE
		Car12HasBeenBlasted			= FALSE
		Car8HasBeenBlasted			= FALSE
//		Car10Blasted				= FALSE
		playbackspeedChangedForTruck = FALSE
		playbackStoppedForTruck 	= FALSE
		propPlaneSoundReleased 		= FALSE
		CarExplodeSoundID3Released 	= FALSE
		CarExplodeSoundID4Released	= FALSE
		CarExplodeSoundID1Released 	= FALSE
		CarExplodeSoundID1Played 	= FALSE
		CarExplodeSoundID3Played 	= FALSE
		CarExplodeSoundID4Played	= FALSE
		loudFireSoundIDPlayed 		= FALSE
		PropPlaneSoundIDPlayed 		= FALSE
 		CarExplodeSoundID2Played 	= FALSE
		pedsCreated 				= FALSE
		DivingPedsAnimsDone 		= FALSE
		nextStageAssetsRequested 	= FALSE
		helisBladesSetup 			= FALSE
//		AIPoliceTasksGiven 			= FALSE
		AIPoliceSetup 				= FALSE
//		collisionSetForCar9 		= FALSE
		haveCarAndTrailerBeenExploded = FALSE
		hornSoundStarted 			= FALSE
		
		GameCanFail 				= TRUE
		distanceCheckRequired 		= TRUE
		checkForEnemyPedDeath		= TRUE	
		MusicPrepared 				= FALSE
		MusicTriggered2 			= FALSE
		EnemyPedTaskGiven 			= FALSE
		enemyPedWaypointTask 		= FALSE
		ShortCutActive 				= FALSE
		ShortCutDone 				= FALSE
		planeSetInvincible 			= FALSE
		AIChaseTaskGiven[0] 		= FALSE
		AIChaseTaskGiven[1] 		= FALSE
		AIChaseBehavioursSet[0] 	= FALSE
		AIChaseBehavioursSet[1] 	= FALSE
		car13Blasted 				= FALSE
		trailerCreated 				= FALSE
		DonePoliceChat 				= FALSE
		playerHasGotCloseEnough		= FALSE
		vehicleUpsideDownTimerSet 	= FALSE
		wantedLevelAirport 			= FALSE
		slowMoStopped 				= FALSE
		slowMoStarted				= FALSE
		doorSoundPlayed 			= FALSE
		iSubStage					= 0
		
		ivehicleUpsideDownTimer		= 0
		
		FOR icount = 0 TO 2
			firemanCowerTaskSet[icount] = FALSE
			FirstAnimStarted[icount] 	= FALSE
		ENDFOR
		
		FOR icount = 0 TO 2
			bVideoRecorded[icount]				= FALSE
		ENDFOR
		
		FOR icount = 0 TO 6
			PoliceCarAddedToAudio[icount] 		= FALSE
			PoliceCarRemovedFromAudio[icount]	= FALSE
		ENDFOR
	
		REQUEST_PTFX_ASSET()
		
		//Load in the uber recording
		//Set flags for uber recording. 
		// Setting this to true will spawn all set piece cars if they're on screen or not.
		bCreateAllWaitingCars 		= TRUE
		//Set this to true to stop set pieces switching to AI
		bSetPieceCarsDontSwitchToAI = TRUE
		
		#IF IS_DEBUG_BUILD
			set_uber_parent_widget_group(Airport_Chase_Widget_Group)
		#endif
		
		//load overlay and get script render target for news heli textures.
		LOAD_OVERLAY_EFFECTS()
		screenRT = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
		
		//Remove peds from here as they get in the way of the chase.
		CLEAR_AREA(<<-823.71, -2548.20, 13.75>>, 20, TRUE)
		SET_PED_NON_CREATION_AREA(<<-833.86, -2564.17, 13.43>>, <<-814.37, -2521.04, 12.94>>)
		SET_PED_PATHS_IN_AREA(<<-833.86, -2564.17, 13.43>>, <<-814.37, -2521.04, 12.94>>, FALSE)
		
		//Shut off roads to normal traffic now for airport chase.
		SET_ROADS_IN_AREA(<<-482, -2118, 6>>, <<-1072, -2724, 17>>, FALSE)
		
		SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE)
		
		INITIALISE_UBER_PLAYBACK("BB_Chase", 45, FALSE)	
		
//		SET_FAKE_WANTED_LEVEL(GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
		
//		RSinteriorGroup = GET_INTERIOR_AT_COORDS(<<-937.1, -2969.5, 15>>)
		
		SET_MAX_WANTED_LEVEL(3)
		
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(PlayerCar)
		ENDIF	
		
		ChaseStage = 0
		
		iControlFlag = 1
		
	ENDIF

	IF iControlFlag = 1
		PRINTSTRING("icontrolflag = 1") PRINTNL()
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		FILL_UBER_RECORDING2()
		iControlFlag = 2
	ENDIF
	
	IF iControlFlag = 2
		PRINTSTRING("icontrolflag = 2") PRINTNL()
		IF NOT DOES_ENTITY_EXIST(VehCam)
			VehCam = CREATE_VEHICLE(BLISTA, <<0,0,1>>, 0 )
		ENDIF
	
		WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(45, "BB_Chase")
			PRINTSTRING("Waiting on 45, BB_Chase loading") PRINTNL()
			WAIT(0)
		ENDWHILE		
	
		IF IS_SCREEN_FADED_OUT()
			IF IS_VEHICLE_DRIVEABLE(PlayerCar)
				SET_VEHICLE_ENGINE_ON(PlayerCar, TRUE, TRUE)
				SET_VEHICLE_FORWARD_SPEED(PlayerCar, 30)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
					
		IF DOES_ENTITY_EXIST(EnemyCar)
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				START_PLAYBACK_RECORDED_VEHICLE(EnemyCar, 45, "BB_Chase")
				SETTIMERB(0)
//				DONT_LET_UBER_PLAYBACK_CLEANUP_THIS_CAR(EnemyCar)
				CREATE_ALL_WAITING_UBER_CARS()
				
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				
				IF DOES_ENTITY_EXIST(PlayerCar)
					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
					ENDIF
				ENDIF
				
				IF NOT IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				iControlFlag = 0
				missionStage = STAGE_MAIN_CHASE
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEANUP_MEMORY_THROUGHOUT_CHASE()
	
	VECTOR setpiece3Coords, setpiece4Coords, setpiece21Coords
	
	//Clean up the 2 cars at start once player has passed them
	IF DOES_ENTITY_EXIST(SetPieceCarID[4])
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[4])	
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[4])
				playerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				setpiece4Coords = GET_ENTITY_COORDS(SetPieceCarID[4])
				IF playerCoords.x < (setpiece4Coords.x - 10)
				AND NOT IS_ENTITY_ON_SCREEN(SetPieceCarID[4])
					STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[4])
					SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPieceCarID[4])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF DOES_ENTITY_EXIST(SetPieceCarID[3])
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[3])	
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[3])
				playerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				setpiece3Coords = GET_ENTITY_COORDS(SetPieceCarID[3])
				IF playerCoords.x < (setpiece3Coords.x - 10)
				AND NOT IS_ENTITY_ON_SCREEN(SetPieceCarID[3])
					STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[3])
					SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPieceCarID[3])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Clean up the bus
	IF DOES_ENTITY_EXIST(SetPieceCarID[21])
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[21])	
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[21])
				playerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				setpiece21Coords = GET_ENTITY_COORDS(SetPieceCarID[21])
				IF playerCoords.x < (setpiece21Coords.x - 10)
				AND NOT IS_ENTITY_ON_SCREEN(SetPieceCarID[21])
					STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[21])
					SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPieceCarID[21])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//clean up the buzzard
	IF DOES_ENTITY_EXIST(SetPieceCarID[0])
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[0])
			IF DOES_ENTITY_EXIST(EnemyCar)
				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50000	
						AND NOT IS_ENTITY_ON_SCREEN(SetPieceCarID[0])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[0])
								STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[0])
							ENDIF
							SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPieceCarID[0])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Clean up fire scene
	IF DOES_ENTITY_EXIST(FireEngine)
		IF IS_VEHICLE_DRIVEABLE(FireEngine)
			IF DOES_ENTITY_EXIST(EnemyCar)
				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 90000	
						AND NOT IS_ENTITY_ON_SCREEN(FireEngine)
							STOP_PARTICLE_FX_LOOPED(C4[0])
							STOP_PARTICLE_FX_LOOPED(C4[1])
							STOP_PARTICLE_FX_LOOPED(C4[2])
							IF DOES_ENTITY_EXIST(Fireman[0])
								SET_PED_AS_NO_LONGER_NEEDED(Fireman[0])
							ENDIF
							IF DOES_ENTITY_EXIST(Fireman[1])
								SET_PED_AS_NO_LONGER_NEEDED(Fireman[1])
							ENDIF
							IF DOES_ENTITY_EXIST(Fireman[2])
								SET_PED_AS_NO_LONGER_NEEDED(Fireman[2])
							ENDIF
							IF DOES_ENTITY_EXIST(Fireman[3])
								SET_PED_AS_NO_LONGER_NEEDED(Fireman[3])
							ENDIF
							SET_VEHICLE_AS_NO_LONGER_NEEDED(FireEngine)
							REMOVE_ANIM_DICT("missheist_agency3aig_lift_waitped_a")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Handles all the set peice plane engines to make sure they are on to create the sound effects
PROC PLANE_ENGINE_CONTROLLER()

	IF DOES_ENTITY_EXIST(SetPieceCarID[2])
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[2])
			IF DOES_ENTITY_EXIST(EnemyCar)
				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 50530
							IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(SetPieceCarID[2])
								SET_VEHICLE_ENGINE_ON(SetPieceCarID[2], TRUE, TRUE)
							ENDIF
						ELSE
							IF GET_IS_VEHICLE_ENGINE_RUNNING(SetPieceCarID[2])
								SET_VEHICLE_ENGINE_ON(SetPieceCarID[2], FALSE, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPieceCarID[5])
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[5])
			IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(SetPieceCarID[5])
				SET_VEHICLE_ENGINE_ON(SetPieceCarID[5], TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPiecePlane[0])
		IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[0])
			IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(SetPiecePlane[0])
				SET_VEHICLE_ENGINE_ON(SetPiecePlane[0], TRUE, TRUE)
			ENDIF
			IF IS_VEHICLE_SEAT_FREE(SetPiecePlane[0])
				IF NOT DOES_ENTITY_EXIST(setPiecePlanePilot[0])
					REQUEST_MODEL(S_M_M_GENTRANSPORT)
					IF HAS_MODEL_LOADED(S_M_M_GENTRANSPORT)
						setPiecePlanePilot[0] = CREATE_PED_INSIDE_VEHICLE(SetPiecePlane[0], PEDTYPE_MISSION, S_M_M_GENTRANSPORT)
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GENTRANSPORT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(SetPiecePlane[1])
		IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[1])
			IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(SetPiecePlane[1])
				SET_VEHICLE_ENGINE_ON(SetPiecePlane[1], TRUE, TRUE)
			ENDIF
			IF IS_VEHICLE_SEAT_FREE(SetPiecePlane[1])
				IF NOT DOES_ENTITY_EXIST(setPiecePlanePilot[1])
					REQUEST_MODEL(S_M_M_GENTRANSPORT)
					IF HAS_MODEL_LOADED(S_M_M_GENTRANSPORT)
						setPiecePlanePilot[1] = CREATE_PED_INSIDE_VEHICLE(SetPiecePlane[1], PEDTYPE_MISSION, S_M_M_GENTRANSPORT)
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GENTRANSPORT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC
	
PROC DO_STAGE_MAIN_CHASE()
	
	//Debug for skipping to end of stage to set up end of chase SHOULD BE REMOVED ONCE END STAGE IS COMPLETED
	#IF IS_DEBUG_BUILD	// Fix for release build compile error
		IF DOES_ENTITY_EXIST(EnemyCar)
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
						GameCanFail = FALSE
						SET_UBER_PLAYBACK_TO_TIME_NOW(EnemyCar, 110000)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-1195.5024, -3028.2200, 12.9444>>)
								SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 242.4391)
								SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 40)
							ENDIF
						ENDIF
						GameCanFail = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	//Create Jet in hanger 
	IF NOT DOES_ENTITY_EXIST(EndPlane)
		IF DOES_ENTITY_EXIST(EnemyCar)
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 30000
					
						REQUEST_MODEL(JET)
						IF HAS_MODEL_LOADED(JET)
							EndPlane = CREATE_VEHICLE(JET, << -952.1345, -2990.2688, 12.9451 >>, 240.7726)
							SET_VEHICLE_LIVERY(EndPlane, 2)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EndPlane, FALSE)
							FREEZE_ENTITY_POSITION(EndPlane, TRUE)
							SET_ENTITY_PROOFS(EndPlane, TRUE, TRUE, TRUE, TRUE, TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(JET)
							SET_ENTITY_LOD_DIST(EndPlane, 500)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PLANE_ENGINE_CONTROLLER()
	
	IF DOES_BLIP_EXIST(EnemyPedBlip)
		UPDATE_CHASE_BLIP(EnemyPedBlip, EnemyPed, 300, 0.9)
	ENDIF
	
	IF iControlFlag = 0
	
		//stop Michael from looking over his shoulder through out chase
		IF ChaseStage < 2
			SET_PED_CAN_HEAD_IK(PLAYER_PED_ID(), false)
//			PRINTSTRING("SET_PED_CAN_HEAD_IK BEING CALLED NOW") PRINTNL()
		ENDIF
	
		//Everything that needs to be called every frame here
		CLEANUP_MEMORY_THROUGHOUT_CHASE()
	
		//DEBUG For applying the blasts to the cop cars.
		#IF IS_DEBUG_BUILD
//			DO_CAR_BLAST_WIDGET()
			DO_DEBUG_CAR_NAMES()
		#ENDIF		
	
		UBER_PLAYBACK_SPEED_CONTROLLER()
//		UPDATE_UBER_PLAYBACK(EnemyCar, 1)//
		
		//Set the heli's blades at full speed for it starting
		IF helisBladesSetup = FALSE
			IF DOES_ENTITY_EXIST(SetPieceCarID[20])
				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
					SET_VEHICLE_ENGINE_ON(SetPieceCarID[20], TRUE, TRUE)
					SET_HELI_BLADES_FULL_SPEED(SetPieceCarID[20])
					helisBladesSetup = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Set the sirens on for the set peices
		TURN_POLICE_SIRENS_ON()
		
		RELEASE_SOUNDS()	
	
		//Control cam recording playback/recording
		IF camrecordingstarted = FALSE
			IF DOES_ENTITY_EXIST(EnemyCar)
				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						START_CAM_PLAYBACK(heliChaseCam, VehCam, "BB_Chase", 1)
						camrecordingstarted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					UPDATE_CAM_PLAYBACK(heliChaseCam, VehCam, fPlaybackSpeed)
					SET_CAM_FOV(heliChaseCam, 45)
				ENDIF
			ENDIF			
		ENDIF	
	
		//Add help text and activate the news heli cam after given time in chase.
		IF b_heliCamHelp = FALSE
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 15000
					AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 125464
					AND SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(TRUE, FALSE)
						//Set distance check to true here regardles of wether player came from wrong side or not
						distanceCheckRequired = TRUE
						
						IF bMissionFailed = FALSE
							CLEAR_HELP(TRUE)
							PRINT_HELP("TRV4_HELP1")//Hold ~INPUT_VEH_CIN_CAM~ to view Weazel News heli camera.
							DISABLE_CELLPHONE(TRUE)
							KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
							SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
							b_heliCamHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF HAS_SCALEFORM_MOVIE_LOADED(ScaleformMovie)
					AND HAS_CAM_RECORDING_LOADED(1, "BB_Chase")
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 15000		
						AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 125464
						AND bMissionFailed = FALSE
						AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							CHOPPER_CAM()
						ENDIF
						//Turn off the heli cam now if the player is using it
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 125464
						OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			          		IF heli_chase_cam_active
								
								CLEAR_HELP()
								
			            		// turn it off
								SET_MULTIHEAD_SAFE(FALSE,TRUE)
			            		SET_CAM_ACTIVE(heliChaseCam, FALSE)
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								DISPLAY_RADAR(TRUE)
	//							CLEAR_TIMECYCLE_MODIFIER()
								IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CHOPPER_CAM")
									STOP_AUDIO_SCENE("SOL_3_CHOPPER_CAM")
								ENDIF
								STOP_SOUND(NewsHeliSoundID)
								//Stats for how long the player views the heli cam
								INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
			//					RELEASE_SOUND_ID(NewsHeliSoundID)

								SET_FRONTEND_RADIO_ACTIVE(TRUE)
								
								//Reset Flags
								AudioSceneStarted[2] 	= FALSE
			            		heli_chase_cam_active 	= FALSE

			          		ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	

	
		SWITCH ChaseStage
		
			CASE 0
				
				//Add in a video recording of the truck blowing up during the chase.
				IF bVideoRecorded[0] = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 44255				
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1187.3, -2847.8, 13.6>>) < 100
								REPLAY_RECORD_BACK_FOR_TIME(10.0)
								bVideoRecorded[0] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//Add in a video recording of the cop car getting blasted by the jet
				IF bVideoRecorded[1] = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 52875				
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1349, -2720, 14>>) < 50
								REPLAY_RECORD_BACK_FOR_TIME(6.0)
								bVideoRecorded[1] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//Add in a video recording of the jet landing over the chase
				IF bVideoRecorded[2] = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 87085				
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1507, -2520, 14>>) < 70
								REPLAY_RECORD_BACK_FOR_TIME(10.0)
								bVideoRecorded[2] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
							
				//Fix bug for setpiece car 9 crashing into parked plane.
//				IF collisionSetForCar9 = FALSE
//					IF DOES_ENTITY_EXIST(SetPieceCarID[9])
//						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[9])	
//							IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 62000
//										SET_ENTITY_COLLISION(SetPieceCarID[9], FALSE)
//									ELSE
//										SET_ENTITY_COLLISION(SetPieceCarID[9], TRUE)
//										collisionSetForCar9 = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				//Setup 2 AI police cars to chase after the player
				AI_COP_CAR_CONTROLLER()
				
				//Do firemen ambient scene 
				CREATE_AMBIENT_FIRE_SCENE()
				
				//Do the audio scene for the fire test station/plane
				IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_PLANE_ON_FIRE")
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 64280
								AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 72000
									IF GET_DISTANCE_BETWEEN_COORDS(<<-1149.6, -2323.7, 22.5>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 130
										PRINTSTRING("SOL_3_PLANE_ON_FIRE AUDIO SCENE STARTING") PRINTNL()
										START_AUDIO_SCENE("SOL_3_PLANE_ON_FIRE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 72500
									STOP_AUDIO_SCENE("SOL_3_PLANE_ON_FIRE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
							
				//Do the audio scene for the jet landing over the player				
				IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_JET_LANDING")
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 78000
								AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 85000
									START_AUDIO_SCENE("SOL_3_EVENT_JET_LANDING")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 85500
									STOP_AUDIO_SCENE("SOL_3_EVENT_JET_LANDING")
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF	
				
				//Do the audio scene for the jet taxing and the police car crashing into its wheels			
				IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_PLANE_TAXIING_EXPLOSION")
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 100000
								AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 107000
									START_AUDIO_SCENE("SOL_3_EVENT_PLANE_TAXIING_EXPLOSION")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 107500
									STOP_AUDIO_SCENE("SOL_3_EVENT_PLANE_TAXIING_EXPLOSION")
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF	
	
				//Force all uber setpeices to load in.
				IF DOES_ENTITY_EXIST(EnemyCar)
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 10000		
								CREATE_ALL_WAITING_UBER_CARS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				//Switch off the roads before the chase enters the airport
				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 30000
							SWITCH_ROADS_OFF_IN_PLAYBACK(EnemyCar, GET_TIME_POSITION_IN_RECORDING(EnemyCar))
						ENDIF
					ENDIF
				ENDIF		
				
				IF MusicPrepared = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 22000
								PREPARE_MUSIC_EVENT("TRV4_AIRPORT_ENTERED")
								MusicPrepared = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MusicTriggered2 = FALSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-940.825317,-2858.601563,11.944824>>, <<-1001.272339,-2823.764160,21.006737>>, 5.750000)
							TRIGGER_MUSIC_EVENT("TRV4_AIRPORT_ENTERED")
							MusicTriggered2 = TRUE
						ENDIF
					ENDIF
				ENDIF
						
				
				//Update objective and checkpoint now 
				IF doneChaseAfterBadGuyText = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF GET_DISTANCE_BETWEEN_ENTITIES(EnemyCar, PLAYER_PED_ID()) < 170
						AND IS_ENTITY_ON_SCREEN(EnemyCar)
							IF DOES_BLIP_EXIST(airportBlip)
								REMOVE_BLIP(airportBlip)
							ENDIF
							IF IS_AUDIO_SCENE_ACTIVE("SOL_3_DRIVE_TO_AIRPORT")
								STOP_AUDIO_SCENE("SOL_3_DRIVE_TO_AIRPORT")
							ENDIF
							IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_MAIN_CHASE")
								START_AUDIO_SCENE("SOL_3_MAIN_CHASE")
							ENDIF
							//Add blip for enemycar
							IF NOT DOES_BLIP_EXIST(enemypedBlip)
								IF NOT IS_PED_INJURED(EnemyPed)
									enemypedBlip = ADD_BLIP_FOR_ENTITY(EnemyPed)
									SET_BLIP_AS_FRIENDLY(enemypedBlip, TRUE)
								ENDIF
							ENDIF	
							CLEAR_GPS_MULTI_ROUTE()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
								PRINT_NOW("TRV4_CHASE2", DEFAULT_GOD_TEXT_TIME, 1)//Chase after the ~b~translator.
								//Set up a checkpoint here
								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_CHASE_START")
								TRIGGER_MUSIC_EVENT("TRV4_CHASE")
								doneChaseAfterBadGuyText = TRUE
							ENDIF
						ENDIF
					ENDIF
					//move stage on if player hits the yellow blip regardless if enemy is on screen or not.
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-536.036560,-2102.792969,5.672412>>, <<-464.575500,-2165.133301,14.751824>>, 65.250000)
						IF DOES_BLIP_EXIST(airportBlip)
							REMOVE_BLIP(airportBlip)
						ENDIF
						//Add blip for enemycar
						IF NOT DOES_BLIP_EXIST(enemypedBlip)
							IF NOT IS_PED_INJURED(EnemyPed)
								enemypedBlip = ADD_BLIP_FOR_ENTITY(EnemyPed)
								SET_BLIP_AS_FRIENDLY(enemypedBlip, TRUE)
							ENDIF
						ENDIF	
						CLEAR_GPS_MULTI_ROUTE()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							PRINT_NOW("TRV4_CHASE2", DEFAULT_GOD_TEXT_TIME, 1)//Chase after the ~b~translator.
							//Set up a checkpoint here
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_CHASE_START")
							TRIGGER_MUSIC_EVENT("TRV4_CHASE")
							doneChaseAfterBadGuyText = TRUE
						ENDIF	
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 15000
//								IF hintcamHelpTextDone = FALSE
//									PRINT_HELP("TRV4_HELP2")//Press ~INPUT_VEH_CIN_CAM~ to toggle focus onto the translator.
//									//Turn off GPS route at same time as help text for hint cam
//		//							SET_BLIP_ROUTE(enemypedBlip, FALSE)	
//									hintcamHelpTextDone = TRUE
//								ENDIF	
								
//								IF doneChat11 = FALSE
//									IF NOT IS_MESSAGE_BEING_DISPLAYED()
//									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
////										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD","T1M4_CHASE", "T1M4_CHASE_4", CONV_PRIORITY_MEDIUM)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE4", CONV_PRIORITY_AMBIENT_MEDIUM)	
//											//Argh! That's my fucking film, bitch!
//											doneChat11 = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
								
//								CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, EnemyCar)
//								IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
//									IF NOT IS_GAMEPLAY_HINT_ACTIVE()
//										PRINTSTRING("Gameplay Hint is not active") PRINTNL()
////										SET_GAMEPLAY_VEHICLE_HINT(EnemyCar, <<0,0,0>>, TRUE)
//										CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, EnemyCar)
//									ELSE
//										KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
//										PRINTSTRING("Gameplay Hint is active")PRINTNL()
//									ENDIF
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
//				//Sort out dialogue 
//				IF doneChat6 = FALSE
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 21517				
//								CLEAR_PRINTS()
//								IF NOT IS_MESSAGE_BEING_DISPLAYED()
//								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
////									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", "T1M4_CHASE_1", CONV_PRIORITY_AMBIENT_MEDIUM)
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", CONV_PRIORITY_AMBIENT_MEDIUM)	
//										//Shit, he's gone right. He's heading straight for the airport.
//										doneChat6 = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF doneChat10 = FALSE
//						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 23500
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyCar) < 50
//										IF NOT IS_MESSAGE_BEING_DISPLAYED()
//										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//	//										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", "T1M4_CHASE_5", CONV_PRIORITY_AMBIENT_MEDIUM)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE5", CONV_PRIORITY_AMBIENT_MEDIUM)
//												//Police, or no police. I'm not going to let you get away!
//												doneChat10 = TRUE
//											ENDIF	
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Sort out dialogue 
//				IF doneChat8 = FALSE
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 60000				
//								CLEAR_PRINTS()
//								IF NOT IS_MESSAGE_BEING_DISPLAYED()
//								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
////									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", "T1M4_CHASE_6", CONV_PRIORITY_AMBIENT_MEDIUM)
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE6", CONV_PRIORITY_AMBIENT_MEDIUM)	
//										//Pull over and i'll give you a way out!
//										doneChat8 = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Sort out dialogue 
//				IF doneChat12 = FALSE
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 85000				
//								CLEAR_PRINTS()
//								IF NOT IS_MESSAGE_BEING_DISPLAYED()
//								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
////									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", "T1M4_CHASE_7", CONV_PRIORITY_AMBIENT_MEDIUM)
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE7", CONV_PRIORITY_AMBIENT_MEDIUM)
//										//You want to take a plane load of tourists down with you, or you want to stop and talk about this?
//										doneChat12 = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Sort out dialogue 
//				IF doneChat5 = FALSE
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 105530				
//								CLEAR_PRINTS()
//								IF NOT IS_MESSAGE_BEING_DISPLAYED()
//								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
////									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE", "T1M4_CHASE_2", CONV_PRIORITY_AMBIENT_MEDIUM)
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_CHASE2", CONV_PRIORITY_AMBIENT_MEDIUM)	
//										//This guy is fucking crazy!
//										doneChat5 = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF

				//Set up 1st set piece not part of uber recording and delete it when required.
				IF setpiecePlane2Setup = FALSE
					REQUEST_MODEL(JET)
					IF HAS_MODEL_LOADED(JET)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 18490
									SetPiecePlane[1] = CREATE_VEHICLE(JET, <<-441.3117, -3435.7363, 409.9454>>, 148.3975)
									START_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[1], 2, "BB_Chase")
									SET_MODEL_AS_NO_LONGER_NEEDED(JET)
									setpiecePlane2Setup = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTSTRING("Waiting for JET model loading") PRINTNL()
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(SetPiecePlane[1])
						IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[1])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPiecePlane[1])
								SET_PLAYBACK_SPEED(SetPiecePlane[1], fPlaybackSpeed)
//										PRINTSTRING("Set peice plane 1 time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(SetPiecePlane[1])) PRINTNL()
								IF GET_TIME_POSITION_IN_RECORDING(SetPiecePlane[1]) > 20000
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SetPiecePlane[1]) > 200
									AND NOT IS_ENTITY_ON_SCREEN(SetPiecePlane[1])
										STOP_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[1])
										IF DOES_ENTITY_EXIST(setPiecePlanePilot[1])
											DELETE_PED(setPiecePlanePilot[1])
										ENDIF
										DELETE_VEHICLE(SetPiecePlane[1])
										iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2269554
									ENDIF
								ENDIF
								IF DOES_ENTITY_EXIST(SetPiecePlane[1])
									IF GET_TIME_POSITION_IN_RECORDING(SetPiecePlane[1]) > 31543
										STOP_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[1])
										FREEZE_ENTITY_POSITION(SetPiecePlane[1], TRUE)
									ENDIF
								ENDIF
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SetPiecePlane[1]) > 200
								AND NOT IS_ENTITY_ON_SCREEN(SetPiecePlane[1])
									IF DOES_ENTITY_EXIST(setPiecePlanePilot[1])
										DELETE_PED(setPiecePlanePilot[1])
									ENDIF
									DELETE_VEHICLE(SetPiecePlane[1])
									iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2269554
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
				
				//Only allow this set peice to be seen once.
//				IF g_bSetPieceTruckExplodeSeen = FALSE
				IF trailerCreated = FALSE
					IF NOT DOES_ENTITY_EXIST(Trailer)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 23000
									REQUEST_MODEL(TANKER)
									IF HAS_MODEL_LOADED(TANKER)
									 	Trailer = CREATE_VEHICLE(Tanker, <<-1325, -2779, 14>>, 233.9563)
										SET_MODEL_AS_NO_LONGER_NEEDED(TANKER)
										SET_VEHICLE_COLOURS(trailer, 57, 57)
										trailerCreated = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				//Stop the playback of the truck here to slow it down a bit.
		//		IF playbackspeedChangedForTruck = FALSE
				IF DOES_ENTITY_EXIST(setpieceCarId[1])
					IF IS_VEHICLE_DRIVEABLE(setpieceCarId[1])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceCarId[1])
		//					PRINTSTRING("Playback position for setpieceCarId[1] is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(setpieceCarId[1])) PRINTNL()
		//					PRINTSTRING("Playback speed for setpieceCarId[1] is ") PRINTFLOAT(GET_ENTITY_SPEED(setpieceCarId[1])) PRINTNL()
							IF GET_TIME_POSITION_IN_RECORDING(setpieceCarId[1]) > 13000
								IF playbackspeedChangedForTruck = FALSE
									SetPieceCarRecordingSpeed[1] = 0.5
									truckDriver = GET_PED_IN_VEHICLE_SEAT(setpieceCarId[1], VS_DRIVER)
									playbackspeedChangedForTruck = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		//		ENDIF
			
				IF trailerdetached = FALSE
					REQUEST_ADDITIONAL_COLLISION_AT_COORD(<<-1325, -2779, 14>>)
					IF DOES_ENTITY_EXIST(setpieceCarId[1])
						IF vehicleColoursSet = FALSE
							SET_VEHICLE_COLOURS(setpieceCarId[1], 57, 57)
							vehicleColoursSet = TRUE
						ENDIF
						IF DOES_ENTITY_EXIST(Trailer)
							IF NOT IS_ENTITY_DEAD(setpieceCarId[1])
								IF NOT IS_ENTITY_DEAD(Trailer)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceCarId[1])
										ATTACH_VEHICLE_TO_TRAILER(setpieceCarId[1], Trailer)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(Trailer)
						IF DOES_ENTITY_EXIST(EnemyCar)
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 41430
										DETACH_ENTITY(Trailer)
										trailerdetached = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF playbackStoppedForTruck = FALSE
						IF IS_VEHICLE_DRIVEABLE(setpieceCarId[1])
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 42700
										//Kill the truck driver now to stop him from trying to drive the truck.
										IF DOES_ENTITY_EXIST(truckDriver)
											IF NOT IS_PED_INJURED(truckDriver)
												SET_ENTITY_HEALTH(truckDriver, 0)
											ENDIF
										ENDIF
										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceCarId[1])
											STOP_PLAYBACK_RECORDED_VEHICLE(setpieceCarId[1])
											playbackStoppedForTruck = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Blow up trailer 
				IF DOES_ENTITY_EXIST(EnemyCar)
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							//Blast truck horn just before crash
							IF hornSoundStarted = FALSE
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 38000
									IF DOES_ENTITY_EXIST(SetPieceCarID[1])
									AND IS_VEHICLE_DRIVEABLE(SetPieceCarID[1])
										PLAY_SOUND_FROM_ENTITY(-1, "Trevor_4_747_Tanker_Horn", SetPieceCarID[1])
										hornSoundStarted = TRUE
									ENDIF
								ENDIF
							ENDIF
							//Start audio scene to lower car engine and radio to emphasise sound fx for trailer and police crashing
							IF AudioSceneStarted[1] = FALSE
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 39000	
									IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_TANKER_CRASH")
										START_AUDIO_SCENE("SOL_3_EVENT_TANKER_CRASH")
									ENDIF
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										SET_ENTITY_PROOFS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE, TRUE, TRUE, FALSE, FALSE)
									ENDIF
									AudioSceneStarted[1] = TRUE
								ENDIF
							ENDIF		
							IF haveCarAndTrailerBeenExploded = FALSE
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 41611//41204
									IF DOES_ENTITY_EXIST(Trailer)
										IF NOT IS_ENTITY_DEAD(Trailer)
											IF DOES_ENTITY_EXIST(SetPieceCarID[7])
												IF NOT IS_ENTITY_DEAD(SetPieceCarID[7])
//													PRINTSTRING("Trailer and SetpieceCarID[7] are still alive and should be exploding here") PRINTNL()
													PLAY_SOUND_FROM_ENTITY(-1, "Trevor_4_747_Tanker_Explosion", Trailer)
													STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[7])
													EXPLODE_VEHICLE(SetPieceCarID[7])
													EXPLODE_VEHICLE(Trailer)
													//Create explosion and flames 
	//												START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_exp_tanker", Trailer, <<-1221, -2852, 17>>, <<0,0,0>>, 1)
	//												C4[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_trailer_fire", trailer, <<-1221, -2852, 17>>, <<0,0,0>>, 1)
													ADD_EXPLOSION(<<-1221, -2852, 17>>,EXP_TAG_DIR_GAS_CANISTER, 1.0)	
	//												START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_exp_tanker", Trailer, <<-1212, -2855, 14.34>>, <<0,0,0>>, 1)
	//												C4[4] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_trailer_fire", trailer, <<-1212, -2855, 14.34>>, <<0,0,0>>, 1)
													ADD_EXPLOSION(<<-1212, -2855, 14.34>>,EXP_TAG_DIR_GAS_CANISTER, 1.0)
													//Set Global flag to true here so this setpeice wont be seen again.
													SET_VEHICLE_AS_NO_LONGER_NEEDED(Trailer)
													SET_VEHICLE_AS_NO_LONGER_NEEDED(setpieceCarId[1])
//													g_bSetPieceTruckExplodeSeen = TRUE
													haveCarAndTrailerBeenExploded = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF	
							ENDIF
							IF AudioSceneStopped[1] = FALSE
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 43204
									//Stop the audio scene now 
									IF IS_AUDIO_SCENE_ACTIVE("SOL_3_EVENT_TANKER_CRASH")
										STOP_AUDIO_SCENE("SOL_3_EVENT_TANKER_CRASH")
									ENDIF
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Tanker_Explosion")
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										SET_ENTITY_PROOFS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE, FALSE, FALSE, FALSE, FALSE)
									ENDIF
									AudioSceneStopped[1] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
//				ENDIF
				
				//Start the audio scene for the heli taking off
				IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_HELI_TAKEOFF")	
					IF DOES_ENTITY_EXIST(SetPieceCarID[0])
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[0])
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 36000
										AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 40000
											START_AUDIO_SCENE("SOL_3_HELI_TAKEOFF")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(SetPieceCarID[0])
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[0])
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 43204
											STOP_AUDIO_SCENE("SOL_3_HELI_TAKEOFF")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Set up 3rd set piece not part of uber recording.
				IF setpiecePlane1Setup = FALSE
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 60500
								REQUEST_MODEL(JET)
							ENDIF
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 66500
								IF HAS_MODEL_LOADED(JET)
									SetPiecePlane[0] = CREATE_VEHICLE(JET, << -1119.0723, -1864.5477, -8.2205 >>, 148.3975)
									SET_ENTITY_INVINCIBLE(SetPiecePlane[0], TRUE)
									SET_VEHICLE_LIVERY(SetPiecePlane[0], 2)
									START_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[0], 35, "BB_Chase")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[0], 3491)
									SET_MODEL_AS_NO_LONGER_NEEDED(JET)
									setpiecePlane1Setup = TRUE
								ELSE
									PRINTSTRING("Waiting for JET model to load") PRINTNL()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(SetPiecePlane[0])
						IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[0])
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPiecePlane[0])
										SET_PLAYBACK_SPEED(SetPiecePlane[0], fPlaybackSpeed)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 80000
											IF GET_DISTANCE_BETWEEN_ENTITIES(SetPiecePlane[0], PLAYER_PED_ID()) > 200
												STOP_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[0])
											ENDIF
										ENDIF
									ELSE
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 80000
											IF GET_DISTANCE_BETWEEN_ENTITIES(SetPiecePlane[0], PLAYER_PED_ID()) > 200
											AND NOT IS_ENTITY_ON_SCREEN(SetPiecePlane[0])
												IF DOES_ENTITY_EXIST(setPiecePlanePilot[0])
													DELETE_PED(setPiecePlanePilot[0])
												ENDIF
												DELETE_VEHICLE(SetPiecePlane[0])
												iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2269554
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
				
//				//Add sound effects for small plane's engine before it crashes
//				IF SFXAddedForSmallPlane = FALSE
//					IF DOES_ENTITY_EXIST(SetPieceCarID[6])
//						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[6])
//							PLAY_SOUND_FROM_ENTITY(PropPlaneSoundID, "Trevor_4_747_Prop_Plane", SetPieceCarID[6])
//							SFXAddedForSmallPlane = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
				
				//Have 2 airport workers dive out of the way of the car
				IF DivingPedsAnimsDone = FALSE
					IF pedsCreated = FALSE
						IF DOES_ENTITY_EXIST(EnemyCar)
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50000	
										REQUEST_MODEL(S_M_Y_AirWorker)
										REQUEST_ANIM_DICT("misstrevor4")
										IF HAS_MODEL_LOADED(S_M_Y_AirWorker)
											DivingWorker1 = CREATE_PED(PEDTYPE_MISSION, S_M_Y_AirWorker, << -1170.5410, -2507.1108, 12.9455 >>, 218.3035)//Plays dive_clear_goon1
											DivingWorker2 = CREATE_PED(PEDTYPE_MISSION, S_M_Y_AirWorker, << -1168.2637, -2508.7151, 12.9455 >>, 70.7735)//Plays dive_clear_goon2
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(DivingWorker1, TRUE)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(DivingWorker2, TRUE)
											SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_AirWorker)
											pedsCreated = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(EnemyCar)
							IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
									IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 58000		
										IF NOT IS_PED_INJURED(DivingWorker1)
											IF HAS_ANIM_DICT_LOADED("misstrevor4")
												IF NOT IS_ENTITY_PLAYING_ANIM(DivingWorker1, "misstrevor4", "dive_clear_goon1")
													TASK_PLAY_ANIM(DivingWorker1, "misstrevor4", "dive_clear_goon1")
												ENDIF
											ENDIF
										ENDIF
										IF NOT IS_PED_INJURED(DivingWorker2)
											IF HAS_ANIM_DICT_LOADED("misstrevor4")
												IF NOT IS_ENTITY_PLAYING_ANIM(DivingWorker2, "misstrevor4", "dive_clear_goon2")
													TASK_PLAY_ANIM(DivingWorker2, "misstrevor4", "dive_clear_goon2")
												ENDIF
											ENDIF
										ENDIF
										DivingPedsAnimsDone = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Clean up the diving peds and the anims
					IF DOES_ENTITY_EXIST(DivingWorker1)
						IF NOT IS_PED_INJURED(DivingWorker1)
							IF NOT IS_ENTITY_PLAYING_ANIM(DivingWorker1, "misstrevor4", "dive_clear_goon1")
								IF DOES_ENTITY_EXIST(DivingWorker2)
									IF NOT IS_PED_INJURED(DivingWorker2)
										IF NOT IS_ENTITY_PLAYING_ANIM(DivingWorker2, "misstrevor4", "dive_clear_goon2")		
											REMOVE_ANIM_DICT("misstrevor4")	
											SET_PED_AS_NO_LONGER_NEEDED(DivingWorker1)
											SET_PED_AS_NO_LONGER_NEEDED(DivingWorker2)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
										
				//Blow up car plane and police car as they crash
//				IF DOES_ENTITY_EXIST(EnemyCar)
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							//Start audio scene to lower car engine and radio to emphasise sound fx for small plane and poice crashing
//							IF AudioSceneStarted[0] = FALSE
//								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 72614	
//									START_AUDIO_SCENE("TREVOR_4_BLOW_BACK_EXCITING_EVENT")
//									AudioSceneStarted[0] = TRUE
//								ENDIF
//							ENDIF
//							IF PropPlaneSoundIDPlayed = FALSE
//								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 73114
//									IF DOES_ENTITY_EXIST(SetPieceCarID[6])
//										IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[6])	
//											PLAY_SOUND_FROM_ENTITY(PropPlaneSoundID, "Trevor_4_747_Plane_Crash", SetPieceCarID[6])
//											PropPlaneSoundIDPlayed = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 73619		
//								IF DOES_ENTITY_EXIST(SetPieceCarID[9])
//									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[9])
//										IF DOES_ENTITY_EXIST(SetPieceCarID[6])
//											IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[6])
//												IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[6])
//													STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[6])
//													EXPLODE_VEHICLE(SetPieceCarID[6])
//													STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[9])
//													EXPLODE_VEHICLE(SetPieceCarID[9])
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//							//Stop audio scene to return car engine and radio volume to normal
//							IF AudioSceneStopped[0] = FALSE
//								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 75619
//									RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Plane_Explosion")
//									STOP_AUDIO_SCENE("TREVOR_4_BLOW_BACK_EXCITING_EVENT")
//									AudioSceneStopped[0] = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF			
				
				//Blast and blow up car 8
				IF DOES_ENTITY_EXIST(SetPieceCarID[8])
					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//							PRINTSTRING("set peice car 8 playback position is") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[8])) PRINTNL()
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50200
											IF Car8HasBeenBlasted = FALSE
												SET_PLAYBACK_SPEED(SetPieceCarID[8], 1)
												IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SetPieceCarID[8]) < 100
													SET_TIME_SCALE(0.3)
													timeScale1BackToNormal = FALSE
													IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
														START_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
													ENDIF
													SET_GAMEPLAY_VEHICLE_HINT(SetPieceCarID[8], <<0,0,0>>, TRUE, 1500, 500, 100)
												ENDIF
												
												SET_ENTITY_LOAD_COLLISION_FLAG(SetPieceCarID[8], TRUE)
												
												SET_VEHICLE_DOOR_BROKEN(SetPieceCarID[8], SC_DOOR_FRONT_RIGHT, FALSE)
												//Start sound effect of car flying through air
												PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID1, "Trevor_4_747_Flying_Car", SetPieceCarID[8])
												SETTIMERB(0)
												Car8HasBeenBlasted = TRUE
											ENDIF
										ENDIF
										IF Car8HasBeenBlasted = TRUE
//											PRINTSTRING("timerb is ") PRINTINT(TIMERB()) PRINTNL()
											IF TIMERB() > 662
												IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
													STOP_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
												ENDIF
												SET_TIME_SCALE(1.0)
												timeScale1BackToNormal = TRUE
											ENDIF
											IF TIMERB() > 1872
												STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[8])
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50200
											IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[8])
												IF CarExplodeSoundID1Played = FALSE
													PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID1, "Trevor_4_747_Carsplosion", SetPieceCarID[8])
													EXPLODE_VEHICLE(SetPieceCarID[8])
													CarExplodeSoundID1Played = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF timeScale1BackToNormal = FALSE
							IF Car8HasBeenBlasted = TRUE
								IF TIMERB() > 662
									IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
										STOP_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
									ENDIF
									SET_TIME_SCALE(1.0)
									timeScale1BackToNormal = TRUE
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ELSE
					IF timeScale1BackToNormal = FALSE
						IF Car8HasBeenBlasted = TRUE
							IF TIMERB() > 662
								IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
									STOP_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
								ENDIF
								SET_TIME_SCALE(1.0)
								timeScale1BackToNormal = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
				
//				//Blast and blow up car 8
//				IF DOES_ENTITY_EXIST(SetPieceCarID[8])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//							IF DOES_ENTITY_EXIST(EnemyCar)
//								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50845
//											IF GET_DISTANCE_BETWEEN_ENTITIES(PlayerCar, SetPieceCarID[8]) < 80
//												SET_TIME_SCALE(0.3)
//												timeScale1BackToNormal = FALSE
//												IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
//													START_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
//												ENDIF
//												SET_GAMEPLAY_VEHICLE_HINT(SetPieceCarID[8], <<0,0,0>>, TRUE, 1500, 500, 100)
//											ENDIF
//											
//											SET_ENTITY_LOAD_COLLISION_FLAG(SetPieceCarID[8], TRUE)
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[8])
//											
//		//									SET_VEHICLE_CAN_BREAK(SetPieceCarID[8], FALSE)
//											SET_VEHICLE_DOOR_BROKEN(SetPieceCarID[8], SC_DOOR_FRONT_RIGHT, FALSE)
//											//Start sound effect of car flying through air
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID1, "Trevor_4_747_Flying_Car", SetPieceCarID[8])
//		//									PLAY_SOUND_FROM_ENTITY(-1, "Trevor_4_747_Carsplosion", SetPieceCarID[8])
//											BLAST_VEHICLE(SetPieceCarID[8], <<0.072, 1.638, 1.362>>, <<0,3,0>>, 2.5)//2.3
//											BLAST_VEHICLE(SetPieceCarID[8], <<0.024, -3.4, 0.462>>, <<0,1,0>>, 100)//30
//											Car8HasBeenBlasted = TRUE
//											SETTIMERB(0)
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ELSE
//							PRINTSTRING("timerb is ") PRINTINT(TIMERB()) PRINTNL()
//							IF Car8HasBeenBlasted = TRUE
//								IF TIMERB() > 662
//									IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
//										STOP_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
//									ENDIF
//									SET_TIME_SCALE(1.0)
//									timeScale1BackToNormal = TRUE
//								ENDIF
//								IF TIMERB() > 1872
//									IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[8])
//										IF CarExplodeSoundID1Played = FALSE
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID1, "Trevor_4_747_Carsplosion", SetPieceCarID[8])
//											CarExplodeSoundID1Played = TRUE
//										ENDIF
//		//								PLAY_SOUND_FROM_ENTITY(-1, "Trevor_4_747_Carsplosion", SetPieceCarID[8])
////										EXPLODE_VEHICLE(SetPieceCarID[8])
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF timeScale1BackToNormal = FALSE
//						IF Car8HasBeenBlasted = TRUE
//							IF TIMERB() > 662
//								IF IS_AUDIO_SCENE_ACTIVE("SOL_3_CAR_JET_ENGINE")
//									STOP_AUDIO_SCENE("SOL_3_CAR_JET_ENGINE")
//								ENDIF
//								SET_TIME_SCALE(1.0)
//								timeScale1BackToNormal = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF				
//				
//				//Blast and blow up car 10
//				IF DOES_ENTITY_EXIST(EnemyCar)
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 81416
//								IF DOES_ENTITY_EXIST(SetPieceCarID[10])
//									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[10])
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[10])
//											IF GET_DISTANCE_BETWEEN_ENTITIES(PlayerCar, SetPieceCarID[10]) < 90
////												SET_TIME_SCALE(0.3)
//												timeScale2BackToNormal = FALSE
////												SET_GAMEPLAY_VEHICLE_HINT(SetPieceCarID[10], <<0,0,0>>, TRUE, 1000, 1000, 500)
//											ENDIF
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[10])
//											
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID2, "Trevor_4_747_Flying_Car", SetPieceCarID[10])
//											BLAST_VEHICLE(SetPieceCarID[10], <<-0.142, 0.953, 4.069>>, <<0,3,0>>, 3.2)
//											BLAST_VEHICLE(SetPieceCarID[10], <<-1.076, -53.342, 0.710>>, <<0,0,0>>, 70)//40
//											SETTIMERA(0)
//											Car10Blasted = TRUE
//										ELSE
//											PRINTSTRING("timera is ") PRINTINT(TIMERA()) PRINTNL()
//											IF Car10Blasted = TRUE
//												//Set time scale back to normal
//												IF TIMERA() > 1885//885
////													SET_TIME_SCALE(1)
//													timeScale2BackToNormal = TRUE
//													IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[10])
//														IF CarExplodeSoundID2Played = FALSE
//															PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID2, "Trevor_4_747_Carsplosion", SetPieceCarID[10])
//															CarExplodeSoundID2Played = TRUE
//														ENDIF
//														EXPLODE_VEHICLE(SetPieceCarID[10])
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELSE
//										IF timeScale2BackToNormal = FALSE
////											SET_TIME_SCALE(1)
//											timeScale2BackToNormal = TRUE
//										ENDIF
//									ENDIF
//								ELSE
//									IF timeScale2BackToNormal = FALSE
////										SET_TIME_SCALE(1)
//										timeScale2BackToNormal = TRUE
//									ENDIF
//								ENDIF
//								//Set time scale back to normal if player passes car 12
//								IF timeScale2BackToNormal = FALSE
//									IF DOES_ENTITY_EXIST(SetPieceCarID[12])
//										IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
//											vcar12Coords = GET_ENTITY_COORDS(SetPieceCarID[12])
//											vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//											IF vPlayerCoords.y < vcar12Coords.y 
////												SET_TIME_SCALE(1)
//												timeScale2BackToNormal = TRUE	
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
//				//Blast and blow up car 11
//				IF DOES_ENTITY_EXIST(SetPieceCarID[11])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[11])
//							IF DOES_ENTITY_EXIST(EnemyCar)
//								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82683
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[11])
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID3, "Trevor_4_747_Flying_Car", SetPieceCarID[11])
//											BLAST_VEHICLE(SetPieceCarID[11], <<-0.142, 0.953, 4.069>>, <<0,3,0>>, 3.5)
//											BLAST_VEHICLE(SetPieceCarID[11], <<-1.076, -53.342, 0.710>>, <<0,0,0>>, 30)
//											Car11HasBeenBlasted = TRUE
//											SETTIMERB(0)
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ELSE
//							IF Car11HasBeenBlasted = TRUE
//								IF TIMERB() > 1872
//									IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[11])
//										IF CarExplodeSoundID3Played = FALSE
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID3, "Trevor_4_747_Carsplosion", SetPieceCarID[11])
//											CarExplodeSoundID3Played = TRUE
//										ENDIF
////										EXPLODE_VEHICLE(SetPieceCarID[11])
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF			
				
				//Blast and blow up car 11
				IF DOES_ENTITY_EXIST(SetPieceCarID[11])
					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[11])
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82683
											IF Car11HasBeenBlasted = FALSE
												PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID3, "Trevor_4_747_Flying_Car", SetPieceCarID[11])
												Car11HasBeenBlasted = TRUE
												SETTIMERB(0)
											ENDIF
											IF Car11HasBeenBlasted = TRUE
												IF TIMERB() > 1872
													IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[11])
														STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[11])
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF Car11HasBeenBlasted = TRUE
								IF CarExplodeSoundID3Played = FALSE
									IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[11])
										PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID3, "Trevor_4_747_Carsplosion", SetPieceCarID[11])
										EXPLODE_VEHICLE(SetPieceCarID[11])
										CarExplodeSoundID3Played = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
				
				//Blast and blow up car 12
				IF DOES_ENTITY_EXIST(SetPieceCarID[12])
					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
							PRINTSTRING("set piece car 12 playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[12])) PRINTNL()
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82463
											IF Car12HasBeenBlasted = FALSE
												PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID4, "Trevor_4_747_Flying_Car", SetPieceCarID[12])
												Car12HasBeenBlasted = TRUE
												SETTIMERB(0)
											ENDIF
											IF Car12HasBeenBlasted = TRUE
												IF TIMERB() > 1000
													IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[12])
														STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[12])
													ENDIF
												ENDIF
											ENDIF											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF slowMoStarted = FALSE
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
									IF GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[12]) > 22216
									AND GET_DISTANCE_BETWEEN_ENTITIES(PlayerCar, SetPieceCarID[12]) < 90
										SET_TIME_SCALE(0.3)
										SET_GAMEPLAY_VEHICLE_HINT(SetPieceCarID[12], <<0,0,0>>, TRUE, 2000, 1000, 500)
										iTimeScaleTimer = GET_GAME_TIMER()
										slowMoStarted = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF Car12HasBeenBlasted = TRUE
								IF CarExplodeSoundID4Played = FALSE
									IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[12])
										PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID4, "Trevor_4_747_Carsplosion", SetPieceCarID[12])
										EXPLODE_VEHICLE(SetPieceCarID[12])
										CarExplodeSoundID4Played = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				IF slowMoStarted = TRUE
					PRINTSTRING("iTimeScaleTimer is ") PRINTINT(iTimeScaleTimer) PRINTNL()
					PRINTSTRING("GET_GAME_TIMER is ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
				ENDIF
				
				//Set time scale back to normal
				IF slowMoStopped = FALSE
				AND slowMoStarted = TRUE
					IF DOES_ENTITY_EXIST(SetPieceCarID[12])
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
							vcar12Coords = GET_ENTITY_COORDS(SetPieceCarID[12])
						ENDIF
					ENDIF
					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					IF GET_GAME_TIMER() > (iTimeScaleTimer + 1579)
					OR (vPlayerCoords.y < vcar12Coords.y)
						SET_TIME_SCALE(1)
						STOP_GAMEPLAY_HINT(FALSE)
						slowMoStopped = TRUE
					ENDIF
				ENDIF
				
//				//Blast and blow up car 12
//				IF DOES_ENTITY_EXIST(SetPieceCarID[12])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
//							IF DOES_ENTITY_EXIST(EnemyCar)
//								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82463
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[12])
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID4, "Trevor_4_747_Flying_Car", SetPieceCarID[12])
//											BLAST_VEHICLE(SetPieceCarID[12], <<-0.142, 0.953, 4.069>>, <<0,3,0>>, 3.5)
//											BLAST_VEHICLE(SetPieceCarID[12], <<-1.076, -53.342, 0.710>>, <<0,0,0>>, 30)
//											Car12HasBeenBlasted = TRUE
//											SETTIMERB(0)
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ELSE
//							IF Car12HasBeenBlasted = TRUE
//								IF TIMERB() > 1000
//									IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[12])
//										IF CarExplodeSoundID4Played = FALSE
//											PLAY_SOUND_FROM_ENTITY(CarExplodeSoundID4, "Trevor_4_747_Carsplosion", SetPieceCarID[12])
//											CarExplodeSoundID4Played = TRUE
//										ENDIF
////										EXPLODE_VEHICLE(SetPieceCarID[12])
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF	
				
				//Blast and blow up car 13
				IF DOES_ENTITY_EXIST(EnemyCar)
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 105000
							AND GET_TIME_POSITION_IN_RECORDING(EnemyCar) < 113000
								REQUEST_ADDITIONAL_COLLISION_AT_COORD(<<-1172.3, -3027.5, 13.3>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(SetPieceCarID[13])
					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[13])
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 108830
											SET_VEHICLE_CAN_BREAK(SetPieceCarID[13], FALSE)
//											SET_VEHICLE_DOOR_BROKEN(SetPieceCarID[13], SC_DOOR_FRONT_RIGHT, FALSE)
											SET_ENTITY_LOAD_COLLISION_FLAG(SetPieceCarID[13], TRUE)
											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(SetPieceCarID[13], FALSE)
											SETTIMERA(0)
											car13Blasted = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF car13Blasted = TRUE
								SET_ENTITY_HEALTH(SetPieceCarID[13], 0)
								SET_VEHICLE_UNDRIVEABLE(SetPieceCarID[13], TRUE)
								SET_ENTITY_HEALTH(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[13]), 0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//				IF DOES_ENTITY_EXIST(SetPieceCarID[13])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[13])
//							IF DOES_ENTITY_EXIST(EnemyCar)
//								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 108830
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[13])
//											BLAST_VEHICLE(SetPieceCarID[13], <<-0.569, -0.193, 1.117>>, <<-0.5, 0, 0>>, 5, TRUE)
//											BLAST_VEHICLE(SetPieceCarID[13], <<18.595, -0.136, -0.093>>, <<0, 0, 0>>, 5, TRUE)
//											SET_VEHICLE_CAN_BREAK(SetPieceCarID[13], FALSE)
//											SET_VEHICLE_DOOR_BROKEN(SetPieceCarID[13], SC_DOOR_FRONT_RIGHT, FALSE)
//											SET_ENTITY_LOAD_COLLISION_FLAG(SetPieceCarID[13], TRUE)
//											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(SetPieceCarID[13], FALSE)
//											SETTIMERA(0)
//		//									car13Blasted = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//		//				IF car13Blasted = TRUE
//		//					IF TIMERA() > 765
//		//						EXPLODE_VEHICLE(SetPieceCarID[13], TRUE)
//		//					ENDIF
//		//				ENDIF
//					ENDIF
//				ENDIF	
				
				//set the plane to be invincible so wheels dont come off it after police car crashes into it
				IF planeSetInvincible = FALSE
					IF DOES_ENTITY_EXIST(SetPieceCarID[5])
						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[5])
							SET_VEHICLE_LIVERY(SetPieceCarID[5], 0)
							SET_ENTITY_INVINCIBLE(SetPieceCarID[5], TRUE)
							planeSetInvincible = TRUE
						ENDIF
					ENDIF
				ENDIF
					
				//Blast and blow up car 15
				IF DOES_ENTITY_EXIST(SetPieceCarID[15])
					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[15])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[15])
							IF DOES_ENTITY_EXIST(EnemyCar)
								IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
										IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 104989
											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[15])
											BLAST_VEHICLE(SetPieceCarID[15], <<-0.040, -2.210, 0.887>>, <<0, -1, 0>>, 5, TRUE)
											BLAST_VEHICLE(SetPieceCarID[15], <<0.440, -0.802, 15.303>>, <<0, 0, 0>>, 12, TRUE)
											EXPLODE_VEHICLE(SetPieceCarID[15])
											
											REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
											
											PLAY_SOUND_FROM_ENTITY(-1, "Trevor_4_747_Carsplosion", SetPieceCarID[15])
											RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\Trv_4_747_Cop_Cars")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
//				//Turn off car 17 recording if player bumps into it
//				IF DOES_ENTITY_EXIST(SetPieceCarID[17])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[17])
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[17])	
//							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SetPieceCarID[17])
//									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[17])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				//Request anything needed for next stage
				IF DOES_ENTITY_EXIST(EnemyCar)
					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 112000
//									CREATE_ROAD_BLOCK()
								REQUEST_WAYPOINT_RECORDING("BB_MOLLY_2")
								REQUEST_ANIM_DICT("misssolomon_3")
								REQUEST_ANIM_DICT("move_f@film_reel_arms")
								ChaseStage = 1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		
			BREAK
			
			CASE 1
				
				//Run this check this stage aswell as next for checking if player is close enough to trigger cut
				IF playerHasGotCloseEnough = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-887.156067,-2957.106201,12.467661>>, <<-848.108032,-2997.587646,18.234352>>, 5.000000)
						PRINTSTRING("playerHasGotCloseEnough = TRUE") PRINTNL()
						playerHasGotCloseEnough = TRUE
					ENDIF
				ENDIF					
				
				//Clean up the AI cop cars
				IF DOES_ENTITY_EXIST(AIPoliceCar[0])
					IF IS_VEHICLE_DRIVEABLE(AIPoliceCar[0])
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(AIPoliceCar[0])
						SET_VEHICLE_AS_NO_LONGER_NEEDED(AIPoliceCar[0])
					ENDIF
				ENDIF
//				IF DOES_ENTITY_EXIST(AIPoliceCar[1])
//					IF IS_VEHICLE_DRIVEABLE(AIPoliceCar[1])
//						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(AIPoliceCar[1])
//						SET_VEHICLE_AS_NO_LONGER_NEEDED(AIPoliceCar[1])
//					ENDIF
//				ENDIF
				IF DOES_ENTITY_EXIST(AIPoliceCop[0])
					IF NOT IS_PED_INJURED(AIPoliceCop[0])
						CLEAR_PED_TASKS(AIPoliceCop[0])
						SET_PED_AS_NO_LONGER_NEEDED(AIPoliceCop[0])
					ENDIF
				ENDIF
//				IF DOES_ENTITY_EXIST(AIPoliceCop[1])
//					IF NOT IS_PED_INJURED(AIPoliceCop[1])
//						CLEAR_PED_TASKS(AIPoliceCop[1])
//						SET_PED_AS_NO_LONGER_NEEDED(AIPoliceCop[1])
//					ENDIF
//				ENDIF
				
			
//				//Turn off car 17 recording if player bumps into it
//				IF DOES_ENTITY_EXIST(SetPieceCarID[17])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[17])
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[17])	
//							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SetPieceCarID[17])
//									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[17])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF			
			
				//Request next stage assets
				IF nextStageAssetsRequested = FALSE
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 115000		
									REQUEST_WAYPOINT_RECORDING("Trev4_5")
									REQUEST_MODEL(prop_cs_film_reel_01)
									
									//Trigger load scene at cutscene area (see bug 1901121)
									IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
										NEW_LOAD_SCENE_START_SPHERE(<<-930.7, -2916.9, 13.4>>, 10)
									ENDIF		
									nextStageAssetsRequested = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Move stage on
				IF bMissionFailed = FALSE
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 120000
									IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_ARRIVE_AT_HANGAR")
										START_AUDIO_SCENE("SOL_3_ARRIVE_AT_HANGAR")
									ENDIF
									//B*-1956060
									REQUEST_VEHICLE_HIGH_DETAIL_MODEL(EnemyCar)
								ENDIF
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 125513
									STOP_PLAYBACK_RECORDED_VEHICLE(EnemyCar)
									FREEZE_ENTITY_POSITION(EnemyCar, TRUE)
			//						STOP_CAM_RECORDING(CamRagData)
									CLEANUP_PARTICLE_EFFECTS()
									CLEAR_PED_NON_CREATION_AREA()
									SET_PED_PATHS_IN_AREA(<<-833.86, -2564.17, 13.43>>, <<-814.37, -2521.04, 12.94>>, TRUE)
									
									STOP_AUDIO_SCENE("TREVOR_4_MAIN_CHASE")								
									
									//Creat the cams for the cutscene to follow
									IF NOT DOES_CAM_EXIST(CamInit)
										CamInit = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
									ENDIF
									IF NOT DOES_CAM_EXIST(CamDest)
										CamDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
									ENDIF					
									
									moveStageOnTimer = GET_GAME_TIMER()
									
									SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
									
									SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
									
									ChaseStage = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			CASE 2
			
				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				//B*-1956060
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(EnemyCar)
															
				IF playerHasGotCloseEnough = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-887.156067,-2957.106201,12.467661>>, <<-848.108032,-2997.587646,18.234352>>, 5.000000)
						
						//Force the vehicle to a high LOD
						//SET_VEHICLE_LOD_MULTIPLIER(EnemyCar, 1.5) using REQUEST_VEHICLE_HIGH_DETAIL_MODEL instead //B*-1956060
						
						PRINTSTRING("playerHasGotCloseEnough = TRUE") PRINTNL()
						playerHasGotCloseEnough = TRUE
					ENDIF
				ENDIF				
			
				//If player is close enough to the enemycar or the timer is up
				IF bMissionFailed = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-930.7, -2916.9, 13.4>>) < 45
						ChaseStage = 3
					ENDIF
					
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_ENTITY_ON_SCREEN(EnemyCar)
							IF playerHasGotCloseEnough = TRUE
								ChaseStage = 3
							ENDIF
						ENDIF
					ENDIF
					
					IF GET_GAME_TIMER() > (moveStageOnTimer + 5000)
					AND playerHasGotCloseEnough = TRUE
						ChaseStage = 3
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 3
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				//B*-1956060
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(EnemyCar)
				
				IF NOT IS_PED_INJURED(EnemyPed)
					IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
//						PRINTSTRING("Sync'd scene phase is ") PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(MollyRunsFromCarSyncdScene)) PRINTNL()
					ENDIF
				ENDIF
				
				IF EnemyPedTaskGiven = FALSE
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF HAS_ANIM_DICT_LOADED("misssolomon_3")
									IF NOT IS_PED_INJURED(EnemyPed)
										//Create the film reel
										IF NOT DOES_ENTITY_EXIST(ObjFilmReel)
											IF HAS_MODEL_LOADED(prop_cs_film_reel_01)
												ObjFilmReel = CREATE_OBJECT(prop_cs_film_reel_01, <<-929, -2917, 13>>)
												ATTACH_ENTITY_TO_ENTITY(ObjFilmReel, EnemyPed, GET_PED_BONE_INDEX(EnemyPed, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
											ENDIF
										ENDIF
										//Start the sync scene of Molly getting out the car
										IF NOT IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
											FREEZE_ENTITY_POSITION(EnemyCar, FALSE)
											MollyRunsFromCarSyncdScene = CREATE_SYNCHRONIZED_SCENE(<< -929.492, -2913.472, 14.160 >>, << -0.000, 0.000, -28.960 >>)		
											PLAY_SYNCHRONIZED_ENTITY_ANIM(EnemyCar, MollyRunsFromCarSyncdScene, "molly_escapes_car_car", "misssolomon_3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 0, 1000)
											TASK_SYNCHRONIZED_SCENE(EnemyPed, MollyRunsFromCarSyncdScene, "misssolomon_3", "molly_escapes_car_mol", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
											SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(MollyRunsFromCarSyncdScene, FALSE)
											TRIGGER_MUSIC_EVENT("TRV4_EXIT_CARS")
											
											
										ELSE
											EnemyPedTaskGiven = TRUE
										ENDIF
									ENDIF
								ELSE
									PRINTSTRING("Waiting on anim dict loading misssolomon_3") PRINTNL()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Move Molly into correct position once she's through the doors.
				IF enemyPedWaypointTask = FALSE
					IF EnemyPedTaskGiven = TRUE	
						IF NOT IS_PED_INJURED(EnemyPed)
							IF IS_ENTITY_IN_ANGLED_AREA(EnemyPed, <<-937.665588,-2927.864014,12.770065>>, <<-934.847900,-2929.483643,14.465233>>, 1.500000)
								IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
									STOP_SYNCHRONIZED_ENTITY_ANIM(EnemyPed, INSTANT_BLEND_OUT, TRUE)
								ENDIF
								CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed, TRUE)
								SET_ENTITY_COORDS(EnemyPed, <<-934.8388, -2932.8306, 12.9649>>)
								SET_ENTITY_HEADING(EnemyPed, 236.5739)	
								TASK_FOLLOW_WAYPOINT_RECORDING(EnemyPed, "BB_MOLLY_2", 1, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
								PRINTSTRING("Task to start WAYPOINT given now") PRINTNL()
								IF HAS_ANIM_DICT_LOADED("move_f@film_reel_arms")
									TASK_PLAY_ANIM(EnemyPed, "move_f@film_reel_arms", "run", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_LOOPING|AF_SECONDARY)
									PRINTSTRING("Task play anim called for film reel arms run") PRINTNL()
								ENDIF
								enemyPedWaypointTask = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Do short cutscene of Molly getting out the car
				IF bMissionFailed = FALSE
					IF ShortCutDone = FALSE
						IF ShortCutActive = FALSE
							IF NOT IS_PED_INJURED(EnemyPed)
								IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
									IF GET_SYNCHRONIZED_SCENE_PHASE(MollyRunsFromCarSyncdScene) > 0.18
									OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-930.7, -2916.9, 13.4>>) < 10
										icutsceneStage = 0
										ShortCutActive = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
						WHILE ShortCutActive = TRUE
							
							REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_LegalTrouble") 
							
							SWITCH icutsceneStage
								
								CASE 0
								
									SET_CAM_PARAMS(CamInit,<<-927.085144,-2921.173340,13.663039>>,<<1.738541,1.294743,28.057985>>,34.664043)
									SET_CAM_PARAMS(CamDest,<<-927.085144,-2921.173340,13.663039>>,<<1.738541,1.294742,32.599148>>,34.664043)
									SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 4000)
									
									DISPLAY_RADAR(FALSE)
									DISPLAY_HUD(FALSE)
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									
									//Delete the heli cam now
									IF DOES_ENTITY_EXIST(VehCam)
										DELETE_VEHICLE(VehCam)
									ENDIF								
									
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-918.6157, -2926.6455, 12.9667>>)
										SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 47.9465)								
									ELSE
										SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-918.6157, -2926.6455, 12.9667>>)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), 47.9465)	
									ENDIF
									
									IF NOT IS_PED_INJURED(EnemyPed)
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, EnemyPed, "MOLLY")
										SET_FORCE_FOOTSTEP_UPDATE(EnemyPed, TRUE)
									ENDIF
									
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 0.0, REPLAY_IMPORTANCE_HIGH)
									REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
									
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									doneMollyChat = FALSE
									
									icutsceneStage ++
									
								BREAK
								
								CASE 1
									
									IF NOT IS_PED_INJURED(EnemyPed)
										IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
											IF doneMollyChat = FALSE
												IF GET_SYNCHRONIZED_SCENE_PHASE(MollyRunsFromCarSyncdScene) > 0.30
													IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_GETOUT", CONV_PRIORITY_MEDIUM)
														
														REPLAY_RECORD_BACK_FOR_TIME(6.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
														
														//He wants to kill me! He wants to kill me!
														//That man's a murderer! He wants to kill me!
														doneMollyChat = TRUE
													ENDIF
												ENDIF
											ENDIF
											IF GET_SYNCHRONIZED_SCENE_PHASE(MollyRunsFromCarSyncdScene) > 0.50
												SET_CAM_PARAMS(CamInit,<<-926.093201,-2916.891113,14.470739>>,<<-3.471637,1.294743,125.831520>>,34.664043)
												SET_CAM_PARAMS(CamDest,<<-926.093201,-2916.891113,14.470739>>,<<-3.471636,1.294743,135.458405>>,34.664043)
												SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 4000)
												iScriptFlashTimer = GET_GAME_TIMER()
												icutsceneStage ++
											ENDIF
										ENDIF
									ENDIF
								
								BREAK
								
								CASE 2
									
									IF NOT IS_PED_INJURED(EnemyPed)
										IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
											// url:bugstar:2057705
											IF iScriptFlashTimer != -1
											AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON 
											AND (GET_GAME_TIMER() - iScriptFlashTimer) >= 2230
												ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
												PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
												iScriptFlashTimer = -1
											ENDIF
								
											IF GET_SYNCHRONIZED_SCENE_PHASE(MollyRunsFromCarSyncdScene) > 0.85
												DISPLAY_HUD(TRUE)
												DISPLAY_RADAR(TRUE)
												
												RENDER_SCRIPT_CAMS(FALSE, FALSE)
												SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
												
												iDoor = HASH("OfficeDoor")

												//Register the door
												ADD_DOOR_TO_SYSTEM(iDoor, V_ILEV_SS_DOOR5_R, <<-935.6410, -2927.1853, 14.0945>>)
												
												//set door state
												DOOR_SYSTEM_SET_OPEN_RATIO(iDoor, 1, FALSE, TRUE)
												
												SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
												IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
													TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
													// url:bugstar:2050416
													IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON
													AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
														FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
													ENDIF
												ENDIF
												
												SET_FORCE_FOOTSTEP_UPDATE(EnemyPed, FALSE)
												
												REPLAY_STOP_EVENT()
												REPLAY_RECORD_BACK_FOR_TIME(0.0, 4.0)
												
												failOnFootTimer = GET_GAME_TIMER()
												ShortCutActive = FALSE
												ShortCutDone = TRUE
												icutsceneStage ++
											ENDIF
										ENDIF
									ENDIF						
								
								BREAK
								
							ENDSWITCH		
							
							WAIT(0)
							
						ENDWHILE
						
					ENDIF
				ENDIF
				
				IF ShortCutDone = TRUE
					DOOR_SYSTEM_SET_DOOR_STATE(iDoor, DOORSTATE_FORCE_OPEN_THIS_FRAME, FALSE, TRUE)
					IF doorSoundPlayed = FALSE
						IF DOES_ENTITY_EXIST(EnemyPed)
							IF NOT IS_PED_INJURED(EnemyPed)
								IF IS_ENTITY_AT_COORD(EnemyPed, <<-935.3, -2927.3, 13>>, <<1.5,1.5,2>>)
									PLAY_SOUND_FROM_COORD(-1, "Trevor_4_747_Molly_Open_Doors", <<-935.1, -2927.6, 13.2>>)
									doorSoundPlayed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_AUDIO_SCENE_ACTIVE("SOL_3_MAIN_CHASE")
						STOP_AUDIO_SCENE("SOL_3_MAIN_CHASE")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("SOL_3_ARRIVE_AT_HANGAR")
						STOP_AUDIO_SCENE("SOL_3_ARRIVE_AT_HANGAR")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_ON_FOOT_CHASE")
						START_AUDIO_SCENE("SOL_3_ON_FOOT_CHASE")
					ENDIF
				ENDIF
				
				PRINTSTRING("ChaseStage = 3 STAGE_MAIN_CHASE") PRINTNL()
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CONTROL_OUTSIDE_COPS()
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-929.790771,-2928.213379,12.194990>>, <<-935.728333,-2924.685059,15.448324>>, 4.750000, FALSE, TRUE, TM_ON_FOOT)
//				AND enemyPedWaypointTask = TRUE
					IF enemyPedWaypointTask = FALSE	
						IF NOT IS_PED_INJURED(EnemyPed)
							IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "misssolomon_3", "molly_escapes_car_mol")
								STOP_SYNCHRONIZED_ENTITY_ANIM(EnemyPed, INSTANT_BLEND_OUT, TRUE)
							ENDIF
							CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed, TRUE)
							SET_ENTITY_COORDS(EnemyPed, <<-934.8388, -2932.8306, 12.9649>>)
							SET_ENTITY_HEADING(EnemyPed, 236.5739)	
							TASK_FOLLOW_WAYPOINT_RECORDING(EnemyPed, "BB_MOLLY_2", 1, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
							PRINTSTRING("Task to start WAYPOINT given now") PRINTNL()
							IF HAS_ANIM_DICT_LOADED("move_f@film_reel_arms")
								TASK_PLAY_ANIM(EnemyPed, "move_f@film_reel_arms", "run", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_LOOPING|AF_SECONDARY)
								PRINTSTRING("Task play anim called for film reel arms run") PRINTNL()
							ENDIF
							enemyPedWaypointTask = TRUE
						ENDIF
					ENDIF
					//Delete the end plane now so that it will be re-created in next stage with correct shading
					IF DOES_ENTITY_EXIST(EndPlane)
						DELETE_VEHICLE(EndPlane)
					ENDIF
					
					iControlFlag = 0
					icutsceneStage = 0
					missionStage = STAGE_ON_FOOT_CHASE
				ENDIF
			
			BREAK
		
		ENDSWITCH
		
	ENDIF
			
ENDPROC

//PROC DO_STAGE_END_OF_VEHICLE_CHASE_CUTSCENE()
//
//	IF iControlFlag = 0
//		
//		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//		
//		//DEBUG STAGE SELECTOR
//		#IF IS_DEBUG_BUILD
//		IF MissionStageBeingSkippedTo = TRUE
//			STAGE_SELECTOR_MISSION_SETUP()
//			MissionStageBeingSkippedTo = FALSE
//		ENDIF
//			jSkipReady 					= FALSE
//		#ENDIF		
//		
////═════════════════════════════════╡ SET FLAGS ╞═══════════════════════════════════	
//		camsRendered				= FALSE
//		heliChaseCamActivated 		= FALSE
//		setWaypointToAssistedMovement = FALSE
//		setupCutscene 				= FALSE
//		
//		KILL_ANY_CONVERSATION()
//		
//		REMOVE_CAM_RECORDING(1, "BB_Chase")	
//		REMOVE_ANIM_DICT("misstrevor4")
//		
//		REQUEST_CUTSCENE("TRV_4_MCS_1_A")
//		
//		WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(38, "BB_Chase")
//		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
//			PRINTSTRING("Waiting on waypoint/vehicle recordings loading") PRINTNL()
//			WAIT(0)
//		ENDWHILE
//		
//		SET_MAX_WANTED_LEVEL(5)
//		
//		IF DOES_BLIP_EXIST(EnemyPedBlip)
//			REMOVE_BLIP(EnemyPedBlip)
//		ENDIF			
//		
//		//Call every frame to give heli cam security cam overlay.
//		OVERLAY_SET_SECURITY_CAM(TRUE,TRUE)
//	
//		SET_TEXT_RENDER_ID (screenRT)
//		DRAW_SCALEFORM_MOVIE(ScaleformMovie, 0.5, 0.5, 0.95, 1.0, 255,255,255,0)
//			
////		CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING(ScaleformMovie, "SET_TEXT", "TRV4_NEWS1", "TRV4_NEWS2", "TRV4_NEWS3", "TRV4_NEWS4")			
//		
//		//calculate the uber recording play back speed.
//		IF NOT IS_ENTITY_DEAD(EnemyCar)
//			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//	//				PRINTSTRING("EnemyCar Playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(EnemyCar)) PRINTNL()
//					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 100, 2, 1)
//					SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//					UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
//				ENDIF
//			ENDIF		
//		ENDIF	
//		
//		IF heliChaseCamActivated = FALSE
//			IF NOT DOES_CAM_EXIST(heliChaseCam)
//				IF DOES_ENTITY_EXIST(SetPieceCarID[20])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//						IF NOT IS_PED_INJURED(EnemyPed)
//							PRINTSTRING("Creating helichaseCam") PRINTNL()
//							// create the heli cam
//							heliChaseCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//							// attach it to the helicopter
//							ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[20], heli_chase_cam_offset, TRUE)
//							// have it point at the player
//							POINT_CAM_AT_ENTITY(heliChaseCam, EnemyPed, <<0.0,0.0,0.0>>)
//							// activate it
//							SET_CAM_ACTIVE(heliChaseCam, TRUE)    
//							heli_chase_cam_active = TRUE       
//							has_cam_been_created = TRUE
//							heliChaseCamActivated = TRUE
//						ENDIF 
//					ENDIF
//				ELSE
//					PRINTSTRING("SetPieceCarID[20] does not exist") PRINTNL()
//				ENDIF
//			ELSE
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//					// attach it to the helicopter
//					ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[20], heli_chase_cam_offset, TRUE)
//					// have it point at the player
//					POINT_CAM_AT_ENTITY(heliChaseCam, EnemyPed, <<0.0,0.0,0.0>>)
//					// activate it
//					SET_CAM_ACTIVE(heliChaseCam, TRUE)  
//					PRINTSTRING("heliChaseCamActivated = TRUE") PRINTNL()
//					heliChaseCamActivated = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF NOT DOES_CAM_EXIST(CamInit)
//			CamInit = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-927.523071,-2926.444580,14.633916>>,<<-4.571874,0.032315,0.704430>>,21.377052)
//		ELSE
//			STOP_CAM_POINTING(CamInit)
//			SET_CAM_PARAMS(CamInit, <<-927.523071,-2926.444580,14.633916>>,<<-4.571874,0.032315,0.704430>>,21.377052)
//		ENDIF
//		IF NOT DOES_CAM_EXIST(CamDest)
//			CamDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-928.262939,-2927.490479,14.509108>>,<<-4.338459,-0.081095,4.252034>>,21.377052)
//		ELSE
//			STOP_CAM_POINTING(CamDest)
//			SET_CAM_PARAMS(CamDest, <<-928.262939,-2927.490479,14.509108>>,<<-4.338459,-0.081095,4.252034>>,21.377052)
//		ENDIF			
//		
//		TRIGGER_MUSIC_EVENT("TRV4_EXIT_CARS")
//		
//		IF NOT IS_SCREEN_FADED_IN()
////			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
//			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//		ENDIF		
//		
//		icutsceneStage = 0
//		iControlFlag = 1
//	ENDIF
//	
//	IF iControlFlag = 1
//	
//		//calculate the uber recording play back speed.
//		IF NOT IS_ENTITY_DEAD(EnemyCar)
//			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//					PRINTSTRING("EnemyCar Playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(EnemyCar)) PRINTNL()
//					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 100, 2, 1)
//					SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//					UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
//				ELSE
//					PRINTSTRING("Playback is NOT going on for enemycar") PRINTNL()
//				ENDIF
//			ENDIF		
//		ENDIF	
//	
//		//Keep checking this till it returns true
//		IF heliChaseCamActivated = FALSE
//			IF NOT DOES_CAM_EXIST(heliChaseCam)
//				IF DOES_ENTITY_EXIST(SetPieceCarID[20])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//						IF NOT IS_PED_INJURED(EnemyPed)
//							// create the heli cam
//							heliChaseCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//							// attach it to the helicopter
//							ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[20], heli_chase_cam_offset, TRUE)
//							// have it point at the player
//							POINT_CAM_AT_ENTITY(heliChaseCam, EnemyPed, <<0.0,0.0,0.0>>)
//							// activate it
//							SET_CAM_ACTIVE(heliChaseCam, TRUE)    
//							heli_chase_cam_active = TRUE       
//							has_cam_been_created = TRUE
//							heliChaseCamActivated = TRUE
//						ENDIF 
//					ENDIF
//				ELSE
//					PRINTSTRING("SetPieceCarID[20] does not exist") PRINTNL()
//				ENDIF
//			ELSE
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//					// attach it to the helicopter
//					ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[20], heli_chase_cam_offset, TRUE)
//					// have it point at the player
//					POINT_CAM_AT_ENTITY(heliChaseCam, EnemyPed, <<0.0,0.0,0.0>>)
//					// activate it
//					SET_CAM_ACTIVE(heliChaseCam, TRUE)  
//					heliChaseCamActivated = TRUE
//				ENDIF
//			ENDIF
//		ENDIF		
//	
//		IF camsRendered = FALSE
//			IF heliChaseCamActivated = TRUE
//				IF DOES_ENTITY_EXIST(SetPieceCarID[20])
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//						SET_VEHICLE_ENGINE_ON(SetPieceCarID[20], TRUE, TRUE)
//						SET_HELI_BLADES_FULL_SPEED(SetPieceCarID[20])
//					ENDIF
//				ENDIF
//				SET_FRONTEND_RADIO_ACTIVE(FALSE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				DISPLAY_RADAR(FALSE)
//				DISPLAY_HUD(FALSE)
//				
//				iskipCutsceneTimer = GET_GAME_TIMER()
//				
//				icutsceneStage = 0
//				iControlFlag = 2				
//				bIsCutSceneRunning = TRUE
//				camsRendered = TRUE
//			ENDIF
//		ENDIF		
//	
//	ENDIF
//	
//	IF iControlFlag = 2
//		
//		WHILE bIsCutSceneRunning
//			
//			//Keep checking this till it returns true
//			IF heliChaseCamActivated = FALSE
//				IF NOT DOES_CAM_EXIST(heliChaseCam)
//					IF DOES_ENTITY_EXIST(SetPieceCarID[20])
//						IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//							IF NOT IS_PED_INJURED(EnemyPed)
//								// create the heli cam
//								heliChaseCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//								// attach it to the helicopter
//								ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[20], heli_chase_cam_offset, TRUE)
//								// have it point at the player
//								POINT_CAM_AT_ENTITY(heliChaseCam, EnemyPed, <<0.0,0.0,0.0>>)
//								// activate it
//								SET_CAM_ACTIVE(heliChaseCam, TRUE)    
//								heli_chase_cam_active = TRUE       
//								has_cam_been_created = TRUE
//								heliChaseCamActivated = TRUE
//							ENDIF 
//						ENDIF
//					ELSE
//						PRINTSTRING("SetPieceCarID[20] does not exist") PRINTNL()
//					ENDIF
//				ELSE
//					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
//						// attach it to the helicopter
//						ATTACH_CAM_TO_ENTITY(heliChaseCam, SetPieceCarID[20], heli_chase_cam_offset, TRUE)
//						// have it point at the player
//						POINT_CAM_AT_ENTITY(heliChaseCam, EnemyPed, <<0.0,0.0,0.0>>)
//						// activate it
//						SET_CAM_ACTIVE(heliChaseCam, TRUE)  
//						heliChaseCamActivated = TRUE
//					ENDIF
//				ENDIF
//			ENDIF			
//			
//			IF GET_GAME_TIMER() > (iskipCutsceneTimer + 1000) 
//				IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//						SkipCutscene()
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//calculate the uber recording play back speed.
//			IF NOT IS_ENTITY_DEAD(EnemyCar)
//				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//						PRINTSTRING("EnemyCar Playback time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(EnemyCar)) PRINTNL()
//						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(EnemyCar, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 100, 2, 1)
//						SET_PLAYBACK_SPEED(EnemyCar, fPlaybackSpeed)
//						UPDATE_UBER_PLAYBACK(EnemyCar, fPlaybackSpeed)
//					ELSE
//						PRINTSTRING("Playback is NOT going on for enemycar") PRINTNL()
//					ENDIF
//				ENDIF		
//			ENDIF		
//			
//			PRINTSTRING("icutscenestage = ") PRINTINT(icutsceneStage) PRINTNL()
//			
////			PRINTSTRING("TimerA = ") PRINTINT(TIMERA()) PRINTNL()
//			
//			WAIT(0)
//			
//			SWITCH icutsceneStage
//			
//				CASE 0
//					
//					//Start cutscene				
//					//Call every frame to give heli cam security cam overlay.
//					OVERLAY_SET_SECURITY_CAM(TRUE,TRUE)
//
//					SET_TEXT_RENDER_ID (screenRT)
//					DRAW_SCALEFORM_MOVIE(ScaleformMovie, 0.5, 0.5, 0.95, 1.0, 255,255,255,0)					
//					
//					//request the mocap section of this cutscene
//					IF setupCutscene = FALSE
//						// turn on the camera
//						SET_CAM_ACTIVE(heliChaseCam, TRUE)
//						SET_CAM_FOV(heliChaseCam, 40)
//						SHAKE_CAM(heliChaseCam, "ROAD_VIBRATION_SHAKE", 1)
//						setupCutscene = TRUE
//					ENDIF
//					
//					IF camsRendered = TRUE
//						INFORM_MISSION_STATS_SYSTEM_OF_INENGINE_CUTSCENE_START()
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//						
//						IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//							SET_ENTITY_COORDS(PlayerCar, << -876.7273, -3012.7397, 12.9450 >>)
//							SET_ENTITY_HEADING(PlayerCar, 351.3571)
//							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PlayerCar)
//							ENDIF
//							START_PLAYBACK_RECORDED_VEHICLE(PlayerCar, 38, "BB_Chase")
//							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(PlayerCar, 2500)
//						ENDIF
//						
////						IF NOT IS_PED_INJURED(EnemyPed)
////							IF DOES_ENTITY_EXIST(RoadBlockCop[0])
////								IF NOT IS_PED_INJURED(RoadBlockCop[0])
////									CLEAR_PED_TASKS(RoadBlockCop[0])
////									TASK_SHOOT_AT_COORD(RoadBlockCop[0], <<-927.05, -2915.95, 14.55>>, -1, FIRING_TYPE_RANDOM_BURSTS)
////								ENDIF
////							ENDIF
////							IF DOES_ENTITY_EXIST(RoadBlockCop[1])
////								IF NOT IS_PED_INJURED(RoadBlockCop[1])
////									CLEAR_PED_TASKS(RoadBlockCop[1])
////									TASK_SHOOT_AT_COORD(RoadBlockCop[1], <<-927.05, -2915.95, 14.55>>, -1, FIRING_TYPE_RANDOM_BURSTS)
////								ENDIF
////							ENDIF
////							IF DOES_ENTITY_EXIST(RoadBlockCop[2])
////								IF NOT IS_PED_INJURED(RoadBlockCop[2])
////								AND NOT IS_PED_INJURED(EnemyPed)
////									CLEAR_PED_TASKS(RoadBlockCop[2])
////									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(RoadBlockCop[2], TRUE)
////									TASK_AIM_GUN_AT_ENTITY(RoadBlockCop[2], EnemyPed, -1, TRUE)
////								ENDIF
////							ENDIF
////						ENDIF
//						
//						icutsceneStage++
//					ELSE
//						PRINTSTRING("Waiting for cutscene camera to start") PRINTNL()
//					ENDIF
//			
//				BREAK
//				
//				CASE 1
//					
////					IF camsRendered = FALSE
////						IF heliChaseCamActivated = TRUE
////							IF DOES_ENTITY_EXIST(SetPieceCarID[20])
////								IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[20])
////									SET_VEHICLE_ENGINE_ON(SetPieceCarID[20], TRUE, TRUE)
////									SET_HELI_BLADES_FULL_SPEED(SetPieceCarID[20])
////								ENDIF
////							ENDIF						
////						
////							RENDER_SCRIPT_CAMS(TRUE, FALSE)
////							DISPLAY_RADAR(FALSE)
////							DISPLAY_HUD(FALSE)
////							camsRendered = TRUE
////						ENDIF
////					ENDIF
//					
//					//Call every frame to give heli cam security cam overlay.
//					OVERLAY_SET_SECURITY_CAM(TRUE,TRUE)					
//					
//					SET_TEXT_RENDER_ID (screenRT)
//					DRAW_SCALEFORM_MOVIE(ScaleformMovie, 0.5, 0.5, 0.95, 1.0, 255,255,255,0)
//						
////					CALL_SCALEFORM_MOVIE_METHOD_WITH_STRING(ScaleformMovie, "SET_TEXT", "TRV4_NEWS1", "TRV4_NEWS2", "TRV4_NEWS3", "TRV4_NEWS4")					
//					
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 126093
//								IF NOT IS_PED_INJURED(EnemyPed)
////									CLEAR_PED_TASKS(EnemyPed)
////									TASK_LEAVE_VEHICLE(EnemyPed, EnemyCar, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|ECF_DONT_CLOSE_DOOR)
//									icutsceneStage++
//								ENDIF
//							ENDIF
//						ELSE
//							IF NOT IS_PED_INJURED(EnemyPed)
////								CLEAR_PED_TASKS(EnemyPed)
////								TASK_LEAVE_VEHICLE(EnemyPed, EnemyCar, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|ECF_DONT_CLOSE_DOOR)
//								icutsceneStage++
//							ENDIF	
//						ENDIF
//					ENDIF
//					
//				BREAK
//				
//				CASE 2
//				
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 127000				
//								icutsceneStage++
//								bIsCutSceneRunning = FALSE
//							ENDIF
//						ELSE
//							icutsceneStage++
//							bIsCutSceneRunning = FALSE	
//						ENDIF
//					ELSE
//						icutsceneStage++
//						bIsCutSceneRunning = FALSE
//					ENDIF
//					
//				BREAK
//				
//			ENDSWITCH
//		
//		ENDWHILE
//		
//		IF bIsCutSceneRunning = FALSE
//			
//			IF DOES_CAM_EXIST(heliChaseCam)
//				SET_CAM_ACTIVE(heliChaseCam, FALSE)
//				DESTROY_CAM(heliChaseCam)
//			ENDIF
//			SET_CAM_ACTIVE(CamDest, FALSE)
//			SET_CAM_ACTIVE(CamInit, FALSE)
//			STOP_CAM_POINTING(CamInit)
//			STOP_CAM_POINTING(CamDest)
//			
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)			
//			
//			IF AudioSceneStarted[2] = TRUE
//				IF NOT HAS_SOUND_FINISHED(NewsHeliSoundID)
//				 	STOP_SOUND(NewsHeliSoundID)
//				ENDIF
//			
//				//Stop sound effects of heli now
//				RELEASE_SOUND_ID(NewsHeliSoundID)
//				NewsHeliSoundIDCreated = FALSE
//				AudioSceneStarted[2] = FALSE
//			ENDIF
//			
//			SET_FRONTEND_RADIO_ACTIVE(TRUE)			
//			
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			icutsceneStage 	= 0
//			iControlFlag 	= 3
//			
//		ENDIF
//		
//	ENDIF
//	
//	IF iControlFlag = 3
//		
//		SWITCH icutsceneStage
//		
//			CASE 0
//		
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					PRINTSTRING("Waiting for cutscene to load") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//		
//				//Register entities for cutscene
//				IF NOT IS_PED_INJURED(EnemyPed)
//					REGISTER_ENTITY_FOR_CUTSCENE(EnemyPed, "Taos_Translator", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_MOLLY)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//						STOP_PLAYBACK_RECORDED_VEHICLE(EnemyCar)
//					ENDIF
//					REGISTER_ENTITY_FOR_CUTSCENE(EnemyCar, "Translators_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, COGCABRIO)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(PlayerCar)
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(PlayerCar)
//						STOP_PLAYBACK_RECORDED_VEHICLE(PlayerCar)
//					ENDIF
//					REGISTER_ENTITY_FOR_CUTSCENE(PlayerCar, "Trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, RAPIDGT)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[14])
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[14])
//						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[14])
//					ENDIF
//					REGISTER_ENTITY_FOR_CUTSCENE(SetPieceCarID[14], "Car_against_cogcabrio", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, POLICE)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[16])
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[16])
//						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[16])
//					ENDIF
//					REGISTER_ENTITY_FOR_CUTSCENE(SetPieceCarID[16], "Car_behind_cogcabrio", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, POLICE)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[17])
//					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[17])
//						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[17])
//					ENDIF
//					REGISTER_ENTITY_FOR_CUTSCENE(SetPieceCarID[17], "Car_near_containers", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, POLICE)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(RoadBlockCopCar[0])
//					REGISTER_ENTITY_FOR_CUTSCENE(RoadBlockCopCar[0], "car_left_roadblock", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, POLICE)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(RoadBlockCopCar[1])
//					REGISTER_ENTITY_FOR_CUTSCENE(RoadBlockCopCar[1], "car_middle_roadblock", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, POLICE)
//				ENDIF
//				IF IS_VEHICLE_DRIVEABLE(RoadBlockCopCar[2])
//					REGISTER_ENTITY_FOR_CUTSCENE(RoadBlockCopCar[2], "car_right_roadblock", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, POLICE)
//				ENDIF
//				
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//				
//				START_CUTSCENE()
//				
//				icutsceneStage ++
//				
//			BREAK
//			
//			CASE 1 
//			
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator", IG_MOLLY)
//					//Move the enemy ped into the correct place so as not to be too close to the player
//					IF DOES_ENTITY_EXIST(EnemyPed)
//						IF NOT IS_PED_INJURED(EnemyPed)
//							CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed)
//							SET_ENTITY_COORDS(EnemyPed, << -920.5093, -2943.9299, 12.9451 >>)
//							SET_ENTITY_HEADING(EnemyPed, 157.6203)
//						ENDIF
//					ENDIF			
//				ENDIF
//				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
//					TASK_GO_STRAIGHT_TO_COORD(player_ped_id(), << -935.4, -2927.9, 13.4 >>, PEDMOVE_RUN, -1) 
//					FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN, TRUE)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				ENDIF				
//				
//				IF NOT IS_CUTSCENE_ACTIVE()				
//					
//					SETTIMERB(0)
//					
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//										
//					DISPLAY_RADAR(TRUE)
//					DISPLAY_HUD(TRUE)
//					CLEAR_TIMECYCLE_MODIFIER()
//					
//					//Set up assisted movement for the player
//					IF setWaypointToAssistedMovement = FALSE
//						USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("Trev4_5", TRUE, 0.5, 1)
//						setWaypointToAssistedMovement = TRUE
//					ENDIF						
//					
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//						FREEZE_ENTITY_POSITION(EnemyCar, FALSE)
//					ENDIF
//					
//					//Set Players wanted level
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
//					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())					
//					
//					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					INFORM_MISSION_STATS_SYSTEM_OF_INENGINE_CUTSCENE_END()
//					
//					//Handle dialogue
//					IF NOT IS_MESSAGE_BEING_DISPLAYED()
//					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_POLICE", "T1M4_POLICE_1", CONV_PRIORITY_AMBIENT_MEDIUM)
//							//Police! Freeze!
//						ENDIF
//					ENDIF
//					
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-934.5091, -2926.1409, 12.9450>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 158.9638)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)					
//					
//					icutsceneStage ++
//					iControlFlag = 4
//					
//				ENDIF
//				
//			BREAK
//			
//		ENDSWITCH
//		
//	ENDIF
//	
//	IF iControlFlag = 4
//		
//		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
//		
//		iControlFlag = 0
//		missionStage = STAGE_ON_FOOT_CHASE
//		
//	ENDIF
//	
//		IF iskip_cutscene_flag = 1
//			
//			//move enemycar
//			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//					STOP_PLAYBACK_RECORDED_VEHICLE(EnemyCar)
//				ENDIF
//				SET_ENTITY_COORDS(EnemyCar, << -928.5512, -2916.5745, 12.9447 >>)
//				SET_ENTITY_HEADING(EnemyCar, 61.5515)
//			ENDIF
//			
//			//move setpiece car 16
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[16])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[16])
//					STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[16])
//				ENDIF
//				SET_ENTITY_COORDS(SetPieceCarID[16], << -924.6111, -2919.7397, 12.9447 >>)
//				SET_ENTITY_HEADING(SetPieceCarID[16], 53.6701)
//			ENDIF		
//			
//			//move setpiece car 14
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[14])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[14])
//					STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[14])
//				ENDIF
//				SET_ENTITY_COORDS(SetPieceCarID[14], << -925.5470, -2914.5205, 12.9447 >>)
//				SET_ENTITY_HEADING(SetPieceCarID[14], 105.4010)
//			ENDIF
//			
//			//move setpiece car 17
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[17])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[17])
//					STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[17])
//				ENDIF
//				SET_ENTITY_COORDS(SetPieceCarID[17], << -912.7549, -2918.8752, 12.9447 >>)
//				SET_ENTITY_HEADING(SetPieceCarID[17], 118.5475)
//			ENDIF			
//			
//			//move player
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -933.7552, -2925.8254, 12.9447 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 146.1716)
//			
//			//move translator
//			IF NOT IS_PED_INJURED(EnemyPed)
//				CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed)
//				SET_ENTITY_COORDS(EnemyPed, << -920.3151, -2942.3096, 12.9448 >>)
//				SET_ENTITY_HEADING(EnemyPed, 153.5699)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed, TRUE)
//			ENDIF
//			
//			IF setWaypointToAssistedMovement = FALSE
//				USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("Trev4_5", TRUE, 0.5, 1)
//				setWaypointToAssistedMovement = TRUE
//			ENDIF			
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			
//			IF AudioSceneStarted[2] = TRUE
//				IF NOT HAS_SOUND_FINISHED(NewsHeliSoundID)
//				 	STOP_SOUND(NewsHeliSoundID)
//				ENDIF									
//			
//				//Stop sound effects of heli now
//				RELEASE_SOUND_ID(NewsHeliSoundID)
//				NewsHeliSoundIDCreated = FALSE
//				AudioSceneStarted[2] = FALSE
//			ENDIF
//			
//			SET_FRONTEND_RADIO_ACTIVE(TRUE)
//			
//			iskip_cutscene_flag = 0
//			bIsCutSceneRunning = FALSE
//			
//		ENDIF
//	
//ENDPROC

//PURPOSE: Calculates the playback speed of the waypoint recording relative to players distance from ped.
PROC CALCULATE_PED_WAYPOINT_PLAYBACK_SPEED()
		
		PlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(EnemyPed)
			IF NOT IS_PED_INJURED(EnemyPed)
				EnemyPedCoords = GET_ENTITY_COORDS(EnemyPed)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(EnemyPed)
			IF NOT IS_PED_INJURED(EnemyPed)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
					IF GET_DISTANCE_BETWEEN_COORDS(PlayerCoords, EnemyPedCoords) < 25
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, 3)
					ELSE
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, 2)
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
ENDPROC

PROC DO_STAGE_ON_FOOT_CHASE()

////PRINTSTRING("iControlFlag = ") PRINTINT(iControlFlag) PRINTNL()
//IF NOT IS_PED_INJURED(EnemyPed)
//	IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
////		PRINTSTRING("Waypoint stage is") PRINTINT(GET_PED_WAYPOINT_PROGRESS(EnemyPed)) PRINTNL()
//	ENDIF
//ENDIF
	
	//change the speed of Molly so she can't get too far ahead of the player
	IF DOES_ENTITY_EXIST(EnemyPed)
		IF NOT IS_PED_INJURED(EnemyPed)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed) < 17
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, PEDMOVEBLENDRATIO_SPRINT, TRUE)
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed) < 22
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed) > 17
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, PEDMOVEBLENDRATIO_RUN, TRUE)
				ENDIF		
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-921.7, -2946.4, 13.4>>) > 8
				AND NOT IS_ENTITY_ON_SCREEN(EnemyPed)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed) > 22
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, PEDMOVEBLENDRATIO_WALK, TRUE)
					ENDIF	
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed) > 22
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, PEDMOVEBLENDRATIO_RUN, TRUE)
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iControlFlag > 0
		
		//Player can only walk when this is set to true
		IF playerSetToWalk = FALSE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-926.025757,-2958.614502,12.464674>>, <<-930.652466,-2956.061035,16.459446>>, 2.750000)
				playerSetToWalk = TRUE
			ENDIF
		ELSE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			
			IF fPlayerMaxMoveSpeed < 1
				fPlayerMaxMoveSpeed = 1
			ENDIF
			fPlayerMaxMoveSpeed = (fPlayerMaxMoveSpeed * 0.98)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), fPlayerMaxMoveSpeed) //PEDMOVE_WALK)			
		ENDIF	
	
		//Create endplane
		IF NOT DOES_ENTITY_EXIST(EndPlane)
			REQUEST_MODEL(JET)
			IF HAS_MODEL_LOADED(JET)
				EndPlane = CREATE_VEHICLE(JET, << -952.1345, -2990.2688, 12.9451 >>, 240.7726)
				SET_VEHICLE_LIVERY(EndPlane, 2)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EndPlane, FALSE)
				FREEZE_ENTITY_POSITION(EndPlane, TRUE)
				SET_ENTITY_PROOFS(EndPlane, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(JET)
				SET_ENTITY_LOD_DIST(EndPlane, 500)
			ENDIF
		ENDIF
		
		//Air worker
		IF NOT DOES_ENTITY_EXIST(AirWorker)
			REQUEST_MODEL(S_M_Y_AirWorker)
			REQUEST_ANIM_DICT("misssolomon_3")
			REQUEST_MODEL(P_AMB_CLIPBOARD_01)
			IF HAS_MODEL_LOADED(S_M_Y_AirWorker)
//			AND HAS_ANIM_DICT_LOADED("amb@world_human_clipboard@male@base")
			AND HAS_ANIM_DICT_LOADED("misssolomon_3")
			AND HAS_MODEL_LOADED(P_AMB_CLIPBOARD_01)
				AirWorker = CREATE_PED(PEDTYPE_MISSION, S_M_Y_AirWorker, <<-932.1812, -2967.9468, 12.9451>>, 170.7182)
				SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PED_CAN_BE_TARGETTED(AirWorker, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, TRUE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, AirWorker, "HangerWorker")
				WorkersClipBoard = CREATE_OBJECT(P_AMB_CLIPBOARD_01, <<-932.1812, -2967.9468, 12.9451>>)
				ATTACH_ENTITY_TO_ENTITY(WorkersClipBoard, AirWorker, GET_PED_BONE_INDEX(EnemyPed, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>) //GET_PED_BONE_COORDS
//				TASK_PLAY_ANIM(AirWorker, "amb@world_human_clipboard@male@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				WorkerSyncScene = CREATE_SYNCHRONIZED_SCENE(<< -932.847, -2978.498, 13.946 >>, << -0.000, 0.000, 16.560 >>)
				TASK_SYNCHRONIZED_SCENE(AirWorker, WorkerSyncScene, "misssolomon_3", "_start_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE|SYNCED_SCENE_DONT_INTERRUPT)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_AirWorker)
				SET_MODEL_AS_NO_LONGER_NEEDED(P_AMB_CLIPBOARD_01)
			ENDIF
		ENDIF	
	
		IF NOT DOES_ENTITY_EXIST(EndCop[0])
			REQUEST_MODEL(S_M_Y_COP_01)
			IF HAS_MODEL_LOADED(S_M_Y_COP_01)
				EndCop[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, << -969.0600, -2955.1858, 12.9450 >>, 241.1703)
				GIVE_WEAPON_TO_PED(EndCop[0], WEAPONTYPE_PISTOL, 1000, TRUE)
				SET_PED_ACCURACY(EndCop[0], 20)
				SET_ENTITY_LOAD_COLLISION_FLAG(EndCop[0], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[0], TRUE)
				TASK_AIM_GUN_AT_ENTITY(EndCop[0], PLAYER_PED_ID(), -1, TRUE)
				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EndCop[0])
			ENDIF
		ENDIF		
		
		IF NOT DOES_ENTITY_EXIST(GetAwayPlane)
			REQUEST_MODEL(SHAMAL)
			IF HAS_MODEL_LOADED(SHAMAL)
				GetAwayPlane = CREATE_VEHICLE(SHAMAL,<< -968.3718, -2952.2976, 12.9451 >>, 114.9439)
				SET_VEHICLE_DOOR_OPEN(GetAwayPlane, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(GetAwayPlane)
				SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
				SET_ENTITY_HEALTH(GetAwayPlane, 2000)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(GetAwayPlane)
			IF IS_VEHICLE_DRIVEABLE(GetAwayPlane)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GetAwayPlane) > 5
					SET_VEHICLE_DOOR_OPEN(GetAwayPlane, SC_DOOR_FRONT_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF iControlFlag = 0

		//Player has reached a checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_ON_FOOT_CHASE")
		
		//Check if mission is on replay at this checkpoint
		IF MissionStageBeingSkippedTo = TRUE
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			STAGE_SELECTOR_MISSION_SETUP()
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
			MissionStageBeingSkippedTo = FALSE
		ENDIF	
//***************************************RESET FLAGS**********************************************
//		enemypedWaypointPlaybackResumed 	= FALSE
//		doneShout2Chat 						= FALSE
		Cop14TaskGiven 						= FALSE
		CopIdsGrabbed 						= FALSE
		playerSetToWalk 					= FALSE
		#IF IS_DEBUG_BUILD
			jSkipReady 						= TRUE
		#ENDIF
		
		fPlayerMaxMoveSpeed 				= 3	
		iSubStage							= 0
		
		//Clean up memory from uber chase
		CLEANUP_UBER_PLAYBACK(FALSE, TRUE)
		REMOVE_VEHICLE_RECORDING(45, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(35, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(2, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(54, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(1, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(9, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(61, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(63, "BB_CHASE")
		REMOVE_VEHICLE_RECORDING(64, "BB_CHASE")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ScaleformMovie)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("digitalOverlay")
		IF DOES_ENTITY_EXIST(EnemyCar)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(EnemyCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(EnemyCar)
		ENDIF
		IF DOES_ENTITY_EXIST(VehCam)
			DELETE_VEHICLE(VehCam)
		ENDIF	
		IF DOES_ENTITY_EXIST(setPiecePlanePilot[0])
			DELETE_PED(setPiecePlanePilot[0])
		ENDIF
		IF DOES_ENTITY_EXIST(SetPiecePlane[0])
			DELETE_VEHICLE(SetPiecePlane[0])
		ENDIF	
		
		SET_FAKE_WANTED_LEVEL(0)
		
		SET_FRONTEND_RADIO_ACTIVE(TRUE)
		
		IF NOT IS_PED_INJURED(EnemyPed)
			IF NOT DOES_BLIP_EXIST(EnemyPedBlip)
				EnemyPedBlip = ADD_BLIP_FOR_ENTITY(EnemyPed)
				SET_BLIP_AS_FRIENDLY(EnemyPedBlip, TRUE)
			ENDIF		
		ENDIF		
		
		SET_AUDIO_FLAG("PoliceScannerDisabled", TRUE)
		
//		//Store the interior hanger in memory for loading reasons.
//		InteriorHanger = GET_INTERIOR_AT_COORDS(<<-935.48, -2969.61, 14.67>>)
//		
//		IF IS_VALID_INTERIOR(InteriorHanger)
//			PIN_INTERIOR_IN_MEMORY(InteriorHanger)		
//			SET_INTERIOR_ACTIVE(InteriorHanger, TRUE)
//		ENDIF
		
		IF AudioSceneStarted[2] = TRUE
			IF NOT HAS_SOUND_FINISHED(NewsHeliSoundID)
			 	STOP_SOUND(NewsHeliSoundID)
			ENDIF
		
			//Stop sound effects of heli now
			RELEASE_SOUND_ID(NewsHeliSoundID)
			NewsHeliSoundIDCreated = FALSE
			AudioSceneStarted[2] = FALSE
		ENDIF		
		
		//Set Players wanted level
		SET_MAX_WANTED_LEVEL(5)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())		
		
		TRIGGER_MUSIC_EVENT("TRV4_RUN")
		
		IF NOT DOES_ENTITY_EXIST(EndPlane)
			REQUEST_MODEL(JET)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(AirWorker)
			REQUEST_MODEL(S_M_Y_AirWorker)
		ENDIF
		
		//Request everything required for end cops
		REQUEST_MODEL(SHAMAL)
		REQUEST_MODEL(S_M_Y_COP_01)
		REQUEST_MODEL(POLICE3)
		REQUEST_MODEL(P_AMB_CLIPBOARD_01)
		REQUEST_VEHICLE_RECORDING(68, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(69, "BB_Chase")	
//		REQUEST_ANIM_DICT("amb@world_human_clipboard@male@base")
		REQUEST_ANIM_DICT("misssolomon_3")
//		REQUEST_CUTSCENE("TRV_4_MCS_2")
		
		//For failing if player takes too long to enter hanger
		SETTIMERA(0)			
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
			USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("Trev4_5", TRUE, 1, 1)		
		ENDIF
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP()
			PRINT_NOW("TRV4_CHASE1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Chase the ~b~translator.
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF			
		
		iControlFlag = 1
	
	ENDIF	
	
	IF iControlFlag = 1	
//		
//		//Fail if player takes too long to enter the hanger.
//		IF TIMERA() > 15000
//			SetMissionFailed("TRV4_FAIL1") //~r~You lost the translator.
//		ENDIF
//		
////		IF DOES_ENTITY_EXIST(AirWorker)
////		AND DOES_ENTITY_EXIST(EndPlane)
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-935.4413, -2928.0610, 12.9451>>, <<3,3,2>>, FALSE, TRUE, TM_ON_FOOT)
////			CLEAR_PRINTS()
////			IF NOT IS_MESSAGE_BEING_DISPLAYED()
////			OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
////				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_HANGER", "T1M4_HANGER_1", CONV_PRIORITY_AMBIENT_MEDIUM)			
////					//Hey, you're not meant to be in here!
////				ENDIF
////			ENDIF
//			SETTIMERA(0)
//			iControlFlag = 2
//		ENDIF	
////		ENDIF
		
		iControlFlag = 2
		
	ENDIF
		
	IF iControlFlag = 2

		//Fail if player tries to be a wido and go round the wrong way.
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-880.365, -2942.26, 14>>, <<9.55, 15.69, 5>>)
			SetMissionFailed("TRV4_FAIL1") //~r~You lost the translator.
		ENDIF
		
		//Fail if player tries to be a wido and go round the wrong way.
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-945.725, -2893.15, 14>>, <<4.5, 9.56, 5>>)
			SetMissionFailed("TRV4_FAIL1") //~r~You lost the translator.
		ENDIF
		
		//Fail if player takes too long to enter the hanger.
		IF TIMERA() > 15000
			SetMissionFailed("TRV4_FAIL1") //~r~You lost the translator.
		ENDIF

		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -932.1196, -2934.3169, 12.9448 >>, <<2,2,2>>)
			IF NOT IS_PED_INJURED(EnemyPed)
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
					TASK_FOLLOW_WAYPOINT_RECORDING(EnemyPed, "BB_MOLLY_2", 10, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
					TASK_PLAY_ANIM(EnemyPed, "move_f@film_reel_arms", "run", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_LOOPING|AF_SECONDARY)
					PRINTSTRING("Waypoint task being given because mission has been skipped to here") PRINTNL()
				ENDIF
			ENDIF
			SETTIMERA(0)
			iControlFlag = 3
		ENDIF
	
	ENDIF
	
	IF iControlFlag = 3		
		
//		IF doneShout2Chat = FALSE
//			IF NOT IS_MESSAGE_BEING_DISPLAYED()
//			OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_SHOUT1", "T1M4_SHOUT1_2", CONV_PRIORITY_AMBIENT_MEDIUM)
//					//Molly, just give me my fucking film.
//					//Do you really think you can out run me?
//					SETTIMERB(0)
//					doneShout2Chat = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Debug option for playing mocap scene
		#IF IS_DEBUG_BUILD
			IF DOES_ENTITY_EXIST(EnemyPed)
				IF NOT IS_PED_INJURED(EnemyPed)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(EnemyPed), <<-933, -2966, 13>>) < 10
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
							distanceCheckRequired = FALSE
							iControlFlag = 0
							missionStage = STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
		IF NOT IS_PED_INJURED(EnemyPed)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
				IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 18
				AND bMissionFailed = FALSE
					distanceCheckRequired = FALSE
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, PEDMOVEBLENDRATIO_SPRINT, TRUE)
					iControlFlag = 0
					missionStage = STAGE_PED_RUNS_TOWARDS_ENGINE
				ENDIF
			ENDIF
		ENDIF
		
		//As a safety check incase something goes wrong, if the timer gets over 50 seconds warp Molly to correct position and move stage on
		IF TIMERA() > 50000
			IF NOT IS_PED_INJURED(EnemyPed)
				CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed)
				SET_ENTITY_COORDS(EnemyPed, <<-932.5, -2964.1, 13.4>>)
				TASK_FOLLOW_WAYPOINT_RECORDING(EnemyPed, "BB_MOLLY_2", 18, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
				TASK_PLAY_ANIM(EnemyPed, "move_f@film_reel_arms", "run", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_LOOPING|AF_SECONDARY)
				distanceCheckRequired = FALSE
				iControlFlag = 0
				missionStage = STAGE_PED_RUNS_TOWARDS_ENGINE
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

//
////PURPOSE: Short cutscene where the enemy ped shoots at the pilot of a 747 as he is getting into the plane.
//PROC DO_STAGE_PED_SHOOTS_PILOT_CUTSCENE()
//
//
//	IF iControlFlag = 0
//		
////**************************************RESET FLAGS********************************************
//		playergiventask 		= FALSE	
//		staticCamActive			= FALSE
//		
//		//Create cams
//		IF NOT DOES_CAM_EXIST(CamInit)
//			CamInit = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//		ELSE
//			STOP_CAM_POINTING(CamInit)
//			SET_CAM_PARAMS(CamInit, <<-924.351807,-3001.322754,14.538351>>, <<-0.345139,-0.004003,16.386713>>, 16.070929)
//		ENDIF
//	
//		IF NOT DOES_CAM_EXIST(CamDest)
//			CamDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//		ELSE
//			STOP_CAM_POINTING(CamDest)
//			SET_CAM_PARAMS(CamDest, <<-928.675781,-3000.916016,17.847477>>, <<-7.214854,-0.004003,10.014034>>, 16.070929)
//		ENDIF
//		
//		IF NOT DOES_CAM_EXIST(CamStatic)
//			CamStatic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//		ELSE
//			SET_CAM_PARAMS(CamStatic, <<-929.661072,-3000.141357,17.710348>>, <<-16.800667,-0.003546,-4.904076>>, 37.519661)
//		ENDIF
//		
//		DISPLAY_RADAR(FALSE)
//		DISPLAY_HUD(FALSE)
//		
//		icutsceneStage = 0
//		bIsCutSceneRunning = TRUE
//		
//		WHILE bIsCutSceneRunning = TRUE
//			
//			WAIT(0)
//			
//			SWITCH icutsceneStage
//			
//				CASE 0
//						
//						SET_CAM_PARAMS(CamInit, <<-924.351807,-3001.322754,14.538351>>, <<-0.345139,-0.004003,16.386713>>, 16.070929)
//						SET_CAM_PARAMS(CamDest, <<-928.675781,-3000.916016,17.847477>>, <<-7.214854,-0.004003,10.014034>>, 16.070929)						
//						
//						SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 3000)
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//						icutsceneStage++
//						
//				BREAK
//				
//				CASE 1
//				
//						IF DOES_ENTITY_EXIST(EnemyPed)
//							IF NOT IS_PED_INJURED(EnemyPed)
//								IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)		
//									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, 3)
//									IF NOT IS_PED_INJURED(Pilot)
//										IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 13
//											IF pedShootingTaskGiven = FALSE
//				//								WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(EnemyPed, pilot, TRUE)
//												TASK_SHOOT_AT_ENTITY(EnemyPed, Pilot, -1, FIRING_TYPE_RANDOM_BURSTS)
//												pedShootingTaskGiven = TRUE
//											ENDIF
//											
//											IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(Pilot)
//												CLEAR_PED_TASKS(pilot)
//												TASK_FOLLOW_WAYPOINT_RECORDING(Pilot, "Trev4_3", 0)
//												SETTIMERB(0)
//											ELSE
//												icutsceneStage++
//											ENDIF
//										ENDIF
//									ENDIF
//								ELSE
//									icutsceneStage++
//								ENDIF
//							ENDIF
//						ENDIF
//							
//				BREAK
//				
//				CASE 2
//						
//						//Set up endplane
//						IF DOES_ENTITY_EXIST(EndPlane)
//							IF IS_VEHICLE_DRIVEABLE(EndPlane)
//								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EndPlane)
//									IF GET_TIME_POSITION_IN_RECORDING(EndPlane) > 48000
//										STOP_PLAYBACK_RECORDED_VEHICLE(EndPlane)
//										FREEZE_ENTITY_POSITION(EndPlane, TRUE)
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						IF staticCamActive = FALSE
//							IF TIMERB() > 1070
//								SET_CAM_PARAMS(CamStatic, <<-929.661072,-3000.141357,17.710348>>, <<-16.800667,-0.003546,-4.904076>>, 37.519661)
//								SET_CAM_ACTIVE(CamStatic, TRUE)
//								SHAKE_CAM(CamStatic, "HAND_SHAKE", 0.5)
//								staticCamActive = TRUE
//							ENDIF
//						ENDIF
//						
//						IF playergiventask = FALSE	
//							IF TIMERB() > 2000
//								CLEAR_PED_TASKS(PLAYER_PED_ID())
//								SET_ENTITY_COORDS(PLAYER_PED_ID(), << -927.6995, -2945.0610, 13.0639 >>)
//								SET_ENTITY_HEADING(PLAYER_PED_ID(), 165.3078)						
//								TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), << -929.3085, -2950.5090, 13.0639 >>, PEDMOVE_RUN)
//								playergiventask = TRUE
//							ENDIF
//						ENDIF
//				
//						IF DOES_ENTITY_EXIST(pilot)
//							IF NOT IS_PED_INJURED(Pilot)
//								IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pilot)
//									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(pilot, 2)
//								ELSE
//									DELETE_PED(Pilot)
//									icutsceneStage++
//								ENDIF
//							ENDIF
//						ENDIF
//				
//				BREAK
//				
//				CASE 3
//						
//						IF DOES_ENTITY_EXIST(EnemyPed)
//							IF NOT IS_PED_INJURED(EnemyPed)
//								IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
//									CLEAR_PED_TASKS(EnemyPed)
//									TASK_FOLLOW_WAYPOINT_RECORDING(EnemyPed, "Trev4_2", 15)
//								ELSE
//									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, 3)
//									icutsceneStage++
//								ENDIF
//							ENDIF
//						ENDIF
//
//				BREAK
//				
//				CASE 4
//				
//					bIsCutSceneRunning = FALSE
//					
//				BREAK
//				
//			ENDSWITCH
//			
//		ENDWHILE
//		
//		IF bIsCutSceneRunning = FALSE
//			
//			PRINTSTRING("Cutscene isnt running but mission is stuck here") PRINTNL()		
//			
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			
//			SET_CAM_ACTIVE(CamInit, FALSE)
//			SET_CAM_ACTIVE(CamDest, FALSE)
//			STOP_CAM_POINTING(CamInit)
//			STOP_CAM_POINTING(CamDest)
//			
//			DISPLAY_RADAR(TRUE)
//			DISPLAY_HUD(TRUE)
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			CLEAR_PED_TASKS(PLAYER_PED_ID())
//			
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			
//			iControlFlag = 0
//			missionStage = STAGE_PED_RUNS_TOWARDS_ENGINE
//			
//		ENDIF
//		
//	ENDIF
//
//ENDPROC

//Controls how the police attack the player at end of mission.
PROC POLICE_ATTACK_CONTROL()
	
	//Make the peds get close to the player before combat
	FOR icount = 0 TO 9
		IF DOES_ENTITY_EXIST(EndCop[icount])
			IF NOT IS_PED_INJURED(EndCop[icount])
				IF NOT IS_PED_IN_ANY_VEHICLE(EndCop[icount])
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EndCop[icount]) > 200
							CLEAR_PED_TASKS(EndCop[icount])
							SET_PED_AS_NO_LONGER_NEEDED(EndCop[icount])
						ELSE
							IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EndCop[icount]) > 20
								IF GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(EndCop[icount], PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 15, 20, FALSE, FALSE, FIRING_TYPE_RANDOM_BURSTS)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_COMBAT) <> WAITING_TO_START_TASK
									TASK_COMBAT_PED(EndCop[icount], PLAYER_PED_ID())
									SET_PED_COMBAT_ATTRIBUTES(EndCop[icount], CA_CAN_CHARGE, TRUE) 
								ENDIF		
							ENDIF
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EndCop[icount]) > 200
							SET_PED_AS_NO_LONGER_NEEDED(EndCop[icount])
						ELSE
							IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EndCop[icount]) > 5
								IF GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(EndCop[icount], PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVE_RUN, TRUE, 15, 20, FALSE, FALSE, FIRING_TYPE_RANDOM_BURSTS)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(EndCop[icount], SCRIPT_TASK_COMBAT) <> WAITING_TO_START_TASK
									TASK_COMBAT_PED(EndCop[icount], PLAYER_PED_ID())
									SET_PED_COMBAT_ATTRIBUTES(EndCop[icount], CA_CAN_CHARGE, TRUE) 
								ENDIF		
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF copsPlaybackStarted = TRUE
		IF endcops6and7tasksgiven = FALSE
			IF DOES_ENTITY_EXIST(endpoliceCar[0])
				IF IS_VEHICLE_DRIVEABLE(endpoliceCar[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(endpoliceCar[0])
	//					PRINTSTRING("endpoliceCar[0] recording time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(endpoliceCar[0])) PRINTNL()
					ELSE
						IF DOES_ENTITY_EXIST(EndCop[6])
							IF NOT IS_PED_INJURED(EndCop[6])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[6], FALSE)
								CLEAR_PED_TASKS(EndCop[6])
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_VEHICLE(NULL, endpoliceCar[0], ECF_DONT_CLOSE_DOOR)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(EndCop[6], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(EndCop[7])
							IF NOT IS_PED_INJURED(EndCop[7])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[7], FALSE)
								CLEAR_PED_TASKS(EndCop[7])
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_VEHICLE(NULL, endpoliceCar[0], ECF_DONT_CLOSE_DOOR)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(EndCop[7], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ENDIF
						endcops6and7tasksgiven = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF donePoliceChat2 = FALSE
				IF DOES_ENTITY_EXIST(endpoliceCar[0])
					IF IS_VEHICLE_DRIVEABLE(endpoliceCar[0])
						IF DOES_ENTITY_EXIST(EndCop[6])
							IF NOT IS_PED_INJURED(EndCop[6])
								IF NOT IS_PED_IN_VEHICLE(EndCop[6], endpoliceCar[0])
									IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EndCop[6]) < 20
										PLAY_PED_AMBIENT_SPEECH(EndCop[6], "DRAW_GUN")
										donePoliceChat2 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF endcops8and9tasksgiven = FALSE
			IF DOES_ENTITY_EXIST(endpoliceCar[1])
				IF IS_VEHICLE_DRIVEABLE(endpoliceCar[1])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(endpoliceCar[1])
	//					PRINTSTRING("endpoliceCar[1] recording time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(endpoliceCar[1])) PRINTNL()
					ELSE
						IF DOES_ENTITY_EXIST(EndCop[8])
							IF NOT IS_PED_INJURED(EndCop[8])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[8], FALSE)
								CLEAR_PED_TASKS(EndCop[8])
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_VEHICLE(NULL, endpoliceCar[1], ECF_DONT_CLOSE_DOOR)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(EndCop[8], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(EndCop[9])
							IF NOT IS_PED_INJURED(EndCop[9])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[9], FALSE)
								CLEAR_PED_TASKS(EndCop[9])
								OPEN_SEQUENCE_TASK(seq)
									TASK_LEAVE_VEHICLE(NULL, endpoliceCar[1], ECF_DONT_CLOSE_DOOR)
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(EndCop[9], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ENDIF
						endcops8and9tasksgiven = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF endcop10TaskGiven = FALSE
		IF DOES_ENTITY_EXIST(EndCop[10])
			IF NOT IS_PED_INJURED(EndCop[10])
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), EndCop[10]) > 200
					SET_PED_AS_NO_LONGER_NEEDED(EndCop[icount])
					REMOVE_ANIM_DICT("misssolomon_3")
				ELSE
					IF HAS_ANIM_DICT_LOADED("misssolomon_3")
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -923.2303, -2948.0039, 12.9451 >>, <<2,2,2>>)
							IF NOT IS_PED_INJURED(EndCop[10])
								CLEAR_PED_TASKS(EndCop[10])
								OPEN_SEQUENCE_TASK(seq)
			//						TASK_SIDEWAYS_DIVE(NULL, FALSE)
									TASK_PLAY_ANIM(NULL, "misssolomon_3", "plyr_roll_left")
									TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(EndCop[10], seq)
								CLEAR_SEQUENCE_TASK(seq)
								endcop10TaskGiven = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

//PURPOSE: Airport Worker runs and dives on player to stop him from getting sucked up from 1st engine.
PROC Worker_Stops_Player()

		IF NOT IS_PED_INJURED(AirWorker)
			PlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		 	AirWorkerCoords = GET_ENTITY_COORDS(AirWorker)			
		ENDIF

		IF NOT IS_PED_INJURED(EnemyPed)
			EnemyPedCoords = GET_ENTITY_COORDS(EnemyPed)
		ENDIF
//		PRINTSTRING("Distance between player and enemyped is ") PRINTFLOAT(GET_DISTANCE_BETWEEN_COORDS(PlayerCoords, EnemyPedCoords)) PRINTNL()
	
		IF AirWorkerTaskGiven = FALSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -932.1352, -2962.8857, 12.9453 >>, <<2,2,2>>, FALSE, TRUE, TM_ON_FOOT)
//				playerCanSeePedSuckingThroughEngine = TRUE
				IF DOES_ENTITY_EXIST(AirWorker)
					IF NOT IS_PED_INJURED(AirWorker)
						CLEAR_PED_TASKS_IMMEDIATELY(AirWorker)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(AirWorker, PLAYER_PED_ID(), <<0.166, 2.715, -0.290>>, PEDMOVE_RUN)
						AirWorkerTaskGiven = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF AirWorkerBlasted = FALSE
				IF NOT IS_PED_INJURED(AirWorker)
					IF GET_DISTANCE_BETWEEN_COORDS(PlayerCoords, AirWorkerCoords) < 3
						CLEAR_PED_TASKS(AirWorker)
						SET_PED_TO_RAGDOLL(AirWorker, 0, 10000, TASK_RELAX, FALSE)
						BLAST_PED(AirWorker, <<0.037, 2.790, 0.395>>, <<0,0,0>>, 7.5)
						SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
						AirWorkerBlasted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		IF playerBlasted = FALSE
			IF NOT IS_PED_INJURED(AirWorker)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), AirWorker)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 0, 10000, TASK_RELAX, FALSE)
					BLAST_PED(PLAYER_PED_ID(), <<-0.162, -1.218, 0.013>>, <<0,0,0>>, 5)
					playerBlasted = TRUE
				ENDIF
			ENDIF
		ELSE
			IF AirWorkerTask2Given = FALSE
				IF NOT IS_PED_INJURED(AirWorker)
					CLEAR_PED_TASKS(AirWorker)
					TASK_FOLLOW_NAV_MESH_TO_COORD(AirWorker, << -938.9813, -2935.6531, 13.0638 >>, PEDMOVE_RUN)
					SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
					AirWorkerTask2Given = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
ENDPROC

PROC HANDLE_AIR_WORKER()

	IF hasPedBeenShot = FALSE
		IF DOES_ENTITY_EXIST(AirWorker)
			IF NOT IS_PED_INJURED(AirWorker)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(AirWorker, PLAYER_PED_ID(), FALSE)
					hasPedBeenShot = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF hasPedBeenShot = FALSE
		IF startedMainAnim = FALSE
			IF DOES_ENTITY_EXIST(AirWorker)
				IF NOT IS_PED_INJURED(AirWorker)
					IF DOES_ENTITY_EXIST(EnemyPed)
						IF NOT IS_PED_INJURED(EnemyPed)
							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
								IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 18
									IF DOES_ENTITY_EXIST(WorkersClipBoard)
										IF IS_ENTITY_ATTACHED(WorkersClipBoard)
											DETACH_ENTITY(WorkersClipBoard)
										ENDIF
									ENDIF
									WorkerSyncScene = CREATE_SYNCHRONIZED_SCENE(<< -932.847, -2978.498, 13.946 >>, << -0.000, 0.000, 16.560 >>)
									TASK_SYNCHRONIZED_SCENE(AirWorker, WorkerSyncScene, "misssolomon_3", "_action", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
									TASK_LOOK_AT_ENTITY(AirWorker, PLAYER_PED_ID(), 9000, SLF_USE_TORSO, SLF_LOOKAT_HIGH)
									startedMainAnim = TRUE
									playedWorkerShout = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF startedMainAnim = TRUE
		AND startedEndLoop = FALSE
			IF DOES_ENTITY_EXIST(AirWorker)
				IF NOT IS_PED_INJURED(AirWorker)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(WorkerSyncScene)
						//Play some dialogue when the player gets close.
						IF NOT playedWorkerShout
							IF GET_SYNCHRONIZED_SCENE_PHASE(WorkerSyncScene) > 0.25
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, AirWorker, "HangerWorker")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(AirWorker, "T1M4_BCAA", "HangerWorker", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
								playedWorkerShout = TRUE
							ENDIF
						ENDIF
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(WorkerSyncScene) > 0.99
							WorkerSyncScene = CREATE_SYNCHRONIZED_SCENE(<< -932.847, -2978.498, 13.946 >>, << -0.000, 0.000, 16.560 >>)
							TASK_SYNCHRONIZED_SCENE(AirWorker, WorkerSyncScene, "misssolomon_3", "_react_to_death", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT, RBF_PLAYER_IMPACT)//INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_LOOP_WITHIN_SCENE)
	//						SET_SYNCHRONIZED_SCENE_LOOPED(WorkerSyncScene, TRUE)
							startedEndLoop = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF startedEndLoop = TRUE
		AND fleeTaskGiven = FALSE
			IF DOES_ENTITY_EXIST(AirWorker)
				IF NOT IS_PED_INJURED(AirWorker)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(WorkerSyncScene)
						IF HAS_ANIM_EVENT_FIRED(AirWorker, GET_HASH_KEY("ENDS_IN_RUN"))//GET_SYNCHRONIZED_SCENE_PHASE(WorkerSyncScene) > 0.99
							OPEN_SEQUENCE_TASK(seq)
								TASK_FORCE_MOTION_STATE(NULL, enum_to_int(MS_ON_FOOT_SPRINT))
								TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1001.9, -2954.3, 13.1>>, PEDMOVEBLENDRATIO_SPRINT, -1, DEFAULT_NAVMESH_FINAL_HEADING, 3)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(AirWorker, seq)
							CLEAR_SEQUENCE_TASK(seq)
							FORCE_PED_MOTION_STATE(AirWorker, MS_ON_FOOT_SPRINT, FALSE, FAUS_DEFAULT) 
							fleeTaskGiven = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF fleeTaskGiven = FALSE
			IF DOES_ENTITY_EXIST(AirWorker)
				IF NOT IS_PED_INJURED(AirWorker)		
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1001.9, -2954.3, 13.1>>, PEDMOVEBLENDRATIO_SPRINT, -1, DEFAULT_NAVMESH_FINAL_HEADING, 3)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(AirWorker, seq)
					CLEAR_SEQUENCE_TASK(seq)
					fleeTaskGiven = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Ped runs towards the planes engine.
PROC DO_STAGE_PED_RUNS_TOWARDS_ENGINE()
	
//	PRINTSTRING("player speed is ") PRINTFLOAT(GET_ENTITY_SPEED(PLAYER_PED_ID())) PRINTNL()
//	PRINTSTRING("fPlayerMaxMoveSpeed is ") PRINTFLOAT(fPlayerMaxMoveSpeed) PRINTNL()
	
	//Player can only walk when this is set to true
	IF playerSetToWalk = FALSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-926.025757,-2958.614502,12.464674>>, <<-930.652466,-2956.061035,16.459446>>, 2.750000)
			playerSetToWalk = TRUE
		ENDIF
	ELSE
		IF godtextUpdated = FALSE	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		
			//Stop the player from moving if he gets too close to the engine
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-938.517822,-2972.022461,12.467147>>, <<-934.103394,-2974.613037,15.967147>>, 5.250000)
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_STILL) //PEDMOVE_WALK)
			ELSE
				//Slow the player down to a maximum walking speed until the film reel has been dropped.
				IF fPlayerMaxMoveSpeed < 1
					fPlayerMaxMoveSpeed = 1
				ENDIF
				fPlayerMaxMoveSpeed = (fPlayerMaxMoveSpeed * 0.975)
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), fPlayerMaxMoveSpeed) //PEDMOVE_WALK)				
			ENDIF
		ENDIF
	ENDIF
	
	IF iControlFlag > 0
		HANDLE_AIR_WORKER()
	ENDIF

	IF iControlFlag = 0
	
//****************************************RESET FLAGS******************************************
		EndCopsSetUp 						= FALSE
		AirWorkerTaskGiven 					= FALSE
//		AirWorkerAnimStarted 				= FALSE
		AirWorkerBlasted 					= FALSE
		playerBlasted 						= FALSE
		AirWorkerTask2Given 				= FALSE
		checkForEnemyPedDeath				= FALSE
//		doneHangerChat 						= FALSE
		EnginesStarted 						= FALSE
		bloodSplatterEffectStarted 			= FALSE
		filmReelDropped 					= FALSE
		SuckedThroughEngineAudioRequested 	= FALSE
		SuckedThroughAnimStarted 			= FALSE
		hintStarted 						= FALSE
		hintStopped 						= FALSE
//		doneChat1 							= FALSE
		godtextUpdated 						= FALSE
		splutterFXStopped[0] 				= FALSE
		splutterFXStarted[0] 				= FALSE
		splutterFXStopped[1] 				= FALSE
		splutterFXStarted[1] 				= FALSE
		splutterFXStopped[2] 				= FALSE
//		airworkerFleeing 					= FALSE
		startedMainAnim 					= FALSE
		startedEndLoop 						= FALSE
		filmReelFrozen 						= FALSE
		filmReelFrozenTimerStarted			= FALSE
		fleeTaskGiven 						= FALSE
		UnfrozeEndPlane 					= FALSE
		effectsStopped 						= FALSE
		hasPedBeenShot 						= FALSE
		bVideoRecorded[3] 					= FALSE
//		playerFailTimerStarted 				= FALSE
//		playerCanSeePedSuckingThroughEngine = FALSE
		
//		REQUEST_ANIM_DICT("get_up")
//		REQUEST_ANIM_DICT("amb@celebration")
		
		REQUEST_MODEL(Prop_Jet_Bloodsplat_01)
		
		SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_60_HangerRm")
		
//		REQUEST_CUTSCENE("TRV_4_MCS_2")
		
		SET_MAX_WANTED_LEVEL(5)
		
		//Set Players wanted level
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())		
		
		//bug fix 1918458
		IF DOES_ENTITY_EXIST(EnemyPed)
			IF NOT IS_PED_INJURED(EnemyPed)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(EnemyPed, PEDMOVEBLENDRATIO_SPRINT, TRUE)	
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF		
		
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1	
		
//		Worker_Stops_Player()
		
		//Request Audio bank now
		IF SuckedThroughEngineAudioRequested = FALSE
			IF REQUEST_SCRIPT_AUDIO_BANK("Trv_4_747")
				SuckedThroughEngineAudioRequested = TRUE
			ENDIF
		ENDIF
		
		PRINTSTRING("STAGE_PED_RUNS_TOWARDS_ENGINE") PRINTNL()
		
//		IF doneShout2Chat = FALSE
//			IF NOT IS_MESSAGE_BEING_DISPLAYED()
//			OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "T1M4_SHOUT1", "T1M4_SHOUT1_2", CONV_PRIORITY_AMBIENT_MEDIUM)
//					//Molly, just give me my fucking film.
//					//Do you really think you can out run me?
//					SETTIMERB(0)
//					doneShout2Chat = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
		
//		IF AirWorkerAnimStarted = FALSE
//			IF DOES_ENTITY_EXIST(EnemyPed)
//				IF NOT IS_PED_INJURED(EnemyPed)
//					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
//						IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 13		
//							IF NOT IS_PED_INJURED(AirWorker)
//								IF HAS_ANIM_DICT_LOADED("amb@celebration")
//									TASK_PLAY_ANIM(AirWorker, "amb@celebration", "male_two_handed_a")
//									AirWorkerAnimStarted = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			IF NOT IS_PED_INJURED(AirWorker)
//				IF IS_ENTITY_PLAYING_ANIM(AirWorker, "amb@celebration", "male_two_handed_a")
//					IF GET_ENTITY_ANIM_CURRENT_TIME(AirWorker, "amb@celebration", "male_two_handed_a") > 0.27
//						STOP_ANIM_TASK(AirWorker, "amb@celebration", "male_two_handed_a", NORMAL_BLEND_OUT)
//					ENDIF
//				ENDIF
//			ENDIF

			
		//Start the engines as she runs passed the 1st one		
		IF EnginesStarted = FALSE
			IF DOES_ENTITY_EXIST(EnemyPed)
				IF NOT IS_PED_INJURED(EnemyPed)
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
						IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 20	
							IF DOES_ENTITY_EXIST(EndPlane)
								IF IS_VEHICLE_DRIVEABLE(EndPlane)
									ATTACH_ENGINE_PTFX_TO_END_JET(EndPlane)
									PLAY_SOUND_FROM_COORD(JetEngineSoundID, "Trevor_4_747_Jet_Engine", <<-937.77, -2981.36, 15.44>>)
									SET_VEHICLE_ENGINE_ON(EndPlane, TRUE, FALSE)
									EnginesStarted = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Start Molly getting sucked into the engine
		IF SuckedThroughAnimStarted = FALSE
			IF DOES_ENTITY_EXIST(EnemyPed)
				IF NOT IS_PED_INJURED(EnemyPed)
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
						IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 23
							CLEAR_PED_TASKS(EnemyPed)
							PRINTSTRING("GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 23") PRINTNL()
							IF DOES_ENTITY_EXIST(EndPlane)
								IF IS_VEHICLE_DRIVEABLE(EndPlane)
									IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_ENGINE_DEATH_SCENE")
										START_AUDIO_SCENE("SOL_3_ENGINE_DEATH_SCENE")
									ENDIF
									SuckedThroughJetSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(SuckedThroughJetSyncdScene, EndPlane, GET_ENTITY_BONE_INDEX_BY_NAME(EndPlane, "chassis_Control"))	
									TASK_SYNCHRONIZED_SCENE(EnemyPed, SuckedThroughJetSyncdScene, "MISSSOLOMON_3", "molly_death", WALK_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(SuckedThroughJetSyncdScene, TRUE)
//									SET_GAMEPLAY_ENTITY_HINT(EnemyPed, <<0,0,0>>, TRUE, 6000, 1000, DEFAULT_INTERP_OUT_TIME, HINTTYPE_DEFAULT)
									SuckedThroughAnimStarted = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
		
//		IF doneChat1 = FALSE
//			IF doneHangerChat = TRUE
//			AND SuckedThroughAnimStarted = TRUE	
//				IF DOES_ENTITY_EXIST(AirWorker)
//					IF NOT IS_PED_INJURED(AirWorker)
//						IF DOES_ENTITY_EXIST(EnemyPed)
//							CLEAR_PED_TASKS(AirWorker)
//							TASK_TURN_PED_TO_FACE_ENTITY(AirWorker, EnemyPed, 5000)
////							IF CREATE_CONVERSATION(MyLocalPedStruct, "T1M4AUD", "SOL3_PH1", CONV_PRIORITY_MEDIUM)
//								//Holy shit! Kill the engine's! Kill the engine's!
//								doneChat1 = TRUE
////							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
					
		
		//Do a hint cam at Molly getting sucked through the engine as long as player is close enough
		IF hintStarted = FALSE
			IF DOES_ENTITY_EXIST(EnemyPed)
				IF NOT IS_PED_INJURED(EnemyPed)
					IF SuckedThroughAnimStarted = TRUE
						vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF vplayerCoords.y < -2965
							SET_GAMEPLAY_ENTITY_HINT(EnemyPed, <<0,0,0>>, TRUE, 6000, 1000, DEFAULT_INTERP_OUT_TIME, HINTTYPE_DEFAULT)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), EnemyPed)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), EnemyPed, 4000, SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
							hintStarted = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF hintStopped = FALSE
				IF NOT DOES_ENTITY_EXIST(EnemyPed)
					STOP_GAMEPLAY_HINT(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					//Allow this cop to behave naturally now.
					IF DOES_ENTITY_EXIST(EndCop[0])	
						IF NOT IS_PED_INJURED(EndCop[0])
							CLEAR_PED_TASKS(EndCop[0])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[0], FALSE)
						ENDIF
					ENDIF
					hintStopped = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Have Molly drop the film reel just before she gets sucked up
		IF filmReelDropped = FALSE
			IF DOES_ENTITY_EXIST(EnemyPed)
				IF NOT IS_PED_INJURED(EnemyPed)
					IF DOES_ENTITY_EXIST(ObjFilmReel)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(ObjFilmReel, EnemyPed)
							IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "MISSSOLOMON_3", "molly_death")
								IF GET_SYNCHRONIZED_SCENE_PHASE(SuckedThroughJetSyncdScene) > 0.59
									DETACH_ENTITY(ObjFilmReel)
									IF DOES_BLIP_EXIST(EnemyPedBlip)
										REMOVE_BLIP(EnemyPedBlip)
									ENDIF
									IF NOT DOES_BLIP_EXIST(filmReelBlip)
//										PLAY_PAIN(EnemyPed, AUD_DAMAGE_REASON_SCREAM_TERROR)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(EnemyPed,"HIGH_FALL","WAVELOAD_PAIN_FEMALE",SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
										IF IS_AUDIO_SCENE_ACTIVE("SOL_3_ON_FOOT_CHASE")
											STOP_AUDIO_SCENE("SOL_3_ON_FOOT_CHASE")
										ENDIF										
										IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS")
											START_AUDIO_SCENE("SOL_3_LOSE_COPS")
										ENDIF
										SETTIMERB(0)
										filmReelDropped = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF godtextUpdated = FALSE
			IF filmReelDropped = TRUE
			AND TIMERB() > 2500
				filmReelBlip = CREATE_BLIP_FOR_OBJECT(ObjFilmReel)
				PRINT_NOW("SOL3_GOD1", DEFAULT_GOD_TEXT_TIME, -1)//~s~Pick up the ~g~film reel.	
				SETTIMERB(0)
				godtextUpdated = TRUE
			ENDIF
		ENDIF
		
		//add video recording of molly getting sucked through engine.
		IF bVideoRecorded[3] = FALSE
			IF godtextUpdated = TRUE
			AND hintStopped = TRUE
				IF timerb() > 2500
					REPLAY_RECORD_BACK_FOR_TIME(10.0)
					bVideoRecorded[3] = TRUE
				ENDIF
			ENDIF
		ENDIF
				
				
//		//Handle the airworker
//		IF filmReelDropped = TRUE
//			IF DOES_ENTITY_EXIST(AirWorker)
//				IF NOT IS_PED_INJURED(AirWorker)
//					IF IS_ENTITY_PLAYING_ANIM(AirWorker, "amb@world_human_clipboard@male@base", "base")
//						STOP_ENTITY_ANIM(AirWorker, "base", "amb@world_human_clipboard@male@base", NORMAL_BLEND_OUT)
//						CLEAR_PED_TASKS(AirWorker)
//						IF DOES_ENTITY_EXIST(WorkersClipBoard)
//							IF IS_ENTITY_ATTACHED(WorkersClipBoard)
//								DETACH_ENTITY(WorkersClipBoard)
//								SET_OBJECT_AS_NO_LONGER_NEEDED(WorkersClipBoard)
//							ENDIF
//						ENDIF
//						TASK_SMART_FLEE_PED(AirWorker, PLAYER_PED_ID(), 150, -1)
//						SET_PED_KEEP_TASK(AirWorker, TRUE)
//						SET_PED_AS_NO_LONGER_NEEDED(AirWorker)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Start the blood splatter as soon as Molly hits the engine and delete Molly
		IF bloodSplatterEffectStarted = FALSE
			IF NOT IS_PED_INJURED(EnemyPed)
				IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "MISSSOLOMON_3", "molly_death")
					IF GET_SYNCHRONIZED_SCENE_PHASE(SuckedThroughJetSyncdScene) > 0.89
						IF DOES_ENTITY_EXIST(EndPlane)
							IF IS_VEHICLE_DRIVEABLE(EndPlane)
								DELETE_PED(EnemyPed)
								START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_747_blood_impact", EndPlane, <<-12.5793, 12.2, -7.09421>>, <<0,0,0>>, 1) //<<-12.5793, 12.4698, -7.09421>>
								EngineSplutterEffectsID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_exhaust_plane_misfire", EndPlane, <<-12.6323, 8.1153, -7.0876>>, <<0,0,0>>, 1)
								PLAY_SOUND_FROM_COORD(SuckedInSoundID, "Trevor_4_747_Man_Sucked_In", <<-938.60, -2984.13, 15.47>>)
								iBloodSplatTimer = GET_GAME_TIMER()
								bloodSplatterEffectStarted = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
//			PRINTSTRING("iBloodSplatTimer = ") PRINTINT((GET_GAME_TIMER() - iBloodSplatTimer)) PRINTNL()
			//Create the blood splat on the ground and add the particle effects of the blood dripping off the back of the enigine
			IF GET_GAME_TIMER() > (iBloodSplatTimer + 574)
				IF NOT DOES_ENTITY_EXIST(objBloodSplat)
					IF HAS_MODEL_LOADED(Prop_Jet_Bloodsplat_01)
						objBloodSplat = CREATE_OBJECT(Prop_Jet_Bloodsplat_01, <<-946.4231, -2979.8259, 12.9264>>)
						SET_ENTITY_ROTATION(objBloodSplat, <<-0.0000, -0.0000, -121.5948>>)
						SET_ENTITY_QUATERNION(objBloodSplat, -0.0000, 0.0000, 0.8729, -0.4879)
						SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Jet_Bloodsplat_01)
					ENDIF
				ENDIF
				IF effectsStopped = FALSE
					IF DOES_ENTITY_EXIST(EndPlane)
						IF IS_VEHICLE_DRIVEABLE(EndPlane)
							START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_747_engine_debris", EndPlane, << -12.6286, 6.85353, -7.13622 >>, <<180, 0, 0>>, 1)
							SET_VEHICLE_ENGINE_ON(EndPlane, FALSE, FALSE)
							DETACH_ENGINE_PTFX_TO_JET()
							STOP_SOUND(JetEngineSoundID)
							effectsStopped = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//Control the sparks
			IF splutterFXStopped[0] = FALSE
				IF GET_GAME_TIMER() > (iBloodSplatTimer + 300)
					IF DOES_PARTICLE_FX_LOOPED_EXIST(EngineSplutterEffectsID)
						STOP_PARTICLE_FX_LOOPED(EngineSplutterEffectsID)
					ENDIF
					splutterFXStopped[0] = TRUE
				ENDIF
			ELSE
				IF splutterFXStarted[0] = FALSE
					IF GET_GAME_TIMER() > (iBloodSplatTimer + 700)
						IF DOES_ENTITY_EXIST(EndPlane)
							IF IS_VEHICLE_DRIVEABLE(EndPlane)
								EngineSplutterEffectsID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_exhaust_plane_misfire", EndPlane, <<-12.6323, 8.1153, -7.0876>>, <<0,0,0>>, 1)
								splutterFXStarted[0] = TRUE
							ENDIF
						ENDIF
					ENDIF				
				ELSE
					IF splutterFXStopped[1] = FALSE
						IF GET_GAME_TIMER() > (iBloodSplatTimer + 1000)
							IF DOES_PARTICLE_FX_LOOPED_EXIST(EngineSplutterEffectsID)
								STOP_PARTICLE_FX_LOOPED(EngineSplutterEffectsID)
							ENDIF
							splutterFXStopped[1] = TRUE
						ENDIF	
					ELSE
						IF splutterFXStarted[1] = FALSE
							IF GET_GAME_TIMER() > (iBloodSplatTimer + 1400)
								IF DOES_ENTITY_EXIST(EndPlane)
									IF IS_VEHICLE_DRIVEABLE(EndPlane)
										EngineSplutterEffectsID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_exhaust_plane_misfire", EndPlane, <<-12.6323, 8.1153, -7.0876>>, <<0,0,0>>, 1)
										EngineDamagePTFX = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_trev4_747_engine_damage", EndPlane, <<-11.956, 10.528, -7.657>>, <<0,2,0>>)
										splutterFXStarted[1] = TRUE
									ENDIF
								ENDIF
							ENDIF				
						ELSE
							IF splutterFXStopped[2] = FALSE
								IF GET_GAME_TIMER() > (iBloodSplatTimer + 3500)
									IF DOES_PARTICLE_FX_LOOPED_EXIST(EngineSplutterEffectsID)
										STOP_PARTICLE_FX_LOOPED(EngineSplutterEffectsID)
									ENDIF
									IF DOES_ENTITY_EXIST(EndPlane)
										IF IS_VEHICLE_DRIVEABLE(EndPlane)
											SET_ENTITY_PROOFS(EndPlane, FALSE, FALSE, FALSE, FALSE, FALSE)
										ENDIF
									ENDIF
									splutterFXStopped[2] = TRUE
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
						
//			PRINTSTRING("iBloodSplatTimer = ") PRINTINT(iBloodSplatTimer) PRINTNL()
//			PRINTSTRING("gametimer = ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
//			PRINTSTRING("plane offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(EndPlane, GET_CAM_COORD(GET_DEBUG_CAM()))) PRINTNL()
//			IF DOES_PARTICLE_FX_LOOPED_EXIST(EngineSplutterEffectsID)
//			ENDIF
		ENDIF	
		
//		IF airworkerFleeing = FALSE
//			IF NOT DOES_ENTITY_EXIST(EnemyPed)
//				IF DOES_ENTITY_EXIST(AirWorker)
//					IF NOT IS_PED_INJURED(AirWorker)
//						SET_PED_CAN_BE_TARGETTED(AirWorker, TRUE)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, FALSE)
//						TASK_SMART_FLEE_PED(AirWorker, PLAYER_PED_ID(), 150, -1)
//						airworkerFleeing = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Freeze the film reels position once it's on the ground.
		IF filmReelFrozen = FALSE
			IF filmReelDropped = TRUE
				IF filmReelFrozenTimerStarted = FALSE
					IF NOT IS_ENTITY_IN_AIR(ObjFilmReel)
						iFreezeFilmReelTimer = GET_GAME_TIMER()
						filmReelFrozenTimerStarted = TRUE
					ENDIF
				ELSE
					IF GET_GAME_TIMER() > (iFreezeFilmReelTimer + 2000)
						FREEZE_ENTITY_POSITION(ObjFilmReel, TRUE)
						filmReelFrozen = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Unfreeze the endplane now
		IF UnfrozeEndPlane = FALSE
			IF NOT DOES_ENTITY_EXIST(EnemyPed)
				IF DOES_ENTITY_EXIST(EndPlane)
					FREEZE_ENTITY_POSITION(EndPlane, FALSE)
					UnfrozeEndPlane = TRUE
				ENDIF
			ENDIF
		ENDIF
			
		//Move stage on once player has picked up the film reel
		IF DOES_ENTITY_EXIST(ObjFilmReel)
			IF DOES_BLIP_EXIST(filmReelBlip)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ObjFilmReel) < 1.5
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					PLAY_SOUND_FRONTEND(-1, "PICK_UP", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					REMOVE_BLIP(filmReelBlip)
					DELETE_OBJECT(ObjFilmReel)
					SET_UP_END_COPS()
					iControlFlag = 0
					missionStage = STAGE_EVADE_POLICE
				ENDIF
			ENDIF	
		ENDIF
		
//		IF DOES_ENTITY_EXIST(EnemyPed)
//			IF NOT IS_PED_INJURED(EnemyPed)	
//				IF IS_ENTITY_PLAYING_ANIM(EnemyPed, "MISSSOLOMON_3", "molly_death")
//					PRINTSTRING("enemyped anim phase is ") PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(SuckedThroughJetSyncdScene)) PRINTNL()
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_CAM_ACTIVE(GET_DEBUG_CAM())
//			PRINTSTRING("camera offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(EndPlane, GET_CAM_COORD(GET_DEBUG_CAM())))
//		ENDIF				
//		IF DOES_ENTITY_EXIST(EnemyPed)
//			IF NOT IS_PED_INJURED(EnemyPed)
//				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
//					IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 19
//						PRINTSTRING("GET_PED_WAYPOINT_PROGRESS(EnemyPed) > 16") PRINTNL()
//						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-934.141663,-2967.625000,12.945072>>, <<-918.171082,-2940.178711,15.612643>>, 6.250000)
//							iControlFlag = 0
//							icutsceneStage = 0
//							REMOVE_WAYPOINT_RECORDING("BB_MOLLY_2")
//							missionStage = STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE
//						ELSE
//							IF playerFailTimerStarted = FALSE
//								SETTIMERA(0)
//								playerFailTimerStarted = TRUE
//							ELSE
//								PRINTSTRING("playerFailTimerStarted = TRUE") PRINTNL()
//								IF TIMERA() > 2000
//									PRINTSTRING("SetMissionFailed as player wasn't far enough into the hanger") PRINTNL()
//									SetMissionFailed("TRV4_FAIL1") //~r~You lost the translator.
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
//		//Start cutscene if player is about to get tackled.
//		IF AirWorkerBlasted = TRUE
//			iControlFlag = 0
//			icutsceneStage = 0
//			missionStage = STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE		
//		ENDIF
		
	ENDIF

ENDPROC


//PURPOSE: cutscene of ped getting sucked through engine
PROC  DO_STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE()
	
	IF iControlFlag = 0
	
		PRINTSTRING("DO_STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE") PRINTNL()
	
//******************************************* RESET FLAGS ****************************************	
		icutsceneStage 			= 0
//		iskip_cutscene_flag 	= 0
	
		//Sound ID for jet engine
		JetEngineSoundID = GET_SOUND_ID()
		
		REQUEST_CUTSCENE("TRV_4_MCS_2")
		
		REQUEST_MODEL(Prop_Jet_Bloodsplat_01)
		
//		REQUEST_ANIM_DICT("get_up")
		
//		WHILE NOT HAS_ANIM_DICT_LOADED("get_up")
//			PRINTSTRING("WAITING ON ANIM DICT get_up TO LOAD") PRINTNL()
//			WAIT(0)
//		ENDWHILE			
//		
//		IF NOT DOES_CAM_EXIST(CamInit)
//			CamInit = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		ENDIF
//
//		IF NOT DOES_CAM_EXIST(CamDest)
//			CamDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		ENDIF			
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
			USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("Trev4_5", FALSE, 0.5, 1)
		ENDIF
		
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF		
		
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
	
		SWITCH iCutsceneStage
		
			CASE 0 	
				
				WHILE NOT HAS_CUTSCENE_LOADED()
					WAIT(0)
					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
				ENDWHILE
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)			
				
				//Register entities for cutscene
				//Molly
				IF NOT IS_PED_INJURED(EnemyPed)
					REGISTER_ENTITY_FOR_CUTSCENE(EnemyPed, "Molly", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_MOLLY)
				ENDIF
				//Airworker
				IF NOT IS_PED_INJURED(AirWorker)
					REGISTER_ENTITY_FOR_CUTSCENE(AirWorker, "Airworker_Tackle_trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, S_M_Y_AIRWORKER)
				ENDIF	
				//Jet
				IF IS_VEHICLE_DRIVEABLE(EndPlane)
					REGISTER_ENTITY_FOR_CUTSCENE(EndPlane, "JET_TRV4", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, JET)
				ENDIF					
				
		      	//Configure script systems into a safe mode.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				TRIGGER_MUSIC_EVENT("TRV4_SUCK_CS")
				
				START_CUTSCENE()			
								
				iCutsceneStage ++
				
			BREAK	
			
			CASE 1
				
//				//Set Cheng back on the waypoint recording for the sucked through engine cutscene.
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Taos_Translator", IG_MOLLY)
//					IF NOT IS_PED_INJURED(EnemyPed)
//						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
//							SET_ENTITY_COORDS(EnemyPed, << -933.8181, -2980.5354, 12.9447 >>)
//							SET_ENTITY_HEADING(EnemyPed, 179.8761)
//							
//							TASK_FOLLOW_WAYPOINT_RECORDING(EnemyPed, "Trev4_10", 18)
//						ENDIF
//					ENDIF
//				ENDIF
				
				SET_UP_END_COPS()
				
				//Set plane to be frozen and no collision for ped getting sucked through engine
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("JET_TRV4", JET)
					IF IS_VEHICLE_DRIVEABLE(EndPlane)	
						IF NOT IS_ENTITY_DEAD(EndPlane)
							FREEZE_ENTITY_POSITION(EndPlane, TRUE)
							
//							SET_ENTITY_COLLISION(EndPlane, FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Airworker_Tackle_trevor", S_M_Y_AIRWORKER)
					IF NOT IS_PED_INJURED(AirWorker)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, TRUE)
						TASK_SMART_FLEE_PED(AirWorker, PLAYER_PED_ID(), 150, -1)
						SET_ENTITY_INVINCIBLE(AirWorker, FALSE)
						SET_PED_CAN_BE_TARGETTED(AirWorker, TRUE)
						SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					//Set up the end cops if not already done so
//					ACTIVATE_INTERIOR_ENTITY_SET(RSinteriorGroup, "Hanger_Blood_Splat")
					SET_UP_END_COPS()
					EQUIP_BEST_PLAYER_WEAPON(TRUE)
//					REFRESH_INTERIOR(RSinteriorGroup)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				ENDIF
				
				IF NOT IS_CUTSCENE_ACTIVE()
					
					//Start scripted cut now
					bIsCutSceneRunning = TRUE
					
					//Create the blood splat on the ground
					IF NOT DOES_ENTITY_EXIST(objBloodSplat)
						IF HAS_MODEL_LOADED(Prop_Jet_Bloodsplat_01)
							objBloodSplat = CREATE_OBJECT(Prop_Jet_Bloodsplat_01, <<-946.4231, -2979.8259, 12.9264>>)
							SET_ENTITY_ROTATION(objBloodSplat, <<-0.0000, -0.0000, -121.5948>>)
							SET_ENTITY_QUATERNION(objBloodSplat, -0.0000, 0.0000, 0.8729, -0.4879)
							SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Jet_Bloodsplat_01)
						ENDIF
					ENDIF
					
					SET_PAD_CAN_SHAKE_DURING_CUTSCENE(FALSE)				
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)				
					
					IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, 150, TRUE)
					ENDIF
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-936.7671, -2968.7251, 12.9451>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 62.5325)
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)					
					
					//Set Flags
//					icutsceneStage 		= 0
					iControlFlag 		= 2
					
				ENDIF
			
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
//	IF iControlFlag = 2
//	
//		WHILE bIsCutSceneRunning = TRUE
//			
//			SWITCH icutsceneStage 
//			
//				CASE 0
//				
//					SET_CAM_PARAMS(CamDest, <<-931.444092,-2976.080811,15.109833>>,<<1.771186,0.702199,148.392639>>, 41.034195)
//					SET_CAM_ACTIVE(CamDest, TRUE)							
//					SET_CAM_PARAMS(CamInit, <<-928.847290,-2971.860596,14.956612>>,<<1.771186,0.702199,148.392639>>, 41.034195)
//					SET_CAM_ACTIVE(CamInit, TRUE)
//					SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 8000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SET_TIME_SCALE(0.3)	
//					SETTIMERA(0)
//					icutsceneStage ++
//					
//				BREAK
//				
//				CASE 1
//					
//					IF TIMERA() > 250
//						ADD_PARTICLE_FX_TO_PLANES_ENGINES()
//						PLAY_SOUND_FROM_COORD(JetEngineSoundID, "Trevor_4_747_Jet_Engine", <<-937.77, -2981.36, 15.44>>)
//						icutsceneStage ++
//					ENDIF
//				
//				BREAK
//				
//				CASE 2
//				
//					IF DOES_ENTITY_EXIST(EnemyPed)
//						IF NOT IS_PED_INJURED(EnemyPed)
//							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
//								IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) = 19
//									CLEAR_PED_TASKS(EnemyPed)
//									SET_PED_TO_RAGDOLL(EnemyPed, 0, 10000, TASK_RELAX, FALSE)
//									BLAST_PED(EnemyPed, <<0.240, -0.396, 0.350>>, <<0,0,0>>, 10)//7.5
//									BLAST_PED(EnemyPed, <<6.000, -2.016, 0.673>>, <<0,0,0>>, 5)//10  <<6.000, -7.016, 0.673>>
//									
//									SET_CAM_PARAMS(CamInit, <<-928.238953,-2992.675293,13.757324>>,<<7.145424,0.727125,50.626450>>,41.034195)
//									SET_CAM_PARAMS(CamDest, <<-931.479858,-2989.399902,14.404553>>,<<7.145424,0.727125,50.626450>>,41.034195)
//									SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 1800, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//									
//									SET_ENTITY_COORDS(PLAYER_PED_ID(), << -934.2948, -2967.9402, 12.9447 >>)
//									SET_ENTITY_HEADING(PLAYER_PED_ID(), 87.0120)
//									
//									EQUIP_BEST_PLAYER_WEAPON(TRUE)
//									
////									IF NOT IS_PED_INJURED(AirWorker)
////										CLEAR_PED_TASKS_IMMEDIATELY(AirWorker)
////										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, TRUE)
////										SET_PED_DUCKING(AirWorker, TRUE)
////										SET_ENTITY_COORDS(AirWorker, << -934.2151, -2963.7605, 12.9447 >>)
////										SET_ENTITY_HEADING(AirWorker, 225.6773)	
////									ENDIF
//									
//									TASK_PLAY_ANIM(PLAYER_PED_ID(), "get_up", "get_up_normal", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, 0.413)
//									SETTIMERA(0)
//									
//									icutsceneStage ++
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF					
//				
//				BREAK
//				
//				CASE 3
//					
//					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up", "get_up_normal")
//						SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "get_up", "get_up_normal", 0.413)
//					ENDIF
//					
//					#IF IS_DEBUG_BUILD
//						PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
//							bIsCutSceneRunning = FALSE
//						ENDIF	
//					#ENDIF
//					
//					IF bloodImpactPTFXStarted = FALSE
//						IF TIMERA() > 200
//							IF DOES_ENTITY_EXIST(EndPlane)
//								IF IS_VEHICLE_DRIVEABLE(EndPlane)
//									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_747_blood_impact", EndPlane, <<-11.956, 10.528, -7.657>>, <<0,0,0>>, 1)
//									PLAY_SOUND_FROM_COORD(-1, "Trevor_4_747_Man_Sucked_In", <<-938.60, -2984.13, 15.47>>)
//									bloodImpactPTFXStarted = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					IF TIMERA() > 500
//						IF DOES_ENTITY_EXIST(EnemyPed)
//							DELETE_PED(EnemyPed)
//							SET_TIME_SCALE(1.0)
//							IF DOES_BLIP_EXIST(EnemyPedBlip)
//								REMOVE_BLIP(EnemyPedBlip)
//							ENDIF
//							
//							IF DOES_ENTITY_EXIST(EndPlane)
//								IF IS_VEHICLE_DRIVEABLE(EndPlane)
//									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_747_engine_debris", EndPlane, <<-11.972, 5.759, -7.643>>, <<180, 0, 0>>, 1)
//								ENDIF
//							ENDIF
//							
//							SET_CAM_PARAMS(CamInit, <<-941.875549,-2975.222900,13.360523>>,<<14.499500,0.727120,174.823532>>,32.464653)
//							SET_CAM_PARAMS(CamDest, <<-941.718140,-2973.460938,13.048782>>,<<12.559783,0.770186,165.319229>>,32.464653)
//							SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 3000)							
//							SHAKE_CAM(CamInit, "HAND_SHAKE", 0.3)
//							SET_UP_END_COPS()
//							
//							icutsceneStage ++
//						ENDIF
//					ENDIF
//					
//				BREAK
//				
//				CASE 4
//
//					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up", "get_up_normal")
//						SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "get_up", "get_up_normal", 0.413)
//					ENDIF
//
//					//Set timer up to control when engines should get turned off
//					SETTIMERB(0)
//					
//					//turn end planes collision back on.
//					IF NOT IS_ENTITY_DEAD(EndPlane)
//						SET_ENTITY_COLLISION(EndPlane, TRUE)
//					ENDIF					
//					
////					PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
////					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
//					IF TIMERA() > 3000
//						bIsCutSceneRunning = FALSE
//					ENDIF
////					ENDIF
//					
//				BREAK
//				
//			ENDSWITCH
//			
//			WAIT(0)
//			
//		ENDWHILE
//		
//		IF bIsCutSceneRunning = FALSE
//
//			SET_CAM_ACTIVE(CamDest, FALSE)
//			SET_CAM_ACTIVE(CamInit, FALSE)
//			
////			IF NOT DOES_ENTITY_EXIST(BloodOnGround)
////				BloodOnGround = CREATE_OBJECT(AP1, << -929.42, -2998.39, 12.95 >>)
////				SET_ENTITY_ROTATION(BloodOnGround, <<0.00, 0.00, 60.73>>)
////			ENDIF	
//			
////			IF DOES_ENTITY_EXIST(AirWorker)
////				IF NOT IS_PED_INJURED(AirWorker)
////					SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
////				ENDIF
////			ENDIF
//			
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			
//			DISPLAY_RADAR(TRUE)
//			DISPLAY_HUD(TRUE)
//			
//			SET_WIDESCREEN_BORDERS(FALSE, -1)
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			
//			SET_TIME_SCALE(1.0)
//			
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			
//			SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
//			
//			iControlFlag = 3
//			
//		ENDIF
//		
//	ENDIF

	IF iControlFlag = 2
		
		#IF IS_DEBUG_BUILD
			DO_PED_BLAST_WIDGET()
		#ENDIF				

		iControlFlag = 0
		missionStage = STAGE_EVADE_POLICE
	
	ENDIF		
		
//	IF iControlFlag = 0
//	
//		PRINTSTRING("DO_STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE") PRINTNL()
//		
//		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//		
////******************************************* RESET FLAGS ****************************************	
//		enginefx5done 			= FALSE
//		ScriptCamActivated 		= FALSE
//		JetEngineSoundIDPlayed 	= FALSE
//		icutsceneStage 			= 0
//		iskip_cutscene_flag 	= 0
//		
//		//Sound ID for jet engine
//		IF JetEngineSoundIDCreated = FALSE
//			JetEngineSoundID = GET_SOUND_ID()
//			JetEngineSoundIDCreated = TRUE
//		ENDIF
//		
//		REQUEST_ANIM_DICT("get_up")
//		
//		WHILE NOT HAS_ANIM_DICT_LOADED("get_up")
//			PRINTSTRING("WAITING ON ANIM DICT get_up TO LOAD") PRINTNL()
//			WAIT(0)
//		ENDWHILE
//	
////		CALCULATE_PED_WAYPOINT_PLAYBACK_SPEED()	
//	
//		IF NOT DOES_CAM_EXIST(CamInit)
//			CamInit = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		ENDIF
//
//		IF NOT DOES_CAM_EXIST(CamDest)
//			CamDest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//		ENDIF
//		
//		bIsCutSceneRunning = TRUE
//		
//		SET_PAD_CAN_SHAKE_DURING_CUTSCENE(FALSE)
//		
//		DISPLAY_RADAR(FALSE)
//		DISPLAY_HUD(FALSE)
//		
//		SET_WIDESCREEN_BORDERS(TRUE, -1)
//		
//		IF NOT IS_PED_INJURED(EnemyPed)
//			SET_PED_CAN_RAGDOLL(EnemyPed, TRUE)
//		ENDIF
////		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//		
//		WHILE bIsCutSceneRunning = TRUE
//			
//			WAIT(0)
//			
////			CALCULATE_PED_WAYPOINT_PLAYBACK_SPEED()	
//				
//			SWITCH icutsceneStage
//			
//				CASE 0
//
//					SETTIMERA(0)
//					icutsceneStage++
//					
//				BREAK
//				
//				CASE 1
//					
//					PRINTSTRING("Case 1 Timer A = ") PRINTINT(TIMERA()) PRINTNL()
//					
//					//Proc to make worker save player
////					Worker_Stops_Player()							
//					
//					IF ScriptCamActivated = FALSE
//						IF AirWorkerBlasted = TRUE
//							SET_CAM_PARAMS(CamDest, <<-933.597107,-2969.841797,14.915227>>,<<-0.480866,0.048951,167.165039>>, 30.495911)
//							SET_CAM_ACTIVE(CamDest, TRUE)							
//							SET_CAM_PARAMS(CamInit, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV())
//							SET_CAM_ACTIVE(CamInit, TRUE)
//							SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 12000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//							RENDER_SCRIPT_CAMS(TRUE, FALSE)
//							SET_TIME_SCALE(0.3)
//							ScriptCamActivated = TRUE
//						ELSE
//							SET_CAM_PARAMS(CamDest, <<-926.063110,-2974.539795,13.914879>>,<<1.244431,-0.030502,143.893021>>,32.434654)
//							SET_CAM_ACTIVE(CamDest, TRUE)							
//							SET_CAM_PARAMS(CamInit, <<-926.063110,-2974.539795,13.914879>>,<<1.244431,-0.030502,110.675323>>,32.434654)
//							SET_CAM_ACTIVE(CamInit, TRUE)	
//							SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 11000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//							
//							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//							
//							SET_ENTITY_COORDS(PLAYER_PED_ID(), << -934.2948, -2967.9402, 12.9447 >>)
//							SET_ENTITY_HEADING(PLAYER_PED_ID(), 87.0120)
//
//							RENDER_SCRIPT_CAMS(TRUE, FALSE)
//							
//							WAIT(0)
//							SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_60_HangerRm")	
//							PRINTSTRING("Waiting for room to be ready") PRINTNL()
//							
//							SET_TIME_SCALE(0.3)
//							ScriptCamActivated = TRUE
//						ENDIF
//					ENDIF
//					
//					//Stops playback for the airworkers anims.
//					IF AirWorkerAnimStarted = TRUE		
//						IF NOT IS_PED_INJURED(AirWorker)
//							IF IS_ENTITY_PLAYING_ANIM(AirWorker, "amb@celebration", "male_two_handed_a")
//								IF GET_ENTITY_ANIM_CURRENT_TIME(AirWorker, "amb@celebration", "male_two_handed_a") > 0.27
//									STOP_ANIM_TASK(AirWorker, "male_two_handed_a", "amb@celebration", NORMAL_BLEND_OUT)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF				
//					
//					IF TIMERA() > 1255
//						
//						//Particle effects to make it look like the engines just got turned on.
//						ADD_PARTICLE_FX_TO_PLANES_ENGINES()
//						IF JetEngineSoundIDPlayed = FALSE
//							PLAY_SOUND_FROM_COORD(JetEngineSoundID, "Trevor_4_747_Jet_Engine", <<-937.77, -2981.36, 15.44>>)
//							JetEngineSoundIDPlayed = TRUE
//						ENDIF
//						icutsceneStage++
//					ENDIF
//					
//				BREAK
//				
//				CASE 2
//				
//					PRINTSTRING("Case 2 Timer A = ") PRINTINT(TIMERA()) PRINTNL()				
//				
////					IF ScriptCamActivated = FALSE
////						IF AirWorkerBlasted = TRUE
////							SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 9000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
////							RENDER_SCRIPT_CAMS(TRUE, FALSE)
////							ScriptCamActivated = TRUE
////						ENDIF					
////					ENDIF					
//				
//					//Proc to make worker save player
////					Worker_Stops_Player()
//						
//					IF DOES_ENTITY_EXIST(EnemyPed)
//						IF NOT IS_PED_INJURED(EnemyPed)
//							IF NOT IS_ENTITY_DEAD(EndPlane)
//								FREEZE_ENTITY_POSITION(EndPlane, TRUE)
//								SET_ENTITY_COLLISION(EndPlane, FALSE)
//							ENDIF
////							IF IS_ENTITY_AT_COORD(EnemyPed, <<-933.00, -2987.68, 12.8>>, <<1, 1, 2>>)
//							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(EnemyPed)
//								IF GET_PED_WAYPOINT_PROGRESS(EnemyPed) = 19
//									CLEAR_PED_TASKS(EnemyPed)
//									SET_PED_TO_RAGDOLL(EnemyPed, 0, 10000, TASK_RELAX, FALSE)
//									BLAST_PED(EnemyPed, <<0.240, -0.396, 0.350>>, <<0,0,0>>, 10)//7.5
//									BLAST_PED(EnemyPed, <<6.000, -2.016, 0.673>>, <<0,0,0>>, 5)//10  <<6.000, -7.016, 0.673>>
//									
//									SET_CAM_PARAMS(CamInit, <<-928.238953,-2992.675293,13.757324>>,<<7.145424,0.727125,50.626450>>,41.034195)
//									SET_CAM_PARAMS(CamDest, <<-931.479858,-2989.399902,14.404553>>,<<7.145424,0.727125,50.626450>>,41.034195)
//									SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 1800, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//									
//									SET_ENTITY_COORDS(PLAYER_PED_ID(), << -934.2948, -2967.9402, 12.9447 >>)
//									SET_ENTITY_HEADING(PLAYER_PED_ID(), 87.0120)
//									
//									EQUIP_BEST_PLAYER_WEAPON(TRUE)
//									
//									IF NOT IS_PED_INJURED(AirWorker)
//										CLEAR_PED_TASKS_IMMEDIATELY(AirWorker)
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, TRUE)
//										SET_PED_DUCKING(AirWorker, TRUE)
//										SET_ENTITY_COORDS(AirWorker, << -934.2151, -2963.7605, 12.9447 >>)
//										SET_ENTITY_HEADING(AirWorker, 225.6773)	
//									ENDIF
//									
//									TASK_PLAY_ANIM(PLAYER_PED_ID(), "get_up", "get_up_normal", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, 0.413)
//									SETTIMERA(0)
//									
//									icutsceneStage ++
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//				BREAK
//				
//				CASE 3
//					
//					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up", "get_up_normal")
//						SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "get_up", "get_up_normal", 0.413)
//					ENDIF
//					
//					#IF IS_DEBUG_BUILD
//						PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
//						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
//							bIsCutSceneRunning = FALSE
//						ENDIF	
//					#ENDIF
//					
//					IF bloodImpactPTFXStarted = FALSE
//						IF TIMERA() > 200
//							IF DOES_ENTITY_EXIST(EndPlane)
//								IF IS_VEHICLE_DRIVEABLE(EndPlane)
//									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_747_blood_impact", EndPlane, <<-11.956, 10.528, -7.657>>, <<0,0,0>>, 1)
//									PLAY_SOUND_FROM_COORD(-1, "Trevor_4_747_Man_Sucked_In", <<-938.60, -2984.13, 15.47>>)
//									bloodImpactPTFXStarted = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					IF TIMERA() > 500
//						IF DOES_ENTITY_EXIST(EnemyPed)
//							DELETE_PED(EnemyPed)
//							SET_TIME_SCALE(1.0)
//							IF DOES_BLIP_EXIST(EnemyPedBlip)
//								REMOVE_BLIP(EnemyPedBlip)
//							ENDIF
//							
//							IF DOES_ENTITY_EXIST(EndPlane)
//								IF IS_VEHICLE_DRIVEABLE(EndPlane)
//									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_trev4_747_engine_debris", EndPlane, <<-11.972, 5.759, -7.643>>, <<180, 0, 0>>, 1)
//								ENDIF
//							ENDIF
//							
//							SET_CAM_PARAMS(CamInit, <<-941.875549,-2975.222900,13.360523>>,<<14.499500,0.727120,174.823532>>,32.464653)
//							SET_CAM_PARAMS(CamDest, <<-941.718140,-2973.460938,13.048782>>,<<12.559783,0.770186,165.319229>>,32.464653)
//							SET_CAM_ACTIVE_WITH_INTERP(CamDest, CamInit, 3000)							
//							SHAKE_CAM(CamInit, "HAND_SHAKE", 0.3)
//							SET_UP_END_COPS()
//							
//							icutsceneStage ++
//						ENDIF
//					ENDIF
//					
//				BREAK
////				
////				CASE 4
////				
////					PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
////					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
////						bIsCutSceneRunning = FALSE
////					ENDIF					
////					
////					IF TIMERA() > 500
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.14, -2982.19, 15.39>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.85, -2982.34, 15.28>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.28, -2982.05, 15.01>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.27, -2982.06, 14.73>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.71, -2982.11, 15.11>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.12, -2982.35, 15.14>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.04, -2982.27, 15.21>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.90, -2982.33, 15.22>>, <<0,0,0>>,1)
////						icutsceneStage ++
////					ENDIF
////
////				BREAK
////
////				CASE 5			
////				
////					IF TIMERA() > 850
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.14, -2982.19, 15.39>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.85, -2982.34, 15.28>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.28, -2982.05, 15.01>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.27, -2982.06, 14.73>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.71, -2982.11, 15.11>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.12, -2982.35, 15.14>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.04, -2982.27, 15.21>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.90, -2982.33, 15.22>>, <<0,0,0>>,1)
////						icutsceneStage ++
////					ENDIF
////					
////				BREAK
////				
////				CASE 6			
////				
////					IF TIMERA() > 1150
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.14, -2982.19, 15.39>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.85, -2982.34, 15.28>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.28, -2982.05, 15.01>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.27, -2982.06, 14.73>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.71, -2982.11, 15.11>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.12, -2982.35, 15.14>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.04, -2982.27, 15.21>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.90, -2982.33, 15.22>>, <<0,0,0>>,1)
////						icutsceneStage ++
////					ENDIF
////				
////					PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
////					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
////						bIsCutSceneRunning = FALSE
////					ENDIF	
////					
////				BREAK
////				
////				CASE 7				
////				
////					IF TIMERA() > 1450
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.14, -2982.19, 15.39>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.85, -2982.34, 15.28>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.28, -2982.05, 15.01>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.27, -2982.06, 14.73>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.71, -2982.11, 15.11>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.12, -2982.35, 15.14>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.04, -2982.27, 15.21>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.90, -2982.33, 15.22>>, <<0,0,0>>,1)
////						icutsceneStage ++
////					ENDIF
////				
////					PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
////					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
////						bIsCutSceneRunning = FALSE
////					ENDIF	
////					
////				BREAK
////				
////				CASE 8
////
////					IF TIMERA() > 1750
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.14, -2982.19, 15.39>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.85, -2982.34, 15.28>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.28, -2982.05, 15.01>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.27, -2982.06, 14.73>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.71, -2982.11, 15.11>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.12, -2982.35, 15.14>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-942.04, -2982.27, 15.21>>, <<0,0,0>>,1)
////						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_exp_phone_head", <<-941.90, -2982.33, 15.22>>, <<0,0,0>>,1)
////						icutsceneStage ++
////					ENDIF
////				
////					PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
////					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
////						bIsCutSceneRunning = FALSE
////					ENDIF	
////					
////				BREAK
//				
//				CASE 4
//
//					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "get_up", "get_up_normal")
//						SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "get_up", "get_up_normal", 0.413)
//					ENDIF
//
//					//Set timer up to control when engines should get turned off
//					SETTIMERB(0)
//					
//					//turn end planes collision back on.
//					IF NOT IS_ENTITY_DEAD(EndPlane)
//						SET_ENTITY_COLLISION(EndPlane, TRUE)
//					ENDIF					
//					
////					PRINTSTRING("Timer A = ") PRINTINT(TIMERA()) PRINTNL()
////					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
//					IF TIMERA() > 3000
//						bIsCutSceneRunning = FALSE
//					ENDIF
////					ENDIF
//					
//				BREAK
//				
//			ENDSWITCH
//			
//		ENDWHILE
//		
//		IF bIsCutSceneRunning = FALSE
//
//			SET_CAM_ACTIVE(CamDest, FALSE)
//			SET_CAM_ACTIVE(CamInit, FALSE)
//			
////			IF NOT DOES_ENTITY_EXIST(BloodOnGround)
////				BloodOnGround = CREATE_OBJECT(AP1, << -929.42, -2998.39, 12.95 >>)
////				SET_ENTITY_ROTATION(BloodOnGround, <<0.00, 0.00, 60.73>>)
////			ENDIF	
//			
//			IF DOES_ENTITY_EXIST(AirWorker)
//				IF NOT IS_PED_INJURED(AirWorker)
//					SET_ENTITY_PROOFS(AirWorker, FALSE, FALSE, FALSE, FALSE, FALSE)
//				ENDIF
//			ENDIF
//			
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			
//			DISPLAY_RADAR(TRUE)
//			DISPLAY_HUD(TRUE)
//			
//			SET_WIDESCREEN_BORDERS(FALSE, -1)
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			
//			SET_TIME_SCALE(1.0)
//			
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			
//			SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
//			
//			iControlFlag = 1
//			
//		ENDIF
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//	
//	#IF IS_DEBUG_BUILD
//		DO_PED_BLAST_WIDGET()
//	#ENDIF
//		
//		//Temp Fix for wanted level bug. will be removed when bug is fixed
////		SET_MAX_WANTED_LEVEL(4)				
//
//		iControlFlag = 0
//		missionStage = STAGE_EVADE_POLICE
//	
//	ENDIF
	
ENDPROC

PROC DO_STAGE_EVADE_POLICE()

	IF IS_SCREEN_FADED_OUT()
		IF DOES_ENTITY_EXIST(GetAwayPlane)
			SET_VEHICLE_DOOR_OPEN(GetAwayPlane, SC_DOOR_FRONT_LEFT, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF iControlFlag > 0
		HANDLE_AIR_WORKER()
	ENDIF	

	IF iControlFlag = 0			

		//DEBUG STAGE SELECTOR
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_EVADE_POLICE", TRUE)
		
//*********************************** RESET FLAGS *******************************************
		copsPlaybackStarted 					= FALSE	
		copsInCar0ReadyForAction				= FALSE
		copsInCar1ReadyForAction 				= FALSE
		endcops6and7tasksgiven 					= FALSE
		endcops8and9tasksgiven 					= FALSE
		endcop10TaskGiven 						= FALSE
		donePoliceChat1 						= FALSE
		donePoliceChat2 						= FALSE
		Cop14TaskGiven 							= FALSE
		getawayCar0Setup			 			= FALSE
		getawayCar1Setup 						= FALSE
		jetMusicTriggered 						= FALSE
		timerStarted 							= FALSE
		iSubStage								= 0
		
		//Request this anim dict set for the task for the police to dive through the doors.
		REQUEST_ANIM_DICT("Misssolomon_3")		
		REQUEST_MODEL(ORACLE2)
		REQUEST_MODEL(BALLER2)
		
		//Remove enemypeds blip	
		IF DOES_BLIP_EXIST(EnemyPedBlip)
			REMOVE_BLIP(EnemyPedBlip)
		ENDIF			
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Trev4_5")
			USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("Trev4_5", FALSE, 1, 1)	
			REMOVE_WAYPOINT_RECORDING("Trev4_5")
		ENDIF	
				
		IF DOES_ENTITY_EXIST(PlayerCar)	
			SET_VEHICLE_AS_NO_LONGER_NEEDED(PlayerCar)
		ENDIF
				
		SETTIMERA(0)

		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP()
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		IF DOES_ENTITY_EXIST(AirWorker)
			IF NOT IS_PED_INJURED(AirWorker)
				SET_PED_CAN_BE_TARGETTED(AirWorker, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AirWorker, FALSE)
			ENDIF
		ENDIF
		
		TRIGGER_MUSIC_EVENT("TRV4_LOSE_COPS")
		IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS")
			START_AUDIO_SCENE("SOL_3_LOSE_COPS")
		ENDIF
		
		CLEAR_PRINTS()
		PRINT_NOW("TRV4_END1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose the cops.		
		
		IF IS_AUDIO_SCENE_ACTIVE("SOL_3_ENGINE_DEATH_SCENE")
			STOP_AUDIO_SCENE("SOL_3_ENGINE_DEATH_SCENE")
		ENDIF		
		
		IF IS_RESTRICTED_AREA_WANTED_LEVEL_SUPPRESSED(AC_AIRPORT_AIRSIDE)
			RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
		ENDIF
		
		NavBlockingArea1 = ADD_NAVMESH_BLOCKING_OBJECT(<<-948.4, -2992.6, 13.3>>, <<6,5,4>>, 60)
		IF DOES_ENTITY_EXIST(EndPlane)
			IF IS_VEHICLE_DRIVEABLE(EndPlane)
				SET_VEHICLE_ACTIVE_FOR_PED_NAVIGATION(EndPlane, FALSE)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(GetAwayPlane)
			IF IS_VEHICLE_DRIVEABLE(GetAwayPlane)
				SET_VEHICLE_ACTIVE_FOR_PED_NAVIGATION(GetAwayPlane, FALSE)
			ENDIF
		ENDIF
		
		//Set Players wanted level
		SET_MAX_WANTED_LEVEL(5)	
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		SET_AUDIO_FLAG("PoliceScannerDisabled", FALSE)
		
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
			
		//Set up a couple of parked cars for escaping in.
		IF getawayCar0Setup = FALSE
			IF NOT DOES_ENTITY_EXIST(GetAwayCar[0])
				IF HAS_MODEL_LOADED(ORACLE2)
					GetAwayCar[0] = CREATE_VEHICLE(ORACLE2, <<-1025.5964, -2869.2366, 12.9585>>, 241.0686)
					SET_ENTITY_LOAD_COLLISION_FLAG(GetAwayCar[0], TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(GetAwayCar[0])
					SET_MODEL_AS_NO_LONGER_NEEDED(ORACLE2)
					getawayCar0Setup = TRUE
				ENDIF
			ENDIF
		ENDIF	
		IF getawayCar1Setup = FALSE
			IF NOT DOES_ENTITY_EXIST(GetAwayCar[1])
				IF HAS_MODEL_LOADED(BALLER2)
					GetAwayCar[1] = CREATE_VEHICLE(BALLER2, <<-971.4223, -2903.3535, 12.9652>>, 61.0213)
					SET_ENTITY_LOAD_COLLISION_FLAG(GetAwayCar[1], TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(GetAwayCar[1])
					SET_MODEL_AS_NO_LONGER_NEEDED(BALLER2)
					getawayCar1Setup = TRUE
				ENDIF
			ENDIF
		ENDIF
		//Clean them up
		IF DOES_ENTITY_EXIST(GetAwayCar[0])
			IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GetAwayCar[0])
					IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_VEHICLE")
						START_AUDIO_SCENE("SOL_3_LOSE_COPS_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GetAwayCar[0]) > 200
			OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GetAwayCar[0])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayCar[0])	
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(GetAwayCar[1])
			IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GetAwayCar[1])
					IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_VEHICLE")
						START_AUDIO_SCENE("SOL_3_LOSE_COPS_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GetAwayCar[1]) > 200
			OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GetAwayCar[1])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayCar[1])	
			ENDIF
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//			DO_PED_BLAST_WIDGET()
//		#ENDIF			
			
		//Some police Dialogue
		IF donePoliceChat1 = FALSE
			IF NOT IS_PED_INJURED(EndCop[0])
 				PLAY_PED_AMBIENT_SPEECH(EndCop[0], "SURROUNDED") 
				donePoliceChat1 = TRUE
			ENDIF
		ENDIF
			
		IF copsPlaybackStarted = FALSE	
			IF TIMERA() > 8000
				IF IS_VEHICLE_DRIVEABLE(endpoliceCar[0])
					START_PLAYBACK_RECORDED_VEHICLE(endpoliceCar[0], 68, "BB_Chase")
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(endpoliceCar[0], <<0,0,0.15>>)
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(endpoliceCar[1])
					START_PLAYBACK_RECORDED_VEHICLE(endpoliceCar[1], 69, "BB_Chase")
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(endpoliceCar[1], <<0,0,0.15>>)
				ENDIF	
				copsPlaybackStarted = TRUE
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(endpoliceCar[0])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(endpoliceCar[0])
					IF GET_TIME_POSITION_IN_RECORDING(endpoliceCar[0]) > 5000
						IF IS_VEHICLE_STOPPED(endpoliceCar[0])
							STOP_PLAYBACK_RECORDED_VEHICLE(endpoliceCar[0])
						ENDIF
					ENDIF
				ELSE
					IF copsInCar0ReadyForAction = FALSE
						IF NOT IS_PED_INJURED(EndCop[6])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[6], FALSE)
						ENDIF
						IF NOT IS_PED_INJURED(EndCop[7])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[7], FALSE)
						ENDIF
						copsInCar0ReadyForAction = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(endpoliceCar[1])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(endpoliceCar[1])
					IF GET_TIME_POSITION_IN_RECORDING(endpoliceCar[1]) > 5000
						IF IS_VEHICLE_STOPPED(endpoliceCar[1])
							STOP_PLAYBACK_RECORDED_VEHICLE(endpoliceCar[1])
						ENDIF
					ENDIF
				ELSE
					IF copsInCar1ReadyForAction = FALSE
						IF NOT IS_PED_INJURED(EndCop[8])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[8], FALSE)
						ENDIF
						IF NOT IS_PED_INJURED(EndCop[9])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EndCop[9], FALSE)
						ENDIF
						copsInCar1ReadyForAction = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
//		
//		IF enginesTurnedOff = FALSE	
//			IF TIMERB() > 8000
//				//Turn off Jets engines
//				DETACH_ENGINE_PTFX_TO_JET()
//				IF JetEngineSoundIDPlayed = TRUE
//					STOP_SOUND(JetEngineSoundID)
//					RELEASE_SOUND_ID(JetEngineSoundID)
//					//Reset creation flag incase player skips back.
////					JetEngineSoundIDCreated = FALSE
//				ENDIF
//				enginesTurnedOff = TRUE
//			ENDIF
//		ENDIF
		
		POLICE_ATTACK_CONTROL()
		
		IF jetMusicTriggered = FALSE
			IF DOES_ENTITY_EXIST(GetAwayPlane)
				IF IS_VEHICLE_DRIVEABLE(GetAwayPlane)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GetAwayPlane)
						TRIGGER_MUSIC_EVENT("TRV4_JET_ENTERED")
						IF NOT IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_PLANE")
							START_AUDIO_SCENE("SOL_3_LOSE_COPS_PLANE")
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
						
						SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayPlane)
						jetMusicTriggered = TRUE
					ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GetAwayPlane) > 200
						SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayPlane)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF jetMusicTriggered = TRUE
			IF DOES_ENTITY_EXIST(GetAwayPlane)
				IF IS_VEHICLE_DRIVEABLE(GetAwayPlane)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GetAwayPlane)
						IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_PLANE")
							STOP_AUDIO_SCENE("SOL_3_LOSE_COPS_PLANE")
						ENDIF
						TRIGGER_MUSIC_EVENT("TRV4_LOSE_COPS")
						jetMusicTriggered = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Check the player has left the airport 1st then once wanted level has been lost pass the mission.
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF timerStarted = FALSE
				iplayerwantedLevelTimer = GET_GAME_TIMER()
				timerStarted = TRUE
			ENDIF
		ENDIF
		
		IF timerStarted = TRUE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF GET_GAME_TIMER() > (iplayerwantedLevelTimer + 3500)
					IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_PLANE")
						STOP_AUDIO_SCENE("SOL_3_LOSE_COPS_PLANE")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS")
						STOP_AUDIO_SCENE("SOL_3_LOSE_COPS")
					ENDIF	
					IF IS_AUDIO_SCENE_ACTIVE("SOL_3_LOSE_COPS_VEHICLE")
						STOP_AUDIO_SCENE("SOL_3_LOSE_COPS_VEHICLE")
					ENDIF	
					TRIGGER_MUSIC_EVENT("TRV4_COPS_LOST")
					//Stats for stopping the timer for losing the wanted level
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					Mission_Passed()
				ENDIF
			ELSE
				timerStarted = FALSE
			ENDIF
		ENDIF
		
	ENDIF
		

ENDPROC

//DEBUG
#IF IS_DEBUG_BUILD
PROC DO_STAGE_DEBUG_PLAYBACK_UBER()
	
	IF iControlFlag = 0
		
		PRINTSTRING("*********STAGE_DEBUG_PLAYBACK_UBER*************") PRINTNL()
	
		REQUEST_MODEL(COGCABRIO)
		REQUEST_MODEL(JET)
		REQUEST_MODEL(TANKER)
		REQUEST_VEHICLE_RECORDING(45, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(35, "BB_Chase")
		REQUEST_VEHICLE_RECORDING(2, "BB_Chase")
		
		WHILE NOT HAS_MODEL_LOADED(COGCABRIO)
		OR NOT HAS_MODEL_LOADED(JET)
		OR NOT HAS_MODEL_LOADED(TANKER)
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(45, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(35, "BB_Chase")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "BB_Chase")
			PRINTSTRING("*********LOADING ASSETS*************") PRINTNL()
			WAIT(0)
		ENDWHILE
		
		EnemyCar = CREATE_VEHICLE(COGCABRIO, EnemyCarChaseStartingCoords, fEnemyCarChaseStartingHeading)
		SET_VEHICLE_COLOURS(EnemyCar, 27, 27)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), EnemyCar)
		SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
		
		#IF IS_DEBUG_BUILD
			set_uber_parent_widget_group(Airport_Chase_Widget_Group)
		#endif 		
		
		iControlFlag = 1
	ENDIF
	
	IF iControlFlag = 1
		PRINTSTRING("*********icontrolflag = 1*************") PRINTNL()
		
		//Set flags for uber recording. 
		// Setting this to true will spawn all set piece cars if they're on screen or not.
		bCreateAllWaitingCars = TRUE
		//Set this to true to stop set pieces switching to AI
		bSetPieceCarsDontSwitchToAI = TRUE
		
		INITIALISE_UBER_PLAYBACK("BB_Chase", 45, TRUE)
		iControlFlag = 2
	ENDIF

	IF iControlFlag = 2
		PRINTSTRING("*********icontrolflag = 2*************") PRINTNL()
		FILL_UBER_RECORDING2()
		iControlFlag = 3
	ENDIF
	
	IF iControlFlag = 3
		PRINTSTRING("*********icontrolflag = 3*************") PRINTNL()
		START_PLAYBACK_RECORDED_VEHICLE(EnemyCar, 45, "BB_Chase")
		iControlFlag = 4
	ENDIF
	
	IF iControlFlag = 4
		
		CHOPPER_CAM()
				
		#IF IS_DEBUG_BUILD
			DO_DEBUG_CAR_NAMES()
		#ENDIF			
		UPDATE_UBER_PLAYBACK(EnemyCar, 1)
		
		//Set up 1st set piece not part of uber recording and delete it when required.
		IF setpiecePlane2Setup = FALSE
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 16990
						SetPiecePlane[1] = CREATE_VEHICLE(JET, <<-441.3117, -3435.7363, 409.9454>>, 148.3975)
						START_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[1], 2, "BB_Chase")
						setpiecePlane2Setup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(SetPiecePlane[1])
				IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[1])
					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPiecePlane[1])
							SET_PLAYBACK_SPEED(SetPiecePlane[1], fPlaybackSpeed)
//							PRINTSTRING("Set peice plane 1 time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(SetPiecePlane[1])) PRINTNL()
							IF GET_TIME_POSITION_IN_RECORDING(SetPiecePlane[1]) > 20000
								IF GET_DISTANCE_FROM_LEAD_CAR(SetPiecePlane[1], PlayerCar) > 200
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[1])
									IF DOES_ENTITY_EXIST(setPiecePlanePilot[1])
										DELETE_PED(setPiecePlanePilot[1])
									ENDIF
									DELETE_VEHICLE(SetPiecePlane[1])
									iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2269554
								ENDIF
							ENDIF
						ELSE
							IF GET_DISTANCE_FROM_LEAD_CAR(SetPiecePlane[1], PlayerCar) > 200
								IF DOES_ENTITY_EXIST(setPiecePlanePilot[1])
									DELETE_PED(setPiecePlanePilot[1])
								ENDIF
								DELETE_VEHICLE(SetPiecePlane[1])
								iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2269554
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		
		IF NOT DOES_ENTITY_EXIST(Trailer)
			REQUEST_MODEL(TANKER)
			IF HAS_MODEL_LOADED(TANKER)
			 	Trailer = CREATE_VEHICLE(Tanker, <<-1325, -2779, 14>>, 233.9563)
				SET_MODEL_AS_NO_LONGER_NEEDED(TANKER)
			ENDIF
		ENDIF
		
		IF trailerdetached = FALSE
			IF DOES_ENTITY_EXIST(setpieceCarId[1])
				IF DOES_ENTITY_EXIST(Trailer)
					IF NOT IS_ENTITY_DEAD(setpieceCarId[1])
						IF NOT IS_ENTITY_DEAD(Trailer)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceCarId[1])
								PRINTSTRING("setpieceCarId[1] time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(setpieceCarId[1])) PRINTNL()
								IF vehicle_recording_started = FALSE
									ATTACH_VEHICLE_TO_TRAILER(setpieceCarId[1], Trailer)
								
									START_RECORDING_VEHICLE(trailer, 68, "BB_Chase", FALSE)
									vehicle_recording_started = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(Trailer)
				IF NOT IS_ENTITY_DEAD(EnemyCar)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
						IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 40241
							DETACH_ENTITY(Trailer)
							trailerdetached = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF vehicle_recording_stopped = FALSE
			IF vehicle_recording_started = TRUE
				IF DOES_ENTITY_EXIST(Trailer)
					IF NOT IS_ENTITY_DEAD(EnemyCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 47348	
								STOP_RECORDING_VEHICLE(Trailer)
								vehicle_recording_stopped = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
						
		//Set up 3rd set piece not part of uber recording.
		IF setpiecePlane1Setup = FALSE
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 66500
						SetPiecePlane[0] = CREATE_VEHICLE(JET, << -1119.0723, -1864.5477, -8.2205 >>, 148.3975)
						START_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[0], 35, "BB_Chase")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[0], 3491)
						setpiecePlane1Setup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(SetPiecePlane[0])
				IF IS_VEHICLE_DRIVEABLE(SetPiecePlane[0])
					IF IS_VEHICLE_DRIVEABLE(PlayerCar)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPiecePlane[0])
							IF GET_DISTANCE_FROM_LEAD_CAR(SetPiecePlane[0], PlayerCar) > 150
								STOP_PLAYBACK_RECORDED_VEHICLE(SetPiecePlane[0])
								IF DOES_ENTITY_EXIST(setPiecePlanePilot[0])
									DELETE_PED(setPiecePlanePilot[0])
								ENDIF
								DELETE_VEHICLE(SetPiecePlane[0])
								iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2269554
							ENDIF
						ELSE
							SET_PLAYBACK_SPEED(SetPiecePlane[0], fPlaybackSpeed)
//							PRINTSTRING("Plane's recording time is ") PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(SetPiecePlane[0])) PRINTNL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
//		
//		//Blow up car 14
//		IF DOES_ENTITY_EXIST(EnemyCar)
//			IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 73619		
//						IF DOES_ENTITY_EXIST(SetPieceCarID[9])
//							IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[9])
//								IF DOES_ENTITY_EXIST(SetPieceCarID[6])
//									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[6])
//										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[6])
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[6])
//											EXPLODE_VEHICLE(SetPieceCarID[6])
//											STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[9])
//											EXPLODE_VEHICLE(SetPieceCarID[9])
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF			
		
		//Blow up trailer 
		IF DOES_ENTITY_EXIST(EnemyCar)
			IF IS_VEHICLE_DRIVEABLE(EnemyCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 41204
						IF DOES_ENTITY_EXIST(Trailer)
							IF NOT IS_ENTITY_DEAD(Trailer)
								IF DOES_ENTITY_EXIST(SetPieceCarID[7])
									IF NOT IS_ENTITY_DEAD(SetPieceCarID[7])
										IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[7]))
											SET_ENTITY_HEALTH(GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[7]), 0)
										ENDIF
										STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[7])
										EXPLODE_VEHICLE(SetPieceCarID[7])
										//Create explosion and flames 
//										START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("exp_grd_rpg", Trailer, <<-1221, -2852, 17>>, <<0,0,0>>, 1)
//										C4[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fbi_ext_blaze", trailer, <<-1221, -2852, 17>>, <<0,0,0>>, 1)
										ADD_EXPLOSION(<<-1221, -2852, 17>>,EXP_TAG_DIR_GAS_CANISTER, 1.0)	
//										START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("exp_grd_rpg", Trailer, <<-1212, -2855, 14.34>>, <<0,0,0>>, 1)
//										C4[4] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fbi_ext_blaze", trailer, <<-1212, -2855, 14.34>>, <<0,0,0>>, 1)
										ADD_EXPLOSION(<<-1212, -2855, 14.34>>,EXP_TAG_DIR_GAS_CANISTER, 1.0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF				
				ENDIF
			ENDIF
		ENDIF	
		
//		//Blast and blow up car 10
//		IF DOES_ENTITY_EXIST(EnemyCar)
//			IF IS_VEHICLE_DRIVEABLE(EnemyCar)	
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//					IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 81416
//						IF DOES_ENTITY_EXIST(SetPieceCarID[10])
//							IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[10])
//								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[10])
//									SET_TIME_SCALE(0.3)
//									SET_GAMEPLAY_VEHICLE_HINT(SetPieceCarID[10], <<0,0,0>>, TRUE, 2000, 500, 500)
//									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[10])
//									BLAST_VEHICLE(SetPieceCarID[10], <<-0.142, 0.953, 4.069>>, <<0,5,0>>, 3)
//									BLAST_VEHICLE(SetPieceCarID[10], <<-1.076, -53.342, 0.710>>, <<0,0,0>>, 60)
//									SETTIMERA(0)
//									Car10Blasted = TRUE
//								ELSE
//									IF Car10Blasted = TRUE
//										IF TIMERA() > 1358
//											SET_TIME_SCALE(1)
//											EXPLODE_VEHICLE(SetPieceCarID[10])
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Blast and blow up car 8
		IF DOES_ENTITY_EXIST(SetPieceCarID[8])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 50845
									SET_TIME_SCALE(0.3)
									SET_GAMEPLAY_VEHICLE_HINT(SetPieceCarID[8], <<0,0,0>>, TRUE, 2000, 500, 500)
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[8])
									BLAST_VEHICLE(SetPieceCarID[8], <<0.072, 1.638, 1.362>>, <<0,4,0>>, 2.3)
									BLAST_VEHICLE(SetPieceCarID[8], <<0.024, -3.4, 0.462>>, <<0,0,0>>, 45)
									Car8HasBeenBlasted = TRUE
									SETTIMERB(0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF Car8HasBeenBlasted = TRUE
						IF TIMERB() > 662
							SET_TIME_SCALE(1.0)
						ENDIF
						IF TIMERB() > 1872
							IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[8])
								EXPLODE_VEHICLE(SetPieceCarID[8])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		
		//Blast and blow up car 11
		IF DOES_ENTITY_EXIST(SetPieceCarID[11])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[11])
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82683
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[11])
									BLAST_VEHICLE(SetPieceCarID[11], <<-0.142, 0.953, 4.069>>, <<0,5,0>>, 3)
									BLAST_VEHICLE(SetPieceCarID[11], <<-1.076, -53.342, 0.710>>, <<0,0,0>>, 60)
									Car11HasBeenBlasted = TRUE
									SETTIMERB(0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF Car11HasBeenBlasted = TRUE
						IF TIMERB() > 1872
							IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[11])
								EXPLODE_VEHICLE(SetPieceCarID[11])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF			

		//Blast and blow up car 12
		IF DOES_ENTITY_EXIST(SetPieceCarID[12])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 82463
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[12])
									BLAST_VEHICLE(SetPieceCarID[12], <<-0.142, 0.953, 4.069>>, <<0,5,0>>, 3)
									BLAST_VEHICLE(SetPieceCarID[12], <<-1.076, -53.342, 0.710>>, <<0,0,0>>, 60)
									Car12HasBeenBlasted = TRUE
									SETTIMERB(0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF Car12HasBeenBlasted = TRUE
						IF TIMERB() > 1000
							IF NOT IS_ENTITY_IN_AIR(SetPieceCarID[12])
								EXPLODE_VEHICLE(SetPieceCarID[12])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		//Blast and blow up car 13
		IF DOES_ENTITY_EXIST(SetPieceCarID[13])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[13])
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 112175
//								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 108830
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[13])
//									APPLY_FORCE_TO_ENTITY(SetPieceCarID[13], APPLY_TYPE_EXTERNAL_IMPULSE, <<-1212.56, -3018.70, 15.77>>, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(SetPieceCarID[13], <<-1213.56, -3019.04, 13.20>>), 0, FALSE, TRUE, TRUE)
									
									EXPLODE_VEHICLE(SetPieceCarID[13])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		//Blast and blow up car 15
		IF DOES_ENTITY_EXIST(SetPieceCarID[15])
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[15])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[15])
					IF DOES_ENTITY_EXIST(EnemyCar)
						IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
								IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 104989
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[15])
									EXPLODE_VEHICLE(SetPieceCarID[15])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
	ENDIF
	
ENDPROC
PROC DO_STAGE_DEBUG_RECORD_UBER()
	
	IF iControlFlag = 0
		REQUEST_MODEL(COGCABRIO)		
		
		WHILE NOT HAS_MODEL_LOADED(COGCABRIO)
			WAIT(0)
		ENDWHILE
	
		EnemyCar = CREATE_VEHICLE(COGCABRIO, EnemyCarChaseStartingCoords, fEnemyCarChaseStartingHeading)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), EnemyCar)
		SET_MODEL_AS_NO_LONGER_NEEDED(COGCABRIO)
		
		#IF IS_DEBUG_BUILD
			set_uber_parent_widget_group(Airport_Chase_Widget_Group)
		#endif 

		iControlFlag = 1
	ENDIF
	
	IF iControlFlag = 1
		INIT_UBER_RECORDING("BB_Chase")
		iControlFlag = 2
	ENDIF
	
	IF iControlFlag = 2
		UPDATE_UBER_RECORDING()
	ENDIF
	
ENDPROC	

PROC DO_STAGE_DEBUG_TEST()

	IF iControlFlag = 0
		
		INTERIOR_INSTANCE_INDEX JetHanger
		
		REQUEST_MODEL(SHAMAL)
		REQUEST_MODEL(JET)
		
		WHILE NOT HAS_MODEL_LOADED(SHAMAL)
		OR NOT HAS_MODEL_LOADED(JET)
			WAIT(0)
		ENDWHILE
		
		JetHanger = GET_INTERIOR_AT_COORDS(<<-967, 2983, 20>>)
//		SET_INTERIOR_ACTIVE()
		WHILE NOT IS_INTERIOR_READY(jetHanger)
			REQUEST_COLLISION_AT_COORD(<< -982.8745, -2953.0205, 12.9451 >>)
			PRINTSTRING("Interior JetHanger is not ready") PRINTNL()
			WAIT(0)
		ENDWHILE

		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -982.8745, -2953.0205, 12.9451 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 222.3527)
		
		IF NOT DOES_ENTITY_EXIST(GetAwayPlane)
			GetAwayPlane = CREATE_VEHICLE(SHAMAL,<< -968.3718, -2952.2976, 12.9451 >>, 114.9439)
			SET_VEHICLE_ON_GROUND_PROPERLY(GetAwayPlane)
			SET_MODEL_AS_NO_LONGER_NEEDED(SHAMAL)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(GetAwayPlane)
			SET_ENTITY_HEALTH(GetAwayPlane, 2000)
		ENDIF	
		
		IF NOT DOES_ENTITY_EXIST(EndPlane)
			EndPlane = CREATE_VEHICLE(JET,<< -952.1978, -2990.2231, 12.9451 >>, 240.5936)
			SET_VEHICLE_LIVERY(EndPlane, 2)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EndPlane, FALSE)
			SET_VEHICLE_ON_GROUND_PROPERLY(EndPlane)
			FREEZE_ENTITY_POSITION(EndPlane, TRUE)
			SET_ENTITY_PROOFS(EndPlane, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(JET)
			SET_ENTITY_LOD_DIST(EndPlane, 500)
		ENDIF
		
		
		
		LOAD_SCENE(<<-967, 2983, 20>>)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
	
	ENDIF

ENDPROC
#ENDIF


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT
	
	PRINTSTRING("TREV 4 - 747 MISSION LAUNCHED")
	PRINTNL()
	
	IF IS_REPEAT_PLAY_ACTIVE()
		SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, FALSE)
	ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 20)
	
//	//To be removed once Ben Rollinson has fixed the flow bug.
//	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
//		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],CHAR_MICHAEL, GET_ENTITY_COORDS(PLAYER_PED_ID()))
//				PRINTSTRING("CREATING MICHAEL") PRINTNL()
//				WAIT(0)
//			ENDWHILE
//		ENDIF
//		MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
//		TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, FALSE)
//	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		STORE_FAIL_WEAPON(PLAYER_PED_ID(), 0)
		TRIGGER_MUSIC_EVENT("TRV4_FAIL")
		PRINTSTRING("playing fail music for death arrest") PRINTNL()
		PRINTSTRING("...Placeholder Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	DISABLE_TAXI_HAILING(TRUE)
	
	SET_MISSION_FLAG(TRUE)	
	
	//INFORM_MISSION_STATS_OF_MISSION_START_TREVOR_FOUR()
	
	#IF IS_DEBUG_BUILD
//		DO_CAR_BLAST_WIDGET()
		jSkipReady = FALSE	
		SET_DEBUG_STAGE_NAMES()
		DO_PED_BLAST_WIDGET()
		set_uber_parent_widget_group(Airport_Chase_Widget_Group)
	#ENDIF	
	
	iControlFlag = 0
	missionStage = STAGE_INIT_MISSION
//	missionStage = STAGE_DEBUG_PLAYBACK_UBER		
//	missionStage = STAGE_DEBUG_TEST
	
	WHILE (TRUE)
		
//		//Record set peice cars getting blasted to keep it consistant every build
//		IF DOES_ENTITY_EXIST(SetPieceCarID[8])
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[8])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//					IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//						START_RECORDING_VEHICLE(SetPieceCarID[8], 108, "bb_chase", TRUE)
//					ENDIF
//				ENDIF
//				IF DOES_ENTITY_EXIST(EnemyCar)
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 52522		
//								IF IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[8])
//									STOP_RECORDING_VEHICLE(SetPieceCarID[8])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		//Record set peice cars getting blasted to keep it consistant every build
//		IF DOES_ENTITY_EXIST(SetPieceCarID[11])
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[11])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[11])
//					IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[11])
//						START_RECORDING_VEHICLE(SetPieceCarID[11], 111, "bb_chase", TRUE)
//					ENDIF
//				ENDIF
//				IF DOES_ENTITY_EXIST(EnemyCar)
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 83675		
//								IF IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[11])
//									STOP_RECORDING_VEHICLE(SetPieceCarID[11])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		//Record set peice cars getting blasted to keep it consistant every build
//		IF DOES_ENTITY_EXIST(SetPieceCarID[12])
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[12])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
//					IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
//						START_RECORDING_VEHICLE(SetPieceCarID[12], 112, "bb_chase", TRUE)
//					ENDIF
//				ENDIF
//				IF DOES_ENTITY_EXIST(EnemyCar)
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 83941		
//								IF IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[12])
//									STOP_RECORDING_VEHICLE(SetPieceCarID[12])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		//Record set peice cars getting blasted to keep it consistant every build
//		IF DOES_ENTITY_EXIST(SetPieceCarID[13])
//			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[13])
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[13])
//					IF NOT IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[13])
//						START_RECORDING_VEHICLE(SetPieceCarID[13], 113, "bb_chase", TRUE)
//					ENDIF
//				ENDIF
//				IF DOES_ENTITY_EXIST(EnemyCar)
//					IF IS_VEHICLE_DRIVEABLE(EnemyCar)		
//						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(EnemyCar)
//							IF GET_TIME_POSITION_IN_RECORDING(EnemyCar) > 111273		
//								IF IS_RECORDING_GOING_ON_FOR_VEHICLE(SetPieceCarID[13])
//									STOP_RECORDING_VEHICLE(SetPieceCarID[13])
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF		
		
		//For video recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_LegalTrouble") 
		
		IF iDisableReplayCameraTimer > GET_GAME_TIMER()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2269554
		ENDIF
		
		//For fail checks
		FAIL_CHECKS()
		
		//Checks what vehicle the player is using 
		PLAYER_VEHICLE()
		
		//Run police check
		POLICE_CHECK()
		
		//Handles all mission dialogue
		MISSION_DIALOGUE()
		
		//Add and remove police cars to audio mix groups
		POLICE_CAR_AUDIO_MIX_GROUPS()
		
		SWITCH missionStage
			
			CASE STAGE_INIT_MISSION
				DO_STAGE_INIT_MISSION()
			BREAK
			
			CASE STAGE_OPENING_CUTSCENE
				DO_STAGE_OPENING_CUTSCENE()
			BREAK

			CASE STAGE_GET_TO_CHASE
				DO_STAGE_GET_TO_CHASE()
			BREAK
			
			CASE STAGE_CHASE_START
				DO_STAGE_CHASE_START()
			BREAK
			
			CASE STAGE_MAIN_CHASE
				DO_STAGE_MAIN_CHASE()
			BREAK
			
			CASE STAGE_ON_FOOT_CHASE
				DO_STAGE_ON_FOOT_CHASE()
			BREAK
			
			CASE STAGE_PED_RUNS_TOWARDS_ENGINE
				DO_STAGE_PED_RUNS_TOWARDS_ENGINE()
			BREAK
			
			CASE STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE
				DO_STAGE_SUCKED_THROUGH_ENGINE_CUTSCENE()
			BREAK
			
			CASE STAGE_EVADE_POLICE
				DO_STAGE_EVADE_POLICE()
			BREAK
				
				CASE STAGE_DEBUG_PLAYBACK_UBER
					#IF IS_DEBUG_BUILD
					DO_STAGE_DEBUG_PLAYBACK_UBER()
					#ENDIF
				BREAK
				
				CASE STAGE_DEBUG_RECORD_UBER
					#IF IS_DEBUG_BUILD
					DO_STAGE_DEBUG_RECORD_UBER()
					#ENDIF
				BREAK
			
				CASE STAGE_DEBUG_TEST
					#IF IS_DEBUG_BUILD
					DO_STAGE_DEBUG_TEST()
					#ENDIF
				BREAK
				
		ENDSWITCH
		WAIT(0)
		
		//Handles all the stats controlled by this script
		STATS_CONTROLER()
		
		//Checks for debug keys being pressed. J-skip, S-Pass, F-Fail, P-Previous
		#IF IS_DEBUG_BUILD
			Debug_Options()
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, 0) = TRUE
				
				//Fade screen out
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
				
				//Flags
				GameCanFail = FALSE
				iControlFlag 	= 0
//				istageskip 		= 0	
				
				//Tells the stat system that the player is warping so invalidates the mission completion time stat.
				//MISSION_STAT_SYSTEM_ALERT_TIME_WARP()
				
				IF iReturnStage = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_GET_TO_CHASE
				ENDIF
				IF iReturnStage = 1
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_CHASE_START
				ENDIF
				IF iReturnStage = 2
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_ON_FOOT_CHASE
				ENDIF
				IF iReturnStage = 3
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_EVADE_POLICE
				ENDIF
			ENDIF
			
		#ENDIF
		
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
