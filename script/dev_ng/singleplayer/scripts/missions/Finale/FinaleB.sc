// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	finale2.sc
//		AUTHOR			:	Ben Barclay
//		DESCRIPTION		:	Franklin has decided to kill Michael. They meet out in the country.
//							After a short chase with trains in the way, they reach a power plant.
//							Here Michael is fleeing whilst every so of tne taking shots at franklin.
//							Michael reaches the top of a tower, frnklin follows to the top.
//							Michael ends up over the barrier whilst fighting franklin.
//							Michael is then droped by franklin onto a police helicopter blade.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"

USING "model_enums.sch"

USING "script_blips.sch"
USING "script_player.sch"
USING "script_misc.sch"
using "script_ped.sch"

Using "Locates_public.sch"
Using "select_mission_stage.sch"
using "asset_management_public.sch"

USING "selector_public.sch"
USING "chase_hint_cam.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
using "completionpercentage_public.sch"
using "timelapse.sch"
using "clearMissionArea.sch"
using "RC_helper_functions.sch" 
USING "achievement_public.sch"
USING "mission_globals.sch"

USING "commands_recording.sch"
USING "cheat_controller_public.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	mpf_frank,
	mpf_mike,
//	mpf_pilot,
	mpf_dead_guy,
//	mpf_police1,
//	mpf_police2,
//	mpf_police3,
//	mpf_police4,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_frank_car,
	mvf_mike_car,
//	mvf_heli,
	mvf_train,
//	mvf_police1,
//	mvf_police2,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_meet,
	msf_1_chase,
	msf_2_power_plant,
	msf_3_confrontation,
	msf_4_heli_appears,
	msf_5_shoot_out,
	msf_6_Ladder_dodge,
	msf_7_over_the_Edge,
	msf_8_passed,
	
	CST_MCS_1,
	CST_MCS_2,
	CST_EXT,
	
	MSF_NUM_OF_STAGES
ENDENUM
ENUM MFF_MISSION_FAIL_FLAGS
	mff_lost_mike,
	mff_police,
	mff_default,	
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
Enum object_List
	obj_dummy,
	obj_dummy1,
	obj_dummy2,
	obj_dummy3,
	obj_dummy4,
	obj_dummy5,
	obj_dummy6,
	
	OBJ_NUM_OF_OBJECTS
endenum
enum HELILIGHT
	HL_FRANK,
	HL_VEC1,
	HL_VEC2,
	HL_MIKE,
	HL_DEAD_MIKE
endenum
enum HELIPOS 
	eH_Corner1,
	eH_Corner2,
	eH_Corner3,
	eH_Corner4
endenum
//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
	BLIP_INDEX			blip
	vector				vdef
ENDSTRUCT
STRUCT OBJ_STRUCT
	OBJECT_INDEX 	id
	vector			pos	
	MODEL_NAMES		model
ENDSTRUCT
//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY		      	0
CONST_INT				STAGE_EXIT		      	-1	 
CONST_INT				TRAIN_CONFIG		  	19		
//-----starting positions-------
VECTOR					V_TRAIN_START 			= << 2574.5146, 2173.4243, 31.5596 >>

//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------

VEHICLE_STRUCT					Vehs[MVF_NUM_OF_VEH]		//holds all of the vehicles for tis level
PEDS_STRUCT						peds[MPF_NUM_OF_PEDS]		//holds all of the peds for this level
OBJ_STRUCT						objs[OBJ_NUM_OF_OBJECTS]	//holds all objects

structPedsForConversation 		convo_struct				//holds peds in the conversation
LOCATES_HEADER_DATA				sLocatesData
//SELECTOR_PED_STRUCT				sSelectorPeds
BLIP_INDEX						blip_objective				//blip for mission objective
Camera_index					cameraIndex					//in game cut scene camera
SEQUENCE_INDEX					seq							//used to create AI sequence
CHASE_HINT_CAM_STRUCT			sHintCam

//DECAL_ID decalBlood[1]
//PED_INDEX 						ladderMike

WEAPON_TYPE playerWeapon

// relationship
REL_GROUP_HASH				REL_Michael

Int							SyncSceneIG1
Int							SyncSceneIG2
Int							SyncSceneIG3
Int							SyncSceneIG5

int 						icam = 0

bool				bcreateScene = false
bool				bloadedScene = true
bool				ringToneStarted
bool				ringToneStopped

bool bFrankFacingMike
bool cutsceneRequested
bool playerInBus
bool bPushInFXPlayed

//============================== time of day variables ============================== 
structTimelapse sTimelapse
bool		bTODstart = false
//------general variables-------
int 			i		
bool			bfailchecks 		= false
bool 			bOnFoot				= false
bool 			bCarRunning 		= false
//========================== Cam ================================
Int 			icamDelay	
//========================== TRAIN STAGE ==========================
FLOAT					train_speed_perc 								// train is 63.35% speed of mike normaly
FLOAT					fplaybackSpeed									//used to increase or decrease vehicle recording
FLOAT 					fDist 
FLOAT					fcurrentplaybackTime 	= 0.0					//used to assign current playback time
INT						iRubberTimer 			= 0						//used for debug on the rubber band sequence
//========================== HELI STAGE ==========================
//helilight  				eHeliLight
//HELIPOS	 				EheliPos
//INT						iHeliDelay
//int						IheliStage
//int 					iHeliDialogueStage
//int 					iHeliTimerDialogue
//bool					bHeliDes				= false

//========================= LADDER STAGE ========================
Int				iBulletTimer 
int 			iLadderStage
//========================== Dialogue and prints ============================
int 	iAmbDialogueTimer
bool	bFollowDisplayed 	= false
bool 	doneUPSTChat
//------------------------------ CS ------------------------------
bool			bcutsceneLoaded = false
bool			bcs_frank		= false
bool			bcs_mike		= false
bool 			bcs_mikeCar		= false
//bool 			bcs_cam			= false
bool			bcs_gun
bool 			bPlaceholder	= false
bool 			endCutRequested	= false
bool 			scriptCamsStopped = false
bool			bmusicTriggerStart	= false
bool 			syncSceneStarted = false
BOOl 			bloodAdded
bool 			doneChatHold
bool 			creditsStarted
bool 			pain1Done		
bool			pain2Done		
bool			pain3Done
bool			damagePack1Done
bool			damagePack2Done


bool			bPunch1 = false
bool 			bPunch2 = false
bool 			bVideoCaptured
//bool 			bMikeMoved = FALSE

MODEL_NAMES eModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_sr_sniperrifle"))

object_index 	Weapon_Object
camera_index 	EndCreditCam

//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
CUTSCENE_PED_VARIATION 	sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]
//----------------

//----------- Stage Management -----------
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						Dialogue_stage			//stage to manage dialogue 
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
//------------- skip stuff ---------------
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
bool					bis_zSkip		//zskip used

#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID widget_debug
	vector 				vDebugPosition = <<2727.982,1576.869,65.250>>
	float				fDebugHeading = 179.990
#endif

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------

// PC CONTROLS FOR SAVE/DROP MICHAEL
BOOL bPCCOntrolsSetup = FALSE

PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = FALSE
			INIT_PC_SCRIPTED_CONTROLS("FINALE B CHOICE")
			bPCCOntrolsSetup = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF bPCCOntrolsSetup = TRUE
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCCOntrolsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC



PROC Point_Gameplay_cam_at_heading(float targetHeading)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(targetHeading - GET_ENTITY_HEADING(Player_ped_id()))
endproc
PROC Point_Gameplay_cam_at_coord(vector target)	
    float aim_heading
	
  	aim_heading = GET_HEADING_BETWEEN_VECTORS_2D(get_entity_coords(player_ped_id()),target)  		
	SET_GAMEPLAY_CAM_RELATIVE_HEADING( aim_heading - GET_ENTITY_HEADING(Player_ped_id()))
endproc
func bool isentityalive(entity_index mentity)
  	if does_entity_exist(mentity)
		if is_entity_a_vehicle(mentity)
			if is_vehicle_driveable(get_vehicle_index_from_entity_index(mentity))
				return true 
			endif
		elif is_entity_a_ped(mentity)
			if not is_ped_injured(get_ped_index_from_entity_index(mentity))
	          	return true 
	    	endif	
		endif           
	endif
      return false
endfunc
FUNC FLOAT GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=true)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC
FUNC FLOAT GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(ped_index miss_ped)

      VECTOR vec_ba
      FLOAT aim_heading

      vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())
      
      //x rotation
      aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(player_ped_id()))
      
      IF aim_heading > 180
            aim_heading -= 360
      ENDIF
      IF aim_heading < -180
            aim_heading += 360
      ENDIF

      RETURN aim_heading
      
ENDFUNC 

FUNC FLOAT GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(ped_index miss_ped)

      VECTOR vec_ba
      VECTOR direction_vec
      
      vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())
      
      direction_vec = normalise_vector(vec_ba)

      RETURN (ATAN2(direction_vec.z, VMAG(<<direction_vec.x, direction_vec.y, 0.0>>)))

ENDFUNC

FUNC ped_index FRANK_ID()
	return peds[mpf_frank].id
ENDFUNC
FUNC ped_index MIKE_ID()
	return peds[mpf_mike].id
ENDFUNC
proc stop_all_audio_scenes(int exclude = 0)
	
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_IN_CAR")
	and exclude != 1
		STOP_AUDIO_SCENE("FIN_2_CHASE_IN_CAR")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_FOCUS_CAM")
	and exclude != 2
		STOP_AUDIO_SCENE("FIN_2_FOCUS_CAM")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_MICHAEL_ESCAPE_SCENE")
	and exclude != 3
		STOP_AUDIO_SCENE("FIN_2_MICHAEL_ESCAPE_SCENE")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_ON_FOOT")
	and exclude != 4
		STOP_AUDIO_SCENE("FIN_2_CHASE_ON_FOOT")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_HELICOPTER_ARRIVES")
	and exclude != 5
		STOP_AUDIO_SCENE("FIN_2_HELICOPTER_ARRIVES")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_MICHAEL_CLIMBS_LADDER")
	and exclude != 6
		STOP_AUDIO_SCENE("FIN_2_MICHAEL_CLIMBS_LADDER")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_UP_LADDER")
	and exclude != 7
		STOP_AUDIO_SCENE("FIN_2_CHASE_UP_LADDER")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_MAKE_CHOICE")
	and exclude != 8
		STOP_AUDIO_SCENE("FIN_2_MAKE_CHOICE")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_SAVE_MICHAEL")
	and exclude != 9
		STOP_AUDIO_SCENE("FIN_2_SAVE_MICHAEL")
	endif
	if IS_AUDIO_SCENE_ACTIVE("FIN_2_KILL_MICHAEL")
	and exclude != 10
		STOP_AUDIO_SCENE("FIN_2_KILL_MICHAEL")
	endif
endproc
PROC give_weapons()	
	if DOES_ENTITY_EXIST(MIKE_ID())
		if not HAS_PED_GOT_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL)
			GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)			
		else
			SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
		endif
	endif
ENDPROC
Proc manage_shooting_from_above()
	if DOES_ENTITY_EXIST(FRANK_ID())
		
		if IS_ENTITY_AT_COORD(FRANK_ID(),<<2735.73, 1576.83, 58.73>>,<<4,4,10>>)
		and IS_PLAYER_CLIMBING(player_id())
			
			vector pos = GET_ENTITY_COORDS(FRANK_ID())
			vector shootpos = <<2735.8,1576.5,66.4>>
			if IS_ENTITY_PLAYING_ANIM(FRANK_ID(),"missfinale_b_ig_4","LHUP_dodgeleft_short")
			or IS_ENTITY_PLAYING_ANIM(FRANK_ID(),"missfinale_b_ig_4","LHUP_dodgeleft_long")
			or IS_ENTITY_PLAYING_ANIM(FRANK_ID(),"missfinale_b_ig_4","LHUP_dodgeright_short")
			or IS_ENTITY_PLAYING_ANIM(FRANK_ID(),"missfinale_b_ig_4","LHUP_dodgeright_long")			
				SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
			elif iLadderStage > 1
			OR pos.z < 49.4
				SET_PLAYER_CONTROL(player_id(),true)				
			endif
			
			switch iLadderStage
				//first shooting
				case 0
					if IS_PLAYER_CLIMBING(player_id())		
					and IS_ENTITY_AT_COORD(FRANK_ID(),<<2735.78296, 1576.85803, 51.27797>>,<<0.5,0.5,3>>)
						SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2736.00537, 1576.75659, 52.01969>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2736.00537, 1576.75659, 52.01969>>)
						iBulletTimer = GET_GAME_TIMER()
						iLadderStage++
					endif
				break
				case 1
					if get_game_timer() - iBulletTimer	> 1000					
						TASK_PLAY_ANIM(FRANK_ID(),"missfinale_b_ig_4","LHUP_dodgeleft_short",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_SECONDARY)
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2735.85205, 1576.79797, 52.27501>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2735.85205, 1576.79797, 52.27501>>)
						
						iBulletTimer = GET_GAME_TIMER()
						iLadderStage++
					endif
				break				
				//2nd shooting
				case 2
					if pos.z >= 56.5
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2736.04224, 1576.74500, 58.29689>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2736.04224, 1576.74500, 58.29689>>)
						iBulletTimer = GET_GAME_TIMER()
						iLadderStage++
					endif
				break
				case 3
					if get_game_timer() - iBulletTimer	> 450		
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2735.01880, 1576.11804, 57.91095>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2735.01880, 1576.11804, 57.91095>>)
						iBulletTimer = GET_GAME_TIMER()
						iLadderStage++
					endif
				break
				//3rd shooting
				case 4
					if pos.z >= 59
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2734.81, 1575.69, 60.61>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2734.81, 1575.69, 60.61>>)
						iBulletTimer = GET_GAME_TIMER()
						iLadderStage++
					endif
				break				
				case 5
					if get_game_timer() - iBulletTimer	> 400
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2736.03, 1576.75, 60.58>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2736.03, 1576.75, 60.58>>)
						iBulletTimer = GET_GAME_TIMER()
						iLadderStage++
					endif
				break
				case 6
					if get_game_timer() - iBulletTimer	> 400
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(shootpos,<<2736.51, 1576.33, 49.54>>,10,true,WEAPONTYPE_HEAVYSNIPER)
						SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2736.51, 1576.33, 49.54>>)
						if isentityalive(MIKE_ID())							
							FREEZE_ENTITY_POSITION(MIKE_ID(),false)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(MIKE_ID(),<<2729.18, 1578.21, 65.54>>,FRANK_ID(),PEDMOVE_SPRINT,true,2)
						endif
						iLadderStage++
					endif
				break
				case 7
					if pos.z >= 59
						if isentityalive(MIKE_ID())							
							FREEZE_ENTITY_POSITION(MIKE_ID(),false)
							if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_ID(),SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(MIKE_ID(),<<2729.18, 1578.21, 65.54>>,FRANK_ID(),PEDMOVE_SPRINT,true,2)
							endif
						endif
					endif
				break
			endswitch
		endif
	endif
endproc

RAGDOLL_BLOCKING_FLAGS eMikeRBFs = RBF_PED_RAGDOLL_BUMP | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_MELEE | RBF_BULLET_IMPACT

func bool Create_michael(vector pos, float heading)
	if isentityalive(MIKE_ID())
		SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_TORSO,22,1)
		SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_LEG,26,0)
		SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_feet,4,0)
		SET_ENTITY_COORDS(MIKE_ID(),pos)
		SET_ENTITY_HEADING(MIKE_ID(),heading)
		SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_ID(),REL_Michael)
		ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL",true)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)	
		GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
		SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_REQUIRES_LOS_TO_SHOOT,false)
		SET_PED_ACCURACY(MIKE_ID(),10)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DisableExplosionReactions,true)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_RunFromFiresAndExplosions,false)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromFire,true)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromExplosions,true)
		SET_PED_PATH_AVOID_FIRE(MIKE_ID(),FALSE) 
		SET_ENTITY_PROOFS(MIKE_ID(),false,true,true,false,false)
		return true
	else
		if CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id,CHAR_MICHAEL,pos,heading)
			SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_TORSO,22,1)
			SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_LEG,26,0)
			SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_feet,4,0)
			SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_ID(),REL_Michael)
			ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL",true)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)	
			GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
			SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_REQUIRES_LOS_TO_SHOOT,false)
			SET_PED_ACCURACY(MIKE_ID(),10)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DisableExplosionReactions,true)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_RunFromFiresAndExplosions,false)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromFire,true)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromExplosions,true)
			SET_PED_PATH_AVOID_FIRE(MIKE_ID(),FALSE) 
			SET_ENTITY_PROOFS(MIKE_ID(),false,true,true,false,false)
			SET_RAGDOLL_BLOCKING_FLAGS(MIKE_ID(),eMikeRBFs )
			return true
		endif
	endif
return false
ENDFUNC
proc clear_players_task_on_control_input(script_task_name task_name)

	if get_script_task_status(player_ped_id(), task_name) = performing_task
			
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28
	
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

		IF NOT IS_LOOK_INVERTED()
			right_stick_y *= -1
		ENDIF
		
		// invert the vertical
		IF (left_stick_y > STICK_DEAD_ZONE)
		OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
		or (left_stick_x > STICK_DEAD_ZONE)
		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
		//or is_control_pressed(player_control, input_sprint) 
		
			clear_ped_tasks(player_ped_id())
			
		endif 		
	endif 

endproc 
PROC DO_EXCITING_NEAR_BULLET_MISS_ON_PED(PED_INDEX pedToMiss, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, 
                                           INT timeBetweenBullets = 60, FLOAT minXrange = -3.9, FLOAT maxXrange = -1.0, FLOAT minYRange = -2.9, FLOAT maxYrange = 3.9,
										   weapon_type weapon = WEAPONTYPE_SNIPERRIFLE)
    //Fire bullets at the player as from the bad  guy...    
    VECTOR bulletHit
    VECTOR bulletOrigin
    
    IF ((GET_GAME_TIMER() - iControlTimer) > timeBetweenBullets)
        IF NOT IS_ENTITY_DEAD(pedToMiss)
        AND NOT IS_ENTITY_DEAD(sourceOfBullets)            
            bulletHit = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedToMiss, <<GET_RANDOM_FLOAT_IN_RANGE(minXrange, maxXrange), GET_RANDOM_FLOAT_IN_RANGE(minYRange, maxYrange), 0.0>>)
            bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
            GET_GROUND_Z_FOR_3D_COORD(bulletHit, bulletHit.z)           
            SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, bulletHit, 1,false,weapon)
            iControlTimer = GET_GAME_TIMER()
      	ENDIF
     ENDIF
ENDPROC
//PURPOSE: Returns the world heading of the gameplay camera.
FUNC FLOAT GET_GAMEPLAY_CAM_WORLD_HEADING()
	RETURN WRAP(GET_GAMEPLAY_CAM_RELATIVE_HEADING() + GET_ENTITY_HEADING(PLAYER_PED_ID()),-180,180)
ENDFUNC
PROC CONVERGE_VALUE(FLOAT &val, FLOAT fDesiredVal, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF val != fDesiredVal
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = fConvergeAmountThisFrame +@ fAmountToConverge
		ENDIF
	
		IF val - fDesiredVal > fConvergeAmountThisFrame
			val -= fConvergeAmountThisFrame
		ELIF val - fDesiredVal < -fConvergeAmountThisFrame
			val += fConvergeAmountThisFrame
		ELSE
			val = fDesiredVal
		ENDIF
	ENDIF
ENDPROC
PROC Rubber_banding(float &fcurrentspeed,ped_index &playerped,Vehicle_index &veh_target)
		
	if isentityalive(playerped)
	AND isentityalive(veh_target)
		
		vector vpos_Franklin 	= GET_ENTITY_COORDS(playerped)
		vector vpos_Target 		= GET_ENTITY_COORDS(veh_target)
		
		//Rubber banding
		fDist = VDIST(vpos_Franklin, vpos_Target)
		FLOAT fTouchingDist = 4 
		FLOAT fIdealDist = 10.0 
		FLOAT fMaxDist = 50.0 
		FLOAT fSuperMaxDist = 100.0 
		FLOAT fDesiredPlaybackSpeed = 1.0
				
		if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_target)
			fcurrentplaybackTime = GET_TIME_POSITION_IN_RECORDING(veh_target)
			//VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh_target, vpos_Franklin)
			
				//At start keep them closer together
			IF fCurrentPlaybackTime < 5000 
				fIdealDist = 20.0
				fMaxDist = 50.0
				fSuperMaxDist = 120.0
			ENDIF
			IF fCurrentPlaybackTime > 5000 and fcurrentplaybackTime < 15000
				fIdealDist = 25.0
				fMaxDist = 50.0
				fSuperMaxDist = 200.0
			ENDIF
			IF fCurrentPlaybackTime > 15000 and fcurrentplaybackTime < 21000
				fIdealDist = 40.0
				fMaxDist = 60.0
				fSuperMaxDist = 250.0
			ENDIF 
			if fCurrentPlaybackTime > 21000 and fcurrentplaybackTime < 25000	
				fidealDist = 60
				fmaxDist = 80
				fSuperMaxDist = 250
			endif
			if fCurrentPlaybackTime > 25000	and fcurrentplaybackTime < 30000	
				fidealDist = 70
				fmaxDist = 250
				fSuperMaxDist = 300
			endif
			if fCurrentPlaybackTime > 30000	and fcurrentplaybackTime < 35000	
				fidealDist = 85
				fmaxDist = 300
				fSuperMaxDist = 300
			endif
			if fCurrentPlaybackTime > 35000	
				fidealDist = 115
				fmaxDist = 350
				fSuperMaxDist = 350
			endif
			
				
		FLOAT fDistRatio = 0.0
		VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh_target, vpos_Franklin)
		IF vOffset.y < 1.0 
			//Player is behind
			IF fDist > fMaxDist 
				//Player is a long way behind, slow the trigger car down by a lot (if not on screen).
				BOOL bInsanelyFarBehind = FALSE
				IF fDist > fSuperMaxDist
					IF fDist > 200.0
						bInsanelyFarBehind = TRUE
					ENDIF
					fDist = fSuperMaxDist
				ENDIF
				
				fDistRatio = ((fDist - fMaxDist) / (fSuperMaxDist - fMaxDist))
				
				IF IS_ENTITY_ON_SCREEN(veh_target) AND NOT bInsanelyFarBehind
					fDesiredPlaybackSpeed = 0.7 - (0.2 * fDistRatio)
				ELSE
					fDesiredPlaybackSpeed = 0.5 - (0.3 * fDistRatio)
				ENDIF
			ELIF fDist > fIdealDist
				//Player is somewhat behind, slow the trigger car down relative to how far behind the player is.
				fDistRatio = ((fDist - fIdealDist) / (fMaxDist - fIdealDist))
				fDesiredPlaybackSpeed = 1.0 - (0.3 * fDistRatio)
			ELSE
				//Player is getting too close, speed up the trigger car relative to how close the player is.
				IF fDist < fTouchingDist
					fDist = fTouchingDist
				ENDIF
				
				fDistRatio = (fIdealDist - fDist) / (fIdealDist - fTouchingDist)
				
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					fDesiredPlaybackSpeed = 1.0 + (fDistRatio * 2.0)
				ELSE
					fDesiredPlaybackSpeed = 1.0 + (fDistRatio)
				ENDIF
			ENDIF		
		ELSE 
			//Player is in front, speed up trigger car by a lot.
			IF fDist > 20
				fDist = 20
			ENDIF
			fDistRatio = fDist / 20			
			
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				fDesiredPlaybackSpeed = 2.25 + fDistRatio
			ELSE
				fDesiredPlaybackSpeed = 1.0 + fDistRatio
			ENDIF			
		ENDIF
			
			if (GET_GAME_TIMER() - irubberTimer) > 500
				irubberTimer = GET_GAME_TIMER()
			ENDIF
					
				CONVERGE_VALUE(fCurrentSpeed, fDesiredPlaybackSpeed, 0.06,true)	
		endif
	endif
ENDPROC
PROC Manage_train_chase()
	
	VECTOR vtrainOffset 
	//If mike has past speed up 
	vtrainOffset= GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehs[mvf_train].id, GET_ENTITY_COORDS(vehs[mvf_mike_car].id))
	float CurrentTrainSpeed = GET_ENTITY_SPEED(vehs[mvf_train].id)
	float NewTrainSpeed
	
	if fcurrentplaybackTime <33500
		IF vtrainOffset.y < 1.5 	
			NewTrainSpeed = (GET_ENTITY_SPEED(MIKE_ID()) * train_speed_perc)//percentage of mikes speed for him to just make it thru the gap
			if NewTrainSpeed != 0
			and NewTrainSpeed <= GET_ENTITY_SPEED(MIKE_ID()) //can never be faster than mike's car 
				SET_TRAIN_SPEED(vehs[mvf_train].id,NewTrainSpeed)			
			endif
		else	
			NewTrainSpeed = GET_ENTITY_SPEED(vehs[mvf_mike_car].id)
			if NewTrainSpeed != 0
			and NewTrainSpeed <= GET_ENTITY_SPEED(MIKE_ID())
				CONVERGE_VALUE(CurrentTrainSpeed,NewTrainSpeed,0.8,true)
				SET_TRAIN_SPEED(vehs[mvf_train].id,CurrentTrainSpeed)
			endif
		endif	
	else		
		CurrentTrainSpeed = GET_ENTITY_SPEED(vehs[mvf_train].id)
		clamp(CurrentTrainSpeed,15,30)
		SET_TRAIN_CRUISE_SPEED(vehs[mvf_train].id,CurrentTrainSpeed)
	endif	
	fDist = VDIST(GET_ENTITY_COORDS(FRANK_ID()),GET_ENTITY_COORDS(MIKE_ID()))
		
endproc
Proc Create_police_in_veh(MPF_MISSION_PED_FLAGS ped,VEHICLE_INDEX veh,VEHICLE_SEAT seat = VS_DRIVER)	
	
	peds[ped].id = CREATE_PED_INSIDE_VEHICLE(veh,PEDTYPE_MISSION,S_M_Y_COP_01,seat)
	SET_PED_COMBAT_MOVEMENT(peds[ped].id,cm_defensive)
	SET_PED_SEEING_RANGE(peds[ped].id,200)
	SET_PED_COMBAT_ATTRIBUTES(peds[ped].id,CA_USE_COVER,true)
	SET_PED_COMBAT_ATTRIBUTES(peds[ped].id,CA_LEAVE_VEHICLES,true)
	SET_PED_SPHERE_DEFENSIVE_AREA(peds[ped].id,peds[ped].vdef,2)
	GIVE_WEAPON_TO_PED(peds[ped].id,WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
	SET_PED_ACCURACY(peds[ped].id,0)
	SET_PED_SHOOT_RATE(peds[ped].id,10)
	SET_PED_COMBAT_ABILITY(peds[ped].id,CAL_POOR)
	SET_PED_COMBAT_ATTRIBUTES(peds[ped].id,CA_DO_DRIVEBYS,false)
	
endproc
//proc manage_searchLight()
//	if IsEntityAlive(peds[mpf_pilot].id)
//	and isentityalive(vehs[mvf_heli].id)
//		SET_VEHICLE_SEARCHLIGHT(vehs[mvf_heli].id,true,true)
//		switch eHeliLight				
//			case HL_FRANK 					
//				IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id) 
//                	if CONTROL_MOUNTED_WEAPON(peds[mpf_pilot].id) 
//						SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,GET_ENTITY_COORDS(frank_ID())) 
//					endif
//                ELSE
//                	SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,GET_ENTITY_COORDS(frank_ID()))   					
//				endif
//			break
//			case HL_VEC1
//				IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id) 
//                	if CONTROL_MOUNTED_WEAPON(peds[mpf_pilot].id) 
//						SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,GET_ENTITY_COORDS(frank_ID()))  
//					endif
//				else
//					SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,<<2741.32, 1523.05, 45.25>>) 
//                ENDIF                  					
//			break
//			case HL_VEC2
//				IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id) 
//                	if CONTROL_MOUNTED_WEAPON(peds[mpf_pilot].id) 
//						SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,<<2736.12, 1537.14, 48.69 >>) 
//					endif
//                ELSE
//					SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,<<2736.12, 1537.14, 48.69 >>) 
//				ENDIF 
//			break
//			case HL_MIKE
//				if IsEntityAlive(MIKE_ID())	
//					IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id) 
//                    	if CONTROL_MOUNTED_WEAPON(peds[mpf_pilot].id) 
//							SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,GET_ENTITY_COORDS(MIKE_ID()))
//						endif
//                   	Else
//						SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,GET_ENTITY_COORDS(MIKE_ID()))
//					ENDIF                        	
//				else
//					eheliLight = HL_FRANK
//				endif					
//			break
//			case HL_DEAD_MIKE
//				IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id) 
//                	if CONTROL_MOUNTED_WEAPON(peds[mpf_pilot].id) 
//						SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,<<2727.42407, 1581.22046, 23.48895>>) 	
//					endif
//				else
//					SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id,NULL,NULL,<<2727.42407, 1581.22046, 23.48895>>) 
//                ENDIF 
//                 
//			break
//		endswitch	
//	endif
//endproc
//PROC Manage_HELI()	
//	if isentityalive(vehs[mvf_heli].id)	
//		if isentityalive(peds[mpf_pilot].id)
//			manage_searchLight()
//			switch EheliPos			
//				case eH_Corner1
//					switch IheliStage
//						case 0
//							CLEAR_PED_TASKS(peds[mpf_pilot].id)
//							IheliStage++
//						break
//						case 1					
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2795.0669, 1538.0674,52.3292 >>,MISSION_GOTO,30,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 2
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2795.0669, 1538.0674,52.3292>>,<<7,7,7>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2795.0669, 1538.0674,52.3292 >>,MISSION_GOTO,30,-1,82.4536,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 3
//							if get_game_timer() - iHeliDelay > 10000
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								IheliStage++
//							endif
//						break
//						case 4					
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2768.5940, 1491.1582, 54.8603>>,MISSION_GOTO,30,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 5
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2768.5940, 1491.1582, 54.8603>>,<<7,7,7>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2768.5940, 1491.1582, 54.8603>>,MISSION_GOTO,30,-1,22.1241,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 6
//							if get_game_timer() - iHeliDelay > 15000
//								IheliStage = 0
//							endif
//						break
//					endswitch
//				break
//				case eH_corner2
//					switch IheliStage
//						case 0
//							CLEAR_PED_TASKS(peds[mpf_pilot].id)
//							IheliStage++
//						break
//						case 1					
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2710.7583, 1490.5580, 63.0989>>,MISSION_GOTO,30,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 2
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2710.7583, 1490.5580, 63.0989>>,<<7,7,7>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2710.7583, 1490.5580, 63.0989>>,MISSION_GOTO,30,-1,326.1907,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 3
//							if get_game_timer() - iHeliDelay > 10000
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								IheliStage++
//							endif
//						break
//						case 4						
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2696.1963, 1524.3916,53.3657 >>,MISSION_GOTO,30,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 5
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2696.1963, 1524.3916,53.3657 >>,<<4,4,4>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2696.1963, 1524.3916,53.3657 >>,MISSION_GOTO,30,-1,278.1787,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 6
//							if get_game_timer() - iHeliDelay > 10000
//								IheliStage = 0
//							endif
//						break
//					endswitch	
//				break
//				case eH_corner3
//					switch IheliStage
//						case 0
//							CLEAR_PED_TASKS(peds[mpf_pilot].id)
//							IheliStage++
//						break
//						case 1				
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2706.0330, 1577.4050,55.0532 >>,MISSION_GOTO,30,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 2
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2706.0330, 1577.4050,55.0532 >>,<<7,7,7>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2706.0330, 1577.4050,55.0532 >>,MISSION_GOTO,30,-1, 212.8791,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 3
//							if get_game_timer() - iHeliDelay > 10000
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								IheliStage++
//							endif
//						break
//						case 4					
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2710.6626, 1552.7446,42.2383 >>,MISSION_GOTO,30,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 5
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2710.6626, 1552.7446,42.2383 >>,<<7,7,7>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2710.6626, 1552.7446,42.2383 >>,MISSION_GOTO,30,-1,306.6938,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 6
//							if get_game_timer() - iHeliDelay > 10000
//								IheliStage = 0
//							endif
//						break
//					endswitch
//				break
//				case eH_corner4
//					switch IheliStage
//						case 0
//							CLEAR_PED_TASKS(peds[mpf_pilot].id)
//							IheliStage++
//						break
//						case 1					
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2684.6001, 1562.7174,64.3599 >>,MISSION_GOTO,20,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 2
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<<2684.6001, 1562.7174,64.3599 >>,<<4,4,4>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2684.6001, 1562.7174,64.3599 >>,MISSION_GOTO,20,-1,281.8232,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 3
//							if get_game_timer() - iHeliDelay > 10000
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								IheliStage++
//							endif
//						break
//						case 4						
//							if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//								TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2724.9990, 1622.4291,64.4212 >>, MISSION_GOTO,20,-1,-1,-1,-1)
//								IheliStage++
//							endif
//						break
//						case 5
//							if IS_ENTITY_AT_COORD(peds[mpf_pilot].id,<< 2724.9990, 1622.4291,64.4212 >> ,<<4,4,4>>)
//								CLEAR_PED_TASKS(peds[mpf_pilot].id)
//								if GET_SCRIPT_TASK_STATUS(peds[mpf_pilot].id,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//									TASK_HELI_MISSION(peds[mpf_pilot].id,vehs[mvf_heli].id,null,null,<<2724.9990, 1622.4291,64.4212 >>, MISSION_GOTO,20,-1,181.5375,-1,-1)
//									iheliDelay = get_Game_timer()
//									IheliStage++
//								endif
//							endif 
//						break
//						case 6
//							if get_game_timer() - iHeliDelay > 10000
//								IheliStage = 0
//							endif
//						break
//					endswitch
//				break
//			endswitch	
//		endif	
//	else
//		if mission_stage >= enum_to_int(msf_4_heli_appears)
//		and mission_substage > STAGE_ENTRY
//			bHeliDes = true
//			iHeliDelay = get_game_timer()
//		endif
//	endif
//	
//	if bHeliDes
//	and get_game_timer() - iHeliDelay > 30000
//		println("HELI DESTROYED")
//		if GET_PLAYER_WANTED_LEVEL(player_id()) < 2
//		and mission_stage < enum_to_int(msf_7_over_the_Edge) 
//			SET_PLAYER_WANTED_LEVEL(player_id(),2)		
//			SET_PLAYER_WANTED_LEVEL_NOW(player_id())
//		endif
//		bHeliDes = false
//		vector 	vHeliStart			
//		float 	fplayerheading = GET_GAMEPLAY_CAM_WORLD_HEADING()
//		if fplayerheading > 315  	// 0 degrees
//		or fplayerheading < 45
//			vHeliStart = << 2759.7231, 1215.3116, 65.5431 >>				
//		elif fplayerheading > 225 	// 270 degrees
//		and fplayerheading < 315
//			vHeliStart = << 2361.2295, 1548.8776,96.4794 >>
//		elif fplayerheading > 135 	// 180 degrees
//		and fplayerheading < 225
//			vHeliStart = << 2726.1267, 1707.1282, 61.8398 >>
//		else 						// 90 degrees
//			vHeliStart = << 3089.7339, 1441.9844,92.4943 >>
//		endif
//		vehs[mvf_heli].id = CREATE_VEHICLE(polmav,vHeliStart,fplayerheading)
//		SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)			
//		if isentityalive(peds[mpf_pilot].id)
//			SAFE_DELETE_PED(peds[mpf_pilot].id)
//		endif
//		peds[mpf_pilot].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli].id,PEDTYPE_MISSION,S_M_M_PILOT_02)
//		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_pilot].id,true)
//		ADD_PED_FOR_DIALOGUE(convo_struct,2,peds[mpf_pilot].id,"FIN2POLICE")		
//		FREEZE_ENTITY_POSITION(vehs[mvf_heli].id,false)			
//		if mission_stage = enum_to_int(msf_4_heli_appears)			
//			EheliPos 	= eH_Corner1				
//		elif mission_stage = enum_to_int(msf_5_shoot_out)			
//			EheliPos 	= eH_Corner2
//		elif mission_stage = enum_to_int(msf_6_Ladder_dodge)				
//			EheliPos 	= eH_Corner3
//		elif mission_stage = enum_to_int(msf_7_over_the_Edge)				
//			EheliPos 	= eH_Corner4
//		endif	
//		IheliStage = 0
//	endif
//ENDPROC
//PROC HELI_DIALOGUE()
////manage dialouge over time
//	if mission_stage >= enum_to_int(msf_5_shoot_out)
//	and not IS_CUTSCENE_PLAYING()
//		switch iHeliDialogueStage
//			case 0	
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)				
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 45000
//				and get_game_timer() - iHeliDelay > 10000
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAA","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)					
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++
//				endif
//			break
//			case 1
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)	
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 25000
//				and get_game_timer() - iHeliDelay > 10000
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAB","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)	
//					request_model(police3)
//					request_model(S_M_Y_Cop_01)
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++
//				endif
//			break
//			case 2
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 25000
//				and get_game_timer() - iHeliDelay > 10000
//				and	HAS_MODEL_LOADED(POLICE3)	
//				and HAS_MODEL_LOADED(S_M_Y_Cop_01)
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAC","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)
//					if mission_stage < enum_to_int(msf_7_over_the_Edge)
//						if GET_PLAYER_WANTED_LEVEL(player_id()) < 1					
//							SET_PLAYER_WANTED_LEVEL(player_id(),1)
//							SET_PLAYER_WANTED_LEVEL_NOW(player_id())						
//						endif
//						
////=========================================  create fake cop cars turning up  ======================================================
//						if not DOES_ENTITY_EXIST(vehs[mvf_police1].id)
//							vehs[mvf_police1].id = CREATE_VEHICLE(police3,<< 2577.0737, 1640.7174, 27.5393 >>, 267.6345)						
//							
//							Create_police_in_veh(mpf_police1,vehs[mvf_police1].id)
//							
//							Create_police_in_veh(mpf_police2,vehs[mvf_police1].id,VS_FRONT_RIGHT)
//							
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_police1].id,<< 2693.3496, 1685.5349, 23.6556 >>,20,DRIVINGSTYLE_ACCURATE,police3,DRIVINGMODE_AVOIDCARS,3,10)
//								TASK_LEAVE_ANY_VEHICLE(null)
//								TASK_COMBAT_PED(null,FRANK_ID())
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(peds[mpf_police1].id,seq)
//							CLEAR_SEQUENCE_TASK(seq)
//								
//							TASK_COMBAT_PED(peds[mpf_police2].id,FRANK_ID())
//						endif
//						if not DOES_ENTITY_EXIST(vehs[mvf_police2].id)
//							vehs[mvf_police2].id = CREATE_VEHICLE(police3,<< 2561.9470, 1639.0753, 27.9980 >>, 268.9215)
//							Create_police_in_veh(mpf_police3,vehs[mvf_police2].id)
//							Create_police_in_veh(mpf_police4,vehs[mvf_police2].id,VS_FRONT_RIGHT)
//							
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_police2].id,<< 2689.6165, 1690.8751, 23.7080 >>,20,DRIVINGSTYLE_ACCURATE,police3,DRIVINGMODE_AVOIDCARS,3,10)
//								TASK_LEAVE_ANY_VEHICLE(null)
//								TASK_COMBAT_PED(null,FRANK_ID())
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(peds[mpf_police3].id,seq)
//							CLEAR_SEQUENCE_TASK(seq)
//							
//							TASK_COMBAT_PED(peds[mpf_police4].id,FRANK_ID())
//						endif
//					endif
////===================================================================================================================================					
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++					
//				endif
//			break
//			case 3
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 45000
//				and get_game_timer() - iHeliDelay > 10000
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAD","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++				
//				endif
//			break
//			case 4
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 25000
//				and get_game_timer() - iHeliDelay > 10000					
//					 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAE","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++
//				endif
//			break
//			case 5
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 35000
//				and get_game_timer() - iHeliDelay > 10000
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAF","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)
//					if GET_PLAYER_WANTED_LEVEL(player_id()) < 2
//					and mission_stage < enum_to_int(msf_7_over_the_Edge)
//						SET_PLAYER_WANTED_LEVEL(player_id(),2)
//						SET_PLAYER_WANTED_LEVEL_NOW(player_id())
//					endif
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++					
//				endif
//			break
//			case 6
//				if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_pilot].id)
//				and not IS_MESSAGE_BEING_DISPLAYED()
//				and get_game_Timer() - iHeliTimerDialogue > 45000
//				and get_game_timer() - iHeliDelay > 10000
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_pilot].id,"FIN2_AUAG","FIN2POLICE",SPEECH_PARAMS_FORCE_HELI)
//					if GET_PLAYER_WANTED_LEVEL(player_id()) < 2
//					and mission_stage < enum_to_int(msf_7_over_the_Edge)
//						SET_PLAYER_WANTED_LEVEL(player_id(),2)
//						SET_PLAYER_WANTED_LEVEL_NOW(player_id())
//					endif
//					iHeliTimerDialogue = get_game_timer()
//					iHeliDialogueStage++					
//				endif
//			break
//		endswitch
//	endif
//ENDPROC
Proc Manage_DIALOGUE(MSF_MISSION_STAGE_FLAGS mission_Stage_di)
	switch  mission_stage_di
		case msf_1_chase
			switch Dialogue_stage
				case 0
					if IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2458.07666, 2444.43091, 38>>,<<2452.16919, 2471.42334, 50>>,64)
						ADD_PED_FOR_DIALOGUE(convo_struct,1,FRANK_ID(),"FRANKLIN")
						if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_SHOUT",CONV_PRIORITY_MEDIUM)
							Dialogue_stage++
						endif						
					endif
					if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2611.6565, 2000.8644, 30.9074 >>,<<30,30,7>>)
						Dialogue_stage++
					endif
				break	
				case 1
					if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2611.6565, 2000.8644, 30.9074 >>,<<30,30,7>>)
						if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_CHSE2",CONV_PRIORITY_MEDIUM)
							Dialogue_stage++
						endif
					ENDIF	
					if IS_ENTITY_AT_COORD(FRANK_ID(),<<2609.14331, 1803.90552, 25.49314>>,<<13,45,5>>)
						Dialogue_stage++
					endif
				break
				case 2				
					if IS_ENTITY_AT_COORD(FRANK_ID(),<<2609.14331, 1803.90552, 25.49314>>,<<13,45,5>>)
					and IS_ENTITY_AT_COORD(vehs[mvf_train].id,<<2609.14331, 1803.90552, 25.49314>>,<<13,45,5>>)
						if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_TRAIN2",CONV_PRIORITY_MEDIUM)
							Dialogue_stage++
						endif
					endif
				break
			endswitch
		break
		case msf_2_power_plant
			switch Dialogue_stage
				case 0
					if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()						
					else
						Dialogue_stage++
					endif
				break
				case 1
					ADD_PED_FOR_DIALOGUE(convo_struct,1,FRANK_ID(),"FRANKLIN")
					if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_CALLS",CONV_PRIORITY_MEDIUM)
						Dialogue_stage++
					endif
				break	
				case 2
					if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2704.0027, 1593.4563, 31.9187 >>,<<2,2,2>>)
						if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							KILL_ANY_CONVERSATION()
						endif
						Dialogue_stage++
					else
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2725.5, 1590.1, 31.7>>) < 60
							if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								if get_Game_timer() - iAmbDialogueTimer > 8000
								and IS_SAFE_TO_DISPLAY_GODTEXT()
									CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
								endif
							else
								iAmbDialogueTimer = GET_GAME_TIMER()
							endif
						endif
					endif
				break
				case 3	
					if isentityalive(MIKE_ID())
						if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_COW",CONV_PRIORITY_MEDIUM)
								Dialogue_stage++
							endif
						endif
					endif
				break
				case 4
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if get_Game_timer() - iAmbDialogueTimer > 9000
						and IS_SAFE_TO_DISPLAY_GODTEXT()
							CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
						endif
					else
						iAmbDialogueTimer = GET_GAME_TIMER()
					endif
				break
			endswitch
		break
		case msf_3_confrontation
			switch Dialogue_stage
				case 0
					if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2766.9392, 1565.0616, 31.4983 >>,<<3.25,3.25,2.5>>)
						if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						endif
						ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL")
						Dialogue_stage++
					else
						if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if get_Game_timer() - iambdialogueTimer > 8000
							and IS_SAFE_TO_DISPLAY_GODTEXT()
								CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
							endif
						else
							iAmbDialogueTimer = GET_GAME_TIMER()
						endif
					endif
				break
				case 1	
					if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_SHOOT",CONV_PRIORITY_MEDIUM)
						Dialogue_stage++
					endif
				break
				case 2				
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if get_Game_timer() - iambdialogueTimer > 8000
						and IS_SAFE_TO_DISPLAY_GODTEXT()
							CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
						endif
					else
						iAmbDialogueTimer = GET_GAME_TIMER()
					endif
				break
			endswitch
		break
		case msf_4_heli_appears
			switch Dialogue_stage
				case 0			
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if get_Game_timer() - iambdialogueTimer > 8000
						and IS_SAFE_TO_DISPLAY_GODTEXT()
							CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
						endif
					else
						iAmbDialogueTimer = GET_GAME_TIMER()
					endif
				break
			endswitch
		break
		case msf_5_shoot_out
			switch Dialogue_stage
				case 0
					if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2729.5039, 1530.7422, 39.3167 >>,<<2.0,2.0,4.0>>)
					or IS_ENTITY_AT_COORD(FRANK_ID(),<< 2738.4851, 1532.3734, 39.7673 >>,<<2.0,2.0,4.0>>)
					or IS_ENTITY_AT_COORD(FRANK_ID(),<< 2754.6116, 1528.1786, 39.8887 >>,<<7.0,7.0,4.0>>)
					or is_Entity_at_coord(FRANK_ID(),<< 2741.3200, 1523.0500, 45.2500 >>,<<1.5,1.5,1.0>>)	
						if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						endif
						Dialogue_stage++
					else
						if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if get_Game_timer() - iambdialogueTimer > 8000
							and IS_SAFE_TO_DISPLAY_GODTEXT()
								CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
							endif
						else
							iAmbDialogueTimer = GET_GAME_TIMER()
						endif
					endif
				break
				case 1	
					if isentityalive(MIKE_ID())
						ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL")
						if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_CHAT",CONV_PRIORITY_MEDIUM)	
							Dialogue_stage++
						endif
					endif
				break
				case 2
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if get_Game_timer() - iambdialogueTimer > 8000
						and IS_SAFE_TO_DISPLAY_GODTEXT()
							CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
						endif
					else
						iAmbDialogueTimer = GET_GAME_TIMER()
					endif
				break
			endswitch
		break
		case msf_6_Ladder_dodge
			switch Dialogue_stage
				case 0
					if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()						
					else
						Dialogue_stage++
					endif
				break
				case 1	
					if CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_POWER",CONV_PRIORITY_MEDIUM)
						Dialogue_stage++
					endif
				break
				case 2
					if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2735.7371, 1576.6788, 64.9689 >>,<<1,1,1>>)				
						if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						endif
						Dialogue_stage++
					else
						if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if get_Game_timer() - iambdialogueTimer > 8000
							and IS_SAFE_TO_DISPLAY_GODTEXT()
								CREATE_CONVERSATION(convo_struct,"FIN2AUD","FIN2_AMB",CONV_PRIORITY_MEDIUM)								
							endif
						else
							iAmbDialogueTimer = GET_GAME_TIMER()
						endif
					endif
				break
			endswitch
		break
	endswitch
endproc
proc Display_Placeholder()
	if bPlaceholder		
		#if DEBUG_BUGSTAR
			Println("PLACEHOLDER")
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1,"STRING","PLACEHOLDER")
		#endif
	endif	
endproc
// purpose:off radar/ hud / player control
proc prep_start_cutscene(bool bplayercontrol,vector varea,bool brender = true,bool binterp = false,bool bclearprj = true,bool bclearFire = true,int interptime = default_interp_to_from_game,set_player_control_flags controlflag = 0,bool bphoneaway = true)
	display_radar(false)
	display_hud(false)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	clear_prints()
	clear_help()
	render_script_cams(brender,binterp,interptime)	
	if bclearprj
		clear_area_of_projectiles(varea,500)
		
	endif	
	if bclearFire
		STOP_FIRE_IN_RANGE(varea,200)
	endif
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),false)
	endif
	if bphoneaway
		hang_up_and_put_away_phone()
	endif
endproc
// purpose:on radar/ hud / player control
proc prep_stop_cutscene(bool bplayercontrol,bool brender = false,bool binterp = false,int iduration = default_interp_to_from_game, set_player_control_flags controlflag = 0,bool bCatchup = false)
	display_radar(true)
	display_hud(true)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	if bCatchup
		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
	else	
		render_script_cams(brender,binterp,iduration)	
	endif
	if not brender	
		destroy_all_cams()
	endif
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),true)
	endif
	bPlaceholder = false
endproc
proc create_dummy_obj(OBJ_STRUCT &obj)	
	obj.id = CREATE_OBJECT(PROP_GOLF_BALL,obj.pos)
	SET_ENTITY_VISIBLE(obj.id,false)	
	SET_ENTITY_COLLISION(obj.id,false)
	FREEZE_ENTITY_POSITION(obj.id,true)	
endproc
Proc SET_CS_OUTFITS_AND_EXITS(PED_INDEX Ignore1 = null)	
	//Michael
	if ignore1 != MIKE_ID()
		IF DOES_ENTITY_EXIST(MIKE_ID())
			IF NOT IS_PED_INJURED(MIKE_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("MICHAEL", MIKE_ID(), PLAYER_ZERO)
			ENDIF
		ELSE
			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_MICHAEL, "MICHAEL")
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_TORSO, 22,1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_LEG, 26,0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_HAND, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_FEET, 4,0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_DECL, 0, 0)	
		ENDIF
	endif	
	//Franklin
	if IsEntityAlive(FRANK_ID())
	and ignore1 != FRANK_ID()
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("FRANKLIN", PLAYER_PED_ID(), PLAYER_ONE)
	endif

endproc
/// PURPOSE: Checks if previous car is not franklins default place that.
///    Otherwise run CREATE_PLAYER_VEHICLE for franklins default car
/// PARAMS: pos, heading 
/// RETURNS: True once car has been placed *CALL EVERY FRAME*
///    
func bool Check_Franklins_Car(vector pos, float heading)
	//check 
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		IF (IS_THIS_MODEL_A_CAR(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()) 
		OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()) 
		OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()) 
		OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL()))
		AND GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() <> BUS
			PRINTSTRING("replay checkpoint vehicle is either a CAR, BIKE, BICYCLE or a QUADBIKE so is being created") PRINTNL()
			REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			while not HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
				PRINTSTRING("waiting for checkpoint vehicle model loading") PRINTNL()
				wait(0)
			endwhile
			vehs[mvf_Frank_car].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(pos,heading)
			return true
		ELSE
			if not CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id,char_Franklin,pos, heading,true,VEHICLE_TYPE_CAR)
				return false
			else
				return true
			endif
		ENDIF
	else
		if not CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id,char_Franklin,pos, heading,true,VEHICLE_TYPE_CAR)
			return false
		else
			return true
		endif
	endif	
endfunc
proc LOAD_asset_stage(MSF_MISSION_STAGE_FLAGS loadStage)
	
	SWITCH loadStage			
		CASE msf_0_meet						
		break
		
		CASE msf_1_chase			
			Load_Asset_Model(sAssetData,FREIGHT) 	//train engine
			Load_Asset_Model(sAssetData,TANKERCAR)	//carridge type	tankercar for version 19				
			Load_Asset_Recording(sAssetData,001,"FIN2") //car chase
		BREAK
		
		CASE msf_2_power_plant
			Load_Asset_Waypoint(sAssetData,"fin2_mike1")
			Load_Asset_AnimDict(sAssetData,"missfinale_b_ig_1")
		BREAK
		
		CASE msf_3_confrontation
			Load_Asset_AnimDict(sAssetData,"missfinale_b_ig_2")
		BREAK
		
		CASE msf_4_heli_appears
		break
		
		CASE msf_5_shoot_out	
			Load_Asset_AnimDict(sAssetData,"missfinale_b_ig_3")
		break
		
		CASE msf_6_Ladder_dodge
			Load_Asset_AnimDict(sAssetData,"missfinale_b_ig_4")
		break
		
		CASE msf_7_over_the_Edge
			Load_Asset_AnimDict(sAssetData,"missfinale_b_ig_5")
		break
		
		CASE msf_8_passed			
		break
		
	ENDSWITCH
endproc

PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH eStage
		CASE msf_0_meet				vCoord = <<2383.6033, 2612.2979, 46.1600 >>		fHeading = 0		BREAK
		CASE msf_1_chase			vCoord = <<2383.6033, 2612.2979, 46.1600 >>		fHeading = 0		BREAK
		CASE msf_2_power_plant		vCoord = <<2686.5000, 1613.4200, 23.6100 >>		fHeading = 0		BREAK
		CASE msf_3_confrontation	vCoord = <<2745.1167, 1584.7615, 31.4980 >>		fHeading = 257.6353	BREAK	
		CASE msf_4_heli_appears		vCoord = <<2768.7725, 1564.3270, 36.6811 >>		fHeading = 340.9131	BREAK
		CASE msf_5_shoot_out		vCoord = <<2758.2583, 1541.8799, 39.3167 >>		fHeading = 160.8288 BREAK
		CASE msf_6_Ladder_dodge		vCoord = <<2738.9934, 1575.4868, 49.6975 >>		fHeading = 71.9890	BREAK
		CASE msf_7_over_the_Edge	vCoord = <<2738.9934, 1575.4868, 49.6975 >>		fHeading = 71.9890 	BREAK
		CASE msf_8_passed			vCoord = <<2677.2988, 1600.2403, 23.4956 >>		fHeading = -22.33 	BREAK
	ENDSWITCH

ENDPROC
//------------set ups-------------------------------
PROC SET_UP_GENERAL()
	//set peds relationship group
	if isentityalive(MIKE_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_ID(),REL_Michael)
		ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL",true)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)
		SET_PED_ACCURACY(MIKE_ID(),25)
	endif
		
	ADD_PED_FOR_DIALOGUE(convo_struct,1,FRANK_ID(),"FRANKLIN",true)//add peds to dialogue 
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FRANK_ID(),true)		//block temp task	
	
	//peds specials
	if DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
	and IS_VEHICLE_DRIVEABLE(vehs[mvf_mike_car].id)
		SET_ENTITY_INVINCIBLE(vehs[mvf_mike_car].id,true)
	endif
	
	//trains
	SET_RANDOM_TRAINS(false)
	
	//begin checking for fail
	bfailchecks = true
	//weapons
	give_weapons()
	
ENDPROC
PROC SET_UP_MSF_0_MEET()
	peds[mpf_frank].id = player_ped_id()
	if IS_REPLAY_IN_PROGRESS()
		SET_CLOCK_TIME(0,0,0)
		//michael's car		
		while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2378.1196, 2612.7002, 45.6341 >>,181.5441)
			wait(0)
		endwhile	
		//michael	
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_mike].id,CHAR_MICHAEL,vehs[mvf_mike_car].id)
			wait(0)
		endwhile
		//franklin	
		while not Check_Franklins_Car(<<2382.3335, 2610.1470, 45.5779>>, 187.5107)
			wait(0)
		endwhile
		
		//url:bugstar:2000004 - Setting michaels shit what not created using Create_Michael()
		IF NOT IS_PED_INJURED(MIKE_ID())
			SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_TORSO,22,1)
			SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_LEG,26,0)
			SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_feet,4,0)
			SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_ID(),REL_Michael)
			ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL",true)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)	
			GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
			SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_REQUIRES_LOS_TO_SHOOT,false)
			SET_PED_ACCURACY(MIKE_ID(),10)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DisableExplosionReactions,true)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_RunFromFiresAndExplosions,false)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromFire,true)
			SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromExplosions,true)
			SET_PED_PATH_AVOID_FIRE(MIKE_ID(),FALSE) 
			SET_ENTITY_PROOFS(MIKE_ID(),false,true,true,false,false)
		ENDIF
		
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP()
		Else
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
		ENDIF
	endif
ENDPROC
PROC SET_UP_MSF_1_CHASE()
	//Time of day 
	SET_CLOCK_TIME(0,0,0)
//michael's car
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2378.1196, 2612.7002, 45.6341 >>,181.5441)
		wait(0)
	endwhile	
//michael	
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_mike].id,CHAR_MICHAEL,vehs[mvf_mike_car].id)
		wait(0)
	endwhile
//franklin	
	while not Check_Franklins_Car(<<2382.3335, 2610.1470, 45.5779>>, 187.5107)
		wait(0)
	endwhile	
	
	//url:bugstar:2000004 - Setting michaels shit what not created using Create_Michael()
	IF NOT IS_PED_INJURED(MIKE_ID())
		SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_TORSO,22,1)
		SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_LEG,26,0)
		SET_PED_COMPONENT_VARIATION(MIKE_ID(),PED_COMP_feet,4,0)
		SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_ID(),REL_Michael)
		ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL",true)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)	
		GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
		SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_REQUIRES_LOS_TO_SHOOT,false)
		SET_PED_ACCURACY(MIKE_ID(),10)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DisableExplosionReactions,true)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_RunFromFiresAndExplosions,false)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromFire,true)
		SET_PED_CONFIG_FLAG(MIKE_ID(),PCF_DontActivateRagdollFromExplosions,true)
		SET_PED_PATH_AVOID_FIRE(MIKE_ID(),FALSE) 
		SET_ENTITY_PROOFS(MIKE_ID(),false,true,true,false,false)
	ENDIF
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_Frank_car].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)
		SET_PED_INTO_VEHICLE(FRANK_ID(),vehs[mvf_Frank_car].id)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehs[mvf_frank_car].id)
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_frank_car].id)
			SET_VEHICLE_LIGHTS(vehs[mvf_frank_car].id,FORCE_VEHICLE_LIGHTS_ON)
			SET_VEHICLE_LIGHT_MULTIPLIER(vehs[mvf_frank_car].id,2)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_frank_car].id)
			SET_VEHICLE_ENGINE_ON(vehs[mvf_Frank_car].id,true,true)	
		ENDIF
	ENDIF
	if IS_PED_ON_ANY_BIKE(player_ped_id())
		GIVE_PED_HELMET(PLAYER_PED_ID(),false)
	endif
	bCarRunning = true	
	
	IF DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_mike_car].id)
			vehs[mvf_mike_car].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_mike_car].id,true)
			SET_VEHICLE_LIGHTS(vehs[mvf_mike_car].id,FORCE_VEHICLE_LIGHTS_ON)	
		ENDIF
	ENDIF
	
	//train
	if not DOES_ENTITY_EXIST(vehs[mvf_train].id)
		vehs[mvf_train].id = CREATE_MISSION_TRAIN(TRAIN_CONFIG,V_TRAIN_START,true)	
	endif	
	TRIGGER_MUSIC_EVENT("FINB_RESTART_CHASE")
	clear_area(<< 2387.6033, 2551.2979, 46.2726 >>,500,false)
	print_now("FIN2_CHASE",DEFAULT_GOD_TEXT_TIME,1)
	
ENDPROC
PROC SET_UP_MSF_2_POWERPLANT()
	//time of day
	SET_CLOCK_TIME(22,30,0)
	//mike
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2682.6819, 1607.4335, 23.4947 >>, 66.2467)
		wait(0)
	endwhile
	while not Create_michael(<< 2675.8303, 1593.1636, 31.4980 >>,266.7406)
		 wait(0)
	endwhile	
	//franklin
	while not Check_Franklins_Car(<< 2689.9885, 1615.6116, 23.6365 >>, 146.5498)
		wait(0)
	endwhile
	
	SET_VEHICLE_DOOR_OPEN(vehs[mvf_frank_car].id,SC_DOOR_FRONT_LEFT)
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF
	
	TRIGGER_MUSIC_EVENT("FINB_RESTART_ARRIVE")
	clear_area(<< 2691.3738, 1641.8214, 24.2936 >>,500,false)
ENDPROC
PROC SET_UP_MSF_3_CONFRONTATION()

	//time of day
	SET_CLOCK_TIME(22,45,0)
	
	//franklin
	while not Check_Franklins_Car(<< 2686.7937, 1617.0187, 23.6144 >>, 97.9135)
		wait(0)
	endwhile	
	//mike
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2682.6819, 1607.4335, 23.4947 >>, 66.2467)
		wait(0)
	endwhile
	while not Create_michael(<< 2752.1804, 1561.7091, 39.3163 >>,0)
		 wait(0)
	endwhile
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
	//misc
	bOnFoot	= true
	TRIGGER_MUSIC_EVENT("FINB_RESTART_ARRIVE")
	clear_area(<< 2691.3738, 1641.8214, 24.2936  >>,500,false)	
	FORCE_PED_MOTION_STATE(FRANK_ID(),MS_ON_FOOT_SPRINT,TRUE,FAUS_CUTSCENE_EXIT)
	
ENDPROC
PROC SET_UP_MSF_4_HELI()
	//time of day
	SET_CLOCK_TIME(22,50,0)
	//franklin	
	while not Check_Franklins_Car(<< 2686.7937, 1617.0187, 23.6144 >>, 97.9135)
		wait(0)
	endwhile
	//mike	
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2682.6819, 1607.4335, 23.4947 >>, 66.2467)
		wait(0)
	endwhile
	while not Create_michael(<<2761.1631, 1549.1521, 39.3377>>,336.6246)
		 wait(0)
	endwhile
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
	//misc
	TRIGGER_MUSIC_EVENT("FINB_RESTART_STEPS")
	bOnFoot	= true
	clear_area(<< 2762.2683, 1555.8557, 39.3163 >>,500,false)	
	FORCE_PED_MOTION_STATE(FRANK_ID(),MS_ON_FOOT_SPRINT,TRUE,FAUS_CUTSCENE_EXIT)
	
ENDPROC
PROC SET_UP_MSF_5_SHOOT_OUT()
	//time of day
	SET_CLOCK_TIME(23,0,0)	
	while not Check_Franklins_Car(<< 2686.7937, 1617.0187, 23.6144 >>, 97.9135)
		wait(0)
	endwhile	
	//mike	
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2682.6819, 1607.4335, 23.4947 >>, 66.2467)
		wait(0)
	endwhile
	while not Create_michael(<< 2740.3176, 1521.1514, 44.4857 >>, 326.7082)
		 wait(0)
	endwhile
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
	//misc
	TRIGGER_MUSIC_EVENT("FINB_RESTART_STEPS")
	bOnFoot	= true
	clear_area(<< 2758.2583, 1541.8799, 39.3167 >>,500,false)	
	FORCE_PED_MOTION_STATE(FRANK_ID(),MS_ON_FOOT_SPRINT,TRUE,FAUS_CUTSCENE_EXIT)
	
ENDPROC
PROC SET_UP_MSF_6_LADDER_DODGE()
	//time of day
	SET_CLOCK_TIME(0,0,0)	
	while not Check_Franklins_Car(<< 2686.7937, 1617.0187, 23.6144 >>, 97.9135)
		wait(0)
	endwhile	
	//mike
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2682.6819, 1607.4335, 23.4947 >>, 66.2467)
		wait(0)
	endwhile
	while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id,CHAR_MICHAEL,<< 2734.5884, 1580.2417, 65.5234 >>, 326.7082)
		 wait(0)
	endwhile
	Create_michael(<< 2734.5884, 1580.2417, 65.5234 >>, 326.7082)
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
	//misc
	TRIGGER_MUSIC_EVENT("FINB_RESTART_CLIMB")
	bOnFoot	= true
	clear_area(<< 2738.9934, 1575.4868, 49.6975 >>,500,false)	
	
ENDPROC
PROC SET_UP_MSF_7_OVER_THE_EDGE()
	//time of day
	SET_CLOCK_TIME(0,0,0)
	while not Check_Franklins_Car(<< 2686.7937, 1617.0187, 23.6144 >>, 97.9135)
		wait(0)
	endwhile	
	//mike	
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id,CHAR_MICHAEL,<< 2682.6819, 1607.4335, 23.4947 >>, 66.2467)
		wait(0)
	endwhile
	while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id,CHAR_MICHAEL,<< 2735.6265, 1577.7052, 65.5224 >>, 326.7082 )
		 wait(0)
	endwhile
	Create_michael(<< 2734.5884, 1580.2417, 65.5234 >>, 326.7082)
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
	//misc
	bOnFoot	= true
	clear_area(<< 2738.9934, 1575.4868, 49.6975 >>,500,false)
	
ENDPROC
PROC su_passed()
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()
	TRIGGER_MUSIC_EVENT("FINB_FAIL")
	
	SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(0) 
	
	//For bug 1847954
	CLEAR_PICKUP_REWARD_TYPE_SUPPRESSION(PICKUP_REWARD_TYPE_WEAPON)
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()	
	ENDIF
	
//	IF IS_DECAL_ALIVE(decalBlood[0])
//		REMOVE_DECAL(decalBlood[0])
//	ENDIF	
	
	IF NOT IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	ENDIF	
	
	END_SRL()
//-------------------peds-----------------------
//all peds are no longer needed except the player ped
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if peds[i].id != player_ped_id()
				SAFE_RELEASE_PED(peds[i].id)
			endif
			if DOES_BLIP_EXIST(peds[i].blip)
				REMOVE_BLIP(peds[i].blip)
			endif
		endif
	endfor	
	
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
	endif
//---------------------vehs-----------------------

	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)		 
		 	SAFE_RELEASE_VEHICLE(vehs[i].id)
		endif
		if DOES_BLIP_EXIST(vehs[i].blip)
			REMOVE_BLIP(vehs[i].blip)
		endif
	endfor
//-------------------- objects -----------------------	
	SAFE_DELETE_OBJECT(Weapon_Object)
	for i = 0 to enum_to_int(OBJ_NUM_OF_OBJECTS)-1
		if DOES_ENTITY_EXIST(objs[i].id)
			SAFE_RELEASE_OBJECT(objs[i].id)
		endif		
	endfor
//-------------------Destroy-----------------------
	CLEAR_PRINTS()
	CLEAR_HELP()
	//Only clear up camera's if not mission passed.
	IF scriptCamsStopped = FALSE
		DESTROY_ALL_CAMS()
		DISPLAY_RADAR(true)
		DISPLAY_HUD(true)		
		Prep_stop_Cutscene(true)
		RENDER_SCRIPT_CAMS(false,false)		
	ENDIF	
		
	KILL_ANY_CONVERSATION()	
	if DOES_BLIP_EXIST(blip_objective)
		remove_blip(blip_objective)
	endif	
	
	KILL_CHASE_HINT_CAM(sHintCam) 	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
//--------------------Resets-----------------------

	//wanted level for mission reset to normal
	SET_WANTED_LEVEL_MULTIPLIER(1)
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PLAYER_WANTED_LEVEL(player_id())
	endif	
	
	SET_RANDOM_TRAINS(true)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, true)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, true)
	//SET_PED_DENSITY_MULTIPLIER(1)
	SET_PLAYER_PED_DATA_IN_CUTSCENES()
	DISABLE_TAXI_HAILING(false)	
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,false)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUFFALO,false)
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
		
	bfailchecks 		= false
	bFollowDisplayed 	= false
	bOnFoot				= false
	
	ASSISTED_MOVEMENT_REMOVE_ROUTE("finalB1sta")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("finaBroute1A")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("finalbroute2")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("finalbround")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("finalb1st")
	
	CLEAR_WEATHER_TYPE_PERSIST()
//-------------------------------------------------

	RELEASE_MISSION_AUDIO_BANK()
	
	CLEANUP_PC_CONTROLS()
 
	SET_PLAYER_CONTROL(player_id(),true)
	PRINTSTRING("...Finale B Mission Cleanup")
	PRINTNL()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Finale B Mission Passed")
	PRINTNL()	
	
	g_iFinaleCreditsToPlay = 1
	
	INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FINB_KILLMIC)
	PUT_DEAD_CHARACTERS_PROPERTIES_BACK_ON_MARKET(CHAR_MICHAEL) 
	AWARD_ACHIEVEMENT_FOR_MISSION(ACH04) // To live or die in los santos
	
	IF IS_REPEAT_PLAY_ACTIVE()
		g_MissionStatSystemSuppressVisual = TRUE
		TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
	ENDIF	
	
	Mission_Flow_Mission_Passed(true)
	IF IS_REPEAT_PLAY_ACTIVE()
		g_bMissionStatSystemBlocker = FALSE
	ENDIF
	Mission_Cleanup()
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed(MFF_MISSION_FAIL_FLAGS fail_condition = mff_default)
	TRIGGER_MUSIC_EVENT("FINB_FAIL")
	STRING strReason = ""
		
	//show fail message
	SWITCH fail_condition
//		CASE mff_debug_fail
//			strReason = "FIN2_FAILDB"
//		BREAK		
		case mff_lost_mike
			strReason = "FIN2_FAIL1"
		break
		case mff_police
			strReason = "FIN2_FAIL2"
		break
		DEFAULT
			strReason = "FIN2_FAILDF"
		BREAk
	ENDSWITCH
	
	bfailchecks = false
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		if IS_CUTSCENE_ACTIVE()
			SET_CUTSCENE_FADE_VALUES()	
		endif
    	WAIT(0)
    ENDWHILE
	
	MISSION_CLEANUP()
	TERMINATE_THIS_THREAD()
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC Mission_stage_management()
	SWITCH stageswitch
		CASE STAGESWITCH_REQUESTED
			#if IS_DEBUG_BUILD
				PRINTLN("[stageManagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedStage)
			#endif
			stageswitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			#if IS_DEBUG_BUILD
				PRINTLN("[StageManagement] Exiting mission_stage: ", mission_stage)
			#endif
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage 	= STAGE_ENTRY
			Dialogue_stage 		= STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			#if IS_DEBUG_BUILD	
				PRINTLN("[StageManagement] Entered mission_stage: ", mission_stage)
			#endif
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE		
			IF (GET_GAME_TIMER() - iStageTimer) > 1500
				#if IS_DEBUG_BUILD
					PRINTLN("[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage, " Dialogue stage: ",Dialogue_stage)
				#endif
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL Mission_Set_Stage(MSF_MISSION_STAGE_FLAGS newStage)
	If stageswitch = STAGESWITCH_IDLE
		requestedstage = ENUM_TO_INT(newStage)
		stageswitch = STAGESWITCH_REQUESTED		
		return True
	else
		return false
	endif
ENDFUNC
PROC RESET_EVERYTHING()
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
//-------------------peds-----------------------

	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif	
			if peds[i].id != player_ped_id()
				SAFE_DELETE_PED(peds[i].id)
			endif
		endif
	endfor	
//-------------------vehs-----------------------
	
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)		
			if vehs[i].id != vehs[mvf_train].id
		 		SAFE_DELETE_VEHICLE(vehs[i].id)
			endif
			if DOES_BLIP_EXIST(vehs[i].blip)
				REMOVE_BLIP(vehs[i].blip)
			endif
		endif
	endfor	
	if DOES_ENTITY_EXIST(vehs[mvf_train].id)
		SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(vehs[mvf_train].id)		
	endif
	
	SAFE_DELETE_OBJECT(Weapon_Object)
	
//-------------------Resets---------------------
	
	SET_WANTED_LEVEL_MULTIPLIER(0)
	CLEAR_PLAYER_WANTED_LEVEL(player_id())
	SET_POLICE_IGNORE_PLAYER(player_id(),false)
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	RENDER_SCRIPT_CAMS(false,false)
	
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
	endif	
	
	Prep_stop_Cutscene(true)
	bcutsceneLoaded		= false
	
	bfailchecks 		= false	
	bFollowDisplayed 	= false
	bOnFoot				= false
	bCarRunning 		= false
	
	bcs_frank		= false
	bcs_mike		= false
	bcs_mikeCar		= false
	bcs_gun			= false
	//bcs_cam		= false
	
	
//------------------Destroys--------------------
	
//	IF IS_DECAL_ALIVE(decalBlood[0])
//		REMOVE_DECAL(decalBlood[0])
//	ENDIF	
	CLEAR_PRINTS()
	CLEAR_HELP()
	DESTROY_ALL_CAMS()
	KILL_ANY_CONVERSATION()	
	SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
	KILL_CHASE_HINT_CAM(sHintCam)
	//blip
	if DOES_BLIP_EXIST(blip_objective)
		remove_blip(blip_objective)
	endif
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
	
	//objects
	for i = 0 to enum_to_int(OBJ_NUM_OF_OBJECTS)-1
		if DOES_ENTITY_EXIST(objs[i].id) 
			if not IS_ENTITY_A_MISSION_ENTITY(objs[i].id)
				DELETE_OBJECT(objs[i].id)
			else
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[i].id)
			endif		
		endif
	endfor
	
ENDPROC
PROC MISSION_STAGE_SKIP()
//a skip has been made
	IF bDoSkip = TRUE
		
		//begin the skip if the switching is idle
		if stageSwitch = STAGESWITCH_IDLE
			if not IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			else
				Mission_Set_Stage(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, iSkipToStage))
			endif
		//Needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
			
			RENDER_SCRIPT_CAMS(false,false)
			SET_PLAYER_CONTROL(player_id(), true)
			
			RESET_EVERYTHING()
			
			Start_Skip_Streaming(sAssetData)
			
			// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading)				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
			ENDIF
		// --------------------------------------------------------------------
							
			load_asset_stage(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage))
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE					
			
			SWITCH int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)			
				case msf_0_meet				set_up_msf_0_meet() 		break				
				case msf_1_chase			set_up_msf_1_chase()		break				
				case msf_2_power_plant		set_up_msf_2_powerplant()	break				
				case msf_3_confrontation	set_up_msf_3_confrontation()break				
				case msf_4_heli_appears 	set_up_msf_4_heli()			break				
				case msf_5_shoot_out		set_up_msf_5_shoot_out()	break				
				case msf_6_ladder_dodge		set_up_msf_6_ladder_dodge()	break				
				case msf_7_over_the_edge	set_up_msf_7_over_the_edge()break	
				case msf_8_passed			su_passed()					break
			ENDSWITCH
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			SET_UP_GENERAL()
			
			bDoSkip = FALSE
			IF NOT IS_REPLAY_BEING_SET_UP()	
				NEW_LOAD_SCENE_STOP()
			endif
		ENDIF
#if Is_debug_build
	//Check is a skip being asked for, dont allow skip during setup stage
	ELIF LAUNCH_MISSION_STAGE_MENU(zMenuNames, iSkipToStage, mission_stage, TRUE)
		IF iSkipToStage > ENUM_TO_INT(msf_7_over_the_Edge)
			MISSION_PASSED()
		ELSE
			iSkipToStage = CLAMP_INT(iSkipToStage, 0, ENUM_TO_INT(MSF_NUM_OF_STAGES)-1)
			if IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				bDoSkip = true
			endif
		endif
		bis_zSkip = true
#endif
	ENDIF
ENDPROC

PROC MISSION_CHECKS()

	//Cutscene outfits
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		SET_CS_OUTFITS_AND_EXITS()
		PRINTSTRING("REQUESTING CUTSCENE ASSETS NOW") PRINTNL()
	ENDIF

//------------------death checks-------------------------------

	for i = 0 to enum_to_int(MPF_NUM_OF_PEDS) -1
		if not IsEntityAlive(peds[i].id)
			if i != enum_to_int(mpf_dead_guy)
				SET_PED_AS_NO_LONGER_NEEDED(peds[i].id) 
			endif
		ENDIF
	ENDFOR
	for i = 0 to enum_to_int(MVF_NUM_OF_VEH) -1
		if not IsEntityAlive(vehs[i].id)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
		ENDIF
	ENDFOR
//-------------------multi stage checks------------------------	
	//Bug fix 1496887 
	ASSISTED_MOVEMENT_OVERRIDE_LOAD_DISTANCE_THIS_FRAME(20)

	//disable
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK2)		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(Player_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
	
	if  mission_stage >= ENUM_TO_INT(msf_2_power_plant)
	and bfailchecks
		if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(FRANK_ID(),<<2727.14795, 1557.29956, 23.50072>>) > 170
			Mission_Failed(mff_lost_mike)
		elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(FRANK_ID(),<<2727.14795, 1557.29956, 23.50072>>) > 150 		
			PRINT_NOW("FIN2_LOST",DEFAULT_GOD_TEXT_TIME,1)		
		endif
	endif
	
	//mikes health
	if isentityalive(MIKE_ID()) 
	and mission_stage < ENUM_TO_INT(msf_7_over_the_Edge)
		SET_ENTITY_INVINCIBLE(MIKE_ID(),true)
		SET_PED_RESET_FLAG(MIKE_ID(), PRF_DisableAssistedAimLockon, TRUE)	//fix for B*1988537
	endif
	
	// stop player getting in vehicles after the on foot section has started	
	if bOnFoot
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_ENTER) 
	endif
	
	//fake interior	
	if DOES_ENTITY_EXIST(player_ped_id())
		vector playerpos
		playerpos = GET_ENTITY_COORDS(player_ped_id())
		if mission_stage > ENUM_TO_INT(msf_1_chase)
			if playerpos.z > 66
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,8)
			elif  playerpos.z > 50
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,7)
			elif  playerpos.z > 47
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,6)
			elif  playerpos.z > 44.5
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,5)
			elif  playerpos.z > 41.5
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,4)
			elif  playerpos.z > 39
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,3)
			elif  playerpos.z > 33
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,2)
			elif  playerpos.z > 25
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,1)
			else
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePowerStationCS503"),2795, 1600,0,0)
			endif
		endif
	endif
		
	//mission stage checks done inside proc
	Display_Placeholder()
	Manage_DIALOGUE(int_to_enum(MSF_MISSION_STAGE_FLAGS,mission_Stage))
	
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), FINB_DAMAGE)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FINB_CAR_DAMAGE)
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	ELSE
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, FINB_CAR_DAMAGE)
	ENDIF
	
ENDPROC

PROC MISSION_SETUP()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		//Create Michael and his car now.
		IF NOT DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
			WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id, CHAR_MICHAEL, <<2394.1, 2622.0, 45.8>>, 0, TRUE, VEHICLE_TYPE_CAR)
				PRINTSTRING("waiting for Michael's car to be created") PRINTNL()
				WAIT(0)
			ENDWHILE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(peds[mpf_mike].id)
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id, CHAR_MICHAEL, <<2398.6, 2620.9, 45.5>>, 0, TRUE)
				PRINTSTRING("waiting for Michael to be created") PRINTNL()
				WAIT(0)
			ENDWHILE
			SET_RAGDOLL_BLOCKING_FLAGS(MIKE_ID(),eMikeRBFs )
		ENDIF	
	ENDIF
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(false)
	
	SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, false)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, false)	
	
	SET_WANTED_LEVEL_MULTIPLIER(0)
	CLEAR_PLAYER_WANTED_LEVEL(player_id())
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,true)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUFFALO2,true)
	
	//player is franklin
	peds[mpf_frank].id = Player_ped_id()
	
	//	set realationships	
//	ADD_RELATIONSHIP_GROUP("michael_rel", REL_Michael)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,REL_Michael,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,REL_Michael)
	ADD_SCENARIO_BLOCKING_AREA(<<2644.98730, 1340.44934, 20>>,<<2839.82617, 1728.39014, 25>>)
	
	ASSISTED_MOVEMENT_REQUEST_ROUTE("finalB1sta")
	ASSISTED_MOVEMENT_REQUEST_ROUTE("finaBroute1A")
	ASSISTED_MOVEMENT_REQUEST_ROUTE("finalbroute2")
	ASSISTED_MOVEMENT_REQUEST_ROUTE("finalbround")
	ASSISTED_MOVEMENT_REQUEST_ROUTE("finalb1st")
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, TRUE)
	
	//is the player replaying previous stage
	IF Is_Replay_In_Progress()
		IF Is_Replay_In_Progress()
			iSkipToStage = Get_Replay_Mid_Mission_Stage()
			if g_bShitskipAccepted 
				iSkipToStage++
				if iskipToStage >= 8				
					iskipToStage = 8
				endif
			endif
			if iSkipToStage = 0	
				iSkipToStage = 1
			endif	
			bis_zSkip = false
		endif
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
		ENDIF		
			
		bDoSkip = TRUE
	ELSE //start from begining
		IF IS_REPEAT_PLAY_ACTIVE()
			iSkipToStage = 0
		ENDIF
		bis_zSkip = false
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"Stage 0: meet")
		mission_stage = enum_to_int(msf_0_meet)
		Mission_Set_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))
		load_asset_stage(msf_0_meet)
		PRINTLN("Start opening Loading")
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE							
		PRINTLN("End opening Loading")
		SET_UP_MSF_0_MEET()
		SET_UP_GENERAL() //has to be after peds creation
	ENDIF
	
	DISABLE_TAXI_HAILING(true)	
	
	//INFORM_MISSION_STATS_OF_MISSION_START_FINALE_2()
	mission_substage = STAGE_ENTRY
ENDPROC

Proc timelapse()	
	sTimelapse.currentTimeOfDay = GET_CURRENT_TIMEOFDAY()
	
	IF sTimelapse.iTimelapseCut = 0
		if not IS_CUTSCENE_ACTIVE()
			bTODstart = false
			REQUEST_CUTSCENE("FIN_B_MCS_1_aandb")	
//			SET_CS_OUTFITS_AND_EXITS()
			bcs_frank		= false
			bcs_mike		= false
			bcs_mikeCar		= false
			bcs_gun			= false
		endif		
	ENDIF
	
	while not DO_TIMELAPSE(SP_MISSION_FINALE_B,sTimelapse,true,false,false,true,TRUE)	
		//Create Michael and his car now.
		IF sTimelapse.iTimelapseCut = 2	
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
				CREATE_PLAYER_VEHICLE(vehs[mvf_mike_car].id, CHAR_MICHAEL, <<2394.1, 2622.0, 45.8>>, 0, TRUE, VEHICLE_TYPE_CAR)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(peds[mpf_mike].id)
	//			CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id, CHAR_MICHAEL, <<2398.6, 2620.9, 45.5>>, 0, TRUE)
				Create_michael(<<2398.6, 2620.9, 45.5>>, 0)
			ENDIF	
		ENDIF
		wait(0)
		IF sTimelapse.iTimelapseCut = 2	
		and not bTODstart
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	
			DISPLAY_RADAR(false)
			display_hud(false)
			
			//Grab the players car now before the area is cleared.
			IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				vehs[mvf_Frank_car].id = GET_MISSION_START_VEHICLE_INDEX()	
				IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
					SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_Frank_car].id)
				ENDIF
			ENDIF			
			
			CLEAR_AREA(<< -116.5982, 493.4021, 136.6638 >>,50,true)
			CLEAR_AREA(<< 2381.4883, 2619.8186, 45.6327 >>,200,true)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("FRANKLIN", PLAYER_PED_ID(), GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MICHAEL", peds[mpf_mike].id, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))	
			bTODstart = true
		endif
	endwhile
	
endproc
Proc Run_Init_CutScene()

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	#ENDIF
	
	IF NOT bcutsceneLoaded
		if NOT IS_CUTSCENE_ACTIVE()
			REQUEST_CUTSCENE("FIN_B_MCS_1_aandb")	
//			SET_CS_OUTFITS_AND_EXITS()
			bcs_frank		= false
			bcs_mike		= false
			bcs_mikeCar		= false
			bcs_gun			= false
			PRINTSTRING("Waiting for IS_CUTSCENE_ACTIVE to return TRUE") PRINTNL()
			WAIT(0)
		endif
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
		
			//pre mission setup			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	
			IF DOES_ENTITY_EXIST(peds[mpf_mike].id)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_mike].id,"MICHAEL",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				SET_CUTSCENE_PED_PROP_VARIATION("MICHAEL", ANCHOR_HEAD, 0, 0)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_mike_car].id,"Michaels_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)	
			ENDIF
			if not HAS_PED_GOT_WEAPON(FRANK_ID(),WEAPONTYPE_PISTOL)
				GIVE_WEAPON_TO_PED(FRANK_ID(),WEAPONTYPE_PISTOL,25,true,true)
			endif
			Weapon_Object = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(FRANK_ID(),WEAPONTYPE_PISTOL)
			
			REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "FRANKLIN", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			REGISTER_ENTITY_FOR_CUTSCENE(Weapon_Object, "Franklins_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			START_CUTSCENE()
			
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			CLEAR_PED_PROP(peds[mpf_mike].id, ANCHOR_HEAD)
			bmusicTriggerStart	= false
			bcutsceneLoaded 	= TRUE			
		ENDIF	
	ENDIF
	
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
proc ST_0_MEET()

	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_KillMichael") 

	switch mission_substage
		case STAGE_ENTRY
			//Flags
			ringToneStarted = FALSE
			ringToneStopped = FALSE
			
			//Grab the players car now before the area is cleared.
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					vehs[mvf_Frank_car].id = GET_MISSION_START_VEHICLE_INDEX()	
					IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
						SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_Frank_car].id)
					ENDIF
				ENDIF
			ENDIF
				
			if IS_REPLAY_IN_PROGRESS()
			and not bis_zSkip
				SET_CLOCK_TIME(0,0,0)
				CLEAR_AREA(<< 2381.4883, 2619.8186, 45.6327 >>,200,true)
				mission_substage = 3
			else 				
				Run_Init_CutScene()
				IF bcutsceneLoaded				
					mission_substage++
				endif
			endif
		break
		case 1
			if IS_CUTSCENE_PLAYING()
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				SET_TODS_CUTSCENE_RUNNING(sTimelapse,false,false,2000,true,false)
				CLEAR_AREA(<< 2381.4883, 2619.8186, 45.6327 >>,200,true)
				STOP_FIRE_IN_RANGE(<< 2381.4883, 2619.8186, 45.6327 >>,200)				
//				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
//				and DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_car"))				
//				//mike					
//					//-------load next stage now ready -----------
//						load_asset_stage(msf_1_chase)
//					//--------------------------------------------
//					peds[mpf_mike].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
//					vehs[mvf_mike_car].id = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_car"))	
//					SET_VEHICLE_LIGHTS(vehs[mvf_mike_car].id,FORCE_VEHICLE_LIGHTS_ON)
//					CLEAR_ALL_PED_PROPS(MIKE_ID())					
//					mission_substage++
//				endif	
				if DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_mike_car].id)
						load_asset_stage(msf_1_chase)
						SET_VEHICLE_LIGHTS(vehs[mvf_mike_car].id,FORCE_VEHICLE_LIGHTS_ON)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						mission_substage++
					ENDIF
				ENDIF
			endif
		break
		case 2		
			if IS_CUTSCENE_PLAYING()
				//frank car
				if DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
					IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehs[mvf_Frank_car].id)) 
					OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehs[mvf_Frank_car].id)) 
					OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehs[mvf_Frank_car].id)) 
					OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(vehs[mvf_Frank_car].id)) 		
					AND GET_ENTITY_MODEL(vehs[mvf_Frank_car].id) <> BUS
						SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<2382.3335, 2610.1470, 45.5779>>)
						SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 187.5107)							
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)
						SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Frank_car].id,false)
						SET_MISSION_LAST_VEHICLE_AS_VEHICLE_GEN(<<0.0,0.0,0.0>>, 0.0, TRUE, CHAR_FRANKLIN)
						mission_substage = 3
					ELSE
						DELETE_VEHICLE(vehs[mvf_Frank_car].id)
					ENDIF					
				ELSE
					WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id, CHAR_FRANKLIN, <<2382.3335, 2610.1470, 45.5779>>, 187.5107, TRUE, VEHICLE_TYPE_CAR)
						PRINTSTRING("waiting on franks car being created") PRINTNL()
						WAIT(0)
					ENDWHILE
				ENDIF					
	
			endif
		break
		case 3
			//Start the ringtone for the phone 
			IF IS_CUTSCENE_PLAYING()
			AND NOT ARE_STRINGS_EQUAL (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, "Silent Ringtone Dummy")
				IF ringToneStarted = FALSE
					IF GET_CUTSCENE_TIME() > 16090
					AND IS_SCREEN_FADED_IN()
						PLAY_PED_RINGTONE(g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].RingtoneForThisPlayer, peds[mpf_mike].id, false)
						PRINTSTRING("PLAY_PED_RINGTONE NOW") PRINTNL()
						ringToneStarted = TRUE
					ENDIF
				ENDIF
				//Stop the ringtone for the phone 
				IF ringToneStopped = FALSE
					IF IS_PED_RINGTONE_PLAYING(peds[mpf_mike].id)
					AND GET_CUTSCENE_TIME() > 19544
						STOP_PED_RINGTONE(peds[mpf_mike].id)
						PRINTSTRING("STOP_PED_RINGTONE NOW") PRINTNL()
						ringToneStopped = TRUE
					ENDIF
				ENDIF			
			ENDIF
			
			if not bmusicTriggerStart
				if IS_CUTSCENE_PLAYING()
				and GET_CUTSCENE_TIME() > 82245
					TRIGGER_MUSIC_EVENT("FINB_START")
					bmusicTriggerStart= true
				endif
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")				
				if WAS_CUTSCENE_SKIPPED()
					SET_ENTITY_COORDS(FRANK_ID(),<<2384.43555, 2620.92871, 45.60714>>)
					SET_ENTITY_HEADING(FRANK_ID(),179.01)
				endif
				FORCE_PED_MOTION_STATE(FRANK_ID(), MS_ON_FOOT_RUN, false, FAUS_CUTSCENE_EXIT )
				TASK_ENTER_VEHICLE(FRANK_ID(),vehs[mvf_frank_car].id,DEFAULT_TIME_NEVER_WARP)				
				
				REPLAY_STOP_EVENT()
				
				bcs_frank = true
			else
				Point_Gameplay_cam_at_heading(182.0164)
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				SET_PED_INTO_VEHICLE(MIKE_ID(),vehs[mvf_mike_car].id)
				SET_VEHICLE_DOORS_SHUT(vehs[mvf_mike_car].id)
				SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_ID(),REL_Michael)
				ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL",true)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)	
				bcs_mike = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id,001,"FIN2")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id,2000)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_mike_car].id)
				bcs_mikecar = true
			endif
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_weapon")
				IF DOES_ENTITY_EXIST(Weapon_Object)
					GIVE_WEAPON_OBJECT_TO_PED(Weapon_Object, FRANK_ID())
				ENDIF
				println("Franklins_weapon")
				bcs_gun = true
			endif
			
			if bcs_frank
			and bcs_mike
			and bcs_mikeCar
			and bcs_gun
			or IS_REPLAY_IN_PROGRESS()	
				if not bmusicTriggerStart
					TRIGGER_MUSIC_EVENT("FINB_START")
					bmusicTriggerStart= true
				endif
				vehs[mvf_mike_car].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_mike_car].id,true)						
				SET_VEHICLE_RADIO_ENABLED(vehs[mvf_frank_car].id,true)				
				print_now("FIN2_CHASE",DEFAULT_GOD_TEXT_TIME,1)				
				SET_PLAYER_CONTROL(player_id(),true)
				DISPLAY_RADAR(true)
				DISPLAY_HUD(true)
				mission_substage = STAGE_ENTRY
				mission_Set_Stage(msf_1_chase)
				
			endif					
		break
	endswitch
ENDPROC
PROC ST_1_CHASE()
	switch mission_substage
		case STAGE_ENTRY
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: chase")
			//Set the lights distance so the headlights don't disappear. needs set to 0 on clean up
			SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(100) 
			
			//flags
			playerInBus = FALSE
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			TRIGGER_MUSIC_EVENT("FINB_CHASE")
			if IS_REPLAY_IN_PROGRESS()
			and g_replayMissionStage = 1
			and bCarRunning
				if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mike_car].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id,001,"FIN2")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id,8000)
				endif
				DELETE_ALL_TRAINS()
				if not DOES_ENTITY_EXIST(vehs[mvf_train].id)
					vehs[mvf_train].id = CREATE_MISSION_TRAIN(TRAIN_CONFIG,V_TRAIN_START,true)	
				endif
				train_speed_perc = 0.38 // 62% slower speed of mike's car
				SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_frank_car].id)
				SET_VEHICLE_FORWARD_SPEED(vehs[mvf_frank_car].id, 20.0)
				bCarRunning = false
			else			
				if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mike_car].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id,001,"FIN2")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id,6000)
				endif
				DELETE_ALL_TRAINS()
				if not DOES_ENTITY_EXIST(vehs[mvf_train].id)
					vehs[mvf_train].id = CREATE_MISSION_TRAIN(TRAIN_CONFIG,V_TRAIN_START,true)	
				endif
				train_speed_perc = 0.3665 // 63.35% slower speed of mike's car
			endif
		
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)					
						
			SET_ENTITY_INVINCIBLE(vehs[mvf_mike_car].id,true)			
			icamDelay = GET_GAME_TIMER()
			CLEAR_AREA_OF_PROJECTILES(<< 2384.3772, 2556.3708, 46.1006 >>,4000) //get rid of previous bombs or grenades fired.
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_IN_CAR")
				START_AUDIO_SCENE("FIN_2_CHASE_IN_CAR")
			endif
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_mike_car].id,"FIN_2_MICHAELS_CAR")
			mission_substage++			
		break
		case 1
			if IS_SCREEN_FADED_OUT()
				IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
					Point_Gameplay_cam_at_heading(182.0164)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)	
				print_now("FIN2_CHASE",DEFAULT_GOD_TEXT_TIME,1)		
			endif
			if GET_GAME_TIMER() - icamDelay > 1500
			and IS_PED_IN_ANY_VEHICLE(FRANK_ID())
				SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(FRANK_ID()),true)	
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: chase")					
				bcreateScene = false
				bloadedScene = false
				mission_substage++			
			endif
		break		
		case 2			
			if not bloadedScene
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE_ID(),<< 2661.4829, 1639.2833, 23.5914 >>) < 200
					//-------load next stage now ready -----------
						load_asset_stage(msf_2_power_plant)
					//--------------------------------------------
					load_asset_model(sAssetData,S_M_M_Security_01)
					CREATE_FORCED_OBJECT(<< 2665.6328, 1638.5288, 24.3911 >>,5,prop_sec_barier_02b,true)
					bcreateScene = true
					bloadedScene = true
				endif
			else
				if bcreateScene
				and HAS_MODEL_LOADED(S_M_M_Security_01)
					//create dead guy 
					peds[mpf_dead_guy].id = CREATE_PED(PEDTYPE_MISSION,S_M_M_Security_01,<< 2661.4829, 1639.2833, 23.5914 >>, 85.5309)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_dead_guy].id,true)
					set_entity_health(peds[mpf_dead_guy].id,101)
					SET_PED_MONEY(peds[mpf_dead_guy].id,0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_dead_guy].id,PED_COMP_TORSO,0,0)	
					Unload_Asset_Model(sassetdata,S_M_M_Security_01)
					bcreateScene = false
				endif
			endif
			if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mike_car].id)
				CLEAR_PED_TASKS_IMMEDIATELY(MIKE_ID())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)
				SET_ENTITY_INVINCIBLE(vehs[mvf_mike_car].id,false)
				GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,true)	
				SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
				OPEN_SEQUENCE_TASK(seq)
					TASK_LEAVE_ANY_VEHICLE(null)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<2676.50684, 1592.90393, 31.51181>>,PEDMOVE_RUN)
					TASK_SHOOT_AT_ENTITY(null,FRANK_ID(),-1,FIRING_TYPE_RANDOM_BURSTS)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())	
				if DOES_BLIP_EXIST(vehs[mvf_mike_car].blip)
					remove_blip(vehs[mvf_mike_car].blip)
				endif
				peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)
				mission_substage++
			endif
		break
		case 3
			//Fail if player tries to go another way round
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2667.2, 1639.0, 24>>) > 100
				mission_failed(mff_lost_mike)
			ENDIF
		
			SET_PED_RESET_FLAG(MIKE_ID(), PRF_UseTighterTurnSettingsForScript, TRUE)
		
			if (not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mike_car].id))
			and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2676.1702, 1593.0712, 31.5119 >>,<<5,5,2>>)
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(FRANK_ID(),<<2676.50684, 1592.90393, 31.51181>>) < 35
				or IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())
				or IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID())
				or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID())
					if IS_PED_IN_ANY_VEHICLE(FRANK_ID())
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = BUS
							playerInBus = TRUE
						ENDIF
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(FRANK_ID()))
					endif
					
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, TRUE, TRUE, FALSE, TRUE)
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, TRUE)
					PRINTSTRING("Clearing area of projectiles") PRINTNL()		
										
					KILL_CHASE_HINT_CAM(sHintCam) 
					SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
					mission_substage = STAGE_ENTRY
					Mission_Set_Stage(msf_2_power_plant)
				endif
			endif
		break
	endswitch
	
	IF DOES_ENTITY_EXIST(vehs[mvf_mike_car].id) AND NOT IS_ENTITY_DEAD(vehs[mvf_mike_car].id)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mike_car].id)
			IF VDIST2(GET_ENTITY_COORDS(vehs[mvf_mike_car].id), <<2611.37891, 1802.92578, 25.41560>>) < (50.0*50.0)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
			ENDIF
		ENDIF
	ENDIF
	
	if IS_PED_IN_ANY_VEHICLE(FRANK_ID())
		if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(FRANK_ID(),<<2676.50684, 1592.90393, 31.51181>>) < 23.0
		or IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2682.93701, 1557.59534, 23.49973>>,<<2683.11572, 1623.92944, 23.49991>>,35)
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(FRANK_ID()))
				TASK_LEAVE_ANY_VEHICLE(FRANK_ID())
			endif
		endif
	endif
	
//stage checks 
	//rubber banding chase & Train
	if isentityalive(FRANK_ID())
	and isentityalive(MIKE_ID())
	and isentityalive(vehs[mvf_mike_car].id)
		fDist = VDIST(GET_ENTITY_COORDS(FRANK_ID()),GET_ENTITY_COORDS(MIKE_ID()))	
		vector vplayerHeight = GET_ENTITY_COORDS(FRANK_ID())
		if vplayerHeight.z < 17.5
			Mission_Failed(mff_lost_mike)
		endif
		Rubber_banding(fplaybackSpeed,peds[mpf_frank].id,vehs[mvf_mike_car].id)
		SET_PLAYBACK_SPEED(vehs[mvf_mike_car].id,fplaybackSpeed)
		Manage_train_chase() //sorts out train speed during chase
		//mission fail if lost michael
		if 	fdist > 225
			Mission_Failed(mff_lost_mike)
		endif
	endif
	
	CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(sHintCam,vehs[mvf_mike_car].id)
	UPDATE_CHASE_BLIP(vehs[mvf_mike_car].blip,vehs[mvf_mike_car].id,225)
	
	if IsEntityAlive(peds[mpf_dead_guy].id)
	and IsEntityAlive(vehs[mvf_mike_car].id)
		if IS_ENTITY_AT_ENTITY(peds[mpf_dead_guy].id,vehs[mvf_mike_car].id,<<2,2,2>>)
			SET_ENTITY_HEALTH(peds[mpf_dead_guy].id,99)
		endif
	endif
		
	if IS_GAMEPLAY_HINT_ACTIVE()
		if not IS_AUDIO_SCENE_ACTIVE("FIN_2_FOCUS_CAM")
			START_AUDIO_SCENE("FIN_2_FOCUS_CAM")
		endif
	else
		if IS_AUDIO_SCENE_ACTIVE("FIN_2_FOCUS_CAM")
			stop_Audio_scene("FIN_2_FOCUS_CAM")
		endif
	endif
	IF IsEntityAlive(MIKE_ID())
		IF GET_DISTANCE_BETWEEN_COORDS(<<2684.2, 1615.1, 24>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 45
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				PRINTSTRING("special ability is being switched off") PRINTNL()
				SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
				ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE) 
			ELSE
				PRINTSTRING("special ability is disabled") PRINTNL()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bLoadingSRL
PROC ST_2_POWERPLANT()
	switch mission_substage
		case STAGE_ENTRY
			//Flags
			bFrankFacingMike = FALSE
			bLoadingSRL = FALSE
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, TRUE, TRUE)
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30)
			PRINTSTRING("Clearing area of projectiles2") PRINTNL()
			
			if DOES_ENTITY_EXIST(vehs[mvf_mike_car].id)
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_mike_car].id)
					SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_mike_car].id)
				endif
				
				SET_ENTITY_INVINCIBLE(vehs[mvf_mike_car].id,false)
			endif
		//sync scene
			SyncSceneIG1 = CREATE_SYNCHRONIZED_SCENE(<< 2676.3481, 1594.8707, 23.595 >>, << 0, 0, -90 >>)			
		//mike	
			SET_CURRENT_PED_WEAPON(MIKE_ID(), WEAPONTYPE_UNARMED, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(),true)
			TASK_SYNCHRONIZED_SCENE(MIKE_ID(),SyncSceneIG1,"missfinale_b_ig_1","arrive_plant_m",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)		
//			SET_FORCE_FOOTSTEP_UPDATE(MIKE_ID(), true)
		//Frank
			if not IS_PED_IN_ANY_VEHICLE(FRANK_ID())			
				SET_ENTITY_FACING(FRANK_ID(),<<2676.50684, 1592.90393, 31.51181>>)
				TASK_LOOK_AT_ENTITY(FRANK_ID(),MIKE_ID(),-1,SLF_WHILE_NOT_IN_FOV)
				if IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2685.24414, 1634.93970, 20>>,<<2685.10547, 1598.70410, 33>>,25)
					TASK_PLAY_ANIM(FRANK_ID(),"missfinale_b_ig_1","arrive_plant_F",INSTANT_BLEND_IN,WALK_BLEND_OUT)	
				else
					TASK_TURN_PED_TO_FACE_ENTITY(FRANK_ID(),MIKE_ID(),5000)
				endif			
			else
				TASK_LEAVE_ANY_VEHICLE(FRANK_ID())
			endif
			//frank car
			if DOES_ENTITY_EXIST(vehs[mvf_frank_car].id)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_frank_car].id,FALSE)
				SET_VEHICLE_RADIO_ENABLED(vehs[mvf_frank_car].id,FALSE)
			endif
			if DOES_BLIP_EXIST(vehs[mvf_mike_car].blip)
				remove_blip(vehs[mvf_mike_car].blip)
			endif
			if NOT DOES_BLIP_EXIST(peds[mpf_mike].blip)
				peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)
			ENDIF
			SET_VEHICLE_FIXED(vehs[mvf_mike_car].id)			
		//camera
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)		
			
			HANG_UP_AND_PUT_AWAY_PHONE()

			cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 2701.9651, 1596.5630, 32.7892 >>, << -1.3076, 0.0972, 109.9187 >> ,32)
			SET_CAM_ACTIVE(cameraIndex,true)			
			SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.4)
			SET_CAM_PARAMS(cameraIndex,<< 2701.9434, 1596.6038, 33.0893 >>, << -3.5782, 0.0972, 113.5868 >>,32,12000)
			Prep_start_Cutscene(false,<< 2701.9651, 1596.5630, 32.7892 >>)
			//misc
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			bOnFoot = true
			icamDelay = GET_GAME_TIMER()
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Stage 2: power plant")
			TRIGGER_MUSIC_EVENT("FINB_ARRIVE")
			STOP_AUDIO_SCENES()
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_MICHAEL_ESCAPE_SCENE")
				START_AUDIO_SCENE("FIN_2_MICHAEL_ESCAPE_SCENE")
			endif
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehs[mvf_mike_car].id)
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FINB_FOOTCHASE_TIME)
			bPushInFXPlayed = FALSE
			mission_substage++
		break	
		case 1		
			IF bFrankFacingMike = FALSE
				if not IS_PED_IN_ANY_VEHICLE(FRANK_ID())
					IF playerInBus = TRUE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2673.8826, 1604.6268, 23.4956>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 239.5733)
					ENDIF
					IF NOT IS_ENTITY_ON_SCREEN(FRANK_ID())
						SET_ENTITY_FACING(FRANK_ID(),<<2693, 1594, 31>>)
						TASK_LOOK_AT_ENTITY(FRANK_ID(),MIKE_ID(),-1,SLF_WHILE_NOT_IN_FOV)
						bFrankFacingMike = TRUE
					ELSE
						TASK_TURN_PED_TO_FACE_ENTITY(FRANK_ID(),MIKE_ID())
						TASK_LOOK_AT_ENTITY(FRANK_ID(),MIKE_ID(),-1,SLF_WHILE_NOT_IN_FOV)
						bFrankFacingMike = TRUE		
					ENDIF
				ENDIF
			ENDIF
			if get_game_timer() - icamDelay > 1500
			and GET_GAME_TIMER() - icamDelay < 3700	// Disable skipping once we've triggered the FX (url:bugstar:2057705)
			and IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)			
				CLEAR_PED_TASKS(FRANK_ID())
				RENDER_SCRIPT_CAMS(false,false)
				Prep_stop_Cutscene(true)				
				Point_Gameplay_cam_at_coord(<<2672.41333, 1592.09290, 29.49044>>)				
				if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG1)
					SET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG1,1)
				endif
				mission_substage = 2
			endif
			// url:bugstar:2057705
			IF NOT bPushInFXPlayed
			AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON 
			AND GET_GAME_TIMER() - icamDelay >= 3700
				ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				bPushInFXPlayed = TRUE
			ENDIF
			if get_game_timer() - icamDelay > 4000
				Prep_stop_Cutscene(true)
				Point_Gameplay_cam_at_coord(<<2703.01978, 1593.13367, 31.51181>>)
				if not IS_PED_IN_ANY_VEHICLE(FRANK_ID())
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(15)	
				endif
				SET_PLAYER_CONTROL(player_id(),true)
//				SET_FORCE_FOOTSTEP_UPDATE(MIKE_ID(), false)
				mission_substage = 2	
			endif
		break
		case 2
			clear_players_task_on_control_input(SCRIPT_TASK_SYNCHRONIZED_SCENE)
			clear_players_task_on_control_input(SCRIPT_TASK_PLAY_ANIM)
			clear_players_task_on_control_input(SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
			
			if IS_AUDIO_SCENE_ACTIVE("FIN_2_MICHAEL_ESCAPE_SCENE")
				STOP_AUDIO_SCENE("FIN_2_MICHAEL_ESCAPE_SCENE")
			endif
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_ON_FOOT")
				START_AUDIO_SCENE("FIN_2_CHASE_ON_FOOT")
			endif
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG1)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG1) >= 1	
				//-------load next stage now ready -----------
					load_asset_stage(msf_3_confrontation)
				//--------------------------------------------
				if IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_ID(),SCRIPT_TASK_SYNCHRONIZED_SCENE)
				or IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_ID(),SCRIPT_TASK_PLAY_ANIM)
				or IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_ID(),SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					CLEAR_PED_TASKS(FRANK_ID())
				endif
				TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_ID(),<< 2764.6760, 1559.7496, 31.4983 >>,PEDMOVE_SPRINT)				
				FORCE_PED_MOTION_STATE(MIKE_ID(),MS_ON_FOOT_RUN,false,FAUS_CUTSCENE_EXIT)
				mission_substage++
			endif
		break			
		case 3
//initial delete of mike after he runs around the corner.
			if isentityalive(MIKE_ID())
				if IS_ENTITY_AT_COORD(MIKE_ID(),<< 2764.6760, 1559.7496, 31.4983 >>,<<4,4,2>>)
				and	(not IS_ENTITY_ON_SCREEN(MIKE_ID())or IS_ENTITY_OCCLUDED(MIKE_ID()))	
					if DOES_BLIP_EXIST(peds[mpf_mike].blip)
						remove_blip(peds[mpf_mike].blip)
					endif
					SAFE_DELETE_PED(peds[mpf_mike].id)
					if not does_blip_exist(blip_objective)	
						blip_objective = CREATE_BLIP_FOR_COORD(<< 2766.9392, 1565.0616, 31.4983 >>)
//						SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)				
					endif
				endif
			else
				if bFollowDisplayed = false
				and IS_SAFE_TO_DISPLAY_GODTEXT()
					PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
					bFollowDisplayed = true
				endif
			endif
			
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
			
//Player reached half way on walkway
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2704.0027, 1593.4563, 31.9187 >>,<<3,3,3>>)	
				//mike re-create
				if DOES_ENTITY_EXIST(MIKE_ID())	
					if not does_blip_exist(peds[mpf_mike].blip)
						peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)
					endif
					if DOES_BLIP_EXIST(blip_objective)
						REMOVE_BLIP(blip_objective)						
					endif	
					
					SET_ENTITY_COORDS(MIKE_ID(),<< 2768.5076, 1575.3491, 31.4983 >>)	
					SET_ENTITY_VISIBLE(MIKE_ID(),true)	
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
					GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)					
								
					OPEN_SEQUENCE_TASK(seq)	
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<< 2769.36572, 1578.53491, 31.51167 >>,FRANK_ID(),PEDMOVE_SPRINT,true)
						TASK_SHOOT_AT_ENTITY(null,FRANK_ID(),-1,FIRING_TYPE_CONTINUOUS)							
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					//misc next stage					
					icamDelay = GET_GAME_TIMER()
					SAFE_RELEASE_PED(peds[mpf_dead_guy].id)
					mission_substage++	
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				else
					Create_michael(<< 2768.5076, 1575.3491, 31.4983 >>,0)	
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				endif
// player went a different way to the location				
			elif IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2765.76025, 1560.41675, 31>>,<<2768.03687, 1569.14636, 34>>,4.5) 
				//Mike
				if DOES_ENTITY_EXIST(MIKE_ID())				
					SET_ENTITY_COORDS(MIKE_ID(),<< 2757.72095, 1537.80286, 39.33770 >>)		
					SET_ENTITY_VISIBLE(MIKE_ID(),false)		
					if not DOES_BLIP_EXIST(blip_objective)
						blip_objective = CREATE_BLIP_FOR_COORD(<< 2766.71436, 1564.82092, 31.51167 >>)
//						SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
						if bFollowDisplayed = false
							PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
							bFollowDisplayed = true
						endif
					endif
					GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,true)	
					SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Stage 3: confrontation")
					if IS_SCREEN_FADED_OUT()			
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					endif
					mission_set_stage(msf_3_confrontation)
					mission_substage = STAGE_ENTRY
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				else
					Create_michael(<< 2757.72095, 1537.80286, 39.33770 >>,341.1591)
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				endif
			endif
		break	
		case 4	
			if isentityalive(MIKE_ID())
				if IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())
				or IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID())
				or GET_GAME_TIMER() - icamDelay > 6000
				or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 30
				or (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID()))
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())		
					TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_ID(),<< 2767.4143, 1572.8604, 31.4983 >>,PEDMOVE_SPRINT)				
					icamDelay = GET_GAME_TIMER()
					mission_substage++
				endif
			else
				mission_substage++
			ENDIF
		break
		case 5
			if isentityalive(MIKE_ID())
				if IS_ENTITY_AT_COORD(mike_ID(),<< 2767.4143, 1572.8604, 31.4983 >>,<<3,3,5>>)
					if DOES_BLIP_EXIST(peds[mpf_mike].blip)
						remove_blip(peds[mpf_mike].blip)
					endif
					SAFE_DELETE_PED(peds[mpf_mike].id)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2766.9392, 1565.0616, 31.4983 >>)
	//				SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
					if bFollowDisplayed = false
						PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
						bFollowDisplayed = true
					endif
					mission_substage++
				endif
			ELSE
				mission_substage++
			ENDIF
		break
		case 6
			mission_set_stage(msf_3_confrontation)
			mission_substage = STAGE_ENTRY			
		break
	endswitch
	
	//Run this check for bug fix 1702338
	if isentityalive(MIKE_ID())
		if IS_ENTITY_AT_COORD(MIKE_ID(),<< 2767.5, 1574.2, 31.8 >>,<<2.5,2.5,2.5>>)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 25
				if DOES_BLIP_EXIST(peds[mpf_mike].blip)
					remove_blip(peds[mpf_mike].blip)
				endif
				SAFE_DELETE_PED(peds[mpf_mike].id)
				IF NOT DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2766.9392, 1565.0616, 31.4983 >>)
					PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_AT_COORD(FRANK_ID(), <<2765.92358, 1565.14172, 31.51252>>, <<30,30,30>>)
		IF NOT bLoadingSRL
			PREFETCH_SRL("finbconf")
			SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
			bLoadingSRL = TRUE
		ENDIF
	ELIF NOT IS_ENTITY_AT_COORD(FRANK_ID(), <<2765.92358, 1565.14172, 31.51252>>, <<40,40,40>>)
		IF bLoadingSRL	
			END_SRL()
			bLoadingSRL = FALSE
		ENDIF
	ENDIF
	
	//Run this check every frame incase player takes a short cut
	If IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2765.76025, 1560.41675, 31>>,<<2768.03687, 1569.14636, 34>>,4.5) 
		//Mike
		if DOES_ENTITY_EXIST(MIKE_ID())				
			SET_ENTITY_COORDS(MIKE_ID(),<< 2757.72095, 1537.80286, 39.33770 >>)		
			SET_ENTITY_VISIBLE(MIKE_ID(),false)		
			if not DOES_BLIP_EXIST(blip_objective)
				blip_objective = CREATE_BLIP_FOR_COORD(<< 2766.71436, 1564.82092, 31.51167 >>)
				SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
				if bFollowDisplayed = false
					PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
					bFollowDisplayed = true
				endif
			endif
			GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,true)	
			SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Stage 3: confrontation")
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			mission_set_stage(msf_3_confrontation)
			mission_substage = STAGE_ENTRY
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
		else
			Create_michael(<< 2757.72095, 1537.80286, 39.33770 >>,341.1591)
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
		endif
	endif	
	
// if player still on vehicle in next area take them off it.	
	if IS_PED_IN_ANY_VEHICLE(FRANK_ID())
		if IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2759.54346, 1604.30334, 22>>,<<2731.96021, 1500.85767, 60>>,70)
		or IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2665.01685, 1593.25269, 22>>,<<2727.68701, 1593.78809, 40>>,50)
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_frank_car].id)	
				TASK_LEAVE_ANY_VEHICLE(FRANK_ID())
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_frank_car].id,FALSE)					
			ENDIF
		endif
	endif
	
	//Fail if player goes too far away from here
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2716.3, 1592.9, 32>>) > 100
		Mission_Failed(mff_lost_mike)
	ENDIF
		
ENDPROC

INT iSRLTime
PROC ST_3_CONFRONTATION()
	switch mission_substage
		case STAGE_ENTRY		
			//Player
			//Grab the players current weapon to be given back after cutscene
			playerWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
			
			//Mike
			if DOES_ENTITY_EXIST(MIKE_ID())				
				SET_ENTITY_COORDS(MIKE_ID(),<< 2757.72095, 1537.80286, 39.33770 >>)		
				SET_ENTITY_VISIBLE(MIKE_ID(),false)		
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2766.71436, 1564.82092, 31.51167 >>)
					SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
					if bFollowDisplayed = false
						PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
						bFollowDisplayed = true
					endif
				endif
				GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,true)	
				SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Stage 3: confrontation")
				if IS_SCREEN_FADED_OUT()			
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				mission_substage++
			else
				Create_michael(<< 2757.72095, 1537.80286, 39.33770 >>,341.1591)
			endif
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
		break
		case 1
			if IS_ENTITY_IN_ANGLED_AREA(FRANK_ID(),<<2765.76025, 1560.41675, 31>>,<<2768.03687, 1569.14636, 34>>,4.5)
			and HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_ID())
				if DOES_BLIP_EXIST(blip_objective)
					remove_blip(blip_objective)
				endif
			//Mike	
				SET_ENTITY_VISIBLE(MIKE_ID(),true)				
				GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,true)	
				SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
				SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_PERFECT_ACCURACY,false)
				if not DOES_BLIP_EXIST(peds[mpf_mike].blip)
					peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)			
				endif
				//-------load next stage now ready -----------
					load_asset_stage(msf_4_heli_appears)
				//--------------------------------------------
				TRIGGER_MUSIC_EVENT("FINB_STEPS")	
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
				
				SyncSceneIG2 = CREATE_SYNCHRONIZED_SCENE(<<2769.011,1563.390,31.525>>,<<0,0,-105.0>>)
				TASK_SYNCHRONIZED_SCENE(FRANK_ID(),SyncSceneIG2,"missfinale_b_ig_2","mic_shoots_fr_f",INSTANT_BLEND_IN,walk_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE(MIKE_ID(),SyncSceneIG2,"missfinale_b_ig_2","mic_shoots_fr_m",INSTANT_BLEND_IN,walk_BLEND_OUT)
			//cams
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)	
				
				cameraIndex = create_cam("DEFAULT_ANIMATED_CAMERA",FALSE) 
				SET_CAM_ACTIVE(cameraIndex,true)
//				PLAY_CAM_ANIM(cameraIndex,"mic_shoots_fr_cam","missfinale_b_ig_2",<<2769.011,1563.390,31.525>>,<<0,0,-105.0>>)
				PLAY_SYNCHRONIZED_CAM_ANIM(cameraIndex, SyncSceneIG2, "mic_shoots_fr_cam","missfinale_b_ig_2")
//				cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<2770.8,1563.7,31.0>>,<<19.9,0.4,153.1>>,19.0)
//				SET_CAM_PARAMS(cameraIndex,<<2770.6,1563.3,31.1>>,<<19.9,0.4,153.1>>,19.0,4000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
//				SHAKE_CAM(cameraIndex,"hand_shake",0.3)


				IF IS_SRL_LOADED()
					BEGIN_SRL()
					printstring("started srl!") printnl()
				ELSE
					END_SRL()
				ENDIF
				iSRLTime = GET_GAME_TIMER()
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,true)
				
				prep_start_cutscene(false,<<2769.011,1563.390,31.525>>)
				mission_substage++
			endif
		break
		case 2		
			SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER() - iSRLTime))
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG2)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG2) >= 0.4
//				SET_CAM_PARAMS(cameraIndex,<<2759.8,1539.4,41.4>>,<<-21.3,0,-12.7>>,28.9,0)
//				set_cam_params(cameraIndex,<<2759.9,1539.5,41.4>>,<<-21.3,0,-12.7>>,28.9,4000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				mission_substage++
			endif
		break
		case 3
			SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER() - iSRLTime))
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG2)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG2) >= 0.63
				SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2767.07178, 1569.26062, 31.51177>>,true)
				mission_substage++
			endif
		break
		case 4
			SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER() - iSRLTime))
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG2)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG2) >= 0.75
//				SET_CAM_PARAMS(cameraIndex,<<2766.7,1563.4,32.9>>,<<-1.9,0,-12.2>>,50,0) //0.754
//				set_cam_params(cameraIndex,<<2766.7,1563.3,32.9>>,<<1.8,0,-35.8>>,50,2000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR) //0.953
				mission_substage++
			endif
		break
		case 5
			SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER() - iSRLTime))
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG2)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG2) >= 0.82
				SET_PED_SHOOTS_AT_COORD(MIKE_ID(),<<2767.76440, 1565.57629, 32.76665>>,true)
				mission_substage++
			endif
		break
		case 6
			SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER() - iSRLTime))
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG2)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG2) >= 0.86
				Point_Gameplay_cam_at_heading(305.7959)
				prep_stop_cutscene(true,false,false,DEFAULT_INTERP_TO_FROM_GAME,SPC_NONE,true)			
				
				END_SRL()
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(FRANK_ID(),<<2769.7646, 1568.7050, 34.0682>>,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP)
				FORCE_PED_MOTION_STATE(FRANK_ID(),MS_ON_FOOT_RUN,true,FAUS_CUTSCENE_EXIT)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),playerWeapon,FALSE)
//				MIKE
				SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_PERFECT_ACCURACY,false)
				SET_PED_ACCURACY(MIKE_ID(),15)
				if not DOES_BLIP_EXIST(peds[mpf_mike].blip)
					peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)			
				endif				
				OPEN_SEQUENCE_TASK(seq)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<< 2749.0803, 1525.3656, 39.3373 >>,FRANK_ID(),PEDMOVE_RUN,true)					
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mike_id(),seq)
				CLEAR_SEQUENCE_TASK(seq)	
				mission_substage++
			endif
		break
		case  7
			clear_players_task_on_control_input(SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
		//top of stairs
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2769.1233, 1566.6147, 38.5209 >>,<<1,1,2>>)	
				icamDelay = GET_GAME_TIMER()				
				mission_substage= STAGE_ENTRY
				mission_set_stage(msf_4_heli_appears)			
			endif
		break		
	endswitch
	
	//mission checks
	if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2737.1770, 1522.5280, 24.3547 >>,<<6,6,3>>)
	or IS_ENTITY_AT_COORD(FRANK_ID(),<< 2757.5522, 1528.3209, 31.9641 >>,<<3,3,2>>)
		Mission_Failed(mff_lost_mike)
	endif
	
ENDPROC
PROC ST_4_HELI()
	switch mission_substage
		case STAGE_ENTRY
	
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Stage 4: heli appears")	
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif	
			
		//mike	
			OPEN_SEQUENCE_TASK(seq)	
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<< 2753.3665, 1523.5206, 39.3162 >>,FRANK_ID(),PEDMOVE_SPRINT,false)
				TASK_SHOOT_AT_COORD(null,<< 2764.5376, 1565.0852, 39.3267 >>,-1,FIRING_TYPE_CONTINUOUS)										
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
			CLEAR_SEQUENCE_TASK(seq)				
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_ON_FOOT")
					start_audio_scene("FIN_2_CHASE_ON_FOOT")
				endif
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_HELICOPTER_ARRIVES")
				start_audio_scene("FIN_2_HELICOPTER_ARRIVES")
			endif
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
			icamDelay = GET_GAME_TIMER()
			mission_substage++			
		break
		case 1							
			if IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())
			or IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID())
			or GET_GAME_TIMER() - icamDelay > 8000
			or (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 26
			and IS_ENTITY_AT_COORD(MIKE_ID(),<<2753.3665, 1523.5206, 39.3162>>,<<3,3,3>>))		
			or (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID()))
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
				TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_ID(),<< 2743.2749, 1525.3877, 39.3162 >>,PEDMOVE_SPRINT,DEFAULT_TIME_BEFORE_WARP,0.3)
				icamDelay = GET_GAME_TIMER()
				//-------load next stage now ready -----------
					load_asset_stage(msf_5_shoot_out)
				//--------------------------------------------
				mission_substage++
			endif
		break
		case 2
		//delete michael from the scene until needed
			if IS_ENTITY_AT_COORD(MIKE_ID(),<< 2743.2749, 1525.3877, 39.3162 >>,<<2,2,2>>)	
				if DOES_ENTITY_EXIST(MIKE_ID())	
					DELETE_PED(peds[mpf_mike].id)
				endif
				mission_substage++
			endif
		break
		case 3
			if GET_GAME_TIMER() - icamDelay > 1000	
				mission_substage = STAGE_ENTRY
				Mission_Set_Stage(msf_5_shoot_out)
			endif
		break
	endswitch	
ENDPROC
PROC ST_5_SHOOT_OUT()
	switch mission_substage
		case STAGE_ENTRY		
		
			//Flags
			doneUPSTChat = FALSE
			
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			settimera(0)	
			if not DOES_BLIP_EXIST(blip_objective)
				blip_objective = CREATE_BLIP_FOR_COORD(<< 2741.32, 1523.05, 46.51 >>)
//				SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
			endif
			if bFollowDisplayed = false
				PRINT_NOW("FIN2_SPOT",DEFAULT_GOD_TEXT_TIME,1)
				bFollowDisplayed = true
			endif			
			
//			//heli	
//			if DOES_ENTITY_EXIST(vehs[mvf_heli].id)
//				SET_VEHICLE_SEARCHLIGHT(vehs[mvf_heli].id,true,true)
//				if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
//					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id)
//				endif
//			endif
//			eheliLight = HL_VEC1
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Stage 5: shoot out")			
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_ON_FOOT")
				start_audio_scene("FIN_2_CHASE_ON_FOOT")
			endif
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_HELICOPTER_ARRIVES")
				start_audio_scene("FIN_2_HELICOPTER_ARRIVES")
			endif
			mission_substage++
		break
		case 1
		//around the corner
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2729.5039, 1530.7422, 39.3167 >>,<<2,2,4>>)
			or IS_ENTITY_AT_COORD(FRANK_ID(),<< 2738.4851, 1532.3734, 39.7673 >>,<<2,2,4>>)
			or IS_ENTITY_AT_COORD(FRANK_ID(),<< 2755.4, 1531.0, 39.8887 >>,<<7,7,4>>)
								
				//mike	
				if Create_michael(<< 2740.5945, 1522.9148, 44.5066 >>, 276.0896)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())						
					CLEAR_PED_TASKS(MIKE_ID())		
					if DOES_BLIP_EXIST(blip_objective)
						REMOVE_BLIP(blip_objective)
					endif
					if not DOES_BLIP_EXIST(peds[mpf_mike].blip)
						peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)
					endif
					GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,TRUE)
					CLEAR_PED_TASKS(MIKE_ID())				
					OPEN_SEQUENCE_TASK(seq)		
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
						TASK_SHOOT_AT_ENTITY(null,FRANK_ID(),-1,FIRING_TYPE_CONTINUOUS)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					SET_ENTITY_VISIBLE(MIKE_ID(), TRUE)
//					//heli
//					eheliLight = HL_MIKE
//					EheliPos   = eH_Corner2
//					IheliStage = 0
					icamDelay = GET_GAME_TIMER()
					mission_substage++
				endif
			endif
			
			if is_Entity_at_coord(FRANK_ID(),<<2741.32, 1523.05, 45.25>>,<<1.5,1.5,1>>)				
				if Create_michael(<<2736.41, 1537.38, 49.6966 >>, 276.0896)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
					if DOES_BLIP_EXIST(blip_objective)
						REMOVE_BLIP(blip_objective)
					endif
					
					CLEAR_PED_TASKS(MIKE_ID())
					if not DOES_BLIP_EXIST(peds[mpf_mike].blip)
						peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)
					endif
					SET_PED_COMBAT_ATTRIBUTES(MIKE_ID(),CA_REQUIRES_LOS_TO_SHOOT,false)
					GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,50,true,TRUE)
					CLEAR_PED_TASKS(MIKE_ID())				
					OPEN_SEQUENCE_TASK(seq)		
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<< 2733.0586, 1528.9579, 49.6923 >>,FRANK_ID(),PEDMOVE_RUN,true)
						TASK_SHOOT_AT_ENTITY(null,FRANK_ID(),-1,FIRING_TYPE_RANDOM_BURSTS)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
					CLEAR_SEQUENCE_TASK(seq)
					
//					//heli
//					eheliLight = HL_MIKE				
//					EheliPos   = eH_Corner2
//					IheliStage = 0
					icamDelay = GET_GAME_TIMER()
					mission_substage = 301 //skip to the alternate 
				endif
			endif
		break
		case 2
		//second step			
			if (IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2741.1575, 1523.1213, 44.4857 >>,<<2,2,2>>))
			or (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID()) and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2741.1575, 1523.1213, 44.4857 >>,<<2,2,2>>))
			or (GET_GAME_TIMER() - icamDelay) > 6000
			or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 9.2
			or (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID()))
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())	
				OPEN_SEQUENCE_TASK(seq)	
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<2738.97021, 1526.11121, 47.15614>>,PEDMOVE_SPRINT,DEFAULT_TIME_NEVER_WARP,0.5)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<<2734.74438, 1527.00732, 47.15614>>,FRANK_ID(),PEDMOVE_SPRINT,true)
					TASK_SHOOT_AT_ENTITY(null,FRANK_ID(),-1,FIRING_TYPE_RANDOM_BURSTS)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				icamDelay = GET_GAME_TIMER()				
				mission_substage++
			endif	
		break			
		case 3
		//right at the top
			if (IS_ENTITY_AT_COORD(FRANK_ID(),<< 2741.3936, 1523.6068, 45.5072 >>,<<1.25,1.25,2>>))			
			or (GET_GAME_TIMER() - icamDelay) > 12500
			or (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID())and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2733.8066, 1526.9257, 47.1349 >>,<<2,2,2>>))
			or (IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2733.8066, 1526.9257, 47.1349 >>,<<2,2,2>>))
			or (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 11 and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2733.8066, 1526.9257, 47.1349 >>,<<2,2,2>>))
			or (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID()))
			or (IS_ENTITY_AT_COORD(FRANK_ID(),<< 2755.3, 1556.6, 42.3 >>,<<4,4,4>>))
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())	
				//mike			
				CLEAR_PED_TASKS(MIKE_ID())				
				OPEN_SEQUENCE_TASK(seq)		
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)					
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2738.3992, 1545.0612, 47.1352 >>,PEDMOVE_SPRINT,DEFAULT_TIME_NEVER_WARP,0.5)					
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				mission_substage++
			endif
		break
		case 301 //incase the player goes a different way there needs to be a differnt path for mike.
			//right at the top
			if (IS_ENTITY_AT_COORD(FRANK_ID(),<< 2744.23, 1523.03, 46.08 >>,<<1.25,1.25,2>>))			
			or (GET_GAME_TIMER() - icamDelay > 12500)
			or (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID())and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2733.0586, 1528.9579, 49.6923 >>,<<2,2,2>>))
			or (IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2733.0586, 1528.9579, 49.6923 >>,<<2,2,2>>))
			or (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 11 and IS_ENTITY_AT_COORD(MIKE_ID(),<< 2733.0586, 1528.9579, 49.6923 >>,<<2,2,2>>))
			or (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID()))
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
				//mike			
				CLEAR_PED_TASKS(MIKE_ID())				
				OPEN_SEQUENCE_TASK(seq)							
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<< 2740.29, 1544.70, 49.70 >>,PEDMOVE_SPRINT,DEFAULT_TIME_NEVER_WARP,0.5)					
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				mission_substage = 4
			endif
		break
		case 4
			//For bug fix 1780365
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 10
			AND NOT IS_ENTITY_ON_SCREEN(MIKE_ID())
				remove_blip(peds[mpf_mike].blip)
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2735.8027, 1535.9937, 48.3381 >>)
				endif
				CLEAR_PED_TASKS(MIKE_ID())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(), TRUE)
				SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blip_objective,true)
				SET_ENTITY_COORDS(MIKE_ID(),<< 2750.81, 1579.74, 49.69>>)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				mission_substage++
			ENDIF
			if IS_ENTITY_AT_COORD(MIKE_ID(),<<2736.12, 1537.14, 48.69 >>,<<2,2,6>>)
			//mike		
				remove_blip(peds[mpf_mike].blip)
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2735.8027, 1535.9937, 48.3381 >>)
				endif
				CLEAR_PED_TASKS(MIKE_ID())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_ID(), TRUE)
				SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(blip_objective,true)
				SET_ENTITY_COORDS(MIKE_ID(),<< 2750.81, 1579.74, 49.69>>)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				mission_substage++
			endif
		break
		case 5
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2734.4053, 1529.7186, 46.3205 >>,<<3,3,20>>)
			or IS_ENTITY_AT_COORD(FRANK_ID(),<< 2757.7056, 1539.8588, 44.8806 >>,<<3,3,20>>)
			or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 20
			//blips		
				if not DOES_BLIP_EXIST(peds[mpf_mike].blip)
					peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_ID(),true)
				endif
				if DOES_BLIP_EXIST(blip_objective)
					REMOVE_BLIP(blip_objective)
				endif
			//mike			
				OPEN_SEQUENCE_TASK(seq)		
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null,<<2741.55, 1574.16, 49.70>>,FRANK_ID(),PEDMOVE_SPRINT,true)
					TASK_SHOOT_AT_ENTITY(null,FRANK_ID(),-1,FIRING_TYPE_RANDOM_BURSTS)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(MIKE_ID(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				
	//-------load next stage now ready -----------
		load_asset_stage(msf_6_Ladder_dodge)
	//--------------------------------------------
//				eheliLight = HL_MIKE
				icamDelay = GET_GAME_TIMER()	
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
				mission_substage++
			endif
		break
		case 6
			if GET_GAME_TIMER() - icamDelay > 10000
			or IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),MIKE_ID())
			or IS_PLAYER_TARGETTING_ENTITY(player_id(),MIKE_ID())	
			or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_ID()),GET_ENTITY_COORDS(FRANK_ID())) < 18
			or (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(MIKE_ID(),FRANK_ID()))
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(MIKE_ID())
			//mike		
				TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_ID(),<< 2735.5762, 1576.0760, 49.5361 >>,PEDMOVE_SPRINT,DEFAULT_TIME_NEVER_WARP)					
				SET_PED_MIN_MOVE_BLEND_RATIO(MIKE_ID(),PEDMOVE_SPRINT)
				mission_substage++
			endif
		break
		case 7 
		//Mike reached the ladder
			if IS_ENTITY_AT_COORD(MIKE_ID(),<< 2735.7717, 1576.9320, 51.6>>,<<1.5,1.5,2>>)							
			//cam				
				cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<< 2738.0515, 1574.1177, 51.0774 >>, << -7.4845, 0.9834, 28.0021 >>,67.1294)
				
				SET_CAM_ACTIVE(cameraIndex,true)
				RENDER_SCRIPT_CAMS(true,false)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.6)
				SET_CAM_PARAMS(cameraIndex,<< 2737.6831, 1574.0182, 51.0801 >>, << 3.5197, -1.0329, 37.5864 >>,67.1294,4000,GRAPH_TYPE_LINEAR)		
			//sycnscene mike
				SET_PED_MIN_MOVE_BLEND_RATIO(MIKE_ID(),PEDMOVE_STILL)
				CLEAR_PED_TASKS_IMMEDIATELY(MIKE_ID())
				SyncSceneIG3 = CREATE_SYNCHRONIZED_SCENE(<<2735.436,1576.891,49.518>>,<<0,0,-104.4>>)
				TASK_SYNCHRONIZED_SCENE(MIKE_ID(),SyncSceneIG3,"missfinale_b_ig_3","climb_ladder_mic",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2738.9934, 1575.4868, 52>>)	
					SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
				endif
				if DOES_BLIP_EXIST(peds[mpf_mike].blip)
					REMOVE_BLIP(peds[mpf_mike].blip)
				endif				
				stop_all_audio_scenes(5)
				if not IS_AUDIO_SCENE_ACTIVE("FIN_2_MICHAEL_CLIMBS_LADDER")
					start_audio_Scene("FIN_2_MICHAEL_CLIMBS_LADDER")
				endif
				icamDelay = GET_GAME_TIMER()
				Prep_start_Cutscene(false,<< 2739.4050, 1577.1288, 51.4361 >>)
				bPushInFXPlayed = FALSE
				mission_substage++
			endif
		break
		case 8
//			IF NOT DOES_ENTITY_EXIST(ladderMike)
//				REQUEST_MODEL(PLAYER_ZERO)
//				IF HAS_MODEL_LOADED(PLAYER_ZERO)
//					ladderMike = CREATE_PED(PEDTYPE_MISSION, PLAYER_ZERO, <<2735.4473, 1575.9996, 49.5362>>, 336.5930)
//					TASK_CLIMB_LADDER(ladderMike, TRUE)
//					SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
//				ENDIF
//			ENDIF
				
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG3)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneIG3) >= 0.46
				SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_UNARMED,true)
				//delete gun				
				mission_substage++
			endif
		break	
		case 9
			IF NOT bPushInFXPlayed
			AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON 
			AND GET_GAME_TIMER() - icamDelay >= 4090
				ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				bPushInFXPlayed = TRUE
			ENDIF
		
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG3)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncsceneIG3) >= 1
			or (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) AND NOT bPushInFXPlayed)
				//cam
				Prep_stop_Cutscene(true)
				RENDER_SCRIPT_CAMS(false,false)
				set_entity_heading(FRANK_ID(), GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(FRANK_ID()),GET_ENTITY_COORDS(MIKE_ID())))
				set_gameplay_cam_relative_heading(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(MIKE_ID()))
				set_gameplay_cam_relative_pitch(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(MIKE_ID()))
				//mike 
				CLEAR_PED_TASKS_IMMEDIATELY(MIKE_ID())
				SET_ENTITY_COORDS(MIKE_ID(),<< 2736.2512, 1578.1141, 65.5427 >>)			
				set_entity_heading(MIKE_ID(), 42.3170)
				TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_ID(),<< 2729.6072, 1579.8430, 65.5428 >>,PEDMOVE_RUN)
				FORCE_PED_MOTION_STATE(MIKE_ID(),MS_ON_FOOT_RUN,true,FAUS_CUTSCENE_EXIT)
				stop_all_audio_scenes(5)
				if not IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_UP_LADDER")
					start_audio_Scene("FIN_2_CHASE_UP_LADDER")
				endif
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				mission_substage++
			endif
		break
		case 10
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
			
			if IS_ENTITY_AT_COORD(Mike_ID(),<< 2729.6072, 1579.8430, 65.5428 >>,<<2,2,2>>)			 
				SET_ENTITY_VISIBLE(MIKE_ID(),false)	
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_COORD(<< 2738.9934, 1575.4868, 52>>)	
					SET_BLIP_COLOUR(blip_objective,BLIP_COLOUR_RED)
				endif
				mission_substage++
			endif
		break		
		case 11
			//Fail here if player goes too far away from ladder.
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2745.7, 1573.3, 50>>) > 110
				Mission_Failed(mff_lost_mike)
			ENDIF
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2736.6770, 1575.8693, 50.0683 >>,<<4,4,2>>)
				if DOES_BLIP_EXIST(blip_objective)
					REMOVE_BLIP(blip_objective)	
				endif
				mission_substage = STAGE_ENTRY
				mission_set_stage(msf_6_Ladder_dodge)
			endif
		break
	endswitch	
	
	//Dialogue
	IF doneUPSTChat = FALSE
		IF mission_substage > 9
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					ADD_PED_FOR_DIALOGUE(convo_struct,1,PLAYER_PED_ID(),"FRANKLIN", TRUE)
					IF CREATE_CONVERSATION(convo_struct, "FIN2AUD", "FIN2_UPST", CONV_PRIORITY_MEDIUM)
						//You goin' up there, huh?
						//Why you going up there?
						doneUPSTChat = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC
PROC ST_6_LADDER_DODGE()	
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			
			//Flags 
			cutsceneRequested = FALSE
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Stage 6: Ladder dodge")
			//michael
			//top of ladder pointing down at franklin
			GIVE_WEAPON_TO_PED(MIKE_ID(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
			SET_CURRENT_PED_WEAPON(MIKE_ID(),WEAPONTYPE_PISTOL,true)
			SET_PED_INFINITE_AMMO(MIKE_ID(),true,WEAPONTYPE_PISTOL)
			SET_PED_COMBAT_MOVEMENT(MIKE_ID(),CM_DEFENSIVE)
			SET_PED_SPHERE_DEFENSIVE_AREA(MIKE_ID(),<< 2735.4871, 1578.3522, 65.7129 >>,2)
			TASK_AIM_GUN_AT_ENTITY(MIKE_ID(),FRANK_ID(),-1)
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_CHASE_UP_LADDER")
				start_audio_Scene("FIN_2_CHASE_UP_LADDER")
			endif
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_HELICOPTER_ARRIVES")
				start_audio_scene("FIN_2_HELICOPTER_ARRIVES")
			endif
			icamDelay = GET_GAME_TIMER()	
			iLadderStage = 0
			mission_substage++
		break
		case 1		
			if IS_ENTITY_AT_COORD(FRANK_ID(),<<2735.73, 1576.83, 58.73>>,<<4,4,10>>)
			and IS_PLAYER_CLIMBING(player_id())						
			//mike
				SET_ENTITY_VISIBLE(MIKE_ID(),true)
				SET_ENTITY_COORDS(MIKE_ID(),<< 2735.91, 1577.47, 65.54 >>)
				if iLadderStage = 7
					SET_PED_ACCURACY(MIKE_ID(),0)
					TASK_SHOOT_AT_ENTITY(MIKE_ID(), FRANK_ID(),-1,FIRING_TYPE_RANDOM_BURSTS)
				endif
				FREEZE_ENTITY_POSITION(MIKE_ID(),true)
				CLEAR_PED_TASKS(MIKE_ID())	
				TRIGGER_MUSIC_EVENT("FINB_CLIMB")
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
				mission_substage++			
			ENDIF			
		break 		
		case 2	
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220747
			
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2735.7068, 1576.8636, 59.4745 >>,<<1,1,1>>)
//-------load next stage now ready -----------
	load_asset_stage(msf_7_over_the_Edge)
//--------------------------------------------
				mission_substage++
			endif
		break
		case 3
			if IS_ENTITY_AT_COORD(frank_id(), << 2735.6948, 1576.5291, 55.5211 >>,<<1,1,1>>)
				mission_substage = 1				
			endif
			if IS_ENTITY_AT_COORD(FRANK_ID(),<< 2735.7371, 1576.6788, 64.9689 >>,<<1,1,1.2>>)							
				mission_substage = STAGE_ENTRY
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, FINB_FOOTCHASE_TIME)
				Mission_Set_Stage(msf_7_over_the_Edge)
				mission_substage++
			endif			
		break
		
	endswitch
	
	manage_shooting_from_above()
	
	//Fail mission if player goes back down ladder and tries to leave the area.
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2735.8, 1577, 50>>) > 50
		Mission_Failed(mff_lost_mike)
	ENDIF
	
	IF cutsceneRequested = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2735.8, 1577, 50>>) < 100
			REQUEST_CUTSCENE("fin_B_MCS_2")			
			cutsceneRequested = TRUE
		ENDIF
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2735.8, 1577, 50>>) > 100
			REMOVE_CUTSCENE()	
			cutsceneRequested = FALSE
		ENDIF	
	ENDIF
		
	
	if mission_substage >= 1 and mission_substage < 4
		if not IS_PLAYER_CLIMBING(player_id())			
			SET_ENTITY_VISIBLE(MIKE_ID(),false)		
			FREEZE_ENTITY_POSITION(MIKE_ID(),false)
			if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_ID(),SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_ID(),<< 2729.5681, 1579.2888, 65.5428 >>,PEDMOVE_RUN)
			endif
			mission_substage = 1					
		endif
	endif		
ENDPROC
proc Manage_cam_angles()

	switch icam
		case 0
			//SET_CAM_PARAMS(cameraIndex,<< 2727.7112, 1575.7562, 67.6807 >>, << -62.3590, -7.3807, 35.3189 >>,62,3000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR) //BUG FIX 1303726
			icamDelay = GET_GAME_TIMER()
			icam = 2//icam++  BUG FIX 1303726 just bypassing this straight to case 2 so it doesn't repeat case 1 now that that camera is to be 1st shot immediately after cutscene.
		break
		case 1
			if get_game_timer() - icamdelay > 2500				
				SET_CAM_PARAMS(cameraIndex,<< 2686.6641, 1578.0531, 23.6635 >>, << 39.7643, -17.7609, -90.8670 >>,22.6486,0)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.8)
				icamDelay = GET_GAME_TIMER()
				icam++
			endif
		break
		case 2
			if get_game_timer() - icamdelay > 3500
				SET_CAM_PARAMS(cameraIndex,<< 2726.6614, 1577.7256, 65.2210 >>, << 23.2568, 7.2233, -107.1534 >>,68.8743,0)
				SET_CAM_PARAMS(cameraIndex,<< 2726.7603, 1578.2632, 65.2330 >>, << 23.2568, 7.2233, -107.1534 >>,68.8743,5000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)			
				icamDelay = GET_GAME_TIMER()
				icam++
			endif
		break
		case 3
//			if get_game_timer() - icamdelay > 4000
//				SET_CAM_FOV(cameraIndex,47)
//				ATTACH_CAM_TO_ENTITY(cameraIndex, vehs[mvf_heli].id, <<-1.8529, -3.0518, -1.5364>>)
//				POINT_CAM_AT_ENTITY(cameraIndex,FRANK_ID(),<<0,0,0>>)				
//				icamDelay = GET_GAME_TIMER()
				icam++
//			endif
		break 
		case 4
			if get_game_timer() - icamdelay > 6000
				STOP_CAM_POINTING(cameraIndex)
				DETACH_CAM(cameraIndex)				
				SET_CAM_PARAMS(cameraIndex,<< 2726.6204, 1575.2729, 67.2784 >>, << -27.9599, 7.2233, -28.8742 >>,45,0)
				SET_CAM_PARAMS(cameraIndex,<< 2726.9519, 1575.1512, 67.3749 >>, << -27.9599, 7.2233, -11.0954 >>,45,5000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)				
				icamDelay = GET_GAME_TIMER()
				icam++
			endif
		break
		case 5
			if get_game_timer() - icamdelay > 6000	
				icam = 1
			endif
		break
	endswitch
#IF IS_DEBUG_BUILD
  IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
//        OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(vehs[mvf_heli].id)
  ENDIF
#ENDIF

endproc
PROC ST_7_OVER_THE_EDGE()

	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_KillMichael") 

	switch mission_substage
		case STAGE_ENTRY					
			if not IS_AUDIO_SCENE_ACTIVE("FIN_2_HELICOPTER_ARRIVES")
				start_audio_scene("FIN_2_HELICOPTER_ARRIVES")
			endif
			
			//For bug 1847954
			SUPPRESS_PICKUP_REWARD_TYPE(PICKUP_REWARD_TYPE_WEAPON, FALSE)
			
			CLEAR_PLAYER_WANTED_LEVEL(player_id())
			SET_WANTED_LEVEL_MULTIPLIER(0)
			REQUEST_CUTSCENE("fin_B_MCS_2")
//			SET_CS_OUTFITS_AND_EXITS()
			creditsStarted = FALSE
			bcs_frank		= false
			bcs_mike		= false
			bcs_mikeCar		= false
			bcs_gun			= false
			endCutRequested = FALSE
			pain1Done		= FALSE
			pain2Done		= FALSE
			pain3Done		= FALSE
			damagePack1Done	= FALSE
			damagePack2Done	= FALSE
			doneChatHold = FALSE
			scriptCamsStopped = FALSE
			syncSceneStarted = FALSE
//			bMikeMoved 		= FALSE
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7,"Stage 7: over the Edge",true)		
			KILL_ANY_CONVERSATION()
			icam = 0
			mission_substage++
		break
		case 1 
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()	
				CLEAR_PED_BLOOD_DAMAGE(MIKE_ID())
				REGISTER_ENTITY_FOR_CUTSCENE(MIKE_ID(),"Michael",CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_FACE_BLOOD_DAMAGE|CEO_PRESERVE_BODY_BLOOD_DAMAGE)		//BUG FIX 1405439
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(),"Franklin",CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_FACE_BLOOD_DAMAGE|CEO_PRESERVE_BODY_BLOOD_DAMAGE)	//BUG FIX 1405439
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)	// Block FP flash for url:bugstar:2057705
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				HANG_UP_AND_PUT_AWAY_PHONE()
				mission_substage++
			endif
		break		
		case 2
			if IS_CUTSCENE_PLAYING()
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				CLEAR_PED_PROP(peds[mpf_mike].id, ANCHOR_HEAD)
				if IS_SCREEN_FADED_OUT()			
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				TRIGGER_MUSIC_EVENT("FINB_TOWER")
				stop_all_audio_Scenes(5)
				CLEAR_AREA(<< 2735.7068, 1576.8636, 59.4745 >>,50,true)
				CLEAR_AREA_OF_COPS(<< 2735.7068, 1576.8636, 59.4745 >>,50)
				CLEAR_AREA_OF_PEDS(<< 2735.7068, 1576.8636, 59.4745 >>,50)
				CLEAR_PLAYER_WANTED_LEVEL(player_id())
				bPunch1 = false
				bPunch2 = false
				bVideoCaptured = false
				mission_substage++
			endif
		break
		case 3
			
			//Add a video capture moment for him getting flung over the balcony.
			IF bVideoCaptured = FALSE
				if IS_CUTSCENE_PLAYING()
					IF GET_CUTSCENE_TIME() > 83500
						REPLAY_RECORD_BACK_FOR_TIME(6.0)
						bVideoCaptured = TRUE
					ENDIF
				ENDIF
			ENDIF
					
			if not bPunch1
				if HAS_ANIM_EVENT_FIRED(MIKE_ID(),GET_HASH_KEY("1stPunch"))
					println("1stPunch")
//					APPLY_PED_BLOOD_BY_ZONE(MIKE_ID(),ENUM_TO_INT(PDZ_HEAD),0.512,0.640,"BulletSmall")
					bPunch1 = true
				endif
			endif
			if not bPunch2
				if HAS_ANIM_EVENT_FIRED(MIKE_ID(),GET_HASH_KEY("2ndPunch"))
					println("2ndPunch")
//					APPLY_PED_BLOOD_BY_ZONE(MIKE_ID(),ENUM_TO_INT(PDZ_HEAD),0.572,0.680,"BulletSmall")
//					APPLY_PED_DAMAGE_DECAL(MIKE_ID(),PDZ_HEAD,0.582,0.692,0,0.637,-1,-1,false,"bruise")
//					APPLY_PED_DAMAGE_DECAL(MIKE_ID(),PDZ_HEAD,0.582,0.692,0,0.637,-1,-1,false,"bruise")
//					APPLY_PED_DAMAGE_DECAL(MIKE_ID(),PDZ_HEAD,0.582,0.692,0,0.637,-1,-1,false,"bruise")
//					APPLY_PED_DAMAGE_DECAL(MIKE_ID(), PDZ_HEAD, 0.6150000, 0.490000, 180.000000, 0.5, -1, 1, false, "stab")
					APPLY_PED_DAMAGE_PACK(MIKE_ID(),"SCR_Finale_Michael_Face",0,1)
					bPunch2 = true
				endif
			endif
					
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				//cam
				cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 2686.6641, 1578.0531, 23.6635 >>, << 39.7643, -17.7609, -90.8670 >>,22.6486) //<< 2727.9268, 1575.4513, 68.3937 >>, << -62.3590, -7.3807, 35.3189 >>,45.0000) //BUG FIX 1303726
				SET_CAM_ACTIVE(cameraIndex,true)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.8)//BUG FIX 1303726
				Prep_start_Cutscene(false,<< 2726.2207, 1579.3988, 91.7594 >>)	
				if not IS_AUDIO_SCENE_ACTIVE("FIN_2_MAKE_CHOICE")
					start_audio_Scene("FIN_2_MAKE_CHOICE")
				endif
				bcs_frank = true
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
				
				SETUP_PC_CONTROLS() // Add special PC controls for final choice
				 
				REPLAY_STOP_EVENT()
				 
				mission_substage++			
			endif
		break
		case 4 
			
			//Call every frame to hide the sniper pick up created from another script.
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(eModel) //Bug fix 1847954 to stop the sniper rifle pick up spawning in.

			IF syncSceneStarted = FALSE
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							
				SyncSceneIG5 = CREATE_SYNCHRONIZED_SCENE(<<2727.982,1576.869,65.250>>,<<0,0,179.990>>)
				//frank
				TASK_SYNCHRONIZED_SCENE(FRANK_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_loop_fra",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK_ID())
				//mike
				TASK_SYNCHRONIZED_SCENE(Mike_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_loop_mic",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(Mike_ID())		
				syncSceneStarted = TRUE
			ENDIF
			
			IF endCutRequested = FALSE
				IF NOT IS_CUTSCENE_ACTIVE()
					//Request the end cutscene now
					REQUEST_CUTSCENE("fin_b_ext")
					endCutRequested = TRUE
				ENDIF
			ENDIF
			
			Manage_cam_angles()
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 1
				SyncSceneIG5 = CREATE_SYNCHRONIZED_SCENE(<<2727.982,1576.869,65.250>>,<<0,0,179.990>>)
				//frank
				TASK_SYNCHRONIZED_SCENE(FRANK_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_loop_fra",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
				//mike
				TASK_SYNCHRONIZED_SCENE(Mike_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_loop_mic",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)				
			endif
			//Only allow the drop to happen once the cutscene has loaded
			IF endCutRequested = TRUE
			AND HAS_CUTSCENE_LOADED()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FIN2_DROP")
					PRINT_HELP_FOREVER("FIN2_DROP")
				ENDIF
				if IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RDOWN) // drop	
				OR (doneChatHold = TRUE AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
	//				SyncSceneIG5 = CREATE_SYNCHRONIZED_SCENE(<<2727.982,1576.869,65.250>>,<<0,0,179.990>>)
	//				//frank
	//				TASK_SYNCHRONIZED_SCENE(FRANK_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_drop_fra",normal_BLEND_IN,walk_BLEND_OUT)
	//				//mike
	//				TASK_SYNCHRONIZED_SCENE(Mike_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_drop_mic",normal_BLEND_IN,NORMAL_BLEND_OUT)				
					// misc
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CLEAR_PRINTS()
					CLEAR_HELP()
					//cam
//					STOP_CAM_POINTING(cameraIndex)
//					DETACH_CAM(cameraIndex)	
//					SET_CAM_PARAMS(cameraIndex,<< 2725.6858, 1583.9159, 67.5568 >>, << -11.1217, 2.3208, -160.0679 >>,17.5411,0)
//					SET_CAM_PARAMS(cameraIndex,<< 2725.2576, 1583.7570, 67.5387 >>, << -11.1217, 2.3208, -159.0268 >>,17.5411,6000)			
					//audio
					STOP_all_AUDIO_SCENES(5)
					TRIGGER_MUSIC_EVENT ("FINB_CHOOSE")
					
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
					
					if not IS_AUDIO_SCENE_ACTIVE("FIN_2_KILL_MICHAEL")
						start_audio_Scene("FIN_2_KILL_MICHAEL")
					endif	
					icam = 4
					mission_substage++
				elif IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SCRIPT_RRIGHT) //save
					//Remove the original cutscene and load in the other one so it starts on section 2
					REMOVE_CUTSCENE()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CLEAR_PRINTS()
					CLEAR_HELP()
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fin_b_ext", CS_SECTION_2|CS_SECTION_3|CS_SECTION_4|CS_SECTION_5|CS_SECTION_6|CS_SECTION_7|CS_SECTION_8|CS_SECTION_9)
					
					SyncSceneIG5 = CREATE_SYNCHRONIZED_SCENE(<<2727.982,1576.869,65.250>>,<<0,0,179.990>>)
					//frank
					TASK_SYNCHRONIZED_SCENE(FRANK_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_headbut_fra",INSTANT_BLEND_IN,walk_BLEND_OUT)
					//mike
					TASK_SYNCHRONIZED_SCENE(Mike_ID(),SyncSceneIG5,"missfinale_b_ig_5","hold_michael_headbut_mic",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					//cam
					STOP_CAM_POINTING(cameraIndex)
					DETACH_CAM(cameraIndex)
					SET_CAM_PARAMS(cameraIndex,<< 2725.6858, 1583.9159, 67.5568 >>, << -11.1217, 2.3208, -160.0679 >>,17.5411,0)
					SET_CAM_PARAMS(cameraIndex,<< 2725.2576, 1583.7570, 67.5387 >>, << -11.1217, 2.3208, -159.0268 >>,17.5411,6000)
					//audio
					STOP_all_AUDIO_SCENES(5)
					TRIGGER_MUSIC_EVENT ("FINB_CHOOSE")
					if not IS_AUDIO_SCENE_ACTIVE("FIN_2_SAVE_MICHAEL")
						start_audio_Scene("FIN_2_SAVE_MICHAEL")
					endif
					
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
					
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FINB_CHOICE) 
					icam = 4
					mission_substage = 401
				endif
			endif
			
			//Have some dialogue between them here.
			IF doneChatHold = FALSE
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						ADD_PED_FOR_DIALOGUE(convo_struct,0,MIKE_ID(),"MICHAEL", TRUE)
						ADD_PED_FOR_DIALOGUE(convo_struct,1,PLAYER_PED_ID(),"FRANKLIN", TRUE)
						IF CREATE_CONVERSATION(convo_struct, "FIN2AUD", "FIN2_HOLD", CONV_PRIORITY_MEDIUM)
							//Eh, you told me, when the time came, I'd know. I'm sorry! 
							//You hypocrite!  
							//I'm sorry. It's like you said... I understand.
							//You don't understand nothing!
							//I'm all you got!
							//You made the wrong call!
							//I'm all you got!
							//You made the wrong call!
							//Argh!
							//I'll bring you with me!
							//I'm taking you down!
							//You're coming in the ground with me!
							//Franklin!
							//Greedy prick!
							doneChatHold = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		break
		case 401
			STOP_CAM_POINTING(cameraIndex)
			DETACH_CAM(cameraIndex)	
			//Play pain sounds 
			IF pain1Done = FALSE
				if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
				and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.257
					PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_CLIMB_LARGE)
					pain1Done = TRUE
				ENDIF
			ENDIF
				
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.683				
				SET_CAM_PARAMS(cameraIndex,<< 2727.4436, 1574.4738, 67.0238 >>, << -12.9643, 2.3208, -6.2610 >>,45,0)
				SET_CAM_PARAMS(cameraIndex,<< 2727.6797, 1574.8011, 67.5812 >>, << -28.1126, 2.3208, 3.7272 >>,45,1400)				
				mission_substage=402
			endif
		break
		case 402
			//Play pain sounds 
			IF pain2Done = FALSE
				if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
				and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.704
					PLAY_PAIN(PLAYER_PED_ID(), AUD_DAMAGE_REASON_CLIMB_LARGE)
					pain2Done = TRUE
				ENDIF
			ENDIF
			//Play pain sounds 
			IF pain3Done = FALSE
				if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
				and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.825
					IF DOES_ENTITY_EXIST(Mike_ID())
						IF NOT IS_PED_INJURED(Mike_ID())
							PLAY_PAIN(Mike_ID(), AUD_DAMAGE_REASON_CLIMB_LARGE)
							pain3Done = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//added extra pack for Franklin, see B*1862063
			IF damagePack1Done = FALSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
				AND GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.885
					APPLY_PED_DAMAGE_PACK(PLAYER_PED_ID(), "SCR_Franklin_finb", 0.0, 1.0)
					damagePack1Done = TRUE
				ENDIF
			ENDIF
			
			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 1			
				mission_substage=5
			ENDIF
		break
		case 5			
//			STOP_CAM_POINTING(cameraIndex)
//			DETACH_CAM(cameraIndex)	
//			//Reposition Michael just before the camera switch so he is not seen being moved.
//			IF bMikeMoved = FALSE
//				if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
//				and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.35
//					CLEAR_PED_TASKS_IMMEDIATELY(MIKE_ID())
//					set_entity_coords(MIKE_ID(),<< 2727.42407, 1581.22046, 49.49731 >>)
//					REMOVE_ALL_PED_WEAPONS(MIKE_ID())	
//					SET_ENTITY_INVINCIBLE(MIKE_ID(), FALSE)
//					SET_ENTITY_HEALTH(MIKE_ID(), 105)
//					bMikeMoved = TRUE
//				ENDIF
//			ENDIF
//			if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIG5)
//			and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIG5) >= 0.8
//				//cam
//				SET_CAM_PARAMS(cameraIndex,<<2727.4807, 1578.6859, 73.0591>>, <<-87.5858, -46.6135, 15.8136>>,17.4602,0)
//				SET_CAM_PARAMS(cameraIndex,<<2727.3752, 1579.0480, 64.1485>>, <<-87.5858, -46.6135, 15.8136>>,17.4602,4000)
////				REQUEST_CUTSCENE("FIN_B_EXT")
////				SET_CS_OUTFITS_AND_EXITS()
//				bPlaceholder = true
//				mission_substage++
//				icamDelay = GET_GAME_TIMER()
//			endif
			bloodAdded = FALSE
			
			REGISTER_ENTITY_FOR_CUTSCENE(MIKE_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DEFAULT, CEO_PRESERVE_FACE_BLOOD_DAMAGE)
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF NOT DOES_CAM_EXIST(EndCreditCam)
				EndCreditCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				SET_CAM_PARAMS(EndCreditCam, <<2689.549561,1549.860962,29.784725>>,<<8.000013,-0.000006,177.999985>>,40.000000)
			ENDIF
			
			REQUEST_ADDITIONAL_TEXT("CREDIT", MISSION_TEXT_SLOT)
			
			START_CUTSCENE()
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			bVideoCaptured = FALSE
			mission_substage++

		break
		case 6
//		//panning heli crash
//			if (GET_GAME_TIMER() - icamDelay) > 4000
//				SET_CAM_PARAMS(cameraIndex,<<2657.0061, 1589.9597, 32.4828>>, <<-7.4921, 5.7494, -106.6028>>,30.4406,0)
//				SET_CAM_PARAMS(cameraIndex, <<2656.2671, 1585.8317, 32.1422>>, <<-8.1866, 1.0515, -96.9596>>,30.4406,8000)
//				icamDelay = GET_GAME_TIMER()
//				mission_substage++
//			endif
			IF IS_CUTSCENE_PLAYING()
				
				//Add a video capture moment from the shot of Michael lying dead on the ground
				IF bVideoCaptured = FALSE
					IF GET_CUTSCENE_TIME() > 8200
						REPLAY_RECORD_BACK_FOR_TIME(10.0)
						bVideoCaptured = TRUE
					ENDIF
				ENDIF
			
				IF bloodAdded = FALSE
					IF HAS_ANIM_EVENT_FIRED(MIKE_ID(),GET_HASH_KEY("Michael_Fucked"))
//						decalBlood[0] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<2727.3877, 1578.0725, 23.5010>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
//						WAIT(0)
//						decalBlood[1] = ADD_DECAL(DECAL_RSID_BLOOD_SPLATTER, <<2727.4, 1578.4, 23.5010>>, << 0.0, 0.0, -1.0 >>, NORMALISE_VECTOR(<< 0.0, 1.0, 0.0 >>), 1.0, 1.0, 0.196, 0, 0, 1.0, -1)
						APPLY_PED_DAMAGE_PACK(MIKE_ID(),"SCR_Finale_Michael",0,1)
						bloodAdded = TRUE
					ENDIF
				ENDIF
				
				//added extra pack for Franklin, see B*1862063
				IF damagePack2Done = FALSE
					IF damagePack1Done = TRUE
						IF GET_CUTSCENE_TIME() > 7500
							APPLY_PED_DAMAGE_PACK(PLAYER_PED_ID(), "SCR_Franklin_finb2", 0.0, 1.0)
							damagePack2Done = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF scriptCamsStopped = FALSE
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_CAM_ACTIVE(EndCreditCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					scriptCamsStopped = TRUE
				ENDIF
				
//				IF GET_CUTSCENE_TIME() > 64700
//					IF NOT IS_SCREEN_FADED_OUT()
//						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
//					ENDIF
//				ENDIF
				
				//Stop player from being able to skip cutscene from this point on
				IF GET_CUTSCENE_TIME() > 46000
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					REPLAY_STOP_EVENT()
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<2689.5, 1383, 23.9>>, PEDMOVEBLENDRATIO_WALK, -1)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK) 
				ENDIF
				
				//Start end credits music here
				IF g_bFinaleCreditsPlaylistStarted = FALSE
					IF GET_CUTSCENE_TIME() > 46710 + 9666
						PLAY_END_CREDITS_MUSIC(TRUE)
						SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)

						SET_MOBILE_PHONE_RADIO_STATE(TRUE)

						SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")

						SET_CUSTOM_RADIO_TRACK_LIST("RADIO_01_CLASS_ROCK", "END_CREDITS_KILL_MICHAEL", TRUE)
						g_bFinaleCreditsPlaylistStarted = TRUE
					ENDIF
				ENDIF
				
				IF creditsStarted = FALSE
					IF GET_CUTSCENE_TIME() >63352//61686// 62500
					//OR NOT IS_CUTSCENE_ACTIVE()
			  			SET_CREDITS_ACTIVE(TRUE)
			  			SET_CREDITS_FADE_OUT_WITH_SCREEN(FALSE)         
			  			START_AUDIO_SCENE("END_CREDITS_SCENE")
						SET_GAME_PAUSES_FOR_STREAMING(FALSE)
						AWARD_ACHIEVEMENT_FOR_MISSION(ACH04) // To live or die in los santos
						creditsStarted = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			
//			IF IS_SCREEN_FADED_OUT()
			IF NOT IS_CUTSCENE_ACTIVE()
				mission_substage++
//				Mission_Passed()
			ENDIF

		break
		case 7
			
			SHAKE_CAM(EndCreditCam, "HAND_SHAKE", 0.1)
			SETTIMERA(0)
			mission_substage++
			
		break
		case 8
			
			IF TIMERA() > 45000
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(5000)
				ENDIF
				IF IS_SCREEN_FADED_OUT()
								
					Mission_Passed()
				ENDIF
			ENDIF
		
		break
		
//		case 7
//			if (GET_GAME_TIMER() - icamDelay) > 5000
//				SET_CAM_PARAMS(cameraIndex,<< 2729.8484, 1577.9122, 68.6455 >>, << -41.6983, -0.0000, 105.8610 >>,30,0)
//				SET_CAM_PARAMS(cameraIndex,<< 2730.2754, 1576.2191, 68.6455 >>, << -41.6983, -0.0000, 90.3166 >>,30,12000)				
//				icamDelay = GET_GAME_TIMER()
//				mission_substage++
//			endif
//		break
//		case 8
//			if (GET_GAME_TIMER() - icamDelay) > 6000
//				prep_stop_cutscene(true,false,true)
//				SET_POLICE_IGNORE_PLAYER(player_id(),true)				
//				Point_Gameplay_cam_at_heading(96.0485)	
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-51.2113)
//				CLEAR_PED_TASKS(FRANK_ID())
//				//BUG FIX 1411536 Tell player to leave the area
//				CLEAR_PRINTS()
//				PRINT_NOW("FIN2_GOD1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Leave the area.
//				bfailchecks = FALSE //BUG FIX setting this to false to stop the mission from failing if you get too far from Michael's car in next substage.
//				bOnFoot	= FALSE //BUG FIX to allow player to get in a vehicle to leave area.
//				mission_substage++
//			endif
//		break
//		case 9
//			//Trigger phone call if player gets over 100m's away or once he's at bottom of the stairs.  BUG FIX 1425859
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2727.6, 1580.1, 70>>) > 100
//			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2735.5, 1576.7, 49.5>>, <<3,3,3>>)
//				mission_substage++
//			ENDIF
//		break
//		case 10
//			ADD_PED_FOR_DIALOGUE(convo_struct,1,FRANK_ID(),"FRANKLIN")
//			ADD_PED_FOR_DIALOGUE(convo_struct,4,NULL,"LAMAR")
//			if PLAYER_CALL_CHAR_CELLPHONE(convo_struct,CHAR_LAMAR,"FIN2AUD","FIN2_EXT",CONV_PRIORITY_MEDIUM)
//				mission_substage++		
//			endif
//		break
//		case 11
//			//BUG FIX fade the screen out at the end before the credits roll
////			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<2727.6, 1580.1, 70>>) > 100
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_SCREEN_FADED_OUT()
//					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
//					mission_substage++
//				ENDIF
//			ENDIF
//		break
//		case 12
//			IF IS_SCREEN_FADED_OUT()
//				Mission_Passed()
//			ENDIF		
//		break
	endswitch
	
	DISPLAY_AMMO_THIS_FRAME(false)
ENDPROC
Proc ST_8_PASSED()
	switch mission_substage
		case STAGE_ENTRY
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			endif
			mission_substage++
		break
		case 1
			IF IS_SCREEN_FADED_OUT()
				Mission_Passed()
			ENDIF
		break
	endswitch
endproc
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		case msf_0_meet 			ST_0_MEET() 			break
		case msf_1_chase			ST_1_CHASE() 			break
		case msf_2_power_plant		ST_2_POWERPLANT() 		break
		case msf_4_heli_appears 	ST_4_HELI()	 			break
		case msf_3_confrontation 	ST_3_CONFRONTATION() 	break
		case msf_5_shoot_out 		ST_5_SHOOT_OUT()		break
		case msf_6_Ladder_dodge 	ST_6_LADDER_DODGE()		break
		case msf_7_over_the_Edge	ST_7_OVER_THE_EDGE()	break
		case msf_8_passed			ST_8_PASSED()			break
	endswitch
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()		
		ENDIF
	ENDPROC
#endif
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...finale 2 Mission Launched")
	PRINTNL()

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...finale 2 Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		if IS_CUTSCENE_ACTIVE()
			SET_CUTSCENE_FADE_VALUES()	
		endif
		Mission_Cleanup()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	REQUEST_MISSION_AUDIO_BANK("FINALE_B_GENERAL")
	
	ADD_RELATIONSHIP_GROUP("michael_rel", REL_Michael)
	
	//load text
	REQUEST_ADDITIONAL_TEXT("FINALE2",MISSION_TEXT_SLOT)
	if not HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		wait(0)
	endif

#if IS_DEBUG_BUILD
	//z menu for skipping stages
	zMenuNames[msf_0_meet].sTxtLabel 			=	"Stage 0: meet"
	zMenuNames[msf_1_chase].sTxtLabel 			=	"Stage 1: chase"
	zMenuNames[msf_2_power_plant].sTxtLabel 	=	"Stage 2: power plant"
	zMenuNames[msf_3_confrontation].sTxtLabel 	=	"Stage 3: confrontation"
	zMenuNames[msf_4_heli_appears].sTxtLabel 	=	"Stage 4: heli appears"
	zMenuNames[msf_5_shoot_out].sTxtLabel 		=	"Stage 5: shoot out"
	zMenuNames[msf_6_Ladder_dodge].sTxtLabel 	=	"Stage 6: Ladder dodge"
	zMenuNames[msf_7_over_the_Edge].sTxtLabel 	=	"Stage 7: over the Edge"
	zMenuNames[msf_8_Passed].sTxtLabel 			= 	"----------- PASSED -----------"
	
	zMenuNames[CST_MCS_1].sTxtLabel    	= "Stage 0: MCS_1"
	zMenuNames[CST_MCS_1].bSelectable  	= false
	zMenuNames[CST_MCS_2].sTxtLabel   	= "Stage 7: MCS_2"
	zMenuNames[CST_MCS_2].bSelectable 	= false
	zMenuNames[CST_EXT].sTxtLabel   	= "Exit Cutscene"
	zMenuNames[CST_EXT].bSelectable 	= false
	
	widget_debug = START_WIDGET_GROUP("Finale 2 Menu")
	//debug position
	ADD_WIDGET_FLOAT_SLIDER("vDebugPosition.x", vDebugPosition.x, -6000.0, 6000.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("vDebugPosition.y", vDebugPosition.y, -6000.0, 6000.0, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("vDebugPosition.z", vDebugPosition.z, -6000.0, 6000.0, 0.01)	
	ADD_WIDGET_FLOAT_SLIDER("fDebugheading", fDebugheading, 0.0, 360.0, 0.01)
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)	
	
#endif
		
	if not IS_REPLAY_IN_PROGRESS()
		timelapse()
	endif
	
	//initialize mission
	MISSION_SETUP()	
	
	WHILE (TRUE)
	
		//For video recorder 
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_KillMichael") 
	
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)	
		
		Mission_stage_management()
		MISSION_STAGE_SKIP()	
		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif 			
				
#IF IS_DEBUG_BUILD
	DO_DEBUG()
#ENDIF	
	
		WAIT(0)
	
	ENDWHILE
ENDSCRIPT
