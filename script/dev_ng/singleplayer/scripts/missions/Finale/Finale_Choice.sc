//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 25/07/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						Finale Choice Control Script							│
//│																				│
//│			Manages allowing the player as Franklin to phone a specific			│
//│			contact to make a choice of who he wants to kill.					│
//╘═════════════════════════════════════════════════════════════════════════════╛

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_public_game.sch"
USING "cellphone_public.sch"
USING "player_ped_public.sch"
USING "comms_control_public.sch"

BOOL bBlockPlayer = TRUE

PROC SCRIPT_CLEANUP()
	//Clean up any chat calls that are still queued.
	CANCEL_COMMUNICATION(CALL_FIN_CHOICE_1A)
	CANCEL_COMMUNICATION(CALL_FIN_CHOICE_1B)
	CANCEL_COMMUNICATION(CALL_FIN_CHOICE_2A)
	CANCEL_COMMUNICATION(CALL_FIN_CHOICE_2B)
	CANCEL_COMMUNICATION(CALL_FIN_CHOICE_3)

	g_QuickSaveDisabledByScript = FALSE
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Cleaned up.")
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Forced to cleanup (DEBUG or SP_TO_MP)")
		SCRIPT_CLEANUP()
	ENDIF
	
	Register_Script_To_Relaunch_List(LAUNCH_BIT_FLOW_FINALE_CHOICE)
	CLEAR_GLOBAL_COMMUNICATION_DELAY()
	CLEAR_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_FRANKLIN)
	CLEAR_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_MICHAEL)
	CLEAR_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_TREVOR)
	CLEAR_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_LESTER_DEATHWISH)
	CLEAR_COMMUNICATION_DELAY_FOR_CHARACTER(CHAR_LESTER)
	
	g_QuickSaveDisabledByScript = TRUE
	CLEAR_AUTOSAVE_REQUESTS()

	//Register chat calls for Franklin to make.
	CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Registered chat calls with comms controller.")
	IF NOT IS_COMMUNICATION_REGISTERED(CALL_FIN_CHOICE_1A)
		REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CALL_FIN_CHOICE_1A, BIT_FRANKLIN, CHAR_TREVOR, 2, 1,FLOW_CHECK_NONE,CPR_VERY_HIGH)
	ENDIF
	IF NOT IS_COMMUNICATION_REGISTERED(CALL_FIN_CHOICE_2A)
		REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CALL_FIN_CHOICE_2A, BIT_FRANKLIN, CHAR_MICHAEL, 0, 1,FLOW_CHECK_NONE,CPR_VERY_HIGH)
	ENDIF
	IF NOT IS_COMMUNICATION_REGISTERED(CALL_FIN_CHOICE_3)
		REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CALL_FIN_CHOICE_3, BIT_FRANKLIN, CHAR_LESTER, 3, 1,FLOW_CHECK_NONE,CPR_VERY_HIGH)
	ENDIF
	
	//If in debug build delay bringing the phone up until the gameflow is stable as we may be gameflow launching.
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Starting to wait for flow controller to be stable.")
		WHILE g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow
			WAIT(2000)
		ENDWHILE
		CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Finished waiting for flow controller to be stable.")
	#ENDIF
	
	BOOL bChoiceOnScreen = FALSE
	WHILE GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_INTRO)
	AND NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE)		
				
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_FALLING(PLAYER_PED_ID())
					AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
					AND NOT IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
						
						IF NOT bChoiceOnScreen
							IF NOT (Is_Player_Timetable_Scene_In_Progress() OR IS_PLAYER_SWITCH_IN_PROGRESS() OR IS_PLAYER_PED_SWITCH_IN_PROGRESS())
								IF LAUNCH_FINALE_CHOICE_CONTACT_LIST()
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice not set as made yet and contact list not on screen.")
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Bringing up finale choice contact list.")
									bChoiceOnScreen = TRUE
								ENDIF
							ENDIF
						ELSE
							IF bBlockPlayer
								CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> bBlockPlayer is true.")
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REENABLE_CONTROL_ON_DEATH|SPC_LEAVE_CAMERA_CONTROL_ON)
								SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_STILL)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_DOWN)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_UP)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_LEFT)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_RIGHT)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_OPTION)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)
								ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
							ELSE
								CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> bBlockPlayer is false.")
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							ENDIF

							SWITCH GET_LAST_COMPLETED_CALL()
								CASE CALL_FIN_CHOICE_1A
									IF NOT IS_COMMUNICATION_REGISTERED(CALL_FIN_CHOICE_1B)
										CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice A call 1 complete. Queuing call 2.")
										REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(	CALL_FIN_CHOICE_1B,
																				CT_END_OF_MISSION, 
																				CHAR_FRANKLIN, 
																				CHAR_MICHAEL, 
																				1, 
																				CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME, 
																				VID_BLANK,
																				CID_BLANK,
																				FLOW_CHECK_NONE,
																				COMM_FLAG_UNLOCKS_MISSION|COMM_FLAG_DONT_SAVE)
										
									ENDIF
								BREAK
								CASE CALL_FIN_CHOICE_1B
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice A calls complete. Locking in gameplay choice A.")
									SET_MISSION_FLOW_INT_VALUE(FLOWINT_MISSION_CHOICE_FINALE, 0)
									SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE, TRUE)
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Setting stat SP_FINAL_DECISION to 2 for choosing to kill Trevor.")
									STAT_SET_INT(SP_FINAL_DECISION, 2)
								BREAK
								CASE CALL_FIN_CHOICE_2A
									IF NOT IS_COMMUNICATION_REGISTERED(CALL_FIN_CHOICE_2B)
										CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice B call 1 complete. Queuing call 2.")
										REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(	CALL_FIN_CHOICE_2B,
																				CT_END_OF_MISSION, 
																				CHAR_FRANKLIN, 
																				CHAR_TREVOR, 
																				2, 
																				CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME,
																				VID_BLANK,
																				CID_BLANK,
																				FLOW_CHECK_NONE,
																				COMM_FLAG_UNLOCKS_MISSION|COMM_FLAG_DONT_SAVE)
									ENDIF
								BREAK
								CASE CALL_FIN_CHOICE_2B
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice B calls complete. Locking in gameplay choice B.")
									SET_MISSION_FLOW_INT_VALUE(FLOWINT_MISSION_CHOICE_FINALE, 1)
									SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE, TRUE)
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Setting stat SP_FINAL_DECISION to 1 for choosing to kill Michael.")
									STAT_SET_INT(SP_FINAL_DECISION, 1)
								BREAK
								CASE CALL_FIN_CHOICE_3
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice C call complete. Locking in gameplay choice C.")
									SET_MISSION_FLOW_INT_VALUE(FLOWINT_MISSION_CHOICE_FINALE, 2)
									SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FINAL_CHOICE_MADE, TRUE)
									CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Setting stat SP_FINAL_DECISION to 3 for choosing to save Michael and Trevor.")
									STAT_SET_INT(SP_FINAL_DECISION, 3)
								BREAK
								DEFAULT
									IF NOT IS_PHONE_ONSCREEN()
										CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Phone is off-screen while being flagged as on-screen. Phone must have been forced away.")
										bChoiceOnScreen = FALSE
									ELSE
										IF bBlockPlayer
											IF IS_CALLING_ANY_CONTACT()
												bBlockPlayer = FALSE
											ENDIF
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_FLOW, "<FIN_CHOICE> Choice made sucessfully. Terminating controller.")
	Remove_Script_From_Relaunch_List(LAUNCH_BIT_FLOW_FINALE_CHOICE)
	g_sAutosaveData.bFlushAutosaves = FALSE
	MAKE_AUTOSAVE_REQUEST()
	SCRIPT_CLEANUP()
	
ENDSCRIPT
