
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	FINALE1.sc
//		AUTHOR			:	Ben Barclay
//		DESCRIPTION		:	Franklin has decided to kill Trevor, He confronts him and ends
//							up chasing him. Micheal turns up and forces Trevor to crash into
//							a Liquid nitrogen storage truck. Trevor is frozen and Micheal and 
//							smashes trevor with a 2x4. Franklin and Micheal say their
//							goodbyes.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
 
USING "replay_public.sch"
USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"
USING "cheat_controller_public.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
using "completionpercentage_public.sch"
USING "taxi_functions.sch"
using "clearMissionArea.sch"
using "timelapse.sch"
using "asset_management_public.sch"
using "RC_helper_functions.sch" 
using "properties_public.sch"
USING "achievement_public.sch"

USING "traffic_default_values.sch"
USING "traffic.sch"

USING "commands_recording.sch"


#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS
	mpf_michael,
	mpf_trevor,
	mpf_franklin,
	
	mpf_Scrap,
	mpf_sandking,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_michael_car,
	mvf_trevor_truck,
	mvf_Frank_car,
	mvf_start_car,
	
	mvf_scrap,
	mvf_Sandking,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_meet,
	msf_1_CHASE_TREVOR,
	msf_2_Fuel_crash_CS,
	msf_3_Ignite_trevor,
	msf_4_Goodbyes_CS,
	msf_5_Passed,
	
	CST_INT,
	CST_MCS_1,
	CST_EXT,
	
	MSF_NUM_OF_STAGES	
ENDENUM
ENUM MFF_MISSION_FAIL_FLAGS
	mff_lost_trevor,
	mff_destroyed_veh,
	mff_michael_death,
	mff_default,
	
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM

//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
	BLIP_INDEX			blip

ENDSTRUCT

//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY		0
CONST_INT				STAGE_EXIT		-1 
CONST_INT				NUM_ASSETS_MAX	30		// maximum number of assets that can be loaded

VECTOR 					vSTART_CAR	=  <<1342.9705, -2553.8333, 46.1407>> 
FLOAT					fSTARTCAR	=  303.0488
//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------
INT iRandomChatTimer

BOOL cutsceneRequested //BUG FIX 1408210
BOOL uberRecordingInitialised
BOOL FranksCarMoved
BOOL MikeCalledFranDone
BOOL trevsLightsTurnedOff
BOOL MikesLightsTurnedOff
BOOL rayfireSetToEndState
BOOL IPLsChanged
BOOL streamLoaded
BOOL streamStarted
BOOL FrankTurnToSpeak 
BOOL TrevTimeToSpeak 
BOOL failTimerSet
BOOL TrevCarImpactTriggered
BOOL PetrolEffectTriggered
BOOL bVideoRecordingDone
//BOOL doneTrevCHat
		
				
CAMERA_INDEX SyncSceneCam, SyncSceneTrevCam, EndCreditCam //BUG FIX 1324072
VEHICLE_INDEX MikeShowRoomCar
INT iRayfireStage, rayfireStage
INT ifailMissionTimer
BOOL rayfireBeingSkipped
RAYFIRE_INDEX rfGasTankExplosion, CrashRayfireEvent

int							i						//game iterator
//========================= TOD =============================
bool					bTODstart = false
//========================= structs =============================
VEHICLE_STRUCT				Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT					peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level
CHASE_HINT_CAM_STRUCT		sHintCam
structTimelapse				sTimelapse
LOCATES_HEADER_DATA			sLocatesData
structPedsForConversation 	convo_struct

//===================== relationships ========================
REL_GROUP_HASH			REL_Michael
REL_GROUP_HASH			REL_Trevor 

//================================== assets =====================================
BLIP_INDEX					blip_objective			//blip for mission objective
Camera_index				cameraIndex				//in game cut scene camera
//Camera_index				cameraIndex2				//in game cut scene camera
//SEQUENCE_INDEX				seq						//used to create AI sequence

string						sAnimDict_IG1	  	= "missfinale_a_ig_1"
string						sAnimDict_IG2	  	= "missfinale_a_ig_2"
string						sAnimDict_IG2_Alt 	= "missfinale_a_ig_2_alt_1"

string						sAnim_mike_LOOP				= "michael_base_idle_pz"
string						sAnim_mike_a 				= "michael_idle_a_pz"
string						sAnim_mike_b 				= "michael_idle_b_pz"
string						sAnim_mike_c 				= "michael_idle_c_pz"

string						sAnim_mike_DRAWS 			= "michael_base_idle_to_aim_into_pz"
string						sAnim_mike_shoots 			= "michael_gun_Shot_&_trevor_death_reaction_pz"
string						sAnim_mike_reacts 			= "michael_alternate_gun_shot_&_trevor_death_reaction"
string						sAnim_mike_FIRE_LOOP		= "michael_trevor_death_base_idle_pz"
string						sAnim_mike_FIRE_LOOP_ALT	= "michael_alternate_trevor_death_base_idle"
//string						sAnim_mike_EXP				= "michael_gas_tanker_explosion_outro_pz"
string						sAnim_mike_EXP_ALT			= "michael_alternate_gas_Tanker_explosion_outro"

string						sAnim_frank_reacts 			= "franklin_trevor_death_reaction_po"
string						sAnim_frank_shoots 			= "franklin_alternate_gun_shot_&_trevor_death_reaction"
string						sAnim_frank_FIRE_LOOP 		= "franklin_trevor_death_base_idle_po"
string						sAnim_frank_FIRE_LOOP_ALT	= "franklin_alternate_trevor_death_base_idle"
//string						sAnim_frank_EXP 			= "franklin_gas_tanker_explosion_outro_po"
string						sAnim_frank_EXP_ALT			= "franklin_alternate_gas_tanker_explosion_outro"

string						sAnim_trev_LOOP 			= "trevor_base_idle_pt"
string						sAnim_trev_A 				= "trevor_idle_a_pt"
string						sAnim_trev_B 				= "trevor_idle_b_pt"
string						sAnim_trev_dies 			= "trevor_death_reaction_pt"
string						sAnim_trev_dead 			= "trevor_dead_idle_pt"


object_index				Weapon_Object

//=========================== heart beat /Ignite =============================== 

int 					iMikeKillTimer
int						iHeartBeat 		= 0 
int						iIgniteStage 	= 0
bool					bMikeKills 		= false
bool					bFrankAim		= false
bool					bupdatecam		= true
bool					bweaponinHand 	= false
bool					bMuz_pistol_effect_played = false
bool				 	bPetrol1 		= false
bool					bMikeDamage 	= false
bool					btrevorDammage 	= false
bool 					SkyEffectStarted
bool 					endcamStarted
bool 					creditsStarted
int 					ipetrolStage

Int						SyncSceneIGMike
Int						SyncSceneIGTREV
INT						SyncSceneIGFRANK

//=========================== Rubber band variables ===============================
INT						iRubberTimer = 0				//used for debug on the rubber band sequence
float					fplaybackSpeed					//used to increase or decrease vehicle recording
float					fcurrentplaybackTime = 0.0		//used to assign current playback time
FLOAT 					fDist 

bool					bCueFence 		= false
bool					bCueTruck		= false
bool					bCueSandking	= false
//=================================== PTFX ==========================================

//========================== camera stuff ============================
int						iCamTimer = 0
int						iDelay
//============================= AUDIO / DIALOGUE / PRINTS ============================
bool 					bmusicTriggerStart  = false
bool 					bdialougeplayed 	= false

//========================== CS - IG / MCS =================================
bool 	bcsFrank 		= false
bool 	bcsTrev 		= false
bool 	bcsTrevCar 		= false
bool	bcsMike			= false
bool	bcsMikeCar		= false
bool	bcsgun			= false
bool	bcscam			= false

BOOL 	bcutsceneLoaded = FALSE
bool 	bPlaceholder	= false
int 	iSkipTimer
//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
CUTSCENE_PED_VARIATION 	sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]
//----------------

//Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch					//current switching status
INT						mission_stage				//current stage
INT						mission_substage			//current substage
INT						requestedStage				//the mission stage requested by a mission_stage switch
INT						iStageTimer					//timer used for debug
INT						iSkipToStage				//the stage to skip to
Bool					bDoSkip						//trigger the skip
bool					bis_zSkip
bool 					bCarRunning 		= false
bool					bloadscene			= true
#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID widget_debug
#endif


// ===========================================================================================================
//		Termination
// ===========================================================================================================


proc load_asset_stage(MSF_MISSION_STAGE_FLAGS loadStage)
	
	SWITCH loadStage			
		CASE msf_0_meet		
			IF IS_SCREEN_FADED_OUT()
			AND NOT Is_Replay_In_Progress()
				REQUEST_IPL("DES_tankercrash")
				REQUEST_IPL("tankerexp_grp0")
				PRINTSTRING("requesting DES_tankercrash and tankerexp_grp0 in load assets screen was faded out")
				WHILE NOT IS_IPL_ACTIVE("DES_tankercrash")
				OR NOT IS_IPL_ACTIVE("tankerexp_grp0")
					PRINTSTRING("waiting for IPL DES_tankercrash and tankerexp_grp0 to activate")
					WAIT(0)
				ENDWHILE
			ENDIF
		break
				
		CASE msf_1_CHASE_TREVOR
			IF IS_SCREEN_FADED_OUT()
			AND NOT Is_Replay_In_Progress()
				REQUEST_IPL("DES_tankercrash")
				REQUEST_IPL("tankerexp_grp0")
				PRINTSTRING("requesting DES_tankercrash and tankerexp_grp0 in load assets screen was faded out")
				WHILE NOT IS_IPL_ACTIVE("DES_tankercrash")
				OR NOT IS_IPL_ACTIVE("tankerexp_grp0")
					PRINTSTRING("waiting for IPL DES_tankercrash and tankerexp_grp0 to activate")
					WAIT(0)
				ENDWHILE	
			ENDIF
		
//			Load_Asset_Recording(sAssetData,001,"Finale1")
			Load_Asset_Recording(sAssetData,0,"BB_FINALE")
			load_Asset_model(sAssetData,scrap)
			load_asset_model(sAssetData,S_M_Y_XMECH_02)
			Load_Asset_Waypoint(sAssetData,"FINA_TRUCK")
			load_Asset_model(sAssetData,SANDKING)
			load_asset_model(sAssetData,A_M_M_SALTON_01)
			Load_Asset_Waypoint(sAssetData,"FINA_SAND")
			
		BREAK
		
		CASE msf_2_Fuel_crash_CS	
			IF IS_SCREEN_FADED_OUT()
			AND NOT Is_Replay_In_Progress()
				REQUEST_IPL("DES_tankercrash")
				REQUEST_IPL("tankerexp_grp0")
				PRINTSTRING("requesting DES_tankercrash and tankerexp_grp0 in load assets screen was faded out")
				WHILE NOT IS_IPL_ACTIVE("DES_tankercrash")
				OR NOT IS_IPL_ACTIVE("tankerexp_grp0")
					PRINTSTRING("waiting for IPL DES_tankercrash and tankerexp_grp0 to activate")
					WAIT(0)
				ENDWHILE		
			ENDIF
		
//			//Load in the IPL's for the tanker.
//			REMOVE_IPL("DES_tankerexp")
//			REQUEST_IPL("DES_tankercrash")
//			REQUEST_IPL("tankerexp_grp0")		
			Load_Asset_Recording(sAssetData,002,"Finale1")	
			Load_Asset_Recording(sAssetData,003,"Finale1")	
		BREAK
		
		CASE msf_3_Ignite_trevor
			IF IS_SCREEN_FADED_OUT()
			AND NOT Is_Replay_In_Progress()
				REQUEST_IPL("DES_tankerexp")
				PRINTSTRING("requesting DES_tankerexp in load assets screen was faded out")
				WHILE NOT IS_IPL_ACTIVE("DES_tankerexp")
					PRINTSTRING("waiting for IPL DES_tankerexp to activate")
					WAIT(0)
				ENDWHILE		
			ENDIF
		
//			REMOVE_IPL("DES_tankercrash")
//			REMOVE_IPL("tankercrash_grp2")
//			REMOVE_IPL("tankerexp_grp0")
//			REQUEST_IPL("DES_tankerexp")
//			WHILE NOT IS_IPL_ACTIVE("DES_tankerexp")
//				PRINTSTRING("waiting for IPL DES_tankerexp to activate")
//				WAIT(0)
//			ENDWHILE
			Load_Asset_AnimDict(sAssetData,sAnimDict_IG1)
			Load_Asset_AnimDict(sAssetData,sAnimDict_IG2)	
			Load_Asset_AnimDict(sAssetData,sAnimDict_IG2_Alt)	
		break
		
		CASE msf_4_Goodbyes_CS	
			IF IS_SCREEN_FADED_OUT()
			AND NOT Is_Replay_In_Progress()
				REQUEST_IPL("tankerexp_grp2")
				PRINTSTRING("requesting tankerexp_grp2 in load assets screen was faded out")
				WHILE NOT IS_IPL_ACTIVE("tankerexp_grp2")
					PRINTSTRING("waiting for IPL tankerexp_grp2 to activate")
					WAIT(0)
				ENDWHILE	
			ENDIF
		break
		
		CASE msf_5_Passed	
		break
	ENDSWITCH
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
func bool isentityalive(entity_index mentity)
  	if does_entity_exist(mentity)
		if is_entity_a_vehicle(mentity)
			if is_vehicle_driveable(get_vehicle_index_from_entity_index(mentity))
				return true 
			endif
		elif is_entity_a_ped(mentity)
			if not is_ped_injured(get_ped_index_from_entity_index(mentity))
	          	return true 
	    	endif	
		endif           
	endif
      return false
endfunc
func ped_index Frank()
	return peds[mpf_franklin].id
endfunc
FUNC ped_index MIKE()
	return peds[mpf_michael].id
ENDFUNC
FUNC ped_index TREV()
	return peds[mpf_trevor].id
ENDFUNC
proc set_mike()
	if isentityalive(MIKE())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE(),TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(MIKE(),REL_Michael)
		SET_PED_ACCURACY(mike(),100)
		SET_PED_COMBAT_ATTRIBUTES(mike(),CA_PERFECT_ACCURACY,true)
		STOP_PED_SPEAKING(mike(),true)
		SET_PED_CONFIG_FLAG(mike(),PCF_DisableExplosionReactions,true)
	endif	
endproc
proc set_trev()
	if isentityalive(TREV())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TREV(),TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(TREV(),REL_Trevor)
		STOP_PED_SPEAKING(TREV(),true)
		SET_PED_CONFIG_FLAG(trev(),PCF_DisableExplosionReactions,true)
	endif
endproc
proc set_exit_states(bool bstate)
	bcsfrank 	= bstate
	bcstrev	 	= bstate
	bcstrevcar 	= bstate
	bcsMike		= bstate
	bcsMikeCar	= bstate
	bcsgun		= bstate
	bcscam		= bstate
endproc
Proc SET_CS_OUTFITS_AND_EXITS(PED_INDEX Ignore1 = null)
	if ignore1 != MIKE() OR MIKE() = NULL
		IF DOES_ENTITY_EXIST(MIKE())
			IF NOT IS_PED_INJURED(MIKE())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("MICHAEL", MIKE(), PLAYER_ZERO)
			ENDIF
		ELSE
			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_MICHAEL, "MICHAEL")
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_TORSO, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_LEG, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_HAND, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_FEET, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", PED_COMP_DECL, 0, 0)	
		ENDIF
	endif	
	if ignore1 != TREV()
		IF DOES_ENTITY_EXIST(TREV())
			IF NOT IS_PED_INJURED(TREV())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("TREVOR", TREV(), PLAYER_TWO)
			ENDIF
		ELSE
			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)
		ENDIF
	endif	
	if IsEntityAlive(FRANK())
	and ignore1 != FRANK()
//		Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister,FRANK(),"FRANKLIN")
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("FRANKLIN", PLAYER_PED_ID(), PLAYER_ONE)
	endif
	
	set_exit_states(false)
endproc
proc Display_Placeholder()
	if bPlaceholder		
		#IF IS_DEBUG_BUILD
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,0.1,"STRING","PLACEHOLDER")
		#ENDIF
	endif
endproc
// purpose:off radar/ hud / player control
proc prep_start_cutscene(bool bplayercontrol,vector varea,bool brender = true,bool binterp = false,bool bclearprj = true,bool bclearFire = true,int interptime = default_interp_to_from_game,set_player_control_flags controlflag = 0,bool bphoneaway = true)
	display_radar(false)
	display_hud(false)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	clear_prints()
	clear_help()
	render_script_cams(brender,binterp,interptime)	
	if bclearprj
		clear_area_of_projectiles(varea,500)
		
	endif	
	if bclearFire
		STOP_FIRE_IN_RANGE(varea,200)
	endif
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),false)
	endif
	if bphoneaway
		hang_up_and_put_away_phone()
	endif
endproc
// purpose:on radar/ hud / player control
proc prep_stop_cutscene(bool bplayercontrol,bool brender = false,bool binterp = false,int iduration = default_interp_to_from_game, set_player_control_flags controlflag = 0)
	display_radar(true)
	display_hud(true)	
	set_player_control(player_id(),bplayercontrol,controlflag)	
	render_script_cams(brender,binterp,iduration)	
	if not brender	
		destroy_all_cams()
	endif
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),true)
	endif
	bPlaceholder = false
endproc
PROC Point_Gameplay_cam_at_coord(float targetHeading)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(targetHeading - GET_ENTITY_HEADING(Player_ped_id()))
endproc
FUNC BOOL IS_DIALOGUE_PLAYING()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	OR IS_SCRIPTED_CONVERSATION_ONGOING() 
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_WEAPON_A_GUN(WEAPON_TYPE eWeapon)
	WEAPON_GROUP eWeaponGroup = GET_WEAPONTYPE_GROUP(eWeapon) 

	IF eWeaponGroup = WEAPONGROUP_PISTOL OR eWeaponGroup = WEAPONGROUP_SMG       OR eWeaponGroup = WEAPONGROUP_RIFLE
	OR eWeaponGroup = WEAPONGROUP_MG     OR eWeaponGroup = WEAPONGROUP_SHOTGUN   OR eWeaponGroup = WEAPONGROUP_SNIPER
	OR eWeaponGroup = WEAPONGROUP_HEAVY  OR eWeaponGroup = WEAPONGROUP_RUBBERGUN
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL Add_Petrol_trail()
vector vPetrolStart 	
	switch ipetrolStage
		case 0		
//		SET_VFX_IGNORE_CAM_DIST_CHECK(TRUE)
		vPetrolStart  = <<1733.97754, -1626.46741, 111.43042>>
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)	
		wait(0)
		ADD_PETROL_DECAL(vPetrolStart,1,20,1)
//		SET_VFX_IGNORE_CAM_DIST_CHECK(false)	
			ipetrolStage++
		break
		case 1
			ipetrolStage = 0
			return true
		break
	endswitch
	
return false	
endfunc
proc clear_players_task_on_control_input(script_task_name task_name)

	if get_script_task_status(player_ped_id(), task_name) = performing_task
			
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28
	
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

		IF NOT IS_LOOK_INVERTED()
			right_stick_y *= -1
		ENDIF
		
		// invert the vertical
		IF (left_stick_y > STICK_DEAD_ZONE)
		OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
		or (left_stick_x > STICK_DEAD_ZONE)
		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
		//or is_control_pressed(player_control, input_sprint) 
		
			clear_ped_tasks(player_ped_id())
			
		endif 		
	endif 

endproc 

// remove mission text
PROC REMOVE_MISSION_TEXT(BOOL bClearSpeech = TRUE, BOOL bClearGodText = TRUE, BOOL bClearHelpText = TRUE)
	IF bClearSpeech
		KILL_ANY_CONVERSATION()
	ENDIF
	
	IF bClearGodText
		IF (NOT IS_DIALOGUE_PLAYING()or GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES)= 0)
		OR bClearSpeech
			CLEAR_PRINTS()
		ENDIF
		CLEAR_REMINDER_MESSAGE()
	ENDIF
	
	IF bClearHelpText
		CLEAR_HELP()
	ENDIF
ENDPROC
FUNC FLOAT GET_AIM_HEADING_FOR_COORD(VECTOR pos) 

      VECTOR vec_ba
      FLOAT aim_heading
		
      vec_ba = pos - get_entity_coords(player_ped_id())
      
      //x rotation
      aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(player_ped_id()))
      
      IF aim_heading > 180
            aim_heading -= 360
      ENDIF
      IF aim_heading < -180
            aim_heading += 360
      ENDIF

      RETURN aim_heading
      
ENDFUNC 

FUNC FLOAT GET_AIM_PITCH_FOR_COORD(VECTOR pos)

      VECTOR vec_ba
      VECTOR direction_vec
      
      vec_ba = POS - get_entity_coords(player_ped_id())
      
      direction_vec = normalise_vector(vec_ba)

      RETURN (ATAN2(direction_vec.z, VMAG(<<direction_vec.x, direction_vec.y, 0.0>>)))

ENDFUNC
PROC CONVERGE_VALUE(FLOAT &val, FLOAT fDesiredVal, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = true)
	IF val != fDesiredVal
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = fConvergeAmountThisFrame +@ fAmountToConverge
		ENDIF
	
		IF val - fDesiredVal > fConvergeAmountThisFrame
			val -= fConvergeAmountThisFrame
		ELIF val - fDesiredVal < -fConvergeAmountThisFrame
			val += fConvergeAmountThisFrame
		ELSE
			val = fDesiredVal
		ENDIF
	ENDIF
ENDPROC
PROC Rubber_banding(float &fcurrentspeed,VEHICLE_INDEX &vehFranklin,Vehicle_index &vehTrevor)
	
	IF IS_VEHICLE_DRIVEABLE(vehFranklin)
	AND IS_VEHICLE_DRIVEABLE(vehTrevor)
		
		vector vpos_Franklin 	= GET_ENTITY_COORDS(vehFranklin)
		vector vpos_Trevor 		= GET_ENTITY_COORDS(vehTrevor)
		
		//Rubber banding
		fDist = VDIST(vpos_Franklin, vpos_Trevor)
		FLOAT fTouchingDist = 4.5 
		FLOAT fIdealDist = 10.0 
		FLOAT fMaxDist = 50.0 
		FLOAT fSuperMaxDist = 100.0 
		FLOAT fDesiredPlaybackSpeed = 1.0
				
		if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevor)
			fcurrentplaybackTime = GET_TIME_POSITION_IN_RECORDING(vehTrevor)
			//VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrevor, vpos_Franklin)
			
				//At start keep them closer together
			IF fCurrentPlaybackTime < 9500
				fIdealDist = 12.0
				fMaxDist = 57.0
				fSuperMaxDist = 100.0
			ENDIF
			//fast straight
			IF fCurrentPlaybackTime > 9500 and fcurrentplaybackTime < 17059
				fIdealDist = 20.0
				fMaxDist = 46.0
				fSuperMaxDist = 100.0
			ENDIF		
			//hard U turn
			IF fCurrentPlaybackTime > 17059 and fcurrentplaybackTime < 30889
				fIdealDist = 32
				fMaxDist = 64.25
				fSuperMaxDist = 157.0
			ENDIF
			// Up the hill
			IF fCurrentPlaybackTime > 30889 and fcurrentplaybackTime < 43276
				fIdealDist = 37.0
				fMaxDist = 75
				fSuperMaxDist = 200.0
			ENDIF
			// right turn around towards crash
			IF fCurrentPlaybackTime > 43276 and fcurrentplaybackTime < 58586
				fIdealDist = 46.75
				fMaxDist = 93.5
				fSuperMaxDist = 274
			ENDIF
			IF fCurrentPlaybackTime > 58586 and fcurrentplaybackTime < 66306
				fIdealDist = 40
				fMaxDist = 55
				fSuperMaxDist = 120
			ENDIF
			IF fCurrentPlaybackTime > 66306 
				fIdealDist = 30.0
				fMaxDist = 45.0
				fSuperMaxDist = 60
			ENDIF
			
			FLOAT fDistRatio = 0.0
			VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrevor, vpos_Franklin)
			IF vOffset.y < 1.0 
				//Player is behind
				IF fDist > fMaxDist 
					//Player is a long way behind, slow the trigger car down by a lot (if not on screen).
					BOOL bInsanelyFarBehind = FALSE
					IF fDist > fSuperMaxDist
						IF fDist > 200.0
							bInsanelyFarBehind = TRUE
						ENDIF
						fDist = fSuperMaxDist
					ENDIF
					
					fDistRatio = ((fDist - fMaxDist) / (fSuperMaxDist - fMaxDist))
					
					IF IS_ENTITY_ON_SCREEN(vehTrevor) AND NOT bInsanelyFarBehind
						fDesiredPlaybackSpeed = 0.7 - (0.2 * fDistRatio)
					ELSE
						fDesiredPlaybackSpeed = 0.5 - (0.3 * fDistRatio)
					ENDIF
				ELIF fDist > fIdealDist
					//Player is somewhat behind, slow the trigger car down relative to how far behind the player is.
					fDistRatio = ((fDist - fIdealDist) / (fMaxDist - fIdealDist))
					fDesiredPlaybackSpeed = 1.0 - (0.3 * fDistRatio)
				ELSE
					//Player is getting too close, speed up the trigger car relative to how close the player is.
					IF fDist < fTouchingDist
						fDist = fTouchingDist
					ENDIF
					
					fDistRatio = (fIdealDist - fDist) / (fIdealDist - fTouchingDist)
					
					IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
						fDesiredPlaybackSpeed = 1.0 + (fDistRatio * 2.0)
					ELSE
						fDesiredPlaybackSpeed = 1.0 + (fDistRatio)
					ENDIF
				ENDIF		
			ELSE 
				//Player is in front, speed up trigger car by a lot.
				IF fDist > fSuperMaxDist
					fDist = fSuperMaxDist
				ENDIF
				fDistRatio = fDist / fSuperMaxDist
				
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					fDesiredPlaybackSpeed = 2.25 + fDistRatio
				ELSE
					fDesiredPlaybackSpeed = 1.0 + fDistRatio
				ENDIF	
				
			ENDIF		
						
			if (GET_GAME_TIMER() - irubberTimer) > 1000
				irubberTimer = GET_GAME_TIMER()
				PRINTLN("curent plaback time: ",fCurrentPlaybackTime," Distance: ",fDist," current playback speed: ",fDesiredPlaybackSpeed," Y = ",vOffset.y)
			ENDIF
			
			IF fCurrentPlaybackTime < 500 				
				CONVERGE_VALUE(fCurrentSpeed,1.00,1)	
			else			
				if fDesiredPlaybackSpeed <= 0.4
					fDesiredPlaybackSpeed = 0.4
				endif
				CONVERGE_VALUE(fCurrentSpeed, fDesiredPlaybackSpeed, 0.02)					
			endif
		endif
	endif
ENDPROC
/// PURPOSE: Checks if previous car is not franklins default place that.
///    Otherwise run CREATE_PLAYER_VEHICLE for franklins default car
/// PARAMS: pos, heading 
/// RETURNS: True once car has been placed *CALL EVERY FRAME*
///    
func bool Check_Franklins_Car(vector pos, float heading)
	//check 
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		//Check if it's a bicycle for bug 1849886 and any other vehicle that would screw the mission bug 1928744
		IF NOT IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
		AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
		AND NOT IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
		AND NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
		
			REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
			while not HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
				wait(0)
			endwhile
			vehs[mvf_Frank_car].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(pos,heading)
			return true
		ELSE
			if not CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id,char_Franklin,pos, heading,true,VEHICLE_TYPE_CAR)
				return false
			else
				return true
			endif	
		ENDIF
	else
		if not CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id,char_Franklin,pos, heading,true,VEHICLE_TYPE_CAR)
			return false
		else
			return true
		endif
	endif
endfunc
FUNC BOOL Set_Current_Player_Ped(SELECTOR_SLOTS_ENUM pedChar, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE)

	IF bWait
		WHILE NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)	
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)
			RETURN FALSE
		ENDIF
	ENDIF
			
	// Additional stuff to set up ped for use in mission
	SWITCH pedChar
		CASE SELECTOR_PED_MICHAEL
			peds[mpf_michael].id = PLAYER_PED_ID()				
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			peds[mpf_franklin].id = PLAYER_PED_ID()
		BREAK
		CASE SELECTOR_PED_TREVOR
			peds[mpf_trevor].id = PLAYER_PED_ID()
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH eStage
		CASE msf_0_meet				vCoord = <<1337.35913, -2550.00562, 46.16376 >>		fHeading = -101.35		BREAK
		CASE msf_1_CHASE_TREVOR		vCoord = <<1337.35913, -2550.00562, 46.16376 >>		fHeading = -101.35		BREAK
		CASE msf_2_Fuel_crash_CS	vCoord = << 1739.8234, -1556.4989,  112.2425 >>		fHeading =  197.39		BREAK
		CASE msf_3_Ignite_trevor	vCoord = << 1731.5767, -1615.9362,  112.4401 >>		fHeading = -154.56		BREAK	
		CASE msf_4_Goodbyes_CS		vCoord = <<1731.36157, -1616.12720, 111.4371 >>		fHeading = -154.56		BREAK	
		CASE msf_5_passed			vCoord = <<1670.57230, -1742.90110, 111.2739 >>		fHeading =   59.94 		BREAK	
	ENDSWITCH

ENDPROC
proc SU_GENERAL()
	set_trev()
	set_mike()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Player_ped_id(),true)
endproc
proc SU_MSF_0_MEET()
	peds[mpf_franklin].id = player_ped_id()	
	if IS_REPLAY_IN_PROGRESS()
		SET_CLOCK_TIME(19,45,00)
	//trev
		while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_truck].id,CHAR_TREVOR,<<  1374.2235, -2531.8555, 48.2458 >>, 317.3613)
			wait(0)
		endwhile
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_trevor_truck].id)
			wait(0)
		endwhile				
		while not Check_Franklins_Car(vSTART_CAR,fSTARTCAR)
			wait(0)
		endwhile		
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP()
		Else
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
		ENDIF	
	endif
endproc
proc su_msf_1_CHASE_TREVOR()
	peds[mpf_franklin].id = player_ped_id()
	
//create trevor	
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_truck].id,CHAR_TREVOR,<<  1374.2235, -2531.8555, 48.2458 >>, 317.3613)
		wait(0)
	endwhile
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_trevor_truck].id)
		wait(0)
	endwhile
	SET_ENTITY_INVINCIBLE(vehs[mvf_trevor_truck].id,true)
	SET_ENTITY_INVINCIBLE(peds[mpf_trevor].id,true)	
	//create Franklins car
	while not Check_Franklins_Car(<< 1361.9222, -2538.0073, 46.9311 >>, 318.1483)
		wait(0)
	endwhile
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_Frank_car].id)
	Else
		SET_PED_INTO_VEHICLE(Frank(),vehs[mvf_Frank_car].id)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)
			SET_VEHICLE_ENGINE_ON(vehs[mvf_Frank_car].id,true,true)	
		ENDIF
	ENDIF
	if IS_PED_ON_ANY_BIKE(player_ped_id())
		GIVE_PED_HELMET(PLAYER_PED_ID(),false)
	endif
	bCarRunning = true	
			
	TRIGGER_MUSIC_EVENT("FINA_RESTART_CHASE")				
endproc
proc su_msf_2_Fuel_crash_CS()
	REMOVE_CUTSCENE()

	peds[mpf_franklin].id = player_ped_id()
	//set up Franklin					
	while not Check_Franklins_Car(<< 1728.3752, -1610.3052, 111.4705 >>,160.3058)
		wait(0)
	endwhile
	//set up Trevor
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_truck].id,CHAR_TREVOR,<< 1739.8234, -1556.4989, 112.2425 >>, 317.3613)
		wait(0)
	endwhile
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_trevor_truck].id)
		wait(0)
	endwhile
	//set up Michael					
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_michael_car].id,CHAR_MICHAEL,<< 1647.7448, -1646.3500, 111.2280 >>, 269.6557,true,VEHICLE_TYPE_CAR)
		wait(0)
	endwhile
//	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_michael_car].id)
//		wait(0)
//	endwhile
	REQUEST_CUTSCENE("FIN_A_MCS_1")//("FIN_A_MCS_1_alt1")
	SET_CS_OUTFITS_AND_EXITS()
	PRINTSTRING("FIN_A_MCS_1 CUTSCENE REQUESTED FOR SKIP") PRINTNL()
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_Frank_car].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
	TRIGGER_MUSIC_EVENT("FINA_RESTART_CHASE")
	
endproc
proc su_msf_3_Ignite_trevor()
	peds[mpf_franklin].id = player_ped_id()
//set up Franklin
	while not Check_Franklins_Car(<< 1728.3752, -1610.3052, 111.4705 >>,160.3058)
		wait(0)
	endwhile	
//set up michael	
	while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_michael].id,CHAR_MICHAEL,<< 1731.66162, -1615.98877, 111.43713>>,-162.95)
		wait(0)
	endwhile
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_michael_car].id,CHAR_MICHAEL,<<1728.2063, -1623.8717, 111.4329>>, 269.6653,true,VEHICLE_TYPE_CAR)
		wait(0)
	endwhile	
//set up Trevor
	while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_trevor].id,CHAR_TREVOR,<<1731.381,-1620.509,111.428>>,-57)
		wait(0)
	endwhile		
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_truck].id,CHAR_TREVOR,<<1732.2578, -1626.3915, 111.4349>>, 216.9961)
		wait(0)
	endwhile			
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF	
//	while not Add_Petrol_trail()
//		wait(0)
//	endwhile
	TRIGGER_MUSIC_EVENT("FINA_KILL_RESTART")
endproc
proc su_msf_4_Goodbyes_CS()
	SET_CLOCK_TIME(20,45,00)
	
	CANCEL_MUSIC_EVENT("FINA_CHASE")
	
	//frank
	peds[mpf_franklin].id = player_ped_id()
	while not Check_Franklins_Car(<< 1663.7246, -1732.3535, 111.2844 >>,18.3814)
		wait(0)
	endwhile	
	//mike
	while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_michael].id,CHAR_MICHAEL,<< 1731.36, -1621.64, 111.43 >>,188.9595)
		wait(0)
	endwhile	
	REQUEST_CUTSCENE("fin_a_ext")
	SET_CS_OUTFITS_AND_EXITS()
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF
endproc
PROC su_passed()
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC MISSION_CLEANUP()
	TRIGGER_MUSIC_EVENT("FINA_FAIL")
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()		
	ENDIF
	
	//Make sure new load scene is stopped in clean up. To stop game crashing when using repeat play.
	NEW_LOAD_SCENE_STOP()
	
	//Set this back to default
	SET_PLAYER_PED_DATA_IN_CUTSCENES()
	
	//==============PEDS==============
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
	endif
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if peds[i].id != PLAYER_PED_ID()	
				SAFE_RELEASE_PED(peds[i].id)
			endif
			if DOES_BLIP_EXIST(peds[i].blip)
				REMOVE_BLIP(peds[i].blip)
			endif
		endif
	endfor
	//================================
	
	//==============VEHS==============
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)		
			SAFE_RELEASE_VEHICLE(vehs[i].id)
		endif
	endfor
	//================================
	
	SAFE_DELETE_OBJECT(Weapon_Object)
	
	//-----------RESETS--------------
	SET_WANTED_LEVEL_MULTIPLIER(1)	
	IF endcamStarted = FALSE
		DISPLAY_RADAR(true)
		DISPLAY_HUD(true)
		DESTROY_ALL_CAMS()
	ENDIF
	SET_PLAYER_CONTROL(player_id(),true)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, true)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, true)	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, true)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, true)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, true)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,true)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER ,true)	
	RESET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN()
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,false)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BODHI2,false)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUFFALO,false)
	SET_TIME_SCALE(1)
	CLEAR_WEATHER_TYPE_PERSIST()
	//-------------------------------
	
	//----------DESTROYS-------------
	CLEAR_PRINTS()
	CLEAR_HELP()
	if DOES_BLIP_EXIST(blip_objective)
		remove_blip(blip_objective)
	endif
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	KILL_ANY_CONVERSATION()
	
	KILL_CHASE_HINT_CAM(sHintCam) 	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
	//-------------------------------
	
	//Remove the IPL at end of mission 
	REMOVE_IPL("DES_tankercrash") //BUG FIX 1416493
	
	DISABLE_TAXI_HAILING(false)	
	DISABLE_CELLPHONE(false)
	DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, FALSE)
	
	RELEASE_MISSION_AUDIO_BANK()
	STOP_STREAM()
	
	SET_ROADS_BACK_TO_ORIGINAL(<<1199.4, -2661.9, 70>>, <<1597.1, -2447.7, -5>>)
	PRINTSTRING("...Finale A Mission Cleanup")
	PRINTNL()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Fail & Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed(MFF_MISSION_FAIL_FLAGS fail_condition = mff_default)
	TRIGGER_MUSIC_EVENT("FINA_FAIL")	
	STRING strReason = ""
		
	//show fail message
	SWITCH fail_condition
//		CASE mff_debug_fail
//			strReason = "FIN1_FAILDB"
//		BREAK
		CASE mff_lost_trevor
			strReason = "FIN1_FAIL1"
		BREAK
		case mff_destroyed_veh
			strReason = "FIN1_FAIL2"
		break
		CASE mff_michael_death
			strReason = "CMN_MDIED"
		BREAK
		DEFAULT
			strReason = "FIN1_FAILDF"
		BREAk
	ENDSWITCH
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)	
	 
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		if IS_CUTSCENE_ACTIVE()
			SET_CUTSCENE_FADE_VALUES()	
		endif
        WAIT(0)
    ENDWHILE
	
	MISSION_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC
PROC Mission_Passed()
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	g_iFinaleCreditsToPlay = 0
	
	PUT_DEAD_CHARACTERS_PROPERTIES_BACK_ON_MARKET(CHAR_TREVOR) 
	INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FINA_KILLTREV)
	PRINTSTRING("...Finale A Mission Passed")
	PRINTNL()
	
	IF creditsStarted = FALSE
		AWARD_ACHIEVEMENT_FOR_MISSION(ACH04) // To live or die in los santos				
		creditsStarted = TRUE	
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		g_MissionStatSystemSuppressVisual = TRUE
		TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
	ENDIF	
	
	Mission_Flow_Mission_Passed(true)
	IF IS_REPEAT_PLAY_ACTIVE()
		g_bMissionStatSystemBlocker = FALSE
	ENDIF
	Mission_Cleanup()
	TERMINATE_THIS_THREAD()
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC Mission_stage_management()
	SWITCH stageswitch
		CASE STAGESWITCH_REQUESTED
			PRINTLN("[stageManagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedStage)
			stageswitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			PRINTLN("[StageManagement] Exiting mission_stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			PRINTLN("[StageManagement] Entered mission_stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 2500
			PRINTLN("[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL Mission_Set_Stage(MSF_MISSION_STAGE_FLAGS newStage)
	If stageswitch = STAGESWITCH_IDLE
		requestedstage = ENUM_TO_INT(newStage)
		stageswitch = STAGESWITCH_REQUESTED		
		return True
	else
		return false
	endif
ENDFUNC
PROC RESET_EVERYTHING()

	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
	
//==============PEDS==============
	//all peds are deleted
		for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
			if DOES_ENTITY_EXIST(peds[i].id)
			AND (NOT IS_PED_INJURED(peds[i].id))
				if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
					SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))
				endif
				if peds[i].id != player_ped_id()
					DELETE_PED(peds[i].id)	
				endif			
			endif
		endfor
//================================
//==============VEHS==============	
	//all vehicles are deleted
		for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
			if DOES_ENTITY_EXIST(vehs[i].id)		
			 	DELETE_VEHICLE(vehs[i].id)
			endif
		endfor
//================================

SAFE_DELETE_OBJECT(Weapon_Object)

//-----------RESETS--------------
		
		DISPLAY_RADAR(true)
		DISPLAY_HUD(true)
		if not IS_PED_INJURED(player_ped_id())
			CLEAR_PED_TASKS(player_ped_id())
		endif
		//wanted level
		SET_WANTED_LEVEL_MULTIPLIER(0)
		CLEAR_PLAYER_WANTED_LEVEL(player_id())
		//reset player control
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		if DOES_ENTITY_EXIST(vehs[mvf_michael_car].id)
			SET_ENTITY_INVINCIBLE(vehs[mvf_michael_car].id,false)
		endif
		SET_TIME_SCALE(1)
		//bools
		bcutsceneLoaded 	= false
		bCarRunning 		= false
		bPlaceholder		= false
		
		set_exit_states(false)
//-------------------------------
	
//----------DESTROYS-------------
		CLEAR_AREA(GET_ENTITY_COORDS(player_ped_id()),200,true)
		KILL_ANY_CONVERSATION()
		SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
		KILL_CHASE_HINT_CAM(sHintCam)
		if DOES_BLIP_EXIST(blip_objective)
			remove_blip(blip_objective)
		endif
		REMOVE_SCENARIO_BLOCKING_AREAS()
		STOP_FIRE_IN_RANGE(<<1734.43018, -1623.52039, 111.42328>>,100)
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(false,false)
		CLEAR_PRINTS()
		CLEAR_HELP()
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
		
		//Remove all IPL's and make sure to request the one's required for that specific stage in load assets
		IF NOT Is_Replay_In_Progress()
			REMOVE_IPL("DES_tankercrash")
			REMOVE_IPL("tankercrash_grp1")
			REMOVE_IPL("tankercrash_grp2")
			REMOVE_IPL("DES_tankerexp")
			REMOVE_IPL("tankerexp_grp0")
			REMOVE_IPL("tankerexp_grp1")
			REMOVE_IPL("tankerexp_grp2")
			PRINTSTRING("removing all IPL's for skip") PRINTNL()
		ENDIF
		
		STOP_AUDIO_SCENES()
//-------------------------------
ENDPROC

PROC MISSION_STAGE_SKIP()
//a skip has been made
	IF bDoSkip = TRUE
		
		//begin the skip if the switching is idle
		if stageSwitch = STAGESWITCH_IDLE
			if not IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(1000)
				ENDIF
			else
				Mission_Set_Stage(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, iSkipToStage))
			endif
		//Needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
			
			RENDER_SCRIPT_CAMS(false,false)
			SET_PLAYER_CONTROL(player_id(), true)
						
			RESET_EVERYTHING()	
			
			Start_Skip_Streaming(sAssetData)
				
			load_asset_stage(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage))	
			WHILE NOT Update_Skip_Streaming(sAssetData)
				PRINTSTRING("waiting on assets loading for mission stage skip") PRINTNL()
				WAIT(0)				
			ENDWHILE				
				
		// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading)				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
			ENDIF
		// --------------------------------------------------------------------
			
			SWITCH INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, MISSION_STAGE)				
				CASE MSF_0_meet				SU_MSF_0_MEET()				BREAK				
				CASE MSF_1_CHASE_TREVOR	 	SU_MSF_1_CHASE_TREVOR()		BREAK
				CASE MSF_2_Fuel_crash_CS 	SU_MSF_2_Fuel_crash_CS()	BREAK				
				CASE MSF_3_Ignite_trevor	SU_MSF_3_Ignite_trevor()	BREAK				
				CASE MSF_4_Goodbyes_CS 		SU_MSF_4_Goodbyes_CS()		BREAK	
				case msf_5_Passed			su_passed()					BREAK
			ENDSWITCH
			
			SU_GENERAL()						
			bDoSkip = FALSE	
			IF NOT IS_REPLAY_BEING_SET_UP()	
				NEW_LOAD_SCENE_STOP()
			endif
		ENDIF
#if Is_debug_build
	//Check is a skip being asked for, dont allow skip during setup stage
	ELIF LAUNCH_MISSION_STAGE_MENU(zMenuNames, iSkipToStage, mission_stage, TRUE)
		IF iSkipToStage > ENUM_TO_INT(msf_4_Goodbyes_CS)
			MISSION_PASSED()
		ELSE
			iSkipToStage = CLAMP_INT(iSkipToStage, 0, ENUM_TO_INT(MSF_NUM_OF_STAGES)-1)
			if IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_OUT(1000)
				bDoSkip = true
			endif
		endif
		bis_zSkip = true	
#endif
	ENDIF
ENDPROC
PROC MISSION_CHECKS()
	
//------------------death checks-------------------------------

	for i = 0 to enum_to_int(MPF_NUM_OF_PEDS) -1
		if DOES_ENTITY_EXIST(peds[i].id)
			if IS_PED_INJURED(peds[i].id)
				if peds[i].id = peds[mpf_michael].id
					Mission_Failed(mff_michael_death)
				endif
				SET_PED_AS_NO_LONGER_NEEDED(peds[i].id)
			ENDIF
		ENDIF
	ENDFOR
	for i = 0 to enum_to_int(MVF_NUM_OF_VEH) -1
		if DOES_ENTITY_EXIST(vehs[i].id)
			if not IS_VEHICLE_DRIVEABLE(vehs[i].id)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
			ENDIF
		ENDIF
	ENDFOR
	
	Display_Placeholder()
//--------------------------------------------------------------------------------
ENDPROC

PROC MISSION_SETUP()

	peds[mpf_franklin].id = player_ped_id()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
		
	//Call this here to give me full control over peds variations in cutscenes
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE)
		
	//	set realationships	
	ADD_RELATIONSHIP_GROUP("trevor_rel", REL_Trevor)
	ADD_RELATIONSHIP_GROUP("michael_rel", REL_Michael)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,REL_Michael,REL_Trevor)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,REL_Trevor,REL_Michael)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,REL_Trevor,RELGROUPHASH_PLAYER)	
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,REL_Michael,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,REL_Michael)
	
	SET_ROADS_IN_AREA(<<1199.4, -2661.9, 70>>, <<1597.1, -2447.7, -5>>, FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(0)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, false)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, false)
	enable_dispatch_service(dt_police_automobile, 	false)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,false)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER ,false)
	SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE,0)
	SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_HELICOPTER,0)
	SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_ROAD_BLOCK,0)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,true)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BODHI2,true)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUFFALO,true)
	//crash site
	ADD_SCENARIO_BLOCKING_AREA(<<1740.29504, -1602.18420, 110>>,<<1699.40442, -1646.40857, 116.52637>>)
	//tourist block
	ADD_SCENARIO_BLOCKING_AREA(<<1369.17896, -2546.99292, 45>>,<<1372.99866, -2542.33276, 48>>)
	//block roads
	SET_ROADS_IN_ANGLED_AREA(<<1506.58704, -2531.95703, 35>>,<<1463.06506, -1962.42371, 130>>,320,true,false)
	SET_ROADS_IN_ANGLED_AREA(<<1467.82227, -1875.56714, 35>>,<<1883.59082, -1232.34180, 130>>,450,true,false)
	SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
	
	//is the player replaying previous stage
	IF Is_Replay_In_Progress()	
	or IS_REPEAT_PLAY_ACTIVE()
	
		IF Is_Replay_In_Progress()
			iSkipToStage = Get_Replay_Mid_Mission_Stage()
			if iSkipToStage = 0	
				iSkipToStage = 1
			endif			
			if g_bShitskipAccepted 
				iSkipToStage++
			endif	
			//Sort out IPL groups before the start_replay_setup so that new load scene is started after the IPL groups
			if iskipToStage = 1
			or iskipToStage = 2
				REQUEST_IPL("DES_tankercrash")
			endif
			if iSkipToStage = 3
				REMOVE_IPL("DES_tankercrash")
				REMOVE_IPL("tankercrash_grp1")
				REMOVE_IPL("tankercrash_grp2")
				REMOVE_IPL("tankerexp_grp0")
				REQUEST_IPL("DES_tankerexp")
			endif
			if iSkipToStage = 4
				REMOVE_IPL("DES_tankerexp")
				REQUEST_IPL("tankerexp_grp2")
			endif
			if iskipToStage >= 5				
				iskipToStage = 5	
			endif			
			bis_zSkip = false
		elif IS_REPEAT_PLAY_ACTIVE()
			REMOVE_IPL("DES_tankerexp")
			REMOVE_IPL("tankerexp_grp1")
			REMOVE_IPL("tankerexp_grp2")
			REMOVE_IPL("tankerexp_grp3")
			REQUEST_IPL("DES_tankercrash")
			REQUEST_IPL("tankerexp_grp0")
			iSkipToStage = 0
		endif
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
		ENDIF		
		
		bDoSkip = TRUE						
	ELSE //start from begining
		bis_zSkip = false
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"Stage 0: meet")
		mission_stage = enum_to_int(msf_0_meet)
		Mission_Set_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))
		load_asset_stage(msf_0_meet)
		PRINTLN("Start opening Loading")
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE							
		PRINTLN("End opening Loading")			
	ENDIF
	
	DISABLE_TAXI_HAILING(true)		
	mission_substage = STAGE_ENTRY
	
ENDPROC
Proc timelapse()	
	sTimelapse.currentTimeOfDay = GET_CURRENT_TIMEOFDAY()
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	
	IF sTimelapse.iTimelapseCut = 0
		if not IS_CUTSCENE_ACTIVE()
			Println("TIME 1")
			bTODstart = false
			REQUEST_CUTSCENE("FIN_A_INT")	
		ENDIF
	endif		
		
	while not DO_TIMELAPSE(SP_MISSION_FINALE_A,sTimelapse,true,false,false,true)		
		//Call every frame this command only returns true once after a cutscene has been requested.
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			PRINTSTRING("cutscene ready for variations to be set now 0")
			SET_CS_OUTFITS_AND_EXITS(MIKE())
		ENDIF		
		Println("TIME 3: ",sTimelapse.iTimelapseCut)
		DISPLAY_RADAR(false)
		display_hud(false)
		wait(0)	
		if not bTODstart
		and sTimelapse.iTimelapseCut = 2
			IF DOES_ENTITY_EXIST(player_ped_id())
				IF NOT IS_PED_INJURED(player_ped_id())
					SET_ENTITY_COORDS(player_ped_id(),<< 1335.79663, -2550.45801, 46.19570 >>)
					SET_ENTITY_HEADING(player_ped_id(), 138.6753)
				ENDIF
			ENDIF
			
			CLEAR_AREA(<< 1334.85095, -2555.59424, 45.58296  >>,200,true)
			
			//frank car				
			if not DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
					vehs[mvf_Frank_car].id = GET_MISSION_START_VEHICLE_INDEX()
					IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL()) 
					OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL()) 
					OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL()) 
					OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL()) 
						IF GET_REPLAY_START_VEHICLE_MODEL() = BUS
							IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
								IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
									SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<1360.2174, -2531.7971, 46.8857>>)
									SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 322.7698)
								ENDIF
							ENDIF	
						ELSE
							IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
								IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
									SET_ENTITY_COORDS(vehs[mvf_Frank_car].id,vSTART_CAR)
									SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, fSTARTCAR)
								ENDIF
							ENDIF
						ENDIF
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0.0,0.0,0.0>>, 0.0, TRUE, CHAR_FRANKLIN)
					ELSE
						SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_Frank_car].id)
						DELETE_VEHICLE(vehs[mvf_Frank_car].id)
						WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id, CHAR_FRANKLIN, vSTART_CAR, fSTARTCAR, TRUE, VEHICLE_TYPE_CAR)
							PRINTSTRING("waiting on franks car being created") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
				ELSE
					WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id, CHAR_FRANKLIN, vSTART_CAR, fSTARTCAR, TRUE, VEHICLE_TYPE_CAR)
						PRINTSTRING("waiting on franks car being created") PRINTNL()
						WAIT(0)
					ENDWHILE
				ENDIF
			else	
				IF GET_ENTITY_MODEL(vehs[mvf_Frank_car].id) = BUS
					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
						SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<1360.2174, -2531.7971, 46.8857>>)
						SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 322.7698)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)
						SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Frank_car].id,false)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
						SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, vSTART_CAR)
						SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, fSTARTCAR)							
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)
						SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Frank_car].id,false)
					ENDIF
				ENDIF
			endif	
			
			bTODstart = true
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
		endif
	endwhile

endproc
Proc Run_Init_CutScene()

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	#ENDIF
	
	IF NOT bcutsceneLoaded
		if NOT IS_CUTSCENE_ACTIVE()
			REQUEST_CUTSCENE("FIN_A_INT")	
		endif
		//Call every frame this command only returns true once after a cutscene has been requested.
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			PRINTSTRING("cutscene ready for variations to be set now 3")
			SET_CS_OUTFITS_AND_EXITS(MIKE())
		ENDIF
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			//pre mission setup			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)	
			WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_truck].id, CHAR_TREVOR, <<1317.8, -2560.5, 45.1>>, 0, TRUE, VEHICLE_TYPE_CAR)
				PRINTSTRING("waiting for trev's car to be created") PRINTNL()
				WAIT(0)
			ENDWHILE
			REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_trevor_truck].id,"Trevors_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY) //trev truck
			REGISTER_ENTITY_FOR_CUTSCENE(null,"Trevor",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,PLAYER_TWO) //trev	
			if not HAS_PED_GOT_WEAPON(Frank(),WEAPONTYPE_PISTOL)
				GIVE_WEAPON_TO_PED(Frank(),WEAPONTYPE_PISTOL,25,true,true)
			endif
			IF NOT DOES_ENTITY_EXIST(Weapon_Object)
				Weapon_Object = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(Frank(),WEAPONTYPE_PISTOL)
			ENDIF
			REGISTER_ENTITY_FOR_CUTSCENE(Weapon_Object, "Franklins_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
			START_CUTSCENE()
			bcutsceneLoaded = TRUE			
		ENDIF	
	ENDIF
	
endproc

//PURPOSE: Adds all the cars needed for the uber recording playback
PROC FILL_UBER_RECORDING()

TrafficCarPos[0] = <<1257.4471, -2067.2722, 44.0916>>
TrafficCarQuatX[0] = -0.0181
TrafficCarQuatY[0] = -0.0018
TrafficCarQuatZ[0] = 0.2169
TrafficCarQuatW[0] = 0.9760
TrafficCarRecording[0] = 1
TrafficCarStartime[0] = 22902.0000
TrafficCarModel[0] = regina

TrafficCarPos[1] = <<1249.3594, -2058.8069, 44.0092>>
TrafficCarQuatX[1] = 0.0001
TrafficCarQuatY[1] = -0.0007
TrafficCarQuatZ[1] = 0.1766
TrafficCarQuatW[1] = 0.9843
TrafficCarRecording[1] = 2
TrafficCarStartime[1] = 23364.0000
TrafficCarModel[1] = RancherXL

//ParkedCarPos[0] = <<1234.9286, -2060.0273, 44.3581>>
//ParkedCarQuatX[0] = -0.0001
//ParkedCarQuatY[0] = 0.0004
//ParkedCarQuatZ[0] = -0.5778
//ParkedCarQuatW[0] = 0.8162
//ParkedCarModel[0] = scrap

TrafficCarPos[2] = <<1229.2913, -2034.4783, 43.9109>>
TrafficCarQuatX[2] = -0.0083
TrafficCarQuatY[2] = -0.0012
TrafficCarQuatZ[2] = 0.9791
TrafficCarQuatW[2] = -0.2033
TrafficCarRecording[2] = 3
TrafficCarStartime[2] = 24816.0000
TrafficCarModel[2] = landstalker

TrafficCarPos[3] = <<1214.2241, -1946.9001, 39.2982>>
TrafficCarQuatX[3] = -0.0411
TrafficCarQuatY[3] = -0.0103
TrafficCarQuatZ[3] = 0.2382
TrafficCarQuatW[3] = 0.9703
TrafficCarRecording[3] = 4
TrafficCarStartime[3] = 28644.0000
TrafficCarModel[3] = Phoenix

TrafficCarPos[4] = <<1186.6726, -1905.5449, 34.6968>>
TrafficCarQuatX[4] = -0.0471
TrafficCarQuatY[4] = -0.0027
TrafficCarQuatZ[4] = 0.2354
TrafficCarQuatW[4] = 0.9708
TrafficCarRecording[4] = 5
TrafficCarStartime[4] = 30294.0000
TrafficCarModel[4] = MESA

TrafficCarPos[5] = <<1169.1614, -1909.9987, 34.4001>>
TrafficCarQuatX[5] = -0.0039
TrafficCarQuatY[5] = 0.0462
TrafficCarQuatZ[5] = 0.9684
TrafficCarQuatW[5] = -0.2450
TrafficCarRecording[5] = 6
TrafficCarStartime[5] = 30492.0000
TrafficCarModel[5] = MESA

//TrafficCarPos[6] = <<1150.1210, -1873.0137, 31.0648>>
//TrafficCarQuatX[6] = -0.0028
//TrafficCarQuatY[6] = 0.0323
//TrafficCarQuatZ[6] = 0.9757
//TrafficCarQuatW[6] = -0.2167
//TrafficCarRecording[6] = 7
//TrafficCarStartime[6] = 31812.0000
//TrafficCarModel[6] = regina

TrafficCarPos[7] = <<1166.3428, -1854.8760, 30.0536>>
TrafficCarQuatX[7] = -0.0313
TrafficCarQuatY[7] = 0.0024
TrafficCarQuatZ[7] = 0.2128
TrafficCarQuatW[7] = 0.9766
TrafficCarRecording[7] = 8
TrafficCarStartime[7] = 32010.0000
TrafficCarModel[7] = landstalker

//ParkedCarPos[1] = <<1163.4136, -1840.8119, 37.2584>>
//ParkedCarQuatX[1] = -0.0110
//ParkedCarQuatY[1] = 0.0081
//ParkedCarQuatZ[1] = 0.8175
//ParkedCarQuatW[1] = 0.5757
//ParkedCarModel[1] = scrap

//TrafficCarPos[8] = <<1172.1002, -1821.6675, 36.6579>>
//TrafficCarQuatX[8] = -0.0045
//TrafficCarQuatY[8] = 0.0024
//TrafficCarQuatZ[8] = 0.9854
//TrafficCarQuatW[8] = -0.1699
//TrafficCarRecording[8] = 9
//TrafficCarStartime[8] = 33000.0000
//TrafficCarModel[8] = Phoenix

//TrafficCarPos[9] = <<1152.6714, -1819.1637, 28.9579>>
//TrafficCarQuatX[9] = -0.0064
//TrafficCarQuatY[9] = 0.0061
//TrafficCarQuatZ[9] = 0.1665
//TrafficCarQuatW[9] = 0.9860
//TrafficCarRecording[9] = 10
//TrafficCarStartime[9] = 33132.0000
//TrafficCarModel[9] = Sadler

TrafficCarPos[10] = <<1146.7635, -1801.8179, 28.9200>>
TrafficCarQuatX[10] = -0.0022
TrafficCarQuatY[10] = 0.0040
TrafficCarQuatZ[10] = 0.1746
TrafficCarQuatW[10] = 0.9846
TrafficCarRecording[10] = 11
TrafficCarStartime[10] = 33594.0000
TrafficCarModel[10] = regina

//ParkedCarPos[2] = <<1138.2710, -1795.8748, 29.2882>>
//ParkedCarQuatX[2] = -0.0016
//ParkedCarQuatY[2] = -0.0003
//ParkedCarQuatZ[2] = 0.1764
//ParkedCarQuatW[2] = 0.9843
//ParkedCarModel[2] = scrap

//TrafficCarPos[11] = <<1140.2302, -1783.4012, 28.7476>>
//TrafficCarQuatX[11] = -0.0002
//TrafficCarQuatY[11] = 0.0009
//TrafficCarQuatZ[11] = 0.1692
//TrafficCarQuatW[11] = 0.9856
//TrafficCarRecording[11] = 12
//TrafficCarStartime[11] = 34122.0000
//TrafficCarModel[11] = landstalker

//TrafficCarPos[12] = <<1111.1577, -1788.5449, 28.9294>>
//TrafficCarQuatX[12] = -0.0041
//TrafficCarQuatY[12] = -0.0015
//TrafficCarQuatZ[12] = 0.9863
//TrafficCarQuatW[12] = -0.1651
//TrafficCarRecording[12] = 13
//TrafficCarStartime[12] = 34386.0000
//TrafficCarModel[12] = RancherXL

//ParkedCarPos[3] = <<1131.2111, -1759.2866, 29.2392>>
//ParkedCarQuatX[3] = -0.0014
//ParkedCarQuatY[3] = 0.0035
//ParkedCarQuatZ[3] = 0.1647
//ParkedCarQuatW[3] = 0.9863
//ParkedCarModel[3] = scrap

TrafficCarPos[13] = <<1099.1915, -1769.3442, 28.7382>>
TrafficCarQuatX[13] = -0.0043
TrafficCarQuatY[13] = -0.0008
TrafficCarQuatZ[13] = 0.9836
TrafficCarQuatW[13] = -0.1805
TrafficCarRecording[13] = 14
TrafficCarStartime[13] = 34980.0000
TrafficCarModel[13] = landstalker

TrafficCarPos[14] = <<1114.1964, -1746.2981, 28.8014>>
TrafficCarQuatX[14] = -0.0049
TrafficCarQuatY[14] = -0.0009
TrafficCarQuatZ[14] = 0.1804
TrafficCarQuatW[14] = 0.9836
TrafficCarRecording[14] = 15
TrafficCarStartime[14] = 35244.0000
TrafficCarModel[14] = MESA

TrafficCarPos[15] = <<1099.1005, -1736.9661, 29.1391>>
TrafficCarQuatX[15] = -0.0082
TrafficCarQuatY[15] = -0.0042
TrafficCarQuatZ[15] = 0.9868
TrafficCarQuatW[15] = -0.1614
TrafficCarRecording[15] = 16
TrafficCarStartime[15] = 35640.0000
TrafficCarModel[15] = Sadler

//TrafficCarPos[16] = <<1084.1342, -1735.9482, 35.1973>>
//TrafficCarQuatX[16] = -0.0034
//TrafficCarQuatY[16] = 0.0027
//TrafficCarQuatZ[16] = 0.8110
//TrafficCarQuatW[16] = 0.5850
//TrafficCarRecording[16] = 17
//TrafficCarStartime[16] = 35970.0000
//TrafficCarModel[16] = MESA
//
//TrafficCarPos[17] = <<1064.2612, -1724.5662, 35.2321>>
//TrafficCarQuatX[17] = -0.0012
//TrafficCarQuatY[17] = 0.0073
//TrafficCarQuatZ[17] = 0.9870
//TrafficCarQuatW[17] = -0.1603
//TrafficCarRecording[17] = 18
//TrafficCarStartime[17] = 36564.0000
//TrafficCarModel[17] = regina

TrafficCarPos[18] = <<1077.4941, -1709.0830, 29.1660>>
TrafficCarQuatX[18] = 0.0002
TrafficCarQuatY[18] = 0.0000
TrafficCarQuatZ[18] = 0.9885
TrafficCarQuatW[18] = -0.1514
TrafficCarRecording[18] = 19
TrafficCarStartime[18] = 36630.0000
TrafficCarModel[18] = Sadler

TrafficCarPos[19] = <<1081.4229, -1699.4463, 29.1073>>
TrafficCarQuatX[19] = 0.0002
TrafficCarQuatY[19] = -0.0018
TrafficCarQuatZ[19] = 0.9880
TrafficCarQuatW[19] = -0.1547
TrafficCarRecording[19] = 20
TrafficCarStartime[19] = 37158.0000
TrafficCarModel[19] = regina

TrafficCarPos[20] = <<1072.8810, -1675.1649, 28.9240>>
TrafficCarQuatX[20] = -0.0029
TrafficCarQuatY[20] = -0.0021
TrafficCarQuatZ[20] = 0.9915
TrafficCarQuatW[20] = -0.1302
TrafficCarRecording[20] = 21
TrafficCarStartime[20] = 37620.0000
TrafficCarModel[20] = MESA

TrafficCarPos[21] = <<1093.0076, -1658.0027, 28.9252>>
TrafficCarQuatX[21] = -0.0010
TrafficCarQuatY[21] = 0.0004
TrafficCarQuatZ[21] = 0.1187
TrafficCarQuatW[21] = 0.9929
TrafficCarRecording[21] = 22
TrafficCarStartime[21] = 38082.0000
TrafficCarModel[21] = MESA

TrafficCarPos[22] = <<1064.4675, -1657.8715, 28.8650>>
TrafficCarQuatX[22] = -0.0043
TrafficCarQuatY[22] = 0.0001
TrafficCarQuatZ[22] = 0.9934
TrafficCarQuatW[22] = -0.1147
TrafficCarRecording[22] = 23
TrafficCarStartime[22] = 38148.0000
TrafficCarModel[22] = landstalker

TrafficCarPos[23] = <<1095.3093, -1672.2139, 29.0386>>
TrafficCarQuatX[23] = 0.0023
TrafficCarQuatY[23] = 0.0003
TrafficCarQuatZ[23] = 0.1384
TrafficCarQuatW[23] = 0.9904
TrafficCarRecording[23] = 24
TrafficCarStartime[23] = 38346.0000
TrafficCarModel[23] = RancherXL

TrafficCarPos[24] = <<1081.6741, -1633.3407, 28.9063>>
TrafficCarQuatX[24] = -0.0005
TrafficCarQuatY[24] = -0.0000
TrafficCarQuatZ[24] = 0.0996
TrafficCarQuatW[24] = 0.9950
TrafficCarRecording[24] = 25
TrafficCarStartime[24] = 38742.0000
TrafficCarModel[24] = landstalker

TrafficCarPos[25] = <<1064.9360, -1637.2181, 29.3989>>
TrafficCarQuatX[25] = -0.0120
TrafficCarQuatY[25] = 0.0019
TrafficCarQuatZ[25] = 0.9947
TrafficCarQuatW[25] = -0.1019
TrafficCarRecording[25] = 26
TrafficCarStartime[25] = 38742.0000
TrafficCarModel[25] = scrap

TrafficCarPos[26] = <<1091.1405, -1629.9049, 28.8929>>
TrafficCarQuatX[26] = -0.0007
TrafficCarQuatY[26] = 0.0035
TrafficCarQuatZ[26] = 0.1006
TrafficCarQuatW[26] = 0.9949
TrafficCarRecording[26] = 27
TrafficCarStartime[26] = 38808.0000
TrafficCarModel[26] = landstalker

TrafficCarPos[27] = <<1068.2368, -1623.2585, 28.7770>>
TrafficCarQuatX[27] = -0.0133
TrafficCarQuatY[27] = 0.0019
TrafficCarQuatZ[27] = 0.9956
TrafficCarQuatW[27] = -0.0930
TrafficCarRecording[27] = 28
TrafficCarStartime[27] = 39336.0000
TrafficCarModel[27] = Phoenix

//TrafficCarPos[28] = <<1063.3774, -1596.8400, 28.8528>>
//TrafficCarQuatX[28] = -0.0173
//TrafficCarQuatY[28] = 0.0068
//TrafficCarQuatZ[28] = 0.9990
//TrafficCarQuatW[28] = -0.0415
//TrafficCarRecording[28] = 29
//TrafficCarStartime[28] = 39930.0000
//TrafficCarModel[28] = RancherXL

TrafficCarPos[29] = <<1057.1163, -1625.4204, 29.3340>>
TrafficCarQuatX[29] = -0.0015
TrafficCarQuatY[29] = -0.0010
TrafficCarQuatZ[29] = 0.9961
TrafficCarQuatW[29] = -0.0880
TrafficCarRecording[29] = 30
TrafficCarStartime[29] = 40194.0000
TrafficCarModel[29] = Biff

TrafficCarPos[30] = <<1062.4103, -1586.4337, 28.4771>>
TrafficCarQuatX[30] = -0.0131
TrafficCarQuatY[30] = 0.0092
TrafficCarQuatZ[30] = 0.9990
TrafficCarQuatW[30] = -0.0425
TrafficCarRecording[30] = 31
TrafficCarStartime[30] = 40458.0000
TrafficCarModel[30] = landstalker

TrafficCarPos[31] = <<1050.4426, -1570.4543, 28.5423>>
TrafficCarQuatX[31] = 0.0064
TrafficCarQuatY[31] = 0.0053
TrafficCarQuatZ[31] = 0.9986
TrafficCarQuatW[31] = -0.0523
TrafficCarRecording[31] = 32
TrafficCarStartime[31] = 40986.0000
TrafficCarModel[31] = Biff

//TrafficCarPos[32] = <<1053.5391, -1520.3832, 27.5834>>
//TrafficCarQuatX[32] = -0.0026
//TrafficCarQuatY[32] = -0.0005
//TrafficCarQuatZ[32] = 1.0000
//TrafficCarQuatW[32] = -0.0075
//TrafficCarRecording[32] = 33
//TrafficCarStartime[32] = 43098.0000
//TrafficCarModel[32] = landstalker

TrafficCarPos[33] = <<1054.1426, -1500.4709, 27.6671>>
TrafficCarQuatX[33] = -0.0120
TrafficCarQuatY[33] = -0.0013
TrafficCarQuatZ[33] = 0.9999
TrafficCarQuatW[33] = -0.0036
TrafficCarRecording[33] = 34
TrafficCarStartime[33] = 43362.0000
TrafficCarModel[33] = bison

TrafficCarPos[34] = <<1059.6545, -1521.0797, 27.4954>>
TrafficCarQuatX[34] = 0.0104
TrafficCarQuatY[34] = 0.0022
TrafficCarQuatZ[34] = 0.9999
TrafficCarQuatW[34] = -0.0083
TrafficCarRecording[34] = 35
TrafficCarStartime[34] = 43560.0000
TrafficCarModel[34] = MESA

TrafficCarPos[35] = <<1040.5317, -1479.9528, 27.6810>>
TrafficCarQuatX[35] = 0.0003
TrafficCarQuatY[35] = -0.0077
TrafficCarQuatZ[35] = 0.9991
TrafficCarQuatW[35] = -0.0414
TrafficCarRecording[35] = 36
TrafficCarStartime[35] = 44022.0000
TrafficCarModel[35] = buccaneer

TrafficCarPos[36] = <<1043.9675, -1454.2734, 28.6681>>
TrafficCarQuatX[36] = 0.0002
TrafficCarQuatY[36] = -0.0082
TrafficCarQuatZ[36] = 0.9991
TrafficCarQuatW[36] = -0.0409
TrafficCarRecording[36] = 37
TrafficCarStartime[36] = 44880.0000
TrafficCarModel[36] = Sadler

TrafficCarPos[37] = <<1050.3926, -1443.6427, 36.4312>>
TrafficCarQuatX[37] = -0.0034
TrafficCarQuatY[37] = -0.0023
TrafficCarQuatZ[37] = 0.7072
TrafficCarQuatW[37] = -0.7070
TrafficCarRecording[37] = 38
TrafficCarStartime[37] = 45210.0000
TrafficCarModel[37] = Sadler

TrafficCarPos[38] = <<1037.0913, -1432.8300, 28.8182>>
TrafficCarQuatX[38] = 0.0046
TrafficCarQuatY[38] = -0.0070
TrafficCarQuatZ[38] = 0.9998
TrafficCarQuatW[38] = -0.0207
TrafficCarRecording[38] = 39
TrafficCarStartime[38] = 45540.0000
TrafficCarModel[38] = bison

TrafficCarPos[39] = <<1058.9618, -1423.1050, 29.0086>>
TrafficCarQuatX[39] = 0.0002
TrafficCarQuatY[39] = -0.0091
TrafficCarQuatZ[39] = 0.9999
TrafficCarQuatW[39] = -0.0054
TrafficCarRecording[39] = 40
TrafficCarStartime[39] = 45870.0000
TrafficCarModel[39] = Sadler

TrafficCarPos[40] = <<1023.6323, -1443.6104, 35.5696>>
TrafficCarQuatX[40] = -0.0287
TrafficCarQuatY[40] = -0.0037
TrafficCarQuatZ[40] = 0.7069
TrafficCarQuatW[40] = -0.7068
TrafficCarRecording[40] = 41
TrafficCarStartime[40] = 46332.0000
TrafficCarModel[40] = landstalker

TrafficCarPos[41] = <<1041.7249, -1405.9795, 29.6160>>
TrafficCarQuatX[41] = 0.0055
TrafficCarQuatY[41] = -0.0096
TrafficCarQuatZ[41] = 0.9999
TrafficCarQuatW[41] = -0.0078
TrafficCarRecording[41] = 42
TrafficCarStartime[41] = 46398.0000
TrafficCarModel[41] = scrap

TrafficCarPos[42] = <<1042.1285, -1387.2562, 29.8631>>
TrafficCarQuatX[42] = 0.0003
TrafficCarQuatY[42] = -0.0281
TrafficCarQuatZ[42] = 0.9996
TrafficCarQuatW[42] = -0.0084
TrafficCarRecording[42] = 43
TrafficCarStartime[42] = 46992.0000
TrafficCarModel[42] = bison

TrafficCarPos[43] = <<1057.5828, -1388.1931, 28.9498>>
TrafficCarQuatX[43] = -0.0066
TrafficCarQuatY[43] = 0.0027
TrafficCarQuatZ[43] = 1.0000
TrafficCarQuatW[43] = -0.0064
TrafficCarRecording[43] = 44
TrafficCarStartime[43] = 47058.0000
TrafficCarModel[43] = MESA

TrafficCarPos[44] = <<1041.4845, -1369.8064, 31.3912>>
TrafficCarQuatX[44] = -0.0046
TrafficCarQuatY[44] = -0.0462
TrafficCarQuatZ[44] = 0.9988
TrafficCarQuatW[44] = -0.0130
TrafficCarRecording[44] = 45
TrafficCarStartime[44] = 47586.0000
TrafficCarModel[44] = Sadler

TrafficCarPos[45] = <<1035.0221, -1351.2770, 32.9924>>
TrafficCarQuatX[45] = 0.0017
TrafficCarQuatY[45] = -0.0618
TrafficCarQuatZ[45] = 0.9978
TrafficCarQuatW[45] = -0.0247
TrafficCarRecording[45] = 46
TrafficCarStartime[45] = 48246.0000
TrafficCarModel[45] = buccaneer

TrafficCarPos[46] = <<1034.5239, -1336.8646, 35.3449>>
TrafficCarQuatX[46] = 0.0179
TrafficCarQuatY[46] = -0.0542
TrafficCarQuatZ[46] = 0.9980
TrafficCarQuatW[46] = -0.0280
TrafficCarRecording[46] = 47
TrafficCarStartime[46] = 48840.0000
TrafficCarModel[46] = Sadler

TrafficCarPos[47] = <<1033.4775, -1319.9642, 37.1015>>
TrafficCarQuatX[47] = 0.0181
TrafficCarQuatY[47] = -0.0545
TrafficCarQuatZ[47] = 0.9979
TrafficCarQuatW[47] = -0.0296
TrafficCarRecording[47] = 48
TrafficCarStartime[47] = 49368.0000
TrafficCarModel[47] = landstalker

TrafficCarPos[48] = <<1054.6504, -1317.9685, 27.0912>>
TrafficCarQuatX[48] = -0.0006
TrafficCarQuatY[48] = 0.0219
TrafficCarQuatZ[48] = 0.9994
TrafficCarQuatW[48] = -0.0279
TrafficCarRecording[48] = 49
TrafficCarStartime[48] = 49566.0000
TrafficCarModel[48] = Sadler

TrafficCarPos[49] = <<1032.1355, -1308.3929, 38.3468>>
TrafficCarQuatX[49] = 0.0050
TrafficCarQuatY[49] = -0.0471
TrafficCarQuatZ[49] = 0.9985
TrafficCarQuatW[49] = -0.0282
TrafficCarRecording[49] = 50
TrafficCarStartime[49] = 49764.0000
TrafficCarModel[49] = bison

TrafficCarPos[50] = <<1038.0043, -1304.0952, 38.6910>>
TrafficCarQuatX[50] = -0.0042
TrafficCarQuatY[50] = -0.0460
TrafficCarQuatZ[50] = 0.9986
TrafficCarQuatW[50] = -0.0256
TrafficCarRecording[50] = 51
TrafficCarStartime[50] = 49896.0000
TrafficCarModel[50] = MESA

TrafficCarPos[51] = <<1032.8864, -1272.8844, 41.7283>>
TrafficCarQuatX[51] = -0.0066
TrafficCarQuatY[51] = -0.0480
TrafficCarQuatZ[51] = 0.9981
TrafficCarQuatW[51] = 0.0392
TrafficCarRecording[51] = 52
TrafficCarStartime[51] = 50886.0000
TrafficCarModel[51] = Sadler

TrafficCarPos[52] = <<1038.0369, -1273.2704, 41.6203>>
TrafficCarQuatX[52] = -0.0151
TrafficCarQuatY[52] = -0.0521
TrafficCarQuatZ[52] = 0.9977
TrafficCarQuatW[52] = 0.0409
TrafficCarRecording[52] = 53
TrafficCarStartime[52] = 50952.0000
TrafficCarModel[52] = landstalker

TrafficCarPos[53] = <<1040.5094, -1256.9590, 42.9299>>
TrafficCarQuatX[53] = -0.0102
TrafficCarQuatY[53] = -0.0355
TrafficCarQuatZ[53] = 0.9919
TrafficCarQuatW[53] = 0.1214
TrafficCarRecording[53] = 54
TrafficCarStartime[53] = 51480.0000
TrafficCarModel[53] = MESA

TrafficCarPos[54] = <<1053.2091, -1226.4725, 45.3710>>
TrafficCarQuatX[54] = -0.0056
TrafficCarQuatY[54] = -0.0329
TrafficCarQuatZ[54] = 0.9697
TrafficCarQuatW[54] = 0.2421
TrafficCarRecording[54] = 55
TrafficCarStartime[54] = 52536.0000
TrafficCarModel[54] = landstalker

TrafficCarPos[55] = <<1047.1648, -1195.5914, 55.7625>>
TrafficCarQuatX[55] = -0.0034
TrafficCarQuatY[55] = 0.0059
TrafficCarQuatZ[55] = -0.6925
TrafficCarQuatW[55] = 0.7214
TrafficCarRecording[55] = 56
TrafficCarStartime[55] = 53658.0000
TrafficCarModel[55] = scrap

TrafficCarPos[56] = <<1025.2529, -1190.3019, 55.3269>>
TrafficCarQuatX[56] = 0.0179
TrafficCarQuatY[56] = 0.0029
TrafficCarQuatZ[56] = -0.6945
TrafficCarQuatW[56] = 0.7192
TrafficCarRecording[56] = 57
TrafficCarStartime[56] = 53988.0000
TrafficCarModel[56] = landstalker

//TrafficCarPos[57] = <<1040.5273, -1179.9304, 55.2897>>
//TrafficCarQuatX[57] = -0.0108
//TrafficCarQuatY[57] = 0.0072
//TrafficCarQuatZ[57] = 0.7213
//TrafficCarQuatW[57] = 0.6926
//TrafficCarRecording[57] = 58
//TrafficCarStartime[57] = 54252.0000
//TrafficCarModel[57] = bison
//
//TrafficCarPos[58] = <<1064.3732, -1177.5841, 55.2528>>
//TrafficCarQuatX[58] = -0.0102
//TrafficCarQuatY[58] = 0.0084
//TrafficCarQuatZ[58] = 0.7223
//TrafficCarQuatW[58] = 0.6915
//TrafficCarRecording[58] = 59
//TrafficCarStartime[58] = 54516.0000
//TrafficCarModel[58] = bison

TrafficCarPos[59] = <<1039.6971, -1166.8888, 46.7159>>
TrafficCarQuatX[59] = 0.0047
TrafficCarQuatY[59] = 0.0091
TrafficCarQuatZ[59] = -0.3573
TrafficCarQuatW[59] = 0.9339
TrafficCarRecording[59] = 60
TrafficCarStartime[59] = 54582.0000
TrafficCarModel[59] = landstalker

TrafficCarPos[60] = <<1067.2932, -1195.4490, 47.0312>>
TrafficCarQuatX[60] = 0.0167
TrafficCarQuatY[60] = -0.0055
TrafficCarQuatZ[60] = 0.9319
TrafficCarQuatW[60] = 0.3623
TrafficCarRecording[60] = 61
TrafficCarStartime[60] = 54714.0000
TrafficCarModel[60] = Sadler

TrafficCarPos[61] = <<1089.4803, -1183.5569, 46.0068>>
TrafficCarQuatX[61] = 0.0143
TrafficCarQuatY[61] = 0.0276
TrafficCarQuatZ[61] = 0.8926
TrafficCarQuatW[61] = 0.4497
TrafficCarRecording[61] = 62
TrafficCarStartime[61] = 54978.0000
TrafficCarModel[61] = landstalker

//TrafficCarPos[62] = <<1109.4390, -1176.6676, 55.2548>>
//TrafficCarQuatX[62] = -0.0067
//TrafficCarQuatY[62] = 0.0161
//TrafficCarQuatZ[62] = 0.7271
//TrafficCarQuatW[62] = 0.6864
//TrafficCarRecording[62] = 63
//TrafficCarStartime[62] = 55308.0000
//TrafficCarModel[62] = MESA
//
//TrafficCarPos[63] = <<1115.8499, -1169.9240, 55.0264>>
//TrafficCarQuatX[63] = 0.0015
//TrafficCarQuatY[63] = 0.0227
//TrafficCarQuatZ[63] = 0.7272
//TrafficCarQuatW[63] = 0.6860
//TrafficCarRecording[63] = 64
//TrafficCarStartime[63] = 56100.0000
//TrafficCarModel[63] = MESA

//TrafficCarPos[64] = <<1163.1779, -1149.7906, 44.3732>>
//TrafficCarQuatX[64] = -0.0117
//TrafficCarQuatY[64] = -0.0227
//TrafficCarQuatZ[64] = 0.7516
//TrafficCarQuatW[64] = 0.6592
//TrafficCarRecording[64] = 65
//TrafficCarStartime[64] = 58608.0000
//TrafficCarModel[64] = Sadler

//TrafficCarPos[65] = <<1198.5988, -1166.0538, 51.0615>>
//TrafficCarQuatX[65] = 0.0010
//TrafficCarQuatY[65] = -0.0000
//TrafficCarQuatZ[65] = 0.7925
//TrafficCarQuatW[65] = 0.6099
//TrafficCarRecording[65] = 66
//TrafficCarStartime[65] = 59070.0000
//TrafficCarModel[65] = landstalker

TrafficCarPos[66] = <<1209.6885, -1146.9996, 47.4201>>
TrafficCarQuatX[66] = -0.0274
TrafficCarQuatY[66] = -0.0234
TrafficCarQuatZ[66] = 0.7991
TrafficCarQuatW[66] = 0.6002
TrafficCarRecording[66] = 67
TrafficCarStartime[66] = 59334.0000
TrafficCarModel[66] = Sadler

TrafficCarPos[67] = <<1252.3525, -1147.1350, 51.1307>>
TrafficCarQuatX[67] = -0.0036
TrafficCarQuatY[67] = 0.0025
TrafficCarQuatZ[67] = 0.8345
TrafficCarQuatW[67] = 0.5509
TrafficCarRecording[67] = 68
TrafficCarStartime[67] = 60720.0000
TrafficCarModel[67] = landstalker

TrafficCarPos[68] = <<1283.0724, -1135.6932, 51.1078>>
TrafficCarQuatX[68] = -0.0035
TrafficCarQuatY[68] = 0.0026
TrafficCarQuatZ[68] = 0.8366
TrafficCarQuatW[68] = 0.5477
TrafficCarRecording[68] = 69
TrafficCarStartime[68] = 61776.0000
TrafficCarModel[68] = MESA

TrafficCarPos[69] = <<1316.0417, -1117.9481, 50.8728>>
TrafficCarQuatX[69] = -0.0012
TrafficCarQuatY[69] = -0.0019
TrafficCarQuatZ[69] = 0.8533
TrafficCarQuatW[69] = 0.5214
TrafficCarRecording[69] = 70
TrafficCarStartime[69] = 63030.0000
TrafficCarModel[69] = buccaneer

TrafficCarPos[70] = <<1319.2922, -1097.9714, 51.1957>>
TrafficCarQuatX[70] = 0.0030
TrafficCarQuatY[70] = -0.0108
TrafficCarQuatZ[70] = 0.8429
TrafficCarQuatW[70] = 0.5380
TrafficCarRecording[70] = 71
TrafficCarStartime[70] = 63360.0000
TrafficCarModel[70] = bison

TrafficCarPos[71] = <<1342.1870, -1117.7548, 51.8750>>
TrafficCarQuatX[71] = 0.0065
TrafficCarQuatY[71] = -0.0048
TrafficCarQuatZ[71] = -0.5362
TrafficCarQuatW[71] = 0.8441
TrafficCarRecording[71] = 72
TrafficCarStartime[71] = 63888.0000
TrafficCarModel[71] = Sadler

TrafficCarPos[72] = <<1351.5930, -1099.8505, 52.0916>>
TrafficCarQuatX[72] = -0.0082
TrafficCarQuatY[72] = -0.0074
TrafficCarQuatZ[72] = 0.8561
TrafficCarQuatW[72] = 0.5166
TrafficCarRecording[72] = 73
TrafficCarStartime[72] = 64350.0000
TrafficCarModel[72] = scrap

TrafficCarPos[73] = <<1358.1727, -1090.9425, 51.8101>>
TrafficCarQuatX[73] = -0.0058
TrafficCarQuatY[73] = -0.0096
TrafficCarQuatZ[73] = 0.8579
TrafficCarQuatW[73] = 0.5136
TrafficCarRecording[73] = 74
TrafficCarStartime[73] = 64680.0000
TrafficCarModel[73] = landstalker

TrafficCarPos[74] = <<1362.6317, -1105.3058, 52.5445>>
TrafficCarQuatX[74] = 0.0076
TrafficCarQuatY[74] = -0.0046
TrafficCarQuatZ[74] = -0.5148
TrafficCarQuatW[74] = 0.8573
TrafficCarRecording[74] = 75
TrafficCarStartime[74] = 64680.0000
TrafficCarModel[74] = scrap

TrafficCarPos[75] = <<1382.2256, -1083.7041, 52.4738>>
TrafficCarQuatX[75] = -0.0107
TrafficCarQuatY[75] = -0.0093
TrafficCarQuatZ[75] = 0.8582
TrafficCarQuatW[75] = 0.5131
TrafficCarRecording[75] = 76
TrafficCarStartime[75] = 65472.0000
TrafficCarModel[75] = landstalker

//TrafficCarPos[76] = <<1400.5847, -1074.1249, 53.0939>>
//TrafficCarQuatX[76] = -0.0110
//TrafficCarQuatY[76] = -0.0098
//TrafficCarQuatZ[76] = 0.8591
//TrafficCarQuatW[76] = 0.5116
//TrafficCarRecording[76] = 77
//TrafficCarStartime[76] = 66132.0000
//TrafficCarModel[76] = MESA
//
//TrafficCarPos[77] = <<1398.4880, -1069.4480, 53.0572>>
//TrafficCarQuatX[77] = -0.0072
//TrafficCarQuatY[77] = -0.0117
//TrafficCarQuatZ[77] = 0.8585
//TrafficCarQuatW[77] = 0.5126
//TrafficCarRecording[77] = 78
//TrafficCarStartime[77] = 66198.0000
//TrafficCarModel[77] = landstalker

TrafficCarPos[78] = <<1422.3512, -1055.3960, 54.1407>>
TrafficCarQuatX[78] = -0.0096
TrafficCarQuatY[78] = -0.0107
TrafficCarQuatZ[78] = 0.8592
TrafficCarQuatW[78] = 0.5114
TrafficCarRecording[78] = 79
TrafficCarStartime[78] = 67056.0000
TrafficCarModel[78] = Sadler

TrafficCarPos[79] = <<1437.4639, -1047.1901, 54.4153>>
TrafficCarQuatX[79] = -0.0089
TrafficCarQuatY[79] = -0.0119
TrafficCarQuatZ[79] = 0.8666
TrafficCarQuatW[79] = 0.4987
TrafficCarRecording[79] = 80
TrafficCarStartime[79] = 67584.0000
TrafficCarModel[79] = MESA

TrafficCarPos[80] = <<1501.8615, -1018.0383, 57.1928>>
TrafficCarQuatX[80] = -0.0105
TrafficCarQuatY[80] = -0.0094
TrafficCarQuatZ[80] = 0.8616
TrafficCarQuatW[80] = 0.5073
TrafficCarRecording[80] = 81
TrafficCarStartime[80] = 70092.0000
TrafficCarModel[80] = scrap

TrafficCarPos[81] = <<1537.7510, -1015.4526, 57.5874>>
TrafficCarQuatX[81] = 0.0134
TrafficCarQuatY[81] = -0.0039
TrafficCarQuatZ[81] = -0.5048
TrafficCarQuatW[81] = 0.8632
TrafficCarRecording[81] = 82
TrafficCarStartime[81] = 70818.0000
TrafficCarModel[81] = MESA

TrafficCarPos[82] = <<1547.0856, -985.7202, 58.2990>>
TrafficCarQuatX[82] = -0.0174
TrafficCarQuatY[82] = -0.0130
TrafficCarQuatZ[82] = 0.8630
TrafficCarQuatW[82] = 0.5047
TrafficCarRecording[82] = 83
TrafficCarStartime[82] = 71544.0000
TrafficCarModel[82] = landstalker

//TrafficCarPos[83] = <<1558.4414, -980.5378, 59.1532>>
//TrafficCarQuatX[83] = -0.0187
//TrafficCarQuatY[83] = -0.0143
//TrafficCarQuatZ[83] = 0.8623
//TrafficCarQuatW[83] = 0.5059
//TrafficCarRecording[83] = 84
//TrafficCarStartime[83] = 71940.0000
//TrafficCarModel[83] = Sadler

TrafficCarPos[84] = <<1572.3722, -988.5083, 59.2547>>
TrafficCarQuatX[84] = 0.0108
TrafficCarQuatY[84] = -0.0245
TrafficCarQuatZ[84] = -0.4971
TrafficCarQuatW[84] = 0.8673
TrafficCarRecording[84] = 85
TrafficCarStartime[84] = 72072.0000
TrafficCarModel[84] = bison
//
//TrafficCarPos[85] = <<1568.1069, -980.8013, 59.5047>>
//TrafficCarQuatX[85] = -0.0003
//TrafficCarQuatY[85] = -0.0259
//TrafficCarQuatZ[85] = 0.8626
//TrafficCarQuatW[85] = 0.5053
//TrafficCarRecording[85] = 86
//TrafficCarStartime[85] = 72138.0000
//TrafficCarModel[85] = Sadler

TrafficCarPos[86] = <<1570.7823, -973.6722, 59.5908>>
TrafficCarQuatX[86] = -0.0207
TrafficCarQuatY[86] = -0.0139
TrafficCarQuatZ[86] = 0.8682
TrafficCarQuatW[86] = 0.4955
TrafficCarRecording[86] = 87
TrafficCarStartime[86] = 72270.0000
TrafficCarModel[86] = bison

TrafficCarPos[87] = <<1586.4951, -987.2821, 60.1358>>
TrafficCarQuatX[87] = 0.0240
TrafficCarQuatY[87] = -0.0041
TrafficCarQuatZ[87] = -0.5017
TrafficCarQuatW[87] = 0.8647
TrafficCarRecording[87] = 88
TrafficCarStartime[87] = 72468.0000
TrafficCarModel[87] = Sadler

TrafficCarPos[88] = <<1588.4323, -962.2726, 60.5369>>
TrafficCarQuatX[88] = -0.0270
TrafficCarQuatY[88] = -0.0121
TrafficCarQuatZ[88] = 0.8626
TrafficCarQuatW[88] = 0.5051
TrafficCarRecording[88] = 89
TrafficCarStartime[88] = 72930.0000
TrafficCarModel[88] = bison

TrafficCarPos[89] = <<1604.5709, -959.2837, 61.3730>>
TrafficCarQuatX[89] = -0.0047
TrafficCarQuatY[89] = -0.0256
TrafficCarQuatZ[89] = 0.8673
TrafficCarQuatW[89] = 0.4972
TrafficCarRecording[89] = 90
TrafficCarStartime[89] = 73392.0000
TrafficCarModel[89] = MESA

TrafficCarPos[90] = <<1620.7482, -967.0914, 62.3388>>
TrafficCarQuatX[90] = 0.0268
TrafficCarQuatY[90] = -0.0015
TrafficCarQuatZ[90] = -0.4947
TrafficCarQuatW[90] = 0.8686
TrafficCarRecording[90] = 91
TrafficCarStartime[90] = 73722.0000
TrafficCarModel[90] = scrap

TrafficCarPos[91] = <<1622.0424, -959.2253, 61.8929>>
TrafficCarQuatX[91] = 0.0124
TrafficCarQuatY[91] = -0.0272
TrafficCarQuatZ[91] = -0.4953
TrafficCarQuatW[91] = 0.8682
TrafficCarRecording[91] = 92
TrafficCarStartime[91] = 73788.0000
TrafficCarModel[91] = buccaneer

TrafficCarPos[92] = <<1633.7638, -942.4766, 63.5730>>
TrafficCarQuatX[92] = -0.0050
TrafficCarQuatY[92] = -0.0265
TrafficCarQuatZ[92] = 0.8694
TrafficCarQuatW[92] = 0.4933
TrafficCarRecording[92] = 93
TrafficCarStartime[92] = 74316.0000
TrafficCarModel[92] = scrap

TrafficCarPos[93] = <<1654.6025, -946.0850, 63.7836>>
TrafficCarQuatX[93] = 0.0279
TrafficCarQuatY[93] = -0.0011
TrafficCarQuatZ[93] = -0.4872
TrafficCarQuatW[93] = 0.8728
TrafficCarRecording[93] = 94
TrafficCarStartime[93] = 74844.0000
TrafficCarModel[93] = buccaneer

TrafficCarPos[94] = <<1658.2738, -938.0108, 64.2821>>
TrafficCarQuatX[94] = 0.0134
TrafficCarQuatY[94] = -0.0275
TrafficCarQuatZ[94] = -0.4875
TrafficCarQuatW[94] = 0.8726
TrafficCarRecording[94] = 95
TrafficCarStartime[94] = 74976.0000
TrafficCarModel[94] = MESA

TrafficCarPos[95] = <<1695.0702, -920.9435, 66.5076>>
TrafficCarQuatX[95] = 0.0270
TrafficCarQuatY[95] = -0.0047
TrafficCarQuatZ[95] = -0.4780
TrafficCarQuatW[95] = 0.8779
TrafficCarRecording[95] = 96
TrafficCarStartime[95] = 76164.0000
TrafficCarModel[95] = landstalker

TrafficCarPos[96] = <<1693.4834, -916.1393, 66.5571>>
TrafficCarQuatX[96] = 0.0145
TrafficCarQuatY[96] = -0.0278
TrafficCarQuatZ[96] = -0.4795
TrafficCarQuatW[96] = 0.8770
TrafficCarRecording[96] = 97
TrafficCarStartime[96] = 76164.0000
TrafficCarModel[96] = MESA

TrafficCarPos[97] = <<1686.7013, -909.9734, 66.3996>>
TrafficCarQuatX[97] = 0.0040
TrafficCarQuatY[97] = -0.0340
TrafficCarQuatZ[97] = 0.8790
TrafficCarQuatW[97] = 0.4755
TrafficCarRecording[97] = 98
TrafficCarStartime[97] = 76890.0000
TrafficCarModel[97] = MESA

//TrafficCarPos[98] = <<1724.1670, -896.0611, 68.5712>>
//TrafficCarQuatX[98] = 0.0151
//TrafficCarQuatY[98] = -0.0278
//TrafficCarQuatZ[98] = -0.4665
//TrafficCarQuatW[98] = 0.8839
//TrafficCarRecording[98] = 99
//TrafficCarStartime[98] = 77616.0000
//TrafficCarModel[98] = MESA

TrafficCarPos[99] = <<1739.9091, -890.9354, 69.7360>>
TrafficCarQuatX[99] = 0.0278
TrafficCarQuatY[99] = -0.0047
TrafficCarQuatZ[99] = -0.4636
TrafficCarQuatW[99] = 0.8856
TrafficCarRecording[99] = 100
TrafficCarStartime[99] = 78210.0000
TrafficCarModel[99] = Sadler

TrafficCarPos[100] = <<1756.1343, -873.2136, 71.2733>>
TrafficCarQuatX[100] = 0.0162
TrafficCarQuatY[100] = -0.0264
TrafficCarQuatZ[100] = -0.4587
TrafficCarQuatW[100] = 0.8880
TrafficCarRecording[100] = 101
TrafficCarStartime[100] = 78738.0000
TrafficCarModel[100] = scrap

TrafficCarPos[101] = <<1736.9729, -875.2971, 69.8341>>
TrafficCarQuatX[101] = -0.0037
TrafficCarQuatY[101] = -0.0346
TrafficCarQuatZ[101] = 0.8860
TrafficCarQuatW[101] = 0.4624
TrafficCarRecording[101] = 102
TrafficCarStartime[101] = 78870.0000
TrafficCarModel[101] = MESA

TrafficCarPos[102] = <<1752.1392, -864.4997, 70.8736>>
TrafficCarQuatX[102] = -0.0029
TrafficCarQuatY[102] = -0.0323
TrafficCarQuatZ[102] = 0.8888
TrafficCarQuatW[102] = 0.4572
TrafficCarRecording[102] = 103
TrafficCarStartime[102] = 79728.0000
TrafficCarModel[102] = MESA

TrafficCarPos[103] = <<1808.1072, -842.0761, 74.2613>>
TrafficCarQuatX[103] = 0.0305
TrafficCarQuatY[103] = -0.0055
TrafficCarQuatZ[103] = -0.4493
TrafficCarQuatW[103] = 0.8929
TrafficCarRecording[103] = 104
TrafficCarStartime[103] = 80850.0000
TrafficCarModel[103] = bison

TrafficCarPos[104] = <<1794.4614, -851.4075, 73.2175>>
TrafficCarQuatX[104] = 0.0337
TrafficCarQuatY[104] = -0.0008
TrafficCarQuatZ[104] = -0.4478
TrafficCarQuatW[104] = 0.8935
TrafficCarRecording[104] = 105
TrafficCarStartime[104] = 81180.0000
TrafficCarModel[104] = MESA

TrafficCarPos[105] = <<1813.3606, -820.5766, 75.1247>>
TrafficCarQuatX[105] = 0.0007
TrafficCarQuatY[105] = -0.0375
TrafficCarQuatZ[105] = 0.8942
TrafficCarQuatW[105] = 0.4460
TrafficCarRecording[105] = 106
TrafficCarStartime[105] = 81576.0000
TrafficCarModel[105] = buccaneer

TrafficCarPos[106] = <<1852.8220, -789.6251, 79.0498>>
TrafficCarQuatX[106] = -0.0088
TrafficCarQuatY[106] = -0.0376
TrafficCarQuatZ[106] = 0.8951
TrafficCarQuatW[106] = 0.4442
TrafficCarRecording[106] = 107
TrafficCarStartime[106] = 83424.0000
TrafficCarModel[106] = bison

TrafficCarPos[107] = <<1893.7203, -758.7050, 83.0709>>
TrafficCarQuatX[107] = -0.0095
TrafficCarQuatY[107] = -0.0383
TrafficCarQuatZ[107] = 0.8947
TrafficCarQuatW[107] = 0.4449
TrafficCarRecording[107] = 108
TrafficCarStartime[107] = 85272.0000
TrafficCarModel[107] = emperor

TrafficCarPos[108] = <<1904.6305, -762.5150, 83.5621>>
TrafficCarQuatX[108] = 0.0265
TrafficCarQuatY[108] = -0.0326
TrafficCarQuatZ[108] = -0.4445
TrafficCarQuatW[108] = 0.8948
TrafficCarRecording[108] = 109
TrafficCarStartime[108] = 85404.0000
TrafficCarModel[108] = MESA

TrafficCarPos[109] = <<1915.3466, -743.6140, 85.1017>>
TrafficCarQuatX[109] = -0.0092
TrafficCarQuatY[109] = -0.0389
TrafficCarQuatZ[109] = 0.8965
TrafficCarQuatW[109] = 0.4411
TrafficCarRecording[109] = 110
TrafficCarStartime[109] = 86262.0000
TrafficCarModel[109] = faggio2

TrafficCarPos[110] = <<1931.0511, -749.3928, 85.8679>>
TrafficCarQuatX[110] = 0.0377
TrafficCarQuatY[110] = -0.0082
TrafficCarQuatZ[110] = -0.4422
TrafficCarQuatW[110] = 0.8961
TrafficCarRecording[110] = 111
TrafficCarStartime[110] = 86460.0000
TrafficCarModel[110] = bison

TrafficCarPos[111] = <<1927.4377, -726.9298, 86.6339>>
TrafficCarQuatX[111] = -0.0316
TrafficCarQuatY[111] = -0.0247
TrafficCarQuatZ[111] = 0.8964
TrafficCarQuatW[111] = 0.4414
TrafficCarRecording[111] = 112
TrafficCarStartime[111] = 86988.0000
TrafficCarModel[111] = emperor

TrafficCarPos[112] = <<1946.5721, -719.6090, 87.9184>>
TrafficCarQuatX[112] = -0.0045
TrafficCarQuatY[112] = -0.0353
TrafficCarQuatZ[112] = 0.8969
TrafficCarQuatW[112] = 0.4408
TrafficCarRecording[112] = 113
TrafficCarStartime[112] = 87846.0000
TrafficCarModel[112] = buccaneer

TrafficCarPos[113] = <<1964.1523, -705.9418, 89.6367>>
TrafficCarQuatX[113] = -0.0064
TrafficCarQuatY[113] = -0.0323
TrafficCarQuatZ[113] = 0.8975
TrafficCarQuatW[113] = 0.4398
TrafficCarRecording[113] = 114
TrafficCarStartime[113] = 88836.0000
TrafficCarModel[113] = MESA

//TrafficCarPos[114] = <<1975.2811, -708.3881, 90.4053>>
//TrafficCarQuatX[114] = 0.0231
//TrafficCarQuatY[114] = -0.0244
//TrafficCarQuatZ[114] = -0.4410
//TrafficCarQuatW[114] = 0.8969
//TrafficCarRecording[114] = 115
//TrafficCarStartime[114] = 89034.0000
//TrafficCarModel[114] = Sadler

TrafficCarPos[115] = <<1975.1713, -689.5190, 91.3147>>
TrafficCarQuatX[115] = -0.0255
TrafficCarQuatY[115] = -0.0189
TrafficCarQuatZ[115] = 0.8984
TrafficCarQuatW[115] = 0.4381
TrafficCarRecording[115] = 116
TrafficCarStartime[115] = 89628.0000
TrafficCarModel[115] = scrap

TrafficCarPos[116] = <<1997.5023, -697.2738, 92.0762>>
TrafficCarQuatX[116] = 0.0286
TrafficCarQuatY[116] = -0.0053
TrafficCarQuatZ[116] = -0.4356
TrafficCarQuatW[116] = 0.8997
TrafficCarRecording[116] = 117
TrafficCarStartime[116] = 90090.0000
TrafficCarModel[116] = scrap

TrafficCarPos[117] = <<1981.1449, -691.6869, 91.0842>>
TrafficCarQuatX[117] = 0.0009
TrafficCarQuatY[117] = -0.0345
TrafficCarQuatZ[117] = 0.8992
TrafficCarQuatW[117] = 0.4362
TrafficCarRecording[117] = 118
TrafficCarStartime[117] = 91080.0000
TrafficCarModel[117] = bison

TrafficCarPos[118] = <<2008.4921, -663.4260, 93.0802>>
TrafficCarQuatX[118] = -0.0187
TrafficCarQuatY[118] = -0.0146
TrafficCarQuatZ[118] = 0.8960
TrafficCarQuatW[118] = 0.4434
TrafficCarRecording[118] = 119
TrafficCarStartime[118] = 91146.0000
TrafficCarModel[118] = faggio2

TrafficCarPos[119] = <<2015.6550, -665.2861, 93.5563>>
TrafficCarQuatX[119] = -0.0041
TrafficCarQuatY[119] = -0.0188
TrafficCarQuatZ[119] = 0.9003
TrafficCarQuatW[119] = 0.4348
TrafficCarRecording[119] = 120
TrafficCarStartime[119] = 91410.0000
TrafficCarModel[119] = Sadler

TrafficCarPos[120] = <<2036.6876, -658.4556, 94.0461>>
TrafficCarQuatX[120] = 0.0087
TrafficCarQuatY[120] = -0.0174
TrafficCarQuatZ[120] = -0.4303
TrafficCarQuatW[120] = 0.9025
TrafficCarRecording[120] = 121
TrafficCarStartime[120] = 92268.0000
TrafficCarModel[120] = bison

TrafficCarPos[121] = <<2057.4204, -648.5696, 94.8897>>
TrafficCarQuatX[121] = 0.0111
TrafficCarQuatY[121] = -0.0006
TrafficCarQuatZ[121] = -0.4291
TrafficCarQuatW[121] = 0.9032
TrafficCarRecording[121] = 122
TrafficCarStartime[121] = 93522.0000
TrafficCarModel[121] = Sadler

TrafficCarPos[122] = <<2062.4495, -627.6309, 94.8388>>
TrafficCarQuatX[122] = 0.0086
TrafficCarQuatY[122] = -0.0116
TrafficCarQuatZ[122] = 0.9041
TrafficCarQuatW[122] = 0.4270
TrafficCarRecording[122] = 123
TrafficCarStartime[122] = 93918.0000
TrafficCarModel[122] = faggio2

TrafficCarPos[123] = <<2099.8123, -614.3542, 94.9874>>
TrafficCarQuatX[123] = 0.0058
TrafficCarQuatY[123] = 0.0122
TrafficCarQuatZ[123] = -0.4234
TrafficCarQuatW[123] = 0.9058
TrafficCarRecording[123] = 124
TrafficCarStartime[123] = 96162.0000
TrafficCarModel[123] = buccaneer

TrafficCarPos[124] = <<2100.9805, -605.6147, 95.4105>>
TrafficCarQuatX[124] = -0.0035
TrafficCarQuatY[124] = -0.0077
TrafficCarQuatZ[124] = -0.4234
TrafficCarQuatW[124] = 0.9059
TrafficCarRecording[124] = 125
TrafficCarStartime[124] = 96492.0000
TrafficCarModel[124] = Sadler

TrafficCarPos[125] = <<2145.7358, -555.6513, 94.4273>>
TrafficCarQuatX[125] = 0.0130
TrafficCarQuatY[125] = 0.0050
TrafficCarQuatZ[125] = 0.9106
TrafficCarQuatW[125] = 0.4131
TrafficCarRecording[125] = 126
TrafficCarStartime[125] = 99594.0000
TrafficCarModel[125] = bison

TrafficCarPos[126] = <<2164.9871, -551.8726, 94.0056>>
TrafficCarQuatX[126] = -0.0161
TrafficCarQuatY[126] = -0.0104
TrafficCarQuatZ[126] = -0.4093
TrafficCarQuatW[126] = 0.9122
TrafficCarRecording[126] = 127
TrafficCarStartime[126] = 100188.0000
TrafficCarModel[126] = MESA

TrafficCarPos[127] = <<2204.7410, -501.7440, 92.0787>>
TrafficCarQuatX[127] = 0.0148
TrafficCarQuatY[127] = 0.0124
TrafficCarQuatZ[127] = 0.9190
TrafficCarQuatW[127] = 0.3937
TrafficCarRecording[127] = 128
TrafficCarStartime[127] = 102960.0000
TrafficCarModel[127] = MESA

TrafficCarPos[128] = <<2203.3894, -496.8589, 91.9812>>
TrafficCarQuatX[128] = -0.0093
TrafficCarQuatY[128] = 0.0226
TrafficCarQuatZ[128] = 0.9187
TrafficCarQuatW[128] = 0.3941
TrafficCarRecording[128] = 129
TrafficCarStartime[128] = 103554.0000
TrafficCarModel[128] = bison

TrafficCarPos[129] = <<2252.1982, -500.7823, 85.1706>>
TrafficCarQuatX[129] = -0.0644
TrafficCarQuatY[129] = 0.0372
TrafficCarQuatZ[129] = -0.4282
TrafficCarQuatW[129] = 0.9006
TrafficCarRecording[129] = 130
TrafficCarStartime[129] = 105402.0000
TrafficCarModel[129] = buccaneer

TrafficCarPos[130] = <<2394.7769, -440.2159, 71.9554>>
TrafficCarQuatX[130] = 0.0148
TrafficCarQuatY[130] = -0.0174
TrafficCarQuatZ[130] = 0.4138
TrafficCarQuatW[130] = 0.9101
TrafficCarRecording[130] = 131
TrafficCarStartime[130] = 110616.0000
TrafficCarModel[130] = bison

TrafficCarPos[131] = <<2494.6003, -527.8857, 68.0146>>
TrafficCarQuatX[131] = 0.0134
TrafficCarQuatY[131] = 0.0081
TrafficCarQuatZ[131] = 0.4058
TrafficCarQuatW[131] = 0.9138
TrafficCarRecording[131] = 132
TrafficCarStartime[131] = 114840.0000
TrafficCarModel[131] = faggio2

TrafficCarPos[132] = <<2486.1355, -675.5621, 61.6248>>
TrafficCarQuatX[132] = -0.0054
TrafficCarQuatY[132] = 0.0043
TrafficCarQuatZ[132] = -0.5571
TrafficCarQuatW[132] = 0.8304
TrafficCarRecording[132] = 133
TrafficCarStartime[132] = 120647.0000
TrafficCarModel[132] = Sadler

TrafficCarPos[133] = <<2447.9417, -684.5295, 62.5512>>
TrafficCarQuatX[133] = -0.0044
TrafficCarQuatY[133] = 0.0070
TrafficCarQuatZ[133] = -0.6585
TrafficCarQuatW[133] = 0.7526
TrafficCarRecording[133] = 134
TrafficCarStartime[133] = 121967.0000
TrafficCarModel[133] = sandking

TrafficCarPos[134] = <<2210.5771, -748.1284, 66.6389>>
TrafficCarQuatX[134] = 0.0036
TrafficCarQuatY[134] = 0.0236
TrafficCarQuatZ[134] = 0.9151
TrafficCarQuatW[134] = 0.4025
TrafficCarRecording[134] = 135
TrafficCarStartime[134] = 129684.0000
TrafficCarModel[134] = MESA

TrafficCarPos[135] = <<2121.3787, -849.5552, 78.6098>>
TrafficCarQuatX[135] = 0.0107
TrafficCarQuatY[135] = 0.0167
TrafficCarQuatZ[135] = 0.8930
TrafficCarQuatW[135] = 0.4497
TrafficCarRecording[135] = 136
TrafficCarStartime[135] = 133512.0000
TrafficCarModel[135] = Sadler

TrafficCarPos[136] = <<1986.4703, -906.1359, 78.6676>>
TrafficCarQuatX[136] = -0.0004
TrafficCarQuatY[136] = 0.0006
TrafficCarQuatZ[136] = 0.8470
TrafficCarQuatW[136] = 0.5316
TrafficCarRecording[136] = 137
TrafficCarStartime[136] = 138396.0000
TrafficCarModel[136] = emperor

TrafficCarPos[137] = <<2004.7029, -986.5530, 82.1703>>
TrafficCarQuatX[137] = -0.0683
TrafficCarQuatY[137] = 0.0124
TrafficCarQuatZ[137] = -0.1439
TrafficCarQuatW[137] = 0.9872
TrafficCarRecording[137] = 138
TrafficCarStartime[137] = 142686.0000
TrafficCarModel[137] = MESA


ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
PROC ST_0_MEET()

	switch mission_substage
		Case STAGE_ENTRY	
			
			//Flags
			uberRecordingInitialised = FALSE
			
			REQUEST_VEHICLE_RECORDING(0, "BB_FINALE")
			REQUEST_PTFX_ASSET()
			
			if IS_REPLAY_IN_PROGRESS()
			and not bis_zSkip
				CLEAR_AREA(<< 1334.85095, -2555.59424, 45.58296  >>,200,true)
				mission_substage = 3
			else 		
				Run_Init_CutScene()
				IF bcutsceneLoaded				
					mission_substage++
				endif
			endif
		break
		case 1	
			if IS_CUTSCENE_PLAYING()
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				SET_TODS_CUTSCENE_RUNNING(sTimelapse,false,false,2000,true,false)
				DESTROY_ALL_CAMS()
				CLEAR_AREA(<< 1334.85095, -2555.59424, 45.58296 >>,200,true)
				STOP_FIRE_IN_RANGE(<< 1334.85095, -2555.59424, 45.58296  >>,200)
				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))	
				AND DOES_ENTITY_EXIST(vehs[mvf_trevor_truck].id)
					//-----------load next stage-------
						load_asset_stage(msf_1_CHASE_TREVOR)
					//---------------------------------
					peds[mpf_trevor].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
//					vehs[mvf_trevor_truck].id = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevors_car"))		
					SET_VEHICLE_LIGHTS(vehs[mvf_trevor_truck].id,FORCE_VEHICLE_LIGHTS_ON)
					CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
					CLEAR_PED_PROP(TREV(), ANCHOR_HEAD)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					mission_substage++
				endif
			endif			
		break
		case 2
			if IS_CUTSCENE_PLAYING()
			//frank car				
				if not DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
					IF IS_REPLAY_START_VEHICLE_AVAILABLE()
						vehs[mvf_Frank_car].id = GET_MISSION_START_VEHICLE_INDEX()
						IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL()) 
						OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL()) 
						OR IS_THIS_MODEL_A_BICYCLE(GET_REPLAY_START_VEHICLE_MODEL()) 
						OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL()) 
							IF GET_REPLAY_START_VEHICLE_MODEL() = BUS
								IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
									IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
										SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<1360.2174, -2531.7971, 46.8857>>)
										SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 322.7698)
									ENDIF
								ENDIF	
							ELSE
								IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
									IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
										SET_ENTITY_COORDS(vehs[mvf_Frank_car].id,vSTART_CAR)
										SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, fSTARTCAR)
									ENDIF
								ENDIF
							ENDIF
							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0.0,0.0,0.0>>, 0.0, TRUE, CHAR_FRANKLIN)
						ELSE
							SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_Frank_car].id)
							DELETE_VEHICLE(vehs[mvf_Frank_car].id)
							WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id, CHAR_FRANKLIN, vSTART_CAR, fSTARTCAR, TRUE, VEHICLE_TYPE_CAR)
								PRINTSTRING("waiting on franks car being created") PRINTNL()
								WAIT(0)
							ENDWHILE
						ENDIF
					ELSE
						WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Frank_car].id, CHAR_FRANKLIN, vSTART_CAR, fSTARTCAR, TRUE, VEHICLE_TYPE_CAR)
							PRINTSTRING("waiting on franks car being created") PRINTNL()
							WAIT(0)
						ENDWHILE
					ENDIF
				else	
					IF GET_ENTITY_MODEL(vehs[mvf_Frank_car].id) = BUS
						IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
							SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<1360.2174, -2531.7971, 46.8857>>)
							SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 322.7698)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)
							SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Frank_car].id,false)
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
							SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, vSTART_CAR)
							SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, fSTARTCAR)							
							SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)
							SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Frank_car].id,false)
						ENDIF
					ENDIF
					mission_substage = 3
				endif
			endif		
		break
		case 3		
			IF uberRecordingInitialised = FALSE
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(0, "BB_FINALE")
					INITIALISE_UBER_PLAYBACK("BB_FINALE", 0, FALSE)
					uberRecordingInitialised = TRUE
				ENDIF
			ENDIF
			if not bmusicTriggerStart
				if IS_CUTSCENE_PLAYING()
				and GET_CUTSCENE_TIME() > 38400
					TRIGGER_MUSIC_EVENT("FINA_START")
					bmusicTriggerStart= true
				endif
			endif			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				SET_PED_USING_ACTION_MODE(frank(), TRUE, 10000)
//				TASK_AIM_GUN_AT_COORD(frank(), GET_ENTITY_COORDS(peds[mpf_trevor].id), 3500, TRUE)
				TASK_ENTER_VEHICLE(frank(),vehs[mvf_Frank_car].id,DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_SPRINT, ECF_RESUME_IF_INTERRUPTED)	
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(frank(), TRUE)
				FORCE_PED_MOTION_STATE(frank(), MS_ON_FOOT_SPRINT, false, FAUS_CUTSCENE_EXIT)
//				TASK_AIM_GUN_AT_ENTITY(frank(), peds[mpf_trevor].id, 2000, TRUE)
				println("frank")
				
				REPLAY_STOP_EVENT()
				
				bcsFrank = true
			else
//				Point_Gameplay_cam_at_coord(138.6753)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			endif
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				//trevor
				set_trev()
				SET_PED_INTO_VEHICLE(peds[mpf_trevor].id,vehs[mvf_trevor_truck].id)
				SET_VEHICLE_DOORS_SHUT(vehs[mvf_trevor_truck].id)
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_trevor].id)
				println("trev")
				bcsTrev = true
			endif
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")
				FILL_UBER_RECORDING()
//				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,001,"Finale1")
//				SET_PLAYBACK_SPEED(vehs[mvf_trevor_truck].id,1.3)
				println("trevcar")
				bcsTrevCar = true
			endif
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_weapon")
				IF DOES_ENTITY_EXIST(Weapon_Object)
					GIVE_WEAPON_OBJECT_TO_PED(Weapon_Object, Frank())
				ENDIF
				println("Franklins_weapon")
				bcsgun = true
			endif
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				bcscam = true
			endif
			
			if bcsFrank
			and bcsTrev
			and bcsTrevCar
			and bcsgun
			and bcscam
			or IS_REPLAY_IN_PROGRESS()
				if not bmusicTriggerStart
					TRIGGER_MUSIC_EVENT("FINA_START")
					bmusicTriggerStart	= true
				endif
				remove_cutscene()				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)				
				SET_PLAYER_CONTROL(player_id(),true)
				DISPLAY_RADAR(true)
				DISPLAY_HUD(true)
				
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_truck].id)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,000,"BB_FINALE")
					SET_PLAYBACK_SPEED(vehs[mvf_trevor_truck].id,1)	
				ENDIF
				
				mission_substage	= stage_entry
//				mission_substage = 4
				Mission_Set_Stage(msf_1_CHASE_TREVOR)
			ENDIF
		break
//	   	CASE 4
//	      	INIT_UBER_RECORDING("BB_FINALE")
//	      	mission_substage = 5
//	   	BREAK
//	  	CASE 5
//	      	UPDATE_UBER_RECORDING()
//	   	BREAK
	endswitch
ENDPROC
PROC ST_1_CHASE_TREVOR()	
	switch mission_substage
		Case STAGE_ENTRY				
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Chase Trevor")
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			TRIGGER_MUSIC_EVENT("FINA_CHASE")
			
			//Load in the IPL's for the tanker.
//			REQUEST_IPL("DES_tankercrash")
//			REQUEST_IPL("tankerexp_grp0")
//			REQUEST_IPL("tankercrash_grp1") //BUG FIX 	1416493			
			
			//Flags
			MikeCalledFranDone 	= FALSE
			FrankTurnToSpeak 	= FALSE
			TrevTimeToSpeak 	= FALSE
			failTimerSet 		= FALSE
			iRandomChatTimer 	= 0
			
			if IS_REPLAY_IN_PROGRESS()
			and  bCarRunning
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id)
				ENDIF
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
					REQUEST_VEHICLE_RECORDING(0, "BB_FINALE")
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(0, "BB_FINALE")
						PRINTSTRING("Waiting for recording 0, BB_FINALE loading") PRINTNL()
						WAIT(0)
					ENDWHILE
					
					INITIALISE_UBER_PLAYBACK("BB_FINALE", 0)
					WAIT(0)
					FILL_UBER_RECORDING()
					WAIT(0)
					
//					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,001,"Finale1")
					IF DOES_ENTITY_EXIST(vehs[mvf_trevor_truck].id)
						IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_truck].id)
							START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,0,"BB_FINALE")
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,5000)		
						ENDIF
					ENDIF
				ENDIF		
				IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_frank_car].id)
						SET_VEHICLE_FORWARD_SPEED(vehs[mvf_Frank_car].id,25)	
					ENDIF
				ENDIF
				bCarRunning = false				
			else						
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
					REQUEST_VEHICLE_RECORDING(0, "BB_FINALE")
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(0, "BB_FINALE")
						PRINTSTRING("Waiting for recording 0, BB_FINALE loading") PRINTNL()
						WAIT(0)
					ENDWHILE
					
					INITIALISE_UBER_PLAYBACK("BB_FINALE", 0)
					WAIT(0)
					FILL_UBER_RECORDING()
					WAIT(0)				
				
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,0,"BB_FINALE")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id,5000)
				ENDIF
			endif	
			if isentityalive(TREV())
				SET_ENTITY_INVINCIBLE(trev(),true)
			endif
			vehs[mvf_trevor_truck].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_trevor_truck].id,true)
			PRINT_NOW("FIN1_CHASE",DEFAULT_GOD_TEXT_TIME,1)			
			if not IS_AUDIO_SCENE_ACTIVE("FIN_1_CHASE_IN_CAR")
				START_AUDIO_SCENE("FIN_1_CHASE_IN_CAR")
			endif	
			
			ADD_PED_FOR_DIALOGUE(convo_struct, 0, NULL, "Michael")
			ADD_PED_FOR_DIALOGUE(convo_struct, 1, PLAYER_PED_ID(), "Franklin")
			
			//1991576 - Disable cheats for getting air vehicles, as they'll break the uber chase.
			DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_trevor_truck].id,"FIN_1_TREVORS_TRUCK")
			CLEAR_PED_TASKS(peds[mpf_trevor].id)
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Frank_car].id,true)
			bdialougeplayed = false
			iDelay = get_game_timer()	
			mission_substage++
		break
		case 1
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				PRINT_NOW("FIN1_CHASE",DEFAULT_GOD_TEXT_TIME-500,1)	
			endif
			if IS_PED_IN_ANY_VEHICLE(frank(),true)
			and GET_GAME_TIMER() - iDelay > 500
				bCueFence 			= false
				bCueTruck			= false
				bCueSandking		= false
				bloadscene			= true
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Chase Trevor")
				mission_substage++
			endif
		break
		case 2			
			if bloadscene
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(trev(),<< 1770.0, -1474.3, 113 >>) < 300
					REMOVE_CUTSCENE()
					//-----------load next stage-------
						load_asset_stage(msf_2_Fuel_crash_CS)
					//---------------------------------	
					REQUEST_CUTSCENE("FIN_A_MCS_1")//("FIN_A_MCS_1_alt1")
					PRINTSTRING("FINALE A = cutscene FIN_A_MCS_1 requested") PRINTNL()
					NEW_LOAD_SCENE_START_SPHERE(<<1734.5, -1635.1, 113>>, 100, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
					bloadscene = false
				endif
			ENDIF
			if not bCueFence
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(trev(),<<1551.7487,-2200.6392,76.5152>>) < 100
					CREATE_FORCED_OBJECT(<<1551.7487,-2200.6392,76.5152>>,10,PROP_FNCLINK_02P,true)
					bCueFence = true
				endif
			endif
			if not bCueTruck
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Frank(),<<1516.7759, -2105.4739, 75.6526>>) < 200
					vehs[mvf_scrap].id = CREATE_VEHICLE(scrap,<<1519.6091, -2103.7261, 75.7564>>, 278.7516)					
					peds[mpf_scrap].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_scrap].id,PEDTYPE_MISSION,S_M_Y_XMECH_02)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_scrap].id,true)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(peds[mpf_Scrap].id,vehs[mvf_scrap].id,"FINA_TRUCK",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
					Unload_Asset_Model(sAssetData,scrap)
					Unload_Asset_Model(sAssetData,S_M_Y_XMECH_02)
				
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGH)
				
					bCueTruck = true
				endif
			endif
			if not bCueSandking	
				if IS_ENTITY_AT_COORD(frank(),<<1742.71191, -1321.26575, 90.47571>>,<<25,25,20>>)
					SAFE_RELEASE_PED(peds[mpf_scrap].id)
					SAFE_RELEASE_VEHICLE(vehs[mvf_scrap].id)
					vehs[mvf_Sandking].id = CREATE_VEHICLE(SANDKING,<<1763.2584, -1437.4216, 111.4945>>, 48.2981)
					peds[mpf_sandking].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_Sandking].id,PEDTYPE_MISSION,A_M_M_SALTON_01)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_sandking].id,true)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(peds[mpf_sandking].id,vehs[mvf_Sandking].id,"FINA_SAND",DRIVINGMODE_AVOIDCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
					Unload_Asset_Model(sAssetData,SANDKING)
					Unload_Asset_Model(sAssetData,A_M_M_SALTON_01)
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGH)
					
					bCueSandking = true
				endif
			endif
			
			//Request extra collision for the lampost being hit in the playback
			IF DOES_ENTITY_EXIST(vehs[mvf_trevor_truck].id)
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_truck].id)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
						IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 15000
						AND GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) < 26000
							REQUEST_ADDITIONAL_COLLISION_AT_COORD(<<1284.9, -2114.2, 45.8>>)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Have Michael call Franklin just after they come off the motorway slip road.
			IF MikeCalledFranDone = FALSE
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
					IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 147292	
						ADD_PED_FOR_DIALOGUE(convo_struct, 0, NULL, "Michael")
						ADD_PED_FOR_DIALOGUE(convo_struct, 1, PLAYER_PED_ID(), "Franklin")
						IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(convo_struct, CHAR_MICHAEL, "FIN1AUD", "MIKE_PHONE", CONV_PRIORITY_MEDIUM)
							//Yo, where are you man? I'm chasing T through the oil fields.
							//Shit. Okay. I'm not far away. Try and stop him.
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGH)
							
							MikeCalledFranDone = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			if IS_ENTITY_AT_COORD(vehs[mvf_trevor_truck].id,<< 1732.0365, -1580.7053, 111.6170 >>,<<40,30,10>>)	
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 172930
					SAFE_RELEASE_PED(peds[mpf_sandking].id)
					SAFE_RELEASE_VEHICLE(vehs[mvf_Sandking].id)	
				
					mission_substage++	
				ENDIF
			endif
		break
		case 3
			IF fdist > 250
				Mission_Failed(mff_lost_trevor)
			ENDIF
			//move michael into place 
//			if CREATE_PLAYER_VEHICLE(vehs[mvf_michael_car].id,CHAR_MICHAEL,<< 1647.7448, -1646.3500, 111.2280 >>, 269.6557,true,VEHICLE_TYPE_CAR) 								
				mission_substage++
//			endif
		break
		case 4		
//			if CREATE_PLAYER_PED_ON_FOOT(peds[mpf_michael].id,CHAR_MICHAEL,<< 1647.7448, -1646.3500, 111.2280 >>, 269.6557)			
				SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_NONE )
				KILL_CHASE_HINT_CAM(sHintCam)
				// Bug fix for 1920968
				IF IS_PHONE_ONSCREEN()
					DISABLE_CELLPHONE(TRUE)
					//DESTROY_MOBILE_PHONE()
				ENDIF
				if DOES_BLIP_EXIST(vehs[mvf_trevor_truck].blip)
					remove_blip(vehs[mvf_trevor_truck].blip)
				endif
				NEW_LOAD_SCENE_STOP()
				
				IF failTimerSet = FALSE
					ifailMissionTimer = GET_GAME_TIMER()
					failTimerSet = TRUE
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGH)
				
//				set_mike()
				mission_substage = STAGE_ENTRY				
				mission_set_stage(msf_2_Fuel_crash_CS)	
//			endif
		break
	endswitch	
	
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		PRINTSTRING("cutscene ready for variations to be set now 1")
		SET_CS_OUTFITS_AND_EXITS()
	ENDIF	
	
	clear_players_task_on_control_input(SCRIPT_TASK_ENTER_VEHICLE)	
	
	//check if player is cheating across the hill
//	if DOES_ENTITY_EXIST(frank())
//	and DOES_ENTITY_EXIST(peds[mpf_trevor].id)
//		vector vPlayer = GET_ENTITY_COORDS(frank())
//		vector vTrev = get_entity_coords(peds[mpf_trevor].id)
//		if (vPlayer.z - vTrev.z) > 15
//			Mission_Failed(mff_lost_trevor)
//		endif
//	endif
	//------------------rubber banding chase----------------------
	if DOES_ENTITY_EXIST(vehs[mvf_trevor_truck].id) And DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)			
		IF IS_playback_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)			
//			Rubber_banding(fplaybackSpeed,vehs[mvf_Frank_car].id,vehs[mvf_trevor_truck].id)	
			PRINTSTRING("distance between cars is ") PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), TREV())) PRINTNL()
			IF NOT IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) < 25000
					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehs[mvf_trevor_truck].id, PLAYER_PED_ID(), fPlaybackSpeed, 1, 25, 35, 100, 1.7, 1, 0.7, FALSE)
				ENDIF
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 25000
				AND GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) < 111000
					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehs[mvf_trevor_truck].id, PLAYER_PED_ID(), fPlaybackSpeed, 1.1, 40, 50, 100, 1.7, 1, 0.7, FALSE)
				ENDIF
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 111000
					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehs[mvf_trevor_truck].id, PLAYER_PED_ID(), fPlaybackSpeed, 1.1, 25, 35, 100, 1.7, 1, 0.7, FALSE)
				ENDIF
			ELSE
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) < 25000
					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehs[mvf_trevor_truck].id, PLAYER_PED_ID(), fPlaybackSpeed, 1.5, 25, 35, 100, 1.7, 1, 0.7, FALSE)
				ENDIF
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 25000
				AND GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) < 111000
					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehs[mvf_trevor_truck].id, PLAYER_PED_ID(), fPlaybackSpeed, 1.5, 40, 50, 100, 1.7, 1, 0.7, FALSE)
				ENDIF
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 111000
					CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehs[mvf_trevor_truck].id, PLAYER_PED_ID(), fPlaybackSpeed, 1.5, 25, 35, 100, 1.7, 1, 0.7, FALSE)
				ENDIF
			ENDIF
			UPDATE_UBER_PLAYBACK(vehs[mvf_trevor_truck].id, fPlaybackSpeed)
			CREATE_ALL_WAITING_UBER_CARS()
//			SET_PLAYBACK_SPEED(vehs[mvf_trevor_truck].id,fplaybackSpeed)
		ENDIF
		SET_ENTITY_INVINCIBLE(vehs[mvf_trevor_truck].id,true)
	endif
	if DOES_ENTITY_EXIST(vehs[mvf_michael_car].id)
		if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_michael_car].id)
			SET_PLAYBACK_SPEED(vehs[mvf_michael_car].id,fplaybackSpeed)
		endif
	endif
	if DOES_ENTITY_EXIST(frank())
	and DOES_ENTITY_EXIST(peds[mpf_trevor].id)
		IF NOT IS_PED_INJURED(frank())
		AND NOT IS_PED_INJURED(peds[mpf_trevor].id)
			IF IS_playback_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
				fDist = VDIST(GET_ENTITY_COORDS(frank()),GET_ENTITY_COORDS(peds[mpf_trevor].id))
				
				//Fail reasons
				if fCurrentPlaybackTime > 21000
				and fdist > 300
					Mission_Failed(mff_lost_trevor)						
				elif fCurrentPlaybackTime < 21000
				and fdist > 350
					Mission_Failed(mff_lost_trevor)
				endif
				
				//Chase Dialogue should stop in time for the phone call
				IF GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) < 140000
					//Have first line from Franklin at start of chase
					if bdialougeplayed = false
					and GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 10000
						ADD_PED_FOR_DIALOGUE(convo_struct,0,frank(),"FRANKLIN")
						ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_trevor].id,"TREVOR")
						IF CREATE_CONVERSATION(convo_struct,"FIN1AUD","FIN1_CHS",CONV_PRIORITY_MEDIUM)
							//Shit. 
							//Man, come on.
							iRandomChatTimer = GET_GAME_TIMER()
							FrankTurnToSpeak = TRUE
							bdialougeplayed = true
						ENDIF
					endif
					
					//Now start random lines between Franklin and Trevor throughout chase. Providing they are close enough to each other
					if bdialougeplayed = true
					and GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_truck].id) > 15000
					and GET_DISTANCE_BETWEEN_ENTITIES(frank(),peds[mpf_trevor].id) < 60
						IF FrankTurnToSpeak = TRUE
							IF GET_GAME_TIMER() > (iRandomChatTimer + 8000)
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
										ADD_PED_FOR_DIALOGUE(convo_struct,0,frank(),"FRANKLIN")
										ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_trevor].id,"TREVOR")
										IF CREATE_CONVERSATION(convo_struct,"FIN1AUD","FIN1_CALL",CONV_PRIORITY_MEDIUM)
											//Trevor!
											//Trevor, dog!
											//Eh, let's do this in a quiet place!
											//Pull over, let's talk!
											//You goin' run from me?
											//Where you going?
											iRandomChatTimer = GET_GAME_TIMER()
											TrevTimeToSpeak = TRUE
											FrankTurnToSpeak = FALSE
										ENDIF
									ENDIF
								ENDIF
							endif
						endif
						IF TrevTimeToSpeak = TRUE
							IF GET_DISTANCE_BETWEEN_ENTITIES(frank(),peds[mpf_trevor].id) < 40
								IF GET_GAME_TIMER() > (iRandomChatTimer + 8000)
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											ADD_PED_FOR_DIALOGUE(convo_struct,0,frank(),"FRANKLIN")
											ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_trevor].id,"TREVOR")
											IF CREATE_CONVERSATION(convo_struct,"FIN1AUD","FIN1_CALLBCK",CONV_PRIORITY_MEDIUM)
												//You snake!
												//I was good to you!
												//I'll come back and gut you!
												//I will hunt you down!
												//I shoulda known!
												//I'll cut your cold heart out your chest!
												//I fell for the lies - again!
												//I'll pull your lying tongue out your throat!
												iRandomChatTimer = GET_GAME_TIMER()
												FrankTurnToSpeak = TRUE
												TrevTimeToSpeak = FALSE
											ENDIF
										ENDIF
									ENDIF
								endif
							ELSE
								iRandomChatTimer = GET_GAME_TIMER()
								FrankTurnToSpeak = TRUE
								TrevTimeToSpeak = FALSE	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		endif
	endif
	
	if DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id) 
		if not IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
			Mission_Failed(mff_destroyed_veh)	
		endif
	endif

	//hint cam 			
	CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(sHintCam, vehs[mvf_trevor_truck].id)
	UPDATE_CHASE_BLIP(vehs[mvf_trevor_truck].blip,vehs[mvf_trevor_truck].id,350)
	
	if IS_GAMEPLAY_HINT_ACTIVE()
		if not IS_AUDIO_SCENE_ACTIVE("FIN_1_FOCUS_CAM")
			START_AUDIO_SCENE("FIN_1_FOCUS_CAM")
		endif
	else
		if IS_AUDIO_SCENE_ACTIVE("FIN_1_FOCUS_CAM")
			stop_Audio_scene("FIN_1_FOCUS_CAM")
		endif
	endif
		
ENDPROC

INT iFuelSpillSound

PROC ST_2_Fuel_crash_CS()
	// url:bugstar:2048375
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_NEXT_CAMERA )
	
	switch mission_substage
		Case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Stage 2: Fuel Crash CutScene")
				
				REMOVE_IPL("DES_tankerexp")
				REMOVE_IPL("tankerexp_grp1")
				REMOVE_IPL("tankerexp_grp2")
				
				IPLsChanged = FALSE
				FranksCarMoved = FALSE
				trevsLightsTurnedOff = FALSE
				MikesLightsTurnedOff = FALSE
				rayfireBeingSkipped = FALSE
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_truck].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_truck].id)
				ENDIF
				STOP_AUDIO_SCENES()
				START_AUDIO_SCENE("FIN_1_TREVOR_CRASH_SCENE")
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehs[mvf_trevor_truck].id)
				set_exit_states(false)
				
				//scene stuff
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_trevor].id,"Trevor",CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"Michael",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
//				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_michael_car].id,"Michaels_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"Showroom_Car",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_trevor_truck].id,"Trevors_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				START_CUTSCENE()		
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++		
			ELSE
				IF failTimerSet = TRUE
					IF GET_GAME_TIMER() > (ifailMissionTimer + 5000) 
						Mission_Failed(mff_lost_trevor)
					ENDIF
				ENDIF
				PRINTSTRING("waiting for cutscene to load fin_a_mcs_1")PRINTNL()
			ENDIF
		break
		case 1
			if IS_CUTSCENE_PLAYING()
			
				REQUEST_PTFX_ASSET()
				REQUEST_ACTION_MODE_ASSET("FRANKLIN_ACTION")
			
				//Put the lights on for the showroom_car and Trevors car
				IF NOT IS_ENTITY_DEAD(vehs[mvf_trevor_truck].id)
					SET_VEHICLE_LIGHTS(vehs[mvf_trevor_truck].id, FORCE_VEHICLE_LIGHTS_ON)
				ENDIF				
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car"))
					IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car"))
						SET_VEHICLE_LIGHTS(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car")), FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
				ENDIF
				//Grab Michael
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
					IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))				
						peds[mpf_michael].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
					ENDIF
				ENDIF
				//Move Franklin to safe spot as soon as scene starts
				IF FranksCarMoved = FALSE
					IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
						SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<1772.7411, -1465.8165, 111.5278>>)
						SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 185.4462)
						FranksCarMoved = TRUE
					ENDIF
				ENDIF
								
					//-----------load next stage-------
						load_asset_stage(msf_3_Ignite_trevor)
					//---------------------------------				
				if IS_SCREEN_FADED_OUT()			
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				TRIGGER_MUSIC_EVENT("FINA_NITRO_CRASH")	
				stop_audio_scenes()

				SET_PED_USING_ACTION_MODE(Frank(), TRUE)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				CLEAR_PED_PROP(peds[mpf_trevor].id, ANCHOR_HEAD)
				CLEAR_PED_PROP(peds[mpf_michael].id, ANCHOR_HEAD)
				REMOVE_PLAYER_HELMET(PLAYER_ID(), TRUE)

				prep_stop_cutscene(true)	
				bPetrol1 				= false
				bMikeDamage 			= false
				btrevorDammage 			= false
				ipetrolStage 			= 0
				FranksCarMoved 			= FALSE
				TrevCarImpactTriggered	= FALSE
				bVideoRecordingDone 	= FALSE
				mission_substage++
			endif
		break
		case 2	
			if IS_CUTSCENE_PLAYING()
				
				//Add in a video recording for the crash scene
				IF bVideoRecordingDone = FALSE
					IF GET_CUTSCENE_TIME() > 17814
						REPLAY_RECORD_BACK_FOR_TIME(12.0, 0, REPLAY_IMPORTANCE_HIGH)
						bVideoRecordingDone = TRUE
					ENDIF
				ENDIF
				
				//Add effect for impact 
				IF TrevCarImpactTriggered = FALSE
					IF GET_CUTSCENE_TIME() > 8308
						IF HAS_PTFX_ASSET_LOADED()
							IF START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_fin_trev_car_impact", <<1732.5, -1628.5, 112.8>>, <<0, 0, 0>>)
								PRINTSTRING("TrevCarImpactTriggered = TRUE")
								TrevCarImpactTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Change IPL's when camera is looking at Franklin
				IF IPLsChanged = FALSE
					IF GET_CUTSCENE_TIME() > 36635
					AND IS_SCREEN_FADED_IN()
						REMOVE_IPL("DES_tankercrash")
						REMOVE_IPL("tankercrash_grp2")
						REMOVE_IPL("tankerexp_grp0")
						REQUEST_IPL("DES_tankerexp")
						PRINTSTRING("IPL's being changed now")
						IPLsChanged = TRUE
					ENDIF
				ENDIF

				//Turn Trev's car lights off just as he crashes.
				IF trevsLightsTurnedOff = FALSE
					IF GET_CUTSCENE_TIME() > 8330
						IF DOES_ENTITY_EXIST(vehs[mvf_trevor_truck].id)
							IF NOT IS_ENTITY_DEAD(vehs[mvf_trevor_truck].id)
								SET_VEHICLE_LIGHTS(vehs[mvf_trevor_truck].id, FORCE_VEHICLE_LIGHTS_OFF)
								iFuelSpillSound = GET_SOUND_ID()
								PLAY_SOUND_FROM_COORD(iFuelSpillSound,  "FINALE_PETROL_SPILL", <<1733, -1627, 113>>)
								
								trevsLightsTurnedOff = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
						
				//Turn Mike's car lights off just as he crashes.
				IF MikesLightsTurnedOff = FALSE
					IF GET_CUTSCENE_TIME() > 8049
						IF DOES_ENTITY_EXIST(MikeShowRoomCar)	
							IF NOT IS_ENTITY_DEAD(MikeShowRoomCar)
								SET_VEHICLE_LIGHTS(MikeShowRoomCar, FORCE_VEHICLE_LIGHTS_OFF)
								MikesLightsTurnedOff = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF FranksCarMoved = FALSE
					IF GET_CUTSCENE_TIME() > 8088
						//frank car
						IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
							IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
								SET_ENTITY_COORDS(vehs[mvf_Frank_car].id, <<1732.6128, -1578.4955, 111.5987>>)
								SET_ENTITY_HEADING(vehs[mvf_Frank_car].id, 134.9147)
								FranksCarMoved = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//Grab Michael's showroom car
				IF NOT DOES_ENTITY_EXIST(MikeShowRoomCar)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car"))
						SET_ENTITY_AS_MISSION_ENTITY(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car"))
						MikeShowRoomCar = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car")) 
					ENDIF
				ENDIF
			
				if not bPetrol1
//					if Add_Petrol_trail()
						bPetrol1 = true
//					endif
				endif
				if not btrevorDammage
					if DOES_ENTITY_EXIST(TREV())
						IF NOT IS_PED_INJURED(TREV())
							if HAS_ANIM_EVENT_FIRED(TREV(),GET_HASH_KEY("Trevor_Damage"))
								PRINTSTRING("anim event Trevor_Damage has triggered and damage should be given to Trevor") PRINTNL()
								SET_PED_WETNESS_HEIGHT(TREV(),1)
								SET_ENABLE_PED_ENVEFF_SCALE(trev(), TRUE)
								SET_PED_ENVEFF_COLOR_MODULATOR(trev(),73,51,99)
								SET_PED_ENVEFF_SCALE(trev(),0.6)
								SET_PED_ENVEFF_CPV_ADD(trev(),0.3)
								APPLY_PED_DAMAGE_PACK(trev(),"BigHitByVehicle",0,1)
								btrevorDammage = true
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				if not bMikeDamage
					if DOES_ENTITY_EXIST(MIKE())
						IF NOT IS_PED_INJURED(MIKE())
							if HAS_ANIM_EVENT_FIRED(MIKE(),GET_HASH_KEY("Michael_Damage"))
//								APPLY_PED_DAMAGE_PACK(MIKE(),"BigHitByVehicle",0,1)
								bMikeDamage = true
							endif
						endif
					endif
				endif
			endif	
			
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")	
//				SET_ENTITY_COORDS(Frank(),<< 1731.5767, -1615.9362,  112.4401 >>)
//				SET_ENTITY_HEADING(Frank(),-154.56)
//				SET_ENTITY_COORDS(Frank(), <<1731.5765, -1615.9360, 111.4372>>)
//				SET_ENTITY_HEADING(Frank(), 205.4400)
				SET_PED_USING_ACTION_MODE(Frank(), TRUE)
				FORCE_PED_MOTION_STATE(Frank(), MS_ACTIONMODE_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
				println("frank")
				bcsFrank = true
//			else
//				Point_Gameplay_cam_at_coord(218.2864)
			endif
			
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")			
				println("trev")
				SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG1,sAnim_trev_LOOP,INSTANT_BLEND_IN,SLOW_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(trev())
				bcsTrev = true
				
				REPLAY_STOP_EVENT()
				
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")	
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_michael].id,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_michael].id,REL_Michael)
				SET_PED_ACCURACY(peds[mpf_michael].id,100)
				SET_PED_COMBAT_ATTRIBUTES(peds[mpf_michael].id,CA_PERFECT_ACCURACY,true)
				STOP_PED_SPEAKING(peds[mpf_michael].id,true)
				SET_PED_CONFIG_FLAG(peds[mpf_michael].id,PCF_DisableExplosionReactions,true)
				SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				TASK_SYNCHRONIZED_SCENE(peds[mpf_michael].id,SyncSceneIGMike,sAnimDict_IG1,sAnim_mike_LOOP,walk_BLEND_IN,SLOW_BLEND_OUT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_michael].id)
				bcsMikeCar = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Showroom_Car")		
//				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehs[mvf_michael_car].id)
				//Turn Mike's car lights off just as he crashes.
				IF MikesLightsTurnedOff = FALSE
					IF DOES_ENTITY_EXIST(MikeShowRoomCar)	
						IF NOT IS_ENTITY_DEAD(MikeShowRoomCar)
							SET_VEHICLE_LIGHTS(MikeShowRoomCar, FORCE_VEHICLE_LIGHTS_OFF)
							SET_ENTITY_PROOFS(MikeShowRoomCar, FALSE, TRUE, TRUE, FALSE, FALSE, TRUE)
							MikesLightsTurnedOff = TRUE
						ENDIF
					ENDIF
				ENDIF
				println("mike car")
				bcsMike = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_car")	
				//Turn Trev's car lights off just as he crashes.
				IF trevsLightsTurnedOff = FALSE
					IF DOES_ENTITY_EXIST(vehs[mvf_trevor_truck].id)
						IF NOT IS_ENTITY_DEAD(vehs[mvf_trevor_truck].id)
							SET_VEHICLE_LIGHTS(vehs[mvf_trevor_truck].id, FORCE_VEHICLE_LIGHTS_OFF)
							trevsLightsTurnedOff = TRUE
						ENDIF
					ENDIF
				ENDIF
//				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehs[mvf_trevor_truck].id)
				println("trev car")
				bcsTrevCar = true
			endif
			
			if bcsFrank
			and bcsTrevCar
			and bcsTrev
			and bcsMike
			and bcsMikeCar
				IF NOT IS_ENTITY_DEAD(vehs[mvf_michael_car].id)
					SET_ENTITY_INVINCIBLE(vehs[mvf_michael_car].id,false)
				ENDIF
				DISPLAY_RADAR(true)																			
				DISPLAY_HUD(true)																			
				SET_PLAYER_CONTROL(player_Id(),true)
				bPlaceholder = false
				IF FranksCarMoved = FALSE
					//frank car
					SET_ENTITY_COORDS(vehs[mvf_Frank_car].id,<< 1728.3752, -1610.3052, 111.4705 >>)
					SET_ENTITY_HEADING(vehs[mvf_Frank_car].id,160.3058)			
					FranksCarMoved = TRUE
				ENDIF				
				
				if isentityalive(trev())
					SET_PED_WETNESS_HEIGHT(TREV(),1)
				endif
				
				IF IS_SCREEN_FADED_OUT()
					rayfireBeingSkipped = TRUE
					
					WHILE rayfireBeingSkipped = TRUE
						SWITCH rayfireStage
							CASE 0
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								SETTIMERB(0)
								CrashRayfireEvent = GET_RAYFIRE_MAP_OBJECT(<<1725.7545, -1636.3733, 117.9358>>, 20, "DES_tankercrash")
								
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(CrashRayfireEvent)
									SET_STATE_OF_RAYFIRE_MAP_OBJECT(CrashRayfireEvent, RFMO_STATE_ENDING)
									rayfireStage ++
								ELSE
									PRINTSTRING("rayfire doesn't exist")
									rayfireStage ++
								ENDIF
								
							BREAK
							CASE 1
								IF DOES_RAYFIRE_MAP_OBJECT_EXIST(CrashRayfireEvent)
									IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(CrashRayfireEvent) = RFMO_STATE_END
//										SET_STATE_OF_RAYFIRE_MAP_OBJECT(CrashRayfireEvent, RFMO_STATE_END)
										PRINTSTRING("Setting rayfire state to end") PRINTNL()
										rayfireStage ++
									ELSE
										IF TIMERB() > 10000
											PRINTSTRING("timer b is over 10000 so moving on for safety")
											rayfireStage ++
										ENDIF
										PRINTSTRING("waiting on rayfire event state changing to RFMO_STATE_END") PRINTNL()
									ENDIF
								ELSE
									rayfireStage ++
									PRINTSTRING("rayfire doesn't exist")
								ENDIF
							BREAK
							CASE 2
								REMOVE_IPL("DES_tankercrash")
								REMOVE_IPL("tankerexp_grp0")
								REMOVE_IPL("tankercrash_grp1")
								REMOVE_IPL("tankercrash_grp2")
								REQUEST_IPL("DES_tankerexp")
								PRINTSTRING("requesting DES_tankerexp now") PRINTNL()
								WHILE NOT IS_IPL_ACTIVE("DES_tankerexp")
									PRINTSTRING("waiting for IPL DES_tankerexp to activate")
									WAIT(0)
								ENDWHILE
								
								rayfireStage ++
							BREAK
							CASE 3
								IF timerb() > 5000
									rayfireBeingSkipped = FALSE
								ENDIF
							BREAK
						ENDSWITCH
						WAIT(0)
						IF NOT IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_OUT(0)
						ENDIF
						//Have this as a safety net to make sure it moves on after 10seconds.
						IF timerb() > 10000
							rayfireBeingSkipped = FALSE
						ENDIF
						PRINTSTRING("Waiting on rayfire event ending") PRINTNL()
					ENDWHILE

				ENDIF
				
				mission_substage = 3
			endif		
			IF NOT IS_CUTSCENE_ACTIVE()
				IF NOT IS_ENTITY_DEAD(vehs[mvf_michael_car].id)
					SET_ENTITY_INVINCIBLE(vehs[mvf_michael_car].id,false)
				ENDIF
				DISPLAY_RADAR(true)																			
				DISPLAY_HUD(true)																			
				SET_PLAYER_CONTROL(player_Id(),true)
				bPlaceholder = false
				IF FranksCarMoved = FALSE
					//frank car
					SET_ENTITY_COORDS(vehs[mvf_Frank_car].id,<< 1728.3752, -1610.3052, 111.4705 >>)
					SET_ENTITY_HEADING(vehs[mvf_Frank_car].id,160.3058)			
					FranksCarMoved = TRUE
				ENDIF				
				
				if isentityalive(trev())
					SET_PED_WETNESS_HEIGHT(TREV(),1)
				endif
				
//				IF IS_SCREEN_FADED_OUT()
//					REMOVE_IPL("DES_tankercrash")
//					REMOVE_IPL("tankercrash_grp1")
//					REMOVE_IPL("tankercrash_grp2")
//					REMOVE_IPL("tankerexp_grp0")
//					REQUEST_IPL("DES_tankerexp")
//					PRINTSTRING("requesting DES_tankerexp now 1") PRINTNL()
//					WHILE NOT IS_IPL_ACTIVE("DES_tankerexp")
//						PRINTSTRING("waiting for IPL DES_tankerexp to activate 1")
//						WAIT(0)
//					ENDWHILE
//				ENDIF
				mission_substage = 3
			ENDIF
		break
		case 3			
			IF NOT IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			iCamTimer = GET_GAME_TIMER()
			mission_substage = stage_entry
			Mission_Set_Stage(msf_3_Ignite_trevor)			
		break
	endswitch
	
	//Make sure Trev is wet
	IF mission_substage > 1
		IF IS_CUTSCENE_PLAYING()
			IF GET_CUTSCENE_TIME() > 15019
				IF DOES_ENTITY_EXIST(TREV())
					SET_PED_WETNESS_ENABLED_THIS_FRAME(TREV())
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(TREV())
				SET_PED_WETNESS_ENABLED_THIS_FRAME(TREV())
			ENDIF	
		ENDIF
	ENDIF
	
//	if bSkipCut
//	and not IS_CUTSCENE_PLAYING()
//		bSkipCut = false
//		bDoSkip = true
//		iSkipToStage = ENUM_TO_INT(msf_3_Ignite_trevor)
//		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)		
//	endif
	
ENDPROC

//PURPOSE: Handles the rayfire event for the tanker blowing up
PROC rayfireController()

	SWITCH iRayfireStage
	
		CASE 0
		
			rfGasTankExplosion = GET_RAYFIRE_MAP_OBJECT(<<1725.7545, -1636.3733, 117.9358>>, 20, "des_tankerexplosion")
			
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfGasTankExplosion)
				PRINTSTRING("RAYFIRE : rfGasTankExplosion exists") PRINTNL()
				iRayfireStage ++
			ELSE
				PRINTSTRING("RAYFIRE : rfGasTankExplosion doesn't exist") PRINTNL()
			ENDIF
			
		BREAK
		
		CASE 1
		
              SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfGasTankExplosion, RFMO_STATE_PRIMING)
			  PRINTSTRING("RAYFIRE : rfGasTankExplosion being set to RFMO_STATE_PRIMING") PRINTNL()
              iRayfireStage++
			
		BREAK
		
		CASE 2
		
            IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfGasTankExplosion) = RFMO_STATE_PRIMED
				 PRINTSTRING("RAYFIRE : rfGasTankExplosion is at RFMO_STATE_PRIMED") PRINTNL()
                  iRayfireStage++  
            ENDIF
		
		BREAK
		
		CASE 3
		
			IF mission_substage > 3 
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfGasTankExplosion, RFMO_STATE_START_ANIM)  
				PRINTSTRING("RAYFIRE : rfGasTankExplosion is being set to RFMO_STATE_START_ANIM") PRINTNL()
				iRayfireStage++
			ENDIF
			
		BREAK
		
		CASE 4

			IF mission_substage = 5
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 0.16)							
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfGasTankExplosion, RFMO_STATE_ENDING)
					iRayfireStage++
				ENDIF
			ENDIF
				
		BREAK
		
		CASE 5
		
			//Handle the rayfire event to end at a specific time in the cutscene
			IF rayfireSetToEndState = FALSE
				IF mission_substage = 5
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 0.184)
						IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfGasTankExplosion)
							IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfGasTankExplosion) = RFMO_STATE_ENDING
								SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfGasTankExplosion, RFMO_STATE_END)
								PRINTSTRING("Setting rayfire state to end") PRINTNL()
								rayfireSetToEndState = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			PRINTSTRING("RAYFIRE : rfGasTankExplosion is running") PRINTNL()
		BREAK
	ENDSWITCH

ENDPROC

PROC ST_3_Ignite_trevor()
	IF DOES_ENTITY_EXIST(TREV())
		SET_PED_WETNESS_ENABLED_THIS_FRAME(TREV())
	ENDIF
	
	// Removed due to url:bugstar:2048375
	//DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 

	SET_PED_MAX_MOVE_BLEND_RATIO(Frank(),PEDMOVE_STILL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CELLPHONE(TRUE)
	
	IF mission_substage > 0
		rayfireController()
	ENDIF
	
	switch mission_substage	
		Case STAGE_ENTRY	
//			REQUEST_IPL("tankerexp_grp1")
			
			iRayfireStage = 0
			rayfireSetToEndState = FALSE
			streamLoaded = FALSE
			streamStarted = FALSE
			PetrolEffectTriggered = FALSE
			bMuz_pistol_effect_played = FALSE
//			doneTrevCHat = FALSE

			REQUEST_PTFX_ASSET()
			
			if IS_SCREEN_FADED_OUT()
			
				IF HAS_SOUND_FINISHED(iFuelSpillSound)
					iFuelSpillSound = GET_SOUND_ID()
					PLAY_SOUND_FROM_COORD(iFuelSpillSound,  "FINALE_PETROL_SPILL", <<1733, -1627, 113>>)
				ENDIF			
			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				//sync scene
				SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG1,sAnim_mike_LOOP,INSTANT_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)	
				SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG1,sAnim_trev_LOOP,INSTANT_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
			endif
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Stage 3: Ignite Trevor")			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_michael].id,TRUE)				
			SET_ENTITY_INVINCIBLE(TREV(),true)
			
			if not DOES_BLIP_EXIST(blip_objective)
				blip_objective = CREATE_BLIP_FOR_PED(TREV(),true)
			endif
			if not IS_AUDIO_SCENE_ACTIVE("FIN_1_DESTROY_TREVOR")
				START_AUDIO_SCENE("FIN_1_DESTROY_TREVOR")
			endif
			if HAS_PED_GOT_WEAPON(Frank(),WEAPONTYPE_PISTOL)
				SET_CURRENT_PED_WEAPON(Frank(),WEAPONTYPE_PISTOL,true)
				IF GET_AMMO_IN_PED_WEAPON(Frank(),WEAPONTYPE_PISTOL) < 50
					ADD_AMMO_TO_PED(Frank(),WEAPONTYPE_PISTOL, 50)
				ENDIF
			else
				GIVE_WEAPON_TO_PED(Frank(),WEAPONTYPE_PISTOL,50,true,true)
			endif
			
			IF DOES_CAM_EXIST(SyncSceneCam)
				DESTROY_CAM(SyncSceneCam)
			ENDIF
			IF DOES_CAM_EXIST(SyncSceneTrevCam)
				DESTROY_CAM(SyncSceneTrevCam)
			ENDIF
			
			//Create the scripted camera for the sync scenes
			IF NOT DOES_CAM_EXIST(SyncSceneCam)
				SyncSceneCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA") //BUG FIX 1324072
			ENDIF
			IF NOT DOES_CAM_EXIST(SyncSceneTrevCam)
				SyncSceneTrevCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA") //BUG FIX 1324072
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
//			REQUEST_CUTSCENE("fin_a_ext") //BUG FIX 1408210 
			print_now("FIN1_DES",DEFAULT_GOD_TEXT_TIME,1)
			iMikeKillTimer = get_Game_timer()
			iIgniteStage = 0
			bMikeKills  = false
			bFrankaim	= false
			cutsceneRequested = FALSE //BUG FIX 1408210 
			mission_substage++
		break
		case 1	
			//Request next cutscene once previous one has finished.
			IF cutsceneRequested = FALSE
				IF NOT IS_CUTSCENE_ACTIVE()
					REQUEST_CUTSCENE("fin_a_ext") //BUG FIX 1408210
					cutsceneRequested = TRUE
				ENDIF
			ELSE
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CS_OUTFITS_AND_EXITS()
				ENDIF
			ENDIF
			
			if get_game_timer() - iMikeKillTimer > 4000 //8000
				switch iIgniteStage
					case 0 // mike
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)
							ADD_PED_FOR_DIALOGUE(convo_struct,2,mike(),"MICHAEL")
							if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"FIN1AUD","FIN1_COUGH","FIN1_COUGH_1",CONV_PRIORITY_MEDIUM)
								if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)			
									SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
									TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG1,sAnim_mike_c,instant_BLEND_IN,normal_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
									iMikeKillTimer = get_Game_timer()
									iIgniteStage++
								endif	
							endif
						endif
					break
					case 1 //trev
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGtrev)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGTREV) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)
							ADD_PED_FOR_DIALOGUE(convo_struct,1,trev(),"TREVOR")
							if CREATE_CONVERSATION(convo_struct,"FIN1AUD","FIN1_COUGH2",CONV_PRIORITY_MEDIUM)
								if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)			
									SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
									TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG1,sAnim_trev_A,instant_BLEND_IN,normal_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
									iMikeKillTimer = get_Game_timer()
									iIgniteStage = 5
								endif 
							endif
						endif
					break
					case 2// mike
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)
							ADD_PED_FOR_DIALOGUE(convo_struct,2,mike(),"MICHAEL")
							if CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"FIN1AUD","FIN1_COUGH","FIN1_COUGH_2","FIN1_COUGH","FIN1_COUGH_3",CONV_PRIORITY_MEDIUM)
								if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)			
									SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
									TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG1,sAnim_mike_a,instant_BLEND_IN,normal_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
									iMikeKillTimer = get_Game_timer()
									iIgniteStage++
								endif 
							endif
						endif					
					break
					case 3//trev
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGtrev)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGTREV) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)
							ADD_PED_FOR_DIALOGUE(convo_struct,1,trev(),"TREVOR")
							if CREATE_CONVERSATION(convo_struct,"FIN1AUD","FIN1_COUGH2",CONV_PRIORITY_MEDIUM)
								if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)			
									SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
									TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG1,sAnim_trev_b,instant_BLEND_IN,normal_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
									iMikeKillTimer = get_Game_timer()
									iIgniteStage++
								endif 
							endif
						endif
					break
					case 4// mike
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)
							ADD_PED_FOR_DIALOGUE(convo_struct,2,mike(),"MICHAEL")
							if CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"FIN1AUD","FIN1_COUGH","FIN1_COUGH_4","FIN1_COUGH","FIN1_COUGH_5",CONV_PRIORITY_MEDIUM)
								if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)			
									SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
									TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG1,sAnim_mike_b,instant_BLEND_IN,normal_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
									iMikeKillTimer = get_Game_timer()
									iIgniteStage++
								endif 
							endif	
						endif
					break
					case 5
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGtrev)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGTREV) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)
							ADD_PED_FOR_DIALOGUE(convo_struct,2,MIKE(),"MICHAEL")
							if CREATE_CONVERSATION(convo_struct,"FIN1AUD","FIN1_MIKE",CONV_PRIORITY_MEDIUM)
								if IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									DISPLAY_RADAR(FALSE)
									DISPLAY_HUD(FALSE)
									SET_CAM_ACTIVE(SyncSceneCam, TRUE)
									SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
									TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2,sAnim_mike_DRAWS,instant_BLEND_IN,normal_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
									PLAY_SYNCHRONIZED_CAM_ANIM(SyncSceneCam, SyncSceneIGMike, "cam_base_idle_to_aim_into_pz", sAnimDict_IG2)//BUG FIX 1324072
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									bMikeKills = true
									bweaponinHand = false
									iIgniteStage++
								endif 
							endif
						endif
					break
					case 6
						//check to give mike gun in phase
						if not bweaponinHand
							if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 0.5)
								GIVE_WEAPON_TO_PED(mike(),WEAPONTYPE_PISTOL,INFINITE_AMMO,true,true)
								SET_CURRENT_PED_WEAPON(MIKE(),WEAPONTYPE_PISTOL,true)
								bweaponinHand = true
							endif
						endif
						
						//check to give mike gun in phase
						if not bMuz_pistol_effect_played
							if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 0.95)
								START_PARTICLE_FX_NON_LOOPED_AT_COORD("muz_pistol", <<1729.85, -1617.4, 112.8>>, <<90, -40, 0>>)
								bMuz_pistol_effect_played = true
							endif
						endif
						
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)								
							SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
							SET_PED_SHOOTS_AT_COORD(mike(),<<1733.70520, -1624.56030, 111.42592>>,true)
							SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
							TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG2,sAnim_trev_dies,normal_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
							PLAY_SYNCHRONIZED_CAM_ANIM(SyncSceneTrevCam, SyncSceneIGTREV, "cam_trevor_death_reaction_pt", sAnimDict_IG2)//BUG FIX 1324072
							SET_FACIAL_IDLE_ANIM_OVERRIDE(TREV(), "burning_1")
							SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)		
							TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2,sAnim_mike_shoots,instant_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
							PLAY_SYNCHRONIZED_CAM_ANIM(SyncSceneCam, SyncSceneIGMike, "cam_gun_shot_&_trevor_death_reaction_pz", sAnimDict_IG2)//BUG FIX 1324072
							SyncSceneIGFRANK = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							if IS_PLAYER_FREE_AIMING(player_id())
							or IS_PLAYER_TARGETTING_ANYTHING(player_id())
								bFrankAim = true
								TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2_Alt,sAnim_frank_shoots,Walk_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
							else
								TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2,sAnim_frank_reacts,instant_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
							endif
							
							//Do video record moment here
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
							mission_substage = 2
						endif 
						if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGTREV) >= 1)
						or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)			
							SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
							TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG1,sAnim_trev_LOOP,Slow_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
						endif 
					break
				endswitch
			else
				if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 1)
				or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)			
					SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
					TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG1,sAnim_mike_LOOP,walk_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
				endif 
				if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGTREV) >= 1)
				or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)			
					SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
					TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG1,sAnim_trev_LOOP,walk_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
				endif 
			endif
			
			if not bMikeKills
				if isentityalive(TREV())	
					if IS_BULLET_IN_ANGLED_AREA(<<1733.29065, -1628.85950, 112.83256>>,<<1732.59058, -1621.27917, 111.42625>>,9.5)
					or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(trev(),frank())
					or IS_ENTITY_ON_FIRE(TREV())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DISPLAY_RADAR(FALSE)
						DISPLAY_HUD(FALSE)
						SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
						SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
						TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG2,sAnim_trev_dies,normal_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
						PLAY_SYNCHRONIZED_CAM_ANIM(SyncSceneTrevCam, SyncSceneIGTREV, "cam_trevor_death_reaction_pt", sAnimDict_IG2)//BUG FIX 1324072
						SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
						SET_FACIAL_IDLE_ANIM_OVERRIDE(TREV(), "burning_1")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
						TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2_Alt,sAnim_mike_reacts,instant_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
						SyncSceneIGFRANK = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)							
						TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2_Alt,sAnim_frank_shoots,walk_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
						bFrankAim = true
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FINA_KILLTREV)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						mission_substage = 2
					endif
				else
					SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
					SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
					TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG2,sAnim_trev_dies,normal_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
					PLAY_SYNCHRONIZED_CAM_ANIM(SyncSceneTrevCam, SyncSceneIGTREV, "cam_trevor_death_reaction_pt", sAnimDict_IG2)//BUG FIX 1324072
					SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(TREV(), "burning_1")
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
					TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMIKE,sAnimDict_IG2_Alt,sAnim_mike_reacts,instant_BLEND_IN,Walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
					SyncSceneIGFRANK = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)							
					TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2_Alt,sAnim_frank_shoots,walk_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
					bFrankAim = true
						
					mission_substage = 2
				endif				
//				if GET_NUMBER_OF_FIRES_IN_RANGE(<<1732.64075, -1624.95190, 111.42876>>,10) > 0
				IF GET_NUMBER_OF_FIRES_IN_RANGE(<<1733.24158, -1625.56287, 111.43230>>, 4.5) > 0 //B*1955557 - Reduced the size of the check to the size of the gas spill. ( Joe Elvin )
					SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
					SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
					TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG2,sAnim_trev_dies,normal_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
					PLAY_SYNCHRONIZED_CAM_ANIM(SyncSceneTrevCam, SyncSceneIGTREV, "cam_trevor_death_reaction_pt", sAnimDict_IG2)//BUG FIX 1324072
					SET_CAM_ACTIVE(SyncSceneTrevCam, TRUE)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(TREV(), "burning_1")
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
					TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMIKE,sAnimDict_IG2_Alt,sAnim_mike_reacts,instant_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
					SyncSceneIGFRANK = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)							
					TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2_Alt,sAnim_frank_shoots,instant_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
					bFrankAim = true
					mission_substage = 2
				endif
				
				//Stops michaels car catching fire.
				IF isentityalive(Vehs[mvf_michael_car].id)
					IF GET_NUMBER_OF_FIRES_IN_RANGE(<<1726.74524, -1622.95288, 111.46024>>, 1) > 0
						SET_ENTITY_INVINCIBLE(Vehs[mvf_michael_car].id, TRUE)
					ENDIF
					IF IS_ENTITY_ON_FIRE(Vehs[mvf_michael_car].id)
						STOP_ENTITY_FIRE(Vehs[mvf_michael_car].id)
					ENDIf
				ENDIF
				IF isentityalive(MikeShowRoomCar)
					IF GET_NUMBER_OF_FIRES_IN_RANGE(<<1726.74524, -1622.95288, 111.46024>>, 1) > 0
						SET_ENTITY_INVINCIBLE(MikeShowRoomCar, TRUE)
					ENDIF
					IF IS_ENTITY_ON_FIRE(MikeShowRoomCar)
						STOP_ENTITY_FIRE(MikeShowRoomCar)
					ENDIf
				ENDIF
				
			endif
			
		break
		case 2		
			//cam
//			cameraIndex2 = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<1730.1110, -1614.5150, 112.9536>>, <<-7.6622, -0.3776, -154.0352>>,45.0)
//			SET_CAM_ACTIVE(cameraIndex2,true)
//			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE",0.5)
//			SHAKE_CAM(cameraIndex2,"SMALL_EXPLOSION_SHAKE",0.5)			
//			prep_start_cutscene(false,<<1736.295,-1619.891,111.290>>,true,true,true,false,1500)	
			SET_MULTIHEAD_SAFE(TRUE)	//B* 2262532: Have blinders on for the sync scene
			//Fix for B*1955557 means that car can explode during cutscene, stopping fire on the back wheel when camera cuts.
			IF GET_NUMBER_OF_FIRES_IN_RANGE(<<1726.74524, -1622.95288, 111.46024>>, 1) > 0
				STOP_FIRE_IN_RANGE(<<1726.74524, -1622.95288, 111.46024>>, 1)
			ENDIF
			IF isentityalive(Vehs[mvf_michael_car].id)
				SET_ENTITY_INVINCIBLE(Vehs[mvf_michael_car].id, FALSE)
			ENDIF
			IF isentityalive(MikeShowRoomCar)
				SET_ENTITY_INVINCIBLE(MikeShowRoomCar, FALSE)
			ENDIF
			
			//Add effect for petrol
			IF PetrolEffectTriggered = FALSE
				IF START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_fin_fire_petrol_trev", <<1734, -1624.4, 111.5>>, <<0,0,0>>)
					PRINTSTRING("PetrolEffectTriggered = TRUE")
					PetrolEffectTriggered = TRUE
				ENDIF
			ENDIF			
			
			//Start Stream
			IF streamStarted = FALSE
				IF streamLoaded = TRUE
				
					STOP_SOUND(iFuelSpillSound)
				
					PRINTSTRING("Starting stream FINALE_A_KILL_TREVOR_SCENE_MASTER CASE 2") 
					PLAY_STREAM_FRONTEND()
					
					streamStarted = TRUE
				ENDIF
			ENDIF
			START_SCRIPT_FIRE(<<1734.28882, -1623.59680, 111.42353>>,5,true)
			START_SCRIPT_FIRE(<<1733.27002, -1624.46289, 111.42641>>,5,true)				
			START_SCRIPT_FIRE(<<1735.64856, -1625.71191, 111.42559>>,5,true)				
			START_SCRIPT_FIRE(<<1732.42139, -1625.29260, 111.43035>>,5,true)				
			START_SCRIPT_FIRE(<<1733.44263, -1625.88269, 111.42981>>,5,true)
			START_SCRIPT_FIRE(<<1732.59961, -1626.65930, 111.43410>>,5,true)				
			START_SCRIPT_FIRE(<<1733.02991, -1627.77722, 111.43572>>,5,true)
			START_SCRIPT_FIRE(<<1731.66602, -1626.07971, 111.43691>>,5,true)
			START_SCRIPT_FIRE(<<1734.27808, -1623.79248, 111.42362>>,5,true)			
			if isentityalive(TREV())
				SET_ENTITY_INVINCIBLE(TREV(), false)
				SET_ENTITY_PROOFS(TREV(), TRUE, FALSE, FALSE, TRUE, TRUE)
				START_ENTITY_FIRE(trev())
//				PLAY_PAIN(trev(),AUD_DAMAGE_REASON_ON_FIRE)
				ADD_PED_FOR_DIALOGUE(convo_struct, 1, TREV(), "Trevor")
				PRINTSTRING("ADD_PED_FOR_DIALOGUE for TREV called.") 
			endif
			if isentityalive(vehs[mvf_trevor_truck].id)
				SET_ENTITY_INVINCIBLE(Vehs[mvf_trevor_truck].id,true)
			endif
//			SET_TIME_SCALE(0.35)
			CLEAR_PRINTS()
			if DOES_BLIP_EXIST(blip_objective)
				REMOVE_BLIP(blip_objective)
			endif
			
			//-----------load next stage-------
				load_asset_stage(msf_4_Goodbyes_CS)
			//---------------------------------
			STOP_AUDIO_SCENES()
			if not IS_AUDIO_SCENE_ACTIVE("FIN_1_TREVOR_DIES_SCENE")
				START_AUDIO_SCENE("FIN_1_TREVOR_DIES_SCENE")
			endif
			iCamTimer  = GET_GAME_TIMER()
			bupdatecam = true
			iSkipTimer =  get_Game_timer()
			mission_substage++
		break
		case 3
			if isentityalive(TREV())
				SET_ENTITY_HEALTH(TREV(),300)
				PRINTSTRING("Setting Trev's health to 300 every frame to allow him to play dialogue") PRINTNL()
			ELSE
				PRINTSTRING("Trev's dead ") PRINTNL()
			ENDIF
//			//Play pain from Trevor
//			IF doneTrevCHat = FALSE
//				IF CREATE_CONVERSATION(convo_struct, "FIN1AUD", "FIN1_BURN", CONV_PRIORITY_HIGH)
//					//Ah! Ah! Ah! Ah!
//					//Mowarghhh! Arghh!
//					doneTrevCHat = TRUE
//					PRINTSTRING("doneTrevCHat = TRUE") 
//				ENDIF
//			ENDIF
		
			//Start Stream
			IF streamStarted = FALSE
				IF streamLoaded = TRUE
					PRINTSTRING("Starting stream FINALE_A_KILL_TREVOR_SCENE_MASTER CASE 3") 
					PLAY_STREAM_FRONTEND()
					streamStarted = TRUE
				ENDIF
			ENDIF		
		
			if get_game_timer() - iCamTimer > 2000
				if isentityalive(TREV())
				and not IS_ENTITY_ON_FIRE(trev())
					START_ENTITY_FIRE(trev())
				endif
			endif	
			
			if bupdatecam
			and (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGFRANK)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGFRANK) >= 0.266)
//				cameraIndex = create_cam("DEFAULT_ANIMATED_CAMERA",FALSE) 
//				SET_CAM_ACTIVE(cameraIndex,true)
//				PLAY_CAM_ANIM(cameraIndex,"cam_alternate_gun_shot_&_trevor_death_reaction",sAnimDict_IG2_Alt,<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)							
				SET_CAM_ACTIVE(SyncSceneTrevCam,true)//BUG FIX 1324072 activate trev's sync'd scene cam once he lights up
				SET_CAM_ACTIVE(SyncSceneCam, FALSE)
				bupdatecam = false	
			endif
			
			if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 1)
			or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)			
				SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				if bMikeKills
					TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2,sAnim_mike_FIRE_LOOP,instant_BLEND_IN,slow_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
				else
					TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2_Alt,sAnim_mike_FIRE_LOOP_ALT,instant_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
				endif
			endif 
			
			if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGFRANK)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGFRANK) >= 1)
			or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGFRANK)			
				SyncSceneIGFRANK = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				if bFrankAim
					TASK_SYNCHRONIZED_SCENE(frank(),SyncSceneIGFRANK,sAnimDict_IG2_Alt,sAnim_frank_FIRE_LOOP_ALT,SLOW_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
				else
					TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2,sAnim_frank_FIRE_LOOP,SLOW_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
				endif
			endif 
			
			if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGTREV) >= 1)
			or not IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGTREV)			
				SyncSceneIGTREV = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
				TASK_SYNCHRONIZED_SCENE(TREV(),SyncSceneIGTREV,sAnimDict_IG2,sAnim_trev_dead,Slow_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_BLOCK_MOVER_UPDATE)
				SET_SYNCHRONIZED_SCENE_LOOPED(SyncSceneIGTREV,true)
				CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(TREV())
				SET_ENTITY_INVINCIBLE(TREV(),false)
				SET_ENTITY_INVINCIBLE(Frank(),true)
				SET_ENTITY_INVINCIBLE(Mike(),true)
				mission_substage++
			endif
			
		break
		case 4					
			//Start dialogue now
			ADD_PED_FOR_DIALOGUE(convo_struct, 2, Mike(), "Michael")
			ADD_PED_FOR_DIALOGUE(convo_struct, 0, PLAYER_PED_ID(), "Franklin")
			CREATE_CONVERSATION(convo_struct, "FIN1AUD", "FIN1_LOVEGAS", CONV_PRIORITY_MEDIUM)
			
			SyncSceneIGMike = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
//			if bMikeKills
//				TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2,sAnim_mike_EXP,SLOW_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
//			else
				TASK_SYNCHRONIZED_SCENE(MIKE(),SyncSceneIGMike,sAnimDict_IG2_Alt,sAnim_mike_EXP_ALT,SLOW_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
//			endif
			
			SyncSceneIGFRANK = CREATE_SYNCHRONIZED_SCENE(<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)	
//			if bFrankAim
				TASK_SYNCHRONIZED_SCENE(frank(),SyncSceneIGFRANK,sAnimDict_IG2_Alt,sAnim_frank_EXP_ALT,SLOW_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
//			else
//				TASK_SYNCHRONIZED_SCENE(Frank(),SyncSceneIGFRANK,sAnimDict_IG2,sAnim_frank_EXP,SLOW_BLEND_IN,SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_FIRE)
//			endif
			if isentityalive(vehs[mvf_michael_car].id)
				SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehs[mvf_michael_car].id,false)
				SET_VEHICLE_ENGINE_HEALTH(vehs[mvf_michael_car].id,4000)
				SET_ENTITY_INVINCIBLE(Vehs[mvf_michael_car].id,true)
			endif
			if isentityalive(vehs[mvf_trevor_truck].id)
				SET_ENTITY_INVINCIBLE(Vehs[mvf_trevor_truck].id,FALSE)
			endif
			ADD_EXPLOSION(<<1732.57910, -1628.07703, 111.43906>>,EXP_TAG_CAR)
			
				cameraIndex = create_cam("DEFAULT_ANIMATED_CAMERA",FALSE) 
				SET_CAM_ACTIVE(cameraIndex,true)
			PLAY_CAM_ANIM(cameraIndex,"cam_alternate_gas_tanker_explosion_outro",sAnimDict_IG2_Alt,<<1736.295,-1619.891,111.290>>, << 0, 0, 0 >>)							
					
									
			iCamTimer = GET_GAME_TIMER()
			SkyEffectStarted = FALSE
			mission_substage++	
		break	
		case 5		
			//Start the effect scr_fin_env_trev_sky 
			IF SkyEffectStarted = FALSE
				IF GET_GAME_TIMER() > (iCamTimer + 1000)
					START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_fin_env_trev_sky", PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_PELVIS) //INT_TO_ENUM(PED_BONETAG, GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_ROOT)))
					SkyEffectStarted = TRUE
				ENDIF
			ENDIF
			if (IS_SYNCHRONIZED_SCENE_RUNNING(SyncSceneIGMike)and GET_SYNCHRONIZED_SCENE_PHASE(SyncSceneIGMike) >= 0.80)
				TRIGGER_MUSIC_EVENT("FINA_END")					
				if isentityalive(trev())	
					SET_ENTITY_HEALTH(TREV(),0)
				endif
				mission_substage = STAGE_ENTRY
				mission_set_stage(msf_4_Goodbyes_CS)	
			endif
		break
	endswitch
	
	PRINTSTRING("icam_timer = ") PRINTINT((GET_GAME_TIMER() - iCamTimer)) PRINTNL()
	
	//Call every frame this command only returns true once after a cutscene has been requested.
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		PRINTSTRING("cutscene ready for variations to be set now 2")
		SET_CS_OUTFITS_AND_EXITS()
	ENDIF	
	
	//Load the stream to be played when the cutscene is trigger for Trevor burning to death.
	IF streamLoaded = FALSE
		IF LOAD_STREAM("FINALE_A_KILL_TREVOR_SCENE_MASTER") 
			streamLoaded = TRUE
		ENDIF
	ENDIF
	
	if mission_substage <= 2			
		if (GET_GAME_TIMER() - iHeartBeat) > 500
			SET_CONTROL_SHAKE(PLAYER_CONTROL,250,80)				
			iHeartBeat = GET_GAME_TIMER()			
		endif
	else
		if get_game_timer() - iSkipTimer > 1500
		and IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
			STOP_STREAM()
			iskiptostage = enum_to_int(msf_4_Goodbyes_CS)
			bDoSkip = true
		endif
	endif
	
ENDPROC
PROC ST_4_Goodbyes_CS()
	switch mission_substage
		
		case STAGE_ENTRY
			//Flags
			endcamStarted = FALSE
			creditsStarted = FALSE
		
			if isentityalive(frank())
				SET_ENTITY_INVINCIBLE(Frank(),false)	
				SET_FORCE_FOOTSTEP_UPDATE(Frank(), true)
			endif
			if isentityalive(mike())
				SET_ENTITY_INVINCIBLE(mike(),false)	
				SET_FORCE_FOOTSTEP_UPDATE(mike(), true)
			endif
			
			CLEAR_HELP()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Stage 4: Goodbyes",true)
			set_exit_states(false)
			mission_substage++			
		break
		case 1
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()	
				if DOES_ENTITY_EXIST(frank())
					REGISTER_ENTITY_FOR_CUTSCENE(frank(),"Franklin",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,GET_ENTITY_MODEL(frank()), CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
				endif
				if DOES_ENTITY_EXIST(peds[mpf_michael].id)
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_michael].id,"Michael",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY,GET_ENTITY_MODEL(peds[mpf_michael].id), CEO_PRESERVE_BODY_BLOOD_DAMAGE|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
				endif
				
				//Just as a safety making sure this gets deleted now as i'm creating it in next stage.
				IF DOES_ENTITY_EXIST(vehs[mvf_michael_car].id)
					DELETE_VEHICLE(vehs[mvf_michael_car].id)
				ENDIF
				
				REQUEST_ADDITIONAL_TEXT("CREDIT", MISSION_TEXT_SLOT)			
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40, TRUE)
				mission_substage++
			endif
		break
		case 2
		 	if IS_CUTSCENE_PLAYING()
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				CLEAR_PED_PROP(peds[mpf_michael].id, ANCHOR_HEAD)
				SET_TIME_SCALE(1)
				prep_stop_cutscene(true)
				HANG_UP_AND_PUT_AWAY_PHONE()
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				endif
				IF NOT DOES_CAM_EXIST(EndCreditCam)
					EndCreditCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					SET_CAM_PARAMS(EndCreditCam, <<1685.640137,-1750.719482,119.864342>>,<<-1.003233,0.243573,78.915367>>,38.864964)
				ENDIF	
				mission_substage++
			endif
		break
		case 3
		
			//Create Michaels car here for end scene so it looks like Michael has left it there BUG FIX 1310899
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_michael_car].id)
				IF CREATE_PLAYER_VEHICLE(vehs[mvf_michael_car].id,CHAR_MICHAEL,<<1698.0569, -1782.4751, 110.5612>>, 321.7204,true,VEHICLE_TYPE_CAR)
					SET_VEHICLE_LIGHTS(vehs[mvf_michael_car].id, FORCE_VEHICLE_LIGHTS_ON)
				ENDIF
			ENDIF
			
			//BUG FIX 1310899 fade the screen out on exit state 
			IF IS_CUTSCENE_PLAYING()
//				IF GET_CUTSCENE_TIME() > 120300
//					IF NOT IS_SCREEN_FADED_OUT()
//						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG) 
//					ENDIF
//				ENDIF
				IF endcamStarted = FALSE
					IF DOES_CAM_EXIST(EndCreditCam)
						SET_CAM_ACTIVE(EndCreditCam, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						DISPLAY_RADAR(FALSE)
						DISPLAY_HUD(FALSE)
						endcamStarted = TRUE
					ENDIF
				ENDIF

				IF GET_CUTSCENE_TIME() > 99000
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
				
				//Start end credits music here
				IF g_bFinaleCreditsPlaylistStarted = FALSE
					IF GET_CUTSCENE_TIME() > 99963
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
						PLAY_END_CREDITS_MUSIC(TRUE)
						SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)

						SET_MOBILE_PHONE_RADIO_STATE(TRUE)

						SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")

						SET_CUSTOM_RADIO_TRACK_LIST("RADIO_01_CLASS_ROCK", "END_CREDITS_KILL_TREVOR", TRUE)
						g_bFinaleCreditsPlaylistStarted = TRUE
					ENDIF
				ENDIF	
				
				IF creditsStarted = FALSE
					IF GET_CUTSCENE_TIME() > 113300
			  			SET_CREDITS_ACTIVE(TRUE)
			  			SET_CREDITS_FADE_OUT_WITH_SCREEN(FALSE)         
			  			START_AUDIO_SCENE("END_CREDITS_SCENE")
						SET_GAME_PAUSES_FOR_STREAMING(FALSE)
												
						AWARD_ACHIEVEMENT_FOR_MISSION(ACH04) // To live or die in los santos
						
						creditsStarted = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			//Delete Franklins car
			IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)		
					DELETE_VEHICLE(vehs[mvf_Frank_car].id)
				ENDIF
			ENDIF
		
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")	
				//BUG FIX 1310899 fade the screen out on exit state 
//				IF NOT IS_SCREEN_FADED_OUT()
//					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT) 
//				ENDIF
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1668.9279, -1743.7573, 111.2063>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 51.8277)
//				SET_FORCE_FOOTSTEP_UPDATE(Frank(), false)
//				SET_FORCE_FOOTSTEP_UPDATE(Mike(), false)
				bcsFrank = true
			endif			
			if bcsFrank
//				if IS_SCREEN_FADED_OUT()
//					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//				endif		
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
//				IF DOES_ENTITY_EXIST(vehs[mvf_Frank_car].id)
//					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Frank_car].id)
//						SET_ENTITY_COORDS(vehs[mvf_Frank_car].id,<< 1663.7246, -1732.3535, 111.2844 >>)
//						SET_ENTITY_HEADING(vehs[mvf_Frank_car].id,18.3814)
//						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Frank_car].id)	
//					ENDIF
//				ENDIF
			endif	
//			IF IS_SCREEN_FADED_OUT()
			IF NOT IS_CUTSCENE_ACTIVE()
				REPLAY_STOP_EVENT()
				mission_substage++
//				Mission_Passed()
			ENDIF			
		break
		case 4
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			PRINTSTRING("Setting player control to false ") PRINTNL()
			SHAKE_CAM(EndCreditCam, "HAND_SHAKE", 0.1)
			SETTIMERA(0)
			mission_substage++		
			
		break
		case 5
			
			IF TIMERA() > 45000
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(5000)
				ENDIF
				IF IS_SCREEN_FADED_OUT()
												
					Mission_Passed()
				ENDIF
			ENDIF
		
		break
	endswitch		
ENDPROC
Proc ST_5_PASSED()
	switch mission_substage
		case STAGE_ENTRY
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//			endif
			mission_substage++
		break
		case 1
			Mission_Passed()
		break
	endswitch
endproc
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
	
		case	msf_0_meet				ST_0_meet()				break
		case 	msf_1_CHASE_TREVOR		ST_1_CHASE_TREVOR() 	break
		case 	msf_2_Fuel_crash_CS		ST_2_Fuel_crash_CS() 	break
		case	msf_3_Ignite_trevor		ST_3_Ignite_trevor()	break
		case	msf_4_Goodbyes_CS		ST_4_Goodbyes_CS()		break
		case 	msf_5_passed			ST_5_PASSED()			break
		
	endswitch
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()		
		ENDIF
	ENDPROC
#endif

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Finale 1 Mission Launched")
	PRINTNL()
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Finale 1 Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		if IS_CUTSCENE_ACTIVE()
			SET_CUTSCENE_FADE_VALUES()	
		endif	
		Mission_Cleanup()
		TERMINATE_THIS_THREAD()
	ENDIF	
	
	SET_MISSION_FLAG(TRUE)
	
	REQUEST_MISSION_AUDIO_BANK("FINALE_A_GENERAL")
	
	REQUEST_IPL("DES_tankercrash")
	
	//load text
	REQUEST_ADDITIONAL_TEXT("FINALE1",MISSION_TEXT_SLOT)
	if not HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		wait(0)
	endif
	
#if IS_DEBUG_BUILD
//z menu for skipping stages
	zMenuNames[msf_0_meet].sTxtLabel 			=	"Stage 0: Phone Call"
	zMenuNames[msf_1_CHASE_TREVOR].sTxtLabel 	= 	"Stage 1: Chase Trevor"
	zMenuNames[msf_2_Fuel_crash_CS].sTxtLabel 	= 	"Stage 2: Fuel Crash CutScene"
	zMenuNames[msf_3_Ignite_trevor].sTxtLabel 	= 	"Stage 3: Ignite Trevor"
	zMenuNames[msf_4_Goodbyes_CS].sTxtLabel 	= 	"Stage 4: Goodbyes"
	zMenuNames[msf_5_Passed].sTxtLabel 			= 	"----------- PASSED -----------"
	
	zMenuNames[CST_INT].sTxtLabel    	= "Stage 0: Init"
	zMenuNames[CST_INT].bSelectable  	= false
	zMenuNames[CST_MCS_1].sTxtLabel    	= "Stage 2: MCS_1_alt1"
	zMenuNames[CST_MCS_1].bSelectable  	= false
	zMenuNames[CST_EXT].sTxtLabel   	= "Exit Cutscene"
	zMenuNames[CST_EXT].bSelectable 	= false
	
	widget_debug = START_WIDGET_GROUP("Finale 1 Menu")
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	
	SET_UBER_PARENT_WIDGET_GROUP(widget_debug) 
#endif

	if not IS_REPLAY_IN_PROGRESS()
		timelapse()
		WAIT(500)
	endif

	//initialize mission
	MISSION_SETUP()
	
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_KillTrevor") 
	
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)	
		
		Mission_stage_management()
		MISSION_STAGE_SKIP()
		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif 	
				
#IF IS_DEBUG_BUILD
	DO_DEBUG()
#ENDIF	
	
		WAIT(0)
	
	ENDWHILE
ENDSCRIPT
