// Includes
USING "friendutil_private.sch"
USING "flow_public_core.sch"
USING "selector_public.sch"
USING "pb_prostitute_stats.sch"

// Constants
CONST_INT TOTAL_MONEY_SPENT 		1000000		// Money spent or saved
CONST_INT STRIP_CLUB_DANCES 		3			// Strip club - number of lap dances
CONST_INT STRIP_CLUB_MONEY_SPENT 	100			// Strip club - cash spent
CONST_INT STOCKMARKET_TIME_SPENT    120000      // Stockmarket - time spent
CONST_INT TOTAL_PEDS_KILLED    		100     	// Number of innocent peds killed
CONST_INT TOTAL_VEHICLES_STOLEN   	100     	// Number of vehicles stolen
CONST_INT FITNESS_LEVEL_FRANKLIN    50     		// Fitness (Stamina, Strength, Lung Capacity)
CONST_INT FITNESS_LEVEL_MICHAEL    	50
CONST_INT FITNESS_LEVEL_TREVOR   	50
CONST_INT TOTAL_STRANGERS_MET   	10			// Number of RC's and RE's completed

// Structs
STRUCT STRUCT_PSYCH_DATA
	INT 			iIntro 
	TEXT_LABEL_3    tl3storychoice 
	TEXT_LABEL_3    tl3mostplayedchar 
	TEXT_LABEL_7    tl3moneyspent 
	TEXT_LABEL_3    tl3stripclubs 
	TEXT_LABEL_3    tl3prostitute 
	TEXT_LABEL_3    tl3family 
	TEXT_LABEL_3    tl3stockmarket 
	TEXT_LABEL_3    tl3killpeds 
	TEXT_LABEL_3    tl3stolenvehicles 
	TEXT_LABEL_3    tl3yoga 
	TEXT_LABEL_3    tl3fitness 
	TEXT_LABEL_3    tl3strangers 
	TEXT_LABEL_3    tl3collectables 
	INT   			iSummary
ENDSTRUCT

/// PURPOSE:
///    Returns randomised introduction line
FUNC TEXT_LABEL_15 GET_SHRINK_INTRO(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "INTRO_"
	INT           randInt = GET_RANDOM_INT_IN_RANGE(1,21)

	sPsychData.iIntro = randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line dependant on whether the player killed Michael, Trevor or saved both  
FUNC TEXT_LABEL_15 GET_SHRINK_STORY_CHOICE(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "STORY_"
	INT           randInt

	// Gameflow choice
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_KILLED)
		sPsychData.tl3storychoice = "M"
		returnStr += "M"
		randInt = GET_RANDOM_INT_IN_RANGE(1,11)
	ELIF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREVOR_KILLED)
		sPsychData.tl3storychoice = "T"
		returnStr += "T"
		randInt = GET_RANDOM_INT_IN_RANGE(1,10)
	ELSE
		sPsychData.tl3storychoice = "B"
		returnStr += "B"
		randInt = GET_RANDOM_INT_IN_RANGE(1,11)
	ENDIF
		
	sPsychData.tl3storychoice += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line associated with most played character 
FUNC TEXT_LABEL_15 GET_SHRINK_MOST_PLAYED_CHAR(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "CHAR_"
	INT           randInt
	
	// Work out most played character
	enumCharacterList 	eMostPlayedChar = CHAR_MICHAEL
	INT 				iTempTime
	INT 				iMaxTime = 0
	
	// Get Michael's playtime...
	STAT_GET_INT(SP0_TOTAL_PLAYING_TIME, iMaxTime, 0)

	// Get Franklin's play time and check if it's larger...
	STAT_GET_INT(SP1_TOTAL_PLAYING_TIME, iTempTime, 1)
	IF iTempTime > iMaxTime
		iMaxTime = iTempTime
		eMostPlayedChar = CHAR_FRANKLIN
	ENDIF
	
	// Check if Trevor's playtime is longest...
	STAT_GET_INT(SP2_TOTAL_PLAYING_TIME, iTempTime, 2)
	IF iTempTime > iMaxTime
		iMaxTime = iTempTime
		eMostPlayedChar = CHAR_TREVOR
	ENDIF

	// Build text based on most played char
	IF eMostPlayedChar = CHAR_MICHAEL
		sPsychData.tl3mostplayedchar = "M"
		returnStr += "M"
		randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	ELIF eMostPlayedChar = CHAR_FRANKLIN
		sPsychData.tl3mostplayedchar = "F"
		returnStr += "F"
		randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	ELSE
		sPsychData.tl3mostplayedchar = "T"
		returnStr += "T"
		randInt = GET_RANDOM_INT_IN_RANGE(1,12)
	ENDIF
	
	sPsychData.tl3mostplayedchar += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line associated with spending or saving money
FUNC TEXT_LABEL_15 GET_SHRINK_MONEY_SPENT(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "CASH_"
	INT           randInt
	
	// Get total amount of cash spent by all characters
	INT iTempCash, iTotalCash
	STAT_GET_INT(SP0_MONEY_TOTAL_SPENT, iTempCash, 0)	iTotalCash += iTempCash
	STAT_GET_INT(SP1_MONEY_TOTAL_SPENT, iTempCash, 1)	iTotalCash += iTempCash
	STAT_GET_INT(SP2_MONEY_TOTAL_SPENT, iTempCash, 2)	iTotalCash += iTempCash
	
	// Player has spent over 1 million
	IF iTotalCash >= TOTAL_MONEY_SPENT
		sPsychData.tl3moneyspent = "SP"
		returnStr += "SP"
		randInt = GET_RANDOM_INT_IN_RANGE(1,14)
	ELSE
		sPsychData.tl3moneyspent = "SA"
		returnStr += "SA"
		randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	ENDIF
	
	// Cloud data
	sPsychData.tl3moneyspent += randInt
	
	// Return suitable text
	returnStr += randInt
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line associated with whether player frequented strip clubs
FUNC TEXT_LABEL_15 GET_SHRINK_VISITED_STRIP_CLUBS(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "STRIP_"
	INT           randInt
	
	// Get total amount of lap dances...
	INT iTempDances, iTotalDances
	STAT_GET_INT(SP0_LAP_DANCED_BOUGHT, iTempDances, 0)	iTotalDances += iTempDances
	STAT_GET_INT(SP1_LAP_DANCED_BOUGHT, iTempDances, 1)	iTotalDances += iTempDances
	STAT_GET_INT(SP2_LAP_DANCED_BOUGHT, iTempDances, 2)	iTotalDances += iTempDances
	
	// ...or money spent in strip clubs by all characters
	INT iTempCash, iTotalCash
	STAT_GET_INT(SP0_MONEY_SPENT_IN_STRIP_CLUBS, iTempCash, 0) iTotalCash += iTempCash
	STAT_GET_INT(SP1_MONEY_SPENT_IN_STRIP_CLUBS, iTempCash, 1) iTotalCash += iTempCash
	STAT_GET_INT(SP2_MONEY_SPENT_IN_STRIP_CLUBS, iTempCash, 2) iTotalCash += iTempCash
	
	// Player has had 3 or more dances or spent over $100 in a strip club
	IF iTotalDances >= STRIP_CLUB_DANCES 
	OR iTotalCash >= STRIP_CLUB_MONEY_SPENT
		sPsychData.tl3stripclubs = "Y"
		returnStr += "Y"
		randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	ELSE
		sPsychData.tl3stripclubs = "N"
		returnStr += "N"
		randInt = GET_RANDOM_INT_IN_RANGE(1,12)
	ENDIF
	
	// Cloud data
	sPsychData.tl3stripclubs += randInt
	
	// Return suitable text
	returnStr += randInt
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line associated with whether player used prostitutes
FUNC TEXT_LABEL_15 GET_SHRINK_USED_PROSTITUTES(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "PROS_"
	INT           randInt
	
	IF GET_NUM_PROSTITUTE_SERVICES_SOLICITED_BY_PLAYER(CHAR_FRANKLIN) > 0
	OR GET_NUM_PROSTITUTE_SERVICES_SOLICITED_BY_PLAYER(CHAR_MICHAEL) > 0
	OR GET_NUM_PROSTITUTE_SERVICES_SOLICITED_BY_PLAYER(CHAR_TREVOR) > 0
		sPsychData.tl3prostitute = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3prostitute = "N"
		returnStr += "N"
	ENDIF
	
	randInt = GET_RANDOM_INT_IN_RANGE(1,13)

	sPsychData.tl3prostitute += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether Michael has paid any attention to his family
FUNC TEXT_LABEL_15 GET_SHRINK_FAMILY_ATTENTION(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "FAMILY_"
	INT           randInt

	IF (HAS_CONNECTION_BEEN_PLAYED(FC_MICHAEL_AMANDA)
	AND HAS_CONNECTION_BEEN_PLAYED(FC_MICHAEL_JIMMY))
		CPRINTLN(DEBUG_REPEAT, "GET_SHRINK_FAMILY_ATTENTION: passed friend activity.")
		sPsychData.tl3family = "Y"
		returnStr += "Y"
		
	ELIF (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ME_AMANDA].completed = TRUE
	AND g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ME_JIMMY].completed = TRUE
	AND g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ME_TRACEY].completed = TRUE)
		CPRINTLN(DEBUG_REPEAT, "GET_SHRINK_FAMILY_ATTENTION: passed all ME's.")
		sPsychData.tl3family = "Y"
		returnStr += "Y"
		
	ELSE
		CPRINTLN(DEBUG_REPEAT, "GET_SHRINK_FAMILY_ATTENTION: failed.")
		sPsychData.tl3family = "N"
		returnStr += "N"
	ENDIF
	
	randInt = GET_RANDOM_INT_IN_RANGE(1,13)

	sPsychData.tl3family += randInt
	returnStr += randInt
	
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether any attention was paid to the stockmarket
FUNC TEXT_LABEL_15 GET_SHRINK_PLAYED_STOCKMARKET(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "STOCK_"
	INT           randInt
	
	// Get total amount of time spent on stockmarket
	INT iTime
	STAT_GET_INT(TIME_SPENT_ON_STOCKMARKET, iTime, 0)
	
	// Player has spent over x minutes in the stockmarket
	IF iTime >= STOCKMARKET_TIME_SPENT
		sPsychData.tl3stockmarket = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3stockmarket = "N"
		returnStr += "N"
	ENDIF

	randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	
	sPsychData.tl3stockmarket += randInt
	returnStr += randInt
	
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether player killed a lot of peds
FUNC TEXT_LABEL_15 GET_SHRINK_KILLED_PEDS(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "PEDS_"
	INT           randInt
		
	// Get number of innocent peds killed by all characters
	INT iTempPeds, iTotalPeds
	STAT_GET_INT(SP0_KILLS_INNOCENTS, iTempPeds, 0) iTotalPeds += iTempPeds
	STAT_GET_INT(SP1_KILLS_INNOCENTS, iTempPeds, 1) iTotalPeds += iTempPeds
	STAT_GET_INT(SP2_KILLS_INNOCENTS, iTempPeds, 2) iTotalPeds += iTempPeds
	
	// Player has killed over threshold amount of innocent peds
	IF iTotalPeds >= TOTAL_PEDS_KILLED
		sPsychData.tl3killpeds = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3killpeds = "N"
		returnStr += "N"
	ENDIF
	
	randInt = GET_RANDOM_INT_IN_RANGE(1,13)

	sPsychData.tl3killpeds += randInt
	returnStr += randInt
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether player stole a lot of vehicles
FUNC TEXT_LABEL_15 GET_SHRINK_STOLE_VEHICLES(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "VEHS_"
	INT           randInt
	
	// Get number of vehicles stolen by all characters
	INT iTempVehicles, iTotalVehicles
	
	// Michael
	STAT_GET_INT(SP0_NUMBER_STOLEN_COP_VEHICLE, iTempVehicles, 0) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP0_NUMBER_STOLEN_CARS,        iTempVehicles, 0) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP0_NUMBER_STOLEN_BIKES,       iTempVehicles, 0) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP0_NUMBER_STOLEN_BOATS,       iTempVehicles, 0) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP0_NUMBER_STOLEN_HELIS,       iTempVehicles, 0) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP0_NUMBER_STOLEN_QUADBIKES,   iTempVehicles, 0) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP0_NUMBER_STOLEN_BICYCLES,    iTempVehicles, 0) iTotalVehicles += iTempVehicles

	// Franklin
	STAT_GET_INT(SP1_NUMBER_STOLEN_COP_VEHICLE, iTempVehicles, 1) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP1_NUMBER_STOLEN_CARS,        iTempVehicles, 1) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP1_NUMBER_STOLEN_BIKES,       iTempVehicles, 1) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP1_NUMBER_STOLEN_BOATS,       iTempVehicles, 1) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP1_NUMBER_STOLEN_HELIS,       iTempVehicles, 1) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP1_NUMBER_STOLEN_QUADBIKES,   iTempVehicles, 1) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP1_NUMBER_STOLEN_BICYCLES,    iTempVehicles, 1) iTotalVehicles += iTempVehicles

	// Trevor
	STAT_GET_INT(SP2_NUMBER_STOLEN_COP_VEHICLE, iTempVehicles, 2) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP2_NUMBER_STOLEN_CARS,        iTempVehicles, 2) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP2_NUMBER_STOLEN_BIKES,       iTempVehicles, 2) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP2_NUMBER_STOLEN_BOATS,       iTempVehicles, 2) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP2_NUMBER_STOLEN_HELIS,       iTempVehicles, 2) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP2_NUMBER_STOLEN_QUADBIKES,   iTempVehicles, 2) iTotalVehicles += iTempVehicles
	STAT_GET_INT(SP2_NUMBER_STOLEN_BICYCLES,    iTempVehicles, 2) iTotalVehicles += iTempVehicles
	
	// Player has stolen over threshold amount of vehicles
	IF iTotalVehicles >= TOTAL_VEHICLES_STOLEN
		sPsychData.tl3stolenvehicles = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3stolenvehicles = "N"
		returnStr += "N"
	ENDIF

	randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	
	sPsychData.tl3stolenvehicles += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether player was active in yoga
FUNC TEXT_LABEL_15 GET_SHRINK_PERFORMED_YOGA(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "YOGA_"
	INT           randInt
	
	// Has the player completed the Yoga minigame?
	IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_YOGA)
		sPsychData.tl3yoga = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3yoga = "N"
		returnStr += "N"
	ENDIF

	randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	
	sPsychData.tl3yoga += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether player was generally fit and active
FUNC TEXT_LABEL_15 GET_SHRINK_FITNESS(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "FIT_"
	INT           randInt	
	
	// All player fitness levels are above set amounts
	IF  GET_SP_PLAYER_PED_STAT_VALUE(CHAR_FRANKLIN, PS_STAMINA) > FITNESS_LEVEL_FRANKLIN 		// Franklin
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_FRANKLIN, PS_STRENGTH) > FITNESS_LEVEL_FRANKLIN 
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_FRANKLIN, PS_LUNG_CAPACITY) > FITNESS_LEVEL_FRANKLIN 
	
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_STAMINA) > FITNESS_LEVEL_MICHAEL			// Michael
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_STRENGTH) > FITNESS_LEVEL_MICHAEL 
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_LUNG_CAPACITY) > FITNESS_LEVEL_MICHAEL  
	
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_TREVOR, PS_STAMINA) > FITNESS_LEVEL_TREVOR			// Trevor
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_TREVOR, PS_STRENGTH) > FITNESS_LEVEL_TREVOR 
	AND GET_SP_PLAYER_PED_STAT_VALUE(CHAR_TREVOR, PS_LUNG_CAPACITY) > FITNESS_LEVEL_TREVOR  
		sPsychData.tl3fitness = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3fitness = "N"
		returnStr += "N"
	ENDIF

	randInt = GET_RANDOM_INT_IN_RANGE(1,11)
	
	sPsychData.tl3fitness += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether player engaged in random chars/events
FUNC TEXT_LABEL_15 GET_SHRINK_RANDOM_CHARS(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "RAND_"
	INT           randInt
	
	// Get total number of completed RCM's and RE's
	INT iCompletedRC, iCompletedRE, iTotal 
	STAT_GET_INT(NUM_RNDPEOPLE_COMPLETED, iCompletedRC)
	STAT_GET_INT(NUM_RNDEVENTS_COMPLETED, iCompletedRE)
	iTotal = iCompletedRC + iCompletedRE
	
	// Player has completed over x Random Events and Random Character missions
	IF (iTotal > TOTAL_STRANGERS_MET)
		sPsychData.tl3strangers = "Y"
		returnStr += "Y"
	ELSE
		sPsychData.tl3strangers = "N"
		returnStr += "N"
	ENDIF
	
	randInt = GET_RANDOM_INT_IN_RANGE(1,13)
	
	sPsychData.tl3strangers += randInt
	returnStr += randInt

	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns line whether player was generally fit and active
FUNC TEXT_LABEL_15 GET_SHRINK_COLLECTABLES(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "COLLECT_"
	INT           randInt
	
	// Check collectables
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_DIVING_SCRAPS_DONE)		// Submarine parts
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_UNLOCKED_TRACT)	// Epsilon tract
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE)		// Dreyfuss letter
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SPACESHIP_PARTS_DONE)	// Omega spaceship
		sPsychData.tl3collectables = "Y"
		returnStr += "Y"
		randInt = GET_RANDOM_INT_IN_RANGE(1,11)
	ELSE
		sPsychData.tl3collectables = "N"
		returnStr += "N"
		randInt = GET_RANDOM_INT_IN_RANGE(1,12)
	ENDIF

	sPsychData.tl3collectables += randInt
	returnStr += randInt
	
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Returns randomised summary
FUNC TEXT_LABEL_15 GET_SHRINK_SUMMARY(STRUCT_PSYCH_DATA &sPsychData)

	TEXT_LABEL_15 returnStr = "SUMMARY_"
	INT           randInt = GET_RANDOM_INT_IN_RANGE(1,24)
	
	sPsychData.iSummary = randInt
	returnStr += randInt
	
	RETURN returnStr
ENDFUNC

/// PURPOSE:
///    Saves psychology report to a given path for the cloud data
/// PARAMS:
///    iLoadStage 	- the switching stage
///    bSuccess	 	- was it a success
///    sPsychData	- Cloud data
FUNC BOOL SAVE_OUT_PSYCH_DATA(INT &iLoadStage, BOOL &bSuccess, STRUCT_PSYCH_DATA &sPsychData)
	
	// The cloud is down... get out of here!
	IF IS_CLOUD_DOWN_CLOUD_LOADER()	
		CPRINTLN(DEBUG_FLOW, "<FIN_ENDGAME> SAVE_OUT_PSYCH_DATA - NOT IS_CLOUD_DOWN_CLOUD_LOADER()")
		RETURN TRUE
	ENDIF	
	
	//Vars needed
	DATAFILE_DICT dfdMainDict

	SWITCH iLoadStage
		
		// Deal with setup
		CASE 0
			IF IS_SCRIPT_UCG_REQUEST_IN_PROGRESS()
				RETURN FALSE
			ENDIF	
			CLEANUP_DATA_FILE()
			iLoadStage++
		BREAK
		
		//Initilise the data file
		CASE 1			
			
			//SET UP THE DATA FILE
			DATAFILE_CREATE()
			
			//Get a handel on the top level dictionay
			dfdMainDict = DATAFILE_GET_FILE_DICT()
			
			//Add an INT
			DATADICT_SET_INT   	(dfdMainDict, "in",		sPsychData.iIntro) 
			DATADICT_SET_STRING	(dfdMainDict, "st",		sPsychData.tl3storychoice) 
			DATADICT_SET_STRING	(dfdMainDict, "mp",		sPsychData.tl3mostplayedchar) 
			DATADICT_SET_STRING	(dfdMainDict, "ms",		sPsychData.tl3moneyspent) 
			DATADICT_SET_STRING	(dfdMainDict, "sc",		sPsychData.tl3stripclubs) 
			DATADICT_SET_STRING	(dfdMainDict, "pr",		sPsychData.tl3prostitute) 
			DATADICT_SET_STRING	(dfdMainDict, "fa",		sPsychData.tl3family) 
			DATADICT_SET_STRING	(dfdMainDict, "sm",		sPsychData.tl3stockmarket) 
			DATADICT_SET_STRING	(dfdMainDict, "kp",		sPsychData.tl3killpeds) 
			DATADICT_SET_STRING	(dfdMainDict, "sv",		sPsychData.tl3stolenvehicles) 
			DATADICT_SET_STRING	(dfdMainDict, "yo",		sPsychData.tl3yoga) 
			DATADICT_SET_STRING	(dfdMainDict, "fi",		sPsychData.tl3fitness) 
			DATADICT_SET_STRING	(dfdMainDict, "rc",		sPsychData.tl3strangers) 
			DATADICT_SET_STRING	(dfdMainDict, "co",		sPsychData.tl3collectables) 
			DATADICT_SET_INT	(dfdMainDict, "su",		sPsychData.iSummary) 
			DATAFILE_START_SAVE_TO_CLOUD("gta5/psych/index.json")
			
			iLoadStage++ 
		BREAK
		
		CASE 2
			// Wait for it to finish
			IF PROCESS_SAVE_FILE_TO_CLOUD(bSuccess)
				CPRINTLN(DEBUG_FLOW, "<FIN_ENDGAME> SAVE_OUT_PSYCH_DATA - Success!")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	// Not done yet pal
	RETURN FALSE
ENDFUNC
