//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 15/03/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						Finale Strand Intro Cutscene							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_cutscene.sch"

USING "clearmissionarea.sch"
USING "flow_public_core_override.sch"
USING "flow_help_public.sch"
USING "script_misc.sch"
USING "replay_public.sch"
USING "commands_recording.sch"

// Variables
BOOL m_bGameplayCamCentered = FALSE
INT	 m_iCutsceneStartTime
VEHICLE_INDEX viPlayerVehicle
PROC Mission_Cleanup()
	// Force Chop to relaunch for B*1351643
	SET_VEHICLE_AS_NO_LONGER_NEEDED(viPlayerVehicle)
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC Mission_Passed()
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
ENDPROC

PROC Load_Cutscene_Assets()
	REQUEST_CUTSCENE("CHOICE_INT")
	WHILE NOT HAS_CUTSCENE_LOADED()
		REQUEST_CUTSCENE("CHOICE_INT")
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("FRANKLIN", PLAYER_PED_ID())
		ENDIF
		WAIT(0)
	ENDWHILE
ENDPROC

PROC Do_Finale_Intro_Cutscene()
	VEHICLE_INDEX veh = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(veh)
		IF NOT IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(veh, CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
			DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<12.5881, 545.7901, 174.9232>>, 5)
		ENDIF
	ELSE
		DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<12.5881, 545.7901, 174.9232>>, 5)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "FRANKLIN", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
	ENDIF
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
	
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FinaleChoice")
	
	REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
	
	START_CUTSCENE()
	SET_WEATHER_TYPE_NOW("CLEAR")
	
	// B*1897079 - Clear area for cutscene.
	CLEAR_AREA_OF_OBJECTS(<<8.61554, 541.07635, 175.02826>>, 7.5)
	// B*1918436 - Clear area of projectiles
	CLEAR_AREA_OF_PROJECTILES(<<8.61554, 541.07635, 175.02826>>, 7.5)
	IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
		RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<26.15189, 560.65405, 190.32320>>, <<-14.36067, 539.86017, 170.34720>>, 30, << 3.51800, 546.92346, 173.72620>>, 125.6127 , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
	ELSE
		RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<26.15189, 560.65405, 190.32320>>, <<-14.36067, 539.86017, 170.34720>>, 30, <<9.0390, 554.2676, 175.1200>>, 119.3638)
	ENDIF

//	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<26.15189, 560.65405, 190.32320>>, <<-14.36067, 539.86017, 170.34720>>, 30, << 1.4300, 543.5197, 173.3552 >>, 126.3274 , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
	BOOL bCutsceneStarted = FALSE
	m_iCutsceneStartTime = GET_GAME_TIMER()
	
	WHILE IS_CUTSCENE_PLAYING() OR NOT bCutsceneStarted
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FinaleChoice")
		
		IF IS_CUTSCENE_PLAYING()
			bCutsceneStarted = TRUE
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(viPlayerVehicle)
			veh = GET_PLAYERS_LAST_VEHICLE()
			IF DOES_ENTITY_EXIST(veh)
				IF NOT IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(veh, CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
				AND NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_FRANKLIN, VEHICLE_TYPE_CAR, <<-1.3389, 544.1759, 172.7673>>, -1)
					CREATE_PLAYER_VEHICLE(viPlayerVehicle, CHAR_FRANKLIN, <<-1.3389, 544.1759, 172.7673>>, 124.3018, TRUE, VEHICLE_TYPE_CAR)
				ENDIF
			ELSE
				IF NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_FRANKLIN, VEHICLE_TYPE_CAR, <<-1.3389, 544.1759, 172.7673>>, -1)
					CREATE_PLAYER_VEHICLE(viPlayerVehicle, CHAR_FRANKLIN, <<-1.3389, 544.1759, 172.7673>>, 124.3018, TRUE, VEHICLE_TYPE_CAR)
				ENDIF
			ENDIF
		ENDIF
		
//		CPRINTLN(DEBUG_FLOW, "Cutscene time: ", GET_GAME_TIMER() - m_iCutsceneStartTime)
		
		IF NOT m_bGameplayCamCentered
			IF (GET_GAME_TIMER() - m_iCutsceneStartTime) > 92400
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				m_bGameplayCamCentered = TRUE
			ENDIF
		ENDIF

		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FRANKLIN", PLAYER_ONE)
			IF NOT m_bGameplayCamCentered
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				m_bGameplayCamCentered = TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				CPRINTLN(DEBUG_FLOW, "Exit state for camera.")
			ENDIF
		#ENDIF
		
		WAIT(0)
	ENDWHILE
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FinaleChoice")
	REMOVE_CUTSCENE()
	
	CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_CAR)
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC


SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FinaleChoice")
	SET_MISSION_FLAG(TRUE)
	SERVICES_TOGGLE(FALSE)
	Load_Cutscene_Assets()
	Do_Finale_Intro_Cutscene()
	SERVICES_TOGGLE(TRUE)

	Mission_Passed()
ENDSCRIPT
