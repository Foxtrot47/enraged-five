// ___                                                                                   ___
// ___              author: Lawrence Kerr                date: 03/06/2011              ___
// _________________________________________________________________________________________
// ___                                                                                   ___
// ___                         fbi1 - coroners mission                             		 ___
// _________________________________________________________________________________________

//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1

// ************************************* include files *************************************
const_int total_number_of_cia_enemies 				17
const_int total_number_of_removed_cia_enemeis		2    
const_int total_number_of_cia_enemies_top_floor 	7
const_int total_number_of_cia_enemies_outside		5

const_int total_number_of_doctors 					5

const_int stick_dead_zone 28

const_int MAX_SKIP_MENU_LENGTH						7

const_int total_number_of_police_men 				4
const_int total_number_of_police_cars 				2

const_int total_number_of_doors 					15

const_int total_number_of_bins						1

const_int total_number_of_pickups					6


USING "rage_builtins.sch"
USING "globals.sch"
using "script_heist.sch"
USING "flow_public_core_override.sch"
using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "commands_misc.sch"
using "commands_recording.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "model_enums.sch"
using "cellphone_public.sch"
using "selector_public.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
using "chase_hint_cam.sch"
using "locates_public.sch"
using "script_blips.sch" 
using "select_mission_stage.sch"
using "replay_public.sch"
using "commands_cutscene.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "CompletionPercentage_public.sch"
using "drunk_public.sch"
using "weapons_public.sch"
using "building_control_public.sch"
using "emergency_call.sch"
using "clearmissionarea.sch"
using "taxi_functions.sch"
using "ambient_common.sch"
using "cheat_controller_public.sch"


struct ped_structure
	ped_index ped 
	model_names model 
	blip_index blip
	AI_BLIP_STRUCT blip_2
	vector pos 
	vector run_to_pos
	float heading 
	int health
	int accuracy
	bool damaged_by_player
	bool allow_body_armour
	bool created 
	weapon_type weapon
	text_label name
endstruct 


struct vehicle_struct
	vehicle_index veh
	model_names model
	blip_index blip
	vector pos 
	float heading
	float speed
	float skip_time
	int health
	float engine_health
	float petrol_tank_health
	int colour
	int recording_number
	bool been_created 
endstruct

struct object_struct 
	object_index obj
	blip_index blip
	model_names model
	vector pos 
	vector offset_pos
	vector offset_pos_2
	vector rot
	vector offset_rot
	float heading
	float scale 
	float health
	string room_name
	int time
	bool been_created
endstruct 

struct pickup_struct 
	pickup_index pickup
	model_names model
	pickup_type type
	blip_index blip
	vector pos
	vector rot
	bool collected
	string room_name
endstruct 

struct quaternion
	float x
	float y
	float z
	float w
endstruct

enum main_mission_flow
	intro_mocap = 0,
	stealth_cutscene,
	get_to_locker,
	stealth_the_cia_officer,
	find_bodies,
	shootout,
	escape_cutscene,
	lose_cops,
	get_to_hills,
	pass_cutscene,
	mission_fail_stage,
	load_stage_selector_assets
endenum
main_mission_flow mission_flow = intro_mocap

enum main_mission_shootout
	attack_phase_1 = 0,
	get_to_pos_1, 
	attack_phase_2,
	get_to_pos_2,
	do_nothing
endenum 
main_mission_shootout cia_enemy_ai_status[total_number_of_cia_enemies]
main_mission_shootout cia_enemy_top_floor_ai_status[total_number_of_cia_enemies_top_floor]
main_mission_shootout cia_enemy_outside_ai_status[total_number_of_cia_enemies_outside]

enum doctor_ai_enum
	action_1 = 0,
	action_1_status, 
	action_2, 
	action_2_status, 
	action_3_status 
endenum
doctor_ai_enum doctor_ai_status[total_number_of_doctors]

ped_structure cia_enemy[total_number_of_cia_enemies]
ped_structure cia_enemy_top_floor[total_number_of_cia_enemies_top_floor]
ped_structure cia_enemy_outside[total_number_of_cia_enemies_outside]
ped_structure doctor[total_number_of_doctors]
ped_structure dead_body[2]
ped_structure franklin
//ped_structure police_man[total_number_of_police_men]
ped_structure cia_stealth
ped_structure dave

vehicle_struct police_car[total_number_of_police_cars]
vehicle_struct ambient_car[2]
vehicle_struct franklins_car

object_struct clip_board
object_struct pen
object_struct bin_bag[2]
object_struct window
object_struct bone_saw
object_struct bin[total_number_of_bins]
object_struct cia_weapon_obj

bool stop_mission_fail_checks = false
bool alarm_on = false
bool activate_elevator_door_system = false
bool zip_sound_activated = false
bool dialogue_paused = false 
bool dave_norton_phone_call_finished = false
bool break_the_glass_window = false
bool operating_table_fail = false
bool michael_clothes_set = false
bool rumble_pad = false
bool pistol_flash_light_equiped = false
bool intro_mocap_shake_pad = false
bool shootout_assets_removed = false
bool taxi_drop_off_set_for_pass_mocap = false
bool bypass_find_dead_bodies_stage = false 
bool player_weapons_restored = false
bool stop_doctor_2_speaking = false

//int alarm_sound[5] 
int alarm_time 
int original_time
int intro_mocap_status = 0
int fbi1_stealth_cutscene_status = 0
int fbi1_master_controller_status = 0
int fbi1_escape_cutscene_status = 0
int current_line = -1
int phone_call_system_status = 0
int cutscene_index
int doctor_0_cutscene_index
int doctor_1_cutscene_index 
int get_to_hills_status = 0 
int zip_sound = 0
int lift_sound = 0
int i_triggered_text_hashes[22]
int dialogue_time
int dialogue_status = 0
int franklin_ai_system_status = 0
int shootout_time = 0
int doctor_4_time
int inspect_body_0_status = 0
int inspect_body_1_status = 0
int phone_hang_up_count = 0
int music_event_system_status = 0
int cia_stealth_enemy_ai_system_status = 0
int cia_stealth_dialogue_system_status = 0
int special_ability_help_text_system_status = 0
int help_time = 0
int attack_time = 0
int doctor_time[total_number_of_doctors]
int interactive_cutscene_cam_status
int phone_call_system_2_status = 0
int phone_call_time = 0
int delete_wave_one_cia_enemies_and_doctors_system_status = 0
int fail_safe_time
int break_entity_glass_system_status = 0
int replay_video_recorder_system_status = 0

int fbi_door[total_number_of_doors]


#IF IS_DEBUG_BUILD
	int p_skip_time
#endif 


vector cutscene_pos
vector cutscene_rot

float zip_inc 
float zip_y 

string mission_failed_text

structpedsforconversation scripted_speech

locates_header_data locates_data

sequence_index seq

camera_index camera_a

interior_instance_index coroner_interior 

blip_index escape_blip

coverpoint_index cia_enemy_cover_point[total_number_of_cia_enemies]
coverpoint_index cia_enemy_top_floor_cover_point[total_number_of_cia_enemies_top_floor]

vehicle_index players_last_vehicle

pickup_index fbi1_pickup[total_number_of_pickups]

//ptfx_id ptfx_paper 

text_label dialogue_root
text_label specific_label



//**********not present in initialise_mission_variables()
int launch_mission_stage_menu_status = 0
bool replay_active = false

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct menu_stage_selector[MAX_SKIP_MENU_LENGTH]
	widget_group_id fbi1_widget_group	
	int menu_return_stage = 0
#endif 


//****************************************lawrence sdk****************************************

proc disable_dispatch_services()
		
	enable_dispatch_service(dt_fire_department, false)
	enable_dispatch_service(dt_police_automobile, false)
	enable_dispatch_service(dt_police_helicopter, false)
	enable_dispatch_service(dt_ambulance_department, false)
	
endproc 

proc enable_dispatch_services()

	enable_dispatch_service(dt_fire_department, true)
	enable_dispatch_service(dt_police_automobile, true)
	enable_dispatch_service(dt_police_helicopter, true)
	enable_dispatch_service(dt_ambulance_department, true)

endproc 

func bool start_new_cutscene_no_fade(bool clear_players_tasks = true, bool hide_weapon_for_cutscene = true, bool kill_conversation_line_immediately = true, bool ignore_can_player_start_cutscene = false)

	if can_player_start_cutscene() OR ignore_can_player_start_cutscene
	
		clear_prints()
		clear_help()
		if kill_conversation_line_immediately
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
		else 
			kill_any_conversation()
		endif 
	
		SPECIAL_ABILITY_DEACTIVATE(player_id())
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

		display_hud(false)
		display_radar(false)
		set_widescreen_borders(true, 500)
		
		if clear_players_tasks
			set_player_control(player_id(), false, spc_clear_tasks)
		else 
			set_player_control(player_id(), false)
		endif 

		if hide_weapon_for_cutscene
			hide_ped_weapon_for_scripted_cutscene(player_ped_id(), true)
		endif 
		
		disable_dispatch_services()
		
		return true
	endif 
		
	return false

endfunc 


proc end_cutscene(bool clear_tasks = true, float interp_heading = 0.0, float interp_pitch = 0.0, bool enable_emergency_services = true)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	
	if is_player_playing(player_id())
	
		destroy_all_cams()
		
		render_script_cams(false, false)
		set_gameplay_cam_relative_heading(interp_heading)
		set_gameplay_cam_relative_pitch(interp_pitch)
			
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
		SET_PLAYER_CLOTH_PIN_FRAMES(player_id(), 1)
	endif 

	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 
	
	do_screen_fade_in(DEFAULT_FADE_TIME)
	
endproc

proc end_cutscene_no_fade(bool clear_tasks = true, bool update_game_camera = true, bool interpolate_behind_player = false, float interp_heading = 0.0, float interp_pitch = 0.0, int interp_to_game_time = 3000, bool enable_emergency_services = true)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
	
	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	if is_player_playing(player_id())
		
		destroy_all_cams()

		if update_game_camera
			if interpolate_behind_player
				render_script_cams(false, true, interp_to_game_time)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			else 
				render_script_cams(false, false)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			endif 
		endif 
		
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
		SET_PLAYER_CLOTH_PIN_FRAMES(player_id(), 1)
	endif 
	
	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 

endproc 

func bool lk_timer(int &start_time, int time) 
	int current_time 
	current_time = get_game_timer()
	
	if ((current_time - start_time) > time) 
		return true
	endif 
	
	return false
endfunc

func bool is_skip_button_pressed()
	
	return is_control_just_pressed(frontend_control, input_frontend_accept)
	
endfunc 

func bool skip_scripted_cut(int &original_time_temp, int cutscene_skip_time)
	if lk_timer(original_time_temp, cutscene_skip_time)
		if is_screen_faded_in()
			if is_cutscene_skip_button_pressed()	
				return true 
			endif 
		endif 
	endif 
	
	return false 
endfunc 

func bool mission_ped_created_and_injured(bool &mission_ped_created, ped_index &mission_ped)

	if mission_ped_created
		return is_ped_injured(mission_ped)
	endif 
	
	return false 

endfunc 

func bool mission_ped_injured(ped_index &this_ped)

	if does_entity_exist(this_ped)
		return is_ped_injured(this_ped)
	endif 
	
	return false
	
endfunc 

func bool mission_vehicle_injured(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if not is_vehicle_driveable(this_vehicle)
			return true
		endif 
	endif 
	
	return false
	
endfunc 

func bool is_mission_entity_attacked(ped_index &mission_ped, bool clear_damage_entity = false)
	
	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
			if has_entity_been_damaged_by_entity(mission_ped, player_ped_id())
				if clear_damage_entity
					clear_entity_last_damage_entity(mission_ped)
				endif 
				return true
			endif 
		else 
			return true
		endif
	endif 
	
	return false
endfunc 

func bool has_char_task_finished_2(ped_index ped, script_task_name taskname)
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

//gets the currenent car the player is in
func bool get_current_player_vehicle(vehicle_index &test_car)
	
	if is_ped_sitting_in_any_vehicle(player_ped_id())
		test_car = get_players_last_vehicle()
		if does_entity_exist(test_car)				
			if is_vehicle_driveable(test_car)
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc

proc stop_player_vehicle()

	vehicle_index player_car
	
	if get_current_player_vehicle(player_car)
		
		float player_car_speed
			
		set_player_control(player_id(), false)
		set_entity_proofs(player_car, true, true, true, true, true)

		player_car_speed = get_entity_speed(player_car)
			 
		while player_car_speed > 0.2
			wait(0)
			if not is_entity_dead(player_car)
				player_car_speed = get_entity_speed(player_car)
			endif 
		endwhile

		set_entity_proofs(player_car, false, false, false, false, false)
		set_player_control(player_id(), true)

	endif
endproc

func bool add_ped_to_players_group(ped_index &this_ped, group_index &players_group)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
	
			players_group = get_player_group(player_id())
			
			set_ped_as_group_member(this_ped, players_group)
			
			return true
		endif 
	endif 

	return false
	
endfunc 

func float distance_from_player_to_ped(ped_index &mission_ped)

	vector player_pos
	vector mission_ped_pos 

	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
			
			player_pos = get_entity_coords(player_ped_id()) 
			mission_ped_pos = get_entity_coords(mission_ped)
			
			return get_distance_between_coords(player_pos, mission_ped_pos)
		endif 
	endif 
	
	return -1.0

endfunc 

func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence, int sequence_progress = -2)
	
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
	or get_sequence_progress(ped) = sequence_progress
		return true
	endif
	
	return false
endfunc

func bool is_ped_playing_anim_at_phase(string anim_dict_name, string anim_name, float phase_target)
		
	if is_entity_playing_anim(player_ped_id(), anim_dict_name, anim_name) 
		if get_entity_anim_current_time(player_ped_id(), anim_dict_name, anim_name) > phase_target 
			return true 
		endif 
	endif 
	
	return false 
	
endfunc

//the players group does not include the player himself. relgrouphash_player is the player
rel_group_hash player_group
rel_group_hash enemy_group

    
proc add_relationship_groups()

	add_relationship_group("players group", player_group)
	add_relationship_group("enemy group", enemy_group)
	
endproc 

proc setup_relationship_contact(ped_index &this_ped, bool block_temporary_events = false)

	set_relationship_between_groups(acquaintance_type_ped_like, player_group, relgrouphash_player)
	set_relationship_between_groups(acquaintance_type_ped_like, relgrouphash_player, player_group)
	
	set_relationship_between_groups(acquaintance_type_ped_hate, relgrouphash_player, enemy_group)
	set_relationship_between_groups(acquaintance_type_ped_hate, player_group, enemy_group)
	set_relationship_between_groups(acquaintance_type_ped_hate, enemy_group, relgrouphash_player)
	set_relationship_between_groups(acquaintance_type_ped_hate, enemy_group, player_group)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
			
			set_ped_relationship_group_hash(this_ped, player_group)
			
			set_ped_target_loss_response(this_ped, tlr_never_lose_target)
			
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_USE_VEHICLE, false)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
			
		endif 
	endif 

endproc

proc setup_relationship_enemy(ped_index &this_ped, bool block_temporary_events = false)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, enemy_group, RELGROUPHASH_COP)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, enemy_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
			
			set_ped_relationship_group_hash(this_ped, enemy_group) 
			
			//set_ped_combat_attributes(this_ped, ca_will_scan_for_dead_peds, false) 
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
			
			set_ped_target_loss_response(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, true)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
		endif 
	endif 
	
endproc

proc setup_enemy(ped_structure &enemy, bool apply_ped_type_cop = false)

	clear_area(enemy.pos, 1.5, true)
		
	if not apply_ped_type_cop
		enemy.ped = create_ped(pedtype_mission, enemy.model, enemy.pos, enemy.heading)
	else 
		enemy.ped = create_ped(pedtype_cop, enemy.model, enemy.pos, enemy.heading)
	endif 
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	
	SET_PED_MONEY(enemy.ped, 0)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	
	set_entity_health(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		set_entity_only_damaged_by_player(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
//	enemy.blip = ADD_STEALTH_BLIP_FOR_PED(enemy.ped)
//	set_blip_display(enemy.blip, DISPLAY_BLIP) 
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	setup_relationship_enemy(enemy.ped, true)
	
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)
	
	enemy.created = true 

endproc

proc setup_enemy_in_vehicle(ped_structure &enemy, vehicle_index &mission_veh, vehicle_seat veh_seat = vs_driver, bool create_blip = false)

	clear_area(enemy.pos, 2.0, true)
		
	enemy.ped = create_ped_inside_vehicle(mission_veh, pedtype_cop, enemy.model, veh_seat)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, false) //only for cops peds
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	if create_blip
		enemy.blip = create_blip_for_ped(enemy.ped, true)
		set_blip_display(enemy.blip, DISPLAY_BLIP) 
	endif
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	setup_relationship_enemy(enemy.ped, true)

endproc

proc setup_enemy_attributes(ped_structure &enemy)

	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	
	SET_PED_MONEY(enemy.ped, 0)
	
	enemy.blip = create_blip_for_ped(enemy.ped, true)
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	
	set_entity_health(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		set_entity_only_damaged_by_player(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	set_ped_can_evasive_dive(enemy.ped, true)

	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)

endproc 

proc setup_buddy(ped_structure &mission_buddy)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped(pedtype_mission, mission_buddy.model, mission_buddy.pos, mission_buddy.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	set_ped_can_ragdoll(mission_buddy.ped, false)
	
	give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	set_blocking_of_non_temporary_events(mission_buddy.ped, true)

endproc 

proc setup_buddy_attributes(ped_index mission_ped)
	
	set_ped_dies_when_injured(mission_ped, false)
	set_ped_can_be_targetted(mission_ped, false)
	set_ped_suffers_critical_hits(mission_ped, false)
	set_ped_can_evasive_dive(mission_ped, false)
	set_entity_is_target_priority(mission_ped, false)
	set_ped_can_ragdoll(mission_ped, false)
	
	set_ped_seeing_range(mission_ped, 250.00)
	set_ped_hearing_range(mission_ped, 250.00)
	set_ped_id_range(mission_ped, 250.00)
	
	SET_PED_CONFIG_FLAG(mission_ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_DisableExplosionReactions, FALSE)
	
endproc 

func bool ped_structure_are_all_enemies_dead(ped_structure &enemy_array[], bool set_ped_no_longer_needed = true)
	
	int peds_dead = 0
	int k = 0
	
	for k = 0 to (count_of(enemy_array) - 1)
		
		if does_entity_exist(enemy_array[k].ped)
			if is_ped_injured(enemy_array[k].ped)
				
				if set_ped_no_longer_needed
					set_ped_as_no_longer_needed(enemy_array[k].ped)
				endif 
				
				if does_blip_exist(enemy_array[k].blip)
					remove_blip(enemy_array[k].blip)
				endif 
				
				if is_special_ability_active(player_id())
					INFORM_MISSION_STATS_OF_INCREMENT(FBI1_SPECKILLS) 
				endif 
				
				INFORM_MISSION_STATS_OF_INCREMENT(FBI1_KILLS) 
				peds_dead ++
			endif 
		
		else 
		
			if enemy_array[k].created
				peds_dead ++
			endif 
			
		endif 
		
		//only process update_ai_ped_blip if a normal blip is not on the ped
		if not does_blip_exist(enemy_array[k].blip)
			UPDATE_AI_PED_BLIP(enemy_array[k].ped, enemy_array[k].blip_2)
		endif 
		
	endfor 
	
	if peds_dead = count_of(enemy_array) 
		return true
	endif 
	
	return false
endfunc

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL b_force_delete = FALSE)
	
	IF DOES_ENTITY_EXIST(veh)
		
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF IS_ENTITY_A_MISSION_ENTITY(veh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh, false)

			if is_player_playing(player_id())
				
				if is_vehicle_driveable(veh) 
					
					if not is_ped_sitting_in_vehicle(player_ped_id(), veh) 

						IF b_force_delete
							DELETE_VEHICLE(veh)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
						ENDIF
					
					else
					
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
					
					endif 
				
				else 
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
				
				endif 
			
			else 
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) //can't delete vehicle with player in it. Fail system requires player to be in car can't use clear_ped_tasks_immediaely()
			endif 
		endif 
		
	ENDIF
	
ENDPROC

//*****call this function underneath ped_structure_are_all_enemies_dead
func bool ped_structure_are_specific_number_enemies_dead(ped_structure &enemy_array[], int number_of_dead_peds)
	
	int peds_dead = 0
	int k = 0
	
	for k = 0 to count_of(enemy_array) - 1
		if enemy_array[k].created
			if is_ped_injured(enemy_array[k].ped)
				peds_dead++
			endif
		endif 
	endfor 
	
	if peds_dead >= number_of_dead_peds
		return true
	endif 
	
	return false
endfunc

func int ped_structure_get_total_number_of_enemies_dead(ped_structure &enemy_array[])

	int peds_dead = 0
	int k = 0
	
	for k = 0 to count_of(enemy_array) - 1
		if enemy_array[k].created
			if is_ped_injured(enemy_array[k].ped)
				peds_dead++
			endif
		endif 
	endfor 

	return peds_dead

endfunc 



func bool is_coord_in_area_2d(vector test_coord, vector top_left, vector bottom_right)
				
	vector tl
	vector br

	if top_left.x < bottom_right.x	
		tl.x = top_left.x
		br.x = bottom_right.x
	else
		tl.x = bottom_right.x
		br.x = top_left.x
	endif
	
	if top_left.y < bottom_right.y	
		tl.y = top_left.y
		br.y = bottom_right.y	
	else
		tl.y = bottom_right.y
		br.y = top_left.y
	endif

	if (test_coord.x > tl.x) and (test_coord.x < br.x)
		if (test_coord.y > tl.y) and (test_coord.y < br.y)
			return true
		endif 
	endif 

	return false 
endfunc

func bool has_ped_been_harmed(ped_index &mission_ped, int &original_health)

	int current_health
	
	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
			
			current_health = get_entity_health(mission_ped)
			
			if has_entity_been_damaged_by_entity(mission_ped , player_ped_id())		
			or (current_health < original_health) 
			or is_ped_responding_to_event(mission_ped, event_damage)	
				
				return true
			
			endif 
			
		else 
		
			return true
		
		endif  
	
	endif 
	
	return false

endfunc 

func bool has_peds_been_harmed(ped_structure &ped_struct[])

	int k = 0

	for k = 0 to (count_of(ped_struct) - 1)
		
		if has_ped_been_harmed(ped_struct[k].ped, ped_struct[k].health)
			return true 
		endif 
		
	endfor 
	
	return false
	
endfunc 

func bool has_player_antagonised_ped(ped_index &mission_ped, float distance, bool distance_check_on = true)

	if not is_ped_injured(mission_ped)
		
		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_ped)) < distance
			//if has_char_spotted_char(this_ped, player_char_id())
			if CAN_PED_SEE_HATED_PED(mission_ped, PLAYER_PED_ID())
				IF IS_ped_ARMED(PLAYER_ped_ID(), WF_INCLUDE_MELEE|WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
					
					if IS_PLAYER_TARGETTING_ENTITY(player_id(), mission_ped)
						return TRUE
					endif 
					
					if is_player_free_aiming_at_entity(player_id(), mission_ped)
						return TRUE
					endif
				endif 
			endif 
			
			if is_ped_shooting(player_ped_id())
				return true
			endif	
			
		endif
		
		if is_bullet_in_area(get_entity_coords(mission_ped), 4.0)
			return true
		endif 
		
		if distance_check_on
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_ped)) < 1.5
				return true
			endif
		endif 

	endif 

	return FALSE
endfunc 

func bool is_player_targeting_ped(ped_index &mission_ped)

	if not is_ped_injured(mission_ped)
		if is_player_targetting_entity(player_id(), mission_ped)
			return true
		endif 
	endif 

	return false

endfunc 

func bool is_player_targeting_any_peds(ped_structure &mission_ped_array[])

	int k = 0
	
	for k = 0 to count_of(mission_ped_array) - 1
		if is_player_targeting_ped(mission_ped_array[k].ped)
			return true
		endif 
	endfor 
	
	return false 

endfunc 


func bool has_vehicle_been_harmed(vehicle_index &mission_vehicle, int &original_car_health)

	int vehicle_health
	
	if does_entity_exist(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
			
			vehicle_health = get_entity_health(mission_vehicle)
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < original_car_health
			or get_vehicle_engine_health(mission_vehicle) < original_car_health
			or has_entity_been_damaged_by_entity(mission_vehicle, player_ped_id())
			or vehicle_health < original_car_health
			
				return true
			
			endif 
		
		else 
		
			return true 
			
		endif 
	
	endif 
	
	return false
		
endfunc

func bool has_vehicles_been_harmed(vehicle_struct &veh_struct[])

	int k = 0
	
	for k = 0 to (count_of(veh_struct) - 1)
		
		if has_vehicle_been_harmed(veh_struct[k].veh, veh_struct[k].health)
			return true 
		endif 
		
	endfor 
	
	return false
endfunc

func bool is_ped_at_coords(ped_index &mission_ped, vector pos, vector locate_size)
	
	if not is_ped_injured(mission_ped)
		if is_entity_at_coord(mission_ped, pos, locate_size)
			return true 
		endif 
	endif 

	return false 

endfunc 

func quaternion build_quaternion_from_euler_angles(float fpitch, float froll, float fheading)

	quaternion quat

    float fltpitch2 
    float fltyaw2   
    float fltroll2  

    fltpitch2 = fpitch * 0.5
    fltyaw2   = froll * 0.5 
    fltroll2  = fheading * 0.5

    float cosy2 = cos(fltyaw2)
    float siny2 = sin(fltyaw2)
    float cosp2 = cos(fltpitch2)
    float sinp2 = sin(fltpitch2)
    float cosr2 = cos(fltroll2)
    float sinr2 = sin(fltroll2)

    quat.x = cosr2 * sinp2 * cosy2 + sinr2 * cosp2 * siny2
    quat.y = cosr2 * cosp2 * siny2 - sinr2 * sinp2 * cosy2
    quat.y *= -1.0
    quat.z = sinr2 * cosp2 * cosy2 - cosr2 * sinp2 * siny2
    quat.w = cosr2 * cosp2 * cosy2 + sinr2 * sinp2 * siny2
	
	return quat

endfunc 

func bool are_vectors_alomost_equal(vector vector_a, vector vector_b, vector unit_vector_x_multiplier)

	//vec b is the target vector
	//vec a is the current positon of the object
	//unit_vector_x_multiplier is the unitvector * frame time * multiplier
	
	vector vector_ba
	vector future_vector

	vector_ba = vector_b - vector_a
	
	if (vmag(vector_ba) < 0.2)
		return true
	endif
	
	future_vector = (vector_b - (vector_a + unit_vector_x_multiplier))
	if (vmag(future_vector) > vmag(vector_ba))
		return true
	endif 
	
	return false

endfunc 

func bool is_ped_in_front_of_ped(ped_index mission_ped_0, ped_index mission_ped_1)

	if does_entity_exist(mission_ped_0)
		if does_entity_exist(mission_ped_1)
			if not is_ped_injured(mission_ped_0)
				if not is_ped_injured(mission_ped_1)
					
					vector pos_a
					vector pos_b
					vector vec_ba
					
					vector ped_1_offset
					vector ped_1_unit_vector
	
					get_entity_coords(mission_ped_0, pos_a.x, pos_a.y, pos_a.z) 
					
					get_entity_coords(mission_ped_1, pos_b.x, pos_b.y, pos_b.z) 
				
					vec_ba = pos_b - pos_a
					
					get_offset_from_entity_in_world_coords(mission_ped_1, 0.0, 0.5, 0.0, ped_1_offset.x, ped_1_offset.y, ped_1_offset.z)
					ped_1_unit_vector = ped_1_offset - pos_b
					
					if dot_product(vec_ba, ped_1_unit_vector) < 0.0
						
						#IF IS_DEBUG_BUILD
						printstring("infront")
						printnl()
						#endif
						
						return (true)
					endif 
				endif 
			endif 
		endif 
	endif 
	
	#IF IS_DEBUG_BUILD
	printstring("behind")
	printnl()
	#endif 
					
	return false
	
endfunc 

func bool is_vehicle_stuck_every_check(vehicle_index &vehicle)
	
	if does_entity_exist(vehicle)

		if is_vehicle_driveable(vehicle)
			
			if is_vehicle_stuck_timer_up(vehicle, veh_stuck_on_roof, roof_time)
			or is_vehicle_stuck_timer_up(vehicle, veh_stuck_jammed, jammed_time)
			or is_vehicle_stuck_timer_up(vehicle, veh_stuck_hung_up, hung_up_time)
			or is_vehicle_stuck_timer_up(vehicle, veh_stuck_on_side, side_time)
			
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc 


proc repostion_players_last_vehicle(vehicle_index &mission_veh, vector area_pos_check, vector area_dimensions, vector veh_pos, float veh_heading)
	
	mission_veh = get_players_last_vehicle()

	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)
			if not is_big_vehicle(mission_veh)
				if not (get_entity_model(mission_veh) = taxi)
					if is_entity_at_coord(mission_veh, area_pos_check, area_dimensions)
					
						if not is_entity_a_mission_entity(mission_veh)
							set_entity_as_mission_entity(mission_veh)
						endif 
						
						if not is_vehicle_in_players_garage(mission_veh, get_current_player_ped_enum(), false)
						
							clear_area(veh_pos, 10.0, true)
							set_entity_coords(mission_veh, veh_pos)  
							set_entity_heading(mission_veh, veh_heading)
							set_vehicle_on_ground_properly(mission_veh)
							
						endif 
					endif 
				endif 
			endif 
		endif
	endif 
	
endproc 

func bool is_ped_inside_interior(ped_index mission_ped, vector interior_pos)

	interior_instance_index interior
	interior_instance_index mission_ped_interior
	
	interior = get_interior_at_coords(interior_pos)
	mission_ped_interior = get_interior_from_entity(mission_ped)
	
	if not (mission_ped_interior = null)
		if (mission_ped_interior = interior)
			return true 
		endif 
	endif 
	
	return false 

endfunc 

func bool is_ped_in_specific_room(ped_index mission_ped, vector interior_pos, string room_name, string interior_name)

	interior_instance_index interior
	interior_instance_index ped_current_interior
	
	int ped_current_room_hash_key
	int room_hash_key
	
	if not is_ped_injured(mission_ped)

		interior = get_interior_at_coords_with_type(interior_pos, interior_name)	
		ped_current_interior = GET_INTERIOR_FROM_ENTITY(mission_ped)
		
		if not (interior = null)

			if (interior = ped_current_interior)

				ped_current_room_hash_key = GET_KEY_FOR_ENTITY_IN_ROOM(mission_ped)
				room_hash_key = get_hash_key(room_name)
		
				if (ped_current_room_hash_key != 0)
					
					if ped_current_room_hash_key = room_hash_key
						return true 
					endif 
				endif 
			endif 
		endif 
		
	endif 
	
	return false 
	
endfunc 

/// PURPOSE:
///    Sorts the triggered label array such that all the empty elements are at the end.
PROC REMOVE_LABEL_ARRAY_SPACES()
    INT k = 0
 	REPEAT (COUNT_OF(i_triggered_text_hashes) - 1) k
	    IF i_triggered_text_hashes[k] = 0
	    	IF i_triggered_text_hashes[k+1] != 0
	        	i_triggered_text_hashes[k] = i_triggered_text_hashes[k+1]
	            i_triggered_text_hashes[k+1] = 0
	        ENDIF
	    ENDIF
    ENDREPEAT
ENDPROC

/// PURPOSE:
///    Finds the array index of a particular triggered label, or -1 if the label hasn't been added.
FUNC INT GET_LABEL_INDEX(INT i_label_hash)
    INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
		IF i_triggered_text_hashes[k] = 0 //We've reached the end of the filled section of the array, no need to continue.
        	RETURN -1
        ELIF i_triggered_text_hashes[k] = i_label_hash
        	RETURN k
        ENDIF
    ENDREPEAT
      
    RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given label has been triggered.  
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING str_label)
	RETURN (GET_LABEL_INDEX(GET_HASH_KEY(str_label)) != -1)
ENDFUNC

/// PURPOSE:
///    Adds/removes a text label to/from the list of labels that have been triggered.
PROC SET_LABEL_AS_TRIGGERED(STRING str_label, BOOL b_trigger)
	
	INT i_hash = GET_HASH_KEY(str_label)
	INT k = 0

	IF b_trigger
	    BOOL b_added = FALSE
	    
	    WHILE NOT b_added AND k < COUNT_OF(i_triggered_text_hashes)
	    	
			IF i_triggered_text_hashes[k] = i_hash //The label is already in the array, don't add it again.
	        	b_added = TRUE 
	        ELIF i_triggered_text_hashes[k] = 0
	        	i_triggered_text_hashes[k] = i_hash
	            b_added = TRUE
	        ENDIF
	          
	        k++
	    ENDWHILE

	    #IF IS_DEBUG_BUILD
	    	IF NOT b_added
	        	SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Failed to add label, array is full.")
	        ENDIF
	    #ENDIF
	ELSE
	    
		INT i_index = GET_LABEL_INDEX(i_hash)
	    IF i_index != -1
			i_triggered_text_hashes[i_index] = 0
	        REMOVE_LABEL_ARRAY_SPACES()
	    ENDIF
		
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
   		i_triggered_text_hashes[k] = 0
	ENDREPEAT
ENDPROC


proc setup_ped_proofs(ped_index &this_ped)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
			set_entity_proofs(this_ped, true, true, true, true, true)
		endif 
	endif 
	
endproc 

proc deactivate_ped_proofs(ped_index &this_ped)

	int ped_health
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)

			ped_health = get_entity_health(this_ped)

			if ped_health < 110
				set_entity_health(this_ped, 120)
			else 
				set_entity_health(this_ped, ped_health)
			endif 
			
			set_entity_proofs(this_ped, false, false, false, false, false)
		endif 
	endif 

endproc


proc setup_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			
			if get_entity_health(this_vehicle) < 500
				set_entity_health(this_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(this_vehicle) < 500
				set_vehicle_engine_health(this_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(this_vehicle) < 500.00
				set_vehicle_petrol_tank_health(this_vehicle, 500.00)
			endif 
				
			set_entity_proofs(this_vehicle, true, true, true, true, true)
		endif 
	endif 
	
endproc

proc restore_vehicle_health(vehicle_index &mission_vehicle)
								
	if does_entity_exist(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)

			if get_entity_health(mission_vehicle) < 500
				set_entity_health(mission_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(mission_vehicle) < 500
				set_vehicle_engine_health(mission_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < 500.00
				set_vehicle_petrol_tank_health(mission_vehicle, 500.00)
			endif 
			
		endif 
	endif 
	
endproc

proc deactivate_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			set_entity_proofs(this_vehicle, false, false, false, false, false)
			restore_vehicle_health(this_vehicle)
		endif 
	endif 
	
endproc

PROC EQUIP_BEST_PLAYER_WEAPON(ped_index mission_ped, BOOL bForceIntoHand = true)
  
  IF NOT IS_PED_INJURED(mission_ped)
       
		WEAPON_TYPE bestWeapon = GET_BEST_PED_WEAPON(mission_ped)
        
		IF bestWeapon != WEAPONTYPE_UNARMED
             SET_CURRENT_PED_WEAPON(mission_ped, bestWeapon, bForceIntoHand)
        ENDIF
  ENDIF
  
ENDPROC

proc setup_player_fail_weapon(ped_index mission_ped, weapon_type backup_weapon, int num_of_bullets)

	weapon_type players_weapon

	players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
	
	if players_weapon != WEAPONTYPE_INVALID
	and players_weapon != WEAPONTYPE_UNARMED 
	
		if get_ammo_in_ped_weapon(mission_ped, players_weapon) < 10
			set_ped_ammo(mission_ped, players_weapon, 10)	
		endif 
		
		set_current_ped_weapon(mission_ped, players_weapon, true)
	
	else 
	
		if not HAS_PED_GOT_WEAPON(mission_ped, backup_weapon)
			give_weapon_to_ped(mission_ped, backup_weapon, num_of_bullets, true, true)
		else 
			if get_ammo_in_ped_weapon(mission_ped, backup_weapon) < num_of_bullets
				set_ped_ammo(mission_ped, backup_weapon, num_of_bullets)	
				set_current_ped_weapon(mission_ped, backup_weapon, true)
			endif 
		endif 
	
	endif 
	
endproc 

proc setup_weapon_for_ped(ped_index mission_ped, weapon_type ped_weapon, int num_of_bullets, bool force_weapon_into_hand = true, bool equip_weapon = true, bool equipe_best_weapon = true)
	
	IF not HAS_PED_GOT_FIREARM(mission_ped, false)
		
		give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
	
	else 
	
		weapon_type players_weapon

		if equipe_best_weapon
		
			EQUIP_BEST_PLAYER_WEAPON(mission_ped, force_weapon_into_hand)
			get_current_ped_weapon(mission_ped, players_weapon)
			
		else 
		
			players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
			
			if players_weapon = WEAPONTYPE_INVALID
			or players_weapon = WEAPONTYPE_UNARMED 
				
				if not HAS_PED_GOT_WEAPON(mission_ped, ped_weapon)
					give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
					players_weapon = ped_weapon
					exit
				endif 
			endif 

		endif 
		
		if players_weapon = WEAPONTYPE_GRENADELAUNCHER
		or players_weapon = WEAPONTYPE_RPG
			
			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < 5
				set_ped_ammo(mission_ped, players_weapon, 5)
			endif 
			
		else 

			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < num_of_bullets
				set_ped_ammo(mission_ped, players_weapon, num_of_bullets)
			endif 
			
		endif 
		
		set_current_ped_weapon(mission_ped, players_weapon, force_weapon_into_hand) 

	endif 
	
endproc 

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

func bool is_ped_within_range_of_target_heading(ped_index miss_ped, float target_heading, float heading_range)
					
	float heading_dif
	
	heading_dif = (target_heading - get_entity_heading(miss_ped))
	
	if heading_dif > 180
		heading_dif -= 360
	endif 
	if heading_dif < -180
		heading_dif += 360
	endif 
	
	if absf(heading_dif) <= heading_range
		return true 
	endif 
	
	return false

endfunc

func bool is_ped_within_heading_range_of_entity(ped_index miss_ped, entity_index miss_entity, float heading_range)
					
	vector vec_ba
	float heading_dif
	
	if does_entity_exist(miss_entity)
		
		vec_ba = get_entity_coords(miss_entity, false) - get_entity_coords(miss_ped)
		
		heading_dif = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(miss_ped))
		
		if heading_dif > 180
			heading_dif -= 360
		endif 
		if heading_dif < -180
			heading_dif += 360
		endif 
		
		if absf(heading_dif) <= heading_range
			return true 
		endif 
		
	endif 
	
	return false

endfunc

func float get_relative_heading_beteween_player_and_ped(ped_index miss_ped)

	vector vec_ba
	float aim_heading

	vec_ba = get_entity_coords(miss_ped, false) - get_entity_coords(player_ped_id())
	
	printstring("get_heading_from_vector_2d(vec_ba.x, vec_ba.y): ")
	printfloat(get_heading_from_vector_2d(vec_ba.x, vec_ba.y))
	printnl()
	
	printstring("player heading: ")
	printfloat(get_entity_heading(player_ped_id()))
	printnl()
	
	//z rotation
	aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(player_ped_id()))
	
	
	printstring("heading diff: ")
	printfloat(aim_heading)
	printnl()
	
	if aim_heading > 180
		aim_heading -= 360
	endif 
	if aim_heading < -180
		aim_heading += 360
	endif 
	
	printstring("relative heading: ")
	printfloat(aim_heading)
	printnl()

	return aim_heading
	
endfunc

func bool move_door_with_new_door_system(int door_hash, bool open_door = true, float door_speed = 1.0)

	float door_ratio

//	if DOOR_SYSTEM_GET_IS_PHYSICS_LOADED(door_hash)

		door_ratio = DOOR_SYSTEM_GET_OPEN_RATIO(door_hash)

		if open_door
		
			door_ratio += get_frame_time() * door_speed
		
			if door_ratio >= 1.0
				door_ratio = 1.0
			endif 
			
			DOOR_SYSTEM_SET_OPEN_RATIO(door_hash, door_ratio, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(door_hash, DOORSTATE_force_locked_this_frame, false, true)
			
			if door_ratio = 1.0
				return true
			endif 
			
		else 
		
			door_ratio -= get_frame_time() * door_speed
		
			if door_ratio <= 0.0
				door_ratio = 0.0
			endif 
			
			DOOR_SYSTEM_SET_OPEN_RATIO(door_hash, door_ratio, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(door_hash, DOORSTATE_force_locked_this_frame, false, true)
			
			if door_ratio = 0.0
				return true
			endif 
		
		endif 
		
//	endif 

	return false

endfunc

func bool move_doors_with_new_door_system(int left_door_hash, int right_door_hash, bool open_door = true, float door_speed = 1.0)

	float left_door_ratio
	float right_door_ratio

	if DOOR_SYSTEM_GET_IS_PHYSICS_LOADED(left_door_hash)
	and DOOR_SYSTEM_GET_IS_PHYSICS_LOADED(right_door_hash)

		left_door_ratio = DOOR_SYSTEM_GET_OPEN_RATIO(left_door_hash)
		right_door_ratio = DOOR_SYSTEM_GET_OPEN_RATIO(right_door_hash)

		if open_door
		
			left_door_ratio -= get_frame_time() * door_speed	

			if left_door_ratio <= -1.0
				left_door_ratio = -1.0
			endif
			
			right_door_ratio += get_frame_time() * door_speed
			
			if right_door_ratio >= 1.0
				right_door_ratio = 1.0
			endif 
			
			DOOR_SYSTEM_SET_OPEN_RATIO(left_door_hash, left_door_ratio, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(left_door_hash, DOORSTATE_force_locked_this_frame, false, true)
			
			DOOR_SYSTEM_SET_OPEN_RATIO(right_door_hash, right_door_ratio, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(right_door_hash, DOORSTATE_force_locked_this_frame, false, true)
			
			if (left_door_ratio = -1.0) and (right_door_ratio = 1.0)
				return true
			endif 
			
		else 

			left_door_ratio += get_frame_time() * door_speed	

			if left_door_ratio >= 0.0
				left_door_ratio = 0.0
			endif
			
			right_door_ratio -= get_frame_time() * door_speed
			
			if right_door_ratio <= 0.0
				right_door_ratio = 0.0
			endif 
			
			DOOR_SYSTEM_SET_OPEN_RATIO(left_door_hash, left_door_ratio, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(left_door_hash, DOORSTATE_force_locked_this_frame, false, true)
			
			DOOR_SYSTEM_SET_OPEN_RATIO(right_door_hash, right_door_ratio, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(right_door_hash, DOORSTATE_force_locked_this_frame, false, true)
			
			if (left_door_ratio = 0.0) and (right_door_ratio = 0.0)
				return true
			endif 
		
		endif 
		
	endif 

	return false

endfunc 

proc set_players_last_vehicle_to_vehicle_gen(vector car_pos, float car_heading, bool switch_vehicle_engine_off = true) 	
	vehicle_index players_last_car 
	
	players_last_car = GET_PLAYERS_LAST_VEHICLE()//GET_MISSION_START_VEHICLE_INDEX()
	
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
		
			if switch_vehicle_engine_off
				set_vehicle_engine_on(players_last_car, true, true)
			endif 

			SET_MISSION_VEHICLE_GEN_VEHICLE(players_last_car, car_pos, car_heading) 
			
		endif 
	endif 
	
endproc 

proc remove_player_ped_props(ped_index mission_ped, bool head_prop = true, bool eyes_prop = true, bool ears_prop = true)

	if head_prop
		set_ped_comp_item_current_sp(mission_ped, COMP_TYPE_PROPS, PROPS_HEAD_NONE, false)
	endif 

	if eyes_prop
		set_ped_comp_item_current_sp(mission_ped, COMP_TYPE_PROPS, PROPS_EYES_NONE, false)
	endif 

	if ears_prop
		set_ped_comp_item_current_sp(mission_ped, COMP_TYPE_PROPS, PROPS_EARS_NONE, false)
	endif 
	
endproc 

//****************************************end of lawrence sdk****************************************


//**************************************widgets****************************************

#IF IS_DEBUG_BUILD

bool widget_reset_cutscene = false
bool widget_vehicle_colour_active = false
bool widget_entity_camera_active = false
bool widget_play_ptfx = false
bool widget_use_shallow_dof_mode = false
bool widget_apply_dof_change = false
bool widget_activate_ptfx = false

float widget_temp_width
float widget_temp_height
float widget_temp_depth
float widget_angled_length
float widget_anim_speed = 1.0
float widget_heading
float widget_pitch
float widget_fov
float widget_near_dof = 0.1
float widget_far_dof = 150.0	
float widget_dof_strength = 1.0

vector widget_offset_bl
vector widget_offset_br
vector widget_bl
vector widget_br
vector widget_object_pos
vector widget_object_rot
vector widget_object_pos_2
vector widget_object_rot_2
vector widget_cam_attach_offset
vector widget_cam_point_offset
vector widget_ptfx_pos
vector widget_ptfx_rot

camera_index widget_camera



//mission specific widget variables
int widget_colour_0 
int widget_colour_1

int mop_bone


proc locate_widget()

	vector widget_player_pos
			
	widget_player_pos = get_entity_coords(player_ped_id())
	is_entity_at_coord(player_ped_id(), widget_player_pos, <<widget_temp_width, widget_temp_depth, widget_temp_height>>, false, true)		

endproc

proc angled_area_locate_widget(entity_index mission_entity)

	if does_entity_exist(mission_entity)

		widget_bl = get_offset_from_entity_in_world_coords(mission_entity, widget_offset_bl) 
		widget_br = get_offset_from_entity_in_world_coords(mission_entity, widget_offset_br)

		is_entity_in_angled_area(mission_entity, widget_bl, widget_br, widget_angled_length, false, true)	
		
		if widget_offset_br.z > 0.0
			set_entity_heading(mission_entity, widget_heading)
		endif 
		
	endif 

endproc

proc object_angled_area_widget(object_index &mission_object)

	if does_entity_exist(mission_object)
		
		widget_bl = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_bl) 
		widget_br = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_br)

		is_entity_in_angled_area(mission_object, widget_bl, widget_br, widget_angled_length, false, true)	
		
		if widget_offset_br.z > 0.0
			set_entity_heading(player_ped_id(), widget_heading)
		endif 
		
	endif 

endproc 

proc attach_object_to_ped_and_move(ped_index mission_ped, object_index &mission_object)

	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
			if does_entity_exist(mission_object)
				attach_entity_to_entity(mission_object, mission_ped, bonetag_r_hand, widget_object_pos, widget_object_rot)
			endif 
		endif 
	endif
	
endproc 

proc set_the_ped_anim_speed(ped_index mission_ped, string dict_name, string anim_name, float anim_speed)

	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
			if is_entity_playing_anim(mission_ped, dict_name, anim_name)
				set_entity_anim_speed(mission_ped, dict_name, anim_name, anim_speed)
			endif 
		endif 
	endif 
	
endproc

proc create_and_move_object(object_index &mission_object)

	if does_entity_exist(mission_object)
		set_entity_coords_no_offset(mission_object, widget_object_pos)
		set_entity_rotation(mission_object, widget_object_rot)  
	endif 
		
endproc 

proc create_and_move_object_2(object_index &mission_object)

	if does_entity_exist(mission_object)
		set_entity_coords_no_offset(mission_object, widget_object_pos_2)
		set_entity_rotation(mission_object, widget_object_rot_2)  
	endif 
		
endproc 

proc attach_object_to_object_and_move(object_index &parent_object, object_index &mission_object)

	if does_entity_exist(parent_object)
		if does_entity_exist(mission_object)
			attach_entity_to_entity(mission_object, parent_object, 0, widget_object_pos_2, widget_object_rot_2)
		endif 
	endif 
	
endproc 


proc vehicle_colour_widget()

	vehicle_index mission_car

	get_current_player_vehicle(mission_car)

	if widget_vehicle_colour_active
		if does_entity_exist(mission_car)
			if is_vehicle_driveable(mission_car)
				set_vehicle_colours(mission_car, widget_colour_0, widget_colour_1)
			endif 
		endif 
	endif
	
endproc

proc camera_attached_to_entity_widget(entity_index mission_entity)

	if widget_entity_camera_active
		if not does_cam_exist(widget_camera)

			widget_camera = create_cam("default_scripted_camera", false)
			attach_cam_to_entity(widget_camera, mission_entity, widget_cam_attach_offset)
			point_cam_at_entity(widget_camera, mission_entity, widget_cam_point_offset)

			set_cam_active(widget_camera, true)
								
			render_script_cams(true, false)
			
		else 
		
			attach_cam_to_entity(widget_camera, mission_entity, widget_cam_attach_offset)
			
			point_cam_at_entity(widget_camera, mission_entity, widget_cam_point_offset)
			
			set_cam_fov(widget_camera, widget_fov)
			
		endif 

	endif 

endproc 

proc play_ptfx_on_truck_widget()

//	if widget_play_ptfx 
//	
//		clear_player_wanted_level(player_id())
//	
//		if does_entity_exist(players_truck.veh)
//		
//			if is_vehicle_driveable(players_truck.veh)
//	
//				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_fbi1_paper_landing", players_truck.veh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	
//				widget_play_ptfx = false
//				
//			endif 
//			
//		endif 
//	
//	endif 

endproc 

proc relative_pitch_and_heading_widget()

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(widget_heading)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(widget_pitch)

endproc

proc pfx_widget()

	if widget_activate_ptfx
	
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_fbi_autopsy_blood", widget_ptfx_pos, widget_ptfx_rot)
		
		widget_activate_ptfx = false
	
	endif 

endproc 

proc load_lk_widgets()
	
	fbi1_widget_group = start_widget_group("fbi1")
	
		start_widget_group("locate widget")
			add_widget_float_slider("width", widget_temp_width, -500.0, 500.0, 0.1)
			add_widget_float_slider("depth", widget_temp_depth, -500.0, 500.0, 0.1)
			add_widget_float_slider("height", widget_temp_height, -500.0, 500.0, 0.1)
		stop_widget_group()
		
		start_widget_group("angled_area")
			add_widget_float_slider("left offset x", widget_offset_bl.x, -5000.0, 5000.0, 0.1)
			add_widget_float_slider("left offset y", widget_offset_bl.y, -5000.0, 5000.0, 0.1)
			add_widget_float_slider("left offset z", widget_offset_bl.z, -5000.0, 5000.0, 0.1)
			
			add_widget_float_slider("right offset x", widget_offset_br.x, -5000.0, 5000.0, 0.1)
			add_widget_float_slider("right offset y", widget_offset_br.y, -5000.0, 5000.0, 0.1)
			add_widget_float_slider("right offset z", widget_offset_br.z, -5000.0, 5000.0, 0.1)
			
			add_widget_float_slider("angled area length", widget_angled_length, -5000.0, 5000.0, 0.1)
			
			add_widget_float_slider("heading", widget_heading, -5000.0, 5000.0, 0.1)
			
			add_widget_float_read_only("widget_bl x", widget_bl.x)
			add_widget_float_read_only("widget_bl y", widget_bl.y)
			add_widget_float_read_only("widget_bl z", widget_bl.z)
			
			add_widget_float_read_only("widget_br x", widget_br.x)
			add_widget_float_read_only("widget_br y", widget_br.y)
			add_widget_float_read_only("widget_br z", widget_br.z)
			
		stop_widget_group()

		start_widget_group("object attach to ped widget")
			add_widget_float_slider("pos offset x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot offset x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()

		start_widget_group("anim speed widget")
			add_widget_float_slider("anim speed", widget_anim_speed, 0.1, 10.0, 0.01)
		stop_widget_group()
		
		start_widget_group("reset cutscene widget")
			add_widget_bool("reset cutscene", widget_reset_cutscene)
		stop_widget_group()
		
		
		start_widget_group("object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("object pos and rot widget 2")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("attach object to object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		
		start_widget_group("mop bone widget")
			add_widget_int_slider("mop bone pos", mop_bone, 0, 30, 1)
		stop_widget_group()
		
		start_widget_group("vehicle colour")
			add_widget_bool("activate car colour", widget_vehicle_colour_active)
			
			add_widget_int_slider("colour 0:", widget_colour_0, -50, 550, 1)
			add_widget_int_slider("colour 1", widget_colour_1, -50, 550, 1)
		stop_widget_group()
		
		start_widget_group("camaera attached to entity widget")
			add_widget_bool("create camera", widget_entity_camera_active)
			
			add_widget_float_slider("pos x", widget_cam_attach_offset.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_cam_attach_offset.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_cam_attach_offset.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("point offset x", widget_cam_point_offset.x, -3000.0, 3000.0, 0.1)
			add_widget_float_slider("point offset y", widget_cam_point_offset.y, -3000.0, 3000.0, 0.1)
			add_widget_float_slider("point offset z", widget_cam_point_offset.z, -3000.0, 3000.0, 0.1)
			
			add_widget_float_slider("fov", widget_fov, -3000.0, 3000.0, 0.1)
		stop_widget_group()
		
		start_widget_group("trigger truck ptfx")
			add_widget_bool("Start ptfx", widget_play_ptfx)
		stop_widget_group()
		
		start_widget_group("mission flag status")
			ADD_WIDGET_int_read_only("fbi1_master_controller_status", fbi1_master_controller_status)
		stop_widget_group()	
		
		start_widget_group("camaera DOF")
			
			add_widget_float_slider("near DOF", widget_near_dof, -3000.0, 3000.0, 1.0)
			add_widget_float_slider("far DOF", widget_far_dof, -3000.0, 3000.0, 1.0)
			add_widget_float_slider("SET_CAM_DOF_STRENGTH", WIDGET_dof_strength, -3000.0, 3000.0, 1.0) 
			add_widget_bool("SET_CAM_USE_SHALLOW_DOF_MODE", widget_use_shallow_dof_mode)
			add_widget_bool("apply dof to camera", widget_apply_dof_change)

		stop_widget_group()
		
		start_widget_group("relative_pitch_and_heading_widget")
			
			add_widget_float_slider("widget_heading", widget_heading, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("widget_pitch", widget_pitch, -3000.0, 3000.0, 0.01)
			
		stop_widget_group()
		
		start_WIDGET_GROUP("activate ptfx")
			add_widget_bool("toggle ptfx",  widget_activate_ptfx)
			add_widget_float_slider("widget_ptfx_pos x", widget_ptfx_pos.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_pos y", widget_ptfx_pos.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_pos z", widget_ptfx_pos.z, -4000.0, 4000.0, 0.1)
			
			add_widget_float_slider("widget_ptfx_rot x", widget_ptfx_rot.x, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_rot y", widget_ptfx_rot.y, -4000.0, 4000.0, 0.1)
			add_widget_float_slider("widget_ptfx_rot z", widget_ptfx_rot.z, -4000.0, 4000.0, 0.1)
		stop_widget_group()

	stop_widget_group()
	
endproc

#endif


//*************************************end widget******************************************

proc reset_glass_window()
	
	if window.been_created
	
		set_object_as_no_longer_needed(window.obj)
		
		remove_model_swap(<<236.03, -1357.49, 40.45>>, 1.0, V_ILev_Cor_windowSolid, V_ILev_Cor_windowSmash)

		window.been_created = false
		//script_assert("window swapped")
	endif 
	
endproc 

proc remove_all_mission_doors()

	int i 
	
	for i = 0 to count_of(fbi_door) - 1
		if is_door_registered_with_system(fbi_door[i])
			REMOVE_DOOR_FROM_SYSTEM(fbi_door[i])
		endif 
	endfor
	
endproc 

proc mission_cleanup()

	if IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
	else 
		REMOVE_CUTSCENE()
	endif 

	REMOVE_CUTSCENE()

	SET_PLAYER_PED_DATA_IN_CUTSCENES()

	kill_any_conversation()
	
	set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_NORMAL, true)
	
	//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, FALSE)
	//RESTORE_PLAYER_PED_WEAPONS(player_ped_id()) //gives the player back all their weapons
	if not player_weapons_restored
		RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(player_ped_id(), false)
	endif 

	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, false)
	
	if is_player_playing(player_id())

		SET_PED_STEALTH_MOVEMENT(player_ped_id(), false)
		SET_PED_USING_ACTION_MODE(player_ped_id(), false)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, false)
		
		set_player_control(player_id(), true)
		
		if not is_entity_attached(player_ped_id())
			freeze_entity_position(player_ped_id(), false)
		endif 
	endif
	
	stop_alarm("FBI_01_MORGUE_ALARMS", false)
	
	trigger_music_event("FBI1_DEAD")
	
	stop_audio_scenes()
	
	SET_AUDIO_SCRIPT_CLEANUP_TIME(5000)
	
	disable_cellphone(false)
	
	 SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(1.0)
	sET_WANTED_LEVEL_DIFFICULTY(player_id(), 0)
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()

	SET_SCRIPT_GFX_DRAW_ORDER(gfx_order_after_hud)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(false)
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	remove_all_mission_doors()
	
	reset_glass_window()
	
	terminate_this_thread()

endproc 

proc mission_passed()

	mission_flow_mission_passed()

	clear_player_wanted_level(player_id())

	mission_cleanup()

endproc 

proc mission_failed()

	//reset_glass_window()
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS("FBI1_WPN") 

	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
endproc 

proc initialise_mission_variables()

	int i

	#IF IS_DEBUG_BUILD
		menu_stage_selector[0].sTxtLabel = "Start of mission" 
		menu_stage_selector[1].sTxtLabel = "stealth cia guard"
		menu_stage_selector[2].sTxtLabel = "start of shootout" 
		menu_stage_selector[3].sTxtLabel = "half way through shootout"
		menu_stage_selector[4].sTxtLabel = "escape cutscene - FBI_1_MCS_2" 
		menu_stage_selector[5].sTxtLabel = "outside coroners" 
		menu_stage_selector[6].sTxtLabel = "pass mocap" 
	#endif 

	stop_mission_fail_checks = false
	alarm_on = false
	activate_elevator_door_system = false
	dialogue_paused = false 
	dave_norton_phone_call_finished = false
	break_the_glass_window = false
	operating_table_fail = false
	michael_clothes_set = false
	rumble_pad = false
	pistol_flash_light_equiped = false
	intro_mocap_shake_pad = false
	zip_sound_activated = false
	shootout_assets_removed = false
	taxi_drop_off_set_for_pass_mocap = false
	bypass_find_dead_bodies_stage = false
	stop_doctor_2_speaking = false
	
	intro_mocap_status = 0
	fbi1_stealth_cutscene_status = 0
	fbi1_master_controller_status = 0
	fbi1_escape_cutscene_status = 0
	current_line = -1
	phone_call_system_status = 0
	get_to_hills_status = 0
	dialogue_status = 0
	franklin_ai_system_status = 0
	inspect_body_0_status = 0
	inspect_body_1_status = 0
	phone_hang_up_count = 0
	music_event_system_status = 0
	cia_stealth_enemy_ai_system_status = 0
	cia_stealth_dialogue_system_status = 0
	special_ability_help_text_system_status = 0
	help_time = 0
	//fail_time = 0
	attack_time = 0
	interactive_cutscene_cam_status = 0
	phone_call_system_2_status = 0
	phone_call_time = 0
	delete_wave_one_cia_enemies_and_doctors_system_status = 0
	fail_safe_time = 0
	break_entity_glass_system_status = 0
	replay_video_recorder_system_status = 0
	
	zip_inc = 0.0
	zip_y = 0.0
	
	dialogue_root = ""
	specific_label = ""
	
	#IF IS_DEBUG_BUILD
	#endif 
	
	
	CLEAR_TRIGGERED_LABELS()
	
	zip_sound = get_sound_id()
	lift_sound = get_sound_id()
//	for i = 0 to count_of(alarm_sound) - 1
//		alarm_sound[i] = get_sound_id()
//	endfor 
	
	clip_board.model = P_CS_Clipboard 
	
	pen.model = Prop_Pencil_01 
	
	bone_saw.model = Prop_Bonesaw
	
	bin_bag[0].model = Prop_LD_rub_binbag_01 
	bin_bag[0].pos = <<274.700, -1375.760, 23.810>>
	bin_bag[0].heading = 0.0
	
	bin_bag[1].model = Prop_LD_rub_binbag_01 
	bin_bag[1].pos = <<249.200, -1375.4077, 38.81>>
	bin_bag[1].heading = 0.0
	
	bin[0].model = Prop_Bin_14b
	bin[0].pos = <<233.890, -1355.930, 29.560>>
	bin[0].rot = <<0.0, 0.0, 318.070>>

	//right
	doctor[0].model = S_M_Y_Autopsy_01
	doctor[0].pos = <<278.6999, -1337.5455, 23.2610>> 
	doctor[0].heading = 328.00
	doctor[0].weapon = weapontype_unarmed
	doctor[0].health = 200
	doctor[0].name = "doctor 0"

	doctor[1].model = S_M_Y_Autopsy_01
	doctor[1].pos = <<280.0447, -1336.3368, 23.2419>> 
	doctor[1].heading = 148.5733 
	doctor[1].weapon = weapontype_unarmed
	doctor[1].health = 200
	doctor[1].name = "doctor 1"
	
	doctor[2].model = S_M_Y_Autopsy_01
	doctor[2].pos = <<265.6501, -1360.3425, 23.2419>> 
	doctor[2].heading = 5.1035
	doctor[2].weapon = weapontype_unarmed
	doctor[2].health = 150
	doctor[2].name = "doctor 2"
	

	doctor[3].model = S_M_Y_Autopsy_01
	doctor[3].pos = <<261.4272, -1354.8154, 23.2419>> 
	doctor[3].heading = 135.6336
	doctor[3].run_to_pos = <<253.3714, -1373.1748, 23.5377>> 
	doctor[3].weapon = weapontype_unarmed
	doctor[3].health = 150
	doctor[3].name = "doctor 3"
	
	
	doctor[4].model = S_M_Y_Autopsy_01
	doctor[4].pos = <<265.6501, -1360.3425, 23.2419>> 
	doctor[4].heading = 5.1035
	doctor[4].run_to_pos = <<265.6501, -1360.3425, 23.2419>>
	doctor[4].weapon = weapontype_unarmed
	doctor[4].health = 150
	doctor[4].name = "doctor 4"
	
	
	dead_body[0].pos = <<292.205, -1345.025, 24.270>>
	dead_body[0].heading = 142.000
	dead_body[0].health = 200
	dead_body[0].model = u_f_y_corpse_01
	dead_body[0].weapon = weapontype_unarmed
	
	dead_body[1].pos = <<297.302, -1348.671, 24.270>>
	dead_body[1].heading = 140.000
	dead_body[1].health = 200
	dead_body[1].model = U_F_M_Corpse_01
	dead_body[1].weapon = weapontype_unarmed
	
	cia_stealth.pos = <<284.5221, -1343.0671, 23.5380>>
	cia_stealth.heading = 231.3517
	cia_stealth.weapon = weapontype_combatpistol
	cia_stealth.model = s_m_m_ciasec_01
	cia_stealth.health = 200
	cia_stealth.accuracy = 5
	cia_stealth.name = "cia enemy 0"

	for i = 0 to count_of(cia_enemy) - 1
		cia_enemy[i].created = false 
		cia_enemy_ai_status[i] = attack_phase_1
	endfor 
	
	for i = 0 to count_of(cia_enemy_top_floor) - 1
		cia_enemy_top_floor[i].created = false
		cia_enemy_top_floor_ai_status[i] = attack_phase_1
	endfor 
	
	for i = 0 to count_of(cia_enemy_outside_ai_status) - 1
		cia_enemy_outside[i].created = false
		cia_enemy_outside_ai_status[i] = attack_phase_1
	endfor 
	
	for i = 0 to count_of(doctor) - 1
		doctor_ai_status[i] = action_1
	endfor 
	
	cia_enemy[0].model = s_m_m_ciasec_01
	cia_enemy[0].weapon = weapontype_combatpistol
	cia_enemy[0].pos = <<269.8, -1344.5, 23.5380>>//<<269.3957, -1344.9933, 23.5380>>
	cia_enemy[0].run_to_pos = <<272.15137, -1343.91382, 23.53772>>
	cia_enemy[0].heading = 314.0738//324.5547 
	cia_enemy[0].health = 200
	cia_enemy[0].accuracy = 5
	cia_enemy[0].name = "cia enemy 0"
	
	cia_enemy[1].model = s_m_m_ciasec_01 
	cia_enemy[1].weapon = weapontype_combatpistol 
	cia_enemy[1].pos = <<264.3177, -1339.1743, 23.5380>>//<<264.8243, -1338.9841, 23.2419>> 
	cia_enemy[1].heading = 223.5415//230.0393
	cia_enemy[1].run_to_pos = <<271.2230, -1342.7653, 23.5380>>//<<266.8404, -1351.8467, 23.2419>>
	cia_enemy[1].health = 150
	cia_enemy[1].accuracy = 5
	cia_enemy[1].name = "cia enemy 1"
	
	cia_enemy[2].model = s_m_m_ciasec_01 
	cia_enemy[2].weapon = weapontype_combatpistol
	cia_enemy[2].pos = <<265.7096, -1354.0114, 23.2419>>
	cia_enemy[2].heading = 316.8201
	cia_enemy[2].run_to_pos = <<260.8433, -1353.2159, 23.2419>>
	cia_enemy[2].health = 200
	cia_enemy[2].accuracy = 5
	cia_enemy[2].name = "cia enemy 2"
	
	cia_enemy[3].model = s_m_m_ciasec_01 
	cia_enemy[3].weapon = weapontype_pumpshotgun
	cia_enemy[3].pos = <<265.5608, -1354.8715, 23.5380>>
	cia_enemy[3].heading = 349.8424
	cia_enemy[3].run_to_pos = <<269.4462, -1345.0549, 23.5380>>
	cia_enemy[3].health = 200
	cia_enemy[3].accuracy = 5
	cia_enemy[3].name = "cia enemy 3"

	cia_enemy[4].model = s_m_m_ciasec_01 
	cia_enemy[4].weapon = weapontype_combatpistol
	cia_enemy[4].pos = <<263.1902, -1359.3120, 23.2419>>
	cia_enemy[4].heading = 351.5096
	cia_enemy[4].run_to_pos = <<263.1902, -1359.3120, 23.5377>>
	cia_enemy[4].health = 125
	cia_enemy[4].accuracy = 5
	cia_enemy[4].name = "cia enemy 4"
	
	cia_enemy[5].model = s_m_m_ciasec_01 
	cia_enemy[5].weapon = weapontype_combatpistol
	cia_enemy[5].pos = <<249.5796, -1365.9684, 23.2392>>
	cia_enemy[5].heading = 222.9028
	cia_enemy[5].run_to_pos = <<255.7347, -1363.1337, 23.2419>>
	cia_enemy[5].health = 200
	cia_enemy[5].accuracy = 5
	cia_enemy[5].name = "cia enemy 5"

	cia_enemy[6].model = s_m_m_ciasec_01 
	cia_enemy[6].weapon = weapontype_pumpshotgun
	cia_enemy[6].pos = <<253.0823, -1356.3030, 23.2419>>
	cia_enemy[6].heading = 182.8046
	cia_enemy[6].run_to_pos = <<258.29, -1361.33, 23.24>>
	cia_enemy[6].health = 200
	cia_enemy[6].accuracy = 5
	cia_enemy[6].name = "cia enemy 6"
	
	cia_enemy[7].model = s_m_m_ciasec_01
	cia_enemy[7].weapon = weapontype_combatpistol
	cia_enemy[7].pos = <<261.5150, -1348.8262, 23.2419>>//<<258.7434, -1351.2220, 23.2419>> 
	cia_enemy[7].heading = 34.0855 
	cia_enemy[7].run_to_pos = <<254.9045, -1344.0269, 23.2419>>
	cia_enemy[7].health = 200
	cia_enemy[7].accuracy = 5
	cia_enemy[7].name = "cia enemy 7"
	cia_enemy[7].created = true
	
	cia_enemy[8].model = s_m_m_ciasec_01
	cia_enemy[8].weapon = weapontype_combatpistol
	cia_enemy[8].pos = <<255.6730, -1354.5822, 23.2419>>
	cia_enemy[8].heading = 213.8065
	cia_enemy[8].run_to_pos = <<250.03, -1350.03, 24.25>>
	cia_enemy[8].health = 200
	cia_enemy[8].accuracy = 5
	cia_enemy[8].name = "cia enemy 8"

	cia_enemy[9].model = s_m_m_ciasec_01
	cia_enemy[9].weapon = weapontype_smg
	cia_enemy[9].pos = <<247.1438, -1371.6957, 23.2391>>
	cia_enemy[9].heading = 322.0567
	cia_enemy[9].run_to_pos = <<248.4301, -1366.5393, 23.5377>>//<<247.1438, -1371.6957, 23.2391>>
	cia_enemy[9].health = 200
	cia_enemy[9].accuracy = 5
	cia_enemy[9].name = "cia enemy 9"

	cia_enemy[10].model = s_m_m_ciasec_01
	cia_enemy[10].weapon = weapontype_combatpistol
	cia_enemy[10].pos = <<248.9279, -1366.2971, 23.5380>>
	cia_enemy[10].heading = 312.8393
	cia_enemy[10].run_to_pos = <<265.0469, -1350.6741, 23.5380>>
	cia_enemy[10].health = 200
	cia_enemy[10].accuracy = 5
	cia_enemy[10].name = "cia enemy 10" 
	
	cia_enemy[11].model = s_m_m_ciasec_01
	cia_enemy[11].weapon = weapontype_combatpistol
	cia_enemy[11].pos = <<244.5939, -1365.7068, 28.3555>>
	cia_enemy[11].heading = 143.0724
	cia_enemy[11].run_to_pos = <<241.3390, -1369.8662, 28.3555>>
	cia_enemy[11].health = 200
	cia_enemy[11].accuracy = 5
	cia_enemy[11].name = "cia enemy 11" 

	cia_enemy[12].model = s_m_m_ciasec_01
	cia_enemy[12].weapon = weapontype_combatpistol
	cia_enemy[12].pos = <<256.1583, -1357.6641, 36.0631>>
	cia_enemy[12].heading = 227.00
	cia_enemy[12].run_to_pos = <<251.7, -1362.8, 33.2999>>//<<253.4599, -1364.3124, 33.2999>>
	cia_enemy[12].health = 200
	cia_enemy[12].accuracy = 5
	cia_enemy[12].name = "cia enemy 12" 
	
	cia_enemy[13].model = s_m_m_ciasec_01
	cia_enemy[13].weapon = weapontype_combatpistol
	cia_enemy[13].pos = <<258.8216, -1361.4050, 31.1218>>//<<257.5641, -1363.1951, 30.2194>>//<<258.8216, -1361.4050, 31.1218>>
	cia_enemy[13].heading = 169.2
	cia_enemy[13].run_to_pos = <<250.1255, -1366.6589, 28.6482>>
	cia_enemy[13].health = 200
	cia_enemy[13].accuracy = 5
	cia_enemy[13].name = "cia enemy 13"
	
	cia_enemy[14].model = s_m_m_ciasec_01
	cia_enemy[14].weapon = weapontype_smg
	cia_enemy[14].pos = <<248.9279, -1366.2971, 23.5380>>
	cia_enemy[14].heading = 312.8393
	cia_enemy[14].run_to_pos = <<258.1613, -1360.3820, 23.5380>>
	cia_enemy[14].health = 200
	cia_enemy[14].accuracy = 5
	cia_enemy[14].name = "cia enemy 14"		
	
	cia_enemy[15].model = s_m_m_ciasec_01
	cia_enemy[15].weapon = weapontype_combatpistol
	cia_enemy[15].pos = <<266.83777, -1345.47253, 23.53782>>//<<267.8736, -1344.3663, 23.5378>>//<<276.8777, -1342.9612, 23.2416>>
	cia_enemy[15].heading = 315.5344 //229.9195
	cia_enemy[15].run_to_pos = <<284.2000, -1344.5238, 23.5344>>
	cia_enemy[15].health = 200
	cia_enemy[15].accuracy = 5
	cia_enemy[15].name = "cia enemy 15"	

	cia_enemy[16].model = s_m_m_ciasec_01
	cia_enemy[16].weapon = weapontype_combatpistol//WEAPONTYPE_PUMPSHOTGUN
	cia_enemy[16].pos = <<274.4696, -1341.7974, 23.2416>>
	cia_enemy[16].heading = 229.5128
	cia_enemy[16].run_to_pos = <<279.2521, -1344.3408, 23.5344>> 
	cia_enemy[16].health = 200
	cia_enemy[16].accuracy = 5
	cia_enemy[16].name = "cia enemy 16"	
	cia_enemy[16].created = true
	
	cia_enemy_top_floor[0].model = s_m_m_ciasec_01
	cia_enemy_top_floor[0].weapon = weapontype_combatpistol
	cia_enemy_top_floor[0].pos = <<242.2337, -1367.5201, 38.5343>>//<<241.9199, -1367.1750, 38.5343>>
	cia_enemy_top_floor[0].heading = 332.00
	cia_enemy_top_floor[0].run_to_pos = <<242.5351, -1374.2463, 38.5343>>
	cia_enemy_top_floor[0].health = 200
	cia_enemy_top_floor[0].accuracy = 5
	cia_enemy_top_floor[0].name = "top floor 0"
	
	cia_enemy_top_floor[1].model = s_m_m_ciasec_01
	cia_enemy_top_floor[1].weapon = weapontype_combatpistol
	cia_enemy_top_floor[1].pos = <<236.2051, -1369.0114, 38.5344>> 
	cia_enemy_top_floor[1].heading = 239.4541
	cia_enemy_top_floor[1].run_to_pos = <<242.2337, -1367.5201, 38.5343>>//<<237.8856, -1367.4769, 38.5342>>
	cia_enemy_top_floor[1].health = 200
	cia_enemy_top_floor[1].accuracy = 5
	cia_enemy_top_floor[1].name = "top floor 1"

	cia_enemy_top_floor[2].model = s_m_m_ciasec_01
	cia_enemy_top_floor[2].weapon = weapontype_smg
	cia_enemy_top_floor[2].pos = <<239.4661, -1379.3192, 38.5342>>
	cia_enemy_top_floor[2].heading = 11.6926
	cia_enemy_top_floor[2].run_to_pos = <<242.9099, -1375.5717, 38.5342>>
	cia_enemy_top_floor[2].health = 200
	cia_enemy_top_floor[2].accuracy = 5
	cia_enemy_top_floor[2].name = "top floor 2"
	
	//peds who run up stairs. 
	cia_enemy_top_floor[3].model = s_m_m_ciasec_01
	cia_enemy_top_floor[3].weapon = weapontype_combatpistol
	cia_enemy_top_floor[3].pos = <<254.7773, -1366.0381, 33.5921>>
	cia_enemy_top_floor[3].heading = 323.0041
	cia_enemy_top_floor[3].run_to_pos = <<246.0538, -1364.6686, 38.5342>>
	cia_enemy_top_floor[3].health = 200
	cia_enemy_top_floor[3].accuracy = 5
	cia_enemy_top_floor[3].name = "top floor 3"
	
	cia_enemy_top_floor[4].model = s_m_m_ciasec_01
	cia_enemy_top_floor[4].weapon = weapontype_combatpistol
	cia_enemy_top_floor[4].pos = <<252.0599, -1362.6416, 33.5921>>
	cia_enemy_top_floor[4].heading = 193.9636 
	cia_enemy_top_floor[4].run_to_pos = <<254.2647, -1371.1111, 38.5342>>
	cia_enemy_top_floor[4].health = 200
	cia_enemy_top_floor[4].accuracy = 5
	cia_enemy_top_floor[4].name = "top floor 4"
	
	cia_enemy_top_floor[5].model = s_m_m_ciasec_01
	cia_enemy_top_floor[5].weapon = weapontype_combatpistol
	cia_enemy_top_floor[5].pos = <<245.1608, -1371.3011, 38.2413>>
	cia_enemy_top_floor[5].heading = 252.7770 
	cia_enemy_top_floor[5].run_to_pos = <<245.2636, -1376.6038, 38.2673>>
	cia_enemy_top_floor[5].health = 200
	cia_enemy_top_floor[5].accuracy = 5
	cia_enemy_top_floor[5].name = "top floor 5"
	
	cia_enemy_top_floor[6].model = s_m_m_ciasec_01
	cia_enemy_top_floor[6].weapon = weapontype_combatpistol
	cia_enemy_top_floor[6].pos = <<238.59, -1368.96, 38.24>>
	cia_enemy_top_floor[6].heading = 50.00
	cia_enemy_top_floor[6].run_to_pos = <<238.59, -1368.96, 38.24>>
	cia_enemy_top_floor[6].health = 200
	cia_enemy_top_floor[6].accuracy = 5
	cia_enemy_top_floor[6].name = "top floor 6"

	
	cia_enemy_outside[0].model = s_m_m_ciasec_01
	cia_enemy_outside[0].weapon = weapontype_combatpistol
	cia_enemy_outside[0].pos = <<249.5792, -1342.7684, 30.8178>>
	cia_enemy_outside[0].heading = 114.22
	cia_enemy_outside[0].run_to_pos = <<231.9526, -1348.4601, 29.4711>>
	cia_enemy_outside[0].health = 200
	cia_enemy_outside[0].accuracy = 2
	cia_enemy_outside[0].name = "out 0"
	
	cia_enemy_outside[1].model = s_m_m_ciasec_01
	cia_enemy_outside[1].weapon = weapontype_combatpistol
	cia_enemy_outside[1].pos = <<228.5237, -1376.5032, 29.7040>>//<<225.8327, -1373.3615, 29.6405>>
	cia_enemy_outside[1].heading = 87.4471
	cia_enemy_outside[1].run_to_pos = <<225.1319, -1361.1047, 29.5114>>
	cia_enemy_outside[1].health = 200
	cia_enemy_outside[1].accuracy = 2
	cia_enemy_outside[1].name = "out 1"
	
	cia_enemy_outside[2].model = s_m_m_ciasec_01
	cia_enemy_outside[2].weapon = weapontype_combatpistol
	cia_enemy_outside[2].pos = <<230.6237, -1386.9448, 29.4619>>//<<213.9920, -1372.0195, 29.5875>>
	cia_enemy_outside[2].heading = 41.9494
	cia_enemy_outside[2].run_to_pos = <<222.6815, -1356.7540, 29.5875>>
	cia_enemy_outside[2].health = 200
	cia_enemy_outside[2].accuracy = 2
	cia_enemy_outside[2].name = "out 2"
	
	cia_enemy_outside[3].model = S_M_Y_Cop_01
	cia_enemy_outside[3].weapon = weapontype_pistol
	cia_enemy_outside[3].run_to_pos = <<222.4067, -1363.6194, 29.5217>>
	cia_enemy_outside[3].health = 200
	cia_enemy_outside[3].accuracy = 2
	cia_enemy_outside[3].name = "out 3 "
	
	cia_enemy_outside[4].model = S_M_Y_Cop_01
	cia_enemy_outside[4].weapon = weapontype_pistol
	cia_enemy_outside[4].run_to_pos = <<217.4619, -1361.3063, 29.5875>>
	cia_enemy_outside[4].health = 200
	cia_enemy_outside[4].accuracy = 2
	cia_enemy_outside[4].name = "out 4"
	
	
	
//	players_truck.model = biff
	
	franklin.pos = <<1600.4019, -1944.4766, 99.6178>>
	franklin.heading = 16.6913
	franklin.model = GET_PLAYER_PED_MODEL(char_franklin)
	
	
	
//	police_man[2].model = S_M_Y_Cop_01
//	police_man[2].weapon = weapontype_pistol
//	
//	police_man[3].model = S_M_Y_Cop_01
//	police_man[3].weapon = weapontype_pistol
	
	police_car[0].model = police3
	police_car[0].pos = <<230.1168, -1396.6647, 29.4900>>
	police_car[0].heading = 7.3
	police_car[0].recording_number = 001
	
	police_car[1].model = police3
	police_car[1].pos = <<120.5738, -1449.2554, 28.2778>>
	police_car[1].heading = 28.9034

	ambient_car[0].pos = <<223.1269, -1352.6667, 29.5775>>
	ambient_car[0].heading = 229.9205
	ambient_car[0].model = felon
	
	ambient_car[1].pos = <<225.6844, -1349.6250, 29.5556>>
	ambient_car[1].heading = 229.4848
	ambient_car[1].model = baller2
	
	franklins_car.pos = <<1604.959,-1944.817,100.693>>
	franklins_car.heading = 98.9533
	franklins_car.model = GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_BIKE) 

endproc 

proc load_text_and_dialogue()

	register_script_with_audio()

	add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("0"), player_ped_id(), "michael")
	
	request_additional_text("fbi1", mission_text_slot)
	request_additional_text("fbi1aud", mission_dialogue_text_slot)

	while not has_additional_text_loaded(mission_text_slot)
	or not has_additional_text_loaded(mission_dialogue_text_slot)
		wait(0)	
	endwhile

endproc 

proc trigger_alarm(bool force_trigger_alarm = false)
					
	if not alarm_on 
	
		if (dave_norton_phone_call_finished and lk_timer(alarm_time, 2000))
		or not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4)  
		or lk_timer(shootout_time, 40000)
		or force_trigger_alarm 
		
			if PREPARE_ALARM("FBI_01_MORGUE_ALARMS")
		
				START_ALARM("FBI_01_MORGUE_ALARMS", false)
		
	//			PLAY_SOUND_FROM_COORD(alarm_sound[0], "FBI_01_DMW_ALARM_E", <<285.07, -1346.1, 26.39>>) //BIOTECH_HEIST_ALARM_BELL
	//			PLAY_SOUND_FROM_COORD(alarm_sound[1], "FBI_01_DMW_ALARM_E", <<256.63, -1357.34, 26.39>>)
	//			PLAY_SOUND_FROM_COORD(alarm_sound[2], "FBI_01_DMW_ALARM_E", <<247.92, -1363.22, 31.5>>)
	//			PLAY_SOUND_FROM_COORD(alarm_sound[3], "FBI_01_DMW_ALARM_E", <<258.47, -1358.14, 41.36>>)
	//			PLAY_SOUND_FROM_COORD(alarm_sound[4], "FBI_01_DMW_ALARM_E", <<237.23, -1366.36, 41.34>>)

				alarm_time = get_game_timer()
				alarm_on = true 
				
				trigger_music_event("FBI1_ALARM")
				
			endif 

		endif 
	endif 
	
endproc

//proc setup_radio_station()
//				
//	SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
//	FREEZE_RADIO_STATION("RADIO_01_CLASS_ROCK")
//	SET_RADIO_AUTO_UNFREEZE(FALSE)
//	SET_RADIO_TRACK("RADIO_01_CLASS_ROCK", "FBI1_COPS_LOST_RADIO")
//	
//endproc 

func bool allow_doctor_4_to_run()

	//if (is_ped_in_specific_room(player_ped_id(), <<251.12, -1367.35, 25.00>>, "CorridorBottom") and is_sphere_visible(get_entity_coords(doctor[4].ped), 2.0))
	if is_entity_in_angled_area(player_ped_id(), <<244.300, -1368.440, 23.543>>, <<248.479, -1363.460, 26.743>>, 7.6)
	and ((get_entity_heading(player_ped_id()) > 4.49 and get_entity_heading(player_ped_id()) < 90.31) or get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(doctor[4].ped)) < 2.0)
	
		return true 
	
	endif 
	
	return false 

endfunc 

proc cia_stealth_dialogue_system()

	switch cia_stealth_dialogue_system_status 
	
		case 0
		
			if create_conversation(scripted_speech, "FBI1AUD", "fbi1_look", conv_priority_medium)
				cia_stealth_dialogue_system_status++
			endif 

		break 
		
		case 1
		
			if not is_any_text_being_displayed(locates_data)
				cia_stealth_dialogue_system_status++
			endif 
		
		break 
		
		case 2
		
			if lk_timer(original_time, 7000)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(cia_stealth.ped)) < 25.00
					if not is_any_text_being_displayed(locates_data)
						if create_conversation(scripted_speech, "FBI1AUD", "fbi1_look", conv_priority_medium)
							original_time = get_game_timer()
						endif 
					endif 
				endif 
			endif 
		
		break 
		
	endswitch 

endproc 

proc cia_stealth_enemy_ai_system()

	if not is_ped_injured(cia_stealth.ped)
	
		set_ped_reset_flag(cia_stealth.ped, PRF_ForceEnableFlashLightForAI, true)
	
		//if is_entity_in_angled_area(player_ped_id(), get_offset_from_entity_in_world_coords(cia_stealth.ped, <<-5.0, 0.5, -1.0>>), get_offset_from_entity_in_world_coords(cia_stealth.ped, <<5.0, 0.5, -1.0>>))
		if cia_stealth_enemy_ai_system_status < 22
		
			vector vec_cia
			vector vec_player
			
			vec_cia = get_offset_from_entity_in_world_coords(cia_stealth.ped, <<0.0, 0.5, 0.0>>) - get_entity_coords(cia_stealth.ped)
			vec_player = get_entity_coords(cia_stealth.ped) - get_entity_coords(player_ped_id())
			
//			printfloat(dot_product(vec_cia, vec_player))
//			printnl()


//			if (dot_product(vec_cia, vec_player) < 0.0 and CAN_PED_SEE_HATED_PED(cia_stealth.ped, player_ped_id()))
//				script_Assert("test 0")
//				printstring("cia test 0")
//				printnl()
//			endif 
//			
//			if has_ped_been_harmed(cia_stealth.ped, cia_stealth.health)
//				script_Assert("test 1")
//				printstring("cia test 1")
//				printnl()
//			endif 
//			
//			if is_entity_touching_entity(player_ped_id(), cia_stealth.ped)
//				script_Assert("test 2")
//				printstring("cia test 2")
//				printnl()
//			endif 
//			
//			if can_ped_hear_player(player_id(), cia_stealth.ped)
//				script_Assert("test 3")
//				printstring("cia test 3")
//				printnl()
//			endif 
//			
//			if is_entity_in_angled_area(player_ped_id(), <<283.228, -1348.540, 23.538>>, <<279.005, -1344.710, 26.538>>, 2.0)
//				script_Assert("test 4")
//				printstring("cia test 4")
//				printnl()
//			endif 
			
	
			
			if (dot_product(vec_cia, vec_player) < 0.0 and CAN_PED_SEE_HATED_PED(cia_stealth.ped, player_ped_id()))
			or has_ped_been_harmed(cia_stealth.ped, cia_stealth.health)
			or is_entity_touching_entity(player_ped_id(), cia_stealth.ped)
			or can_ped_hear_player(player_id(), cia_stealth.ped)
			or is_entity_in_angled_area(player_ped_id(), <<283.228, -1348.540, 23.538>>, <<279.005, -1344.710, 26.538>>, 2.0)
			
				set_blocking_of_non_temporary_events(cia_stealth.ped, false)
				task_combat_ped(cia_stealth.ped, player_ped_id())
				
				original_time = get_game_timer()
				
				cia_stealth_enemy_ai_system_status = 22
			
			endif 
			
		endif 
				
		switch cia_stealth_enemy_ai_system_status
		
			case 0
			
				open_sequence_task(seq)
					task_play_anim(null, "missfbi1ig_7", "idle_a", slow_blend_in)
					task_play_anim(null, "missfbi1ig_7", "idle_b")
					task_play_anim(null, "missfbi1ig_7", "idle_c", normal_blend_in, slow_blend_out)
				close_sequence_task(seq)
				task_perform_sequence(cia_stealth.ped, seq)
				clear_sequence_task(seq)
				
//				open_sequence_task(seq)
//					task_go_to_coord_while_aiming_at_coord(null, <<287.6915, -1346.0591, 23.5377>>, <<295.7, -1352.8, 25.2>>, pedmove_walk, false, 0.2, 0.2, true, ENAV_STOP_EXACTLY)
//					task_aim_gun_at_coord(null, <<285.2, -1351.1, 25.0>>, 1000)
//					task_go_to_coord_while_aiming_at_coord(null, <<286.7805, -1348.7582, 23.5377>>, <<285.4, -1352.0, 25.00>>, pedmove_walk, false, 0.1, 0.1, true, enav_stop_exactly)
//				close_sequence_task(seq)
//				task_perform_sequence(cia_stealth.ped, seq)
//				clear_sequence_task(seq)
				
				cia_stealth_enemy_ai_system_status++
			
			break 
			
			case 1
			
//				cia_stealth_system()
//				
//				func bool cia_stealth_system()
//				
//					one anim phase > 0.99 start idle_b 
//					
//				endfunc 
			
				if has_ped_task_finished_2(cia_stealth.ped)
				
					force_ped_motion_state(cia_stealth.ped, ms_aiming)
				
					open_sequence_task(seq)
						task_aim_gun_at_coord(null, <<296.9, -1352.5, 24.00>>, 4000, true)
						task_aim_gun_at_coord(null, <<296.3, -1348.3, 25.00>>, 4000)
						task_aim_gun_at_coord(null, <<296.9, -1352.5, 24.00>>, 4000)
						task_aim_gun_at_coord(null, <<296.3, -1348.3, 25.00>>, 4000)
					close_sequence_task(seq)
					task_perform_sequence(cia_stealth.ped, seq)
					clear_sequence_task(seq)
					
					cia_stealth_enemy_ai_system_status++
					
				endif 

			break 
			
			case 2

				if has_ped_task_finished_2(cia_stealth.ped)
						
					open_sequence_task(seq)
						task_go_to_coord_while_aiming_at_coord(null, <<288.09836, -1346.54749, 23.53782>>, <<285.4, -1352.0, 25.00>>, pedmove_walk, false, 0.1, 0.1, true, enav_no_stopping)
						task_go_to_coord_while_aiming_at_coord(null, <<286.7805, -1348.7582, 23.5377>>, <<285.4, -1352.0, 25.00>>, pedmove_walk, false, 0.1, 0.1, true, enav_stop_exactly)
						task_aim_gun_at_coord(null, <<284.2, -1351.1, 24.00>>, 1500)
						task_aim_gun_at_coord(null, <<284.1, -1351.8, 25.7>>, 2000)
						task_aim_gun_at_coord(null, <<286.9, -1352.0, 25.00>>, 2000)
						//TASK_FOLLOW_WAYPOINT_RECORDING(null, "fbi1_0", 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_START_TASK_INITIALLY_AIMING) 
						task_go_to_coord_while_aiming_at_coord(null, <<290.86984, -1348.44971, 23.53797>>, <<295.6, -1352.3, 25.0>>, pedmove_walk, false)
						task_aim_gun_at_coord(null, <<295.6, -1352.3, 25.0>>, 4000)
					close_sequence_task(seq)
					task_perform_sequence(cia_stealth.ped, seq)
					clear_sequence_task(seq)

					cia_stealth_enemy_ai_system_status++

				endif 
				
			break 
			
			case 3
	
				if not is_ped_facing_ped(cia_stealth.ped, player_ped_id(), 35)
				or not is_ped_at_coords(cia_stealth.ped, get_entity_coords(cia_stealth.ped), <<1.0, 1.0, 1.6>>)
				
					if has_ped_task_finished_2(cia_stealth.ped)
					
						open_sequence_task(seq)
							task_turn_ped_to_face_entity(null, player_ped_id())
							task_go_to_coord_while_aiming_at_coord(null, get_entity_coords(player_ped_id()), get_entity_coords(player_ped_id()), pedmove_walk, false, 0.1, 0.1, true, enav_stop_exactly)
						close_sequence_task(seq)
						task_perform_sequence(cia_stealth.ped, seq)
						clear_sequence_task(seq)
						
					endif 
				endif 
				
			break 
					
			
//				if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(cia_stealth.ped)
//					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(cia_stealth.ped, 1.0)
//					WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(cia_stealth.ped, <<295.6, -1352.3, 25.0>>, false) 
//				endif 
//
//				if is_entity_at_coord(cia_stealth.ped, <<291.8003, -1349.1173, 23.5377>>, <<1.0, 1.0, 2.0>>)
//				
//					open_sequence_task(seq)
//						task_aim_gun_at_coord(null, <<296.9, -1352.5, 24.00>>, 4000)
//						task_aim_gun_at_coord(null, <<296.3, -1348.3, 25.00>>, 4000)
//						set_sequence_to_repeat(seq, repeat_forever)
//					close_sequence_task(seq)
//					task_perform_sequence(cia_stealth.ped, seq)
//					clear_sequence_task(seq)
//				
//					cia_stealth_enemy_ai_system_status++
//						
//				endif 

//			break 
			
			case 22
			
				set_ped_reset_flag(cia_stealth.ped, PRF_PreventAllMeleeTakedowns, true)
			
				if lk_timer(original_time, 1500)
					STOP_PED_SPEAKING(cia_stealth.ped, false)
					cia_stealth_enemy_ai_system_status++
				endif 
			
			break 
			
			case 23
			
				set_ped_reset_flag(cia_stealth.ped, PRF_PreventAllMeleeTakedowns, true)
			
			break 
			
		endswitch 
		
		cia_stealth_dialogue_system()
		
	endif 

endproc 

//	// Set health pack placement flags
//	iPlacementFlags = 0
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES)) // You might not want this?
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
//	iPlacementFlagsNoSnap = iPlacementFlags
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))

proc create_pickups()

	fbi1_pickup[0] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<262.4710, -1357.8521, 24.8703>>, <<0,0,63>>, 0)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(fbi1_pickup[0], "Reception")
	
	fbi1_pickup[1] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<240.8327, -1359.4211, 38.6342>>, <<0,0,27>>, 0)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(fbi1_pickup[1], "topoff_right")
	
	fbi1_pickup[2] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<254.350, -1342.04, 24.610>>, <<0,0,50>>, 0)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(fbi1_pickup[2], "AutopsyLargeA")
	
	fbi1_pickup[3] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<248.130, -1355.140, 24.620>>, <<90,0,-40>>, 0)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(fbi1_pickup[3], "AutopsyLargeB")
	
	fbi1_pickup[4] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<241.8990, -1364.6920, 24.9978>>, <<0.0000, 0.0000, -39.4400>>, 0)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(fbi1_pickup[4], "CorridorBottom")
	
	fbi1_pickup[5] = CREATE_PICKUP_ROTATE(PICKUP_ARMOUR_STANDARD, <<247.6937, -1374.5460, 38.7342>>, <<0.0000, 0.0000, 20.00>>, 0)
	ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(fbi1_pickup[5], "topoff_mid")
	
ENDPROC

proc remove_pickups()

	int i = 0
	
	for i = 0 to count_of(fbi1_pickup) - 1
	
		IF DOES_PICKUP_EXIST(fbi1_pickup[i])
			REMOVE_PICKUP(fbi1_pickup[i])
		ENDIF
		
	endfor 
	
endproc 

proc load_normal_start_of_mission()

	if replay_active
		start_replay_setup(<<280.645, -1335.833, 24.500>>, -138.000, false)
	endif 
				
	request_model(doctor[0].model)
	request_model(doctor[2].model)
	request_model(dead_body[0].model)
	set_ped_model_is_suppressed(dead_body[0].model, true)
	request_model(dead_body[1].model)
	set_ped_model_is_suppressed(dead_body[1].model, true)
	request_model(cia_enemy[0].model)

	request_model(clip_board.model)
	request_model(pen.model)
	request_model(bone_saw.model)
	request_model(bin_bag[0].model)
	request_model(bin[0].model)
	request_model(get_weapontype_model(cia_stealth.weapon))
	request_weapon_asset(cia_stealth.weapon, enum_to_int(wrf_request_all_anims), WEAPON_COMPONENT_FLASH)
	
	request_anim_dict("missfbi1")//mini@cpr@char_b@cpr_def   missfbi1

	request_streamed_texture_dict("fbi1")
	
	PREPARE_ALARM("FBI_01_MORGUE_ALARMS")
	
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	request_ambient_audio_bank("FBI_01_DMW_Autopsy_1")
	load_stream("FIB1_MORGUE_AMBIENCE_master")
	
	prepare_music_event("FBI1_WAKE_UP")

	
	set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_DESTROYED) 
	
	coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner")

	while not is_valid_interior(coroner_interior)
		coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner") 
		wait(0)
	endwhile 
	
	pin_interior_in_memory(coroner_interior)
	
	REQUEST_PTFX_ASSET()
	
//	assisted_movement_request_route("coroner1")
	assisted_movement_request_route("coroner2")
	assisted_movement_request_route("coroner_stairs")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(doctor[0].model)
	or not has_model_loaded(doctor[2].model)
	or not has_model_loaded(dead_body[0].model)
	or not has_model_loaded(dead_body[1].model)
	or not has_model_loaded(cia_enemy[0].model)
	or not has_model_loaded(clip_board.model)
	or not has_model_loaded(pen.model)
	or not has_model_loaded(bone_saw.model)
	or not has_model_loaded(bin_bag[0].model)
	or not has_model_loaded(bin[0].model)
	or not has_model_loaded(get_weapontype_model(cia_stealth.weapon))
	or not has_weapon_asset_loaded(cia_stealth.weapon)
	or not has_anim_dict_loaded("missfbi1")
	or not has_streamed_texture_dict_loaded("fbi1")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	or not request_ambient_audio_bank("script\\FBI_01_DMW_Autopsy_1")
	or not load_stream("FIB1_MORGUE_AMBIENCE_master")
	or not has_ptfx_asset_loaded()
	or not prepare_music_event("FBI1_WAKE_UP")
	or not is_interior_ready(coroner_interior)
	or not has_PTFX_ASSET_loaded()

		wait(0)
		
		draw_rect(0.5, 0.5, 1, 1, 0, 0, 0, 255)
		
	endwhile
	
	disable_cellphone(true)

	add_scenario_blocking_area(<<278.00, -1473.12, 100.00>>, <<222.50, -1243.38, -100.00>>)
	
	clear_area(<<287.9576, -1345.7250, 23.2416>>, 300.00, true)
	remove_decals_in_range(<<287.9576, -1345.7250, 23.2416>>, 300.00)
	
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	
	create_pickups()
	
	PLAY_STREAM_FRONTEND()
	
	//PLAY_STREAM_FRONTEND()

	end_cutscene_no_fade() 
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Start of mission")
	
	mission_flow = stealth_cutscene

	//***************debug jumps
	//mission_flow = escape_cutscene
	//***************end jumps 

endproc 

proc load_trip_skip_data_0()

	//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, TRUE)
	remove_all_ped_weapons(player_ped_id())
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, true)

	load_normal_start_of_mission() //can be achieved because the script fades out after the intro mocap.

endproc 

//PURPOSE: Player fails between getting start of stealth guard and before killing guard = start of stealth guard
proc load_trip_skip_data_1()

	int i
	
	if replay_active
		start_replay_setup(<<279.0734, -1337.0759, 23.5545>>, 227.22, false)
	endif 

	request_model(doctor[0].model)
	request_model(dead_body[0].model)
	set_ped_model_is_suppressed(dead_body[0].model, true)
	request_model(dead_body[1].model)
	set_ped_model_is_suppressed(dead_body[1].model, true)
	request_model(cia_enemy[0].model)
	
	request_model(bin_bag[0].model)
	
	request_model(bin[0].model)
	
	request_model(get_weapontype_model(weapontype_combatpistol))
	request_weapon_asset(WEAPONTYPE_combatpistol)
	
	request_anim_dict("missfbi1")
	request_anim_dict("missfbi1ig_7")
	request_anim_dict("dead")
			
	REQUEST_STEALTH_MODE_ASSET("MICHAEL_ACTION")
	
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
	
	REQUEST_PTFX_ASSET()
	
	set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_DESTROYED) 
	
	coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner")

	while not is_valid_interior(coroner_interior)
		coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner") 
		wait(0)
	endwhile 
	
	pin_interior_in_memory(coroner_interior)
	
	assisted_movement_request_route("coroner2")
	assisted_movement_request_route("coroner_stairs")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	

	while not has_model_loaded(doctor[0].model)
	or not has_model_loaded(dead_body[0].model)
	or not has_model_loaded(dead_body[1].model)
	or not has_model_loaded(cia_enemy[0].model)
	or not has_model_loaded(bin_bag[0].model)
	or not has_model_loaded(bin[0].model)
	or not has_model_loaded(get_weapontype_model(weapontype_combatpistol))
	or not has_weapon_asset_loaded(weapontype_combatpistol)
	or not has_anim_dict_loaded("missfbi1")
	or not has_anim_dict_loaded("missfbi1ig_7")
	or not has_anim_dict_loaded("dead")
	or not HAS_stealth_MODE_ASSET_LOADED("michael_action")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	or not has_ped_preload_variation_data_finished(player_ped_id())
	or not has_ptfx_asset_loaded()
	or not is_interior_ready(coroner_interior)

		wait(0)
		
	endwhile 

	add_scenario_blocking_area(<<278.00, -1473.12, 100.00>>, <<222.50, -1243.38, -100.00>>)

	clear_area(<<278.58, -1336.61, 23.68>>, 1000, true)
	remove_decals_in_range(<<278.58, -1336.61, 23.68>>, 1000.00)
	
	create_pickups()
	
	//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, TRUE)
	remove_all_ped_weapons(player_ped_id())
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, true)
	
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
	//clear_all_ped_props(PLAYER_PED_ID())
	set_ped_comp_item_current_sp(player_ped_id(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, false)


	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<279.0734, -1337.0759, 23.5545>>)
	set_entity_heading(player_ped_id(), 227.5366) 
	SET_PED_STEALTH_MOVEMENT(player_ped_id(), true)
	force_ped_motion_state(player_ped_id(), MS_STEALTHMODE_IDLE) 
	force_ped_ai_and_animation_update(player_ped_id())
	add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
	FORCE_ROOM_FOR_ENTITY(player_ped_id(), coroner_interior, get_hash_key("AutopsySmall")) 
	FORCE_ROOM_FOR_GAME_VIEWPORT(coroner_interior, get_hash_key("AutopsySmall"))
	
	setup_enemy(cia_stealth)
	cia_stealth.blip = create_blip_for_ped(cia_stealth.ped, true)
	GIVE_WEAPON_COMPONENT_TO_PED(cia_stealth.ped, weapontype_combatpistol, WEAPONCOMPONENT_AT_PI_FLSH)
	set_current_ped_weapon(cia_stealth.ped, weapontype_combatpistol, true)
	STOP_PED_SPEAKING(cia_stealth.ped, true)
	add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("6"), cia_stealth.ped, "CIA1") 
	fORCE_PED_MOTION_STATE(cia_stealth.ped, MS_aiming, TRUE)
	cia_stealth_enemy_ai_system()
	force_ped_ai_and_animation_update(cia_stealth.ped)
	
	for i = 0 to count_of(bin_bag) - 1
					
		bin_bag[i].obj = create_object_no_offset(bin_bag[i].model, bin_bag[i].pos)
		set_entity_heading(bin_bag[i].obj, bin_bag[i].heading)
		
		if i = 0
			FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<277.2329, -1336.3568, 24.00>>, "V_coroner"), get_hash_key("AutopsySmall"))
		elif i = 1
			FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<249.00, -1372.3, 39.5>>, "V_coroner"), get_hash_key("topoff_mid"))
		endif 
		
	endfor 
	
	bin[0].obj = create_object(bin[0].model, bin[0].pos)
	set_entity_rotation(bin[0].obj, bin[0].rot)
	freeze_entity_position(bin[0].obj, true)
	
	setup_buddy(dead_body[0])
	remove_blip(dead_body[0].blip)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TORSO, 2, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_JBIB, 0, 0)
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[0].ped)
	set_ped_can_ragdoll(dead_body[0].ped, true)
	RETAIN_ENTITY_IN_INTERIOR(dead_body[0].ped, get_interior_at_coords_with_type(<<292.205, -1345.025, 24.570>>, "V_Coroner"))
	set_entity_load_collision_flag(dead_body[0].ped, true)

	cutscene_pos = <<291.552, -1345.871, 24.57>>//24.62 
	cutscene_rot = <<0.000, 0.000, 147.000>>
				
	task_play_anim_advanced(dead_body[0].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(dead_body[0].ped)
				

	setup_buddy(dead_body[1])
	remove_blip(dead_body[1].blip)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_JBIB, 0, 0)
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[1].ped)
	set_ped_can_ragdoll(dead_body[1].ped, true)
	RETAIN_ENTITY_IN_INTERIOR(dead_body[1].ped, get_interior_at_coords_with_type(<<297.302, -1348.671, 24.570>>, "V_Coroner"))
	set_entity_load_collision_flag(dead_body[1].ped, true)
	
	cutscene_pos = <<296.702, -1349.621, 24.570>> 
	cutscene_rot = <<0.000, 0.000, 140.000>>

	task_play_anim_advanced(dead_body[1].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(dead_body[1].ped)	
	
	setup_enemy(doctor[1])
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(doctor[1].ped)
	DISABLE_PED_PAIN_AUDIO(doctor[1].ped, true)  
	STOP_PED_SPEAKING(doctor[1].ped, true)
	SET_PED_COMPONENT_VARIATION(doctor[1].ped, PED_COMP_HEAD, 1, 0)
	SET_PED_COMPONENT_VARIATION(doctor[1].ped, PED_COMP_TORSO, 0, 1)
	remove_blip(doctor[1].blip)
	SET_PED_CAN_RAGDOLL(doctor[1].ped, TRUE)
	
	//reception ped doctor.
	setup_enemy(doctor[4])
	remove_blip(doctor[4].blip)
	set_ped_can_be_targetted(doctor[4].ped, false)
					
	task_play_anim_advanced(doctor[1].ped, "Dead", "dead_c", <<278.835, -1336.046, 24.580>>, <<0.000, 0.000, 74.000>>, instant_blend_in, normal_blend_out, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(doctor[1].ped)
	
	if not replay_active
	
		NEW_LOAD_SCENE_START(<<279.0734, -1337.0759, 23.5545>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 227.5366>>), 8.0)

		while not IS_NEW_LOAD_SCENE_LOADED()	
			wait(0)
		endwhile 
		
		NEW_LOAD_SCENE_STOP()
		
	endif 
	
	trigger_music_event("FBI1_WAKE_UP_RT")
	
	start_audio_scene("FBI_1_STEALTH_KILL_AGENT")
	
	end_cutscene(false, 0, 0, false)
	
	mission_flow = stealth_the_cia_officer
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "stealth the cia guard")

endproc 

proc load_trip_skip_data_2()

	int i
	
	if replay_active
		start_replay_setup(<<294.4570, -1351.7913, 23.5344>>, 58.34, false)
	endif

	request_model(doctor[0].model)
	request_model(doctor[2].model)
	request_model(dead_body[0].model)
	set_ped_model_is_suppressed(dead_body[0].model, true)
	request_model(dead_body[1].model)
	set_ped_model_is_suppressed(dead_body[1].model, true)
	request_model(cia_enemy[0].model)
	request_model(bin[0].model)
	request_model(bin_bag[0].model)
	
	request_anim_dict("missfbi1")//mini@cpr@char_b@cpr_def   missfbi1
	request_anim_dict("dead")

	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	
	REQUEST_PTFX_ASSET()
	
	set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_DESTROYED) 
	
	coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner")

	while not is_valid_interior(coroner_interior)
		coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner") 
		wait(0)
	endwhile 
	
	pin_interior_in_memory(coroner_interior)
	
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
	
//	assisted_movement_request_route("coroner1")
	assisted_movement_request_route("coroner2")
	assisted_movement_request_route("coroner_stairs")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(doctor[0].model)
	or not has_model_loaded(doctor[2].model)
	or not has_model_loaded(dead_body[0].model)
	or not has_model_loaded(dead_body[1].model)
	or not has_model_loaded(cia_enemy[0].model)
	or not has_model_loaded(bin[0].model)
	or not has_model_loaded(bin_bag[0].model)
	or not has_anim_dict_loaded("missfbi1")
	or not has_anim_dict_loaded("dead")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	or not has_ptfx_asset_loaded()
	or not is_interior_ready(coroner_interior)
	or not has_ped_preload_variation_data_finished(player_ped_id())

		wait(0)
		
		draw_rect(0.5, 0.5, 1, 1, 0, 0, 0, 255)
		
	endwhile
	
//	
//	//lift doors
//	set_state_of_closest_door_of_type(V_ILev_Cor_DoorLift01, <<248.9, -1371.0, 23.2>>, true, 0.0) 
//	set_state_of_closest_door_of_type(V_ILev_Cor_DoorLift02, <<247.6, -1369.9, 23.2>>, true, 0.0) 
//	
//	//SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_cor_doorglassa, <<285.58, -1345.97, 24.68>>, true, 0.0, 0.0, -1.0)
//	set_state_of_closest_door_of_type(v_ilev_cor_doorglassa, <<285.58, -1345.97, 24.68>>, true, -1.0)
//	set_state_of_closest_door_of_type(v_ilev_cor_doorglassa, <<285.58, -1345.97, 24.68>>, false, -1.0)

	add_scenario_blocking_area(<<278.00, -1473.12, 100.00>>, <<222.50, -1243.38, -100.00>>)
	
	clear_area(<<287.9576, -1345.7250, 23.2416>>, 300.00, true)
	remove_decals_in_range(<<287.9576, -1345.7250, 23.2416>>, 300.00)
	
	create_pickups()
	
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_visible(player_ped_id(), true)
	set_entity_coords(player_ped_id(), <<294.4570, -1351.7913, 23.5344>>)
	set_entity_heading(player_ped_id(), 58.3410)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
	//clear_all_ped_props(PLAYER_PED_ID())
	remove_player_ped_props(player_ped_id())
	add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
	FORCE_ROOM_FOR_ENTITY(player_ped_id(), coroner_interior, get_hash_key("BodyStorage")) 
	FORCE_ROOM_FOR_GAME_VIEWPORT(coroner_interior, get_hash_key("BodyStorage"))
	
	//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, TRUE)
	remove_all_ped_weapons(player_ped_id())

	if not has_ped_got_weapon(player_ped_id(), weapontype_combatpistol) 
		give_weapon_to_ped(player_ped_id(), weapontype_combatpistol, 50, true, true)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), weapontype_combatpistol) < 50
			set_ped_ammo(player_ped_id(), weapontype_combatpistol, 50)
		endif 
		set_current_ped_weapon(player_ped_id(), weapontype_combatpistol, true)
	endif 
	
	for i = 1 to count_of(bin_bag) - 1
					
		bin_bag[i].obj = create_object_no_offset(bin_bag[i].model, bin_bag[i].pos)
		set_entity_heading(bin_bag[i].obj, bin_bag[i].heading)
		
		if i = 0
			FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<277.2329, -1336.3568, 24.00>>, "V_coroner"), get_hash_key("AutopsySmall"))
		elif i = 1
			FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<249.00, -1372.3, 39.5>>, "V_coroner"), get_hash_key("topoff_mid"))
		endif 
		
	endfor 
	
	bin[0].obj = create_object(bin[0].model, bin[0].pos)
	set_entity_rotation(bin[0].obj, bin[0].rot)
	freeze_entity_position(bin[0].obj, true)

	if not replay_active 
	
		NEW_LOAD_SCENE_START(<<294.4570, -1351.7913, 23.5344>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 58.34>>), 8.0)

		while not IS_NEW_LOAD_SCENE_LOADED()	
			wait(0)
		endwhile 
		
		NEW_LOAD_SCENE_STOP()
		
	endif 

	setup_buddy(dead_body[0])
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TORSO, 2, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_JBIB, 0, 0)
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[0].ped)
	set_ped_can_ragdoll(dead_body[0].ped, true)
	RETAIN_ENTITY_IN_INTERIOR(dead_body[0].ped, get_interior_at_coords_with_type(<<292.205, -1345.025, 24.570>>, "V_Coroner"))
	set_entity_load_collision_flag(dead_body[0].ped, true)

	cutscene_pos = <<291.552, -1345.871, 24.570>> 
	cutscene_rot = <<0.000, 0.000, 147.000>>
	
	task_play_anim_advanced(dead_body[0].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE |AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(dead_body[0].ped)

	setup_buddy(dead_body[1])
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_JBIB, 0, 0)
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[1].ped)
	set_ped_can_ragdoll(dead_body[1].ped, true)
	RETAIN_ENTITY_IN_INTERIOR(dead_body[1].ped, get_interior_at_coords_with_type(<<297.302, -1348.671, 24.570>>, "V_Coroner"))
	set_entity_load_collision_flag(dead_body[1].ped, true)
	
	cutscene_pos = <<296.702, -1349.621, 24.570>> 
	cutscene_rot = <<0.000, 0.000, 140.000>>
	
	task_play_anim_advanced(dead_body[1].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(dead_body[1].ped)
	
	
	setup_enemy(doctor[1])
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(doctor[1].ped)
	DISABLE_PED_PAIN_AUDIO(doctor[1].ped, true)  
	STOP_PED_SPEAKING(doctor[1].ped, true)
	SET_PED_COMPONENT_VARIATION(doctor[1].ped, PED_COMP_HEAD, 1, 0)
	SET_PED_COMPONENT_VARIATION(doctor[1].ped, PED_COMP_TORSO, 0, 1)
	remove_blip(doctor[1].blip)
	SET_PED_CAN_RAGDOLL(doctor[1].ped, TRUE)
					
	task_play_anim_advanced(doctor[1].ped, "Dead", "dead_c", <<278.835, -1336.046, 24.580>>, <<0.000, 0.000, 74.000>>, instant_blend_in, normal_blend_out, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(doctor[1].ped)

	
//	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
//		wait(0)
//	endwhile
	
	RELEASE_PED_PRELOAD_VARIATION_DATA(player_ped_id())
	
	
	phone_call_system_status = 3
	
	escape_blip = create_blip_for_coord(<<236.4304, -1357.7548, 38.5342>>)

	trigger_alarm(true)
	
	trigger_music_event("FBI1_SHOOTOUT_RT")
	
	start_audio_scene("FBI_1_ESCAPE_BASE_SCENE")

	end_cutscene(false, 0.0, 0.0, false)
	
//	clear_prints()
//	print_now("FBI1_God_7", default_god_text_time, 1)
//	escape_building_text_printed = true
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "Start of shootout")
	
	mission_flow = shootout

endproc 

proc load_trip_skip_data_3()

	int i = 0
	
	if replay_active
		start_replay_setup(<<248.6566, -1366.2362, 23.5377>>, 90.17, false)
	endif 

	request_model(doctor[0].model)
	
	request_model(dead_body[0].model)
	set_ped_model_is_suppressed(dead_body[0].model, true)
	request_model(dead_body[1].model)
	set_ped_model_is_suppressed(dead_body[1].model, true)
	request_model(cia_enemy[0].model)
	request_model(bin_bag[0].model)
	request_model(bin[0].model)
	
	request_anim_dict("missfbi1")//mini@cpr@char_b@cpr_def   missfbi1
	
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	
	REQUEST_PTFX_ASSET()
	
	set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_DESTROYED) 
	
	coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner")

	while not is_valid_interior(coroner_interior)
		coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner") 
		wait(0)
	endwhile 
	
	pin_interior_in_memory(coroner_interior)
	
//	assisted_movement_request_route("coroner1")
	assisted_movement_request_route("coroner2")
	assisted_movement_request_route("coroner_stairs")
	
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
	
	REQUEST_ACTION_MODE_ASSET("michael_ACTION")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(doctor[0].model)
	or not has_model_loaded(dead_body[0].model)
	or not has_model_loaded(dead_body[1].model)
	or not has_model_loaded(cia_enemy[0].model)
	or not has_model_loaded(bin[0].model)
	or not has_model_loaded(bin_bag[0].model)
	or not has_anim_dict_loaded("missfbi1")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_01")
	or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
	or not has_ptfx_asset_loaded()
	or not is_interior_ready(coroner_interior)
	or not has_ped_preload_variation_data_finished(player_ped_id())
	or not HAS_ACTION_MODE_ASSET_LOADED("michael_action")

		wait(0)
		
	endwhile
	
	//lift doors
//	set_state_of_closest_door_of_type(V_ILev_Cor_DoorLift01, <<248.9, -1371.0, 23.2>>, true, -1.0) 
//	set_state_of_closest_door_of_type(V_ILev_Cor_DoorLift02, <<247.6, -1369.9, 23.2>>, true, 1.0) 
//	
//	set_state_of_closest_door_of_type(v_ilev_cor_firedoor, <<252.87, -1366.76, 29.67>>, true, 1.0)

	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[8], -1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[8], DOORSTATE_force_locked_this_frame, false, true)
	
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[9], 1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[9], DOORSTATE_force_locked_this_frame, false, true)

	add_scenario_blocking_area(<<278.00, -1473.12, 100.00>>, <<222.50, -1243.38, -100.00>>)//door enemy 13 runs through down stairs
	
	clear_area(<<287.9576, -1345.7250, 23.2416>>, 300.00, true)
	remove_decals_in_range(<<287.9576, -1345.7250, 23.2416>>, 300.00)
	
	create_pickups()
	
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_visible(player_ped_id(), true)
	set_entity_coords(player_ped_id(), <<248.6566, -1366.2362, 23.5377>>)
	set_entity_heading(player_ped_id(), 90.1748)
	//setup_weapon_for_ped(player_ped_id(), weapontype_combatpistol, 50, true, true, false)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
	//clear_all_ped_props(PLAYER_PED_ID())
	remove_player_ped_props(player_ped_id())
		
	//This will restore the players weapons using the chached weapon data that was stored at the checkpoint. 
	remove_all_ped_weapons(player_ped_id())
	SET_PED_WEAPONS(player_ped_id(), fbi1_player_stored_weapons, true)
	
	setup_player_fail_weapon(player_ped_id(), weapontype_combatpistol, 50)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
	force_ped_motion_state(player_ped_id(), MS_ACTIONMODE_idle)
	force_ped_ai_and_animation_update(player_ped_id())
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
	add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael") 
	FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), coroner_interior, get_hash_key("CorridorBottom"))
	FORCE_ROOM_FOR_GAME_VIEWPORT(coroner_interior, get_hash_key("CorridorBottom"))

	setup_buddy(dead_body[0])
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TORSO, 2, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_JBIB, 0, 0)
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[0].ped)
	set_ped_can_ragdoll(dead_body[0].ped, true)
	RETAIN_ENTITY_IN_INTERIOR(dead_body[0].ped, get_interior_at_coords_with_type(<<292.205, -1345.025, 24.570>>, "V_Coroner"))
	set_entity_load_collision_flag(dead_body[0].ped, true)

	cutscene_pos = <<291.552, -1345.871, 24.570>> 
	cutscene_rot = <<0.000, 0.000, 147.000>>
	
	task_play_anim_advanced(dead_body[0].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(dead_body[0].ped)

	setup_buddy(dead_body[1])
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_JBIB, 0, 0)
	DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[1].ped)
	set_ped_can_ragdoll(dead_body[1].ped, true)
	RETAIN_ENTITY_IN_INTERIOR(dead_body[1].ped, get_interior_at_coords_with_type(<<297.302, -1348.671, 24.570>>, "V_Coroner"))
	set_entity_load_collision_flag(dead_body[1].ped, true)
	
	cutscene_pos = <<296.702, -1349.621, 24.570>> 
	cutscene_rot = <<0.000, 0.000, 140.000>>
	task_play_anim_advanced(dead_body[1].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
	force_ped_ai_and_animation_update(dead_body[1].ped)

	setup_enemy(doctor[3])
	remove_blip(doctor[3].blip)
	set_ped_can_be_targetted(doctor[3].ped, false)
	add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("D"), doctor[3].ped, "doctor1")
	task_cower(doctor[3].ped, -1)
	doctor_ai_status[3] = action_1_status 

	for i = 1 to count_of(bin_bag) - 1
					
		bin_bag[i].obj = create_object_no_offset(bin_bag[i].model, bin_bag[i].pos)
		set_entity_heading(bin_bag[i].obj, bin_bag[i].heading)
		
		if i = 0
			FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<277.2329, -1336.3568, 24.00>>, "V_coroner"), get_hash_key("AutopsySmall"))
		elif i = 1
			FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<249.00, -1372.3, 39.5>>, "V_coroner"), get_hash_key("topoff_mid"))
		endif 
		
	endfor 

	bin[0].obj = create_object(bin[0].model, bin[0].pos)
	set_entity_rotation(bin[0].obj, bin[0].rot)
	freeze_entity_position(bin[0].obj, true)
	
	wait(0)
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
		wait(0)
	endwhile
	

	escape_blip = create_blip_for_coord(<<236.4304, -1357.7548, 38.5342>>)

	special_ability_help_text_system_status = 3 //deactivates help text
	
	fbi1_master_controller_status = 2

	phone_call_system_status = 3
	
	music_event_system_status = 2

	trigger_alarm(true)
	
	trigger_music_event("FBI1_SHOOTOUT_HALFWAY_RT")
	
	start_audio_scene("FBI_1_ESCAPE_BASE_SCENE")

	end_cutscene(false, 0.0, 0.0, false)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "half way through shootout")
	
	mission_flow = shootout

endproc 

proc load_trip_skip_data_4()

	int i = 0

	if replay_active
		start_replay_setup(<<235.5931, -1354.1631, 29.5508>>, 34.39)
	endif
	
	request_model(franklin.model)
	request_model(cia_enemy_outside[1].model)
	request_model(cia_enemy_outside[3].model)
	
	request_model(ambient_car[0].model)
	request_model(ambient_car[1].model)
	request_model(police_car[0].model)
	request_model(franklins_car.model)
	set_vehicle_model_is_suppressed(franklins_car.model, true)
	
	request_model(bin[0].model)
	
	request_anim_dict("MISSFBI1LEADINOUTFBI_1_EXT")
	request_anim_dict("missfbi1")//mini@cpr@char_b@cpr_def   missfbi1
	
	request_vehicle_recording(police_car[0].recording_number, "lkfbi1")
	
	//request_ambient_audio_bank("SCRIPT\\Alarms")
	
	REQUEST_PTFX_ASSET()
	
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
	
	if replay_active
		END_REPLAY_SETUP()
	endif

	while not has_model_loaded(franklin.model)
	or not has_model_loaded(cia_enemy_outside[1].model)
	or not has_model_loaded(cia_enemy_outside[3].model)
	or not has_model_loaded(ambient_car[0].model)
	or not has_model_loaded(ambient_car[1].model)
	or not has_model_loaded(police_car[0].model)
	or not has_model_loaded(franklins_car.model)
	or not has_model_loaded(bin[0].model)
	or not has_anim_dict_loaded("MISSFBI1LEADINOUTFBI_1_EXT")
	or not has_anim_dict_loaded("missfbi1")
	or not has_vehicle_recording_been_loaded(police_car[0].recording_number, "lkfbi1")
	or not has_ptfx_asset_loaded()
	or not has_ped_preload_variation_data_finished(player_ped_id())
	
		wait(0)
		
	endwhile 
	
	clear_area(<<287.9576, -1345.7250, 23.2416>>, 300.00, true)
	
	DISTANT_COP_CAR_SIRENS(true)
	
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<235.5931, -1354.1631, 29.5508>>)
	set_entity_heading(player_ped_id(), 34.3910)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
	//clear_all_ped_props(PLAYER_PED_ID())
	remove_player_ped_props(player_ped_id())
	
	remove_all_ped_weapons(player_ped_id())
	SET_PED_WEAPONS(player_ped_id(), fbi1_player_stored_weapons, true)
	
	add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael") 
	
	bin[0].obj = create_object(bin[0].model, bin[0].pos)
	set_entity_rotation(bin[0].obj, bin[0].rot)
	freeze_entity_position(bin[0].obj, true)
	
	for i = 0 to count_of(ambient_car) - 1
		ambient_car[i].veh = create_vehicle(ambient_car[i].model, ambient_car[i].pos, ambient_car[i].heading)
		set_vehicle_as_no_longer_needed(ambient_car[i].veh)
		set_model_as_no_longer_needed(ambient_car[i].model)
	endfor 
	
	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	set_max_wanted_level(3)
	set_create_random_cops(false)
	set_wanted_level_multiplier(0.5)
	
	set_player_wanted_level(player_id(), 3)
	set_player_wanted_level_now(player_id())
	sET_WANTED_LEVEL_DIFFICULTY(player_id(), 0)
	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI1_COP_LOSS_TIME) 
	
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
		wait(0)
	endwhile
	
	//setup_radio_station()
	
	special_ability_help_text_system_status = 3 //deactivates help text
	
	end_cutscene()
	
	trigger_music_event("FBI1_OUTSIDE_CORONERS_RT")
	
	start_audio_scene("FBI_1_LOSE_COPS")
	
	print_now("LOSE_WANTED", default_god_text_time, 1)

	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "outside coroners escape police", true)
	
	mission_flow = get_to_hills

	SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)
endproc	

proc load_trip_skip_data_5()

	if replay_active
		start_replay_setup(<<1612.0714, -1943.6034, 100.9448>>, 153.72)
	endif 
				
	request_model(get_player_ped_model(char_michael))
	request_model(ambient_car[0].model)
	request_model(franklins_car.model)
	set_vehicle_model_is_suppressed(franklins_car.model, true)
	
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
	
	if replay_active
		END_REPLAY_SETUP()
	endif
	
	while not has_model_loaded(get_player_ped_model(char_michael))
	or not has_model_loaded(ambient_car[0].model)
	or not has_model_loaded(franklins_car.model)
	or not has_ped_preload_variation_data_finished(player_ped_id())
	
		wait(0)
		
	endwhile 
	
	clear_area(<<1612.0714, -1943.6034, 100.9448>>, 1000, true)

	set_entity_coords(player_ped_id(), <<1612.0714, -1943.6034, 100.9448>>) 
	set_entity_heading(player_ped_id(), 153.7002)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
	//clear_all_ped_props(PLAYER_PED_ID())
	remove_player_ped_props(player_ped_id())
	add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael") 
	
	ambient_car[0].veh = create_vehicle(ambient_car[0].model, <<1616.2025, -1933.7650, 100.7450>>, 65.3292)
	set_vehicle_on_ground_properly(ambient_car[0].veh)
	
	CREATE_PLAYER_VEHICLE(franklins_car.veh, CHAR_FRANKLIN, franklins_car.pos, franklins_car.heading, TRUE, VEHICLE_TYPE_BIKE)


	request_cutscene("FBI_1_EXT")
	
	while not has_cutscene_loaded()
	
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", PLAYER_PED_ID())
	
		wait(0)
		
	endwhile 
	

	if not replay_active
	
		NEW_LOAD_SCENE_START(<<1612.0714, -1943.6034, 100.9448>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 153.70>>), 100.0)
		
		while not IS_NEW_LOAD_SCENE_LOADED()
			wait(0)
		endwhile 
		
		NEW_LOAD_SCENE_STOP()
		
	endif 
	
	
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
	
	start_cutscene()
	

	while not is_cutscene_playing()
	
		wait(0)
		
	endwhile 
	
	
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1629.403, -1946.552, 91.196>>, <<1571.303, -1946.552, 107.096>>, 62.100, <<1616.2025, -1933.7650, 100.7450>>, 65.3292, <<15.0, 15.0, 15.0>>)//, false)
	set_players_last_vehicle_to_vehicle_gen(<<1616.2025, -1933.7650, 100.7450>>, 65.3292, true)

	SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(2.0)
	
	original_time = get_game_timer() 

	get_to_hills_status = 5

	mission_flow = get_to_hills
	
	do_screen_fade_in(default_fade_time)

endproc 


proc remove_all_blips()

	int i = 0

	for i = 0 to count_of(bin_bag) - 1
		if does_blip_exist(bin_bag[i].blip)
			remove_blip(bin_bag[i].blip)
		endif
	endfor 
	
	if does_blip_exist(escape_blip)
		remove_blip(escape_blip)
	endif

	for i = 0 to count_of(dead_body) - 1 
		if does_blip_exist(dead_body[i].blip)
			remove_blip(dead_body[i].blip)
		endif 
	endfor 
	
	for i = 0 to count_Of(cia_enemy) - 1
		if does_blip_exist(cia_enemy[i].blip)
			remove_blip(cia_enemy[i].blip)
		endif
		CLEANUP_AI_PED_BLIP(cia_enemy[i].blip_2)
	endfor 
	
	for i = 0 to count_Of(cia_enemy_top_floor) - 1
		if does_blip_exist(cia_enemy_top_floor[i].blip)
			remove_blip(cia_enemy_top_floor[i].blip)
		endif 
		CLEANUP_AI_PED_BLIP(cia_enemy_top_floor[i].blip_2)
	endfor 

endproc 


proc setup_mission_fail()

	MISSION_FLOW_MISSION_FAILED_WITH_REASON(mission_failed_text)

	remove_all_blips()
	
	//fail_time = get_game_timer()
	
	trigger_music_event("FBI1_DEAD")
	
	stop_mission_fail_checks = true
	
	mission_flow = mission_fail_stage 
	
endproc 

func bool mission_fail_checks()
	
	switch mission_flow
		
		case stealth_cutscene
		
			if operating_table_fail
			
				mission_failed_text = "FBI1_fail_3"
						
				return true 
			
			endif 

		break 
		
		case get_to_locker
		
//			if get_to_locker_status < 2
//				
//				if fail_time = 0
//					if not is_ped_in_specific_room(player_ped_id(), <<279.1091, -1337.4714, 24.0>>, "AutopsySmall") 
//						fail_time = get_game_timer()
//					endif 
//				else 

//					if lk_timer(fail_time, 90000)
//					
//						trigger_alarm(true)
//					
//						mission_failed_text = "FBI1_fail_2"
//						
//						return true 
//						
//					endif 
//				endif 
//			endif 

		break 
		
		case stealth_the_cia_officer
		
			if not is_ped_injured(cia_stealth.ped)
				
				if cia_stealth_enemy_ai_system_status >= 22

					if lk_timer(original_time, 12000)

						START_ALARM("FBI_01_MORGUE_ALARMS", true)
					
						mission_failed_text = "FBI1_fail_2"
				
						return true 
						
					endif 
				
				endif 
				
			endif 

//			if lk_timer(fail_time, 90000)
//			
//				trigger_alarm(true)
//			
//				//makes cia stealth enemy attack the player.
//				if not is_ped_injured(cia_stealth.ped)
//					set_blocking_of_non_temporary_events(cia_stealth.ped, false)
//					task_combat_ped(cia_stealth.ped, player_ped_id())
//				endif 
//			
//				mission_failed_text = "FBI1_fail_2"
//				
//				return true 
//				
//			endif 

		break 
		
		case find_bodies

//			if lk_timer(fail_time, 90000)
//			
//				trigger_alarm(true)
//			
//				mission_failed_text = "FBI1_fail_2"
//				
//				return true 
//				
//			endif 

		break 

		case shootout
		
		
		break 
		
		case escape_cutscene

		break
		
		case get_to_hills
		
			if mission_ped_injured(franklin.ped)
				mission_failed_text = "FBI1_fail_0"
				return true 
			endif 
			
			if get_player_wanted_level(player_id()) > 0
				if does_entity_exist(franklin.ped)
					if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(franklin.ped)) < 45.00
						if is_entity_on_screen(franklin.ped)
							mission_failed_text = "FBI1_fail_1"
							return true 
						endif 
					endif 
				endif 
			endif 
		
		break 
		
	endswitch 

	return false

endfunc

proc remove_all_mission_assets()

	int i = 0

	if IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
	else 
		REMOVE_CUTSCENE()
	endif 
	
	REMOVE_CUTSCENE()

	while is_cutscene_active()
		wait(0)	
	endwhile
	
	//REMOVE_CUTSCENE()
	
	release_mission_audio_bank()
	
	trigger_music_event("FBI1_DEAD")
	
	stop_audio_scenes()
	
	if zip_sound_activated
		if not has_sound_finished(zip_sound)
			stop_sound(zip_sound)
		endif 
	endif 
	
	SET_SCRIPT_GFX_DRAW_ORDER(gfx_order_after_hud)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(false)
	
	disable_cellphone(false)
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, false)
	
	REPLAY_CANCEL_EVENT()

	clear_player_wanted_level(player_id())
	
	//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, FALSE)
	
	//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	set_entity_visible(player_ped_id(), true)
	SET_PED_STEALTH_MOVEMENT(player_ped_id(), false)
	SET_PED_USING_ACTION_MODE(player_ped_id(), false)
	
	clear_area(<<287.9576, -1345.7250, 23.2416>>, 300.00, true)
	remove_decals_in_range(<<287.9576, -1345.7250, 23.2416>>, 300.00)
	
	clear_ped_tasks_immediately(player_ped_id())
	
	end_cutscene_no_fade()
	
	remove_all_blips()
	
//	if DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<264.77, -1345.87, 24.68>>, 1.0, v_ilev_cor_doorglassb) 
//		set_state_of_closest_door_of_type(v_ilev_cor_doorglassb, <<264.77, -1345.87, 24.68>>, false, 0.0)
//		set_state_of_closest_door_of_type(v_ilev_cor_doorglassa, <<267.43, -1343.90, 24.68>>, false, 0.0)
//	endif 

	if DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<252.69, -1361.46, 24.68>>, 1.0, v_ilev_cor_doorglassb)
		set_state_of_closest_door_of_type(v_ilev_cor_doorglassb, <<252.69, -1361.46, 24.68>>, false, 0.0)
		set_state_of_closest_door_of_type(v_ilev_cor_doorglassa, <<254.34, -1359.49, 24.68>>, false, 0.0)
	endif 

	if DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<252.87, -1366.76, 24.55>>, 1.0, v_ilev_cor_firedoor)
		set_state_of_closest_door_of_type(v_ilev_cor_firedoor, <<252.87, -1366.76, 24.55>>, false, 0.0)
		set_state_of_closest_door_of_type(v_ilev_cor_firedoor, <<251.1, -1365.27, 24.55>>, false, 0.0)
	endif 

//	for i = 0 to count_of(cia_enemy_cover_point) - 1
//		remove_cover_point(cia_enemy_cover_point[i])
//	endfor
//	
//	for i = 0 to count_of(cia_enemy_top_floor_cover_point) - 1
//		remove_cover_point(cia_enemy_top_floor_cover_point[i])
//	endfor 
	
//	for i = 0 to count_of(alarm_sound) - 1
//		if not has_sound_finished(alarm_sound[i])
//			stop_sound(alarm_sound[i])
//		endif 
//	endfor 
	
	stop_alarm("FBI_01_MORGUE_ALARMS", true)

	for i = 0 to count_of(doctor) - 1
		if does_entity_exist(doctor[i].ped)
			delete_ped(doctor[i].ped)
			set_model_as_no_longer_needed(doctor[i].model)
		endif 
	endfor 
	
	for i = 0 to count_of(dead_body) - 1 
		if does_entity_exist(dead_body[i].ped)
			delete_ped(dead_body[i].ped)
			set_model_as_no_longer_needed(dead_body[i].model)
		endif 
	endfor 
	
	if does_entity_exist(cia_stealth.ped)
		delete_ped(cia_stealth.ped)
		set_model_as_no_longer_needed(cia_stealth.model)
	endif 
	
	for i = 0 to count_Of(cia_enemy) - 1
		if does_entity_exist(cia_enemy[i].ped)
			delete_ped(cia_enemy[i].ped)
			set_model_as_no_longer_needed(cia_enemy[i].model)
		endif 
	endfor 
	
	for i = 0 to count_Of(cia_enemy_top_floor) - 1
		if does_entity_exist(cia_enemy_top_floor[i].ped)
			delete_ped(cia_enemy_top_floor[i].ped)
			set_model_as_no_longer_needed(cia_enemy_top_floor[i].model)
		endif 
	endfor 
	
	for i = 0 to count_of(cia_enemy_outside) - 1
		if does_entity_exist(cia_enemy_outside[i].ped)
			delete_ped(cia_enemy_outside[i].ped)
			set_model_as_no_longer_needed(cia_enemy_outside[i].model)
		endif 
	endfor 
	
	if does_entity_exist(franklin.ped)
		delete_ped(franklin.ped)
		set_model_as_no_longer_needed(franklin.model)
	endif 
	
	for i = 0 to count_of(bin_bag) - 1
		if does_entity_exist(bin_bag[i].obj)
			delete_object(bin_bag[i].obj)
			set_model_as_no_longer_needed(bin_bag[i].model)
		endif 
	endfor 
	
	if does_entity_exist(clip_board.obj)				
		set_object_as_no_longer_needed(clip_board.obj)
		set_model_as_no_longer_needed(clip_board.model)
	endif 
						
	if does_entity_exist(pen.obj)
		set_object_as_no_longer_needed(pen.obj)
		set_model_as_no_longer_needed(pen.model)
	endif 
	
	for i = 0 to count_of(bin) - 1
		if does_entity_exist(bin[i].obj)
			delete_object(bin[i].obj)
		endif 
		set_model_as_no_longer_needed(bin[i].model)
	endfor 
	
	for i = 0 to count_of(bin) - 1
	
		if does_blip_exist(bin_bag[i].blip)
			remove_blip(bin_bag[i].blip)
		endif 
		
	endfor 
			
	for i = 0 to count_of(ambient_car) - 1
		if does_entity_exist(ambient_car[i].veh)
			delete_vehicle(ambient_car[i].veh)
		endif 
		set_model_as_no_longer_needed(ambient_car[i].model)
	endfor
	
	for i = 0 to count_of(police_car) - 1
		if does_entity_exist(police_car[i].veh)
			delete_vehicle(police_car[i].veh)
		endif 
		set_model_as_no_longer_needed(police_car[i].model)
	endfor
	
	reset_glass_window()
	
	remove_all_mission_doors()
	
	remove_pickups()
	
	stop_stream()
						
	remove_anim_dict("missfbi1")
	remove_anim_dict("move_crouch_proto")
	REMOVE_STEALTH_MODE_ASSET("MICHAEL_ACTION")
	
	remove_waypoint_recording("fbi1_0")
	
//	if does_entity_exist(window.obj)
//		set_object_as_no_longer_needed(window.obj)
//	endif 

	CLEAR_TRIGGERED_LABELS()

endproc 


proc skip_system()

	#IF IS_DEBUG_BUILD
		dont_do_j_skip(locates_data)
		
		if is_keyboard_key_just_pressed(key_j)

			switch mission_flow
			
				case intro_mocap
				
					if is_cutscene_playing()
						stop_cutscene()
					endif 
				
				break 
			
				case stealth_cutscene

					if is_cutscene_playing()
						stop_cutscene()
					else 
						remove_cutscene()
					endif 
					
					fbi1_stealth_cutscene_status = 22
				
				break
				
				case get_to_locker
				
					set_entity_coords(player_ped_id(), <<274.9483, -1337.9481, 23.5377>>)
				
				break 
				
				case stealth_the_cia_officer
				
					if not is_ped_injured(cia_stealth.ped)
						set_entity_health(cia_stealth.ped, 2)
					endif 
					
					set_entity_coords(player_ped_id(), <<287.6915, -1346.0591, 23.5377>>)
				
				break 
				
				case find_bodies
					
					remove_all_mission_assets()

					load_trip_skip_data_2()

				break 
				
				case shootout
				
					switch fbi1_master_controller_status
					
						case 0
						case 1
							
					
							clear_ped_tasks_immediately(player_ped_id())
							set_entity_coords(player_ped_id(), <<244.9275, -1365.0420, 23.2391>>) 
							set_entity_heading(player_ped_id(), 134.7274) 
							
							activate_elevator_door_system = true
							
							fbi1_master_controller_status = 2

						break 
						
						case 2
						case 3
						case 4
						
							remove_all_mission_assets()
							
							if not does_blip_exist(escape_blip)
								escape_blip = create_blip_for_coord(<<236.4304, -1357.7548, 38.5342>>)
							endif 

							set_entity_coords(player_ped_id(), <<240.0, -1362.1, 38.5342>>) 
							set_entity_heading(player_ped_id(), 45.6778) 
							fbi1_master_controller_status = 5
						
						break 
						
						case 5
						case 6
						case 7
						
							if does_entity_exist(window.obj)
					
						
								mission_flow = escape_cutscene
								
							endif 
						
						break 
						
					endswitch 

				break 
				
				case escape_cutscene
				
					if fbi1_escape_cutscene_status >= 3
					
						remove_all_mission_assets()
			
						initialise_mission_variables()

						launch_mission_stage_menu_status = 4
						
						mission_flow = load_stage_selector_assets

					endif 
				
				break 

				case get_to_hills
				
					if is_cutscene_playing()
					
						stop_cutscene()
					
					else  
				
						clear_player_wanted_level(player_id())

						clear_ped_tasks_immediately(player_ped_id())
						set_entity_coords(player_ped_id(), <<1593.3832, -1934.3777, 98.6576>>) 
						set_entity_heading(player_ped_id(), 134.7274) 
						
					endif 

				break 
			
			endswitch
			
		elif is_keyboard_key_just_pressed(key_p)
		
			switch mission_flow
		
				
				case stealth_cutscene
				case get_to_locker
				case find_bodies
				case stealth_the_cia_officer
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					launch_mission_stage_menu_status = 0
					
				break 
				
				case shootout
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 1
					else
						launch_mission_stage_menu_status = 0
					endif 
						
					p_skip_time = get_game_timer()
				
				break 

				case escape_cutscene
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 3
					else
						launch_mission_stage_menu_status = 2
					endif 
						
					p_skip_time = get_game_timer()
					
				break 

				case get_to_hills
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 4
					else
						launch_mission_stage_menu_status = 3
					endif 
						
					p_skip_time = get_game_timer()
				
				break 

			endswitch 
		
		endif 
	
		if LAUNCH_MISSION_STAGE_MENU(menu_stage_selector, menu_return_stage) 
			
			remove_all_mission_assets()
			
			initialise_mission_variables()

			launch_mission_stage_menu_status = menu_return_stage
			
			mission_flow = load_stage_selector_assets
		endif
	#endif 

endproc 

proc fbi1_intro_mocap()

	switch intro_mocap_status
					
		case 0

			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fbi_1_int", CS_SECTION_2|CS_SECTION_3|CS_SECTION_4|CS_SECTION_5|CS_SECTION_6)

			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", PLAYER_PED_ID())
		
			if has_cutscene_loaded()
				
				//grabs dave from ben's lead in anims
				if does_entity_exist(g_sTriggerSceneAssets.ped[0])
					
					dave.ped = g_sTriggerSceneAssets.ped[0]
					
					set_entity_as_mission_entity(dave.ped, true, true)
					
					register_entity_for_cutscene(dave.ped, "Dave_FBI", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)

				endif 
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
				
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_1)
				
				start_cutscene(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_CUTSCENE_FADE_VALUES(false, false, true)

				intro_mocap_status++
			
			endif 

		break
		
		case 1
		
			if is_cutscene_playing()
			
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-389.924, 1116.777, 324.517>>, <<-472.317, 1138.854, 340.917>>, 148.500, <<-410.9237, 1174.4790, 324.6415>>, 253.2136, <<15.0, 15.0, 15.0>>)//, false)
				set_players_last_vehicle_to_vehicle_gen(<<-410.9237, 1174.4790, 324.6415>>, 253.2136)
			
				clear_area(get_entity_coords(player_ped_id()), 1000, true)
				
				intro_mocap_status++
			
			endif 
		
		break 
		
		case 2

			printint(get_cutscene_time())
			printnl()
			
			if not intro_mocap_shake_pad
				if get_cutscene_time() >= 133861
					SET_PAD_CAN_SHAKE_DURING_CUTSCENE(true)
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 200)
					intro_mocap_shake_pad = true
				endif 
			endif 

			if not is_cutscene_active()
				
				REPLAY_STOP_EVENT()
				
				load_normal_start_of_mission()
				
			endif 

		break 
		
	endswitch 

endproc 


float cam_speed = 0.5
FLOAT fMinYChange = 55.00
FLOAT fMaxYChange = 67.4
FLOAT fMinXChange = -139.00
FLOAT fMaxXChange = -119.00

vector current_cam_rot = <<63.400543,-0.000000,-129.389664>>

proc interactive_cutscene_cam()

	switch interactive_cutscene_cam_status 
	
		case 0
		
			if does_cam_exist(camera_a)
			
				printfloat(get_cam_near_dof(camera_a))
				printnl()
				
				printfloat(get_cam_far_dof(camera_a))
				printnl()
			
				#if is_debug_build 
					if widget_apply_dof_change

						SET_CAM_NEAR_DOF(camera_a, widget_near_dof)
						SET_CAM_FAR_DOF(camera_a, widget_far_dof)
						SET_CAM_DOF_STRENGTH(camera_a, widget_dof_strength)

						SET_CAM_USE_SHALLOW_DOF_MODE(camera_a, true)
						
						widget_apply_dof_change = false
						 
					endif 
				#endif 
				
			endif 
			
			SET_USE_HI_DOF()

			interactive_cutscene_cam_status++
		
		break 
		
		case 1
		
			if does_cam_exist(camera_a)

				int left_stick_x
				int left_stick_y
				int right_stick_x
				int right_stick_y
				
				float ftemp

				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
				
				IF NOT IS_LOOK_INVERTED()
					right_stick_y *= -1
				ENDIF
				
				// invert the vertical
				IF (right_stick_y > STICK_DEAD_ZONE)
				OR (right_stick_y < (STICK_DEAD_ZONE * -1))

					fTemp = TO_FLOAT(right_stick_y)
					fTemp *= fTemp
					fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
					fTemp *= cam_speed
					
					IF (right_stick_y < 0)
						fTemp *= -1.0
					ENDIF 

					current_cam_rot.x += fTemp
					
					IF (current_cam_rot.x < fMinYChange)
						current_cam_rot.x = fMinYChange	
					ENDIF
					IF (current_cam_rot.x > fMaxYChange)
						current_cam_rot.x = fMaxYChange	
					ENDIF
					
				endif 
				
				IF (right_stick_x > STICK_DEAD_ZONE)
				OR (right_stick_x < (STICK_DEAD_ZONE * -1))

					fTemp = TO_FLOAT(right_stick_x)
					fTemp *= fTemp
					fTemp /= TO_FLOAT(128 - STICK_DEAD_ZONE) * TO_FLOAT(128 - STICK_DEAD_ZONE)
					fTemp *= cam_speed
					
					IF (right_stick_x > 0)
						fTemp *= -1.0
					ENDIF

					current_cam_rot.z += fTemp
					
					IF (current_cam_rot.z < fMinXChange)
						current_cam_rot.z = fMinXChange	
					ENDIF
					IF (current_cam_rot.z > fMaxXChange)
						current_cam_rot.z = fMaxXChange	
					ENDIF
					
				endif 

				set_cam_rot(camera_a, <<current_cam_rot.x, 0.0, current_cam_rot.z>>) 
				
			endif 
			
		break 

	endswitch 
	
endproc 

proc request_mocap_data_for_FBI_1_MCS_1()
			
	if fbi1_stealth_cutscene_status > 0
	and not (fbi1_stealth_cutscene_status >= 6)
	
		if not is_cutscene_playing()
	
			request_cutscene("FBI_1_MCS_1_concat")
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
			
			if not is_ped_injured(doctor[0].ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Scientist_with_clipboard", doctor[0].ped)
			endif 
			
			if not is_ped_injured(doctor[1].ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Scientist_whos_grabbed", doctor[1].ped)
			endif 
			
		endif 
		
	endif 
		
endproc 


proc fbi1_stealth_cutscene()

	int i = 0
	
//	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//		fbi1_stealth_cutscene_status = 22
//	endif

//	printint(fbi1_stealth_cutscene_status)
//	printnl()
//	
//	if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_1_cutscene_index)
//		printstring("scene running")
//		printnl()
//	endif 
//	
//	if has_model_loaded(get_weapontype_model(cia_stealth.weapon))
//		printstring("cia_stealth.weapon loaded")
//		printnl()
//	endif 
		

	interactive_cutscene_cam()
	
	request_mocap_data_for_FBI_1_MCS_1()

	switch fbi1_stealth_cutscene_status 

		case 0
			
			request_anim_dict("Dead")
			
			//Set the multihead blinders instantly on
			SET_MULTIHEAD_SAFE(TRUE,TRUE)
							
			if start_new_cutscene_no_fade()
			
				//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, TRUE)
				remove_all_ped_weapons(player_ped_id())
				
				DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, true)

				clear_area(<<278.58, -1336.61, 23.68>>, 300, true)
				remove_decals_in_range(<<278.58, -1336.61, 23.68>>, 300.00)
			
				camera_a = create_cam_with_params("default_scripted_camera", <<280.335999,-1335.461914,24.553310>>,<<63.400543,-0.000000,-129.389664>>,56)//69.50
				SET_CAM_NEAR_DOF(camera_a, 0.01)
				SET_CAM_FAR_DOF(camera_a, 0.05)
				SET_CAM_DOF_STRENGTH(camera_a, 0.2)//1.0
				SET_CAM_USE_SHALLOW_DOF_MODE(camera_a, true)
				
				setup_enemy(doctor[0])
				DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(doctor[0].ped)
				DISABLE_PED_PAIN_AUDIO(doctor[0].ped, true) 
				SET_PED_COMPONENT_VARIATION(doctor[0].ped, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(doctor[0].ped, PED_COMP_TORSO, 0, 0)
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("3"), doctor[0].ped, "doctor1")
				remove_blip(doctor[0].blip)
				
				clip_board.obj = create_object(clip_board.model, (doctor[0].pos + <<0.0, 0.0, 1.0>>))
				ATTACH_ENTITY_TO_ENTITY(clip_board.obj, doctor[0].ped, get_ped_bone_index(doctor[0].ped, BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>) 
				
				pen.obj = create_object(pen.model, (doctor[0].pos + <<0.0, 0.0, 1.5>>))
				ATTACH_ENTITY_TO_ENTITY(pen.obj, doctor[0].ped, get_ped_bone_index(doctor[0].ped, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
				cutscene_pos = << 280.583, -1335.896, 23.588 >>
				cutscene_rot = << 0.000, 0.000, 38.000 >>
				doctor_0_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
				TASK_SYNCHRONIZED_SCENE(doctor[0].ped, doctor_0_cutscene_index, "missfbi1", "fbi_1_ig_1_b_clipb", INSTANT_BLEND_IN, normal_BLEND_OUT)
				force_ped_ai_and_animation_update(doctor[0].ped)

				//doctor who dies
				setup_enemy(doctor[1])
				DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(doctor[1].ped)
				DISABLE_PED_PAIN_AUDIO(doctor[1].ped, true)  
				STOP_PED_SPEAKING(doctor[1].ped, true)
				SET_PED_COMPONENT_VARIATION(doctor[1].ped, PED_COMP_HEAD, 1, 0)
				SET_PED_COMPONENT_VARIATION(doctor[1].ped, PED_COMP_TORSO, 0, 1)
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("4"), doctor[1].ped, "doctor2")
				remove_blip(doctor[1].blip)
		
				cutscene_pos = <<281.083, -1336.146, 23.638>>//<< 281.033, -1336.146, 23.588 >>
				cutscene_rot = << 0.000, 0.000, 44.000 >>
				doctor_1_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
				TASK_SYNCHRONIZED_SCENE(doctor[1].ped, doctor_1_cutscene_index, "missfbi1", "fbi_1_ig_1_a", INSTANT_BLEND_IN, normal_BLEND_OUT)
				set_synchronized_scene_phase(doctor_1_cutscene_index, 0.145)
				SET_SYNCHRONIZED_SCENE_RATE(doctor_1_cutscene_index, 0.0)
				force_ped_ai_and_animation_update(doctor[1].ped)
				
				//reception ped doctor.
				setup_enemy(doctor[4])
				remove_blip(doctor[4].blip)
				set_ped_can_be_targetted(doctor[4].ped, false)
							
				setup_buddy(dead_body[0])
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TORSO, 2, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_LEG, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[0].ped, PED_COMP_JBIB, 0, 0)
				DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[0].ped)
				set_ped_can_ragdoll(dead_body[0].ped, true)
				RETAIN_ENTITY_IN_INTERIOR(dead_body[0].ped, get_interior_at_coords_with_type(<<292.205, -1345.025, 24.570>>, "V_Coroner"))
				set_entity_load_collision_flag(dead_body[0].ped, true)
			
				cutscene_pos = <<291.552, -1345.871, 24.570>> 
				cutscene_rot = <<0.000, 0.000, 147.000>>
				
				task_play_anim_advanced(dead_body[0].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
				force_ped_ai_and_animation_update(dead_body[0].ped)
				
				//**********TEMP 
				//freeze_entity_position(dead_body[0].ped, true)
				//**********
				
				
				setup_buddy(dead_body[1])
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_LEG, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(dead_body[1].ped, PED_COMP_JBIB, 0, 0)
				DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(dead_body[1].ped)
				set_ped_can_ragdoll(dead_body[1].ped, true)
				RETAIN_ENTITY_IN_INTERIOR(dead_body[1].ped, get_interior_at_coords_with_type(<<297.302, -1348.671, 24.570>>, "V_Coroner"))
				set_entity_load_collision_flag(dead_body[1].ped, true)
				
				cutscene_pos = <<296.702, -1349.621, 24.570>> 
				cutscene_rot = <<0.000, 0.000, 140.000>>
				
//				dead_body_cutscene_index[1] = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
				
				
//				TASK_SYNCHRONIZED_SCENE(dead_body[1].ped, dead_body_cutscene_index[1], "missfbi1", "cpr_pumpchest_idle", INSTANT_BLEND_IN, normal_blend_out, synced_scene_use_physics)
//				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(dead_body_cutscene_index[1], true)
				task_play_anim_advanced(dead_body[1].ped, "missfbi1", "cpr_pumpchest_idle", cutscene_pos, cutscene_rot, INSTANT_BLEND_IN, normal_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
				force_ped_ai_and_animation_update(dead_body[1].ped)

				//**********TEMP 
				//freeze_entity_position(dead_body[1].ped, true)
				//**********


				clear_ped_tasks_immediately(player_ped_id())
				
				cutscene_pos = << 280.645, -1335.833, 24.500 >>
				cutscene_rot = << 0.000, 0.000, -138.000 >>
				cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
				TASK_SYNCHRONIZED_SCENE(player_ped_id(), cutscene_index, "missfbi1", "cpr_pumpchest_idle", INSTANT_BLEND_IN, normal_BLEND_OUT)
				set_entity_visible(player_ped_id(), false)

				//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_hand, 0, 0)
//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_special, 0, 0)
//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_special2, 0, 0)
//				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)

				//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 26, 0)//19
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 18, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 7, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
		

				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EARS)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_MOUTH)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_LEFT_HAND)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_RIGHT_HAND)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_LEFT_WRIST)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_RIGHT_WRIST)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HIP)
				
				add_ped_for_dialogue(scripted_speech, 0, player_ped_id(), "michael")
//				
				
				//set_entity_collision(player_ped_id(), false)
				//freeze_entity_position(player_ped_id(), true)
				
				for i = 0 to count_of(bin_bag) - 1
					
					bin_bag[i].obj = create_object_no_offset(bin_bag[i].model, bin_bag[i].pos)
					set_entity_heading(bin_bag[i].obj, bin_bag[i].heading)
					
					if i = 0
						FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<277.2329, -1336.3568, 24.00>>, "V_coroner"), get_hash_key("AutopsySmall"))
					elif i = 1
						FORCE_ROOM_FOR_ENTITY(bin_bag[i].obj, get_interior_at_coords_with_type(<<249.00, -1372.3, 39.5>>, "V_coroner"), get_hash_key("topoff_mid"))
					endif 
					
				endfor 
				
				bin[0].obj = create_object(bin[0].model, bin[0].pos)
				set_entity_rotation(bin[0].obj, bin[0].rot)
				freeze_entity_position(bin[0].obj, true)

				zip_inc = 0.05
				zip_y = -0.50 //-0.5
			

				set_cam_active(camera_a, true)						

				render_script_cams(true, false)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
				
				SET_CAM_CONTROLS_MINI_MAP_HEADING(camera_a, true)
				
				draw_rect(0.5, 0.5, 1, 1, 0, 0, 0, 255)
				
				wait(0)
				
				SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("rm_bsmt_AutopsySmall")
				
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)//1497341
				
				do_screen_fade_in(2000)
				
				START_AUDIO_SCENE("FBI_01_DMW_IN_BODY_BAG")

				original_time = get_game_timer()
				
				fbi1_stealth_cutscene_status++

			endif 
			
			draw_rect(0.5, 0.5, 1, 1, 0, 0, 0, 255)

		break 
		
		case 1

			draw_sprite("fbi1","zipbig", 0.5, zip_y, 1.0, 3.1, 0.0000, 255, 255, 255, 255)//3.0
			
			if is_screen_faded_in()
				if create_conversation(scripted_speech, "FBI1AUD", "fbi1_doc1", CONV_PRIORITY_medium)
					fbi1_stealth_cutscene_status++
				endif 
			endif
			
			SET_USE_HI_DOF()

		break 
		
		case 2

			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)

			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				if not zip_sound_activated
					stop_audio_scene("FBI_01_DMW_IN_BODY_BAG")
					play_sound_from_entity(zip_sound, "UNZIP", player_ped_id(), "FBI_01_SOUNDS")
					
					if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_1_cutscene_index)
						//SET_SYNCHRONIZED_SCENE_PHASE(doctor_1_cutscene_index, 0.135) //0.14, 
						SET_SYNCHRONIZED_SCENE_RATE(doctor_1_cutscene_index, 1.0)
					endif 
					
					zip_sound_activated = true
				endif 
				
				if zip_inc < 0.2
					zip_inc = zip_inc + 0.01
				endif
				
				if zip_y < 2.5
					zip_y += zip_inc
					draw_sprite("fbi1","zipbig", 0.5, zip_y, 1.0, 3.1, 0.0000, 255, 255, 255, 255) //zip zipbig
				else

					if zip_sound_activated
						if not has_sound_finished(zip_sound)
							stop_sound(zip_sound)
						endif 
					endif 
						
					original_time = get_game_timer()
						
					fbi1_stealth_cutscene_status++
					
				endif 
				
			else
				draw_sprite("fbi1","zipbig", 0.5, zip_y, 1.0, 3.1, 0.0000, 255, 255, 255, 255)
			endif
			
			SET_USE_HI_DOF()

		break
		
		case 3
		
			if create_conversation(scripted_speech, "FBI1AUD", "fbi1_doc5", CONV_PRIORITY_low)
				fbi1_stealth_cutscene_status++
			endif 
			
			SET_USE_HI_DOF()
				
		break 
		
		case 4
			
			printstring("get_cam_far_dof: ")
			printfloat(get_cam_far_dof(camera_a))
			printnl()
		
			if get_cam_far_dof(camera_a) < 4.0
				
				float far_dof
				
				far_dof = get_cam_far_dof(camera_a)

				//far_dof = far_dof + (far_dof * get_frame_time() * 0.5)
				
				//far_dof += @+1.0
				
				far_dof = far_dof +@ (far_dof * 2.0)
				
				
				if far_dof > 4.0
					far_dof = 4.0
				endif 
				
				SET_CAM_FAR_DOF(camera_a, far_dof)
				
				SET_USE_HI_DOF()
				
				if far_dof = 4.0
					fbi1_stealth_cutscene_status++
				endif 

			endif 
		
		break 

		case 5
		
			if not has_label_been_triggered("fbi1_doc2")
			
				int left_stick_x
				int left_stick_y
				int right_stick_x
				int right_stick_y
			
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
		
				IF NOT IS_LOOK_INVERTED()
					right_stick_y *= -1
				ENDIF
				
				// invert the vertical
				IF (right_stick_x > STICK_DEAD_ZONE) OR (right_stick_x < (STICK_DEAD_ZONE * -1))
				or (right_stick_y > STICK_DEAD_ZONE) OR (right_stick_y < (STICK_DEAD_ZONE * -1))
					if create_conversation(scripted_speech, "FBI1AUD", "fbi1_doc2", CONV_PRIORITY_medium)
						set_label_as_triggered("fbi1_doc2", true)

						print_help_forever("FBI1_help1")
				
						dialogue_time = get_game_timer()
						original_time = get_game_timer()
						
						fbi1_stealth_cutscene_status++
						
					endif 
				endif 
				
			endif 
		
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				print_help_forever("FBI1_help1")
				
				dialogue_time = get_game_timer()
				original_time = get_game_timer()
				
				fbi1_stealth_cutscene_status++
				
			endif 
			
		break 
		
		case 6
		
			if not has_label_been_triggered("fbi1_doc2")
				if not has_label_been_triggered("fbi1_doc4") //dont trigger dialogue once bone cutter dialogue plays
			
					int left_stick_x
					int left_stick_y
					int right_stick_x
					int right_stick_y
				
					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
			
					IF NOT IS_LOOK_INVERTED()
						right_stick_y *= -1
					ENDIF
					
					// invert the vertical
					IF (right_stick_x > STICK_DEAD_ZONE) OR (right_stick_x < (STICK_DEAD_ZONE * -1))
					or (right_stick_y > STICK_DEAD_ZONE) OR (right_stick_y < (STICK_DEAD_ZONE * -1))
						
						if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

							dialogue_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
				
							IF ARE_STRINGS_EQUAL(dialogue_root, "fbi1_doc3")
					
								specific_label = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
		
								dialogue_paused = true 
								
							endif 
							
						endif 
						
						if create_conversation(scripted_speech, "FBI1AUD", "fbi1_doc2", CONV_PRIORITY_medium)
							set_label_as_triggered("fbi1_doc2", true)
							//script_assert("test 0")
						
						else 
						
							printstring("not create conversation")
							printnl()
						
						endif 
						
					endif 
					
				endif 
				
			endif 

			if not has_label_been_triggered("fbi1_doc3")
				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech, "FBI1AUD", "fbi1_doc3", CONV_PRIORITY_low)
						set_label_as_triggered("fbi1_doc3", true)
					endif 
				endif 
				
			else
			
				if not is_any_text_being_displayed(locates_data)
					
					if dialogue_paused
						
						if not is_string_null(specific_label)
							if create_conversation_from_specific_line(scripted_speech, "FBI1AUD", "fbi1_doc3", specific_label, CONV_PRIORITY_low) 
								dialogue_paused = false
							endif 
						
						else 
							dialogue_paused = false
						endif 
						
					endif 
				endif 
				
			endif 

			if not has_label_been_triggered("fbi1_doc4")
				if lk_timer(original_time, 55000) 
					if not is_any_text_being_displayed(locates_data)

						if create_conversation(scripted_speech, "FBI1AUD", "fbi1_doc4", CONV_PRIORITY_medium)
						
							if not is_ped_injured(doctor[0].ped)
							
								cutscene_pos = << 280.583, -1335.896, 23.588 >>
								cutscene_rot = << 0.000, 0.000, 38.000 >>
								doctor_0_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
								TASK_SYNCHRONIZED_SCENE(doctor[0].ped, doctor_0_cutscene_index, "missfbi1", "fbi_1_ig_3_b_clipb", 1, normal_BLEND_OUT)
								set_synchronized_scene_phase(doctor_0_cutscene_index, 0.45)
								//force_ped_ai_and_animation_update(doctor[0].ped)
								
							endif 
							
							if not is_ped_injured(doctor[1].ped)

								cutscene_pos = <<281.083, -1336.146, 23.638>>//<< 281.033, -1336.146, 23.588 >>
								cutscene_rot = << 0.000, 0.000, 44.000 >>
								doctor_1_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
								TASK_SYNCHRONIZED_SCENE(doctor[1].ped, doctor_1_cutscene_index, "missfbi1", "fbi_1_ig_3_a", 1, normal_BLEND_OUT)
								set_synchronized_scene_phase(doctor_1_cutscene_index, 0.45)
								//force_ped_ai_and_animation_update(doctor[1].ped)
									
							endif 
							
							set_label_as_triggered("fbi1_doc4", true)
						endif 
						
					endif 
				
				else 
				
					if not is_ped_injured(doctor[0].ped)
						if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_0_cutscene_index)
//							if is_entity_playing_anim("missfbi1", react anims 
//								if GET_SYNCHRONIZED_SCENE_PHASE(doctor_0_cutscene_index) >= 1.0
//								
//									cutscene_pos = << 280.583, -1335.896, 23.588 >>
//									cutscene_rot = << 0.000, 0.000, 38.000 >>
//									doctor_0_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
//									TASK_SYNCHRONIZED_SCENE(doctor[0].ped, doctor_0_cutscene_index, "missfbi1", "fbi_1_ig_2_b_clipb", normal_BLEND_IN, normal_BLEND_OUT)
//									set_synchronized_scene_phase(doctor_1_cutscene_index, set to a nice blend phase to suit both anims)
//									
//								endif 
//								
//							else 

								if GET_SYNCHRONIZED_SCENE_PHASE(doctor_0_cutscene_index) >= 1.0

									cutscene_pos = << 280.583, -1335.896, 23.588 >>
									cutscene_rot = << 0.000, 0.000, 38.000 >>
									doctor_0_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
									TASK_SYNCHRONIZED_SCENE(doctor[0].ped, doctor_0_cutscene_index, "missfbi1", "fbi_1_ig_2_b_clipb", normal_BLEND_IN, normal_BLEND_OUT)
									force_ped_ai_and_animation_update(doctor[0].ped)
									
								endif 
//							endif 
						endif 
					endif 
					
					if not is_ped_injured(doctor[1].ped)
						if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_1_cutscene_index)
						//	if is_entity_playing_anim("missfbi1", react anims 
//								if GET_SYNCHRONIZED_SCENE_PHASE(doctor_1_cutscene_index) >= 1.0
//								
//									cutscene_pos = << 280.583, -1335.896, 23.588 >>
//									cutscene_rot = << 0.000, 0.000, 38.000 >>
//									doctor_1_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
//									TASK_SYNCHRONIZED_SCENE(doctor[1].ped, doctor_1_cutscene_index, "missfbi1", "fbi_1_ig_2_b_clipb", normal_BLEND_IN, normal_BLEND_OUT)
//									set_synchronized_scene_phase(doctor_1_cutscene_index, set to a nice blend phase to suit both anims)
//									
//								endif 
//								
//							else 

								if GET_SYNCHRONIZED_SCENE_PHASE(doctor_0_cutscene_index) >= 1.0

									cutscene_pos = <<281.083, -1336.146, 23.638>>//<< 281.033, -1336.146, 23.588 >>
									cutscene_rot = << 0.000, 0.000, 44.000 >>
									doctor_1_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
									TASK_SYNCHRONIZED_SCENE(doctor[1].ped, doctor_1_cutscene_index, "missfbi1", "fbi_1_ig_2_a", normal_BLEND_IN, normal_BLEND_OUT)
									force_ped_ai_and_animation_update(doctor[1].ped)
									
								endif 
								
//							endif 
						endif 
					endif 

				endif 
			
			else 
			
				//if not is_any_text_being_displayed(locates_data)
				
				if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_1_cutscene_index)
					if GET_SYNCHRONIZED_SCENE_PHASE(doctor_1_cutscene_index) >= 0.6
					
						clear_help()
						
						if not is_ped_injured(doctor[1].ped)
							bone_saw.obj = create_object(bone_saw.model, get_offset_from_entity_in_world_coords(doctor[1].ped, <<0.0, 0.0, 1.0>>))
							attach_entity_to_entity(bone_saw.obj, doctor[1].ped,  get_ped_bone_index(doctor[1].ped, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						endif 

						fbi1_stealth_cutscene_status = 30
						
					endif 
				endif 
			
			endif 
			
			if is_control_pressed(frontend_control, INPUT_SKIP_CUTSCENE)
				if has_cutscene_loaded()
					if start_new_cutscene_no_fade(false)
					
						stop_stream()
					
						trigger_music_event("FBI1_WAKE_UP")
						
						if not is_ped_injured(doctor[0].ped)
							register_entity_for_cutscene(doctor[0].ped, "Scientist_with_clipboard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						endif 
						
						if not is_ped_injured(doctor[1].ped)
							register_entity_for_cutscene(doctor[1].ped, "Scientist_whos_grabbed", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						endif 

						if is_entity_attached(clip_board.obj)
							detach_entity(clip_board.obj)
						endif 
						
						if is_entity_attached(pen.obj)
							detach_entity(pen.obj)
						endif 
						
						cia_weapon_obj.obj = CREATE_WEAPON_OBJECT(cia_stealth.weapon, infinite_ammo, <<282.9, -1345.1, 25.3>>, true)
						GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(cia_weapon_obj.obj, WEAPONCOMPONENT_AT_PI_FLSH)
						register_entity_for_cutscene(cia_weapon_obj.obj, "cia_pistol", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						
						register_entity_for_cutscene(clip_board.obj, "FIB_1_clipboard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						register_entity_for_cutscene(pen.obj, "FIB_1_Pencil", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "cia_guard", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, cia_stealth.model)
						
						//cia_weapon_obj = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(steve.ped, WEAPONTYPE_CARBINERIFLE) 
						register_entity_for_cutscene(cia_weapon_obj.obj, "cia_pistol", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)

						start_cutscene()
						
//						PRELOAD_STORED_PLAYER_PED_VARIATION(player_ped_id())
//						SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_TORSO, 19, 0)
						
//						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
						SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)

						original_time = get_game_timer()
						
						fbi1_stealth_cutscene_status = 7
					endif 
				endif 
			endif

		break
		
		case 7
		
			if is_cutscene_playing()
			
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
				
				set_entity_visible(player_ped_id(), true)
				set_entity_collision(player_ped_id(), true)
				freeze_entity_position(player_ped_id(), false)
			
				destroy_all_cams()
				render_script_cams(false, false)
				set_gameplay_cam_relative_heading(0)
				set_gameplay_cam_relative_pitch(0)
			
				if does_entity_exist(bone_saw.obj)
					delete_object(bone_saw.obj)
					set_model_as_no_longer_needed(bone_saw.model)
				endif 
			
				fbi1_stealth_cutscene_status++
			
			endif 
		
		break 
		
		case 8

			request_anim_dict("missfbi1ig_7")
			REQUEST_STEALTH_MODE_ASSET("MICHAEL_ACTION")
			
			if does_entity_exist(cia_stealth.ped)
				if not is_ped_injured(cia_stealth.ped)
					set_ped_reset_flag(cia_stealth.ped, PRF_ForceEnableFlashLightForAI, true)
				endif 
			endif 
			
			if does_entity_exist(cia_weapon_obj.obj)
				SET_CUTSCENES_WEAPON_FLASHLIGHT_ON_THIS_FRAME(cia_weapon_obj.obj)
			endif 
		
			if is_cutscene_active()
			
//				printint(get_cutscene_time())
//				printnl()
			
				if not was_cutscene_skipped()

					if not michael_clothes_set
						if get_cutscene_time() > 17200
//							RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_hand, 0, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_special, 0, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_special2, 0, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
//							clear_all_ped_props(PLAYER_PED_ID())

//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
							//clear_all_ped_props(PLAYER_PED_ID())
							remove_player_ped_props(player_ped_id())
							michael_clothes_set = true 
						endif 
					endif 
					
					if not rumble_pad
						if get_cutscene_time() > 500
							SET_PAD_CAN_SHAKE_DURING_CUTSCENE(true)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 6500, 200)
							rumble_pad = true
						endif 
					endif 
					
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("cia_guard", cia_stealth.model))
						cia_stealth.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("cia_guard", cia_stealth.model))
					ENDIF

					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Scientist_whos_grabbed")
						
						if not is_ped_injured(doctor[1].ped)
						
							SET_PED_CAN_RAGDOLL(doctor[1].ped, TRUE)
							
							task_play_anim_advanced(doctor[1].ped, "Dead", "dead_c", <<278.835, -1336.046, 24.580>>, <<0.000, 0.000, 74.000>>, instant_blend_in, normal_blend_out, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
							force_ped_ai_and_animation_update(doctor[1].ped)
						
							remove_ped_for_dialogue(scripted_speech, 4)

						ENDIF
					ENDIF
				
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("cia_guard")
						
						if has_anim_dict_loaded("missfbi1ig_7")
					
							if not is_ped_injured(cia_stealth.ped)
							
								setup_enemy_attributes(cia_stealth)
								setup_relationship_enemy(cia_stealth.ped, true)
								GIVE_WEAPON_COMPONENT_TO_PED(cia_stealth.ped, weapontype_combatpistol, WEAPONCOMPONENT_AT_PI_FLSH)
								set_current_ped_weapon(cia_stealth.ped, weapontype_combatpistol, true)
							
								//SET_PED_DROPS_WEAPONS_WHEN_DEAD(cia_stealth.ped, false)
								STOP_PED_SPEAKING(cia_stealth.ped, true)
								add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("6"), cia_stealth.ped, "CIA1") 

								fORCE_PED_MOTION_STATE(cia_stealth.ped, MS_aiming, TRUE)
								cia_stealth_enemy_ai_system()
								
							endif 
					
						endif 
					
					endif 
					
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("cia_pistol")
						give_weapon_object_to_ped(cia_weapon_obj.obj, cia_stealth.ped)
					endif 
				
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael")
					
						if has_STEALTH_MODE_asset_loaded("MICHAEL_ACTION")
						
							SET_PED_STEALTH_MOVEMENT(player_ped_id(), true)
							force_ped_motion_state(player_ped_id(), MS_STEALTHMODE_IDLE) 
							force_ped_ai_and_animation_update(player_ped_id())
							SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 1500) 
							REPLAY_STOP_EVENT()
						endif 
						
					endif
					
				else 
				
					SET_CUTSCENE_FADE_VALUES(false, false, true)
					
					fbi1_stealth_cutscene_status = 22
				
				endif 
				
			else 
			
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("script\\FBI_01_DMW_Autopsy_1")
			
				RELEASE_PED_PRELOAD_VARIATION_DATA(player_ped_id())
			
				delete_ped(doctor[0].ped)
				set_model_as_no_longer_needed(doctor[0].model)

				if does_entity_exist(clip_board.obj)				
					delete_object(clip_board.obj)
				endif 
				set_model_as_no_longer_needed(clip_board.model)
									
				if does_entity_exist(pen.obj)
					delete_object(pen.obj)
				endif 
				set_model_as_no_longer_needed(pen.model)

				set_streamed_texture_dict_as_no_longer_needed("fbi1")
				
				disable_cellphone(false)
				
				SET_SCRIPT_GFX_DRAW_ORDER(gfx_order_after_hud)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(false)
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "stealth_the_cia_officer")

				trigger_music_event("FBI1_GET_GUN")
				start_audio_scene("FBI_1_STEALTH_KILL_AGENT")
				
				end_cutscene_no_fade(false, false)

				mission_flow = stealth_the_cia_officer

			endif 

		break 
		
		case 22
		
			if is_cutscene_active()
			
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("cia_guard", GET_PLAYER_PED_MODEL(char_trevor)))
					cia_stealth.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("cia_guard", GET_PLAYER_PED_MODEL(char_trevor)))
				ENDIF
			
			else 
			
				if has_anim_dict_loaded("missfbi1ig_7")
				and has_STEALTH_MODE_asset_loaded("MICHAEL_ACTION")
				and has_ped_preload_variation_data_finished(player_ped_id())

					if is_audio_scene_active("FBI_01_DMW_IN_BODY_BAG")
						stop_audio_scene("FBI_01_DMW_IN_BODY_BAG")
					endif 
					
					if zip_sound_activated
						if not has_sound_finished(zip_sound)
							stop_sound(zip_sound)
						endif 
					endif 
					
					delete_ped(doctor[0].ped)
					set_model_as_no_longer_needed(doctor[0].model)

					if does_entity_exist(clip_board.obj)				
						delete_object(clip_board.obj)
					endif 
					set_model_as_no_longer_needed(clip_board.model)
										
					if does_entity_exist(pen.obj)
						delete_object(pen.obj)
					endif 
					set_model_as_no_longer_needed(pen.model)

					set_streamed_texture_dict_as_no_longer_needed("fbi1")

					SET_SCRIPT_GFX_DRAW_ORDER(gfx_order_after_hud)
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(false)
					
					set_streamed_texture_dict_as_no_longer_needed("fbi1")
					
					if not michael_clothes_set
						
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HEAD, 0, 0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
//						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
						//clear_all_ped_props(PLAYER_PED_ID())
						remove_player_ped_props(player_ped_id())
						
					endif 

					clear_ped_tasks_immediately(player_ped_id())
					set_entity_visible(player_ped_id(), true)
					set_entity_collision(player_ped_id(), true)
					freeze_entity_position(player_ped_id(), false)
					set_entity_coords(player_ped_id(), <<279.0734, -1337.0759, 23.5545>>)
					set_entity_heading(player_ped_id(), 227.5366) 
					SET_PED_STEALTH_MOVEMENT(player_ped_id(), true)
					force_ped_motion_state(player_ped_id(), MS_STEALTHMODE_IDLE) 
					force_ped_ai_and_animation_update(player_ped_id())
					SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 1500) 
					
					
					if not is_ped_injured(cia_stealth.ped)
						clear_ped_tasks_immediately(cia_stealth.ped)
						set_entity_coords(cia_stealth.ped, <<284.5221, -1343.0671, 23.5380>>)
						set_entity_heading(cia_stealth.ped, 231.3517) 
						setup_enemy_attributes(cia_stealth)
						setup_relationship_enemy(cia_stealth.ped, true)
						give_weapon_object_to_ped(cia_weapon_obj.obj, cia_stealth.ped)
						GIVE_WEAPON_COMPONENT_TO_PED(cia_stealth.ped, weapontype_combatpistol, WEAPONCOMPONENT_AT_PI_FLSH)
						set_current_ped_weapon(cia_stealth.ped, weapontype_combatpistol)
						//SET_PED_DROPS_WEAPONS_WHEN_DEAD(cia_stealth.ped, false)
						STOP_PED_SPEAKING(cia_stealth.ped, true)
						add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("6"), cia_stealth.ped, "CIA1") 

						fORCE_PED_MOTION_STATE(cia_stealth.ped, MS_aiming, TRUE)
						cia_stealth_enemy_ai_system()
						force_ped_ai_and_animation_update(cia_stealth.ped)
					endif 
				

					if not is_ped_injured(doctor[1].ped)
						SET_PED_CAN_RAGDOLL(doctor[1].ped, TRUE)
					
						task_play_anim_advanced(doctor[1].ped, "Dead", "dead_c", <<278.835, -1336.046, 24.580>>, <<0.000, 0.000, 74.000>>, instant_blend_in, normal_blend_out, -1, AF_ENDS_IN_DEAD_POSE | AF_OVERRIDE_PHYSICS | af_extract_initial_offset | AF_NOT_INTERRUPTABLE, 0.99)
						force_ped_ai_and_animation_update(doctor[1].ped)
						
						remove_ped_for_dialogue(scripted_speech, 4)
					ENDIF


					disable_cellphone(false)
					
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "stealth_the_cia_officer")

					trigger_music_event("FBI1_GET_GUN")
					start_audio_scene("FBI_1_STEALTH_KILL_AGENT")
					
					while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
						wait(0)
					endwhile 	
					
					end_cutscene()
					
					mission_flow = stealth_the_cia_officer
					
				endif 
				
			endif 
			
		break 
		
		case 30
		
			if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_1_cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(doctor_1_cutscene_index) >= 0.65

					start_particle_fx_non_looped_at_coord("scr_fbi_autopsy_blood", <<280.7, -1335.8, 24.7>>, <<0.0, 0.0, 0.0>>)//<<90.0, 0.0, 50.0>>)

					SET_CONTROL_SHAKE(PLAYER_CONTROL, 1500, 200)
					
					fbi1_stealth_cutscene_status++
				endif 
			endif 

		break 
			
		case 31

			if IS_SYNCHRONIZED_SCENE_RUNNING(doctor_1_cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(doctor_1_cutscene_index) >= 0.72

					//operating_table_fail = true 
					
					if not is_screen_faded_out()
						if not is_screen_fading_out()
							clear_help()
							do_screen_fade_out(2000)
						endif 
					
					 else
						
						remove_particle_fx_in_range(<<280.7, -1335.8, 24.7>>, 100.00)
											 
					 	set_entity_visible(player_ped_id(), true)
						set_entity_health(player_ped_id(), 2)
						end_cutscene_no_fade()
						//SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("rm_bsmt_AutopsySmall")
						//mission_failed()
					 
					 endif 
					
				endif 
			endif 
					
		break 
		
	endswitch 

endproc 

proc inspect_body_0_system()

	get_relative_heading_beteween_player_and_ped(dead_body[0].ped)
			
	switch inspect_body_0_status 
	
		case 0
		
			request_anim_dict("missfbi1ig_9")
			
			if has_anim_dict_loaded("missfbi1ig_9")

				if is_entity_at_coord(player_ped_id(), get_entity_coords(dead_body[0].ped, false), <<2.0, 2.0, 2.5>>, false, true)
					
					if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[0].ped, 60.00)
					
						if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[0].ped, 35.00) //25
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_front", slow_blend_in, slow_blend_out, -1) 

						elif get_relative_heading_beteween_player_and_ped(dead_body[0].ped) < -35.00
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_right", slow_blend_in, slow_blend_out, -1) 
						
						elif get_relative_heading_beteween_player_and_ped(dead_body[0].ped) > 35.00
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_left", slow_blend_in, slow_blend_out, -1) 
							
						endif 
						
						//--- only force the hint for first person. 
						if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
							
							vector focus_pos
						
							focus_pos = get_entity_coords(dead_body[0].ped, false)
							focus_pos.z -= 1.0
						
							SET_GAMEPLAY_COORD_HINT(focus_pos, 3000, 2500)
							
						endif 
						//---
						
						//SET_GAMEPLAY_COORD_HINT(get_entity_coords(dead_body[0].ped), 3000, 2500)
						
						if does_blip_exist(dead_body[0].blip)
							remove_blip(dead_body[0].blip)
						endif 
						
						task_look_at_coord(player_ped_id(), get_entity_coords(dead_body[0].ped, false), -1)
						
						//inspect_body_0_status = 22
						inspect_body_0_status = 2
						
					else 
						
						vector focus_pos
					
						focus_pos = get_entity_coords(dead_body[0].ped, false)
						focus_pos.z -= 1.0
					
						SET_GAMEPLAY_COORD_HINT(focus_pos, 3000, 2500)
					
						open_sequence_task(seq)
							task_turn_ped_to_face_coord(null, get_entity_coords(dead_body[0].ped, false)) 
						close_sequence_task(seq)
						task_perform_sequence(player_ped_id(), seq)
						clear_sequence_task(seq)
						
						fail_safe_time = get_game_timer()
						
						inspect_body_0_status++
					
					endif 

				endif 
				
			endif 
		
		break 
		
		case 1
		
			if not is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[0].ped, 25.00)
			
				printstring("dead body 0 text 0")
				printnl()
				
				if has_ped_task_finished_2(player_ped_id())
				
					open_sequence_task(seq)
						task_turn_ped_to_face_coord(null, get_entity_coords(dead_body[0].ped, false)) 
					close_sequence_task(seq)
					task_perform_sequence(player_ped_id(), seq)
					clear_sequence_task(seq)
					
				endif 
				
			else 
			
				printstring("dead body 0 text 1")
				printnl()
			
				if has_ped_task_finished_2(player_ped_id())
				//or lk_timer(fail_safe_time, 10000)
				fail_safe_time = fail_safe_time
				
					if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[0].ped, 60.00)
					
						if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[0].ped, 35.00) //25
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_front", slow_blend_in, slow_blend_out, -1) 

						elif get_relative_heading_beteween_player_and_ped(dead_body[0].ped) < -35.00
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_right", slow_blend_in, slow_blend_out, -1) 
						
						elif get_relative_heading_beteween_player_and_ped(dead_body[0].ped) > 35.00
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_left", slow_blend_in, slow_blend_out, -1) 
							
						endif 
						
						if does_blip_exist(dead_body[0].blip)
							remove_blip(dead_body[0].blip)
						endif
						
						task_look_at_coord(player_ped_id(), get_entity_coords(dead_body[0].ped, false), -1)
						
						//inspect_body_0_status = 22
						
						inspect_body_0_status = 2
						
					endif 
					
				endif 

				//inspect_body_0_status = 0
			
			endif 
		
		break
		
		case 2
		
			if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
				if IS_GAMEPLAY_HINT_ACTIVE()	
					 DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
       				 DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				endif 
			endif 
		
			if not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_front")
			and not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_right")
			and not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_left")
			
				task_clear_look_at(player_ped_id())
				
				STOP_GAMEPLAY_HINT()
				
				inspect_body_0_status = 22
			
			endif 
		
		break 
		
		case 22
		
		break 

	endswitch 
	
endproc 

proc inspect_body_1_system()

	get_relative_heading_beteween_player_and_ped(dead_body[1].ped)
			
	switch inspect_body_1_status 
	
		case 0
		
			request_anim_dict("missfbi1ig_9")
			
			if has_anim_dict_loaded("missfbi1ig_9")

				if is_entity_at_coord(player_ped_id(), get_entity_coords(dead_body[1].ped, false), <<2.0, 2.0, 2.5>>, false, true)
					
					if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[1].ped, 60.00)
					
						if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[1].ped, 35.00)//25
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_front", slow_blend_in, slow_blend_out, -1)//, AF_SECONDARY | AF_UPPERBODY) 

						elif get_relative_heading_beteween_player_and_ped(dead_body[1].ped) < -35.00
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_right", slow_blend_in, slow_blend_out, -1)//, AF_SECONDARY | AF_UPPERBODY) 
						
						elif get_relative_heading_beteween_player_and_ped(dead_body[1].ped) > 35.00
						
							task_play_anim(player_ped_id(), "missfbi1ig_9", "look_left", slow_blend_in, slow_blend_out, -1)//, AF_SECONDARY | AF_UPPERBODY) 
							
						endif 
						
						//--- only force the hint for first person. 
						if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
							
							vector focus_pos
						
							focus_pos = get_entity_coords(dead_body[1].ped, false)
							focus_pos.z -= 1.0
						
							SET_GAMEPLAY_COORD_HINT(focus_pos, 3000, 2500)
							
						endif 
						
						if does_blip_exist(dead_body[1].blip)
							remove_blip(dead_body[1].blip)
						endif 
						
						task_look_at_coord(player_ped_id(), get_entity_coords(dead_body[1].ped, false), -1)
						
						inspect_body_1_status = 2
						
					else 
					
						vector focus_pos
					
						focus_pos = get_entity_coords(dead_body[1].ped, false)
						focus_pos.z -= 1.0
					
						SET_GAMEPLAY_COORD_HINT(focus_pos, 3000, 2500)
					
						open_sequence_task(seq)
							task_turn_ped_to_face_coord(null, get_entity_coords(dead_body[1].ped, false)) 
						close_sequence_task(seq)
						task_perform_sequence(player_ped_id(), seq)
						clear_sequence_task(seq)
						
						inspect_body_1_status++
					
					endif 

				endif 
				
			endif 
		
		break 
		
		case 1
		
			if not is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[1].ped, 25.00)
			
				if has_ped_task_finished_2(player_ped_id())
				
					open_sequence_task(seq)
						task_turn_ped_to_face_coord(null, get_entity_coords(dead_body[1].ped, false)) 
					close_sequence_task(seq)
					task_perform_sequence(player_ped_id(), seq)
					clear_sequence_task(seq)
					
				endif 
				
			else 
			
				if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[1].ped, 60.00)
					
					if is_ped_within_heading_range_of_entity(player_ped_id(), dead_body[1].ped, 35.00)//25
					
						task_play_anim(player_ped_id(), "missfbi1ig_9", "look_front", slow_blend_in, slow_blend_out, -1)//, AF_SECONDARY | AF_UPPERBODY) 

					elif get_relative_heading_beteween_player_and_ped(dead_body[1].ped) < -35.00
					
						task_play_anim(player_ped_id(), "missfbi1ig_9", "look_right", slow_blend_in, slow_blend_out, -1)//, AF_SECONDARY | AF_UPPERBODY) 
					
					elif get_relative_heading_beteween_player_and_ped(dead_body[1].ped) > 35.00
					
						task_play_anim(player_ped_id(), "missfbi1ig_9", "look_left", slow_blend_in, slow_blend_out, -1)//, AF_SECONDARY | AF_UPPERBODY) 
						
					endif 
					
					if does_blip_exist(dead_body[1].blip)
						remove_blip(dead_body[1].blip)
					endif 
					
					task_look_at_coord(player_ped_id(), get_entity_coords(dead_body[1].ped, false), -1)
				
					inspect_body_1_status = 2
					
				endif 
			
			endif 
		
		break
		
		case 2
		
			if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
				if IS_GAMEPLAY_HINT_ACTIVE()	
					 DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
       				 DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				endif 
			endif 
		
			if not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_front")
			and not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_right")
			and not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_left")
			
				task_clear_look_at(player_ped_id())
				
				STOP_GAMEPLAY_HINT()
				
				inspect_body_1_status = 22
			
			endif 
		
		break 
		
		case 22
		
		break 

	endswitch 
	
endproc 

proc find_bodies_dialogue_system()

	if not has_label_been_triggered("mr_k_0")
		if not does_blip_exist(dead_body[0].blip) 
			if create_conversation(scripted_speech, "FBI1AUD", "mr_k_0", CONV_PRIORITY_low)
				clear_prints()
				set_label_as_triggered("mr_k_0", true)
			endif 
		endif 
	endif
	
	if not has_label_been_triggered("mr_k_1")
		if not does_blip_exist(dead_body[1].blip) 
			if create_conversation(scripted_speech, "FBI1AUD", "mr_k_1", CONV_PRIORITY_low)
				clear_prints()
				set_label_as_triggered("mr_k_1", true)
			endif 
		endif 
	endif

endproc 

proc give_player_weapon_component_system()
	
	if not pistol_flash_light_equiped
		if has_ped_got_weapon(player_ped_id(), weapontype_combatpistol)
			GIVE_WEAPON_COMPONENT_TO_PED(player_ped_id(), weapontype_combatpistol, WEAPONCOMPONENT_AT_PI_FLSH)
			set_ped_ammo(player_ped_id(), weapontype_combatpistol, 80)
			SET_PLAYER_PED_WEAPON_COMP_PURCHASED(CHAR_MICHAEL, WEAPONTYPE_COMBATPISTOL, WEAPONCOMPONENT_AT_PI_FLSH, TRUE)
			pistol_flash_light_equiped = true
		endif 
	endif 
	
endproc 

func bool fbi1_find_bodies()

	int i

	//player_dazed_movement_system()

	if not does_blip_exist(dead_body[0].blip) and not does_blip_exist(dead_body[1].blip)
//		//if get_script_task_status(player_ped_id(), script_task_play_anim) != performing_task
//	
		if not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_front")
		and not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_left") 
		and not is_entity_playing_anim(player_ped_id(), "missfbi1ig_9", "look_right")

			if has_label_been_triggered("mr_k_0") and has_label_been_triggered("mr_k_1")

				if not is_any_text_being_displayed(locates_data)
				
					task_clear_look_at(player_ped_id())
//
					set_player_can_use_cover(player_id(), true)
					
					DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, false)
				
					add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("5"), null, "dave") 

					shootout_time = get_game_timer()

					mission_flow = shootout
					
					return true 
					
				endif 
			endif 
			
		endif  

	endif 
	
	//bypass find dead bodis stage if the player decides to walk round the corner in the labs where the cia enemies are
	if not is_entity_in_angled_area(player_ped_id(), <<284.463, -1354.515, 23.538>>, <<294.471, -1342.418, 26.538>>, 18.600)  
	and not is_entity_in_angled_area(player_ped_id(), <<277.116, -1340.988, 23.537>>, <<283.480, -1333.405, 26.537>>, 11.00)
		
		for i = 0 to count_of(dead_body) - 1
			if does_blip_exist(dead_body[i].blip)
				remove_blip(dead_body[i].blip)
			endif 
		endfor 
		
		phone_call_system_status = 3
		
		set_player_can_use_cover(player_id(), true)
					
		DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, false)
		
		bypass_find_dead_bodies_stage = true
		
		mission_flow = shootout
					
		return true 

	endif 
		
	
	//stops player climbing onto dead bodies bead and then playing look at anim - bug 1484345
	if is_ped_in_specific_room(player_ped_id(), <<288.9353, -1347.9430, 23.5378>>, "BodyStorage", "v_coroner")   
		disable_control_action(PLAYER_CONTROL, INPUT_JUMP) 
	endif 

	inspect_body_0_system()
	
	inspect_body_1_system()
	
	find_bodies_dialogue_system()
	
	give_player_weapon_component_system()
	
	return false 
	

endfunc 


//OPTIMISE TO NEW DOOR SYSTEM
func bool elevator_door_system()

	if not has_label_been_triggered("lift0")
		if not is_any_text_being_displayed(locates_data)
			if is_entity_in_angled_area(player_ped_id(), <<246.014, -1370.986, 23.537>>, <<248.076, -1372.730, 26.537>>, 3.2)
				if create_conversation(scripted_speech, "FBI1AUD", "lift0", CONV_PRIORITY_low) 
					set_label_as_triggered("lift0", true)
				endif 
			endif 
		endif 
	endif 
	
	if not is_ped_injured(cia_enemy[9].ped)
		if not has_label_been_triggered("cia_attak4")
			//if (left_door_pos < -0.5) and (right_door_pos > 0.5)
			if ((DOOR_SYSTEM_GET_OPEN_RATIO(fbi_door[8]) < -0.5) and (DOOR_SYSTEM_GET_OPEN_RATIO(fbi_door[9]) > 0.5))
				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech, "FBI1AUD", "cia_attak4", CONV_PRIORITY_low)
						set_label_as_triggered("cia_attak4", true)
					endif 
				endif
			endif 
		endif 
	endif 

	if activate_elevator_door_system
		if move_doors_with_new_door_system(fbi_door[8], fbi_door[9], true, 0.5)
			return true 
		endif 
	endif 
	
	return false 
	
endfunc

func bool area_check_and_timer()

	if not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4)  
		if lk_timer(attack_time, 5000)
		
			return true 
			
		endif 

	else 
	
		if attack_time = 0
			attack_time = get_game_timer()
		endif 
		
	endif 
	
	return false 

endfunc 

proc cia_enemy_ai_system()

	int i = 0
	
	for i = 0 to count_of(cia_enemy) - 1
	
		if not is_ped_injured(cia_enemy[i].ped)

			switch cia_enemy_ai_status[i]
			
				case attack_phase_1
				
					switch i
					
						case 0

							//cia_enemy_cover_point[0] = cia_enemy_cover_point[0]
							
							REGISTER_TARGET(cia_enemy[i].ped, player_ped_id())
						
							open_sequence_task(seq)
								task_put_ped_directly_into_cover(null, cia_enemy[i].run_to_pos, 2000, false, 0.25, false, false, cia_enemy_cover_point[0], true) 
							close_sequence_task(seq)
							task_perform_sequence(cia_enemy[i].ped, seq)
							clear_sequence_task(seq)

							//force_ped_motion_state(cia_enemy[i].ped, ms_on_foot_run)
							
//							SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_BURSTS_IN_COVER, -1) //10
//							SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_PEEKS, 3.0) //
							
							//SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ForcePeekFromCover, TRUE) 
							
							//SET_PED_CAN_PEEK_IN_COVER(cia_enemy[i].ped, TRUE)
							
							cia_enemy_ai_status[i] = get_to_pos_1

						break 
						
						case 1
						
							task_aim_gun_at_coord(cia_enemy[i].ped, <<270.7, -1343.9, 25.3>>, -1)
							
							cia_enemy_ai_status[i] = get_to_pos_1
		
						break
						
						case 2
						
							task_aim_gun_at_coord(cia_enemy[i].ped, <<270.7, -1343.9, 25.3>>, -1)
							
							cia_enemy_ai_status[i] = get_to_pos_1

						break 
						
						case 3
						
							task_aim_gun_at_coord(cia_enemy[i].ped, <<270.7, -1343.9, 25.3>>, -1)
							
							cia_enemy_ai_status[i] = get_to_pos_1
		
						break 
						
						case 4
												
							open_sequence_task(seq)
								task_put_ped_directly_into_cover(null, <<262.68463, -1358.72913, 23.53772>>, -1)
							close_sequence_task(seq)
							task_perform_sequence(cia_enemy[i].ped, seq)
							clear_sequence_task(seq)
							
							cia_enemy_ai_status[i] = get_to_pos_1
							
						break 
						
						case 5
						
							//if is_entity_in_angled_area(player_ped_id(), <<268.798, -1358.956, 23.247>>, <<252.790, -1345.520, 26.72>>, 5.0)    
							if is_ped_injured(cia_enemy[6].ped)
							or is_entity_in_angled_area(player_ped_id(), <<256.101, -1364.552, 23.538>>, <<252.639, -1361.677, 26.538>>, 2.2)     

								open_sequence_task(seq)
									task_pause(null, (50 * get_random_int_in_range(1, 2)))//1000 
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
								cia_enemy_ai_status[i] = get_to_pos_1
								
							endif 
			
						break 
						
						case 6
			
							task_aim_gun_at_coord(cia_enemy[i].ped, <<270.7, -1343.9, 25.3>>, -1)
							
							cia_enemy_ai_status[i] = get_to_pos_1
						
						break 
						
						case 7
						
							task_aim_gun_at_coord(cia_enemy[7].ped, <<268.82, -1343.19, 25.13>>, -1)
						
							cia_enemy_ai_status[i] = attack_phase_2

						break
						
						case 8
						
							task_aim_gun_at_coord(cia_enemy[i].ped, <<270.7, -1343.9, 25.3>>, -1)
							
							cia_enemy_ai_status[i] = get_to_pos_1

						break
						
						case 9
						
							stop_ped_speaking(cia_enemy[i].ped, true) //fix for ped being blipped by blipping system

							task_aim_gun_at_entity(cia_enemy[i].ped, player_ped_id(), -1)
							
							cia_enemy_ai_status[i] = get_to_pos_1
							
						break
						
						//linked with ped 10 and 14
						case 10
						
							if is_ped_injured(cia_enemy[2].ped)
							or is_entity_in_angled_area(player_ped_id(), <<267.960, -1350.535, 23.538>>, <<264.483, -1347.523, 26.538>>, 2.0)
							or is_entity_in_angled_area(player_ped_id(), <<259.288, -1353.374, 23.536>>, <<250.221, -1345.979, 28.536>>, 2.0)
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
							
							endif 
						
						break 
			
						case 11

							if is_entity_in_angled_area(player_ped_id(), <<236.294, -1369.881, 25.801>> , <<237.938, -1371.343, 29.301>>, 5.000)//4.0   
							or is_entity_in_angled_area(player_ped_id(), <<234.600, -1371.647, 26.095>>, <<238.332, -1374.823, 29.095>>, 2.200)     
		
								set_ped_capsule(cia_enemy[i].ped, 0.50)
							
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, cia_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
								start_audio_scene("FBI_1_AGENT_BURSTS_THROUGH_DOOR") 
								
								cia_enemy_ai_status[i] = get_to_pos_1
							
							endif 
							
						break 
						
						case 12
									
							if is_entity_in_angled_area(player_ped_id(), <<255.901, -1363.158, 28.578>>, <<257.557, -1364.607, 32.078>>, 3.0) //2.0 
							or is_entity_in_angled_area(player_ped_id(), <<253.856, -1358.690, 28.654>>, <<257.135, -1361.471, 31.654>>, 2.1)  
							or has_ped_been_harmed(cia_enemy[i].ped, cia_enemy[i].health)
								
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, cia_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
								cia_enemy_ai_status[i] = get_to_pos_1
								
							endif 
						
						break 
						
						case 13
						
							if is_entity_in_angled_area(player_ped_id(),<<244.371, -1367.501, 28.648>>, <<242.762, -1366.151, 31.648>>, 1.4) 

//								open_sequence_task(seq)
//									task_pause(null, 1000)
//								close_sequence_task(seq)
//								task_perform_sequence(cia_enemy[i].ped, seq)
//								clear_sequence_task(seq)
								
								cia_enemy_ai_status[i] = get_to_pos_1
						
							endif 
						
						break 
						
						case 14
						
							//same as 10
							if is_ped_injured(cia_enemy[2].ped)
							or is_entity_in_angled_area(player_ped_id(), <<267.960, -1350.535, 23.538>>, <<264.483, -1347.523, 26.538>>, 2.0)
							or is_entity_in_angled_area(player_ped_id(), <<259.288, -1353.374, 23.536>>, <<250.221, -1345.979, 28.536>>, 2.0)
					
								open_sequence_task(seq)
									task_pause(null, 2000)
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
								cia_enemy_ai_status[i] = get_to_pos_1
							
							endif 
							
						break
						
						case 15

							if is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4) 
							
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, cia_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 4.0) 
									task_aim_gun_at_entity(null, player_ped_id(), 7500)
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)
								
							else 
							
								open_sequence_task(seq)
									task_pause(null, 500)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, cia_enemy[i].run_to_pos, player_ped_id(), pedmove_run, false, 0.5, 4.0) 
									task_aim_gun_at_entity(null, player_ped_id(), 7500)
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)
							
							endif 
							
							cia_enemy_ai_status[i] = get_to_pos_1
						
						break 
						
						case 16
						
							open_sequence_task(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, cia_enemy[i].run_to_pos, player_ped_id(), 1.5, false, 0.5, 0.8) 
								task_put_ped_directly_into_cover(null, <<280.03, -1344.69, 23.53>>, -1, true)
							close_sequence_task(seq)
							task_perform_sequence(cia_enemy[i].ped, seq)
							clear_sequence_task(seq)
							
							SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 10.00) //10.00
							SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_PEEKS, 5.0) 
							
							cia_enemy_ai_status[i] = get_to_pos_1
					
						break 
						
					endswitch 
					
				break 
				
				case get_to_pos_1
				
					switch i 
					
						case 0

						
							//SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ForcePeekFromCover, TRUE) 
							//SET_PED_CAN_PEEK_IN_COVER(cia_enemy[i].ped, TRUE)

							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							or has_ped_been_harmed(cia_enemy[i].ped, cia_enemy[i].health)
							or (has_player_antagonised_ped(cia_enemy[i].ped, 10.0) and is_ped_in_cover(cia_enemy[i].ped, true))
							or (is_entity_in_angled_area(player_ped_id(), <<276.603, -1347.275, 23.248>>, <<279.499, -1343.830, 26.748>>, 2.0) and is_ped_in_cover(cia_enemy[i].ped, true))

//								SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.5)
//								SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_PEEKS, 10.0)
//								//SET_PED_COMBAT_MOVEMENT(cia_enemy[i].ped, CM_STATIONARY)

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 8.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)

								//set_ped_sphere_defensive_area(cia_enemy[i].ped, <<266.4477, -1342.3348, 23.5380>>, 4.0, false, true)
								task_combat_ped(cia_enemy[i].ped, player_ped_id())
							
								cia_enemy_ai_status[i] = do_nothing
							
							endif 
						
						break
						
						case 1
						
							//if is_entity_in_angled_area(player_ped_id(), <<276.603, -1347.275, 23.248>>, <<279.499, -1343.830, 26.748>>, 1.0)
							//if is_entity_in_angled_area(player_ped_id(), <<274.059, -1345.187, 23.538>>, <<276.915, -1341.710, 26.538>>, 4.0)
							if is_entity_in_angled_area(player_ped_id(), <<272.711, -1344.186, 23.538>>, <<275.649, -1340.646, 26.538>>, 1.6)

								SET_PED_CHANCE_OF_FIRING_BLANKS(cia_enemy[i].ped, 0.5, 0.8) 
								
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 2
						
							SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, true) //added for bug 1320322
						
							if is_entity_in_angled_area(player_ped_id(), <<272.293, -1345.321, 23.247>>, <<268.848, -1342.426, 26.747>>, 2.0)   
							or (is_ped_injured(cia_enemy[0].ped) and is_ped_injured(cia_enemy[1].ped))		
								
								if not has_label_been_triggered("cia_attak2")
									//if not is_any_text_being_displayed(locates_data)
										if create_conversation(scripted_speech, "FBI1AUD", "cia_attak2", CONV_PRIORITY_medium)
											set_label_as_triggered("cia_attak2", true)
										endif 
									//endif 
								endif 

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_DISABLE_TACTICAL_POINTS_WITHOUT_CLEAR_LOS, true) //fix for bug 1468473
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)

								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 3
	
							SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, true)
							SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ForceEnableFlashLightForAI, TRUE) 

							if is_ped_injured(cia_enemy[1].ped)
							or is_entity_in_angled_area(player_ped_id(), <<271.829, -1345.845, 23.538>>, <<268.325, -1342.712, 26.538>>, 1.3)
							//if is_entity_in_angled_area(player_ped_id(), <<270.715, -1341.039, 23.538>>, <<274.274, -1342.370, 26.538>>, 6.0)
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_DISABLE_TACTICAL_POINTS_WITHOUT_CLEAR_LOS, true) //fix for bug 1468473
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 4
						
							if has_ped_been_harmed(cia_enemy[i].ped, cia_enemy[i].health)
							or is_entity_in_angled_area(player_ped_id(), <<268.439, -1349.881, 23.247>>, <<255.876, -1339.339, 26.748>>, 2.0)

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 4.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_DISABLE_TACTICAL_POINTS_WITHOUT_CLEAR_LOS, true) //fix for bug 1468473
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
								
						break 
						
						case 5
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
		
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break
						
						
						case 6
						
							if is_entity_in_angled_area(player_ped_id(), <<268.798, -1358.956, 23.247>>, <<252.790, -1345.520, 26.72>>, 10.0) //8.5 8.0   
							or is_entity_in_angled_area(player_ped_id(), <<260.849, -1351.601, 23.255>>, <<257.326, -1348.643, 26.355>>, 26.300)    
								
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_DISABLE_TACTICAL_POINTS_WITHOUT_CLEAR_LOS, true) //fix for bug 1468473
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 8
						
							if is_entity_in_angled_area(player_ped_id(), <<260.849, -1351.601, 23.255>>, <<257.326, -1348.643, 26.355>>, 26.300)    
							or is_entity_in_angled_area(player_ped_id(), <<259.076, -1359.946, 23.248>>, <<256.168, -1357.500, 26.748>>, 2.0) 

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = attack_phase_2

							endif 
								
						break 
						
						case 9

							if is_entity_in_angled_area(player_ped_id(), <<252.429, -1367.320, 23.247>>, <<250.590, -1365.777, 26.72>>, 7.0)  
								
								//cia_enemy[i].blip = create_blip_for_ped(cia_enemy[i].ped, true)
								
								stop_ped_speaking(cia_enemy[i].ped, false)
								
								task_aim_gun_at_entity(cia_enemy[i].ped, player_ped_id(), -1)

								play_sound_from_coord(lift_sound, "FBI_01_DMW_LIFT_ARRIVE", cia_enemy[i].pos, "FBI_01_SOUNDS")
								
								activate_elevator_door_system = true
								
								cia_enemy_ai_status[i] = attack_phase_2
								
							endif 

						break 
						
						case 13
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 1.5) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 	
							
						break 
						
						case 11
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) //1.0
								task_combat_ped(cia_enemy[i].ped, player_ped_id())
								
								cia_enemy_ai_status[i] = attack_phase_2

							else 
							
								//stops peds gun and hand clipping through the door
								set_ped_capsule(cia_enemy[i].ped, 0.50)
							
							endif 			
						
							if not has_label_been_triggered("cia_attak3")
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "FBI1AUD", "cia_attak3", CONV_PRIORITY_medium)
										set_label_as_triggered("cia_attak3", true)
									endif 
								endif 
							endif 
						
						break 
						
						case 12

							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							
								if not has_label_been_triggered("cia_attak1")
									if not is_any_text_being_displayed(locates_data)
										if create_conversation(scripted_speech, "FBI1AUD", "cia_attak1", CONV_PRIORITY_medium)
											set_label_as_triggered("cia_attak1", true)
										endif 
									endif 
								endif 
						
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
	
								cia_enemy_ai_status[i] = do_nothing
								
							endif  
						
						break 
						
						case 14
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							
								SET_PED_CHANCE_OF_FIRING_BLANKS(cia_enemy[i].ped, 0.5, 0.8) 
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 			
						
						break
						
						case 15
						
//							weapon_type players_weapon 
//							players_weapon = weapontype_unarmed
//							get_current_ped_weapon(player_ped_id(), players_weapon)
						
							if is_entity_at_coord(cia_enemy[i].ped, cia_enemy[i].run_to_pos, <<2.0, 2.0, 2.0>>)
								if not has_label_been_triggered("cia_attak0")
									if not is_any_text_being_displayed(locates_data)
										if create_conversation(scripted_speech, "FBI1AUD", "cia_attak0", CONV_PRIORITY_medium)
											set_label_as_triggered("cia_attak0", true)
										endif 
									endif 
								endif 
							endif 
							
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							//or not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4)  
							or has_ped_been_harmed(cia_enemy[15].ped, cia_enemy[15].health)
							or has_ped_been_harmed(cia_enemy[16].ped, cia_enemy[16].health)
							or is_player_targeting_ped(cia_enemy[15].ped)
							or is_player_free_aiming_at_entity(player_id(), cia_enemy[15].ped)
							or is_ped_shooting(player_ped_id())
							//or (players_weapon != weapontype_unarmed and CAN_PED_SEE_HATED_PED(cia_enemy[i].ped, player_ped_id()))
							//or get_distance_between_coords(get_entity_coords(cia_enemy[i].ped), get_entity_coords(player_ped_id())) < 2.0
							or (not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4)  and get_distance_between_coords(get_entity_coords(cia_enemy[i].ped), get_entity_coords(player_ped_id())) < 3.0)
	
								if not has_label_been_triggered("cia_attak0")
									if not is_any_text_being_displayed(locates_data)
										if create_conversation(scripted_speech, "FBI1AUD", "cia_attak0", CONV_PRIORITY_medium)
											set_label_as_triggered("cia_attak0", true)
										endif 
									endif 
								endif 

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
							
							endif 

						break 
						
						
						case 16
						
							weapon_type players_weapon 
							players_weapon = weapontype_unarmed
							get_current_ped_weapon(player_ped_id(), players_weapon)
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							//or get_distance_between_coords(get_entity_coords(cia_enemy[i].ped), get_entity_coords(player_ped_id())) < 2.0
							or not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4)  
							or has_ped_been_harmed(cia_enemy[15].ped, cia_enemy[15].health)
							or has_ped_been_harmed(cia_enemy[16].ped, cia_enemy[16].health)
							or is_player_targeting_ped(cia_enemy[i].ped)
							//or is_player_free_aiming_at_entity(player_id(), cia_enemy[i].ped)
							//or (players_weapon != weapontype_unarmed and CAN_PED_SEE_HATED_PED(cia_enemy[i].ped, player_ped_id()))
							or is_ped_shooting(player_ped_id())
		
								SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.5)
								SET_COMBAT_FLOAT(cia_enemy[i].ped, CCF_TIME_BETWEEN_PEEKS, 10.0)
								SET_PED_COMBAT_MOVEMENT(cia_enemy[i].ped, CM_STATIONARY)

								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
								
								cia_enemy_ai_status[i] = do_nothing
							
							endif 

						break 
						
//						case 17
//						
//						break 
					
					endswitch 
				
				break
				
				case attack_phase_2
				
					switch i 
					
						case 1
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 8.0) 
								set_ped_sphere_defensive_area(cia_enemy[i].ped, <<271.9024, -1359.9326, 23.5377 >>, 8.0, false, true) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 2
						case 6
						
//							if not has_label_been_triggered("fbi1_ciawpn")
//								if create_conversation(scripted_speech, "fbi1aud", "fbi1_ciawpn", CONV_priority_medium)
//									set_label_as_triggered("fbi1_ciawpn", true)
//									script_assert("test DIALOGUE")
//								endif 
//							endif 
						
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
					
						case 7
						
							if is_entity_in_angled_area(player_ped_id(), <<260.849, -1351.601, 23.255>>, <<257.326, -1348.643, 26.355>>, 26.300)    
							or is_entity_in_angled_area(player_ped_id(), <<259.076, -1359.946, 23.248>>, <<256.168, -1357.500, 26.748>>, 2.0) 
							or is_entity_in_angled_area(player_ped_id(), <<273.560, -1343.790, 23.538>>, <<270.113, -1340.897, 26.538>>, 2.0)
							or has_ped_been_harmed(cia_enemy[i].ped, cia_enemy[i].health)
							//or has_player_antagonised_ped(cia_enemy[i].ped, 7.0)
							or IS_PLAYER_TARGETTING_ENTITY(player_id(), cia_enemy[i].ped)
							or is_player_free_aiming_at_entity(player_id(), cia_enemy[i].ped)
							
								open_sequence_task(seq)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, cia_enemy[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
								close_sequence_task(seq)
								task_perform_sequence(cia_enemy[i].ped, seq)
								clear_sequence_task(seq)

								cia_enemy_ai_status[i] = get_to_pos_2
							endif 
							
						break 
						
						case 8
						
							if is_ped_injured(cia_enemy[9].ped)
								
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								
								cia_enemy_ai_status[i] = do_nothing
							endif 
						
						break 
					
						case 9
						
							if elevator_door_system()
							
								set_ped_reset_flag(cia_enemy[i].ped, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, true)
							
								SET_PED_CHANCE_OF_FIRING_BLANKS(cia_enemy[i].ped, 0.5, 0.8) 
							
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)

								cia_enemy_ai_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 11
						
							if is_entity_in_angled_area(player_ped_id(), <<236.687, -1369.182, 26.866>>, <<238.485, -1370.617, 30.466>>, 1.3)
						
								REMOVE_PED_DEFENSIVE_AREA(cia_enemy[i].ped)
								set_ped_combat_movement(cia_enemy[i].ped, CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
					endswitch 
			   
			    break 
			   
			    case get_to_pos_2
				
					switch i 
					
						case 7
				
							if has_char_task_finished_2(cia_enemy[i].ped, script_task_perform_sequence)
									
								set_blocking_of_non_temporary_events(cia_enemy[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy[i].ped, cia_enemy[i].run_to_pos, 2.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy[i].ped, CA_CAN_CHARGE, TRUE)
								task_combat_hated_targets_around_ped(cia_enemy[i].ped, 200.00)
							
								cia_enemy_ai_status[i] = do_nothing
								
							endif 
							
						break 

					endswitch 
				
			    break 
				
			    case do_nothing 
				
					switch i 
					
						case 2
						
							SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, true)
						
						break 
						
						case 3

							SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, true)
							SET_PED_RESET_FLAG(cia_enemy[i].ped, PRF_ForceEnableFlashLightForAI, TRUE) 

						break 
					
						case 6
						
//							if not has_label_been_triggered("fbi1_ciawpn")
//								if create_conversation(scripted_speech, "fbi1aud", "fbi1_ciawpn", CONV_priority_medium)
//									set_label_as_triggered("fbi1_ciawpn", true)
//								endif 
//							endif 
						
						break 
						
						case 12
						
							if not has_label_been_triggered("cia_attak1")
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "FBI1AUD", "cia_attak1", CONV_PRIORITY_medium)
										set_label_as_triggered("cia_attak1", true)
									endif 
								endif 
							endif 
						
						break 
				
						case 15
						
							if not has_label_been_triggered("cia_attak0")
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "FBI1AUD", "cia_attak0", CONV_PRIORITY_medium)
										set_label_as_triggered("cia_attak0", true)
									endif 
								endif 
							endif 
							
						break 
						
					endswitch 
				
			    break 
	
			endswitch
			
		else 
		
			switch i 
			
				case 11
				
					if cia_enemy[i].created	
						if IS_AUDIO_SCENE_ACTIVE("FBI_1_AGENT_BURSTS_THROUGH_DOOR")
							STOP_AUDIO_SCENE("FBI_1_AGENT_BURSTS_THROUGH_DOOR")
						endif 
					endif 
				
				break 
				
			endswitch 
		
		endif 
	
	endfor 

endproc 

proc cia_enemy_top_floor_ai_system()

	int i = 0

	//is_entity_at_coord(player_ped_id(), cia_enemy_top_floor[1].run_to_pos, <<1.0, 1.0, 1.0>>)
	
	for i = 0 to count_of(cia_enemy_top_floor) - 1
	
		if not is_ped_injured(cia_enemy_top_floor[i].ped)

			switch cia_enemy_top_floor_ai_status[i]
			
				case attack_phase_1
				
					switch i
					
						case 0
						
							open_sequence_task(seq)
								task_put_ped_directly_into_cover(null, <<242.27132, -1367.46399, 38.53431>>, -1, false, 0, false, false, cia_enemy_top_floor_cover_point[0])
							close_sequence_task(seq)
							task_perform_sequence(cia_enemy_top_floor[i].ped, seq)
							clear_sequence_task(seq)
							
							force_ped_ai_and_animation_update(cia_enemy_top_floor[i].ped)
						
							cia_enemy_top_floor_ai_status[i] = get_to_pos_1

						break 
						
						case 1
						
//							if is_entity_in_angled_area(player_ped_id(), <<247.127, -1367.785, 38.540>>, <<249.778, -1364.398, 42.040>>, 3.000)    
//								script_assert("test 0")
//							endif 
//							
//							if is_entity_in_angled_area(player_ped_id(), <<244.069, -1383.662, 38.542>>, <<246.962, -1380.216, 42.042>>, 2.000)    
//								script_assert("test 1")
//							endif 
//							
//							if is_entity_in_angled_area(player_ped_id(), <<247.172, -1369.088, 38.537>>, <<249.700, -1371.208, 42.042>>, 2.0)   
//								script_assert("test 2")
//							endif 
						
						
							if is_entity_in_angled_area(player_ped_id(), <<247.127, -1367.785, 38.540>>, <<249.778, -1364.398, 42.040>>, 5.000)    
							or is_entity_in_angled_area(player_ped_id(), <<244.069, -1383.662, 38.542>>, <<246.962, -1380.216, 42.042>>, 2.000)    
							or is_entity_in_angled_area(player_ped_id(), <<247.172, -1369.088, 38.537>>, <<249.700, -1371.208, 42.042>>, 2.0)   
							or get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_Coords(cia_enemy_top_floor[i].ped)) < 8.0 

								set_blocking_of_non_temporary_events(cia_enemy_top_floor[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].run_to_pos, 1.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, false)//fix for 1419889
								task_combat_ped(cia_enemy_top_floor[i].ped, player_ped_id())
								
								//cia_enemy_top_floor_ai_status[i] = get_to_pos_1
								cia_enemy_top_floor_ai_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 2

							task_put_ped_directly_into_cover(cia_enemy_top_floor[i].ped, <<239.49130, -1379.23584, 38.53422>>, -1, false, 0, true, true, cia_enemy_top_floor_cover_point[2])  
							force_ped_ai_and_animation_update(cia_enemy_top_floor[i].ped, true)
							
							cia_enemy_top_floor_ai_status[i] = get_to_pos_1 
						
						break 
						
						case 3
						case 4
						
							set_blocking_of_non_temporary_events(cia_enemy_top_floor[i].ped, false)
							set_ped_sphere_defensive_area(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].run_to_pos, 2.0) 
							SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_CAN_CHARGE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, false)
							task_combat_ped(cia_enemy_top_floor[i].ped, player_ped_id())
							
							cia_enemy_top_floor_ai_status[i] = get_to_pos_1 
						
						break 

					endswitch 
					
				break 
				
				case get_to_pos_1
				
					switch i 
				
						case 0
						
							//if is_entity_in_angled_area(player_ped_id(), <<247.127, -1367.785, 38.540>>, <<249.778, -1364.398, 42.040>>, 2.000)    
							//or is_entity_in_angled_area(player_ped_id(), <<244.069, -1383.662, 38.542>>, <<246.962, -1380.216, 42.042>>, 2.000)    
							//or is_entity_in_angled_area(player_ped_id(), <<247.172, -1369.088, 38.537>>, <<249.700, -1371.208, 42.042>>, 2.0)   

							if is_entity_in_angled_area(player_ped_id(), <<250.167, -1365.738, 38.539>>, <<252.706, -1367.846, 41.039>>, 1.4)     
							or has_ped_been_harmed(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].health)
							or has_player_antagonised_ped(cia_enemy_top_floor[i].ped, 15.00)

								set_blocking_of_non_temporary_events(cia_enemy_top_floor[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].run_to_pos, 1.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, false)
								task_combat_ped(cia_enemy_top_floor[i].ped, player_ped_id())
							
								cia_enemy_top_floor_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 1
						
							if not has_label_been_triggered("cia_attak5")
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "FBI1AUD", "cia_attak5", CONV_PRIORITY_medium)
										set_label_as_triggered("cia_attak5", true)
									endif 
								endif 
							endif 
						
							if has_char_task_finished_2(cia_enemy_top_floor[i].ped, script_task_perform_sequence)
							
								set_blocking_of_non_temporary_events(cia_enemy_top_floor[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].run_to_pos, 1.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, false)
								task_combat_ped(cia_enemy_top_floor[i].ped, player_ped_id())
		
								cia_enemy_top_floor_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 2
						
							if is_entity_in_angled_area(player_ped_id(), <<252.643, -1367.767, 38.534>>, <<250.109, -1365.654,41.534>>, 1.3)     
							or has_ped_been_harmed(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].health)
							or has_player_antagonised_ped(cia_enemy_top_floor[i].ped, 15.00)
							
								set_blocking_of_non_temporary_events(cia_enemy_top_floor[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_top_floor[i].ped, cia_enemy_top_floor[i].run_to_pos, 1.0) 
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_CAN_CHARGE, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, false)
								task_combat_ped(cia_enemy_top_floor[i].ped, player_ped_id())
								
								cia_enemy_top_floor_ai_status[i] = do_nothing
							
							endif 

						break
						
						case 3
						case 4
						
							//incase player runs past guards before they reach their defensive sphere. 
							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(cia_enemy_top_floor[i].ped)) < 5
						
								REMOVE_PED_DEFENSIVE_AREA(cia_enemy_top_floor[i].ped)
								set_ped_combat_movement(cia_enemy_top_floor[i].ped, CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_top_floor[i].ped, CA_CAN_CHARGE, TRUE)
						
								cia_enemy_top_floor_ai_status[i] = do_nothing
								
							endif 
						
						break 
	
					endswitch 
					
				break
				
				case get_to_pos_2

				break 
				
				case do_nothing
				
					switch i 
					
						case 1
						
							if not has_label_been_triggered("cia_attak5")
								if not is_any_text_being_displayed(locates_data)
									if create_conversation(scripted_speech, "FBI1AUD", "cia_attak5", CONV_PRIORITY_medium)
										set_label_as_triggered("cia_attak5", true)
									endif 
								endif 
							endif
						
						break 
						
					endswitch 
				
				break 
	
			endswitch
			
		endif 
	
	endfor 

endproc 

proc cia_enemy_outside_ai_system()

	int i = 0

	for i = 0 to count_of(cia_enemy_outside) - 1
	
		if not is_ped_injured(cia_enemy_outside[i].ped)

			switch cia_enemy_outside_ai_status[i]
			
				case attack_phase_1
				
					switch i 
					
						case 0
						case 1
						case 2
						
							set_blocking_of_non_temporary_events(cia_enemy_outside[i].ped, false)
							set_ped_sphere_defensive_area(cia_enemy_outside[i].ped, cia_enemy_outside[i].run_to_pos, 2.0) 
							task_combat_ped(cia_enemy_outside[i].ped, player_ped_id())

							cia_enemy_outside_ai_status[i] = get_to_pos_1

						break 
						
						case 3

							if is_vehicle_driveable(police_car[0].veh)
								if is_playback_going_on_for_vehicle(police_car[0].veh)
									if ((get_time_position_in_recording(police_car[0].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[0].recording_number, "lkfbi1"))
									
										stop_playback_recorded_vehicle(police_car[0].veh)
	
										set_ped_combat_attributes(cia_enemy_outside[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										set_blocking_of_non_temporary_events(cia_enemy_outside[i].ped, false)
										set_ped_sphere_defensive_area(cia_enemy_outside[i].ped, cia_enemy_outside[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(cia_enemy_outside[i].ped, 200.00)
										set_ped_as_no_longer_needed(cia_enemy_outside[i].ped)
										set_model_as_no_longer_needed(cia_enemy_outside[i].model)
										cia_enemy_outside_ai_status[i] = do_nothing
																				
									endif 
								endif 
							
							else 
							
								set_ped_combat_attributes(cia_enemy_outside[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								set_blocking_of_non_temporary_events(cia_enemy_outside[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_outside[i].ped, cia_enemy_outside[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(cia_enemy_outside[i].ped, 200.00)
								set_ped_as_no_longer_needed(cia_enemy_outside[i].ped)
								set_model_as_no_longer_needed(cia_enemy_outside[i].model)
								cia_enemy_outside_ai_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 4

							if is_vehicle_driveable(police_car[0].veh)
								if not is_playback_going_on_for_vehicle(police_car[0].veh)
									
									open_sequence_task(seq)
										task_pause(null, 500)
									close_sequence_task(seq)
									task_perform_sequence(cia_enemy_outside[i].ped, seq)
									clear_sequence_task(seq)
									
									cia_enemy_outside_ai_status[i] = get_to_pos_1
												
								endif 
								
							else 
								
								set_ped_combat_attributes(cia_enemy_outside[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								set_blocking_of_non_temporary_events(cia_enemy_outside[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_outside[i].ped, cia_enemy_outside[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(cia_enemy_outside[i].ped, 200.00)
								set_ped_as_no_longer_needed(cia_enemy_outside[i].ped)
								set_model_as_no_longer_needed(cia_enemy_outside[i].model)
								cia_enemy_outside_ai_status[i] = do_nothing

							endif 
							
						break 
						
					endswitch 
				
				break 
				
				case get_to_pos_1
				
					switch i 
					
						case 0
						case 1
						case 2

							if is_entity_at_coord(cia_enemy_outside[i].ped, cia_enemy_outside[i].run_to_pos, <<4.0, 4.0, 4.0>>)
							
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_outside[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_ATTRIBUTES(cia_enemy_outside[i].ped, CA_CAN_CHARGE, TRUE)
								
								set_ped_as_no_longer_needed(cia_enemy_outside[i].ped)
								set_model_as_no_longer_needed(cia_enemy_outside[i].model)

								cia_enemy_outside_ai_status[i] = do_nothing
								
							endif 
							
						break 
						
						case 4
						
							if has_ped_task_finished_2(cia_enemy_outside[i].ped)
						
								set_ped_combat_attributes(cia_enemy_outside[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								set_blocking_of_non_temporary_events(cia_enemy_outside[i].ped, false)
								set_ped_sphere_defensive_area(cia_enemy_outside[i].ped, cia_enemy_outside[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(cia_enemy_outside[i].ped, 200.00)
								set_ped_as_no_longer_needed(cia_enemy_outside[i].ped)
								set_model_as_no_longer_needed(cia_enemy_outside[i].model)
								cia_enemy_outside_ai_status[i] = do_nothing
								
							endif 
						
						break 
						
					endswitch 
				
				break 
				
				case do_nothing
				
				break 
				
			endswitch 
			
		endif 
		
	endfor 

endproc 

proc fbi1_escape_cutscene()

	int i = 0
	
//	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//		
//		if fbi1_escape_cutscene_status < 3
//			fbi1_escape_cutscene_status  = 22
//		endif 
//
//	endif 
	
	//temp fix untill interior polys are fixed for bug 1169985 and mover is added to animation of jumping from ledge
//	if fbi1_escape_cutscene_status > 0
//		clear_room_for_entity(player_ped_id())
//	endif 

	switch fbi1_escape_cutscene_status
		
		case 0
		
			disable_on_foot_first_person_view_this_update()
		
			if is_synchronized_scene_running(cutscene_index)
				if get_synchronized_scene_phase(cutscene_index) >= 0.95
				
					if start_new_cutscene_no_fade()
					
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
						SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
					
						set_fake_wanted_level(3)

						for i = 0 to count_of(cia_enemy) - 1
							delete_ped(cia_enemy[i].ped)
							//set_model_as_no_longer_needed(cia_enemy[i].model)
							
							if does_blip_exist(cia_enemy[i].blip)
								remove_blip(cia_enemy[i].blip)
							endif 
						endfor 
						
						for i = 0 to count_of(cia_enemy_top_floor) - 1
							delete_ped(cia_enemy_top_floor[i].ped)
							//set_model_as_no_longer_needed(cia_enemy_top_floor[i].model)
							
							if does_blip_exist(cia_enemy_top_floor[i].blip)
								remove_blip(cia_enemy_top_floor[i].blip)
							endif 
						endfor 
						
						for i = 0 to count_of(doctor) - 1
							delete_ped(doctor[i].ped)
							set_model_as_no_longer_needed(doctor[i].model)
						endfor 
						
						for i = 0 to count_of(dead_body) - 1
							delete_ped(dead_body[i].ped)
							set_model_as_no_longer_needed(dead_body[i].model)
						endfor 
						
						for i = 0 to count_of(bin_bag) - 1
							delete_object(bin_bag[i].obj)
							set_model_as_no_longer_needed(bin_bag[i].model)
						endfor 
						
						set_object_as_no_longer_needed(clip_board.obj)
						set_model_as_no_longer_needed(clip_board.model)
						
						set_object_as_no_longer_needed(pen.obj)
						set_model_as_no_longer_needed(pen.model)
				
						clear_area(<<233.4198, -1355.4689, 29.5590>>, 200.0, true)

						prepare_music_event("FBI1_JUMP")
						
						START_AUDIO_SCENE("FBI_1_JUMP")
				
						REMOVE_SCENARIO_BLOCKING_AREAS()

						for i = 0 to count_of(ambient_car) - 1
						
							if does_entity_exist(ambient_car[i].veh)
								if not is_entity_at_coord(ambient_car[i].veh, ambient_car[i].pos, <<5.0, 5.0, 5.0>>, false)
									set_entity_coords(ambient_car[i].veh, ambient_car[i].pos, false)
									set_entity_heading(ambient_car[i].veh, ambient_car[i].heading)
								endif 
								set_vehicle_as_no_longer_needed(ambient_car[i].veh)
								set_model_as_no_longer_needed(ambient_car[i].model)
							endif 
							
						endfor 
						
						if does_entity_exist(window.obj)
							break_entity_glass(window.obj, <<236.05, -1357.488, 40.02953>>, 0.3, <<-0.6866288, 0.6827963, -0.2496601>>, 500, 2, true)
						endif 
						
						CLEAR_ROOM_FOR_GAME_VIEWPORT()
						clear_room_for_entity(player_ped_id())
						
						CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(false)

						cutscene_pos = <<236.344, -1356.722, 39.304>>
						cutscene_rot = <<0.000, 0.000, -40.680>>
						cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
						//TASK_SYNCHRONIZED_SCENE(player_ped_id(), cutscene_index, "missfbi1", "ledge_jump", normal_BLEND_IN, slow_BLEND_OUT, synced_scene_none, rbf_none, normal_blend_in)//, SYNCED_SCENE_USE_PHYSICS)
						TASK_SYNCHRONIZED_SCENE(player_ped_id(), cutscene_index, "missfbi1", "ledge_jump", instant_BLEND_IN, slow_BLEND_OUT)//, SYNCED_SCENE_USE_PHYSICS)
						force_ped_ai_and_animation_update(player_ped_id())
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(bin[0].obj, cutscene_index, "ledge_jump_Bin", "missfbi1", instant_BLEND_IN) 
						force_entity_ai_and_animation_update(bin[0].obj)
						
						camera_a = CREATE_CAMERA(camtype_animated, true)
						play_cam_anim(camera_a, "ledge_jump_cam", "missfbi1", cutscene_pos, cutscene_rot)
							
						render_script_cams(true, false)
						
						if IS_AUDIO_SCENE_ACTIVE("FBI_1_AGENT_BURSTS_THROUGH_DOOR")
							STOP_AUDIO_SCENE("FBI_1_AGENT_BURSTS_THROUGH_DOOR")
						endif 
						
						if IS_AUDIO_SCENE_ACTIVE("FBI_1_ESCAPE_BASE_SCENE")
							STOP_AUDIO_SCENE("FBI_1_ESCAPE_BASE_SCENE")
						endif 
						
						if IS_AUDIO_SCENE_ACTIVE("FBI_1_AIM_AT_WINDOW")
							STOP_AUDIO_SCENE("FBI_1_AIM_AT_WINDOW")
						endif 
					
						fbi1_escape_cutscene_status++
						
					endif 
					
				endif 
				
			endif 
		
		break 
		
		case 1
		
			if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.09 //0.115
				
//					set_cam_active(camera_a, false)
//					set_cam_active(camera_b, false)
//					
//					spline_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
//					
//					ADD_CAM_SPLINE_NODE(spline_cam, <<235.931274,-1356.435059,42.545235>>,<<-75.071808,2.145942,83.985275>>, 0)    
//					ADD_CAM_SPLINE_NODE(spline_cam, <<236.183426,-1355.823242,35.880280>>,<<-60.726868,2.145942,84.420143>>, 2000)
//					ADD_CAM_SPLINE_NODE(spline_cam, <<237.374268,-1354.117065,34.712608>>,<<-34.718250,2.145941,124.594528>>, 6000) //1500
//						
//					set_cam_fov(spline_cam, 32.799644) 
//					
//					set_cam_active(spline_cam, true)
	
					fbi1_escape_cutscene_status++
					
				endif 
			endif 
		
		break 
		
		case 2
		
			if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.21
				
					//play_sound_from_entity(land_on_truck_sound, "FBI_01_DMW_FLATBED_LAND", player_ped_id())
					
					play_sound_from_entity(-1, "FBI_01_DMW_GARBAGE_LAND", player_ped_id(), "FBI_01_DMW_02_SOUNDS")  
					
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 200)
					
					//start_particle_fx_non_looped_at_coord("scr_fbi1_litter", get_offset_from_entity_in_world_coords(bin[0].obj, <<0.0, 0.0, 1.0>>), <<0.0, 0.0, 0.0>>)
					start_particle_fx_non_looped_at_coord("scr_fbi1_litter", <<233.5, -1355.5, 31.3>>, <<0.0, 0.0, 0.0>>)
				
					APPLY_PED_DAMAGE_PACK(player_ped_id(), "SCR_Dumpster", 0.0, 1.0)

//					if is_vehicle_driveable(players_truck.veh)
//						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_fbi1_paper_landing", players_truck.veh, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//					endif 

					fbi1_escape_cutscene_status++
					
				endif 
			endif 
		
		break 
		
		case 3
		
			if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.46
			
//					set_cam_active(spline_cam, false)
//					
//					destroy_cam(spline_cam)
//
//					spline_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
//					
//					ADD_CAM_SPLINE_NODE(spline_cam, <<237.385590,-1354.566162,33.550797>>,<<-20.611015,2.149386,113.411957>>, 0)    
//					ADD_CAM_SPLINE_NODE(spline_cam, <<237.446762,-1354.415771,33.265747>>,<<-14.840520,2.149385,103.458557>>, 1500)
//					//ADD_CAM_SPLINE_NODE(spline_cam, <<238.240799,-1352.943604,32.459808>>,<<-31.116749,2.145944,127.979942>>, 3000) //1500
//					ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(spline_cam, 3000)
//						
//					set_cam_fov(spline_cam, 50.00) 
//					
//					set_cam_active(spline_cam, true)
					
					trigger_music_event("FBI1_LAND_TRUCK")
					
//					set_cam_active(camera_g, true)
//					set_cam_active_with_interp(camera_h, camera_g, 5500, graph_type_linear)
					
//					original_time = get_game_timer()

					fbi1_escape_cutscene_status++
					
				endif 
				
			endif 
			
		break 
		
		case 4
		
			if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.95
				
					request_model(franklin.model)
					request_model(cia_enemy_outside[1].model)						
					request_model(cia_enemy_outside[3].model)
					request_model(police_car[0].model)
					request_model(franklins_car.model)
					set_vehicle_model_is_suppressed(franklins_car.model, true)
					request_anim_dict("MISSFBI1LEADINOUTFBI_1_EXT")
					request_vehicle_recording(police_car[0].recording_number, "lkfbi1")

					fbi1_escape_cutscene_status++
					
				endif 
				
			endif 
		
		break 
		
		case 5
		
//			SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(player_ped_id())
//			
//			set_gameplay_cam_relative_heading(0.0)
		

			if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
				if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 1.0
				
					REPLAY_STOP_EVENT()

					NEW_LOAD_SCENE_STOP()

					CASCADE_SHADOWS_INIT_SESSION()

					remove_anim_dict("missfbi1")

					set_time_scale(1.0)
					
					set_fake_wanted_level(0)
					
					set_max_wanted_level(3)
					set_create_random_cops(false)
					set_wanted_level_multiplier(0.5)
					
					set_player_wanted_level(player_id(), 3)
					set_player_wanted_level_now(player_id())
					SET_WANTED_LEVEL_DIFFICULTY(player_id(), 0)
					SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI1_COP_LOSS_TIME) 
							
					DISTANT_COP_CAR_SIRENS(false)
						
//					setup_radio_station()

					original_time = 0 //set timer to spawn enemies in.

					set_entity_proofs(player_ped_id(), false, false, false, false, false)
					
					end_cutscene_no_fade(true, false)
					
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					
					SET_PED_USING_ACTION_MODE(player_ped_id(), false)
					SET_PED_STEALTH_MOVEMENT(player_ped_id(), false)
					
					print_now("LOSE_WANTED", default_god_text_time, 1)
					
					remove_ped_for_dialogue(scripted_speech, 3)
					remove_ped_for_dialogue(scripted_speech, 4)
					remove_ped_for_dialogue(scripted_speech, 5)
					remove_ped_for_dialogue(scripted_speech, 6)
					remove_ped_for_dialogue(scripted_speech, 7)
					remove_ped_for_dialogue(scripted_speech, 8)
					
					if is_valid_interior(coroner_interior)
						unpin_interior(coroner_interior)
					endif 
					
					INSTANTLY_FILL_VEHICLE_POPULATION()
					
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_FIB_1_01", 0.0)
					
					trigger_music_event("FBI1_LOSE_COPS_START")
					
					if IS_AUDIO_SCENE_ACTIVE("FBI_1_JUMPR")
						STOP_AUDIO_SCENE("FBI_1_JUMP")
					endif 
					
					start_audio_scene("FBI_1_LOSE_COPS")
					
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "outside coroners escape police", true)
					
					//force cleanup will overwrite the weapon globals that the checkpoint data was stored in with the original 
					//weapons that were stored at the begining of the mission. Therefore we need to store it here.
					GET_PED_WEAPONS(player_ped_id(), fbi1_player_stored_weapons)
					
					REPLAY_STOP_EVENT()
					
					//mission_flow = lose_cops
					mission_flow = get_to_hills
					
					SET_ALL_RANDOM_PEDS_FLEE(player_id(), true)

					//**********debug
					//fbi1_escape_cutscene_status++
					
				endif 

			endif 
		
		break 

//		case 22
//		
//			if is_screen_faded_out()
//				if is_cutscene_active()
//					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)) 
//
//						CLEAR_ROOM_FOR_GAME_VIEWPORT()
//						clear_room_for_entity(player_ped_id())
//
//						cutscene_pos = << 236.235, -1356.587, 39.257 >>
//						cutscene_rot = << 0.000, 0.000, -41.000 >>
//						cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
//						TASK_SYNCHRONIZED_SCENE(player_ped_id(), cutscene_index, "missfbi1", "ledge_look", INSTANT_BLEND_IN, normal_BLEND_OUT)//, SYNCED_SCENE_USE_PHYSICS)
//
//						set_cam_active(camera_e, true)
//						set_cam_active_with_interp(camera_f, camera_e, 5500, graph_type_linear)
//						
//						render_script_cams(true, false)
//						
//						shake_cam(camera_f, "HAND_SHAKE", 1.0)
//						
//						original_time = get_game_timer()
//						
//						do_screen_fade_in(default_fade_time)
//						
//						fbi1_escape_cutscene_status = 3
//						
//					endif 
//				endif 
//			endif 
//
//		break 
		
	endswitch 

endproc 

proc special_ability_help_text_system()

	switch special_ability_help_text_system_status 
	
		case 0
		
			//if is_ped_injured(cia_enemy[15].ped)
			//or is_entity_in_angled_area(player_ped_id(),<<284.213, -1347.350, 23.247>>, <<280.730, -1344.500, 26.747>>, 2.000)    
			if is_entity_in_angled_area(player_ped_id(), <<273.212, -1344.521, 23.538>>, <<276.105, -1341.074, 26.538>>, 2.0)
				
				SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				SPECIAL_ABILITY_FILL_METER(player_id(), true)
				SPECIAL_ABILITY_CHARGE_NORMALIZED(player_id(), 1.0, true)
				
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					print_help("FBI1_help4_KM", 12500)
				ELSE
					print_help("FBI1_help4", 12500)
				ENDIF
				FLASH_ABILITY_BAR(DEFAULT_HELP_TEXT_TIME)
				
				special_ability_help_text_system_status++
			endif 
		
		break 
		
		case 1
		
			if not is_help_message_being_displayed()
				help_time = get_game_timer() 
				special_ability_help_text_system_status++
			endif 

		break 
		
		case 2
		
			if lk_timer(help_time, 2000)
				print_help("FBI1_help5", 12000)
				special_ability_help_text_system_status++
			endif 
		
		break 
		
		case 3
		
		break 
		
	endswitch 

endproc 

proc create_2_cia_enemy_top_floor_peds()
			
	if not cia_enemy_top_floor[3].created

		int dead_count = 0
		int k = 0
		
		vector player_pos
	
		for k = 0 to 2
			
			if does_entity_exist(cia_enemy_top_floor[k].ped)
				
				if is_ped_injured(cia_enemy_top_floor[k].ped)
					dead_count++
				endif
				
			else 
				
				if cia_enemy_top_floor[k].created
					dead_count++
				endif
				
			endif 
			
		endfor
		
		if dead_count >= 2
		
			player_pos = get_entity_coords(player_ped_id())
			
			if player_pos.z > 38.00
			
				setup_enemy(cia_enemy_top_floor[3])
				setup_enemy(cia_enemy_top_floor[4])
				
			endif 
			
		endif 
		
	endif 

endproc 

proc break_entity_glass_system()
					
	switch break_entity_glass_system_status 
	
		case 0
		
			break_entity_glass_system_status++
		
		break 
		
		case 1
			
			break_entity_glass(window.obj, <<236.05, -1357.488, 40.02953>>, 0.3, <<-0.6866288, 0.6827963, -0.2496601>>, 700, 3, false) //500
			
			break_entity_glass(window.obj, <<235.8064, -1357.778, 40.1281>>, 0.4, <<-0.6795667, 0.7014526, -0.2148329>>, 500, 3, false) //500
			
			break_entity_glass(window.obj, <<236.3226, -1357.163, 39.7619>>, 0.5, <<-0.376403, 0.8627338, -0.3376553>>, 500, 3, false)   
			
			break_entity_glass_system_status++

			break_the_glass_window = true
			
		break 
		
	endswitch 
	
endproc 


func bool fb1_shootout_master_controller_system()

	int i = 0

	ped_structure_are_all_enemies_dead(cia_enemy)
	
	ped_structure_are_all_enemies_dead(cia_enemy_top_floor)
	
	if does_blip_exist(doctor[3].blip)
		printstring("doctor 3 blip exists")
		printnl()
	else 
		printstring("doctor 3 blip DOES NOT exists")
		printnl()
	endif 

	switch fbi1_master_controller_status
	
		case 0

//			if (dave_norton_phone_call_finished and is_sphere_visible(<<284.24, -1343.17, 25.33>>, 4.0))
//			or not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4)  
//			or lk_timer(shootout_time, 40000)

			if ((dave_norton_phone_call_finished) and ((is_sphere_visible(<<284.24, -1343.17, 25.33>>, 4.0)) or (not is_entity_in_angled_area(player_ped_id(), <<287.544, -1354.412, 23.247>>, <<294.497, -1345.887, 26.247>>, 13.4))))
			or not is_entity_in_angled_area(player_ped_id(), <<284.463, -1354.515, 23.538>>, <<294.471, -1342.418, 26.538>>, 18.600)  
			or lk_timer(shootout_time, 40000)
			or bypass_find_dead_bodies_stage 
			
				//delete the first reception desk doctor
				if does_entity_exist(doctor[4].ped)
					delete_ped(doctor[4].ped)
				endif 

				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
				set_player_can_use_cover(player_id(), true)

				remove_anim_dict("Dead")
				
				SET_PED_STEALTH_MOVEMENT(player_ped_id(), false)
				REMOVE_STEALTH_MODE_ASSET("MICHAEL_ACTION")
						
				SET_PED_USING_ACTION_MODE(player_ped_id(), true)
				
				DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[6], 0.0, false, false) 
				DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[6], DOORSTATE_force_unlocked_this_frame, false, true)
				DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[7], 0.0, false, false) 
				DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[7], DOORSTATE_force_unlocked_this_frame, false, true)
			
				if not does_blip_exist(escape_blip)
					escape_blip = create_blip_for_coord(<<236.4304, -1357.7548, 38.5342>>)
				endif 
				
//				if phone_call_system_status < 3 //stops the music event triggering on a reaply.
//					KILL_PHONE_CONVERSATION()
//					phone_call_system_status = 2
//				endif 
		
				
				for i = 15 to 15//16
					setup_enemy(cia_enemy[i])
					STOP_PED_SPEAKING(cia_enemy[i].ped, true)
				endfor 
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("6"), cia_enemy[15].ped, "CIA1") 
				
				start_audio_scene("FBI_1_SHOOTOUT_GROUND_FLOOR")

				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "Start of shootout")
				
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGH)
				
				GET_PED_WEAPONS(player_ped_id(), fbi1_player_stored_weapons)
				
				fbi1_master_controller_status++
			
			endif 
			
			//saftey case
//			if is_entity_in_angled_area(player_ped_id(),<<284.213, -1347.350, 23.247>>, <<280.730, -1344.500, 26.747>>, 2.000)    
//				fbi1_master_controller_status = 1
//			endif 
		
		break 
	
		case 1

			if is_entity_in_angled_area(player_ped_id(), <<284.213, -1347.350, 23.247>>, <<280.730, -1344.500, 26.747>>, 3.000)  //2.0  
			or is_entity_in_angled_area(player_ped_id(), <<278.582, -1349.604, 23.528>>, <<281.867, -1345.703, 26.728>>, 6.5)   

				for i = 0 to 10
				
					if i != 7
						setup_enemy(cia_enemy[i])
					
						if i = 3
							GIVE_WEAPON_COMPONENT_TO_PED(cia_enemy[i].ped, WEAPONTYPE_pumpshotgun, WEAPONCOMPONENT_AT_AR_FLSH)
						endif 
					endif 
					
				endfor
				
				setup_enemy(cia_enemy[14])
				
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("8"), cia_enemy[2].ped, "CIA3")
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("E"), cia_enemy[6].ped, "CIA8")
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("B"), cia_enemy[9].ped, "CIA5") 
				

				for i = 2 to 3
					setup_enemy(doctor[i])
					set_ped_can_be_targetted(doctor[i].ped, false)
				endfor 
				
				//doctor[indexs] change from 3 to 2 in order to fix bug 1823962
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("F"), doctor[2].ped, "doctor2")
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("D"), doctor[3].ped, "doctor1")
				

				original_time = get_game_timer()

				fbi1_master_controller_status++
				
			endif 
			
		break
		
		case 2
			
			if is_entity_in_angled_area(player_ped_id(), <<243.756, -1367.919, 23.537>>, <<247.835, -1362.988, 26.737>>, 2.6)  //6.2   
				
				release_named_script_audio_bank("SCRIPT\\FBI_01_DMW_01")
				
//				for i = 0 to 10
//					if does_entity_exist(cia_enemy[i].ped)
//						set_ped_as_no_longer_needed(cia_enemy[i].ped)
//					endif 
//					if does_blip_exist(cia_enemy[i].blip)
//						remove_blip(cia_enemy[i].blip)
//					endif 
//				endfor 

				for i = 11 to 13
				
					if not cia_enemy[i].created
						
						setup_enemy(cia_enemy[i])
					
					endif 
				endfor 
				
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("A"), cia_enemy[11].ped, "CIA4")
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("7"), cia_enemy[12].ped, "CIA2")

				//print_now("FBI1_God_3", default_god_text_time, 1)
				
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(char_dave, "FBI1_WPN", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
				
				bin_bag[1].blip = create_blip_for_entity(bin_bag[1].obj)
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_1_SHOOTOUT_GROUND_FLOOR")
					STOP_AUDIO_SCENE("FBI_1_SHOOTOUT_GROUND_FLOOR")
				endif 

				start_audio_scene("FBI_1_GET_UPSTAIRS")
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "half way through shootout")
				
				GET_PED_WEAPONS(player_ped_id(), fbi1_player_stored_weapons)

				fbi1_master_controller_status++
		
			endif 
		
		break 
		
		case 3
			
			request_model(V_ILev_Cor_windowSmash)
			
			request_model(ambient_car[0].model)
			set_vehicle_model_is_suppressed(ambient_car[0].model, true)
			
			request_model(ambient_car[1].model)
			set_vehicle_model_is_suppressed(ambient_car[1].model, true)

//			if is_entity_in_angled_area(player_ped_id(), <<251.106, -1365.121, 28.653>>, <<253.007, -1366.745, 31.153>>, 1.500)  //6.2   

//				if does_blip_exist(escape_blip)
//					remove_blip(escape_blip)
//				endif 
						
//				escape_blip = create_blip_for_coord(<<239.5941, -1361.6381, 38.5342>>)
				
				fbi1_master_controller_status++
				
//			endif 
		
		break 

		case 4
		
			if is_entity_in_angled_area(player_ped_id(), <<252.445, -1367.718, 38.246>>, <<250.278, -1365.944, 41.246>>, 7.5) 
			
				for i = 0 to count_of(cia_enemy) - 1 
					if does_blip_exist(cia_enemy[i].blip)
						remove_blip(cia_enemy[i].blip)
					endif 
				endfor 
					
				for i = 0 to 2
					setup_enemy(cia_enemy_top_floor[i])
				endfor 
				
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("C"), cia_enemy_top_floor[1].ped, "CIA6")

				if IS_AUDIO_SCENE_ACTIVE("FBI_1_GET_UPSTAIRS")
					STOP_AUDIO_SCENE("FBI_1_GET_UPSTAIRS")
				endif 
				
				start_audio_scene("FBI_1_AIM_AT_WINDOW")
				
				print_now("FBI1_God_4", default_god_text_time, 1)
					
				fbi1_master_controller_status++
				
			endif 
		
		break 
		
		case 5
		
			create_2_cia_enemy_top_floor_peds()

			request_model(V_ILev_Cor_windowSmash)

			request_model(ambient_car[0].model)
			set_vehicle_model_is_suppressed(ambient_car[0].model, true)
			
			request_model(ambient_car[1].model)
			set_vehicle_model_is_suppressed(ambient_car[1].model, true)
			
			if does_object_of_type_exist_at_coords(<<236.03, -1357.49, 40.45>>, 2.0, V_ILev_Cor_windowSolid)
				
				if has_model_loaded(V_ILev_Cor_windowSmash)
				and has_model_loaded(ambient_car[0].model)
				and has_model_loaded(ambient_car[1].model)
					
					CREATE_MODEL_SWAP(<<236.03, -1357.49, 40.45>>, 1.0, V_ILev_Cor_windowSolid, V_ILev_Cor_windowSmash, TRUE)	
					window.obj = GET_CLOSEST_OBJECT_OF_TYPE(<<236.03, -1357.49, 40.45>>, 1.0, V_ILev_Cor_windowSmash)
					
					if does_entity_exist(window.obj)
						window.been_created = true 
					ELSE 
						script_assert("WINDOW_DOES_NOT_EXIST")
					endif 
					
					for i = 0 to count_of(ambient_car) - 1
						ambient_car[i].veh = create_vehicle(ambient_car[i].model, ambient_car[i].pos, ambient_car[i].heading)
						set_vehicle_doors_locked(ambient_car[i].veh, vehiclelock_locked_but_can_be_damaged)
					endfor 
					
					fbi1_master_controller_status++
					
				endif 
			
			endif 

		break 
		
		case 6
		
			create_2_cia_enemy_top_floor_peds()
		
			if does_entity_exist(window.obj)
				if is_entity_on_screen(window.obj)
					freeze_entity_position(window.obj, false)
				endif 
			else 
				window.obj = GET_CLOSEST_OBJECT_OF_TYPE(<<236.03, -1357.49, 40.45>>, 1.0, V_ILev_Cor_windowSmash)
				if does_entity_exist(window.obj)
					window.been_created = true 
				endif 
			endif 
		
			if is_entity_in_angled_area(player_ped_id(), <<234.193, -1364.507, 38.634>>, <<240.026, -1357.653, 41.534>>, 6.05, false) //2.0
			and does_entity_exist(window.obj)

				freeze_entity_position(window.obj, false)
				
				if not (GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(window.obj, false) < 0.7)
					print_now("FBI1_God_12", default_god_text_time, 1)
				endif 
				
				fbi1_master_controller_status++
				
			endif 
		
		break 

		case 7
		
			create_2_cia_enemy_top_floor_peds()

			printfloat(GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(window.obj, false))
			printnl()
			
			if GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(window.obj, false) < 0.7  //0.5 0.7
			
				if not break_the_glass_window
				
					break_entity_glass_system()
					
				endif 
				
				if not has_label_been_triggered("FBI1_God_14")
					print_now("FBI1_God_14", default_god_text_time, 1)
					set_label_as_triggered("FBI1_God_14", true)
					original_time = get_game_timer()
				endif


				//area near window
				//if is_entity_in_angled_area(player_ped_id(), <<235.770, -1358.843, 38.534>>, <<237.181, -1357.156, 41.134>>, 1.100, false) //2.0
				
				cutscene_pos = <<236.344, -1356.722, 39.304>>
				cutscene_rot = <<0.000, 0.000, -40.680>>
				
				if is_entity_at_coord(player_ped_id(), GET_ANIM_INITIAL_OFFSET_POSITION("missfbi1", "ledge_climbout", cutscene_pos, cutscene_rot, 0), <<0.75, 0.75, 1.6>>, false)
					
					if is_ped_within_range_of_target_heading(player_ped_id(), 48.26, 45.00)
					and not is_ped_ragdoll(player_ped_id())

//					if (get_entity_heading(player_ped_id()) > 85.00 and get_entity_heading(player_ped_id()) < 125.00)
//					or (get_entity_heading(player_ped_id()) >= 45.00 and get_entity_heading(player_ped_id()) < 85.00)
			
						if not has_label_been_triggered("FBI1_help6")
							if lk_timer(original_time, 1000)
								print_help("FBI1_help6")
								set_label_as_triggered("FBI1_help6", true)
							endif 
						endif 
						
						if is_control_pressed(player_control, input_jump)
						or is_disabled_control_pressed(player_control, input_jump)
							
							DISTANT_COP_CAR_SIRENS(TRUE)
							
							NEW_LOAD_SCENE_START(<<235.1414, -1357.6021, 29.5540>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 53.2332>>), 5.0)

							clear_help()
							
							if is_this_print_being_displayed("FBI1_God_14")
								clear_prints()
							endif 
							
							if does_blip_exist(escape_blip)
								remove_blip(escape_blip)
							endif 

							set_player_control(player_id(), false)
		
							cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
							TASK_SYNCHRONIZED_SCENE(player_ped_id(), cutscene_index, "missfbi1", "ledge_climbout", slow_BLEND_IN, normal_BLEND_OUT, synced_scene_dont_interrupt, rbf_none, slow_blend_in)
							SET_SYNCHRONIZED_SCENE_PHASE(cutscene_index, 0.11)
							
							set_entity_proofs(player_ped_id(), true, true, true, true, true)
		
							fbi1_master_controller_status++
							
						endif 
						
					endif 
				
				else 
				
//					if is_this_print_being_displayed("FBI1_God_14")
//						clear_prints()
//					endif 
				
				//elif is_entity_in_angled_area(player_ped_id(), area outside 0.75 m locate)
				
				endif 

			endif 

		break 
		
		case 8
		
			disable_on_foot_first_person_view_this_update()
		
			if is_synchronized_scene_running(cutscene_index)
				if get_synchronized_scene_phase(cutscene_index) >= 0.90
				
					fbi1_escape_cutscene()
				
					fbi1_master_controller_status++
					
					return true 
					
				endif 
			endif 

		break 
		
		case 9
		
			return true
		
		break 
		
	endswitch
	
	return false

endfunc 

proc doctors_ai_system()

	int i = 0
	
	for i = 2 to count_of(doctor) - 1
		
		if not is_ped_injured(doctor[i].ped)
		
			switch doctor_ai_status[i]
			
				case action_1
				
					switch i 
					
						case 2
						case 4
						
							open_sequence_task(seq)
								task_look_at_entity(null, player_ped_id(), -1)
								task_hands_up(null, -1)
							close_sequence_task(seq)
							task_perform_sequence(doctor[i].ped, seq)
							clear_sequence_task(seq)
							
							doctor_ai_status[i] = action_1_status 
						
						break 
						
						case 3
						
							if is_entity_in_angled_area(player_ped_id(), <<273.347, -1344.646, 23.247>>, <<276.306, -1341.125, 26.747>>, 2.0)   

								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null, doctor[i].run_to_pos, pedmove_run, -1)
									task_cower(null, -1)
								close_sequence_task(seq)
								task_perform_sequence(doctor[i].ped, seq)
								clear_sequence_task(seq)
								
								PLAY_PED_AMBIENT_SPEECH(doctor[i].ped, "SCREAM_PANIC") 
								
								doctor_ai_status[i] = action_1_status 
								
							endif 

						break
						
					endswitch 
					
				break 
				
				case action_1_status 
				
					switch i 
					
						case 2
							
							if is_entity_in_angled_area(player_ped_id(), <<267.343, -1351.262, 23.543>>, <<254.703, -1340.656, 26.0>>, 6.0) 
								if is_sphere_visible(get_entity_coords(doctor[i].ped), 1.0)
								
									original_time = get_game_timer()
									
									doctor_ai_status[i] = action_2_status 
									
								endif 
								
							endif 
						
						break 
					
						case 3
				
							if not is_entity_at_coord(doctor[i].ped, doctor[i].run_to_pos, <<2.0, 2.0, 2.5>>)
								if has_char_task_finished_2(doctor[i].ped, script_task_perform_sequence)
								
									open_sequence_task(seq)
										task_follow_nav_mesh_to_coord(null, doctor[i].run_to_pos, pedmove_run, -1)
										task_cower(null, -1)
									close_sequence_task(seq)
									task_perform_sequence(doctor[i].ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							endif 
							
							if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(doctor[i].ped)) < 3
								if lk_timer(doctor_time[i], 10000)
							
									play_ped_ambient_speech(doctor[i].ped, "GENERIC_FRIGHTENED_HIGH")//"WHIMPER")
									//if create_conversation(scripted_speech, "FBI1AUD", "fbi_doc_cow1", conv_priority_medium)
										doctor_time[i] = get_game_timer()
									//endif
									
								endif 
							endif 
	
						break 
						
						case 4
						
							if not is_entity_at_coord(doctor[i].ped, doctor[i].pos, <<2.0, 2.0, 2.5>>)
								if has_char_task_finished_2(doctor[i].ped, script_task_perform_sequence)
								
									open_sequence_task(seq)
										task_follow_nav_mesh_to_coord(null, doctor[i].pos, pedmove_walk  -1)
										task_turn_ped_to_face_entity(null, player_ped_id())
										task_look_at_entity(null, player_ped_id(), -1)
										task_hands_up(null, -1)
									close_sequence_task(seq)
									task_perform_sequence(doctor[i].ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							endif  
							
						break 
						
					endswitch 
				
				break
				
				case action_2_status 
				
					switch i 
					
						case 2
						
							if lk_timer(original_time, 250) //1000

								open_sequence_task(seq)
									task_cower(null, -1)
								close_sequence_task(seq)
								task_perform_sequence(doctor[i].ped, seq)
								clear_sequence_task(seq)
								
								doctor_ai_status[i] = action_3_status 
								
							endif 
						
						break 
					
						case 4
						
							if lk_timer(doctor_4_time, 2000) //2500
							
								open_sequence_task(seq)
									task_clear_look_at(null)
									task_follow_nav_mesh_to_coord(null, doctor[0].pos, pedmove_run, -1)
									task_cower(null, -1)
								close_sequence_task(seq)
								task_perform_sequence(doctor[i].ped, seq)
								clear_sequence_task(seq)
								
								doctor_ai_status[i] = action_3_status 
								
							endif 
						
						break 
					
					endswitch 
				
				break 
				
				case action_3_status 
				
					switch i 
						
						case 2
						
							if has_ped_task_finished_2(doctor[i].ped)
							
								open_sequence_task(seq)
									task_cower(null, -1)
								close_sequence_task(seq)
								task_perform_sequence(doctor[i].ped, seq)
								clear_sequence_task(seq)
								
							endif 
							
							
							//fix for bug 1765710
							if not stop_doctor_2_speaking 
								if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(doctor[i].ped)) > 15.0
									stop_ped_speaking(doctor[i].ped, true)
								endif 
							else 
								if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(doctor[i].ped)) < 10.0
									stop_ped_speaking(doctor[i].ped, false)
									stop_doctor_2_speaking = false
								endif 
							endif 
						break 
						
					endswitch 
				
				break 
				
			endswitch 
			
		endif 
		
	endfor 	

endproc 

proc turn_off_alarm()

	if alarm_on
		if lk_timer(alarm_time, 60000)
			if IS_ALARM_PLAYING("FBI_01_MORGUE_ALARMS")
				STOP_ALARM("FBI_01_MORGUE_ALARMS", true)
			endif 
		endif 
	endif 

endproc

proc phone_call_system()
	
	switch phone_call_system_status 
		
		case 0
		
			if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech, char_dave, "fbi1aud", "call1d", conv_priority_very_high)
				
				trigger_music_event("FBI1_CALL_NORTON")
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_1_INSPECT_BODIES")
					STOP_AUDIO_SCENE("FBI_1_INSPECT_BODIES")
				endif
				
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
				
				current_line = -1
				phone_call_system_status++
			endif 
  	
		break 
		
		case 1
		
			if not HAS_CELLPHONE_CALL_FINISHED()
				if get_CURRENT_SCRIPTED_CONVERSATION_LINE() <> -1
					
					current_line = get_CURRENT_SCRIPTED_CONVERSATION_LINE()

//					if current_line > 3
//						//trigger_alarm()
//						phone_call_system_status = 2
//					endif 

				endif 
			else
			
				if current_line <> -1
					if (NOT WAS_LAST_CELLPHONE_CALL_INTERRUPTED()) OR (current_line >= 0)
						
						//trigger_alarm()

						if not does_blip_exist(escape_blip)
							escape_blip = create_blip_for_coord(<<236.4304, -1357.7548, 38.5342>>)
						endif 

						phone_call_system_status = 2

					else 
						phone_call_system_status = 0
					endif 
					
				else 

					phone_call_system_status = 0
					
					phone_hang_up_count++ //if the player constantly hang up the phone the mission will progress
					
					if phone_hang_up_count > 2
						//trigger_alarm()
						phone_call_system_status = 2
					endif 

				endif 
			
			endif 
			
		break 
		
		case 2
		
			trigger_music_event("FBI1_LEAK")
			
			start_audio_scene("FBI_1_ESCAPE_BASE_SCENE")

			dave_norton_phone_call_finished = true
		
			alarm_time = get_game_timer()
			
			phone_call_system_status++
		
		break 
		
		case 3
		
		break 
		
	endswitch 
	
endproc 

func bool are_cia_enemies_close()

	int i = 0
			
//	vector player_pos = get_entity_coords(player_ped_id())
//	
//	float close_distance = 40.00
//
//	for i = 0 to count_of(cia_enemy) - 1
//		if not is_ped_injured(cia_enemy[i].ped) 
//			if get_distance_between_coords(player_pos, cia_enemy[i].ped) < close_distance
//				return true
//			endif 
//		endif 
//	endfor 
//	
//	for i = 0 to count_of(cia_enemy_top_floor) - 1
//		if not is_ped_injured(cia_enemy[i].ped) 
//			if get_distance_between_coords(player_pos, cia_enemy[i].ped) < close_distance
//				return true
//			endif 
//		endif 
//	endfor 

	for i = 0 to count_of(cia_enemy) - 1
		if not is_ped_injured(cia_enemy[i].ped)
			return true
		endif 
	endfor 
	
	for i = 0 to count_of(cia_enemy_top_floor) - 1
		if not is_ped_injured(cia_enemy_top_floor[i].ped)
			return true
		endif 
	endfor
	
	
	return false
	
endfunc

proc dialogue_system()

	switch dialogue_status

		case 0
		
			if has_label_been_triggered("cia_attak0")
			or (cia_enemy[15].created and is_ped_injured(cia_enemy[15].ped))
				if not is_any_text_being_displayed(locates_data)
					clear_prints()
					print_now("FBI1_God_7", default_god_text_time, 1)
						
					dialogue_time = get_game_timer()
					dialogue_status++
				endif 
			endif 
		
		break 
		
		case 1
		
		
//			if not is_ped_injured(doctor[4].ped)
//					
//				//if is_ped_in_specific_room(player_ped_id(), <<251.12, -1367.35, 25.00>>, "CorridorBottom")
//				//if is_entity_in_angled_area(player_ped_id(), <<244.300, -1368.440, 23.543>>, <<248.479, -1363.460, 26.743>>, 7.6)
//				//and ((get_entity_heading(player_ped_id()) > 4.49 and get_entity_heading(player_ped_id()) < 90.31) or get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(doctor[4].ped)) < 2.0)
//
//				if allow_doctor_4_to_run()
//					
//					if create_conversation(scripted_speech, "fbi1aud", "fbi_esc0", CONV_priority_medium)
//						set_label_as_triggered("fbi_esc0", true)
//					endif 
//					
//					dialogue_status++
//						
//				endif 
//			
//			else 
//			
//				if is_entity_in_angled_area(player_ped_id(), <<243.756, -1367.919, 23.537>>, <<247.835, -1362.988, 26.737>>, 8.200)  //6.2  4.3   
//
////					print_now("FBI1_God_3", default_god_text_time, 1)
////					set_label_as_triggered("FBI1_God_3", true)
//					
//					dialogue_status = 3
//					
//				endif 
//			
//			endif 
			

		break
		
		case 2
		
//			if not has_label_been_triggered("fbi_esc0")
//				if create_conversation(scripted_speech, "fbi1aud", "fbi_esc0", CONV_priority_medium)
//					set_label_as_triggered("fbi_esc0", true)
//				endif 
//				
//			else 
//			
//				if not is_any_text_being_displayed(locates_data)
//					print_now("FBI1_God_3", default_god_text_time, 1)
//					dialogue_status++
//				endif 
//				
//			endif 	
		
		break 
		
		case 3
		
		break 
	
	endswitch 
	
	if lk_timer(dialogue_time, 13000)
		if is_ped_shooting(player_ped_id())

			//if is_ped_in_combat(player_ped_id() //or if lower enemies alive and player z is < ground gfloor
			if not is_any_text_being_displayed(locates_data)
				if are_cia_enemies_close()

					if create_conversation(scripted_speech, "fbi1aud", "mik_attk0", CONV_priority_low)
						dialogue_time = get_game_timer()
					endif 
					
				endif 

			endif 
		endif 
	endif 

endproc 

proc music_event_system()
	
	switch music_event_system_status 
	
		case 0
		
			if ped_structure_are_specific_number_enemies_dead(cia_enemy, (total_number_of_removed_cia_enemeis + 3))
			
				trigger_music_event("FBI1_THREE_DEAD")
				
				music_event_system_status++
				
			endif 
			
			//if the player runs through without killing anyone
			if prepare_music_event("FBI1_LIFT_ENEMY")
				if is_entity_in_angled_area(player_ped_id(), <<252.686, -1367.138, 23.538>>, <<250.692, -1365.470, 26.1>>, 1.000)  
					trigger_music_event("FBI1_LIFT_ENEMY")
					trigger_music_event("FBI1_THREE_DEAD")
					music_event_system_status = 2
				endif 
			endif
		
		break 
		
		case 1
		
			if prepare_music_event("FBI1_LIFT_ENEMY")
				if is_entity_in_angled_area(player_ped_id(), <<252.686, -1367.138, 23.538>>, <<250.692, -1365.470, 26.1>>, 1.000)  
					trigger_music_event("FBI1_LIFT_ENEMY")
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_NORMAL)
					
					music_event_system_status++
				endif 
			endif 
		
		break 
		
		case 2
		
			if is_entity_in_angled_area(player_ped_id(), <<243.591, -1368.662, 23.537>>, <<241.829, -1367.184, 26.137>>, 1.3)
		
				trigger_music_event("FBI1_STAIRWELL")
				
				music_event_system_status++
				
			endif 
		
		break 
		
		case 3
		
			if is_entity_in_angled_area(player_ped_id(), <<251.106, -1365.121, 28.653>>, <<253.007, -1366.745, 31.153>>, 1.500)    
		
				trigger_music_event("FBI1_2ND_STAIRWELL")
				
				music_event_system_status++
				
			endif 
		
		break 
		
		case 4

			if prepare_music_event("FBI1_TOP_FLOOR")
				if is_entity_in_angled_area(player_ped_id(), <<252.445, -1367.718, 38.246>>, <<250.278, -1365.944, 41.246>>, 7.5) 
					trigger_music_event("FBI1_TOP_FLOOR")
					music_event_system_status++
				endif 

			endif 
		
		break 
		
		case 5
		
			if is_entity_in_angled_area(player_ped_id(), <<238.427, -1362.878, 38.539>>, <<240.223, -1360.730, 41.746>>, 4.0, false) //2.0

				trigger_music_event("FBI1_ESCAPE")
				
				music_event_system_status++
			
			endif 
		
		break 
		
	endswitch 

endproc

proc get_weapons_back_system()
			
	if does_entity_exist(bin_bag[1].obj)

		//if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(bin_bag[1].obj)) < 2.0
		if is_entity_touching_entity(player_ped_id(), bin_bag[1].obj)
		
			delete_object(bin_bag[1].obj)
			set_model_as_no_longer_needed(bin_bag[1].model)
			
			if does_blip_exist(bin_bag[1].blip)
				remove_blip(bin_bag[1].blip)
			endif 

			//BLOCK_PLAYER_PED_WEAPON_STORE(CHAR_MICHAEL, FALSE)
			RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(player_ped_id(), false)
			
			play_sound_from_entity(-1, "PICK_UP_PARACHUTE", player_ped_id(),"HUD_FRONTEND_CUSTOM_SOUNDSET") 
			
			player_weapons_restored = true
		
		endif 
	endif 
	
endproc 

proc delete_wave_one_cia_enemies_and_doctors_system()

	int i = 0

	switch delete_wave_one_cia_enemies_and_doctors_system_status
		
		case 0
		
			if is_entity_in_angled_area(player_ped_id(), <<251.106, -1365.121, 28.653>>, <<253.007, -1366.745, 31.153>>, 1.500)    
		
				delete_wave_one_cia_enemies_and_doctors_system_status++
				
			endif 
			
		break 
		
		case 1
		
			vector player_pos
			int peds_removed 

			peds_removed = 0
			
			player_pos = get_entity_coords(player_ped_id())
			
			for i = 0 to 10
			
				if does_entity_exist(cia_enemy[i].ped)
			
					if not is_entity_on_screen(cia_enemy[i].ped)
						
						if not is_ped_in_specific_room(cia_enemy[i].ped, <<247.7852, -1366.4725, 28.6480>>, "CorridorTop", "v_coroner")
						and not is_ped_in_specific_room(cia_enemy[i].ped, <<256.6936, -1363.8687, 29.5428>>, "MainStairs", "v_coroner")
						and not is_ped_in_specific_room(cia_enemy[i].ped, <<250.0518, -1368.5043, 38.5342>>, "topoff_mid", "v_coroner")
						and not is_entity_at_coord(cia_enemy[i].ped, player_pos, <<5.0, 5.0, 2.5>>)
						
							delete_ped(cia_enemy[i].ped)
						
							if does_blip_exist(cia_enemy[i].blip)
								remove_blip(cia_enemy[i].blip)
							endif 
							
							peds_removed++
						
						endif 
						
					endif 
				
				else 
				
					if cia_enemy[i].created
						peds_removed++
					endif 

				endif 
						
			endfor 
			
			for i = 0 to count_of(doctor) - 1
			
				if does_entity_exist(cia_enemy[i].ped)
			
					if not is_entity_on_screen(cia_enemy[i].ped)
						
						delete_ped(cia_enemy[i].ped)
						
						peds_removed++

					endif 
					
				else 
				
					if cia_enemy[i].created
					
						peds_removed++
					
					endif 
				
				endif 
			
			endfor 
			
			if peds_removed > (11 + count_of(doctor))
				delete_wave_one_cia_enemies_and_doctors_system_status++
			endif 

		break 
		
		case 2
		
		break 
		
	endswitch 
endproc

proc replay_video_recorder_system()
	
	switch replay_video_recorder_system_status 
	
		case 0
		
			if does_entity_exist(cia_enemy[11].ped)
				replay_video_recorder_system_status++
			endif 
		
		break 
		
		case 1
		
			if is_ped_injured(cia_enemy[11].ped)
				REPLAY_RECORD_BACK_FOR_TIME(3, 2.0, REPLAY_IMPORTANCE_NORMAL )
				replay_video_recorder_system_status++
			endif 
		
		break 
		
		case 2
		
		break 

	endswitch 

endproc 


func bool fbi1_shootout()

	DEACTIVATE_VENDING_MACHINES_THIS_FRAME()
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2144295
	
	if fb1_shootout_master_controller_system()
		mission_flow = escape_cutscene
		return true 
	endif 
	
	phone_call_system()
	
	trigger_alarm()

	cia_enemy_ai_system()
	
	cia_enemy_top_floor_ai_system()
	
	doctors_ai_system()
	
	elevator_door_system()
	
	dialogue_system()
	
	special_ability_help_text_system()
	
	music_event_system()

	get_weapons_back_system()

	give_player_weapon_component_system()
	
	delete_wave_one_cia_enemies_and_doctors_system()
	
	replay_video_recorder_system()

	if is_special_ability_active(player_id())
				
		if is_this_help_message_being_displayed("FBI1_help4")
			clear_help()
			set_label_as_triggered("FBI1_help4", true) //stops the special ability help text rendering if already triggered
			FLASH_ABILITY_BAR(0)
		endif
		
		IF IS_PC_VERSION()
			if is_this_help_message_being_displayed("FBI1_help4_KM")
				clear_help()
				set_label_as_triggered("FBI1_help4", true) //stops the special ability help text rendering if already triggered
				FLASH_ABILITY_BAR(0)
			endif
		ENDIF

	endif 

	//turn_off_alarm()

//	bool door_lock_state
//	float door_ratio
//	get_state_of_closest_door_of_type(v_ilev_cor_doorglassb, <<232.58, -1363.37, 39.68>>, door_lock_state, door_ratio) 
//	printstring("door locked state:")
//	printfloat(door_ratio)
//	if door_lock_state
//		printstring("door locked")
//		printnl()
//	else 
//		printstring("door OPEN")
//		printnl()
//	endif 
//	
//	set_state_of_closest_door_of_type(v_ilev_cor_doorglassb, <<232.58, -1363.37, 39.68>>, true, 0.0)
	
	return false 

endfunc

proc franklin_ai_system()
		
	if not is_ped_injured(franklin.ped)
		
		switch franklin_ai_system_status 
		
			case 0
			
				if not is_synchronized_scene_running(cutscene_index)
					if not is_entity_at_coord(franklin.ped, franklin.pos, <<0.7, 0.7, 2.5>>)
						if has_ped_task_finished_2(franklin.ped, script_task_perform_sequence, 1)
						
							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, franklin.pos, pedmove_walk, -1)
								task_turn_ped_to_face_entity(null, player_ped_id())
							close_sequence_task(seq)
							task_perform_sequence(franklin.ped, seq)
							clear_sequence_task(seq)
							
						endif 
					endif 
				endif 
				
				if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

					dialogue_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
				
					IF ARE_STRINGS_EQUAL(dialogue_root, "call2")

						TASK_USE_MOBILE_PHONE(franklin.ped, true)
						
						franklin_ai_system_status++
					
					endif 
					
				endif 
								
			break 
			
			case 1

				if has_cellphone_call_finished()
				or not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					TASK_USE_MOBILE_PHONE(franklin.ped, false)
				
					franklin_ai_system_status++
				
				endif 
				
			break 
			
			case 2
			
				vector franklin_rot
				
				cutscene_pos = <<1602.060, -1944.361, 100.796>>
				cutscene_rot = <<-0.000, 1.000, 12.000>>

				franklin.pos = GET_ANIM_INITIAL_OFFSET_POSITION("MISSFBI1LEADINOUTFBI_1_EXT", "_LEADIN_FRANKLIN", cutscene_pos, cutscene_rot)
				franklin_rot = GET_ANIM_INITIAL_OFFSET_rotation("MISSFBI1LEADINOUTFBI_1_EXT", "_LEADIN_FRANKLIN", cutscene_pos, cutscene_rot)

				if not is_entity_at_coord(franklin.ped, franklin.pos, <<1.0, 1.0, 1.6>>)
				
					if has_ped_task_finished_2(franklin.ped)
					
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, franklin.pos, pedmove_walk, -1, 0.1, enav_default, franklin_rot.z)
						close_sequence_task(seq)
						task_perform_sequence(franklin.ped, seq)
						clear_sequence_task(seq)
						
					endif 
					
				else 
			
					cutscene_pos = <<1602.060, -1944.361, 100.796>>
					cutscene_rot = <<-0.000, 1.000, 12.000>>
					cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
					TASK_SYNCHRONIZED_SCENE(franklin.ped, cutscene_index, "MISSFBI1LEADINOUTFBI_1_EXT", "_LEADIN_FRANKLIN", slow_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, rbf_none, normal_blend_in)
					set_synchronized_scene_looped(cutscene_index, true)
					
				endif 

			break 
			
		endswitch 
		
	endif 

endproc 


proc truck_ptfx_system()
	
//	if not is_entity_dead(players_truck.veh)
//		if is_entity_in_angled_area(player_ped_id(), get_offset_from_entity_in_world_coords(players_truck.veh, <<-1.100, -1.400, 1.300>>), get_offset_from_entity_in_world_coords(players_truck.veh, <<1.100, -1.400, 3.500>>), 4.4)   
//			if not is_ped_stopped(player_ped_id())
//				
//				if lk_timer(original_time, 500)
//					
//					if not DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_paper)
//						//ptfx_paper = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_fbi1_paper_disturb", player_ped_id(),  <<0.0, 0.3, -1.0>>, <<0.0, 0.0, 0.0>>, 1.0)
//						start_particle_fx_non_looped_on_entity("scr_fbi1_paper_disturb", player_ped_id(),  <<0.0, 0.3, -1.0>>, <<0.0, 0.0, 0.0>>, 1.0)
//						original_time = get_game_timer()
//					
//					else 
//					
//						stop_particle_fx_looped(ptfx_paper)
//					
//					endif 
//					
//				endif 
//				
//			else 
//			
//				if DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_paper)
//					stop_particle_fx_looped(ptfx_paper)
//				endif 
//			
//			endif 
//			
//		endif 
//		
//	endif 

endproc 

proc request_mocap_data_for_FBI_1_EXT()

	mocap_streaming_system(<<1593.3832, -1934.3777, 97.8>>, DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_UNLOAD_DIST, "FBI_1_EXT") //FBI_1_EXT
	
	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", PLAYER_PED_ID())
	SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("franklin", franklin.ped)
	
endproc 

proc phone_call_system_2()
			
	switch phone_call_system_2_status 
	
		case 0
		
			if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech, char_franklin, "fbi1aud", "call2", conv_priority_very_high)
				REPLAY_RECORD_BACK_FOR_TIME(1.0, 10.0, REPLAY_IMPORTANCE_NORMAL)
				phone_call_system_2_status++
			endif
		
		break 
		
		case 1
		
			if has_cellphone_call_finished()
			
				add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("5"), null, "dave")  

				phone_call_time = get_game_timer()
				
				phone_call_system_2_status++
			
			endif 
		
		break 
		
		case 2
		
			if lk_timer(phone_call_time, 4000)
			
				if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech, char_dave, "fbi1aud", "call3", conv_priority_very_high)
					
					phone_call_system_2_status++
				endif 
			
			endif 
		
		break 
		
		case 3
		
			if has_cellphone_call_finished()
				phone_call_system_2_status++
			endif 
		
		break
		
		case 4
		
		break 
		
	endswitch 

endproc 

proc remove_shootout_assets()

	int i = 0
			
	if not shootout_assets_removed

		if get_distance_between_coords(get_entity_coords(player_ped_id()), <<235.2498, -1354.1217, 29.5623>>) > 100.00

			for i = 0 to count_of(cia_enemy_outside) - 1
				
				if does_entity_exist(cia_enemy_outside[i].ped)

					set_ped_as_no_longer_needed(cia_enemy_outside[i].ped)
					set_model_as_no_longer_needed(cia_enemy_outside[i].model)
					
				endif 
			endfor 
			
			for i = 0 to count_of(police_car) - 1
				
				if does_entity_exist(police_car[i].veh)
					
					set_vehicle_as_no_longer_needed(police_car[i].veh)
					set_model_as_no_longer_needed(police_car[i].model)
					
				endif 
			endfor 
			
			shootout_assets_removed = true
			
		endif 
		
	endif 

endproc 

proc taxi_drop_off_for_pass_mocap()
			
	if does_blip_exist(locates_data.LocationBlip)
		if not taxi_drop_off_set_for_pass_mocap 
			SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(locates_data.LocationBlip, <<1610.2008, -1914.6011, 99.6843>>, 138.8997)
			taxi_drop_off_set_for_pass_mocap = true
		endif 
	else 
		taxi_drop_off_set_for_pass_mocap = false 
	endif 
	
endproc 

proc fbi1_get_to_hills()

	int i = 0

	truck_ptfx_system()
	
	cia_enemy_outside_ai_system()
	
	switch get_to_hills_status 
	
		case 0
		
			request_model(franklin.model)
			request_model(cia_enemy_outside[1].model)
			request_model(cia_enemy_outside[3].model)
			request_model(police_car[0].model)
			request_model(franklins_car.model)
			set_vehicle_model_is_suppressed(franklins_car.model, true)
			request_anim_dict("MISSFBI1LEADINOUTFBI_1_EXT")
			request_vehicle_recording(police_car[0].recording_number, "lkfbi1")
			
			if not does_entity_exist(franklin.ped)
				
				if original_time = 0
					original_time = get_game_timer()
				endif 
				
				if has_model_loaded(franklin.model)
				and has_model_loaded(cia_enemy_outside[1].model)
				and has_model_loaded(cia_enemy_outside[3].model)
				and has_model_loaded(police_car[0].model)
				and has_model_loaded(franklins_car.model)
				and has_anim_dict_loaded("MISSFBI1LEADINOUTFBI_1_EXT")
				and has_vehicle_recording_been_loaded(police_car[0].recording_number, "lkfbi1")
				and lk_timer(original_time, 3000)

					for i = 1 to 2
						setup_enemy(cia_enemy_outside[i], true)
					endfor 
				
					create_player_ped_on_foot(franklin.ped, char_franklin, franklin.pos, franklin.heading)
					//SET_PED_COMP_ITEM_CURRENT_SP(franklin.ped, COMP_TYPE_PROPS, PROPS_P1_HEADSET)
					setup_buddy_attributes(franklin.ped)
					//setup_relationship_contact(franklin.ped, true)
					set_blocking_of_non_temporary_events(franklin.ped, true)
					add_ped_for_dialogue(scripted_speech, ConvertSingleCharacter("1"), franklin.ped, "franklin")
					
					cutscene_pos = <<1602.060, -1944.361, 100.796>>
					cutscene_rot = <<-0.000, 1.000, 12.000>>
					cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
					TASK_SYNCHRONIZED_SCENE(franklin.ped, cutscene_index, "MISSFBI1LEADINOUTFBI_1_EXT", "_LEADIN_FRANKLIN", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					set_synchronized_scene_looped(cutscene_index, true)

					CREATE_PLAYER_VEHICLE(franklins_car.veh, CHAR_FRANKLIN, franklins_car.pos, franklins_car.heading, TRUE, VEHICLE_TYPE_BIKE)
					
					for i = 0 to 0//count_of(police_car) - 1
						clear_area(police_car[i].pos, 10.00, true)
						police_car[i].veh = create_vehicle(police_car[i].model, police_car[i].pos, police_car[i].heading)
						if i = 0 
							start_playback_recorded_vehicle(police_car[i].veh, police_car[i].recording_number, "lkfbi1")
							skip_time_in_playback_recorded_vehicle(police_car[i].veh, 0)
							force_playback_recorded_vehicle_update(police_car[i].veh)
							set_vehicle_siren(police_car[i].veh, true)
						endif 
					endfor 
					
					setup_enemy_in_vehicle(cia_enemy_outside[3], police_car[0].veh)
					setup_enemy_in_vehicle(cia_enemy_outside[4], police_car[0].veh, vs_front_right)

				endif 
			
			else 
			
				franklin_ai_system()
				
////				prepare it for every car the player enters.
//				if not has_label_been_triggered("FBI1_RADIO")
//					if is_ped_sitting_in_any_vehicle(player_ped_id())
//						prepare_music_event("FBI1_RADIO")
//						set_label_as_triggered("FBI1_RADIO", true)
////					else 
////						set_label_as_triggered("FBI1_RADIO", false)
//					endif 
//				endif 

				if not is_player_wanted_level_greater(player_id(), 0)

					if not is_ped_in_any_vehicle(player_ped_id())
						trigger_music_event("FBI1_COPS_LOST")
					else 
						trigger_music_event("FBI1_RADIO")
					endif 
					
					if IS_AUDIO_SCENE_ACTIVE("FBI_1_LOSE_COPS")
						STOP_AUDIO_SCENE("FBI_1_LOSE_COPS")
					endif 
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 

					clear_this_print("LOSE_WANTED")
					
					clear_player_wanted_level(player_id())
					set_wanted_level_multiplier(0.2)
					
					get_to_hills_status++
				endif 

			endif 

		break 
		
		case 1
		
			if does_entity_exist(franklins_car.veh)
				if is_vehicle_driveable(franklins_car.veh)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(franklins_car.veh)//stops pop in cutscene.
				endif 
			endif 
		
			phone_call_system_2()

			franklin_ai_system()
			
			request_mocap_data_for_FBI_1_EXT()
			
			remove_shootout_assets()

			IS_PLAYER_AT_LOCATION_any_means(locates_data, <<1593.3832, -1934.3777, 98.6576>>, <<0.01, 0.01, 0.01>>, false, "FBI1_God_6", true)
			
			taxi_drop_off_for_pass_mocap()

			if does_blip_exist(locates_data.LocationBlip)
				if is_entity_in_angled_area(player_ped_id(), <<1586.747, -1943.608, 90.969>>, <<1611.747, -1943.609, 110.969>>, 42.000)

					if get_current_player_vehicle(players_last_vehicle)
						if IS_VEHICLE_ON_ALL_WHEELS(players_last_vehicle)
					
							setup_vehicle_proofs(players_last_vehicle)
								
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_last_vehicle, 7)
							
							kill_any_conversation() //kills phone call
									
							get_to_hills_status++
						endif 
						
					else 

						//if has_cutscene_loaded()
						if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
							if start_new_cutscene_no_fade(false)

								SET_PED_USING_ACTION_MODE(player_ped_id(), false)
							
								setup_ped_proofs(franklin.ped)
							
								register_entity_for_cutscene(player_ped_id(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								register_entity_for_cutscene(franklin.ped, "Franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
								
								if does_entity_exist(franklins_car.veh)
									if is_vehicle_driveable(franklins_car.veh)
										SET_FORCE_HD_VEHICLE(franklins_car.veh, true)
									endif 
								endif 
								
								REPLAY_RECORD_BACK_FOR_TIME(7.5, 0.0, REPLAY_IMPORTANCE_HIGH)
								
								START_CUTSCENE()
													
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
								get_to_hills_status = 4
								
							endif 
						endif 
						
					endif 
					
				endif 
							
			endif 

		break 
		
		case 2
		
			request_mocap_data_for_FBI_1_EXT()
		
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_last_vehicle, 7)
				set_player_control(player_id(), false)
				get_to_hills_status++
			endif 
			
		break 
		
		case 3
		
			request_mocap_data_for_FBI_1_EXT()

			//if has_cutscene_loaded()
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				if not is_any_text_being_displayed(locates_data)
					if start_new_cutscene_no_fade(false, true, true, true)

						SET_PED_USING_ACTION_MODE(player_ped_id(), false)
					
						setup_ped_proofs(franklin.ped)
					
						register_entity_for_cutscene(player_ped_id(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						register_entity_for_cutscene(franklin.ped, "Franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						
						if does_entity_exist(franklins_car.veh)
							if is_vehicle_driveable(franklins_car.veh)
								SET_FORCE_HD_VEHICLE(franklins_car.veh, true)
							endif 
						endif 
						
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						get_to_hills_status++
						
					endif 
					
				endif 
							
			endif 
			
		break 
		
		case 4
		
			if is_cutscene_playing()
			
				//use new reposition tool
				//repostion_players_last_vehicle(players_last_vehicle, <<1621.736, -1922.556, 92.360>>, <<50.00, 50.00, 50.00>>, <<1616.2025, -1933.7650, 100.7450>>, 65.3292)
		
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1629.403, -1946.552, 91.196>>, <<1571.303, -1946.552, 107.096>>, 62.100, <<1616.2025, -1933.7650, 100.7450>>, 65.3292, <<15.0, 15.0, 15.0>>)//, false)
				set_players_last_vehicle_to_vehicle_gen(<<1616.2025, -1933.7650, 100.7450>>, 65.3292, true)

				clear_area(get_entity_coords(player_ped_id()), 300.00, true)
				
				if does_entity_exist(franklins_car.veh)
					if is_vehicle_driveable(franklins_car.veh)
						setup_vehicle_proofs(franklins_car.veh)
						set_entity_heading(franklins_car.veh, franklins_car.heading)
						set_entity_coords(franklins_car.veh, franklins_car.pos)
						set_vehicle_on_ground_properly(franklins_car.veh)
						freeze_entity_position(franklins_car.veh, true)
					endif 
				endif 
				
				SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(2.0)
				
				original_time = get_game_timer() 

				get_to_hills_status++
			
			endif 
		
		break 
		
		case 5
		
			if does_entity_exist(franklins_car.veh)
				if is_vehicle_driveable(franklins_car.veh)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(franklins_car.veh)//stops pop in cutscene.
				endif 
			endif 
		
			SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(2.0)
		
			if is_cutscene_active()
				if not WAS_CUTSCENE_SKIPPED()
			
					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))

					endif
					
//					IF NOT cam_set_behind_player
//						IF lk_timer(original_time, 160000)
//				        
//				            SET_GAMEPLAY_CAM_RELATIVE_HEADING((125 - GET_ENTITY_HEADING(PLAYER_PED_ID())))
//				            SET_GAMEPLAY_CAM_RELATIVE_PITCH()
//							
//				            cam_set_behind_player = true  
//							
//						ENDIF
//					ENDIF

				else 

					SET_CUTSCENE_FADE_VALUES(false, false, true)
					
					get_to_hills_status = 22
					
				endif 
			
			else 
			
//				if does_entity_exist(players_last_vehicle)
//					set_vehicle_as_no_longer_needed(players_last_vehicle)
//				endif 

				deactivate_vehicle_proofs(players_last_vehicle)
				
				if does_entity_exist(franklins_car.veh)
					delete_vehicle(franklins_car.veh)
				endif
			
				end_cutscene_no_fade(false, false)
						
				REPLAY_STOP_EVENT()
						
				mission_passed()

			endif 

		break 
		
		case 22
		
			if not is_cutscene_active()
			
				deactivate_vehicle_proofs(players_last_vehicle)
				
				if does_entity_exist(franklins_car.veh)
					delete_vehicle(franklins_car.veh)
				endif
			
				end_cutscene(false)
				
				REPLAY_STOP_EVENT()
				
				mission_passed()
			
			endif 
		
		break 
		
	endswitch 
	
endproc 

proc fbi1_pass_cutscene()

	end_cutscene()
	
	mission_passed()

endproc

proc initialise_mission_doors()

	//operating table room
	fbi_door[0] = get_hash_key("fbi_1_door_0") //<<283.59, -1340.94, 24.68>>
	add_door_to_system(fbi_door[0], v_ilev_cor_doorglassa, <<283.59, -1340.94, 24.68>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[0], -1.2, false, false) //1.0
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[0], DOORSTATE_force_locked_this_frame, false, true)
	
	fbi_door[1] = get_hash_key("fbi_1_door_1") //<<283.59, -1340.94, 24.68>>
	add_door_to_system(fbi_door[1], v_ilev_cor_doorglassb, <<281.93, -1342.90, 24.68>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[1], 1.2, false, false) //1.0
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[1], DOORSTATE_force_locked_this_frame, false, true)
	
	//opposite morgue room
	fbi_door[2] = get_hash_key("fbi_1_door_2") 
	add_door_to_system(fbi_door[2], v_ilev_cor_doorglassb, <<287.23, -1343.99, 24.68>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[2], 1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[2], DOORSTATE_force_locked_this_frame, false, true)
	
	fbi_door[3] = get_hash_key("fbi_1_door_3") 
	add_door_to_system(fbi_door[3], v_ilev_cor_doorglassa, <<285.57, -1345.96, 24.68>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[3], -1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[3], DOORSTATE_force_locked_this_frame, false, true)
	
	//first lab doors.
	fbi_door[4] = get_hash_key("fbi_1_door_4") 
	add_door_to_system(fbi_door[4], v_ilev_cor_doorglassb, <<265.77, -1345.87, 24.68>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[4], 1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[4], DOORSTATE_force_unlocked_this_frame, false, true)
	
	fbi_door[5] = get_hash_key("fbi_1_door_5") 
	add_door_to_system(fbi_door[5], v_ilev_cor_doorglassa, <<267.42, -1343.90, 24.68>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[5], -1.6, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[5], DOORSTATE_force_locked_this_frame, false, true)
	
	//end of corridor doors
	fbi_door[6] = get_hash_key("fbi_1_door_6") 
	add_door_to_system(fbi_door[6], v_ilev_cor_firedoor, <<252.87, -1366.76, 24.55>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[6], 0.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[6], DOORSTATE_force_locked_this_frame, false, true)
	
	fbi_door[7] = get_hash_key("fbi_1_door_7") 
	add_door_to_system(fbi_door[7], v_ilev_cor_firedoor, <<251.10, -1365.28, 24.55>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[7], 0.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[7], DOORSTATE_force_locked_this_frame, false, true)
	
	
	//elevator doors
	fbi_door[8] = get_hash_key("fbi_1_door_8") 
	add_door_to_system(fbi_door[8], v_ilev_cor_doorlift01, <<248.86, -1371.01, 23.52>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[8], 0.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[8], DOORSTATE_force_locked_this_frame, false, true)
	
	fbi_door[9] = get_hash_key("fbi_1_door_9") 
	add_door_to_system(fbi_door[9], v_ilev_cor_doorlift02, <<247.58, -1369.94, 24.52>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[9], 0.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[9], DOORSTATE_force_locked_this_frame, false, true)
	
	//single door shotgun enemy runs through
	fbi_door[10] = get_hash_key("fbi_1_door_10") 
	add_door_to_system(fbi_door[10], v_ilev_cor_firedoorwide, <<243.51, -1367.83, 29.66>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[10], 0.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[10], DOORSTATE_force_unlocked_this_frame, false, true)
	
	//double doors leading up to top floor - left
	fbi_door[11] = get_hash_key("fbi_1_door_11") 
	add_door_to_system(fbi_door[11], v_ilev_cor_firedoor, <<251.10, -1365.28, 29.67>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[11], 1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[11], DOORSTATE_force_locked_this_frame, false, true)
	
	//double doors leading up to top floor - right
	fbi_door[12] = get_hash_key("fbi_1_door_12")
	add_door_to_system(fbi_door[12], v_ilev_cor_firedoor, <<252.87, -1366.76, 29.67>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[12], -1.0, false, false)
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[12], DOORSTATE_force_unlocked_this_frame, false, true)
	
	//double doors top floor operating table room
	fbi_door[13] = get_hash_key("fbi_1_door_13") 
	add_door_to_system(fbi_door[13], v_ilev_cor_firedoor, <<252.87, -1366.76, 39.55>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[13], -1.2, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[13], DOORSTATE_force_locked_this_frame, false, true)
	
	fbi_door[14] = get_hash_key("fbi_1_door_14")
	add_door_to_system(fbi_door[14], v_ilev_cor_firedoor, <<251.10, -1365.28, 39.55>>)
	DOOR_SYSTEM_SET_OPEN_RATIO(fbi_door[14], 1.2, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(fbi_door[14], DOORSTATE_force_locked_this_frame, false, true)

endproc 

proc load_repeat_play_assets()
			
	request_model(get_player_ped_model(char_michael))
	
	while not has_model_loaded(get_player_ped_model(char_michael))
	
		wait(0)
		
	endwhile 
	
	set_current_selector_ped(selector_ped_michael)
	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<-444.1117, 1060.1145, 326.6852>>)

	
	REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fbi_1_int", CS_SECTION_2|CS_SECTION_3|CS_SECTION_4|CS_SECTION_5|CS_SECTION_6)

	while not has_cutscene_loaded()
	
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", PLAYER_PED_ID())
	
		wait(0)
		
	endwhile 
	
	
	start_cutscene()
	
	SET_CUTSCENE_FADE_VALUES(false, false, true)
	
	
	while not is_cutscene_playing()
	
		wait(0)
		
	endwhile 
	
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-411.094, 1180.761, 324.342>>, <<-414.034, 1171.203, 330.642>>, 13.00, <<-410.9237, 1174.4790, 324.6415>>, 253.2136, <<15.0, 15.0, 15.0>>)//, false)
	set_players_last_vehicle_to_vehicle_gen(<<-410.9237, 1174.4790, 324.6415>>, 253.2136)

	clear_area(get_entity_coords(player_ped_id()), 1000, true)
	
	intro_mocap_status = 1
	
	do_screen_fade_in(default_fade_time)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Start of mission")
	
endproc 

proc mission_setup()
	
	#IF IS_DEBUG_BUILD
		load_lk_widgets()
		set_locates_header_widget_group(fbi1_widget_group)
	#endif
	
	SUPPRESS_EMERGENCY_CALLS()
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(false)
	
	//INFORM_MISSION_STATS_OF_MISSION_START_fbi_one()
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
	
	set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_NORMAL, true)
	
	load_text_and_dialogue()
	
	add_relationship_groups()
	
	//cover point for player at the left wall
	add_cover_point(<<267.63803, -1350.05750, 23.53772>>, 139.1686, covuse_walltoleft, covheight_toohigh, COVARC_180) 
	add_cover_point(<<281.09470, -1344.61365, 23.53772>>, 49.3918, covuse_walltoright, covheight_toohigh, COVARC_180, true) 
	
	add_cover_point(<<245.97498, -1367.51013, 23.53797>>, 137.1840, covuse_walltoleft, covheight_toohigh, COVARC_180) 
	add_cover_point(<<242.58643, -1364.72803, 23.53797>>, 137.2854, covuse_walltoright, covheight_toohigh, COVARC_180)
	add_cover_point(<<247.16701, -1366.89417, 38.53422>>, 139.2864, covuse_walltoleft, covheight_toohigh, COVARC_180) 
	//add_cover_point(<<237.7, -1357.65527, 38.53422>>, 230.8, covuse_walltoleft, covheight_toohigh, COVARC_180) 
	add_cover_point(<<237.57045, -1357.65686, 38.53421>>, 230.8, covuse_walltoleft, covheight_toohigh, COVARC_180)
	//add_cover_point(<<236.36862, -1359.08789, 38.53422>>, 230.8070, covuse_walltoright, covheight_toohigh, COVARC_180)
	add_cover_point(<<236.40781, -1359.09778, 38.53421>>, 230.8070, covuse_walltoright, covheight_toohigh, COVARC_180)
	add_cover_point(<<234.05081, -1361.90210, 38.53421>>, 230.8812, covuse_walltoleft, covheight_toohigh, COVARC_180) 
	
		
	cia_enemy_cover_point[0] = add_cover_point(<<272.05145, -1344.09839, 23.53782>>, 232.800, covuse_walltoright, covheight_toohigh, COVARC_180) 
	cia_enemy_cover_point[3] = add_cover_point(<<258.43, -1355.88, 23.54>>, 324.4275, covuse_walltoleft, COVHEIGHT_low, COVARC_180) 
	cia_enemy_cover_point[4] = add_cover_point(<<262.65381, -1358.82007, 23.53772>>, 49.0454, covuse_walltoleft, COVHEIGHT_low, COVARC_180) 
	cia_enemy_cover_point[6] = add_cover_point(<<258.29, -1361.33, 23.54>>, 315.0264, covuse_walltoright, covheight_toohigh, COVARC_180)
	cia_enemy_cover_point[16] = add_cover_point(<<280.02, -1344.69, 23.54>>, 322.6696, covuse_walltoleft, covheight_toohigh, COVARC_180)  
	
	cia_enemy_cover_point[4] = add_cover_point(<<260.62198, -1352.67603, 23.53772>>, 322.8431, covuse_walltoleft, covheight_toohigh, COVARC_180)  										
											
											
	cia_enemy_top_floor_cover_point[0] = add_cover_point(<<242.27132, -1367.46399, 38.53431>>, 234.1995, covuse_walltoright, covheight_toohigh, COVARC_180)
	//cia_enemy_top_floor_cover_point[1] = add_cover_point(<<238.54, -1368.56, 38.24>>, 50.0000, covuse_walltoleft, covheight_toohigh, COVARC_180) 
	cia_enemy_top_floor_cover_point[2] = add_cover_point(<<239.49130, -1379.23584, 38.53422>>, 0.0, COVUSE_WALLTOBOTH, COVHEIGHT_low, COVARC_180) //covuse_walltoright 
	
	
	//cia_enemy_top_floor_cover_point[0] = cia_enemy_top_floor_cover_point[0]
	//cia_enemy_top_floor_cover_point[1] = cia_enemy_top_floor_cover_point[1]
	//cia_enemy_top_floor_cover_point[2] = cia_enemy_top_floor_cover_point[2] 
	
	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)

	//reset_glass_window()
	window.been_created = false
	
	initialise_mission_variables()
	
	initialise_mission_doors()
	
	if is_replay_in_progress() 
	
		replay_active = true 
	
		if not g_bShitskipAccepted
	
			switch Get_Replay_Mid_Mission_Stage()
			
				case 0
				
					load_trip_skip_data_0() //start of mission - player on operating table
				
				break 
				
				case 1
				
					load_trip_skip_data_1()	//stealth the cia guard
				
				break 
			
				case 2
				
					load_trip_skip_data_2()	//start of shootout - player in dead bodies room 

				break 
				
				case 3
				
					load_trip_skip_data_3() //half way through shootout 

				break 
				
				case 4
				
					load_trip_skip_data_4() //outside coroners escape police.  
				
				break 
			
			endswitch 
			
		else 
		
			int shit_skip_status 
			
			shit_skip_status = Get_Replay_Mid_Mission_Stage() + 1
			
			switch shit_skip_status 
			
				case 1
			
					load_trip_skip_data_1()
			
				break 
				
				case 2
				
					load_trip_skip_data_2()
				
				break 
				
				case 3
				
					load_trip_skip_data_3()
				
				break 
				
				case 4
				
					load_trip_skip_data_4()
				
				break
				
				case 5
				
					load_trip_skip_data_5()
					
				break 
				
			endswitch 
		
		endif 
		
		replay_active = false 
			
	else 
		STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(player_ped_id()) //
	
		if not is_repeat_play_active()
	
			if not is_screen_faded_in()
				if not is_screen_fading_in()
					do_screen_fade_in(default_fade_time)
				endif 
			endif 
			
		else
		
			load_repeat_play_assets()
		
		endif 
		
	endif 

endproc 

proc setup_assets_after_locker_mocap()

	disable_cellphone(false)

	remove_anim_dict("move_crouch_proto")

	//setup_weapon_for_ped(player_ped_id(), weapontype_combatpistol, 20, false, false)
	
	clear_ped_tasks_immediately(cia_stealth.ped)
	set_entity_coords(cia_stealth.ped, <<287.5420, -1345.9211, 23.5380>>)
	set_entity_heading(cia_stealth.ped, 227.9037)
	SET_PED_CAPSULE(cia_stealth.ped, 0.0)
	force_ped_ai_and_animation_update(cia_stealth.ped)

	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<275.7152, -1337.0142, 23.5377>>)
	set_entity_heading(player_ped_id(), 241.8949)
	SET_PED_STEALTH_MOVEMENT(player_ped_id(), true)
	force_ped_motion_state(player_ped_id(), MS_STEALTHMODE_IDLE) 
	force_ped_ai_and_animation_update(player_ped_id())
	
	trigger_music_event("FBI1_GET_GUN")
			
	if is_screen_faded_in()
		end_cutscene_no_fade(false)
	else 
		end_cutscene(false)
	endif 
	
	print_now("FBI1_God_11", default_god_text_time, 1)
	
	//fail_time = get_game_timer() 
	
	mission_flow = stealth_the_cia_officer
	
endproc 

proc reset_clothes_cutscene()

	clear_ped_tasks_immediately(player_ped_id())
	set_entity_coords(player_ped_id(), <<275.0209, -1337.7212, 23.5377>>)
	
	destroy_all_cams()
	
	delete_ped(cia_stealth.ped)
	
	#if is_debug_build
		widget_reset_cutscene = false
	#endif 

endproc 

proc fbi1_stealth_the_cia_officer()

	DEACTIVATE_VENDING_MACHINES_THIS_FRAME()
	
	if is_ped_in_specific_room(player_ped_id(), <<288.9353, -1347.9430, 23.5378>>, "BodyStorage", "v_coroner")   
		disable_control_action(PLAYER_CONTROL, INPUT_JUMP) 
	endif 

//	SET_PLAYER_SNEAKING_NOISE_MULTIPLIER(player_id(), 0.1)
//	SET_PLAYER_NOISE_MULTIPLIER(player_id(), 0.3)

	if is_ped_injured(cia_stealth.ped)

		remove_anim_dict("missfbi1ig_7")
		
		remove_waypoint_recording("fbi1_0")
		
		REPLAY_RECORD_BACK_FOR_TIME(3, 2.0, REPLAY_IMPORTANCE_NORMAL)
	
		//set_ped_weapon_visible
	
		//SET_PED_CURRENT_WEAPON_VISIBLE(cia_stealth.ped, false, true, false)
		
//		give_weapon_to_ped(player_ped_id(), weapontype_combatpistol, 50) 
//		GIVE_WEAPON_COMPONENT_TO_PED(player_ped_id(), weapontype_combatpistol, WEAPONCOMPONENT_AT_PI_FLSH)

		if does_blip_exist(cia_stealth.blip)
			remove_blip(cia_stealth.blip)
		endif 
		
		dead_body[0].blip = add_blip_for_entity(dead_body[0].ped) //(add_blip_for_coord(get_entity_coords(dead_body[0].ped))
		set_blip_colour(dead_body[0].blip, blip_colour_green)
		set_blip_scale(dead_body[0].blip, 0.5)
		SET_BLIP_NAME_FROM_TEXT_FILE(dead_body[0].blip, "FBI1_God_13")
		
		dead_body[1].blip = add_blip_for_entity(dead_body[1].ped)//add_blip_for_coord(get_entity_coords(dead_body[1].ped))
		set_blip_colour(dead_body[1].blip, blip_colour_green)
		set_blip_scale(dead_body[1].blip, 0.5)
		SET_BLIP_NAME_FROM_TEXT_FILE(dead_body[1].blip, "FBI1_God_13")
		
		trigger_music_event("FBI1_FIND_BODY")
		
		if IS_AUDIO_SCENE_ACTIVE("FBI_1_STEALTH_KILL_AGENT")
			STOP_AUDIO_SCENE("FBI_1_STEALTH_KILL_AGENT")
		endif 
		
		start_audio_scene("FBI_1_INSPECT_BODIES")
		
		print_now("FBI1_God_1", default_god_text_time, 1)
		
		original_time = get_game_timer()
		
		mission_flow = find_bodies
		
	else 
	
		//bypass find dead bodis stage if the player decides to walk round the corner in the labs where the cia enemies are
		if not is_entity_in_angled_area(player_ped_id(), <<284.463, -1354.515, 23.538>>, <<294.471, -1342.418, 26.538>>, 18.600)  
		and not is_entity_in_angled_area(player_ped_id(), <<277.116, -1340.988, 23.537>>, <<283.480, -1333.405, 26.537>>, 11.00)
			
			int i
			
			for i = 0 to count_of(dead_body) - 1
				if does_blip_exist(dead_body[i].blip)
					remove_blip(dead_body[i].blip)
				endif 
			endfor 
			
			phone_call_system_status = 3
			
			set_player_can_use_cover(player_id(), true)
						
			DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, false)
			
			bypass_find_dead_bodies_stage = true
			
			mission_flow = shootout
						
			exit

		endif 
	
	endif 
	
	cia_stealth_enemy_ai_system()
	
	get_weapons_back_system()
	
	give_player_weapon_component_system()
	
endproc 

proc fbi1_load_stage_selector_assets()

	int i = 0

	initialise_mission_variables()

	wait(0)//removing and adding the doors in one frame will not allow the doors to be added. A wait 0 is required
	
	initialise_mission_doors()
	
	switch launch_mission_stage_menu_status
	
		case 0
		
			load_normal_start_of_mission()

		break 
		
		case 1
		
			load_trip_skip_data_1()
		
		break 
		
		case 2
		
			load_trip_skip_data_2()

		break 
		
		case 3
		
			load_trip_skip_data_3()
		
		break 
		
		case 4
				
			request_model(ambient_car[0].model)
			request_model(ambient_car[1].model)
			request_model(cia_enemy_outside[0].model)
			request_model(V_ILev_Cor_windowSmash)
			request_model(bin[0].model)
			
			request_anim_dict("missfbi1")
			
			request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
			
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)
			
			set_building_state(BUILDINGNAME_IPL_CORONER_INTERIOR, BUILDINGSTATE_DESTROYED) 

			coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner")

			while not is_valid_interior(coroner_interior)
				coroner_interior = get_interior_at_coords_with_type(<<279.9322, -1337.4902, 23.7419>>, "v_coroner") 
				wait(0)
			endwhile 
			
			pin_interior_in_memory(coroner_interior)
			
			while not has_model_loaded(ambient_car[0].model)
			or not has_model_loaded(ambient_car[1].model)
			or not has_model_loaded(cia_enemy_outside[0].model)
			or not has_model_loaded(V_ILev_Cor_windowSmash)
			or not has_model_loaded(bin[0].model)
			or not has_anim_dict_loaded("missfbi1")
			or not request_ambient_audio_bank("SCRIPT\\FBI_01_DMW_02")
			or not is_interior_ready(coroner_interior)

				wait(0)
				
				draw_rect(0.5, 0.5, 1, 1, 0, 0, 0, 255)
				
			endwhile

			clear_area(<<234.2708, -1360.3062, 44.0340>>, 300.00, true)

			clear_ped_tasks_immediately(player_ped_id())
			set_entity_visible(player_ped_id(), true)
			set_entity_coords(player_ped_id(), <<239.4876, -1360.4357, 38.5342>>)
			set_entity_heading(player_ped_id(), 50.2059)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 19, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 0, 0)	
			//clear_all_ped_props(PLAYER_PED_ID())
			remove_player_ped_props(player_ped_id())
			
			setup_player_fail_weapon(player_ped_id(), weapontype_combatpistol, 50)
			
			CREATE_MODEL_SWAP(<<236.03, -1357.49, 40.45>>, 1.0, V_ILev_Cor_windowSolid, V_ILev_Cor_windowSmash, TRUE)	
			window.obj = GET_CLOSEST_OBJECT_OF_TYPE(<<236.03, -1357.49, 40.45>>, 1.0, V_ILev_Cor_windowSmash)
					
			if does_entity_exist(window.obj)
				window.been_created = true 
			endif
			
			for i = 0 to count_of(ambient_car) - 1
				ambient_car[i].veh = create_vehicle(ambient_car[i].model, ambient_car[i].pos, ambient_car[i].heading)
				set_vehicle_doors_locked(ambient_car[i].veh, vehiclelock_locked_but_can_be_damaged)
			endfor
			
			bin[0].obj = create_object(bin[0].model, bin[0].pos)
			set_entity_rotation(bin[0].obj, bin[0].rot)
			freeze_entity_position(bin[0].obj, true)
			
			trigger_alarm(true)
			
			trigger_music_event("FBI1_SHOOTOUT_HALFWAY_RT")
			
			load_scene(<<234.2708, -1360.3062, 44.0340>>)
			
			end_cutscene()
			
			special_ability_help_text_system_status = 3 //deactivates help text

			phone_call_system_status = 3
			
			music_event_system_status = 2

			fbi1_master_controller_status = 6
			
			mission_flow = shootout
		
		break 
		
		case 5
		
			load_trip_skip_data_4()
		
		break 
		
		case 6
		
			load_trip_skip_data_5()
		
		break 
		
	endswitch 

endproc 

proc fbi1_mission_fail_stage()
				
	//process anything that needs to run untill game is faded out.
	
	//stops the player doing a take down
	if not is_ped_injured(cia_stealth.ped)
		set_ped_reset_flag(cia_stealth.ped, PRF_PreventAllMeleeTakedowns, true)
	endif 
	
	IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
	
		if operating_table_fail
	
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<338.3677, -1394.1179, 31.5093>>, 61.6520)//hospital 
			
		else 
		
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-413.3700, 1168.5259, 324.8537>>, 156.3851)
		
		endif 
		
		mission_failed()
		
	endif 
	
endproc  
				
proc setup_mission_for_force_cleanup()

	//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<760.5546, -983.6993, 24.7744>>, 82.4206)

endproc 

script
	
	set_mission_flag(true)
	
	if has_force_cleanup_occurred()
	
		Store_Fail_Weapon(player_ped_id(), enum_to_int(get_current_player_ped_enum()))
		mission_flow_mission_force_cleanup()
		reset_glass_window()
		
		//restores the players original weapons that were cached at the begining of the mission via 
		//STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT. The weapon globals here are nothing to do with the weapon globals 
		//used in SET_REPLAY_MID_MISSION_STAGE_WITH_NAME
		if not player_weapons_restored
			RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL)
		endif
		
		if is_ped_injured(player_ped_id())
			g_RestoreSnapshotWeaponsOnDeath = TRUE
		endif
		
		mission_cleanup()
	endif
	
	mission_setup()//sets up all components required from the start of the mission to the end.
	

	while true

		wait(0)
		
		if not stop_mission_fail_checks
			if mission_fail_checks()
				setup_mission_fail()
			endif 
		endif
		
		skip_system()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_DeadManWalking")

		switch mission_flow 
		
			case intro_mocap
			
				fbi1_intro_mocap()

			break 
			
			case stealth_cutscene
			
				fbi1_stealth_cutscene()
			
			break 
			
			case stealth_the_cia_officer
			
				fbi1_stealth_the_cia_officer()
			
			break 
			
			case find_bodies
				
				fbi1_find_bodies()
			
			break 

			case shootout
			
				fbi1_shootout()
			
			break 
			
			case escape_cutscene
			
				fbi1_escape_cutscene()
				
			break 
			
			case get_to_hills
			
				fbi1_get_to_hills()
			
			break 
			
			case mission_fail_stage

				fbi1_mission_fail_stage()

			break 
			
			case load_stage_selector_assets

				fbi1_load_stage_selector_assets()
				
			break 
	
		endswitch 
		
		
		#IF IS_DEBUG_BUILD
		
			//is_entity_in_angled_area(player_ped_id(), <<267.960, -1350.535, 23.538>>, <<264.483, -1347.523, 26.538>>, 2.0)
		
//			if is_keyboard_key_just_pressed(key_t)
//				
//				SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_TORSO, 18, 0)
//				
//				while not has_ped_preload_variation_data_finished(player_ped_id()) 
//					printstring("waiting on assets loading")
//					printnl()
//					wait(0)
//				endwhile
//				
//			endif 

			
			
		
//			if does_entity_exist(bin_bag[1].obj)
//				printvector(get_entity_coords(bin_bag[1].obj))
//				printnl()
//			endif 
		
//			if does_entity_exist(dead_body[0].ped)
//				printvector(get_entity_coords(dead_body[0].ped, false))
//				printnl()
//			endif 
//			
//			if does_entity_exist(dead_body[1].ped)
//				printvector(get_entity_coords(dead_body[1].ped, false))
//				printnl()
//			endif 

			angled_area_locate_widget(player_ped_id())
			
			//play_ptfx_on_truck_widget()
			
			//create_and_move_object(explosive_barrels[0].obj)
			
//			create_and_move_object(bin_bag[1].obj)
			//create_and_move_object_2(bins[1].obj)
			
//			if not does_entity_exist(bin[0].obj)
//				
//				request_model(bin[0].model)
//				
//				if has_model_loaded(bin[0].model)
//				
//					bin[0].obj = create_object(bin[0].model, bin[0].pos)
//
//				endif 
//				
//			endif 
//			
//			create_and_move_object_2(bin[0].obj)

//			relative_pitch_and_heading_widget()

			pfx_widget()

			if is_keyboard_key_just_pressed(key_s)
				end_cutscene_no_fade()
				set_entity_visible(player_ped_id(), true)
				set_entity_collision(player_ped_id(), true)
				freeze_entity_position(player_ped_id(), false)
				mission_passed()
			endif
		
			if not stop_mission_fail_checks
				if is_keyboard_key_just_pressed(key_f)
					mission_failed_text = ""
					setup_mission_fail()
				endif
			endif 
			
			//angled_area_locate_widget(players_truck.veh)
			
			//is_entity_in_angled_area(player_ped_id(), <<252.429, -1367.320, 23.247>>, <<250.590, -1365.777, 26.72>>, 1.5)
			
		#endif 

	endwhile 
	
endscript 

//CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED


//line 1711

//278.25, -1335.96, 24.25

//line 2002

//258.0359, -1344.9565, 23.2419

//int_office_interior = get_interior_at_coords_with_type(<<57.3878, -1346.0355, 23.7>>, "v_coroner") 
//pin_interior_in_memory(coroner_interior)
//
//while not is_interior_ready(int_office_interior)
//	wait(0)
//endwhile

//Target pos = 1612.0624, -1937.1222, 100.6656

//139249


//INDEX 		speaker id	character		dialogue line
//
//15			6	        CIA1			cia_attak0
//2			8			CIA3			cia_attak2
//9			B			CIA5			cia_attak4	
//11			A			CIA4			cia_attak3
//12			7			CIA2			cia_attak1
//
//tfcia 1 	C			CIA6			cia_attak5


//Bug Change: 373726 - dead bodies bug 
//369947 - animation to ragdoll command 

//dialogue bug with fbi1_doc3 playing during mocap 731786


//794153  - door bug

//787629 - window bug

//SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 

//weapon object - 907536 

//camera 

//ANIM reactions for doctor when player moves = 893061

//CORPSE VARIATIONS CHANGE - 839479

//jumping out of window bug = 968403

// no scene handles = 1017792

//1101462 = blip not existing. 

//1093583 - dialogue not being killed on wake up mocap

//1169985 - player dropping out issue when climbing out of window.






//proc what_the_fuck_anim_system()
//
//	switch what_the_fuck_anim_system_status 
//
//		case 0
//		
//			if any controler stick pressed (dead zones)
//				kill the dialogue immediately 
//				
//				play wtf synched scene 
//				
//				what_the_fuck_anim_system_status++
//				
//		break 
//		
//		case 1
//		
//			if create_conversation wtf dialogue line 
//			
//				what_the_fuck_anim_system_status++
//			
//			endif 
//		
//		break 
//		
//		case 2
//		
//			if wtf anim >= 1.0
//				play loop
//				
//				what_the_fuck_anim_system_status++
//		
//		break
//		
//		case 3
//		
//			if any controler stick pressed (dead zones)
//			
//				play run away anim
//				
//			else 
//			
//				if are_strings_equal(currently_playing_conversation_root, fbi1_conv_1) 
//					
//					operating_table_anim_system_status = 3 
//					
//				elif are_strings_equal(currently_playing_conversation, fbi1_conv_2
//					
//					operating_table_anim_system_status = 5
//					
//				elif are_strings_equal(currently_playing_conversation, fbi1_conv_3
//				
//					operating_table_anim_system_status = 7
//					
//				elif are_strings_equal(currently_playing_conversation, fbi1_conv_4
//				
//					operating_table_anim_system_status = 10
//					
//				endif 
//				
//			endif 
//			
//		
//		break 
//		
//		case 4
//		
//		break 
//		
//	endswitch 
//	
//endproc 
//
//proc operating table anim system()
//
//	switch operating_table_anim_system  
//	
//		case 0
//
//			if intro zip anim >= 1.0
//				if play synched scene fbi1_conv_1
//					create_conversation fbi1_conv_1 //will work the first time as no dialogue will be playing 100%
//					
//					currently_playing_conversation = "fbi1_conv_1"
//					
//				endif 
//			endif 
//			
//		break 
//		
//		case 1
//		
//			if fbi1_conv_1 anim >= 1.0
//				play idle anim
//				task_synchonized_scene_looped
//			endif 
//		
//			if not is_any_text_being_displayed
//				if create_conversation fbi1_conv_2
//					
//					play synched scene anims fbi1_conv_2
//					
//					currently_playing_conversation = "fbi1_conv_2"
//					
//					operating_table_anim_system_status++
//					
//				endif 
//			endif 
//		
//		break 
//		
//	endswitch 
//	
//endproc 
//
//what_the_fuck_anim_system()
//
//operating table anim system 


//break entity glass no particles issue - 1156519

//dialogue progression issue - 1152294	


//SET_PED_HEARING_RANGE
//
//SET_PLAYER_SNEAKING_NOISE_MULTIPLIER
//SET_PLAYER_NOISE_MULTIPLIER

//1261774 - BREAK ENTITY GLASS BUG

//PCF_ShouldChargeNow

//1313469 
//bugs with peds going through doors in combat
//Can you try setting CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS to FALSE for these guys 
//and also CCF_STRAFE_WHEN_MOVING_CHANCE to 1.0? 


//1430614 - has_ped_preload_variation_data_finished(player_ped_id()) not working

//1513806 - wake up help text is not rendering.

		
		
			









	
	
	
	
