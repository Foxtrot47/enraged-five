
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"

USING "CompletionPercentage_public.sch"

using "dialogue_public.sch"

USING "flow_public_core_override.sch"
USING "flow_public_GAME.sch"

using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "replay_public.sch"

USING "script_blips.sch"
USING "script_heist.sch"
USING "script_ped.sch"

USING "selector_public.sch"

USING "flow_special_event_checks.sch"

USING "RC_Area_public.sch"
USING "RC_helper_functions.sch"
USING "RC_threat_public.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 3
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Template.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_AFTER_MOCAP                0 //following beverly down the road

//-----------------------
//		CONSTANTS
//-----------------------

//POLY TEST CONST
CONST_INT MAX_POLY_TEST_VERTS		8		//poly test
//Ambient encounter speech
CONST_INT MAX_AMBIENT_CONV_LINES	8
CONST_INT MAX_INVALIDMODS			2

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
//	MS_LEAVE,					//1
//	MS_GET_PETROL,				//2
	MS_LEAVE_AREA,				//3
	MS_FAILED					//5
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages
//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************

//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints
BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state

//GOD TEXT
BOOL bObjectiveShown = FALSE

///MISSION PED VARS SCRIPT AI

INT iMissTimer = 0 //Generic timer

BOOL bScriptCleanedupCar = FALSE

VEHICLE_SETUP_STRUCT mChosenCarSetup

BLIP_INDEX biBlip
///==============| GOD TEXT BOOLS |============
BOOL bGunShopHelp = FALSE

///===============| models |===================

/// ===============| START VECTORS |==============
VECTOR vStartPos
/// ==============| START HEADINGS |===============
FLOAT fHeading
/// ==============| PED INDICES |================


/// ==============| VEHICLE INDICES |===========
//VEHICLE_INDEX viChosenCar

/// ===============| GROUPS |========================

/// ===============| DIALOGUE |======================
STRING sTextBlock = "FBIPRAU"

//Used for tracking an interupted conversation

/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()
	
	
ENDPROC

///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD

	BOOL bShowDebugText = TRUE
	WIDGET_GROUP_ID widgetGroup
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	
	ENDPROC
	
	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
//	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)
//
//		VECTOR vBottom[2]
//		VECTOR vTop[2]
//		
//		vBottom[0] = vec1
//		vBottom[1] = vec2
//		
//		vTop[0] = vec1
//		vTop[1] = vec2
//		
//		IF vec1.z > vec2.z
//			vBottom[0].z = vec2.z
//			vBottom[1].z = vec2.z
//			vTop[0].z = vec1.z
//			vTop[1].z = vec1.z
//		ELSE
//			vBottom[0].z = vec1.z
//			vBottom[1].z = vec1.z
//			vTop[0].z = vec2.z
//			vTop[1].z = vec2.z
//		ENDIF
//
//		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
//		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
//		VECTOR w = side * (width / 2.0)
//
//		// Bottom points
//		VECTOR c1 = vBottom[0] - w  // base left
//		VECTOR c2 = vBottom[0] + w  // base right              
//		VECTOR c3 = vBottom[1] + w  // top rt
//		VECTOR c4 = vBottom[1] - w  // top lt
//
//		// Top points
//		VECTOR d1 = vTop[0] - w  // base left
//		VECTOR d2 = vTop[0] + w  // base right              
//		VECTOR d3 = vTop[1] + w  // top rt
//		VECTOR d4 = vTop[1] - w  // top lt
//
//		// Draw bottom lines
//		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
//		// Draw top lines
//		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
//		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
//		// Draw uprights
//		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
//		
//	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Prints objective to the screen if the objective hasnt already been printed
///    Checks and expired bObjectiveShown
/// PARAMS:
///    objstr - The text key to print
PROC PRINT_OBJ(String objstr)
	IF NOT bObjectiveShown
		PRINT_NOW(objstr, DEFAULT_GOD_TEXT_TIME,0)
		bObjectiveShown = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  SK_PRINT_INT(_debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD
			SK_PRINT("MODEL LOADED")
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_OK(vehicleindex)
			SET_ENTITY_COORDS(vehicleindex, pos)
			SET_ENTITY_HEADING(vehicleindex, dir)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED()
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the mission check point 
PROC SET_CHECKPOINT()

ENDPROC

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads a scene around a point used to the player 
///    seeing the map stream in when starting a replay or debug skipping
/// PARAMS:
///    pos - the position to stream the scene around at
///    rad - the radius of the scene to load
PROC LOAD_SCENE_SKIP(VECTOR pos, FLOAT rad)
	NEW_LOAD_SCENE_START_SPHERE(pos, rad)
	INT i_load_scene_timer = GET_GAME_TIMER()
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED()) AND (GET_GAME_TIMER() - i_load_scene_timer < 12000)
		WAIT(0)
	ENDWHILE
	NEW_LOAD_SCENE_STOP()
ENDPROC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				REQUEST_ADDITIONAL_TEXT("FBIPRC", MISSION_TEXT_SLOT)
				REQUEST_ADDITIONAL_TEXT(sTextBlock, MISSION_DIALOGUE_TEXT_SLOT)

				IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						viChosenCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//						vStartPos = GET_ENTITY_COORDS(viChosenCar)
//						fHeading = GET_ENTITY_HEADING(viChosenCar)
//					    SET_ENTITY_AS_MISSION_ENTITY(viChosenCar, TRUE)
//						GET_VEHICLE_SETUP(viChosenCar, mChosenCarSetup)
//						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4_PREP_3)	
//						RETURN TRUE
//					ELSE
						IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
						    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
//							viChosenCar = g_sTriggerSceneAssets.veh[0]
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(g_sTriggerSceneAssets.veh[0]) 
							vStartPos = GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[0])
							fHeading = GET_ENTITY_HEADING(g_sTriggerSceneAssets.veh[0])
							GET_VEHICLE_SETUP(g_sTriggerSceneAssets.veh[0], mChosenCarSetup)
							RETURN TRUE
						ENDIF

//					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED") 
			#ENDIF
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sits in the NEXT_STAGE() function and moniters for assest that
///    should be unloaded based on the mission state
/// PARAMS:
///    state - the current state we should evaluate
PROC MONITER_UNLOAD_ASSETS(MISSION_STATE state)
	SWITCH state
		CASE MS_SET_UP
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Unloads all models
PROC UNLOAD_ALL_MODELS()
ENDPROC

/// PURPOSE:
///    Unloads all waypoints 
PROC UNLOAD_ALL_WAYPOINTS()
ENDPROC

/// PURPOSE:
///    Unloads all anims
PROC UNLOAD_ANIMS()
ENDPROC

/// PURPOSE:
///    Unload all vehicle recordings 
PROC UNLOAD_ALL_CAR_RECS()
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBlip)
ENDPROC


/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
//	SAFE_DELETE_VEHICLE(viChosenCar)
ENDPROC

PROC RELEASE_ALL()
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4_PREP_3)	
//	SAFE_RELEASE_VEHICLE(viChosenCar)
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF

	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()
	WAIT_FOR_CUTSCENE_TO_STOP()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	REMOVE_BLIPS()
	UNLOAD_ALL_MODELS()
	UNLOAD_ALL_WAYPOINTS()
	UNLOAD_ANIMS()
	UNLOAD_ALL_CAR_RECS()
	IF bDelAll
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()
//	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT,TRUE)
	SERVICES_TOGGLE(TRUE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	ENDIF
	
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL) 
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	MONITER_UNLOAD_ASSETS(eMissionState)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
		SET_MISSION_VEHICLE_GEN_VEHICLE(g_sTriggerSceneAssets.veh[0], <<0.0,0.0,0.0>>, 0.0, VEHGEN_MISSION_VEH_FBI4_PREP)
	ENDIF
	Mission_Flow_Mission_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_BLIPS()
			
			/* (not supported for story missions atm)
			// set if we want to delay the fade
			BOOL bDelayFade
			bDelayFade = FALSE
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				bDelayFade = TRUE // delay the fade if we failed for one of these reasons
			ENDIF
			*/

			MISSION_FLOW_MISSION_FAILED(TRUE)

			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
//			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				int iTimer
				iTimer = GET_GAME_TIMER()
				PRINT_HELP_FOREVER("PRC_FCAR")
				
				WHILE (GET_GAME_TIMER() - iTimer) < 7500
					WAIT(0)
				ENDWHILE
				CLEAR_HELP()
				RELEASE_ALL()
				Script_Cleanup()
		
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//
//			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
//			ENDIF
		BREAK

	ENDSWITCH
ENDPROC


//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
//	bLoadingFinCutscene = FALSE
	bObjectiveShown = FALSE
	eState = SS_INIT
	CLEANUP() //delete everything
ENDPROC

///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				IF IS_REPEAT_PLAY_ACTIVE()
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
				ENDIF
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
				IF STOP_MY_VEHICLE()
					NEXT_STAGE()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MONITER_PLAYER_MOVING_CAR()
	IF NOT IS_ENTITY_IN_RANGE_COORDS(g_sTriggerSceneAssets.veh[0], vStartPos, 25)
		MISSION_FAILED()
	ELIF NOT IS_ENTITY_IN_RANGE_COORDS(g_sTriggerSceneAssets.veh[0], vStartPos, 10)
		IF NOT DOES_BLIP_EXIST(biBlip)
			biBlip = CREATE_BLIP_FOR_COORD(vStartPos, TRUE)
			PRINT_OBJ("PRC_RTN")
		ENDIF
	ELSE
		IF IS_THIS_PRINT_BEING_DISPLAYED("PRC_RTN")
			CLEAR_PRINTS()
		ENDIF
		SAFE_REMOVE_BLIP(biBlip)
	ENDIF
ENDPROC

PROC MONITOR_PLAYER_MOVING_PETROL()
	IF NOT DOES_PICKUP_OF_TYPE_EXIST_IN_AREA(PICKUP_WEAPON_PETROLCAN, vStartPos, 10)
		IF NOT DOES_BLIP_EXIST(biBlip)
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
				biBlip = CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
				PRINT_OBJ("PRC_GAS")
			ENDIF
		ENDIF
	ELSE
		SAFE_REMOVE_BLIP(biBlip)
	ENDIF
ENDPROC

PROC MONITOR_PLAYER_LEAVING_AREA()
	IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[0], 225)
			bScriptCleanedupCar = TRUE
			SAFE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
		ENDIF
	ELSE
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vStartPos, 200)
			IF bScriptCleanedupCar
				IF SPAWN_VEHICLE(g_sTriggerSceneAssets.veh[0], mChosenCarSetup.eModel, vStartPos, fHeading)
					SET_VEHICLE_SETUP(g_sTriggerSceneAssets.veh[0], mChosenCarSetup)
					bScriptCleanedupCar = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LEAVE()
	SWITCH eState
		CASE SS_INIT	
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE")  #ENDIF
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[0])
					eState = SS_CLEANUP
				ELSE
					MONITER_PLAYER_MOVING_CAR()
				ENDIF
			ELSE
				MISSION_FAILED()
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP LEAVE")  #ENDIF
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH

ENDPROC

PROC SET_CLOSEST_GUN_SHOP_BLIPS_LONG_RANGE(BOOL bOn)
	SET_SHOP_BLIP_LONG_RANGE(GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(PLAYER_PED_ID()), SHOP_TYPE_GUN), bOn)
ENDPROC

PROC GET_PETROL()
	MONITOR_PLAYER_LEAVING_AREA()
	SWITCH eState
		CASE SS_INIT	
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT GET_PETROL")  #ENDIF
			PRINT_OBJ("PRC_GAS")
			bObjectiveShown = FALSE
			SET_CLOSEST_GUN_SHOP_BLIPS_LONG_RANGE(TRUE)
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
				biBlip = CREATE_BLIP_FOR_COORD(vStartPos)
				//CREATE_BLIP_FOR_VEHICLE(viChosenCar, FALSE)
			ENDIF
			iMissTimer = GET_GAME_TIMER()
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
				IF NOT bGunShopHelp
					IF (GET_GAME_TIMER() - iMissTimer) > 2000
						PRINT_HELP("PRC_HGAS" , DEFAULT_GOD_TEXT_TIME)
						bGunShopHelp = TRUE
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vStartPos, 10)
				AND DOES_PICKUP_OF_TYPE_EXIST_IN_AREA(PICKUP_WEAPON_PETROLCAN, vStartPos, 10)
					SAFE_REMOVE_BLIP(biBlip)
					eState = SS_CLEANUP
				ENDIF
			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[0])
					SAFE_REMOVE_BLIP(biBlip)
					MONITER_PLAYER_MOVING_CAR()
				ELSE
					IF NOT DOES_BLIP_EXIST(biBlip)
						biBlip = CREATE_BLIP_FOR_COORD(vStartPos)
						//CREATE_BLIP_FOR_VEHICLE(viChosenCar, FALSE)
					ENDIF
				ENDIF
			ELSE
				IF NOT bScriptCleanedupCar
					MISSION_FAILED()
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP GET_PETROL")  #ENDIF
			SET_CLOSEST_GUN_SHOP_BLIPS_LONG_RANGE(FALSE)
			SAFE_REMOVE_BLIP(biBlip)
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_FAST_CAR_STAT()
	MODEL_NAMES mnTemp = GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[0])
	IF GET_VEHICLE_MODEL_ACCELERATION(mnTemp) > 0.219
	AND GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp) > 45
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ACCELERATION  == ", GET_VEHICLE_MODEL_ACCELERATION(mnTemp)) 
			CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED  == ", GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp)) 
		#ENDIF
		//INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI4P3_SPORTSCAR_PICKED)
	ENDIF

ENDPROC

PROC LEAVE_AREA()
	SWITCH eState
		CASE SS_INIT	
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_AREA")  #ENDIF
			PRINT_OBJ("PRC_LEAVE")
			bObjectiveShown = FALSE
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[0])
					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vStartPos, 60)
					
						eState = SS_CLEANUP
					ENDIF
				ELSE
					MONITER_PLAYER_MOVING_CAR()
					MONITOR_PLAYER_MOVING_PETROL()
				ENDIF
			ELSE
				MISSION_FAILED()
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP LEAVE_AREA")  #ENDIF
			SET_FAST_CAR_STAT()
			GET_PLAYERS_LAST_VEHICLE()
			Script_Passed()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH
ENDPROC

///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED()
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
					i_debug_jump_stage++
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINTSTRING("FORCE CLEAN UP") PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
        Script_Cleanup()
	ENDIF

	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			SWITCH eMissionState
			
				CASE MS_SET_UP
					INITMISSION()
				BREAK

//				CASE MS_LEAVE
//					LEAVE()
//				BREAK
//
//				CASE MS_GET_PETROL
//					GET_PETROL()
//				BREAK
				
				CASE MS_LEAVE_AREA
					LEAVE_AREA()
				BREAK
				
				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK
			ENDSWITCH

			IF eMissionState <> MS_FAILED
				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
