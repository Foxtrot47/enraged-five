//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ste Kerrigan					Date: 15/03/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						FBI4 Mission Intro Cutscene	Script						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_script.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_cutscene.sch"

USING "flow_public_core_override.sch"
USING "flow_help_public.sch"
USING "script_misc.sch"
USING "replay_public.sch"

USING "RC_helper_functions.sch"
USING "commands_recording.sch"



#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 1
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Template.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------


//-----------------------
//		CONSTANTS
//-----------------------



ENUM MISSION_PED_ENUM
	ANDREAS = 0,
	DAVE,
	STEVE,
	MIKE,
	TREV,
	FRANK,
	
	NUM_PEDS
ENDENUM

ENUM MISSION_CAR_ENUM
	MIKE_CAR = 0,
	FRANK_CAR,
	TREV_CAR,
	GENERIC_CAR,
	
	NUM_CARS
ENDENUM

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
	MS_INTRO,					//1
	MS_LEAVE_AREA,
	MS_FAILED					//3
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

/// PURPOSE: Mission requirements used for loading 
///    and creating mission assets
ENUM MISSION_REQ
	RQ_NONE,
	RQ_TEXT,
	RQ_CUTSCENE,
	RQ_MIKE,
	RQ_FRANK,
	RQ_TREV,
	RQ_STEVE,
	RQ_ANDREAS,
	RQ_DAVE,
	RQ_DAVES_PHONE,
	RQ_MIKE_CAR,
	RQ_FRANK_CAR,
	RQ_TREV_CAR
ENDENUM

/// PURPOSE: Fail reason enums for picking correct fail reason
ENUM FAILED_REASONS
	FR_NONE,
	FR_MIKE_KILLED,
	FR_MIKE_THREATENED
ENDENUM


ENUM MIKE_STATES
	MSS_NULL,
	MSS_WAIT_SMOKE,
	MSS_SMOKE,
	MSS_WAIT_PHONE,
	MSS_PHONE,
	MSS_IDLE
ENDENUM

MIKE_STATES eMikeState = MSS_NULL

///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages
enumCharacterList eStartedPlayer
//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************


//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints
BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state

//Fail vars
STRING sFailReason = NULL				//String to display when mission is failed

//Cutscene
BOOL bLoadingFinCutscene = FALSE

//ScriptCamera
CAMERA_INDEX camMain					//Script camera used in place holder cutscenes

VECTOR vSafeVec = <<0,0,0>>				//safe vector used when a proper position isnt needed 

//PEDS
PED_INDEX piPeds[NUM_PEDS]
VEHICLE_INDEX viCars[NUM_CARS]

//Object
OBJECT_INDEX	oiDavesPhone

INT iMissionTimer = -1

INT iDialogueTimer = -1

INT iMikeTimer = -1

structPedsForConversation s_conversation_peds		//conversation struct


/// ===============| DIALOGUE |======================


/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()

ENDPROC


BOOL bRegisteredFrank = FALSE
BOOL bRegisteredTrev = FALSE
///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD

	BOOL bShowDebugText = TRUE
	WIDGET_GROUP_ID widgetGroup
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	
	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Set a peds position and heading safely ie check its ok then move it and set its heading
/// PARAMS:
///    index - The ped to move
///    pos - The position to move it to
///    dir - The Heading to set
PROC SET_PED_POS(PED_INDEX index, VECTOR pos, FLOAT dir)
	IF IS_PED_UNINJURED(index)
		CLEAR_PED_TASKS(index)
	ENDIF

	SAFE_TELEPORT_ENTITY(index, pos, dir)
ENDPROC

/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  SK_PRINT_INT(_debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD
			SK_PRINT("MODEL LOADED")
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn - If true we unload the model after spawning the ped
///    bFreeze - if true we freeze the ped after spawning
///    bTempEvents - if true we block temp events
///    bTargetable - if false we set the ped to not be targetted
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir,   BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bTempEvents = TRUE, BOOL bTargetable = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			IF IS_PED_UNINJURED(pedindex)
				#IF IS_DEBUG_BUILD SK_PRINT("PED CREATED !") #ENDIF
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, bTempEvents)
				ENDIF
				
				IF NOT bTargetable
					SET_PED_CAN_BE_TARGETTED(pedindex, bTargetable)
				ENDIF
				

				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(pedindex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_MODEL_IS_SUPPRESSED(model, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_OK(vehicleindex)
			SET_ENTITY_COORDS(vehicleindex, pos)
			SET_ENTITY_HEADING(vehicleindex, dir)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a ped inside a vehicle
/// PARAMS:
///    pedindex - The index to write the newly created ped back to (ref)
///    Car - The vehicle to create the ped in
///    modelName - The model of the ped to create
///    seat - the seat to create the ped in 
///    bUnloadAfterSpawn - should we unload the ped model after creation
/// RETURNS:
///    TRUE if the ped was spawned correctly
FUNC BOOL SPAWN_PED_IN_VEHICLE(PED_INDEX &pedindex, VEHICLE_INDEX Car, MODEL_NAMES modelName, VEHICLE_SEAT seat = VS_DRIVER, BOOL bUnloadAfterSpawn = TRUE)
	IF IS_VEHICLE_OK(Car)
		IF NOT DOES_ENTITY_EXIST(pedindex)
			IF REQUEST_AND_CHECK_MODEL(modelName,"Loading")
				pedindex = CREATE_PED_INSIDE_VEHICLE(Car, PEDTYPE_MISSION, modelName, seat)
				
				IF DOES_ENTITY_EXIST(pedindex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(modelName)
					ENDIF
					RETURN TRUE
				ENDIF

			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

PROC REQUEST_VARIATIONS()
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		#IF IS_DEBUG_BUILD SK_PRINT("CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY") #ENDIF
		switch get_current_player_ped_enum() 
			case CHAR_MICHAEL
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID())
				ENDIF
			break

			case CHAR_FRANKLIN      
				IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[MIKE])
				AND IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[TREV])
				AND IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", g_sTriggerSceneAssets.ped[MIKE])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("trevor", g_sTriggerSceneAssets.ped[TREV])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
				ENDIF
			break 

			case CHAR_TREVOR
				IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[MIKE])
				AND IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("trevor", PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", g_sTriggerSceneAssets.ped[MIKE])
				ENDIF
			break
		
		ENDSWITCH
		
		IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[ANDREAS])
		AND IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[DAVE])
		AND IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[STEVE])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Andreas_Sanchez", g_sTriggerSceneAssets.ped[ANDREAS])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Dave_FBI", g_sTriggerSceneAssets.ped[DAVE])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("steve_FBI", g_sTriggerSceneAssets.ped[STEVE])
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail = FR_NONE)
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	SWITCH fail		
		CASE FR_NONE
			
		BREAK

		CASE FR_MIKE_KILLED
			sFailReason = "CMN_MDIED"
		BREAK

		CASE FR_MIKE_THREATENED
			sFailReason = "INT_THREAT"
		BREAK
	ENDSWITCH
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	SWITCH missionReq
		CASE RQ_NONE
			IF ARE_VECTORS_ALMOST_EQUAL(pos, vSafeVec)
			AND dir = 0.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_CUTSCENE
			IF NOT IS_REPLAY_IN_PROGRESS()
				IF HAS_CUTSCENE_LOADED()
					#IF IS_DEBUG_BUILD SK_PRINT("HAS_CUTSCENE_LOADED()") #ENDIF
					RETURN TRUE
				ENDIF
				IF eStartedPlayer = CHAR_FRANKLIN
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_4_INT", cs_section_4|cs_section_5| cs_section_6|CS_SECTION_7)
				ELIF eStartedPlayer = CHAR_TREVOR
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_4_INT", cs_section_3 | cs_section_4| cs_section_5| cs_section_6|CS_SECTION_7)
				ELSE
					REQUEST_CUTSCENE("FBI_4_INT")
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF

			#IF IS_DEBUG_BUILD SK_PRINT("HAS_CUTSCENE_LOADED() FAILED") #ENDIF
		BREAK

		CASE RQ_MIKE
			IF eStartedPlayer = CHAR_MICHAEL
				RETURN TRUE
			ENDIF

			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[3])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[3], TRUE, TRUE)
				piPeds[MIKE] = g_sTriggerSceneAssets.ped[3]
				
				IF IS_PED_UNINJURED(piPeds[MIKE])
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_MIKE")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_MIKE FAILED")
				#ENDIF
			ELSE
				IF CREATE_PLAYER_PED_ON_FOOT(piPeds[MIKE], CHAR_MICHAEL, <<1982.1981, 3818.9526, 31.4232>>, 215.9222, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE RQ_FRANK
//			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[FRANK])
//			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[FRANK], TRUE, TRUE)
//				piPeds[FRANK] = g_sTriggerSceneAssets.ped[FRANK]
//				
//				IF IS_PED_UNINJURED(piPeds[FRANK])
//					#IF IS_DEBUG_BUILD
//						SK_PRINT("RQ_FRANK")
//					#ENDIF
					RETURN TRUE
//				ENDIF
//				
//				#IF IS_DEBUG_BUILD
//					SK_PRINT("RQ_FRANK FAILED")
//				#ENDIF
//			ENDIF
		BREAK
		
		CASE RQ_TREV
			IF eStartedPlayer = CHAR_MICHAEL
				RETURN TRUE
			ELIF eStartedPlayer = CHAR_TREVOR
				RETURN TRUE
			ENDIF

			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[4])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[4], TRUE, TRUE)
				piPeds[TREV] = g_sTriggerSceneAssets.ped[4]
				
				IF IS_PED_UNINJURED(piPeds[TREV])
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_TREV")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TREV FAILED")
				#ENDIF
			ELSE
				IF CREATE_PLAYER_PED_ON_FOOT(piPeds[TREV], CHAR_TREVOR, <<1982.1981, 3818.9526, 31.4232>>, 215.9222, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE RQ_STEVE
			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[2])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2], TRUE, TRUE)
				piPeds[STEVE] = g_sTriggerSceneAssets.ped[2]
				
				IF IS_PED_UNINJURED(piPeds[STEVE])
					SET_PED_PROP_INDEX(piPeds[STEVE], ANCHOR_EYES, 0)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_STEVE")
					#ENDIF
					RETURN TRUE
				ENDIF
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_STEVE FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_ANDREAS
			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[0])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
				piPeds[ANDREAS] = g_sTriggerSceneAssets.ped[0]
				
				IF IS_PED_UNINJURED(piPeds[ANDREAS])
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_ANDREAS")
					#ENDIF
					RETURN TRUE
				ENDIF
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_ANDREAS FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_DAVE
			IF IS_PED_UNINJURED(g_sTriggerSceneAssets.ped[1])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
				piPeds[DAVE] = g_sTriggerSceneAssets.ped[1]
				
				IF IS_PED_UNINJURED(piPeds[DAVE])
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_DAVE")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_DAVE FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_DAVES_PHONE
			IF eStartedPlayer != CHAR_MICHAEL
				RETURN TRUE
			ENDIF

			IF IS_ENTITY_ALIVE(g_sTriggerSceneAssets.object[0])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0], TRUE, TRUE)
				oiDavesPhone = g_sTriggerSceneAssets.object[0]
				
				IF IS_ENTITY_ALIVE(oiDavesPhone)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_DAVES_PHONE")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_DAVES_PHONE FAILED")
				#ENDIF
			ENDIF
		BREAK
		
		CASE RQ_MIKE_CAR
//			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
//			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
//				viCars[MIKE_CAR] = g_sTriggerSceneAssets.veh[0]
//				
//				IF IS_VEHICLE_OK(viCars[MIKE_CAR])
//					#IF IS_DEBUG_BUILD
//						SK_PRINT("RQ_MIKE_CAR")
//					#ENDIF
//					RETURN TRUE
//				ENDIF
//				
//				#IF IS_DEBUG_BUILD
//					SK_PRINT("RQ_MIKE_CAR FAILED")
//				#ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD
//					SK_PRINT("RQ_MIKE_CAR")
//				#ENDIF
				RETURN TRUE
//			ENDIF
		BREAK
		CASE RQ_FRANK_CAR
			IF CREATE_PLAYER_VEHICLE(viCars[FRANK_CAR], CHAR_FRANKLIN, <<1379.6998, -2056.0024, 50.9983>>, 231.3996)
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_FRANK_CAR")
				#ENDIF
				RETURN TRUE
			ENDIF
				
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_FRANK_CAR FAILED")
			#ENDIF
		BREAK
		CASE RQ_TREV_CAR
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[1])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
				viCars[TREV_CAR] = g_sTriggerSceneAssets.veh[1]
				
				IF IS_VEHICLE_OK(viCars[TREV_CAR])
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_TREV_CAR")
					#ENDIF
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TREV_CAR FAILED")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TREV_CAR")
				#ENDIF
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_SETUP_FOR_AGENTS()
	IF SETUP_STAGE_REQUIREMENTS(RQ_STEVE, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_ANDREAS, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_DAVE, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_DAVES_PHONE, vSafeVec)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_SETUP_FOR_PLAYER_MIKE()
	IF	CREATE_PLAYER_PED_ON_FOOT(piPeds[TREV], CHAR_TREVOR, <<1377.1952, -2103.5322, 53.4723>>, 215.9222, FALSE, FALSE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL DO_SETUP_FOR_PLAYER_TREV()
	IF SETUP_STAGE_REQUIREMENTS(RQ_MIKE, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_MIKE_CAR, vSafeVec)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL DO_SETUP_FOR_PLAYER_FRANK()
	IF SETUP_STAGE_REQUIREMENTS(RQ_MIKE, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_TREV, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_MIKE_CAR, vSafeVec)
	AND SETUP_STAGE_REQUIREMENTS(RQ_TREV_CAR, vSafeVec)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL PICK_AND_LOAD_FOR_PLAYER()
	SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
		CASE CHAR_MICHAEL
			IF DO_SETUP_FOR_PLAYER_MIKE()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			IF DO_SETUP_FOR_PLAYER_FRANK()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF DO_SETUP_FOR_PLAYER_TREV()
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC SWITCH_PLAYER_TO_TREV()
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		SELECTOR_PED_STRUCT sSelector

		sSelector.pedID[SELECTOR_PED_TREVOR] = piPeds[TREV]
		sSelector.pedID[SELECTOR_PED_MICHAEL] = piPeds[MIKE]
		sSelector.pedID[SELECTOR_PED_FRANKLIN] = piPeds[FRANK]


		IF eStartedPlayer != CHAR_TREVOR
			MAKE_SELECTOR_PED_SELECTION(sSelector, SELECTOR_PED_TREVOR)
			IF TAKE_CONTROL_OF_SELECTOR_PED(sSelector, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
				#IF IS_DEBUG_BUILD  SK_PRINT("TAKE_CONTROL_OF_SELECTOR_PED(sSelector)")  #ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_CUTSCENE, vSafeVec)
					IF NOT IS_REPLAY_IN_PROGRESS()
						IF DO_SETUP_FOR_AGENTS()
						AND PICK_AND_LOAD_FOR_PLAYER()
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))

							#IF IS_DEBUG_BUILD  
								SK_PRINT("MS_SET_UP") 
							#ENDIF
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4_INTRO)
							RETURN TRUE //all mission stage requirements set up return true and activate stage
						ENDIF
					ELSE
						RETURN TRUE //all mission stage requirements set up return true and activate stage				
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED") 
			#ENDIF
		BREAK

		CASE MS_INTRO
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_MISSION_STAGE(MS_SET_UP)
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				REQUEST_ADDITIONAL_TEXT("FBIINT", MISSION_TEXT_SLOT)
				IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
					IF eStartedPlayer != CHAR_TREVOR
						REQUEST_MODEL(BODHI2)
						REQUEST_MODEL(BAGGER)
						IF HAS_MODEL_LOADED(BODHI2)
						AND HAS_MODEL_LOADED(BAGGER)
							RETURN TRUE //all mission stage requirements set up return true and activate stage
						ENDIF
					ELSE
						REQUEST_MODEL(BAGGER)
						IF HAS_MODEL_LOADED(BAGGER)
							RETURN TRUE //all mission stage requirements set up return true and activate stage
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_INTRO, FAILED") 
			#ENDIF
		BREAK

		CASE MS_LEAVE_AREA
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					IF CREATE_PLAYER_PED_ON_FOOT(piPeds[TREV], CHAR_TREVOR, <<1377.1952, -2103.5322, 53.4723>>, 215.9222, FALSE, FALSE)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, <<1399.7205, -2057.4124, 50.9981>>, 324.4025 , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
							
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							piPeds[MIKE] = PLAYER_PED_ID()
							SWITCH_PLAYER_TO_TREV()
							SET_PED_POS(piPeds[MIKE],  <<1391.7767, -2071.4766, 50.9982>>, 35.6553)
						ELSE
							piPeds[FRANK] = PLAYER_PED_ID()
							SWITCH_PLAYER_TO_TREV()
							WHILE NOT CREATE_PLAYER_PED_ON_FOOT(piPeds[MIKE], CHAR_MICHAEL, <<1391.7767, -2071.4766, 50.9982>>, 35.6553, FALSE, FALSE)
								WAIT(0)
							ENDWHILE
						ENDIF
						
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4_INTRO)
					ENDIF
				ELSE
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(piPeds[MIKE], CHAR_MICHAEL, <<1391.7767, -2071.4766, 50.9982>>, 35.6553, FALSE, FALSE)
						WAIT(0)
					ENDWHILE
				ENDIF
				
				IF CREATE_PLAYER_VEHICLE(viCars[TREV_CAR], CHAR_TREVOR, <<1382.499878,-2068.988037,51.967495>>, 13.785377)
					REQUEST_ADDITIONAL_TEXT("FBIINT", MISSION_TEXT_SLOT)
					IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
						bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
						END_REPLAY_SETUP(viCars[TREV_CAR])
						RC_END_Z_SKIP()
					ENDIF
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_LEAVE_AREA, FAILED") 
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
	INT i 
	FOR i = ENUM_TO_INT(ANDREAS) TO ENUM_TO_INT(FRANK)
		IF piPeds[i] != PLAYER_PED_ID()
			#IF IS_DEBUG_BUILD SK_PRINT_INT("DELETED === ", i) #ENDIF
			SAFE_DELETE_PED(piPeds[i])
		ENDIF
	ENDFOR

	FOR i = ENUM_TO_INT(MIKE_CAR) TO (ENUM_TO_INT(NUM_CARS)-1)
		#IF IS_DEBUG_BUILD SK_PRINT_INT("DELETED VEHICLE === ", i) #ENDIF
		IF ENUM_TO_INT(TREV_CAR) <> i
			SAFE_DELETE_VEHICLE(viCars[i])
		ELSE
			SAFE_RELEASE_VEHICLE(viCars[i])
		ENDIF
	ENDFOR
ENDPROC

PROC RELEASE_ALL()
	INT i 
	FOR i = ENUM_TO_INT(ANDREAS) TO ENUM_TO_INT(FRANK)
		IF piPeds[i] != PLAYER_PED_ID()
			#IF IS_DEBUG_BUILD SK_PRINT_INT("RELEASED === ", i) #ENDIF
			SAFE_RELEASE_PED(piPeds[i])
		ENDIF
	ENDFOR

	FOR i = ENUM_TO_INT(MIKE_CAR) TO (ENUM_TO_INT(NUM_CARS)-1)
		#IF IS_DEBUG_BUILD SK_PRINT_INT("RELEASED VEHICLE === ", i) #ENDIF
		SAFE_RELEASE_VEHICLE(viCars[i])
	ENDFOR
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF

	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()
	WAIT_FOR_CUTSCENE_TO_STOP()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	IF bDelAll
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF

	IF DOES_CAM_EXIST(camMain)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(camMain)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()
	SERVICES_TOGGLE(TRUE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	eState = SS_INIT	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed(BOOL paramSeamlessMissionPass = FALSE, BOOL paramHidePassScreen = FALSE)
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	Mission_Flow_Mission_Passed(paramSeamlessMissionPass, paramHidePassScreen)
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			CLEAR_HELP()
			#IF IS_DEBUG_BUILD SK_PRINT("Init fail") #ENDIF

			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)  
			ELSE
				MISSION_FLOW_MISSION_FAILED()
			ENDIF

			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//
				
				#IF IS_DEBUG_BUILD SK_PRINT("Failed") #ENDIF
				DELETE_ALL()
				Script_Cleanup()
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("waiting fail screen") #ENDIF
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
//	bLoadingFinCutscene = FALSE
	eState = SS_INIT
	CLEANUP() //delete everything
ENDPROC


///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			//DO_SCREEN_FADE_OUT(0)
//			#IF IS_DEBUG_BUILD
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
//			#ENDIF
			
			
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF
			
			IF SETUP_MISSION_STAGE(eMissionState) 			
				
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "MS_INTRO"
				#ENDIF

				IF IS_VEHICLE_OK(viCars[TREV_CAR])
					SET_VEHICLE_DOORS_LOCKED(viCars[TREV_CAR], VEHICLELOCK_UNLOCKED)
				ENDIF

				
				SERVICES_TOGGLE(FALSE)

				
				IF IS_REPLAY_IN_PROGRESS()
					JUMP_TO_STAGE(MS_LEAVE_AREA)
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
					ENDIF

					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC


PROC REG_PEDS_FOR_SCENE()
	IF IS_PED_UNINJURED(piPeds[ANDREAS])
	AND IS_PED_UNINJURED(piPeds[DAVE])
	AND IS_PED_UNINJURED(piPeds[STEVE])
		REGISTER_ENTITY_FOR_CUTSCENE(piPeds[ANDREAS], "Andreas_Sanchez", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		REGISTER_ENTITY_FOR_CUTSCENE(piPeds[DAVE], "Dave_FBI", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		REGISTER_ENTITY_FOR_CUTSCENE(piPeds[STEVE], "steve_FBI", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		IF IS_ENTITY_ALIVE(oiDavesPhone)
			REGISTER_ENTITY_FOR_CUTSCENE(oiDavesPhone, "DaveNortons_Phone", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		ENDIF
	ENDIF
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM() 

		CASE CHAR_MICHAEL
			piPeds[MIKE] = PLAYER_PED_ID()
			REGISTER_ENTITY_FOR_CUTSCENE(piPeds[FRANK], "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			IF IS_PED_UNINJURED(piPeds[TREV])
				REGISTER_ENTITY_FOR_CUTSCENE(piPeds[TREV], "trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
			ENDIF
			REGISTER_ENTITY_FOR_CUTSCENE(viCars[TREV_CAR], "trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BODHI2, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
		BREAK

		CASE CHAR_FRANKLIN
			IF IS_PED_UNINJURED(piPeds[MIKE])
			AND IS_PED_UNINJURED(piPeds[TREV])
			AND IS_VEHICLE_OK(viCars[TREV_CAR])
				REGISTER_ENTITY_FOR_CUTSCENE(piPeds[MIKE], "michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(piPeds[TREV], "trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				piPeds[FRANK] = PLAYER_PED_ID()
//				IF NOT IS_ENTITY_A_MISSION_ENTITY(piPeds[FRANK])
//					SET_ENTITY_AS_MISSION_ENTITY(piPeds[FRANK], TRUE, TRUE)
//				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(viCars[TREV_CAR], "trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, BODHI2, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
			ENDIF
		BREAK 

		CASE CHAR_TREVOR
			IF IS_PED_UNINJURED(piPeds[MIKE])
		     	REGISTER_ENTITY_FOR_CUTSCENE(piPeds[MIKE], "michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				REGISTER_ENTITY_FOR_CUTSCENE(piPeds[FRANK], "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				
				IF IS_VEHICLE_OK(GET_PLAYERS_LAST_VEHICLE())
					enumCharacterList playercar 
					playercar = GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(GET_PLAYERS_LAST_VEHICLE())
					IF playercar = CHAR_TREVOR
						viCars[TREV_CAR]= GET_PLAYERS_LAST_VEHICLE()
						#IF IS_DEBUG_BUILD SK_PRINT("Vehicle is owned by TREV") #ENDIF
						SET_ENTITY_AS_MISSION_ENTITY(viCars[TREV_CAR], TRUE, TRUE)
						REGISTER_ENTITY_FOR_CUTSCENE(viCars[TREV_CAR], "trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, BODHI2, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(viCars[TREV_CAR], "trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BODHI2, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
					ENDIF
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(viCars[TREV_CAR], "trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BODHI2, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
				ENDIF
			ENDIF
		BREAK 

	ENDSWITCH 
ENDPROC


PROC MARK_START_CAR_AS_VEH_GEN(VECTOR pos, FLOAT dir)
	VEHICLE_INDEX veh = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(veh)
		IF IS_VEHICLE_DRIVEABLE(veh)
			CPRINTLN(DEBUG_MISSION, "Setting snapshop vehicle as vehicle get for prep  ", GET_THIS_SCRIPT_NAME())
			SET_MISSION_VEHICLE_GEN_VEHICLE(veh, pos, dir)
		ELSE
			CPRINTLN(DEBUG_MISSION, " vehicle not drivable  ", GET_THIS_SCRIPT_NAME())
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "vehicle doesnt exist  ", GET_THIS_SCRIPT_NAME())
	ENDIF
ENDPROC


/// PURPOSE:
///    State for the mocap at the safe
PROC MOCAP_INTRO()
	INT iCutTime = GET_CUTSCENE_TIME()
	IF eState = SS_ACTIVE
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() 
		AND bLoadingFinCutscene
		AND NOT bJumpSkip
		AND iCutTime < 157323
			#IF IS_DEBUG_BUILD  SK_PRINT_INT("Player skiped cutscene at time = ", iCutTime)  #ENDIF
//			eState = SS_SKIPPED
		ELSE
			IF GET_CUTSCENE_TIME() > 157323
				SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
				#IF IS_DEBUG_BUILD  SK_PRINT_INT("cutscene at time = ", iCutTime)  #ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Load everything for the next stage while the cutscene is playing 
	IF NOT bLoadingFinCutscene
		IF SETUP_MISSION_STAGE(MS_INTRO, bJumpSkip)
			bLoadingFinCutscene = TRUE //loading done
		ENDIF
	ENDIF

	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			IF CAN_PLAYER_START_CUTSCENE(TRUE)
				#IF IS_DEBUG_BUILD  SK_PRINT("CAN_PLAYER_START_CUTSCENE(TRUE)")  #ENDIF
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					IF IS_PED_UNINJURED(piPeds[STEVE])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("steve_FBI", piPeds[STEVE])
						SET_CUTSCENE_PED_PROP_VARIATION("steve_FBI", ANCHOR_EYES, 0)
					ENDIF
					IF IS_PED_UNINJURED(piPeds[FRANK])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", piPeds[FRANK])
					ENDIF
					IF IS_PED_UNINJURED(piPeds[TREV])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("trevor", piPeds[TREV])
					ENDIF
					IF IS_PED_UNINJURED(piPeds[MIKE])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("michael", piPeds[MIKE])
					ENDIF
				ENDIF

				IF RC_IS_CUTSCENE_OK_TO_START()
					#IF IS_DEBUG_BUILD  SK_PRINT("RC_IS_CUTSCENE_OK_TO_START()")  #ENDIF
					
					REG_PEDS_FOR_SCENE()
					
					START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_TREVOR)

					SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(GET_PLAYER_VEH_MODEL(CHAR_TREVOR))

					IF IS_VEHICLE_OK(viCars[TREV_CAR])
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(viCars[TREV_CAR])
					ENDIF
					WAIT(0)
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
					
					VECTOR vRespotVeh, vLargeRespotVeh
					FLOAT fRespotVeh, fLargeRespotVeh
					
					vLargeRespotVeh = <<1368.7412, -2068.1438, 51.0552>>
					fLargeRespotVeh = 163.6238
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL

							IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
								vRespotVeh = <<1388.0695, -2057.9080, 50.9983>>
								fRespotVeh = 263.5418
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, vRespotVeh, fRespotVeh , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
								MARK_START_CAR_AS_VEH_GEN(vRespotVeh, fRespotVeh)
							ELSE
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, vLargeRespotVeh, fLargeRespotVeh)
								MARK_START_CAR_AS_VEH_GEN(vLargeRespotVeh, fLargeRespotVeh)
							ENDIF
							viCars[MIKE_CAR] = GET_PLAYERS_LAST_VEHICLE()
							IF IS_VEHICLE_OK(viCars[MIKE_CAR])
								SET_ENTITY_AS_MISSION_ENTITY(viCars[MIKE_CAR])
							ENDIF
						BREAK

						CASE CHAR_FRANKLIN
							IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
								vRespotVeh = <<1379.6998, -2056.0024, 50.9983>>
								fRespotVeh = 231.3996
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, vRespotVeh, fRespotVeh , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
//								MARK_START_CAR_AS_VEH_GEN(vRespotVeh, fRespotVeh)
							ELSE
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, vLargeRespotVeh, fLargeRespotVeh)
//								MARK_START_CAR_AS_VEH_GEN(vLargeRespotVeh, fLargeRespotVeh)
							ENDIF
							viCars[FRANK_CAR] = GET_PLAYERS_LAST_VEHICLE()
							IF IS_VEHICLE_OK(viCars[FRANK_CAR])
								SET_ENTITY_AS_MISSION_ENTITY(viCars[FRANK_CAR])
							ENDIF
						BREAK

						CASE CHAR_TREVOR
							IF NOT IS_VEHICLE_OK(viCars[TREV_CAR])
								IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
									vRespotVeh = <<1382.499878,-2068.988037,51.967495>>
									fRespotVeh = 13.785377
									RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, vLargeRespotVeh, fLargeRespotVeh , GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
	//								MARK_START_CAR_AS_VEH_GEN(vRespotVeh, fRespotVeh)
								ELSE
									RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1423.14331, -2040.78406, 45.99826>>, <<1345.54712, -2093.60181, 120.02565>>, 79.30, vLargeRespotVeh, fLargeRespotVeh)
	//								MARK_START_CAR_AS_VEH_GEN(vLargeRespotVeh, fLargeRespotVeh)
								ENDIF
	//							viCars[TREV_CAR] = GET_PLAYERS_LAST_VEHICLE()
	//							IF IS_VEHICLE_OK(viCars[TREV_CAR])
	//								SET_ENTITY_AS_MISSION_ENTITY(viCars[TREV_CAR])
	//							ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					RC_START_CUTSCENE_MODE(<<-1046.6174, -516.3990, 35.0386>>)
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)
					
					
					#IF IS_DEBUG_BUILD  SK_PRINT("INIT  MOCAP_INTRO")  #ENDIF
					eState = SS_ACTIVE	
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.1)

			IF eStartedPlayer != CHAR_FRANKLIN
				IF NOT bRegisteredFrank
					IF NOT IS_PED_UNINJURED(piPeds[FRANK])
						IF DOES_CUTSCENE_ENTITY_EXIST("Franklin")
							IF  DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
								#IF IS_DEBUG_BUILD SK_PRINT("GET_CUTSCENE_SECTION_PLAYING REGISTERED FRANK") #ENDIF
								piPeds[FRANK] = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
							ENDIF
						ENDIF
					ENDIF

					IF GET_CUTSCENE_SECTION_PLAYING() = 4
						bRegisteredFrank = SETUP_STAGE_REQUIREMENTS(RQ_FRANK_CAR, vSafeVec)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_UNINJURED(piPeds[TREV])
				SWITCH_PLAYER_TO_TREV()
			ENDIF
			
			IF NOT IS_VEHICLE_OK(viCars[TREV_CAR])
				IF NOT bRegisteredTrev
					IF DOES_CUTSCENE_ENTITY_EXIST("trevors_car")
						IF  DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car"))
							viCars[TREV_CAR] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car"))
							SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(viCars[TREV_CAR])
							#IF	IS_DEBUG_BUILD SK_PRINT("GET_CUTSCENE_SECTION_PLAYING REGISTERED TREV") #ENDIF
							bRegisteredTrev = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT bRegisteredTrev
					IF IS_VEHICLE_OK(viCars[TREV_CAR])
						bRegisteredTrev = TRUE
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(viCars[TREV_CAR])
					ENDIF
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Andreas_Sanchez")
				#IF IS_DEBUG_BUILD  SK_PRINT("ANDREAS' EXIT STATE")  #ENDIF
				SAFE_DELETE_PED(piPeds[ANDREAS])
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dave_FBI")
				#IF IS_DEBUG_BUILD  SK_PRINT("DAVEs EXIT STATE")  #ENDIF
				SAFE_DELETE_PED(piPeds[DAVE])
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("steve_FBI")
				#IF IS_DEBUG_BUILD  SK_PRINT("STEVEs EXIT STATE")  #ENDIF
				SAFE_DELETE_PED(piPeds[STEVE])
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("DaveNortons_Phone")
				#IF IS_DEBUG_BUILD  SK_PRINT("DaveNortons_Phone EXIT STATE")  #ENDIF
				SAFE_DELETE_OBJECT(oiDavesPhone)
			ENDIF
			

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				#IF IS_DEBUG_BUILD  SK_PRINT("Michael's EXIT STATE")  #ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				#IF IS_DEBUG_BUILD  SK_PRINT("Trevor's EXIT STATE")  #ENDIF
				IF IS_VEHICLE_OK(viCars[TREV_CAR])
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viCars[TREV_CAR])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					SET_VEHICLE_DOOR_SHUT(viCars[TREV_CAR], SC_DOOR_FRONT_LEFT)
				ENDIF
//					SAFE_DELETE_VEHICLE(viCars[TREV_CAR])
//					DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_TREVOR)
//					SAFE_DELETE_PED(piPeds[TREV])
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				#IF IS_DEBUG_BUILD SK_PRINT("Franklin's EXIT STATE") #ENDIF
				IF NOT bRegisteredFrank
					IF DOES_CUTSCENE_ENTITY_EXIST("Franklin")
						IF  DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
							#IF IS_DEBUG_BUILD SK_PRINT("GET_CUTSCENE_SECTION_PLAYING REGISTERED FRANK") #ENDIF
							piPeds[FRANK] = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
						ENDIF
					ENDIF
				ENDIF
				SAFE_DELETE_PED(piPeds[FRANK])
			ENDIF

			IF iCutTime >= 166500
				SAFE_DELETE_VEHICLE(viCars[FRANK_CAR])
				DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_FRANKLIN)
			ENDIF


			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				#IF IS_DEBUG_BUILD  SK_PRINT("CAMERA's EXIT STATE")  #ENDIF
				IF eStartedPlayer != CHAR_TREVOR
					PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS") 
					ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE) 
				ENDIF
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-40.8422)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-2.8838)	
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
			AND bLoadingFinCutscene		
				SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			STOP_GAMEPLAY_HINT()
			IF iMissionTimer = -1
				iMissionTimer = GET_GAME_TIMER()
			ELIF ( GET_GAME_TIMER() - iMissionTimer) > 1000
				bLoadingFinCutscene = FALSE
				RC_END_CUTSCENE_MODE()
				#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP  MOCAP_INTRO")  #ENDIF
				NEXT_STAGE()
			ENDIF
		BREAK

		CASE SS_SKIPPED
			#IF IS_DEBUG_BUILD  SK_PRINT("SKIPPED")  #ENDIF
			IF IS_CUTSCENE_PLAYING()
				IF NOT bRegisteredTrev
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevors_car")
						IF DOES_CUTSCENE_ENTITY_EXIST("trevors_car")
							IF  DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car"))
								#IF IS_DEBUG_BUILD SK_PRINT("GET_CUTSCENE_SECTION_PLAYING REGISTERED TREV") #ENDIF
								viCars[TREV_CAR] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car"))
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF NOT bRegisteredFrank
					IF DOES_CUTSCENE_ENTITY_EXIST("Franklin")
						IF  DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
							#IF IS_DEBUG_BUILD SK_PRINT("GET_CUTSCENE_SECTION_PLAYING REGISTERED FRANK") #ENDIF
							piPeds[FRANK] = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
							bRegisteredFrank = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE

				SAFE_DELETE_VEHICLE(viCars[FRANK_CAR])
				DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_FRANKLIN)

				IF bRegisteredFrank
					SAFE_DELETE_PED(piPeds[FRANK])
				ENDIF

				SET_GAMEPLAY_CAM_RELATIVE_HEADING(-27.6467)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-5.1166)	
				eState = SS_CLEANUP
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_PLAYER_BUMPED_MIKE()
	IF IS_ENTITY_TOUCHING_ENTITY(piPeds[MIKE], PLAYER_PED_ID())
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


PROC START_NEW_SCENARIO(STRING rio, MIKE_STATES state)
	CLEAR_PED_TASKS(piPeds[MIKE])
	IF NOT IS_STRING_NULL_OR_EMPTY(rio)
		TASK_START_SCENARIO_IN_PLACE(piPeds[MIKE], rio, 1000, TRUE)
	ENDIF
	eMikeState = state
	iMikeTimer = -1
ENDPROC


PROC WAIT_TO_UPDATE(INT iStateTime, STRING rio, MIKE_STATES state)
	IF iMikeTimer != -1
		IF (GET_GAME_TIMER() - iMikeTimer) > iStateTime
			START_NEW_SCENARIO(rio, state)
		ENDIF
	ELSE
		iMikeTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

PROC MIKE_STATE()
				
	IF iDialogueTimer = -1
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDialogueTimer = GET_GAME_TIMER()
		ENDIF
	ELSE
		IF (GET_GAME_TIMER() - iDialogueTimer) > 12000
			IF CREATE_CONVERSATION(s_conversation_peds, "FBIPRAU", "FBI4_INT_M", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				iDialogueTimer = -1
			ENDIF
		ENDIF
	ENDIF

	SWITCH eMikeState
		CASE MSS_WAIT_SMOKE
			WAIT_TO_UPDATE(2000, "WORLD_HUMAN_SMOKING", MSS_SMOKE)
		BREAK

		CASE MSS_SMOKE
			WAIT_TO_UPDATE(40000, "", MSS_WAIT_PHONE)
		BREAK
		
		CASE MSS_WAIT_PHONE
			WAIT_TO_UPDATE(6000, "WORLD_HUMAN_STAND_MOBILE", MSS_PHONE)
		BREAK
		
		CASE MSS_PHONE
			WAIT_TO_UPDATE(40000, "", MSS_IDLE)
		BREAK
	
		CASE MSS_IDLE
			WAIT_TO_UPDATE(6000, "WORLD_HUMAN_STAND_IMPATIENT", MSS_WAIT_SMOKE)
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_AREA()
	
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_AREA")  #ENDIF
				iMissionTimer = GET_GAME_TIMER()
				IF IS_PED_UNINJURED(piPeds[MIKE])
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 0, piPeds[MIKE], "MICHAEL")
					START_NEW_SCENARIO("WORLD_HUMAN_SMOKING", MSS_SMOKE)
					SET_PED_PRIMARY_LOOKAT(piPeds[MIKE], PLAYER_PED_ID())
				ENDIF
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF IS_PED_UNINJURED(piPeds[MIKE])
				MIKE_STATE()
				IF HAS_PLAYER_BUMPED_MIKE()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piPeds[MIKE])
						TASK_SMART_FLEE_PED(piPeds[MIKE], PLAYER_PED_ID(), 500, -1)
						SET_PED_KEEP_TASK(piPeds[MIKE], TRUE)
						MISSION_FAILED(FR_MIKE_THREATENED)
						EXIT
					ENDIF
				ENDIF
				IF IS_BULLET_IN_AREA(<<1393.96460, -2070.81079, 50.99826>>, 18.48)
				OR IS_PROJECTILE_IN_AREA(<<1402.48901, -2056.89648, 50.0>>, <<1389.38367, -2082.86694, 61>>)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piPeds[MIKE], PLAYER_PED_ID())
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(piPeds[MIKE])
					TASK_SMART_FLEE_PED(piPeds[MIKE], PLAYER_PED_ID(), 500, -1)
					SET_PED_KEEP_TASK(piPeds[MIKE], TRUE)
					MISSION_FAILED(FR_MIKE_THREATENED)
					EXIT
				ENDIF
			ENDIF

			IF NOT IS_THIS_PRINT_BEING_DISPLAYED("INT_LEAVE")
				PRINT_NOW("INT_LEAVE", 60000, 0)
			ENDIF

			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<1392.9344, -2072.4121, 50.9982>>, 180)
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP LEAVE_AREA")  #ENDIF
			Script_Passed()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC



///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED(FR_NONE)
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
//					IF i_debug_jump_stage >= 3
//						SWITCH i_debug_jump_stage
//							CASE 3 //MS_RETURN_TO_STUDIO
//								i_debug_jump_stage = 4
//							BREAK
//							
//							CASE 4 //MS_OUTRO
//								i_debug_jump_stage = 5
//							BREAK
//
//						ENDSWITCH
//					ELSE
						i_debug_jump_stage++
//					ENDIF
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF


PROC CHECK_MIKE_ALIVE()
	IF NOT IS_PED_UNINJURED(piPeds[MIKE])
		MISSION_FAILED(FR_MIKE_KILLED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Holds functions that moniter for the player failing
PROC CHECK_FOR_FAIL()
	IF NOT bJumpSkip
	AND eMissionState <> MS_FAILED
		IF eMissionState > MS_INTRO
			CHECK_MIKE_ALIVE()
		ENDIF
	ENDIF
ENDPROC

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() !=  FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED
			PRINTSTRING("FORCE CLEAN UP") PRINTNL()
			sFailReason = NULL
			Mission_Flow_Mission_Force_Cleanup()
	        Script_Cleanup()
		ELSE
			IF eMissionState != MS_FAILED
				PRINTSTRING("Silent pass mission player was killed or was arrested") PRINTNL()
				Script_Passed(TRUE, TRUE)
			ELSE
				PRINTSTRING("FORCE CLEAN UP") PRINTNL()
				sFailReason = NULL
				Mission_Flow_Mission_Force_Cleanup()
		        Script_Cleanup()
			ENDIF
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF

	eStartedPlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF IS_REPLAY_IN_PROGRESS()
		START_REPLAY_SETUP(<<1377.1952, -2103.5322, 53.4723>>, 215.9222)
	ENDIF
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BPINT")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			CHECK_FOR_FAIL()
			SWITCH eMissionState
			
				CASE MS_SET_UP
					INITMISSION()
				BREAK
				
				CASE MS_INTRO
					MOCAP_INTRO()
				BREAK
				
				CASE MS_LEAVE_AREA
					LEAVE_AREA()
				BREAK

				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK				
			ENDSWITCH

			IF eMissionState <> MS_FAILED
				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
