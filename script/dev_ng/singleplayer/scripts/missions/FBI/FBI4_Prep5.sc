
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"

USING "CompletionPercentage_public.sch"

using "dialogue_public.sch"

USING "flow_public_core_override.sch"
USING "flow_public_GAME.sch"

using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "replay_public.sch"

USING "script_blips.sch"
USING "script_heist.sch"
USING "script_ped.sch"

USING "selector_public.sch"

USING "flow_special_event_checks.sch"

BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state
USING "prep_mission_common.sch"

USING "shop_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 1
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Template.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_AFTER_MOCAP                0 //following beverly down the road

//-----------------------
//		CONSTANTS
//-----------------------

//POLY TEST CONST
CONST_INT MAX_POLY_TEST_VERTS		8		//poly test
//Ambient encounter speech
CONST_INT MAX_AMBIENT_CONV_LINES	8

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
	MS_BUY,						//1
	MS_LEAVE_SHOP,				//2
	MS_FAILED					//3
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

/// PURPOSE: Mission requirements used for loading 
///    and creating mission assets
ENUM MISSION_REQ
	RQ_NONE,
	RQ_TEXT
ENDENUM

/// PURPOSE: Fail reason enums for picking correct fail reason
ENUM FAILED_REASONS
	FR_NONE,
	FR_KICKOFFSHOP,
	FR_LEFT,
	FR_NO_CASH,
	FR_COPS
ENDENUM


ENUM ABM_CONV_STATES
	ACS_START_LINE,
	ACS_WAIT_LINE_FIN,
	ACS_PICK_NEXT_LINE,
	ACS_FIN
ENDENUM



STRING mObj[3]

///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages
DYNAMIC_PHONE_STATE ePhoneCallState = DPS_CHOOSE_PHONE_CALL

//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************

TEST_POLY mAreaCheck1		//Poly area 1
TEST_POLY mAreaCheck2		//Poly area 2

//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints

//Fail vars
STRING sFailReason = NULL				//String to display when mission is failed

//GOD TEXT
STRING sGodText = "FBIPRE"
BOOL bObjectiveShown = FALSE			//Has an objective been shown
INT iNumberBought = 0

BOOL bBoilersBought = FALSE
BOOL bStatOn = FALSE
BOOL bDeletedBlip = FALSE
BOOL bStopPlayerWantedLevel = FALSE
//BLIPs
BLIP_INDEX biBlip						//Mission blip - mainly used for go to objectives


//ScriptCamera
CAMERA_INDEX camMain					//Script camera used in place holder cutscenes

VECTOR vSafeVec = <<0,0,0>>				//safe vector used when a proper position isnt needed 

VECTOR vBoilers = <<20.8336, -1113.7928, 28.7970>>

//BOOL bArrivedShopLoc = FALSE

///==============| GOD TEXT BOOLS |============
BOOL bWarnLeft = FALSE
///===============| models |===================


/// ===============| START VECTORS |==============

/// ==============| START HEADINGS |===============

/// ==============| PED INDICES |================


/// ==============| VEHICLE INDICES |===========


/// ===============| GROUPS |========================


/// ===============| DIALOGUE |======================
STRING sTextBlock = "FBIPRAU"			//The Dialogue block for the mission
structPedsForConversation s_conversation_peds		//conversation struct
TEXT_LABEL_15 sPhoneUpdateString
INT iMissionPassDelayTimer
INT iWantedFailTimer = -1
//Used for tracking an interupted conversation
//TEXT_LABEL_23 sResumeRoot = "NONE"
//TEXT_LABEL_23 sResumeLine = "NONE"

/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()
	bWarnLeft = FALSE
	iWantedFailTimer = -1
	bBoilersBought = FALSE
	bStatOn = FALSE
	bDeletedBlip = FALSE
ENDPROC


///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD

	BOOL bShowDebugText = TRUE
	WIDGET_GROUP_ID widgetGroup
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	
	ENDPROC
	
	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
//	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)
//
//		VECTOR vBottom[2]
//		VECTOR vTop[2]
//		
//		vBottom[0] = vec1
//		vBottom[1] = vec2
//		
//		vTop[0] = vec1
//		vTop[1] = vec2
//		
//		IF vec1.z > vec2.z
//			vBottom[0].z = vec2.z
//			vBottom[1].z = vec2.z
//			vTop[0].z = vec1.z
//			vTop[1].z = vec1.z
//		ELSE
//			vBottom[0].z = vec1.z
//			vBottom[1].z = vec1.z
//			vTop[0].z = vec2.z
//			vTop[1].z = vec2.z
//		ENDIF
//
//		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
//		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
//		VECTOR w = side * (width / 2.0)
//
//		// Bottom points
//		VECTOR c1 = vBottom[0] - w  // base left
//		VECTOR c2 = vBottom[0] + w  // base right              
//		VECTOR c3 = vBottom[1] + w  // top rt
//		VECTOR c4 = vBottom[1] - w  // top lt
//
//		// Top points
//		VECTOR d1 = vTop[0] - w  // base left
//		VECTOR d2 = vTop[0] + w  // base right              
//		VECTOR d3 = vTop[1] + w  // top rt
//		VECTOR d4 = vTop[1] - w  // top lt
//
//		// Draw bottom lines
//		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
//		// Draw top lines
//		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
//		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
//		// Draw uprights
//		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
//		
//	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Set a peds position and heading safely ie check its ok then move it and set its heading
/// PARAMS:
///    index - The ped to move
///    pos - The position to move it to
///    dir - The Heading to set
PROC SET_PED_POS(PED_INDEX index, VECTOR pos, FLOAT dir)
	IF IS_PED_UNINJURED(index)
		CLEAR_PED_TASKS(index)
	ENDIF

	SAFE_TELEPORT_ENTITY(index, pos, dir)
ENDPROC

/// PURPOSE:
///    Adds a blip to a location safly checking to see that its not in existance before creatinging it
/// PARAMS:
///    blipIn - The blip index to write the newly created blip to
///    pos - the position to set the blip at
///    route - should the blip have a GPS route
PROC ADD_BLIP_LOCATION_WITH_NAME(BLIP_INDEX &blipIn, VECTOR pos, BOOL route = TRUE, BOOL bAddNameToBlip = FALSE, STRING sBlipName = NULL)
	IF NOT DOES_BLIP_EXIST(blipIn)
		blipIn = CREATE_COORD_BLIP(pos, BLIPPRIORITY_MED, route)
		IF bAddNameToBlip
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIn, sBlipName)  //Tooth
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Print a warning God text string and expires a bool when the warning has been shown
/// PARAMS:
///    WarnObj - the text key of the wanrning
///    expire - the bool to change
PROC PRINT_WARNING_OBJ(STRING WarnObj, BOOL &expire)
	IF NOT expire
		PRINT_NOW(WarnObj, DEFAULT_GOD_TEXT_TIME,0)
		expire = TRUE
	ENDIF
ENDPROC

///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

// ------------BOILER FUNCTIONS -------------------------------------------

PROC GET_ENUM_RANGE(enumCharacterList ePlayer, PED_COMP_NAME_ENUM &eStart, PED_COMP_NAME_ENUM &eEnd)
	SWITCH ePlayer
		CASE CHAR_MICHAEL
			eStart = OUTFIT_P0_PREP_BOILER_SUIT_1
			eEnd = OUTFIT_P0_PREP_BOILER_SUIT_3
		BREAK

		CASE CHAR_FRANKLIN
			eStart = OUTFIT_P1_PREP_BOILER_SUIT_1
			eEnd = OUTFIT_P1_PREP_BOILER_SUIT_3
		BREAK

		CASE CHAR_TREVOR
			eStart = OUTFIT_P2_PREP_BOILER_SUIT_1
			eEnd = OUTFIT_P2_PREP_BOILER_SUIT_3
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Returns the number of masks that the player has bought
/// RETURNS:
///    
FUNC INT GET_NUM_OF_BOILERS_BOUGHT()
	INT iBoilers = 0
	INT iBoilersToCheck
	FOR iBoilersToCheck = ENUM_TO_INT(CHAR_MICHAEL) TO (NUM_OF_PLAYABLE_PEDS-1)
		IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[INT_TO_ENUM( enumCharacterList, iBoilersToCheck)] != DUMMY_PED_COMP
			iBoilers++
		ENDIF
	ENDFOR
	CPRINTLN(DEBUG_MISSION, "The player has bought ",iBoilers, " Boilers")
	RETURN iBoilers

ENDFUNC

PROC PRINT_BOILER_OBJ(STRING Toprint, STRING ToClear1, STRING ToClear2)
	IF IS_THIS_PRINT_BEING_DISPLAYED(ToClear1)
	OR IS_THIS_PRINT_BEING_DISPLAYED(ToClear2)
		CLEAR_PRINTS()
	ENDIF
	
	IF NOT IS_THIS_PRINT_BEING_DISPLAYED(ToPrint)
		PRINT_NOW(Toprint, 60000, 0)
	ENDIF
ENDPROC

PROC PRINT_CURRENT_BOILER_OBJ() //enumCharacterList ePlayerChar) // , INT i)
//	INT p
	IF IS_CUSTOM_MENU_ON_SCREEN()
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_MICHAEL], mObj[CHAR_FRANKLIN], mObj[CHAR_TREVOR])
				ELIF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_FRANKLIN], mObj[CHAR_MICHAEL], mObj[CHAR_TREVOR])
				ELIF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_TREVOR], mObj[CHAR_FRANKLIN], mObj[CHAR_MICHAEL])
				ENDIF
			BREAK
			
			CASE CHAR_FRANKLIN
				IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_FRANKLIN], mObj[CHAR_MICHAEL], mObj[CHAR_TREVOR])
				ELIF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_MICHAEL], mObj[CHAR_FRANKLIN], mObj[CHAR_TREVOR])
				ELIF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_TREVOR], mObj[CHAR_FRANKLIN], mObj[CHAR_MICHAEL])
				ENDIF
			BREAK
			
			CASE CHAR_TREVOR
				IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_TREVOR], mObj[CHAR_FRANKLIN], mObj[CHAR_MICHAEL])
				ELIF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_MICHAEL], mObj[CHAR_FRANKLIN], mObj[CHAR_TREVOR])
				ELIF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = DUMMY_PED_COMP
					PRINT_BOILER_OBJ(mObj[CHAR_FRANKLIN], mObj[CHAR_MICHAEL], mObj[CHAR_TREVOR])
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_AQUIRED_OUTFITS()
		PRINT_CURRENT_BOILER_OBJ()//ePlayerChar) //, iNumBoilers)
		IF iNumberBought >= 3
			IF IS_THIS_PRINT_BEING_DISPLAYED("PRE_BUYM")
			OR IS_THIS_PRINT_BEING_DISPLAYED("PRE_BUYT")
			OR IS_THIS_PRINT_BEING_DISPLAYED("PRE_BUYF")
				CLEAR_PRINTS()
			ENDIF
			RETURN TRUE
		ENDIF
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail = FR_NONE)
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	SWITCH fail		
		CASE FR_NONE
			
		BREAK

		CASE FR_KICKOFFSHOP
			sFailReason = "PRE_FKICKOFF"
		BREAK

		CASE FR_LEFT
			sFailReason = "PRE_FLEFT"
		BREAK
		
		CASE FR_NO_CASH
			sFailReason = "PRE_CASH"
		BREAK
		
		CASE FR_COPS
			sFailReason = "PRE_COPS"
		BREAK
	ENDSWITCH
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the mission check point 
PROC SET_CHECKPOINT()

ENDPROC

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads a scene around a point used to the player 
///    seeing the map stream in when starting a replay or debug skipping
/// PARAMS:
///    pos - the position to stream the scene around at
///    rad - the radius of the scene to load
PROC LOAD_SCENE_SKIP(VECTOR pos, FLOAT rad)
	NEW_LOAD_SCENE_START_SPHERE(pos, rad)
	INT i_load_scene_timer = GET_GAME_TIMER()
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED()) AND (GET_GAME_TIMER() - i_load_scene_timer < 12000)
		WAIT(0)
	ENDWHILE
	NEW_LOAD_SCENE_STOP()
ENDPROC

/// PURPOSE:
///    Adds peds for dialogue based off the Mission state
PROC ADD_PEDS_FOR_DIALOGUE()
	ADD_CURRENT_PLAYER_FOR_DIALOGUE(s_conversation_peds)
ENDPROC

/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	SWITCH missionReq
		CASE RQ_NONE
			IF ARE_VECTORS_ALMOST_EQUAL(pos, vSafeVec)
			AND dir = 0.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_TEXT

			REQUEST_ADDITIONAL_TEXT(sGodText, MISSION_TEXT_SLOT)
			REQUEST_ADDITIONAL_TEXT(sTextBlock, MISSION_DIALOGUE_TEXT_SLOT)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TEXT")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("TEXT FAILED")
			#ENDIF
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	ADD_PEDS_FOR_DIALOGUE()
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_TEXT, vSafeVec)
					RETURN TRUE //all mission stage requirements set up return true and activate stage
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED") 
			#ENDIF
		BREAK

		CASE MS_BUY
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				SET_PED_POS(PLAYER_PED_ID(), vBoilers, 354.7487)
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_STEAL, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_LEAVE_SHOP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				SET_PED_POS(PLAYER_PED_ID(), <<16.0317, -1118.4553, 27.8667>>, 170.4274)
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_STEAL, FAILED") 
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sits in the NEXT_STAGE() function and moniters for assest that
///    should be unloaded based on the mission state
/// PARAMS:
///    state - the current state we should evaluate
PROC MONITER_UNLOAD_ASSETS(MISSION_STATE state)
	SWITCH state
		CASE MS_SET_UP
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Unloads all models
PROC UNLOAD_ALL_MODELS()
ENDPROC

/// PURPOSE:
///    Unloads all waypoints 
PROC UNLOAD_ALL_WAYPOINTS()
ENDPROC

/// PURPOSE:
///    Unloads all anims
PROC UNLOAD_ANIMS()
ENDPROC

/// PURPOSE:
///    Unload all vehicle recordings 
PROC UNLOAD_ALL_CAR_RECS()
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBlip)
	
ENDPROC


/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
ENDPROC

PROC RELEASE_ALL()
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF
	WAIT_FOR_CUTSCENE_TO_STOP()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	REMOVE_BLIPS()
	UNLOAD_ALL_MODELS()
	UNLOAD_ALL_WAYPOINTS()
	UNLOAD_ANIMS()
	UNLOAD_ALL_CAR_RECS()
	IF bDelAll
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		CLEAR_PRINTS()
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)

	IF DOES_CAM_EXIST(camMain)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(camMain)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()
//	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT,TRUE)
	SERVICES_TOGGLE(TRUE)
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	OPEN_MASK_SHOP_ON_OTHER_PREP()
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	MONITER_UNLOAD_ASSETS(eMissionState)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	SET_BOILER_SUIT_AVAILABILITY(FALSE) // boiler suits locked again (until end of story)
	Mission_Flow_Mission_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

PROC RESET_OUTFITS_ON_FAIL()
	CPRINTLN(DEBUG_MISSION, "Reset Boilers")
	g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = DUMMY_PED_COMP
	g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = DUMMY_PED_COMP
	g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = DUMMY_PED_COMP
	SET_BOILER_SUIT_AVAILABILITY(FALSE) // boiler suits no longer available from shop
ENDPROC

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_BLIPS()
			
			/* (not supported for story missions atm)
			// set if we want to delay the fade
			BOOL bDelayFade
			bDelayFade = FALSE
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				bDelayFade = TRUE // delay the fade if we failed for one of these reasons
			ENDIF
			*/

			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				IF ARE_STRINGS_EQUAL(sFailReason, "PRE_FKICKOFF")
					WHILE (GET_GAME_TIMER() - iWantedFailTimer) < 2000
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF

			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)  
			ELSE
				MISSION_FLOW_MISSION_FAILED()
			ENDIF
			
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				RESET_OUTFITS_ON_FAIL()
				DELETE_ALL()
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
//	bLoadingFinCutscene = FALSE
	bObjectiveShown = FALSE
	eState = SS_INIT
	CLEANUP() //delete everything
ENDPROC

/// PURPOSE:
///    Adds points to a poly 
/// PARAMS:
///    polys - The points to be added to the poly 
///    count - the number of points to be added to the poly 
PROC POPULATE_POLY_CHECKS(VECTOR &polys[], INT count = MAX_POLY_TEST_VERTS)
	 

	OPEN_TEST_POLY(mAreaCheck1)
	INT i
	
	FOR i = 0 TO (count-1)
		ADD_TEST_POLY_VERT(mAreaCheck1, polys[i])
	ENDFOR
	CLOSE_TEST_POLY(mAreaCheck1)
	
	COPY_EXPANDED_POLY(mAreaCheck2, mAreaCheck1, 50)

ENDPROC

/// PURPOSE:
///    Creates a poly check area using a local array
PROC CREATE_POLY_CHECKS()
	VECTOR polys[MAX_POLY_TEST_VERTS]
	polys[0] = << 344.0985, -1605.1539, 28.2928 >>
	polys[1] = << 365.6985, -1580.4351, 28.2928 >>
	polys[2] = << 407.9943, -1616.2589, 28.2928 >>
	polys[3] = << 387.3322, -1641.2583, 29.0912 >>
	POPULATE_POLY_CHECKS(polys, 4)
ENDPROC

PROC POPULATE_VEHICLES()
ENDPROC

PROC POPULATE_OBJECTIVES()
	
	mObj[CHAR_MICHAEL] = "PRE_BUYM"
	
	mObj[CHAR_FRANKLIN] = "PRE_BUYF"
	
	mObj[CHAR_TREVOR] = "PRE_BUYT"
ENDPROC

/// PURPOSE:
///    Initialises all variables and structs
PROC POPULATE_STUFF()
	POPULATE_VEHICLES()
	POPULATE_OBJECTIVES()
	CREATE_POLY_CHECKS()
ENDPROC


/// PURPOSE:
///    Sets the Game world time after a replay/shit skip
PROC SET_TIME_FOR_REPLAY()
	IF g_bShitskipAccepted
	ELSE
	ENDIF

ENDPROC


///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			//DO_SCREEN_FADE_OUT(0)
//			#IF IS_DEBUG_BUILD
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
//			#ENDIF
			
			
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF
			
			IF SETUP_MISSION_STAGE(eMissionState) 			
				
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "MS_BUY"
				#ENDIF

				INT iTimeOut 
//				SERVICES_TOGGLE(FALSE)
				IF IS_REPLAY_IN_PROGRESS()
					VECTOR vReplayVeh
					FLOAT fReplayDir
					IF IS_REPLAY_CHECKPOINT_VEHICLE_UNDER_SIZE_LIMIT(<<0,0,0>>)
						vReplayVeh = <<29.0849, -1111.0872, 28.3095>>
						fReplayDir = 81.7758
					ELSE
						vReplayVeh = <<42.7098, -1105.5011, 28.1318>>
						fReplayDir = 147.8968
					ENDIF

					VEHICLE_INDEX mReplayVehicle
					CREATE_VEHICLE_FOR_REPLAY(mReplayVehicle, vReplayVeh, fReplayDir, FALSE, FALSE, FALSE, FALSE, FALSE)
					END_REPLAY_SETUP()
					iTimeOut = GET_GAME_TIMER()
					WHILE NOT IS_SHOP_OPEN_FOR_BUSINESS(GUN_SHOP_01_DT)
					AND (GET_GAME_TIMER() - iTimeOut) < 2000
						#IF IS_DEBUG_BUILD SK_PRINT_INT("WAITING - Gun shop not open", (GET_GAME_TIMER() - iTimeOut)) #ENDIF
						WAIT(0)
					ENDWHILE
					
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					SAFE_RELEASE_VEHICLE(mReplayVehicle)
					eState = SS_CLEANUP
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
						SET_PED_POS(PLAYER_PED_ID(), <<16.5342, -1122.5829, 27.8635>>, 354.7487)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						iTimeOut = GET_GAME_TIMER()
						WHILE NOT IS_SHOP_OPEN_FOR_BUSINESS(GUN_SHOP_01_DT)
						AND (GET_GAME_TIMER() - iTimeOut) < 2000
							#IF IS_DEBUG_BUILD SK_PRINT_INT("WAITING - Gun shop not open", (GET_GAME_TIMER() - iTimeOut)) #ENDIF
							WAIT(0)
						ENDWHILE
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
					ENDIF
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
		
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC

PROC STAT_PLAYER_IN_SHOP()
	IF NOT bStatOn
		IF IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT)
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI4P5_QUICK_SHOPPER)
			#IF IS_DEBUG_BUILD  SK_PRINT("STARTED TIMER")  #ENDIF
			bStatOn = TRUE	
		ENDIF
	ENDIF
ENDPROC

PROC BUY()
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT BUY")  #ENDIF
				ADD_BLIP_LOCATION_WITH_NAME(biBlip, vBoilers, TRUE, TRUE, "PRE_BLIP")
				IF DOES_BLIP_EXIST(biBlip)
					SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_GREEN)
					SET_BLIP_ROUTE_COLOUR(biBlip, BLIP_COLOUR_GREEN)
				ENDIF
				PRINT_OBJ("PRE_GCLOTH", bObjectiveShown) //Buy a ~g~boiler suit.
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			STAT_PLAYER_IN_SHOP()
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBoilers, 2)
			AND IS_PLAYER_BROWSING_ITEMS_IN_SHOP(GUN_SHOP_01_DT)
				IF NOT bStopPlayerWantedLevel
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_MAX_WANTED_LEVEL(0)
					bStopPlayerWantedLevel = TRUE
				ENDIF
				SAFE_REMOVE_BLIP(biBlip)
			ELSE
				IF bStopPlayerWantedLevel
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_MAX_WANTED_LEVEL(5)
					bStopPlayerWantedLevel = FALSE
				ENDIF
				IF bBoilersBought
					eState = SS_CLEANUP
				ELSE
					IF IS_THIS_PRINT_BEING_DISPLAYED("PRE_BUYM")
					OR IS_THIS_PRINT_BEING_DISPLAYED("PRE_BUYT")
					OR IS_THIS_PRINT_BEING_DISPLAYED("PRE_BUYF")
						CLEAR_PRINTS()
					ENDIF
					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBoilers, 75)
						IF NOT bDeletedBlip
							SAFE_REMOVE_BLIP(biBlip)
							bDeletedBlip = TRUE
						ENDIF
						ADD_BLIP_LOCATION_WITH_NAME(biBlip, vBoilers, TRUE, TRUE, "PRE_SHOP")
					ELIF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBoilers, 12)
						IF bDeletedBlip
							SAFE_REMOVE_BLIP(biBlip)
							bDeletedBlip = FALSE
						ENDIF
						ADD_BLIP_LOCATION_WITH_NAME(biBlip, vBoilers, TRUE, TRUE, "PRE_BLIP")
						IF DOES_BLIP_EXIST(biBlip)
							SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_GREEN)
							SET_BLIP_ROUTE_COLOUR(biBlip, BLIP_COLOUR_GREEN)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP

			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP BUY")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)
			
			enumCharacterList eChar

			BOOL bStatAchieved

			bStatAchieved = TRUE
			REPEAT MAX_BATTLE_BUDDIES eChar
				SWITCH eChar
					CASE CHAR_MICHAEL
						SWITCH g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL]
							CASE OUTFIT_P0_PREP_BOILER_SUIT_1
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_1
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_1
									bStatAchieved = FALSE
								ENDIF
							BREAK
							CASE OUTFIT_P0_PREP_BOILER_SUIT_2
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_2
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_2
									bStatAchieved = FALSE
								ENDIF
							BREAK
							CASE OUTFIT_P0_PREP_BOILER_SUIT_3
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_3
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_3
									bStatAchieved = FALSE
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE CHAR_FRANKLIN
						SWITCH g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN]
							CASE OUTFIT_P1_PREP_BOILER_SUIT_1
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_1
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_1
									bStatAchieved = FALSE
								ENDIF
							BREAK
							CASE OUTFIT_P1_PREP_BOILER_SUIT_2
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_2
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_2
									bStatAchieved = FALSE
								ENDIF
							BREAK
							CASE OUTFIT_P1_PREP_BOILER_SUIT_3
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_3
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_3
									bStatAchieved = FALSE
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
					
					CASE CHAR_TREVOR
						SWITCH g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR]
							CASE OUTFIT_P2_PREP_BOILER_SUIT_1
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_1
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_1
									bStatAchieved = FALSE
								ENDIF
							BREAK
							CASE OUTFIT_P2_PREP_BOILER_SUIT_2
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_2
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_2
									bStatAchieved = FALSE
								ENDIF
							BREAK
							CASE OUTFIT_P2_PREP_BOILER_SUIT_3
								IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_3
								OR g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_3
									bStatAchieved = FALSE
								ENDIF
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH

				
			ENDREPEAT

			IF bStatAchieved
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI4P5_UNITED_COLOURS) // Waititng on all shop assets for player characters
			ENDIF

			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_1
			g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_1
			g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_1
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_SHOP()
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_SHOP")  #ENDIF
				PRINT_OBJ("PRE_LEAVE", bObjectiveShown) //Leave Ammu-Nation.
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF NOT IS_PLAYER_IN_SHOP(GUN_SHOP_01_DT)
				iMissionPassDelayTimer = GET_GAME_TIMER()
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, FBI4P5_QUICK_SHOPPER)
				eState = SS_CLEANUP
			ENDIF
		BREAK

		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("PRE_LEAVE")
				CLEAR_THIS_PRINT("PRE_LEAVE")
			ENDIF
			IF  (GET_GAME_TIMER() - iMissionPassDelayTimer) > 2000
				#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP LEAVE_SHOP")  #ENDIF
				SAFE_REMOVE_BLIP(biBlip)

				IF UPDATE_PHONE_STATES(ePhoneCallState, sPhoneUpdateString, s_conversation_peds)
					Script_Passed()
				ENDIF
			ENDIF
		BREAK

		CASE SS_SKIPPED
			SET_PED_POS(PLAYER_PED_ID(), <<16.0317, -1118.4553, 27.8667>>, 170.4274)
			RC_END_Z_SKIP()
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_1
					g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_1
					g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_1

					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED(FR_NONE)
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
					SWITCH i_debug_jump_stage
						CASE 0 //MS_STEAL
							i_debug_jump_stage++
						BREAK
						
						CASE 1 //MS_RETURN_WITH_THING
							i_debug_jump_stage++
						BREAK

					ENDSWITCH
					
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF

FUNC BOOL HAS_PLAYER_GOT_ENOUGH_CASH()
	INT iCashNeeded = 315
	
	IF iNumberBought = 1
		iCashNeeded = 210
	ELIF iNumberBought = 2
		iCashNeeded = 105
	ENDIF
	
	IF NOT DOES_PLAYER_HAVE_THE_CASH(bBoilersBought, iCashNeeded)
	AND NOT bBoilersBought
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Holds functions that moniter for the player failing
PROC CHECK_FOR_FAIL()
	IF NOT bJumpSkip
	AND eMissionState <> MS_FAILED
		IF eMissionState > MS_SET_UP
			IF NOT bBoilersBought
				IF NOT IS_SHOP_OPEN_FOR_BUSINESS(GUN_SHOP_01_DT)
					#IF IS_DEBUG_BUILD SK_PRINT("SHOP CLOSED") #ENDIF
					MISSION_FAILED(FR_KICKOFFSHOP)
				ENDIF
				
				IF IS_PLAYER_KICKING_OFF_IN_SHOP(GUN_SHOP_01_DT)
					#IF IS_DEBUG_BUILD SK_PRINT("PLAYER KICKED OFF IN SHOP") #ENDIF
					MISSION_FAILED(FR_KICKOFFSHOP)
				ENDIF
				
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					#IF IS_DEBUG_BUILD SK_PRINT("PLAYER HAS WANTED LEVEL") #ENDIF
					iWantedFailTimer = GET_GAME_TIMER()
					MISSION_FAILED(FR_COPS)
					EXIT
				ENDIF
			ENDIF

			IF NOT HAS_PLAYER_GOT_ENOUGH_CASH()
				#IF IS_DEBUG_BUILD SK_PRINT("PLAYER HAS NO CASH") #ENDIF
				MISSION_FAILED(FR_NO_CASH)
				EXIT
			ENDIF
			
			IF NOT bBoilersBought
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBoilers, 150)
					MISSION_FAILED(FR_LEFT)
				ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBoilers, 75)
					IF NOT bWarnLeft
						CLEAR_PRINTS()
						PRINT_WARNING_OBJ("PRE_RETSHOP", bWarnLeft)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_PLAYER_MONEY_ON_REPEAT_PLAY()
	enumBankAccountName	ePlayerAccount
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			ePlayerAccount = BANK_ACCOUNT_MICHAEL
		BREAK
		
		CASE CHAR_FRANKLIN
			ePlayerAccount = BANK_ACCOUNT_FRANKLIN
		BREAK
		
		CASE CHAR_TREVOR
			ePlayerAccount = BANK_ACCOUNT_TREVOR
		BREAK
	ENDSWITCH
	
	IF GET_ACCOUNT_BALANCE(ePlayerAccount) < 315
		CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_CASH_DEPOSIT, 315, FALSE, FALSE)
	ENDIF
ENDPROC

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	AND NOT IS_PLAYER_DEAD(PLAYER_ID())
		#IF IS_DEBUG_BUILD SK_PRINT("Wanted level cleared ") #ENDIF
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINTSTRING("FORCE CLEAN UP") PRINTNL()
		sFailReason = NULL
		Mission_Flow_Mission_Force_Cleanup()
		RESET_OUTFITS_ON_FAIL()
        Script_Cleanup()
	ENDIF

	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF
	POPULATE_STUFF()
	
	// make the boiler suits available from the shop
	SET_BOILER_SUIT_AVAILABILITY(TRUE)
	IF IS_REPLAY_IN_PROGRESS()
		START_REPLAY_SETUP(<<16.5342, -1122.5829, 27.8635>>, 354.7487)
	ENDIF
	CLOSE_MASK_SHOP_ON_OTHER_PREP()
	IF IS_REPEAT_PLAY_ACTIVE()
		FORCE_SHOP_RESET(GUN_SHOP_01_DT)
		GIVE_PLAYER_MONEY_ON_REPEAT_PLAY()
	ENDIF
	
	//Remove potential boilers from inventory
	g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = DUMMY_PED_COMP
	g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = DUMMY_PED_COMP
	g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = DUMMY_PED_COMP
	#IF IS_DEBUG_BUILD SK_PRINT("Cleared boiler suit globals") #ENDIF
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BS")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF eMissionState = MS_BUY
				iNumberBought = GET_NUM_OF_BOILERS_BOUGHT()
				IF HAS_PLAYER_AQUIRED_OUTFITS()
					bBoilersBought = TRUE
				ENDIF
			ENDIF

			CHECK_FOR_FAIL()
			SWITCH eMissionState
			
				CASE MS_SET_UP
					INITMISSION()
				BREAK
				
				CASE MS_BUY
					BUY()
				BREAK
				
				CASE MS_LEAVE_SHOP
					LEAVE_SHOP()
				BREAK
				
				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK
			ENDSWITCH

			IF eMissionState <> MS_FAILED
				IF eMissionState >= MS_SET_UP
				AND NOT bJumpSkip
				ENDIF

				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


