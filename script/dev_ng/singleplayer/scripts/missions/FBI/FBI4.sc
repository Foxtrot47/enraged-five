
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					40
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					4
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				22 
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		14
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		15 
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		4


const_int total_number_of_pickups 3
const_int total_number_of_police_cars 14
const_int total_number_of_police_men 36
const_int total_number_of_absail_police_men 4
const_int total_number_of_fires	24
const_int total_number_of_backup_police_front 3//5
const_int total_number_of_backup_police_alley 3
const_int total_number_of_security_guards 2

const_int MAX_SKIP_MENU_LENGTH 7

USING "rage_builtins.sch"
USING "globals.sch"
using "script_heist.sch"
USING "flow_public_core_override.sch"
using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "commands_misc.sch"
using "commands_recording.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "model_enums.sch"
using "cellphone_public.sch"
using "selector_public.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
using "chase_hint_cam.sch"
using "locates_public.sch"
using "commands_physics.sch"
using "script_blips.sch" 
using "select_mission_stage.sch"
using "replay_public.sch"
using "traffic.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "CompletionPercentage_public.sch"
using "building_control_public.sch"
using "script_ped.sch"
using "emergency_call.sch"
using "clearmissionarea.sch"
using "player_scene_private.sch"
using "spline_cam_edit.sch"
using "achievement_public.sch"

//#IF IS_DEBUG_BUILD
//	USING "rerecord_recording.sch"
//#ENDIF

struct ped_structure
	ped_index ped 
	model_names model 
	blip_index blip
	vector pos 
	vector run_to_pos
	float heading 
	int health 
	int accuracy
	bool damaged_by_player
	bool allow_body_armour
	bool created 
	weapon_type weapon
	text_label name
endstruct 


struct vehicle_struct
	vehicle_index veh
	model_names model
	blip_index blip
	vector pos 
	float heading
	float speed
	float skip_time
	int health
	float engine_health
	float petrol_tank_health
	int colour
	int recording_number
	bool been_created 
endstruct

struct object_struct 
	object_index obj
	blip_index blip
	model_names model
	vector pos 
	vector offset_pos
	vector offset_pos_2
	vector rot
	vector offset_rot
	float heading
	float scale 
	int health
	string room_name
	int time
	bool been_created
endstruct 

struct pickup_struct 
	pickup_index pickup
	model_names model
	pickup_type type
	blip_index blip
	vector pos
	vector rot
	bool collected
	string room_name
endstruct 

STRUCT ROPE_struct
	ROPE_INDEX rope
	FLOAT f_length
	INT i_num_segments
	FLOAT f_length_per_segment
	vector pos
	vector rot 
ENDSTRUCT

struct fire_struct
	fire_index fire
	vector offset
	bool set
endstruct 


enum main_mission_flow
	intro_mocap = 0,
	get_ambulance_into_pos,
	vehicle_spotted,
	park_ambulance,
	ram_army_truck,
	blow_open_truck_doors,
	shootout,
	switch_to_michael_or_franklin,
	burn_rubbish_truck,
	phone_call_to_michael,
	switch_to_michael,
	meet_devin,
	pass_cutscene,
	cover_blown, 
	load_stage_selector_assets, 
	mission_fail_stage, 
	mag_demo_2_setup, 
	mag_demo_2_end
endenum 

enum shootout_enum
	leave_car_or_fight = 0, 
	run_to_pos_or_fight,
	get_dead_police_men,
	run_to_pos_2, 
	run_to_pos_2_status,
	do_nothing
endenum 

enum absail_shootout_enum
	create_absail_ped = 0, 
	pause_anim,
	slow_down_helicopter,
	unpause_anim,
	absail_playing_anim,
	absail_with_sliding,
	absail_run_to_pos,
	absail_get_into_pos, 
	absail_do_nothing
endenum

main_mission_flow mission_flow = intro_mocap
shootout_enum shootout_status[total_number_of_police_men]  
shootout_enum backup_police_front_shootout_status[total_number_of_backup_police_front]
shootout_enum backup_police_alley_shootout_status[total_number_of_backup_police_alley]
absail_shootout_enum absail_police_man_status[total_number_of_absail_police_men] 

vehicle_struct rubbish_truck 
vehicle_struct army_truck
vehicle_struct tow_truck
vehicle_struct police_car[total_number_of_police_cars]
vehicle_struct helicopter[2]
vehicle_struct devin_car
vehicle_struct trevors_car
vehicle_struct traffic_vehicle[8]
vehicle_struct michaels_car
vehicle_struct get_away_car

ped_structure army_truck_driver
ped_structure army_truck_passenger 
ped_structure army_truck_guy
ped_structure army_truck_guy_2
ped_structure police_man[total_number_of_police_men]
ped_structure absail_police_man[total_number_of_absail_police_men]
ped_structure helicopter_driver[2]
ped_structure devin
ped_structure dave
ped_structure steve
ped_structure andreas
ped_structure backup_police_front[total_number_of_backup_police_front]
ped_structure backup_police_alley[total_number_of_backup_police_alley]
ped_structure security_guard[total_number_of_security_guards]

selector_slots_enum target_switch_to_ped

fire_struct fire[total_number_of_fires]

object_struct c4
object_struct binoculars
object_struct van_light
object_struct devins_phone
object_struct documents
//object_struct bin[2]
//object_struct skip[2]

rope_struct rope[2]

LOCATES_HEADER_DATA locates_data

SELECTOR_PED_STRUCT selector_ped
SELECTOR_CAM_STRUCT sCamDetails


bool stop_mission_fail_checks = false
bool help_text_rendered = false
bool allow_hotswap = false
bool player_in_area = false
bool outside_area_text_printed = false
bool start_ram_time_2 = false 
bool start_ram_time_3 = false
bool switch_reminder = false
bool assets_cleaned_up 
//bool drive_to_end_vehicle_selected = false
bool force_applied_to_helicopter = false
bool tow_truck_switched_to_ai = false
bool player_told_to_get_into_cover = false
bool scale_form_movie_active = false
bool area_fail = false
bool standing_away_from_c4 = false
bool c4_planted_manually = false
//bool michael_aiming_task_given[3]
bool hover_backwards = false
bool apply_fail_time = false
bool rubbish_truck_out_of_burning_area = false
bool boiler_suits_applied = false
bool create_debug_getaway_car = false
//bool police_man_8_killed_by_trevor = false 
//bool police_man_9_killed_by_trevor = false
//bool police_man_31_killed_by_trevor = false
bool player_in_cover_down_alley = false
bool setup_rubbish_truck_for_switch = false
bool mag_demo_cover_set = false
bool clenaup_fake_wanted_level = false
bool blip_truck_on_leaving_on_first_occasion = false
bool wave_1_police_created = false
bool wave_2_police_created = false
bool wave_3_police_created = false
bool wave_5_police_created = false
bool preload_outfits = false
bool brake_truck_if_player_gets_close_to_get_away_vehicle = false
bool burn_rubbish_truck_distance_saftey_check_active = false
bool attacked_lesters_security_fail = false
bool sound_army_horn = false
bool snipers_in_position = false
bool wave_2_front_dialogue_triggered = false
bool trigger_switch_effect_to_michael = false
bool fbi_4_binoculars_srl_requested = false
bool FBI_4_MCS_2_CONCAT_prestreaming_srl = false
bool apply_money_truck_roof_damage = false
bool sniper_killed_by_trevor = false
bool trevor_binoculars_hit_out_0 = false
bool trevor_binoculars_hit_out_1 = false
bool trevor_binoculars_hit_out_2 = false
bool trevor_binoculars_hit_out_3 = false
bool trevor_binoculars_transition_0 = false

int fbi4_vehicle_spotted_status = 0
int park_ambulance_status = 0
int original_time
int ambulance_cutscene_status = 0
int ram_army_truck_status = 0
int ram_army_truck_cutscene_status = 0
int michael_ai_system_status = 0
int blow_open_truck_doors_status = 0
int michael_cutscene_index
int cutscene_index
int cutscene_index_2
int hot_swap_system_status = 0
int factory_worker_0_system_status = 0
int army_truck_driver_ai_status = 0
int army_truck_passenger_ai_status = 0
int surveillance_status = 0
int dialogue_time
int dialogue_status= 0
int area_time
int ram_time_2
int ram_time_3
int park_time
int truck_ram_time
int army_truck_guy_ai_status = 0
int smashing_wall_rayfire_status = 0
int rope_system_status = 0
int hot_swap_time
int army_truck_guy_ai_2_status = 0
int helicopter_speed_system_status = 0
int devin_ai_system_status
int fbi4_get_ambulance_into_pos_status = 0
int shootout_master_controler_status = 0
//int random_time
int fbi4_pass_cutscene_status = 0
int c4_explosion_cutscene_status = 0
int rayfire_sound = 0
int explosion_sound = 0
int franklin_ai_system_2_status = 0
int i_triggered_text_hashes[20]
int switch_dialogue_status
int fbi4_intro_mocap_status = 0
int blow_open_truck_doors_dialogue_status = 0
int end_of_shootout_dialogue_status = 0
int fbi4_mission_attempts = 0
int get_ambulance_into_position_dialogue_status = 0
int police_vehicle_explosion_system_status = 0
int trevor_time = 0
int trevor_ai_system_status = 0
int cops_killed_during_shootout = 0
int original_cops_killed = 0
int shootout_time 
int shootout_wanted_level_system_status = 0
int shootout_music_event_system_status = 0
int uber_speed_system_status = 0
int update_dispatch_pos_status = 0
int dispatch_spawn_position_system_status = 0
int dispatch_time = 0
int devin_scene_id = -1
int petrol_can_help_system_status = 0
int burn_rubbish_truck_master_flow_system_status = 0
int fire_system_status = 0
int fire_time
int fire_shot_up_index 
int fire_shot_down_index
int burn_rubbish_truck_music_event_system_status = 0
int debug_intro_mocap_screen_status = 0
int plant_bomb_time = 0
int fail_time = 0
int switch_to_michael_or_franklin_status = 0
int phone_call_to_michael_status = 0
int switch_to_michael_status = 0
int layby_cutscene_status = 0
int horn_time = 0
int meet_devin_master_flow_status = 0
int snipers_dialogue_system_status = 0
int police_man_time[total_number_of_police_men]
int sniper_dialogue_time = 0
int police_man_8_z_offset_time = 0
int police_man_9_z_offset_time = 0
int fbi4_mag_demo_2_setup_status = 0
int michael_defensive_sphere_system_status = 0
int franklin_defensive_sphere_system_status = 0
int backup_police_front_counter = 0
int backup_police_alley_counter = 0
int total_number_of_points = 0
int wave_1_alley_police_dead_count = 0
int backup_police_front_spawn_system_status = 0
int backup_police_alley_spawn_system_status = 0
int wave_0_alive_count = 0
int helicopter_time[2]
int helicopter_1_sysstem_status = 0
int mag_demo_2_end_status = 0
int helicopter_0_system_status = 0
int michael_ai_system_2_status = 0
int police_vehicle_siren_system_status = 0
int apply_damage_time = 0
int health_reduction_time = 0
int force_ped_to_switch_damage_hint_system_status = 0
int left_alley_dialogue_system_status = 0
int flee_time = 0
int audio_explosion_system_status = 0
int wave_1_timer_0 = 0
int wave_1_timer_1 = 0
int cop_dead_stat_time = 0
int total_number_of_wave_2_peds = 0
int total_number_of_wave_4_peds = 0
int global_backup_police_front_dead_count = 0
int global_backup_police_alley_dead_count = 0
int weapon_fail_time = 0
int get_away_car_time = 0
int blow_open_struck_doors_audio_scene_system_status = 0
int burn_rubbish_truck_audio_scene_system_status = 0
int health_dialogue_time = 0
int wave_2_dialogue_system_status = 0
int wave_4_dialogue_system_status = 0
int wave_5_dialogue_system_status = 0
int total_police_men_alive_count = 0
int create_wave_1_police_status = 0
int create_wave_2_police_status = 0
int michael_special_ability_time = 0
int burn_truck_time = 0
int security_guard_ai_system_status[total_number_of_security_guards]
int switch_to_michael_vehicle_recording_controller_status = 0
int take_off_mask_anim_system_status = 0
int switch_to_franklin_time = 0
int trevor_ai_system_2_status = 0
int switch_sound_time = 0

int police_siren_sound = get_sound_id()
int rubbish_truck_explosion_sound = get_sound_id()


//int player_cutscene_index
//int force_time
//int fire_time
//int petrol_shot_up_index = 0
//int petrol_shot_down_index = 0

#IF IS_DEBUG_BUILD
	int get_ambulance_into_pos_skip_status = 0
	int p_skip_time 
	//int shootout_skip_status = 0
#endif 

vector ambulance_target_pos
vector cutscene_pos
vector cutscene_rot
vector michael_run_to_pos
vector final_node_rot
vector rpg_target_pos 
vector fire_start_pos 
vector cached_player_pos
vector michael_car_node[2]

float current_time_scale = 0.6
//float relative_player_heading
float hover_speed = 1.0
//float time_step
//float door_ratio
float police_man_8_z_offset = 1.0
float police_man_9_z_offset = 1.0

string warning_string
string mission_failed_text

blip_index ambulance_target_blip
blip_index bomb_blip
blip_index michael_blip
blip_index trevor_blip
blip_index franklin_blip
blip_index player_cover_blip 
//blip_index drive_to_end_vehicle_blip

camera_index camera_a
camera_index camera_b
camera_index camera_c
camera_index camera_d
camera_index camera_e
camera_index camera_f
camera_index camera_g
camera_index camera_h
camera_index final_node_cam
camera_index camera_anim

sequence_index seq

ped_index factory_worker

//vehicle_index drive_to_end_vehicle 
vehicle_index temp_vehicle
vehicle_index players_last_vehicle

coverpoint_index michael_cover_point
coverpoint_index michael_cover_point_2
coverpoint_index players_cover_point
coverpoint_index mag_demo_cover_point

//string get_in_vehicle_string
//string get_back_in_vehicle_string 

//vehicle_index parked_cars[2]

structPedsForConversation scripted_speech[2]

rayfire_index smashing_wall

scaleform_index sf_binoculars = null

streamvol_id streaming_volume

cam_view_mode players_vehicle_cam_mode

VEHICLE_GEN_DATA_STRUCT get_away_vehicle


#IF IS_DEBUG_BUILD
widget_group_id fbi_4_widget_group
MissionStageMenuTextStruct menu_stage_selector[MAX_SKIP_MENU_LENGTH]
int menu_return_stage
#endif 


//*****not present in initialise_mission_variables()
int launch_mission_stage_menu_status = 0

bool switched_to_michael = false
bool switched_to_franklin = false
bool switched_to_trevor = false
bool replay_active = false

int ramming_truck_blipping_system_bit_set 
const_int get_back_in_vehicle 0
const_int ram_the_truck 1


//****************************************LAWRENCE SDK****************************************

proc disable_dispatch_services()
		
	enable_dispatch_service(DT_POLICE_AUTOMOBILE, false) 
    enable_dispatch_service(DT_POLICE_HELICOPTER, false) 
    enable_dispatch_service(DT_FIRE_DEPARTMENT, false) 
  	enable_dispatch_service(DT_SWAT_AUTOMOBILE, false) 
	enable_dispatch_service(DT_SWAT_HELICOPTER, false)
  	enable_dispatch_service(DT_AMBULANCE_DEPARTMENT, false) 
    enable_dispatch_service(DT_POLICE_RIDERS, false)  
    enable_dispatch_service(DT_POLICE_VEHICLE_REQUEST, false) 
    enable_dispatch_service(DT_POLICE_ROAD_BLOCK, false) 
    enable_dispatch_service(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, false) 
    enable_dispatch_service(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, false) 
    enable_dispatch_service(DT_GANGS, false) 
    enable_dispatch_service(DT_SWAT_HELICOPTER, false) 
   	enable_dispatch_service(DT_POLICE_BOAT, false) 
	
endproc 

proc enable_dispatch_services()

	enable_dispatch_service(DT_POLICE_AUTOMOBILE, true) 
    enable_dispatch_service(DT_POLICE_HELICOPTER, true) 
    enable_dispatch_service(DT_FIRE_DEPARTMENT, true) 
  	enable_dispatch_service(DT_SWAT_AUTOMOBILE, true) 
	enable_dispatch_service(DT_SWAT_HELICOPTER, true)
  	enable_dispatch_service(DT_AMBULANCE_DEPARTMENT, true) 
    enable_dispatch_service(DT_POLICE_RIDERS, true)  
    enable_dispatch_service(DT_POLICE_VEHICLE_REQUEST, true) 
    enable_dispatch_service(DT_POLICE_ROAD_BLOCK, true) 
    enable_dispatch_service(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, true) 
    enable_dispatch_service(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, true) 
    enable_dispatch_service(DT_GANGS, true) 
    enable_dispatch_service(DT_SWAT_HELICOPTER, true) 
   	enable_dispatch_service(DT_POLICE_BOAT, true) 

endproc 

func bool start_new_cutscene_no_fade(bool clear_players_tasks = true, bool hide_weapon_for_cutscene = true, bool kill_conversation_line_immediately = true, bool ignore_can_player_start_cutscene = false, bool bUseBlinders = TRUE)

	if can_player_start_cutscene() or ignore_can_player_start_cutscene
	
		SPECIAL_ABILITY_DEACTIVATE(player_id())
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(true, DEFAULT,bUseBlinders)
		
		clear_prints()
		clear_help()
		if kill_conversation_line_immediately
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
		else 
			kill_any_conversation()
		endif 
		
		SET_FRONTEND_RADIO_ACTIVE(false)
		
		display_hud(false)
		display_radar(false)
		set_widescreen_borders(true, 500)
		
		if clear_players_tasks
			set_player_control(player_id(), false, spc_clear_tasks)
		else 
			set_player_control(player_id(), false)
		endif 

		if hide_weapon_for_cutscene
			hide_ped_weapon_for_scripted_cutscene(player_ped_id(), true)
		endif 
		
		disable_dispatch_services()
		
		return true
	endif 
		
	return false

endfunc


proc end_cutscene(bool clear_tasks = true, bool update_game_camera = true, float interp_heading = 0.0, float interp_pitch = 0.0, bool enable_emergency_services = true, bool bUseBlinders = TRUE)

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false, default, bUseBlinders)

	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	SET_FRONTEND_RADIO_ACTIVE(true)
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	
	if is_player_playing(player_id())
	
		destroy_all_cams()
		
		if update_game_camera
			render_script_cams(false, false)
			set_gameplay_cam_relative_heading(interp_heading)
			set_gameplay_cam_relative_pitch(interp_pitch)
		endif 
			
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		set_player_control(player_id(), true)
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
	endif 

	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 
	
//	if mission_car_marked 
//		mark_vehicle_as_no_longer_needed(current_car)
//		mission_car_marked = false
//	endif 
	
	do_screen_fade_in(DEFAULT_FADE_TIME)
	
endproc

proc end_cutscene_no_fade(bool clear_tasks = true, bool update_game_camera = true, bool interpolate_behind_player = false, float interp_heading = 0.0, float interp_pitch = 0.0, int interp_to_game_time = 3000, bool enable_emergency_services = true, bool bUseBlinders = TRUE)

	INFORM_MISSION_STATS_SYSTEM_OF_INengine_CUTSCENE_END()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(false, default, bUseBlinders)
	
	clear_prints()
	clear_help()
	kill_face_to_face_conversation()
	
	SET_FRONTEND_RADIO_ACTIVE(true)
	
	display_hud(true)
	display_radar(true)
	set_widescreen_borders(false, 500)
	
	if is_player_playing(player_id())
		
		destroy_all_cams()
		
		if update_game_camera
			if interpolate_behind_player
				render_script_cams(false, true, interp_to_game_time)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			else 
				render_script_cams(false, false)
				set_gameplay_cam_relative_heading(interp_heading)
				set_gameplay_cam_relative_pitch(interp_pitch)
			endif 
		endif 
		
		if clear_tasks
			clear_ped_tasks(player_ped_id())
		endif 
		
		enable_special_ability(player_id(), true)
		
		hide_ped_weapon_for_scripted_cutscene(player_ped_id(), false)
		
		set_player_control(player_id(), true)
	endif 
	
	if enable_emergency_services
		enable_dispatch_services()
	else 
		disable_dispatch_services()
	endif 

endproc 

func bool lk_timer(int &start_time, int time) 
	int current_time 
	current_time = get_game_timer()
	
	if ((current_time - start_time) > time) 
		return true
	endif 
	
	return false
endfunc

func bool is_skip_button_pressed()
	
	return is_control_just_pressed(frontend_control, input_frontend_accept)
	
endfunc 

func bool skip_scripted_cut(int &original_time_temp, int cutscene_skip_time)
	if lk_timer(original_time_temp, cutscene_skip_time)
		if is_screen_faded_in()
			if IS_CUTSCENE_SKIP_BUTTON_PRESSED()	
				return true 
			endif 
		endif 
	endif 
	
	return false 
endfunc 

func bool mission_ped_injured(ped_index &this_ped)

	if DOES_ENTITY_EXIST(this_ped)
		return is_ped_injured(this_ped)
	endif 
	
	return false
	
endfunc 

func bool mission_vehicle_injured(vehicle_index &this_vehicle)
	
	if DOES_ENTITY_EXIST(this_vehicle)
		if not is_vehicle_driveable(this_vehicle)
			return true
		endif 
	endif 
	
	return false
	
endfunc 

func bool is_mission_entity_attacked(ped_index &mission_ped, bool clear_damage_entity = false)
	
	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_ped, PLAYER_ped_ID())
				if clear_damage_entity
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mission_ped)
				endif 
				return true
			endif 
		else 
			return true
		endif
	endif 
	
	return false
endfunc 

func bool has_char_task_finished_2(ped_index ped, script_task_name taskname)
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if status = finished_task 
	or status = dormant_task
		return true
	endif
	
	return false
endfunc

//gets the currenent car the player is in
func bool get_current_player_vehicle(vehicle_index &test_car)
	
	if is_ped_sitting_in_any_vehicle(player_ped_id())
		test_car = get_players_last_vehicle()
		if DOES_ENTITY_EXIST(test_car)	
			if is_vehicle_driveable(test_car)
				return true
			endif 
		endif 
	endif
	
	return false
endfunc

PROC STOP_PLAYER_vehicle()

	VEHICLE_INDEX player_car
	
	if get_current_player_vehicle(player_car)
		
		FLOAT player_car_speed
			
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		SET_ENTITY_PROOFS(player_car, true, true, true, true, true)

		player_car_speed = GET_ENTITY_SPEED(player_car)
			 
		WHILE player_car_speed > 0.2
			WAIT(0)
			if not IS_ENTITY_DEAD(player_car)
				player_car_speed = GET_ENTITY_SPEED(player_car)
			endif 
		ENDWHILE

		SET_ENTITY_PROOFS(player_car, false, false, false, false, false)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

	ENDIF
ENDPROC

/// PURPOSE: add ped to players group
///
func bool add_ped_to_players_group(ped_index &this_ped, group_index &players_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if not is_ped_injured(this_ped)
	
			players_group = get_player_group(player_id())
			
			set_ped_as_group_member(this_ped, players_group)
			
			return true
		endif 
	endif 

	return false
	
endfunc 

func float distance_from_player_to_ped(ped_index &mission_ped)

	vector player_pos
	vector mission_ped_pos 

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			
			player_pos = GET_ENTITY_COORDS(player_ped_id()) 
			mission_ped_pos = GET_ENTITY_COORDS(mission_ped)
			
			return get_distance_between_coords(player_pos, mission_ped_pos)
		endif 
	endif 
	
	return -1.0

endfunc 

//func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence, int sequence_progress = -2)
//	
//	scripttaskstatus status
//	status = get_script_task_status(ped, taskname)
//	
//	if status = finished_task 
//	or status = dormant_task
//	or get_sequence_progress(ped) = sequence_progress
//		return true
//	endif
//	
//	return false
//endfunc

func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence, int sequence_progress = -2, bool use_seq = true)
	
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if use_seq
	
		if status = finished_task 
		or status = dormant_task
		or get_sequence_progress(ped) = sequence_progress
			return true
		endif
		
	else 
	
		if status = finished_task 
		or status = dormant_task
			return true
		endif
	
	endif 
	
	return false
endfunc

func bool is_ped_playing_anim_at_phase(string anim_dict_name, string anim_name, float phase_target)
		
	if IS_ENTITY_PLAYING_ANIM(player_ped_id(), anim_dict_name, anim_name) 
		if GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(), anim_dict_name, anim_name) > phase_target 
			return true 
		endif 
	endif 
	
	return false 
	
endfunc

//The players group does not include the player himself. RELGROUPHASH_PLAYER is the player
REL_GROUP_HASH player_group
REL_GROUP_HASH enemy_group

    
proc add_relationship_groups()

	ADD_RELATIONSHIP_GROUP("players group", player_group)
	ADD_RELATIONSHIP_GROUP("enemy group", enemy_group)
	
endproc 

proc setup_relationship_contact(ped_index &this_ped, bool block_temporary_events = false)

	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, relgrouphash_cop)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(this_ped, player_group) 
			
			SET_PED_COMBAT_MOVEMENT(this_ped, CM_DEFENSIVE)
			
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, ca_use_vehicle, false)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_LEAVE_VEHICLES, true)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, true)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_DISABLE_PINNED_DOWN, true)
			
			SET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, false)
			
			set_ped_accuracy(this_ped, 60)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
		endif 
	endif 

endproc

//if get_player_wanted_level(player_id()) > 0
//	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, relgrouphash_cop)
//else 
//	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, relgrouphash_cop)	
//endif 

proc setup_relationship_enemy(ped_index &this_ped, bool block_temporary_events = false)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, player_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, player_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, enemy_group, RELGROUPHASH_COP)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, enemy_group)
	
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, player_group, enemy_group)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_between_groups(ACQUAINTANCE_TYPE_PED_HATE, enemy_group, player_group)
	
	if DOES_ENTITY_EXIST(this_ped)
		if NOT is_ped_injured(this_ped)
			
			SET_ped_RELATIONSHIP_GROUP_hash(this_ped, enemy_group) 
			
			set_ped_combat_attributes(this_ped, ca_will_scan_for_dead_peds, false)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_USE_VEHICLE, false)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_LEAVE_VEHICLES, true)
			SET_PED_COMBAT_ATTRIBUTES(this_ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
			
			SET_PED_TARGET_LOSS_RESPONSE(this_ped, tlr_never_lose_target)
			
			set_entity_is_target_priority(this_ped, true)
			
			if block_temporary_events
				set_blocking_of_non_temporary_events(this_ped, true)
			else 
				set_blocking_of_non_temporary_events(this_ped, false)
			endif 
		endif 
	endif 
	
endproc


proc setup_enemy(ped_structure &enemy, bool create_blip = false)

	clear_area(enemy.pos, 2.0, false)
		
	enemy.ped = create_ped(pedtype_cop, enemy.model, enemy.pos, enemy.heading)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, false) //only for cops peds
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	if create_blip
		enemy.blip = create_blip_for_ped(enemy.ped, true)
		set_blip_display(enemy.blip, DISPLAY_BLIP) 
	endif 
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	set_blocking_of_non_temporary_events(enemy.ped, true)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)
	
	ADD_DEADPOOL_TRIGGER(enemy.ped, FBI4_KILLS)

endproc


proc setup_enemy_in_vehicle(ped_structure &enemy, vehicle_index &mission_veh, vehicle_seat veh_seat = vs_driver, bool create_blip = false)

	clear_area(enemy.pos, 2.0, true)
		
	enemy.ped = create_ped_inside_vehicle(mission_veh, pedtype_cop, enemy.model, veh_seat)
	set_ped_random_component_variation(enemy.ped)
	set_ped_dies_when_injured(enemy.ped, true)
	set_ped_as_enemy(enemy.ped, true)
	set_entity_is_target_priority(enemy.ped, true)
	set_ped_keep_task(enemy.ped, true)
	SET_PED_CONFIG_FLAG(enemy.ped, PCF_KeepRelationshipGroupAfterCleanUp, false) //only for cops peds
	
	give_weapon_to_ped(enemy.ped, enemy.weapon, infinite_ammo, true)
	set_ped_accuracy(enemy.ped, enemy.accuracy)
	
	SET_ENTITY_HEALTH(enemy.ped, enemy.health)
	set_ped_max_health(enemy.ped, enemy.health)
		
	if enemy.damaged_by_player
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(enemy.ped, true)
	endif 
	
	if enemy.allow_body_armour 
		add_armour_to_ped(enemy.ped, 100)
	endif 
	
	SET_PED_MONEY(enemy.ped, 0)
	
	enemy.created = true 
	
	set_ped_name_debug(enemy.ped, enemy.name)
	
	set_ped_hearing_range(enemy.ped, 250.00)
	set_ped_seeing_range(enemy.ped, 250.00)
	set_ped_id_range(enemy.ped, 250.00)
	
	if create_blip
		enemy.blip = create_blip_for_ped(enemy.ped, true)
		set_blip_display(enemy.blip, DISPLAY_BLIP) 
	endif
	
	set_ped_can_evasive_dive(enemy.ped, true)
	
	set_blocking_of_non_temporary_events(enemy.ped, true)
	
	ADD_DEADPOOL_TRIGGER(enemy.ped, FBI4_KILLS)
	
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemy.ped)

endproc

func bool ped_structure_are_all_enemies_dead(ped_structure &enemy_array[], bool set_ped_no_longer_needed = true)
	
	int i = 0
	int peds_dead
	
	for i = 0 to (count_of(enemy_array) - 1)
		
		if DOES_ENTITY_EXIST(enemy_array[i].ped)
			if is_ped_injured(enemy_array[i].ped)

				if set_ped_no_longer_needed
					SET_PED_AS_NO_LONGER_NEEDED(enemy_array[i].ped) 
				endif 
				
				if does_blip_exist(enemy_array[i].blip)
					REMOVE_BLIP(enemy_array[i].blip)
				endif 
				
				peds_dead ++
			endif 
		
		else 
		
			if enemy_array[i].created
				peds_dead ++
			endif 
			
		endif 
		
	endfor 
	
	if peds_dead = count_of(enemy_array) 
		return TRUE
	endif 
	
	return FALSE
endfunc


//*****call this function underneath ped_structure_are_all_enemies_dead
func bool ped_structure_are_specific_number_enemies_dead(ped_structure &enemy_array[], int number_of_dead_peds)
	
	int peds_dead = 0
	int i = 0
	
	for i = 0 to count_of(enemy_array) - 1
		if enemy_array[i].created
			if is_ped_injured(enemy_array[i].ped)
				peds_dead++
			endif
		endif 
	endfor 
	
	if peds_dead >= number_of_dead_peds
		return TRUE
	endif 
	
	return FALSE
endfunc

func int ped_structure_get_total_number_of_enemies_dead(ped_structure &enemy_array[])

	int peds_dead = 0
	int i = 0
	
	for i = 0 to count_of(enemy_array) - 1
		if enemy_array[i].created
			if is_ped_injured(enemy_array[i].ped)
				peds_dead++
			endif
		endif 
	endfor 

	return peds_dead

endfunc 

func bool are_backup_police_dead(ped_structure &backup_police[])

	int peds_dead = 0
	int i = 0

	for i = 0 to count_of(backup_police) - 1
	
		if does_entity_exist(backup_police[i].ped)
		
			if is_ped_injured(backup_police[i].ped)
		
				set_ped_as_no_longer_needed(backup_police[i].ped)
				
				if (backup_police[i].ped = backup_police_front[i].ped)
					
					global_backup_police_front_dead_count++
				
				elif (backup_police[i].ped = backup_police_alley[i].ped)
					
					global_backup_police_alley_dead_count++
					
				endif 
				
				peds_dead++
				
			endif 
			
		else 
		
			peds_dead++
		
		endif 
		
	endfor 
	
	global_backup_police_front_dead_count = global_backup_police_front_dead_count
	global_backup_police_alley_dead_count = global_backup_police_alley_dead_count
	
	if peds_dead = count_of(backup_police)
		return true 
	endif 
	
	return false
		
endfunc


//You can't use does_entity_exist with backup_police[i].created as you may have an instance because of 
//backup_police_front_spawn_system() where only 3 peds are created when going into the final shootout case
//so the counters won't work as it won't count peds that don't exit

func int get_total_number_of_backup_police_dead(ped_structure &backup_police[])

	int peds_dead = 0
	int i = 0
	
	for i = 0 to count_of(backup_police) - 1
		if is_ped_injured(backup_police[i].ped)
			peds_dead++
		endif 
	endfor 

	return peds_dead

endfunc 

///PURPOSE: makes sure slots 0 and 1 are not re-used when 0 and 1 die. It will create the next ped in slot 2
proc create_backup_police_front()

	int i = 0

	for i = backup_police_front_counter to count_of(backup_police_front) - 1
	
		if not does_entity_exist(backup_police_front[i].ped)
		
			setup_enemy(backup_police_front[i])
			setup_relationship_enemy(backup_police_front[i].ped, true)
			backup_police_front_shootout_status[i] = leave_car_or_fight
			
			backup_police_front_counter++
			
			if backup_police_front_counter >= count_of(backup_police_front)
				backup_police_front_counter = 0
			endif 
			
			exit
			
		endif
	
	endfor 
	
	for i = 0 to backup_police_front_counter - 1
	
		if not does_entity_exist(backup_police_front[i].ped)
		
			setup_enemy(backup_police_front[i])
			setup_relationship_enemy(backup_police_front[i].ped, true)
			backup_police_front_shootout_status[i] = leave_car_or_fight
				
			backup_police_front_counter++
			
			if backup_police_front_counter >= count_of(backup_police_front)
				backup_police_front_counter = 0
			endif 
			
			exit
			
		endif
	
	endfor 

endproc 

proc backup_police_front_spawn_system()

	if player_in_area

		int i = 0
				
		total_police_men_alive_count = 0
		
		for i = 0 to count_of(police_man) - 1
			if not is_ped_injured(police_man[i].ped)
				total_police_men_alive_count++
			endif 
		endfor 
		
		for i = 0 to count_of(backup_police_front) - 1
			if not is_ped_injured(backup_police_front[i].ped)
				total_police_men_alive_count++
			endif 
		endfor 
		
		for i = 0 to count_of(backup_police_alley) - 1
			if not is_ped_injured(backup_police_alley[i].ped)
				total_police_men_alive_count++
			endif 
		endfor 
		
		if total_police_men_alive_count < 10
		
			switch backup_police_front_spawn_system_status 
			
				case 0

					if get_total_number_of_backup_police_dead(backup_police_front) >= 2
						create_backup_police_front()
						create_backup_police_front()
						backup_police_front_spawn_system_status++
					endif 
					
				break 
				
				case 1
				
					if get_total_number_of_backup_police_dead(backup_police_front) >= 1
						create_backup_police_front()
						backup_police_front_spawn_system_status++
					endif 
				
				break 
				
				case 2
				
					if get_total_number_of_backup_police_dead(backup_police_front) >= 2//2
						create_backup_police_front()
						create_backup_police_front()
						backup_police_front_spawn_system_status = 0
					endif 
				
				break 
				
			endswitch 
			
		endif 

	endif 

endproc 

proc create_backup_police_alley()

	int i = 0

	for i = backup_police_alley_counter to count_of(backup_police_alley) - 1
	
		if not does_entity_exist(backup_police_alley[i].ped)
		
			setup_enemy(backup_police_alley[i])
			setup_relationship_enemy(backup_police_alley[i].ped, true)
			backup_police_alley_shootout_status[i] = leave_car_or_fight
			
			backup_police_alley_counter++
			
			if backup_police_alley_counter >= count_of(backup_police_alley)
				backup_police_alley_counter = 0
			endif 
			
			exit
			
		endif
	
	endfor 
	
	for i = 0 to backup_police_alley_counter - 1
	
		if not does_entity_exist(backup_police_alley[i].ped)
		
			setup_enemy(backup_police_alley[i])
			setup_relationship_enemy(backup_police_alley[i].ped, true)
			backup_police_alley_shootout_status[i] = leave_car_or_fight
				
			backup_police_alley_counter++
			
			if backup_police_alley_counter >= count_of(backup_police_alley)
				backup_police_alley_counter = 0
			endif 
			
			exit
			
		endif
	
	endfor 

endproc 

proc backup_police_alley_spawn_system()

	if player_in_area
			
		switch backup_police_alley_spawn_system_status 
		
			case 0

				if get_total_number_of_backup_police_dead(backup_police_alley) >= 2
					create_backup_police_alley()
					create_backup_police_alley()
					backup_police_alley_spawn_system_status++
				endif 
				
			break 
			
			case 1
			
				if get_total_number_of_backup_police_dead(backup_police_alley) >= 1
					create_backup_police_alley()
					backup_police_alley_spawn_system_status++
				endif 
			
			break 
			
			case 2
			
				if get_total_number_of_backup_police_dead(backup_police_alley) >= 2
					create_backup_police_alley()
					backup_police_alley_spawn_system_status++
				endif 
			
			break 
			
			case 3
			
				if get_total_number_of_backup_police_dead(backup_police_alley) >= 3
					create_backup_police_alley()
					create_backup_police_alley()
					backup_police_alley_spawn_system_status = 0
				endif 
			
			break 
			
		endswitch 
		
	endif 

endproc 

proc setup_buddy(ped_structure &mission_buddy)

	clear_area(mission_buddy.pos, 4.0, true)

	mission_buddy.ped = create_ped(pedtype_mission, mission_buddy.model, mission_buddy.pos, mission_buddy.heading)
	set_entity_health(mission_buddy.ped, mission_buddy.health)
	set_ped_dies_when_injured(mission_buddy.ped, false)
	set_ped_can_be_targetted(mission_buddy.ped, false)
	set_ped_suffers_critical_hits(mission_buddy.ped, false)
	set_ped_can_evasive_dive(mission_buddy.ped, false)
	set_entity_is_target_priority(mission_buddy.ped, false)
	set_ped_keep_task(mission_buddy.ped, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_buddy.ped, PCF_DisableHurt, true)
	set_ped_can_ragdoll(mission_buddy.ped, false)
	
	mission_buddy.blip = CREATE_BLIP_FOR_PED(mission_buddy.ped, false)
	set_blip_display(mission_buddy.blip, DISPLAY_BLIP) 
	
	give_weapon_to_ped(mission_buddy.ped, mission_buddy.weapon, infinite_ammo, false)
	set_ped_seeing_range(mission_buddy.ped, 250.00)
	set_ped_hearing_range(mission_buddy.ped, 250.00)
	set_ped_id_range(mission_buddy.ped, 250.00)
	
	setup_relationship_contact(mission_buddy.ped, true)
	//set_ped_combat_movement(

endproc 

proc setup_buddy_attributes(ped_index mission_ped, bool block_interaction_with_the_cops = false)
	
	set_ped_dies_when_injured(mission_ped, false)
	set_ped_can_be_targetted(mission_ped, false)
	set_ped_suffers_critical_hits(mission_ped, false)
	set_ped_can_evasive_dive(mission_ped, false)
	set_entity_is_target_priority(mission_ped, false)
	set_ped_can_ragdoll(mission_ped, false)
	
	set_ped_seeing_range(mission_ped, 250.00)
	set_ped_hearing_range(mission_ped, 250.00)
	set_ped_id_range(mission_ped, 250.00)
	
	SET_PED_CONFIG_FLAG(mission_ped, PCF_KeepRelationshipGroupAfterCleanUp, true)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_WillFlyThroughWindscreen, false) 
	SET_PED_CONFIG_FLAG(mission_ped, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_DisableExplosionReactions, true)
	SET_PED_CONFIG_FLAG(mission_ped, PCF_DisableHurt, true)
	
	if block_interaction_with_the_cops
		SET_PED_CONFIG_FLAG(mission_ped, PCF_OnlyAttackLawIfPlayerIsWanted, true)
		SET_PED_CONFIG_FLAG(mission_ped, PCF_CannotBeTargeted, true)
	endif 
	
endproc 

func bool is_coord_in_area_2d(vector test_coord, vector top_left, vector bottom_right)
				
	vector tl
	vector br

	if top_left.x < bottom_right.x	
		tl.x = top_left.x
		br.x = bottom_right.x
	else
		tl.x = bottom_right.x
		br.x = top_left.x
	endif
	
	if top_left.y < bottom_right.y	
		tl.y = top_left.y
		br.y = bottom_right.y	
	else
		tl.y = bottom_right.y
		br.y = top_left.y
	endif

	if (test_coord.x > tl.x) and (test_coord.x < br.x)
		if (test_coord.y > tl.y) and (test_coord.y < br.y)
			return true
		endif 
	endif 

	return false 
endfunc

func bool has_ped_been_harmed(ped_index &mission_ped, int &original_health, bool ped_created)

	int current_health
	
	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			
			current_health = GET_ENTITY_HEALTH(mission_ped)
			
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_ped , player_ped_id())		
			or (current_health < original_health) 
			or is_ped_responding_to_event(mission_ped, EVENT_DAMAGE)	
				
				return true
			
			endif 
			
		else 
		
			return true
		
		endif
		
	else 
		
		if ped_created
			return true
		endif 
	
	endif 
	
	return false

endfunc 

func bool has_peds_been_harmed(ped_structure &ped_struct[])

	int i = 0

	for i = 0 to (count_of(ped_struct) - 1)
		
		if has_ped_been_harmed(ped_struct[i].ped, ped_struct[i].health, ped_struct[i].created)
			return true 
		endif 
		
	endfor 
	
	return false
	
endfunc 


func bool has_vehicle_been_harmed(vehicle_index &mission_vehicle, int &original_car_health)

	int vehicle_health
	
	if DOES_ENTITY_EXIST(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
			
			vehicle_health = GET_ENTITY_HEALTH(mission_vehicle)
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < original_car_health
			or get_vehicle_engine_health(mission_vehicle) < original_car_health
			or /*HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED*/ HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_vehicle, player_ped_id())
			or vehicle_health < original_car_health
			
				return true
			
			endif 
		
		else 
		
			return true 
			
		endif 
	
	endif 
	
	return false
		
endfunc

func bool has_ped_got_no_ammo_in_weapon(weapon_type miss_weapon)

	if has_ped_got_weapon(player_ped_id(), miss_weapon)
		if get_ammo_in_ped_weapon(player_ped_id(), miss_weapon) < 1
			return true
		endif 
		
//		printint(get_ammo_in_ped_weapon(player_ped_id(), miss_weapon))
//		printnl()
		
	else 
		return true 
	endif 
	
	return false 
	
endfunc 

func bool has_vehicles_been_harmed(vehicle_struct &veh_struct[])

	int i = 0

	for i = 0 to (count_of(veh_struct) - 1)
		
		if has_vehicle_been_harmed(veh_struct[i].veh, veh_struct[i].health)
			return true 
		endif 
		
	endfor 
	
	return false
endfunc

func bool is_vehicle_stuck_every_check(vehicle_index &vehicle)
	
	if DOES_ENTITY_EXIST(vehicle)

		if is_vehicle_driveable(vehicle)
			
			if is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_JAMMED, JAMMED_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			or is_vehicle_stuck_timer_up(vehicle, VEH_STUCK_ON_SIDE, SIDE_TIME)
			
				return true
			endif 
		endif 
	endif 
	
	return false
endfunc 

proc setup_ped_proofs(ped_index &this_ped)
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)
			set_entity_proofs(this_ped, true, true, true, true, true)
		endif 
	endif 
	
endproc 

proc deactivate_ped_proofs(ped_index &this_ped)

	int ped_health
	
	if does_entity_exist(this_ped)
		if not is_ped_injured(this_ped)

			ped_health = get_entity_health(this_ped)

			if ped_health < 110
				set_entity_health(this_ped, 120)
			else 
				set_entity_health(this_ped, ped_health)
			endif 
			
			set_entity_proofs(this_ped, false, false, false, false, false)
		endif 
	endif 

endproc


proc setup_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			
			if get_entity_health(this_vehicle) < 500
				set_entity_health(this_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(this_vehicle) < 500
				set_vehicle_engine_health(this_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(this_vehicle) < 500.00
				set_vehicle_petrol_tank_health(this_vehicle, 500.00)
			endif 
				
			set_entity_proofs(this_vehicle, true, true, true, true, true)
		endif 
	endif 
	
endproc

proc restore_vehicle_health(vehicle_index &mission_vehicle)
								
	if does_entity_exist(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)

			if get_entity_health(mission_vehicle) < 500
				set_entity_health(mission_vehicle, 500)
			endif  
			
			if get_vehicle_engine_health(mission_vehicle) < 500
				set_vehicle_engine_health(mission_vehicle, 500.00)
			endif 
			
			if get_vehicle_petrol_tank_health(mission_vehicle) < 500.00
				set_vehicle_petrol_tank_health(mission_vehicle, 500.00)
			endif 
			
		endif 
	endif 
	
endproc

proc deactivate_vehicle_proofs(vehicle_index &this_vehicle)
	
	if does_entity_exist(this_vehicle)
		if is_vehicle_driveable(this_vehicle)
			set_entity_proofs(this_vehicle, false, false, false, false, false)
			restore_vehicle_health(this_vehicle)
		endif 
	endif 
	
endproc


proc setup_mission_ped_and_vehicle_proofs(ped_index &mission_ped, vehicle_index &mission_car)
									
	setup_ped_proofs(mission_ped) 
	
	setup_vehicle_proofs(mission_car)
	
endproc 

proc restore_mission_ped_and_vehicle_proofs(ped_index &mission_ped, vehicle_index &mission_vehicle)
	
	deactivate_vehicle_proofs(mission_vehicle)
	
	deactivate_ped_proofs(mission_ped)

endproc


/// PURPOSE:
///    Sorts the triggered label array such that all the empty elements are at the end.
PROC REMOVE_LABEL_ARRAY_SPACES()
    INT k = 0
 	REPEAT (COUNT_OF(i_triggered_text_hashes) - 1) k
	    IF i_triggered_text_hashes[k] = 0
	    	IF i_triggered_text_hashes[k+1] != 0
	        	i_triggered_text_hashes[k] = i_triggered_text_hashes[k+1]
	            i_triggered_text_hashes[k+1] = 0
	        ENDIF
	    ENDIF
    ENDREPEAT
ENDPROC

/// PURPOSE:
///    Finds the array index of a particular triggered label, or -1 if the label hasn't been added.
FUNC INT GET_LABEL_INDEX(INT i_label_hash)
    INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
		IF i_triggered_text_hashes[k] = 0 //We've reached the end of the filled section of the array, no need to continue.
        	RETURN -1
        ELIF i_triggered_text_hashes[k] = i_label_hash
        	RETURN k
        ENDIF
    ENDREPEAT
      
    RETURN -1
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given label has been triggered.  
FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING str_label)
	RETURN (GET_LABEL_INDEX(GET_HASH_KEY(str_label)) != -1)
ENDFUNC

/// PURPOSE:
///    Adds/removes a text label to/from the list of labels that have been triggered.
PROC SET_LABEL_AS_TRIGGERED(STRING str_label, BOOL b_trigger)
	
	INT i_hash = GET_HASH_KEY(str_label)
	INT k = 0

	IF b_trigger
	    BOOL b_added = FALSE
	    
	    WHILE NOT b_added AND k < COUNT_OF(i_triggered_text_hashes)
	    	
			IF i_triggered_text_hashes[k] = i_hash //The label is already in the array, don't add it again.
	        	b_added = TRUE 
	        ELIF i_triggered_text_hashes[k] = 0
	        	i_triggered_text_hashes[k] = i_hash
	            b_added = TRUE
	        ENDIF
	          
	        k++
	    ENDWHILE

	    #IF IS_DEBUG_BUILD
	    	IF NOT b_added
	        	SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Failed to add label, array is full.")
	        ENDIF
	    #ENDIF
	ELSE
	    
		INT i_index = GET_LABEL_INDEX(i_hash)
	    IF i_index != -1
			i_triggered_text_hashes[i_index] = 0
	        REMOVE_LABEL_ARRAY_SPACES()
	    ENDIF
		
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT k = 0
	REPEAT COUNT_OF(i_triggered_text_hashes) k
   		i_triggered_text_hashes[k] = 0
	ENDREPEAT
ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR v_rot)
	RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

//proc clear_players_task_on_control_input()//script_task_name task_name)
//
//	//if get_script_task_status(player_ped_id(), task_name) = performing_task
//	if is_entity_playing_anim(player_ped_id(), "missfbi4", "REACT_Explosion")
//		if GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(), "missfbi4", "REACT_Explosion") > 0.7
//			
//			int left_stick_x
//			int left_stick_y
//			int right_stick_x
//			int right_stick_y
//			int stick_dead_zone = 28
//		
//			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)
//
//			IF NOT IS_LOOK_INVERTED()
//				right_stick_y *= -1
//			ENDIF
//			
//			// invert the vertical
//			IF (left_stick_y > STICK_DEAD_ZONE)
//			OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
//			or (left_stick_x > STICK_DEAD_ZONE)
//			OR (left_stick_x < (STICK_DEAD_ZONE * -1))
//			//or is_control_pressed(player_control, input_sprint) 
//			
//				clear_ped_tasks(player_ped_id())
//				
//			endif 
//			
//		endif 
//		
//	endif 
//
//endproc 

proc clear_players_task_on_control_input(script_task_name script_task = script_task_perform_sequence, bool sticks_only_input = false)//script_task_name task_name)

	if get_script_task_status(player_ped_id(), script_task) = performing_task
				
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28
	
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

		IF NOT IS_LOOK_INVERTED()
			right_stick_y *= -1
		ENDIF
		
		if sticks_only_input
		
			// invert the vertical
			IF (left_stick_y > STICK_DEAD_ZONE)
			OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
			or (left_stick_x > STICK_DEAD_ZONE)
			OR (left_stick_x < (STICK_DEAD_ZONE * -1))
			
				clear_ped_tasks(player_ped_id())
				
			endif 
			
		else 
		
			IF (left_stick_y > STICK_DEAD_ZONE)
			OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
			or (left_stick_x > STICK_DEAD_ZONE)
			OR (left_stick_x < (STICK_DEAD_ZONE * -1))
			or is_control_pressed(player_control, input_sprint) 
			or is_control_pressed(player_control, INPUT_JUMP)
			or is_control_pressed(player_control, INPUT_ENTER)
			or is_control_pressed(player_control, INPUT_ATTACK)
			or is_control_pressed(player_control, INPUT_AIM)
			
				clear_ped_tasks(player_ped_id())
				
			endif 
			
		endif 
		
	endif 

endproc 

proc repostion_players_last_vehicle(vehicle_index &mission_veh, vector area_pos_check, vector area_dimensions, vector veh_pos, float veh_heading)
	
	mission_veh = get_players_last_vehicle()

	if does_entity_exist(mission_veh)
		if is_vehicle_driveable(mission_veh)
			if not is_big_vehicle(mission_veh)
				if not (get_entity_model(mission_veh) = taxi)
					if is_entity_at_coord(mission_veh, area_pos_check, area_dimensions)
					
						//delete anything attached to the tow truck
						if (get_entity_model(players_last_vehicle) = towtruck)
					
							entity_index attached_vehicle
							attached_vehicle = GET_ENTITY_ATTACHED_TO_TOW_TRUCK(players_last_vehicle)
							VEHICLE_INDEX thevehicle
							thevehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(attached_vehicle)
							
							if does_entity_exist(attached_vehicle)
								DETACH_VEHICLE_FROM_TOW_TRUCK(players_last_vehicle, thevehicle)
								delete_entity(attached_vehicle)
							endif 
							
						endif 
					
						if not is_entity_a_mission_entity(mission_veh)
							set_entity_as_mission_entity(mission_veh)
						endif 
						
						if is_vehicle_siren_on(players_last_vehicle)
							set_vehicle_siren(players_last_vehicle, false)
						endif 
						
						if not is_vehicle_in_players_garage(mission_veh, get_current_player_ped_enum(), false)
						
							clear_area(veh_pos, 10.0, true)
							set_entity_coords(mission_veh, veh_pos)  
							set_entity_heading(mission_veh, veh_heading)
							set_vehicle_on_ground_properly(mission_veh)
							
						endif 
					endif 
				endif 
			endif 
		endif
	endif 
	
endproc 


PROC EQUIP_BEST_PLAYER_WEAPON(ped_index mission_ped, BOOL bForceIntoHand = true)
  
  IF NOT IS_PED_INJURED(mission_ped)
       
		WEAPON_TYPE bestWeapon = GET_BEST_PED_WEAPON(mission_ped)
        
		IF bestWeapon != WEAPONTYPE_UNARMED
             SET_CURRENT_PED_WEAPON(mission_ped, bestWeapon, bForceIntoHand)
        ENDIF
  ENDIF
  
ENDPROC

proc setup_player_fail_weapon(ped_index mission_ped)

	weapon_type players_weapon

	players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
	
	if players_weapon != WEAPONTYPE_INVALID
	and players_weapon != WEAPONTYPE_UNARMED 
		set_current_ped_weapon(mission_ped, players_weapon, true)
	endif 
	
endproc 
	

proc setup_weapon_for_ped(ped_index mission_ped, weapon_type ped_weapon, int num_of_bullets, bool force_weapon_into_hand = true, bool equip_weapon = true, bool equipe_best_weapon = true)
	
	IF not HAS_PED_GOT_FIREARM(mission_ped, false)
		
		give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
		exit
	
	else 
	
		weapon_type players_weapon

		if equipe_best_weapon
		
			EQUIP_BEST_PLAYER_WEAPON(mission_ped, force_weapon_into_hand)
			get_current_ped_weapon(mission_ped, players_weapon)
			
		else 
		
			players_weapon = Get_Fail_Weapon(enum_to_int(get_player_ped_enum(mission_ped)))
				
			if players_weapon = WEAPONTYPE_INVALID
			or players_weapon = WEAPONTYPE_UNARMED 
				
				if not HAS_PED_GOT_WEAPON(mission_ped, ped_weapon)
					give_weapon_to_ped(mission_ped, ped_weapon, num_of_bullets, force_weapon_into_hand, equip_weapon)
					exit
				else 
					players_weapon = ped_weapon 
				endif 

			endif 

		endif 
		
		if players_weapon = WEAPONTYPE_GRENADELAUNCHER
		or players_weapon = WEAPONTYPE_RPG
			
			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < 5
				set_ped_ammo(mission_ped, players_weapon, 5)
			endif 
			
		else 

			if get_ammo_in_ped_weapon(mission_ped, players_weapon) < num_of_bullets
				set_ped_ammo(mission_ped, players_weapon, num_of_bullets)
			endif 
			
		endif 
		
		set_current_ped_weapon(mission_ped, players_weapon, force_weapon_into_hand) 

	endif 
	

endproc 

/// PURPOSE:
///    Sets the player ped to be wearing the mask that was bought for them in FBI 4 Prep 4
/// PARAMS:
///    mMissionPed - the ped we are setting the mask for
PROC SET_PLAYER_PED_MASK(PED_INDEX mMissionPed)
	SET_STORED_FBI4_MASK(mMissionPed)
ENDPROC

/// PURPOSE:
///    Checks if the player is currently hanging around where the given ped is trying to get to. This is useful for checking if peds 
///    playing synced scenes need to break into AI due to the player getting in the way.
/// PARAMS:
///    ped - The ped moving towards the destination.
///    v_destination - The destination
///    f_max_dist_before_check - The ped must be within this distance of the destination before we start checking what the player is doing.
///    b_player_must_be_in_cover - If TRUE then the player must be in cover for them to be considered in the way.  
FUNC BOOL IS_PLAYER_STEALING_PEDS_DESTINATION(PED_INDEX ped, VECTOR destination, FLOAT distance_check, BOOL player_must_be_in_cover = true)
	
	IF NOT IS_PED_INJURED(ped)
		
		VECTOR player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR ped_pos = GET_ENTITY_COORDS(ped)

		FLOAT dist_from_ped_to_dest = VDIST(ped_pos, destination)
		FLOAT dist_from_player_to_dest = VDIST(player_pos, destination)

		IF dist_from_ped_to_dest < distance_check
		
			printstring("test 0")
			printnl()
		
			IF dist_from_player_to_dest < dist_from_ped_to_dest
			and is_entity_at_coord(player_ped_id(), destination, <<1.0, 1.0, 2.0>>) 
				
				printstring("test 1")
				printnl()
				
				IF player_must_be_in_cover
					IF IS_PED_IN_COVER(PLAYER_PED_ID())
					//or is_ped_going_into_cover(player_ped())
						printstring("test 2")
						printnl()
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

proc check_if_ped_killed_by_specific_player_character(ped_index &misison_ped, bool &damaged_flag_to_set, enumCharacterList char)
					
	if does_entity_exist(misison_ped)
		if is_ped_injured(misison_ped)
			if has_entity_been_damaged_by_entity(misison_ped, player_ped_id())
				if get_current_player_ped_enum() = char 
					damaged_flag_to_set = true
				endif 
			endif
		endif 
	endif 
	
endproc 

///PURPOSE:
///    checks if a sniper ped is killed by the player playing as trevor. Will set the flag to true else
///    reset it to false. This stops the player killing the ped the ped with michael and then switching
///    to trevor and getting the lines of dilaogue. 
proc check_if_sniper_killed_by_trevor(ped_index &mission_ped)

	if does_entity_exist(mission_ped) //ped won't exist next frame as ped_structure_are_all_enemies_dead will have cleaned the ped up
		if is_ped_injured(mission_ped)
			if has_entity_been_damaged_by_entity(mission_ped, player_ped_id())
				if get_current_player_ped_enum() = char_trevor 
					sniper_killed_by_trevor = true 
				else 
					sniper_killed_by_trevor = false
				endif 
			endif 
		endif 
	endif 

endproc 

//func bool is_one_sniper_killed_by_trevor()
//
//	if police_man_8_killed_by_trevor
//		
//
//endproc 

//PURPOSE: repositions the players last vehicle if it exists via calling GET_MISSION_START_VEHICLE_INDEX()
//		   and sets the vehicle as a mission entity so that clear area will not clear it. Remember to 

proc reposition_players_last_vehicle(vector car_pos, float car_heading, bool switch_vehicle_engine_off = true)

	vehicle_index players_last_car 
	
	players_last_car = GET_PLAYERS_LAST_VEHICLE()
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
		
			SET_ENTITY_AS_MISSION_ENTITY(players_last_car, true, true)
			
			clear_area(car_pos, 5.0, true)
			set_entity_coords(players_last_car, car_pos)
			set_entity_heading(players_last_car, car_heading)
			set_vehicle_on_ground_properly(players_last_car)
			set_entity_as_mission_entity(players_last_car)
			
			if switch_vehicle_engine_off
				set_vehicle_engine_on(players_last_car, true, true)
			endif 
			
		endif 
	endif 
	
endproc

func bool has_ped_been_found_by_sniper_scope(ped_index &miss_ped)

	IF NOT IS_PED_INJURED(miss_ped)

		IF IS_ENTITY_ON_SCREEN(miss_ped)
			
			IF GET_GAMEPLAY_CAM_FOV() < 15.0 //10
				
				FLOAT f_x = 0.0
				FLOAT f_y = 0.0
				GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(miss_ped), f_x, f_y)
			
				IF ABSF(f_x - 0.5) < 0.1 
					
					IF ABSF(f_y - 0.5) < 0.1 
						
						return true 
						
					ENDIF
				
				ENDIF
			
			ENDIF
			
		endif 
		
	endif 
	
	return false 
	
endfunc 

FUNC BOOL HAS_PED_BEEN_damaged_by_sniper(PED_INDEX ped_damaged, PED_INDEX ped_with_sniper)
	
	IF NOT IS_ENTITY_DEAD(ped_damaged)
		IF NOT IS_ENTITY_DEAD(ped_with_sniper)
			WEAPON_TYPE wPed
			IF GET_CURRENT_PED_WEAPON(ped_with_sniper, wPed)
				
				IF wPed = WEAPONTYPE_HEAVYSNIPER
				or wPed = WEAPONTYPE_SNIPERRIFLE
				
					if ((HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(ped_damaged, WEAPONTYPE_HEAVYSNIPER) or HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(ped_damaged, WEAPONTYPE_SNIPERRIFLE))
					and HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_damaged, ped_with_sniper))
						RETURN TRUE
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


func int get_closest_pos_to_player(vector &pos[])
	
	float smallest_distance_between_vectors
	float current_distance_between_vectors
	int current_node = 0
	int i
	
	
	for i = 0 to count_of(pos) - 1
	
		current_distance_between_vectors = get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), pos[i])
	
		if i = 0
		
			smallest_distance_between_vectors = current_distance_between_vectors
			current_node = i
			
		else 
		
			if current_distance_between_vectors < smallest_distance_between_vectors
				smallest_distance_between_vectors = current_distance_between_vectors
				current_node = i
			endif 
			
		endif 
		
	endfor 
	
	return current_node

endfunc 

func int get_closest_ped_structure_ped_to_ped(ped_index mission_ped, ped_structure &target_ped[])

	float smallest_distance_between_vectors
	float current_distance_between_vectors
	int current_node = 0
	int i
	
	if not is_ped_injured(mission_ped)
	
		for i = 0 to count_of(target_ped) - 1
		
			if not is_ped_injured(target_ped[i].ped)
		
				current_distance_between_vectors = get_distance_between_coords(GET_ENTITY_COORDS(mission_ped), get_entity_coords(target_ped[i].ped))
			
				if i = 0
				
					smallest_distance_between_vectors = current_distance_between_vectors
					current_node = i
					
				else 
				
					if current_distance_between_vectors < smallest_distance_between_vectors
						smallest_distance_between_vectors = current_distance_between_vectors
						current_node = i
					endif 
					
				endif 
				
			endif 
			
		endfor 
		
	endif  
	
	return current_node
	
endfunc 

func int get_total_number_of_cops_killed_stat()

	int player_0
	int player_1
	int player_2
	
	STAT_GET_INT(SP0_KILLS_COP, player_0) 
	STAT_GET_INT(SP1_KILLS_COP, player_1) 
	STAT_GET_INT(SP2_KILLS_COP, player_2) 

	return (player_0 + player_1 + player_2)

endfunc 

func int get_total_number_of_swat_killed_stat()

	int player_0
	int player_1
	int player_2
	
	STAT_GET_INT(SP0_KILLS_swat, player_0) 
	STAT_GET_INT(SP1_KILLS_swat, player_1) 
	STAT_GET_INT(SP2_KILLS_swat, player_2) 

	return (player_0 + player_1 + player_2)

endfunc

func int get_total_number_of_cop_cars_exploded_stat()

	int player_0
	int player_1
	int player_2

    STAT_GET_INT(SP0_CARS_COPS_EXPLODED, player_0) 
    STAT_GET_INT(SP1_CARS_COPS_EXPLODED, player_1) 
    STAT_GET_INT(SP2_CARS_COPS_EXPLODED, player_2) 
	
	return (player_0 + player_1 + player_2)

endfunc 

func int get_total_number_of_helicopters_exploded_stat()

	int player_0
	int player_1
	int player_2

	STAT_GET_INT(SP0_HELIS_EXPLODED, player_0) 
    STAT_GET_INT(SP1_HELIS_EXPLODED, player_1) 
    STAT_GET_INT(SP2_HELIS_EXPLODED, player_2) 
	
	return (player_0 + player_1 + player_2)
	
endfunc 

func int total_wanted_level_stat_count()

	return (get_total_number_of_cops_killed_stat() + get_total_number_of_swat_killed_stat() + get_total_number_of_cop_cars_exploded_stat() + get_total_number_of_helicopters_exploded_stat())
	
endfunc 

func bool HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(ped_index miss_ped)
	
	if does_entity_exist(miss_ped)
		if not is_ped_injured(miss_ped)
			if HAVE_ALL_STREAMING_REQUESTS_COMPLETED(miss_ped)
				return true 
			endif 
		endif 
	endif 
	
	return false 

endfunc 

proc set_players_last_vehicle_to_vehicle_gen(vector car_pos, float car_heading)

	vehicle_index players_last_car 
	
	players_last_car = GET_PLAYERS_LAST_VEHICLE()
	
	if does_entity_exist(players_last_car)
		if is_vehicle_driveable(players_last_car)
		
			SET_MISSION_VEHICLE_GEN_VEHICLE(players_last_car, car_pos, car_heading) 
			
		endif 
	endif 
	
endproc 


//****************************************END OF LAWRENCE SDK****************************************


//****************************************WIDGETS****************************************

#IF IS_DEBUG_BUILD

bool widget_reset_cutscene = false
bool widget_create_object = false
bool widget_modify = false
bool widget_vehicle_camera_active = false
bool widget_reset_deformation = false
bool widget_apply_deformation = false
bool widget_create_tow_truck = false
bool widget_show_police_car_info = false
bool widget_trigger_pfx = false 

float widget_temp_width
float widget_temp_height
float widget_temp_depth
float widget_angled_length
float widget_anim_speed = 1.0
float widget_heading
float widget_fov = 45.00
float widget_vehicle_damage
float widget_vehicle_deformation
float widget_recording_time

vector widget_offset_bl
vector widget_offset_br
vector widget_bl
vector widget_br
vector widget_object_pos
vector widget_object_rot
vector widget_object_pos_2
vector widget_object_rot_2
vector widget_cam_attach_offset
vector widget_cam_point_offset
vector widget_veh_offset

int widget_total_number_of_cops_dead


camera_index widget_camera


proc locate_widget()

	vector widget_player_pos
			
	widget_player_pos = GET_ENTITY_COORDS(player_ped_id())
	IS_ENTITY_AT_COORD(player_ped_id(), widget_player_pos, <<widget_temp_width, widget_temp_depth, widget_temp_height>>, false, true)		

endproc

proc angled_area_locate_widget()

	widget_bl = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_bl) 
	widget_br = get_offset_from_entity_in_world_coords(player_ped_id(), widget_offset_br)

	IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), widget_bl, widget_br, widget_angled_length, false, true)	
	
	if widget_offset_br.z > 0.0
		SET_ENTITY_HEADING(player_ped_id(), widget_heading)
	endif 

endproc

proc angled_area_locate_widget_entity(entity_index mission_entity)

	if does_entity_exist(mission_entity)

		widget_bl = get_offset_from_entity_in_world_coords(mission_entity, widget_offset_bl) 
		widget_br = get_offset_from_entity_in_world_coords(mission_entity, widget_offset_br)

		IS_ENTITY_IN_ANGLED_AREA(mission_entity, widget_bl, widget_br, widget_angled_length, false, TRUE)	
		
//		if widget_offset_br.z > 0.0
//			SET_ENTITY_HEADING(player_ped_id(), widget_heading)
//		endif 
		
	endif 

endproc

proc draw_marker_offset_from_entity_widget(entity_index mission_entity)

	if does_entity_exist(mission_entity)
		if not is_entity_dead(mission_entity)

			draw_marker(MARKER_CYLINDER, get_offset_from_entity_in_world_coords(mission_entity, widget_veh_offset) , <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.5>>, 255, 0, 128, 178)
		
		endif 

	endif 
	
endproc 

proc attach_object_to_ped_and_move(ped_index mission_ped, object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if DOES_ENTITY_EXIST(mission_object)
				ATTACH_ENTITY_TO_ENTITY(mission_object, mission_ped, GET_PED_BONE_INDEX(mission_ped, BONETAG_PH_L_HAND), widget_object_pos, widget_object_rot)
			endif 
		endif 
	endif
	
endproc 

proc set_the_ped_anim_speed(ped_index mission_ped, string dict_name, string anim_name, float anim_speed)

	if DOES_ENTITY_EXIST(mission_ped)
		if not is_ped_injured(mission_ped)
			if IS_ENTITY_PLAYING_ANIM(mission_ped, dict_name, anim_name)
				SET_ENTITY_ANIM_SPEED(mission_ped, dict_name, anim_name, anim_speed)
			endif 
		endif 
	endif 
	
endproc

proc create_and_move_object(object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_object)
		SET_ENTITY_COORDS_NO_OFFSET(mission_object, widget_object_pos)
		SET_ENTITY_ROTATION(mission_object, widget_object_rot)  
	endif 
		
endproc 

proc create_and_move_object_2(object_index &mission_object)

	if DOES_ENTITY_EXIST(mission_object)
		SET_ENTITY_COORDS_NO_OFFSET(mission_object, widget_object_pos_2)
		SET_ENTITY_ROTATION(mission_object, widget_object_rot_2)  
	endif 
		
endproc 

proc attach_object_to_object_and_move(object_index &parent_object, object_index &mission_object)

	if DOES_ENTITY_EXIST(parent_object)
		if DOES_ENTITY_EXIST(mission_object)
			ATTACH_ENTITY_TO_ENTITY(mission_object, parent_object, -1, widget_object_pos_2, widget_object_rot_2)
		endif 
	endif 
	
endproc 

proc create_and_move_pickup_widget(pickup_index &widget_mission_pickup, pickup_type pick_type, vector &pos, vector &rot)

	if widget_create_object
		if not widget_modify
			widget_mission_pickup = create_pickup_rotate(pick_type, pos, rot, true)
			
			widget_object_pos.x = pos.x 
			widget_object_pos.y = pos.y
			widget_object_pos.z = pos.z 
			widget_object_rot.x = rot.x
			widget_object_rot.y = rot.y
			widget_object_rot.z = rot.z
			
			widget_modify = true 
		else 
			
			if lk_timer(original_time, 500)
				if does_pickup_exist(widget_mission_pickup)
					remove_pickup(widget_mission_pickup)
				else
					widget_mission_pickup = create_pickup_rotate(pick_type, widget_object_pos, widget_object_rot, true)
				endif
				
				original_time = get_game_timer()
			endif 
		endif 
	endif 
	
endproc 

			
proc camera_attached_to_vehicle_widget(vehicle_index &mission_veh)

	if widget_vehicle_camera_active
		if not does_cam_exist(widget_camera)

			widget_camera = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(widget_camera, mission_veh, widget_cam_attach_offset)
			POINT_CAM_AT_ENTITY(widget_camera, mission_veh, widget_cam_point_offset)

			set_cam_active(widget_camera, true)
								
			render_script_cams(true, false)
			
		else 
		
			ATTACH_CAM_TO_ENTITY(widget_camera, mission_veh, widget_cam_attach_offset)
			
			POINT_CAM_AT_ENTITY(widget_camera, mission_veh, widget_cam_point_offset)
			
			set_cam_fov(widget_camera, widget_fov)
			
		endif 

	endif 

endproc 

proc vehicle_damage_widget(vehicle_index mission_vehicle)

	if does_entity_exist(mission_vehicle)
		if is_vehicle_driveable(mission_vehicle)
		
			if widget_reset_deformation
				set_vehicle_fixed(mission_vehicle)
				widget_reset_deformation = false 
				widget_apply_deformation = false
			endif 
			
			if widget_apply_deformation
				SET_VEHICLE_DAMAGE(mission_vehicle, widget_veh_offset, widget_vehicle_damage, widget_vehicle_deformation, TRUE)
				widget_apply_deformation = false
			endif 
			
		endif 
	endif 
	
endproc 

proc tow_truck_recording_wdiget()
			
	if widget_create_tow_truck 
	
		clear_ped_tasks_immediately(Player_ped_id())
	
		if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
			delete_ped(selector_ped.pedID[selector_ped_franklin])
		endif 
		
		if does_entity_exist(tow_truck.veh)
			delete_vehicle(tow_truck.veh)
		endif 
		
		tow_truck.veh = create_vehicle(tow_truck.model, <<1377.14, -2076.2, 52.0>>, 40.038132)
		set_vehicle_colours(tow_truck.veh, 0, 0)
		set_vehicle_on_ground_properly(tow_truck.veh)
		set_ped_into_vehicle(player_ped_id(), tow_truck.veh)
		
		widget_create_tow_truck = false
		
	endif 

endproc 

proc ptfx_explosion_widget()

	if widget_trigger_pfx

		if not does_entity_exist(army_truck.veh)
		
			request_model(army_truck.model)
		
			if has_model_loaded(army_truck.model)
			
				if does_entity_exist(rubbish_truck.veh)
					clear_ped_tasks_immediately(player_ped_id())
					set_ped_into_vehicle(player_ped_id(), rubbish_truck.veh)
					set_entity_coords(rubbish_truck.veh, <<907.3049, -2344.8853, 29.5104>>)
				endif 
			
				army_truck.veh = create_vehicle(army_truck.model, <<880.0, -2357.4, 30.150>>, 175.2936)
				set_vehicle_has_strong_axles(army_truck.veh, true)
				SET_ENTITY_PROOFS(army_truck.veh, true, true, true, true, true)
				set_entity_coords_no_offset(army_truck.veh, <<890.495, -2365.6, 30.5039>>)
				set_entity_rotation(army_truck.veh, <<0.0, 91.8056, 173.211>>)
				FREEZE_ENTITY_POSITION(army_truck.veh, true)
				
			endif 
			
		else 

			ADD_EXPLOSION_WITH_USER_VFX(<<889.7, -2362.58, 30.24>>, exp_tag_grenade, get_hash_key("EXP_VFXTAG_FBI4_TRUCK_DOORS"), 0.5)
	
			widget_trigger_pfx = false
			
		endif 
		
	endif 
	
endproc 


proc load_lk_widgets()

	fbi_4_widget_group = start_widget_group("FBI4")

		#IF IS_DEBUG_BUILD
		start_widget_group("locate widget")
			add_widget_float_slider("width", widget_temp_width, -3000.0, 3000.0, 0.1)
			add_widget_float_slider("depth", widget_temp_depth, -3000.0, 3000.0, 0.1)
			add_widget_float_slider("height", widget_temp_height, -3000.0, 3000.0, 0.1)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD 
		start_widget_group("angled_area")
			add_widget_float_slider("left offset x", widget_offset_bl.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("left offset y", widget_offset_bl.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("left offset z", widget_offset_bl.z, -2000.0, 2000.0, 0.1)
			
			add_widget_float_slider("right offset x", widget_offset_br.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("right offset y", widget_offset_br.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("right offset z", widget_offset_br.z, -2000.0, 2000.0, 0.1)
			
			add_widget_float_slider("angled area length", widget_angled_length, -2000.0, 2000.0, 0.1)
			
			add_widget_float_slider("heading", widget_heading, -2000.0, 2000.0, 0.1)
			
			add_widget_float_read_only("widget_bl x", widget_bl.x)
			add_widget_float_read_only("widget_bl y", widget_bl.y)
			add_widget_float_read_only("widget_bl z", widget_bl.z)
			
			add_widget_float_read_only("widget_br x", widget_br.x)
			add_widget_float_read_only("widget_br y", widget_br.y)
			add_widget_float_read_only("widget_br z", widget_br.z)
			
		stop_widget_group()
		#endif
		
		start_widget_group("draqw marker offset from entity")
			add_widget_float_slider("offset x", widget_veh_offset.x, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("offset y", widget_veh_offset.y, -2000.0, 2000.0, 0.1)
			add_widget_float_slider("offset z", widget_veh_offset.z, -2000.0, 2000.0, 0.1)
		stop_widget_group()
		
		
		#IF IS_DEBUG_BUILD
		start_widget_group("object attach to ped widget")
			add_widget_float_slider("pos offset x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos offset z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot offset x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot offset z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		
		#IF IS_DEBUG_BUILD
		start_widget_group("anim speed widget")
			add_widget_float_slider("anim speed", widget_anim_speed, 0.1, 10.0, 0.01)
		stop_widget_group()
		#endif
		
		
		#IF IS_DEBUG_BUILD
		start_widget_group("reset cutscene widget")
			add_widget_bool("reset cutscene", widget_reset_cutscene)
		stop_widget_group()
		#endif
		
		
		#IF IS_DEBUG_BUILD
		start_widget_group("object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("object pos and rot widget 2")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD
		start_widget_group("attach object to object pos and rot widget")
			add_widget_float_slider("pos x", widget_object_pos_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos y", widget_object_pos_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("pos z", widget_object_pos_2.z, -3000.0, 3000.0, 0.01)
			
			add_widget_float_slider("rot x", widget_object_rot_2.x, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot y", widget_object_rot_2.y, -3000.0, 3000.0, 0.01)
			add_widget_float_slider("rot z", widget_object_rot_2.z, -3000.0, 3000.0, 0.01)
		stop_widget_group()
		#endif
		
		#IF IS_DEBUG_BUILD 
		start_widget_group("create and move PICKUP inside interior")
			ADD_WIDGET_bool("create_object", widget_create_object)
			
			ADD_WIDGET_FLOAT_SLIDER("object pos x", widget_object_pos.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("object pos y", widget_object_pos.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("object pos z", widget_object_pos.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("object rotation x", widget_object_rot.x, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("object rotation y", widget_object_rot.y, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("object rotation z", widget_object_rot.z, 0.0, 360.0, 0.1)
		stop_widget_group()
		#endif 
		
		start_widget_group("camaera attached to vehicle widget")
			add_widget_bool("create camera", widget_vehicle_camera_active)
			
			ADD_WIDGET_FLOAT_SLIDER("pos x", widget_cam_attach_offset.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos y", widget_cam_attach_offset.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("pos z", widget_cam_attach_offset.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("point offset x", widget_cam_point_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("point offset y", widget_cam_point_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("point offset z", widget_cam_point_offset.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("FOV", widget_fov, -3000.0, 3000.0, 0.1)
		stop_widget_group()
		
		start_widget_group("vehicle damage")
			ADD_WIDGET_bool("Reset vehicle deformation", widget_reset_deformation)
			ADD_WIDGET_bool("Apply vehicle deformation", widget_apply_deformation)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle offset x", widget_veh_offset.x, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("vehicle offset y", widget_veh_offset.y, -3000.0, 3000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("vehicle offset z", widget_veh_offset.z, -3000.0, 3000.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle damage", widget_vehicle_damage, 0.0, 2000, 1000)
			ADD_WIDGET_FLOAT_SLIDER("vehicle deformation", widget_vehicle_deformation, 0.0, 2000, 1000)

		stop_widget_group()	

		start_widget_group("recording tow truck")
			ADD_WIDGET_bool("create_tow_truck", widget_create_tow_truck)
		stop_widget_group()	
		
		start_widget_group("switch on police car id")
			ADD_WIDGET_bool("widget_show_police_car_info", widget_show_police_car_info)
		stop_widget_group()	
		
		start_widget_group("trigger explosion with pfx EXP_VFXTAG_FBI4_TRUCK_DOORS")
			ADD_WIDGET_bool("widget_trigger_pfx", widget_trigger_pfx)
		stop_widget_group()	
		
		start_widget_group("status widget tracking")
			add_widget_int_read_only("shootout_master_controler_status", shootout_master_controler_status)
			add_widget_int_read_only("total number of cops dead", widget_total_number_of_cops_dead) 
			add_widget_int_read_only("total_police_men_alive_count", total_police_men_alive_count)
			add_widget_int_read_only("uber_speed_system_status", uber_speed_system_status)
			add_widget_float_read_only("widget_recording_time", widget_recording_time)
		stop_widget_group()	

	stop_widget_group()
	
endproc

#endif



////****************************************VEHICLE FORCE WIDGET VARIABLES****************************************
#IF IS_DEBUG_BUILD

bool reset_vehicle_position = false
bool create_veh = false
bool apply_force_position = false
bool update_vehicle = false
bool original_creation = false
bool output_data = false

int force_status = 0

float force_multiplier = 1.0
float vehicle_rot

vector force_vec_a
vector force_vec_b 
vector force_vec_ba
vector force_offset = <<0.0, 0.0, 0.0>>
vector angular_impulse

vehicle_index current_car

TEXT_WIDGET_ID widget_model_name

model_names vehicle_model


proc load_vehicle_force_widget_data()

	set_current_widget_group(fbi_4_widget_group)
		start_widget_group("vehicle force widget")
			
			add_widget_bool("create_vehicle", create_veh)
			add_widget_bool("reset vehicle position", reset_vehicle_position)
			add_widget_bool("update vehicle model", update_vehicle)
			
			widget_model_name = ADD_TEXT_WIDGET("Vehicle Model")
			SET_CONTENTS_OF_TEXT_WIDGET(widget_model_name, "sultan")
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle pos.x", force_vec_a.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle pos.y", force_vec_a.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle pos.z", force_vec_a.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle rot", vehicle_rot, 0, 360, 0.1)
		
			ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.x", force_vec_b.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.y", force_vec_b.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.z", force_vec_b.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.x", force_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.y", force_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.z", force_offset.z, -3000.0, 3000.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("angular force.x", angular_impulse.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("angular force.y", angular_impulse.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("angular force.z", angular_impulse.z, -3000.0, 3000.0, 0.1)
			
			add_widget_float_slider("force multiplier", force_multiplier, 0.0, 200, 0.1)
			
			add_widget_bool("apply force to vehicle", apply_force_position)
			
			add_widget_bool("Output data to temp_debug.txt", output_data)
			
			add_widget_string("")
			add_widget_string("Data to input into function apply_force_to_car()")
			ADD_WIDGET_FLOAT_SLIDER("vforce.x", force_vec_ba.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vforce.y", force_vec_ba.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vforce.z", force_vec_ba.z, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("voffset.x", force_offset.x, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("voffset.y", force_offset.y, -3000.0, 3000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("voffset.z", force_offset.z, -3000.0, 3000.0, 0.1)
			
		stop_widget_group()
	clear_current_widget_group(fbi_4_widget_group)

endproc
	
proc vehicle_force_system()

	if reset_vehicle_position
		force_status = 0
		create_veh = true
		update_vehicle = false
		reset_vehicle_position = false
		
		STOP_RECORDING_ALL_VEHICLES()
		
		if DOES_ENTITY_EXIST(current_car)
			DELETE_VEHICLE(current_car)
		endif 
		
		clear_area(force_vec_a, 100, true)
	endif 
	
//	if update_vehicle or mission_vehicle_injured(current_car)
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(current_car)
//		DELETE_VEHICLE(current_car)
//		create_veh = true 
//		force_status = 0
//		update_vehicle = false
//	endif 
	
	if output_data
		OPEN_DEBUG_FILE()
		save_string_to_debug_file("vForce = ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.z)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		save_string_to_debug_file("vOffset = ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.z)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
			
		output_data = false
	endif 
	
	
	//check script / draw debug lines and sphers 
	DRAW_MARKER(MARKER_CYLINDER, force_vec_b, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.5>>, 255, 0, 128, 178)



	switch force_status 
	
		case 0
			
			if create_veh
			
				vehicle_model = INT_TO_ENUM(model_names, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(widget_model_name)))
				
				IF NOT IS_MODEL_IN_CDIMAGE(vehicle_model)
					SCRIPT_ASSERT("Vehicle model does not exist!") 
				else 
				
					if not has_model_loaded(vehicle_model)
						request_model(vehicle_model)
					else 
						
						if DOES_ENTITY_EXIST(current_car)
							if is_vehicle_driveable(current_car)
								SET_ENTITY_COORDS(current_car, force_vec_a)
								SET_ENTITY_HEADING(current_car, vehicle_rot)
								force_status++
							endif 
						else 
							if not original_creation
								if not is_ped_injured(player_ped_id())
									force_vec_a = get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 4.0, 2.5>>)  
									force_vec_b = get_offset_from_entity_in_world_coords(player_ped_id(), <<-1.0, 4.0, 0.0>>)  
									original_creation = true 
								endif 
							endif 
								
							current_car = create_vehicle(vehicle_model, force_vec_a, vehicle_rot)
							set_model_as_no_longer_needed(vehicle_model)
							SET_ENTITY_PROOFS(current_car, true, true, true, true, true)
							force_status++
						endif 
						
					endif 
					
				endif 
				
			endif 

		break 
		
		case 1

			if apply_force_position
			
				if START_RECORDING_VEHICLE(current_car, 0, "lkcountry", true)
									
					//get_car_coordinates(current_car, force_vec_a.x, force_vec_a.y, force_vec_a.z)
					
					force_vec_ba = force_vec_b - force_vec_a
					
					normalise_vector(force_vec_ba)
					
					force_vec_ba.x *= force_multiplier
					force_vec_ba.y *= force_multiplier
					force_vec_ba.z *= force_multiplier
					
					//*****TEMP WAIT TILL JACK COMES BACK AND GET HIM TO UPDATE CODE
	//				angular_impulse.x *= 60
	//				angular_impulse.y *= 60
	//				angular_impulse.z *= 60
		
					APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_EXTERNAL_IMPULSE, force_vec_ba, force_offset, 0, false, true, true)  
					//APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_IMPULSE, force_vec_ba, force_offset, 0, false, true, true)  
					APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_ANGULAR_IMPULSE, angular_impulse, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
	
					apply_force_position = false
				
					force_status++
				endif 
			else 
			
				SET_ENTITY_HEADING(current_car, vehicle_rot)
				SET_ENTITY_COORDS_NO_OFFSET(current_car, force_vec_a)
				
			endif 
											
		break 
		
		case 3
		
		
		break 
		
	endswitch 

endproc 

#endif

//**************************************** END VEHICLE FORCE WIDGET ****************************************



//**************************************** LOCAL WIDGET ****************************************

float time_multiplier = 0.05

proc load_local_widgets
	
	#IF IS_DEBUG_BUILD
	set_current_widget_group(fbi_4_widget_group)
		start_widget_group("time multiplier")
				
			ADD_WIDGET_FLOAT_SLIDER("time multiplier", time_multiplier, 0, 100, 0.01)
				
		stop_widget_group()
	clear_current_widget_group(fbi_4_widget_group)
	#endif 

endproc 

proc time_scale_system(float target_time_scale)

	float frame_time

	frame_time = (get_frame_time() * time_multiplier)
	
	
	if current_time_scale < target_time_scale
		
		current_time_scale += frame_time 
		
	else 
	
		current_time_scale = target_time_scale
		
	endif 
		
	
//	printfloat(current_time_scale)
//	printnl()

endproc 

//**************************************** END LOCAL WIDGET ****************************************


//============================== SCOTT PENMAN TOTONTO FIB4 SWITCH CAMS =========================================

ENUM FIB4_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE = 0,
	SWITCH_CAM_START_SPLINE1,
	SWITCH_CAM_PLAYING_SPLINE1,
	SWITCH_CAM_START_SPLINE2,
	SWITCH_CAM_PLAYING_SPLINE2,
	SWITCH_CAM_START_SPLINE3,
	SWITCH_CAM_PLAYING_SPLINE3,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
FIB4_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsTruckToTrevor
SWITCH_CAM_STRUCT scsBinocularsScene
SWITCH_CAM_STRUCT scsTrevorToTruck

#IF IS_DEBUG_BUILD
BOOL bResetTrevorWithBinoculars = FALSE
#ENDIF
VECTOR vTrevorBinocularsPos = <<790.8106, -2330.1296, 61.095>>
FLOAT fTrevorBinocularsHeading = 137.22
			
INT iWooshSoundID = -1

FLOAT fWoosh1StartPhase = 0.402
FLOAT fWoosh1StopPhase = 0.540

FLOAT fWoosh2StartPhase = 0.577
FLOAT fWoosh2StopPhase = 0.808

INT iBinocularsMaskStartNode = 0
INT iBinocularsMaskStopNode = 3

INT iBinocularsJumpCutNode = 4

INT iTrevorBinocularsConvo1Delay = 4250
INT iTrevorBinocularsConvo2Delay = 3500
INT iTrevorBinocularsConvo3Delay = 9250

FLOAT fTruckRecordingJump1SkipTime = 11000.000
FLOAT fTruckRecordingJump2SkipTime = 19000.000

FLOAT fTrevorBinocularAnim2Phase = 0.575

FLOAT fTruckDriveByShakeStartPhase = 0.970
FLOAT fTruckDriveByShakeScale = 0.800

FLOAT fSwitchFX1StartPhase = 0.402
FLOAT fSwitchFX2StartPhase = 0.454
FLOAT fSwitchFX3StartPhase = 0.577
FLOAT fSwitchFX4StartPhase = 0.663

BOOL bWoosh1Started = FALSE
BOOL bWoosh1Stopped = FALSE
BOOL bWoosh2Started = FALSE
BOOL bWoosh2Stopped = FALSE
BOOL bTrevorBinocularsConvo1Started = FALSE
BOOL bTrevorBinocularsConvo2Started = FALSE
BOOL bTrevorBinocularsConvo3Started = FALSE
BOOL bPrimaryTrafficStarted = FALSE
BOOL bHandledJumpCutEvents = FALSE
BOOL bTruckDriveByShakeStarted = FALSE
BOOL bSwitchFX1Started = FALSE
BOOL bSwitchFX2Started = FALSE
BOOL bSwitchFX3Started = FALSE
BOOL bSwitchFX4Started = FALSE

STRING strAnimDictCamShake = "shake_cam_all@"

STRING strSRLName = "fbi_4_binoculars"
FLOAT fSRLTime = 0.0

#IF IS_DEBUG_BUILD
BOOL bTeleportTruckToDestination = FALSE
#ENDIF

PROC SETUP_SPLINE_CAM_NODE_ARRAY_TRUCK_TO_TREVOR(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX truck, PED_INDEX trevor)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_TRUCK_TO_TREVOR")

	IF NOT thisSwitchCam.bInitialized

		
		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-5.3750, -4.6971, 1.3159>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<-2.9126, 1.2595, 0.9778>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 45.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 1
		thisSwitchCam.nodes[0].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 3500
		thisSwitchCam.nodes[1].vNodePos = <<-4.9301, -3.6343, 1.3798>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<-2.9214, 1.2467, 1.1996>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 45.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-4.9840, -3.4091, 6.9017>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<-3.2586, 0.8236, 7.1985>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 45.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 100
		thisSwitchCam.nodes[2].iRampDownDuration = 100
		thisSwitchCam.nodes[2].iHoldDuration = 800
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.4000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<-0.1211, 1.0820, -0.0704>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].vNodeDir = <<-14.6695, -0.0020, 170.8240>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 45.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 600
		thisSwitchCam.nodes[5].vNodePos = <<-0.1532, 0.8833, 0.6980>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].vNodeDir = <<-14.6696, -0.0020, 170.8240>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 45.0000
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 0.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 4000
		thisSwitchCam.nodes[6].vNodePos = <<-0.2473, 0.8093, 0.6748>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].vNodeDir = <<-14.6696, -0.0020, 170.8240>>
		thisSwitchCam.nodes[6].bPointAtEntity = FALSE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 45.0000
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 0.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000


		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---



		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_FIB4_TruckToTrevor.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_FIB4_TruckToTrevor.xml"

		thisSwitchCam.bInitialized = TRUE
	ENDIF
		
	thisSwitchCam.viVehicles[0] = truck
	thisSwitchCam.piPeds[1] = trevor
		
ENDPROC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_BINOCULARS_SCENE(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX trevor, VEHICLE_INDEX truck)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_BINOCULARS_SCENE")

	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<768.7385, -2357.3118, 57.6729>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<-17.0247, 0.0011, 153.7904>>
		thisSwitchCam.nodes[0].bPointAtEntity = FALSE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].fNodeFOV = 15.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_HEAVY
		thisSwitchCam.nodes[0].fNodeCamShake = 0.2500
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 2000
		thisSwitchCam.nodes[1].vNodePos = <<768.7133, -2356.7507, 57.8537>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<-18.4764, -0.4104, 161.6453>>
		thisSwitchCam.nodes[1].bPointAtEntity = FALSE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 15.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_HEAVY
		thisSwitchCam.nodes[1].fNodeCamShake = 0.2500
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 2000
		thisSwitchCam.nodes[2].vNodePos = <<768.7133, -2356.7507, 57.8537>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-12.7536, -0.4104, 170.3669>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 15.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_HEAVY
		thisSwitchCam.nodes[2].fNodeCamShake = 0.2500
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[3].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 3000
		thisSwitchCam.nodes[3].vNodePos = <<765.1157, -2378.9001, 50.2175>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[3].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[3].vNodeDir = <<-14.0014, 0.0000, 169.4132>>
		thisSwitchCam.nodes[3].bPointAtEntity = FALSE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].fNodeFOV = 10.0000
		thisSwitchCam.nodes[3].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].iNodeToClone = 0
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_HEAVY
		thisSwitchCam.nodes[3].fNodeCamShake = 0.2500
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[3].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[4].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 0
		thisSwitchCam.nodes[5].vNodePos = <<759.9988, -2387.2766, 21.0888>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<1.0832, 0.0000, 161.7656>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 25.3700
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 2500
		thisSwitchCam.nodes[6].vNodePos = <<758.6471, -2387.2766, 21.0888>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<1.0832, 0.0000, 161.0360>>
		thisSwitchCam.nodes[6].bPointAtEntity = FALSE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 25.3700
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[6].fNodeCamShake = 0.5000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000



		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 0
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_FIB4_BinocularsScene.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_FIB4_BinocularsScene.xml"

		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = trevor
	thisSwitchCam.viVehicles[1] = truck
	
ENDPROC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_TO_TRUCK(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX trevor, VEHICLE_INDEX truck)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_TO_TRUCK")

	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<0.2801, 1.2967, 0.5344>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<-0.1119, 0.0055, 0.4533>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 45.0000
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 3000
		thisSwitchCam.nodes[1].vNodePos = <<-1.0713, -0.7852, 0.5344>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<-3.4369, 0.0000, -53.5033>>
		thisSwitchCam.nodes[1].bPointAtEntity = FALSE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].fNodeFOV = 45.0000
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-1.1351, -0.8325, -0.7892>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<-3.4369, 0.0000, -53.5033>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].fNodeFOV = 45.0000
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 2
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.2000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 100.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 50
		thisSwitchCam.nodes[2].iRampDownDuration = 50
		thisSwitchCam.nodes[2].iHoldDuration = 200
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.4000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<3.3398, -7.5898, 7.7279>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].vNodeDir = <<-2.3801, -0.0023, 36.1109>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 50.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 1.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 600
		thisSwitchCam.nodes[5].vNodePos = <<3.5008, -7.8098, 1.1697>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].vNodeDir = <<-2.3801, -0.0023, 36.1112>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 50.0000
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 0.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 1000
		thisSwitchCam.nodes[6].vNodePos = <<3.4317, -7.7129, 1.1648>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].vNodeDir = <<-2.3801, -0.0023, 36.1112>>
		thisSwitchCam.nodes[6].bPointAtEntity = FALSE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 50.0000
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 0.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000


		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.bSplineNoSmoothing = FALSE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 1000


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_FIB4_TrevorToTruck.txt"
		thisSwitchCam.strXMLFileName = "CameraInfo_FIB4_TrevorToTruck.xml"

		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = trevor
	thisSwitchCam.viVehicles[1] = truck
	
ENDPROC

PROC PLAY_TREVOR_BINOCULARS_ANIM(PED_INDEX piTrevor, FLOAT fStartPhase = 0.0)
	CDEBUG3LN(DEBUG_MISSION, "PLAY_TREVOR_BINOCULARS_ANIM")
	
	IF DOES_ENTITY_EXIST(piTrevor)
		IF NOT IS_ENTITY_DEAD(piTrevor)
			CLEAR_PED_TASKS(piTrevor)
			TASK_PLAY_ANIM(piTrevor, "missfbi4", "_binoculars_trevor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING, fStartPhase)
			TASK_LOOK_AT_COORD(piTrevor, (<<788.3, -2333.5, 61.6>>), -1, SLF_WHILE_NOT_IN_FOV | SLF_USE_EYES_ONLY)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(piTrevor)
		ENDIF
	ENDIF
	
ENDPROC

PROC SETUP_TREVOR_WITH_BINOCULARS(PED_INDEX piTrevor)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_TREVOR_WITH_BINOCULARS")
	
	IF DOES_ENTITY_EXIST(piTrevor)
		IF NOT IS_ENTITY_DEAD(piTrevor)
			CLEAR_PED_TASKS_IMMEDIATELY(piTrevor)
			
			SET_ENTITY_COORDS(piTrevor, vTrevorBinocularsPos)
			SET_ENTITY_HEADING(piTrevor, fTrevorBinocularsHeading)
			
			FREEZE_ENTITY_POSITION(piTrevor, TRUE)
			
			binoculars.obj = CREATE_OBJECT(binoculars.model, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(piTrevor, (<<0.0, 0.0, 40.00>>)))
			IF DOES_ENTITY_EXIST(binoculars.obj)
				IF NOT IS_ENTITY_DEAD(binoculars.obj)
					ATTACH_ENTITY_TO_ENTITY(binoculars.obj, piTrevor, GET_PED_BONE_INDEX(piTrevor, BONETAG_PH_L_HAND), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>))
				ENDIF
			ENDIF
			
			PLAY_TREVOR_BINOCULARS_ANIM(piTrevor)
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")
	
	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables -")
		START_WIDGET_GROUP("Truck to Trevor")
			ADD_WIDGET_BOOL("Teleport Truck to Destination", bTeleportTruckToDestination)
			START_WIDGET_GROUP("Truck and Traffic Tunables")
				ADD_WIDGET_FLOAT_SLIDER("Target Truck Recording 1 Skip Time", fTruckRecordingJump1SkipTime, 0.0, 999999.9, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("Target Truck Recording 2 Skip Time", fTruckRecordingJump2SkipTime, 0.0, 999999.9, 100.0)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Conversation Timing")
				ADD_WIDGET_INT_SLIDER("Trevor Binoculars Convo 1 Delay", iTrevorBinocularsConvo1Delay, 0, 999999, 100)
				ADD_WIDGET_INT_SLIDER("Trevor Binoculars Convo 2 Delay", iTrevorBinocularsConvo2Delay, 0, 999999, 100)
				ADD_WIDGET_INT_SLIDER("Trevor Binoculars Convo 3 Delay", iTrevorBinocularsConvo3Delay, 0, 999999, 100)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Binoculars Mask Min/Max Nodes")
				ADD_WIDGET_INT_SLIDER("Start Node", iBinocularsMaskStartNode, 0, 30, 1)
				ADD_WIDGET_INT_SLIDER("Stop Node", iBinocularsMaskStopNode, 0, 30, 1)
				ADD_WIDGET_INT_SLIDER("Binoculars Jump Cut Node", iBinocularsJumpCutNode, 0, 30, 1)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Woosh Sound Tunables")
				ADD_WIDGET_FLOAT_SLIDER("Woosh 1 Start Phase", fWoosh1StartPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Woosh 1 Stop Phase", fWoosh1StopPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Woosh 2 Start Phase", fWoosh2StartPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Woosh 2 Stop Phase", fWoosh2StopPhase, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Trevor with Binoculars")
				ADD_WIDGET_BOOL("Reset Trevor with Binoculars", bResetTrevorWithBinoculars)
				ADD_WIDGET_VECTOR_SLIDER("Pos", vTrevorBinocularsPos, -15000.0, 15000.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Heading", fTrevorBinocularsHeading, -360.0, 360.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Trevor Binocular Anim 2 Phase", fTrevorBinocularAnim2Phase, 0.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Truck Drive by Shake")
				ADD_WIDGET_FLOAT_SLIDER("Start Phase", fTruckDriveByShakeStartPhase, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Shake Scale", fTruckDriveByShakeScale, 0.0, 10.0, 0.01)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_SPLINE_CAM_SCRIPT_SPECIFIC_WIDGETS()
	//CDEBUG3LN(DEBUG_MISSION, "UPDATE_SPLINE_CAM_SCRIPT_SPECIFIC_WIDGETS")

	IF bTeleportTruckToDestination
		
		DO_SCREEN_FADE_OUT(0)
		
		WAIT(0)
		
		VECTOR vDebugTruckPos = <<917.3, -2194.2, 30.8>>
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(rubbish_truck.veh)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(rubbish_truck.veh)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), rubbish_truck.veh)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), rubbish_truck.veh)
				ENDIF
				SET_ENTITY_HEADING(rubbish_truck.veh, 180.0)
				SET_ENTITY_COORDS(rubbish_truck.veh, vDebugTruckPos)
				SET_VEHICLE_ON_GROUND_PROPERLY(rubbish_truck.veh)
			ENDIF
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		
		LOAD_SCENE(vDebugTruckPos)
		
		WAIT(0)
		
		DO_SCREEN_FADE_IN(0)
		
		bTeleportTruckToDestination = FALSE
	ENDIF
	
	IF bResetTrevorWithBinoculars
		SETUP_TREVOR_WITH_BINOCULARS(PLAYER_PED_ID())
		bResetTrevorWithBinoculars = FALSE
	ENDIF
	
ENDPROC

#ENDIF


PROC CLEAR_TRAFFIC_AND_STREETS()
	CDEBUG3LN(DEBUG_MISSION, "CLEAR_TRAFFIC_AND_STREETS")
	
	//Setup the streets for the scene
	//----------------------------------
	
	CLEANUP_UBER_PLAYBACK(TRUE)//delete all setpiece cars
		
	//roads near shootout allowing traffic to pass at end of road
	SET_ROADS_IN_AREA(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, FALSE)
	SET_PED_PATHS_IN_AREA(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, FALSE) 
	ADD_SCENARIO_BLOCKING_AREA(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)

	//switches off generators near shootout
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, FALSE) //cars near ally way
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, FALSE) //vehicle near cover blocks

	//roads for stockade in cutscene
	SET_ROADS_IN_AREA(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>, FALSE)

	//CLEAR_AREA(<<790.83, -2330.06, 62.67>>, 1000.00, TRUE)
	
ENDPROC

PROC REMOVE_UNNEEDED_ASSETS()
	CDEBUG3LN(DEBUG_MISSION, "REMOVE_UNNEEDED_ASSETS")
	
	IF DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
		DELETE_PED(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))

	IF DOES_ENTITY_EXIST(tow_truck.veh)
		DELETE_VEHICLE(tow_truck.veh)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(tow_truck.model)

	IF DOES_ENTITY_EXIST(trevors_car.veh)
		DELETE_VEHICLE(trevors_car.veh)
	ENDIF 
	SET_MODEL_AS_NO_LONGER_NEEDED(trevors_car.model)
	
	IF DOES_ENTITY_EXIST(michaels_car.veh)
		DELETE_VEHICLE(michaels_car.veh)
	ENDIF 
	SET_MODEL_AS_NO_LONGER_NEEDED(michaels_car.model)
	
	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_PED_POPULATION_BUDGET(0)
	
ENDPROC

PROC REQUEST_BINOCULARS_SCENE_ASSETS()
	CDEBUG3LN(DEBUG_MISSION, "REQUEST_BINOCULARS_SCENE_ASSETS")

	REQUEST_MODEL(army_truck.model) 
	REQUEST_MODEL(army_truck_driver.model)
	REQUEST_MODEL(traffic_vehicle[0].model)
	
	REQUEST_VEHICLE_RECORDING(002, "lkcountry") 
	REQUEST_VEHICLE_RECORDING(003, "lkcountry") 
	REQUEST_VEHICLE_RECORDING(004, "lkcountry") 
	REQUEST_VEHICLE_RECORDING(005, "lkcountry") 
	REQUEST_VEHICLE_RECORDING(010, "lkcountry")
	REQUEST_VEHICLE_RECORDING(011, "lkcountry")
	REQUEST_VEHICLE_RECORDING(012, "lkcountry")
	REQUEST_VEHICLE_RECORDING(014, "lkcountry")
	REQUEST_VEHICLE_RECORDING(015, "lkcountry")
	REQUEST_VEHICLE_RECORDING(016, "lkcountry")
	
	sf_binoculars = REQUEST_SCALEFORM_MOVIE("binoculars")
	
ENDPROC

FUNC BOOL HAS_BINOCULARS_SCENE_ASSETS_LOADED()
	//CDEBUG3LN(DEBUG_MISSION, "HAS_BINOCULARS_SCENE_ASSETS_LOADED")
	
	IF NOT HAS_MODEL_LOADED(army_truck.model)
		CDEBUG3LN(DEBUG_MISSION, "Waiting for Army Truck model...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(army_truck_driver.model)
		CDEBUG3LN(DEBUG_MISSION, "Waiting for Army Truck Driver model...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(traffic_vehicle[0].model)
		CDEBUG3LN(DEBUG_MISSION, "Waiting for Traffic Vehicle model...")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(002, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 2...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(003, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 3...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(004, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 4...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(005, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 5...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(010, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 10...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(011, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 11...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(012, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 12...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(014, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 14...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(015, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 15...")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(016, "lkcountry")
		CDEBUG3LN(DEBUG_MISSION, "Waiting for vehicle recording 16...")
		RETURN FALSE
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sf_binoculars)
		CDEBUG3LN(DEBUG_MISSION, "Waiting for Binoculars scaleform movie...")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CREATE_PRIMARY_TRAFFIC()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_PRIMARY_TRAFFIC")
	
	traffic_vehicle[0].veh = create_vehicle(traffic_vehicle[0].model, traffic_vehicle[0].pos)
	start_playback_recorded_vehicle(traffic_vehicle[0].veh, traffic_vehicle[0].recording_number, "lkheat")
	skip_time_in_playback_recorded_vehicle(traffic_vehicle[0].veh, 7000)
	force_playback_recorded_vehicle_update(traffic_vehicle[0].veh)

	traffic_vehicle[1].veh = create_vehicle(traffic_vehicle[1].model, traffic_vehicle[1].pos)
	start_playback_recorded_vehicle(traffic_vehicle[1].veh, traffic_vehicle[1].recording_number, "lkheat")
	skip_time_in_playback_recorded_vehicle(traffic_vehicle[1].veh, 7000)
	force_playback_recorded_vehicle_update(traffic_vehicle[1].veh)
	
	traffic_vehicle[2].veh = create_vehicle(traffic_vehicle[2].model, traffic_vehicle[2].pos)
	start_playback_recorded_vehicle(traffic_vehicle[2].veh, traffic_vehicle[2].recording_number, "lkheat")
	force_playback_recorded_vehicle_update(traffic_vehicle[2].veh)
	
	traffic_vehicle[3].veh = create_vehicle(traffic_vehicle[3].model, traffic_vehicle[3].pos)
	start_playback_recorded_vehicle(traffic_vehicle[3].veh, traffic_vehicle[3].recording_number, "lkheat")
	skip_time_in_playback_recorded_vehicle(traffic_vehicle[3].veh, 7000)
	force_playback_recorded_vehicle_update(traffic_vehicle[3].veh)
	
	traffic_vehicle[7].veh = create_vehicle(traffic_vehicle[7].model, traffic_vehicle[7].pos)
	start_playback_recorded_vehicle(traffic_vehicle[7].veh, traffic_vehicle[7].recording_number, "lkheat")
	skip_time_in_playback_recorded_vehicle(traffic_vehicle[7].veh, 4200)
	force_playback_recorded_vehicle_update(traffic_vehicle[7].veh)
//	pause_playback_recorded_vehicle(traffic_vehicle[7].veh)

ENDPROC

PROC CREATE_SECONDARY_TRAFFIC()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SECONDARY_TRAFFIC")

	traffic_vehicle[4].veh = create_vehicle(traffic_vehicle[4].model, traffic_vehicle[4].pos)
	start_playback_recorded_vehicle(traffic_vehicle[4].veh, traffic_vehicle[0].recording_number, "lkheat")
	skip_time_in_playback_recorded_vehicle(traffic_vehicle[4].veh, 3000) //4000
	force_playback_recorded_vehicle_update(traffic_vehicle[4].veh)

	traffic_vehicle[5].veh = create_vehicle(traffic_vehicle[5].model, traffic_vehicle[5].pos)
	start_playback_recorded_vehicle(traffic_vehicle[5].veh, traffic_vehicle[5].recording_number, "lkheat")
	skip_time_in_playback_recorded_vehicle(traffic_vehicle[5].veh, 5250)
	force_playback_recorded_vehicle_update(traffic_vehicle[5].veh)
	
	traffic_vehicle[6].veh = create_vehicle(traffic_vehicle[6].model, traffic_vehicle[6].pos)
	start_playback_recorded_vehicle(traffic_vehicle[6].veh, traffic_vehicle[6].recording_number, "lkheat")
	force_playback_recorded_vehicle_update(traffic_vehicle[6].veh)
	
ENDPROC

PROC CREATE_ARMY_TRUCK()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_ARMY_TRUCK")
	
	IF DOES_ENTITY_EXIST(army_truck.veh)
		DELETE_VEHICLE(army_truck.veh)
	ENDIF
	army_truck.veh = CREATE_VEHICLE(army_truck.model, army_truck.pos, army_truck.heading)
	IF DOES_ENTITY_EXIST(army_truck.veh)
		IF NOT IS_ENTITY_DEAD(army_truck.veh)
			
			SET_VEHICLE_HAS_STRONG_AXLES(army_truck.veh, TRUE)
			SET_ENTITY_PROOFS(army_truck.veh, TRUE, TRUE, TRUE, TRUE, TRUE)
			
			army_truck_driver.ped = CREATE_PED_INSIDE_VEHICLE(army_truck.veh, PEDTYPE_MISSION, army_truck_driver.model, VS_FRONT_RIGHT)
			IF DOES_ENTITY_EXIST(army_truck_driver.ped)
				IF NOT IS_ENTITY_DEAD(army_truck_driver.ped)
					SET_PED_DIES_WHEN_INJURED(army_truck_driver.ped, TRUE)
					DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_driver.ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(army_truck_driver.ped, TRUE)
				ENDIF
			ENDIF
			
			army_truck_passenger.ped = CREATE_PED_INSIDE_VEHICLE(army_truck.veh, PEDTYPE_MISSION, army_truck_passenger.model)
			IF DOES_ENTITY_EXIST(army_truck_passenger.ped)
				IF NOT IS_ENTITY_DEAD(army_truck_passenger.ped)
					SET_PED_DIES_WHEN_INJURED(army_truck_passenger.ped, TRUE)
					DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_passenger.ped)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(army_truck_passenger.ped, TRUE)
				ENDIF
			ENDIF
			
			START_PLAYBACK_RECORDED_VEHICLE(army_truck.veh, 001, "lkcountry") 
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(army_truck.veh, fTruckRecordingJump1SkipTime)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(army_truck.veh)
			
		ENDIF
	ENDIF
	
ENDPROC



PROC CLEANUP_BINOCULARS_SCENE()
	CDEBUG3LN(DEBUG_MISSION, "CLEANUP_BINOCULARS_SCENE")
	
	INT i
	REPEAT COUNT_OF(traffic_vehicle) i
		SET_MODEL_AS_NO_LONGER_NEEDED(traffic_vehicle[i].model)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(traffic_vehicle[i].veh)
	ENDREPEAT 

	//switch on roads for stockade cutscene and switch off roads near shootout
	//SET_ROADS_IN_AREA(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>, TRUE)
	set_roads_back_to_original(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>)
	SET_ROADS_IN_AREA(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, FALSE)

	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sf_binoculars)

	if is_vehicle_driveable(army_truck.veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(army_truck.veh)
			STOP_PLAYBACK_RECORDED_VEHICLE(army_truck.veh)
		ENDIF 
	endif 

	DELETE_PED(army_truck_driver.ped)
	DELETE_PED(army_truck_passenger.ped)
	SET_MODEL_AS_NO_LONGER_NEEDED(army_truck_driver.model)

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

proc request_rubbish_truck_assets()
			
	request_model(GET_PLAYER_PED_MODEL(char_michael))
	request_model(GET_PLAYER_PED_MODEL(char_franklin))
	
	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)
	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(c4.model)
	
	request_vehicle_recording(002, "lkcountry") 
	request_vehicle_recording(003, "lkcountry") 
	request_vehicle_recording(004, "lkcountry") 
	request_vehicle_recording(005, "lkcountry") 
	request_vehicle_recording(010, "lkcountry")
	request_vehicle_recording(011, "lkcountry")
	request_vehicle_recording(012, "lkcountry")
	request_vehicle_recording(014, "lkcountry")
	request_vehicle_recording(015, "lkcountry")
	request_vehicle_recording(016, "lkcountry")
	
	request_anim_dict("missfbi4")
	request_anim_dict("misssagrab")
	
	REQUEST_WAYPOINT_RECORDING("heat1")
	REQUEST_WAYPOINT_RECORDING("heat2")
	REQUEST_WAYPOINT_RECORDING("heat3")
	
	//request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_01")
	request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	
endproc 

PROC UPDATE_SRL_FRAME_TIME()
	//CDEBUG3LN(DEBUG_MISSION, "UPDATE_SRL_FRAME_TIME")
	
	OVERRIDE_LODSCALE_THIS_FRAME(1.0)
	
	fSRLTime += (GET_FRAME_TIME() * 1000)
	IF fSRLTime < 0.0
		fSRLTime = 0.0
	ENDIF
	CDEBUG3LN(DEBUG_MISSION, "SRL Time = ", fSRLTime)
	SET_SRL_TIME(fSRLTime)
			
ENDPROC

FUNC BOOL HANDLE_TREVOR_BINOCULARS_CINEMATIC(SWITCH_CAM_STRUCT &thisSwitchCam1, SWITCH_CAM_STRUCT &thisSwitchCam2, SWITCH_CAM_STRUCT &thisSwitchCam3)
	//CDEBUG3LN(DEBUG_MISSION, "HANDLE_TREVOR_BINOCULARS_CINEMATIC")
	
	INT iCurrentNode
	FLOAT fCamPhase
	BOOL bLoadSceneOk = FALSE
	bLoadSceneOk = bLoadSceneOk
	
	/*printstring("eSwitchCamState = ")
	printint(enum_to_int(eSwitchCamState))
	printnl()*/
	
	SWITCH eSwitchCamState
		
		CASE SWITCH_CAM_IDLE
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_START_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
			
			setup_vehicle_proofs(rubbish_truck.veh)
			
			set_roads_back_to_original(<<885.8, -2086.9, 100.00>>, <<941.9, -2463.0, -100.00>>) 
			
			CLEAR_TRAFFIC_AND_STREETS()
			REMOVE_UNNEEDED_ASSETS()
			REQUEST_BINOCULARS_SCENE_ASSETS()
			
			if IS_AUDIO_SCENE_ACTIVE("FBI_4_DRIVE_TO_CYPRUS_FLATS")
				STOP_AUDIO_SCENE("FBI_4_DRIVE_TO_CYPRUS_FLATS")
			endif 
			
			//TELEPORT_PLAYER_NEAR_TREVOR()

			DESTROY_ALL_CAMS()

			SETUP_SPLINE_CAM_NODE_ARRAY_TRUCK_TO_TREVOR(thisSwitchCam1, rubbish_truck.veh, selector_ped.pedID[SELECTOR_PED_TREVOR])
			CREATE_SPLINE_CAM(thisSwitchCam1)
			
			clear_area_of_vehicles(<<913.3915, -2231.6924, 29.3850>>, 100)
			clear_ped_tasks_immediately(player_ped_id())
			set_ped_into_vehicle(player_ped_id(), rubbish_truck.veh)
//			set_entity_heading(rubbish_truck.veh, 175.1171)
//			set_entity_coords(rubbish_truck.veh, <<913.3915, -2231.6924, 29.3850>>)
//			set_vehicle_on_ground_properly(rubbish_truck.veh)

			CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(false)

			SET_CAM_ACTIVE(thisSwitchCam1.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			if not has_label_been_triggered("in_pos_1_1")
				//if create_conversation(scripted_speech[0], "heataud", "in_pos_1", CONV_PRIORITY_MEDIUM)//over_take0  in_pos_1
				if PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech[0], "heataud", "in_pos_1", "in_pos_1_1", CONV_PRIORITY_medium) 
					
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 30.0, REPLAY_IMPORTANCE_HIGHEST)
					
					set_label_as_triggered("in_pos_1_1", true)
				endif
			endif
			
			bWoosh1Started = FALSE
			bWoosh1Stopped = FALSE
			bWoosh2Started = FALSE
			bWoosh2Stopped = FALSE
			bTrevorBinocularsConvo1Started = FALSE
			bTrevorBinocularsConvo2Started = FALSE
			bTrevorBinocularsConvo3Started = FALSE
			bPrimaryTrafficStarted = FALSE
			bHandledJumpCutEvents = FALSE
			bTruckDriveByShakeStarted = FALSE
			bSwitchFX1Started = FALSE
			bSwitchFX2Started = FALSE
			bSwitchFX3Started = FALSE
			bSwitchFX4Started = FALSE
			
			SETUP_TREVOR_WITH_BINOCULARS(selector_ped.pedID[SELECTOR_PED_TREVOR])
			
			BEGIN_SRL()
			fSRLTime = -9999.0 //Just setting to large negitive so first frame i actully set to zero in UPDATE_SRL_FRAME_TIME()
			
			SETTIMERA(0)
			
			trigger_music_event("FBI4_SWITCH_1")
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
		FALLTHRU
	
		CASE SWITCH_CAM_PLAYING_SPLINE1
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			UPDATE_SRL_FRAME_TIME()
			
			IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)
			
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam1)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam1.ciSpline)
				
				printstring("fCamPhase = ")
				printfloat(fCamPhase)
				printnl()
				
				//added to make release compile
				bWoosh1Started = bWoosh1Started
				bWoosh1Stopped = bWoosh1Stopped
				fWoosh2StartPhase = fWoosh2StartPhase
				fWoosh1StopPhase = fWoosh1StopPhase
				
//				IF NOT bWoosh1Started
//					IF fCamPhase >= fWoosh1StartPhase
//						iWooshSoundID = GET_SOUND_ID()
//						PLAY_SOUND_FRONTEND(iWooshSoundID, "In", "SHORT_PLAYER_SWITCH_SOUND_SET")
//						bWoosh1Started = TRUE
//					ENDIF
//				ENDIF
				
//				IF NOT bWoosh1Stopped
//					IF fCamPhase >= fWoosh1StopPhase
//						STOP_SOUND(iWooshSoundID)
//						bWoosh1Stopped = TRUE
//					ENDIF
//				ENDIF

				//----- NEW 
				
				if not trevor_binoculars_hit_out_0
					IF fCamPhase >= fWoosh1StartPhase
						
						PLAY_SOUND_FRONTEND(-1, "Hit_out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						switch_sound_time = get_game_timer()
						trevor_binoculars_hit_out_0 = true 
						
					endif 
				
				else 
				
					if not trevor_binoculars_transition_0
					
						if lk_timer(switch_sound_time, 200)
						
							iWooshSoundID = GET_SOUND_ID()
							PLAY_SOUND_FRONTEND(iWooshSoundID, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							trevor_binoculars_transition_0 = true 
							
						endif 
						
					endif 
				
				endif 
				
				//----
			
				IF NOT bSwitchFX1Started
					IF fCamPhase >= fSwitchFX1StartPhase
						ANIMPOSTFX_PLAY("SwitchShortMichaelIn", 0, FALSE)
						bSwitchFX1Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSwitchFX2Started
					IF fCamPhase >= fSwitchFX2StartPhase
						
						ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
						
						bSwitchFX2Started = TRUE
					ENDIF
				ENDIF

				if not trevor_binoculars_hit_out_1
					IF fCamPhase >= 0.5
						
//						if not has_sound_finished(iWooshSoundID)
//							STOP_SOUND(iWooshSoundID)
//						endif 
						
//						PLAY_SOUND_FRONTEND(-1, "Hit_out", "PLAYER_SWITCH_CUSTOM_SOUNDSET") //removed bug 2057552
						
						trevor_binoculars_hit_out_1 = true 
					endif 
				endif 

				
				IF NOT bTrevorBinocularsConvo1Started
					IF TIMERA() > iTrevorBinocularsConvo1Delay
						CDEBUG3LN(DEBUG_MISSION, "Starting Trevor Binoculars Convo 1...")
						CREATE_CONVERSATION(scripted_speech[0], "HeatAud", "heat_looks", CONV_PRIORITY_MEDIUM)
						bTrevorBinocularsConvo1Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bPrimaryTrafficStarted
					IF iCurrentNode >= 3
						CLEAR_AREA(<<790.83, -2330.06, 62.67>>, 10000.00, TRUE)
						set_entity_heading(rubbish_truck.veh, 175.1171)
						set_entity_coords(rubbish_truck.veh, <<913.3915, -2231.6924, 29.3850>>)
						set_vehicle_on_ground_properly(rubbish_truck.veh)
						CREATE_PRIMARY_TRAFFIC()
						
						bPrimaryTrafficStarted = TRUE
					ENDIF
				ENDIF

				IF fCamPhase >= 1.0

					start_audio_scene("FBI_4_CUTSCENE_BINOCULARS")
				
					SET_TIME_SCALE(1.0)
					IF DOES_CAM_EXIST(thisSwitchCam1.ciSpline)
						IF IS_CAM_ACTIVE(thisSwitchCam1.ciSpline)
							DESTROY_CAM(thisSwitchCam1.ciSpline)
						ENDIF
					ENDIF
					DESTROY_ALL_CAMS()
					
					eSwitchCamState = SWITCH_CAM_START_SPLINE2
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		FALLTHRU
		
		CASE SWITCH_CAM_START_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE2")
			
			DESTROY_ALL_CAMS()

			SETUP_SPLINE_CAM_NODE_ARRAY_BINOCULARS_SCENE(thisSwitchCam2, selector_ped.pedID[SELECTOR_PED_TREVOR], army_truck.veh)
			CREATE_SPLINE_CAM(thisSwitchCam2)

			//Need to pause until the binoculars overlay can be removed from screen
			SET_CAM_SPLINE_NODE_EXTRA_FLAGS(thisSwitchCam2.ciSpline, iBinocularsJumpCutNode + 1, CAM_SPLINE_NODE_FORCE_PAUSE)
			
			SET_CAM_ACTIVE(thisSwitchCam2.ciSpline, TRUE)
			
			SET_TIMECYCLE_MODIFIER("telescope")
			
			CREATE_PRIMARY_TRAFFIC()
			CREATE_ARMY_TRUCK()
			
			SETTIMERA(0)
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2")
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE2
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE2")
			
			UPDATE_SRL_FRAME_TIME()

			IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)
			
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam2)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam2.ciSpline)
				
				printstring("iCurrentNode = ")
				printint(iCurrentNode)
				printnl()
				
				IF NOT bHandledJumpCutEvents
				AND NOT IS_CAM_SPLINE_PAUSED(thisSwitchCam2.ciSpline)
					IF iCurrentNode >= iBinocularsMaskStartNode
					AND iCurrentNode <= iBinocularsMaskStopNode
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
						DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
						SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					ENDIF
				ENDIF
				
				IF NOT bHandledJumpCutEvents
					IF IS_CAM_SPLINE_PAUSED(thisSwitchCam2.ciSpline)
						SET_CAM_SPLINE_NODE_EXTRA_FLAGS(thisSwitchCam2.ciSpline, iBinocularsJumpCutNode + 1, CAM_SPLINE_NODE_FORCE_NONE)
						
						CREATE_SECONDARY_TRAFFIC()
						
						PLAY_TREVOR_BINOCULARS_ANIM(selector_ped.pedID[SELECTOR_PED_TREVOR], fTrevorBinocularAnim2Phase)
						
						//Start Truck Recording Jump 2
						IF DOES_ENTITY_EXIST(army_truck.veh)
							IF NOT IS_ENTITY_DEAD(army_truck.veh)
								CDEBUG3LN(DEBUG_MISSION, "Starting Truck Jump 2...")
								STOP_PLAYBACK_RECORDED_VEHICLE(army_truck.veh)
								START_PLAYBACK_RECORDED_VEHICLE(army_truck.veh, 001, "lkcountry") 
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(army_truck.veh, fTruckRecordingJump2SkipTime)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(army_truck.veh)
								start_audio_scene("FBI_4_CUTSCENE_TRUCK")
							ENDIF
						ENDIF
						
						CLEAR_TIMECYCLE_MODIFIER()
						
						bHandledJumpCutEvents = TRUE
					ENDIF
				ENDIF
				
				IF NOT bTrevorBinocularsConvo2Started
					IF TIMERA() > iTrevorBinocularsConvo2Delay
						CDEBUG3LN(DEBUG_MISSION, "Starting Trevor Binoculars Convo 2...")
						CREATE_CONVERSATION(scripted_speech[0], "HeatAud", "van_cut_0", CONV_PRIORITY_MEDIUM) 
						bTrevorBinocularsConvo2Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bTrevorBinocularsConvo3Started
					IF TIMERA() > iTrevorBinocularsConvo3Delay
						CDEBUG3LN(DEBUG_MISSION, "Starting Trevor Binoculars Convo 3...")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "heataud", "van_cut_0b", CONV_PRIORITY_MEDIUM)
						bTrevorBinocularsConvo3Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bTruckDriveByShakeStarted
					IF fCamPhase >= fTruckDriveByShakeStartPhase
						SHAKE_CAM(thisSwitchCam2.ciSpline, "SKY_DIVING_SHAKE", fTruckDriveByShakeScale)
						bTruckDriveByShakeStarted = TRUE
					ENDIF
				ENDIF
				
				IF fCamPhase >= 1.0
				
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(false)
				
					SET_TIME_SCALE(1.0)
					IF DOES_CAM_EXIST(thisSwitchCam2.ciSpline)
						IF IS_CAM_ACTIVE(thisSwitchCam2.ciSpline)
							DESTROY_CAM(thisSwitchCam2.ciSpline)
						ENDIF
					ENDIF
					DESTROY_ALL_CAMS()
					
					if IS_AUDIO_SCENE_ACTIVE("FBI_4_CUTSCENE_TRUCK")
						STOP_AUDIO_SCENE("FBI_4_CUTSCENE_TRUCK")
					endif 
					
					CLEANUP_BINOCULARS_SCENE()
									
					eSwitchCamState = SWITCH_CAM_START_SPLINE3
				ELSE
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
			
		FALLTHRU
		

		CASE SWITCH_CAM_START_SPLINE3
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE3")

			SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_TO_TRUCK(thisSwitchCam3, selector_ped.pedID[SELECTOR_PED_TREVOR], rubbish_truck.veh)
			CREATE_SPLINE_CAM(thisSwitchCam3)

			SET_CAM_ACTIVE(thisSwitchCam3.ciSpline, TRUE)
			
			SETTIMERA(0)
			
			request_rubbish_truck_assets()

			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE3
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE3")
		FALLTHRU
	
		CASE SWITCH_CAM_PLAYING_SPLINE3
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE3")
			
			UPDATE_SRL_FRAME_TIME()
			
			IF IS_CAM_ACTIVE(thisSwitchCam3.ciSpline)
			
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam3)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam3.ciSpline)
				
				printstring("icurrentNode")
				printint(icurrentNode)
				printnl()
				
				printstring("fCamPhase = ")
				printfloat(fCamPhase)
				printnl()
				
				if not trevor_binoculars_hit_out_2
					IF fCamPhase > 0.55
						PLAY_SOUND_FRONTEND(-1, "Hit_out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						switch_sound_time = get_game_timer()
						trevor_binoculars_hit_out_2 = true
					endif 
				else 

					IF NOT bWoosh2Started
						fWoosh2StartPhase = fWoosh2StartPhase
						//IF fCamPhase > fWoosh2StartPhase
						if lk_timer(switch_sound_time, 200)
							iWooshSoundID = GET_SOUND_ID()
							//PLAY_SOUND_FRONTEND(iWooshSoundID, "In", "SHORT_PLAYER_SWITCH_SOUND_SET")
							PLAY_SOUND_FRONTEND(iWooshSoundID, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bWoosh2Started = TRUE
						ENDIF
					ENDIF
					
				endif 
				
				IF NOT bWoosh2Stopped
					IF fCamPhase > fWoosh2StopPhase
						//STOP_SOUND(iWooshSoundID)
						
						VECTOR vPostCamPos
						VECTOR vPostCamDir
						vPostCamPos = <<914.7, -2216.7, 34.8>>
						vPostCamDir = <<-5.3, 0.0, 174.9>>
						SET_SRL_POST_CUTSCENE_CAMERA(vPostCamPos, vPostCamDir)
						
						bWoosh2Stopped = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSwitchFX3Started
					IF fCamPhase >= fSwitchFX3StartPhase
						ANIMPOSTFX_PLAY("SwitchShortTrevorIn", 0, FALSE)
						bSwitchFX3Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSwitchFX4Started
					IF fCamPhase >= fSwitchFX4StartPhase
						ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
						bSwitchFX4Started = TRUE
					ENDIF
				ENDIF
				
				if not trevor_binoculars_hit_out_3
				
					IF fCamPhase >= 0.75 
					
						if not has_sound_finished(iWooshSoundID)
							stop_sound(iWooshSoundID)
						endif 
					
						PLAY_SOUND_FRONTEND(-1, "Hit_out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						trevor_binoculars_hit_out_3 = true 
						
					endif 
				endif 

				IF fCamPhase >= 1.0
				
					if IS_AUDIO_SCENE_ACTIVE("FBI_4_CUTSCENE_BINOCULARS")
						STOP_AUDIO_SCENE("FBI_4_CUTSCENE_BINOCULARS")
					endif 
				
					SET_TIME_SCALE(1.0)
					
					IF DOES_CAM_EXIST(thisSwitchCam3.ciSpline)
						IF IS_CAM_ACTIVE(thisSwitchCam3.ciSpline)
							DESTROY_CAM(thisSwitchCam3.ciSpline)
						ENDIF
					ENDIF
					DESTROY_ALL_CAMS()
					
					CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(true)
				
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
					
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SET_WIDESCREEN_BORDERS(FALSE, 500)
					
					STOP_SOUND(iWooshSoundID)

					SETTIMERA(0)
					
					deactivate_vehicle_proofs(rubbish_truck.veh)
					end_cutscene_no_fade(false, true, false, 0, 0, 3000, false)
					
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
				ENDIF

			ENDIF
			
		BREAK
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
			
			UPDATE_SRL_FRAME_TIME()
			
			IF TIMERA() > 100
			
				SET_VEHICLE_POPULATION_BUDGET(3)
				SET_PED_POPULATION_BUDGET(3)
				
				END_SRL()
				
				eSwitchCamState = SWITCH_CAM_IDLE
				CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
			
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH

	RETURN FALSE
ENDFUNC



//**************************************** LOCAL FUNCTIONS ****************************************

proc cleanup_assets_after_shootout()

	int i = 0

//	clear_player_wanted_level(player_id())
//	set_fake_wanted_level(0)
//	set_max_wanted_level(1)
//	set_create_random_cops(false) //dont want to create random cops 
//	set_wanted_level_multiplier(0.1)

	set_roads_back_to_original(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
	set_ped_paths_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, true)
	
	//roads outside depot
	set_roads_back_to_original(<<1412.83, -1954.96, -100.00>>, <<1406.50, -1941.47, 100.00>>)
	
	//roads for driving down to park ambulance
	set_roads_back_to_original(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>)
	
	//not sure
	set_roads_back_to_original(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>)

	SET_VEHICLE_AS_NO_LONGER_NEEDED(tow_truck.veh)
	set_model_as_no_longer_needed(tow_truck.model)
	
//	SET_VEHICLE_AS_NO_LONGER_NEEDED(rubbish_truck.veh)
//	set_model_as_no_longer_needed(rubbish_truck.model)
	
	for i = 0 to count_of(police_man) - 1
		SET_PED_AS_NO_LONGER_NEEDED(police_man[i].ped)
		set_model_as_no_longer_needed(police_man[i].model)
	endfor 
	
	for i = 0 to count_of(police_car) -1
		if does_entity_exist(police_car[i].veh)
			if not is_vehicle_driveable(police_car[i].veh)
				INFORM_MISSION_STATS_OF_INCREMENT(FBI4_VEHICLES_DESTROYED) 
			endif 
		endif 
	endfor 
	
	for i = 0 to count_of(police_car) - 1
		SET_VEHICLE_AS_NO_LONGER_NEEDED(police_car[i].veh)
		set_model_as_no_longer_needed(police_car[i].model)
	endfor 
	
	for i = 0 to count_of(absail_police_man) - 1
		SET_PED_AS_NO_LONGER_NEEDED(absail_police_man[i].ped)
		set_model_as_no_longer_needed(absail_police_man[i].model)
	endfor 
	
	for i = 0 to count_of(backup_police_front) - 1
		SET_PED_AS_NO_LONGER_NEEDED(backup_police_front[i].ped)
		set_model_as_no_longer_needed(backup_police_front[i].model)
	endfor 
	
	for i = 0 to count_of(backup_police_alley) - 1
		SET_PED_AS_NO_LONGER_NEEDED(backup_police_alley[i].ped)
		set_model_as_no_longer_needed(backup_police_alley[i].model)
	endfor 
	
	for i = 0 to count_of(helicopter) - 1
		SET_VEHICLE_AS_NO_LONGER_NEEDED(helicopter[i].veh)
		set_model_as_no_longer_needed(helicopter[i].model)
		SET_PED_AS_NO_LONGER_NEEDED(helicopter_driver[i].ped)
		set_model_as_no_longer_needed(helicopter_driver[i].model)
	endfor 

	SET_VEHICLE_AS_NO_LONGER_NEEDED(army_truck.veh)
	set_model_as_no_longer_needed(army_truck.model)
	SET_PED_AS_NO_LONGER_NEEDED(army_truck_driver.ped)
	SET_PED_AS_NO_LONGER_NEEDED(army_truck_passenger.ped)
	SET_PED_AS_NO_LONGER_NEEDED(army_truck_guy.ped)
	SET_PED_AS_NO_LONGER_NEEDED(army_truck_guy_2.ped)
	set_model_as_no_longer_needed(army_truck_guy.model)
	
	SET_OBJECT_AS_NO_LONGER_NEEDED(c4.obj)
	set_model_as_no_longer_needed(c4.model)
	
	set_model_as_no_longer_needed(S_M_Y_Cop_01)
	set_model_as_no_longer_needed(S_M_Y_SWAT_01)
	
	remove_vehicle_asset(police_car[0].model)
	
	remove_vehicle_recording(002, "lkcountry") 
	remove_vehicle_recording(003, "lkcountry") 
	remove_vehicle_recording(004, "lkcountry") 
	remove_vehicle_recording(005, "lkcountry") 
	remove_vehicle_recording(010, "lkcountry")
	remove_vehicle_recording(011, "lkcountry")
	remove_vehicle_recording(012, "lkcountry")
	remove_vehicle_recording(014, "lkcountry")
	remove_vehicle_recording(015, "lkcountry")
	remove_vehicle_recording(016, "lkcountry")

	remove_vehicle_recording(helicopter[0].recording_number, "lkfbi4")
	remove_vehicle_recording(helicopter[1].recording_number, "lkfbi4")
	remove_vehicle_recording(police_car[0].recording_number, "lkfbi4") 
	remove_vehicle_recording(police_car[1].recording_number, "lkfbi4") 
	remove_vehicle_recording(police_car[2].recording_number, "lkfbi4") 
	remove_vehicle_recording(police_car[3].recording_number, "lkfbi4") 
	remove_vehicle_recording(police_car[4].recording_number, "lkfbi4") 
	remove_vehicle_recording(police_car[5].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[6].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[7].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[8].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[9].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[10].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[11].recording_number, "lkcountry")
//	remove_vehicle_recording(police_car[12].recording_number, "lkfbi4")
//	remove_vehicle_recording(police_car[13].recording_number, "lkfbi4")

	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)

	assets_cleaned_up = true	

endproc 

proc remove_fbi4_recordings()

	remove_vehicle_recording(001, "lkcountry") 

	remove_vehicle_recording(001, "lkheat") 
	remove_vehicle_recording(002, "lkheat") 
	remove_vehicle_recording(003, "lkheat") 
	remove_vehicle_recording(004, "lkheat") 
	remove_vehicle_recording(005, "lkheat") 
	remove_vehicle_recording(010, "lkheat") 
	remove_vehicle_recording(011, "lkheat") 
	remove_vehicle_recording(012, "lkheat") 
	remove_vehicle_recording(013, "lkheat") 
	remove_vehicle_recording(014, "lkheat") 
	remove_vehicle_recording(015, "lkheat") 
	remove_vehicle_recording(016, "lkheat") 
	remove_vehicle_recording(017, "lkheat") 
	remove_vehicle_recording(018, "lkheat") 
	remove_vehicle_recording(019, "lkheat") 
	remove_vehicle_recording(020, "lkheat") 
	remove_vehicle_recording(021, "lkheat") 
	remove_vehicle_recording(021, "lkheat") 
	remove_vehicle_recording(022, "lkheat") 
	remove_vehicle_recording(023, "lkheat") 
	remove_vehicle_recording(024, "lkheat") 
	remove_vehicle_recording(025, "lkheat") 
	remove_vehicle_recording(026, "lkheat") 
	remove_vehicle_recording(027, "lkheat") 
	
	remove_vehicle_recording(101, "lkheat") 
	remove_vehicle_recording(102, "lkheat") 
	
	remove_vehicle_recording(201, "lkheat") 
	remove_vehicle_recording(202, "lkheat") 
	remove_vehicle_recording(203, "lkheat") 
	remove_vehicle_recording(204, "lkheat") 
	
endproc 

proc remove_all_mission_assets()

	int i = 0
	
	clear_prints()
	clear_help()
	kill_any_conversation()

	if IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
	else 
		REMOVE_CUTSCENE()
	endif 

	while is_cutscene_active()
		wait(0)
	endwhile
	
	REMOVE_CUTSCENE()

	clear_ped_tasks_immediately(player_ped_id())
	SET_PED_USING_ACTION_MODE(player_ped_id(), false)
	
	clear_player_wanted_level(player_id())
	set_fake_wanted_level(0)
	//g_allowmaxwantedlevelcheck = true
	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(0.5)

	DISTANT_COP_CAR_SIRENS(false) 
	
	if not has_sound_finished(police_siren_sound)
		stop_sound(police_siren_sound)
	endif 
	
	DISTANT_COP_CAR_SIRENS(false) 
	
	RESET_DISPATCH_SPAWN_BLOCKING_AREAS()

	clear_mission_locate_stuff(locates_data)
	
	cleanup_uber_playback(true)
	
	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(true)
	
	end_cutscene_no_fade()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sf_binoculars)
	
	cleanup_selector_cam(sCamDetails)
	
	TRIGGER_MUSIC_EVENT("FBI4_MISSION_FAIL")
	
	stop_audio_scenes()
	
	if not has_sound_finished(police_siren_sound)
		stop_sound(police_siren_sound)
	endif 
	
	remove_ped_for_dialogue(scripted_speech[0], 0)
	remove_ped_for_dialogue(scripted_speech[0], 1)
	remove_ped_for_dialogue(scripted_speech[0], 2)
	remove_ped_for_dialogue(scripted_speech[0], 3)
	remove_ped_for_dialogue(scripted_speech[0], 4)
	remove_ped_for_dialogue(scripted_speech[0], 5)
	
	set_selector_ped_hint(selector_ped, selector_ped_michael, false)
	set_selector_ped_hint(selector_ped, selector_ped_franklin, false)
	set_selector_ped_hint(selector_ped, selector_ped_trevor, false)

	set_selector_ped_blocked(selector_ped, selector_ped_michael, false)
	set_selector_ped_blocked(selector_ped, selector_ped_franklin, false)
	set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)

	REPLAY_CANCEL_EVENT()
	
	if DOES_ENTITY_EXIST(andreas.ped)
		DELETE_PED(andreas.ped)
	endif 
	
	if DOES_ENTITY_EXIST(dave.ped)
		DELETE_PED(dave.ped)
	endif 
	
	if DOES_ENTITY_EXIST(steve.ped)
		DELETE_PED(steve.ped)
	endif
	
	if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
		DELETE_PED(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	endif 
	set_model_as_no_longer_needed(get_player_ped_model(char_franklin))
	
	if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_MICHAEL])
		DELETE_PED(selector_ped.pedID[SELECTOR_PED_MICHAEL])
	endif
	set_model_as_no_longer_needed(get_player_ped_model(char_michael))
	
	if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_TREVOR])
		DELETE_PED(selector_ped.pedID[SELECTOR_PED_TREVOR])
	endif 
	set_model_as_no_longer_needed(get_player_ped_model(char_trevor))

	if DOES_ENTITY_EXIST(army_truck_driver.ped)
		DELETE_PED(army_truck_driver.ped)
	endif 
	set_model_as_no_longer_needed(army_truck_driver.model)
	
	if DOES_ENTITY_EXIST(army_truck_passenger.ped)
		DELETE_PED(army_truck_driver.ped)
	endif 
	set_model_as_no_longer_needed(army_truck_passenger.model)

	if DOES_ENTITY_EXIST(rubbish_truck.veh)
		DELETE_VEHICLE(rubbish_truck.veh)
	endif 
	set_model_as_no_longer_needed(rubbish_truck.model)
	
	if DOES_ENTITY_EXIST(army_truck_guy.ped)
		DELETE_PED(army_truck_guy.ped)
	endif 
	set_model_as_no_longer_needed(army_truck_guy.model)
	
	if DOES_ENTITY_EXIST(army_truck_guy_2.ped)
		DELETE_PED(army_truck_guy_2.ped)
	endif 
	set_model_as_no_longer_needed(army_truck_guy_2.model)
	
	if does_entity_exist(devin.ped)
		DELETE_PED(devin.ped)
		set_model_as_no_longer_needed(devin.model)
	endif 
	
	for i = 0 to count_of(helicopter_driver) - 1
		if DOES_ENTITY_EXIST(helicopter_driver[i].ped)
			DELETE_PED(helicopter_driver[i].ped)
		endif 
		set_model_as_no_longer_needed(helicopter_driver[i].model)
	endfor 
	
	for i = 0 to count_of(police_man) - 1	
		
		DELETE_PED(police_man[i].ped)
		
		if does_blip_exist(police_man[i].blip)
			remove_blip(police_man[i].blip)
		endif 
		
		set_model_as_no_longer_needed(police_man[i].model)
		
		printstring("index: ")
		printint(i)
		printnl()
		
		//police_man[i].created = false
	endfor 
	
	for i = 0 to count_of(absail_police_man) - 1	
	
		DELETE_PED(absail_police_man[i].ped)
		
		if does_blip_exist(absail_police_man[i].blip)
			remove_blip(absail_police_man[i].blip)
		endif 
		
		set_model_as_no_longer_needed(absail_police_man[i].model)
	endfor 
	
	for i = 0 to count_of(backup_police_front) - 1
		if does_entity_exist(backup_police_front[i].ped)
			delete_ped(backup_police_front[i].ped)
		endif 
		set_model_as_no_longer_needed(backup_police_front[i].model)
	endfor 

	for i = 0 to count_of(backup_police_alley) - 1
		if does_entity_exist(backup_police_alley[i].ped)
			delete_ped(backup_police_alley[i].ped)
		endif 
		set_model_as_no_longer_needed(backup_police_alley[i].model)
	endfor 

	if does_entity_exist(rubbish_truck.veh)
		delete_vehicle(rubbish_truck.veh)
	endif
	set_model_as_no_longer_needed(rubbish_truck.model)
	
	if does_entity_exist(tow_truck.veh)
		delete_vehicle(tow_truck.veh)
	endif
	set_model_as_no_longer_needed(tow_truck.model)
	
	if does_entity_exist(trevors_car.veh)
		delete_vehicle(trevors_car.veh)
	endif 
	set_model_as_no_longer_needed(trevors_car.model)
	
	if DOES_ENTITY_EXIST(army_truck.veh)
		DELETE_VEHICLE(army_truck.veh)
	endif 
	set_model_as_no_longer_needed(army_truck.model)
	
	if does_entity_exist(devin_car.veh)
		delete_vehicle(devin_car.veh)
		set_model_as_no_longer_needed(devin_car.model)
	endif 
	
	if does_entity_exist(get_away_car.veh)
		delete_vehicle(get_away_car.veh)
		set_model_as_no_longer_needed(get_away_car.model)
	endif 
	
	if does_entity_exist(michaels_car.veh)
		delete_vehicle(michaels_car.veh)
		set_model_as_no_longer_needed(michaels_car.model)
	endif 
	
	if does_entity_exist(c4.obj)
		DELETE_OBJECT(c4.obj)
	endif 
	set_model_as_no_longer_needed(c4.model)
	
	for i = 0 to count_of(helicopter) - 1
		if DOES_ENTITY_EXIST(helicopter[i].veh)
			DELETE_VEHICLE(helicopter[i].veh)
		endif 
		set_model_as_no_longer_needed(helicopter[i].model)
	endfor 
	
	if does_entity_exist(binoculars.obj)
		delete_object(binoculars.obj)
		set_model_as_no_longer_needed(binoculars.model)
	endif 
	
	if does_entity_exist(documents.obj)
		delete_object(documents.obj)
	endif 
	set_model_as_no_longer_needed(documents.model)

	if does_blip_exist(ambulance_target_blip)
		remove_blip(ambulance_target_blip)
	endif 
	
	if does_blip_exist(player_cover_blip)
		remove_blip(player_cover_blip)
	endif 
		
	if does_blip_exist(bomb_blip)
		remove_blip(bomb_blip)
	endif
	
	remove_anim_dict("missfbi4")
	remove_anim_dict("misssagrab") 
	remove_anim_dict("missheat") 
	
	remove_fbi4_recordings()
	
	remove_ptfx_asset()
	
	cleanup_assets_after_shootout()
	
	CLEAR_TRIGGERED_LABELS()
	
	STREAMVOL_DELETE(streaming_volume)
	
	CASCADE_SHADOWS_INIT_SESSION()

endproc 

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL b_force_delete = FALSE)
	
	IF DOES_ENTITY_EXIST(veh)
		
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF IS_ENTITY_A_MISSION_ENTITY(veh) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(veh, false)

			if is_player_playing(player_id())
				
				if is_vehicle_driveable(veh) 
					
					if not is_ped_sitting_in_vehicle(player_ped_id(), veh) 

						IF b_force_delete
							DELETE_VEHICLE(veh)
						ELSE
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
						ENDIF
					
					else
					
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
					
					endif 
				
				else 
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) 
				
				endif 
			
			else 
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh) //can't delete vehicle with player in it. Fail system requires player to be in car can't use clear_ped_tasks_immediaely()
			endif 
		endif 
		
	ENDIF
	
ENDPROC

proc remove_all_vehicles()

	int i = 0

	REMOVE_VEHICLE(rubbish_truck.veh)
	
	REMOVE_VEHICLE(army_truck.veh)
	
	REMOVE_VEHICLE(tow_truck.veh)
	REMOVE_VEHICLE(devin_car.veh)
	
	for i = 0 to count_of(police_car) - 1
		REMOVE_VEHICLE(police_car[i].veh)
	endfor 
	
	for i = 0 to count_of(helicopter) - 1
		REMOVE_VEHICLE(helicopter[i].veh)
	endfor 

endproc 


proc delete_all_mission_assets()

	int i = 0

	clear_area(<<1376.45, -2083.29, 55.51>>, 10000.00, true)
	
	//clear_ped_tasks_immediately(player_ped_id())
	
	if does_entity_exist(army_truck_driver.ped)
		delete_ped(army_truck_driver.ped)
	endif 
	
	if does_entity_exist(army_truck_passenger.ped)
		delete_ped(army_truck_passenger.ped)
	endif 
	
	if does_entity_exist(army_truck_guy.ped)
		delete_ped(army_truck_guy.ped)
	endif 
	
	if does_entity_exist(army_truck_guy_2.ped)
		delete_ped(army_truck_guy_2.ped)
	endif 
	
	for i = 0 to count_of(police_man) - 1
		if does_entity_exist(police_man[i].ped)
			delete_ped(police_man[i].ped)
		endif 
	endfor 
	
	for i = 0 to count_of(absail_police_man) - 1
		if does_entity_exist(absail_police_man[i].ped)
			delete_ped(absail_police_man[i].ped)
		endif 
	endfor 
	
	for i = 0 to count_of(helicopter_driver) - 1
		if does_entity_exist(helicopter_driver[i].ped)
			delete_ped(helicopter_driver[i].ped)
		endif 
	endfor 
	
	
	if does_entity_exist(devin.ped)
		delete_ped(devin.ped)
	endif 
	
	if does_entity_exist(dave.ped)
		delete_ped(dave.ped)
	endif 
	
	if does_entity_exist(steve.ped)
		delete_ped(steve.ped)
	endif 
	
	if does_entity_exist(andreas.ped)
		delete_ped(andreas.ped)
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		delete_ped(selector_ped.pedID[selector_ped_michael])
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
		delete_ped(selector_ped.pedID[selector_ped_franklin])
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		delete_ped(selector_ped.pedID[selector_ped_trevor])
	endif 
	
	remove_all_vehicles()

	if does_entity_exist(c4.obj)
		delete_object(c4.obj)
	endif 
	
	for i = 0 to count_of(rope) - 1
		if DOES_ROPE_EXIST(rope[i].rope)
			delete_rope(rope[i].rope)
		endif 
	endfor

endproc 


proc mission_cleanup()

	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4)

	kill_any_conversation()
	
	if is_player_playing(player_id())
		SET_PED_USING_ACTION_MODE(player_ped_id(), false)
		SET_ALL_RANDOM_PEDS_FLEE(player_id(), false)
	endif 

	remove_ped_for_dialogue(scripted_speech[0], 0)
	remove_ped_for_dialogue(scripted_speech[0], 1)
	remove_ped_for_dialogue(scripted_speech[0], 2)
	remove_ped_for_dialogue(scripted_speech[0], 3)
	remove_ped_for_dialogue(scripted_speech[0], 4)
	
//	SET_PLAYER_PED_STORED_HEALTH(char_michael, 200)
//	SET_PLAYER_PED_STORED_HEALTH(char_trevor, 200)
//	SET_PLAYER_PED_STORED_HEALTH(char_franklin, 200)
	
//	if not g_bmagdemoactive
//		if restore_player_variations
//			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
//		endif 
//		RESTORE_PLAYER_PED_VARIATIONS(selector_ped.pedID[SELECTOR_PED_michael])
//		RESTORE_PLAYER_PED_VARIATIONS(selector_ped.pedID[SELECTOR_PED_franklin])
//		RESTORE_PLAYER_PED_VARIATIONS(selector_ped.pedID[SELECTOR_PED_trevor])
//	endif 
	
	//fix for bug 1542276 stop replay slow down being reset.
	if not IS_REPLAY_BEING_PROCESSED()
		set_time_scale(1.0)
	endif 
	
	set_fake_wanted_level(0)
	//g_allowmaxwantedlevelcheck = true
	set_max_wanted_level(6)
	set_create_random_cops(true)
	set_wanted_level_multiplier(1.0)
	
	END_SRL()
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	DISTANT_COP_CAR_SIRENS(false) 
	
	if not has_sound_finished(police_siren_sound)
		stop_sound(police_siren_sound)
	endif 
	
	RESET_DISPATCH_SPAWN_BLOCKING_AREAS()
	
	remove_scenario_blocking_areas()
	
	CLEAR_PED_NON_CREATION_AREA()
	
	SET_DISTANT_CARS_ENABLED(true)
	
//	set_roads_in_area(<<913.91, -2457.88, -100.00>>, <<914.07, -2090.31, 100.00>>, true)
//	set_ped_paths_in_area(<<913.91, -2457.88, -100.00>>, <<914.07, -2090.31, 100.00>>, true)
//	
//	set_roads_in_area(<<1078.61, -2494.14, -100.00>>, <<809, -2048, 100.00>>, true)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<945.8, -2080.8, 100.00>>, <<1015.3, -2035.2, 32.3>>, true) //near rubbish truck in layby
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<924.63, -2338.35, 100.00>>, <<906.44, -2371.57, -100.00>>, true)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, true)

	CLEAR_WEATHER_TYPE_PERSIST()
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)

	REMOVE_WAYPOINT_RECORDING("heat1")
	
	if ! (sf_binoculars = null)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sf_binoculars)
	endif
	
	STREAMVOL_DELETE(streaming_volume)
	
	TRIGGER_MUSIC_EVENT("FBI4_MISSION_FAIL")

	END_SRL()
	
	terminate_this_thread()

endproc 

proc mission_passed()

	Mission_Flow_Mission_Passed()
	AWARD_ACHIEVEMENT_FOR_MISSION(ACH42) // Blizted
	
	clear_player_wanted_level(player_id())
	
	//set_vehicle_density_multiplier(1.0)
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
	
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MISSION_VEH_FBI4_PREP, FALSE) //cleans up the get away car
	
	if switched_to_michael
	and switched_to_franklin
	and switched_to_trevor
		//INFORM_STAT_FBI_FOUR_ALL_USED_IN_SHOOTOUT()
	endif 
	
	mission_cleanup()

endproc 

proc mission_failed()

	RESTORE_PLAYER_PED_VARIATIONS(player_ped_id())
	
	delete_all_mission_assets()
	
	clear_area(<<1391.53, -2063.67, 52.79>>, 10000, true)
	
	SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_NORMAL, true)
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	

	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
endproc 

func bool ram_truck_area_fail_system()
	
	if is_ped_sitting_in_vehicle(player_ped_id(), tow_truck.veh)
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<875.867, -2347.926, 27.521>>, <<925.899, -2352.039, 35.521>>, 190.8) 
			if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<913.410, -2369.405, 27.550>>, <<914.169, -2360.739, 33.550>>, 22.9)   //<<907.714, -2366.854, 29.559>>, <<908.319, -2360.684, 33.059>>, 6.0)    

				if not start_ram_time_2
					ram_time_2 = get_game_timer()
					start_ram_time_2 = true 
				else 
					if lk_timer(ram_time_2, 5000)
						return true 
					endif 
				endif 
			
			else 
			
//				printfloat(GET_ENTITY_SPEED(tow_truck.veh))
//				printnl()
				
				//if GET_ENTITY_SPEED(tow_truck.veh) < x
					if not start_ram_time_3
						ram_time_3 = get_game_timer()
						start_ram_time_3 = true 
					else 
						if lk_timer(ram_time_3, 5000)
							return true 
						endif 
					endif 
				//endif 
				
			endif 
			
		endif 
	
	else 
	
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<913.410, -2369.405, 27.550>>, <<914.169, -2360.739, 33.550>>, 22.9)   //<<907.714, -2366.854, 29.559>>, <<908.319, -2360.684, 33.059>>, 6.0)    
			return true 
		endif 
		
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<875.867, -2347.926, 27.521>>, <<925.899, -2352.039, 35.521>>, 190.8) 
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(army_truck.veh)) < 7.0
				return true 
			endif 
		endif 
	
	endif 

	return false
	
endfunc 

func bool ram_truck_cover_blown_system()

	if lk_timer(truck_ram_time, 42000)
	or ram_truck_area_fail_system()
	or is_player_wanted_level_greater(player_id(), 0)
		return true 
	endif 
	
	if is_ped_sitting_in_vehicle(player_ped_id(), tow_truck.veh)
			
		if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<907.426, -2371.301, 29.550>>, <<908.093, -2362.426, 33.550>>, 6.0) //5.8
			
			if has_ped_been_harmed(army_truck_driver.ped, army_truck_driver.health, army_truck_driver.created)
			or has_ped_been_harmed(army_truck_passenger.ped, army_truck_passenger.health, army_truck_passenger.created)
			or has_vehicle_been_harmed(army_truck.veh, army_truck.health)
				return true 
			endif 
		
		else 
		
			printstring("IN AREA")
			printnl()

			if (GET_ENTITY_HEADING(tow_truck.veh) > 45 and GET_ENTITY_HEADING(tow_truck.veh) < 135)
				
				if IS_ENTITY_TOUCHING_ENTITY(tow_truck.veh, army_truck.veh)
					
					if get_entity_speed(tow_truck.veh) < 5.0
					
						return true
						
					endif 

				endif 
				
			else 
				
				if has_ped_been_harmed(army_truck_driver.ped, army_truck_driver.health, army_truck_driver.created)
					printstring("fail test 0")
					printnl()
				endif 
				
				if has_ped_been_harmed(army_truck_passenger.ped, army_truck_passenger.health, army_truck_passenger.created)
					printstring("fail test 1")
					printnl()
				endif 
				
				if has_vehicle_been_harmed(army_truck.veh, army_truck.health)
					printstring("fail test 2")
					printnl()
				endif 

				if has_ped_been_harmed(army_truck_driver.ped, army_truck_driver.health, army_truck_driver.created)
				or has_ped_been_harmed(army_truck_passenger.ped, army_truck_passenger.health, army_truck_passenger.created)
				or has_vehicle_been_harmed(army_truck.veh, army_truck.health)
					return true 
				endif 
				
			endif 

		endif 
		
	else 
	
		if has_ped_been_harmed(army_truck_driver.ped, army_truck_driver.health, army_truck_driver.created)
			printstring("fail test 3")
			printnl()
		endif 
		
		if has_ped_been_harmed(army_truck_passenger.ped, army_truck_passenger.health, army_truck_passenger.created)
			printstring("fail test 4")
			printnl()
		endif 
		
		if has_vehicle_been_harmed(army_truck.veh, army_truck.health)
			printstring("fail test 5")
			printnl()
		endif 
		
		if has_ped_been_harmed(army_truck_driver.ped, army_truck_driver.health, army_truck_driver.created)
		or has_ped_been_harmed(army_truck_passenger.ped, army_truck_passenger.health, army_truck_passenger.created)
		or has_vehicle_been_harmed(army_truck.veh, army_truck.health)
			return true 
		endif 
	
	endif 
	
	return false 
	
endfunc 
					
proc setup_security_guards_to_attack()

	int i 
	
	for i = 0 to count_of(security_guard) - 1
		if not is_ped_injured(security_guard[i].ped)
		
			set_blocking_of_non_temporary_events(security_guard[i].ped, false)
			set_ped_sphere_defensive_area(security_guard[i].ped, security_guard[i].run_to_pos, 2.0) 
			SET_PED_COMBAT_ATTRIBUTES(security_guard[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(security_guard[i].ped, CA_CAN_CHARGE, TRUE)
			task_combat_hated_targets_around_ped(security_guard[i].ped, 200.00)
			
		endif 
	endfor 
	
endproc 

func bool devins_house_area_fail_system()
				
	if is_entity_in_angled_area(player_ped_id(), <<-2560.765, 1899.763, 166.057>>, <<-2553.950, 1898.684, 171.157>>, 15.9) 
	or is_entity_in_angled_area(player_ped_id(), <<-2558.312, 1900.146, 166.870>>, <<-2604.425, 1926.876, 174.5>>, 16.2) 
	or is_entity_in_angled_area(player_ped_id(), <<-2559.274, 1911.036, 166.870>>, <<-2604.942, 1934.406, 174.5>>, 24.1)  
	
		return true
	
	endif
	
	return false 

endfunc 

func bool mission_fail_checks()

	switch mission_flow
	
		case intro_mocap
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				mission_failed_text = "cntry_fail7"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "cntry_fail8"
				return true
			endif 
		
			if mission_vehicle_injured(trevors_car.veh)
				mission_failed_text = "cntry_fail19" 
				return true
			endif 
			
			if mission_vehicle_injured(rubbish_truck.veh)
				mission_failed_text = "cntry_fail3" 
				return true
			endif 
			
			if mission_vehicle_injured(tow_truck.veh)
				mission_failed_text = "cntry_fail19" 
				return true
			endif

			
			if get_current_player_ped_enum() = char_franklin
				
				if does_entity_exist(selector_ped.pedID[SELECTOR_PED_MICHAEL])
					if has_entity_been_damaged_by_entity(selector_ped.pedID[SELECTOR_PED_MICHAEL], player_ped_id())
						mission_failed_text = "cntry_fail18"
						return true
					endif 
				endif 
				
				if does_entity_exist(selector_ped.pedID[SELECTOR_PED_TREVOR])
					if has_entity_been_damaged_by_entity(selector_ped.pedID[SELECTOR_PED_TREVOR], player_ped_id())
						mission_failed_text = "cntry_fail18"
						return true
					endif 
				endif 
				
			elif get_current_player_ped_enum() = char_trevor
				if does_entity_exist(selector_ped.pedID[SELECTOR_PED_MICHAEL])
					if has_entity_been_damaged_by_entity(selector_ped.pedID[SELECTOR_PED_MICHAEL], player_ped_id())
						mission_failed_text = "cntry_fail18"
						return true
					endif 
				endif 
			endif 
		
			if mission_vehicle_injured(rubbish_truck.veh)
				mission_failed_text = "cntry_fail3"
				return true
			endif 
	
			if mission_vehicle_injured(tow_truck.veh)
				mission_failed_text = "cntry_fail4"
				return true
			endif 
		
		break 
		
		case mag_demo_2_setup
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
			endif 
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
			endif 
		
			if mission_vehicle_injured(trevors_car.veh)
			endif 
			
			if mission_vehicle_injured(rubbish_truck.veh)
			endif 
			
			if mission_vehicle_injured(tow_truck.veh)
			endif
		
		break 

		case get_ambulance_into_pos
		
			if mission_vehicle_injured(rubbish_truck.veh)
				mission_failed_text = "cntry_fail3" 
				return true
			endif 
			
			if is_vehicle_stuck_every_check(rubbish_truck.veh)
				mission_failed_text = "cntry_fail12"
				return true
			endif 
			
			if mission_vehicle_injured(tow_truck.veh)
				mission_failed_text = "cntry_fail4"
				return true
			endif 
			
			if mission_vehicle_injured(trevors_car.veh)
				mission_failed_text = "cntry_fail19"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				mission_failed_text = "cntry_fail7"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "cntry_fail8"
				return true
			endif 
			
//			if not is_playback_going_on_for_vehicle(trevors_car.veh)
//				if not is_entity_at_coord(trevors_car.veh, get_position_of_vehicle_recording_at_time(101, get_total_duration_of_vehicle_recording(101, "lkheat"), "lkheat"), <<4.0, 4.0, 4.0>>)
//					mission_failed_text = "cntry_fail6"
//					return true
//				endif 
//			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(rubbish_truck.veh)) > 200
				mission_failed_text = "cntry_fail22"
				return true 
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<972.8909, -2073.6919, 30.50>>) > 500
				mission_failed_text = "cntry_fail10"
				return true 
			endif 
			
		break 
	
		case vehicle_spotted
		
			if mission_vehicle_injured(army_truck.veh)
				mission_failed_text = "cntry_fail2"
				return true
			endif 
			
			if mission_ped_injured(army_truck_driver.ped)
				mission_failed_text = "cntry_fail0"
				return true
			endif 
			
			if mission_ped_injured(army_truck_passenger.ped)
				mission_failed_text = "cntry_fail1"
				return true
			endif 
			
			if mission_ped_injured(factory_worker)
				mission_failed_text = "cntry_fail6"
				return true
			endif 
		
			if mission_vehicle_injured(rubbish_truck.veh)
				mission_failed_text = "cntry_fail3"
				return true
			endif 
			
//			if get_current_player_ped_enum() = char_trevor
//				
//				vector player_pos
//				
//				player_pos = get_entity_coords(player_ped_id())
//				
//				if player_pos.z < 54.00
////				if (not is_entity_in_angled_area(player_ped_id(), <<798.208, -2320.561, 59.101>>, <<796.885, -2335.101, 64.601>>, 7.5) 
////				and player_pos.z < 54.00)
//					
//					mission_failed_text = "cntry_fail9"
//					
//					return true
//					
//				endif 
//				
//			endif 
			
			if get_player_wanted_level(player_id()) > 0
				mission_failed_text =  "cntry_fail6"
				return true
			endif 

		break 
		
		case park_ambulance
		
			if mission_vehicle_injured(rubbish_truck.veh)
				mission_failed_text = "cntry_fail3"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				mission_failed_text = "cntry_fail7"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if mission_vehicle_injured(army_truck.veh)
				mission_failed_text = "cntry_fail2"
				return true
			endif 
			
			if is_vehicle_stuck_every_check(rubbish_truck.veh)
				mission_failed_text = "cntry_fail12"
				return true
			endif 
			
			if mission_vehicle_injured(tow_truck.veh)
				mission_failed_text = "cntry_fail4"
				return true
			endif 
			
			if get_player_wanted_level(player_id()) > 0
				mission_failed_text =  "cntry_fail6"
				return true
			endif 
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), <<972.8909, -2073.6919, 30.50>>) > 500
				mission_failed_text = "cntry_fail10"
				return true 
			endif 
			
		break 
		
		case ram_army_truck
		
			if mission_vehicle_injured(rubbish_truck.veh)
				mission_failed_text = "cntry_fail3"
				return true
			endif 
			
			if mission_vehicle_injured(tow_truck.veh)
				mission_failed_text = "cntry_fail4"
				return true
			endif 
		
			if mission_vehicle_injured(army_truck.veh)
				mission_failed_text = "cntry_fail2"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if is_vehicle_stuck_every_check(tow_truck.veh)
				mission_failed_text = "cntry_fail20"
				return true
			endif 
			
			if (get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), GET_ENTITY_COORDS(army_truck.veh)) > 150.00)
				mission_failed_text = "cntry_fail11"
				return true
			endif 

			if ram_army_truck_status = 0

				if ram_truck_cover_blown_system()
				
					printstring("ram_truck_cover_blown_system")
					printnl()
					
					if not is_ped_injured(army_truck_driver.ped)
						if is_ped_sitting_in_vehicle(army_truck_driver.ped, army_truck.veh)
							
							open_sequence_task(seq)
								task_vehicle_drive_wander(null, army_truck.veh, 20.00, drivingmode_ploughthrough)
							close_sequence_task(seq)
							task_perform_sequence(army_truck_driver.ped, seq)
							clear_sequence_task(seq)

						endif 
						
					else 
					
						if not is_ped_injured(army_truck_passenger.ped)
							open_sequence_task(seq)
								task_smart_flee_ped(null, player_ped_id(), 200.00, -1)
							close_sequence_task(seq)
							task_perform_sequence(army_truck_driver.ped, seq)
							clear_sequence_task(seq)
						endif 
					
					endif 
					
					clear_prints()
					mission_failed_text = "cntry_fail6"
					
					set_player_wanted_level(player_id(), 3) 
					
					original_time = get_game_timer()
					
					mission_flow = cover_blown
					
					return true 
				endif 
			endif 
			
			if get_player_wanted_level(player_id()) > 0
				mission_failed_text =  "cntry_fail6"
				return true
			endif 
		
		break 
		
		case blow_open_truck_doors
		
			if mission_vehicle_injured(army_truck.veh)
				mission_failed_text = "cntry_fail2"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				mission_failed_text = "cntry_fail7"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "cntry_fail8"
				return true
			endif
			
			if get_player_wanted_level(player_id()) > 0
				mission_failed_text =  "cntry_fail6"
				return true
			endif 
			
			if c4_explosion_cutscene_status = 0
				//if lk_timer(plant_bomb_time, 90000)
				if lk_timer(plant_bomb_time, 90000000)
					mission_failed_text = "cntry_fail26"
					DISTANT_COP_CAR_SIRENS(true)
					PLAY_SOUND_FROM_coord(police_siren_sound, "Distant_Sirens", <<913.6, -2301.6, 34.00>>)//<<901.4, -2335.2, 33.7>>)//<<910.8, -2300, 30.6>>)
					apply_fail_time = true
					return true 
				endif 
			endif 
				
			
//			if has_ped_got_no_ammo_in_weapon(WEAPONTYPE_STICKYBOMB)
//				printstring("not got sticky bomb")
//				printnl()
//			else 
//				printstring("got sticky bomb")
//				printnl()
//			endif 
//			
//			
//			if not is_projectile_type_in_angled_area(<<891.202, -2361.976, 28.324>>, <<888.773, -2361.656, 31.824>>, 1.0, weapontype_stickybomb)
//				printstring("not projectile in area")
//				printnl()
//			else 
//				printstring("projectile in area")
//				printnl()
//			endif 
//			
//			if c4_planted_manually = true	
//				printstring("c4_planted_manually")
//				printnl()
//			else 
//				printstring("not c4_planted_manually")
//				printnl()
//			endif 
		
			

			if has_ped_got_no_ammo_in_weapon(WEAPONTYPE_STICKYBOMB)
			and not IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<891.034, -2362.032, 29.282>>, <<888.756, -2361.720, 31.622>>, 1.1, weapontype_stickybomb)
			and not c4_planted_manually 	
			
				if lk_timer(weapon_fail_time, 2500)
				
					mission_failed_text = "cntry_god22"
				
					return true
					
				endif 
				
			else 
			
				weapon_fail_time = get_game_timer()
			
			endif 
			
			if area_fail
				//mission_failed_text  inside area_system()
				return true 
			endif 
		
		break 
		
		case shootout
		case switch_to_michael_or_franklin
		
			if not scamdetails.bSplineActive
			
				if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
					mission_failed_text = "cntry_fail5"
					return true
				endif 
				
				if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
					mission_failed_text = "cntry_fail7"
					return true
				endif 
				
				if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
					mission_failed_text = "cntry_fail8"
					return true
				endif 
				
				if area_fail
					//mission_failed_text  inside area_system()
					return true 
				endif 
				
			endif 
		
		break 
		
		case burn_rubbish_truck

			if does_entity_exist(rubbish_truck.veh)
				if is_entity_dead(rubbish_truck.veh)
				
					if not burn_rubbish_truck_distance_saftey_check_active 
						if not is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<50.00, 50.00, 50.00>>, false, false)
							mission_failed_text = "cntry_fail25"
							return true
						endif 
						
					else 
			
						if not is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<110.00, 110.00, 110.00>>, false, false)
							mission_failed_text = "cntry_fail25"
							return true
						endif

					endif 
				endif 

			endif 
			
			
			if not does_blip_exist(rubbish_truck.blip)
				if is_vehicle_stuck_every_check(rubbish_truck.veh)
					mission_failed_text = "cntry_fail24"
					return true
				endif 
			endif 
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				mission_failed_text = "cntry_fail7"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "cntry_fail8"
				return true
			endif 
			
			if is_vehicle_driveable(rubbish_truck.veh)
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(rubbish_truck.veh)) > 200
					mission_failed_text = "cntry_fail22"
					return true 
				endif 
			endif 
		
		break 
		
		case meet_devin
		
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				mission_failed_text = "cntry_fail5"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				mission_failed_text = "cntry_fail7"
				return true
			endif 
			
			if mission_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				mission_failed_text = "cntry_fail8"
				return true
			endif 
			
			if mission_ped_injured(devin.ped)
				mission_failed_text = "cntry_fail13"
				setup_security_guards_to_attack()
				return true
			endif 
			
			if mission_vehicle_injured(devin_car.veh)
				mission_failed_text = "cntry_fail14"
				setup_security_guards_to_attack()
				return true
			endif
			
			if attacked_lesters_security_fail
				mission_failed_text = "cntry_fail18"
				return true 
			endif 

			if does_entity_exist(devin.ped) and does_entity_exist(devin_car.veh)
			
				if has_ped_been_harmed(devin.ped, devin.health, devin.created)
					printstring("fail test 0")
					printnl()
				endif 
				
				if is_explosion_in_sphere(exp_tag_dontcare, get_entity_coords(devin.ped), 15.00)
					printstring("fail test 1")
					printnl()
				endif 
				
				if not is_entity_at_coord(devin_car.veh, get_entity_coords(devin_car.veh), <<2.0, 2.0, 2.0>>)
					printstring("fail test 2")
					printnl()
				endif 
				
				if has_entity_been_damaged_by_entity(devin_car.veh, player_ped_id())
					printstring("fail test 3")
					printnl()
				endif 
				
				if devins_house_area_fail_system()
					printstring("fail test 4")
					printnl()
				endif 

				if has_ped_been_harmed(devin.ped, devin.health, devin.created)
				or is_explosion_in_sphere(exp_tag_dontcare, get_entity_coords(devin.ped), 15.00)
				or is_bullet_in_area(get_entity_coords(devin.ped), 4.0)
				or not is_entity_at_coord(devin_car.veh, get_entity_coords(devin_car.veh), <<2.0, 2.0, 2.0>>)
				or has_entity_been_damaged_by_entity(devin_car.veh, player_ped_id())
				or devins_house_area_fail_system()

					setup_security_guards_to_attack()
					
					open_sequence_task(seq)
						task_smart_flee_ped(null, player_ped_id(), 200, -1)
					close_sequence_task(seq)
					task_perform_sequence(devin.ped, seq)
					clear_sequence_task(seq)
					
					mission_failed_text = "cntry_fail18"
					
					return true
				endif 

				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(devin.ped)) < 30
					if get_player_wanted_level(player_id()) > 0
						mission_failed_text = "cntry_fail18"
						return true
					endif 
				endif 
		
			endif 

		break 
		
		case pass_cutscene
	
			if mission_ped_injured(devin.ped)
				mission_failed_text = "cntry_fail13"
				return true
			endif 
			
			if mission_vehicle_injured(devin_car.veh)
				mission_failed_text = "cntry_fail14"
				return true
			endif 
			
		break 

	endswitch 
	
	return false
	
endfunc 


proc initialise_mission_variables()

	int i = 0

	#IF IS_DEBUG_BUILD
		menu_stage_selector[0].sTxtLabel = "Enter rubbish truck at FIB depot"
		menu_stage_selector[1].sTxtLabel = "Trevor on rooftop"     
		menu_stage_selector[2].sTxtLabel = "Ram money truck"
		menu_stage_selector[3].sTxtLabel = "Detonate C4" 
		menu_stage_selector[4].sTxtLabel = "shootout"  
		menu_stage_selector[5].sTxtLabel = "Burn rubbish truck"
		menu_stage_selector[6].sTxtLabel = "meet devin - FBI_4_MCS_2" 
	#endif 

	start_ram_time_2 = false
	start_ram_time_3 = false
	
	stop_mission_fail_checks = false
	help_text_rendered = false
	allow_hotswap = false
	player_in_area = false
	outside_area_text_printed = false
	start_ram_time_2 = false 
	start_ram_time_3 = false
	switch_reminder = false
	assets_cleaned_up = false
	//drive_to_end_vehicle_selected = false
	force_applied_to_helicopter = false
	tow_truck_switched_to_ai = false
	player_told_to_get_into_cover = false
	scale_form_movie_active = false
	area_fail = false
	standing_away_from_c4 = false
	c4_planted_manually = false
	hover_backwards = false
	apply_fail_time = false
	rubbish_truck_out_of_burning_area = false
	boiler_suits_applied = false
	create_debug_getaway_car = false
//	police_man_8_killed_by_trevor = false 
//	police_man_9_killed_by_trevor = false
//	police_man_31_killed_by_trevor = false
	player_in_cover_down_alley = false
	setup_rubbish_truck_for_switch = false
	mag_demo_cover_set = false
	clenaup_fake_wanted_level = false
	blip_truck_on_leaving_on_first_occasion = false
	wave_1_police_created = false
	wave_2_police_created = false
	wave_3_police_created = false
	wave_5_police_created = false
	preload_outfits = false
	brake_truck_if_player_gets_close_to_get_away_vehicle = false
	burn_rubbish_truck_distance_saftey_check_active = false
	attacked_lesters_security_fail = false
	sound_army_horn = false
	snipers_in_position = false
	wave_2_front_dialogue_triggered = false
	trigger_switch_effect_to_michael = false
	fbi_4_binoculars_srl_requested = false
	apply_money_truck_roof_damage = false
	sniper_killed_by_trevor = false
	trevor_binoculars_hit_out_0 = false
	trevor_binoculars_hit_out_1 = false
	trevor_binoculars_hit_out_2 = false
	trevor_binoculars_hit_out_3 = false
	trevor_binoculars_transition_0 = false
	
	
 	fbi4_vehicle_spotted_status = 0
	park_ambulance_status = 0
	ambulance_cutscene_status = 0
	ram_army_truck_status = 0
	ram_army_truck_cutscene_status = 0
	michael_ai_system_status = 0
	blow_open_truck_doors_status = 0
	hot_swap_system_status = 0
	factory_worker_0_system_status = 0
	army_truck_driver_ai_status = 0
	army_truck_passenger_ai_status = 0
	surveillance_status = 0
	dialogue_status= 0
	smashing_wall_rayfire_status = 0
	rope_system_status = 0
	army_truck_guy_ai_status = 0
	army_truck_guy_ai_2_status = 0
	helicopter_speed_system_status = 0
	devin_ai_system_status = 0
	fbi4_get_ambulance_into_pos_status = 0
	shootout_master_controler_status = 0
	fbi4_pass_cutscene_status = 0
	c4_explosion_cutscene_status = 0
	switch_dialogue_status = 0
	fbi4_intro_mocap_status = 0
	blow_open_truck_doors_dialogue_status = 0
	end_of_shootout_dialogue_status = 0
	get_ambulance_into_position_dialogue_status = 0
	police_vehicle_explosion_system_status = 0
	trevor_ai_system_status = 0
	cops_killed_during_shootout = 0
	original_cops_killed = 0
	shootout_wanted_level_system_status = 0
	shootout_music_event_system_status = 0
	uber_speed_system_status = 0
	update_dispatch_pos_status = 0
	dispatch_spawn_position_system_status = 0
	petrol_can_help_system_status = 0
	burn_rubbish_truck_master_flow_system_status = 0
	fire_system_status = 0
	burn_rubbish_truck_music_event_system_status = 0
	debug_intro_mocap_screen_status = 0
	park_time = 0
	plant_bomb_time = 0
	fail_time = 0
	switch_to_michael_or_franklin_status = 0
	phone_call_to_michael_status = 0
	switch_to_michael_status = 0
	layby_cutscene_status = 0
	horn_time = 0
	meet_devin_master_flow_status = 0
	snipers_dialogue_system_status = 0
	police_vehicle_explosion_system_status = 0
	sniper_dialogue_time = 0
	police_man_8_z_offset_time = 0
	police_man_9_z_offset_time = 0
	fbi4_mag_demo_2_setup_status = 0
	michael_defensive_sphere_system_status = 0
	franklin_defensive_sphere_system_status = 0
	franklin_ai_system_2_status = 0
	backup_police_front_counter = 0
	backup_police_alley_counter = 0
	total_number_of_points = 0
	backup_police_front_spawn_system_status = 0
	backup_police_alley_spawn_system_status = 0
	wave_0_alive_count = 0
	helicopter_1_sysstem_status = 0
	mag_demo_2_end_status = 0
	helicopter_0_system_status = 0
	michael_ai_system_2_status = 0
	police_vehicle_siren_system_status = 0
	apply_damage_time = 0
	health_reduction_time = 0
	force_ped_to_switch_damage_hint_system_status = 0
	left_alley_dialogue_system_status = 0
	flee_time = 0
	audio_explosion_system_status = 0
	wave_1_timer_0 = 0
	wave_1_timer_1 = 0
	cop_dead_stat_time = 0
	total_number_of_wave_2_peds = 0
	total_number_of_wave_4_peds = 0
	global_backup_police_front_dead_count = 0
	global_backup_police_alley_dead_count = 0
	weapon_fail_time = 0
	get_away_car_time = 0
	blow_open_struck_doors_audio_scene_system_status = 0
	burn_rubbish_truck_audio_scene_system_status = 0
	health_dialogue_time = 0
	wave_2_dialogue_system_status = 0
	wave_4_dialogue_system_status = 0
	wave_5_dialogue_system_status = 0
	total_police_men_alive_count = 0
	create_wave_1_police_status = 0
	create_wave_2_police_status = 0
	michael_special_ability_time = 0
	burn_truck_time = 0
	switch_to_michael_vehicle_recording_controller_status = 0
	take_off_mask_anim_system_status = 0
	switch_to_franklin_time = 0
	trevor_ai_system_2_status = 0
	switch_sound_time = 0

	hover_speed = 1.0
	police_man_8_z_offset = 1.0
	police_man_9_z_offset = 1.0
	
	set_selector_ped_blocked(selector_ped, selector_ped_michael, false)
	set_selector_ped_blocked(selector_ped, selector_ped_franklin, false)
	set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)
	
	players_vehicle_cam_mode = cam_view_mode_third_person_medium
	
	eSwitchCamState = switch_cam_idle
	
	#IF IS_DEBUG_BUILD
		get_ambulance_into_pos_skip_status = 0
		//shootout_skip_status = 0
	#endif 
	
	michael_run_to_pos = <<881.0168, -2333.9124, 33.9075>> 
	
	dave.model = get_npc_ped_model(char_dave)
	dave.pos = <<1392.2704, -2071.4844, 51.0172>>
	dave.health = 200
	
	steve.model = get_npc_ped_model(char_steve)
	steve.pos = <<1393.0402, -2069.2317, 51.0114>>
	steve.health = 200
	
	andreas.model = get_npc_ped_model(char_andreas)//a_m_y_business_02//get_npc_ped_model(char_andreas) IG Andreas
	andreas.pos = <<1390.1227, -2070.5527, 51.0074>>
	andreas.weapon = weapontype_unarmed
	andreas.health = 200
	
	trevors_car.model = get_player_veh_model(char_trevor)
	
	rubbish_truck.model = trash 
	rubbish_truck.pos = <<959.5624, -2071.3081, 29.6226>>
	rubbish_truck.heading = 87.4847 

	tow_truck.model = towtruck
	tow_truck.pos = <<1022.3509, -2376.3162, 29.5306>>//<<1016.1619, -2375.7366, 29.5306>> 
    tow_truck.heading = 85.1557

	army_truck_driver.model = S_M_M_Armoured_01
	
	army_truck_passenger.model = S_M_M_Armoured_01
	
	army_truck_guy.pos = <<855.28, -2374.2, 43.26>>   
	army_truck_guy.heading = 7.2
	army_truck_guy.model = S_M_M_Armoured_01
	army_truck_guy.health = 200
	army_truck_guy.name = "army guy"
	
	army_truck_guy_2.pos = <<855.28, -2374.2, 44.26>>   
	army_truck_guy_2.heading = 7.2
	army_truck_guy_2.model = S_M_M_Armoured_01
	army_truck_guy_2.health = 200
	army_truck_guy_2.name = "army guy 2"

	army_truck.model = stockade
	army_truck.pos = <<736.8857, -2730.5625, 6.3323>>   
	army_truck.heading = 0.3915
	army_truck.health = 1000
	
	michaels_car.model = get_player_veh_model(char_michael)
	michaels_car.pos = <<-823.5428, 181.3025, 70.6662>>
	michaels_car.heading = 142.8150  
	
	traffic_vehicle[0].pos = <<736.4277, -2347.0225, 24.0021>>
	traffic_vehicle[0].model = stanier
	traffic_vehicle[0].recording_number = 201
	
	traffic_vehicle[1].pos = <<730.5193, -2405.8359, 20.1767>>
	traffic_vehicle[1].model = stanier
	traffic_vehicle[1].recording_number = 202
	
	traffic_vehicle[2].pos = <<639.3552, -2504.7156, 17.4360>>
	traffic_vehicle[2].model = stanier
	traffic_vehicle[2].recording_number = 203
	
	traffic_vehicle[3].pos = <<682.7643, -2493.1550, 19.7100>>
	traffic_vehicle[3].model = stanier
	traffic_vehicle[3].recording_number = 204
	
	traffic_vehicle[4].pos = <<736.4277, -2347.0225, 24.0021>>
	traffic_vehicle[4].model = stanier
	traffic_vehicle[4].recording_number = 201
	
	traffic_vehicle[5].pos = <<730.5193, -2405.8359, 20.1767>>
	traffic_vehicle[5].model = stanier
	traffic_vehicle[5].recording_number = 201//202
	
	traffic_vehicle[6].pos = <<639.3552, -2504.7156, 17.4360>>
	traffic_vehicle[6].model = stanier
	traffic_vehicle[6].recording_number = 203
	
	traffic_vehicle[7].pos = <<682.7643, -2493.1550, 19.7100>>
	traffic_vehicle[7].model = stanier
	traffic_vehicle[7].recording_number = 204

	ambulance_target_pos = <<906.2, -2375.5010, 30.5320>> 


	c4.model = GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB)  //prop_c4_final //GET_WEAPONTYPE_MODEL(WEAPONTYPE_STICKYBOMB)  
	c4.pos = <<872, -2369, 36.72>>
	c4.health = 1000
	
	binoculars.model = Prop_Binoc_01
	
//	skip[0].model = Prop_Skip_02a
//	skip[0].pos = <<893.63, -2332.52, 30.63>>
//	skip[0].rot = <<0.0, 0.0, 0.0>>
//	
//	skip[1].model = Prop_Skip_02a
//	skip[1].pos = <<888.5, -2336.28, 30.3>>
//	skip[1].rot = <<0.0, 0.0, 0.0>>

	documents.model = Prop_CS_Envolope_01
	documents.pos = <<890.16370, -2363.36255, 29.77>>
	documents.rot = <<0.0, 0.0, 0.0>>

	van_light.model = Prop_securityVAN_lightrig
	van_light.pos = <<890.365, -2367.289, 28.104>>
	van_light.rot = <<0.0, 0.0, -0.11>>

	for i = 0 to count_of(police_man) - 1
		police_man[i].created = false
		shootout_status[i] = leave_car_or_fight
	endfor 
	
	for i = 0 to count_of(backup_police_front) - 1
		backup_police_front[i].created = false
		backup_police_front_shootout_status[i] = leave_car_or_fight
	endfor 

	for i = 0 to count_of(backup_police_alley) - 1
		backup_police_alley[i].created = false
		backup_police_alley_shootout_status[i] = leave_car_or_fight
	endfor 
	
	for i = 0 to count_of(security_guard) - 1
		security_guard_ai_system_status[i] = 0
	endfor 

	
	police_man[0].run_to_pos = <<883.9125, -2361.3347, 29.2142>> 
	police_man[0].model = S_M_Y_Cop_01
	police_man[0].weapon = weapontype_pistol 
	police_man[0].damaged_by_player = true
	police_man[0].health = 200
	police_man[0].accuracy = 5
	police_man[0].name = "police man 0"
	
	police_man[1].run_to_pos = <<886.47150, -2342.36255, 29.33342>>
	police_man[1].model = S_M_Y_Cop_01 
	police_man[1].weapon = weapontype_pistol
	police_man[1].damaged_by_player = true
	police_man[1].health = 200
	police_man[1].accuracy = 5
	police_man[1].name = "police man 1"
	
	police_man[2].run_to_pos = <<888.0029, -2362.3438, 29.2334>>
	police_man[2].model = S_M_Y_Cop_01 
	police_man[2].weapon = weapontype_pistol
	police_man[2].damaged_by_player = false
	police_man[2].health = 200
	police_man[2].accuracy = 5
	police_man[2].name = "police man 2"
	
	police_man[3].run_to_pos = <<891.9032, -2362.3477, 29.3657>>
	police_man[3].model = S_M_Y_Cop_01 
	police_man[3].weapon = weapontype_pistol 
	police_man[3].damaged_by_player = true
	police_man[3].health = 200
	police_man[3].accuracy = 5
	police_man[3].name = "police man 3"
	
	police_man[4].run_to_pos = <<893.0900, -2346.8401, 29.4129>> 
	police_man[4].model = S_M_Y_Cop_01 
	police_man[4].weapon = weapontype_pistol
	police_man[4].damaged_by_player = false
	police_man[4].health = 200
	police_man[4].accuracy = 5
	police_man[4].name = "police man 4"
	
	police_man[5].run_to_pos = <<886.3158, -2343.9456, 29.3304>> 
	police_man[5].model = S_M_Y_Cop_01 
	police_man[5].weapon = weapontype_pistol 
	police_man[5].damaged_by_player = true
	police_man[5].health = 200
	police_man[5].accuracy = 5
	police_man[5].name = "police man 5"
	
	police_man[6].run_to_pos = <<886.8484, -2341.6223, 29.3343>>
	police_man[6].model = S_M_Y_Cop_01 
	police_man[6].weapon = weapontype_pistol
	police_man[6].damaged_by_player = false
	police_man[6].health = 200
	police_man[6].accuracy = 5
	police_man[6].name = "police man 6"
	
	police_man[7].run_to_pos = <<899.53, -2346.0066, 29.5092>>
	police_man[7].model = S_M_Y_Cop_01 
	police_man[7].weapon = weapontype_pistol 
	police_man[7].damaged_by_player = true
	police_man[7].health = 200
	police_man[7].accuracy = 5
	police_man[7].name = "police man 7"

	//sniper on roof
	police_man[8].pos = <<962.9001, -2379.5808, 40.1731>>
	police_man[8].heading = 85.5324
	police_man[8].run_to_pos = <<925.7290, -2375.8982, 40.1731>>
	police_man[8].model = S_M_Y_SWAT_01
	police_man[8].weapon = weapontype_sniperrifle
	police_man[8].damaged_by_player = true
	police_man[8].health = 120
	police_man[8].accuracy = 5//100 //20
	police_man[8].name = "police man 8"
	
	//sniper on roof
	police_man[9].pos = <<944.7178, -2401.4944, 40.1731>>
	police_man[9].heading = 75.9638
	police_man[9].run_to_pos = <<922.8425, -2400.8088, 40.2425>>
	police_man[9].model = S_M_Y_SWAT_01
	police_man[9].weapon = weapontype_sniperrifle
	police_man[9].damaged_by_player = true
	police_man[9].health = 120
	police_man[9].accuracy = 5//100 //20
	police_man[9].name = "police man 9"
	
	//helicopter ped
	police_man[10].run_to_pos = <<894.1380, -2346.7490, 29.4404>>
	police_man[10].model = S_M_Y_SWAT_01
	police_man[10].weapon = weapontype_carbinerifle
	police_man[10].damaged_by_player = true
	police_man[10].health = 200
	police_man[10].accuracy = 2
	police_man[10].name = "police man 10"
	
	//helicopter ped
	police_man[11].run_to_pos = <<886.7021, -2345.0166, 29.3304>>
	police_man[11].model = S_M_Y_SWAT_01
	police_man[11].weapon = weapontype_carbinerifle
	police_man[11].damaged_by_player = true
	police_man[11].health = 200
	police_man[11].accuracy = 2
	police_man[11].name = "police man 11"
	
	//alley cops back left
	police_man[12].pos = <<845.1862, -2253.6438, 29.2907>>
	police_man[12].heading = 177.1563
	police_man[12].run_to_pos = <<843.84436, -2315.91577, 29.33460>>
	police_man[12].model = S_M_Y_SWAT_01
	police_man[12].weapon = weapontype_smg
	police_man[12].damaged_by_player = true
	police_man[12].health = 200
	police_man[12].accuracy = 5
	police_man[12].name = "police man 12"
	
	//alley cops back left
	police_man[13].pos = <<846.6603, -2270.7913, 29.3452>>
	police_man[13].heading = 186.9243
	police_man[13].run_to_pos = <<857.6421, -2312.7307, 29.3462>>
	police_man[13].model = S_M_Y_SWAT_01
	police_man[13].weapon = weapontype_smg
	police_man[13].damaged_by_player = true
	police_man[13].health = 200
	police_man[13].accuracy = 5
	police_man[13].name = "police man 13"
	
	//alley cops front 
	police_man[14].pos = <<949.6615, -2374.8660, 29.8815>>
	police_man[14].heading = 115.1845
	police_man[14].run_to_pos = <<885.1977, -2361.1411, 29.2205>>
	police_man[14].model = S_M_Y_cop_01
	police_man[14].weapon = weapontype_pistol
	police_man[14].damaged_by_player = false
	police_man[14].health = 200
	police_man[14].accuracy = 5
	police_man[14].name = "police man 14"

	//alley cops front
	police_man[15].pos = <<957.0480, -2366.3638, 29.5281>>
	police_man[15].heading = 135.4363
	police_man[15].run_to_pos = <<886.9384, -2344.6528, 29.3308>>
	police_man[15].model = S_M_Y_cop_01
	police_man[15].weapon = weapontype_pistol
	police_man[15].damaged_by_player = true
	police_man[15].health = 200
	police_man[15].accuracy = 5
	police_man[15].name = "police man 15"
	
	//alley cops back left
	police_man[16].pos = <<864.4395, -2251.6404, 29.4899>>
	police_man[16].heading = 176.2140
	police_man[16].run_to_pos = <<855.10992, -2305.22729, 29.34599>>
	police_man[16].model = S_M_Y_SWAT_01
	police_man[16].weapon = weapontype_smg
	police_man[16].damaged_by_player = false
	police_man[16].health = 200
	police_man[16].accuracy = 5
	police_man[16].name = "police man 16"
	
	//alley cops back left
	police_man[17].pos = <<868.1833, -2259.8342, 37.7890>>
	police_man[17].heading = 171.4895
	police_man[17].run_to_pos = <<854.8495, -2296.6997, 29.3433>>
	police_man[17].model = S_M_Y_SWAT_01
	police_man[17].weapon = weapontype_smg
	police_man[17].damaged_by_player = true
	police_man[17].health = 200
	police_man[17].accuracy = 5
	police_man[17].name = "police man 17"
	
	//alley cops back left
	police_man[18].pos = <<836.7518, -2249.3528, 29.1574>>
	police_man[18].heading = 243.6401
	police_man[18].run_to_pos = <<845.26855, -2304.28027, 29.33778>>
	police_man[18].model = S_M_Y_SWAT_01
	police_man[18].weapon = weapontype_smg
	police_man[18].damaged_by_player = false
	police_man[18].health = 200
	police_man[18].accuracy = 5
	police_man[18].name = "police man 18"
	
	police_man[19].pos = <<866.9947, -2251.4563, 29.5108>> 
	police_man[19].heading = 93.5280
	police_man[19].run_to_pos = <<861.5505, -2324.3267, 29.3458>>
	police_man[19].model = S_M_Y_SWAT_01
	police_man[19].weapon = weapontype_smg
	police_man[19].damaged_by_player = true
	police_man[19].health = 200
	police_man[19].accuracy = 5
	police_man[19].name = "police man 19"
	
	police_man[20].pos = <<831.0994, -2250.9937, 29.0958>>
	police_man[20].heading = 266.1835
	police_man[20].run_to_pos = <<847.28729, -2287.96216, 29.33541>>
	police_man[20].model = S_M_Y_SWAT_01
	police_man[20].weapon = weapontype_pistol
	police_man[20].damaged_by_player = false
	police_man[20].health = 200
	police_man[20].accuracy = 5
	police_man[20].name = "police man 20"
	police_man[20].created = true
	
	police_man[21].run_to_pos = <<862.5962, -2283.8804, 37.7922>> 
	police_man[21].model = S_M_Y_SWAT_01
	police_man[21].weapon = weapontype_pistol
	police_man[21].damaged_by_player = true
	police_man[21].health = 150
	police_man[21].accuracy = 5
	police_man[21].name = "police man 21"
	
	police_man[22].run_to_pos = <<864.55481, -2284.06543, 37.79219>>
	police_man[22].model = S_M_Y_SWAT_01
	police_man[22].weapon = weapontype_smg
	police_man[22].damaged_by_player = false
	police_man[22].health = 150
	police_man[22].accuracy = 5
	police_man[22].name = "police man 22"
	
	police_man[23].run_to_pos = <<847.04865, -2308.66064, 29.33128>>
	police_man[23].model = S_M_Y_SWAT_01
	police_man[23].weapon = weapontype_pistol
	police_man[23].damaged_by_player = true
	police_man[23].health = 150
	police_man[23].accuracy = 5
	police_man[23].name = "police man 23"
	
	police_man[24].run_to_pos = <<844.24176, -2315.34131, 29.33460>>
	police_man[24].model = S_M_Y_SWAT_01
	police_man[24].weapon = weapontype_smg
	police_man[24].damaged_by_player = false
	police_man[24].health = 150
	police_man[24].accuracy = 5
	police_man[24].name = "police man 24"
	
	police_man[25].run_to_pos = <<858.58038, -2313.15649, 29.34623>> 
	police_man[25].model = S_M_Y_SWAT_01
	police_man[25].weapon = weapontype_pistol
	police_man[25].damaged_by_player = true
	police_man[25].health = 150
	police_man[25].accuracy = 5
	police_man[25].name = "police man 25"
	
	police_man[26].run_to_pos = <<852.40906, -2315.25146, 29.34282>> 
	police_man[26].model = S_M_Y_SWAT_01
	police_man[26].weapon = weapontype_smg
	police_man[26].damaged_by_player = false
	police_man[26].health = 150
	police_man[26].accuracy = 5
	police_man[26].name = "police man 26"
	police_man[26].created = true
	
	police_man[27].run_to_pos = <<884.3048, -2361.2385, 29.2162>>
	police_man[27].model = S_M_Y_cop_01
	police_man[27].weapon = weapontype_pistol
	police_man[27].damaged_by_player = true
	police_man[27].health = 200
	police_man[27].accuracy = 5
	police_man[27].name = "police man 27"
	
	police_man[28].run_to_pos = <<890.2, -2347.5, 29.3346>>
	police_man[28].model = S_M_Y_cop_01
	police_man[28].weapon = weapontype_pistol
	police_man[28].damaged_by_player = true
	police_man[28].health = 200
	police_man[28].accuracy = 5
	police_man[28].name = "police man 28"
	
	police_man[29].pos = <<957.1928, -2366.2271, 29.5267>>
	police_man[29].heading = 75.0198
	police_man[29].run_to_pos = <<896.0663, -2355.3826, 29.4741>>
	police_man[29].model = S_M_Y_cop_01
	police_man[29].weapon = weapontype_pistol
	police_man[29].damaged_by_player = true
	police_man[29].health = 200
	police_man[29].accuracy = 5
	police_man[29].name = "police man 29"
	
	police_man[30].pos = <<950.1217, -2375.1311, 29.8892>>
	police_man[30].heading = 38.0107 
	police_man[30].run_to_pos = <<888.4880, -2358.4719, 29.2735>>
	police_man[30].model = S_M_Y_cop_01
	police_man[30].weapon = weapontype_pistol
	police_man[30].damaged_by_player = true
	police_man[30].health = 200
	police_man[30].accuracy = 5
	police_man[30].name = "police man 30"
	
	police_man[31].pos = <<969.2328, -2382.3147, 40.2425>>//<<939.9561, -2377.6033, 40.2425>>
	police_man[31].heading = 104.0753
	police_man[31].run_to_pos = <<923.9522, -2394.8845, 47.0570>>
	police_man[31].model = S_M_Y_SWAT_01
	police_man[31].weapon = weapontype_sniperrifle
	police_man[31].damaged_by_player = true
	police_man[31].health = 200
	police_man[31].accuracy = 5
	police_man[31].name = "police man 31"
	
	police_man[32].run_to_pos = <<858.5545, -2312.7759, 29.3462>>
	police_man[32].model = S_M_Y_cop_01
	police_man[32].weapon = weapontype_pistol
	police_man[32].damaged_by_player = true
	police_man[32].health = 200
	police_man[32].accuracy = 5
	police_man[32].name = "police man 32"

	police_man[33].run_to_pos = <<848.8008, -2302.2439, 29.3291>>
	police_man[33].model = S_M_Y_cop_01
	police_man[33].weapon = weapontype_pistol
	police_man[33].damaged_by_player = true
	police_man[33].health = 200
	police_man[33].accuracy = 5
	police_man[33].name = "police man 33"
	
	police_man[34].pos = <<847.2, -2270.3, 29.5123>>
	police_man[34].heading = 250.8976
	police_man[34].run_to_pos = <<844.4641, -2315.1985, 29.3346>>
	police_man[34].model = S_M_Y_cop_01
	police_man[34].weapon = weapontype_pistol
	police_man[34].damaged_by_player = true
	police_man[34].health = 200
	police_man[34].accuracy = 5
	police_man[34].name = "police man 34"
	
	police_man[35].pos = <<856.1734, -2266.6604, 35.1378>>
	police_man[35].heading = 178.9939
	police_man[35].run_to_pos = <<855.0160, -2297.2720, 29.3439>>
	police_man[35].model = S_M_Y_cop_01
	police_man[35].weapon = weapontype_pistol
	police_man[35].damaged_by_player = true
	police_man[35].health = 200
	police_man[35].accuracy = 5
	police_man[35].name = "police man 35"

	
	police_car[0].pos = <<866.4801, -2240.5852, 29.4915>>   
	police_car[0].heading = 264.0104
	police_car[0].model = police3
	police_car[0].recording_number = 011 		
	 
	police_car[1].pos = << 850.8638, -2238.8315, 29.5151>>    
	police_car[1].heading = 264.1243
	police_car[1].model = police3
	police_car[1].recording_number = 012		
	 
	police_car[2].pos = <<1035.1160, -2417.3760, 28.2291>> 
	police_car[2].heading = 173.4809
	police_car[2].model = police3
	police_car[2].recording_number = 013		
	  
	police_car[3].pos = <<1036.7899, -2402.2937, 28.8534>>    
	police_car[3].heading = 173.5939
	police_car[3].model = police3
	police_car[3].recording_number = 014	
	
	police_car[4].pos = <<1041.8916, -2349.5530, 29.5271>> //drives from the right 
	police_car[4].heading = 173.2809
	police_car[4].model = police3
	police_car[4].recording_number = 006 //lkfbi4
	
//	
//	//riot van
//	police_car[5].pos = <<1044.3179, -2354.3262, 29.4722>>    
//	police_car[5].heading = 174.8313
//	police_car[5].model = riot
//	police_car[5].recording_number = 014 //cntry

//	police_car[4].pos = <<879.8879, -2244.5342, 29.5950>> //drives from the left 
//	police_car[4].heading = 265.5648
//	police_car[4].model = police3
//	police_car[4].recording_number = 001 //lkfbi4
	
	police_car[5].pos = <<810.7969, -2240.9773, 29.4969>> //drives from the left and parks here    
	police_car[5].heading = -98.9281
	police_car[5].model = police3
	police_car[5].recording_number = 002 //lkfbi4
	
	police_car[6].pos = <<1035.1079, -2404.4380, 28.7375>> //drives from the left 
	police_car[6].heading = 172.0542
	police_car[6].model = police3
	police_car[6].recording_number = 003 //lkfbi4
	
	police_car[7].pos = <<1041.8916, -2349.5530, 29.5271>> //drives from the right 
	police_car[7].heading = 173.2809
	police_car[7].model = police3
	police_car[7].recording_number = 006 //lkfbi4
	
	police_car[8].pos = <<1035.6378, -2392.1111, 29.1724>> //drives from the right 
	police_car[8].heading = 173.2055
	police_car[8].model = police3
	police_car[8].recording_number = 005 //lkfbi4
	
	police_car[9].pos = <<884.7551, -2082.0029, 29.5991>> //drives from the right
	police_car[9].heading = 260.9446
	police_car[9].model = police3
	police_car[9].recording_number = 004 //lkfbi4
	
	police_car[10].pos = <<899.9318, -2076.0388, 29.7546>> //drives from the left
	police_car[10].heading = 261.2550
	police_car[10].model = police3
	police_car[10].recording_number = 007 //lkfbi4
	
	//police car who comes round behind the player
	police_car[11].pos = <<760.2631, -2410.3855, 19.4231>>    
	police_car[11].heading = 178.6288
	police_car[11].model = police3
	police_car[11].recording_number = 010   
	
	//2 new recordings added
	police_car[12].pos = <<899.9318, -2076.0388, 29.7546>>
	police_car[12].heading = 261.2550
	police_car[12].model = police3
	police_car[12].recording_number = 008  //fbi4
	
	police_car[13].pos = <<875.1038, -2074.4688, 29.4820>>
	police_car[13].heading = 261.4077
	police_car[13].model = police3
	police_car[13].recording_number = 009  //fbi
	
//	police_car[14].pos = <<782.6512, -2453.0559, 19.5825>>
//	police_car[14].heading = 271.8155
//	police_car[14].model = riot
//	police_car[14].recording_number = 015  //fbi



	//alley cops front 
	backup_police_front[0].pos = <<949.6615, -2374.8660, 29.8815>>
	backup_police_front[0].heading = 115.1845
	backup_police_front[0].run_to_pos = <<885.1977, -2361.1411, 29.2205>>
	backup_police_front[0].model = S_M_Y_cop_01
	backup_police_front[0].weapon = weapontype_pistol
	backup_police_front[0].damaged_by_player = true
	backup_police_front[0].health = 200
	backup_police_front[0].accuracy = 5
	backup_police_front[0].name = "backup_front 0"

	//alley cops front
	backup_police_front[1].pos = <<957.0480, -2366.3638, 29.5281>>
	backup_police_front[1].heading = 135.4363
	backup_police_front[1].run_to_pos = <<892.1824, -2361.9575, 29.3794>>
	backup_police_front[1].model = S_M_Y_cop_01
	backup_police_front[1].weapon = weapontype_pistol
	backup_police_front[1].damaged_by_player = true
	backup_police_front[1].health = 200
	backup_police_front[1].accuracy = 5
	backup_police_front[1].name = "backup_front 1"
	
	//spawns to the left
	backup_police_front[2].pos = <<904.8995, -2267.1782, 29.5456>>
	backup_police_front[2].heading = 210.8770
	backup_police_front[2].run_to_pos = <<898.5200, -2356.4607, 29.4943>>
	backup_police_front[2].model = S_M_Y_cop_01
	backup_police_front[2].weapon = weapontype_pistol
	backup_police_front[2].damaged_by_player = true
	backup_police_front[2].health = 200
	backup_police_front[2].accuracy = 5
	backup_police_front[2].name = "backup_front 2"
	
	//spawns to the right 
//	backup_police_front[3].pos = <<922.2668, -2439.5239, 27.4715>>
//	backup_police_front[3].heading = 48.9417
//	backup_police_front[3].run_to_pos = <<904.3785, -2351.6917, 29.3846>>
//	backup_police_front[3].model = S_M_Y_cop_01
//	backup_police_front[3].weapon = weapontype_pistol
//	backup_police_front[3].damaged_by_player = true
//	backup_police_front[3].health = 200
//	backup_police_front[3].accuracy = 5
//	backup_police_front[3].name = "backup_front 3"
//	
//	//spawns to the left across road
//	backup_police_front[4].pos = <<936.2743, -2253.8276, 29.5342>>
//	backup_police_front[4].heading = 84.5915
//	backup_police_front[4].run_to_pos = <<894.6367, -2343.1001, 29.4289>>
//	backup_police_front[4].model = S_M_Y_cop_01
//	backup_police_front[4].weapon = weapontype_pistol
//	backup_police_front[4].damaged_by_player = true
//	backup_police_front[4].health = 200
//	backup_police_front[4].accuracy = 5
//	backup_police_front[4].name = "backup_front 4"
	
	//*****BACK LEFT ALLLEY COPS*****
	
	backup_police_alley[0].pos = <<845.1862, -2253.6438, 29.2907>>
	backup_police_alley[0].heading = 177.1563
	backup_police_alley[0].run_to_pos = <<843.84436, -2315.91577, 29.33460>>
	backup_police_alley[0].model = S_M_Y_cop_01
	backup_police_alley[0].weapon = weapontype_pistol
	backup_police_alley[0].damaged_by_player = true
	backup_police_alley[0].health = 200
	backup_police_alley[0].accuracy = 5
	backup_police_alley[0].name = "backup_alley 0"

	backup_police_alley[1].pos = <<834.8723, -2250.6267, 29.1487>>
	backup_police_alley[1].heading = 263.29
	backup_police_alley[1].run_to_pos = <<857.6421, -2312.7307, 29.3462>>
	backup_police_alley[1].model = S_M_Y_cop_01
	backup_police_alley[1].weapon = weapontype_pistol
	backup_police_alley[1].damaged_by_player = true
	backup_police_alley[1].health = 200
	backup_police_alley[1].accuracy = 5
	backup_police_alley[1].name = "backup_alley 1"
	
	backup_police_alley[2].pos = <<864.4395, -2251.6404, 29.4899>>
	backup_police_alley[2].heading = 176.2140
	backup_police_alley[2].run_to_pos = <<855.10992, -2305.22729, 29.34599>>
	backup_police_alley[2].model = S_M_Y_cop_01
	backup_police_alley[2].weapon = weapontype_pistol
	backup_police_alley[2].damaged_by_player = true
	backup_police_alley[2].health = 200
	backup_police_alley[2].accuracy = 5
	backup_police_alley[2].name = "backup_alley 2"

	helicopter[0].pos = <<775.1086, -2442.4436, 38.1976>>
	helicopter[0].heading = -102.3257
	helicopter[0].model = POLMAV
	helicopter[0].recording_number = 15

//	helicopter.pos = <<927.1403, -2161.2671, 42.00>>
	helicopter[1].pos = <<794.2238, -2356.9136, 40.3881>>
	helicopter[1].heading = -6.2599
	helicopter[1].model = POLMAV
	helicopter[1].recording_number = 16
	
	
	helicopter_driver[0].model = S_M_Y_SWAT_01
	helicopter_driver[0].health = 200
	helicopter_driver[0].weapon = weapontype_unarmed
	helicopter_driver[0].damaged_by_player = true
	
	helicopter_driver[1].model = S_M_Y_SWAT_01
	helicopter_driver[1].health = 200
	helicopter_driver[1].weapon = weapontype_unarmed
	helicopter_driver[1].damaged_by_player = true
	
	absail_police_man[0].run_to_pos = <<888.0029, -2362.3438, 29.2334>>
	absail_police_man[0].model = S_M_Y_SWAT_01
	absail_police_man[0].weapon = weapontype_pistol 
	absail_police_man[0].damaged_by_player = true
	absail_police_man[0].health = 200
	absail_police_man[0].accuracy = 5
	absail_police_man[0].name = "absail 0"
	
	absail_police_man[1].run_to_pos = <<893.0900, -2346.8401, 29.4129>>
	absail_police_man[1].model = S_M_Y_SWAT_01
	absail_police_man[1].weapon = weapontype_pistol 
	absail_police_man[1].damaged_by_player = false
	absail_police_man[1].health = 200
	absail_police_man[1].accuracy = 5
	absail_police_man[1].name = "absail 1"
	
	absail_police_man[2].run_to_pos = <<893.0900, -2346.8401, 29.4129>>
	absail_police_man[2].model = S_M_Y_SWAT_01
	absail_police_man[2].weapon = weapontype_pistol 
	absail_police_man[2].damaged_by_player = true
	absail_police_man[2].health = 200
	absail_police_man[2].accuracy = 5
	absail_police_man[2].name = "absail 2"
	
	absail_police_man[3].run_to_pos = <<893.0900, -2346.8401, 29.4129>>
	absail_police_man[3].model = S_M_Y_SWAT_01
	absail_police_man[3].weapon = weapontype_pistol 
	absail_police_man[3].damaged_by_player = false
	absail_police_man[3].health = 200
	absail_police_man[3].accuracy = 5
	absail_police_man[3].name = "absail 3"
	
	get_away_car.pos = <<1109.8912, -1243.8976, 19.5119>>
	get_away_car.heading = 64.3406
	get_away_car.model = fugitive
	
	michael_car_node[0] = <<1201.4122, -1412.4779, 34.2269>>
	michael_car_node[1] = <<1489.8585, -1484.1273, 66.8696>>
	
	devin.pos = <<-2594.9998, 1930.1602, 166.2958>>
	devin.heading = 94.5464
	devin.model = get_npc_ped_model(char_devin)
	
	devins_phone.model =  prop_phone_ing  //PROP_NPC_PHONE
	
	devin_car.pos = <<-2592.6890, 1930.7489, 166.2953>>//<<-2588.6228, 1930.5492, 166.2956>>
	devin_car.heading = 95.2679//6.22
	devin_car.model = get_npc_veh_model(char_devin)
	devin_car.recording_number = 20
	
	security_guard[0].pos = <<-2558.4131, 1914.5460, 167.8589>>
	security_guard[0].heading = 228.9937 
	security_guard[0].model = S_M_Y_DevinSec_01//s_m_m_highsec_01
	security_guard[0].health = 200
	security_guard[0].weapon = weapontype_combatpistol
	security_guard[0].name = "security  0"
	
	security_guard[1].pos = <<-2559.4651, 1912.7107, 167.8373>>
	security_guard[1].heading = 245.3184 
	security_guard[1].model = S_M_Y_DevinSec_01//s_m_m_highsec_01
	security_guard[1].health = 200
	security_guard[1].weapon = weapontype_combatpistol
	security_guard[1].name = "security  1"


//	fire[0].pos = <<905.35, -2349.32, 29.43>>
//	fire[0].model = P_OIL_SLICK_01
//	fire[1].pos = <<905.54, -2349.86, 29.44>>
//	fire[1].model = P_OIL_SLICK_01
//	fire[2].pos = <<905.79, -2350.47, 29.46>>
//	fire[2].model = P_OIL_SLICK_01
//	fire[3].pos = <<905.95, -2350.98, 29.47>>
//	fire[3].model = P_OIL_SLICK_01
//	fire[4].pos = <<906.09, -2351.54, 29.48>>
//	fire[4].model = P_OIL_SLICK_01
//	fire[5].pos = <<906.33, -2351.98, 29.49>>
//	fire[5].model = P_OIL_SLICK_01
//	fire[6].pos = <<906.55, -2352.62, 29.50>>
//	fire[6].model = P_OIL_SLICK_01
//	fire[7].pos = <<906.76, -2353.12, 29.51>>
//	fire[7].model = P_OIL_SLICK_01
//	fire[8].pos = <<906.90, -2353.70, 29.52>>
//	fire[8].model = P_OIL_SLICK_01
//	fire[9].pos = <<907.17, -2354.17, 29.53>>
//	fire[9].model = P_OIL_SLICK_01

	fire_shot_up_index = 0
	fire_shot_down_index = 0

	//topleft to top right
	fire[0].offset = <<-1.2, 4.2, -0.7>>
	fire[1].offset = <<-0.4, 4.2, -0.7>>
	fire[2].offset = <<0.4, 4.2, -0.7>>
	fire[3].offset = <<1.2, 4.2, -0.7>>

	//top right to bottom right 
	fire[4].offset = <<1.2, 4.2, -0.7>>
	fire[5].offset = <<1.2, 2.9, -0.7>>
	fire[6].offset = <<1.2, 1.6, -0.7>>
	fire[7].offset = <<1.2, 0.3, -0.7>>
	fire[8].offset = <<1.2, -1.0, -0.7>>
	fire[9].offset = <<1.2, -2.3, -0.7>>
	fire[10].offset = <<1.2, -3.6, -0.7>>
	fire[11].offset = <<1.2, -4.9, -0.7>>

	//topleft to top right
	fire[12].offset = <<-1.2, -5.1, -0.7>>
	fire[13].offset = <<-0.4, -5.1, -0.7>>
	fire[14].offset = <<0.4, -5.1, -0.7>>
	fire[15].offset = <<1.2, -5.1, -0.7>>

	//bottom left to top left 
	fire[16].offset = <<-1.3, -4.9, -0.7>>
	fire[17].offset = <<-1.3, -3.6, -0.7>>
	fire[18].offset = <<-1.3, -2.3, -0.7>>
	fire[19].offset = <<-1.3, -1.0, -0.7>>
	fire[20].offset = <<-1.3, 0.3, -0.7>>
	fire[21].offset = <<-1.3, 1.6, -0.7>>
	fire[22].offset = <<-1.3, 2.9, -0.7>>
	fire[23].offset = <<-1.3, 4.2, -0.7>>
	
	for i = 0 to count_of(absail_police_man) - 1
		absail_police_man_status[i] = create_absail_ped
	endfor 
	
	for i = 0 to count_of(absail_police_man) - 1
		absail_police_man[i].created = false
		absail_police_man_status[i] = create_absail_ped
	endfor
	
	for i = 0 to count_of(fire) - 1
		fire[i].set = false
	endfor 
	
	for i = 0 to count_of(helicopter_time) - 1
		helicopter_time[i] = 0
	endfor 
	
	
//	for i = 0 to count_of(michael_aiming_task_given) - 1
//		michael_aiming_task_given[i] = false
//	endfor 
	
	army_truck_guy.created = false
	army_truck_guy_2.created = false
	
	CLEAR_TRIGGERED_LABELS()
	
	
//	for i = 0 to count_of(fire) - 1
//		fire[i]. = false
//	endfor 
	
endproc 

proc load_text_and_dialogue()
	
	register_script_with_audio()

	request_additional_text("heataud", mission_dialogue_text_slot)
	request_additional_text("CNTRY1", mission_text_slot)
	
	while not has_additional_text_loaded(mission_text_slot)
	or not has_additional_text_loaded(mission_dialogue_text_slot)
		wait(0)	
	endwhile

endproc 

proc setup_michaels_weapons(ped_index mission_ped)

	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
		
			if not HAS_PED_GOT_WEAPON_IN_GROUP(mission_ped, weapongroup_sniper)
			//if not has_ped_got_weapon(mission_ped, WEAPONTYPE_SNIPERRIFLE)
				give_weapon_to_ped(mission_ped, WEAPONTYPE_SNIPERRIFLE, 200, false, false)
			else
				
				weapon_type michaels_sniper
				
				if GET_FIRST_PED_WEAPON_IN_WEAPON_GROUP(mission_ped, weapongroup_sniper, michaels_sniper) 
	
					if get_ammo_in_ped_weapon(mission_ped, michaels_sniper) < 200
						set_ped_ammo(mission_ped, michaels_sniper, 200)
					endif 
					
					if michaels_sniper = WEAPONTYPE_HEAVYSNIPER
						if HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, michaels_sniper, WEAPONCOMPONENT_AT_AR_SUPP)
							remove_weapon_component_from_ped(mission_ped, michaels_sniper, WEAPONCOMPONENT_AT_AR_SUPP)//weaponcomponent_type
						endif 
					endif 
				
				endif 
				
			endif 
		
			if not has_ped_got_weapon(mission_ped, WEAPONTYPE_HEAVYSNIPER)
			
				give_weapon_to_ped(mission_ped, WEAPONTYPE_HEAVYSNIPER, 200, false, false)

				if HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_AR_SUPP)
					remove_weapon_component_from_ped(mission_ped, WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_AR_SUPP)//weaponcomponent_type
				endif 
				
			else 
				
				if HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_AR_SUPP)
					remove_weapon_component_from_ped(mission_ped, WEAPONTYPE_HEAVYSNIPER, WEAPONCOMPONENT_AT_AR_SUPP)//weaponcomponent_type
				endif 
				
				if get_ammo_in_ped_weapon(mission_ped, WEAPONTYPE_HEAVYSNIPER) < 200
					set_ped_ammo(mission_ped, WEAPONTYPE_HEAVYSNIPER, 200)
				endif 
				
				//set_current_ped_weapon(mission_ped, WEAPONTYPE_HEAVYSNIPER, true)
			endif 
			
			if not has_ped_got_weapon(mission_ped, weapontype_grenade)
				give_weapon_to_ped(mission_ped, weapontype_grenade, 15, false, false)
			else 
				if get_ammo_in_ped_weapon(mission_ped, weapontype_grenade) < 15
					set_ped_ammo(mission_ped, weapontype_grenade, 15)
				endif 
			endif 
		
			if not has_ped_got_weapon(mission_ped, weapontype_combatmg) 
			
				give_weapon_to_ped(mission_ped, weapontype_combatmg, 500, true, true)
				//set_ammo_in_clip(mission_ped, weapontype_combatmg, 500)
				GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_COMBATMG_CLIP_01) 
				GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_AT_SCOPE_MEDIUM)
				GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_AT_AR_AFGRIP)
				
			else
			
				if get_ammo_in_ped_weapon(mission_ped, weapontype_combatmg) < 500
					set_ped_ammo(mission_ped, weapontype_combatmg, 500)	
				endif 
			
				if HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_COMBATMG_CLIP_01)
					GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_COMBATMG_CLIP_01) 
				endif 
				
				if HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_AT_SCOPE_MEDIUM)
					GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_AT_SCOPE_MEDIUM) 
				endif 
				
				if HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_AT_AR_AFGRIP)
					GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_combatmg, WEAPONCOMPONENT_AT_AR_AFGRIP) 
				endif 
				
				set_current_ped_weapon(mission_ped, weapontype_combatmg, true)

			endif 
			
//			if not has_ped_got_weapon(mission_ped, weapontype_carbinerifle)//weapontype_combatmg)
//				
//				give_weapon_to_ped(mission_ped, weapontype_carbinerifle, 800, true, true)
////			GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_carbinerifle, weaponcomponent_at_ar_flsh) 
//				
//			else 
//				
//				if get_ammo_in_ped_weapon(mission_ped, weapontype_carbinerifle) < 800
//					set_ped_ammo(mission_ped, weapontype_carbinerifle, 800)
//				endif
//				
////			if not HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, weapontype_carbinerifle, weaponcomponent_at_ar_flsh)
////				GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_carbinerifle, weaponcomponent_at_ar_flsh)
////			endif 
//				
//				set_current_ped_weapon(mission_ped, weapontype_carbinerifle, true)
//				
//			endif 
			
		endif 
	endif

endproc 

proc setup_franklins_weapons(ped_index mission_ped, bool force_into_hand = true)

	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)
		
			if not has_ped_got_weapon(mission_ped, weapontype_pumpshotgun)
				give_weapon_to_ped(mission_ped, weapontype_pumpshotgun, 200, false, false)
			else 
				if get_ammo_in_ped_weapon(mission_ped, weapontype_pumpshotgun) < 200
					set_ped_ammo(mission_ped, weapontype_pumpshotgun, 200)
				endif 
			endif 
			
			if not has_ped_got_weapon(mission_ped, WEAPONTYPE_HEAVYSNIPER)
				give_weapon_to_ped(mission_ped, WEAPONTYPE_HEAVYSNIPER, 200, false, false)
			else 
				if get_ammo_in_ped_weapon(mission_ped, WEAPONTYPE_HEAVYSNIPER) < 200
					set_ped_ammo(mission_ped, WEAPONTYPE_HEAVYSNIPER, 200)
				endif 
			endif 
			
			if not has_ped_got_weapon(mission_ped, weapontype_grenade)
				give_weapon_to_ped(mission_ped, weapontype_grenade, 15, false, false)
			else 
				if get_ammo_in_ped_weapon(mission_ped, weapontype_grenade) < 15
					set_ped_ammo(mission_ped, weapontype_grenade, 15)
				endif 
			endif 

			if not has_ped_got_weapon(mission_ped, weapontype_carbinerifle)//weapontype_combatmg)
				
				give_weapon_to_ped(mission_ped, weapontype_carbinerifle, 800, force_into_hand, force_into_hand)
//				GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_carbinerifle, weaponcomponent_at_ar_flsh) 
				
			else 
				
				if get_ammo_in_ped_weapon(mission_ped, weapontype_carbinerifle) < 800
					set_ped_ammo(mission_ped, weapontype_carbinerifle, 800)
				endif
				
//				if not HAS_PED_GOT_WEAPON_COMPONENT(mission_ped, weapontype_carbinerifle, weaponcomponent_at_ar_flsh)
//					GIVE_WEAPON_COMPONENT_TO_PED(mission_ped, weapontype_carbinerifle, weaponcomponent_at_ar_flsh)
//				endif 
				
				set_current_ped_weapon(mission_ped, weapontype_carbinerifle, force_into_hand)
				
			endif 
			
		endif 
	endif 
	
endproc

proc setup_trevors_weapons(ped_index mission_ped)
			
	if does_entity_exist(mission_ped)
		if not is_ped_injured(mission_ped)

			if not HAS_PED_GOT_WEAPON_IN_GROUP(mission_ped, weapongroup_sniper)
			//if not has_ped_got_weapon(mission_ped, WEAPONTYPE_SNIPERRIFLE)
				give_weapon_to_ped(mission_ped, WEAPONTYPE_SNIPERRIFLE, 200, false, false)
			else
				
				weapon_type trevors_sniper
				
				if GET_FIRST_PED_WEAPON_IN_WEAPON_GROUP(mission_ped, weapongroup_sniper, trevors_sniper) 
	
					if get_ammo_in_ped_weapon(mission_ped, trevors_sniper) < 200
						set_ped_ammo(mission_ped, trevors_sniper, 200)
					endif 
				
				endif 
			endif 
			
			if not has_ped_got_weapon(mission_ped, weapontype_rpg)
				give_weapon_to_ped(mission_ped, weapontype_rpg, 16, true, true)
			else 
				if get_ammo_in_ped_weapon(mission_ped, weapontype_rpg) < 16
					set_ped_ammo(mission_ped, weapontype_rpg, 16)
				endif 
				set_current_ped_weapon(mission_ped, weapontype_rpg, true)
			endif 
			
		endif 
		
	endif 

endproc




func bool smashing_wall_rayfire_system(float trigger_time = 0.0)
	
	switch smashing_wall_rayfire_status
	
		case 0
		
			printstring("waiting for ray to exist in order to prime")
			printnl()
		
			smashing_wall = GET_RAYFIRE_MAP_OBJECT(<<890.3647, -2367.289, 28.10582>>, 10, "DES_Smash2") //888.91, -2366.38, 30.24      890.3647, -2367.289, 28.10582
	
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(smashing_wall)
		
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(smashing_wall, RFMO_STATE_PRIMING)

				smashing_wall_rayfire_status++

			ENDIF
			
		
		break 
		
		case 1

			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(smashing_wall) = RFMO_STATE_PRIMED
				
				#IF IS_DEBUG_BUILD
					printstring("ray fire primmed")
					printnl()
				#endif 
			
				if trigger_time > 1100.00
				
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(smashing_wall, RFMO_STATE_START_ANIM) 

					//play_sound_frontend(rayfire_sound, "FBI_04_HEAT_WALL_SMASH")
					//PLAY_STREAM_FRONTEND()
					
					smashing_wall_rayfire_status++
					
					return true
					
				endif

			else 
			
				#IF IS_DEBUG_BUILD
					printstring("ray fire NOT primmed")
					printnl()
				#endif 
			
			endif 
		
		break 	
		
		case 2
		
			printstring("ray fire PLAYING")
			printnl()
			
		break 
		
		case 22
		
			printstring("ending state")
			printnl()
		
		break 
		
	endswitch 
	
	return false 

endfunc


proc load_uber_data()
//speedo
//utillitruck3
//intruder

	SetPieceCarPos[0] = <<1422.3533, -1877.0969, 70.8741>>
	SetPieceCarQuatX[0] = -0.0341
	SetPieceCarQuatY[0] = -0.0104
	SetPieceCarQuatZ[0] = 0.9968
	SetPieceCarQuatW[0] = 0.0712
	SetPieceCarRecording[0] = 1
	SetPieceCarStartime[0] = 0.0000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = intruder
	
	SetPieceCarPos[1] = <<1271.8022, -2046.9530, 43.9714>>
	SetPieceCarQuatX[1] = -0.0056
	SetPieceCarQuatY[1] = 0.0029
	SetPieceCarQuatZ[1] = -0.5599
	SetPieceCarQuatW[1] = 0.8285
	SetPieceCarRecording[1] = 2
	SetPieceCarStartime[1] = 9000.0000 //0.0
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = intruder
	
	SetPieceCarPos[2] = <<1121.7990, -2084.0630, 39.4448>>
	SetPieceCarQuatX[2] = -0.0369
	SetPieceCarQuatY[2] = 0.0405
	SetPieceCarQuatZ[2] = 0.7168
	SetPieceCarQuatW[2] = -0.6952
	SetPieceCarRecording[2] = 3
	SetPieceCarStartime[2] = 2000.0000
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = intruder
	
//	SetPieceCarPos[3] = <<1265.4590, -2102.5020, 45.5267>>
//	SetPieceCarQuatX[3] = -0.0211
//	SetPieceCarQuatY[3] = -0.0038
//	SetPieceCarQuatZ[3] = 0.1811
//	SetPieceCarQuatW[3] = 0.9832
//	SetPieceCarRecording[3] = 4
//	SetPieceCarStartime[3] = 3500.0000 //4000.00
//	SetPieceCarRecordingSpeed[3] = 1.0000
//	SetPieceCarModel[3] = utillitruck3
	
	SetPieceCarPos[4] = <<1198.3480, -1975.2615, 41.4057>>
	SetPieceCarQuatX[4] = -0.0144
	SetPieceCarQuatY[4] = 0.0367
	SetPieceCarQuatZ[4] = 0.9809
	SetPieceCarQuatW[4] = -0.1905
	SetPieceCarRecording[4] = 5
	SetPieceCarStartime[4] = 6500.00//4500.0000  //5250
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = utillitruck3
	
	SetPieceCarPos[5] = <<1068.9875, -2081.0024, 33.3438>>
	SetPieceCarQuatX[5] = -0.0391
	SetPieceCarQuatY[5] = 0.0363
	SetPieceCarQuatZ[5] = 0.7185
	SetPieceCarQuatW[5] = -0.6935
	SetPieceCarRecording[5] = 6
	SetPieceCarStartime[5] = 5500.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = utillitruck3
	
	SetPieceCarPos[6] = <<963.4304, -2086.3254, 30.2687>>
	SetPieceCarQuatX[6] = 0.0050
	SetPieceCarQuatY[6] = -0.0043
	SetPieceCarQuatZ[6] = -0.7028
	SetPieceCarQuatW[6] = 0.7114
	SetPieceCarRecording[6] = 7
	SetPieceCarStartime[6] = 7500.0000
	SetPieceCarRecordingSpeed[6] = 1.0000
	SetPieceCarModel[6] = speedo
	
//	SetPieceCarPos[7] = <<1266.4374, -2107.3267, 45.4826>>
//	SetPieceCarQuatX[7] = -0.0220
//	SetPieceCarQuatY[7] = -0.0046
//	SetPieceCarQuatZ[7] = 0.1504
//	SetPieceCarQuatW[7] = 0.9884
//	SetPieceCarRecording[7] = 8
//	SetPieceCarStartime[7] = 11000.0000 //9000.00
//	SetPieceCarRecordingSpeed[7] = 1.0000
//	SetPieceCarModel[7] = speedo
	
	SetPieceCarPos[8] = <<907.7520, -2082.9995, 30.0697>>
	SetPieceCarQuatX[8] = 0.0014
	SetPieceCarQuatY[8] = -0.0038
	SetPieceCarQuatZ[8] = 0.7314
	SetPieceCarQuatW[8] = -0.6820
	SetPieceCarRecording[8] = 9
	SetPieceCarStartime[8] = 10000.0000  //10500.00
	SetPieceCarRecordingSpeed[8] = 1.0000
	SetPieceCarModel[8] = speedo

	SetPieceCarPos[9] = <<932.7582, -2037.1530, 29.8118>>
	SetPieceCarQuatX[9] = -0.0265
	SetPieceCarQuatY[9] = -0.0070
	SetPieceCarQuatZ[9] = 0.9995
	SetPieceCarQuatW[9] = 0.0158
	SetPieceCarRecording[9] = 10
	SetPieceCarStartime[9] = 14500.0000
	SetPieceCarRecordingSpeed[9] = 1.0000
	SetPieceCarModel[9] = intruder
	
//	SetPieceCarPos[10] = <<1065.1875, -2133.2349, 32.3238>>
//	SetPieceCarQuatX[10] = -0.0025
//	SetPieceCarQuatY[10] = 0.0047
//	SetPieceCarQuatZ[10] = -0.0399
//	SetPieceCarQuatW[10] = 0.9992
//	SetPieceCarRecording[10] = 11
//	SetPieceCarStartime[10] = 14500.0000
//	SetPieceCarRecordingSpeed[10] = 1.0000
//	SetPieceCarModel[10] = intruder
	
	SetPieceCarPos[11] = <<1102.1646, -2071.4629, 37.0020>>
	SetPieceCarQuatX[11] = -0.0721
	SetPieceCarQuatY[11] = -0.0486
	SetPieceCarQuatZ[11] = 0.6862
	SetPieceCarQuatW[11] = 0.7222
	SetPieceCarRecording[11] = 12
	SetPieceCarStartime[11] = 15700.00//14800.0000
	SetPieceCarRecordingSpeed[11] = 1.0000
	SetPieceCarModel[11] = intruder
	
	SetPieceCarPos[12] = <<929.1259, -2132.2727, 29.7939>>
	SetPieceCarQuatX[12] = -0.0040
	SetPieceCarQuatY[12] = -0.0066
	SetPieceCarQuatZ[12] = -0.0359
	SetPieceCarQuatW[12] = 0.9993
	SetPieceCarRecording[12] = 13
	SetPieceCarStartime[12] = 16500.00//14500.0000
	SetPieceCarRecordingSpeed[12] = 1.0000
	SetPieceCarModel[12] = speedo
	
	SetPieceCarPos[13] = <<927.9391, -2152.3672, 30.0681>>
	SetPieceCarQuatX[13] = -0.0000
	SetPieceCarQuatY[13] = -0.0014
	SetPieceCarQuatZ[13] = -0.0385
	SetPieceCarQuatW[13] = 0.9993
	SetPieceCarRecording[13] = 14
	SetPieceCarStartime[13] = 20000.00//15000.0000
	SetPieceCarRecordingSpeed[13] = 1.0000
	SetPieceCarModel[13] = utillitruck3
	
	SetPieceCarPos[14] = <<867.0321, -2104.5317, 29.8221>>
	SetPieceCarQuatX[14] = -0.0044
	SetPieceCarQuatY[14] = -0.0205
	SetPieceCarQuatZ[14] = -0.0787
	SetPieceCarQuatW[14] = 0.9967
	SetPieceCarRecording[14] = 15
	SetPieceCarStartime[14] = 18000.0000
	SetPieceCarRecordingSpeed[14] = 1.0000
	SetPieceCarModel[14] = speedo
//	
//	TrafficCarPos[15] = <<911.9921, -2082.8569, 30.1200>>
//	TrafficCarQuatX[15] = 0.0022
//	TrafficCarQuatY[15] = -0.0024
//	TrafficCarQuatZ[15] = 0.7241
//	TrafficCarQuatW[15] = -0.6897
//	TrafficCarRecording[15] = 16
//	TrafficCarStartime[15] = 22462.0000
//	TrafficCarModel[15] = rapidgt
//
//	TrafficCarPos[16] = <<901.3917, -2071.2493, 30.1486>>
//	TrafficCarQuatX[16] = 0.0020
//	TrafficCarQuatY[16] = 0.0015
//	TrafficCarQuatZ[16] = 0.6769
//	TrafficCarQuatW[16] = 0.7361
//	TrafficCarRecording[16] = 17
//	TrafficCarStartime[16] = 22858.0000
//	TrafficCarModel[16] = radi

	TrafficCarPos[17] = <<792.7391, -2071.5518, 28.9547>>
	TrafficCarQuatX[17] = -0.0040
	TrafficCarQuatY[17] = -0.0208
	TrafficCarQuatZ[17] = -0.5235
	TrafficCarQuatW[17] = 0.8518
	TrafficCarRecording[17] = 18
	TrafficCarStartime[17] = 27148.0000
	TrafficCarModel[17] = feltzer2

	TrafficCarPos[18] = <<789.3976, -2078.5901, 28.9540>>
	TrafficCarQuatX[18] = 0.0042
	TrafficCarQuatY[18] = -0.0002
	TrafficCarQuatZ[18] = -0.0429
	TrafficCarQuatW[18] = 0.9991
	TrafficCarRecording[18] = 19
	TrafficCarStartime[18] = 27214.0000
	TrafficCarModel[18] = fq2

	TrafficCarPos[19] = <<760.3094, -2127.5635, 28.8160>>
	TrafficCarQuatX[19] = -0.0005
	TrafficCarQuatY[19] = -0.0036
	TrafficCarQuatZ[19] = 0.9988
	TrafficCarQuatW[19] = 0.0494
	TrafficCarRecording[19] = 20
	TrafficCarStartime[19] = 30513.0000
	TrafficCarModel[19] = serrano

	TrafficCarPos[20] = <<753.7613, -2140.9443, 28.8163>>
	TrafficCarQuatX[20] = -0.0021
	TrafficCarQuatY[20] = -0.0121
	TrafficCarQuatZ[20] = 0.9987
	TrafficCarQuatW[20] = 0.0494
	TrafficCarRecording[20] = 21
	TrafficCarStartime[20] = 30579.0000
	TrafficCarModel[20] = fq2

	TrafficCarPos[21] = <<780.1787, -2120.8403, 28.8168>>
	TrafficCarQuatX[21] = -0.0044
	TrafficCarQuatY[21] = -0.0000
	TrafficCarQuatZ[21] = -0.0484
	TrafficCarQuatW[21] = 0.9988
	TrafficCarRecording[21] = 22
	TrafficCarStartime[21] = 30843.0000
	TrafficCarModel[21] = serrano

	TrafficCarPos[22] = <<750.2296, -2242.9326, 28.8457>>
	TrafficCarQuatX[22] = -0.0002
	TrafficCarQuatY[22] = -0.0035
	TrafficCarQuatZ[22] = 0.9991
	TrafficCarQuatW[22] = 0.0431
	TrafficCarRecording[22] = 23
	TrafficCarStartime[22] = 35727.0000
	TrafficCarModel[22] = radi

	TrafficCarPos[23] = <<781.4967, -2233.7319, 28.8944>>
	TrafficCarQuatX[23] = 0.0052
	TrafficCarQuatY[23] = -0.0057
	TrafficCarQuatZ[23] = 0.6125
	TrafficCarQuatW[23] = 0.7904
	TrafficCarRecording[23] = 24
	TrafficCarStartime[23] = 36255.0000
	TrafficCarModel[23] = bison

	TrafficCarPos[24] = <<772.0815, -2276.5010, 28.6292>>
	TrafficCarQuatX[24] = 0.0161
	TrafficCarQuatY[24] = -0.0006
	TrafficCarQuatZ[24] = -0.0421
	TrafficCarQuatW[24] = 0.9990
	TrafficCarRecording[24] = 25
	TrafficCarStartime[24] = 36915.0000
	TrafficCarModel[24] = fq2

	TrafficCarPos[25] = <<763.6131, -2310.3257, 26.7922>>
	TrafficCarQuatX[25] = 0.0355
	TrafficCarQuatY[25] = -0.0023
	TrafficCarQuatZ[25] = -0.0452
	TrafficCarQuatW[25] = 0.9983
	TrafficCarRecording[25] = 26
	TrafficCarStartime[25] = 38235.0000
	TrafficCarModel[25] = bison

	TrafficCarPos[26] = <<766.6949, -2338.0071, 24.7797>>
	TrafficCarQuatX[26] = 0.0396
	TrafficCarQuatY[26] = -0.0014
	TrafficCarQuatZ[26] = -0.0413
	TrafficCarQuatW[26] = 0.9984
	TrafficCarRecording[26] = 27
	TrafficCarStartime[26] = 39225.0000
	TrafficCarModel[26] = bison

endproc

proc load_trip_skip_data_0()

	if replay_active
		start_replay_setup(<<1376.8572, -2079.4595, 50.9983>>, 16.5575, false)
	endif 

	SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_NORMAL)

	request_model(GET_PLAYER_PED_MODEL(char_michael))
	request_model(GET_PLAYER_PED_MODEL(char_franklin))
	request_model(GET_PLAYER_PED_MODEL(char_trevor))

	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)

	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(trevors_car.model)
	set_vehicle_model_is_suppressed(trevors_car.model, true) 

	request_anim_dict("missfbi4")
	request_anim_dict("missheat")
	
	request_model(binoculars.model)
	sf_binoculars = REQUEST_SCALEFORM_MOVIE("binoculars")
	
	REQUEST_PTFX_ASSET()

	request_vehicle_recording(001, "lkcountry") 

	request_vehicle_recording(001, "lkheat") 
	request_vehicle_recording(002, "lkheat") 
	request_vehicle_recording(003, "lkheat") 
	request_vehicle_recording(004, "lkheat") 
	request_vehicle_recording(005, "lkheat") 
	request_vehicle_recording(006, "lkheat") 
	request_vehicle_recording(007, "lkheat") 
	request_vehicle_recording(008, "lkheat") 
	request_vehicle_recording(009, "lkheat") 
	request_vehicle_recording(010, "lkheat") 
	request_vehicle_recording(011, "lkheat") 
	request_vehicle_recording(012, "lkheat") 
	request_vehicle_recording(013, "lkheat") 
	request_vehicle_recording(014, "lkheat") 
	request_vehicle_recording(015, "lkheat") 
	request_vehicle_recording(016, "lkheat") 
	request_vehicle_recording(017, "lkheat") 
	request_vehicle_recording(018, "lkheat") 
	request_vehicle_recording(019, "lkheat") 
	request_vehicle_recording(020, "lkheat") 
	request_vehicle_recording(021, "lkheat") 
	request_vehicle_recording(021, "lkheat") 
	request_vehicle_recording(022, "lkheat") 
	request_vehicle_recording(023, "lkheat") 
	request_vehicle_recording(024, "lkheat") 
	request_vehicle_recording(025, "lkheat") 
	request_vehicle_recording(026, "lkheat") 
	request_vehicle_recording(027, "lkheat") 
	
	request_vehicle_recording(101, "lkheat") 
	request_vehicle_recording(102, "lkheat") 
	
	request_vehicle_recording(201, "lkheat") 
	request_vehicle_recording(202, "lkheat") 
	request_vehicle_recording(203, "lkheat") 
	request_vehicle_recording(204, "lkheat") 
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(rubbish_truck.model)
	or not has_model_loaded(tow_truck.model)
	or not has_model_loaded(trevors_car.model)
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(binoculars.model)
	or not has_scaleform_movie_loaded(sf_binoculars)
	or not has_anim_dict_loaded("missfbi4")
	or not has_anim_dict_loaded("missheat")
	or not HAS_PTFX_ASSET_LOADED()
	or not has_vehicle_recording_been_loaded(001, "lkcountry")
	or not has_vehicle_recording_been_loaded(001, "lkheat")
	or not has_vehicle_recording_been_loaded(002, "lkheat")
	or not has_vehicle_recording_been_loaded(003, "lkheat") 
	or not has_vehicle_recording_been_loaded(004, "lkheat")
	or not has_vehicle_recording_been_loaded(005, "lkheat") 
	or not has_vehicle_recording_been_loaded(006, "lkheat") 
	or not has_vehicle_recording_been_loaded(007, "lkheat") 
	or not has_vehicle_recording_been_loaded(008, "lkheat")
	or not has_vehicle_recording_been_loaded(009, "lkheat") 
	or not has_vehicle_recording_been_loaded(010, "lkheat") 
	or not has_vehicle_recording_been_loaded(011, "lkheat") 
	or not has_vehicle_recording_been_loaded(012, "lkheat") 
	or not has_vehicle_recording_been_loaded(013, "lkheat") 
	or not has_vehicle_recording_been_loaded(014, "lkheat")
	or not has_vehicle_recording_been_loaded(015, "lkheat")
	or not has_vehicle_recording_been_loaded(016, "lkheat") 
	or not has_vehicle_recording_been_loaded(017, "lkheat")
	or not has_vehicle_recording_been_loaded(018, "lkheat") 
	or not has_vehicle_recording_been_loaded(019, "lkheat") 
	or not has_vehicle_recording_been_loaded(020, "lkheat") 
	or not has_vehicle_recording_been_loaded(021, "lkheat") 
	or not has_vehicle_recording_been_loaded(021, "lkheat")
	or not has_vehicle_recording_been_loaded(022, "lkheat")
	or not has_vehicle_recording_been_loaded(023, "lkheat")
	or not has_vehicle_recording_been_loaded(024, "lkheat") 
	or not has_vehicle_recording_been_loaded(025, "lkheat")
	or not has_vehicle_recording_been_loaded(026, "lkheat")
	or not has_vehicle_recording_been_loaded(027, "lkheat")
	or not has_vehicle_recording_been_loaded(101, "lkheat") 
	or not has_vehicle_recording_been_loaded(102, "lkheat") 
	or not has_vehicle_recording_been_loaded(201, "lkheat")
	or not has_vehicle_recording_been_loaded(202, "lkheat")
	or not has_vehicle_recording_been_loaded(203, "lkheat")
	or not has_vehicle_recording_been_loaded(204, "lkheat")

		wait(0)
		
	endwhile 
	

	clear_player_wanted_level(player_id())
	
	if get_current_player_vehicle(temp_vehicle)
		set_entity_coords(temp_vehicle, <<1400.6285, -2053.2800, 50.9983>>)  
		set_entity_heading(temp_vehicle, 78.1507)
	else 
		if does_entity_exist(get_players_last_vehicle())
			if is_vehicle_driveable(get_players_last_vehicle())
				if is_entity_at_coord(get_players_last_vehicle(), <<1379.4003, -2074.7783, 50.9988>>, <<50.00, 50.00, 50.00>>)
					set_entity_coords(get_players_last_vehicle(), <<1400.6285, -2053.2800, 50.9983>>)  
					set_entity_heading(get_players_last_vehicle(), 78.1507)
				endif 
			endif
		endif 
	endif
	
	set_roads_in_area(<<1412.83, -1954.96, -100.00>>, <<1406.50, -1941.47, 100.00>>, false)
	
	clear_area(<<1379.3738, -2074.8240, 50.9985>>, 1000.00, true)
	
	rubbish_truck.veh = create_vehicle(rubbish_truck.model, <<1381.4722, -2072.2454, 50.9981>>, 38.3340)
	set_vehicle_has_strong_axles(rubbish_truck.veh, true)
	set_vehicle_automatically_attaches(rubbish_truck.veh, false)
	set_vehicle_as_restricted(rubbish_truck.veh, 0)
	//SET_SIREN_CAN_BE_CONTROLLED_BY_AUDIO(rubbish_truck.veh, false) 
	//set_vehicle_livery(rubbish_truck.veh, 0)
	
	tow_truck.veh = create_vehicle(tow_truck.model, <<1374.8580, -2077.3743, 50.9981>>, 37.5148)
	set_vehicle_colours(tow_truck.veh, 0, 0)
	set_vehicle_has_strong_axles(tow_truck.veh, true)
	set_entity_only_damaged_by_player(tow_truck.veh, true)
	set_vehicle_doors_locked(tow_truck.veh, vehiclelock_lockout_player_only) 
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	set_vehicle_as_restricted(tow_truck.veh, 1)
	
	CREATE_PLAYER_VEHICLE(trevors_car.veh, CHAR_TREVOR, get_position_of_vehicle_recording_at_time(101, 1450, "lkheat"), 40.3225, false)
	set_entity_only_damaged_by_player(trevors_car.veh, true)
	set_vehicle_doors_locked(trevors_car.veh, vehiclelock_lockout_player_only)  

	
	set_current_selector_ped(selector_ped_michael, false)
	clear_ped_tasks_immediately(player_ped_id())
	SET_STORED_FBI4_OUTFIT(player_ped_id())
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
//	set_entity_coords(player_ped_id(), <<1377.9087, -2071.4409, 50.9981>>)
//	set_entity_heading(player_ped_id(), 321.9411)
	set_entity_coords(player_ped_id(), <<1376.8572, -2079.4595, 50.9983>>)
	set_entity_heading(player_ped_id(), 16.5575)
	//set_ped_into_vehicle(player_ped_id(), rubbish_truck.veh)
	add_ped_for_dialogue(scripted_speech[0], 0, player_ped_id(), "michael")
	
	
	create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_TREVOR], char_trevor, trevors_car.veh, vs_driver, false)
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_TREVOR])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_TREVOR])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
	add_ped_for_dialogue(scripted_speech[0], 2, selector_ped.pedID[SELECTOR_PED_TREVOR], "trevor")	
	
	#IF IS_DEBUG_BUILD
		set_uber_parent_widget_group(fbi_4_widget_group)
	#endif 
	INITIALISE_UBER_PLAYBACK("lkheat", 101, true)
	load_uber_data()
	fUberPlaybackDensitySwitchOffRange = 200
	switch_SetPieceCar_to_ai_on_collision = true
	allow_veh_to_stop_on_any_veh_impact = true
	start_playback_recorded_vehicle(trevors_car.veh, 101, "lkheat")
	update_uber_playback(trevors_car.veh, 1.0)
	SET_UBER_PLAYBACK_TO_TIME_NOW(trevors_car.veh, 2000.00)
	update_uber_playback(trevors_car.veh, 1.0)
	force_playback_recorded_vehicle_update(trevors_car.veh)
		
	create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], char_franklin, tow_truck.veh, vs_driver, false) 
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_FRANKLIN], true)
	set_entity_load_collision_flag(selector_ped.pedID[SELECTOR_PED_FRANKLIN], true)
	add_ped_for_dialogue(scripted_speech[0], 1, selector_ped.pedID[SELECTOR_PED_franklin], "franklin")
		
	start_playback_recorded_vehicle(tow_truck.veh, 102, "lkheat")
	skip_time_in_playback_recorded_vehicle(tow_truck.veh, 800.00) 
	force_playback_recorded_vehicle_update(tow_truck.veh)

	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	wait(0) //must wait a frame to ensure the outfit variaitons have been applied before checking the streaming requests for the player
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[SELECTOR_PED_trevor])
	
		wait(0)
		
	endwhile
		
	end_cutscene(true, true, 0, 0, false)
	
	original_time = get_game_timer()
	
	trigger_music_event("FBI4_SWITCH_BINOC_ST")
	
	Set_Replay_Mid_Mission_Stage_With_Name(0, "Enter ambulance at FIB depot")
	
	mission_flow = get_ambulance_into_pos
	
endproc

proc load_trip_skip_data_1()

	set_roads_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	set_ped_paths_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	SET_PED_NON_CREATION_AREA(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
	add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)
	
	//switches off generators near shootout
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, false) //cars near ally way
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, false) //vehicle near cover blocks


	clear_area(tow_truck.pos, 1000.00, true)
	clear_area(<<890.4393, -2346.4761, 29.3413>>, 10000.00, true)
	
	if replay_active
		start_replay_setup(<<989.9289, -2373.2302, 29.5308>>, 80.8604, false)
	endif 
	
	request_model(GET_PLAYER_PED_MODEL(char_michael))
	request_model(GET_PLAYER_PED_MODEL(char_franklin))
	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	
	request_model(army_truck_driver.model)
	set_vehicle_model_is_suppressed(army_truck_driver.model, true)
	
	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)
	
	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(army_truck.model)
	set_vehicle_model_is_suppressed(army_truck.model, true)
	
	request_model(van_light.model)
	
	request_model(get_weapontype_model(weapontype_combatmg))
	request_weapon_asset(weapontype_combatmg)
	
	REQUEST_PTFX_ASSET()
	
	request_vehicle_recording(002, "lkcountry") 
	request_vehicle_recording(003, "lkcountry") 
	request_vehicle_recording(004, "lkcountry") 
	request_vehicle_recording(005, "lkcountry") 
	request_vehicle_recording(010, "lkcountry")
	request_vehicle_recording(011, "lkcountry")
	request_vehicle_recording(012, "lkcountry")
	request_vehicle_recording(014, "lkcountry")
	request_vehicle_recording(015, "lkcountry")
	request_vehicle_recording(016, "lkcountry")
	
	request_anim_dict("missfbi4")
	request_anim_dict("misssagrab")
	request_anim_dict("missheat")
	
	REQUEST_WAYPOINT_RECORDING("heat1")
	REQUEST_WAYPOINT_RECORDING("heat2")
	REQUEST_WAYPOINT_RECORDING("heat3")
	
	request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	
	LOAD_STREAM("Truck_Crash_Stream","FBI_04_HEAT_SOUNDS")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 

	
	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(army_truck_driver.model)
	or not has_model_loaded(rubbish_truck.model)
	or not has_model_loaded(tow_truck.model)
	or not has_model_loaded(army_truck.model)
	or not has_model_loaded(van_light.model)
	or not has_model_loaded(get_weapontype_model(weapontype_combatmg))
	or not has_weapon_asset_loaded(weapontype_combatmg)
	or not HAS_PTFX_ASSET_LOADED()
	or not has_vehicle_recording_been_loaded(002, "lkcountry")
	or not has_vehicle_recording_been_loaded(003, "lkcountry")
	or not has_vehicle_recording_been_loaded(004, "lkcountry")
	or not has_vehicle_recording_been_loaded(005, "lkcountry")
	or not has_vehicle_recording_been_loaded(010, "lkcountry")
	or not has_vehicle_recording_been_loaded(011, "lkcountry")
	or not has_vehicle_recording_been_loaded(012, "lkcountry")
	or not has_vehicle_recording_been_loaded(014, "lkcountry")
	or not has_vehicle_recording_been_loaded(015, "lkcountry")
	or not has_vehicle_recording_been_loaded(016, "lkcountry")
	or not has_anim_dict_loaded("missfbi4")
	or not has_anim_dict_loaded("misssagrab")
	or not has_anim_dict_loaded("missheat")
	or not get_is_waypoint_recording_loaded("heat1")
	or not get_is_waypoint_recording_loaded("heat2")
	or not get_is_waypoint_recording_loaded("heat3")
	or not request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
	or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	or not LOAD_STREAM("Truck_Crash_Stream","FBI_04_HEAT_SOUNDS")

		wait(0)
		
	endwhile 
	
	tow_truck.veh = create_vehicle(tow_truck.model, <<1024.1974, -2376.5244, 29.5306>>, 85.7590)
	set_vehicle_colours(tow_truck.veh, 0, 0)
	set_vehicle_has_strong_axles(tow_truck.veh, true)
	set_vehicle_engine_on(tow_truck.veh, true, true)
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	start_playback_recorded_vehicle(tow_truck.veh, 012, "lkcountry")
	skip_time_in_playback_recorded_vehicle(tow_truck.veh, 6250)
	set_playback_speed(tow_truck.veh, 0.0)//set the speed to 1.0 once everything is loaded
	force_playback_recorded_vehicle_update(tow_truck.veh)
	set_vehicle_as_restricted(tow_truck.veh, 1)

	clear_ped_tasks_immediately(player_ped_id())
	set_current_selector_ped(selector_ped_franklin, false)
	SET_STORED_FBI4_OUTFIT(player_ped_id())
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	SET_ENTITY_COORDS(player_ped_id(), <<890.4393, -2346.4761, 29.3413>>)
	SET_ENTITY_HEADING(player_ped_id(), 184.3769) 
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	add_ped_for_dialogue(scripted_speech[0], 1, player_ped_id(), "franklin")
	set_ped_into_vehicle(player_ped_id(), tow_truck.veh)
	SET_VEH_RADIO_STATION(tow_truck.veh, "OFF")
	
	rubbish_truck.veh = create_vehicle(rubbish_truck.model, <<906.2, -2375.5010, 30.5320>>, 250.22)//200.00)
	set_vehicle_on_ground_properly(rubbish_truck.veh)
	set_vehicle_has_strong_axles(rubbish_truck.veh, true)
	set_vehicle_automatically_attaches(rubbish_truck.veh, false)
	set_entity_proofs(rubbish_truck.veh, true, true, true, true, true)
	set_vehicle_as_restricted(rubbish_truck.veh, 0)
	//SET_SIREN_CAN_BE_CONTROLLED_BY_AUDIO(rubbish_truck.veh, false) 
	//set_vehicle_livery(rubbish_truck.veh, 0)
	//SET_VEHICLE_SIREN(rubbish_truck.veh, true)
	
	create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_michael], char_michael, rubbish_truck.veh, vs_driver, false)
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_michael])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech[0], 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
	
	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 
	
	streaming_volume = STREAMVOL_CREATE_SPHERE(<<904.981, -2367.179, 30.150>>, 7.0, FLAG_COLLISIONS_MOVER)
	
	while not STREAMVOL_HAS_LOADED(streaming_volume)
		wait(0)
	endwhile

	army_truck.veh = create_vehicle(army_truck.model, <<904.981, -2367.179, 30.150>>, 175.2936)
	set_vehicle_has_strong_axles(army_truck.veh, true)
	set_vehicle_on_ground_properly(army_truck.veh)
	set_vehicle_automatically_attaches(army_truck.veh, false)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(army_truck.veh, "FBI_4_STOCKADE_GROUP")

	army_truck_driver.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_driver.model)
	set_ped_dies_when_injured(army_truck_driver.ped, true)
	//DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_driver.ped)
	DISABLE_PED_PAIN_AUDIO(army_truck_driver.ped, true)
	set_blocking_of_non_temporary_events(army_truck_driver.ped, true)
	
	army_truck_passenger.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_passenger.model, vs_front_right) 
	set_ped_dies_when_injured(army_truck_passenger.ped, true)
	//DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_passenger.ped)
	DISABLE_PED_PAIN_AUDIO(army_truck_passenger.ped, true)
	set_blocking_of_non_temporary_events(army_truck_passenger.ped, true)
	
	//NEW_LOAD_SCENE_START(get_entity_coords(player_ped_id()), CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 145.8395>>), 3.0)

	wait(0) //must wait a frame to ensure the outfit variaitons have been applied before checking the streaming requests for the player
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
		
		wait(0)
		
	endwhile 
	
	if is_vehicle_driveable(tow_truck.veh)
		set_playback_speed(tow_truck.veh, 1.0)
	endif 
	
	trigger_music_event("fbi4_TRUCK_RAM_RESTART_ST")
	
	STREAMVOL_DELETE(streaming_volume)
					
	truck_ram_time = get_game_timer()
	
	players_vehicle_cam_mode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
	SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_first_PERSON)	

	end_cutscene(true, true, 0, 0, false)
	
	print_now("cntry_god3", default_god_text_time, 1)
	
	Set_Replay_Mid_Mission_Stage_With_Name(1, "Ram money truck")
	
	mission_flow = ram_army_truck
	
endproc 

proc load_trip_skip_data_2()

	//temp stops random cars spawning miles away and then driving into the area.
	set_vehicle_population_budget(0)

	set_roads_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	set_ped_paths_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	SET_PED_NON_CREATION_AREA(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
	add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)
	
	//switches off generators near shootout
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, false) //cars near ally way
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, false) //vehicle near cover blocks

	
	clear_area(tow_truck.pos, 1000.00, true)
	clear_area(<<890.4393, -2346.4761, 29.3413>>, 1000.00, true)
	
	if replay_active
		start_replay_setup(<<890.4393, -2346.4761, 29.3413>>, 184.3769, false)
	endif 
	
	request_model(GET_PLAYER_PED_MODEL(char_michael))
	request_model(GET_PLAYER_PED_MODEL(char_franklin))
	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	
	request_model(army_truck_driver.model)
	set_vehicle_model_is_suppressed(army_truck_driver.model, true)
	
	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)
	
	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(army_truck.model)
	set_vehicle_model_is_suppressed(army_truck.model, true)
	
//	request_model(get_weapontype_model(WEAPONTYPE_HEAVYSNIPER))
//	request_weapon_asset(WEAPONTYPE_HEAVYSNIPER)

	request_model(get_weapontype_model(weapontype_combatmg))
	request_weapon_asset(weapontype_combatmg)
	
	request_model(get_weapontype_model(weapontype_carbinerifle))
	request_weapon_asset(weapontype_carbinerifle)
	
	request_model(van_light.model)
	request_model(documents.model)

	request_anim_dict("missfbi4")
	request_anim_dict("misssagrab")
	request_anim_dict("missheat")
	
	REQUEST_WAYPOINT_RECORDING("heat1")
	REQUEST_WAYPOINT_RECORDING("heat2")
	REQUEST_WAYPOINT_RECORDING("heat3")
	
	LOAD_STREAM("Truck_Crash_Stream", "FBI_04_HEAT_SOUNDS")
	
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	
	REQUEST_PTFX_ASSET()
	
	request_vehicle_recording(002, "lkcountry") 
	request_vehicle_recording(003, "lkcountry") 
	request_vehicle_recording(004, "lkcountry") 
	request_vehicle_recording(005, "lkcountry") 
	request_vehicle_recording(010, "lkcountry")
	request_vehicle_recording(011, "lkcountry")
	request_vehicle_recording(012, "lkcountry")
	request_vehicle_recording(014, "lkcountry")
	request_vehicle_recording(015, "lkcountry")
	request_vehicle_recording(016, "lkcountry")
	
	if replay_active
		END_REPLAY_SETUP()
	endif
	
	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(army_truck_driver.model)
	or not has_model_loaded(rubbish_truck.model)
	or not has_model_loaded(tow_truck.model)
	or not has_model_loaded(army_truck.model)
	or not has_model_loaded(get_weapontype_model(weapontype_combatmg))
	or not has_weapon_asset_loaded(weapontype_combatmg)
	or not has_model_loaded(get_weapontype_model(weapontype_carbinerifle))
	or not has_weapon_asset_loaded(weapontype_carbinerifle)
	or not has_model_loaded(van_light.model)
	or not has_model_loaded(documents.model)
	or not has_anim_dict_loaded("missfbi4")
	or not has_anim_dict_loaded("misssagrab")
	or not has_anim_dict_loaded("missheat")
	or not get_is_waypoint_recording_loaded("heat1")
	or not get_is_waypoint_recording_loaded("heat2")
	or not get_is_waypoint_recording_loaded("heat3")
	or not has_vehicle_recording_been_loaded(002, "lkcountry")
	or not has_vehicle_recording_been_loaded(003, "lkcountry")
	or not has_vehicle_recording_been_loaded(004, "lkcountry")
	or not has_vehicle_recording_been_loaded(005, "lkcountry")
	or not has_vehicle_recording_been_loaded(010, "lkcountry")
	or not has_vehicle_recording_been_loaded(011, "lkcountry")
	or not has_vehicle_recording_been_loaded(012, "lkcountry")
	or not has_vehicle_recording_been_loaded(014, "lkcountry")
	or not has_vehicle_recording_been_loaded(015, "lkcountry")
	or not has_vehicle_recording_been_loaded(016, "lkcountry")
	or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	or not LOAD_STREAM("Truck_Crash_Stream", "FBI_04_HEAT_SOUNDS")
	or not HAS_PTFX_ASSET_LOADED()
	
		wait(0)
		
	endwhile 
	
	SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_DESTROYED)
	
	clear_ped_tasks_immediately(player_ped_id())
	set_current_selector_ped(selector_ped_franklin, false)
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
	SET_STORED_FBI4_OUTFIT(player_ped_id())
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	SET_ENTITY_COORDS(player_ped_id(), <<890.4393, -2346.4761, 29.3413>>)
	SET_ENTITY_HEADING(player_ped_id(), 184.3769) 
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	
	setup_franklins_weapons(player_ped_id(), false)
	
	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_STICKYBOMB, 5, true)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB) < 5
			set_ped_ammo(player_ped_id(), WEAPONTYPE_STICKYBOMB, 5)
		endif 
	endif 
	set_current_ped_weapon(player_ped_id(), weapontype_stickybomb, true)
	
	//setup_player_fail_weapon(player_ped_id())
	add_ped_for_dialogue(scripted_speech[0], 1, player_ped_id(), "franklin")

	
	rubbish_truck.veh = create_vehicle(rubbish_truck.model, <<901.9453, -2383.3679, 29.2789>>, 341.1729)//200.00)
	set_vehicle_on_ground_properly(rubbish_truck.veh)
	set_vehicle_has_strong_axles(rubbish_truck.veh, true)
	set_vehicle_automatically_attaches(rubbish_truck.veh, false)
	set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_locked)
	set_entity_proofs(rubbish_truck.veh, true, true, true, true, true)//stops it being destroyed in shootout
	set_vehicle_as_restricted(rubbish_truck.veh, 0)
	
	tow_truck.veh = create_vehicle(tow_truck.model, <<896.0925, -2364.3994, 29.4761>>, 84.1277)
	set_vehicle_colours(tow_truck.veh, 0, 0)
	set_vehicle_on_ground_properly(tow_truck.veh)
	set_vehicle_has_strong_axles(tow_truck.veh, true)
	SET_VEHICLE_DAMAGE(tow_truck.veh, <<0.0, 6.5, 0.5>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	set_vehicle_doors_locked(tow_truck.veh, vehiclelock_locked)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(tow_truck.veh, false)
	setup_vehicle_proofs(tow_truck.veh)
	set_vehicle_as_restricted(tow_truck.veh, 1)

	create_player_ped_on_foot(selector_ped.pedID[SELECTOR_PED_michael], char_michael, <<894.0912, -2349.3481, 29.4448>>, 163.8641, false)
	SET_PED_USING_ACTION_MODE(selector_ped.pedID[SELECTOR_PED_michael], true)
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_michael])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	setup_michaels_weapons(selector_ped.pedID[SELECTOR_PED_michael])
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_michael], true)
	add_ped_for_dialogue(scripted_speech[0], 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
	michael_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_michael])
	set_blip_display(michael_blip, display_blip)
	set_blip_scale(michael_blip, 0.5)
	task_aim_gun_at_coord(selector_ped.pedID[SELECTOR_PED_michael], <<891.74, -2358.18, 30.71>>, -1, true)
	force_ped_motion_state(selector_ped.pedID[SELECTOR_PED_michael], ms_aiming)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(selector_ped.pedID[SELECTOR_PED_michael])
	
	create_player_ped_on_foot(selector_ped.pedID[SELECTOR_PED_TREVOR], char_TREVOR, <<804.7164, -2330.2073, 61.0967>>, 264.4371, false)
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_TREVOR])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	setup_trevors_weapons(selector_ped.pedID[SELECTOR_PED_TREVOR])
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_trevor])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
	add_ped_for_dialogue(scripted_speech[0], 2, selector_ped.pedID[SELECTOR_PED_TREVOR], "TREVOR")
	TREVOR_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_TREVOR])
	set_blip_display(michael_blip, display_blip)
	set_blip_scale(michael_blip, 0.5)
	
	c4.obj = create_object_no_offset(c4.model, <<889.84, -2361.97, 30.58>>)
	set_entity_health(c4.obj, c4.health)
	freeze_entity_position(c4.obj, true)
	set_entity_heading(c4.obj, 90.00)
	c4_planted_manually = true
	
	//wait(0)
	
	//script_assert("test 1")
	
	if not is_ped_injured(army_truck_driver.ped)
		set_entity_health(army_truck_driver.ped, 2)
	endif 
	
	if not is_ped_injured(army_truck_passenger.ped)
		set_entity_health(army_truck_passenger.ped, 2)
	endif 

	if not replay_active
		load_scene(<<890.4393, -2346.4761, 29.3413>>)
	endif 
	
	//truck repositioned after ray fire wall update after load_scene()
	army_truck.veh = create_vehicle(army_truck.model, <<880.0, -2357.4, 30.150>>, 175.2936)
	set_vehicle_has_strong_axles(army_truck.veh, true)
	SET_ENTITY_PROOFS(army_truck.veh, true, true, true, true, true)
	set_entity_coords_no_offset(army_truck.veh, <<890.495, -2365.6, 30.5039>>)
	set_entity_rotation(army_truck.veh, <<0.0, 91.8056, 173.211>>)
	FREEZE_ENTITY_POSITION(army_truck.veh, true)
	SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(army_truck.veh, true)
	SET_VEHICLE_DAMAGE(army_truck.veh, <<-2.0, -0.5, 0.5>>, 1000.00, 1000.00, TRUE)
	set_vehicle_automatically_attaches(army_truck.veh, false)
	set_vehicle_doors_locked(army_truck.veh, vehiclelock_locked)
	SET_HORN_ENABLED(army_truck.veh, false) 
	setup_vehicle_proofs(army_truck.veh)
	
	army_truck_driver.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_driver.model)
	set_ped_dies_when_injured(army_truck_driver.ped, true)
	//DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_driver.ped)
	DISABLE_PED_PAIN_AUDIO(army_truck_driver.ped, true)
	set_blocking_of_non_temporary_events(army_truck_driver.ped, true)
	set_entity_health(army_truck_driver.ped, 2)
	set_ped_as_no_longer_needed(army_truck_driver.ped)
	
	army_truck_passenger.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_passenger.model, vs_front_right) 
	set_ped_dies_when_injured(army_truck_passenger.ped, true)
	//DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_passenger.ped)
	DISABLE_PED_PAIN_AUDIO(army_truck_passenger.ped, true)
	set_blocking_of_non_temporary_events(army_truck_passenger.ped, true)
	set_entity_health(army_truck_passenger.ped, 2)
	set_ped_as_no_longer_needed(army_truck_passenger.ped)
	
	van_light.obj = create_object(van_light.model, van_light.pos)
	set_entity_rotation(van_light.obj, van_light.rot)
	freeze_entity_position(van_light.obj, true)
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[SELECTOR_PED_michael])
	or not HAVE_ALL_STREAMING_REQUESTS_COMPLETED_for_ped(selector_ped.pedID[SELECTOR_PED_trevor])
		wait(0)
	endwhile 
	
	trigger_music_event("FBI4_EXPLODE_RESTART_ST")
	
	michael_ai_system_status = 1
	
	blow_open_truck_doors_status = 2
	
	blow_open_truck_doors_dialogue_status = 22
	
	plant_bomb_time = get_game_timer()//sets fail timer for planting sticky bombs.
	
	Set_Replay_Mid_Mission_Stage_With_Name(2, "Detonate C4")
	
	end_cutscene(false, true, 0, 0, false)
	
	//print_now("cntry_god8", default_god_text_time, 1)
	
	mission_flow = blow_open_truck_doors 
				
	//script_assert("test 0")

endproc 

proc load_trip_skip_data_3()
	
	set_roads_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	set_ped_paths_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	SET_PED_NON_CREATION_AREA(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
	add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)
	
	//switches off generators near shootout
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, false) //cars near ally way
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, false) //vehicle near cover blocks

	clear_area(tow_truck.pos, 1000.00, true)
	clear_area(<<890.4393, -2346.4761, 29.3413>>, 1000.00, true)
	
	if replay_active
		start_replay_setup(<<873.9058, -2353.0959, 29.3312>>, 13.2, false)
	endif 
	
	request_model(GET_PLAYER_PED_MODEL(char_michael))
	request_model(GET_PLAYER_PED_MODEL(char_franklin))
	request_model(GET_PLAYER_PED_MODEL(char_trevor))
	request_model(police_man[0].model)
	request_model(police_man[8].model)
	
	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)
	
	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(army_truck.model)
	set_vehicle_model_is_suppressed(army_truck.model, true)

	request_model(police_car[0].model)
	request_model(helicopter[0].model)
	
	request_model(van_light.model)

	request_vehicle_asset(police_car[0].model, enum_to_int(VRF_REQUEST_ALL_ANIMS))
	
	REQUEST_WAYPOINT_RECORDING("heat1")
	REQUEST_WAYPOINT_RECORDING("heat2")
	REQUEST_WAYPOINT_RECORDING("heat3")
	
	request_vehicle_recording(004, "lkcountry") //tow truck
	request_vehicle_recording(helicopter[0].recording_number, "lkfbi4")
	request_vehicle_recording(helicopter[1].recording_number, "lkfbi4")
	request_vehicle_recording(police_car[0].recording_number, "lkfbi4") 
	request_vehicle_recording(police_car[1].recording_number, "lkfbi4") 
	request_vehicle_recording(police_car[2].recording_number, "lkfbi4") 
	request_vehicle_recording(police_car[3].recording_number, "lkfbi4") 
	request_vehicle_recording(police_car[4].recording_number, "lkfbi4") 
	request_vehicle_recording(police_car[5].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[6].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[7].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[8].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[9].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[10].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[11].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[12].recording_number, "lkfbi4")
//	request_vehicle_recording(police_car[13].recording_number, "lkfbi4")
	
	LOAD_STREAM("Truck_Crash_Stream", "FBI_04_HEAT_SOUNDS")
	
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	
	if replay_active
		END_REPLAY_SETUP()
	endif 
	
	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(police_man[0].model)
	or not has_model_loaded(police_man[8].model)
	or not has_model_loaded(rubbish_truck.model)
	or not has_model_loaded(tow_truck.model)
	or not has_model_loaded(army_truck.model)
	or not has_model_loaded(police_car[0].model)
	or not has_model_loaded(helicopter[0].model)
	or not has_model_loaded(van_light.model)
	or not has_vehicle_asset_loaded(police_car[0].model)
	or not get_is_waypoint_recording_loaded("heat1")
	or not get_is_waypoint_recording_loaded("heat2")
	or not get_is_waypoint_recording_loaded("heat3")
	or not has_vehicle_recording_been_loaded(004, "lkcountry")
	or not has_vehicle_recording_been_loaded(helicopter[0].recording_number, "lkfbi4")
	or not has_vehicle_recording_been_loaded(helicopter[1].recording_number, "lkfbi4")
	or not has_vehicle_recording_been_loaded(police_car[0].recording_number, "lkfbi4") 
	or not has_vehicle_recording_been_loaded(police_car[1].recording_number, "lkfbi4") 
	or not has_vehicle_recording_been_loaded(police_car[2].recording_number, "lkfbi4") 
	or not has_vehicle_recording_been_loaded(police_car[3].recording_number, "lkfbi4") 
	or not has_vehicle_recording_been_loaded(police_car[4].recording_number, "lkfbi4") 
	or not has_vehicle_recording_been_loaded(police_car[5].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[6].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[7].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[8].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[9].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[10].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[11].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[12].recording_number, "lkfbi4")
//	or not has_vehicle_recording_been_loaded(police_car[13].recording_number, "lkfbi4")
	or not LOAD_STREAM("Truck_Crash_Stream", "FBI_04_HEAT_SOUNDS")
	or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
			
		wait(0)
		
	endwhile 
	
	clear_area(<<890.4393, -2346.4761, 29.3413>>, 10000.00, true)
	
	SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_DESTROYED)
	
	DISTANT_COP_CAR_SIRENS(true)
	
	rubbish_truck.veh = create_vehicle(rubbish_truck.model, <<901.9453, -2383.3679, 29.2789>>, 341.1729)//200.00)
	set_vehicle_has_strong_axles(rubbish_truck.veh, true)
	set_vehicle_automatically_attaches(rubbish_truck.veh, false)
	set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_locked)
	set_entity_proofs(rubbish_truck.veh, true, true, true, true, true)//stops it being destroyed in shootout
	set_vehicle_as_restricted(rubbish_truck.veh, 0)
	
	tow_truck.veh = create_vehicle(tow_truck.model, <<896.0925, -2364.3994, 29.4761>>, 84.1277)
	set_vehicle_colours(tow_truck.veh, 0, 0)
	set_vehicle_has_strong_axles(tow_truck.veh, true)
	SET_VEHICLE_DAMAGE(tow_truck.veh, <<0.0, 6.5, 0.5>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	set_vehicle_doors_locked(tow_truck.veh, vehiclelock_locked)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(tow_truck.veh, false)
	setup_vehicle_proofs(tow_truck.veh)
	set_vehicle_as_restricted(tow_truck.veh, 1)

	clear_ped_tasks_immediately(player_ped_id())
	set_current_selector_ped(selector_ped_franklin, false)
	SET_PED_USING_ACTION_MODE(player_ped_id(), true)
	SET_STORED_FBI4_OUTFIT(player_ped_id())
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	SET_ENTITY_COORDS(player_ped_id(), <<873.9058, -2353.0959, 29.3312>>)
	SET_ENTITY_HEADING(player_ped_id(), 13.9200)//314.9656)
	
	setup_franklins_weapons(player_ped_id())
	//setup_weapon_for_ped(player_ped_id(), weapontype_combatmg, 800, true, true, false)
	//setup_player_fail_weapon(player_ped_id())
	task_put_ped_directly_into_cover(player_ped_id(), <<873.2560, -2352.7104, 29.3307>>, -1, true, 0, true, true, players_cover_point)
	force_ped_ai_and_animation_update(player_ped_id(), true)
	add_ped_for_dialogue(scripted_speech[0], 1, player_ped_id(), "franklin")

	create_player_ped_on_foot(selector_ped.pedID[SELECTOR_PED_TREVOR], char_TREVOR, <<804.7164, -2330.2073, 61.0967>>, 264.4371, false)
	SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_trevor], true)
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_trevor])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	setup_trevors_weapons(selector_ped.pedID[SELECTOR_PED_TREVOR])
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_trevor])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
	add_ped_for_dialogue(scripted_speech[0], 2, selector_ped.pedID[SELECTOR_PED_TREVOR], "TREVOR")
	TREVOR_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_TREVOR])
	set_blip_display(TREVOR_blip, display_blip)
	set_blip_scale(TREVOR_blip, 0.5)
	SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped_trevor], 1500)//2000 4000)

	create_player_ped_on_foot(selector_ped.pedID[SELECTOR_PED_michael], char_michael, michael_run_to_pos, 262.00, false)
	SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_michael], true)
	SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_michael])
	SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	add_ped_for_dialogue(scripted_speech[0], 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
	michael_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_michael])
	set_blip_display(michael_blip, display_blip)
	set_blip_scale(michael_blip, 0.5)
	SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped_michael], 1500)//2000 4000)
	
	setup_michaels_weapons(selector_ped.pedID[SELECTOR_PED_michael])
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_michael], true)
	task_put_ped_directly_into_cover(selector_ped.pedID[SELECTOR_PED_michael], <<881.53, -2334.04, 33.91>>, -1, false, 0, false, false, michael_cover_point)
	michael_ai_system_status = 6


	if is_vehicle_driveable(tow_truck.veh)
		FREEZE_ENTITY_POSITION(tow_truck.veh,false)
		SET_ENTITY_PROOFS(tow_truck.veh, false, false, false, false, false)
	endif 
	
	switch fbi4_shootout_wanted_level
	
//		case 1
//		case 2
//		
//			clear_player_wanted_level(player_id())
//			//g_allowmaxwantedlevelcheck = false
//			set_max_wanted_level(0)
//			set_create_random_cops(false)
//			set_fake_wanted_level(2)
//			
//			original_time = get_game_timer()
//			original_time += 8000
//		
//		break 
		
		case 1
		case 2
		case 3
		case 4
		
			clear_player_wanted_level(player_id())
			//g_allowmaxwantedlevelcheck = false
			set_max_wanted_level(0)
			set_create_random_cops(false)
			set_fake_wanted_level(4)
			
			dialogue_status = 4
			michael_ai_system_status = 6
			shootout_master_controler_status = 1
			
//			forces the time on for the allow_switch flag in the shootout_master_controler_status = 1
			shootout_time = get_game_timer()
			shootout_time -= 6000

		break 

	endswitch 

	if not replay_active
		load_scene(<<873.3599, -2352.4114, 29.3306>>)
	endif 
	
	//create truck after load scene to allow the ray fire wall to update before placing truck.
	army_truck.veh = create_vehicle(army_truck.model, <<904.981, -2367.179, 30.150>>, 175.2936)
	set_vehicle_has_strong_axles(army_truck.veh, true)
	set_entity_coords_no_offset(army_truck.veh, <<890.495, -2365.6, 30.5039>>)
	set_entity_rotation(army_truck.veh, <<0.0, 91.8056, 173.211>>)
	FREEZE_ENTITY_POSITION(army_truck.veh, true)
	SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(army_truck.veh, true)
	SET_VEHICLE_DAMAGE(army_truck.veh, <<-2.0, -0.5, 0.5>>, 1000.00, 1000.00, TRUE)
	set_vehicle_door_broken(army_truck.veh, sc_door_rear_left, true)
	set_vehicle_door_broken(army_truck.veh, sc_door_rear_right, true)
	set_vehicle_automatically_attaches(army_truck.veh, false)
	set_vehicle_doors_locked(army_truck.veh, vehiclelock_locked)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(army_truck.veh, false)
	setup_vehicle_proofs(army_truck.veh)

	set_vehicle_door_broken(army_truck.veh, sc_door_rear_left, false)
	set_vehicle_door_broken(army_truck.veh, sc_door_rear_right, false)
		
	clear_area(<<904.981, -2367.179, 30.150>>, 100, true) //clear doors

	van_light.obj = create_object(van_light.model, van_light.pos)
	set_entity_rotation(van_light.obj, van_light.rot)
	freeze_entity_position(van_light.obj, true)
	
	
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
	or not (not is_ped_injured(selector_ped.pedID[SELECTOR_PED_michael]) and HAVE_ALL_STREAMING_REQUESTS_COMPLETED(selector_ped.pedID[SELECTOR_PED_michael]))
		wait(0)
	endwhile 
	
	PLAY_SOUND_FROM_coord(police_siren_sound, "Distant_Sirens_Skip_Start", <<913.6, -2301.6, 34.00>>, "FBI_04_HEAT_SOUNDS") 
	
	trigger_music_event("FBI4_COVER_RESTART")

	army_truck_guy_ai_status = 22
	army_truck_guy.created = true
	
	army_truck_guy_ai_2_status = 22
	army_truck_guy_2.created = true

	end_cutscene(false, true, -88, 0, false)//-90
	
	Set_Replay_Mid_Mission_Stage_With_Name(3, "Start of Shootout")

	mission_flow = shootout
		
endproc 

proc load_trip_skip_data_4()

	//might be the roads off = ram truck cutscene
	set_roads_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	set_ped_paths_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
	SET_PED_NON_CREATION_AREA(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
	add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>) //1013.58,, -2445.88
	
//	switches off generators near shootout
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, false) //cars near ally way
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, false) //vehicle near cover blocks

	clear_area(<<890.4393, -2346.4761, 29.3413>>, 10000.00, true)
	
	if replay_active
		start_replay_setup(<<897.9976, -2351.9265, 29.4873>>, 203.22, false)
	endif 

	request_model(GET_PLAYER_PED_MODEL(char_michael))

	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)

	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)

	request_model(army_truck.model)
	set_vehicle_model_is_suppressed(army_truck.model, true)
	
	request_model(police_car[0].model)
	
	request_model(van_light.model)

//	request_model(police)
//	request_model(riot)

	request_vehicle_recording(002, "lkcountry") 
	request_vehicle_recording(003, "lkcountry") 
	request_vehicle_recording(004, "lkcountry") 
	request_vehicle_recording(005, "lkcountry") 
	request_vehicle_recording(010, "lkcountry")
	request_vehicle_recording(011, "lkcountry")
	request_vehicle_recording(012, "lkcountry")
	request_vehicle_recording(014, "lkcountry")
	request_vehicle_recording(015, "lkcountry")
	request_vehicle_recording(016, "lkcountry")

	request_anim_dict("missfbi4")
	
	REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	
	if replay_active
		END_REPLAY_SETUP()
	endif

//	while not has_model_loaded(S_M_Y_Cop_01)
//	or not has_model_loaded(S_M_Y_SWAT_01)
	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))	
//	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	or not has_model_loaded(rubbish_truck.model)
	or not has_model_loaded(tow_truck.model)
	or not has_model_loaded(army_truck.model)
	or not has_model_loaded(police_car[0].model)
	or not has_model_loaded(van_light.model)
//	or not has_model_loaded(police)
//	or not has_model_loaded(riot)
	or not has_anim_dict_loaded("missfbi4")
	or not has_vehicle_recording_been_loaded(002, "lkcountry")
	or not has_vehicle_recording_been_loaded(003, "lkcountry")
	or not has_vehicle_recording_been_loaded(004, "lkcountry")
	or not has_vehicle_recording_been_loaded(005, "lkcountry")
	or not has_vehicle_recording_been_loaded(010, "lkcountry")
	or not has_vehicle_recording_been_loaded(011, "lkcountry")
	or not has_vehicle_recording_been_loaded(012, "lkcountry")
	or not has_vehicle_recording_been_loaded(014, "lkcountry")
	or not has_vehicle_recording_been_loaded(015, "lkcountry")
	or not has_vehicle_recording_been_loaded(016, "lkcountry")
	or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
	
		wait(0)
	
	endwhile 
	
	PLAY_SOUND_FROM_coord(police_siren_sound, "SIRENS_DISTANT_01_MASTER", <<844.7, -2213.3, 57.9>>)
	
	SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_DESTROYED)

	disable_dispatch_services()
	
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, true)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, true)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_AUTOMOBILE, true)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER, true)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_VEHICLE_REQUEST, true)
   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_ROAD_BLOCK, true)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, true)
   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, true)
	
	
	clear_player_wanted_level(player_id())
	set_max_wanted_level(0)
	set_create_random_cops(false)
	set_fake_wanted_level(1)
	
	rubbish_truck.veh = create_vehicle(rubbish_truck.model, <<901.9453, -2383.3679, 29.2789>>, 341.1729)//200.00)
	set_vehicle_has_strong_axles(rubbish_truck.veh, true)
	set_vehicle_automatically_attaches(rubbish_truck.veh, false)
	set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_locked)
	set_entity_proofs(rubbish_truck.veh, true, true, true, true, true)//stops it being destroyed in shootout
	set_vehicle_as_restricted(rubbish_truck.veh, 0)

	tow_truck.veh = create_vehicle(tow_truck.model, <<896.0925, -2364.3994, 29.4761>>, 84.1277)
	set_vehicle_colours(tow_truck.veh, 0, 0)
	set_vehicle_has_strong_axles(tow_truck.veh, true)
	SET_VEHICLE_DAMAGE(tow_truck.veh, <<0.0, 6.5, 0.5>>, 375.00, 375.00, TRUE)
	SET_VEHICLE_DAMAGE(tow_truck.veh, <<-2.0, -0.5, 0.5>>, 1000.00, 1000.00, TRUE)
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	set_vehicle_doors_locked(tow_truck.veh, vehiclelock_locked)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(tow_truck.veh, false)
	setup_vehicle_proofs(tow_truck.veh)
	set_vehicle_as_restricted(tow_truck.veh, 1)
	
	police_car[0].veh = create_vehicle(police_car[0].model, <<891.0364, -2354.0908, 29.3747>>, 64.0679)
	SET_VEHICLE_DAMAGE(police_car[0].veh, <<0.0, 6.5, 0.5>>, 500.00, 500.00, TRUE)
	set_vehicle_on_ground_properly(police_car[0].veh)
	
	police_car[1].veh = create_vehicle(police_car[1].model, <<895.6796, -2344.6904, 29.4389>>, 101.3151)
	SET_VEHICLE_DAMAGE(police_car[1].veh, <<-0.5, 6.5, 0.5>>, 500.00, 500.00, TRUE)
	set_vehicle_on_ground_properly(police_car[1].veh)
	
	police_car[2].veh = create_vehicle(police_car[2].model, <<902.2714, -2343.1741, 29.5052>>, 150.3280)
	SET_VEHICLE_DAMAGE(police_car[2].veh, <<0.5, 6.5, 0.5>>, 500.00, 500.00, TRUE)
	set_vehicle_on_ground_properly(police_car[2].veh)
	
	police_car[3].veh = create_vehicle(police_car[3].model, <<917.7601, -2331.9087, 29.3799>>, 176.3431)
	set_vehicle_on_ground_properly(police_car[3].veh)
	
	
	clear_ped_tasks_immediately(player_ped_id())
	if get_current_player_ped_enum() != char_franklin
		set_current_selector_ped(selector_ped_franklin, false)
	endif 
	SET_STORED_FBI4_OUTFIT(player_ped_id())
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	//setup_michaels_weapons(player_ped_id())
	setup_player_fail_weapon(player_ped_id())
	set_entity_coords(player_ped_id(), <<897.9976, -2351.9265, 29.4873>>)
	set_entity_heading(player_ped_id(), 203.2388)
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	add_ped_for_dialogue(scripted_speech[0], 1, player_ped_id(), "franklin")

	if GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, vehgen_s_flag_available)
		GET_VEHICLE_GEN_DATA(get_away_vehicle, VEHGEN_MISSION_VEH_FBI4_PREP)
	ELSE
		create_debug_getaway_car = true
		get_away_vehicle.coords = <<1121.0305, -1249.9076, 19.5717>>
	endif 
	
	//stops peds being created by army_truck_guy_ai_system()
	army_truck_guy.created = true
	army_truck_guy_2.created = true

	if not replay_active
		load_scene(<<890.4393, -2346.4761, 29.3413>>)
	endif 
	
	army_truck.veh = create_vehicle(army_truck.model, <<904.981, -2367.179, 30.150>>, 175.2936)
	set_vehicle_has_strong_axles(army_truck.veh, true)
	//start_playback_recorded_vehicle(army_truck.veh, 003, "lkcountry")
	//skip_to_end_and_stop_playback_recorded_vehicle(army_truck.veh)
	set_entity_coords_no_offset(army_truck.veh, <<890.495, -2365.6, 30.5039>>)
	set_entity_rotation(army_truck.veh, <<0.0, 91.8056, 173.211>>)
	FREEZE_ENTITY_POSITION(army_truck.veh, true)
	SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(army_truck.veh, true)
	SET_VEHICLE_DAMAGE(army_truck.veh, <<-2.0, -0.5, 0.5>>, 1000.00, 1000.00, TRUE)
	set_vehicle_door_broken(army_truck.veh, sc_door_rear_left, true)
	set_vehicle_door_broken(army_truck.veh, sc_door_rear_right, true)
	set_vehicle_automatically_attaches(army_truck.veh, false)
	set_vehicle_doors_locked(army_truck.veh, vehiclelock_locked)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(army_truck.veh, false)
	setup_vehicle_proofs(army_truck.veh)
	
	van_light.obj = create_object(van_light.model, van_light.pos)
	set_entity_rotation(van_light.obj, van_light.rot)
	freeze_entity_position(van_light.obj, true)
	
	original_time = get_game_timer()

	//stops damage effect been seen
	while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
	or not lk_timer(original_time, 2000)
		wait(0)
	endwhile 
	
	//forces rubbish truck vehicle blip to render if player has a wanted level
	DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(locates_data, TRUE) 
	
	trigger_music_event("FBI4_GETAWAY_RESTART")
	
	Set_Replay_Mid_Mission_Stage_With_Name(4, "burn the truck", true)

	mission_flow = burn_rubbish_truck 
		
	end_cutscene(false, true, 0, 0, false)

endproc

proc load_trip_skip_data_5()

	if replay_active
		start_replay_setup(<<-2557.4751, 1912.8906, 167.8713>>, 46.22)
	endif 

	request_model(devin.model)
	request_model(devin_car.model)
	request_model(michaels_car.model)
	request_model(get_player_ped_model(char_michael))
	
	if replay_active
		END_REPLAY_SETUP()
	endif 

	while not has_model_loaded(devin.model)
	or not has_model_loaded(devin_car.model)
	or not has_model_loaded(michaels_car.model)
	or not has_model_loaded(get_player_ped_model(char_michael))
	
		wait(0)
		
	endwhile 
	
	clear_area(<<-2557.4751, 1912.8906, 167.8713>>, 1000.00, true)
	
	clear_ped_tasks_immediately(player_ped_id())
	set_current_selector_ped(selector_ped_michael)
	SET_STORED_FBI4_OUTFIT(player_ped_id())
	SET_PED_COMP_ITEM_CURRENT_SP(player_ped_id(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	set_entity_coords(player_ped_id(), <<-2557.4751, 1912.8906, 167.8713>>)
	set_entity_heading(player_ped_id(), 46.2899) 
			
	devin_car.veh = create_vehicle(devin_car.model, devin_car.pos, devin_car.heading)
	remove_vehicle_window(devin_car.veh, sc_window_front_left) //sc_window_list
	remove_vehicle_window(devin_car.veh, sc_window_front_right)
	set_vehicle_on_ground_properly(devin_car.veh)
	set_vehicle_colours(devin_car.veh, 0, 0)

	create_npc_ped_on_foot(devin.ped, char_devin, devin.pos, devin.heading)
	setup_buddy_attributes(devin.ped)
	setup_relationship_contact(devin.ped, true)
	set_blocking_of_non_temporary_events(devin.ped, true)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_TORSO, 2, 1)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_LEG, 2, 1)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_SPECIAL, 4, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_DECL, 1, 0)
	SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_JBIB, 0, 0)
	
	CREATE_PLAYER_VEHICLE(michaels_car.veh, CHAR_michael, <<-2551.9309, 1912.0355, 168.0432>>, 244.1441, false)
	
	request_cutscene("FBI_4_MCS_2_CONCAT")
		
	while not has_cutscene_loaded()
	
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", PLAYER_PED_ID())
		
		if not is_ped_injured(devin.ped)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("devin", devin.ped)
		endif 

		wait(0)
		
	endwhile 
	
	if not replay_active
		load_scene(get_entity_coords(player_ped_id()))
	endif 

	if not is_ped_injured(devin.ped)
		register_entity_for_cutscene(devin.ped, "devin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, devin.model)
	endif 
	
	if is_vehicle_driveable(devin_car.veh)
		register_entity_for_cutscene(devin_car.veh, "devins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, devin_car.model)
	endif 

	SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
	
	start_cutscene()
	
	while not is_cutscene_playing()
		wait(0)
	endwhile
	
	fbi4_pass_cutscene_status = 1

	do_screen_fade_in(default_fade_time)
	
	mission_flow = pass_cutscene
	
endproc 

proc fbi4_load_stage_selector_assets()

	initialise_mission_variables()	
	
	//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(players_vehicle_cam_mode)
	
	switch launch_mission_stage_menu_status
	
		case 0
		
			load_trip_skip_data_0()	//getting into the ambulance at FIB depot
		
		break 
	
		case 1
		
			set_roads_in_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false)
			set_ped_paths_in_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false) 
			add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)
			
			load_all_objects_now()
			
			set_current_selector_ped(selector_ped_trevor, false)
			
			//remove_all_ped_weapons(player_ped_id())
			set_entity_coords(player_ped_id(), <<790.8106, -2330.1296, 61.2638>>)
			set_entity_heading(player_ped_id(), 137.0764) 
			
			add_ped_for_dialogue(scripted_speech[0], 2, player_ped_id(), "trevor")
			add_ped_for_dialogue(scripted_speech[0], 0, null, "michael")
			
			fbi4_vehicle_spotted_status = 4
			
			request_model(GET_PLAYER_PED_MODEL(char_michael))
			request_model(GET_PLAYER_PED_MODEL(char_franklin))
			
			request_model(rubbish_truck.model)
			set_vehicle_model_is_suppressed(rubbish_truck.model, true)
			request_model(tow_truck.model)
			set_vehicle_model_is_suppressed(tow_truck.model, true)
			
			request_model(c4.model)
			
			request_vehicle_recording(002, "lkcountry") 
			request_vehicle_recording(003, "lkcountry") 
			request_vehicle_recording(004, "lkcountry") 
			request_vehicle_recording(005, "lkcountry") 
			request_vehicle_recording(010, "lkcountry")
			request_vehicle_recording(011, "lkcountry")
			request_vehicle_recording(012, "lkcountry")
			request_vehicle_recording(014, "lkcountry")
			request_vehicle_recording(015, "lkcountry")
			request_vehicle_recording(016, "lkcountry")
			
			request_anim_dict("missfbi4")
			request_anim_dict("misssagrab")
			
			REQUEST_WAYPOINT_RECORDING("heat1")
			REQUEST_WAYPOINT_RECORDING("heat2")
			REQUEST_WAYPOINT_RECORDING("heat3")

			request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
			REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
			
			load_scene(<<790.8106, -2330.1296, 61.2638>>)
			
			end_cutscene(true, true, 0, 0, false)
			
			mission_flow = vehicle_spotted

		break 
		
		case 2
		
			load_trip_skip_data_1() //ram the money truck
			
		break 
		
		case 3
		
			load_trip_skip_data_2()	//detonate c4
			
			setup_franklins_weapons(player_ped_id(), false)
			setup_franklins_weapons(player_ped_id(), false) 
			if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB)
				give_weapon_to_ped(player_ped_id(), WEAPONTYPE_STICKYBOMB, 5, true)
			else 
				if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB) < 5
					set_ped_ammo(player_ped_id(), WEAPONTYPE_STICKYBOMB, 5)
				endif 
			endif 
			set_current_ped_weapon(player_ped_id(), weapontype_stickybomb, true)
				
		break 
		
		case 4
		
			fbi4_shootout_wanted_level = 4 
		
			//shootout removed 
			load_trip_skip_data_3() //shootout
			//only give the player a specific weapon if they z skip.
			//setup_weapon_for_ped(player_ped_id(), weapontype_combatmg, 800, true, true, false)

		break 
		
		case 5
		
			load_trip_skip_data_4() //burn rubbish truck
			
		break 
		
		case 6
			
			load_trip_skip_data_5() //shit skip mission pass
		
		break 
	
	endswitch
	
	#IF IS_DEBUG_BUILD
		p_skip_time = get_game_timer()
	#ENDIF 

endproc 

proc request_michael_variation_data()

//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_HEAD, 0, 0) 
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_BERD, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_HAIR, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_TORSO, 9, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_LEG, 8, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_HAND, 2, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_FEET, 7, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_TEETH, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_SPECIAL, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_SPECIAL2, 0, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_DECL, 2, 0)
//	SET_PED_PRELOAD_VARIATION_DATA(player_ped_id(), PED_COMP_JBIB, 0, 0)
	
endproc 

proc load_fbi4_recordings()

	request_vehicle_recording(001, "lkcountry") 

	request_vehicle_recording(001, "lkheat") 
	request_vehicle_recording(002, "lkheat") 
	request_vehicle_recording(003, "lkheat") 
	request_vehicle_recording(004, "lkheat") 
	request_vehicle_recording(005, "lkheat") 
	request_vehicle_recording(010, "lkheat") 
	request_vehicle_recording(011, "lkheat") 
	request_vehicle_recording(012, "lkheat") 
	request_vehicle_recording(013, "lkheat") 
	request_vehicle_recording(014, "lkheat") 
	request_vehicle_recording(015, "lkheat") 
	request_vehicle_recording(016, "lkheat") 
	request_vehicle_recording(017, "lkheat") 
	request_vehicle_recording(018, "lkheat") 
	request_vehicle_recording(019, "lkheat") 
	request_vehicle_recording(020, "lkheat") 
	request_vehicle_recording(021, "lkheat") 
	request_vehicle_recording(021, "lkheat") 
	request_vehicle_recording(022, "lkheat") 
	request_vehicle_recording(023, "lkheat") 
	request_vehicle_recording(024, "lkheat") 
	request_vehicle_recording(025, "lkheat") 
	request_vehicle_recording(026, "lkheat") 
	request_vehicle_recording(027, "lkheat") 
	
	request_vehicle_recording(101, "lkheat") 
	request_vehicle_recording(102, "lkheat") 
	
	request_vehicle_recording(201, "lkheat") 
	request_vehicle_recording(202, "lkheat") 
	request_vehicle_recording(203, "lkheat") 
	request_vehicle_recording(204, "lkheat") 

endproc 

func bool has_fbi4_recordings_loaded()

	if has_vehicle_recording_been_loaded(001, "lkcountry")
	and has_vehicle_recording_been_loaded(001, "lkheat")
	and has_vehicle_recording_been_loaded(002, "lkheat")
	and has_vehicle_recording_been_loaded(003, "lkheat") 
	and has_vehicle_recording_been_loaded(004, "lkheat")
	and has_vehicle_recording_been_loaded(005, "lkheat") 
	and has_vehicle_recording_been_loaded(010, "lkheat") 
	and has_vehicle_recording_been_loaded(011, "lkheat") 
	and has_vehicle_recording_been_loaded(012, "lkheat") 
	and has_vehicle_recording_been_loaded(013, "lkheat") 
	and has_vehicle_recording_been_loaded(014, "lkheat")
	and has_vehicle_recording_been_loaded(015, "lkheat")
	and has_vehicle_recording_been_loaded(016, "lkheat") 
	and has_vehicle_recording_been_loaded(017, "lkheat")
	and has_vehicle_recording_been_loaded(018, "lkheat") 
	and has_vehicle_recording_been_loaded(019, "lkheat") 
	and has_vehicle_recording_been_loaded(020, "lkheat") 
	and has_vehicle_recording_been_loaded(021, "lkheat") 
	and has_vehicle_recording_been_loaded(021, "lkheat")
	and has_vehicle_recording_been_loaded(022, "lkheat")
	and has_vehicle_recording_been_loaded(023, "lkheat")
	and has_vehicle_recording_been_loaded(024, "lkheat") 
	and has_vehicle_recording_been_loaded(025, "lkheat")
	and has_vehicle_recording_been_loaded(026, "lkheat")
	and has_vehicle_recording_been_loaded(027, "lkheat")
	and has_vehicle_recording_been_loaded(101, "lkheat") 
	and has_vehicle_recording_been_loaded(102, "lkheat") 
	and has_vehicle_recording_been_loaded(201, "lkheat")
	and has_vehicle_recording_been_loaded(202, "lkheat")
	and has_vehicle_recording_been_loaded(203, "lkheat")
	and has_vehicle_recording_been_loaded(204, "lkheat")
	
		return true 
	
	endif 
	
	return false 

endfunc 


proc create_assets_for_get_ambulance_into_pos()

	RELEASE_PED_PRELOAD_VARIATION_DATA(player_ped_id())
	RELEASE_PED_PRELOAD_VARIATION_DATA(SELECTOR_PED.pedID[SELECTOR_PED_franklin])
	RELEASE_PED_PRELOAD_VARIATION_DATA(SELECTOR_PED.pedID[SELECTOR_PED_trevor])

	//clear_area(<<1404.9, -2043.76, 58.33>>, 100.00, true)

	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	add_ped_for_dialogue(scripted_speech[0], 0, player_ped_id(), "michael")
	
	set_ped_into_vehicle(selector_ped.pedID[SELECTOR_PED_TREVOR], trevors_car.veh)
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_TREVOR])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
	add_ped_for_dialogue(scripted_speech[0], 2, selector_ped.pedID[SELECTOR_PED_TREVOR], "trevor")	
					
	set_ped_into_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], tow_truck.veh)
	set_entity_load_collision_flag(selector_ped.pedID[SELECTOR_PED_FRANKLIN], true)
	setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
	setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_FRANKLIN], true)
	add_ped_for_dialogue(scripted_speech[0], 1, selector_ped.pedID[SELECTOR_PED_franklin], "franklin")
	
	set_entity_visible(rubbish_truck.veh, true)
	freeze_entity_position(rubbish_truck.veh, false)
	set_entity_collision(rubbish_truck.veh, true)
	set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_unlocked)
	set_vehicle_as_restricted(rubbish_truck.veh, 0)

	
	set_entity_visible(trevors_car.veh, true)
	freeze_entity_position(trevors_car.veh, false)
	set_entity_collision(trevors_car.veh, true)
	#IF IS_DEBUG_BUILD
		set_uber_parent_widget_group(fbi_4_widget_group)
	#endif 
	INITIALISE_UBER_PLAYBACK("lkheat", 101, true)
	load_uber_data()
	fUberPlaybackDensitySwitchOffRange = 200
	switch_SetPieceCar_to_ai_on_collision = true
	allow_veh_to_stop_on_any_veh_impact = true
	start_playback_recorded_vehicle(trevors_car.veh, 101, "lkheat")
	update_uber_playback(trevors_car.veh, 1.0)
	SET_UBER_PLAYBACK_TO_TIME_NOW(trevors_car.veh, 2000.00) //5500.00
	update_uber_playback(trevors_car.veh, 1.0)
	set_vehicle_engine_on(trevors_car.veh, true, true)
	force_playback_recorded_vehicle_update(trevors_car.veh)
				
	set_entity_visible(tow_truck.veh, true)
	freeze_entity_position(tow_truck.veh, false)
	set_entity_collision(tow_truck.veh, true)
	set_vehicle_doors_locked(tow_truck.veh, vehiclelock_lockout_player_only)
	set_vehicle_doors_shut(tow_truck.veh, true)
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	start_playback_recorded_vehicle(tow_truck.veh, 102, "lkheat", false)
	skip_time_in_playback_recorded_vehicle(tow_truck.veh, 800.00) //1000
	set_playback_speed(tow_truck.veh, 1.0)
	//SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(tow_truck.veh)
	set_vehicle_engine_on(trevors_car.veh, true, true)
	force_playback_recorded_vehicle_update(tow_truck.veh, false)
	set_vehicle_as_restricted(tow_truck.veh, 1)

//	vehicle_index players_last_veh 
//	players_last_veh = GET_MISSION_START_VEHICLE_INDEX()
//	if does_entity_exist(players_last_veh)
//		if is_vehicle_driveable(players_last_veh)
//			set_vehicle_engine_on(players_last_veh, false, true)
//			set_vehicle_as_no_longer_needed(players_last_veh)
//			//script_assert("no longer needed")
//		endif 
//	endif 
	
	if trigger_switch_effect_to_michael
		if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person 
			ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
			PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
		endif 
	endif 
	
	if is_screen_faded_in()
		end_cutscene_no_fade(false, false, false, 0, 0, 3000, false)
	else 
		end_cutscene(false, false, 0, 0, false)
		force_ped_ai_and_animation_update(player_ped_id(), true)
	endif 

	original_time = get_game_timer()
	
	trigger_music_event("FBI4_SWITCH_BINOC_ST")
	
	mission_flow = get_ambulance_into_pos
	
endproc 

func bool can_set_exit_state_for_player()
			
	switch get_current_player_ped_enum()
	
		case char_michael
		
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
				
				return true 
				
			endif 
		
		break 
		
		case char_franklin
		
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin))
				
				return true 
				
			endif 
		
		break 
		
		case char_trevor
		
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor))
				
				return true 
				
			endif 
		
		break 
		
	endswitch 
	
	return false 
	
endfunc 

func bool debug_intro_mocap_screen()

	if debug_intro_mocap_screen_status > 0
		if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			debug_intro_mocap_screen_status = 22
		endif 
	endif 
					
	switch debug_intro_mocap_screen_status
	
		case 0
		
			camera_a = create_cam_with_params("default_scripted_camera", <<1389.010986,-2063.858398,53.907593>>,<<-8.392499,0.237759,-153.687546>>,35.259651)
			camera_b = create_cam_with_params("default_scripted_camera", <<1388.967285,-2063.770020,53.210480>>,<<-8.392499,0.237759,-153.687546>>,33.259651)
			
			set_cam_active(camera_a, true)
			set_cam_active_with_interp(camera_b, camera_a, 7500, graph_type_linear)

			render_script_cams(true, false)
			
			print_help("cntry_mocp0")
			
			debug_intro_mocap_screen_status++
		
		break 
		
		case 1
		
			if not is_cam_interpolating(camera_b)
			
				while not has_fbi4_recordings_loaded() 
				or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
				
					wait(0)
		
				endwhile 
				
				mission_fail_checks()
				
				create_assets_for_get_ambulance_into_pos()
				
				return true
				
			endif 
		
		break 
		
		case 22
		
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(default_fade_time)
				endif 
			
			else 
			
				while not has_fbi4_recordings_loaded() 
				or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
				
					wait(0)
		
				endwhile 
				
				mission_fail_checks()
				
				create_assets_for_get_ambulance_into_pos()
			
				return true 
			endif 
		
		break 
		
	endswitch 
	
	return false

endfunc 

proc request_fbi_4_mcs_3_concat_assets()
				
	request_model(GET_PLAYER_PED_MODEL(char_michael))
	request_model(GET_PLAYER_PED_MODEL(char_franklin))
	request_model(GET_PLAYER_PED_MODEL(char_trevor))

	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)
	REQUEST_VEHICLE_ASSET(rubbish_truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS)) //not checking for loading

	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(trevors_car.model)
	set_vehicle_model_is_suppressed(trevors_car.model, true) 
	
	request_vehicle_recording(102, "lkheat")

endproc 

func bool has_fbi_4_mcs_3_concat_assets_loaded()

	if has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
	and has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	and has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	and has_model_loaded(rubbish_truck.model)
	and has_vehicle_asset_loaded(rubbish_truck.model)
	and has_model_loaded(tow_truck.model)
	and has_model_loaded(trevors_car.model)
	and has_vehicle_recording_been_loaded(102, "lkheat")
	
		return true
	
	endif 
	
	return false
	
endfunc 

proc fbi4_intro_mocap()

//	VEHICLE_INDEX     g_sTriggerSceneAssets.veh[0]  //rubbish truck
//	VEHICLE_INDEX     g_sTriggerSceneAssets.veh[1]  //Towing truck
//	VEHICLE_INDEX     g_sTriggerSceneAssets.veh[2]  //Trevor’s truck
//	PED_INDEX         g_sTriggerSceneAssets.ped[0]  //michael  
//	PED_INDEX         g_sTriggerSceneAssets.ped[1]  //franklin      
//	PED_INDEX         g_sTriggerSceneAssets.ped[2]  //trevor     

	switch fbi4_intro_mocap_status 
	
		case 0
		
			SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_NORMAL)
			
			switch get_current_player_ped_enum() 
			
				case char_michael
				
					request_cutscene("fbi_4_mcs_3_concat")

					if has_cutscene_loaded()
				
						if does_entity_exist(g_sTriggerSceneAssets.veh[0])
						and does_entity_exist(g_sTriggerSceneAssets.veh[1])

							rubbish_truck.veh = g_sTriggerSceneAssets.veh[0]  
							tow_truck.veh = g_sTriggerSceneAssets.veh[1]  

							set_entity_as_mission_entity(rubbish_truck.veh, true, true)
							set_entity_as_mission_entity(tow_truck.veh, true, true)
							
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_franklin))
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))
							
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, trevors_car.model)
							register_entity_for_cutscene(rubbish_truck.veh, "fbi_bin_lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, trash)
							register_entity_for_cutscene(tow_truck.veh, "fbi_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, towtruck)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
							
							start_cutscene()
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
							fbi4_intro_mocap_status++
						
						endif 
						
					endif 

				break 
				
				case char_franklin
				
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fbi_4_mcs_3_concat", cs_section_2 | cs_section_3 | cs_section_4)
				
					if has_cutscene_loaded()
					
						if does_entity_exist(g_sTriggerSceneAssets.ped[0])
						and does_entity_exist(g_sTriggerSceneAssets.veh[0])
						and does_entity_exist(g_sTriggerSceneAssets.veh[1])
						
							trigger_switch_effect_to_michael = true 
						
							selector_ped.pedID[selector_ped_michael] = g_sTriggerSceneAssets.ped[0]
							
							rubbish_truck.veh = g_sTriggerSceneAssets.veh[0]  
							tow_truck.veh = g_sTriggerSceneAssets.veh[1]  
						
							set_entity_as_mission_entity(selector_ped.pedID[selector_ped_michael], true, true)
							set_entity_as_mission_entity(rubbish_truck.veh, true, true)
							set_entity_as_mission_entity(tow_truck.veh, true, true)
							
							register_entity_for_cutscene(selector_ped.pedID[selector_ped_michael], "michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))

							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, trevors_car.model)
							register_entity_for_cutscene(rubbish_truck.veh, "fbi_bin_lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, trash)
							register_entity_for_cutscene(tow_truck.veh, "fbi_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, towtruck)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
							
							start_cutscene(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
							
							REPLAY_START_EVENT()
								
							fbi4_intro_mocap_status++
						
						endif 
						
					endif
				
				break 
				
				case char_trevor
				
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("fbi_4_mcs_3_concat", cs_section_3 | cs_section_4)
					
					if has_cutscene_loaded()
					
						if does_entity_exist(g_sTriggerSceneAssets.ped[0])
						and does_entity_exist(g_sTriggerSceneAssets.ped[1])
						and does_entity_exist(g_sTriggerSceneAssets.veh[0])
						and does_entity_exist(g_sTriggerSceneAssets.veh[1])
						and does_entity_exist(g_sTriggerSceneAssets.veh[2])
						
							trigger_switch_effect_to_michael = true
						
							selector_ped.pedID[selector_ped_michael] = g_sTriggerSceneAssets.ped[0]
							selector_ped.pedID[selector_ped_franklin] = g_sTriggerSceneAssets.ped[1]

							rubbish_truck.veh = g_sTriggerSceneAssets.veh[0]  
							tow_truck.veh = g_sTriggerSceneAssets.veh[1]  
							trevors_car.veh = g_sTriggerSceneAssets.veh[2]  
							
							//fix up vehicle incase player has wrecked it.
							if not is_entity_dead(trevors_car.veh)
								set_vehicle_fixed(trevors_car.veh)
								set_entity_health(trevors_car.veh, 1000)
								set_vehicle_petrol_tank_health(trevors_car.veh, 1000)
								set_vehicle_engine_health(trevors_car.veh, 1000)
							endif 
						
							set_entity_as_mission_entity(selector_ped.pedID[selector_ped_michael], true, true)
							set_entity_as_mission_entity(selector_ped.pedID[selector_ped_franklin], true, true)
							set_entity_as_mission_entity(rubbish_truck.veh, true, true)
							set_entity_as_mission_entity(tow_truck.veh, true, true)
							set_entity_as_mission_entity(trevors_car.veh, true, true)
							
							register_entity_for_cutscene(selector_ped.pedID[selector_ped_michael], "michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							register_entity_for_cutscene(selector_ped.pedID[selector_ped_franklin], "franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							
							REGISTER_ENTITY_FOR_CUTSCENE(trevors_car.veh, "trevors_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, trevors_car.model)
							register_entity_for_cutscene(rubbish_truck.veh, "fbi_bin_lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, trash)
							register_entity_for_cutscene(tow_truck.veh, "fbi_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, towtruck)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
							
							start_cutscene(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
							fbi4_intro_mocap_status++

	 					endif 
						
					endif 
				
				break

			endswitch 
			
			
			
		break 

		case 1
		
			if IS_CUTSCENE_PLAYING()

				//used for repeat play
				//if is_screen_faded_out()
				if not is_screen_faded_in()
					if not is_screen_fading_in()
						do_screen_fade_in(default_fade_time)
					endif 
				endif 
				
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4)
				
				vehicle_index players_last_car
				
				players_last_car = get_players_last_vehicle()

				//only resolve the vehicle if it is not trevors truck. Trevors truck will be used in the next stage of the mission
				if players_last_car != trevors_car.veh
					
//					if does_entity_exist(players_last_car)
//						if is_vehicle_driveable(players_last_car)
//							if not is_entity_at_coord(players_last_car, <<1401.5897, -2053.6978, 50.9983>> 
					
					delete_vehicle_gen_vehicles_in_area(<<1401.5897, -2053.6978, 50.9983>>, 4.0)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1351.419, -2065.903, 46.098>>, <<1395.749, -2028.968, 58.998>>, 80.000, <<1401.5897, -2053.6978, 50.9983>>, 80.9310, <<15.0, 15.0, 15.0>>)//, false) //2.5, 5.5, 2.0
					//reposition_players_last_vehicle(<<1401.5897, -2053.6978, 50.9983>>, 80.9310)
					set_players_last_vehicle_to_vehicle_gen(<<1401.5897, -2053.6978, 50.9983>>, 80.9310)
					
				endif 

				set_roads_in_area(<<1412.83, -1954.96, -100.00>>, <<1406.50, -1941.47, 100.00>>, false)

				clear_area(<<1379.3738, -2074.8240, 50.9985>>, 1000.00, true)

				clear_mission_locate_stuff(locates_data)
		
//				set_entity_visible(trevors_car.veh, false)
//				freeze_entity_position(trevors_car.veh, true)
//				set_entity_collision(trevors_car.veh, false)
				
				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(tow_truck.veh)

				load_fbi4_recordings()
				
				request_fbi_4_mcs_3_concat_assets()
				
				fbi4_intro_mocap_status++
				
			endif 
		
		break 
		
		case 2
		
			if is_cutscene_active()
				if not WAS_CUTSCENE_SKIPPED()
				
					load_fbi4_recordings()
				
					request_fbi_4_mcs_3_concat_assets()

					printint(get_cutscene_time())
					printnl()

//					printvector(get_entity_coords(tow_truck.veh))
//					printnl()
//					printfloat(get_entity_heading(tow_truck.veh))
//					printnl()

					switch get_current_player_ped_enum() 
			
						case char_michael

							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin)))
								selector_ped.pedID[selector_ped_franklin] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin)))
							ENDIF
						
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
								selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
							ENDIF
							
							//obtains trevors truck if the player starts mission as michael or franklin
							if not does_entity_exist(trevors_car.veh)
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
									trevors_car.veh = GET_vehicle_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
									if GET_CLOCK_HOURS() >= 20
									or GET_CLOCK_HOURS() <= 6
										set_vehicle_lights(trevors_car.veh, set_vehicle_lights_on)
									endif 
								ENDIF	
							endif 
							
							if not preload_outfits
								if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
								and does_entity_exist(selector_ped.pedID[selector_ped_trevor])

									PRELOAD_STORED_FBI4_OUTFIT(player_ped_id()) 
									PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_franklin])
									PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_trevor])
									
									preload_outfits = true
								
								endif 
							endif 
							
							if not boiler_suits_applied
								if get_cutscene_time() >= 84000
									SET_STORED_FBI4_OUTFIT(player_ped_id())
									SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_franklin])
									SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_trevor])
									boiler_suits_applied = true 
								endif 
							endif 
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin))
								set_ped_into_vehicle(selector_ped.pedID[SELECTOR_PED_franklin], tow_truck.veh)
							endif 
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
								//set_current_selector_ped(selector_ped_michael)
							endif 

						break 
						
						case char_franklin
						
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
								selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
							ENDIF
							
							//obtains trevors truck if the player starts mission as michael or franklin
							if not does_entity_exist(trevors_car.veh)
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
									trevors_car.veh = GET_vehicle_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
									if GET_CLOCK_HOURS() >= 20
									or GET_CLOCK_HOURS() <= 6
										set_vehicle_lights(trevors_car.veh, set_vehicle_lights_on)
									endif 
								ENDIF	
							endif 
							
							if not preload_outfits
								if does_entity_exist(selector_ped.pedID[selector_ped_trevor])

									PRELOAD_STORED_FBI4_OUTFIT(player_ped_id()) 
									PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_michael])
									PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_trevor])
									
									preload_outfits = true
								
								endif 
							endif 
							
							if not boiler_suits_applied
								if get_cutscene_time() >= 84000
									SET_STORED_FBI4_OUTFIT(player_ped_id())
									SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_michael])
									SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_trevor])
									boiler_suits_applied = true 
								endif 
							endif 
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin))
								set_ped_into_vehicle(player_ped_id(), tow_truck.veh)
							endif 
						
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
								
								if (get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person)
								and (get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) != cam_view_mode_first_person)
							
									make_selector_ped_selection(selector_ped, selector_ped_michael)
									take_control_of_selector_ped(selector_ped, true, true)
									
								endif 
							endif  
							
							//added for the blend out when in first person. 
							//bug 2028708
							if CAN_SET_EXIT_STATE_FOR_CAMERA()
								
								if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
								or get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
								
									make_selector_ped_selection(selector_ped, selector_ped_michael)
									take_control_of_selector_ped(selector_ped, true, true)
								
								endif 
								
							endif 
						
						break 
						
						case char_trevor
						
							if not preload_outfits
								PRELOAD_STORED_FBI4_OUTFIT(player_ped_id()) 
								PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_michael])
								PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_franklin])
								preload_outfits = true 
							endif 

							if not boiler_suits_applied
								if get_cutscene_time() >= 84000
									SET_STORED_FBI4_OUTFIT(player_ped_id())
									SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_michael])
									SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_franklin])
									boiler_suits_applied = true 
								endif 
							endif 
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin))
								set_ped_into_vehicle(selector_ped.pedID[SELECTOR_PED_franklin], tow_truck.veh)
							endif 
							
							if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
								
								if (get_cam_view_mode_for_context(cam_view_mode_context_on_foot) != cam_view_mode_first_person)
								and (get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) != cam_view_mode_first_person)
							
									make_selector_ped_selection(selector_ped, selector_ped_michael)
									take_control_of_selector_ped(selector_ped, true, true)
									
								endif 
								
							endif  
							
							if CAN_SET_EXIT_STATE_FOR_CAMERA()
								
								if get_cam_view_mode_for_context(cam_view_mode_context_on_foot) = cam_view_mode_first_person
								or get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
								
									make_selector_ped_selection(selector_ped, selector_ped_michael)
									take_control_of_selector_ped(selector_ped, true, true)
								
								endif 
								
							endif 

						break 
						
					endswitch 
					
//					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(tow_truck.veh)
//					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("fbi_truck", tow_truck.model)
//						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(tow_truck.veh)
//					endif 
				
				else 
					REPLAY_CANCEL_EVENT()
					SET_CUTSCENE_FADE_VALUES(false, false, true)
				
					fbi4_intro_mocap_status++
					
				endif 
				
			else 
				REPLAY_STOP_EVENT()
				create_assets_for_get_ambulance_into_pos()
			
			endif 

		break 
		
		case 3
		
			if is_cutscene_active()
			
				switch get_current_player_ped_enum() 
			
					case char_michael
		
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin)))
							selector_ped.pedID[selector_ped_franklin] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin)))
						ENDIF
					
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
							selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
						ENDIF
						
						//obtains trevors truck if the player starts mission as michael or franklin
						if not does_entity_exist(trevors_car.veh)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
								trevors_car.veh = GET_vehicle_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
							ENDIF	
						endif 
						
						if not preload_outfits
							if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
							and does_entity_exist(selector_ped.pedID[selector_ped_trevor])

								PRELOAD_STORED_FBI4_OUTFIT(player_ped_id()) 
								PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_franklin])
								PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_trevor])
								
								preload_outfits = true 
							endif 
						
						endif 
						
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
							//set_current_selector_ped(selector_ped_michael)
							REPLAY_STOP_EVENT()
						endif 
						
					break 
					
					case char_franklin
					
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
							selector_ped.pedID[selector_ped_trevor] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevor", GET_PLAYER_PED_MODEL(char_trevor)))
						ENDIF
						
						//obtains trevors truck if the player starts mission as michael or franklin
						if not does_entity_exist(trevors_car.veh)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
								trevors_car.veh = GET_vehicle_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("trevors_car", trevors_car.model))
							ENDIF	
						endif 

						if not preload_outfits
							if does_entity_exist(selector_ped.pedID[selector_ped_trevor])

								PRELOAD_STORED_FBI4_OUTFIT(player_ped_id()) 
								PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_michael])
								PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_trevor])
								
								preload_outfits = true
							
							endif
						endif 
						
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin))
							set_ped_into_vehicle(player_ped_id(), tow_truck.veh)
						endif 
					
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
							make_selector_ped_selection(selector_ped, selector_ped_michael)
							take_control_of_selector_ped(selector_ped, true, true)
							REPLAY_STOP_EVENT()
						endif 
						
					break 
					
					case char_trevor
					
						if not preload_outfits
							PRELOAD_STORED_FBI4_OUTFIT(player_ped_id()) 
							PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_michael])
							PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_franklin])
							preload_outfits = true 
						endif 
					
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("franklin", GET_PLAYER_PED_MODEL(char_franklin))
							set_ped_into_vehicle(selector_ped.pedID[SELECTOR_PED_franklin], tow_truck.veh)
						endif 
						
						if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael))
							make_selector_ped_selection(selector_ped, selector_ped_michael)
							take_control_of_selector_ped(selector_ped, true, true)
							REPLAY_STOP_EVENT()
						endif 

					break 
					
				endswitch 
				
//				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("fbi_truck", tow_truck.model)
//					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(tow_truck.veh)
//				endif 
	
			
			else 
			
				load_fbi4_recordings()
				
				request_fbi_4_mcs_3_concat_assets()
			
				if has_fbi_4_mcs_3_concat_assets_loaded()
				and has_fbi4_recordings_loaded() 
				and has_ped_preload_variation_data_finished(player_ped_id())
				and has_ped_preload_variation_data_finished(selector_ped.pedID[SELECTOR_PED_franklin])
				and has_ped_preload_variation_data_finished(selector_ped.pedID[SELECTOR_PED_trevor])
				
					REPLAY_STOP_EVENT()
				
					switch get_current_player_ped_enum()
					
						case char_michael

							if not boiler_suits_applied
								SET_STORED_FBI4_OUTFIT(player_ped_id())
								SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_franklin])
								SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_trevor])
								boiler_suits_applied = true
							endif 
						
						break 

					endswitch 
					
					while not HAVE_ALL_STREAMING_REQUESTS_COMPLETED(player_ped_id())
					
						wait(0)
				
					endwhile 
										
					mission_fail_checks()
					
					create_assets_for_get_ambulance_into_pos()

				endif 

			endif 
		
		break 
		
	endswitch 
	
//	initialise_mission_variables()

//	load_mission_assets()
	
//	#IF IS_DEBUG_BUILD
//		bShowSetPieceIndices = true
//		bShowTrafficIndices = true 
//	#endif 
		
endproc 
 
proc army_truck_driver_ai()

	switch army_truck_driver_ai_status 
	
		case 0
			
			if lk_timer(original_time, 13000) 
			
				task_clear_look_at(army_truck_driver.ped)
				
				open_sequence_task(seq)
					task_enter_vehicle(null, army_truck.veh, -1, vs_driver, pedmove_walk)
				close_sequence_task(seq)
				task_perform_sequence(army_truck_driver.ped, seq)
				clear_sequence_task(seq)
				
				army_truck_driver_ai_status++
			endif 
		
		break 
		
		case 1
		
		break 
		
	endswitch 

endproc 
			
proc army_truck_passenger_ai()

	switch army_truck_passenger_ai_status 
	
		case 0
			
			open_sequence_task(seq)
				task_pause(null, 1000) 
				task_enter_vehicle(null, army_truck.veh, -1, vs_front_right, pedmove_walk)
			close_sequence_task(seq)
			task_perform_sequence(army_truck_passenger.ped, seq)
			clear_sequence_task(seq)
				
			army_truck_passenger_ai_status++
				
		break 
		
		case 1
		
		break 
		
	endswitch 
endproc 

proc factory_worker_0_ai_system()
			
	switch factory_worker_0_system_status
	
		case 0

			if lk_timer(original_time, 14000)
				
				open_sequence_task(seq)
					task_follow_nav_mesh_to_coord(null, <<508.3191, -3047.6064, 5.0687>>, pedmove_walk, 9999999)
				close_sequence_task(seq)
				task_perform_sequence(factory_worker, seq)
				clear_sequence_task(seq)
				
				factory_worker_0_system_status++
			endif 
		
		break 
		
		case 1
		
		break 
		
	endswitch 

endproc 

proc reset_surveillance_cutscene()
	
	#IF IS_DEBUG_BUILD
	if widget_reset_cutscene
	
		end_cutscene_no_fade()
		
		if is_playback_going_on_for_vehicle(army_truck.veh)
			stop_playback_recorded_vehicle(army_truck.veh)
		endif 

		delete_ped(army_truck_driver.ped)
		delete_ped(army_truck_passenger.ped)
		delete_vehicle(army_truck.veh)
		
		sf_binoculars = REQUEST_SCALEFORM_MOVIE("binoculars")
		
		while not has_scaleform_movie_loaded(sf_binoculars)
			wait(0)
		endwhile 
		
		surveillance_status = 0
		
		start_new_cutscene_no_fade()
		
		widget_reset_cutscene = false
		
	endif
	#endif 

endproc 

func bool surveillance_cutscene()

	int i = 0

	reset_surveillance_cutscene()

	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		surveillance_status  = 22
	endif 
	
	float playback_time 
	
	if is_vehicle_driveable(army_truck.veh)
		if is_playback_going_on_for_vehicle(army_truck.veh)
			playback_time = GET_TIME_POSITION_IN_RECORDING(army_truck.veh)
//			printfloat(playback_time)
//			printnl()
		endif 
	endif 
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)

	switch surveillance_status 
	
		case 0
		
			if has_model_loaded(army_truck.model) 
			and has_model_loaded(army_truck_driver.model)
			and has_model_loaded(traffic_vehicle[0].model)

				//streaming_volume = STREAMVOL_CREATE_SPHERE(<<793.9, -2329.9, 61.8>>, 5.00, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
			
				cleanup_uber_playback(true)//delete all setpiece cars
				
				//roads near shootout allowing traffic to pass at end of road
				set_roads_in_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false)
				set_ped_paths_in_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false) 
				add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)
		
				//SET_VEHICLE_POPULATION_BUDGET(0)
				
				//switches off generators near shootout
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, false) //cars near ally way
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<906.8, -2322.0, 100.00>>, <<844.0, -2365.1, -100.00>>, false) //vehicle near cover blocks

				//roads for stockade in cutscene
				set_roads_in_area(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>, false)
				//add_scenario_blocking_area(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>)

				clear_area(<<790.83, -2330.06, 62.67>>, 1000.00, true)

				army_truck.veh = create_vehicle(army_truck.model, army_truck.pos, army_truck.heading)
				set_vehicle_has_strong_axles(army_truck.veh, true)
				set_entity_proofs(army_truck.veh, true, true, true, true, true)
				set_vehicle_automatically_attaches(army_truck.veh, false)
				start_playback_recorded_vehicle(army_truck.veh, 001, "lkcountry") 
				skip_time_in_playback_recorded_vehicle(army_truck.veh, 6000) //12000
				force_playback_recorded_vehicle_update(army_truck.veh)
				
			
				army_truck_driver.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_driver.model, vs_front_right)
				set_ped_dies_when_injured(army_truck_driver.ped, true)
				DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_driver.ped)
				set_blocking_of_non_temporary_events(army_truck_driver.ped, true)
				
				army_truck_passenger.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_passenger.model) 
				set_ped_dies_when_injured(army_truck_passenger.ped, true)
				DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_passenger.ped)
				set_blocking_of_non_temporary_events(army_truck_passenger.ped, true)
	
				traffic_vehicle[0].veh = create_vehicle(traffic_vehicle[0].model, traffic_vehicle[0].pos)
				start_playback_recorded_vehicle(traffic_vehicle[0].veh, traffic_vehicle[0].recording_number, "lkheat")
				skip_time_in_playback_recorded_vehicle(traffic_vehicle[0].veh, 7000)
				force_playback_recorded_vehicle_update(traffic_vehicle[0].veh)

				traffic_vehicle[1].veh = create_vehicle(traffic_vehicle[1].model, traffic_vehicle[1].pos)
				start_playback_recorded_vehicle(traffic_vehicle[1].veh, traffic_vehicle[1].recording_number, "lkheat")
				skip_time_in_playback_recorded_vehicle(traffic_vehicle[1].veh, 7000)
				force_playback_recorded_vehicle_update(traffic_vehicle[1].veh)
				
				traffic_vehicle[2].veh = create_vehicle(traffic_vehicle[2].model, traffic_vehicle[2].pos)
				start_playback_recorded_vehicle(traffic_vehicle[2].veh, traffic_vehicle[2].recording_number, "lkheat")
				force_playback_recorded_vehicle_update(traffic_vehicle[2].veh)
				
				traffic_vehicle[3].veh = create_vehicle(traffic_vehicle[3].model, traffic_vehicle[3].pos)
				start_playback_recorded_vehicle(traffic_vehicle[3].veh, traffic_vehicle[3].recording_number, "lkheat")
				skip_time_in_playback_recorded_vehicle(traffic_vehicle[3].veh, 7000)
				force_playback_recorded_vehicle_update(traffic_vehicle[3].veh)
				
				traffic_vehicle[7].veh = create_vehicle(traffic_vehicle[7].model, traffic_vehicle[7].pos)
				start_playback_recorded_vehicle(traffic_vehicle[7].veh, traffic_vehicle[7].recording_number, "lkheat")
				skip_time_in_playback_recorded_vehicle(traffic_vehicle[7].veh, 4200)
//				force_playback_recorded_vehicle_update(traffic_vehicle[7].veh)
//				pause_playback_recorded_vehicle(traffic_vehicle[7].veh)
				

				camera_a = create_cam_with_params("default_scripted_camera", <<768.628906,-2357.275146,57.672928>>,<<-5.133545,0.001094,153.944199>>, 28.00)
				camera_b = create_cam_with_params("default_scripted_camera", <<768.713318,-2356.750732,57.853672>>,<<-18.866131,-0.410374,158.942352>>,28.00)
	
				camera_c = create_cam_with_params("default_scripted_camera", <<768.713318,-2356.750732,57.853672>>,<<-18.866131,-0.410374,158.942352>>,28.000000)
				camera_d = create_cam_with_params("default_scripted_camera",<<765.145447,-2378.903809,50.217480>>,<<-13.958374,0.000000,172.838638>>,22.251091)
				
				camera_g = create_cam_with_params("default_scripted_camera", <<765.141724,-2378.912354,50.215240>>,<<-25.080309,-0.257130,167.934708>>,20.208502)
				
				camera_e = create_cam_with_params("default_scripted_camera", <<757.393555,-2407.696289,20.083193>>,<<3.599488,0.001119,156.817551>>,36.713017)
				camera_f = create_cam_with_params("default_scripted_camera", <<757.899353,-2406.809326,20.134092>>,<<4.586555,-0.000000,158.100967>>,36.713017)
				
				DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0) // adjust to taste
				scale_form_movie_active = true 
				
				set_cam_active(camera_a, true)
				set_cam_active_with_interp(camera_b, camera_a, 5500, graph_type_linear)
				
				CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(true)
				
				shake_cam(camera_d, "HAND_SHAKE", 1.0)

				render_script_cams(true, false)
				
				original_time = get_game_timer()
				
				start_audio_scene("FBI_4_CUTSCENE_BINOCULARS")
				
				
				//load future assets
				
				request_model(GET_PLAYER_PED_MODEL(char_michael))
				request_model(GET_PLAYER_PED_MODEL(char_franklin))
				
				request_model(rubbish_truck.model)
				set_vehicle_model_is_suppressed(rubbish_truck.model, true)
				request_model(tow_truck.model)
				set_vehicle_model_is_suppressed(tow_truck.model, true)
				
				request_model(c4.model)
				
				request_vehicle_recording(002, "lkcountry") 
				request_vehicle_recording(003, "lkcountry") 
				request_vehicle_recording(004, "lkcountry") 
				request_vehicle_recording(005, "lkcountry") 
				request_vehicle_recording(010, "lkcountry")
				request_vehicle_recording(011, "lkcountry")
				request_vehicle_recording(012, "lkcountry")
				request_vehicle_recording(014, "lkcountry")
				request_vehicle_recording(015, "lkcountry")
				request_vehicle_recording(016, "lkcountry")
				
				request_anim_dict("missfbi4")
				request_anim_dict("misssagrab")
				
				REQUEST_WAYPOINT_RECORDING("heat1")
				REQUEST_WAYPOINT_RECORDING("heat2")
				REQUEST_WAYPOINT_RECORDING("heat3")
				
				//request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_01")
				request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
				REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
				
//				LOAD_STREAM("FBI_04_TRUCK_CRASH_MASTER") 
				
				//LOAD_STREAM("Truck_Crash_Stream","FBI_04_HEAT_SOUNDS")
				
				create_conversation(scripted_speech[0], "HeatAud", "heat_looks", conv_priority_medium)

				surveillance_status++
				
			endif 

		break 
		
		case 1
		
			if not is_cam_interpolating(camera_b)
			
				set_cam_active(camera_a, false)
				set_cam_active(camera_b, false)
				
				traffic_vehicle[4].veh = create_vehicle(traffic_vehicle[4].model, traffic_vehicle[4].pos)
				start_playback_recorded_vehicle(traffic_vehicle[4].veh, traffic_vehicle[0].recording_number, "lkheat")
				skip_time_in_playback_recorded_vehicle(traffic_vehicle[4].veh, 3000) //4000
				force_playback_recorded_vehicle_update(traffic_vehicle[4].veh)

				traffic_vehicle[5].veh = create_vehicle(traffic_vehicle[5].model, traffic_vehicle[5].pos)
				start_playback_recorded_vehicle(traffic_vehicle[5].veh, traffic_vehicle[5].recording_number, "lkheat")
				skip_time_in_playback_recorded_vehicle(traffic_vehicle[5].veh, 5250)
				force_playback_recorded_vehicle_update(traffic_vehicle[5].veh)
				
				traffic_vehicle[6].veh = create_vehicle(traffic_vehicle[6].model, traffic_vehicle[6].pos)
				start_playback_recorded_vehicle(traffic_vehicle[6].veh, traffic_vehicle[6].recording_number, "lkheat")
				force_playback_recorded_vehicle_update(traffic_vehicle[6].veh)
				
//				traffic_vehicle[7].veh = create_vehicle(traffic_vehicle[7].model, traffic_vehicle[7].pos)
//				start_playback_recorded_vehicle(traffic_vehicle[7].veh, traffic_vehicle[7].recording_number, "lkheat")
//				//skip_time_in_playback_recorded_vehicle(traffic_vehicle[7].veh, 3700)
//				force_playback_recorded_vehicle_update(traffic_vehicle[7].veh)
				
//				if is_vehicle_driveable(traffic_vehicle[7].veh)
//					if is_playback_going_on_for_vehicle(traffic_vehicle[7].veh)
//						unpause_playback_recorded_vehicle(traffic_vehicle[7].veh)
//					
//					endif 
//				endif 
		
				set_cam_active(camera_c, true)
				set_cam_active_with_interp(camera_d, camera_c, 5500, graph_type_linear)
				
				original_time = get_game_timer()

				surveillance_status++
				
			endif 
			
			DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
		
		break 
		
		case 2

			if lk_timer(original_time, 3000)
		
				create_conversation(scripted_speech[0], "HeatAud", "van_cut_0", conv_priority_medium) 
				
				surveillance_status++
				
			endif 
			
			DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
		
		break 
		
		case 3
		
			//clear_area(<<790.83, -2330.06, 62.67>>, 10000.00, true)
		
			if not is_cam_interpolating(camera_d)

				set_cam_active(camera_c, false)

				set_cam_active(camera_d, true)
				set_cam_active_with_interp(camera_g, camera_d, 3000, graph_type_linear)

				surveillance_status++

			endif 
			
			DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
			
		break 
		
		case 4

			if playback_time > 20277
			
				set_cam_active(camera_d, false)
				set_cam_active(camera_g, false)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "heataud", "van_cut_0b", CONV_PRIORITY_MEDIUM)

				//temp fix before implementing uber system or just playing back lots of recordings for traffic
//				clear_area_of_vehicles(<<755.950195,-2410.051758,20.329510>>, 10.00)

				set_playback_speed(army_truck.veh, 0.8)
				
				set_cam_active(camera_e, true)
				//set_cam_active_with_interp(camera_f, camera_e, 1700, graph_type_linear)
				
				start_audio_scene("FBI_4_CUTSCENE_TRUCK")
				
				shake_cam(camera_e, "ROAD_VIBRATION_SHAKE", 1.0)
				
				scale_form_movie_active = false
				
				original_time = get_game_timer()
	
				surveillance_status++
				
			else 
			
				DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
			
			endif 
			
		break 
		
		case 5
		
//			printfloat(playback_time)
//			printnl()
		
			//if not is_cam_interpolating(camera_f)
			if lk_timer(original_time, 1800) 
			
				for i = 0 to count_of(traffic_vehicle) - 1
					set_model_as_no_longer_needed(traffic_vehicle[i].model)
					set_vehicle_as_no_longer_needed(traffic_vehicle[i].veh)
				endfor 

				//switch on roads for stockade cutscene and switch off roads near shootout
				set_roads_in_area(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>, true)
				set_roads_in_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false)
			
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sf_binoculars)

				if is_playback_going_on_for_vehicle(army_truck.veh)
					stop_playback_recorded_vehicle(army_truck.veh)
				endif 

				delete_ped(army_truck_driver.ped)
				delete_ped(army_truck_passenger.ped)
				set_model_as_no_longer_needed(army_truck_driver.model)
				
				//delete_vehicle(army_truck.veh)
				//set_model_as_no_longer_needed(army_truck.model)
				
				set_current_ped_weapon(player_ped_id(), weapontype_unarmed, true)
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_CUTSCENE_TRUCK")
					STOP_AUDIO_SCENE("FBI_4_CUTSCENE_TRUCK")
				endif 
				
				CASCADE_SHADOWS_INIT_SESSION() 
				
				end_cutscene_no_fade(false, true, false, 0, 0, 3000, false)
				
				return true

			endif 
				
		break 
		
		case 22
		
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(1000)
				endif 
				
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)

				if scale_form_movie_active
					DRAW_SCALEFORM_MOVIE(sf_binoculars, 0.5, 0.5, 1.0, 1.0, 255, 255, 255, 0)
				endif 

			else
			
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_CUTSCENE_TRUCK")
					STOP_AUDIO_SCENE("FBI_4_CUTSCENE_TRUCK")
				endif 
			
				for i = 0 to count_of(traffic_vehicle) - 1
					set_model_as_no_longer_needed(traffic_vehicle[i].model)
					set_vehicle_as_no_longer_needed(traffic_vehicle[i].veh)
				endfor 
			
				request_model(GET_PLAYER_PED_MODEL(char_michael))
				request_model(GET_PLAYER_PED_MODEL(char_franklin))
				
				request_model(rubbish_truck.model)
				set_vehicle_model_is_suppressed(rubbish_truck.model, true)
				request_model(tow_truck.model)
				set_vehicle_model_is_suppressed(tow_truck.model, true)
				
				request_model(c4.model)
				
				request_vehicle_recording(002, "lkcountry") 
				request_vehicle_recording(003, "lkcountry") 
				request_vehicle_recording(004, "lkcountry") 
				request_vehicle_recording(005, "lkcountry") 
				request_vehicle_recording(010, "lkcountry")
				request_vehicle_recording(011, "lkcountry")
				request_vehicle_recording(012, "lkcountry")
				request_vehicle_recording(014, "lkcountry")
				request_vehicle_recording(015, "lkcountry")
				request_vehicle_recording(016, "lkcountry")
				
				request_anim_dict("missfbi4")
				request_anim_dict("misssagrab")
				
				REQUEST_WAYPOINT_RECORDING("heat1")
				REQUEST_WAYPOINT_RECORDING("heat2")
				REQUEST_WAYPOINT_RECORDING("heat3")
				
				//request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_01")
				request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
				REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
				
//				LOAD_STREAM("FBI_04_TRUCK_CRASH_MASTER") 
				//LOAD_STREAM("Truck_Crash_Stream","FBI_04_HEAT_SOUNDS") 


				while not has_model_loaded(rubbish_truck.model)
				or not has_model_loaded(tow_truck.model)
				or not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
				or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
				or not has_model_loaded(c4.model)
				or not has_vehicle_recording_been_loaded(002, "lkcountry")
				or not has_vehicle_recording_been_loaded(003, "lkcountry")
				or not has_vehicle_recording_been_loaded(004, "lkcountry")
				or not has_vehicle_recording_been_loaded(005, "lkcountry")
				or not has_vehicle_recording_been_loaded(010, "lkcountry")
				or not has_vehicle_recording_been_loaded(011, "lkcountry")
				or not has_vehicle_recording_been_loaded(012, "lkcountry")
				or not has_vehicle_recording_been_loaded(014, "lkcountry")
				or not has_vehicle_recording_been_loaded(015, "lkcountry")
				or not has_vehicle_recording_been_loaded(016, "lkcountry")
				or not has_anim_dict_loaded("missfbi4")
				or not has_anim_dict_loaded("misssagrab")
				or not get_is_waypoint_recording_loaded("heat1")
				or not get_is_waypoint_recording_loaded("heat2")
				or not get_is_waypoint_recording_loaded("heat3")
				//or not request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_01")
				or not request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
				or not REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")
				//or not LOAD_STREAM("Truck_Crash_Stream","FBI_04_HEAT_SOUNDS")
//				or not LOAD_STREAM("FBI_04_TRUCK_CRASH_MASTER") 
				
					wait(0)
					
				endwhile

				//set_roads_in_area(<<774.76, -2946.16, 100.00>>, <<362.54, -1949.75, -100.00>>, true)
				
				//switch on roads for stockade cutscene and switch off roads near shootout
				//set_roads_in_area(<<1341.17, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>, true)
				set_roads_in_area(<<1034.00, -2873.8, 100.00>>, <<164.1, -1944.8, -100.00>>, true)
				set_roads_in_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false)
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sf_binoculars)
				
				if is_playback_going_on_for_vehicle(army_truck.veh)
					stop_playback_recorded_vehicle(army_truck.veh)
				endif 

				delete_ped(army_truck_driver.ped)
				delete_ped(army_truck_passenger.ped)
				set_model_as_no_longer_needed(army_truck_driver.model)
				
				delete_vehicle(army_truck.veh)
				set_model_as_no_longer_needed(army_truck.model)
				
				CASCADE_SHADOWS_INIT_SESSION() 
			
				set_current_ped_weapon(player_ped_id(), weapontype_unarmed, true)
				
				end_cutscene(false, true, 0, 0, false)
				
				return true 
				
			endif 

		break 
		
	endswitch 	
	
	return false 
	
endfunc 

func bool setup_hotwap_cam_to_rubbish_truck()

//	IF NOT sCamDetails.bSplineCreated
//		
//		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
//			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
//		ENDIF                                                 
//		
//		// Initial camera behind the player       
//		ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0)    
//		ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<915.700928,-2107.101563,45.491390>>, <<-4.013334,-0.112904,150.825455>>, 5300)//4300
//		ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 1000)
//			
//		set_cam_fov(sCamDetails.camID, get_final_rendered_cam_fov())
//
//		sCamDetails.bSplineCreated = TRUE
//		sCamDetails.camType = selector_cam_long_spline//SELECTOR_CAM_STRAIGHT_INTERP
		
		sCamDetails.pedTo = selector_ped.pedID[SELECTOR_PED_MICHAEL] //ped that the spline camera is going to

		return true 
		
//	ENDIF 
//	
//	return false
	 
ENDfunc

proc setup_hotswap_michael()

	clear_help()
	clear_prints()
			
	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_michael, false)
	SET_SELECTOR_PED_BLOCKED(selector_ped, selector_ped_franklin, false)

	setup_hotwap_cam_to_rubbish_truck()
	
	SET_ENTITY_LOAD_COLLISION_FLAG(rubbish_truck.veh, true)
	
	fbi4_vehicle_spotted_status++
	
endproc 

proc fbi4_vehicle_spotted()

	switch fbi4_vehicle_spotted_status 
	
		case 0

			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails)	// Returns FALSE when the camera spline is complete
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails, switch_type_medium)
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sCamDetails, camera_e, switch_type_medium)	
				IF sCamDetails.bOKToSwitchPed
				
					render_script_cams(true, false)
					set_cam_active(camera_e, true)
				
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, false)
						
							sCamDetails.bPedSwitched = TRUE
							
							SET_ENTITY_LOAD_COLLISION_FLAG(player_ped_id(), false)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(player_ped_id(), true) 
							FREEZE_ENTITY_POSITION(player_ped_id(), false)
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)

//							SET_PED_AS_NO_LONGER_NEEDED(selector_ped.pedID[selector_ped_franklin])
//							set_model_as_no_longer_needed(get_player_ped_model(char_franklin))
							
							if does_entity_exist(selector_ped.pedID[selector_ped_michael])
								delete_ped(selector_ped.pedID[selector_ped_michael])
							endif
							set_model_as_no_longer_needed(get_player_ped_model(char_michael))
							
							if does_entity_exist(rubbish_truck.veh)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(rubbish_truck.veh)
							endif 
							set_model_as_no_longer_needed(rubbish_truck.model)

//							SET_VEHICLE_AS_NO_LONGER_NEEDED(tow_truck.veh)
//							set_model_as_no_longer_needed(tow_truck.model)
							
							//SET_VEHICLE_AS_NO_LONGER_NEEDED(trevors_car.veh)
							if does_entity_exist(trevors_car.veh)
								delete_vehicle(trevors_car.veh)
							endif 
							set_model_as_no_longer_needed(trevors_car.model)
							
//							request_model(army_truck_driver.model)
//							set_ped_model_is_suppressed(army_truck_driver.model, true)
//							
//							request_model(army_truck.model)
//							set_vehicle_model_is_suppressed(army_truck.model, true)

							binoculars.obj = create_object(binoculars.model, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 40.00>>))
							attach_entity_to_entity(binoculars.obj, player_ped_id(), get_ped_bone_index(player_ped_id(), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							set_entity_heading(player_ped_id(), 137.22)
							task_play_anim(player_ped_id(), "missheat", "binoculars_loop", instant_blend_in, normal_blend_out, -1, af_looping)
						
						ENDIF
					ENDIF
					
				else 
				
					render_script_cams(false, false)
					
				ENDIF
			
			else

				STREAMVOL_DELETE(streaming_volume)
				
				streaming_volume = STREAMVOL_CREATE_SPHERE(<<740.8940, -2475.9226, 19.3226>>, 50.00, FLAG_COLLISIONS_MOVER)//15
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
				
				start_new_cutscene_no_fade(false)

				remove_ped_for_dialogue(scripted_speech[0], 1)

				add_ped_for_dialogue(scripted_speech[0], 0, null, "michael")

				set_cam_active_with_interp(camera_f, camera_e, 2500, graph_type_linear)
			
				render_script_cams(true, false)
				
				fbi4_vehicle_spotted_status++
					 
			endif 
			
		break
		
		case 1
	
			if not is_cam_interpolating(camera_f)
				
				surveillance_cutscene()
				
				original_time  = get_game_timer()
				
				fbi4_vehicle_spotted_status++
				
			endif 
			
		break 

		case 2
			
			if surveillance_cutscene()
			
				STREAMVOL_DELETE(streaming_volume)
		
				original_time = get_game_timer()
				
				fbi4_vehicle_spotted_status++
			endif 

		break 
		
		case 3
		
			if is_screen_faded_in()
				if create_conversation(scripted_speech[0], "heatAUD", "van_cut_1", conv_priority_medium)
					fbi4_vehicle_spotted_status++
				endif 
			endif 

		break 
		
		case 4
		
			//if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

			if has_model_loaded(rubbish_truck.model)
			and has_model_loaded(tow_truck.model)
			and has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
			and has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
			and has_model_loaded(c4.model)
			and has_vehicle_recording_been_loaded(002, "lkcountry")
			and has_vehicle_recording_been_loaded(003, "lkcountry")
			and has_vehicle_recording_been_loaded(004, "lkcountry")
			and has_vehicle_recording_been_loaded(005, "lkcountry")
			and has_vehicle_recording_been_loaded(010, "lkcountry")
			and has_vehicle_recording_been_loaded(011, "lkcountry")
			and has_vehicle_recording_been_loaded(012, "lkcountry")
			and has_vehicle_recording_been_loaded(014, "lkcountry")
			and has_vehicle_recording_been_loaded(015, "lkcountry")
			and has_vehicle_recording_been_loaded(016, "lkcountry")
			and has_anim_dict_loaded("missfbi4")
			and has_anim_dict_loaded("misssagrab")
			and get_is_waypoint_recording_loaded("heat1")
			and get_is_waypoint_recording_loaded("heat2")
			and get_is_waypoint_recording_loaded("heat3")
			and request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
			and REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")

				//area near rubbish truck
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<945.8, -2080.8, 100.00>>, <<1015.3, -2035.2, -100.0>>, false)
			
				clear_area(rubbish_truck.pos, 50.00, true)
				
				//rubbish truck 
				streaming_volume = STREAMVOL_CREATE_SPHERE(<<937.2, -2080.8, 30.0>>, 20.00, FLAG_COLLISIONS_MOVER)//15
		
				rubbish_truck.veh = create_vehicle(rubbish_truck.model, rubbish_truck.pos, rubbish_truck.heading)
				set_vehicle_has_strong_axles(rubbish_truck.veh, true)
				set_vehicle_automatically_attaches(rubbish_truck.veh, false)
				set_vehicle_on_ground_properly(rubbish_truck.veh)
				start_playback_recorded_vehicle(rubbish_truck.veh, 011, "lkcountry")
				skip_time_in_playback_recorded_vehicle(rubbish_truck.veh, 5200)
				force_playback_recorded_vehicle_update(rubbish_truck.veh)
				set_playback_speed(rubbish_truck.veh, 0.0)
				set_vehicle_as_restricted(rubbish_truck.veh, 0)
				//set_vehicle_livery(rubbish_truck.veh, 0)
				//SET_SIREN_CAN_BE_CONTROLLED_BY_AUDIO(rubbish_truck.veh, false) 
				//SET_VEHICLE_SIREN(rubbish_truck.veh, true)
				//set_siren_with_no_driver
				
				create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_MICHAEL], char_michael, rubbish_truck.veh, vs_driver, false)
				SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_michael])
				SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
				setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael])
				setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_michael], true)
				add_ped_for_dialogue(scripted_speech[0], 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
				
				tow_truck.veh = create_vehicle(tow_truck.model, <<1024.1974, -2376.5244, 29.5306>>, 85.7590)
				set_vehicle_colours(tow_truck.veh, 0, 0)
				set_vehicle_has_strong_axles(tow_truck.veh, true)
				set_vehicle_engine_on(tow_truck.veh, true, true)
				set_vehicle_doors_locked(tow_truck.veh, vehiclelock_lockout_player_only)
				SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
				set_vehicle_as_restricted(tow_truck.veh, 1)
				
				create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], char_franklin, tow_truck.veh, vs_driver, false)
				SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
				add_ped_for_dialogue(scripted_speech[0], 1, selector_ped.pedID[SELECTOR_PED_FRANKLIN], "franklin")
				set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_FRANKLIN], true)
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_CUTSCENE_BINOCULARS")
					STOP_AUDIO_SCENE("FBI_4_CUTSCENE_BINOCULARS")
				endif 

				fbi4_vehicle_spotted_status++
						
			endif 
		
		break 
		
		case 5

			make_selector_ped_selection(selector_ped, SELECTOR_PED_MICHAEL)
			setup_hotswap_michael()

		break 

		case 6
		
			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 1000, 1000, selector_cam_default, 0)	// Returns FALSE when the camera spline is complete
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails, switch_type_medium)
							
				//SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
				
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, true, true)
							
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
							
							sCamDetails.bPedSwitched = TRUE
						ENDIF
						
					ENDIF
				ENDIF
				

				if not setup_rubbish_truck_for_switch
					if IS_PLAYER_SWITCH_IN_PROGRESS()

						SWITCH_STATE camera_switch_state
						camera_switch_state = GET_PLAYER_SWITCH_STATE() 

						//if camera_switch_state > SWITCH_STATE_JUMPCUT_DESCENT //similar to 0 i.e last cam in the spline 
						if camera_switch_state = SWITCH_STATE_OUTRO_HOLD
						//or ((camera_switch_state >= SWITCH_STATE_JUMPCUT_DESCENT) and (GET_PLAYER_SWITCH_JUMP_CUT_INDEX() < 1)) //< = 1

							if is_playback_going_on_for_vehicle(rubbish_truck.veh)
												
								set_playback_speed(rubbish_truck.veh, 1.0)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(rubbish_truck.veh, (5200 - GET_TIME_POSITION_IN_RECORDING(rubbish_truck.veh)))
								force_playback_recorded_vehicle_update(rubbish_truck.veh)
								setup_rubbish_truck_for_switch = true
										
							else 
							
								start_playback_recorded_vehicle(rubbish_truck.veh, 011, "lkcountry")
								skip_time_in_playback_recorded_vehicle(rubbish_truck.veh, 5200)
								force_playback_recorded_vehicle_update(rubbish_truck.veh)
								setup_rubbish_truck_for_switch = true

							endif 
						endif 
						
					endif
					
				endif 
				

				//stops cars being created and crashing into ambulance on recording. 
				if lk_timer(original_time, 2500)
					clear_area(rubbish_truck.pos, 100.00, true)
				endif
				
//				if get_cam_spline_phase(sCamDetails.camID) > 0.8
//					if not is_playback_going_on_for_vehicle(rubbish_truck.veh)
//						
//						start_playback_recorded_vehicle(rubbish_truck.veh, 011, "lkcountry")
//						//skip_time_in_playback_recorded_vehicle(rubbish_truck.veh, 1500)
//						force_playback_recorded_vehicle_update(rubbish_truck.veh)
//						
//					endif 
//				
//				endif 

			else 
			
//				if is_playback_going_on_for_vehicle(rubbish_truck.veh)
//									
//					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(rubbish_truck.veh, (5200 - GET_TIME_POSITION_IN_RECORDING(rubbish_truck.veh)))
//					force_playback_recorded_vehicle_update(rubbish_truck.veh)
//							
//				else 
//				
//					start_playback_recorded_vehicle(rubbish_truck.veh, 011, "lkcountry")
//					skip_time_in_playback_recorded_vehicle(rubbish_truck.veh, 5200)
//					force_playback_recorded_vehicle_update(rubbish_truck.veh)
//
//				endif 

				STREAMVOL_DELETE(streaming_volume)
			
				SET_ENTITY_LOAD_COLLISION_FLAG(rubbish_truck.veh, false)
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
			
				if does_entity_exist(binoculars.obj)
					detach_entity(binoculars.obj)
					DELETE_OBJECT(binoculars.obj)
				endif 
				set_model_as_no_longer_needed(binoculars.model)
			
				if not is_ped_injured(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
					FREEZE_ENTITY_POSITION(selector_ped.pedID[selector_ped.ePreviousSelectorPed], true)
				endif 
				
				if not is_ped_injured(army_truck_driver.ped)
					DELETE_PED(army_truck_driver.ped)
				endif 
				
				if not is_ped_injured(army_truck_passenger.ped)
					DELETE_PED(army_truck_passenger.ped)
				endif
				
				if not is_ped_injured(factory_worker)
					remove_ped_for_dialogue(scripted_speech[0], 1)
					DELETE_PED(factory_worker)
				endif
				
				if is_vehicle_driveable(army_truck.veh)
					
					if is_playback_going_on_for_vehicle(army_truck.veh)
						stop_playback_recorded_vehicle(army_truck.veh)
					endif 
					
					DELETE_VEHICLE(army_truck.veh)
				endif 
				
				if is_playback_going_on_for_vehicle(rubbish_truck.veh)
					vector velocity
					velocity = get_entity_velocity(rubbish_truck.veh)
					velocity *= 1.2
					stop_playback_recorded_vehicle(rubbish_truck.veh)
					set_entity_velocity(rubbish_truck.veh, velocity) 
					//set_vehicle_forward_speed(rubbish_truck.veh, 12.0)
				endif 
				
				remove_ped_for_dialogue(scripted_speech[0], 0)
				add_ped_for_dialogue(scripted_speech[0], 0, player_ped_id(), "michael")
				create_conversation(scripted_speech[0], "heataud", "heat_move", CONV_PRIORITY_VERY_HIGH)
			
				start_audio_scene("FBI_4_BLOCK_THE_STREET")
				
				mission_flow = park_ambulance
			
			endif 
		
		break 	

		
	endswitch 

endproc 

func bool ambulance_cutscene()

	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		ambulance_cutscene_status = 22
	endif 

	if does_entity_exist(army_truck.veh)
		if is_vehicle_driveable(army_truck.veh)
			printvector(get_entity_coords(army_truck.veh))
			printnl()
		endif 
	endif 
	
	printstring("ambulance_cutscene_status")
	printint(ambulance_cutscene_status)
	printnl()
		
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2198877
	
	switch ambulance_cutscene_status 
	
		case 0

			if IS_AUDIO_SCENE_ACTIVE("FBI_4_BLOCK_THE_STREET")
				STOP_AUDIO_SCENE("FBI_4_BLOCK_THE_STREET")
			endif 
			
			start_audio_scene("FBI_4_TRUCK_SMASH_MAIN")
		
			set_entity_proofs(rubbish_truck.veh, true, true, true, true, true)//stops it being destroyed in shootout
						
			set_vehicle_doors_locked(tow_truck.veh, vehiclelock_unlocked)

			remove_ped_for_dialogue(scripted_speech[0], 0)
			remove_ped_for_dialogue(scripted_speech[0], 1)
			remove_ped_for_dialogue(scripted_speech[0], 2)
			remove_ped_for_dialogue(scripted_speech[0], 3)
			remove_ped_for_dialogue(scripted_speech[0], 4)
			
			SET_PED_NON_CREATION_AREA(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
			
			SET_DISTANT_CARS_ENABLED(false)

			clear_area(<<922.4016, -2156.8010, 29.4934>>, 1000.00, true)

			camera_a = create_cam_with_params("default_scripted_camera", <<910.214539,-2388.130859,34.777031>>,<<-2.897402,0.000003,13.471053>>,36.062519)
			camera_b = create_cam_with_params("default_scripted_camera", <<910.232544,-2388.202881,33.276062>>,<<-2.897402,0.000003,13.471053>>,34.062519)
			
			add_ped_for_dialogue(scripted_speech[0], 0, player_ped_id(), "michael")
			
			
			set_vehicle_fixed(tow_truck.veh)
			set_entity_coords(tow_truck.veh, <<1024.1974, -2376.5244, 29.5306>>)
			set_entity_heading(tow_truck.veh, 85.7590)
			set_model_as_no_longer_needed(tow_truck.model)
			
			//create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], char_franklin, tow_truck.veh, vs_driver, false)
			//SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[SELECTOR_PED_FRANKLIN], 2000)			
			set_ped_into_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], tow_truck.veh)
			//SET_PED_PROP_INDEX(selector_ped.pedID[SELECTOR_PED_FRANKLIN], anchor_head, 0, 0)
			add_ped_for_dialogue(scripted_speech[0], 1, selector_ped.pedID[SELECTOR_PED_FRANKLIN], "franklin")
			
			make_selector_ped_selection(selector_ped, SELECTOR_PED_FRANKLIN)
			take_control_of_selector_ped(selector_ped)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)

		
			//SET_VEHICLE_SIREN(rubbish_truck.veh, true)
			SET_ENTITY_COORDS(rubbish_truck.veh, ambulance_target_pos)  
			//SET_ENTITY_HEADING(rubbish_truck.veh, 198.8476)
			set_vehicle_on_ground_properly(rubbish_truck.veh)
			ACTIVATE_PHYSICS(rubbish_truck.veh)

			set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_MICHAEL], TRUE)
			task_look_at_coord(selector_ped.pedID[SELECTOR_PED_MICHAEL], <<907.64, -2373.35, 31.5>>, 5000)

		
			army_truck.veh = create_vehicle(army_truck.model, <<922.4016, -2156.8010, 29.4934>>, 174.54)
			set_vehicle_has_strong_axles(army_truck.veh, true)
			set_vehicle_doors_locked(army_truck.veh, vehiclelock_lockout_player_only) 
			set_vehicle_automatically_attaches(army_truck.veh, false)
			army_truck.blip = create_blip_for_vehicle(army_truck.veh, true)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(army_truck.veh, "FBI_4_STOCKADE_GROUP")
			SET_FORCE_HD_VEHICLE(army_truck.veh, true)
			
			army_truck_driver.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_driver.model)
			set_ped_dies_when_injured(army_truck_driver.ped, true)
			//DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_driver.ped)
			DISABLE_PED_PAIN_AUDIO(army_truck_driver.ped, true)
			set_blocking_of_non_temporary_events(army_truck_driver.ped, true)
			add_ped_for_dialogue(scripted_speech[0], 6, army_truck_driver.ped, "AGENT1")
			
			army_truck_passenger.ped = create_ped_inside_vehicle(army_truck.veh, pedtype_mission, army_truck_passenger.model, vs_front_right) 
			set_ped_dies_when_injured(army_truck_passenger.ped, true)
			//DISABLE_PED_INJURED_ON_GROUND_BEHAVIOUR(army_truck_passenger.ped)
			DISABLE_PED_PAIN_AUDIO(army_truck_passenger.ped, true)
			set_blocking_of_non_temporary_events(army_truck_passenger.ped, true)
			add_ped_for_dialogue(scripted_speech[0], 7, army_truck_passenger.ped, "AGENT2")

			start_playback_recorded_vehicle(army_truck.veh, 002, "lkcountry")
			skip_time_in_playback_recorded_vehicle(army_truck.veh, 7500)//7000
			force_playback_recorded_vehicle_update(army_truck.veh)
					
			camera_c = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_c, army_truck.veh, <<2.180, -0.300, 0.320>>)
			POINT_CAM_AT_ENTITY(camera_c, army_truck.veh, <<1.400, 8.000, 0.600>>)
			set_cam_fov(camera_c, 50.00)
			
			camera_d = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_d, army_truck.veh, <<2.210, -0.800, 1.630>>)
			POINT_CAM_AT_ENTITY(camera_d, army_truck.veh, <<-0.800, 8.000, 0.700>>)
			set_cam_fov(camera_d, 35.00)
			
			camera_e = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_e, army_truck.veh, <<1.580, 5.830, 0.780>>) 
			POINT_CAM_AT_ENTITY(camera_e, army_truck.veh, <<-1.200, 0.200, 1.100>>)
			set_cam_fov(camera_e, 32.00)
			
			camera_h = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_h, army_truck.veh, <<0.820, 4.850, 1.430>>)   
			POINT_CAM_AT_ENTITY(camera_h, army_truck.veh, <<-0.600, -0.200, 1.400>>)
			set_cam_fov(camera_h, 32.000)
			
			camera_f = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_f, tow_truck.veh, <<-0.210, 0.75, 1.35>>)//0.7
			POINT_CAM_AT_ENTITY(camera_f, tow_truck.veh, <<-3.1, 20.00, -1.3>>)
			set_cam_fov(camera_f, 45.00)

			camera_g = create_cam("default_scripted_camera", false)
			ATTACH_CAM_TO_ENTITY(camera_g, tow_truck.veh, <<-0.210, 0.8, 1.35>>) //0.75
			POINT_CAM_AT_ENTITY(camera_g, tow_truck.veh, <<-3.1, 20.00, -1.3>>)
			set_cam_fov(camera_g, 45.00)
			

//			vector cascade_pos 
//			cascade_pos = get_offset_from_entity_in_world_coords(rubbish_truck.veh, <<0.0, 4.0, 0.0>>)
//			CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0, true, cascade_pos.x, cascade_pos.y, cascade_pos.z, 2.5)
			
			//SET_SIREN_CAN_BE_CONTROLLED_BY_AUDIO(rubbish_truck.veh, false)
			//SET_VEHICLE_SIREN(rubbish_truck.veh, true)
	
			streaming_volume = STREAMVOL_CREATE_SPHERE(<<905.92, -2367.47, 30.54>>, 20.00, FLAG_COLLISIONS_MOVER)//20 | FLAG_COLLISIONS_WEAPON) //FLAG_MAPDATA |

			cutscene_pos = <<892.138, -2368.970, 30.939>>
			cutscene_rot = <<0.0, 0.0, 0.0>>

			set_cam_active(camera_c, true)
			shake_cam(camera_c, "ROAD_VIBRATION_SHAKE", 2.5)
			
			camera_anim = CREATE_CAMERA(camtype_animated, true)
			play_cam_anim(camera_anim, "fbi4_garagetruck_block_cam", "missfbi4", cutscene_pos, cutscene_rot)
			
			render_script_cams(true, false)
			
			original_time = get_game_timer()
			
			trigger_music_event("fbi4_PARK_AMBULANCE_OS")
			
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(tow_truck.veh, "FBI_4_TOWTRUCK_GROUP")

			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

			ambulance_cutscene_status++
		
		break 
		
		case 1
		
			if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 1.0

				//destroy_cam(camera_anim)
				set_cam_active(camera_anim, false)

				CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(true)
			
				set_cam_active(camera_c, true)
				
				original_time = get_game_timer()

				ambulance_cutscene_status++
				
			endif 
			
		break 
				
		
		case 2
		
			if lk_timer(original_time, 2000) //2500
			
				set_cam_active(camera_d, false)
				set_cam_active(camera_c, false)
	
				set_cam_active(camera_e, true)
				set_cam_active_with_interp(camera_h, camera_e, 3000, graph_type_linear)
				
				shake_cam(camera_h, "ROAD_VIBRATION_SHAKE", 2.5) //HAND_SHAKE ROAD_VIBRATION_SHAKE
				
				create_conversation(scripted_speech[0], "heataud", "amb_cut_1", CONV_PRIORITY_medium)

				original_time = get_game_timer()
				
				ambulance_cutscene_status++

			endif 

		break 
		
		case 3
		
			if lk_timer(original_time, 2000)//2500
			
				set_cam_active(camera_h, false)
				set_cam_active(camera_e, false)
				
				skip_time_in_playback_recorded_vehicle(army_truck.veh, 1000)
				force_playback_recorded_vehicle_update(army_truck.veh)
				
				SET_VEHICLE_LIGHTS(tow_truck.veh, force_vehicle_lights_on)

				start_playback_recorded_vehicle(tow_truck.veh, 015, "lkcountry")
				skip_time_in_playback_recorded_vehicle(tow_truck.veh, 1500)
				
				//set_cam_active(camera_j, true)
				
				cutscene_pos = <<924.627, -2362.0, 29.516>>
				cutscene_rot = <<0.0, 0.0, 0.0>>

				play_cam_anim(camera_anim, "fbi4_securityvan_blocked_cam", "missfbi4", cutscene_pos, cutscene_rot)  
				SET_CAM_ACTIVE(camera_anim, true)
				
				//start_vehicle_horn(army_truck.veh, 2500)

				original_time = get_game_timer()
				
				ambulance_cutscene_status++
				
			endif 
			
		break 
	
				
		case 4
		
			if lk_timer(original_time, 1000)
			
				//create_conversation(scripted_speech[0], "heataud", "amb_cut_1b", CONV_PRIORITY_medium)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
				//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "heataud", "amb_cut_1b", CONV_PRIORITY_MEDIUM)

			//	start_vehicle_horn(army_truck.veh, 2500)
				
				trigger_music_event("fbi4_PRE_TRUCK_RAM_MA")

				//set_cam_active_with_interp(camera_k, camera_j, 3700, graph_type_linear)

				original_time = get_game_timer()
				
				ambulance_cutscene_status++
			endif 

		break 

		case 5
		
			if not IS_MUSIC_ONESHOT_PLAYING()
				prepare_music_event("FBI4_RAM_OS")
			endif 

			if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 1.0

				set_cam_active(camera_anim, false)
				
//					CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0, false, 0.0, 0.0, 0.0, 2.5)
//					CASCADE_SHADOWS_SET_CASCADE_BOUNDS(1, false, 0.0, 0.0, 0.0, 2.5)
				
				clear_area(<<1030.1470, -2376.7976, 29.4686>>, 150.00, true) 

				SET_ENTITY_COORDS(army_truck.veh, <<904.981, -2367.179, 30.150>>)
				SET_ENTITY_HEADING(army_truck.veh, 175.2936)

				if is_playback_going_on_for_vehicle(tow_truck.veh)
					stop_playback_recorded_vehicle(tow_truck.veh)
				endif 
				
				SET_ENTITY_COORDS(tow_truck.veh, <<1030.1470, -2376.7976, 29.4686>>) 
				SET_ENTITY_HEADING(tow_truck.veh, 86.1538) 

				start_playback_recorded_vehicle(tow_truck.veh, 012, "lkcountry")
				skip_time_in_playback_recorded_vehicle(tow_truck.veh, 4500) //5500
				set_playback_speed(tow_truck.veh, 1.0) //0.7
				//force_ped_ai_and_animation_update(player_ped_id())
				force_playback_recorded_vehicle_update(tow_truck.veh)
				
//					set_cam_active(camera_f, true)
//					set_cam_active_with_interp(camera_g, camera_f, 3000, graph_type_linear)
				
				
				
				//-----------cew synched scene camera
				
				destroy_cam(camera_anim)
				
				cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
				cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
	
				cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)

				if is_vehicle_driveable(tow_truck.veh)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(cutscene_index, tow_truck.veh, 0)
				endif
				
				camera_anim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
				PLAY_SYNCHRONIZED_CAM_ANIM(camera_anim, cutscene_index, "fbi4_franklin_truck_cam", "missfbi4")
				SET_CAM_ACTIVE(camera_anim, true)
				force_ped_ai_and_animation_update(player_ped_id(), true)	
			

				original_time = get_game_timer()
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "heataud", "amb_cut_2", CONV_PRIORITY_MEDIUM)
				
				trigger_music_event("FBI4_RAM_OS")
				
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(tow_truck.veh)
				
				start_audio_scene("FBI_4_PICKUP_TRUCK_INT")
				
				ambulance_cutscene_status++

			endif 

		break 
		
		case 6
		
			//DISPLAY_PLAYBACK_RECORDED_VEHICLE(tow_truck.veh, rdm_wholeline)

			//if lk_timer(original_time, 2500) //or not is_playback_going_on_for_vehicle(tow_truck.veh)//2500
			//if not is_playback_going_on_for_vehicle(tow_truck.veh)
			if (get_time_position_in_recording(tow_truck.veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(012, "lkcountry")

				if IS_AUDIO_SCENE_ACTIVE("FBI_4_TRUCK_SMASH_MAIN")
					STOP_AUDIO_SCENE("FBI_4_TRUCK_SMASH_MAIN")
				endif 
			
				CASCADE_SHADOWS_INIT_SESSION() 
				
				SET_FORCE_HD_VEHICLE(army_truck.veh, false)
				
				players_vehicle_cam_mode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
				SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_FIRST_PERSON)

				//if IS_STREAMVOL_ACTIVE
				
				STREAMVOL_DELETE(streaming_volume)
				
				SET_DISTANT_CARS_ENABLED(true)
			
				truck_ram_time = get_game_timer()
				
				REPLAY_STOP_EVENT()
				
				end_cutscene_no_fade(false, true, false, 0, 0, 3000, false)

				print_now("cntry_god3", default_god_text_time, 1)
				
				return true
				
			endif
			
		break 
		
		case 22
		
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(500)
				endif 
				
			else 
			
				REPLAY_CANCEL_EVENT()
			
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_TRUCK_SMASH_MAIN")
					STOP_AUDIO_SCENE("FBI_4_TRUCK_SMASH_MAIN")
				endif 
			
				//CASCADE_SHADOWS_SET_CASCADE_BOUNDS(0, false, 905.92, -2367.47, 30.54, 50.00)
				CASCADE_SHADOWS_INIT_SESSION() 
				
				SET_FORCE_HD_VEHICLE(army_truck.veh, false)
			
				request_vehicle_recording(012, "lkcountry")
	
				while not has_vehicle_recording_been_loaded(012, "lkcountry")
					wait(0)
				endwhile 
				
				//if IS_STREAMVOL_ACTIVE
				
				STREAMVOL_DELETE(streaming_volume)
				
				SET_DISTANT_CARS_ENABLED(true)
			
				if is_playback_going_on_for_vehicle(army_truck.veh)
					skip_to_end_and_stop_playback_recorded_vehicle(army_truck.veh)
				endif 
				SET_ENTITY_COORDS(army_truck.veh, <<904.981, -2367.179, 30.150>>)
				SET_ENTITY_HEADING(army_truck.veh, 175.2936)
				
				if is_playback_going_on_for_vehicle(tow_truck.veh)
					stop_playback_recorded_vehicle(tow_truck.veh)
				endif 
			
				SET_ENTITY_COORDS(tow_truck.veh, <<1030.1470, -2376.7976, 29.4686>>) 
				SET_ENTITY_HEADING(tow_truck.veh, 86.1538) 
				SET_VEHICLE_LIGHTS(tow_truck.veh, force_vehicle_lights_on)

				start_playback_recorded_vehicle(tow_truck.veh, 012, "lkcountry")
				skip_time_in_playback_recorded_vehicle(tow_truck.veh, 6500)
				set_playback_speed(tow_truck.veh, 1.0)
				
				players_vehicle_cam_mode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() 
				SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_FIRST_PERSON)
				
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(tow_truck.veh)
				
				if not is_audio_scene_active("FBI_4_PICKUP_TRUCK_INT")
					start_audio_scene("FBI_4_PICKUP_TRUCK_INT")
				endif 
				
				truck_ram_time = get_game_timer()
				
				end_cutscene(true, true, 0, 0, false)
				
				print_now("cntry_god3", default_god_text_time, 1)
				
				REPLAY_STOP_EVENT()
				
				return true
				
			endif 
		
		break 

	endswitch 
				

	return false 

endfunc

proc setup_previous_ped_blip()
		
	switch selector_ped.ePreviousSelectorPed 
	
		case SELECTOR_PED_MICHAEL
	
			if not does_blip_exist(michael_blip)
				michael_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				set_blip_as_friendly(michael_blip, true)
				set_blip_display(michael_blip, display_blip)
				set_blip_scale(michael_blip, 0.5)
			endif 
			
			if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				set_ped_can_ragdoll(selector_ped.pedID[SELECTOR_PED_MICHAEL], false)
			endif 
				
		break 

		case SELECTOR_PED_FRANKLIN
		
			if not does_blip_exist(franklin_blip)
				franklin_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				set_blip_as_friendly(franklin_blip, true)
				set_blip_display(franklin_blip, display_blip)
				set_blip_scale(franklin_blip, 0.5)
			endif 
			
			if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				set_ped_can_ragdoll(selector_ped.pedID[SELECTOR_PED_FRANKLIN], false)
			endif 
			
		break 
		
		case SELECTOR_PED_TREVOR
		
			if not does_blip_exist(trevor_blip)
				trevor_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_TREVOR])
				set_blip_as_friendly(trevor_blip, true)
				set_blip_display(trevor_blip, display_blip)
				set_blip_scale(trevor_blip, 0.5)
			endif 
			
			if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_TREVOR])
				set_ped_can_ragdoll(selector_ped.pedID[SELECTOR_PED_TREVOR], false)
			endif 

		break 
		
	endswitch 
	
endproc 

//float ingame_camera_heading

func float get_final_ingame_camera_heading_relative_to_player(ped_index new_selector_ped)

	vector player_offset_vec = get_offset_from_entity_in_world_coords(new_selector_ped, <<0.0, 1.0, 0.0>>) - get_entity_coords(new_selector_ped)

	vector target_vec = normalise_vector(<<893.53, -2353.36, 30.52>> - get_entity_coords(new_selector_ped))

	FLOAT rot_z = GET_ANGLE_BETWEEN_2D_VECTORS(player_offset_vec.x, player_offset_vec.y, target_vec.x, target_vec.y)
	
//	//VECTOR cross_product_vec = cross_product(<<0.0,1.0,0.0>>, target_vec)//CROSS_PRODUCT(player_offset_vec, target_vec)
//	VECTOR cross_product_vec = CROSS_PRODUCT(player_offset_vec, target_vec)
//
//	IF cross_product_vec.z > 0
//		rot_z = ABSF(rot_z)
//	ELSE
//		rot_z = (ABSF(rot_z) * -1)
//	ENDIF
//	
//	if rot_z < 0.0
//		rot_z+= 360.00
//	endif
	
	return rot_z
	
	//use that to pass into final spline node rotation
	
	//another method = GET_ANGLE_BETWEEN_2D_VECTORS - player heading in world coords
	//			
	
endfunc 

proc update_extra_spline_cam_node_pos_and_rot(ped_index buddy_selector_ped, vector look_at_pos, float z_pos_above_ped = 1.0)
	
	if not is_ped_injured(buddy_selector_ped)
	
		vector target_vec
		vector final_node_camera_pos
		
		target_vec =  look_at_pos - get_entity_coords(buddy_selector_ped)

		final_node_rot = GET_ENTITY_ROTATION(buddy_selector_ped)
		final_node_rot.z = get_heading_from_vector_2d(target_vec.x, target_vec.y)

		final_node_camera_pos = get_entity_coords(buddy_selector_ped) - (normalise_vector(target_vec) * 1.5)
		//final_node_camera_pos = get_offset_from_entity_in_world_coords(buddy_selector_ped, <<-1.5, 0.0, 0.5>>) - (normalise_vector(target_vec) * 1.5)
		final_node_camera_pos.z += z_pos_above_ped
		
		set_cam_coord(final_node_cam, final_node_camera_pos)  
		set_cam_rot(final_node_cam, final_node_rot) 
		
	endif 
	
endproc 

//func float get_final_gameplay_cam_relative_heading_to_look_at_pos(ped_index &ped_to, vector look_at_pos)
//	
//	if not is_ped_injured(ped_to)
//	
//		vector target_vec
//			
//		target_vec = look_at_pos - get_entity_coords(ped_to)
//		
//		return (get_heading_from_vector_2d(target_vec.x, target_vec.y) - get_entity_heading(ped_to))
//		
//	endif 
//	
//endfunc 

func bool setup_hotswap_cam_to_michael()

	sCamDetails.pedTo = selector_ped.pedID[SELECTOR_PED_michael]

	if does_blip_exist(michael_blip)
		remove_blip(michael_blip)
	endif 
	
	clear_help()
	switch_reminder = true 

	switched_to_michael = true

	return true

endfunc 

func bool setup_hotswap_cam_to_franklin()

	sCamDetails.pedTo = selector_ped.pedID[SELECTOR_PED_FRANKLIN]
	
	if does_blip_exist(franklin_blip)
		remove_blip(franklin_blip)
	endif 
	
	clear_help()
	switch_reminder = true 
	
	switched_to_franklin = true
	
	return true

endfunc 

func bool setup_hotswap_cam_to_trevor()

	REFILL_AMMO_INSTANTLY(selector_ped.pedID[SELECTOR_PED_TREVOR])
	
	sCamDetails.pedTo = selector_ped.pedID[SELECTOR_PED_TREVOR]
	
	if does_blip_exist(trevor_blip)
		remove_blip(trevor_blip)
	endif
	
	clear_help()
	switch_reminder = true 
	
	switched_to_trevor = true

	trevor_ai_system_status = 2

	return true

endfunc 

PROC INITIALISE_ROPE(rope_struct &s_rope, VECTOR v_pos, VECTOR v_rot, FLOAT f_length, PHYSICS_ROPE_TYPE physics_type)
	
	s_rope.rope = ADD_ROPE(v_pos, v_rot, f_length, physics_type)
	s_rope.f_length = f_length
	s_rope.i_num_segments = GET_ROPE_VERTEX_COUNT(s_rope.rope)
	s_rope.f_length_per_segment = s_rope.f_length / TO_FLOAT(s_rope.i_num_segments)
	
ENDPROC

PROC ATTACH_ROPE_TO_RAPPELLING_PED(rope_struct &s_rope, PED_INDEX ped, VECTOR v_rope_start_pos)
	
	VECTOR v_hand_pos
	vector v_curr_vertex_pos
	vector v_next_vertex_pos
	vector v_dir_to_hand
	FLOAT f_horizontal_dist_to_hand
	float f_z_dist_from_prev_vertex
	INT i_current_vertex
	int j
	BOOL b_has_rope_reached_hand

	IF NOT IS_PED_INJURED(ped)
		v_hand_pos = GET_PED_BONE_COORDS(ped, BONETAG_L_HAND, <<0,0,0>>)
		
		//Calculate the directional vector from the rope start to the ped's hand //normalise vector 
		v_dir_to_hand = normalise_vector((v_hand_pos - v_rope_start_pos))// / VMAG(v_hand_pos - v_rope_start_pos)
		
		//Each vertex before the hand is pinned such that they form a line leading to the hand
		i_current_vertex = 0
		b_has_rope_reached_hand = FALSE
		v_curr_vertex_pos = <<0.0, 0.0, 0.0>>
		
		WHILE i_current_vertex < s_rope.i_num_segments AND NOT b_has_rope_reached_hand
			IF i_current_vertex = 0
				v_curr_vertex_pos = v_rope_start_pos
			ELSE
				v_curr_vertex_pos = v_rope_start_pos + (v_dir_to_hand * (s_rope.f_length_per_segment * i_current_vertex))
			ENDIF
			
			PIN_ROPE_VERTEX(s_rope.rope, i_current_vertex, v_curr_vertex_pos)
			if (get_distance_between_coords(v_curr_vertex_pos, v_hand_pos) < s_rope.f_length_per_segment)
			//IF DISTANCE_BETWEEN_COORDS(v_curr_vertex_pos, v_hand_pos, FALSE, FALSE) < (s_rope.f_length_per_segment * s_rope.f_length_per_segment)
				b_has_rope_reached_hand = TRUE
			ENDIF
			
			i_current_vertex++
		ENDWHILE
		
		//The next vertex is positioned directly under the hand (x,y coords are the same as the hand x,y).
		//The segment length must stay consistent, so z may vary. We know the segment length and can find the horizontal distance between the previous vertex
		//and the hand.
		//Using these values we can work out where z should be for the next vertex.
		v_next_vertex_pos = v_hand_pos
		//f_horizontal_dist_to_hand = DISTANCE_BETWEEN_COORDS(v_curr_vertex_pos, v_hand_pos, TRUE, TRUE) 
		f_horizontal_dist_to_hand = get_distance_between_coords(v_curr_vertex_pos, v_hand_pos, false)
		f_z_dist_from_prev_vertex = SQRT((s_rope.f_length_per_segment * s_rope.f_length_per_segment) - (f_horizontal_dist_to_hand * f_horizontal_dist_to_hand))								
		v_next_vertex_pos.z = v_curr_vertex_pos.z - f_z_dist_from_prev_vertex
		PIN_ROPE_VERTEX(s_rope.rope, i_current_vertex, v_next_vertex_pos)
		i_current_vertex++
		
		//The rest of the vertices are  to hang straight down
		j = 1
		WHILE i_current_vertex < s_rope.i_num_segments
			PIN_ROPE_VERTEX(s_rope.rope, i_current_vertex, v_next_vertex_pos - <<0.0, 0.0, s_rope.f_length_per_segment * j>>)
			i_current_vertex++
			j++
		ENDWHILE
	ENDIF
ENDPROC

PROC ATTACH_ROPE_TO_SINGLE_POINT(rope_struct &mission_rope, VECTOR v_pos)

	int k = 0
	
	for k = 0 to (mission_rope.i_num_segments - 1)
	
		IF k = 0
			PIN_ROPE_VERTEX(mission_rope.rope, k, v_pos)
		ELSE
			UNPIN_ROPE_VERTEX(mission_rope.rope, k)
		ENDIF
	
	endfor 
ENDPROC

proc rope_system()

	//if not is_ped_injured(police_man[16].ped)

//	if rope_system_status > 0
//		if rope_system_status < 3
//			if not IS_ENTITY_PLAYING_ANIM(police_man[16].ped, "misssagrab", "rappel_intro_player")
//
//				ATTACH_ROPE_TO_SINGLE_POINT(rope[0], rope[0].pos)
//				
//				SET_PED_GRAVITY(police_man[16].ped, TRUE)
//				
//				clear_ped_tasks(police_man[16].ped)
//				
//				original_time = get_game_timer()
//				
//				rope_system_status = 3
//			endif 
//		endif 
//	endif 
	
	DRAW_MARKER(MARKER_CYLINDER, <<907.97, -2359.48, 40.0>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<2.0, 2.0, 2.0>>, 255, 0, 128, 178)
	

	switch rope_system_status
	
		case 0
		
			request_model(helicopter[0].model)
//			request_model(police_man[15].model)
//			request_anim_dict("misssagrab")
//			
//			if has_model_loaded(helicopter.model)
//			and has_model_loaded(police_man[15].model)
//			and has_anim_dict_loaded("misssagrab")
//			
//				helicopter.veh = CREATE_VEHICLE(helicopter.model, helicopter.pos, helicopter.heading)
//				FREEZE_ENTITY_POSITION(helicopter.veh, TRUE)
//				SET_VEHICLE_ENGINE_ON(helicopter.veh, TRUE, TRUE)
//				SET_HELI_BLADES_FULL_SPEED(helicopter.veh)
//				SET_MODEL_AS_NO_LONGER_NEEDED(helicopter.model)
//				
//				police_man[15].ped = create_ped_inside_vehicle(helicopter.veh, pedtype_mission, police_man[15].model)
//			
//				//Rope test
//				rope.pos = get_offset_from_entity_in_world_coords(helicopter.veh, <<-0.75, 0.0, 1.25>>)
//				GET_GROUND_Z_FOR_3D_COORD(rope.pos, ground_z)
//				
//				printfloat(ground_z)
//				printnl()
//				
//				INITIALISE_ROPE(rope, rope.pos, <<0.0, 90.0, 0.0>>, (rope.pos.z - (ground_z + 0.2)), PHYSICS_ROPE_DEFAULT)
//				PIN_ROPE_VERTEX(rope.rope, 0, rope.pos)
//
//				police_man[16].pos = get_offset_from_entity_in_world_coords(helicopter.veh, <<-0.65, 0.0, -1.0>>)
//				police_man[16].heading = GET_ENTITY_HEADING(helicopter.veh) - 180.0
//				setup_enemy(police_man[16])
//			
//				SET_PED_GRAVITY(police_man[16].ped, FALSE)
//				TASK_PLAY_ANIM(police_man[16].ped, "misssagrab", "rappel_intro_player", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)	
//
//				rope_system_status++
//			endif 

		break 
		
		case 1
		
			rope[0].pos = get_offset_from_entity_in_world_coords(helicopter[0].veh, <<-0.75, 0.0, 1.25>>)
		
//			IF GET_ENTITY_ANIM_CURRENT_TIME(police_man[16].ped, "misssagrab", "rappel_intro_player") > 0.7
//			
//				SET_ENTITY_ANIM_SPEED(police_man[16].ped, "misssagrab", "rappel_intro_player", 0.0)
//				
//				SET_ENTITY_VELOCITY(police_man[16].ped, <<0.0, 0.0, -7.0>>)
//				
//				rope_system_status++
//			endif
			
//			ATTACH_ROPE_TO_RAPPELLING_PED(rope[0], police_man[16].ped, rope[0].pos)
			
//			for i = 0 to (rope.i_num_segments - 1)
//				printvector(GET_ROPE_VERTEX_COORD(rope.rope, i))
//				printnl()
//			endfor 
//			printstring("END2******************")
//			printnl()

		break 
		
		case 2

			rope[0].pos = get_offset_from_entity_in_world_coords(helicopter[0].veh, <<-0.75, 0.0, 1.25>>)
			
//			vector v_hand_pos
//			vector v_rope_end_pos
//			v_hand_pos = GET_PED_BONE_COORDS(police_man[16].ped, BONETAG_L_HAND, <<0.0, 0.0, 0.0>>)
//			v_rope_end_pos = GET_ROPE_VERTEX_COORD(rope[0].rope, rope[0].i_num_segments - 1)
							
//			IF (v_hand_pos.z < (v_rope_end_pos.z + 1.5))
//
//				ATTACH_ROPE_TO_SINGLE_POINT(rope[0], rope[0].pos)
//				
//				SET_PED_GRAVITY(police_man[16].ped, TRUE)
//				
//				original_time = get_game_timer()
//				
//				rope_system_status++
//			
//			else 
//			
//				ATTACH_ROPE_TO_RAPPELLING_PED(rope[0], police_man[16].ped, rope[0].pos)
//					
//				SET_ENTITY_VELOCITY(police_man[16].ped, <<0.0, 0.0, -7.0>>)
//				
//			endif 
			
//			for i = 0 to (rope.i_num_segments - 1)
//				printvector(GET_ROPE_VERTEX_COORD(rope.rope, i))
//				printnl()
//			endfor 
//			printstring("END2******************")
//			printnl()
		
		break 
		
		case 3
		
//			for i = 0 to (rope.i_num_segments - 1)
//				printvector(GET_ROPE_VERTEX_COORD(rope.rope, i))
//				printnl()
//			endfor 
//			printstring("END2******************")
//			printnl()
		
//			police_man[16].pos.z = GET_ENTITY_HEIGHT_ABOVE_GROUND(police_man[16].ped)
//			printfloat(GET_ENTITY_HEIGHT_ABOVE_GROUND(police_man[16].ped))
//			printnl()
			
//			open_sequence_task(seq)
//				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<934.68, -2174.1, 30.04>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
//			close_sequence_task(seq)
//			task_perform_sequence(police_man[16].ped, seq)
//			clear_sequence_task(seq)
		
			rope_system_status++
			
		break 
		
		case 4
		
//			if not IS_ENTITY_AT_COORD(police_man[16].ped, <<934.68, -2174.1, 30.04>>, <<1.0, 1.0, 1.6>>, false, true)
//				if has_ped_task_finished_2(police_man[16].ped, script_task_perform_sequence)
//					if GET_ENTITY_HEIGHT_ABOVE_GROUND(police_man[16].ped) < 1.2
//					
//						clear_ped_tasks(police_man[16].ped)
//						set_blocking_of_non_temporary_events(police_man[16].ped, true)
//					
//						open_sequence_task(seq)
//							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<934.68, -2174.1, 30.04>>, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
//						close_sequence_task(seq)
//						task_perform_sequence(police_man[16].ped, seq)
//						clear_sequence_task(seq)
//						
//					endif 
//				endif 
//			endif 

//			if lk_timer(original_time, 15000)
//
//				DELETE_PED(police_man[15].ped)
//				DELETE_PED(police_man[16].ped)
//				DELETE_VEHICLE(helicopter.veh)
//				delete_rope(rope[0].rope)
//			
//				rope_system_status = 0
//				
//			endif 
			
		break 
		
	endswitch 
	
endproc 

proc park_rubbish_truck_dialogue_system()
						
	if not has_label_been_triggered("park_truck")
		if has_label_been_triggered("stop_truck")
			if not is_any_text_being_displayed(locates_data)
				if create_conversation(scripted_speech[0], "HeatAud", "park_truck", conv_priority_medium)  
					set_label_as_triggered("park_truck", true)
				endif 
			endif 
		endif 
	endif 
	
	if not has_label_been_triggered("heat_tofar0")
		if not is_any_text_being_displayed(locates_data)
			//ally coords
			if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
				if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), <<1016.7465, -2375.7974, 29.5309>>) < 55.00 
				and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_franklin])) < 15.00
					if create_conversation(scripted_speech[0], "heataud", "heat_tofar0", CONV_PRIORITY_LOW)
						set_label_as_triggered("heat_tofar0", true)
					endif 
				endif 
			endif 
		endif 
	endif 
	
endproc 				


proc fbi4_park_ambulance()

//	vehicle_index temp_car
//	
//	if get_current_player_vehicle(temp_car)
//		if is_vehicle_driveable(temp_car)
//			if is_playback_going_on_for_vehicle(temp_car)
//				printfloat(get_time_position_in_recording(temp_car))
//				printnl()
//				
//				if get_time_position_in_recording(temp_car) > 42700
//		
//					printstring("fly ************")
//					printnl()
//					printstring("fly ************")
//					printnl()
//					printstring("fly ************")
//					printnl()
//					printstring("fly ************")
//					printnl()
//					
//				endif 
//			endif 
//		endif 
//	endif 
	
	//rope_system()
	
	IF PARK_AMBULANCE_STATUS < 1
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() //Fix for bug 2198877
	ENDIF

	switch park_ambulance_status 
	
		case 0
		
			if does_entity_exist(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				if not is_ped_in_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], tow_truck.veh)
					if has_ped_task_finished_2(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
						
						open_sequence_task(seq)
							task_enter_vehicle(null, tow_truck.veh, -1)
						close_sequence_task(seq)
						task_perform_sequence(selector_ped.pedID[SELECTOR_PED_FRANKLIN], seq)
						clear_sequence_task(seq)
					
					endif 
				endif 
			endif 
		
			if is_playback_going_on_for_vehicle(rubbish_truck.veh)
				stop_playback_recorded_vehicle(rubbish_truck.veh)
				original_time = 0
				park_time = 0
			endif 
			
			if not has_label_been_triggered("park_truck")
				if create_conversation(scripted_speech[0], "HeatAud", "park_truck", conv_priority_medium)
					set_label_as_triggered("park_truck", true)
				endif 
			endif 
			
			IS_PLAYER_AT_LOCATION_IN_VEHICLE(locates_data, <<906.2, -2375.5010, 29.5320>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, false, rubbish_truck.veh, 
	  		"cntry_god2", "cntry_god14", "cntry_god13", true)
			
//			if IS_PLAYER_AT_ANGLED_AREA_IN_VEHICLE(locates_data, ambulance_target_pos, <<912.891, -2376.258, 29.409>>,
//			<<900.659, -2374.972, 33.309>>, 16.8, true, rubbish_truck.veh, "cntry_god2", "cntry_god14", "cntry_god13", true)  
		

			if does_blip_exist(locates_data.LocationBlip)

				if not has_label_been_triggered("cntry_help8")
					if is_entity_in_angled_area(player_ped_id(), <<917.759, -2375.988, 29.409>>, <<896.070, -2373.784, 33.4>>, 55.000)  
						print_help("cntry_help8")
						set_label_as_triggered("cntry_help8", true)
					endif 
				endif 
				
				if is_entity_in_angled_area(player_ped_id(), <<912.891, -2376.258, 29.409>>, <<900.659, -2374.972, 33.309>>, 16.8, false)
				
					if not has_label_been_triggered("stop_truck")
						if not is_any_text_being_displayed(locates_data)
							if create_conversation(scripted_speech[0], "HeatAud", "stop_truck", conv_priority_medium)  
								set_label_as_triggered("stop_truck", true)
							endif 
						endif 
					endif 
					
					if does_blip_exist(locates_data.LocationBlip)
						remove_blip(locates_data.LocationBlip)
					endif 

					rubbish_truck.heading = get_entity_heading(rubbish_truck.veh)
					
					if (rubbish_truck.heading > 215 and rubbish_truck.heading < 305)
					or (rubbish_truck.heading > 50.000 and rubbish_truck.heading < 140.00)
					
//						printfloat(get_entity_speed(rubbish_truck.veh))
//						printnl()
//
//						printstring("test 0")
//						printnl()
					
						if get_entity_speed(rubbish_truck.veh) < 0.5
						
							if is_this_help_message_being_displayed("cntry_help8")
								clear_help()
							endif 

							if not is_any_text_being_displayed(locates_data)
								if has_label_been_triggered("stop_truck")
									
									if park_time = 0
										park_time = get_game_timer()
									endif 
									
									if lk_timer(park_time, 500)//2000
				
										if start_new_cutscene_no_fade(false)
									
											clear_mission_locate_stuff(locates_data)

											park_ambulance_status++

											//comment back in when re-doing truck physics
											//park_ambulance_status = 22
											
										endif 
										
									endif 
									
								endif 
								
							endif 

						else 
						
							if get_entity_speed(rubbish_truck.veh) > 5.0
							
								park_rubbish_truck_dialogue_system()

							endif 

							park_time = 0
						
						endif 
					
					else 
					
						park_time = 0
						
						park_rubbish_truck_dialogue_system()
					
						if not has_label_been_triggered("cntry_help8")
							if get_entity_speed(rubbish_truck.veh) < 2.0
							
								if original_time = 0
									original_time = get_game_timer()
								endif 
								
								if lk_timer(original_time, 2000)
									print_help_forever("cntry_help8")
									set_label_as_triggered("cntry_help8", true)
								endif 
								
							endif 
							
						else 
						
							if not is_this_help_message_being_displayed("cntry_help8")
								if get_entity_speed(rubbish_truck.veh) < 2.0
									print_help_forever("cntry_help8")
								endif 
							endif 
							
						endif
					
					endif
					
				else 
				
					is_entity_at_coord(player_ped_id(), <<906.2, -2375.5010, 29.7320>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, true)

					park_rubbish_truck_dialogue_system()
					
					park_time = 0
					
//					if is_this_help_message_being_displayed("cntry_help8")
//						clear_help()
//					endif 
				
				endif 
				
			else 
			
				park_rubbish_truck_dialogue_system()
			
				park_time = 0
				
				if is_this_help_message_being_displayed("cntry_help8")
					clear_help()
				endif 
				
			endif 
			
		break 
		
		case 1
		
			prepare_music_event("fbi4_PARK_AMBULANCE_OS")
					
			request_model(army_truck.model)
			set_vehicle_model_is_suppressed(army_truck.model, true)
			
			//REQUEST_VEHICLE_HIGH_DETAIL_MODEL
			
			request_model(army_truck_driver.model)
			set_ped_model_is_suppressed(army_truck_driver.model, true)
			
			request_model(van_light.model)
			
			request_model(get_weapontype_model(weapontype_combatmg))
			request_weapon_asset(weapontype_combatmg)

			LOAD_STREAM("Truck_Crash_Stream", "FBI_04_HEAT_SOUNDS")
			
			create_conversation(scripted_speech[0], "HeatAud", "in_pos_2", CONV_PRIORITY_medium)
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
			
			//stop_player_vehicle()
	
			//set_player_control(player_id(), false)	
			original_time = get_game_timer()
			
			park_ambulance_status++
					
		break 
		
		case 2
		
			if has_model_loaded(army_truck.model)
			and has_model_loaded(army_truck_driver.model)
			and has_model_loaded(van_light.model)
			and has_model_loaded(get_weapontype_model(weapontype_combatmg))
			and has_weapon_asset_loaded(weapontype_combatmg)
			and LOAD_STREAM("Truck_Crash_Stream", "FBI_04_HEAT_SOUNDS")
			and prepare_music_event("fbi4_PARK_AMBULANCE_OS")
			and not is_any_text_being_displayed(locates_data)
				ACTIVATE_PHYSICS(rubbish_truck.veh)
				park_ambulance_status++
			endif 
			
		break 
			
		
		case 3
			
			if ambulance_cutscene()
			
				Set_Replay_Mid_Mission_Stage_With_Name(1, "Ram money truck")
			
				mission_flow = ram_army_truck
			
			endif 
							
		break 
		
	endswitch 
		
endproc 

proc reset_army_truck_cutscene()

	#IF IS_DEBUG_BUILD
		if widget_reset_cutscene

			mission_flow = ram_army_truck
			ram_army_truck_status = 1
			ram_army_truck_cutscene_status = 0 
			smashing_wall_rayfire_status = 0
		
			if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				DELETE_PED(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
			endif 
			
			if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_MICHAEL])
				DELETE_PED(selector_ped.pedID[SELECTOR_PED_MICHAEL])
			endif 
			
			if is_vehicle_driveable(tow_truck.veh)
				if is_playback_going_on_for_vehicle(tow_truck.veh)
					stop_playback_recorded_vehicle(tow_truck.veh)
				endif
			endif 
			
			if is_vehicle_driveable(army_truck.veh)
				if is_playback_going_on_for_vehicle(army_truck.veh)
					stop_playback_recorded_vehicle(army_truck.veh)
				endif 
			endif 
			
			request_vehicle_recording(003, "lkcountry")
			request_vehicle_recording(004, "lkcountry")
			
			load_all_objects_now()
			
			destroy_all_cams()
			
			if is_vehicle_driveable(tow_truck.veh)
				SET_ENTITY_COORDS(tow_truck.veh, <<926.7616, -2367.5168, 29.5308>>)
				SET_ENTITY_HEADING(tow_truck.veh, 83.00)
				SET_ENTITY_HEALTH(tow_truck.veh, 1000)
				set_vehicle_petrol_tank_health(tow_truck.veh, 1000)
				set_vehicle_engine_health(tow_truck.veh, 1000)
				set_vehicle_fixed(tow_truck.veh)
			endif 
			
			if is_vehicle_driveable(army_truck.veh)
				SET_ENTITY_COORDS(army_truck.veh, <<907.0347, -2350.1428, 29.5310>>)
				SET_ENTITY_HEADING(army_truck.veh, 174.0)
				SET_ENTITY_HEALTH(army_truck.veh, 1000)
				set_vehicle_petrol_tank_health(army_truck.veh, 1000)
				set_vehicle_engine_health(army_truck.veh, 1000)
				set_vehicle_fixed(army_truck.veh)
			endif 

			widget_reset_cutscene = false

		endif 
	#endif 
	
endproc 

bool go_to_main_cover_point = false
bool refresh_task = false
coverpoint_index michael_target_cover_point 

proc michael_ai_system()

	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
	
			switch michael_ai_system_status 
			
				case 0
				
					//set_ped_reset_flag(selector_ped.pedID[SELECTOR_PED_michael], PRF_ForceEnableFlashLightForAI, true)
				
					set_current_ped_weapon(selector_ped.pedID[SELECTOR_PED_michael], weapontype_combatmg, true)
					//set_current_ped_weapon(selector_ped.pedID[SELECTOR_PED_michael], weapontype_carbinerifle, true)
					
					open_sequence_task(seq)
						task_go_to_coord_while_aiming_at_coord(null, <<894.0912, -2349.3481, 29.4448>>, <<891.74, -2358.18, 30.71>>, 2.0, false, 0.5, 1.0, false, enav_default, true) 
						task_aim_gun_at_coord(null, <<891.74, -2358.18, 30.71>>, -1) 
					close_sequence_task(seq)
					task_perform_sequence(selector_ped.pedID[SELECTOR_PED_michael], seq)
					clear_sequence_task(seq)
					
					michael_ai_system_status++
				
				break 
				
				case 1
				
					//set_ped_reset_flag(selector_ped.pedID[SELECTOR_PED_michael], PRF_ForceEnableFlashLightForAI, true)
			
					if not IS_ENTITY_AT_COORD(selector_ped.pedID[SELECTOR_PED_michael], <<894.0912, -2349.3481, 29.4448>>, <<1.0, 1.0, 2.0>>, false, true) 

						if has_ped_task_finished_2(selector_ped.pedID[SELECTOR_PED_michael], script_task_perform_sequence, 1)
							
							set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_michael], true)
							clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_michael])
								
							open_sequence_task(seq)
								task_go_to_coord_while_aiming_at_coord(null, <<894.0912, -2349.3481, 29.4448>>, <<891.74, -2358.18, 30.71>>, 2.0, false, 0.5, 0.5) 
								task_aim_gun_at_coord(null, <<891.74, -2358.18, 30.71>>, -1) 
							close_sequence_task(seq)
							task_perform_sequence(selector_ped.pedID[SELECTOR_PED_michael], seq)
							clear_sequence_task(seq)
						
						endif 
					
					endif 
				
				break 
				
				case 2
				
					setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael]) 

					cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
					cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
				
					michael_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)

					task_synchronized_scene(selector_ped.pedID[selector_ped_michael], michael_cutscene_index, "missfbi4", "push_agents_player0", instant_blend_in, normal_blend_out, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)	
					
					if is_vehicle_driveable(army_truck.veh)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(michael_cutscene_index, army_truck.veh, 0)
					endif 
					
					force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
				
					michael_ai_system_status++
					
				break 
				
				case 3

					if IS_SYNCHRONIZED_SCENE_RUNNING(michael_cutscene_index)
	//					printstring("michael phase: ")
	//					printfloat(GET_SYNCHRONIZED_SCENE_PHASE(michael_cutscene_index))
	//					printnl()
	
						if does_entity_exist(documents.obj)
							
							if not is_entity_attached(documents.obj)
								
								if GET_SYNCHRONIZED_SCENE_PHASE(michael_cutscene_index) >= 0.483
									delete_object(documents.obj)
									set_model_as_no_longer_needed(documents.model)
								endif 

							endif 
							
						endif 
									
						
						
						if GET_SYNCHRONIZED_SCENE_PHASE(michael_cutscene_index) >= 0.6
						
							if GET_SYNCHRONIZED_SCENE_PHASE(michael_cutscene_index) >= 0.82
									
								if not is_ped_injured(army_truck_guy.ped) and not is_ped_injured(army_truck_guy.ped)
									create_conversation(scripted_speech[0], "HeatAud", "van_scene_3", CONV_PRIORITY_medium)
								endif 
											
								open_sequence_task(seq)
									task_aim_gun_at_coord(null, <<887.65, -2360.66, 30.85>>, 100, true)
									TASK_FOLLOW_WAYPOINT_RECORDING(null, "heat3", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_START_TASK_INITIALLY_AIMING) 
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[SELECTOR_PED_michael], seq)
								clear_sequence_task(seq)
								
								
								go_to_main_cover_point = true
								refresh_task = false
								michael_target_cover_point = michael_cover_point
	
								michael_ai_system_status++
								
							endif 
							
						endif 

					endif 
					
				break
				
				case 4
				
//					if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[SELECTOR_PED_michael])
//						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(selector_ped.pedID[SELECTOR_PED_michael], 2.0)
//						WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(selector_ped.pedID[SELECTOR_PED_michael], <<900.25, -2338.46, 30.96>>, false) 
//					endif 
//				
//					if (IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[selector_ped_michael]) and GET_PED_WAYPOINT_PROGRESS(selector_ped.pedID[selector_ped_michael]) > 13)
//				
					
					if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[SELECTOR_PED_michael])

						michael_ai_system_status++
						
					endif 

				break 

				case 5
				
					if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[SELECTOR_PED_michael])
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(selector_ped.pedID[SELECTOR_PED_michael], 2.0)
						WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(selector_ped.pedID[SELECTOR_PED_michael], <<900.25, -2338.46, 30.96>>, false) 
					endif 

//					vector target_waypoint_pos
//					//int waypoint_node_pos
//
//					waypoint_recording_get_coord("heat3", 27, target_waypoint_pos)
//
//					if is_entity_at_coord(selector_ped.pedID[selector_ped_michael], target_waypoint_pos, <<1.0, 1.0, 2.0>>)
//
//						if not IS_PLAYER_STEALING_PEDS_DESTINATION(selector_ped.pedID[SELECTOR_PED_michael], <<881.53, -2334.04, 33.91>>, 2.0)
//						
//							task_put_ped_directly_into_cover(selector_ped.pedID[SELECTOR_PED_michael], <<881.53, -2334.04, 33.91>>, -1, false, 0, false, false, michael_cover_point)
//							
//						else 
//						
//							//backup incase bellow secondary waypoint code fails
//							open_sequence_task(seq)
//								task_go_to_coord_while_aiming_at_coord(null, <<881.53, -2334.04, 33.91>>, <<900.25, -2338.46, 30.96>>, pedmove_run, false, 0.5, 0.5) 
//								task_aim_gun_at_coord(null, <<900.25, -2338.46, 30.96>>, -1) 
//							close_sequence_task(seq)
//							task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
//							clear_sequence_task(seq)
//
//						endif 
//
//						michael_ai_system_status++
//						
//					endif 
//					
//					
//					if (IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[selector_ped_michael]) and GET_PED_WAYPOINT_PROGRESS(selector_ped.pedID[selector_ped_michael]) > 13)
//						if IS_PLAYER_STEALING_PEDS_DESTINATION(selector_ped.pedID[SELECTOR_PED_michael], <<881.53, -2334.04, 33.91>>, 2.0)
//						or is_entity_in_angled_area(player_ped_id(), <<878.698, -2332.427, 31.493>>, <<878.277, -2336.807, 36.593>>, 7.0)
//					
//							open_sequence_task(seq)
//								task_follow_nav_mesh_to_coord(null, <<871.78998, -2344.44165, 29.33137>>, pedmove_run, -1)
//								task_put_ped_directly_into_cover(null, <<881.53, -2334.04, 33.91>>, -1, false, 0, false, false, michael_cover_point_2)
//							close_sequence_task(seq)
//							task_perform_sequence(selector_ped.pedID[selector_ped_michael], seq)
//							clear_sequence_task(seq)
//							
//							michael_run_to_pos = <<871.78998, -2344.44165, 29.33137>>
//							
//							michael_ai_system_status++
//					
//						endif 
//					endif 
					
					
					//-----------
					
					if not is_ped_in_cover(selector_ped.pedID[selector_ped_michael])
					
						if go_to_main_cover_point
						
							if (IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[selector_ped_michael]) and GET_PED_WAYPOINT_PROGRESS(selector_ped.pedID[selector_ped_michael]) > 13)
								if IS_PLAYER_STEALING_PEDS_DESTINATION(selector_ped.pedID[SELECTOR_PED_michael], <<881.53, -2334.04, 33.91>>, 2.0)
								or is_entity_in_angled_area(player_ped_id(), <<878.698, -2332.427, 31.493>>, <<878.277, -2336.807, 36.593>>, 7.0)
						
									michael_run_to_pos = <<871.78998, -2344.44165, 29.33137>>
									michael_target_cover_point = michael_cover_point_2
									refresh_task = true
									go_to_main_cover_point = false
									
								endif 
								
							endif 
							
						else 
						
							if IS_PLAYER_STEALING_PEDS_DESTINATION(selector_ped.pedID[SELECTOR_PED_michael], <<871.78998, -2344.44165, 29.33137>>, 2.0)
							
								michael_run_to_pos = <<881.53, -2334.04, 33.91>>
								michael_target_cover_point = michael_cover_point
								refresh_task = true
								go_to_main_cover_point = true
								
							endif 
						
						endif 
						
						if is_entity_at_coord(selector_ped.pedID[SELECTOR_PED_michael], michael_run_to_pos, <<2.0, 2.0, 2.0>>)

							IF (not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[SELECTOR_PED_michael])
							and NOT IS_PED_IN_COVER(selector_ped.pedID[SELECTOR_PED_michael])
							//if (not IS_PED_IN_COVER(selector_ped.pedID[SELECTOR_PED_michael])
							AND NOT IS_PED_GOING_INTO_COVER(selector_ped.pedID[SELECTOR_PED_michael])
							AND GET_SCRIPT_TASK_STATUS(selector_ped.pedID[SELECTOR_PED_michael], SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) != PERFORMING_TASK
							and GET_SCRIPT_TASK_STATUS(selector_ped.pedID[SELECTOR_PED_michael], SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT) != PERFORMING_TASK
							and GET_SCRIPT_TASK_STATUS(selector_ped.pedID[SELECTOR_PED_michael], SCRIPT_TASK_SEEK_COVER_TO_COORDS) != performing_task)
							or refresh_task
							
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(selector_ped.pedID[SELECTOR_PED_michael], TRUE)
						
								task_put_ped_directly_into_cover(selector_ped.pedID[SELECTOR_PED_michael], michael_run_to_pos, -1, false, 0, false, false, michael_target_cover_point)
								
								refresh_task = false

							endif 
							
						else 

							if (get_script_task_status(selector_ped.pedID[SELECTOR_PED_michael], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != performing_task
							and not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(selector_ped.pedID[selector_ped_michael]))
							or refresh_task
						
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(selector_ped.pedID[SELECTOR_PED_michael], TRUE)
								
								task_follow_nav_mesh_to_coord(selector_ped.pedID[SELECTOR_PED_michael], michael_run_to_pos, pedmove_run, -1, 0.2)
								
								refresh_task = false
								
								//script_assert("test 3")
							
							endif 
						
						endif 
						
					else 
					
						michael_ai_system_status++
					
					endif 
		
				break
		
				case 6
				
				
					if shootout_master_controler_status > 1

						//set_current_ped_weapon(selector_ped.pedID[selector_ped_michael], weapontype_combatmg, false)
						setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael])
						setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_michael])
						//SET_PED_COMBAT_ATTRIBUTES(selector_ped.pedID[SELECTOR_PED_michael], CA_USE_COVER, false)
						set_ped_sphere_defensive_area(selector_ped.pedID[SELECTOR_PED_michael], michael_run_to_pos, 1.5) //4.0
						task_combat_hated_targets_around_ped(selector_ped.pedID[SELECTOR_PED_michael], 200.00)
						
						michael_ai_system_status++
								
					endif 
				
				break 
				
				case 7
				
				break 
				
			endswitch 
			
		endif 
		
	endif 
	
endproc 


proc michael_defensive_sphere_system()

	if not player_in_cover_down_alley
		if police_man[12].created
			if is_entity_in_angled_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
				michael_defensive_sphere_system_status = 1
				player_in_cover_down_alley = true 
			endif
		endif 
	endif 
	
//	if shootout_ai_system >= 12
//	
//	elif shootout_ai_system >= 9
//	
//	elif shootout_ai_system >= 7
//	
//		if not player_in_cover_down_alley
//		
//			if is_entity_in_angled_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
//				
//				switch ePreviousSelectorPed
//				
//					case selector_ped_michael
//					
//						michael_defensive_sphere_system_status = 1
//						
//						michael_run_to_pos = <<845.3195, -2332.2019, 29.3346>>
//					
//					break 
//					
//					case selector_ped_franklin
//					
//					break 
//					
//				endswitch 
//				
//				player_in_cover_down_alley = true
//				
//			endif 
//			
//		endif 
//	
//	endif 

	switch michael_defensive_sphere_system_status
	
		case 0
			
//			open_sequence_task(seq)
//				task_set_blocking_of_non_temporary_events(null, false)
//				task_combat_hated_targets_around_ped(null, 200.00)
//			close_sequence_task(seq)
//			task_perform_sequence(selector_ped.pedID[selector_ped.ePreviousSelectorPed], seq)
//			clear_sequence_task(seq)
			
			set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], michael_run_to_pos, 1.5)
			task_combat_hated_targets_around_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 200.00)	
			SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 1500)
		
		break 
		
		case 1
		
			set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<845.3195, -2332.2019, 29.3346>>, 2.5) //2.2
			task_combat_hated_targets_around_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 200.00)
			SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 1500)
		
		break 
		
		case 2
		
		break 
	
	endswitch 

endproc 

proc franklin_defensive_sphere_system()

	if not player_in_cover_down_alley
		if police_man[12].created
			if is_entity_in_angled_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
				franklin_defensive_sphere_system_status = 1
				player_in_cover_down_alley = true 
			endif
		endif 
	endif 
	
	
	//second check

	switch franklin_defensive_sphere_system_status 
	
		case 0 

			set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<874.01642, -2353.89307, 29.33240>>, 2.5) //2.2
			task_combat_hated_targets_around_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 200.00)
			SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 1500)

		break 
		
		case 1

			set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<845.3195, -2332.2019, 29.3346>>, 2.5) //2.2
			task_combat_hated_targets_around_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 200.00)
			SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 1500)
		
		break 
		
	endswitch 

endproc 

func bool ram_army_truck_cutscene()
	float playback_time

	reset_army_truck_cutscene()
	
	if DOES_ENTITY_EXIST(army_truck.veh)
		if is_playback_going_on_for_vehicle(army_truck.veh)
			playback_time = get_time_position_in_recording(army_truck.veh)
//			printfloat(playback_time)
//			printnl()
			
		endif 
	endif 
	
	
	//	if is_playback_going_on_for_vehicle(tow_truck.veh)
//		printfloat(get_time_position_in_recording(tow_truck.veh))
//		printnl()
//	endif 
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2198877
	
	if IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		ram_army_truck_cutscene_status  = 22
	endif 
	
	smashing_wall_rayfire_system(playback_time)
	
	switch ram_army_truck_cutscene_status 
	
		case 0
		
			CLEAR_TRIGGERED_LABELS()
	
			remove_blip(army_truck.blip)
			
			REPLAY_RECORD_BACK_FOR_TIME(1)

			if IS_AUDIO_SCENE_ACTIVE("FBI_4_PICKUP_TRUCK_INT")
				STOP_AUDIO_SCENE("FBI_4_PICKUP_TRUCK_INT")
			endif 
			
			//************remove ************************
			//fix to stop cars from spawning in area where switch_roads_back_to_original() is broken
			set_vehicle_population_budget(0)
			//*******************************
			
			set_roads_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
			set_ped_paths_in_area(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>, false)
			SET_PED_NON_CREATION_AREA(<<770.00, -1982.81, 100.00>>, <<1045.00, -2500.00, -100.00>>)
			add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1372.22, -2752.3, -100.00>>)
		
			clear_area(<<906.246704, -2357.915283, 30.943356>>, 10000.00, true)
			
			camera_a = create_cam_with_params("default_scripted_camera", <<906.042358,-2377.925537,30.724129>>,<<0.229425,-1.864868,12.602599>>,39.641628)
			
			camera_b = create_cam_with_params("default_scripted_camera", <<907.677490,-2369.175049,31.802547>>, <<-5.724536,0.207416,57.186512>>, 37.0)
			camera_c = create_cam_with_params("default_scripted_camera", <<900.202881,-2367.950439,32.188667>>, <<-13.272571,0.458764,56.312191>>, 35.0)
			
			camera_d = create_cam_with_params("default_scripted_camera", <<881.209167,-2372.341309,34.582050>>,<<-14.494484,3.857126,-69.517838>>,27.690786)
			camera_e = create_cam_with_params("default_scripted_camera", <<879.636963,-2374.732666,30.373684>>,<<-2.835233,3.857126,-52.996078>>, 26.474293)
			
			camera_f = create_cam_with_params("default_scripted_camera", <<899.638123,-2368.813477,30.395008>>, <<5.178746,1.312154,55.968925>>, 50.00)

			camera_g = create_cam_with_params("default_scripted_camera", <<879.636963,-2374.732666,30.373684>>,<<-2.835233,3.857126,-52.996078>>, 25.8)
	
			
			SET_PED_USING_ACTION_MODE(player_ped_id(), true)
			
			setup_franklins_weapons(player_ped_id(), false) 
			
			if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB)
				give_weapon_to_ped(player_ped_id(), WEAPONTYPE_STICKYBOMB, 5, true)
			else 
				if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB) < 5
					set_ped_ammo(player_ped_id(), WEAPONTYPE_STICKYBOMB, 5)
				endif 
			endif 
			set_current_ped_weapon(player_ped_id(), weapontype_stickybomb, true)
			
			
			
			
			SET_PED_USING_ACTION_MODE(selector_ped.pedID[SELECTOR_PED_michael], true)
			//set_player_ped_mask(selector_ped.pedID[SELECTOR_PED_michael])
			setup_michaels_weapons(selector_ped.pedID[SELECTOR_PED_michael])
			setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael])
			setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_michael], true)
			//add_ped_for_dialogue(scripted_speech[0], 1, selector_ped.pedID[SELECTOR_PED_michael], "michael")
			michael_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_michael])
			set_blip_display(michael_blip, display_blip)
			set_blip_scale(michael_blip, 0.5)
			set_blocking_of_non_temporary_events(selector_ped.pedid[selector_ped_michael], true)
																					
			create_player_ped_on_foot(selector_ped.pedID[SELECTOR_PED_TREVOR], char_TREVOR, <<804.7164, -2330.2073, 61.0967>>, 264.4371, false)
			SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_TREVOR])
			SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
			setup_trevors_weapons(selector_ped.pedID[SELECTOR_PED_TREVOR])
			setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_trevor])
			setup_relationship_contact(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
			add_ped_for_dialogue(scripted_speech[0], 2, selector_ped.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			TREVOR_blip = create_blip_for_ped(selector_ped.pedID[SELECTOR_PED_TREVOR])
			set_blip_display(TREVOR_blip, display_blip)
			set_blip_scale(TREVOR_blip, 0.5)

			setup_vehicle_proofs(army_truck.veh)
			start_playback_recorded_vehicle(army_truck.veh, 003, "lkcountry")
			force_playback_recorded_vehicle_update(army_truck.veh)

			setup_vehicle_proofs(tow_truck.veh)
			SET_VEHICLE_CAN_DEFORM_WHEELS(tow_truck.veh, false)
			//START_PLAYBACK_RECORDED_VEHICLE_USING_AI(tow_truck.veh, 004, "lkcountry", 25, DF_ForceStraightLine|DF_UseShortCutLinks)
			start_playback_recorded_vehicle(tow_truck.veh, 004, "lkcountry")
			skip_time_in_playback_recorded_vehicle(tow_truck.veh, 8375)  //8975 8350
			force_playback_recorded_vehicle_update(tow_truck.veh)
			
			if is_vehicle_driveable(rubbish_truck.veh)
				SET_ENTITY_COORDS(rubbish_truck.veh, <<901.9453, -2383.3679, 29.2789>>)  
				SET_ENTITY_HEADING(rubbish_truck.veh, 341.1729)
				set_vehicle_on_ground_properly(rubbish_truck.veh)
			endif 

			set_time_scale(0.4)
			
//			PLAY_SOUND_FROM_ENTITY(-1,"RURAL_BANK_HIJACK_TRUCK_COLLISION", army_truck.veh)
//			PLAY_SOUND_FROM_ENTITY(-1,"RURAL_BANK_HIJACK_TRUCK_COLLISION_2", army_truck.veh)
			
			PLAY_STREAM_FRONTEND()

			ACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_FIB4_TRUCK_SMASH")
			
			clear_ped_tasks_immediately(selector_ped.pedID[SELECTOR_PED_michael])
			SET_ENTITY_COORDS(selector_ped.pedID[SELECTOR_PED_michael], <<907.14099, -2376.70972, 29.53554>>)//<<907.32880, -2377.14478, 29.52699>>)
			SET_ENTITY_HEADING(selector_ped.pedID[SELECTOR_PED_michael], 9.7089)
			michael_ai_system()
			force_ped_ai_and_animation_update(selector_ped.pedID[SELECTOR_PED_michael])
			
//			set_cam_active(camera_a, true)

			camera_anim = CREATE_CAMERA(camtype_animated, true)
			play_cam_anim(camera_anim, "fbi4_RAMCAM_CAM", "missfbi4", <<921.402, -2362.843, 29.479>>, <<0.0, 0.0, -2.25>>)

			render_script_cams(true, false)
			
			start_audio_scene("FBI_4_COLLISION_RAYFIRE")
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			original_time = get_game_timer()
			
			ram_army_truck_cutscene_status++
		
		break 
		
		case 1
		
			if playback_time > 80
				
				create_conversation(scripted_speech[0], "heataud", "ram_cut_1", CONV_PRIORITY_medium)
				
				START_PARTICLE_FX_NON_LOOPED_on_entity("scr_fbi4_trucks_crash", tow_truck.veh, <<0.0, 5.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
				trigger_music_event("fbi4_TRUCK_RAM_MA")

				ram_army_truck_cutscene_status++
			endif 
		
		break 
		
		case 2
		
			if playback_time > 600 //450
//			if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 1.0
		
				set_cam_active(camera_a, false)
//				set_cam_active(camera_anim, false)

				CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(true) 

				SET_VEHICLE_DAMAGE(tow_truck.veh, <<0.0, 6.5, 0.5>>, 375.00, 400.00, TRUE)
				SET_VEHICLE_DAMAGE(army_truck.veh, <<-2.0, -0.5, 0.5>>, 1000.00, 1000.00, TRUE)
				
				if not is_ped_injured(army_truck_driver.ped)
					task_play_anim_advanced(army_truck_driver.ped, "missheat", "Guard_Dead_DS", GET_ENTITY_COORDS(army_truck.veh), GET_ENTITY_ROTATION(army_truck.veh), instant_blend_in, instant_blend_out, -1, af_hold_last_frame) 
				endif 
				
				if not is_ped_injured(army_truck_passenger.ped)
					task_play_anim_advanced(army_truck_passenger.ped, "missheat", "Guard_Dead_PS", GET_ENTITY_COORDS(army_truck.veh), GET_ENTITY_ROTATION(army_truck.veh), instant_blend_in, instant_blend_out, -1, af_hold_last_frame) 
				endif 
				
				original_time = get_game_timer()
				
				set_time_scale(0.4)//0.6
				
//				set_cam_active(camera_d, true)
//				set_cam_active_with_interp(camera_e, camera_d, 5500, graph_type_decel)

				
				camera_anim = CREATE_CAMERA(camtype_animated, true)
				play_cam_anim(camera_anim, "fbi4_WALLCRUSH_CAM", "missfbi4", <<921.677, -2360.643, 28.779>>, <<0.0, 0.0, 0.25>>)

				set_entity_coords(selector_ped.pedID[SELECTOR_PED_michael], <<905.51105, -2373.91650, 29.53282>>, true, true, true)
		
				ram_army_truck_cutscene_status++
				
			endif 
			
		break 
		
		case 3
		
			printfloat(GET_CAM_ANIM_CURRENT_PHASE(camera_anim))
			printnl()
		
			if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 0.3
			
				//roof damage
				SET_VEHICLE_DAMAGE(army_truck.veh, <<0.0, -1.5, 2.0>>, 1000.00, 1000.00, TRUE)
				apply_money_truck_roof_damage = true 

				ram_army_truck_cutscene_status++

			endif 

		break 
		
		case 4
		
			if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) = cam_view_mode_first_person
			
				if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 0.97
				
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE) 
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					
					ram_army_truck_cutscene_status++

				endif 
			
			else 
			
				ram_army_truck_cutscene_status++
			
			endif 
		
		break 
		
		case 5
		
//			if not is_cam_interpolating(camera_e)
			if GET_CAM_ANIM_CURRENT_PHASE(camera_anim) >= 1.0
	
//				set_cam_active_with_interp(camera_g, camera_e, 7500, graph_type_linear)
//				shake_cam(camera_e, "HAND_SHAKE", 1.0)

				FREEZE_ENTITY_POSITION(army_truck.veh, true)
				SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(army_truck.veh, true)
				
				if is_playback_going_on_for_vehicle(army_truck.veh)
					//skip_to_end_and_stop_playback_recorded_vehicle(army_truck.veh)
					stop_Playback_recorded_vehicle(army_truck.veh)
				endif
			
				original_time = get_game_timer()
				
				ram_army_truck_cutscene_status++
				
			endif 
				
		break 

		case 6

			//if lk_timer(original_time, 700) 
			
				set_time_scale(1.0)
				
				DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_FIB4_TRUCK_SMASH")
				
				if is_playback_going_on_for_vehicle(tow_truck.veh)
					//skip_to_end_and_stop_playback_recorded_vehicle(tow_truck.veh)
					//force_playback_recorded_vehicle_update(tow_truck.veh)
					stop_playback_recorded_vehicle(tow_truck.veh)
				endif 
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tow_truck.veh)
				
				
				FREEZE_ENTITY_POSITION(army_truck.veh, true)
				SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(army_truck.veh, true)
				
				original_time = get_game_timer()
				
				ram_army_truck_cutscene_status++
				
			//endif 
			
		break
		
		case 7
		
			tow_truck.pos = get_entity_coords(tow_truck.veh)
			tow_truck.heading = get_entity_heading(tow_truck.veh)
			tow_truck.pos = <<896.0892, -2364.4160, 29.4761>>
			tow_truck.heading = 84.0784
			set_entity_coords(tow_truck.veh, tow_truck.pos)
			set_entity_heading(tow_truck.veh, tow_truck.heading)
			set_vehicle_on_ground_properly(tow_truck.veh)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tow_truck.veh)
		
			van_light.obj = create_object(van_light.model, van_light.pos)
			set_entity_rotation(van_light.obj, van_light.rot)
			freeze_entity_position(van_light.obj, true)
			
			CASCADE_SHADOWS_INIT_SESSION()
		
			create_conversation(scripted_speech[0], "heataud", "ram_cut_2", CONV_PRIORITY_medium)
		
			if not is_ped_injured(army_truck_driver.ped)
				//SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(army_truck_driver.ped, TRUE)
				SET_ENTITY_HEALTH(army_truck_driver.ped, 2)	
			endif 
			
			if not is_ped_injured(army_truck_passenger.ped)
				//SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(army_truck_driver.ped, TRUE)
				SET_ENTITY_HEALTH(army_truck_passenger.ped, 2)
			endif 
			
			SET_HORN_ENABLED(army_truck.veh, false) 

			STOP_AUDIO_SCENE("RURAL_BANK_HEIST_SETUP_HIJACK_SCENE")

			//ASSISTED_MOVEMENT_REQUEST_ROUTE("Heat1assists")
			
			//restore camera back to the stored cam view before the first person camera change
			//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_third_PERSON)//players_vehicle_cam_mode)
			SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(players_vehicle_cam_mode)
		
			end_cutscene_no_fade(false, true, false, -30.0, 0.0, 1500, false)
			
			set_player_control(player_id(), false)
			
			ram_army_truck_cutscene_status++

		break 
		
		case 8
		
			if not IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			
				stop_stream()
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_COLLISION_RAYFIRE")
					STOP_AUDIO_SCENE("FBI_4_COLLISION_RAYFIRE")
				endif 
			
				remove_ped_for_dialogue(scripted_speech[0], 3)
				remove_ped_for_dialogue(scripted_speech[0], 4)
				remove_ped_for_dialogue(scripted_speech[0], 5)
				remove_ped_for_dialogue(scripted_speech[0], 6)
				remove_ped_for_dialogue(scripted_speech[0], 7)

				FIX_VEHICLE_WINDOW(army_truck.veh, sc_window_rear_left)
				FIX_VEHICLE_WINDOW(army_truck.veh, sc_window_rear_right)

				set_vehicle_doors_locked(army_truck.veh, vehiclelock_locked)
				set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_locked)
				
				set_vehicle_doors_locked(tow_truck.veh, vehiclelock_locked)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(tow_truck.veh, false)
			
				set_player_control(player_id(), true)
				
				//deactivate_vehicle_proofs(tow_truck.veh)
				
				REPLAY_STOP_EVENT()
				
				return true
			
			endif 
		
		break 
		
		case 22
		
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(500)
				endif 
				
			else 
			
				stop_stream()

				if IS_AUDIO_SCENE_ACTIVE("FBI_4_COLLISION_RAYFIRE")
					STOP_AUDIO_SCENE("FBI_4_COLLISION_RAYFIRE")
				endif 
			
				DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_FIB4_TRUCK_SMASH")
			
				if not does_entity_exist(van_light.obj)
					van_light.obj = create_object(van_light.model, van_light.pos)
					set_entity_rotation(van_light.obj, van_light.rot)
					freeze_entity_position(van_light.obj, true)
				endif 
			
				//SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_DESTROYED)
				
				CASCADE_SHADOWS_INIT_SESSION()
				
				stop_stream()
			
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(smashing_wall)
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(smashing_wall, RFMO_STATE_ending)
					smashing_wall_rayfire_status = 22
				endif 

				set_time_scale(1.0)
				 
				remove_ped_for_dialogue(scripted_speech[0], 3)
				remove_ped_for_dialogue(scripted_speech[0], 4)
				remove_ped_for_dialogue(scripted_speech[0], 5)
				remove_ped_for_dialogue(scripted_speech[0], 6)
				remove_ped_for_dialogue(scripted_speech[0], 7)
				
				if not is_ped_injured(army_truck_driver.ped)
					SET_ENTITY_HEALTH(army_truck_driver.ped, 2)
				endif 
				
				if not is_ped_injured(army_truck_passenger.ped)
					SET_ENTITY_HEALTH(army_truck_passenger.ped, 2)
				endif 
				
				SET_HORN_ENABLED(army_truck.veh, false) 
				
				if not apply_money_truck_roof_damage
					SET_VEHICLE_DAMAGE(army_truck.veh, <<0.0, -1.5, 2.0>>, 1000.00, 1000.00, TRUE)
				 endif 
			
				if is_playback_going_on_for_vehicle(army_truck.veh)
					skip_to_end_and_stop_playback_recorded_vehicle(army_truck.veh)
				endif
				setup_vehicle_proofs(army_truck.veh)
				FREEZE_ENTITY_POSITION(army_truck.veh, true)
				SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(army_truck.veh, true)
				FIX_VEHICLE_WINDOW(army_truck.veh, sc_window_rear_left)
				FIX_VEHICLE_WINDOW(army_truck.veh, sc_window_rear_right)
				set_vehicle_doors_locked(army_truck.veh, vehiclelock_locked)
				
				
				if is_playback_going_on_for_vehicle(tow_truck.veh)
					skip_to_end_and_stop_playback_recorded_vehicle(tow_truck.veh)
				endif
				setup_vehicle_proofs(tow_truck.veh)
				set_vehicle_doors_locked(tow_truck.veh, vehiclelock_locked)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(tow_truck.veh, false)
				
				set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_locked)

				clear_ped_tasks_immediately(selector_ped.pedID[SELECTOR_PED_michael])
				SET_ENTITY_COORDS(selector_ped.pedID[SELECTOR_PED_michael], <<908.1069, -2371.9827, 29.5291>>)
				SET_ENTITY_HEADING(selector_ped.pedID[SELECTOR_PED_michael], 25.6402)
				michael_ai_system_status = 0
				michael_ai_system()	
		
				
				STOP_AUDIO_SCENE("RURAL_BANK_HEIST_SETUP_HIJACK_SCENE")
				stop_sound(rayfire_sound)
				
				//ASSISTED_MOVEMENT_REQUEST_ROUTE("Heat1assists")

				//deactivate_vehicle_proofs(tow_truck.veh)
				
				//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_third_PERSON)//players_vehicle_cam_mode)
				SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(players_vehicle_cam_mode)
				
				end_cutscene(false, true, -30.00, 0.0, false)
				
				REPLAY_STOP_EVENT()
				
				return true
				
			endif 
			
		break 
		
	endswitch 
	
	return false

endfunc 






proc ramming_truck_blipping_system()

	if does_blip_exist(army_truck.blip)
		
		if not is_ped_sitting_in_vehicle(player_ped_id(), tow_truck.veh)

			if is_this_print_being_displayed("cntry_god3")
				clear_this_print("cntry_god3")
			endif 
			
			remove_blip(army_truck.blip)
			
			tow_truck.blip = create_blip_for_vehicle(tow_truck.veh)
			
			if not has_label_been_triggered("heat_getout")
				if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_michael])) < 20
					if create_conversation(scripted_speech[0], "heataud", "heat_getout", CONV_PRIORITY_medium) 
						set_label_as_triggered("heat_getout", true)
					endif 
				endif 
			endif 
			
			if not is_bit_set(ramming_truck_blipping_system_bit_set, get_back_in_vehicle)
				if not is_any_text_being_displayed(locates_data)
					clear_prints()
					print_now("cntry_god15", default_god_text_time, 1)
					set_bit(ramming_truck_blipping_system_bit_set, get_back_in_vehicle)
				endif 
			endif 
		
		endif 
		
	else 
	
		if is_ped_sitting_in_vehicle(player_ped_id(), tow_truck.veh)
		
			if is_this_print_being_displayed("cntry_god15")
				clear_this_print("cntry_god15")
			endif 
			
			if does_blip_exist(tow_truck.blip)
				remove_blip(tow_truck.blip)
			endif 
			
			army_truck.blip = create_blip_for_vehicle(army_truck.veh, true)

		endif 

	endif 

endproc 

proc sound_army_truck_horn()

	if not sound_army_horn
		if get_distance_between_coords(get_entity_coords(tow_truck.veh),  get_entity_coords(army_truck.veh)) < 18.0
			play_sound_from_coord(-1, "Securicar_Horn", get_entity_coords(army_truck.veh), "FBI_04_HEAT_SOUNDS")
			sound_army_horn = true 
		endif
	endif 

	if lk_timer(horn_time, 4000)
		start_vehicle_horn(army_truck.veh, 2500)
		horn_time = get_game_timer()
	endif 
	
endproc 

///PURPOSE: tracks the current follow vehicle cam veiw mode during the ram truck stage. If it is different from
///    		CAM_VIEW_MODE_FIRST_PERSON which was set during the ambulance_cutscene() then it updates it.     		
proc track_current_follow_vehicle_cam_view_mode()

	cam_view_mode current_vehicle_cam_mode
	
	current_vehicle_cam_mode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
	
	if (current_vehicle_cam_mode != CAM_VIEW_MODE_FIRST_PERSON)
		players_vehicle_cam_mode = current_vehicle_cam_mode
	endif 
	
endproc 

proc fbi4_ram_army_truck()
	
	IF RAM_ARMY_TRUCK_STATUS < 1
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2198877
	ENDIF
	
	switch ram_army_truck_status 
	
		case 0
			
			sound_army_truck_horn()

			smashing_wall_rayfire_system()
		
			ramming_truck_blipping_system()
			
			if not IS_MUSIC_ONESHOT_PLAYING()
				prepare_music_event("FBI4_TRUCK_RAM_MA")
			endif 
			
			track_current_follow_vehicle_cam_view_mode()
			
			if does_blip_exist(army_truck.blip)
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<907.426, -2371.301, 29.550>>, <<908.093, -2362.426, 33.550>>, 5.8)  
					if (GET_ENTITY_HEADING(tow_truck.veh) > 45 and GET_ENTITY_HEADING(tow_truck.veh) < 135)
						if IS_ENTITY_TOUCHING_ENTITY(tow_truck.veh, army_truck.veh)
							if get_entity_speed(tow_truck.veh) >= 5.0
								if start_new_cutscene_no_fade()

									ram_army_truck_cutscene()

									ram_army_truck_status++
									
								endif 
							endif 
						endif 
					endif 
				endif 
			endif 

		break 
		
		case 1
		
			if ram_army_truck_cutscene()

				//SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), false)
				
				plant_bomb_time = get_game_timer()

				bomb_blip = create_blip_for_coord(<<889.7667, -2361.8057, 29.2762>>)
				set_blip_as_friendly(bomb_blip, true)
				SET_BLIP_NAME_FROM_TEXT_FILE(bomb_blip, "cntry_god21")
				
				help_text_rendered = false
				
				mission_flow = blow_open_truck_doors
			endif 
			
		break 
		
		case 2
		
		break 
		
	endswitch 

endproc  

func bool reposition_player_after_explosion_cutscene()
			
	if not is_entity_in_angled_area(player_ped_id(), <<897.491, -2352.488, 29.431>>, <<860.245, -2349.099, 32.331>>, 28.2)    
		return true 
	endif 
	
	if is_entity_in_angled_area(player_ped_id(), <<897.491, -2352.488, 29.431>>, <<860.245, -2349.099, 32.331>>, 28.2)    
		if not is_entity_in_angled_area(player_ped_id(), <<897.957, -2352.269, 29.358>>, <<884.508, -2351.092, 33.658>>, 12.300) 
			if not is_ped_in_cover(player_ped_id())

				return true 
				
			endif 
		endif 
	endif 
	
	///route michael runs into van in
	//if is_entity_in_angled_area(player_ped_id(), <<893.341, -2355.797, 29.358>>, <<890.660, -2355.472, 33.658>>, 13.200)
	if is_entity_in_angled_area(player_ped_id(), <<894.352, -2355.901, 29.401>>, <<889.687, -2355.331, 33.641>>, 13.4)//data -  player pos = <<892.0082, -2352.7954, 29.3974>>, 173.1000  offsets = -2.700, 2.8, -1.0    2.0, 2.8, 3.24,    13.4
		return true
	endif 
	
	return false 
	
endfunc 

func bool c4_explosion_cutscene()
				
	switch c4_explosion_cutscene_status 
	
		case 0
		
			if does_entity_exist(army_truck_guy.ped)
				delete_ped(army_truck_guy.ped)
			endif 
			set_model_as_no_longer_needed(army_truck_guy.model)
			
			if does_entity_exist(army_truck_passenger.ped)
				delete_ped(army_truck_passenger.ped)
			endif 
			set_model_as_no_longer_needed(army_truck_passenger.model)

			if is_vehicle_driveable(rubbish_truck.veh)
				set_entity_proofs(rubbish_truck.veh, true, true, true, true, true)
			endif 
		
			clear_area(<<889.7411, -2345.4558, 29.3307>>, 1000.00, true)
		
			clear_player_wanted_level(player_id())
			//g_allowmaxwantedlevelcheck = false
			set_max_wanted_level(0)
			set_create_random_cops(false)
			
	
			if not is_ped_in_cover(player_ped_id())
			
				clear_ped_tasks_immediately(player_ped_id())
				set_entity_coords(player_ped_id(), get_entity_coords(player_ped_id()))
				
				//forces the player to face the army truck explosion.
				vector vec_ba 
				vec_ba = get_entity_coords(army_truck.veh) - get_entity_coords(player_ped_id())
				set_entity_heading(player_ped_id(), GET_HEADING_FROM_VECTOR_2D(vec_ba.x, vec_ba.y))
				
				//script_assert("test")
	
			endif 
			
			if reposition_player_after_explosion_cutscene()
				
				clear_ped_tasks_immediately(player_ped_id())
				set_entity_coords(player_ped_id(), <<890.4393, -2346.4761, 29.3416>>)
				set_entity_heading(player_ped_id(), 184.3769) 
				
			endif 
				
			if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB)
				give_weapon_to_ped(player_ped_id(), WEAPONTYPE_STICKYBOMB, 3, false, false) 
			else 
				if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB) < 3
					set_ped_ammo(player_ped_id(), WEAPONTYPE_STICKYBOMB, 3)
				endif 
			endif 
//			if has_ped_got_weapon(player_ped_id(), weapontype_combatmg)
//				set_current_ped_weapon(player_ped_id(), weapontype_combatmg , false)
//			endif
			
//			EQUIP_BEST_PLAYER_WEAPON(player_ped_id(), true)
//			if not HAS_PED_GOT_FIREARM(player_ped_id())
//				setup_franklins_weapons(player_ped_id(), false)
//			endif 
	
			set_current_ped_weapon(player_ped_id(), weapontype_carbinerifle, true)

			if is_vehicle_driveable(rubbish_truck.veh)
				SET_ENTITY_COORDS(rubbish_truck.veh, <<901.9453, -2383.3679, 29.2789>>)  
				SET_ENTITY_HEADING(rubbish_truck.veh, 341.1729)
				set_vehicle_on_ground_properly(rubbish_truck.veh)
			endif 
			
			if is_vehicle_driveable(tow_truck.veh)
				set_entity_coords(tow_truck.veh, <<898.3076, -2364.3623, 29.4919>>)
				set_entity_heading(tow_truck.veh, 87.4580)
				set_vehicle_on_ground_properly(tow_truck.veh)
			endif 
		
			set_vehicle_door_broken(army_truck.veh, sc_door_rear_left, false)
			set_vehicle_door_broken(army_truck.veh, sc_door_rear_right, false)

			ADD_EXPLOSION_WITH_USER_VFX(<<889.7, -2362.58, 30.24>>, exp_tag_grenade, get_hash_key("EXP_VFXTAG_FBI4_TRUCK_DOORS"), 0.5)

			play_sound_frontend(explosion_sound, "FBI_04_HEAT_C4_DOORS") 

			DELETE_OBJECT(c4.obj)
			
			camera_a = create_cam_with_params("default_scripted_camera", <<888.935059,-2355.473145,29.955381>>, <<4.753477,-1.775736,-163.737991>>, 36.009514)
			
			set_cam_active(camera_a, true)
			SHAKE_CAM(camera_a, "SMALL_EXPLOSION_SHAKE", 0.5)
			render_script_cams(true, false)
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
			trigger_music_event("fbi4_EXPLODE_MA")
			
			start_audio_scene("FBI_4_EXPLOSION_RAYFIRE")
			
			original_time = get_game_timer()
			
			c4_explosion_cutscene_status++
		
		break 
		
		case 1
		
			if lk_timer(original_time, 1100)

				if IS_AUDIO_SCENE_ACTIVE("FBI_4_EXPLOSION_RAYFIRE")
					STOP_AUDIO_SCENE("FBI_4_EXPLOSION_RAYFIRE")
				endif 

				task_play_anim(player_ped_id(), "missfbi4", "REACT_Explosion", instant_blend_in, slow_blend_out, -1, AF_FORCE_START, 0.2)
				force_ped_ai_and_animation_update(player_ped_id())
				
				start_audio_scene("FBI_4_PREP_FOR_COPS")
				
				REPLAY_STOP_EVENT()

				end_cutscene_no_fade(false, true, false, 0.0, 0.0, 3000, false, false)

				return true
				
			endif 
		
		break 
		
		case 2
		
		break 
		
		case 22
		
		break 
		
	endswitch 

	return false 
	
endfunc 

proc army_truck_guy_ai_system()
	
	if not is_ped_injured(army_truck_guy.ped)
	
//		printvector(get_entity_coords(army_truck_guy.ped))
//		printnl()

		switch army_truck_guy_ai_status 
		
			case 0
			
				if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
					if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.28
					
						SET_ENTITY_PROOFS(army_truck_guy.ped, false, false, false, false, false)
						SET_ENTITY_COLLISION(army_truck_guy.ped, true)
						//set_ped_can_ragdoll(army_truck_guy.ped, true) 
					endif 
				endif 
				
				
				if has_ped_been_harmed(army_truck_guy.ped, army_truck_guy.health, army_truck_guy.created) 

					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null, <<898.86, -2361.77, 29.37>>, pedmove_run, -1, -1, enav_no_stopping)
						task_follow_nav_mesh_to_coord(null, <<908.22, -2370.33, 29.55>>, pedmove_run, -1, -1, enav_no_stopping)
						task_smart_flee_ped(null, player_ped_id(), 500, -1) 
					close_sequence_task(seq)
					task_perform_sequence(army_truck_guy.ped, seq)
					clear_sequence_task(seq)
					
					army_truck_guy_ai_status = 1
						
				else 
				
//					if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
//						if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 0.38
//
//							open_sequence_task(seq)
//								task_follow_nav_mesh_to_coord(null, <<887.4342, -2360.4651, 29.2382>>, pedmove_walk, -1)
//								TASK_TURN_PED_TO_FACE_ENTITY(null, player_ped_id())
//								task_play_anim(null, "missheat", "Dazed_Guard_Loop", normal_blend_in, normal_blend_out, -1, af_looping)
//							close_sequence_task(seq)
//							task_perform_sequence(army_truck_guy.ped, seq)
//							clear_sequence_task(seq)
//							
//							army_truck_guy_ai_status++
//							
//						endif 
//						
//					else 
//					
//						army_truck_guy_ai_status++
					
					if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index)
//						printstring("army guy 0: ")
//						printfloat(GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index))
//						printnl()
						if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index) >= 1.0

							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<898.86, -2361.77, 29.37>>, pedmove_run, -1, -1, enav_no_stopping)
								task_follow_nav_mesh_to_coord(null, <<908.22, -2370.33, 29.55>>, pedmove_run, -1, -1, enav_no_stopping)
								task_smart_flee_ped(null, player_ped_id(), 500, -1) 
							close_sequence_task(seq)
							task_perform_sequence(army_truck_guy.ped, seq)
							clear_sequence_task(seq)

							army_truck_guy_ai_status = 1
							
						endif 
						
					else 
					
						army_truck_guy_ai_status = 1
					
					endif 
				
				
				endif 

			break 
			
			case 1
			
				SET_PED_CONFIG_FLAG(army_truck_guy.ped, PCF_DisableExplosionReactions, false) 
					
				SET_PED_AS_NO_LONGER_NEEDED(army_truck_guy.ped)
				set_model_as_no_longer_needed(army_truck_guy.model)
				remove_ped_for_dialogue(scripted_speech[0], 6)
				
				army_truck_guy_ai_status++

			break 
			
			case 2
			
			break 
			
		endswitch 
		
	else 
	
		if does_entity_exist(army_truck_guy.ped)
			SET_PED_AS_NO_LONGER_NEEDED(army_truck_guy.ped)
			set_model_as_no_longer_needed(army_truck_guy.model)
			remove_ped_for_dialogue(scripted_speech[0], 6)
		endif 
	
		if not army_truck_guy.created
	
			army_truck_guy.ped = create_ped(pedtype_mission, army_truck_guy.model, army_truck_guy.pos,  army_truck_guy.heading)
			//SET_ENTITY_COLLISION(army_truck_guy.ped, false)
			//set_ped_can_ragdoll(army_truck_guy.ped, false) 
			//SET_ENTITY_PROOFS(army_truck_guy.ped, true, true, true, true, true)
			set_ped_random_component_variation(army_truck_guy.ped)
			SET_RAGDOLL_BLOCKING_FLAGS(army_truck_guy.ped, RBF_VEHICLE_IMPACT | RBF_FIRE | RBF_PLAYER_IMPACT | RBF_EXPLOSION)
			SET_PED_CONFIG_FLAG(army_truck_guy.ped, PCF_DisableExplosionReactions, true) 
			set_ped_dies_when_injured(army_truck_guy.ped, true)
			set_ped_keep_task(army_truck_guy.ped, true)
			set_blocking_of_non_temporary_events(army_truck_guy.ped, true)
			add_ped_for_dialogue(scripted_speech[0], 6, army_truck_guy.ped, "AGENT1")
			set_ped_name_debug(army_truck_guy.ped, army_truck_guy.name)
			army_truck_guy.created = true
			
			cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
			cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
		
			cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)

			task_synchronized_scene(army_truck_guy.ped, cutscene_index, "missfbi4", "push_agents_agent1", instant_blend_in, slow_blend_out, SYNCED_SCENE_USE_PHYSICS)	
			if is_vehicle_driveable(army_truck.veh)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(cutscene_index, army_truck.veh, 0)
			endif
			
			force_ped_ai_and_animation_update(army_truck_guy.ped)
		
		endif 

	endif 

endproc 

proc army_truck_guy_2_ai_system()
	
	if not is_ped_injured(army_truck_guy_2.ped)
	
		switch army_truck_guy_ai_2_status 
		
			case 0
			
				if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index_2)

					if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index_2) >= 0.25
					
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(army_truck_guy_2.ped)
						set_entity_health(army_truck_guy_2.ped, 200)
						set_ped_max_health(army_truck_guy_2.ped, 200)
						clear_ragdoll_blocking_flags(army_truck_guy_2.ped, RBF_BULLET_IMPACT)
						SET_ENTITY_PROOFS(army_truck_guy_2.ped, false, false, false, false, false)
						SET_ENTITY_COLLISION(army_truck_guy_2.ped, true)

						army_truck_guy_ai_2_status = 1
						
					else 
						
						if get_entity_health(army_truck_guy_2.ped) <= 9900
							SET_ENTITY_PROOFS(army_truck_guy_2.ped, true, true, true, true, true)
							army_truck_guy_ai_2_status = 3
						endif 
						
					endif 
					
				endif 
			
			break 
			
			case 1
			
				if has_ped_been_harmed(army_truck_guy_2.ped, army_truck_guy_2.health, army_truck_guy_2.created) //or shootout_master_controler_status > 0
					if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index_2)
						if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index_2) >= 0.25

							//clear_ped_tasks(army_truck_guy_2.ped)

							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<898.86, -2361.77, 29.37>>, pedmove_run, -1, -1, enav_no_stopping)
								task_follow_nav_mesh_to_coord(null, <<908.22, -2370.33, 29.55>>, pedmove_run, -1, -1, enav_no_stopping)
								task_smart_flee_ped(null, player_ped_id(), 500, -1) 
							close_sequence_task(seq)
							task_perform_sequence(army_truck_guy_2.ped, seq)
							clear_sequence_task(seq)

							army_truck_guy_ai_2_status++
							
						endif 
						
					endif 
						
				else 
				
					if IS_SYNCHRONIZED_SCENE_RUNNING(cutscene_index_2)

						if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index_2) >= 0.655
							
							open_sequence_task(seq)
								task_follow_nav_mesh_to_coord(null, <<898.86, -2361.77, 29.37>>, pedmove_run, -1, -1, enav_no_stopping)
								task_follow_nav_mesh_to_coord(null, <<908.22, -2370.33, 29.55>>, pedmove_run, -1, -1, enav_no_stopping)
								task_smart_flee_ped(null, player_ped_id(), 500, -1) 
							close_sequence_task(seq)
							task_perform_sequence(army_truck_guy_2.ped, seq)
							clear_sequence_task(seq)
							
							army_truck_guy_ai_2_status++
							
						endif 
						
					else 
					
						army_truck_guy_ai_2_status++
					
					endif 
				
				endif 
			
			break 
			
			case 2
			
				SET_PED_CONFIG_FLAG(army_truck_guy_2.ped, PCF_DisableExplosionReactions, false) 

				SET_PED_AS_NO_LONGER_NEEDED(army_truck_guy_2.ped)
				set_model_as_no_longer_needed(army_truck_guy_2.model)
			
				army_truck_guy_ai_2_status++

			break 
			
			case 3
			
				if GET_SYNCHRONIZED_SCENE_PHASE(cutscene_index_2) >= 0.262
					SET_PED_TO_RAGDOLL(army_truck_guy_2.ped, 2000, 3000, TASK_RELAX)
					clear_ped_tasks(army_truck_guy_2.ped)
					set_entity_health(army_truck_guy_2.ped, 2)
				endif 
					
			break 
			
		endswitch 

	else 
	
		if does_entity_exist(army_truck_guy_2.ped)
			SET_PED_AS_NO_LONGER_NEEDED(army_truck_guy_2.ped)
			set_model_as_no_longer_needed(army_truck_guy_2.model)
		endif 

		if not army_truck_guy_2.created
	
			army_truck_guy_2.ped = create_ped(pedtype_mission, army_truck_guy_2.model, army_truck_guy_2.pos, army_truck_guy_2.heading)
			army_truck_guy_2.created = true
			set_ped_random_component_variation(army_truck_guy_2.ped)
			set_ped_suffers_critical_hits(army_truck_guy_2.ped, false)
			set_entity_health(army_truck_guy_2.ped, 10000)
			set_ped_max_health(army_truck_guy_2.ped, 10000)
			SET_ENTITY_PROOFS(army_truck_guy_2.ped, false, true, true, true, true)
			SET_RAGDOLL_BLOCKING_FLAGS(army_truck_guy_2.ped, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_FIRE | RBF_PLAYER_IMPACT | RBF_EXPLOSION)
			SET_PED_CONFIG_FLAG(army_truck_guy_2.ped, PCF_DisableExplosionReactions, true) 
			set_ped_dies_when_injured(army_truck_guy_2.ped, true)
			set_ped_keep_task(army_truck_guy_2.ped, true)
			set_blocking_of_non_temporary_events(army_truck_guy_2.ped, true)
			add_ped_for_dialogue(scripted_speech[0], 7, army_truck_guy_2.ped, "AGENT2")
			set_ped_name_debug(army_truck_guy_2.ped, army_truck_guy_2.name)
			
			cutscene_pos = <<0.0, 0.0, 0.0>> //GET_ENTITY_COORDS(army_truck.veh)
			cutscene_rot = <<0.0, 0.0, 0.0>> //GET_ENTITY_ROTATION(army_truck.veh)
		
			cutscene_index_2 = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)

			task_synchronized_scene(army_truck_guy_2.ped, cutscene_index_2, "missfbi4", "push_agents_agent2", instant_blend_in, slow_blend_out, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)	
			if is_vehicle_driveable(army_truck.veh)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(cutscene_index_2, army_truck.veh, 0)
			endif 
			
			force_ped_ai_and_animation_update(army_truck_guy_2.ped)
				
		endif 
	endif 

endproc 

proc blow_open_truck_doors_dialogue_system()
	
	if not is_any_text_being_displayed(locates_data)
	
		switch blow_open_truck_doors_dialogue_status 
		
			case 0
			
				if create_conversation(scripted_speech[0], "heataud", "heat_plant", CONV_PRIORITY_medium)//heat_c4_0
					blow_open_truck_doors_dialogue_status++
				endif 
			
			break 
			
			case 1

				if create_conversation(scripted_speech[0], "heataud", "heat_c4_1", CONV_PRIORITY_medium)
					blow_open_truck_doors_dialogue_status++
				endif 	
				
			break 
			
			case 2
			
				if not is_any_text_being_displayed(locates_data)
					
					if does_blip_exist(bomb_blip)
						dialogue_time = get_game_timer() 
						print_now("cntry_god7", default_god_text_time, 1)
					endif 
					
					blow_open_truck_doors_dialogue_status++
					
				endif 
			break 

			case 3
			
				if not does_blip_exist(bomb_blip)
					if (get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), <<891.7935, -2357.5984, 29.3537>>) < 50.0) 
						if create_conversation(scripted_speech[0], "HeatAud", "fbi4_c4_0", CONV_PRIORITY_medium)
							blow_open_truck_doors_dialogue_status++
						endif 
					endif 
				
				else 
				
					if lk_timer(dialogue_time, 15000)
					
						if create_conversation(scripted_speech[0], "HeatAud", "heat_remind", CONV_PRIORITY_medium)
							blow_open_truck_doors_dialogue_status++
						endif 
					
					endif 

				endif 
			
			break 
			
		endswitch
		
	endif 
	
endproc

proc area_system()

	if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<787.998, -2336.441, 29.2>>, <<933.748, -2351.246, 80.350>>, 126.300) //increase area width to incrase left alley box   
	
		player_in_area = true 
		
		if outside_area_text_printed
			if is_this_print_being_displayed(warning_string)
				clear_this_print(warning_string)
				outside_area_text_printed = false
			endif 
		endif 
		
		area_time = get_game_timer()	
		
	else 
		
		player_in_area = false
		
		if not outside_area_text_printed
		
			switch get_current_player_ped_enum()
			
				case char_michael
					
					print_now("cntry_area1", default_god_text_time, 1)
					warning_string = "cntry_area1"

				break 
				
				case char_franklin
				
					print_now("cntry_area2", default_god_text_time, 1)
					warning_string = "cntry_area2"
				
				break 
				
				case char_trevor
				
					print_now("cntry_area0", default_god_text_time, 1)
					warning_string = "cntry_area0"
				
				break 
			
			endswitch 

			outside_area_text_printed = true
		endif 
		
		
		if lk_timer(area_time, 10000)
			if not scamdetails.bSplineActive
			
				switch get_current_player_ped_enum()
				
					case char_michael
						
						mission_failed_text = "cntry_fail10"
						
					break 
					
					case char_franklin
					
						mission_failed_text =  "cntry_fail11"
					
					break 
					
					case char_trevor
					
						mission_failed_text = "cntry_fail9"
					
					break 
				
				endswitch 

				area_fail = true 
				
			endif 
		endif 
	
	endif 

endproc 

func bool c4_cutscene_ready_to_trigger()
				
	if not c4_planted_manually
		
		if IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<891.034, -2362.032, 29.282>>, <<888.756, -2361.720, 31.622>>, 1.1, weapontype_stickybomb)
		or is_projectile_type_in_angled_area(<<891.034, -2362.032, 29.282>>, <<888.756, -2361.720, 31.622>>, 1.1, WEAPONTYPE_DLC_PROXMINE)
		or (does_entity_exist(c4.obj) and is_entity_in_angled_area(c4.obj, <<891.202, -2361.976, 28.324>>, <<888.773, -2361.656, 31.824>>, 1.0))
			c4_planted_manually = true 
		endif 

	
	else 
		
		if not does_entity_exist(c4.obj)

			if is_control_pressed(player_control, INPUT_DETONATE) 
			or IS_EXPLOSION_IN_ANGLED_AREA(exp_tag_stickybomb, <<891.202, -2361.976, 28.324>>, <<888.773, -2361.656, 31.824>>, 1.0)
			or IS_EXPLOSION_IN_ANGLED_AREA(exp_tag_proxmine, <<891.202, -2361.976, 28.324>>, <<888.773, -2361.656, 31.824>>, 1.0)
				return true 	
			endif 
			
		else 
			
			if is_control_pressed(player_control, INPUT_DETONATE) 
		 	or IS_EXPLOSION_IN_ANGLED_AREA(exp_tag_stickybomb, <<891.202, -2361.976, 28.324>>, <<888.773, -2361.656, 31.824>>, 1.0)
			or IS_EXPLOSION_IN_ANGLED_AREA(exp_tag_proxmine, <<891.202, -2361.976, 28.324>>, <<888.773, -2361.656, 31.824>>, 1.0)
			or get_entity_health(c4.obj) < 990
			
				return true 
				
			endif 
		endif 
		
	endif 
	
	return false 
	
endfunc 

///PURPOSE: removes army blip if the player goes out of the area. Uses the function area_system(). 
proc army_truck_bliping_system()

	if not player_in_area
		
		if does_blip_exist(bomb_blip)
			remove_blip(bomb_blip)
		endif 
		
	else 
	
		switch blow_open_truck_doors_status
		
			case 0
			case 1
		
				if not does_blip_exist(bomb_blip)
				
					bomb_blip = create_blip_for_coord(<<889.7667, -2361.8057, 29.2762>>)
					set_blip_as_friendly(bomb_blip, true)
					SET_BLIP_NAME_FROM_TEXT_FILE(bomb_blip, "cntry_god21")
					
				endif 
				
			break 
			
			case 2
			
			break 
			
		endswitch 
		
	endif 
	
endproc 

proc blow_open_struck_doors_audio_scene_system()
	
	switch blow_open_struck_doors_audio_scene_system_status 
	
		case 0
		
			if is_vehicle_driveable(tow_truck.veh)
				if not is_ped_sitting_in_vehicle(player_ped_id(), tow_truck.veh)

					start_audio_scene("FBI_4_RIG_EXPLOSIVES")
				
					blow_open_struck_doors_audio_scene_system_status++
				endif 
				
			endif 
			
		break 
		
		case 1
		
		break 
		
	endswitch 
	
endproc 


proc fbi4_blow_open_truck_doors()

//	if not DOES_ENTITY_EXIST(c4.obj)
//		c4.obj = create_object(c4.model, c4.pos)
//		ATTACH_ENTITY_TO_ENTITY(c4.obj, player_ped_id(), bonetag_ph_r_hand, <<0.08, 0.01, -0.09>>, <<258.95, 172.00, -18.00>>)
//	endif 

//	if is_vehicle_driveable(army_truck.veh)
//		printvector(GET_ENTITY_COORDS(army_truck.veh))
//		printnl()
//		printvector(GET_ENTITY_ROTATION(army_truck.veh))
//		printnl()
//	endif 

	reset_army_truck_cutscene()

	michael_ai_system()
	
	blow_open_truck_doors_dialogue_system()
	
	area_system()
	
	army_truck_bliping_system()
	
	blow_open_struck_doors_audio_scene_system()

	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2228115

	switch blow_open_truck_doors_status
	
		case 0
		
			request_model(documents.model)
		
			//temp guy created inside truck for audio as the player approaches the truck
			army_truck_guy.ped = create_ped(pedtype_mission, army_truck_guy.model, army_truck_guy.pos,  army_truck_guy.heading)
			SET_ENTITY_COLLISION(army_truck_guy.ped, false)
			freeze_entity_position(army_truck_guy.ped, true) 
			set_entity_visible(army_truck_guy.ped, false)
			set_blocking_of_non_temporary_events(army_truck_guy.ped, true)
			add_ped_for_dialogue(scripted_speech[0], 6, army_truck_guy.ped, "AGENT1")
			set_ped_name_debug(army_truck_guy.ped, army_truck_guy.name)
			
			blow_open_truck_doors_status++
		
		break 
	
		case 1

			if IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<891.034, -2362.032, 29.282>>, <<888.756, -2361.720, 31.622>>, 1.1, weapontype_stickybomb)
			or is_projectile_type_in_angled_area(<<891.034, -2362.032, 29.282>>, <<888.756, -2361.720, 31.622>>, 1.1, WEAPONTYPE_DLC_PROXMINE)
				
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
				trigger_music_event("fbi4_PLANT_BOMB_MA")
				
				ASSISTED_MOVEMENT_REMOVE_ROUTE("Heat1assists")

				if does_blip_exist(bomb_blip)
					remove_blip(bomb_blip)
				endif 

				clear_this_print("cntry_god7")
				
				SET_PED_CAN_SWITCH_WEAPON(player_ped_id(), true)
				
				//create_conversation(scripted_speech[0], "HeatAud", "fbi4_c4_0", CONV_PRIORITY_medium)
				
//				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
//				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(scripted_speech[0], "heataud", "amb_cut_1b", CONV_PRIORITY_MEDIUM)

				help_text_rendered = false
				
				dialogue_time = get_game_timer()

				c4_planted_manually = true 
				
				Set_Replay_Mid_Mission_Stage_With_Name(2, "Detonate C4")
				
				blow_open_truck_doors_status++
				
			endif 
			
		break 
		
		case 2

			if not has_label_been_triggered("cntry_god8")
				if not is_any_text_being_displayed(locates_data)
					print_now("cntry_god8", 5000, 1)
					set_label_as_triggered("cntry_god8", true)
				endif 
			endif 

			if not help_text_rendered
				if (get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), <<891.7935, -2357.5984, 29.3537>>) > 6.0)  
					
					clear_prints()

					print_help("cntry_help3")
					
					original_time = get_game_timer()
					
					help_text_rendered = true 
				endif 
			
			else 
			
				if (get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), <<891.7935, -2357.5984, 29.3537>>) < 50.0) 
			
					if not is_any_text_being_displayed(locates_data)
						if not has_label_been_triggered("heat_c4_3")
							if create_conversation(scripted_speech[0], "HeatAud", "heat_c4_3", CONV_PRIORITY_medium) 
								set_label_as_triggered("heat_c4_3", true)
							endif 

						else 
					
							if lk_timer(original_time, 10000)
								if create_conversation(scripted_speech[0], "HeatAud", "heat_c4_4", CONV_PRIORITY_medium) 
									original_time = get_game_timer()
								endif 
							endif 
							
						endif 
					endif 
					
				endif 
			
			endif 
			
			//player too far away from c4
			if not has_label_been_triggered("heat_c4_5")
				if (get_distance_between_coords(GET_ENTITY_COORDS(player_ped_id()), <<891.7935, -2357.5984, 29.3537>>) > 22.0) 
					if create_conversation(scripted_speech[0], "HeatAud", "heat_c4_5", CONV_PRIORITY_low) 
						set_label_as_triggered("heat_c4_5", true)
					endif 
				endif 
			endif 
			
			//player to close to c4
			if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<893.067, -2361.493, 29.320>>, <<887.091, -2360.959, 33.320>>, 4.500, false)   
				
				standing_away_from_c4 = true 
				
			else 
			
				if lk_timer(dialogue_time, 10000) or standing_away_from_c4
					if create_conversation(scripted_speech[0], "HeatAud", "fbi4_c4_0", CONV_PRIORITY_medium) 
						dialogue_time = get_game_timer()
						standing_away_from_c4 = false
					endif 
				endif 
			
			endif 
			
			if c4_cutscene_ready_to_trigger()
			
				REMOVE_ALL_PROJECTILES_OF_TYPE(WEAPONTYPE_GRENADE)
				
				clear_area(<<889.7, -2362.58, 30.24>>, 50.00, false)
					
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(army_truck.veh, false)
				SET_ENTITY_PROOFS(army_truck.veh, true, true, true, true, true)

				clear_help()
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_RIG_EXPLOSIVES")
					STOP_AUDIO_SCENE("FBI_4_RIG_EXPLOSIVES")
				endif 
				
				original_time = get_game_timer()
				
				if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<893.067, -2361.493, 29.320>>, <<887.091, -2360.959, 33.320>>, 4.500, false)   
					if start_new_cutscene_no_fade(true, true, true, true, false)
						c4_explosion_cutscene()
						blow_open_truck_doors_status++
					endif
				else 
					
					ADD_EXPLOSION_WITH_USER_VFX(<<889.7, -2362.58, 30.24>>, exp_tag_grenade, get_hash_key("EXP_VFXTAG_FBI4_TRUCK_DOORS"), 0.5)
					play_sound_frontend(explosion_sound, "FBI_04_HEAT_C4_DOORS") 
					DELETE_OBJECT(c4.obj)	
					set_entity_health(player_ped_id(), 2)
					
				endif 

			endif 
				
		break 
		
		case 3
			
			if c4_explosion_cutscene()

				//remove_weapon_from_ped(player_ped_id(), weapontype_c4_detonator)
				
				//SET_PLAYER_PED_STORED_HEALTH(char_michael, 3000)
				//SET_PLAYER_PED_STORED_HEALTH(char_trevor, 200)
				//SET_PLAYER_PED_STORED_HEALTH(char_franklin, 3000)
				
				if has_model_loaded(documents.model)
					documents.obj = create_object_no_offset(documents.model, documents.pos)
					set_entity_rotation(documents.obj, documents.rot)
					freeze_entity_position(documents.obj, true)
				endif 
			
				remove_ped_for_dialogue(scripted_speech[0], 0)
				remove_ped_for_dialogue(scripted_speech[0], 1)
				remove_ped_for_dialogue(scripted_speech[0], 2)
				remove_ped_for_dialogue(scripted_speech[0], 3)
				remove_ped_for_dialogue(scripted_speech[0], 4)
				remove_ped_for_dialogue(scripted_speech[0], 5)
				remove_ped_for_dialogue(scripted_speech[0], 6)
				remove_ped_for_dialogue(scripted_speech[0], 7)
				
				add_ped_for_dialogue(scripted_speech[0], 1, player_ped_id(), "franklin")
				add_ped_for_dialogue(scripted_speech[0], 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")
				add_ped_for_dialogue(scripted_speech[0], 2, selector_ped.pedID[SELECTOR_PED_trevor], "trevor")
				
				michael_ai_system_status = 2
				michael_ai_system()
				army_truck_guy_ai_system()
				army_truck_guy_2_ai_system()
				
				original_time = get_game_timer()

				Set_Replay_Mid_Mission_Stage_With_Name(2, "Detonate C4")
				
				mission_flow = shootout
				
			endif 

		break 
		
	endswitch 
	
endproc 

proc police_helicopter_system()

endproc 

proc cleanup_helicopter_assets(bool allow_pilot_to_fly_away = true)

	int k = 0
				
	for k = 0 to count_of(absail_police_man) - 1
		
		if enum_to_int(absail_police_man_status[k]) < enum_to_int(absail_run_to_pos)
		
			if not is_ped_injured(absail_police_man[k].ped)
				if IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(absail_police_man[k].ped)
					DETACH_ENTITY(absail_police_man[k].ped)
				endif 
				SET_PED_GRAVITY(absail_police_man[k].ped, TRUE)
			
			else 
			
				absail_police_man[k].created = true
			
			endif 

			absail_police_man_status[k] = absail_run_to_pos
		
		endif 
		
	endfor 
	
	if does_rope_exist(rope[0].rope)
		delete_rope(rope[0].rope)
	endif 
	
	if does_rope_exist(rope[1].rope)
		delete_rope(rope[1].rope)
	endif 
	
	if is_vehicle_driveable(helicopter[1].veh)
		
		if is_playback_going_on_for_vehicle(helicopter[1].veh)
			stop_playback_recorded_vehicle(helicopter[1].veh)
		endif 
	
		if allow_pilot_to_fly_away
			if not is_ped_injured(helicopter_driver[1].ped)

				clear_ped_tasks(helicopter_driver[1].ped)
					
				open_sequence_task(seq)
					TASK_HELI_MISSION(null, helicopter[1].veh, null, null, <<0.0, 0.0, 0.0>>, MISSION_GOTO, 25.0, 0, -1, 70, 65) 
				close_sequence_task(seq)
				task_perform_sequence(helicopter_driver[1].ped, seq)
				clear_sequence_task(seq)
			endif 
		endif 
		
	endif
	
	
//	if not is_ped_injured(helicopter_driver[1].ped)
//		set_entity_health(helicopter_driver[1].ped, 2)
//		set_ped_as_no_longer_needed(helicopter_driver[1].ped)
//	endif 
//		
//	if not is_ped_injured(police_man[36].ped)
//		if is_ped_sitting_in_vehicle(police_man[36].ped, helicopter[1].veh)
//			set_entity_health(helicopter_driver[1].ped, 2)
//		endif 
//	endif 
//	
//	if not is_ped_injured(police_man[37].ped)
//		if is_ped_sitting_in_vehicle(police_man[37].ped, helicopter[1].veh)
//			set_entity_health(police_man[37].ped, 2)
//		endif 
//	endif 
	
	SET_PED_AS_NO_LONGER_NEEDED(helicopter_driver[1].ped)
	
	SET_VEHICLE_AS_NO_LONGER_NEEDED(helicopter[1].veh)
	set_model_as_no_longer_needed(helicopter[1].model)
		
endproc 

proc helicopter_speed_system()
	
	switch helicopter_speed_system_status 
	
		case 0
		
			if get_time_position_in_recording(helicopter[1].veh) > 41000
				set_playback_speed(helicopter[1].veh, 0.15)
				helicopter_speed_system_status++
			endif 
		
		break 
		
		case 1
		
			if get_time_position_in_recording(helicopter[1].veh) > 42700 //43000
				set_playback_speed(helicopter[1].veh, 1.0)
				helicopter_speed_system_status++
			endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch
	
endproc 

proc apply_forces_to_helicopter()
					
	if not force_applied_to_helicopter
		
		if not is_ped_injured(helicopter_driver[1].ped)
			set_entity_health(helicopter_driver[1].ped, 2)
		endif 

		if is_vehicle_driveable(helicopter[1].veh)
			if is_playback_going_on_for_vehicle(helicopter[1].veh)
				stop_playback_recorded_vehicle(helicopter[1].veh)
			endif 

			APPLY_FORCE_TO_ENTITY(helicopter[1].veh, APPLY_TYPE_EXTERNAL_IMPULSE, <<1.0, 0.0, -6.0>>, <<0.0, -1.5, 0.0>>, 0, true, true, true)
			
			if is_explosion_in_sphere(exp_tag_dontcare, get_offset_from_entity_in_world_coords(helicopter[1].veh, <<0.0, 3.0, 0.0>>), 3.0)
				APPLY_FORCE_TO_ENTITY(helicopter[1].veh, APPLY_TYPE_ANGULAR_IMPULSE, <<0.0, 0.0, -20.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
			else 
				APPLY_FORCE_TO_ENTITY(helicopter[1].veh, APPLY_TYPE_ANGULAR_IMPULSE, <<0.0, 0.0, 20.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
			endif 
		endif 

		//script_assert("test")
		
		if get_current_player_ped_enum() = char_trevor
			//INFORM_STAT_FBI_FOUR_HELISHOT()
		endif 
		
		force_applied_to_helicopter = true 

	endif 

endproc 

PROC MAINTAIN_CHOPPER_HOVER(vehicle_index heli, float iMinRecTime, float iMaxRecTime)

	IF IS_VEHICLE_DRIVEABLE(heli)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(heli)
			IF NOT hover_backwards
				IF GET_TIME_POSITION_IN_RECORDING(heli) >= iMaxRecTime
					
					hover_speed = -1
			
					hover_backwards = TRUE
				ENDIF
			ELSE
				IF GET_TIME_POSITION_IN_RECORDING(heli) <= iMinRecTime
					
					hover_speed = 1

					hover_backwards = FALSE
				ENDIF
			ENDIF
			
			SET_PLAYBACK_SPEED(heli, hover_speed)
		ENDIF
	ENDIF
	
ENDPROC

proc helicopter_0_system()
	
	if does_entity_exist(helicopter[0].veh)
		if not is_ped_injured(helicopter_driver[0].ped)
			if is_vehicle_driveable(helicopter[0].veh)
				if is_playback_going_on_for_vehicle(helicopter[0].veh)
				
					switch helicopter_0_system_status 
					
						case 0
						
							SET_VEHICLE_SEARCHLIGHT(helicopter[0].veh, TRUE, true)
							TASK_VEHICLE_AIM_AT_COORD(helicopter_driver[0].ped, <<863.7, -2272, 38>>)
							
							helicopter_0_system_status++
						
						break 
						
						case 1
						
							if get_time_position_in_recording(helicopter[0].veh) > 28000
								TASK_VEHICLE_AIM_AT_COORD(helicopter_driver[0].ped, <<864.3, -2266.9, 38.8>>)
								helicopter_0_system_status++
							endif 
						
						break 
						
						case 2
						
							if get_time_position_in_recording(helicopter[0].veh) > 31000
								TASK_VEHICLE_AIM_AT_COORD(helicopter_driver[0].ped, <<850, -2305, 30>>)
								helicopter_0_system_status++
							endif 
						
						break 
						
						case 3
						
							if get_time_position_in_recording(helicopter[0].veh) > 45000
								TASK_VEHICLE_AIM_AT_COORD(helicopter_driver[0].ped, <<841, -2230, 38>>)
								helicopter_0_system_status++
							endif 
						
						break
						
						case 4
						
						break 
						
					endswitch 
				
				else 
					
					delete_ped(helicopter_driver[0].ped)
					delete_vehicle(helicopter[0].veh)
					remove_vehicle_recording(helicopter[0].recording_number, "lkfbi4")
					
				endif
			
			else 
			
				set_entity_health(helicopter_driver[0].ped, 2)
				set_ped_as_no_longer_needed(helicopter_driver[0].ped)
		
				add_explosion(get_entity_coords(helicopter[0].veh, false), exp_tag_grenade)
				set_vehicle_as_no_longer_needed(helicopter[0].veh)
			
			endif 
		
		else 
		
			if is_entity_dead(helicopter[0].veh)
				explode_vehicle(helicopter[0].veh)
			endif 
			
			set_ped_as_no_longer_needed(helicopter_driver[0].ped)
				
			set_vehicle_as_no_longer_needed(helicopter[0].veh)
			
		endif 
	endif 

endproc 

proc helicopter_1_system()

	if DOES_ENTITY_EXIST(helicopter[1].veh)
		if DOES_ENTITY_EXIST(helicopter_driver[1].ped)
			if not is_ped_injured(helicopter_driver[1].ped)
				if is_vehicle_driveable(helicopter[1].veh)
												
					if not is_playback_going_on_for_vehicle(helicopter[1].veh)
			
						switch helicopter_1_sysstem_status 
						
							case 0
							
								if has_ped_task_finished_2(helicopter_driver[1].ped)
							
									open_sequence_task(seq)
										TASK_HELI_MISSION(null, helicopter[1].veh, null, null, <<834.8, -2327.7, 68.00>>, MISSION_GOTO, 10.0, 1.0, 173.2674, 15, 10) 
										TASK_HELI_MISSION(null, helicopter[1].veh, null, null, <<813.1, -2338.4, 68.00>>, MISSION_GOTO, 10.0, 1.0, 173.2674, 15, 10) 
										TASK_HELI_MISSION(null, helicopter[1].veh, null, null, <<811.4, -2313.22, 68.00>>, MISSION_GOTO, 10.0, 1.0, 0.0, 15, 10) 
									close_sequence_task(seq)
									task_perform_sequence(helicopter_driver[1].ped, seq)
									clear_sequence_task(seq)
									
								endif 
							
							break 
							
							case 1
							
							break 
							
						endswitch 
						
					else 
					
						printstring("heli rec time = ")
						printfloat(get_time_position_in_recording(helicopter[1].veh))
						printnl()
					
					endif 
					
				else
				
//					//stat for heli being shot down.
//					if has_entity_been_damaged_by_entity(helicopter[1].veh, player_ped_id())
//						if has_entity_been_damaged_by_weapon(helicopter[1].veh, weapontype_rpg)
//							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI4_HELISHOT)
//						endif 
//					endif 
						
					set_entity_health(helicopter_driver[1].ped, 2)
					set_ped_as_no_longer_needed(helicopter_driver[1].ped)
					
					if not is_ped_injured(police_man[10].ped)
						if is_ped_sitting_in_vehicle(police_man[10].ped, helicopter[1].veh)
							set_entity_health(police_man[10].ped, 2)
						endif 
					endif 
					
					if not is_ped_injured(police_man[11].ped)
						if is_ped_sitting_in_vehicle(police_man[11].ped, helicopter[1].veh)
							set_entity_health(police_man[11].ped, 2)
						endif 
					endif 
			
					if not is_entity_dead(helicopter[1].veh)
						explode_vehicle(helicopter[1].veh)
					endif 
					
					add_explosion(get_entity_coords(helicopter[1].veh, false), exp_tag_grenade, 1.0)
					set_vehicle_as_no_longer_needed(helicopter[1].veh)
				
					//INFORM_STAT_FBI_FOUR_HELISHOT()
					cleanup_helicopter_assets()

				endif 
				
			else 
		
				//apply_forces_to_helicopter()
				
				//INFORM_STAT_FBI_FOUR_HELISHOT()
				
//				if not is_entity_dead(helicopter[1].veh)
//					explode_vehicle(helicopter[1].veh, false)
//				endif 
				
				set_ped_as_no_longer_needed(helicopter_driver[1].ped)
				
				helicopter_time[1] = get_game_timer()

				//cleanup_helicopter_assets()

			endif 
		
		else 
		
			if not is_entity_dead(helicopter[1].veh)

				if (get_entity_height_above_ground(helicopter[1].veh) < 2.2)
				or ((helicopter_time[1] != 0) and (lk_timer(helicopter_time[1], 7000)))
					
					set_entity_proofs(helicopter[1].veh, false, false, false, false, false)
					explode_vehicle(helicopter[1].veh)
					explode_vehicle(helicopter[1].veh)
					//SET_VEHICLE_AS_NO_LONGER_NEEDED(helicopter[1].veh)
					//set_model_as_no_longer_needed(helicopter[1].model)
					
					if not is_ped_injured(helicopter_driver[1].ped)
						set_entity_health(helicopter_driver[1].ped, 2)
						set_ped_as_no_longer_needed(helicopter_driver[1].ped)
					endif 
						
					if not is_ped_injured(police_man[10].ped)
						if is_ped_sitting_in_vehicle(police_man[10].ped, helicopter[1].veh)
							set_entity_health(police_man[10].ped, 2)
						endif 
					endif 
					
					if not is_ped_injured(police_man[11].ped)
						if is_ped_sitting_in_vehicle(police_man[11].ped, helicopter[1].veh)
							set_entity_health(police_man[11].ped, 2)
						endif 
					endif 
					
					cleanup_helicopter_assets()

				endif 
			
			else 
		
				cleanup_helicopter_assets()
	
			endif 
			
		endif 
	
	endif 
	
endproc 


proc police_men_absail_system()

	int i = 0

	helicopter_1_system()
	
	//ai for the police men absailing from the helicopter
	for i = 0 to count_of(absail_police_man) - 1
		
		if not is_ped_injured(absail_police_man[i].ped)

			switch absail_police_man_status[i]
			
				case pause_anim
				
					switch i
					
						case 0
						case 1
						case 2
						case 3

							//if is_entity_playing_anim(police_man[21].ped, "rappel_intro_player", "misssagrab") 
							if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player")
								set_entity_anim_speed(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 0.0)
								absail_police_man_status[i] = unpause_anim
							endif 
						
						break 
						
					endswitch 
				
				break 
			
				
				case unpause_anim
				
					switch i 
					
						case 0
	
							if get_time_position_in_recording(helicopter[1].veh) > 41200
						
								if IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(absail_police_man[i].ped)
									
									detach_entity(absail_police_man[i].ped, false)
									
									//if is_entity_playing_anim(police_man[i].ped, "rappel_intro_player", "misssagrab") 
									if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player")
										set_entity_anim_speed(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 1.0)
									endif 
									
									ATTACH_ROPE_TO_RAPPELLING_PED(rope[0], absail_police_man[i].ped, rope[0].pos)
									
									absail_police_man_status[i] = absail_playing_anim
								endif
								
							endif 
							
						break
						
						case 1

							if get_time_position_in_recording(helicopter[1].veh) > 41600
						
								if IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(absail_police_man[i].ped)
									
									detach_entity(absail_police_man[i].ped, false)
									
									//if is_entity_playing_anim(police_man[i].ped, "rappel_intro_player", "misssagrab") 
									if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player")
										set_entity_anim_speed(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 1.0)
									endif 
									
									ATTACH_ROPE_TO_RAPPELLING_PED(rope[1], absail_police_man[i].ped, rope[1].pos)
									
									absail_police_man_status[i] = absail_playing_anim
								endif
								
							endif 
							
						break 
						
						case 2
						
							if get_time_position_in_recording(helicopter[1].veh) > 41900
						
								if IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(absail_police_man[i].ped)
									
									detach_entity(absail_police_man[i].ped, false)
									
									//if is_entity_playing_anim(police_man[i].ped, "rappel_intro_player", "misssagrab") 
									if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player")
										set_entity_anim_speed(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 1.0)
									endif 
									
									ATTACH_ROPE_TO_RAPPELLING_PED(rope[1], absail_police_man[i].ped, rope[1].pos)
									
									absail_police_man_status[i] = absail_playing_anim
								endif
								
							endif 
						
						break 
						
						case 3
				
							if get_time_position_in_recording(helicopter[1].veh) > 42300
						
								if IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(absail_police_man[i].ped)
									
									detach_entity(absail_police_man[i].ped, false)
									
									//if is_entity_playing_anim(police_man[i].ped, "rappel_intro_player", "misssagrab") 
									if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player")
										set_entity_anim_speed(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 1.0)
									endif 
									
									ATTACH_ROPE_TO_RAPPELLING_PED(rope[1], absail_police_man[i].ped, rope[1].pos)
									
									absail_police_man_status[i] = absail_playing_anim
								endif
								
							endif 
						
						break 
						
					endswitch 

				break 
				
				case absail_playing_anim
				
					switch i 
					
						case 0
						case 2
				
							if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player") 
								IF GET_ENTITY_ANIM_CURRENT_TIME(absail_police_man[i].ped, "misssagrab", "rappel_intro_player") > 0.7
								
									SET_ENTITY_ANIM_SPEED(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 0.0)
									
									SET_ENTITY_VELOCITY(absail_police_man[i].ped, <<0.0, 0.0, -7.0>>)
									
									set_ped_can_ragdoll(absail_police_man[i].ped, true)
									
									absail_police_man_status[i] = absail_with_sliding
									
								endif 
								
								ATTACH_ROPE_TO_RAPPELLING_PED(rope[0], absail_police_man[i].ped, rope[0].pos)
							endif 
							
						break 
						
						case 1
						case 3
				
							if IS_ENTITY_PLAYING_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player")
								IF GET_ENTITY_ANIM_CURRENT_TIME(absail_police_man[i].ped, "misssagrab", "rappel_intro_player") > 0.7
								
									SET_ENTITY_ANIM_SPEED(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", 0.0)
									
									SET_ENTITY_VELOCITY(absail_police_man[i].ped, <<0.0, 0.0, -7.0>>)
									
									set_ped_can_ragdoll(absail_police_man[i].ped, true)
									
									absail_police_man_status[i] = absail_with_sliding
									
								endif 
								
								ATTACH_ROPE_TO_RAPPELLING_PED(rope[1], absail_police_man[i].ped, rope[1].pos)
							endif 
							
						break 
						
					endswitch 
					
				break 

				case absail_with_sliding
				
					vector v_hand_pos
					vector v_rope_end_pos

					switch i 

						case 0
						case 2

							v_hand_pos = GET_PED_BONE_COORDS(absail_police_man[i].ped, BONETAG_L_HAND, <<0.0, 0.0, 0.0>>)
							v_rope_end_pos = GET_ROPE_VERTEX_COORD(rope[0].rope, rope[0].i_num_segments - 1)
											
							IF (v_hand_pos.z < (v_rope_end_pos.z + 1.5))

								ATTACH_ROPE_TO_SINGLE_POINT(rope[0], rope[0].pos)
								
								SET_PED_GRAVITY(absail_police_man[i].ped, TRUE)

								original_time = get_game_timer()
								
								absail_police_man_status[i] = absail_run_to_pos
							
							else 
							
								ATTACH_ROPE_TO_RAPPELLING_PED(rope[0], absail_police_man[i].ped, rope[0].pos)
									
								SET_ENTITY_VELOCITY(absail_police_man[i].ped, <<0.0, 0.0, -7.0>>)
								
							endif 

						break 
						
						case 1
						case 3
					
							v_hand_pos = GET_PED_BONE_COORDS(absail_police_man[i].ped, BONETAG_L_HAND, <<0.0, 0.0, 0.0>>)
							v_rope_end_pos = GET_ROPE_VERTEX_COORD(rope[1].rope, rope[1].i_num_segments - 1)
											
							IF (v_hand_pos.z < (v_rope_end_pos.z + 1.5))

								ATTACH_ROPE_TO_SINGLE_POINT(rope[1], rope[1].pos)
								
								SET_PED_GRAVITY(absail_police_man[i].ped, TRUE)

								original_time = get_game_timer()
								
								absail_police_man_status[i] = absail_run_to_pos
							
							else 
							
								ATTACH_ROPE_TO_RAPPELLING_PED(rope[1], absail_police_man[i].ped, rope[1].pos)
									
								SET_ENTITY_VELOCITY(absail_police_man[i].ped, <<0.0, 0.0, -7.0>>)
								
							endif 

						break 
						
					endswitch 
				
				break 
				
				case absail_run_to_pos
				
					switch i 
						
						case 0
						case 1
						case 2
						case 3

							open_sequence_task(seq)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, absail_police_man[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
							close_sequence_task(seq)
							task_perform_sequence(absail_police_man[i].ped, seq)
							clear_sequence_task(seq)
						
							absail_police_man_status[i] = absail_get_into_pos
							
						break 
						
					endswitch 
					
				break 
				
				case absail_get_into_pos
				
					switch i 
						
						case 0
						case 1
						case 2
						case 3
				
							if not IS_ENTITY_AT_COORD(absail_police_man[i].ped, absail_police_man[i].run_to_pos, <<1.0, 1.0, 1.6>>, false, true)
								if has_ped_task_finished_2(absail_police_man[i].ped, script_task_perform_sequence)
									if GET_ENTITY_HEIGHT_ABOVE_GROUND(absail_police_man[i].ped) < 1.2
									
										clear_ped_tasks(absail_police_man[i].ped)
										set_blocking_of_non_temporary_events(absail_police_man[i].ped, true)
									
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, absail_police_man[i].run_to_pos, player_ped_id(), pedmove_run, true, 0.5, 0.8) 
										close_sequence_task(seq)
										task_perform_sequence(absail_police_man[i].ped, seq)
										clear_sequence_task(seq)
										
									endif 
								endif 
							
							else
							
								set_blocking_of_non_temporary_events(absail_police_man[i].ped, false)
								set_ped_sphere_defensive_area(absail_police_man[i].ped, absail_police_man[i].run_to_pos, 2.0)
								task_combat_ped(absail_police_man[i].ped, player_ped_id())
								absail_police_man_status[i] = absail_do_nothing
							
							endif 
							
						break 
						
					endswitch 
					
				break
				
				case absail_do_nothing
				
				break 

			endswitch 
			
		else 

			switch absail_police_man_status[i]
			
				case create_absail_ped
				
					switch i 
					
						case 0
						
							absail_police_man_status[i] = pause_anim
						
						break 
					
						case 1

							absail_police_man_status[i] = pause_anim
							
						break 
						
						case 2
						
							if not absail_police_man[i].created
								if is_vehicle_driveable(helicopter[1].veh)
									if get_time_position_in_recording(helicopter[1].veh) > 41900
								
										setup_enemy_in_vehicle(absail_police_man[i], helicopter[1].veh, vs_back_left)
										clear_ped_tasks_immediately(absail_police_man[i].ped)
										setup_relationship_enemy(absail_police_man[i].ped, true)
										attach_entity_to_entity(absail_police_man[i].ped, helicopter[1].veh, 0, <<-1.15, 0.0, 0.0>>, <<0.0, 0.0, 180.0>>, true)
										SET_PED_GRAVITY(absail_police_man[i].ped, FALSE)
										TASK_PLAY_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1)	

										absail_police_man_status[i] = pause_anim
									endif 
								endif 
								
							endif 
						
						break 
						
						case 3
			
							if not absail_police_man[i].created
								if is_vehicle_driveable(helicopter[1].veh)
									if get_time_position_in_recording(helicopter[1].veh) > 42300

										setup_enemy_in_vehicle(absail_police_man[i], helicopter[1].veh, vs_back_right)
										clear_ped_tasks_immediately(absail_police_man[i].ped)
										setup_relationship_enemy(absail_police_man[i].ped, true)
										attach_entity_to_entity(absail_police_man[i].ped, helicopter[1].veh, 0, <<1.15, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, true)
										SET_PED_GRAVITY(absail_police_man[i].ped, FALSE)
										set_ped_can_ragdoll(absail_police_man[i].ped, false)
										TASK_PLAY_ANIM(absail_police_man[i].ped, "misssagrab", "rappel_intro_player", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1)	

										absail_police_man_status[i] = pause_anim
									endif 
								endif 
								
							endif 
						
						break 
						
					endswitch 
				
				break 
				
			endswitch 
		
		endif 
		
	endfor 

endproc 

proc backup_police_front_ai_system()

	int i = 0

	for i = 0 to count_of(backup_police_front) - 1
		
		if not is_ped_injured(backup_police_front[i].ped)
		
			switch backup_police_front_shootout_status[i] 
			
				case leave_car_or_fight
				
					switch i 
					
						case 0
						case 1
						case 2
						case 3
						case 4
						case 5

							set_ped_combat_attributes(backup_police_front[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							//set_ped_combat_attributes(backup_police_front[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
							set_blocking_of_non_temporary_events(backup_police_front[i].ped, false)
							set_ped_sphere_defensive_area(backup_police_front[i].ped, backup_police_front[i].run_to_pos, 5.0)
							task_combat_hated_targets_around_ped(backup_police_front[i].ped, 200.00)
							
							backup_police_front_shootout_status[i] = run_to_pos_or_fight 

						break 
						
					endswitch 
				
				break 
				
				case run_to_pos_or_fight 
				
					switch i 
					
						case 0
						
						break 
						
					endswitch 
				
				break 
				
				case do_nothing
				
				break 
				
			endswitch 
			
		endif 
		
	endfor 

endproc 

proc backup_police_alley_ai_system()

	int i = 0

	for i = 0 to count_of(backup_police_alley) - 1
		
		if not is_ped_injured(backup_police_alley[i].ped)
		
			switch backup_police_alley_shootout_status[i] 
			
				case leave_car_or_fight
				
					switch i 
					
						case 0
						case 1
						case 2
						case 3
						case 4
						case 5

							set_ped_combat_attributes(backup_police_alley[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							//set_ped_combat_attributes(backup_police_alley[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
							set_blocking_of_non_temporary_events(backup_police_alley[i].ped, false)
							set_ped_sphere_defensive_area(backup_police_alley[i].ped, backup_police_alley[i].run_to_pos, 5.0)
							task_combat_hated_targets_around_ped(backup_police_alley[i].ped, 200.00)
							
							backup_police_alley_shootout_status[i] = run_to_pos_or_fight 

						break 
						
					endswitch 
				
				break 
				
				case run_to_pos_or_fight 
				
					switch i 
					
						case 0
						
						break 
						
					endswitch 
				
				break 
				
				case do_nothing
				
				break 
				
			endswitch 
			
		endif 
		
	endfor 

endproc 

func bool make_wave_1_peds_advance()

	if wave_1_police_created
	
		if wave_1_timer_0 = 0 
			wave_1_timer_0 = get_game_timer()
		endif 
	
		if not is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)
			
			if lk_timer(wave_1_timer_0, 15000)
				
				return true 
				
			endif
			
		else 
		
			if wave_1_timer_1 = 0
				wave_1_timer_1 = get_game_timer()
			endif 
			
			if lk_timer(wave_1_timer_1, 10000)
			
				return true 
				
			endif 
		
		endif 
	
	endif 
	
	return false

endfunc 

func bool police_man_ai_system()

	int i = 0

//	printint(ped_structure_get_total_number_of_enemies_dead(absail_police_man))
//	printnl()
	
	//police_men_absail_system()
	
	
	//*****if police men at fron of shootout are dead. Make nemies at the back run forward into their places.

	for i = 0 to count_of(police_man) - 1
		
		if not is_ped_injured(police_man[i].ped)
		
			switch shootout_status[i] 
			
				case leave_car_or_fight
				
					switch i 
					
						case 0

							if is_vehicle_driveable(police_car[0].veh)
								if is_playback_going_on_for_vehicle(police_car[0].veh)
									
									//stops drivers being shot whilst cars are on their way untill they turn in
									if ((get_time_position_in_recording(police_car[0].veh) + 3000) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[0].recording_number, "lkfbi4"))
										set_entity_proofs(police_man[i].ped, false, false, false, false, false)
									else 
										set_entity_proofs(police_man[i].ped, true, false, false, false, false)
									endif 
									
									if ((get_time_position_in_recording(police_car[0].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[0].recording_number, "lkfbi4"))
									
										stop_playback_recorded_vehicle(police_car[0].veh)

										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										shootout_status[i] = do_nothing
																				
									endif 
								endif 
							
							else 
							
//								if not is_entity_dead(police_car[0].veh)
//									if is_playback_going_on_for_vehicle(police_car[0].veh)
//										stop_playback_recorded_vehicle(police_car[0].veh)
//									endif 
//								endif 
							
								set_entity_proofs(police_man[i].ped, false, false, false, false, false)
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 1

							if is_vehicle_driveable(police_car[0].veh)
	
								if not is_playback_going_on_for_vehicle(police_car[0].veh)
									
									open_sequence_task(seq)
										task_pause(null, 500)
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
									shootout_status[i] = run_to_pos_or_fight
	
								endif 
								
							else 
									
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing

							endif 
							
						break 
						
						case 2
						
							if is_vehicle_driveable(police_car[1].veh)
								if is_playback_going_on_for_vehicle(police_car[1].veh)
									
									if ((get_time_position_in_recording(police_car[1].veh) + 3000) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[1].recording_number, "lkfbi4"))
										set_entity_proofs(police_man[i].ped, false, false, false, false, false)
									else 
										set_entity_proofs(police_man[i].ped, true, false, false, false, false)
									endif 
									
									if ((get_time_position_in_recording(police_car[1].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[1].recording_number, "lkfbi4"))
									
										stop_playback_recorded_vehicle(police_car[1].veh)

										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										
										shootout_status[i] = do_nothing

									endif 
								endif 
							
							else 
							
								set_entity_proofs(police_man[i].ped, false, false, false, false, false)
								
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing	
								
							endif 
						
						break 
						
						case 3

							if is_vehicle_driveable(police_car[1].veh)
								if not is_playback_going_on_for_vehicle(police_car[1].veh)
									
									open_sequence_task(seq)
										task_pause(null, 250)
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
									shootout_status[i] = run_to_pos_or_fight

								endif 
								
							else 
								
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing	
							endif 
							
						break 
						
						case 4
						
							if is_vehicle_driveable(police_car[2].veh)
								if is_playback_going_on_for_vehicle(police_car[2].veh)

									if ((get_time_position_in_recording(police_car[2].veh) + 3000) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[2].recording_number, "lkfbi4"))
										set_entity_proofs(police_man[i].ped, false, false, false, false, false)
									else 
										set_entity_proofs(police_man[i].ped, true, false, false, false, false)
									endif 
									
									if ((get_time_position_in_recording(police_car[2].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[2].recording_number, "lkfbi4"))
									
										stop_playback_recorded_vehicle(police_car[2].veh)
										
										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										shootout_status[i] = do_nothing
										
									endif 
								endif 
							
							else 

								set_entity_proofs(police_man[i].ped, false, false, false, false, false)
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 5

							if is_vehicle_driveable(police_car[2].veh)
								if not is_playback_going_on_for_vehicle(police_car[2].veh)
									
									open_sequence_task(seq)
										task_pause(null, 250)
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
									shootout_status[i] = run_to_pos_or_fight
								
								endif 
								
							else 
									
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing	
							endif 
							
						break 
						
						case 6
						
							if is_vehicle_driveable(police_car[3].veh)
								if not is_playback_going_on_for_vehicle(police_car[3].veh)
								
									stop_playback_recorded_vehicle(police_car[3].veh)
									
									set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
									//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
									set_blocking_of_non_temporary_events(police_man[i].ped, false)
									set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
									task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
									//set_ped_as_no_longer_needed(police_man[i].ped)
									shootout_status[i] = do_nothing

								else 
								
									if ((get_time_position_in_recording(police_car[3].veh) + 3000) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[3].recording_number, "lkfbi4"))
										set_entity_proofs(police_man[i].ped, false, false, false, false, false)
									else 
										set_entity_proofs(police_man[i].ped, true, false, false, false, false)
									endif 

								endif 
							
							else 

								set_entity_proofs(police_man[i].ped, false, false, false, false, false)
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
								
							endif 
						
						break 
						
						case 7
						
							if is_vehicle_driveable(police_car[3].veh)
								if not is_playback_going_on_for_vehicle(police_car[3].veh)
									
									open_sequence_task(seq)
										task_pause(null, 250)
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
									shootout_status[i] = run_to_pos_or_fight
								
								endif 
								
							else 
		
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing	
							endif 
						
						break 

						case 8
						
//							if does_entity_exist(helicopter.veh)
//								if is_vehicle_driveable(helicopter.veh)
//									if is_playback_going_on_for_vehicle(helicopter.veh)
//										if get_time_position_in_recording(helicopter.veh) > 33000
										
											open_sequence_task(seq)
												TASK_GO_TO_COORD_WHILE_AIMING_AT_coord(null, police_man[i].run_to_pos, <<879.7, -2354.9, 42.2>>, pedmove_run, false, 0.5, 0.5, true, enav_stop_exactly) 
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_8_z_offset = 0.0
											police_man_8_z_offset_time = get_game_timer()
											
											shootout_status[i] = run_to_pos_or_fight
											
//										endif 
//									endif 
//								endif 
//							endif 
			
						break
						
						case 9
						
//							if does_entity_exist(helicopter.veh)
//								if is_playback_going_on_for_vehicle(helicopter.veh)
//									if get_time_position_in_recording(helicopter.veh) > 33000
									
										open_sequence_task(seq)
											TASK_GO_TO_COORD_WHILE_AIMING_AT_coord(null, police_man[i].run_to_pos, <<879.7, -2354.9, 42.2>>, pedmove_run, false, 0.5, 0.5, true, enav_stop_exactly)  
										close_sequence_task(seq)
										task_perform_sequence(police_man[i].ped, seq)
										clear_sequence_task(seq)
										
										police_man_9_z_offset = 0.0//1.0
										police_man_9_z_offset_time = get_game_timer()
										
										shootout_status[i] = run_to_pos_or_fight
										
//									endif 
//								endif 
//							endif 

						break 
						
						case 10
						
							if does_entity_exist(helicopter[1].veh)
								if is_vehicle_driveable(helicopter[1].veh)
									if is_ped_sitting_in_vehicle(police_man[i].ped, helicopter[1].veh)
										if is_playback_going_on_for_vehicle(helicopter[1].veh) 
											if get_time_position_in_recording(helicopter[1].veh) > 13000
														
												shootout_status[i] = run_to_pos_or_fight
												
											endif 
										endif 
									else 
										shootout_status[i] = run_to_pos_or_fight
									endif 
									
								else 
								
									if not is_ped_sitting_in_any_vehicle(police_man[i].ped)
								
										shootout_status[i] = run_to_pos_or_fight
										
									else 
									
										set_entity_health(police_man[i].ped, 2)
									
									endif 
									
								endif 
								
							else 
							
								shootout_status[i] = run_to_pos_or_fight
							
							endif 

						break 
						
						case 11
						
							if does_entity_exist(helicopter[1].veh)
								if is_vehicle_driveable(helicopter[1].veh)
									if is_ped_sitting_in_vehicle(police_man[i].ped, helicopter[1].veh)
										if is_playback_going_on_for_vehicle(helicopter[1].veh) 
											if get_time_position_in_recording(helicopter[1].veh) > 13000
														
												shootout_status[i] = run_to_pos_or_fight
												
											endif 
										endif 
									else 
										shootout_status[i] = run_to_pos_or_fight
									endif 
									
								else 
								
									if not is_ped_sitting_in_any_vehicle(police_man[i].ped)
								
										shootout_status[i] = run_to_pos_or_fight
										
									else 
									
										set_entity_health(police_man[i].ped, 2)
									
									endif 
									
								endif 
								
							else 
							
								shootout_status[i] = run_to_pos_or_fight
							
							endif 
						
						break 
						
						case 12

							
							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = run_to_pos_or_fight
						
						break 
						
						case 13

							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = run_to_pos_or_fight
							
						break 

						case 14
						case 15
						
							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = do_nothing

						break 
						
						case 16

							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = run_to_pos_or_fight
	
						break 
						
						case 17
 
							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = run_to_pos_or_fight

						break 
						
						case 18

							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = run_to_pos_or_fight

						break 
						
						case 19
						case 20

							open_sequence_task(seq)
								task_pause(null, 1500)
//								TASK_SET_SPHERE_DEFENSIVE_AREA(null, police_man[i].run_to_pos, 2.0)
//								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null, false)
//								task_combat_hated_targets_around_ped(null, 200.00)
							close_sequence_task(seq)
							task_perform_sequence(police_man[i].ped, seq)
							clear_sequence_task(seq)
							
							shootout_status[i] = run_to_pos_or_fight

						break 
						
						case 21
						
							if is_vehicle_driveable(helicopter[0].veh)
								if is_playback_going_on_for_vehicle(helicopter[0].veh)
									if (get_time_position_in_recording(helicopter[0].veh) > 27000)
									
										open_sequence_task(seq)
											task_go_to_coord_while_aiming_at_coord(null, police_man[i].run_to_pos, <<847.3, -2326.2, 31.0>>, pedmove_run, false, 0.5, 0.5, true, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION) 
										close_sequence_task(seq)
										task_perform_sequence(police_man[i].ped, seq)
										clear_sequence_task(seq)

										shootout_status[i] = run_to_pos_or_fight 
										
									endif 
								endif 
							endif 
							
						break 
						
						case 22
						
							if is_vehicle_driveable(helicopter[0].veh)
								if is_playback_going_on_for_vehicle(helicopter[0].veh)
									if (get_time_position_in_recording(helicopter[0].veh) > 27000)

										open_sequence_task(seq)
											task_go_to_coord_while_aiming_at_coord(null, police_man[i].run_to_pos, <<847.3, -2326.2, 31.0>>, pedmove_run, false, 0.5, 0.5, true, ENAV_STOP_EXACTLY | ENAV_DONT_ADJUST_TARGET_POSITION) 
										close_sequence_task(seq)
										task_perform_sequence(police_man[i].ped, seq)
										clear_sequence_task(seq)

										shootout_status[i] = run_to_pos_or_fight 
										
									endif 
								endif 
							endif 
							
						break 

						case 23
									
							open_sequence_task(seq)
								TASK_FOLLOW_WAYPOINT_RECORDING(null, "heat1", 3, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_START_TASK_INITIALLY_AIMING) 
							close_sequence_task(seq)
							task_perform_sequence(police_man[i].ped, seq)
							clear_sequence_task(seq)
							
							shootout_status[i] = run_to_pos_or_fight 
							
						break 
						
						case 24
						
						//ewaypoint_follow_flags

							open_sequence_task(seq)
								task_pause(null, 1000)
								TASK_FOLLOW_WAYPOINT_RECORDING(null, "heat1", 5, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS | EWAYPOINT_START_TASK_INITIALLY_AIMING | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE) 
								task_aim_gun_at_entity(null, player_ped_id(), 1000)
							close_sequence_task(seq)
							task_perform_sequence(police_man[i].ped, seq)
							clear_sequence_task(seq)

							shootout_status[i] = run_to_pos_or_fight

						break 
						
						case 25
						
							open_sequence_task(seq)
								task_pause(null, 2000)
								TASK_FOLLOW_WAYPOINT_RECORDING(null, "heat1", 5, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_START_TASK_INITIALLY_AIMING) 
								task_aim_gun_at_entity(null, player_ped_id(), 2000)
							close_sequence_task(seq)
							task_perform_sequence(police_man[i].ped, seq)
							clear_sequence_task(seq)
							
							shootout_status[i] = run_to_pos_or_fight
							
						break 
						
						case 26
						
							open_sequence_task(seq)
								task_pause(null, 3000)
								TASK_FOLLOW_WAYPOINT_RECORDING(null, "heat1", 5, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS | EWAYPOINT_START_TASK_INITIALLY_AIMING)  //| EWAYPOINT_navmesh_to_initial_waypoint
							close_sequence_task(seq)
							task_perform_sequence(police_man[i].ped, seq)
							clear_sequence_task(seq)

							shootout_status[i] = run_to_pos_or_fight
						
						break 

						case 27
						
							if is_vehicle_driveable(police_car[4].veh)
								if is_playback_going_on_for_vehicle(police_car[4].veh)
									if ((get_time_position_in_recording(police_car[4].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[4].recording_number, "lkfbi4"))
									
										stop_playback_recorded_vehicle(police_car[4].veh)

										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										shootout_status[i] = do_nothing
																				
									endif 
								endif 
							
							else 
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 28
						
							if is_vehicle_driveable(police_car[4].veh)
								if not is_playback_going_on_for_vehicle(police_car[4].veh)
									
									open_sequence_task(seq)
										task_pause(null, 250)
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
									shootout_status[i] = run_to_pos_or_fight
								endif 
								
							else 
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing	
							endif 
	
						break 
						
						case 29
						case 30
						case 34
						case 35
						
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							shootout_status[i] = do_nothing

						break 
						
						case 31
						
							open_sequence_task(seq)
								task_aim_gun_at_coord(null, <<877.1, -2353.6, 40.0>>, 5500) 
								task_go_to_coord_while_aiming_at_coord(null, <<932.2432, -2378.9121, 40.2425>>, <<877.1, -2353.6, 40.0>>, pedmove_run, false, 0.5, 0.5) 
								task_follow_nav_mesh_to_coord(null, police_man[i].run_to_pos, pedmove_run, 20000)
							close_sequence_task(seq)
							task_perform_sequence(police_man[i].ped, seq)
							clear_sequence_task(seq)
							
							shootout_status[i] = run_to_pos_or_fight
											
						break 
						
						case 32
						
							if is_vehicle_driveable(police_car[5].veh)
								if is_playback_going_on_for_vehicle(police_car[5].veh)
									if ((get_time_position_in_recording(police_car[5].veh) + 500) > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(police_car[5].recording_number, "lkfbi4"))
									
										clear_area(get_entity_coords(police_car[5].veh), 2.0, true)
										stop_playback_recorded_vehicle(police_car[5].veh)

										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										shootout_status[i] = do_nothing
																				
									endif 
								endif 
							
							else 
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 33
						
							if is_vehicle_driveable(police_car[5].veh)
								if not is_playback_going_on_for_vehicle(police_car[5].veh)
									
									open_sequence_task(seq)
										task_pause(null, 250)
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
									shootout_status[i] = run_to_pos_or_fight
								endif 
								
							else 
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing	
							endif 
	
						break 

					endswitch 

				break 
			
				case run_to_pos_or_fight 
				
					switch i
					
						case 1
						case 3
						case 5
						case 7
						case 15
						case 28
						case 33
						
							if has_ped_task_finished_2(police_man[i].ped)
						
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
								
							endif 

						break 
						
						case 8
						
							if not is_entity_at_coord(police_man[i].ped, police_man[i].run_to_pos, <<1.0, 1.0, 2.0>>)
								
								if has_ped_task_finished_2(police_man[i].ped)
								or has_ped_task_finished_2(police_man[i].ped, script_task_perform_sequence, 1)
								//if get_script_task_status(police_man[i].ped, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) != performing_task 
							
									open_sequence_task(seq)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_coord(null, police_man[i].run_to_pos, <<879.7, -2354.9, 42.2>>, pedmove_run, false, 0.5, 0.5, true, enav_stop_exactly) 
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							else 
							
//								on take control for every sniper ped clear their task and give them task aim gun
//								or task_aim_gun
//								
//								when run spline is over proof the player then have a system which un proofs them after a 
//								second
//								
//								if the player switches straight away before the proofs deactivated 
//								on setup cam for switch 
//								
//								disable all 3 ped proofs use get_current_player_ped_enum()

//								police_man_time[8] = 0
//								police_man_time[9] = 0
							
								if lk_timer(police_man_time[i], 7500)
								
									switch get_current_player_ped_enum()
									
										//case char_michael
										case char_trevor
							
											open_sequence_task(seq)
												task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<0.0, 0.0, police_man_8_z_offset>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)
												task_aim_gun_at_entity(null, selector_ped.pedID[selector_ped_franklin], -1)
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_time[i] = get_game_timer()
											
											GET_PED_MAX_HEALTH(selector_ped.pedID[selector_ped_franklin])
										
										break 
										
										case char_michael
										case char_franklin
										
											open_sequence_task(seq)
												//task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_michael], <<0.0, 0.0, police_man_8_z_offset>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)
												task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, police_man_8_z_offset>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)
												task_aim_gun_at_entity(null, player_ped_id(), -1)
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_time[i] = get_game_timer()
										
										break 

									endswitch 

									if lk_timer(police_man_8_z_offset_time, 10000)
										
										police_man_8_z_offset -= 0.2
										
										if police_man_8_z_offset < 0.0
											police_man_8_z_offset = 0.0
									 	endif 
										
										police_man_8_z_offset_time = get_game_timer()
										
									endif 
									
								endif  
							
							endif 
							
							if has_ped_been_damaged_by_sniper(police_man[i].ped, player_ped_id())
								set_entity_health(police_man[i].ped, 2)
							endif 
						
						break 

						case 9
						
							if not is_entity_at_coord(police_man[i].ped, police_man[i].run_to_pos, <<1.0, 1.0, 2.0>>)
								
								if has_ped_task_finished_2(police_man[i].ped)
								or has_ped_task_finished_2(police_man[i].ped, script_task_perform_sequence, 1)
								//if get_script_task_status(police_man[i].ped, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) != performing_task 
							
									open_sequence_task(seq)
										TASK_GO_TO_COORD_WHILE_AIMING_AT_coord(null, police_man[i].run_to_pos, <<879.7, -2354.9, 42.2>>, pedmove_run, false, 0.5, 0.5, true, enav_stop_exactly) 
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							else 
							
								if lk_timer(police_man_time[i], 10500)
								
									switch get_current_player_ped_enum()
									
										case char_trevor
								
											open_sequence_task(seq)
												task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<0.0, 0.0, police_man_9_z_offset>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)firing_type_1_then_aim)
												task_aim_gun_at_entity(null, selector_ped.pedID[selector_ped_franklin], -1)
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_time[i] = get_game_timer()
										
										break 
										
										case char_michael
										case char_franklin
										
											open_sequence_task(seq)
												//TASK_SHOOT_AT_ENTITY(null, 
												task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, police_man_9_z_offset>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)firing_type_1_then_aim)
												task_aim_gun_at_entity(null, player_ped_id(), -1)
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_time[i] = get_game_timer()
										
										break 
										
									endswitch 
									
								endif 
								
								if lk_timer(police_man_9_z_offset_time, 20000)
										
									police_man_9_z_offset -= 0.2
									
									if police_man_9_z_offset < 0.0
										police_man_9_z_offset = 0.0
								 	endif 
									
									police_man_9_z_offset_time = get_game_timer()
									
								endif 
							
							endif 
							
							if has_ped_been_damaged_by_sniper(police_man[i].ped, player_ped_id())
								set_entity_health(police_man[i].ped, 2)
							endif 
						
						
//							if has_ped_task_finished_2(police_man[i].ped)
//						
//								//set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
//								//set_ped_combat_movement(police_man[i].ped, cm_stationary)
//								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
//								set_blocking_of_non_temporary_events(police_man[i].ped, false)
//								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 1.0)
//								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
//								//set_ped_as_no_longer_needed(police_man[i].ped)
//								shootout_status[i] = do_nothing
//								
//							endif 
						
						break 
						
						case 10
						
							if does_entity_exist(helicopter[1].veh)
								if is_vehicle_driveable(helicopter[1].veh)
									if is_ped_sitting_in_vehicle(police_man[i].ped, helicopter[1].veh)
										if has_ped_task_finished_2(police_man[i].ped, script_task_Drive_by, -2, false)
		
											switch get_current_player_ped_enum()
						
												case char_trevor
										
													TASK_DRIVE_BY(police_man[i].ped, player_ped_id(), null, <<0.0, 0.0, 2.0>>, 300.00, 50, false, FIRING_PATTERN_FULL_AUTO)
													
													//firing_pattern_burst_fire_driveby
												
												break 
												
												case char_michael
												case char_franklin
												
													TASK_DRIVE_BY(police_man[i].ped, selector_ped.pedID[selector_ped_trevor], null, <<0.0, 0.0, 2.0>>, 300.00, 50, false, FIRING_PATTERN_FULL_AUTO)
												
												break 
												
											endswitch 
											
											police_man_time[10] = get_game_timer()

										endif
									
									else 
									
										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										shootout_status[i] = do_nothing
										
									endif 
								
								else 
								
									set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
									//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
									set_blocking_of_non_temporary_events(police_man[i].ped, false)
									set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
									task_combat_hated_targets_around_ped(police_man[i].ped, 200)
									//set_ped_as_no_longer_needed(police_man[i].ped)
									shootout_status[i] = do_nothing
								
								endif
								
							else 
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
							
							endif 
						
						break 
						
						case 11
						
							if does_entity_exist(helicopter[1].veh)
								if is_vehicle_driveable(helicopter[1].veh)
									if is_ped_sitting_in_vehicle(police_man[i].ped, helicopter[1].veh)
										if has_ped_task_finished_2(police_man[i].ped, script_task_Drive_by, -2, false)
		
											//if lk_timer(police_man_time[11], 7000)
											
												switch get_current_player_ped_enum()
							
													case char_trevor
											
														TASK_DRIVE_BY(police_man[i].ped, player_ped_id(), null, <<0.0, 0.0, 2.0>>, 300.00, 50, false,  FIRING_PATTERN_FULL_AUTO)
													
													break 
													
													case char_michael
													case char_franklin
													
														TASK_DRIVE_BY(police_man[i].ped, selector_ped.pedID[selector_ped_trevor], null, <<0.0, 0.0, 2.0>>, 300.00, 50, false,  FIRING_PATTERN_FULL_AUTO)
													
													break 
													
												endswitch
												
												police_man_time[11] = get_game_timer()
												
											//endif 

										endif
									
									else 
									
										set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
										//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
										set_blocking_of_non_temporary_events(police_man[i].ped, false)
										set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
										task_combat_hated_targets_around_ped(police_man[i].ped, 200)
										//set_ped_as_no_longer_needed(police_man[i].ped)
										shootout_status[i] = do_nothing
										
									endif 
								
								else 
								
									set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
									//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
									set_blocking_of_non_temporary_events(police_man[i].ped, false)
									set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
									task_combat_hated_targets_around_ped(police_man[i].ped, 200)
									//set_ped_as_no_longer_needed(police_man[i].ped)
									shootout_status[i] = do_nothing
								
								endif
								
							else 
							
								set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, true)
								//set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200)
								//set_ped_as_no_longer_needed(police_man[i].ped)
								shootout_status[i] = do_nothing
							
							endif 
						
						break 
						
						//wave 1 police
						case 12
						case 13
						case 16
						case 17
						case 18
						
//							if is_entity_in_angled_area(player_ped_id(), <<843.540, -2286.460, 29.339>>, <<864.798, -2287.797, 32.339>>, 80.00)
//							or is_entity_in_angled_area(player_ped_id(), <<835.410, -2326.029, 29.339>>, <<859.732, -2316.838, 32.339>>, 22.00)
							if make_wave_1_peds_advance()
							or has_ped_been_harmed(police_man[i].ped, police_man[i].health, police_man[i].created)
							
								REMOVE_PED_DEFENSIVE_AREA(police_man[i].ped)
								set_ped_combat_movement(police_man[i].ped, CM_WILLADVANCE)
								SET_PED_COMBAT_ATTRIBUTES(police_man[i].ped, CA_CAN_CHARGE, TRUE)
								SET_PED_config_flag(police_man[i].ped, PCF_ShouldChargeNow, true)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200)
								
								//script_assert("advance")
								
								shootout_status[i] = do_nothing
	
							endif 
							
						break 
	
						case 19
						case 20
		
//							if is_entity_in_angled_area(player_ped_id(), <<843.540, -2286.460, 29.339>>, <<864.798, -2287.797, 32.339>>, 80.00)
//							or is_entity_in_angled_area(player_ped_id(), <<835.410, -2326.029, 29.339>>, <<859.732, -2316.838, 32.339>>, 22.00)
//							or has_ped_been_harmed(police_man[i].ped, police_man[i].health, police_man[i].created)
							
							if has_ped_task_finished_2(police_man[i].ped)
							
								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
								set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								
								shootout_status[i] = do_nothing
	
							endif 
						
						break 
						
						case 21
						case 22
						
							if has_ped_task_finished_2(police_man[i].ped)

								set_blocking_of_non_temporary_events(police_man[i].ped, false)
								set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 1.0)
								task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
								
								shootout_status[i] = run_to_pos_2
								
							endif 

						break 
						
						case 23
						case 24
						case 25
						case 26
						
							vector target_waypoint_pos
							
							target_waypoint_pos = <<0.0, 0.0, 0.0>>
						
							if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(police_man[i].ped)
								WAYPOINT_PLAYBACK_OVERRIDE_SPEED(police_man[i].ped, 2.0)
								WAYPOINT_PLAYBACK_START_AIMING_AT_COORD(police_man[i].ped, <<849.0, -2328.6, 31.1>>, false) 
							endif 
							
							waypoint_recording_get_num_points("heat1", total_number_of_points)
							total_number_of_points -= 1
							
							police_man[i].pos = get_entity_coords(police_man[i].ped)

							if waypoint_recording_get_coord("heat1", total_number_of_points, target_waypoint_pos)
								if is_entity_at_coord(police_man[i].ped, target_waypoint_pos, <<1.0, 1.0, 1.6>>)
								//or WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(
								or (IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(police_man[i].ped) and GET_PED_WAYPOINT_PROGRESS(police_man[i].ped) > 11)
								or police_man[i].pos.z < 30.5
								
									set_ped_combat_attributes(police_man[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, true) 
									SET_PED_config_flag(police_man[i].ped, PCF_ShouldChargeNow, true)
									set_blocking_of_non_temporary_events(police_man[i].ped, false)
									set_ped_sphere_defensive_area(police_man[i].ped, police_man[i].run_to_pos, 2.0)
									task_combat_hated_targets_around_ped(police_man[i].ped, 200)
				
									shootout_status[i] = do_nothing
									
								endif 
							endif 
							
							

						break
						
						case 31
						
							if not is_entity_at_coord(police_man[i].ped, police_man[i].run_to_pos, <<1.0, 1.0, 2.0>>)
								
								if has_ped_task_finished_2(police_man[i].ped)
								or has_ped_task_finished_2(police_man[i].ped, script_task_perform_sequence, 1)
								//if get_script_task_status(police_man[i].ped, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) != performing_task 
							
									open_sequence_task(seq)
										task_follow_nav_mesh_to_coord(null, police_man[i].run_to_pos, pedmove_run, 20000, 0.5) 
									close_sequence_task(seq)
									task_perform_sequence(police_man[i].ped, seq)
									clear_sequence_task(seq)
									
								endif 
								
							else 
							
								if lk_timer(police_man_time[i], 12000)
								
									switch get_current_player_ped_enum()
									
										case char_michael
										case char_trevor
							
											open_sequence_task(seq)
												task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<0.0, 0.0, police_man_8_z_offset>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)
												task_aim_gun_at_entity(null, selector_ped.pedID[selector_ped_franklin], -1)
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_time[i] = get_game_timer()
										
										break 
										
										case char_franklin
										
											open_sequence_task(seq)
												task_shoot_at_coord(null, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 0.2>>), 4000, firing_type_1_burst)//firing_type_1_then_aim)
												task_aim_gun_at_entity(null, player_ped_id(), -1)
											close_sequence_task(seq)
											task_perform_sequence(police_man[i].ped, seq)
											clear_sequence_task(seq)
											
											police_man_time[i] = get_game_timer()
										
										break 

									endswitch 

//									if lk_timer(police_man_8_z_offset_time, 10000)
//										
//										police_man_8_z_offset -= 0.2
//										
//										if police_man_8_z_offset < 0.0
//											police_man_8_z_offset = 0.0
//									 	endif 
//										
//										police_man_8_z_offset_time = get_game_timer()
//										
//									endif 
									
								endif  
							
							endif 
							
							if has_ped_been_damaged_by_sniper(police_man[i].ped, player_ped_id())
								set_entity_health(police_man[i].ped, 2)
							endif 
						
						break 

					endswitch 
					
				break
				
				case run_to_pos_2
				
					switch i 
					
						case 21
						case 22
						
							int heli_swat_dead_count
							
							heli_swat_dead_count = 0

							//wait till all peds created
							if police_man[21].created
							and police_man[22].created
							and police_man[23].created
							and police_man[24].created
							and police_man[25].created
							
								int k
							
								for k = 21 to 25
									if is_ped_injured(police_man[k].ped)
										heli_swat_dead_count++
									endif 
								endfor 
								
								if heli_swat_dead_count >= 2
								
									REMOVE_PED_DEFENSIVE_AREA(police_man[i].ped)
									set_ped_combat_movement(police_man[i].ped, CM_WILLADVANCE)
									SET_PED_COMBAT_ATTRIBUTES(police_man[i].ped, CA_CAN_CHARGE, TRUE)
									SET_PED_config_flag(police_man[i].ped, PCF_ShouldChargeNow, true)
									task_combat_hated_targets_around_ped(police_man[i].ped, 200)
									
									shootout_status[i] = do_nothing
								
								endif 
		
							endif
						
						break 
						
					endswitch 
				
				break 
				
				case do_nothing
				
				break 
				
			endswitch 
			
		endif 
		
	endfor 
	
	return false 

endfunc 

proc hot_swap_system()

//	proc disable_previous_ped_ragdoll_system()
//	
//		switch disable_previous_ped_ragdoll_system_status
//		
//			case 0
//			
//			if disable_previous_ped_ragdoll
//
//				if not is_ped_ragdoll(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
//					set_ped_can_ragdoll(selector_ped.pedID[selector_ped.ePreviousSelectorPed], false)
//					
//					disable_previous_ped_ragdoll = true 
//				endif 
//	
//	endproc 

//	printstring("final node rot z: ")
//	printfloat(final_node_rot.z)
//	printnl()
//	
//	printstring("relative heading: ")
//	printfloat(relative_player_heading)
//	printnl()
//
//	printstring("player heading")
//	printfloat(get_entity_heading(player_ped_id()))
//	printnl()
	
	switch hot_swap_system_status 
	
		case 0
			
			if allow_hotswap
				if update_selector_hud(selector_ped)
					
					clear_help()

					if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_MICHAEL)
	
						if setup_hotswap_cam_to_michael()
							hot_swap_system_status++
						endif 
						
					elif HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRANKLIN)
					
						if setup_hotswap_cam_to_franklin()
							hot_swap_system_status++
						endif 
					
					elif HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_TREVOR)
						
						if setup_hotswap_cam_to_trevor()
							hot_swap_system_status++
						endif 

					endif 

				endif 
				
			endif  
			
		break 
		
		case 1
	
//			//calculate extra camera when switching to trevor.
//			if GET_CURRENT_PLAYER_PED_ENUM() = char_trevor
//				update_extra_spline_cam_node_pos_and_rot(sCamDetails.pedTo, <<893.53, -2353.36, 30.52>>, 1.2) 
//			else
//				update_extra_spline_cam_node_pos_and_rot(sCamDetails.pedTo, <<893.53, -2353.36, 30.52>>)
//			endif 


//			//*****add back when using the pointing at coord system to update the spline node pos 
//			update_extra_spline_cam_node_pos_and_rot(sCamDetails.pedTo, <<893.53, -2353.36, 30.52>>, 1.0) 
			
			//*****added back for pointing at pos 
			//relative_player_heading = get_final_gameplay_cam_relative_heading_to_look_at_pos(sCamDetails.pedTo, <<893.53, -2353.36, 30.52>>)
		
			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 0.0, relative_player_heading, selector_cam_default, 0) //500	// Returns FALSE when the camera spline is complete
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails, switch_type_short, 0, 999, 999)//relative_player_heading)
			//if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)
				
//				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
//				
//				set_gameplay_cam_relative_heading(relative_player_heading)
				
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, true, true)	
						
							set_ped_can_ragdoll(player_ped_id(), true)
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
							
							switch get_current_player_ped_enum()
							
								case char_michael 
								
									start_audio_scene("FBI_4_SHOOTOUT_MICHAEL")

									michael_special_ability_time = 0
								
								break 
								
								case char_franklin
								
									start_audio_scene("FBI_4_SHOOTOUT_FRANKLIN")

								break
								
								case char_trevor
								
									start_audio_scene("FBI_4_SHOOTOUT_TREVOR")
								
								break 
								
							endswitch 
							
							setup_buddy_attributes(selector_ped.pedID[selector_ped.ePreviousSelectorPed]) //true
							setup_relationship_contact(selector_ped.pedID[selector_ped.ePreviousSelectorPed], false) //true
							//task_combat_hated_targets_around_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 200.00)
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(selector_ped.pedID[selector_ped.ePreviousSelectorPed], "FBI_4_BUDDIES_GROUP")
							
							weapon_type previous_ped_weapon
							get_current_ped_weapon(selector_ped.pedID[selector_ped.ePreviousSelectorPed], previous_ped_weapon)
							
							//sets the peds weapon to be a pistol to stop the buddies killing all the police easily.
							//if GET_PREVIOUS_PLAYER_PED_ENUM() != char_trevor
							if (selector_ped.ePreviousSelectorPed != selector_ped_trevor)
							
								if previous_ped_weapon = weapontype_grenadelauncher
								or previous_ped_weapon = weapontype_rpg
									if not has_ped_got_weapon(selector_ped.pedID[selector_ped.ePreviousSelectorPed], weapontype_pistol)
										give_weapon_to_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], weapontype_pistol, 200)
									else 
										if get_ammo_in_ped_weapon(selector_ped.pedID[selector_ped.ePreviousSelectorPed], weapontype_pistol) < 200
											set_ped_ammo(selector_ped.pedID[selector_ped.ePreviousSelectorPed], weapontype_pistol, 200)
										endif
										set_current_ped_weapon(selector_ped.pedID[selector_ped.ePreviousSelectorPed], weapontype_pistol, false)
									endif
									//equipe best player weapon
								endif
								
							endif 
							
							switch selector_ped.ePreviousSelectorPed 
							
								case SELECTOR_PED_MICHAEL
								
									if IS_AUDIO_SCENE_ACTIVE("FBI_4_SHOOTOUT_MICHAEL")
										STOP_AUDIO_SCENE("FBI_4_SHOOTOUT_MICHAEL")
									endif

									michael_defensive_sphere_system()
								
								break 
							
								case SELECTOR_PED_FRANKLIN
								
									if IS_AUDIO_SCENE_ACTIVE("FBI_4_SHOOTOUT_FRANKLIN")
										STOP_AUDIO_SCENE("FBI_4_SHOOTOUT_FRANKLIN")
									endif
						
									franklin_defensive_sphere_system()
								
								break 
								
								case SELECTOR_PED_TREVOR
								
									if IS_AUDIO_SCENE_ACTIVE("FBI_4_SHOOTOUT_TREVOR")
										STOP_AUDIO_SCENE("FBI_4_SHOOTOUT_TREVOR")
									endif
			
									vector trevor_pos
									
									trevor_pos = get_entity_coords(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
									
									if trevor_pos.z > 40.00
									
										if not is_entity_on_screen(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
											set_entity_coords(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<804.7164, -2330.2073, 61.0967>>)
											set_entity_heading(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 264.4371)
										endif 
									
										set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<804.7164, -2330.2073, 61.09672>>, 0.75)
										set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped.ePreviousSelectorPed], true)
										
									else 
									
										set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<871.4848, -2339.3643, 29.3369>>, 2.0)
										set_blocking_of_non_temporary_events(player_ped_id(), false)
									
									endif 
									
									trevor_time = get_game_timer() //sets timer for firing rpg
									
									SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped_trevor], 1500)

								break 

							endswitch 
							
							//makes the snipers aim at the previous ped so when control is given back to the player
							//after the switch, the snipers will NOT be continuing their task of shooting at the 
							//ped index the player has switched to.
							if shootout_status[8] = run_to_pos_or_fight
								if not is_ped_injured(police_man[8].ped)
									task_aim_gun_at_entity(police_man[8].ped, selector_ped.pedID[selector_ped.ePreviousSelectorPed], 2000)
									force_ped_ai_And_animation_update(police_man[8].ped)
									police_man_time[8] = 0 //forces ped to aim again as task is only 2000ms so he will stop after 2000ms and if timer not exceeded will stad still untill timer met and task re-given
								endif 
							endif 
							
							if shootout_status[9] = run_to_pos_or_fight
								if not is_ped_injured(police_man[9].ped)
									task_aim_gun_at_entity(police_man[9].ped, selector_ped.pedID[selector_ped.ePreviousSelectorPed], 2000)
									force_ped_ai_And_animation_update(police_man[9].ped)
									police_man_time[9] = 0
								endif 
							endif 
							
							if shootout_status[31] = run_to_pos_or_fight
								if not is_ped_injured(police_man[31].ped)
									task_aim_gun_at_entity(police_man[31].ped, selector_ped.pedID[selector_ped.ePreviousSelectorPed], 2000)
									force_ped_ai_And_animation_update(police_man[31].ped)
									police_man_time[31] = 0 
								endif 
							endif 
							
							//stops trevor from dying as he stands in the open during the shootout
							if not wave_5_police_created
								if get_current_player_ped_enum() != char_trevor
									set_entity_only_damaged_by_player(selector_ped.pedID[selector_ped_trevor], true)
									printstring("trevor only DAMAGED BY PLAYER TRUE")
									printnl()
								else 
									set_entity_only_damaged_by_player(player_ped_id(), false)
									printstring("player as trevor only DAMAGED BY PLAYER TRUE")
									printnl()
								endif 
							else 
								if get_current_player_ped_enum() != char_trevor
									set_entity_only_damaged_by_player(selector_ped.pedID[selector_ped_trevor], false)
									printstring("only DAMAGED BY PLAYER FALSE")
									printnl()
								endif 
							endif 
							
							INFORM_MISSION_STATS_OF_INCREMENT(FBI4_SWITCHES)
							
							sCamDetails.bPedSwitched = TRUE
						endif 
					endif 
				endif 
			
			else
			
				switch get_current_player_ped_enum()
				
					case char_michael
					
					break 
					
					case char_franklin
					
					break 
					
					case char_trevor
					
						create_conversation(scripted_speech[0], "heataud", "heat_T_att0" , CONV_priority_medium)
					
					break 
					
				endswitch 
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
			
				setup_previous_ped_blip()
				
				hot_swap_system_status = 0
				
			endif 

		break 
		
		case 2
		
//			if DOES_ENTITY_EXIST(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
//				if not is_ped_injured(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
//		
//					setup_relationship_contact(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
//					//task_combat_hated_targets_around_ped(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 200.00)
//					
//					switch selector_ped.ePreviousSelectorPed 
//					
//						case SELECTOR_PED_MICHAEL
//						
//							set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<871.5512, -2344.4771, 29.3310>>, 2.0)
//							printstring("*****MICHAEL******")
//							printnl()
//						
//						break 
//					
//						case SELECTOR_PED_FRANKLIN
//						
//							set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<873.0978, -2352.7793, 29.3307>>, 2.0)
//							printstring("*****FRANKLIN******")
//							printnl()
//						
//						break 
//						
//						case SELECTOR_PED_TREVOR
//	
//							vector trevor_pos
//							
//							trevor_pos = get_entity_coords(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
//							
//							if trevor_pos.z > 40.00
//							
//								if not is_entity_on_screen(selector_ped.pedID[selector_ped.ePreviousSelectorPed])
//									set_entity_coords(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<804.7164, -2330.2073, 61.0967>>)
//									set_entity_heading(selector_ped.pedID[selector_ped.ePreviousSelectorPed], 61.1230)
//								endif 
//							
//								set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<804.7164, -2330.2073, 61.09672>>, 0.1)
//								set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped.ePreviousSelectorPed], true)
//								
//							else 
//							
//								set_ped_sphere_defensive_area(selector_ped.pedID[selector_ped.ePreviousSelectorPed], <<871.4848, -2339.3643, 29.3369>>, 2.0)
//								set_blocking_of_non_temporary_events(player_ped_id(), false)
//							
//							endif 
//							
//							printstring("*****TREVOR******")
//							printnl()
//
//						break 
//
//					endswitch 
//					
//					hot_swap_system_status = 0
//					
//				endif 
//			endif 
		
		break 
		
		case 3
		
		break 
		
	endswitch 

endproc 

func bool has_left_alley_cops_been_harmed_by_player()

	int i

	for i = 12 to 13
		if police_man[i].created
			if is_ped_injured(police_man[i].ped)
			or has_entity_been_damaged_by_entity(police_man[i].ped, player_ped_id())
				return true 
			endif 
		endif 
	endfor 
	
	for i = 16 to 18 
		if police_man[i].created
			if is_ped_injured(police_man[i].ped)
			or has_entity_been_damaged_by_entity(police_man[i].ped, player_ped_id())
				return true 
			endif 
		endif 
	endfor
	
	return false
	
endfunc 

func bool left_alley_dialogue_system()

	if left_alley_dialogue_system_status < 5
		if left_alley_dialogue_system_status > 2 //check once the main dialogue has been played
	
			if is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
			or has_left_alley_cops_been_harmed_by_player()
			
				left_alley_dialogue_system_status = 5
				
			endif 
			
		endif 
		
	endif 
	

	switch left_alley_dialogue_system_status
	
		case 0
		
			if police_man[12].created
				if create_conversation(scripted_speech[0], "heataud", "fbi4_frankB", CONV_PRIORITY_medium) 
					left_alley_dialogue_system_status++
					return true
				endif 
			endif 
		
		break 
		
		case 1
		
			//if create_conversation(scripted_speech[0], "heataud", "m_attk_snipe", CONV_PRIORITY_medium) 
			if create_conversation(scripted_speech[0], "heataud", "alleycops1", CONV_PRIORITY_medium) 
				left_alley_dialogue_system_status++
				return true
			endif 
		
		break 
		
		case 2
		
			if not is_any_text_being_displayed(locates_data)
				print_now("cntry_god30", default_god_text_time, 1)
				left_alley_dialogue_system_status++
				return true
			endif 
			
		break 
		
		case 3
		
			if not is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
			and not has_left_alley_cops_been_harmed_by_player()

				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech[0], "heataud", "fbi4_frankB2", CONV_PRIORITY_medium) 
						left_alley_dialogue_system_status++
						return true
					endif 
				endif 
				
			endif 
		
		break 
		
		case 4
		
			if not is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
			and not has_left_alley_cops_been_harmed_by_player()
			
				if create_conversation(scripted_speech[0], "heataud", "fbi4_frankB3", CONV_PRIORITY_medium) 
					left_alley_dialogue_system_status++
					return true
				endif 
				
			endif 
		
		break 
		
		case 5
		
			//do nothing
		
		break 
		
	endswitch 
	
	return false 

endfunc 

func bool trigger_wave_2_front_dialogue()
	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		//ped not in alley area
		if not is_entity_in_angled_area(selector_ped.pedID[selector_ped_michael], <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)
			if create_conversation(scripted_speech[0], "heataud", "fbi4_MFront", CONV_PRIORITY_medium)
				wave_2_dialogue_system_status++
				return true
			endif 
		endif 
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
		if not is_entity_in_angled_area(selector_ped.pedID[selector_ped_franklin], <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)
			if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
				wave_2_dialogue_system_status++
				return true
			endif 
		endif 
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		if not is_entity_in_angled_area(selector_ped.pedID[selector_ped_trevor], <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)
			if create_conversation(scripted_speech[0], "heataud", "fbi4_Tfront", CONV_PRIORITY_medium)
				wave_2_dialogue_system_status++
				return true
			endif 
		endif 
	endif 
	
	return false 
	
endfunc 

func bool wave_2_dialogue_system()

	if wave_2_dialogue_system_status < 3
		if wave_3_police_created
			
			if is_this_print_being_displayed("cntry_god31")
				clear_this_print("cntry_god31")
			endif 
			
			wave_2_dialogue_system_status = 3
		endif 
	endif 

	switch wave_2_dialogue_system_status 
	
		case 0
		
			//if police_man[27].created
			if wave_2_police_created 
		
				//area down alley
				if is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)
			
					switch get_current_player_ped_enum()
					
						case char_michael
						
							if create_conversation(scripted_speech[0], "heataud", "fbi4_clearm", CONV_PRIORITY_medium)
								wave_2_dialogue_system_status++
								return true
							endif 
						
						break 
						
						case char_franklin
							
							if create_conversation(scripted_speech[0], "heataud", "fbi4_clearf", CONV_PRIORITY_medium)
								wave_2_dialogue_system_status++
								return true
							endif 
							
						break 
						
					endswitch 
					
				else 
				
					wave_2_dialogue_system_status++ 
				
				endif 
				
			endif 
		
		break 
	
		case 1
		
			if trigger_wave_2_front_dialogue()
				 wave_2_front_dialogue_triggered = true 
			endif 

//			switch get_current_player_ped_enum()
//			
//				case char_michael
//				
//					if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
//						wave_2_dialogue_system_status++
//						return true
//					endif
//				
//				break 
//				
//				case char_franklin
//					
//					if create_conversation(scripted_speech[0], "heataud", "fbi4_MFront", CONV_PRIORITY_medium)
//						wave_2_dialogue_system_status++
//						return true
//					endif
//					
//				break 
//				
//				case char_trevor
//				
//					if create_conversation(scripted_speech[0], "heataud", "fbi4_Tfront", CONV_PRIORITY_medium)
//						wave_2_dialogue_system_status++
//						return true
//					endif 
//				
//				break 
//				
//			endswitch 

		break 
		
		case 2
		
			if not is_any_text_being_displayed(locates_data)
			
				print_now("cntry_god31", default_god_text_time, 1)
				
				set_label_as_triggered("cntry_god31", true)
				
				wave_2_dialogue_system_status++
				
				return true
				
			endif 
		
		break 
		
		case 3

			if not is_any_text_being_displayed(locates_data)
				if create_conversation(scripted_speech[0], "heataud", "fbi4_FrGot", CONV_PRIORITY_medium)
					wave_2_dialogue_system_status++
					return true
				endif
			endif 
		
		break 
		
		case 4
		
		break 
		
	endswitch
	
	return false

endfunc 

func bool are_snipers_in_position()

	if not snipers_in_position
					
		if does_entity_exist(police_man[8].ped)
			if not is_ped_injured(police_man[8].ped)
				if is_entity_at_coord(police_man[8].ped, police_man[8].run_to_pos, <<1.0, 1.0, 2.0>>)			
					snipers_in_position = true
					return true 
				endif 
			endif 
		else 
			if police_man[8].created 
				snipers_in_position = true
				return true
			endif 
		endif 
		
		if does_entity_exist(police_man[9].ped)
			if not is_ped_injured(police_man[9].ped)
				if is_entity_at_coord(police_man[9].ped, police_man[9].run_to_pos, <<1.0, 1.0, 2.0>>)			
					snipers_in_position = true
					return true 
				endif 
			endif
		else 
			if police_man[9].created 
				snipers_in_position = true
				return true
			endif 
		endif 
		
		if does_entity_exist(police_man[31].ped)
			if not is_ped_injured(police_man[31].ped)
				if is_entity_at_coord(police_man[31].ped, police_man[31].run_to_pos, <<1.0, 1.0, 2.0>>)			
					snipers_in_position = true
					return true 
				endif 
			endif
		else 
			if police_man[31].created 
				snipers_in_position = true
				return true
			endif 
		endif 
		
	else 
	
		return true 
	
	endif 
	
	return false 

endfunc 

func int get_number_of_wave_3_snipers_dead()
		
	int dead_count = 0
	
	if does_entity_exist(police_man[8].ped)
		if is_ped_injured(police_man[8].ped)
			dead_count++
		endif 
	else 
		if police_man[8].created
			dead_count++
		endif 
	endif 
	
	if does_entity_exist(police_man[9].ped)
		if is_ped_injured(police_man[9].ped)
			dead_count++
		endif 
	else 
		if police_man[9].created
			dead_count++
		endif 
	endif 
	
	if does_entity_exist(police_man[31].ped)
		if is_ped_injured(police_man[31].ped)
			dead_count++
		endif 
	else 
		if police_man[31].created
			dead_count++
		endif 
	endif 
	
	return dead_count

endfunc 

func bool snipers_dialogue_system()

	if wave_3_police_created

		if not is_ped_injured(police_man[8].ped)
		or not is_ped_injured(police_man[9].ped)
		or not is_ped_injured(police_man[31].ped)
		
			int snipers_dead = get_number_of_wave_3_snipers_dead() 
		
			if not has_label_been_triggered("fbi4_first")
				if snipers_dead = 1
					if sniper_killed_by_trevor
						if create_conversation(scripted_speech[0], "heataud", "fbi4_first", CONV_PRIORITY_medium) 
							set_label_as_triggered("fbi4_first", true)
							return true
						endif 
					endif 
				endif 
			endif 
			
			if not has_label_been_triggered("f_snip_react")
				if has_label_been_triggered("fbi4_first")
					if create_conversation(scripted_speech[0], "heataud", "f_snip_react", CONV_PRIORITY_medium) 
						set_label_as_triggered("f_snip_react", true)
						return true
					endif 
				endif 
			endif 
			
			if not has_label_been_triggered("fbi4_second")
				if snipers_dead = 2
					if sniper_killed_by_trevor
						if create_conversation(scripted_speech[0], "heataud", "fbi4_second", CONV_PRIORITY_medium) 
							set_label_as_triggered("fbi4_second", true)
							return true
						endif 
					endif 
				endif 
			endif 



			if not has_label_been_triggered("t_snip_see")
				if snipers_dialogue_system_status > 1
					if get_current_player_ped_enum() = char_trevor
						if IS_FIRST_PERSON_AIM_CAM_ACTIVE()
							if create_conversation(scripted_speech[0], "heataud", "t_snip_see", CONV_PRIORITY_medium)
								set_label_as_triggered("t_snip_see", true)
								return true 
							endif 
						endif 
					endif 
				endif 
			endif 

			switch snipers_dialogue_system_status 

				case 0

					if are_snipers_in_position()
					//or vehicle exist and recording is > time
					
						if get_current_player_ped_enum() != char_franklin
							
							if create_conversation(scripted_speech[0], "heataud", "f_snipers2", CONV_priority_medium)
								snipers_dialogue_system_status++
								return true
							endif 
							
						else 
						
							if create_conversation(scripted_speech[0], "heataud", "m_snipers2", CONV_PRIORITY_medium) 
								snipers_dialogue_system_status++
								return true
							endif
						
						endif 
						
					endif 

				break 
				
				case 1
				
					if not is_any_text_being_displayed(locates_data)
						print_now("cntry_god32", default_god_text_time, 1)
						snipers_dialogue_system_status++
						return true
					endif 
				
				break 
				
				case 2
				
					if create_conversation(scripted_speech[0], "heataud", "snip_on_roof", CONV_priority_medium) 
						snipers_dialogue_system_status++
						return true
					endif 

				break 
				
				case 3
				
					if get_current_player_ped_enum() != char_franklin

						if create_conversation(scripted_speech[0], "heataud", "f_snip_help", CONV_priority_medium) 
							snipers_dialogue_system_status++
							sniper_dialogue_time = get_game_timer()
							return true
						endif 
						
					else 

						if create_conversation(scripted_speech[0], "heataud", "m_snip_help", CONV_priority_medium) 
							snipers_dialogue_system_status++
							sniper_dialogue_time = get_game_timer()
							return true
						endif 
						
					endif 
				
				break 

				case 4

					if lk_timer(sniper_dialogue_time, 30000)
					
						switch get_current_player_ped_enum() 
						
							case char_michael
							
								if create_conversation(scripted_speech[0], "heataud", "f_snipers", CONV_priority_medium)
									snipers_dialogue_system_status++
									return true
								endif 

							break 
							
							case char_franklin
							
								if create_conversation(scripted_speech[0], "heataud", "m_snipers", CONV_PRIORITY_medium) 
									snipers_dialogue_system_status++
									return true
								endif
							
							break
							
							case char_trevor
							
								switch get_random_int_in_range(0, 2)
								
									case 0
									
										if create_conversation(scripted_speech[0], "heataud", "m_snipers", CONV_PRIORITY_medium) 
											snipers_dialogue_system_status++
											return true
										endif
									
									break 
									
									case 1
									
										if create_conversation(scripted_speech[0], "heataud", "f_snipers", CONV_priority_medium)
											snipers_dialogue_system_status++
											return true
										endif 
									
									break 
									
								endswitch 
							
							break 
						
						endswitch 
				
					endif 

				break 
				
				case 5
				
					if create_conversation(scripted_speech[0], "heataud", "snip_on_roof", CONV_PRIORITY_medium)
						sniper_dialogue_time = get_game_timer()
						snipers_dialogue_system_status = 4
						return true
					endif
				
				break 
				
			endswitch 

		else 
		
			if is_this_print_being_displayed("cntry_god32")
				clear_this_print("cntry_god32")
			endif 
			
			if not has_label_been_triggered("fbi4_gone")
				if create_conversation(scripted_speech[0], "heataud", "fbi4_gone", CONV_PRIORITY_medium) 
					set_label_as_triggered("fbi4_gone", true)
					return true
				endif 
			endif 

		endif 
		
	endif 
	
	return false 

endfunc

func bool wave_4_dialogue_system()

	if wave_4_dialogue_system_status < 2
		if wave_5_police_created
			clear_this_print("cntry_god33")
			wave_4_dialogue_system_status = 2
		endif 
	endif 
				
	switch wave_4_dialogue_system_status
	
		case 0
		
			if police_man[34].created
				if create_conversation(scripted_speech[0], "heataud", "fbi4_micleft", CONV_PRIORITY_medium)
					wave_4_dialogue_system_status++
					return true
				endif 
			endif 
			
		break 
		
		case 1
			
			if not is_any_text_being_displayed(locates_data)
				print_now("cntry_god33", default_god_text_time, 1)
				wave_4_dialogue_system_status++
				return true
			endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch 
	
	return false 

endfunc 

func bool wave_5_dialogue_system()

	if not has_label_been_triggered("fbi4_FrNice")
		if helicopter_driver[1].created
			if not does_entity_exist(helicopter[1].veh)
				if create_conversation(scripted_speech[0], "heataud", "fbi4_FrNice", CONV_PRIORITY_medium)
					clear_this_print("cntry_god34")
					set_label_as_triggered("fbi4_FrNice", true)
					wave_5_dialogue_system_status = 5
				endif 
			endif 
		endif 
	endif 

	switch wave_5_dialogue_system_status
	
		case 0
		
			switch get_current_player_ped_enum()
			
				case char_michael
				case char_franklin
				
					if helicopter_driver[1].created
						if create_conversation(scripted_speech[0], "heataud", "sniper_chopt", CONV_PRIORITY_medium)
							wave_5_dialogue_system_status++
							return true
						endif 
					endif 

				break 
				
				case char_trevor 

					if helicopter_driver[1].created
						if create_conversation(scripted_speech[0], "heataud", "sniper_chopm", CONV_PRIORITY_medium)
							wave_5_dialogue_system_status++
							return true
						endif 
					endif 
				
				break 
				
			endswitch 
		
		break 
		
		case 1
		
			if not is_any_text_being_displayed(locates_data)
				print_now("cntry_god34", default_god_text_time, 1)
				wave_5_dialogue_system_status++
				return true
			endif 
		
		break 
		
		case 2

			if helicopter_driver[1].created	
				if does_entity_exist(helicopter[1].veh)
					if create_conversation(scripted_speech[0], "heataud", "fbi4_FrHeli", CONV_PRIORITY_medium)
						wave_5_dialogue_system_status++
						return true
					endif 
				endif 
			endif 

		break 
		
		case 3
		
			if helicopter_driver[1].created	
				if does_entity_exist(helicopter[1].veh)
				
					switch get_current_player_ped_enum()
					
						case char_michael
						
							if not has_label_been_triggered("sniper_chopf")
								if create_conversation(scripted_speech[0], "heataud", "sniper_chopf", CONV_PRIORITY_medium)
									set_label_as_triggered("sniper_chopf", true)
									return true
								endif 
							endif 
						
						break 
						
						case char_franklin
						
							if not has_label_been_triggered("sniper_chopm")
								if create_conversation(scripted_speech[0], "heataud", "sniper_chopm", CONV_PRIORITY_medium)
									set_label_as_triggered("sniper_chopm", true)
									return true
								endif 
							endif 
						
						break 
						
						case char_trevor
						
							if not has_label_been_triggered("sniper_chopf")
								if create_conversation(scripted_speech[0], "heataud", "sniper_chopm", CONV_PRIORITY_medium)
									set_label_as_triggered("sniper_chopf", true)
									return true
								endif 
							endif 
						
						break 
						
					endswitch
					
				endif 
				
			endif 
		
		break 
		
		case 5
		
		break 

	endswitch 
	
	return false
	
endfunc  

func bool switch_reminder_dialogue_system()
				
	if not switch_reminder

		switch switch_dialogue_status 
		
			case 0
				
				if lk_timer(hot_swap_time, 7000)
					if create_conversation(scripted_speech[0], "heataud", "heat_info0", CONV_PRIORITY_medium) 
						hot_swap_time = get_game_timer()
						switch_dialogue_status++
						return true
					endif 
				endif 
				
			break 
			
			case 1

				if lk_timer(hot_swap_time, 20000)
					if create_conversation(scripted_speech[0], "heataud", "heat_info1", CONV_PRIORITY_medium) 
						hot_swap_time = get_game_timer()
						switch_dialogue_status++
						return true
					endif 
				endif 
				
			break 
		
			case 2
	
//				if lk_timer(hot_swap_time, 30000)
//					if create_conversation(scripted_speech[0], "heataud", "heat_info0", CONV_PRIORITY_medium) 
//						hot_swap_time = get_game_timer()
//						switch_dialogue_status = 1
//					endif 
//				endif 
			
			break 
			
		endswitch 

	endif 
	
	return false

endfunc 



proc dialogue_system()

	if not has_any_selector_ped_been_selected(selector_ped) 
	and not sCamDetails.bSplineActive 
	and player_in_area

		switch dialogue_status
			
			case 0
				
				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech[0], "heataud", "heat_van0", CONV_PRIORITY_medium) 
						
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
						
						//dialogue_status = 22
						dialogue_status++
					endif 
				endif 
				
			break 
			
			case 1

				//allows truck guy dialogue to be hit
//				if not is_any_text_being_displayed(locates_data)
//					dialogue_time = get_game_timer()
//					dialogue_status = 22

				if not has_label_been_triggered("heat_killM")
					if not is_any_text_being_displayed(locates_data)
						
						if (not does_entity_exist(army_truck_guy.ped) and army_truck_guy.created)
						and (not does_entity_exist(army_truck_guy_2.ped) and army_truck_guy_2.created)
							
							if create_conversation(scripted_speech[0], "heataud", "heat_killM", CONV_PRIORITY_medium) 
								set_label_as_triggered("heat_killM", true)
							endif 
							
						endif 
					endif 
				endif 

				if michael_ai_system_status > 3
					if not is_any_text_being_displayed(locates_data)
						if create_conversation(scripted_speech[0], "heataud", "sirens", CONV_PRIORITY_medium) 
							dialogue_status++
						endif 
					endif 
				endif 
			
			break 
			
			case 2

				dialogue_status++
				
			break 
			
			case 3
			
				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech[0], "heataud", "cops_wave_0", CONV_PRIORITY_medium) 
						dialogue_status++
					endif 
				endif 
			
			break 
			
			case 4


				if not is_any_text_being_displayed(locates_data)
					
					if not IS_ENTITY_AT_COORD(player_ped_id(), <<873.3599, -2352.4114, 29.3306>>, <<LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, 2.0>>, false, true)
					or (IS_ENTITY_AT_COORD(player_ped_id(), <<873.3599, -2352.4114, 29.3306>>, <<LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, 2.0>>, false, true) and not is_ped_in_cover(player_ped_id()))
					
						if create_conversation(scripted_speech[0], "heataud", "heat_cover", CONV_PRIORITY_medium)
							dialogue_status++
						endif 
						
					else 
					
						if create_conversation(scripted_speech[0], "heataud", "heat_cover2", CONV_PRIORITY_medium)  
							dialogue_status++
						endif 
						
					endif 
					
				endif 
				
			break 
			
			case 5
				
				if create_conversation(scripted_speech[0], "heataud", "get_ready_0", CONV_PRIORITY_medium)
					dialogue_status++
				endif 


			break 
			
			case 6

				dialogue_status++

			break 

			case 7

				if not is_any_text_being_displayed(locates_data)
					if create_conversation(scripted_speech[0], "heataud", "get_ready_1", CONV_PRIORITY_medium)
						dialogue_status++
					endif 
				endif 
				
				
			break 
			
			case 8
			
				if allow_hotswap
					if not is_any_text_being_displayed(locates_data)
				
						clear_prints()
						print_now("cntry_god9", default_god_text_time, 1)
						
						dialogue_status++
						
					endif 
				endif 
				
			break 
			
			case 9
			

			break 
			
			case 10

			break 
			
			case 11


			break 
			
			case 22
			
			break 
			
		endswitch
		
		//once the main shootout dialogue has been triggered start specific lines to trigger
		if dialogue_status > 8
		
			if not is_any_text_being_displayed(locates_data)
				
				//make functions return true so that you can exit out once any text of dialogue has triggered
				//this will stop god text being overwritten by dialogue in the bellow calls e.g. instruct_1
				//All dialogue wrapped in is_any_text_being_displayed so multiple lines could be overwritten
				//as the check is only done once.
				if left_alley_dialogue_system()
					exit
				endif 
				
				if wave_2_dialogue_system()
					exit
				endif 

				if snipers_dialogue_system()	
					exit
				endif 

				if switch_reminder_dialogue_system()
					exit
				endif 
				
				if wave_4_dialogue_system()
					exit
				endif 
		
				if wave_5_dialogue_system()
					exit
				endif 
				
				if not has_label_been_triggered("instruct_1")
					if create_conversation(scripted_speech[0], "heataud", "instruct_1", CONV_PRIORITY_medium)
						set_label_as_triggered("instruct_1", true)
					endif
				endif 
						
				if not has_label_been_triggered("trevor_rpg")  //trevor_rpg
					if allow_hotswap
						if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
							if has_ped_got_weapon(selector_ped.pedID[selector_ped_trevor], weapontype_rpg)
								if create_conversation(scripted_speech[0], "heataud", "trevor_rpg", CONV_priority_medium)
									set_label_as_triggered("trevor_rpg", true)
								endif 
							endif 
						endif 
					endif 
				endif 
				

				if not has_label_been_triggered("rpg_M_respon")
					if get_current_player_ped_enum() = char_michael
						if is_explosion_in_sphere(exp_tag_rocket, rpg_target_pos, 10.0)
							if create_conversation(scripted_speech[0], "heataud", "rpg_M_respon", CONV_PRIORITY_medium) 
								set_label_as_triggered("rpg_M_respon", true)
							endif 
						endif 
					endif 
				endif 
				
				if not has_label_been_triggered("rpg_f_respon")
					if get_current_player_ped_enum() = char_franklin
						if is_explosion_in_sphere(exp_tag_rocket, rpg_target_pos, 10.0)
							if create_conversation(scripted_speech[0], "heataud", "rpg_f_respon", CONV_PRIORITY_medium) 
								set_label_as_triggered("rpg_f_respon", true)
							endif 
						endif 
					endif 
				endif 
				

				if not has_label_been_triggered("all_out")
					if get_current_player_ped_enum() = char_trevor
						if not has_ped_got_weapon(player_ped_id(), weapontype_rpg)
							if create_conversation(scripted_speech[0], "heataud", "all_out", CONV_priority_medium)
								set_label_as_triggered("all_out", true)
							endif 
						endif 
					endif 
				endif 
				
			endif 
			
			if not has_label_been_triggered("fbi4_norock")
				if get_current_player_ped_enum() = char_trevor
					if not has_ped_got_weapon(player_ped_id(), weapontype_rpg)
						if create_conversation(scripted_speech[0], "heataud", "fbi4_norock" , CONV_priority_low)
							set_label_as_triggered("fbi4_norock", true)
							dialogue_time = get_game_timer()
						endif 
					endif
				endif 
			endif 
			
		endif 
		
		if allow_hotswap
		
			int num_cops_dead = (total_wanted_level_stat_count() - original_cops_killed)
		
			if num_cops_dead > cops_killed_during_shootout
				cops_killed_during_shootout = num_cops_dead
				cop_dead_stat_time = get_game_timer()
			endif 
		
			if lk_timer(dialogue_time, 7000)
				if is_ped_shooting(player_ped_id())
					if not is_any_text_being_displayed(locates_data)
						
						switch get_current_player_ped_enum() 
						
							case char_michael
					
								if create_conversation(scripted_speech[0], "heataud", "heat_M_att0" , CONV_priority_low)
									dialogue_time = get_game_timer()
								endif 
								
							break 
							
							case char_franklin
							
								if (get_game_timer() - cop_dead_stat_time) < 1000
								
									if create_conversation(scripted_speech[0], "heataud", "fbi4_FShoot1" , CONV_priority_low)
										dialogue_time = get_game_timer()
									endif 
									
								else 
							
									if create_conversation(scripted_speech[0], "heataud", "heat_F_att0" , CONV_priority_low)
										dialogue_time = get_game_timer()
									endif 
										
								endif 
							
							break 
							
							case char_trevor
							
								weapon_type players_weapon
							
								get_current_ped_weapon(player_ped_id(), players_weapon)
								
								if get_entity_height_above_ground(player_ped_id()) > 10
								
									if players_weapon = weapontype_rpg
			
										if create_conversation(scripted_speech[0], "heataud", "heat_T_att0" , CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 

									else
			
										if create_conversation(scripted_speech[0], "heataud", "fbi4_rattack" , CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									
									endif 
									
								endif 
							
							break 
							
						endswitch 
						
					endif 
				
				else
				
					if not is_any_text_being_displayed(locates_data)
					
						switch get_current_player_ped_enum() 
						
							case char_michael
							
								if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
									if is_ped_shooting(selector_ped.pedID[selector_ped_trevor])
										if create_conversation(scripted_speech[0], "heataud", "heat_t_att0", CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									endif 
								endif 

								if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
									if is_ped_shooting(selector_ped.pedID[selector_ped_franklin])
										if create_conversation(scripted_speech[0], "heataud", "heat_F_att0", CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									endif 
								endif 
							
							break 
							
							case char_franklin
							
								if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
									if is_ped_shooting(selector_ped.pedID[selector_ped_michael])
										if create_conversation(scripted_speech[0], "heataud", "heat_M_att0", CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									endif 
								endif 

								if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
									if is_ped_shooting(selector_ped.pedID[selector_ped_trevor])
										if create_conversation(scripted_speech[0], "heataud", "heat_t_att0", CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									endif 
								endif
			
							break 
							
							case char_trevor
							
								if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
									if is_ped_shooting(selector_ped.pedID[selector_ped_michael])
										if create_conversation(scripted_speech[0], "heataud", "heat_M_att0", CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									endif 
								endif 
								
								if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
									if is_ped_shooting(selector_ped.pedID[selector_ped_franklin])
										if create_conversation(scripted_speech[0], "heataud", "fbi4_FShoot1", CONV_priority_low)
											dialogue_time = get_game_timer()
										endif 
									endif 
								endif 
							
							break 
							
						endswitch 
						
					endif 
						
				endif 
				
			endif 
		
		endif 
		
	else 
	
		if not player_in_area
			if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF 
		endif 
		
	endif 
	
endproc 

proc cover_blip_system()
				
	//adds blip once Trevor tells the player the cops are coming. Handles situation if player moves out of the area
	//just as the dialogue is triggered.
	
	if shootout_master_controler_status > 0
		
		IF IS_ENTITY_AT_COORD(player_ped_id(), <<873.3599, -2352.4114, 29.3306>>, <<LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, 2.0>>, false, true)
		and is_ped_in_cover(player_ped_id())	
			
			if does_blip_exist(player_cover_blip)
				remove_blip(player_cover_blip)
			endif 
			
			if is_this_print_being_displayed("cntry_god12")
				clear_this_print("cntry_god12")
			endif 

		else 
			
			if not player_told_to_get_into_cover
				if not does_blip_exist(player_cover_blip)
					
					player_cover_blip = create_blip_for_coord(<<873.3599, -2352.4114, 29.3306>>)

					player_told_to_get_into_cover = true 

				endif 
			endif 
			
		endif
	endif 

endproc 

proc block_specific_dispatch(bool block_dispatch)
			
	if block_dispatch
	
		enable_dispatch_service(dt_police_automobile, false)
		enable_dispatch_service(dt_police_helicopter, false)

		BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, true)
	    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, true)

	else 
	
		enable_dispatch_service(dt_police_automobile, true)
		enable_dispatch_service(dt_police_helicopter, true)

		BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, false)
	    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, false)
	
	endif 

endproc 
	
proc update_dispatch_pos()

	switch update_dispatch_pos_status 
	
		case 0
		
//			SET_DISPATCH_SPAWN_LOCATION(<<1047.6, -2272.6, 30.4>>)
//			update_dispatch_pos_status++
		
			SET_DISPATCH_SPAWN_LOCATION(<<910.4, -2354.1, 30.00>>)
			update_dispatch_pos_status = 3
		break 
		
		case 1
		
			SET_DISPATCH_SPAWN_LOCATION(<<988.7, -2085.8, 30.9>>)
			update_dispatch_pos_status++
		
		break 
		
		case 2
		
			SET_DISPATCH_SPAWN_LOCATION(<<1034.0, -2451.9, 28.2>>)
			update_dispatch_pos_status = 0
		
		break 
		
	endswitch 

endproc 
	
proc dispatch_spawn_position_system()

	switch dispatch_spawn_position_system_status
	
		case 0
		
			update_dispatch_pos()
			
			dispatch_time = get_game_timer()
			
			dispatch_spawn_position_system_status++
		
		break 

		case 1
			
			if lk_timer(dispatch_time, 5000)
				
				update_dispatch_pos()
				
				block_specific_dispatch(true)
				
				dispatch_time = get_game_timer()
				
				dispatch_spawn_position_system_status++
			endif 
		
		break 
		
		case 2
		
			if lk_timer(dispatch_time, 1000)
			
				block_specific_dispatch(false)
				
				dispatch_time = get_game_timer()
				
				dispatch_spawn_position_system_status = 1
			
			endif 
		
		break 
	
	endswitch 
	
endproc 

proc setup_dispatch_data()

	disable_dispatch_services()
			
	dispatch_spawn_position_system()

	SET_DISPATCH_IDEAL_SPAWN_DISTANCE(125.00) 
	
	//wharehouse area in front of player
	ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<746.643, -1934.229, 0.343>>, <<685.267, -2731.871, 130.343>>, 362.5)
	
	//area behind player in cover
	ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<996.284, -2103.281, 0.343>>, <<967.615, -2445.886, 130.343>>, 111.100)
	
	enable_dispatch_service(dt_police_automobile, true)
	enable_dispatch_service(dt_police_helicopter, true)

	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, false)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, false)

endproc 

//cache current wanted level for replay fbi4_replay_shootout_wanted_level_star = 4
//in replay function
//proc set_wanted_level_star_for_mission()
//
//	switch fbi4_replay_shootout_wanted_level_star //make a global
//
//		case 1
//		
//			setup_dispatch_data()
//			
//			set_max_wanted_level(1)
//			set_player_wanted_level(player_id(), 1)
//			set_player_wanted_level_now(player_id())
//			
//			original_cops_killed = total_wanted_level_stat_count()
//			
//			SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 4)
//
//			shootout_time = get_game_timer()
//			
//			shootout_wanted_level_system_status = 4
//		
//		break 
//		
//		case 2
//		
//			setup_dispatch_data()
//			
//			set_max_wanted_level(2)
//			set_player_wanted_level(player_id(), 2)
//			set_player_wanted_level_now(player_id())
//
//			original_cops_killed = total_wanted_level_stat_count()
//			
//			SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 4)
//
//			shootout_time = get_game_timer()
//			
//			shootout_wanted_level_system_status = 3
//		
//		break 
//		
//		case 3
//		
//			setup_dispatch_data()
//			
//			set_max_wanted_level(3)
//			set_player_wanted_level(player_id(), 3)
//			set_player_wanted_level_now(player_id())
//			
//			original_cops_killed = total_wanted_level_stat_count()
//			
//			SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 8)
//
//			shootout_time = get_game_timer()
//					
//			shootout_wanted_level_system_status = 2
//		
//		break 
//		
//		case 4
//		
//			//do nothing
//	//		trigger mission as normal starting from a wanted level of 4
//		
//		break 
//	
//	endswitch 
//	
//endproc 
	


func bool shootout_wanted_level_system()

	switch shootout_wanted_level_system_status 
	
		case 0
		
			disable_dispatch_services()
			
			dispatch_spawn_position_system()
		
			SET_DISPATCH_IDEAL_SPAWN_DISTANCE(125.00) //50.00
			
			//wharehouse area in front of player
			ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<746.643, -1934.229, 0.343>>, <<685.267, -2731.871, 130.343>>, 362.5)
			
			//area behind player in cover
			ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<996.284, -2103.281, 0.343>>, <<967.615, -2445.886, 130.343>>, 111.100)
			
			enable_dispatch_service(dt_police_automobile, true)
//			enable_dispatch_service(dt_police_helicopter, true)

			BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, false)
//		    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, false)


			set_fake_wanted_level(0)
			set_max_wanted_level(4)
			set_create_random_cops(true)
			set_wanted_level_multiplier(1.0)
			
			set_player_wanted_level(player_id(), 4)
			set_player_wanted_level_now(player_id())
			
			SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 12)
			//SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(dt_police_helicopter, 3) 
			
			original_cops_killed = total_wanted_level_stat_count()

			shootout_time = get_game_timer()
			
//			fbi4_replay_shootout_wanted_level_star = 4
			
			shootout_wanted_level_system_status++
		
		break 
		
		case 1
			
			dispatch_spawn_position_system()
			
//			printint(get_game_timer() - shootout_time)
//			printnl()
			
			cops_killed_during_shootout = (total_wanted_level_stat_count() - original_cops_killed)
			
			printstring("cops killed: ")
			printint(cops_killed_during_shootout)
			printnl()
		
			if not lk_timer(shootout_time, 120000) and not (cops_killed_during_shootout > 12)
				
				set_player_wanted_level(player_id(), 4)
				set_player_wanted_level_now(player_id())

//				if not police_car_created
//					if lk_timer(original_time, 22000)
//						police_car_creation()
//						police_car_created = true
//					endif 
//				endif 
					
			else 

				set_max_wanted_level(3)
				set_player_wanted_level(player_id(), 3)
				set_player_wanted_level_now(player_id())
				
				original_cops_killed = total_wanted_level_stat_count()
				
				SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 8)

				shootout_time = get_game_timer()
				
//				fbi4_replay_shootout_wanted_level_star = 3
				
				shootout_wanted_level_system_status++
				
			endif 
		
		break 
		
		case 2
		
			dispatch_spawn_position_system()
		
			cops_killed_during_shootout = (total_wanted_level_stat_count() - original_cops_killed)
		
			if not lk_timer(shootout_time, 60000) and not (cops_killed_during_shootout > 10)
			
				set_max_wanted_level(3)
				set_player_wanted_level(player_id(), 3)
				set_player_wanted_level_now(player_id())
				
			else 
			
				set_max_wanted_level(2)
				set_player_wanted_level(player_id(), 2)
				set_player_wanted_level_now(player_id())

				original_cops_killed = total_wanted_level_stat_count()
				
				SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 4)

				shootout_time = get_game_timer()
				
//				fbi4_replay_shootout_wanted_level_star = 2
				
				shootout_wanted_level_system_status++
				
			endif 

		break 
		
		case 3
		
			dispatch_spawn_position_system()
		
			cops_killed_during_shootout = (total_wanted_level_stat_count() - original_cops_killed)
		
			if not lk_timer(shootout_time, 12000) and not (cops_killed_during_shootout > 7)
			
				set_max_wanted_level(2)
				set_player_wanted_level(player_id(), 2)
				set_player_wanted_level_now(player_id())
				
			else 

				set_max_wanted_level(1)
				set_player_wanted_level(player_id(), 1)
				set_player_wanted_level_now(player_id())
				
				original_cops_killed = total_wanted_level_stat_count()
				
				SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE, 4)

				shootout_time = get_game_timer()
				
//				fbi4_replay_shootout_wanted_level_star = 1
				
				shootout_wanted_level_system_status++
			
			endif 
			
		break 
		
		case 4
		
			dispatch_spawn_position_system()
		
			cops_killed_during_shootout = (total_wanted_level_stat_count() - original_cops_killed)
		
			#if is_debug_build 
				printstring("cops_killed_during_shootout: ")
				printint(cops_killed_during_shootout)
				printnl()
			#endif 
		
			if not lk_timer(shootout_time, 10000) and not (cops_killed_during_shootout > 14)
			
				set_max_wanted_level(1)
				set_player_wanted_level(player_id(), 1)
				set_player_wanted_level_now(player_id())
				
			else 
			
				set_create_random_cops(false)
				set_max_wanted_level(1)
				set_player_wanted_level(player_id(), 1)
				set_player_wanted_level_now(player_id())
				
//				clear_player_wanted_level(player_id())
//				set_max_wanted_level(0)
//				set_create_random_cops(false)
//				set_fake_wanted_level(1)
			
				disable_dispatch_services()
				
				BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, true)
			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, true)
			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_AUTOMOBILE, true)
				BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER, true)
			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_VEHICLE_REQUEST, true)
			   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_ROAD_BLOCK, true)
			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, true)
			   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, true)
				
				original_cops_killed = total_wanted_level_stat_count()

				shootout_time = get_game_timer()
				
				shootout_wanted_level_system_status++
				
				return true 
				
			endif 

		break 
		
		case 5

//			set_create_random_cops(false)
//			set_max_wanted_level(0)
//			set_player_wanted_level_now(player_id())
//			
//			clear_player_wanted_level(player_id())

			return true 
		
//			if not lk_timer(shootout_time, 3000)  
//			
//				set_max_wanted_level(1)
//				set_player_wanted_level(player_id(), 1)
//				set_player_wanted_level_now(player_id())
//				
//			else 
//			
//				disable_dispatch_services()
//				
//				BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, true)
//			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, true)
//			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_AUTOMOBILE, true)
//			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_VEHICLE_REQUEST, true)
//			   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_ROAD_BLOCK, true)
//			    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, true)
//			   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, true)
//
//				clear_player_wanted_level(player_id())
//				set_max_wanted_level(0)
//				set_create_random_cops(false)
//				
//				return true 
//			
//			endif 
		
		break 
		
	endswitch 
	
	return false 
	
endfunc

proc head_tracking_guards_system()

	if is_entity_in_angled_area(player_ped_id(), <<893.272, -2360.499, 29.293>>, <<886.701, -2359.877, 31.893>>, 5.5)  
	
		if does_entity_exist(army_truck_guy.ped) or does_entity_exist(army_truck_guy_2.ped)
			if not is_ped_injured(army_truck_guy.ped) or not is_ped_injured(army_truck_guy_2.ped)

				if not is_ped_injured(army_truck_guy.ped)
					if get_script_task_status(player_ped_id(), SCRIPT_TASK_LOOK_AT_ENTITY) != performing_task
						task_look_at_entity(player_ped_id(), army_truck_guy.ped, 2000)
					endif 
					
				elif not is_ped_injured(army_truck_guy_2.ped)
					if get_script_task_status(player_ped_id(), SCRIPT_TASK_LOOK_AT_ENTITY) != performing_task
						task_look_at_entity(player_ped_id(), army_truck_guy_2.ped, 2000)
					endif 
				endif 
				
			endif 
			
		else
			
			//army_truck_guy_2 AI system marks peds as nolonger needed once they are killed or run away
			if get_script_task_status(player_ped_id(), SCRIPT_TASK_LOOK_AT_ENTITY) = performing_task
				task_clear_look_at(player_ped_id())
			endif 

		endif 
		
	else 

		if get_script_task_status(player_ped_id(), SCRIPT_TASK_LOOK_AT_ENTITY) = performing_task
			task_clear_look_at(player_ped_id())
		endif
		
	endif 

endproc

///PURPOSE: creates the alleyway cops if the player kills >= 17 guys or takes out the 2 snipers on the roof.
///    
///	NOTES:
proc create_alley_cops()

	if not police_man[12].created
		
		if ped_structure_get_total_number_of_enemies_dead(police_man) >= 17
		or (police_man[8].created and police_man[9].created and is_ped_injured(police_man[8].ped) and is_ped_injured(police_man[9].ped))
		
			//alley cops
			police_car[11].veh = create_vehicle(police_car[11].model, police_car[11].pos, police_car[11].heading)
			set_entity_only_damaged_by_player(police_car[11].veh, true)
			set_vehicle_siren(police_car[11].veh, true)
			start_playback_recorded_vehicle(police_car[11].veh, police_car[11].recording_number, "lkfbi4") 

			setup_enemy_in_vehicle(police_man[12], police_car[11].veh) 
			setup_relationship_enemy(police_man[12].ped, true)
			remove_blip(police_man[12].blip)
	
			setup_enemy_in_vehicle(police_man[13], police_car[11].veh, vs_front_right)
			setup_relationship_enemy(police_man[13].ped, true)
			remove_blip(police_man[13].blip)
			
		endif 
		
	endif 
	
endproc 

//func bool is_ped_in_wave_1_alive_array(ped_index &mission_ped)
//
//	int i
//					
//	if wave_0_alive_count > 0
//		
//		for i = 0 to (wave_0_alive_count - 1)
//		
//			if wave_0_alive_police[i] = mission_ped
//				return true
//			endif 
//			
//		endfor 
//		
//	endif 
//			
//	return false 
//
//endfunc

func bool create_wave_1_police()

	switch create_wave_1_police_status 
	
		case 0
		
			setup_enemy(police_man[12]) 
			setup_relationship_enemy(police_man[12].ped, true)
			
			create_wave_1_police_status++
		
		break 
		
		case 1
		
			setup_enemy(police_man[13])
			setup_relationship_enemy(police_man[13].ped, true)
			
			create_wave_1_police_status++
		
		break 
		
		case 2
		
			setup_enemy(police_man[16])
			setup_relationship_enemy(police_man[16].ped, true)
			
			create_wave_1_police_status++
		
		break 
		
		case 3
		
			setup_enemy(police_man[17])
			setup_relationship_enemy(police_man[17].ped, true)
			
			create_wave_1_police_status++
		
		break 
		
		case 4
		
			setup_enemy(police_man[18])
			setup_relationship_enemy(police_man[18].ped, true)

			create_wave_1_police_status++
		
			return true 
			
		break 
		
		case 5
		
			return true 
		
		break 
		
	endswitch 

	return false 

endfunc 

func bool create_wave_2_police()

	switch create_wave_2_police_status 
	
		case 0

			police_car[4].veh = create_vehicle(police_car[4].model, police_car[4].pos, police_car[4].heading)
			set_entity_only_damaged_by_player(police_car[4].veh, true)
			set_vehicle_siren(police_car[4].veh, true)
			start_playback_recorded_vehicle(police_car[4].veh, police_car[4].recording_number, "lkfbi4") 
		
			setup_enemy_in_vehicle(police_man[27], police_car[4].veh) 
			setup_relationship_enemy(police_man[27].ped, true)
			
			setup_enemy_in_vehicle(police_man[28], police_car[4].veh, vs_front_right)
			setup_relationship_enemy(police_man[28].ped, true)
			
			create_wave_2_police_status++

		break 
		
		case 1
		
			setup_enemy(police_man[29])
			setup_relationship_enemy(police_man[29].ped, true)
			
			create_wave_2_police_status++

		break 
		
		case 2
		
			setup_enemy(police_man[30])
			setup_relationship_enemy(police_man[30].ped, true)
			
			create_wave_2_police_status++
			
			return true 
		
		break 
		
		case 3
		
			return true 
		
		break 
		
	endswitch 
	
	return false 
	
endfunc 

proc stop_all_audio_scenes_after_shootout()

	if IS_AUDIO_SCENE_ACTIVE("FBI_4_SHOOTOUT_MICHAEL")
		STOP_AUDIO_SCENE("FBI_4_SHOOTOUT_MICHAEL")
	endif		
			
	if IS_AUDIO_SCENE_ACTIVE("FBI_4_SHOOTOUT_FRANKLIN")
		STOP_AUDIO_SCENE("FBI_4_SHOOTOUT_FRANKLIN")
	endif

	if IS_AUDIO_SCENE_ACTIVE("FBI_4_SHOOTOUT_TREVOR")
		STOP_AUDIO_SCENE("FBI_4_SHOOTOUT_TREVOR")
	endif
	
endproc 


func bool shootout_master_controler_system()

	int i = 0

	#if is_debug_build
		widget_total_number_of_cops_dead = ped_structure_get_total_number_of_enemies_dead(police_man)
	#endif

	switch shootout_master_controler_status 
	
		case 0
			
			head_tracking_guards_system()
		
			if michael_ai_system_status > 3
			
				//PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_FIB_4_01", 0.0)

				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(army_truck.veh)
			
				DISTANT_COP_CAR_SIRENS(true)
				PLAY_SOUND_FROM_coord(police_siren_sound, "Distant_Sirens", <<913.6, -2301.6, 34.00>>, "FBI_04_HEAT_SOUNDS") 
			
				task_clear_look_at(player_ped_id())
			
				if is_vehicle_driveable(army_truck.veh)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(army_truck.veh, true)
				endif 

				//sets up buddies max health
				SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped_michael], 4000)
				SET_PED_MAX_HEALTH_WITH_SCALE(selector_ped.pedID[selector_ped_trevor], 4000)


				michael_ai_system_status = 4
				
				original_time = get_game_timer()
				shootout_time = get_game_timer()
				//random_time = get_random_int_in_range(7, 9) //14, 16
				//random_time *= 1000

				
				if does_entity_exist(army_truck_driver.ped)
					set_ped_as_no_longer_needed(army_truck_driver.ped)
				endif 
				set_model_as_no_longer_needed(army_truck_driver.model)
				
				
				if does_entity_exist(army_truck_passenger.ped)
					set_ped_as_no_longer_needed(army_truck_passenger.ped)
				endif 
				set_model_as_no_longer_needed(army_truck_passenger.model)
				
				remove_ptfx_asset()
				
				remove_anim_dict("missfbi4")
				remove_anim_dict("misssagrab") 
				remove_anim_dict("missheat") 
				
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING) 
				
				shootout_master_controler_status++ 
				
				Set_Replay_Mid_Mission_Stage_With_Name(3, "Start of Shootout")
			endif 
		
		break 
	
		case 1
		
			request_model(police_man[0].model)
			request_model(police_man[8].model)
			request_model(police_car[0].model)
			request_model(helicopter[0].model)
			request_vehicle_asset(police_car[0].model, enum_to_int(VRF_REQUEST_ALL_ANIMS))
			request_vehicle_recording(helicopter[0].recording_number, "lkfbi4")
			request_vehicle_recording(helicopter[1].recording_number, "lkfbi4")
			request_vehicle_recording(police_car[0].recording_number, "lkfbi4") 
			request_vehicle_recording(police_car[1].recording_number, "lkfbi4") 
			request_vehicle_recording(police_car[2].recording_number, "lkfbi4") 
			request_vehicle_recording(police_car[3].recording_number, "lkfbi4") 
			request_vehicle_recording(police_car[4].recording_number, "lkfbi4") 
			request_vehicle_recording(police_car[5].recording_number, "lkfbi4")

			//cover_blip_system()
			
			if player_in_area
				
				if has_model_loaded(police_man[0].model)
				and has_model_loaded(police_man[8].model)
				and has_model_loaded(police_car[0].model)
				and has_model_loaded(helicopter[0].model)
				and has_vehicle_asset_loaded(police_car[0].model)
				and has_vehicle_recording_been_loaded(helicopter[0].recording_number, "lkfbi4")
				and has_vehicle_recording_been_loaded(helicopter[1].recording_number, "lkfbi4")
				and has_vehicle_recording_been_loaded(police_car[0].recording_number, "lkfbi4") 
				and has_vehicle_recording_been_loaded(police_car[1].recording_number, "lkfbi4") 
				and has_vehicle_recording_been_loaded(police_car[2].recording_number, "lkfbi4") 
				and has_vehicle_recording_been_loaded(police_car[3].recording_number, "lkfbi4") 
				and has_vehicle_recording_been_loaded(police_car[4].recording_number, "lkfbi4") 
				and has_vehicle_recording_been_loaded(police_car[5].recording_number, "lkfbi4")
//				
					if lk_timer(shootout_time, 10000)//random_time) 10000 12000
					
						original_cops_killed = total_wanted_level_stat_count()
					
						//stops any random cars being created - don't trust switch roads off
						SET_VEHICLE_POPULATION_BUDGET(0)
						SET_PED_POPULATION_BUDGET(0)

						clear_player_wanted_level(player_id())
						//g_allowmaxwantedlevelcheck = false
						set_max_wanted_level(0)
						set_create_random_cops(false)
						set_fake_wanted_level(4)
							
						police_car[0].veh = create_vehicle(police_car[0].model, police_car[0].pos, police_car[0].heading)
						set_entity_only_damaged_by_player(police_car[0].veh, true)
						set_vehicle_siren(police_car[0].veh, true)
						start_playback_recorded_vehicle(police_car[0].veh, police_car[0].recording_number, "lkfbi4") 
						
						police_car[2].veh = create_vehicle(police_car[2].model, police_car[2].pos, police_car[2].heading)
						set_entity_only_damaged_by_player(police_car[2].veh, true)
						set_vehicle_siren(police_car[2].veh, true)
						start_playback_recorded_vehicle(police_car[2].veh, police_car[2].recording_number, "lkfbi4") 
					
						setup_enemy_in_vehicle(police_man[0], police_car[0].veh) 
						setup_relationship_enemy(police_man[0].ped, true)
						setup_enemy_in_vehicle(police_man[1], police_car[0].veh, vs_front_right)
						setup_relationship_enemy(police_man[1].ped, true)
					
						setup_enemy_in_vehicle(police_man[4], police_car[2].veh) 
						setup_relationship_enemy(police_man[4].ped, true)
						setup_enemy_in_vehicle(police_man[5], police_car[2].veh, vs_front_right)
						setup_relationship_enemy(police_man[5].ped, true)
						
						original_cops_killed = total_wanted_level_stat_count()

						shootout_time = get_game_timer()
//
						shootout_master_controler_status++
//						
					endif 
						
				endif
				
			endif 

		break 
		
		case 2
		
			if lk_timer(shootout_time, 1500)
						
				police_car[1].veh = create_vehicle(police_car[1].model, police_car[1].pos, police_car[1].heading)
				set_entity_only_damaged_by_player(police_car[1].veh, true)
				set_vehicle_siren(police_car[1].veh, false)
				start_playback_recorded_vehicle(police_car[1].veh, police_car[1].recording_number, "lkfbi4") 
				set_playback_speed(police_car[1].veh, 0.9)
				
				police_car[3].veh = create_vehicle(police_car[3].model, police_car[3].pos, police_car[3].heading)
				set_entity_only_damaged_by_player(police_car[3].veh, true)
				set_vehicle_siren(police_car[3].veh, false)
				start_playback_recorded_vehicle(police_car[3].veh, police_car[3].recording_number, "lkfbi4") 

				setup_enemy_in_vehicle(police_man[2], police_car[1].veh) 
				setup_relationship_enemy(police_man[2].ped, true)
				setup_enemy_in_vehicle(police_man[3], police_car[1].veh, vs_front_right)
				setup_relationship_enemy(police_man[3].ped, true)
				
				setup_enemy_in_vehicle(police_man[6], police_car[3].veh) 
				setup_relationship_enemy(police_man[6].ped, true)
				setup_enemy_in_vehicle(police_man[7], police_car[3].veh, vs_front_right)
				setup_relationship_enemy(police_man[7].ped, true)
				
				shootout_master_controler_status++
				
			endif 
		
		break 
		
		case 3
		
			//cover_blip_system()
		
			if lk_timer(shootout_time, 10000) 
			//or (is_entity_in_angled_area(selector_ped.pedID[selector_ped_michael], <<877.527, -2334.883, 33.913>>, <<877.757, -2332.192, 36.913>>, 8.8) and is_ped_in_cover(player_ped_id()))

				REPLAY_RECORD_BACK_FOR_TIME(3)
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_PREP_FOR_COPS")
					STOP_AUDIO_SCENE("FBI_4_PREP_FOR_COPS")
				endif 
				
				start_audio_scene("FBI_4_COPS_ARRIVE")
				
				start_audio_scene("FBI_4_SHOOTOUT_FRANKLIN")
				
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(selector_ped.pedID[selector_ped_michael], "FBI_4_BUDDIES_GROUP")
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(selector_ped.pedID[selector_ped_trevor], "FBI_4_BUDDIES_GROUP")
			
				SET_PED_USING_ACTION_MODE(player_ped_id(), true)
				SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_michael], true)
				SET_PED_USING_ACTION_MODE(selector_ped.pedID[selector_ped_trevor], true)

				if not has_sound_finished(police_siren_sound)
					stop_sound(police_siren_sound)
				endif 
				
				DISTANT_COP_CAR_SIRENS(false) 
				
				SET_SELECTOR_PED_PRIORITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_PED_trevor, selector_ped_franklin)
				
				allow_hotswap = true
				hot_swap_time = get_game_timer()
				
				trevor_time = get_game_timer()
				
				shootout_master_controler_status++
				
			endif 
		
		break 
		
		case 4
		
			if lk_timer(hot_swap_time, 2000)
			
				print_help("cntry_help5", 13000)
				
				shootout_time = get_game_timer()
				
				shootout_master_controler_status++
				
			endif 
		
		break 
		
		case 5
		
			if player_in_area
			
				if ped_structure_get_total_number_of_enemies_dead(police_man) >= 1
				or lk_timer(shootout_time, 5000)
				//or player runs out of angled area
				
					setup_enemy(police_man[14]) 
					setup_relationship_enemy(police_man[14].ped, true)
					
					setup_enemy(police_man[15])
					setup_relationship_enemy(police_man[15].ped, true)
					
					shootout_time = get_game_timer()
							
					shootout_master_controler_status++
					
				endif 
			
			endif 
		
		break 
		
		case 6
		
			if ped_structure_get_total_number_of_enemies_dead(police_man) >= 7
			
				if create_wave_1_police()
				
					SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
					SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
					SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_MICHAEL, TRUE)
					SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_trevor, selector_ped_franklin)

					wave_1_police_created = true
				
					wave_1_timer_0 = get_game_timer()
					
					shootout_master_controler_status++
					
				endif 
				
			endif 
		
		break 
		
		case 7
		
			//if not make_wave_1_peds_advance()
				backup_police_front_spawn_system()
			//endif 

			wave_1_alley_police_dead_count = 0
		
			for i = 12 to 13
				if is_ped_injured(police_man[i].ped)
					wave_1_alley_police_dead_count++
				endif 
			endfor 
			
			for i = 16 to 18 //20
				if is_ped_injured(police_man[i].ped)
					wave_1_alley_police_dead_count++
				endif 
			endfor
			
			if wave_1_alley_police_dead_count >= 1
			
				setup_enemy(police_man[19])
				setup_relationship_enemy(police_man[19].ped, true)
				
				helicopter[0].veh = create_vehicle(helicopter[0].model, helicopter[0].pos, helicopter[0].heading)
				set_vehicle_livery(helicopter[0].veh, 0)
				set_heli_blades_full_speed(helicopter[0].veh)
				start_playback_recorded_vehicle(helicopter[0].veh, helicopter[0].recording_number, "lkfbi4")
				skip_time_in_playback_recorded_vehicle(helicopter[0].veh, 5000)
				set_playback_speed(helicopter[0].veh, 2.2)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(helicopter[0].veh)
				
				setup_enemy_in_vehicle(helicopter_driver[0], helicopter[0].veh) 
				setup_relationship_enemy(helicopter_driver[0].ped, true)
				
				setup_enemy_in_vehicle(police_man[21], helicopter[0].veh, vs_back_left)  
				setup_relationship_enemy(police_man[21].ped, true)
				ADD_ARMOUR_TO_PED(police_man[21].ped, -GET_PED_ARMOUR(police_man[21].ped))
				
				setup_enemy_in_vehicle(police_man[22], helicopter[0].veh, vs_back_right)  
				setup_relationship_enemy(police_man[22].ped, true)
				ADD_ARMOUR_TO_PED(police_man[22].ped, -GET_PED_ARMOUR(police_man[22].ped))
				
				request_vehicle_asset(helicopter[0].model, enum_to_int(VRF_REQUEST_ALL_ANIMS)) 
				
				shootout_master_controler_status++
				
			endif 

		break 
		
		case 8
		
			backup_police_front_spawn_system()

			if not is_entity_dead(helicopter[0].veh)
			
				if get_time_position_in_recording(helicopter[0].veh) > 28000
					set_playback_speed(helicopter[0].veh, 0.1)
				endif 
			
//				printfloat(get_time_position_in_recording(helicopter[0].veh))
//				printnl()
			
				if get_time_position_in_recording(helicopter[0].veh) > 28500
					
					if not police_man[23].created
						if not is_ped_injured(police_man[21].ped)
							if not is_ped_in_vehicle(police_man[21].ped, helicopter[0].veh)
								setup_enemy_in_vehicle(police_man[23], helicopter[0].veh, vs_back_left)
								setup_relationship_enemy(police_man[23].ped, true)
								ADD_ARMOUR_TO_PED(police_man[23].ped, -GET_PED_ARMOUR(police_man[23].ped))
							endif 
						else 
							setup_enemy_in_vehicle(police_man[23], helicopter[0].veh, vs_back_left)
							setup_relationship_enemy(police_man[23].ped, true)
							ADD_ARMOUR_TO_PED(police_man[23].ped, -GET_PED_ARMOUR(police_man[23].ped))
						endif 
					endif 
					
					if not police_man[24].created
						if not is_ped_injured(police_man[22].ped)
							if not is_ped_in_vehicle(police_man[22].ped, helicopter[0].veh)
								setup_enemy_in_vehicle(police_man[24], helicopter[0].veh, vs_back_right)
								setup_relationship_enemy(police_man[24].ped, true)
								ADD_ARMOUR_TO_PED(police_man[24].ped, -GET_PED_ARMOUR(police_man[24].ped))
							endif 
						else 
							setup_enemy_in_vehicle(police_man[24], helicopter[0].veh, vs_back_right)
							setup_relationship_enemy(police_man[24].ped, true)
							ADD_ARMOUR_TO_PED(police_man[24].ped, -GET_PED_ARMOUR(police_man[24].ped))
						endif 
					endif  
					
					if not police_man[25].created
						if police_man[23].created
							if not is_ped_injured(police_man[23].ped)
								if not is_ped_in_vehicle(police_man[23].ped, helicopter[0].veh)
									setup_enemy_in_vehicle(police_man[25], helicopter[0].veh, vs_back_left)
									setup_relationship_enemy(police_man[25].ped, true)
									ADD_ARMOUR_TO_PED(police_man[25].ped, -GET_PED_ARMOUR(police_man[25].ped))
								endif 
							else 
								setup_enemy_in_vehicle(police_man[25], helicopter[0].veh, vs_back_left)
								setup_relationship_enemy(police_man[25].ped, true)
								ADD_ARMOUR_TO_PED(police_man[25].ped, -GET_PED_ARMOUR(police_man[25].ped))
							endif 
						endif 
					endif 
					
				endif 
				
			else 

				police_man[23].created = true 
				police_man[24].created = true 
				police_man[25].created = true
				//police_man[26].created = true
			
			endif 
		
			if police_man[23].created
			and police_man[24].created
			and police_man[25].created
			//and police_man[26].created
			
				if is_vehicle_driveable(helicopter[0].veh)
					if is_playback_going_on_for_vehicle(helicopter[0].veh)
						set_playback_speed(helicopter[0].veh, 1.1)
					endif 
				endif 

				shootout_master_controler_status++
			
			endif 
		
		break 
		
		case 9

			backup_police_front_spawn_system()
			
			wave_1_alley_police_dead_count = 0
		
			for i = 12 to 13
				if is_ped_injured(police_man[i].ped)
					wave_1_alley_police_dead_count++
				endif 
			endfor 
			
			for i = 16 to 19
				if is_ped_injured(police_man[i].ped)
					wave_1_alley_police_dead_count++
				endif 
			endfor
			
			//removed 19, 20, 26
			for i = 21 to 25
				if is_ped_injured(police_man[i].ped)
					wave_1_alley_police_dead_count++
				endif 
			endfor
			
			if wave_1_alley_police_dead_count >= 8  //10
			
				if create_wave_2_police()
		
					//wave 0 police
					for i = 0 to 7
						if not is_ped_injured(police_man[i].ped)
							wave_0_alive_count++
						endif 
					endfor
					
					for i = 14 to 15
						if not is_ped_injured(police_man[i].ped)
							wave_0_alive_count++
						endif 
					endfor
					
					for i = 0 to count_of(backup_police_front) - 1
						if not is_ped_injured(backup_police_front[i].ped)
							wave_0_alive_count++
						endif 
					endfor 

					total_number_of_wave_2_peds = wave_0_alive_count + 4   //4 wave 2 peds created in above code
					
					wave_2_police_created = true 
					
					REPLAY_RECORD_BACK_FOR_TIME(3)

					shootout_master_controler_status++
					
				endif 
				
			endif 
		
		break 
		
		case 10
			
			backup_police_alley_spawn_system()
			
			int wave_2_alive_count
			
			wave_2_alive_count = 0

			for i = 27 to 30
				if not is_ped_injured(police_man[i].ped)
					wave_2_alive_count++
				endif 
			endfor
			
			for i = 0 to 7
				if not is_ped_injured(police_man[i].ped)
					wave_2_alive_count++
				endif 
			endfor 
			
			for i = 14 to 15
				if not is_ped_injured(police_man[i].ped)
					wave_2_alive_count++
				endif 
			endfor 

			//I used not is_ped_injured method instead of does_entity_exist is_ped_injured / ped.created and injured because we would have to reset all the .created flags when gathering the count in above case then cycle through array and each alive ped and set their flag as created. Also was leading to issues int he creation process of backup peds.
			for i = 0 to count_of(backup_police_front) - 1
				if not is_ped_injured(backup_police_front[i].ped)
					wave_2_alive_count++
				endif
			endfor 
			
			
			if (total_number_of_wave_2_peds - wave_2_alive_count) >= 3

				setup_enemy(police_man[8])
				setup_relationship_enemy(police_man[8].ped, true)
				remove_blip(police_man[8].blip)
				
				setup_enemy(police_man[9])
				setup_relationship_enemy(police_man[9].ped, true)
				remove_blip(police_man[9].blip)
				
				setup_enemy(police_man[31])
				setup_relationship_enemy(police_man[31].ped, true)
				remove_blip(police_man[31].blip)
				
				if get_current_player_ped_enum() != char_trevor
					if has_ped_got_weapon(selector_ped.pedID[selector_ped_trevor], WEAPONTYPE_SNIPERRIFLE) 
						set_current_ped_weapon(selector_ped.pedID[selector_ped_trevor], WEAPONTYPE_SNIPERRIFLE, true)
					endif 
				endif 
				
				wave_3_police_created = true
				
				shootout_time = 0 //reset for time test bellow
				
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, TRUE)
//				SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_franklin)
				
				shootout_master_controler_status++
			
			endif 

		break 
		
		case 11

			backup_police_front_spawn_system()
			
			backup_police_alley_spawn_system()
			
			shootout_time = 0 //reset timer
		
			shootout_master_controler_status++

		break 
		
		case 12
		
			backup_police_front_spawn_system()
			
			backup_police_alley_spawn_system()
		
			if is_ped_injured(police_man[8].ped)
			and is_ped_injured(police_man[9].ped)
			and is_ped_injured(police_man[31].ped)
			
				if shootout_time = 0
					REPLAY_RECORD_BACK_FOR_TIME(3) //added here because it is flagged
					shootout_time = get_game_timer()
				endif 
			
				//create_ped early and have selector ped michael shoot at him. Make ped only injured by player. 
				if not police_man[34].created
					setup_enemy(police_man[34])
					setup_relationship_enemy(police_man[34].ped, true)
				endif 
			
				//Forces michael to be facing the left cops alley
				if does_entity_exist(selector_ped.pedID[selector_ped_michael])
					//if is_entity_in_area(selector_ped.pedID[selector_ped_michael], <<845.3195, -2332.2019, 29.3346>>, <<4.0, 4.0, 4.0>>)
					if michael_defensive_sphere_system_status > 0 //michael's defensive sphere is here left alley <<845.3195, -2332.2019, 29.3346>>
//						if not is_ped_in_cover(selector_ped.pedID[selector_ped_michael])
//							if get_script_task_status(selector_ped.pedID[selector_ped_michael], script_task_put_ped_directly_into_cover) != performing_task
//								task_put_ped_directly_into_cover(selector_ped.pedID[selector_ped_michael], <<845.3195, -2332.2019, 29.3346>>, -1, false)
//							endif 
//						endif
						
//						if get_script_task_status(selector_ped.pedID[selector_ped_michael], script_task_combat) != performing_task
//							if not is_ped_injured(police_man[34].ped)
//								set_blocking_of_non_temporary_events(selector_ped.pedID[selector_ped_michael], true)
//								clear_ped_tasks(selector_ped.pedID[selector_ped_michael])
//								task_combat_ped(selector_ped.pedID[selector_ped_michael], police_man[34].ped)
//								//task_shoot_at_entity(selector_ped.pedID[selector_ped_michael], police_man[34].ped, 3000, firing_type_1_burst) 
//							endif 
//						endif

						if not mag_demo_cover_set
							set_entity_coords(selector_ped.pedID[selector_ped_michael], <<845.8183, -2332.2798, 29.3346>>)
							set_entity_heading(selector_ped.pedID[selector_ped_michael], 75.8831) 
							task_put_ped_directly_into_cover(selector_ped.pedID[selector_ped_michael], <<845.66034, -2332.30762, 29.33460>>, -1, false, 0, true, false, mag_demo_cover_point)  
							force_ped_ai_and_animation_update(selector_ped.pedID[selector_ped_michael])
							mag_demo_cover_set = true
						endif 
						
					endif 
				endif 
				
				

//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, TRUE)
//				SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_trevor, selector_ped_franklin)

				//MAG DEMO ONLY CODE REMOVE FOR REAL GAME
				//if IS_PLAYER_SWITCH_IN_PROGRESS() and get_current_player_ped_enum() = char_michael
				if get_current_player_ped_enum() = char_michael
				or get_current_player_ped_enum() = char_franklin
				or lk_timer(shootout_time, 10000)
				
					int wave_1_alive_count 
					
					wave_1_alive_count = 0
					
					police_car[5].veh = create_vehicle(police_car[5].model, police_car[5].pos, police_car[5].heading)
					set_entity_only_damaged_by_player(police_car[5].veh, true)
					set_vehicle_siren(police_car[5].veh, true)
					start_playback_recorded_vehicle(police_car[5].veh, police_car[5].recording_number, "lkfbi4") 
					skip_time_in_playback_recorded_vehicle(police_car[5].veh, 1500)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(police_car[5].veh)
					
					setup_enemy_in_vehicle(police_man[32], police_car[5].veh) 
					setup_relationship_enemy(police_man[32].ped, true)
					setup_enemy_in_vehicle(police_man[33], police_car[5].veh, vs_front_right)
					setup_relationship_enemy(police_man[33].ped, true)

					//*****police 34 created before switch 
					
					setup_enemy(police_man[35])
					setup_relationship_enemy(police_man[35].ped, true)
					
					
					for i = 12 to 13
						if not is_ped_injured(police_man[i].ped)
							wave_1_alive_count++
						endif 
					endfor
					
					for i = 16 to 18
						if not is_ped_injured(police_man[i].ped)
							wave_1_alive_count++
						endif 
					endfor
					
					for i = 21 to 25
						if not is_ped_injured(police_man[i].ped)
							wave_1_alive_count++
						endif 
					endfor 
					
					for i = 0 to count_of(backup_police_alley) - 1
						if not is_ped_injured(backup_police_alley[i].ped)
							wave_1_alive_count++
						endif 
					endfor 

					total_number_of_wave_4_peds = wave_1_alive_count + 4 

					shootout_master_controler_status++

				endif 
			
			endif 
		
		break 
		
		case 13
		
			int wave_4_alive_count
			
			wave_4_alive_count = 0

			//-- wave 1 police
			for i = 12 to 13
				if not is_ped_injured(police_man[i].ped)
					wave_4_alive_count++
				endif 
			endfor
			
			for i = 16 to 18
				if not is_ped_injured(police_man[i].ped)
					wave_4_alive_count++
				endif 
			endfor
			
			for i = 21 to 25
				if not is_ped_injured(police_man[i].ped)
					wave_4_alive_count++
				endif 
			endfor 
			
			//-- wave 4 police
			for i = 32 to 35
				if not is_ped_injured(police_man[32].ped)
					wave_4_alive_count++
				endif 
			endfor 
			
			//alley back up
			for i = 0 to count_of(backup_police_alley) - 1
				if not is_ped_injured(backup_police_alley[i].ped)
					wave_4_alive_count++
				endif 
			endfor 

			
			//--
					
			if (total_number_of_wave_4_peds - wave_4_alive_count) >= 3
			
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, TRUE)
//				SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_franklin, selector_ped_michael)

				helicopter[1].veh = create_vehicle(helicopter[1].model, helicopter[1].pos, helicopter[1].heading)
				set_vehicle_livery(helicopter[1].veh, 0)
				set_heli_blades_full_speed(helicopter[1].veh)
				start_playback_recorded_vehicle(helicopter[1].veh, helicopter[1].recording_number, "lkfbi4")
				skip_time_in_playback_recorded_vehicle(helicopter[1].veh, 1000)//5000
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(helicopter[1].veh)
				SET_VEHICLE_SEARCHLIGHT(helicopter[1].veh, TRUE, true)
				
				setup_enemy_in_vehicle(helicopter_driver[1], helicopter[1].veh) 
				setup_relationship_enemy(helicopter_driver[1].ped, true)
				
				setup_enemy_in_vehicle(police_man[10], helicopter[1].veh, vs_back_left)  
				setup_relationship_enemy(helicopter_driver[1].ped, true)
				
				setup_enemy_in_vehicle(police_man[11], helicopter[1].veh, vs_back_right) 
				setup_relationship_enemy(helicopter_driver[1].ped, true)
					
				wave_5_police_created = true 
				
				if get_current_player_ped_enum() != char_trevor
					if has_ped_got_weapon(selector_ped.pedID[selector_ped_trevor], weapontype_rpg) 
						set_current_ped_weapon(selector_ped.pedID[selector_ped_trevor], weapontype_rpg, true)
					endif 
					set_entity_only_damaged_by_player(selector_ped.pedID[selector_ped_trevor], false)
				endif 
				
				set_fake_wanted_level(3)
				
				shootout_master_controler_status++

			endif
		
		break
		
		case 14
		
			if not does_entity_exist(helicopter[1].veh)
			
				set_selector_ped_hint(selector_ped, selector_ped_michael, false)
				set_selector_ped_hint(selector_ped, selector_ped_franklin, false)
				set_selector_ped_hint(selector_ped, selector_ped_trevor, false)
			
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI4_HELISHOT)
				
				for i = 0 to count_of(police_man) - 1
					if not is_ped_injured(police_man[i].ped)
						if get_entity_health(police_man[i].ped) > 125
						
							set_entity_health(police_man[i].ped, 125)
							set_entity_only_damaged_by_player(police_man[i].ped, false)

							//stops peds staying near truck at end of shootout. bug 1501041
							set_ped_combat_attributes(police_man[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, false)
							set_blocking_of_non_temporary_events(police_man[i].ped, false)
							set_ped_angled_defensive_area(police_man[i].ped, <<886.087, -2332.138, 29.331>>, <<883.225, -2365.516, 32.331>>, 15.5)
							task_combat_hated_targets_around_ped(police_man[i].ped, 200.00)
							
							shootout_status[i] = do_nothing
							
						endif  
					endif 
				endfor 
				
				for i = 0 to count_of(backup_police_front) - 1
					if not is_ped_injured(backup_police_front[i].ped)
						if get_entity_health(backup_police_front[i].ped) > 125
						
							set_entity_health(backup_police_front[i].ped, 125)
							set_entity_only_damaged_by_player(backup_police_front[i].ped, false)
							
							set_ped_combat_attributes(backup_police_front[i].ped, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, false)
							set_blocking_of_non_temporary_events(backup_police_front[i].ped, false)
							set_ped_angled_defensive_area(backup_police_front[i].ped, <<886.087, -2332.138, 29.331>>, <<883.225, -2365.516, 32.331>>, 15.5)
							task_combat_hated_targets_around_ped(backup_police_front[i].ped, 200.00)
							
							backup_police_front_shootout_status[i] = do_nothing
							
						endif  
					endif 
				endfor 
				
				for i = 0 to count_of(backup_police_alley) - 1
					if not is_ped_injured(backup_police_alley[i].ped)
						if get_entity_health(backup_police_alley[i].ped) > 125
						
							set_entity_health(backup_police_alley[i].ped, 125)
							set_entity_only_damaged_by_player(backup_police_alley[i].ped, false)
							
						endif  
					endif 
				endfor 
				
				set_fake_wanted_level(2)
				
				REPLAY_RECORD_BACK_FOR_TIME(3)
				
				shootout_master_controler_status++
			
			endif 
		
		break 
		
		case 15

//			ped_structure_are_all_enemies_dead(police_man)
//			are_backup_police_dead(backup_police_front)
//			are_backup_police_dead(backup_police_alley)
			
			int total_cops_dead 
			
			total_cops_dead = ped_structure_get_total_number_of_enemies_dead(police_man) + get_total_number_of_backup_police_dead(backup_police_front) + get_total_number_of_backup_police_dead(backup_police_alley)
			
			if total_cops_dead >= ((total_number_of_police_men + total_number_of_backup_police_front + total_number_of_backup_police_alley) - 3)

				set_fake_wanted_level(1)
				
				shootout_master_controler_status++
				
			endif 
			
			switch get_current_player_ped_enum()
			
				case char_michael
				
					SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_trevor)
				
				break 
				
				case char_franklin
				
					SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_trevor)
				
				break 
				
				case char_trevor
				
					SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_franklin, selector_ped_michael)
				
				break
				
			endswitch 
			
//			ped_structure_get_total_number_of_enemies_dead(police_man)
//			get_total_number_of_backup_police_dead()

			
//			if all_police_dead and all_backup_police_dead
//			
//				shootout_master_controler_status++
//			
//			endif 
		
		break 
		
		case 16
		
			if not has_any_selector_ped_been_selected(selector_ped) and not scamdetails.bSplineActive
//		
				if create_conversation(scripted_speech[0], "HeatAud", "heat_end_0", CONV_PRIORITY_medium) 
				
					stop_all_audio_scenes_after_shootout()

					SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

					if not has_sound_finished(police_siren_sound)
						stop_sound(police_siren_sound)
					endif 
					PLAY_SOUND_FROM_coord(police_siren_sound, "Distant_Sirens", <<844.7, -2213.3, 57.9>>)
					
					CLEAR_TRIGGERED_LABELS()
					
					//set_fake_wanted_level(1)
				
					add_scenario_blocking_area(<<965.2, -1693.0, -100.00>>, <<1144.9, -1929.1, 100.00>>)
					
					if does_entity_exist(selector_ped.pedID[SELECTOR_PED_michael])
						//setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael], true)
						//set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_michael], true)
						//SET_PED_USING_ACTION_MODE(selector_ped.pedID[SELECTOR_PED_michael], false)
						//clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_michael])
					endif 
					
					if does_entity_exist(selector_ped.pedID[SELECTOR_PED_franklin])
						//setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_franklin], true)
						//set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_franklin], true)
						//SET_PED_USING_ACTION_MODE(selector_ped.pedID[SELECTOR_PED_franklin], false)
						//clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
					endif 
						
					if does_entity_exist(selector_ped.pedID[SELECTOR_PED_trevor])
						setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_trevor], true)
						set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_trevor], true)
						//SET_PED_USING_ACTION_MODE(selector_ped.pedID[SELECTOR_PED_trevor], false)
						clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_TREVOR])
					endif 
					
					//kill_face_to_face_conversation()
					
					cleanup_helicopter_assets()
					
					//STREAMVOL_DELETE(streaming_volume)

					if is_vehicle_driveable(army_truck.veh)
						set_vehicle_doors_locked(army_truck.veh, vehiclelock_unlocked) 
						SET_VEHICLE_CAN_DEFORM_WHEELS(army_truck.veh, true)
						deactivate_vehicle_proofs(army_truck.veh)
					endif 

					if is_vehicle_driveable(tow_truck.veh)
						set_vehicle_doors_locked(tow_truck.veh, vehiclelock_unlocked)
					endif

					if GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, vehgen_s_flag_available)
						GET_VEHICLE_GEN_DATA(get_away_vehicle, VEHGEN_MISSION_VEH_FBI4_PREP)	
					else 
						//only active when selecting mission from debug menu.
						create_debug_getaway_car = true
						get_away_vehicle.coords = <<1121.03, -1249.90, 19.57>>	
					endif 
					
					if does_entity_exist(selector_ped.pedID[selector_ped_michael])
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(selector_ped.pedID[selector_ped_michael])
					endif 
					
					if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(selector_ped.pedID[selector_ped_franklin])
					endif 
					
					if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(selector_ped.pedID[selector_ped_trevor])
					endif 
							
					if get_current_player_ped_enum() = char_franklin
					
						set_selector_ped_hint(selector_ped, selector_ped_michael, false)
						set_selector_ped_hint(selector_ped, selector_ped_franklin, false)
						set_selector_ped_hint(selector_ped, selector_ped_trevor, false)
					
						set_selector_ped_blocked(selector_ped, selector_ped_michael, true)
						set_selector_ped_blocked(selector_ped, selector_ped_franklin, true)
						set_selector_ped_blocked(selector_ped, selector_ped_trevor, true)

						if does_blip_exist(trevor_blip)
							remove_blip(trevor_blip)
						endif 
						
						if does_blip_exist(michael_blip)
							remove_blip(michael_blip)
						endif 
						
						if does_blip_exist(franklin_blip)
							remove_blip(franklin_blip)
						endif
					
						if does_entity_exist(selector_ped.pedID[SELECTOR_PED_franklin])
							task_look_at_entity(selector_ped.pedID[SELECTOR_PED_FRANKLIN], player_ped_id(), -1)
						endif 
						
						Set_Replay_Mid_Mission_Stage_With_Name(4, "burn the truck", true)
						
						original_time = get_game_timer()
						
						flee_time = get_game_timer()
						
						if is_vehicle_driveable(rubbish_truck.veh)
							set_entity_only_damaged_by_player(rubbish_truck.veh, true)
							set_entity_proofs(rubbish_truck.veh, false, false, false, false, false)
						endif 
						
						DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(locates_data, TRUE)
								
						mission_flow = burn_rubbish_truck
						
					else
					
						set_selector_ped_hint(selector_ped, selector_ped_michael, false)
						set_selector_ped_hint(selector_ped, selector_ped_franklin, true)
						set_selector_ped_hint(selector_ped, selector_ped_trevor, false)
					
						set_selector_ped_blocked(selector_ped, selector_ped_michael, true)
						set_selector_ped_blocked(selector_ped, selector_ped_franklin, false)
						set_selector_ped_blocked(selector_ped, selector_ped_trevor, true)
						
						//forces quick switch from trevor to franklin
						SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_michael, selector_ped_franklin)
					
						switch_to_franklin_time = get_game_timer()
					
						mission_flow = switch_to_michael_or_franklin

					endif 
					
					return true 
				
				endif 
				
			endif 
		
		break 
		
	endswitch 
	
	//don't process on 15 as you already do it in that case
	//if shootout_master_controler_status != 15
		ped_structure_are_all_enemies_dead(police_man)
		are_backup_police_dead(backup_police_front)
		are_backup_police_dead(backup_police_alley)
	//endif 
			
	return false 
		
endfunc

proc police_vehicle_explosion_system()

	int i = 0

	switch police_vehicle_explosion_system_status 
	
		case 0
		
			if does_entity_exist(police_car[0].veh)
			and does_entity_exist(police_car[1].veh)
			and does_entity_exist(police_car[2].veh)
			and does_entity_exist(police_car[3].veh)
				
				if not is_entity_dead(police_car[0].veh)
				and not is_entity_dead(police_car[1].veh)
				and not is_entity_dead(police_car[2].veh)
				and not is_entity_dead(police_car[3].veh)
					
					if not is_playback_going_on_for_vehicle(police_car[0].veh)
					and not is_playback_going_on_for_vehicle(police_car[1].veh)
					and not is_playback_going_on_for_vehicle(police_car[2].veh)
					and not is_playback_going_on_for_vehicle(police_car[3].veh)
					
						for i = 0 to 3
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(police_car[i].veh)
						endfor 
						
						police_vehicle_explosion_system_status = 1
					
					endif 
				
				else 

					//if the player expodes a vehicle with an prg before they stop their recording then don't allow
					//the vehicle exposion system to run untill near the end of the mission.
					police_vehicle_explosion_system_status = 2
				
				endif 
			endif 

		break 
	
	
		case 1

			if ped_structure_get_total_number_of_enemies_dead(police_man) >= count_of(police_man) - 4 
			
				police_vehicle_explosion_system_status = 2
				
			else 

				for i = 0 to 3
				
					if does_entity_exist(police_car[i].veh)
				
						if is_vehicle_driveable(police_car[i].veh)

							if has_entity_been_damaged_by_entity(police_car[i].veh, player_ped_id())
							
								police_car[i].health = get_entity_health(police_car[i].veh)
								police_car[i].engine_health = get_vehicle_engine_health(police_car[i].veh)
								police_car[i].petrol_tank_health = get_vehicle_petrol_tank_health(police_car[i].veh)
						
								if get_entity_health(police_car[i].veh) < 700
								or get_vehicle_engine_health(police_car[i].veh) < 700.00
								or get_vehicle_petrol_tank_health(police_car[i].veh) < 700.00
								
									if is_playback_going_on_for_vehicle(police_car[i].veh)
										stop_playback_recorded_vehicle(police_car[i].veh) 
									endif 
								
									//explode_vehicle(police_car[i].veh)
									add_owned_explosion(player_ped_id(), get_entity_coords(police_car[i].veh), exp_tag_car, 1.0)
									
									
									set_vehicle_as_no_longer_needed(police_car[i].veh)
									
									police_vehicle_explosion_system_status = 2
									
									exit
		
								else 
								
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(police_car[i].veh)
								
								endif 
								
							endif 
							
						endif 
						
					endif 
				endfor 
				
			endif 
		
		break 
		
		case 2
					
			if ped_structure_get_total_number_of_enemies_dead(police_man) >= count_of(police_man) - 4 
			
				for i = 0 to count_of(police_car) - 1
					if does_entity_exist(police_car[i].veh)
						if is_vehicle_driveable(police_car[i].veh)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(police_car[i].veh)
						endif 
					endif
				endfor 
				
				police_vehicle_explosion_system_status = 3
				
			endif 
			
		break
		
		case 3
			
			for i = 0 to count_of(police_car) - 1
			
				if does_entity_exist(police_car[i].veh)
				
					if is_vehicle_driveable(police_car[i].veh)
					
						if has_entity_been_damaged_by_entity(police_car[i].veh, player_ped_id())
					
							if get_entity_health(police_car[i].veh) < 700
							or get_vehicle_engine_health(police_car[i].veh) < 700
							or get_vehicle_petrol_tank_health(police_car[i].veh) < 700
							
								if is_playback_going_on_for_vehicle(police_car[i].veh)
									stop_playback_recorded_vehicle(police_car[i].veh) 
								endif 
							
								explode_vehicle(police_car[i].veh)
								
								set_vehicle_as_no_longer_needed(police_car[i].veh)
								
								police_vehicle_explosion_system_status = 22
								
								exit
								
							else 
								
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(police_car[i].veh)
							
							endif 
							
						endif 
					endif 
				endif
				
			endfor 
		
		break 
		
		case 22
		
		break 
		
	endswitch 
	
	//explode car 5
	if does_entity_exist(police_car[5].veh)
		if is_vehicle_driveable(police_car[5].veh)
			if not is_playback_going_on_for_vehicle(police_car[5].veh)

				if has_entity_been_damaged_by_entity(police_car[5].veh, player_ped_id())
					
					if get_entity_health(police_car[5].veh) < 500
					or get_vehicle_engine_health(police_car[5].veh) < 500
					or get_vehicle_petrol_tank_health(police_car[5].veh) < 500
					
						if is_playback_going_on_for_vehicle(police_car[5].veh)
							stop_playback_recorded_vehicle(police_car[5].veh) 
						endif 
					
						explode_vehicle(police_car[5].veh)
						
						//set_vehicle_as_no_longer_needed(police_car[i].veh)
						
					else 
									
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(police_car[5].veh)
					
					endif 
				
				endif 

			else 
		
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(police_car[5].veh)
			
			endif 
		endif 
	endif 

endproc 

proc trevor_ai_system()

	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
	
		if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
		
			switch trevor_ai_system_status 
			
				case 0
				
					if allow_hotswap
						if lk_timer(trevor_time, 20000)
						
							TASK_SHOOT_AT_COORD(selector_ped.pedID[selector_ped_trevor], <<925.45, -2343.15, 31.98>>, 5000, firing_type_1_burst)
							
							rpg_target_pos = <<925.45, -2343.15, 31.98>>
							
							trevor_time = get_game_timer()

							trevor_ai_system_status++

						endif 
					endif 

				break 
				
				case 1
					
					if lk_timer(trevor_time, 30000)
						
						if is_any_vehicle_near_point(<<913.23, -2360.12, 80.00>>, 40.0)

							rpg_target_pos =  <<913.23, -2360.12, 60.00>>
							
						else 
						
							switch get_random_int_in_range(0, 4)
							
								case 0
								
									rpg_target_pos = <<913.5, -2357.29, 30.4>>
		
								break
								
								case 1
								
									rpg_target_pos = <<911.00, -2337.11, 30.02>>
									
								break
								
								case 2
								
									rpg_target_pos = <<920.75, -2364.49, 30.22>>

								break 
								
								case 3
								
									rpg_target_pos = <<925.45, -2343.15, 31.98>>
								
								break 
							
							endswitch 

						endif 
						
						TASK_SHOOT_AT_COORD(selector_ped.pedID[selector_ped_trevor], rpg_target_pos, 5000, firing_type_1_burst)
						
							//task_reload_weapon(null, true)
						trevor_time = get_game_timer()
						
					endif 
					
					if does_entity_exist(helicopter[1].veh)
						trevor_ai_system_status++
					endif 
				
				break
				
				case 2
				
					if does_entity_exist(helicopter[1].veh)
					
						if is_vehicle_driveable(helicopter[1].veh)
						
//							if not is_player_switch_in_progress()
							
								if get_script_task_status(selector_ped.pedID[selector_ped_trevor], script_task_aim_gun_at_entity) != performing_task
							
									task_aim_gun_at_entity(selector_ped.pedID[selector_ped_trevor], helicopter[1].veh, -1)
									
									task_look_at_entity(selector_ped.pedID[selector_ped_trevor], helicopter[1].veh, -1)
									
								endif 
								
//							else 
//							
//								if get_script_task_status(selector_ped.pedID[selector_ped_trevor], script_task_aim_gun_at_coord) != performing_task
//							
//									vector aim_pos 
//									vector trevor_pos
//									
//									aim_pos = get_entity_coords(helicopter[1].veh)
//									trevor_pos = get_entity_coords(selector_ped.pedID[selector_ped_trevor])
//									aim_pos.z = trevor_pos.z
//							
//									task_aim_gun_at_coord(selector_ped.pedID[selector_ped_trevor], aim_pos, -1)
//									
//								endif 
//							
//							endif 
							
						endif 
						
					else 
					
						task_clear_look_at(selector_ped.pedID[selector_ped_trevor])
					
						trevor_time = get_game_timer() 
						
						trevor_ai_system_status = 1

					endif 
				
				break 
				
				case 22
				
				break 
				
			endswitch 
			
		endif 
		
	else 
	
		if get_current_player_ped_enum() = char_trevor
		
			set_ped_reset_flag(player_ped_id(), PRF_DisableDropDowns, true)
			
		endif 
//	
//			if does_cam_exist(sCamDetails.camID)
//				
//				printfloat(get_cam_spline_phase(sCamDetails.camID))
//				printnl()
//		
//				if get_cam_spline_phase(sCamDetails.camID) > 0.5
//				
//					if has_ped_task_finished_2(player_ped_id(), script_task_shoot_at_coord, -2, false)
//			
//						if get_player_wanted_level(player_id()) > 0
//						
//							TASK_SHOOT_AT_COORD(player_ped_id(), <<925.45, -2343.15, 31.98>>, 2000, firing_type_1_burst)
//				
//							//trevor_time = get_game_timer()
//				
//							//trevor_ai_system_status = 1
//							
//						endif 
//						
//					endif 
//				
//				else 
//				
//					clear_players_task_on_control_input(script_task_shoot_at_coord)
//
//				endif 
//			endif 
//		endif 
		
	endif 

endproc 

func bool is_player_near_vehicle(vehicle_index &mission_entity, float distance)

	if does_entity_exist(mission_entity)
		if not is_entity_dead(mission_entity)
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(mission_entity)) < distance
			
				return true 
				
			endif 
		endif 
	endif 
	
	return false 

endfunc 

proc shootout_music_event_system()
	
	switch shootout_music_event_system_status
	
		case 0
		
//			if not has_label_been_triggered("FBI4_COVER")
//				if is_ped_in_cover(player_ped_id())
//					trigger_music_event("FBI4_COVER")
//					set_label_as_triggered("FBI4_COVER", true)
//				endif 
//			endif 
		
		
			if prepare_music_event("fbi4_SHOOTOUT_MA")
		
				if allow_hotswap
				or is_player_near_vehicle(police_car[0].veh, 30.0)
				or is_player_near_vehicle(police_car[1].veh, 30.0)
				or is_player_near_vehicle(police_car[2].veh, 30.0)
				
					if is_ped_shooting(player_ped_id())
					or does_cam_exist(sCamDetails.camID)
			
						trigger_music_event("fbi4_SHOOTOUT_MA")
					
						shootout_music_event_system_status++
						
					endif 
					
				endif 
				
			endif 
		
		break 
		
		case 1
		
		break 
		
		case 2
		
		break 
		
	endswitch 

endproc 

proc disable_friendly_fire_audio()
	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		if not is_ped_injured(selector_ped.pedID[selector_ped_michael])
			SET_PED_RESET_FLAG(selector_ped.pedID[selector_ped_michael], PRF_DisableFriendlyGunReactAudio, true)
		endif 
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
		if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
			SET_PED_RESET_FLAG(selector_ped.pedID[selector_ped_franklin], PRF_DisableFriendlyGunReactAudio, true)
		endif 
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		if not is_ped_injured(selector_ped.pedID[selector_ped_trevor])
			SET_PED_RESET_FLAG(selector_ped.pedID[selector_ped_trevor], PRF_DisableFriendlyGunReactAudio, true)
		endif 
	endif 
	
endproc 

proc clear_player_react_explosion_anim()

	if is_entity_playing_anim(player_ped_id(), "missfbi4", "REACT_Explosion")
		if GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(), "missfbi4", "REACT_Explosion") > 0.7
			clear_players_task_on_control_input(script_task_play_anim, true)
		endif 
	endif 
	
endproc 

proc setup_player_ped_proofs()
	
	if does_entity_exist(selector_ped.pedID[selector_ped_michael])
		//set_entity_proofs(selector_ped.pedID[selector_ped_michael], true, true, true, true, true)
		set_entity_invincible(selector_ped.pedID[selector_ped_michael], true)
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
		//set_entity_proofs(selector_ped.pedID[selector_ped_franklin], true, true, true, true, true)
		set_entity_invincible(selector_ped.pedID[selector_ped_franklin], true)
	endif 
	
	if does_entity_exist(selector_ped.pedID[selector_ped_trevor])
		//set_entity_proofs(selector_ped.pedID[selector_ped_trevor], true, true, true, true, true)
		set_entity_invincible(selector_ped.pedID[selector_ped_trevor], true)
	endif 
	
	set_entity_invincible(player_ped_id(), true)
	
endproc 

/// PURPOSE:  deactivates the police sirens for the mission. Mag demo only request.
//
proc police_vehicle_siren_system()

	switch police_vehicle_siren_system_status 
	
		case 0

			if does_entity_exist(police_car[1].veh)
				if not is_entity_dead(police_car[1].veh)
					if not is_ped_injured(police_man[2].ped)
						if not is_ped_sitting_in_vehicle(police_man[2].ped, police_car[1].veh)
							set_vehicle_siren(police_car[1].veh, false)
							police_vehicle_siren_system_status++
						endif 
					else 
						set_vehicle_siren(police_car[1].veh, false)
						police_vehicle_siren_system_status++
					endif 
				endif 
			endif 
			
		break 
		
		case 1
		
			if does_entity_exist(police_car[3].veh)
				if not is_entity_dead(police_car[3].veh)
					if not is_ped_injured(police_man[6].ped)
						if not is_ped_sitting_in_vehicle(police_man[6].ped, police_car[3].veh)
							set_vehicle_siren(police_car[3].veh, false)
							police_vehicle_siren_system_status++
						endif 
					else 
						set_vehicle_siren(police_car[3].veh, false)
						police_vehicle_siren_system_status++
					endif 
				endif 
			endif 
		
		break
		
		case 2
		
		break 

	endswitch 

endproc 

//-----
		
proc apply_damage_to_ped_system(selector_slots_enum target_ped)

	if lk_timer(apply_damage_time, 1000)

		switch target_ped
		
			case selector_ped_michael
				
				set_entity_health(selector_ped.pedID[selector_ped_michael], (get_entity_health(selector_ped.pedID[selector_ped_michael]) - 15)) 
			
			break 
			
			case selector_ped_franklin
				
				set_entity_health(selector_ped.pedID[selector_ped_franklin], (get_entity_health(selector_ped.pedID[selector_ped_franklin]) - 15)) 
			
			break 

		endswitch 
		
		apply_damage_time = get_game_timer()
		
	endif 

endproc
			
	
proc force_ped_to_switch_damage_hint_system()

	switch force_ped_to_switch_damage_hint_system_status 
	
		case 0
		
			if is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)   
				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
			endif 
		
			//initialise target_switch_to_ped
			target_switch_to_ped = number_of_selector_peds
		
			if wave_2_police_created 
	
				if wave_2_front_dialogue_triggered
				
					switch get_current_player_ped_enum() 

						case char_michael
					
							//if not has_entity_been_damaged_by_entity(police_man[27].ped, player_ped_id())
								
								//if is_entity_in_angled_area(selector_ped.pedID[selector_ped_franklin] fraont area near wave 2
								//and not is_entity_in_angled_area(player_ped_id() fraont area near wave 2
								//and get_distance_between_peds(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_franklin])) > 10

									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, TRUE)
									SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
									
									target_switch_to_ped = selector_ped_franklin
									
									health_reduction_time = get_game_timer()
	
									force_ped_to_switch_damage_hint_system_status++
	
								//endif 
								
							//endif 
							
						break 
						
						case char_franklin
						
							//if not has_entity_been_damaged_by_entity(police_man[27].ped, player_ped_id())
								
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, true)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
							SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_trevor, selector_ped_michael)
							
							target_switch_to_ped = selector_ped_michael
							
							health_reduction_time = get_game_timer()
			
							force_ped_to_switch_damage_hint_system_status++
						
						break 

						case char_trevor
							
							if michael_defensive_sphere_system_status = 1 //michael's defensive sphere is in the alley way
							
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, TRUE)
								SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
					
								target_switch_to_ped = selector_ped_franklin
								
								health_reduction_time = get_game_timer()
				
								force_ped_to_switch_damage_hint_system_status++
								
							elif franklin_defensive_sphere_system_status = 1
							
								//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)

									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, true)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
									SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_trevor, selector_ped_michael)
									
									target_switch_to_ped = selector_ped_michael
									
									health_reduction_time = get_game_timer()
				
									force_ped_to_switch_damage_hint_system_status++
								//endif 
					
							else 
							
								//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
								
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, TRUE)
									SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
									
									target_switch_to_ped = selector_ped_franklin
									
									health_reduction_time = get_game_timer()
				
									force_ped_to_switch_damage_hint_system_status++
									
								//endif 

							endif 
							
						break
					
					endswitch 
					
				endif 
	
			endif 

		break 
		
		case 1
		
			if not police_man[8].created //if not wave 3 police created
		
				if (get_current_player_ped_enum() != get_player_ped_enum_from_selector_slot(target_switch_to_ped))
				and (target_switch_to_ped != number_of_selector_peds)
				
					if lk_timer(health_reduction_time, 10000)
					
						SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped)
					
						apply_damage_to_ped_system(target_switch_to_ped)

					endif 

					
					if not is_any_text_being_displayed(locates_data)
					
						if has_label_been_triggered("cntry_god31") //only trigger if wave 2 god text triggered
						
							switch get_player_ped_enum_from_selector_slot(target_switch_to_ped)
							
								case char_michael
								
									if lk_timer(health_dialogue_time, 20000) 
										if create_conversation(scripted_speech[0], "heataud", "fbi4_MFront", CONV_PRIORITY_medium)
											health_dialogue_time = get_game_timer()
										endif 
									endif 
								
								break 
								
								case char_franklin
								
									if lk_timer(health_dialogue_time, 20000) 
										if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
											health_dialogue_time = get_game_timer()
										endif 
									endif 
								
								break 
								
							endswitch 
							
						endif 
						
					endif 
					
				endif 
		
			else 
			
				SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped, false)
			
				target_switch_to_ped = number_of_selector_peds
				
				//cached_player_ped = get_current_player_ped_enum() 
				
				if are_snipers_in_position()
				
					switch get_current_player_ped_enum() 
						
						case char_michael
						
							//if not has_entity_been_damaged_by_entity(police_man[8].ped, player_ped_id())
								
								//if is_entity_in_angled_area(
								
									//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
										
										//set_label_as_triggered("fbi4_FFront", true)
		
										SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
										SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
										SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, TRUE)
										SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_franklin)
		
										target_switch_to_ped = selector_ped_trevor
										
										health_reduction_time = get_game_timer()
		
										force_ped_to_switch_damage_hint_system_status++
										
									//endif 
									
								//endif 
								
							//endif 
							
						break 
						
						case char_franklin
						
							//if not has_entity_been_damaged_by_entity(police_man[8].ped, player_ped_id())
								
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, TRUE)
								SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_franklin)
		
								target_switch_to_ped = selector_ped_trevor
								
								health_reduction_time = get_game_timer()
				
								force_ped_to_switch_damage_hint_system_status++
								
							//endif 
						
						break 

						case char_trevor

						break
						
					endswitch 
					
				endif 
			
			endif 
			
			//saftey check incase it jumps back into here from bellow when target_switch_to_ped = number_of_selector_peds
			if police_man[34].created
				force_ped_to_switch_damage_hint_system_status = 2
			endif

		break 
		
		case 2
		
			if (get_current_player_ped_enum() != get_player_ped_enum_from_selector_slot(target_switch_to_ped))
			and (target_switch_to_ped != number_of_selector_peds)
			
				if lk_timer(health_reduction_time, 10000)
				
					//SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped)
				
					//apply_damage_to_ped_system(player_ped_id(),)
					//snipers peds are tasked to shoot at the player with 100 accuracy. 

				endif 
				
			else 
			
				//if you switch to trevor killed the last wave 2 guy then wave 3 is cretaed then you switch to 
				//micahel you would still expect it to flash
				//or get it to go back to case 0
			
				//force_ped_to_switch_damage_hint_system_status++
				
//				if (target_switch_to_ped = number_of_selector_peds)
//					force_ped_to_switch_damage_hint_system_status = 1
//				endif 
			
			endif
			
			
			if police_man[34].created //wave 5

				SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped, false)
				
				target_switch_to_ped = number_of_selector_peds
				
//				cached_player_ped_enum = get_current_player_ped_enum() 
				
				switch get_current_player_ped_enum() 
					
					case char_michael
					
						if not has_entity_been_damaged_by_entity(police_man[34].ped, player_ped_id())
							
							//if is_entity_in_angled_area(
							
								//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
									
									//set_label_as_triggered("fbi4_FFront", true)
	
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, true)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
									SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_michael, selector_ped_franklin)
	
									target_switch_to_ped = selector_ped_franklin
									
									health_reduction_time = get_game_timer()
	
									force_ped_to_switch_damage_hint_system_status++
									
								//endif 
								
							//endif 
							
						else 
						
							force_ped_to_switch_damage_hint_system_status++
						
						endif 
						
					break 
					
					case char_franklin
					
						if not has_entity_been_damaged_by_entity(police_man[34].ped, player_ped_id())
							
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, true)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
							SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_franklin)
	
							target_switch_to_ped = selector_ped_michael
							
							health_reduction_time = get_game_timer()
			
							force_ped_to_switch_damage_hint_system_status++
							
						else 
						
							force_ped_to_switch_damage_hint_system_status++
						
						endif 
					
					break 

					case char_trevor
					
						if michael_defensive_sphere_system_status = 1 //michael's defensive sphere is in the alley way
						
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, true)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
							SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_trevor, selector_ped_michael)
				
							target_switch_to_ped = selector_ped_michael
							
							health_reduction_time = get_game_timer()
			
							force_ped_to_switch_damage_hint_system_status++
							
						elif franklin_defensive_sphere_system_status = 1
						
							//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)

								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, true)
								SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
								
								target_switch_to_ped = selector_ped_franklin
								
								health_reduction_time = get_game_timer()
			
								force_ped_to_switch_damage_hint_system_status++
							//endif 
				
						else 
						
							//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
							
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, TRUE)
								SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
								
								target_switch_to_ped = selector_ped_trevor
								
								health_reduction_time = get_game_timer()
			
								force_ped_to_switch_damage_hint_system_status++
								
							//endif 

						endif 
						
					break
					
				endswitch 
				
			endif 

		break 
		
		case 3
		
			if (get_current_player_ped_enum() != get_player_ped_enum_from_selector_slot(target_switch_to_ped))
			and (target_switch_to_ped != number_of_selector_peds)
			
				if lk_timer(health_reduction_time, 10000)
				
					SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped)
				
					//apply_damage_to_ped_system(player_ped_id(),)
					//snipers peds are tasked to shoot at the player with 100 accuracy. 

				endif 
				
			else 
			
				//force_ped_to_switch_damage_hint_system_status++
			
			endif
			
//			if not IS_PLAYER_SWITCH_IN_PROGRESS()
//				if cached_player_ped_enum != get_current_player_ped_enum() 
//
//					SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped, false)
//			
//					force_ped_to_switch_damage_hint_system_status = 2
//					
//				endif 
//				
//			endif 
			
			
			if police_man[10].created
			
				SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped, false)
			
				target_switch_to_ped = number_of_selector_peds
				
				//cached_player_ped = get_current_player_ped_enum() 
			

				switch get_current_player_ped_enum() 
					
					case char_michael
					
						if not has_entity_been_damaged_by_entity(police_man[10].ped, player_ped_id())
							
							//if is_entity_in_angled_area(
							
								//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
									
									//set_label_as_triggered("fbi4_FFront", true)
	
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
									SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, TRUE)
									SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_franklin)
	
									target_switch_to_ped = selector_ped_trevor
									
									health_reduction_time = get_game_timer()
	
									force_ped_to_switch_damage_hint_system_status++
									
								//endif 
								
							//endif 
							
						endif 
						
					break 
					
					case char_franklin
					
						if not has_entity_been_damaged_by_entity(police_man[10].ped, player_ped_id())
							
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, TRUE)
							SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_trevor, SELECTOR_PED_michael, selector_ped_franklin)
	
							target_switch_to_ped = selector_ped_trevor
							
							health_reduction_time = get_game_timer()
			
							force_ped_to_switch_damage_hint_system_status++
							
						endif 
					
					break 

					case char_trevor
					
//						if michael_defensive_sphere_system_status = 1 //michael's defensive sphere is in the alley way
//						
//							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
//							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
//							SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, TRUE)
//							SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
//				
//							target_switch_to_ped = selector_ped_trevor
//							
//							health_reduction_time = get_game_timer()
//			
//							force_ped_to_switch_damage_hint_system_status++
//							
//						elif franklin_defensive_sphere_system_status = 1
//						
//							//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
//
//								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, true)
//								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
//								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
//								SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_michael, SELECTOR_PED_trevor, selector_ped_michael)
//								
//								target_switch_to_ped = selector_ped_trevor
//								
//								health_reduction_time = get_game_timer()
//			
//								force_ped_to_switch_damage_hint_system_status++
//							//endif 
//				
//						else 
//						
//							//if create_conversation(scripted_speech[0], "heataud", "fbi4_FFront", CONV_PRIORITY_medium)
//							
//								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
//								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
//								SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, TRUE)
//								SET_SELECTOR_PED_PRIORITY(selector_ped, selector_ped_franklin, SELECTOR_PED_trevor, selector_ped_michael)
//								
//								target_switch_to_ped = selector_ped_trevor
//								
//								health_reduction_time = get_game_timer()
//			
//								force_ped_to_switch_damage_hint_system_status++
//								
//							//endif 
//
//						endif 
						
					break
					
				endswitch
				
				//safety check incase the player is trevor when the helicopter is created and the above code does 
				//not progress. 
				if not does_entity_exist(helicopter[1].veh)
					force_ped_to_switch_damage_hint_system_status = 4
				endif 
				
			endif 

		break 
		
		case 4
		
//			if not IS_PLAYER_SWITCH_IN_PROGRESS()
//				if cached_player_ped_enum != get_current_player_ped_enum() 
//
//					SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped, false)
//			
//					force_ped_to_switch_damage_hint_system_status = 2
//					
//				endif 
//				
//			endif 

			if (get_current_player_ped_enum() != get_player_ped_enum_from_selector_slot(target_switch_to_ped))
			and (target_switch_to_ped != number_of_selector_peds)
			
				if lk_timer(health_reduction_time, 10000)
				
					SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped)
				
					//apply_damage_to_ped_system(player_ped_id(),)
					//snipers peds are tasked to shoot at the player with 100 accuracy. 

				endif 
				
			else 
			
				//force_ped_to_switch_damage_hint_system_status++
			
			endif
			
			if not does_entity_exist(helicopter[1].veh)
			
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_trevor, false)
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_michael, false)
			
				SET_SELECTOR_PED_DAMAGED(selector_ped, target_switch_to_ped, false)
				
				force_ped_to_switch_damage_hint_system_status++
				
			endif 
		
		break 
		
		case 5
		
		break 
		
	endswitch 

endproc 

///PURPOSE: Renders help text to remind player about michaels special ability
///    
proc michael_special_ability_help_text()

	if not fbi4_special_ability_help_activated
	
		if ((get_current_player_ped_enum() = char_michael) and (not does_cam_exist(sCamDetails.camID)))
		
			if michael_special_ability_time = 0
			
				michael_special_ability_time = get_game_timer()
				
			else 
			
				if lk_timer(michael_special_ability_time, 3000)

					if is_pc_version() 
						
						if is_using_keyboard_and_mouse(PLAYER_CONTROL)
						
							print_help("cntry_help11_KM")
						
						else 
						
							print_help("cntry_help11")
							
						endif 
						
					else 
					
						print_help("cntry_help11")
					
					endif 

					fbi4_special_ability_help_activated = true
					
				endif 
				
			endif 
		endif 
	endif 

endproc 

func bool fbi4_shootout()

	area_system()
	
	disable_friendly_fire_audio()
	
	//used in dialogue function snipers_dialogue to trigger specific line from trevor. Must be called before
	//ped_structure_are_all_enemies_dead as this will set the ped as no longer needed 
//	check_if_ped_killed_by_specific_player_character(police_man[8].ped, police_man_8_killed_by_trevor, char_trevor)
//	check_if_ped_killed_by_specific_player_character(police_man[9].ped, police_man_9_killed_by_trevor, char_trevor)
//	check_if_ped_killed_by_specific_player_character(police_man[31].ped, police_man_31_killed_by_trevor, char_trevor)
	
	check_if_sniper_killed_by_trevor(police_man[8].ped)
	check_if_sniper_killed_by_trevor(police_man[9].ped)
	check_if_sniper_killed_by_trevor(police_man[31].ped)
				
				
	//setup_player_ped_proofs()
	
	if shootout_master_controler_system()
		return true 
	endif 
	
	clear_player_react_explosion_anim()

	police_man_ai_system()
	
	backup_police_front_ai_system()
	
	backup_police_alley_ai_system()
	
	helicopter_0_system()
	
	helicopter_1_system()
	
	police_vehicle_explosion_system()
	
	if g_bMagdemoActive
		police_vehicle_siren_system()
	endif 
	
	michael_ai_system()
	
	trevor_ai_system()

	hot_swap_system()
	
	force_ped_to_switch_damage_hint_system()
	
	dialogue_system()
	
	michael_special_ability_help_text()

	army_truck_guy_ai_system()
	army_truck_guy_2_ai_system()
	
	shootout_music_event_system()
	
	return false 
	
endfunc 

func bool are_all_police_a_set_distance_away_from_ped(ped_index mission_ped, float dist)
							
	int i
	
	for i = 0 to count_of(police_man) - 1
		
		if does_entity_exist(police_man[i].ped)
			if not is_ped_injured(police_man[i].ped)
				
				if get_distance_between_coords(get_entity_coords(mission_ped), get_entity_coords(police_man[i].ped)) < dist
				
					return false
					
				endif 
			
			endif 
		endif 
		
	endfor 
	
	for i = 0 to count_of(backup_police_front) - 1
		
		if does_entity_exist(backup_police_front[i].ped)
			if not is_ped_injured(backup_police_front[i].ped)
				
				if get_distance_between_coords(get_entity_coords(mission_ped), get_entity_coords(backup_police_front[i].ped)) < dist
				
					return false
					
				endif 
			
			endif 
		endif 
		
	endfor 
	
	for i = 0 to count_of(backup_police_alley) - 1
		
		if does_entity_exist(backup_police_alley[i].ped)
			if not is_ped_injured(backup_police_alley[i].ped)
				
				if get_distance_between_coords(get_entity_coords(mission_ped), get_entity_coords(backup_police_alley[i].ped)) < dist
				
					return false
					
				endif 
			
			endif 
		endif 
		
	endfor 
	
	return true 
	
endfunc

proc michael_ai_system_2()

	if does_entity_exist(selector_ped.pedID[SELECTOR_PED_michael])
		
		if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])

			switch michael_ai_system_2_status 
	
				case 0
	
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						if get_current_player_ped_enum() = char_franklin //only runs when the player is franklin

							if (ped_structure_are_all_enemies_dead(police_man) and are_backup_police_dead(backup_police_front) and are_backup_police_dead(backup_police_alley))
							or not is_ped_in_combat(selector_ped.pedID[SELECTOR_PED_michael])
							or are_all_police_a_set_distance_away_from_ped(selector_ped.pedID[selector_ped_michael], 40)
							or lk_timer(flee_time, 15000)
														
								clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_michael])
								setup_buddy_attributes(selector_ped.pedID[SELECTOR_PED_michael], true)
								set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_michael], true)
								set_ped_keep_task(selector_ped.pedID[SELECTOR_PED_michael], true)
							
								open_sequence_task(seq)
									task_clear_look_at(null)
									//task_smart_flee_ped(null, player_ped_id(), 300.00, -1)
									task_smart_flee_coord(null, <<901.9453, -2383.3679, 29.2789>>, 5000, -1) 
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[SELECTOR_PED_michael], seq)
								clear_sequence_task(seq)
								
								RELEASE_PED_PRELOAD_VARIATION_DATA(selector_ped.pedID[SELECTOR_PED_michael])
								set_ped_as_no_longer_needed(selector_ped.pedID[SELECTOR_PED_michael])
								set_model_as_no_longer_needed(GET_PLAYER_PED_MODEL(char_michael))

								michael_ai_system_2_status++
							endif  
							
						endif 
						
					endif 
					
				break 
				
				case 1
				
				break 
				
			endswitch  
			
		endif  
		
	endif 

endproc 

proc franklin_ai_system_2()

	if does_entity_exist(selector_ped.pedID[SELECTOR_PED_franklin])
	
		if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])

			switch franklin_ai_system_2_status 
			
				case 0
				
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						if get_current_player_ped_enum() = char_michael //only runs when the player is michael
						
							set_ped_keep_task(selector_ped.pedID[SELECTOR_PED_franklin], true)
						
							open_sequence_task(seq)
								task_clear_look_at(null)
								task_smart_flee_ped(null, player_ped_id(), 300.00, -1)
							close_sequence_task(seq)
							task_perform_sequence(selector_ped.pedID[SELECTOR_PED_franklin], seq)
							clear_sequence_task(seq)
							
							franklin_ai_system_2_status++
							
						endif 
						
					endif 
				
				break 
				
				case 1
				
				break 
				
			endswitch 
			
		endif 
		
	endif 

endproc 

proc trevor_ai_system_2()

	if does_entity_exist(selector_ped.pedID[SELECTOR_PED_trevor])
		
		if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_trevor])

			switch trevor_ai_system_2_status 
	
				case 0
	
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						if get_current_player_ped_enum() = char_franklin //only runs when the player is franklin

							if (ped_structure_are_all_enemies_dead(police_man) and are_backup_police_dead(backup_police_front) and are_backup_police_dead(backup_police_alley))
							or not is_ped_in_combat(selector_ped.pedID[SELECTOR_PED_trevor])
							or are_all_police_a_set_distance_away_from_ped(selector_ped.pedID[selector_ped_trevor], 40)
							or lk_timer(flee_time, 15000)
														
								set_ped_keep_task(selector_ped.pedID[SELECTOR_PED_trevor], true)
							
								open_sequence_task(seq)
									task_clear_look_at(null)
									task_smart_flee_coord(null, <<901.9453, -2383.3679, 29.2789>>, 5000, -1) 
								close_sequence_task(seq)
								task_perform_sequence(selector_ped.pedID[SELECTOR_PED_trevor], seq)
								clear_sequence_task(seq)
								
								
								RELEASE_PED_PRELOAD_VARIATION_DATA(selector_ped.pedID[SELECTOR_PED_trevor])
								set_ped_as_no_longer_needed(selector_ped.pedID[SELECTOR_PED_trevor])
								set_model_as_no_longer_needed(GET_PLAYER_PED_MODEL(char_trevor))
								
								trevor_ai_system_2_status++
							endif  
							
						endif 
						
					endif 
					
				break 
				
				case 1
				
				break 
				
			endswitch  
			
		endif  
		
	endif 

endproc 

proc remove_buddy_blips_and_set_text()
					
	clear_prints()
	set_label_as_triggered("cntry_help6", true)
	
	if does_blip_exist(trevor_blip)
		remove_blip(trevor_blip)
	endif 
	
	if does_blip_exist(michael_blip)
		remove_blip(michael_blip)
	endif 
	
	if does_blip_exist(franklin_blip)
		remove_blip(franklin_blip)
	endif
	
endproc 

proc fbi4_switch_to_michael_or_franklin()

//	set_max_wanted_level(1)
//	set_player_wanted_level(player_id(), 1)
//	set_player_wanted_level_now(player_id())

	switch switch_to_michael_or_franklin_status
	
		case 0
		
			if not has_label_been_triggered("cntry_help6")
				if not is_any_text_being_displayed(locates_data)
					print_now("cntry_help6", default_god_text_time, 1)
					set_label_as_triggered("cntry_help6", true)
				endif 
			endif 
		
			if update_selector_hud(selector_ped)
			
//				if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_MICHAEL)
//	
//					if setup_hotswap_cam_to_michael()
//						remove_buddy_blips_and_set_text()
//						switch_to_michael_or_franklin_status++
//					endif 
//					
//				elif HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRANKLIN)
//				
//					if setup_hotswap_cam_to_franklin()
//						remove_buddy_blips_and_set_text()
//						switch_to_michael_or_franklin_status++
//					endif 
//					
//				endif 
				
				if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_FRANKLIN)
				
					if setup_hotswap_cam_to_franklin()
						remove_buddy_blips_and_set_text()
						switch_to_michael_or_franklin_status++
					endif 
					
				endif 
				
			else 
				
				//force switch after 12.5s
				if lk_timer(switch_to_franklin_time, 12500)
			
					make_selector_ped_selection(selector_ped, SELECTOR_PED_franklin)
			
					setup_hotswap_cam_to_franklin()
				
					remove_buddy_blips_and_set_text()
					
					switch_to_michael_or_franklin_status++
					
				endif 
			
			endif 
			
		break 
		
		case 1
		
			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails)	// Returns FALSE when the camera spline is complete
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails, switch_type_short, 0, 999, 999)
				
				//SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
				
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped)	
						
							//want michael to keep combatting
//							if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_michael])
//								set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_michael], true)
//								clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_michael])
//							endif 

							if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_franklin])
								set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_franklin], true)
								clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
							endif 
								
							if not is_ped_injured(selector_ped.pedID[SELECTOR_PED_trevor])
								set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_trevor], true)
								clear_ped_tasks(selector_ped.pedID[SELECTOR_PED_TREVOR])
							endif 
						
							sCamDetails.bPedSwitched = TRUE
						endif 
					endif 
				endif 
			
			else 
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
				
				
				if is_entity_in_angled_area(selector_ped.pedID[SELECTOR_PED_trevor], <<806.297, -2289.667, 50.598>>, <<796.823, -2421.828, 64.098>>, 37.000) //area arround trevor roof top   
				or ((get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_trevor])) > 30) and (not is_entity_on_screen(selector_ped.pedID[SELECTOR_PED_trevor])))
					DELETE_PED(selector_ped.pedID[SELECTOR_PED_trevor])
				endif 

				original_time = get_game_timer()
				
				flee_time = get_game_timer()
				
				Set_Replay_Mid_Mission_Stage_With_Name(4, "burn the truck", true)
				
				if is_vehicle_driveable(rubbish_truck.veh)
					set_entity_only_damaged_by_player(rubbish_truck.veh, true)
					set_entity_proofs(rubbish_truck.veh, false, false, false, false, false)
				endif 
				
				DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(locates_data, TRUE)
				
				mission_flow = burn_rubbish_truck

			endif 
	
		break 
		
	endswitch 
	
	//************************** remove once shootout is put back in ****************************
	//michael_ai_system_2()
	//franklin_ai_system_2()
	//army_truck_guy_ai_system()
	//army_truck_guy_2_ai_system()

endproc 

proc devin_ai_system()

	if not is_ped_injured(devin.ped)
	
		switch devin_ai_system_status 
		
			case 0
			
				if not is_entity_at_coord(devin.ped, devin.pos, <<1.5, 1.5, 2.0>>)
					if has_ped_task_finished_2(devin.ped, script_task_perform_sequence)
						
						set_blocking_of_non_temporary_events(devin.ped, true)
						clear_ped_tasks(devin.ped)
						
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null, devin.pos, pedmove_walk, -1)
							task_turn_ped_to_face_entity(null, player_ped_id())
						close_sequence_task(seq)
						task_perform_sequence(devin.ped, seq)
						close_sequence_task(seq)
						
					endif 
				endif 		
			
			break 
			
			case 2
			
			break 
			
		endswitch 
		
	else 
	
		if not devin.created 
			
			request_model(devin.model)
			request_model(devin_car.model)
			request_model(devins_phone.model)
			
			request_anim_dict("missfbi4")
			
//			request_vehicle_recording(18, "lkcountry")
//			request_vehicle_recording(19, "lkcountry")
//			request_vehicle_recording(devin_car.recording_number, "lkcountry")
//			request_vehicle_recording(21, "lkcountry")
			
			if has_model_loaded(devin.model)
			and has_model_loaded(devin_car.model)
			and has_model_loaded(devins_phone.model)
			and has_anim_dict_loaded("missfbi4")
//			and has_vehicle_recording_been_loaded(18, "lkcountry")
//			and has_vehicle_recording_been_loaded(19, "lkcountry")
//			and has_vehicle_recording_been_loaded(devin_car.recording_number, "lkcountry")
//			and has_vehicle_recording_been_loaded(21, "lkcountry")
			
				devin_car.veh = create_vehicle(devin_car.model, devin_car.pos, devin_car.heading)
				set_vehicle_doors_locked(devin_car.veh, vehiclelock_lockout_player_only)
				remove_vehicle_window(devin_car.veh, sc_window_front_left) //sc_window_list
				remove_vehicle_window(devin_car.veh, sc_window_front_right)
				set_vehicle_on_ground_properly(devin_car.veh)
				set_vehicle_colours(devin_car.veh, 0, 0)
			
				create_npc_ped_on_foot(devin.ped, char_devin, devin.pos, devin.heading)
				setup_buddy_attributes(devin.ped)
				setup_relationship_contact(devin.ped, true)
				set_blocking_of_non_temporary_events(devin.ped, true)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_BERD, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_TORSO, 2, 1)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_LEG, 2, 1)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_HAND, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_FEET, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_TEETH, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_SPECIAL, 4, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_DECL, 1, 0)
				SET_PED_COMPONENT_VARIATION(devin.ped, PED_COMP_JBIB, 0, 0)
				
				devins_phone.obj = create_object(devins_phone.model, get_offset_from_entity_in_world_coords(devin.ped, <<0.0, 0.0, 5.0>>))
				attach_entity_to_entity(devins_phone.obj, devin.ped, get_ped_bone_index(devin.ped, BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>) 

				cutscene_pos = get_entity_coords(devin_car.veh)//<<1377.417, -2065.475, 52.675>>
				cutscene_rot = get_entity_rotation(devin_car.veh)
				devin_scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
				TASK_SYNCHRONIZED_SCENE(devin.ped, devin_scene_id, "missfbi4", "Idle_Loop_Devin", INSTANT_BLEND_IN, slow_BLEND_OUT, synced_scene_use_physics)
				set_synchronized_scene_looped(devin_scene_id, true)

				devin_ai_system_status++
				
			endif 
			
		endif 
		
	endif 

endproc

proc reset_pass_cutscene()
	
	#IF IS_DEBUG_BUILD

	if widget_reset_cutscene
	
		clear_ped_tasks_immediately(player_ped_id())
	
		if is_playback_going_on_for_vehicle(rubbish_truck.veh)
			stop_playback_recorded_vehicle(rubbish_truck.veh)
		endif 
		
		DELETE_VEHICLE(rubbish_truck.veh)
		DELETE_VEHICLE(devin_car.veh)
		DELETE_ped(devin.ped)
		
		destroy_all_cams()
		
		fbi4_pass_cutscene_status = 0
		
		widget_reset_cutscene = false
	
	endif 
	
	#endif 

endproc

func bool fbi4_pass_cutscene()

	int i

	switch fbi4_pass_cutscene_status 
	
		case 0
		
			//if has_cutscene_loaded()
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 0.0)
			
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
				if is_entity_attached(devins_phone.obj)
					detach_entity(devins_phone.obj)
				endif
				
				register_entity_for_cutscene(devins_phone.obj, "davenortons_phone",  CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, devins_phone.model)
				register_entity_for_cutscene(devin.ped, "devin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, devin.model)
				register_entity_for_cutscene(devin_car.veh, "devins_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, devin_car.model)
				
				if not is_ped_injured(security_guard[0].ped)
					register_entity_for_cutscene(security_guard[0].ped, "FIB_Security_Guy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, security_guard[0].model)
				endif 
				
				if not is_ped_injured(security_guard[1].ped)
					register_entity_for_cutscene(security_guard[1].ped, "FIB_Security_Guy^1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, security_guard[1].model)
				endif 
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
				
				start_cutscene()
				
				
				
				//deactivates fail and ai checks for security_guards
				for i = 0 to count_of(security_guard) - 1
					security_guard_ai_system_status[i] = 23
				endfor 
				
				fbi4_pass_cutscene_status++
			
			endif 
		
		break 
		
		case 1
		
			if is_cutscene_playing()

				SET_PED_USING_ACTION_MODE(player_ped_id(), false)
		
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-2556.251, 1908.599, 167.581>>, <<-2552.673, 1915.530, 171.481>>, 9.1, <<-2551.9309, 1912.0355, 168.0432>>, 244.1441, <<15.0, 15.0, 15.0>>)//, false) //2.5, 5.5, 2.0
				//reposition_players_last_vehicle(<<1383.1658, -2055.0244, 50.9987>>, 102.2)
				set_players_last_vehicle_to_vehicle_gen(<<-2551.9309, 1912.0355, 168.0432>>, 244.1441)

				clear_area(<<-2551.9309, 1912.0355, 168.0432>>, 10000.0, true)

				clear_ped_prop(player_ped_id(), anchor_head)

				fbi4_pass_cutscene_status++
			
			endif 
		
		break 
		
		case 2
		
			if is_cutscene_active()
			
				printstring("devins_car pos = ")
				printvector(get_entity_coords(devin_car.veh))
				printnl()
			
				if not WAS_CUTSCENE_SKIPPED()

					if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)) 
						force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
						SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
						force_ped_ai_and_animation_update(player_ped_id())
					endif 
					
				else 
				
					SET_CUTSCENE_FADE_VALUES(false, false, true, false)
					
					fbi4_pass_cutscene_status = 22
				
				endif 
				
			else 
			
				if does_entity_exist(devin.ped)
					delete_ped(devin.ped)
				endif 
				
				for i = 0 to count_of(security_guard) - 1
					if does_entity_exist(security_guard[i].ped)
						delete_ped(security_guard[i].ped)
					endif 
				endfor 
						
				//delete_vehicle(devin_car.veh)
						
				end_cutscene_no_fade(false, false)
			
				mission_passed()
			
			endif 
		
		break 
		
		case 22
		
			if is_cutscene_active()
			
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("michael", GET_PLAYER_PED_MODEL(char_michael)) 
					force_ped_motion_state(player_ped_id(), ms_on_foot_walk, false)
					SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000) 
					force_ped_ai_and_animation_update(player_ped_id())
				endif 
					
			else 
			
				if does_entity_exist(devin.ped)
					delete_ped(devin.ped)
				endif 
				
				for i = 0 to count_of(security_guard) - 1
					if does_entity_exist(security_guard[i].ped)
						delete_ped(security_guard[i].ped)
					endif 
				endfor 
						
				//delete_vehicle(devin_car.veh)
				
				REPLAY_STOP_EVENT()
				
				end_cutscene()
				
				mission_passed()
			endif 
	

		break 
		
	endswitch 

	return false 
	
endfunc 

proc end_of_shootout_dialogue_system()
	
	if not is_any_text_being_displayed(locates_data)

		switch end_of_shootout_dialogue_status 
		
			case 0

				//removed as franklin and trevor might be deleted by this point
				remove_ped_for_dialogue(scripted_speech[0], 0)
				remove_ped_for_dialogue(scripted_speech[0], 2)
				
				trigger_music_event("fbi4_SHOOTOUT_MID_MA")
				
				set_label_as_triggered("fbi4_SHOOTOUT_MID_MA", true)
				
				//added as null as franklin and trevor might have be deleted by this point
				add_ped_for_dialogue(scripted_speech[0], 0, null, "michael") 
				add_ped_for_dialogue(scripted_speech[0], 2, null, "TREVOR") 
			
				if create_conversation(scripted_speech[0], "HeatAud", "heat_end_1", CONV_PRIORITY_medium)
					end_of_shootout_dialogue_status = 2
				else 
					end_of_shootout_dialogue_status = 1
				endif 
			
			break 
			
			case 1
			
				if create_conversation(scripted_speech[0], "HeatAud", "heat_end_1", CONV_PRIORITY_medium)
					end_of_shootout_dialogue_status++
				endif 
				
			break 
			
			case 2
			
				if create_conversation(scripted_speech[0], "HeatAud", "heat_end_2", CONV_PRIORITY_medium)  
					end_of_shootout_dialogue_status++
				endif 
			
			break 
			
			case 3
			
			break 
			
		endswitch 
		
	endif 

endproc 

proc petrol_can_help_system()
								
	switch petrol_can_help_system_status 
	
		case 0
		
			if not does_blip_exist(locates_data.vehicleBlip)
			and not does_blip_exist(locates_data.LocationBlip)
				if not is_ped_sitting_in_any_vehicle(player_ped_id())
					if is_entity_in_angled_area(rubbish_truck.veh, <<1049.886, -1826.542, 30.823>>, <<1030.960, -1829.777, 35.423>>, 28.5)  
						print_help("cntry_help9")
						petrol_can_help_system_status++
					endif 
				endif 
			endif 

		break 
		
		case 1
		
			WEAPON_TYPE players_weapon 
			
			IF GET_CURRENT_PED_WEAPON(player_ped_id(), players_weapon)
				IF players_weapon = WEAPONTYPE_PETROLCAN
					IF IS_CONTROL_PRESSED(player_control,INPUT_ATTACK)
						
						if IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("cntry_help9")
							clear_help()
						endif 
						
						petrol_can_help_system_status++
					endif 
				endif 
			endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch 
	
endproc 

proc burn_rubbish_truck_music_event_system()
	
	switch burn_rubbish_truck_music_event_system_status 
	
		case 0
		
			if is_ped_sitting_in_vehicle(player_ped_id(), rubbish_truck.veh)
				trigger_music_event("FBI4_ENTER_VEHICLE_MA")
				set_label_as_triggered("FBI4_ENTER_VEHICLE_MA", true)
				burn_rubbish_truck_music_event_system_status++
			endif 
		
		break 
		
		case 1
		
			weapon_type players_weapon
			if get_current_ped_weapon(player_ped_id(), players_weapon)
				if (players_weapon = WEAPONTYPE_PETROLCAN)
					trigger_music_event("FBI4_PETROL")
					burn_rubbish_truck_music_event_system_status = 2
				endif 
			endif 
			
			prepare_music_event("FBI4_PETROL_EXPLODE")
			
			if is_entity_dead(rubbish_truck.veh)
				trigger_music_event("FBI4_PETROL_EXPLODE")
				burn_rubbish_truck_music_event_system_status = 2
			endif 
			
		break 
		
		case 2
		
			if is_entity_dead(rubbish_truck.veh)
				trigger_music_event("FBI4_PETROL_EXPLODE")
				burn_rubbish_truck_music_event_system_status++
			endif 
		
		break 
		
		case 3
		
		break 
		
	endswitch 
	
endproc 

proc audio_explosion_system()

	switch audio_explosion_system_status 
	
		case 0
		
			if get_distance_between_coords(get_entity_coords(player_ped_id()), get_away_vehicle.coords) < 100
			
				REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_04_HEAT_GARBAGE_TRUCK_EXPLODE")
				
				audio_explosion_system_status++
				
			endif 

		break 
		
		case 1
		
			if is_entity_dead(rubbish_truck.veh)
			
				play_sound_from_coord(rubbish_truck_explosion_sound, "Garbage_Truck_Explosion", get_entity_coords(rubbish_truck.veh, false), "FBI_04_HEAT_SOUNDS")
				
				audio_explosion_system_status++
			
			endif 
		
		break 
		
		case 2
		
			if has_sound_finished(rubbish_truck_explosion_sound)
				release_script_audio_bank()
				audio_explosion_system_status++
			endif 	
		
		break 
		
		case 3
		
		break 
		
	endswitch 

endproc 

proc setup_players_petrol_can()
					
	if not has_ped_got_weapon(player_ped_id(), WEAPONTYPE_PETROLCAN)
		give_weapon_to_ped(player_ped_id(), WEAPONTYPE_PETROLCAN, 5000, false, false)
	else 
		if get_ammo_in_ped_weapon(player_ped_id(), WEAPONTYPE_PETROLCAN) < 5000
			set_ped_ammo(player_ped_id(), WEAPONTYPE_PETROLCAN, 5000)
		endif 
	endif 
	
endproc 

func bool burn_rubbish_truck_master_flow_system()

	//safety check in case player destroys the truck inside the larger area allowance before hitting the 
	//braking range
	if is_entity_dead(rubbish_truck.veh)
		clear_player_wanted_level(player_id())
		burn_rubbish_truck_master_flow_system_status = 3
	endif 

	switch burn_rubbish_truck_master_flow_system_status 
	
		case 0
		
			if is_vehicle_driveable(rubbish_truck.veh)
				set_vehicle_doors_locked(rubbish_truck.veh, vehiclelock_unlocked)
				deactivate_vehicle_proofs(rubbish_truck.veh)
			endif 
			
			burn_rubbish_truck_master_flow_system_status++
		
		break 
	
		case 1
		
			if not has_label_been_triggered("FBI4_ENTER_VEHICLE_MA")
				if is_ped_sitting_in_vehicle(player_ped_id(), rubbish_truck.veh)
					trigger_music_event("FBI4_ENTER_VEHICLE_MA")
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					set_label_as_triggered("FBI4_ENTER_VEHICLE_MA", true)
				endif 
			endif 

				
			if is_player_at_location_in_vehicle(locates_data, get_away_vehicle.coords, <<30.00, 30.00, LOCATE_SIZE_HEIGHT>>, false, rubbish_truck.veh, "cntry_god27", "cntry_god14", "cntry_god13", false)  		
			//if does_blip_exist(locates_data.LocationBlip)
				//if is_entity_in_angled_area(player_ped_id(), <<1110.420, -1255.806, 18.992>>, <<1124.563, -1244.835, 23.492>>, 12.6)   
		
					clear_mission_locate_stuff(locates_data)
					
					setup_players_petrol_can()
				
					SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET(rubbish_truck.veh, true)
				
					//BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(rubbish_truck.veh, 5)
					
					print_now("cntry_god28", default_god_text_time, 1)
				
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
					burn_rubbish_truck_master_flow_system_status = 3
					
				//endif 
		
			endif
			
			
			if is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<100.00, 100.00, 100.00>>)
				
				if burn_truck_time = 0
					burn_truck_time = get_game_timer()
				endif 
				
				if lk_timer(burn_truck_time, 100000)
					setup_players_petrol_can()
					burn_rubbish_truck_distance_saftey_check_active = true
					burn_rubbish_truck_master_flow_system_status = 3
				endif 
				
			else 
			
				burn_rubbish_truck_distance_saftey_check_active = false
			
			endif 
			
		break 
		
		case 2
		
			//if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(rubbish_truck.veh, 5)

				//print_now("cntry_god28", default_god_text_time, 1)

				burn_rubbish_truck_master_flow_system_status++
			
			//endif 
		
		break 
		
		case 3
		
			
			
			if is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<100.00, 100.00, 100.00>>)
				
				if burn_truck_time = 0
					burn_truck_time = get_game_timer()
				endif 
				
				if lk_timer(burn_truck_time, 100000)
					burn_rubbish_truck_distance_saftey_check_active = true
				endif 
				
			else 
			
				burn_rubbish_truck_distance_saftey_check_active = false
			
			endif 
			
		

				
			//blipping system to ensure the truck is exploded in the correct area. 
			
			if not burn_rubbish_truck_distance_saftey_check_active
			
				if not is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<40.00, 40.00, 15.00>>)
					
					if does_blip_exist(rubbish_truck.blip)
						remove_blip(rubbish_truck.blip)
					endif 

					rubbish_truck_out_of_burning_area = true
					
					is_player_at_location_in_vehicle(locates_data, get_away_vehicle.coords, <<20.00, 20.00, 10.00>>, true, rubbish_truck.veh, "", "cntry_god14", "cntry_god13", true) 
		
				else 
				
					
					//forces player to brake truck if they get within 15m of getaway veh
					if not brake_truck_if_player_gets_close_to_get_away_vehicle
						if is_ped_sitting_in_vehicle(player_ped_id(), rubbish_truck.veh)
							if is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<15.00, 15.00, 10.00>>)
								if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(rubbish_truck.veh, 5)
									brake_truck_if_player_gets_close_to_get_away_vehicle = true
								endif 
							endif 
						endif 
					endif 
					
					//----
//					//makes sure the first time the truck is blipped is when the player exits it.
					//if not rubbish_truck_out_of_burning_area
						if not blip_truck_on_leaving_on_first_occasion
							if not does_blip_exist(rubbish_truck.blip)
								if not is_ped_sitting_in_vehicle(player_ped_id(), rubbish_truck.veh)
									
									if does_blip_exist(locates_data.LocationBlip)
										remove_blip(locates_data.LocationBlip)
									endif 
									
									if does_blip_exist(locates_data.vehicleBlip)
										remove_blip(locates_data.vehicleBlip)
									endif 
									
									print_now("cntry_god25", 12000, 1)
									
									rubbish_truck.blip = create_blip_for_entity(rubbish_truck.veh, true)

									blip_truck_on_leaving_on_first_occasion = true 
								endif 
							endif 
						endif 
					//endif 
					//--

					if is_entity_at_coord(rubbish_truck.veh, get_away_vehicle.coords, <<20.00, 20.00, 10.00>>)

						if does_blip_exist(locates_data.LocationBlip)
							remove_blip(locates_data.LocationBlip)
						endif 
						
						if does_blip_exist(locates_data.vehicleBlip)
							remove_blip(locates_data.vehicleBlip)
						endif 
						
						if not does_blip_exist(rubbish_truck.blip)
							if blip_truck_on_leaving_on_first_occasion
								rubbish_truck.blip = create_blip_for_entity(rubbish_truck.veh, true)
							endif 
						endif 

						rubbish_truck_out_of_burning_area = false
						
					else 

						if rubbish_truck_out_of_burning_area
							//if player enters the 30m locate to progress into this case (case 3) then drives out of
							//40m locate rubbish_truck_out_of_burning_area is set. If the player then drives the 
							//truck back into 40m then gets out the red blip for the truck will be added but 
							//the locates will still be running so it will add another blip for the truck to get
							//back in the truck. Wrapping this up in does_blip_exist will stop this and removing
							//the locates_data.LocationBlip and vehicle blip will kill any locate blips on first
							//attempt of leaving the truck. There after the player will need to drive the truck
							//to 20m locate to activate the red blip again.
							if not does_blip_exist(rubbish_truck.blip)
								is_player_at_location_in_vehicle(locates_data, get_away_vehicle.coords, <<0.01, 0.01, 10.00>>, true, rubbish_truck.veh, "cntry_god24", "cntry_god14", "cntry_god13", true) //<<20.00, 20.00, 10.00>>
							endif 
						endif 
					
					endif 


				endif 
				
			else 
			
				if not blip_truck_on_leaving_on_first_occasion
					if not does_blip_exist(rubbish_truck.blip)
						print_now("cntry_god25", 12000, 1)
						rubbish_truck.blip = create_blip_for_entity(rubbish_truck.veh, true)
						blip_truck_on_leaving_on_first_occasion = true 
					endif 
				else 
					if not does_blip_exist(rubbish_truck.blip)
						rubbish_truck.blip = create_blip_for_entity(rubbish_truck.veh, true)
					endif 	
				endif 

				if does_blip_exist(locates_data.LocationBlip)
					remove_blip(locates_data.LocationBlip)
				endif 
				
				if does_blip_exist(locates_data.vehicleBlip)
					remove_blip(locates_data.vehicleBlip)
				endif 

			endif 
			

			if is_entity_dead(rubbish_truck.veh)
			
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
				clear_mission_locate_stuff(locates_data)
			
				SET_ALL_RANDOM_PEDS_FLEE(player_id(), true) //stops ambient peds from attacking the player when the player makes the phone call.
				
				clear_player_wanted_level(player_id())
			
				clear_help()
				
				clear_prints()
				
				if not is_entity_in_water(rubbish_truck.veh)
					add_explosion(get_entity_coords(rubbish_truck.veh, false), exp_tag_grenade, 2.0)
				endif 
				
				//play_sound_from_coord(-1, "Garbage_Truck_Explosion", get_entity_coords(rubbish_truck.veh, false), "FBI_04_HEAT_SOUNDS")
				
				if does_blip_exist(rubbish_truck.blip)
					remove_blip(rubbish_truck.blip)
				endif 
				
//				SET_VEHICLE_AS_NO_LONGER_NEEDED(rubbish_truck.veh)
//				set_model_as_no_longer_needed(rubbish_truck.model)

				original_time = get_game_timer()
				
				get_away_car_time = get_game_timer()

				return true 

			endif 
		
		break
		
		case 4
		
		break 
		
	endswitch 

	return false 
	
endfunc 

func int get_furthest_away_pos_to_ped(ped_index mission_ped, vector &node[])

	float smallest_distance_between_vectors
	float current_distance_between_vectors
	int current_node = 0
	
	int i = 0

	for i = 0 to count_of(node) - 1
	
		current_distance_between_vectors = get_distance_between_coords(GET_ENTITY_COORDS(mission_ped), node[i]) 

		if i = 0
		
			smallest_distance_between_vectors = current_distance_between_vectors
			current_node = 0
		
		else 
			
			if current_distance_between_vectors > smallest_distance_between_vectors
				smallest_distance_between_vectors = current_distance_between_vectors
				current_node = i
			endif 
		
		endif 
		
	endfor 
	
	return current_node
	
endfunc

/// PURPOSE: Sets the get away vehicle as a misison entity otherwise if you enter it and leave it it will be 
///         set as no longer needed and potentialy cleaned up  
proc set_getaway_vehicle_as_a_mission_entity()

	if not does_entity_exist(get_away_car.veh)

		get_away_car.veh = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)
	
		if does_entity_exist(get_away_car.veh)
			if is_vehicle_driveable(get_away_car.veh)
				
				set_entity_as_mission_entity(get_away_car.veh, true, true)
				set_entity_proofs(get_away_car.veh, false, true, true, false, false)

			endif 
		endif 
		
	endif 
	
endproc 

//PURPOSE: resets the get away vehicle proofs
proc reset_get_away_vehicle_proofing_system()

	if does_entity_exist(get_away_car.veh)

		if not does_entity_exist(rubbish_truck.veh)
			if is_vehicle_driveable(get_away_car.veh)
				if is_ped_sitting_in_vehicle(player_ped_id(), get_away_car.veh)
				or lk_timer(get_away_car_time, 3500)
					
					set_entity_proofs(get_away_car.veh, false, false, false, false, false)
					
				endif 
			endif 
		
		else
		
			if is_entity_dead(rubbish_truck.veh)
				if is_vehicle_driveable(get_away_car.veh)
					if is_ped_sitting_in_vehicle(player_ped_id(), get_away_car.veh)
			
						set_entity_proofs(get_away_car.veh, false, false, false, false, false)
					
					endif 
				endif
			endif 
		
		endif 
	
	endif 

endproc 

proc take_off_mask_anim_system()
	
	switch take_off_mask_anim_system_status 
	
		case 0

			request_anim_dict("misscommon@std_take_off_masks")
		
			if has_anim_dict_loaded("misscommon@std_take_off_masks")
				if is_ped_sitting_in_any_vehicle(player_ped_id())
				
					vehicle_index players_current_car
					
					if get_current_player_vehicle(players_current_car)
					
						if is_this_model_a_car(get_entity_model(players_current_car))
				
							disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)

							if get_cam_view_mode_for_context(cam_view_mode_context_in_vehicle) != cam_view_mode_first_person 
							
								task_play_anim(player_ped_id(), "misscommon@std_take_off_masks", "take_off_mask_ds") 
					
								take_off_mask_anim_system_status++ 
								
							else
							
								take_off_mask_anim_system_status = 2
							
							endif 
							
						elif is_this_model_a_bike(get_entity_model(players_current_car))
							
							if IS_PED_WEARING_HELMET(player_ped_id())
							
								take_off_mask_anim_system_status = 2
								
							endif 
						
						endif 
					
					endif 
					
				endif 
			endif 

		break 
		
		case 1
		
			disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
			if is_entity_playing_anim(player_ped_id(), "misscommon@std_take_off_masks", "take_off_mask_ds")
				if GET_ENTITY_ANIM_CURRENT_TIME(player_ped_id(), "misscommon@std_take_off_masks", "take_off_mask_ds") > 0.25

					IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[CHAR_FRANKLIN] = COMP_TYPE_PROPS 
						set_ped_comp_item_current_sp(player_ped_id(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, false) //prop mask
					else 
						set_ped_comp_item_current_sp(player_ped_id(), COMP_TYPE_SPECIAL2, SPECIAL2_P1_NONE, false) //variation mask
					endif 
					
					take_off_mask_anim_system_status++ 
					
				endif 

			endif 
		
		break 
		
		case 2
		
			disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
			if not is_entity_playing_anim(player_ped_id(), "misscommon@std_take_off_masks", "take_off_mask_ds")
				
				remove_anim_dict("misscommon@std_take_off_masks")
				
				take_off_mask_anim_system_status++
				
			endif 
		
		break 
		
		case 3
		
		break
		
	endswitch 

endproc 


proc fbi4_phone_call_to_michael()

	audio_explosion_system()//will cleanup audio bank once sound has finished.
	
	set_getaway_vehicle_as_a_mission_entity()
	
	take_off_mask_anim_system()

	switch phone_call_to_michael_status 
	
		case 0
		
			cached_player_pos = get_entity_coords(player_ped_id())
			
			if is_vehicle_driveable(get_away_car.veh)
			
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
				get_away_car.blip = create_blip_for_entity(get_away_car.veh)
				print_now("cntry_god35", default_god_text_time, 1) 
			endif 
			
			add_ped_for_dialogue(scripted_speech[0], 0, null, "michael")

			original_time = get_game_timer()

			phone_call_to_michael_status++
		
		break 
		
		case 1
		
			if is_vehicle_driveable(get_away_car.veh)
				if is_ped_sitting_in_vehicle(player_ped_id(), get_away_car.veh)
				
					if does_blip_exist(get_away_car.blip)
						remove_blip(get_away_car.blip)
					endif 
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					print_now("cntry_god36", default_god_text_time, 1) 
					
					phone_call_to_michael_status = 2
					
					//DISABLE_TAXI_HAILING(true)
					
				endif 
				
			else 
			
				if does_blip_exist(get_away_car.blip)
					remove_blip(get_away_car.blip)
				endif 
				
				print_now("cntry_god36", default_god_text_time, 1) 
				
				phone_call_to_michael_status = 2
			
			endif 
			
			//incase player walks away
			if get_distance_between_coords(get_entity_coords(player_ped_id()), cached_player_pos) > 200
				phone_call_to_michael_status = 2
			endif 

		break 
	
		case 2
			
			if get_distance_between_coords(get_entity_coords(player_ped_id()), cached_player_pos) > 200

				if does_blip_exist(get_away_car.blip)
					remove_blip(get_away_car.blip)
				endif 
				
				if PLAYER_CALL_CHAR_CELLPHONE(scripted_speech[0], char_michael, "HeatAud", "fbi4_phoneM", CONV_PRIORITY_high) //TM44_CALL
				
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
					clear_this_print("cntry_god36")
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(rubbish_truck.veh)
					set_model_as_no_longer_needed(rubbish_truck.model)
					
					phone_call_to_michael_status++
				endif 
				
			endif 

		break 
		
		case 3
		
			request_model(get_player_ped_model(char_michael))
			request_model(michaels_car.model)
			request_model(devin.model)
			request_model(devin_car.model)
			request_vehicle_recording(301, "lkheat")
			request_vehicle_recording(302, "lkheat")

			if has_model_loaded(get_player_ped_model(char_michael)) 
			and has_model_loaded(michaels_car.model)
			and has_model_loaded(devin.model)
			and has_model_loaded(devin_car.model)
			and has_vehicle_recording_been_loaded(301, "lkheat")
			and has_vehicle_recording_been_loaded(302, "lkheat")
			and HAS_CELLPHONE_CALL_FINISHED()

				switch get_furthest_away_pos_to_ped(player_ped_id(), michael_car_node)
			
					case 0
					
						michaels_car.pos = <<-2855.6580, 1280.9719, 59.1824>>
						michaels_car.heading = 277.9344
						michaels_car.recording_number = 301
					
					break 
					
					case 1
					
						michaels_car.pos = <<-2033.4918, 1993.0211, 188.9565>>
						michaels_car.heading = 30.3781
						michaels_car.recording_number = 302
					
					break 
					
				endswitch 
				
				set_selector_ped_hint(selector_ped, selector_ped_michael, true)
				set_selector_ped_hint(selector_ped, selector_ped_franklin, false)
				set_selector_ped_hint(selector_ped, selector_ped_trevor, false)
			
				set_selector_ped_blocked(selector_ped, selector_ped_michael, false)
				set_selector_ped_blocked(selector_ped, selector_ped_franklin, false)
				set_selector_ped_blocked(selector_ped, selector_ped_trevor, false)


				clear_area(michaels_car.pos, 20.00, true)
				
				CREATE_PLAYER_VEHICLE(michaels_car.veh, CHAR_michael, michaels_car.pos, michaels_car.heading, false)
				start_playback_recorded_vehicle(michaels_car.veh, michaels_car.recording_number, "lkheat")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(michaels_car.veh, 5000)
				force_playback_recorded_vehicle_update(michaels_car.veh)
				set_playback_speed(michaels_car.veh, 0.0)
				set_entity_only_damaged_by_player(michaels_car.veh, true)
				
				set_roads_in_area(get_offset_from_coord_and_heading_in_world_coords(michaels_car.pos, michaels_car.heading, <<-200.00, 200.00, 200.00>>), get_offset_from_coord_and_heading_in_world_coords(michaels_car.pos, michaels_car.heading, <<200.00, -200.00, -200.00>>), false)

				create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_michael], char_michael, michaels_car.veh, vs_driver, false)
				SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_michael], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
				set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_michael], true)
				add_ped_for_dialogue(scripted_speech[0], 0, selector_ped.pedID[SELECTOR_PED_michael], "michael")	
				
				set_selector_ped_hint(selector_ped, SELECTOR_PED_MICHAEL, true)
				
				cached_player_pos = get_entity_coords(player_ped_id())
				
				mission_flow = switch_to_michael
			
			endif 
			
		break 

	endswitch 
	
endproc 

proc switch_to_michael_vehicle_recording_controller()
						
	if IS_PLAYER_SWITCH_IN_PROGRESS()

		SWITCH_STATE camera_switch_state
		camera_switch_state = GET_PLAYER_SWITCH_STATE() 
		
		
		switch switch_to_michael_vehicle_recording_controller_status
		
			case 0
			
				IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				   IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() <= 2

						if is_vehicle_driveable(michaels_car.veh)
							
							clear_area(get_entity_coords(michaels_car.veh), 200, true)
							
							if is_playback_going_on_for_vehicle(michaels_car.veh)
				
								set_playback_speed(michaels_car.veh, 1.0)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(michaels_car.veh, (5000 - GET_TIME_POSITION_IN_RECORDING(michaels_car.veh)))
								force_playback_recorded_vehicle_update(michaels_car.veh)
										
							else 
							
								start_playback_recorded_vehicle(michaels_car.veh, michaels_car.recording_number, "lkheat")
								skip_time_in_playback_recorded_vehicle(michaels_car.veh, 5000)
								force_playback_recorded_vehicle_update(michaels_car.veh)

							endif 

						endif 

						switch_to_michael_vehicle_recording_controller_status++
						
						//script_assert("test 2")
						
					endif 
					
				endif 
			
			break 
			
			case 1
			
				IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				   IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() <= 1

						if is_vehicle_driveable(michaels_car.veh)
							
							clear_area(get_entity_coords(michaels_car.veh), 200, true)
							
							if is_playback_going_on_for_vehicle(michaels_car.veh)
				
								set_playback_speed(michaels_car.veh, 1.0)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(michaels_car.veh, (5000 - GET_TIME_POSITION_IN_RECORDING(michaels_car.veh)))
								force_playback_recorded_vehicle_update(michaels_car.veh)
										
							else 
							
								start_playback_recorded_vehicle(michaels_car.veh, michaels_car.recording_number, "lkheat")
								skip_time_in_playback_recorded_vehicle(michaels_car.veh, 5000)
								force_playback_recorded_vehicle_update(michaels_car.veh)

							endif 
							
							switch_to_michael_vehicle_recording_controller_status++
							
							//script_assert("test 1")

						endif 
						
					endif 
					
				endif 
				
			break 
			
			case 2
			
				IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				   IF GET_PLAYER_SWITCH_JUMP_CUT_INDEX() <= 0

						if is_vehicle_driveable(michaels_car.veh)
							
							clear_area(get_entity_coords(michaels_car.veh), 200, true)
							
							if is_playback_going_on_for_vehicle(michaels_car.veh)
				
								set_playback_speed(michaels_car.veh, 1.0)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(michaels_car.veh, (5000 - GET_TIME_POSITION_IN_RECORDING(michaels_car.veh)))
								force_playback_recorded_vehicle_update(michaels_car.veh)
										
							else 
							
								start_playback_recorded_vehicle(michaels_car.veh, michaels_car.recording_number, "lkheat")
								skip_time_in_playback_recorded_vehicle(michaels_car.veh, 5000)
								force_playback_recorded_vehicle_update(michaels_car.veh)

							endif 

						endif 

						switch_to_michael_vehicle_recording_controller_status++
						
						//script_assert("test 0")
						
					endif 
				endif 
			
			break 
			
			case 3

				if camera_switch_state >= SWITCH_STATE_OUTRO_HOLD

					if is_vehicle_driveable(michaels_car.veh)
					
						clear_area(get_entity_coords(michaels_car.veh), 200, true)
					
						if is_playback_going_on_for_vehicle(michaels_car.veh)
			
							set_playback_speed(michaels_car.veh, 1.0)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(michaels_car.veh, (5000 - GET_TIME_POSITION_IN_RECORDING(michaels_car.veh)))
							force_playback_recorded_vehicle_update(michaels_car.veh)
									
						else 
						
							start_playback_recorded_vehicle(michaels_car.veh, michaels_car.recording_number, "lkheat")
							skip_time_in_playback_recorded_vehicle(michaels_car.veh, 5000)
							force_playback_recorded_vehicle_update(michaels_car.veh)

						endif 
						
						set_entity_only_damaged_by_player(michaels_car.veh, false)

						//script_assert("OUTRO_HOLD")
						
						switch_to_michael_vehicle_recording_controller_status++

					endif 

				endif 
				
			break
			
			case 4
			
			break 
			
		endswitch  
		
	endif 
	
endproc 

proc fbi4_switch_to_michael()

	switch switch_to_michael_status
	
		case 0

			if IS_AUDIO_SCENE_ACTIVE("FBI_4_DESTROY_TRUCK")
				stop_audio_scene("FBI_4_DESTROY_TRUCK")
			endif 

			clear_prints()
			
			clear_player_wanted_level(player_id())
			set_fake_wanted_level(0)
			//g_allowmaxwantedlevelcheck = true
			set_max_wanted_level(5)
			set_create_random_cops(false) //dont want to create random cops 
			set_wanted_level_multiplier(0.2)
			
			vehicle_index players_last_veh
			
			//lets franklin be in the getaway car after the mission. for alwyns stuff.
			if get_current_player_vehicle(players_last_veh)
				STORE_VEH_DATA_FROM_VEH(PLAYER_PED_ID(), players_last_veh, g_sPlayerLastVeh[CHAR_FRANKLIN], g_vPlayerLastVehCoord[CHAR_FRANKLIN], g_fPlayerLastVehHead[CHAR_FRANKLIN], g_ePlayerLastVehState[CHAR_FRANKLIN], g_ePlayerLastVehGen[CHAR_FRANKLIN])
			endif 
			
			make_selector_ped_selection(selector_ped, selector_ped_michael)

			sCamDetails.pedTo = selector_ped.pedID[SELECTOR_PED_michael]

			switch_to_michael_status++
		
		break 
		
		case 1
		
			//IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails)	// Returns FALSE when the camera spline is complete
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails)
			
				//SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
				
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, false)	
							sCamDetails.bPedSwitched = TRUE
						endif 
					endif 
				endif 
				
				switch_to_michael_vehicle_recording_controller()

			else 
			
				remove_anim_dict("misscommon@std_take_off_masks")//incase the player decided to walk.
		
				set_roads_back_to_original(get_offset_from_coord_and_heading_in_world_coords(michaels_car.pos, michaels_car.heading, <<-200.00, 200.00, 200.00>>), get_offset_from_coord_and_heading_in_world_coords(michaels_car.pos, michaels_car.heading, <<200.00, -200.00, -200.00>>))
			
				if is_vehicle_driveable(michaels_car.veh)
					if is_playback_going_on_for_vehicle(michaels_car.veh)
						stop_playback_recorded_vehicle(michaels_car.veh)
					endif 
				endif 
				
				if does_entity_exist(get_away_car.veh)
					set_vehicle_as_no_longer_needed(get_away_car.veh)
				endif 
				

				start_audio_scene("FBI_4_GO_TO_MEETING")
			
				freeze_entity_position(michaels_car.veh, false)
			
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
				
				//REMOVE ONCE DOORS ADDED TO AMBIENT DOOR SYSTEM ADDED
//				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_lrggate_01c_l, <<-2559.19, 1910.86, 169.07>>, true, 0.0, 0.0, 1.0)
//				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_lrggate_01c_r, <<-2556.65, 1915.71, 169.07>>, true, 0.0, 0.0, -1.0)

				mission_flow = meet_devin

				
			endif 
		
		break 
		
	endswitch 
	
endproc 

proc fire_system()

	int i = 0

	if does_entity_exist(rubbish_truck.veh)
		if not is_entity_dead(rubbish_truck.veh)
		//if is_vehicle_driveable(rubbish_truck.veh)

			switch fire_system_status 
			
				case 0
				
//					printstring("test 0")
//					printnl()
//					
//					if not GET_CLOSEST_FIRE_POS(fire_start_pos, get_entity_coords(rubbish_truck.veh))
//						printstring("no fire")
//						printnl()
//					endif 

					if GET_CLOSEST_FIRE_POS(fire_start_pos, get_entity_coords(rubbish_truck.veh))
						
						printvector(fire_start_pos)
						printnl()
						
						if is_point_in_angled_area(fire_start_pos, get_offset_from_entity_in_world_coords(rubbish_truck.veh, <<-2.4, -0.4, -1.5>>), get_offset_from_entity_in_world_coords(rubbish_truck.veh, <<2.4, -0.4, 3.0>>), 11.5) 
						//or is_petrol_at_any_truck_node() 
						//if player hits vehicle offset node  struct flag to true = minitor trail point similar to get_trail_point
						//cycle through all flags then if true  
						

							float current_distance_between_vectors
							float smallest_distance_between_vectors
							int closest_node

							for i = 0 to count_of(fire) -1
							
								current_distance_between_vectors = get_distance_between_coords(fire_start_pos, get_offset_from_entity_in_world_coords(rubbish_truck.veh, fire[i].offset)) 

								if i = 0
								
									smallest_distance_between_vectors = current_distance_between_vectors
									closest_node = 0
									
								else 
								
									if current_distance_between_vectors < smallest_distance_between_vectors
										smallest_distance_between_vectors = current_distance_between_vectors
										closest_node = i
									endif 
										
								endif 
								
							endfor 
							
							fire[closest_node].fire = START_SCRIPT_FIRE(get_offset_from_entity_in_world_coords(rubbish_truck.veh, fire[closest_node].offset), 0, true)
							fire[closest_node].set = true
							
							fire_time = get_game_timer()
							
							fire_shot_up_index = closest_node
							fire_shot_down_index = closest_node
							
							fire_system_status++
							
						endif 
					endif 
						
				break 

				case 1
				
					int fire_set_count
					
					fire_set_count = 0
				
					if lk_timer(fire_time, 250)
						
						if fire_shot_up_index < count_of(fire) - 1
							fire_shot_up_index++
							fire[fire_shot_up_index].fire = START_SCRIPT_FIRE(get_offset_from_entity_in_world_coords(rubbish_truck.veh, fire[fire_shot_up_index].offset), 0, true)
							fire[fire_shot_up_index].set = true
						endif 
						
						if fire_shot_down_index > 0
							fire_shot_down_index--
							fire[fire_shot_down_index].fire = START_SCRIPT_FIRE(get_offset_from_entity_in_world_coords(rubbish_truck.veh, fire[fire_shot_down_index].offset), 0, true)
							fire[fire_shot_down_index].set = true
						endif 

						fire_time = get_game_timer()
						
						for i = 0 to count_of(fire) - 1
							if fire[i].set 
								fire_set_count++
							endif 
						endfor
						
						if fire_set_count = count_of(fire)
							fire_time = get_game_timer()
							fire_system_status++
						endif 
					
					endif 

				break 
				
				case 2
				
					if lk_timer(fire_time, 1000)
						for i = 0 to count_of(fire) - 1
							remove_script_fire(fire[i].fire)
						endfor
					endif 

				break 
				
			endswitch 
			
		else 
		
			//if lk_timer(fire_time, 1000)
			for i = 0 to count_of(fire) - 1
				remove_script_fire(fire[i].fire)
			endfor
			//endif 
			
			//explode the vehicle if it is not on fire to enure that it is completely destroyed and on fire.
			if GET_CLOSEST_FIRE_POS(fire_start_pos, get_entity_coords(rubbish_truck.veh, false))
				if get_distance_between_coords(fire_start_pos, get_entity_coords(rubbish_truck.veh, false)) > 2.0
					add_explosion(get_entity_coords(rubbish_truck.veh, false), exp_tag_grenade, 2.0)
				endif 
			endif 

			//add_explosion(get_entity_coords(rubbish_truck.veh, false), exp_tag_grenade, 2.0)
			
			original_time = get_game_timer()

			set_vehicle_as_no_longer_needed(rubbish_truck.veh)

		endif 
		
	endif 

endproc

proc set_buddies_no_longer_needed()

//	if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_michael])
//		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_michael])) > 20
//			if not IS_ENTITY_ON_SCREEN(selector_ped.pedID[SELECTOR_PED_michael])
//				//DELETE_PED(selector_ped.pedID[SELECTOR_PED_michael])
//				task_smart_flee_ped(selector_ped.pedID[SELECTOR_PED_michael], player_ped_id(), 200.00, -1)
//				set_ped_keep_task(selector_ped.pedID[SELECTOR_PED_michael], true)
//				RELEASE_PED_PRELOAD_VARIATION_DATA(selector_ped.pedID[SELECTOR_PED_michael])
//				set_ped_as_no_longer_needed(selector_ped.pedID[SELECTOR_PED_michael])
//				set_model_as_no_longer_needed(GET_PLAYER_PED_MODEL(char_michael))
//			endif 
//		endif 
//	endif
//	
//	if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_franklin])
//		if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[SELECTOR_PED_franklin])) > 20
//			if not IS_ENTITY_ON_SCREEN(selector_ped.pedID[SELECTOR_PED_franklin])
//				task_smart_flee_ped(selector_ped.pedID[SELECTOR_PED_franklin], player_ped_id(), 200.00, -1)
//				set_ped_keep_task(selector_ped.pedID[SELECTOR_PED_franklin], true)
//				RELEASE_PED_PRELOAD_VARIATION_DATA(selector_ped.pedID[SELECTOR_PED_franklin])
//				set_ped_as_no_longer_needed(selector_ped.pedID[SELECTOR_PED_franklin])
//				//DELETE_PED(selector_ped.pedID[SELECTOR_PED_franklin])
//				set_model_as_no_longer_needed(GET_PLAYER_PED_MODEL(char_franklin))
//			endif 
//		endif 
//	endif
	
	if DOES_ENTITY_EXIST(selector_ped.pedID[SELECTOR_PED_trevor])
		if not IS_ENTITY_ON_SCREEN(selector_ped.pedID[SELECTOR_PED_trevor])
			//DELETE_PED(selector_ped.pedID[SELECTOR_PED_trevor])
			task_smart_flee_ped(selector_ped.pedID[SELECTOR_PED_trevor], player_ped_id(), 200.00, -1)
			set_ped_keep_task(selector_ped.pedID[SELECTOR_PED_trevor], true)
			RELEASE_PED_PRELOAD_VARIATION_DATA(selector_ped.pedID[SELECTOR_PED_trevor])
			set_ped_as_no_longer_needed(selector_ped.pedID[SELECTOR_PED_trevor])
			set_model_as_no_longer_needed(GET_PLAYER_PED_MODEL(char_trevor))
		endif  
	endif
	
endproc 

proc reset_wanted_level_data()
	
//	if not reset_wanted_level
//		//if get_distance_between_coords(get_entity_coords(player_ped_id()), <<890.99, -2353.5, 30.13>>) > 100
//		if get_player_wanted_level(player_id()) = 0
//			
//			clear_player_wanted_level(player_id())
//			set_fake_wanted_level(0)
//			//g_allowmaxwantedlevelcheck = true
//			set_max_wanted_level(6)
//			set_create_random_cops(false) //dont want to create random cops 
//			set_wanted_level_multiplier(0.2)
//			
//			reset_wanted_level = true 
//			
//		endif 
//	endif 
	
endproc 

//PURPOSE: creates a get aay vehicle for the player if the missions has been launched off the debug menu or the get away car from the prep has been destroyed
proc create_debug_get_away_vehicle()

	if create_debug_getaway_car
		
		if not does_entity_exist(get_away_car.veh)
		
			//if get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(get_away_car.pos)) < 150
		
				request_model(get_away_car.model)
			
				if has_model_loaded(get_away_car.model)
			
					clear_area(get_away_car.pos, 10.00, true)
					get_away_car.veh = create_vehicle(get_away_car.model, get_away_car.pos, get_away_car.heading)
					set_entity_proofs(get_away_car.veh, false, true, true, false, false)
					set_vehicle_on_ground_properly(get_away_car.veh)
					
				endif 
				
			//endif 
		endif 
	endif 	

endproc

proc cleanup_fake_wanted_level()
	
	if not clenaup_fake_wanted_level
	
		if get_distance_between_coords(get_entity_coords(player_ped_id()), <<882.5773, -2347.3586, 29.3307>>) > 250.00

			clear_player_wanted_level(player_id())
			set_fake_wanted_level(0)
			set_max_wanted_level(1)
			set_create_random_cops(false) //dont want to create random cops 
			set_wanted_level_multiplier(0.1)
			
			enable_dispatch_services()
			
			BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE, false)
		    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, false)
		    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_AUTOMOBILE, false)
			BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER, false)
		    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_VEHICLE_REQUEST, false)
		   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_ROAD_BLOCK, false)
		    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, false)
		   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, false)
			
			clenaup_fake_wanted_level = true
			
		endif 
		
	endif 
	
endproc 

proc burn_rubbish_truck_audio_scene_system()
	
	switch burn_rubbish_truck_audio_scene_system_status 
	
		case 0
		
			if is_ped_sitting_in_vehicle(player_ped_id(), rubbish_truck.veh)
			
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_COPS_ARRIVE")
					STOP_AUDIO_SCENE("FBI_4_COPS_ARRIVE")
				endif 
				
				start_audio_scene("FBI_4_GO_TO_ALLEYWAY")
				
				burn_rubbish_truck_audio_scene_system_status++
			endif 
		
		break 
		
		case 1
		
			if does_blip_exist(rubbish_truck.blip)
			
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_GO_TO_ALLEYWAY")
					STOP_AUDIO_SCENE("FBI_4_GO_TO_ALLEYWAY")
				endif 
				
				start_audio_scene("FBI_4_DESTROY_TRUCK")
				
				burn_rubbish_truck_audio_scene_system_status++
				
			endif 
		
		break 
		
		case 2
		
			if is_entity_dead(rubbish_truck.veh)
			
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_GO_TO_ALLEYWAY")
					STOP_AUDIO_SCENE("FBI_4_GO_TO_ALLEYWAY")
				endif 
				
				burn_rubbish_truck_audio_scene_system_status++
			
			endif 
		
		break 
		
		case 3
		
		break 
		
	endswitch 

endproc 

func bool fbi4_burn_rubbish_truck()

	set_getaway_vehicle_as_a_mission_entity()
	create_debug_get_away_vehicle()
	reset_get_away_vehicle_proofing_system()

	end_of_shootout_dialogue_system()
	
	burn_rubbish_truck_music_event_system()
	
	reset_wanted_level_data()
	
	audio_explosion_system()
	
	burn_rubbish_truck_audio_scene_system()
	
	if burn_rubbish_truck_master_flow_system()
		
		mission_flow = phone_call_to_michael
		
		return true 
	endif 
	
//	petrol_can_help_system()
	
//	if is_point_in_angled_area(get_entity_coords(rubbish_truck.veh, false), <<886.633, -1839.647, 29.627>>, <<901.177, -1840.919, 34.127>>, 44.000)   
//		fire_system()
//	endif 

	michael_ai_system_2()
	
	franklin_ai_system_2()
	
	trevor_ai_system_2()

	//set_buddies_no_longer_needed()

	
	if not assets_cleaned_up
		if get_distance_between_coords(get_entity_coords(player_ped_id()), <<882.5773, -2347.3586, 29.3307>>) > 150.00
			cleanup_assets_after_shootout()
			add_scenario_blocking_area(<<965.2, -1693.0, -100.00>>, <<1144.9, -1929.1, 100.00>>) //blocks burning truck area
		endif 
	endif
	
	cleanup_fake_wanted_level()
	
	return false 

endfunc 

proc FBI_4_MCS_2_CONCAT_prestreaming_srl_system()
			
	if is_cutscene_active() or has_cutscene_loaded()
		if not FBI_4_MCS_2_CONCAT_prestreaming_srl
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF) 
			FBI_4_MCS_2_CONCAT_prestreaming_srl = true
		endif 
	else 
		if FBI_4_MCS_2_CONCAT_prestreaming_srl
			FBI_4_MCS_2_CONCAT_prestreaming_srl = false
		endif 
	endif 
	 
endproc 

func bool meet_devin_master_flow_system()

	switch meet_devin_master_flow_status
	
		case 0
		
			IS_PLAYER_AT_LOCATION_ANY_MEANS(locates_data, <<-2552.4431, 1911.0929, 167.9768>>, <<0.01, 0.01, LOCATE_SIZE_HEIGHT>>, false, "cntry_god5", true, true)
			
			if does_entity_exist(devin.ped)
			
				mocap_streaming_system(GET_ENTITY_COORDS(devin.ped), DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_UNLOAD_DIST, "FBI_4_MCS_2_CONCAT")
				if can_request_assets_for_cutscene_entity()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("michael", PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("devin", devin.ped)
					
					if not is_ped_injured(security_guard[0].ped)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("FIB_Security_Guy", security_guard[0].ped)
					endif 
					
					if not is_ped_injured(security_guard[1].ped)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("FIB_Security_Guy^1", security_guard[1].ped)
					endif 
					
				endif 
				FBI_4_MCS_2_CONCAT_prestreaming_srl_system()
			
						
				if does_blip_exist(locates_data.LocationBlip)
				
					if is_entity_in_angled_area(player_ped_id(), <<-2556.251, 1908.599, 167.581>>, <<-2552.673, 1915.530, 171.481>>, 9.1)
					
						if IS_AUDIO_SCENE_ACTIVE("FBI_4_GO_TO_MEETING")
							STOP_AUDIO_SCENE("FBI_4_GO_TO_MEETING")
						endif 
						
						if get_current_player_vehicle(players_last_vehicle)
							
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_last_vehicle, 4.0) 
							
							meet_devin_master_flow_status++
						
						else 
						
							if is_entity_in_angled_area(player_ped_id(), <<-2557.516, 1909.942, 167.877>>, <<-2554.610, 1915.979, 171.377>>, 4.0)
							
								if has_cutscene_loaded()
									if start_new_cutscene_no_fade()
										trigger_music_event("fbi4_DEPOT_STOP_TRACK")
										fbi4_pass_cutscene()
										return true 
									endif 
								endif 
								
							endif 
						
						endif 
						
					endif 

//				else 
//				
//					//draws caronna at correct position.
//					if does_blip_exist(locates_data.LocationBlip)
//						is_entity_at_coord(player_ped_id(), GET_ENTITY_COORDS(devin.ped), <<15.00, 15.00, LOCATE_SIZE_HEIGHT>>) 
//					endif 
					
				endif 
			endif
			
		break 
		
		case 1
		
		
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(players_last_vehicle, 4.0) 
				if has_cutscene_loaded()
					if start_new_cutscene_no_fade(TRUE, TRUE, TRUE, TRUE)
						trigger_music_event("fbi4_DEPOT_STOP_TRACK")
						fbi4_pass_cutscene()
						return true 
					endif 
				endif 
			endif 
		
		break 
		
		case 2
		
		break 
		
	endswitch 
	
	return false	

endfunc 

proc security_guard_ai_system()
	
	int i
	
	for i = 0 to count_of(security_guard) - 1
	
		if not is_ped_injured(security_guard[i].ped)
		
			if security_guard_ai_system_status[i] != 22
			
				if has_ped_been_harmed(security_guard[i].ped, security_guard[i].health, security_guard[i].created)
				//or has_player_antagonised_ped(security_guard[i].ped, 30.00, false)
				or is_ped_ragdoll(security_guard[i].ped)
				//or (is_entity_in_angled_area(player_ped_id()) area of lesters house
					
					security_guard_ai_system_status[0] = 22
					security_guard_ai_system_status[1] = 22
					
					attacked_lesters_security_fail = true
				
				endif 
			
			endif 

			switch security_guard_ai_system_status[i] 
			
				case 0
				
					task_look_at_entity(security_guard[i].ped, player_ped_id(), -1, slf_while_not_in_fov)
					//task_play_anim()
					
					security_guard_ai_system_status[i] = 1
				
				break 
			
				case 1
				
					if not is_entity_at_coord(security_guard[i].ped, security_guard[i].pos, <<0.3, 0.3, 2.0>>)
								
						if has_ped_task_finished_2(security_guard[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
						
							task_look_at_entity(security_guard[i].ped, player_ped_id(), -1, slf_while_not_in_fov)
							
							task_follow_nav_mesh_to_coord(security_guard[i].ped, security_guard[i].pos, pedmove_walk, -1, 0.2, enav_stop_exactly, security_guard[i].heading)  
							
						endif 
						
					else 
					
						if has_ped_task_finished_2(security_guard[i].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							if has_ped_task_finished_2(security_guard[i].ped)
							
								switch i 
								
									case 0
								
										open_sequence_task(seq)
											task_play_anim(null, "mini@strip_club@idles@bouncer@idle_a", "idle_a", normal_blend_in, normal_blend_out, -1, af_looping)
										close_sequence_task(seq)
										task_perform_sequence(security_guard[i].ped, seq)
										clear_sequence_task(seq)
								
									break 
									
									case 1
									
										open_sequence_task(seq)
											task_play_anim(null, "mini@strip_club@idles@bouncer@idle_c", "idle_c", normal_blend_in, normal_blend_out, -1, af_looping)
										close_sequence_task(seq)
										task_perform_sequence(security_guard[i].ped, seq)
										clear_sequence_task(seq)
									
									break 
									
								endswitch 
								
							endif 
							
						endif 
						
					endif 
				
				break 
				
				case 22

					set_blocking_of_non_temporary_events(security_guard[i].ped, false)
					set_ped_sphere_defensive_area(security_guard[i].ped, security_guard[i].run_to_pos, 2.0) 
					SET_PED_COMBAT_ATTRIBUTES(security_guard[i].ped, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(security_guard[i].ped, CA_CAN_CHARGE, TRUE)
					task_combat_hated_targets_around_ped(security_guard[i].ped, 200.00)

					security_guard_ai_system_status[i] = 23
					
				break 
				
				case 23
				
				break 
				
			endswitch 
			
		else 
		
			if not security_guard[i].created
			
				request_model(security_guard[i].model)
				request_anim_dict("mini@strip_club@idles@bouncer@idle_a")
				request_anim_dict("mini@strip_club@idles@bouncer@idle_c")
				
				if has_model_loaded(security_guard[i].model)
				and has_anim_dict_loaded("mini@strip_club@idles@bouncer@idle_a")
				and has_anim_dict_loaded("mini@strip_club@idles@bouncer@idle_c")
				
					setup_enemy(security_guard[i])
					setup_relationship_enemy(security_guard[i].ped, true)
					
				endif 
				
			endif 
		
		endif 
		
	endfor 

endproc 


proc fbi4_meet_devin()

	if meet_devin_master_flow_system()
		mission_flow = pass_cutscene
	endif 
	
	devin_ai_system()
	
	franklin_ai_system_2()
	
	security_guard_ai_system()

endproc 

proc remove_all_blips()

	int i = 0

	if does_blip_exist(ambulance_target_blip)
		remove_blip(ambulance_target_blip)
	endif 
	
	if does_blip_exist(player_cover_blip)
		remove_blip(player_cover_blip)
	endif 
		
	if does_blip_exist(bomb_blip)
		remove_blip(bomb_blip)
	endif 
	

	for i = 0 to count_of(police_man) - 1	
	
		if does_blip_exist(police_man[i].blip)
			remove_blip(police_man[i].blip)
		endif 
		
	endfor 
	
	
	for i = 0 to count_of(absail_police_man) - 1	
	
		if does_blip_exist(absail_police_man[i].blip)
			remove_blip(absail_police_man[i].blip)
		endif 
		
	endfor 

endproc 
	

proc setup_mission_fail()

	//script_assert("test")
	
	if apply_fail_time
		fail_time = get_game_timer()
	endif 

	while not lk_timer(fail_time, 4000)
		wait(0)
	endwhile
	
	remove_all_blips()

	stop_mission_fail_checks = true 
	
	original_time = get_game_timer()
	
	TRIGGER_MUSIC_EVENT("FBI4_MISSION_FAIL")
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(mission_failed_text)
	
	mission_flow = mission_fail_stage

endproc 

proc fbi4_cover_blown()
				
	//if lk_timer(original_time, 7500)
		
		mission_failed()
	
	//endif 

endproc 

proc uber_speed_system()

	if is_vehicle_driveable(trevors_car.veh)
		if is_playback_going_on_for_vehicle(trevors_car.veh)
	
			switch uber_speed_system_status 
	
				case 0
				
					if get_time_position_in_recording(trevors_car.veh) > 18000//15000
						uber_speed_system_status++ 
					endif 
					
					update_uber_playback(trevors_car.veh, 1.0)
					
				break 
				
				case 1
			
					if get_time_position_in_recording(trevors_car.veh) < 44300 
								
						calculate_new_playback_speed_from_char(trevors_car.veh, PLAYER_ped_ID(), trevors_car.speed, 1.0, 260.00, 280.00, 320.00, 1.2, 1.0, 1.0)//added 60
		
						update_uber_playback(trevors_car.veh, trevors_car.speed)
									
	//					printfloat(trevors_car.speed)
	//					printnl()
								
					else 
						
						cleanup_uber_playback(false)
						
						set_roads_in_area(<<885.8, -2086.9, 100.00>>, <<941.9, -2463.0, -100.00>>, false) //switches off road the garabage truck is parked on. Clear area was removing the ambient cars for the binocular cut. This redues the chances of this happening.
						
						uber_speed_system_status++ 
						
					endif 
					
				break 
				
				case 2
				
				break 
				
			endswitch 
			
		endif 
		
	endif 

endproc

proc setup_switch_to_trevor()

	//clear_prints()
	clear_help()
	//kill_any_conversation()

	cleanup_uber_playback(false)
	
	clear_mission_locate_stuff(locates_data)
	
	if IS_AUDIO_SCENE_ACTIVE("FBI_4_DRIVE_TO_CYPRUS_FLATS")
		STOP_AUDIO_SCENE("FBI_4_DRIVE_TO_CYPRUS_FLATS")
	endif 
	
	request_model(army_truck_driver.model)
	set_ped_model_is_suppressed(army_truck_driver.model, true)
	
	request_model(army_truck.model)
	set_vehicle_model_is_suppressed(army_truck.model, true)
	
	request_model(traffic_vehicle[0].model)
	set_vehicle_model_is_suppressed(traffic_vehicle[0].model, true)
	
	make_selector_ped_selection(selector_ped, SELECTOR_PED_trevor)
	
	SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor, false)
	SET_SELECTOR_PED_BLOCKED(selector_ped, selector_ped_franklin, false)

	clear_ped_tasks_immediately(selector_ped.pedID[selector_ped_trevor])
	set_entity_coords(selector_ped.pedID[selector_ped_trevor], <<790.8106, -2330.1296, 61.2638>>)
	set_entity_heading(selector_ped.pedID[selector_ped_trevor], 169.4868)
	//setup_trevors_weapons(selector_ped.pedID[selector_ped_trevor])
	SET_ENTITY_LOAD_COLLISION_FLAG(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(selector_ped.pedID[SELECTOR_PED_TREVOR], false) 
	FREEZE_ENTITY_POSITION(selector_ped.pedID[SELECTOR_PED_TREVOR], true)
	
//	IF NOT sCamDetails.bSplineCreated
//
//		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
//			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE)  //DEFAULT_SPLINE_CAMERA SMOOTHED_SPLINE_CAMERA
//		ENDIF 
//
//		camera_c = create_cam_with_params("default_scripted_camera", <<916.583679,-2087.274658,45.429474>>,<<-3.778669,0.00,152.599899>>,49.922977, true)
//		camera_d = create_cam_with_params("default_scripted_camera", <<793.948181,-2312.297852,62.743717>>, <<-5.976134,0.0,175.580765>>, 45.00, true)
//		camera_e = create_cam_with_params("default_scripted_camera", <<790.551636,-2329.125488,62.957919>>,<<-9.119337,2.105519,176.636658>>,31.790150, true)
//		camera_f = create_cam_with_params("default_scripted_camera", <<790.624084,-2329.897461,62.829895>>,<<-12.230901,2.136429,170.074951>>,31.738914, true)
//		 
//		//ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, camera_a, 0)
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, camera_b, 0)
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, camera_c, 1250)
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, camera_d, 5000) 
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, camera_e, 500)//350
//		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, camera_f, 1350)//1000
//	
//		sCamDetails.bSplineCreated = TRUE
//		sCamDetails.camType = selector_cam_long_spline
		
		//set_cam_active(camera_e, true)
		
		sCamDetails.pedTo = selector_ped.pedID[SELECTOR_PED_TREVOR] //ped that the spline camera is going to
		
		camera_e = create_cam_with_params("default_scripted_camera", <<790.551636,-2329.125488,62.957919>>,<<-9.119337,2.105519,176.636658>>,31.790150, false)
		camera_f = create_cam_with_params("default_scripted_camera", <<790.624084,-2329.897461,62.829895>>,<<-12.230901,2.136429,170.074951>>,31.738914, false)

		
//		set_cam_active(sCamDetails.camID, true)
		
		trigger_music_event("FBI4_SWITCH_1")

//	ENDIF 
	
	mission_flow = vehicle_spotted
	
endproc  

func bool is_player_infront_of_franklin()

	vector vec_BA
	vector franklin_forward_vec
	
	vec_BA = (get_entity_coords(selector_ped.pedID[selector_ped_franklin]) - GET_ENTITY_COORDS(player_ped_id()))
	franklin_forward_vec = get_offset_from_entity_in_world_coords(selector_ped.pedID[selector_ped_franklin], <<0.0, 3.0, 0.0>>) - GET_ENTITY_COORDS(selector_ped.pedID[selector_ped_franklin])
	
	if dot_product(vec_BA, franklin_forward_vec) < 0.0
		#IF IS_DEBUG_BUILD
		printstring("infront")
		printnl()
		#endif
		return true
	endif 

	return false 

endfunc


proc get_ambulance_into_position_dialogue_system()
			
	switch get_ambulance_into_position_dialogue_status
	
		case 0
			
			//**POSSIBLE IN CAR CONVERSATION
//			if not is_any_text_being_displayed(locates_data)
//	
//				if does_blip_exist(locates_data.LocationBlip)
//					if create_conversation(scripted_speech[0], "heataud", "in car conversation", CONV_PRIORITY_medium)
//						get_ambulance_into_position_dialogue_status++
//					endif 
//				endif
//				
//			endif 

			get_ambulance_into_position_dialogue_status++
		
		break 
		
		case 1
		
			//PAUSE IN CAR CONVERSATION
		
//			if does_blip_exist(locates_data.LocationBlip)
//		
//				if audio_paused
//					PAUSE_FACE_TO_FACE_CONVERSATION(false)
//					audio_paused = FALSE
//				endif 
//				
//				if NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					get_to_meth_lab_dialogue_system_status++
//				endif
//				
//			else 
//			
//				if NOT audio_paused
//					PAUSE_FACE_TO_FACE_CONVERSATION(true)
//					audio_paused = TRUE
//				endif 
//			endif 

			get_ambulance_into_position_dialogue_status++
		
		break 
		
		case 2
		
//			if does_blip_exist(locates_data.LocationBlip)
//				if is_entity_at_coord(player_ped_id(), <<959.2477, -2071.3098, 29.6145>>, <<8.5, 8.5, 3.0>>, true)
//					
					switch fbi4_mission_attempts
//					
						case 0
//					
//							if create_conversation(scripted_speech[0], "heataud", "in_pos_1", CONV_PRIORITY_medium)
//								get_ambulance_into_position_dialogue_status++
//							endif 
//							
						break 
//						
//						case 1
//						
//							if create_conversation(scripted_speech[0], "heataud", "in_pos_1b", CONV_PRIORITY_medium)
//								get_ambulance_into_position_dialogue_status++
//							endif 
//						
//						break 
//						
					endswitch 
//					
//				endif 
//			endif 

		break 
		
	endswitch  
	
	
	if not has_label_been_triggered("fbi4_ram0")
		if not is_any_text_being_displayed(locates_data)
			if does_blip_exist(locates_data.LocationBlip)
				if has_entity_been_damaged_by_entity(tow_truck.veh, player_ped_id()) 
					if create_conversation(scripted_speech[0], "heataud", "fbi4_ram0", CONV_PRIORITY_LOW)
						set_label_as_triggered("fbi4_ram0", true)
					endif 
				endif 
			endif
		endif 
	endif 
	
	if not has_label_been_triggered("heat_tofar0")
		if not is_any_text_being_displayed(locates_data)
			//ally coords
			if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
				if get_distance_between_coords(get_entity_coords(selector_ped.pedID[selector_ped_franklin]), <<1016.7465, -2375.7974, 29.5309>>) < 55.00 
				and get_distance_between_coords(get_entity_coords(player_ped_id()), get_entity_coords(selector_ped.pedID[selector_ped_franklin])) < 15.00
					if create_conversation(scripted_speech[0], "heataud", "heat_tofar0", CONV_PRIORITY_LOW)
						set_label_as_triggered("heat_tofar0", true)
					endif 
				endif 
			endif 
		endif 
	endif 

endproc 

proc request_get_ambulance_into_pos_assets()
	
	request_model(binoculars.model)
	
	request_anim_dict("missfbi4")
	request_anim_dict("missheat")
	request_anim_dict(strAnimDictCamShake)
	
	sf_binoculars = REQUEST_SCALEFORM_MOVIE("binoculars")
	
	prepare_music_event("FBI4_SWITCH_1")
	
	REQUEST_PTFX_ASSET()
	
endproc 
		
func bool has_get_ambulance_into_pos_assets_loaded()

	if not has_model_loaded(binoculars.model)
		printstring("not loaded 0")
		printnl()
	endif 
	
	if not has_anim_dict_loaded("missfbi4")
		printstring("not loaded 1")
		printnl()
	endif 
	
	if not has_anim_dict_loaded("missheat")
		printstring("not loaded 2")
		printnl()
	endif 
	
	if not has_scaleform_movie_loaded(sf_binoculars)
		printstring("not loaded 3")
		printnl()
	endif 
	
	if not prepare_music_event("FBI4_SWITCH_1")
		printstring("not loaded 4")
		printnl()
	endif 
	
	if not HAS_PTFX_ASSET_LOADED()	
		printstring("not loaded 5")
		printnl()
	endif 
	
	if not IS_SRL_LOADED()	
		printstring("not loaded 6")
		printnl()
	endif
		
	if has_model_loaded(binoculars.model)
	and has_anim_dict_loaded("missfbi4")
	and has_anim_dict_loaded("missheat")
	and has_anim_dict_loaded(strAnimDictCamShake)
	and has_scaleform_movie_loaded(sf_binoculars)
	and prepare_music_event("FBI4_SWITCH_1")
	and HAS_PTFX_ASSET_LOADED()	
	and IS_SRL_LOADED()
		return true 
		
	endif 
	
	return false 
	
endfunc 

func bool layby_cutscene()

	switch layby_cutscene_status
	
		case 0
		
			//delete all mission assets
		
			cleanup_uber_playback(true)
			
			if does_entity_exist(selector_ped.pedID[selector_ped_franklin])
				delete_ped(selector_ped.pedID[selector_ped_franklin])
			endif
			set_model_as_no_longer_needed(get_player_ped_model(char_franklin))
										
			if does_entity_exist(tow_truck.veh)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(tow_truck.veh)
			endif 
			set_model_as_no_longer_needed(tow_truck.model)
			
		
			clear_area(<<959.5624, -2071.3081, 29.6226>>, 10.00, true)
		
			set_entity_coords(rubbish_truck.veh, <<959.5624, -2071.3081, 29.6226>>)
			set_entity_heading(rubbish_truck.veh, 87.4847)
			set_vehicle_on_ground_properly(rubbish_truck.veh)
			
			task_look_at_coord(player_ped_id(), <<923.7, -2114.2, 44.3>>, 8000)
		
			camera_a = create_cam_with_params("default_scripted_camera", <<956.398804,-2067.969482,31.289093>>,<<2.297298,-0.000000,153.923889>>,38.633377)
			camera_b = create_cam_with_params("default_scripted_camera", <<956.398804,-2067.969482,31.289093>>,<<5.316877,-0.000000,153.923889>>,37.688091)
		
			set_cam_active(camera_a, true)
			set_cam_active_with_interp(camera_b, camera_a, 8000, graph_type_linear) 
			
			render_script_cams(true, false)
			
			if not has_label_been_triggered("in_pos_1_2")
				//if create_conversation(scripted_speech[0], "heataud", "in_pos_1_2", CONV_PRIORITY_MEDIUM)//over_take0  in_pos_1
				if PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech[0], "heataud", "in_pos_1", "in_pos_1_2", CONV_PRIORITY_medium) 
					set_label_as_triggered("in_pos_1_2", true)
				endif
			endif 
			
			original_time = get_game_timer()
			
			layby_cutscene_status++

		break 
		
		case 1
		
//			if not has_label_been_triggered("in_pos_1")
//				if create_conversation(scripted_speech[0], "heataud", "in_pos_1", CONV_PRIORITY_MEDIUM)//over_take0  in_pos_1
//					set_label_as_triggered("in_pos_1", true)
//				endif

			if not has_label_been_triggered("in_pos_1_2")
				//if create_conversation(scripted_speech[0], "heataud", "in_pos_1_2", CONV_PRIORITY_MEDIUM)//over_take0  in_pos_1
				if PLAY_SINGLE_LINE_FROM_CONVERSATION(scripted_speech[0], "heataud", "in_pos_1", "in_pos_1_2", CONV_PRIORITY_medium) 
					set_label_as_triggered("in_pos_1_2", true)
				endif

			else 
			
//				if not is_any_conversation_ongoing_or_queued() //saftey condiiton
//				or GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 0
				if not is_any_text_being_displayed(locates_data)
				
					layby_cutscene_status++
					
					return true
					
				endif 
				
			endif 

		break 
		
		case 2
		
			return true
		
		break 
		
		case 22
		
		break 
		
	endswitch 
	
	return false

endfunc

//fire_index rubbish_truck_fire = null

proc fbi4_get_ambulance_into_pos()

	VECTOR vLocatePos = <<913.6, -2229.3, 29.4>>
	
	if fbi4_get_ambulance_into_pos_status < 2
		request_get_ambulance_into_pos_assets()
	endif 
	
	//Requesting the SRL over and over makes it impossible to record... 
	//changing to be requested once the player is in the truck and closer to the switch. 
	//This should also help with streaming.
	IF NOT fbi_4_binoculars_srl_requested
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(rubbish_truck.veh)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(rubbish_truck.veh)
				VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), rubbish_truck.veh)
					IF VDIST2(vPlayerPos, vLocatePos) <= 32400.0 //180.0 * 180.0
						CDEBUG3LN(DEBUG_MISSION, "Requesting SRL...")
						PREFETCH_SRL(strSRLName)
						SET_SRL_READAHEAD_TIMES(3, 3, 3, 3)
						//SET_SRL_LONG_JUMP_MODE(TRUE) //for some reason this causes the bridge not to draw in.
						SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF)
						fbi_4_binoculars_srl_requested = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF

	switch fbi4_get_ambulance_into_pos_status
	
		case 0
		
//			if rubbish_truck_fire = null
//				rubbish_truck_fire = START_SCRIPT_FIRE(get_offset_from_entity_in_world_coords(rubbish_truck.veh, <<-1.5, 1.0, -0.5>>), 0, true)
//			endif 
			
			//get_offset_from_entity_in_world_coords(rubbish_truck.veh, <<-2.4, -0.4, -1.5>>), get_offset_from_entity_in_world_coords(rubbish_truck.veh, <<2.4, -0.4, 3.0>>)
	
			//rubbish_truck_fire = rubbish_truck_fire
		
			if not has_label_been_triggered("over_take0")
				if does_blip_exist(locates_data.LocationBlip)
					if is_entity_in_angled_area(player_ped_id(), <<1129.308, -2095.91, 32.359>>, <<1129.251, -2062.817, 48.059>>, 115.00)  //93.2  
						if is_player_infront_of_franklin()
							if create_conversation(scripted_speech[0], "HeatAud", "over_take0", conv_priority_low) 
								set_label_as_triggered("over_take0", true)
							endif 
						endif 
					endif 
				endif 
			endif 
			
			if not has_label_been_triggered("FBI_4_DRIVE_TO_CYPRUS_FLATS")
				if is_ped_sitting_in_vehicle(player_ped_id(), rubbish_truck.veh)
					start_audio_scene("FBI_4_DRIVE_TO_CYPRUS_FLATS")
					set_label_as_triggered("FBI_4_DRIVE_TO_CYPRUS_FLATS", true)
				endif 
			endif 
					
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(locates_data, vLocatePos, <<2.0, 2.0, LOCATE_SIZE_HEIGHT>>,
			TRUE, rubbish_truck.veh, "cntry_god18", "cntry_god23", "cntry_god13", TRUE, 0, TRUE)

				clear_mission_locate_stuff(locates_data)
				
				disable_control_action(PLAYER_CONTROL, INPUT_VEH_EXIT)

				//SET_SELECTOR_PED_HINT(selector_ped, selector_ped_trevor)
				//SET_SELECTOR_PED_BLOCKED(selector_ped, selector_ped_franklin)
				
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(rubbish_truck.veh, 7)

				original_time = get_game_timer()
				
				eSwitchCamState = SWITCH_CAM_START_SPLINE1
					
				fbi4_get_ambulance_into_pos_status++
					
			endif 
		
		break 
		
		case 1
		
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(rubbish_truck.veh, 7)

				if has_get_ambulance_into_pos_assets_loaded()
				
					if not is_any_text_being_displayed(locates_data)
						
						if start_new_cutscene_no_fade()

							HANDLE_TREVOR_BINOCULARS_CINEMATIC(scsTruckToTrevor, scsBinocularsScene, scsTrevorToTruck)
				
							// [SP] Temporarily stubbing out
							//layby_cutscene()

							// [SP] Temporarily stubbing out
							//trevor on railing
							//streaming_volume = STREAMVOL_CREATE_SPHERE(<<793.9, -2329.9, 61.8>>, 5.00, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
							
							REPLAY_RECORD_BACK_FOR_TIME(3)

							fbi4_get_ambulance_into_pos_status++
						endif 
					endif 
				endif 
			endif

		break 
		
		case 2
		
			printstring("fbi4_get_ambulance_into_pos_status test 2")
			printnl()
		
			IF HANDLE_TREVOR_BINOCULARS_CINEMATIC(scsTruckToTrevor, scsBinocularsScene, scsTrevorToTruck)
			
				fbi4_get_ambulance_into_pos_status++
				
			endif 
			
		break 
		
		case 3
			
			request_rubbish_truck_assets()
			
			if has_model_loaded(rubbish_truck.model)
			and has_model_loaded(tow_truck.model)
			and has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
			and has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
			and has_model_loaded(c4.model)
			and has_vehicle_recording_been_loaded(002, "lkcountry")
			and has_vehicle_recording_been_loaded(003, "lkcountry")
			and has_vehicle_recording_been_loaded(004, "lkcountry")
			and has_vehicle_recording_been_loaded(005, "lkcountry")
			and has_vehicle_recording_been_loaded(010, "lkcountry")
			and has_vehicle_recording_been_loaded(011, "lkcountry")
			and has_vehicle_recording_been_loaded(012, "lkcountry")
			and has_vehicle_recording_been_loaded(014, "lkcountry")
			and has_vehicle_recording_been_loaded(015, "lkcountry")
			and has_vehicle_recording_been_loaded(016, "lkcountry")
			and has_anim_dict_loaded("missfbi4")
			and has_anim_dict_loaded("misssagrab")
			and get_is_waypoint_recording_loaded("heat1")
			and get_is_waypoint_recording_loaded("heat2")
			and get_is_waypoint_recording_loaded("heat3")
			and request_ambient_audio_bank("SCRIPT\\FBI_04_HEAT_02")
			and REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SIREN_DISTANT")

				if does_entity_exist(binoculars.obj)
					detach_entity(binoculars.obj)
					DELETE_OBJECT(binoculars.obj)
				endif 
				set_model_as_no_longer_needed(binoculars.model)
			
				if not is_ped_injured(selector_ped.pedID[selector_ped_franklin])
					FREEZE_ENTITY_POSITION(selector_ped.pedID[selector_ped_franklin], true)
				endif 
				
				if not is_ped_injured(army_truck_driver.ped)
					DELETE_PED(army_truck_driver.ped)
				endif 
				
				if not is_ped_injured(army_truck_passenger.ped)
					DELETE_PED(army_truck_passenger.ped)
				endif
				
				if not is_ped_injured(factory_worker)
					remove_ped_for_dialogue(scripted_speech[0], 1)
					DELETE_PED(factory_worker)
				endif
				
				if is_vehicle_driveable(army_truck.veh)
					
					if is_playback_going_on_for_vehicle(army_truck.veh)
						stop_playback_recorded_vehicle(army_truck.veh)
					endif 
					
					DELETE_VEHICLE(army_truck.veh)
				endif 
				
				REMOVE_ANIM_DICT(strAnimDictCamShake)
				
//				if is_playback_going_on_for_vehicle(rubbish_truck.veh)
//					vector velocity
//					velocity = get_entity_velocity(rubbish_truck.veh)
//					velocity *= 1.2
//					stop_playback_recorded_vehicle(rubbish_truck.veh)
//					set_entity_velocity(rubbish_truck.veh, velocity) 
//					//set_vehicle_forward_speed(rubbish_truck.veh, 12.0)
//				endif 

				tow_truck.veh = create_vehicle(tow_truck.model, <<1024.1974, -2376.5244, 29.5306>>, 85.7590)
				set_vehicle_colours(tow_truck.veh, 0, 0)
				set_vehicle_has_strong_axles(tow_truck.veh, true)
				set_vehicle_engine_on(tow_truck.veh, true, true)
				set_vehicle_doors_locked(tow_truck.veh, vehiclelock_lockout_player_only)
				SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
				set_vehicle_as_restricted(tow_truck.veh, 1)
				
				create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_FRANKLIN], char_franklin, tow_truck.veh, vs_driver, false)
				SET_STORED_FBI4_OUTFIT(selector_ped.pedID[SELECTOR_PED_FRANKLIN])
				SET_PED_COMP_ITEM_CURRENT_SP(selector_ped.pedID[SELECTOR_PED_franklin], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
				add_ped_for_dialogue(scripted_speech[0], 1, selector_ped.pedID[SELECTOR_PED_FRANKLIN], "franklin")
				set_blocking_of_non_temporary_events(selector_ped.pedID[SELECTOR_PED_FRANKLIN], true)
				
				if IS_AUDIO_SCENE_ACTIVE("FBI_4_CUTSCENE_BINOCULARS")
					STOP_AUDIO_SCENE("FBI_4_CUTSCENE_BINOCULARS")
				endif 

				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
				
				remove_ped_for_dialogue(scripted_speech[0], 0)
				add_ped_for_dialogue(scripted_speech[0], 0, player_ped_id(), "michael")
				create_conversation(scripted_speech[0], "heataud", "heat_move", CONV_PRIORITY_VERY_HIGH)
			
				start_audio_scene("FBI_4_BLOCK_THE_STREET")
				
				mission_flow = park_ambulance
			
			ENDIF
			
			/*if layby_cutscene()
				setup_switch_to_trevor()
			endif */

		break 
		
		case 4
		
		break 
		
	endswitch 
	
	get_ambulance_into_position_dialogue_system()
	
	if not tow_truck_switched_to_ai
		if is_playback_going_on_for_vehicle(tow_truck.veh)
		
			printstring("tow_truck.veh = ")
			printfloat(get_time_position_in_recording(tow_truck.veh))
			printnl()
			
			if get_time_position_in_recording(tow_truck.veh) > 18000
				SET_PLAYBACK_TO_USE_AI(tow_truck.veh)
				tow_truck_switched_to_ai = true
			endif 
		endif 
	endif 

	uber_speed_system()

	#IF IS_DEBUG_BUILD
//		 switch record_main_uber_recording_status
	//		 
//			CASE 0
	//		
	//			#IF IS_DEBUG_BUILD
	//				set_uber_parent_widget_group(fbi_4_widget_group)
	//				INIT_UBER_RECORDING("lkheat")
	//			#endif 
	//			
	//			debug_blip = add_blip_for_coord(<<777.7610, -2277.9160, 28.0688>>)
	//			set_blip_route(debug_blip, true)
	//	       
	//			record_main_uber_recording_status++
	//		   
//		  	BREAK
	//	  
	//	  	CASE 1
	//		
	//	        UPDATE_UBER_RECORDING()
	//			
	//	  	BREAK
	//		
//		endswitch 
	#endif 

endproc 

proc skip_system()

	#IF IS_DEBUG_BUILD
	
		int i = 0
	
		dont_do_j_skip(locates_data)
		
		if is_keyboard_key_just_pressed(key_j)
		//or mission_warp_count > 0

			switch mission_flow
			
				case intro_mocap
				
					if not IS_CUTSCENE_PLAYING()
				
						clear_ped_tasks_immediately(player_ped_id())
						set_entity_coords(player_ped_id(), <<1388.9193, -2066.3245, 50.9982>>)
						set_entity_heading(player_ped_id(), 220.7670)
						
					else
					
						stop_cutscene()
					
					endif 
				
				break 
				
				case get_ambulance_into_pos
				
					switch get_ambulance_into_pos_skip_status
					
						case 0
				
							clear_ped_tasks_immediately(player_ped_id())
							set_ped_into_vehicle(player_ped_id(), rubbish_truck.veh)
							set_entity_coords(rubbish_truck.veh, <<971.1750, -2071.6299, 29.7511>>)
							set_entity_heading(rubbish_truck.veh, 93.0558)

							get_ambulance_into_pos_skip_status++
							
						break 
						
						case 1
						
							make_selector_ped_selection(selector_ped, SELECTOR_PED_TREVOR)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
							setup_switch_to_trevor()
						
						break 
					
					endswitch 
				
				break 
			
				case vehicle_spotted 

					if fbi4_vehicle_spotted_status = 4
						make_selector_ped_selection(selector_ped, SELECTOR_PED_MICHAEL)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
						setup_hotswap_michael()
					endif 

				break 
			
				case park_ambulance 

					if park_ambulance_status < 2

						clear_ped_tasks_immediately(player_ped_id())
						set_ped_into_vehicle(player_ped_id(), rubbish_truck.veh)	

						SET_ENTITY_COORDS(rubbish_truck.veh, ambulance_target_pos)
						set_entity_heading(rubbish_truck.veh, 266.3282)
						
					endif 
				
				break 
				
				case ram_army_truck 

					if ram_army_truck_status = 0
						if is_playback_going_on_for_vehicle(tow_truck.veh)
							stop_playback_recorded_vehicle(tow_truck.veh)
						endif 
						clear_ped_tasks_immediately(player_ped_id())
						set_ped_into_vehicle(player_ped_id(), tow_truck.veh)	
						set_entity_coords(tow_truck.veh, <<913.0657, -2366.6985, 29.3809>>)
						set_vehicle_forward_speed(tow_truck.veh, 22.00)
					endif 
				
				break 
				
				case blow_open_truck_doors 
				
					if blow_open_truck_doors_status = 0
			
						clear_ped_tasks_immediately(player_ped_id())
						set_player_control(player_id(), false)
						set_current_ped_weapon(player_ped_id(), WEAPONTYPE_STICKYBOMB, true)
						set_entity_coords(player_ped_id(), <<889.653, -2361.100, 30.271>>)
						set_entity_heading(player_ped_id(), 171.000)

					else 
					
						if does_blip_exist(bomb_blip)
							remove_blip(bomb_blip)
						endif 
					
						blow_open_truck_doors_status = 3
						SET_ENTITY_PROOFS(army_truck.veh, true, true, true, true, true)
						clear_ped_tasks_immediately(player_ped_id())
						set_entity_coords(player_ped_id(), <<889.9106, -2349.3643, 29.3342>>)
					
					endif 
				
				break 
				
				case shootout 
				
					//switch shootout_skip_status
					switch shootout_master_controler_status
					
						case 2
						case 3
						case 4
						
							for i = 0 to 3
								if is_vehicle_driveable(police_car[i].veh)
									if is_playback_going_on_for_vehicle(police_car[i].veh)
										skip_to_end_and_stop_playback_recorded_vehicle(police_car[i].veh)
									endif 
								endif 
							endfor 
										
						
							for i = 0 to 7
								if not is_ped_injured(police_man[i].ped)
									set_entity_health(police_man[i].ped, 2)
								endif 
							endfor 
							
							if does_entity_exist(army_truck_guy.ped)
								delete_ped(army_truck_guy.ped)
							endif 
							
							if does_entity_exist(army_truck_guy_2.ped)
								delete_ped(army_truck_guy_2.ped)
							endif 
							
							set_model_as_no_longer_needed(army_truck_guy.model)
							
							if does_blip_exist(player_cover_blip)
								remove_blip(player_cover_blip)
							endif 
			
							if get_current_player_ped_enum() = char_franklin
								set_entity_coords(player_ped_id(), <<873.3599, -2352.4114, 29.3306>>)
							else 
								set_entity_coords(selector_ped.pedID[selector_ped_franklin], <<873.3599, -2352.4114, 29.3306>>)
							endif 
							
							if get_current_player_ped_enum() = char_michael
								set_entity_coords(player_ped_id(), <<873.3599, -2352.4114, 29.3306>>)
								set_entity_heading(player_ped_id(), 24.1016)
							else 
								set_entity_coords(selector_ped.pedID[selector_ped_michael], <<873.3599, -2352.4114, 29.3306>>)
								set_entity_heading(selector_ped.pedID[selector_ped_michael], 24.1016)
							endif 

							if get_current_player_ped_enum() != char_michael
								make_selector_ped_selection(selector_ped, selector_ped_michael)
								take_control_of_selector_ped(selector_ped, true)
							endif 
							
							michael_ai_system_status = 6
							
							clear_ped_tasks_immediately(player_ped_id())
							set_entity_coords(player_ped_id(), <<845.3195, -2332.2019, 29.3346>>)

							if not has_sound_finished(police_siren_sound)
								stop_sound(police_siren_sound)
							endif 
							
							DISTANT_COP_CAR_SIRENS(false) 
							
							SET_SELECTOR_PED_PRIORITY(selector_ped, SELECTOR_PED_MICHAEL, SELECTOR_PED_trevor, selector_ped_franklin)

							allow_hotswap = true
							hot_swap_time = get_game_timer()
							
							trevor_time = get_game_timer()
							
							shootout_master_controler_status = 5

						break 

						
//							if not has_sound_finished(police_siren_sound)
//								stop_sound(police_siren_sound)
//							endif 
//							
//							DISTANT_COP_CAR_SIRENS(false) 
//						
//							clear_player_wanted_level(player_id())
//							set_create_random_cops(false)
//							set_max_wanted_level(0)
//							set_player_wanted_level_now(player_id())
//
//							shootout_master_controler_status = 5
//							shootout_wanted_level_system_status = 5
						
					endswitch 

				break
				
				case switch_to_michael_or_franklin

					make_selector_ped_selection(selector_ped, SELECTOR_PED_MICHAEL)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
					setup_hotswap_cam_to_michael()
					switch_to_michael_or_franklin_status = 1

					clear_prints()
						
					if does_blip_exist(trevor_blip)
						remove_blip(trevor_blip)
					endif 
					
					if does_blip_exist(michael_blip)
						remove_blip(michael_blip)
					endif 
					
					if does_blip_exist(franklin_blip)
						remove_blip(franklin_blip)
					endif
					
				break 
				
				case burn_rubbish_truck
				
					switch burn_rubbish_truck_master_flow_system_status 
					
						case 0
						
							clear_ped_tasks_immediately(player_ped_id())
							set_ped_into_vehicle(player_ped_id(), rubbish_truck.veh)

							set_entity_coords(rubbish_truck.veh, <<1121.0305, -1249.9076, 20.7>>)
							set_entity_heading(rubbish_truck.veh, 54.8192)
							set_vehicle_on_ground_properly(rubbish_truck.veh)
							
						break 
						
						case 1
						case 2
						
							clear_ped_tasks_immediately(player_ped_id())
							set_entity_coords(player_ped_id(), <<1131.0305, -1259.9076, 20.7>>)
							set_entity_heading(player_ped_id(), 54.8192)
							
							explode_vehicle(rubbish_truck.veh)
						
						break 
						
					endswitch 
				
				break 
				
				case meet_devin
				
					clear_ped_tasks_immediately(player_ped_id())
					set_entity_coords(player_ped_id(), <<1379.9153, -2059.4282, 50.9988>>) 

				break 
				
			endswitch 
			
		elif is_keyboard_key_just_pressed(key_p)
			
			switch mission_flow

				case get_ambulance_into_pos
				case vehicle_spotted
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
	
					launch_mission_stage_menu_status = 0

				break 
				
				
				case park_ambulance
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
					
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 1	
					else
						launch_mission_stage_menu_status = 0
					endif 
				
				break 
				
				
				case ram_army_truck
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 2	
					else
						launch_mission_stage_menu_status = 1
					endif 
				
				break 
				
				
				case blow_open_truck_doors
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 3
					else
						launch_mission_stage_menu_status = 2
					endif 
						
					p_skip_time = get_game_timer()
				
				break 
			
				case shootout
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 4	
					else
						launch_mission_stage_menu_status = 3
					endif 
						
					p_skip_time = get_game_timer()
					
				break 
				
				case burn_rubbish_truck
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 5	
					else
						launch_mission_stage_menu_status = 4
					endif 
						
					p_skip_time = get_game_timer()
					
				break 
				
				case phone_call_to_michael
				case switch_to_michael
				case meet_devin
				
					remove_all_mission_assets()
					mission_flow = load_stage_selector_assets
				
					if (get_game_timer() - p_skip_time) > 3000	
						launch_mission_stage_menu_status = 5	
					else
						launch_mission_stage_menu_status = 4
					endif 
						
					p_skip_time = get_game_timer()
				
				break 
				
			endswitch 
			
		endif 
		
		if LAUNCH_MISSION_STAGE_MENU(menu_stage_selector, menu_return_stage) 
			
			remove_all_mission_assets()

			launch_mission_stage_menu_status = menu_return_stage
			
			mission_flow = load_stage_selector_assets
			
			do_screen_fade_out(1000)
			
		endif
	#endif 
	
endproc 

proc load_repeat_play_assets()
			
	request_model(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	request_model(GET_PLAYER_PED_MODEL(CHAR_franklin))
	request_model(GET_PLAYER_PED_MODEL(CHAR_trevor))

	request_model(rubbish_truck.model)
	set_vehicle_model_is_suppressed(rubbish_truck.model, true)
	REQUEST_VEHICLE_ASSET(rubbish_truck.model, enum_to_int(VRF_REQUEST_ALL_ANIMS)) //not checking for loading

	request_model(tow_truck.model)
	set_vehicle_model_is_suppressed(tow_truck.model, true)
	
	request_model(trevors_car.model)
	set_vehicle_model_is_suppressed(trevors_car.model, true) 

	while not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
	or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
	or not has_model_loaded(rubbish_truck.model)
	or not has_vehicle_asset_loaded(rubbish_truck.model)
	or not has_model_loaded(tow_truck.model)
	or not has_model_loaded(trevors_car.model)
	
		wait(0)
	
	endwhile 
	
	SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_NORMAL)
	
	clear_area(<<1377.9087, -2071.4409, 50.9983>>, 1000, true)
	
	clear_ped_tasks_immediately(player_ped_id())
	set_current_selector_ped(selector_ped_michael, false)
	set_entity_coords(player_ped_id(), <<1368.4437, -2072.8225, 50.9983>>)
	set_entity_heading(player_ped_id(), 235.72)
	
	rubbish_truck.veh = CREATE_VEHICLE(rubbish_truck.model, <<1381.4722, -2072.2454, 50.9981>>, 38.3340)
	SET_SIREN_CAN_BE_CONTROLLED_BY_AUDIO(rubbish_truck.veh, FALSE) 
	set_vehicle_as_restricted(rubbish_truck.veh, 0)

	tow_truck.veh = CREATE_VEHICLE(tow_truck.model, <<1374.8580, -2077.3743, 50.9981>>, 37.5148)
	set_vehicle_colours(tow_truck.veh, 0, 0)
	SET_VEHICLE_DOORS_LOCKED(tow_truck.veh, VEHICLELOCK_LOCKOUT_PLAYER_ONLY) 
	SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
	set_vehicle_as_restricted(tow_truck.veh, 1)
			
	request_cutscene("fbi_4_mcs_3_concat")
	
	while not has_cutscene_loaded()
	
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("franklin", PLAYER_PED_ID())
		SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(char_michael, "michael")
		SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(char_trevor, "trevor")
		
		wait(0)
	
	endwhile
	

	NEW_LOAD_SCENE_START(<<1368.4437, -2072.8225, 50.9983>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 235.72>>), 10.0)
	
	while not IS_NEW_LOAD_SCENE_LOADED()
		wait(0)
	endwhile 
	
	NEW_LOAD_SCENE_STOP()
	
	
	REGISTER_ENTITY_FOR_CUTSCENE(NULL, "franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_franklin))
	REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))		
	
	REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevors_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, trevors_car.model)
	
	if is_vehicle_driveable(rubbish_truck.veh)
		register_entity_for_cutscene(rubbish_truck.veh, "fbi_bin_lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, trash)
	endif 
	
	if is_vehicle_driveable(tow_truck.veh)
		register_entity_for_cutscene(tow_truck.veh, "fbi_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, towtruck)
	endif 
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)

	start_cutscene()

	fbi4_intro_mocap_status = 1
	
endproc 

proc mission_setup()
	
	#IF IS_DEBUG_BUILD
		load_lk_widgets()
		load_vehicle_force_widget_data()
		load_local_widgets()
		SET_LOCATES_HEADER_WIDGET_GROUP(fbi_4_widget_group)
	#endif 
	
//	#IF IS_DEBUG_BUILD
//		SETUP_SPLINE_CAM_NODE_ARRAY_TRUCK_TO_TREVOR(scsTruckToTrevor, INT_TO_NATIVE(VEHICLE_INDEX, 0), INT_TO_NATIVE(PED_INDEX, 0))
//		SETUP_SPLINE_CAM_NODE_ARRAY_BINOCULARS_SCENE(scsBinocularsScene, INT_TO_NATIVE(PED_INDEX, 0), INT_TO_NATIVE(VEHICLE_INDEX, 0))
//		SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_TO_TRUCK(scsTrevorToTruck, INT_TO_NATIVE(PED_INDEX, 0), INT_TO_NATIVE(VEHICLE_INDEX, 0))
//		CREATE_SPLINE_CAM_WIDGETS(scsTruckToTrevor, "Truck", "Trevor", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
//		CREATE_SPLINE_CAM_WIDGETS(scsBinocularsScene, "Binoculars", "", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
//		CREATE_SPLINE_CAM_WIDGETS(scsTrevorToTruck, "Trevor", "Truck", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
//		CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
//	#ENDIF
	
	//INFORM_MISSION_STATS_OF_MISSION_START_fbi_four()
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id())
	
	load_text_and_dialogue()
	
	add_relationship_groups()
	
	if is_player_playing(player_id())
		clear_player_wanted_level(player_id())
	endif 
	
	set_wanted_level_multiplier(0.3)
	
	SUPPRESS_EMERGENCY_CALLS()

	//**********CHANGE TO 200 ONCE YOU GET BUG BACK FROM KENNETH
	//SET_PLAYER_PED_STORED_HEALTH(char_michael, 3000)
	//SET_PLAYER_PED_STORED_HEALTH(char_trevor, 200)
	//SET_PLAYER_PED_STORED_HEALTH(char_franklin, 3000)
	
	//area arround outside the fib depot
	add_scenario_blocking_area(<<1570.8, -1923.1, 100.00>>, <<1258.2, -2153.2, -100.00>>)
	
	ADD_NAVMESH_BLOCKING_OBJECT(<<891.696, -2368.981, 29.853>> , <<8.675, 0.750, 2.125>>, DEG_TO_RAD(-44.4), FALSE)
	ADD_NAVMESH_BLOCKING_OBJECT(<< 884.755, -2365.731, 29.785>>, <<7.325, 0.75, 2.125>>, DEG_TO_RAD(-4.7), FALSE)
	
	ADD_COVER_BLOCKING_AREA(<<843.70941, -2302.69775, 29.34290>>, <<846.34943, -2306.48950, 29.33395>>, true, false, true)
		
	michael_cover_point = add_cover_point(<<881.53, -2334.04, 33.91>>, 269.9536, covuse_walltoleft, COVHEIGHT_low, COVARC_180) 
	michael_cover_point_2 = add_cover_point(<<871.78998, -2344.44165, 29.33137>>, 273.9499, covuse_walltoleft, COVHEIGHT_low, COVARC_180)  
	players_cover_point = add_cover_point(<<873.85779, -2353.02979, 29.33141>>, 281.0534, covuse_walltoleft, COVHEIGHT_low, COVARC_180) 
	
	mag_demo_cover_point = add_cover_point(<<845.66034, -2332.30762, 29.33460>>, 348.7454, covuse_walltoleft, COVHEIGHT_low, COVARC_180)   
	
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 20.0)

	SET_PED_ALLOW_HURT_COMBAT_FOR_ALL_MISSION_PEDS(FALSE)

	initialise_mission_variables()
	
	//for mag demo 2
	g_bMagDemoFRA2Passed = true
	
	if not g_bMagdemoActive
		IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = DUMMY_PED_COMP
	        g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL] = OUTFIT_P0_PREP_BOILER_SUIT_2
	        g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_FRANKLIN] = OUTFIT_P1_PREP_BOILER_SUIT_1
	        g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_TREVOR] = OUTFIT_P2_PREP_BOILER_SUIT_3
	  	ENDIF
	endif 

	if is_replay_in_progress() 
	
		replay_active = true
	
		if not g_bShitskipAccepted
		
			switch Get_Replay_Mid_Mission_Stage()
			
				case 0
				
					load_trip_skip_data_0()	//mission start 
				
				break 
			
				case 1
				
					load_trip_skip_data_1() //ram the money truck

				break 
				
				case 2
				
					load_trip_skip_data_2()	//detonate the c4

				break 
				
				case 3
				
					load_trip_skip_data_3() //shootout
				
				break 
				
				case 4
				
					load_trip_skip_data_4() //burn rubbish truck 
				
				break 
			
			endswitch 

		else 
		
			int shit_skip_status 
			
			shit_skip_status = Get_Replay_Mid_Mission_Stage() + 1
			
			switch shit_skip_status 
			
				case 1
				
					load_trip_skip_data_1()
					
				break 
				
				case 2
				
					load_trip_skip_data_2()
				
				break 
				
				case 3
				
					load_trip_skip_data_3()
				
				break 
				
				case 4
				
					load_trip_skip_data_4()
				
				break 
				
				case 5
				
					load_trip_skip_data_5()
				
				break
				
			endswitch 
		
		endif 
		
		replay_active = false 
			
	else 

		fbi4_shootout_wanted_level = 4

		if not is_repeat_play_active()
	
			if not is_screen_faded_in()
				if not is_screen_fading_in()
					do_screen_fade_in(default_fade_time)
				endif 
			endif 
		
			Set_Replay_Mid_Mission_Stage_With_Name(0, "Start of mission") 
			
		else 
		
			load_repeat_play_assets()

		endif

	endif 
	
	//Set_Replay_Mid_Mission_Stage(3)
	
//	trace_native_command("SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA")
//	trace_native_command("add_scenario_blocking_area")

endproc

proc fbi_4_mission_failed_stage()

	if lk_timer(fail_time, 7000)//forces the fake cop sirens play for 3 seconds if fail time 
		if GET_MISSION_FLOW_SAFE_TO_CLEANUP() 
			mission_failed()
		endif 
	endif 

endproc 

proc function_calls_required_every_frame()

	set_vehicle_density_multiplier_this_frame(0.8)

	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
	
endproc 


proc fbi4_mag_demo_2_setup()
				
	switch fbi4_mag_demo_2_setup_status 
	
		case 0

			SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_NORMAL)

			request_model(GET_PLAYER_PED_MODEL(char_michael))
			request_model(GET_PLAYER_PED_MODEL(char_franklin))
			request_model(GET_PLAYER_PED_MODEL(char_trevor))

			request_model(rubbish_truck.model)
			set_vehicle_model_is_suppressed(rubbish_truck.model, true)

			request_model(tow_truck.model)
			set_vehicle_model_is_suppressed(tow_truck.model, true)
			
			request_model(trevors_car.model)
			set_vehicle_model_is_suppressed(trevors_car.model, true) 

			request_anim_dict("missfbi4")
			request_anim_dict("missheat")
			
			request_model(binoculars.model)
			sf_binoculars = REQUEST_SCALEFORM_MOVIE("binoculars")
			
			REQUEST_PTFX_ASSET()

			request_vehicle_recording(001, "lkcountry") 

			request_vehicle_recording(001, "lkheat") 
			request_vehicle_recording(002, "lkheat") 
			request_vehicle_recording(003, "lkheat") 
			request_vehicle_recording(004, "lkheat") 
			request_vehicle_recording(005, "lkheat") 
			request_vehicle_recording(006, "lkheat") 
			request_vehicle_recording(007, "lkheat") 
			request_vehicle_recording(008, "lkheat") 
			request_vehicle_recording(009, "lkheat") 
			request_vehicle_recording(010, "lkheat") 
			request_vehicle_recording(011, "lkheat") 
			request_vehicle_recording(012, "lkheat") 
			request_vehicle_recording(013, "lkheat") 
			request_vehicle_recording(014, "lkheat") 
			request_vehicle_recording(015, "lkheat") 
			request_vehicle_recording(016, "lkheat") 
			request_vehicle_recording(017, "lkheat") 
			request_vehicle_recording(018, "lkheat") 
			request_vehicle_recording(019, "lkheat") 
			request_vehicle_recording(020, "lkheat") 
			request_vehicle_recording(021, "lkheat") 
			request_vehicle_recording(021, "lkheat") 
			request_vehicle_recording(022, "lkheat") 
			request_vehicle_recording(023, "lkheat") 
			request_vehicle_recording(024, "lkheat") 
			request_vehicle_recording(025, "lkheat") 
			request_vehicle_recording(026, "lkheat") 
			request_vehicle_recording(027, "lkheat") 
			
			request_vehicle_recording(101, "lkheat") 
			request_vehicle_recording(102, "lkheat") 
			
			request_vehicle_recording(201, "lkheat") 
			request_vehicle_recording(202, "lkheat") 
			request_vehicle_recording(203, "lkheat") 
			request_vehicle_recording(204, "lkheat") 
			
			request_anim_dict("missfbi4leadinoutfbi_4_mcs_3")
			
			//PRELOAD_STORED_FBI4_OUTFIT(player_ped_id(), g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_MICHAEL]) 
			//PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_franklin], g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_franklin])
			//PRELOAD_STORED_FBI4_OUTFIT(selector_ped.pedID[selector_ped_trevor], g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[CHAR_trevor])				

			while not has_model_loaded(rubbish_truck.model)
			or not has_model_loaded(tow_truck.model)
			or not has_model_loaded(trevors_car.model)
			or not has_model_loaded(GET_PLAYER_PED_MODEL(char_michael))
			or not has_model_loaded(GET_PLAYER_PED_MODEL(char_franklin))
			or not has_model_loaded(GET_PLAYER_PED_MODEL(char_trevor))
			or not has_vehicle_recording_been_loaded(001, "lkcountry")
			or not has_vehicle_recording_been_loaded(001, "lkheat")
			or not has_vehicle_recording_been_loaded(002, "lkheat")
			or not has_vehicle_recording_been_loaded(003, "lkheat") 
			or not has_vehicle_recording_been_loaded(004, "lkheat")
			or not has_vehicle_recording_been_loaded(005, "lkheat") 
			or not has_vehicle_recording_been_loaded(006, "lkheat") 
			or not has_vehicle_recording_been_loaded(007, "lkheat") 
			or not has_vehicle_recording_been_loaded(008, "lkheat")
			or not has_vehicle_recording_been_loaded(009, "lkheat") 
			or not has_vehicle_recording_been_loaded(010, "lkheat") 
			or not has_vehicle_recording_been_loaded(011, "lkheat") 
			or not has_vehicle_recording_been_loaded(012, "lkheat") 
			or not has_vehicle_recording_been_loaded(013, "lkheat") 
			or not has_vehicle_recording_been_loaded(014, "lkheat")
			or not has_vehicle_recording_been_loaded(015, "lkheat")
			or not has_vehicle_recording_been_loaded(016, "lkheat") 
			or not has_vehicle_recording_been_loaded(017, "lkheat")
			or not has_vehicle_recording_been_loaded(018, "lkheat") 
			or not has_vehicle_recording_been_loaded(019, "lkheat") 
			or not has_vehicle_recording_been_loaded(020, "lkheat") 
			or not has_vehicle_recording_been_loaded(021, "lkheat") 
			or not has_vehicle_recording_been_loaded(021, "lkheat")
			or not has_vehicle_recording_been_loaded(022, "lkheat")
			or not has_vehicle_recording_been_loaded(023, "lkheat")
			or not has_vehicle_recording_been_loaded(024, "lkheat") 
			or not has_vehicle_recording_been_loaded(025, "lkheat")
			or not has_vehicle_recording_been_loaded(026, "lkheat")
			or not has_vehicle_recording_been_loaded(027, "lkheat")
			or not has_vehicle_recording_been_loaded(101, "lkheat") 
			or not has_vehicle_recording_been_loaded(102, "lkheat") 
			or not has_vehicle_recording_been_loaded(201, "lkheat")
			or not has_vehicle_recording_been_loaded(202, "lkheat")
			or not has_vehicle_recording_been_loaded(203, "lkheat")
			or not has_vehicle_recording_been_loaded(204, "lkheat")
			or not has_anim_dict_loaded("missfbi4leadinoutfbi_4_mcs_3")

				wait(0)
				
			endwhile 

			clear_player_wanted_level(player_id())
			
			clear_area(<<1376.0820, -2082.0479, 50.9983>>, 40.00, true)
	
			set_roads_in_area(<<1412.83, -1954.96, -100.00>>, <<1406.50, -1941.47, 100.00>>, false)
			
			create_player_ped_on_foot(selector_ped.pedID[SELECTOR_PED_michael], char_michael, <<1376.0820, -2082.0479, 50.9983>>, 46.5960, false)
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_NAVY_SUIT, FALSE)

			cutscene_pos = << 1377.630, -2082.943, 50.997 >>
			cutscene_rot = << 0.000, 0.000, 130.500 >>
			
			michael_cutscene_index = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
			
			TASK_SYNCHRONIZED_SCENE(selector_ped.pedID[SELECTOR_PED_michael], michael_cutscene_index, "missfbi4leadinoutfbi_4_mcs_3", "_leadin_michael", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				
			
			rubbish_truck.veh = create_vehicle(rubbish_truck.model, <<1381.4722, -2072.2454, 50.9981>>, 38.3340)
			set_vehicle_on_ground_properly(rubbish_truck.veh)
			set_vehicle_has_strong_axles(rubbish_truck.veh, true)
			set_vehicle_automatically_attaches(rubbish_truck.veh, false)
			set_vehicle_as_restricted(rubbish_truck.veh, 0)

			tow_truck.veh = create_vehicle(tow_truck.model, <<1377.14, -2076.2, 52.0>>, 40.038132)
			set_vehicle_colours(tow_truck.veh, 0, 0)
			set_vehicle_on_ground_properly(tow_truck.veh)
			set_vehicle_has_strong_axles(tow_truck.veh, true)
			set_entity_only_damaged_by_player(tow_truck.veh, true)
			set_vehicle_doors_locked(tow_truck.veh, vehiclelock_lockout_player_only) 
			SET_VEHICLE_DISABLE_TOWING(tow_truck.veh, true)
			force_entity_ai_and_animation_update(tow_truck.veh)
			set_vehicle_as_restricted(tow_truck.veh, 1)
			
			CREATE_PLAYER_VEHICLE(trevors_car.veh, CHAR_TREVOR, get_position_of_vehicle_recording_at_time(101, 1450, "lkheat"), 40.3225, false)
			set_entity_only_damaged_by_player(trevors_car.veh, true)
			set_vehicle_doors_locked(trevors_car.veh, vehiclelock_lockout_player_only)  
			
			fbi4_mag_demo_2_setup_status++
			
		break 
		
		case 1
		
			request_cutscene("fbi_4_mcs_3_concat")

			if IS_PLAYER_SWITCH_IN_PROGRESS()
			
				if not g_bMagDemoFRA2Ready                  
					SWITCH_STATE camera_switch_state
					camera_switch_state = GET_PLAYER_SWITCH_STATE()

					IF ((camera_switch_state >= SWITCH_STATE_JUMPCUT_DESCENT) and (GET_PLAYER_SWITCH_JUMP_CUT_INDEX() < 1))  //<=1
					AND HAS_CUTSCENE_LOADED()
					    ar_ALLOW_PLAYER_SWITCH_OUTRO()
					    g_bMagDemoFRA2Ready = TRUE
					endif 
				ENDIF		
			
			elif is_entity_in_angled_area(player_ped_id(), <<1386.038452,-2063.979248,50.623257>>, <<1360.417480,-2085.506348,57.873257>>, 14.625000)
			and IS_PED_ON_FOOT(player_ped_id())

				if has_cutscene_loaded()                    

					register_entity_for_cutscene(selector_ped.pedID[selector_ped_michael], "michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, get_player_ped_model(char_michael))

					register_entity_for_cutscene(rubbish_truck.veh, "fbi_bin_lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, trash)
				    register_entity_for_cutscene(tow_truck.veh, "fbi_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, towtruck)

					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, get_player_ped_model(char_trevor))

				    start_cutscene()
				    
				    fbi4_intro_mocap_status = 1
				    
				    mission_flow = intro_mocap
				                          
				endif 
				
			endif 

		break 
		
		case 2
		
		break 
		
	endswitch 

endproc 

proc fbi4_mag_demo_2_end()
				
	switch mag_demo_2_end_status 
	
		case 0
		
//			locate
//			if blip exist set timer move onto next case
		
			if is_vehicle_driveable(get_away_car.veh)
				if is_ped_sitting_in_vehicle(player_ped_id(), get_away_car.veh)
			
					original_time = get_game_timer()
				
					mag_demo_2_end_status++
				endif 
			
			else 
			
				mag_demo_2_end_status++
			
			endif 
		
		break 
		
		case 1
		
			if lk_timer(original_time, 20022)
			
				mission_passed()
				
			endif 
			
		break 
		
	endswitch 

endproc 
			

SCRIPT
	
	set_mission_flag(true)
	
	if has_force_cleanup_occurred()
		SET_BUILDING_STATE(BUILDINGNAME_RF_HEAT_WALL, BUILDINGSTATE_NORMAL, true)
		Store_Fail_Weapon(player_ped_id(), enum_to_int(get_current_player_ped_enum()))
		Mission_Flow_Mission_Force_Cleanup()
		mission_cleanup()
	endif
	
	mission_setup()

/*#IF IS_DEBUG_BUILD
	SETUP_SPLINE_CAM_NODE_ARRAY_TRUCK_TO_TREVOR(scsTruckToTrevor, INT_TO_NATIVE(VEHICLE_INDEX, 0), INT_TO_NATIVE(PED_INDEX, 0))
	SETUP_SPLINE_CAM_NODE_ARRAY_BINOCULARS_SCENE(scsBinocularsScene, INT_TO_NATIVE(PED_INDEX, 0), INT_TO_NATIVE(VEHICLE_INDEX, 0))
	SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_TO_TRUCK(scsTrevorToTruck, INT_TO_NATIVE(PED_INDEX, 0), INT_TO_NATIVE(VEHICLE_INDEX, 0))
	CREATE_SPLINE_CAM_WIDGETS(scsTruckToTrevor, "Truck", "Trevor", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
	CREATE_SPLINE_CAM_WIDGETS(scsBinocularsScene, "Binoculars", "", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
	CREATE_SPLINE_CAM_WIDGETS(scsTrevorToTruck, "Trevor", "Truck", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
	CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
#ENDIF*/

	while true

		wait(0)
		
		if not stop_mission_fail_checks
			if mission_fail_checks()
				setup_mission_fail()
			endif 
		endif
		
		
		skip_system()
		
		function_calls_required_every_frame()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BLITZ_PLAY")

		switch mission_flow 
		
			case intro_mocap 
			
				fbi4_intro_mocap()
			
			break 
			
			case mag_demo_2_setup
			
				fbi4_mag_demo_2_setup()
			
			break 
		
			case get_ambulance_into_pos
			
				fbi4_get_ambulance_into_pos()
			
			break 
			
			case vehicle_spotted 
			
				fbi4_vehicle_spotted()
			
			break 
			
			case park_ambulance 
			
				fbi4_park_ambulance()
			
			break
			
			case ram_army_truck 
		
				fbi4_ram_army_truck()
			
			break 
			
			case blow_open_truck_doors 
			
				fbi4_blow_open_truck_doors()
			
			break 
			
			case shootout
			
				fbi4_shootout()
				
			break 
			
			case switch_to_michael_or_franklin
			
				fbi4_switch_to_michael_or_franklin()
				
			break 
			
			case burn_rubbish_truck
			
				fbi4_burn_rubbish_truck()

			break 
			
			case phone_call_to_michael
			
				fbi4_phone_call_to_michael()
				
			break 
			
			case switch_to_michael
			
				fbi4_switch_to_michael()
			
			break 
			
			case meet_devin
			
				fbi4_meet_devin()
			
			break
			
			case pass_cutscene
			
				fbi4_pass_cutscene()
				
			break 
			
			case mag_demo_2_end
			
				fbi4_mag_demo_2_end()
				
			break 
			
			case cover_blown

				fbi4_cover_blown()

			break
			
			case load_stage_selector_assets

				fbi4_load_stage_selector_assets()
				
			break 
			
			case mission_fail_stage
			
				fbi_4_mission_failed_stage()

			break 
			
		endswitch 
		
		
		#IF IS_DEBUG_BUILD
			
			int i = 0
			
			if does_entity_exist(documents.obj)
				printstring("documents = ")
				printvector(get_entity_coords(documents.obj))
				printnl()
			endif 
			
			//is_entity_in_angled_area(player_ped_id(), <<1351.419, -2065.903, 46.098>>, <<1395.749, -2028.968, 58.998>>, 80.000)
			
			/*UPDATE_SPLINE_CAM_WIDGETS(scsTruckToTrevor)
			UPDATE_SPLINE_CAM_WIDGETS(scsBinocularsScene)
			UPDATE_SPLINE_CAM_WIDGETS(scsTrevorToTruck)
			UPDATE_SPLINE_CAM_SCRIPT_SPECIFIC_WIDGETS()*/
		
			
//			is_entity_in_angled_area(player_ped_id(), <<840.267, -2312.316, 29.342>>, <<863.364, -2314.499, 32.342>>, 72.0)
//			
//			is_entity_in_angled_area(player_ped_id(), <<843.540, -2286.460, 29.339>>, <<864.798, -2287.797, 32.339>>, 80.00)
//			
//			is_entity_in_angled_area(player_ped_id(), <<835.410, -2326.029, 29.339>>, <<859.732, -2316.838, 32.339>>, 22.00)

			//IS_ENTITY_IN_ANGLED_AREA(player_ped_id(), <<866.552, -2409.314, 20.350>>, <<878.432, -2299.957, 80.350>>, 170.500) 
						
			ptfx_explosion_widget()

			//BREAK_ON_NATIVE_COMMAND("clear_player_wanted_level", false)
		
			//BREAK_ON_NATIVE_COMMAND("SET_ROADS_BACK_TO_ORIGINAL", false)
		
//			if does_entity_exist(rubbish_truck.veh)
//				if not is_entity_dead(rubbish_truck.veh)
					//fire_system()
//				endif 
//			endif 

//			is_entity_in_angled_area(player_ped_id(), <<897.957, -2352.269, 29.358>>, <<884.508, -2351.092, 33.658>>, 12.300) 
//			
//			is_entity_in_angled_area(player_ped_id(), <<893.341, -2355.797, 29.358>>, <<890.660, -2355.472, 33.658>>, 13.200)
			
			//draw_marker_offset_from_entity_widget(rubbish_truck.veh)
			
			//vehicle_force_system()

			angled_area_locate_widget()
			
			//angled_area_locate_widget_entity(rubbish_truck.veh)
			
			//vehicle_index players_car = get_players_last_vehicle()
			
			vehicle_damage_widget(get_players_last_vehicle())//army_truck.veh)
			
			//draw_sprite_widget(
			
			//camera_attached_to_vehicle_widget(army_truck.veh)
			
			//attach_object_to_ped_and_move(selector_ped.pedID[selector_ped_michael], documents.obj)
			//camera_attached_to_vehicle_widget(tow_truck.veh)
			camera_attached_to_vehicle_widget(army_truck.veh)
			//create_and_move_pickup_widget(mission_pickup[0].pickup, mission_pickup[0].type, mission_pickup[0].pos, mission_pickup[0].rot)
			
			
			
			//tow_truck_recording_wdiget()

//			pickup_struct pistol
//			pickup_struct shotgun

//			if is_keyboard_key_just_pressed(key_z)
//				if DOES_ENTITY_EXIST(army_truck.veh)
//					SET_ENTITY_COORDS(army_truck.veh, <<904.981, -2367.179, 30.150>>)
//					SET_ENTITY_HEADING(army_truck.veh, 175.2936)
//				else 
//					army_truck.veh = create_vehicle(army_truck.model, <<904.981, -2367.179, 30.150>>, 175.2936)
//				endif 
//			endif 

//			if is_keyboard_key_just_pressed(key_w)
//				setup_michaels_weapons(selector_ped.pedID[SELECTOR_PED_michael])
//				setup_franklins_weapons(selector_ped.pedID[SELECTOR_PED_franklin])
//				setup_trevors_weapons(selector_ped.pedID[SELECTOR_PED_TREVOR])
//			endif 

			//create_and_move_object(bin[0].obj)
			
			
			
//			if mission_flow = shootout
//				if not does_entity_exist(bin[0].obj)
//				
//					bin[0].model = PROP_CS_DUMPSTER_01A
//					bin[0].pos = <<856.240, -2327.970, 29.850>>
//					bin[0].rot = <<0.0, 0.0, 6.950>>
//
//					bin[1].model = PROP_CS_DUMPSTER_01A
//					bin[1].pos = <<846.530, -2323.980, 29.850>> 
//					bin[1].rot = <<0.0, 0.0, 7.000>>
//					
//					request_model(bin[0].model)
//					
//					if has_model_loaded(bin[0].model)
//
//						for i = 0 to count_of(bin) - 1
//							bin[i].obj = create_object_no_offset(bin[i].model, bin[i].pos)
//							set_entity_rotation(bin[i].obj, bin[i].rot)
//							freeze_entity_position(bin[i].obj, true)
//						endfor 
//						
//					endif 
//					
//				endif
//			endif 
			
			if is_keyboard_key_just_pressed(key_t)
				PLAY_SOUND_FRONTEND(-1,"Short_Transition_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
			endif 
			
			if is_keyboard_key_just_pressed(key_o)
				PLAY_SOUND_FRONTEND(-1, "Hit_out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
			endif 

			if is_keyboard_key_just_pressed(key_s)
				end_cutscene_no_fade(false)
				mission_passed()
			endif
		
			if not stop_mission_fail_checks
				if is_keyboard_key_just_pressed(key_f)
					end_cutscene_no_fade(false)
					//mission_failed()
					setup_mission_fail()
				endif
			endif 
			
//			if is_keyboard_key_just_pressed(key_z)
//				if not is_ped_injured(helicopter_driver.ped)
//					SET_ENTITY_HEALTH(helicopter_driver.ped, 2)
//				endif 
//			endif 
			

//			player_pos = <<890.4393, -2346.4761, 29.3380 >>, 265.6000
//			widget data:
//			-400.00
//			-175.000
//			-10.000
//			400.000
//			-175.000
//			100
//			362.5
//			265.6
			//is_entity_in_angled_area(player_ped_id(), <<746.643, -1934.229, 0.343>>, <<685.267, -2731.871, 130.343>>, 362.5)
			

//			player_pos = <<890.4393, -2346.4761, 29.3380>> 
//			widget data:
//			-250.900
//			86.000
//			-10.000
//			92.900
//			84.900
//			14.400
//			111.100
//			265.400
			//is_entity_in_angled_area(player_ped_id(), <<996.284, -2103.281, 0.343>>, <<967.615, -2445.886, 130.343>>, 111.100)
			
			//is_entity_in_area(player_ped_id(), <<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>)
			
				
		//	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<935.9, -2390.9, 100.00>>, <<899.3, -2319.6, -100.00>>, false)
			
			//add_scenario_blocking_area(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>)
			//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<798.06, -1982.81, 100.00>>, <<1013.58, -2445.88, -100.00>>, false)
			
//			if does_widget_group_exist(fbi_4_widget_group)
//				DO_RE_RECORDING_WIDGET(fbi_4_widget_group)
//			endif 
//			
//			
////			**********renders debug numbers above vehicles**********
//			for i = 0 to count_of(traffic_vehicle) - 1
//			
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//			
//				if DOES_ENTITY_EXIST(traffic_vehicle[i].veh)
//					if is_vehicle_driveable(traffic_vehicle[i].veh)
//					
//						text_label vehicle_debug = ""
//						vehicle_debug += i
//
//						draw_debug_text(vehicle_debug, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(traffic_vehicle[i].veh, <<0.0, 0.0, 2.5>>))
//					endif 
//				endif
//				
//			endfor
//
//
			if widget_show_police_car_info
				for i = 0 to count_of(police_car) - 1
				
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				
					if DOES_ENTITY_EXIST(police_car[i].veh)
						if is_vehicle_driveable(police_car[i].veh)
						
							text_label vehicle_debug = ""
							vehicle_debug += i

							draw_debug_text(vehicle_debug, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(police_car[i].veh, <<0.0, 0.0, 2.5>>))
						endif 
					endif
					
				endfor
			endif 

		#endif 

	endwhile 
	
endscript 

//new locations: 
//1030.8676, -2473.8188, 27.5731

//498.7679, -955.2325, 26.2886

//991.8163, -2320.1006, 29.5095


//hot swap via spline cam

//create_player_ped_inside_vehicle(selector_ped.pedID[SELECTOR_PED_franklin], char_franklin, tow_truck.veh)

//PROC setup_hotwap_cam()
//
//	IF NOT sCamDetails.bSplineCreated
//		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
//			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
//		ENDIF                                                 
//		
//		// Initial camera behind the player       
//		ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0)    
//		ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<917.86, -1945.10, 32.72>>, <<-9.61, 0.0, -105.47>>, 1500)
//		ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<879.93, -1930.16, 34.32>>, <<-13.46, 0.0, -108.45>>, 2000)
//		sCamDetails.bSplineCreated = TRUE
//		sCamDetails.camType = SELECTOR_CAM_STRAIGHT_INTERP
//	ENDIF 
//	 
//ENDPROC
//
//
//
//				STOP_PLAYER_vehicle()
//				
//				SET_SELECTOR_PED_HINT(selector_ped, SELECTOR_PED_franklin)
//				
//				remove_blip(ambulance_target_blip)
//				
//				print_help("cntry_help2")
//
//				skip_status++
//				park_ambulance_status++
//			//endif 
//		endif 
//	endif 
//
//break 
//		
//case 4
//
//	if update_selector_hud(selector_ped)
//		if HAS_SELECTOR_PED_BEEN_SELECTED(selector_ped, SELECTOR_PED_franklin)
//		
//			clear_help()
//			
//			setup_hotwap_cam()
//			
//			sCamDetails.pedTo = selector_ped.pedID[selector_ped.eNewSelectorPed] 
//			
//			park_ambulance_status++
//		ENDIF 
//	ENDIF 
//	
//BREAK 
//
//CASE 5
//
//	IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails)	// Returns FALSE when the camera spline is complete
//		IF sCamDetails.bOKToSwitchPed
//			IF NOT sCamDetails.bPedSwitched
//				IF TAKE_CONTROL_OF_SELECTOR_PED(selector_ped, false)
//					sCamDetails.bPedSwitched = TRUE
//				endif 
//			endif 
//		endif 
//		
//	else 
//	
//		mission_fail_checks_status++
//		mission_flow = ram_army_truck
//	
//	endif 
//					
//break 


//******vehicle force physics data for stockade
//904.9805, -2363.1787, 30.150 >>   heading: 175.2936
//883.359, -2361.384, 36.000
//0.0, 0.0, 0.0
//0.0, -3.0, 0.0
//0.5

//new stockade position
//904.981, -2367.179, 30.150 heading: 175.2936
//883.359, -2365.384, 36.000
//0.0, 0.0, 0.0
//0.0, -3.0, 0.0
//0.5

//ray fire stockade data:
//904.981, -2367.179, 30.150 heading: 175.2936
//883.359, -2365.384, 36.000
//0.0, 0.0, 0.0
//0.0, -3.0, 0.52  or //0.0, -3.0, 0.5
//0.5              or //0.52

//REGISTER_CALL_FROM_CHARACTER_TO_PLAYER( CT_END_OF_MISSION,  CHAR_MICHAEL,  CHAR_LESTER,  "CONV_TEXT_BLOCK",   "CONV_LABEL",   9000)

//melanie mini garage


//FUNC BOOL CREATE_NPC_PED_ON_FOOT(PED_INDEX &ReturnPed, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE)
//
//
//FUNC BOOL CREATE_NPC_PED_INSIDE_VEHICLE(PED_INDEX &ReturnPed, enumCharacterList ePed, VEHICLE_INDEX vehicle, VEHICLE_SEAT seat = VS_DRIVER, BOOL bCleanupModel = TRUE)
//
//
//FUNC BOOL CREATE_NPC_VEHICLE(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE)




//********RUBY
//X:\gta5\tools\script\util\script\script_convert_old_cmds_to_new.rb


//Prop_Binoc_01

//107592 - ped not getting in ambulance bug



//detonate c4
//trigger c4 explosion cutscene quick cut of explosion and doors blowing off 1000 - 1500ms
//cut back to michael holding his face and camera shaking and slightly blur. Start synchronized scene for 
//Franklin and army guys. 
//shake stops and game play resumes. 


//SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(ped, TRUE)
//SET_ENTITY_HEALTH(ped, 0)


//reduce recall on weapon players weapon weapontype_combatmg 

////michael
///* START SYNCHRONIZED SCENE -  */
//VECTOR scenePosition = << 1389.399, -2065.870, 52.010 >>
//VECTOR sceneRotation = << 0.000, 0.000, 3.000 >>
//INT sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//TASK_SYNCHRONIZED_SCENE (ENTER_PED_HERE, sceneId, "missfbi4", "agents_p3_idle_b_player0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//
///* CLEAN UP SYNCHRONIZED SCENE */
//CLEAR_PED_TASKS (ENTER_PED_HERE)
//
//
////trevor
///* START SYNCHRONIZED SCENE -  */
//VECTOR scenePosition = << 1391.349, -2066.120, 52.010 >>
//VECTOR sceneRotation = << 0.000, 0.000, 0.000 >>
//INT sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//TASK_SYNCHRONIZED_SCENE (ENTER_PED_HERE, sceneId, "missfbi4", "agents_p3_idle_b_player2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//
///* CLEAN UP SYNCHRONIZED SCENE */
//CLEAR_PED_TASKS (ENTER_PED_HERE)





//RAY FIRE

//     - when the mission starts as normal via the intro mocap  THE RAY FIRE TO FIXED VIA KENNETH'S FUNCTION. 
//	 - when performing a trip skip / z skip decide what state the wall needs to be in STARTING OR FIXED. Then 
//	   implement it into the function yourself.



///336776
///345011
//346204
//194541 train line issue.

//cop stats - 396510
//367173 - bad area streaming

//get rid of cops bug - 482566

//colin camera bug - 528431

//police car driving forward 550329
//van doors disappear - 560955
//ambulance siren 411850

//script profile - 529254

//proload variations issue - 540412 

//SET_PED_STEALTH_MOVEMENT(

//help text bug for swithcing - 579540

//cars spawning infront of player

//cars driving into the fib bepot - 563881

//michael not in cover - Les bug - 787813

//if not is rubbish truck in angled area
//	if is petrol on fire in area 
//	or is petrol on fire in area get_offset_from_entity_in_world_coords(rubbish_truck.veh)
//		fail 


//1113.6, -1236.5, 20.7
	
//SET_VEHICLE_SETUP(yourVehicleIndex, g_savedGlobals.sVehicleGenData.sDynamicData[NUMBER_OF_DYNAMIC_VEHICLE_GENS-1] ) 

//839412 - MASK ANIMATION

//Wed 12/12/2012 17:33

//1007900 - franklin stands up


//OVERRIDE_LODSCALE_THIS_FRAME 

//1011213 - heli bug from LES

//rpg loads twice = 1011287

//franklin does not return to his defensive area = 1025755


//PROC SET_CUTSCENE_PED_OUTFIT(STRING sSceneHandle, MODEL_NAMES ePedModel, PED_COMP_NAME_ENUM eOutfit)

//1188477 - fbi_4_mcs_3_concat
//1190649 - no scene handles

//1204631 - tow truck recording pops

//1206530 - masks not working.

//cops not blipped coming in - 1210538

//969023 - blocking road camera.

//1214681 -michael can't aim in cover

//1228834 - truck not bruning quick enough.

//1231198 - lead in anims

//1254130 - time logged in bugs.

//1289926 - door locked

//1309245 - devins phone animated of ph_l_prop_bone

//will advance bug - 1344684


//1346229 - tow truck jumping forward 

//1338501 - ped falls off roof top nav mesh issue. 

//1353752 - helicopter does not explode on rocket fire.

//1370361 - buddies don't take damage

//1390093 - out of streaming memory after skipping intro mocap

//matt freedman 

//985472 - switch bug CL = 4082940 

//1478217 - tow truck decelerates

//1474885 - michael in wrong clothes. 

//1503873 - default camera code.

//1532688 - michael does not run into pos when on a waypoint

//1532806 - t_snip_see CONVERSATION slot gets blocked.
