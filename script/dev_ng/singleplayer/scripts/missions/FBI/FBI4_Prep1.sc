
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"

USING "CompletionPercentage_public.sch"

using "dialogue_public.sch"

USING "flow_public_core_override.sch"
USING "flow_public_GAME.sch"

using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "replay_public.sch"

USING "script_blips.sch"
USING "script_heist.sch"
USING "script_ped.sch"
 
USING "selector_public.sch"

USING "flow_special_event_checks.sch"

BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state
USING "prep_mission_common.sch"
USING "route_manager_public.sch"
USING "flow_public_GAME.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "event_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 3
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Template.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_AFTER_MOCAP                0 //following beverly down the road

//-----------------------
//		CONSTANTS
//-----------------------

//POLY TEST CONST
CONST_INT MAX_POLY_TEST_VERTS		8		//poly test
//Ambient encounter speech
CONST_INT MAX_AMBIENT_CONV_LINES	8

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
//	MS_GET_TO_STEAL_THING,		//1
	MS_STEAL,					//2
	MS_RETURN_WITH_THING,		//3
	MS_LEAVE_VEHICLE,			//4
	MS_LEAVE_AREA,
	MS_FAILED					//5
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

/// PURPOSE: Mission requirements used for loading 
///    and creating mission assets
ENUM MISSION_REQ
	RQ_NONE,
	RQ_TEXT,
	RQ_STEAL_CAR,
	RQ_STEAL_CAR_DRIVER,
	RQ_STEAL_CAR_PASSENGER
ENDENUM

/// PURPOSE: Fail reason enums for picking correct fail reason
ENUM FAILED_REASONS
	FR_NONE,
	FR_WRECKED_STEAL_CAR,
	FR_ABAN_CAR,
	FR_STUCK_CAR
ENDENUM

ENUM ABM_CONV_STATES
	ACS_START_LINE,
	ACS_WAIT_LINE_FIN,
	ACS_PICK_NEXT_LINE,
	ACS_FIN
ENDENUM

ENUM BATTLE_BUDDY_CONV_TYPE
	BBCT_WANTED = 0,
	BBCT_LOSTWANTED,
	BBCT_CHATFM,
	BBCT_CHATFT,
	BBCT_CHATTM,
	BBCT_THERE
ENDENUM

///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages
COP_MONITOR eCopMonitor = CM_MONITER
IN_STEAL_CAR_MONITOR eStealCarState = ISCM_NOT_IN_VEHICLE
DYNAMIC_PHONE_STATE ePhoneCallState = DPS_CHOOSE_PHONE_CALL


//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************

/// PURPOSE: Holds data used to create a ped 
///    Contains ped specific variables for the
///    perception system
STRUCT MYPED
	PED_INDEX 			id
	VECTOR 				vPos
	FLOAT 				fDir
	MODEL_NAMES 		Mod = DUMMY_MODEL_FOR_SCRIPT
	AI_BLIP_STRUCT		mAIBlip
ENDSTRUCT

/// PURPOSE: Holds data for creating a 
///    vehicle in an encounter
STRUCT MYVEHICLE
	VEHICLE_INDEX 		id
	VECTOR 				vPos
	FLOAT 				fDir
	MODEL_NAMES 		Mod = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

/// PURPOSE: Holds a string and a int 
///    String is used to play a conversation
///    the int is used to expire a bit in a bit field
///    
STRUCT DIALOGUE_HOLDER
	STRING 	sConv[6]
	INT		iExpiredBit = 0 
ENDSTRUCT

DIALOGUE_HOLDER mBBChatter[MAX_BATTLE_BUDDIES]


TEST_POLY mAreaCheck1		//Poly area 1
TEST_POLY mAreaCheck2		//Poly area 2

//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints

//Fail vars
STRING sFailReason = NULL				//String to display when mission is failed

//GOD TEXT
STRING sGodText = "FBIPRA"
BOOL bObjectiveShown = FALSE			//Has an objective been shown

//BLIPs
BLIP_INDEX biBlip						//Mission blip - mainly used for go to objectives
BLIP_INDEX biVehicleBlip


//ScriptCamera
CAMERA_INDEX camMain					//Script camera used in place holder cutscenes

VECTOR vSafeVec = <<0,0,0>>				//safe vector used when a proper position isnt needed 

VECTOR vDropOffLoc = <<1381.4722, -2072.2454, 50.9981>>

INT iTotalDelPoints

BOOL bDamageSpeedStatTurnedOn = FALSE
BOOL bWantedTimeStatTriggered = FALSE


BOOL bJackedConv = FALSE

BOOL bCallTimer = FALSE
INT iCallTimerDelay = 0
BOOL bForceStop = FALSE


BOOL bLoadedTrashAssets = FALSE
///==============| GOD TEXT BOOLS |============
BOOL bExpireReturnVehWanted = FALSE
BOOL bExpireReturnVeh = FALSE


///===============| models |===================


/// ===============| START VECTORS |==============

/// ==============| START HEADINGS |===============

/// ==============| PED INDICES |================
MYPED 	mGarbageTruckDriver
MYPED 	mGarbageTruckPassenger

/// ==============| VEHICLE INDICES |===========
MYVEHICLE mGarbageTruck
VEHICLE_INDEX viStealCar
VEHICLE_INDEX viAnyValidTruck

//End mission vehicle
VEHICLE_INDEX viLastCar
BOOL bSpawnedEndMissionVeh = FALSE

/// ===============| GROUPS |========================

/// ===============| DIALOGUE |======================
STRING sTextBlock = "FIBP1AU"			//The Dialogue block for the mission
structPedsForConversation s_conversation_peds		//conversation struct
TEXT_LABEL_15 sPhoneUpdateString


BOOL bAddBuddyForDialogue[MAX_BATTLE_BUDDIES]

INT iChatterTimer = -1

//Used for tracking an interupted conversation
//TEXT_LABEL_23 sResumeRoot = "NONE"
//TEXT_LABEL_23 sResumeLine = "NONE"

/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()
	IF eMissionState = MS_STEAL
		REGISTER_PED_AS_DRIVER(mGarbageTruckDriver.id)
		REGISTER_PED_AS_PASSENGER(mGarbageTruckPassenger.id)
		REGISTER_VEHICLE(mGarbageTruck.id)
		ACTIVATE_DELIVERY_MANAGER(iTotalDelPoints, TRUE, FALSE, TRUE)
	ENDIF
	bSpawnedEndMissionVeh = FALSE
	bExpireReturnVehWanted = FALSE
	bExpireReturnVeh = FALSE
	bCallTimer = FALSE
	iCallTimerDelay = 0
	bForceStop = FALSE
ENDPROC

///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID widgetGroup
	BOOL bShowDebugText = TRUE
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	
	ENDPROC
	
	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
//	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)
//
//		VECTOR vBottom[2]
//		VECTOR vTop[2]
//		
//		vBottom[0] = vec1
//		vBottom[1] = vec2
//		
//		vTop[0] = vec1
//		vTop[1] = vec2
//		
//		IF vec1.z > vec2.z
//			vBottom[0].z = vec2.z
//			vBottom[1].z = vec2.z
//			vTop[0].z = vec1.z
//			vTop[1].z = vec1.z
//		ELSE
//			vBottom[0].z = vec1.z
//			vBottom[1].z = vec1.z
//			vTop[0].z = vec2.z
//			vTop[1].z = vec2.z
//		ENDIF
//
//		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
//		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
//		VECTOR w = side * (width / 2.0)
//
//		// Bottom points
//		VECTOR c1 = vBottom[0] - w  // base left
//		VECTOR c2 = vBottom[0] + w  // base right              
//		VECTOR c3 = vBottom[1] + w  // top rt
//		VECTOR c4 = vBottom[1] - w  // top lt
//
//		// Top points
//		VECTOR d1 = vTop[0] - w  // base left
//		VECTOR d2 = vTop[0] + w  // base right              
//		VECTOR d3 = vTop[1] + w  // top rt
//		VECTOR d4 = vTop[1] - w  // top lt
//
//		// Draw bottom lines
//		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
//		// Draw top lines
//		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
//		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
//		// Draw uprights
//		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
//		
//	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Set a peds position and heading safely ie check its ok then move it and set its heading
/// PARAMS:
///    index - The ped to move
///    pos - The position to move it to
///    dir - The Heading to set
PROC SET_PED_POS(PED_INDEX index, VECTOR pos, FLOAT dir)
	IF IS_PED_UNINJURED(index)
		CLEAR_PED_TASKS(index)
	ENDIF

	SAFE_TELEPORT_ENTITY(index, pos, dir)
ENDPROC


//PURPOSE: 
/// PURPOSE:
///    Checkes to see if a ped is in a given vehicle, also does alive checks. 
///    Returns TRUE if the given ped is in the given vehicle and they are all alive and well
/// PARAMS:
///    ped - Ped to check
///    veh - The vehicle to check they are in
/// RETURNS:
///    TRUE if the ped is in the vehicle and they are both alive and well
FUNC BOOL IS_SAFE_PED_IN_VEHICLE(PED_INDEX ped, VEHICLE_INDEX veh)
	IF veh != NULL
		IF IS_VEHICLE_OK(veh)
			IF IS_PED_UNINJURED(ped)
				IF IS_PED_IN_VEHICLE(ped,veh)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Print a warning God text string and expires a bool when the warning has been shown
/// PARAMS:
///    WarnObj - the text key of the wanrning
///    expire - the bool to change
PROC PRINT_WARNING_OBJ(STRING WarnObj, BOOL &expire)
	IF NOT expire
		PRINT_NOW(WarnObj, DEFAULT_GOD_TEXT_TIME,0)
		expire = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns and sets up the ped with a debug name
/// PARAMS:
///    index - The PED_INDEX to write the newly created ped to 
///    pos - The postion to create the ped at
///    fDir - The heading to give the new ped
///    modelName - The model to create the ped with
///    i - Debug number for Debug name
///    _name - The Debug name 
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SETUP_PED(PED_INDEX &index, VECTOR pos, FLOAT fDir, MODEL_NAMES modelName, INT i, STRING _name) 
	
	IF SPAWN_PED(index, modelName, pos, fDir)
		#IF IS_DEBUG_BUILD
			SK_PRINT("SPAWNED")
		#ENDIF
		
		IF IS_PED_UNINJURED(index)
			TEXT_LABEL tDebugName = _name
			tDebugName += i
			#IF IS_DEBUG_BUILD
				SK_PRINT(tDebugName)
			#ENDIF
			SET_PED_NAME_DEBUG(index, tDebugName)
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
ENDFUNC




///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Tells a ped to leave a specified vehicle 
///    performs dead checks on the ped and vehicle
/// PARAMS:
///    ped - The ped to leave the vechicle
///    veh - The vehicle said ped is leaving
PROC GIVE_LEAVE_VEHICLE_ORDER(PED_INDEX ped, VEHICLE_INDEX veh)
	IF IS_VEHICLE_OK(veh)
		IF IS_PED_UNINJURED(ped)
			TASK_LEAVE_VEHICLE(ped, veh)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to enter a specified vehicle 
///    performs dead checks on the ped and vehicle
/// PARAMS:
///    ped - The ped to leave the vechicle
///    veh - The vehicle said ped is leaving
///    seat - Seate the ped should enter the car from
PROC GIVE_ENTER_VEHICLE_ORDER(PED_INDEX ped, VEHICLE_INDEX veh, VEHICLE_SEAT seat = VS_DRIVER)
	IF IS_VEHICLE_OK(veh)
		IF IS_PED_UNINJURED(ped)
			TASK_ENTER_VEHICLE(ped, veh, DEFAULT_TIME_BEFORE_WARP, seat)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped a waypoint task
/// PARAMS:
///    ped - The ped to give the task to 
///    waypoint - The name of the waypoint rec to give the ped
PROC GIVE_WAYPOINT_TASK(PED_INDEX ped, STRING waypoint, INT iStartPoint = 0 , EWAYPOINT_FOLLOW_FLAGS eFollow = EWAYPOINT_DEFAULT)
	IF IS_PED_UNINJURED(ped)
		FREEZE_ENTITY_POSITION(ped, FALSE)
		CLEAR_PED_TASKS(ped)
		TASK_FOLLOW_WAYPOINT_RECORDING(ped, waypoint, iStartPoint, eFollow)
	ENDIF
ENDPROC

PROC GIVE_NAVMESH_TASK(PED_INDEX ped, VECTOR vGoto, FLOAT moveBlend = PEDMOVEBLENDRATIO_WALK, INT warpTime = DEFAULT_TIME_BEFORE_WARP)
	IF IS_PED_UNINJURED(ped)
		FREEZE_ENTITY_POSITION(ped, FALSE)
		CLEAR_PED_TASKS(ped)
		TASK_FOLLOW_NAV_MESH_TO_COORD(ped, vGoto, moveBlend, warpTime)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped an attack order after the player has been spotted
///    Only called when the player fails the mission for getting spotted or attacking a ped
///    and possibly when the player has completed the sex scene section
/// PARAMS:
///    ped - The ped to give the order to
PROC GIVE_PED_ATTACK_ORDER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_COMBAT)
			FREEZE_ENTITY_POSITION(ped, FALSE)
			CLEAR_PED_TASKS(ped)
			TASK_COMBAT_PED(ped, PLAYER_PED_ID())
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to smart flee 200m from the player
///    Checks to see if the task is already being performed
///    Checks the ped is alive
/// PARAMS:
///    ped - the ped to flee
PROC GIVE_PED_FLEE_ORDER(PED_INDEX ped, BOOL bClearTasks = TRUE)
	IF IS_PED_UNINJURED(ped)
		IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_SMART_FLEE_PED) 
			FREEZE_ENTITY_POSITION(ped, FALSE)
			IF bClearTasks
				CLEAR_PED_TASKS(ped)
			ENDIF
			TASK_SMART_FLEE_PED(ped, PLAYER_PED_ID(), 200, -1)
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks that 2 entities are at the same height - within
///    1.5m
/// PARAMS:
///    e1 - first entity
///    e2 - second entity
/// RETURNS:
///    TRUE if the entities are at the same height
FUNC BOOL ARE_ENTITYS_AT_SAME_HEIGHT(ENTITY_INDEX e1, ENTITY_INDEX e2)
	VECTOR vE1 = GET_ENTITY_COORDS(e1)
	VECTOR vE2 = GET_ENTITY_COORDS(e2)
	FLOAT fDiff = ABSF(vE1.z - vE2.z) //want the absolute difference so it doesnt matter which entitys height was taken away first 
	IF fDiff <= 1.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks that the player is with in range of the hunter used when playing dialogue
/// PARAMS:
///    distance - The distance the player must be with in - defaults to 10.0
/// RETURNS:
///    TRUE if the player is with in the distance 
FUNC BOOL IS_IN_CONV_DISTANCE(PED_INDEX ped, FLOAT distance = 10.0)
	IF ped = NULL
		RETURN TRUE
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(ped)
//		#IF IS_DEBUG_BUILD SK_PRINT_FLOAT(" DISTANCE BETWEEN CONVO  === ", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE))#ENDIF
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE) <= distance
		AND ARE_ENTITYS_AT_SAME_HEIGHT(ped, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates a conversation and then displays an objective
///    Expires a bool when the conversation has been created
///    assume the player is talking to another ped
///    kills the conversation if its started and the peds are too far away
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The bool to expire when the conversation has been created (ref)
///    sConvo - the string of the conversation to play
///    sObj - the text key for the obj
///    distance - the distance the player and ped need to be in before the convostation happens
PROC DO_CONVO_WITH_OBJ(PED_INDEX ped, BOOL &bExpire, STRING sConvo, STRING sObj, FLOAT distance = 10.0)
	
	IF NOT bObjectiveShown
//		#IF IS_DEBUG_BUILD SK_PRINT(sConvo) #ENDIF
//		#IF IS_DEBUG_BUILD SK_PRINT(" NOT bObjectiveShown") #ENDIF
//
		IF NOT bExpire
//			#IF IS_DEBUG_BUILD SK_PRINT("NOT bExpire") #ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				IF IS_IN_CONV_DISTANCE(ped, distance)
					bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_IN_CONV_DISTANCE(ped, distance)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				PRINT_OBJ(sObj, bObjectiveShown) 
		 	ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

/// PURPOSE:
///    Creates a conversation and then displays an objective
///    And then creates another conversation
///    Expires a bool when each of the conversation have been created
///    assume the player is talking to another ped
///    kills the conversation if its started and the peds are too far away
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The expiry bool for the first conversation (ref)
///    bExpireAfterConv - bool for the second conversation (ref)
///    sConvo - the string of the 1st conversation to play
///    sConvToPlayAfterObj - the string 2nd of the conversation to play
///    sObj - the text key for the obj
///    distance - the distance the player and ped need to be in before the convostation happens
PROC DO_TWO_CONVO_WITH_OBJ(PED_INDEX ped, BOOL &bExpire, BOOL &bExpireAfterConv, STRING sConvo, STRING sConvToPlayAfterObj, STRING sObj, FLOAT distance = 10.0)
	
	IF NOT bObjectiveShown
//		#IF IS_DEBUG_BUILD SK_PRINT(" NOT bObjectiveShown") #ENDIF
		
		IF NOT bExpire
//			#IF IS_DEBUG_BUILD SK_PRINT(sConvo) #ENDIF
//			#IF IS_DEBUG_BUILD SK_PRINT("NOT bExpire") #ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				IF IS_IN_CONV_DISTANCE(ped, distance)
					bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_IN_CONV_DISTANCE(ped, distance)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF

			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				PRINT_OBJ(sObj, bObjectiveShown) 
		 	ENDIF
		ENDIF
	ELSE
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sObj)
		OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
			IF NOT IS_STRING_NULL_OR_EMPTY(sConvToPlayAfterObj)
				IF NOT bExpireAfterConv
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
						IF IS_IN_CONV_DISTANCE(ped, distance)
							bExpireAfterConv = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvToPlayAfterObj, CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_IN_CONV_DISTANCE(ped, distance)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			ELSE
				bExpireAfterConv = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a conversation then expires a bool when the conversation has been created
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The expiry bool for the first conversation (ref)
///    sConvo - the string of the conversation to play
///    distance - the distance the player and ped need to be in before the convostation happens
/// RETURNS:
///    true when the bool is expired
FUNC BOOL DO_CONVO(PED_INDEX ped, BOOL &bExpire, STRING sConvo, FLOAT distance = 10.0, enumConversationPriority priority = CONV_PRIORITY_MEDIUM)
	
	IF NOT bExpire
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND IS_SCREEN_FADED_IN()
			IF IS_IN_CONV_DISTANCE(ped, distance)
				bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, priority)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_IN_CONV_DISTANCE(ped, distance)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
	RETURN bExpire
ENDFUNC



/// PURPOSE:
///    Checks to see if the player has killed innocents then increaments the mission stat.
/// PARAMS:
///    bIsAfterOtherFail - Flag to see if the player has already failed the mission when this 
///    returns true and changes the mission fail reason accordingly.
PROC CHECK_FOR_MURDER()
/*
	PED_INDEX mPed
	INT i = 0
	IF  Has_Ped_Been_Killed()

		REPEAT Get_Number_Of_Ped_Killed_Events() i
			IF DOES_ENTITY_EXIST(Get_Index_Of_Killed_Ped(i))
            	mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Killed_Ped(i))
				IF mPed <> PLAYER_PED_ID()
					IF IS_ENTITY_A_PED(mPed)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(mPed)
						AND IS_PED_HUMAN(mPed)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
								//this stat has been removed
								//INFORM_MISSION_STATS_OF_INCREMENT(FBI4P1_INNOCENTS_KILLED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF	
	*/
ENDPROC

PROC SET_STATS_WATCH_OFF()
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL) 
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, FBI4P1_VEHICLE_DAMAGE)
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, FBI4P1_LEVEL_LOSS)
ENDPROC

PROC MANAGE_PLAYER_VEHICLE_STATS()
	IF NOT bDamageSpeedStatTurnedOn
		IF IS_VEHICLE_OK(viStealCar)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(viStealCar) 
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(viStealCar, FBI4P1_VEHICLE_DAMAGE)
				bDamageSpeedStatTurnedOn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bWantedTimeStatTriggered
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI4P1_LEVEL_LOSS)
			bWantedTimeStatTriggered = TRUE
		ENDIF
	ELSE
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, FBI4P1_LEVEL_LOSS)
			bWantedTimeStatTriggered = FALSE
		ENDIF		
	ENDIF
	
ENDPROC

PROC MONITER_STATS()
	CHECK_FOR_MURDER()
	MANAGE_PLAYER_VEHICLE_STATS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail = FR_NONE)
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	SWITCH fail		
		CASE FR_NONE
			
		BREAK

		CASE FR_WRECKED_STEAL_CAR
			sFailReason = "PRA_FWRECK"
		BREAK
		
		CASE FR_ABAN_CAR
			sFailReason = "PRA_FFAR"
		BREAK
		
		CASE FR_STUCK_CAR
			sFailReason = "PRA_FSTUCK"
		BREAK

	ENDSWITCH
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the mission check point 
PROC SET_CHECKPOINT()

ENDPROC

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads a scene around a point used to the player 
///    seeing the map stream in when starting a replay or debug skipping
/// PARAMS:
///    pos - the position to stream the scene around at
///    rad - the radius of the scene to load
PROC LOAD_SCENE_SKIP(VECTOR pos, FLOAT rad)
	NEW_LOAD_SCENE_START_SPHERE(pos, rad)
	INT i_load_scene_timer = GET_GAME_TIMER()
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED()) AND (GET_GAME_TIMER() - i_load_scene_timer < 12000)
		WAIT(0)
	ENDWHILE
	NEW_LOAD_SCENE_STOP()
ENDPROC

/// PURPOSE:
///    Adds peds for dialogue based off the Mission state
PROC ADD_PEDS_FOR_DIALOGUE()
	ADD_CURRENT_PLAYER_FOR_DIALOGUE(s_conversation_peds)
ENDPROC

FUNC BOOL IS_PLAYER_IN_CORRECT_MODEL_ON_START()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRASH)
			SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE, FALSE)
			viStealCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_OK(viStealCar)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	SWITCH missionReq
		CASE RQ_NONE
			IF ARE_VECTORS_ALMOST_EQUAL(pos, vSafeVec)
			AND dir = 0.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_TEXT
			REQUEST_ADDITIONAL_TEXT(sGodText, MISSION_TEXT_SLOT)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TEXT")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("TEXT FAILED")
			#ENDIF
		BREAK

		CASE RQ_STEAL_CAR		
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
				mGarbageTruck.id = g_sTriggerSceneAssets.veh[0]
				
				IF IS_VEHICLE_OK(mGarbageTruck.id)
					SET_VEHICLE_AS_RESTRICTED(mGarbageTruck.id, 0)
					SET_VEHICLE_HAS_STRONG_AXLES(mGarbageTruck.id, TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mGarbageTruck.id, TRUE)
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_VEHICLE(mGarbageTruck.id, mGarbageTruck.Mod, mGarbageTruck.vPos, mGarbageTruck.fDir)
					SET_VEHICLE_AS_RESTRICTED(mGarbageTruck.id, 0)
					SET_VEHICLE_HAS_STRONG_AXLES(mGarbageTruck.id, TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mGarbageTruck.id, TRUE)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_STEAL_CAR")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_STEAL_CAR FAILED")
			#ENDIF		
		BREAK

		CASE RQ_STEAL_CAR_DRIVER		
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_DEAD_OR_DYING(g_sTriggerSceneAssets.ped[0])
				    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
					mGarbageTruckDriver.id = g_sTriggerSceneAssets.ped[0]
					
					IF IS_PED_UNINJURED(mGarbageTruckDriver.id)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mGarbageTruckDriver.id, TRUE)
						SET_PED_CONFIG_FLAG(mGarbageTruckDriver.id, PCF_ForceDirectEntry, FALSE)
						SET_PED_FLEE_ATTRIBUTES(mGarbageTruckDriver.id, FA_FORCE_EXIT_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(mGarbageTruckDriver.id, CA_CAN_CHARGE, TRUE)
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_PED(mGarbageTruckDriver.id, mGarbageTruckDriver.Mod, mGarbageTruckDriver.vPos, mGarbageTruckDriver.fDir)
					IF IS_VEHICLE_OK(mGarbageTruck.id)
						SET_PED_INTO_VEHICLE(mGarbageTruckDriver.id, mGarbageTruck.id)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mGarbageTruckDriver.id, TRUE)
						SET_PED_CONFIG_FLAG(mGarbageTruckDriver.id, PCF_ForceDirectEntry, FALSE)
						SET_PED_FLEE_ATTRIBUTES(mGarbageTruckDriver.id, FA_FORCE_EXIT_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(mGarbageTruckDriver.id, CA_CAN_CHARGE, TRUE)						#IF IS_DEBUG_BUILD
							SK_PRINT("RQ_STEAL_CAR_DRIVER")
						#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF 
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_STEAL_CAR_DRIVER FAILED")
			#ENDIF		
		BREAK
		
		CASE RQ_STEAL_CAR_PASSENGER
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				IF NOT IS_PED_DEAD_OR_DYING(g_sTriggerSceneAssets.ped[1])
				    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
					mGarbageTruckPassenger.id = g_sTriggerSceneAssets.ped[1]
					
					IF IS_PED_UNINJURED(mGarbageTruckPassenger.id)
						SET_PED_CONFIG_FLAG(mGarbageTruckPassenger.id, PCF_ForceDirectEntry, FALSE)
						SET_PED_FLEE_ATTRIBUTES(mGarbageTruckPassenger.id, FA_FORCE_EXIT_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(mGarbageTruckPassenger.id, CA_CAN_CHARGE, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mGarbageTruckPassenger.id, TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(mGarbageTruckPassenger.id, TRUE)
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_PED(mGarbageTruckPassenger.id, mGarbageTruckPassenger.Mod, mGarbageTruckPassenger.vPos, mGarbageTruckPassenger.fDir)
					IF IS_VEHICLE_OK(mGarbageTruck.id)
						SET_PED_INTO_VEHICLE(mGarbageTruckPassenger.id, mGarbageTruck.id, VS_FRONT_RIGHT)
						SET_PED_CONFIG_FLAG(mGarbageTruckPassenger.id, PCF_ForceDirectEntry, FALSE)
						SET_PED_FLEE_ATTRIBUTES(mGarbageTruckPassenger.id, FA_FORCE_EXIT_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(mGarbageTruckPassenger.id, CA_CAN_CHARGE, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mGarbageTruckPassenger.id, TRUE)
						SET_ENTITY_LOAD_COLLISION_FLAG(mGarbageTruckPassenger.id, TRUE)
						#IF IS_DEBUG_BUILD
							SK_PRINT("RQ_STEAL_CAR_DRIVER")
						#ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_STEAL_CAR_DRIVER FAILED")
			#ENDIF		
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_TEXT, vSafeVec)
					IF IS_PLAYER_IN_CORRECT_MODEL_ON_START()
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4_PREP_1)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(S_M_Y_GARBAGE, TRUE)
						eMissionState = MS_RETURN_WITH_THING//tesst
						SAFE_REMOVE_BLIP(biBlip)
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						SET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, <<1244.3380, -339.3197, 68.0823>>)
						ADD_PEDS_FOR_DIALOGUE()
						SET_SCENARIO_TYPE_ENABLED("DRIVE", FALSE)
						SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_ATTRACTOR", FALSE)
						SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_PASSENGERS", FALSE)
						SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
						RETURN TRUE
					ELSE
						IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, mGarbageTruck.vPos, mGarbageTruck.fDir)
						AND SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR_DRIVER, mGarbageTruckDriver.vPos, mGarbageTruckDriver.fDir)
						AND SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR_PASSENGER, mGarbageTruckPassenger.vPos, mGarbageTruckPassenger.fDir)
						AND LOAD_DELIVERY_ANIMS()
							ADD_PEDS_FOR_DIALOGUE()
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_FBI_4_PREP_1)
							IF IS_PED_UNINJURED(mGarbageTruckDriver.id)
								REGISTER_PED_AS_DRIVER(mGarbageTruckDriver.id)
							ENDIF
							IF IS_PED_UNINJURED(mGarbageTruckPassenger.id)
								REGISTER_PED_AS_PASSENGER(mGarbageTruckPassenger.id)
							ENDIF
							
							REGISTER_VEHICLE(mGarbageTruck.id)
							
							IF IS_PED_UNINJURED(mGarbageTruckPassenger.id)
							AND IS_PED_UNINJURED(mGarbageTruckDriver.id)
								ACTIVATE_DELIVERY_MANAGER(iTotalDelPoints, TRUE, FALSE, TRUE)
							ENDIF
							
							SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, TRUE)
							SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
							SET_VEHICLE_MODEL_IS_SUPPRESSED(S_M_Y_GARBAGE, TRUE)
							IF GET_BLIP_FROM_ENTITY(mGarbageTruck.id) != NULL
								BLIP_INDEX blip 
								blip = GET_BLIP_FROM_ENTITY(mGarbageTruck.id)
								SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
								SAFE_REMOVE_BLIP(blip)
								SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
							ENDIF
							ADD_SAFE_BLIP_TO_VEHICLE(biBlip, mGarbageTruck.id, TRUE)
							SET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, <<1244.3380, -339.3197, 68.0823>>)
							SET_SCENARIO_TYPE_ENABLED("DRIVE", FALSE)
							SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_ATTRACTOR", FALSE)
							SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_PASSENGERS", FALSE)
							SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
							RETURN TRUE //all mission stage requirements set up return true and activate stage
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED")
			#ENDIF
		BREAK

		CASE MS_STEAL
//			CLEAR_AREA_OF_VEHICLES(mGarbageTruck.vPos, 60)
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, mGarbageTruck.vPos, mGarbageTruck.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR_DRIVER, mGarbageTruckDriver.vPos, mGarbageTruckDriver.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR_PASSENGER, mGarbageTruckPassenger.vPos, mGarbageTruckPassenger.fDir)
					SET_PED_POS(PLAYER_PED_ID(), <<1197.8611, -353.0110, 68.0929>>, 278.9647)
					RESET_ALL()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					SET_SCENARIO_TYPE_ENABLED("DRIVE", FALSE)
					SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_ATTRACTOR", FALSE)
					SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_PASSENGERS", FALSE)
					SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
					RC_END_Z_SKIP()
				ENDIF
			ELSE
//				IF LOAD_DELIVERY_ANIMS()
					ADD_PEDS_FOR_DIALOGUE()
					RETURN TRUE //all mission stage requirements set up return true and activate stage
//				ENDIf
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_STEAL, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_RETURN_WITH_THING
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, mGarbageTruck.vPos, mGarbageTruck.fDir)
					RESET_ALL()
					ADD_PEDS_FOR_DIALOGUE()
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mGarbageTruck.id)
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_RETURN_WITH_THING, FAILED") 
			#ENDIF
		BREAK

		CASE MS_LEAVE_VEHICLE
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vDropOffLoc, mGarbageTruck.fDir)
					RESET_ALL()
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mGarbageTruck.id)
					ADD_PEDS_FOR_DIALOGUE()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_LEAVE_VEHICLE, FAILED") 
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sits in the NEXT_STAGE() function and moniters for assest that
///    should be unloaded based on the mission state
/// PARAMS:
///    state - the current state we should evaluate
PROC MONITER_UNLOAD_ASSETS(MISSION_STATE state)
	SWITCH state
		CASE MS_SET_UP
			
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_BUDDY_NEED_TO_DRIVE_TRUCK(PED_INDEX hPed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	
		IF DOES_ENTITY_EXIST(mGarbageTruck.id)
		AND IS_VEHICLE_DRIVEABLE(mGarbageTruck.id)
		AND NOT IS_ENTITY_AT_COORD(mGarbageTruck.id, vDropOffLoc, <<2,2,2>>)
					
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mGarbageTruck.id)
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			
				IF IS_PED_IN_VEHICLE(hPed, mGarbageTruck.id)
				AND GET_PED_IN_VEHICLE_SEAT(mGarbageTruck.id, VS_DRIVER) = hPed

					RETURN TRUE
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_BUUDY_NEED_TO_CHASE_TRUCK(PED_INDEX hPed)
	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			VEHICLE_INDEX Veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(Veh)
				IF IS_VEHICLE_MODEL(Veh, TRASH)
					RETURN FALSE
				ENDIF
				
				IF IS_PED_IN_VEHICLE(hPed, Veh)
				AND GET_PED_IN_VEHICLE_SEAT(Veh, VS_DRIVER) = hPed
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_BUDDY_CHATTER(enumCharacterList eChar, BATTLE_BUDDY_CONV_TYPE eType)
	IF NOT IS_BIT_SET(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(eType))	
		IF iChatterTimer != -1
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, mBBChatter[eChar].sConv[eType], CONV_PRIORITY_MEDIUM)
					SET_BIT(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(eType))
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				iChatterTimer = GET_GAME_TIMER()
			ELSE
				iChatterTimer = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BATTLE_BUDDY_CONV_TYPE eType)
	IF IS_BIT_SET(mBBChatter[CHAR_MICHAEL].iExpiredBit, ENUM_TO_INT(eType))
	OR IS_BIT_SET(mBBChatter[CHAR_FRANKLIN].iExpiredBit, ENUM_TO_INT(eType))
	OR IS_BIT_SET(mBBChatter[CHAR_TREVOR].iExpiredBit, ENUM_TO_INT(eType))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MONITOR_BUDDY_DIALOGUE(enumCharacterList eChar)
	IF eMissionState > MS_STEAL
		IF eCopMonitor > CM_MONITER
			IF NOT IS_BIT_SET(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_WANTED))
			AND eStealCarState = ISCM_IN_VEHICLE
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, mBBChatter[eChar].sConv[BBCT_WANTED], CONV_PRIORITY_HIGH)
						SET_BIT(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_WANTED))
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_WANTED))
			AND NOT IS_BIT_SET(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_LOSTWANTED))
			AND eStealCarState = ISCM_IN_VEHICLE
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, mBBChatter[eChar].sConv[BBCT_LOSTWANTED], CONV_PRIORITY_MEDIUM)
						SET_BIT(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_LOSTWANTED))
					ENDIF
				ENDIF
			ENDIF

			IF NOT IS_BIT_SET(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_THERE))
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(viStealCar, vDropOffLoc) < 15
				AND eStealCarState = ISCM_IN_VEHICLE
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, mBBChatter[eChar].sConv[BBCT_THERE], CONV_PRIORITY_MEDIUM)
							SET_BIT(mBBChatter[eChar].iExpiredBit, ENUM_TO_INT(BBCT_THERE))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
				CASE CHAR_MICHAEL
					IF eChar = CHAR_FRANKLIN
						IF NOT HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BBCT_CHATFM)
							DO_BUDDY_CHATTER(eChar, BBCT_CHATFM)
						ENDIF
					ELIF eChar = CHAR_TREVOR
						IF NOT HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BBCT_CHATTM)
							DO_BUDDY_CHATTER(eChar, BBCT_CHATTM)
						ENDIF
					ENDIF
				BREAK
				
				CASE CHAR_FRANKLIN
					IF eChar = CHAR_MICHAEL
						IF NOT HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BBCT_CHATFM)
							DO_BUDDY_CHATTER(eChar, BBCT_CHATFM)
						ENDIF
					ELIF eChar = CHAR_TREVOR
						IF NOT HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BBCT_CHATFT)
							DO_BUDDY_CHATTER(eChar, BBCT_CHATFT)
						ENDIF
					ENDIF
				BREAK
				
				CASE CHAR_TREVOR
					IF eChar = CHAR_FRANKLIN
						IF NOT HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BBCT_CHATFT)
							DO_BUDDY_CHATTER(eChar, BBCT_CHATFT)
						ENDIF
					ELIF eChar = CHAR_MICHAEL
						IF NOT HAS_ANY_BB_CONV_EXPIRED_OF_TYPE(BBCT_CHATTM)
							DO_BUDDY_CHATTER(eChar, BBCT_CHATTM)
						ENDIF
					ENDIF
				BREAK
			
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

PROC MONITOR_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	BOOL bUpdatedDialogue = FALSE
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
				IF NOT bUpdatedDialogue
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), hBuddy, 5)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							MONITOR_BUDDY_DIALOGUE(eChar)
							bUpdatedDialogue = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT bAddBuddyForDialogue[ENUM_TO_INT(eChar)]
					IF eChar = CHAR_MICHAEL
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, ENUM_TO_INT(eChar), hBuddy, "MICHAEL")
					ELIF eChar = CHAR_FRANKLIN
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, ENUM_TO_INT(eChar), hBuddy, "FRANKLIN")
					ELIF eChar = CHAR_TREVOR
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, ENUM_TO_INT(eChar), hBuddy, "TREVOR")
					ENDIF
					bAddBuddyForDialogue[ENUM_TO_INT(eChar)] = TRUE
				ENDIF
			ENDIF

			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
			
				// Does script need to take control of buddy and drive to dest?
				IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
					IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
					OR DOES_BUUDY_NEED_TO_CHASE_TRUCK(hBuddy)
						IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
							SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
							CLEAR_PED_TASKS(hBuddy)
						ENDIF
					ENDIF
				ENDIF
			
			ELSE

				// Does script still need to take control of buddy and drive to dest?
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
					IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
						
						// Buddy drives truck to destination
						IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
							
							IF ARE_NODES_LOADED_FOR_AREA(623.37811, -172.26003, 1700.73169, -2115.84277)
								TASK_VEHICLE_DRIVE_TO_COORD(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), vDropOffLoc, 20.0, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS|DF_UseStringPullingAtJunctions,2.0, 10.0)
							ELSE
								REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(623.37811, -172.26003, 1700.73169, -2115.84277)
								#IF IS_DEBUG_BUILD  
									SK_PRINT("LOADING NODES") 
								#ENDIF
							ENDIF
						ENDIF
					ELIF DOES_BUUDY_NEED_TO_CHASE_TRUCK(hBuddy)
						IF IS_VEHICLE_OK(mGarbageTruck.id)
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(hBuddy, mGarbageTruck.id, 17)
								IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
									TASK_VEHICLE_MISSION(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), mGarbageTruck.id, MISSION_BLOCK, 20, DRIVINGMODE_AVOIDCARS_RECKLESS, 2, 0.5)
								ENDIF
							ELSE
								
							ENDIF
						ENDIF
					ELSE
						
						RELEASE_BATTLEBUDDY(hBuddy)
					ENDIF
				
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

PROC RELEASE_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
				RELEASE_BATTLEBUDDY(hBuddy)
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Unloads all models
PROC UNLOAD_ALL_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(mGarbageTruck.Mod)
ENDPROC

/// PURPOSE:
///    Unloads all waypoints 
PROC UNLOAD_ALL_WAYPOINTS()
ENDPROC

/// PURPOSE:
///    Unloads all anims
PROC UNLOAD_ANIMS()
	IF bLoadedTrashAssets
		REMOVE_VEHICLE_ASSET(TRASH)
	ENDIF
ENDPROC

/// PURPOSE:
///    Unload all vehicle recordings 
PROC UNLOAD_ALL_CAR_RECS()
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBlip)
	
ENDPROC


/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
	SAFE_DELETE_VEHICLE(mGarbageTruck.id)
	SAFE_DELETE_VEHICLE(viAnyValidTruck)
	SAFE_DELETE_VEHICLE(viStealCar)
	SAFE_DELETE_PED(mGarbageTruckDriver.id)
	SAFE_DELETE_PED(mGarbageTruckPassenger.id)
	SAFE_DELETE_VEHICLE(viLastCar)
	RELEASE_DELIVERY_ENTITIES()
ENDPROC

PROC RELEASE_ALL()
	RELEASE_DELIVERY_ENTITIES()
	SAFE_RELEASE_VEHICLE(mGarbageTruck.id)
	SAFE_RELEASE_VEHICLE(viAnyValidTruck)
	SAFE_RELEASE_VEHICLE(viStealCar)
	SAFE_RELEASE_PED(mGarbageTruckDriver.id)
	SAFE_RELEASE_PED(mGarbageTruckPassenger.id)
	SAFE_RELEASE_VEHICLE(viLastCar)
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF


	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_SCENARIO_TYPE_ENABLED("DRIVE", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_ATTRACTOR", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_PASSENGERS", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
	
	REMOVE_BLIPS()
	UNLOAD_ALL_MODELS()
	UNLOAD_ALL_WAYPOINTS()
	UNLOAD_ANIMS()
	UNLOAD_ALL_CAR_RECS()
	RELEASE_BATTLE_BUDDIES()
	IF bDelAll
		IF DOES_ENTITY_EXIST(viStealCar)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
						VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) // get out of vehicle
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		CLEAR_PRINTS()
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	IF DOES_CAM_EXIST(camMain)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(camMain)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()
//	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT,TRUE)
	SERVICES_TOGGLE(TRUE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(S_M_Y_GARBAGE, FALSE)
	
	SET_STATS_WATCH_OFF()
	//RELEASE_PATH_NODES()
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	OPEN_MASK_SHOP_ON_OTHER_PREP()
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	MONITER_UNLOAD_ASSETS(eMissionState)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	Mission_Flow_Mission_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets where the player will be warped to if they accept the replay
PROC SET_REPLAY_ACCEPTED_WARP_LOCATION()
	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<918.8851, -269.7890, 67.2145>>, 325.9081)
	SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<917.5291, -263.8595, 67.3489>>, 188.0605)
ENDPROC


/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_BLIPS()

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX veh
				veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(veh)
				AND GET_ENTITY_MODEL(veh) = TOWTRUCK
					SET_VEHICLE_AS_RESTRICTED(veh, 2)
				ENDIF
			ENDIF

			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)  
			ELSE
				MISSION_FLOW_MISSION_FAILED()
			ENDIF

			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//
				IF HAS_PLAYER_ACCEPTED_REPLAY()
					SET_REPLAY_ACCEPTED_WARP_LOCATION()
				ENDIF

				DELETE_ALL()
				CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, TRUE)
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
//	bLoadingFinCutscene = FALSE
	bObjectiveShown = FALSE
	eState = SS_INIT
	CLEANUP() //delete everything
ENDPROC

/// PURPOSE:
///    Fills an new MYPED
/// PARAMS:
///    pos - position the ped is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    a newly initialised MYPED
FUNC MYPED FILL_PED(VECTOR pos, FLOAT dir, MODEL_NAMES mod)
	MYPED mTempPed
	
	mTempPed.vPos = pos
	mTempPed.fDir = dir
	mTempPed.Mod = mod
	
	RETURN mTempPed 
ENDFUNC

/// PURPOSE:
///    Fills a new MYVEHICLE
/// PARAMS:
///    pos - the position the vehicle is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    A newly initialised MYVEHICLE
FUNC MYVEHICLE FILL_VEHICLE(VECTOR pos, FLOAT dir, MODEL_NAMES mod)
	MYVEHICLE mTempVeh
	
	mTempVeh.vPos = pos
	mTempVeh.fDir = dir
	mTempVeh.Mod = mod
	
	RETURN mTempVeh 
ENDFUNC

/// PURPOSE:
///    Adds points to a poly 
/// PARAMS:
///    polys - The points to be added to the poly 
///    count - the number of points to be added to the poly 
PROC POPULATE_POLY_CHECKS(VECTOR &polys[], INT count = MAX_POLY_TEST_VERTS)
	 

	OPEN_TEST_POLY(mAreaCheck1)
	INT i
	
	FOR i = 0 TO (count-1)
		ADD_TEST_POLY_VERT(mAreaCheck1, polys[i])
	ENDFOR
	CLOSE_TEST_POLY(mAreaCheck1)
	
	COPY_EXPANDED_POLY(mAreaCheck2, mAreaCheck1, 50)

ENDPROC

/// PURPOSE:
///    Creates a poly check area using a local array
PROC CREATE_POLY_CHECKS()
	VECTOR polys[6]
	polys[0] = <<1251.3082, -400.6231, 68.0926>>
	polys[1] = <<1183.9928, -403.3443, 66.8734>>
	polys[2] = <<1161.5404, -372.6073, 66.6034>>
	polys[3] = <<1167.6444, -273.7987, 67.9705>>
	polys[4] = <<1252.3618, -247.3530, 77.5631>>
	polys[5] = <<1330.0615, -341.0342, 100.3476>>
	POPULATE_POLY_CHECKS(polys, 6)
ENDPROC

/// PURPOSE:
///    Fill in a new dialogue holder
/// PARAMS:
///    sConv - the coversation lable to store in this holder
///    iExpireBit - the bit that will be used to expire the conversation
/// RETURNS:
///    a newly initialised DIALOGUE_HOLDER
FUNC DIALOGUE_HOLDER FILL_DIALOGUE(STRING sConv1, STRING sConv2, STRING sConv3, STRING sConvFM, STRING sConvFT, STRING sConvTM)
	DIALOGUE_HOLDER TempDial
	
	TempDial.sConv[BBCT_WANTED] 		= sConv1
	TempDial.sConv[BBCT_LOSTWANTED] 	= sConv2
	TempDial.sConv[BBCT_THERE] 			= sConv3	
	
	TempDial.sConv[BBCT_CHATFM] 			= sConvFM	
	TempDial.sConv[BBCT_CHATFT] 			= sConvFT	
	TempDial.sConv[BBCT_CHATTM] 			= sConvTM

	RETURN TempDial

ENDFUNC

PROC POPULATE_VEHICLES()
	mGarbageTruck = FILL_VEHICLE(<<1203.1462, -337.3543, 67.9314>>, 189.7047, TRASH)//
ENDPROC

PROC POPULATE_PEDS()
	mGarbageTruckDriver = FILL_PED(<<1200.6096, -344.2721, 68.0424>>, 269.2015, S_M_Y_GARBAGE)
	mGarbageTruckPassenger = FILL_PED(<<1202.4204, -341.6034, 67.9378>>, 273.1125, S_M_Y_GARBAGE)
ENDPROC

PROC POPULATE_DELIVERY_POINTS()
	mDeliveryPoint[0] = FILL_DELIVERY_POINT_DEST_PED(<<1203.1462, -337.3543, 67.9314>>,<<1245.1176, -348.5059, 68.2099>>, 253.1908, DUMMY_MODEL_FOR_SCRIPT)
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[0], vSafeVec, FALSE, 15, 5, 270,  PARK_TYPE_PULL_OVER, <<1192.0063, -631.4703, 61.6972>>, <<10,10, 5>>)

	mDeliveryPoint[1] = FILL_DELIVERY_POINT_DEST_PED(<<1215.0840, -339.3139, 68.1323>>, <<964.9214, -555.2120, 58.0211>>, 39.6450, DUMMY_MODEL_FOR_SCRIPT)
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[1], vSafeVec, FALSE, 15, 10, 270, PARK_TYPE_PULL_OVER, <<1103.2896, -764.5320, 56.6330>>, <<10,10, 5>>)

	mDeliveryPoint[2] = FILL_DELIVERY_POINT_DEST_PED( <<1179.5330, -361.3286, 67.4559>>, <<929.8701, -619.4534, 56.4632>>, 164.3631, DUMMY_MODEL_FOR_SCRIPT)
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[2], vSafeVec, FALSE, 15, 10, 270, PARK_TYPE_PULL_OVER, <<933.3745, -615.9453, 56.3041>>, <<10,10, 5>>)

	mDeliveryPoint[3] = FILL_DELIVERY_POINT_DEST_PED(<<1107.6744, -364.0569, 65.9581>>, <<1099.2655, -775.0809, 57.3525>>, 178.2038, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[3], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1062.1489, -479.4801, 62.9454>>, <<10,10, 5>>)

	mDeliveryPoint[4] = FILL_DELIVERY_POINT_DEST_PED(<<1069.2791, -393.5795, 66.0262>>, <<1065.3644, -389.9561, 66.1504>>, 220.1611, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[4], vSafeVec, FALSE, 5, 30, 128.1732, PARK_TYPE_PULL_OVER, <<1069.7177, -393.9912, 66.0261>>, <<10,10, 5>>)

	mDeliveryPoint[5] = FILL_DELIVERY_POINT_DEST_PED(<<1031.6730, -424.5990, 64.5439>>, <<1027.1793, -420.6656, 64.6268>>, 178.2038, P_BINBAG_01_S )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[5], <<1028.2009, -428.3677, 64.3571>>, TRUE, 15, 2, 128.3915, PARK_TYPE_PERPENDICULAR_NOSE_IN, <<1027.1793, -420.6656, 64.6268>>, <<10,10, 5>>)

	mDeliveryPoint[6] = FILL_DELIVERY_POINT_DEST_PED(<<942.1528, -488.8956, 59.3129>>, <<932.0198, -489.3886, 58.9212>>, 202.5785, P_BINBAG_01_S )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[6], <<930.7186, -494.9253, 58.6160>>, TRUE, 15, 2, 119.6518, PARK_TYPE_PERPENDICULAR_NOSE_IN, <<952.9781, -482.5082, 60.0003>>, <<10,10, 5>>)

	mDeliveryPoint[7] = FILL_DELIVERY_POINT_DEST_PED(<<868.2734, -529.5473, 56.2071>>, <<865.0042, -522.4746, 56.3363>>, 241.9711, P_BINBAG_01_S)
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[7], <<865.2349, -535.8759, 56.1574>>, TRUE, 15, 2, 162.7219, PARK_TYPE_PERPENDICULAR_NOSE_IN, <<1062.1489, -479.4801, 62.9454>>, <<10,10, 5>>)
	
	mDeliveryPoint[8] = FILL_DELIVERY_POINT_DEST_PED(<<874.8076, -571.2073, 56.2987>>, <<983.1250, -541.9902, 58.5929>>, 49.7653, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[8], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1062.1489, -479.4801, 62.9454>>, <<10,10, 5>>)
	
	mDeliveryPoint[9] = FILL_DELIVERY_POINT_DEST_PED(<<931.5218, -614.2144, 56.3119>>, <<928.6471, -620.7110, 56.4611>>, -108.06, P_BINBAG_01_S )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[9], <<940.4086, -622.1787, 56.3003>>, TRUE, 15, 2, 228.1176, PARK_TYPE_PERPENDICULAR_NOSE_IN, <<918.6716, -602.9258, 56.3395>>, <<10,10, 5>>)
	
	mDeliveryPoint[10] = FILL_DELIVERY_POINT_DEST_PED(<<964.9758, -645.9614, 56.4465>>, <<927.1033, -575.4162, 56.5329>>, 55.9317, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[10], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1062.1489, -479.4801, 62.9454>>, <<10,10, 5>>)

	mDeliveryPoint[11] = FILL_DELIVERY_POINT_DEST_PED(<<983.4438, -683.1442, 56.4087>>, <<929.3757, -618.9091, 56.4612>>, 181.1844, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[11], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)

	mDeliveryPoint[12] = FILL_DELIVERY_POINT_DEST_PED(<<1016.2606, -731.5460, 56.5550>>, <<1011.8010, -733.0652, 56.7435>>, 314.5626, P_BINBAG_01_S )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[12], <<1020.2507, -736.8150, 56.6235>>, TRUE, 15, 2, 220.7804, PARK_TYPE_PERPENDICULAR_NOSE_IN, <<1011.2380, -724.7085, 56.5183>>, <<10,10, 5>>)

	mDeliveryPoint[13] = FILL_DELIVERY_POINT_DEST_PED(<<1164.1262, -762.2866, 56.6656>>, <<1099.3422, -775.4273, 57.3525>>, 177.5729, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[13], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)

	mDeliveryPoint[14] = FILL_DELIVERY_POINT_DEST_PED(<<1199.3394, -678.0995, 59.9051>>, <<1205.4152, -672.1282, 60.1445>>, 105.2374, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[14], vSafeVec, FALSE, 15, 2, 12.1103, PARK_TYPE_PULL_OVER, <<1199.3394, -678.0995, 59.9051>>, <<10,10, 5>>)
 
	mDeliveryPoint[15] = FILL_DELIVERY_POINT_DEST_PED(<<1183.4449, -582.9476, 63.0962>>, <<929.3757, -618.9091, 56.4612>>, 181.1844, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[15], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)

	mDeliveryPoint[16] = FILL_DELIVERY_POINT_DEST_PED(<<1192.4794, -485.1086, 64.7195>>, <<1264.3593, -719.0840, 63.3280>>, 65.0519, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[16], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)

	mDeliveryPoint[17] = FILL_DELIVERY_POINT_DEST_PED(<<1212.6934, -400.0532, 67.0971>>, <<1284.1086, -676.7280, 65.0225>>, 69.9898, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[17], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)
 
	mDeliveryPoint[18] = FILL_DELIVERY_POINT_DEST_PED(<<1114.5735, -232.3706, 68.0949>>, <<929.3757, -618.9091, 56.4612>>, 181.1844, DUMMY_MODEL_FOR_SCRIPT )
	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[18], vSafeVec, FALSE, 15, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)
 
//	mDeliveryPoint[19] = FILL_DELIVERY_POINT_DEST_PED(<<1281.5195, -498.2445, 67.9411>>, <<1264.4473, -499.2488, 68.1254>>, 70.1427, PROP_BIG_BAG_01 )
//	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[19], FALSE, 10, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)
//
//	mDeliveryPoint[20] = FILL_DELIVERY_POINT_DEST_PED(<<1263.4022, -392.0704, 68.0014>>, <<929.3757, -618.9091, 56.4612>>, 181.1844, DUMMY_MODEL_FOR_SCRIPT )
//	FILL_DELIVERY_POINT_PARKING(mDeliveryPoint[20], FALSE, 10, 2, 270, PARK_TYPE_PULL_OVER, <<1149.1356, -981.2310, 45.0495>>, <<10,10, 5>>)
	
	
	iTotalDelPoints = 19
ENDPROC

PROC POPULATE_BATTLE_BUDDY_DIALOGUE()
	bAddBuddyForDialogue[0] = FALSE
	bAddBuddyForDialogue[1] = FALSE
	bAddBuddyForDialogue[2] = FALSE

	mBBChatter[0] = FILL_DIALOGUE("FP1_WANTM","FP1_LOSTM", "FP1_THEREM", "FP1_CHATFM", "", "FP1_CHATTM")
	mBBChatter[1] = FILL_DIALOGUE("FP1_WANTT","FP1_LOSTT", "FP1_THERET", "FP1_CHATFM", "FP1_CHATFT", "")
	mBBChatter[2] = FILL_DIALOGUE("FP1_WANTF","FP1_LOSTF", "FP1_THEREF", "", "FP1_CHATFT", "FP1_CHATTM")

ENDPROC

/// PURPOSE:
///    Initialises all variables and structs
PROC POPULATE_STUFF()
	POPULATE_VEHICLES()
	POPULATE_PEDS()
	CREATE_POLY_CHECKS()
	POPULATE_DELIVERY_POINTS()
	POPULATE_BATTLE_BUDDY_DIALOGUE()
ENDPROC


/// PURPOSE:
///    Sets the Game world time after a replay/shit skip
PROC SET_TIME_FOR_REPLAY()
	IF g_bShitskipAccepted
	ELSE
	ENDIF

ENDPROC

PROC CREATE_RELATIONSHIP_GROUP()
ENDPROC

///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			//DO_SCREEN_FADE_OUT(0)
			#IF IS_DEBUG_BUILD
				IF bShowDebugText
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
				ENDIF
			#ENDIF
			
			
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF
			
			IF SETUP_MISSION_STAGE(eMissionState) 
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "MS_GET_TO_VAN"
					s_skip_menu[1].sTxtLabel = "MS_STEAL"
					s_skip_menu[2].sTxtLabel = "MS_RETURN_WITH_THING"					
				#ENDIF
				
				MARK_PREP_START_CAR_AS_VEH_GEN()
				INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_MISSION_FBI_4_PREP_1)
	//			ADD_RELATIONSHIP_GROUP("MYFRIEND", HASH_FRIENDS)
				
				SET_MAX_WANTED_LEVEL(2)
				SET_WANTED_LEVEL_MULTIPLIER(0.1)
//				#IF IS_DEBUG_BUILD
//					BREAK_ON_NATIVE_COMMAND("ENABLE_DISPATCH_SERVICE", FALSE)
//				#ENDIF
				IF IS_REPLAY_IN_PROGRESS()
					END_REPLAY_SETUP()
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					eState = SS_CLEANUP
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
						SET_PED_POS(PLAYER_PED_ID(), <<1228.6351, -348.4277, 68.0929>>, 86.7244)
						WAIT_FOR_WORLD_TO_LOAD(<<1228.6351, -348.4277, 68.0929>>)
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
					ENDIF
					CREATE_RELATIONSHIP_GROUP()
					IF eMissionState = MS_RETURN_WITH_THING
						eState = SS_INIT
					ELSE
						eState = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC

PROC CREATE_CUSTOM_GPS_BLIP()
	IF NOT DOES_BLIP_EXIST(biBlip)
	    START_GPS_MULTI_ROUTE(HUD_COLOUR_BLUE, TRUE)
//			ADD_POINT_TO_GPS_CUSTOM_ROUTE( GET_ENTITY_COORDS(PLAYER_PED_ID()) )
			ADD_POINT_TO_GPS_CUSTOM_ROUTE( mGarbageTruck.vPos )		//END					
		SET_GPS_MULTI_ROUTE_RENDER(TRUE)
		ADD_SAFE_BLIP_TO_VEHICLE(biBlip, mGarbageTruck.id, TRUE)
	ENDIF

ENDPROC

PROC MONITOR_GARBAGE_TRUCK_ASSETS()
	IF IS_VEHICLE_OK(mGarbageTruck.id)
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mGarbageTruck.id, 100)
		AND NOT bLoadedTrashAssets
			REQUEST_VEHICLE_ASSET(TRASH)
			IF NOT HAS_VEHICLE_ASSET_LOADED(TRASH)
				bLoadedTrashAssets = TRUE
			ENDIF
		ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mGarbageTruck.id, 120)
		AND bLoadedTrashAssets
			REMOVE_VEHICLE_ASSET(TRASH)
			bLoadedTrashAssets = FALSE
		ENDIF
	ENDIF
ENDPROC


PROC DO_JACKED_CONV()
	STRING conv

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			conv = "FBI4_JACKSM"
		BREAK
		
		CASE CHAR_FRANKLIN
			conv = "FBI4_JACKSF"
		BREAK
		
		CASE CHAR_TREVOR
			conv = "FBI4_JACKST"
		BREAK
	ENDSWITCH
	
	bJackedConv = CREATE_CONVERSATION(s_conversation_peds, "FBIPRAU", conv, CONV_PRIORITY_MEDIUM)
	
	IF bJackedConv
		REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
	ENDIF
	
ENDPROC

PROC STEAL()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vDropOffLoc, mGarbageTruck.id)
	MONITOR_GARBAGE_TRUCK_ASSETS()
	UPDATE_ROUTE_MANAGER()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT STEAL_VAN")  #ENDIF
				ADD_SAFE_BLIP_TO_VEHICLE(biBlip, mGarbageTruck.id, TRUE)
				PRINT_OBJ("PRA_GOVAN", bObjectiveShown)
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITOR_BATTLE_BUDDIES()
			IF IS_VEHICLE_OK(mGarbageTruck.id)
				CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct,  mGarbageTruck.id )
			ENDIF

			IF NOT bJackedConv
				IF IS_PED_UNINJURED(mGarbageTruckDriver.id)
				AND IS_PED_BEING_JACKED(mGarbageTruckDriver.id) 
					DO_JACKED_CONV()
				ENDIF

				IF IS_PED_UNINJURED(mGarbageTruckPassenger.id)
				AND IS_PED_BEING_JACKED(mGarbageTruckPassenger.id) 
					DO_JACKED_CONV()
				ENDIF
			ENDIF

			IF IS_VEHICLE_OK(viStealCar)
				IF eStealCarState = ISCM_IN_VEHICLE
				OR eStealCarState = ISCM_IN_ANY_VEHICLE
				OR eStealCarState = ISCM_IS_TOWING_VEHICLE
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP STEAL_VAN")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			NEXT_STAGE()
		BREAK

		CASE SS_SKIPPED
			SAFE_DELETE_PED(mGarbageTruckDriver.id)
			SAFE_DELETE_PED(mGarbageTruckPassenger.id)
			IF IS_VEHICLE_OK(mGarbageTruck.id)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mGarbageTruck.id)
			ENDIF
			SERVICES_TOGGLE(TRUE)
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH
ENDPROC


PROC MONITOR_END_MISSION_VEH()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, 220)
		IF NOT bSpawnedEndMissionVeh
		AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, 100)
			bSpawnedEndMissionVeh = SPAWN_VEHICLE(viLastCar, CAVALCADE2, <<1370.9122, -2060.0549, 50.9983>>, 312.8686)
		ENDIF
	ELIF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, 240)
		IF bSpawnedEndMissionVeh
			SAFE_RELEASE_VEHICLE(viLastCar)
			bSpawnedEndMissionVeh= FALSE
		ENDIF
	ENDIF
ENDPROC



PROC RETURN_THING()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vDropOffLoc, mGarbageTruck.id)
	UPDATE_ROUTE_MANAGER()
	MONITOR_END_MISSION_VEH()
	MONITOR_GARBAGE_TRUCK_ASSETS()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT TAKE_VAN_BACK")  #ENDIF
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_BLIP_LOCATION(biBlip, vDropOffLoc)
					PRINT_OBJ("PRA_TAKBACK", bObjectiveShown)
				ENDIF
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITER_PLAYER_WANTED(biBlip, vDropOffLoc, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			MONITOR_BATTLE_BUDDIES()
			IF RETURN_STOLEN_VEHICLE(viStealCar, vDropOffLoc, biBlip, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVeh)
				
				REPLAY_RECORD_BACK_FOR_TIME(8.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP TAKE_VAN_BACK")  #ENDIF
			RELEASE_BATTLE_BUDDIES()
			SAFE_REMOVE_BLIP(biBlip)
			SAFE_REMOVE_BLIP(biVehicleBlip)
			IF STOP_MY_VEHICLE()
				NEXT_STAGE()
			ENDIF
		BREAK
		
		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(viStealCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viStealCar)
				ELSE
					SAFE_TELEPORT_ENTITY(viStealCar, vDropOffLoc, 146.5443)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_VEHICLE()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vDropOffLoc, mGarbageTruck.id)
	MONITOR_GARBAGE_TRUCK_ASSETS()
	UPDATE_ROUTE_MANAGER()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_RING_LESTER")  #ENDIF
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
					IF IS_VEHICLE_OK(viStealCar)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
							PRINT_OBJ("PRA_LEVVEH", bObjectiveShown)
						ENDIF
					ENDIF
				ENDIF
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITER_PLAYER_WANTED(biBlip, vDropOffLoc, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			IF LEAVE_VEHICLE_STEAL(viStealCar, vDropOffLoc, biBlip, eStealCarState, eCopMonitor, bForceStop, bExpireReturnVeh, iCallTimerDelay, bCallTimer, bObjectiveShown)
				IF NOT IS_FIB4_AVAILABLE_AFTER_PASSED()
					IF UPDATE_PHONE_STATES(ePhoneCallState, sPhoneUpdateString, s_conversation_peds)
						SET_STATS_WATCH_OFF()
						Script_Passed()
					ENDIF
				ELSE
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_UNLOCKED_FROM_PREP, TRUE)
					SET_STATS_WATCH_OFF()
					NEXT_STAGE()
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(mGarbageTruck.id)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mGarbageTruck.id)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mGarbageTruck.id)
				ELSE
					SAFE_TELEPORT_ENTITY(mGarbageTruck.id, vDropOffLoc, 126.0705)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC


FUNC CC_CommID GET_CHAR_END_MISSION_CONV(enumCharacterList ePlayer)
	IF ePlayer = CHAR_FRANKLIN
		RETURN CALL_FIB4_P1_F_DONE
	ENDIF

	RETURN CALL_FIB4_P1_T_DONE
ENDFUNC

FUNC enumCharacterList GET_WHO_TO_PHONE(enumCharacterList ePlayer)
	IF ePlayer = CHAR_FRANKLIN
		RETURN CHAR_MIKE_TREV_CONF
	ENDIF

	RETURN CHAR_MIKE_FRANK_CONF
ENDFUNC

PROC LEAVE_AREA()
	SWITCH eState
		CASE SS_INIT	
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_AREA")  #ENDIF
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE

			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, 270)
				eState = SS_CLEANUP
			ELSE
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRA_LEVAREA")
					PRINT_NOW("PRA_LEVAREA", 60000, 0)
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP LEAVE_AREA")  #ENDIF
			enumCharacterList ePlayer 
			ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
			IF IS_THIS_PRINT_BEING_DISPLAYED("PRA_LEVAREA")
				CLEAR_PRINTS()
			ENDIF

			IF ePlayer = CHAR_MICHAEL
				IF UPDATE_PHONE_STATES(ePhoneCallState, sPhoneUpdateString, s_conversation_peds)
					SET_STATS_WATCH_OFF()
					Script_Passed()
				ENDIF
			ELSE
				REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER(GET_CHAR_END_MISSION_CONV(ePlayer), CALL_FIB4_GO_TIME, CALL_FIB4_GO_TIME, FLOW_CHECK_FBI4_PREP3, CT_END_OF_MISSION, ePlayer, GET_WHO_TO_PHONE(ePlayer), 0, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME)
				SET_STATS_WATCH_OFF()
				Script_Passed()
			ENDIF
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED(FR_NONE)
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
					SWITCH i_debug_jump_stage
						CASE 0 //MS_STEAL
							i_debug_jump_stage++
						BREAK
						
						CASE 1 //MS_RETURN_WITH_THING
							i_debug_jump_stage++
						BREAK

					ENDSWITCH
					
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF

PROC MONITER_STEAL_CAR()
	FLOAT fFailOutofRange = 600.0
	IF NOT IS_VEHICLE_OK(viStealCar)
		IF NOT IS_VEHICLE_OK(viAnyValidTruck) 
		AND NOT IS_VEHICLE_OK(mGarbageTruck.id)
			MISSION_FAILED(FR_WRECKED_STEAL_CAR)
			EXIT
		ENDIF
		
		IF IS_VEHICLE_OK(mGarbageTruck.id)
		AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mGarbageTruck.id, fFailOutofRange)
			MISSION_FAILED(FR_ABAN_CAR)
		ENDIF
	ELSE
		INT iCountOutOfRange = 0 
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viStealCar, fFailOutofRange)
			iCountOutOfRange++
			#IF IS_DEBUG_BUILD SK_PRINT_INT("Player out of range of the steal car out of range count = ", iCountOutOfRange) #ENDIF
		ENDIF
		
		IF IS_VEHICLE_OK(viAnyValidTruck)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viAnyValidTruck, fFailOutofRange)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Player out of range of the the other garbage truck out of range count = ", iCountOutOfRange) #ENDIF
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
//			#IF IS_DEBUG_BUILD SK_PRINT_INT("other garbage truck doesnt exist out of range count = ", iCountOutOfRange) #ENDIF
		ENDIF
		
		IF IS_VEHICLE_OK(mGarbageTruck.id)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mGarbageTruck.id, fFailOutofRange)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Player out of range of the garbage truck out of range count = ", iCountOutOfRange) #ENDIF
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
			#IF IS_DEBUG_BUILD SK_PRINT_INT("garbage truck doesnt exits out of range count = ", iCountOutOfRange) #ENDIF
		ENDIF

		IF iCountOutOfRange >= 3
			MISSION_FAILED(FR_ABAN_CAR)
			EXIT
		ENDIF
		
		
		iCountOutOfRange = 0 
		IF IS_CAR_STUCK(viStealCar)
			#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck  viStealCar = ", iCountOutOfRange) #ENDIF
			iCountOutOfRange++
		ENDIF
		
		IF IS_VEHICLE_OK(viAnyValidTruck)
			IF IS_CAR_STUCK(viAnyValidTruck)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck  viAnyValidTruck = ", iCountOutOfRange) #ENDIF
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
		ENDIF

		IF IS_VEHICLE_OK(mGarbageTruck.id)
			IF IS_CAR_STUCK(mGarbageTruck.id)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck  mGarbageTruck = " , iCountOutOfRange) #ENDIF
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
		ENDIF
		
		IF iCountOutOfRange >= 3
			#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck All ", iCountOutOfRange) #ENDIF
			MISSION_FAILED(FR_STUCK_CAR)
			EXIT
		ENDIF

	ENDIF
ENDPROC


/// PURPOSE:
///    Holds functions that moniter for the player failing
PROC CHECK_FOR_FAIL()
	IF NOT bJumpSkip
	AND eMissionState <> MS_FAILED
		IF eMissionState > MS_SET_UP
			MONITER_STEAL_CAR()
		ENDIF
	ENDIF
ENDPROC

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINTSTRING("FORCE CLEAN UP") PRINTNL()
		sFailReason = NULL
		Mission_Flow_Mission_Force_Cleanup()
        Script_Cleanup()
	ENDIF

	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF
	POPULATE_STUFF()
	
	IF IS_REPLAY_IN_PROGRESS()
		START_REPLAY_SETUP(<<918.8851, -269.7890, 67.2145>>, 68.2149)
	ENDIF
	CLOSE_MASK_SHOP_ON_OTHER_PREP()
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TrT")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			CHECK_FOR_FAIL()
			MONITER_STATS()
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
			SWITCH eMissionState

				CASE MS_SET_UP
					INITMISSION()
				BREAK

//				CASE MS_GET_TO_STEAL_THING
//					GET_TO_STEAL_THING()
//				BREAK

				CASE MS_STEAL
					STEAL()
				BREAK

				CASE MS_RETURN_WITH_THING
					RETURN_THING()
				BREAK
				
				CASE MS_LEAVE_VEHICLE
					LEAVE_VEHICLE()
				BREAK
				
				CASE MS_LEAVE_AREA
					LEAVE_AREA()
				BREAK

				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK
			ENDSWITCH

			IF eMissionState <> MS_FAILED
				IF eMissionState >= MS_SET_UP
				AND NOT bJumpSkip
				ENDIF

				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
