//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║ 				Author: Michael Wadelin				 Date: 					║
//║																				║
//║					Prev:	David Gentles 										║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║ 																			║
//║ 				FBI3 - By the Book											║
//║																				║
//║ 		Trevor tortures Mr. K for information								║
//║ 																			║
//║ 		*	Trevor or Michael drives to the warehouse						║
//║ 		*	Michael drives to fake location									║
//║ 		*	Trevor tortures for real location								║
//║ 		*	Michael drives to real location									║
//║ 		*	Trevor tortures to find out he's Azerbaijani					║
//║ 		*	Michael searches, not enough, asks for more						║
//║ 		*	Trevor tortures to find out he has a beard						║
//║ 		*	Michael searches, not enough, asks for more						║
//║ 		*	Trevor tortures to find out he smokes Redwood cigarettes		║
//║ 		*	Michael shoots the target										║
//║ 		*	Trevor drives the torture victim to the airport					║
//║ 																			║
//║ 		Gameplay elements introduced: 	Torture								║
//║ 		Vehicle Introduced: 						 						║
//║ 		Location: 	Torture warehouse, Chumash								║
//║ 		Notes:  															║
//║ 																			║
//╚═════════════════════════════════════════════════════════════════════════════╝

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//═════════════════════════════════╡ COMMAND HEADERS ╞═══════════════════════════════════
  
using "rage_builtins.sch"
using "globals.sch"

using "commands_misc.sch" 
using "commands_pad.sch" 
using "commands_script.sch"
 
using "commands_graphics.sch" 
using "commands_object.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "selector_public.sch"
using "model_enums.sch"
using "dialogue_public.sch"
using "player_ped_public.sch"
USING "flow_public_core_override.sch"
USING "commands_entity.sch"
using "script_blips.sch"
USING "select_mission_stage.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "script_ped.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "flow_public_GAME.sch"
USING "building_control_public.sch"
USING "asset_management_public.sch"
USING "player_scene_private.sch"
USING "flow_mission_trigger_public.sch"
USING "clearmissionarea.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"

//═════════════════════════════════╡ SOUND ╞═══════════════════════════════════
ENUM SOUND_LIST
	SND_HEART_FLATLINE_LOOP									= 0,
	SND_HEART_BEEP_LOOP,
	SND_HEART_BEEP,
	SND_ELEC_HUM_LOOP,
	SND_ELEC_ZAP_LOOP,
	SND_ELEC_ATTACH_CLIP_LEFT,
	SND_ELEC_ATTACH_CLIP_RIGHT,
	SND_ELEC_DETACH_CLIP_LEFT,
	SND_ELEC_DETACH_CLIP_RIGHT,
	SND_ELEC_SPARK,
	SND_TOOTH_PULL_LOOP,
	SND_TOOTH_PULL_OUT,
	SND_WATER_POUR_LOOP,
	SND_WATER_PICK_UP,
	SND_WRENCH_HIT,
	SND_MENU_HIGHLIGHT_ELEC,
	SND_MENU_HIGHLIGHT_PLIERS,
	SND_MENU_HIGHLIGHT_WATER,
	SND_MENU_HIGHLIGHT_WRENCH,
	
	SND_LIST_SIZE
ENDENUM

//═════════════════════════════════╡ ENUMS ╞═══════════════════════════════════

ENUM MISSION_PED_FLAGS
	mpf_party_coke_m_1,
	mpf_party_coke_m_3,
	mpf_party_coke_m_5,
	mpf_party_coke_f_1,
	
	mpf_party_b_m_1,
	mpf_party_b_m_2,
	mpf_party_b_f_1,
	
	mpf_party_c_m_1,
	mpf_party_c_m_2,
	
	mpf_party_d_m_1,
	mpf_party_d_m_2,
	mpf_party_d_m_3,
	mpf_party_d_f_1,
	
	mpf_decoy_f,
	mpf_decoy_m,
	mpf_target,
	
	mpf_num_peds
ENDENUM

ENUM MISSION_OBJECT_FLAGS
	mof_target_ciggy,
	mof_target_packet,
	mof_decoy_f_ciggy,
	mof_decoy_m_ciggy,
	mof_coffee_table,
	mof_coffee_mike,
	mof_coffee_dave,
	mof_coffee_chair,
	mof_binoculars,
	mof_mike_ciggy,
	mof_turd,
	mof_UV_face_object,
	mof_num_objects
ENDENUM

ENUM SYNC_SCENES
	SCENE_WEAPON_SELECT_IDLE = 0,
	SCENE_WEAPON_SELECT_PICKUP_WRENCH,
	SCENE_WEAPON_SELECT_PICKUP_PLIERS,
	SCENE_WEAPON_SELECT_PICKUP_CLIP0,
	SCENE_WEAPON_SELECT_PICKUP_WATER,
	SCENE_WEAPON_SELECT_PICKUP_SYRINGE,
	
	SCENE_WRENCH_LEFT,
	SCENE_WRENCH_MID,
	SCENE_WRENCH_RIGHT,
	SCENE_WRENCH_IDLE,
	
	SCENE_PLIERS_PULL_HARD,
	SCENE_PLIERS_PULL_WEAK,
	SCENE_PLIERS_ATTACH,
	SCENE_PLIERS_PULL_OUT,
	SCENE_PLIERS_PULL_OUT2,
	SCENE_PLIERS_IDLE,
	SCENE_PLIERS_READY_IN,
	SCENE_PLIERS_READY_LOOP,
	SCENE_PLIERS_READY_OUT,
	
	SCENE_ELEC_IDLE,
	SCENE_ELEC_SHOCK,
	SCENE_ELEC_SPARK,
	SCENE_ELEC_OUTRO,
	SCENE_ELEC_LEFT_GRIP,
	SCENE_ELEC_RIGHT_GRIP,
	SCENE_ELEC_BOTH_GRIP,
	
	SCENE_WATER_FLIP,
	SCENE_WATER_IDLE,
	SCENE_WATER_READY_TO_POUR,
	SCENE_WATER_POUR,
	SCENE_WATER_POUR_TO_READY,
	SCENE_WATER_PRE_OUTRO,
	SCENE_WATER_OUTRO,
	SCENE_WATER_READY,
	
	SCENE_SYRINGE_USE,
	SCENE_SYRINGE_IDLE,
	
	SCENE_RAG_IDLE,
	
	SCENE_EXTRA_MR_K_FIDGET,
	
	SCENE_COFFEE,
	
	SCENE_SNIPE_PRONE,
	SCENE_PARTY_A,
	SCENE_PARTY_B,
	SCENE_PARTY_C,
	SCENE_PARTY_D,
	
	SCENE_CUNTS,
	
	NUMBER_OF_SCENES
ENDENUM

ENUM MISSION_FAIL
	FAIL_INVALID 				= -1,
	FAIL_FORCE_CLEANUP,
	FAIL_GENERIC,
	FAIL_MICHAEL_DEAD,
	FAIL_TREVOR_DEAD,
	FAIL_VICTIM_DEAD,
	FAIL_DAVE_DEAD,
	FAIL_STEVE_DEAD,
	
	FAIL_CUNTS_SPOOKED,
	FAIL_CUNTS_INNOCENT_DEAD,
	
	FAIL_PARTY_SEEN_AT_PARTY,
	FAIL_PARTY_SPOOKED,
	FAIL_PARTY_PED_DEAD_INNOCENT,
	FAIL_PARTY_PED_DEAD_INNOCENT_NO_ID,
	
	FAIL_VEHICLE_DEAD_TREVOR,	// trevors truck is mission critical for taking Mr K to the airport
	FAIL_VEHICLE_STUCK_TREVOR,
	
	FAIL_ABANDONED_DAVE,
	FAIL_ABANDONED_MRK,
	
	FAIL_COPS_WAREHOUSE
ENDENUM
MISSION_FAIL eMissionFail

ENUM MISSION_STAGE
	ST_INIT = -1,
	ST_0_DRIVE_TO_START,
	ST_1_INTRO_CUT,
	ST_2_DRIVE_TO_COORD_FIRST,		
	ST_3_TORTURE_FOR_FIRST,			
	ST_4_DRIVE_TO_COORD_SECOND,		
	ST_5_TORTURE_FOR_SECOND,		
	ST_6_REACT_TO_SECOND,			
	ST_7_TORTURE_FOR_THIRD,		
	ST_8_REACT_TO_THIRD,
	ST_9_TORTURE_FINAL,		
	ST_10_REACT_FINAL,				
	ST_11_DRIVE_TO_AIRPORT,
	ST_12_EXIT_CUT,
	ST_13_PASSED_MISSION,
	NUM_OF_STAGES
ENDENUM

ENUM TORTURE_WEAPONS
	TOR_WATER,
	TOR_RAG,
	TOR_PLIERS,
	TOR_CLIP0,
	TOR_CLIP1,
	TOR_WRENCH,
	TOR_SYRINGE,
	
	NO_OF_TORTURE_WEAPONS
ENDENUM

ENUM TORTURE_CAMERA
	TC_RESET										= 0,
	TC_DONTUSE,
	TC_HOTSWAP0, TC_HOTSWAP1,
	TC_CIRCLE0, TC_CIRCLE1, TC_CIRCLE2, TC_CIRCLE3,
	TC_WRENCH0, TC_WRENCH1, TC_WRENCH2, TC_WRENCH3, TC_WRENCH4, TC_WRENCH5, TC_WRENCH6, TC_WRENCH7, TC_WRENCH8, TC_WRENCH9,
	TC_WPNSEL0, TC_WPNSEL1, TC_WPNSEL2,
	TC_PLIERS0, TC_PLIERS1, TC_PLIERS2, TC_PLIERS3, TC_PLIERS4, TC_PLIERS5, TC_PLIERS6,
	TC_BATTERY0, TC_BATTERY1,
	TC_WATER0, TC_WATER1, TC_WATER2, TC_WATER3, TC_WATER4, TC_WATER5, TC_WATER6,
	TC_SYRINGE0, TC_SYRINGE1,
	TC_ELEC0, TC_ELEC1, TC_ELEC2
ENDENUM

ENUM SCOPE_STATE
	SCOPESTATE_ENTERING_AIM,
	SCOPESTATE_AIMING,
	SCOPESTATE_NOT_AIMING
ENDENUM

ENUM HOUSE_SECTIONS
	HS_HIGH_LEFT									= 0,
	HS_HIGH_RIGHT,
	HS_LOW_LEFT,
	HS_LOW_RIGHT,
	HS_CENTRE,
	HS_NUM_OF_HOUSE_SECTIONS
ENDENUM
VECTOR vHouseSectionCentre[HS_NUM_OF_HOUSE_SECTIONS]

ENUM MR_K_VARIATIONS
	MR_K_FACE_BLOODY,
	MR_K_NIPPLES_BURNED,
	MR_K_LEG_BLOODY,
	MR_K_CROTCH_BLOODY,
	MR_K_TORSO_BLOODY,
	MR_K_WET,
	MR_K_RAG_FACE,
	MR_K_CABLES,
	MR_K_NUM_OF_VARIATIONS
ENDENUM

BOOL bMrKVariations[MR_K_NUM_OF_VARIATIONS]

ENUM CLEANUP_TYPE_FLAG
	CTF_RESET,
	CTF_INSTANT,
	CTF_DEATH_ARREST_DELAYED,
	CTF_MISSION_PASSED
ENDENUM

ENUM TARGET_PED_SLOT
	PEDSLOT_NOT_ASSIGNED = -1,
	PEDSLOT_UPPER_LEFT,
	PEDSLOT_UPPER_CENTER,
	PEDSLOT_UPPER_RIGHT,
	PEDSLOT_LOWER_LEFT,
	PEDSLOT_LOWER_CENTER,
	PEDSLOT_LOWER_RIGHT,
	PEDSLOT_NUM_OF_SLOTS
ENDENUM

ENUM CUTSCENE_SWITCH_ENUM
	CUTSWITCH_FIRST,
	CUTSWITCH_NORMAL,
	CUTSWITCH_FINAL
ENDENUM

ENUM PARTY_CRASHED_STATE
	PCS_NOT_CRASHED,
	PCS_MISSED_SHOT,
	PCS_KILLED_INNOCENT_MALE,
	PCS_KILLED_INNOCENT_FEMALE,
	PCS_TARGET_DOWN
ENDENUM

ENUM EKG_CAM_STATE
	EKGCAM_TORTURE,
	EKGCAM_INTERP_IN,
	EKGCAM_LOOKING_AT,
	EKGCAM_INTERP_OUT
ENDENUM

ENUM LIST_CUNT_PEDS
	CUNTPED_ACTOR_M,
	CUNTPED_ACTOR_F,
	CUNTPED_CREW_CAM_GUY,
	CUNTPED_CREW_SOUND_GUY,
	CUNTPED_CREW_PRODUCER
ENDENUM

ENUM LIST_CUNT_PROPS
	CUNTPROP_CAM,
	CUNTPROP_BOOM,
	CUNTPROP_CLIPBOARD
ENDENUM



//═════════════════════════════════╡ CONSTANTS ╞═══════════════════════════════════

CUTSCENE_SECTION 		cutSections_Mike 				= CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7
CUTSCENE_SECTION 		cutSections_TrevLeadIn 			= CS_SECTION_1 | CS_SECTION_3 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7
CUTSCENE_SECTION 		cutSections_TrevFull 			= CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7

// Bit masks & shifters for storing replay info
CONST_INT	BITMASK_TARGET_SLOT_0		(BIT0|BIT1|BIT2|BIT3|BIT4)
CONST_INT	BITMASK_TARGET_SLOT_1		(BIT5|BIT6|BIT7|BIT8|BIT9)
CONST_INT	BITMASK_TARGET_SLOT_2		(BIT10|BIT11|BIT12|BIT13|BIT14)
CONST_INT	BITSHIFTER_TARGET_SLOT		5

CONST_INT	BITMASK_VICTIMHEALTH		(BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6)
CONST_INT	BITSHIFTER_VICTIMHEALTH		7

CONST_INT	BITMASK_VICTIMVARS			(BIT7|BIT8|BIT9|BIT10|BIT11|BIT12|BIT13|BIT14)
CONST_INT	BITSHIFTER_VICTIMVARS		8

CONST_INT	BITMASK_VICTIMTEETH			(BIT15|BIT16|BIT17|BIT18)
CONST_INT	BITSHIFTER_VICTIMTEETH		4

CONST_INT	BITMASK_SYRINGES			(BIT19|BIT20|BIT21|BIT22)
CONST_INT	BITSHIFTER_SYRINGES			4

//═════════╡ VALUES ╞═════════
CONST_INT		I_MAX_SYRINGE_USES				3

CONST_FLOAT		F_ELECTRO_TIME					6.5		// how long to electrocute the victim for
CONST_FLOAT		F_ELECTRO_DAMAGE 				35.0	// overall damage received from the timed electrocution (for fun will be more)
CONST_FLOAT		F_ELECTRO_DAMAGE_EXTRA 			40.0	// overall damage received from the timed electrocution (for fun will be more)

CONST_INT		I_TOOTH_PULL_ROT_SPEED			360		// degrees per second
CONST_FLOAT		F_TOOTH_PULL_FORCE_LIMIT		5.0		// 5 seconds
CONST_FLOAT		F_TOOTH_PULL_MIN_DURATION		0.1		// 0.2 seconds after starting to rotate the stick does the tooth begin to pull
CONST_FLOAT		F_TOOTH_PULL_DAMAGE_OVERALL		25.0	// overall daamage received from having tooth pulled
CONST_FLOAT		F_TOOTH_PULL_DAMAGE_FINAL		5.0		// final pull damage		
CONST_INT 		I_TOOTH_STUBBORNNESS			3
CONST_FLOAT 	F_PERCENT_TOOTH_HEIGHT			0.6

CONST_FLOAT		F_WRENCH_DAMAGE					25.0

CONST_FLOAT		F_WATERBOARD_TIME				6.5
CONST_FLOAT		F_WATERBOARD_DAMAGE				10.0
CONST_FLOAT		F_WATERBOARD_DAMAGE_EXTRA		40.0

CONST_FLOAT 	fVictimMaxHealth 				100.0

CONST_FLOAT 	fMaxBPM							200.0
CONST_FLOAT 	fMinBPM							80.0

CONST_FLOAT 	fSmokeChange					0.02

CONST_FLOAT		F_ZOOM_SCOPE_CHECK				3.65

VECTOR			TORTURE_LOAD_SCENE_COORD		= <<141.946091,-2201.899658,3.686588>>
CONST_FLOAT 	TORTURE_LOAD_SCENE_RADIUS		10.0

CONST_INT		I_TIME_BETWEEN_PLEES			2000

CONST_FLOAT 	F_SNIPE_CAM_ZOOM_DEFAULT		2.060
CONST_FLOAT 	F_SNIPE_CAM_ZOOM_MIN 			1.464

//═════════╡ COORDINATES AND HEADINGS ╞═════════

VECTOR vTrevTurd									= << 129.34, -2203.33, 5.03 >>

VECTOR vMichaelSpawnCoords							= <<133.7404, -2196.8147, 6.1353>>
CONST_FLOAT fMichaelSpawnHeading					53.750

VECTOR vTrevorTortureCoords							= << 142.8228, -2202.2410, 3.6867 >>
CONST_FLOAT fTrevorTortureHeading					211.8283

VECTOR vMichaelSnipeCoords							= << -2956.2791, 314.3996, 29.6051 >>
CONST_FLOAT fMichaelSnipeHeading					76.1479

VECTOR vDaveSnipeCoords								= << -2954.5432, 310.1234, 29.1099 >>
CONST_FLOAT fDaveSnipeHeading						75.1103

VECTOR vChairCoords									= <<142.99, -2201.98, 4.07>>
VECTOR vChairRot									= <<-0.45, 0.0, 96.61>>

VECTOR vTortureScene								= vChairCoords
VECTOR vTortureRot									= vChairRot + <<0,0,180>>

VECTOR vTableCoords									= <<0,0,0>>
VECTOR vTableRot									= <<0,0,0>>

VECTOR vBatteryCoords								= <<0,0,0>>
VECTOR vBatteryRot									= <<0,0,0>>

VECTOR vSmallTableCoords							= <<143.1199, -2201.3455, 4.350>>
VECTOR vSmallTableRot								= << 0, 0, 0 >>

VECTOR vMonitorCoords								= <<143.1000, -2201.3455, 4.5020>>
VECTOR vMonitorRot									= <<0.000, 0.0, 173.1000>>

// Warehouse vehicle coords
VECTOR vTrevorCarOutsideTortureCoord				= << 120.0400, -2195.4614, 5.0328 >>
CONST_FLOAT fTrevorCarOutsideTortureHeading			309.4985

VECTOR vDaveCarOutsideTortureCoords					= <<128.3181, -2195.5840, 5.0324>>
CONST_FLOAT fDaveCarOutsideTortureHeading			292.8319

VECTOR vOtherVehicleOutsideWarehouse				= <<111.0266, -2189.0339, 4.9691>> //<<126.1588, -2187.4060, 4.9524>>
CONST_FLOAT fOtherVehicleOutsideWarehouse			268.2706 //270.3849

VECTOR vTaxiToStartCoords							= << 169.2886, -2194.2666, 5.0330 >>
CONST_FLOAT fTaxiToStartHeading						90.0

VECTOR v_BlipRockfordHillsHouse 					= <<-876.0392, -13.2516, 41.5182>> //<< -876.2056, -13.9527, 41.4756 >>


// Drive to chumash
VECTOR vDriveTo2Coords								= <<-2965.7070, 359.8771, 13.7698>> //<< -2980.6990, 372.7359, 13.7840 >>

VECTOR vCoffeeShopCarCoord							= << -1263.8647, -892.5212, 10.4036 >>
CONST_FLOAT fCoffeeShopCarHeading					123.590

VECTOR vCoffeeShopMikeCoord							= << -1268.4265, -888.1281, 10.4502 >>
CONST_FLOAT fCoffeeShopMikeHeading					208.924

VECTOR vCoffeeShopDaveCoord							= << -1268.1293, -886.0143, 10.5154 >>
CONST_FLOAT fCoffeeShopDaveHeading					213.1984


// Drive to Airport
VECTOR vDriveToAirport								= << -1033.5138, -2729.6450, 20.1064 >>
CONST_FLOAT fDriveToAirport							235.4340

VECTOR vTrevorCarDriveToAirportCoord				= << 126.8514, -2198.8608, 6.1049 >> //5.0330 >>
CONST_FLOAT fTrevorCarSpawnHeading					311.4299

VECTOR vSteveCoords									= << 141.6254, -2198.8315, 4.6875 >>  //3.688 >>
CONST_FLOAT fSteveHeading							190.2745
//VECTOR vDaveCoords									= << 148.5836, -2203.9780, 3.6016 >>
CONST_FLOAT fDaveHeading							45.5475

VECTOR 	vProneCoords 								= << -2956.042, 315.845, 29.638 >>
VECTOR 	vProneRotation 								= << 3.500, -0.000, 76.000 >>
VECTOR 	vProneCamStartCoord							= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-2955.5596, 316.1508, 30.1988>>, 77.1451, <<-0.075,0.25,0>>)
VECTOR 	vProneCamStartRot							= <<-6.3088, 0.0000, 91.1270>>
FLOAT	fProneCamStartFOV							= 43.0
VECTOR 	vProneCamEndCoord							= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<-2955.5596, 316.1508, 30.1988>>, 77.1451, <<0.075,0.25,0>>)
VECTOR 	vProneCamEndRot								= <<-6.3088, 0.0000, 91.1270>>
FLOAT	fProneCamEndFOV								= 43.0

VECTOR vPartySyncCoord 								= <<-3095.699, 344.770, 13.460>>
VECTOR vPartySyncRot								= <<0,0,75.0>>

VECTOR vCoffeeIGCoord								= << -1267.915, -881.060, 11.727 >>
VECTOR vCoffeeIGRot									= << -0.000, 0.000, -58.200 >>

VECTOR 	v_CuntsSceneCoord							= <<-886.168, -12.005, 42.165>>
VECTOR 	v_CuntsSceneHeading 		 				= <<0,0,85.0>>

VECTOR vNewLoadSceneSnipeCoord						= << -2955.3462, 316.2738, 30.2934 >>
VECTOR vNewLoadSceneSnipeDir						= <<-3173.5278, 285.2572, 11.0148>> - vNewLoadSceneSnipeCoord
FLOAT fNewLoadSceneSnipeFarClip						= 500.0

//═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
//════════════════════════════════════════════════════╡ START ASSET LIST ╞═════════════════════════════════════════════════════
//═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════

MODEL_NAMES mnVictim 			= CS_MRK // IG_MRK
MODEL_NAMES mnTarget 			= U_M_M_PARTYTARGET
MODEL_NAMES mnPartyMale1 		= U_M_Y_PARTY_01
MODEL_NAMES mnPartyFem 			= A_F_Y_BEVHILLS_04

MODEL_NAMES mnCuntActorM 		= G_M_M_ARMBOSS_01
MODEL_NAMES mnCuntActorF 		= A_F_M_BEVHILLS_01
MODEL_NAMES mnCuntCrewM			= A_M_Y_HIPSTER_03
MODEL_NAMES mnCuntCrewF			= A_F_Y_HIPSTER_04
MODEL_NAMES mnCuntCam			= PROP_V_CAM_01
MODEL_NAMES mnCuntBoom			= PROP_V_BMIKE_01
MODEL_NAMES mnCuntClipboard		= P_AMB_CLIPBOARD_01

MODEL_NAMES mnBench 			= P_CS_TROLLEY_01_S
MODEL_NAMES mnSmallTable 		= V_ILEV_TORT_STOOL
MODEL_NAMES mnWrench 			= PROP_CS_WRENCH
MODEL_NAMES mnPliers 			= P_PLIERS_01_S
MODEL_NAMES mnBattery 			= PROP_TORTURE_01
MODEL_NAMES mnSyringe 			= PROP_SYRINGE_01
MODEL_NAMES mnChair 			= PROP_TORTURE_CH_01
MODEL_NAMES mnClip0 			= P_LD_CROCCLIPS01_S //Prop_LD_crocclips01 
MODEL_NAMES mnClip1 			= P_LD_CROCCLIPS02_S //Prop_LD_crocclips02
MODEL_NAMES mnTooth 			= Prop_LD_tooth
MODEL_NAMES mnWater 			= GET_WEAPONTYPE_MODEL(WEAPONTYPE_PETROLCAN)
MODEL_NAMES mnRag				= P_LOOSE_RAG_01_S
MODEL_NAMES mnMonitor 			= PROP_ECG_01
MODEL_NAMES	mnArmTape			= P_ARM_BIND_CUT_S
MODEL_NAMES mnUVFaceProp		= P_WBOARD_CLTH_S //P_UV_WBOARD_CLTH_S

MODEL_NAMES mnTrevTurd			= PROP_BIG_SHIT_01

STRING adPartyGetUpFlee = "missfbi3_party"

STRING adSteve = "MISSFBI3_STEVE_PHONE"
	
STRING adWrongHouse = "MISSFBI3_CamCrew"
	
STRING adSniping = "MISSFBI3_SNIPING"
	STRING animProneMichael = "prone_michael"
	STRING animProneDave = "prone_dave"
	
STRING adCoffee = "switch@michael@coffee_w_dave"

STRING shakeRoadVib = "ROAD_VIBRATION_SHAKE"
STRING shakeHand = "HAND_SHAKE"

STRING sfHeart = "ECG_MONITOR"
STRING sfTooth = "TEETH_PULLING"

STRING csTrevor = "Trevor"
STRING csDave = "Dave_FBI"
STRING csTrevorCar = "Trevors_car"
STRING csTable = "TORTURE_Trolley"
STRING csSmallTable = "Torture_table_for_ECG"
STRING csBattery = "TORTURE_Battery"
STRING csChair = "TORTURE_Chair"
STRING csMonitor = "TORTURE_ECG_Machine"
STRING csWrench = "TORTURE_Wrench"
STRING csPliers = "TORTURE_Pliers"
STRING csClip0 = "TORTURE_Battery_clip_right"
STRING csClip1 = "TORTURE_Battery_clip_left"
STRING csWater = "TORTURE_Jerrycan"
STRING csSyringe = "TORTURE_Syringe"// "PROP_SYRINGE_01"
STRING csSteve = "Steve_FBI"
STRING csMrK = "MR_K"
STRING csMrK_TL = "MRK_toothless"
STRING csTapeArmLeft = "Torture_Binding_Arm"
STRING csTapeArmRight = "Torture_Binding_Arm_2"
STRING csRag = "Torture_Rag"
STRING strMrKCSHandle
STRING strMrKCSHandle_DONT_ANIMATE

STRING godSwitchTrevor = "TFT_HTSWPTREV"
STRING godShootTarget = "TFT_SHOOT"
STRING godDriveToMalibu = "TFT_DRIVE2"
STRING godDriveToAirport = "TFT_DRIVE3"
STRING godMichaelGetIn = "CMN_GENGETIN"
STRING godMichaelGetBack = "CMN_GENGETBCK"

STRING sbTortureGeneral = "FBI_03_TORTURE_General"
STRING sbTortureElec = "FBI_03_TORTURE_Electric"
STRING sbTortureTeeth = "FBI_03_TORTURE_Teeth"
STRING sbTortureWater = "FBI_03_TORTURE_Water"
STRING sbTortureWrench = "FBI_03_TORTURE_Wrench"

STRING soundHeartBeat = "FBI_03_TORTURE_Heart_Monitor_Single"
STRING soundHeartFlatline = "FBI_03_TORTURE_Heart_Monitor_Flatline"


STRING audsceneZoom = "FBI_3_TORTURE_ZOOM"

#IF NOT IS_JAPANESE_BUILD
	STRING eventEarlyCutIn = "EARLYCUT_IN"
	STRING fxBloodExtract = "scr_fbi3_blood_extraction"
	STRING fxBloodMouth = "scr_fbi3_blood_mouth"
	STRING fxBloodThrow = "scr_fbi3_blood_throwaway"
	STRING fxElecSmoulder = "scr_fbi3_elec_smoulder"
	STRING fxElecSparks = "scr_fbi3_elec_sparks"
	STRING fxWaterPour = "scr_fbi3_dirty_water_pour"//"scr_fbi3_water_pouring"

	STRING hlpTorture1 = "hlp_TOR1"
	STRING hlpTorture2 = "hlp_TOR2"
	STRING hlpTorture3 = "hlp_TOR3"
	STRING hlpTorture4 = "hlp_TOR4"
	STRING hlpWrench = "hlp_WRENCH"
	STRING hlpPliers1 = "hlp_PLIER1"
	STRING hlpPliers2 = "hlp_PLIER2"
	STRING hlpBattery = "hlp_BATTERY"
	STRING hlpChair = "hlp_CHAIR"
	STRING hlpPour = "hlp_POUR"
	STRING hlpSyringe = "hlp_SYRINGE"
	STRING hlpMenu = "hlp_MENU"

	STRING soundMenuHighlightElec = "FBI_03_TORTURE_Choose_Electric"
	STRING soundMenuHighlightPliers = "FBI_03_TORTURE_Choose_Pliers"
	STRING soundMenuHighlightWater = "FBI_03_TORTURE_Choose_Water"
	STRING soundMenuHighlightWrench = "FBI_03_TORTURE_Choose_Wrench"

	STRING soundElectricHum = "FBI_03_TORTURE_Electric_Hum"
	STRING soundElectricZap = "FBI_03_TORTURE_Electric_Zap"
	STRING soundElectricSpark = "FBI_03_TORTURE_Electric_Spark"
	STRING soundToothPull = "FBI_03_TORTURE_Teeth_Pulling"
	STRING soundToothPullOut = "FBI_03_TORTURE_Teeth_Tooth_Out"
	STRING soundWaterPour = "FBI_03_TORTURE_Water_Pour_Water"
	STRING soundWaterPickUp = "FBI_03_TORTURE_Water_Pick_Up"
	STRING soundWrenchHit = "FBI_03_TORTURE_Wrench_Hit_Shin"
#ENDIF


//═════════════════════════════════╡ STRUCTS ╞═══════════════════════════════════

STRUCT PARTY_PED_STRUCT
	PED_INDEX	id
	BOOL		bFleeing
ENDSTRUCT

PARTY_PED_STRUCT	peds[mpf_num_peds]
OBJECT_INDEX		objs[mof_num_objects]

STRUCT sMenuObject
	OBJECT_INDEX objObject
	MODEL_NAMES model
	VECTOR vDefaultCoords
	VECTOR vDefaultRot
	BOOL bIsHighlighted
	VECTOR vAttachOffset
	VECTOR vAttachRot
	BOOL bHasPlayedAnim
	STRING strLastPlayedDict
	TEXT_LABEL_63 strLastPlayedAnim
	BOOL bSelectable
	INT iTimesSelected
	STRING strCSName
ENDSTRUCT
sMenuObject menuWeapon[NO_OF_TORTURE_WEAPONS]



STRUCT SWAY_CAM_STRUCT
	CAMERA_INDEX camInit
	CAMERA_INDEX camDest
	BOOL bInitialised
	VECTOR vCamInitCoord
	VECTOR vCamInitRot
	FLOAT fCamInitFOV
	VECTOR vCamDestCoord
	VECTOR vCamDestRot
	FLOAT fCamDestFOV
	INT iSwayDuration
	BOOL bShakeSwayCam
	STRING sShakeType
	FLOAT fShakeAmount
	BOOL bSwayDirection
ENDSTRUCT

SWAY_CAM_STRUCT sSwayTorture
SWAY_CAM_STRUCT sSwaySniping
 
//═════════════════════════════════╡ VARIABLES ╞═══════════════════════════════════

MISSION_STAGE 					eMissionStage
LOCATES_HEADER_DATA 			sLocatesData
ASSET_MANAGEMENT_DATA			sAssetData
CUTSCENE_PED_VARIATION 			sCutscenePedVariationRegister[10]
CLEANUP_QUEUE_ENTITY_STRUCT 	sEntityCleanupQueue[30]
VECTOR 							vTargetPedSlots[PEDSLOT_NUM_OF_SLOTS]
INT 							iTargetPedSlot[3]

//═════════╡ FLAGS ╞═════════
BOOL bStageSetup									= FALSE
BOOL bDoSkip										= FALSE
BOOL bMissionFailed									= FALSE
BOOL bUpdateMissionFailed							= FALSE
#IF IS_DEBUG_BUILD
BOOL b_draw_debug_info								= FALSE
#ENDIF

STRING strOneShotLoaded								= ""
INT i_EstablishHoldTimer


// Start/Drives
BOOL bSwitchedAtStart
BOOL bTrousersUp									= FALSE
TEXT_LABEL_23 		str_InteruptedRoot				= ""
TEXT_LABEL_23 		str_InteruptedLabel				= ""
INT					iDriveStoppedTimer 				= -1
INT 				iDriveComplaintTimer			= 0
BOOL				bMusicCueTriggered				= FALSE
BOOL				bMusicCueKilled					= FALSE

BOOL bHeartbeatFrame								= FALSE
BOOL bFlatLine										= FALSE

BOOL bDisplayHeartMonitor							= FALSE
BOOL bDisplayToothPull								= FALSE

SYNC_SCENES eLastTorturePlayed						= SCENE_WEAPON_SELECT_IDLE

BOOL bIdlingElectricity								= FALSE

BOOL bHasChairPlayedAnim							= FALSE
BOOL bHasPlayedPickupAnim							= FALSE
BOOL bTortureMusicPlaying							= FALSE
BOOL bPerformedPostTortureFadeIn					= FALSE
BOOL bPlayedTortureSceneThisFrame					= FALSE
BOOL bPlayedTortureSceneLastFrame					= FALSE

#IF NOT IS_JAPANESE_BUILD

ANIM_DATA sAnimDataBlank

BOOL bMenuRightPressed								= FALSE
BOOL bMenuLeftPressed								= FALSE
BOOL bInitialWeaponHighlighted						= FALSE
BOOL bRevived										= FALSE
BOOL bJustStarting									= TRUE
BOOL bHasBeenSyringed								= FALSE
INT i_MinPourTimer
BOOL bPlayedPliersInWhimper							= FALSE

FLOAT 	fShockLoopTransition, fShockLoopInc
FLOAT 	fClipGripProgLeft, fClipGripProgRight
FLOAT 	fClipGripIncLeft, fClipGripIncRight
FLOAT 	fLeftWeight, fRightWeight, fIdleWeight, fBothWeight
FLOAT 	fLRBalance, fLRBalanceNorm
BOOL 	bElecgripSpike

INT iPreviousFrameRX								= 0
INT iPreviousFrameRY								= 0
INT iNumberOfSyringesUsed							= 0
INT	iMrKArrestTimer									= -1
FLOAT		fTargetHealth							= 100.0
FLOAT fCurrentSFToothDepth							= 0.0
FLOAT fCurrentSFToothAngle							= 0.0
FLOAT fStartPhase									= 0.0
INT	i_SparkTimer									= 0
INT iTortureStage									= 0
INT iTortureWeaponStage
INT	iWeaponSelDiaTimer								= 0
INT	iWeaponSelDiaDelay								= 0
INT iTimeSinceLastShock								= -1
VECTOR vWaterPourOffset 							= <<0.15, 0.025, 0.2>>
BOOL bHasRestraintPlayedAnim						= FALSE

#ENDIF

EKG_CAM_STATE eEKGCamState							= EKGCAM_TORTURE
BOOL bECGSetup										= FALSE
BOOL bLookingAtECG									= FALSE

CONST_INT NUMBER_OF_EVENT_FLAGS 5
BOOL bEventFlag[NUMBER_OF_EVENT_FLAGS]
BOOL bScoped[HS_NUM_OF_HOUSE_SECTIONS]
BOOL bStartedAsMichael 								= FALSE
BOOL bScopeAiming									= FALSE
INT	 iAttemptToAimTimer								= -1
BOOL bSnipeCamDirInit								= FALSE
BOOL bDriveConversationFlags[3]
INT iNeedVehicleUses								= 0


PARTY_CRASHED_STATE ePartyState						= PCS_NOT_CRASHED
BOOL bManageParty									= FALSE
BOOL bProneSetup									= FALSE
BOOL bPlayingMusic									= FALSE
BOOL bHasCokeSnortPlayed							= FALSE
BOOL bTargetHasBeenIDed								= FALSE
BOOL bTargetKilled									= FALSE

BOOL bDisplaySwitchHUD								= FALSE
BOOL bMadeSelection									= FALSE
BOOL bSafeToSwitch									= FALSE
BOOL bCutsceneLoaded								= FALSE
BOOL bPromptedSWITCH								= FALSE
BOOL bCutsceneSWITCHStarted							= FALSE
BOOL bCutSceneSWITCHPreped							= FALSE
BOOL bHasSwitchSetupCam								= FALSE
BOOL bRunPostSwitchFocusCleanup						= FALSE
BOOL bPlayedSwitchSting								= FALSE
BOOL bMovedFocus									= FALSE
BOOL bMichaelExited									= FALSE
BOOL bDaveExited									= FALSE
BOOL bTrevorExited									= FALSE
BOOL bCamExited										= FALSE
VECTOR v_FirstPersonCamRotPrev						= <<0,0,0>>
BOOL bAimingAtTheTarget								= FALSE
INT iAimTimer										= -1
BOOL bDontPlayFullSnipeChat							= FALSE
INT iCoffeeStage									= 0
#IF IS_DEBUG_BUILD
	BOOL b_SimulatedStreamingStuck						= FALSE
#ENDIF
INT iSwitchFailTimer								= -1

BOOL b_CuntPropPlayingAnim[3]
INT i_realCuntsStage

BOOL b_SpawnTrain


SCENARIO_BLOCKING_INDEX sba_airport
SCENARIO_BLOCKING_INDEX chairBlocker
SCENARIO_BLOCKING_INDEX sba_wrongLocation
SCENARIO_BLOCKING_INDEX sba_chumash_car_park
SCENARIO_BLOCKING_INDEX sba_coffee_shop

INTERIOR_INSTANCE_INDEX interior_torture

INT iCurrentMenuWeapon								= 0
INT iRoadNodeSpeedZone[3]
INT rtECG
STRING sECGRenderTarget								= "ECG"
INT rtScreen
INT iToothAlpha										= 255
INT iWeaponSelStage									= 0
INT	iCokeSnortTimer									= 0
INT	iPartyBStage 									= 0
INT	iPartyDStage 									= 0
INT iGateHashA 										= HASH("WrongLocationGateA")
INT iGateHashB 										= HASH("WrongLocationGateB")
INT iNumberOfTeethPulled							= 0
INT iWrongHouseCut 									= -1
INT	i_time_of_last_convo 							= 0
INT iFailTimer									= -1

FLOAT fTortureForceApplied							= 0
FLOAT 		fVictimHealth 							= 100.0
INT 		iLastHeartBeatTime
FLOAT 		fBPM_Base
FLOAT 		fBPM_Spike
FLOAT 		fBPM_SpikeStart
FLOAT 		fBPM_SpikeDesired
INT 		iBPM_SpikeInTime
INT 		iBPM_SpikeHoldTime
INT 		iBPM_SpikeReturnTime
INT 		iBPM_SpikeTimer
INT 		iUpdateEKGTimer

FLOAT fCurrentSFHealth								= 0.0
FLOAT fCurrentSFBMP									= 0.0
FLOAT fSmokeEvo										= 0.0
FLOAT fAudioZoom									= 0.0
FLOAT fAudioZoomPreviousFrame						= 0.0
FLOAT fPullStrength
BOOL bPliersInMouth
BOOL bPlayerLockedIn 
FLOAT fTortureShake 								= 0.5
BOOL bCustomPCControlsLoaded						= FALSE

BOOL bStoredSniperSnapshot
WEAPON_INFO sSniperSnapshot 

//═════════╡ Stages ╞═════════
INT i_mission_substage									= 0
INT iSelectWeaponStage								= 0


//═════════╡ Selector ╞═════════
SELECTOR_CAM_STRUCT 		sCamDetails
SELECTOR_PED_STRUCT 		sSelectorPeds
structPedsForConversation 	structConvoPeds

//═════════╡ PEDS ╞═════════
PED_INDEX pedVictim
PED_INDEX pedSteve
PED_INDEX pedDave
PED_INDEX pedCunts[5]
PED_INDEX pedProneMichael
PED_INDEX pedAirport[8]

//═════════╡ VEHICLES ╞═════════
VEHICLE_INDEX vehDriveToVeh
VEHICLE_INDEX vehReplay
VEHICLE_INDEX vehTrevor
VEHICLE_INDEX vehTrainStart
VEHICLE_INDEX vehWrongLocationCutscene

//═════════╡ OBJECTS ╞═════════

OBJECT_INDEX objCunts[3]
OBJECT_INDEX propPhoneDave
OBJECT_INDEX propPhoneSteve
OBJECT_INDEX objTable
OBJECT_INDEX objSmallTable
OBJECT_INDEX objBattery
OBJECT_INDEX objChair
OBJECT_INDEX objMonitor
OBJECT_INDEX objTooth
OBJECT_INDEX objTapeArmLeft
OBJECT_INDEX objTapeArmRight

//═════════╡ SEQUENCES ╞═════════
SEQUENCE_INDEX seqSequence

//═════════╡ WEAPONS ╞═════════
WEAPON_TYPE		weapMichaelsSniper					= WEAPONTYPE_SNIPERRIFLE

//═════════╡ PARTICLE ID ╞═════════
#IF NOT IS_JAPANESE_BUILD
PTFX_ID ptfxBloodMouth
BOOL bFXBloodMouth									= FALSE
PTFX_ID ptfxBloodThrow
BOOL bFXBloodThrow									= FALSE
PTFX_ID ptfxElecSmoulder
BOOL bFXElecSmoulder								= FALSE
PTFX_ID ptfxWaterPour
BOOL bFXWaterPour									= FALSE
#ENDIF

//═════════╡ Scene ID ╞═════════
INT syncScene[NUMBER_OF_SCENES]

//═════════╡ Sound ID ╞═════════

//═════════╡ BLIPS ╞═════════
BLIP_INDEX blipLocate

//═════════╡ GROUPS ╞═════════
REL_GROUP_HASH grpCrew

//═════════╡ MODEL NAMES ╞═════════

//═════════╡ SCALEFORM ╞═════════
SCALEFORM_INDEX sfHeartMonitor
SCALEFORM_INDEX sfToothPull

//═════════╡ STRINGS ╞═════════
STRING sAudioBlock = "FB3AUD"
STRING sTextBlock = "FBI3"

//═════════╡ CAMERAS ╞═════════
CAMERA_INDEX	camGeneralInit
CAMERA_INDEX	camGeneralDest
CAMERA_INDEX	camEKG
CAMERA_INDEX	camSyncSceneAnimated
CAMERA_INDEX	camTorture
CAMERA_INDEX	camWrongLocation
CAMERA_INDEX 	cam_establish

//═════════╡ CONVERSATIONS ╞═════════
BOOL bTriggeringConversation
TEXT_LABEL_15 str_dialogue_root
TEXT_LABEL_15 str_dialogue_label

//═════════╡ DEBUG ╞═════════
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 	widgetDebug
	FLOAT				debug_f_cam_rel_heading
	FLOAT				debug_f_cam_rel_pitch
	FLOAT				debug_f_scope_zoom
	FLOAT				debug_f_scope_speed_x
	FLOAT				debug_f_scope_speed_z
	VECTOR 				debug_v_scope_cam_rot
		
	MissionStageMenuTextStruct SkipMenuStruct[22]			// struct containing the debug menu 
	
	INT 				iDebugCounter
#ENDIF



//  ___ ___  _ __ ___  _ __ __    ___  _ __		common functions for David (PRE FAILCHECK)
// / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \ 
//| (_| (_) | | | | | | | | | | | (_) | | | |
// \___\___/|_| |_| |_|_| |_| |_|\___/|_| |_|

VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>

#IF IS_DEBUG_BUILD

PROC DRAW_DEBUG_TEXT_WRAPPED( STRING strValue, INT iColR = 0, INT iColG = 0, INT iColB = 0, INT iColA = 255  )

	CONST_FLOAT f_left 			0.05
	CONST_FLOAT f_top 			0.25
	CONST_FLOAT f_spacing		0.025

	DRAW_DEBUG_TEXT_2D( strValue, <<f_left, f_top + iDebugCounter * f_spacing, 0.0>>, iColR, iColG, iColB, iColA )
	iDebugCounter++

ENDPROC

PROC DRAW_DEBUG_TEXT_WRAPPED_STRING( STRING strName, STRING strValue, INT iColR = 0, INT iColG = 0, INT iColB = 0, INT iColA = 255  )
	TEXT_LABEL_63 strTemp = strName
	strTemp += ": "
	strTemp += strValue
	DRAW_DEBUG_TEXT_WRAPPED( strTemp, iColR, iColG, iColB, iColA )
ENDPROC

PROC DRAW_DEBUG_TEXT_WRAPPED_INT( STRING strName, INT iValue, INT iColR = 0, INT iColG = 0, INT iColB = 0, INT iColA = 255  )
	TEXT_LABEL_63 strTemp = strName
	strTemp += ": "
	strTemp += iValue
	DRAW_DEBUG_TEXT_WRAPPED( strTemp, iColR, iColG, iColB, iColA )
ENDPROC

PROC DRAW_DEBUG_TEXT_WRAPPED_FLOAT( STRING strName, FLOAT fValue, INT iColR = 0, INT iColG = 0, INT iColB = 0, INT iColA = 255  )
	TEXT_LABEL_63 strTemp = strName
	strTemp += ": "
	strTemp += FLOAT_TO_STRING( fValue )
	DRAW_DEBUG_TEXT_WRAPPED( strTemp, iColR, iColG, iColB, iColA )
ENDPROC

PROC DRAW_DEBUG_TEXT_WRAPPED_BOOL( STRING strName, BOOL bValue, INT iColR = 0, INT iColG = 0, INT iColB = 0, INT iColA = 255  )
	TEXT_LABEL_63 strTemp = strName
	strTemp += ": "
	IF bValue
		strTemp += "TRUE"
	ELSE
		strTemp += "FALSE"
	ENDIF
	DRAW_DEBUG_TEXT_WRAPPED( strTemp, iColR, iColG, iColB, iColA )
ENDPROC

#ENDIF

FUNC PED_VARIATION_STRUCT GET_DAVE_VARIATIONS(BOOL bIncludeSunGlasses)

	PED_VARIATION_STRUCT sResult
	
	sResult.iDrawableVariation[PED_COMP_HEAD]		= 0
	sResult.iTextureVariation[PED_COMP_HEAD]		= 0
	sResult.iDrawableVariation[PED_COMP_HAIR]		= 0
	sResult.iTextureVariation[PED_COMP_HAIR]		= 0
	sResult.iDrawableVariation[PED_COMP_BERD]		= 0
	sResult.iTextureVariation[PED_COMP_BERD]		= 0
	sResult.iDrawableVariation[PED_COMP_TORSO]		= 0
	sResult.iTextureVariation[PED_COMP_TORSO]		= 2
	sResult.iDrawableVariation[PED_COMP_LEG]		= 0
	sResult.iTextureVariation[PED_COMP_LEG]			= 3
	sResult.iDrawableVariation[PED_COMP_FEET]		= 0
	sResult.iTextureVariation[PED_COMP_FEET]		= 0
	sResult.iDrawableVariation[PED_COMP_SPECIAL]	= 0
	sResult.iTextureVariation[PED_COMP_SPECIAL]		= 0
	sResult.iDrawableVariation[PED_COMP_SPECIAL2]	= 0
	sResult.iTextureVariation[PED_COMP_SPECIAL2]	= 0
	sResult.iDrawableVariation[PED_COMP_HAND]		= 0
	sResult.iTextureVariation[PED_COMP_HAND]		= 0
	sResult.iDrawableVariation[PED_COMP_DECL]		= 0
	sResult.iTextureVariation[PED_COMP_DECL]		= 0
	sResult.iDrawableVariation[PED_COMP_JBIB]		= 0
	sResult.iTextureVariation[PED_COMP_JBIB]		= 0
	sResult.iPropIndex[ANCHOR_HEAD]					= -1
	sResult.iPropTexture[ANCHOR_HEAD]				= 0
	IF bIncludeSunGlasses
		sResult.iPropIndex[ANCHOR_EYES]				= 0
		sResult.iPropTexture[ANCHOR_EYES]			= 0
	ELSE
		sResult.iPropIndex[ANCHOR_EYES]				= -1
		sResult.iPropTexture[ANCHOR_EYES]			= 0
	ENDIF
	sResult.iPropIndex[ANCHOR_EARS]					= -1
	sResult.iPropTexture[ANCHOR_EARS]				= 0
	sResult.iPropIndex[ANCHOR_HIP]					= -1
	sResult.iPropTexture[ANCHOR_HIP]				= 0
	sResult.iPropIndex[ANCHOR_LEFT_HAND]			= -1
	sResult.iPropTexture[ANCHOR_LEFT_HAND]			= 0
	sResult.iPropIndex[ANCHOR_LEFT_WRIST]			= -1
	sResult.iPropTexture[ANCHOR_LEFT_WRIST]			= 0
	sResult.iPropIndex[ANCHOR_RIGHT_HAND]			= -1
	sResult.iPropTexture[ANCHOR_RIGHT_HAND]			= 0
	sResult.iPropIndex[ANCHOR_RIGHT_WRIST]			= -1
	sResult.iPropTexture[ANCHOR_RIGHT_WRIST]		= 0
	sResult.iPropIndex[ANCHOR_MOUTH]				= -1
	sResult.iPropTexture[ANCHOR_MOUTH]				= 0
	
	
	RETURN sResult
	
ENDFUNC

//PURPOSE: Shutsdown the current pc minigame control scheme.
PROC SHUTDOWN_PC_CONTROLS()

	IF bCustomPCControlsLoaded
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
		bCustomPCControlsLoaded = FALSE
	ENDIF

ENDPROC

//PURPOSE: Sets a custom PC control scheme and unloads the previous scheme if necessary
PROC SET_PC_CONTROLS( STRING sPCControls )

	SHUTDOWN_PC_CONTROLS()
	
	INIT_PC_SCRIPTED_CONTROLS(sPCControls)
	bCustomPCControlsLoaded = TRUE

ENDPROC


//PURPOSE: Warps the player and sets the correct character
FUNC BOOL SET_CURRENT_PLAYER_PED(SELECTOR_SLOTS_ENUM char, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE)

	IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) != char
		
		IF char = SELECTOR_PED_MULTIPLAYER
			SCRIPT_ASSERT("NOT A PLAYABLE CHARACTER!")
		ELSE
			IF bWait
				WHILE NOT SET_CURRENT_SELECTOR_PED(char, bCleanUpModel)
					WAIT(0)
				ENDWHILE
				sSelectorPeds.pedID[char] = PLAYER_PED_ID()
			ELSE
				IF SET_CURRENT_SELECTOR_PED(char, bCleanUpModel)
					sSelectorPeds.pedID[char] = PLAYER_PED_ID()
				ENDIF
			ENDIF
		ENDIF
		
	ELSE 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns the largest of the two integers
FUNC INT MAX_INTEGER(INT int1, INT int2)
	IF int1 > int2
		RETURN int1
	ELSE
		RETURN int2
	ENDIF
ENDFUNC

//PURPOSE: Checks if a world point lies within a given screen area
FUNC BOOL IS_POINT_IN_SCREEN_AREA(vector vWorldCoord, float x1, float y1, float x2, float y2)
	float sX, sY
	GET_SCREEN_COORD_FROM_WORLD_COORD(vWorldCoord,sX,sY)
	if sX>= x1 and sX<=x2
	    if sY>=y1  and sy<=y2
	          return TRUE
	    ENDIF
	ENDIF
	return FALSE
ENDFUNC

PROC SET_GAMEPLAY_CAM_WORLD_HEADING(FLOAT fHeading = 0.0)
    SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading - GET_ENTITY_HEADING(PLAYER_PED_ID()))
ENDPROC

//PURPOSE: DELETES AN ATTACHED PROP
PROC DELETE_ATTACHED_PROP(OBJECT_INDEX &prop)
	IF DOES_ENTITY_EXIST(prop)
		IF IS_ENTITY_ATTACHED(prop)
			DETACH_ENTITY(prop)
		ENDIF
		DELETE_OBJECT(prop)
	ENDIF
ENDPROC

//PURPOSE:	Does all the checks needed to see if a vehicle has rolled over
FUNC BOOL hasVehicleRolledOver(VEHICLE_INDEX thisVehicle)
	IF IS_VEHICLE_DRIVEABLE(thisVehicle)
		IF IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
		//OR IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_JAMMED, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_HUNG_UP, ROOF_TIME)
		OR IS_VEHICLE_STUCK_TIMER_UP(thisVehicle, VEH_STUCK_ON_SIDE, ROOF_TIME)
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC PED_INDEX MIKE_PED_ID()
	IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
		RETURN PLAYER_PED_ID() 
	ELSE
		RETURN sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
	ENDIF
ENDFUNC

FUNC PED_INDEX TREV_PED_ID()
	IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
		RETURN PLAYER_PED_ID() 
	ELSE
		RETURN sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
	ENDIF
ENDFUNC

PROC printLine(STRING sTextLine)
	CDEBUG1LN(DEBUG_MIKE, "[***FBI 3***]: ", sTextLine, "\n")
ENDPROC

//PURPOSE:	Checks if the player has killed the ped
FUNC BOOL HasPedBeenKilledByPlayer(PED_INDEX thisPed)
	IF DOES_ENTITY_EXIST(thisPed)
		IF IS_PED_INJURED(thisPed)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisPed, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:	Checks if the player has shot the ped
FUNC BOOL HasPedBeenShotByPlayer(PED_INDEX thisPed)
	IF DOES_ENTITY_EXIST(thisPed)
		IF NOT IS_PED_INJURED(thisPed)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(thisPed, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Runs a conversation
PROC RunConversation(STRING convRoot, STRING convLabel = NULL, BOOL bAppendToothlessTag = FALSE)
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	TEXT_LABEL_15 strRoot = convRoot
	IF bAppendToothlessTag
	AND iNumberOfTeethPulled > 0
		strRoot += "_TL"
	ENDIF

	bTriggeringConversation = TRUE
	str_dialogue_root 	= strRoot
	IF IS_STRING_NULL_OR_EMPTY(convLabel)
	
		IF CREATE_CONVERSATION(structConvoPeds, sAudioBlock, str_dialogue_root, CONV_PRIORITY_HIGH)
			bTriggeringConversation = FALSE
			str_dialogue_root = ""
		ENDIF
	
	ELSE
	
		str_dialogue_label 	= convLabel 
		IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(structConvoPeds, sAudioBlock, str_dialogue_root, str_dialogue_label, CONV_PRIORITY_HIGH)
			bTriggeringConversation = FALSE
			str_dialogue_root = ""
			str_dialogue_label = ""
		ENDIF
		
	ENDIF
	
ENDPROC

//PURPOSE: Updates the conversation syste,
PROC UpdateConversationStarter()
	IF bTriggeringConversation
	
		IF IS_STRING_NULL_OR_EMPTY(str_dialogue_label)
			IF CREATE_CONVERSATION(structConvoPeds, sAudioBlock, str_dialogue_root, CONV_PRIORITY_HIGH)
				bTriggeringConversation = FALSE
				str_dialogue_root = ""
			ENDIF
		ELSE
			
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(structConvoPeds, sAudioBlock, str_dialogue_root, str_dialogue_label, CONV_PRIORITY_HIGH)
				bTriggeringConversation = FALSE
				str_dialogue_root = ""
				str_dialogue_label = ""
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Checks if the user has failed a level stage
PROC FailCheck()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			ENDIF
		ELSE
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(MIKE_PED_ID())
		IF IS_PED_INJURED(MIKE_PED_ID())
			eMissionFail = FAIL_MICHAEL_DEAD
			bMissionFailed = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(TREV_PED_ID())
		IF IS_PED_INJURED(TREV_PED_ID())
			eMissionFail = FAIL_TREVOR_DEAD
			bMissionFailed = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedVictim)
		IF IS_PED_INJURED(pedVictim)
			eMissionFail = FAIL_VICTIM_DEAD
			bMissionFailed = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedSteve)
		IF IS_PED_INJURED(pedSteve)
			eMissionFail = FAIL_STEVE_DEAD
			bMissionFailed = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedDave)
		IF IS_PED_INJURED(pedDave)
			eMissionFail = FAIL_DAVE_DEAD
			bMissionFailed = TRUE
		ENDIF
	ENDIF
	
	// Got too close to the party, fail
	IF eMissionStage >= ST_4_DRIVE_TO_COORD_SECOND
	AND eMissionStage < ST_11_DRIVE_TO_AIRPORT
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND (IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
		OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
		AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-3091.85, 346.71, 6.51>>) < 105	
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-3114.270752,353.227356,6.238481>>, <<-3082.558350,345.265533,21.124956>>, 23.125000)
			eMissionFail = FAIL_PARTY_SEEN_AT_PARTY
			bMissionFailed = TRUE
		ENDIF
	ENDIF
	
	// trevor's car fucked (only this car is mission critical)
	IF DOES_ENTITY_EXIST(vehTrevor)
		IF eMissionStage > ST_1_INTRO_CUT
			IF NOT IS_VEHICLE_DRIVEABLE(vehTrevor)
				eMissionFail = FAIL_VEHICLE_DEAD_TREVOR
				bMissionFailed = TRUE
			ELSE
				IF hasVehicleRolledOver(vehTrevor)
					eMissionFail = FAIL_VEHICLE_STUCK_TREVOR
					bMissionFailed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH eMissionStage
		CASE ST_0_DRIVE_TO_START
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF IS_COP_PED_IN_AREA_3D(<<126.393562,-2193.830811,13.532487>>-<<21.937500,14.687500,8.687500>>,
					<<126.393562,-2193.830811,13.532487>>+<<21.937500,14.687500,8.687500>>)
					eMissionFail = FAIL_COPS_WAREHOUSE
					bMissionFailed = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ST_2_DRIVE_TO_COORD_FIRST
		
			BOOL bPedsShouldFlee
		
			// Too close OR bullet/projectile hit near them
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-893.271423,-6.694348,42.693260>>) < 10.0
				eMissionFail 	= FAIL_CUNTS_SPOOKED
				bMissionFailed 	= TRUE
//			ELIF HAS_BULLET_IMPACTED_IN_AREA(<<-891.384644,-4.884975,34.978268>>, 35.250000, TRUE)
//			OR IS_PROJECTILE_IN_AREA(<<-885.478577,-0.343218,46.963562>> - <<23.687500,32.937500,10.750000>>, <<-885.478577,-0.343218,46.963562>> + <<23.687500,32.937500,10.750000>>)
//				bPedsShouldFlee 	= TRUE
//				eMissionFail 		= FAIL_CUNTS_SPOOKED
//				bMissionFailed 		= TRUE
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-889.04, -11.79, 42.22>>) < 48.0
			AND ((IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()))
			OR (IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID()) AND IS_VEHICLE_SIREN_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
				bPedsShouldFlee 	= TRUE
				eMissionFail 		= FAIL_CUNTS_SPOOKED
				bMissionFailed 		= TRUE
			ENDIF

			INT i
			FOR i = 0 TO 4
			
				IF DOES_ENTITY_EXIST(pedCunts[i])

					// Killed either of them fail
					IF HasPedBeenKilledByPlayer(pedCunts[i])
						CPRINTLN(DEBUG_MIKE, "CUNTS Dead")
						bPedsShouldFlee = TRUE
						eMissionFail = FAIL_CUNTS_INNOCENT_DEAD
						bMissionFailed = TRUE

					// attacked one of them
					ELIF HasPedBeenShotByPlayer(pedCunts[i])
						CPRINTLN(DEBUG_MIKE, "CUNTS Shot")
						bPedsShouldFlee = TRUE
						eMissionFail = FAIL_CUNTS_SPOOKED
						bMissionFailed = TRUE
					
					// Spooked
					ELIF HAS_PED_RECEIVED_EVENT(pedCunts[i], EVENT_SHOT_FIRED)
					OR HAS_PED_RECEIVED_EVENT(pedCunts[i], EVENT_SHOT_FIRED_BULLET_IMPACT)
					OR HAS_PED_RECEIVED_EVENT(pedCunts[i], EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(pedCunts[i], EVENT_EXPLOSION_HEARD)
					
						CPRINTLN(DEBUG_MIKE, "CUNTS Gunshot, explosion, etc")
						bPedsShouldFlee = TRUE
						eMissionFail = FAIL_CUNTS_SPOOKED
						bMissionFailed = TRUE
					
					ENDIF
				
				ENDIF
			
			ENDFOR
			
			// Peds should fleed
			IF bMissionFailed	AND bPedsShouldFlee

				REPEAT COUNT_OF(pedCunts) i
				
					IF DOES_ENTITY_EXIST(pedCunts[i])
					AND NOT IS_PED_INJURED(pedCunts[i])
					AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedCunts[i], SCRIPT_TASK_SMART_FLEE_PED)
						TASK_SMART_FLEE_PED(pedCunts[i], PLAYER_PED_ID(), 100.0, -1)
					ENDIF

				ENDREPEAT
			
				IF b_CuntPropPlayingAnim[0]	
					STOP_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CLIPBOARD], NORMAL_BLEND_OUT, TRUE)
					ACTIVATE_PHYSICS(objCunts[CUNTPROP_CLIPBOARD])
					b_CuntPropPlayingAnim[0] = FALSE
				ENDIF
				IF b_CuntPropPlayingAnim[1]
					STOP_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CAM], NORMAL_BLEND_OUT, TRUE)
					ACTIVATE_PHYSICS(objCunts[CUNTPROP_CAM])
					b_CuntPropPlayingAnim[1] = FALSE	
				ENDIF
				IF b_CuntPropPlayingAnim[2]
					STOP_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_BOOM], NORMAL_BLEND_OUT, TRUE)
					ACTIVATE_PHYSICS(objCunts[CUNTPROP_BOOM])
					b_CuntPropPlayingAnim[2] = FALSE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(MIKE_PED_ID())
			AND DOES_ENTITY_EXIST(pedDave)
				IF GET_DISTANCE_BETWEEN_ENTITIES(MIKE_PED_ID(), pedDave) >= 150
					eMissionFail = FAIL_ABANDONED_DAVE
					bMissionFailed = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ST_4_DRIVE_TO_COORD_SECOND
			IF DOES_ENTITY_EXIST(MIKE_PED_ID())
			AND DOES_ENTITY_EXIST(pedDave)
				IF GET_DISTANCE_BETWEEN_ENTITIES(MIKE_PED_ID(), pedDave) >= 150
					eMissionFail = FAIL_ABANDONED_DAVE
					bMissionFailed = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE ST_11_DRIVE_TO_AIRPORT
			IF DOES_ENTITY_EXIST(TREV_PED_ID())
			AND DOES_ENTITY_EXIST(pedVictim)
				IF GET_DISTANCE_BETWEEN_ENTITIES(TREV_PED_ID(), pedVictim) >= 150
					eMissionFail = FAIL_ABANDONED_MRK
					bMissionFailed = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DISPLAY_CHECK()		//Run every frame, sand whenever a safeWait is called. Stops the display flickering when a wait is called
	
	IF IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)	
	
		SET_TEXT_RENDER_ID(rtECG)

		IF bDisplayHeartMonitor
		
			fVictimHealth = CLAMP(fVictimHealth, 0, 100)

			DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0,0,0,255)
			
			IF sfHeartMonitor != NULL
			AND HAS_SCALEFORM_MOVIE_LOADED(sfHeartMonitor)
			
				IF NOT bECGSetup
					CDEBUG1LN(DEBUG_MIKE, "ECG: Setting_Color")
					BEGIN_SCALEFORM_MOVIE_METHOD(sfHeartMonitor, "SET_COLOUR")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(235)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
					END_SCALEFORM_MOVIE_METHOD()
					bECGSetup = TRUE
				ENDIF
			
				DRAW_SCALEFORM_MOVIE(sfHeartMonitor, 0.2, 0.3, 0.4, 0.6, 255, 255, 255, 255)
			ELSE
				bECGSetup = FALSE
			ENDIF
			
		ELSE
			DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0,0,0,255)
			
		ENDIF
		
		SET_TEXT_RENDER_ID(rtScreen)
	
	ENDIF
	
	IF bDisplayToothPull
		SET_TEXT_RENDER_ID(rtScreen)
		IF HAS_SCALEFORM_MOVIE_LOADED(sfToothPull)
			DRAW_SCALEFORM_MOVIE(sfToothPull, 0.5, 0.825, 0.2, 0.3, 255, 255, 255, iToothAlpha)
		ENDIF
	ENDIF
	
	IF bDisplaySwitchHUD
		UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE, TRUE)
	ENDIF
ENDPROC


PROC clearText(BOOL bClearConvo = TRUE, BOOL bClearGodText = TRUE, BOOL bClearHelp = TRUE)
	IF bClearConvo
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_ANY_CONVERSATION()
		ENDIF
	ENDIF
	IF bClearGodText
		IF IS_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CLEAR_PRINTS()
		ENDIF
	ENDIF
	IF bClearHelp
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

STRUCT sSound
	INT SoundId				= -1
	BOOL bHasPlayed 		= FALSE
	BOOL bHasPlayedAtAll 	= FALSE
ENDSTRUCT
sSound sndList[SND_LIST_SIZE]

//PURPOSE:	Plays a sound
PROC PlaySound(SOUND_LIST ListName, STRING SoundName, BOOL PlayOnce = TRUE)
	IF NOT sndList[ListName].bHasPlayed
		IF NOT sndList[ListName].bHasPlayedAtAll
			sndList[ListName].SoundId = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND(sndList[ListName].SoundId, SoundName)
		sndList[ListName].bHasPlayedAtAll = TRUE
		IF PlayOnce
			sndList[ListName].bHasPlayed = TRUE
		ENDIF
	ENDIF
ENDPROC


//PURPOSE:	Plays a sound from an entity
PROC PlaySoundFromEntity(SOUND_LIST ListName, STRING SoundName, ENTITY_INDEX EntityIndex, BOOL PlayOnce = TRUE, STRING strSoundSet = NULL)
	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT sndList[ListName].bHasPlayed
			IF NOT sndList[ListName].bHasPlayedAtAll
				sndList[ListName].SoundId = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FROM_ENTITY(sndList[ListName].SoundId, SoundName, EntityIndex, strSoundSet)
			sndList[ListName].bHasPlayedAtAll = TRUE
			IF PlayOnce
				sndList[ListName].bHasPlayed = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Has the sound played at all in the mission
FUNC BOOL HasSoundPlayed(SOUND_LIST ListName)
	IF sndList[ListName].bHasPlayedAtAll
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:	Release the sound and resources
PROC ReleaseSound(SOUND_LIST ListName)
	IF HasSoundPlayed(ListName)
		IF sndList[ListName].SoundId != -1
			IF NOT HAS_SOUND_FINISHED(sndList[ListName].SoundId)
				STOP_SOUND(sndList[ListName].SoundId)
			ENDIF
			RELEASE_SOUND_ID(sndList[ListName].SoundId)
			sndList[ListName].SoundId = -1
		ENDIF
		sndList[ListName].bHasPlayed = FALSE
		sndList[ListName].bHasPlayedAtAll = FALSE
	ENDIF
ENDPROC

PROC LOAD_ONESHOT(STRING strName)
 	
	IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
		// trying to load a different one shot, cancel the prev one
		IF NOT ARE_STRINGS_EQUAL(strName, strOneShotLoaded)
			CANCEL_MUSIC_EVENT(strOneShotLoaded)
			strOneShotLoaded = strName
			PREPARE_MUSIC_EVENT(strOneShotLoaded)
		ELSE
			// already equal, do nothing as we are attempting to load the same sound
		ENDIF
		
	// Nothing loaded, load a new one
	ELSE
		strOneShotLoaded = strName
		PREPARE_MUSIC_EVENT(strOneShotLoaded)
	ENDIF	
	
 ENDPROC
 
 FUNC BOOL PLAY_ONESHOT()
 	
	IF IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
		SCRIPT_ASSERT("Oneshot not requested")
	ELSE
		IF PREPARE_MUSIC_EVENT(strOneShotLoaded)
			TRIGGER_MUSIC_EVENT(strOneShotLoaded)
			CDEBUG1LN(DEBUG_MIKE, "Oneshot: ", strOneShotLoaded, " played")
			strOneShotLoaded = ""
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_MIKE, "Oneshot not loaded")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE:	Safely adds the ped to dialogue, checking if that ped is dead. if so, adds the voice ID as NULL
PROC SAFE_ADD_PED_FOR_DIALOGUE(PED_INDEX thisPed, INT thisNumberID, STRING thisVoiceID)
	IF DOES_ENTITY_EXIST(thisPed)
	AND NOT IS_PED_INJURED(thisPed)
		ADD_PED_FOR_DIALOGUE(structConvoPeds, thisNumberID, thisPed, thisVoiceID)
	ELSE
		REMOVE_PED_FOR_DIALOGUE(structConvoPeds, thisNumberID)
		ADD_PED_FOR_DIALOGUE(structConvoPeds, thisNumberID, NULL, thisVoiceID)
		PRINTLN("Failed to add [", thisVoiceID, "] for dialogue.")
	ENDIF
ENDPROC

//═════════════════════════════════╡ CUTSCENE ╞═══════════════════════════════════

//PURPOSE:		Checks if the cutscene is ready to skip
FUNC BOOL ReadyToSkipCutscene()
	IF TIMERB() > 1000
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:		Shakes both cameras at the same time
PROC doCamShake(CAMERA_INDEX camInit, CAMERA_INDEX camDest, STRING ShakeName, FLOAT fAmplitudeScalar = 1.0)
	IF DOES_CAM_EXIST(camInit)
		SHAKE_CAM(camInit, ShakeName, fAmplitudeScalar)
	ENDIF
	IF DOES_CAM_EXIST(camDest)
		SHAKE_CAM(camDest, ShakeName, fAmplitudeScalar)
	ENDIF
ENDPROC

//PURPOSE:		Changes shake amplitude
PROC setCamShakeAmp(CAMERA_INDEX camInit, CAMERA_INDEX camDest, FLOAT fAmplitudeScalar = 1.0)
	IF DOES_CAM_EXIST(camInit)
	AND IS_CAM_SHAKING(camInit)
		SET_CAM_SHAKE_AMPLITUDE(camInit, fAmplitudeScalar)
	ENDIF
	IF DOES_CAM_EXIST(camDest)
	AND IS_CAM_SHAKING(camDest)
		SET_CAM_SHAKE_AMPLITUDE(camDest, fAmplitudeScalar)
	ENDIF
ENDPROC

//PURPOSE:		Stops both cams shaking
PROC stopCamShake(CAMERA_INDEX camInit, CAMERA_INDEX camDest, BOOL bStopImmediately = FALSE)
	IF DOES_CAM_EXIST(camInit)
		STOP_CAM_SHAKING(camInit, bStopImmediately)
	ENDIF
	IF DOES_CAM_EXIST(camDest)
		STOP_CAM_SHAKING(camDest, bStopImmediately)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration
PROC doTwoPointCam(CAMERA_INDEX &camInit, CAMERA_INDEX &camDest,	VECTOR camInitCoord, VECTOR camInitRot, FLOAT camInitFOV, 
					VECTOR camDestCoord, VECTOR camDestRot, FLOAT camDestFOV,
					INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR, STRING strTwoPointCamName = null)
	IF DOES_CAM_EXIST(camInit)
		DETACH_CAM(camInit)
		STOP_CAM_POINTING(camInit)
	ELSE
		camInit = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		IF NOT IS_STRING_NULL_OR_EMPTY(strTwoPointCamName)
			TEXT_LABEL_23 strDebug = strTwoPointCamName
			strDebug += "_Init"
			SET_CAM_DEBUG_NAME(camInit, strDebug)
		ENDIF
	ENDIF
	SET_CAM_COORD(camInit, camInitCoord)
	SET_CAM_ROT(camInit, camInitRot)
	SET_CAM_FOV(camInit, camInitFOV)
	SET_CAM_ACTIVE(camInit, TRUE)

	IF DOES_CAM_EXIST(camDest)	
		DETACH_CAM(camDest)
		STOP_CAM_POINTING(camDest)
	ELSE
		camDest = CREATE_CAMERA(CAMTYPE_SCRIPTED)
		IF NOT IS_STRING_NULL_OR_EMPTY(strTwoPointCamName)
			TEXT_LABEL_23 strDebug = strTwoPointCamName
			strDebug += "_Dest"
			SET_CAM_DEBUG_NAME(camDest, strDebug)
		ENDIF
	ENDIF
	SET_CAM_COORD(camDest, camDestCoord)
	SET_CAM_ROT(camDest, camDestRot)
	SET_CAM_FOV(camDest, camDestFOV)
	SET_CAM_ACTIVE(camDest, FALSE)

	SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, duration, GraphType, GraphType)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

//PURPOSE: Sets up a camera that sways slowly between two points
PROC setupSwayCam(SWAY_CAM_STRUCT &sCam, VECTOR camInitCoord, VECTOR camInitRot, FLOAT camInitFOV,
					VECTOR camDestCoord, VECTOR camDestRot, FLOAT camDestFOV,
					INT swayDuration, BOOL shakeSwayCam = FALSE, STRING shakeType = NULL, 
					FLOAT shakeAmount = 0.0, STRING strSwayCamName = null)
	IF DOES_CAM_EXIST(sCam.camInit)	
		DESTROY_CAM(sCam.camInit)
	ENDIF
	sCam.camInit	= CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)	
	IF NOT IS_STRING_NULL_OR_EMPTY(strSwayCamName)
		TEXT_LABEL_23 strDebug = strSwayCamName
		strDebug += "_Init"
		SET_CAM_DEBUG_NAME(sCam.camInit, strDebug)
	ENDIF
	SET_CAM_AFFECTS_AIMING(sCam.camInit, FALSE)
	
	IF DOES_CAM_EXIST(sCam.camDest)	
		DESTROY_CAM(sCam.camDest)
	ENDIF
	sCam.camDest	= CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)	
	IF NOT IS_STRING_NULL_OR_EMPTY(strSwayCamName)
		TEXT_LABEL_23 strDebug = strSwayCamName
		strDebug += "_Dest"
		SET_CAM_DEBUG_NAME(sCam.camDest, strDebug)
	ENDIF
	SET_CAM_AFFECTS_AIMING(sCam.camDest, FALSE)
					
	sCam.vCamInitCoord = camInitCoord
	sCam.vCamInitRot = camInitRot
	sCam.fCamInitFOV = camInitFov
	sCam.vCamDestCoord = camDestCoord
	sCam.vCamDestRot = camDestRot
	sCam.fCamDestFOV = camDestFOV
	sCam.iSwayDuration = swayDuration
	sCam.bShakeSwayCam = shakeSwayCam
	sCam.sShakeType = shakeType
	sCam.fShakeAmount = shakeAmount
	sCam.bSwayDirection = FALSE
	
	doTwoPointCam(sCam.camInit, sCam.camDest,
					sCam.vCamInitCoord, sCam.vCamInitRot, sCam.fCamInitFOV,
					sCam.vCamDestCoord, sCam.vCamDestRot, sCam.fCamDestFOV,
					sCam.iSwayDuration/2, GRAPH_TYPE_SIN_ACCEL_DECEL)
	sCam.bSwayDirection = TRUE
	IF sCam.bShakeSwayCam
		doCamShake(sCam.camInit, sCam.camDest, shakeType, shakeAmount)
	ENDIF
	SETTIMERA(0)
ENDPROC

//PURPOSE: Updates the swaying camera
PROC doSwayCam(SWAY_CAM_STRUCT &sCam)
	
	IF TIMERA() >= sCam.iSwayDuration/2
	
		IF NOT DOES_CAM_EXIST(sCam.camInit)
			sCam.camInit = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sCam.vCamInitCoord, sCam.vCamInitRot, sCam.fCamInitFOV, TRUE)				
		ENDIF
		
		IF NOT IS_CAM_SHAKING(sCam.camInit)
			SHAKE_CAM(sCam.camInit, sCam.sShakeType, sCam.fShakeAmount)
		ENDIF
		
		IF NOT DOES_CAM_EXIST(sCam.camDest)
			sCam.camDest = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sCam.vCamDestCoord, sCam.vCamDestRot, sCam.fCamDestFOV, TRUE)
		ENDIF
		
		IF NOT IS_CAM_SHAKING(sCam.camDest)
			SHAKE_CAM(sCam.camDest, sCam.sShakeType, sCam.fShakeAmount)
		ENDIF
	
		IF sCam.bSwayDirection = FALSE
			SET_CAM_ACTIVE(sCam.camInit, TRUE)
			SET_CAM_ACTIVE_WITH_INTERP(sCam.camDest, sCam.camInit, sCam.iSwayDuration/2, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
		ELSE
			SET_CAM_ACTIVE(sCam.camDest, TRUE)
			SET_CAM_ACTIVE_WITH_INTERP(sCam.camInit, sCam.camDest, sCam.iSwayDuration/2, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
		ENDIF

		sCam.bSwayDirection = !sCam.bSwayDirection
		SETTIMERA(0)
	ENDIF

ENDPROC

//═════════════════════════════════╡ Sync Scene ╞═══════════════════════════════════
//PURPOSE: Returns true if the synchronized scene has a phase less than 1
FUNC BOOL IS_SYNCHRONIZED_SCENE_PLAYING(INT sceneID)
	IF sceneID != -1
	AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
		IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) < 1.0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns true if the synchronized scene has a phase great or equal to 1
FUNC BOOL HAS_SYNCHRONIZED_SCENE_FINISHED(INT sceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
		IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 1.0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Set the scene ID to -1. If the entities involved in the scene have been destroyed, the scene is deleted
PROC DESTROY_SYNCHRONIZED_SCENE(INT &sceneID)
	sceneID = -1
ENDPROC

//═════════════════════════════════╡ SAFE FADE ╞═══════════════════════════════════

//PURPOSE:		Only fades out if the screen is faded in
PROC SAFE_FADE_OUT(INT iTime = DEFAULT_FADE_TIME)
	IF IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
ENDPROC

//PURPOSE:		Only fades in if the screen is faded out
PROC SAFE_FADE_IN(INT iTime = DEFAULT_FADE_TIME)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
ENDPROC

//═════════════════════════════════╡ TIDY UP ╞═══════════════════════════════════
//PURPOSE: Either deletes or sets the ped as no longer needed based on bImmediately
PROC TidyUpPed(PED_INDEX &pedTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(pedTidy)
	AND pedTidy != PLAYER_PED_ID()
		IF bImmediately
			IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTidy)
				SET_ENTITY_AS_MISSION_ENTITY(pedTidy)
			ENDIF
			//DELETE_PED(pedTidy)
			Add_Ped_To_Cleanup_Queue(sEntityCleanupQueue, pedTidy)
		ELSE
			IF NOT IS_PED_INJURED(pedTidy)
//				CLEAR_PED_TASKS(pedTidy)
				SET_PED_KEEP_TASK(pedTidy, TRUE)
			ENDIF
			
			SET_PED_AS_NO_LONGER_NEEDED(pedTidy)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Either deletes or sets the vehicle as no longer needed based on bImmediately
PROC TidyUpVehicle(VEHICLE_INDEX &vehTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(vehTidy)
	
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehTidy)
			SET_ENTITY_AS_MISSION_ENTITY(vehTidy, TRUE, TRUE)
		ENDIF
	
		IF bImmediately
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTidy)
			//DELETE_VEHICLE(vehTidy)
			Add_Vehicle_To_Cleanup_Queue(sEntityCleanupQueue, vehTidy)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTidy)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Either deletes or sets the object as no longer needed based on bImmediately
PROC TidyUpObject(OBJECT_INDEX &objTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(objTidy)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(objTidy)
			SET_ENTITY_AS_MISSION_ENTITY(objTidy, TRUE, TRUE)
		ENDIF
		IF bImmediately
			IF IS_ENTITY_ATTACHED(objTidy)
				DETACH_ENTITY(objTidy, FALSE)
			ENDIF
			//DELETE_OBJECT(objTidy)
			Add_Object_To_Cleanup_Queue(sEntityCleanupQueue, objTidy)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(objTidy)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Registers all the peds in the mission with the dialogue file. Handles if peds are dead or alive etc.
PROC ADD_ALL_PEDS_TO_DIALOGUE()
	SAFE_ADD_PED_FOR_DIALOGUE(MIKE_PED_ID(), 0, "MICHAEL")
	SAFE_ADD_PED_FOR_DIALOGUE(TREV_PED_ID(), 2, "TREVOR")
	SAFE_ADD_PED_FOR_DIALOGUE(pedSteve, 3, "STEVE")
	SAFE_ADD_PED_FOR_DIALOGUE(pedDave, 4, "DAVE")
	SAFE_ADD_PED_FOR_DIALOGUE(pedVictim, 5, "MisterK")
ENDPROC

//PURPOSE: Adds the ped to the crew group, and applies all the ped attributes
PROC SET_PED_CREW(PED_INDEX thisPed)
	IF DOES_ENTITY_EXIST(thisPed)
		IF NOT IS_PED_INJURED(thisPed)
			SET_PED_CAN_BE_DRAGGED_OUT(thisPed, FALSE)
			SET_PED_CAN_BE_TARGETTED(thisPed, FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(thisPed, FALSE)
			//SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(thisPed, FALSE, RELGROUPHASH_PLAYER)
			IF thisPed != PLAYER_PED_ID()
				SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, grpCrew)
			ENDIF
			SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_CAN_INVESTIGATE, FALSE) 
			SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_PLAY_REACTION_ANIMS, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
			SET_PED_ALERTNESS(thisPed, AS_NOT_ALERT)
			SET_PED_COMBAT_ABILITY(thisPed, CAL_POOR)
			SET_PED_COMBAT_RANGE(thisPed, CR_NEAR)
			SET_PED_GENERATES_DEAD_BODY_EVENTS(thisPed, FALSE)
			SET_PED_SEEING_RANGE(thisPed, 0)
			SET_PED_HEARING_RANGE(thisPed, 0)
		ENDIF
	ENDIF		
ENDPROC

//PURPOSE: Returns true if the scene is currently being played and the phase is inbetween the two limits
FUNC BOOL IS_SCENE_AT_PHASE(SYNC_SCENES thisScene, FLOAT fPhaseStart, FLOAT fPhaseEnd)
	IF syncScene[thisScene] != -1
	AND IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[thisScene])
		FLOAT fPhase
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[thisScene])
		IF fPhase > fPhaseStart AND fPhase < fPhaseEnd
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//═════════════════════════════════╡ CUTSCENE ╞═══════════════════════════════════

//PURPOSE: Sets Mr K back to his original look
PROC RESET_MR_K_VARIATIONS()
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 0, 1)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TORSO, 1, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL, 2, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_DECL, 0, 0)
	bMrKVariations[MR_K_CABLES] = TRUE
ENDPROC 

//PURPOSE: Only use this when the cutscene is going to create the peds for you. Otherwise use Register them the normal way.
PROC Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(BOOL bIncludeDave, BOOL bIncludeDavesGlasses, STRING strMrKHandle, BOOL bOnlyMrK = FALSE)

	// NORMAL MR K
	IF iNumberOfTeethPulled = 0
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_TEETH, 0, 0, CS_MRK)
		
	// TOOTHLESS VERSION MR K
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_TEETH, 1, iNumberOfTeethPulled, CS_MRK)
	ENDIF

	// Mr K
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_TORSO, 1, 0, CS_MRK)
	Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_SPECIAL, 0, 0, CS_MRK)

	IF bMrKVariations[MR_K_FACE_BLOODY]
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_HEAD, 0, 2, CS_MRK)
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_HEAD, 0, 1, CS_MRK)
	ENDIF
	IF bMrKVariations[MR_K_LEG_BLOODY]
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_FEET, 1, 0, CS_MRK)
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_FEET, 0, 0, CS_MRK)
	ENDIF
	IF bMrKVariations[MR_K_CROTCH_BLOODY]
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_JBIB, 1, 0, CS_MRK)
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_JBIB, 0, 0, CS_MRK)
	ENDIF
	IF bMrKVariations[MR_K_TORSO_BLOODY]
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_BERD, 1, 0, CS_MRK)
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_BERD, 0, 0, CS_MRK)
	ENDIF
	IF bMrKVariations[MR_K_NIPPLES_BURNED]
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_DECL, 1, 0, CS_MRK)
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_DECL, 0, 0, CS_MRK)
	ENDIF
	
	IF bMrKVariations[MR_K_WET]
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_SPECIAL2, 1, 0, CS_MRK)
	ELSE
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKHandle, PED_COMP_SPECIAL2, 0, 0, CS_MRK)
	ENDIF
	
	IF NOT bOnlyMrK

		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, csSteve, PED_COMP_TORSO, 0, 1)
		Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, csSteve, PED_COMP_LEG, 0, 1)

		// Dave
		IF bIncludeDave

			PED_COMPONENT comp
			PED_PROP_POSITION prop
			PED_VARIATION_STRUCT sDave = GET_DAVE_VARIATIONS(bIncludeDavesGlasses)
			
			REPEAT NUM_PED_COMPONENTS comp
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, csDave, comp, sDave.iDrawableVariation[comp], sDave.iTextureVariation[comp])
			ENDREPEAT
			
			REPEAT COUNT_OF(PED_PROP_POSITION) prop
				Register_Ped_Prop_For_Cutscene(sCutscenePedVariationRegister, csDave, prop, sDave.iPropIndex[prop], sDave.iPropTexture[prop])
			ENDREPEAT
		
		ENDIF
	
	ENDIF
ENDPROC

//PURPOSE: Changes Mr K based on what is happening to him
PROC MANAGE_MR_K_VARIATIONS()

	IF NOT bMrKVariations[MR_K_WET]
		IF IS_SCENE_AT_PHASE(SCENE_WATER_POUR, 0.339, 1.000)
			bMrKVariations[MR_K_WET] = TRUE
		ENDIF
	ENDIF
	IF bMrKVariations[MR_K_FACE_BLOODY]
		IF IS_SCENE_AT_PHASE(SCENE_WATER_POUR, 0.100, 1.000)
			bMrKVariations[MR_K_FACE_BLOODY] = FALSE
		ENDIF
	ENDIF
		
	IF IS_SCENE_AT_PHASE(SCENE_WRENCH_RIGHT, 0.245, 0.268)
		IF NOT IS_CAM_SHAKING(camTorture)
			SHAKE_CAM(camTorture, "JOLT_SHAKE", 1.5)
		ENDIF
		
		IF NOT bMrKVariations[MR_K_LEG_BLOODY]
			bMrKVariations[MR_K_LEG_BLOODY] = TRUE
		ENDIF
	ENDIF
	IF IS_SCENE_AT_PHASE(SCENE_WRENCH_MID, 0.186, 0.217)
		IF NOT IS_CAM_SHAKING(camTorture)
			SHAKE_CAM(camTorture, "JOLT_SHAKE", 1.5)
		ENDIF
		
		IF NOT bMrKVariations[MR_K_CROTCH_BLOODY]
			bMrKVariations[MR_K_CROTCH_BLOODY] = TRUE
		ENDIF
	ENDIF
	
	IF IS_SCENE_AT_PHASE(SCENE_WRENCH_LEFT, 0.395, 0.435)
		IF NOT IS_CAM_SHAKING(camTorture)
			SHAKE_CAM(camTorture, "JOLT_SHAKE", 1.5)
		ENDIF
		
		IF NOT bMrKVariations[MR_K_TORSO_BLOODY]
			bMrKVariations[MR_K_TORSO_BLOODY] = TRUE
		ENDIF
	ENDIF
	IF NOT bMrKVariations[MR_K_NIPPLES_BURNED]
		IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_OUTRO])
		OR fSmokeEvo > 0.2
			bMrKVariations[MR_K_NIPPLES_BURNED] = TRUE
		ENDIF
	ENDIF
	
	IF NOT bMrKVariations[MR_K_FACE_BLOODY]
		IF IS_SCENE_AT_PHASE(SCENE_PLIERS_PULL_OUT, 0.064, 0.087)
		OR IS_SCENE_AT_PHASE(SCENE_PLIERS_PULL_OUT2, 0.06, 0.082)			
			bMrKVariations[MR_K_FACE_BLOODY] = TRUE
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(pedVictim)
	AND NOT IS_PED_INJURED(pedVictim)	
	
		//CPRINTLN(DEBUG_MIKE, "**** FBI3: Mr K is alive")
		
		PED_VARIATION_STRUCT sVictimVariation
		GET_PED_VARIATIONS(pedVictim, sVictimVariation)
		
		IF bMrKVariations[MR_K_CABLES]
			IF IS_CUTSCENE_ACTIVE()
				SET_PED_PRELOAD_VARIATION_DATA(pedVictim, PED_COMP_SPECIAL, 2, 0)
			ELSE
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedVictim)
					SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL, 2, 0)
					RELEASE_PED_PRELOAD_VARIATION_DATA(pedVictim)
				ENDIF
			ENDIF
		ENDIF
		
		IF bMrKVariations[MR_K_RAG_FACE]
			IF sVictimVariation.iDrawableVariation[PED_COMP_TEETH] != 2
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TEETH, 2, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: RagFace set")
			ENDIF
		ELSE
			IF iNumberOfTeethPulled = 0
				IF sVictimVariation.iDrawableVariation[PED_COMP_TEETH] != 0
					SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TEETH, 0, 0)
					CPRINTLN(DEBUG_MIKE, "**** FBI3: No teeth pulled set")
				ENDIF
			ELIF sVictimVariation.iDrawableVariation[PED_COMP_TEETH] != 1
			OR sVictimVariation.iTextureVariation[PED_COMP_TEETH] != iNumberOfTeethPulled
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TEETH, 1, iNumberOfTeethPulled)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: ", iNumberOfTeethPulled, " teeth pulled out set")
			ENDIF
		ENDIF
	
		IF bMrKVariations[MR_K_FACE_BLOODY]
			IF sVictimVariation.iDrawableVariation[PED_COMP_HEAD] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_HEAD] != 2
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 0, 2)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody face set")
			ENDIF
		ELSE
			IF sVictimVariation.iDrawableVariation[PED_COMP_HEAD] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_HEAD] != 1
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 0, 1)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody face removed")
			ENDIF
		ENDIF
		IF bMrKVariations[MR_K_LEG_BLOODY]
			IF sVictimVariation.iDrawableVariation[PED_COMP_FEET] != 1
			OR sVictimVariation.iTextureVariation[PED_COMP_FEET] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_FEET, 1, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody leg set")
			ENDIF
		ELSE
			IF sVictimVariation.iDrawableVariation[PED_COMP_FEET] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_FEET] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_FEET, 0, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody leg removed")
			ENDIF
		ENDIF
		IF bMrKVariations[MR_K_CROTCH_BLOODY]
			IF sVictimVariation.iDrawableVariation[PED_COMP_JBIB] != 1
			OR sVictimVariation.iTextureVariation[PED_COMP_JBIB] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_JBIB, 1, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody crotch set")
			ENDIF
		ELSE
			IF sVictimVariation.iDrawableVariation[PED_COMP_JBIB] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_JBIB] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_JBIB, 0, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody crotch removed")
			ENDIF
		ENDIF
		IF bMrKVariations[MR_K_TORSO_BLOODY]
			IF sVictimVariation.iDrawableVariation[PED_COMP_BERD] != 1
			OR sVictimVariation.iTextureVariation[PED_COMP_BERD] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_BERD, 1, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody torso set")
			ENDIF
		ELSE
			IF sVictimVariation.iDrawableVariation[PED_COMP_BERD] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_BERD] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_BERD, 0, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Bloody torso removed")
			ENDIF
		ENDIF
		IF bMrKVariations[MR_K_NIPPLES_BURNED]
			IF sVictimVariation.iDrawableVariation[PED_COMP_DECL] != 1
			OR sVictimVariation.iTextureVariation[PED_COMP_DECL] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_DECL, 1, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Nipples burnt set")
			ENDIF
		ELSE
			IF sVictimVariation.iDrawableVariation[PED_COMP_DECL] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_DECL] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_DECL, 0, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Nipples burnt removed")
			ENDIF
		ENDIF
		
		IF bMrKVariations[MR_K_WET]
			IF sVictimVariation.iDrawableVariation[PED_COMP_SPECIAL2] != 1
			OR sVictimVariation.iTextureVariation[PED_COMP_SPECIAL2] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL2, 1, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Wet face set")
			ENDIF
		ELSE
			IF sVictimVariation.iDrawableVariation[PED_COMP_SPECIAL2] != 0
			OR sVictimVariation.iTextureVariation[PED_COMP_SPECIAL2] != 0
				SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL2, 0, 0)
				CPRINTLN(DEBUG_MIKE, "**** FBI3: Wet face removed")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRIVE_HandleDriveConvo_MikeWithDave(STRING strMainConvo, VECTOR vDriveEndLocation, FLOAT fDistanceCutOff)

	// Don't start the handling of Daves conversation until Dave has gotten in the vehicle
	IF NOT bDriveConversationFlags[0]
		IF IS_PED_SITTING_IN_ANY_VEHICLE(pedDave)
			iDriveStoppedTimer 			= GET_GAME_TIMER() + 5000
			iDriveComplaintTimer		= -1
			str_InteruptedRoot			= strMainConvo
			bDriveConversationFlags[0] 	= TRUE
			
			PRINTLN( "*** FBI 3 *** - ", "DAVE ENTERED CAR, SETTING INITIAL CONVO SETTING")
		ENDIF
	ELSE

		// No stored conversation
		IF IS_STRING_NULL_OR_EMPTY(str_InteruptedRoot)
		AND IS_STRING_NULL_OR_EMPTY(str_InteruptedLabel)
		
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			OR (NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(pedDave))
			OR NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			OR (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IS_VEHICLE_ALMOST_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
				IF iDriveStoppedTimer = -1
					iDriveStoppedTimer = GET_GAME_TIMER() + 2000
				ENDIF
			ELSE
				iDriveStoppedTimer = -1
			ENDIF
		
			// Stop the current conversation if there is one on going
			IF iDriveStoppedTimer != -1 
			AND GET_GAME_TIMER() > iDriveStoppedTimer
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					str_InteruptedRoot 		= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					str_InteruptedLabel 	= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					str_InteruptedRoot		= "NOCONVO"
					str_InteruptedLabel		= "NOCONVO"
				ENDIF
				IF NOT bDriveConversationFlags[1]
					iDriveComplaintTimer			= -1
					bDriveConversationFlags[2] 		= FALSE
				ELSE
					iDriveComplaintTimer			= -2
				ENDIF
			ENDIF
			
		// There is a conversation stored
		ELSE
		
			// check if we can resume the stored conversation
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
			AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PED_SITTING_IN_ANY_VEHICLE(pedDave)
			AND NOT IS_VEHICLE_ALMOST_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))

				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					// start the drive to convo
					IF NOT bDriveConversationFlags[1]
					AND NOT ARE_STRINGS_EQUAL(str_InteruptedRoot, "NOCONVO")
						IF NOT bDriveConversationFlags[2]
							iDriveStoppedTimer 				= -1
							bDriveConversationFlags[2] 		= TRUE
						ENDIF
						IF iDriveStoppedTimer = -1
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF IS_SAFE_TO_START_CONVERSATION()
									IF IS_PED_SITTING_IN_ANY_VEHICLE(MIKE_PED_ID())
									AND IS_PED_SITTING_IN_VEHICLE(pedDave, GET_VEHICLE_PED_IS_IN(MIKE_PED_ID()))
										iDriveStoppedTimer = GET_GAME_TIMER() + 5000
									ENDIF
								ENDIF
							ENDIF
							
						ELIF GET_GAME_TIMER() > iDriveStoppedTimer

							RunConversation(str_InteruptedRoot)
							str_InteruptedRoot 			= ""
							str_InteruptedLabel 		= ""
							bDriveConversationFlags[1] 	= TRUE
							iDriveStoppedTimer 			= GET_GAME_TIMER()
							
						ENDIF
				
					// There is a conversation to resume
					ELIF NOT IS_STRING_NULL_OR_EMPTY(str_InteruptedRoot)
					AND NOT IS_STRING_NULL_OR_EMPTY(str_InteruptedLabel)
					AND NOT ARE_STRINGS_EQUAL(str_InteruptedRoot, "NOCONVO")
					AND NOT ARE_STRINGS_EQUAL(str_InteruptedLabel, "NOCONVO")
					
						IF iDriveStoppedTimer = -1 
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF IS_SAFE_TO_START_CONVERSATION()
									IF IS_PED_SITTING_IN_ANY_VEHICLE(MIKE_PED_ID())
									AND IS_PED_SITTING_IN_VEHICLE(pedDave, GET_VEHICLE_PED_IS_IN(MIKE_PED_ID()))
										iDriveStoppedTimer = GET_GAME_TIMER() + 2000
									ENDIF
								ENDIF
							ENDIF
							
						ELIF GET_GAME_TIMER() > iDriveStoppedTimer
					
							RunConversation(str_InteruptedRoot, str_InteruptedLabel)
							str_InteruptedRoot 	= ""
							str_InteruptedLabel = ""
						
						ENDIF
						
					// Theres no covno to resume
					ELSE
						str_InteruptedRoot 	= ""
						str_InteruptedLabel = ""
					ENDIF
				ENDIF
			
			// Can't resume it, check if we can do a complaint from dave and which one to do
			ELSE
				bDriveConversationFlags[2] = FALSE
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					PRINTLN( "*** FBI 3 *** - ", "SETTING COMPLAINT DELAY")
				
					IF iDriveComplaintTimer = -2
						iDriveComplaintTimer = GET_GAME_TIMER() + 1000
						PRINTLN( "*** FBI 3 *** - ", "SETTING COMPLAINT DELAY TO 1 SECOND")
						
					ELIF iDriveComplaintTimer = -1
						iDriveComplaintTimer = GET_GAME_TIMER() + 20000
						PRINTLN( "*** FBI 3 *** - ", "SETTING COMPLAINT DELAY TO 20 SECONDS")
					ENDIF
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				
					IF iDriveComplaintTimer >= 0
					AND GET_GAME_TIMER() > iDriveComplaintTimer
				
					// wanted level complaint
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDave, "FBI3_DLAA", "Dave", SPEECH_PARAMS_FORCE_NORMAL)
							iDriveComplaintTimer = -1
							
							PRINTLN( "*** FBI 3 *** - ", "PLAYING WANTED COMPLAINT")
						
						ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveEndLocation) > fDistanceCutoff
						// not got a vehicle complaint
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(pedDave)
							AND iNeedVehicleUses < 2
							
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDave, "NEED_A_VEHICLE", "Dave", SPEECH_PARAMS_FORCE_NORMAL)
								iDriveComplaintTimer = -1
								iNeedVehicleUses++
								
								PRINTLN( "*** FBI 3 *** - ", "PLAYING NEED VEHICLE COMPLAINT")
								
						// dave waiting for michael to get in or to drive.
							ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							OR IS_VEHICLE_ALMOST_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDave, "FBI3_DKAA", "Dave", SPEECH_PARAMS_FORCE_NORMAL)
								iDriveComplaintTimer = -1
								
								PRINTLN( "*** FBI 3 *** - ", "PLAYING HURRY UP COMPLAINT")
								
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			
			ENDIF

		ENDIF
	
	ENDIF

ENDPROC

INT iAssetRequests

PROC MANAGE_REAL_CUNTS_SCENE()

	// STREAM IN/OUT the wrong location scene
	//----------------------------------------------
			
	// Stream in
	IF NOT DOES_ENTITY_EXIST(pedCunts[0])
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_BlipRockfordHillsHouse) < 300
		
			SWITCH iAssetRequests
				CASE 0 		
					Load_Asset_Model(sAssetData, mnCuntActorM)			
					iAssetRequests++			
				BREAK
				CASE 1		
					IF HAS_MODEL_LOADED(mnCuntActorM)
						Load_Asset_Model(sAssetData, mnCuntActorF)			
						iAssetRequests++			
					ENDIF
				BREAK	
				CASE 2		
					IF HAS_MODEL_LOADED(mnCuntActorF)
						Load_Asset_Model(sAssetData, mnCuntCrewM)			
						iAssetRequests++			
					ENDIF
				BREAK
				CASE 3		
					IF HAS_MODEL_LOADED(mnCuntCrewM)
						Load_Asset_Model(sAssetData, mnCuntCrewF)			
						iAssetRequests++
					ENDIF
				BREAK
				CASE 4		
					IF HAS_MODEL_LOADED(mnCuntCrewF)
						Load_Asset_Model(sAssetData, mnCuntCam)				
						iAssetRequests++			
					ENDIF
				BREAK
				CASE 5		
					IF HAS_MODEL_LOADED(mnCuntCam)
						Load_Asset_Model(sAssetData, mnCuntBoom)			
						iAssetRequests++			
					ENDIF
				BREAK
				CASE 6		
					IF HAS_MODEL_LOADED(mnCuntBoom)
						Load_Asset_Model(sAssetData, mnCuntClipboard)		
						iAssetRequests++			
					ENDIF
				BREAK
				CASE 7
					IF HAS_MODEL_LOADED(mnCuntClipboard)
						Load_Asset_AnimDict(sAssetData, adWrongHouse)		
						iAssetRequests++
					ENDIF
				BREAK
				CASE 8
					IF HAS_ANIM_DICT_LOADED(adWrongHouse)
						
						pedCunts[CUNTPED_ACTOR_M] 			= CREATE_PED(PEDTYPE_MISSION, mnCuntActorM, <<-887.3506, -12.2818, 42.1914>>, 224.8453)
						pedCunts[CUNTPED_ACTOR_F] 			= CREATE_PED(PEDTYPE_MISSION, mnCuntActorF, <<-887.3506, -12.2818, 42.1914>>, 224.8453)
						
						pedCunts[CUNTPED_CREW_PRODUCER] 	= CREATE_PED(PEDTYPE_MISSION, mnCuntCrewF, 	<<-887.3506, -12.2818, 42.1914>>, 224.8453)
						pedCunts[CUNTPED_CREW_CAM_GUY] 		= CREATE_PED(PEDTYPE_MISSION, mnCuntCrewM, 	<<-887.3506, -12.2818, 42.1914>>, 224.8453)
						pedCunts[CUNTPED_CREW_SOUND_GUY] 	= CREATE_PED(PEDTYPE_MISSION, mnCuntCrewM, 	<<-887.3506, -12.2818, 42.1914>>, 224.8453)
						
						MISSION_PED_FLAGS i
						FOR i = CUNTPED_ACTOR_M TO CUNTPED_CREW_PRODUCER
							//SET_PED_CONFIG_FLAG( pedCunts[i], PCF_DisableShockingDrivingOnPavementEvents, FALSE )
							SET_PED_CAN_EVASIVE_DIVE( pedCunts[i], FALSE )
						ENDFOR
						
						objCunts[CUNTPROP_CLIPBOARD]		= CREATE_OBJECT(mnCuntClipboard, 	<<-887.3506, -12.2818, 42.1914>>)
						objCunts[CUNTPROP_CAM]				= CREATE_OBJECT(mnCuntCam, 			<<-887.3506, -12.2818, 42.1914>>)
						objCunts[CUNTPROP_BOOM]				= CREATE_OBJECT(mnCuntBoom, 		<<-887.3506, -12.2818, 42.1914>>)
						
						Unload_Asset_Model(sAssetData, mnCuntActorM)
						Unload_Asset_Model(sAssetData, mnCuntActorF)
						Unload_Asset_Model(sAssetData, mnCuntCrewM)
						Unload_Asset_Model(sAssetData, mnCuntCrewF)
						Unload_Asset_Model(sAssetData, mnCuntCam)
						Unload_Asset_Model(sAssetData, mnCuntBoom)
						Unload_Asset_Model(sAssetData, mnCuntClipboard)
						
						syncScene[SCENE_CUNTS] = CREATE_SYNCHRONIZED_SCENE(v_CuntsSceneCoord, v_CuntsSceneHeading)
						SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_CUNTS], TRUE)
						
						TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_ACTOR_M], 			syncScene[SCENE_CUNTS], adWrongHouse, "base_guy", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_ACTOR_F], 			syncScene[SCENE_CUNTS], adWrongHouse, "base_gal", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						
						TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_PRODUCER], 	syncScene[SCENE_CUNTS], adWrongHouse, "base_prod", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_CAM_GUY], 	syncScene[SCENE_CUNTS], adWrongHouse, "base_cman", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_SOUND_GUY], 	syncScene[SCENE_CUNTS], adWrongHouse, "base_sman", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CLIPBOARD], syncScene[SCENE_CUNTS], "base_clip", adWrongHouse, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CAM], 		syncScene[SCENE_CUNTS], "base_vcam", adWrongHouse, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_BOOM], 		syncScene[SCENE_CUNTS], "base_bmik", adWrongHouse, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
		
						INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("FBI_3_FIRST_ACTION", v_CuntsSceneCoord)
						
						
						
						b_CuntPropPlayingAnim[0] = TRUE
						b_CuntPropPlayingAnim[1] = TRUE
						b_CuntPropPlayingAnim[2] = TRUE
					
					ENDIF
				BREAK
			ENDSWITCH

		ENDIF
	
	// Steam out
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_BlipRockfordHillsHouse) > 350
			
			INT i
			REPEAT COUNT_OF(pedCunts) i
				IF NOT DOES_ENTITY_EXIST(pedCunts[i])
					DELETE_PED(pedCunts[i])
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(objCunts) i
				IF DOES_ENTITY_EXIST(objCunts[i])
					DELETE_OBJECT(objCunts[i])
				ENDIF
			ENDREPEAT
			
			Unload_Asset_Model(sAssetData, mnCuntActorM)
			Unload_Asset_Model(sAssetData, mnCuntActorF)
			Unload_Asset_Model(sAssetData, mnCuntCrewF)
			Unload_Asset_Model(sAssetData, mnCuntCrewM)
			Unload_Asset_Model(sAssetData, mnCuntCam)
			Unload_Asset_Model(sAssetData, mnCuntBoom)
			Unload_Asset_Model(sAssetData, mnCuntClipboard)
			Unload_Asset_Anim_Dict(sAssetData, adWrongHouse)
			
			iAssetRequests = 0
			
		ENDIF
	ENDIF
	
	// Play anims
	IF DOES_ENTITY_EXIST(pedCunts[0])
		
		SWITCH i_realCuntsStage
			CASE 1
				IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("FBI_3_FIRST_ACTION", 0)
					syncScene[SCENE_CUNTS] = CREATE_SYNCHRONIZED_SCENE(v_CuntsSceneCoord, v_CuntsSceneHeading)
					SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_CUNTS], FALSE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncScene[SCENE_CUNTS], TRUE)
					
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_ACTOR_M], 			syncScene[SCENE_CUNTS], adWrongHouse, "first_action_guy", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_ACTOR_F], 			syncScene[SCENE_CUNTS], adWrongHouse, "first_action_gal", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_PRODUCER], 	syncScene[SCENE_CUNTS], adWrongHouse, "first_action_prod", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_CAM_GUY], 	syncScene[SCENE_CUNTS], adWrongHouse, "first_action_cman", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_SOUND_GUY], 	syncScene[SCENE_CUNTS], adWrongHouse, "first_action_sman", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CLIPBOARD], syncScene[SCENE_CUNTS], "first_action_clip", adWrongHouse, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CAM], 		syncScene[SCENE_CUNTS], "first_action_vcam", adWrongHouse, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_BOOM], 		syncScene[SCENE_CUNTS], "first_action_bmik", adWrongHouse, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
					
					PLAY_SYNCHRONIZED_AUDIO_EVENT(syncScene[SCENE_CUNTS])
					
					i_realCuntsStage++
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncScene[SCENE_CUNTS])
				OR GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_CUNTS]) >= 1.0

					syncScene[SCENE_CUNTS] = CREATE_SYNCHRONIZED_SCENE(v_CuntsSceneCoord, v_CuntsSceneHeading)
					SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_CUNTS], TRUE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncScene[SCENE_CUNTS], TRUE)
					
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_ACTOR_M], 			syncScene[SCENE_CUNTS], adWrongHouse, "final_loop_guy", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_ACTOR_F], 			syncScene[SCENE_CUNTS], adWrongHouse, "final_loop_gal", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_PRODUCER], 	syncScene[SCENE_CUNTS], adWrongHouse, "final_loop_prod", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_CAM_GUY], 	syncScene[SCENE_CUNTS], adWrongHouse, "final_loop_cman", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(pedCunts[CUNTPED_CREW_SOUND_GUY], 	syncScene[SCENE_CUNTS], adWrongHouse, "final_loop_sman", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CLIPBOARD], syncScene[SCENE_CUNTS], "final_loop_clip", adWrongHouse, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_CAM], 		syncScene[SCENE_CUNTS], "final_loop_vcam", adWrongHouse, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCunts[CUNTPROP_BOOM], 		syncScene[SCENE_CUNTS], "final_loop_bmik", adWrongHouse, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					
					i_realCuntsStage++
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	

ENDPROC

FUNC BOOL TORTURE_IsValidTortureScene(SYNC_SCENES eScene)
	SWITCH eScene
		CASE SCENE_ELEC_IDLE						FALLTHRU
		CASE SCENE_ELEC_LEFT_GRIP					FALLTHRU
		CASE SCENE_ELEC_OUTRO						FALLTHRU
		CASE SCENE_ELEC_RIGHT_GRIP					FALLTHRU
		CASE SCENE_ELEC_SHOCK						FALLTHRU
		CASE SCENE_ELEC_SPARK						FALLTHRU
		CASE SCENE_PLIERS_ATTACH					FALLTHRU
		CASE SCENE_PLIERS_IDLE						FALLTHRU
		CASE SCENE_PLIERS_PULL_HARD					FALLTHRU
		CASE SCENE_PLIERS_PULL_OUT					FALLTHRU
		CASE SCENE_PLIERS_PULL_OUT2					FALLTHRU
		CASE SCENE_PLIERS_PULL_WEAK					FALLTHRU
		CASE SCENE_PLIERS_READY_IN					FALLTHRU
		CASE SCENE_PLIERS_READY_LOOP				FALLTHRU
		CASE SCENE_PLIERS_READY_OUT					FALLTHRU
		CASE SCENE_SYRINGE_IDLE						FALLTHRU
		CASE SCENE_SYRINGE_USE						FALLTHRU
		CASE SCENE_WATER_FLIP						FALLTHRU
		CASE SCENE_WATER_IDLE						FALLTHRU
		CASE SCENE_WATER_PRE_OUTRO					FALLTHRU
		CASE SCENE_WATER_OUTRO						FALLTHRU
		CASE SCENE_WATER_POUR						FALLTHRU
		CASE SCENE_WATER_POUR_TO_READY				FALLTHRU
		CASE SCENE_WATER_READY_TO_POUR				FALLTHRU
		CASE SCENE_WATER_READY						FALLTHRU
		CASE SCENE_WRENCH_IDLE						FALLTHRU
		CASE SCENE_WRENCH_LEFT						FALLTHRU
		CASE SCENE_WRENCH_MID						FALLTHRU
		CASE SCENE_WRENCH_RIGHT						FALLTHRU
		CASE SCENE_WEAPON_SELECT_IDLE				FALLTHRU
		CASE SCENE_WEAPON_SELECT_PICKUP_CLIP0		FALLTHRU
		CASE SCENE_WEAPON_SELECT_PICKUP_PLIERS		FALLTHRU
		CASE SCENE_WEAPON_SELECT_PICKUP_SYRINGE		FALLTHRU
		CASE SCENE_WEAPON_SELECT_PICKUP_WATER		FALLTHRU
		CASE SCENE_WEAPON_SELECT_PICKUP_WRENCH		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

STRUCT TORTURE_ANIM_DATA
	STRING strAnimDict
	STRING strTrevor
	STRING strMrK
	STRING strSteve
	STRING strCam
	STRING strWeapon
	STRING strChair
	STRING strECG
	STRING strRag
	STRING strTapeLH
	STRING strTapeRH	
	STRING strBenchPositionerWrench
	STRING strBenchPositionerClip0
	STRING strBenchPositionerClip1
	STRING strBenchPositionerJerrycan
	STRING strBenchPositionerPliers
	STRING strBenchPositionerBattery
	STRING strBenchPositionerTrolley
	STRING strBenchPositionerSyringe
	STRING strBenchPositionerRag
	STRING strWeaponSelectionOverride
	BOOL bSeparateSynSceneTrevor		= FALSE		// when doing the weapon select we don't want to include trevor in the sync scene
	BOOL bSeparateSynSceneMrK			= FALSE		// used when Mr K is playing his fidgeting loop
	BOOL bAttachWeaponProp				= TRUE		// attach the weapon prop to the player's prop bone
	BOOL bForceUpdateDueToCut			= FALSE		// a camera cut occurs here so force an update on the peds AI and ANIMATION
	BOOL bScriptedAnimationTask			= FALSE		// indicates for this scene that a Trevor and Mr K will run a script anim task
	
ENDSTRUCT

FUNC BOOL TORTURE_GetTortureSceneAnimData(SYNC_SCENES eScene, TORTURE_ANIM_DATA &sAnimData)
	
	IF TORTURE_IsValidTortureScene(eScene)

		SWITCH eScene
	// >>>> ELECTRICUTE SCENES
			CASE SCENE_ELEC_IDLE
			CASE SCENE_ELEC_LEFT_GRIP
			CASE SCENE_ELEC_RIGHT_GRIP
			CASE SCENE_ELEC_BOTH_GRIP
			CASE SCENE_ELEC_SHOCK
				sAnimData.StrAnimDict					= "MISSFBI3_electrocute"
			BREAK
			
			CASE SCENE_ELEC_SPARK
				sAnimData.StrAnimDict					= "MISSFBI3_electrocute"
				sAnimData.strTrevor 					= "Electrocute_Spark_Player"
				sAnimData.strMrk						= "Electrocute_Spark_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Electrocute_Spark_Cam"
				sAnimData.strWeapon						= "Electrocute_Spark"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Electrocute_Spark_hand_l"
				sAnimData.strTapeRH						= "Electrocute_Spark_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
			CASE SCENE_ELEC_OUTRO
				sAnimData.StrAnimDict					= "MISSFBI3_electrocute"
				sAnimData.strTrevor 					= "Electrocute_Both_Outro_Player"
				sAnimData.strMrk						= "Electrocute_Both_Outro_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Electrocute_Both_Outro_Cam"
				sAnimData.strWeapon						= "Electrocute_Both_Outro"
				sAnimData.strChair 						= "Electrocute_Both_Outro_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Electrocute_Both_Outro_hand_l"
				sAnimData.strTapeRH						= "Electrocute_Both_Outro_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
			BREAK
			
	// >>> PLIER SCENES/TOOTH PULL
			CASE SCENE_PLIERS_IDLE
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Idle_Player"
				sAnimData.strMrk						= "Pull_Tooth_Idle_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Idle_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Idle_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Idle_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Idle_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
			CASE SCENE_PLIERS_ATTACH
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Intro_Player"
				sAnimData.strMrk						= "Pull_Tooth_Intro_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Intro_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Intro_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Intro_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Intro_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
			CASE SCENE_PLIERS_PULL_WEAK
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Loop_Weak_Player"
				sAnimData.strMrk						= "Pull_Tooth_Loop_Weak_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Loop_Weak_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Loop_Weak_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Loop_Weak_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Loop_Weak_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
				sAnimData.bScriptedAnimationTask		= TRUE
			BREAK
			CASE SCENE_PLIERS_PULL_HARD
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Loop_Player"
				sAnimData.strMrk						= "Pull_Tooth_Loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Loop_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Loop_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Loop_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
				sAnimData.bScriptedAnimationTask		= TRUE
			BREAK
			CASE SCENE_PLIERS_PULL_OUT
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Outro_Player"
				sAnimData.strMrk						= "Pull_Tooth_Outro_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Outro_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Outro_Pliers"
				sAnimData.strChair 						= "Pull_Tooth_Outro_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Outro_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Outro_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
			CASE SCENE_PLIERS_PULL_OUT2
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Outro_B_Player"
				sAnimData.strMrk						= "Pull_Tooth_Outro_B_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Outro_B_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Outro_B_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Outro_B_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Outro_B_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
			CASE SCENE_PLIERS_READY_IN
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Ready_Intro_Player"
				sAnimData.strMrk						= "Pull_Tooth_Ready_Intro_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Ready_Intro_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Ready_Intro_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Ready_Intro_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Ready_Intro_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE				
			BREAK
			CASE SCENE_PLIERS_READY_LOOP
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Ready_Loop_Player"
				sAnimData.strMrk						= "Pull_Tooth_Ready_Loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Ready_Loop_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Ready_Loop_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Ready_Loop_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Ready_Loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
				
			BREAK
			CASE SCENE_PLIERS_READY_OUT
				sAnimData.StrAnimDict					= "MISSFBI3_TOOTHPULL"
				sAnimData.strTrevor 					= "Pull_Tooth_Ready_Outro_Player"
				sAnimData.strMrk						= "Pull_Tooth_Ready_Outro_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Pull_Tooth_Ready_Outro_Cam"
				sAnimData.strWeapon						= "Pull_Tooth_Ready_Outro_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Pull_Tooth_Ready_Outro_hand_l"
				sAnimData.strTapeRH						= "Pull_Tooth_Ready_Outro_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK		
	// >>> SYRINGE
			CASE SCENE_SYRINGE_IDLE
				sAnimData.StrAnimDict					= "MISSFBI3_SYRINGE"
				sAnimData.strTrevor 					= "Syringe_Idle_Player"
				sAnimData.strMrk						= "flatline_loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Syringe_Idle_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "flatline_loop_chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "flatline_loop_hand_l"
				sAnimData.strTapeRH						= "flatline_loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
			BREAK
			CASE SCENE_SYRINGE_USE
				sAnimData.strAnimDict					= "MISSFBI3_SYRINGE"
				sAnimData.strTrevor 					= "Syringe_Use_Player"
				sAnimData.strMrk						= "Syringe_Use_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Syringe_Use_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Syringe_Use_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Syringe_Use_hand_l"
				sAnimData.strTapeRH						= "Syringe_Use_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
			BREAK
	// >>>> WATERBOARD
			CASE SCENE_WATER_FLIP
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_kick_Player"
				sAnimData.strMrk						= "Waterboard_Kick_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Kick_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Kick_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Kick_Rag"
				sAnimData.strTapeLH						= "Waterboard_Kick_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Kick_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
			BREAK
			CASE SCENE_WATER_IDLE
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Idle_Player"
				sAnimData.strMrk						= "Waterboard_Idle_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Idle_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Idle_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Idle_Rag"
				sAnimData.strTapeLH						= "Waterboard_Idle_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Idle_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
			CASE SCENE_WATER_PRE_OUTRO
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Loop_To_Ready_Player"
				sAnimData.strMrk						= "Waterboard_Loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Loop_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Loop_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Loop_Rag"
				sAnimData.strTapeLH						= "Waterboard_Loop_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
				sAnimData.bSeparateSynSceneMrK			= TRUE
			BREAK
			CASE SCENE_WATER_OUTRO
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Outro_Player"
				sAnimData.strMrk						= "Waterboard_Outro_Victim"
				sAnimData.strSteve 						= "Waterboard_Outro_Steve"
				sAnimData.strCam						= "Waterboard_Outro_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Outro_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Outro_Rag"
				sAnimData.strTapeLH						= "Waterboard_Outro_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Outro_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
			BREAK
			CASE SCENE_WATER_POUR
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Loop_Player"
				sAnimData.strMrk						= "Waterboard_Loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Loop_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Loop_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Loop_Rag"
				sAnimData.strTapeLH						= "Waterboard_Loop_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
			BREAK
			CASE SCENE_WATER_READY_TO_POUR
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Ready_To_Loop_Player"
				sAnimData.strMrk						= "Waterboard_Ready_Loop_Victim" //"Waterboard_Ready_To_Loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Ready_To_Loop_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Ready_Loop_Chair" //"Waterboard_Ready_To_Loop_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Ready_To_Loop_Rag"
				sAnimData.strTapeLH						= "Waterboard_Ready_Loop_hand_l" //"Waterboard_Ready_To_Loop_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Ready_Loop_hand_r" //"Waterboard_Ready_To_Loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
				sAnimData.bSeparateSynSceneMrK 			= TRUE
			BREAK
			CASE SCENE_WATER_POUR_TO_READY
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Loop_To_Ready_Player"
				sAnimData.strMrk						= "Waterboard_Ready_Loop_Victim" //"Waterboard_Loop_To_Ready_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Loop_To_Ready_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Ready_Loop_Chair" //"Waterboard_Loop_To_Ready_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Loop_To_Ready_Rag"
				sAnimData.strTapeLH						= "Waterboard_Ready_Loop_hand_l" //"Waterboard_Loop_To_Ready_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Ready_Loop_hand_r" //"Waterboard_Loop_To_Ready_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bSeparateSynSceneMrK 			= TRUE
			BREAK
			CASE SCENE_WATER_READY
				sAnimData.strAnimDict					= "MISSFBI3_WATERBOARD"
				sAnimData.strTrevor 					= "Waterboard_Ready_Loop_Player"
				sAnimData.strMrk						= "Waterboard_Ready_Loop_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Waterboard_Ready_Loop_Cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "Waterboard_Ready_Loop_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Waterboard_Ready_Loop_Rag"
				sAnimData.strTapeLH						= "Waterboard_Ready_Loop_hand_l"
				sAnimData.strTapeRH						= "Waterboard_Ready_Loop_hand_r"
				sAnimData.bForceUpdateDueToCut			= FALSE
			BREAK
	// >>>> WRENCH ATTACK
			CASE SCENE_WRENCH_IDLE
				sAnimData.strAnimDict					= "MISSFBI3_WRENCH"
				sAnimData.strTrevor 					= "Wrench_Idle_Player"
				sAnimData.strMrk						= "Wrench_Idle_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Wrench_Idle_Cam"
				sAnimData.strWeapon						= "Wrench_Idle_Wrench"
				sAnimData.strChair 						= "Wrench_Idle_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Wrench_Idle_hand_l"
				sAnimData.strTapeRH						= "Wrench_Idle_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
			BREAK
			CASE SCENE_WRENCH_LEFT
				sAnimData.strAnimDict					= "MISSFBI3_WRENCH"
				sAnimData.strTrevor 					= "Wrench_Attack_Left_Player"
				sAnimData.strMrk						= "Wrench_Attack_Left_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Wrench_Attack_Left_Cam"
				sAnimData.strWeapon						= "Wrench_Attack_Left_Wrench"
				sAnimData.strChair 						= "Wrench_Attack_Left_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Wrench_Attack_Left_hand_l"
				sAnimData.strTapeRH						= "Wrench_Attack_Left_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
			BREAK 
			CASE SCENE_WRENCH_MID
				sAnimData.strAnimDict					= "MISSFBI3_WRENCH"
				sAnimData.strTrevor 					= "Wrench_Attack_Mid_Player"
				sAnimData.strMrk						= "Wrench_Attack_Mid_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Wrench_Attack_Mid_Cam"
				sAnimData.strWeapon						= "Wrench_Attack_Mid_Wrench"
				sAnimData.strChair 						= "Wrench_Attack_Mid_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Wrench_Attack_Mid_hand_l"
				sAnimData.strTapeRH						= "Wrench_Attack_Mid_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
			BREAK
			CASE SCENE_WRENCH_RIGHT
				sAnimData.strAnimDict					= "MISSFBI3_WRENCH"
				sAnimData.strTrevor 					= "Wrench_Attack_Right_Player"
				sAnimData.strMrk						= "Wrench_Attack_Right_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Wrench_Attack_Right_Cam"
				sAnimData.strWeapon						= "Wrench_Attack_Right_Wrench"
				sAnimData.strChair 						= "Wrench_Attack_Right_Chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Wrench_Attack_Right_hand_l"
				sAnimData.strTapeRH						= "Wrench_Attack_Right_hand_r"
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
			BREAK
	// >>> WEAPON SELECTION
			CASE SCENE_WEAPON_SELECT_IDLE
				sAnimData.strAnimDict					= "MISSFBI3_WEAPON_SELECT"
				sAnimData.strTrevor 					= "Weapon_Select_Loop"
				sAnimData.strMrk						= "waterboard_idle_victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= ""
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "waterboard_idle_chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "waterboard_idle_hand_l"
				sAnimData.strTapeRH						= "waterboard_idle_hand_r"
				sAnimData.bSeparateSynSceneTrevor		= TRUE
				sAnimData.bSeparateSynSceneMrK			= FALSE
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
				sAnimData.strBenchPositionerWrench		= "weapon_select_bench_wrench" 
				sAnimData.strBenchPositionerClip0		= "weapon_select_bench_clip1"
				sAnimData.strBenchPositionerClip1		= "weapon_select_bench_clip2"
				sAnimData.strBenchPositionerJerrycan	= "weapon_select_bench_can"
				sAnimData.strBenchPositionerPliers		= "weapon_select_bench_pliers"
				sAnimData.strBenchPositionerBattery		= "weapon_select_bench_battery"
				sAnimData.strBenchPositionerTrolley		= "weapon_select_bench_bench"
				sAnimData.strBenchPositionerSyringe		= "weapon_select_bench_syringe"
				sAnimData.strBenchPositionerRag			= "weapon_select_bench_rag"
			BREAK
			CASE SCENE_WEAPON_SELECT_PICKUP_CLIP0
				sAnimData.strAnimDict					= "MISSFBI3_WEAPON_SELECT"
				sAnimData.strTrevor 					= "Weapon_Select_Electrocute_Player"
				sAnimData.strMrk						= "waterboard_idle_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Weapon_Select_Electrocute_Cam"
				sAnimData.strWeapon						= "Weapon_Select_Electrocute"
				sAnimData.strChair 						= "waterboard_idle_chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "waterboard_idle_hand_l"
				sAnimData.strTapeRH						= "waterboard_idle_hand_r"
				sAnimData.bSeparateSynSceneTrevor		= FALSE
				sAnimData.bSeparateSynSceneMrK			= TRUE
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
			BREAK
			CASE SCENE_WEAPON_SELECT_PICKUP_PLIERS
				sAnimData.strAnimDict					= "MISSFBI3_WEAPON_SELECT"
				sAnimData.strTrevor 					= "Weapon_Select_Pliers_Player"
				sAnimData.strMrk						= "weapon_select_pliers_victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Weapon_Select_Pliers_Cam"
				sAnimData.strWeapon						= "Weapon_Select_Pliers_Pliers"
				sAnimData.strChair 						= ""
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "Weapon_Select_Pliers_hand_l"
				sAnimData.strTapeRH						= "Weapon_Select_Pliers_hand_r"
				sAnimData.bSeparateSynSceneTrevor		= FALSE
				sAnimData.bSeparateSynSceneMrK			= FALSE
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
				sAnimData.strBenchPositionerWrench		= "weapon_bench_toothpull_wrench"
				sAnimData.strBenchPositionerClip0		= "weapon_bench_toothpull_clip1"
				sAnimData.strBenchPositionerClip1		= "weapon_bench_toothpull_clip2"
				sAnimData.strBenchPositionerJerrycan	= "weapon_bench_toothpull_can"
				sAnimData.strBenchPositionerPliers		= "" // OMITTED
				sAnimData.strBenchPositionerBattery		= "weapon_bench_toothpull_battery"
				sAnimData.strBenchPositionerTrolley		= "weapon_bench_toothpull_trolley"
				sAnimData.strBenchPositionerSyringe		= "weapon_bench_toothpull_syringe"
				sAnimData.strBenchPositionerRag			= "weapon_bench_toothpull_rag"
			BREAK
			CASE SCENE_WEAPON_SELECT_PICKUP_SYRINGE
				sAnimData.strAnimDict					= "MISSFBI3_SYRINGE"
				sAnimData.strTrevor 					= "Weapon_Select_Syringe_Player" //"flatline_intro_player"
				sAnimData.strMrk						= "flatline_intro_victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Weapon_Select_Syringe_Cam" //"flatline_intro_cam"
				sAnimData.strWeapon						= ""
				sAnimData.strChair 						= "flatline_intro_chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "flatline_intro_hand_l"
				sAnimData.strTapeRH						= "flatline_intro_hand_r"
				sAnimData.bSeparateSynSceneTrevor		= FALSE
				sAnimData.bSeparateSynSceneMrK			= FALSE
				sAnimData.bScriptedAnimationTask		= FALSE
				sAnimData.bAttachWeaponProp				= TRUE
				sAnimData.strBenchPositionerWrench		= "weapon_bench_syringe_wrench"
				sAnimData.strBenchPositionerClip0		= "weapon_bench_syringe_clip1"
				sAnimData.strBenchPositionerClip1		= "weapon_bench_syringe_clip2"
				sAnimData.strBenchPositionerJerrycan	= "weapon_bench_syringe_can"
				sAnimData.strBenchPositionerPliers		= "weapon_bench_syringe_pliers"
				sAnimData.strBenchPositionerBattery		= "weapon_bench_syringe_battery"
				sAnimData.strBenchPositionerTrolley		= "weapon_bench_syringe_trolley"
				sAnimData.strBenchPositionerSyringe		= "" // OMITTED
				sAnimData.strBenchPositionerRag			= "weapon_bench_syringe_rag"
			BREAK
			CASE SCENE_WEAPON_SELECT_PICKUP_WATER
				sAnimData.strAnimDict					= "MISSFBI3_WEAPON_SELECT"
				sAnimData.strTrevor 					= "Weapon_Select_Waterboard_Player"
				sAnimData.strMrk						= "waterboard_idle_victim"//"Weapon_Select_Waterboard_Victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Weapon_Select_Waterboard_Cam"
				sAnimData.strWeapon						= "Weapon_Select_Waterboard_jerryCan"
				sAnimData.strChair 						= "waterboard_idle_chair" //"Weapon_Select_Waterboard_Chair" 
				sAnimData.strECG						= ""
				sAnimData.strRag						= "Weapon_Select_Waterboard_rag"
				sAnimData.strTapeLH						= "waterboard_idle_hand_l"//"Weapon_Select_Waterboard_Hand_L"
				sAnimData.strTapeRH						= "waterboard_idle_hand_r"//"Weapon_Select_Waterboard_Hand_R"
				sAnimData.bSeparateSynSceneTrevor		= FALSE
				sAnimData.bSeparateSynSceneMrK			= TRUE
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
				sAnimData.strBenchPositionerWrench		= "weapon_bench_waterboard_wrench"
				sAnimData.strBenchPositionerClip0		= "weapon_bench_waterboard_clip1"
				sAnimData.strBenchPositionerClip1		= "weapon_bench_waterboard_clip2"
				sAnimData.strBenchPositionerJerrycan	= "" // OMITTED
				sAnimData.strBenchPositionerPliers		= "weapon_bench_waterboard_pliers"
				sAnimData.strBenchPositionerBattery		= "weapon_bench_waterboard_battery"
				sAnimData.strBenchPositionerTrolley		= "weapon_bench_waterboard_trolley"
				sAnimData.strBenchPositionerSyringe		= "weapon_bench_waterboard_syringe"
				sAnimData.strBenchPositionerRag			= ""
			BREAK
			CASE SCENE_WEAPON_SELECT_PICKUP_WRENCH
				sAnimData.strAnimDict					= "MISSFBI3_WEAPON_SELECT"
				sAnimData.strTrevor 					= "Weapon_Select_Wrench_Player"
				sAnimData.strMrk						= "waterboard_idle_victim"
				sAnimData.strSteve 						= ""
				sAnimData.strCam						= "Weapon_Select_Wrench_Cam"
				sAnimData.strWeapon						= "Weapon_Select_Wrench_Wrench"
				sAnimData.strChair 						= "waterboard_idle_chair"
				sAnimData.strECG						= ""
				sAnimData.strRag						= ""
				sAnimData.strTapeLH						= "waterboard_idle_hand_l"
				sAnimData.strTapeRH						= "waterboard_idle_hand_r"
				sAnimData.bSeparateSynSceneTrevor		= FALSE
				sAnimData.bSeparateSynSceneMrK			= TRUE
				sAnimData.bForceUpdateDueToCut			= TRUE
				sAnimData.bAttachWeaponProp				= FALSE
				sAnimData.strBenchPositionerWrench		= "" // OMMITED
				sAnimData.strBenchPositionerClip0		= "weapon_bench_wrench_clip1"
				sAnimData.strBenchPositionerClip1		= "weapon_bench_wrench_clip2"
				sAnimData.strBenchPositionerJerrycan	= "weapon_bench_wrench_can"
				sAnimData.strBenchPositionerPliers		= "weapon_bench_wrench_pliers"
				sAnimData.strBenchPositionerBattery		= "weapon_bench_wrench_battery"
				sAnimData.strBenchPositionerTrolley		= "weapon_bench_wrench_trolley"
				sAnimData.strBenchPositionerSyringe		= "weapon_bench_wrench_syringe"
				sAnimData.strBenchPositionerRag			= "weapon_bench_wrench_rag"
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	
	ENDIF

	RETURN FALSE
ENDFUNC

PROC TORTURE_StopTortureWeaponAnim(sMenuObject &thisObject, FLOAT blendDelta)

	IF thisObject.bHasPlayedAnim
		
		IF NOT IS_STRING_NULL_OR_EMPTY(thisObject.strLastPlayedDict)
		AND NOT IS_STRING_NULL_OR_EMPTY(thisObject.strLastPlayedAnim)
			IF IS_ENTITY_PLAYING_ANIM(thisObject.objObject, thisObject.strLastPlayedDict, thisObject.strLastPlayedAnim)
				STOP_ENTITY_ANIM(thisObject.objObject, thisObject.strLastPlayedAnim, thisObject.strLastPlayedDict, blendDelta)
				thisObject.bHasPlayedAnim 			= FALSE
			ENDIF
		ELSE
			STOP_SYNCHRONIZED_ENTITY_ANIM(thisObject.objObject, blendDelta, FALSE)
			thisObject.bHasPlayedAnim 			= FALSE
		ENDIF
		
		thisObject.strLastPlayedDict		= ""
		thisObject.strLastPlayedAnim		= ""
		
	ENDIF

ENDPROC

FUNC BOOL TORTURE_PlaySyncScene(SYNC_SCENES eScene, BOOL bLoop = FALSE, FLOAT StartPhase = 0.0, 
		FLOAT BlendInDelta = INSTANT_BLEND_IN, FLOAT BlendOutDelta = INSTANT_BLEND_OUT,
		SYNCED_SCENE_PLAYBACK_FLAGS PlaybackFlagsTorture = SYNCED_SCENE_NONE, SYNCED_SCENE_PLAYBACK_FLAGS PlaybackFlagsVictim = SYNCED_SCENE_NONE)

	IF TORTURE_IsValidTortureScene(eScene)
	AND NOT bPlayedTortureSceneLastFrame
	AND NOT bPlayedTortureSceneThisFrame
	
		TORTURE_ANIM_DATA sAnimData
		IF TORTURE_GetTortureSceneAnimData(eScene, sAnimData)	
		AND NOT sAnimData.bScriptedAnimationTask
			
			CPRINTLN(DEBUG_MIKE, "\n")
			CPRINTLN(DEBUG_MIKE, "[*** TORTURE SYNCED SCENE ***]")
			CPRINTLN(DEBUG_MIKE, "	Scene Info")
			CPRINTLN(DEBUG_MIKE, "		AnimDict : ", 						sAnimData.strAnimDict)
			CPRINTLN(DEBUG_MIKE, "		Trevor : ", 						sAnimData.strTrevor)
			CPRINTLN(DEBUG_MIKE, "		Mr. K : ", 							sAnimData.strMrK)
			CPRINTLN(DEBUG_MIKE, "		Steve : ", 							sAnimData.strSteve)
			CPRINTLN(DEBUG_MIKE, "		Cam : ", 							sAnimData.strCam)
			CPRINTLN(DEBUG_MIKE, "		Weapon : ",							sAnimData.strWeapon)
			CPRINTLN(DEBUG_MIKE, "		Chair : ", 							sAnimData.strChair)
			CPRINTLN(DEBUG_MIKE, "		ECG : ", 							sAnimData.strECG)
			CPRINTLN(DEBUG_MIKE, "		Rag : ", 							sAnimData.strRag)
			CPRINTLN(DEBUG_MIKE, "		Tape LH : ", 						sAnimData.strTapeLH)
			CPRINTLN(DEBUG_MIKE, "		Tape RH : ", 						sAnimData.strTapeRH)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Wrench : ", 					sAnimData.strBenchPositionerWrench)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Clip0 : ", 					sAnimData.strBenchPositionerClip0)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Clip1 : ", 					sAnimData.strBenchPositionerClip1)
			CPRINTLN(DEBUG_MIKE, "		EXTRA JerryCan : ", 				sAnimData.strBenchPositionerJerrycan)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Pliers : ", 					sAnimData.strBenchPositionerPliers)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Battery : ", 					sAnimData.strBenchPositionerBattery)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Trolley : ", 					sAnimData.strBenchPositionerTrolley)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Trolley : ", 					sAnimData.strBenchPositionerSyringe)
			CPRINTLN(DEBUG_MIKE, "		EXTRA Trolley : ", 					sAnimData.strBenchPositionerRag)
			
			CPRINTLN(DEBUG_MIKE, "		Separate Sync Scene Trevor : ", 	sAnimData.bSeparateSynSceneTrevor)
			CPRINTLN(DEBUG_MIKE, "		Separate Sync Scene Mr K : ", 		sAnimData.bSeparateSynSceneMrK)
			CPRINTLN(DEBUG_MIKE, "		Force ped AI and ANIM update : ", 	sAnimData.bForceUpdateDueToCut)
			CPRINTLN(DEBUG_MIKE, "		Scripted Anim Task : ", 			sAnimData.bScriptedAnimationTask,"\n")
			
			CPRINTLN(DEBUG_MIKE, "		BLEND IN : ", 						BlendInDelta)
			CPRINTLN(DEBUG_MIKE, "		BLEND OUT : ", 						BlendOutDelta)
			CPRINTLN(DEBUG_MIKE, "		Attach weapon to PH : ", 			sAnimData.bAttachWeaponProp)
			
			CPRINTLN(DEBUG_MIKE, "\n")
			
			IF IS_SCREEN_FADED_OUT()
			AND NOT bDoSkip
				//BlendOutDelta = INSTANT_BLEND_OUT
				BlendInDelta = INSTANT_BLEND_IN
				sAnimData.bForceUpdateDueToCut = TRUE
				SAFE_FADE_IN(DEFAULT_FADE_TIME_SHORT)
			ENDIF

			syncScene[eScene] 							= CREATE_SYNCHRONIZED_SCENE(vTortureScene, vTortureRot)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncScene[eScene], TRUE)
			
			// Mr K needs to play in a separate scync scene as hes using a loop of a different length
			IF sAnimData.bSeparateSynSceneMrK
				syncScene[SCENE_EXTRA_MR_K_FIDGET] 		= CREATE_SYNCHRONIZED_SCENE(vTortureScene, vTortureRot)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncScene[SCENE_EXTRA_MR_K_FIDGET], TRUE)
			ENDIF
			
	// <<<<<<<<<<<<<< Trevor >>>>>>>>>>>>>>>
	//---------------------------------------------------------
			SET_PED_CURRENT_WEAPON_VISIBLE(TREV_PED_ID(), FALSE) 
			
			// Specific anims
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strTrevor)
				IF NOT sAnimData.bSeparateSynSceneTrevor
					
					TASK_SYNCHRONIZED_SCENE(TREV_PED_ID(), syncScene[eScene], sAnimData.strAnimDict, sAnimData.strTrevor, BlendInDelta, BlendOutDelta, PlaybackFlagsTorture)
					
				// Weapon select idle
				ELSE
					TORTURE_ANIM_DATA sAnimDataTrevor
					IF TORTURE_GetTortureSceneAnimData(SCENE_WEAPON_SELECT_IDLE, sAnimDataTrevor)
					AND ARE_STRINGS_EQUAL(sAnimData.strAnimDict, sAnimDataTrevor.strAnimDict)
						TASK_PLAY_ANIM_ADVANCED(TREV_PED_ID(), 
								sAnimDataTrevor.strAnimDict, 
								sAnimDataTrevor.strTrevor, 
								<< 144.053, -2203.052, 4.706 >>, << 0.000, 0.000, 180.000 >>,
								INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, 
								AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING, 0.0)
						
					ELSE
						SCRIPT_ASSERT("NO SPECIFIC ANIMS NOR IS WEAPON SELECT IDLE LOADED FOR TREVOR!")
					ENDIF
				ENDIF
			ENDIF
			
	// <<<<<<<<<<<<<< Victim - Mr K >>>>>>>>>>>>>>>
	//---------------------------------------------------------
	
			SYNC_SCENES eMrKScene
			IF sAnimData.bSeparateSynSceneMrK
				eMrKScene = SCENE_EXTRA_MR_K_FIDGET
			ELSE
				eMrKScene = eScene
			ENDIF
			
			IF NOT IS_STRING_NULL(sAnimData.strMrK)
				IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sAnimData.strAnimDict, sAnimData.strMrK, ANIM_DEFAULT)
					TASK_SYNCHRONIZED_SCENE(pedVictim, syncScene[eMrKScene], sAnimData.strAnimDict, sAnimData.strMrK, BlendInDelta, BlendOutDelta, PlaybackFlagsVictim)
				ENDIF
			ELSE
				SCRIPT_ASSERT("MR K HAS NO ANIM, HE MUST HAVE AN ANIM AT THIS POINT OF THE TORTURE")
			ENDIF
			
	// <<<<<<<<<<<<<< Chair >>>>>>>>>>>>>>>
	//---------------------------------------------------------
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strChair)
				IF IS_ENTITY_ATTACHED(objChair)
					DETACH_ENTITY(objChair, FALSE)
				ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(objChair, sAnimData.strAnimDict, sAnimData.strChair, ANIM_DEFAULT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objChair, syncScene[eMrKScene], sAnimData.strChair, sAnimData.strAnimDict, BlendInDelta, BlendOutDelta, enum_to_int(PlaybackFlagsVictim))
					bHasChairPlayedAnim = TRUE
				ENDIF
			ELSE
				IF IS_ENTITY_ATTACHED(objChair)
					DETACH_ENTITY(objChair, FALSE)
				ENDIF
			
				IF bHasChairPlayedAnim
					STOP_SYNCHRONIZED_ENTITY_ANIM(objChair, BlendOutDelta, FALSE)
					bHasChairPlayedAnim = FALSE
				ENDIF
			
				SET_ENTITY_COORDS(objChair, vChairCoords)
				SET_ENTITY_ROTATION(objChair, vChairRot)
			ENDIF
			
	// <<<<<<<<<<<<<< Restraints >>>>>>>>>>>>>>>
	//---------------------------------------------------------
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strTapeLH)
			AND NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strTapeRH)
				IF IS_ENTITY_ATTACHED(objTapeArmLeft)
					DETACH_ENTITY(objTapeArmLeft, FALSE)
				ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(objTapeArmLeft, sAnimData.strAnimDict, sAnimData.strTapeLH, ANIM_DEFAULT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objTapeArmLeft, syncScene[eMrKScene], sAnimData.strTapeLH, sAnimData.strAnimDict, BlendInDelta, BlendOutDelta, enum_to_int(PlaybackFlagsVictim))
				ENDIF
				
				IF IS_ENTITY_ATTACHED(objTapeArmRight)
					DETACH_ENTITY(objTapeArmRight, FALSE)
				ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(objTapeArmRight, sAnimData.strAnimDict, sAnimData.strTapeRH, ANIM_DEFAULT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objTapeArmRight, syncScene[eMrKScene], sAnimData.strTapeRH, sAnimData.strAnimDict, BlendInDelta, BlendOutDelta, enum_to_int(PlaybackFlagsVictim))
				ENDIF
				
				#IF NOT IS_JAPANESE_BUILD
					bHasRestraintPlayedAnim = TRUE
				#ENDIF

			ENDIF
			
			
	// <<<<<<<<<<<<<< Torture weapon >>>>>>>>>>>>>>>
	//---------------------------------------------------------
			IF eScene != SCENE_WEAPON_SELECT_IDLE // DO NOTHING WITH THE WEAPON IF THIS IS THE WEAPON SELECT SELECT SYNC SCENE
			
				TORTURE_StopTortureWeaponAnim(menuWeapon[iCurrentMenuWeapon], BlendOutDelta)
				IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP1)
					TORTURE_StopTortureWeaponAnim(menuWeapon[iCurrentMenuWeapon], BlendOutDelta)
				ENDIF
			 
			// Sort out the weapons attachment
				IF sAnimData.bAttachWeaponProp
				
					IF NOT IS_ENTITY_ATTACHED(menuWeapon[iCurrentMenuWeapon].objObject)
						ATTACH_ENTITY_TO_ENTITY(menuWeapon[iCurrentMenuWeapon].objObject, TREV_PED_ID(), 
												GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_R_HAND),
												menuWeapon[iCurrentMenuWeapon].vAttachOffset, 
												menuWeapon[iCurrentMenuWeapon].vAttachRot)
					ENDIF
					IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP0)

						IF NOT IS_ENTITY_ATTACHED(menuWeapon[ENUM_TO_INT(TOR_CLIP1)].objObject)
							ATTACH_ENTITY_TO_ENTITY(menuWeapon[ENUM_TO_INT(TOR_CLIP1)].objObject, 
													TREV_PED_ID(), 
													GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_L_HAND),
													menuWeapon[ENUM_TO_INT(TOR_CLIP1)].vAttachOffset, 
													menuWeapon[ENUM_TO_INT(TOR_CLIP1)].vAttachRot)
						ENDIF
					ENDIF
				
				ELSE
					IF IS_ENTITY_ATTACHED(menuWeapon[iCurrentMenuWeapon].objObject)
						DETACH_ENTITY(menuWeapon[iCurrentMenuWeapon].objObject, FALSE)
					ENDIF
					
					IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP0)
						IF IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP1].objObject)
							DETACH_ENTITY(menuWeapon[TOR_CLIP1].objObject, FALSE)
						ENDIF
					ENDIF
				ENDIF
			
			// Play the weapon anim if one exists
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strWeapon)
				
					TEXT_LABEL_63 strAnim 		= sAnimData.strWeapon
					TEXT_LABEL_63 strAnimExtra	= sAnimData.strWeapon

					IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP0)
						strAnim 		+= "_R_Clip"
						strAnimExtra	+= "_L_Clip"
					ENDIF

					// Not attached so play as a synched scene
					IF NOT sAnimData.bAttachWeaponProp
						
						PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[iCurrentMenuWeapon].objObject, syncScene[eScene], strAnim, sAnimData.strAnimDict, BlendInDelta, BlendOutDelta, enum_to_int(PlaybackFlagsTorture))
						menuWeapon[iCurrentMenuWeapon].strLastPlayedDict 	= ""
						menuWeapon[iCurrentMenuWeapon].strLastPlayedAnim	= ""
						menuWeapon[iCurrentMenuWeapon].bHasPlayedAnim 		= TRUE
						
						IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP0)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[ENUM_TO_INT(TOR_CLIP1)].objObject, syncScene[eScene], strAnimExtra, sAnimData.strAnimDict, BlendInDelta, BlendOutDelta, enum_to_int(PlaybackFlagsTorture))
							menuWeapon[TOR_CLIP1].strLastPlayedDict 	= ""
							menuWeapon[TOR_CLIP1].strLastPlayedAnim		= ""
							menuWeapon[TOR_CLIP1].bHasPlayedAnim 		= TRUE
						ENDIF
						
					// Is attached so play as a normal anim
					ELSE

						PLAY_ENTITY_ANIM(menuWeapon[iCurrentMenuWeapon].objObject, 
											strAnim,  sAnimData.strAnimDict, BlendInDelta, 
											bLoop, TRUE, DEFAULT, StartPhase)
						
						menuWeapon[TOR_CLIP1].strLastPlayedDict 	= sAnimData.strAnimDict
						menuWeapon[TOR_CLIP1].strLastPlayedAnim		= strAnim
						menuWeapon[TOR_CLIP1].bHasPlayedAnim 		= TRUE
						
						IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP1)
							TORTURE_StopTortureWeaponAnim(menuWeapon[iCurrentMenuWeapon], BlendOutDelta)
						ENDIF
						
						IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP0)
							PLAY_ENTITY_ANIM(menuWeapon[TOR_CLIP1].objObject, 
												strAnimExtra,  sAnimData.strAnimDict, BlendInDelta, 
												bLoop, TRUE, DEFAULT, StartPhase)
							menuWeapon[TOR_CLIP1].strLastPlayedDict 	= sAnimData.strAnimDict
							menuWeapon[TOR_CLIP1].strLastPlayedAnim		= strAnimExtra
							menuWeapon[TOR_CLIP1].bHasPlayedAnim 		= TRUE
						ENDIF
					
					ENDIF
				
				ENDIF
			
			ENDIF
			
	// <<<<<<<<<< EXTRA ANIMATED WEAPONS >>>>>>>>>>>>>>
	// this is for wepaon pickups, all the other torture props are moved out of the way of the weapon pickup currently playing
	
			IF eScene = SCENE_WEAPON_SELECT_IDLE
		
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerWrench)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_WRENCH].objObject)
						DETACH_ENTITY(menuWeapon[TOR_WRENCH].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_WRENCH], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_WRENCH].objObject,
							sAnimData.strBenchPositionerWrench, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_WRENCH].strLastPlayedDict	= "missfbi3_weapon_select"
					menuWeapon[TOR_WRENCH].strLastPlayedAnim	= sAnimData.strBenchPositionerWrench
					menuWeapon[TOR_WRENCH].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_WRENCH].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerPliers)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_PLIERS].objObject)
						DETACH_ENTITY(menuWeapon[TOR_PLIERS].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_PLIERS], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_PLIERS].objObject,
							sAnimData.strBenchPositionerPliers, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_PLIERS].strLastPlayedDict	= "missfbi3_weapon_select"
					menuWeapon[TOR_PLIERS].strLastPlayedAnim	= sAnimData.strBenchPositionerPliers
					menuWeapon[TOR_PLIERS].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_PLIERS].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerClip0)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP0].objObject)
						DETACH_ENTITY(menuWeapon[TOR_CLIP0].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_CLIP0], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_CLIP0].objObject,
							sAnimData.strBenchPositionerClip0, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_CLIP0].strLastPlayedDict	= "missfbi3_weapon_select"
					menuWeapon[TOR_CLIP0].strLastPlayedAnim	= sAnimData.strBenchPositionerClip0
					menuWeapon[TOR_CLIP0].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_CLIP0].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerClip1)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP1].objObject)
						DETACH_ENTITY(menuWeapon[TOR_CLIP1].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_CLIP1], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_CLIP1].objObject,
							sAnimData.strBenchPositionerClip1, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_CLIP1].strLastPlayedDict	= "missfbi3_weapon_select"
					menuWeapon[TOR_CLIP1].strLastPlayedAnim	= sAnimData.strBenchPositionerClip1
					menuWeapon[TOR_CLIP1].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_CLIP1].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerJerrycan)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_WATER].objObject)
						DETACH_ENTITY(menuWeapon[TOR_WATER].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_WATER], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_WATER].objObject,
							sAnimData.strBenchPositionerJerrycan, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_WATER].strLastPlayedDict		= "missfbi3_weapon_select"
					menuWeapon[TOR_WATER].strLastPlayedAnim		= sAnimData.strBenchPositionerJerrycan
					menuWeapon[TOR_WATER].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_WATER].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerRag)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_RAG].objObject)
						DETACH_ENTITY(menuWeapon[TOR_RAG].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_RAG], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_RAG].objObject,
							sAnimData.strBenchPositionerRag, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_RAG].strLastPlayedDict		= "missfbi3_weapon_select"
					menuWeapon[TOR_RAG].strLastPlayedAnim		= sAnimData.strBenchPositionerRag
					menuWeapon[TOR_RAG].bHasPlayedAnim 			= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_RAG].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerSyringe)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_SYRINGE].objObject)
						DETACH_ENTITY(menuWeapon[TOR_SYRINGE].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_SYRINGE], INSTANT_BLEND_OUT)
					
					PLAY_ENTITY_ANIM(menuWeapon[TOR_SYRINGE].objObject,
							sAnimData.strBenchPositionerSyringe, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
					
					menuWeapon[TOR_SYRINGE].strLastPlayedDict		= "missfbi3_weapon_select"
					menuWeapon[TOR_SYRINGE].strLastPlayedAnim		= sAnimData.strBenchPositionerSyringe
					menuWeapon[TOR_SYRINGE].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_SYRINGE].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerTrolley)

					IF IS_ENTITY_ATTACHED(objTable)
						DETACH_ENTITY(objTable)
					ENDIF
					
					PLAY_ENTITY_ANIM(objTable,
							sAnimData.strBenchPositionerTrolley, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTable)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerBattery)

					IF IS_ENTITY_ATTACHED(objBattery)
						DETACH_ENTITY(objBattery)
					ENDIF
					
					PLAY_ENTITY_ANIM(objBattery,
							sAnimData.strBenchPositionerBattery, 
							"missfbi3_weapon_select",
							BlendInDelta, TRUE, TRUE)
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBattery)
				ENDIF

			ELSE
	
				STRING strWeaponPosDic
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strWeaponSelectionOverride)
					strWeaponPosDic = sAnimData.strWeaponSelectionOverride
				ELSE
					strWeaponPosDic = "missfbi3_weapon_select_trolley"
				ENDIF

				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerWrench)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_WRENCH].objObject)
						DETACH_ENTITY(menuWeapon[TOR_WRENCH].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_WRENCH], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_WRENCH].objObject,
							syncScene[eScene], 
							sAnimData.strBenchPositionerWrench, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					menuWeapon[TOR_WRENCH].strLastPlayedDict	= ""
					menuWeapon[TOR_WRENCH].strLastPlayedAnim	= ""
					menuWeapon[TOR_WRENCH].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_WRENCH].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerClip0)
					
					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP0].objObject)
						DETACH_ENTITY(menuWeapon[TOR_CLIP0].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_CLIP0], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_CLIP0].objObject, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerClip0, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
							
					menuWeapon[TOR_CLIP0].strLastPlayedDict	= ""
					menuWeapon[TOR_CLIP0].strLastPlayedAnim	= ""
					menuWeapon[TOR_CLIP0].bHasPlayedAnim 	= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_CLIP0].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerClip1)
					
					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP1].objObject)
						DETACH_ENTITY(menuWeapon[TOR_CLIP1].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_CLIP1], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_CLIP1].objObject, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerClip1, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					menuWeapon[TOR_CLIP1].strLastPlayedDict	= ""
					menuWeapon[TOR_CLIP1].strLastPlayedAnim	= ""
					menuWeapon[TOR_CLIP1].bHasPlayedAnim 	= TRUE
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_CLIP1].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerPliers)
					
					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_PLIERS].objObject)
						DETACH_ENTITY(menuWeapon[TOR_PLIERS].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_PLIERS], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_PLIERS].objObject, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerPliers, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					menuWeapon[TOR_PLIERS].strLastPlayedDict	= ""
					menuWeapon[TOR_PLIERS].strLastPlayedAnim	= ""
					menuWeapon[TOR_PLIERS].bHasPlayedAnim 		= TRUE
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_PLIERS].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerJerrycan)
					
					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_WATER].objObject)
						DETACH_ENTITY(menuWeapon[TOR_WATER].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_WATER], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_WATER].objObject, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerJerrycan, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					menuWeapon[TOR_WATER].strLastPlayedDict	= ""
					menuWeapon[TOR_WATER].strLastPlayedAnim	= ""
					menuWeapon[TOR_WATER].bHasPlayedAnim = TRUE
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_WATER].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerSyringe)

					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_SYRINGE].objObject)
						DETACH_ENTITY(menuWeapon[TOR_SYRINGE].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_SYRINGE], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_SYRINGE].objObject,
							syncScene[eScene], 
							sAnimData.strBenchPositionerSyringe, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
							
					menuWeapon[TOR_SYRINGE].strLastPlayedDict	= ""
					menuWeapon[TOR_SYRINGE].strLastPlayedAnim	= ""
					menuWeapon[TOR_SYRINGE].bHasPlayedAnim 		= TRUE
							
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_SYRINGE].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerRag)
					
					IF IS_ENTITY_ATTACHED(menuWeapon[TOR_RAG].objObject)
						DETACH_ENTITY(menuWeapon[TOR_RAG].objObject)
					ENDIF
					
					TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_RAG], INSTANT_BLEND_OUT)
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_RAG].objObject, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerRag, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					menuWeapon[TOR_RAG].strLastPlayedDict	= ""
					menuWeapon[TOR_RAG].strLastPlayedAnim	= ""
					menuWeapon[TOR_RAG].bHasPlayedAnim = TRUE
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_RAG].objObject)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerTrolley)
					
					IF IS_ENTITY_ATTACHED(objTable)
						DETACH_ENTITY(objTable)
					ENDIF
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objTable, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerTrolley, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTable)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strBenchPositionerBattery)
		
					IF IS_ENTITY_ATTACHED(objBattery)
						DETACH_ENTITY(objBattery)
					ENDIF
				
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objBattery, 
							syncScene[eScene], 
							sAnimData.strBenchPositionerBattery, 
							strWeaponPosDic,
							BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBattery)
				ENDIF
				
			ENDIF
			
	// <<<<<<<<<<<<<< Agent Steve >>>>>>>>>>>>>>>
	//---------------------------------------------------------
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strSteve)
				TASK_SYNCHRONIZED_SCENE(pedSteve, syncScene[eScene], sAnimData.strAnimDict, sAnimData.strSteve, BlendInDelta, BlendOutDelta)
			ELSE
				IF NOT IS_ENTITY_DEAD(pedSteve)
				AND GET_SCRIPT_TASK_STATUS(pedSteve, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
					CLEAR_PED_TASKS_IMMEDIATELY(pedSteve)
					SET_ENTITY_COORDS(pedSteve, vSteveCoords)
					SET_ENTITY_HEADING(pedSteve, fSteveHeading)
					OPEN_SEQUENCE_TASK(seqSequence)
						TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_a", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_a", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_b", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_a", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_b", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
						SET_SEQUENCE_TO_REPEAT(seqSequence, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seqSequence)
					TASK_PERFORM_SEQUENCE(pedSteve, seqSequence)
					CLEAR_SEQUENCE_TASK(seqSequence)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(propPhoneSteve)
				IF NOT IS_ENTITY_ATTACHED(propPhoneSteve)
					ATTACH_ENTITY_TO_ENTITY(propPhoneSteve, pedSteve, GET_PED_BONE_INDEX(pedSteve, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
				ENDIF
			ENDIF
			
	// <<<<<<<<<<<<<< Camera >>>>>>>>>>>>>>>
	//---------------------------------------------------------
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strCam)
					
				IF NOT DOES_CAM_EXIST(camTorture)
					camTorture = CREATE_CAMERA(CAMTYPE_ANIMATED)	
					SET_CAM_DEBUG_NAME(camTorture, "Torture Cam")
				ELSE
					STOP_CAM_SHAKING(camTorture, TRUE)
				ENDIF

				SET_CAM_ACTIVE(camTorture, TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(camTorture, syncScene[eScene], sAnimData.strCam, sAnimData.strAnimDict)
				
				// added bug 1890635
				IF NOT IS_SCRIPT_GLOBAL_SHAKING()
					ANIMATED_SHAKE_SCRIPT_GLOBAL("SHAKE_CAM_medium", "medium", "", fTortureShake)
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF

	// <<<<<<<<<<<<<< Rag >>>>>>>>>>>>>>>
	//---------------------------------------------------------
	CPRINTLN(DEBUG_MIKE, "	Process Rag")
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimData.strRag)
			
				CPRINTLN(DEBUG_MIKE, "		Rag(MAIN) anim found, detach rag and play animation")

				IF IS_ENTITY_ATTACHED(menuWeapon[TOR_RAG].objObject)
					DETACH_ENTITY(menuWeapon[TOR_RAG].objObject, FALSE)
				ENDIF
				
				TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_RAG], BlendOutDelta)
				
				// If being picked up for the water torture then use the anim dicitonary obtained from the scene ANIM DATA information
				IF iCurrentMenuWeapon = enum_to_int(TOR_WATER)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_RAG].objObject, syncScene[eScene], sAnimData.strRag, sAnimData.strAnimDict, BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))	
				
				// Otherwise play using the weapon position dictionary.
				ELSE
					PLAY_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[TOR_RAG].objObject, syncScene[eScene], sAnimData.strRag, "missfbi3_weapon_select_trolley", BlendInDelta, BlendOutdelta, enum_to_int(PlaybackFlagsTorture | SYNCED_SCENE_LOOP_WITHIN_SCENE))	
					
				ENDIF
				menuWeapon[TOR_RAG].bHasPlayedAnim = TRUE
				
				IF sAnimData.bForceUpdateDueToCut
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_RAG].objObject)
				ENDIF

			ENDIF
			
			//SET_SYNCHRONIZED_SCENE_PHASE(syncScene[eScene], StartPhase)
			SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[eScene], bLoop)
			IF sAnimData.bSeparateSynSceneMrK
				SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_EXTRA_MR_K_FIDGET], TRUE)	
			ENDIF
			
			IF sAnimData.bForceUpdateDueToCut
			OR BlendInDelta = INSTANT_BLEND_IN
				CPRINTLN(DEBUG_MIKE, "\n	Force Updates")
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim Trev")
				FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV_PED_ID())
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim MrK")
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim Steve")
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSteve)	
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim Chair")
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objChair)
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim Weapon")
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[iCurrentMenuWeapon].objObject)
				IF iCurrentMenuWeapon = enum_to_int(TOR_CLIP0)
					CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim Clip1")
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_CLIP1].objObject)
				ENDIF
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim TapeArmLeft")
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTapeArmLeft)				
				CPRINTLN(DEBUG_MIKE, "		ForceUpdateAIAnim TapeArmRight")
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTapeArmRight)
				
				bPlayedTortureSceneThisFrame = TRUE
			ENDIF
			
			CPRINTLN(DEBUG_MIKE, "[*** END ***]\n")

			eLastTorturePlayed = eScene
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE: Makes Steve do a shocked reaction animation
PROC TORTURE_PlaySteveReaction()
	IF NOT IS_ENTITY_PLAYING_ANIM(pedSteve, adSteve, "steve_phone_reaction")
		CLEAR_PED_TASKS_IMMEDIATELY(pedSteve)
		SET_ENTITY_COORDS(pedSteve, vSteveCoords)
		SET_ENTITY_HEADING(pedSteve, fSteveHeading)
		OPEN_SEQUENCE_TASK(seqSequence)
			TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_reaction", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_a", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
			TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_a", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
			TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_b", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
			TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_a", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
			TASK_PLAY_ANIM_ADVANCED(NULL, adSteve, "steve_phone_idle_b", vSteveCoords, <<0,0,fSteveHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS)
			SET_SEQUENCE_TO_REPEAT(seqSequence, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(seqSequence)
		TASK_PERFORM_SEQUENCE(pedSteve, seqSequence)
		CLEAR_SEQUENCE_TASK(seqSequence)
	ENDIF
ENDPROC

PROC TORTURE_InitWeapSelectionCam()
	setupSwayCam(sSwayTorture, << 142.3567, -2202.3789, 6.6759 >>, << -41.1685, -9.3446, -125.0095 >>, 29.0273, 
			<< 142.2558, -2202.5918, 6.7048 >>, << -42.4644, -9.3446, -118.1968 >>, 29.0273, 10000, TRUE, shakeRoadVib, 0.1, "WEAP_SEL")
ENDPROC

//PURPOSE: Starts preloading the elements required for the torture scene
PROC TORTURE_LoadAssets(BOOL bAssetsForPeds, BOOL bAssetsForWeapons)

	IF bAssetsForPeds
		Load_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_STEVE))
		Load_Asset_Model(sAssetData, mnVictim)
		Load_Asset_Model(sAssetData, PROP_NPC_PHONE)
		Load_Asset_AnimDict(sAssetData, adSteve)
	ENDIF
		
	IF bAssetsForWeapons
		Load_Asset_Model(sAssetData, mnSmallTable)
		Load_Asset_Model(sAssetData, mnBench)
		Load_Asset_Model(sAssetData, mnWrench)
		Load_Asset_Model(sAssetData, mnPliers)
		Load_Asset_Model(sAssetData, mnBattery)
		Load_Asset_Model(sAssetData, mnClip0)
		Load_Asset_Model(sAssetData, mnClip1)
		Load_Asset_Model(sAssetData, mnSyringe)
		Load_Asset_Model(sAssetData, mnWater)
		Load_Asset_Model(sAssetData, mnRag)
		Load_Asset_Model(sAssetData, mnChair)
		Load_Asset_Model(sAssetData, mnMonitor)
		Load_Asset_Model(sAssetData, mnArmTape)
	ENDIF
	
	Load_Asset_AnimDict(sAssetData, "missfbi3_weapon_select")
	Load_Asset_AnimDict(sAssetData, "missfbi3_weapon_select_trolley")
	Load_Asset_AnimDict(sAssetData, "SHAKE_CAM_medium")
	Load_Asset_AnimDict(sAssetData, adSteve)
	Load_Asset_Scaleform(sAssetData, sfHeart, sfHeartMonitor)
	Load_Asset_Scaleform(sAssetData, sfTooth, sfToothPull)
	Load_Asset_Model(sAssetData, mnUVFaceProp)
	
	Load_Asset_Audiobank(sAssetData, sbTortureGeneral)
	Load_Asset_Audiobank(sAssetData, "FBI_03_Torture_Chair")
	
	Load_Asset_PTFX(sAssetData)
ENDPROC

//PURPOSE: Returns the menu object, created based on the passed information
PROC TORTURE_InitialiseMenuObject(sMenuObject &MenuObject, VECTOR DefaultCoords, VECTOR DefaultRot, 
									VECTOR AttachOffset, VECTOR AttachRot, BOOL Selectable, STRING csName, MODEL_NAMES modelName)

	MenuObject.vDefaultCoords 			= DefaultCoords
	MenuObject.vDefaultRot 				= DefaultRot
	MenuObject.vAttachOffset 			= AttachOffset
	MenuObject.vAttachRot 				= AttachRot
	MenuObject.bHasPlayedAnim 			= FALSE
	MenuObject.bSelectable 				= Selectable
	MenuObject.strCSName 				= csName
	MenuObject.model					= modelName

ENDPROC

PROC TORTURE_DetachAllProps()
	INT iCounter
	FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1
		IF DOES_ENTITY_EXIST(menuWeapon[iCounter].objObject)
//			IF menuWeapon[iCounter].bHasPlayedAnim
//				STOP_SYNCHRONIZED_ENTITY_ANIM(menuWeapon[iCounter].objObject, INSTANT_BLEND_OUT, FALSE)
//				menuWeapon[iCounter].bHasPlayedAnim = FALSE
//			ENDIF
			IF IS_ENTITY_ATTACHED(menuWeapon[iCounter].objObject)
				DETACH_ENTITY(menuWeapon[iCounter].objObject, FALSE)
			ENDIF
			SET_ENTITY_COLLISION(menuWeapon[iCounter].objObject, FALSE)
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(propPhoneSteve)
		IF IS_ENTITY_ATTACHED(propPhoneSteve)
			DETACH_ENTITY(propPhoneSteve)
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Detaches Torture Props
PROC TORTURE_ResetAllProps(BOOL bPostCutscene)
	
	CDEBUG2LN(DEBUG_MIKE, "TORTURE_ResetAllProps(bPostCutscene = ", bPostCutscene, ")")

	INT iCounter
	FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1
		IF DOES_ENTITY_EXIST(menuWeapon[iCounter].objObject)
		AND (NOT bPostCutscene OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(menuWeapon[iCounter].strCSName))
		AND NOT IS_VECTOR_ZERO(menuWeapon[iCounter].vDefaultCoords)
		AND NOT IS_VECTOR_ZERO(menuWeapon[iCounter].vDefaultRot)
			CPRINTLN(DEBUG_MIKE, "**** FBI3 - Respositioning weapon: ", menuWeapon[iCounter].strCSName)

			IF IS_ENTITY_ATTACHED(menuWeapon[iCounter].objObject)
				DETACH_ENTITY(menuWeapon[iCounter].objObject, FALSE)
				CPRINTLN(DEBUG_MIKE, "	Detached weapon")
			ENDIF
			SET_ENTITY_COLLISION(menuWeapon[iCounter].objObject, FALSE)
			SET_ENTITY_COORDS_NO_OFFSET(menuWeapon[iCounter].objObject, menuWeapon[iCounter].vDefaultCoords)
			SET_ENTITY_ROTATION(menuWeapon[iCounter].objObject, menuWeapon[iCounter].vDefaultRot)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[iCounter].objObject)
			CPRINTLN(DEBUG_MIKE, "	Set weapon pos: ", menuWeapon[iCounter].vDefaultCoords)
			CPRINTLN(DEBUG_MIKE, "	Set weapon rot: ", menuWeapon[iCounter].vDefaultRot)
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(objChair)
	AND (NOT bPostCutscene OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csChair))
		SET_ENTITY_COORDS_NO_OFFSET(objChair, vChairCoords)
		SET_ENTITY_ROTATION(objChair, vChairRot)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objChair)
		bHasChairPlayedAnim = FALSE
		CDEBUG2LN(DEBUG_MIKE, "Postitioned: ", csChair)
	ENDIF
	IF DOES_ENTITY_EXIST(objTable)
	AND (NOT bPostCutscene OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csTable))
	AND NOT IS_VECTOR_ZERO(vTableCoords)
	AND NOT IS_VECTOR_ZERO(vTableRot)
		SET_ENTITY_COORDS_NO_OFFSET(objTable, vTableCoords)
		SET_ENTITY_ROTATION(objTable, vTableRot)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTable)
		CDEBUG2LN(DEBUG_MIKE, "Postitioned: ", csTable)
	ENDIF
	IF DOES_ENTITY_EXIST(objMonitor)
	AND (NOT bPostCutscene OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csMonitor))
		SET_ENTITY_COORDS_NO_OFFSET(objMonitor, vMonitorCoords)
		SET_ENTITY_ROTATION(objMonitor, vMonitorRot)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objMonitor)
		CDEBUG2LN(DEBUG_MIKE, "Postitioned: ", csMonitor)
	ENDIF
	IF DOES_ENTITY_EXIST(objBattery)
	AND (NOT bPostCutscene OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csBattery))
	AND NOT IS_VECTOR_ZERO(vBatteryCoords)
	AND NOT IS_VECTOR_ZERO(vBatteryRot)
		SET_ENTITY_COORDS_NO_OFFSET(objBattery, vBatteryCoords)
		SET_ENTITY_ROTATION(objBattery, vBatteryRot)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBattery)
		CDEBUG2LN(DEBUG_MIKE, "Postitioned: ", csBattery)
	ENDIF
	IF DOES_ENTITY_EXIST(objSmallTable)
	AND (NOT bPostCutscene OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csSmallTable))
		SET_ENTITY_COORDS_NO_OFFSET(objSmallTable, vSmallTableCoords)
		SET_ENTITY_ROTATION(objSmallTable, vSmallTableRot)
		FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objSmallTable)
		CDEBUG2LN(DEBUG_MIKE, "Postitioned: ", csSmallTable)
	ENDIF
	
ENDPROC

/// PURPOSE: Loads in and creates the torture scene elements
FUNC BOOL TORTURE_IsTortureReady(BOOL bCreatePeds, BOOL bCreateWeapons, BOOL bCreateVehicle)

	BOOL bIsReady = TRUE
	BOOL bCreatedEntityThisFrame
	
	IF bCreateVehicle
		IF NOT DOES_ENTITY_EXIST(vehTrevor)
			IF HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR))
				IF NOT bCreatedEntityThisFrame
					IF CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, vTrevorCarDriveToAirportCoord, fTrevorCarSpawnHeading)
						SET_VEHICLE_ENGINE_ON(vehTrevor, TRUE, TRUE)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTrevor, SC_DOOR_FRONT_LEFT, FALSE)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTrevor, SC_DOOR_FRONT_RIGHT, FALSE)
						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Trevor truck created")
						bCreatedEntityThisFrame = TRUE
					ELSE
//						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Vehicle NOT created yet")
						bIsReady = FALSE
					ENDIF
				ELSE
//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Vehicle NOT created yet")
					bIsReady = FALSE
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Vehicle waiting for next frame, entity already created this frame")
				bIsReady = FALSE
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objs[mof_turd])
			IF HAS_MODEL_LOADED(mnTrevTurd)
				IF NOT bCreatedEntityThisFrame
					objs[mof_turd] = CREATE_OBJECT(mnTrevTurd, vTrevTurd)
					Unload_Asset_Model(sAssetData, mnTrevTurd)
					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Trevor's Turd CREATED")
					bCreatedEntityThisFrame = TRUE
				ELSE
//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() TURD waiting for next frame, entity already created this frame")
					bIsReady = FALSE
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Trevor's Turd NOT LOADED")
				bIsReady = FALSE
			ENDIF
		ENDIF
	ENDIF

	IF bCreatePeds
		IF NOT DOES_ENTITY_EXIST(pedSteve)
			IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_STEVE))
			AND HAS_ANIM_DICT_LOADED(adSteve)
				IF NOT bCreatedEntityThisFrame
					IF CREATE_NPC_PED_ON_FOOT(pedSteve, CHAR_STEVE, vSteveCoords, fSteveHeading, FALSE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedSteve, TRUE)
						SET_PED_COMPONENT_VARIATION(pedSteve, PED_COMP_TORSO, 0, 1)
						SET_PED_COMPONENT_VARIATION(pedSteve, PED_COMP_LEG, 0, 1)
						Unload_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_STEVE))
						SET_PED_CREW(pedSteve)
						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Steve created")
						bCreatedEntityThisFrame = TRUE
					ELSE
//						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Steve not yet created")
						bIsReady = FALSE
					ENDIF
				ELSE
//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Steve waiting for next frame, entity already created this frame")
					bIsReady = FALSE
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Steve model/animation NOT YET LOADED")
				bIsReady = FALSE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedSteve)
			IF NOT DOES_ENTITY_EXIST(propPhoneSteve)
				IF HAS_MODEL_LOADED(PROP_NPC_PHONE)
					IF NOT bCreatedEntityThisFrame
						propPhoneSteve = CREATE_OBJECT(PROP_NPC_PHONE, GET_PED_BONE_COORDS(pedSteve, BONETAG_PH_L_HAND, <<0,0,0>>))
						Unload_Asset_Model(sAssetData, PROP_NPC_PHONE)
						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Phone created")
						bCreatedEntityThisFrame = TRUE
					ELSE
//						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Phone waiting for next frame, entity already created this frame")
						bIsReady = FALSE
					ENDIF
				ELSE
//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Phone NOT YET LOADED")
					bIsReady = FALSE
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(propPhoneSteve)
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(propPhoneSteve, pedSteve)
					IF IS_ENTITY_ATTACHED(propPhoneSteve)
						DETACH_ENTITY(propPhoneSteve, FALSE)
					ENDIF
					
					// Preventing 'Check if entity is alive' assert
					IF NOT IS_ENTITY_DEAD(pedSteve)
						ATTACH_ENTITY_TO_ENTITY(propPhoneSteve, pedSteve, GET_PED_BONE_INDEX(pedSteve, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, FALSE, FALSE)
					ENDIF
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Phone doesn't exist")
				bIsReady = FALSE
			ENDIF
		ENDIF
		
		
		IF NOT DOES_ENTITY_EXIST(pedVictim)
			IF HAS_MODEL_LOADED(mnVictim)
				IF NOT bCreatedEntityThisFrame
					pedVictim = CREATE_PED(PEDTYPE_MISSION, mnVictim, <<143.5551, -2201.5913, 3.6880>>, 261.2970)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedVictim, TRUE)
					Unload_Asset_Model(sAssetData, mnVictim)
					RESET_MR_K_VARIATIONS() // sets Mr K's variations to standard
					MANAGE_MR_K_VARIATIONS()
					SET_PED_CREW(pedVictim)
					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Mr K created")
					bCreatedEntityThisFrame = TRUE
				ELSE
//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() MrK waiting for next frame, entity already created this frame")
					bIsReady = FALSE
				ENDIF
			ELSE
//				CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Mr K NOT LOADED")
				bIsReady = FALSE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedVictim)
		AND NOT IS_PED_INJURED(pedVictim)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedVictim)
//				CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Mr K STREAMING REQUESTS NOT COMPLETED")
				bIsReady = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	
	// Initialise torture weapon positions
	IF HAS_ANIM_DICT_LOADED("missfbi3_weapon_select")
	
		VECTOR vDefaultPos
		VECTOR vDefaultRot
		TORTURE_ANIM_DATA sAnimData
		
		IF TORTURE_GetTortureSceneAnimData(SCENE_WEAPON_SELECT_IDLE, sAnimData)
			IF menuWeapon[TOR_WRENCH].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerWrench, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerWrench, vTortureScene, vTortureRot)
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_WRENCH],	
							vDefaultPos,
							vDefaultRot, 
							VECTOR_ZERO, VECTOR_ZERO, TRUE, "TORTURE_Wrench", mnWrench)	
			ENDIF
			IF menuWeapon[TOR_PLIERS].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerPliers, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerPliers, vTortureScene, vTortureRot)
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_PLIERS],
							vDefaultPos,
							vDefaultRot,
							VECTOR_ZERO, VECTOR_ZERO, TRUE, "TORTURE_Pliers", mnPliers)
			ENDIF
			IF menuWeapon[TOR_CLIP0].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerClip0, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerClip0, vTortureScene, vTortureRot)	
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_CLIP0],
							vDefaultPos,
							vDefaultRot, 
							VECTOR_ZERO, VECTOR_ZERO, TRUE, "TORTURE_Battery_clip_right", mnClip0)
			ENDIF
			IF menuWeapon[TOR_CLIP1].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerClip1, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerClip1, vTortureScene, vTortureRot)
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_CLIP1],
							vDefaultPos,
							vDefaultRot,
							VECTOR_ZERO, VECTOR_ZERO, FALSE, "TORTURE_Battery_clip_left", mnClip1)
			ENDIF
			IF menuWeapon[TOR_WATER].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerJerrycan, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerJerrycan, vTortureScene, vTortureRot)
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_WATER],
							vDefaultPos,
							vDefaultRot,
							VECTOR_ZERO, VECTOR_ZERO, TRUE, "TORTURE_Jerrycan", mnWater)
			ENDIF
			IF menuWeapon[TOR_RAG].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerRag, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerRag, vTortureScene, vTortureRot)
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_RAG],
							vDefaultPos,
							vDefaultRot,
							VECTOR_ZERO, VECTOR_ZERO, FALSE, "TORTURE_rag", mnRag)
			ENDIF
			IF menuWeapon[TOR_SYRINGE].model = DUMMY_MODEL_FOR_SCRIPT
				vDefaultPos = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", sAnimData.strBenchPositionerSyringe, vTortureScene, vTortureRot)
				vDefaultRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", sAnimData.strBenchPositionerSyringe, vTortureScene, vTortureRot)
				TORTURE_InitialiseMenuObject(menuWeapon[TOR_SYRINGE],
							vDefaultPos,
							vDefaultRot,
							VECTOR_ZERO, VECTOR_ZERO, FALSE, "TORTURE_Syringe", mnSyringe)
			ENDIF
			
			IF IS_VECTOR_ZERO(vTableCoords)
			OR IS_VECTOR_ZERO(vTableRot)
				vTableCoords = GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", 		sAnimData.strBenchPositionerTrolley, 	vTortureScene, vTortureRot)
				vTableRot	= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", 		sAnimData.strBenchPositionerTrolley, 	vTortureScene, vTortureRot)
			ENDIF
			
			IF IS_VECTOR_ZERO(vBatteryCoords)
			OR IS_VECTOR_ZERO(vBatteryRot)
				vBatteryCoords 	= GET_ANIM_INITIAL_OFFSET_POSITION("missfbi3_weapon_select", 	sAnimData.strBenchPositionerBattery,	vTortureScene, 	vTortureRot)
				vBatteryRot		= GET_ANIM_INITIAL_OFFSET_ROTATION("missfbi3_weapon_select", 	sAnimData.strBenchPositionerBattery, 	vTortureScene, 	vTortureRot)
			ENDIF
			
			IF bCreateWeapons

				IF NOT DOES_ENTITY_EXIST(objTable)
					IF HAS_MODEL_LOADED(mnBench)
						IF NOT bCreatedEntityThisFrame
							objTable = CREATE_OBJECT(mnBench, vTableCoords)
							Unload_Asset_Model(sAssetData, mnBench)
							SET_ENTITY_COORDS_NO_OFFSET(objTable, vTableCoords)
							SET_ENTITY_ROTATION(objTable, vTableRot)
							SET_ENTITY_COLLISION(objTable, FALSE)
							FREEZE_ENTITY_POSITION(objTable, TRUE)
							bCreatedEntityThisFrame = TRUE
						ELSE
		//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() TortureTable waiting for next frame, entity already created this frame")
							bIsReady = FALSE
						ENDIF
					ELSE
						bIsReady = FALSE
					ENDIF
				ENDIF

				IF NOT DOES_ENTITY_EXIST(objSmallTable)
					IF HAS_MODEL_LOADED(mnSmallTable)
						IF NOT bCreatedEntityThisFrame
							objSmallTable = CREATE_OBJECT(mnSmallTable, vSmallTableCoords)
							Unload_Asset_Model(sAssetData, mnSmallTable)
							SET_ENTITY_COORDS_NO_OFFSET(objSmallTable, vSmallTableCoords)
							SET_ENTITY_ROTATION(objSmallTable, vSmallTableRot)
							FREEZE_ENTITY_POSITION(objSmallTable, TRUE)
							SET_ENTITY_COLLISION(objSmallTable, FALSE)
							bCreatedEntityThisFrame = TRUE
						ELSE
		//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() TortureStool waiting for next frame, entity already created this frame")
							bIsReady = FALSE
						ENDIF
					ELSE
						bIsReady = FALSE
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(objBattery)
					IF HAS_MODEL_LOADED(mnBattery)
						IF NOT bCreatedEntityThisFrame
							objBattery = CREATE_OBJECT(mnBattery, vBatteryCoords)
							Unload_Asset_Model(sAssetData, mnBattery)
							SET_ENTITY_COORDS_NO_OFFSET(objBattery, vBatteryCoords)
							SET_ENTITY_ROTATION(objBattery, vBatteryRot)
							SET_ENTITY_COLLISION(objBattery, FALSE)
							FREEZE_ENTITY_POSITION(objBattery, TRUE)
							bCreatedEntityThisFrame = TRUE
						ELSE
		//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Battery waiting for next frame, entity already created this frame")
							bIsReady = FALSE
						ENDIF
					ELSE
						bIsReady = FALSE
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(objMonitor)
					IF HAS_MODEL_LOADED(mnMonitor)
						IF NOT bCreatedEntityThisFrame
							objMonitor = CREATE_OBJECT(mnMonitor, vMonitorCoords)
							Unload_Asset_Model(sAssetData, mnMonitor)
							SET_ENTITY_COORDS_NO_OFFSET(objMonitor, vMonitorCoords)
							SET_ENTITY_ROTATION(objMonitor, vMonitorRot)
							FREEZE_ENTITY_POSITION(objMonitor, TRUE)
							bCreatedEntityThisFrame = TRUE
						ELSE
		//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Monitor waiting for next frame, entity already created this frame")
							bIsReady = FALSE
						ENDIF
					ELSE
						bIsReady = FALSE
					ENDIF
				ENDIF
				
//				IF DOES_ENTITY_EXIST(objMonitor)
					IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
						REGISTER_NAMED_RENDERTARGET(sECGRenderTarget)
						LINK_NAMED_RENDERTARGET(mnMonitor)
						rtECG = GET_NAMED_RENDERTARGET_RENDER_ID(sECGRenderTarget)
					ENDIF
//				ELSE
//					bIsReady = FALSE
//				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(objChair)
					IF HAS_MODEL_LOADED(mnChair)
						IF NOT bCreatedEntityThisFrame
							objChair = CREATE_OBJECT(mnChair, vChairCoords)
							Unload_Asset_Model(sAssetData, mnChair)
							SET_ENTITY_COORDS_NO_OFFSET(objChair, vChairCoords)
							SET_ENTITY_ROTATION(objChair, vChairRot)
							SET_ENTITY_COLLISION(objChair, FALSE)
							FREEZE_ENTITY_POSITION(objChair, TRUE)
							bCreatedEntityThisFrame = TRUE
						ELSE
		//					CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Chair waiting for next frame, entity already created this frame")
							bIsReady = FALSE
						ENDIF
					ELSE
						bIsReady = FALSE
					ENDIF
				ENDIF
				
				// Chair has to exist for tape to be created
				IF DOES_ENTITY_EXIST(objChair)
				
					IF NOT DOES_ENTITY_EXIST(objTapeArmLeft)
						IF HAS_MODEL_LOADED(mnArmTape)
							IF NOT bCreatedEntityThisFrame
								objTapeArmLeft = CREATE_OBJECT(mnArmTape, vTortureScene + <<0.1,0,0>>)		
								bCreatedEntityThisFrame = TRUE
							ELSE
		//						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() TapeArmLeft waiting for next frame, entity already created this frame")
								bIsReady = FALSE
							ENDIF
						ELSE
				 			bIsReady = FALSE
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(objTapeArmRight)
						IF HAS_MODEL_LOADED(mnArmTape)
							IF NOT bCreatedEntityThisFrame 
								objTapeArmRight = CREATE_OBJECT(mnArmTape, vTortureScene - <<0.1,0,0>>)
								bCreatedEntityThisFrame = TRUE
							ELSE
		//						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() TapeArmRight waiting for next frame, entity already created this frame")
								bIsReady = FALSE
							ENDIF
						ELSE
				 			bIsReady = FALSE
						ENDIF
					ENDIF

				ENDIF
				
				INT i
				REPEAT COUNT_OF(menuWeapon) i
					IF NOT DOES_ENTITY_EXIST(menuWeapon[i].objObject)
						IF HAS_MODEL_LOADED(menuWeapon[i].model)
							IF NOT bCreatedEntityThisFrame
								CPRINTLN(DEBUG_MIKE, "***** FBI3: Creating weapon: ", menuWeapon[i].strCSName)
								menuWeapon[i].objObject = CREATE_OBJECT(menuWeapon[i].model, menuWeapon[i].vDefaultCoords)
								SET_ENTITY_COORDS_NO_OFFSET(menuWeapon[i].objObject, menuWeapon[i].vDefaultCoords)
								SET_ENTITY_ROTATION(menuWeapon[i].objObject, menuWeapon[i].vDefaultRot)
								SET_ENTITY_COLLISION(menuWeapon[i].objObject, FALSE)
								CPRINTLN(DEBUG_MIKE, "	Set weapon pos: ", menuWeapon[i].vDefaultCoords)
								CPRINTLN(DEBUG_MIKE, "	Set weapon rot: ", menuWeapon[i].vDefaultRot)
								bCreatedEntityThisFrame = TRUE
							ELSE
		//						CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Weapon:", menuWeapon[i].strCSName, " waiting for next frame, entity already created this frame")
								bIsReady = FALSE
							ENDIF
						ELSE
							bIsReady = FALSE
						ENDIF
					ENDIF
				ENDREPEAT
				
			ENDIF
			
		ELSE
			bIsReady = FALSE
			CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() defeault positions unable to initialise")
		ENDIF
	
	ELSE
		bIsReady = FALSE
		CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() missfbi3_weapon_select NOT LOADED and object defeault positions not initialised")
	ENDIF

	
	IF NOT HAS_ANIM_DICT_LOADED("missfbi3_weapon_select")
	OR NOT HAS_ANIM_DICT_LOADED("missfbi3_weapon_select_trolley")
	OR NOT HAS_ANIM_DICT_LOADED(adSteve)
	OR NOT HAS_ANIM_DICT_LOADED("SHAKE_CAM_medium")
		bIsReady = FALSE
//		CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Dictionaries NOT LOADED")
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sfHeartMonitor)
	OR NOT HAS_SCALEFORM_MOVIE_LOADED(sfToothPull)
		bIsReady = FALSE
//		CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Scaleform NOT LOADED")
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK(sbTortureGeneral)
		bIsReady = FALSE
//		CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() Audiobank: General NOT LOADED")
	ENDIF
	
	IF NOT HAS_PTFX_ASSET_LOADED()	
		bIsReady = FALSE
//		CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady() PTFX NOT LOADED")
	ENDIF
	
	CDEBUG2LN(DEBUG_MIKE, "TORTURE_IsTortureReady(",bCreatePeds,",",bCreateWeapons,",",bCreateVehicle,") return: ", bIsReady)
	RETURN bIsReady
ENDFUNC

//PURPOSE: Cleans up the menu object bools, stops the synchronized scene animation and tidies up the object based on bImmediately
PROC TORTURE_CleanupWeapon(sMenuObject &thisObject, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(thisObject.objObject)
		TORTURE_StopTortureWeaponAnim(thisObject, INSTANT_BLEND_OUT)
		TidyUpObject(thisObject.objObject, bImmediately)
	ENDIF
	thisObject.bIsHighlighted = FALSE
	thisObject.bHasPlayedAnim = FALSE
ENDPROC

//PURPOSE: Release all the sound resources
PROC TORTURE_ReleaseAllSounds()
	ReleaseSound(SND_ELEC_HUM_LOOP)
	ReleaseSound(SND_ELEC_ZAP_LOOP)
	ReleaseSound(SND_ELEC_ATTACH_CLIP_LEFT)
	ReleaseSound(SND_ELEC_ATTACH_CLIP_RIGHT)
	ReleaseSound(SND_ELEC_SPARK)
	ReleaseSound(SND_TOOTH_PULL_LOOP)
	ReleaseSound(SND_TOOTH_PULL_OUT)
	ReleaseSound(SND_WATER_POUR_LOOP)
	ReleaseSound(SND_WATER_PICK_UP)
	ReleaseSound(SND_WRENCH_HIT)
ENDPROC

//PURPOSE: Cleans up the torture scene
PROC TORTURE_CleanupEntitiesAndAssets()

	IF DOES_ENTITY_EXIST(TREV_PED_ID())
	AND NOT IS_PED_INJURED(TREV_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(TREV_PED_ID())
			FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
		ENDIF
		SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
		// url:bugstar:2107753
		IF PLAYER_PED_ID() != TREV_PED_ID()
			SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE)
		ENDIF
	ENDIF
	
	Unload_Asset_Model(sAssetData, mnSmallTable)
	Unload_Asset_Model(sAssetData, mnBench)
	Unload_Asset_Model(sAssetData, mnMonitor)
	Unload_Asset_Model(sAssetData, mnChair)
	Unload_Asset_Model(sAssetData, mnBattery)
	Unload_Asset_Model(sAssetData, mnTooth)
	
	Unload_Asset_Model(sAssetData, mnWrench)
	Unload_Asset_Model(sAssetData, mnClip0)
	Unload_Asset_Model(sAssetData, mnClip1)
	Unload_Asset_Model(sAssetData, mnWater)
	Unload_Asset_Model(sAssetData, mnRag)
	Unload_Asset_Model(sAssetData, mnPliers)
	Unload_Asset_Model(sAssetData, mnSyringe)
	Unload_Asset_Model(sAssetData, mnArmTape)
	Unload_Asset_Model(sAssetData, mnUVFaceProp)
	
	Unload_Asset_Model(sAssetData, mnTrevTurd)

	//Unload_Asset_PTFX(sAssetData)
	Unload_Asset_Anim_Dict(sAssetData, adSteve)
	Unload_Asset_Anim_Dict(sAssetData, "missfbi3_weapon_select")
	Unload_Asset_Anim_Dict(sAssetData, "missfbi3_weapon_select_trolley")
	Unload_Asset_Anim_Dict(sAssetData, "SHAKE_CAM_medium")
	Unload_Asset_Scaleform(sAssetData, sfHeartMonitor)
	Unload_Asset_Scaleform(sAssetData, sfToothPull)
	Unload_Asset_Audio_Bank(sAssetData, sbTortureGeneral)
	Unload_Asset_Audio_Bank(sAssetData, sbTortureElec)
	Unload_Asset_Audio_Bank(sAssetData, sbTortureTeeth)
	Unload_Asset_Audio_Bank(sAssetData, "FBI_03_Torture_Teeth_Pain")
	Unload_Asset_Audio_Bank(sAssetData, sbTortureWater)
	Unload_Asset_Audio_Bank(sAssetData, sbTortureWrench)
	Unload_Asset_Audio_Bank(sAssetData, "FBI_03_Torture_Chair")
	
	Unload_Asset_Model(sAssetData, GET_NPC_PED_MODEL(CHAR_STEVE))
	Unload_Asset_Model(sAssetData, IG_MRK)
		
	// solves the 0 bmp problem with the scaleform.
	fCurrentSFBMP		= -1
	fCurrentSFHealth	= -1
	
	TORTURE_ReleaseAllSounds()
	
	IF IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
		RELEASE_NAMED_RENDERTARGET(sECGRenderTarget)
	ENDIF
	
	TidyUpObject(objTable, TRUE)
	TidyUpObject(objSmallTable, TRUE)
	TidyUpObject(objBattery, TRUE)
	TidyUpObject(objChair, TRUE)
	TidyUpObject(objMonitor, TRUE)
	TidyUpObject(objTooth, TRUE)
	TidyUpObject(objs[mof_turd], TRUE)
	TidyUpObject(objTapeArmLeft, TRUE)
	TidyUpObject(objTapeArmRight, TRUE)
	TidyUpObject(propPhoneSteve, TRUE)
	
	INT iCounter
	FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1
		TORTURE_CleanupWeapon(menuWeapon[iCounter], TRUE)
	ENDFOR
	TidyUpPed(pedSteve, TRUE)
	TidyUpPed(pedVictim, TRUE)
	TidyUpVehicle(vehTrevor, TRUE)
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	ENDIF
	
	bECGSetup = FALSE
ENDPROC

//PURPOSE: Deletes an existing prop and registers it for the cutscene for recreate it (all this to avoid a bloody assert!)
PROC TORTURE_RegisterPropForCutscene(OBJECT_INDEX &prop, STRING handle, MODEL_NAMES model, CUTSCENE_USAGE usage)
	IF usage = CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY
		IF DOES_ENTITY_EXIST(prop)
			//DELETE_ATTACHED_PROP(prop)
			REGISTER_ENTITY_FOR_CUTSCENE(prop, handle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, model)
		ELSE
			REGISTER_ENTITY_FOR_CUTSCENE(null, handle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, model)
		ENDIF
	ELIF usage = CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY
	OR usage = CU_ANIMATE_EXISTING_SCRIPT_ENTITY
		IF DOES_ENTITY_EXIST(prop)
			REGISTER_ENTITY_FOR_CUTSCENE(prop, handle, usage, model)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Registers Torture Props with the current loaded cutscene
PROC TORTURE_RegisterAllPropsForCutscene(BOOL bPropsNeededPostCutscene = TRUE, BOOL bDaveStartsWithPhone = FALSE)

	TEXT_LABEL_23 strPhoneHandle
	IF bDaveStartsWithPhone
		strPhoneHandle = "DaveNortons_Phone"
	ELSE
		strPhoneHandle = "SteveHains_Phone"
	ENDIF

	IF NOT bPropsNeededPostCutscene
		
		TORTURE_RegisterPropForCutscene(propPhoneSteve, strPhoneHandle, PROP_NPC_PHONE,	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		
		TORTURE_RegisterPropForCutscene(objTable, 		csTable, 		mnBench, 		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objSmallTable, 	csSmallTable, 	mnSmallTable, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objBattery, 	csBattery, 		mnBattery, 		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objChair, 		csChair, 		mnChair, 		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objMonitor, 	csMonitor, 		mnMonitor, 		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objTapeArmLeft,	csTapeArmLeft,	mnArmTape,		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objTapeArmRight,csTapeArmRight,	mnArmTape,		CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_WRENCH].objObject, 	csWrench, 	mnWrench, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_PLIERS].objObject, 	csPliers, 	mnPliers, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_CLIP0].objObject, 	csClip0, 	mnClip0, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_CLIP1].objObject, 	csClip1,	mnClip1, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_WATER].objObject, 	csWater, 	mnWater, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_RAG].objObject,		csRag,		mnRag, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_SYRINGE].objObject, 	csSyringe,	mnSyringe, 	CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)

	ELSE
		TORTURE_RegisterPropForCutscene(propPhoneSteve,	strPhoneHandle, PROP_NPC_PHONE,	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		
		TORTURE_RegisterPropForCutscene(objTable, 		csTable, 		mnBench, 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objSmallTable, 	csSmallTable, 	mnSmallTable, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objBattery, 	csBattery, 		mnBattery, 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objChair, 		csChair, 		mnChair, 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objMonitor, 	csMonitor, 		mnMonitor, 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objTapeArmLeft,	csTapeArmLeft,	mnArmTape,		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(objTapeArmRight,csTapeArmRight,	mnArmTape,		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
	
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_WRENCH].objObject, 	csWrench, 	mnWrench, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_PLIERS].objObject, 	csPliers, 	mnPliers, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_CLIP0].objObject, 	csClip0, 	mnClip0, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_CLIP1].objObject, 	csClip1,	mnClip1, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_WATER].objObject, 	csWater, 	mnWater, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_RAG].objObject,		csRag,		mnRag, 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
		TORTURE_RegisterPropForCutscene(menuWeapon[TOR_SYRINGE].objObject, 	csSyringe,	mnSyringe, 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY)
	ENDIF
ENDPROC

//PURPOSE: Attempts to grab a prop index from the cutscene
FUNC BOOL TORTURE_GetPropFromCutscene(OBJECT_INDEX &propId, STRING handle, MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT)
	ENTITY_INDEX entity
	IF NOT IS_CUTSCENE_PLAYING()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(propID)
		//CPRINTLN(DEBUG_MIKE, "\n		Attemping to grab prop: ", handle)
		entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(handle, model)
		IF DOES_ENTITY_EXIST(entity)
			propID = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(entity)
//			RETAIN_ENTITY_IN_INTERIOR(entity, interiorGarage)
			CDEBUG2LN(DEBUG_MIKE, "		Succesfully grabbed prop: ", handle)
			RETURN TRUE
		ELSE
			CDEBUG2LN(DEBUG_MIKE, "		Failed to grab prop: ", handle)
			RETURN FALSE
		ENDIF
	ELSE
		//CPRINTLN(DEBUG_MIKE, "		Already grabbed prop: ", handle)
		RETURN TRUE
	ENDIF
ENDFUNC

//PURPOSE: Grabs the indices of the torture props that have been created by the cutscene
FUNC BOOL TORTURE_GrabAllPropsFromCutscene(BOOL bDaveStartsWithPhone = FALSE)
	IF NOT IS_CUTSCENE_PLAYING()
		RETURN FALSE
	ENDIF
	
	//CPRINTLN(DEBUG_MIKE, "\n\n*****Attempting to Grab Torture Props From Cutscene*****")
	
	BOOL bFinished = TRUE
	
	IF bDaveStartsWithPhone
		IF NOT TORTURE_GetPropFromCutscene(propPhoneSteve,	"DaveNortons_Phone", PROP_NPC_PHONE)	bFinished = FALSE		ENDIF
	ELSE
		IF NOT TORTURE_GetPropFromCutscene(propPhoneSteve,	"SteveHains_Phone", PROP_NPC_PHONE)		bFinished = FALSE		ENDIF
	ENDIF
	
	IF NOT TORTURE_GetPropFromCutscene(objTable, 		csTable, 		mnBench)			bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(objSmallTable, 	csSmallTable, 	mnSmallTable)		bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(objChair,		csChair,		mnChair)			bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(objMonitor, 		csMonitor , 	mnMonitor)			
		bFinished = FALSE		
	ELSE
		IF DOES_ENTITY_EXIST(objMonitor)
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
				REGISTER_NAMED_RENDERTARGET(sECGRenderTarget)
				LINK_NAMED_RENDERTARGET(mnMonitor)
				rtECG = GET_NAMED_RENDERTARGET_RENDER_ID(sECGRenderTarget)
			ENDIF
		ELSE
			bFinished = FALSE
		ENDIF
	ENDIF
	IF NOT TORTURE_GetPropFromCutscene(objBattery, 		csBattery, 		mnBattery)			bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(objTapeArmLeft, 	csTapeArmLeft, 	mnArmTape)			bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(objTapeArmRight, csTapeArmRight, mnArmTape)			bFinished = FALSE		ENDIF
	
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_WRENCH].objObject, 	csWrench, 	mnWrench)		bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_WATER].objObject, 	csWater, 	mnWater)		bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_RAG].objObject, 		csRag,		mnRag)			bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_CLIP0].objObject, 	csClip0, 	mnClip0)		bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_CLIP1].objObject, 	csClip1, 	mnClip1)		bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_PLIERS].objObject, 	csPliers, 	mnPliers)		bFinished = FALSE		ENDIF
	IF NOT TORTURE_GetPropFromCutscene(menuWeapon[TOR_SYRINGE].objObject, 	csSyringe, 	mnSyringe)		bFinished = FALSE		ENDIF

	RETURN bFinished
ENDFUNC

FUNC BOOL TORTURE_GrabAllPedsFromCutscene(MODEL_NAMES modMrK = DUMMY_MODEL_FOR_SCRIPT)
	BOOL bFinished = TRUE
	ENTITY_INDEX entity
	
	IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")
		IF DOES_ENTITY_EXIST(entity)
			sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
		ELSE
			bFinished = FALSE
		ENDIF
	ENDIF
				
	IF NOT DOES_ENTITY_EXIST(pedVictim)
	
		IF modMrK = DUMMY_MODEL_FOR_SCRIPT
			entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(strMrKCSHandle, mnVictim)
		ELSE
			entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(strMrKCSHandle, modMrK)
		ENDIF
		IF DOES_ENTITY_EXIST(entity)
			pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
			//RESET_MR_K_VARIATIONS()
			MANAGE_MR_K_VARIATIONS()

//			RETAIN_ENTITY_IN_INTERIOR(pedVictim, interiorGarage)
		ELSE
			bFinished = FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedSteve)
		entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(csSteve, IG_STEVEHAINS)
		IF DOES_ENTITY_EXIST(entity)
			pedSteve = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
//			RETAIN_ENTITY_IN_INTERIOR(pedSteve, interiorGarage)
		ELSE
			bFinished = FALSE
		ENDIF
	ENDIF
	
	RETURN bFinished
ENDFUNC

PROC TORTURE_SpikeHeartRate(FLOAT fSpikePecentage, INT iSpikeInTime, INT iSpikeHoldTime, INT iSpikeReturnTime)

	fBPM_SpikeDesired 		= (fMaxBPM - fBPM_Base) * fSpikePecentage
	fBPM_SpikeStart			= fBPM_Spike
	iBPM_SpikeTimer 		= GET_GAME_TIMER()
	iBPM_SpikeInTime 		= iSpikeInTime
	iBPM_SpikeHoldTime 		= iSpikeHoldTime
	iBPM_SpikeReturnTime	= iSpikeReturnTime

ENDPROC

//PURPOSE: Sets the heartbeat bool true for one frame according to the BPM
PROC TORTURE_ManageHeartbeat()

	CDEBUG1LN(DEBUG_MIKE, "TORTURE_ManageHeartbeat() CALLED")

	FLOAT fHeartRate

	IF bFlatLine
		bHeartbeatFrame			= FALSE
		fBPM_Base 				= 0
		fBPM_Spike				= 0
		fBPM_SpikeDesired		= 0
		fBPM_SpikeStart			= 0
		fHeartRate				= 0
		
		CDEBUG1LN(DEBUG_MIKE, "TORTURE_ManageHeartbeat(): FLATLINE")
		
	// Calculate heart rate
	ELSE
	
		// Calculate the base rate off of the peds health
		fBPM_Base = CLAMP(fMinBPM + ((1-(fVictimHealth/fVictimMaxHealth))*(fMaxBPM-fMinBPM)), 0, fMaxBPM)
		
		// Calculate the spike heart rate
		IF fBPM_SpikeDesired = 0
			fBPM_Spike = 0
		ELSE
			INT iSpikeTimer = GET_GAME_TIMER() - iBPM_SpikeTimer
			FLOAT fAlpha
			// Spike in
			IF iSpikeTimer <= iBPM_SpikeInTime
				
				fAlpha 		= CLAMP(iSpikeTimer / TO_FLOAT(iBPM_SpikeInTime), 0.0, 1.0)
				fBPM_Spike 	= LERP_FLOAT(fBPM_SpikeStart, fBPM_SpikeDesired, fAlpha)
			
			// Spike hold
			ELIF iSpikeTimer <= iBPM_SpikeHoldTime
			
				
				fBPM_Spike 	= fBPM_SpikeDesired
			
			// Spike return
			ELIF iSpikeTimer <= iBPM_SpikeReturnTime
			
				fAlpha 		= CLAMP( (iSpikeTimer - iBPM_SpikeHoldTime - iBPM_SpikeInTime) / TO_FLOAT(iBPM_SpikeReturnTime), 0.0, 1.0)
				fBPM_Spike 	= LERP_FLOAT(fBPM_SpikeDesired, 0.0, fAlpha)
			
			// Reset the spike
			ELSE
				
				fBPM_Spike 				= 0
				fBPM_SpikeDesired		= 0
				fBPM_SpikeStart			= 0
				iBPM_SpikeTimer			= 0
				iBPM_SpikeInTime		= 0
				iBPM_SpikeHoldTime		= 0
				iBPM_SpikeReturnTime	= 0
				
			ENDIF
		ENDIF
		
		// Overall rate
		fHeartRate = fBPM_Base + fBPM_Spike + GET_RANDOM_INT_IN_RANGE(0, 3)
		
		IF GET_GAME_TIMER() - iLastHeartBeatTime >= ROUND(60000/fHeartRate)
			bHeartbeatFrame 	= TRUE
			iLastHeartBeatTime 	= GET_GAME_TIMER()
			CDEBUG1LN(DEBUG_MIKE, "TORTURE_ManageHeartbeat(): heartBeat")
		ELSE
			bHeartbeatFrame 	= FALSE
		ENDIF
	ENDIF
	
	// render target does not need the object to exist,
	// the final cutscene in the torture room creates the object and the script does not have a handle to it
	//IF DOES_ENTITY_EXIST(objMonitor) 
	IF sfHeartMonitor != NULL
	AND HAS_SCALEFORM_MOVIE_LOADED(sfHeartMonitor)
		IF GET_GAME_TIMER() - iUpdateEKGTimer > 500
			IF fCurrentSFBMP != fHeartRate
				fCurrentSFBMP = fHeartRate
				INT iBPM = ROUND(fCurrentSFBMP)
				FLOAT fRoundBPM = TO_FLOAT(iBPM)
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHeartMonitor, "SET_HEART_RATE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRoundBPM)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
			iUpdateEKGTimer = GET_GAME_TIMER()
		ENDIF
		IF fCurrentSFHealth != fVictimHealth
			fCurrentSFHealth = fVictimHealth
			BEGIN_SCALEFORM_MOVIE_METHOD(sfHeartMonitor, "SET_HEALTH")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentSFHealth)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF		
		IF NOT bFlatLine
			IF bHeartbeatFrame 
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHeartMonitor, "SET_HEART_BEAT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3.5)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//PURPOSE: Vibrates the pad based on the phases of various sync scenes
PROC TORTURE_ManagePadVibration()
	INT iDuration, iFrequency = 0
	FLOAT fPhase = 0
	
	CONST_INT 	I_HEARTBEAT_VIBRATION_DUR 	130
	CONST_INT 	I_HEARTBEAT_VIBRATION_FREQ 	130
	
	INT iLeftTriggerDur, iLeftTriggerFreq = 0
	INT iRightTriggerDur, iRightTriggerFreq = 0

	//If it's a heartbeat frame, heartbeat!
	IF bHeartbeatFrame 
	AND NOT bFlatLine
		iDuration = MAX_INTEGER(iDuration, I_HEARTBEAT_VIBRATION_DUR)
		iFrequency = MAX_INTEGER(iFrequency, I_HEARTBEAT_VIBRATION_FREQ)
	ENDIF
			
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_LEFT])	//Wrench smack
	AND eLastTorturePlayed = SCENE_WRENCH_LEFT
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_LEFT])
		IF fPhase > 0.393 AND fPhase < 0.407
			
			iDuration = MAX_INTEGER(iDuration, 260)
			iFrequency = MAX_INTEGER(iFrequency, 255)
			
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 260)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ENDIF
			
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_MID])	//Wrench smack
	AND eLastTorturePlayed = SCENE_WRENCH_MID
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_MID])
		IF fPhase > 0.185 AND fPhase < 0.202
		
			iDuration = MAX_INTEGER(iDuration, 260)
			iFrequency = MAX_INTEGER(iFrequency, 255)
			
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 260)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ENDIF
			
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_RIGHT])	//Wrench smack
	AND eLastTorturePlayed = SCENE_WRENCH_RIGHT
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_RIGHT])
		IF fPhase > 0.245 AND fPhase < 0.268
		
			iDuration = MAX_INTEGER(iDuration, 260)
			iFrequency = MAX_INTEGER(iFrequency, 255)
			
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 260)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ENDIF
			
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	
	// As the pliers enter the victims mouth do a quick impulse
	IF ( IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_ATTACH] )	AND eLastTorturePlayed = SCENE_PLIERS_ATTACH ) 
	OR ( IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_OUT] ) AND eLastTorturePlayed = SCENE_PLIERS_READY_OUT )
	
		BOOL bAttached
		IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_ATTACH] ) AND eLastTorturePlayed = SCENE_PLIERS_ATTACH
			
			fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_ATTACH])
			IF fPhase > 0.762 AND fPhase < 0.837
				bAttached = TRUE
			ENDIF
			
		ELIF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_OUT] ) AND eLastTorturePlayed = SCENE_PLIERS_READY_OUT
			
			fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_READY_OUT])
			IF fPhase > 0.444 AND fPhase < 0.581
				bAttached = TRUE
			ENDIF
			
		ENDIF
			
		IF bAttached
		
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 130)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerDur, 200)
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, 130)
				iFrequency 	= MAX_INTEGER(iFrequency, 200)
			ENDIF
		
		ENDIF
		
	ELIF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_IN] )	AND eLastTorturePlayed = SCENE_PLIERS_READY_IN
	
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_READY_IN])
		IF fPhase < 0.140
		
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 100)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerDur, 100)
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, 100)
				iFrequency 	= MAX_INTEGER(iFrequency, 100)
			ENDIF
			
		ENDIF
		
	ENDIF
	
	
	// As the pliers are held in the mouth do shake
	IF bPliersInMouth
		INT iLocalDur, iLocalFreq
		IF fPullStrength > 0.0
			
			iLocalDur = 130
			iLocalFreq = 20 + ROUND(20 * CLAMP(fTortureForceApplied/F_TOOTH_PULL_FORCE_LIMIT, 0.0, 1.0)) 
				+ ROUND(215 * fPullStrength) 
		ELSE

			IF IS_XBOX_PLATFORM()
				IF bHeartbeatFrame 
				AND NOT bFlatLine
					iLocalDur = I_HEARTBEAT_VIBRATION_DUR
					iLocalFreq = I_HEARTBEAT_VIBRATION_FREQ
				ENDIF
			ENDIF

		ENDIF
		
		IF IS_XBOX_PLATFORM()
			iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, iLocalDur)
			iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, iLocalFreq)
		ELSE
			iDuration 	= MAX_INTEGER(iDuration, iLocalDur)
			iFrequency 	= MAX_INTEGER(iFrequency, iLocalFreq)
		ENDIF

	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_PULL_OUT])	//Shake for when tooth pops out
	AND eLastTorturePlayed = SCENE_PLIERS_PULL_OUT
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_PULL_OUT])
		IF fPhase > 0.014 AND fPhase < 0.032
		
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 260)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, 260)
				iFrequency 	= MAX_INTEGER(iFrequency, 255)
			ENDIF

			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_PULL_OUT2])	//Shake for when tooth pops out
	AND eLastTorturePlayed = SCENE_PLIERS_PULL_OUT2
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_PULL_OUT2])
		IF fPhase > 0.06 AND fPhase < 0.082
		
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 260)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, 260)
				iFrequency 	= MAX_INTEGER(iFrequency, 255)
			ENDIF
			
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_OUT])	//Shakes for when plier reattach to teeth
	AND eLastTorturePlayed = SCENE_PLIERS_PULL_OUT2
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_READY_OUT])
		IF fPhase > 0.562 AND fPhase < 1.0
		
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 130)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 200)
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, 130)
				iFrequency 	= MAX_INTEGER(iFrequency, 200)
			ENDIF
			
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_SPARK])	//Elec Spark buzz
	AND eLastTorturePlayed = SCENE_ELEC_SPARK
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_ELEC_SPARK])
		IF fPhase > 0.142 AND fPhase < 0.213
			
			IF IS_XBOX_PLATFORM()
				iLeftTriggerDur 	= MAX_INTEGER(iLeftTriggerDur, 130)
				iLeftTriggerFreq 	= MAX_INTEGER(iLeftTriggerFreq, 255)
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 130)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ENDIF
			
			iDuration 	= MAX_INTEGER(iDuration, 130)
			iFrequency 	= MAX_INTEGER(iFrequency, 255)
			
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_OUTRO])	//End electrocution buzz
	AND eLastTorturePlayed = SCENE_ELEC_OUTRO
	
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_ELEC_OUTRO])
		IF fPhase > 0.0 AND fPhase < 0.198
			
			IF IS_XBOX_PLATFORM()
				iLeftTriggerDur 	= MAX_INTEGER(iLeftTriggerDur, 130)
				iLeftTriggerFreq 	= MAX_INTEGER(iLeftTriggerFreq, 255)
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, 130)
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, 255)
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, 130)
				iFrequency 	= MAX_INTEGER(iFrequency, 255)
			ENDIF
			
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF
	


#IF NOT IS_JAPANESE_BUILD

// Electrocution loop
	IF fClipGripProgLeft >= 1.0 AND fClipGripProgRight >= 1.0
	
		INT iLocalDur, iLocalFreq
		iLocalDur = 130
		iLocalFreq = ROUND(255 * fShockLoopTransition * CLAMP(fTortureForceApplied/F_ELECTRO_TIME, 0.5, 1.0))
		
		IF IS_XBOX_PLATFORM()
			iLeftTriggerDur 	= MAX_INTEGER(iLeftTriggerDur, iLocalDur)
			iLeftTriggerFreq 	= MAX_INTEGER(iLeftTriggerFreq, iLocalFreq)
			iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, iLocalDur)
			iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, iLocalFreq)
		ELSE
			iDuration 	= MAX_INTEGER(iDuration, iLocalDur)
			iFrequency 	= MAX_INTEGER(iFrequency, iLocalFreq)
		ENDIF
		
		CPRINTLN( DEBUG_MIKE, "TRIGGER SHAKE(ELECTRO): ", iLeftTriggerDur, iLeftTriggerFreq, iRightTriggerDur, iRightTriggerFreq )
		
	ELSE
	
	// Electrocution Left clamp
		// Holding on by itself, small vibrate ONLY ON DURANGO
		IF fClipGripProgLeft >= 1.0
		
			IF IS_XBOX_PLATFORM()
				IF bHeartbeatFrame 
				AND NOT bFlatLine
					iLeftTriggerDur 	= MAX_INTEGER(iLeftTriggerDur, I_HEARTBEAT_VIBRATION_DUR )
					iLeftTriggerFreq 	= MAX_INTEGER(iLeftTriggerFreq, I_HEARTBEAT_VIBRATION_FREQ )
				ENDIF
			ENDIF
		
		// Impulse on clamp/release
		ELIF fClipGripProgLeft > 0.704 AND fClipGripProgLeft < 1.0
		
			INT iLocalDur, iLocalFreq
		
		 	IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_LT )
				iLocalDur	= 130
				iLocalFreq	= 200
			ELSE
				iLocalDur	= 100
				iLocalFreq	= 100
			ENDIF
			
			IF IS_XBOX_PLATFORM()
				iLeftTriggerDur 	= MAX_INTEGER(iLeftTriggerDur, iLocalDur )
				iLeftTriggerFreq 	= MAX_INTEGER(iLeftTriggerFreq, iLocalFreq )
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, iLocalDur )
				iFrequency 	= MAX_INTEGER(iFrequency, iLocalFreq )
			ENDIF
		
		ENDIF
		
	// Electrocution Right clamp
		// Holding on by itself, small vibrate ONLY ON DURANGO
		IF fClipGripProgRight >= 1.0
		
			IF IS_XBOX_PLATFORM()
				IF bHeartbeatFrame 
				AND NOT bFlatLine
					iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, I_HEARTBEAT_VIBRATION_DUR )
					iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, I_HEARTBEAT_VIBRATION_FREQ )
				ENDIF
			ENDIF
		
		// Impulse on clamp/release
		ELIF fClipGripProgRight > 0.704 AND fClipGripProgRight < 1.0

			INT iLocalDur, iLocalFreq
		
			// On Clamp
		 	IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_SCRIPT_RT )
				iLocalDur	= 130
				iLocalFreq	= 200
				
			// On Release
			ELSE
				iLocalDur	= 100
				iLocalFreq	= 100
			ENDIF
			
			IF IS_XBOX_PLATFORM()
				iRightTriggerDur 	= MAX_INTEGER(iRightTriggerDur, iLocalDur )
				iRightTriggerFreq 	= MAX_INTEGER(iRightTriggerFreq, iLocalFreq )
			ELSE
				iDuration 	= MAX_INTEGER(iDuration, iLocalDur )
				iFrequency 	= MAX_INTEGER(iFrequency, iLocalFreq )
			ENDIF
			
		ENDIF
		
	ENDIF
	
	
#ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_FLIP])	//Shake for chair hitting ground
	AND eLastTorturePlayed = SCENE_WATER_FLIP
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WATER_FLIP])
		IF fPhase > 0.107 AND fPhase < 0.112
			iDuration = MAX_INTEGER(iDuration, 260)
			iFrequency = MAX_INTEGER(iFrequency, 255)
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_OUTRO])	//End water shake
	AND eLastTorturePlayed = SCENE_WATER_OUTRO
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WATER_OUTRO])
		IF fPhase > 0.0 AND fPhase < 0.043
			iDuration = MAX_INTEGER(iDuration, 130)
			iFrequency = MAX_INTEGER(iFrequency, 200)
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_USE])	//Smack for syringe plunge into chest
	AND eLastTorturePlayed = SCENE_SYRINGE_USE
		fPhase = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_SYRINGE_USE])
		IF fPhase > 0.03 AND fPhase < 0.043
			iDuration = MAX_INTEGER(iDuration, 260)
			iFrequency = MAX_INTEGER(iFrequency, 255)
			TORTURE_PlaySteveReaction()
		ENDIF
	ENDIF 
	
	IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_POUR])	//If pouring water, shake
	AND eLastTorturePlayed = SCENE_WATER_POUR
		iDuration = MAX_INTEGER(iDuration, 130)
		iFrequency = MAX_INTEGER(iFrequency, 200)
	ENDIF
		
	// Xbox one trigger shake
	IF IS_XBOX_PLATFORM()
		IF ( iLeftTriggerDur != 0 AND iLeftTriggerFreq != 0 ) 
		OR ( iRightTriggerDur != 0 AND iRightTriggerFreq != 0 )
			SET_CONTROL_TRIGGER_SHAKE( PLAYER_CONTROL, 0, 0, 0, 0 )
			SET_CONTROL_TRIGGER_SHAKE( PLAYER_CONTROL, iLeftTriggerDur, iLeftTriggerFreq, iRightTriggerDur, iRightTriggerFreq )
			CPRINTLN( DEBUG_MIKE, "TRIGGER SHAKE: ", iLeftTriggerDur, iLeftTriggerFreq, iRightTriggerDur, iRightTriggerFreq )
		ENDIF
	ENDIF
	
	// Pad Shake
	IF iDuration != 0 AND iFrequency != 0
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 0, 0)
		SET_CONTROL_SHAKE(PLAYER_CONTROL, iDuration, iFrequency)
	ENDIF
	
ENDPROC

//PURPOSE: Manage playing sound based on what scene is playing
PROC TORTURE_ManageSound()
	
	IF bHeartbeatFrame
		ReleaseSound(SND_HEART_FLATLINE_LOOP)
		STOP_SOUND(sndList[SND_HEART_FLATLINE_LOOP].SoundId)
		//PlaySoundFromEntity(SND_HEART_BEEP, soundHeartBeat, objMonitor, FALSE)
		IF NOT sndList[SND_HEART_BEEP].bHasPlayedAtAll
			sndList[SND_HEART_BEEP].SoundId = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND_FROM_COORD( sndList[SND_HEART_BEEP].SoundId, soundHeartBeat, <<143.0145, -2201.3586, 4.3415>> )
		sndList[SND_HEART_BEEP].bHasPlayedAtAll = TRUE
		SET_VARIABLE_ON_SOUND(sndList[SND_HEART_BEEP].SoundId, "BPM", fBPM_Base + fBPM_Spike)
	ELIF fBPM_Base + fBPM_Spike = 0
		ReleaseSound(SND_HEART_BEEP)
		STOP_SOUND(sndList[SND_HEART_BEEP].SoundId)
		IF NOT sndList[SND_HEART_FLATLINE_LOOP].bHasPlayedAtAll
			sndList[SND_HEART_FLATLINE_LOOP].SoundId = GET_SOUND_ID()
		ENDIF
		IF HAS_SOUND_FINISHED(sndList[SND_HEART_FLATLINE_LOOP].SoundId)
			//PlaySoundFromEntity(SND_HEART_FLATLINE_LOOP, soundHeartFlatline, objMonitor, FALSE)
			PLAY_SOUND_FROM_COORD( sndList[SND_HEART_FLATLINE_LOOP].SoundId, soundHeartFlatline, <<143.0145, -2201.3586, 4.3415>> )
			sndList[SND_HEART_FLATLINE_LOOP].bHasPlayedAtAll = TRUE
		ENDIF
	ENDIF

#IF NOT IS_JAPANESE_BUILD
// WRENCH
	IF IS_SCENE_AT_PHASE(SCENE_WRENCH_RIGHT, 0.245, 0.268)
	OR IS_SCENE_AT_PHASE(SCENE_WRENCH_MID, 0.185, 0.202)
	OR IS_SCENE_AT_PHASE(SCENE_WRENCH_LEFT, 0.393, 0.407)
		PlaySoundFromEntity(SND_WRENCH_HIT, soundWrenchHit, pedVictim)
	ELSE
		sndList[SND_WRENCH_HIT].bHasPlayed = FALSE
	ENDIF
	
// ELECTROCUTE
	IF IS_SCENE_AT_PHASE(SCENE_WEAPON_SELECT_PICKUP_CLIP0, 0.222, 1.000)
		IF NOT HasSoundPlayed(SND_ELEC_HUM_LOOP)
			PlaySoundFromEntity(SND_ELEC_HUM_LOOP, soundElectricHum, objTable)
		ENDIF
	ELIF IS_SCENE_AT_PHASE(SCENE_ELEC_OUTRO, 0.904, 1.000)
		ReleaseSound(SND_ELEC_HUM_LOOP)
	ENDIF
	
	IF fClipGripProgLeft > 0.704
		PlaySoundFromEntity(SND_ELEC_ATTACH_CLIP_LEFT, "Electrical_Clamp_On", menuWeapon[TOR_CLIP0].objObject, TRUE, "FBI_03_Torture_Sounds")
		sndList[SND_ELEC_DETACH_CLIP_LEFT].bHasPlayed = FALSE
	ELSE
		PlaySoundFromEntity(SND_ELEC_DETACH_CLIP_LEFT, "Electrical_Clamp_Off", menuWeapon[TOR_CLIP0].objObject, TRUE, "FBI_03_Torture_Sounds")
		sndList[SND_ELEC_ATTACH_CLIP_LEFT].bHasPlayed = FALSE
	ENDIF
	
	IF fClipGripProgRight > 0.704
		PlaySoundFromEntity(SND_ELEC_ATTACH_CLIP_RIGHT, "Electrical_Clamp_On", menuWeapon[TOR_CLIP1].objObject, TRUE, "FBI_03_Torture_Sounds")
		sndList[SND_ELEC_DETACH_CLIP_RIGHT].bHasPlayed = FALSE
	ELSE
		PlaySoundFromEntity(SND_ELEC_DETACH_CLIP_RIGHT, "Electrical_Clamp_Off", menuWeapon[TOR_CLIP1].objObject, TRUE, "FBI_03_Torture_Sounds")
		sndList[SND_ELEC_ATTACH_CLIP_RIGHT].bHasPlayed = FALSE
	ENDIF
	
	IF fShockLoopTransition > 0.0
		IF NOT HasSoundPlayed(SND_ELEC_ZAP_LOOP)
			PlaySoundFromEntity(SND_ELEC_ZAP_LOOP, soundElectricZap, pedVictim)
		ENDIF
		
	ELIF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_OUTRO])
	OR IS_SCENE_AT_PHASE(SCENE_ELEC_OUTRO, 0.198, 1.000)
		ReleaseSound(SND_ELEC_ZAP_LOOP)
	ENDIF
	
	IF IS_SCENE_AT_PHASE(SCENE_ELEC_SPARK, 0.146, 0.246)
		PlaySoundFromEntity(SND_ELEC_SPARK, soundElectricSpark, TREV_PED_ID())
	ELSE
		sndList[SND_ELEC_SPARK].bHasPlayed = FALSE
	ENDIF
	
// WATER
	IF IS_SCENE_AT_PHASE(SCENE_WEAPON_SELECT_PICKUP_WATER, 0.140, 0.192)
		PlaySoundFromEntity(SND_WATER_PICK_UP, soundWaterPickUp, TREV_PED_ID())
	ELSE
		sndList[SND_WATER_PICK_UP].bHasPlayed = FALSE
	ENDIF

	IF IS_SCENE_AT_PHASE(SCENE_WATER_POUR, 0.000, 1.000)
		IF NOT HasSoundPlayed(SND_WATER_POUR_LOOP)
			PlaySoundFromEntity(SND_WATER_POUR_LOOP, soundWaterPour, pedVictim)
		ENDIF
	ELSE
		ReleaseSound(SND_WATER_POUR_LOOP)
	ENDIF
	
// PLIERS
	IF IS_SCENE_AT_PHASE(SCENE_PLIERS_PULL_OUT, 0.01, 0.02)
	OR IS_SCENE_AT_PHASE(SCENE_PLIERS_PULL_OUT2, 0.06, 0.082)
		PlaySoundFromEntity(SND_TOOTH_PULL_OUT, soundToothPullOut, TREV_PED_ID())
	ELSE
		sndList[SND_TOOTH_PULL_OUT].bHasPlayed = FALSE
	ENDIF

	IF bPliersInMouth
		IF NOT HasSoundPlayed(SND_TOOTH_PULL_LOOP)
			PlaySoundFromEntity(SND_TOOTH_PULL_LOOP, soundToothPull, pedVictim)
		ENDIF
		SET_VARIABLE_ON_SOUND(sndList[SND_TOOTH_PULL_LOOP].SoundId, "Intensity", fPullStrength)
	ELSE
		ReleaseSound(SND_TOOTH_PULL_LOOP)
	ENDIF
#ENDIF
	
ENDPROC

//PURPOSE: Updates the camera to look at the ecg should the player be pressing the correct button
PROC TORTURE_UpdateLookAtECG()

	SWITCH eEKGCamState
		CASE EKGCAM_TORTURE
		
			IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_IDLE])
			OR IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_IDLE])
			OR IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_LOOP])
			OR IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_IDLE])
			OR IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_READY])
			OR IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_IDLE])
			OR IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_IDLE])
			OR bIdlingElectricity
				//IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_RELOAD) // bug: 1661973
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
					IF DOES_CAM_EXIST(camEKG)
						DESTROY_CAM(camEKG)
					ENDIF 
					clearText(FALSE, FALSE, TRUE)
					eEKGCamState 	= EKGCAM_INTERP_IN
				ENDIF
			ENDIF
			
		BREAK
		CASE EKGCAM_INTERP_IN
			
			IF NOT DOES_CAM_EXIST(camEKG)
			
				camEKG = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<143.0422, -2200.8059, 4.8667>>, <<-24.0583, -0.0000, 174.2966>>, 21.9763, FALSE)
				//camEKG = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<142.8827, -2200.8303, 4.9091>>, <<-27.9544, -0.0439, -168.1845>>, 22.9599, FALSE)
				SET_CAM_DEBUG_NAME(camEKG, "camECG_1")
				SHAKE_CAM(camEKG, shakeHand, 0.2)
				SET_CAM_MOTION_BLUR_STRENGTH(camEKG, 0.1)
				SET_CAM_ACTIVE_WITH_INTERP(camEKG, GET_RENDERING_CAM(), 500, GRAPH_TYPE_ACCEL)
				
			ELSE
				//IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_RELOAD) // bug: 1661973
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
					
					eEKGCamState 	= EKGCAM_LOOKING_AT
					
				ELSE
					IF NOT IS_CAM_INTERPOLATING(camEKG)
					
						IF DOES_CAM_EXIST(sSwayTorture.camInit)
							DESTROY_CAM(sSwayTorture.camInit)
						ENDIF
						
						IF DOES_CAM_EXIST(sSwayTorture.camDest)
							DESTROY_CAM(sSwayTorture.camDest)
						ENDIF
						
						eEKGCamState = EKGCAM_LOOKING_AT

					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		CASE EKGCAM_LOOKING_AT
		
			//IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_RELOAD) // bug: 1661973
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
			
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_IDLE])
					TORTURE_InitWeapSelectionCam()
					SET_CAM_ACTIVE(camEKG, TRUE)
					SET_CAM_ACTIVE_WITH_INTERP(sSwayTorture.camInit, camEKG, 500, GRAPH_TYPE_DECEL)
				ELSE
					IF DOES_CAM_EXIST(camTorture)
						SET_CAM_ACTIVE_WITH_INTERP(camTorture, camEKG, 500, GRAPH_TYPE_DECEL)
					ENDIF
				ENDIF
				
				eEKGCamState = EKGCAM_INTERP_OUT
			
			ENDIF
		
		BREAK
		CASE EKGCAM_INTERP_OUT
		
			//IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_RELOAD) // bug: 1661973
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
			
				eEKGCamState = EKGCAM_TORTURE
			
			ELSE
		
				IF NOT IS_CAM_INTERPOLATING(camEKG)

					IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_IDLE])
						IF NOT IS_CAM_INTERPOLATING(sSwayTorture.camInit)
							TORTURE_InitWeapSelectionCam()
							DESTROY_CAM(camEKG)
						ENDIF
					ELSE
						IF DOES_CAM_EXIST(camTorture)
							SET_CAM_ACTIVE(camTorture, TRUE)
						ENDIF
						DESTROY_CAM(camEKG)
					ENDIF
					
					eEKGCamState 	= EKGCAM_TORTURE
				
				ENDIF
			
			ENDIF
		
		BREAK
	ENDSWITCH
	
	IF eEKGCamState = EKGCAM_TORTURE
		bLookingAtECG = FALSE
	ELSE
		bLookingAtECG = TRUE
	ENDIF
	
ENDPROC

PROC TORTURE_Store_Victim_State()

	// Store the victims health
	g_replay.iReplayInt[1] -= g_replay.iReplayInt[1] & BITMASK_VICTIMHEALTH				// Wipe existing data
	g_replay.iReplayInt[1] |= ROUND(fVictimHealth)										// store data into bitfield
	
	// Store the victims variations
	g_replay.iReplayInt[1] -= g_replay.iReplayInt[1] & BITMASK_VICTIMVARS				// Wipe existing data
	INT iVariantsSet = 0
	INT i
	REPEAT COUNT_OF(bMrKVariations) i
		IF bMrKVariations[i]
			SET_BIT(iVariantsSet, i)
		ELSE
			CLEAR_BIT(iVariantsSet, i)
		ENDIF
	ENDREPEAT
	g_replay.iReplayInt[1] |= SHIFT_LEFT(iVariantsSet, BITSHIFTER_VICTIMHEALTH)	// store data into bitfield
	
	// Store the number of teeth pulled
	g_replay.iReplayInt[1] -= g_replay.iReplayInt[1] & BITMASK_VICTIMTEETH
	g_replay.iReplayInt[1] |= SHIFT_LEFT(iNumberOfTeethPulled, BITSHIFTER_VICTIMHEALTH + BITSHIFTER_VICTIMVARS)
	
	#IF NOT IS_JAPANESE_BUILD
	// Store the number of syringes used
	g_replay.iReplayInt[1] -= g_replay.iReplayInt[1] & BITMASK_SYRINGES
	g_replay.iReplayInt[1] |= SHIFT_LEFT(iNumberOfSyringesUsed, BITSHIFTER_VICTIMHEALTH + BITSHIFTER_VICTIMVARS + BITSHIFTER_VICTIMTEETH)
	#ENDIF

ENDPROC

PROC TORTURE_Request_Cutscene(BOOL bStreamPedDamageAboutToBeInflicted, TORTURE_WEAPONS eWeapon = TOR_WRENCH, INT iOptionalDamageType = 0)

	IF IS_CUTSCENE_ACTIVE()
		REMOVE_CUTSCENE()
	ENDIF
	
	BOOL bOverrideCutsceneAudioToToothless

	IF NOT bStreamPedDamageAboutToBeInflicted
		Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(FALSE, FALSE, strMrKCSHandle)
	ELSE
	
		STRING strMrKCS
		IF eWeapon = TOR_PLIERS
		OR iNumberOfTeethPulled > 0
			strMrKCS = csMrK_TL
			bOverrideCutsceneAudioToToothless = TRUE
		ELSE
			strMrKCS = csMrK
		ENDIF
		
		Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(FALSE, FALSE, strMrKCS)
	
		SWITCH eWeapon
			CASE TOR_PLIERS
				IF NOT bMrKVariations[MR_K_FACE_BLOODY]
					Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_HEAD, 0, 1, CS_MRK)
				ENDIF
				Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_TEETH, 1, iOptionalDamageType, CS_MRK)
			BREAK
			CASE TOR_CLIP0
				IF NOT bMrKVariations[MR_K_NIPPLES_BURNED]
					Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_DECL, 1, 0, CS_MRK)
				ENDIF
			BREAK
			CASE TOR_WATER
				IF NOT bMrKVariations[MR_K_WET]
					Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_SPECIAL2, 1, 0, CS_MRK)
				ENDIF
				IF bMrKVariations[MR_K_FACE_BLOODY]
					Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_HEAD, 0, 0, CS_MRK)
				ENDIF
			BREAK
			CASE TOR_WRENCH
				SWITCH iOptionalDamageType 
					CASE 0
						IF NOT bMrKVariations[MR_K_LEG_BLOODY]
							Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_FEET, 1, 0, CS_MRK)
						ENDIF
					BREAK
					CASE 1
						IF NOT bMrKVariations[MR_K_TORSO_BLOODY]
							Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_BERD, 1, 0, CS_MRK)
						ENDIF
					BREAK
					CASE 2
						IF NOT bMrKVariations[MR_K_CROTCH_BLOODY]
							Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, strMrKCS, PED_COMP_JBIB, 1, 0, CS_MRK)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	
	ENDIF
	
	// load the cutscene for the death anytime that the ped has arrested
	IF fVictimHealth <= 0.0
	
		//IF iTortureStage = 2
			REQUEST_CUTSCENE("FBI_3_MCS_6p1_b")
		//ENDIF
	ELSE
	
		IF bOverrideCutsceneAudioToToothless
		OR iNumberOfTeethPulled > 0
			SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS") 
		ENDIF
	
		SWITCH eMissionStage
			CASE ST_3_TORTURE_FOR_FIRST
				REQUEST_CUTSCENE("fbi_3_mcs_1")
			BREAK
			CASE ST_5_TORTURE_FOR_SECOND
				REQUEST_CUTSCENE("fbi_3_mcs_3")
			BREAK
			CASE ST_7_TORTURE_FOR_THIRD
				REQUEST_CUTSCENE("fbi_3_mcs_5")
			BREAK
			CASE ST_9_TORTURE_FINAL
				REQUEST_CUTSCENE("FBI_3_mcs_6p2")
			BREAK
		ENDSWITCH
	
	ENDIF
	
ENDPROC

FUNC BOOL TORTURE_Is_Control_Pressed(CONTROL_ACTION eControlAction, BOOL bJustPressed, BOOL eCheckEKGCam)

	IF bMissionFailed
		RETURN FALSE
	ENDIF

	IF bJustPressed
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, eControlAction)
			IF NOT eCheckEKGCam
			OR NOT bLookingAtECG
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, eControlAction)
			IF NOT eCheckEKGCam
			OR NOT bLookingAtECG
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF NOT IS_JAPANESE_BUILD


// PURPOSE: Fudge to allow PC to use a different button to pour, as pressing 'W' to do it doesn't make much sense.
PROC FUDGE_PC_POURING_CONTROL( INT &iLY )
												
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RT)
			iLY = -100
		ELSE
			iLY = 0
		ENDIF
	ENDIF
	
ENDPROC
	


//PURPOSE: Performs the torture sequences based on what the player's current torture weapon is, and the players input
FUNC BOOL TORTURE_ManageMinigame()

	IF bMadeSelection
		RETURN FALSE
	ENDIF
	
	IF TOR_PLIERS != INT_TO_ENUM(TORTURE_WEAPONS, iCurrentMenuWeapon)
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	ENDIF

	TORTURE_WEAPONS eCurrentMenuWeapon
	INT iLX = 0
	INT iLY = 0
	INT iRX = 0
	INT iRY = 0	
	FLOAT fXChange = 0
	FLOAT fYChange = 0
	FLOAT fPercentHealComplete = 0
	FLOAT fTortureTime = 0.0
	FLOAT fToothDepth = 0.0
	FLOAT fToothAngle = 0.0
	FLOAT fStickHypot
	FLOAT fStickAngleChange

	SWITCH iTortureStage
		CASE 0		
			eCurrentMenuWeapon = INT_TO_ENUM(TORTURE_WEAPONS, iCurrentMenuWeapon)
			
			TORTURE_UpdateLookAtECG()
			
			SWITCH eCurrentMenuWeapon
				CASE TOR_WRENCH			//Weapon is wrench
					IF bJustStarting
						
						IF HAS_ANIM_DICT_LOADED("MISSFBI3_WRENCH")
							IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_IDLE])
								IF TORTURE_PlaySyncScene(SCENE_WRENCH_IDLE, TRUE)
								
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpWrench)
									AND NOT bLookingAtECG
										PRINT_HELP_FOREVER(hlpWrench)	
									ENDIF
									
									bJustStarting = FALSE
								
								ENDIF
							ENDIF
						ENDIF

					ELSE
						IF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, TRUE, TRUE)
						
							BOOL bTortureStarted
						
							fTortureTime = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_IDLE])	//Get phase of idle animation
//							IF 	(fTortureTime > 0.090 AND fTortureTime < 0.150)	//If trevor on left of victim, play left anim
//							OR 	(fTortureTime > 0.305 AND fTortureTime < 0.370)
//							OR 	(fTortureTime > 0.505 AND fTortureTime < 0.555)
//							OR 	(fTortureTime > 0.590 AND fTortureTime < 0.620)
//							OR 	(fTortureTime > 0.665 AND fTortureTime < 0.735)
//							OR 	(fTortureTime > 0.920 AND fTortureTime < 0.990)
							IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Left"))
																
								IF TORTURE_PlaySyncScene(SCENE_WRENCH_LEFT)
									TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 1)
									
									clearText()
									
									SWITCH eMissionStage
										CASE ST_3_TORTURE_FOR_FIRST			RunConversation("F3_ARM1", "")		BREAK
										CASE ST_5_TORTURE_FOR_SECOND		RunConversation("F3_ARM2", "")		BREAK
										CASE ST_7_TORTURE_FOR_THIRD			RunConversation("F3_ARM3", "")		BREAK
										CASE ST_9_TORTURE_FINAL				RunConversation("F3_ARM4", "")		BREAK
									ENDSWITCH
									
									bTortureStarted = TRUE
								ENDIF
														
//							ELIF(fTortureTime > 0.020 AND fTortureTime < 0.070)	//If trevor on right of victim, play right anim
//							OR 	(fTortureTime > 0.190 AND fTortureTime < 0.235)
//							OR 	(fTortureTime > 0.400 AND fTortureTime < 0.410)
//							OR 	(fTortureTime > 0.635 AND fTortureTime < 0.650)
//							OR 	(fTortureTime > 0.775 AND fTortureTime < 0.845)
							ELIF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Right"))
								
								IF TORTURE_PlaySyncScene(SCENE_WRENCH_RIGHT)
									TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 0)
									
									clearText()
									
									SWITCH eMissionStage
										CASE ST_3_TORTURE_FOR_FIRST			RunConversation("F3_KNEE1", "")		BREAK
										CASE ST_5_TORTURE_FOR_SECOND		RunConversation("F3_KNEE2", "")		BREAK
										CASE ST_7_TORTURE_FOR_THIRD			RunConversation("F3_KNEE3", "")		BREAK
										CASE ST_9_TORTURE_FINAL				RunConversation("F3_KNEE1", "")		BREAK
									ENDSWITCH
									
									bTortureStarted = TRUE
								ENDIF
								
							ELSE												//Otherwise play the middle anim
								
								IF TORTURE_PlaySyncScene(SCENE_WRENCH_MID)
									TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 2)
									
									clearText()
									
									SWITCH eMissionStage
										CASE ST_3_TORTURE_FOR_FIRST			RunConversation("F3_BALLS1", "")		BREAK
										CASE ST_5_TORTURE_FOR_SECOND		RunConversation("F3_BALLS2", "")		BREAK
										CASE ST_7_TORTURE_FOR_THIRD			RunConversation("F3_BALLS3", "")		BREAK
										CASE ST_9_TORTURE_FINAL				RunConversation("F3_BALLS1", "")		BREAK
									ENDSWITCH
									
									bTortureStarted = TRUE
								ENDIF
								
							ENDIF
							
							IF bTortureStarted

								TORTURE_SpikeHeartRate(0.5, 3000, 2500, 7000)
								INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_WRENCH)

								bJustStarting = TRUE
								iTortureStage++

							ENDIF
						ELSE
							// Mr k begs
							IF IS_SAFE_TO_START_CONVERSATION()
								IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
									RunConversation("F3_NOWREN", "", TRUE)
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				BREAK
				CASE TOR_PLIERS											//Weapon pliers				

					SWITCH iTortureWeaponStage
					// IDLE: WAIT TO START PLIERS TORTURE
						CASE 0
							IF HAS_ANIM_DICT_LOADED("MISSFBI3_TOOTHPULL")
								IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_IDLE])
								AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_IN])
								AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_LOOP])
									
									IF bJustStarting
										
										IF TORTURE_PlaySyncScene(SCENE_PLIERS_IDLE, TRUE, 0.0, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)

											iWeaponSelDiaTimer = GET_GAME_TIMER()
											iWeaponSelDiaDelay = 14000
											
											bJustStarting 	= FALSE
												
										ENDIF
									ELSE
										
										IF TORTURE_PlaySyncScene(SCENE_PLIERS_READY_LOOP, TRUE, 0.0, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)

											iWeaponSelDiaTimer = GET_GAME_TIMER()
											iWeaponSelDiaDelay = 14000
												
										ENDIF
									
									ENDIF
									
								ELSE
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpPliers1)
									AND NOT bLookingAtECG
										PRINT_HELP_FOREVER(hlpPliers1)
									ENDIF
								
									// Player has pressed to attach pliers
									IF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, FALSE, TRUE)	
									
										IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_IDLE])
											IF TORTURE_PlaySyncScene(SCENE_PLIERS_ATTACH, FALSE, 0.0, WALK_BLEND_IN, NORMAL_BLEND_OUT)			
												bPlayedPliersInWhimper = FALSE
												iTortureWeaponStage++
											ENDIF
										ELIF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_LOOP])
											IF TORTURE_PlaySyncScene(SCENE_PLIERS_READY_OUT, FALSE, 0.0, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)			
												bPlayedPliersInWhimper = FALSE
												iTortureWeaponStage++
											ENDIF
										ENDIF
										
									ELSE
										// Mr K begs
										IF IS_SAFE_TO_START_CONVERSATION()
											IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_IDLE])
												IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
													RunConversation("F3_NOPLI", "", TRUE)
												ENDIF
											ELIF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_LOOP])
												IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES/2
													RunConversation("F3_HEAD", "", TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						BREAK
					// TRANSITION ATTACH: Stick pliers into Mr K's mouth for the first time
						CASE 1
							
							IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_ATTACH]) 
								
								// Trevor speaks as he puts the pliers in
								IF iWeaponSelStage = 0
									IF IS_SAFE_TO_START_CONVERSATION()
										RunConversation("F3_INMOUTH")
										iWeaponSelStage++
									ENDIF
								ENDIF
								
								// Play one shot as the pliers go into Mr K's mouth
								IF IS_SCENE_AT_PHASE(SCENE_PLIERS_ATTACH, 0.949, 1.0)
									IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
										PLAY_ONESHOT()
									ENDIF
								ENDIF
								// Play a whimper from Mr K
								IF IS_SCENE_AT_PHASE(SCENE_PLIERS_ATTACH, 0.949, 1.0)
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									AND IS_PED_IN_CURRENT_CONVERSATION(pedVictim)
									AND IS_SCRIPTED_SPEECH_PLAYING(pedVictim)
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
									IF NOT bPlayedPliersInWhimper
										PLAY_SOUND_FROM_ENTITY(-1, "Teeth_Initial_Pain", pedVictim, "FBI_03_Torture_Sounds")
										bPlayedPliersInWhimper = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							// Subsequent uses of the pliers after first putting themin his mouth this torture round
							IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_OUT]) 
								IF IS_SCENE_AT_PHASE(SCENE_PLIERS_READY_OUT, 0.496, 1.0)
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									AND IS_PED_IN_CURRENT_CONVERSATION(pedVictim)
									AND IS_SCRIPTED_SPEECH_PLAYING(pedVictim)
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
									IF NOT bPlayedPliersInWhimper
										PLAY_SOUND_FROM_ENTITY(-1, "Teeth_Initial_Pain", pedVictim, "FBI_03_Torture_Sounds")
										bPlayedPliersInWhimper = TRUE
									ENDIF
								ENDIF
							ENDIF
						
							IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_ATTACH])
							AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_READY_OUT])

								TORTURE_ANIM_DATA sAnimData
								TEXT_LABEL_63 strTemp
								TEXT_LABEL_63 camAnim
								TEXT_LABEL_63 camAnimDict
								
								ANIM_DATA sAnimDataTrevorPullWeak,		sAnimDataTrevorPullHard
								ANIM_DATA sAnimDataPliersPullWeak,		sAnimDataPliersPullHard
								ANIM_DATA sAnimDataMrKPullWeak,			sAnimDataMrKPullHard
								ANIM_DATA sAnimDataMrKPullWeakFace, 	sAnimDataMrKPullHardFace
								ANIM_DATA sAnimDataRestraintLPullWeak,  sAnimDataRestraintLPullHard
								ANIM_DATA sAnimDataRestraintRPullWeak,  sAnimDataRestraintRPullHard
									
								IF TORTURE_GetTortureSceneAnimData(SCENE_PLIERS_PULL_WEAK, sAnimData)
								
								// Trevor
									sAnimDataTrevorPullWeak.type 				= APT_SINGLE_ANIM
									sAnimDataTrevorPullWeak.flags				= AF_LOOPING | AF_OVERRIDE_PHYSICS
									
									sAnimDataTrevorPullWeak.dictionary0			= sAnimData.strAnimDict
									sAnimDataTrevorPullWeak.anim0				= sAnimData.strTrevor
									sAnimDataTrevorPullWeak.weight0				= 1.0
									
								// Pliers
									sAnimDataPliersPullWeak.type 				= APT_SINGLE_ANIM
									sAnimDataPliersPullWeak.flags				= AF_LOOPING //| AF_OVERRIDE_PHYSICS
									
									sAnimDataPliersPullWeak.dictionary0			= sAnimData.strAnimDict
									sAnimDataPliersPullWeak.anim0				= sAnimData.strWeapon
									sAnimDataPliersPullWeak.weight0				= 1.0
									
								// Mr K
									strTemp = sAnimData.strMrK
									strTemp += "_facial"
								
									sAnimDataMrKPullWeak.type 					= APT_SINGLE_ANIM
									sAnimDataMrKPullWeak.flags					= AF_LOOPING | AF_OVERRIDE_PHYSICS

									sAnimDataMrKPullWeak.dictionary0			= sAnimData.strAnimDict
									sAnimDataMrKPullWeak.anim0					= sAnimData.strMrK
									sAnimDataMrKPullWeak.weight0				= 1.0

								// Mr K face
									sAnimDataMrKPullWeakFace.type 				= APT_SINGLE_ANIM
									sAnimDataMrKPullWeakFace.flags				= AF_LOOPING | AF_OVERRIDE_PHYSICS | AF_SECONDARY
								
									sAnimDataMrKPullWeakFace.dictionary0		= sAnimData.strAnimDict
									sAnimDataMrKPullWeakFace.anim0				= CONVERT_TEXT_LABEL_TO_STRING(strTemp)
									sAnimDataMrKPullWeakFace.weight0			= 1.0
									
								ENDIF
								
								IF TORTURE_GetTortureSceneAnimData(SCENE_PLIERS_PULL_HARD, sAnimData)
								
								// Trevor
									sAnimDataTrevorPullHard.type 				= APT_SINGLE_ANIM
									sAnimDataTrevorPullHard.flags				= AF_LOOPING | AF_OVERRIDE_PHYSICS
									
									sAnimDataTrevorPullHard.dictionary0			= sAnimData.strAnimDict
									sAnimDataTrevorPullHard.anim0				= sAnimData.strTrevor
									sAnimDataTrevorPullHard.weight0				= 0.0
									
								// Pliers
									sAnimDataPliersPullHard.type 				= APT_SINGLE_ANIM
									sAnimDataPliersPullHard.flags				= AF_LOOPING// | AF_OVERRIDE_PHYSICS
									
									sAnimDataPliersPullHard.dictionary0			= sAnimData.strAnimDict
									sAnimDataPliersPullHard.anim0				= sAnimData.strWeapon
									sAnimDataPliersPullHard.weight0				= 0.0
									
								// Mr K
									strTemp = sAnimData.strMrK
									strTemp += "_facial"
								
									sAnimDataMrKPullHard.type 					= APT_SINGLE_ANIM
									sAnimDataMrKPullHard.flags					= AF_LOOPING | AF_OVERRIDE_PHYSICS
									
									sAnimDataMrKPullHard.dictionary0			= sAnimData.strAnimDict
									sAnimDataMrKPullHard.anim0					= sAnimData.strMrK
									sAnimDataMrKPullHard.weight0				= 0.0
									
								// Mr K face
									sAnimDataMrKPullHardFace.type 				= APT_SINGLE_ANIM
									sAnimDataMrKPullHardFace.flags				= AF_LOOPING | AF_OVERRIDE_PHYSICS | AF_SECONDARY
								
									sAnimDataMrKPullHardFace.dictionary0		= sAnimData.strAnimDict
									sAnimDataMrKPullHardFace.anim0				= CONVERT_TEXT_LABEL_TO_STRING(strTemp)
									sAnimDataMrKPullHardFace.weight0			= 0.0
									
									camAnim 									= sAnimData.strCam
									camAnimDict									= sAnimData.strAnimDict
									
								// Restraint Left
									sAnimDataRestraintLPullWeak.type 			= APT_SINGLE_ANIM
									sAnimDataRestraintLPullWeak.flags			= AF_LOOPING
								
									sAnimDataRestraintLPullWeak.dictionary0		= sAnimData.strAnimDict
									sAnimDataRestraintLPullWeak.anim0			= sAnimData.strTapeLH
									sAnimDataRestraintLPullWeak.weight0			= 1.0
									
								// Restraint Right
									sAnimDataRestraintRPullWeak.type 			= APT_SINGLE_ANIM
									sAnimDataRestraintRPullWeak.flags			= AF_LOOPING
								
									sAnimDataRestraintRPullWeak.dictionary0		= sAnimData.strAnimDict
									sAnimDataRestraintRPullWeak.anim0			= sAnimData.strTapeRH
									sAnimDataRestraintRPullWeak.weight0			= 1.0
									
								// Restraint Left
									sAnimDataRestraintLPullHard.type 			= APT_SINGLE_ANIM
									sAnimDataRestraintLPullHard.flags			= AF_LOOPING
								
									sAnimDataRestraintLPullHard.dictionary0		= sAnimData.strAnimDict
									sAnimDataRestraintLPullHard.anim0			= sAnimData.strTapeLH
									sAnimDataRestraintLPullHard.weight0			= 0.0
									
								// Restraint Right
									sAnimDataRestraintRPullHard.type 			= APT_SINGLE_ANIM
									sAnimDataRestraintRPullHard.flags			= AF_LOOPING
								
									sAnimDataRestraintRPullHard.dictionary0		= sAnimData.strAnimDict
									sAnimDataRestraintRPullHard.anim0			= sAnimData.strTapeRH
									sAnimDataRestraintRPullHard.weight0			= 0.0
								
								ENDIF
								
							// Trevor & Mr K
								TASK_SCRIPTED_ANIMATION(TREV_PED_ID(), 	sAnimDataTrevorPullWeak,	sAnimDataTrevorPullHard, 	sAnimDataBlank, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION)
								TASK_SCRIPTED_ANIMATION(pedVictim,		sAnimDataMrKPullWeak, 		sAnimDataMrKPullHard,		sAnimDataBlank, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION)
								TASK_SCRIPTED_ANIMATION(pedVictim,		sAnimDataMrKPullWeakFace, 	sAnimDataMrKPullHardFace,	sAnimDataBlank, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV_PED_ID(), FALSE, TRUE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim, FALSE, TRUE)
								
							// Pliers
								TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_PLIERS], NORMAL_BLEND_OUT)
								IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(menuWeapon[TOR_PLIERS].objObject, TREV_PED_ID())
									IF IS_ENTITY_ATTACHED(menuWeapon[TOR_PLIERS].objObject)
										DETACH_ENTITY(menuWeapon[TOR_PLIERS].objObject)
									ENDIF
									ATTACH_ENTITY_TO_ENTITY(menuWeapon[TOR_PLIERS].objObject, TREV_PED_ID(), GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
								ENDIF
								PLAY_ENTITY_SCRIPTED_ANIM(menuWeapon[TOR_PLIERS].objObject, sAnimDataPliersPullWeak, sAnimDataPliersPullHard, sAnimDataBlank, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(menuWeapon[TOR_PLIERS].objObject)
								
							// Tape cleanup
								IF bHasRestraintPlayedAnim
									STOP_SYNCHRONIZED_ENTITY_ANIM(objTapeArmLeft, NORMAL_BLEND_OUT, FALSE)
									STOP_SYNCHRONIZED_ENTITY_ANIM(objTapeArmRight, NORMAL_BLEND_OUT, FALSE)
									bHasRestraintPlayedAnim = FALSE
								ENDIF
								
							// Tape Left
								IF IS_ENTITY_ATTACHED(objTapeArmLeft)
									DETACH_ENTITY(objTapeArmLeft)
								ENDIF
								PLAY_ENTITY_SCRIPTED_ANIM(objTapeArmLeft, sAnimDataRestraintLPullWeak, sAnimDataRestraintLPullHard, sAnimDataBlank, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTapeArmLeft)
							
							// Tape Right
								IF IS_ENTITY_ATTACHED(objTapeArmRight)
									DETACH_ENTITY(objTapeArmRight)
								ENDIF
								PLAY_ENTITY_SCRIPTED_ANIM(objTapeArmRight, sAnimDataRestraintRPullWeak, sAnimDataRestraintRPullHard, sAnimDataBlank, NORMAL_BLEND_DURATION, NORMAL_BLEND_DURATION)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objTapeArmRight)
								
							// Cam
								IF NOT DOES_CAM_EXIST(camTorture)
									camTorture = CREATE_CAMERA(CAMTYPE_ANIMATED)	
									SET_CAM_DEBUG_NAME(camTorture, "Torture Cam")
								ENDIF
								
								PLAY_CAM_ANIM(camTorture, camAnim, camAnimDict, vTortureScene, vTortureRot, CAF_LOOPING)
								SET_CAM_ACTIVE(camTorture, TRUE)
								
								// added bug 1890635
								IF NOT IS_SCRIPT_GLOBAL_SHAKING()
									ANIMATED_SHAKE_SCRIPT_GLOBAL("SHAKE_CAM_medium", "medium", "", fTortureShake)
								ENDIF

								eLastTorturePlayed = SCENE_PLIERS_PULL_WEAK
								bPliersInMouth = TRUE
								TORTURE_SpikeHeartRate(0.5, 100, 500, 7000)
								iTortureWeaponStage++
								
							ENDIF
						BREAK
					// TOOTH YANKING
						CASE 2
						
							IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(TREV_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
							
								// No longer holding the grip, go back to ready
								IF NOT TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, FALSE, TRUE)
								
									IF TORTURE_PlaySyncScene(SCENE_PLIERS_READY_IN, FALSE, 0, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
										
										//CLEAR_PED_SECONDARY_TASK(pedVictim)
										//CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(pedVictim)
										
										// Stop speech
										IF DOES_ENTITY_EXIST(TREV_PED_ID())
										AND NOT IS_PED_INJURED(TREV_PED_ID())
										
											IF IS_AMBIENT_SPEECH_PLAYING(TREV_PED_ID())
												STOP_CURRENT_PLAYING_AMBIENT_SPEECH(TREV_PED_ID())
											ENDIF
										
										ENDIF
										
										IF DOES_ENTITY_EXIST(pedVictim)
										AND NOT IS_PED_INJURED(pedVictim)
										
											IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
												STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
											ENDIF
											
										ENDIF
										
										// kill mouth blood VFX
										IF bFXBloodMouth
											IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodMouth)
												STOP_PARTICLE_FX_LOOPED(ptfxBloodMouth)
											ENDIF
											bFXBloodMouth = FALSE
										ENDIF
										
										RunConversation("F3_HEAD", "", TRUE)
										
										bPliersInMouth 			= FALSE
										fPullStrength			= 0
										iTortureWeaponStage 	= 0
									ENDIF
									
								// Still holding grip
								ELSE
								
									bDisplayToothPull = TRUE
									
									// Different help text displayed on PC if you're using keyboard and mouse.
									IF NOT bLookingAtECG
										
										IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
											IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpPliers2)
												PRINT_HELP_FOREVER(hlpPliers2)
											ENDIF
										ELSE
											IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_PLIER2_KM")
												PRINT_HELP_FOREVER("HLP_PLIER2_KM")
											ENDIF
										ENDIF
										
									ENDIF
									
									GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLX, iLY, iRX, iRY, FALSE, TRUE) // Using unbound version so PC mouse works, and disabling look inversion.
																		
									fXChange = TO_FLOAT(iRX - iPreviousFrameRX)
									fYChange = TO_FLOAT(iRY - iPreviousFrameRY)
									
									fStickHypot = SQRT((TO_FLOAT(iRX)*TO_FLOAT(iRX))+(TO_FLOAT(iRY)*TO_FLOAT(iRY)))
									
									IF fStickHypot > 100 // hypotenuse used to simulate a dead zone
									OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) 
										fStickAngleChange = GET_ANGLE_BETWEEN_2D_VECTORS(
																TO_FLOAT(iRX),TO_FLOAT(iRY),								// This frame
																TO_FLOAT(iPreviousFrameRX),TO_FLOAT(iPreviousFrameRY))		// last frame
									ELSE
										fStickAngleChange = 0.0
									ENDIF
									
									// Update prev frame angle
									iPreviousFrameRX = iRX
									iPreviousFrameRY = iRY
									
									// Calculate the new pull strength
									IF ( fStickHypot > 100
									OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) )
									AND fStickAngleChange > I_TOOTH_PULL_ROT_SPEED * TIMESTEP()
									AND ((iRy > 0 AND fXChange < 0.0)
									OR (iRx > 0 AND fYChange > 0.0)
									OR (iRy < 0 AND fXChange > 0.0)
									OR (iRx < 0 AND fYChange < 0.0))
										fPullStrength += 0.5 * TIMESTEP()
									ELSE
										fPullStrength -= 0.25 * TIMESTEP()
									ENDIF
									fPullStrength = CLAMP(fPullStrength, 0.0, 1.0)
									
								#IF IS_DEBUG_BUILD
									DRAW_DEBUG_TEXT_WRAPPED_BOOL( "Using mouse?", IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) )
									
									DRAW_DEBUG_TEXT_WRAPPED_INT( "iLX", iLX )
									DRAW_DEBUG_TEXT_WRAPPED_INT( "iLY", iLY )
									DRAW_DEBUG_TEXT_WRAPPED_INT( "iRX", iRX )
									DRAW_DEBUG_TEXT_WRAPPED_INT( "iRY", iRY )

									DRAW_DEBUG_TEXT_WRAPPED_FLOAT( "fStickHypot", fStickHypot )
									DRAW_DEBUG_TEXT_WRAPPED_FLOAT( "fStickAngleChange", fStickAngleChange )
									DRAW_DEBUG_TEXT_WRAPPED_FLOAT( "fPullStrength", fPullStrength )
								#ENDIF
									
									// Update the anim weight and camera shake from the pull strength
									SET_ANIM_WEIGHT(TREV_PED_ID(), 		fPullStrength, 	AF_PRIORITY_MEDIUM, 0)
									SET_ANIM_WEIGHT(pedVictim, 			fPullStrength, 	AF_PRIORITY_MEDIUM, 0)
									SET_ANIM_WEIGHT(pedVictim, 			fPullStrength, 	AF_PRIORITY_MEDIUM, 0, TRUE)
									
									SET_ANIM_WEIGHT(objTapeArmLeft, 	fPullStrength,	AF_PRIORITY_MEDIUM, 0, TRUE)
									SET_ANIM_WEIGHT(objTapeArmRight, 	fPullStrength, 	AF_PRIORITY_MEDIUM, 0, TRUE)
									SET_ANIM_WEIGHT(menuWeapon[TOR_PLIERS].objObject, 	fPullStrength, 		AF_PRIORITY_MEDIUM, 0, TRUE)
									
									//SET_CAM_SHAKE_AMPLITUDE(camTorture, CLAMP(fPullStrength*3.5, 0.25, 3.5))
									
									// Update the victims damage
									IF fPullStrength > 0.50
									
										fTortureForceApplied += fPullStrength * TIMESTEP()
										fVictimHealth -= (F_TOOTH_PULL_DAMAGE_OVERALL-F_TOOTH_PULL_DAMAGE_FINAL) * ((fPullStrength * TIMESTEP())/F_TOOTH_PULL_FORCE_LIMIT)

										IF NOT bFXBloodMouth							
											ptfxBloodMouth = START_PARTICLE_FX_LOOPED_ON_PED_BONE(	fxBloodMouth, pedVictim, 
																	<<0,0.1,0>>, VECTOR_ZERO,
																	BONETAG_HEAD)
											bFXBloodMouth = TRUE
										ENDIF
									ELSE
										IF bFXBloodMouth
											IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodMouth)
												STOP_PARTICLE_FX_LOOPED(ptfxBloodMouth)
											ENDIF
											bFXBloodMouth = FALSE
										ENDIF
									ENDIF
									
									// Update tooth pull scaleform
									fToothDepth = POW((fTortureForceApplied/F_TOOTH_PULL_FORCE_LIMIT), I_TOOTH_STUBBORNNESS)
									fToothDepth *= F_PERCENT_TOOTH_HEIGHT
									//Gets the decimal percentage of how pulled the tooth is, multiplied by itself to exp increase, so tough at start
									
									fToothAngle = TO_FLOAT(iRx)*fToothDepth //Dampens the angle by the depth, so it wiggles more the more the tooth is out
									IF (iRx > 0 AND fToothAngle < 5) //Add wiggle at start
										fToothAngle = 5
									ENDIF
									IF (iRx < 0 AND fToothAngle > -5)
										fToothAngle = -5
									ENDIF
									fToothAngle = ((fToothAngle/128)*30)+30 //Converts the iRx -128 to 128 range into 0 to 360 range
									IF fCurrentSFToothAngle != fToothAngle
										fCurrentSFToothAngle = fToothAngle
										BEGIN_SCALEFORM_MOVIE_METHOD(sfToothPull, "SET_TEETH_ANGLE")
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentSFToothAngle)
										END_SCALEFORM_MOVIE_METHOD()
									ENDIF
									fToothDepth *= 100 //Converts the decimal percentage complete into a percent, used for depth
									IF fCurrentSFToothDepth != fToothDepth
										fCurrentSFToothDepth = fToothDepth
										BEGIN_SCALEFORM_MOVIE_METHOD(sfToothPull, "SET_TEETH_DEPTH")
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCurrentSFToothDepth)
										END_SCALEFORM_MOVIE_METHOD()
									ENDIF
									
								// >>>>>> Tooth has come loose, play pull outro
								//-------------------------------------------------------
									IF fTortureForceApplied >= F_TOOTH_PULL_FORCE_LIMIT
									
										// Decide which tooth pull scene to play
										FLOAT fToothRandom
										fToothRandom = GET_RANDOM_FLOAT_IN_RANGE()
										SYNC_SCENES eToothOutScene
										IF fToothRandom >= 0.5
											eToothOutScene = SCENE_PLIERS_PULL_OUT
										ELSE
											eToothOutScene = SCENE_PLIERS_PULL_OUT2
										ENDIF
										
										// PLAY TOOH PULL OUTRO
										IF TORTURE_PlaySyncScene(eToothOutScene, FALSE, 0.0, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
										
											CLEAR_PED_SECONDARY_TASK(pedVictim)
											//CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(pedVictim)

											// Stop the mouth blood loop
											IF bFXBloodMouth
												IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodMouth)
													STOP_PARTICLE_FX_LOOPED(ptfxBloodMouth)
												ENDIF
												bFXBloodMouth = FALSE
											ENDIF
											
											// kill mouth blood VFX
											IF bFXBloodMouth
												IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodMouth)
													STOP_PARTICLE_FX_LOOPED(ptfxBloodMouth)
												ENDIF
												bFXBloodMouth = FALSE
											ENDIF
											
											// Reset tooth pull scaleform
											BEGIN_SCALEFORM_MOVIE_METHOD(sfToothPull, "SET_TEETH_ANGLE")
												SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(30.0)
											END_SCALEFORM_MOVIE_METHOD()
											BEGIN_SCALEFORM_MOVIE_METHOD(sfToothPull, "SET_TEETH_DEPTH")
												SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(100.0)
											END_SCALEFORM_MOVIE_METHOD()

											// Process conversations
											clearText()
											
											// Stop speech
											IF DOES_ENTITY_EXIST(TREV_PED_ID())
											AND NOT IS_PED_INJURED(TREV_PED_ID())
												IF IS_AMBIENT_SPEECH_PLAYING(TREV_PED_ID())
													STOP_CURRENT_PLAYING_AMBIENT_SPEECH(TREV_PED_ID())
												ENDIF
											ENDIF
											
											iNumberOfTeethPulled++
											SWITCH eMissionStage
												CASE ST_3_TORTURE_FOR_FIRST		RunConversation("F3_PHIT1", "", TRUE)		BREAK
												CASE ST_5_TORTURE_FOR_SECOND	RunConversation("F3_PHIT2", "", TRUE)		BREAK
												CASE ST_7_TORTURE_FOR_THIRD		RunConversation("F3_PHIT3", "", TRUE)		BREAK
												CASE ST_9_TORTURE_FINAL			RunConversation("F3_PHIT4", "", TRUE)		BREAK
											ENDSWITCH
											
											INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_TOOTH)

											fTortureForceApplied 	= 0
											bJustStarting 			= TRUE
											bPliersInMouth 			= FALSE
											fPullStrength			= 0
											iTortureStage++
										ENDIF
									ELSE
										// Grunting and Screaming while pulling the tooth
										IF fPullStrength > 0.25
											IF DOES_ENTITY_EXIST(TREV_PED_ID())
											AND NOT IS_PED_INJURED(TREV_PED_ID())
												IF IS_SCRIPTED_SPEECH_PLAYING(TREV_PED_ID())
													KILL_FACE_TO_FACE_CONVERSATION()
												ELSE
													IF NOT IS_AMBIENT_SPEECH_PLAYING(TREV_PED_ID())
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(TREV_PED_ID(), "FBI3_BUAA", "Trevor", SPEECH_PARAMS_FORCE_NORMAL)
													ENDIF
												ENDIF
											ENDIF
											
											// Mr K
											IF DOES_ENTITY_EXIST(pedVictim)
											AND NOT IS_PED_INJURED(pedVictim)
												IF IS_SCRIPTED_SPEECH_PLAYING(pedVictim)
													KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												ELSE
													IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Toothpull_pain", "MisterK", SPEECH_PARAMS_FORCE_NORMAL)
													ENDIF
												ENDIF
											ENDIF
											
										ELSE
											// Mr K
											IF DOES_ENTITY_EXIST(pedVictim)
											AND NOT IS_PED_INJURED(pedVictim)
												IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Pliers_Panting", "MisterK", SPEECH_PARAMS_FORCE_NORMAL)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								
								ENDIF
							
							ENDIF
						
						BREAK
					ENDSWITCH
					
				BREAK
				CASE TOR_CLIP0					//Weapon is Electrocution
				
					SWITCH iTortureWeaponStage
						CASE 0
							IF HAS_ANIM_DICT_LOADED("MISSFBI3_electrocute")
								IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_SPARK])
							
									ANIM_DATA			sAnimDataTrevorIdle, 	sAnimDataTrevorAttach, 	sAnimDataTrevorShock
									ANIM_DATA			sAnimDataMrKIdle, 		sAnimDataMrKAttach, 	sAnimDataMrKShock
									ANIM_DATA			sAnimDataMrKIdleFace,	sAnimDataMrKAttachFace,	sAnimDataMrKShockFace
									ANIM_DATA			sAnimDataWireLIdle,		sAnimDataWireLAttach, 	sAnimDataWireLShock
									ANIM_DATA			sAnimDataWireRIdle,		sAnimDataWireRAttach, 	sAnimDataWireRShock
									
									TEXT_LABEL_63 		camAnim
									TEXT_LABEL_63		camAnimDict
									
									sAnimDataTrevorIdle.type 		= APT_SINGLE_ANIM
									sAnimDataTrevorIdle.flags		= AF_LOOPING | AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataMrKIdle.type 			= APT_SINGLE_ANIM
									sAnimDataMrKIdle.flags			= AF_LOOPING | AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET 
									sAnimDataMrKIdleFace.type 		= APT_SINGLE_ANIM
									sAnimDataMrKIdleFace.flags		= AF_LOOPING | AF_OVERRIDE_PHYSICS | AF_SECONDARY
									sAnimDataWireLIdle.type 		= APT_SINGLE_ANIM
									sAnimDataWireLIdle.flags		= AF_LOOPING //| AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET 
									sAnimDataWireRIdle.type 		= APT_SINGLE_ANIM
									sAnimDataWireRIdle.flags		= AF_LOOPING //| AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET 
									
									sAnimDataTrevorAttach.type 		= APT_3_WAY_BLEND
									sAnimDataTrevorAttach.flags		= AF_LOOPING | AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataMrKAttach.type 		= APT_3_WAY_BLEND
									sAnimDataMrKAttach.flags		= AF_LOOPING | AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataMrKAttachFace.type 	= APT_3_WAY_BLEND
									sAnimDataMrKAttachFace.flags	= AF_LOOPING | AF_OVERRIDE_PHYSICS | AF_SECONDARY
									sAnimDataWireLAttach.type 		= APT_3_WAY_BLEND
									sAnimDataWireLAttach.flags		= AF_LOOPING //| AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataWireRAttach.type 		= APT_3_WAY_BLEND
									sAnimDataWireRAttach.flags		= AF_LOOPING //| AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									
									sAnimDataTrevorShock.type 		= APT_SINGLE_ANIM
									sAnimDataTrevorShock.flags		= AF_LOOPING | AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataMrKShock.type 			= APT_SINGLE_ANIM
									sAnimDataMrKShock.flags			= AF_LOOPING | AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataMrKShockFace.type 		= APT_SINGLE_ANIM
									sAnimDataMrKShockFace.flags		= AF_LOOPING | AF_OVERRIDE_PHYSICS | AF_SECONDARY
									sAnimDataWireLShock.type 		= APT_SINGLE_ANIM
									sAnimDataWireLShock.flags		= AF_LOOPING //| AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									sAnimDataWireRShock.type 		= APT_SINGLE_ANIM
									sAnimDataWireRShock.flags		= AF_LOOPING //| AF_OVERRIDE_PHYSICS// | AF_EXTRACT_INITIAL_OFFSET
									
								// Set up animations for scripted animation task
									camAnimDict								= "MISSFBI3_electrocute"
									camAnim 								= "Clamp_Wait_Loop_Cam"
										
								// Stand IDLE with the clips ready to shock
									sAnimDataTrevorIdle.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataTrevorIdle.anim0				= "Clamp_Wait_Loop_Player"
									
									sAnimDataMrKIdle.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataMrKIdle.anim0					= "Clamp_Wait_Loop_Victim"
									
									sAnimDataMrKIdleFace.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataMrKIdleFace.anim0				= "Clamp_Wait_Loop_Victim_facial"
									sAnimDataMrKIdleFace.weight0			= 1.0
									
									sAnimDataWireLIdle.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataWireLIdle.anim0				= "Clamp_Wait_Loop_l_clip"
									
									sAnimDataWireRIdle.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataWireRIdle.anim0				= "Clamp_Wait_Loop_r_clip"
									
								// Use left grip
									sAnimDataTrevorAttach.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataTrevorAttach.anim0				= "Clamp_Left_Loop_Player"
									sAnimDataTrevorAttach.weight0			= 0.0
									
									sAnimDataMrKAttach.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataMrKAttach.anim0				= "Clamp_Left_Loop_Victim"
									sAnimDataMrKAttach.weight0				= 0.0
									
									sAnimDataMrKAttachFace.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataMrKAttachFace.anim0			= "Clamp_Left_Loop_Victim_facial"
									sAnimDataMrKAttachFace.weight0			= 0.0
									
									sAnimDataWireLAttach.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataWireLAttach.anim0				= "Clamp_Left_Loop_l_clip"
									sAnimDataWireLAttach.weight0			= 0.0
									
									sAnimDataWireRAttach.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataWireRAttach.anim0				= "Clamp_Left_Loop_r_clip"
									sAnimDataWireRAttach.weight0			= 0.0
									
								// Use right grip								
									sAnimDataTrevorAttach.dictionary2		= "MISSFBI3_electrocute"
									sAnimDataTrevorAttach.anim2				= "Clamp_Right_Loop_Player"
									sAnimDataTrevorAttach.weight2			= 0.0
									
									sAnimDataMrKAttach.dictionary2			= "MISSFBI3_electrocute"
									sAnimDataMrKAttach.anim2				= "Clamp_Right_Loop_Victim"
									sAnimDataMrKAttach.weight2				= 0.0
									
									sAnimDataMrKAttachFace.dictionary2		= "MISSFBI3_electrocute"
									sAnimDataMrKAttachFace.anim2			= "Clamp_Right_Loop_Victim_facial"
									sAnimDataMrKAttachFace.weight2			= 0.0
									
									sAnimDataWireLAttach.dictionary2		= "MISSFBI3_electrocute"
									sAnimDataWireLAttach.anim2				= "Clamp_Right_Loop_l_clip"
									sAnimDataWireLAttach.weight2			= 0.0
									
									sAnimDataWireRAttach.dictionary2		= "MISSFBI3_electrocute"
									sAnimDataWireRAttach.anim2				= "Clamp_Right_Loop_r_clip"
									sAnimDataWireRAttach.weight2			= 0.0

								// Use both grips
									sAnimDataTrevorAttach.dictionary1		= "MISSFBI3_electrocute"
									sAnimDataTrevorAttach.anim1				= "Electrocute_Both_Loop_Player"
									sAnimDataTrevorAttach.weight1			= 0.0
									
									sAnimDataMrKAttach.dictionary1			= "MISSFBI3_electrocute"
									sAnimDataMrKAttach.anim1				= "Electrocute_Both_Loop_Victim" //"clamp_both_idle_victim"
									sAnimDataMrKAttach.weight1				= 0.0
									
									sAnimDataMrKAttachFace.dictionary1		= "MISSFBI3_electrocute"
									sAnimDataMrKAttachFace.anim1			= "Electrocute_Both_Loop_Victim_facial"
									sAnimDataMrKAttachFace.weight1			= 0.0
									
									sAnimDataWireLAttach.dictionary1		= "MISSFBI3_electrocute"
									sAnimDataWireLAttach.anim1				= "electrocute_both_loop_l_clip"
									sAnimDataWireLAttach.weight1			= 0.0
									
									sAnimDataWireRAttach.dictionary1		= "MISSFBI3_electrocute"
									sAnimDataWireRAttach.anim1				= "electrocute_both_loop_r_clip"
									sAnimDataWireRAttach.weight1			= 0.0
									
								// Electrocute									
									sAnimDataTrevorShock.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataTrevorShock.anim0				= "Electrocute_Both_Loop_Player"
									sAnimDataTrevorShock.weight0			= 0.0
									
									sAnimDataMrKShock.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataMrKShock.anim0					= "Electrocute_Both_Loop_Victim"
									sAnimDataMrKShock.weight0				= 0.0
									
									sAnimDataMrKShockFace.dictionary0		= "MISSFBI3_electrocute"
									sAnimDataMrKShockFace.anim0				= "Electrocute_Both_Loop_Victim_facial"
									sAnimDataMrKShockFace.weight0			= 0.0
									
									sAnimDataWireLShock.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataWireLShock.anim0				= "electrocute_both_loop_l_clip"
									sAnimDataWireLShock.weight0				= 0.0
									
									sAnimDataWireRShock.dictionary0			= "MISSFBI3_electrocute"
									sAnimDataWireRShock.anim0				= "electrocute_both_loop_r_clip"
									sAnimDataWireRShock.weight0				= 0.0


									FLOAT fBlendDuration
									FLOAT fBlendDelta
									IF bJustStarting
										fBlendDuration 	= INSTANT_BLEND_DURATION
										fBlendDelta		= INSTANT_BLEND_IN
									ELSE
										fBlendDuration 	= SLOW_BLEND_DURATION
										fBlendDelta		= SLOW_BLEND_IN
									ENDIF
									
									IF bHasChairPlayedAnim
										IF DOES_ENTITY_EXIST(objChair)
											STOP_SYNCHRONIZED_ENTITY_ANIM(objChair, INSTANT_BLEND_OUT, FALSE)
											bHasChairPlayedAnim = FALSE
										ENDIF
									ENDIF
									TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_CLIP0], -fBlendDelta)
									TORTURE_StopTortureWeaponAnim(menuWeapon[TOR_CLIP1], -fBlendDelta)
									
									IF NOT IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP0].objObject)
										ATTACH_ENTITY_TO_ENTITY(menuWeapon[TOR_CLIP0].objObject, TREV_PED_ID(), GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
									ENDIF
									
									IF NOT IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP1].objObject)
										ATTACH_ENTITY_TO_ENTITY(menuWeapon[TOR_CLIP1].objObject, TREV_PED_ID(), GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
									ENDIF
									
									IF bHasRestraintPlayedAnim
										STOP_SYNCHRONIZED_ENTITY_ANIM(objTapeArmLeft, -fBlendDelta, FALSE)
										STOP_SYNCHRONIZED_ENTITY_ANIM(objTapeArmRight, -fBlendDelta, FALSE)
										bHasRestraintPlayedAnim = FALSE
									ENDIF
									
									VECTOR vCoord, vRot
									vCoord 	= GET_ANIM_INITIAL_OFFSET_POSITION("MISSFBI3_electrocute", "Electrocute_Both_Loop_hand_l", vChairCoords, vChairRot-<<0,0,180>>)
									vRot 	= GET_ANIM_INITIAL_OFFSET_ROTATION("MISSFBI3_electrocute", "Electrocute_Both_Loop_hand_l", vChairCoords, vChairRot-<<0,0,180>>)
									SET_ENTITY_COORDS(objTapeArmLeft, vCoord)
									SET_ENTITY_ROTATION(objTapeArmLeft, vRot)
									PLAY_ENTITY_ANIM(objTapeArmLeft, "Electrocute_Both_Loop_hand_l", "MISSFBI3_electrocute", fBlendDelta, TRUE, FALSE)
									vCoord 	= GET_ANIM_INITIAL_OFFSET_POSITION("MISSFBI3_electrocute", "Electrocute_Both_Loop_hand_r", vChairCoords, vChairRot-<<0,0,180>>)
									vRot 	= GET_ANIM_INITIAL_OFFSET_ROTATION("MISSFBI3_electrocute", "Electrocute_Both_Loop_hand_r", vChairCoords, vChairRot-<<0,0,180>>)
									SET_ENTITY_COORDS(objTapeArmRight, vCoord)
									SET_ENTITY_ROTATION(objTapeArmRight, vRot)
									PLAY_ENTITY_ANIM(objTapeArmRight, "Electrocute_Both_Loop_hand_r", "MISSFBI3_electrocute", fBlendDelta, TRUE, FALSE)
									
									TASK_SCRIPTED_ANIMATION(TREV_PED_ID(), 	sAnimDataTrevorAttach, 	sAnimDataTrevorIdle, 	sAnimDataTrevorShock, 	fBlendDuration)
									TASK_SCRIPTED_ANIMATION(pedVictim, 		sAnimDataMrKAttach, 	sAnimDataMrKIdle, 		sAnimDataMrKShock, 		fBlendDuration)
									TASK_SCRIPTED_ANIMATION(pedVictim,		sAnimDataMrKAttachFace, sAnimDataMrKIdleFace,	sAnimDataMrKShockFace, 	fBlendDuration)
									PLAY_ENTITY_SCRIPTED_ANIM(menuWeapon[TOR_CLIP0].objObject, sAnimDataWireRAttach, sAnimDataWireRIdle, sAnimDataWireRShock, fBlendDuration)
									PLAY_ENTITY_SCRIPTED_ANIM(menuWeapon[TOR_CLIP1].objObject, sAnimDataWireLAttach, sAnimDataWireLIdle, sAnimDataWireLShock, fBlendDuration)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV_PED_ID())
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedVictim)
									
									bPlayedTortureSceneThisFrame = TRUE
									SAFE_FADE_IN(DEFAULT_FADE_TIME_SHORT)
									
									// Cam
									IF NOT DOES_CAM_EXIST(camTorture)
										camTorture = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)	
										SET_CAM_DEBUG_NAME(camTorture, "Torture Cam")
									ENDIF
									
									PLAY_CAM_ANIM(camTorture, camAnim, camAnimDict, vTortureScene, vTortureRot, CAF_LOOPING)
									SHAKE_CAM(camTorture, shakeRoadVib, 0.25)
									
									// added bug 1890635
									IF NOT IS_SCRIPT_GLOBAL_SHAKING()
										ANIMATED_SHAKE_SCRIPT_GLOBAL("SHAKE_CAM_medium", "medium", "", fTortureShake)
									ENDIF
										
									// PTFX
									IF NOT bFXElecSmoulder
										ptfxElecSmoulder = START_PARTICLE_FX_LOOPED_ON_PED_BONE(fxElecSmoulder, pedVictim,
																								<<0,0.0,0>>, VECTOR_ZERO, BONETAG_NECK)
										fSmokeEvo = 0.0
										bFXElecSmoulder = TRUE
									ENDIF
									
									iTimeSinceLastShock = 0
									bElecgripSpike 	= FALSE
									bJustStarting 	= FALSE
									iTortureWeaponStage++
									
								ENDIF
							ENDIF
						BREAK
						CASE 1
						// >>>> START OUTRO
							// Either player has let go after the required electrocution time
							IF ((fTortureForceApplied > F_ELECTRO_TIME OR (fTortureForceApplied > F_ELECTRO_TIME/4 AND fVictimHealth <= 20.0))
							AND (NOT TORTURE_Is_Control_Pressed(INPUT_SCRIPT_LT, FALSE, TRUE)
							OR NOT TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, FALSE, TRUE)	) )
							
							// OR they have electrocuted Mrk too much
							OR fVictimHealth <= 0.0
								
								IF TORTURE_PlaySyncScene(SCENE_ELEC_OUTRO, FALSE, 0.0, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
								
									CLEAR_PED_SECONDARY_TASK(pedVictim)

									fTortureForceApplied 	= 0
									fClipGripProgLeft 		= 0
									fClipGripProgRight 		= 0
									fClipGripIncLeft		= 0
									fClipGripIncRight		= 0
									fLRBalance				= 0
									fLRBalanceNorm			= 0
									fLeftWeight 			= 0
									fRightWeight 			= 0
									fIdleWeight 			= 0
									fBothWeight				= 0
									fShockLoopTransition	= 0
									fShockLoopInc			= 0
									
									clearText()
									
									RunConversation("F3_EHIT1")
									
									iTortureStage++		//ELECTROCUTED, MOVE ONTO OUTRO SECTION
								ENDIF
								
						// >>>> SPARK ANIM
							ELIF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RDOWN, TRUE, TRUE)
							AND GET_GAME_TIMER() > i_SparkTimer 

								IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_SPARK])
									IF TORTURE_PlaySyncScene(SCENE_ELEC_SPARK, FALSE, DEFAULT, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
									
										IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
										RunConversation("F3_REACT", DEFAULT, TRUE)
										
										fClipGripProgLeft 		= 0
										fClipGripProgRight 		= 0
										fClipGripIncLeft		= 0
										fClipGripIncRight		= 0
										fLRBalance				= 0
										fLRBalanceNorm			= 0
										fLeftWeight 			= 0
										fRightWeight 			= 0
										fIdleWeight 			= 0
										fBothWeight				= 0
										fShockLoopTransition	= 0
										fShockLoopInc			= 0
										
										i_SparkTimer = GET_GAME_TIMER() + 500
										iTortureWeaponStage++
									ENDIF
								ENDIF
								
							ELSE
							
								IF NOT IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP0].objObject)
									ATTACH_ENTITY_TO_ENTITY(menuWeapon[ENUM_TO_INT(TOR_CLIP0)].objObject, 
										TREV_PED_ID(), 
										GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_R_HAND),
										menuWeapon[ENUM_TO_INT(TOR_CLIP1)].vAttachOffset, 
										menuWeapon[ENUM_TO_INT(TOR_CLIP1)].vAttachRot)
								ENDIF
								
								IF NOT IS_ENTITY_ATTACHED(menuWeapon[TOR_CLIP1].objObject)
									ATTACH_ENTITY_TO_ENTITY(menuWeapon[ENUM_TO_INT(TOR_CLIP1)].objObject, 
										TREV_PED_ID(), 
										GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_L_HAND),
										menuWeapon[ENUM_TO_INT(TOR_CLIP1)].vAttachOffset, 
										menuWeapon[ENUM_TO_INT(TOR_CLIP1)].vAttachRot)
								ENDIF
						
								// >>>>> MANAGE ANIM BLENDING <<<<<
								//----------------------------------
								FLOAT fIncrement
								fIncrement = TIMESTEP()/0.250
							
								IF fClipGripProgLeft = 0.0
									IF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_LT, FALSE, TRUE)
										fClipGripIncLeft = fIncrement
									ENDIF
								ELIF fClipGripProgLeft = 1.0
									IF NOT TORTURE_Is_Control_Pressed(INPUT_SCRIPT_LT, FALSE, TRUE)
										fClipGripIncLeft = -fIncrement //* 0.5 // slower out
									ENDIF
								ENDIF
								
								IF fClipGripProgRight = 0.0
									IF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, FALSE, TRUE)
										fClipGripIncRight = fIncrement
									ENDIF
								ELIF fClipGripProgRight = 1.0
									IF NOT TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, FALSE, TRUE)
										fClipGripIncRight = -fIncrement //* 0.5 // slower out
									ENDIF
								ENDIF
								
								fClipGripProgLeft 	= CLAMP(fClipGripProgLeft + fClipGripIncLeft, 0.0, 1.0)
								fClipGripProgRight 	= CLAMP(fClipGripProgRight + fClipGripIncRight, 0.0, 1.0)

								fLRBalance		= fClipGripProgRight - fClipGripProgLeft
								fLRBalanceNorm	= ((fClipGripProgRight - fClipGripProgLeft) + 1.0)/2.0
								fLeftWeight 	= ABSF(CLAMP(fLRBalance, -1.0, 0.0))
								fRightWeight 	= ABSF(CLAMP(fLRBalance, 0.0, 1.0))
								fIdleWeight 	= 1 - ((fClipGripProgLeft * (1.0 - fLRBalanceNorm)) + (fClipGripProgRight * fLRBalanceNorm))
								fBothWeight		= 1 - ABSF(fLRBalance)
								
								IF fClipGripProgLeft = 1.0 AND fClipGripProgRight = 1.0
									IF fShockLoopTransition = 0.0
										fShockLoopInc = TIMESTEP()/0.125
									ENDIF
								ELSE
									IF fShockLoopTransition > 0.0
										fShockLoopInc = -TIMESTEP()/0.125
									ENDIF
								ENDIF
								
								fShockLoopTransition = CLAMP(fShockLoopTransition + fShockLoopInc, 0.0, 1.0)
								
								SET_ANIM_WEIGHT(TREV_PED_ID(), fShockLoopTransition, 	AF_PRIORITY_HIGH)
								SET_ANIM_WEIGHT(TREV_PED_ID(), fIdleWeight, 			AF_PRIORITY_MEDIUM)
								SET_ANIM_WEIGHT(TREV_PED_ID(), fLeftWeight, 			AF_PRIORITY_LOW, 	0)
								SET_ANIM_WEIGHT(TREV_PED_ID(), fRightWeight,			AF_PRIORITY_LOW, 	2)
								SET_ANIM_WEIGHT(TREV_PED_ID(), fBothWeight, 			AF_PRIORITY_LOW, 	1)
								
								SET_ANIM_WEIGHT(pedVictim, fShockLoopTransition, 		AF_PRIORITY_HIGH)
								SET_ANIM_WEIGHT(pedVictim, fIdleWeight, 				AF_PRIORITY_MEDIUM)
								SET_ANIM_WEIGHT(pedVictim, fLeftWeight, 				AF_PRIORITY_LOW, 	0)
								SET_ANIM_WEIGHT(pedVictim, fRightWeight,				AF_PRIORITY_LOW, 	2)
								SET_ANIM_WEIGHT(pedVictim, fBothWeight, 				AF_PRIORITY_LOW, 	1)
								
								SET_ANIM_WEIGHT(pedVictim, fShockLoopTransition, 		AF_PRIORITY_HIGH, 	0, TRUE)
								SET_ANIM_WEIGHT(pedVictim, fIdleWeight, 				AF_PRIORITY_MEDIUM, 0, TRUE)
								SET_ANIM_WEIGHT(pedVictim, fLeftWeight, 				AF_PRIORITY_LOW, 	0, TRUE)
								SET_ANIM_WEIGHT(pedVictim, fRightWeight,				AF_PRIORITY_LOW, 	2, TRUE)
								SET_ANIM_WEIGHT(pedVictim, fBothWeight, 				AF_PRIORITY_LOW, 	1, TRUE)
								
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP0].objObject, fShockLoopTransition, 		AF_PRIORITY_HIGH,	0, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP0].objObject, fIdleWeight, 				AF_PRIORITY_MEDIUM, 0, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP0].objObject, fLeftWeight, 				AF_PRIORITY_LOW, 	0, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP0].objObject, fRightWeight,				AF_PRIORITY_LOW, 	2, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP0].objObject, fBothWeight, 				AF_PRIORITY_LOW, 	1, TRUE)
								
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP1].objObject, fShockLoopTransition, 		AF_PRIORITY_HIGH,	0, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP1].objObject, fIdleWeight, 				AF_PRIORITY_MEDIUM,	0, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP1].objObject, fLeftWeight, 				AF_PRIORITY_LOW, 	0, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP1].objObject, fRightWeight,				AF_PRIORITY_LOW, 	2, TRUE)
								SET_ANIM_WEIGHT(menuWeapon[TOR_CLIP1].objObject, fBothWeight, 				AF_PRIORITY_LOW, 	1, TRUE)
								
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpBattery)
								AND NOT bLookingAtECG
									PRINT_HELP_FOREVER(hlpBattery)
								ENDIF
								
								IF fIdleWeight = 1.0
									bIdlingElectricity = TRUE
								ELSE
									bIdlingElectricity = FALSE
								ENDIF
								
								// >>>>>> Process electrocution damage <<<<<<<
								//---------------------------------------------
								IF fShockLoopTransition = 1.0
									
									IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
										PLAY_ONESHOT()
									ENDIF
									
									INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_ELECTROCUTION)
								
									fTortureForceApplied 	+= TIMESTEP()
									fVictimHealth			-= (F_ELECTRO_DAMAGE/F_ELECTRO_TIME) * TIMESTEP()
									fSmokeEvo 				+= fSmokeChange
									
									IF fTortureForceApplied > F_ELECTRO_TIME
									OR fVictimHealth <= 20.0
										IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
											RunConversation("F3_ENOUGH1")
										ENDIF
									ENDIF
								
								ELSE
								
									IF (fClipGripProgLeft = 1.0 AND fClipGripProgRight = 0.0) 
									OR (fClipGripProgLeft = 0.0 AND fClipGripProgRight = 1.0)
									
										IF NOT bElecgripSpike
											TORTURE_SpikeHeartRate(0.5, 100, 500, 7000)
											iWeaponSelDiaTimer = GET_GAME_TIMER()
											iWeaponSelDiaDelay = 1500
											bElecgripSpike = TRUE
										ENDIF
										
									ELSE
										bElecgripSpike = FALSE
									ENDIF
									
								ENDIF
								
							// Manage Mr pleading and his pain noises
								// Electrocution pain
								IF fShockLoopTransition != 0.0
								
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
								
										IF iTimeSinceLastShock != -1
											IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
												STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
											ENDIF
											
											iTimeSinceLastShock = -1
										ENDIF
										
										IF IS_SCRIPTED_SPEECH_PLAYING(pedVictim)
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ELSE
											IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Electrocution", "MisterK", SPEECH_PARAMS_FORCE_NORMAL)
											ENDIF
										ENDIF
									
									ENDIF
								
								// Post electrocution panting and then pleading with Trevor not to do it
								ELSE
								
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
								
										IF iTimeSinceLastShock = -1
											iTimeSinceLastShock = GET_GAME_TIMER() + 2000
											IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
												STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
											ENDIF
										ENDIF
										
										IF GET_GAME_TIMER() <= iTimeSinceLastShock
											IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Pliers_Panting", "MisterK", SPEECH_PARAMS_FORCE)
											ENDIF
										ELSE
											IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
												IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
													IF IS_SAFE_TO_START_CONVERSATION()
														RunConversation("F3_JUMP", "", TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									
									ENDIF
								
								ENDIF
								
								IF bFXElecSmoulder		//Makes the smoke increase or decrease if player is being elec
								AND DOES_PARTICLE_FX_LOOPED_EXIST(ptfxElecSmoulder)
									fSmokeEvo = CLAMP(fSmokeEvo - fSmokeChange/2, 0.0, 1.0)
									SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxElecSmoulder, "smoke", fSmokeEvo)
								ENDIF
							
							ENDIF
							
							
						BREAK
						CASE 2
							IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_SPARK])
								
								IF IS_SCENE_AT_PHASE(SCENE_ELEC_SPARK, 0.110, 1.0)
									START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(	fxElecSparks, 
																			menuWeapon[ENUM_TO_INT(TOR_CLIP0)].objObject,
																			VECTOR_ZERO, VECTOR_ZERO)
									TORTURE_SpikeHeartRate(0.5, 100, 500, 7000)
									iTortureWeaponStage 	= 0
								ENDIF
								
							ENDIF
						BREAK
					ENDSWITCH
					
				BREAK
				CASE TOR_WATER						//Weapon is Waterboarding
				
					SWITCH iTortureWeaponStage
					
					// WAITING TO FLIP CHAIR
						CASE 0
							IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_IDLE])
								IF HAS_ANIM_DICT_LOADED("MISSFBI3_WATERBOARD")
									// Idle with the jerry can
									IF TORTURE_PlaySyncScene(SCENE_WATER_IDLE, TRUE, 0, SLOW_BLEND_IN, SLOW_BLEND_OUT)
										bJustStarting = FALSE
									ENDIF
								ENDIF
							ELSE
								IF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, TRUE, TRUE)	
									IF TORTURE_PlaySyncScene(SCENE_WATER_FLIP)
										CLEAR_HELP()
										iTortureWeaponStage++
									ENDIF
								
								// Display help text and play dialogue
								ELSE
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpChair)
									AND NOT bLookingAtECG
										PRINT_HELP_FOREVER(hlpChair)
									ENDIF
									
									IF IS_SAFE_TO_START_CONVERSATION()
										IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
											RunConversation("F3_CAN", "", TRUE)
										ENDIF
									ENDIF
								ENDIF
							
							ENDIF
						BREAK
					// TRANSITION: FLIPPING CHAIR
						CASE 1
							IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()	//If skipped chair kick, fade out
							AND NOT bMissionFailed
								SAFE_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
							ENDIF
						
							IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_FLIP])
								IF IS_SCREEN_FADED_OUT()							//If faded out, move on
									SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WATER_FLIP], 1.0)
									SET_ENTITY_VISIBLE(menuWeapon[TOR_RAG].objObject, FALSE)
									bMrKVariations[MR_K_RAG_FACE] = TRUE
									
									IF NOT DOES_ENTITY_EXIST(objs[mof_UV_face_object])
									AND HAS_MODEL_LOADED(mnUVFaceProp)
										objs[mof_UV_face_object] = CREATE_OBJECT(mnUVFaceProp, GET_WORLD_POSITION_OF_ENTITY_BONE(pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_HEAD)))
										ATTACH_ENTITY_TO_ENTITY(objs[mof_UV_face_object], pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_HEAD), <<0,0,0>>, <<0,0,0>>)
										SET_ENTITY_VISIBLE(objs[mof_UV_face_object], FALSE)
									ENDIF
								ELSE
								
									fTortureTime = GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WATER_FLIP])
									
									// Place the rag on his face
									IF fTortureTime >= 0.524
										SET_ENTITY_VISIBLE(menuWeapon[TOR_RAG].objObject, FALSE)
										bMrKVariations[MR_K_RAG_FACE] = TRUE
										
										IF NOT DOES_ENTITY_EXIST(objs[mof_UV_face_object])
										AND HAS_MODEL_LOADED(mnUVFaceProp)
											objs[mof_UV_face_object] = CREATE_OBJECT(mnUVFaceProp, GET_WORLD_POSITION_OF_ENTITY_BONE(pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_HEAD)))
											ATTACH_ENTITY_TO_ENTITY(objs[mof_UV_face_object], pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_HEAD), <<0,0,0>>, <<0,0,0>>)
											SET_ENTITY_VISIBLE(objs[mof_UV_face_object], FALSE)
										ENDIF
									ENDIF

									IF fTortureTime >= 0.096
										IF fTortureTime <= 0.15
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_GBAA", "MisterK", SPEECH_PARAMS_FORCE)
										ELSE
											IF bMrKVariations[MR_K_RAG_FACE]
												IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												AND IS_PED_IN_CURRENT_CONVERSATION(pedVictim)
													KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
												ENDIF
												IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_GCAA", "MisterK", SPEECH_PARAMS_FORCE)
												ENDIF
											ELSE
												IF IS_SAFE_TO_START_CONVERSATION()
													IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES/2
														RunConversation("F3_CAN", "", TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								
								ENDIF
							
							// finished flip, put in to ready idle
							ELSE
								IF TORTURE_PlaySyncScene(SCENE_WATER_READY, TRUE, 0, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
									IF bFXWaterPour
										IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxWaterPour)
											STOP_PARTICLE_FX_LOOPED(ptfxWaterPour)
										ENDIF
										bFXWaterPour = FALSE
									ENDIF
									iWeaponSelDiaTimer = GET_GAME_TIMER() + 2000
									iTortureWeaponStage++
								ENDIF
							ENDIF
						BREAK
					// WAITING TO POUR
						CASE 2
							IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_READY])
							
								IF NOT bLookingAtECG
								
									IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpPour)
											PRINT_HELP_FOREVER(hlpPour)
										ENDIF
									
									ELSE
									
									// Different help if using keyboard and mouse
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_POUR_KM")
											PRINT_HELP_FOREVER("HLP_POUR_KM")
										ENDIF
									
									ENDIF
								
								ENDIF
							
								GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLX, iLY, iRX, iRY)
								
								FUDGE_PC_POURING_CONTROL(iLY) // Fudge for PC version so we can use a different control from Left stick for pouring.
								
								// Started to pour, transition to pour
								IF iLY < -50
								AND NOT bLookingAtECG
									IF TORTURE_PlaySyncScene(SCENE_WATER_READY_TO_POUR, FALSE, 0, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
										clearText(FALSE, FALSE, TRUE)
										iTortureWeaponStage++
									ENDIF
								ELSE
									IF IS_SAFE_TO_START_CONVERSATION()
									AND GET_GAME_TIMER() - i_time_of_last_convo > 8000
										RunConversation("F3_BWAIT")
									ENDIF
									
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
										IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_GCAA", "MisterK", SPEECH_PARAMS_FORCE)
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						BREAK
						
					// [TRANSITION]: FROM: READY 	TO: POUR
						CASE 3
							IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_READY_TO_POUR])	

								GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLX, iLY, iRX, iRY)
								
								FUDGE_PC_POURING_CONTROL(iLY) // Fudge for PC version so we can use a different control from Left stick for pouring.
								
								IF TORTURE_PlaySyncScene(SCENE_WATER_POUR, TRUE)
									IF NOT bFXWaterPour
										ptfxWaterPour = START_PARTICLE_FX_LOOPED_ON_ENTITY(	fxWaterPour, 
															menuWeapon[ENUM_TO_INT(TOR_WATER)].objObject, 
															vWaterPourOffset, VECTOR_ZERO)
										bFXWaterPour = TRUE
									ENDIF
									INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_WATERBOARD)
									
									i_MinPourTimer = GET_GAME_TIMER()
									iTortureWeaponStage++
								ENDIF
								
							ENDIF
							
						BREAK
					// POURING LOOP
						CASE 4
						
							IF GET_GAME_TIMER() - i_MinPourTimer > 500
								IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
									PLAY_ONESHOT()
								ENDIF
							ENDIF
						
							IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_POUR])
							AND GET_GAME_TIMER() - i_MinPourTimer > 1000

								GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLX, iLY, iRX, iRY)
								
								FUDGE_PC_POURING_CONTROL(iLY) // Fudge for PC version so we can use a different control from Left stick for pouring.
							
								// Player has finished this torture
								 
								IF (iLY >= -50 // Player released controls
									AND (fTortureForceApplied > F_WATERBOARD_TIME 	// and torture time was exceeded....
									OR (fTortureForceApplied > F_WATERBOARD_TIME/2.0 AND fVictimHealth <= 10.0)))						// ....or victim was low on health
								OR fVictimHealth <= 0	// OR Victim health deplease

									IF TORTURE_PlaySyncScene(SCENE_WATER_PRE_OUTRO, FALSE, 0, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
										CLEAR_HELP()
									
										IF bFXWaterPour
											IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxWaterPour)
												STOP_PARTICLE_FX_LOOPED(ptfxWaterPour)
											ENDIF
											bFXWaterPour = FALSE
										ENDIF

										iTortureWeaponStage = 6
									ENDIF
																	
								// Player let go early, do not finish the torture yet, transition to the ready loop
								ELIF iLY >= -50
								
									IF TORTURE_PlaySyncScene(SCENE_WATER_POUR_TO_READY, FALSE)
										IF bFXWaterPour
											IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxWaterPour)
												STOP_PARTICLE_FX_LOOPED(ptfxWaterPour)
											ENDIF
											bFXWaterPour = FALSE
										ENDIF
										
										IF DOES_ENTITY_EXIST(objs[mof_UV_face_object])
											SET_ENTITY_VISIBLE(objs[mof_UV_face_object], FALSE)
										ENDIF
										
										KILL_FACE_TO_FACE_CONVERSATION()
										iTortureWeaponStage = 5
									ENDIF

								// still trying to pour
								ELIF iLY < -50
								
									IF DOES_ENTITY_EXIST(objs[mof_UV_face_object])
										SET_ENTITY_VISIBLE(objs[mof_UV_face_object], TRUE)
									ENDIF
								
									fTortureForceApplied += TIMESTEP()
									IF fTortureForceApplied > F_WATERBOARD_TIME
										fVictimHealth -= (F_WATERBOARD_DAMAGE_EXTRA/F_WATERBOARD_TIME) * TIMESTEP()
									ELSE
										fVictimHealth -= (F_WATERBOARD_DAMAGE/F_WATERBOARD_TIME) * TIMESTEP()
									ENDIF
									
									IF fTortureForceApplied > F_WATERBOARD_TIME
										IF IS_SAFE_TO_START_CONVERSATION()
										AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
											RunConversation("F3_ENOUGH2")
										ENDIF
									ENDIF
									
								ENDIF
								
							ENDIF
						BREAK
						CASE 5
							IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_POUR_TO_READY])
								
								IF TORTURE_PlaySyncScene(SCENE_WATER_READY, TRUE, 0, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									IF bFXWaterPour
										ptfxWaterPour = START_PARTICLE_FX_LOOPED_ON_ENTITY(	fxWaterPour, 
															menuWeapon[ENUM_TO_INT(TOR_WATER)].objObject, 
															vWaterPourOffset, VECTOR_ZERO)
										bFXWaterPour = FALSE
									ENDIF
									INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_WATERBOARD)
									iTortureWeaponStage = 2
								ENDIF
							
							ENDIF
						BREAK
						CASE 6
							//IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_PRE_OUTRO])
							TORTURE_ANIM_DATA sAnimData
							IF TORTURE_GetTortureSceneAnimData(SCENE_WATER_PRE_OUTRO, sAnimData)
								IF NOT IS_ENTITY_PLAYING_ANIM(TREV_PED_ID(), sAnimData.strAnimDict, sAnimData.strTrevor)
									// Play outro
									IF TORTURE_PlaySyncScene(SCENE_WATER_OUTRO)
									
										IF DOES_ENTITY_EXIST(objs[mof_UV_face_object])
											DELETE_OBJECT(objs[mof_UV_face_object])
										ENDIF
										
										REMOVE_PARTICLE_FX_IN_RANGE(<<143.1187, -2202.0327, 3.6880>>, 10.0)
										
										fTortureForceApplied = 0
										iWeaponSelStage = 0

										CPRINTLN(DEBUG_MIKE, menuWeapon[TOR_WATER].iTimesSelected)
									
										iTortureStage++		//WATER POURED, MOVE ONTO OUTRO SECTION
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				
				BREAK
				CASE TOR_SYRINGE				//Victim dying, syringe is the weapon
					IF NOT bHasBeenSyringed
						IF NOT bHasPlayedPickupAnim

							IF HAS_ANIM_DICT_LOADED("MISSFBI3_SYRINGE")
								IF TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_PICKUP_SYRINGE)
									fStartPhase = 0.0

									LOAD_ONESHOT("FBI3_SYRINGE")
									
									START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_ADRENALINE")
									
									SET_PC_CONTROLS( "FBI3 MINIGAME DEFAULT" )
									
									bHasPlayedPickupAnim = TRUE
								ENDIF
							ENDIF
							
						ELSE
							IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_SYRINGE]) 
							OR GET_SCRIPT_TASK_STATUS(TREV_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
								bFlatLine = TRUE

								IF TORTURE_Is_Control_Pressed(INPUT_SCRIPT_RT, TRUE, TRUE)	
								
									IF TORTURE_PlaySyncScene(SCENE_SYRINGE_USE)
									
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpSyringe)
											CLEAR_HELP()
										ENDIF
										
										KILL_FACE_TO_FACE_CONVERSATION()

										SETTIMERB(1)
										
										fTargetHealth = 5	//Make this non-zero to fix streaming of wrong cut
										
										iNumberOfSyringesUsed++				//Increase the amount of syringes
										IF iNumberOfSyringesUsed = 1		//Everytime the victim is syringed, the target's health increase less
											fTargetHealth = 80
										ELIF iNumberOfSyringesUsed = 2
											fTargetHealth = 60
										ELIF iNumberOfSyringesUsed = 3
											fTargetHealth = 40
										ELSE iNumberOfSyringesUsed = 4
											fTargetHealth = 20
										ENDIF
										
										menuWeapon[TOR_SYRINGE].iTimesSelected++
										bHasBeenSyringed = TRUE
										iWeaponSelStage = 0
										iTortureStage++
									ENDIF
									
								ELSE
								
									CPRINTLN(DEBUG_MIKE, "iTortureStage: ", iTortureStage, " ", iMrKArrestTimer, "***** iMrKArrestTimer", (GET_GAME_TIMER() - iMrKArrestTimer), "elapsed time")
								
									IF iMrKArrestTimer != -1
									AND GET_GAME_TIMER() - iMrKArrestTimer >= 15000
									
										IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpSyringe)
											CLEAR_HELP()
										ENDIF
										
										CPRINTLN(DEBUG_MIKE,"***** VICTIM DIDN'T GET SYRINGE IN TIME")
										
										iTortureStage = 2
										
										//fVictimHealth = -1
									
									ELIF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_IDLE])
									AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_USE])
									
										IF TORTURE_PlaySyncScene(SCENE_SYRINGE_IDLE, TRUE, 0, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE, SYNCED_SCENE_LOOP_WITHIN_SCENE)						
											iMrKArrestTimer = GET_GAME_TIMER()
										ENDIF
									
									ELIF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_IDLE])
									
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpSyringe)
										AND NOT bLookingAtECG
											PRINT_HELP_FOREVER(hlpSyringe)
										ENDIF
									
									ENDIF
								ENDIF
							// Adjust BPM while picking up the syringe
							ELSE
								IF iWeaponSelStage = 0
									IF IS_SAFE_TO_START_CONVERSATION()
										RunConversation("F3_DIE")
										iWeaponSelStage++
									ENDIF
								ENDIF
							
								IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WEAPON_SELECT_PICKUP_SYRINGE]) >= 0.2
									bFlatLine = TRUE
									bRevived = FALSE
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
										IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
											STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
										ENDIF
									ENDIF
								ELSE
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
										IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "about_to_die", "MisterK", SPEECH_PARAMS_FORCE_NORMAL)
											CPRINTLN(DEBUG_MIKE, "Play Mr K about_to_die")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			// Ped health has arrested, rerequest cutscene and stream variations
			IF fVictimHealth <= 0
			AND iTortureStage = 1
				
				CPRINTLN(DEBUG_MIKE,"*****HIT ARREST AFTER TORTURE STAGE 0")
			
				IF eCurrentMenuWeapon != TOR_WRENCH
					IF eCurrentMenuWeapon = TOR_PLIERS
						TORTURE_Request_Cutscene(TRUE, TOR_PLIERS, iNumberOfTeethPulled+1)
						CPRINTLN(DEBUG_MIKE,"*****LOADED DEATH CUT WITH TOOTH PULL PREDICTED DAMAGE")
					ELSE
						TORTURE_Request_Cutscene(TRUE, eCurrentMenuWeapon)
						CPRINTLN(DEBUG_MIKE,"*****LOADED DEATH CUT PREDICTED DAMAGE (NOT TOOTH PULL OR WRENCH)")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1				//OUTRO ANIM STAGE
			IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_MID])
			AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_LEFT])
			AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_RIGHT])
			AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_PULL_OUT])
			AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_PULL_OUT2])
			AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_OUTRO])
			AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_USE])
			AND (NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_OUTRO]) OR (fVictimHealth <= 0 AND GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WATER_OUTRO]) >= 0.719))
			
				TORTURE_ReleaseAllSounds()
				
				IF DOES_ENTITY_EXIST(objTooth)					
					IF IS_ENTITY_ATTACHED(objTooth)
						DETACH_ENTITY(objTooth)
					ENDIF
					DELETE_OBJECT(objTooth)
					IF bFXBloodThrow
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodThrow)
							STOP_PARTICLE_FX_LOOPED(ptfxBloodThrow)
						ENDIF
						bFXBloodThrow = FALSE
					ENDIF
				ENDIF
				IF bFXBloodMouth
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodMouth)
						STOP_PARTICLE_FX_LOOPED(ptfxBloodMouth)
					ENDIF
					bFXBloodMouth = FALSE
				ENDIF
				IF bFXBloodThrow
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxBloodThrow)
						STOP_PARTICLE_FX_LOOPED(ptfxBloodThrow)
					ENDIF
					bFXBloodThrow = FALSE
				ENDIF
				IF bFXElecSmoulder
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxElecSmoulder)
						STOP_PARTICLE_FX_LOOPED(ptfxElecSmoulder)
					ENDIF
					bFXElecSmoulder = FALSE
				ENDIF
				IF bFXWaterPour
					IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxWaterPour)
						STOP_PARTICLE_FX_LOOPED(ptfxWaterPour)
					ENDIF
					bFXWaterPour = FALSE
				ENDIF
			
				TORTURE_ANIM_DATA sAnimData
				IF TORTURE_GetTortureSceneAnimData(SCENE_ELEC_IDLE, sAnimData)
					Unload_Asset_Anim_Dict(sAssetData, sAnimData.strAnimDict)
				ENDIF
				Unload_Asset_Audio_Bank(sAssetData, sbTortureElec)
				IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_ELECTRIC")
					STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_ELECTRIC")
				ENDIF
			
				IF TORTURE_GetTortureSceneAnimData(SCENE_PLIERS_IDLE, sAnimData)
					Unload_Asset_Anim_Dict(sAssetData, sAnimData.strAnimDict)
				ENDIF
				Unload_Asset_Audio_Bank(sAssetData, sbTortureTeeth)
				Unload_Asset_Audio_Bank(sAssetData, "FBI_03_Torture_Teeth_Pain")
				IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_PLIERS")
					STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_PLIERS")
				ENDIF
			
				IF TORTURE_GetTortureSceneAnimData(SCENE_WRENCH_IDLE, sAnimData)
					Unload_Asset_Anim_Dict(sAssetData, sAnimData.strAnimDict)
				ENDIF
				Unload_Asset_Audio_Bank(sAssetData, sbTortureWrench)
				IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_WRENCH")
					STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_WRENCH")
				ENDIF
			
				IF TORTURE_GetTortureSceneAnimData(SCENE_WATER_IDLE, sAnimData)
					Unload_Asset_Anim_Dict(sAssetData, sAnimData.strAnimDict)
				ENDIF
				Unload_Asset_Audio_Bank(sAssetData, sbTortureWater)
				IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_WATER")
					STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_WATER")
				ENDIF

				
				SET_ENTITY_VISIBLE(propPhoneSteve, TRUE)
				
				// If ped has arrested
				IF fVictimHealth <= 0										
					
					TORTURE_ResetAllProps(FALSE)
					TORTURE_ReleaseAllSounds()
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_HEART_STOPPED)

					// Mr K dies
					IF iNumberOfSyringesUsed >= I_MAX_SYRINGE_USES
					
						CPRINTLN(DEBUG_MIKE,"***** ALL SYRINGES USED, GO STRAIGHT TO DEATH CUT")
						iTortureStage 				= 2
		
					// Allows to revive wtih adrenaline
					ELSE
						CPRINTLN(DEBUG_MIKE,"***** PLAYER HAS CHANCE TO USE SYRING")
					
						iCurrentMenuWeapon 			= ENUM_TO_INT(TOR_SYRINGE)
						iTortureStage 				= 0
						fTortureForceApplied 		= 0
						iWeaponSelStage				= 0
						bHasBeenSyringed			= FALSE
						bHasPlayedPickupAnim		= FALSE
					ENDIF
						
				// Ped not arrested
				ELSE
	
					iTortureStage = 0
					
					IF bTortureMusicPlaying
						TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
						bTortureMusicPlaying = FALSE
					ENDIF
					
					IF TORTURE_GetTortureSceneAnimData(SCENE_SYRINGE_IDLE, sAnimData)
						Unload_Asset_Anim_Dict(sAssetData, sAnimData.strAnimDict)
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_ADRENALINE")
						STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_ADRENALINE")
					ENDIF
					
					clearText()

					CPRINTLN(DEBUG_MIKE,"***** VICTIM DIDN'T ARREST RETURN TRUE")
					RETURN TRUE
				ENDIF
				
			// Not finished playing outro
			ELSE

				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				AND NOT bMissionFailed
					SAFE_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				ENDIF
			
		//>>>>>>>>> TOOTH PULL <<<<<<<<<<<<<<<<<
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_PULL_OUT])
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_PULL_OUT], 1.0)
						
						fVictimHealth -= F_TOOTH_PULL_DAMAGE_FINAL
						
						// Stop torture audio if ped is not going to need the syringe
						IF fVictimHealth > 0.0
							IF bTortureMusicPlaying
								TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
								bTortureMusicPlaying = FALSE
							ENDIF
						ELSE 
							// RE-REQUEST ped is going to arrest, need death cutscene ready
							TORTURE_Request_Cutscene(TRUE, TOR_PLIERS, iNumberOfTeethPulled)							
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedVictim)
					AND NOT IS_PED_INJURED(pedVictim)
						IF IS_SCRIPTED_SPEECH_PLAYING(pedVictim)
							CPRINTLN(DEBUG_MIKE, "Mr K scripted conversation is already going")
							IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
								CPRINTLN(DEBUG_MIKE, "Mr K is playing ambient speech, killing this")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_MIKE, "Mr K is not playing scripted conversation")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								//SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Pliers_Panting", "MisterK", SPEECH_PARAMS_FORCE)
								CPRINTLN(DEBUG_MIKE, "Play Mr K pliers_panting")
							ENDIF
						ENDIF
					ENDIF

					IF NOT DOES_ENTITY_EXIST(objTooth)
					
						IF IS_SCENE_AT_PHASE(SCENE_PLIERS_PULL_OUT, 0.071, 1.0)
					
							objTooth = CREATE_OBJECT(mnTooth, GET_ENTITY_COORDS(TREV_PED_ID()))
	
							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(	fxBloodExtract, pedVictim, <<0,0.1,0>>, VECTOR_ZERO, 
																		BONETAG_HEAD)
							IF NOT bFXBloodThrow
								ptfxBloodThrow = START_PARTICLE_FX_LOOPED_ON_ENTITY(fxBloodThrow, objTooth, 
																					VECTOR_ZERO, VECTOR_ZERO)
								bFXBloodThrow = TRUE
							ENDIF

							IF IS_ENTITY_ATTACHED(objTooth)
								DETACH_ENTITY(objTooth)
							ENDIF
							ATTACH_ENTITY_TO_ENTITY(objTooth, TREV_PED_ID(), 
													GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_L_HAND), 
													VECTOR_ZERO, VECTOR_ZERO)
							
							// Apply damage
							fVictimHealth -= F_TOOTH_PULL_DAMAGE_FINAL
							
							// Stop torture audio if ped is not going to need the syringe
							IF fVictimHealth > 0.0
								IF bTortureMusicPlaying
									TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
									bTortureMusicPlaying = FALSE
								ENDIF
							ELSE
								// RE-REQUEST ped is going to arrest, need death cutscene ready
								TORTURE_Request_Cutscene(TRUE, TOR_PLIERS, iNumberOfTeethPulled)							
							ENDIF
						
						ENDIF
						
					ENDIF
					
				ENDIF
			
		//>>>>>>>>> TOOTH PULL ALTERNATIVE (B) <<<<<<<<<<<<<<<<<
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PLIERS_PULL_OUT2])
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_PLIERS_PULL_OUT2], 1.0)
						
						// Apply damage
						fVictimHealth -= F_TOOTH_PULL_DAMAGE_FINAL
						
						// Stop torture audio if ped is not going to need the syringe
						IF fVictimHealth > 0.0
							IF bTortureMusicPlaying
								TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
								bTortureMusicPlaying = FALSE
							ENDIF
						ELSE
							// RE-REQUEST ped is going to arrest, need death cutscene ready
							TORTURE_Request_Cutscene(TRUE, TOR_PLIERS, iNumberOfTeethPulled)							
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedVictim)
					AND NOT IS_PED_INJURED(pedVictim)
						IF IS_SCRIPTED_SPEECH_PLAYING(pedVictim)
							CPRINTLN(DEBUG_MIKE, "Mr K scripted conversation is already going")
							IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
								CPRINTLN(DEBUG_MIKE, "Mr K is playing ambient speech, killing this")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_MIKE, "Mr K is not playing scripted conversation")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Pliers_Panting", "MisterK", SPEECH_PARAMS_FORCE)
								CPRINTLN(DEBUG_MIKE, "Play Mr K pliers_panting")
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(objTooth)
						
						IF IS_SCENE_AT_PHASE(SCENE_PLIERS_PULL_OUT2, 0.071, 1.0)
							
							objTooth = CREATE_OBJECT(mnTooth, GET_ENTITY_COORDS(TREV_PED_ID()))

							START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(	fxBloodExtract, pedVictim, <<0,0.1,0>>, VECTOR_ZERO, 
																		BONETAG_HEAD)
							IF NOT bFXBloodThrow
								ptfxBloodThrow = START_PARTICLE_FX_LOOPED_ON_ENTITY(fxBloodThrow, objTooth, 
																					VECTOR_ZERO, VECTOR_ZERO)
								bFXBloodThrow = TRUE
							ENDIF
							
							IF IS_ENTITY_ATTACHED(objTooth)
								DETACH_ENTITY(objTooth)
							ENDIF
							
							ATTACH_ENTITY_TO_ENTITY(objTooth, TREV_PED_ID(), 
													GET_PED_BONE_INDEX(TREV_PED_ID(), BONETAG_PH_L_HAND), 
													VECTOR_ZERO, VECTOR_ZERO)
							
							// Apply damage
							fVictimHealth -= F_TOOTH_PULL_DAMAGE_FINAL
							
							// Stop torture audio if ped is not going to need the syringe
							IF fVictimHealth > 0.0
								IF bTortureMusicPlaying
									TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
									bTortureMusicPlaying = FALSE
								ENDIF
							ELSE
								// RE-REQUEST ped is going to arrest, need death cutscene ready
								TORTURE_Request_Cutscene(TRUE, TOR_PLIERS, iNumberOfTeethPulled)							
							ENDIF
							
						ENDIF
						
					ENDIF
				ENDIF
				
			//>>>>>>>>>>>>>>>>
			// WRENCH LEFT
			//>>>>>>>>>>>>>>>>
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_LEFT])
				
					// If skipped
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_LEFT], 1.0)
						IF fTortureForceApplied 	!= 1.0
							fTortureForceApplied 	= 1.0
							fVictimHealth 			-= F_WRENCH_DAMAGE
						ENDIF
						
						IF fVictimHealth <= 0.0
							TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 1)
						ENDIF
					ENDIF
		
					IF DOES_ENTITY_EXIST(pedVictim)
					AND NOT IS_PED_INJURED(pedVictim)
						IF IS_SCENE_AT_PHASE(SCENE_WRENCH_LEFT, 0.395, 0.435)
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_BSAA", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							IF iNumberOfTeethPulled = 0
	//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Wrench_Pain", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							ELSE
	//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Wrench_Pain_Toothless", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_SCENE_AT_PHASE(SCENE_WRENCH_LEFT, 0.395, 1.0)
						
						IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
							PLAY_ONESHOT()
						ENDIF
					
						IF fTortureForceApplied != 1.0
							
							fTortureForceApplied = 1.0
							fVictimHealth 		-= F_WRENCH_DAMAGE
							
							IF fVictimHealth <= 0.0
								TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 1)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
			//>>>>>>>>>>>>>>>>
			// WRENCH MID
			//>>>>>>>>>>>>>>>>
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_MID])
				
					// IF skipped
					IF IS_SCREEN_FADED_OUT()
					
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_MID], 1.0)
						
						IF fTortureForceApplied 	!= 1.0
							fTortureForceApplied 	= 1.0
							fVictimHealth 			-= F_WRENCH_DAMAGE							
						ENDIF
						
						IF fVictimHealth <= 0.0
							TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 2)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedVictim)
					AND NOT IS_PED_INJURED(pedVictim)
						IF IS_SCENE_AT_PHASE(SCENE_WRENCH_MID, 0.186, 0.217)
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_BSAA", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							IF iNumberOfTeethPulled = 0
	//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Wrench_Pain", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							ELSE
	//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Wrench_Pain_Toothless", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_SCENE_AT_PHASE(SCENE_WRENCH_MID, 0.180, 1.0)
					
						IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
							PLAY_ONESHOT()
						ENDIF
					
						IF fTortureForceApplied != 1.0
							
							fTortureForceApplied = 1.0
							fVictimHealth -= F_WRENCH_DAMAGE
							
							IF fVictimHealth <= 0.0
								TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 2)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			
			//>>>>>>>>>>>>>>>>
			// WRENCH RIGHT
			//>>>>>>>>>>>>>>>>
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WRENCH_RIGHT])
					
					// If skipped
					IF IS_SCREEN_FADED_OUT()
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WRENCH_RIGHT], 1.0)
						
						IF fTortureForceApplied 	!= 1.0
							fTortureForceApplied 	= 1.0
							fVictimHealth 			-= F_WRENCH_DAMAGE
						ENDIF
						
						IF fVictimHealth <= 0.0
							TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 0)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedVictim)
					AND NOT IS_PED_INJURED(pedVictim)
						IF IS_SCENE_AT_PHASE(SCENE_WRENCH_RIGHT, 0.245, 0.268)
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_BSAA", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							IF iNumberOfTeethPulled = 0
	//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Wrench_Pain", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							ELSE
	//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Wrench_Pain_Toothless", "MisterK", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	//							ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_SCENE_AT_PHASE(SCENE_WRENCH_RIGHT, 0.245, 1.0)
					
						IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
							PLAY_ONESHOT()
						ENDIF
					
						IF fTortureForceApplied != 1.0
							
							fTortureForceApplied += 1.0
							fVictimHealth -= F_WRENCH_DAMAGE
							
							IF fVictimHealth <= 0.0
								TORTURE_Request_Cutscene(TRUE, TOR_WRENCH, 0)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
				
			/// <<<<<<<<< ELECTROCUTE >>>>>>>>>>>>
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_ELEC_OUTRO])
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_ELEC_OUTRO], 1.0)
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedVictim)
					AND NOT IS_PED_INJURED(pedVictim)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
							
							IF IS_SCENE_AT_PHASE(SCENE_WATER_OUTRO, 0.0, 0.198)	
							
								IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Electrocution", "MisterK", SPEECH_PARAMS_FORCE)
									CPRINTLN(DEBUG_MIKE, "Play Mr K Electrocution")
								ENDIF
							
							ELSE
							
								IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
									IF fVictimHealth <= 0.0
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "about_to_die", "MisterK", SPEECH_PARAMS_FORCE)
										CPRINTLN(DEBUG_MIKE, "Play Mr K about_to_die")
									ELSE
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "Pliers_Panting", "MisterK", SPEECH_PARAMS_FORCE)
										CPRINTLN(DEBUG_MIKE, "Play Mr K Pliers_Panting")
									ENDIF
								ENDIF
							
							ENDIF
							
							
						ENDIF
					ENDIF
					
					IF fVictimHealth > 0.0
						IF bTortureMusicPlaying
							TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
							bTortureMusicPlaying = FALSE
						ENDIF
					ENDIF
					
				ENDIF
				
				
			/// <<<<<<<<< WATER >>>>>>>>>>>>
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WATER_OUTRO])	
				
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WATER_OUTRO], 1.0)
						bMrKVariations[MR_K_RAG_FACE] = FALSE
						SET_ENTITY_VISIBLE(menuWeapon[TOR_RAG].objObject, TRUE)
						SET_ENTITY_VISIBLE(propPhoneSteve, TRUE)
					ENDIF
					
						IF IS_ENTITY_VISIBLE(propPhoneSteve)
							IF IS_SCENE_AT_PHASE(SCENE_WATER_OUTRO, 0.325, 1.0)	
								SET_ENTITY_VISIBLE(propPhoneSteve, FALSE)
							ENDIF
						ENDIF
						
						SWITCH iWeaponSelStage
							CASE 0
								IF DOES_ENTITY_EXIST(pedVictim)
								AND NOT IS_PED_INJURED(pedVictim)
									IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_GCAA", "MisterK", SPEECH_PARAMS_FORCE)
									ENDIF
								ENDIF
							
								IF IS_SAFE_TO_START_CONVERSATION()
									RunConversation("F3_GETUP")
									iWeaponSelStage++
								ENDIF
							BREAK
							CASE 1
								IF IS_SCENE_AT_PHASE(SCENE_WATER_OUTRO, 0.179, 1.0)	
									bMrKVariations[MR_K_RAG_FACE] = FALSE
									SET_ENTITY_VISIBLE(menuWeapon[TOR_RAG].objObject, TRUE)
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
										IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
											STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedVictim)
										ENDIF
									ENDIF
									iWeaponSelStage++
								ELSE
									IF DOES_ENTITY_EXIST(pedVictim)
									AND NOT IS_PED_INJURED(pedVictim)
										IF NOT IS_AMBIENT_SPEECH_PLAYING(pedVictim)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedVictim, "FBI3_GCAA", "MisterK", SPEECH_PARAMS_FORCE)
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 2							
								IF fVictimHealth > 0
									IF IS_SAFE_TO_START_CONVERSATION()
										RunConversation("F3_TOWEL2", "", TRUE)
										iWeaponSelStage++
									ENDIF
								ENDIF
							BREAK
							CASE 3
								IF IS_SAFE_TO_START_CONVERSATION()
									SWITCH eMissionStage
										CASE ST_3_TORTURE_FOR_FIRST			RunConversation("F3_BHIT1", "", TRUE)				BREAK
										CASE ST_5_TORTURE_FOR_SECOND		RunConversation("F3_BHIT2", "", TRUE)				BREAK
										CASE ST_7_TORTURE_FOR_THIRD			RunConversation("F3_BHIT3", "", TRUE)				BREAK
										CASE ST_9_TORTURE_FINAL				RunConversation("F3_BHIT4", "", TRUE)				BREAK
									ENDSWITCH
									iWeaponSelStage++
								ENDIF
							BREAK
						ENDSWITCH
					
					IF fVictimHealth > 0.0
						IF bTortureMusicPlaying
							TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
							bTortureMusicPlaying = FALSE
						ENDIF
					ENDIF

				ENDIF
				
			/// <<<<<<<< SYRINGE >>>>>>>>     
			/// ----------------------------------------------------------------------
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_SYRINGE_USE])
					IF IS_SCREEN_FADED_OUT()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_SYRINGE_USE], 1.0)
						bFlatLine = FALSE
						bJustStarting = FALSE
					ELSE
						SWITCH iWeaponSelStage
							CASE 0
								IF IS_SAFE_TO_START_CONVERSATION()
									// Last shot
									IF I_MAX_SYRINGE_USES - iNumberOfSyringesUsed = 0
										runConversation("F3_OUT")
									ELSE
										runConversation("F3_DIE2")
									ENDIF
									iWeaponSelStage++
								ENDIF
							BREAK
							
						ENDSWITCH
					ENDIF
					
					IF bFlatLine
						IF fVictimHealth <= 0.0
							CPRINTLN(DEBUG_MIKE,"***** SYRINGE USED, REQUEST CUTSCENE AGAIN WITHOUT PREDICTED DAMAGE (DAMAGE IS ALREADY DONE AT THIS POINT)")
							fVictimHealth = 0.0000001
							TORTURE_Request_Cutscene(FALSE)
							iMrKArrestTimer = -1
						ENDIF
						
						IF IS_SCENE_AT_PHASE(SCENE_SYRINGE_USE, 0.045, 1.0)
						
							IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
								PLAY_ONESHOT()
							ENDIF
							
							IF bTortureMusicPlaying
								TRIGGER_MUSIC_EVENT("FBI3_TORTURE_DONE")
								bTortureMusicPlaying = FALSE
							ENDIF
							
							bFlatLine = FALSE
							bJustStarting = FALSE
						ENDIF
					ELSE
						IF bJustStarting = FALSE
							IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_SYRINGE_USE]) >= 0.689
								
								fVictimHealth = fTargetHealth
								bJustStarting = TRUE
								iCurrentMenuWeapon = ENUM_TO_INT(TOR_WRENCH)
								//INFORM_STAT_FBI_THREE_CARDIAC_ARREST()
							ELSE
								//Slide increase mr k health
								fPercentHealComplete = CLAMP(GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_SYRINGE_USE])/(0.689 - 0.045), 0.0, 1.0)
								fVictimHealth = LERP_FLOAT(0, fTargetHealth, fPercentHealComplete)
								
								IF NOT bRevived
								AND IS_SCENE_AT_PHASE(SCENE_SYRINGE_USE, 0.176, 1.0)
								AND IS_SAFE_TO_START_CONVERSATION()
									runConversation("F3_WAKE", "", TRUE)
									bRevived = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF 
		BREAK
	ENDSWITCH
	
	/// Handle ped dead cutscene
	IF iTortureStage = 2
		IF HAS_CUTSCENE_LOADED()
			
			CPRINTLN(DEBUG_MIKE,"***** DEATH SCENE STARTED")
			
			TORTURE_DetachAllProps()
			REGISTER_ENTITY_FOR_CUTSCENE(pedVictim, strMrKCSHandle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, CS_MRK)
			
			REGISTER_ENTITY_FOR_CUTSCENE(pedSteve, csSteve, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			TORTURE_RegisterAllPropsForCutscene(FALSE)
			
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
			START_CUTSCENE()
			
			iTortureStage++
		ENDIF
		
	ELIF iTortureStage = 3
		IF IS_CUTSCENE_PLAYING()
			SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)

			IF GET_CUTSCENE_TIME() >= 38000 - 4500
			OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			
				CPRINTLN(DEBUG_MIKE,"*****DEATH SCENE FAIL START")
			
				eMissionFail 	= FAIL_VICTIM_DEAD
				bMissionFailed 	= TRUE
				iFailTimer		= GET_CUTSCENE_TIME()
				iTortureStage++
			ENDIF
		ENDIF
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedVictim)
	AND NOT IS_PED_INJURED(pedVictim)
		IF IS_AMBIENT_SPEECH_PLAYING(pedVictim)
			SET_PED_CAN_PLAY_VISEME_ANIMS(pedVictim, FALSE, PVF_ALLOW_VISEME_ANIMS_AUDIO)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#ENDIF

FUNC BOOL TORTURE_ManageWeaponSelection()
	IF bMadeSelection
		RETURN FALSE
	ENDIF
	
#IF NOT IS_JAPANESE_BUILD
	
	INT iRightX, iRightY, iLeftX, iLeftY
	
	VECTOR vObject
	FLOAT fObjectZ
	FLOAT fHoverHeight
	FLOAT fTempPhaseHolder
	BOOL bMenuMovedThisFrame 	= FALSE
	
#ENDIF
	
	IF NOT IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	ENDIF
	
	// manage the camera over trevor at the trolley weapon selection
	IF iSelectWeaponStage < 2
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
		
		IF iSelectWeaponStage = 0
			DESTROY_CAM(camGeneralInit)
			DESTROY_CAM(camGeneralDest)
		ENDIF

		IF sSwayTorture.bInitialised
			doSwayCam(sSwayTorture)
		ENDIF
		
	ENDIF
	
	SWITCH iSelectWeaponStage
		CASE 0			
			IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_IDLE])
				TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
			ENDIF
			
			TORTURE_InitWeapSelectionCam()
			doSwayCam(sSwayTorture)
			
			INT iResult, i
			iResult = -1
	
			FOR i = 0 TO 3
				TORTURE_WEAPONS eWeap
				SWITCH i
					CASE 0	eWeap = TOR_PLIERS	BREAK
					CASE 1	eWeap = TOR_CLIP0	BREAK
					CASE 2	eWeap = TOR_WRENCH	BREAK
					CASE 3	eWeap = TOR_WATER	BREAK
				ENDSWITCH
				
				IF iResult = -1
				OR (iResult >= 0 AND menuWeapon[eWeap].iTimesSelected < menuWeapon[iResult].iTimesSelected)
					iResult = enum_to_int(eWeap)
				ENDIF
			ENDFOR
			
			IF iResult != -1
				iCurrentMenuWeapon = iResult
			ENDIF
			
#IF NOT IS_JAPANESE_BUILD
			
			IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_WRENCH)
				fHoverHeight = 0.06
			ELIF iCurrentMenuWeapon = ENUM_TO_INT(TOR_PLIERS)
				fHoverHeight = 0.06
			ELIF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP0)
				fHoverHeight = 0.06
			ELIF iCurrentMenuWeapon = ENUM_TO_INT(TOR_CLIP1)
				fHoverHeight = 0.06
			ELIF iCurrentMenuWeapon = ENUM_TO_INT(TOR_WATER)
				fHoverHeight = 0.04	
			ELSE
				fHoverHeight = 0.02
			ENDIF
		
			IF bInitialWeaponHighlighted
				SET_ENTITY_COORDS_NO_OFFSET(menuWeapon[iCurrentMenuWeapon].objObject, menuWeapon[iCurrentMenuWeapon].vDefaultCoords+<<0,0,fHoverHeight>>)
				menuWeapon[iCurrentMenuWeapon].bIsHighlighted = TRUE
			ENDIF
			
			iWeaponSelDiaTimer 	= GET_GAME_TIMER()
			iWeaponSelDiaDelay 	= 5000
			
#ENDIF

			SAFE_FADE_IN(DEFAULT_FADE_TIME_SHORT)
			
			SET_PC_CONTROLS( "FBI3 MINIGAME SELECT")
			
			iWeaponSelStage		= 0
			iSelectWeaponStage++
		BREAK
		CASE 1
			//IF NOT IS_JAPANESE_VERSION()
			#IF NOT IS_JAPANESE_BUILD
		
				// Manage the look at the ecg
				TORTURE_UpdateLookAtECG()
			
				
				IF NOT bLookingAtECG
					IF IS_PC_VERSION()
					AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("hlp_MENU2")
							PRINT_HELP_FOREVER("hlp_MENU2")
						ENDIF
					ELSE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(hlpMenu)
							PRINT_HELP_FOREVER(hlpMenu)
						ENDIF
					ENDIF
				ENDIF
			
				ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
				
				IF ( ((iLeftX > 50 OR iLeftY > 50) AND NOT bLookingAtECG)
				OR TORTURE_Is_Control_Pressed(INPUT_SCRIPT_PAD_RIGHT, TRUE, TRUE)
				OR TORTURE_Is_Control_Pressed(INPUT_SCRIPT_PAD_DOWN, TRUE, TRUE))
				AND NOT bMenuRightPressed
					bMenuRightPressed = TRUE
					bMenuMovedThisFrame = TRUE
					
					IF bInitialWeaponHighlighted
						iCurrentMenuWeapon++
						IF iCurrentMenuWeapon >= ENUM_TO_INT(TOR_SYRINGE)
							iCurrentMenuWeapon = 0
						ENDIF
						
						WHILE NOT menuWeapon[iCurrentMenuWeapon].bSelectable
							iCurrentMenuWeapon++
							IF iCurrentMenuWeapon >= ENUM_TO_INT(TOR_SYRINGE)
								iCurrentMenuWeapon = 0
							ENDIF
						ENDWHILE
					ENDIF
				ELSE
					IF iLeftX <= 50 
					AND iLeftY <= 50
						bMenuRightPressed = FALSE
					ENDIF
				ENDIF
				IF ( ((iLeftX < -50 OR iLeftY < -50) AND NOT bLookingAtECG)
				OR TORTURE_Is_Control_Pressed(INPUT_SCRIPT_PAD_LEFT, TRUE, TRUE)
				OR TORTURE_Is_Control_Pressed(INPUT_SCRIPT_PAD_UP, TRUE, TRUE))
				AND NOT bMenuLeftPressed
					bMenuLeftPressed = TRUE
					bMenuMovedThisFrame = TRUE
					
					IF bInitialWeaponHighlighted
						iCurrentMenuWeapon--
						IF iCurrentMenuWeapon < 0
							iCurrentMenuWeapon = ENUM_TO_INT(TOR_SYRINGE)-1
						ENDIF
						
						WHILE NOT menuWeapon[iCurrentMenuWeapon].bSelectable
							iCurrentMenuWeapon--
							IF iCurrentMenuWeapon < 0
								iCurrentMenuWeapon = ENUM_TO_INT(TOR_SYRINGE)-1
							ENDIF
						ENDWHILE
					ENDIF
				ELSE
					IF iLeftX >= -50
					AND iLeftY >= -50
						bMenuLeftPressed = FALSE
					ENDIF
				ENDIF
				
				IF bMenuMovedThisFrame
					SWITCH INT_TO_ENUM(TORTURE_WEAPONS, iCurrentMenuWeapon)
						CASE TOR_WRENCH
							PlaySoundFromEntity(SND_MENU_HIGHLIGHT_WRENCH, soundMenuHighlightWrench, 
												menuWeapon[iCurrentMenuWeapon].objObject, FALSE)
						BREAK
						CASE TOR_PLIERS
							PlaySoundFromEntity(SND_MENU_HIGHLIGHT_PLIERS, soundMenuHighlightPliers, 
												menuWeapon[iCurrentMenuWeapon].objObject, FALSE)
						BREAK
						CASE TOR_CLIP0
							PlaySoundFromEntity(SND_MENU_HIGHLIGHT_ELEC, soundMenuHighlightElec, 
												menuWeapon[iCurrentMenuWeapon].objObject, FALSE)
						BREAK
						CASE TOR_WATER
							PlaySoundFromEntity(SND_MENU_HIGHLIGHT_WATER, soundMenuHighlightWater, 
												menuWeapon[iCurrentMenuWeapon].objObject, FALSE)
						BREAK
					ENDSWITCH
					
					bInitialWeaponHighlighted = TRUE
				ENDIF
				
				INT iCounter
				FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1
					menuWeapon[iCounter].bIsHighlighted = FALSE
				ENDFOR
				
				FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1	
					IF bInitialWeaponHighlighted
						IF iCounter = iCurrentMenuWeapon
							menuWeapon[iCounter].bIsHighlighted = TRUE			
							IF iCounter = ENUM_TO_INT(TOR_CLIP0)
								menuWeapon[ENUM_TO_INT(TOR_CLIP1)].bIsHighlighted = TRUE
							ELIF iCounter = ENUM_TO_INT(TOR_WATER)
								menuWeapon[ENUM_TO_INT(TOR_RAG)].bIsHighlighted = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1
					IF menuWeapon[iCounter].bIsHighlighted
						IF iCounter = ENUM_TO_INT(TOR_WRENCH)
							fHoverHeight = 0.06
						ELIF iCounter = ENUM_TO_INT(TOR_PLIERS)
							fHoverHeight = 0.06
						ELIF iCounter = ENUM_TO_INT(TOR_CLIP0)
							fHoverHeight = 0.08
						ELIF iCounter = ENUM_TO_INT(TOR_CLIP1)
							fHoverHeight = 0.06
						ELIF iCounter = ENUM_TO_INT(TOR_WATER)
						OR iCounter = ENUM_TO_INT(TOR_RAG)
							fHoverHeight = 0.04	
						ELSE
							fHoverHeight = 0.02
						ENDIF
						vObject = GET_ENTITY_COORDS(menuWeapon[iCounter].objObject)
						fObjectZ = (menuWeapon[iCounter].vDefaultCoords.z + fHoverHeight - vObject.z)/2
						fObjectZ = SQRT(fObjectZ*fObjectZ)/3.0
						SLIDE_OBJECT(	menuWeapon[iCounter].objObject, menuWeapon[iCounter].vDefaultCoords+<<0,0,fHoverHeight>>, 
										<<0,0,fObjectZ>>, FALSE)
								
						// Weapon highlighting
						IF iCounter != ENUM_TO_INT(TOR_CLIP1)
	
							vObject = GET_ENTITY_COORDS(menuWeapon[iCounter].objObject)
							// Manual positioning for the wrench
							IF iCounter = ENUM_TO_INT(TOR_WRENCH)
								vObject = <<144.0282, -2203.5342, 4.7336>>
							ENDIF
							
							IF iCounter != ENUM_TO_INT(TOR_RAG)	// Rag does not need its own point lights
								// Light the object
								IF iCounter != ENUM_TO_INT(TOR_WATER) // jerry can does not need extra lighting on the object
									DRAW_LIGHT_WITH_RANGE(<<vObject.x, vObject.y, vObject.z+0.05>>, 255, 255, 255, 0.2, 0.5)
								ENDIF
								
								// Little circle of light underneath the weapon
								DRAW_LIGHT_WITH_RANGE(<<vObject.x, vObject.y, 4.69>> , 255, 255, 255, 0.2, 0.5)
							ENDIF
						ENDIF
					
					ELSE
						vObject = GET_ENTITY_COORDS(menuWeapon[iCounter].objObject)
						fObjectZ = (menuWeapon[iCounter].vDefaultCoords.z - vObject.z)/2
						fObjectZ = SQRT(fObjectZ*fObjectZ)/3.0
						SLIDE_OBJECT(	menuWeapon[iCounter].objObject, menuWeapon[iCounter].vDefaultCoords, 
										<<0,0,fObjectZ>>, FALSE)
					ENDIF
				ENDFOR
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
				AND NOT bMissionFailed
					
					IF iCurrentMenuWeapon != enum_to_int(TOR_WRENCH)
						IF iCurrentMenuWeapon = enum_to_int(TOR_PLIERS)
							TORTURE_Request_Cutscene(TRUE, TOR_PLIERS, iNumberOfTeethPulled+1)
						ELSE
							TORTURE_Request_Cutscene(TRUE, int_to_enum(TORTURE_WEAPONS, iCurrentMenuWeapon))
						ENDIF
					ENDIF
					
					// Select appropriate PC control scheme for the weapon selected.
					IF IS_PC_VERSION()
						IF iCurrentMenuWeapon = enum_to_int(TOR_CLIP0)
							SET_PC_CONTROLS( "FBI3 MINIGAME ELECTRODES")	
						ELSE
							SET_PC_CONTROLS( "FBI3 MINIGAME DEFAULT")	
						ENDIF
					ENDIF
					
					menuWeapon[iCurrentMenuWeapon].iTimesSelected++
					
					KILL_FACE_TO_FACE_CONVERSATION()
					fStartPhase = 0.0
					iTortureWeaponStage = 0
					clearText(false, true, true)
					iWeaponSelStage = 0
					iSelectWeaponStage++
				
				// not selected a weapon, run some random dialogue
				ELSE
					IF IS_SAFE_TO_START_CONVERSATION()
					AND (GET_GAME_TIMER() - iWeaponSelDiaTimer >= iWeaponSelDiaDelay)
						
						TEXT_LABEL_15 strTrevDia
						TEXT_LABEL_15 strSteveDia
						TEXT_LABEL_15 strMrKDia
						
						SWITCH eMissionStage
							CASE ST_3_TORTURE_FOR_FIRST
								strTrevDia 	= "F3_HMM1"
								strSteveDia = "F3_TORGO1"
								strMrKDia	= "F3_KMENU"
							BREAK
							CASE ST_5_TORTURE_FOR_SECOND
								strTrevDia 	= "F3_HMM2"
								strSteveDia = "F3_TORGO2"
								strMrKDia	= "F3_WTF2"
							BREAK
							CASE ST_7_TORTURE_FOR_THIRD
								strTrevDia 	= "F3_HMM3"
								strSteveDia = "F3_TORGO3"
								strMrKDia	= "F3_WTF3"
							BREAK
							CASE ST_9_TORTURE_FINAL
								strTrevDia 	= "F3_HMM4"
								strSteveDia = "F3_TORGO4"
								strMrKDia	= "F3_WTF4"
							BREAK
						ENDSWITCH
						
						SWITCH iWeaponSelStage
							CASE 0
								IF IS_SAFE_TO_START_CONVERSATION()
									RunConversation(strSteveDia)
									iWeaponSelStage++
								ENDIF
							BREAK
							CASE 1
							CASE 2
							CASE 4
							CASE 5
							CASE 6
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
									RunConversation(strMrKDia, "", TRUE)
									iWeaponSelStage++
								ENDIF
							BREAK
							CASE 3
								IF IS_SAFE_TO_START_CONVERSATION()
									RunConversation(strTrevDia)
									iWeaponSelStage++
								ENDIF
							BREAK
						ENDSWITCH
						IF iWeaponSelStage > 6
							iWeaponSelStage = 0
						ENDIF
					ENDIF
				ENDIF
			#ENDIF
				
		// JP VERSION
		//---------------------------------
			#IF IS_JAPANESE_BUILD
				TEXT_LABEL_15 strTrevDia
			
				SWITCH eMissionStage
					CASE ST_3_TORTURE_FOR_FIRST
						strTrevDia 	= "F3_HMM1"
					BREAK
					CASE ST_5_TORTURE_FOR_SECOND
						strTrevDia 	= "F3_HMM2"
					BREAK
					CASE ST_7_TORTURE_FOR_THIRD
						strTrevDia 	= "F3_HMM3"
					BREAK
					CASE ST_9_TORTURE_FINAL
						strTrevDia 	= "F3_HMM4"
					BREAK
				ENDSWITCH
				
				SWITCH iWeaponSelStage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
							RunConversation(strTrevDia)
							iWeaponSelStage++
						ENDIF
					BREAK
					CASE 1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
							bHasPlayedPickupAnim 	= TRUE
							iSelectWeaponStage++
						ENDIF
					BREAK
				ENDSWITCH
			#ENDIF
		BREAK 
		CASE 2			//Play pickup anims
			IF NOT bHasPlayedPickupAnim

#IF NOT IS_JAPANESE_BUILD
			
				TORTURE_ANIM_DATA sAnimData

				SWITCH int_to_enum(TORTURE_WEAPONS, iCurrentMenuWeapon)
					CASE TOR_WRENCH
						IF TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_PICKUP_WRENCH, FALSE, fStartPhase)
						
							IF TORTURE_GetTortureSceneAnimData(SCENE_WRENCH_IDLE, sAnimData)
								Load_Asset_AnimDict(sAssetData, sAnimData.strAnimDict)
								Load_Asset_Audiobank(sAssetData, sbTortureWrench)
								
								LOAD_ONESHOT("FBI3_WRENCH_HIT")
								START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_WRENCH")

								IF TORTURE_GetTortureSceneAnimData(SCENE_WEAPON_SELECT_PICKUP_WRENCH, sAnimData)
									FIND_ANIM_EVENT_PHASE(sAnimData.strAnimDict, sAnimData.strTrevor,
											eventEarlyCutIn, fStartPhase, fTempPhaseHolder)
								ENDIF
								
								IF IS_SAFE_TO_START_CONVERSATION()
									SWITCH eMissionStage				
										CASE ST_3_TORTURE_FOR_FIRST			RunConversation("F3_WREN1", "", TRUE)			BREAK
										CASE ST_5_TORTURE_FOR_SECOND		RunConversation("F3_WREN2", "", TRUE)			BREAK
										CASE ST_7_TORTURE_FOR_THIRD			RunConversation("F3_WREN3", "", TRUE)			BREAK
										CASE ST_9_TORTURE_FINAL				RunConversation("F3_WREN4", "", TRUE)			BREAK
									ENDSWITCH
								ENDIF
								
								bHasPlayedPickupAnim = TRUE							
							ENDIF
							
						ENDIF
					BREAK
					CASE TOR_CLIP0
						IF TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_PICKUP_CLIP0, FALSE, fStartPhase)
						
							IF TORTURE_GetTortureSceneAnimData(SCENE_ELEC_IDLE, sAnimData)
								Load_Asset_AnimDict(sAssetData, sAnimData.strAnimDict)
								Load_Asset_Audiobank(sAssetData, sbTortureElec)
								
								LOAD_ONESHOT("FBI3_BATTERY_GRIPPED")
								START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_ELECTRIC")

								IF TORTURE_GetTortureSceneAnimData(SCENE_WEAPON_SELECT_PICKUP_CLIP0, sAnimData)
									FIND_ANIM_EVENT_PHASE(sAnimData.strAnimDict, sAnimData.strTrevor,
											eventEarlyCutIn, fStartPhase, fTempPhaseHolder)
								ENDIF

								IF IS_SAFE_TO_START_CONVERSATION()
									RunConversation("F3_EWAIT")
								ENDIF
								bHasPlayedPickupAnim = TRUE
							ENDIF
								
						ENDIF
					BREAK
					CASE TOR_PLIERS
						IF TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_PICKUP_PLIERS, FALSE, fStartPhase)
						
							IF TORTURE_GetTortureSceneAnimData(SCENE_PLIERS_IDLE, sAnimData)
								Load_Asset_AnimDict(sAssetData, sAnimData.strAnimDict)
								Load_Asset_Model(sAssetData, mnTooth)
								Load_Asset_Audiobank(sAssetData, sbTortureTeeth)
								Load_Asset_Audiobank(sAssetData, "FBI_03_Torture_Teeth_Pain")
								
								LOAD_ONESHOT("FBI3_TOOTH_PULLED")
								START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_PLIERS")

								IF TORTURE_GetTortureSceneAnimData(SCENE_WEAPON_SELECT_PICKUP_PLIERS, sAnimData)
									FIND_ANIM_EVENT_PHASE(sAnimData.strAnimDict, sAnimData.strTrevor,
											eventEarlyCutIn, fStartPhase, fTempPhaseHolder)
								ENDIF
							
								IF IS_SAFE_TO_START_CONVERSATION()
									runConversation("F3_PWAIT")
								ENDIF
								bHasPlayedPickupAnim = TRUE
							ENDIF
							
						ENDIF
					BREAK
					CASE TOR_WATER
						IF TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_PICKUP_WATER, FALSE, fStartPhase)
						
							IF TORTURE_GetTortureSceneAnimData(SCENE_WATER_IDLE, sAnimData)
								Load_Asset_AnimDict(sAssetData, sAnimData.strAnimDict)
								Load_Asset_Audiobank(sAssetData, sbTortureWater)
								
								LOAD_ONESHOT("FBI3_WATER_BOARDED")
								START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_WATER")

								IF TORTURE_GetTortureSceneAnimData(SCENE_WEAPON_SELECT_PICKUP_WATER, sAnimData)
									FIND_ANIM_EVENT_PHASE(sAnimData.strAnimDict, sAnimData.strTrevor,
											eventEarlyCutIn, fStartPhase, fTempPhaseHolder)
								ENDIF
										
								IF IS_SAFE_TO_START_CONVERSATION()			
									RunConversation("F3_KWAIT")
								ENDIF
								bHasPlayedPickupAnim = TRUE
							ENDIF
							
						ENDIF
					BREAK
				ENDSWITCH
				
				IF bHasPlayedPickupAnim
					IF TORTURE_GetTortureSceneAnimData(SCENE_SYRINGE_IDLE, sAnimData)
						Load_Asset_AnimDict(sAssetData, sAnimData.strAnimDict)
					ENDIF
				ENDIF
				
#ENDIF			
					
			ELSE
				BOOL bSelectDone
				bSelectDone = FALSE
				#IF IS_JAPANESE_BUILD
					bSelectDone = TRUE
				#ENDIF

				IF iCurrentMenuWeapon = ENUM_TO_INT(TOR_SYRINGE)
					IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_SYRINGE])
						bSelectDone = TRUE
					ENDIF
					
				ELIF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_WRENCH])
				AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_PLIERS])
				AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_CLIP0])
				AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_WATER])
				AND NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_SYRINGE])
					
					bSelectDone = TRUE
					
				ENDIF				
				
				IF bSelectDone
					
					iSelectWeaponStage = 0
					
					RETURN TRUE
				ELSE
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					AND NOT bMissionFailed
						SAFE_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
					ENDIF
				
					IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_WRENCH])
						IF IS_SCREEN_FADED_OUT()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WEAPON_SELECT_PICKUP_WRENCH], 1.0)
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
								SWITCH iWeaponSelStage
									CASE 0				RunConversation("F3_NOWREN", "", TRUE)	iWeaponSelStage++		BREAK
									CASE 1			
										IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
											RunConversation("F3_NOWREN", "", TRUE)			
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_PLIERS])
						IF IS_SCREEN_FADED_OUT()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WEAPON_SELECT_PICKUP_PLIERS], 1.0)
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
								SWITCH iWeaponSelStage
									CASE 0				RunConversation("F3_NOPLI", "", TRUE)	iWeaponSelStage++		BREAK
									CASE 1			
										IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
											RunConversation("F3_NOPLI", "", TRUE)			
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_WATER])
						IF IS_SCREEN_FADED_OUT()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WEAPON_SELECT_PICKUP_WATER], 1.0)
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
								SWITCH iWeaponSelStage
									CASE 0				RunConversation("F3_NOCAN", "", TRUE)	iWeaponSelStage++		BREAK
									CASE 1				RunConversation("F3_CAN", "", TRUE)		iWeaponSelStage++		BREAK
									CASE 2			
										IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
											RunConversation("F3_CAN", "", TRUE)			
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_SYRINGE])
						IF IS_SCREEN_FADED_OUT()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WEAPON_SELECT_PICKUP_SYRINGE], 1.0)
						ENDIF
					ENDIF
				
					IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_WEAPON_SELECT_PICKUP_CLIP0])
						IF IS_SCREEN_FADED_OUT()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_WEAPON_SELECT_PICKUP_CLIP0], 1.0)
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
								SWITCH iWeaponSelStage
									CASE 0				RunConversation("F3_NOJUMP", "", TRUE)	iWeaponSelStage++		BREAK
									CASE 1				RunConversation("F3_JUMP", "", TRUE)	iWeaponSelStage++		BREAK
									CASE 2			
										IF GET_GAME_TIMER() - i_time_of_last_convo > I_TIME_BETWEEN_PLEES
											RunConversation("F3_JUMP", "", TRUE)			
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//PURPOSE: Loads the assets needed for the sniping prone scene with Michael and Dave
FUNC BOOL PARTY_LoadAssets(BOOL bDriveToStage, BOOL bIncParty, BOOL bDontLoadProne = FALSE, BOOL bDontLoadCoffee = FALSE)

	CONST_INT 		I_START_LOADING_COFFEE_ASSETS 			0
	CONST_INT 		I_START_LOADING_PRONE_ASSETS 			0
	CONST_INT 		I_START_LOADING_REPLAY_VEH_ASSETS 		5
	CONST_INT 		I_START_LOADING_PARTY_ASSETS 			8
	CONST_INT 		I_PARTY_LOADING_FINISHED 				23	

	IF bDriveToStage
		
		// Coffee
		IF NOT bDontLoadCoffee
			SWITCH iAssetRequests
				CASE I_START_LOADING_COFFEE_ASSETS
					Load_Asset_Model(sAssetData, P_ING_COFFEECUP_01)
					iAssetRequests++
				BREAK
				CASE 1
					IF HAS_MODEL_LOADED(P_ING_COFFEECUP_01)
						Load_Asset_Model(sAssetData, PROP_NPC_PHONE)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 2
					IF HAS_MODEL_LOADED(PROP_NPC_PHONE)
						Load_Asset_Model(sAssetData, PROP_CHAIR_01A)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 3
					IF HAS_MODEL_LOADED(PROP_CHAIR_01A)
						Load_Asset_AnimDict(sAssetData, adCoffee)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 4
					IF HAS_ANIM_DICT_LOADED(adCoffee)
						iAssetRequests++
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF iAssetRequests < I_START_LOADING_REPLAY_VEH_ASSETS
				iAssetRequests = I_START_LOADING_REPLAY_VEH_ASSETS
			ENDIF
		ENDIF
		
		// Replay vehicle
		IF iAssetRequests >= I_START_LOADING_REPLAY_VEH_ASSETS
			IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				SWITCH iAssetRequests
					CASE I_START_LOADING_REPLAY_VEH_ASSETS
						CPRINTLN(DEBUG_MIKE, "PARTY_LoadAssets(): Made replay checkpoint vehicle model request MODEL:", GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
//						IF Load_Asset_Model(sAssetData, GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
//							CPRINTLN(DEBUG_MIKE, "PARTY_LoadAssets(): Asset management requested MODEL:", GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
//						ELSE
//							CPRINTLN(DEBUG_MIKE, "PARTY_LoadAssets(): Asset management thinks MODEL:", GET_REPLAY_CHECKPOINT_VEHICLE_MODEL(), " is already loaded.")
//						ENDIF
						iAssetRequests++
					BREAK
					CASE 6
						IF HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
							iAssetRequests = I_START_LOADING_PARTY_ASSETS
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				IF iAssetRequests <= I_START_LOADING_REPLAY_VEH_ASSETS
					iAssetRequests = I_START_LOADING_PARTY_ASSETS
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF NOT bDontLoadProne
			SWITCH iAssetRequests
				CASE I_START_LOADING_PRONE_ASSETS
					Load_Asset_AnimDict(sAssetData, adSniping)
					iAssetRequests++
				BREAK
				CASE 1
					IF HAS_ANIM_DICT_LOADED(adSniping)
						Load_Asset_Model(sAssetData, PROP_NPC_PHONE)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 2
					IF HAS_MODEL_LOADED(PROP_NPC_PHONE)
						Load_Asset_Weapon_Asset(sAssetData, weapMichaelsSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 3
					IF HAS_WEAPON_ASSET_LOADED(weapMichaelsSniper)
						Load_Asset_Model(sAssetData, PROP_BINOC_01)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 4
					IF HAS_MODEL_LOADED(PROP_BINOC_01)
						iAssetRequests = I_START_LOADING_PARTY_ASSETS
					ENDIF
				BREAK
			ENDSWITCH
		
		ELSE
			IF iAssetRequests < I_START_LOADING_PARTY_ASSETS
				iAssetRequests = I_START_LOADING_PARTY_ASSETS
			ENDIF
		ENDIF
	ENDIF
	
	IF iAssetRequests >= I_START_LOADING_PARTY_ASSETS
		IF bIncParty
			SWITCH iAssetRequests
				CASE I_START_LOADING_PARTY_ASSETS
					Load_Asset_PTFX(sAssetData)
					iAssetRequests++
				BREAK
				CASE 9
					IF HAS_PTFX_ASSET_LOADED()
						Load_Asset_Model(sAssetData, mnTarget)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 10
					IF HAS_MODEL_LOADED(mnTarget)
						Load_Asset_Model(sAssetData, mnPartyMale1)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 11
					IF HAS_MODEL_LOADED(mnPartyMale1)
						Load_Asset_Model(sAssetData, mnPartyFem)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 12
					IF HAS_MODEL_LOADED(mnPartyFem)
						Load_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 13
					IF HAS_MODEL_LOADED(PROP_CS_CIGGY_01B)
						Load_Asset_Model(sAssetData, P_FAG_PACKET_01_S)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 14
					IF HAS_MODEL_LOADED(P_FAG_PACKET_01_S)
						Load_Asset_Model(sAssetData, PROP_FBI3_COFFEE_TABLE)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 15
					IF HAS_MODEL_LOADED(PROP_FBI3_COFFEE_TABLE)
						Load_Asset_AnimDict(sAssetData, "MISSFBI3_PARTY")
						iAssetRequests++
					ENDIF
				BREAK
				CASE 16
					IF HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY")
						Load_Asset_AnimDict(sAssetData, "MISSFBI3_PARTY_B")
						iAssetRequests++
					ENDIF
				BREAK
				CASE 17
					IF HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_B")
						Load_Asset_AnimDict(sAssetData, "MISSFBI3_PARTY_C")
						iAssetRequests++
					ENDIF
				BREAK
				CASE 18
					IF HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_C")
						Load_Asset_AnimDict(sAssetData, "MISSFBI3_PARTY_D")
						iAssetRequests++
					ENDIF
				BREAK
				CASE 19
					IF HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_D")
						Load_Asset_AnimDict(sAssetData, "MISSFBI3_PARTY_E")
						iAssetRequests++
					ENDIF
				BREAK
				CASE 20
					IF HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_E")
						Load_Asset_AnimDict(sAssetData, adPartyGetUpFlee)
						iAssetRequests++
					ENDIF
				BREAK
				CASE 21
					IF HAS_ANIM_DICT_LOADED(adPartyGetUpFlee)
						Load_Asset_Audiobank(sAssetData, "FBI_03_Panic")
						iAssetRequests++
					ENDIF
				BREAK
				CASE 22
					IF REQUEST_SCRIPT_AUDIO_BANK("FBI_03_Panic")
						iAssetRequests = I_PARTY_LOADING_FINISHED
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF iAssetRequests < I_PARTY_LOADING_FINISHED
				iAssetRequests = I_PARTY_LOADING_FINISHED
			ENDIF
		ENDIF
	ENDIF
	
	IF iAssetRequests = I_PARTY_LOADING_FINISHED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Sets up michael with the sniper with the max zoom scope. Also makes sure the ped has minimum 20 bullets.
PROC PARTY_GivePedCustomSniperRifle(PED_INDEX ped)
	IF ped = MIKE_PED_ID()
		CPRINTLN(DEBUG_MIKE, "GIVING CUSTOM SNIPER TO PLAYER")
	ELSE
		CPRINTLN(DEBUG_MIKE, "GIVING CUSTOM SNIPER TO PED")
	ENDIF
	IF NOT HAS_PED_GOT_WEAPON(ped, weapMichaelsSniper)
		GIVE_WEAPON_TO_PED(ped, weapMichaelsSniper, 20, FALSE, FALSE)
		GIVE_WEAPON_COMPONENT_TO_PED(ped, weapMichaelsSniper, WEAPONCOMPONENT_AT_SCOPE_MAX)
		SET_CURRENT_PED_WEAPON(ped, weapMichaelsSniper, TRUE)
		CPRINTLN(DEBUG_MIKE, "NEW WEAPON")
	ELSE
		SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_UNARMED, FALSE)
		SET_PED_AMMO(ped, weapMichaelsSniper, 20)
		GIVE_WEAPON_COMPONENT_TO_PED(ped, weapMichaelsSniper, WEAPONCOMPONENT_AT_SCOPE_MAX)
		SET_CURRENT_PED_WEAPON(ped, weapMichaelsSniper, TRUE)	
		CPRINTLN(DEBUG_MIKE, "EXISTING SNIPER")
	ENDIF
	
ENDPROC

//PURPOSE: Checks if the player has looked at every bit of the house
PROC PARTY_ScopeCheck()
	CONST_FLOAT fRadius 0.1	
	
	IF GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR() >= F_ZOOM_SCOPE_CHECK
		IF NOT bScoped[HS_CENTRE]
			IF IS_SPHERE_VISIBLE(vHouseSectionCentre[HS_CENTRE], fRadius)
				bScoped[HS_CENTRE] = TRUE
			ENDIF
		ENDIF
		IF NOT bScoped[HS_HIGH_LEFT]
			IF IS_SPHERE_VISIBLE(vHouseSectionCentre[HS_HIGH_LEFT], fRadius)
				bScoped[HS_HIGH_LEFT] = TRUE
			ENDIF
		ENDIF
		IF NOT bScoped[HS_HIGH_RIGHT]
			IF IS_SPHERE_VISIBLE(vHouseSectionCentre[HS_HIGH_RIGHT], fRadius)
				bScoped[HS_HIGH_RIGHT] = TRUE
			ENDIF
		ENDIF
		IF NOT bScoped[HS_LOW_LEFT]
			IF IS_SPHERE_VISIBLE(vHouseSectionCentre[HS_LOW_LEFT], fRadius)
				bScoped[HS_LOW_LEFT] = TRUE
			ENDIF
		ENDIF
		IF NOT bScoped[HS_LOW_RIGHT]
			IF IS_SPHERE_VISIBLE(vHouseSectionCentre[HS_LOW_RIGHT], fRadius)
				bScoped[HS_LOW_RIGHT] = TRUE
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

//PURPOSE: Sets all the scope booleans to false, ready to check if the player has looked at the entire house
PROC PARTY_ResetScopeCheck()
	INT i
	REPEAT COUNT_OF(bScoped) i
		bScoped[i] = FALSE
	ENDREPEAT
ENDPROC

PTFX_ID ptfx_Cigs[3]

//PURPOSE: Manages the activities of the party goers, some specific events based on where the player is looking, etc
PROC PARTY_ManagePartyScene(BOOL bStartMusic = FALSE)
	
	CONST_FLOAT fTargetHeading	259.4096

	IF NOT bPlayingMusic
		TEXT_LABEL_15 strStream
	
		SWITCH eMissionStage
			CASE ST_6_REACT_TO_SECOND
				strStream = "Party"
			BREAK
			CASE ST_8_REACT_TO_THIRD
				strStream = "Party_2"
			BREAK
			CASE ST_10_REACT_FINAL
				strStream = "Party_3"
			BREAK
		ENDSWITCH
	
		IF LOAD_STREAM(strStream, "FBI_03_Torture_Sounds")
		
			IF bStartMusic
				SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
				PLAY_STREAM_FROM_POSITION(<<-3024.7515, 332.2954, 13.6596>>)
				START_AUDIO_SCENE(audsceneZoom)
				fAudioZoom = 0.0
				fAudioZoomPreviousFrame = 0.0
				SET_VARIABLE_ON_STREAM("ZoomLevel", fAudioZoom)
				SET_AUDIO_SCENE_VARIABLE(audsceneZoom, "apply", fAudioZoom)
				bPlayingMusic = TRUE
				CPRINTLN(DEBUG_MIKE, "*** MUSIC STARTED FOR FBI3 ***")
			ENDIF
		ENDIF
	ENDIF

	PARTY_ScopeCheck()
	
	// PARTY TARGET AND DECOYS
	IF eMissionStage = ST_10_REACT_FINAL
		IF DOES_ENTITY_EXIST(peds[mpf_target].id) 
		AND NOT IS_PED_INJURED(peds[mpf_target].id) 
		AND NOT peds[mpf_target].bFleeing
		
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_target].id, SCRIPT_TASK_PERFORM_SEQUENCE)
			
				SET_ENTITY_COORDS( peds[mpf_target].id, vTargetPedSlots[iTargetPedSlot[0]] )
		
				OPEN_SEQUENCE_TASK(seqSequence)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_A", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_C", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_D", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_B", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_A", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_C", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_D", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_B", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_C", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_A", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_D", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Male_B", vTargetPedSlots[iTargetPedSlot[0]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
					SET_SEQUENCE_TO_REPEAT(seqSequence, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(seqSequence)
				TASK_PERFORM_SEQUENCE(peds[mpf_target].id, seqSequence)
				CLEAR_SEQUENCE_TASK(seqSequence)
			
			ELSE
				IF HAS_ANIM_EVENT_FIRED(peds[mpf_target].id, HASH("exhale"))
					START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", peds[mpf_target].id, <<0,0,0>>,<<0,0,0>>, BONETAG_HEAD, 1.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	// decoy female smoker
	IF DOES_ENTITY_EXIST(peds[mpf_decoy_f].id) 
	AND NOT IS_PED_INJURED(peds[mpf_decoy_f].id) 
	AND NOT peds[mpf_decoy_f].bFleeing
	
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_decoy_f].id, SCRIPT_TASK_PERFORM_SEQUENCE)
		
			SET_ENTITY_COORDS( peds[mpf_decoy_f].id, vTargetPedSlots[iTargetPedSlot[1]] )
	
			OPEN_SEQUENCE_TASK(seqSequence)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_A", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_C", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_D", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_B", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_A", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_C", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_D", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_B", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_C", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_A", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_D", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_LH_Female_B", vTargetPedSlots[iTargetPedSlot[1]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				SET_SEQUENCE_TO_REPEAT(seqSequence, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seqSequence)
			TASK_PERFORM_SEQUENCE(peds[mpf_decoy_f].id, seqSequence)
			CLEAR_SEQUENCE_TASK(seqSequence)
		
		ELSE
			IF HAS_ANIM_EVENT_FIRED(peds[mpf_decoy_f].id, HASH("exhale"))
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", peds[mpf_decoy_f].id, <<0,0,0>>,<<0,0,0>>, BONETAG_HEAD, 1.0)
			ENDIF
		ENDIF
	ENDIF

	// decoy male smoker
	IF DOES_ENTITY_EXIST(peds[mpf_decoy_m].id)
	AND NOT IS_PED_INJURED(peds[mpf_decoy_m].id) 
	AND NOT peds[mpf_decoy_m].bFleeing
		
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_decoy_m].id, SCRIPT_TASK_PERFORM_SEQUENCE)
		
			SET_ENTITY_COORDS( peds[mpf_decoy_m].id, vTargetPedSlots[iTargetPedSlot[2]] )
	
			OPEN_SEQUENCE_TASK(seqSequence)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_A", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_C", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_D", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_B", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_A", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_C", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_D", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_B", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_C", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_A", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_D", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				TASK_PLAY_ANIM_ADVANCED(null, "MISSFBI3_PARTY_E", "Target_Smoke_RH_Male_B", vTargetPedSlots[iTargetPedSlot[2]], <<0,0,fTargetHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_NOT_INTERRUPTABLE | AF_DISABLE_FORCED_PHYSICS_UPDATE)
				SET_SEQUENCE_TO_REPEAT(seqSequence, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(seqSequence)
			TASK_PERFORM_SEQUENCE(peds[mpf_decoy_m].id, seqSequence)
			CLEAR_SEQUENCE_TASK(seqSequence)
		ELSE
			IF HAS_ANIM_EVENT_FIRED(peds[mpf_decoy_m].id, HASH("exhale"))
				START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("cs_cig_exhale_mouth", peds[mpf_decoy_m].id, <<0,0,0>>,<<0,0,0>>, BONETAG_HEAD, 1.0)
			ENDIF
		ENDIF
	ENDIF
		
	// COKE SCENE
	IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PARTY_A])

		// Coke snort scene
		IF bScoped[HS_HIGH_LEFT]														// player has looked here
		AND eMissionStage != ST_8_REACT_TO_THIRD										// not in the second stage when the target is standing watching the guy prepping the coke
		AND (NOT bHasCokeSnortPlayed													// the anim has not played before
			OR (bHasCokeSnortPlayed AND GET_GAME_TIMER() - iCokeSnortTimer >= 15000) )	// the anim has played before but is was 90+ seconds ago
			
			IF (NOT IS_PED_INJURED(peds[mpf_party_coke_m_1].id) AND NOT peds[mpf_party_coke_m_1].bFleeing)
			OR (NOT IS_PED_INJURED(peds[mpf_party_coke_m_3].id) AND NOT peds[mpf_party_coke_m_3].bFleeing)
			OR (NOT IS_PED_INJURED(peds[mpf_party_coke_m_5].id) AND NOT peds[mpf_party_coke_m_5].bFleeing)
			OR (NOT IS_PED_INJURED(peds[mpf_party_coke_f_1].id) AND NOT peds[mpf_party_coke_f_1].bFleeing)
			OR ((eMissionStage = ST_5_TORTURE_FOR_SECOND OR eMissionStage = ST_6_REACT_TO_SECOND OR eMissionStage = ST_7_TORTURE_FOR_THIRD OR eMissionStage = ST_8_REACT_TO_THIRD) AND NOT IS_PED_INJURED(peds[mpf_target].id) AND NOT peds[mpf_target].bFleeing)
				syncScene[SCENE_PARTY_A] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
			ENDIF
			
			IF NOT IS_PED_INJURED(peds[mpf_party_coke_m_1].id) AND NOT peds[mpf_party_coke_m_1].bFleeing
				TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_m_1].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_B_Male1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			IF eMissionStage = ST_7_TORTURE_FOR_THIRD
			OR eMissionStage = ST_8_REACT_TO_THIRD
				IF NOT IS_PED_INJURED(peds[mpf_target].id) AND NOT peds[mpf_target].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_target].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_B_Male2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ENDIF
			IF eMissionStage = ST_5_TORTURE_FOR_SECOND
			OR eMissionStage = ST_6_REACT_TO_SECOND
				IF NOT IS_PED_INJURED(peds[mpf_target].id) AND NOT peds[mpf_target].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_target].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_B_Male3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(peds[mpf_party_coke_m_3].id) AND NOT peds[mpf_party_coke_m_3].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_m_3].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_B_Male3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(peds[mpf_party_coke_m_5].id) AND NOT peds[mpf_party_coke_m_5].bFleeing
				TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_m_5].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_B_Male5", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			IF NOT IS_PED_INJURED(peds[mpf_party_coke_f_1].id) AND NOT peds[mpf_party_coke_f_1].bFleeing
				TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_f_1].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_B_Female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			
			bHasCokeSnortPlayed = TRUE
			iCokeSnortTimer = GET_GAME_TIMER()
		
		// Coke prep scene
		ELSE
			IF (NOT IS_PED_INJURED(peds[mpf_party_coke_m_1].id) AND NOT peds[mpf_party_coke_m_1].bFleeing)
			OR (NOT IS_PED_INJURED(peds[mpf_party_coke_m_3].id) AND NOT peds[mpf_party_coke_m_3].bFleeing)
			OR (NOT IS_PED_INJURED(peds[mpf_party_coke_m_5].id) AND NOT peds[mpf_party_coke_m_5].bFleeing)
			OR (NOT IS_PED_INJURED(peds[mpf_party_coke_f_1].id) AND NOT peds[mpf_party_coke_f_1].bFleeing)
			OR ((eMissionStage = ST_5_TORTURE_FOR_SECOND OR eMissionStage = ST_6_REACT_TO_SECOND OR eMissionStage = ST_7_TORTURE_FOR_THIRD OR eMissionStage = ST_8_REACT_TO_THIRD) AND NOT IS_PED_INJURED(peds[mpf_target].id) AND NOT peds[mpf_target].bFleeing)
				syncScene[SCENE_PARTY_A] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
			ENDIF
			
			IF NOT IS_PED_INJURED(peds[mpf_party_coke_m_1].id) AND NOT peds[mpf_party_coke_m_1].bFleeing
				TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_m_1].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_A_Male1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			IF eMissionStage = ST_5_TORTURE_FOR_SECOND
			OR eMissionStage = ST_6_REACT_TO_SECOND
				IF NOT IS_PED_INJURED(peds[mpf_target].id) AND NOT peds[mpf_target].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_target].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_A_Male3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(peds[mpf_party_coke_m_3].id) AND NOT peds[mpf_party_coke_m_3].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_m_3].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_A_Male3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ENDIF
			IF eMissionStage = ST_7_TORTURE_FOR_THIRD
			OR eMissionStage = ST_8_REACT_TO_THIRD
				IF NOT IS_PED_INJURED(peds[mpf_target].id) AND NOT peds[mpf_target].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_target].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_A_Male2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(peds[mpf_party_coke_m_5].id) AND NOT peds[mpf_party_coke_m_5].bFleeing
				TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_m_5].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_A_Male5", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
			IF NOT IS_PED_INJURED(peds[mpf_party_coke_f_1].id) AND NOT peds[mpf_party_coke_f_1].bFleeing
				TASK_SYNCHRONIZED_SCENE(peds[mpf_party_coke_f_1].id, syncScene[SCENE_PARTY_A], "MISSFBI3_PARTY", "Snort_Coke_A_Female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF
		ENDIF
		
	ENDIF
	
	// GUYS ON TOP BALCONY
	IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PARTY_B])
		
		SWITCH iPartyBStage
			CASE 0
				IF (NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing)
					syncScene[SCENE_PARTY_B] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Talk_Balcony_Loop_Male1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_2].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Talk_Balcony_Loop_Male2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_f_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Talk_Balcony_Loop_Female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyBStage++
			BREAK
			CASE 1	
				IF (NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing)
					syncScene[SCENE_PARTY_B] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Walk_Inside_Male1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_2].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Walk_Inside_Male2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_f_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Walk_Inside_Female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyBStage++
			BREAK
			CASE 2
				IF (NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing)
					syncScene[SCENE_PARTY_B] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Talk_Inside_Loop_Male1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_2].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Talk_Inside_Loop_Male2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_f_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Talk_Inside_Loop_Female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyBStage++
			BREAK
			CASE 3
				IF (NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing)
					syncScene[SCENE_PARTY_B] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Walk_To_Balcony_Male1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_m_2].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Walk_To_Balcony_Male2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_b_f_1].id, syncScene[SCENE_PARTY_B], "MISSFBI3_PARTY_B", "Walk_To_Balcony_Female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyBStage = 0
			BREAK
		ENDSWITCH
		
	ENDIF
	
	// GUYS SMOKING WEED TOP RIGHT
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncScene[SCENE_PARTY_C])
	
		IF (NOT IS_PED_INJURED(peds[mpf_party_c_m_1].id) AND NOT peds[mpf_party_c_m_1].bFleeing)
		OR (NOT IS_PED_INJURED(peds[mpf_party_c_m_2].id) AND NOT peds[mpf_party_c_m_2].bFleeing)
			syncScene[SCENE_PARTY_C] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
			SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_PARTY_C], TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(peds[mpf_party_c_m_1].id) AND NOT peds[mpf_party_c_m_1].bFleeing
			TASK_SYNCHRONIZED_SCENE(peds[mpf_party_c_m_1].id, syncScene[SCENE_PARTY_C], "MISSFBI3_PARTY_C", "Marijuana_Loop_A_Male1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
		ENDIF
		IF NOT IS_PED_INJURED(peds[mpf_party_c_m_2].id) AND NOT peds[mpf_party_c_m_2].bFleeing
			TASK_SYNCHRONIZED_SCENE(peds[mpf_party_c_m_2].id, syncScene[SCENE_PARTY_C], "MISSFBI3_PARTY_C", "Marijuana_Loop_A_Male2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
		ENDIF
	
	ENDIF
	
	// GUYS ON LOWER BALCONY
	IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_PARTY_D])
		
		SWITCH iPartyDStage
			CASE 0
				IF (NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing)
					syncScene[SCENE_PARTY_D] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_A_Male1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_2].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_A_Male2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_3].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_A_Male3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_f_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_A_Female", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyDStage++
			BREAK
			CASE 1
				IF (NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing)
					syncScene[SCENE_PARTY_D] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_A_To_B_Male1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_2].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_A_To_B_Male2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_3].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_A_To_B_Male3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_f_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_A_To_B_Female", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyDStage++
			BREAK
			CASE 2
				IF (NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing)
					syncScene[SCENE_PARTY_D] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_B_Male1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_2].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_B_Male2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_3].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_B_Male3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_f_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Stand_Talk_Loop_B_Female", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyDStage++
			BREAK
			CASE 3
				IF (NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing)
				OR (NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing)
					syncScene[SCENE_PARTY_D] = CREATE_SYNCHRONIZED_SCENE(vPartySyncCoord, vPartySyncRot)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_B_To_A_Male1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_2].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_B_To_A_Male2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_m_3].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_B_To_A_Male3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing
					TASK_SYNCHRONIZED_SCENE(peds[mpf_party_d_f_1].id, syncScene[SCENE_PARTY_D], "MISSFBI3_PARTY_D", "Walk_From_B_To_A_Female", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				iPartyDStage = 0
			BREAK
		ENDSWITCH

	ENDIF
	
	// Manage the music coming from the party based on the zoom (yes I know this isn't realistic)
	fAudioZoom = (1 - (GET_GAMEPLAY_CAM_FOV() - 5) / 45)
	
	IF fAudioZoom != fAudioZoomPreviousFrame
		SET_VARIABLE_ON_STREAM("ZoomLevel", fAudioZoom)
		SET_AUDIO_SCENE_VARIABLE(audsceneZoom, "apply", fAudioZoom)
		fAudioZoomPreviousFrame = fAudioZoom
	ENDIF
	
	IF ePartyState != PCS_NOT_CRASHED
		IF fAudioZoom != fAudioZoomPreviousFrame
			SET_PED_WALLA_DENSITY(fAudioZoom/1.5, 1)
		ENDIF
	ELSE
		SET_PED_WALLA_DENSITY(0,0)
	ENDIF

ENDPROC

//PURPOSE: Checks if the party scene is ready
FUNC BOOL PARTY_IsSceneReady(BOOL bDriveToStage, BOOL bIncParty, MISSION_STAGE eStage, BOOL bDontLoadProne = FALSE, BOOL bDontLoadCoffee = FALSE)
	BOOL bIsReady = TRUE
	BOOL bCreatedEntityThisFrame
	
	IF bDriveToStage
	
		IF NOT DOES_ENTITY_EXIST(vehDriveToVeh)
			
			IF NOT bCreatedEntityThisFrame
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
				
					CPRINTLN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() CHECKPOINT VEHICLE AVAILABLE, CREATING - MODEL HASH:", GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
				
					IF HAS_MODEL_LOADED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						vehDriveToVeh = CREATE_REPLAY_CHECKPOINT_VEHICLE(vCoffeeShopCarCoord, fCoffeeShopCarHeading)
						CPRINTLN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() CREATED CHECKPOINT VEHICLE")
						bCreatedEntityThisFrame = TRUE
					ELSE
						CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() locally stored vehicle model NOT LOADED")
						bIsReady = FALSE
					ENDIF
			
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() CHECKPOINT VEHICLE UNAVAILABLE, CREATING DAVES DEFAULT")
					// Creates default vehicle, Daves blue BMW
					IF CREATE_NPC_VEHICLE(vehDriveToVeh, CHAR_DAVE, vCoffeeShopCarCoord, fCoffeeShopCarHeading, TRUE)
						CPRINTLN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() CREATED DAVES DEFAULT")
						bCreatedEntityThisFrame = TRUE
					ELSE
						bIsReady = FALSE
					ENDIF
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() VEH Wait for next frame, already created an entity this frame")
				bIsReady = FALSE
			ENDIF
		ENDIF
		
		// create dave
		IF NOT DOES_ENTITY_EXIST(pedDave)
			IF NOT bCreatedEntityThisFrame
				IF CREATE_NPC_PED_ON_FOOT(pedDave, CHAR_DAVE, vCoffeeShopDaveCoord, fCoffeeShopDaveHeading, FALSE)
					SET_ENTITY_LOD_DIST(pedDave, 100)
					SET_PED_LOD_MULTIPLIER(pedDave, 1.5)
					SET_PED_CREW(pedDave)
					PED_VARIATION_STRUCT sDave 
					sDave = GET_DAVE_VARIATIONS(TRUE)
					SET_PED_VARIATIONS(pedDave, sDave)
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DAVE))
					bIsReady = FALSE
					bCreatedEntityThisFrame = TRUE
				ELSE
					bIsReady = FALSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Dave NOT LOADED")
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() PEDDAVE Wait for next frame, already created an entity this frame")
				bIsReady = FALSE
			ENDIF
		ELIF NOT IS_PED_INJURED(pedDave)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDave)
				bIsReady = FALSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Dave variations NOT LOADED")
			ENDIF
		ENDIF
		
		IF bDontLoadCoffee
			IF DOES_ENTITY_EXIST(pedDave)
				IF NOT IS_PED_IN_VEHICLE(pedDave, vehDriveToVeh, TRUE)
					SET_PED_INTO_VEHICLE(pedDave, vehDriveToVeh, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehDriveToVeh)
			AND NOT IS_PED_INJURED(MIKE_PED_ID())
			AND NOT IS_PED_INJURED(pedDave)
			
				IF NOT DOES_ENTITY_EXIST(objs[mof_coffee_mike])
				OR NOT DOES_ENTITY_EXIST(objs[mof_coffee_dave])
				OR NOT DOES_ENTITY_EXIST(objs[mof_coffee_chair])
				OR NOT DOES_ENTITY_EXIST(propPhoneDave)
				
					bIsReady = FALSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Coffee not created")
				
					IF NOT bCreatedEntityThisFrame
						IF HAS_MODEL_LOADED(P_ING_COFFEECUP_01)
						AND HAS_MODEL_LOADED(PROP_NPC_PHONE)
						AND HAS_MODEL_LOADED(PROP_CHAIR_01A)
							objs[mof_coffee_mike] 	= CREATE_OBJECT(P_ING_COFFEECUP_01, GET_ENTITY_COORDS(MIKE_PED_ID()))
							objs[mof_coffee_dave] 	= CREATE_OBJECT(P_ING_COFFEECUP_01, GET_ENTITY_COORDS(pedDave))
							
							CREATE_MODEL_HIDE(<<-1267.62, -880.46, 10.95>>, 0.25, PROP_CHAIR_01A, TRUE)
							objs[mof_coffee_chair]	= CREATE_OBJECT(PROP_CHAIR_01A, 	<<-1267.62, -880.46, 10.95>>)
							chairBlocker			= ADD_SCENARIO_BLOCKING_AREA(<<-1267.62, -880.46, 10.95>> - << 10.0, 10.0, 10.0 >>, <<-1267.62, -880.46, 10.95>> + << 10.0, 10.0, 10.0 >>)
							propPhoneDave			= CREATE_OBJECT(PROP_NPC_PHONE, 	GET_ENTITY_COORDS(pedDave))
							
							ATTACH_ENTITY_TO_ENTITY(objs[mof_coffee_mike], 	MIKE_PED_ID(), 	GET_PED_BONE_INDEX(MIKE_PED_ID(), BONETAG_PH_L_HAND), 	<<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							ATTACH_ENTITY_TO_ENTITY(objs[mof_coffee_dave], 	pedDave, 		GET_PED_BONE_INDEX(pedDave, BONETAG_PH_R_HAND), 		<<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							ATTACH_ENTITY_TO_ENTITY(propPhoneDave, 				pedDave, 		GET_PED_BONE_INDEX(pedDave, BONETAG_PH_L_HAND), 		<<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							
							bCreatedEntityThisFrame = TRUE
						ELSE
							bIsReady = FALSE
							CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Coffee cup, phone and chair not loaded")
						ENDIF
					ELSE
						CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() CoffeeCups/Chair/Phone Wait for next frame, already created an entity this frame")
						bIsReady = FALSE
					ENDIF
				
				ELSE
				
					IF NOT IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_COFFEE])
				
						IF HAS_ANIM_DICT_LOADED(adCoffee)		
							
							syncScene[SCENE_COFFEE] = CREATE_SYNCHRONIZED_SCENE(vCoffeeIGCoord, vCoffeeIGRot)
							TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), 	syncScene[SCENE_COFFEE], 	adCoffee, 	"002291_02_fbi_3_coffee_w_dave_idle_mic", 	INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							TASK_SYNCHRONIZED_SCENE(pedDave, 		syncScene[SCENE_COFFEE], 	adCoffee, 	"002291_02_fbi_3_coffee_w_dave_idle_dave", 	INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_coffee_chair], syncScene[SCENE_COFFEE], "002291_02_fbi_3_coffee_w_dave_idle_chair", adCoffee, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT))
							SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_COFFEE], TRUE)
							
							VECTOR v1, v2
							v1 = GET_ANIM_INITIAL_OFFSET_POSITION(adCoffee, "002291_02_fbi_3_coffee_w_dave_exit_cam", vCoffeeIGCoord, vCoffeeIGRot, 0)
							v2 = GET_ANIM_INITIAL_OFFSET_ROTATION(adCoffee, "002291_02_fbi_3_coffee_w_dave_exit_cam", vCoffeeIGCoord, vCoffeeIGRot, 0)
							PRINTLN( v1 )
							PRINTLN( v2 )
						
						ELSE
							bIsReady = FALSE
							CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Coffee scene anim dict not loaded")
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
	ELIF NOT bDontLoadProne
		
		// create dave
		IF NOT DOES_ENTITY_EXIST(pedDave)
			IF NOT bCreatedEntityThisFrame
				IF CREATE_NPC_PED_ON_FOOT(pedDave, CHAR_DAVE, vDaveSnipeCoords, fDaveSnipeHeading, FALSE)
					SET_ENTITY_LOD_DIST(pedDave, 100)
					SET_PED_LOD_MULTIPLIER(pedDave, 1.5)
					SET_PED_CREW(pedDave)
					PED_VARIATION_STRUCT sDave 
					sDave = GET_DAVE_VARIATIONS(TRUE)
					SET_PED_VARIATIONS(pedDave, sDave)
					bIsReady = FALSE
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DAVE))
					bCreatedEntityThisFrame = TRUE
				ELSE
					bIsReady = FALSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Dave NOT LOADED")
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() PEDDAVE Wait for next frame, already created an entity this frame")
				bIsReady = FALSE
			ENDIF
		ELIF NOT IS_PED_INJURED(pedDave)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDave)
				bIsReady = FALSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Dave variations NOT LOADED")
			ENDIF
		ENDIF
		
		// create fake michael
		IF NOT DOES_ENTITY_EXIST(pedProneMichael)
			IF NOT bCreatedEntityThisFrame
				IF CREATE_PLAYER_PED_ON_FOOT(pedProneMichael, CHAR_MICHAEL, vMichaelSnipeCoords, fMichaelSnipeHeading, TRUE)
					INT iQueue
					REPEAT COUNT_OF(g_ambientSelectorPedDeleteQueue) iQueue
						IF g_ambientSelectorPedDeleteQueue[iQueue] = MIKE_PED_ID()
						OR g_ambientSelectorPedDeleteQueue[iQueue] = pedProneMichael
							g_ambientSelectorPedDeleteQueue[iQueue] = NULL
						ENDIF
					ENDREPEAT
					SET_PED_CREW(pedProneMichael)
					bCreatedEntityThisFrame = TRUE
					bIsReady = FALSE
				ELSE
					bIsReady = FALSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() fakeProneMichael NOT LOADED")
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() PEDPRONEMIKE Wait for next frame, already created an entity this frame")
				bIsReady = FALSE
			ENDIF
		ELIF NOT IS_PED_INJURED(pedProneMichael)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedProneMichael)
				bIsReady = FALSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() pedProneMichael variations NOT LOADED")
			ENDIF
		ENDIF

		IF NOT bProneSetup	
			IF DOES_ENTITY_EXIST(pedDave)
			AND DOES_ENTITY_EXIST(pedProneMichael)
			AND NOT IS_PED_INJURED(pedProneMichael)
			AND NOT IS_PED_INJURED(pedDave)
				IF HAS_ANIM_DICT_LOADED(adSniping)
				AND HAS_WEAPON_ASSET_LOADED(weapMichaelsSniper)
				AND HAS_MODEL_LOADED(PROP_NPC_PHONE)
				AND HAS_MODEL_LOADED(PROP_BINOC_01)

					SET_ENTITY_COORDS(MIKE_PED_ID(), << -3024.7515, 332.2954, 19.3610 >>)
					SET_ENTITY_ROTATION(MIKE_PED_ID(), <<0,0,76.0353>>)
					SET_ENTITY_VISIBLE(MIKE_PED_ID(), FALSE)
					SET_ENTITY_COLLISION(MIKE_PED_ID(), FALSE)
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
				
					syncScene[SCENE_SNIPE_PRONE] = CREATE_SYNCHRONIZED_SCENE(vProneCoords, vProneRotation)
					
					TASK_SYNCHRONIZED_SCENE(pedDave, syncScene[SCENE_SNIPE_PRONE], adSniping, animProneDave, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, AIK_DISABLE_HEAD_IK )
						
					// phone
					IF DOES_ENTITY_EXIST(propPhoneDave)
						IF IS_ENTITY_ATTACHED(propPhoneDave)
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(propPhoneDave, pedDave)
								DETACH_ENTITY(propPhoneDave, FALSE, TRUE)
							ENDIF
						ENDIF
					ELSE
						propPhoneDave = CREATE_OBJECT(PROP_NPC_PHONE, GET_PED_BONE_COORDS(pedDave, BONETAG_PH_R_HAND, <<0,0,0>>))
						Unload_Asset_Model(sAssetData, PROP_NPC_PHONE)
					ENDIF
					IF NOT IS_ENTITY_ATTACHED(propPhoneDave)
						ATTACH_ENTITY_TO_ENTITY(propPhoneDave, pedDave, GET_PED_BONE_INDEX(pedDave, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, FALSE, FALSE)
					ENDIF
					
					// binoculars
					IF DOES_ENTITY_EXIST(objs[mof_binoculars])
						IF IS_ENTITY_ATTACHED(objs[mof_binoculars])
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objs[mof_binoculars], pedDave)
								DETACH_ENTITY(objs[mof_binoculars], FALSE, TRUE)
							ENDIF
						ENDIF
					ELSE
						objs[mof_binoculars] = CREATE_OBJECT(PROP_BINOC_01, GET_PED_BONE_COORDS(pedDave, BONETAG_PH_L_HAND, <<0,0,0>>))
					ENDIF
					IF NOT IS_ENTITY_ATTACHED(objs[mof_binoculars])
						ATTACH_ENTITY_TO_ENTITY(objs[mof_binoculars], pedDave, GET_PED_BONE_INDEX(pedDave, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, FALSE, FALSE)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(pedProneMichael, syncScene[SCENE_SNIPE_PRONE], adSniping, animProneMichael, 
										INSTANT_BLEND_IN, INSTANT_BLEND_OUT)

					SET_SYNCHRONIZED_SCENE_LOOPED(syncScene[SCENE_SNIPE_PRONE], TRUE)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedProneMichael)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDave)

					PARTY_GivePedCustomSniperRifle(MIKE_PED_ID())
					PARTY_GivePedCustomSniperRifle(pedProneMichael)	
					
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
					
					bScopeAiming			= FALSE
					bSnipeCamDirInit 		= FALSE
					bProneSetup				= TRUE
					bIsReady 				= FALSE

				ELSE
					bIsReady = FALSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() Animation and/or phone not loaded NOT LOADED")
				ENDIF
			ELSE
				bIsReady = FALSE
				CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady Waiting for Peds to be created first")
			ENDIF
		ENDIF
	ENDIF

	
	IF bIncParty
	
		IF HAS_MODEL_LOADED(mnPartyMale1)
		//AND HAS_MODEL_LOADED(mnPartyMale2)
		AND HAS_MODEL_LOADED(mnPartyFem)
		AND HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY")
		AND HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_B")
		AND HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_C")
		AND HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_D")
		AND HAS_ANIM_DICT_LOADED(adPartyGetUpFlee)
		AND REQUEST_MISSION_AUDIO_BANK("FBI_03_Panic")
		
		// coke peds
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_coke_m_1].id)
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_coke_m_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_coke_m_1].id, "COKE_M_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_1].id, PED_COMP_HEAD, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_1].id, PED_COMP_HAIR, 3, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_1].id, PED_COMP_TORSO, 2, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_1].id, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_1].id, PED_COMP_SPECIAL, 1, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_coke_m_1].id,	ANCHOR_EYES, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_coke_m_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_coke_m_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_coke_m_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() COKE_M_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			IF eStage != ST_6_REACT_TO_SECOND
			AND eStage != ST_5_TORTURE_FOR_SECOND
				IF NOT DOES_ENTITY_EXIST(peds[mpf_party_coke_m_3].id)
					IF NOT bCreatedEntityThisFrame
						peds[mpf_party_coke_m_3].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0.5,0>>, 0)
						SET_PED_NAME_DEBUG(peds[mpf_party_coke_m_3].id, "COKE_M_3")
						SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_3].id, PED_COMP_HEAD, 1, 2)
						SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_3].id, PED_COMP_HAIR, 1, 0)
						SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_3].id, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_3].id, PED_COMP_LEG, 1, 0)
						SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_3].id, PED_COMP_SPECIAL, 2, 0)
						SET_PED_PROP_INDEX(peds[mpf_party_coke_m_3].id,	ANCHOR_HEAD, 1, 0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_coke_m_3].id, TRUE)
						SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_coke_m_3].id, FALSE)
						SET_PED_LOD_MULTIPLIER(peds[mpf_party_coke_m_3].id, 2.5)
						bCreatedEntityThisFrame = TRUE
					ELSE
						CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() COKE_M_3 Wait for next frame, already created an entity this frame")
						bIsReady = FALSE
					ENDIF
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_coke_m_5].id)
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_coke_m_5].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.0,-0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_coke_m_5].id, "COKE_M_5")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_5].id, PED_COMP_HEAD, 0, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_5].id, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_5].id, PED_COMP_TORSO, 1, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_5].id, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_m_5].id, PED_COMP_SPECIAL, 0, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_coke_m_5].id,	ANCHOR_HEAD, 0, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_coke_m_5].id,	ANCHOR_EYES, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_coke_m_5].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_coke_m_5].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_coke_m_5].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() COKE_M_5 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_coke_f_1].id)
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_coke_f_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyFem, 	vPartySyncCoord + <<-0.5,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_coke_f_1].id, "COKE_F_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_f_1].id, PED_COMP_HEAD, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_f_1].id, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_f_1].id, PED_COMP_TORSO, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_coke_f_1].id, PED_COMP_LEG, 1, 1)
					SET_PED_PROP_INDEX(peds[mpf_party_coke_f_1].id,	ANCHOR_EYES, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_coke_f_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_coke_f_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_coke_f_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() COKE_F_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			
		// party b
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_b_m_1].id) AND NOT peds[mpf_party_b_m_1].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_b_m_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_b_m_1].id, "B_M_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_1].id, PED_COMP_HEAD, 1, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_1].id, PED_COMP_HAIR, 3, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_1].id, PED_COMP_TORSO, 2, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_1].id, PED_COMP_LEG, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_1].id, PED_COMP_SPECIAL, 0, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_b_m_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_b_m_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_b_m_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() B_M_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_b_m_2].id) AND NOT peds[mpf_party_b_m_2].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_b_m_2].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.0,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_b_m_2].id, "B_M_2")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_2].id, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_2].id, PED_COMP_HAIR, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_2].id, PED_COMP_TORSO, 1, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_2].id, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_m_2].id, PED_COMP_SPECIAL, 2, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_b_m_2].id,	ANCHOR_EYES, 1, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_b_m_2].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_b_m_2].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_b_m_2].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() B_M_2 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF

			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_b_f_1].id) AND NOT peds[mpf_party_b_f_1].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_b_f_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyFem,  vPartySyncCoord + <<-0.5,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_b_f_1].id, "B_F_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_f_1].id, PED_COMP_HEAD, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_f_1].id, PED_COMP_HAIR, 1, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_f_1].id, PED_COMP_TORSO, 0, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_b_f_1].id, PED_COMP_LEG, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_b_f_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_b_f_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_b_f_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() B_F_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF

		//party c
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_c_m_1].id) AND NOT peds[mpf_party_c_m_1].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_c_m_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_c_m_1].id, "C_M_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_1].id, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_1].id, PED_COMP_HAIR, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_1].id, PED_COMP_TORSO, 1, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_1].id, PED_COMP_LEG, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_1].id, PED_COMP_SPECIAL, 0, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_c_m_1].id,	ANCHOR_HEAD, 0, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_c_m_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_c_m_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_c_m_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() C_M_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_c_m_2].id) AND NOT peds[mpf_party_c_m_2].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_c_m_2].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.0,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_c_m_2].id, "C_M_2")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_2].id, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_2].id, PED_COMP_HAIR, 3, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_2].id, PED_COMP_TORSO, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_2].id, PED_COMP_LEG, 1, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_c_m_2].id, PED_COMP_SPECIAL, 1, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_c_m_2].id,	ANCHOR_EYES, 1, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_c_m_2].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_c_m_2].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_c_m_2].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() C_M_2 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			
		// party d
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_d_m_1].id) AND NOT peds[mpf_party_d_m_1].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_d_m_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_d_m_1].id, "D_M_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_1].id, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_1].id, PED_COMP_HAIR, 3, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_1].id, PED_COMP_TORSO, 2, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_1].id, PED_COMP_LEG, 1, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_1].id, PED_COMP_SPECIAL, 0, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_d_m_1].id,	ANCHOR_HEAD, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_d_m_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_d_m_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_d_m_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() D_M_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_d_m_2].id) AND NOT peds[mpf_party_d_m_2].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_d_m_2].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.0,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_d_m_2].id, "D_M_2")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_2].id, PED_COMP_HEAD, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_2].id, PED_COMP_HAIR, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_2].id, PED_COMP_TORSO, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_2].id, PED_COMP_LEG, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_2].id, PED_COMP_SPECIAL, 1, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_d_m_2].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_d_m_2].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_d_m_2].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() D_M_2 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_d_m_3].id) AND NOT peds[mpf_party_d_m_3].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_d_m_3].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_d_m_3].id, "D_M_3")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_3].id, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_3].id, PED_COMP_HAIR, 3, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_3].id, PED_COMP_TORSO, 0, 2)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_3].id, PED_COMP_LEG, 0, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_m_3].id, PED_COMP_SPECIAL, 2, 0)
					SET_PED_PROP_INDEX(peds[mpf_party_d_m_3].id, ANCHOR_EYES, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_d_m_3].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_d_m_3].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_d_m_3].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() D_M_3 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(peds[mpf_party_d_f_1].id) AND NOT peds[mpf_party_d_f_1].bFleeing
				IF NOT bCreatedEntityThisFrame
					peds[mpf_party_d_f_1].id = CREATE_PED(PEDTYPE_MISSION, mnPartyFem,  vPartySyncCoord + <<-0.5,0.5,0>>, 0)
					SET_PED_NAME_DEBUG(peds[mpf_party_d_f_1].id, "D_F_1")
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_f_1].id, PED_COMP_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_f_1].id, PED_COMP_HAIR, 1, 1)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_f_1].id, PED_COMP_TORSO, 0, 0)
					SET_PED_COMPONENT_VARIATION(peds[mpf_party_d_f_1].id, PED_COMP_LEG, 0, 2)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_party_d_f_1].id, TRUE)
					SET_PED_STEERS_AROUND_PEDS(peds[mpf_party_d_f_1].id, FALSE)
					SET_PED_LOD_MULTIPLIER(peds[mpf_party_d_f_1].id, 2.5)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() D_F_1 Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ENDIF

			// Setup for the target
			IF NOT DOES_ENTITY_EXIST(peds[mpf_target].id)
				SWITCH eStage
					CASE ST_6_REACT_TO_SECOND
					FALLTHRU
					CASE ST_8_REACT_TO_THIRD
					// Doing coke OR standing watching coke prep
						IF HAS_MODEL_LOADED(mnTarget)
							IF NOT bCreatedEntityThisFrame
								peds[mpf_target].id = CREATE_PED(PEDTYPE_MISSION, mnTarget, vPartySyncCoord + <<0.5,0,0>>, 0)
								SET_PED_STEERS_AROUND_PEDS(peds[mpf_target].id, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_target].id, TRUE)
								SET_PED_NAME_DEBUG(peds[mpf_target].id, "THE TARGET")
								SET_PED_LOD_MULTIPLIER(peds[mpf_target].id, 2.5)
								bCreatedEntityThisFrame = TRUE
							ELSE
								CPRINTLN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() THE TARGET Wait for next frame, already created an entity this frame")
								bIsReady = FALSE
							ENDIF
						ELSE
							bIsReady = FALSE // waiting on target model and cigarette packet
						ENDIF
					BREAK
					CASE ST_10_REACT_FINAL
					// Out on the balcony smoking
						IF HAS_MODEL_LOADED(mnTarget)
						AND HAS_MODEL_LOADED(PROP_CS_CIGGY_01B)
						AND HAS_MODEL_LOADED(P_FAG_PACKET_01_S)
						AND HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_E")
							IF NOT bCreatedEntityThisFrame
								peds[mpf_target].id = CREATE_PED(PEDTYPE_MISSION, mnTarget, vPartySyncCoord + <<0.5,0,0>>, 0)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_target].id, TRUE)
								SET_PED_STEERS_AROUND_PEDS(peds[mpf_target].id, FALSE)
								SET_PED_NAME_DEBUG(peds[mpf_target].id, "THE TARGET")
								objs[mof_target_ciggy]	= CREATE_OBJECT(PROP_CS_CIGGY_01B, GET_ENTITY_COORDS(peds[mpf_target].id))
								// bug 1715362
								ATTACH_ENTITY_TO_ENTITY(objs[mof_target_ciggy], peds[mpf_target].id, GET_PED_BONE_INDEX(peds[mpf_target].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, DEFAULT, DEFAULT, FALSE)
								//SET_ENTITY_COLLISION(objs[mof_target_ciggy], FALSE)
								ptfx_Cigs[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objs[mof_target_ciggy], <<0,0,0>>, <<0,0,0>>, 1.0)
								objs[mof_target_packet]	= CREATE_OBJECT(P_FAG_PACKET_01_S, GET_ENTITY_COORDS(peds[mpf_target].id))
								ATTACH_ENTITY_TO_ENTITY(objs[mof_target_packet], peds[mpf_target].id, GET_PED_BONE_INDEX(peds[mpf_target].id, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, DEFAULT, DEFAULT, FALSE)
								SET_ENTITY_COLLISION(objs[mof_target_packet], FALSE)
								SET_PED_LOD_MULTIPLIER(peds[mpf_target].id, 2.5)
								bCreatedEntityThisFrame = TRUE
							ELSE
								CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() THE TARGET Wait for next frame, already created an entity this frame")
								bIsReady = FALSE
							ENDIF
						ELSE
							bIsReady = FALSE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF HAS_MODEL_LOADED(PROP_CS_CIGGY_01B)
			AND HAS_ANIM_DICT_LOADED("MISSFBI3_PARTY_E")
			// decoy
				IF NOT DOES_ENTITY_EXIST(peds[mpf_decoy_m].id)
					//IF HAS_MODEL_LOADED(mnPartyMale2)
					IF HAS_MODEL_LOADED(mnPartyMale1)
						IF NOT bCreatedEntityThisFrame
							peds[mpf_decoy_m].id = CREATE_PED(PEDTYPE_MISSION, mnPartyMale1, vPartySyncCoord + <<0.5,0.5,0>>, 0)
							SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_m].id, PED_COMP_HEAD, 	0,0)
							SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_m].id, PED_COMP_HAIR, 	0,0)
							SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_m].id, PED_COMP_TORSO, 	1,0)
							SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_m].id, PED_COMP_LEG, 	0,0)
							SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_m].id, PED_COMP_SPECIAL, 0,0)
							SET_PED_PROP_INDEX(peds[mpf_decoy_m].id, ANCHOR_HEAD, 1, 1)
							SET_PED_NAME_DEBUG(peds[mpf_decoy_m].id, "DECOY M")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_decoy_m].id, TRUE)
							SET_PED_STEERS_AROUND_PEDS(peds[mpf_decoy_m].id, FALSE)
							objs[mof_decoy_m_ciggy]	= CREATE_OBJECT(PROP_CS_CIGGY_01B, GET_ENTITY_COORDS(peds[mpf_decoy_m].id))
							// bug 1715362
							ATTACH_ENTITY_TO_ENTITY(objs[mof_decoy_m_ciggy], peds[mpf_decoy_m].id, GET_PED_BONE_INDEX(peds[mpf_decoy_m].id, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, DEFAULT, DEFAULT, FALSE)
							//SET_ENTITY_COLLISION(objs[mof_decoy_m_ciggy], FALSE)
							ptfx_Cigs[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objs[mof_decoy_m_ciggy], <<0,0,0>>, <<0,0,0>>, 1.0)
							SET_PED_LOD_MULTIPLIER(peds[mpf_decoy_m].id, 2.5)
							bCreatedEntityThisFrame = TRUE
						ELSE
							CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() DECOY M Wait for next frame, already created an entity this frame")
							bIsReady = FALSE
						ENDIF
					ELSE
						bIsReady = FALSE // waiting on decoy model
					ENDIF
				ENDIF
				
			// female smoker
				IF NOT DOES_ENTITY_EXIST(peds[mpf_decoy_f].id)
					IF NOT bCreatedEntityThisFrame
						
						peds[mpf_decoy_f].id = CREATE_PED(PEDTYPE_MISSION, mnPartyFem, vPartySyncCoord + <<0.0,0.5,0>>, 0)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_decoy_f].id, TRUE)
						SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_f].id, PED_COMP_HEAD, 0, 1)
						SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_f].id, PED_COMP_HAIR, 0, 1)
						SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_f].id, PED_COMP_TORSO, 1, 2)
						SET_PED_COMPONENT_VARIATION(peds[mpf_decoy_f].id, PED_COMP_LEG, 1, 1)
						SET_PED_PROP_INDEX(peds[mpf_decoy_f].id, ANCHOR_EYES, 1, 0)
						SET_PED_NAME_DEBUG(peds[mpf_decoy_f].id, "DECOY F")
						SET_PED_STEERS_AROUND_PEDS(peds[mpf_decoy_f].id, FALSE)
						objs[mof_decoy_f_ciggy]	= CREATE_OBJECT(PROP_CS_CIGGY_01B, GET_ENTITY_COORDS(peds[mpf_decoy_f].id))
						// bug 1715362
						ATTACH_ENTITY_TO_ENTITY(objs[mof_decoy_f_ciggy], peds[mpf_decoy_f].id, GET_PED_BONE_INDEX(peds[mpf_decoy_f].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, DEFAULT, DEFAULT, FALSE)
						//SET_ENTITY_COLLISION(objs[mof_decoy_f_ciggy], FALSE)
						ptfx_Cigs[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_cig_smoke", objs[mof_decoy_f_ciggy], <<0,0,0>>, <<0,0,0>>, 1.0)
						SET_PED_LOD_MULTIPLIER(peds[mpf_decoy_f].id, 2.5)
						bCreatedEntityThisFrame = TRUE
					ELSE
						CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() DECOY F Wait for next frame, already created an entity this frame")
						bIsReady = FALSE
					ENDIF
				ENDIF
			ELSE
				bIsReady = FALSE	// not ready waiting on cigarette and anim dictionary
			ENDIF
			
		ELSE
			bIsReady = FALSE // waiting on party ped male, female, and the party anims
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objs[mof_coffee_table])
			IF HAS_MODEL_LOADED(PROP_FBI3_COFFEE_TABLE)
				IF NOT bCreatedEntityThisFrame
					objs[mof_coffee_table] = CREATE_OBJECT_NO_OFFSET(PROP_FBI3_COFFEE_TABLE, <<-3099.3064, 342.8378, 13.5146>>)
					SET_ENTITY_ROTATION(objs[mof_coffee_table], <<-0.0000, 0.0000, 75.3550>>)
					bCreatedEntityThisFrame = TRUE
				ELSE
					CDEBUG2LN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady() mof_coffee_table Wait for next frame, already created an entity this frame")
					bIsReady = FALSE
				ENDIF
			ELSE
				bIsReady = FALSE // waiting on table model to load
			ENDIF	
		ENDIF
		
		IF bIsReady
			SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
			PARTY_ManagePartyScene()
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_MIKE, "PARTY_IsSnipingSceneReady(
		BOOL bDriveToStage: ", bDriveToStage, ", 
		BOOL bIncParty: ", bIncParty, ",  
		MISSION_STAGE eStage: ", eStage, ", 
		BOOL bDontLoadProne: ", bDontLoadProne, ", 
		BOOL bDontLoadCoffee: ", bDontLoadCoffee,") 
		return: ", bIsReady)

	RETURN bIsReady
ENDFUNC

//PURPOSE: Manages michael's zooming in and out also restricting the view through the scope
PROC PARTY_ManageProneAndScope()
	BOOL bCorrectGameplayCam = FALSE

	DISPLAY_RADAR(FALSE)
	DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175240
	
	WEAPON_TYPE weapMike
	IF GET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weapMike)
		IF weapMike != weapMichaelsSniper
			SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weapMichaelsSniper, TRUE)
		ELSE
			IF GET_AMMO_IN_PED_WEAPON(MIKE_PED_ID(), weapMichaelsSniper) <= 10
				SET_PED_AMMO(MIKE_PED_ID(), weapMichaelsSniper, 20)
			ENDIF
		ENDIF
	ENDIF
	
	weapMike = WEAPONTYPE_UNARMED
	IF DOES_ENTITY_EXIST(pedProneMichael)
	AND NOT IS_PED_INJURED(pedProneMichael)
		IF GET_CURRENT_PED_WEAPON(pedProneMichael, weapMike)
			
			IF weapMike != weapMichaelsSniper
				SET_CURRENT_PED_WEAPON(pedProneMichael, weapMichaelsSniper, TRUE)
			ENDIF
			
			weapMike = WEAPONTYPE_UNARMED
			IF GET_CURRENT_PED_WEAPON(pedProneMichael, weapMike)
				IF weapMike = weapMichaelsSniper
					ENTITY_INDEX tempEntity = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(pedProneMichael)
				
					IF DOES_ENTITY_EXIST(tempEntity)
						REQUEST_WEAPON_HIGH_DETAIL_MODEL(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity))
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	

	// AIMING
	IF bScopeAiming
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM)
		OR IS_SELECTOR_ONSCREEN()
		OR bMissionFailed
			CDEBUG2LN(DEBUG_MIKE,"AIMING STATE: Trigger NOT Pressed")
		
			SET_ENTITY_VISIBLE(MIKE_PED_ID(), FALSE)
			SET_ENTITY_COLLISION(MIKE_PED_ID(), FALSE)
			//HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(MIKE_PED_ID(), TRUE) 
			SET_PED_CURRENT_WEAPON_VISIBLE(MIKE_PED_ID(), FALSE, FALSE)
			
			setupSwayCam(sSwaySniping, vProneCamStartCoord, vProneCamStartRot, fProneCamStartFOV,
							vProneCamEndCoord, vProneCamEndRot, fProneCamEndFOV,
							40000, TRUE, shakeHand, 0.5, "SNIPE")
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
			SET_MICROPHONE_POSITION(FALSE, <<0,0,0>>, <<0,0,0>>, <<0,0,0>>)
			USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
			bScopeAiming = FALSE
		ELSE
			CDEBUG2LN(DEBUG_MIKE,"AIMING STATE: Trigger Pressed")
			
			IF eMissionStage = ST_10_REACT_FINAL
				IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_SNIPER_SCOPE_TARGET")
					START_AUDIO_SCENE("FBI_3_MICHAEL_SNIPER_SCOPE_TARGET")
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_SNIPER_SCOPE")
					START_AUDIO_SCENE("FBI_3_MICHAEL_SNIPER_SCOPE")
				ENDIF
			ENDIF
			
//			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_1")
//				STOP_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_1")
//			ENDIF
//			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_2")
//				STOP_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_2")
//			ENDIF
//			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_3")
//				STOP_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_3")
//			ENDIF
			
			VECTOR v_FirstPersonCamRot 
			v_FirstPersonCamRot = GET_FINAL_RENDERED_CAM_ROT()
			
			FLOAT fXDiff
			fXDiff = ABSF(v_FirstPersonCamRot.x - v_FirstPersonCamRotPrev.x)/TIMESTEP()
			FLOAT fZDiff 
			fZDiff = ABSF(v_FirstPersonCamRot.z - v_FirstPersonCamRotPrev.z)/TIMESTEP()
			
			#IF IS_DEBUG_BUILD
				debug_v_scope_cam_rot 	= v_FirstPersonCamRot
				debug_f_scope_speed_x 	= fXDiff
				debug_f_scope_speed_z 	= fZDiff
				debug_f_scope_zoom 		= GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()
			#ENDIF
			
			
			IF DOES_ENTITY_EXIST(peds[mpf_target].id)
			AND NOT IS_PED_INJURED(peds[mpf_target].id)
			AND (IS_POINT_IN_SCREEN_AREA(GET_ENTITY_COORDS(peds[mpf_target].id), 0.4, 0.4, 0.6, 0.6)
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), peds[mpf_target].id))
			AND GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR() >= F_ZOOM_SCOPE_CHECK
			AND fXDiff < 1.5
			AND fZDiff < 1.5
				IF iAimTimer = -1
					iAimTimer = GET_GAME_TIMER() + 1000
				ENDIF
			ELSE
				iAimTimer = -1
			ENDIF
			
			IF iAimTimer != -1
			AND GET_GAME_TIMER() > iAimTimer
				bAimingAtTheTarget = TRUE
			ELSE
				bAimingAtTheTarget = FALSE
			ENDIF
			
			v_FirstPersonCamRotPrev = v_FirstPersonCamRot

			bCorrectGameplayCam = TRUE
		ENDIF
	
	// NOT AIMING
	ELSE
	
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM)
		AND NOT IS_SELECTOR_ONSCREEN()
		AND NOT bMissionFailed
		
			SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
			
			IF iAttemptToAimTimer = -1
				iAttemptToAimTimer = GET_GAME_TIMER() + 32000
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_MICROPHONE_POSITION(TRUE, GET_ENTITY_COORDS(pedProneMichael), GET_ENTITY_COORDS(pedProneMichael), GET_ENTITY_COORDS(pedProneMichael))
			bCorrectGameplayCam = TRUE
			bScopeAiming = TRUE
		ELSE						
			// Hide the player if not already done so at this stage
			SET_ENTITY_VISIBLE(MIKE_PED_ID(), FALSE)
			SET_ENTITY_COLLISION(MIKE_PED_ID(), FALSE)
			SET_PED_CURRENT_WEAPON_VISIBLE(MIKE_PED_ID(), FALSE, FALSE)
			
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_ATTACK2)
			
			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_SNIPER_SCOPE")
				STOP_AUDIO_SCENE("FBI_3_MICHAEL_SNIPER_SCOPE")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_SNIPER_SCOPE_TARGET")
				STOP_AUDIO_SCENE("FBI_3_MICHAEL_SNIPER_SCOPE_TARGET")
			ENDIF
			
			USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
			
			doSwayCam(sSwaySniping)
		ENDIF
		
		bAimingAtTheTarget = FALSE
	ENDIF
	
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_PHONE)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_BEHIND)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_JUMP)
	
	IF NOT IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	ENDIF
	
	SET_PED_NO_TIME_DELAY_BEFORE_SHOT(MIKE_PED_ID())

	// Corrects the aim
	IF bCorrectGameplayCam
		FLOAT f_gcam_heading 	= GET_GAMEPLAY_CAM_RELATIVE_HEADING()
		FLOAT f_gcam_pitch 		= GET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		CONST_FLOAT F_SNIPE_CAM_HEADING_DEFAULT	1.072
		CONST_FLOAT F_SNIPE_CAM_PITCH_DEFAULT	-7.008
		
		CONST_FLOAT F_SNIPE_CAM_HEADING_MIN 	-13.920
		CONST_FLOAT F_SNIPE_CAM_HEADING_MAX		15.755
		CONST_FLOAT F_SNIPE_CAM_PITCH_MIN 		-12.941
		CONST_FLOAT F_SNIPE_CAM_PITCH_MAX		-1.999
		
		IF bSnipeCamDirInit = FALSE
			IF (f_gcam_heading <= F_SNIPE_CAM_HEADING_DEFAULT - 1.0 	OR f_gcam_heading >= F_SNIPE_CAM_HEADING_DEFAULT + 1.0)
			OR (f_gcam_pitch <= F_SNIPE_CAM_PITCH_DEFAULT - 1.0 		OR f_gcam_pitch >= F_SNIPE_CAM_PITCH_DEFAULT + 1.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(F_SNIPE_CAM_HEADING_DEFAULT)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(F_SNIPE_CAM_PITCH_DEFAULT)
			ELSE
				bSnipeCamDirInit = TRUE
			ENDIF
		ENDIF
		
		SET_ENTITY_HEADING(MIKE_PED_ID(), 76.0353)
		
		// Limits the movement of the scope to be locked within a certain range
		SET_FIRST_PERSON_AIM_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(F_SNIPE_CAM_HEADING_MIN,F_SNIPE_CAM_HEADING_MAX)
		SET_FIRST_PERSON_AIM_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(F_SNIPE_CAM_PITCH_MIN,F_SNIPE_CAM_PITCH_MAX)
		SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR_LIMITS_THIS_UPDATE(F_SNIPE_CAM_ZOOM_MIN, 10.0)
	ENDIF
ENDPROC

// PURPOSE: Manages the havoc when theplayers attempts a shot at the party
PROC PARTY_ManagePartyCrashed()

	MISSION_PED_FLAGS x

	IF ePartyState != PCS_NOT_CRASHED
		FOR x = mpf_party_coke_m_1 TO mpf_target
			IF DOES_ENTITY_EXIST(peds[x].id)
			AND NOT IS_PED_INJURED(peds[x].id)
				VECTOR vPedPos = GET_ENTITY_COORDS(peds[x].id)
					
				IF NOT peds[x].bFleeing
					IF vPedPos.z > 13.5
						OPEN_SEQUENCE_TASK(seqSequence)
						SWITCH x
							CASE mpf_party_coke_m_1
								TASK_PLAY_ANIM_ADVANCED(null, adPartyGetUpFlee, "Shot_Reaction_Male1", vPartySyncCoord, vPartySyncRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							BREAK
							CASE mpf_party_coke_m_3
								TASK_PLAY_ANIM_ADVANCED(null, adPartyGetUpFlee, "Shot_Reaction_Male3", vPartySyncCoord, vPartySyncRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							BREAK
							CASE mpf_party_coke_m_5
								TASK_PLAY_ANIM_ADVANCED(null, adPartyGetUpFlee, "Shot_Reaction_Male5", vPartySyncCoord, vPartySyncRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							BREAK
							CASE mpf_party_coke_f_1
								TASK_PLAY_ANIM_ADVANCED(null, adPartyGetUpFlee, "Shot_Reaction_Female", vPartySyncCoord, vPartySyncRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							BREAK
							CASE mpf_target
								SWITCH eMissionStage
									CASE ST_6_REACT_TO_SECOND
										TASK_PLAY_ANIM_ADVANCED(null, adPartyGetUpFlee, "Shot_Reaction_Male3", vPartySyncCoord, vPartySyncRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
									BREAK
									CASE ST_8_REACT_TO_THIRD
										TASK_PLAY_ANIM_ADVANCED(null, adPartyGetUpFlee, "Shot_Reaction_Male2", vPartySyncCoord, vPartySyncRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
									BREAK
								ENDSWITCH
							BREAK
						ENDSWITCH
							TASK_FOLLOW_NAV_MESH_TO_COORD(null, << -3111.2844, 352.5119, 13.4585 >>, PEDMOVE_RUN)
						CLOSE_SEQUENCE_TASK(seqSequence)
						TASK_PERFORM_SEQUENCE(peds[x].id, seqSequence)
						CLEAR_SEQUENCE_TASK(seqSequence)
						
					ELIF vPedPos.z <= 13.5
						TASK_FOLLOW_NAV_MESH_TO_COORD(peds[x].id, << -3106.1372, 347.9758, 9.8209 >>, PEDMOVE_RUN)
					ENDIF
					
					peds[x].bFleeing = TRUE
				ELSE
					IF vPedPos.z > 13.5
						IF IS_ENTITY_IN_ANGLED_AREA(peds[x].id, <<-3110.091064,355.796783,13.458379>>, <<-3113.502930,343.224304,16.231632>>, 6.812500)
							DELETE_PED(peds[x].id)
						ENDIF
					ELIF vPedPos.z <= 13.5
						IF IS_ENTITY_IN_ANGLED_AREA(peds[x].id, <<-3106.898193,346.120392,9.820868>>, <<-3104.993408,353.235138,13.080032>>, 6.812500)
							DELETE_PED(peds[x].id)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		INT y
		REPEAT COUNT_OF(ptfx_Cigs) y
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_Cigs[y])
				STOP_PARTICLE_FX_LOOPED(ptfx_Cigs[y])
			ENDIF
		ENDREPEAT
	ENDIF

	SWITCH ePartyState
		CASE PCS_NOT_CRASHED

			FOR x = mpf_party_coke_m_1 TO mpf_target
			
				IF DOES_ENTITY_EXIST(peds[x].id)
				
					// Ped was killed
					IF IS_PED_INJURED(peds[x].id)

						IF x = mpf_target
							ePartyState = PCS_TARGET_DOWN
						ELSE
							IF GET_ENTITY_MODEL(peds[x].id) = mnPartyFem
								ePartyState = PCS_KILLED_INNOCENT_FEMALE
							ELSE
								ePartyState = PCS_KILLED_INNOCENT_MALE
							ENDIF
						ENDIF
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						i_mission_substage = 0
						
					ELSE
						// Missed shot, spooked the peds
						IF HAS_PED_RECEIVED_EVENT(peds[x].id, EVENT_SHOT_FIRED)
						OR HAS_PED_RECEIVED_EVENT(peds[x].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						OR HAS_PED_RECEIVED_EVENT(peds[x].id, EVENT_SHOT_FIRED_WHIZZED_BY)
							ePartyState = PCS_MISSED_SHOT
							
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							i_mission_substage = 0
						ENDIF
						
					ENDIF
				ENDIF
			ENDFOR
			
			// Start panic walla from party
			IF ePartyState != PCS_NOT_CRASHED
				FORCE_PED_PANIC_WALLA()
				PLAY_SOUND_FROM_COORD(-1, "Party_panic", <<-3094.6677, 347.2250, 13.4458>>, "FBI_03_Torture_Sounds")
			ENDIF

		BREAK
		CASE PCS_MISSED_SHOT
			// Check if the player has killed an innocent or the target
			FOR x = mpf_party_coke_m_1 TO mpf_target
				IF DOES_ENTITY_EXIST(peds[x].id)
					IF IS_PED_INJURED(peds[x].id)
						IF x = mpf_target
							ePartyState = PCS_TARGET_DOWN
						ELSE
							IF GET_ENTITY_MODEL(peds[x].id) = mnPartyFem
								ePartyState = PCS_KILLED_INNOCENT_FEMALE
							ELSE
								ePartyState = PCS_KILLED_INNOCENT_MALE
							ENDIF
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						i_mission_substage = 0
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE PCS_KILLED_INNOCENT_MALE
			// Swap to killed female
			FOR x = mpf_party_coke_m_1 TO mpf_target
				IF DOES_ENTITY_EXIST(peds[x].id)
					IF IS_PED_INJURED(peds[x].id)
						IF x != mpf_target
							IF GET_ENTITY_MODEL(peds[x].id) = mnPartyFem
								ePartyState = PCS_KILLED_INNOCENT_FEMALE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								i_mission_substage = 0
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE PCS_KILLED_INNOCENT_FEMALE
			
		BREAK
		CASE PCS_TARGET_DOWN
			// Check if the player has killed an innocent or the target
			FOR x = mpf_party_coke_m_1 TO mpf_target
				IF DOES_ENTITY_EXIST(peds[x].id)
					IF IS_PED_INJURED(peds[x].id)
						IF x != mpf_target
							IF GET_ENTITY_MODEL(peds[x].id) = mnPartyFem
								ePartyState = PCS_KILLED_INNOCENT_FEMALE
							ELSE
								ePartyState = PCS_KILLED_INNOCENT_MALE
							ENDIF
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							i_mission_substage = 0
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL PARTY_HasTargetBeenSuccessfullyKilled()

	IF ePartyState != PCS_NOT_CRASHED
		SWITCH i_mission_substage
			CASE 0
				IF IS_SAFE_TO_START_CONVERSATION()
				
					IF IS_CUTSCENE_ACTIVE()
						REMOVE_CUTSCENE()
					ENDIF
					
					SWITCH ePartyState 
						CASE PCS_TARGET_DOWN
							IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
								SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS") 
							ENDIF
							REQUEST_CUTSCENE("fbi_3_mcs_7")
							CPRINTLN(DEBUG_MIKE, "PARTY_HasTargetBeenSuccessfullyKilled() requesting MCS_7")
							Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(FALSE, FALSE, strMrKCSHandle)
						
							IF bTargetHasBeenIDed
								RunConversation("F3_KTARGET")
								i_mission_substage = 3
							ELSE
								RunConversation("F3_RIGHTONE")
								i_mission_substage = 301
							ENDIF
						BREAK
						CASE PCS_MISSED_SHOT
							IF bTargetHasBeenIDed
								RunConversation("F3_SPOOKEDa")
								eMissionFail 	= FAIL_PARTY_SPOOKED
								i_mission_substage 	= 101
							ELSE
								RunConversation("F3_SPOOKEDe")
								eMissionFail 	= FAIL_PARTY_SPOOKED
								i_mission_substage 	= 2
							ENDIF
						BREAK
						CASE PCS_KILLED_INNOCENT_MALE
							IF bTargetHasBeenIDed
								RunConversation("F3_WRNGM2")
								eMissionFail 	= FAIL_PARTY_PED_DEAD_INNOCENT
								i_mission_substage 	= 2
							ELSE
								RunConversation("F3_KILLWRNGM")
								eMissionFail 	= FAIL_PARTY_PED_DEAD_INNOCENT_NO_ID
								i_mission_substage 	= 2
							ENDIF
						BREAK
						CASE PCS_KILLED_INNOCENT_FEMALE
							RunConversation("F3_KILLWRNGF")
							eMissionFail 	= FAIL_PARTY_PED_DEAD_INNOCENT
							i_mission_substage 	= 2
						BREAK
					ENDSWITCH
					
				ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			BREAK
			CASE 101
				IF IS_SAFE_TO_START_CONVERSATION()
					IF ePartyState != PCS_TARGET_DOWN
						IF NOT DOES_ENTITY_EXIST(peds[mpf_target].id)
							RunConversation("F3_SPOOKEDb")
							i_mission_substage 	= 2
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// FAIL
			CASE 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					bMissionFailed = TRUE
				ENDIF
			BREAK
			
			// SUCCESS
			CASE 301
				IF IS_SAFE_TO_START_CONVERSATION()
					RunConversation("F3_TELLSTEVE")
					i_mission_substage = 3
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					bSafeToSwitch = TRUE
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE: Cleans up all the party and the sniping stuff
PROC PARTY_CleanupEntitiesAndAssets(BOOL bDontCleanupDrive = FALSE)

	IF NOT bDontCleanupDrive
		IF DOES_ENTITY_EXIST(MIKE_PED_ID())
		AND NOT IS_PED_INJURED(MIKE_PED_ID())
			IF NOT IS_PED_IN_ANY_VEHICLE(MIKE_PED_ID())
				FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
			ENDIF
			SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
		ENDIF
	ENDIF
	
	DESTROY_SYNCHRONIZED_SCENE(syncScene[SCENE_COFFEE])
	DESTROY_SYNCHRONIZED_SCENE(syncScene[SCENE_SNIPE_PRONE])
	DESTROY_SYNCHRONIZED_SCENE(syncScene[SCENE_PARTY_A])
	DESTROY_SYNCHRONIZED_SCENE(syncScene[SCENE_PARTY_B])
	DESTROY_SYNCHRONIZED_SCENE(syncScene[SCENE_PARTY_C])
	DESTROY_SYNCHRONIZED_SCENE(syncScene[SCENE_PARTY_D])

	Unload_Asset_Model(sAssetData, mnPartyMale1)
	Unload_Asset_Model(sAssetData, mnPartyFem)
	Unload_Asset_Model(sAssetData, mnTarget)
	//Unload_Asset_Model(sAssetData, mnPartyMale2)
	Unload_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
	Unload_Asset_Model(sAssetData, P_FAG_PACKET_01_S)
	Unload_Asset_Model(sAssetData, P_ING_COFFEECUP_01)
	Unload_Asset_Model(sAssetData, PROP_NPC_PHONE)
	Unload_Asset_Model(sAssetData, PROP_CHAIR_01A)
	Unload_Asset_Model(sAssetData, PROP_FBI3_COFFEE_TABLE)
	Unload_Asset_Model(sAssetData, PROP_BINOC_01)
	
	Unload_Asset_Anim_Dict(sAssetData, adCoffee)
	Unload_Asset_Anim_Dict(sAssetData, adSniping)
	Unload_Asset_Anim_Dict(sAssetData, "MISSFBI3_PARTY")
	Unload_Asset_Anim_Dict(sAssetData, "MISSFBI3_PARTY_B")
	Unload_Asset_Anim_Dict(sAssetData, "MISSFBI3_PARTY_C")
	Unload_Asset_Anim_Dict(sAssetData, "MISSFBI3_PARTY_D")
	Unload_Asset_Anim_Dict(sAssetData, "MISSFBI3_PARTY_E")
	Unload_Asset_Anim_Dict(sAssetData, adPartyGetUpFlee)
	
	Unload_Asset_Weapon_Asset(sAssetData, weapMichaelsSniper)
	
	Unload_Asset_Audio_Bank(sAssetData, "FBI_03_Panic")
	
	INT z
	REPEAT COUNT_OF(ptfx_Cigs) z
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_Cigs[z])
			STOP_PARTICLE_FX_LOOPED(ptfx_Cigs[z])
		ENDIF
	ENDREPEAT

	IF NOT bDontCleanupDrive
		TidyUpVehicle(vehDriveToVeh, TRUE)
		TidyUpPed(pedDave, TRUE)
	ENDIF
	TidyUpPed(pedProneMichael, TRUE)
	TidyUpObject(propPhoneDave, TRUE)

	MISSION_OBJECT_FLAGS y
	FOR y = mof_target_ciggy TO mof_binoculars
		TidyUpObject(objs[y], TRUE)
	ENDFOR
	
	MISSION_PED_FLAGS x	
	FOR x = mpf_party_coke_m_1 TO mpf_target
		TidyUpPed(peds[x].id, TRUE)
	ENDFOR

	IF bPlayingMusic
		STOP_AUDIO_SCENE(audsceneZoom) 
		STOP_STREAM()
		bPlayingMusic = FALSE
	ENDIF
	
	IF chairBlocker != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(chairBlocker)
	ENDIF
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	ENDIF
	
	bProneSetup = FALSE
ENDPROC


//PURPOSE: Handles the switch from the free roaming before the mission starts to the torture loctation
FUNC BOOL SWITCH_Free_Roam_To_Torture()
	IF NOT bMadeSelection
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<128.64, -2200.98, 5.03>>) < 80.0
			bDisplaySwitchHUD = FALSE
		ELSE
			bDisplaySwitchHUD 				= TRUE
			g_sSelectorUI.bFX_SkipFadeOut 	= FALSE
			IF NOT UPDATE_SELECTOR_HUD(sSelectorPeds, TRUE, TRUE)
				IF bStartedAsMichael
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
				ELSE
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
				ENDIF
			ELSE
				IF bStartedAsMichael
					cam_establish = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<116.5, -2184.8, 7.5>>, <<2.2, 0.1, -147.7>>, 39.5, TRUE)
					SHAKE_CAM(cam_establish, "HAND_SHAKE", 0.4)
				ELSE
					cam_establish = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<122.2,-2182.5, 6.2>>,<<8.2, 0.9,-149.4>>, 39.9, TRUE)
					SHAKE_CAM(cam_establish, "HAND_SHAKE", 0.4)
				ENDIF
			
				sCamDetails.pedTo 		= sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]			
				sCamDetails.camTo 		= cam_establish
				bMadeSelection 			= TRUE
				i_EstablishHoldTimer 	= -1
				clearText()			
				
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				
				IF IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
				ENDIF
				
				DISABLE_PROCOBJ_CREATION()
				
				bCutsceneLoaded = FALSE
				INCRAMENT_Pause_Outro_Count()
			ENDIF
		ENDIF
	ENDIF
	
	IF bMadeSelection

		IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM_LONG_RANGE(sCamDetails, cam_establish)
		
			SWITCH_STATE swState = GET_PLAYER_SWITCH_STATE()
			
			IF swState >= SWITCH_STATE_PAN
			
				IF swState >= SWITCH_STATE_OUTRO
					IF NOT bCutsceneLoaded
					
						IF NOT IS_CUTSCENE_ACTIVE()
						
							IF bStartedAsMichael
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_3_INT", cutSections_TrevFull)
								Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, TREV_PED_ID(), csTrevor)
							ELSE
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_3_INT", cutSections_Mike)
							ENDIF
							Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(TRUE, FALSE, csMrK)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							DISPLAY_RADAR(FALSE)
							i_EstablishHoldTimer = GET_GAME_TIMER() + 4500
						
						ELIF HAS_CUTSCENE_LOADED()
							//ar_ALLOW_PLAYER_SWITCH_OUTRO()
							bCutsceneLoaded = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				// Take control of trevor when posible
				IF sCamDetails.bOKToSwitchPed
				AND NOT sCamDetails.bPedSwitched
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						// Swapping to Trevor
						IF bStartedAsMichael
							TASK_PLAY_ANIM_ADVANCED(TREV_PED_ID(), "missfbi3ig_0", "shit_loop_trev", << 129.173, -2203.357, 6.017 >>, <<0,0,79.5>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
						ELSE
							TASK_PLAY_ANIM_ADVANCED(MIKE_PED_ID(), "missfbi3leadinout", "idle_c", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), TRUE)	
						ENDIF
						
						sCamDetails.bPedSwitched = TRUE
					ENDIF
				ENDIF
				
			ENDIF	
		ELSE
			// SWITCH complete
			SET_PED_CREW(TREV_PED_ID())
			SET_PED_CREW(MIKE_PED_ID())
			bMadeSelection	= FALSE
			iAssetRequests	= 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Handles switching from the sniping scene to a cutscene instant start at the tortture room
FUNC BOOL SWITCH_Wronglocation_To_Torture_CUTSCENE()

	// Start the switch
	IF NOT bCutsceneSWITCHStarted
	
		SET_GAME_PAUSES_FOR_STREAMING(FALSE)
		SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), TRUE)
		TORTURE_LoadAssets(TRUE, TRUE)
		Load_Asset_NewLoadScene_Sphere(sAssetData, TORTURE_LOAD_SCENE_COORD, TORTURE_LOAD_SCENE_RADIUS, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
		
		//clearText()
		clearText(FALSE)			// TU FIX: michael's conversation was getting killed here going straight to Dave phoning Steve.
		bDisplaySwitchHUD 			= FALSE	
		bCutsceneSWITCHStarted 		= TRUE
		bProneSetup 				= FALSE
		bSafeToSwitch				= FALSE
		bHasSwitchSetupCam 			= FALSE
		iAssetRequests				= 0
		
	// While running
	ELSE
		IF TORTURE_IsTortureReady(TRUE, TRUE, FALSE)
		AND HAS_COLLISION_LOADED_AROUND_ENTITY(TREV_PED_ID())
		//AND IS_NEW_LOAD_SCENE_LOADED()
		AND bSafeToSwitch

			CDEBUG1LN(DEBUG_MIKE, "TORTURE Torture ready, collision loaded, scene loaded, moving to new locate")
		
			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_FOCUS_CAM_PHOTOSHOOT")
				STOP_AUDIO_SCENE("FBI_3_FOCUS_CAM_PHOTOSHOOT")
			ENDIF
			START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_1")
			
			doTwoPointCam(camGeneralInit, camGeneralDest,	<< 146.7890, -2204.1140, 5.6386 >>, << -10.8642, 7.4269, 61.6078 >>, 17.4610,
									<< 146.7890, -2204.1140, 5.6386 >>, << -10.8642, 7.4269, 61.6078 >>, 16.0803,
									5000)
			doCamShake(camGeneralInit, camGeneralDest, shakeHand, 0.1)
			bHasSwitchSetupCam = TRUE			

			IF DOES_CAM_EXIST(camWrongLocation)
				DESTROY_CAM(camWrongLocation)
			ENDIF
			
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
			SET_PED_CREW(TREV_PED_ID())
			SET_PED_CREW(MIKE_PED_ID())
			
			ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
			PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
			
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			Unload_Asset_NewLoadScene(sAssetData)
			
			TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)	
			
			// Clean up wrong location scene 
			INT i
			REPEAT COUNT_OF(pedCunts) i
				IF DOES_ENTITY_EXIST(pedCunts[i])
					DELETE_PED(pedCunts[i])
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(objCunts) i
				IF DOES_ENTITY_EXIST(objCunts[i])
					DELETE_OBJECT(objCunts[i])
				ENDIF
			ENDREPEAT
			
			Unload_Asset_Anim_Dict(sAssetData, adWrongHouse)
			
			IF DOES_ENTITY_EXIST(pedDave)
				DELETE_PED(pedDave)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTrevor)
				DELETE_VEHICLE(vehTrevor)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehDriveToVeh)
				DELETE_VEHICLE(vehDriveToVeh)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehReplay)
				DELETE_VEHICLE(vehReplay)
			ENDIF
			
			REMOVE_SCENARIO_BLOCKING_AREA(sba_wrongLocation)
			
			CLEAR_PED_NON_CREATION_AREA()

			SET_PED_PATHS_BACK_TO_ORIGINAL(<<-890.959290,-4.526050,48.068375>> - <<35.000000,30.000000,10.437500>>, 
				<<-890.959290,-4.526050,48.068375>> + <<35.000000,30.000000,10.437500>>)
				
			FADE_UP_PED_LIGHT()

			bMadeSelection				= FALSE
			bCutsceneSWITCHStarted 		= FALSE
			iAssetRequests				= 0
			
			RETURN TRUE
				
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Handles the switch from the torture room to the chumash drive
FUNC BOOL SWITCH_Torture_To_Coffee()	

	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	
	// Start the switch
	IF NOT bCutsceneSWITCHStarted
		IF HAS_CUTSCENE_LOADED()
#IF IS_JAPANESE_BUILD
		AND IS_SCREEN_FADED_OUT()
#ENDIF		
		
			
			
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			IF DOES_ENTITY_EXIST( MIKE_PED_ID() )
				
//				CPRINTLN( DEBUG_MIKE, "*** FBI3 *** - ", "SWITCH_Torture_To_Coffee() Setting Mike as focus entity")
				FREEZE_ENTITY_POSITION(MIKE_PED_ID(), FALSE)
				SET_ENTITY_COORDS(MIKE_PED_ID(), vCoffeeShopMikeCoord)
				SET_ENTITY_HEADING(MIKE_PED_ID(), fCoffeeShopMikeHeading)
				FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
				SET_FOCUS_ENTITY(MIKE_PED_ID())
				
				IF IS_ENTITY_FOCUS( MIKE_PED_ID() )
//					CPRINTLN( DEBUG_MIKE, "*** FBI3 *** - ", "SWITCH_Torture_To_Coffee() SUCCEEDED to set MIKE as focus ped")
//					SCRIPT_ASSERT( "*** FBI3 *** - SWITCH_Torture_To_Coffee() SUCCEEDED to set MIKE as focus ped" )
				ELSE
//					CPRINTLN( DEBUG_MIKE, "*** FBI3 *** - ", "SWITCH_Torture_To_Coffee() FAILED to set MIKE as focus ped")
//					SCRIPT_ASSERT( "*** FBI3 *** - SWITCH_Torture_To_Coffee() FAILED to set MIKE as focus ped" )
				ENDIF
				
			ELSE
//				CPRINTLN( DEBUG_MIKE, "*** FBI3 *** - ", "SWITCH_Torture_To_Coffee() MIKE_PED_ID() does not exist cannot set ped as focus")
//				SCRIPT_ASSERT( "*** FBI3 *** - SWITCH_Torture_To_Coffee() MIKE_PED_ID() does not exist cannot set ped as focus" )
			ENDIF

			
			IF Load_Asset_NewLoadScene_Frustum(sAssetData, <<-1270.9983, -891.1161, 12.0253>>, NORMALISE_VECTOR(<<-1267.3468, -870.2449, 16.8556>> - <<-1270.9983, -891.1161, 12.0253>>), 40.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
//				CPRINTLN(DEBUG_MIKE, "LOAD SCENE STARTED")
			ELSE
//				CPRINTLN(DEBUG_MIKE, "LOAD SCENE COULDN'T START")
			ENDIF

			IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_1")
				STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_1")
			ENDIF
		
			REGISTER_ENTITY_FOR_CUTSCENE(pedSteve, csSteve, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(pedVictim, strMrKCSHandle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, CS_MRK)
			TORTURE_DetachAllProps()		// detach the weapons so the cutscene can animate them
			TORTURE_RegisterAllPropsForCutscene(FALSE)
			
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			START_CUTSCENE( CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN )
			
			/*
			IF NOT IS_REPLAY_RECORDING()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			ENDIF
			*/
			
			clearText()
			bDisplaySwitchHUD 			= FALSE	
			bCutsceneSWITCHStarted 		= TRUE
			bProneSetup 				= FALSE
			bPerformedPostTortureFadeIn = FALSE
			bTrevorExited				= FALSE
			bCamExited					= FALSE
			iCoffeeStage				= 0
			iAssetRequests				= 0
			bMusicCueTriggered			= FALSE
		ENDIF
	
	// While running
	ELSE

		SWITCH iCoffeeStage
			CASE 0
				IF IS_CUTSCENE_PLAYING()
					SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
					IF NOT bPerformedPostTortureFadeIn
						SAFE_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						bPerformedPostTortureFadeIn = TRUE
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF IS_REPLAY_RECORDING()
						REPLAY_STOP_EVENT()
					ENDIF
					
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					
					bCamExited = TRUE
					RESET_ADAPTATION()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					bTrevorExited = TRUE
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
//					CPRINTLN(DEBUG_MIKE, "Trevor exited cutscene")
				ENDIF
				
				IF PARTY_LoadAssets(TRUE, FALSE)
				AND PARTY_IsSceneReady(TRUE, FALSE, ST_4_DRIVE_TO_COORD_SECOND)
				AND HAS_COLLISION_LOADED_AROUND_ENTITY(MIKE_PED_ID())
				AND IS_NEW_LOAD_SCENE_LOADED()
				AND HAS_ANIM_DICT_LOADED(adCoffee)
				AND bTrevorExited
				AND bCamExited
				
//					CPRINTLN( DEBUG_MIKE, "*** FBI3 *** - ", "SWITCH_Torture_To_Coffee() Exit cutscene and start coffee scene" )
				
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					SET_PED_CREW(TREV_PED_ID())
					SET_PED_CREW(MIKE_PED_ID())
					
					// bug 1695494
					IF PLAYER_PED_ID() = MIKE_PED_ID()
					AND IS_ENTITY_FOCUS(MIKE_PED_ID())
					AND IS_NEW_LOAD_SCENE_ACTIVE()
	//					VECTOR vPedCoord
	//					vPedCoord = GET_ENTITY_COORDS( MIKE_PED_ID() )
	//					CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - SWITCH_Torture_To_Coffee() PLAYER_PED is Michael, Michael is the focus entity, Michael is at = ", vPedCoord )
						
	//					CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", "... clearing the focus... " )
						SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						CLEAR_FOCUS()
						
	//					CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", "... and unloading the new load scene" )
						Unload_Asset_NewLoadScene(sAssetData) 

					ENDIF
					
					TORTURE_CleanupEntitiesAndAssets()
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SAFE_FADE_IN()
					
					ADD_ALL_PEDS_TO_DIALOGUE()
					
					// coffee scene 
					RunConversation("F3_COFFEE")
					
					camSyncSceneAnimated = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					syncScene[SCENE_COFFEE] = CREATE_SYNCHRONIZED_SCENE(vCoffeeIGCoord, vCoffeeIGRot)
					TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), 	syncScene[SCENE_COFFEE], 	adCoffee, 	"002291_02_fbi_3_coffee_w_dave_exit_mic", 	INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_TAG_SYNC_OUT)
					TASK_SYNCHRONIZED_SCENE(pedDave, 		syncScene[SCENE_COFFEE], 	adCoffee, 	"002291_02_fbi_3_coffee_w_dave_exit_dave", 	INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_TAG_SYNC_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_coffee_chair], syncScene[SCENE_COFFEE], "002291_02_fbi_3_coffee_w_dave_exit_chair", adCoffee, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT))
					PLAY_SYNCHRONIZED_CAM_ANIM(camSyncSceneAnimated, syncScene[SCENE_COFFEE], "002291_02_fbi_3_coffee_w_dave_exit_cam", adCoffee)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncScene[SCENE_COFFEE], FALSE)
					
					// added bug 1890635
					IF IS_SCRIPT_GLOBAL_SHAKING()
						STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
					ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE_PED_ID())
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDave)
					
					SET_FORCE_FOOTSTEP_UPDATE(MIKE_PED_ID(), TRUE)
					SET_FORCE_FOOTSTEP_UPDATE(pedDave, TRUE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					START_AUDIO_SCENE("FBI_3_MICHAEL_COFFEE_BREAK")
					TRIGGER_MUSIC_EVENT("FBI3_BACK_TO_MICHAEL")
					SETTIMERB(0)
					
					DISPLAY_RADAR(FALSE)
					
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					
					sba_coffee_shop = ADD_SCENARIO_BLOCKING_AREA(<<-1267.129395,-887.206116,12.520452>> - <<12.437500,9.437500,2.500000>>, 
						<<-1267.129395,-887.206116,12.520452>> - <<12.437500,9.437500,2.500000>>)
						
					CLEAR_AREA(<<-1267.129395,-887.206116,12.520452>>, 15.0, TRUE)
					
					bPerformedPostTortureFadeIn = FALSE
					bMichaelExited				= FALSE
					bDaveExited					= FALSE
					bCamExited					= FALSE
					iCoffeeStage++
				ELSE
//					CPRINTLN( DEBUG_MIKE, "*** FBI3 *** - ", "SWITCH_Torture_To_Coffee() waiting for everything to load so we can fade back in..." )
//
//					IF PLAYER_PED_ID() = MIKE_PED_ID()
//						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - PLAYER_PED_ID() = Mike " )
//					ELIF PLAYER_PED_ID() = TREV_PED_ID()
//						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - PLAYER_PED_ID() = Trev " )
//					ENDIF
//					
//					VECTOR vCoord
//					IF IS_ENTITY_FOCUS( MIKE_PED_ID() )
//						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - Focus entity = Mike " )
//						vCoord = GET_ENTITY_COORDS( MIKE_PED_ID() )
//						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - Michael is at = ", vCoord )
//					ELIF IS_ENTITY_FOCUS( TREV_PED_ID() )
//						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - Focus entity = Trev " )
//						vCoord = GET_ENTITY_COORDS( TREV_PED_ID() )
//						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - Trevor is at = ", vCoord )
//					ENDIF
//
//					vCoord = GET_GAMEPLAY_CAM_COORD()
//					CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", " - Camera is at = ", vCoord )
				ENDIF
			BREAK
			CASE 1
				IF NOT bMusicCueTriggered
					IF TIMERB() > 3500
						TRIGGER_MUSIC_EVENT("FBI3_MICHAEL_MUSIC_2")
						bMusicCueTriggered = TRUE
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_PLAYING(syncScene[SCENE_COFFEE])
				
					IF IS_SCREEN_FADED_IN()
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
						AND IS_SCENE_AT_PHASE(SCENE_COFFEE, 0.0, 0.675)
						AND NOT bMissionFailed
							CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", "Coffee cutscene skipped, doing fade out" )
							SAFE_FADE_OUT()
						ENDIF
					ENDIF
					
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					// Skip to the end of the cutscene
					IF IS_SCREEN_FADED_OUT()
					
						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", "Coffee scene is faded out stop new load scene and start a new 1" )
					
						Unload_Asset_NewLoadScene(sAssetData)
						Load_Asset_NewLoadScene_Frustum(sAssetData, <<-1272.1626, -880.3244, 12.8902>>, NORMALISE_VECTOR(<<-1270.2233, -883.1075, 12.5055>> - <<-1272.1626, -880.3244, 12.8902>>), 100)
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						DESTROY_CAM(camSyncSceneAnimated)
						
						SET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE], 1.0)
						
						SET_ENTITY_COORDS(MIKE_PED_ID(), <<-1267.2947, -888.0948, 10.4967>>)
						SET_ENTITY_HEADING(MIKE_PED_ID(), 209.4294)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDave)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						IF DOES_ENTITY_EXIST(objs[mof_coffee_dave])
							IF IS_ENTITY_ATTACHED(objs[mof_coffee_dave])
								DETACH_ENTITY(objs[mof_coffee_dave])
							ENDIF
							DELETE_OBJECT(objs[mof_coffee_dave])
						ENDIF
						
						IF DOES_ENTITY_EXIST(objs[mof_coffee_mike])
							IF IS_ENTITY_ATTACHED(objs[mof_coffee_mike])
								DETACH_ENTITY(objs[mof_coffee_mike])
							ENDIF
							DELETE_OBJECT(objs[mof_coffee_mike])
						ENDIF
						
						IF DOES_ENTITY_EXIST(propPhoneDave)
							IF IS_ENTITY_ATTACHED(propPhoneDave)
								DETACH_ENTITY(propPhoneDave)
							ENDIF
							DELETE_OBJECT(propPhoneDave)
						ENDIF
						
						IF NOT bMusicCueTriggered
							TRIGGER_MUSIC_EVENT("FBI3_MICHAEL_MUSIC_2")
							bMusicCueTriggered = TRUE
						ENDIF
						
					ELSE
				
						// Detach cups
						IF IS_ENTITY_ATTACHED(objs[mof_coffee_mike])
							//IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE]) >= 0.55
							IF HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("DetachCup"))
								DETACH_ENTITY(objs[mof_coffee_mike], FALSE, TRUE)
								FREEZE_ENTITY_POSITION(objs[mof_coffee_mike], TRUE)
							ENDIF
						ENDIF
						IF IS_ENTITY_ATTACHED(objs[mof_coffee_dave])
							IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE]) >= 0.675
								DETACH_ENTITY(objs[mof_coffee_dave], TRUE)
							ENDIF
						ENDIF

						// Delete phone
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE]) >= 0.90
							
							IF DOES_ENTITY_EXIST(propPhoneDave)
								IF IS_ENTITY_ATTACHED(propPhoneDave)
									DETACH_ENTITY(propPhoneDave)
								ENDIF
								DELETE_OBJECT(propPhoneDave)
							ENDIF
							
						ENDIF
						
						// Blend out peds
						IF NOT bDaveExited
							//IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE]) >= 0.896
							IF HAS_ANIM_EVENT_FIRED(pedDave, GET_HASH_KEY("ENDS_IN_WALK"))
								
								OPEN_SEQUENCE_TASK(seqSequence)
									TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_ON_FOOT_WALK))
									IF IS_VEHICLE_DRIVEABLE(vehDriveToVeh)
										TASK_ENTER_VEHICLE(null, vehDriveToVeh, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
									ELSE
										TASK_TURN_PED_TO_FACE_ENTITY(null, MIKE_PED_ID(), -1)
									ENDIF
								CLOSE_SEQUENCE_TASK(seqSequence)
								TASK_PERFORM_SEQUENCE(pedDave, seqSequence)
								CLEAR_SEQUENCE_TASK(seqSequence)

								bDaveExited = TRUE
							ENDIF
						ENDIF
						
						IF NOT bMichaelExited
							//IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE]) >= 0.928
							IF HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("WalkInterruptible"))

								TASK_FORCE_MOTION_STATE(MIKE_PED_ID(), enum_to_int(MS_ON_FOOT_WALK))
								SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500, 192.2249, FALSE)
								SET_FORCE_FOOTSTEP_UPDATE(MIKE_PED_ID(), FALSE)
								
								bMichaelExited = TRUE
							ENDIF
						ENDIF
						
						IF NOT bMichaelExited
							
						ENDIF
						
						IF NOT IS_GAMEPLAY_CAM_RENDERING()
						AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
							FLOAT fInterruptPhase, fDummy
							IF FIND_ANIM_EVENT_PHASE(adCoffee, "002291_02_fbi_3_coffee_w_dave_exit_mic", "WalkInterruptible", fInterruptPhase, fDummy)
								CPRINTLN(DEBUG_MIKE, "Interrupt Phase Start: ", fInterruptPhase, " End: ", fDummy)
								IF GET_SYNCHRONIZED_SCENE_PHASE(syncScene[SCENE_COFFEE]) >= fInterruptPhase - (3.0/16.667)   // START BLEND OUT 2.5 seconds before the 
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(212.243 - GET_ENTITY_HEADING(MIKE_PED_ID()))
									RENDER_SCRIPT_CAMS(FALSE, TRUE, 3000, FALSE)
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF

				ELSE
					IF IS_SCREEN_FADED_IN()
					OR NOT IS_NEW_LOAD_SCENE_ACTIVE()
					OR IS_NEW_LOAD_SCENE_LOADED()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						DISABLE_CELLPHONE(FALSE)
						DISPLAY_RADAR(TRUE)
						
						STOP_SYNCHRONIZED_ENTITY_ANIM(MIKE_PED_ID(), NORMAL_BLEND_OUT, TRUE)
						STOP_SYNCHRONIZED_ENTITY_ANIM(pedDave, NORMAL_BLEND_OUT, TRUE)
						STOP_SYNCHRONIZED_ENTITY_ANIM(objs[mof_coffee_chair], NORMAL_BLEND_OUT, TRUE)
						
						DELETE_OBJECT(objs[mof_coffee_chair])
						REMOVE_MODEL_HIDE(<<-1267.62, -880.46, 10.95>>, 0.25, PROP_CHAIR_01A)
						
						DESTROY_CAM(camSyncSceneAnimated)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDave, TRUE)
						
						IF IS_VEHICLE_DRIVEABLE(vehDriveToVeh)				
							SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vehDriveToVeh)
						ENDIF
						
						SAFE_FADE_IN()
						
						iDriveStoppedTimer 		= -1
						iDriveComplaintTimer 	= -1
						bEventFlag[0]			= FALSE
						bEventFlag[1]			= FALSE
						bEventFlag[2]			= FALSE
						bEventFlag[3]			= FALSE
						str_InteruptedLabel		= ""
						str_InteruptedRoot		= ""
						bManageParty			= FALSE
						bMadeSelection			= FALSE
						bCutsceneSWITCHStarted 	= FALSE
						iAssetRequests			= 0
						bDisplayHeartMonitor 	= FALSE
						
						Unload_Asset_NewLoadScene( sAssetData )
						
						RETURN TRUE
					ELSE
						CPRINTLN( DEBUG_MIKE, "*** FBI3 ***", "Coffee scene waiting on new load scene while faded out" )
					ENDIF
				ENDIF
			
			BREAK
		ENDSWITCH
		
		IF iCoffeeStage > 0
			bDisplayHeartMonitor = FALSE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//PURPOSE: Handles switching from the sniping scene to a cutscene instant start at the tortture room
FUNC BOOL SWITCH_Torture_To_Sniping_CUTSCENE()

	// Start the switch
	IF NOT bCutsceneSWITCHStarted
		IF HAS_CUTSCENE_LOADED()
#IF IS_JAPANESE_BUILD
		AND IS_SCREEN_FADED_OUT()
#ENDIF		
		
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			SET_FOCUS_ENTITY(MIKE_PED_ID())
			CPRINTLN(DEBUG_MIKE, "SWITCH_Torture_To_Sniping_CUTSCENE()	Setting Mike as focus entity")
			//SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), TRUE)
			Load_Asset_NewLoadScene_Frustum(sAssetData, vNewLoadSceneSnipeCoord, vNewLoadSceneSnipeDir, fNewLoadSceneSnipeFarClip, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
			
			SWITCH eMissionStage
				CASE ST_5_TORTURE_FOR_SECOND
					IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_2")
						STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_2")
					ENDIF
				BREAK
				CASE ST_7_TORTURE_FOR_THIRD
					IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_3")
						STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_3")
					ENDIF
				BREAK
				CASE ST_9_TORTURE_FINAL
					IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_TORTURE_4")
						STOP_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_4")
					ENDIF
				BREAK
			ENDSWITCH
			
			IF NOT bStoredSniperSnapshot
				IF DOES_ENTITY_EXIST(MIKE_PED_ID())
					STORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
				ENDIF
				sSniperSnapshot = g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21]
				g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21].iAmmoCount 			= 0
				g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21].iModsAsBitfield 	= 0
				g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21].iTint 				= 0
				g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21].iCamo 				= 0
				IF DOES_ENTITY_EXIST(MIKE_PED_ID())
					RESTORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
				ENDIF
				bStoredSniperSnapshot = TRUE
			ENDIF
		
			REGISTER_ENTITY_FOR_CUTSCENE(pedSteve, csSteve, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(pedVictim, strMrKCSHandle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, CS_MRK)
			TORTURE_DetachAllProps()		// detach the weapons so the cutscene can animate them
			TORTURE_RegisterAllPropsForCutscene(FALSE)
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			START_CUTSCENE()
			
			IF NOT IS_REPLAY_RECORDING()
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			ENDIF
			
			clearText()
			bDisplaySwitchHUD 			= FALSE	
			bCutsceneSWITCHStarted 		= TRUE
			bProneSetup 				= FALSE
			bPerformedPostTortureFadeIn = FALSE
			bTrevorExited				= FALSE
			bCamExited 					= FALSE
			bRunPostSwitchFocusCleanup 	= FALSE
			iAssetRequests				= 0
		ENDIF
	
	// While running
	ELSE
		IF NOT bRunPostSwitchFocusCleanup
			IF IS_CUTSCENE_PLAYING()
				SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
				IF NOT bPerformedPostTortureFadeIn
					SAFE_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					bPerformedPostTortureFadeIn = TRUE
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
				bTrevorExited = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF IS_REPLAY_RECORDING()
					REPLAY_STOP_EVENT()
				ENDIF
				
				setupSwayCam(sSwaySniping, vProneCamStartCoord, vProneCamStartRot, fProneCamStartFOV,
									vProneCamEndCoord, vProneCamEndRot, fProneCamEndFOV,
									40000, TRUE, shakeHand, 0.5, "SNIPE")
			
				doSwayCam(sSwaySniping)
				
				ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				
				bCamExited = TRUE
			ENDIF
				
			IF PARTY_LoadAssets(FALSE, TRUE)
			AND PARTY_IsSceneReady(FALSE, TRUE, int_to_enum(MISSION_STAGE, enum_to_int(eMissionStage)+1))
			AND HAS_COLLISION_LOADED_AROUND_ENTITY(MIKE_PED_ID())
			AND IS_NEW_LOAD_SCENE_LOADED()
			AND bTrevorExited
			AND bCamExited
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				SET_PED_CREW(TREV_PED_ID())
				SET_PED_CREW(MIKE_PED_ID())
				
				TORTURE_CleanupEntitiesAndAssets()

				SWITCH eMissionStage
					CASE ST_5_TORTURE_FOR_SECOND
						IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_1")
							START_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_1")
						ENDIF
					BREAK
					CASE ST_7_TORTURE_FOR_THIRD
						IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_2")
							START_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_2")
						ENDIF
					BREAK
					CASE ST_9_TORTURE_FINAL
						IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_3")
							START_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_3")
						ENDIF
					BREAK
				ENDSWITCH

				bRunPostSwitchFocusCleanup 	= TRUE

				SAFE_FADE_IN()
				
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)

			ENDIF
		ELSE
			//SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE)
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			CLEAR_FOCUS()
			Unload_Asset_NewLoadScene(sAssetData)
		
			bMadeSelection				= FALSE
			bCutsceneSWITCHStarted 		= FALSE
			bPerformedPostTortureFadeIn = FALSE
			iAssetRequests				= 0

			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: Handles switching from the sniping scene to a cutscene instant start at the tortture room
FUNC BOOL SWITCH_Sniping_To_Torture_CUTSCENE(MISSION_STAGE eStage = ST_10_REACT_FINAL)

	IF NOT bCutSceneSWITCHPreped
		SWITCH eStage
			CASE ST_4_DRIVE_TO_COORD_SECOND		FALLTHRU
			CASE ST_6_REACT_TO_SECOND			FALLTHRU
			CASE ST_8_REACT_TO_THIRD 			
				Load_Asset_NewLoadScene_Sphere(sAssetData, TORTURE_LOAD_SCENE_COORD, TORTURE_LOAD_SCENE_RADIUS, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				TORTURE_LoadAssets(FALSE, FALSE)
			BREAK
			CASE ST_10_REACT_FINAL
				Load_Asset_NewLoadScene_Sphere(sAssetData, TORTURE_LOAD_SCENE_COORD, TORTURE_LOAD_SCENE_RADIUS, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				Load_Asset_Scaleform(sAssetData, sfHeart, sfHeartMonitor)
			BREAK
			
		ENDSWITCH
		
		SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), TRUE)
		SET_GAME_PAUSES_FOR_STREAMING(FALSE)
		
		g_sSelectorUI.bFX_SkipFadeOut 	= TRUE
		bRunPostSwitchFocusCleanup		= FALSE
		bPromptedSWITCH 				= FALSE
		bCutSceneSWITCHPreped 			= TRUE
		bTrevorExited					= FALSE
		bMovedFocus						= FALSE
		bPerformedPostTortureFadeIn		= FALSE
		iAssetRequests					= 0
		iSwitchFailTimer				= -1
	ENDIF

	IF bCutSceneSWITCHPreped
	AND bSafeToSwitch
		// Start the switch
		IF NOT bCutsceneSWITCHStarted
			IF HAS_CUTSCENE_LOADED()
			
				BOOL bStartCutscene
			
				SWITCH eStage
					CASE ST_4_DRIVE_TO_COORD_SECOND
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_SCREEN_FADING_OUT()
						AND TORTURE_IsTortureReady(FALSE, FALSE, FALSE)
						
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

							REGISTER_ENTITY_FOR_CUTSCENE(TREV_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(pedDave, csDave, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(null, csSteve, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_STEVEHAINS)
							REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mnVictim)
							REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, mnVictim)
							
							TORTURE_RegisterAllPropsForCutscene(TRUE, TRUE)
							TORTURE_DetachAllProps()
							
							clearText()
							bMadeSelection 			= TRUE
							bDisplaySwitchHUD 		= FALSE	
							bManageParty 			= FALSE
							bCutsceneSWITCHStarted 	= TRUE
							bTortureMusicPlaying	= FALSE
							
							IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_DRIVE_CHUMASH")
								STOP_AUDIO_SCENE("FBI_3_MICHAEL_DRIVE_CHUMASH")
							ENDIF
	
							SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
							START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
														
							CASCADE_SHADOWS_INIT_SESSION()
							SET_MICROPHONE_POSITION(FALSE, <<0,0,0>>, <<0,0,0>>, <<0,0,0>>)
							
							// DONT TRIGGER TORTURE MUSIC YET AS CUTSCENE STARTS WITH MICHAEL AND DAVE GETTING INTO POSITION
						ENDIF
					BREAK
					CASE ST_6_REACT_TO_SECOND
					FALLTHRU
					CASE ST_8_REACT_TO_THIRD
					
						IF IS_NEW_LOAD_SCENE_LOADED()
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(TREV_PED_ID())
						AND TORTURE_IsTortureReady(FALSE, FALSE, FALSE)
						#IF IS_DEBUG_BUILD
						AND NOT b_SimulatedStreamingStuck
						#ENDIF
						
							IF IS_SCREEN_FADED_IN()
								iSwitchFailTimer = -1

								IF NOT UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE)
									SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
									SET_SELECTOR_PED_ACTIVITY(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_ACTIVITY_TORTURE)
									
									IF NOT bPromptedSWITCH
										PRINT_NOW(godSwitchTrevor, DEFAULT_GOD_TEXT_TIME, 1)
										bPromptedSWITCH = TRUE
									ENDIF
								ELSE
									bStartCutscene = TRUE
								ENDIF
								
							ELIF IS_SCREEN_FADED_OUT()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									bStartCutscene = TRUE
								ENDIF
							ENDIF
						ELSE
						
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF iSwitchFailTimer = -1
									iSwitchFailTimer = GET_GAME_TIMER()
								ENDIF
							
								// Safety fade out after 7 seconds
								IF IS_SCREEN_FADED_IN()
									IF GET_GAME_TIMER() - iSwitchFailTimer > 7000
									AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										SAFE_FADE_OUT()
									ENDIF
									TEXT_LABEL_63 strDebug
									strDebug = "iSwitchFailTimer: "
									strDebug += GET_GAME_TIMER() - iSwitchFailTimer
									DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.5, 0.0>>, 0,0,0, 255)
									
								// Once faded out clean up
								ELIF IS_SCREEN_FADED_OUT()
									
									IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
										FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
										MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
										TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
										PARTY_CleanupEntitiesAndAssets()
										RENDER_SCRIPT_CAMS(FALSE, FALSE)
										IF DOES_CAM_EXIST(sSwaySniping.camInit)
											DESTROY_CAM(sSwaySniping.camInit)
										ENDIF
										IF DOES_CAM_EXIST(sSwaySniping.camDest)
											DESTROY_CAM(sSwaySniping.camDest)
										ENDIF
									ENDIF
								
								ENDIF
							ENDIF
						ENDIF
						
						IF bStartCutscene
						
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						
							REGISTER_ENTITY_FOR_CUTSCENE(TREV_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(null, csSteve, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_STEVEHAINS)
							REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mnVictim)
							REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, mnVictim)
							
							TORTURE_DetachAllProps()
							TORTURE_RegisterAllPropsForCutscene(TRUE)
							clearText()
							bMadeSelection 			= TRUE
							bDisplaySwitchHUD 		= FALSE	
							bManageParty 			= FALSE
							bCutsceneSWITCHStarted 	= TRUE
							bPlayedSwitchSting 		= FALSE
							
							IF eMissionStage = ST_6_REACT_TO_SECOND
								IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_1")
									STOP_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_1")
								ENDIF
							ELIF eMissionStage = ST_8_REACT_TO_THIRD
								IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_2")
									STOP_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_2")
								ENDIF
							ENDIF
							
							IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_SNIPER_SCOPE")
								STOP_AUDIO_SCENE("FBI_3_MICHAEL_SNIPER_SCOPE")
							ENDIF
							
							SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
							START_CUTSCENE( CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH | CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN )
							SET_MICROPHONE_POSITION(FALSE, <<0,0,0>>, <<0,0,0>>, <<0,0,0>>)
							
							TRIGGER_MUSIC_EVENT("FBI3_TORTURE")
							bTortureMusicPlaying = TRUE
						ENDIF
					BREAK
					CASE ST_10_REACT_FINAL
					// if final switch just take control of trevor at the end					
						IF IS_NEW_LOAD_SCENE_LOADED()
						AND HAS_COLLISION_LOADED_AROUND_ENTITY(TREV_PED_ID())
						AND IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)						
						AND HAS_SCALEFORM_MOVIE_LOADED( sfHeartMonitor )
						#IF IS_DEBUG_BUILD
						AND NOT b_SimulatedStreamingStuck
						#ENDIF
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF IS_SCREEN_FADED_IN()
								OR IS_SCREEN_FADED_OUT()
									bStartCutscene = TRUE
								ENDIF
							ENDIF
							
						// Not loaded yet
						ELSE
						
							IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
								REGISTER_NAMED_RENDERTARGET(sECGRenderTarget)
								
								IF NOT IS_NAMED_RENDERTARGET_LINKED(mnMonitor)
									LINK_NAMED_RENDERTARGET(mnMonitor)
								ENDIF
								
								rtECG = GET_NAMED_RENDERTARGET_RENDER_ID(sECGRenderTarget)
							ENDIF
						
							IF IS_SCREEN_FADED_IN()
							
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
									// start the safety fade timer
									IF iSwitchFailTimer = -1
										iSwitchFailTimer = GET_GAME_TIMER()
									ENDIF
							
									// Safety fade out after 7 seconds
									IF GET_GAME_TIMER() - iSwitchFailTimer > 7000
										SAFE_FADE_OUT()
									ENDIF
								
								ENDIF
								
								TEXT_LABEL_63 strDebug
								strDebug = "iSwitchFailTimer: "
								strDebug += GET_GAME_TIMER() - iSwitchFailTimer
								DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.5, 0.0>>, 0,0,0, 255)
								
							// Once faded out clean up the current scene
							ELIF IS_SCREEN_FADED_OUT()
								
								IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
									FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
									MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
									TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
									PARTY_CleanupEntitiesAndAssets()
									RENDER_SCRIPT_CAMS(FALSE, FALSE)
									IF DOES_CAM_EXIST(sSwaySniping.camInit)
										DESTROY_CAM(sSwaySniping.camInit)
									ENDIF
									IF DOES_CAM_EXIST(sSwaySniping.camDest)
										DESTROY_CAM(sSwaySniping.camDest)
									ENDIF
								ENDIF
								
							ENDIF
							
						ENDIF
						
						IF bStartCutscene
						
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						
							REGISTER_ENTITY_FOR_CUTSCENE(TREV_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_MRK)
							REGISTER_ENTITY_FOR_CUTSCENE(null, csTrevorCar, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_VEH_MODEL(CHAR_TREVOR))
							REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, mnVictim)
							
							TORTURE_DetachAllProps()
							TORTURE_RegisterAllPropsForCutscene(FALSE)
							
							bMrKVariations[MR_K_CABLES] = FALSE
							
							clearText()
							bMadeSelection 			= TRUE
							bDisplaySwitchHUD 		= FALSE	
							bManageParty 			= FALSE
							bCutsceneSWITCHStarted 	= TRUE
							bPlayedSwitchSting 		= FALSE
							
							IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_VANTAGE_POINT_3")
								STOP_AUDIO_SCENE("FBI_3_MICHAEL_VANTAGE_POINT_3")
							ENDIF
							IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_SNIPER_SCOPE_TARGET")
								STOP_AUDIO_SCENE("FBI_3_MICHAEL_SNIPER_SCOPE_TARGET")
							ENDIF
							
							SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
							START_CUTSCENE()
							SET_MICROPHONE_POSITION(FALSE, <<0,0,0>>, <<0,0,0>>, <<0,0,0>>)
							
							SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
							CLEAR_AREA_OF_VEHICLES(vTortureScene, 500.0)
						
						ENDIF
						
					BREAK
				ENDSWITCH
			ENDIF
		
		// While running
		ELSE
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		
			SWITCH eStage
				CASE ST_4_DRIVE_TO_COORD_SECOND
					
					IF NOT bRunPostSwitchFocusCleanup
						IF IS_CUTSCENE_PLAYING()
							PARTY_CleanupEntitiesAndAssets()
							TORTURE_GrabAllPedsFromCutscene()
							TORTURE_GrabAllPropsFromCutscene(TRUE)
							
							IF GET_CUTSCENE_TIME() >= 13600
								IF NOT bTortureMusicPlaying
								AND GET_CUTSCENE_TIME() < 14600
								AND HAS_CUTSCENE_CUT_THIS_FRAME()
									TRIGGER_MUSIC_EVENT("FBI3_TORTURE")
									bTortureMusicPlaying 	= TRUE
									ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
									PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								ENDIF
								bDisplayHeartMonitor = TRUE
							ENDIF
							SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
						ENDIF
						
						TORTURE_ResetAllProps(TRUE)
						
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							CPRINTLN(DEBUG_MIKE, "************ CUTSCENE EXIT CAMERA ***********")
							bCamExited = TRUE
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
						
							CPRINTLN(DEBUG_MIKE, "************ CUTSCENE EXIT TREVOR ***********")

							FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
							SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE)
							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
							FREEZE_ENTITY_POSITION(TREV_PED_ID(), FALSE)
							
							bHasPlayedPickupAnim = FALSE

							TORTURE_ManageWeaponSelection()
							
							TORTURE_InitWeapSelectionCam()
							TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
							
							START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_2")
							
							SAFE_FADE_IN()

							bRunPostSwitchFocusCleanup = TRUE
							bTrevorExited = TRUE
						ENDIF
					ELSE
					
//						SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE) - Moved due to url:bugstar:2073069
						SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						Unload_Asset_NewLoadScene(sAssetData)
					
						bMadeSelection			= FALSE
						bCutsceneSWITCHStarted 	= FALSE
						bCutSceneSWITCHPreped 	= FALSE
						iAssetRequests				= 0

						RETURN TRUE
					ENDIF
				BREAK
				CASE ST_6_REACT_TO_SECOND
				FALLTHRU
				CASE ST_8_REACT_TO_THIRD
					IF NOT bRunPostSwitchFocusCleanup
						IF IS_CUTSCENE_PLAYING()
						
							IF NOT bPlayedSwitchSting
								ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								bPlayedSwitchSting = TRUE
							ENDIF
						
							PARTY_CleanupEntitiesAndAssets()
							TORTURE_GrabAllPedsFromCutscene()
							TORTURE_GrabAllPropsFromCutscene()
							SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
							bDisplayHeartMonitor = TRUE
							
							IF NOT bPerformedPostTortureFadeIn
								SAFE_FADE_IN()
								bPerformedPostTortureFadeIn = TRUE
							ENDIF
						ENDIF
						
						TORTURE_ResetAllProps(TRUE)
						
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							CPRINTLN(DEBUG_MIKE, "************ CUTSCENE EXIT CAMERA ***********")
							bCamExited = TRUE
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
						
							CPRINTLN(DEBUG_MIKE, "************ CUTSCENE EXIT TREVOR ***********")
							
							FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
								SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE)
								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
								TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
							ENDIF
							FREEZE_ENTITY_POSITION(TREV_PED_ID(), FALSE)
							
							bHasPlayedPickupAnim = FALSE

							TORTURE_ManageWeaponSelection()
							
							TORTURE_InitWeapSelectionCam()
							TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
							
							IF eMissionStage = ST_6_REACT_TO_SECOND
								START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_3")
							ELIF eMissionStage = ST_8_REACT_TO_THIRD
								START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_4")
							ENDIF
							
							SAFE_FADE_IN()

							bTrevorExited 				= TRUE
							bRunPostSwitchFocusCleanup 	= TRUE
						ENDIF
					
					ELSE
//						SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE) - Moved due to url:bugstar:2073069
						SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						Unload_Asset_NewLoadScene(sAssetData)
						
						bRunPostSwitchFocusCleanup 	= FALSE
						bMadeSelection				= FALSE
						bCutsceneSWITCHStarted 		= FALSE
						bCutSceneSWITCHPreped 		= FALSE
						iAssetRequests				= 0

						RETURN TRUE
					ENDIF
					
				BREAK
				CASE ST_10_REACT_FINAL
					IF NOT bRunPostSwitchFocusCleanup
					
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

						// Manage moving the focus and start streaming the outside for after the cutscene
						IF NOT bMovedFocus
							SET_FOCUS_ENTITY(TREV_PED_ID())
							CPRINTLN(DEBUG_MIKE, "SWITCH_Sniping_To_Torture_CUTSCENE()	Setting Trevor as focus entity")
							Unload_Asset_NewLoadScene(sAssetData)
							//Load_Asset_NewLoadScene_Frustum(sAssetData, <<120.5339, -2202.8474, 8.2390>>, NORMALISE_VECTOR(<<127.7537, -2195.3679, 6.9974>>-<<120.5339, -2202.8474, 8.2390>>), 200.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
							Load_Asset_NewLoadScene_Sphere(sAssetData, <<130.6803, -2193.3494, 5.0325>>, 500.0) //, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
							
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DESTROY_ALL_CAMS()
							bMovedFocus = TRUE
						ENDIF
						
						// Cutscene was skipped, kill the load scene and wait for the cutscene to exit
						IF WAS_CUTSCENE_SKIPPED()
							IF IS_SCREEN_FADED_OUT()
								CPRINTLN(DEBUG_MIKE, "SWITCH_Sniping_To_Torture_CUTSCENE()	Skipping the cutscene")
								UNPIN_INTERIOR(interior_torture)
								Unload_Asset_NewLoadScene(sAssetData)	
							ENDIF
						ENDIF
					
						IF IS_CUTSCENE_PLAYING()
						
							IF NOT bPlayedSwitchSting
								ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								bPlayedSwitchSting = TRUE
							ENDIF
							
							IF GET_CUTSCENE_TIME() < 38750

								bDisplayHeartMonitor = TRUE
								
								// flat line ecg
								IF GET_CUTSCENE_TIME() > 15540
									bFlatLine = TRUE
								ENDIF
								
							// turn off ecg
							ELSE
								
								bDisplayHeartMonitor = FALSE
							
								bFlatLine = FALSE
								STOP_SOUND(sndList[SND_HEART_FLATLINE_LOOP].SoundId)
								ReleaseSound(SND_HEART_FLATLINE_LOOP)
								
							ENDIF
							
							IF NOT bPerformedPostTortureFadeIn
								SAFE_FADE_IN()
								bPerformedPostTortureFadeIn = TRUE
							ENDIF
						
							PARTY_CleanupEntitiesAndAssets()
							TORTURE_GrabAllPedsFromCutscene(IG_MRK)
							IF NOT DOES_ENTITY_EXIST(vehTrevor)
								ENTITY_INDEX vehTemp 
								vehTemp = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(csTrevorCar, GET_PLAYER_VEH_MODEL(CHAR_TREVOR))
								IF DOES_ENTITY_EXIST(vehTemp)
									vehTrevor = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(vehTemp)
								ENDIF
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
							CPRINTLN(DEBUG_MIKE, "************ CUTSCENE EXIT CAMERA ***********")
							bCamExited = TRUE
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(strMrKCSHandle)
							IF NOT IS_PED_IN_VEHICLE(pedVictim, vehTrevor)
								SET_PED_INTO_VEHICLE(pedVictim, vehTrevor, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
					
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
							
							CPRINTLN(DEBUG_MIKE, "************ CUTSCENE EXIT TREVOR ***********")
						
							FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
								SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE)
								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
								TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
							ENDIF
							FREEZE_ENTITY_POSITION(TREV_PED_ID(), FALSE)
						
							IF NOT IS_PED_IN_VEHICLE(TREV_PED_ID(), vehTrevor)
								SET_PED_INTO_VEHICLE(TREV_PED_ID(), vehTrevor)
							ENDIF
							
							SET_ENTITY_COORDS(vehTrevor, vTrevorCarDriveToAirportCoord)
							SET_ENTITY_HEADING(vehTrevor, fTrevorCarSpawnHeading)
							
							SET_VEHICLE_DOOR_SHUT(vehTrevor, SC_DOOR_FRONT_LEFT, 	TRUE)
							SET_VEHICLE_DOOR_SHUT(vehTrevor, SC_DOOR_FRONT_RIGHT, 	TRUE)
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
							SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
							DISPLAY_RADAR(TRUE)
							
							TORTURE_ResetAllProps(FALSE)
							
							DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])

							bTrevorExited = TRUE
							bRunPostSwitchFocusCleanup 	= TRUE
							bFlatLine 					= FALSE
							IF IS_SCREEN_FADED_OUT()
								CPRINTLN(DEBUG_MIKE, "SWITCH_Sniping_To_Torture_CUTSCENE()	Trevor exited on a fade, make sure the interior is unpinned now and the load scene has stopped")
								UNPIN_INTERIOR(interior_torture)
								Unload_Asset_NewLoadScene(sAssetData)	
							ENDIF
						ENDIF
					
					ELSE
						IF IS_SCREEN_FADED_OUT()
							IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								Load_Asset_NewLoadScene_Sphere(sAssetData, <<130.6803, -2193.3494, 5.0325>>, 100.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
							ELSE
								IF IS_NEW_LOAD_SCENE_LOADED()
								AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(TREV_PED_ID())
								AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedVictim)
									SAFE_FADE_IN()
								ENDIF
							ENDIF
						ELSE
							CLEAR_FOCUS()
//							SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), FALSE) - Moved due to url:bugstar:2073069
							SET_GAME_PAUSES_FOR_STREAMING(TRUE)
							Unload_Asset_NewLoadScene(sAssetData)
							
							bMadeSelection				= FALSE
							bCutsceneSWITCHStarted 		= FALSE
							bCutSceneSWITCHPreped 		= FALSE
							bRunPostSwitchFocusCleanup 	= FALSE
							iAssetRequests				= 0
							bFlatLine 					= FALSE

							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC




//═════════════════════════════════╡ DEBUG PROCEDURES ╞═══════════════════════════════════
#IF IS_DEBUG_BUILD

//PURPOSE:		Creates debug widgets
PROC Debug_CreateWidgets() //Called on mission setup	
	widgetDebug = START_WIDGET_GROUP("FBI3")
		ADD_WIDGET_FLOAT_READ_ONLY("Gameplay Cam Heading(relative)",	debug_f_cam_rel_heading)
		ADD_WIDGET_FLOAT_READ_ONLY("Gameplay Cam Pitch(relative)",		debug_f_cam_rel_pitch)
		ADD_WIDGET_FLOAT_READ_ONLY("Weapon Zoom level", 				debug_f_scope_zoom)
		ADD_WIDGET_FLOAT_READ_ONLY("Scope speed x", 					debug_f_scope_speed_x)
		ADD_WIDGET_FLOAT_READ_ONLY("Scope speed z", 					debug_f_scope_speed_z)
		ADD_WIDGET_BOOL("Simulate Streaming Stuck", b_SimulatedStreamingStuck)
		ADD_WIDGET_VECTOR_SLIDER("Scope cam rot", 						debug_v_scope_cam_rot, -99999, 99999, 0.1)
#IF NOT IS_JAPANESE_BUILD
		ADD_WIDGET_INT_READ_ONLY("TortureWeaponStage", 					iTortureWeaponStage)
#ENDIF
		START_WIDGET_GROUP("Mr K Variations")
			ADD_WIDGET_BOOL("CROTCH BLOODY", bMrKVariations[MR_K_CROTCH_BLOODY])
			ADD_WIDGET_BOOL("FACE BLOODY", bMrKVariations[MR_K_FACE_BLOODY])
			ADD_WIDGET_BOOL("LEG BLOODY", bMrKVariations[MR_K_LEG_BLOODY])
			ADD_WIDGET_BOOL("NIPPLE BURNED", bMrKVariations[MR_K_NIPPLES_BURNED])
			ADD_WIDGET_BOOL("RAG ON FACE", bMrKVariations[MR_K_RAG_FACE])
			ADD_WIDGET_BOOL("TORSO BLOODY", bMrKVariations[MR_K_TORSO_BLOODY])
			ADD_WIDGET_BOOL("WET", bMrKVariations[MR_K_WET])
			ADD_WIDGET_INT_READ_ONLY("TEETH PULLED", iNumberOfTeethPulled)
		STOP_WIDGET_GROUP()
#IF NOT IS_JAPANESE_BUILD
		START_WIDGET_GROUP("Electrocute scripted task debug")
			ADD_WIDGET_FLOAT_READ_ONLY("Input LT", fClipGripProgLeft)
			ADD_WIDGET_FLOAT_READ_ONLY("Input RT", fClipGripProgRight)
			ADD_WIDGET_FLOAT_READ_ONLY("Input Balance", fLRBalance)
			ADD_WIDGET_FLOAT_READ_ONLY("Input Balance(Normalised)", fLRBalanceNorm)
			ADD_WIDGET_FLOAT_READ_ONLY("AnimWeight Idle", fIdleWeight)
			ADD_WIDGET_FLOAT_READ_ONLY("AnimWeight Attach Left", fLeftWeight)
			ADD_WIDGET_FLOAT_READ_ONLY("AnimWeight Attach Right", fRightWeight)
			ADD_WIDGET_FLOAT_READ_ONLY("AnimWeight Attach Both", fBothWeight)
			ADD_WIDGET_FLOAT_READ_ONLY("AnimWeight Shock", fShockLoopTransition)
		STOP_WIDGET_GROUP()
#ENDIF
		
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widgetDebug)
ENDPROC

//PURPOSE: Updates any debug script each frame
PROC Debug_Update()
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		eMissionStage = ST_13_PASSED_MISSION
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		eMissionFail = FAIL_GENERIC
		bMissionFailed = TRUE
	ENDIF
				
	SET_TEXT_RENDER_ID(rtScreen)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		debug_f_cam_rel_heading		= GET_GAMEPLAY_CAM_RELATIVE_HEADING()
		debug_f_cam_rel_pitch		= GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF

	// Toggle debug info
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
		b_draw_debug_info = !b_draw_debug_info
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(b_draw_debug_info)
	ENDIF

	IF b_draw_debug_info
		CONST_FLOAT f_left 			0.05
		CONST_FLOAT f_top 			0.25
		CONST_FLOAT f_spacing		0.025
			
		TEXT_LABEL_63 str_debug_info
		
		DRAW_DEBUG_TEXT_WRAPPED_INT( "Mission Stage", enum_to_int( eMissionStage ) )
		
		str_debug_info = "Mission Substage: "
		IF bStageSetup = FALSE
			str_debug_info += "SETUP"
		ELSE
			str_debug_info += i_mission_substage
		ENDIF
		DRAW_DEBUG_TEXT_WRAPPED( str_debug_info )
		
		DRAW_DEBUG_TEXT_WRAPPED_FLOAT( "Victim Health", fVictimHealth )
		
		DRAW_DEBUG_TEXT_WRAPPED_FLOAT( "Torture Force", fTortureForceApplied )
		
	#IF NOT IS_JAPANESE_BUILD
		
		DRAW_DEBUG_TEXT_WRAPPED_INT( "Torture Stage", iTortureStage )
		 
		DRAW_DEBUG_TEXT_WRAPPED_INT( "Torture Weapon Stage", iTortureWeaponStage )
		
		str_debug_info = "Selected Weapon: "
		SWITCH INT_TO_ENUM(TORTURE_WEAPONS, iCurrentMenuWeapon)
			CASE TOR_CLIP0					str_debug_info += "TOR_CLIP0"				BREAK
			CASE TOR_CLIP1					str_debug_info += "TOR_CLIP1"				BREAK
			CASE TOR_PLIERS					str_debug_info += "TOR_PLIERS"				BREAK
			CASE TOR_RAG					str_debug_info += "TOR_RAG"					BREAK
			CASE TOR_SYRINGE				str_debug_info += "TOR_SYRINGE"				BREAK
			CASE TOR_WATER					str_debug_info += "TOR_WATER"				BREAK
			CASE TOR_WRENCH					str_debug_info += "TOR_WRENCH"				BREAK
		ENDSWITCH
		DRAW_DEBUG_TEXT_WRAPPED( str_debug_info )

		DRAW_DEBUG_TEXT_WRAPPED_BOOL( "bMadeSelection", bMadeSelection )
		
		DRAW_DEBUG_TEXT_WRAPPED_INT( "iNumberOfTeethPulled", iNumberOfTeethPulled )
		
		DRAW_DEBUG_TEXT_WRAPPED_INT( "iNumberOfSyringesUsed", iNumberOfSyringesUsed )
		
		DRAW_DEBUG_TEXT_WRAPPED_INT( "iAttemptToAimTimer", iAttemptToAimTimer - GET_GAME_TIMER() )
		
		DRAW_DEBUG_TEXT_WRAPPED_INT( "iDriveComplaintTimer", iDriveComplaintTimer )
		
		str_debug_info = "iDriveComplaintTimer(count down): "
		IF iDriveComplaintTimer - GET_GAME_TIMER() >= 0
			str_debug_info += iDriveComplaintTimer - GET_GAME_TIMER()
		ELSE
			str_debug_info += 0
		ENDIF
		DRAW_DEBUG_TEXT_WRAPPED( str_debug_info )

		DRAW_DEBUG_TEXT_WRAPPED_BOOL( "bDisplayHeartMonitor", bDisplayHeartMonitor )
		
	#ENDIF

	ENDIF
	
	iDebugCounter = 0 // reset each frame
	
ENDPROC

#ENDIF

PROC GET_SKIP_STAGE_COORD_AND_HEADING(INT iStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH int_to_enum(MISSION_STAGE, iStage)
		CASE ST_0_DRIVE_TO_START				vCoord = g_startSnapshot.mPlayerStruct.vPos 		fHeading = g_startSnapshot.mPlayerStruct.fHeading			BREAK
		CASE ST_1_INTRO_CUT						vCoord = <<126.6574, -2198.7236, 5.0325>>			fHeading = 196.4746											BREAK
		CASE ST_2_DRIVE_TO_COORD_FIRST			vCoord = vDaveCarOutsideTortureCoords				fHeading = fDaveCarOutsideTortureHeading					BREAK
		CASE ST_3_TORTURE_FOR_FIRST				vCoord = vTrevorTortureCoords						fHeading = fTrevorTortureHeading							BREAK
		CASE ST_4_DRIVE_TO_COORD_SECOND			vCoord = <<-1270.1217, -883.5477, 10.9303>>			fHeading = 219.3017											BREAK
		CASE ST_5_TORTURE_FOR_SECOND			vCoord = vTrevorTortureCoords						fHeading = fTrevorTortureHeading							BREAK
		CASE ST_6_REACT_TO_SECOND				vCoord = vMichaelSnipeCoords						fHeading = fMichaelSnipeHeading								BREAK
		CASE ST_7_TORTURE_FOR_THIRD				vCoord = vTrevorTortureCoords						fHeading = fTrevorTortureHeading							BREAK
		CASE ST_8_REACT_TO_THIRD				vCoord = vMichaelSnipeCoords						fHeading = fMichaelSnipeHeading								BREAK
		CASE ST_9_TORTURE_FINAL					vCoord = vTrevorTortureCoords						fHeading = fTrevorTortureHeading							BREAK
		CASE ST_10_REACT_FINAL					vCoord = vMichaelSnipeCoords						fHeading = fMichaelSnipeHeading								BREAK
		CASE ST_11_DRIVE_TO_AIRPORT				vCoord = vTrevorCarDriveToAirportCoord				fHeading = fTrevorCarSpawnHeading							BREAK
		CASE ST_13_PASSED_MISSION				vCoord = <<-1034.6471, -2729.8953, 19.0672>>		fHeading = 240.9291											BREAK
	ENDSWITCH

ENDPROC

//PURPOSE: Setsup the mission
PROC MissionSetup()

	SET_MISSION_FLAG(TRUE)
	
// Handle replay
//----------------------------------------------------------------------

	iTargetPedSlot[0] = enum_to_int(PEDSLOT_NOT_ASSIGNED)
	iTargetPedSlot[1] = enum_to_int(PEDSLOT_NOT_ASSIGNED)
	iTargetPedSlot[2] = enum_to_int(PEDSLOT_NOT_ASSIGNED)
	
	IF IS_REPLAY_IN_PROGRESS()
		b_SpawnTrain = FALSE
	ELSE
		b_SpawnTrain = TRUE
	ENDIF

	IF IS_REPLAY_IN_PROGRESS()	// RETRIES
	OR IS_REPEAT_PLAY_ACTIVE()	// REPLAY VIA START MENU
	
		INT iCheckpointToSkipTo
	
		IF IS_REPLAY_IN_PROGRESS()
			
			iCheckpointToSkipTo = GET_REPLAY_MID_MISSION_STAGE()
			CPRINTLN(DEBUG_MIKE, "FBI3: Get_Replay_Mid_Mission_Stage() = ", iCheckpointToSkipTo)
			IF g_bShitskipAccepted
				iCheckpointToSkipTo += 1
				CPRINTLN(DEBUG_MIKE, "FBI3: g_bShitskipAccepted")
			ENDIF
			
		ELIF IS_REPEAT_PLAY_ACTIVE()
		
			iCheckpointToSkipTo = 0
		
		ENDIF
		
		CPRINTLN(DEBUG_MIKE, "FBI3: Checkpoint skipping to ", iCheckpointToSkipTo)
		
		// Translate checkpoint into mission stage
		SWITCH iCheckpointToSkipTo
			// Make sure to always skip the drive section for mission replays via the mission replay menu.
			CASE 0			
				IF IS_REPEAT_PLAY_ACTIVE()
					eMissionStage = ST_1_INTRO_CUT
				ELSE
					eMissionStage = ST_0_DRIVE_TO_START						
				ENDIF
			BREAK
			CASE 1						eMissionStage = ST_1_INTRO_CUT							BREAK
			CASE 2						eMissionStage = ST_2_DRIVE_TO_COORD_FIRST				BREAK
			CASE 3						
				eMissionStage = ST_3_TORTURE_FOR_FIRST
				// if has shit skipped here reset the replay vehicle, we don't know what the player took here
				// and it hasn't been through the restriction logic 
				IF g_bShitskipAccepted
					Reset_Vehicle_Snapshot(g_stageSnapshot.mVehicleStruct)
				ENDIF
			BREAK
			CASE 4						eMissionStage = ST_4_DRIVE_TO_COORD_SECOND				BREAK
			CASE 5						eMissionStage = ST_5_TORTURE_FOR_SECOND					BREAK
			CASE 6						eMissionStage = ST_6_REACT_TO_SECOND					BREAK
			CASE 7						eMissionStage = ST_7_TORTURE_FOR_THIRD					BREAK
			CASE 8						eMissionStage = ST_8_REACT_TO_THIRD						BREAK
			CASE 9						eMissionStage = ST_9_TORTURE_FINAL						BREAK
			CASE 10						eMissionStage = ST_10_REACT_FINAL						BREAK
			CASE 11						eMissionStage = ST_11_DRIVE_TO_AIRPORT					BREAK
			CASE 12						eMissionStage = ST_12_EXIT_CUT							BREAK
			CASE 13						eMissionStage = ST_13_PASSED_MISSION					BREAK
		ENDSWITCH
		
		IF NOT g_bShitskipAccepted
			IF eMissionStage = ST_6_REACT_TO_SECOND
			OR eMissionStage = ST_8_REACT_TO_THIRD
			OR eMissionStage = ST_10_REACT_FINAL
				bDontPlayFullSnipeChat = TRUE
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_MIKE, "FBI3: stage skipping to ", enum_to_int(eMissionStage))
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(enum_to_int(eMissionStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(DEBUG_MIKE, "FBI3: Using replay warp system - Warp Coord ", vSkipToCoord, " Warp Heading ", fSkipToHeading)
		ENDIF
		
		bDoSkip = TRUE
	ENDIF
	
	//SET_INTERIOR_CAPPED(INTERIOR_V_TORTURE, FALSE)
	
	SET_PED_MODEL_IS_SUPPRESSED(mnTarget, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR), TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_NPC_VEH_MODEL(CHAR_DAVE), TRUE)
	
	//INFORM_MISSION_STATS_OF_MISSION_START_FBI_THREE()
	REGISTER_SCRIPT_WITH_AUDIO()
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB3_BEACH_HOUSE, BUILDINGSTATE_DESTROYED)
	
	Load_Asset_Additional_Text(sAssetData, sTextBlock, MISSION_TEXT_SLOT)
	Load_Asset_Additional_Text(sAssetData, sAudioBlock, MISSION_DIALOGUE_TEXT_SLOT)
	
	#IF IS_DEBUG_BUILD
		Debug_CreateWidgets()
	#ENDIF
	
	
	// Get the character started as from the replay snapshot
	IF g_startSnapshot.eCharacter = CHAR_MICHAEL
		bStartedAsMichael = TRUE
		sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_MICHAEL
	ELSE
		bStartedAsMichael = FALSE
		sSelectorPeds.eCurrentSelectorPed = SELECTOR_PED_TREVOR
	ENDIF
	
	ADD_RELATIONSHIP_GROUP("CREW", grpCrew)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, grpCrew)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, grpCrew, RELGROUPHASH_PLAYER)

	vHouseSectionCentre[HS_HIGH_LEFT] 		= << -3091.7322, 332.170875, 18.6771 >>		//<< -3091.7322, 337.2985, 16.7714 >>
	vHouseSectionCentre[HS_HIGH_RIGHT] 		= << -3091.7322, 362.594475, 18.6771 >>		//<< -3091.7322, 357.5809, 16.7714 >>
	vHouseSectionCentre[HS_LOW_LEFT] 		= << -3091.7322, 332.170875, 7.500 >>		//<< -3091.7322, 337.2985, 9.4100 >>
	vHouseSectionCentre[HS_LOW_RIGHT] 		= << -3091.7322, 362.594475, 7.500 >>		//<< -3091.7322, 357.5809, 9.4100 >>
	vHouseSectionCentre[HS_CENTRE] 			= << -3091.7322, 347.55375, 12.9600 >>		//<< -3091.7322, 347.55375, 12.9600 >>
	
	vTargetPedSlots[PEDSLOT_UPPER_LEFT] 	= <<-3092.78, 340.39, 14.48>>
	vTargetPedSlots[PEDSLOT_UPPER_CENTER] 	= <<-3091.56, 344.96, 14.48>>
	vTargetPedSlots[PEDSLOT_UPPER_RIGHT] 	= <<-3091.20, 354.61, 14.47>>
	
	vTargetPedSlots[PEDSLOT_LOWER_LEFT] 	= <<-3092.49, 342.06, 10.82>>
	vTargetPedSlots[PEDSLOT_LOWER_CENTER] 	= <<-3092.49, 349.96, 10.82>>
	vTargetPedSlots[PEDSLOT_LOWER_RIGHT] 	= <<-3091.08, 355.04, 10.84>>
	
	rtScreen = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
	
	#IF IS_DEBUG_BUILD
		SkipMenuStruct[0].sTxtLabel 		= "[0] 0. Drive to the start"
		SkipMenuStruct[1].sTxtLabel			= "[1] 1. FBI_3_INT"
		SkipMenuStruct[2].sTxtLabel 		= "[2] 2. Michael Drives 1st"
		SkipMenuStruct[3].sTxtLabel 		= "[3] 3. Trevor Tortures 1st"
	SkipMenuStruct[4].sTxtLabel 		= "[4] MCS_1 - Info Chumash"
	SkipMenuStruct[4].bSelectable		= FALSE
		SkipMenuStruct[5].sTxtLabel 		= "[5] 4. Michael Drives 2nd"
	SkipMenuStruct[6].sTxtLabel 		= "[6] MCS_2 - Arrive at Chumash"
	SkipMenuStruct[6].bSelectable		= FALSE
		SkipMenuStruct[7].sTxtLabel 		= "[7] 5. Trevor Tortures 2nd"
	SkipMenuStruct[8].sTxtLabel 		= "[8] MCS_3 - Info Build/Height/Age"
	SkipMenuStruct[8].bSelectable		= FALSE
		SkipMenuStruct[9].sTxtLabel 		= "[9] 6. Michael Reacts to 2nd"
	SkipMenuStruct[10].sTxtLabel 		= "[10] MCS_4p2 - Cutback"
	SkipMenuStruct[10].bSelectable		= FALSE
		SkipMenuStruct[11].sTxtLabel 		= "[11] 7. Trevor Tortures 3rd"
	SkipMenuStruct[12].sTxtLabel 		= "[12] MCS_5 - Info Beard"
	SkipMenuStruct[12].bSelectable		= FALSE
		SkipMenuStruct[13].sTxtLabel 		= "[13] 8. Michael Reacts to 3rd"
	SkipMenuStruct[14].sTxtLabel 		= "[14] MCS_5p2 - Cutback Smokes"
	SkipMenuStruct[14].bSelectable		= FALSE
		SkipMenuStruct[15].sTxtLabel		= "[15] 9. Trevor Tortures Final"
	SkipMenuStruct[16].sTxtLabel 		= "[16] MCS_6p2 - Info Smoker"
	SkipMenuStruct[16].bSelectable		= FALSE
		SkipMenuStruct[17].sTxtLabel 		= "[17] 10. Michael Kills Target"
	SkipMenuStruct[18].sTxtLabel 		= "[18] MCS_7 - Get MrK into Truck"
	SkipMenuStruct[18].bSelectable		= FALSE
		SkipMenuStruct[19].sTxtLabel 		= "[19] 11. Trevor Drives MrK to Airport"
		SkipMenuStruct[20].sTxtLabel 		= "[20] 12.	MCS_8 - Drop off MrK"
		SkipMenuStruct[21].sTxtLabel 		= "[21] 13. Mission Passed at Airport"
	#ENDIF
	
	// Sort out the roads and paths
	SET_ROADS_IN_AREA(<< 77.0999, -2243.1301, -2.8697 >>, << 306.7302, -2159.3760, 35.3531 >>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 77.0999, -2243.1301, -2.8697 >>,<< 306.7302, -2159.3760, 35.3531 >>,FALSE)
	
	ADD_SCENARIO_BLOCKING_AREA(
		<<-3100.807129,357.957367,14.520833>> - <<2.937500,2.750000,1.312500>>, 
		<<-3100.807129,357.957367,14.520833>> + <<2.937500,2.750000,1.312500>>)

	ADD_SCENARIO_BLOCKING_AREA(
		<<-3091.965088,345.613770,10.923056>> - <<1.500000,1.875000,1.312500>>,
		<<-3091.965088,345.613770,10.923056>> + <<1.500000,1.875000,1.312500>>)
	
	// Dock vehicles coming in
	ADD_SCENARIO_BLOCKING_AREA(
		<<225.981720,-2079.282471,16.715635>> + <<1.000000,1.000000,1.000000>>,
		<<225.981720,-2079.282471,16.715635>> - <<1.000000,1.000000,1.000000>>)
		
	// Dock vehicles going out
	ADD_SCENARIO_BLOCKING_AREA(
		<<248.975784,-2184.234619,8.348366>> + <<1.000000,1.000000,1.000000>>,
		<<248.975784,-2184.234619,8.348366>> - <<1.000000,1.000000,1.000000>>)
			
	CLEAR_AREA(<<-3103.392090,349.964447,8.943123>>, 17.625000, TRUE) 
	
	ADD_SCENARIO_BLOCKING_AREA(
		<<153.994003,-2189.285156,13.849371>>-<<29.875000,18.875000,10.187500>>,
		<<153.994003,-2189.285156,13.849371>>+<<29.875000,18.875000,10.187500>>)
		
	CLEAR_AREA(<<153.994003,-2189.285156,13.849371>>, 30.0, TRUE)
	
	INT iCounter
	FOR iCounter = 0 TO ENUM_TO_INT(NUMBER_OF_SCENES)-1
		syncScene[iCounter] = -1
	ENDFOR

	// Grab the target/dummy target positions from the replay info
	IF Is_Replay_In_Progress()
		
		// retrieve the target and decoy slots from the packed bit field
		iTargetPedSlot[0] 	= g_replay.iReplayInt[0] & BITMASK_TARGET_SLOT_0
		iTargetPedSlot[1] 	= SHIFT_RIGHT(g_replay.iReplayInt[0], BITSHIFTER_TARGET_SLOT) & BITMASK_TARGET_SLOT_0
		iTargetPedSlot[2] 	= SHIFT_RIGHT(g_replay.iReplayInt[0], 2*BITSHIFTER_TARGET_SLOT) & BITMASK_TARGET_SLOT_0
		
		// Retrieve the victims health
		fVictimHealth 		= TO_FLOAT(g_replay.iReplayInt[1] & BITMASK_VICTIMHEALTH)
		
		// Retrieve the victims variations
		INT iVarBitField 	= SHIFT_RIGHT(g_replay.iReplayInt[1] & BITMASK_VICTIMVARS, BITSHIFTER_VICTIMHEALTH)
		INT i
		REPEAT COUNT_OF(bMrKVariations) i
			IF IS_BIT_SET(iVarBitField, i)
				bMrKVariations[i] = TRUE
			ELSE
				bMrKVariations[i] = FALSE
			ENDIF
		ENDREPEAT
		
		// Retrieve the number of teeth pulled
		iNumberOfTeethPulled 	= SHIFT_RIGHT(g_replay.iReplayInt[1] & BITMASK_VICTIMTEETH, BITSHIFTER_VICTIMHEALTH + BITSHIFTER_VICTIMVARS) 
		
		#IF NOT IS_JAPANESE_BUILD
		// Retrieve the numver of syringes used
		iNumberOfSyringesUsed 	= SHIFT_RIGHT(g_replay.iReplayInt[1] & BITMASK_SYRINGES, BITSHIFTER_VICTIMHEALTH + BITSHIFTER_VICTIMVARS + BITSHIFTER_VICTIMTEETH)
		#ENDIF

	// not doing a replay so randomly pick the positions for the target/dummy targets
	ELSE	
	
		iTargetPedSlot[0] 		= -1
		iTargetPedSlot[1] 		= -1
		iTargetPedSlot[2] 		= -1
		
		// Store the target position
		WHILE iTargetPedSlot[0] = -1
		OR iTargetPedSlot[0] 	= enum_to_int(PEDSLOT_NUM_OF_SLOTS)
			iTargetPedSlot[0] 	= GET_RANDOM_INT_IN_RANGE(0, enum_to_int(PEDSLOT_NUM_OF_SLOTS))
		ENDWHILE
		g_replay.iReplayInt[0] -= g_replay.iReplayInt[0] & BITMASK_TARGET_SLOT_0			// Wipe any stored data in this slot
		g_replay.iReplayInt[0] |= iTargetPedSlot[0]											// store data into bitfield
		
		WHILE iTargetPedSlot[1] = -1
		OR iTargetPedSlot[1] 	= iTargetPedSlot[0]
		OR iTargetPedSlot[1] 	= enum_to_int(PEDSLOT_NUM_OF_SLOTS)
			iTargetPedSlot[1] 	= GET_RANDOM_INT_IN_RANGE(0, enum_to_int(PEDSLOT_NUM_OF_SLOTS))
		ENDWHILE
		g_replay.iReplayInt[0] -= g_replay.iReplayInt[0] & BITMASK_TARGET_SLOT_1			// Wipe any stored data in this slot
		g_replay.iReplayInt[0] |= SHIFT_LEFT(iTargetPedSlot[1], BITSHIFTER_TARGET_SLOT)		// store data into bitfield
		
		WHILE iTargetPedSlot[2] = -1
		OR iTargetPedSlot[2] 	= iTargetPedSlot[0]
		OR iTargetPedSlot[2] 	= iTargetPedSlot[1]
		OR iTargetPedSlot[2] 	= enum_to_int(PEDSLOT_NUM_OF_SLOTS)
			iTargetPedSlot[2] 	= GET_RANDOM_INT_IN_RANGE(0, enum_to_int(PEDSLOT_NUM_OF_SLOTS))
		ENDWHILE
		g_replay.iReplayInt[0] -= g_replay.iReplayInt[0] & BITMASK_TARGET_SLOT_2			// Wipe any stored data in this slot
		g_replay.iReplayInt[0] |= SHIFT_LEFT(iTargetPedSlot[2], 2*BITSHIFTER_TARGET_SLOT)	// store data into bitfield
		
		TORTURE_Store_Victim_State()
		
	ENDIF
	
	interior_torture = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 143.3550, -2202.2996, 6.1902 >>, "v_torture")
	IF IS_VALID_INTERIOR(interior_torture)
		PIN_INTERIOR_IN_MEMORY(interior_torture)
	ELSE
		SCRIPT_ASSERT("INTERIOR NOT FOUND! V_TORTURE")
	ENDIF
	
	bMrKVariations[MR_K_CABLES] = TRUE
	
	Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
ENDPROC

//PURPOSE:	Clears up the mission booleans. Leave bResetHotswap true unless occuring in Cardiac arrest
PROC MissionStageTidyUp()	
	INT iCounter
	FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
		bEventFlag[iCounter] = FALSE
	ENDFOR
	
	FOR iCounter = 0 TO 2
		bDriveConversationFlags[iCounter] = FALSE
	ENDFOR
	
	str_InteruptedRoot								= ""
	str_InteruptedLabel								= ""

	bPromptedSWITCH 								= FALSE
	bMadeSelection									= FALSE
	bHasPlayedPickupAnim							= FALSE
	fTortureForceApplied 							= 0
	iSelectWeaponStage 								= 0
	fSmokeEvo										= 0
	
#IF NOT IS_JAPANESE_BUILD
	bJustStarting									= TRUE
	bHasBeenSyringed								= FALSE
	bMenuRightPressed								= FALSE
	bMenuLeftPressed								= FALSE
	iTortureStage 									= 0
	
	IF bFXElecSmoulder
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxElecSmoulder)
			STOP_PARTICLE_FX_LOOPED(ptfxElecSmoulder)
		ENDIF
		bFXElecSmoulder = FALSE
	ENDIF	
#ENDIF
ENDPROC

//PURPOSE:	Cleans up the mission
PROC MissionCleanup(CLEANUP_TYPE_FLAG cleanupType)

	INT i

	CPRINTLN(DEBUG_MIKE, "-------- FBI3: Start MissionCleanup()")
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)

	BOOL bImmediateCleanup
	INT iCounter

	SWITCH cleanupType
		CASE CTF_RESET
			bImmediateCleanup = TRUE
		BREAK
		CASE CTF_INSTANT
			bImmediateCleanup = TRUE
		BREAK
		CASE CTF_DEATH_ARREST_DELAYED
			bImmediateCleanup = FALSE
			SET_CLEANUP_TO_RUN_ON_RESPAWN(RCI_FBI3)
		BREAK
		CASE CTF_MISSION_PASSED
			bImmediateCleanup = FALSE
		BREAK
	ENDSWITCH
	
	IF cleanupType != CTF_RESET
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB3_BEACH_HOUSE, BUILDINGSTATE_NORMAL)
		//SET_INTERIOR_CAPPED(INTERIOR_V_TORTURE, TRUE)
	ENDIF
	
	IF IS_CUTSCENE_ACTIVE()
		REMOVE_CUTSCENE()
	ENDIF
	
	IF NOT IS_PED_INJURED( PLAYER_PED_ID() )
	AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF GET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_LEGS) = GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 25, 0, COMP_TYPE_LEGS)
			SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_LEGS,	 	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 0, 0, COMP_TYPE_LEGS), FALSE)
		ENDIF
		IF GET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_SPECIAL) = GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 12, 0, COMP_TYPE_SPECIAL)
			SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_SPECIAL,	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 0, 0, COMP_TYPE_SPECIAL), FALSE)
		ENDIF
		IF GET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_FEET) = GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 11, 0, COMP_TYPE_FEET)
			SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_FEET,		GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 0, 0, COMP_TYPE_FEET), FALSE)
		ENDIF
	ENDIF
	
	PARTY_ResetScopeCheck()
	Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
		
	// Cleanups that dont happen when not an immediate cleanup
	IF bImmediateCleanup
		
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR
	
		IF IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
			RELEASE_NAMED_RENDERTARGET(sECGRenderTarget)
		ENDIF
		
		IF bPlayingMusic
			STOP_AUDIO_SCENE(audsceneZoom) 
			STOP_STREAM()
			bPlayingMusic = FALSE
		ENDIF
		
		SET_FRONTEND_RADIO_ACTIVE(TRUE)
		
		CPRINTLN(DEBUG_MIKE, "-------- FBI3: Immediate entity cleanup")
		PARTY_CleanupEntitiesAndAssets()
		TORTURE_CleanupEntitiesAndAssets()
		
		REMOVE_DECALS_IN_RANGE(<<-3095.93, 344.02, 13.50>>, 34.375)
		
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	
	FOR iCounter = 0 TO ENUM_TO_INT(NUMBER_OF_SCENES)-1
		syncScene[iCounter] = -1
	ENDFOR

	IF bHasChairPlayedAnim
		IF DOES_ENTITY_EXIST(objChair)
			STOP_SYNCHRONIZED_ENTITY_ANIM(objChair, INSTANT_BLEND_OUT, FALSE)
		ENDIF
	ENDIF
	MissionStageTidyUp()
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
//═════════╡ TORTURE ╞═════════
	FOR iCounter = 0 TO ENUM_TO_INT(NO_OF_TORTURE_WEAPONS)-1
		TORTURE_CleanupWeapon(menuWeapon[iCounter], TRUE)
	ENDFOR
	
//═════════╡ BLIPS ╞═════════		
	IF DOES_BLIP_EXIST(blipLocate)
		REMOVE_BLIP(blipLocate)
	ENDIF

	CPRINTLN(DEBUG_MIKE, "-------- FBI3: Object tidyUp") 
//═════════╡ OBJECTS ╞═════════
	TidyUpObject(objTable, 			bImmediateCleanup)
	TidyUpObject(objSmallTable, 	bImmediateCleanup)
	TidyUpObject(objBattery, 		bImmediateCleanup)
	TidyUpObject(objChair, 			bImmediateCleanup)
	TidyUpObject(objMonitor, 		bImmediateCleanup)
	TidyUpObject(objTooth, 			bImmediateCleanup)
	TidyUpObject(propPhoneDave, 	bImmediateCleanup)
	TidyUpObject(propPhoneSteve, 	bImmediateCleanup)
	TidyUpObject(objTapeArmLeft, 	bImmediateCleanup)
	TidyUpObject(objTapeArmRight, 	bImmediateCleanup)
	
	REPEAT COUNT_OF(objCunts) i
		TidyUpObject(objCunts[i], bImmediateCleanup)
	ENDREPEAT
	
	MISSION_OBJECT_FLAGS eObj
	REPEAT COUNT_OF(objs) eObj
		TidyUpObject(objs[eObj], bImmediateCleanup)
	ENDREPEAT

//═════════╡ VEHICLES ╞═════════)
	TidyUpVehicle(vehTrevor, 			bImmediateCleanup)
	TidyUpVehicle(vehDriveToVeh, 		bImmediateCleanup)
	TidyUpVehicle(vehReplay, 			bImmediateCleanup)
	IF bImmediateCleanup
		DELETE_ALL_TRAINS()
	ELSE
		SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(vehTrainStart)
	ENDIF
		
//═════════╡ PEDS ╞═════════

	TidyUpPed(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 	bImmediateCleanup)
	TidyUpPed(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 	bImmediateCleanup)
	TidyUpPed(pedVictim, 									bImmediateCleanup)
	TidyUpPed(pedSteve, 									bImmediateCleanup)
	TidyUpPed(pedDave, 										bImmediateCleanup)
	
	TidyUpPed(pedProneMichael,								bImmediateCleanup)
	
	REPEAT COUNT_OF(pedCunts) i
		TidyUpPed(pedCunts[i], 								bImmediateCleanup)	
	ENDREPEAT
	
	MISSION_PED_FLAGS ePedCounter
	REPEAT COUNT_OF(peds) ePedCounter
		TidyUpPed(peds[ePedCounter].id, bImmediateCleanup)
		peds[ePedCounter].bFleeing 				= FALSE
	ENDREPEAT
	
	iPartyBStage			= 0
	iPartyDStage			= 0

	//Activate player control
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
			IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	DISABLE_CELLPHONE(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	str_dialogue_root = ""
	bTriggeringConversation = FALSE

	IF IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
		RELEASE_NAMED_RENDERTARGET(sECGRenderTarget)
	ENDIF

//═════════╡ SEQUENCES ╞═════════
	CLEAR_SEQUENCE_TASK(seqSequence)
	
//═════════╡ CAMS ╞═════════
		
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DISPLAY_RADAR(TRUE)
	
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	IF IS_PLAYER_PLAYING(PLAYER_ID())	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

	//═════════╡ GROUPS ╞═════════
	IF cleanupType != CTF_RESET
		REMOVE_RELATIONSHIP_GROUP(grpCrew)
	ENDIF
	
	IF NOT IS_PLAYER_DEAD(PLAYER_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
	ENDIF

	// Flags
	bHasChairPlayedAnim 	= FALSE
	#IF NOT IS_JAPANESE_BUILD
	bHasRestraintPlayedAnim	= FALSE
	#ENDIF
	ePartyState				= PCS_NOT_CRASHED
	
	IF cleanupType != CTF_RESET
		UNREGISTER_SCRIPT_WITH_AUDIO()
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
				DELETE_WIDGET_GROUP(widgetDebug)
			ENDIF
		#ENDIF
		
		SET_PED_MODEL_IS_SUPPRESSED(mnTarget, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR), FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_NPC_VEH_MODEL(CHAR_DAVE), FALSE)
	ENDIF
	
	clearText(TRUE, TRUE, TRUE)
	
	SET_TIME_SCALE(1.0)
	SET_RANDOM_TRAINS(TRUE)
	
	//Stopping a switch when cleaning up for a switch to MP will break
	//the MP switch. #1618486
	IF eMissionFail = FAIL_FORCE_CLEANUP
	AND GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() != FORCE_CLEANUP_FLAG_SP_TO_MP
		STOP_PLAYER_SWITCH()
	ENDIF
	
	bTortureMusicPlaying = FALSE
	IF NOT IS_STRING_NULL_OR_EMPTY(strOneShotLoaded)
		CANCEL_MUSIC_EVENT(strOneShotLoaded)
		strOneShotLoaded = ""
	ENDIF
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	ENDIF
	
	STOP_AUDIO_SCENES()
	
	IF bPlayerLockedIn
		CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
		bPlayerLockedIn 	= FALSE
	ENDIF
	
	CASCADE_SHADOWS_INIT_SESSION()
	
	IF bStoredSniperSnapshot
		IF DOES_ENTITY_EXIST(MIKE_PED_ID())
			STORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
		ENDIF
		g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21] = sSniperSnapshot
		IF DOES_ENTITY_EXIST(MIKE_PED_ID())
			RESTORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
		ENDIF
		bStoredSniperSnapshot = FALSE
	ENDIF
	
	// cleanup anything that has already marked for cleanup
	Update_Entity_Cleanup_Queue(sEntityCleanupQueue, TRUE)
	
	g_sSelectorUI.bFX_SkipFadeOut = FALSE
	
	iAttemptToAimTimer = -1
	
	SHUTDOWN_PC_CONTROLS()
	
	SET_MULTIHEAD_SAFE(FALSE)
	
ENDPROC

//PURPOSE: Sets up the mission based on the stage the player is skipping to
PROC MissionStageSkip()

	IF bDoSkip
		PRINTLN("PROCESSING SKIP")
		IF IS_SCREEN_FADED_OUT()
	
			PRINTLN("DOING SKIP CLEANUP")
			MissionCleanup(CTF_RESET)
			clearText()
			
			Unload_Asset_NewLoadScene(sAssetData)
			
		// Warping
		// -----------------------------------------------
		
			Start_Skip_Streaming(sAssetData)
		
			IF NOT IS_REPLAY_BEING_SET_UP()
			
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Performing NON-REPLAY skip warping")
				
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(enum_to_int(eMissionStage), vWarpCoord, fWarpHeading)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
				
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() NON-REPLAY skip warp coord ", vWarpCoord, " warp heading ", fWarpHeading)
				
			ENDIF
			
		// Asset loading
		// -----------------------------------------------
			Load_Asset_Additional_Text(sAssetData, sTextBlock, MISSION_TEXT_SLOT)
			Load_Asset_Additional_Text(sAssetData, sAudioBlock, MISSION_DIALOGUE_TEXT_SLOT)
			
			SWITCH eMissionStage
				CASE ST_0_DRIVE_TO_START			
					// Started as michael
					IF bStartedAsMichael
						SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
					// Started as Trevor
					ELSE
						SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					ENDIF
				BREAK
				CASE ST_1_INTRO_CUT
				
					IF bStartedAsMichael
						SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
						Load_Asset_Model(sAssetData, mnTrevTurd)
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_3_INT", cutSections_TrevFull)
					ELSE
						SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
						Load_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_3_INT", cutSections_Mike)
					ENDIF

					Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(TRUE, FALSE, strMrKCSHandle)
					
				BREAK
				CASE ST_2_DRIVE_TO_COORD_FIRST
				
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
				
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						//Load_Asset_Model(sAssetData, GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
					ENDIF
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorTortureCoords, fTrevorTortureHeading)
						wait(0)
					ENDWHILE
					
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())

				BREAK 
				CASE ST_3_TORTURE_FOR_FIRST
				
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vCoffeeShopMikeCoord, fCoffeeShopMikeHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
				
					TORTURE_LoadAssets(TRUE, TRUE)
				BREAK
				CASE ST_4_DRIVE_TO_COORD_SECOND
				
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorTortureCoords, fTrevorTortureHeading)
						wait(0)
					ENDWHILE
					PRINTLN("DRIVE TO SECOND CREATED TREVOR")
					
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
				
					WHILE NOT PARTY_LoadAssets(TRUE, FALSE, FALSE, TRUE)
						wait(0)
					ENDWHILE
				BREAK 
				CASE ST_5_TORTURE_FOR_SECOND
				
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelSnipeCoords, fMichaelSnipeHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
				
					TORTURE_LoadAssets(TRUE, TRUE)
				BREAK
				CASE ST_6_REACT_TO_SECOND

					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorTortureCoords, fTrevorTortureHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
					
					WHILE NOT PARTY_LoadAssets(FALSE, TRUE)
						wait(0)
					ENDWHILE
				BREAK
				CASE ST_7_TORTURE_FOR_THIRD
					
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelSnipeCoords, fMichaelSnipeHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
					
					TORTURE_LoadAssets(TRUE, TRUE)
				BREAK
				CASE ST_8_REACT_TO_THIRD
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorTortureCoords, fTrevorTortureHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
					
					WHILE NOT PARTY_LoadAssets(FALSE, TRUE)
						wait(0)
					ENDWHILE
				BREAK
				CASE ST_9_TORTURE_FINAL
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelSnipeCoords, fMichaelSnipeHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
					
					TORTURE_LoadAssets(TRUE, TRUE)	
				BREAK
				CASE ST_10_REACT_FINAL
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE, TRUE)
					
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorTortureCoords, fTrevorTortureHeading)
						wait(0)
					ENDWHILE
					FREEZE_ENTITY_POSITION(TREV_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(TREV_PED_ID(), TRUE)
					
					SET_PED_CREW(MIKE_PED_ID())
					SET_PED_CREW(TREV_PED_ID())
					
					WHILE NOT PARTY_LoadAssets(FALSE, TRUE)
						wait(0)
					ENDWHILE
				BREAK
				CASE ST_11_DRIVE_TO_AIRPORT
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					SET_PED_CREW(TREV_PED_ID())
					Load_Asset_Model(sAssetData, IG_MRK)
				BREAK
				CASE ST_12_EXIT_CUT
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					SET_PED_CREW(TREV_PED_ID())
					
					IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
						SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS")  
					ENDIF
					REQUEST_CUTSCENE("fbi_3_mcs_8")
					Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(TRUE, FALSE, strMrKCSHandle, TRUE)
					
				BREAK
				CASE ST_13_PASSED_MISSION
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR, TRUE, TRUE)
					SET_PED_CREW(TREV_PED_ID())
				BREAK
			ENDSWITCH
			
			WHILE NOT Update_Skip_Streaming(sAssetData)
				PRINTLN("UPDATING SKIP STREAMING")
				wait(0)
			ENDWHILE
			
		//>>>>>>>>>>>>>> [FINISHED] SKIP STREAMING & PLAYER WARPING
			
			SWITCH eMissionStage
				CASE ST_0_DRIVE_TO_START
					PED_VEH_DATA_STRUCT sVehData
					VECTOR vClearCoord
					IF bStartedAsMichael
						CREATE_VEHICLE_FOR_PHONECALL_TRIGGER_REPLAY(vehReplay, FALSE, FALSE, FALSE, TRUE)
						IF IS_THIS_MODEL_A_PLANE( GET_ENTITY_MODEL( vehReplay ) )
							SET_VEHICLE_ENGINE_ON( vehReplay, TRUE, TRUE )
							SET_VEHICLE_FORWARD_SPEED( vehReplay, 30.0 )
						ENDIF
						IF IS_REPLAY_BEING_SET_UP()
							END_REPLAY_SETUP(vehReplay)
						ENDIF
						vClearCoord = GET_ENTITY_COORDS(vehReplay)
					ELSE
						GET_PLAYER_VEH_DATA(CHAR_TREVOR, sVehData)
						CREATE_VEHICLE_FOR_PHONECALL_TRIGGER_REPLAY(vehTrevor, FALSE, FALSE, FALSE, TRUE, sVehData.model, sVehData.iColourCombo, CHAR_TREVOR)
						IF IS_THIS_MODEL_A_PLANE( GET_ENTITY_MODEL( vehTrevor ) )
							SET_VEHICLE_ENGINE_ON( vehTrevor, TRUE, TRUE )
							SET_VEHICLE_FORWARD_SPEED( vehTrevor, 30.0 )
						ENDIF
						IF IS_REPLAY_BEING_SET_UP()
							END_REPLAY_SETUP(vehTrevor)
						ENDIF
						vClearCoord = GET_ENTITY_COORDS(vehTrevor)
					ENDIF
					CLEAR_AREA_OF_VEHICLES(vClearCoord, 20.0)
					
				BREAK
				CASE ST_1_INTRO_CUT
					
					WHILE NOT CREATE_NPC_VEHICLE(vehDriveToVeh, CHAR_DAVE, vDaveCarOutsideTortureCoords, fDaveCarOutsideTortureHeading, TRUE)
						wait(0)
					ENDWHILE
					
					IF bStartedAsMichael
						
						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, << 129.173, -2203.357, 6.017 >>)
							wait(0)
						ENDWHILE

						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TREV_PED_ID(), TRUE)
						
						#IF NOT IS_JAPANESE_BUILD
						
	//						SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_LEG, 		25, 0)
	//						SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_SPECIAL, 	12, 0)
							SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_LEGS,	 	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 25, 0, COMP_TYPE_LEGS))
							SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_SPECIAL,	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 12, 0, COMP_TYPE_SPECIAL))
							SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_FEET,		GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 11, 0, COMP_TYPE_FEET))
						
						#ENDIF
						
						objs[mof_turd] = CREATE_OBJECT(mnTrevTurd, vTrevTurd)
						Unload_Asset_Model(sAssetData, mnTrevTurd)
						
						WHILE NOT CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, vTrevorCarOutsideTortureCoord, fTrevorCarOutsideTortureHeading)
							wait(0)
						ENDWHILE
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor, FALSE)
					ELSE

						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelSpawnCoords, fMichaelSpawnHeading)
							wait(0)
						ENDWHILE
								
						SET_ENTITY_LOAD_COLLISION_FLAG(MIKE_PED_ID(), TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), TRUE)	
								
						objs[mof_mike_ciggy] = CREATE_OBJECT(PROP_CS_CIGGY_01B, GET_PED_BONE_COORDS(MIKE_PED_ID(), BONETAG_PH_R_HAND, <<0,0,0>>))
						ATTACH_ENTITY_TO_ENTITY(objs[mof_mike_ciggy], MIKE_PED_ID(), GET_PED_BONE_INDEX(MIKE_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
						Unload_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				BREAK
				CASE ST_2_DRIVE_TO_COORD_FIRST
				
					DISABLE_TAXI_HAILING(TRUE)
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						vehReplay = CREATE_REPLAY_CHECKPOINT_VEHICLE(vOtherVehicleOutsideWarehouse, fOtherVehicleOutsideWarehouse)
					ENDIF
					
					WHILE NOT CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, vTrevorCarOutsideTortureCoord, fTrevorCarOutsideTortureHeading)
						wait(0)
					ENDWHILE
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor, FALSE)
					
					WHILE NOT CREATE_NPC_VEHICLE(vehDriveToVeh, CHAR_DAVE, vDaveCarOutsideTortureCoords, fDaveCarOutsideTortureHeading, TRUE)
						wait(0)
					ENDWHILE
					SET_VEHICLE_ON_GROUND_PROPERLY(vehDriveToVeh)
					SET_VEHICLE_ENGINE_ON(vehDriveToVeh, TRUE, TRUE)
				
					WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedDave, CHAR_DAVE, vehDriveToVeh, VS_FRONT_RIGHT)
						WAIT(0)
					ENDWHILE
					PED_VARIATION_STRUCT sDave 
					sDave = GET_DAVE_VARIATIONS(TRUE)
					SET_PED_VARIATIONS(pedDave, sDave)
					SET_PED_CREW(pedDave)

					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SET_CREATE_RANDOM_COPS(TRUE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehDriveToVeh)
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehDriveToVeh)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				BREAK 
				CASE ST_3_TORTURE_FOR_FIRST
				
					DISABLE_TAXI_HAILING(TRUE)
					
					WHILE NOT TORTURE_IsTortureReady(TRUE, TRUE, FALSE)
						WAIT(0)
					ENDWHILE

					START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_1")
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
					
					doTwoPointCam(camGeneralInit, camGeneralDest,	<< 146.7890, -2204.1140, 5.6386 >>, << -10.8642, 7.4269, 61.6078 >>, 17.4610,
									<< 146.7890, -2204.1140, 5.6386 >>, << -10.8642, 7.4269, 61.6078 >>, 16.0803,
									5000)
					doCamShake(camGeneralInit, camGeneralDest, shakeHand, 0.1)
					bHasSwitchSetupCam = TRUE
					wait(0)
					
					TRIGGER_MUSIC_EVENT("FBI3_TORTURE_RT")
					bTortureMusicPlaying = TRUE
				BREAK
				CASE ST_4_DRIVE_TO_COORD_SECOND
				
					DISABLE_TAXI_HAILING(TRUE)
				
					WHILE NOT PARTY_IsSceneReady(TRUE, FALSE, ST_INIT, FALSE, TRUE)
						WAIT(0)
					ENDWHILE
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
										
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SET_CREATE_RANDOM_COPS(TRUE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehDriveToVeh)
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehDriveToVeh, VS_DRIVER)
					ENDIF
					
					TRIGGER_MUSIC_EVENT("FBI3_MICHAEL_MUSIC_1")
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				BREAK 
				CASE ST_5_TORTURE_FOR_SECOND
				
					DISABLE_TAXI_HAILING(TRUE)
				
					WHILE NOT TORTURE_IsTortureReady(TRUE, TRUE, FALSE)
						WAIT(0)
					ENDWHILE
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
					START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_2")
					TRIGGER_MUSIC_EVENT("FBI3_TORTURE_RT")
					bTortureMusicPlaying = TRUE
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
				BREAK
				CASE ST_6_REACT_TO_SECOND
				
					DISABLE_TAXI_HAILING(TRUE)

					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ELSE
						//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)	// DO NOT UNFREEZE FOR MICHAEL AS HE IS FLOATING IN MID AIR FOR A REASON HERE
					ENDIF
					
					WHILE NOT PARTY_IsSceneReady(FALSE, TRUE, ST_6_REACT_TO_SECOND)
						wait(0)
					ENDWHILE
					
					setupSwayCam(sSwaySniping, vProneCamStartCoord, vProneCamStartRot, fProneCamStartFOV,
						vProneCamEndCoord, vProneCamEndRot, fProneCamEndFOV,
						40000, TRUE, shakeHand, 0.5, "SNIPE")
					PARTY_ManagePartyScene()
					PARTY_ManageProneAndScope()
				BREAK
				CASE ST_7_TORTURE_FOR_THIRD
				
					DISABLE_TAXI_HAILING(TRUE)
				
					WHILE NOT TORTURE_IsTortureReady(TRUE, TRUE, FALSE)
						WAIT(0)
					ENDWHILE
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
					START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_3")
					TRIGGER_MUSIC_EVENT("FBI3_TORTURE_RT")
					bTortureMusicPlaying = TRUE
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
				BREAK
				CASE ST_8_REACT_TO_THIRD
				
					DISABLE_TAXI_HAILING(TRUE)
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ELSE
						//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					WHILE NOT PARTY_IsSceneReady(FALSE, TRUE, ST_8_REACT_TO_THIRD)
						wait(0)
					ENDWHILE
					
					setupSwayCam(sSwaySniping, vProneCamStartCoord, vProneCamStartRot, fProneCamStartFOV,
						vProneCamEndCoord, vProneCamEndRot, fProneCamEndFOV,
						40000, TRUE, shakeHand, 0.5, "SNIPE")
					PARTY_ManagePartyScene()
					PARTY_ManageProneAndScope()
				BREAK
				CASE ST_9_TORTURE_FINAL
				
					DISABLE_TAXI_HAILING(TRUE)
				
					WHILE NOT TORTURE_IsTortureReady(TRUE, TRUE, FALSE)
						WAIT(0)
					ENDWHILE
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					TORTURE_PlaySyncScene(SCENE_WEAPON_SELECT_IDLE, TRUE)
					START_AUDIO_SCENE("FBI_3_TREVOR_TORTURE_4")
					TRIGGER_MUSIC_EVENT("FBI3_TORTURE_RT")
					bTortureMusicPlaying = TRUE
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				BREAK
				CASE ST_10_REACT_FINAL
				
					DISABLE_TAXI_HAILING(TRUE)
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ELSE
						//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					WHILE NOT PARTY_IsSceneReady(FALSE, TRUE, ST_10_REACT_FINAL)
						wait(0)
					ENDWHILE
					
					setupSwayCam(sSwaySniping, vProneCamStartCoord, vProneCamStartRot, fProneCamStartFOV,
						vProneCamEndCoord, vProneCamEndRot, fProneCamEndFOV,
						40000, TRUE, shakeHand, 0.5, "SNIPE")
					PARTY_ManagePartyScene()
					PARTY_ManageProneAndScope()
				BREAK
				CASE ST_11_DRIVE_TO_AIRPORT		
				
					DISABLE_TAXI_HAILING(TRUE)
				
					WHILE NOT CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, vTrevorCarDriveToAirportCoord, fTrevorCarSpawnHeading)
						Wait(0)
					ENDWHILE
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					IF NOT DOES_ENTITY_EXIST(pedVictim)
						pedVictim = CREATE_PED_INSIDE_VEHICLE(vehTrevor, PEDTYPE_MISSION, IG_MRK, VS_FRONT_RIGHT)
						RESET_MR_K_VARIATIONS()
						bMrKVariations[MR_K_CABLES] = FALSE
						MANAGE_MR_K_VARIATIONS()
					ENDIF
					SET_PED_CREW(pedVictim)
					
					SET_PED_INTO_VEHICLE(TREV_PED_ID(), vehTrevor, VS_DRIVER)
					SET_VEHICLE_ENGINE_ON(vehTrevor, TRUE, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevor)

					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SET_CREATE_RANDOM_COPS(TRUE)
					
					WHILE NOT IS_PED_INJURED(TREV_PED_ID()) AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(TREV_PED_ID())
					AND NOT IS_PED_INJURED(pedVictim) AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedVictim)
						wait(0)
					ENDWHILE
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehTrevor)
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTrevor)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				CASE ST_12_EXIT_CUT
				
					SET_PED_NON_CREATION_AREA(
						<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
						<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>)
						
					SET_PED_PATHS_IN_AREA(
						<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
						<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>, FALSE)
						
					sba_airport = ADD_SCENARIO_BLOCKING_AREA(<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
						<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>)
	
					IF iRoadNodeSpeedZone[0] != -1
						REMOVE_ROAD_NODE_SPEED_ZONE(iRoadNodeSpeedZone[0])
					ENDIF
					iRoadNodeSpeedZone[0] = ADD_ROAD_NODE_SPEED_ZONE(<<-1057.38, -2715.40, 19.12>>, 4.375, 0.0)
					CLEAR_AREA_OF_VEHICLES(vDriveToAirport, 100)
					
					IF bStoredSniperSnapshot
						IF DOES_ENTITY_EXIST(MIKE_PED_ID())
							STORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
						ENDIF
						g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21] = sSniperSnapshot
						IF DOES_ENTITY_EXIST(MIKE_PED_ID())
							RESTORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
						ENDIF
						bStoredSniperSnapshot = FALSE
					ENDIF
				
					DISABLE_TAXI_HAILING(TRUE)

					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					WHILE NOT CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, <<-1034.6471, -2729.8953, 19.0672>>, 240.9291)
						Wait(0)
					ENDWHILE
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehTrevor)
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTrevor)
					ENDIF
					
				BREAK
				CASE ST_13_PASSED_MISSION
				
					DISABLE_TAXI_HAILING(TRUE)
				
					CLEAR_AREA_OF_VEHICLES(<<-1032.140869,-2730.226318,19.116234>>, 15.0, FALSE, FALSE, TRUE, TRUE)
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					WHILE NOT CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, <<-1034.6471, -2729.8953, 19.0672>>, 240.9291)
						Wait(0)
					ENDWHILE
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehTrevor)
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTrevor)
					ENDIF
					
				BREAK
			ENDSWITCH
			
			Unload_Asset_NewLoadScene(sAssetData)
			
			ADD_ALL_PEDS_TO_DIALOGUE()
			
			IF eMissionStage != ST_1_INTRO_CUT
			AND eMissionStage != ST_12_EXIT_CUT
				SAFE_FADE_IN()
			ENDIF
			bDoSkip = FALSE
		ENDIF
	
#IF IS_DEBUG_BUILD
	// Check is a skip is being asked for, and dont allow skip during setup stage
	// Stage skip not directly linked to number of stages
	ELIF IS_SCREEN_FADED_IN() 
	AND NOT IS_CUTSCENE_PLAYING()
	
		INT iCurrentStage
		INT iReturnStage
				
		IF TRANSLATE_TO_Z_MENU(SkipMenuStruct, enum_to_int(eMissionStage), iCurrentStage)
	
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, iCurrentStage, FALSE, "FBI 3", FALSE, TRUE,
				KEY_Z, KEYBOARD_MODIFIER_NONE, TRUE, TRUE)
			
				IF iReturnStage < iCurrentStage
					fVictimHealth = 100 // reset victim health if jumping back for debug purposes
				ENDIF
			
				eMissionStage = int_to_enum(MISSION_STAGE, TRANSLATE_FROM_Z_MENU(SkipMenuStruct, iReturnStage)	)
				
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				
				IF IS_CUTSCENE_PLAYING()
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
				ENDIF
				
				bStageSetup = FALSE
				bDoSkip = TRUE
			ENDIF
			
		ENDIF
#ENDIF	
	ENDIF
ENDPROC

//PURPOSE:	Performs necessary actions for player completing the mission, then calls cleanup
PROC MissionPassed()
	
	#IF IS_JAPANESE_BUILD
	// pass all these stats at the end of the mission for the japanese build
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_TOOTH)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_WRENCH)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_WATERBOARD)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FBI3_ELECTROCUTION)
	#ENDIF

	clearText()
	MissionCleanup(CTF_MISSION_PASSED)
	Mission_Flow_Mission_Passed()
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE:	Performs necessary actions for player failing the mission, then calls cleanup
PROC MissionFailed()
	
	IF eMissionStage = ST_6_REACT_TO_SECOND
	OR eMissionStage = ST_8_REACT_TO_THIRD
	OR eMissionStage = ST_10_REACT_FINAL
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-2955.2388, 311.2846, 29.0388>>, 76.5327)
	ELIF eMissionStage = ST_3_TORTURE_FOR_FIRST
	OR eMissionStage = ST_5_TORTURE_FOR_SECOND
	OR eMissionStage = ST_7_TORTURE_FOR_THIRD
	OR eMissionStage = ST_9_TORTURE_FINAL
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<132.7334, -2197.0349, 5.1073>>, 292.9964)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<135.6781, -2191.3748, 5.0119>>, 270.0899)
	ENDIF

	IF eMissionFail = FAIL_FORCE_CLEANUP
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		MissionCleanup(CTF_DEATH_ARREST_DELAYED)
		TRIGGER_MUSIC_EVENT("FBI3_FAIL")
		TERMINATE_THIS_THREAD()	
	ELSE
		STRING strReason
		SWITCH eMissionFail
			CASE FAIL_GENERIC
				strReason = "F_GEN"
			BREAK
			CASE FAIL_MICHAEL_DEAD
				strReason = "F_MDEAD"
			BREAK
			CASE FAIL_TREVOR_DEAD
				strReason = "F_TDEAD"
			BREAK
			CASE FAIL_VICTIM_DEAD
				strReason = "F_KDEAD"
			BREAK
			CASE FAIL_DAVE_DEAD
				strReason = "F_DDEAD"
			BREAK
			CASE FAIL_STEVE_DEAD
				strReason = "F_SDEAD"
			BREAK
			
			CASE FAIL_VEHICLE_STUCK_TREVOR
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					strReason = "F_TRUCKSTUCK_M"
				ELSE
					strReason = "F_TRUCKSTUCK_T"
				ENDIF
			BREAK
			CASE FAIL_VEHICLE_DEAD_TREVOR
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					strReason = "F_TRUCKDEAD_M"
				ELSE
					strReason = "F_TRUCKDEAD_T"
				ENDIF
			BREAK
			
			CASE FAIL_CUNTS_SPOOKED
				strReason = "F_PHOTO_SPOOK"
			BREAK
			CASE FAIL_CUNTS_INNOCENT_DEAD
				strReason = "F_PHOTO_INN"
			BREAK
			
			CASE FAIL_PARTY_SEEN_AT_PARTY
				strReason = "F_PRTY_SEEN"
			BREAK
			CASE FAIL_PARTY_SPOOKED
				strReason = "F_PRTY_SPOOK"
			BREAK
			CASE FAIL_PARTY_PED_DEAD_INNOCENT_NO_ID
				strReason = "F_PRTY_INN_ID"
			BREAK
			CASE FAIL_PARTY_PED_DEAD_INNOCENT
				strReason = "F_PRTY_INN"
			BREAK
			
			CASE FAIL_ABANDONED_DAVE
				strReason = "F_ABAN_D"
			BREAK
			CASE FAIL_ABANDONED_MRK
				strReason = "F_ABAN_MRK"
			BREAK
			
			CASE FAIL_COPS_WAREHOUSE
				strReason = "F_COPS"
			BREAK
		ENDSWITCH
		
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)
		
		TRIGGER_MUSIC_EVENT("FBI3_FAIL")
		
		bUpdateMissionFailed = TRUE
	ENDIF
ENDPROC

PROC UpdateMissionFailed()



	IF NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
	
		CPRINTLN(DEBUG_MIKE,"***** NOT SAFE TO CLEAN UP")
	
		IF iFailTimer != -1
		
			CPRINTLN(DEBUG_MIKE,"***** DEATH SCENE FAIL TIMER NOT NEGATIVE")
		
			CPRINTLN(DEBUG_MIKE,"***** DEATH SCENE FAIL LOOKING TO START FADE, ", GET_CUTSCENE_TIME() - iFailTimer)
			IF IS_CUTSCENE_PLAYING()
			AND GET_CUTSCENE_TIME() - iFailTimer > 2500
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					CPRINTLN(DEBUG_MIKE,"***** DEATH SCENE FAIL START FADE")
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_LONG)
				ENDIF
			ENDIF
		ENDIF
	
	ELSE
	
		CPRINTLN(DEBUG_MIKE,"***** SAFE TO CLEAN UP - TERMINATE STRAIGHT AWAY")
	
		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
		MissionCleanup(CTF_INSTANT) 
		TERMINATE_THIS_THREAD()	
	ENDIF
ENDPROC

//═════════════════════════════════╡ Stage FUNCTIONS AND PROCEDURES ╞═══════════════════════════════════

//PURPOSE:		Performs the section where Michael or Trevor drives to the mission start
PROC stage0_DriveToStart()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	

		DISABLE_CELLPHONE(FALSE)

		// if started as michael need the trevor shitting anim
		IF bStartedAsMichael
			Load_Asset_AnimDict(sAssetData, "missfbi3ig_0")
			Load_Asset_Audiobank(sAssetData, "FBI_03_TREVOR_SHITTING")
			Load_Asset_Model(sAssetData, mnTrevTurd)
		ELSE
			Load_Asset_AnimDict(sAssetData, "missfbi3leadinout")
			Load_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
		ENDIF

		CLEAR_AREA(<<125.25, -2189.42, 4.98>>, 35.0, TRUE)
		bPlayerLockedIn 	= FALSE
		bSwitchedAtStart	= FALSE
		bStageSetup 		= TRUE
		i_mission_substage 	= 0

//═════════╡ UPDATE ╞═════════
	ENDIF
	
	IF bStageSetup
		
		SWITCH i_mission_substage
			CASE 0
				IF NOT DOES_ENTITY_EXIST(vehDriveToVeh)
					IF CREATE_NPC_VEHICLE(vehDriveToVeh, CHAR_DAVE, vDaveCarOutsideTortureCoords, fDaveCarOutsideTortureHeading, TRUE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDriveToVeh, FALSE)
					ENDIF
				ENDIF
				
				IF bStartedAsMichael
					IF NOT DOES_ENTITY_EXIST(TREV_PED_ID())
						IF HAS_ANIM_DICT_LOADED("missfbi3ig_0")
						AND HAS_MODEL_LOADED(mnTrevTurd)
							IF CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, << 129.173, -2203.357, 6.017 >>)
								// kick off shitting anim
								SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED_ID(), TRUE)
								TASK_PLAY_ANIM_ADVANCED(TREV_PED_ID(), "missfbi3ig_0", "shit_loop_trev", << 129.173, -2203.357, 6.017 >>, <<0,0,79.5>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TREV_PED_ID(), TRUE)
								SET_PED_CONFIG_FLAG(TREV_PED_ID(), PCF_DisableExplosionReactions, TRUE)
								
								SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
#IF NOT IS_JAPANESE_BUILD
//								SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_LEG, 		25, 0)
//								SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_SPECIAL, 	12, 0)
//								SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_FEET, 		11, 0)
								SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_LEGS,	 	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 25, 0, COMP_TYPE_LEGS), FALSE)
								SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_SPECIAL,	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 12, 0, COMP_TYPE_SPECIAL), FALSE)
								SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_FEET,		GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 11, 0, COMP_TYPE_FEET), FALSE)
#ENDIF								
								
								objs[mof_turd] = CREATE_OBJECT(mnTrevTurd, vTrevTurd)
								Unload_Asset_Model(sAssetData, mnTrevTurd)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(vehTrevor)
						IF CREATE_PLAYER_VEHICLE(vehTrevor, CHAR_TREVOR, vTrevorCarOutsideTortureCoord, fTrevorCarOutsideTortureHeading)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor, FALSE)
							SET_VEHICLE_DISABLE_TOWING(vehTrevor, TRUE)
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_ENTITY_EXIST(MIKE_PED_ID())
						IF HAS_ANIM_DICT_LOADED("missfbi3leadinout")
						AND HAS_MODEL_LOADED(PROP_CS_CIGGY_01B)
						
							IF CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelSpawnCoords, fMichaelSpawnHeading)
								SET_ENTITY_LOAD_COLLISION_FLAG(MIKE_PED_ID(), TRUE)
								
								OPEN_SEQUENCE_TASK(seqSequence)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missfbi3leadinout", "idle_a", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missfbi3leadinout", "idle_b", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missfbi3leadinout", "idle_c", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missfbi3leadinout", "idle_b", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missfbi3leadinout", "idle_a", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM_ADVANCED(NULL, "missfbi3leadinout", "idle_c", vMichaelSpawnCoords, <<0,0,fMichaelSpawnHeading>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									SET_SEQUENCE_TO_REPEAT(seqSequence, REPEAT_FOREVER)
								CLOSE_SEQUENCE_TASK(seqSequence)
								TASK_PERFORM_SEQUENCE(MIKE_PED_ID(), seqSequence)
								CLEAR_SEQUENCE_TASK(seqSequence)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), TRUE)	
							
								objs[mof_mike_ciggy] = CREATE_OBJECT(PROP_CS_CIGGY_01B, GET_PED_BONE_COORDS(MIKE_PED_ID(), BONETAG_PH_R_HAND, <<0,0,0>>))
								ATTACH_ENTITY_TO_ENTITY(objs[mof_mike_ciggy], MIKE_PED_ID(), GET_PED_BONE_INDEX(MIKE_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
								Unload_Asset_Model(sAssetData, PROP_CS_CIGGY_01B)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				// Check everything has been created
				IF DOES_ENTITY_EXIST(MIKE_PED_ID())
				AND DOES_ENTITY_EXIST(TREV_PED_ID())
				AND DOES_ENTITY_EXIST(vehDriveToVeh)
					IF NOT bStartedAsMichael OR DOES_ENTITY_EXIST(vehTrevor)
						SET_PED_CREW(MIKE_PED_ID())
						SET_PED_CREW(TREV_PED_ID())
					
						i_mission_substage++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF SWITCH_Free_Roam_To_Torture()
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					bSwitchedAtStart = TRUE
					i_mission_substage++
				ELSE
					// Switch has been started
					IF IS_SELECTOR_CAM_ACTIVE()
					
						IF bMadeSelection
							bDisplaySwitchHUD = FALSE
							IF IS_MESSAGE_BEING_DISPLAYED()
								clearText()
							ENDIF
						ENDIF
						
					// Player is traveling there
					ELSE
						
						IF NOT IS_CUTSCENE_ACTIVE()
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << 129.173, -2203.357, 6.017 >>) < DEFAULT_CUTSCENE_LOAD_DIST
								IF bStartedAsMichael
									REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_3_INT", cutSections_TrevLeadIn)
									Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, TREV_PED_ID(), csTrevor)
								ELSE
									REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("FBI_3_INT", cutSections_Mike)
								ENDIF
								Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(TRUE, FALSE, strMrKCSHandle)
								
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << 129.173, -2203.357, 6.017 >>) > DEFAULT_CUTSCENE_UNLOAD_DIST
							OR IS_PLAYER_CHANGING_CLOTHES()
								REMOVE_CUTSCENE()
							ENDIF
						ENDIF
					
						// Michael arriving to see trevor shitting behind a dumpster
						IF bStartedAsMichael
						
							IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData, << 129.173, -2203.357, 6.017 >>, <<0.1,0.1,0.1>>,
								FALSE, "TFT_GOTOW", TRUE, TRUE)

							IF DOES_ENTITY_EXIST(TREV_PED_ID()) AND NOT IS_PED_INJURED(TREV_PED_ID())
							AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), TREV_PED_ID()) <= 7.312500
							AND (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
							
								IF NOT bPlayerLockedIn
									BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
									bPlayerLockedIn = TRUE

									//Deal with the player being in a vehicle.
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										VEHICLE_INDEX vehPlayer
										vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										
										//Bring the vehicle out of critical state if it is in one.
										IF GET_ENTITY_HEALTH(vehPlayer) < 1
											SET_ENTITY_HEALTH(vehPlayer, 1)
										ENDIF
										IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
											SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
										ENDIF
										IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
											SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
										ENDIF
										
										//Stop the vehicle.
										BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 1.0, 2, 0.1)
										
										//Get the player out.
										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
									ENDIF
								ENDIF
							
								// Trigger the conversation and reaction
								IF IS_SAFE_TO_START_CONVERSATION()
									SAFE_ADD_PED_FOR_DIALOGUE(MIKE_PED_ID(), 0, "Michael")
									SAFE_ADD_PED_FOR_DIALOGUE(TREV_PED_ID(), 2, "Trevor")
									RunConversation("F3_MLDI")
									CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
									
									TASK_PLAY_ANIM_ADVANCED(TREV_PED_ID(), "missfbi3ig_0", "shit_react_trev", << 129.173, -2203.357, 6.017 >>, <<0,0,79.5>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME)
									TASK_LOOK_AT_ENTITY(MIKE_PED_ID(), TREV_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
									
									i_EstablishHoldTimer = -1
									i_mission_substage++
								ENDIF		
							ENDIF
							
							
						// Trevor arriving to see Michael smoking
						ELSE
							IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData, vMichaelSpawnCoords, <<0,0,0>>,
								FALSE, "TFT_GOTOW", TRUE, TRUE)
								
							IF DOES_ENTITY_EXIST(MIKE_PED_ID()) AND NOT IS_PED_INJURED(MIKE_PED_ID())
							AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MIKE_PED_ID()) <= 7.312500
							AND (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
							
								IF NOT bPlayerLockedIn
									//Deal with the player being in a vehicle.
									IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									
										VEHICLE_INDEX vehPlayer
										vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									
										IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5)
											
											//Bring the vehicle out of critical state if it is in one.
											IF GET_ENTITY_HEALTH(vehPlayer) < 1
												SET_ENTITY_HEALTH(vehPlayer, 1)
											ENDIF
											IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
												SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
											ENDIF
											IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
												SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
											ENDIF

											//Get the player out.
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
											
											BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
											bPlayerLockedIn = TRUE
										ENDIF
									ELSE
										BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
										bPlayerLockedIn = TRUE
									ENDIF
								ENDIF
							
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF IS_SAFE_TO_START_CONVERSATION()
										
										SAFE_ADD_PED_FOR_DIALOGUE(MIKE_PED_ID(), 0, "Michael")
										SAFE_ADD_PED_FOR_DIALOGUE(TREV_PED_ID(), 2, "Trevor")
										RunConversation("F3_TLDI")
										CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
										
										TASK_LOOK_AT_ENTITY(MIKE_PED_ID(), TREV_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
										TASK_LOOK_AT_ENTITY(TREV_PED_ID(), MIKE_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
									
										i_EstablishHoldTimer = -1
										i_mission_substage++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						AND (NOT DOES_BLIP_EXIST(g_CustomDropOffBlip) OR sLocatesData.LocationBlip != g_CustomDropOffBlip)
							SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(sLocatesData.LocationBlip, vTaxiToStartCoords, fTaxiToStartHeading)
						ENDIF
							
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF i_EstablishHoldTimer != -1

					IF GET_GAME_TIMER() > i_EstablishHoldTimer
						CPRINTLN(DEBUG_MIKE, "FBI3: Establish Complete")
						CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
						DISABLE_CELLPHONE(FALSE)
						eMissionStage = ST_1_INTRO_CUT
					ELSE
						DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON )
						IF NOT IS_CELLPHONE_DISABLED()
							DISABLE_CELLPHONE(TRUE)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF bStartedAsMichael
							CPRINTLN(DEBUG_MIKE, "FBI3: Trevor Leadin Complete")
							CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
							eMissionStage = ST_1_INTRO_CUT
						ELSE
							CPRINTLN(DEBUG_MIKE, "FBI3: Michael Leadin Complete")
							CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
							eMissionStage = ST_1_INTRO_CUT
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF bPlayerLockedIn
			UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
			IF bStartedAsMichael
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			ENDIF
		ENDIF
		
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_0_DRIVE_TO_START
		
			SETTIMERB(0)
			stopCamShake(camGeneralInit, camGeneralDest)
			MissionStageTidyUp()
			ADD_ALL_PEDS_TO_DIALOGUE()
			
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC stage1_IntroCut()

//═════════╡ SETUP ╞═════════
	
	IF NOT bStageSetup	

		DISABLE_CELLPHONE(FALSE)
		DISABLE_TAXI_HAILING(TRUE)
		
		bMichaelExited	= FALSE
		bDaveExited 	= FALSE
		bCamExited		= FALSE
		bTrousersUp 	= FALSE
		bStageSetup 	= TRUE
		i_mission_substage 	= 0

	ENDIF
	
//═════════╡ UPDATE ╞═════════

	IF bStageSetup
	
		SWITCH i_mission_substage
			CASE 0
				IF HAS_CUTSCENE_LOADED()
				AND NOT IS_ENTITY_ON_FIRE(MIKE_PED_ID())
				AND NOT IS_ENTITY_ON_FIRE(TREV_PED_ID())
				AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				OR IS_SCREEN_FADED_OUT()
				OR bSwitchedAtStart)
				
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "1. Intro cutscene", FALSE, FALSE, NULL, FALSE)
				
					Unload_Asset_Audio_Bank(sAssetData, "FBI_03_TREVOR_SHITTING")
					
					Load_Asset_Scaleform(sAssetData, sfHeart, sfHeartMonitor)
					
					IF PLAYER_PED_ID() = MIKE_PED_ID()
						IF DOES_ENTITY_EXIST(TREV_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(TREV_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(MIKE_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(null, csDave, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_DAVENORTON)
					
					IF DOES_ENTITY_EXIST(objs[mof_mike_ciggy])
						DETACH_ENTITY(objs[mof_mike_ciggy])
						REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_mike_ciggy], "CigLit_Michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF bPlayerLockedIn
						CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
						bPlayerLockedIn 	= FALSE
					ENDIF
					
					TORTURE_RegisterAllPropsForCutscene(FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
					
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					
					i_mission_substage++
				ENDIF
			BREAK
			CASE 1
				IF IS_CUTSCENE_PLAYING()
					SAFE_FADE_IN()
					
					DISPLAY_RADAR(TRUE)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					IF DOES_CAM_EXIST(cam_establish)
						DESTROY_CAM(cam_establish)
					ENDIF
					
					IF bStartedAsMichael
						Unload_Asset_Anim_Dict(sAssetData, "MISSFBI3IG_0")
					ELSE
						bTrousersUp = TRUE
						Unload_Asset_Anim_Dict(sAssetData, "missfbi3leadinout")
					ENDIF
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(
						<<115.668617,-2198.093262,5.032481>>, <<139.517273,-2197.381836,12.048586>>, 16.562500, 
						vOtherVehicleOutsideWarehouse, fOtherVehicleOutsideWarehouse)
					
					VEHICLE_INDEX vehTemp
					vehTemp = GET_PLAYERS_LAST_VEHICLE()
					IF DOES_ENTITY_EXIST(vehTemp)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)
						AND NOT IS_VEHICLE_RESTRICTED(vehTemp)
						AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehTemp))
						AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehTemp))
						AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
						
							IF IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(vehTemp, CHAR_TREVOR)
								IF NOT IS_ENTITY_A_MISSION_ENTITY(vehTemp)
									SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
								ENDIF
								vehTrevor = vehTemp
								SET_VEHICLE_ENGINE_ON(vehTrevor, FALSE, TRUE)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTrevor, FALSE)
							ELSE
								SET_MISSION_LAST_VEHICLE_AS_VEHICLE_GEN(vOtherVehicleOutsideWarehouse, fOtherVehicleOutsideWarehouse)
								SET_VEHICLE_ENGINE_ON(vehTemp, FALSE, TRUE)
							ENDIF
							
						ELSE
							IF NOT IS_ENTITY_A_MISSION_ENTITY(vehTemp)
								SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
							ENDIF
							DELETE_VEHICLE(vehTemp)
						ENDIF
					ENDIF

					IF DOES_ENTITY_EXIST(vehDriveToVeh)
						IF IS_VEHICLE_DRIVEABLE(vehDriveToVeh)
							SET_ENTITY_COORDS(vehDriveToVeh, vDaveCarOutsideTortureCoords)
							SET_ENTITY_HEADING(vehDriveToVeh, fDaveCarOutsideTortureHeading)
							SET_VEHICLE_ENGINE_ON(vehDriveToVeh, FALSE, TRUE)
						ELSE
							DELETE_VEHICLE(vehDriveToVeh)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehTrevor)
						IF IS_VEHICLE_DRIVEABLE(vehTrevor)
							SET_ENTITY_COORDS(vehTrevor, vTrevorCarOutsideTortureCoord)
							SET_ENTITY_HEADING(vehTrevor, fTrevorCarOutsideTortureHeading)
							SET_VEHICLE_ENGINE_ON(vehTrevor, FALSE, TRUE)
						ELSE
							DELETE_VEHICLE(vehTrevor)
						ENDIF
					ENDIF

					CLEAR_AREA_OF_PROJECTILES(vDaveCarOutsideTortureCoords, 100)
					CLEAR_AREA_OF_OBJECTS(vDaveCarOutsideTortureCoords, 100)
					i_mission_substage++
				ENDIF
			BREAK
			CASE 2
				IF IS_CUTSCENE_PLAYING()
					
					IF GET_CUTSCENE_CONCAT_SECTION_PLAYING() >= 5
						IF NOT bMusicCueTriggered
							TRIGGER_MUSIC_EVENT("FBI3_START")
							bMusicCueTriggered = TRUE
						ENDIF
					ENDIF	
					
					IF GET_CUTSCENE_TIME() >= 170000
						bDisplayHeartMonitor = TRUE
					ENDIF
					
					IF DOES_CUTSCENE_ENTITY_EXIST(csMonitor)
						IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sECGRenderTarget)
							REGISTER_NAMED_RENDERTARGET(sECGRenderTarget)
							
							IF NOT IS_NAMED_RENDERTARGET_LINKED(mnMonitor)
								LINK_NAMED_RENDERTARGET(mnMonitor)
							ENDIF
							
							rtECG = GET_NAMED_RENDERTARGET_RENDER_ID(sECGRenderTarget)
						ENDIF
						
						SET_TEXT_RENDER_ID(rtECG)
						DRAW_RECT_FROM_CORNER(0,0, 1.0, 1.0, 0, 0, 0, 255)
						SET_TEXT_RENDER_ID(rtScreen)
					ENDIF
					
				ENDIF
		
				// Grab dave from the cutscene
				IF NOT DOES_ENTITY_EXIST(pedDave)
					ENTITY_INDEX entity 
					entity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(csDave, IG_DAVENORTON)
					IF DOES_ENTITY_EXIST(entity)
						pedDave = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
						SET_PED_CREW(pedDave)
						CDEBUG1LN(DEBUG_MIKE, "******* GRABBED DAVE **********")
					ENDIF
				ENDIF
					
				IF NOT bTrousersUp 	
					IF GET_CUTSCENE_TIME() >= 11700
						IF IS_CUTSCENE_PLAYING()
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 		0, 0)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 	0, 0)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 		10, 0)
						ENDIF
						bTrousersUp = TRUE
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					bCamExited = TRUE
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csDave)				
				
					CDEBUG1LN(DEBUG_MIKE, "******* DAVE EXITED CUTSCENE **********")
					bDaveExited = TRUE
					
					IF DOES_ENTITY_EXIST(pedDave)
					AND NOT IS_PED_INJURED(pedDave)
						SET_ENTITY_COORDS(pedDave, <<133.0303, -2198.1868, 5.1692>>)
						SET_ENTITY_HEADING(pedDave, 41.7744)
						FORCE_PED_MOTION_STATE(pedDave, MS_ON_FOOT_WALK)
						CDEBUG1LN(DEBUG_MIKE, "******* DAVE WARPED AFTER CUT **********")
					ELSE
						CDEBUG1LN(DEBUG_MIKE, "******* DAVE DOESNT EXIST/IS DEAD **********")
					ENDIF
					
				ENDIF
			
				// Warp player upon exiting 
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					IF IS_REPLAY_RECORDING()
						REPLAY_STOP_EVENT()
					ENDIF
				
					CDEBUG1LN(DEBUG_MIKE, "******* MICHAEL EXITED CUTSCENE **********")
					bMichaelExited = TRUE
			
					// Take control of michael
					IF PLAYER_PED_ID() != MIKE_PED_ID()
						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
							SET_PED_CREW(MIKE_PED_ID())
						ENDIF
					ENDIF
					
					IF NOT bTrousersUp
//						SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_LEG, 		0, 0)
//						SET_PED_COMPONENT_VARIATION(TREV_PED_ID(), PED_COMP_SPECIAL, 	0, 0)
						SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_LEGS,	 	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 0, 0, COMP_TYPE_LEGS), FALSE)
						SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_SPECIAL,	GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 0, 0, COMP_TYPE_SPECIAL), FALSE)
						SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED_ID(), COMP_TYPE_FEET,		GET_PED_COMP_ITEM_FROM_VARIATIONS(TREV_PED_ID(), 0, 0, COMP_TYPE_FEET), FALSE)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehDriveToVeh)
						SET_VEHICLE_ENGINE_ON(vehDriveToVeh, FALSE, FALSE)
					ENDIF
					
					// bug 2006026
					// Cutting to the player walking down the steps, 
					// if the player is in first person cam view then do a 3rd person push
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
						IF DOES_CAM_EXIST( cam_establish )
							DESTROY_CAM( cam_establish )
						ENDIF
						cam_establish = CREATE_CAMERA( CAMTYPE_SCRIPTED, FALSE )
						ATTACH_CAM_TO_ENTITY(cam_establish, PLAYER_PED_ID(),  <<0.3996, -2.3506, 0.7873>>, FALSE)
						POINT_CAM_AT_ENTITY(cam_establish, PLAYER_PED_ID(), <<0.4001, 0.6177, 0.3519>>, FALSE)
						SET_CAM_ACTIVE( cam_establish, TRUE )
						RENDER_SCRIPT_CAMS( TRUE, FALSE )
					ENDIF
					
					SET_ENTITY_COORDS(MIKE_PED_ID(), <<133.1413, -2201.8418, 6.1864>>)
					SET_ENTITY_HEADING(MIKE_PED_ID(), 3.0596)
					FORCE_PED_MOTION_STATE(MIKE_PED_ID(), MS_ON_FOOT_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
					SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
					
					TRIGGER_MUSIC_EVENT("FBI3_BACK_TO_MICHAEL")
					SETTIMERB(0)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)

				ENDIF
				
				IF bMichaelExited 
				AND bDaveExited
				AND bCamExited
					IF DOES_ENTITY_EXIST(vehDriveToVeh)
					AND IS_VEHICLE_DRIVEABLE(vehDriveToVeh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDriveToVeh, TRUE)
					ENDIF
					
					ENABLE_PROCOBJ_CREATION()
					
					Unload_Asset_Scaleform(sAssetData, sfHeartMonitor)
					bECGSetup = FALSE
					bDisplayHeartMonitor = FALSE
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					eMissionStage = ST_2_DRIVE_TO_COORD_FIRST
				ENDIF
			BREAK
		ENDSWITCH

	//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_1_INTRO_CUT
			MissionStageTidyUp()
		
			SETTIMERB(0)
			stopCamShake(camGeneralInit, camGeneralDest)
			MissionStageTidyUp()
			ADD_ALL_PEDS_TO_DIALOGUE()
			
			bStageSetup = FALSE
		ENDIF
	ENDIF

ENDPROC

//PURPOSE:		Performs the section where Michael drives
PROC stage2_DriveToCoordFirst()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
	
		SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 30)
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "2. Michael Drives 1st", FALSE, FALSE, NULL, FALSE)
		// if Trevors vehicle, wipe it from the snapshot
//		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
//		AND IS_VEHICLE_A_PERSONAL_VEHICLE(GET_PLAYERS_LAST_VEHICLE())
//		AND GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(GET_PLAYERS_LAST_VEHICLE()) = CHAR_TREVOR
		IF g_stageSnapshot.mVehicleStruct.ePlayerCharacter = CHAR_TREVOR
		AND g_stageSnapshot.mVehicleStruct.bPersonalVehicle
			Reset_Vehicle_Snapshot(g_stageSnapshot.mVehicleStruct)
		ENDIF
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		IF b_SpawnTrain
			Load_Asset_Model(sAssetData, FREIGHT)
			Load_Asset_Model(sAssetData, FREIGHTCONT1)
			Load_Asset_Model(sAssetData, FREIGHTCONT2)
			Load_Asset_Model(sAssetData, FREIGHTGRAIN)
			Load_Asset_Model(sAssetData, TANKERCAR)
			Load_Asset_Model(sAssetData, FREIGHTCAR)
			
			SET_RANDOM_TRAINS(FALSE)
			DELETE_ALL_TRAINS()
		ENDIF
		
		ADD_DOOR_TO_SYSTEM(iGateHashA, PROP_LRGGATE_02_LD, <<-880.59, -9.56, 41.97>>)
		ADD_DOOR_TO_SYSTEM(iGateHashB, PROP_LRGGATE_02_LD, <<-893.16, 8.63, 43.71>>)
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iGateHashA)
		OR NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iGateHashB)
			SCRIPT_ASSERT("Gate(s) not found!")
		ELSE
			DOOR_SYSTEM_SET_DOOR_STATE(iGateHashA, DOORSTATE_LOCKED, TRUE, TRUE)
			DOOR_SYSTEM_SET_DOOR_STATE(iGateHashB, DOORSTATE_LOCKED, TRUE, TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehDriveToVeh)
		AND IS_VEHICLE_DRIVEABLE(vehDriveToVeh)
			SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vehDriveToVeh)
		ENDIF
		
		RunConversation("F3_WHERE")
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDave, TRUE)
		
		CLEAR_AREA(<<-890.90, -4.30, 42.39>>, 38.688, TRUE)
		
		sba_wrongLocation = ADD_SCENARIO_BLOCKING_AREA(<<-890.959290,-4.526050,48.068375>> - <<35.000000,30.000000,10.437500>>, 
			<<-890.959290,-4.526050,48.068375>> + <<35.000000,30.000000,10.437500>>)
			
		SET_PED_NON_CREATION_AREA(<<-890.959290,-4.526050,48.068375>> - <<35.000000,30.000000,10.437500>>, 
			<<-890.959290,-4.526050,48.068375>> + <<35.000000,30.000000,10.437500>>)
			
		SET_PED_PATHS_IN_AREA(<<-890.959290,-4.526050,48.068375>> - <<35.000000,30.000000,10.437500>>, 
			<<-890.959290,-4.526050,48.068375>> + <<35.000000,30.000000,10.437500>>, FALSE)
			
		GIVE_WEAPON_TO_PED(pedDave, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE)
		
		// bug 2006026
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
			RENDER_SCRIPT_CAMS( FALSE, TRUE, 2500, FALSE )
		ENDIF
		
		iNeedVehicleUses			= 0
		iWrongHouseCut				= -1
		iDriveStoppedTimer			= -1
		iDriveComplaintTimer		= 0
		i_mission_substage 			= 0
		str_InteruptedRoot			= ""
		str_InteruptedLabel			= ""
		bStageSetup 				= TRUE
		bEventFlag[1]				= FALSE
		bEventFlag[2]				= FALSE
		bEventFlag[3]				= FALSE
		bMusicCueTriggered			= FALSE
		bMusicCueKilled				= FALSE
		i_realCuntsStage			= 0
		b_CuntPropPlayingAnim[0] 	= FALSE
		b_CuntPropPlayingAnim[1] 	= FALSE
		b_CuntPropPlayingAnim[2] 	= FALSE
		iAssetRequests				= 0
	ENDIF

//═════════╡ UPDATE ╞═════════
	
	IF bStageSetup
	
		MANAGE_REAL_CUNTS_SCENE()
	
		SWITCH i_mission_substage
			CASE 0
				IF NOT bEventFlag[4]
					IF IS_PED_IN_ANY_VEHICLE(pedDave)
						bEventFlag[4] = TRUE
					ELSE
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly, TRUE)
					ENDIF
				ENDIF
				
				IF IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDY_ANY_MEANS(sLocatesData, v_BlipRockfordHillsHouse, 
						<<-874.998962,-21.779987,40.751186>>, <<-871.393311,-6.139875,45.712112>>, 13.062500, TRUE, pedDave,
						"TFT_DRIVE1", "TFT_RTNDAV", FALSE, TRUE)
						
					i_realCuntsStage = 1
											
					CLEAR_PRINTS()
					KILL_FACE_TO_FACE_CONVERSATION()
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					// Grab the last vehicle the player was using
					VEHICLE_INDEX vehLastVeh
					vehLastVeh = GET_PLAYERS_LAST_VEHICLE()
					IF DOES_ENTITY_EXIST(vehLastVeh)
						IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehLastVeh)
							SET_ENTITY_AS_MISSION_ENTITY(vehLastVeh, TRUE, TRUE)
						ENDIF
					ENDIF
					
					VECTOR vDimensionsMin, vDimensionsMax
					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehLastVeh), vDimensionsMin, vDimensionsMax)
					
					IF IS_ENTITY_ON_FIRE(MIKE_PED_ID())
						STOP_ENTITY_FIRE(MIKE_PED_ID())
					ENDIF
					IF IS_ENTITY_ON_FIRE(pedDave)
						STOP_ENTITY_FIRE(pedDave)
					ENDIF
					
					IF IS_ENTITY_ON_FIRE(vehLastVeh)
						STOP_ENTITY_FIRE(vehLastVeh)
					ENDIF
					SET_VEHICLE_BODY_HEALTH(vehLastVeh, 1000)
					SET_VEHICLE_PETROL_TANK_HEALTH(vehLastVeh, 1000)
					SET_VEHICLE_ENGINE_HEALTH(vehLastVeh, 1000)
					
					// Reposition player and their vehicle if they are in one and create the first camera
					IF IS_PED_IN_VEHICLE(MIKE_PED_ID(), vehLastVeh)
					AND IS_VEHICLE_DRIVEABLE(vehLastVeh)
					AND NOT IS_VEHICLE_RESTRICTED(vehLastVeh)
					AND NOT IS_THIS_MODEL_A_BIKE( GET_ENTITY_MODEL( vehLastVeh ) )
					AND NOT IS_THIS_MODEL_A_QUADBIKE( GET_ENTITY_MODEL( vehLastVeh ) )
					AND NOT IS_THIS_MODEL_A_BICYCLE( GET_ENTITY_MODEL( vehLastVeh ) )
					AND ABSF(vDimensionsMax.z - vDimensionsMin.z) < 2.5
					
						// this is the one the scripted cutscene will now refer to
						vehWrongLocationCutscene = vehLastVeh

						CLEAR_AREA(v_BlipRockfordHillsHouse, 10.0, TRUE)
						
						TEXT_LABEL_15 strSeatName
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehWrongLocationCutscene))
							strSeatName = "seat_f"
						ELSE
							strSeatName = "seat_dside_f"
						ENDIF
						
						SET_ENTITY_COORDS(vehWrongLocationCutscene, <<-876.7368, -14.0188, 41.4273>>)
						SET_ENTITY_HEADING(vehWrongLocationCutscene, 164.5692)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehWrongLocationCutscene)
						
						VECTOR camPos1, camPos2

//						camPos1 = <<-874.6,-16.0, 42.8>> - GET_WORLD_POSITION_OF_ENTITY_BONE(vehLastVeh, GET_ENTITY_BONE_INDEX_BY_NAME(vehLastVeh, strSeatName))
//						camPos1 = <<-874.5345, -15.5768, 42.7978>> - GET_WORLD_POSITION_OF_ENTITY_BONE(vehLastVeh, GET_ENTITY_BONE_INDEX_BY_NAME(vehLastVeh, strSeatName))						
//						CPRINTLN(DEBUG_MIKE, "OFFSET START: ", camPos1)
//						camPos1 = << 1.74115, -1.80626, 0.810997 >> + GET_WORLD_POSITION_OF_ENTITY_BONE(vehWrongLocationCutscene, GET_ENTITY_BONE_INDEX_BY_NAME(vehWrongLocationCutscene, strSeatName)) 
						camPos1 = << 1.79144, -1.3507, 0.80946 >> + <<0.0,0.0,0.15>> + GET_WORLD_POSITION_OF_ENTITY_BONE(vehWrongLocationCutscene, GET_ENTITY_BONE_INDEX_BY_NAME(vehWrongLocationCutscene, strSeatName)) 
						
//						camPos2 = <<-874.0,-15.0, 42.8>> - GET_WORLD_POSITION_OF_ENTITY_BONE(vehLastVeh, GET_ENTITY_BONE_INDEX_BY_NAME(vehLastVeh, strSeatName)) 
//						CPRINTLN(DEBUG_MIKE, "OFFSET END: ", camPos2)
						camPos2 = << 2.34113, -0.806258, 0.810997 >> + <<0.0,0.0,0.15>> + GET_WORLD_POSITION_OF_ENTITY_BONE(vehWrongLocationCutscene, GET_ENTITY_BONE_INDEX_BY_NAME(vehWrongLocationCutscene, strSeatName)) 

						camWrongLocation 	= CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,camPos1, <<-3.7418, -0.0529, 76.2490>>, 33.4690, TRUE)
						SET_CAM_PARAMS(camWrongLocation, camPos2, <<-3.7418, -0.0529, 76.2490>>, 33.4690, 3000)
						
						SHAKE_CAM(camWrongLocation, shakeHand, 0.5)
						
						SET_ENTITY_COORDS(vehWrongLocationCutscene, <<-875.0729, -9.6045, 41.7469>>) //<<-875.2629, -9.0932, 41.7644>>)
						SET_ENTITY_HEADING(vehWrongLocationCutscene, 162.8776)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehWrongLocationCutscene)
						TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), vehWrongLocationCutscene, <<-877.7476, -17.7999, 41.2598>>, 2.0, DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL( GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())), DRIVINGMODE_PLOUGHTHROUGH, 0.1, 10.0)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_VEHICLE_FORWARD_SPEED(vehWrongLocationCutscene, 2.0)

					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-878.9167, -13.8135, 41.5259>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 84.7426)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
						TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), pedCunts[CUNTPED_ACTOR_M], -1)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						ENDIF
						
						CLEAR_AREA(v_BlipRockfordHillsHouse, 10.0, TRUE)
						
						SET_ENTITY_COORDS(pedDave, <<-878.7094, -12.4876, 41.5789>>)
						SET_ENTITY_HEADING(pedDave, 83.8295)
						FORCE_PED_MOTION_STATE(pedDave, MS_ON_FOOT_IDLE)
						TASK_TURN_PED_TO_FACE_ENTITY(pedDave, pedCunts[CUNTPED_ACTOR_M], -1)

						camWrongLocation = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-876.6390, -12.2161, 44.6939>>, <<-7.0418, 0.0000, 90.6103>>, 30.4, TRUE)
						SET_CAM_PARAMS(camWrongLocation, <<-876.7625, -13.8252, 43.4649>>, <<-5.5399, 0.0000, 72.9562>>, 30.4, 7000)
					ENDIF
				
					
					SET_CAM_FAR_CLIP(camWrongLocation, 30.0)
					
					IF DOES_ENTITY_EXIST(vehLastVeh)
					AND IS_VEHICLE_DRIVEABLE(vehLastVeh)
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLastVeh, v_BlipRockfordHillsHouse) < 100.0
					
						// Store the last vehicle the player was using ready for the 2nd drive
						CPRINTLN(DEBUG_MIKE, "STORING THE LAST VEHICLE THE PLAYER USED")
						OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehLastVeh)
							
					ENDIF
					
					// Always clean up Trevors vehicle as it can never be used for this part of the mission
					IF DOES_ENTITY_EXIST(vehTrevor)
						DELETE_VEHICLE(vehTrevor)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehDriveToVeh)
					AND (NOT DOES_ENTITY_EXIST(vehWrongLocationCutscene) OR vehWrongLocationCutscene != vehDriveToVeh)
						IF DOES_ENTITY_EXIST(vehLastVeh)
						AND vehLastVeh = vehDriveToVeh
							vehLastVeh = null
						ENDIF
						DELETE_VEHICLE(vehDriveToVeh)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehReplay)
					AND (NOT DOES_ENTITY_EXIST(vehWrongLocationCutscene) OR vehWrongLocationCutscene != vehReplay)
						IF DOES_ENTITY_EXIST(vehLastVeh)
						AND vehLastVeh = vehReplay
							vehLastVeh = null
						ENDIF
						DELETE_VEHICLE(vehReplay)
					ENDIF

					IF DOES_ENTITY_EXIST(vehLastVeh)
					AND (NOT DOES_ENTITY_EXIST(vehWrongLocationCutscene) OR vehWrongLocationCutscene != vehLastVeh)
						DELETE_VEHICLE(vehLastVeh)
					ENDIF			
					
					DISPLAY_RADAR(FALSE)
					SHAKE_CAM(camWrongLocation, shakeHand, 0.2)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					// 1st look at
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedCunts[CUNTPED_ACTOR_F], -1, SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_ENTITY(pedDave, pedCunts[CUNTPED_ACTOR_F], -1, SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					
					TORTURE_LoadAssets(TRUE, TRUE)
					
					STOP_AUDIO_SCENE("FBI_3_MICHAEL_DRIVE_FIRST")
					START_AUDIO_SCENE("FBI_3_FOCUS_CAM_PHOTOSHOOT")
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					iWrongHouseCut = 0
					i_mission_substage++
				
				ELSE
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_BlipRockfordHillsHouse) < 50
							Load_Asset_NewLoadScene_Frustum(sAssetData, <<-872.9581, -13.6342, 42.8937>>, <<-883.6840, -12.3810, 43.8704>> - <<-872.9581, -13.6342, 42.8937>>, 30.0)
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_BlipRockfordHillsHouse) > 75
							Unload_Asset_NewLoadScene(sAssetData)
						ENDIF
					ENDIF
				
					IF NOT bMusicCueTriggered
						IF TIMERB() > 3500
						OR IS_PED_IN_ANY_VEHICLE(MIKE_PED_ID(), TRUE)
							TRIGGER_MUSIC_EVENT("FBI3_MICHAEL_MUSIC_1")
							bMusicCueTriggered = TRUE
						ENDIF
					ELIF NOT bMusicCueKilled
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_BlipRockfordHillsHouse) < 25.0
							TRIGGER_MUSIC_EVENT("FBI3_MICHAEL_ARRIVES_1")
							bMusicCueKilled = TRUE
						ENDIF
					ENDIF
				
					IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_DRIVE_FIRST")
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							START_AUDIO_SCENE("FBI_3_MICHAEL_DRIVE_FIRST")
						ENDIF
					ENDIF
				
					// Spawn a train if this is not a replay
					IF b_SpawnTrain
						IF NOT bEventFlag[3]
							IF NOT DOES_ENTITY_EXIST(vehTrainStart)
							AND HAS_MODEL_LOADED(FREIGHT)
							AND HAS_MODEL_LOADED(FREIGHTCONT1)
							AND HAS_MODEL_LOADED(FREIGHTCONT2)
							AND HAS_MODEL_LOADED(FREIGHTGRAIN)
							AND HAS_MODEL_LOADED(FREIGHTCAR)
							AND HAS_MODEL_LOADED(TANKERCAR)
								vehTrainStart = CREATE_MISSION_TRAIN(15, << 217.6160, -2215.7500, 11.6666 >>, TRUE)
								Unload_Asset_Model(sAssetData, FREIGHT)
								Unload_Asset_Model(sAssetData, FREIGHTCONT1)
								Unload_Asset_Model(sAssetData, FREIGHTCONT2)
								Unload_Asset_Model(sAssetData, FREIGHTGRAIN)
								Unload_Asset_Model(sAssetData, TANKERCAR)
								Unload_Asset_Model(sAssetData, FREIGHTCAR)
								
								SET_TRAIN_SPEED(vehTrainStart, 15.0)
								SET_TRAIN_CRUISE_SPEED(vehTrainStart, 15.0)
								bEventFlag[3] = TRUE
							ENDIF
						ELSE
							// Remove train when out of range
							IF DOES_ENTITY_EXIST(vehTrainStart)
								IF GET_DISTANCE_BETWEEN_ENTITIES(vehTrainStart, MIKE_PED_ID()) > 200
									SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(vehTrainStart)
									vehTrainStart = NULL
									SET_RANDOM_TRAINS(TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					// Handle drive conversation with dave and all its interrupts
					DRIVE_HandleDriveConvo_MikeWithDave("F3_DRV1", v_BlipRockfordHillsHouse, 30)
					
				ENDIF
			BREAK
			CASE 1
				IF DOES_ENTITY_EXIST(vehWrongLocationCutscene)
				AND IS_VEHICLE_DRIVEABLE(vehWrongLocationCutscene)
					SET_VEHICLE_DEFORMATION_FIXED(vehWrongLocationCutscene)
				ENDIF
				i_mission_substage++
			BREAK
			CASE 2
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-876.706604,-17.079636,40.987129>>, <<-875.831665,-13.647627,44.562244>>, 5.250000)
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 1.0, -1)
								IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
									CLEAR_PED_TASKS(PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			
				IF SWITCH_Wronglocation_To_Torture_CUTSCENE()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)	//Don't disable blinders
					eMissionStage = ST_3_TORTURE_FOR_FIRST
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PLAYER_PED_ID(), SCRIPT_TASK_LOOK_AT_ENTITY)
						// 2nd look at tasked in case clearing the tasks removes the peds look at.
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedCunts[CUNTPED_ACTOR_F], -1, SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
			
		SWITCH iWrongHouseCut
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					
					RunConversation("F3_CALL1Ai")
					iWrongHouseCut++
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TASK_CLEAR_LOOK_AT(pedDave)
					TASK_USE_MOBILE_PHONE(pedDave, TRUE)
					PLAY_SOUND_FROM_ENTITY(-1, "PED_PHONE_DIAL_01", pedDave)
					SETTIMERB(0)
					iWrongHouseCut++
				ENDIF
			BREAK	
			CASE 2
				IF TIMERB() > 3500
					RunConversation("F3_CALL1Aii")
					iWrongHouseCut++
				ENDIF
			BREAK
			CASe 3
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TASK_USE_MOBILE_PHONE(pedDave, FALSE)
					SETTIMERB(0)
					iWrongHouseCut++
				ENDIF
			BREAK
			CASE 4
				IF TIMERB() > 1000
					TASK_LOOK_AT_ENTITY(pedDave, MIKE_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV | SLF_SLOW_TURN_RATE)
					iWrongHouseCut++
				ENDIF
			BREAK
			CASE 5
				IF TIMERB() > 2000
					RunConversation("F3_COFFEE2")
					iWrongHouseCut++
				ENDIF
			BREAK
			CASE 6
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					SETTIMERB(0)
					iWrongHouseCut++
				ENDIF
			BREAK
			CASE 7
				IF TIMERB() > 1000
					IF IS_REPLAY_RECORDING()
						REPLAY_STOP_EVENT()
					ENDIF
					
					bSafeToSwitch = TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
		IF iWrongHouseCut > -1 AND iWrongHouseCut <= 6
			IF IS_SCREEN_FADED_OUT()
				TORTURE_LoadAssets(TRUE, TRUE)
				bSafeToSwitch = TRUE
			ELSE
				IF NOT IS_SCREEN_FADING_OUT()
					IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					AND NOT bMissionFailed
						IF IS_REPLAY_RECORDING()
							REPLAY_CANCEL_EVENT()
						ENDIF
						DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
//═════════╡ CLEANUP ╞════════
		IF eMissionStage != ST_2_DRIVE_TO_COORD_FIRST
			IF IS_REPLAY_RECORDING()
				REPLAY_STOP_EVENT()
			ENDIF
			DISABLE_CELLPHONE(FALSE)
			
			IF DOES_ENTITY_EXIST(vehTrainStart)
				SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(vehTrainStart)
				vehTrainStart = NULL
			ENDIF
			SET_RANDOM_TRAINS(TRUE)
			
			MissionStageTidyUp()
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Trevor tortures the victim for the first time
PROC stage3_TortureForFirst()

	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	DISABLE_PLAYER_PED_STAT_UPDATES_THIS_FRAME()
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "3. Trevor Tortures First", FALSE, FALSE, NULL, FALSE)
		TORTURE_Store_Victim_State()
		
		DISPLAY_RADAR(FALSE)
		
		DISABLE_CELLPHONE(TRUE)
		
	#IF NOT IS_JAPANESE_BUILD
		bInitialWeaponHighlighted 	= FALSE
	#ENDIF

		i_mission_substage			= 0
		bStageSetup 				= TRUE
		iAssetRequests				= 0
		
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)
		SET_CREATE_RANDOM_COPS(FALSE)
		
		SET_PC_CONTROLS( "FBI3 MINIGAME SELECT")
		SET_MULTIHEAD_SAFE(TRUE)
		
	ENDIF

//═════════╡ UPDATE ╞═════════		
	IF bStageSetup
		bDisplayHeartMonitor = TRUE
		
		IF i_mission_substage < 5
			IF IS_SCREEN_FADED_IN()
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				AND NOT bMissionFailed
					SAFE_FADE_OUT()
				ENDIF
			ELIF IS_SCREEN_FADED_OUT()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
		ENDIF
		
		SWITCH i_mission_substage
			CASE 0
				SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
				
				TRIGGER_MUSIC_EVENT("FBI3_TORTURE_START")
				bTortureMusicPlaying = TRUE

				IF NOT bHasSwitchSetupCam
					doTwoPointCam(camGeneralInit, camGeneralDest,	<< 146.7890, -2204.1140, 5.6386 >>, << -10.8642, 7.4269, 61.6078 >>, 17.4610,
									<< 146.7890, -2204.1140, 5.6386 >>, << -10.8642, 7.4269, 61.6078 >>, 16.0803,
									5000)
					doCamShake(camGeneralInit, camGeneralDest, shakeHand, 0.3)
				ENDIF
				
				SETTIMERB(0)
				
				RunConversation("F3_CALL1B")
				SAFE_FADE_IN()
#IF NOT IS_JAPANESE_BUILD
				PRINT_HELP_FOREVER(hlpTorture1)
#ENDIF
				i_mission_substage++
			BREAK
			CASE 1
				IF TIMERB() > 5000
				OR IS_SCREEN_FADED_OUT()
					doTwoPointCam(camGeneralInit, camGeneralDest, 	<< 143.8915, -2202.9985, 7.5430 >>, << -73.9446, -17.3188, -160.2916 >>, 16.3626,
									<< 143.6610, -2203.0364, 7.5307 >>, << -72.3070, -17.3188, -154.9457 >>, 16.3626,
									5000)
					doCamShake(camGeneralInit, camGeneralDest, shakeHand, 0.3)
					SETTIMERB(0)
#IF NOT IS_JAPANESE_BUILD					
					PRINT_HELP_FOREVER(hlpTorture2)
#ENDIF					
					i_mission_substage++
				ENDIF
			BREAK
			CASE 2
				IF TIMERB() > 5000
				OR IS_SCREEN_FADED_OUT()
					doTwoPointCam(camGeneralInit, camGeneralDest, 	<< 142.5872, -2200.3647, 4.5637 >>, << 7.3505, -5.0, -159.0778 >>, 24.4943,
									<< 142.5258, -2200.3882, 4.5588 >>, << 7.3505, -5.0, -157.9305 >>, 24.4943,
									5000)
					doCamShake(camGeneralInit, camGeneralDest, shakeHand, 0.3)
					SETTIMERB(0)
#IF NOT IS_JAPANESE_BUILD					
					PRINT_HELP_FOREVER(hlpTorture3)
#ENDIF					
					i_mission_substage++
				ENDIF
			BREAK
			CASE 3
				IF TIMERB() > 5000
				OR IS_SCREEN_FADED_OUT()
					doTwoPointCam(camGeneralInit, camGeneralDest, 	<<143.4552, -2204.9785, 4.8811>>, <<-3.6438, 2.7744, 7.1743>>, 18.4641,
									<<143.4501, -2204.9922, 4.8>>, <<-3.6438, 2.7744, 7.1743>>, 18.4641,
									5000)
					doCamShake(camGeneralInit, camGeneralDest, shakeHand, 0.3)
					SETTIMERB(0)
#IF NOT IS_JAPANESE_BUILD					
					PRINT_HELP_FOREVER(hlpTorture4)
#ENDIF					
					i_mission_substage++
				ENDIF
			BREAK
			CASE 4
				IF TIMERB() > 5000
				OR IS_SCREEN_FADED_OUT()
					TORTURE_ManageWeaponSelection()
					SAFE_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					i_mission_substage++
				ENDIF
			BREAK
			CASE 5
				IF TORTURE_ManageWeaponSelection()
#IF IS_JAPANESE_BUILD
						TORTURE_Request_Cutscene(FALSE)
#ENDIF
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					i_mission_substage++
				ENDIF
			BREAK
			CASE 6
#IF NOT IS_JAPANESE_BUILD
				IF TORTURE_ManageMinigame()
#ENDIF
					SHUTDOWN_PC_CONTROLS()
					SWITCH_Torture_To_Coffee()
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(15.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					i_mission_substage++
#IF NOT IS_JAPANESE_BUILD
				ENDIF
#ENDIF
			BREAK
			CASE 7
				IF SWITCH_Torture_To_Coffee()
					eMissionStage = ST_4_DRIVE_TO_COORD_SECOND
				ENDIF
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_3_TORTURE_FOR_FIRST
			clearText()
			TORTURE_CleanupEntitiesAndAssets()
			MissionStageTidyUp()
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC


//PURPOSE:		Performs the section where Michael drives to the snipe location
PROC stage4_DriveToCoordSecond()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "4. Michael Drives 2nd")
		TORTURE_Store_Victim_State()
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		DISPLAY_RADAR(TRUE)
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_ForceDirectEntry, TRUE)
		
		i_mission_substage 	= 0
		bStageSetup 	= TRUE
		bMichaelExited 	= FALSE
		bDaveExited		= FALSE
		
		SET_PED_NON_CREATION_AREA(<<-2969.646484,351.977753,16.206539>> - <<24.437500,26.125000,2.687500>>,
			<<-2969.646484,351.977753,16.206539>> + <<24.437500,26.125000,2.687500>>)
		
		SET_PED_PATHS_IN_AREA(<<-2969.646484,351.977753,16.206539>> - <<24.437500,26.125000,2.687500>>,
			<<-2969.646484,351.977753,16.206539>> + <<24.437500,26.125000,2.687500>>, FALSE)
				
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2969.646484,351.977753,16.206539>> - <<24.437500,26.125000,2.687500>>,
			<<-2969.646484,351.977753,16.206539>> + <<24.437500,26.125000,2.687500>>, FALSE)
			
		sba_chumash_car_park = ADD_SCENARIO_BLOCKING_AREA(<<-2969.646484,351.977753,16.206539>> - <<24.437500,26.125000,2.687500>>,
			<<-2969.646484,351.977753,16.206539>> + <<24.437500,26.125000,2.687500>>)
			
		CLEAR_AREA(vDriveTo2Coords, 100.0, TRUE)
		
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		IF IS_VEHICLE_DRIVEABLE(vehDriveToVeh)				
			SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(sLocatesData, vehDriveToVeh)
		ENDIF
		
		ePartyState = PCS_NOT_CRASHED
		
		bMusicCueTriggered 	= FALSE
		bMusicCueKilled 	= FALSE
		iNeedVehicleUses	= 0
		iAssetRequests		= 0
		bEventFlag[0]		= FALSE
		
		SET_MULTIHEAD_SAFE(FALSE)

//═════════╡ UPDATE ╞═════════
	ENDIF
	
	IF bStageSetup
	
		IF bManageParty
			PARTY_ManagePartyScene(TRUE)
			PARTY_ManagePartyCrashed()
			
			IF IS_SAFE_TO_START_CONVERSATION()
				SWITCH ePartyState
					CASE PCS_MISSED_SHOT
						//RunConversation("F3_SPOOKEDe")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDave, "GENERIC_SHOCKED_MED", "Dave", SPEECH_PARAMS_FORCE)
						eMissionFail 	= FAIL_PARTY_SPOOKED
					BREAK
					CASE PCS_KILLED_INNOCENT_MALE
						//RunConversation("F3_KILLWRNGM")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDave, "GENERIC_SHOCKED_MED", "Dave", SPEECH_PARAMS_FORCE)
						eMissionFail 	= FAIL_PARTY_PED_DEAD_INNOCENT_NO_ID
					BREAK
					CASE PCS_KILLED_INNOCENT_FEMALE
						//RunConversation("F3_KILLWRNGF")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDave, "GENERIC_SHOCKED_MED", "Dave", SPEECH_PARAMS_FORCE)
						eMissionFail 	= FAIL_PARTY_PED_DEAD_INNOCENT
					BREAK
				ENDSWITCH
			
				IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
					IF ePartyState != PCS_NOT_CRASHED
						bMissionFailed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF ePartyState = PCS_NOT_CRASHED

			SWITCH i_mission_substage
				
				CASE 0
					IF IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_COFFEE_BREAK")
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)	
							STOP_AUDIO_SCENE("FBI_3_MICHAEL_COFFEE_BREAK")
						ENDIF
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("FBI_3_MICHAEL_DRIVE_CHUMASH")
						IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
							START_AUDIO_SCENE("FBI_3_MICHAEL_DRIVE_CHUMASH")
						ENDIF
					ENDIF
					
					DRIVE_HandleDriveConvo_MikeWithDave("F3_DRV2", vDriveTo2Coords, 30)
					
				// Stream in and manage the party
				//-------------------------------------------------------------
				
					IF NOT bEventFlag[0]
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-3102.10, 349.82, 16.71>>) < 250.0
							bEventFlag[0] = TRUE
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-3102.10, 349.82, 16.71>>) > 300.0
							PARTY_CleanupEntitiesAndAssets(TRUE)
							iAssetRequests = 0
							bEventFlag[0] = FALSE
							bManageParty = FALSE
						ENDIF
					ENDIF

					IF bEventFlag[0]	
						IF PARTY_LoadAssets(FALSE, TRUE, TRUE)
						AND PARTY_IsSceneReady(FALSE, TRUE, ST_2_DRIVE_TO_COORD_FIRST, TRUE)
							bManageParty = TRUE	
						ENDIF
					ENDIF

					
					// Chumash locate)
					IF IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDY_IN_ANY_VEHICLE(sLocatesData, vDriveTo2Coords, <<-2958.950928,370.906342,13.519890>>, <<-2967.798096,352.648590,16.831692>>, 4.625000, 
																	TRUE, pedDave,
																	godDriveToMalibu, "TFT_RTNDAV", godMichaelGetIn, godMichaelGetBack, TRUE, TRUE, 2)
														
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						clearText(FALSE, TRUE, TRUE)
						DISABLE_CELLPHONE(TRUE)
						KILL_FACE_TO_FACE_CONVERSATION()

						i_mission_substage++
						
					ELSE
					
						IF NOT IS_CUTSCENE_ACTIVE()
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveTo2Coords) < DEFAULT_CUTSCENE_LOAD_DIST
								IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
									SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS")  
								ENDIF
								REQUEST_CUTSCENE("fbi_3_mcs_2")
								Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(TRUE, TRUE, strMrKCSHandle)
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveTo2Coords) > DEFAULT_CUTSCENE_UNLOAD_DIST
							OR IS_PLAYER_CHANGING_CLOTHES()
								REMOVE_CUTSCENE()
							ENDIF
						ENDIF
					
						IF bMusicCueTriggered
							IF NOT bMusicCueKilled
								IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveTo2Coords) < 25.0
									TRIGGER_MUSIC_EVENT("FBI3_MICHAEL_ARRIVES_2")
									bMusicCueKilled = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT (IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
					OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 4.0, 1, 0.1)
							bSafeToSwitch = TRUE
							
							i_mission_substage++
						ENDIF
					ELSE
						bSafeToSwitch = TRUE
						i_mission_substage++
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					AND NOT IS_PED_IN_ANY_VEHICLE(pedDave, TRUE)
						SET_MULTIHEAD_SAFE(TRUE) //B* 2233643: Set blinders on early
						i_mission_substage++
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							ENDIF
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(pedDave, TRUE)
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedDave, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
								TASK_LEAVE_ANY_VEHICLE(pedDave, 500)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF SWITCH_Sniping_To_Torture_CUTSCENE(ST_4_DRIVE_TO_COORD_SECOND)
						DISPLAY_RADAR(FALSE) 
						
						eMissionStage = ST_5_TORTURE_FOR_SECOND
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_4_DRIVE_TO_COORD_SECOND
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			
			CLEAR_PED_NON_CREATION_AREA()
		
			SET_PED_PATHS_BACK_TO_ORIGINAL(<<-2969.646484,351.977753,16.206539>> - <<24.437500,26.125000,2.687500>>,
				<<-2969.646484,351.977753,16.206539>> + <<24.437500,26.125000,2.687500>>)
			
			REMOVE_SCENARIO_BLOCKING_AREA(sba_chumash_car_park)
			REMOVE_SCENARIO_BLOCKING_AREA(sba_coffee_shop)
			
			DISABLE_CELLPHONE(FALSE)
			MissionStageTidyUp()
			
			bStageSetup = FALSE
			
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE:		Performs the section where Trevor tortures the victim for the third time
PROC stage5_TortureForSecond()

	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	DISABLE_PLAYER_PED_STAT_UPDATES_THIS_FRAME()
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
	
		DISPLAY_RADAR(FALSE) 
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "5.Trevor Tortures Second")
		TORTURE_Store_Victim_State()
	
		ADD_ALL_PEDS_TO_DIALOGUE()

		i_mission_substage = 0
		bStageSetup = TRUE
		iAssetRequests	= 0
		
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)
		SET_CREATE_RANDOM_COPS(FALSE)
		
		SET_MULTIHEAD_SAFE(TRUE)

//═════════╡ UPDATE ╞═════════		
	ENDIF
	
	IF bStageSetup
		bDisplayHeartMonitor = TRUE
		
		SWITCH i_mission_substage
			CASE 0
				IF TORTURE_ManageWeaponSelection()
#IF IS_JAPANESE_BUILD
						TORTURE_Request_Cutscene(FALSE)
#ENDIF
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					i_mission_substage++
				ENDIF
			BREAK
			CASE 1
#IF NOT IS_JAPANESE_BUILD
				IF TORTURE_ManageMinigame()
#ENDIF
					SHUTDOWN_PC_CONTROLS()
					SWITCH_Torture_To_Sniping_CUTSCENE()
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(15.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					i_mission_substage++
#IF NOT IS_JAPANESE_BUILD				
				ENDIF
#ENDIF				
			BREAK
			CASE 2
				IF SWITCH_Torture_To_Sniping_CUTSCENE()
					PARTY_ManagePartyScene()
					PARTY_ManageProneAndScope()
					eMissionStage = ST_6_REACT_TO_SECOND
				ENDIF
			BREAK
			
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_5_TORTURE_FOR_SECOND
			clearText()

			MissionStageTidyUp()
			iSelectWeaponStage = 0

			bHasPlayedPickupAnim = FALSE
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Michael reacts to the information revealed for the third time
PROC stage6_ReactToSecond()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175240
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "6. Michael Reacts to 2nd")
		TORTURE_Store_Victim_State()
		
		DISPLAY_RADAR(FALSE)
		
		
		DISABLE_CELLPHONE(TRUE)
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		IF NOT bDontPlayFullSnipeChat
			PRINT_NOW(godShootTarget, DEFAULT_GOD_TEXT_TIME, 1)
		ENDIF
		PARTY_ResetScopeCheck()
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI3_ASSASSIN_TIME)
		
		TRIGGER_MUSIC_EVENT("FBI3_BACK_TO_MICHAEL")
		
		i_mission_substage 	= 0
		bManageParty 	= TRUE
		bStageSetup 	= TRUE
		bSafeToSwitch 	= FALSE
		iAssetRequests	= 0
		iAttemptToAimTimer = -1
		
		SET_MULTIHEAD_SAFE(FALSE)
		
	ENDIF
//═════════╡ UPDATE ╞═════════		
	IF bStageSetup
		IF bManageParty
			PARTY_ManagePartyScene(TRUE)
			PARTY_ManageProneAndScope()
			PARTY_ManagePartyCrashed()
		ENDIF
		
		IF ePartyState != PCS_NOT_CRASHED
			IF IS_THIS_PRINT_BEING_DISPLAYED(godShootTarget)
			OR IS_THIS_PRINT_BEING_DISPLAYED(godSwitchTrevor)
				CLEAR_PRINTS()
			ENDIF
		ENDIF
	
		IF ePartyState = PCS_NOT_CRASHED		
			
			SWITCH i_mission_substage
				CASE 0
					// Jump ahead
					IF bDontPlayFullSnipeChat
						bDontPlayFullSnipeChat	= FALSE
						i_mission_substage = 2
					
					// Normal sniping chat
					ELSE
						IF bScoped[HS_CENTRE]
						OR (iAttemptToAimTimer != -1 AND GET_GAME_TIMER() > iAttemptToAimTimer)
							IF IS_SAFE_TO_START_CONVERSATION()
								RunConversation("F3_ZOOM1")
								i_mission_substage++
								
							ELIF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ELSE
							IF NOT bEventFlag[0]
								IF IS_SAFE_TO_START_CONVERSATION()
									RunConversation("F3_SCOPE1")
									bEventFlag[0] = TRUE
								ENDIF
							ELSE
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_time_of_last_convo > 5000
									RunConversation("F3_ZOOMD")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_SAFE_TO_START_CONVERSATION()
						RunConversation("F3_SPOT1")
						IF NOT IS_REPLAY_RECORDING()
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						ENDIF
						i_mission_substage++
					ENDIF
				BREAK
				CASE 2
					IF IS_SAFE_TO_START_CONVERSATION()
						IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
							SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS")  
						ENDIF
						REQUEST_CUTSCENE("fbi_3_mcs_4p2")
						Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(FALSE, FALSE, strMrKCSHandle)
						RunConversation("F3_SPOT1b")
						i_mission_substage++
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bSafeToSwitch = TRUE
						i_mission_substage++
					ENDIF
				BREAK
				CASE 4
					IF SWITCH_Sniping_To_Torture_CUTSCENE(ST_6_REACT_TO_SECOND)
						DISPLAY_RADAR(FALSE) 
						
						eMissionStage = ST_7_TORTURE_FOR_THIRD
					ELSE
						IF bCutsceneSWITCHStarted
							bManageParty = FALSE
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_time_of_last_convo > 15000
								RunConversation("F3_PROMPT")
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		
		// Party crashed
		ELSE
			IF PARTY_HasTargetBeenSuccessfullyKilled()
				IF SWITCH_Sniping_To_Torture_CUTSCENE()
					eMissionStage = ST_11_DRIVE_TO_AIRPORT
				ENDIF
			ENDIF
		ENDIF
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_6_REACT_TO_SECOND
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			DISABLE_CELLPHONE(FALSE)
			MissionStageTidyUp()
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Trevor tortures the victim for the fourth time
PROC stage7_TortureForThird()

	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	DISABLE_PLAYER_PED_STAT_UPDATES_THIS_FRAME()
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		DISPLAY_RADAR(FALSE)
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "7. Trevor tortures thrid")
		TORTURE_Store_Victim_State()
	
		ADD_ALL_PEDS_TO_DIALOGUE()

		iAssetRequests	= 0
		i_mission_substage = 0
		bStageSetup = TRUE
		
		SET_MULTIHEAD_SAFE(TRUE)
		
	ENDIF

//═════════╡ UPDATE ╞═════════		

	IF bStageSetup
		bDisplayHeartMonitor = TRUE
		
		SWITCH i_mission_substage
			CASE 0
				IF TORTURE_ManageWeaponSelection()
#IF IS_JAPANESE_BUILD
						TORTURE_Request_Cutscene(FALSE)
#ENDIF
					i_mission_substage++
				ENDIF
			BREAK
			CASE 1
#IF NOT IS_JAPANESE_BUILD				
				IF TORTURE_ManageMinigame()
#ENDIF				
					SHUTDOWN_PC_CONTROLS()
					SWITCH_Torture_To_Sniping_CUTSCENE()
					i_mission_substage++
#IF NOT IS_JAPANESE_BUILD				
				ENDIF
#ENDIF			
			BREAK
			CASE 2
				IF SWITCH_Torture_To_Sniping_CUTSCENE()
					PARTY_ManagePartyScene(TRUE)
					PARTY_ManageProneAndScope()
					eMissionStage = ST_8_REACT_TO_THIRD
				ENDIF
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_7_TORTURE_FOR_THIRD
			clearText()

			MissionStageTidyUp()
			iSelectWeaponStage = 0

			bHasPlayedPickupAnim = FALSE
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Michael reacts to the information revealed for the third time
PROC stage8_ReactToThird()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175240
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
	
		DISPLAY_RADAR(FALSE)
		
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8, "8. Michael Reacts to 3rd")
		TORTURE_Store_Victim_State()

		DISABLE_CELLPHONE(TRUE)
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		PARTY_ResetScopeCheck()
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI3_ASSASSIN_TIME)
		
		TRIGGER_MUSIC_EVENT("FBI3_BACK_TO_MICHAEL")
		
		i_mission_substage = 0
		bManageParty = TRUE
		bStageSetup = TRUE
		bSafeToSwitch = FALSE
		iAssetRequests	= 0
		iAttemptToAimTimer = -1
		
		SET_MULTIHEAD_SAFE(FALSE)
		
	ENDIF

//═════════╡ UPDATE ╞═════════		
	IF bStageSetup
		IF bManageParty
			PARTY_ManagePartyScene(TRUE)
			PARTY_ManageProneAndScope()
			PARTY_ManagePartyCrashed()
		ENDIF
		
		IF ePartyState != PCS_NOT_CRASHED
			IF IS_THIS_PRINT_BEING_DISPLAYED(godSwitchTrevor)
				CLEAR_PRINTS()
			ENDIF
		ENDIF
	
		IF ePartyState = PCS_NOT_CRASHED
		
			SWITCH i_mission_substage
				CASE 0
					IF bDontPlayFullSnipeChat
						bDontPlayFullSnipeChat	= FALSE
						i_mission_substage = 2
					ELSE
						IF bScoped[HS_CENTRE]
						OR (iAttemptToAimTimer != -1 AND GET_GAME_TIMER() > iAttemptToAimTimer)
							IF IS_SAFE_TO_START_CONVERSATION()
								RunConversation("F3_ZOOM2")
								i_mission_substage++
							
							ELIF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ELSE
							IF NOT bEventFlag[0]
								IF IS_SAFE_TO_START_CONVERSATION()
									RunConversation("F3_SCOPE2")
									bEventFlag[0] = TRUE
								ENDIF
							ELSE
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_time_of_last_convo > 5000
									RunConversation("F3_ZOOMD")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_SAFE_TO_START_CONVERSATION()
						RunConversation("F3_SPOT2")
						i_mission_substage++
					ENDIF
				BREAK
				CASE 2
					IF IS_SAFE_TO_START_CONVERSATION()
						IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
							SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS")  
						ENDIF
						REQUEST_CUTSCENE("FBI_3_MCS_5p2")
						Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(FALSE, FALSE, strMrKCSHandle)
						RunConversation("F3_SPOT2b")
						i_mission_substage++
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bSafeToSwitch = TRUE
						i_mission_substage++
					ENDIF
				BREAK
				CASE 4
					IF SWITCH_Sniping_To_Torture_CUTSCENE(ST_8_REACT_TO_THIRD)
						DISPLAY_RADAR(FALSE) 
						
						eMissionStage = ST_9_TORTURE_FINAL
					ELSE
						IF bCutsceneSWITCHStarted
							bManageParty = FALSE
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_time_of_last_convo > 15000
								RunConversation("F3_PROMPT")
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
		// Party crashed
		ELSE
			IF PARTY_HasTargetBeenSuccessfullyKilled()
				IF SWITCH_Sniping_To_Torture_CUTSCENE()
					eMissionStage = ST_11_DRIVE_TO_AIRPORT
				ENDIF
			ENDIF
		ENDIF
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_8_REACT_TO_THIRD
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			DISABLE_CELLPHONE(FALSE)
			MissionStageTidyUp()
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Trevor tortures the victim for the final time
PROC stage9_TortureFinal()

	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	DISABLE_PLAYER_PED_STAT_UPDATES_THIS_FRAME()
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9, "9. Trevor tortures Final")
		TORTURE_Store_Victim_State()
		
		DISPLAY_RADAR(FALSE)
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		i_mission_substage = 0
		bStageSetup = TRUE
		iAssetRequests	= 0
		
		SET_MULTIHEAD_SAFE(TRUE)
		
	ENDIF	

//═════════╡ UPDATE ╞═════════		
	IF bStageSetup
		bDisplayHeartMonitor = TRUE
		
		SWITCH i_mission_substage
			CASE 0
				IF TORTURE_ManageWeaponSelection()
#IF IS_JAPANESE_BUILD					
						TORTURE_Request_Cutscene(FALSE)
#ENDIF					
					i_mission_substage++
				ENDIF
			BREAK
			CASE 1
#IF NOT IS_JAPANESE_BUILD			
				IF TORTURE_ManageMinigame()
#ENDIF				
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_RECORD_BACK_FOR_TIME(15.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					SHUTDOWN_PC_CONTROLS()
					SWITCH_Torture_To_Sniping_CUTSCENE()
					i_mission_substage++
#IF NOT IS_JAPANESE_BUILD					
				ENDIF
#ENDIF				
			BREAK
			CASE 2
				IF SWITCH_Torture_To_Sniping_CUTSCENE()
					PARTY_ManagePartyScene(TRUE)
					PARTY_ManageProneAndScope()
					eMissionStage = ST_10_REACT_FINAL
				ENDIF
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_9_TORTURE_FINAL
			clearText()

			MissionStageTidyUp()
			iSelectWeaponStage = 0

			bHasPlayedPickupAnim = FALSE
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Michael takes the final shot
PROC stage10_ReactFinal()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2175240
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(10, "10. Michael Kills Target")
		TORTURE_Store_Victim_State()
		
		DISPLAY_RADAR(FALSE)
		
		DISABLE_CELLPHONE(TRUE)
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		bTargetKilled 		= FALSE
		bTargetHasBeenIDed 	= FALSE
		
		bMrKVariations[MR_K_WET] = FALSE

		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI3_ASSASSIN_TIME)
		
		TRIGGER_MUSIC_EVENT("FBI3_BACK_TO_MICHAEL")
		
		bManageParty 	= TRUE
		bStageSetup 	= TRUE
		i_mission_substage 	= 0
		bSafeToSwitch 	= FALSE
		iAssetRequests	= 0
		iAttemptToAimTimer = -1
	ENDIF

//═════════╡ UPDATE ╞═════════		
	IF bStageSetup
		IF bManageParty
			PARTY_ManagePartyScene(TRUE)
			PARTY_ManageProneAndScope()
			PARTY_ManagePartyCrashed()
		ENDIF
		
		IF ePartyState != PCS_NOT_CRASHED
			IF IS_THIS_PRINT_BEING_DISPLAYED(godSwitchTrevor)
				CLEAR_PRINTS()
			ENDIF
		ENDIF
		
		IF ePartyState = PCS_NOT_CRASHED
		OR ePartyState = PCS_MISSED_SHOT
		
			IF NOT bTargetKilled
			AND DOES_ENTITY_EXIST(peds[mpf_target].id)
			AND IS_PED_INJURED(peds[mpf_target].id)
			
				IF NOT IS_REPLAY_RECORDING()
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
				ENDIF
			
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				bTargetKilled = TRUE
				CDEBUG1LN(DEBUG_MIKE, "TargetKilled = TRUE")
			ENDIF
		
		ENDIF
		
		IF ePartyState = PCS_NOT_CRASHED
		
			SWITCH i_mission_substage
				CASE 0
					IF bDontPlayFullSnipeChat
						bDontPlayFullSnipeChat	= FALSE
						i_mission_substage = 2
					ELSE
						IF NOT IS_PED_INJURED(peds[mpf_target].id)
							IF IS_SAFE_TO_START_CONVERSATION()
								IF bScoped[HS_CENTRE]
								OR (iAttemptToAimTimer != -1 AND GET_GAME_TIMER() > iAttemptToAimTimer)
									i_mission_substage++
								ELSE
									IF NOT bEventFlag[0]
										RunConversation("F3_SCOPE3")
										bEventFlag[0] = TRUE
									ELSE
										IF GET_GAME_TIMER() - i_time_of_last_convo > 5000
											RunConversation("F3_ZOOMD")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF

				BREAK
				CASE 1
					IF IS_SAFE_TO_START_CONVERSATION()
						RunConversation("F3_SPOT3")
						i_mission_substage++
					ENDIF
				BREAK
				CASE 2
					IF IS_SAFE_TO_START_CONVERSATION()
						RunConversation("F3_SPOT3l")
						i_mission_substage++
					ENDIF
				BREAK
				CASE 3
					IF DOES_ENTITY_EXIST(peds[mpf_target].id)
					AND NOT IS_PED_INJURED(peds[mpf_target].id)
						IF bAimingAtTheTarget
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							RunConversation("F3_SPOT3b")
							i_mission_substage++
						ENDIF
					ENDIF
				BREAK
				CASE 4
					IF IS_SAFE_TO_START_CONVERSATION()
						RunConversation("F3_SPOT3c")
						bTargetHasBeenIDed = TRUE
						i_mission_substage++
					ENDIF
				BREAK
				
			ENDSWITCH
		
		// Party crashed
		ELSE
			IF PARTY_HasTargetBeenSuccessfullyKilled()
				IF NOT IS_CUTSCENE_ACTIVE()
					IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
						SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS")  
					ENDIF
					REQUEST_CUTSCENE("fbi_3_mcs_7")
					Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(FALSE, FALSE, strMrKCSHandle)
				ENDIF
			
				IF SWITCH_Sniping_To_Torture_CUTSCENE()
					eMissionStage = ST_11_DRIVE_TO_AIRPORT
				ENDIF
			ENDIF
		ENDIF
	
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_10_REACT_FINAL
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			MissionStageTidyUp()
			DISABLE_CELLPHONE(FALSE)
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:		Performs the section where Trevor drives the victim to the airport
PROC stage11_DriveToAirport()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(11, "12. Trevor Drives MrK to Airport", TRUE)

		ADD_ALL_PEDS_TO_DIALOGUE()
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		
		SETTIMERB(0)
		SET_PED_CONFIG_FLAG(pedVictim, PCF_GetOutBurningVehicle, FALSE)
		
		IF DOES_ENTITY_EXIST(vehTrevor)
		AND IS_VEHICLE_DRIVEABLE(vehTrevor)
			SET_VEHICLE_DISABLE_TOWING(vehTrevor, TRUE)
		ENDIF
		
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		SET_PED_CREW(pedVictim)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
		
		UNPIN_INTERIOR(interior_torture)
		
		SET_VEHICLE_HAS_STRONG_AXLES(vehTrevor, TRUE)
		
		START_AUDIO_SCENE("FBI_3_TREVOR_DRIVE_TO_AIRPORT")
		
		SET_PED_NON_CREATION_AREA(
			<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
			<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>)
			
		SET_PED_PATHS_IN_AREA(
			<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
			<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>, FALSE)
			
		sba_airport = ADD_SCENARIO_BLOCKING_AREA(<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
			<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>)
			
		CLEAR_AREA(<<-1038.279907,-2738.568359,21.232233>>, 40.813, TRUE)
		
		IF iRoadNodeSpeedZone[0] != -1
			REMOVE_ROAD_NODE_SPEED_ZONE(iRoadNodeSpeedZone[0])
		ENDIF
		iRoadNodeSpeedZone[0] = ADD_ROAD_NODE_SPEED_ZONE(<<-1057.38, -2715.40, 19.12>>, 4.375, 0.0)
		CLEAR_AREA_OF_VEHICLES(vDriveToAirport, 100)
		
		IF bStoredSniperSnapshot
			IF DOES_ENTITY_EXIST(MIKE_PED_ID())
				STORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
			ENDIF
			g_savedGlobals.sPlayerData.sInfo.sWeapons[CHAR_MICHAEL].sWeaponInfo[21] = sSniperSnapshot
			IF DOES_ENTITY_EXIST(MIKE_PED_ID())
				RESTORE_PLAYER_PED_WEAPONS(MIKE_PED_ID())
			ENDIF
			bStoredSniperSnapshot = FALSE
		ENDIF		
		
		SAFE_FADE_IN()
		i_mission_substage = 0
		iWeaponSelStage = 0
		bStageSetup = TRUE
		iAssetRequests	= 0
		
		IF NOT IS_REPLAY_RECORDING()
			REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		ENDIF
	ENDIF

//═════════╡ UPDATE ╞═════════
	IF bStageSetup
		SWITCH i_mission_substage
			CASE 0
				// Set to stream in
				IF NOT bEventFlag[0]
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveToAirport) < 250
						bEventFlag[0] = TRUE
					ENDIF
				// Set to stream out
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveToAirport) > 300
						bEventFlag[0] = FALSE
					ENDIF
				ENDIF
				
				IF NOT IS_CUTSCENE_ACTIVE()
					IF bEventFlag[1] AND bEventFlag[2]
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveToAirport) < DEFAULT_CUTSCENE_LOAD_DIST
						IF ARE_STRINGS_EQUAL(strMrKCSHandle, csMrK_TL)
							SET_CUTSCENE_AUDIO_OVERRIDE("_TOOTHLESS")  
						ENDIF
						REQUEST_CUTSCENE("fbi_3_mcs_8")
						
						Register_Torture_Ped_Variations_For_Cutscene_Prestreaming(TRUE, FALSE, strMrKCSHandle, TRUE)
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[0], "Ped_1")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[1], "Ped_2")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[2], "Ped_3")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[3], "Ped_4")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[4], "Ped_6")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[5], "Ped_7")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[6], "Ped_8")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, pedAirport[7], "Ped_11")

					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveToAirport) > DEFAULT_CUTSCENE_UNLOAD_DIST
						IF IS_CUTSCENE_ACTIVE()
							REMOVE_CUTSCENE()
						ENDIF
					ENDIF
				ENDIF
			
				// Stream in
				IF bEventFlag[0]
					IF (NOT bEventFlag[1] OR NOT bEventFlag[2])
						
						SWITCH iAssetRequests
							CASE 0
								Load_Asset_Model(sAssetData, A_F_Y_BEVHILLS_01)
								iAssetRequests++
							BREAK
							CASE 1
								IF HAS_MODEL_LOADED(A_F_Y_BEVHILLS_01)
									Load_Asset_Model(sAssetData, A_M_M_BEVHILLS_01)
									iAssetRequests++
								ENDIF
							BREAK
							CASE 2
								IF HAS_MODEL_LOADED(A_M_M_BEVHILLS_01)
									Load_Asset_Model(sAssetData, S_F_Y_AIRHOSTESS_01)
									iAssetRequests++
								ENDIF
							BREAK
							CASE 3
								IF HAS_MODEL_LOADED(S_F_Y_AIRHOSTESS_01)
									iAssetRequests++
								ENDIF
							BREAK
						ENDSWITCH

						IF iAssetRequests >= 4
						
							IF NOT bEventFlag[1]
								pedAirport[0] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEVHILLS_01, <<-1031.32, -2735.31, 19.22>>)
								SET_PED_KEEP_TASK(pedAirport[0], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[0], "Ped_1")
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,1), 0, 0, 0) //(berd)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,4), 23, 0, 0) //(lowr)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,6), 10, 0, 0) //(feet)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
								SET_PED_COMPONENT_VARIATION(pedAirport[0], INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
								
								
							
								pedAirport[1] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEVHILLS_01, <<-1031.93, -2739.68, 19.66>>)
								SET_PED_KEEP_TASK(pedAirport[1], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[1], "Ped_2")
							
								pedAirport[2] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEVHILLS_01, <<-1033.03, -2735.05, 19.17>>)
								SET_PED_KEEP_TASK(pedAirport[2], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[2], "Ped_3")
								SET_PED_COMPONENT_VARIATION(pedAirport[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
								SET_PED_COMPONENT_VARIATION(pedAirport[2], INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
								SET_PED_COMPONENT_VARIATION(pedAirport[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(pedAirport[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
								SET_PED_COMPONENT_VARIATION(pedAirport[2], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
								SET_PED_COMPONENT_VARIATION(pedAirport[2], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
							
								pedAirport[3] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEVHILLS_01, <<-1042.16, -2729.93, 19.17>>)
								SET_PED_KEEP_TASK(pedAirport[3], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[3], "Ped_4")
								SET_PED_COMPONENT_VARIATION(pedAirport[3], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
								SET_PED_COMPONENT_VARIATION(pedAirport[3], INT_TO_ENUM(PED_COMPONENT,2), 4, 2, 0) //(hair)
								SET_PED_COMPONENT_VARIATION(pedAirport[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(pedAirport[3], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
								SET_PED_COMPONENT_VARIATION(pedAirport[3], INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
								SET_PED_COMPONENT_VARIATION(pedAirport[3], INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
							
								pedAirport[4] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_AIRHOSTESS_01, <<-1040.61, -2730.07, 19.17>>)
								SET_PED_KEEP_TASK(pedAirport[4], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[4], "Ped_6")
							
								pedAirport[5] = CREATE_PED(PEDTYPE_MISSION, A_M_M_BEVHILLS_01, <<-1030.32, -2738.53, 19.17>>)
								SET_PED_KEEP_TASK(pedAirport[5], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[5], "Ped_7")
							
								pedAirport[6] = CREATE_PED(PEDTYPE_MISSION, A_M_M_BEVHILLS_01, <<-1035.18, -2740.79, 19.17>>)
								SET_PED_KEEP_TASK(pedAirport[6], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[6], "Ped_8")
							
								pedAirport[7] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_AIRHOSTESS_01, <<-1046.80, -2735.29, 19.59>>)
								SET_PED_KEEP_TASK(pedAirport[7], TRUE)
								SET_PED_NAME_DEBUG(pedAirport[7], "Ped_11")
								SET_PED_COMPONENT_VARIATION(pedAirport[7], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
								SET_PED_COMPONENT_VARIATION(pedAirport[7], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
								SET_PED_COMPONENT_VARIATION(pedAirport[7], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
								SET_PED_COMPONENT_VARIATION(pedAirport[7], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)

								bEventFlag[1] = TRUE
								
							ELIF NOT bEventFlag[2]
							
								bEventFlag[2] = TRUE
								
								IF NOT IS_PED_INJURED(pedAirport[0])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1031.32, -2735.31, 19.22>>, "WORLD_HUMAN_SMOKING", 0.25, FALSE)				// ped 1
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[0], <<-1031.32, -2735.31, 19.22>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[1])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1031.93, -2739.68, 19.66>>, "PROP_HUMAN_SEAT_CHAIR", 0.25, FALSE)			// ped 2
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[1], <<-1031.93, -2739.68, 19.66>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[2])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1033.03, -2735.05, 19.17>>, "WORLD_HUMAN_HANG_OUT_STREET", 0.25, FALSE)		// ped 3
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[2], <<-1033.03, -2735.05, 19.17>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[3])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1042.16, -2729.93, 19.17>>, "WORLD_HUMAN_DRINKING", 0.25, FALSE)				// ped 4
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[3], <<-1042.16, -2729.93, 19.17>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[4])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1040.61, -2730.07, 19.17>>, "WORLD_HUMAN_LEANING", 0.25, FALSE)				// ped 6
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[4], <<-1040.61, -2730.07, 19.17>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[5])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1030.32, -2738.53, 19.17>>, "WORLD_HUMAN_DRINKING", 0.25, FALSE)				// ped 7
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[5], <<-1030.32, -2738.53, 19.17>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[6])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1035.18, -2740.79, 19.17>>, "WORLD_HUMAN_DRINKING", 0.25, FALSE)				// ped 8
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[6], <<-1035.18, -2740.79, 19.17>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
								IF NOT IS_PED_INJURED(pedAirport[7])
								AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedAirport[7], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
								AND DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1046.80, -2735.29, 19.59>>, "PROP_HUMAN_SEAT_CHAIR", 0.25, FALSE)			// ped 11
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[7], <<-1046.80, -2735.29, 19.59>>, 0.25, -1)
									bEventFlag[2] = FALSE
								ENDIF
							ENDIF
							
						ENDIF
							
					ENDIF
				// Stream out
				ELSE
					
					Unload_Asset_Model(sAssetData, A_F_Y_BEVHILLS_01)
					Unload_Asset_Model(sAssetData, A_M_M_BEVHILLS_01)
					Unload_Asset_Model(sAssetData, S_F_Y_AIRHOSTESS_01)
					
					IF DOES_ENTITY_EXIST(pedAirport[0])
						DELETE_PED(pedAirport[0])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[1])
						DELETE_PED(pedAirport[1])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[2])
						DELETE_PED(pedAirport[2])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[3])
						DELETE_PED(pedAirport[3])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[4])
						DELETE_PED(pedAirport[4])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[5])
						DELETE_PED(pedAirport[5])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[6])
						DELETE_PED(pedAirport[6])
					ENDIF
					IF DOES_ENTITY_EXIST(pedAirport[7])
						DELETE_PED(pedAirport[7])
					ENDIF
					
					bEventFlag[1] = FALSE
					bEventFlag[2] = FALSE
					bEventFlag[3] = FALSE
					
				ENDIF
			
				// Reached destination
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(	sLocatesData, vDriveToAirport,<<5.062500,4.812500,2.812500>>, TRUE, pedVictim, vehTrevor,
																godDriveToAirport, "", "", "CMN_GENGETBCKYT",
																TRUE, TRUE, TRUE)
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					KILL_FACE_TO_FACE_CONVERSATION()
					i_mission_substage++
				
				ELSE
				
					// Drive conversations
					SWITCH iWeaponSelStage
						CASE 0
							// Initial trevor "you got a flight to catch" line
							IF IS_SAFE_TO_START_CONVERSATION()
								//RunConversation("F3_FLIGHT")
								iWeaponSelStage++
							ENDIF
						BREAK
						CASE 1
							// Drive banter
							IF IS_SAFE_TO_START_CONVERSATION()
								IF IS_PED_IN_VEHICLE(TREV_PED_ID(), vehTrevor)
								AND IS_PED_IN_VEHICLE(pedVictim, vehTrevor)
								AND TIMERB() >= 2000						

									RunConversation("F3_DRV3", "", TRUE)
									
									IF sba_airport != NULL
										REMOVE_SCENARIO_BLOCKING_AREA(sba_airport)
									ENDIF

									iWeaponSelStage++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDriveToAirport) < 100
								IF NOT IS_SAFE_TO_START_CONVERSATION()
									KILL_FACE_TO_FACE_CONVERSATION()
								ELSE
									RunConversation("F3_AIRPORT")
									iWeaponSelStage++
								ENDIF
							ELSE
								// Pause convo if 1 of the peds is out of the vehicle
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_PED_IN_VEHICLE(TREV_PED_ID(), vehTrevor)
									OR NOT IS_PED_IN_VEHICLE(pedVictim, vehTrevor)
									OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
										PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
									ELSE
										PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
				ENDIF
			BREAK
			CASE 1
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTrevor, 4.0, 2, 0.25)
					eMissionStage = ST_12_EXIT_CUT
				ENDIF
			BREAK
		ENDSWITCH
		
		//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_11_DRIVE_TO_AIRPORT
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
	
			MissionStageTidyUp()
			
			bStageSetup = FALSE
		ENDIF
		
	ENDIF	
	
ENDPROC

//PURPOSE:		Performs the section where Trevor drives the victim to the airport
PROC stage12_ExitCutscene()

	//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup

		i_mission_substage = 0
		iWeaponSelStage = 0
		bStageSetup = TRUE
		iAssetRequests	= 0
		
	ENDIF
	
	IF bStageSetup
			
		SWITCH i_mission_substage
		
			CASE 0
				IF HAS_CUTSCENE_LOADED()
				
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				
					IF NOT IS_PED_INJURED(pedVictim)
						REGISTER_ENTITY_FOR_CUTSCENE(pedVictim, strMrKCSHandle, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(null, strMrKCSHandle_DONT_ANIMATE, CU_DONT_ANIMATE_ENTITY, CS_MRK)
					REGISTER_ENTITY_FOR_CUTSCENE(vehTrevor, csTrevorCar, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					IF NOT IS_PED_INJURED(pedAirport[0])
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[0], "Ped_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // A_F_Y_BEVHILLS_01
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(null, "Ped_1", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_F_Y_BEVHILLS_01)
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[1])
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[1], "Ped_2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // A_F_Y_BEVHILLS_01
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(null, "Ped_2", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_F_Y_BEVHILLS_01)
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[2])
						//REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[2], "Ped_3", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // A_F_Y_BEVHILLS_01
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[2], "Ped_3", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY) // A_F_Y_BEVHILLS_01
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[3])
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[3], "Ped_4", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // A_F_Y_BEVHILLS_01
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(null, "Ped_4", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_F_Y_BEVHILLS_01)
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[4])
						//REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[4], "Ped_6", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // S_F_Y_AIRHOSTESS_01
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[4], "Ped_6", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY) // S_F_Y_AIRHOSTESS_01
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[5])
						//REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[5], "Ped_7", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // A_M_M_BEVHILLS_01
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[5], "Ped_7", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[6])
						//REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[6], "Ped_8", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // A_M_Y_BEVHILLS_01
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[6], "Ped_8", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF NOT IS_PED_INJURED(pedAirport[7])
						REGISTER_ENTITY_FOR_CUTSCENE(pedAirport[7], "Ped_11", CU_ANIMATE_EXISTING_SCRIPT_ENTITY) // S_F_Y_AIRHOSTESS_01
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(null, "Ped_11", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, S_F_Y_AIRHOSTESS_01)
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					START_CUTSCENE()
					
					IF NOT IS_REPLAY_RECORDING()
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF IS_AUDIO_SCENE_ACTIVE("FBI_3_TREVOR_DRIVE_TO_AIRPORT")
						STOP_AUDIO_SCENE("FBI_3_TREVOR_DRIVE_TO_AIRPORT")
					ENDIF

					i_mission_substage++
				ENDIF
			BREAK
			CASE 1				
				IF IS_CUTSCENE_PLAYING()
				
					SAFE_FADE_IN()
		
					SET_ROADS_IN_AREA(<<-909.826843,-2519.508057,61.213654>> - <<231.750000,250.000000,49.750000>>, 
						<<-909.826843,-2519.508057,61.213654>> + <<231.750000,250.000000,49.750000>>, FALSE)
				
					IF IS_ENTITY_ON_FIRE(vehTrevor)
						STOP_ENTITY_FIRE(vehTrevor)
					ENDIF
					IF IS_ENTITY_ON_FIRE(TREV_PED_ID())
						STOP_ENTITY_FIRE(TREV_PED_ID())
					ENDIF
					IF IS_ENTITY_ON_FIRE(pedVictim)
						STOP_ENTITY_FIRE(pedVictim)
					ENDIF
					CLEAR_AREA_OF_PEDS(<<-1036.71, -2731.66, 19.17>>, 5.0)
					CLEAR_AREA_OF_VEHICLES(<<-950.40, -2561.11, 12.83>>, 250.0)
					CLEAR_AREA_OF_PROJECTILES(<<-1032.90, -2731.43, 19.04>>, 30.0)
					REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(vehTrevor), 15.0)
					
					// Sort the doors on trevors truck
					SET_VEHICLE_DOOR_CONTROL(vehTrevor, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
					SET_VEHICLE_DOOR_CONTROL(vehTrevor, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
					
					i_mission_substage++
				ENDIF
			BREAK
			CASE 2
				IF NOT DOES_ENTITY_EXIST(pedAirport[0])
					IF DOES_CUTSCENE_ENTITY_EXIST("Ped_1")
						pedAirport[0] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ped_1"))
					ENDIF
				ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_1")
					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_1")
					IF NOT IS_PED_INJURED(pedAirport[0])
						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1031.32, -2735.31, 19.22>>, "WORLD_HUMAN_SMOKING", 0.25, FALSE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[0], <<-1031.32, -2735.31, 19.22>>, 0.25, -1)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[0])
							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
						ELSE
							CPRINTLN(DEBUG_MIKE, "		Failed to find scneario")
						ENDIF
					ENDIF
					
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedAirport[1])
					IF DOES_CUTSCENE_ENTITY_EXIST("Ped_2")
						pedAirport[0] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ped_2"))
					ENDIF
				ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_2")
					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_2")
					
					IF NOT IS_PED_INJURED(pedAirport[1])
						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1031.93, -2739.68, 19.66>>, "PROP_HUMAN_SEAT_CHAIR", 0.25, FALSE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[1], <<-1031.93, -2739.68, 19.66>>, 0.25, -1)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[1])
							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
						ELSE
							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
						ENDIF
					ENDIF

				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_3")
//					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_3")
//					
//					IF NOT IS_PED_INJURED(pedAirport[2])
//						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1033.03, -2735.05, 19.17>>, "WORLD_HUMAN_HANG_OUT_STREET", 0.25, FALSE)		// ped 3
//							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[2], <<-1033.03, -2735.05, 19.17>>, 0.25, -1)
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[2])
//							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
//						ELSE
//							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
//						ENDIF
//					ENDIF
//				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedAirport[3])
					IF DOES_CUTSCENE_ENTITY_EXIST("Ped_4")
						pedAirport[3] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ped_4"))
					ENDIF
				ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_4")
					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_4")
					IF NOT IS_PED_INJURED(pedAirport[3])
						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1042.16, -2729.93, 19.17>>, "WORLD_HUMAN_DRINKING", 0.25, FALSE)				// ped 4
							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[3], <<-1042.16, -2729.93, 19.17>>, 0.25, -1)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[3])
							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
						ELSE
							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
						ENDIF
					ENDIF
				ENDIF
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_6")
//					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_6")
//					IF NOT IS_PED_INJURED(pedAirport[4])
//						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1040.61, -2730.07, 19.17>>, "WORLD_HUMAN_LEANING", 0.25, FALSE)				// ped 6
//							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[4], <<-1040.61, -2730.07, 19.17>>, 0.25, -1)
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[4])
//							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
//						ELSE
//							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
//						ENDIF
//					ENDIF
//				ENDIF
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_7")
//					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_7")
//					IF NOT IS_PED_INJURED(pedAirport[5])
//						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1030.32, -2738.53, 19.17>>, "WORLD_HUMAN_DRINKING", 0.25, FALSE)				// ped 7
//							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[5], <<-1030.32, -2738.53, 19.17>>, 0.25, -1)
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[5])
//							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
//						ELSE
//							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
//						ENDIF
//					ENDIF
//				ENDIF
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_8")
//					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_8")
//					IF NOT IS_PED_INJURED(pedAirport[6])
//						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1034.21, -2740.14, 19.17>>, "WORLD_HUMAN_DRINKING", 0.25, FALSE)				// ped 8
//							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[6], <<-1034.21, -2740.14, 19.17>>, 0.25, -1)
//							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[6])
//							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
//						ELSE
//							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
//						ENDIF
//					ENDIF
//				ENDIF
				IF NOT DOES_ENTITY_EXIST(pedAirport[7])
					IF DOES_CUTSCENE_ENTITY_EXIST("Ped_11")
						pedAirport[7] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Ped_11"))
					ENDIF
				ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Ped_11")
					CPRINTLN(DEBUG_MIKE, "PED EXITED: Ped_11")
					IF NOT IS_PED_INJURED(pedAirport[7])
						IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<-1046.80, -2735.29, 19.59>>, "PROP_HUMAN_SEAT_CHAIR", 0.25, FALSE)			// ped 11
							TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(pedAirport[7], <<-1046.80, -2735.29, 19.59>>, 0.25, -1)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAirport[7])
							CPRINTLN(DEBUG_MIKE, "		Set into scneario")
						ELSE
							CPRINTLN(DEBUG_MIKE, "		Failed to find scenario")
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF IS_REPLAY_RECORDING()
						REPLAY_STOP_EVENT()
					ENDIF
				
					bCamExited = TRUE
				ENDIF
				
				IF bCamExited
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csTrevorCar)
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehTrevor, TRUE, FALSE, FALSE)
					SET_VEHICLE_DOOR_SHUT(vehTrevor, SC_DOOR_FRONT_LEFT, 	TRUE)
					SET_VEHICLE_DOOR_SHUT(vehTrevor, SC_DOOR_FRONT_RIGHT, 	TRUE)
					SET_VEHICLE_ENGINE_ON(vehTrevor, TRUE, TRUE) 
				ENDIF
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(csTrevor)
				
					IF sba_airport != NULL
						REMOVE_SCENARIO_BLOCKING_AREA(sba_airport)
					ENDIF
				
					CLEAR_PED_NON_CREATION_AREA()
					
					SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1038.279907,-2738.568359,21.232233>>-<<97.250000,69.500000,2.312500>>, 
								<<-1038.279907,-2738.568359,21.232233>>+<<97.250000,69.500000,2.312500>>)
					
					SET_ROADS_BACK_TO_ORIGINAL(<<-909.826843,-2519.508057,61.213654>> - <<231.750000,250.000000,49.750000>>, 
						<<-909.826843,-2519.508057,61.213654>> + <<231.750000,250.000000,49.750000>>)
				
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
				
					SET_PED_INTO_VEHICLE(TREV_PED_ID(), vehTrevor)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV_PED_ID())
					
					REMOVE_SCENARIO_BLOCKING_AREA(sba_airport)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadNodeSpeedZone[0])

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
					eMissionStage = ST_13_PASSED_MISSION

				ENDIF
			BREAK
		ENDSWITCH
			
//═════════╡ CLEANUP ╞═════════
		IF eMissionStage != ST_12_EXIT_CUT
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			
			IF iRoadNodeSpeedZone[0] != -1
				REMOVE_ROAD_NODE_SPEED_ZONE(iRoadNodeSpeedZone[0])
				iRoadNodeSpeedZone[0] = -1
			ENDIF
	
			MissionStageTidyUp()
			
			bStageSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Contains the mission flow control structure
PROC MissionFlow()

	bPlayedTortureSceneLastFrame = bPlayedTortureSceneThisFrame
	bPlayedTortureSceneThisFrame = FALSE

	SWITCH eMissionStage
		CASE ST_0_DRIVE_TO_START				stage0_DriveToStart()				BREAK
		CASE ST_1_INTRO_CUT						stage1_IntroCut()					BREAK
		CASE ST_2_DRIVE_TO_COORD_FIRST			stage2_DriveToCoordFirst()			BREAK 
		CASE ST_3_TORTURE_FOR_FIRST				stage3_TortureForFirst()			BREAK
		CASE ST_4_DRIVE_TO_COORD_SECOND			stage4_DriveToCoordSecond()			BREAK
		CASE ST_5_TORTURE_FOR_SECOND			stage5_TortureForSecond()			BREAK
		CASE ST_6_REACT_TO_SECOND				stage6_ReactToSecond() 				BREAK
		CASE ST_7_TORTURE_FOR_THIRD				stage7_TortureForThird()			BREAK
		CASE ST_8_REACT_TO_THIRD				stage8_ReactToThird()				BREAK
		CASE ST_9_TORTURE_FINAL					stage9_TortureFinal()				BREAK
		CASE ST_10_REACT_FINAL					stage10_ReactFinal()				BREAK
		CASE ST_11_DRIVE_TO_AIRPORT 			stage11_DriveToAirport()			BREAK
		CASE ST_12_EXIT_CUT						stage12_ExitCutscene()				BREAK
		CASE ST_13_PASSED_MISSION
			IF NOT bStageSetup 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				SAFE_FADE_IN()
				bStageSetup = TRUE
			ELSE
				IF IS_SCREEN_FADED_IN()
					MissionPassed()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	

	IF NOT DOES_CAM_EXIST(camTorture)
	OR NOT IS_CAM_ACTIVE(camTorture)
	// bug 1890635
	//OR NOT IS_CAM_RENDERING(camTorture)
		IF IS_SCRIPT_GLOBAL_SHAKING()
			STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
		ENDIF
	ENDIF
		
// bug 1890635
//	ELSE		
//		IF NOT IS_SCRIPT_GLOBAL_SHAKING()
//			ANIMATED_SHAKE_SCRIPT_GLOBAL("SHAKE_CAM_medium", "medium", "", fTortureShake)
//		ENDIF
//	ENDIF

DUMMY_REFERENCE_FLOAT(fTortureShake)
	
	IF bDisplayHeartMonitor
		TORTURE_ManageHeartbeat()
		TORTURE_ManagePadVibration()
		TORTURE_ManageSound()
	ENDIF
	
	DISPLAY_CHECK()
	
	MANAGE_MR_K_VARIATIONS()
	
ENDPROC


//═════════════════════════════════╡ MAIN SCRIPT ╞═══════════════════════════════════

SCRIPT
	MissionSetup()
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		eMissionFail = FAIL_FORCE_CLEANUP
		MissionFailed()
	ENDIF
	
	WHILE TRUE
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BTB")
	
		IF iNumberOfTeethPulled = 0
			strMrKCSHandle = csMrK
			strMrKCSHandle_DONT_ANIMATE = csMrK_TL
		ELSE
			strMrKCSHandle = csMrK_TL
			strMrKCSHandle_DONT_ANIMATE = csMrK
		ENDIF
	
		bDisplayHeartMonitor = FALSE
		bDisplayToothPull 	= FALSE
		bDisplaySwitchHUD 	= FALSE
		bHeartbeatFrame 	= FALSE
		
		Update_Entity_Cleanup_Queue(sEntityCleanupQueue)
		Update_Asset_Management_System(sAssetData)
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			i_time_of_last_convo = -1
		ELSE
			IF i_time_of_last_convo = -1
				i_time_of_last_convo = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		IF NOT bDoSkip
			FailCheck()
		ENDIF
		
		MissionStageSkip()
		
		IF NOT bDoSkip
			UpdateConversationStarter()
			MissionFlow()
		ENDIF
		
		#IF IS_DEBUG_BUILD	Debug_Update()	#ENDIF
		
		IF bMissionFailed
			IF NOT bUpdateMissionFailed
				MissionFailed()
			ELSE
				UpdateMissionFailed()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K)
			IF DOES_ENTITY_EXIST(peds[mpf_target].id)
			AND NOT IS_PED_INJURED(peds[mpf_target].id)
				SET_ENTITY_HEALTH(peds[mpf_target].id, 0)
			ENDIF
		ENDIF
		#ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
