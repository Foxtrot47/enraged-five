
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "getaway_vehicle_include.sch"
INT iStoppedTime = -1

/// PURPOSE:
///    Checks to see if the player has the phone cursor over one of the available contacts
/// RETURNS:
///    TRUE if the player is highlighting one of the available contacts
FUNC BOOL IS_PLAYER_HIGHLIGHTING_CONTACT()
	IF SAFE_IS_CONTACT_LIST_ON_SCREEN()

		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF eHighlighted = CHAR_TREVOR
			OR eHighlighted = CHAR_FRANKLIN
				#IF IS_DEBUG_BUILD 
					IF eHighlighted = CHAR_TREVOR
						SK_PRINT("The player is highlighting CHAR_TREV, can be rang to leave the getaway") 
					ELIF eHighlighted = CHAR_FRANKLIN
						SK_PRINT("The player is highlighting CHAR_FRANK, can be rang to leave the getaway") 
					ENDIF
				#ENDIF
				RETURN TRUE
//			ELSE
//				#IF IS_DEBUG_BUILD
//					IF (GET_GAME_TIMER() - iGenericPrintTimer) > 1500
//						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_SHARM_STEALTH, "Phone is on screen but player is not highlighting correct contact") #ENDIF
//					ENDIF
//				#ENDIF
			ENDIF
		ELSE
			IF eHighlighted = CHAR_MICHAEL
				#IF IS_DEBUG_BUILD SK_PRINT("The player is highlighting Michael, can be rang to leave the getaway") #ENDIF
				RETURN TRUE
//			ELSE
//				#IF IS_DEBUG_BUILD
//					IF (GET_GAME_TIMER() - iGenericPrintTimer) > 1500
//						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_SHARM_STEALTH, "Phone is on screen but player is not highlighting correct contact") #ENDIF
//					ENDIF
//				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_PLAYER_TO_CLOSE_TO_FBI_LOCS()
	IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<855.72577, -2344.49072, 29.33145>>) <= 230*230) //HIEST location
		IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
			SET_HELP_TEXT_BIT(HTE_LOC_SEC_ROUTE)
		ENDIF
//		PRINT_HELP("AM_H_FBIC6A", DEFAULT_HELP_TEXT_TIME)//This location isn't discrete enough.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "TOO CLOSE TO HIEST location RETURN TRUE")
		#ENDIF
		RETURN TRUE
		
	ELIF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1383.21021, -2068.01978, 51.87057>>) <= 400*400) //FBI LOT
		IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
			SET_HELP_TEXT_BIT(HTE_LOC_FIB_LOC)
		ENDIF
//		PRINT_HELP("AM_H_FBIC6B", DEFAULT_HELP_TEXT_TIME)//This location isn't discrete enough.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "TOO CLOSE TO FBI LOT() RETURN TRUE")
		#ENDIF
		RETURN TRUE
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs all checks required to decide if the vehicle is suitable to use as a vehicle
/// PARAMS:
///    Veh - the vehicle perfore the checks on
/// RETURNS:
///    TRUE if the current vehicle is valid
FUNC BOOL IS_CURRENT_VEHICLE_VALID(VEHICLE_INDEX Veh)
	IF IS_VEHICLE_ROAD_VEHICLE()
	AND DO_VALID_GETAWAY_CAR_CHECK(Veh)
		RETURN TRUE 
	ENDIF
	
	RETURN FALSE
ENDFUNC

//SET_PED_ALLOW_MINOR_REACTIONS_AS_MISSION_PED

PROC MONITOR_HIDING_PLACE_VALID()

	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
	AND g_OnMissionState = MISSION_TYPE_OFF_MISSION
	AND NOT IS_MISSION_LEADIN_ACTIVE()
		IF eCopMonitor = CM_MONITER
			IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPotentialHidingPos, 5)
					IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE)) ///
					OR IS_PLAYER_HIGHLIGHTING_CONTACT()
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
						AND NOT IS_BIT_SET(iHelpTextBit[0],  ENUM_TO_INT(HTE_VEH_CAN_BE_USED_FRANK_TREV))
						AND NOT IS_BIT_SET(iHelpTextBit[1],  ENUM_TO_INT(HTE_VEH_CAN_BE_USED_MIKE))
							IF IS_PED_STOPPED(PLAYER_PED_ID())
								CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_VEH_STOP)
								#IF IS_DEBUG_BUILD SK_PRINT("Player is stopped start vehicle checks") #ENDIF
								IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
									IF NOT IS_PLAYER_TO_CLOSE_TO_FBI_LOCS()
									AND IS_AREA_QUIET(GET_ENTITY_COORDS(viVeh))
										IF IS_AREA_CLEAR_AROUND_LOCATION_VEHICLE(viVeh)
											IF IS_VEHICLE_UPRIGHT_AND_LEVEL(viVeh)
												SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
												IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
													SET_HELP_TEXT_BIT(HTE_LOC_SUITABLEFT)
													CLEAR_ALL_HELP_BITS(HTE_LOC_SUITABLEFT, TRUE)
												ELSE
													SET_HELP_TEXT_BIT(HTE_LOC_SUITABLEM)
													CLEAR_ALL_HELP_BITS(HTE_LOC_SUITABLEM, TRUE)													
												ENDIF
												vPotentialHidingPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
												EXIT
											ENDIF
										ENDIF
									ENDIF

									IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
										CLEAR_ALL_HELP_BITS(NUM_HELP)
										CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
										iStoppedTime = -1
									ENDIF
									vPotentialHidingPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
								ENDIF
								
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_LOC_SUITABLEFT, HTE_LOC_SUITABLEM)
									IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
										CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
										iStoppedTime = -1
									ELSE
										SET_HELP_TEXT_BIT(HTE_VEH_STOP)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT SAFE_IS_CONTACT_LIST_ON_SCREEN()
							IF IS_PED_STOPPED(PLAYER_PED_ID())
							AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							AND NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE)) ///
								IF iStoppedTime != -1
									IF (GET_GAME_TIMER() - iStoppedTime) > 1000
										SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
									ENDIF
								ELSE
									iStoppedTime = GET_GAME_TIMER()
								ENDIF
							ELSE
								CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
						vPotentialHidingPos = <<0,0,0>>
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPotentialHidingPos, 10)
						#IF IS_DEBUG_BUILD SK_PRINT("Player is 10m away from the vPotentialHidingPos clearing flag")  #ENDIF
						vPotentialHidingPos = <<0,0,0>>
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
						iStoppedTime = -1
					ELIF NOT IS_PED_STOPPED(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE)) ///
								SET_HELP_TEXT_BIT(HTE_VEH_STOP)
							ENDIF
							CLEAR_ALL_HELP_BITS(HTE_VEH_STOP, TRUE)
							CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
							CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
							iStoppedTime = -1
							vPotentialHidingPos = <<0,0,0>>
							#IF IS_DEBUG_BUILD SK_PRINT("Player started moving whilst ringing") #ENDIF
						ENDIF
					ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_OK(viVeh)
							IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != viVeh
								CLEAR_ALL_HELP_BITS(NUM_HELP)
								HANG_UP_AND_PUT_AWAY_PHONE()
								CLEAR_CAR_RELATED_BITS()
								vPotentialHidingPos = <<0,0,0>>
								CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
								iStoppedTime = -1
								#IF IS_DEBUG_BUILD SK_PRINT("got in to a different car whilst ringing") #ENDIF
							ENDIF
						ENDIF
					ELIF NOT IS_AREA_CLEAR_AROUND_LOCATION_VEHICLE(viVeh)
						CLEAR_ALL_HELP_BITS(HTE_LOC_PUBLIC, TRUE)
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
						vPotentialHidingPos = <<0,0,0>>
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TIME_OUT_HIDING_PLACE))
						iStoppedTime = -1
						#IF IS_DEBUG_BUILD SK_PRINT("ped walked passed") #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC




PROC MONITOR_PLAYERS_CAR()
	IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
	AND eCopMonitor = CM_MONITER
	AND NOT IS_MISSION_LEADIN_ACTIVE()
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT MONITOR_PLAYER_CARGO_BOBBING_CAR()
						IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
							VEHICLE_INDEX viPlayersVeh = GET_PLAYERS_LAST_VEHICLE()
							IF IS_VEHICLE_OK(viPlayersVeh)
								IF NOT HAS_VEHICLE_BEEN_CHECKED_BEFORE(viPlayersVeh)
									#IF IS_DEBUG_BUILD SK_PRINT("3") #ENDIF
									IF NOT DECOR_EXIST_ON(viPlayersVeh, "Getaway_Winched")
										IF IS_CURRENT_VEHICLE_VALID(viPlayersVeh)
										AND NOT IS_PLAYER_PASSENGER_IN_TAXI()
											IF NOT IS_ENTITY_A_MISSION_ENTITY(viPlayersVeh)
												#IF IS_DEBUG_BUILD SK_PRINT("bIsCarValid = TRUE 1 ") #ENDIF
												IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
													SET_HELP_TEXT_BIT(HTE_VEH_CAN_BE_USED_FRANK_TREV)
												ELSE
													SET_HELP_TEXT_BIT(HTE_VEH_CAN_BE_USED_MIKE)
												ENDIF
												viVeh = viPlayersVeh
												SET_ENTITY_AS_MISSION_ENTITY(viVeh, TRUE, TRUE)
												IF DECOR_SET_BOOL(viVeh, "GetawayVehicleValid", TRUE)
													#IF IS_DEBUG_BUILD SK_PRINT("ADDED decor - GetawayVehicleValid = TRUE") #ENDIF
												ENDIF
												SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
												SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
												SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
												EXIT
											ELSE
												#IF IS_DEBUG_BUILD 
													IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
														SK_PRINT("BISCARVALID = FALSE 1 ") 
													ENDIF
													CPRINTLN(DEBUG_MISSION, "Player entered mission vehicle, which was then checked by the getaway vehicle script", GET_THIS_SCRIPT_NAME())
												#ENDIF

												#IF IS_DEBUG_BUILD SK_PRINT("MONITOR_PLAYERS_CAR IS_ENTITY_A_MISSION_ENTITY(viPlayersVeh)- SAFE_RELEASE_VEHICLE(VIVEH)") #ENDIF
												SAFE_RELEASE_VEHICLE(viVeh)
												#IF IS_DEBUG_BUILD SK_PRINT("bHasCurrentVehicleBeenChecked = TRUE 3 ") #ENDIF
												CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
												CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
												EXIT
											ENDIF
											
										ENDIF
									ENDIF
									IF DECOR_SET_BOOL(viPlayersVeh, "GetawayVehicleValid", FALSE)
										#IF IS_DEBUG_BUILD SK_PRINT("ADDED decor - GetawayVehicleValid = FALSE ") #ENDIF
									ENDIF
									SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
									#IF IS_DEBUG_BUILD SK_PRINT("bHasCurrentVehicleBeenChecked = TRUE 1 ") #ENDIF
								ELSE
									IF IS_PREVIOUSLY_CHECKED_VEHICLE_VALID(viPlayersVeh)
										#IF IS_DEBUG_BUILD SK_PRINT("bIsCarValid = TRUE 2 ") #ENDIF
										viVeh = viPlayersVeh
										SET_ENTITY_AS_MISSION_ENTITY(viVeh, TRUE, TRUE)
										SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
										SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
										SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
										EXIT
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD 
									IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
										SK_PRINT("bIsCarValid = FALSE 1 ") 
									ENDIF
								#ENDIF

								#IF IS_DEBUG_BUILD SK_PRINT("MONITOR_PLAYERS_CAR 1  - SAFE_RELEASE_VEHICLE(viVeh)") #ENDIF
								SAFE_RELEASE_VEHICLE(viVeh)
								#IF IS_DEBUG_BUILD SK_PRINT("bHasCurrentVehicleBeenChecked = TRUE 2 ") #ENDIF
								SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
								CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
							ENDIF
						ELSE
							IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
								IF NOT IS_VEHICLE_OK(viVeh)
									#IF IS_DEBUG_BUILD SK_PRINT("VEHICLE is dead reset bits ") #ENDIF
									SAFE_RELEASE_VEHICLE(viVeh)
									SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
									SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
									SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
						VEHICLE_INDEX viPlayersVeh = GET_PLAYERS_LAST_VEHICLE()
						IF IS_VEHICLE_OK(viPlayersVeh)
							CPRINTLN(DEBUG_MISSION, "GET_PLAYERS_LAST_VEHICLE() is valid in GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE")
							IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viPlayersVeh, 10)
							AND NOT DECOR_EXIST_ON(viPlayersVeh, "Getaway_Winched")
								CPRINTLN(DEBUG_MISSION, "Player has returned to a vaild vehicle, grabbing vehicle")
								viVeh = viPlayersVeh
								SET_ENTITY_AS_MISSION_ENTITY(viVeh, TRUE, TRUE)
								SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
								SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD 
							IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
								SK_PRINT("bHasCurrentVehicleBeenChecked = FALSE 1 ") 
							ENDIF
						#ENDIF
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_OK(viVeh)
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viVeh, 80)
						#IF IS_DEBUG_BUILD 
							IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
								SK_PRINT("bIsCarValid = FALSE 2 ") 
							ENDIF
						#ENDIF

						IF NOT DECOR_EXIST_ON(viVeh, "Getaway_Winched") 
							#IF IS_DEBUG_BUILD SK_PRINT("MONITOR_PLAYERS_CAR 2 - SAFE_RELEASE_VEHICLE(viVeh)") #ENDIF
							SET_BIT(iHelpTextBit[GET_HELP_BIT_ID(ENUM_TO_INT(HTE_LEFT_CAR))], ENUM_TO_INT(HTE_LEFT_CAR))
							SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
						ELSE
							CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
						ENDIF

						SAFE_RELEASE_VEHICLE(viVeh)
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
					ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != viVeh
							#IF IS_DEBUG_BUILD
								IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
									SK_PRINT("bIsCarValid = FALSE 3 ") 
								ENDIF
							#ENDIF
							#IF IS_DEBUG_BUILD SK_PRINT("MONITOR_PLAYERS_CAR 3 - SAFE_RELEASE_VEHICLE(viVeh)") #ENDIF
							SAFE_RELEASE_VEHICLE(viVeh)
							CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_VEH_CAN_BE_USED_FRANK_TREV)
							CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_VEH_CAN_BE_USED_MIKE)
							CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_UNSUITABLE_VEH)
							#IF IS_DEBUG_BUILD SK_PRINT("bHasCurrentVehicleBeenChecked = FALSE 1 ") #ENDIF
							CLEAR_CAR_RELATED_BITS()
						ELSE
							IF MONITOR_PLAYER_CARGO_BOBBING_CAR()
								#IF IS_DEBUG_BUILD
									IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
										SK_PRINT("bIsCarValid = FALSE 3 ") 
									ENDIF
								#ENDIF
								#IF IS_DEBUG_BUILD SK_PRINT("MONITOR_PLAYERS_CAR 3 - SAFE_RELEASE_VEHICLE(viVeh)") #ENDIF
								SAFE_RELEASE_VEHICLE(viVeh)

								#IF IS_DEBUG_BUILD SK_PRINT("bHasCurrentVehicleBeenChecked = FALSE 1 ") #ENDIF
								CLEAR_CAR_RELATED_BITS()
							ENDIF
						ENDIF
					ELIF NOT IS_GETAWAY_VEHICLE_HEALTHY(viVeh)
						IF DECOR_SET_BOOL(viVeh, "GetawayVehicleValid", FALSE)
							#IF IS_DEBUG_BUILD SK_PRINT("ADDED decor - GetawayVehicleValid HEALTH = FALSE ") #ENDIF
						ENDIF

						CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_VEH_CAN_BE_USED_FRANK_TREV)
						CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HTE_VEH_CAN_BE_USED_MIKE)
						SAFE_RELEASE_VEHICLE(viVeh)
						CLEAR_CAR_RELATED_BITS()
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
							SK_PRINT("bIsCarValid = FALSE 4 ")
						ENDIF
					#ENDIF
					#IF IS_DEBUG_BUILD SK_PRINT("MONITOR_PLAYERS_CAR 4 - SAFE_RELEASE_VEHICLE(viVeh)") #ENDIF
					SAFE_RELEASE_VEHICLE(viVeh)
					CLEAR_CAR_RELATED_BITS()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_CAR_BEEN_PLACED()
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
//		VEHICLE_INDEX viPlayersVeh = GET_PLAYERS_LAST_VEHICLE()
		IF IS_VEHICLE_OK(viVeh)
			IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
				
				IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
				OR IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
				OR IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
					CLEAR_ALL_HELP_BITS(NUM_HELP, TRUE)
					GRAB_CAR_PLACED_INFO()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC WAIT_PLACE_FIRST_ITEM()
	MONITOR_RESTRICTED_AREAS()
	MONITOR_PLAYERS_CAR()
	MONITOR_HIDING_PLACE_VALID()
	MONITOR_PHONE_CALL()
	MONITOR_CONTACTS()

	IF HAS_CAR_BEEN_PLACED()
		MISSION_STATE eNextState
		IF PICK_PHONE_CALL_AND_NEXT_STATE(eNextState)
				
			CONTACT_WATCH(FALSE)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PED_STOPPED(PLAYER_PED_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())

				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " Telling the player to get out of the getaway vehicle after phone call") #ENDIF
			ENDIF
			
			IF IS_VEHICLE_OK(viVeh)
				SET_VEHICLE_DOORS_LOCKED(viVeh, VEHICLELOCK_LOCKED)
			ENDIF

			UPDATE_SAVED_STATE(eNextState)
		ENDIF
	ENDIF
	
ENDPROC

PROC MONITOR_GETAWAY()
	MAINTAIN_GETAWAY_VEHICLE()
	MONITOR_PHONE_CALL()
ENDPROC


PROC MONITOR_PLAYER_ON_FIB4()
	IF MISSION_FLOW_GET_RUNNING_MISSION() = SP_MISSION_FBI_4
		IF IS_REPLAY_IN_PROGRESS()
		AND NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, VEHGEN_S_FLAG_AVAILABLE)
			IF mDroppedOffCarStruct.eModel != DUMMY_MODEL_FOR_SCRIPT
				CPRINTLN(DEBUG_MISSION, "REINITIALISING - Vehicle gen after replay from ", GET_THIS_SCRIPT_NAME())
				UPDATE_DYNAMIC_VEHICLE_GEN_DATA(VEHGEN_MISSION_VEH_FBI4_PREP, mDroppedOffCarStruct, vCarPosition, fCarHeading)
			ENDIF
		ENDIF
	ELIF MISSION_FLOW_GET_RUNNING_MISSION() = SP_MISSION_NONE
		IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPotentialHidingPos, 5)
				IF IS_PLAYER_HIGHLIGHTING_CONTACT()
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						IF IS_FRIEND_ACTIVITY_SCRIPT_RUNNING()
							SET_HELP_TEXT_BIT(HTE_ON_FRIEND_ACT)
						ELIF g_bTaxiProceduralRunning
							SET_HELP_TEXT_BIT(HTE_ON_TAXI_MISSION)
						ELSE
							SET_HELP_TEXT_BIT(HTE_ON_MISSION)
						ENDIF	
						eHelpTextState = HTS_DISPLAY
						fHelpDisplayedFor = 0.0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_ON_MISSION_CHECKS()
	MONITOR_PLAYER_MOVING_VEHICLE_ON_MISSION()
	MONITOR_PLAYER_ON_FIB4()
ENDPROC

PROC WAIT_ON_MISSION()
//	#IF IS_DEBUG_BUILD
//		IF iOnMissionTimerCheck != -1
//			IF (GET_GAME_TIMER() - iOnMissionTimerCheck) > 5000
//				CPRINTLN(DEBUG_SHARM_STEALTH, "In on mission state", GET_THIS_SCRIPT_NAME())
//			ENDIF
//		ENDIF
//	#ENDIF


	DO_ON_MISSION_CHECKS()
ENDPROC



SCRIPT
	IF NOT REGISTER_SCRIPT_TO_RELAUNCH_LIST(LAUNCH_BIT_FBI4_PREP3)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Loading from a save - ", GET_THIS_SCRIPT_NAME())
		#ENDIF

		eMissionState = INT_TO_ENUM(MISSION_STATE, g_savedGlobals.sAmbient.iGetawayState)
		IF eMissionState = MS_MONITOR_GETAWAY_AREA
			SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION))
			vCarPosition = GET_VEHICLE_GEN_COORDS(VEHGEN_MISSION_VEH_FBI4_PREP) //talk to kenneth
			fCarHeading  =  GET_VEHICLE_GEN_HEADING(VEHGEN_MISSION_VEH_FBI4_PREP)
			
			IF GET_VEHICLE_GEN_VEHICLE_SETUP_SAVED_DATA(mDroppedOffCarStruct, VEHGEN_MISSION_VEH_FBI4_PREP)
				IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_VEHICLE_PLACED))
					SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_VEHICLE_PLACED))
				ENDIF
				CPRINTLN(DEBUG_MISSION, "Vehicle gen vehicle setup data was found - ")
			ELSE
				SCRIPT_ASSERT("Vehicle gen data was not found for VEHGEN_MISSION_VEH_FBI4_PREP")
			ENDIF
		ENDIF

		CPRINTLN(DEBUG_MISSION, "Saved state - ", g_savedGlobals.sAmbient.iGetawayState)
	ELSE
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "First time initialisation - ", GET_THIS_SCRIPT_NAME())
		#ENDIF

		g_savedGlobals.sAmbient.iGetawayState = ENUM_TO_INT(eMissionState)
	ENDIF


	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY ))
		#IF IS_DEBUG_BUILD SK_PRINT("HAS_FORCE_CLEANUP_OCCURRED") #ENDIF
		IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_SP_TO_MP
			CONTACT_WATCH(FALSE)
	        Script_Cleanup(LAUNCH_BIT_FBI4_PREP3, TRUE)
		ELSE
	        Script_Cleanup(LAUNCH_BIT_FBI4_PREP3)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bShowDebugText
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
		ENDIF
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
		SET_UP_RAG_WIDGETS()
//		BREAK_ON_NATIVE_COMMAND("CLEAR_HELP", FALSE)
	#ENDIF

	POPULATE_STUFF(FIB4_GETAWAY)

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		#IF IS_DEBUG_BUILD
			IF (GET_GAME_TIMER() - iGenericPrintTimer) > 20000	
				 SK_PRINT("Still looping prep 3 ") 
			ENDIF
		#ENDIF
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4)
			#IF IS_DEBUG_BUILD SK_PRINT("FIB 4 HAS PASSED Clean up Getaway FIB 4 script") #ENDIF
			Script_Cleanup(LAUNCH_BIT_FBI4_PREP3)
		ENDIF
		#IF IS_DEBUG_BUILD
			IF iGenericPrintTimer = -1
				iGenericPrintTimer = GET_GAME_TIMER()
			ELSE
				IF (GET_GAME_TIMER() - iGenericPrintTimer) > 20000	
					iGenericPrintTimer = -1
				ENDIF
			ENDIF
		#ENDIF
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			MONITOR_PLAYER_IN_MOD_SHOP()
			MONITOR_ON_MISSION_STATUS()
			MONITOR_FOR_PLAYER_SWITCH()
			MONITER_HELP_TEXT()
			SWITCH eMissionState
				CASE MS_PLACE_FIRST_ITEM
					WAIT_PLACE_FIRST_ITEM()
				BREAK
				
				CASE MS_MONITOR_GETAWAY_AREA
					MONITOR_GETAWAY()
				BREAK
				
				CASE MS_WAIT_ON_MISSION
					WAIT_ON_MISSION()
				BREAK
				
				CASE MS_DISPLAYING_SPLASH
					DISPLAY_SPLASH(FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED)
				BREAK
				
				CASE MS_DONE
					
				BREAK
				DEFAULT
					SCRIPT_ASSERT("Getaway car mission state is invalid. Add bug and include details about if loaded from a save or if there where any RE's or minigames around at the time")
				BREAK
			ENDSWITCH

			#IF IS_DEBUG_BUILD
				UPDATE_WIDGET()
				// Check debug completion/failure
				UPDATE_DEBUG_PASS()
			#ENDIF
			SAFE_MONITOR_CONTACT_HIGHLIGHT()
		ELSE
			CLEAR_ALL_HELP_BITS(NUM_HELP, TRUE)
		ENDIF
//		#IF IS_DEBUG_BUILD
//			IF (GET_GAME_TIMER() - iGenericPrintTimer) > 2000	
//				 SK_PRINT("B ") 
//			ENDIF
//		#ENDIF
		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

