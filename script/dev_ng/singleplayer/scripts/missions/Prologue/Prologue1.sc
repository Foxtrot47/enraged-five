//+-----------------------------------------------------------------------------+
//¦																				¦
//¦				Author: Alan Litobarski				Date: 06/09/2010		 	¦
//¦																				¦
//¦-----------------------------------------------------------------------------¦
//¦																				¦
//¦ 			Prologue - (Prologue1.sc)										¦
//¦ 																			¦
//¦ 			Ludendorff, North Yankton, 2003. Michael, Trevor and Brad 		¦
//¦ 			are holding up a cash depot. The mission introduces all the 	¦
//¦ 			core gameplay (walking, driving, shooting, switching, etc.)		¦
//¦ 			in a fast paced tutorial mission. At the end of the heist		¦
//¦ 			Michael is seen getting shot and assumed dead.					¦
//¦																				¦
//+-----------------------------------------------------------------------------+

//---------------------------------¦ HEADERS ¦-----------------------------------

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_object.sch"
USING "commands_task.sch"
USING "commands_misc.sch"
USING "commands_entity.sch"
USING "commands_hud.sch"
USING "commands_itemsets.sch"
USING "commands_network.sch"

USING "cellphone_public.sch"
USING "flow_public_core_override.sch"
USING "flow_public_game.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "locates_public.sch"
USING "chase_hint_cam.sch"
USING "LineActivation.sch"
USING "script_blips.sch"
USING "script_heist.sch"
USING "script_buttons.sch"
USING "replay_public.sch"
USING "cam_recording_public.sch"
USING "help_at_location.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"
USING "script_misc.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//--------------------------------¦ VARIABLES ¦----------------------------------

//Integers
CONST_INT REPLAY_VALUE_CURRENT_TAKE 		0

#IF IS_DEBUG_BUILD
FLOAT fDistIntoCover = 2.0
FLOAT fTimeIntoCover = 0.9
#ENDIF

INT iCutsceneStage

INT iDialogueStage = 0
INT iDialogueLineCount[5]
INT iDialogueTimer

INT iCopPerFrame
INT iCarPerFrame

INT iAmbientTimer

INT iHelpTimer = -1

INT iBulletTimers[6]

INT iRespawnTimers[8]

INT iFailTimer

INT iCreditsStage

ENUM BLEND_IN_TIMER
	BLEND_IN_MICHAEL,
	BLEND_IN_TREVOR,
	BLEND_IN_BRAD
ENDENUM

INT iBlendInTimer[COUNT_OF(BLEND_IN_TIMER)]

INT iCoverAnimTimer

INT iTrainHornTimer
VECTOR vTrainHornLocation

INT iAccountBalance

CONST_INT iTotalHostages 4
CONST_INT iTotalCops 42
CONST_INT iTotalCopVan 2
CONST_INT iTotalCopCar 20
CONST_INT iTotalVan 2
CONST_INT iTotalTraffic 2
CONST_INT iTotalTrain 8
CONST_INT iTotalCrate 3
CONST_INT iTotalBarrier 4
CONST_INT iTotalScreens 8

//INT iShotScreen[10] //CCTV room
//INT iShotScreenTimer[10]

INT iHostageAimTimer[iTotalHostages]

INT iStuckTimer	//Car is stuck in snow

INT iTimerCopCars	//Timer to track cop cars in cashdepot shootout

INT iDeadCopPlus

INT iSequenceBuddy, iSequenceBrad
VECTOR vSequenceBuddy, vSequenceBrad

ENUM PEDRESETFLAG_TIMER
	MICHAEL_PRF_TIMER,
	TREVOR_PRF_TIMER,
	BRAD_PRF_TIMER
ENDENUM

//INT iPedResetFlagTimer[COUNT_OF(PEDRESETFLAG_TIMER)]

//Stats
//INT iAmmo = -1
//WEAPON_TYPE wtCurrent

//Bools
BOOL bVideoRecording

BOOL bInitStage
BOOL bCleanupStage
BOOL bRadar

#IF IS_DEBUG_BUILD //Debug
	BOOL bAutoSkipping
#ENDIF

BOOL bSkipped //Used for J-skipping and Mid Mission Replay

BOOL bReplaySkip

BOOL bCutsceneSkipped	//Tracks if WAS_CUTSCENE_SKIPPED() returns TRUE

BOOL bPassed	//Passed the mission

BOOL bGarageVisible
BOOL bGrabShutter

#IF FEATURE_GEN9_EXCLUSIVE
BOOL bPlayGarageSound
#ENDIF

BOOL bSwapTrolley

BOOL bPillarCoverRight

BOOL bAudioStream	//Fix for bug 2284329

#IF IS_DEBUG_BUILD
BOOL bDebugDisplayCreditsText
BOOL bDebugClearCreditsText
FLOAT fDebugCreditsBlockX
FLOAT fDebugCreditsBlockY
TEXT_WIDGET_ID textWidgetCreditsAlign
TEXT_WIDGET_ID textWidgetText1
TEXT_WIDGET_ID textWidgetText2
TEXT_WIDGET_ID textWidgetText3
TEXT_WIDGET_ID textWidgetFont1
TEXT_WIDGET_ID textWidgetFont2
TEXT_WIDGET_ID textWidgetFont3
TEXT_WIDGET_ID textWidgetCreditsColour1
TEXT_WIDGET_ID textWidgetCreditsColour2
TEXT_WIDGET_ID textWidgetCreditsColour3

BOOL bDebugDoors
BOOL bDebugHelpText
BOOL bDebugAudio
#ENDIF

//Vectors and Floats
VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>

VECTOR vPlayerStart = <<5311.2363, -5212.5635, 85.7187 - 3.2>>
CONST_FLOAT fPlayerStart 10.6920

VECTOR vTrevorStart = <<5308.4175, -5212.3984, 85.7187 - 3.2>>
CONST_FLOAT fTrevorStart 350.4967

VECTOR vBuddyStart = <<5311.9736, -5211.5239, 85.7187 - 3.2>>
CONST_FLOAT fBuddyStart 21.6992

VECTOR vCar = <<5428.7910, -5115.1890, 77.2632>>
CONST_FLOAT fCar 100.9597

VECTOR vHostageStart[iTotalHostages]
FLOAT fHostageStart[iTotalHostages]

VECTOR vCopStart[iTotalCops]
FLOAT fCopStart[iTotalCops]

VECTOR vGaragePosition = <<5320.60, -5188.56, 82.50>>
VECTOR vGarageRotate = <<0.0, 0.0, -90.0>>

VECTOR vDoorPosition = <<5308.8574, -5208.1560, 86.9186 - 3.2 -0.05>>
VECTOR vDoorRotate = <<0.0, 0.0, 0.0>>

VECTOR vRoute[40]

VECTOR vRoutePoint1[14]
VECTOR vRoutePoint2[14]
FLOAT fRouteWidth[14]

FLOAT fSnowMarks

// Path that Brad & Michael take to get to the getaway car
VECTOR vPathToGetawayCar[3]

INT iNavMeshBlockingID1, iNavMeshBlockingID2, iNavMeshBlockingID3, iNavMeshBlockingID4
INT iNavMeshBlockingNearGarage, iNavMeshBlockingFarm, iNavMeshBlockingCopPath1, iNavMeshBlockingCopPath2

//FLOAT fAnimTotalTime, fAnimProgressTime

//Strings
TEXT_LABEL_23 txtConversationPoint

//Carrec
STRING sCarrec = "ALPrologue"

//Anim Dicts
//STRING sAnimDictSignal = "misscarsteal3"
STRING sAnimDictMapObjects = "Map_Objects"

//Animation Dictionaries
STRING sAnimDictDeepSnow = "missprologuesnow_moves"

STRING sAnimDictDeadGuard = "missprologuedead_guard"

STRING sAnimDictPrologue1 = "missprologueig_1"
STRING sAnimDictPrologue2_MCS1_GuardFacial = "facials@blend_out@static_hostage_2-pro_mcs_1_face@guard-pro_mcs_1"

STRING sAnimDictPrologue2 = "missprologueig_2"
STRING sAnimDictPrologue2_MCS1 = "missprologuemcs_1"
STRING sAnimDictPrologue2_TrevorReturn = "missprologueig_3@get_into_cover"

STRING sAnimDictPrologue3 = "missprologueig_3@react_to_explosion"
STRING sAnimDictPrologue3_Cam = "missprologueig_3"	//"missprologueig_3@react_to_explosion"

STRING sAnimDictPrologue3_Impatient = "missprologueig_3@cover_impatient"

STRING sAnimDictPrologue3_IdleAtCupboard = "missprologuebrad_alert_idle"

STRING sAnimDictPrologue3_IdleInVault = "missbigscore2big_2"

STRING sAnimDictPrologue_LeaveVault = "missprologuemcs_2_walkout_vault"

STRING sAnimDictPrologue4_Base = "missprologueig_4@hold_head_base"
STRING sAnimDictPrologue4_Shot = "missprologueig_4@wipe_blood"
STRING sAnimDictPrologue4_PlayerPause = "missprologueig_4_p2"
STRING sAnimDictPrologue4_Fail = "missprologueig_4@kill_michael"

STRING sAnimDictPrologue5_Start = "missprologueig_5@set_c4_start_loop"
STRING sAnimDictPrologue5_Main = "missprologueig_5@set_c4_mainaction"
STRING sAnimDictPrologue5_End = "missprologueig_5@set_c4_end_loop"
STRING sAnimDictPrologue5_Duck = "missprologueig_5@press_button_duck"

STRING sAnimDictPrologue_LeadOut = "missprologueleadinoutpro_mcs_5_p3"

STRING sAnimDictPrologue6 = "missprologueig_6"
STRING sAnimDictPrologue6FirstPerson = "MISSPROLOGUEIG_6@FIRST_PERSON"

STRING sAnimDictPrologue_Cough = "missprologueig_5@cough"
STRING sAnimDictPrologue_CleanSmoke = "missprologueig_5@clean_smoke"

//Waypoints
STRING sWaypointRoute1 = "prolog2" //Trevor follow to the vault
STRING sWaypointRoute2 = "prolog5" //Trevor leaves the vault

//Scenes
INT sceneIntro, sceneIntroTrevor, sceneIntroBradHostage, sceneIntroBrad, sceneIntroHostage
VECTOR sceneIntroPos = <<5310.140, -5208.314, 82.557 - 0.04>>
VECTOR sceneIntroRot = <<0.0, 0.0, 0.0>>

INT sceneIntroCam

INT sceneHostage
VECTOR sceneHostagesPos = <<5310.140, -5208.270, 82.520>>
VECTOR sceneHostagesRot = <<0.0, 0.0, 0.0>>

INT sceneVaultReact
VECTOR sceneVaultReactPos = <<5310.140, -5208.279, 82.520>>
VECTOR sceneVaultReactRot = <<0.0, 0.0, 0.0>>

INT sceneVaultReactCam
VECTOR sceneVaultReactCamPos = <<5310.083, -5204.825, 82.520>>	//<<5310.140, -5208.279, 82.520>>
VECTOR sceneVaultReactCamRot = <<0.0, 0.0, 0.0>>

INT sceneLoopVault
INT sceneLeaveVault
VECTOR scenePosition = <<5298.805, -5188.455, 82.540>>
VECTOR sceneRotation = <<0.0, 0.0, -90.0>>

//INT sceneGuardAmbush
VECTOR sceneGuardAmbushPos = <<5297.250, -5192.246, 82.523>>
VECTOR sceneGuardAmbushRot = <<0.0, 0.0, 180.0>>

INT sceneBlastDoors
VECTOR sceneBlastDoorsPos = <<5316.087, -5178.637, 82.519>>
VECTOR sceneBlastDoorsRot = <<0.0, 0.0, 0.0>>

INT sceneDuckUnder, sceneShutterSwitch
VECTOR sceneDuckUnderPos = <<5320.469 + 0.25, -5186.864, 82.519>>
VECTOR sceneDuckUnderRot = <<0.0, 0.0, 0.0>>

//INT sceneToCover
//VECTOR sceneToCoverPos = <<5327.350, -5185.658, 81.823>>
//VECTOR sceneToCoverRot = <<0.0, 0.0, 0.0>>

INT sceneMichaelDead

INT sceneAnimatedFirstPersonCam

//---------------------------------¦ INDEXES ¦-----------------------------------

//Ped
PED_INDEX playerPedID
PED_INDEX notPlayerPedID
PED_INDEX playerPedMichael
PED_INDEX playerPedTrevor

PED_INDEX pedBrad
PED_INDEX pedGetaway
ENUM hostagePeds
	HostageGuard,
	HostageFemale,
	HostageMale1,
	HostageMale2
ENDENUM
PED_INDEX pedHostage[iTotalHostages]
PED_INDEX pedGuard
PED_INDEX pedCop[iTotalCops]
PED_INDEX pedTraffic[iTotalTraffic]
PED_INDEX pedDeadGuard

AI_BLIP_STRUCT blipStructCop[iTotalCops]

//Vehicle
VEHICLE_INDEX vehCar
VEHICLE_INDEX vehCop[iTotalCopCar]
VEHICLE_INDEX vehVan[iTotalVan]
VEHICLE_INDEX vehCarInLot
VEHICLE_INDEX vehTraffic[iTotalTraffic]
VEHICLE_INDEX vehTrain[iTotalTrain]

//Cutscene Index
ENUM cutsceneEntity
	CopCarDriver,
	CopCarShotgun
ENDENUM
PED_INDEX pedCutscene[COUNT_OF(cutsceneEntity)]
VEHICLE_INDEX vehCutscene

//Blip
BLIP_INDEX blipMichael
BLIP_INDEX blipTrevor
BLIP_INDEX blipBuddy
BLIP_INDEX blipGuard
BLIP_INDEX blipCar
BLIP_INDEX blipCover
BLIP_INDEX blipDestination
BLIP_INDEX blipHostage[iTotalHostages]
BLIP_INDEX blipCash
BLIP_INDEX blipCop[iTotalCops]
BLIP_INDEX blipScreen[iTotalScreens]

//Camera
CAMERA_INDEX camMain
CAMERA_INDEX camInterpDOF
CAMERA_INDEX camAnim
CAMERA_INDEX camCinematic

//Sequence
SEQUENCE_INDEX seqMain

//Interior
INTERIOR_INSTANCE_INDEX intDepot

//Object
ENUM WEAPON_REGISTER
	WEAPON_MICHAEL,
	WEAPON_TREVOR,
	WEAPON_BRAD,
	WEAPON_DAVE
ENDENUM

OBJECT_INDEX objWeapon[COUNT_OF(WEAPON_REGISTER)]
OBJECT_INDEX objCupboardDoor
OBJECT_INDEX objCash
OBJECT_INDEX objGarage
OBJECT_INDEX objGun
OBJECT_INDEX objBomb
OBJECT_INDEX objBombGreen
OBJECT_INDEX objBomb3
OBJECT_INDEX objBomb2
OBJECT_INDEX objBomb1
OBJECT_INDEX objDoor
OBJECT_INDEX objHiddenCollision
OBJECT_INDEX objBarrier[iTotalBarrier]
//OBJECT_INDEX objScreen[iTotalScreens]
OBJECT_INDEX objDebris

ENUM BAG_PROPS
	BRAD_BAG_STATIC,
	BRAD_BAG_ANIM,
	BRAD_BAG_ANIM_STRAP,
	MICHAEL_BAG_STATIC,
	TREVOR_BAG_STATIC,
	TREVOR_BAG_DROPPED
ENDENUM

OBJECT_INDEX objBag[COUNT_OF(BAG_PROPS)]

ENUM BALACLAVA_PROPS
	MICHAEL_BALACLAVA,
	TREVOR_BALACLAVA,
	TREVOR_SKIMASK
ENDENUM

OBJECT_INDEX objBalaclava[COUNT_OF(BALACLAVA_PROPS)]

ENUM LIGHTING_RIG_PROPS
	LIGHTING_RIG_OPENING,
	LIGHTING_RIG_HOSTAGE,
	LIGHTING_RIG_VAULT,
	LIGHTING_RIG_GUARD
ENDENUM

OBJECT_INDEX objLightingRig[COUNT_OF(LIGHTING_RIG_PROPS)]

//Particle
PTFX_ID ptfxSmoke, ptfxFog
PTFX_ID ptfxRadiator

INT iEmberTimer
PTFX_ID ptfxEmber, ptfxSmokeCloud

//Cover
COVERPOINT_INDEX covPoint[5]
COVERPOINT_INDEX covPointBlastDoors[4]
COVERPOINT_INDEX covPointMap[5]
ITEMSET_INDEX itemCover

//Weapons
WEAPON_TYPE wtPistol = WEAPONTYPE_PISTOL
WEAPON_TYPE wtSMG = WEAPONTYPE_SMG
WEAPON_TYPE wtCarbineRifle = WEAPONTYPE_CARBINERIFLE
WEAPON_TYPE wtShotgun = WEAPONTYPE_PUMPSHOTGUN

//Relationship Groups
REL_GROUP_HASH relGroupFriendlyFire
REL_GROUP_HASH relGroupBuddy
REL_GROUP_HASH relGroupEnemy

//Kinematic Flag Tracking
enumCharacterList eKinematicChar = NO_CHARACTER

//Rayfire
RAYFIRE_INDEX rfVaultExplosion
RAYFIRE_INDEX rfSecDoorExplosion

//Sound IDs
INT sIDBargeDoor = -1
INT sIDVaultExplosion = -1
INT sIDSecDoorBombBeep = -1
INT sIDSecDoorBlast = -1
INT sIDDistantSirens = -1
INT sIDSecurityDoorAlarm = -1
INT sIDHeadShot = -1
INT sIDCopCarExplosion = -1
INT sIDDistantSirensFarm = -1
INT sIDTrainBell = -1
INT sIDTrainHorn = -1
#IF FEATURE_GEN9_EXCLUSIVE
INT sIDGarageDoor = -1
#ENDIF

//Model Names
//MODEL_NAMES modCash = PROP_CASH_CRATE_01
MODEL_NAMES modBomb = PROP_C4_FINAL
MODEL_NAMES modBombGreen = PROP_C4_FINAL_GREEN
MODEL_NAMES modBomb3 = PROP_C4_NUM_0003
MODEL_NAMES modBomb2 = PROP_C4_NUM_0002
MODEL_NAMES modBomb1 = PROP_C4_NUM_0001
MODEL_NAMES modBarrier = PROP_BARRIER_WORK06A

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widGroup
	BOOL bDebugLightingRig
	BOOL bDebugSwitchCamAnimSide
#ENDIF

//----------------------------------¦ ENUMS ¦------------------------------------

ENUM MissionObjective
	initMission,
	cutIntro,
	stageLearnWalking,
	cutTieUp,
	stageLearnAiming,
	stageLearnPhone,
	stageLearnBlips,
	stageLeaveVault,
	cutGuard,
	stageBlastDoors,
	stageDuckUnderShutter,
	stageShootOut,
	cutGetAway,
	stageGetAway,
	cutFinale,
	stageFinale,
	passMission,
	failMission
ENDENUM

MissionObjective eMissionObjective = initMission

#IF IS_DEBUG_BUILD

MissionObjective eMissionObjectiveAutoJSkip = initMission

#ENDIF

ENUM MissionFail
	failDefault,
	failPlayerDied,
	failMichaelDied,
	failTrevorDied,
	failBradDied,
	failDriverDied,
	failHostageDied,
	failCarDestroyed,
	failOutOfAmmo,
	failOffRoute,
	failStuck,
	failAbandonedTrevor,
	failAbandonedMichael,
	failAbandonedCrew,
	failAbandonedCar,
	failRanAway,
	failHostageGotAway
ENDENUM

MissionFail eMissionFail = failDefault

ENUM AUDIO_TRACK
	NO_AUDIO,
	PROLOGUE_TEST_MISSION_START,
	PROLOGUE_TEST_HOSTAGES,
	PROLOGUE_TEST_PRE_SAFE_EXPLOSION,
	PROLOGUE_TEST_COLLECT_MONEY,
	PROLOGUE_TEST_COLLECT_CASH,
	PROLOGUE_TEST_GUARD_HOSTAGE_OS,
	PROLOGUE_TEST_GUARD_HOSTAGE,
	PROLOGUE_TEST_GUARD_HOSTAGE_RT,
	PROLOGUE_TEST_GUARD_SWITCH,
	PROLOGUE_TEST_HEAD_TO_SECURITY_ROOM_MA,
	PROLOGUE_TEST_COVER_AT_BLAST_DOORS,
	PROLOGUE_TEST_BLAST_DOORS_EXPLODE,
	PROLOGUE_TEST_SHUTTER_OPEN_OS,
	PROLOGUE_TEST_COP_GUNFIGHT,
	PROLOGUE_TEST_COP_GUNFIGHT_PROGRESS,
	PROLOGUE_TEST_COP_GUNFIGHT_RT,
	PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE,
	PROLOGUE_TEST_GETAWAY_CUTSCENE,
	PROLOGUE_TEST_GETAWAY_RT,
	PROLOGUE_TEST_POLICE_CAR_CHASE_OS,
	PROLOGUE_TEST_POLICE_CAR_CHASE,
	PROLOGUE_TEST_POLICE_CAR_CRASH,
	PROLOGUE_TEST_CAR_CHASE,
	PROLOGUE_TEST_POLICE_DRIVE_BY,
	PROLOGUE_TEST_ROADBLOCK_WARNING,
	PROLOGUE_TEST_TRAIN_CRASH,
	PROLOGUE_TEST_BRAD_DOWN,
	PROLOGUE_TEST_AFTER_TRAIN,
	PROLOGUE_TEST_FINALE_RT,
	PROLOGUE_TEST_GRAB_WOMAN,
	PROLOGUE_TEST_FINAL_CUTSCENE,
	PROLOGUE_TEST_FINAL_CUTSCENE_MA,
	PROLOGUE_TEST_MISSION_END,
	PROLOGUE_TEST_FAIL,
	PROLOGUE_TEST_KILL_ONESHOT,
	PROLOGUE_TEST_MISSION_CLEANUP
ENDENUM

AUDIO_TRACK ePrepAudioTrack = NO_AUDIO
AUDIO_TRACK ePlayAudioTrack = NO_AUDIO

//Stage Selector
#IF IS_DEBUG_BUILD

INT iStageSkipMenu

CONST_INT MAX_SKIP_MENU_LENGTH COUNT_OF(MissionObjective) - 2 //Number of stages in mission minus Pass/Fail

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]

PROC MissionNames()
	SkipMenuStruct[0].sTxtLabel = "initMission"
	SkipMenuStruct[1].sTxtLabel = "cutIntro"
	SkipMenuStruct[2].sTxtLabel = "stageLearnWalking"
	SkipMenuStruct[3].sTxtLabel = "cutTieUp - (pro_mcs_1)"
	SkipMenuStruct[4].sTxtLabel = "stageLearnAiming"
	SkipMenuStruct[5].sTxtLabel = "stageLearnPhone"
	SkipMenuStruct[6].sTxtLabel = "stageLearnBlips - (pro_mcs_2)"
	SkipMenuStruct[7].sTxtLabel = "stageLeaveVault"
	SkipMenuStruct[8].sTxtLabel = "cutGuard - (pro_mcs_3_pt1)"
//	SkipMenuStruct[9].sTxtLabel = "stageDisableCameras"
	SkipMenuStruct[9].sTxtLabel = "stageBlastDoors"
	SkipMenuStruct[10].sTxtLabel = "stageDuckUnderShutter"
	SkipMenuStruct[11].sTxtLabel = "stageShootOut"
	SkipMenuStruct[12].sTxtLabel = "cutGetAway - (pro_mcs_5)"
	SkipMenuStruct[13].sTxtLabel = "stageGetAway"
	SkipMenuStruct[14].sTxtLabel = "cutFinale - (PRO_MCS_6)"
	SkipMenuStruct[15].sTxtLabel = "stageFinale - (PRO_MCS_7_Concat)"
ENDPROC

#ENDIF

//Text
STRING sConversationBlock = "PROAUD"

ENUM TRIGGEREDBOOLS
	AdvanceCop0,
	AdvanceCop6,
	AmbientAnims,
	AnimStarted,
	BlastDoorsReady,
	BradAndHostage,
	BradHeadWound,
	BradHostages,
	BradShot,
	BradStartCombat,
	CLEANSMOKE,
	CMN_GENGETBCK,
	CMN_GENGETIN,
	COUGHING,
	CatchUpCam,
	ClearShutterTasks,
	CopCarWave1,
	CopCarWave2,
	CopCarWave3,
	CopCarsBackupPlayback,
	CopCarsPlayback,
	CopFarm0,
	CopFarm1,
	CopFarm2,
	CopFarm3,
	CopFarm4,
	CopFarm5,
	CopFarm6,
	CopFarm7,
	CopsArrive0,
	CopsArrive1,
	CopsArrive2,
	CopsArrive3,
	CopsArrive4,
	CopsArrive5,
	Cops_Arrive_2,
	CutsceneBagRemove,
	DoorIsClosing,
	DriverDied,
	FinalCar1Driver,
	FinalCar1Passenger,
	FinalCar2Driver,
	FinalCar2Passenger,
	FirstSwitchMade,
	FirstWaveDead,
	FootstepHostageGuard,
	FootstepHostageMale1,
	FootstepHostageMale2,
	ForceIdlePlayer,
	GameCamCash,
	GuardHeadWound,
	GuardShootMichael,
	HURRY_HOSTAGES,
	HintCamBomb,
	HintCamBombStop,
	HintSet,
	HostageGuardScream,
	HostageMale1Scream,
	HostageMale2Scream,
	MichaelBag,
	MichaelCash,
	MichaelCashLost,
	MichaelDeadAnim,
	MichaelStartCombat,
	MichaelWillDie,
	NODES_LOADED,
	PLAYER_MOVED_AIM,
	PROHLP_AIM1a,
	PROHLP_AIM1b,
	PROHLP_AIM1c,
	PROHLP_AIM2a,
	PROHLP_AIM2b,
	PROHLP_AIM3,
	PROHLP_TRADAIM,
	PROHLP_BLIPS1,
	PROHLP_BLIPS2,
	PROHLP_BLIPS3,
	PROHLP_BLIPS4,
	CMN_FPSHELP,
	PROHLP_CAR2,
	PROHLP_CAR3,
	PROHLP_CAR4,
	PROHLP_CARSWITCH,
	PROHLP_COVER1,
	PROHLP_COVER2,
	PROHLP_COVER3,
	PROHLP_DEST1,
	PROHLP_DEST2,
	PROHLP_FREEAIM2,
	PROHLP_FREEAIM3,
	PROHLP_FREEAIM4,
	PROHLP_FREEAIMa,
	PROHLP_FREEAIMb,
	PROHLP_FREEAIMc,
	PROHLP_HURRY,
	PROHLP_RADAR,
	PROHLP_RELOAD,
	PROHLP_RUN,
	PROHLP_RUNANDGUN,
	PROHLP_SPRINT,
	PROHLP_SWITCH2,
	PROHLP_SWITCH3,
	PROHLP_SWITCH4,
	PROHLP_SWITCH6,
	PROHLP_WALK,
	PRO_AIM,
	PRO_Advance_1,
	PRO_BUDDY,
	PRO_Back,
	PRO_BackHere,
	PRO_Bills,
	PRO_Blockade,
	PRO_CAR1,
	PRO_CAR2,
	PRO_CARCREW,
	PRO_CASH,
	PRO_CHARGES,
	PRO_COP,
	PRO_COPFARM,
	PRO_COVER1,
	PRO_Charges_Set,
	PRO_Combat2,
	PRO_Combat3,
	PRO_CombatB,
	PRO_CombatM,
	PRO_CombatT,
	PRO_Cover,
	PRO_Door,
	PRO_Doors_1,
	PRO_Doors_3,
	PRO_Doors_4,
	PRO_Set2,
	PRO_Set3,
	PRO_cover1alt,
	PRO_cover2,
	PRO_Drive,
	PRO_Drive1_1,
	PRO_Drive1_2,
	PRO_Drive1_3,
	PRO_Drive1_4,
	PRO_Drive1_5,
	PRO_Drive2_1,
	PRO_Drive2_2,
	PRO_Drive2_3,
	PRO_Drive2_4,
	PRO_Drive2_5,
	PRO_Drive2_6,
	PRO_Drive2_7,
	PRO_Drive2_8,
	PRO_Drive_1,
	PRO_Drive_2,
	PRO_FLAA,
	PRO_FLAB,
	PRO_FLAC,
	PRO_FLAD,
	PRO_GETAWAY,
	PRO_Where,
	PRO_GUARD,
	PRO_GenIdle,
	PRO_GetIn_1,
	PRO_GetIn_3,
	PRO_HostGap_1,
	PRO_HostGap_2,
	PRO_HostGap_3,
	PRO_Hostage,
	PRO_HostageF,
	PRO_HostageP,
	PRO_Idle1B,
	PRO_Idle1T,
	PRO_callit,
	PRO_detonate,
	PRO_Idle3_1,
	PRO_Idle3_2,
	PRO_Idle3_3,
	PRO_Idle3_4,
	PRO_Idle3_5,
	PRO_Idle3_6,
	PRO_Idle4,
	PRO_Under_1,
	PRO_Under_2,
	PRO_Idle_1,
	PRO_Idle_2,
	PRO_Intro_7,
	PRO_Intro_8,
	PRO_quiet,
	PRO_move_hst,
	PRO_ToCar,
	PRO_RespCar,
	PRO_listen,
	PRO_call,
	PRO_see,
	PRO_shoot,
	PRO_Leave,
	PRO_Leave_Preload,
	PRO_LeftCar,
	PRO_MCS3LI,
	PRO_OffRoute,
	PRO_OffShoot,
	PRO_Rage_1,
	PRO_Rage_2,
	PRO_Rage_3,
	PRO_Rage_4,
	PRO_Rage_5,
	PRO_Rage_6,
	PRO_Rage_7,
	PRO_Rage_8,
	PRO_Rescued,
	PRO_Return_1,
	PRO_RunToCar,
	PRO_STAY1,
	PRO_STAY2,
	PRO_SUBTITLE1,
	PRO_SUBTITLE2,
	PRO_SUBTITLE3,
	PRO_SUBTITLE4,
	PRO_SUBTITLE5,
	PRO_Safe_1,
	PRO_Safe_2,
	PRO_Shutter,
	PRO_ShutterDoor,
	PRO_StopYell,
	PRO_TEAM,
	PRO_Unmask,
	PRO_VAULT_PRINT,
	PRO_Vault,
	PRO_WALK,
	PRO_WrongWay,
	PRO_bradhost_1,
	PRO_bradhost_2,
	PRO_bradhost_3,
	PRO_bradhost_4,
	PRO_bradhost_5,
	PRO_bradhost_6,
	PRO_bradhost_7,
//	PRO_bradhost_8,
	PRO_bradhost_9,
	PRO_hostf,
	PlayLotWarning,
	PromptVault,
	Pullout,
	RayfireLoaded,
	ReplaySkipCops,
	RetaskBuddyEnterVehicle,
	RoadblockAudio,
	ShutterOpen,
	SirensVehCop0,
	SirensVehCop1,
	SirensVehCop2,
	SirensVehCop3,
	SirensVehCop4,
	SirensVehCop5,
	SirensVehCop6,
	SirensVehCop7,
	SirensVehCop8,
	SkipAimIntro,
	SnowCover,
	StartShutterAnim,
	TRIGGER_PROLOGUE_TEST_BRAD_DOWN,
	TRIGGER_PROLOGUE_TEST_GUARD_HOSTAGE_OS,
	TRIGGER_PROLOGUE_TEST_POLICE_CAR_CHASE,
	TRIGGER_PROLOGUE_TEST_POLICE_CAR_CHASE_OS,
	TRIGGER_PROLOGUE_TEST_POLICE_CAR_CRASH,
	TRIGGER_PROLOGUE_TEST_PRE_SAFE_EXPLOSION,
	TRIGGER_PROLOGUE_TEST_TRAIN_CRASH,
	TRIGGER_V_ILEV_CD_LAMPAL,
	TRIGGER_V_ILev_CD_Door2,
	TRIGGER_cutFinale,
	TaskBradToVehicle,
	TaskFinalCar,
	TaskMichaelToVehicle,
	TaskTrevorToVehicle,
	TieUpCutsceneSkipped,
	TrainCrash,
	TreeCrash,
	RadiatorSteam,
	TrevorCash,
	TrevorCashLost,
	TrevorIdleVault,
	TrevorRunsOffA,
	TrevorRunsOffB,
	TrevorSetCharge,
	TriggerBlizzard,
	Turning,
	VaultBlast,
	WindowSmash1,
	WindowSmash2,
	WindowSmash3,
	WindowSmash4,
	get_into_cover_player_two,
	pedCop10,
	pedCop11,
	pedCop12,
	pedCop13,
	pedCop14,
	pedCop15,
	pedCop16,
	pedCop9,
	shot_guard_player2,
	SLOWMO_PROLOGUE_VAULT,
	EndCutsceneCleanup,
	WoodSplinter,
	CamEffectIntroShot1,
	CamEffectIntroShot2,
	CamEffectIntroShot3,
	CamEffectIntroShot4,
	CamEffectIntroShot5,
	CamEffectIntroShot6,
	CamEffectIntroShot7,
	V_CORP_CD_CHAIR_FREEZE_1,
	V_CORP_CD_CHAIR_FREEZE_2,
//	RainParticleOverride,
	FirstPersonRelativeHeadingSpam,
	FIRST_PERSON_SWITCH_CAM_ANIM,
	FIRST_PERSON_SWITCH_CAM_FLASH,
	KICK_DOWN_PLAYER_ZERO_CHECK,
	FIRST_PERSON_INTRO_CAM_FLASH
ENDENUM

BOOL bTriggeredBools[COUNT_OF(TRIGGEREDBOOLS)]

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(TRIGGEREDBOOLS bTriggeredBool)
	IF bTriggeredBools[bTriggeredBool]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(TRIGGEREDBOOLS bTriggeredBool, BOOL bTrigger)
	bTriggeredBools[bTriggeredBool] = bTrigger
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT i = 0
	
	REPEAT COUNT_OF(TRIGGEREDBOOLS) i
		bTriggeredBools[i] = FALSE
	ENDREPEAT
ENDPROC

//Models
ENUM TRIGGEREDMODELS
	MODEL_A_F_M_PROLHOST_01,
	MODEL_A_M_M_HILLBILLY_01,
	MODEL_A_M_M_HILLBILLY_02,
	MODEL_A_M_M_PROLHOST_01,
	MODEL_CSB_PROLSEC,
	MODEL_EMPEROR3,
	MODEL_FREIGHT,
	MODEL_FREIGHTCONT1,
	MODEL_FREIGHTCONT2,
	MODEL_GET_PLAYER_PED_MODEL_CHAR_MICHAEL,
	MODEL_GET_PLAYER_PED_MODEL_CHAR_TREVOR,
	MODEL_IG_BRAD,
	MODEL_IG_PROLSEC_02,
	MODEL_POLICEOLD1,
	MODEL_POLICEOLD2,
	MODEL_PROP_CS_HEIST_BAG_02,
	MODEL_PROP_GAR_DOOR_A_01,
	MODEL_PROP_MICHAEL_BALACLAVA,
	MODEL_P_CSH_STRAP_01_S,
	MODEL_P_GDOOR1COLOBJECT_S,
	MODEL_P_LD_HEIST_BAG_S_1,
	MODEL_P_TREVOR_PROLOGE_BALLY_S,
	MODEL_P_TREV_SKI_MASK_S,
	MODEL_RANCHERXL2,
	MODEL_STOCKADE3,
	MODEL_S_M_M_SNOWCOP_01,
	MODEL_TRACTOR3,
	MODEL_U_M_M_PROLSEC_01,
	MODEL_U_M_Y_PROLDRIVER_01,
	MODEL_V_ILev_CD_Door2,
	MODEL_V_ILev_CD_Dust,
	MODEL_modBomb,
	MODEL_modBombGreen,
	MODEL_modBomb3,
	MODEL_modBomb2,
	MODEL_modBomb1,
	MODEL_PROP_1ST_PROLOGUE_SCENE,
	MODEL_PROP_1ST_HOSTAGE_SCENE,
	MODEL_PROP_VAULT_DOOR_SCENE,
	MODEL_PROP_2ND_HOSTAGE_SCENE
ENDENUM

BOOL bTriggeredModels[COUNT_OF(TRIGGEREDMODELS)]

FUNC BOOL HAS_MODEL_BEEN_LOADED(TRIGGEREDMODELS bTriggeredModel)
	IF bTriggeredModels[bTriggeredModel]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_MODEL_AS_LOADED(TRIGGEREDMODELS bTriggeredModel, BOOL bIsLoaded)
	bTriggeredModels[bTriggeredModel] = bIsLoaded
ENDPROC

PROC CLEAR_LOADED_MODELS()
	INT i = 0
	
	REPEAT COUNT_OF(TRIGGEREDMODELS) i
		bTriggeredModels[i] = FALSE
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_CHECK(TRIGGEREDMODELS bTriggeredModel, MODEL_NAMES mnModel)
	IF NOT HAS_MODEL_BEEN_LOADED(bTriggeredModel)
		REQUEST_MODEL(mnModel)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING MODEL #", ENUM_TO_INT(mnModel))	#ENDIF
		
		IF HAS_MODEL_LOADED(mnModel)
			SET_MODEL_AS_LOADED(bTriggeredModel, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("MODEL #", ENUM_TO_INT(mnModel), " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Recordings
INT iLoadedRecordingHashes[30]

FUNC BOOL HAS_RECORDING_BEEN_LOADED(INT iFileNumber, STRING sRecordingName)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		IF iLoadedRecordingHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_RECORDING_AS_LOADED(INT iFileNumber, STRING sRecordingName, BOOL bIsLoaded)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedRecordingHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedRecordingHashes[i] = 0
				iLoadedRecordingHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedRecordingHashes[i] = iHash
				iLoadedRecordingHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_RECORDINGS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		iLoadedRecordingHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_RECORDING_LOADED_CHECK(INT iFileNumber, STRING sRecordingName)
	IF NOT HAS_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
		REQUEST_VEHICLE_RECORDING(iFileNumber, sRecordingName)
		
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
			SET_RECORDING_AS_LOADED(iFileNumber, sRecordingName, TRUE)
			PRINTSTRING("RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName) PRINTSTRING(" LOADED")
			PRINTNL()
		ELSE
			PRINTSTRING("LOADING RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName)
			PRINTNL()
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Animation Dictionaries
INT iAnimDictHashes[25]

FUNC BOOL HAS_ANIM_DICT_BEEN_LOADED(STRING strAnimDict)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		IF iAnimDictHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_ANIM_DICT_AS_LOADED(STRING strAnimDict, BOOL bIsLoaded)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	BOOL bQuitLoop = FALSE
	
	WHILE i < iNumHashes AND NOT bQuitLoop
		IF bIsLoaded
			IF iAnimDictHashes[i] = 0
				iAnimDictHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iAnimDictHashes[i] = iHash
				iAnimDictHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_ANIM_DICTS()
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		iAnimDictHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_ANIM_DICT_LOADED_CHECK(STRING strAnimDict)
	IF NOT HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REQUEST_ANIM_DICT(strAnimDict)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING ANIM DICT ", strAnimDict)	#ENDIF
		
		IF HAS_ANIM_DICT_LOADED(strAnimDict)
			SET_ANIM_DICT_AS_LOADED(strAnimDict, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("ANIM DICT ", strAnimDict, " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ANIM_DICT(STRING strAnimDict)
	IF HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REMOVE_ANIM_DICT(strAnimDict)	#IF IS_DEBUG_BUILD	PRINTLN("REMOVE ANIM DICT ", strAnimDict)	#ENDIF
		SET_ANIM_DICT_AS_LOADED(strAnimDict, FALSE)
	ENDIF
ENDPROC

//--------------------------------¦ STRUCTURES ¦---------------------------------

//SWITCH
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails

//Dialogue
structPedsForConversation sPedsForConversation

//Locates Struct
LOCATES_HEADER_DATA sLocatesData

//--------------------------------¦ FUNCTIONS ¦----------------------------------

BOOL bPlayerControl = TRUE

PROC SAFE_SET_PLAYER_CONTROL(PLAYER_INDEX iPlayerIndex, BOOL bSetControlOn, SET_PLAYER_CONTROL_FLAGS iFlags = 0)
	SET_PLAYER_CONTROL(iPlayerIndex, bSetControlOn, iFlags)
	bPlayerControl = bSetControlOn
ENDPROC

FUNC BOOL SAFE_IS_PLAYER_CONTROL_ON()
	IF bPlayerControl
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADVANCE_CUTSCENE()
	SETTIMERA(0)
	
	iCutsceneStage++
ENDPROC

PROC ADVANCE_STAGE()
	bCleanupStage = TRUE
ENDPROC

FUNC BOOL SKIPPED_STAGE()
	IF bSkipped = TRUE
		bSkipped = FALSE
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL INIT_STAGE()
	IF bInitStage = FALSE
		SETTIMERA(0)
		
		bInitStage = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL CLEANUP_STAGE()
	IF bCleanupStage = TRUE
		SETTIMERA(0)
		
		bInitStage = FALSE
		bCleanupStage = FALSE
		iCutsceneStage = 0
		
		iDialogueStage = 0
		INT i
		REPEAT COUNT_OF(iDialogueLineCount) i
			iDialogueLineCount[i] = -1
		ENDREPEAT
		iDialogueTimer = 0
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//STRING sFLOATING_HELP_STRING

PROC SAFE_CLEAR_HELP(BOOL bClearNow = TRUE)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(bClearNow)	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_HELP - Frame ", GET_FRAME_COUNT())	#ENDIF
		//sFLOATING_HELP_STRING = NULL
	ENDIF
ENDPROC

//PROC SAFE_CLEAR_FLOATING_HELP()
//	IF IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
//		CLEAR_ALL_FLOATING_HELP()
//		sFLOATING_HELP_STRING = NULL
//	ENDIF
//ENDPROC

PROC CLEAR_TEXT()
	CLEAR_PRINTS()
	SAFE_CLEAR_HELP(TRUE)
	//SAFE_CLEAR_FLOATING_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//sFLOATING_HELP_STRING = NULL
	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_TEXT")	#ENDIF
ENDPROC

PROC PRINT_ADV(TRIGGEREDBOOLS bTriggeredBool, STRING sPrint, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(bTriggeredBool)
		PRINT_NOW(sPrint, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(bTriggeredBool, bOnce)
		#IF IS_DEBUG_BUILD	PRINTLN("PRINT ", sPrint)	#ENDIF
	ENDIF
ENDPROC

PROC PRINT_HELP_ADV(TRIGGEREDBOOLS bTriggeredBool, STRING sPrint, BOOL bOnce = TRUE, INT iOverrideTime = -1, INT iMinimumHelpTimer = 2000)
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR (IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND GET_GAME_TIMER() > iHelpTimer)
		IF NOT HAS_LABEL_BEEN_TRIGGERED(bTriggeredBool)
			PRINT_HELP(sPrint, iOverrideTime)
			SET_LABEL_AS_TRIGGERED(bTriggeredBool, bOnce)
			#IF IS_DEBUG_BUILD	PRINTLN("PRINTHELP ", sPrint)	#ENDIF
			iHelpTimer = GET_GAME_TIMER() + iMinimumHelpTimer
		ENDIF
	ENDIF
ENDPROC

//INFO: Author - Alan Litobarski
//PARAM NOTES: (iHour 0-23, iMinute 0-59, iSecond 0-59)
//PURPOSE: Smoothly transitions the clock to a specific time
PROC SMOOTH_SET_CLOCK_TIME(INT iHour, INT iMinute, INT iSecond)
	INT iClockHour, iClockMinute, iClockSecond
	
	iClockHour = GET_CLOCK_HOURS()
	iClockMinute = GET_CLOCK_MINUTES()
	iClockSecond = 0	//GET_CLOCK_SECONDS()
	
	iSecond = iSecond	//Temp - remove this
	
	IF iClockHour < iHour
		IF iClockMinute < 59
//			IF iClockSecond < 59
//				iClockSecond++
//			ELSE
//				iClockSecond = 0
				iClockMinute++
//			ENDIF
		ELSE
			iClockMinute = 0
			iClockHour++
		ENDIF
	ELIF iClockHour > iHour
		IF iClockMinute > 0
//			IF iClockSecond > 0
//				iClockSecond--
//			ELSE
//				iClockSecond = 59
				iClockMinute--
//			ENDIF
		ELSE
			iClockMinute = 59
			iClockHour--
		ENDIF
	ELIF iClockMinute < iMinute
//		IF iClockSecond < 59
//			iClockSecond++
//		ELSE
//			iClockSecond = 0
			iClockMinute++
//		ENDIF
	ELIF iClockMinute > iMinute
//		IF iClockSecond > 0
//			iClockSecond--
//		ELSE
//			iClockSecond = 59
			iClockMinute--
//		ENDIF
//	ELIF iClockSecond < iSecond
//		iClockSecond++
//	ELIF iClockSecond > iSecond
//		iClockSecond--
	ENDIF
	
	SET_CLOCK_TIME(iClockHour, iClockMinute, iClockSecond)
ENDPROC

//INT iFLOATING_HELP_TIME
//BOOL bFLOATING_HELP_UPDATE_ABOVE_PLAYER
//
//PROC UPDATE_FLOATING_HELP_ABOVE_PLAYER()
//	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("|||<<<<<<<<<< UPDATE_FLOATING_HELP_ABOVE_PLAYER() >>>>>>>>>>||")	ENDIF	#ENDIF
//	IF NOT IS_STRING_NULL_OR_EMPTY(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF NOT IS_STRING_NULL_OR_EMPTY(", sFLOATING_HELP_STRING, ")")	ENDIF	#ENDIF
//		IF bFLOATING_HELP_UPDATE_ABOVE_PLAYER	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF bFLOATING_HELP_UPDATE_ABOVE_PLAYER")	ENDIF	#ENDIF
//			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(", sFLOATING_HELP_STRING, ")")	ENDIF	#ENDIF
//				CLEAR_HELP()	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("CLEAR_HELP()")	ENDIF	#ENDIF
//			ENDIF
//			
//			IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED(", sFLOATING_HELP_STRING, ")")	ENDIF	#ENDIF
//				IF iFLOATING_HELP_TIME != -1 /*2147483647*/	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF iFLOATING_HELP_TIME != -1")	ENDIF	#ENDIF
//					HELP_AT_ENTITY(sFLOATING_HELP_STRING, playerPedID, HELP_TEXT_SOUTH, iFLOATING_HELP_TIME - GET_GAME_TIMER())	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("HELP_AT_ENTITY(", sFLOATING_HELP_STRING, ", playerPedID, HELP_TEXT_SOUTH, iFLOATING_HELP_TIME(", iFLOATING_HELP_TIME, ") - GET_GAME_TIMER(", GET_GAME_TIMER(), "))")	ENDIF	#ENDIF
//				ELSE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("ELSE")	ENDIF	#ENDIF
//					HELP_AT_ENTITY(sFLOATING_HELP_STRING, playerPedID, HELP_TEXT_SOUTH, -1)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("HELP_AT_ENTITY(", sFLOATING_HELP_STRING, ", playerPedID, HELP_TEXT_SOUTH, -1)")	ENDIF	#ENDIF
//				ENDIF
//			ENDIF
//		ELSE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("ELSE")	ENDIF	#ENDIF
//			IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED(", sFLOATING_HELP_STRING, ")")	ENDIF	#ENDIF
//				CLEAR_ALL_FLOATING_HELP()	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("CLEAR_ALL_FLOATING_HELP()")	ENDIF	#ENDIF
//			ENDIF
//			
//			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(", sFLOATING_HELP_STRING, ")")	ENDIF	#ENDIF
//				PRINT_HELP(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("PRINT_HELP(", sFLOATING_HELP_STRING, ")")	ENDIF	#ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_AIM_CAM_ACTIVE()
//		OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_GAMEPLAY_CAM_COORD()) < 1.8
//			#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF IS_AIM_CAM_ACTIVE() = ", IS_AIM_CAM_ACTIVE(), " OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_GAMEPLAY_CAM_COORD()) < 1.8 = ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_GAMEPLAY_CAM_COORD()))	ENDIF	#ENDIF
//			IF bFLOATING_HELP_UPDATE_ABOVE_PLAYER = TRUE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF bFLOATING_HELP_UPDATE_ABOVE_PLAYER = TRUE")	ENDIF	#ENDIF
//				bFLOATING_HELP_UPDATE_ABOVE_PLAYER = FALSE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("bFLOATING_HELP_UPDATE_ABOVE_PLAYER = FALSE")	ENDIF	#ENDIF
//			ENDIF
//		ELSE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("ELSE")	ENDIF	#ENDIF
//			IF bFLOATING_HELP_UPDATE_ABOVE_PLAYER = FALSE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF bFLOATING_HELP_UPDATE_ABOVE_PLAYER = FALSE")	ENDIF	#ENDIF
//				bFLOATING_HELP_UPDATE_ABOVE_PLAYER = TRUE	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("bFLOATING_HELP_UPDATE_ABOVE_PLAYER = TRUE")	ENDIF	#ENDIF
//			ENDIF
//		ENDIF
//		
//		IF iFLOATING_HELP_TIME != -1 /*2147483647*/	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF iFLOATING_HELP_TIME != -1")	ENDIF	#ENDIF
//			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sFLOATING_HELP_STRING)
//			AND GET_GAME_TIMER() > iFLOATING_HELP_TIME
//				#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(", sFLOATING_HELP_STRING, ") = ", IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sFLOATING_HELP_STRING), " AND GET_GAME_TIMER()[", GET_GAME_TIMER(), "] > iFLOATING_HELP_TIME[", iFLOATING_HELP_TIME, "]")	ENDIF	#ENDIF
//				SAFE_CLEAR_HELP()	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("SAFE_CLEAR_HELP()")	ENDIF	#ENDIF
//			ENDIF
//			
//			IF NOT IS_STRING_NULL_OR_EMPTY(sFLOATING_HELP_STRING)	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF NOT IS_STRING_NULL_OR_EMPTY(sFLOATING_HELP_STRING)")	ENDIF	#ENDIF
//				IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED(sFLOATING_HELP_STRING)
//				AND GET_GAME_TIMER() > iFLOATING_HELP_TIME
//					#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(", sFLOATING_HELP_STRING, ") = ", IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sFLOATING_HELP_STRING), " AND GET_GAME_TIMER()[", GET_GAME_TIMER(), "] > iFLOATING_HELP_TIME[", iFLOATING_HELP_TIME, "]")	ENDIF	#ENDIF
//					SAFE_CLEAR_FLOATING_HELP()	#IF	IS_DEBUG_BUILD	IF bDebugHelpText	PRINTLN("SAFE_CLEAR_FLOATING_HELP()")	ENDIF	#ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//PROC PRINT_HELP_ADV_POS(STRING sPrint, VECTOR vLocation, ENTITY_INDEX entIndex = NULL, BOOL bOnce = TRUE, eARROW_DIRECTION eArrow = HELP_TEXT_SOUTH, INT iDuration = 7500)
//	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
//		sFLOATING_HELP_STRING = NULL
//		
//		IF NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
//		AND vLocation.Z != 0.0
//		AND entIndex = NULL
//			HELP_AT_LOCATION(sPrint, vLocation, eArrow, iDuration)
//		ELIF NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
//		AND vLocation.Z = 0.0
//		AND entIndex = NULL
//			HELP_AT_SCREEN_LOCATION(sPrint, vLocation.X, vLocation.Y, eArrow, iDuration)
//		ELIF entIndex != NULL
//		AND ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
//			IF GET_PED_INDEX_FROM_ENTITY_INDEX(entIndex) = playerPedID
//				sFLOATING_HELP_STRING = sPrint
//				IF iDuration != -1
//					iFLOATING_HELP_TIME = GET_GAME_TIMER() + iDuration
//				ELSE
//					iFLOATING_HELP_TIME = -1 //2147483647 //Max Value Int
//				ENDIF
//				bFLOATING_HELP_UPDATE_ABOVE_PLAYER = TRUE
//			ENDIF
//			
//			HELP_AT_ENTITY(sPrint, entIndex, eArrow, iDuration)
//		ELIF entIndex != NULL
//		AND NOT ARE_VECTORS_EQUAL(vLocation, VECTOR_ZERO)
//			HELP_AT_ENTITY_OFFSET(sPrint, entIndex, vLocation, eArrow, iDuration)
//		ENDIF
//		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
//		PRINTLN("PRINTHELP ", sPrint)
//	ENDIF
//ENDPROC

FUNC PED_INDEX PLAYER_PED(enumCharacterList CHAR_TYPE)
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TYPE
		RETURN playerPedID
	ELSE
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TYPE)]
	ENDIF
ENDFUNC

FUNC PED_INDEX NOT_PLAYER_PED_ID()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TREVOR)]
	ELSE //GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_MICHAEL)]
	ENDIF
ENDFUNC

PROC UPDATE_PED_REFERENCES()
	playerPedID = PLAYER_PED_ID()
	notPlayerPedID = NOT_PLAYER_PED_ID()
	playerPedMichael = PLAYER_PED(CHAR_MICHAEL)
	playerPedTrevor	= PLAYER_PED(CHAR_TREVOR)
ENDPROC

PROC SET_PED_POSITION(PED_INDEX pedIndex, VECTOR vCoords, FLOAT fHeading, BOOL bKeepVehicle = TRUE)
	IF bKeepVehicle = TRUE
		SET_PED_COORDS_KEEP_VEHICLE(pedIndex, vCoords)
	ELSE
		SET_ENTITY_COORDS(pedIndex, vCoords)
	ENDIF
	
	SET_ENTITY_HEADING(pedIndex, fHeading)
ENDPROC

PROC SET_VEHICLE_POSITION(VEHICLE_INDEX vehicleIndex, VECTOR vCoords, FLOAT fHeading)
	SET_ENTITY_COORDS(vehicleIndex, vCoords)
	SET_ENTITY_HEADING(vehicleIndex, fHeading)
ENDPROC

PROC SPAWN_PLAYER(PED_INDEX &pedIndex, enumCharacterList eChar, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedIndex, eChar, vStart, fStart)
			WAIT(0)
		ENDWHILE
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
		UPDATE_PED_REFERENCES()
	ENDIF
ENDPROC

PROC SPAWN_PED(PED_INDEX &pedIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		pedIndex = CREATE_PED(PEDTYPE_MISSION, eModel, vStart, fStart)	#IF IS_DEBUG_BUILD	PRINTLN("CREATING PED AT <<", vStart.X, ", ", vStart.Y, ", ", vStart.Z, ">>") #ENDIF
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedIndex, TRUE)
	ENDIF
ENDPROC

PROC SPAWN_VEHICLE(VEHICLE_INDEX &vehIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, INT iColour = -1, FLOAT fDirt = 15.0)
	IF NOT DOES_ENTITY_EXIST(vehIndex)
		vehIndex = CREATE_VEHICLE(eModel, vStart, fStart)
		IF iColour >= 0
			SET_VEHICLE_COLOURS(vehIndex, iColour, iColour)
		ENDIF
		SET_VEHICLE_DIRT_LEVEL(vehIndex, fDirt)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex, TRUE)
	ENDIF
ENDPROC

FUNC BOOL SAFE_IS_PED_DEAD(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_IS_VEHICLE_DEAD(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_ENTITY_DEAD(pedIndex)
		OR IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_VEHICLE(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
		OR NOT IS_VEHICLE_DRIVEABLE(vehIndex)
		OR IS_VEHICLE_PERMANENTLY_STUCK(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SAFE_SET_ENTITY_VISIBLE(ENTITY_INDEX EntityIndex, BOOL VisibleFlag)
	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			IF VisibleFlag
				IF NOT IS_ENTITY_VISIBLE(EntityIndex)
					SET_ENTITY_VISIBLE(EntityIndex, TRUE)
				ENDIF
			ELSE
				IF IS_ENTITY_VISIBLE(EntityIndex)
					SET_ENTITY_VISIBLE(EntityIndex, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_LOCATION(BLIP_INDEX &blipIndex, VECTOR vCoords, BOOL bRoute = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		blipIndex = CREATE_BLIP_FOR_COORD(vCoords, bRoute)
		SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGHEST)
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(pedIndex)
			IF NOT IS_PED_INJURED(pedIndex)
				blipIndex = CREATE_BLIP_FOR_PED(pedIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_VEHICLE(BLIP_INDEX &blipIndex, VEHICLE_INDEX &vehIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(vehIndex)
			IF NOT IS_ENTITY_DEAD(vehIndex)
				blipIndex = CREATE_BLIP_FOR_VEHICLE(vehIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
		REMOVE_BLIP(blipIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		DELETE_PED(pedIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_VEHICLE(VEHICLE_INDEX &vehicleIndex)
	IF DOES_ENTITY_EXIST(vehicleIndex)
		DELETE_VEHICLE(vehicleIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_OBJECT(OBJECT_INDEX &objectIndex)
	IF DOES_ENTITY_EXIST(objectIndex)
		DELETE_OBJECT(objectIndex)
	ENDIF
ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &vehIndex, FLOAT fTime)
	IF IS_VEHICLE_DRIVEABLE(vehIndex)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehIndex)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehIndex, fTime - GET_TIME_POSITION_IN_RECORDING(vehIndex))
		ENDIF
	ENDIF
ENDPROC

//Borrowed from locates_private.sch!
PROC EMPTY_VEHICLE_OF_PEDS(VEHICLE_INDEX inVehicle)
    PED_INDEX pedTemp
    INT i
    INT iMaxNumberOfPassengers
	
    //Clear tasks of any passengers or drivers
    IF DOES_ENTITY_EXIST(inVehicle)
        IF IS_VEHICLE_DRIVEABLE(inVehicle)
            //Driver
            pedTemp = GET_PED_IN_VEHICLE_SEAT(inVehicle)  
            IF DOES_ENTITY_EXIST(pedTemp)
                IF NOT IS_PED_INJURED(pedTemp)
                    CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
                ENDIF
            ENDIF
            
            iMaxNumberOfPassengers = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(inVehicle) 
			
            //Passengers
            REPEAT iMaxNumberOfPassengers i
                IF NOT IS_VEHICLE_SEAT_FREE(inVehicle, INT_TO_ENUM(VEHICLE_SEAT, i))
                    pedTemp = GET_PED_IN_VEHICLE_SEAT(inVehicle, INT_TO_ENUM(VEHICLE_SEAT, i)) 
                    IF DOES_ENTITY_EXIST(pedTemp)
                        IF NOT IS_PED_INJURED(pedTemp)
                            CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
                        ENDIF
                    ENDIF
                ENDIF
            ENDREPEAT
        ENDIF
    ENDIF
ENDPROC

STRUCT BLIP_TIMER
	BLIP_INDEX blipIndex
	INT iBlipTimer
ENDSTRUCT

BLIP_TIMER sBlipTimer[10]

PROC SET_BLIP_FLASH_DURATION(BLIP_INDEX blipIndex, INT iDuration = 7500)
	INT i
	
	BOOL bSlotCheck
	BOOL bFreeSlot
	
	REPEAT COUNT_OF(sBlipTimer) i
		IF sBlipTimer[i].blipIndex = blipIndex
			bSlotCheck = TRUE
		ENDIF
	ENDREPEAT
	
	IF bSlotCheck = FALSE
		REPEAT COUNT_OF(sBlipTimer) i
			IF bFreeSlot = FALSE
				IF NOT DOES_BLIP_EXIST(sBlipTimer[i].blipIndex)
					sBlipTimer[i].blipIndex = blipIndex
					sBlipTimer[i].iBlipTimer = GET_GAME_TIMER() + iDuration
					
					SET_BLIP_FLASHES(sBlipTimer[i].blipIndex, TRUE)
					
					bFreeSlot = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bFreeSlot = FALSE
			SCRIPT_ASSERT("No free blip timer slots! Consider expanding array.")
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_BLIP_FLASH_TIMERS()
	INT i
	
	REPEAT COUNT_OF(sBlipTimer) i
		IF DOES_BLIP_EXIST(sBlipTimer[i].blipIndex)
			IF GET_GAME_TIMER() > sBlipTimer[i].iblipTimer
				SET_BLIP_FLASHES(sBlipTimer[i].blipIndex, FALSE)
				
				sBlipTimer[i].blipIndex = NULL
				sBlipTimer[i].iblipTimer = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC FLOAT GET_INTERP_POINT_FLOAT(FLOAT fStartPos, FLOAT fEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN ((((fEndPos - fStartPos) / (fEndTime - fStartTime)) * (fPointTime - fStartTime)) + fStartPos)
ENDFUNC

FUNC VECTOR GET_INTERP_POINT_VECTOR(VECTOR vStartPos, VECTOR vEndPos, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	RETURN <<GET_INTERP_POINT_FLOAT(vStartPos.X, vEndPos.X, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Y, vEndPos.Y, fStartTime, fEndTime, fPointTime), GET_INTERP_POINT_FLOAT(vStartPos.Z, vEndPos.Z, fStartTime, fEndTime, fPointTime)>>
ENDFUNC

//This function will return the value of a number after it has been raised to a power
FUNC FLOAT TPO(FLOAT fNumber, INT iPower)
	INT iLoopControl //This will act as a counter to control how long the loop runs
	FLOAT fInitialValue = fNumber //This will be used in the loop to multiply the new value by its initial value
	FLOAT fResult = 0 //This is the value that the function returns
	
	//Use an if statement to determine how to handle the power function
	//This loop will determine how many times to multiply the number by itself
	IF iPower = 0
		fResult = 1 //Note n^0 = 1
	ELIF iPower = 1
		fResult = fNumber
	ELIF iPower < 0
		iLoopControl = -iPower + 1
		WHILE iLoopControl > 0
			fNumber *= fInitialValue
			iLoopControl--
		ENDWHILE
		fResult = 1 / fNumber
	ELIF iPower > 1
		iLoopControl = iPower - 1
		WHILE iLoopControl > 0
			fNumber *= fInitialValue
			iLoopControl--
		ENDWHILE
		fResult = fNumber
	ENDIF
	
	RETURN fResult
ENDFUNC

FUNC VECTOR HERMITE_CURVE(VECTOR vStartPoint, VECTOR vStartTangent, VECTOR vEndPoint, VECTOR vEndTangent, FLOAT fStartTime, FLOAT fEndTime, FLOAT fPointTime)
	FLOAT fScale, H1, H2, H3, H4
	VECTOR vPoint
	
	fScale = fPointTime / (fEndTime - fStartTime)
	H1 = 2 * TPO(fScale, 3) - 3 * TPO(fScale, 2) + 1
	H2 = -2 * TPO(fScale, 3) + 3 * TPO(fScale, 2)
	H3 = TPO(fScale, 3) - 2 * TPO(fScale, 2) + fScale
	H4 = TPO(fScale, 3) - TPO(fScale, 2)
	vPoint = H1 * vStartPoint + H2 * vEndPoint + H3 * vStartTangent + H4 * vEndTangent
	RETURN vPoint
ENDFUNC

INT iCupboardDoor = HASH("CUPBOARD_DOOR")
INT iLeftReceptionDoor = HASH("LEFT_RECEPTION_DOOR")
INT iRightReceptionDoor = HASH("RIGHT_RECEPTION_DOOR")
INT iCCTVRoomDoor = HASH("CCTV_ROOM_DOOR")

ENUM DOORS_LIST 
	CUPBOARD_DOOR,			//V_ILev_CD_Door2, <<5316.65, -5205.74, 83.67>>
	LEFT_RECEPTION_DOOR,	//V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>
	RIGHT_RECEPTION_DOOR,	//V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>
	CCTV_ROOM_DOOR			//v_ilev_cd_door3, <<5305.46, -5177.75, 83.67>>
ENDENUM

FUNC BOOL SMOOTH_CLOSE_DOOR(DOORS_LIST eDoorsList, MODEL_NAMES modelName, VECTOR vCoords, BOOL bLock, FLOAT fSpeed = 0.02, FLOAT fTolerance = 0.02, FLOAT fOpenRatioIn = 0.0)
	#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("|||<<<<<<<<<< SMOOTH_CLOSE_DOOR = ", GET_MODEL_NAME_FOR_DEBUG(modelName), ", ", ENUM_TO_INT(eDoorsList), " >>>>>>>>>>|||")	ENDIF	#ENDIF
	
	INT iFrontDoor
	
	SWITCH eDoorsList
		CASE CUPBOARD_DOOR
			iFrontDoor = iCupboardDoor
		BREAK
		CASE LEFT_RECEPTION_DOOR
			iFrontDoor = iLeftReceptionDoor
		BREAK
		CASE RIGHT_RECEPTION_DOOR
			iFrontDoor = iRightReceptionDoor
		BREAK
		CASE CCTV_ROOM_DOOR
			iFrontDoor = iCCTVRoomDoor
		BREAK
	ENDSWITCH
	
	DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor)
	#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("eDoorState{", ENUM_TO_INT(eDoorState),"} = DOOR_SYSTEM_GET_DOOR_STATE(", iFrontDoor, ")")	ENDIF	#ENDIF
	
	FLOAT fDoorOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(iFrontDoor)
	#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("fDoorOpenRatio{", fDoorOpenRatio,"} = DOOR_SYSTEM_GET_OPEN_RATIO(", iFrontDoor, ")")	ENDIF	#ENDIF
	
	//Release...
	eDoorState = eDoorState
	modelName = modelName
	
	IF fDoorOpenRatio <= fOpenRatioIn + -fTolerance
	OR fDoorOpenRatio >= fOpenRatioIn + fTolerance
		IF fDoorOpenRatio > fOpenRatioIn
			fDoorOpenRatio -= fSpeed
		ELIF fDoorOpenRatio < fOpenRatioIn
			fDoorOpenRatio += fSpeed
		ENDIF
		
		CLEAR_AREA_OF_OBJECTS(vCoords, 2.0)
		
		DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, fDoorOpenRatio, FALSE)
		#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("DOOR_SYSTEM_SET_OPEN_RATIO(", iFrontDoor, ", ", fDoorOpenRatio, ")")	ENDIF	#ENDIF
		
		IF bLock
			DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_LOCKED, FALSE, TRUE)
		ELSE
			DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("RETURN FALSE")	ENDIF	#ENDIF
		RETURN FALSE
	ELSE
		fDoorOpenRatio = fOpenRatioIn
		
		DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, fDoorOpenRatio, FALSE)
		#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("DOOR_SYSTEM_SET_OPEN_RATIO(", iFrontDoor, ", ", fDoorOpenRatio, ")")	ENDIF	#ENDIF
		
		IF bLock
			DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_LOCKED, FALSE, TRUE)
		ELSE
			DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("RETURN TRUE")	ENDIF	#ENDIF
		RETURN TRUE
	ENDIF
ENDFUNC

PROC UNLOCK_DOOR(DOORS_LIST eDoorsList, MODEL_NAMES modelName)
	#IF IS_DEBUG_BUILD	IF bDebugDoors	PRINTLN("|||<<<<<<<<<< UNLOCK_DOOR = ", GET_MODEL_NAME_FOR_DEBUG(modelName), ", ", ENUM_TO_INT(eDoorsList), "  >>>>>>>>>>|||")	ENDIF	#ENDIF
	INT iFrontDoor
	
	SWITCH eDoorsList
		CASE CUPBOARD_DOOR
			iFrontDoor = iCupboardDoor
		BREAK
		CASE LEFT_RECEPTION_DOOR
			iFrontDoor = iLeftReceptionDoor
		BREAK
		CASE RIGHT_RECEPTION_DOOR
			iFrontDoor = iRightReceptionDoor
		BREAK
		CASE CCTV_ROOM_DOOR
			iFrontDoor = iCCTVRoomDoor
		BREAK
	ENDSWITCH
	
	//Release...
	modelName = modelName
	
	DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor)
	
	IF eDoorState = DOORSTATE_LOCKED
		DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
ENDPROC

FUNC FLOAT HEADING_BETWEEN_COORDS(VECTOR vCoord1, VECTOR vCoord2)
	VECTOR vDirection = vCoord2 - vCoord1
	RETURN ATAN2(vDirection.Y, vDirection.X)
ENDFUNC

// / PURPOSE: Counts the number of elements in a string, seperated by the delimiter (1 character max).
// /    
// / PARAMS:
// /    sString - The string you want to parse for elements.
// /    sDelimiter - The delimiter to seperate the elements in the string.
// / RETURNS: Integer equal to number of elements in string.
// /    
//FUNC INT TOTAL_ELEMENTS_IN_STRING(STRING sString, STRING sDelimiter)
//	INT iElementCount = 1
//	
//	IF NOT IS_STRING_NULL(sString)
//	AND NOT ARE_STRINGS_EQUAL(sString, "")
//		INT i
//		
//		FOR i = 0 TO GET_LENGTH_OF_LITERAL_STRING(sString)
//			IF ARE_STRINGS_EQUAL(sDelimiter, GET_STRING_FROM_STRING(sString, i, CLAMP_INT(i + 1, 0, GET_LENGTH_OF_LITERAL_STRING(sString))))
//				iElementCount++
//			ENDIF
//		ENDFOR
//	ELSE
//		SCRIPT_ASSERT("String is null or empty. Must be greater than 1 character.")
//		RETURN iElementCount
//	ENDIF
//	
//	RETURN iElementCount
//ENDFUNC

// / PURPOSE: Gets an element from a string, elements are seperated by the delimiter (1 character max).
// /    
// / PARAMS:
// /    sString - The string you want to parse for elements.
// /    iElement - The element you want (an element is the characters between the delimiters in the string.
// /    sDelimiter - The delimiter to seperate the elements in the string.
// / RETURNS: Element as string.
// /    
//FUNC STRING GET_ELEMENT_FROM_STRING(STRING sString, STRING sDelimiter, INT iElement = 0)
//	IF NOT IS_STRING_NULL(sString)
//	AND NOT ARE_STRINGS_EQUAL(sString, "")
//		INT i
//		INT iCurrentElement
//		INT iElementStringLength
//		
//		FOR i = 0 TO GET_LENGTH_OF_LITERAL_STRING(sString)
//			IF ARE_STRINGS_EQUAL(sDelimiter, GET_STRING_FROM_STRING(sString, i, CLAMP_INT(i + 1, 0, GET_LENGTH_OF_LITERAL_STRING(sString))))
//			OR i = GET_LENGTH_OF_LITERAL_STRING(sString)
//				IF iElement = iCurrentElement
//					RETURN GET_STRING_FROM_STRING(sString, i - iElementStringLength, i)
//				ENDIF
//				
//				iCurrentElement++
//				iElementStringLength = 0
//			ELSE
//				iElementStringLength++
//			ENDIF
//		ENDFOR
//	ELSE
//		SCRIPT_ASSERT("String is null or empty. Must be greater than 1 character.")
//		RETURN sString
//	ENDIF
//	
//	STRING sEmptyString
//	RETURN sEmptyString
//ENDFUNC

FUNC STRUCT_ENTITY_ID CONSTRUCT_ENTITY_ID()
	STRUCT_ENTITY_ID structEntityID
	RETURN structEntityID
ENDFUNC

FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x, V2.y-V1.y)
ENDFUNC

VECTOR vLastAnimCameraCoords, vThisAnimCameraCoords

FUNC BOOL HAS_ANIMATED_CAMERA_CUT(CAMERA_INDEX camAnimated)
	vLastAnimCameraCoords = vThisAnimCameraCoords
	vThisAnimCameraCoords = GET_CAM_COORD(camAnimated)
	
	IF GET_DISTANCE_BETWEEN_COORDS(vLastAnimCameraCoords, vThisAnimCameraCoords) > 0.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

STRING sAudioEvent
INT iAudioPrepareTimer	//This is an override in case an audio event is prepared but never used

PROC AUDIO_CONTROLLER()	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("<<<<<<<< AUDIO_CONTROLLER >>>>>>>> (sAudioEvent = ", sAudioEvent, ")")	ENDIF	#ENDIF
	IF ePlayAudioTrack = NO_AUDIO
	AND (NOT IS_MUSIC_ONESHOT_PLAYING()
	OR GET_GAME_TIMER() > iAudioPrepareTimer)
		SWITCH ePrepAudioTrack
			CASE PROLOGUE_TEST_MISSION_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_MISSION_START")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_MISSION_START")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_MISSION_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_MISSION_START")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_MISSION_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_HOSTAGES	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_HOSTAGES")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_HOSTAGES")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_HOSTAGES"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_HOSTAGES")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_HOSTAGES")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_PRE_SAFE_EXPLOSION	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_PRE_SAFE_EXPLOSION")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_PRE_SAFE_EXPLOSION")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_PRE_SAFE_EXPLOSION"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_PRE_SAFE_EXPLOSION")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_PRE_SAFE_EXPLOSION")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_GUARD_HOSTAGE_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_GUARD_HOSTAGE_OS")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_GUARD_HOSTAGE_OS")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_GUARD_HOSTAGE_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_GUARD_HOSTAGE_OS")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_GUARD_HOSTAGE_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_SHUTTER_OPEN_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_SHUTTER_OPEN_OS")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_SHUTTER_OPEN_OS")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
						#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_SHUTTER_OPEN_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_SHUTTER_OPEN_OS")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_SHUTTER_OPEN_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_POLICE_CAR_CHASE_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_POLICE_CAR_CHASE_OS")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_POLICE_CAR_CHASE_OS")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_POLICE_CAR_CHASE_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CHASE_OS")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_POLICE_CAR_CHASE_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_TRAIN_CRASH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_TRAIN_CRASH")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_TRAIN_CRASH")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_TRAIN_CRASH"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_TRAIN_CRASH")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_TRAIN_CRASH")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_BRAD_DOWN	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_BRAD_DOWN")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_BRAD_DOWN")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_BRAD_DOWN"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_BRAD_DOWN")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_BRAD_DOWN")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PROLOGUE_TEST_FINAL_CUTSCENE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_FINAL_CUTSCENE")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_FINAL_CUTSCENE")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "PROLOGUE_TEST_FINAL_CUTSCENE"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_FINAL_CUTSCENE")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - PROLOGUE_TEST_FINAL_CUTSCENE")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH ePlayAudioTrack
		CASE PROLOGUE_TEST_MISSION_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_MISSION_START")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_MISSION_START")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_MISSION_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_MISSION_START")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_MISSION_START")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_MISSION_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_HOSTAGES	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_HOSTAGES")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_HOSTAGES")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_HOSTAGES"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_HOSTAGES")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_HOSTAGES")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_HOSTAGES")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_PRE_SAFE_EXPLOSION	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_PRE_SAFE_EXPLOSION")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_PRE_SAFE_EXPLOSION")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_PRE_SAFE_EXPLOSION"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_PRE_SAFE_EXPLOSION")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_PRE_SAFE_EXPLOSION")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_PRE_SAFE_EXPLOSION")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_COLLECT_MONEY	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_COLLECT_MONEY")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_COLLECT_MONEY")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_COLLECT_MONEY")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_COLLECT_CASH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_COLLECT_CASH")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_COLLECT_CASH")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_COLLECT_CASH")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GUARD_HOSTAGE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GUARD_HOSTAGE")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GUARD_HOSTAGE")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GUARD_HOSTAGE")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GUARD_HOSTAGE_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GUARD_HOSTAGE_OS")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_GUARD_HOSTAGE_OS")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_GUARD_HOSTAGE_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_GUARD_HOSTAGE_OS")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GUARD_HOSTAGE_OS")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GUARD_HOSTAGE_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GUARD_HOSTAGE_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GUARD_HOSTAGE_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GUARD_HOSTAGE_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GUARD_HOSTAGE_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GUARD_SWITCH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GUARD_SWITCH")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GUARD_SWITCH")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GUARD_SWITCH")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_HEAD_TO_SECURITY_ROOM_MA	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_HEAD_TO_SECURITY_ROOM_MA")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_HEAD_TO_SECURITY_ROOM_MA")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_HEAD_TO_SECURITY_ROOM_MA")	ENDIF	#ENDIF
			ENDIF
		BREAK	
		CASE PROLOGUE_TEST_COVER_AT_BLAST_DOORS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_COVER_AT_BLAST_DOORS")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_COVER_AT_BLAST_DOORS")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_COVER_AT_BLAST_DOORS")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_BLAST_DOORS_EXPLODE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_BLAST_DOORS_EXPLODE")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_BLAST_DOORS_EXPLODE")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_BLAST_DOORS_EXPLODE")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_SHUTTER_OPEN_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_SHUTTER_OPEN_OS")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_SHUTTER_OPEN_OS")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_SHUTTER_OPEN_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_SHUTTER_OPEN_OS")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_SHUTTER_OPEN_OS")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_SHUTTER_OPEN_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_COP_GUNFIGHT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_COP_GUNFIGHT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_COP_GUNFIGHT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_COP_GUNFIGHT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_COP_GUNFIGHT_PROGRESS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_COP_GUNFIGHT_PROGRESS")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_COP_GUNFIGHT_PROGRESS")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_COP_GUNFIGHT_PROGRESS")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_COP_GUNFIGHT_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_COP_GUNFIGHT_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_COP_GUNFIGHT_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_COP_GUNFIGHT_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GETAWAY_CUTSCENE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GETAWAY_CUTSCENE")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GETAWAY_CUTSCENE")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GETAWAY_CUTSCENE")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GETAWAY_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GETAWAY_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GETAWAY_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GETAWAY_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_POLICE_CAR_CHASE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_POLICE_CAR_CHASE")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_POLICE_CAR_CHASE")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_POLICE_CAR_CHASE"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CHASE")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CHASE")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_POLICE_CAR_CHASE")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_POLICE_CAR_CRASH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_POLICE_CAR_CRASH")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CRASH")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_POLICE_CAR_CRASH")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_CAR_CHASE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_CAR_CHASE")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_CAR_CHASE")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_CAR_CHASE")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_POLICE_DRIVE_BY	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_POLICE_DRIVE_BY")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_POLICE_DRIVE_BY")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_POLICE_DRIVE_BY")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_ROADBLOCK_WARNING	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_ROADBLOCK_WARNING")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_ROADBLOCK_WARNING")
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 6.0)
				
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_ROADBLOCK_WARNING")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_POLICE_CAR_CHASE_OS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - PROLOGUE_TEST_POLICE_CAR_CHASE_OS")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_POLICE_CAR_CHASE_OS")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_POLICE_CAR_CHASE_OS"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CHASE_OS")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CHASE_OS")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_POLICE_CAR_CHASE_OS")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_TRAIN_CRASH	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_TRAIN_CRASH")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_TRAIN_CRASH")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_TRAIN_CRASH"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_TRAIN_CRASH")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_TRAIN_CRASH")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_TRAIN_CRASH")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_BRAD_DOWN	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_BRAD_DOWN")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_BRAD_DOWN")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_BRAD_DOWN"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_BRAD_DOWN")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_BRAD_DOWN")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_BRAD_DOWN")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_AFTER_TRAIN	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_AFTER_TRAIN")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_AFTER_TRAIN")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_AFTER_TRAIN")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_FINALE_RT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_FINALE_RT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_FINALE_RT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_FINALE_RT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_GRAB_WOMAN	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_GRAB_WOMAN")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_GRAB_WOMAN")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_GRAB_WOMAN")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_FINAL_CUTSCENE_MA	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_FINAL_CUTSCENE_MA")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_FINAL_CUTSCENE_MA")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_FINAL_CUTSCENE_MA")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_FINAL_CUTSCENE	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_FINAL_CUTSCENE")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "PROLOGUE_TEST_FINAL_CUTSCENE")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "PROLOGUE_TEST_FINAL_CUTSCENE"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("PROLOGUE_TEST_FINAL_CUTSCENE")
					IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_FINAL_CUTSCENE")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_FINAL_CUTSCENE")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_MISSION_END	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_MISSION_END")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_MISSION_END")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_MISSION_END")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_FAIL	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_FAIL")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_FAIL")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_FAIL")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_KILL_ONESHOT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_KILL_ONESHOT")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_KILL_ONESHOT")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_KILL_ONESHOT")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE PROLOGUE_TEST_MISSION_CLEANUP	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - PROLOGUE_TEST_MISSION_CLEANUP")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("PROLOGUE_TEST_MISSION_CLEANUP")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - PROLOGUE_TEST_MISSION_CLEANUP")	ENDIF	#ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC LOAD_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePrepAudioTrack = eSetAudioTrack	
	
	AUDIO_CONTROLLER()
ENDPROC

PROC PLAY_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePlayAudioTrack = eSetAudioTrack
	
	AUDIO_CONTROLLER()
ENDPROC

SCALEFORM_INDEX sfCredits
BOOL bRequestedCreditsScaleform
BOOL bCreditsScaleformActive

//fStepDuration = 0.166 , fFadeInDuration = 0.5 , fFadeOutDuration = 0.5 , strHudColour = "HUD_COLOUR_MICHAEL" / "HUD_COLOUR_TREVOR" / "HUD_COLOUR_FRANKLIN" / "HUD_COLOUR_FREEMODE"

PROC SCALEFORM_SETUP_SINGLE_LINE(STRING strName, FLOAT fFadeInDuration, FLOAT fFadeOutDuration, FLOAT fXAlign, FLOAT fYAlign, STRING strAlign)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SETUP_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXAlign)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fYAlign)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAlign)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_DISPLAY_SINGLE_LINE(STRING strName, STRING strSingleLine, STRING strFont, STRING strHudColour, BOOL bUseLiteralString = TRUE)	//, STRING strLanguage = "en", FLOAT fYOffset = 0.0)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "ADD_TEXT_TO_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strSingleLine)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFont)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strHudColour)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
		
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strLanguage)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fYOffset)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_SINGLE_LINE(STRING strName, FLOAT fStepDuration)	//, STRING strAnimInStyle, FLOAT fAnimInValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SHOW_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
		
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAnimInStyle)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(fAnimInValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_SINGLE_LINE(STRING strName, FLOAT fStepDuration)	//, STRING strAnimOutStyle, FLOAT fAnimOutValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "HIDE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
		
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAnimInStyle)
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(fAnimInValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_REMOVE_ALL_SINGLE_LINES()
	IF bCreditsScaleformActive	
		BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "REMOVE_ALL")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

//Cleanup
PROC MISSION_CLEANUP(BOOL bRestart = FALSE, BOOL bDelayCleanup = FALSE)	#IF IS_DEBUG_BUILD	PRINTLN("PROLOGUE MISSION CLEAN UP")	#ENDIF
//	IF NOT IS_SCREEN_FADING_OUT()
//	AND NOT IS_SCREEN_FADED_OUT()
//		DO_SCREEN_FADE_OUT(500)
//	ENDIF
//	
//	WHILE NOT IS_SCREEN_FADED_OUT()
//		WAIT(0)
//	ENDWHILE

	NETWORK_SET_CAN_RECEIVE_PRESENCE_INVITES(TRUE)
	
	SET_AUDIO_FLAG("PoliceScannerDisabled", FALSE)
	
	IF bVideoRecording
		REPLAY_STOP_EVENT()
		
		bVideoRecording = FALSE
	ENDIF
	
	INT i
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)	//Fix for bug 1826570
		STOP_SYNCHRONIZED_AUDIO_EVENT(sceneIntro)
	ENDIF
	
	//Stats
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
	
	//Reset player
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_SCREEN_FADED_OUT()
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			SET_PED_POSITION(playerPedID, GET_ENTITY_COORDS(playerPedID), GET_ENTITY_HEADING(playerPedID), FALSE)
		ENDIF
		SET_PED_USING_ACTION_MODE(playerPedID, FALSE)
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(playerPedID, TRUE)
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(playerPedMichael)
		ENDIF
	ENDIF
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	//Selector Cam
	CLEANUP_SELECTOR_CAM(sCamDetails)
	
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_YANKTON_CEMETARY", TRUE, TRUE)
	
	//Time in case of skip...
	SET_TIME_SCALE(1.0)	PRINTLN("SET_TIME_SCALE(1.0)")
	
	//Timecycle
	CLEAR_TIMECYCLE_MODIFIER()	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_TIMECYCLE_MODIFIER - Mission Cleanup")	#ENDIF
	
	IF GET_TIMECYCLE_MODIFIER_INDEX() != -1	#IF IS_DEBUG_BUILD	PRINTLN("IF GET_TIMECYCLE_MODIFIER_INDEX() != -1 - Mission Cleanup")	#ENDIF
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0)	#IF IS_DEBUG_BUILD	PRINTLN("SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0) - Mission Cleanup")	#ENDIF
	ENDIF
	
	//Delete all peds, vehicles, objects and blips
	REPEAT COUNT_OF(covPoint) i
		REMOVE_COVER_POINT(covPoint[i])
	ENDREPEAT
	
	REPEAT COUNT_OF(covPointMap) i
		REMOVE_COVER_POINT(covPointMap[i])
	ENDREPEAT
	
	REPEAT COUNT_OF(covPointBlastDoors) i
		REMOVE_COVER_POINT(covPointBlastDoors[i])
	ENDREPEAT
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	IF IS_ITEMSET_VALID(itemCover)
		DESTROY_ITEMSET(itemCover)
	ENDIF
	
	//Ped
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	ENDIF
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	ENDIF
	IF DOES_ENTITY_EXIST(pedBrad)
		IF NOT IS_ENTITY_DEAD(pedBrad)
			SET_PED_KEEP_TASK(pedBrad, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedBrad)
	ENDIF
	REPEAT COUNT_OF(pedHostage) i
		IF DOES_ENTITY_EXIST(pedHostage[i])
			IF NOT IS_ENTITY_DEAD(pedHostage[i])
				SET_PED_KEEP_TASK(pedHostage[i], TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedHostage[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(pedGuard)
		IF NOT IS_ENTITY_DEAD(pedGuard)
			SET_PED_KEEP_TASK(pedGuard, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedGuard)
	ENDIF
	REPEAT COUNT_OF(pedCop) i
		IF DOES_ENTITY_EXIST(pedCop[i])
			IF NOT IS_ENTITY_DEAD(pedCop[i])
				SET_PED_KEEP_TASK(pedCop[i], TRUE)
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedCop[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(pedGetaway)
		IF NOT IS_ENTITY_DEAD(pedGetaway)
			SET_PED_KEEP_TASK(pedGetaway, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedGetaway)
	ENDIF
	IF DOES_ENTITY_EXIST(pedTraffic[0])
		IF NOT IS_ENTITY_DEAD(pedTraffic[0])
			SET_PED_KEEP_TASK(pedTraffic[0], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedTraffic[0])
	ENDIF
	IF DOES_ENTITY_EXIST(pedTraffic[1])
		IF NOT IS_ENTITY_DEAD(pedTraffic[1])
			SET_PED_KEEP_TASK(pedTraffic[1], TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedTraffic[1])
	ENDIF
	IF DOES_ENTITY_EXIST(pedDeadGuard)
		IF NOT IS_ENTITY_DEAD(pedDeadGuard)
			SET_PED_KEEP_TASK(pedDeadGuard, TRUE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedDeadGuard)
	ENDIF
	
	//Vehicle
	IF DOES_ENTITY_EXIST(vehCar)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
	ENDIF
	REPEAT COUNT_OF(vehCop) i
		IF DOES_ENTITY_EXIST(vehCop[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCop[i])
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(vehVan) i
		IF DOES_ENTITY_EXIST(vehVan[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehVan[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(vehCarInLot)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCarInLot)
	ENDIF
	IF DOES_ENTITY_EXIST(vehTraffic[0])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTraffic[0])
	ENDIF
	IF DOES_ENTITY_EXIST(vehTraffic[1])
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTraffic[1])
	ENDIF
	
	//Object
	REPEAT COUNT_OF(objWeapon) i
		IF DOES_ENTITY_EXIST(objWeapon[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(objWeapon[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(objCupboardDoor)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objCupboardDoor)
	ENDIF
	IF DOES_ENTITY_EXIST(objCash)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objCash)
	ENDIF
	IF DOES_ENTITY_EXIST(objGarage)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objGarage)
	ENDIF
	IF DOES_ENTITY_EXIST(objGun)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objGun)
	ENDIF
	IF DOES_ENTITY_EXIST(objDoor)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objDoor)	#IF IS_DEBUG_BUILD	PRINTLN("PROLOGUE MISSION CLEAN UP - Set Door As No Longer Needed")	#ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(objHiddenCollision)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objHiddenCollision)
	ENDIF
	REPEAT COUNT_OF(objBarrier) i
		IF DOES_ENTITY_EXIST(objBarrier[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(objBarrier[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(objDebris)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objDebris)
	ENDIF
	REPEAT COUNT_OF(objBag) i
		IF DOES_ENTITY_EXIST(objBag[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(objBag[i])
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(objBalaclava) i
		IF DOES_ENTITY_EXIST(objBalaclava[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(objBalaclava[i])
		ENDIF
	ENDREPEAT
	IF DOES_ENTITY_EXIST(objBomb)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objBomb)
	ENDIF
	IF DOES_ENTITY_EXIST(objBombGreen)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objBombGreen)
	ENDIF
	REPEAT COUNT_OF(objLightingRig) i
		IF DOES_ENTITY_EXIST(objLightingRig[i])
			SET_OBJECT_AS_NO_LONGER_NEEDED(objLightingRig[i])
		ENDIF
	ENDREPEAT
	
	//Blip
	SAFE_REMOVE_BLIP(blipMichael)
	SAFE_REMOVE_BLIP(blipTrevor)
	SAFE_REMOVE_BLIP(blipBuddy)
	SAFE_REMOVE_BLIP(blipGuard)
	SAFE_REMOVE_BLIP(blipCar)
	SAFE_REMOVE_BLIP(blipCover)
	SAFE_REMOVE_BLIP(blipDestination)
	SAFE_REMOVE_BLIP(blipCash)
	REPEAT COUNT_OF(pedHostage) i
		SAFE_REMOVE_BLIP(blipHostage[i])
	ENDREPEAT
	REPEAT COUNT_OF(pedCop) i
		SAFE_REMOVE_BLIP(blipCop[i])
	ENDREPEAT
	REPEAT COUNT_OF(blipScreen) i
		SAFE_REMOVE_BLIP(blipScreen[i])
	ENDREPEAT
	
	STOP_GAMEPLAY_HINT()
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DESTROY_ALL_CAMS()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
	
	CLEAR_TEXT()
	
	SET_GPS_FLASHES(FALSE)
	
	//Reset Variables
	fSnowMarks = 0.0
	
	CLEAR_TRIGGERED_LABELS()
	CLEAR_LOADED_MODELS()
	CLEAR_LOADED_RECORDINGS()
	CLEAR_LOADED_ANIM_DICTS()
	
	SETTIMERA(0)
	SETTIMERB(0)
	
	iStuckTimer = 0
	
	iTrainHornTimer = 0
	vTrainHornLocation = VECTOR_ZERO
	
	iCutsceneStage = 0
	
	iDialogueStage = 0
	REPEAT COUNT_OF(iDialogueLineCount) i
		iDialogueLineCount[i] = -1
	ENDREPEAT
	iDialogueTimer = 0
	
	iHelpTimer = -1
	
	bInitStage = FALSE
	
	bReplaySkip = FALSE
	
	bCutsceneSkipped = FALSE
	
//	fAnimProgressTime = 0.0
//	fAnimTotalTime = 0.0
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
	
	REPEAT COUNT_OF(iBulletTimers) i
		iBulletTimers[i] = 0
	ENDREPEAT
	
	REPEAT COUNT_OF(iHostageAimTimer) i
		iHostageAimTimer[i] = 0
	ENDREPEAT
	
//	REPEAT COUNT_OF(iPedResetFlagTimer) i
//		iPedResetFlagTimer[i] = 0
//	ENDREPEAT
	
//	REPEAT COUNT_OF(iShotScreen) i
//		iShotScreen[i] = 0
//	ENDREPEAT
//	
//	REPEAT COUNT_OF(iShotScreenTimer) i
//		iShotScreenTimer[i] = 0
//	ENDREPEAT
	
	//Kinematic Flag Tracking
	eKinematicChar = NO_CHARACTER
	
	//Doors
	UNLOCK_DOOR(CUPBOARD_DOOR, V_ILev_CD_Door2)
	
	REPEAT COUNT_OF(iRespawnTimers) i
		iRespawnTimers[i] = 0
	ENDREPEAT
	
	//Particles
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmoke)
		STOP_PARTICLE_FX_LOOPED(ptfxSmoke)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFog)
		STOP_PARTICLE_FX_LOOPED(ptfxFog)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEmber)
		STOP_PARTICLE_FX_LOOPED(ptfxEmber)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmokeCloud)
		STOP_PARTICLE_FX_LOOPED(ptfxSmokeCloud)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRadiator)
		STOP_PARTICLE_FX_LOOPED(ptfxRadiator)
	ENDIF
	
	REMOVE_PARTICLE_FX_IN_RANGE(<<5299, -5189, 82.6>>, 50.0)
	REMOVE_PARTICLE_FX_IN_RANGE(<<3530.8, -4717.9, 113.1>>, 50.0)
	
	REMOVE_PTFX_ASSET()
	
	//Snow Marks
	IF NOT IS_PED_INJURED(playerPedMichael)
		IF DOES_ENTITY_HAVE_DRAWABLE(playerPedMichael)
		AND DOES_ENTITY_HAVE_PHYSICS(playerPedMichael)
		AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedMichael)
			SET_ENABLE_PED_ENVEFF_SCALE(playerPedMichael, FALSE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(playerPedTrevor)
		IF DOES_ENTITY_HAVE_DRAWABLE(playerPedTrevor)
		AND DOES_ENTITY_HAVE_PHYSICS(playerPedTrevor)
		AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedTrevor)
			SET_ENABLE_PED_ENVEFF_SCALE(playerPedTrevor, FALSE)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedBrad)
		IF DOES_ENTITY_HAVE_DRAWABLE(pedBrad)
		AND DOES_ENTITY_HAVE_PHYSICS(pedBrad)
		AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedBrad)
			SET_ENABLE_PED_ENVEFF_SCALE(pedBrad, FALSE)
		ENDIF
	ENDIF
	
	//Dialogue
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 2)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 3)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 6)
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	//Credits
	IF bRequestedCreditsScaleform
		SCALEFORM_REMOVE_ALL_SINGLE_LINES()
	
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfCredits)
		bRequestedCreditsScaleform = FALSE
		bCreditsScaleformActive = FALSE
	ENDIF
	
	//Audio Scene
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_INSIDE_OFFICE")
		STOP_AUDIO_SCENE("PROLOGUE_GET_INSIDE_OFFICE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_VAULT_RAYFIRE")
		STOP_AUDIO_SCENE("PROLOGUE_VAULT_RAYFIRE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_THREATEN_HOSTAGES")
		STOP_AUDIO_SCENE("PROLOGUE_THREATEN_HOSTAGES")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DETONATE_CHARGES")
		STOP_AUDIO_SCENE("PROLOGUE_DETONATE_CHARGES")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_TO_VAULT")
		STOP_AUDIO_SCENE("PROLOGUE_GET_TO_VAULT")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_SWITCH_TO_TREVOR")
		STOP_AUDIO_SCENE("PROLOGUE_SWITCH_TO_TREVOR")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_TAKE_COVER")
		STOP_AUDIO_SCENE("PROLOGUE_TAKE_COVER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_POLICE_SHOOTOUT")
		STOP_AUDIO_SCENE("PROLOGUE_POLICE_SHOOTOUT")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_TO_PICKUP")
		STOP_AUDIO_SCENE("PROLOGUE_DRIVE_TO_PICKUP")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_ESCAPE")
		STOP_AUDIO_SCENE("PROLOGUE_DRIVE_ESCAPE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GRAB_WOMAN")
		STOP_AUDIO_SCENE("PROLOGUE_GRAB_WOMAN")
	ENDIF
	
	//Score
	IF bPassed = FALSE
		PLAY_AUDIO(PROLOGUE_TEST_FAIL)
	ENDIF
	
	IF CANCEL_MUSIC_EVENT("PROLOGUE_TEST_MISSION_START")
		#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("STOPPED AUDIO EVENT... [PROLOGUE_TEST_MISSION_START]")	ENDIF	#ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
		IF CANCEL_MUSIC_EVENT(sAudioEvent)
			#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("STOPPED AUDIO EVENT... [", sAudioEvent, "]")	ENDIF	#ENDIF
		ENDIF
		
		sAudioEvent = NULL_STRING()	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("sAudioEvent = NULL")	ENDIF	#ENDIF
	ENDIF
	
	ePrepAudioTrack = NO_AUDIO
	ePlayAudioTrack = NO_AUDIO
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	SET_USER_RADIO_CONTROL_ENABLED(TRUE)
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
	//Stream (Audio)
	STOP_STREAM()
	
	//Alarm
	IF IS_ALARM_PLAYING("PROLOGUE_VAULT_ALARMS")
		STOP_ALARM("PROLOGUE_VAULT_ALARMS", TRUE)
	ENDIF
	
	//Sound
	IF sIDBargeDoor != -1
		STOP_SOUND(sIDBargeDoor)
		RELEASE_SOUND_ID(sIDBargeDoor)
		sIDBargeDoor = -1
	ENDIF
	
	IF sIDVaultExplosion != -1
		STOP_SOUND(sIDVaultExplosion)
		RELEASE_SOUND_ID(sIDVaultExplosion)
		sIDVaultExplosion = -1
	ENDIF
	
	IF sIDSecDoorBombBeep != -1
		STOP_SOUND(sIDSecDoorBombBeep)
		RELEASE_SOUND_ID(sIDSecDoorBombBeep)
		sIDSecDoorBombBeep = -1
	ENDIF
	
	IF sIDSecDoorBlast != -1
		STOP_SOUND(sIDSecDoorBlast)
		RELEASE_SOUND_ID(sIDSecDoorBlast)
		sIDSecDoorBlast = -1
	ENDIF
	
	IF sIDDistantSirens != -1
		STOP_SOUND(sIDDistantSirens)
		RELEASE_SOUND_ID(sIDDistantSirens)
		sIDDistantSirens = -1
	ENDIF
	
	IF sIDDistantSirensFarm != -1
		STOP_SOUND(sIDDistantSirensFarm)
		RELEASE_SOUND_ID(sIDDistantSirensFarm)
		sIDDistantSirensFarm = -1
	ENDIF
	
	IF sIDSecurityDoorAlarm != -1
		STOP_SOUND(sIDSecurityDoorAlarm)
		RELEASE_SOUND_ID(sIDSecurityDoorAlarm)
		sIDSecurityDoorAlarm = -1
	ENDIF
	
	IF sIDHeadShot != -1
		STOP_SOUND(sIDHeadShot)
		RELEASE_SOUND_ID(sIDHeadShot)
		sIDHeadShot = -1
	ENDIF
	
	IF sIDCopCarExplosion != -1
		STOP_SOUND(sIDCopCarExplosion)
		RELEASE_SOUND_ID(sIDCopCarExplosion)
		sIDCopCarExplosion = -1
	ENDIF
	
	IF sIDTrainBell != -1
		STOP_SOUND(sIDTrainBell)
		RELEASE_SOUND_ID(sIDTrainBell)
		sIDTrainBell = -1
	ENDIF
	
	IF sIDTrainHorn != -1
		STOP_SOUND(sIDTrainHorn)
		RELEASE_SOUND_ID(sIDTrainHorn)
		sIDTrainHorn = -1
	ENDIF
	#IF FEATURE_GEN9_EXCLUSIVE
		IF sIDGarageDoor != -1
			STOP_SOUND(sIDGarageDoor)
			RELEASE_SOUND_ID(sIDGarageDoor)
			sIDGarageDoor = -1
		ENDIF
	#ENDIF
	
	IF HAS_LABEL_BEEN_TRIGGERED(SLOWMO_PROLOGUE_VAULT)
		DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_PROLOGUE_VAULT")
	ENDIF
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	//Doors
	UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
	UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
	UNLOCK_DOOR(CUPBOARD_DOOR, V_ILev_CD_Door2)
	
	vDoorPosition = <<5308.8574, -5208.1560, 86.9186 - 3.2 -0.05>>
	vDoorRotate = VECTOR_ZERO
	
	REMOVE_MODEL_HIDE(<<5316.64, -5205.74, 83.67>>, 1.0, V_ILev_CD_Door2)
	
	//Vehicle Assets
	REMOVE_VEHICLE_ASSET(POLICEOLD1)
	REMOVE_VEHICLE_ASSET(POLICEOLD2)
	
	//Waypoints
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRoute1)
		REMOVE_WAYPOINT_RECORDING(sWaypointRoute1)
	ENDIF
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRoute2)
		REMOVE_WAYPOINT_RECORDING(sWaypointRoute2)
	ENDIF
		
	//Sequence
	CLEAR_SEQUENCE_TASK(seqMain)
	
	//Assisted
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S1")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S1a")
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S2")
	
	//Relationship Groups
	REMOVE_RELATIONSHIP_GROUP(relGroupFriendlyFire)
	REMOVE_RELATIONSHIP_GROUP(relGroupBuddy)
	REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	
	//Roads
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<3477.819580, -4862.902832, 109.788643>>, <<3504.164063, -4869.103027, 120.770584>>, 16.0)
	
	//Navmesh Blocking
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingID1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingID1)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingID2)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingID2)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingID3)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingID3)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingID4)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingID4)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingNearGarage)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingNearGarage)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingFarm)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingFarm)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingCopPath1)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingCopPath1)
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingCopPath2)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingCopPath2)
	ENDIF
	
	//Wanted
	SET_MAX_WANTED_LEVEL(6)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_FAKE_WANTED_LEVEL(0)
	
	//Ambulances etc.
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	
	SET_RANDOM_TRAINS(TRUE)
	
	//Default Player
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		REMOVE_ALL_PED_WEAPONS(playerPedID)
		SETUP_DEFAULT_PLAYER_INFO()
	ENDIF
	
	//Special Ability
	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	
	//Cutscene State
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	//Interface
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	UNLOCK_MINIMAP_ANGLE()
	
	//Phone
	g_Use_Prologue_Cellphone = FALSE
	
	//Cheats
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	//Model Swaps
	BOOL bLazy = FALSE	//Fix for bug 1695596
	
	IF IS_CUTSCENE_ACTIVE()
		bLazy = TRUE
	ENDIF
	
	REMOVE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, bLazy)
	REMOVE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, bLazy)
	
	bPassed = FALSE	//Reset the bool
	
	IF bRestart = FALSE OR bDelayCleanup
		//Doors
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iCupboardDoor)
			REMOVE_DOOR_FROM_SYSTEM(iCupboardDoor)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iLeftReceptionDoor)
			REMOVE_DOOR_FROM_SYSTEM(iLeftReceptionDoor)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iRightReceptionDoor)
			REMOVE_DOOR_FROM_SYSTEM(iRightReceptionDoor)
		ENDIF
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iCCTVRoomDoor)
			REMOVE_DOOR_FROM_SYSTEM(iCCTVRoomDoor)
		ENDIF
		
		//Interior
		UNPIN_INTERIOR(intDepot)
		
		IF bDelayCleanup = FALSE
		//IPL Groups
		REMOVE_IPL("prologue01")
		REMOVE_IPL("prologue02")
		REMOVE_IPL("prologue03")
		REMOVE_IPL("prologue04")
		REMOVE_IPL("prologue05")
		REMOVE_IPL("prologue06")
		REMOVE_IPL("prologuerd")
		REMOVE_IPL("Prologue01c")
		REMOVE_IPL("Prologue01d")
		REMOVE_IPL("Prologue01e")
		REMOVE_IPL("Prologue01f")
		REMOVE_IPL("Prologue01g")
		REMOVE_IPL("prologue01h")
		REMOVE_IPL("prologue01i")
		REMOVE_IPL("prologue01j")
		REMOVE_IPL("prologue01k")
		REMOVE_IPL("prologue01z")
		REMOVE_IPL("prologue03b")
		REMOVE_IPL("prologue04b")
		REMOVE_IPL("prologue05b")
		REMOVE_IPL("prologue06b")
		REMOVE_IPL("prologuerdb")
		REMOVE_IPL("prologue_occl")
		REMOVE_IPL("prologue06_int")
		REMOVE_IPL("prologue04_cover")
		REMOVE_IPL("prologue03_grv_dug")
		REMOVE_IPL("prologue03_grv_cov")
		REMOVE_IPL("prologue03_grv_fun")
		REMOVE_IPL("prologue_grv_torch")
		REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_DistantLights")
		REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT("prologue_LODLights")
		REMOVE_IPL("DES_ProTree_start")
		REMOVE_IPL("DES_ProTree_start_lod")
		
		//Cullbox
		SET_MAPDATACULLBOX_ENABLED("Prologue_Main", FALSE)
		
		//Weather
		CLEAR_WEATHER_TYPE_PERSIST()
		UNLOAD_ALL_CLOUD_HATS()
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		
		//Minimap
		SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(FALSE)
		ENDIF
		
		SET_ALLOW_STREAM_PROLOGUE_NODES(FALSE)
		
		SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(TRUE)
		
		ALLOW_SONAR_BLIPS(TRUE)
		
		SET_AMBIENT_VEHICLE_NEON_ENABLED(TRUE)
		
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//Mission Passed
PROC missionPassed()
	//TRIGGER_MISSION_STATS_UI(TRUE,TRUE)

	WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
		WAIT(0)
	ENDWHILE
	
	UPDATE_PED_REFERENCES()
	
	SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	
	CLEAR_TEXT()
	Mission_Flow_Mission_Passed(FALSE,TRUE)
	
	bPassed = TRUE
	
	MISSION_CLEANUP()
ENDPROC

//Mission Failed
PROC missionFailed()
	CLEAR_PRINTS()	//CLEAR_TEXT()
	SAFE_CLEAR_HELP(TRUE)
	
	//Score
	PLAY_AUDIO(PROLOGUE_TEST_FAIL)
	
	SWITCH eMissionFail
		CASE failDefault
			MISSION_FLOW_SET_FAIL_REASON("PRO_FAIL")
		BREAK
		CASE failPlayerDied
			MISSION_FLOW_SET_FAIL_REASON("PRO_FAIL")
		BREAK
		CASE failMichaelDied
			IF eMissionObjective < stageGetAway
				MISSION_FLOW_SET_FAIL_REASON("PRO_MDIED")
			ELSE
				MISSION_FLOW_SET_FAIL_REASON("CMN_MDIED")
			ENDIF
		BREAK
		CASE failTrevorDied
			IF eMissionObjective < stageGetAway
				MISSION_FLOW_SET_FAIL_REASON("PRO_TDIED")
			ELSE
				MISSION_FLOW_SET_FAIL_REASON("CMN_TDIED")
			ENDIF
		BREAK
		CASE failBradDied
			MISSION_FLOW_SET_FAIL_REASON("PRO_BDIED")
		BREAK
		CASE failDriverDied
			MISSION_FLOW_SET_FAIL_REASON("PRO_DDIED")
		BREAK
		CASE failHostageDied
			MISSION_FLOW_SET_FAIL_REASON("PRO_HDIED")
			
			CREATE_CONVERSATION(sPedsForConversation, sConversationBlock, "PRO_KillHost", CONV_PRIORITY_MEDIUM)
			
			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				WAIT(0)
			ENDWHILE
		BREAK
		CASE failCarDestroyed
			MISSION_FLOW_SET_FAIL_REASON("PRO_FCAR")
		BREAK
		CASE failOutOfAmmo
			MISSION_FLOW_SET_FAIL_REASON("PRO_FAMMO")
		BREAK
		CASE failOffRoute
			MISSION_FLOW_SET_FAIL_REASON("PRO_FROUTE")
		BREAK
		CASE failStuck
			MISSION_FLOW_SET_FAIL_REASON("PRO_FSTUCK")
		BREAK
		CASE failAbandonedTrevor
			MISSION_FLOW_SET_FAIL_REASON("CMN_TLEFT")
		BREAK
		CASE failAbandonedMichael
			MISSION_FLOW_SET_FAIL_REASON("CMN_MLEFT")
		BREAK
		CASE failAbandonedCrew
			MISSION_FLOW_SET_FAIL_REASON("PRO_FLEFT")
		BREAK
		CASE failAbandonedCar
			MISSION_FLOW_SET_FAIL_REASON("PRO_FABANCAR")
		BREAK
		CASE failRanAway
			MISSION_FLOW_SET_FAIL_REASON("PRO_FRANAWAY")
		BREAK
		CASE failHostageGotAway
			MISSION_FLOW_SET_FAIL_REASON("PRO_FHOSTAWAY")
		BREAK
	ENDSWITCH
	
	Mission_Flow_Mission_Failed()
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off).
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		
		WAIT(0)
	ENDWHILE
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
ENDPROC

PROC DEATH_CHECKS()
	//Shooting - Sneak a bit of script here not really to do with death checks!
//	IF DOES_ENTITY_EXIST(playerPedID)
//		IF NOT IS_ENTITY_DEAD(playerPedID)
//			IF eMissionObjective < cutGuard
//				DISABLE_PLAYER_FIRING(PLAYER_ID(), TRUE)
//				
//				SET_PED_RESET_FLAG(playerPedID, PRF_BlockWeaponFire, TRUE)
//				
////				IF GET_PED_CONFIG_FLAG(playerPedID, PCF_BlockWeaponFire) = FALSE
////					SET_PED_CONFIG_FLAG(playerPedID, PCF_BlockWeaponFire, TRUE)
////				ENDIF
////			ELIF GET_PED_CONFIG_FLAG(playerPedID, PCF_BlockWeaponFire) = TRUE
////				SET_PED_CONFIG_FLAG(playerPedID, PCF_BlockWeaponFire, FALSE)
//			ENDIF
//		ENDIF
//	ENDIF
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2191621
	
	INT i
	
	IF IS_ENTITY_DEAD(playerPedID)
		eMissionFail = failPlayerDied
		
		missionFailed()
	ENDIF
	
	IF eMissionObjective < stageFinale
		IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			eMissionFail = failMichaelDied
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		eMissionFail = failTrevorDied
		
		missionFailed()
	ENDIF
	
	IF bSkipped = FALSE
		IF eMissionObjective != stageFinale
			IF IS_GAMEPLAY_CAM_RENDERING()
			AND IS_SCREEN_FADED_IN()
			AND NOT IS_CUTSCENE_PLAYING()
				IF NOT IS_PED_INJURED(playerPedMichael)
				AND NOT IS_PED_INJURED(playerPedTrevor)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedMichael), GET_ENTITY_COORDS(playerPedTrevor)) > 75.0
					OR ((eMissionObjective = stageShootOut OR (eMissionObjective = cutGetAway AND iCutsceneStage = 0))
					AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedMichael), GET_ENTITY_COORDS(playerPedTrevor)) > 50.0
					OR (NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5284.030762, -5195.381836, 76.656349>>, <<5417.995117, -5195.255371, 93.967865>>, 70.0)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5360.419922, -5167.944336, 76.751923>>, <<5463.675781, -5171.346680, 92.769196>>, 140.0))))
						PRINT_ADV(PRO_TEAM, "PRO_TEAM")	#IF IS_DEBUG_BUILD	PRINTLN("PRO_TEAM - Players out of range!")	#ENDIF
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedMichael), GET_ENTITY_COORDS(playerPedTrevor)) > 100.0
					OR ((eMissionObjective = stageShootOut OR (eMissionObjective = cutGetAway AND iCutsceneStage = 0))
					AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedMichael), GET_ENTITY_COORDS(playerPedTrevor)) > 75.0
					OR (NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5261.509277, -5196.847168, 101.503700>>, <<5380.443848, -5196.899902, 71.411133>>, 110.0)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5481.102051, -5169.144531, 97.666740>>, <<5342.795898, -5165.168457, 72.457390>>, 180.0))))
						IF (NOT IS_PED_INJURED(pedBrad)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), GET_ENTITY_COORDS(notPlayerPedID)) < 30.0)
							eMissionFail = failAbandonedCrew
						ELSE
							IF GET_PLAYER_PED_ENUM(playerPedID) = CHAR_MICHAEL
								eMissionFail = failAbandonedTrevor
							ELIF GET_PLAYER_PED_ENUM(playerPedID) = CHAR_TREVOR
								eMissionFail = failAbandonedMichael
							ENDIF
						ENDIF
						missionFailed()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedBrad)
		IF eMissionObjective < stageFinale
			eMissionFail = failBradDied
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedGetaway)
		IF eMissionObjective < cutGetAway
			eMissionFail = failDriverDied
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehCar)
	OR (NOT IS_ENTITY_DEAD(vehCar)
	AND IS_ENTITY_ON_FIRE(vehCar))
		IF eMissionObjective < stageFinale
			eMissionFail = failCarDestroyed
			
			missionFailed()
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(pedHostage) i
		IF SAFE_DEATH_CHECK_PED(pedHostage[i])
			IF NOT IS_PED_INJURED(pedBrad)
				TASK_TURN_PED_TO_FACE_ENTITY(pedBrad, playerPedID, 10000)
				TASK_LOOK_AT_ENTITY(pedBrad, playerPedID, 10000)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objCupboardDoor)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
					STOP_SYNCHRONIZED_ENTITY_ANIM(objCupboardDoor, INSTANT_BLEND_OUT, TRUE)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			INT iDebugLoop
			REPEAT COUNT_OF(pedHostage) iDebugLoop
				VECTOR vDebugVector = GET_ENTITY_COORDS(pedHostage[iDebugLoop], FALSE)
				PRINTLN("Hostage[", iDebugLoop, "] Injured[", IS_ENTITY_DEAD(pedHostage[iDebugLoop]), "] at coords <<", vDebugVector.X, ", ", vDebugVector.Y, ", ", vDebugVector.Z, ">>")
			ENDREPEAT
			#ENDIF
			
			eMissionFail = failHostageDied
			
			missionFailed()
		ENDIF
	ENDREPEAT
	
	SAFE_DEATH_CHECK_PED(pedGuard)
	
	REPEAT COUNT_OF(pedCop) i
		IF DOES_ENTITY_EXIST(pedCop[i])
			REMOVE_COP_BLIP_FROM_PED(pedCop[i])
		ENDIF
		
		UPDATE_AI_PED_BLIP(pedCop[i], blipStructCop[i])
		
//		IF SAFE_DEATH_CHECK_PED(pedCop[i])
//			SAFE_REMOVE_BLIP(blipCop[i])
//		ENDIF
//		
//		IF NOT DOES_BLIP_EXIST(blipCop[i])
//			IF eMissionObjective <= cutGetAway
//				IF DOES_ENTITY_EXIST(pedCop[i])
//					IF NOT IS_PED_INJURED(pedCop[i])
//						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop[i]), GET_ENTITY_COORDS(playerPedID)) < 50.0
//							SAFE_ADD_BLIP_PED(blipCop[i], pedCop[i], TRUE)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(vehCop) i
		SAFE_DEATH_CHECK_VEHICLE(vehCop[i])
	ENDREPEAT
ENDPROC

INT iCurrentTake
INT iDisplayedTake
INT iMoneyCountEffect = -1
INT iTakeFrameCount

//PURPOSE: Displays the money counter
PROC DISPLAY_TAKE()
	IF iTakeFrameCount != GET_FRAME_COUNT()
		iTakeFrameCount = GET_FRAME_COUNT()
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
		
		IF iDisplayedTake != iCurrentTake
		    IF iMoneyCountEffect = -1
		    	iMoneyCountEffect = GET_SOUND_ID()
				
				PLAY_SOUND_FRONTEND(iMoneyCountEffect, "ROBBERY_MONEY_TOTAL", "HUD_FRONTEND_CUSTOM_SOUNDSET")
		    ENDIF
			
		    IF iDisplayedTake > iCurrentTake
				iDisplayedTake = iDisplayedTake - ROUND(0 +@ 500000)
		          
		        IF iDisplayedTake < iCurrentTake
		        	iDisplayedTake = iCurrentTake
		        ENDIF
				
				DRAW_GENERIC_SCORE(iDisplayedTake, "CMN_TAKE", HEIST_TAKE_DISPLAY_FLASH_TIME, HUD_COLOUR_RED, HUDORDER_DONTCARE, FALSE, "HUD_CASH", FALSE, 0.0, HUDFLASHING_FLASHWHITE, 250)
		    ELSE
				IF iCurrentTake - iDisplayedTake < 10000
					iDisplayedTake = iCurrentTake
		        ELSE
					iDisplayedTake = iDisplayedTake + ROUND(0 +@ 500000)
		        ENDIF
				
		        IF iDisplayedTake > iCurrentTake
		            iDisplayedTake = iCurrentTake
		        ENDIF
				
				DRAW_GENERIC_SCORE(iDisplayedTake, "CMN_TAKE", HEIST_TAKE_DISPLAY_FLASH_TIME, HUD_COLOUR_GREEN, HUDORDER_DONTCARE, FALSE, "HUD_CASH", FALSE, 0.0, HUDFLASHING_FLASHWHITE, 250)
		    ENDIF
		ELSE
		    IF iMoneyCountEffect <> -1
		    	STOP_SOUND(iMoneyCountEffect)
				
				iMoneyCountEffect = -1
		    ENDIF
			
			DRAW_GENERIC_SCORE(iDisplayedTake, "CMN_TAKE", HEIST_TAKE_DISPLAY_FLASH_TIME, HUD_COLOUR_GREEN, HUDORDER_DONTCARE, FALSE, "HUD_CASH", FALSE, 0.0, HUDFLASHING_FLASHWHITE, 250)
		ENDIF
	ENDIF
ENDPROC

PROC WAIT_WITH_DEATH_CHECKS(INT iWait = 0, BOOL bDisableCameraChange = FALSE)
	INT iGameTime = GET_GAME_TIMER() + iWait	
	
	WHILE GET_GAME_TIMER() <= iGameTime
		IF eMissionObjective = stageLearnAiming
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		ENDIF
		
		IF bDisableCameraChange
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		ENDIF
		
		WAIT(0)
		
		//Hide HUD
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)	//#IF IS_DEBUG_BUILD	PRINTLN("HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)")	#ENDIF
		
		//Show HUD
		IF eMissionObjective >= stageLearnBlips AND eMissionObjective <= stageDuckUnderShutter
		AND iCurrentTake > 0
			IF IS_VALID_INTERIOR(intDepot)
				IF NOT (HAS_THIS_CUTSCENE_LOADED("pro_mcs_7_concat")
				AND IS_CUTSCENE_PLAYING())
					DISPLAY_TAKE()
				ENDIF
			ENDIF
		ENDIF
		
		//Random Peds
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		//Traffic
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		IF eMissionObjective <= stageShootOut
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_COP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			ENDIF
		ENDIF
		
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Prologue")
		
		DEATH_CHECKS()
	ENDWHILE
ENDPROC

PROC LOAD_SCENE_ADV(VECTOR vCoords, FLOAT fRadius = 20.0, BOOL bDisableCameraChange = FALSE)
	NEW_LOAD_SCENE_START_SPHERE(vCoords, fRadius)
	
	INT iTimeOut = GET_GAME_TIMER() + 20000
	
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_LOADED()
	AND GET_GAME_TIMER() < iTimeOut
		#IF IS_DEBUG_BUILD
		PRINTLN("NEW_LOAD_SCENE_START_SPHERE(", vCoords.X, ", ", vCoords.Y, ", ", vCoords.Z, ", ", fRadius, ")...")
		
		PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
		#ENDIF
		
		WAIT_WITH_DEATH_CHECKS(0, bDisableCameraChange)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	IF GET_GAME_TIMER() > iTimeOut
		SCRIPT_ASSERT("NEW_LOAD_SCENE_START_SPHERE timed out, see log for details.")
	ENDIF
	#ENDIF
	
	NEW_LOAD_SCENE_STOP()
ENDPROC

PROC CREATE_CONVERSATION_ADV(TRIGGEREDBOOLS bTriggeredBool, STRING sLabel, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE, enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(bTriggeredBool)
		WHILE NOT CREATE_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, eConvPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(bTriggeredBool, bOnce)
	ENDIF
ENDPROC

PROC CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	WHILE NOT CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		
		PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(", ") PRINTSTRING(sLabelSub) PRINTSTRING(" CONVERSATION")
		PRINTNL()
		
		WAIT_WITH_DEATH_CHECKS(0)
	ENDWHILE
ENDPROC

PROC PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(TRIGGEREDBOOLS bTriggeredBool, STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE, enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(bTriggeredBool)
		WHILE NOT PLAY_SINGLE_LINE_FROM_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(", ") PRINTSTRING(sLabelSub) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(bTriggeredBool, bOnce)
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_START_CUTSCENE(FLOAT fRadius = 0.0, BOOL bClearTasks = TRUE, BOOL bInstantKillDialogue = TRUE, BOOL bDontKillHelp = FALSE)
	IF CAN_PLAYER_START_CUTSCENE() = TRUE
		IF fRadius <> 0.0
			CLEAR_AREA(GET_ENTITY_COORDS(playerPedID), fRadius, TRUE)
		ENDIF
		
		IF NOT bDontKillHelp
			IF bInstantKillDialogue
				CLEAR_TEXT()
			ELSE
				CLEAR_PRINTS()
				SAFE_CLEAR_HELP(TRUE)
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
		ELSE
			CLEAR_PRINTS()
			IF bInstantKillDialogue
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
		ENDIF
		KILL_PHONE_CONVERSATION()
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		
		IF bClearTasks = TRUE
			IF NOT IS_PED_IN_ANY_VEHICLE(playerPedID)
				CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN CAN_PLAYER_START_CUTSCENE()
ENDFUNC

//Mid Mission Replay
ENUM MissionReplay
	replayStart,
	replayCutsceneGuard,
	replayAimAtGuard,
	replayShootout,
	replayGetAway,
	replayFinale,
	replayPass
ENDENUM

PROC MISSION_REPLAY(MissionReplay eMissionReplay = replayStart)
	//-	Replay Overall
	
	//-	Replay Specific Position
	VECTOR vStartReplay
	FLOAT fStartReplay
	
	SWITCH eMissionReplay
		CASE replayStart
			vStartReplay = vPlayerStart
			fStartReplay = fPlayerStart
		BREAK
		CASE replayCutsceneGuard
			vStartReplay = <<5299.9331, -5187.6924, 85.7184 - 3.2>>
			fStartReplay = 165.5154
		BREAK
		CASE replayAimAtGuard
			vStartReplay = <<5299.9331, -5187.6924, 85.7184 - 3.2>>
			fStartReplay = 165.5154
		BREAK
		CASE replayShootout
			vStartReplay = <<5326.8550, -5189.1138, 85.0058 - 3.2>>
			fStartReplay = 272.8408
		BREAK
		CASE replayGetAway
			vStartReplay = <<4581.8042, -5059.4099, 109.3138>>
			fStartReplay = 118.5509
		BREAK
		CASE replayFinale
			vStartReplay = <<3543.8826, -4696.1963, 112.6563>>
			fStartReplay = 351.6286
		BREAK
		CASE replayPass
			vStartReplay = <<3272.7, -4571.2, 118.2>>
			fStartReplay = 0.0
		BREAK
	ENDSWITCH
	
	START_REPLAY_SETUP(vStartReplay, fStartReplay, FALSE)
	
	END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
	
	DEATH_CHECKS()	//Since some entities are created before the replay setup
	
	//Cam
	IF NOT DOES_CAM_EXIST(camMain)
		camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF
	
	//-	Replay Specific
	SWITCH eMissionReplay
		CASE replayStart
			SET_PED_POSITION(playerPedID, vPlayerStart, fPlayerStart)
			
			eMissionObjective = cutIntro	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'cutIntro'")	#ENDIF
		BREAK
		CASE replayCutsceneGuard
			SET_PED_POSITION(playerPedID, <<5299.9331, -5187.6924, 85.7184 - 3.2>>, 165.5154)
			
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_GUARD_HOSTAGE_RT)
			
			eMissionObjective = cutGuard	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'cutGuard'")	#ENDIF
		BREAK
		CASE replayAimAtGuard
			SET_PED_POSITION(playerPedID, <<5299.9331, -5187.6924, 85.7184 - 3.2>>, 165.5154)
			
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_GUARD_HOSTAGE_RT)
			
			bReplaySkip = TRUE
			
			eMissionObjective = cutGuard	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'cutGuard'")	#ENDIF
		BREAK
		CASE replayShootout
			//Switch Selector
			ENABLE_SELECTOR()
			
			SET_PED_POSITION(playerPedID, <<5326.8550, -5189.1138, 85.0058 - 3.2>>, 272.8408)
			
			SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 12, 0)
			SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL2, 1, 0)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				UPDATE_PED_REFERENCES()
				
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			ENDIF
			
			//Trevor Balaclava
			SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 1, 0)
			SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 13, 0)
			SET_PED_PROP_INDEX(playerPedTrevor, ANCHOR_EYES, 4)
			
			//Michael Balaclava
			SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_HAIR, 5, 0)
			SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL, 0, 0)
			
			//Brad - Duffle Bag
			SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 1, 0)
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
			
			SET_PED_CAN_BE_TARGETTED(playerPedMichael, FALSE)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedID, "TREVOR")
			
			IF bSwapTrolley = FALSE
				CREATE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
				CREATE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
				
				bSwapTrolley = TRUE
			ENDIF
			
			bGarageVisible = TRUE
			
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_COP_GUNFIGHT_RT)
			
//			bReplaySkip = TRUE
			
			eMissionObjective = stageShootOut	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'stageShootOut'")	#ENDIF
		BREAK
		CASE replayGetAway
			//Switch Selector
			ENABLE_SELECTOR()
			
			//Brad - Duffle Bag
			SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 1, 0)
			
			SET_PED_POSITION(playerPedID, <<4581.8042, -5059.4099, 109.3138>>, 118.5509)
			
			//Player Car
			REQUEST_MODEL(RANCHERXL2)
			
			WHILE NOT HAS_MODEL_LOADED(RANCHERXL2)
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				SPAWN_VEHICLE(vehCar, RANCHERXL2, vCar, fCar, 0)
				SET_VEHICLE_NUMBER_PLATE_TEXT(vehCar, "87ALN974")
				SET_MODEL_AS_NO_LONGER_NEEDED(RANCHERXL2)
				SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
				//SET_ENTITY_PROOFS(vehCar, FALSE, TRUE, TRUE, FALSE, FALSE)
				SET_VEHICLE_TYRES_CAN_BURST(vehCar, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_REAR_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_REAR_RIGHT, FALSE)
				SET_VEHICLE_RADIO_ENABLED(vehCar, FALSE)
			ENDIF
			
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_GETAWAY_RT)
			
			IF g_bShitskipAccepted = TRUE
				REQUEST_MODEL(U_M_Y_PROLDRIVER_01)
				
				WHILE NOT HAS_MODEL_LOADED(U_M_Y_PROLDRIVER_01)
					WAIT(0)
				ENDWHILE
				
				pedGetaway = CREATE_PED_INSIDE_VEHICLE(vehCar, PEDTYPE_MISSION, U_M_Y_PROLDRIVER_01)
				SET_PED_COMPONENT_VARIATION(pedGetaway, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedGetaway, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedGetaway, relGroupFriendlyFire)
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedGetaway, "PROGETAWAY")
				SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_PROLDRIVER_01)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGetaway, TRUE)
				
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedGetaway)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				bReplaySkip = TRUE
				
				eMissionObjective = cutGetAway	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'stageGetAway'")	#ENDIF
			ELSE
				eMissionObjective = stageGetAway	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'stageGetAway'")	#ENDIF
			ENDIF
		BREAK
		CASE replayFinale
			//Brad - Duffle Bag
			SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 1, 0)
			
			SET_PED_POSITION(playerPedID, <<3543.8826, -4696.1963, 112.6563>>, 351.6286, FALSE)
			
			//Player Car
			REQUEST_MODEL(RANCHERXL2)
			
			WHILE NOT HAS_MODEL_LOADED(RANCHERXL2)
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				SPAWN_VEHICLE(vehCar, RANCHERXL2, vCar, fCar, 0)
				SET_VEHICLE_NUMBER_PLATE_TEXT(vehCar, "87ALN974")
				SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_LEFT)
				SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_RIGHT)
				SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
				SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_RIGHT)
				SET_VEHICLE_DAMAGE(vehCar, <<0.5, -0.4, 0.05>>, 750.0, 750.0, TRUE)
				SET_VEHICLE_DAMAGE(vehCar, <<0.5, 0.4, 0.05>>, 750.0, 750.0, TRUE)
				SET_VEHICLE_DAMAGE(vehCar, <<0.0, -2.0, 1.0>>, 250.0, 250.0, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(RANCHERXL2)
				//SET_ENTITY_PROOFS(vehCar, FALSE, TRUE, TRUE, FALSE, FALSE)
				SET_VEHICLE_TYRES_CAN_BURST(vehCar, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_REAR_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_REAR_RIGHT, FALSE)
				SET_VEHICLE_RADIO_ENABLED(vehCar, FALSE)
			ENDIF
			
			IF g_bShitskipAccepted = TRUE
				eMissionObjective = cutFinale	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'stageFinale'")	#ENDIF
			ELSE
				bReplaySkip = TRUE
				
				eMissionObjective = stageFinale	#IF IS_DEBUG_BUILD	PRINTLN("REPLAYING MISSION FROM STAGE 'stageFinale'")	#ENDIF
			ENDIF
		BREAK
		CASE replayPass
			//Cutscene
			REQUEST_CUTSCENE("Pro_mcs_7_concat", CUTSCENE_REQUESTED_DIRECTLY_FROM_SKIP)
			
			SET_PED_POSITION(playerPedID, <<3272.7, -4571.2, 118.2>>, 0.0, FALSE)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				UPDATE_PED_REFERENCES()
				
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			ENDIF
			
			//LOAD_SCENE_ADV(<<3272.7, -4571.2, 118.2>>)
			
			SET_CURRENT_PED_WEAPON(playerPedID, WEAPONTYPE_UNARMED, TRUE)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedTrevor)
				WAIT_WITH_DEATH_CHECKS(0)			#IF IS_DEBUG_BUILD	PRINTLN("HAVE_ALL_STREAMING_REQUESTS_COMPLETED: Trevor")	#ENDIF
			ENDWHILE
			
			WHILE NOT HAS_CUTSCENE_LOADED()
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			SET_CUTSCENE_FADE_VALUES(FALSE, TRUE, TRUE, FALSE)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			
			START_CUTSCENE()
			
			LOAD_CLOUD_HAT("Stormy 01")
			
			WHILE NOT IS_CUTSCENE_PLAYING()
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
			ENDIF
			
			WHILE IS_CUTSCENE_PLAYING()
				WAIT(0)
			ENDWHILE
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF
			
			eMissionObjective = passMission
		BREAK
	ENDSWITCH
	
//	IF eMissionObjective <> passMission
//		LOAD_SCENE_ADV(GET_ENTITY_COORDS(playerPedID))
//	ENDIF
	
	bSkipped = TRUE
ENDPROC

PROC LOAD_UNLOAD_ASSETS()
	//Stages Reference
		//cutIntro
		//stageLearnWalking
		//cutTieUp
		//stageLearnAiming
		//stageLearnPhone
		//stageLearnBlips
		//stageLeaveVault
		//cutGuard
		//stageDisableCameras
		//stageBlastDoors
		//stageDuckUnderShutter
		//stageShootOut
		//stageGetAway
		//stageFinale
		//passMission
		//failMission	
	
	//Mission Peds:
		//GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
		//GET_PLAYER_PED_MODEL(CHAR_TREVOR)
		//IG_BRAD
		//U_M_Y_PROLDRIVER_01
		//A_M_M_PROLHOST_01
		//A_F_M_PROLHOST_01
		//IG_PROLSEC_02
		//S_M_M_SNOWCOP_01
	
	//Mission Cars:
		//RANCHERXL2
		//STANIER
		//POLICEOLD1
		//POLICEOLD2
		//MINIVAN
		//STOCKADE3
	
	//Mission Props:
		//PROP_GAR_DOOR_A_01
	
	//Mission Recordings:
		//001
		//003
		//004
		//005
		//006
		//008
		//009
		//011
		//018
		//019
		//020
		//021
		//024
		//026
	
	//Request Models
		//GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
		IF (NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		AND playerPedID = playerPedMichael)
		AND (eMissionObjective <= cutIntro)
			HAS_MODEL_LOADED_CHECK(MODEL_GET_PLAYER_PED_MODEL_CHAR_MICHAEL, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		ENDIF
		
		//GET_PLAYER_PED_MODEL(CHAR_TREVOR)
		IF eMissionObjective <= cutIntro
			HAS_MODEL_LOADED_CHECK(MODEL_GET_PLAYER_PED_MODEL_CHAR_TREVOR, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		ENDIF
		
		//IG_BRAD - Buddy
		IF eMissionObjective <= cutIntro
			HAS_MODEL_LOADED_CHECK(MODEL_IG_BRAD, IG_BRAD)
		ENDIF
		
		//A_M_M_PROLHOST_01 - Hostages 0 and 3
		IF eMissionObjective = cutIntro
			HAS_MODEL_LOADED_CHECK(MODEL_A_M_M_PROLHOST_01, A_M_M_PROLHOST_01)
		ENDIF
		
		//A_F_M_PROLHOST_01 - Hostage 2
		IF eMissionObjective = cutIntro
			HAS_MODEL_LOADED_CHECK(MODEL_A_F_M_PROLHOST_01, A_F_M_PROLHOST_01)
		ENDIF
		
		//IG_PROLSEC_02 - Hostage 1
		IF eMissionObjective = cutIntro
			HAS_MODEL_LOADED_CHECK(MODEL_IG_PROLSEC_02, IG_PROLSEC_02)
		ENDIF
		
		//V_ILev_CD_Dust - Debris
		IF (eMissionObjective >= stageLearnAiming
		AND eMissionObjective <= stageLearnPhone)
			HAS_MODEL_LOADED_CHECK(MODEL_V_ILev_CD_Dust, V_ILev_CD_Dust)
		ENDIF
		
		//CSB_PROLSEC - Guard
		IF (eMissionObjective >= stageLearnBlips
		AND eMissionObjective <= cutGuard)
			HAS_MODEL_LOADED_CHECK(MODEL_CSB_PROLSEC, CSB_PROLSEC)
		ENDIF
		
		//RANCHERXL2 - Getaway Car
		IF eMissionObjective >= stageDuckUnderShutter
			HAS_MODEL_LOADED_CHECK(MODEL_RANCHERXL2, RANCHERXL2)
		ENDIF
		
		//U_M_Y_PROLDRIVER_01 - Getaway Driver
		IF eMissionObjective >= stageDuckUnderShutter
		AND eMissionObjective <= stageShootOut
			HAS_MODEL_LOADED_CHECK(MODEL_U_M_Y_PROLDRIVER_01, U_M_Y_PROLDRIVER_01)
		ENDIF
		
		//S_M_M_SNOWCOP_01 - Cops
		IF eMissionObjective >= stageDuckUnderShutter
			HAS_MODEL_LOADED_CHECK(MODEL_S_M_M_SNOWCOP_01, S_M_M_SNOWCOP_01)
		ENDIF
		
		//U_M_M_PROLSEC_01 - Dead Guard
		IF eMissionObjective >= stageDuckUnderShutter
		AND eMissionObjective <= stageShootOut
			HAS_MODEL_LOADED_CHECK(MODEL_U_M_M_PROLSEC_01, U_M_M_PROLSEC_01)
		ENDIF
		
		//POLICEOLD1, POLICEOLD2 - Cop Cars
		IF eMissionObjective >= stageDuckUnderShutter
			HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD1, POLICEOLD1)
			HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD2, POLICEOLD2)
			REQUEST_VEHICLE_ASSET(POLICEOLD1, ENUM_TO_INT(VRF_REQUEST_EXIT_TO_AIM_ANIMS))
			REQUEST_VEHICLE_ASSET(POLICEOLD2, ENUM_TO_INT(VRF_REQUEST_EXIT_TO_AIM_ANIMS))
		ENDIF
		
		//STOCKADE3 - Security Vans
		IF eMissionObjective >= stageDuckUnderShutter
		AND eMissionObjective <= stageShootOut
			HAS_MODEL_LOADED_CHECK(MODEL_STOCKADE3, STOCKADE3)
		ENDIF
		
		//FREIGHT - Train Engine
		IF eMissionObjective >= cutFinale
			HAS_MODEL_LOADED_CHECK(MODEL_FREIGHT, FREIGHT)
		ENDIF
		
		//FREIGHTCONT1 - Train Carriage 1
		IF eMissionObjective >= cutFinale
			HAS_MODEL_LOADED_CHECK(MODEL_FREIGHTCONT1, FREIGHTCONT1)
		ENDIF
		
		//FREIGHTCONT2 - Train Carriage 2
		IF eMissionObjective >= cutFinale
			HAS_MODEL_LOADED_CHECK(MODEL_FREIGHTCONT2, FREIGHTCONT2)
		ENDIF
		
		//PROP_CS_HEIST_BAG_02 - Duffle Bag Prop
		IF eMissionObjective >= stageLearnBlips
			HAS_MODEL_LOADED_CHECK(MODEL_PROP_CS_HEIST_BAG_02, PROP_CS_HEIST_BAG_02)
			HAS_MODEL_LOADED_CHECK(MODEL_P_LD_HEIST_BAG_S_1, P_LD_HEIST_BAG_S_1)
			HAS_MODEL_LOADED_CHECK(MODEL_P_CSH_STRAP_01_S, P_CSH_STRAP_01_S)
		ENDIF
		
		//P_TREVOR_PROLOGE_BALLY_S - Balaclava + Ski Mask Prop
		IF eMissionObjective >= stageDuckUnderShutter
		AND eMissionObjective <= stageShootOut
			HAS_MODEL_LOADED_CHECK(MODEL_P_TREVOR_PROLOGE_BALLY_S, P_TREVOR_PROLOGE_BALLY_S)
			HAS_MODEL_LOADED_CHECK(MODEL_P_TREV_SKI_MASK_S, P_TREV_SKI_MASK_S)
		ENDIF
		
		//PROP_MICHAEL_BALACLAVA - Balaclava Prop
		IF eMissionObjective >= stageLeaveVault
		AND eMissionObjective <= cutGuard
			HAS_MODEL_LOADED_CHECK(MODEL_PROP_MICHAEL_BALACLAVA, PROP_MICHAEL_BALACLAVA)
		ENDIF
		
		//Lighting Rigs
		IF eMissionObjective = cutIntro
			HAS_MODEL_LOADED_CHECK(MODEL_PROP_1ST_PROLOGUE_SCENE, PROP_1ST_PROLOGUE_SCENE)
		ENDIF
		
		IF eMissionObjective >= stageLearnWalking
		AND eMissionObjective <= stageLearnAiming
			HAS_MODEL_LOADED_CHECK(MODEL_PROP_1ST_HOSTAGE_SCENE, PROP_1ST_HOSTAGE_SCENE)
		ENDIF
		
		IF eMissionObjective >= stageLearnAiming
		AND eMissionObjective <= stageLearnPhone
			HAS_MODEL_LOADED_CHECK(MODEL_PROP_VAULT_DOOR_SCENE, PROP_VAULT_DOOR_SCENE)
		ENDIF
		
		IF eMissionObjective >= stageLeaveVault
		AND eMissionObjective <= cutGuard
			HAS_MODEL_LOADED_CHECK(MODEL_PROP_2ND_HOSTAGE_SCENE, PROP_2ND_HOSTAGE_SCENE)
		ENDIF
		
	//Vehicle Recordings
		//014 - Actual camera car back
		//015 - Actual camera car front
		//016 - Actual camera car left
		//017 - Actual camera car right
		//022 - Actual camera chopper
		
		IF eMissionObjective >= stageBlastDoors
			HAS_RECORDING_LOADED_CHECK(003, sCarrec)
			HAS_RECORDING_LOADED_CHECK(004, sCarrec)
			HAS_RECORDING_LOADED_CHECK(005, sCarrec)
			HAS_RECORDING_LOADED_CHECK(006, sCarrec)
			HAS_RECORDING_LOADED_CHECK(008, sCarrec)
			HAS_RECORDING_LOADED_CHECK(009, sCarrec)
			HAS_RECORDING_LOADED_CHECK(011, sCarrec)
			HAS_RECORDING_LOADED_CHECK(026, sCarrec)
			HAS_RECORDING_LOADED_CHECK(027, sCarrec)
			HAS_RECORDING_LOADED_CHECK(028, sCarrec)
			HAS_RECORDING_LOADED_CHECK(029, sCarrec)
			HAS_RECORDING_LOADED_CHECK(032, sCarrec)
			HAS_RECORDING_LOADED_CHECK(033, sCarrec)
			HAS_RECORDING_LOADED_CHECK(034, sCarrec)
			HAS_RECORDING_LOADED_CHECK(035, sCarrec)
		ENDIF
	
	//Waypoint Recordings
		IF eMissionObjective = stageLearnPhone
			REQUEST_WAYPOINT_RECORDING(sWaypointRoute1)
		ELIF eMissionObjective > stageLeaveVault
			IF GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRoute1)
				REMOVE_WAYPOINT_RECORDING(sWaypointRoute1)
			ENDIF
		ENDIF
		
		IF eMissionObjective = stageLearnBlips
			REQUEST_WAYPOINT_RECORDING(sWaypointRoute2)
		ELIF eMissionObjective > stageLeaveVault
			IF GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRoute2)
				REMOVE_WAYPOINT_RECORDING(sWaypointRoute2)
			ENDIF
		ENDIF
	
	//Animation Dictionaries
		IF eMissionObjective >= stageDuckUnderShutter
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDeepSnow)
		ENDIF
		
//		IF eMissionObjective >= cutGuard
//		AND eMissionObjective <= stageDisableCameras
//			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictSignal)
//		ELSE
//			UNLOAD_ANIM_DICT(sAnimDictSignal)
//		ENDIF
		
		IF eMissionObjective >= stageBlastDoors
		AND eMissionObjective <= stageDuckUnderShutter
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMapObjects)
//		ELSE
//			UNLOAD_ANIM_DICT(sAnimDictMapObjects)
		ENDIF
		
		IF eMissionObjective <= cutTieUp
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue1)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue1)
		ENDIF
		
		IF eMissionObjective >= stageLearnWalking
		AND eMissionObjective <= stageLearnAiming
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_MCS1)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_MCS1_GuardFacial)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_TrevorReturn)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue2)
			UNLOAD_ANIM_DICT(sAnimDictPrologue2_MCS1)
			UNLOAD_ANIM_DICT(sAnimDictPrologue2_MCS1_GuardFacial)
			UNLOAD_ANIM_DICT(sAnimDictPrologue2_TrevorReturn)
		ENDIF
		
		IF eMissionObjective >= stageLearnAiming
		AND eMissionObjective <= stageLearnPhone
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_Cam)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue3)
			UNLOAD_ANIM_DICT(sAnimDictPrologue3_Cam)
		ENDIF
		
		IF eMissionObjective >= cutTieUp
		AND eMissionObjective <= stageLearnBlips
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_Impatient)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue3_Impatient)
		ENDIF
		
		IF eMissionObjective >= cutTieUp
		AND eMissionObjective <= stageLearnPhone
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_IdleAtCupboard)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue3_IdleAtCupboard)
		ENDIF
		
		IF eMissionObjective >= stageLearnPhone
		AND eMissionObjective <= stageLearnBlips
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_IdleInVault)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue3_IdleInVault)
		ENDIF
		
		IF eMissionObjective >= stageLearnAiming
		AND eMissionObjective <= stageLeaveVault
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_LeaveVault)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue_LeaveVault)
		ENDIF
		
		IF eMissionObjective >= stageLearnPhone
		AND eMissionObjective <= stageLearnBlips
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_Cough)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue_Cough)
		ENDIF
		
		IF eMissionObjective >= stageLeaveVault
		AND eMissionObjective <= cutGuard
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_Base)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_Shot)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_PlayerPause)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_Fail)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue4_Base)
			UNLOAD_ANIM_DICT(sAnimDictPrologue4_Shot)
			UNLOAD_ANIM_DICT(sAnimDictPrologue4_PlayerPause)
			UNLOAD_ANIM_DICT(sAnimDictPrologue4_Fail)
		ENDIF
		
		IF eMissionObjective >= cutGuard
		AND eMissionObjective <= stageDuckUnderShutter
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Start)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Main)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_End)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue5_Start)
			UNLOAD_ANIM_DICT(sAnimDictPrologue5_Main)
			UNLOAD_ANIM_DICT(sAnimDictPrologue5_End)
		ENDIF
		
		IF eMissionObjective >= stageBlastDoors
		AND eMissionObjective <= stageDuckUnderShutter
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Duck)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue5_Duck)
		ENDIF
		
		IF eMissionObjective >= stageShootOut
		AND eMissionObjective <= stageGetAway
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_LeadOut)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue_LeadOut)
		ENDIF
		
		IF (eMissionObjective >= stageBlastDoors
		AND eMissionObjective <= stageShootOut)
		OR (eMissionObjective >= stageGetAway
		AND eMissionObjective <= stageFinale)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue6)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue6)
		ENDIF
		
		IF (eMissionObjective >= stageBlastDoors
		AND eMissionObjective <= stageShootOut)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue6FirstPerson)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue6FirstPerson)
		ENDIF
		
		IF eMissionObjective >= stageBlastDoors
		AND eMissionObjective <= stageDuckUnderShutter
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_CleanSmoke)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictPrologue_CleanSmoke)
		ENDIF
		
		IF (eMissionObjective >= stageDuckUnderShutter
		AND eMissionObjective <= stageShootOut)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDeadGuard)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictDeadGuard)
		ENDIF
ENDPROC

PROC initialiseVariables()
	//Hostages
	vHostageStart[0] = <<5311.9209, -5206.0557, 85.7187 - 3.2>>
	fHostageStart[0] = 272.2921
	vHostageStart[1] = <<5310.6543, -5207.0322, 85.7187 - 3.2>>
	fHostageStart[1] = 139.6356
	vHostageStart[2] = <<5311.5762, -5211.3032, 85.7187 - 3.2>>
	fHostageStart[2] = 22.0567
	vHostageStart[3] = <<5314.7949, -5205.6216, 85.7187 - 3.2>>
	fHostageStart[3] = 151.4604
	
	//Cops
	//	Cops in first cars
	vCopStart[0] = <<5353.8232, -5184.7437, 81.7620>>
	fCopStart[0] = 169.0588
	vCopStart[1] = <<5355.1685, -5182.4038, 81.7620>>
	fCopStart[1] = 61.0588
	vCopStart[2] = <<5353.9199, -5190.8887, 81.7620>>
	fCopStart[2] = 130.9412
	vCopStart[3] = <<5352.0396, -5188.7974, 81.7620>>
	fCopStart[3] = 21.5294
	vCopStart[4] = <<5353.89, -5192.23, 81.7620>>
	fCopStart[4] = 115.6477
	vCopStart[5] = <<5343.4702, -5196.2905, 81.7620>>
	fCopStart[5] = 358.9412
	// Backup cops
	vCopStart[6] = <<5382.5513, -5176.6104, 80.4568>>
	fCopStart[6] = 108.5755
	vCopStart[7] = <<5381.8916, -5175.5967, 80.4709>>
	fCopStart[7] = 123.5640
	vCopStart[8] = <<5381.1890, -5173.6582, 80.4267>>
	fCopStart[8] = 117.5804
	//	Cops for outside car
	vCopStart[9] = <<5389.6899, -5191.0996, 80.2098>>
	fCopStart[9] = 143.6466
	vCopStart[10] = <<5390.5415, -5186.2769, 79.9868>>
	fCopStart[10] = 59.6471
	//	More cop cars pulls up
	vCopStart[11] = <<5410.3872, -5182.0073, 78.6563>>
	fCopStart[11] = 135.1765
	vCopStart[12] = <<5408.5981, -5180.1704, 78.6633>>
	fCopStart[12] = 133.0588
	vCopStart[13] = <<5419.5688, -5171.7134, 77.9652>>
	fCopStart[13] = 201.5294
	vCopStart[14] = <<5411.2510, -5179.0444, 78.4814>>
	fCopStart[14] = 45.5294
	vCopStart[15] = <<5432.5781, -5151.1963, 77.1536>>
	fCopStart[15] = 104.1176
	vCopStart[16] = <<5431.6709, -5148.8687, 77.0620>>
	fCopStart[16] = 106.2353
	//	Driving set piece car
	vCopStart[17] = <<5410.8320, -5177.5654, 79.4830>>
	fCopStart[17] = 115.8309
	//	Road Block
	vCopStart[18] = <<3497.6106, -4872.6333, 110.5807>>
	fCopStart[18] = 262.9412
	vCopStart[19] = <<3497.8025, -4870.1411, 110.7024>>
	fCopStart[19] = 266.0508
	vCopStart[20] = <<3494.8452, -4869.0930, 110.7144>>
	fCopStart[20] = 279.8827
	vCopStart[21] = <<3494.4653, -4866.5808, 110.7753>>
	fCopStart[21] = 280.5879
	vCopStart[22] = <<3496.4709, -4865.4631, 110.7407>>
	fCopStart[22] = 226.2353
	vCopStart[23] = <<3495.5537, -4864.5581, 110.7269>>
	fCopStart[23] = 222.0000
	//	Farm Cops
//	vCopStart[23] = <<3529.1243, -4678.3862, 113.3239>>		//Since farm cops are positioned already
//	fCopStart[23] = 192.3529
	vCopStart[24] = <<3530.2092, -4673.2529, 113.2062>>
	fCopStart[24] = 274.9412
	vCopStart[25] = <<3532.4717, -4673.4292, 113.2055>>
	fCopStart[25] = 189.5294
	vCopStart[26] = <<3533.2830, -4671.6245, 113.2060>>
	fCopStart[26] = 213.2454
	vCopStart[27] = <<3536.7886, -4671.3682, 113.2061>>
	fCopStart[27] = 270.7059
	vCopStart[28] = <<3533.1016, -4666.0059, 113.1611>>
	fCopStart[28] = 176.8235
	vCopStart[29] = <<3542.2075, -4660.9946, 113.4237>>
	fCopStart[29] = 131.6470
	vCopStart[30] = <<3546.6194, -4659.5532, 113.1374>>
	fCopStart[30] = 212.8229
	vCopStart[31] = <<3550.4600, -4651.6206, 113.2091>>
	fCopStart[31] = 92.1176
	vCopStart[32] = <<3552.1580, -4654.5864, 113.2239>>
	fCopStart[32] = 174.4004
	vCopStart[33] = <<3548.8970, -4650.2505, 113.2084>>
	fCopStart[33] = 177.3907
	vCopStart[34] = <<3541.2754, -4662.7432, 113.3224>>
	fCopStart[34] = 255.1765
	//	Farm Respawn Points
	vCopStart[35] = <<3514.9692, -4655.0806, 113.5005>>
	fCopStart[35] = 226.6858
	vCopStart[36] = <<3544.4329, -4643.0356, 113.1429>>
	fCopStart[36] = 233.6201
	vCopStart[37] = <<3560.4609, -4635.5728, 113.8873>>
	fCopStart[37] = 166.5792
	vCopStart[38] = <<3510.5347, -4675.3320, 113.2635>>
	fCopStart[38] = 320.8466
	
	//Route
	vRoutePoint1[0] = <<4587.656250, -5054.247070, 106.987206>>
	vRoutePoint1[1] = <<4558.967285, -5074.121582, 107.657402>>
	vRoutePoint1[2] = <<4528.416016, -5091.893066, 107.540009>>
	vRoutePoint1[3] = <<4487.448730, -5103.481934, 107.970634>>
	vRoutePoint1[4] = <<4438.860840, -5102.146973, 107.903770>>
	vRoutePoint1[5] = <<4337.285645, -5082.368652, 106.054886>>
	vRoutePoint1[6] = <<4067.197998, -5057.169922, 103.584610>>
	vRoutePoint1[7] = <<3966.121582, -5043.051270, 104.908440>>
	vRoutePoint1[8] = <<3880.685791, -5022.592773, 106.335892>>
	vRoutePoint1[9] = <<3782.208740, -4988.326660, 106.596001>>
	vRoutePoint1[10] = <<3630.503418, -4916.526855, 106.600075>>
	vRoutePoint1[11] = <<3570.768311, -4890.154297, 106.696304>>
	vRoutePoint1[12] = <<3546.578369, -4883.235840, 106.738144>>
	vRoutePoint1[13] = <<3514.123047, -4852.970215, 110.168335>>//
	
	vRoutePoint2[0] = <<4552.222656, -5078.522949, 117.635406>>
	vRoutePoint2[1] = <<4515.127441, -5096.684570, 117.544296>>
	vRoutePoint2[2] = <<4471.467285, -5106.235352, 118.198639>>
	vRoutePoint2[3] = <<4426.590332, -5100.460938, 117.977173>>
	vRoutePoint2[4] = <<4317.045898, -5079.535156, 117.869759>>
	vRoutePoint2[5] = <<4054.019531, -5055.593262, 115.677887>>
	vRoutePoint2[6] = <<3955.333984, -5040.765137, 117.076363>>
	vRoutePoint2[7] = <<3862.413086, -5017.158691, 118.460083>>
	vRoutePoint2[8] = <<3771.825195, -4984.122559, 118.267883>>
	vRoutePoint2[9] = <<3621.113037, -4912.028809, 118.616516>>
	vRoutePoint2[10] = <<3561.085938, -4886.648926, 118.709610>>
	vRoutePoint2[11] = <<3477.843750, -4862.327148, 118.788033>>
	vRoutePoint2[12] = <<3512.075195, -4850.826660, 119.892403>>
	vRoutePoint2[13] = <<3544.632080, -4882.229980, 115.740044>>
	
	fRouteWidth[0] = 20.0
	fRouteWidth[1] = 20.0
	fRouteWidth[2] = 20.0
	fRouteWidth[3] = 20.0
	fRouteWidth[4] = 20.0
	fRouteWidth[5] = 20.0
	fRouteWidth[6] = 20.0
	fRouteWidth[7] = 20.0
	fRouteWidth[8] = 20.0
	fRouteWidth[9] = 20.0
	fRouteWidth[10] = 20.0
	fRouteWidth[11] = 20.0
	fRouteWidth[12] = 20.0
	fRouteWidth[13] = 21.0
	
	vRoute[0] = <<4970.8247, -5091.4155, 88.9819>>
	vRoute[1] = <<4905.5083, -5074.9399, 93.4457>>
	vRoute[2] = <<4853.3379, -5078.5552, 95.6620>>
	vRoute[3] = <<4802.6348, -5084.3965, 103.6738>>
	vRoute[4] = <<4749.6914, -5081.7344, 106.0403>>
	vRoute[5] = <<4687.3491, -5076.9678, 104.7117>>
	vRoute[6] = <<4642.8271, -5078.2974, 104.5495>>
	vRoute[7] = <<4584.6670, -5061.9907, 109.1664>>
	vRoute[8] = <<4565.5078, -5068.2544, 109.6636>>
	vRoute[9] = <<4526.3853, -5088.6621, 109.4986>>
	vRoute[10] = <<4483.2266, -5101.4351, 110.0345>>
	vRoute[11] = <<4449.3604, -5100.7881, 109.9771>>
	vRoute[12] = <<4413.3799, -5095.3276, 110.1241>>
	vRoute[13] = <<4372.2852, -5087.9482, 109.9770>>
	vRoute[14] = <<4324.6626, -5080.5264, 109.9992>>
	vRoute[15] = <<4292.9604, -5075.0874, 109.5519>>
	vRoute[16] = <<4245.4780, -5071.1367, 109.7107>>
	vRoute[17] = <<4204.5264, -5067.8569, 110.2702>>
	vRoute[18] = <<4163.5806, -5064.1592, 108.8908>>
	vRoute[19] = <<4118.8467, -5061.0415, 107.8974>>
	vRoute[20] = <<4079.5952, -5057.5542, 107.4726>>
	vRoute[21] = <<4035.8784, -5052.0747, 107.7924>>
	vRoute[22] = <<3989.1301, -5045.0024, 108.5552>>
	vRoute[23] = <<3942.5750, -5035.9556, 109.3346>>
	vRoute[24] = <<3898.9187, -5026.5864, 110.1669>>
	vRoute[25] = <<3857.7480, -5013.5630, 110.4648>>
	vRoute[26] = <<3817.7344, -4999.7656, 110.7629>>
	vRoute[27] = <<3778.0940, -4985.4976, 110.4797>>
	vRoute[28] = <<3741.1372, -4970.3735, 110.1673>>
	vRoute[29] = <<3700.5952, -4951.1714, 110.6960>>
	vRoute[30] = <<3660.6372, -4930.7935, 110.6758>>
	vRoute[31] = <<3627.8958, -4913.8423, 110.6055>>
	vRoute[32] = <<3609.6926, -4905.1797, 110.6365>>
	vRoute[33] = <<3574.8577, -4890.7861, 110.6897>>
	vRoute[34] = <<3543.4487, -4876.3232, 110.6801>>
	vRoute[35] = <<3526.9912, -4858.0391, 110.6590>>
	vRoute[36] = <<3522.7896, -4836.8965, 110.8203>>
	vRoute[37] = <<3525.2629, -4812.3511, 110.9858>>
	vRoute[38] = <<3526.6951, -4799.7651, 111.0327>>
	vRoute[39] = <<3528.1250, -4785.9927, 111.0324>>
ENDPROC

// RJM - I needed the general cop attribute setup stuff to be separated from CreateCop(), because
// my new enemies weren't part of the pedCop[] array.
PROC SETUP_COP_ATTRIBUTES(PED_INDEX &pedIndex)
	IF NOT IS_ENTITY_DEAD(pedIndex)
		SET_PED_RANDOM_COMPONENT_VARIATION(pedIndex)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedIndex, relGroupEnemy)
		SET_PED_AS_ENEMY(pedIndex, TRUE)
		SET_ENTITY_HEALTH(pedIndex, 200)
		SET_PED_MAX_HEALTH(pedIndex, 200)
		SET_PED_DIES_WHEN_INJURED(pedIndex, TRUE)
		SET_PED_ACCURACY(pedIndex, 1)
		GIVE_WEAPON_TO_PED(pedIndex, wtPistol, INFINITE_AMMO, TRUE)
		SET_PED_COMBAT_MOVEMENT(pedIndex, CM_WILLADVANCE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_FLEE_WHILST_IN_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedIndex, CA_USE_VEHICLE, FALSE)
		SET_PED_CONFIG_FLAG(pedIndex, PCF_DisableHurt, TRUE)
		SET_PED_CONFIG_FLAG(pedIndex, PCF_CanAttackNonWantedPlayerAsLaw, TRUE)
		SET_PED_CONFIG_FLAG(pedIndex, PCF_DontBlipCop, TRUE)
	ENDIF
ENDPROC

PROC createCop(INT i)
	IF NOT ARE_VECTORS_EQUAL(vCopStart[i], VECTOR_ZERO)
		MODEL_NAMES modelName
		
		modelName = S_M_M_SNOWCOP_01
		
		SPAWN_PED(pedCop[i], modelName, vCopStart[i], fCopStart[i])
		
		SETUP_COP_ATTRIBUTES(pedCop[i])
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[i], TRUE)
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL tDebugName = "Cop "
			tDebugName += i
			
			SET_PED_NAME_DEBUG(pedCop[i], tDebugName)
		#ENDIF
		
		//Stats
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(pedCop[i])
	ENDIF
ENDPROC

PROC MOVE_SHOOT_PEDS_TO_COORD(PED_INDEX &pedMove, PED_INDEX &pedTarget, VECTOR vCoord, FLOAT moveState)
	IF NOT IS_PED_INJURED(pedMove)
		IF DOES_ENTITY_EXIST(pedTarget)
			IF IS_PED_INJURED(pedTarget)
				CLEAR_PED_TASKS(pedMove)
				
				BOOL bEnemiesAlive = FALSE
				
				FLOAT fDistance = 10000.0 //High number that the peds will all be closer than
				
				INT i
				
				REPEAT COUNT_OF(pedCop) i
					IF NOT IS_PED_INJURED(pedCop[i])
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedMove), GET_ENTITY_COORDS(pedCop[i])) < fDistance
							pedTarget = pedCop[i]
							fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedMove), GET_ENTITY_COORDS(pedCop[i]))
						ENDIF
						
						bEnemiesAlive = TRUE
					ENDIF
				ENDREPEAT
				
				IF bEnemiesAlive = TRUE
					IF NOT IS_ENTITY_AT_COORD(pedMove, vCoord, <<2.0, 2.0, 2.0>>)
						IF GET_SCRIPT_TASK_STATUS(pedMove, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(pedMove, vCoord, pedTarget, moveState, TRUE)
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(pedMove, SCRIPT_TASK_SHOOT_AT_ENTITY) <> PERFORMING_TASK
							TASK_SHOOT_AT_ENTITY(pedMove, pedTarget, -1, FIRING_TYPE_RANDOM_BURSTS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Triggers multiple bullet hits around the player at given intervals.
//AUTHOR: Ross Wallace
PROC DO_EXCITING_NEAR_BULLET_MISS_ON_COORD(VECTOR coordToMiss, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, 
										INT timeBetweenBullets = 60, FLOAT minXrange = -3.9, FLOAT maxXrange = -1.0, FLOAT minYRange = -2.9, FLOAT maxYrange = 3.9)
	//Fire bullets at the player as from the bad guy...
	INT currentBulletTime = GET_GAME_TIMER()
	VECTOR bulletHit
	VECTOR bulletOrigin
	
	IF ((currentBulletTime - iControlTimer) > timeBetweenBullets)
		IF DOES_ENTITY_EXIST(sourceOfBullets)
		AND NOT IS_PED_INJURED(sourceOfBullets)
			IF HAS_PED_GOT_FIREARM(sourceOfBullets)
				bulletHit = coordToMiss + <<GET_RANDOM_FLOAT_IN_RANGE(minXrange, maxXrange), GET_RANDOM_FLOAT_IN_RANGE(minYRange, maxYrange), 0.0>>
				bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
				GET_GROUND_Z_FOR_3D_COORD(bulletHit, bulletHit.z)           
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, bulletHit, 1, FALSE, WEAPONTYPE_ASSAULTRIFLE, sourceOfBullets)
				iControlTimer = currentBulletTime
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_BUDDY_AMMO()
	IF NOT HAS_PED_GOT_WEAPON(notPlayerPedID, wtPistol)
		GIVE_WEAPON_TO_PED(notPlayerPedID, wtPistol, GET_WEAPON_CLIP_SIZE(wtPistol))
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(notPlayerPedID, wtPistol)
		IF GET_AMMO_IN_PED_WEAPON(notPlayerPedID, wtPistol) < GET_WEAPON_CLIP_SIZE(wtPistol)
			SET_PED_AMMO(notPlayerPedID, wtPistol, GET_WEAPON_CLIP_SIZE(wtPistol) * 2)
		ENDIF
	ENDIF
	
	IF NOT HAS_PED_GOT_WEAPON(notPlayerPedID, wtCarbineRifle)
		GIVE_WEAPON_TO_PED(notPlayerPedID, wtCarbineRifle, GET_WEAPON_CLIP_SIZE(wtCarbineRifle))
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(notPlayerPedID, wtCarbineRifle)
		IF GET_AMMO_IN_PED_WEAPON(notPlayerPedID, wtCarbineRifle) < GET_WEAPON_CLIP_SIZE(wtCarbineRifle)
			SET_PED_AMMO(notPlayerPedID, wtCarbineRifle, GET_WEAPON_CLIP_SIZE(wtCarbineRifle) * 2)
		ENDIF
	ENDIF
	
	WEAPON_TYPE wtCurrent = WEAPONTYPE_UNARMED
	
	GET_CURRENT_PED_WEAPON(notPlayerPedID, wtCurrent)
	
	IF IS_PED_IN_ANY_VEHICLE(notPlayerPedID)
		IF wtCurrent != wtPistol
			SET_CURRENT_PED_WEAPON(notPlayerPedID, wtPistol, TRUE)
		ENDIF
	ELSE
		IF wtCurrent != wtCarbineRifle
			SET_CURRENT_PED_WEAPON(notPlayerPedID, wtCarbineRifle, TRUE)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Triggers multiple bullet hits around the player at given intervals.
//AUTHOR: Ross Wallace
PROC DO_EXCITING_BULLET_ON_COORD(VECTOR coordToHit, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, 
										INT timeBetweenBullets = 60)
	//Fire bullets at the player as from the bad guy...
	INT currentBulletTime = GET_GAME_TIMER()
	VECTOR bulletOrigin
	
	IF ((currentBulletTime - iControlTimer) > timeBetweenBullets)
		IF NOT IS_ENTITY_DEAD(sourceOfBullets)            
			bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
			GET_GROUND_Z_FOR_3D_COORD(coordToHit, coordToHit.z)           
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, coordToHit, 1, FALSE, WEAPONTYPE_ASSAULTRIFLE, sourceOfBullets)
			iControlTimer = currentBulletTime
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_HEADING_FROM_VECTOR(VECTOR vVector)
	RETURN vVector.Z
ENDFUNC

//-----------------------------¦ SETUP PROCEDURES ¦------------------------------

//---------------------------¦ OBJECTIVE PROCEDURES ¦----------------------------

PROC initialiseMission()
	IF INIT_STAGE()
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
		
		SET_AUDIO_FLAG("PoliceScannerDisabled", TRUE)
		
		SET_GAMEPLAY_CAM_ALTITUDE_FOV_SCALING_STATE(FALSE)
		
		SET_ALLOW_STREAM_PROLOGUE_NODES(TRUE)
		
		ALLOW_SONAR_BLIPS(FALSE)
		
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_YANKTON_CEMETARY", FALSE, TRUE)
		
		SET_AMBIENT_VEHICLE_NEON_ENABLED(FALSE)
		
		//Prevent Recording
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		//INFORM_MISSION_STATS_OF_MISSION_START_PROLOGUE()
		
		//Prepare Mission
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(0)
		ENDIF
		
		CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
		
		//Special Ability
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		
		SET_PED_POSITION(playerPedID, vPlayerStart, fPlayerStart)
		
		//Switch Selector
		DISABLE_SELECTOR()
		
		//Ped
		SET_PED_MODEL_IS_SUPPRESSED(A_F_M_PROLHOST_01, TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(A_M_M_PROLHOST_01, TRUE)
		SET_PED_MODEL_IS_SUPPRESSED(S_M_M_ARMOURED_01, TRUE)
		
		//Request Additional Text
		REQUEST_ADDITIONAL_TEXT("PROLOG", MISSION_TEXT_SLOT)
		//REQUEST_ADDITIONAL_TEXT("PROAUD", MISSION_DIALOGUE_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING TEXT")	#ENDIF
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
		
		//Particle Effects
		REQUEST_PTFX_ASSET()
		
		//Particles
		WHILE NOT HAS_PTFX_ASSET_LOADED()
			WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING PTFX")	#ENDIF
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
		
		//Doors
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iCupboardDoor)
			ADD_DOOR_TO_SYSTEM(iCupboardDoor, V_ILev_CD_Door2, <<5316.65, -5205.74, 83.67>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iLeftReceptionDoor)
			ADD_DOOR_TO_SYSTEM(iLeftReceptionDoor, V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iRightReceptionDoor)
			ADD_DOOR_TO_SYSTEM(iRightReceptionDoor, V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iCCTVRoomDoor)
			ADD_DOOR_TO_SYSTEM(iCCTVRoomDoor, v_ilev_cd_door3, <<5305.46, -5177.75, 83.67>>)
		ENDIF
		
		//Score
		REGISTER_SCRIPT_WITH_AUDIO()
		
		//LOAD_AUDIO(PROLOGUE_TEST_MISSION_START)
		
		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("Prologue_Foley")
			WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING AUDIO BANK = Prologue_Foley")	#ENDIF
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
		
		//Relationships
		ADD_RELATIONSHIP_GROUP("FRIEND", relGroupFriendlyFire)
		ADD_RELATIONSHIP_GROUP("BUDDIES", relGroupBuddy)
		ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
		
		//Buddy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupBuddy)
		
		//Friendly Fire
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendlyFire, RELGROUPHASH_PLAYER)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendlyFire, relGroupBuddy)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupEnemy, relGroupFriendlyFire)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendlyFire, relGroupEnemy)
		
		//Enemy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBuddy, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, relGroupBuddy)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, RELGROUPHASH_PLAYER)
		
		#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widGroup)
				widGroup = START_WIDGET_GROUP("Prologue")
					START_WIDGET_GROUP("LightingRig")
						 ADD_WIDGET_BOOL("Debug Draw Rigs", bDebugLightingRig)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("First Person Switch Cam")
						 ADD_WIDGET_BOOL("Side Anim?", bDebugSwitchCamAnimSide)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Credits")
					    ADD_WIDGET_BOOL("Display test text", bDebugDisplayCreditsText)
					    ADD_WIDGET_BOOL("Clear test text", bDebugClearCreditsText)
					    ADD_WIDGET_FLOAT_SLIDER("Credits block X", fDebugCreditsBlockX, -1000.0, 1000.0, 1.0)
					    ADD_WIDGET_FLOAT_SLIDER("Credits block Y", fDebugCreditsBlockY, -1000.0, 1000.0, 1.0)
					    textWidgetCreditsAlign = ADD_TEXT_WIDGET("Align")
					   
					    ADD_WIDGET_STRING("")
					    
					    textWidgetText1 = ADD_TEXT_WIDGET("Text 1")
						textWidgetFont1 = ADD_TEXT_WIDGET("Font 1")
						textWidgetCreditsColour1 = ADD_TEXT_WIDGET("Colour 1")
					    textWidgetText2 = ADD_TEXT_WIDGET("Text 2")
						textWidgetFont2 = ADD_TEXT_WIDGET("Font 2")
						textWidgetCreditsColour2 = ADD_TEXT_WIDGET("Colour 2")
					    textWidgetText3 = ADD_TEXT_WIDGET("Text 3")
						textWidgetFont3 = ADD_TEXT_WIDGET("Font 3")
						textWidgetCreditsColour3 = ADD_TEXT_WIDGET("Colour 3")
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Debug Print")
						ADD_WIDGET_BOOL("Doors", bDebugDoors)
						ADD_WIDGET_BOOL("Help Text", bDebugHelpText)
						ADD_WIDGET_BOOL("Audio", bDebugAudio)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Debug")
						ADD_WIDGET_FLOAT_SLIDER("fDistIntoCover", fDistIntoCover, 0.0, 10.0, 0.01)
						ADD_WIDGET_FLOAT_SLIDER("fTimeIntoCover", fTimeIntoCover, 0.0, 10.0, 0.01)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				SET_LOCATES_HEADER_WIDGET_GROUP(widGroup)
			ENDIF
		#ENDIF
		
		//Variables
		initialiseVariables()
		
		//IPL Groups - Interior
		REQUEST_IPL("prologue06_int")
		
		//Interior
		intDepot = GET_INTERIOR_AT_COORDS_WITH_TYPE(vPlayerStart, "V_CashDepot")
		
		PIN_INTERIOR_IN_MEMORY(intDepot)
			
		PRINTLN("Pinning interior \"V_CashDepot\"")
		
		WHILE NOT IS_INTERIOR_READY(intDepot)
			WAIT(0)	PRINTLN("Waiting for interior \"V_CashDepot\" to become ready.")
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
		
		//IPL Groups
		//NB: if these change remember to notfiy AI code so they can update the "GTA5_IplGroup.xml" used to create navmeshes
		REQUEST_IPL("prologue01")
		REQUEST_IPL("prologue02")
		REQUEST_IPL("prologue03")
		REQUEST_IPL("prologue04")
		REQUEST_IPL("prologue05")
		REQUEST_IPL("prologue06")
		REQUEST_IPL("prologuerd")
		
		//REQUEST_IPL("Prologue01b")
		REQUEST_IPL("Prologue01c")
		REQUEST_IPL("Prologue01d")
		REQUEST_IPL("Prologue01e")
		REQUEST_IPL("Prologue01f")
		REQUEST_IPL("Prologue01g")
		REQUEST_IPL("prologue01h")
		REQUEST_IPL("prologue01i")
		REQUEST_IPL("prologue01j")
		REQUEST_IPL("prologue01k")
		REQUEST_IPL("prologue01z")
		REQUEST_IPL("prologue03b")
		REQUEST_IPL("prologue04b")
		REQUEST_IPL("prologue05b")
		REQUEST_IPL("prologue06b")
		REQUEST_IPL("prologuerdb")
		
		REQUEST_IPL("prologue_occl")
		
		REQUEST_IPL("DES_ProTree_start")
		REQUEST_IPL("DES_ProTree_start_lod")
		
		REQUEST_IPL("prologue04_cover")
		
		REQUEST_IPL("prologue03_grv_fun")	//REQUEST_IPL("prologue03_grv_dug")	//prologue03_grv_cov
		
		REQUEST_IPL("prologue_LODLights") REMOVE_IPL_FROM_REMOVAL_LIST("prologue_LODLights")
		REQUEST_IPL("prologue_DistantLights") REMOVE_IPL_FROM_REMOVAL_LIST("prologue_DistantLights")
		
		REMOVE_IPL("prologue03_grv_dug")
		REMOVE_IPL("prologue_grv_torch")
		
		//Cullbox
		SET_MAPDATACULLBOX_ENABLED("Prologue_Main", TRUE)
		
		//Clear Area
		CLEAR_AREA(vPlayerStart, 1000.0, TRUE)
		
		//Scenario Blocking
		ADD_SCENARIO_BLOCKING_AREA(vPlayerStart - <<1000.0, 1000.0, 1000.0>>, vPlayerStart + <<1000.0, 1000.0, 1000.0>>)
		
		//Map Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5297.0796, -5182.9907, 82.5187>>)
			covPointMap[0] = ADD_COVER_POINT(<<5297.0796, -5182.9907, 82.5187>>, 274.0717, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5315.5337, -5182.3809, 82.5186>>)
			covPointMap[1] = ADD_COVER_POINT(<<5315.5337, -5182.3809, 82.5186>>, 182.4798, COVUSE_WALLTONEITHER, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5364.8784, -5182.8281, 81.6339>>)
			covPointMap[2] = ADD_COVER_POINT(<<5364.8784, -5182.8281, 81.6339>>, 88.5395, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5364.8784, -5182.8281, 81.6339>>)
			covPointMap[3] = ADD_COVER_POINT(<<5364.8784, -5182.8281, 81.6339>>, 88.5395, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5364.8784, -5182.8281, 81.6339>>)
			covPointMap[4] = ADD_COVER_POINT(<<5364.8784, -5182.8281, 81.6339>>, 88.5395, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
		ENDIF
		
		//Ambient
		SET_AMBIENT_ZONE_LIST_STATE("ZONE_LIST_YANKTON", TRUE) 
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		//Ambulances etc.
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
		SET_CREATE_RANDOM_COPS(FALSE)
		
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		SET_RANDOM_TRAINS(FALSE)
		
		//Weather
		SET_WEATHER_TYPE_NOW_PERSIST("SNOW")
		
		LOAD_CLOUD_HAT("RAIN")	//LOAD_CLOUD_HAT("Snowy 01")
		
		SET_CLOCK_DATE(15, DECEMBER, 2003)
		
		SET_CLOCK_TIME(5, 0, 0)
		
		//Minimap
		SET_MINIMAP_IN_PROLOGUE_AND_HANDLE_STATIC_BLIPS(TRUE)
		
		//Phone
		g_Use_Prologue_Cellphone = TRUE
		
		//Phone
		DISABLE_CELLPHONE(TRUE)
		
		//Shutter Door Object
		bGarageVisible = TRUE
		bGrabShutter = FALSE
		
		//Set player
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
			WAIT(0)
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
		
		UPDATE_PED_REFERENCES()
		
		SET_PED_POSITION(playerPedID, vPlayerStart, fPlayerStart)
		
		CLEAR_PED_WETNESS(playerPedID)
		
		SET_PED_TARGET_LOSS_RESPONSE(playerPedID, TLR_NEVER_LOSE_TARGET)
		
		SET_PED_CONFIG_FLAG(playerPedMichael, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(playerPedMichael, PCF_DisableExplosionReactions, TRUE)
		
		STOP_PED_SPEAKING(playerPedMichael, TRUE)
		
		//Weapon
		REMOVE_ALL_PED_WEAPONS(playerPedID)
		GIVE_WEAPON_TO_PED(playerPedID, wtCarbineRifle, 500, TRUE)
		SET_PED_AMMO(playerPedID, wtCarbineRifle, 500)
		
		SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
		
		//Variations
		//Clear Variations and Props
		SET_PED_DEFAULT_COMPONENT_VARIATION(playerPedID)
		CLEAR_ALL_PED_PROPS(playerPedID)
		
		//Face
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HEAD, 0, 3)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HAIR, 1, 0)
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL, 7, 0)
		
		//Clothes
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HAND, 5, 0)
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_TORSO, 31, 0)
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_LEG, 26, 0)
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_FEET, 14, 0)
		
		//Duffle Bag
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL2, 11, 0)
		
		//Cleanup Michael
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		
		//Spawn Trevor
		SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vTrevorStart, fTrevorStart)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
		SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_DisableExplosionReactions, TRUE)
		SET_PED_CAN_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
		REMOVE_ALL_PED_WEAPONS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, 500, TRUE)
		SET_PED_AMMO(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, 500)
		SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 50)
		SET_PED_TARGET_LOSS_RESPONSE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TLR_NEVER_LOSE_TARGET)
		SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		
		STOP_PED_SPEAKING(playerPedTrevor, TRUE)
		
		//Variations
		//Clear Variations and Props
		SET_PED_DEFAULT_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		CLEAR_ALL_PED_PROPS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		
		//Face
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HEAD, 0, 5)
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_BERD, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAIR, 1, 0)
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL, 13, 0)
		SET_PED_PROP_INDEX(playerPedTrevor, ANCHOR_EYES, 4)
		
		//Clothes
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_TORSO, 9, 0)
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_LEG, 9, 0)
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_FEET, 12, 0)
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_HAND, 4, 0)
		
		//Duffle Bag
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 1, 0)
		
		//Cleanup Trevor
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
		
		//Spawn Brad
		SPAWN_PED(pedBrad, IG_BRAD, vBuddyStart, fBuddyStart)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_DisableHurt, TRUE)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_DisableExplosionReactions, TRUE)
		RETAIN_ENTITY_IN_INTERIOR(pedBrad, intDepot)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, TRUE)
		SET_PED_CAN_RAGDOLL(pedBrad, FALSE)
		SET_PED_CAN_BE_TARGETTED(pedBrad, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedBrad, relGroupBuddy)
		GIVE_WEAPON_TO_PED(pedBrad, wtPistol, INFINITE_AMMO, TRUE)
		GIVE_WEAPON_TO_PED(pedBrad, wtShotgun, INFINITE_AMMO, TRUE)
		SET_PED_ACCURACY(pedBrad, 50)
		SET_PED_TARGET_LOSS_RESPONSE(pedBrad, TLR_NEVER_LOSE_TARGET)
		SAFE_ADD_BLIP_PED(blipBuddy, pedBrad, FALSE)
		
		STOP_PED_SPEAKING(pedBrad, TRUE)
		
		//Dialogue
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedID, "MICHAEL")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, pedBrad, "BRAD")
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_HAIR, 1, 0)
		
		//Mask
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_FEET, 1, 0)
		
		//Cleanup
		SET_MODEL_AS_NO_LONGER_NEEDED(IG_BRAD)
		
		//Wait for variations to load...
		WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedMichael)
		OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedTrevor)
		OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedBrad)
			WAIT_WITH_DEATH_CHECKS(0)			#IF IS_DEBUG_BUILD	PRINTLN("HAVE_ALL_STREAMING_REQUESTS_COMPLETED: Michael | Trevor | Brad")	#ENDIF
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
	ELSE
		//Prevent Recording
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		ADVANCE_STAGE()
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Your mission is being replayed
		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted = TRUE
				MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE() + 1))
			ELSE
				MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE()))
			ENDIF
			
			IF INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE()) > replayStart
				iCurrentTake = g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE]		#IF IS_DEBUG_BUILD	PRINTLN("REPLAY | g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
				iDisplayedTake = iCurrentTake
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayStart), "initMission")
	ENDIF
ENDPROC

PROC cutsceneIntro()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Prevent Recording
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Portraits
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, FALSE)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TRV_PRO_MASK_REMOVED, FALSE)
		
		//Hostages
		//Guy
		SPAWN_PED(pedHostage[HostageMale1], A_M_M_PROLHOST_01, vHostageStart[0], fHostageStart[0])
		SET_PED_CONFIG_FLAG(pedHostage[HostageMale1], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedHostage[HostageMale1], PCF_DisableExplosionReactions, TRUE)
		SET_RAGDOLL_BLOCKING_FLAGS(pedHostage[HostageMale1], RBF_PLAYER_IMPACT)
		SET_PED_MAX_HEALTH(pedHostage[HostageMale1], 101)
		SET_ENTITY_HEALTH(pedHostage[HostageMale1], 101)
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, pedHostage[HostageMale1], "PROHOSTAGE2")
		SET_ENTITY_INVINCIBLE(pedHostage[HostageMale1], TRUE)
		
		//Guard
		SPAWN_PED(pedHostage[HostageGuard], IG_PROLSEC_02, vHostageStart[1], fHostageStart[1])
		SET_PED_CONFIG_FLAG(pedHostage[HostageGuard], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedHostage[HostageGuard], PCF_DisableExplosionReactions, TRUE)
		SET_RAGDOLL_BLOCKING_FLAGS(pedHostage[HostageGuard], RBF_PLAYER_IMPACT)
		SET_PED_MAX_HEALTH(pedHostage[HostageGuard], 101)
		SET_ENTITY_HEALTH(pedHostage[HostageGuard], 101)
		SET_ENTITY_INVINCIBLE(pedHostage[HostageGuard], TRUE)
		
		//Girl
		SPAWN_PED(pedHostage[HostageFemale], A_F_M_PROLHOST_01, vHostageStart[2], fHostageStart[2])
		SET_PED_CONFIG_FLAG(pedHostage[HostageFemale], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedHostage[HostageFemale], PCF_DisableExplosionReactions, TRUE)
		SET_RAGDOLL_BLOCKING_FLAGS(pedHostage[HostageFemale], RBF_PLAYER_IMPACT)
		SET_PED_MAX_HEALTH(pedHostage[HostageFemale], 101)
		SET_ENTITY_HEALTH(pedHostage[HostageFemale], 101)
		SET_PED_AO_BLOB_RENDERING(pedHostage[HostageFemale], FALSE)	//Shadows
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedHostage[HostageFemale], "PROHOSTAGE")
		SET_ENTITY_INVINCIBLE(pedHostage[HostageFemale], TRUE)
		
		SET_PED_CAN_BE_TARGETTED(pedHostage[HostageFemale], FALSE)
		
		//Guy
		SPAWN_PED(pedHostage[HostageMale2], A_M_M_PROLHOST_01, vHostageStart[3], fHostageStart[3])
		SET_PED_CONFIG_FLAG(pedHostage[HostageMale2], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedHostage[HostageMale2], PCF_DisableExplosionReactions, TRUE)
		SET_RAGDOLL_BLOCKING_FLAGS(pedHostage[HostageMale2], RBF_PLAYER_IMPACT)
		SET_PED_MAX_HEALTH(pedHostage[HostageMale2], 101)
		SET_ENTITY_HEALTH(pedHostage[HostageMale2], 101)
		SET_PED_COMPONENT_VARIATION(pedHostage[HostageMale2], PED_COMP_HEAD, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedHostage[HostageMale2], PED_COMP_TORSO, 1, 0)
		SET_ENTITY_INVINCIBLE(pedHostage[HostageMale2], TRUE)
		
		//Cleanup Hostages
		SET_MODEL_AS_NO_LONGER_NEEDED(IG_PROLSEC_02)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_F_M_PROLHOST_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_PROLHOST_01)
		
		INT i
		
		//Blips
//		REPEAT COUNT_OF(pedHostage) i
//			SAFE_ADD_BLIP_PED(blipHostage[i], pedHostage[i], TRUE)
//		ENDREPEAT
		
		SAFE_ADD_BLIP_PED(blipHostage[HostageGuard], pedHostage[HostageGuard], TRUE)
		
		REPEAT COUNT_OF(pedHostage) i
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedHostage[i], TRUE)
			//SET_PED_CAN_RAGDOLL(pedHostage[i], FALSE)
		ENDREPEAT
		
		//Player Anim
		
		//Anims on hostages
		
		//Door
		IF NOT DOES_ENTITY_EXIST(objDoor)
			objDoor = CREATE_OBJECT_NO_OFFSET(V_ILEV_CD_ENTRYDOOR, vDoorPosition)
			FREEZE_ENTITY_POSITION(objDoor, TRUE)
		ENDIF
		
		//Assisted
		ASSISTED_MOVEMENT_REQUEST_ROUTE("Pro_S1")
		
		//Radar
		bRadar = FALSE
		
		//Camera
		IF NOT DOES_CAM_EXIST(camMain)
			camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		ENDIF
		
		SET_CAM_PARAMS(camMain,
						<<5314.385742, -5210.130859, 83.931236>>, 
						<<1.569747, 0.0, 83.868217>>, 
						22.585051)
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_CashD_reception")
		
		//Prevent Recording
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		//Score
		SET_FRONTEND_RADIO_ACTIVE(FALSE) //TEMPORARY
		
		//Load Scene
		LOAD_SCENE_ADV(vPlayerStart)
		LOAD_AUDIO(PROLOGUE_TEST_HOSTAGES)
		
		// Wait for the install screen to finish.
		#IF IS_DEBUG_BUILD	PRINTLN("PROLOGUE WAITING FOR INSTALL SCREEN TO FINISH")	#ENDIF
		WHILE (NOT IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_INSTALL_SCREEN_FINISHED))
		AND NOT (IS_AUTOSAVE_REQUEST_IN_PROGRESS() OR (g_sAutosaveData.iQueuedRequests > 0))
		AND NOT IS_REPLAY_IN_PROGRESS()
		AND NOT IS_REPEAT_PLAY_ACTIVE()
			WAIT_WITH_DEATH_CHECKS(0)
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		ENDWHILE
		#IF IS_DEBUG_BUILD	PRINTLN("INSTALL SCREEN HAS FINISHED")	#ENDIF
		
		
		PLAY_AUDIO(PROLOGUE_TEST_MISSION_START)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
		
		//SET_WARNING_MESSAGE("PRO_SETTING", FE_WARNING_NONE)
		
		WHILE NOT PREPARE_SYNCHRONIZED_AUDIO_EVENT("PRO_IG_1", 0)
			#IF IS_DEBUG_BUILD	PRINTLN("PREPARE_SYNCHRONIZED_AUDIO_EVENT - PRO_IG_1")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			//Prevent Recording
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
			AUDIO_CONTROLLER()
		ENDWHILE
		
		//Ends the artwork loading screen if it is still displaying.
		SHUTDOWN_LOADING_SCREEN()
		
		//Fix for 1654536. Ensure the player is unfrozen as the MP character
		//creator can leave them frozen.
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		
		SETTIMERA(0)
		
		LOCK_MINIMAP_ANGLE(15)
		
		IF NOT DOES_ENTITY_EXIST(objLightingRig[LIGHTING_RIG_OPENING])
			objLightingRig[LIGHTING_RIG_OPENING] = CREATE_OBJECT(PROP_1ST_PROLOGUE_SCENE, <<5310.661, -5211.665, 81.575>>)
			FORCE_ROOM_FOR_ENTITY(objLightingRig[LIGHTING_RIG_OPENING], intDepot, HASH("V_CashD_reception"))
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Request Cutscene
		REQUEST_CUTSCENE("pro_mcs_1")
		
		IF SKIPPED_STAGE()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(1000)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - pro_mcs_1
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_SecurityGuard_Tiedup", pedHostage[HostageGuard])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Male_Hostage_1", pedHostage[HostageMale1])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Male_Hostage_2", pedHostage[HostageMale2])
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				FLOAT fFontHeight
				
				SET_ENTITY_HEADING(playerPedID, fPlayerStart)
				
				IF GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_KOREAN
				OR GET_CURRENT_LANGUAGE() = LANGUAGE_CHINESE_SIMPLIFIED
					SET_TEXT_SCALE(0.675, 0.675)	//150% larger font
					
					fFontHeight = GET_RENDERED_CHARACTER_HEIGHT(0.675)
				ELSE	//Other Languages
					SET_TEXT_SCALE(0.45, 0.45)
					
					fFontHeight = GET_RENDERED_CHARACTER_HEIGHT(0.45)
				ENDIF
				
				SET_TEXT_CENTRE(TRUE)
				
		        SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				
		        BEGIN_TEXT_COMMAND_DISPLAY_TEXT("PRO_SETTING")
		        END_TEXT_COMMAND_DISPLAY_TEXT(0.5, 0.5 - (fFontHeight / 2))
				
		        RESET_SCRIPT_GFX_ALIGN()
				
				HIDE_LOADING_ON_FADE_THIS_FRAME()
				
				//Prevent Recording
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				
				IF TIMERA() > 4500
					//Sync Scene (Michae + Trevor + Brad + Female Hostage + Door)
					sceneIntro = CREATE_SYNCHRONIZED_SCENE(sceneIntroPos, sceneIntroRot)
					
					//Michael
					TASK_SYNCHRONIZED_SCENE(playerPedID, sceneIntro, sAnimDictPrologue1, "michael_main_player_zero", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID)
					
					//Trevor
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneIntro, sAnimDictPrologue1, "main_player_two", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					
					//Brad
					TASK_SYNCHRONIZED_SCENE(pedBrad, sceneIntro, sAnimDictPrologue1, "main_brad", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
					
					SET_CURRENT_PED_WEAPON(pedBrad, wtShotgun, TRUE)
					
					//Female Hostage
					TASK_SYNCHRONIZED_SCENE(pedHostage[HostageFemale], sceneIntro, sAnimDictPrologue1, "main_femalehostage", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageFemale])
					
					//Door
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objDoor, sceneIntro, "Main_EntryDoor", sAnimDictPrologue1, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					
					//Audio
					PLAY_SYNCHRONIZED_AUDIO_EVENT(sceneIntro)
					
					//Male Hostage 01
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue1, "main_malehostage01", sceneIntroPos, sceneIntroRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue1, "idle_loop_malehostage01", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedHostage[HostageMale1], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					//Male Hostage 02
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue1, "main_malehostage02", sceneIntroPos, sceneIntroRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue1, "idle_loop_malehostage02", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedHostage[HostageMale2], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					//Guard
//					OPEN_SEQUENCE_TASK(seqMain)
//						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue1, "main_gaurd", sceneIntroPos, sceneIntroRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue1, "idle_loop_gaurd", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//					CLOSE_SEQUENCE_TASK(seqMain)
//					TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
//					CLEAR_SEQUENCE_TASK(seqMain)
					
					TASK_SYNCHRONIZED_SCENE(pedHostage[HostageGuard], sceneIntro, sAnimDictPrologue1, "main_gaurd", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageGuard])
					
					PLAY_FACIAL_ANIM(pedHostage[HostageGuard], "main_gaurd_facial", sAnimDictPrologue1)
					
					//Camera
					IF NOT DOES_CAM_EXIST(camAnim)
						camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					ENDIF
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					PLAY_CAM_ANIM(camAnim, "main_camera", sAnimDictPrologue1, sceneIntroPos, sceneIntroRot)
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				IF TIMERA() > 500
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
				AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneIntro)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) > 0.210	//0.243
					//Audio Scene
					IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_INSIDE_OFFICE")
						START_AUDIO_SCENE("PROLOGUE_GET_INSIDE_OFFICE")
					ENDIF
					
					IF IS_RADAR_PREFERENCE_SWITCHED_ON()
						PRINT_HELP_ADV(PROHLP_RADAR, "PROHLP_RADAR", DEFAULT, (DEFAULT_HELP_TEXT_TIME / 4) * 3)
						
						SETTIMERB(0)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
				AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneIntro)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.5	//0.567
					APPLY_PED_BLOOD_SPECIFIC(pedHostage[HostageGuard], ENUM_TO_INT(PDZ_HEAD), 0.354, 0.696, 0.0, 1.0, 0, 0.0, "BulletLarge")
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 4
//				IF NOT HAS_LABEL_BEEN_TRIGGERED(Turning)
//					IF NOT IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue1, "michael_main_player_zero")
//						TASK_GO_STRAIGHT_TO_COORD(playerPedID, GET_ENTITY_COORDS(playerPedID), PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 20.0871)
//						
//						SET_LABEL_AS_TRIGGERED(Turning, TRUE)
//					ENDIF
//				ENDIF
				
				IF IS_CAM_PLAYING_ANIM(camAnim, "main_camera", sAnimDictPrologue1)
				AND GET_CAM_ANIM_CURRENT_PHASE(camAnim) >= 0.99
				OR NOT IS_CAM_PLAYING_ANIM(camAnim, "main_camera", sAnimDictPrologue1)
					IF GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					OR IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5316.579590, -5205.753906, 82.518654>>, <<5305.574707, -5205.736816, 86.518654>>, 5.0)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMN_FPSHELP")
							SAFE_CLEAR_HELP()
						ENDIF
						
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(32.547848)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-8.712918)
							
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 0, CAM_SPLINE_SLOW_IN_SMOOTH)
							
							CLEAR_PED_TASKS(playerPedID)
						ENDIF
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						
						//TASK_GO_STRAIGHT_TO_COORD(playerPedID, GET_ENTITY_COORDS(playerPedID), PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 20.0871)
						
						ADVANCE_STAGE()
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
			IF DOES_CAM_EXIST(camAnim)
				IF IS_CAM_RENDERING(camAnim)
//					PRINTLN("GET_CAM_ANIM_CURRENT_PHASE(camAnim) = ", GET_CAM_ANIM_CURRENT_PHASE(camAnim))
//					PRINTLN("TIMERB() = ", TIMERB())
					IF NOT HAS_LABEL_BEEN_TRIGGERED(FIRST_PERSON_INTRO_CAM_FLASH)
						IF IS_CAM_PLAYING_ANIM(camAnim, "main_camera", sAnimDictPrologue1)
						AND GET_CAM_ANIM_CURRENT_PHASE(camAnim) >= 0.606985
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							
							SET_LABEL_AS_TRIGGERED(FIRST_PERSON_INTRO_CAM_FLASH, TRUE)
						ENDIF
					ENDIF
					
					IF IS_CAM_PLAYING_ANIM(camAnim, "main_camera", sAnimDictPrologue1)
					AND GET_CAM_ANIM_CURRENT_PHASE(camAnim) >= 0.625
						CLEAR_PED_TASKS(playerPedID)
						
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						
						SET_ENTITY_HEADING(playerPedID, 20.0)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-1.6)
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)	//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 0, CAM_SPLINE_SLOW_IN_SMOOTH)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneIntro)
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot1)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.0
				AND HAS_ANIMATED_CAMERA_CUT(camAnim)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camAnim, 1.2)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camAnim, 3.0)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 0.0)
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot1, TRUE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot2)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.072 - 0.01
				AND HAS_ANIMATED_CAMERA_CUT(camAnim)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camAnim, 2.45)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camAnim, 3.0)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 0.0)
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot2, TRUE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot3)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.113 - 0.01
				AND HAS_ANIMATED_CAMERA_CUT(camAnim)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camAnim, 2.0)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camAnim, 4.0)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 0.0)
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot3, TRUE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot4)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.199 - 0.01
				AND HAS_ANIMATED_CAMERA_CUT(camAnim)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camAnim, 6.0)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camAnim, 2.0)//0.75)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 0.0)
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot4, TRUE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot5)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.253 - 0.01
				AND HAS_ANIMATED_CAMERA_CUT(camAnim)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camAnim, 1.75)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camAnim, 2.5)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 0.0)
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot5, TRUE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot6)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.349 - 0.01
				AND HAS_ANIMATED_CAMERA_CUT(camAnim)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camAnim, 10.0)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 1.0)	
					SET_CAM_DOF_FNUMBER_OF_LENS(camAnim, 2.0)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camAnim, 0.0)
					
					SAFE_DELETE_OBJECT(objLightingRig[LIGHTING_RIG_OPENING])
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot6, TRUE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CamEffectIntroShot7)
				IF IS_CAM_PLAYING_ANIM(camAnim, "main_camera", sAnimDictPrologue1)
				AND GET_CAM_ANIM_CURRENT_PHASE(camAnim) >= 0.90
				OR NOT IS_CAM_PLAYING_ANIM(camAnim, "main_camera", sAnimDictPrologue1)
					SET_CAM_PARAMS(camMain,
									<<5312.071289, -5213.576660, 84.145706>>,
									<<-5.135734, 0.003036, 15.537188>>,
									50.0)
					
					SET_CAM_ACTIVE_WITH_INTERP(camMain, camAnim, 500, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL)
					
					SET_LABEL_AS_TRIGGERED(CamEffectIntroShot7, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(WoodSplinter)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneIntro)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.470
				IF HAS_PTFX_ASSET_LOADED()
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pro_door_splinters", <<5309.9, -5208.2, 83.7>>, <<0.0, 0.0, -90.0>>)
				ENDIF
				
				SET_LABEL_AS_TRIGGERED(WoodSplinter, TRUE)
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED(PROHLP_RADAR)
			IF TIMERB() < 5000
				#IF IS_DEBUG_BUILD	PRINTLN("(TIMERB() / 250) % 2 = ", (TIMERB() / 250) % 2)	#ENDIF
				IF (TIMERB() / 250) % 2 = 0
					DISPLAY_RADAR(TRUE)		//bRadar = TRUE
				ELSE
					DISPLAY_RADAR(FALSE)	//bRadar = FALSE
				ENDIF
				#IF IS_DEBUG_BUILD	PRINTLN("bRadar = ", bRadar)	#ENDIF
			ELIF TIMERB() > 5000
			AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_BLIPS1)
				bRadar = TRUE
				
				SET_LABEL_AS_TRIGGERED(PROHLP_BLIPS1, TRUE) 						// - remove me if undo
				
//				IF IS_RADAR_PREFERENCE_SWITCHED_ON()								// - and bring me back
//					PRINT_HELP_ADV(PROHLP_BLIPS1, "PROHLP_BLIPS1")					// |
//				ENDIF																// |
//																					// |
//				SET_BLIP_FLASH_DURATION(blipTrevor, 3000 + 3000)					// |
//				SET_BLIP_FLASH_DURATION(blipBuddy, 3000 + 3000)						// |
//				SET_BLIP_FLASH_DURATION(blipHostage[HostageGuard], 3000 + 3000)		// - up to here

//			ELIF TIMERB() > 10000
//			AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_BLIPS3)
//				IF IS_RADAR_PREFERENCE_SWITCHED_ON()
//					PRINT_HELP_ADV(PROHLP_BLIPS3, "PROHLP_BLIPS3")
//				ENDIF
//				
//				SET_BLIP_FLASH_DURATION(blipHostage[HostageGuard], 3000 + 3000)
			ENDIF
			
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) <> CAM_VIEW_MODE_FIRST_PERSON
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_ADV(CMN_FPSHELP, "CMN_FPSHELP")
				ENDIF
			ENDIF
		ENDIF
		
//		IF iCutsceneStage > 0
//			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
//				AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneIntro)
//				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.567
//					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Intro_7, "PRO_Intro", "PRO_Intro_7")
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF IS_SCREEN_FADED_IN()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE1)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.053
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)	//ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
							PRINT_ADV(PRO_SUBTITLE1, "PRO_SUBTITLE1", 3000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE1, TRUE)
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE2)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.12
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
							PRINT_ADV(PRO_SUBTITLE2, "PRO_SUBTITLE2", 4000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE2, TRUE)
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE3)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.23
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
							PRINT_ADV(PRO_SUBTITLE3, "PRO_SUBTITLE3", 4000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE3, TRUE)
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE4)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.36
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
							PRINT_ADV(PRO_SUBTITLE4, "PRO_SUBTITLE4", 3000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE4, TRUE)
						ENDIF
					ENDIF
//				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE5)
//					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.577
//						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
//							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
//							PRINT_ADV(PRO_SUBTITLE5, "PRO_SUBTITLE5", 7000)
//						ELSE
//							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE5, TRUE)
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 1.0)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroTrevor)
				sceneIntroTrevor = CREATE_SYNCHRONIZED_SCENE(sceneIntroPos, sceneIntroRot)
				
				TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneIntroTrevor, sAnimDictPrologue1, "idle_loop_player_two", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIntroTrevor, TRUE)
			ENDIF
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 1.0)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue1, "idle_loop_gaurd")
				//Guard
				TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageGuard], sAnimDictPrologue1, "idle_loop_gaurd", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageGuard])
			ENDIF
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 1.0)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBradHostage)
				sceneIntroBradHostage = CREATE_SYNCHRONIZED_SCENE(sceneIntroPos, sceneIntroRot)
				
				TASK_SYNCHRONIZED_SCENE(pedBrad, sceneIntroBradHostage, sAnimDictPrologue1, "idle_loop_brad", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				TASK_SYNCHRONIZED_SCENE(pedHostage[HostageFemale], sceneIntroBradHostage, sAnimDictPrologue1, "idle_loop_femalehostage", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageFemale])
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIntroBradHostage, TRUE)
			ENDIF
		ENDIF
		
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iCupboardDoor) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iCupboardDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iCupboardDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iCupboardDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iLeftReceptionDoor) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iLeftReceptionDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iLeftReceptionDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iLeftReceptionDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iRightReceptionDoor) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iRightReceptionDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iRightReceptionDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iRightReceptionDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		
		//Disable Player Movement
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		vDoorPosition.Y = -5208.0560
		vDoorRotate.Z = 160.0
		
		SET_ENTITY_COORDS_NO_OFFSET(objDoor, vDoorPosition)
		SET_ENTITY_ROTATION(objDoor, vDoorRotate)
		
		REPLAY_STOP_EVENT()
		
		IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		ENDIF
		
		STOP_CAM_SHAKING(camMain, TRUE)
		
		UNLOCK_MINIMAP_ANGLE()
		
		SET_ENTITY_INVINCIBLE(pedHostage[HostageFemale], FALSE)
		SET_ENTITY_INVINCIBLE(pedHostage[HostageGuard], FALSE)
		SET_ENTITY_INVINCIBLE(pedHostage[HostageMale1], FALSE)
		SET_ENTITY_INVINCIBLE(pedHostage[HostageMale2], FALSE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC LearnWalking()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Radar
		bRadar = TRUE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Idle Dialogue Timer
		iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7000)
		
		//Cutscene
		REQUEST_CUTSCENE("pro_mcs_1")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_INSIDE_OFFICE")
			START_AUDIO_SCENE("PROLOGUE_GET_INSIDE_OFFICE")
		ENDIF
		
		PRINT_ADV(PRO_WALK, "PRO_WALK")
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS(playerPedID)
			
			//Trevor
			TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue1, "idle_loop_player_two", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			//Brad
			TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue1, "idle_loop_brad", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			//Female Hostage
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageFemale], sAnimDictPrologue1, "idle_loop_femalehostage", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			//Male Hostage 01
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageMale1], sAnimDictPrologue1, "idle_loop_malehostage01", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			//Male Hostage 02
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageMale2], sAnimDictPrologue1, "idle_loop_malehostage02", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			//Guard
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageGuard], sAnimDictPrologue1, "idle_loop_gaurd", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			APPLY_PED_BLOOD_SPECIFIC(pedHostage[HostageGuard], ENUM_TO_INT(PDZ_HEAD), 0.354, 0.696, 0.0, 1.0, 0, 0.0, "BulletLarge")
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		SET_PED_CAPSULE(pedBrad, 0.75)
		SET_PED_CAPSULE(pedHostage[HostageFemale], 0.75)
		
//		IF HAS_LABEL_BEEN_TRIGGERED(PROHLP_RADAR)
//			IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_BLIPS3)
//				IF IS_RADAR_PREFERENCE_SWITCHED_ON()
//					PRINT_HELP_ADV(PROHLP_BLIPS3, "PROHLP_BLIPS3")
//				ENDIF
//				
//				SET_BLIP_FLASH_DURATION(blipHostage[HostageGuard], 3000 + 3000)
//			ENDIF
//		ENDIF
		
		//Request Cutscene Variations - pro_mcs_1
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_SecurityGuard_Tiedup", pedHostage[HostageGuard])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Male_Hostage_1", pedHostage[HostageMale1])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Male_Hostage_2", pedHostage[HostageMale2])
		ENDIF
		
		IF IS_SCREEN_FADED_IN()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE1)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.053
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)	//ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
							PRINT_ADV(PRO_SUBTITLE1, "PRO_SUBTITLE1", 3000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE1, TRUE)
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE2)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.12
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
							PRINT_ADV(PRO_SUBTITLE2, "PRO_SUBTITLE2", 4000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE2, TRUE)
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE3)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.23
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
							PRINT_ADV(PRO_SUBTITLE3, "PRO_SUBTITLE3", 4000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE3, TRUE)
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE4)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.36
						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
							PRINT_ADV(PRO_SUBTITLE4, "PRO_SUBTITLE4", 3000)
						ELSE
							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE4, TRUE)
						ENDIF
					ENDIF
//				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SUBTITLE5)
//					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.577
//						IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 1
//							FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)
//							PRINT_ADV(PRO_SUBTITLE5, "PRO_SUBTITLE5", 7000)
//						ELSE
//							SET_LABEL_AS_TRIGGERED(PRO_SUBTITLE5, TRUE)
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 1.0)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroTrevor)
				sceneIntroTrevor = CREATE_SYNCHRONIZED_SCENE(sceneIntroPos, sceneIntroRot)
				
				TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneIntroTrevor, sAnimDictPrologue1, "idle_loop_player_two", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIntroTrevor, TRUE)
			ENDIF
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 1.0)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue1, "idle_loop_gaurd")
				//Guard
				TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageGuard], sAnimDictPrologue1, "idle_loop_gaurd", sceneIntroPos, sceneIntroRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageGuard])
			ENDIF
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 1.0)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBradHostage)
				sceneIntroBradHostage = CREATE_SYNCHRONIZED_SCENE(sceneIntroPos, sceneIntroRot)
				
				TASK_SYNCHRONIZED_SCENE(pedBrad, sceneIntroBradHostage, sAnimDictPrologue1, "idle_loop_brad", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				TASK_SYNCHRONIZED_SCENE(pedHostage[HostageFemale], sceneIntroBradHostage, sAnimDictPrologue1, "idle_loop_femalehostage", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageFemale])
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIntroBradHostage, TRUE)
			ENDIF
		ENDIF
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) <> CAM_VIEW_MODE_FIRST_PERSON
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_ADV(CMN_FPSHELP, "CMN_FPSHELP")
			ENDIF
		ENDIF
		
		IF NOT IS_PED_STOPPED(playerPedID) 
		AND NOT IS_PED_STILL(playerPedID)
			SET_LABEL_AS_TRIGGERED(PROHLP_WALK, TRUE)
		ENDIF
		
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntro) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntro) >= 0.75)
		OR IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroTrevor)
//			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Intro_7)
//				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Intro_7, "PRO_Intro", "PRO_Intro_7")
//				ENDIF
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Intro_8)
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Intro_8, "PRO_Intro", "PRO_Intro_8")
				ENDIF
			ELSE
				IF TIMERA() > 500
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_WALK)
						PRINT_HELP_ADV(PROHLP_WALK, "PROHLP_WALK")	//PRINT_HELP_ADV_POS("PROHLP_WALK", VECTOR_ZERO, playerPedID)
						
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION_ADV(PRO_quiet, "PRO_quiet")
						ENDIF
					ENDIF
					
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						//Idle Dialogue
						SWITCH iDialogueStage
							CASE 0
								IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Intro_8")
									IF GET_GAME_TIMER() > iDialogueTimer
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											iDialogueTimer = GET_GAME_TIMER() + 2500
											
											iDialogueStage++
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 1
								IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Intro_8")
									IF GET_GAME_TIMER() > iDialogueTimer
										CREATE_CONVERSATION_ADV(PRO_Door, "PRO_Door")
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueStage++
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 2
								IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Door")
									IF GET_GAME_TIMER() > iDialogueTimer
										CREATE_CONVERSATION_ADV(PRO_Back, "PRO_Back")
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueStage++
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 3
								IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Back")
									IF GET_GAME_TIMER() > iDialogueTimer
										CREATE_CONVERSATION_ADV(PRO_Idle1T, "PRO_Idle1T")
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
											
											iDialogueStage++
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 4
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1B, "PRO_Idle1B")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
							CASE 5
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1T, "PRO_Idle1T")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
							CASE 6
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1B, "PRO_Idle1B")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
							CASE 7
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1T, "PRO_Idle1T")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1B, "PRO_Idle1B")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
							CASE 9
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1T, "PRO_Idle1T")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
							CASE 10
								IF GET_GAME_TIMER() > iDialogueTimer
									CREATE_CONVERSATION_ADV(PRO_Idle1B, "PRO_Idle1B")
									
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
										
										iDialogueStage++
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5316.579590, -5205.753906, 82.518654>>, <<5305.574707, -5205.736816, 86.518654>>, 5.0)
		OR IS_ENTITY_AT_COORD(playerPedID, <<5309.496094, -5207.671875, 84.018631>>, <<1.0, 1.5, 1.5>>)
			IF SAFE_START_CUTSCENE(0.0, FALSE, DEFAULT, TRUE)
				ADVANCE_STAGE()
			ENDIF
		ENDIF
		
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iCupboardDoor) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iCupboardDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iCupboardDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iCupboardDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iLeftReceptionDoor) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iLeftReceptionDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iLeftReceptionDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iLeftReceptionDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iRightReceptionDoor) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iRightReceptionDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iRightReceptionDoor, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iRightReceptionDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		
		//Disable Player Controls
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S1")
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_INSIDE_OFFICE")
			STOP_AUDIO_SCENE("PROLOGUE_GET_INSIDE_OFFICE")
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC cutsceneTieUp()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Text
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMN_FPSHELP")
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION()
		ELSE
			CLEAR_TEXT()
		ENDIF
		
		//Shadows
		SET_PED_AO_BLOB_RENDERING(pedHostage[HostageFemale], TRUE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		REQUEST_CUTSCENE("pro_mcs_1", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		
		IF NOT DOES_ENTITY_EXIST(objLightingRig[LIGHTING_RIG_HOSTAGE])
			objLightingRig[LIGHTING_RIG_HOSTAGE] = CREATE_OBJECT(PROP_1ST_HOSTAGE_SCENE, <<5310.661, -5206.56, 81.575>>)
			FORCE_ROOM_FOR_ENTITY(objLightingRig[LIGHTING_RIG_HOSTAGE], intDepot, HASH("V_CashD_reception"))
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_IN_COVER(playerPedMichael)
			SET_PED_CAPSULE(playerPedMichael, 0.5)
		ENDIF
		
		//Camera
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			sceneIntroCam = CREATE_SYNCHRONIZED_SCENE(<<5305.510, -5190.700, 82.520>>, VECTOR_ZERO)
			
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIntroCam, TRUE)
			
			IF NOT DOES_CAM_EXIST(camAnim)
				camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
			ENDIF
			
			SET_CAM_ACTIVE(camAnim, TRUE)
			
			PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneIntroCam, "blendout_pro_mcs_1_cam", sAnimDictPrologue1)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(-87.2 - GET_ENTITY_HEADING(playerPedID))
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-2.0)
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two")
			SET_ENTITY_ANIM_SPEED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two", GET_SYNCHRONIZED_SCENE_RATE(sceneIntroBrad))
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(BradAndHostage)
			//IF GET_CUTSCENE_TIME() > ROUND(8.000000 * 1000.0)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroCam)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntroCam) >= 0.3
				SET_SYNCHRONIZED_SCENE_RATE(sceneIntroBrad, 1.0)
				SET_SYNCHRONIZED_SCENE_RATE(sceneIntroHostage, 1.0)
				
				SET_FORCE_FOOTSTEP_UPDATE(pedBrad, TRUE)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageFemale], TRUE)
				
				SET_LABEL_AS_TRIGGERED(BradAndHostage, TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(TrevorSetCharge)
			//IF GET_CUTSCENE_TIME() > ROUND(8.500000 * 1000.0)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroCam)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntroCam) >= 0.4
				//Trevor
				IF WAS_CUTSCENE_SKIPPED()
					TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 1.0)
				ELSE
					TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				
				SET_LABEL_AS_TRIGGERED(TrevorSetCharge, TRUE)
			ENDIF
		ENDIF
		
		//Michael
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			SET_PED_POSITION(playerPedID, <<5308.9097, -5206.3105, 82.5186>>, 272.0266, FALSE)
			
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			
			OPEN_SEQUENCE_TASK(seqMain)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2_MCS1, "kick_down_player_zero", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.125)
				//TASK_PLAY_ANIM(NULL, sAnimDictPrologue2, "main_player_zero", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
			CLOSE_SEQUENCE_TASK(seqMain)
			TASK_PERFORM_SEQUENCE(playerPedID, seqMain)
			CLEAR_SEQUENCE_TASK(seqMain)
			
			//TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero", 1.0, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID)
			
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION_ADV(PRO_hostf, "PRO_hostf")
			ENDIF
		ENDIF
		
		//Trevor
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			TASK_AIM_GUN_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], pedHostage[HostageGuard], -1, TRUE)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
		
		//Guard
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_SecurityGuard_Tiedup")
			//TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			OPEN_SEQUENCE_TASK(seqMain)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2_MCS1, "kick_down_gaurd", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.1)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_on_floor_gaurd", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			CLOSE_SEQUENCE_TASK(seqMain)
			TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
			CLEAR_SEQUENCE_TASK(seqMain)
			
			PLAY_FACIAL_ANIM(pedHostage[HostageGuard], "002907_03_gc_pro_mcs_1", sAnimDictPrologue2_MCS1_GuardFacial)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageGuard])
		ENDIF
		
		//Male Hostage 01
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_Male_Hostage_1")
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageMale1])
		ENDIF
		
		//Male Hostage 02
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_Male_Hostage_2")
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageMale2])
		ENDIF
		
		//Weapon
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_weapon")
			GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_MICHAEL], playerPedMichael)
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
			GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_TREVOR], playerPedTrevor)
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				//Request Cutscene Variations - pro_mcs_1
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_SecurityGuard_Tiedup", pedHostage[HostageGuard])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Male_Hostage_1", pedHostage[HostageMale1])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Male_Hostage_2", pedHostage[HostageMale2])					
				ENDIF
				
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(playerPedID, FALSE)
					
					REGISTER_ENTITY_FOR_CUTSCENE(pedHostage[HostageGuard], "PRO_SecurityGuard_Tiedup", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedHostage[HostageMale1], "PRO_Male_Hostage_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedHostage[HostageMale2], "PRO_Male_Hostage_2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedMichael)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_MICHAEL], "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedTrevor)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					REGISTER_ENTITY_FOR_CUTSCENE(playerPedTrevor, "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)	#IF IS_DEBUG_BUILD	PRINTLN("START_CUTSCENE() - pro_mcs_1")	#ENDIF
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
					ENDIF
					
					//Radar
					bRadar = FALSE
					
					CLEAR_AREA(GET_ENTITY_COORDS(playerPedID), 15.0, TRUE)
					
					SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, TRUE)
					
					FLOAT fStartPhaseBrad
					fStartPhaseBrad = 0.106
					
					FLOAT fStartPhaseHostage
					fStartPhaseHostage = 0.211
					
					//Brad
					sceneIntroBrad = CREATE_SYNCHRONIZED_SCENE(sceneHostagesPos, sceneHostagesRot)
					
					TASK_SYNCHRONIZED_SCENE(pedBrad, sceneIntroBrad, sAnimDictPrologue2, "main_brad", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroBrad, fStartPhaseBrad)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
					
					SET_CURRENT_PED_WEAPON(pedBrad, wtShotgun, TRUE)
					
					//Female Hostage
					sceneIntroHostage = CREATE_SYNCHRONIZED_SCENE(sceneHostagesPos, sceneHostagesRot)
					
					TASK_SYNCHRONIZED_SCENE(pedHostage[HostageFemale], sceneIntroHostage, sAnimDictPrologue2, "main_femalehostage", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroHostage, fStartPhaseHostage)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageFemale])
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
				AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroHostage)
					SET_SYNCHRONIZED_SCENE_RATE(sceneIntroBrad, 0.0)
					SET_SYNCHRONIZED_SCENE_RATE(sceneIntroHostage, 0.0)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				IF NOT HAS_LABEL_BEEN_TRIGGERED(GuardHeadWound)
					IF GET_CUTSCENE_TIME() > ROUND(2.442666 * 1000.0)
						ENTITY_INDEX entityGuard
						entityGuard = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("PRO_SecurityGuard_Tiedup", CS_PROLSEC_02)
						
						IF NOT IS_ENTITY_DEAD(entityGuard)
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityGuard), ENUM_TO_INT(PDZ_HEAD), 0.354, 0.696, 0.0, 1.0, 0, 0.0, "BulletLarge")
							
							SET_LABEL_AS_TRIGGERED(GuardHeadWound, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
//					IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two")
//						SET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two", 1.0)
//					ENDIF
//					
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
//						SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroBrad, 0.442)
//					ENDIF
//					
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
//						SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroHostage, 0.883)
//					ENDIF
//					
//					SET_SYNCHRONIZED_SCENE_RATE(sceneIntroBrad, 1.0)
//					SET_SYNCHRONIZED_SCENE_RATE(sceneIntroHostage, 1.0)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-87.2 - GET_ENTITY_HEADING(playerPedID))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-2.0)
					
					SAFE_REMOVE_BLIP(blipTrevor)
					
					SET_LABEL_AS_TRIGGERED(TieUpCutsceneSkipped, TRUE)
				ENDIF
				#IF IS_DEBUG_BUILD	PRINTLN("HAS_CUTSCENE_FINISHED()")	#ENDIF
				IF HAS_CUTSCENE_FINISHED()
					IF WAS_CUTSCENE_SKIPPED()
						IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two")
							SET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two", 1.0)
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
							SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroBrad, 0.442)
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
							SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroHostage, 0.883)
						ENDIF
						
						SET_SYNCHRONIZED_SCENE_RATE(sceneIntroBrad, 1.0)
						SET_SYNCHRONIZED_SCENE_RATE(sceneIntroHostage, 1.0)
						
						SET_FORCE_FOOTSTEP_UPDATE(pedBrad, TRUE)
						SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageFemale], TRUE)
					ENDIF
					
					PLAY_AUDIO(PROLOGUE_TEST_HOSTAGES)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Charges_Set)
						CREATE_CONVERSATION_ADV(PRO_Charges, "PRO_Charges", CONV_PRIORITY_HIGH, FALSE) //Reused same label as print...
						
						SET_LABEL_AS_TRIGGERED(PRO_Charges_Set, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(KICK_DOWN_PLAYER_ZERO_CHECK)
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					AND (IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2_MCS1, "kick_down_player_zero") 
					AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedID, sAnimDictPrologue2_MCS1, "kick_down_player_zero") >= 0.806)
						RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME / 2)
						
						SET_LABEL_AS_TRIGGERED(KICK_DOWN_PLAYER_ZERO_CHECK, TRUE)
					ENDIF
				ENDIF
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroCam)	AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntroCam) >= 1.0)
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF DOOR_SYSTEM_GET_DOOR_STATE(iCupboardDoor) != DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_DOOR_STATE(iCupboardDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		IF DOOR_SYSTEM_GET_DOOR_STATE(iLeftReceptionDoor) != DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_DOOR_STATE(iLeftReceptionDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
		IF DOOR_SYSTEM_GET_DOOR_STATE(iRightReceptionDoor) != DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_DOOR_STATE(iRightReceptionDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF DOES_ENTITY_EXIST(objWeapon[WEAPON_MICHAEL])
			GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_MICHAEL], playerPedMichael)
		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(objWeapon) i
			SAFE_DELETE_OBJECT(objWeapon[i])
		ENDREPEAT
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		IF DOES_CAM_EXIST(camAnim)
			IF NOT IS_CAM_RENDERING(camAnim)
				DESTROY_CAM(camAnim)
			ENDIF
		ENDIF
		
//		SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
		SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, TRUE)
		SET_CURRENT_PED_WEAPON(pedBrad, wtShotgun, TRUE)
		
		SAFE_REMOVE_BLIP(blipHostage[HostageGuard])
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC LearnAiming()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Radar
		bRadar = TRUE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Print
		IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Charges")
			CLEAR_TEXT()
		ENDIF
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5310.6885, -5204.9897, 82.5199>>)
			covPoint[0] = ADD_COVER_POINT(<<5310.6885, -5204.9897, 82.5199>>, 0.0, COVUSE_WALLTOBOTH, COVHEIGHT_TOOHIGH, COVARC_180)
		ENDIF
		
		APPLY_PED_BLOOD_SPECIFIC(pedHostage[HostageGuard], ENUM_TO_INT(PDZ_HEAD), 0.354, 0.696, 0.0, 1.0, 0, 0.0, "BulletLarge")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_THREATEN_HOSTAGES")
			START_AUDIO_SCENE("PROLOGUE_THREATEN_HOSTAGES")
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objLightingRig[LIGHTING_RIG_HOSTAGE])
			objLightingRig[LIGHTING_RIG_HOSTAGE] = CREATE_OBJECT(PROP_1ST_HOSTAGE_SCENE, <<5310.661, -5206.56, 81.575>>)
			FORCE_ROOM_FOR_ENTITY(objLightingRig[LIGHTING_RIG_HOSTAGE], intDepot, HASH("V_CashD_reception"))
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Rayfire
		rfVaultExplosion = GET_RAYFIRE_MAP_OBJECT(<<5298.8892, -5189.0869, 82.5182>>, 10.0, "DES_VaultDoor001")
		
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfVaultExplosion)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion) != RFMO_STATE_START
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion, RFMO_STATE_STARTING)
			ENDIF
		ENDIF
		
		//Doors
		IF NOT HAS_LABEL_BEEN_TRIGGERED(TieUpCutsceneSkipped)
			UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
			UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
		ENDIF
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			SET_PED_POSITION(playerPedID, <<5308.8320, -5206.5249, 82.5187>>, 284.3977)
			
			STOP_CAM_SHAKING(camMain, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2155703
		
//		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
//		OR IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue2, "idle_outside_cuboard_brad")
//			SET_PED_CAPSULE(pedBrad, 0.25)
//		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(V_CORP_CD_CHAIR_FREEZE_1)
			OBJECT_INDEX objChair = GET_CLOSEST_OBJECT_OF_TYPE(<<5313.3271, -5208.2446, 82.5107>>, 0.01, V_CORP_CD_CHAIR, FALSE)
			
			IF DOES_ENTITY_EXIST(objChair)
				FREEZE_ENTITY_POSITION(objChair, TRUE)
				
				SET_LABEL_AS_TRIGGERED(V_CORP_CD_CHAIR_FREEZE_1, TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(V_CORP_CD_CHAIR_FREEZE_2)
			OBJECT_INDEX objChair = GET_CLOSEST_OBJECT_OF_TYPE(<<5314.8643, -5208.2363, 82.5107>>, 0.01, V_CORP_CD_CHAIR, FALSE)
			
			IF DOES_ENTITY_EXIST(objChair)
				FREEZE_ENTITY_POSITION(objChair, TRUE)
				
				SET_LABEL_AS_TRIGGERED(V_CORP_CD_CHAIR_FREEZE_2, TRUE)
			ENDIF
		ENDIF
		
		INT i
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two")	//IF NOT IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero")
					//Michael
					//TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 1.0)
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2_MCS1, "kick_down_player_zero", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.125)
						//TASK_PLAY_ANIM(NULL, sAnimDictPrologue2, "main_player_zero", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(playerPedID, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(playerPedID, FALSE)
					
					//Trevor
					TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 1.0)
					
					FLOAT fStartPhaseBrad
					fStartPhaseBrad = 0.442	//0.106
					
					FLOAT fStartPhaseHostage
					fStartPhaseHostage = 0.883	//0.211
					
					//Brad
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
						sceneIntroBrad = CREATE_SYNCHRONIZED_SCENE(sceneHostagesPos, sceneHostagesRot)
						
						TASK_SYNCHRONIZED_SCENE(pedBrad, sceneIntroBrad, sAnimDictPrologue2, "main_brad", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroBrad, fStartPhaseBrad)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
					ENDIF
					
					SET_CURRENT_PED_WEAPON(pedBrad, wtShotgun, TRUE)
					
					//Female Hostage
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroHostage)
						sceneIntroHostage = CREATE_SYNCHRONIZED_SCENE(sceneHostagesPos, sceneHostagesRot)
						
						TASK_SYNCHRONIZED_SCENE(pedHostage[HostageFemale], sceneIntroHostage, sAnimDictPrologue2, "main_femalehostage", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneIntroHostage, fStartPhaseHostage)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageFemale])
					ENDIF
					
					//Male Hostage 01
					TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					
					//Male Hostage 02
					TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					
					//Guard
					//TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2_MCS1, "kick_down_gaurd", sceneHostagesPos, sceneHostagesRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.1)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_on_floor_gaurd", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					PLAY_FACIAL_ANIM(pedHostage[HostageGuard], "002907_03_gc_pro_mcs_1", sAnimDictPrologue2_MCS1_GuardFacial)
				ENDIF
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 1
				IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two")
					IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two") > 0.750 //0.600
						SAFE_REMOVE_BLIP(blipTrevor)
					ENDIF
				ENDIF
				
				//Score
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_PRE_SAFE_EXPLOSION)
					LOAD_AUDIO(PROLOGUE_TEST_PRE_SAFE_EXPLOSION)
					
					SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_PRE_SAFE_EXPLOSION, TRUE)
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(blipTrevor)
					IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two")
						IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue2, "main_player_two") >= 1.0
							SMOOTH_CLOSE_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>, TRUE, 1.0, 1.0)
							SMOOTH_CLOSE_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>, TRUE, 1.0, 1.0)
						ENDIF
					ENDIF
					
					SMOOTH_CLOSE_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>, TRUE)
					SMOOTH_CLOSE_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>, TRUE)
					
					IF SMOOTH_CLOSE_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>, TRUE)
					AND SMOOTH_CLOSE_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>, TRUE)
						IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
							IF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 0
								PRINT_HELP_ADV(PROHLP_AIM1b, "PROHLP_AIM1b")
							ELIF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 1
								PRINT_HELP_ADV(PROHLP_AIM1c, "PROHLP_AIM1c")
							ELIF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 2
								PRINT_HELP_ADV(PROHLP_AIM1c, "PROHLP_AIM1c")
							ENDIF
							
							ADVANCE_CUTSCENE()
						ENDIF
					ENDIF
				ELSE
					UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
					UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					// Don't want to display this on PC if using mouse to aim.
					IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP_ADV(PROHLP_TRADAIM, "PROHLP_TRADAIM")
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_1)
				OR HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_2)
				OR HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_3)
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
						AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale1])
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageMale1])))
						OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
						AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale2])
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageMale2])))
						OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
						AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageGuard])
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageGuard])))
							IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_4)
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_4, "PRO_bradhost", "PRO_bradhost_4", CONV_PRIORITY_LOW)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_5)
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_5, "PRO_bradhost", "PRO_bradhost_5", CONV_PRIORITY_LOW)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_6)
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_6, "PRO_bradhost", "PRO_bradhost_6", CONV_PRIORITY_LOW)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_7)
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_7, "PRO_bradhost", "PRO_bradhost_7", CONV_PRIORITY_LOW)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_9)
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_9, "PRO_bradhost", "PRO_bradhost_9", CONV_PRIORITY_LOW)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_AIM)
						//Blips
						REPEAT COUNT_OF(pedHostage) i
							SAFE_ADD_BLIP_PED(blipHostage[i], pedHostage[i], TRUE)
						ENDREPEAT
						
						IF NOT DOES_BLIP_EXIST(blipDestination)
							PRINT_ADV(PRO_AIM, "PRO_AIM", DEFAULT_GOD_TEXT_TIME - 4000)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION_ADV(PRO_listen, "PRO_listen", CONV_PRIORITY_LOW, TRUE, DO_NOT_DISPLAY_SUBTITLES)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
						OR NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
						OR NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
							IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_AIM")
								CLEAR_PRINTS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM1a)
				OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM1b)
				OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM1c)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM2a)
					AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM2b)
						IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
						OR (NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_AIM1a")
						AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_AIM1b")
						AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_AIM1c"))
							SAFE_CLEAR_HELP(TRUE)
							
//							IF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 0
//								PRINT_HELP_ADV(PROHLP_AIM2a, "PROHLP_AIM2a")
//							ELIF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 1
//							OR GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 2
//								PRINT_HELP_ADV(PROHLP_AIM2b, "PROHLP_AIM2b")
								SET_LABEL_AS_TRIGGERED(PROHLP_AIM2b, TRUE)
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 0
					IF HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM2a)
					OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM2b)
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM3)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_AIM2a")
							OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_AIM2b")
								IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
								AND (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LR)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UD))
									SET_LABEL_AS_TRIGGERED(PLAYER_MOVED_AIM, TRUE)
								ENDIF
								
								IF GET_GAME_TIMER() > iHelpTimer
									IF HAS_LABEL_BEEN_TRIGGERED(PLAYER_MOVED_AIM)
										SAFE_CLEAR_HELP()
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP_ADV(PROHLP_AIM3, "PROHLP_AIM3")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Male Hostage 01
				IF NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale1])
					iHostageAimTimer[HostageMale1] = GET_GAME_TIMER() + 250
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(HostageMale1Scream)
					IF GET_GAME_TIMER() > iAmbientTimer
						IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedHostage[HostageMale1]), 2.5)
							PLAY_PAIN(pedHostage[HostageMale1], AUD_DAMAGE_REASON_SCREAM_SHOCKED)
							
							iAmbientTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(750, 1000)
							
							SET_LABEL_AS_TRIGGERED(HostageMale1Scream, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF ((IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale1])
				AND GET_GAME_TIMER() > iHostageAimTimer[HostageMale1])
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageMale1]))
				AND IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
//					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_8, "PRO_bradhost", "PRO_bradhost_8", CONV_PRIORITY_LOW)
//					ENDIF
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage01_standup_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedHostage[HostageMale1], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_standup_malehostage01")
				AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_standup_malehostage01") > 0.4
				OR GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_standup_malehostage01") < 0.5)
					TASK_LOOK_AT_ENTITY(pedHostage[HostageMale1], playerPedID, 1000)
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY1")
						AND NOT DOES_BLIP_EXIST(blipDestination)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_1, "PRO_bradhost", "PRO_bradhost_1", CONV_PRIORITY_LOW)
						ENDIF
					ENDIF
				ENDIF
				
				//Middle Hostage
//				IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
//				OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
//				AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02") < 0.209
//				OR GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02") > 0.349))
					IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
					OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
					AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd") < 0.050	//0.252
					OR GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd") > 0.267))
						IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_outside_cuboard_malehostage01")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_outside_cuboard_malehostage01") > 0.1
						OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage01_standup_malehostage01")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage01_standup_malehostage01") > 0.9)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PLAY_ANIM(NULL, sAnimDictPrologue2, "idle_inside_cuboard_malehostage01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedHostage[HostageMale1], seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							WAIT_WITH_DEATH_CHECKS(10)
						ENDIF
					ENDIF
//				ENDIF
				
				//Male Hostage 02
				IF NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale2])
					iHostageAimTimer[HostageMale2] = GET_GAME_TIMER() + 250
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(HostageMale2Scream)
					IF GET_GAME_TIMER() > iAmbientTimer
						IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedHostage[HostageMale2]), 2.5)
							PLAY_PAIN(pedHostage[HostageMale2], AUD_DAMAGE_REASON_SCREAM_TERROR)
							
							iAmbientTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(750, 1000)
							
							SET_LABEL_AS_TRIGGERED(HostageMale2Scream, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF ((IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale2])
				AND GET_GAME_TIMER() > iHostageAimTimer[HostageMale2])
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageMale2]))
				AND IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
//					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_8, "PRO_bradhost", "PRO_bradhost_8", CONV_PRIORITY_LOW)
//					ENDIF
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage_02_standup_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedHostage[HostageMale2], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage_02_standup_malehostage02")
				AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage_02_standup_malehostage02") > 0.4
				OR GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage_02_standup_malehostage02") < 0.5)
					TASK_LOOK_AT_ENTITY(pedHostage[HostageMale2], playerPedID, 1000)
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
					AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageMale2])
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageMale2]))
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY1")
						AND NOT DOES_BLIP_EXIST(blipDestination)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_2, "PRO_bradhost", "PRO_bradhost_2", CONV_PRIORITY_LOW)
						ENDIF
					ENDIF
				ENDIF
				
				//Closest to Cupboard Hostage
				IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
				OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
				AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01") > 0.349))
					IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
					OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
					AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd") < 0.1
					OR GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd") > 0.371))
						IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_outside_cuboard_malehostage02")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_outside_cuboard_malehostage02") > 0.1
						OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage_02_standup_malehostage02")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage_02_standup_malehostage02") > 0.9)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PLAY_ANIM(NULL, sAnimDictPrologue2, "idle_inside_cuboard_malehostage02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedHostage[HostageMale2], seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							WAIT_WITH_DEATH_CHECKS(10)
						ENDIF
					ENDIF
				ENDIF
				
				//Guard
				IF NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageGuard])
					iHostageAimTimer[HostageGuard] = GET_GAME_TIMER() + 250
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(HostageGuardScream)
					IF GET_GAME_TIMER() > iAmbientTimer
						IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedHostage[HostageGuard]), 2.5)
							PLAY_PAIN(pedHostage[HostageGuard], AUD_DAMAGE_REASON_SCREAM_SHOCKED)
							
							iAmbientTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(750, 1000)
							
							SET_LABEL_AS_TRIGGERED(HostageGuardScream, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF ((IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedHostage[HostageGuard])
				AND GET_GAME_TIMER() > iHostageAimTimer[HostageGuard])
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedHostage[HostageGuard]))
				AND IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
//					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_8, "PRO_bradhost", "PRO_bradhost_8", CONV_PRIORITY_LOW)
//					ENDIF
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "guard_standup_gaurd", sceneHostagesPos, sceneHostagesRot, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_gaurd", sceneHostagesPos, sceneHostagesRot, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "guard_standup_gaurd")
				AND (GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "guard_standup_gaurd") > 0.4
				OR GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "guard_standup_gaurd") < 0.5)
					TASK_LOOK_AT_ENTITY(pedHostage[HostageGuard], playerPedID, 1000)
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY1")
						AND NOT DOES_BLIP_EXIST(blipDestination)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_bradhost_3, "PRO_bradhost", "PRO_bradhost_3", CONV_PRIORITY_LOW)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_1)
				OR NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_2)
				OR NOT HAS_LABEL_BEEN_TRIGGERED(PRO_bradhost_3)
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
						OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
						OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
							IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle_1)
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle_1, "PRO_Idle", "PRO_Idle_1", CONV_PRIORITY_LOW)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle_2)
							AND TIMERA() > 15000
								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle_2, "PRO_Idle", "PRO_Idle_2", CONV_PRIORITY_LOW)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Closest to Player Hostage - There is no way the guard can intersect if hostages are playing their cupboard entry anims
//				IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
//				OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
//				AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01") > 0.448)
//					IF NOT IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
//					OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
//					AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02") > 0.214)
						IF (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_outside_cuboard_gaurd")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_outside_cuboard_gaurd") > 0.1)
						OR (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "guard_standup_gaurd")
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "guard_standup_gaurd") > 0.9)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd", sceneHostagesPos, sceneHostagesRot, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PLAY_ANIM(NULL, sAnimDictPrologue2, "idle_inside_cuboard_gaurd", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)	//TASK_PLAY_ANIM_ADVANCED
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							WAIT_WITH_DEATH_CHECKS(10)
						ENDIF
//					ENDIF
//				ENDIF
				
				//Debug Hostage Entry
//				#IF IS_DEBUG_BUILD
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//				
//				DRAW_RECT(0.1 + 0.4, 0.1, 0.8, 0.025, 255, 255, 255, 155)
//				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
//					DRAW_RECT(0.1 + (0.8 * GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")), 0.1, 0.001, 0.025, 255, 255, 255, 255)
//					DRAW_DEBUG_TEXT_2D(GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01"), <<0.1 + (0.8 * GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")), 0.11, 0.0>>, 255, 255, 255)
//				ENDIF
//				
//				DRAW_RECT(0.1 + 0.4, 0.15, 0.8, 0.025, 255, 255, 255, 155)
//				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
//					DRAW_RECT(0.1 + (0.8 * GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage02")), 0.15, 0.001, 0.025, 255, 255, 255, 255)
//					DRAW_DEBUG_TEXT_2D(GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage02"), <<0.1 + (0.8 * GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage02")), 0.16, 0.0>>, 255, 255, 255)
//				ENDIF
//				
//				DRAW_RECT(0.1 + 0.4, 0.2, 0.8, 0.025, 255, 255, 255, 155)
//				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
//					DRAW_RECT(0.1 + (0.8 * GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")), 0.2, 0.001, 0.025, 255, 255, 255, 255)
//					DRAW_DEBUG_TEXT_2D(GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd"), <<0.1 + (0.8 * GET_ENTITY_ANIM_CURRENT_TIME(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")), 0.21, 0.0>>, 255, 255, 255)
//				ENDIF
//				#ENDIF
				
				//Hostage Dialogue
				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
				OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
				OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostageF)
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) <> PERFORMING_TASK
						AND IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
							IF TIMERA() > 18000
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY1")
								AND NOT DOES_BLIP_EXIST(blipDestination)
									IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
									AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION_ADV(PRO_HostageF, "PRO_HostageF")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
//				ELIF IS_ENTITY_AT_COORD(pedHostage[HostageMale1], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//				AND IS_ENTITY_AT_COORD(pedHostage[HostageGuard], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//				AND IS_ENTITY_AT_COORD(pedHostage[HostageFemale], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//				AND IS_ENTITY_AT_COORD(pedHostage[HostageMale2], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostageF)
//						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) <> PERFORMING_TASK
//						AND IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
//							IF TIMERA() > 18000
//								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY1")
//								AND NOT DOES_BLIP_EXIST(blipDestination)
//									IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//									AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										CREATE_CONVERSATION_ADV(PRO_HostageP, "PRO_HostageP")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
				ENDIF
				
//				//IF NOT HAS_LABEL_BEEN_TRIGGERED(HURRY_HOSTAGES)
//					IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_HURRY)
//						IF HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM2a)
//						OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM2b)
//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()	//IS_ANY_FLOATING_HELP_BEING_DISPLAYED()	//AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//								//IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
//								//OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
//								//OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
//								IF NOT IS_ENTITY_AT_COORD(pedHostage[HostageMale1], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//								OR NOT IS_ENTITY_AT_COORD(pedHostage[HostageGuard], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//								OR NOT IS_ENTITY_AT_COORD(pedHostage[HostageFemale], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//								OR NOT IS_ENTITY_AT_COORD(pedHostage[HostageMale2], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
//									PRINT_HELP_ADV(PROHLP_HURRY, "PROHLP_HURRY")
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
//							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_HURRY")
//								SAFE_CLEAR_HELP()
//							ENDIF
//							
//							IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
//								OPEN_SEQUENCE_TASK(seqMain)
//									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage01_standup_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//								CLOSE_SEQUENCE_TASK(seqMain)
//								TASK_PERFORM_SEQUENCE(pedHostage[HostageMale1], seqMain)
//								CLEAR_SEQUENCE_TASK(seqMain)
//							ENDIF
//							
//							IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
//								OPEN_SEQUENCE_TASK(seqMain)
//									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage_02_standup_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//								CLOSE_SEQUENCE_TASK(seqMain)
//								TASK_PERFORM_SEQUENCE(pedHostage[HostageMale2], seqMain)
//								CLEAR_SEQUENCE_TASK(seqMain)
//							ENDIF
//							
//							IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
//								OPEN_SEQUENCE_TASK(seqMain)
//									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "guard_standup_gaurd", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_gaurd", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//								CLOSE_SEQUENCE_TASK(seqMain)
//								TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
//								CLEAR_SEQUENCE_TASK(seqMain)
//							ENDIF
//							
//							IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostGap_1)
//								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_HostGap_1, "PRO_HostGap", "PRO_HostGap_1", CONV_PRIORITY_LOW)
//							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostGap_2)
//								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_HostGap_2, "PRO_HostGap", "PRO_HostGap_2", CONV_PRIORITY_LOW)
//							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostGap_3)
//								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_HostGap_3, "PRO_HostGap", "PRO_HostGap_3", CONV_PRIORITY_LOW)
//							ENDIF
//							
//							//SET_LABEL_AS_TRIGGERED(HURRY_HOSTAGES, TRUE)
//						ENDIF
//					ENDIF
//				//ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_HURRY")
					IF IS_ENTITY_AT_COORD(pedHostage[HostageMale1], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
					AND IS_ENTITY_AT_COORD(pedHostage[HostageGuard], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
					AND IS_ENTITY_AT_COORD(pedHostage[HostageFemale], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
					AND IS_ENTITY_AT_COORD(pedHostage[HostageMale2], <<5318.457520, -5206.782227, 84.268654>>, <<2.0, 2.5, 2.0>>)
						SAFE_CLEAR_HELP()
					ENDIF
				ENDIF
				
				//Hostage Aim Priority
				IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageGuard], TRUE)
				ELIF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageMale1], TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageGuard], FALSE)
				ELIF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageMale2], TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageMale1], FALSE)
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageGuard], FALSE)
				ELSE
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageMale2], FALSE)
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageMale1], FALSE)
					SET_ENTITY_IS_TARGET_PRIORITY(pedHostage[HostageGuard], FALSE)
				ENDIF
				
				//Trevor
				IF (IS_ENTITY_AT_COORD(pedHostage[HostageMale1], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
				AND IS_ENTITY_AT_COORD(pedHostage[HostageGuard], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
				AND IS_ENTITY_AT_COORD(pedHostage[HostageFemale], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
				AND IS_ENTITY_AT_COORD(pedHostage[HostageMale2], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>))
				OR TIMERA() > 20000
					IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
					AND ((IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
					AND NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5308.802246, -5204.578613, 84.018631>>, <<1.0, 1.0, 1.5>>)))
						SET_LABEL_AS_TRIGGERED(DoorIsClosing, TRUE)
					ELSE
						UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
						UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
					ENDIF
					
					IF NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.607422, -5204.709961, 83.518654>>, <<0.5, 0.75, 1.0>>)
						IF NOT HAS_LABEL_BEEN_TRIGGERED(get_into_cover_player_two)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2_TrevorReturn, "get_into_cover_player_two", <<5310.146, -5208.317, 82.519>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, <<5310.6885, -5204.9897, 82.5199>>, -1, FALSE, 1.0, TRUE, TRUE, covPoint[0])
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							
							SET_LABEL_AS_TRIGGERED(get_into_cover_player_two, TRUE)
						ELSE
							SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(BradHostages)
					IF HAS_LABEL_BEEN_TRIGGERED(PRO_HostageF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue2, "shut_cuboard_door_brad")
								CLEAR_PED_TASKS(pedBrad)
								
								OPEN_SEQUENCE_TASK(seqMain)
									IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
										TASK_AIM_GUN_AT_ENTITY(NULL, pedHostage[HostageMale1], 1500)
									ENDIF
									IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
										TASK_AIM_GUN_AT_ENTITY(NULL, pedHostage[HostageMale2], 1500)
									ENDIF
									IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
										TASK_AIM_GUN_AT_ENTITY(NULL, pedHostage[HostageGuard], 1500)
									ENDIF
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_brad", sceneHostagesPos, sceneHostagesRot, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedBrad, seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
							ENDIF
							
							IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_on_floor_malehostage01")
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage01_standup_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_malehostage01", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedHostage[HostageMale1], seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
								
								WAIT_WITH_DEATH_CHECKS(500)
							ENDIF
							
							IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_on_floor_malehostage02")
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "hostage_02_standup_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_malehostage02", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedHostage[HostageMale2], seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
								
								WAIT_WITH_DEATH_CHECKS(500)
							ENDIF
							
							IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_on_floor_gaurd")
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "guard_standup_gaurd", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue2, "idle_outside_cuboard_gaurd", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedHostage[HostageGuard], seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
								
								WAIT_WITH_DEATH_CHECKS(500)
							ENDIF
							
							SET_LABEL_AS_TRIGGERED(BradHostages, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_V_ILEV_CD_Door2)
//					#IF IS_DEBUG_BUILD	PRINTLN("DOOR_SYSTEM_GET_OPEN_RATIO(iCupboardDoor) = ", DOOR_SYSTEM_GET_OPEN_RATIO(iCupboardDoor))	#ENDIF
//					IF DOOR_SYSTEM_GET_OPEN_RATIO(iCupboardDoor) > 0.9	//1.010
////					AND (IS_ENTITY_AT_COORD(pedHostage[HostageMale1], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
////					AND IS_ENTITY_AT_COORD(pedHostage[HostageGuard], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
////					AND IS_ENTITY_AT_COORD(pedHostage[HostageFemale], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
////					AND IS_ENTITY_AT_COORD(pedHostage[HostageMale2], <<5318.457520, -5206.782227, 84.268654>>, <<2.75, 2.25, 1.75>>))
//						DOOR_SYSTEM_SET_OPEN_RATIO(iCupboardDoor, DOOR_SYSTEM_GET_OPEN_RATIO(iCupboardDoor), FALSE)
//						DOOR_SYSTEM_SET_DOOR_STATE(iCupboardDoor, DOORSTATE_LOCKED, FALSE, TRUE)
//						SET_LABEL_AS_TRIGGERED(TRIGGER_V_ILEV_CD_Door2, TRUE)
//					ENDIF
//				ENDIF
				
				IF (IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "idle_inside_cuboard_malehostage01")
				AND IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "idle_inside_cuboard_gaurd")
				AND IS_ENTITY_PLAYING_ANIM(pedHostage[HostageFemale], sAnimDictPrologue2, "idle_inside_cuboard_femalehostage")
				AND IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "idle_inside_cuboard_malehostage02"))
				OR (IS_ENTITY_AT_COORD(pedHostage[HostageMale1], <<5318.615234, -5206.959473, 84.018631>>, <<2.0, 2.5, 2.0>>)
				AND IS_ENTITY_AT_COORD(pedHostage[HostageGuard], <<5318.615234, -5206.959473, 84.018631>>, <<2.0, 2.5, 2.0>>)
				AND IS_ENTITY_AT_COORD(pedHostage[HostageFemale], <<5318.615234, -5206.959473, 84.018631>>, <<2.0, 2.5, 2.0>>)
				AND IS_ENTITY_AT_COORD(pedHostage[HostageMale2], <<5318.615234, -5206.959473, 84.018631>>, <<2.0, 2.5, 2.0>>))
				AND NOT IS_ENTITY_AT_COORD(playerPedID, <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
				AND NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
				AND NOT IS_ENTITY_AT_COORD(pedBrad, <<5318.457520, -5206.782227, 84.268654>>, <<1.75, 2.25, 1.75>>)
//					IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_V_ILEV_CD_Door2)
//						UNLOCK_DOOR(CUPBOARD_DOOR, V_ILev_CD_Door2, TRUE)
//						
//						SET_LABEL_AS_TRIGGERED(TRIGGER_V_ILEV_CD_Door2, TRUE)
//					ENDIF
					
					//IF SMOOTH_CLOSE_DOOR(CUPBOARD_DOOR, V_ILev_CD_Door2, <<5316.64, -5205.74, 83.67>>, TRUE, 0.2, 0.01, 1.0)
//					IF HAS_LABEL_BEEN_TRIGGERED(TRIGGER_V_ILEV_CD_Door2)
						IF DOES_ENTITY_EXIST(objCupboardDoor)
							//Brad closes cupboard door
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
							AND GET_SYNCHRONIZED_SCENE_PHASE(sceneHostage) > 0.287	//0.635
							OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
								IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
								AND IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
								//AND IS_ENTITY_IN_ANGLED_AREA(pedBrad, <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
									ADVANCE_STAGE()
								ENDIF
							ENDIF
						ELIF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<5316.64, -5205.74, 83.67>>, 1.0, V_ILev_CD_Door2) //HAS_MODEL_LOADED_CHECK(MODEL_V_ILev_CD_Door2, V_ILev_CD_Door2)
//							CREATE_MODEL_HIDE(<<5316.64, -5205.74, 83.67>>, 1.0, V_ILev_CD_Door2, FALSE)
//							
//							objCupboardDoor = CREATE_OBJECT_NO_OFFSET(V_ILev_CD_Door2, <<5316.64, -5205.74, 83.67>>)	//<<5316.6475, -5205.7368, 83.6686>>)
//							
//							SET_ENTITY_ROTATION(objCupboardDoor, <<-0.005, 0.002, -10.658>>)	//<<0.0, 0.0, -8.19>>)
//							
//							SET_MODEL_AS_NO_LONGER_NEEDED(V_ILev_CD_Door2)
							
							objCupboardDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<5316.64, -5205.74, 83.67>>, 1.0, V_ILev_CD_Door2)
							
							sceneHostage = CREATE_SYNCHRONIZED_SCENE(sceneHostagesPos, sceneHostagesRot)
							
							TASK_SYNCHRONIZED_SCENE(pedBrad, sceneHostage, sAnimDictPrologue2, "shut_cuboard_door_brad", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)	//TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue2, "shut_cuboard_door_brad", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(objCupboardDoor, sceneHostage, "shut_cuboard_door_door", sAnimDictPrologue2, WALK_BLEND_IN, NORMAL_BLEND_OUT, 0, WALK_BLEND_IN)	//PLAY_ENTITY_ANIM(objCupboardDoor, "shut_cuboard_door_door", sAnimDictPrologue2, 0.0, FALSE, TRUE)
						ENDIF
//					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
//		IF GET_GAME_TIMER() < fAnimProgressTime
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero")
//				OR GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
//					CLEAR_PED_TASKS(playerPedID)
//				ENDIF
//			ELIF NOT IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero")
//				IF GET_ENTITY_HEADING(playerPedID) < 284.3977 - 7.5 OR GET_ENTITY_HEADING(playerPedID) > 284.3977 + 7.5
//					IF GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
//						TASK_GO_STRAIGHT_TO_COORD(playerPedID, GET_ENTITY_COORDS(playerPedID), PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 284.3977)
//					ENDIF
//				ELSE
//					TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS, fAnimTotalTime - (fAnimProgressTime - GET_GAME_TIMER()))
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(CatchUpCam)
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroCam)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntroCam) >= 1.0)
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroCam)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroCam)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 0, CAM_SPLINE_SLOW_IN_SMOOTH)
					
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneIntroCam, FALSE)
				ELSE
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SET_LABEL_AS_TRIGGERED(CatchUpCam, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2_MCS1, "kick_down_player_zero")	//sAnimDictPrologue2, "main_player_zero")
			SET_LABEL_AS_TRIGGERED(AnimStarted, TRUE)
			
			SET_PED_RESET_FLAG(playerPedID, PRF_InstantBlendToAim, TRUE)
		ELIF NOT IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2_MCS1, "kick_down_player_zero")	//sAnimDictPrologue2, "main_player_zero")
			IF HAS_LABEL_BEEN_TRIGGERED(AnimStarted)
				IF NOT HAS_LABEL_BEEN_TRIGGERED(ForceIdlePlayer)
					FORCE_PED_MOTION_STATE(playerPedID, MS_AIMING)
					
					SETTIMERB(0)
					
					SET_LABEL_AS_TRIGGERED(ForceIdlePlayer, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED(ForceIdlePlayer)
			IF NOT HAS_LABEL_BEEN_TRIGGERED(AmbientAnims)
				SET_PED_RESET_FLAG(playerPedID, PRF_InstantBlendToAim, TRUE)
				
				IF TIMERB() > 2000
					SET_PED_CAN_PLAY_AMBIENT_ANIMS(playerPedID, TRUE)
					
					SET_LABEL_AS_TRIGGERED(AmbientAnims, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5305.638184, -5209.354004, 82.518677>>, <<5317.589844 - 1.13, -5209.528809, 85.518677>>, 10.0)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5317.081055, -5209.484375, 82.518654>>, <<5317.070313, -5214.340332, 86.018654>>, 1.0)
			PRINT_ADV(PRO_STAY1, "PRO_STAY1")
			
			//Blips
			REPEAT COUNT_OF(pedHostage) i
				SAFE_REMOVE_BLIP(blipHostage[i])
			ENDREPEAT
			SAFE_REMOVE_BLIP(blipTrevor)
			SAFE_REMOVE_BLIP(blipBuddy)
			SAFE_ADD_BLIP_LOCATION(blipDestination, <<5309.4556, -5207.3184, 85.7187 - 3.2>>)
		ELSE
			IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY1")
				CLEAR_PRINTS()
			ENDIF			
			
			//Blips
			SAFE_REMOVE_BLIP(blipDestination)
			REPEAT COUNT_OF(pedHostage) i
				SAFE_ADD_BLIP_PED(blipHostage[i], pedHostage[i], TRUE)
			ENDREPEAT
			IF HAS_LABEL_BEEN_TRIGGERED(get_into_cover_player_two)
				SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
			ENDIF
			SAFE_ADD_BLIP_PED(blipBuddy, pedBrad, FALSE)
		ENDIF
		
//		IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2_MCS1, "kick_down_player_zero")	//sAnimDictPrologue2, "main_player_zero")
//		AND (HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM1a)
//		OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM1b)
//		OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_AIM1c))
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				CLEAR_PED_TASKS(playerPedID)				
//			ENDIF
//		ENDIF
		
		//Radar
//		IF NOT bRadar
//			IF IS_GAMEPLAY_CAM_RENDERING()
//			AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//			AND NOT IS_INTERPOLATING_TO_SCRIPT_CAMS()
//				bRadar = TRUE
//			ENDIF
//		ENDIF
		
		//Brad and Hostage go to Idle
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroBrad)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntroBrad) = 1.0
			TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue2, "idle_outside_cuboard_brad", sceneHostagesPos, sceneHostagesRot, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
			
			SET_FORCE_FOOTSTEP_UPDATE(pedBrad, FALSE)
		ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIntroHostage)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIntroHostage) = 1.0
			TASK_PLAY_ANIM_ADVANCED(pedHostage[HostageFemale], sAnimDictPrologue2, "idle_inside_cuboard_femalehostage", sceneHostagesPos, sceneHostagesRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedHostage[HostageFemale])
			
			SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageFemale], FALSE)
		ENDIF
		
		//Footsteps
		IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
			IF NOT HAS_LABEL_BEEN_TRIGGERED(FootstepHostageMale1)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageMale1], TRUE)
				
				SET_LABEL_AS_TRIGGERED(FootstepHostageMale1, TRUE)
			ENDIF
		ELSE
			IF HAS_LABEL_BEEN_TRIGGERED(FootstepHostageMale1)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageMale1], FALSE)
				
				SET_LABEL_AS_TRIGGERED(FootstepHostageMale1, FALSE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
			IF NOT HAS_LABEL_BEEN_TRIGGERED(FootstepHostageMale2)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageMale2], TRUE)
				
				SET_LABEL_AS_TRIGGERED(FootstepHostageMale2, TRUE)
			ENDIF
		ELSE
			IF HAS_LABEL_BEEN_TRIGGERED(FootstepHostageMale2)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageMale2], FALSE)
				
				SET_LABEL_AS_TRIGGERED(FootstepHostageMale2, FALSE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
			IF NOT HAS_LABEL_BEEN_TRIGGERED(FootstepHostageGuard)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageGuard], TRUE)
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_NORMAL)
				
				SET_LABEL_AS_TRIGGERED(FootstepHostageGuard, TRUE)
			ENDIF
		ELSE
			IF HAS_LABEL_BEEN_TRIGGERED(FootstepHostageGuard)
				SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageGuard], FALSE)
				
				SET_LABEL_AS_TRIGGERED(FootstepHostageGuard, FALSE)
			ENDIF
		ENDIF
		
		INT iRandom
		
		//Trevor Cover Idles
		IF IS_PED_IN_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
		AND IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.699219, -5205.045898, 84.018669>>, <<0.5, 0.5, 1.5>>)
			IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_A")
			AND NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_B")
			AND NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_C")
				IF iCoverAnimTimer = -1
					iCoverAnimTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1500, 3000)
				ENDIF
				
				IF GET_GAME_TIMER() > iCoverAnimTimer
					iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
					
					ANIM_DATA none
					ANIM_DATA animDataBlend1
					animDataBlend1.type = APT_SINGLE_ANIM
					animDataBlend1.anim0 = "Trevor_Cover_Impatient_A"
					animDataBlend1.dictionary0 = sAnimDictPrologue3_Impatient
					//animDataBlend1.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
					animDataBlend1.flags = AF_SECONDARY
					
					IF iRandom = 0
						//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_A", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						animDataBlend1.anim0 = "Trevor_Cover_Impatient_A"
						TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
					ELIF iRandom = 1
						//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_B", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						animDataBlend1.anim0 = "Trevor_Cover_Impatient_B"
						TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
					ELIF iRandom = 2
						//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_C", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						animDataBlend1.anim0 = "Trevor_Cover_Impatient_C"
						TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
					ENDIF
					
					iCoverAnimTimer = -1
				ENDIF
			ENDIF
		ENDIF
		
		//Hostages Panic into the Cupboard
		IF IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale1], sAnimDictPrologue2, "hostage01_enter_cuboard_malehostage01")
		OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageMale2], sAnimDictPrologue2, "hostage02_enter_cuboard_malehostage02")
		OR IS_ENTITY_PLAYING_ANIM(pedHostage[HostageGuard], sAnimDictPrologue2, "gaurd_enter_cuboard_gaurd")
			IF NOT IS_AMBIENT_SPEECH_PLAYING(pedHostage[HostageMale1])
			AND NOT IS_AMBIENT_SPEECH_PLAYING(pedHostage[HostageMale2])
			AND NOT IS_AMBIENT_SPEECH_PLAYING(pedHostage[HostageGuard])
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_FLAA)
					//PRO_FLAA	PROVICTIM1	What's he doing?					8
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedHostage[HostageGuard], "PROVICTIM1")
					
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedHostage[HostageGuard], "PRO_FLAA", "PROVICTIM1", SPEECH_PARAMS_FORCE)
					
					SET_LABEL_AS_TRIGGERED(PRO_FLAA,TRUE)
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_FLAB)
					//PRO_FLAB	PROVICTIM1	He's gonna kill us in here!			8
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedHostage[HostageMale2], "PROVICTIM1")
					
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedHostage[HostageMale2], "PRO_FLAB", "PROVICTIM1", SPEECH_PARAMS_FORCE)
					
					SET_LABEL_AS_TRIGGERED(PRO_FLAB,TRUE)
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_FLAC)
					//PRO_FLAC	PROVICTIM1	Leave us alone!						8
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedHostage[HostageMale1], "PROVICTIM1")
					
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedHostage[HostageMale1], "PRO_FLAC", "PROVICTIM1", SPEECH_PARAMS_FORCE)
					
					SET_LABEL_AS_TRIGGERED(PRO_FLAC,TRUE)
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_FLAD)
					//PRO_FLAD	PROVICTIM1	We didn't do shit to you!			8
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedHostage[HostageGuard], "PROVICTIM1")
					
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedHostage[HostageGuard], "PRO_FLAD", "PROVICTIM1", SPEECH_PARAMS_FORCE)
					
					SET_LABEL_AS_TRIGGERED(PRO_FLAD,TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		//Idle Dialogue
//		IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostageF)
//		AND NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostageP)
//			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				SWITCH iDialogueStage
//					CASE 0
//						IF GET_GAME_TIMER() > iDialogueTimer
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
//								
//								iDialogueStage++
//							ENDIF
//						ENDIF
//					BREAK
//					CASE 1
//						IF GET_GAME_TIMER() > iDialogueTimer
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostGap_1)
//									iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
//								ENDIF
//								
//								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_HostGap_1, "PRO_HostGap", "PRO_HostGap_1", CONV_PRIORITY_LOW)
//								
//								iDialogueStage++
//							ENDIF
//						ENDIF
//					BREAK
//					CASE 2
//						IF GET_GAME_TIMER() > iDialogueTimer
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostGap_2)
//									iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
//								ENDIF
//								
//								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_HostGap_2, "PRO_HostGap", "PRO_HostGap_2", CONV_PRIORITY_LOW)
//								
//								iDialogueStage++
//							ENDIF
//						ENDIF
//					BREAK
//					CASE 3
//						IF GET_GAME_TIMER() > iDialogueTimer
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_HostGap_3)
//									iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
//								ENDIF
//								
//								PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_HostGap_3, "PRO_HostGap", "PRO_HostGap_3", CONV_PRIORITY_LOW)
//								
//								iDialogueStage++
//							ENDIF
//						ENDIF
//					BREAK
//				ENDSWITCH
//			ENDIF
//		ENDIF
		
		//Disable Player Controls
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		STOP_CAM_SHAKING(camMain, TRUE)
		
		SAFE_REMOVE_BLIP(blipDestination)
		SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		SAFE_ADD_BLIP_PED(blipBuddy, pedBrad, FALSE)
		
		SAFE_REMOVE_BLIP(blipHostage[HostageMale1])
		SAFE_REMOVE_BLIP(blipHostage[HostageGuard])
		SAFE_REMOVE_BLIP(blipHostage[HostageFemale])
		SAFE_REMOVE_BLIP(blipHostage[HostageMale2])
		
		SET_FORCE_FOOTSTEP_UPDATE(pedBrad, FALSE)
		SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageMale1], FALSE)
		SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageGuard], FALSE)
		SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageFemale], FALSE)
		SET_FORCE_FOOTSTEP_UPDATE(pedHostage[HostageMale2], FALSE)
		
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(playerPedID, TRUE)
		
		SAFE_DELETE_OBJECT(objLightingRig[LIGHTING_RIG_HOSTAGE])
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

BOOL bPhone[4]

PROC LearnPhone()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Radar
		bRadar = TRUE
		
		//Print
		ADD_CONTACT_TO_PHONEBOOK(CHAR_DETONATEBOMB, MICHAEL_BOOK, FALSE)
		
		//Phone
		DISABLE_CELLPHONE(FALSE)
		
		//Assisted
		ASSISTED_MOVEMENT_REQUEST_ROUTE("Pro_S2")
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5310.6816, -5205.0249, 82.5146>>)	//<<5310.6885, -5204.9897, 82.5199>>)
			covPoint[0] = ADD_COVER_POINT(<<5310.6885, -5204.9897, 82.5199>>, 0.0, COVUSE_WALLTOBOTH, COVHEIGHT_TOOHIGH, COVARC_180)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		objBomb = CREATE_OBJECT(modBomb, <<5298.270020, -5187.850098, 83.870003>>)
		SET_ENTITY_ROTATION(objBomb, <<0.0, 0.0, -90.527321>>)
		FREEZE_ENTITY_POSITION(objBomb, TRUE)
		
		objBombGreen = CREATE_OBJECT(modBombGreen, <<5298.270020, -5187.850098, 83.870003>>)
		SET_ENTITY_ROTATION(objBombGreen, <<0.0, 0.0, -90.527321>>)
		FREEZE_ENTITY_POSITION(objBombGreen, TRUE)
		SET_ENTITY_VISIBLE(objBombGreen, FALSE)
		
//		objBomb3 = CREATE_OBJECT(modBomb3, <<5298.270020, -5187.850098, 83.870003 + 0.075>>)
//		SET_ENTITY_ROTATION(objBomb3, <<0.0, 0.0, -90.527321>>)
//		FREEZE_ENTITY_POSITION(objBomb3, TRUE)
		
		objBomb2 = CREATE_OBJECT(modBomb2, <<5298.270020, -5187.850098, 83.870003 + 0.075>>)
		SET_ENTITY_ROTATION(objBomb2, <<0.0, 0.0, -90.527321>>)
		FREEZE_ENTITY_POSITION(objBomb2, TRUE)
//		SET_ENTITY_VISIBLE(objBomb2, FALSE)
		
		objBomb1 = CREATE_OBJECT(modBomb1, <<5298.270020, -5187.850098, 83.870003 + 0.075>>)
		SET_ENTITY_ROTATION(objBomb1, <<0.0, 0.0, -90.527321>>)
		FREEZE_ENTITY_POSITION(objBomb1, TRUE)
		SET_ENTITY_VISIBLE(objBomb1, FALSE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Brad
			CLEAR_PED_TASKS_IMMEDIATELY(pedBrad)
			
			SET_PED_POSITION(pedBrad, <<5313.574, -5205.261, 82.519>>, 114.018)
			
			//Trevor
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.6885, -5204.9897, 82.5199>>, 355.8240 + 90.0)
			
			TASK_PUT_PED_DIRECTLY_INTO_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.6885, -5204.9897, 82.5199>>, -1, FALSE, 1.0, TRUE, TRUE, covPoint[0])
			
			//Player
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			
			SET_PED_POSITION(playerPedID, <<5308.8560, -5206.2939, 85.7187 - 3.2>>, 355.8240)
			
			SMOOTH_CLOSE_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>, TRUE, 1.0, 1.0, 0.0)
			SMOOTH_CLOSE_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>, TRUE, 1.0, 1.0, 0.0)
			
			SAFE_DELETE_PED(pedHostage[HostageMale1])
			SAFE_DELETE_PED(pedHostage[HostageGuard])
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 7)
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 8)
			SAFE_DELETE_PED(pedHostage[HostageFemale])
			SAFE_DELETE_PED(pedHostage[HostageMale2])
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_IN_COVER(playerPedMichael)
			SET_PED_CAPSULE(playerPedMichael, 0.5)
		ENDIF
		
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Return_1)
				PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Return_1, "PRO_Return", "PRO_Return_1", CONV_PRIORITY_HIGH)
				
				iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 15000)
			ENDIF
		ENDIF
		
		//Brad
		IF NOT IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue3_IdleAtCupboard, "Brad_Alert_Idle")
		AND NOT IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue3_IdleInVault, "idle_d")
		AND NOT IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue3, "react_to_explosion_brad")
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneHostage) = 1.0
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
				TASK_PLAY_ANIM(pedBrad, sAnimDictPrologue3_IdleAtCupboard, "Brad_Alert_Idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			ENDIF
		ELSE
			IF IS_ENTITY_AT_COORD(playerPedID, GET_ENTITY_COORDS(pedBrad), <<3.0, 3.0, 2.0>>)
				TASK_LOOK_AT_ENTITY(pedBrad, playerPedID, 5000)
			ENDIF
		ENDIF
		
		INT iRandom
		
		//Trevor Cover Idles
		IF IS_PED_IN_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
		AND IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.699219, -5205.045898, 84.018669>>,<<0.5, 0.5, 1.5>>)
			IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_A")
			AND NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_B")
			AND NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_C")
				IF iCoverAnimTimer = -1
					iCoverAnimTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1500, 3000)
				ENDIF
				
				IF GET_GAME_TIMER() > iCoverAnimTimer
					iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
					
					ANIM_DATA none
					ANIM_DATA animDataBlend1
					animDataBlend1.type = APT_SINGLE_ANIM
					animDataBlend1.anim0 = "Trevor_Cover_Impatient_A"
					animDataBlend1.dictionary0 = sAnimDictPrologue3_Impatient
					//animDataBlend1.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
					animDataBlend1.flags = AF_SECONDARY
					
					IF iRandom = 0
						//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_A", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						animDataBlend1.anim0 = "Trevor_Cover_Impatient_A"
						TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
					ELIF iRandom = 1
						//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_B", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						animDataBlend1.anim0 = "Trevor_Cover_Impatient_B"
						TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
					ELIF iRandom = 2
						//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_C", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						animDataBlend1.anim0 = "Trevor_Cover_Impatient_C"
						TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
					ENDIF
					
					iCoverAnimTimer = -1
				ENDIF
			ENDIF
		ENDIF
		
//		IF iCutsceneStage > 1
//			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY(CUTSCENE_SKIP_DELAY * 2)
//				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
//				
//				WHILE NOT IS_SCREEN_FADED_OUT()
//					WAIT_WITH_DEATH_CHECKS(0)
//				ENDWHILE
//				
//				SET_TIME_SCALE(1.0)
//				
//				TASK_USE_MOBILE_PHONE(playerPedID, FALSE)
//				
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)				
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				
//				REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(CHAR_DETONATEBOMB)
//				
//				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				
//				SETTIMERA(6000)
//				
//				iCutsceneStage = 10
//			ENDIF
//		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT IS_PHONE_ONSCREEN(TRUE)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_PHONE4")	//IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("PROHLP_PHONE4")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_PHONE5")   //OR IS_THIS_FLOATING_HELP_BEING_DISPLAYED("PROHLP_PHONE5")
						SAFE_CLEAR_HELP()	//SAFE_CLEAR_HELP(FALSE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5305.638184, -5209.354004, 82.518677>>, <<5317.589844, -5209.528809, 85.518677>>, 11.0)
					IF NOT IS_CELLPHONE_DISABLED()
						SAFE_CLEAR_HELP()
						DISABLE_CELLPHONE(TRUE)
					ENDIF
					
					PRINT_ADV(PRO_STAY2, "PRO_STAY2")
					
					SAFE_REMOVE_BLIP(blipTrevor)
					SAFE_REMOVE_BLIP(blipBuddy)
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<5309.4556, -5207.3184, 85.7187 - 3.2>>)
				ELSE
					IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY2")
						SAFE_CLEAR_THIS_PRINT("PRO_STAY2")
					ENDIF
					
					SAFE_REMOVE_BLIP(blipDestination)
					SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
					SAFE_ADD_BLIP_PED(blipBuddy, pedBrad, FALSE)
					
					IF IS_CELLPHONE_DISABLED()
						DISABLE_CELLPHONE(FALSE)
					ENDIF
					
					IF bPhone[0] = FALSE
						IF HAS_LABEL_BEEN_TRIGGERED(PRO_Return_1)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT_ADV(PRO_CHARGES, "PRO_CHARGES")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION_ADV(PRO_call, "PRO_call", CONV_PRIORITY_LOW, TRUE, DO_NOT_DISPLAY_SUBTITLES)
							ENDIF
							
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
								CLEAR_PED_TASKS(pedBrad)
								TASK_GO_STRAIGHT_TO_COORD(pedBrad, <<5313.9053, -5205.7490, 82.5187>>, PEDMOVE_WALK, -1, 98.0530)
							ENDIF
							
							SETTIMERB(0)
							
							bPhone[0] = TRUE
						ENDIF
					ELSE
						IF TIMERB() > DEFAULT_GOD_TEXT_TIME + 4000
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_STAY2")
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_CHARGES")
									IF GET_GAME_TIMER() > iDialogueTimer
										IF HAS_LABEL_BEEN_TRIGGERED(PRO_Return_1)
										AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
										AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle3_1)
												PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle3_1, "PRO_Idle3", "PRO_Idle3_1")
												
												TASK_LOOK_AT_ENTITY(playerPedTrevor, playerPedMichael, 3000, SLF_WHILE_NOT_IN_FOV)
											ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle3_2)
												PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle3_2, "PRO_Idle3", "PRO_Idle3_2")
												
												TASK_LOOK_AT_ENTITY(playerPedTrevor, playerPedMichael, 3000, SLF_WHILE_NOT_IN_FOV)
											ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle3_3)
												PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle3_3, "PRO_Idle3", "PRO_Idle3_3")
												
												TASK_LOOK_AT_ENTITY(playerPedTrevor, playerPedMichael, 3000, SLF_WHILE_NOT_IN_FOV)
											ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle3_4)
												PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle3_4, "PRO_Idle3", "PRO_Idle3_4")
												
												TASK_LOOK_AT_ENTITY(playerPedTrevor, playerPedMichael, 3000, SLF_WHILE_NOT_IN_FOV)
											ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle3_5)
												PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle3_5, "PRO_Idle3", "PRO_Idle3_5")
												
												TASK_LOOK_AT_ENTITY(playerPedTrevor, playerPedMichael, 3000, SLF_WHILE_NOT_IN_FOV)
											ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle3_6)
												PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Idle3_6, "PRO_Idle3", "PRO_Idle3_6")
												
												TASK_LOOK_AT_ENTITY(playerPedTrevor, playerPedMichael, 3000, SLF_WHILE_NOT_IN_FOV)
											ENDIF
											
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 15000)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PHONE_ONSCREEN()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_PHONE1")
							PRINT_HELP_FOREVER("PROHLP_PHONE1")
						ENDIF
					ELSE
						IF IS_PHONE_ONSCREEN(TRUE)
						AND NOT IS_CONTACTS_LIST_ON_SCREEN()
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_PHONE4")
								PRINT_HELP_FOREVER("PROHLP_PHONE4")
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_detonate)
								IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF GET_GAME_TIMER() > iDialogueTimer - 1000
										CREATE_CONVERSATION_ADV(PRO_detonate, "PRO_detonate")
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_PHONE_ONSCREEN(TRUE)
						AND IS_CONTACTS_LIST_ON_SCREEN()
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_PHONE5")
								PRINT_HELP_FOREVER("PROHLP_PHONE5")
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_callit)
								IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF GET_GAME_TIMER() > iDialogueTimer - 1000
										CREATE_CONVERSATION_ADV(PRO_callit, "PRO_callit")
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bPhone[1] = FALSE
						IF IS_PHONE_ONSCREEN(TRUE)
						AND NOT IS_CONTACTS_LIST_ON_SCREEN()
							bPhone[1] = TRUE
						ENDIF
					ELIF bPhone[2] = FALSE
						IF bPhone[3] = FALSE
							IF IS_PHONE_ONSCREEN(TRUE)
							AND IS_CONTACTS_LIST_ON_SCREEN()
								bPhone[3] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfVaultExplosion)
						IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion) != RFMO_STATE_PRIMED
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion, RFMO_STATE_PRIMING)
						ENDIF
					ENDIF
					
					IF IS_CALLING_CONTACT(CHAR_DETONATEBOMB)
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						PLAY_AUDIO(PROLOGUE_TEST_PRE_SAFE_EXPLOSION)
						
						CLEAR_PRINTS()
						SAFE_CLEAR_HELP()
						//CLEAR_TEXT()
						
						//Audio Scene
						IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_THREATEN_HOSTAGES")
							STOP_AUDIO_SCENE("PROLOGUE_THREATEN_HOSTAGES")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DETONATE_CHARGES")
							START_AUDIO_SCENE("PROLOGUE_DETONATE_CHARGES")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_MUTE_SPRINKLERS")
							START_AUDIO_SCENE("PROLOGUE_MUTE_SPRINKLERS")
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED(SLOWMO_PROLOGUE_VAULT)
							ACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_PROLOGUE_VAULT")
							
							SET_LABEL_AS_TRIGGERED(SLOWMO_PROLOGUE_VAULT, TRUE)
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(1.5, 1.0, REPLAY_IMPORTANCE_HIGH)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneHostage)
					IF NOT IS_ENTITY_AT_COORD(pedBrad, <<5313.7813, -5205.1440, 83.5237>>, <<0.5, 0.5, 2.0>>)
						IF GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedBrad, <<5313.7813, -5205.1440, 83.5237>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 114.0175)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				LOAD_STREAM("PROLOGUE_BLOW_THE_VAULT_MASTER")
				
				PREPARE_ALARM("PROLOGUE_VAULT_ALARMS")
				
				IF TIMERA() > 1500 + 1000
					SAFE_DELETE_PED(pedHostage[HostageMale1])
					SAFE_DELETE_PED(pedHostage[HostageGuard])
					REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 7)
					REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 8)
					SAFE_DELETE_PED(pedHostage[HostageFemale])
					SAFE_DELETE_PED(pedHostage[HostageMale2])
					
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					CLEAR_TEXT()
					
					bRadar = FALSE
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					CLEAR_AREA(<<5296.97, -5188.88, 82.74>>, 10.0, TRUE)
					
					SET_CAM_PARAMS(camMain, 
									<<5297.291992,-5187.329590,83.824295>>,
									<<6.358143,-8.767557,-122.514175>>,
									28.340401,
									0,
									GRAPH_TYPE_DECEL,
									GRAPH_TYPE_DECEL)
					
					SET_CAM_PARAMS(camMain, 
									<<5297.325195,-5187.351074,83.828720>>,
									<<6.358143,-8.767557,-122.514175>>,
									28.340401,
									1800,
									GRAPH_TYPE_DECEL,
									GRAPH_TYPE_DECEL)
					
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camMain, 1.1)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camMain, 2.8)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					IF NOT bAudioStream
						SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
						
						bAudioStream = TRUE
					ENDIF
					
					PLAY_STREAM_FRONTEND()
					
					SET_TIMECYCLE_MODIFIER("cashdepot")
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_CashD_vault")
					
//					SET_CAM_MOTION_BLUR_STRENGTH(camMain, 0.3)
					
					SET_PED_POSITION(playerPedID, <<5308.6709, -5206.5474, 82.5186>>, 269.1302)
					
					IF NOT HAS_PED_GOT_WEAPON(playerPedID, wtCarbineRifle)
						GIVE_WEAPON_TO_PED(playerPedID, wtCarbineRifle, 500, TRUE)
					ENDIF
					
					SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
					
					IF NOT bVideoRecording
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
						
						bVideoRecording = TRUE
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(objLightingRig[LIGHTING_RIG_VAULT])
						objLightingRig[LIGHTING_RIG_VAULT] = CREATE_OBJECT(PROP_VAULT_DOOR_SCENE, <<5297.717, -5188.909, 81.575>>)
						FORCE_ROOM_FOR_ENTITY(objLightingRig[LIGHTING_RIG_VAULT], intDepot, HASH("V_CashD_side"))
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				PREPARE_ALARM("PROLOGUE_VAULT_ALARMS")
				
				IF IS_ENTITY_VISIBLE(objBomb2)
				AND TIMERA() > 500
					SET_ENTITY_VISIBLE(objBomb2, FALSE)
					SET_ENTITY_VISIBLE(objBomb1, TRUE)
				ENDIF
				
				IF IS_ENTITY_VISIBLE(objBomb1)
				AND TIMERA() > 1500
					SET_ENTITY_VISIBLE(objBomb1, FALSE)
				ENDIF
				
				IF TIMERA() > 1000
					SET_ENTITY_VISIBLE(objBomb, FALSE)
					SET_ENTITY_VISIBLE(objBombGreen, TRUE)
				ENDIF
				
				IF TIMERA() > 1800
					SET_ENTITY_VISIBLE(objBomb, FALSE)
					SET_ENTITY_VISIBLE(objBombGreen, TRUE)
					
					IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfVaultExplosion)
						IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion) = RFMO_STATE_PRIMED
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion, RFMO_STATE_START_ANIM)
						ENDIF
					ENDIF
					
					//Sound
//					sIDVaultExplosion = GET_SOUND_ID()
//					PLAY_SOUND_FRONTEND(sIDVaultExplosion, "Vault_Blast", "Prologue_Sounds")
					
					//Audio Scene
					IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_VAULT_RAYFIRE")
						START_AUDIO_SCENE("PROLOGUE_VAULT_RAYFIRE")
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DETONATE_CHARGES")
						STOP_AUDIO_SCENE("PROLOGUE_DETONATE_CHARGES")
					ENDIF
					
					IF DOES_CAM_EXIST(camMain)
						SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camMain, 0.0)
						SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 0.0)
						SET_CAM_DOF_FNUMBER_OF_LENS(camMain, 2.8)
						
						DESTROY_CAM(camMain)
					ENDIF
					
					IF NOT DOES_CAM_EXIST(camMain)
						camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					ENDIF
					
					SET_CAM_PARAMS(camMain, 
									<<5292.704102, -5185.750977, 82.847717>>,
									<<9.034329, -2.898424, -131.669739>>,
									45.0,
									0,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					
					SAFE_DELETE_OBJECT(objLightingRig[LIGHTING_RIG_VAULT])
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				PREPARE_ALARM("PROLOGUE_VAULT_ALARMS")
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(VaultBlast)
					IF TIMERA() > 300
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("ent_ray_pro1_vault_exp_lit", <<5298.20068, -5189.05176, 83.86238>>, VECTOR_ZERO)
						
						SHAKE_CAM(camMain, "GRENADE_EXPLOSION_SHAKE", 3.0)
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
						
						//Particles
						IF HAS_PTFX_ASSET_LOADED()
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmoke)
								ptfxSmoke = START_PARTICLE_FX_LOOPED_AT_COORD("scr_prologue_vault_haze", <<5299, -5189, 82.6>>, VECTOR_ZERO)
							ENDIF
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFog)
								ptfxFog = START_PARTICLE_FX_LOOPED_AT_COORD("scr_prologue_vault_fog", <<5299, -5189, 82.6>>, VECTOR_ZERO)
							ENDIF
						ENDIF
						
						IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_MUTE_SPRINKLERS")
							STOP_AUDIO_SCENE("PROLOGUE_MUTE_SPRINKLERS")
						ENDIF
						
						SET_TIME_SCALE(0.5)	PRINTLN("SET_TIME_SCALE(0.5)")
						
						SET_LABEL_AS_TRIGGERED(VaultBlast, TRUE)
					ENDIF
				ENDIF
				
				IF TIMERA() > 400
					PLAY_AUDIO(PROLOGUE_TEST_COLLECT_MONEY)
			        
					START_ALARM("PROLOGUE_VAULT_ALARMS", FALSE)
					
					SAFE_DELETE_OBJECT(objBomb)
					SAFE_DELETE_OBJECT(objBombGreen)
					SAFE_DELETE_OBJECT(objBomb3)
					SAFE_DELETE_OBJECT(objBomb2)
					SAFE_DELETE_OBJECT(objBomb1)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 4
				IF TIMERA() > 250
//					SET_CAM_PARAMS(camMain, 
//									<<5292.692871, -5185.696777, 83.150391>>,
//									<<4.563637, 4.519622, -132.361115>>,
//									44.864380,
//									0,
//									GRAPH_TYPE_LINEAR,
//									GRAPH_TYPE_LINEAR)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 5
				IF TIMERA() > 450 //1250
					SET_TIME_SCALE(1.0)	PRINTLN("SET_TIME_SCALE(1.0)")
										
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_VAULT_RAYFIRE")
						STOP_AUDIO_SCENE("PROLOGUE_VAULT_RAYFIRE")
					ENDIF
					
					STOP_CAM_SHAKING(camMain, FALSE)
					
					CLEAR_TIMECYCLE_MODIFIER()
					
//					SET_CAM_MOTION_BLUR_STRENGTH(camMain, 0.0)
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Safe_1, "PRO_Safe", "PRO_Safe_1", CONV_PRIORITY_HIGH)
					
//					SET_CAM_PARAMS(camMain, 
//									<<5307.003418,-5206.881836,84.703842>>,
//									<<-15.874626,0.000000,-73.740723>>,
//									46.066589,
//									0,
//									GRAPH_TYPE_LINEAR,
//									GRAPH_TYPE_LINEAR)
//					
//					SET_CAM_PARAMS(camMain, 
//									<<5306.786621,-5206.944824,84.768021>>,
//									<<-15.874626,0.000000,-73.740723>>,
//									46.066589,
//									3000,
//									GRAPH_TYPE_LINEAR,
//									GRAPH_TYPE_LINEAR)
					
					//Camera
					IF NOT DOES_CAM_EXIST(camAnim)
						camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					ENDIF
					
					sceneVaultReact = CREATE_SYNCHRONIZED_SCENE(sceneVaultReactPos, sceneVaultReactRot)
					sceneVaultReactCam = CREATE_SYNCHRONIZED_SCENE(sceneVaultReactCamPos, sceneVaultReactCamRot)
					
					SET_PED_POSITION(playerPedID, <<5307.4751, -5207.1147, 82.5187>> + <<-0.08, 0.32, 0.0>>, 272.3664)
					TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue3, "react_to_explosion_player_zero", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT, 0.075 + 0.050)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID)
					CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneVaultReact, sAnimDictPrologue3, "react_to_explosion_player_two", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
					TASK_SYNCHRONIZED_SCENE(pedBrad, sceneVaultReact, sAnimDictPrologue3, "react_to_explosion_brad", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
					PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneVaultReactCam, "react_to_explosion_cam", sAnimDictPrologue3_Cam)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneVaultReact, 0.075 + 0.050)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneVaultReactCam, 0.421)
					
					SET_CAM_ACTIVE(camAnim, TRUE)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_CashD_reception")
					
//					SHAKE_CAM(camMain, "MEDIUM_EXPLOSION_SHAKE", 0.2)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 6
				//IF GET_SYNCHRONIZED_SCENE_PHASE(sceneVaultReact) >= 0.069 + 0.050
					IF HAS_PTFX_ASSET_LOADED()
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_prologue_ceiling_debris", <<5310.24512, -5205.66309, 85.22590>>, <<0.0, 0.0, 0.0>>)
					ENDIF
					
					ADVANCE_CUTSCENE()
				//ENDIF
			BREAK
			CASE 7
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneVaultReact) >= 0.085 + 0.050
					IF HAS_PTFX_ASSET_LOADED()
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_prologue_ceiling_debris", <<5309.8, -5207.6, 85.40824>>, <<0.0, 0.0, 90.0>>)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 8
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneVaultReact) >= 0.087 + 0.050 //0.075 + 0.050
					IF HAS_PTFX_ASSET_LOADED()
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_prologue_ceiling_debris", <<5313.99268, -5207.3, 85.34588>>, <<0.0, 0.0, 180.0>>)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 9
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneVaultReact) >= 0.350 + 0.050
					STOP_CAM_SHAKING(camMain, TRUE)
					
					IF NOT DOES_CAM_EXIST(camInterpDOF)
						camInterpDOF = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					ENDIF
					
					SET_CAM_PARAMS(camInterpDOF, 
									<<5295.858887,-5188.994141,82.992493>>,
									<<3.961173,-0.003078,-90.428894>>,
									35.788742,
									0,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camInterpDOF, 8.0)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camInterpDOF, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camInterpDOF, 1.0)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camInterpDOF, 0.0)
					
					SHAKE_CAM(camInterpDOF, "HAND_SHAKE", 0.5)
					
					SET_CAM_PARAMS(camMain, 
									<<5296.373535,-5188.994141,83.027702>>,
									<<3.408814,-0.003078,-91.278107>>,
									35.788742,
									0,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					
					//SET_CAM_ACTIVE(camMain, TRUE)
					
					IF DOES_CAM_EXIST(camAnim)
						DESTROY_CAM(camAnim)
					ENDIF
					
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camMain, 8.0)
					SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 1.0)
					SET_CAM_DOF_FNUMBER_OF_LENS(camMain, 1.3)
					SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 0.0)
					
					SHAKE_CAM(camMain, "HAND_SHAKE", 0.5)
					
					SET_CAM_ACTIVE_WITH_INTERP(camMain, camInterpDOF, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					
					CLEAR_PED_TASKS(playerPedID)
					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					CLEAR_PED_TASKS(pedBrad)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
//					SET_CAM_MOTION_BLUR_STRENGTH(camMain, 0.3)
					
					SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_CashD_vault")
					
					//ACTIVATE_INTERIOR_ENTITY_SET(intDepot, "V_CashDepot_dust")
					
					//REFRESH_INTERIOR(intDepot)
					
					objDebris = CREATE_OBJECT_NO_OFFSET(V_ILev_CD_Dust, <<5312.14, -5209.04, 83.02>>)
					FREEZE_ENTITY_POSITION(objDebris, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(V_ILev_CD_Dust)
					
					IF HAS_PTFX_ASSET_LOADED()
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_prologue_ceiling_debris", <<5298.206055 ,-5189.063477 ,85.281166>>, <<0.0, 0.0, 180.0>>)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 10
				IF TIMERA() > 3000
					CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					
					SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.6885, -5204.9897, 82.5199>>, 355.8240 + 90.0)
					
					TASK_PUT_PED_DIRECTLY_INTO_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.6885, -5204.9897, 82.5199>>, -1, FALSE, 0, TRUE, TRUE, covPoint[0])
					
					SET_PED_POSITION(playerPedID, <<5308.8560, -5206.2939, 85.7187 - 3.2>>, 355.8240)
					
					FORCE_PED_MOTION_STATE(playerPedID, MS_ACTIONMODE_IDLE)
					
					bRadar = TRUE
					
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					IF bVideoRecording
						REPLAY_STOP_EVENT()
						
						bVideoRecording = FALSE
					ENDIF
					
					IF DOES_CAM_EXIST(camMain)
						SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camMain, 0.0)
						SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 0.0)
						SET_CAM_DOF_FNUMBER_OF_LENS(camMain, 2.8)
						
						DESTROY_CAM(camMain)
					ENDIF
					
					IF NOT DOES_CAM_EXIST(camMain)
						camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					ENDIF
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-5)
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-30)
					ENDIF
					
					REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(CHAR_DETONATEBOMB)
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
//					SET_CAM_MOTION_BLUR_STRENGTH(camMain, 0.0)
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF iCutsceneStage > 1 AND iCutsceneStage < 10
			//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		ENDIF
		
		//Close Doors
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
		AND ((IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5311.497559, -5204.677246, 85.718628 - 3.2>>, <<5311.415039, -5220.339355, 88.718628 - 3.2>>, 12.0, FALSE, TRUE)
		AND NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5308.802246, -5204.578613, 84.018631>>, <<1.0, 1.0, 1.5>>)))
			SET_LABEL_AS_TRIGGERED(DoorIsClosing, TRUE)
		ELSE
			UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
			UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		SET_CAM_ACTIVE(camMain, TRUE)
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		IF DOES_CAM_EXIST(camInterpDOF)
			DESTROY_CAM(camInterpDOF)
		ENDIF
		
		CLEAR_TIMECYCLE_MODIFIER()
		
		SAFE_DELETE_OBJECT(objLightingRig[LIGHTING_RIG_VAULT])
		
		SAFE_DELETE_PED(pedHostage[HostageMale1])
		SAFE_DELETE_PED(pedHostage[HostageGuard])
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 8)
		SAFE_DELETE_PED(pedHostage[HostageFemale])
		SAFE_DELETE_PED(pedHostage[HostageMale2])
		
		SAFE_DELETE_OBJECT(objBomb)
		SAFE_DELETE_OBJECT(objBombGreen)
		SAFE_DELETE_OBJECT(objBomb3)
		SAFE_DELETE_OBJECT(objBomb2)
		SAFE_DELETE_OBJECT(objBomb1)
		
		IF NOT DOES_ENTITY_EXIST(objDebris)
			objDebris = CREATE_OBJECT_NO_OFFSET(V_ILev_CD_Dust, <<5312.14, -5209.04, 83.02>>)
			FREEZE_ENTITY_POSITION(objDebris, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(V_ILev_CD_Dust)
		ENDIF
		
		//Score
		PLAY_AUDIO(PROLOGUE_TEST_COLLECT_MONEY)
		
		//Sound
		IF sIDVaultExplosion != -1
			STOP_SOUND(sIDVaultExplosion)
			RELEASE_SOUND_ID(sIDVaultExplosion)
			sIDVaultExplosion = -1
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED(SLOWMO_PROLOGUE_VAULT)
			DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_PROLOGUE_VAULT")
		ENDIF
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_THREATEN_HOSTAGES")
			STOP_AUDIO_SCENE("PROLOGUE_THREATEN_HOSTAGES")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_VAULT_RAYFIRE")
			STOP_AUDIO_SCENE("PROLOGUE_VAULT_RAYFIRE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_MUTE_SPRINKLERS")
			STOP_AUDIO_SCENE("PROLOGUE_MUTE_SPRINKLERS")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DETONATE_CHARGES")
			STOP_AUDIO_SCENE("PROLOGUE_DETONATE_CHARGES")
		ENDIF
		
		SAFE_REMOVE_BLIP(blipDestination)
		SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		SAFE_ADD_BLIP_PED(blipBuddy, pedBrad, FALSE)
		
		SAFE_CLEAR_HELP(TRUE)
		//SAFE_CLEAR_FLOATING_HELP()
		REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(CHAR_DETONATEBOMB)
		
		SET_TIME_SCALE(1.0)	PRINTLN("SET_TIME_SCALE(1.0)")
		
		HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
		
		IF DOES_CAM_EXIST(camMain)
			IF IS_CAM_RENDERING(camMain)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)		
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			ENDIF
		ENDIF
		
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Phone
		DISABLE_CELLPHONE(TRUE)
		
		CLEAR_PED_TASKS(playerPedID)
//		CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//		CLEAR_PED_TASKS(pedBrad)
		
		SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
		
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfVaultExplosion)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion) != RFMO_STATE_END
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion, RFMO_STATE_ENDING)
			ENDIF
		ENDIF
		
		UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
		UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC LearnBlips()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Radar
		bRadar = TRUE
		
		//Print
		CLEAR_PRINTS()
		CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV("PRO_Safe", "PRO_Safe_2", CONV_PRIORITY_HIGH)	//PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Safe_2, "PRO_Safe", "PRO_Safe_2", CONV_PRIORITY_HIGH)
		
		//Blips
		SAFE_ADD_BLIP_LOCATION(blipDestination, <<5303.5576, -5188.4141, 85.7189 - 3.2>>)
		
		SET_BLIP_FLASH_DURATION(blipDestination, 5000)
		
		PRINT_HELP_ADV(PROHLP_BLIPS4, "PROHLP_BLIPS4")
		
		//Particles
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmoke)
			ptfxSmoke = START_PARTICLE_FX_LOOPED_AT_COORD("scr_prologue_vault_haze", <<5299, -5189, 82.6>>, VECTOR_ZERO)
		ENDIF
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFog)
			ptfxFog = START_PARTICLE_FX_LOOPED_AT_COORD("scr_prologue_vault_fog", <<5299, -5189, 82.6>>, VECTOR_ZERO)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		iCurrentTake = 0
		iDisplayedTake = 0
		
		CREATE_MONEY_PICKUPS(<<5303.31055, -5189.00049, 82.51867>>, 2500, 5)
		
		INT receivingStruct
		STAT_GET_INT(SP0_TOTAL_CASH, receivingStruct)
    	iAccountBalance = receivingStruct
		
		//Cutscene
		REQUEST_CUTSCENE("pro_mcs_2")
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			SET_PED_POSITION(playerPedID, <<5308.8560, -5206.2939, 85.7187 - 3.2>>, 355.8240)
			
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.6885, -5204.9897, 82.5199>>, 355.8240 + 90.0)
			TASK_PUT_PED_DIRECTLY_INTO_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.6885, -5204.9897, 82.5199>>, -1, FALSE, 0, TRUE, TRUE, covPoint[0])
			
			CLEAR_PED_TASKS_IMMEDIATELY(pedBrad)
			SET_PED_POSITION(pedBrad, <<5313.9053, -5205.7490, 82.5187>>, 98.0530)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		INT receivingStruct
		STAT_GET_INT(SP0_TOTAL_CASH, receivingStruct)
    	
		IF receivingStruct > iAccountBalance
			iCurrentTake += receivingStruct - iAccountBalance
			
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake	#IF IS_DEBUG_BUILD	PRINTLN("g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
			
			iAccountBalance = receivingStruct
		ENDIF
		
		//SET_PED_CAPSULE(pedBrad, 0.5)
		
		UNLOCK_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
		UNLOCK_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR)
		
		IF IS_PED_RUNNING(playerPedID)
		AND NOT IS_PED_SHOOTING(playerPedID)
		AND IS_ENTITY_AT_COORD(playerPedID, <<5308.813477, -5204.577637, 84.018631>>, <<1.25, 1.5, 1.5>>)
		AND (DOOR_SYSTEM_GET_OPEN_RATIO(iLeftReceptionDoor) <> 0.0
		OR DOOR_SYSTEM_GET_OPEN_RATIO(iRightReceptionDoor) <> 0.0)
			IF sIDBargeDoor = -1
				sIDBargeDoor = GET_SOUND_ID()
				PLAY_SOUND_FROM_COORD(sIDBargeDoor, "Barge_Door", <<5308.813477, -5204.577637, 84.018631>>, "Prologue_Sounds")
			ENDIF
		ENDIF
		
		//Request Cutscene Variations - pro_mcs_2
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
		ENDIF
		
//		IF NOT IS_ENTITY_AT_COORD(pedBrad, <<5314.453613, -5205.312988, 84.018631>>, <<0.75, 0.75, 1.5>>)
//			CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			
//			OPEN_SEQUENCE_TASK(seqMain)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<5300.7422, -5190.4980, 82.5184>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 248.5686 + 46.0)
//				TASK_PLAY_ANIM(NULL, sAnimDictPrologue3_IdleInVault, "idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
//			CLOSE_SEQUENCE_TASK(seqMain)
//			TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
//			CLEAR_SEQUENCE_TASK(seqMain)
//			
//			SET_LABEL_AS_TRIGGERED(TrevorIdleVault, TRUE)
//		ENDIF
		
		//Trevor Idle
		IF NOT HAS_LABEL_BEEN_TRIGGERED(TrevorIdleVault)
			IF iCutsceneStage <= 1
				IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5308.730957, -5188.500977, 82.518669>>, <<5298.838867, -5188.512695, 86.018364>>, 9.0)
					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					CLEAR_PED_SECONDARY_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<5300.7422, -5190.4980, 82.5184>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 248.5686 + 46.0)
						TASK_PLAY_ANIM(NULL, sAnimDictPrologue3_IdleInVault, "idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0, FALSE, AIK_DISABLE_LEG_IK)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					SET_LABEL_AS_TRIGGERED(TrevorIdleVault, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				//Text
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_VAULT_PRINT)
					IF TIMERA() > 1000 //Threshold for audio conversation to play... :/
						IF HAS_LABEL_BEEN_TRIGGERED(PRO_Safe_2)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT_ADV(PRO_VAULT, "PRO_VAULT", DEFAULT_GOD_TEXT_TIME - 4000, FALSE)
							
							SET_LABEL_AS_TRIGGERED(PRO_VAULT_PRINT, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				//Trevor leaves cover
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5310.549316, -5202.885742, 82.518677>>, <<5307.083496, -5202.865234, 85.518677>>, 3.0)
						//CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						CLEAR_PED_SECONDARY_TASK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						
						TASK_FOLLOW_WAYPOINT_RECORDING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sWaypointRoute1)
						SET_COMBAT_FLOAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.0)
						
						//Audio Scene
						IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_TO_VAULT")
							START_AUDIO_SCENE("PROLOGUE_GET_TO_VAULT")
						ENDIF
					ENDIF
					
					INT iRandom
					
					IF IS_PED_IN_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					AND IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5310.699219, -5205.045898, 84.018669>>,<<0.5, 0.5, 1.5>>)
						IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_A")
						AND NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_B")
						AND NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_C")
							IF iCoverAnimTimer = -1
								iCoverAnimTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1500, 3000)
							ENDIF
							
							IF GET_GAME_TIMER() > iCoverAnimTimer
								iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
								
								ANIM_DATA none
								ANIM_DATA animDataBlend1
								animDataBlend1.type = APT_SINGLE_ANIM
								animDataBlend1.anim0 = "Trevor_Cover_Impatient_A"
								animDataBlend1.dictionary0 = sAnimDictPrologue3_Impatient
								//animDataBlend1.filter = GET_HASH_KEY("BONEMASK_UPPERONLY")
								animDataBlend1.flags = AF_SECONDARY
								
								IF iRandom = 0
									//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_A", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
									animDataBlend1.anim0 = "Trevor_Cover_Impatient_A"
									TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
								ELIF iRandom = 1
									//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_B", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
									animDataBlend1.anim0 = "Trevor_Cover_Impatient_B"
									TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
								ELIF iRandom = 2
									//TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sAnimDictPrologue3_Impatient, "Trevor_Cover_Impatient_C", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
									animDataBlend1.anim0 = "Trevor_Cover_Impatient_C"
									TASK_SCRIPTED_ANIMATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
								ENDIF
								
								iCoverAnimTimer = -1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Waypoints
				INT iWaypointPlayer, iWaypointTrevor, iWaypointMax
				
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					WAYPOINT_RECORDING_GET_NUM_POINTS(sWaypointRoute1, iWaypointMax)
					
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sWaypointRoute1, GET_ENTITY_COORDS(playerPedID), iWaypointPlayer)
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sWaypointRoute1, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), iWaypointTrevor)
					
					iWaypointPlayer = CLAMP_INT((iWaypointPlayer - 1) - 1, 0, iWaypointMax)
					
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(playerPedID), PEDMOVEBLENDRATIO_WALK, PEDMOVEBLENDRATIO_SPRINT))
				ENDIF
				
				IF iWaypointPlayer > iWaypointTrevor
				OR iWaypointTrevor > 16	//Don't block vault door
				OR iWaypointTrevor = 1	//Don't block reception doors
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF WAYPOINT_PLAYBACK_GET_IS_PAUSED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							WAYPOINT_PLAYBACK_RESUME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						ENDIF
					ENDIF
				ELSE
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT WAYPOINT_PLAYBACK_GET_IS_PAUSED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							WAYPOINT_PLAYBACK_PAUSE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PromptVault)
					AND GET_GAME_TIMER() > iDialogueTimer
						IF IS_ENTITY_AT_COORD(playerPedID, <<5306.708496, -5180.153809, 84.518669>>, <<14.5, 3.0, 2.0>>)
							IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF iDialogueLineCount[1] > 0
									CREATE_CONVERSATION_ADV(PRO_Vault, "PRO_Vault", CONV_PRIORITY_MEDIUM, FALSE)
									
									iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
									
									SET_LABEL_AS_TRIGGERED(PromptVault, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_BackHere)
							IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5295.025391, -5185.918945, 82.518784>>, <<5295.129395, -5178.219238, 86.018784>>, 6.5)
								CREATE_CONVERSATION_ADV(PRO_BackHere, "PRO_BackHere")
								
								iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
							ENDIF
						ENDIF
					ENDIF
					
					IF GET_GAME_TIMER() > iDialogueTimer
						IF iDialogueLineCount[0] = -1
							iDialogueLineCount[0] = 5
						ELIF iDialogueLineCount[1] = -1
							iDialogueLineCount[1] = 4
						ENDIF
						
						IF NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5308.730957, -5188.500977, 82.518669>>, <<5298.838867, -5188.512695, 86.018364>>, 9.0)
						AND IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5308.730957, -5188.500977, 82.518669>>, <<5298.838867, -5188.512695, 86.018364>>, 9.0)
							iDialogueStage = 1
						ELSE
							iDialogueStage = 0
						ENDIF
						
						IF iDialogueLineCount[iDialogueStage] > 0
							IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF GET_PED_DESIRED_MOVE_BLEND_RATIO(playerPedTrevor) < 1.0
								AND NOT IS_ENTITY_AT_COORD(playerPedMichael, GET_ENTITY_COORDS(playerPedTrevor), <<3.5, 3.5, 3.0>>)
									IF iDialogueStage = 0
										CREATE_CONVERSATION_ADV(PRO_GenIdle, "PRO_GenIdle", CONV_PRIORITY_MEDIUM, FALSE)
									ELIF iDialogueStage = 1
										CREATE_CONVERSATION_ADV(PRO_Vault, "PRO_Vault", CONV_PRIORITY_MEDIUM, FALSE)
									ENDIF
								ENDIF
								
								iDialogueLineCount[iDialogueStage]--
							ENDIF
						ENDIF
						
						iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
					ENDIF
				ENDIF
				
				//Coughing
				IF NOT HAS_LABEL_BEEN_TRIGGERED(COUGHING)
					IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5296.629395, -5186.645996, 82.518631>>, <<5296.792969, -5191.115234, 85.518631>>, 3.0)
					AND NOT IS_PED_IN_COVER(playerPedID) AND NOT IS_PED_GOING_INTO_COVER(playerPedID)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(playerPedID, "COUGH", "WAVELOAD_PAIN_MALE")
						
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue_Cough, "walk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						ENDIF
						
						SET_LABEL_AS_TRIGGERED(COUGHING, TRUE)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue_Cough, "walk")
					SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5303.912109, -5184.687988, 85.690247 - 3.2>>, <<5303.916504, -5191.533691, 88.718628 - 3.2>>, 10.0, FALSE, TRUE)
					//Blips
					IF NOT DOES_BLIP_EXIST(blipCash)
						objCash = CREATE_OBJECT(PROP_PING_PONG, <<5307.6755, -5191.0529, 83.0186>>)
						FREEZE_ENTITY_POSITION(objCash, TRUE)
						SET_ENTITY_VISIBLE(objCash, FALSE)
						blipCash = CREATE_BLIP_FOR_OBJECT(objCash)
						SET_BLIP_NAME_FROM_TEXT_FILE(blipCash, "PRO_BLIPCASH")
//						SET_OBJECT_AS_NO_LONGER_NEEDED(objCash)
//						blipCash = ADD_BLIP_FOR_COORD(<<5307.6755, -5191.0529, 83.0186>>)
//						SET_BLIP_COLOUR(blipCash, BLIP_COLOUR_GREEN)
//						SET_BLIP_DIM(blipCash, FALSE)
						
						SAFE_REMOVE_BLIP(blipDestination)
						
						CLEAR_PRINTS()
						SAFE_CLEAR_HELP()
						
						PRINT_ADV(PRO_CASH, "PRO_CASH")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CREATE_CONVERSATION_ADV(PRO_see, "PRO_see", CONV_PRIORITY_LOW, TRUE, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
						//PRINT_HELP_ADV(PROHLP_BLIPS2, "PROHLP_BLIPS2")	//PRINT_HELP_ADV_POS("PROHLP_BLIPS2", <<5307.6755, -5191.0529, 84.0186>>)
						
						SET_BLIP_FLASH_DURATION(blipCash, 3000)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_ADV(PROHLP_BLIPS2, "PROHLP_BLIPS2")
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_GAME_TIMER() > iDialogueTimer
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Bills)
							CREATE_CONVERSATION_ADV(PRO_Bills, "PRO_Bills")
						ELSE
							IF iDialogueLineCount[0] > 0
								CREATE_CONVERSATION_ADV(PRO_GenIdle, "PRO_GenIdle", CONV_PRIORITY_MEDIUM, FALSE)
								
								iDialogueLineCount[0]--
							ENDIF
						ENDIF
						
						iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(blipCash)
					IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5307.583496, -5184.533203, 82.018890>>, <<5307.440918, -5191.546875, 86.567856>>, 3.0)	//<<5308.160645, -5185.493652, 82.018631>>, <<5308.078125, -5191.555176, 86.518639>>, 2.75)
					OR (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
					AND IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5307.583496, -5184.533203, 82.018890>>, <<5307.440918, -5191.546875, 86.567856>>, 4.5))
						IF SAFE_START_CUTSCENE(0.0, FALSE)	//, FALSE)
							IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedMichael)
								
								REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_MICHAEL], "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedTrevor)
								
								REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
								LOAD_AUDIO(PROLOGUE_TEST_GUARD_HOSTAGE_OS)
								
								PLAY_AUDIO(PROLOGUE_TEST_COLLECT_CASH)
								
								START_CUTSCENE()
								
								WAIT_WITH_DEATH_CHECKS(0)
								
								IF NOT IS_REPEAT_PLAY_ACTIVE()
									SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
								ENDIF
								
								CLEAR_AREA(GET_ENTITY_COORDS(playerPedID), 10.0, TRUE)
								
								//PROP_CASH_TROLLY - <<5302.142, -5191.521, 82.992>>, 75.000
								//PROP_CASH_TROLLY - <<5308.040, -5191.028, 82.992>>, 20.000
								
								IF bSwapTrolley = FALSE
									CREATE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
									CREATE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
									
									bSwapTrolley = TRUE
								ENDIF
								
								SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 0, 0)
								
								bRadar = FALSE
																
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
								ADVANCE_CUTSCENE()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
//			CASE 2
//				IF CAN_SET_EXIT_STATE_FOR_CAMERA()	//GET_CUTSCENE_TIME() > (13.849023 * 1000.0)
//				OR WAS_CUTSCENE_SKIPPED()
////					SET_CAM_PARAMS(camMain, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
////					SET_CAM_PARAMS(camMain, <<5307.293945, -5189.190430, 84.549606>>, <<-23.342194, 0.0, 89.400475>>, 50.0, DEFAULT_INTERP_TO_FROM_GAME)	//<<5308.738770, -5189.824707, 85.107216>>, <<-18.344702, 0.0, 74.455780>>, 50.0, DEFAULT_INTERP_TO_FROM_GAME)
////					
////					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(89.4 - GET_ENTITY_HEADING(playerPedID))
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.344316)
//					
//					ADVANCE_CUTSCENE()
//				ENDIF
//			BREAK
			CASE 2
//				IF GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
//					INT iTolerance
//					
//					iTolerance = 8
//					
//					INT iLTSx, iLTSy, iRTSx, iRTSy
//					
//					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
//					
//					IF (iLTSx < -iTolerance OR iLTSx > iTolerance)
//					OR (iLTSy < -iTolerance OR iLTSy > iTolerance)
//						CLEAR_PED_TASKS(playerPedID)
//					ENDIF
//				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					//Player
//					IF WAS_CUTSCENE_SKIPPED()
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-24.348312)
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.344316)
//						
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//						
//						SET_PED_POSITION(playerPedID, <<5306.0747, -5189.8545, 82.5187>>, 86.4050)
//						
//						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 750, 86.4050, FALSE)
//						
//						//TASK_GO_STRAIGHT_TO_COORD(playerPedID, <<5303.3008, -5189.0464, 82.5187>>, PEDMOVEBLENDRATIO_WALK)
//					ELSE
//						RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
//					ENDIF
					
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(89.4 - GET_ENTITY_HEADING(playerPedID))
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.344316)
					
					SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
					SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, TRUE)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
//		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
//			SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
//		ENDIF
		
		IF IS_CUTSCENE_PLAYING()
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Leave_Preload)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRELOAD_CONVERSATION(sPedsForConversation, sConversationBlock, "PRO_Leave", CONV_PRIORITY_LOW)
					
					SET_LABEL_AS_TRIGGERED(PRO_Leave_Preload, TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(GameCamCash)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(89.4 - GET_ENTITY_HEADING(playerPedID))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.344316)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelBag)
				IF GET_CUTSCENE_TIME() > ROUND(7.411327 * 1000.0)
					SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 12, 0)
					
					SET_LABEL_AS_TRIGGERED(MichaelBag, TRUE)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				//SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 750)
				
				//TASK_GO_STRAIGHT_TO_COORD(playerPedID, <<5305.8823, -5189.5723, 82.5186>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 81.6185)
				
//				IF NOT WAS_CUTSCENE_SKIPPED()
//					FORCE_PED_MOTION_STATE(playerPedID, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
//				ENDIF
				
				SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 12, 0)
				
				SET_LABEL_AS_TRIGGERED(GameCamCash, TRUE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5299.9536, -5188.4478, 82.5183>>, 242.6358)
				
				//Bag
				IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM])
					objBag[BRAD_BAG_ANIM] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<5293.259766, -5192.220215, 82.629997>>)
					ATTACH_ENTITY_TO_ENTITY(objBag[BRAD_BAG_ANIM], sSelectorPeds.pedID[SELECTOR_PED_TREVOR], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
				ENDIF
				
				sceneLoopVault = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneLoopVault, sAnimDictPrologue_LeaveVault, "mcs_2_idle_player2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], playerPedID, -1)
				
				PLAY_ENTITY_ANIM(objBag[BRAD_BAG_ANIM], "mcs_2_idle_bag", sAnimDictPrologue_LeaveVault, INSTANT_BLEND_IN, TRUE, FALSE)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneLoopVault, TRUE)
				
				SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 1, 0)
				
				SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, TRUE)
			ENDIF
			
			//Weapon
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_weapon")
				GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_MICHAEL], playerPedMichael)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
				GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_TREVOR], playerPedTrevor)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelCash)
				IF GET_CUTSCENE_TIME() > ROUND(5.400000 * 1000.0)
					iCurrentTake += 93000
					
					g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake	#IF IS_DEBUG_BUILD	PRINTLN("g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
					
					SET_LABEL_AS_TRIGGERED(MichaelCash, TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(TrevorCash)
				IF GET_CUTSCENE_TIME() > ROUND(7.600000 * 1000.0)
					iCurrentTake += 84000
					
					g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake	#IF IS_DEBUG_BUILD	PRINTLN("g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
					
					SET_LABEL_AS_TRIGGERED(TrevorCash, TRUE)
				ENDIF
			ENDIF
			
			IF bVideoRecording
				IF GET_CUTSCENE_TIME() > ROUND(5.100000 * 1000.0)
					REPLAY_STOP_EVENT()
					
					bVideoRecording = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
	
		REPLAY_STOP_EVENT()
	
		//Cleanup (Blips, peds, variables etc.)
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 12, 0)
		
		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(playerPedID, FALSE)
		
		SET_OBJECT_AS_NO_LONGER_NEEDED(objCash)
		
		//Particles
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmoke)
			STOP_PARTICLE_FX_LOOPED(ptfxSmoke)
		ENDIF
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFog)
			STOP_PARTICLE_FX_LOOPED(ptfxFog)
		ENDIF
		
		IF sIDBargeDoor != -1
			STOP_SOUND(sIDBargeDoor)
			RELEASE_SOUND_ID(sIDBargeDoor)
			sIDBargeDoor = -1
		ENDIF
		
		//Cover
		INT i
		
		REPEAT COUNT_OF(covPoint) i
			REMOVE_COVER_POINT(covPoint[i])
		ENDREPEAT
		
		SAFE_REMOVE_BLIP(blipCash)
		SAFE_REMOVE_BLIP(blipDestination)
		
		SAFE_DELETE_OBJECT(objCash)
		
		SAFE_DELETE_OBJECT(objDoor)
		
		SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL2, 12, 0)
		SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PED_COMP_SPECIAL2, 1, 0)
		
		ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S2")
		
		SET_PED_CAN_PEEK_IN_COVER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
		
		REPEAT COUNT_OF(objWeapon) i
			SAFE_DELETE_OBJECT(objWeapon[i])
		ENDREPEAT
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC LeaveVault()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Radar
		bRadar = TRUE
		
		//Blips
		
		//Assisted
		ASSISTED_MOVEMENT_REQUEST_ROUTE("Pro_S1a")
		
		//Guard
		IF NOT DOES_ENTITY_EXIST(pedGuard)
			SPAWN_PED(pedGuard, CSB_PROLSEC, <<5297.8311, -5186.6440, 85.7190 - 3.2>>, 91.9750)
			SET_PED_COMPONENT_VARIATION(pedGuard, PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedGuard, PED_COMP_TORSO, 1, 0)
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedGuard, "PROGUARD")
			SET_MODEL_AS_NO_LONGER_NEEDED(CSB_PROLSEC)
			GIVE_WEAPON_TO_PED(pedGuard, wtPistol, INFINITE_AMMO, TRUE)
			SET_PED_DROPS_WEAPONS_WHEN_DEAD(pedGuard, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuard, TRUE)
			SET_PED_AS_ENEMY(pedGuard, TRUE)
			SET_PED_DIES_WHEN_INJURED(pedGuard, TRUE)
			STOP_PED_WEAPON_FIRING_WHEN_DROPPED(pedGuard)
		ENDIF
		
		SET_ENTITY_VISIBLE(pedGuard, FALSE)
		
		IF bSwapTrolley = FALSE
			CREATE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
			CREATE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
			
			bSwapTrolley = TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		REQUEST_CUTSCENE("pro_mcs_3_pt1")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_TO_VAULT")
			START_AUDIO_SCENE("PROLOGUE_GET_TO_VAULT")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			SET_PED_POSITION(playerPedID, <<5303.3008, -5189.0464, 82.5187>>, 98.0764)
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5299.9536, -5188.4478, 82.5183>>, 242.6358) //<<5300.0117, -5188.3506, 82.5183>>, 92.7196)
			
			//Bag
			IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM])
				objBag[BRAD_BAG_ANIM] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<5293.2598, -5192.2202, 82.6300>>)
				ATTACH_ENTITY_TO_ENTITY(objBag[BRAD_BAG_ANIM], sSelectorPeds.pedID[SELECTOR_PED_TREVOR], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneLoopVault)
				sceneLoopVault = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneLoopVault, sAnimDictPrologue_LeaveVault, "mcs_2_idle_player2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], playerPedID, -1)
				
				PLAY_ENTITY_ANIM(objBag[BRAD_BAG_ANIM], "mcs_2_idle_bag", sAnimDictPrologue_LeaveVault, INSTANT_BLEND_IN, TRUE, FALSE)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneLoopVault, TRUE)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
//		IF TIMERA() < 500
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL_STATS)
//		ENDIF
		
		//Request Cutscene Variations - pro_mcs_3_pt1
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_SecurityGuard_Grabs_mike", pedGuard)
		ENDIF
		
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
		AND IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(89.4 - GET_ENTITY_HEADING(playerPedID))
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.344316)
		ENDIF
		
//		CREATE_CONVERSATION_ADV(PRO_Leave, "PRO_Leave")
//		
//		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5300.448730, -5193.559570, 82.518639>>, <<5300.519531, -5185.229004, 85.537689>>, 5.0) //IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5298.845703, -5191.692383, 85.718445 - 3.2>>, <<5298.884766, -5186.600586, 88.718506 - 3.2>>, 5.5, FALSE, TRUE)
//			IF SAFE_START_CUTSCENE(10.0, FALSE)
//				TASK_GO_STRAIGHT_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5298.3125, -5189.1655, 82.5182>>, 1.2 /*PEDMOVE_WALK*/, DEFAULT_TIME_BEFORE_WARP, 139.1003)
//				
//				TASK_GO_STRAIGHT_TO_COORD(playerPedID, <<5297.40, -5189.51, 82.52>>, 0.5 /*PEDMOVE_WALK*/)	//TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(playerPedID, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<0.0, -1.0, 0.0>>, PEDMOVE_WALK)
//				
//				WAIT_WITH_DEATH_CHECKS(1500)
//				
//				ADVANCE_STAGE()
//			ENDIF
//		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Leave)
					IF HAS_LABEL_BEEN_TRIGGERED(PRO_Leave_Preload)
					AND GET_IS_PRELOADED_CONVERSATION_READY()
						BEGIN_PRELOADED_CONVERSATION()
						
						SET_LABEL_AS_TRIGGERED(PRO_Leave, TRUE)
					ELSE
						CREATE_CONVERSATION_ADV(PRO_Leave, "PRO_Leave")
					ENDIF
					
					iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 7500)
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5298.023926, -5189.134766, 82.518318>>, <<5302.975586, -5189.083008, 85.518600>>, 5.5) //IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5298.845703, -5191.692383, 85.718445 - 3.2>>, <<5298.884766, -5186.600586, 88.718506 - 3.2>>, 5.5, FALSE, TRUE)
					
					REPLAY_RECORD_BACK_FOR_TIME(1.0, 4.0)
					
					//TASK_GO_STRAIGHT_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5295.6973, -5189.9873, 82.5186>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP,  167.1197)
					
					//Bag
					IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM])
						objBag[BRAD_BAG_ANIM] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<5293.259766, -5192.220215, 82.629997>>)
						ATTACH_ENTITY_TO_ENTITY(objBag[BRAD_BAG_ANIM], sSelectorPeds.pedID[SELECTOR_PED_TREVOR], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
					ENDIF
					
					sceneLeaveVault = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sceneLeaveVault, sAnimDictPrologue_LeaveVault, "mcs_2_walkout_player2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
					TASK_CLEAR_LOOK_AT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					
					PLAY_ENTITY_ANIM(objBag[BRAD_BAG_ANIM], "mcs_2_walkout_bag", sAnimDictPrologue_LeaveVault, NORMAL_BLEND_IN, FALSE, TRUE)
					
					TASK_GO_STRAIGHT_TO_COORD(playerPedID, <<5298.3516, -5188.9092, 82.5182>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP)
					
					ADVANCE_CUTSCENE()
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF GET_GAME_TIMER() > iDialogueTimer
						IF iDialogueLineCount[iDialogueStage] = -1 
							iDialogueLineCount[iDialogueStage] = 4
						ENDIF
						
						iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000, 5000)
						
						iDialogueLineCount[iDialogueStage]--
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CREATE_CONVERSATION_ADV(PRO_MCS3LI, "PRO_MCS3LI")
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM) //[MF] Fix for B* 2040862
				//IF NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5299.9536, -5188.4478, 82.5183 - 0.5>>, <<0.5, 0.5, 3.0>>)
				//IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5299.907715, -5188.944336, 82.518318>>, <<5295.755371, -5188.944824, 85.518631>>, 4.0)
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveVault)
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveVault)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveVault) > 0.75)
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM])
		AND NOT IS_ENTITY_DEAD(objBag[BRAD_BAG_ANIM])
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), objBag[BRAD_BAG_ANIM])
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), objBag[BRAD_BAG_ANIM])
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			ENDIF
		ENDIF
		
		SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		SAFE_REMOVE_BLIP(blipCash)
		
		TASK_CLEAR_LOOK_AT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		
		ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S1a")
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GET_TO_VAULT")
			STOP_AUDIO_SCENE("PROLOGUE_GET_TO_VAULT")
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

BOOL bCinematicSwitch

PROC cutsceneGuard()
	IF INIT_STAGE()
		//Replay 
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayCutsceneGuard), "cutGuard")
		
		SET_SELECTOR_PED_PRIORITY(sSelectorPeds, NUMBER_OF_SELECTOR_PEDS, NUMBER_OF_SELECTOR_PEDS, NUMBER_OF_SELECTOR_PEDS)
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Guard Position
		IF NOT DOES_ENTITY_EXIST(pedGuard)
			SPAWN_PED(pedGuard, CSB_PROLSEC, <<5297.8311, -5186.6440, 85.7190 - 3.2>>, 91.9750)
			SET_PED_COMPONENT_VARIATION(pedGuard, PED_COMP_HEAD, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedGuard, PED_COMP_TORSO, 1, 0)
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedGuard, "PROGUARD")
			SET_MODEL_AS_NO_LONGER_NEEDED(CSB_PROLSEC)
			GIVE_WEAPON_TO_PED(pedGuard, wtPistol, INFINITE_AMMO, TRUE)
			SET_PED_DROPS_WEAPONS_WHEN_DEAD(pedGuard, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuard, TRUE)
			SET_PED_AS_ENEMY(pedGuard, TRUE)
			SET_PED_DIES_WHEN_INJURED(pedGuard, TRUE)
			STOP_PED_WEAPON_FIRING_WHEN_DROPPED(pedGuard)
		ENDIF
		
		SET_ENTITY_VISIBLE(pedGuard, TRUE)
		
		//Assisted
//		ASSISTED_MOVEMENT_REQUEST_ROUTE("Pro_S3")
		
		//Score
		PLAY_AUDIO(PROLOGUE_TEST_GUARD_HOSTAGE)
		
		//Radar
		bRadar = FALSE
		
		//Text
		//CLEAR_TEXT()
		CLEAR_PRINTS()
		SAFE_CLEAR_HELP()
		
		//Variable
		bCinematicSwitch = FALSE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		IF bReplaySkip = FALSE
			REQUEST_CUTSCENE("pro_mcs_3_pt1")
		ENDIF
		
//		//Textures
//		REQUEST_STREAMED_TEXTURE_DICT("SplashScreens") //MichaelTSplash
		
		SET_PED_PRELOAD_VARIATION_DATA(playerPedMichael, PED_COMP_HAIR, 5, 0)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Player Position
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			SET_PED_POSITION(playerPedID, <<5299.9331, -5187.6924, 85.7184 - 3.2>>, 165.5154)
			
			//Trevor Position
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5297.2866, -5189.6323, 85.7187 - 3.2>>, 113.4281)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			PLAY_AUDIO(PROLOGUE_TEST_KILL_ONESHOT)
			
			//Rayfire
			rfVaultExplosion = GET_RAYFIRE_MAP_OBJECT(<<5298.8892, -5189.0869, 82.5182>>, 10.0, "DES_VaultDoor001")
			
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfVaultExplosion)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion) != RFMO_STATE_END
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfVaultExplosion, RFMO_STATE_ENDING)
				ENDIF
			ENDIF
			
			//Balaclava
			IF NOT DOES_ENTITY_EXIST(objBalaclava[MICHAEL_BALACLAVA])
				objBalaclava[MICHAEL_BALACLAVA] = CREATE_OBJECT(PROP_MICHAEL_BALACLAVA, <<5296.55273, -5187.38867, 82.51867>>)
			ENDIF
			
			//Trolley
			IF bSwapTrolley = FALSE
				CREATE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
				CREATE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
				
				bSwapTrolley = TRUE
			ENDIF
			
			IF bReplaySkip = TRUE
				//Balaclava
				SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HAIR, 5, 0)
				SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL, 0, 0)
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
				
				//Bag
				SAFE_DELETE_OBJECT(objBag[BRAD_BAG_ANIM])
				
				IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_STATIC])
					objBag[BRAD_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<5293.2598, -5192.2202, 82.6300>>)
					SET_ENTITY_COORDS(objBag[BRAD_BAG_STATIC], <<5293.259766, -5192.220215, 82.629997>>)
					SET_ENTITY_ROTATION(objBag[BRAD_BAG_STATIC], <<77.7600, -1.0800, -130.6800>>)
					FREEZE_ENTITY_POSITION(objBag[BRAD_BAG_STATIC], TRUE)
					RETAIN_ENTITY_IN_INTERIOR(objBag[BRAD_BAG_STATIC], intDepot)
				ENDIF
				
				//Object
				objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedMichael)
				
				IF NOT DOES_ENTITY_EXIST(objWeapon[WEAPON_MICHAEL])
					objGun = CREATE_OBJECT_NO_OFFSET(GET_WEAPONTYPE_MODEL(WEAPONTYPE_CARBINERIFLE), <<5295.052, -5188.801, 82.562>>)
				ELSE
					objGun = objWeapon[WEAPON_MICHAEL]
					SET_ENTITY_COORDS_NO_OFFSET(objGun, <<5295.052, -5188.801, 82.562>>)
				ENDIF
				SET_ENTITY_ROTATION(objGun, <<-85.792, -1.133, -92.236>>)
				FREEZE_ENTITY_POSITION(objGun, TRUE)
				
				//Michael
				REMOVE_ALL_PED_WEAPONS(playerPedID)
				
				TASK_PLAY_ANIM_ADVANCED(playerPedID, sAnimDictPrologue4_Base, "hold_head_loop_base_player0", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)		
				
				//Guard
				TASK_PLAY_ANIM_ADVANCED(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				
				SET_CURRENT_PED_WEAPON(pedGuard, wtPistol, TRUE)
				
				//Trevor
				SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5294.4834, -5191.2212, 82.5187>>, 329.4068)
				
				SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, TRUE)
				
				OPEN_SEQUENCE_TASK(seqMain)
					TASK_FORCE_MOTION_STATE(NULL, ENUM_TO_INT(MS_AIMING))
					TASK_AIM_GUN_AT_COORD(NULL, <<5296.375000, -5187.973145, 83.838951 + 0.1>>, -1, TRUE)
				CLOSE_SEQUENCE_TASK(seqMain)
				TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
				CLEAR_SEQUENCE_TASK(seqMain)
				
				//Player
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				//Radar
				bRadar = TRUE
				
				//Text
				CLEAR_TEXT()
				
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				UPDATE_PED_REFERENCES()
				
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedID, "TREVOR")
				
				TASK_AIM_GUN_AT_ENTITY(playerPedTrevor, pedGuard, -1, TRUE)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedTrevor, TRUE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				IF DOES_CAM_EXIST(camCinematic)
					SET_CAM_ACTIVE(camCinematic, FALSE)
					DESTROY_CAM(camCinematic)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					//AI and Behaviour
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 50)
					
					//Health
					SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
					SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 400)
					
					//Weapons
					REMOVE_ALL_PED_WEAPONS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 0)
					
					//Anim
					FLOAT fAnimCurrentTime
					
					IF IS_ENTITY_PLAYING_ANIM(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard")
						fAnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard")
					ENDIF
					
					TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Base, "hold_head_loop_base_player0", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_TURN_OFF_COLLISION | AF_IGNORE_GRAVITY | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, fAnimCurrentTime)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					
					IF IS_ENTITY_PLAYING_ANIM(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard")
						SET_ENTITY_ANIM_CURRENT_TIME(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard", fAnimCurrentTime)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuard)
					ENDIF
					
					//Blips
					SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
					SAFE_REMOVE_BLIP(blipTrevor)
					
					//Misc
					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
					SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_SPECIAL2, 12, 0)
				ENDIF
				
				//Variables
				iCutsceneStage = 5
				
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
				
				SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
			ENDIF
			
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(1000)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - pro_mcs_3_pt1
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_SecurityGuard_Grabs_mike", pedGuard)
		ENDIF
		
		IF iCutsceneStage > 0
		AND bReplaySkip = FALSE
		AND NOT IS_CUTSCENE_PLAYING()
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
		
		IF iCutsceneStage > 0
		AND iCutsceneStage <= 4
			IF IS_CUTSCENE_PLAYING()
				IF GET_CUTSCENE_TIME() > ROUND(8.000000 * 1000.0)
					IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_STATIC])
						objBag[BRAD_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<5293.2598, -5192.2202, 82.6300>>)
						SET_ENTITY_COORDS(objBag[BRAD_BAG_STATIC], <<5293.259766, -5192.220215, 82.629997>>)
						SET_ENTITY_ROTATION(objBag[BRAD_BAG_STATIC], <<77.7600, -1.0800, -130.6800>>)
						FREEZE_ENTITY_POSITION(objBag[BRAD_BAG_STATIC], TRUE)
						RETAIN_ENTITY_IN_INTERIOR(objBag[BRAD_BAG_STATIC], intDepot)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
				//Cam
				STOP_CAM_SHAKING(camMain, TRUE)
				
				SET_CAM_PARAMS(camMain,
								<<5297.019531, -5186.303711, 84.227119>>,
								<<-7.860384, -0.000164, 165.093689>>,
								24.0,							
								0,
								GRAPH_TYPE_LINEAR,
								GRAPH_TYPE_LINEAR)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(camMain, 4.0)
				SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 1.0)
				SET_CAM_DOF_FNUMBER_OF_LENS(camMain, 2.0)
				SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(camMain, 0.0)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(155.0 - GET_ENTITY_HEADING(playerPedID))	//GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(playerPedMichael), GET_ENTITY_COORDS(playerPedTrevor))
				
				IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_STATIC])
					objBag[BRAD_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<5293.2598, -5192.2202, 82.6300>>)
					SET_ENTITY_COORDS(objBag[BRAD_BAG_STATIC], <<5293.259766, -5192.220215, 82.629997>>)
					SET_ENTITY_ROTATION(objBag[BRAD_BAG_STATIC], <<77.7600, -1.0800, -130.6800>>)
					FREEZE_ENTITY_POSITION(objBag[BRAD_BAG_STATIC], TRUE)
					RETAIN_ENTITY_IN_INTERIOR(objBag[BRAD_BAG_STATIC], intDepot)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				//Clear Help
				CLEAR_TEXT()
				
				//Balaclava
				SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HAIR, 5, 0)
				SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL, 0, 0)
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
				
				//Michael
				REMOVE_ALL_PED_WEAPONS(playerPedID)
				
				TASK_PLAY_ANIM_ADVANCED(playerPedID, sAnimDictPrologue4_Base, "hold_head_loop_base_player0", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_SecurityGuard_Grabs_mike")
				//Guard
				TASK_PLAY_ANIM_ADVANCED(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuard)
				
				SET_CURRENT_PED_WEAPON(pedGuard, wtPistol, TRUE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				//Trevor
				SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<5294.4834, -5191.2212, 82.5187>>, 329.4068)
				
				SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], wtCarbineRifle, TRUE)
				
				OPEN_SEQUENCE_TASK(seqMain)
					TASK_FORCE_MOTION_STATE(NULL, ENUM_TO_INT(MS_AIMING))
					TASK_AIM_GUN_AT_COORD(NULL, <<5296.375000, -5187.973145, 83.838951 + 0.1>>, -1, TRUE)
				CLOSE_SEQUENCE_TASK(seqMain)
				TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqMain)
				CLEAR_SEQUENCE_TASK(seqMain)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			ENDIF
			
			//Weapon
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_weapon")
//				SAFE_DELETE_OBJECT(objWeapon[WEAPON_MICHAEL])
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
				GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_TREVOR], playerPedTrevor)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Security_guard_pistol")
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				AND (HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(playerPedMichael)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveVault)
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveVault)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveVault) > 0.95))
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedGuard, "PRO_SecurityGuard_Grabs_mike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedMichael)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_MICHAEL], "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(playerPedTrevor)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
					ENDIF
					
					//Balaclava
					IF NOT DOES_ENTITY_EXIST(objBalaclava[MICHAEL_BALACLAVA])
						objBalaclava[MICHAEL_BALACLAVA] = CREATE_OBJECT(PROP_MICHAEL_BALACLAVA, <<5296.55273, -5187.38867, 82.51867>>)
					ENDIF
					
					//Trolley
					IF bSwapTrolley = FALSE
						CREATE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
						CREATE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
						
						bSwapTrolley = TRUE
					ENDIF
					
					//Bag
					SAFE_DELETE_OBJECT(objBag[BRAD_BAG_ANIM])
					
					//Clear Area
					CLEAR_AREA(<<5298.8394, -5189.1509, 85.7183 - 3.2>>, 10.0, TRUE)
					
					//Balaclava
//					SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HAIR, 5, 0)
//					SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL, 0, 0)
					
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF WAS_CUTSCENE_SKIPPED()
					PLAY_AUDIO(PROLOGUE_TEST_KILL_ONESHOT)
					
					iCutsceneStage = 4
				ENDIF
				
				//Sound
//				REQUEST_SCRIPT_AUDIO_BANK("CHAR_INTRO_MICHAEL_01")
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_GUARD_HOSTAGE_OS)
					IF GET_CUTSCENE_TIME() > ROUND(2.000000 * 1000.0)
						PLAY_AUDIO(PROLOGUE_TEST_GUARD_HOSTAGE_OS)
						
						SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_GUARD_HOSTAGE_OS, TRUE)
					ENDIF
				ENDIF
				
				IF GET_CUTSCENE_TIME() > ROUND(2.234332 * 1000.0)
				AND NOT WAS_CUTSCENE_SKIPPED()
				OR NOT IS_CUTSCENE_PLAYING()
					//Balaclava
					SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_HAIR, 5, 0)
					//SET_PED_COMPONENT_VARIATION(playerPedID, PED_COMP_SPECIAL, 0, 0)
					
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				IF NOT WAS_CUTSCENE_SKIPPED()
					ADVANCE_CUTSCENE()
				ELSE
					PLAY_AUDIO(PROLOGUE_TEST_KILL_ONESHOT)
					
					iCutsceneStage = 4
				ENDIF
			BREAK
			CASE 3
				IF WAS_CUTSCENE_SKIPPED()
					PLAY_AUDIO(PROLOGUE_TEST_KILL_ONESHOT)
					
					iCutsceneStage = 4
				ENDIF
				
				IF GET_CUTSCENE_TIME() > ROUND(11.621325 * 1000.0)
				AND NOT WAS_CUTSCENE_SKIPPED()
				OR NOT IS_CUTSCENE_PLAYING()
					//CLEAR_TEXT()
					//
					//PRINT_HELP_ADV(PROHLP_SWITCH1, "PROHLP_SWITCH1")
					
					//Camera coordinates to match cutscene
					//SET_CAM_PARAMS(_cams_index,<<5292.046875,-5186.548340,83.562843>>,<<-1.693416,0.000000,-125.738396>>,40.000000)
					//SET_CAM_PARAMS(_cams_index,<<5292.147461,-5186.621094,83.559174>>,<<-1.693388,-0.000014,-125.738380>>,40.000000)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 4
				IF HAS_CUTSCENE_FINISHED()
					IF NOT DOES_ENTITY_EXIST(objLightingRig[LIGHTING_RIG_GUARD])
						objLightingRig[LIGHTING_RIG_GUARD] = CREATE_OBJECT(PROP_2ND_HOSTAGE_SCENE, <<5297.717, -5188.909, 80.549 + 1.0>>)
						FORCE_ROOM_FOR_ENTITY(objLightingRig[LIGHTING_RIG_GUARD], intDepot, HASH("V_CashD_side"))
					ENDIF
					
					REPLAY_STOP_EVENT()					
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(PRO_MICHAEL_SAVE_TIME)
					
					//Player
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					//Radar
					bRadar = TRUE
					
					//Text
					CLEAR_TEXT()
					
					//Object
					IF NOT DOES_ENTITY_EXIST(objWeapon[WEAPON_MICHAEL])
						objGun = CREATE_OBJECT_NO_OFFSET(GET_WEAPONTYPE_MODEL(WEAPONTYPE_CARBINERIFLE), <<5295.052, -5188.801, 82.562>>)
					ELSE
						objGun = objWeapon[WEAPON_MICHAEL]
						SET_ENTITY_COORDS_NO_OFFSET(objGun, <<5295.052, -5188.801, 82.562>>)
					ENDIF
					SET_ENTITY_ROTATION(objGun, <<-85.792, -1.133, -92.236>>)
					FREEZE_ENTITY_POSITION(objGun, TRUE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)	//B* 2170077: Don't re-enable blinders before switching to Trevor
					
					SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_CashD_vault")
					
					SET_CAM_NEAR_CLIP(camMain, 0.5)
					
					//Switch Selector
					ENABLE_SELECTOR()
					
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 5
				//Audio Scene
				IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_SWITCH_TO_TREVOR")
					START_AUDIO_SCENE("PROLOGUE_SWITCH_TO_TREVOR")
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF NOT HAS_LABEL_BEEN_TRIGGERED(FirstSwitchMade)
						IF IS_SELECTOR_UI_BUTTON_PRESSED()
							IF TIMERA() > 500
								IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_SWITCH4)
									IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_SWITCH4")
										SAFE_CLEAR_HELP()
										PRINTLN("PROHLP_SWITCH4 printing")
										PRINT_HELP_FOREVER("PROHLP_SWITCH4")	//PRINT_HELP_ADV_POS("PROHLP_SWITCH3", <<0.51, 0.3, 0.0>>, NULL, TRUE, HELP_TEXT_SOUTH, 5500)	//<<0.184, 0.6175, 0.0>>
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_SWITCH2")	//IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED("PROHLP_SWITCH2")
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_SWITCH2")
									SAFE_CLEAR_HELP()
									PRINTLN("PROHLP_SWITCH2 printing")
									PRINT_HELP_FOREVER("PROHLP_SWITCH2")	//PRINT_HELP_ADV_POS("PROHLP_SWITCH2", VECTOR_ZERO, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE, HELP_TEXT_SOUTH, -1)
								ENDIF
							ENDIF
							
							SETTIMERA(0)
						ENDIF
					ENDIF
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(GuardShootMichael)
					IF NOT IS_PED_INJURED(pedGuard)
						IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF NOT IS_PED_INJURED(pedGuard)
								CLEAR_PED_TASKS(pedGuard)
								
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue4_Fail, "KILL_Michael_Guard", sceneGuardAmbushPos, sceneGuardAmbushRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.133, EULER_YXZ, AIK_DISABLE_LEG_IK)
									TASK_COMBAT_PED(NULL, sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedGuard, seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(pedGuard, relGroupEnemy)
								
								SET_PED_KEEP_TASK(pedGuard, TRUE)
							ENDIF
						ENDIF
						
						SET_PED_SHOOTS_AT_COORD(pedGuard, GET_PED_BONE_COORDS(playerPedMichael, BONETAG_HEAD, VECTOR_ZERO))
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(<<5296.7, -5188.0, 84.1>>, GET_PED_BONE_COORDS(playerPedMichael, BONETAG_HEAD, VECTOR_ZERO), 1000, TRUE, WEAPONTYPE_PISTOL)
						
						SET_ENTITY_HEALTH(playerPedMichael, 0)
						
						IF NOT IS_PED_INJURED(pedGuard)
							CLEAR_PED_TASKS(pedGuard)
							
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue4_Fail, "KILL_Michael_Guard", sceneGuardAmbushPos, sceneGuardAmbushRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.133, EULER_YXZ, AIK_DISABLE_LEG_IK)
								TASK_COMBAT_PED(NULL, playerPedID)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedGuard, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							SET_PED_RELATIONSHIP_GROUP_HASH(pedGuard, relGroupEnemy)
						ENDIF
					ENDIF
				ENDIF
				
				IF (IS_PED_SHOOTING(playerPedID)
				AND NOT IS_PED_INJURED(pedGuard))
				OR (HAS_LABEL_BEEN_TRIGGERED(MichaelWillDie)
				AND GET_GAME_TIMER() > iDialogueTimer)
					IF NOT IS_PED_INJURED(playerPedMichael)
						IF GET_ENTITY_HEALTH(playerPedMichael) > 101
							SET_PED_ACCURACY(pedGuard, 100)
							SET_PED_COMBAT_ATTRIBUTES(pedGuard, CA_PERFECT_ACCURACY, TRUE)
							
							//WAIT_WITH_DEATH_CHECKS(0)	//Double shot, for good measure
							
							SET_LABEL_AS_TRIGGERED(GuardShootMichael, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF NOT DOES_CAM_EXIST(sCamDetails.camID)
						IF NOT IS_PED_INJURED(pedGuard)
							IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIMa)
							AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIMb)
							AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIMc)
								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayAimAtGuard), "cutGuard(AimAtGuard)")
								
								SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
								
								//TASK_AIM_GUN_AT_ENTITY(playerPedID, pedGuard, 2000, TRUE)	//CLEAR_PED_TASKS(playerPedID)
								
								IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Idle4")
									txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								ENDIF
								
								SAFE_CLEAR_HELP()	//CLEAR_TEXT()
								
								KILL_FACE_TO_FACE_CONVERSATION()
								
								IF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 0
									PRINT_HELP_ADV(PROHLP_FREEAIMb, "PROHLP_FREEAIMb")
								ELIF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 1
									PRINT_HELP_ADV(PROHLP_FREEAIMc, "PROHLP_FREEAIMc")
								ELIF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 2
									PRINT_HELP_ADV(PROHLP_FREEAIMc, "PROHLP_FREEAIMc")
								ENDIF
								
								SAFE_ADD_BLIP_PED(blipGuard, pedGuard)
								SET_BLIP_PRIORITY(blipGuard, BLIPPRIORITY_HIGH_HIGHEST)
								
								//Radar
								bRadar = TRUE
								
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								
								bReplaySkip = FALSE
								
								SETTIMERB(0)
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_AIM_GUN_AT_ENTITY) = PERFORMING_TASK
							OR GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_AIM_GUN_AT_COORD) = PERFORMING_TASK
							OR GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
								INT iTolerance
								
								iTolerance = 2
								
								INT iLTSx, iLTSy, iRTSx, iRTSy
								
								GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
								
								IF (iLTSx < -iTolerance OR iLTSx > iTolerance)
								OR (iLTSy < -iTolerance OR iLTSy > iTolerance)
								OR (iRTSx < -iTolerance OR iRTSx > iTolerance)
								OR (iRTSy < -iTolerance OR iRTSy > iTolerance)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
									CLEAR_PED_TASKS(playerPedID)	#IF IS_DEBUG_BUILD	PRINTLN("STOP AIMING NOW!")	#ENDIF
								ENDIF
							ENDIF
							
							IF HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIMa)
							OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIMb)
							OR HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIMc)
								INT iTolerance
								
								iTolerance = 8
								
								INT iLTSx, iLTSy, iRTSx, iRTSy
								
								GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
								
								IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM2)
								AND (NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM))
									PRINT_HELP_ADV(PROHLP_FREEAIM2, "PROHLP_FREEAIM2", TRUE, DEFAULT_HELP_TEXT_TIME - 3000, 4000)
									SETTIMERB(0)
								ELIF (HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM2)
								AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM3))
								AND (NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								OR (TIMERB() > 2000
								AND ((iRTSx < -iTolerance OR iRTSx > iTolerance)
								OR (iRTSy < -iTolerance OR iRTSy > iTolerance))))
									PRINT_HELP_ADV(PROHLP_FREEAIM3, "PROHLP_FREEAIM3", TRUE, DEFAULT_HELP_TEXT_TIME - 5000)
									SETTIMERB(0)
								ELIF (HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM2)
								AND HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM3)
								AND NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM4))
								AND (NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM))
									PRINT_HELP_ADV(PROHLP_FREEAIM4, "PROHLP_FREEAIM4")
								ENDIF
							ENDIF
						ELSE
							//Sound
							sIDHeadShot = GET_SOUND_ID()
							PLAY_SOUND_FROM_COORD(sIDHeadShot, "Guard_Headshot", <<5296.7, -5188.0, 84.1>>, "Prologue_Sounds")
							
							//Gun
							SET_PED_DROPS_WEAPON(pedGuard)
							
							//Score
							PLAY_AUDIO(PROLOGUE_TEST_HEAD_TO_SECURITY_ROOM_MA)
							
							//Stats
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, PRO_MICHAEL_SAVE_TIME)
							
							//Player Control
							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							//Michael
							SET_PED_TO_LOAD_COVER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
							
							TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Shot, "wipe_blood_player0", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
							
							SET_RAGDOLL_BLOCKING_FLAGS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RBF_PLAYER_IMPACT)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							
							APPLY_PED_BLOOD_SPECIFIC(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ENUM_TO_INT(PDZ_HEAD), 0.424, 0.671, 310.632, 0.7, 3, 0.0, "Scripted_Ped_Splash_Back")
							
							//Trevor
							TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue4_PlayerPause, "shot_guard_player2", INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_DEFAULT, 0.0, FALSE, AIK_DISABLE_LEG_IK)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID)
							
							//Brad
							SET_PED_TO_LOAD_COVER(pedBrad, TRUE)
							
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue4_Shot, "wipe_blood_brad", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.133, EULER_YXZ, AIK_DISABLE_LEG_IK)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedBrad, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
							
							//Blip
							SAFE_REMOVE_BLIP(blipGuard)
							
							//Radar
							bRadar = TRUE
							
							//Objective
							CLEAR_TEXT()
							CREATE_CONVERSATION_ADV(PRO_Rescued, "PRO_Rescued")
							REPLAY_RECORD_BACK_FOR_TIME(1.0, 4.0)
							//Ragdoll
							IF NOT SAFE_IS_PED_DEAD(pedGuard)
								SET_PED_CAN_RAGDOLL(pedGuard, TRUE)
								CLEAR_PED_TASKS(pedGuard)
							ENDIF
							
							WAIT_WITH_DEATH_CHECKS(10)
							
							//Player Control
							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
							SET_PED_PRELOAD_VARIATION_DATA(pedBrad, PED_COMP_SPECIAL2, 1, 0)
							
							SAFE_DELETE_OBJECT(objLightingRig[LIGHTING_RIG_GUARD])
							
							ADVANCE_CUTSCENE()
						ENDIF
					ENDIF
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Idle4)
						CREATE_CONVERSATION_ADV(PRO_Idle4, "PRO_Idle4")
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_GUARD)
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								PRINT_ADV(PRO_GUARD, "PRO_GUARD", DEFAULT_GOD_TEXT_TIME - 4000)
								
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									CREATE_CONVERSATION_ADV(PRO_shoot, "PRO_shoot", CONV_PRIORITY_LOW, TRUE, DO_NOT_DISPLAY_SUBTITLES)
								ENDIF
							ELSE
								IF GET_GAME_TIMER() > iDialogueTimer
									IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelWillDie)
										iDialogueTimer = GET_GAME_TIMER() + 30000
										
										SET_LABEL_AS_TRIGGERED(MichaelWillDie, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF bCinematicSwitch = FALSE
								IF NOT IS_STRING_NULL_OR_EMPTY(txtConversationPoint)
									CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV("PRO_Idle4", txtConversationPoint)
									
									txtConversationPoint = NULL_STRING()
								ELSE
									IF GET_GAME_TIMER() > iDialogueTimer
										IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelWillDie)
											iDialogueTimer = GET_GAME_TIMER() + 20000
											
											SET_LABEL_AS_TRIGGERED(MichaelWillDie, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Disable Player Movement
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			BREAK
			CASE 6
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2182867
				
				IF DOES_ENTITY_EXIST(objGun)
					IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Shot, "wipe_blood_player0")
						IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Shot, "wipe_blood_player0") >= 0.396
							IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtCarbineRifle)
								GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtCarbineRifle, 500, TRUE)
							ENDIF
							
							SAFE_DELETE_OBJECT(objGun)
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objBag[BRAD_BAG_STATIC])
					IF IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad") >= 0.318
							//Bag
							IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM])
								objBag[BRAD_BAG_ANIM] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<5293.2598, -5192.2202, 82.6300>>)
								ATTACH_ENTITY_TO_ENTITY(objBag[BRAD_BAG_ANIM], pedBrad, GET_PED_BONE_INDEX(pedBrad, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM_STRAP])
								objBag[BRAD_BAG_ANIM_STRAP] = CREATE_OBJECT(P_CSH_STRAP_01_S, <<5293.2598, -5192.2202, 82.6300>>)
								ATTACH_ENTITY_TO_ENTITY(objBag[BRAD_BAG_ANIM_STRAP], pedBrad, GET_PED_BONE_INDEX(pedBrad, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
							ENDIF
							
							PLAY_ENTITY_ANIM(objBag[BRAD_BAG_ANIM], "wipe_blood_bag", sAnimDictPrologue4_Shot, INSTANT_BLEND_IN, FALSE, FALSE, FALSE, GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad"))
							
							PLAY_ENTITY_ANIM(objBag[BRAD_BAG_ANIM_STRAP], "wipe_blood_strap", sAnimDictPrologue4_Shot, INSTANT_BLEND_IN, FALSE, FALSE, FALSE, GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad"))
							
							SAFE_DELETE_OBJECT(objBag[BRAD_BAG_STATIC])
							

							REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.5, REPLAY_IMPORTANCE_HIGH)

						ENDIF
					ENDIF
				ELIF DOES_ENTITY_EXIST(objBag[BRAD_BAG_ANIM])
					IF IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad") >= 0.465
							IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedBrad)
								//Brad - Duffle Bag
								SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 1, 0)
								
								WAIT_WITH_DEATH_CHECKS(0)
								
								SAFE_DELETE_OBJECT(objBag[BRAD_BAG_ANIM])
								SAFE_DELETE_OBJECT(objBag[BRAD_BAG_ANIM_STRAP])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Shot, "wipe_blood_player0")
					IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Shot, "wipe_blood_player0") >= 0.9	//0.811	//0.65
						IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot) - <<0.0, 0.0, 1.0>>)
							covPoint[0] = ADD_COVER_POINT(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot) - <<0.0, 0.0, 1.0>>, -GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot)), COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
						ENDIF
						
						CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot), PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0, ENAV_NO_STOPPING)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot), 1000, FALSE, 0.9, TRUE, TRUE, covPoint[0], TRUE)
							TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
						FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], MS_ON_FOOT_IDLE, TRUE, FAUS_DEFAULT)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad")
					IF GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad") >= 0.781 //0.95
						IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot) - <<0.0, 0.0, 1.0>>)
							covPoint[1] = ADD_COVER_POINT(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot) - <<0.0, 0.0, 1.0>>, -GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot)) - 90.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
						ENDIF
						
						CLEAR_PED_TASKS(pedBrad)
						
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot), PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0, ENAV_NO_STOPPING)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot), 1000, FALSE, 0.9, TRUE, TRUE, covPoint[1], TRUE)
							TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedBrad, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
						FORCE_PED_MOTION_STATE(pedBrad, MS_ON_FOOT_RUN, TRUE, FAUS_DEFAULT)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(shot_guard_player2)
					IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue4_PlayerPause, "shot_guard_player2")
						IF GET_ENTITY_ANIM_CURRENT_TIME(playerPedID, sAnimDictPrologue4_PlayerPause, "shot_guard_player2") >= 0.65 //0.5
							INT iTolerance
							
							iTolerance = 8
							
							INT iLTSx, iLTSy, iRTSx, iRTSy
							
							GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
							
							IF (iLTSx < -iTolerance OR iLTSx > iTolerance)
							OR (iLTSy < -iTolerance OR iLTSy > iTolerance)
								SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								
								CLEAR_PED_TASKS(playerPedID)
								
								FORCE_PED_MOTION_STATE(playerPedID, MS_ON_FOOT_IDLE)
								
								SET_LABEL_AS_TRIGGERED(shot_guard_player2, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
				
				IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Shot, "wipe_blood_player0")
				AND NOT IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue4_Shot, "wipe_blood_brad")
				AND NOT IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue4_PlayerPause, "shot_guard_player2")
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF bCinematicSwitch = TRUE
			//Don't clear dialogue
			CLEAR_PRINTS()
			SAFE_CLEAR_HELP(TRUE)
			
			SET_LABEL_AS_TRIGGERED(PROHLP_SWITCH2, TRUE)
			SET_LABEL_AS_TRIGGERED(PROHLP_SWITCH3, TRUE)
			SET_LABEL_AS_TRIGGERED(PROHLP_SWITCH4, TRUE)
			
			iDialogueTimer = GET_GAME_TIMER() + 20000
			
			SET_LABEL_AS_TRIGGERED(MichaelWillDie, FALSE)
			
			IF NOT DOES_CAM_EXIST(camCinematic)
				camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ENDIF
			
			SET_CAM_NEAR_CLIP(camCinematic, 0.5)
			SET_CAM_MOTION_BLUR_STRENGTH(camCinematic, 0.5)
			
			VECTOR vStartCoord, vStartRot, vEndCoord, vEndRot, vStartTangent, vEndTangent
			FLOAT fStartFov, fEndFov
			
			vStartCoord = GET_CAM_COORD(camMain)
			vStartRot = GET_CAM_ROT(camMain)
			vEndCoord = <<5293.889648, -5193.047363, 84.128967>>
			vEndRot = <<0.0, 0.0, 329.96875>>
			vStartTangent = <<-9.213379, 0.828125, 2.509613>>
			vEndTangent = <<7.010742 - 3.0, -3.646972 - 0.75, -2.416809>>
			fStartFov = GET_CAM_FOV(camMain)
			fEndFov = 35.0
			
			SET_CAM_PARAMS(camCinematic,
							vStartCoord, 
							vStartRot,
							fStartFov)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)			
			
			//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("V_CashD_vault")
			
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_GUARD_SWITCH)
			
			FLOAT i, ii
			
			WHILE i <= 45
				SET_CAM_PARAMS(camCinematic,
				/*Pos*/		   HERMITE_CURVE(vStartCoord, vStartTangent, vEndCoord, vEndTangent, 0, 45, i),
				/*Rot*/		   HERMITE_CURVE(vStartRot, vStartTangent, vEndRot, vEndTangent, 0, 45, i),
				/*Fov*/		   GET_INTERP_POINT_FLOAT(fStartFov, fEndFov, 0, 45, i))
				
				i = i +@ (ii * 10)
				
				IF i < 2.0
					IF ii < 1.0
						ii += 0.1
					ENDIF
				ELIF i > 43.0
					IF ii > 0.2 //Never let speed fall below 0.1
						ii -= 0.1
					ENDIF
				ENDIF
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
				
				//Disable Player Movement
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UP_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LEFT_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_RIGHT_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_DOWN_ONLY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bCinematicSwitch = FALSE
		ENDIF
		
		//SWITCH
		IF NOT IS_PED_INJURED(pedGuard)
			SET_PED_RESET_FLAG(playerPedID, PRF_FORCEPEDTOSTRAFE, TRUE)
		ENDIF
		
		BOOL bPostSwitchCam_FP_Setup_Complete
		
		IF iCutsceneStage > 4
			IF NOT sCamDetails.bRun
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF UPDATE_SELECTOR_HUD(sSelectorPeds) //Returns TRUE when the player has made a selection
						IF sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
							INFORM_MISSION_STATS_OF_INCREMENT(PRO_SWITCHES)
							
							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
							sCamDetails.bRun = TRUE
							
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							
							SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
							
							SET_LABEL_AS_TRIGGERED(PROHLP_SWITCH2, TRUE)
							SET_LABEL_AS_TRIGGERED(PROHLP_SWITCH3, TRUE)
							SET_LABEL_AS_TRIGGERED(PROHLP_SWITCH4, TRUE)
							
							SET_LABEL_AS_TRIGGERED(FirstSwitchMade, TRUE)
							
							SAFE_CLEAR_HELP()
							
							iDialogueTimer = GET_GAME_TIMER() + 20000
							
							SET_LABEL_AS_TRIGGERED(MichaelWillDie, FALSE)
							
							//Score
							PLAY_AUDIO(PROLOGUE_TEST_GUARD_SWITCH)
							
							//bCinematicSwitch = TRUE
							
							//Audio Scene
							IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_SWITCH_TO_TREVOR")
								START_AUDIO_SCENE("PROLOGUE_SWITCH_TO_TREVOR")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					IF sCamDetails.bOKToSwitchPed
						IF NOT sCamDetails.bPedSwitched
							IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
								UPDATE_PED_REFERENCES()
								
								VECTOR vTrevorSwitchCamAimCoord = <<5297.4, -5186.3, 84.0>>
								TASK_AIM_GUN_AT_COORD(playerPedID, vTrevorSwitchCamAimCoord, 3000, TRUE)
								SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), TRUE)
								
								WHILE GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
									//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
									
									WAIT_WITH_DEATH_CHECKS(0)
								ENDWHILE
								
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedID, "TREVOR")
								
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
								
								RENDER_SCRIPT_CAMS(FALSE, TRUE, 500, FALSE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								
								IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									//AI and Behaviour
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
									SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 50)
									
									//Health
									SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
									SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 400)
									
									//Weapons
									REMOVE_ALL_PED_WEAPONS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 0)
									
									//Anim
									FLOAT fAnimCurrentTime
									
									IF IS_ENTITY_PLAYING_ANIM(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard")
										fAnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard")
									ENDIF
									
									TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue4_Base, "hold_head_loop_base_player0", sceneGuardAmbushPos, sceneGuardAmbushRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_TURN_OFF_COLLISION | AF_IGNORE_GRAVITY | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, fAnimCurrentTime)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									
									IF IS_ENTITY_PLAYING_ANIM(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard")
										SET_ENTITY_ANIM_CURRENT_TIME(pedGuard, sAnimDictPrologue4_Base, "hold_head_loop_base_guard", fAnimCurrentTime)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGuard)
									ENDIF
									
									//Blips
									SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
									SAFE_REMOVE_BLIP(blipTrevor)
									
									//Misc
									SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
									SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_SPECIAL2, 12, 0)
								ENDIF
								
								sCamDetails.bPedSwitched = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bPostSwitchCam_FP_Setup_Complete
						//[MF] - Fix for First Person Weapon pop - B* 2066820
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							IF NOT IS_ENTITY_DEAD(playerPedID)
								PRINTLN("[MF] Performing post Switch FP cam aiming setup....")
								CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
								FORCE_PED_MOTION_STATE(playerPedID, MS_AIMING, TRUE, FAUS_DEFAULT, TRUE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID, true)
							ENDIF
						ENDIF
						
						bPostSwitchCam_FP_Setup_Complete = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, PRO_MICHAEL_SAVE_TIME)
		
		SET_GAME_PAUSED(FALSE)
		
		CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
		
		SAFE_DELETE_OBJECT(objLightingRig[LIGHTING_RIG_GUARD])
		
		//Sound
		IF sIDHeadShot != -1
			STOP_SOUND(sIDHeadShot)
			RELEASE_SOUND_ID(sIDHeadShot)
			sIDHeadShot = -1
		ENDIF
		
		sCamDetails.bPedSwitched = FALSE
		sCamDetails.bRun = FALSE
		
		SAFE_REMOVE_BLIP(blipGuard)
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		STOP_CAM_SHAKING(camMain, TRUE)
		
		IF DOES_CAM_EXIST(camCinematic)
			SET_CAM_ACTIVE(camCinematic, FALSE)
			DESTROY_CAM(camCinematic)
		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(objWeapon) i
			SAFE_DELETE_OBJECT(objWeapon[i])
		ENDREPEAT
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			UPDATE_PED_REFERENCES()
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedID, "TREVOR")
		
		IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			//AI and Behaviour
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 50)
			
			//Health
			SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			SET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 400)
			SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			//Weapons
			REMOVE_ALL_PED_WEAPONS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			
			//Blips
			SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			SAFE_REMOVE_BLIP(blipTrevor)
			
			//Misc
			SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			//Balaclava
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_HAIR, 5, 0)
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_SPECIAL, 0, 0)
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
			
			SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PED_COMP_SPECIAL2, 12, 0)
			
			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtCarbineRifle)
				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtCarbineRifle, 500, TRUE)
			ENDIF
			
			CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			
			SET_PED_TO_LOAD_COVER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			
			IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot) - <<0.0, 0.0, 1.0>>)
				covPoint[0] = ADD_COVER_POINT(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot) - <<0.0, 0.0, 1.0>>, -GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot)), COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
			ENDIF
			
			OPEN_SEQUENCE_TASK(seqMain)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot), PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0, ENAV_NO_STOPPING)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot), 1000, FALSE, 0.9, TRUE, TRUE, covPoint[0], TRUE)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			CLOSE_SEQUENCE_TASK(seqMain)
			TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqMain)
			CLEAR_SEQUENCE_TASK(seqMain)
		ENDIF
		
		SAFE_REMOVE_BLIP(blipTrevor)
		
		SAFE_DELETE_OBJECT(objGun)
		SAFE_DELETE_OBJECT(objWeapon[WEAPON_MICHAEL])
		
		SAFE_DELETE_OBJECT(objBag[BRAD_BAG_STATIC])
		SAFE_DELETE_OBJECT(objBag[BRAD_BAG_ANIM])
		SAFE_DELETE_OBJECT(objBag[BRAD_BAG_ANIM_STRAP])
		
		IF NOT IS_PED_INJURED(pedGuard)
			CLEAR_PED_TASKS_IMMEDIATELY(pedGuard)
			SET_PED_CAN_RAGDOLL(pedGuard, TRUE)
			EXPLODE_PED_HEAD(pedGuard)
		ENDIF
		
		//Brad - Duffle Bag
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 1, 0)
		
		RELEASE_PED_PRELOAD_VARIATION_DATA(pedBrad)
		
		RELEASE_PED_PRELOAD_VARIATION_DATA(playerPedMichael)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC BlastDoors()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		//Switch Selector
		ENABLE_SELECTOR()
		
		//Cover
		SET_PED_TO_LOAD_COVER(playerPedMichael, FALSE)
		SET_PED_TO_LOAD_COVER(pedBrad, FALSE)
		
		//Pathing
		SET_PED_STEERS_AROUND_PEDS(playerPedMichael, FALSE)
		
		//Radar
		bRadar = TRUE
		
		//Assisted
//		ASSISTED_MOVEMENT_REQUEST_ROUTE("Pro_S4")
		
		//Objective
		SAFE_ADD_BLIP_LOCATION(blipDestination, <<5313.5903, -5177.6826, 82.5186>>)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_SWITCH_TO_TREVOR")
			START_AUDIO_SCENE("PROLOGUE_SWITCH_TO_TREVOR")
		ENDIF
		
		SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Rayfire
		rfSecDoorExplosion = GET_RAYFIRE_MAP_OBJECT(<<5318.2, -5185.1, 83.7>>, 10.0, "des_prologue_door")
		
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) != RFMO_STATE_START
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion, RFMO_STATE_STARTING)
			ENDIF
		ENDIF
		
		IF SKIPPED_STAGE()
			//Trevor
			SET_PED_POSITION(playerPedID, <<5306.5869, -5179.8296, 82.5187>>, 269.0729, FALSE)
			
			SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
			
			//Michael
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			
			TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot, REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			
			//Brad
			CLEAR_PED_TASKS_IMMEDIATELY(pedBrad)
			
			TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0")
		OR IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
			SET_PED_CAPSULE(playerPedMichael, 0.75)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<5294.823730, -5194.944824, 82.518829>>, <<5294.976074, -5177.934570, 85.518784>>, 4.0)
		OR IS_ENTITY_IN_ANGLED_AREA(pedBrad, <<5294.823730, -5194.944824, 82.518829>>, <<5294.976074, -5177.934570, 85.518784>>, 4.0)
			SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
			
			SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ELSE
			SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_RUN)
			
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				PRINT_HELP_ADV(PROHLP_RUN, "PROHLP_RUN_KM")
			ELSE
//				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
//					//First person run help text
//					//Running not enabled in first person in interior
//					//PRINT_HELP_ADV(PROHLP_RUN, "PROHLP_RUN_FP")
//				ELSE
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) <> CAM_VIEW_MODE_FIRST_PERSON
						PRINT_HELP_ADV(PROHLP_RUN, "PROHLP_RUN")
					ENDIF
//				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PC_VERSION()
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_RUN_KM")
				IF IS_PED_RUNNING(playerPedTrevor)
					IF GET_GAME_TIMER() > iHelpTimer
						SAFE_CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_RUN")
			IF IS_PED_RUNNING(playerPedTrevor)
				IF GET_GAME_TIMER() > iHelpTimer
					SAFE_CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_RUN_FP")
			IF IS_PED_RUNNING(playerPedTrevor)
				IF GET_GAME_TIMER() > iHelpTimer
					SAFE_CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot), <<1.0, 1.0, 2.0>>)
			iBlendInTimer[BLEND_IN_MICHAEL] = GET_GAME_TIMER() + 1500
		ENDIF
		
		IF NOT IS_ENTITY_AT_COORD(pedBrad, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot), <<1.0, 1.0, 2.0>>)
			iBlendInTimer[BLEND_IN_BRAD] = GET_GAME_TIMER() + 1500
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(BlastDoorsReady)
			IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0") //IF IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", sceneBlastDoorsPos, sceneBlastDoorsRot), <<1.0, 1.0, 2.0>>)
			AND IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad") //AND IS_ENTITY_AT_COORD(pedBrad, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", sceneBlastDoorsPos, sceneBlastDoorsRot), <<1.0, 1.0, 2.0>>)
			AND GET_GAME_TIMER() > iBlendInTimer[BLEND_IN_MICHAEL]
			AND GET_GAME_TIMER() > iBlendInTimer[BLEND_IN_BRAD]
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
					sceneBlastDoors = CREATE_SYNCHRONIZED_SCENE(sceneBlastDoorsPos, sceneBlastDoorsRot)
					
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sceneBlastDoors, sAnimDictPrologue5_Start, "set_c4_start_loop_a_player0", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION, SLOW_BLEND_IN)
					TASK_SYNCHRONIZED_SCENE(pedBrad, sceneBlastDoors, sAnimDictPrologue5_Start, "set_c4_start_loop_a_brad", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION, SLOW_BLEND_IN)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlastDoors, TRUE)
					
					SET_LABEL_AS_TRIGGERED(BlastDoorsReady, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
//		IF NOT IS_PED_INJURED(playerPedID)
//			IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor")
//				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
//					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//				ENDIF
//			ENDIF
//		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.481934, -5177.932129, 82.518646>>, <<5306.319824, -5177.905273, 85.525978>>, 12.0, FALSE, TRUE)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_COVER1)
						SAFE_REMOVE_BLIP(blipDestination)
						
						SAFE_ADD_BLIP_LOCATION(blipDestination, <<5313.5903, -5177.6826, 82.5186>>)
						
						PRINT_ADV(PRO_COVER1, "PRO_COVER1", DEFAULT_GOD_TEXT_TIME)
						
						PRINT_HELP_ADV(PROHLP_COVER1, "PROHLP_COVER1")
						
						//SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), FALSE)
						
						INT i
						
						REPEAT COUNT_OF(covPointBlastDoors) i
							REMOVE_COVER_POINT(covPointBlastDoors[i])
						ENDREPEAT
						
						IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5317.6992, -5175.9941, 82.5187>>)
							covPointBlastDoors[0] = ADD_COVER_POINT(<<5317.6992, -5175.9941, 82.5187>>, 187.3502, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180, TRUE)
						ENDIF
						
						IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5314.7065, -5175.7411, 82.5187>>)
							covPointBlastDoors[1] = ADD_COVER_POINT(<<5314.7065, -5175.7411, 82.5187>>, 175.9433, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180, TRUE)
						ENDIF
						
						IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5313.5439, -5176.6865, 82.5186>>)
							covPointBlastDoors[2] = ADD_COVER_POINT(<<5313.5439, -5176.6865, 82.5186>>, 269.647, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180, TRUE)
						ENDIF
						
						IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5313.5723, -5177.6260, 82.5186>>)
							covPointBlastDoors[3] = ADD_COVER_POINT(<<5313.5723, -5177.6260, 82.5186>>, 255.5294, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_180, TRUE)
						ENDIF
						
						itemCover = CREATE_ITEMSET(TRUE)
						
						REPEAT COUNT_OF(covPointBlastDoors) i
							IF NATIVE_TO_INT(covPointBlastDoors[i]) <> 0
								IF DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(GET_SCRIPTED_COVER_POINT_COORDS(covPointBlastDoors[i]))
									ADD_TO_ITEMSET(covPointBlastDoors[i], itemCover)
								ENDIF
							ENDIF
						ENDREPEAT
						
						SET_PED_PREFERRED_COVER_SET(playerPedID, itemCover)
						
						ADD_COVER_BLOCKING_AREA(<<5313.613281, -5179.025879, 83.768784>> - <<8.0, 5.5, 1.75>>, <<5313.613281, -5179.025879, 83.768784>> + <<8.0, 5.5, 1.75>>, TRUE, TRUE, TRUE)
						
//						ADD_COVER_BLOCKING_AREA(<<5309.272461, -5178.685059, 84.018921>> - <<4.0, 5.0, 1.5>>, <<5309.272461, -5178.685059, 84.018921>> + <<4.0, 5.0, 1.5>>, TRUE, TRUE, TRUE, TRUE)
//						ADD_COVER_BLOCKING_AREA(<<5316.555664, -5174.613281, 84.018921>> - <<4.0, 1.25, 1.5>>, <<5316.555664, -5174.613281, 84.018921>> + <<4.0, 1.25, 1.5>>, TRUE, TRUE, TRUE, TRUE)
//						ADD_COVER_BLOCKING_AREA(<<5316.279297, -5176.725586, 84.018921>> - <<1.4, 3.0, 1.5>>, <<5316.279297, -5176.725586, 84.018921>> + <<1.4, 3.0, 1.5>>, TRUE, TRUE, TRUE, TRUE)
//						ADD_COVER_BLOCKING_AREA(<<5315.143555, -5182.560059, 84.018921>> - <<5.5, 4.25, 1.5>>, <<5315.143555, -5182.560059, 84.018921>> + <<5.5, 4.25, 1.5>>, TRUE, TRUE, TRUE, TRUE)
//						ADD_COVER_BLOCKING_AREA(<<5318.115723, -5174.764648, 84.018921>> - <<2.5, 1.1, 1.5>>, <<5318.115723, -5174.764648, 84.018921>> + <<2.5, 1.1, 1.5>>, TRUE, TRUE, TRUE, TRUE)
//						ADD_COVER_BLOCKING_AREA(<<5318.751953, -5178.320313, 84.018921>> - <<2.0, 1.0, 1.5>>, <<5318.751953, -5178.320313, 84.018921>> + <<2.0, 1.0, 1.5>>, TRUE, TRUE, TRUE, TRUE)
//						ADD_COVER_BLOCKING_AREA(<<5319.438965, -5177.877441, 84.018921>> - <<1.1, 4.5, 1.5>>, <<5319.438965, -5177.877441, 84.018921>> + <<1.1, 4.5, 1.5>>, TRUE, TRUE, TRUE, TRUE)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ELSE
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV(PRO_move_hst, "PRO_move_hst")
					ENDIF
					
					SETTIMERA(0)
				ENDIF
			BREAK
			CASE 1
//				IF NOT SAFE_IS_PLAYER_CONTROL_ON()
//					IF GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
//						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					ENDIF
//				ENDIF
				
				IF NOT IS_PED_IN_COVER(playerPedID)
				AND NOT IS_PED_GOING_INTO_COVER(playerPedID)
				AND SAFE_IS_PLAYER_CONTROL_ON()
					IF DOES_CAM_EXIST(camMain)
						IF NOT IS_CAM_RENDERING(camMain)
							IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								SWITCH iDialogueStage
									CASE 0
										IF GET_GAME_TIMER() > iDialogueTimer
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
									CASE 1
										IF GET_GAME_TIMER() > iDialogueTimer
											PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Doors_1, "PRO_Doors", "PRO_Doors_1", CONV_PRIORITY_HIGH)
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
									CASE 2
										IF GET_GAME_TIMER() > iDialogueTimer
											CREATE_CONVERSATION_ADV(PRO_Set2, "PRO_Set2")
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
									CASE 3
										IF GET_GAME_TIMER() > iDialogueTimer
											PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Doors_3, "PRO_Doors", "PRO_Doors_3", CONV_PRIORITY_HIGH)
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
									CASE 4
										IF GET_GAME_TIMER() > iDialogueTimer
											CREATE_CONVERSATION_ADV(PRO_cover1alt, "PRO_cover1")
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
									CASE 5
										IF GET_GAME_TIMER() > iDialogueTimer
											CREATE_CONVERSATION_ADV(PRO_Set3, "PRO_Set3")
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
									CASE 6
										IF GET_GAME_TIMER() > iDialogueTimer
											CREATE_CONVERSATION_ADV(PRO_cover2, "PRO_cover2")
											
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 7500)
												
												iDialogueStage++
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Disable Player Control when entering cover and if in cover in the right location
				IF (IS_PED_IN_COVER(playerPedID)
				AND (IS_ENTITY_AT_COORD(playerPedID, <<5317.6992, -5175.9941, 82.5187>>, <<0.5, 0.5, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5317.6992, -5175.9941, 82.5187>>, <<0.5, 0.5, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5314.7065, -5175.7411, 82.5187>>, <<0.5, 0.5, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5313.5439, -5176.6865, 82.5186>>, <<0.5, 0.5, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5313.5723, -5177.6260, 82.5186>>, <<0.5, 0.5, 2.0>>)))
				OR IS_PED_GOING_INTO_COVER(playerPedID)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					SET_PED_RESET_FLAG(playerPedID, PRF_ForceScriptedCameraLowCoverAngleWhenEnteringCover, TRUE)
					
					//Audio Scene
					IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_TAKE_COVER")
						START_AUDIO_SCENE("PROLOGUE_TAKE_COVER")
					ENDIF
					
					IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_SWITCH_TO_TREVOR")
						STOP_AUDIO_SCENE("PROLOGUE_SWITCH_TO_TREVOR")
					ENDIF
				ELSE
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				PRINTLN("IS_PED_IN_COVER(playerPedID) = ", IS_PED_IN_COVER(playerPedID))
				PRINTLN("NOT IS_PED_GOING_INTO_COVER(playerPedID) = ", IS_PED_GOING_INTO_COVER(playerPedID))
				PRINTLN("NOT IS_PED_IN_COVER_FACING_LEFT(playerPedID) = ", IS_PED_IN_COVER_FACING_LEFT(playerPedID))
				PRINTLN("IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.476563, -5178.753418, 82.518654>>, <<5307.015137, -5178.661621, 85.518654>>, 10.0) = ", IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.476563, -5178.753418, 82.518654>>, <<5307.015137, -5178.661621, 85.518654>>, 10.0))
				PRINTLN("NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5317.843262, -5185.007813, 82.518654>>, <<5317.709961, -5177.232422, 85.518654>>, 5.25) = ", IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5317.843262, -5185.007813, 82.518654>>, <<5317.709961, -5177.232422, 85.518654>>, 5.25))
				PRINTLN("IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors) = ", IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors))
				PRINTLN("GET_GAME_TIMER(){", GET_GAME_TIMER(), "} > iBlendInTimer[BLEND_IN_BRAD]{", iBlendInTimer[BLEND_IN_BRAD], "}")
				#ENDIF
				
				IF IS_PED_IN_COVER(playerPedID, TRUE)
				AND NOT IS_PED_GOING_INTO_COVER(playerPedID)
				AND NOT IS_PED_IN_COVER_FACING_LEFT(playerPedID)
				AND (IS_ENTITY_AT_COORD(playerPedID, <<5317.6992, -5175.9941, 82.5187>>, <<1.0, 1.0, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5317.6992, -5175.9941, 82.5187>>, <<1.0, 1.0, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5314.7065, -5175.7411, 82.5187>>, <<1.0, 1.0, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5313.5439, -5176.6865, 82.5186>>, <<1.0, 1.0, 2.0>>)
				OR IS_ENTITY_AT_COORD(playerPedID, <<5313.5723, -5177.6260, 82.5186>>, <<1.0, 1.0, 2.0>>))
				AND IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.476563, -5178.753418, 82.518654>>, <<5307.015137, -5178.661621, 85.518654>>, 10.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5317.843262, -5185.007813, 82.518654>>, <<5317.709961, -5177.232422, 85.518654>>, 5.25)
				AND IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
				AND GET_GAME_TIMER() > iBlendInTimer[BLEND_IN_MICHAEL]
				AND GET_GAME_TIMER() > iBlendInTimer[BLEND_IN_BRAD]
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					SETTIMERB(0)
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_COVER1")
						CLEAR_PRINTS()
					ENDIF
					
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						SAFE_CLEAR_HELP(TRUE)
					ENDIF
					
					SAFE_REMOVE_BLIP(blipDestination)
					
					IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					OR GET_IS_USING_FPS_THIRD_PERSON_COVER()
						PRINTLN("[MF] Player is NOT in FP Cam - Playing anim: set_c4_mainaction_trevor")
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_PLAY_ANIM(NULL, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						CLOSE_SEQUENCE_TASK(seqMain)
					ELIF IS_ENTITY_AT_COORD(playerPedID, <<5317.6992, -5175.9941, 82.5187>>, <<1.0, 1.0, 2.0>>)
						PRINTLN("[MF] Player IS in FP Cam at cover position 2 - Playing anim: SET_C4_MainAction_Trevor_Fps_Pos2")
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_PLAY_ANIM(NULL, sAnimDictPrologue5_Main, "SET_C4_MainAction_Trevor_Fps_Pos2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						CLOSE_SEQUENCE_TASK(seqMain)
					ELSE
						PRINTLN("[MF] Player IS in FP Cam - Playing anim: set_c4_mainaction_trevor_fps")
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_PLAY_ANIM(NULL, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor_fps", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						CLOSE_SEQUENCE_TASK(seqMain)
					ENDIF
					
					TASK_PERFORM_SEQUENCE(playerPedID, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedID)
					
					sceneBlastDoors = CREATE_SYNCHRONIZED_SCENE(sceneBlastDoorsPos, sceneBlastDoorsRot)
					
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sceneBlastDoors, sAnimDictPrologue5_Main, "set_c4_mainaction_player0", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION)
					TASK_SYNCHRONIZED_SCENE(pedBrad, sceneBlastDoors, sAnimDictPrologue5_Main, "set_c4_mainaction_brad", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION)
					
					SET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors, 0.0)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlastDoors, FALSE)
					
					objBomb = CREATE_OBJECT(modBomb, GET_PED_BONE_COORDS(pedBrad, BONETAG_PH_L_HAND, VECTOR_ZERO))
					ATTACH_ENTITY_TO_ENTITY(objBomb, pedBrad, GET_PED_BONE_INDEX(pedBrad, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
					
					objBombGreen = CREATE_OBJECT(modBombGreen, <<5298.270020, -5187.850098, 83.870003>>)
					SET_ENTITY_ROTATION(objBombGreen, <<0.0, 0.0, -90.527321>>)
					ATTACH_ENTITY_TO_ENTITY(objBombGreen, pedBrad, GET_PED_BONE_INDEX(pedBrad, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
					SET_ENTITY_VISIBLE(objBombGreen, FALSE)
					
					IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
						IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) != RFMO_STATE_PRIMED
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion, RFMO_STATE_PRIMING)
						ENDIF
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(3.5, 0, REPLAY_IMPORTANCE_HIGH)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				//Sound
				LOAD_STREAM("PROLOGUE_BLAST_SECURITY_DOORS_MASTER")
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Doors_4, "PRO_Doors", "PRO_Doors_4", CONV_PRIORITY_HIGH)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(HintCamBomb)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) > 0.037 //0.068
							SET_GAMEPLAY_ENTITY_HINT(objBomb, VECTOR_ZERO, TRUE, 5000, 2500, 2500, HINTTYPE_NO_FOV)	//<<5318.2, -5184.4, 84.0>>
							
							IF NOT bVideoRecording
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
								
								bVideoRecording = TRUE
							ENDIF
							
							SET_LABEL_AS_TRIGGERED(HintCamBomb, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(HintCamBombStop)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) > 0.232 //0.068
							STOP_GAMEPLAY_HINT()
							
							SET_LABEL_AS_TRIGGERED(HintCamBombStop, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
//				IF IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue5_Main, "set_c4_mainaction_brad")
//					IF GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue5_Main, "set_c4_mainaction_brad") > 0.239
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) > 0.239
					//Tag for detach
						IF DOES_ENTITY_EXIST(objBomb)
							DETACH_ENTITY(objBomb, FALSE)
							DETACH_ENTITY(objBombGreen, FALSE)
							
							FREEZE_ENTITY_POSITION(objBomb, TRUE)
							FREEZE_ENTITY_POSITION(objBombGreen, TRUE)
						ENDIF
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 3
				//Sound
				LOAD_STREAM("PROLOGUE_BLAST_SECURITY_DOORS_MASTER")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				
//				IF IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue5_Main, "set_c4_mainaction_brad")
//					IF GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue5_Main, "set_c4_mainaction_brad") > 0.453
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) > 0.403
						SET_ENTITY_VISIBLE(objBomb, FALSE)
						SET_ENTITY_VISIBLE(objBombGreen, TRUE)
						
						IF sIDSecDoorBombBeep = -1
							sIDSecDoorBombBeep = GET_SOUND_ID()
							PLAY_SOUND_FROM_ENTITY(sIDSecDoorBombBeep, "Security_Door_Bomb_Bleeps", objBombGreen, "Prologue_Sounds")
						ENDIF
					ENDIF
					
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) > 0.453
						SET_ENTITY_VISIBLE(objBomb, FALSE)
						SET_ENTITY_VISIBLE(objBombGreen, TRUE)
						
						CLEAR_AREA(<<5318.1221, -5185.5044, 85.7186 - 3.2>>, 4.0, TRUE)
						
						SHAKE_CAM(camMain, "MEDIUM_EXPLOSION_SHAKE", 0.1)
						
						IF HAS_PTFX_ASSET_LOADED()
							START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_prologue_door_blast", <<5318.00, -5185.06, 83.82>>, <<0.0, 0.0, 0.0>>, 1.0)
						ENDIF
						
						IF HAS_PTFX_ASSET_LOADED()
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmokeCloud)
								ptfxSmokeCloud = START_PARTICLE_FX_LOOPED_AT_COORD("scr_prologue_vault_fog", <<5313.3545, -5177.7656, 82.5186>>, <<0.0, 0.0, 0.0>>, 1.0)
							ENDIF
						ENDIF
						
						IF HAS_PTFX_ASSET_LOADED()
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEmber)
								ptfxEmber = START_PARTICLE_FX_LOOPED_AT_COORD("ent_ray_pro_door_embers", <<5318.1626, -5184.8325, 82.5186>>, <<0.0, 0.0, 0.0>>, 1.0)
								
								iEmberTimer = GET_GAME_TIMER() + 15000
							ENDIF
						ENDIF
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
						
						SHAKE_GAMEPLAY_CAM("MEDIUM_EXPLOSION_SHAKE", 0.25)
						
						//Sound
//						sIDSecDoorBlast = GET_SOUND_ID()
//						PLAY_SOUND_FROM_COORD(sIDSecDoorBlast, "Security_Door_Explosion", <<5318.2, 5184.8, 84.1>>, "Prologue_Sounds")
						
						sIDDistantSirens = GET_SOUND_ID()
						PLAY_SOUND_FROM_COORD(sIDDistantSirens, "COPS_ARRIVE", <<5359.9, -5190, 83.00>>, "Prologue_Sounds")
						
						sIDSecurityDoorAlarm = GET_SOUND_ID()
						PLAY_SOUND_FROM_COORD(sIDSecurityDoorAlarm, "Security_Door_Alarm", <<5318.2, -5184.8, 84.1>>, "Prologue_Sounds")
						
						IF sIDSecDoorBombBeep != -1
							STOP_SOUND(sIDSecDoorBombBeep)
							RELEASE_SOUND_ID(sIDSecDoorBombBeep)
							sIDSecDoorBombBeep = -1
						ENDIF
						
						IF NOT bAudioStream
							SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
							
							bAudioStream = TRUE
						ENDIF
						
						PLAY_STREAM_FRONTEND()
						
						//Rayfire
						IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
							IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) = RFMO_STATE_PRIMED
								SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion, RFMO_STATE_START_ANIM)
							ENDIF
						ENDIF
						
						PLAY_AUDIO(PROLOGUE_TEST_BLAST_DOORS_EXPLODE)
						
						SAFE_DELETE_OBJECT(objBomb)
						SAFE_DELETE_OBJECT(objBombGreen)
						
						PLAY_PED_AMBIENT_SPEECH(pedBrad, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 4
//				IF IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue5_Main, "set_c4_mainaction_brad")
//					IF GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue5_Main, "set_c4_mainaction_brad") > 0.630 //0.653
				PRINTLN("DISABLING DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) > 0.630
//						RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
						
						//CLEAR_PED_TASKS(playerPedID)
						
						IF bVideoRecording
							REPLAY_STOP_EVENT()
							
							bVideoRecording = FALSE
						ENDIF
						
						TASK_PUT_PED_DIRECTLY_INTO_COVER(playerPedID, GET_ENTITY_COORDS(playerPedID), -1, FALSE, 0.5, TRUE, FALSE)
						
						WHILE NOT IS_PED_IN_COVER(playerPedID)
						OR IS_PED_GOING_INTO_COVER(playerPedID)
							IF GET_IS_USING_FPS_THIRD_PERSON_COVER()
								IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor")
								OR IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor_fps")
								OR IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "SET_C4_MainAction_Trevor_Fps_Pos2")
								OR IS_PED_IN_COVER(playerPedID)
								OR IS_PED_GOING_INTO_COVER(playerPedID)
									DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
								ENDIF
							ENDIF
							
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-137.6 - GET_ENTITY_HEADING(playerPedID))
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-11.6)
						
						CREATE_CONVERSATION_ADV(PRO_Cover, "PRO_Cover")
						
						//CLEAR_TEXT()
						CLEAR_PRINTS()
						SAFE_CLEAR_HELP()
						PRINT_HELP_ADV(PROHLP_COVER3, "PROHLP_COVER3")
						
						ADVANCE_STAGE()
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF GET_IS_USING_FPS_THIRD_PERSON_COVER()
			IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor")
			OR IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor_fps")
			OR IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "SET_C4_MainAction_Trevor_Fps_Pos2")
			OR IS_PED_IN_COVER(playerPedID)
			OR IS_PED_GOING_INTO_COVER(playerPedID)
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			ENDIF
		ENDIF
		
//		IF IS_GAMEPLAY_HINT_ACTIVE()
//		AND TIMERB() > DEFAULT_INTERP_IN_TIME
//		AND TIMERB() < DEFAULT_INTERP_IN_TIME + DEFAULT_DWELL_TIME
//		AND NOT HAS_LABEL_BEEN_TRIGGERED(HintSet)
//			VECTOR vCamRot = GET_FINAL_RENDERED_CAM_ROT()
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(vCamRot.Z - GET_ENTITY_HEADING(playerPedID))
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-11.6)
//			
//			SET_LABEL_AS_TRIGGERED(HintSet, TRUE)
//		ENDIF
		
//		IF IS_CAM_RENDERING(camMain)
//		AND NOT IS_GAMEPLAY_CAM_RENDERING()
//		AND NOT IS_INTERPOLATING_TO_SCRIPT_CAMS()
//		AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//			VECTOR vCamRot = GET_CAM_ROT(camMain)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(vCamRot.Z - GET_ENTITY_HEADING(playerPedID))
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-11.6)
//			
//			INT iTolerance
//			
//			iTolerance = 8
//			
//			INT iLTSx, iLTSy, iRTSx, iRTSy
//			
//			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLTSx, iLTSy, iRTSx, iRTSy)
//			
//			IF (iRTSx < -iTolerance OR iRTSx > iTolerance)
//			OR (iRTSy < -iTolerance OR iRTSy > iTolerance)
//				RENDER_SCRIPT_CAMS(FALSE, TRUE, 500, FALSE)
//			ENDIF
//		ENDIF
		
		IF iCutsceneStage < 3
//			IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.476563, -5178.753418, 82.518654>>, <<5307.015137, -5178.661621, 85.518654>>, 10.0)
//			AND NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5317.843262, -5185.007813, 82.518654>>, <<5317.709961, -5177.232422, 85.518654>>, 5.25)
			IF IS_ENTITY_AT_COORD(playerPedID, <<5306.901367, -5175.998535, 83.518784>>, <<1.75, 3.0, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5309.857422, -5173.836426, 83.518784>>, <<3.75, 1.75, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5319.701660, -5174.781738, 83.518784>>, <<1.25, 2.0, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5316.187988, -5178.350586, 83.518784>>, <<1.25, 2.0, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5317.587891, -5178.854004, 83.518784>>, <<3.25, 0.75, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5317.539063, -5183.494141, 83.518784>>, <<3.0, 1.5, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5317.539063, -5183.494141, 83.518784>>, <<3.0, 1.5, 1.5>>)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5310.659180, -5182.642090, 84.518784>>, <<6.5, 1.0, 2.0>>)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				
				SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			ENDIF
		ENDIF
		
		//Particles
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEmber)
			IF GET_GAME_TIMER() >= iEmberTimer
				STOP_PARTICLE_FX_LOOPED(ptfxEmber)
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), TRUE)
		
		SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, FALSE)
		
		//Pathing
		SET_PED_STEERS_AROUND_PEDS(playerPedMichael, TRUE)
		
		//Cover
		INT i
		
		REPEAT COUNT_OF(covPoint) i
			REMOVE_COVER_POINT(covPoint[i])
		ENDREPEAT
		
		REMOVE_ALL_COVER_BLOCKING_AREAS()
		
		IF IS_ITEMSET_VALID(itemCover)
			DESTROY_ITEMSET(itemCover)
		ENDIF
		
		//Rayfire
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) != RFMO_STATE_END
			AND GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) != RFMO_STATE_ANIMATING
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion, RFMO_STATE_ENDING)
			ENDIF
		ENDIF
		
		SAFE_DELETE_OBJECT(objBomb)
		SAFE_DELETE_OBJECT(objBombGreen)
		SAFE_DELETE_OBJECT(objBomb3)
		SAFE_DELETE_OBJECT(objBomb2)
		SAFE_DELETE_OBJECT(objBomb1)
		
//		ASSISTED_MOVEMENT_REMOVE_ROUTE("Pro_S4")
		
		SAFE_REMOVE_BLIP(blipDestination)
		
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_SWITCH_TO_TREVOR")
			STOP_AUDIO_SCENE("PROLOGUE_SWITCH_TO_TREVOR")
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(modBomb)
		SET_MODEL_AS_NO_LONGER_NEEDED(modBombGreen)
		SET_MODEL_AS_NO_LONGER_NEEDED(modBomb3)
		SET_MODEL_AS_NO_LONGER_NEEDED(modBomb2)
		SET_MODEL_AS_NO_LONGER_NEEDED(modBomb1)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC DuckUnderShutter()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Radar
		bRadar = TRUE
		
		#IF FEATURE_GEN9_EXCLUSIVE
			bPlayGarageSound = FALSE
		#ENDIF
		
		IF NOT HAS_PED_GOT_WEAPON(playerPedID, wtCarbineRifle)
			GIVE_WEAPON_TO_PED(playerPedID, wtCarbineRifle, 500, TRUE)
		ENDIF
		
		SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)
		
		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS")
		OR NOT REQUEST_SCRIPT_AUDIO_BANK("ICE_FOOTSTEPS")
			#IF IS_DEBUG_BUILD	PRINTLN("LOADING AUDIO BANK = SNOW_FOOTSTEPS")	#ENDIF
			#IF IS_DEBUG_BUILD	PRINTLN("LOADING AUDIO BANK = ICE_FOOTSTEPS")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CREATE_CONVERSATION_ADV(PRO_Cover, "PRO_Cover")
		ENDIF
		SAFE_ADD_BLIP_LOCATION(blipDestination, <<5320.2261, -5186.1284, 82.5187>>)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(playerPedID, <<5317.1089, -5182.1265, 82.5186>>, 201.2845, FALSE)
			
			//Michael
			SET_PED_POSITION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<5320.0708, -5187.5234, 82.5186>>, 97.2528, FALSE)
			
			//Brad
			SET_PED_POSITION(pedBrad, <<5316.5908, -5187.2148, 82.5186>>, 269.9326, FALSE)
			
			sceneDuckUnder = CREATE_SYNCHRONIZED_SCENE(sceneDuckUnderPos, sceneDuckUnderRot)
			
			TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sceneDuckUnder, sAnimDictPrologue5_End, "set_c4_end_loop_player0", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION)
			TASK_SYNCHRONIZED_SCENE(pedBrad, sceneDuckUnder, sAnimDictPrologue5_End, "set_c4_end_loop_brad", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneDuckUnder, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlastDoors)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sceneBlastDoors) >= 1.0
			sceneDuckUnder = CREATE_SYNCHRONIZED_SCENE(sceneDuckUnderPos, sceneDuckUnderRot)
			
			TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sceneDuckUnder, sAnimDictPrologue5_End, "set_c4_end_loop_player0", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION)
			TASK_SYNCHRONIZED_SCENE(pedBrad, sceneDuckUnder, sAnimDictPrologue5_End, "set_c4_end_loop_brad", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_VEHICLE_IMPACT | RBF_EXPLOSION)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneDuckUnder, TRUE)
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_SHUTTER)
						PRINT_ADV(PRO_SHUTTER, "PRO_SHUTTER")
						
						SAFE_ADD_BLIP_LOCATION(blipDestination, <<5320.2261, -5186.1284, 82.5187>>)
						SET_BLIP_COLOUR(blipDestination, BLIP_COLOUR_GREEN)
					ELIF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_SHUTTER")
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							SWITCH iDialogueStage
								CASE 0
									IF GET_GAME_TIMER() > iDialogueTimer
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Under_1, "PRO_Under", "PRO_Under_1", CONV_PRIORITY_LOW)
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 5000)
											
											iDialogueStage++
										ENDIF
									ENDIF
								BREAK
								CASE 1
									IF GET_GAME_TIMER() > iDialogueTimer
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Under_2, "PRO_Under", "PRO_Under_2", CONV_PRIORITY_LOW)
										
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 5000)
											
											iDialogueStage++
										ENDIF
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
				
//				IF NOT IS_PED_IN_COVER(playerPedID)
//				AND NOT IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue5_Main, "set_c4_mainaction_trevor")
//					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_COVER3")
//					AND GET_GAME_TIMER() > iHelpTimer
//						SAFE_CLEAR_HELP()
//					ENDIF
//				ENDIF
				
				IF IS_ENTITY_AT_COORD(playerPedID, <<5318.190430, -5187.577637, 84.268669>>, <<2.5, 4.0, 1.75>>)
				OR IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue_CleanSmoke, "walk")
					SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)	PRINTLN("PEDMOVE_WALK")
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				ENDIF
				
				IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				AND IS_ENTITY_AT_COORD(playerPedID, <<5318.190430, -5187.577637, 84.268669>>, <<10.0, 10.0, 1.75>>))
					SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)	PRINTLN("PEDMOVE_WALK")
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.485840, -5186.873535, 82.518639>>, <<5315.874023, -5186.868164, 86.018639>>, 3.0)
					SAFE_REMOVE_BLIP(blipDestination)
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_SHUTTER")
						SAFE_CLEAR_THIS_PRINT("PRO_SHUTTER")
					ENDIF
					
					CLEAR_PED_TASKS(playerPedID)
					
					TASK_FOLLOW_NAV_MESH_TO_COORD(playerPedID, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Duck, "press_button_player2", sceneDuckUnderPos - <<0.0, 0.41, 0.0>> + <<-0.18, -0.06, 0.0>>, sceneDuckUnderRot), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictPrologue5_Duck, "press_button_player2", sceneDuckUnderPos - <<0.0, 0.41, 0.0>> + <<-0.18, -0.06, 0.0>>, sceneDuckUnderRot)))
					
					SET_GAMEPLAY_COORD_HINT(<<5320.46387, -5186.74561, 84.11650>>, DEFAULT_DWELL_TIME, DEFAULT_INTERP_IN_TIME, DEFAULT_INTERP_OUT_TIME, HINTTYPE_NO_FOV)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT HAS_LABEL_BEEN_TRIGGERED(StartShutterAnim)
					IF IS_ENTITY_AT_COORD(playerPedID, GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictPrologue5_Duck, "press_button_player2", sceneDuckUnderPos - <<0.0, 0.41, 0.0>> + <<-0.18, -0.06, 0.0>>, sceneDuckUnderRot), <<0.2, 0.2, 3.0>>)
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
							sceneShutterSwitch = CREATE_SYNCHRONIZED_SCENE(sceneDuckUnderPos - <<0.0, 0.41, 0.0>> + <<-0.18, -0.06, 0.0>>, sceneDuckUnderRot)
							TASK_SYNCHRONIZED_SCENE(playerPedID, sceneShutterSwitch, sAnimDictPrologue5_Duck, "press_button_player2", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneShutterSwitch, FALSE)
							
							SET_LABEL_AS_TRIGGERED(StartShutterAnim, TRUE)
						ELSE
							sceneShutterSwitch = CREATE_SYNCHRONIZED_SCENE(sceneDuckUnderPos - <<0.0, 0.41, 0.0>> + <<-0.18, -0.06, 0.0>>, sceneDuckUnderRot)
							TASK_SYNCHRONIZED_SCENE(playerPedID, sceneShutterSwitch, sAnimDictPrologue5_Duck, "press_button_player2", NORMAL_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, NORMAL_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneShutterSwitch, FALSE)
							
							SET_LABEL_AS_TRIGGERED(StartShutterAnim, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch)
				OR (HAS_LABEL_BEEN_TRIGGERED(StartShutterAnim)
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch))
					#IF FEATURE_GEN9_EXCLUSIVE
						IF NOT bPlayGarageSound
							IF sIDGarageDoor = -1
								sIDGarageDoor = GET_SOUND_ID()
								PLAY_SOUND_FROM_COORD(sIDGarageDoor, "GEN9_Prologue_Garage_Door_Motor", <<5320.60, -5188.61, 85.02>>)
								bPlayGarageSound = TRUE
							ENDIF
						ENDIF
					#ENDIF
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_ShutterDoor)
							CREATE_CONVERSATION_ADV(PRO_Shutter, "PRO_Shutter", CONV_PRIORITY_MEDIUM, FALSE)
							
							SET_LABEL_AS_TRIGGERED(PRO_ShutterDoor, TRUE)
						ENDIF
						
						ADVANCE_CUTSCENE()
					ELSE
						IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_Shutter")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneShutterSwitch) > 0.178)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch)
					IF DOES_ENTITY_EXIST(objGarage)
						IF NOT IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
							PLAY_ENTITY_ANIM(objGarage, "GDoor_Open", sAnimDictMapObjects, INSTANT_BLEND_IN, FALSE, TRUE)
						ELSE
							SET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open", 0.0)
							SET_ENTITY_ANIM_SPEED(objGarage, sAnimDictMapObjects, "GDoor_Open", 1.0)
						ENDIF
					ENDIF
					
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pro_door_snow", <<5320.53711, -5188.55469, 82.51863>>, <<0.0, 0.0, 0.0>>)	//83.86947
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_TAKE_COVER")
						STOP_AUDIO_SCENE("PROLOGUE_TAKE_COVER")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_POLICE_SHOOTOUT")
						START_AUDIO_SCENE("PROLOGUE_POLICE_SHOOTOUT")
					ENDIF
					
					//Recording hitting button and then ducking out.
					REPLAY_RECORD_BACK_FOR_TIME(0.5, 5, REPLAY_IMPORTANCE_HIGH)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF DOES_ENTITY_EXIST(objGarage)
					IF IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
					AND GET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open") >= 0.05
						TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictPrologue5_Duck, "duck_shutter_player0", sceneDuckUnderPos, sceneDuckUnderRot, NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
						TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue5_Duck, "duck_shutter_brad", sceneDuckUnderPos, sceneDuckUnderRot, SLOW_BLEND_IN, WALK_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0.221)
						
						SET_ENTITY_NO_COLLISION_ENTITY(objHiddenCollision, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
						SET_ENTITY_NO_COLLISION_ENTITY(objHiddenCollision, pedBrad, FALSE)
						SET_ENTITY_NO_COLLISION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], objHiddenCollision, FALSE)
						SET_ENTITY_NO_COLLISION_ENTITY(pedBrad, objHiddenCollision, FALSE)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneShutterSwitch) >= 0.5
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch)
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		//Clean Smoke
		IF TIMERA() < 10000
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CLEANSMOKE)
				IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5320.420410, -5182.820800, 82.518669>>, <<5315.878418, -5182.866699, 85.518669>>, 3.5)
				AND NOT IS_PED_IN_COVER(playerPedID) AND NOT IS_PED_GOING_INTO_COVER(playerPedID)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(playerPedID, "COUGH", "WAVELOAD_PAIN_MALE")
					
					IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
						TASK_PLAY_ANIM(playerPedID, sAnimDictPrologue_CleanSmoke, "walk", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
					ENDIF
					
					SET_LABEL_AS_TRIGGERED(CLEANSMOKE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF iCutsceneStage >= 1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue_CleanSmoke, "walk")
			SET_PED_CAPSULE(playerPedID, 0.75)
			
			SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, PEDMOVE_WALK)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		ENDIF
		
		//Particles
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEmber)
			IF GET_GAME_TIMER() >= iEmberTimer
				STOP_PARTICLE_FX_LOOPED(ptfxEmber)
			ENDIF
		ENDIF
		
		SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Hint
		STOP_GAMEPLAY_HINT()
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_TAKE_COVER")
			STOP_AUDIO_SCENE("PROLOGUE_TAKE_COVER")
		ENDIF
		
		//Cover
		INT i
		
		REPEAT COUNT_OF(covPointMap) i
			REMOVE_COVER_POINT(covPointMap[i])
		ENDREPEAT
		
		REPEAT COUNT_OF(covPointBlastDoors) i
			REMOVE_COVER_POINT(covPointBlastDoors[i])
		ENDREPEAT
		
		//Guard
		SET_PED_AS_NO_LONGER_NEEDED(pedGuard)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)
		
		//Sound
		IF sIDSecDoorBlast != -1
			STOP_SOUND(sIDSecDoorBlast)
			RELEASE_SOUND_ID(sIDSecDoorBlast)
			sIDSecDoorBlast = -1
		ENDIF
		
		IF sIDDistantSirens != -1
			STOP_SOUND(sIDDistantSirens)
			RELEASE_SOUND_ID(sIDDistantSirens)
			sIDDistantSirens = -1
		ENDIF
		
		IF sIDSecurityDoorAlarm != -1
			STOP_SOUND(sIDSecurityDoorAlarm)
			RELEASE_SOUND_ID(sIDSecurityDoorAlarm)
			sIDSecurityDoorAlarm = -1
		ENDIF
		
		RELEASE_SCRIPT_AUDIO_BANK()
		
		//Particles
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxEmber)
			STOP_PARTICLE_FX_LOOPED(ptfxEmber)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxSmokeCloud)
			STOP_PARTICLE_FX_LOOPED(ptfxSmokeCloud)
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

BOOL bFirstWaveCopsDead

INT iShootOutStage1, iShootOutStage2, iShootOutStage3

ENUM ALLY_STATE
	ALLY_INIT,
	ALLY_WAIT_TO_TASK_INTO_PARKING_LOT,
	ALLY_WAIT_FOR_PARKING_LOT_CLEAR,
	ALLY_WAIT_FOR_PLAYER_BY_GATE,
	ALLY_WAIT_FOR_VAN_CLEAR,
	ALLY_WAIT_FOR_CAR_EXPLOSION,
	ALLY_TASK_NEAR_EXPLODED_CARS, 
	ALLY_GO_NEAR_FINAL_CAR, 
	ALLY_TASK_SHOOT_THE_SHIT_OUT_OF_THE_FINAL_CAR,
	ALLY_DONE
ENDENUM
ALLY_STATE eAllyState

// RJM - Wrapper for tasking the allies.  This replaces the ped recordings.
//  We tell the AIs to run to a coord, set a small defensive sphere around their destination, and task them to combat.
//  Also, add a little random pause before they run, so it looks a bit organic when Brad & Michael run together.
PROC TASK_ALLY_RUN_TO_COORD_AND_ATTACK(PED_INDEX pedIndex, VECTOR vDefenseSphereCenter)
	FLOAT fRadius = 5.0
	INT iRandomTime = GET_RANDOM_INT_IN_RANGE(0, 500)
	IF NOT IS_ENTITY_DEAD(pedIndex)
//		CLEAR_PED_TASKS(pedIndex)
		REMOVE_PED_DEFENSIVE_AREA(pedIndex)
		REMOVE_PED_DEFENSIVE_AREA(pedIndex, TRUE)
		SET_PED_SPHERE_DEFENSIVE_AREA(pedIndex, vDefenseSphereCenter, fRadius)
		IF NOT IS_PED_IN_COMBAT(pedIndex)
		OPEN_SEQUENCE_TASK(seqMain)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			TASK_AIM_GUN_AT_COORD(NULL, vDefenseSphereCenter - <<0.0, 0.0, 1.0>>, iRandomTime)
//			TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vDefenseSphereCenter, vDefenseSphereCenter + (vDefenseSphereCenter - GET_ENTITY_COORDS(pedIndex)), PEDMOVE_RUN, FALSE)	//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDefenseSphereCenter, PEDMOVE_SPRINT)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 1000.0)
		CLOSE_SEQUENCE_TASK(seqMain)
		TASK_PERFORM_SEQUENCE(pedIndex, seqMain)
		CLEAR_SEQUENCE_TASK(seqMain)
		ENDIF
//		IF pedIndex = notPlayerPedID
//			iSequenceBuddy = 2
//			vSequenceBuddy = GET_ENTITY_COORDS(notPlayerPedID)
//		ELIF pedIndex = pedBrad
//			iSequenceBrad = 2
//			vSequenceBrad = GET_ENTITY_COORDS(pedBrad)
//		ENDIF
	ENDIF
ENDPROC

// RJM - This is the new state machine for tasking Brad and Michael to advance. 
// I wanted this to be centrally located, so we could easily change and update Brad and Michael's tasks
PROC UPDATE_ALLIES_TASKING()
	//Main stage state machine
	SWITCH eAllyState
		CASE ALLY_INIT
			#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_WAIT_TO_TASK_INTO_PARKING_LOT")	#ENDIF
			eAllyState = ALLY_WAIT_TO_TASK_INTO_PARKING_LOT
		BREAK
		// RJM - In this stage, we wait for Brad and Michael to finish playing their 'duck' animations before we task them
		CASE ALLY_WAIT_TO_TASK_INTO_PARKING_LOT
			// Wait for Michael to finish playing his animation before tasking him into combat
			IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelStartCombat)
				IF NOT IS_ENTITY_PLAYING_ANIM(notPlayerPedID, sAnimDictPrologue5_Duck, "duck_shutter_player0")
				OR (IS_ENTITY_PLAYING_ANIM(notPlayerPedID, sAnimDictPrologue5_Duck, "duck_shutter_player0")
				AND GET_ENTITY_ANIM_CURRENT_TIME(notPlayerPedID, sAnimDictPrologue5_Duck, "duck_shutter_player0") > 0.784)	//0.95)
					CLEAR_PED_TASKS(notPlayerPedID)
					SET_PED_SPHERE_DEFENSIVE_AREA(notPlayerPedID,<<5336.7935, -5193.3403, 81.9088>>, 2.0)
					SET_PED_SPHERE_DEFENSIVE_AREA(notPlayerPedID, <<5329.6567, -5195.1406, 81.7030>>, 2.0, TRUE, TRUE)
					
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<5322.7285, -5195.1299, 82.5190>>, <<5343.3496, -5190.2065, 82.7770>>, PEDMOVE_RUN, FALSE, 0.5, 4.0, FALSE, ENAV_NO_STOPPING)
						TASK_GO_STRAIGHT_TO_COORD(NULL, <<5328.2588, -5195.1309, 81.6819>>, PEDMOVE_RUN)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(notPlayerPedID, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					iSequenceBuddy = 1
					vSequenceBuddy = GET_ENTITY_COORDS(notPlayerPedID)
					
//					TASK_ALLY_RUN_TO_COORD_AND_ATTACK(notPlayerPedID, <<5328.2588, -5195.1309, 81.6819>>)
					
					SET_PED_ACCURACY(notPlayerPedID, 2)
					
					SET_LABEL_AS_TRIGGERED(MichaelStartCombat, TRUE)
				ENDIF
			ENDIF
			
			// Wait for Brad to finish playing his animation before tasking him into combat
			IF NOT HAS_LABEL_BEEN_TRIGGERED(BradStartCombat)
				IF NOT IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue5_Duck, "duck_shutter_brad")
				OR (IS_ENTITY_PLAYING_ANIM(pedBrad, sAnimDictPrologue5_Duck, "duck_shutter_brad")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedBrad, sAnimDictPrologue5_Duck, "duck_shutter_brad") > 0.95)
					CLEAR_PED_TASKS(pedBrad)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedBrad, <<5331.9883, -5191.7563, 81.7708>>, 2.0)
//					SET_PED_SPHERE_DEFENSIVE_AREA(pedBrad, <<5326.3403, -5191.8643, 81.7742>>, 2.0, TRUE)
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_SHOOT_AT_COORD(NULL, <<5343.3496, -5190.2065, 82.7770>>, 1000, FIRING_TYPE_DEFAULT)	//TASK_AIM_GUN_AT_COORD(NULL, <<5343.3496, -5190.2065, 82.7770>>, 1500)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<5323.0347, -5192.4453, 82.5190>>, <<5343.3496, -5190.2065, 82.7770>>, PEDMOVE_WALK, FALSE, 0.5, 4.0, FALSE, ENAV_NO_STOPPING)
						TASK_GO_STRAIGHT_TO_COORD(NULL, <<5325.6709, -5191.8677, 81.7683>>, PEDMOVE_RUN)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 500.0)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedBrad, seqMain)
					CLEAR_SEQUENCE_TASK(seqMain)
					
					iSequenceBrad = 2
					vSequenceBrad = GET_ENTITY_COORDS(pedBrad)
					
//					TASK_ALLY_RUN_TO_COORD_AND_ATTACK(pedBrad, <<5325.6709, -5191.8677, 81.7683>>)
					
					SET_PED_ACCURACY(pedBrad, 2)
					
					SET_LABEL_AS_TRIGGERED(BradStartCombat, TRUE)
				ENDIF
			ENDIF
			
			// Once both have been tasked, move on
			IF HAS_LABEL_BEEN_TRIGGERED(MichaelStartCombat)
			AND HAS_LABEL_BEEN_TRIGGERED(BradStartCombat)
				#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_WAIT_FOR_PARKING_LOT_CLEAR")	#ENDIF
				eAllyState = ALLY_WAIT_FOR_PARKING_LOT_CLEAR	
			ENDIF
		BREAK
		// Wait for all the parking lot cops to be dead before moving up the allies
		CASE ALLY_WAIT_FOR_PARKING_LOT_CLEAR
			IF bFirstWaveCopsDead		//THIS IS SET IN THE ShootOut() FUNCTION, WHEN DUDES 0 -> 11 ARE DEAD!
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(notPlayerPedID, <<5353.9634, -5190.4790, 81.7737>>)
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(pedBrad, <<5355.9717, -5186.0688, 81.7840>>)
				
				#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_WAIT_FOR_PLAYER_BY_GATE")	#ENDIF
				eAllyState = ALLY_WAIT_FOR_PLAYER_BY_GATE
			ENDIF
		BREAK
		// Wait for the player to run to the gate before tasking the allies to move ahead
		CASE ALLY_WAIT_FOR_PLAYER_BY_GATE
			IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5356.732422, -5201.155273, 80.831223>>, <<5356.545410, -5179.600098, 96.836906>>, 20.0)
			OR IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5417.894043, -5108.792480, 75.563187>>, <<5412.487793, -5240.660156, 95.597893>>, 100.0)
			OR (IS_PED_DEAD_OR_DYING(pedCop[9]) AND IS_PED_DEAD_OR_DYING(pedCop[10]))
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(notPlayerPedID, <<5373.1367, -5189.1504, 81.2204>>)
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(pedBrad, <<5372.1699, -5182.8872, 81.2387>>)
				
				#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_WAIT_FOR_VAN_CLEAR")	#ENDIF
				eAllyState = ALLY_WAIT_FOR_VAN_CLEAR
			ENDIF
		BREAK
		// Wait for the 2 van enemies to be killed before the allies take cover by it
		CASE ALLY_WAIT_FOR_VAN_CLEAR
			IF IS_PED_DEAD_OR_DYING(pedCop[9]) AND IS_PED_DEAD_OR_DYING(pedCop[10])
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(notPlayerPedID, <<5383.8882, -5184.4175, 80.3353>>)
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(pedBrad, <<5384.1724, -5190.8066, 80.5079>>)
				
				#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_WAIT_FOR_CAR_EXPLOSION")	#ENDIF
				eAllyState = ALLY_WAIT_FOR_CAR_EXPLOSION
			ENDIF
		BREAK
		// Wait for the car to explode before moving the allies ahead
		CASE ALLY_WAIT_FOR_CAR_EXPLOSION	//Car doesn't explode any more...
			IF IS_PED_INJURED(pedCop[11])
			AND IS_PED_INJURED(pedCop[12])
				#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_TASK_NEAR_EXPLODED_CARS")	#ENDIF
				eAllyState = ALLY_TASK_NEAR_EXPLODED_CARS
			ENDIF
		BREAK
		// Move the allies near the exploded car
		CASE ALLY_TASK_NEAR_EXPLODED_CARS
			TASK_ALLY_RUN_TO_COORD_AND_ATTACK(notPlayerPedID, <<5398.9614, -5178.0063, 79.4069>>)
			TASK_ALLY_RUN_TO_COORD_AND_ATTACK(pedBrad, <<5400.8267, -5180.2720, 79.3336>>)
			
			#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_GO_NEAR_FINAL_CAR")	#ENDIF
			eAllyState = ALLY_GO_NEAR_FINAL_CAR
		BREAK
		// Once all the enemies from the 2 cop cars near the barn are dead, move up the allies
		CASE ALLY_GO_NEAR_FINAL_CAR
			IF IS_PED_DEAD_OR_DYING(pedCop[11]) AND IS_PED_DEAD_OR_DYING(pedCop[12])
			AND IS_PED_DEAD_OR_DYING(pedCop[13]) AND IS_PED_DEAD_OR_DYING(pedCop[14])
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(notPlayerPedID, (<<5417.7598, -5175.6465, 78.1579>>))
				TASK_ALLY_RUN_TO_COORD_AND_ATTACK(pedBrad, (<<5417.4375, -5168.9175, 77.9954>>))
				
				#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_TASKING - Going to ALLY_TASK_SHOOT_THE_SHIT_OUT_OF_THE_FINAL_CAR")	#ENDIF
				eAllyState = ALLY_TASK_SHOOT_THE_SHIT_OUT_OF_THE_FINAL_CAR
			ENDIF
		BREAK
		CASE ALLY_TASK_SHOOT_THE_SHIT_OUT_OF_THE_FINAL_CAR
			eAllyState = ALLY_DONE
		BREAK
		CASE ALLY_DONE
			// Done
		BREAK
	ENDSWITCH	
ENDPROC

INT iMaskRemoveTimeout	//Added for bug 1640970

PROC ShootOut()
	IF INIT_STAGE()
		//Reset
		iShootOutStage1 = 0
		iShootOutStage2 = 0
		iShootOutStage3 = 0
		eAllyState = ALLY_INIT
		
		//Replay 
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayShootout), "stageShootOut")
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		IF NOT IS_SCREEN_FADED_OUT()
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_COP_GUNFIGHT)
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_POLICE_SHOOTOUT")
			START_AUDIO_SCENE("PROLOGUE_POLICE_SHOOTOUT")
		ENDIF
		
		//Radar
		bRadar = TRUE
		
		IF bSwapTrolley = FALSE
			CREATE_MODEL_SWAP(<<5302.142, -5191.521, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
			CREATE_MODEL_SWAP(<<5308.040, -5191.028, 82.992>>, 1.0, PROP_CASH_TROLLY, PROP_GOLD_TROLLY, TRUE)
			
			bSwapTrolley = TRUE
		ENDIF
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5336.8643, -5193.5386, 81.9129>>)
			covPoint[0] = ADD_COVER_POINT(<<5336.8174, -5193.3325, 81.9110>>, 270.8514, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5332.2729, -5185.2710, 81.7757>>)
			covPoint[1] = ADD_COVER_POINT(<<5332.2729, -5185.2710, 81.7757>>, 276.6267, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5332.1934, -5191.9116, 81.7758>>)
			covPoint[2] = ADD_COVER_POINT(<<5332.1934, -5191.9116, 81.7758>>, 276.6232, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5326.3535, -5185.2485, 81.7796>>)
			covPoint[3] = ADD_COVER_POINT(<<5326.3535, -5185.2485, 81.7796>>, 276.9970, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<5326.3535, -5191.8320, 81.7743>>)
			covPoint[4] = ADD_COVER_POINT(<<5326.3535, -5191.8320, 81.7743>>, 277.2627, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_0TO60)
		ENDIF
		
		//Action Mode
		SET_PED_USING_ACTION_MODE(playerPedID, TRUE)
		SET_PED_USING_ACTION_MODE(notPlayerPedID, TRUE, -1)
		SET_PED_USING_ACTION_MODE(pedBrad, TRUE, -1, "DEFAULT_ACTION")
		
		SET_ENTITY_PROOFS(playerPedID, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
		SET_ENTITY_PROOFS(notPlayerPedID, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
		SET_ENTITY_PROOFS(pedBrad, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
		
		SET_PED_UPPER_BODY_DAMAGE_ONLY(playerPedID, TRUE)
		SET_PED_UPPER_BODY_DAMAGE_ONLY(notPlayerPedID, TRUE)
		SET_PED_UPPER_BODY_DAMAGE_ONLY(pedBrad, TRUE)
		
		//Player
		SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_DISABLE_ENTRY_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_DisableExplosionReactions, TRUE)
		//ADDED STUFF TO TRY TO MAKE HIM BEHAVE IN COVER
		SET_PED_COMBAT_MOVEMENT(playerPedTrevor, CM_DEFENSIVE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_USE_COVER, TRUE)
		//SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_ForcePedToFaceRightInCover, TRUE)
		SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_BlockPedFromTurningInCover, FALSE)
		//SET_PED_FIRING_PATTERN(playerPedTrevor, FIRING_PATTERN_SHORT_BURSTS)
		
		//Michael
		SET_PED_SUFFERS_CRITICAL_HITS(playerPedMichael, FALSE)
		SET_PED_MAX_HEALTH(playerPedMichael, 800)
		SET_ENTITY_HEALTH(playerPedMichael, 800)
		SET_COMBAT_FLOAT(playerPedMichael, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.0)
		SET_PED_CONFIG_FLAG(playerPedMichael, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(playerPedMichael, PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedMichael, CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedMichael, CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedMichael, CA_DISABLE_ENTRY_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedMichael, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		//ADDED STUFF TO TRY TO MAKE HIM BEHAVE IN COVER
		SET_PED_COMBAT_MOVEMENT(playerPedMichael, CM_DEFENSIVE)
		SET_PED_COMBAT_ATTRIBUTES(playerPedMichael, CA_USE_COVER, TRUE)
		//SET_PED_CONFIG_FLAG(playerPedMichael, PCF_ForcePedToFaceRightInCover, TRUE)
		SET_PED_CONFIG_FLAG(playerPedMichael, PCF_BlockPedFromTurningInCover, FALSE)
		//SET_PED_FIRING_PATTERN(playerPedMichael, FIRING_PATTERN_SHORT_BURSTS)
		
		//Buddy
		SET_PED_SUFFERS_CRITICAL_HITS(pedBrad, FALSE)
		SET_PED_CHANCE_OF_FIRING_BLANKS(pedBrad, 100.0, 100.0)
		SET_PED_MAX_HEALTH(pedBrad, 800)
		SET_ENTITY_HEALTH(pedBrad, 800)
		SET_COMBAT_FLOAT(pedBrad, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.0)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_DISABLE_BULLET_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_DISABLE_ENTRY_REACTIONS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_DisableExplosionReactions, TRUE)
		//ADDED STUFF TO TRY TO MAKE HIM BEHAVE IN COVER
		SET_PED_COMBAT_MOVEMENT(pedBrad, CM_DEFENSIVE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_USE_COVER, TRUE)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_ForcePedToFaceRightInCover, TRUE)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_BlockPedFromTurningInCover, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_BLIND_FIRE_IN_COVER, TRUE)
		SET_PED_FIRING_PATTERN(pedBrad, FIRING_PATTERN_SHORT_BURSTS)
		
		//Blips
		SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		SAFE_REMOVE_BLIP(blipTrevor)
		
		//Dead Guard
		SPAWN_PED(pedDeadGuard, U_M_M_PROLSEC_01, <<5362.900, -5180.634, 82.802>>, 0.0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDeadGuard, TRUE)
		TASK_PLAY_ANIM_ADVANCED(pedDeadGuard, sAnimDictDeadGuard, "dead_guard", <<5362.900, -5180.634, 82.802>>, <<0.0, 0.0, 0.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION | AF_ENDS_IN_DEAD_POSE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDeadGuard)
		SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_PROLSEC_01)
		
		//Player Car
		SPAWN_VEHICLE(vehCar, RANCHERXL2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(026, 500.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(026, 500.0, sCarrec)), 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehCar, "87ALN974")
		FREEZE_ENTITY_POSITION(vehCar, TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(RANCHERXL2)
		SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
		//SET_ENTITY_PROOFS(vehCar, FALSE, TRUE, TRUE, FALSE, FALSE)
		SET_VEHICLE_TYRES_CAN_BURST(vehCar, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_FRONT_LEFT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_FRONT_RIGHT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_REAR_LEFT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehCar, SC_DOOR_REAR_RIGHT, FALSE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCar, FALSE)
		SET_VEHICLE_RADIO_ENABLED(vehCar, FALSE)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		pedGetaway = CREATE_PED_INSIDE_VEHICLE(vehCar, PEDTYPE_MISSION, U_M_Y_PROLDRIVER_01)
		SET_PED_COMPONENT_VARIATION(pedGetaway, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedGetaway, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedGetaway, relGroupFriendlyFire)
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedGetaway, "PROGETAWAY")
		SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_PROLDRIVER_01)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGetaway, TRUE)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		//Vans
		SPAWN_VEHICLE(vehVan[0], STOCKADE3, <<5341.3525 + 1.365, -5177.1487, 81.7620>>, 0.3367)
		//SET_ENTITY_PROOFS(vehVan[0], FALSE, TRUE, TRUE, FALSE, FALSE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehVan[0], FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehVan[0], TRUE)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		SPAWN_VEHICLE(vehVan[1], STOCKADE3, <<5337.0996 + 1.365, -5177.0315, 81.7620>>, 2.5903)
		//SET_ENTITY_PROOFS(vehVan[1], FALSE, TRUE, TRUE, FALSE, FALSE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehVan[1], FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehVan[1], TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(STOCKADE3)
		
		WAIT_WITH_DEATH_CHECKS(0)	//Trying to prevent the game pausing...
		
		//Car In Lot
		SPAWN_VEHICLE(vehCarInLot, EMPEROR3, <<5355.2109, -5179.9209, 81.8178>>, 341.2480)
		//SET_ENTITY_PROOFS(vehCarInLot, FALSE, TRUE, TRUE, FALSE, FALSE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCarInLot, FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehCarInLot, TRUE)
		
		//Police Cars
		//	First wave of cars in the parking lot
		SPAWN_VEHICLE(vehCop[0], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(003, 0.0, sCarrec)))
		WAIT_WITH_DEATH_CHECKS(0)
		SPAWN_VEHICLE(vehCop[1], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(004, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(004, 0.0, sCarrec)))
		WAIT_WITH_DEATH_CHECKS(0)
		SPAWN_VEHICLE(vehCop[2], POLICEOLD1, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(005, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(005, 0.0, sCarrec)))
		WAIT_WITH_DEATH_CHECKS(0)
		
		// Cop van that fits between first and second waves
		SPAWN_VEHICLE(vehCop[3], POLICEOLD1, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(006, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(006, 0.0, sCarrec)))
		WAIT_WITH_DEATH_CHECKS(0)
		
		// Second wave of cars that comes driving up the road by the barn
		SPAWN_VEHICLE(vehCop[4], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(008, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(008, 0.0, sCarrec)))
		WAIT_WITH_DEATH_CHECKS(0)
		SPAWN_VEHICLE(vehCop[5], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(009, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(009, 0.0, sCarrec)))
		WAIT_WITH_DEATH_CHECKS(0)
		
		//Cop car at the bottom of the road, near the barn
		SPAWN_VEHICLE(vehCop[6], POLICEOLD2, <<5505.6299, -5128.1724, 77.3763>>, 87.9630)
		WAIT_WITH_DEATH_CHECKS(0)
		
		INT i
		
		REPEAT COUNT_OF(vehCop) i
			IF NOT IS_ENTITY_DEAD(vehCop[i])
				//SET_ENTITY_PROOFS(vehCop[i], FALSE, TRUE, TRUE, FALSE, FALSE)
				SET_VEHICLE_STRONG(vehCop[i], TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[i], FALSE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehCop[i], TRUE)
				FREEZE_ENTITY_POSITION(vehCop[i], TRUE)
			ENDIF
		ENDREPEAT
		
		//Cops
		REPEAT 17 i //First 17 cops
			IF i <> 7
				createCop(i)
				
				IF i = 0 OR i = 3 OR i = 7
					GIVE_WEAPON_TO_PED(pedCop[i], wtSMG, INFINITE_AMMO, TRUE)
				ENDIF
				
				IF i = 4 OR i = 5 OR i = 9 OR i = 14 OR i = 16
					GIVE_WEAPON_TO_PED(pedCop[i], wtShotgun, INFINITE_AMMO, TRUE)
				ENDIF
				
				IF i >= 0 AND i <= 8
					SET_PED_COMBAT_ATTRIBUTES(pedCop[i], CA_CAN_USE_DYNAMIC_STRAFE_DECISIONS, FALSE)
					SET_COMBAT_FLOAT(pedCop[i], CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
				ENDIF
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDIF
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(pedCop[0])
			SET_PED_INTO_VEHICLE(pedCop[0], vehCop[0], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[1])
			SET_PED_INTO_VEHICLE(pedCop[1], vehCop[0], VS_FRONT_RIGHT)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[2])
			SET_PED_INTO_VEHICLE(pedCop[2], vehCop[1], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[3])
			SET_PED_INTO_VEHICLE(pedCop[3], vehCop[1], VS_FRONT_RIGHT)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[4])
			SET_PED_INTO_VEHICLE(pedCop[4], vehCop[2], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[5])
			SET_PED_INTO_VEHICLE(pedCop[5], vehCop[2], VS_FRONT_RIGHT)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[9])
			SET_PED_INTO_VEHICLE(pedCop[9], vehCop[3], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[10])
			SET_PED_INTO_VEHICLE(pedCop[10], vehCop[3], VS_FRONT_RIGHT)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[11])
			SET_PED_INTO_VEHICLE(pedCop[11], vehCop[4], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[12])
			SET_PED_INTO_VEHICLE(pedCop[12], vehCop[4], VS_FRONT_RIGHT)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[13])
			SET_PED_INTO_VEHICLE(pedCop[13], vehCop[5], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[14])
			SET_PED_INTO_VEHICLE(pedCop[14], vehCop[5], VS_FRONT_RIGHT)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[15])
			SET_PED_INTO_VEHICLE(pedCop[15], vehCop[6], VS_DRIVER)
		ENDIF
		IF DOES_ENTITY_EXIST(pedCop[16])
			SET_PED_INTO_VEHICLE(pedCop[16], vehCop[6], VS_FRONT_RIGHT)
		ENDIF
		
		// RJM - These cover blocking areas prevent the AI from going into cover in a few zones.
		//	These encompass the fences around the parking lot, the player's starting area, and some spots along the side
		//  of the road after the gate.  This keeps the enemy position more focused on the road.
		ADD_COVER_BLOCKING_AREA(<<5370.1421, -5212.4551, 78.9143>>, <<5359.3608, -5191.8301, 86.8785>>, TRUE, TRUE, TRUE, TRUE)	//Fence (Right)
		ADD_COVER_BLOCKING_AREA(<<5375.4380, -5180.9668, 79.4894>>, <<5360.3911, -5161.3555, 88.8130>>, TRUE, TRUE, TRUE, TRUE)	//Fence (Left)
		ADD_COVER_BLOCKING_AREA(<<5366.0146, -5162.6812, 81.0458>>, <<5274.0205, -5152.0400, 89.9633>>, TRUE, TRUE, TRUE, TRUE)	//Fence (Side)
		ADD_COVER_BLOCKING_AREA(<<5397.5415, -5174.0562, 79.0350>>, <<5373.8130, -5160.8711, 87.0641>>, TRUE, TRUE, TRUE, TRUE)	//Trailer + Hay
		ADD_COVER_BLOCKING_AREA(<<5428.4150, -5193.0215, 76.6645>>, <<5411.4014, -5185.2822, 82.3215>>, TRUE, TRUE, TRUE, TRUE)	//Hay Bales
		//ADD_COVER_BLOCKING_AREA(<<5336.5498, -5180.3726, 80.5163>>, <<5317.1948, -5169.9785, 87.8767>>, TRUE, TRUE, TRUE)	//Rocks
		
		// RJM - These navmesh blocking areas prevent the AI from pathing within a zone.  We're using these to prevent the AI
		//  from running along the outside of the fence, and from running into the player's start position.
		iNavMeshBlockingID1 = 			ADD_NAVMESH_BLOCKING_OBJECT(<<5324.8066, -5130.8901, 75.4401>>, <<90.0, 70.0, 15.0>>, 0.0)	// Along long side of fence
		iNavMeshBlockingID2 =			ADD_NAVMESH_BLOCKING_OBJECT(<<5370.8306, -5156.0420, 80.3580>>, <<19.5, 50.0, 15.0>>, 0.0)	// On left side of gate (from start position)
		iNavMeshBlockingID3 = 			ADD_NAVMESH_BLOCKING_OBJECT(<<5370.3306, -5226.0420, 80.3580>>, <<15.0, 50.0, 15.0>>, 0.0)	// On right side of gate (from start position)
		iNavMeshBlockingID4 = 			ADD_NAVMESH_BLOCKING_OBJECT(<<5357.089844, -5164.394043, 82.905312>>, <<8.0, 5.0, 3.0>>, 0.0)	// On right side of gate (from start position)
		//iNavMeshBlockingNearGarage = 	ADD_NAVMESH_BLOCKING_OBJECT(<<5326.555176, -5183.244629, 86.795723>>, <<15.0, 51.5, 6.0>>, 0.0)	// On right side of gate (from start position)
		
		//Timers
		iTimerCopCars = GET_GAME_TIMER() + 7500
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, NULL, "PROCOP")
		
		SET_LABEL_AS_TRIGGERED(PRO_COVER1, FALSE)
		
		SET_PED_PRELOAD_VARIATION_DATA(playerPedTrevor, PED_COMP_HAIR, 7, 0)
		SET_PED_PRELOAD_VARIATION_DATA(playerPedTrevor, PED_COMP_BERD, 1, 0)
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedTrevor)
			SET_PED_POSITION(playerPedTrevor, <<5321.2480, -5188.4775, 82.5189>>, 271.9670)
			
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedMichael)
			SET_PED_POSITION(playerPedMichael, <<5323.1680, -5190.4590, 82.5189>>, 181.1772)
			
			CLEAR_PED_TASKS_IMMEDIATELY(pedBrad)
			SET_PED_POSITION(pedBrad, <<5323.2959, -5188.0449, 82.5189>>, 182.8223)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(playerPedID), <<5318.2, -5185.1, 83.7>>) - GET_ENTITY_HEADING(playerPedID))
			
			//Rayfire
			rfSecDoorExplosion = GET_RAYFIRE_MAP_OBJECT(<<5318.2, -5185.1, 83.7>>, 10.0, "des_prologue_door")
			
			WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
				rfSecDoorExplosion = GET_RAYFIRE_MAP_OBJECT(<<5318.2, -5185.1, 83.7>>, 10.0, "des_prologue_door")
				
				#IF IS_DEBUG_BUILD	PRINTLN("Getting Rayfire Map Object - des_prologue_door")	#ENDIF
			ENDWHILE
			
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) != RFMO_STATE_END
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion, RFMO_STATE_ENDING)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(objGarage)
				IF NOT IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
					PLAY_ENTITY_ANIM(objGarage, "GDoor_Open", sAnimDictMapObjects, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.25)
				ELSE
					SET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open", 0.0)
					SET_ENTITY_ANIM_SPEED(objGarage, sAnimDictMapObjects, "GDoor_Open", 1.0)
				ENDIF
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
//			IF bReplaySkip = TRUE
//				//Interior
//				UNPIN_INTERIOR(intDepot)
//				
//				CLEAR_PED_TASKS_IMMEDIATELY(playerPedTrevor)
//				
//				GIVE_WEAPON_TO_PED(playerPedTrevor, wtCarbineRifle, 500, TRUE)
//				
//				SET_CURRENT_PED_WEAPON(playerPedTrevor, wtCarbineRifle, TRUE)
//				
//				SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 1, 0)
//				SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 13, 0)
//				SET_PED_PROP_INDEX(playerPedTrevor, ANCHOR_EYES, 4)
//				
//				SET_PED_POSITION(playerPedTrevor, <<5332.1592, -5185.2871, 81.7894>>, 0.0, FALSE)
//				
//				TASK_PUT_PED_DIRECTLY_INTO_COVER(playerPedTrevor, <<5332.1592, -5185.2871, 81.7894>>, -1, FALSE, 1.0, TRUE, FALSE, covPoint[1])
//				
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedTrevor)
//				
//				WHILE NOT IS_PED_IN_COVER(playerPedTrevor)
//				OR IS_PED_GOING_INTO_COVER(playerPedTrevor)
//					IF GET_SCRIPT_TASK_STATUS(playerPedTrevor, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) != PERFORMING_TASK
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(playerPedTrevor, <<5332.1592, -5185.2871, 81.7894>>, -1, FALSE, 1.0, TRUE, FALSE)
//					ENDIF
//					
//					WAIT_WITH_DEATH_CHECKS(0)
//				ENDWHILE
//				
//				WAIT_WITH_DEATH_CHECKS(500)
//				
//				//Balaclava
//				SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
//				SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
//				CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
//				
//				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TRV_PRO_MASK_REMOVED, TRUE)
//				
//				iCutsceneStage = 3
//				
//				SET_LABEL_AS_TRIGGERED(PRO_COVER1, TRUE)
//				
//				bReplaySkip = FALSE
//			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_LABEL_BEEN_TRIGGERED(ClearShutterTasks)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneShutterSwitch)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneShutterSwitch) >= 0.95
				CLEAR_PED_TASKS(playerPedID)
				
				//Score
				PLAY_AUDIO(PROLOGUE_TEST_SHUTTER_OPEN_OS)
				
				SET_LABEL_AS_TRIGGERED(ClearShutterTasks, TRUE)
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_IN()
			IF DOES_ENTITY_EXIST(objGarage)
				IF (IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
				AND GET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open") = 1.0)
					IF IS_SCREEN_FADED_OUT()
						//Score
						PLAY_AUDIO(PROLOGUE_TEST_COP_GUNFIGHT)
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
						
						DO_SCREEN_FADE_IN(1000)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Request Cutscene Variations - pro_mcs_5
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAIR, 5, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAIR, 7, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_BERD, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Brad", pedBrad)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_HAIR, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_FEET, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Getaway_driver", pedGetaway)
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Cop_Driving_Car", pedCutscene[CopCarDriver])
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_COP_Shooting_car", pedCutscene[CopCarShotgun])
		ENDIF
		
		//Shootout Dialogue
		IF HAS_LABEL_BEEN_TRIGGERED(PRO_TEAM)
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CREATE_CONVERSATION_ADV(PRO_OffShoot, "PRO_OffShoot", CONV_PRIORITY_MEDIUM, TRUE, DO_NOT_DISPLAY_SUBTITLES)
			ENDIF
		ENDIF
		
		//Shootout Help
		IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_COVER1)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_ADV(PROHLP_COVER1, "PROHLP_COVER1")
			ENDIF
		ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_COVER2)
			IF (IS_PED_IN_COVER(playerPedID)
			AND NOT IS_PED_GOING_INTO_COVER(playerPedID)
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_COVER1"))
			OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_ADV(PROHLP_COVER2, "PROHLP_COVER2")
			ENDIF
		ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_FREEAIM3)
			IF (IS_PED_IN_COVER(playerPedID)
			AND NOT IS_PED_GOING_INTO_COVER(playerPedID)
			AND IS_PED_AIMING_FROM_COVER(playerPedID)
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_COVER2"))
			OR NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_ADV(PROHLP_FREEAIM3, "PROHLP_FREEAIM3")
			ENDIF
		ENDIF
		
		//Shootout God Text
		IF HAS_LABEL_BEEN_TRIGGERED(PRO_COVER1)
			IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_COVER1")
				PRINT_ADV(PRO_COP, "PRO_COP")
			ENDIF
		ENDIF
		
		//Trevor removes Balaclava (Take Cover)
		SWITCH iCutsceneStage
			CASE 0
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(playerPedTrevor)
				AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
					IF playerPedID = playerPedTrevor
					AND ((IS_PED_IN_COVER(playerPedID)
					AND NOT IS_PED_GOING_INTO_COVER(playerPedID))
					OR HAS_LABEL_BEEN_TRIGGERED(PRO_Advance_1))
//					AND IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())))
						WEAPON_TYPE wpCurrent
						
						GET_CURRENT_PED_WEAPON(playerPedID, wpCurrent)
						
						IF NOT IS_PED_RELOADING(playerPedID)
						AND wpCurrent = WEAPONTYPE_CARBINERIFLE
							IF IS_ENTITY_AT_COORD(playerPedID, <<5332.167480, -5185.253906, 83.809387>>, <<0.5, 0.5, 2.0>>)
							OR IS_ENTITY_AT_COORD(playerPedID, <<5332.145020, -5191.842773, 83.773270>>, <<0.5, 0.5, 2.0>>)
							OR IS_ENTITY_AT_COORD(playerPedID, <<5326.206055, -5185.222168, 83.793671>>, <<0.5, 0.5, 2.0>>)
							OR IS_ENTITY_AT_COORD(playerPedID, <<5326.216797, -5191.865234, 83.776054>>, <<0.5, 0.5, 2.0>>)
							OR IS_ENTITY_AT_COORD(playerPedID, <<5332.157227, -5178.680176, 83.872810>>, <<0.5, 0.5, 2.0>>)
							OR IS_ENTITY_AT_COORD(playerPedID, <<5332.795898, -5195.612305, 83.999756>>, <<0.5, 0.75, 2.0>>)
							OR IS_ENTITY_AT_COORD(playerPedID, <<5336.999023, -5178.898926, 83.838768>>, <<0.75, 2.25, 2.0>>)
							OR HAS_LABEL_BEEN_TRIGGERED(PRO_Advance_1)
								IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Advance_1)
									SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
								ELSE
									CLEAR_PED_TASKS(playerPedID)
								ENDIF
								
								WAIT_WITH_DEATH_CHECKS(100)
								
								IF NOT IS_PED_RELOADING(playerPedID)
								AND wpCurrent = WEAPONTYPE_CARBINERIFLE
									IF NOT HAS_PED_GOT_WEAPON(playerPedTrevor, wtCarbineRifle)
										GIVE_WEAPON_TO_PED(playerPedTrevor, wtCarbineRifle, 500, TRUE)
									ENDIF
									
									SET_CURRENT_PED_WEAPON(playerPedTrevor, wtCarbineRifle, TRUE)
									
									IF IS_PED_IN_COVER_FACING_LEFT(playerPedTrevor)
										bPillarCoverRight = FALSE
									ELSE
										bPillarCoverRight = TRUE
									ENDIF
									
									//Balaclava
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
										IF NOT DOES_ENTITY_EXIST(objBalaclava[TREVOR_BALACLAVA])
											objBalaclava[TREVOR_BALACLAVA] = CREATE_OBJECT(P_TREVOR_PROLOGE_BALLY_S, GET_ENTITY_COORDS(playerPedTrevor))
											SET_ENTITY_COLLISION(objBalaclava[TREVOR_BALACLAVA], FALSE)
											ATTACH_ENTITY_TO_ENTITY(objBalaclava[TREVOR_BALACLAVA], playerPedTrevor, GET_PED_BONE_INDEX(playerPedTrevor, BONETAG_HEAD), VECTOR_ZERO, VECTOR_ZERO)
										ENDIF
										
										IF NOT DOES_ENTITY_EXIST(objBalaclava[TREVOR_SKIMASK])
											objBalaclava[TREVOR_SKIMASK] = CREATE_OBJECT(P_TREV_SKI_MASK_S, GET_ENTITY_COORDS(playerPedTrevor))
											SET_ENTITY_COLLISION(objBalaclava[TREVOR_SKIMASK], FALSE)
											ATTACH_ENTITY_TO_ENTITY(objBalaclava[TREVOR_SKIMASK], playerPedTrevor, GET_PED_BONE_INDEX(playerPedTrevor, BONETAG_HEAD), VECTOR_ZERO, VECTOR_ZERO)
										ENDIF
									ENDIF
									
									ANIM_DATA none
									ANIM_DATA animDataBlend1
									animDataBlend1.type = APT_SINGLE_ANIM
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
										animDataBlend1.dictionary0 = sAnimDictPrologue6FirstPerson
									ELSE
										animDataBlend1.dictionary0 = sAnimDictPrologue6
									ENDIF
									animDataBlend1.filter = GET_HASH_KEY("NoMover_filter")
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
										animDataBlend1.ikFlags = AIK_USE_FP_ARM_LEFT
									ELSE
										animDataBlend1.ikFlags = AIK_DISABLE_ARM_IK
									ENDIF
									
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
										animDataBlend1.anim0 = "REMOVE_BALACLAVA"
										animDataBlend1.flags = AF_SECONDARY | AF_UPPERBODY
										TASK_SCRIPTED_ANIMATION(playerPedTrevor, animDataBlend1, none, none, INSTANT_BLEND_DURATION, NORMAL_BLEND_DURATION)
									ELSE
										//Cover behind pillar
										IF bPillarCoverRight = FALSE
											animDataBlend1.anim0 = "remove_balaclava_right"
											animDataBlend1.flags = AF_SECONDARY
											TASK_SCRIPTED_ANIMATION(playerPedTrevor, animDataBlend1, none, none, INSTANT_BLEND_DURATION, NORMAL_BLEND_DURATION)
											
											//TASK_PLAY_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right", INSTANT_BLEND_IN, FAST_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)	//, AF_EXTRACT_INITIAL_OFFSET)	// | AF_UPPERBODY | AF_SECONDARY)	//0.430
											
											PLAY_ENTITY_ANIM(objBalaclava[TREVOR_BALACLAVA], "remove_balaclava_right_balaclava", sAnimDictPrologue6, INSTANT_BLEND_IN, FALSE, TRUE)
											PLAY_ENTITY_ANIM(objBalaclava[TREVOR_SKIMASK], "remove_balaclava_right_skimask", sAnimDictPrologue6, INSTANT_BLEND_IN, FALSE, TRUE)
										ELSE
											IF HAS_LABEL_BEEN_TRIGGERED(PRO_Advance_1)
												animDataBlend1.anim0 = "remove_balaclava_right"
												animDataBlend1.flags = AF_UPPERBODY | AF_SECONDARY
												TASK_SCRIPTED_ANIMATION(playerPedTrevor, animDataBlend1, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
												
												//TASK_PLAY_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)	//, AF_EXTRACT_INITIAL_OFFSET)	// | AF_UPPERBODY | AF_SECONDARY)	//0.430
											ELSE
												animDataBlend1.anim0 = "remove_balaclava"
												animDataBlend1.flags = AF_SECONDARY
												TASK_SCRIPTED_ANIMATION(playerPedTrevor, animDataBlend1, none, none, INSTANT_BLEND_DURATION, NORMAL_BLEND_DURATION)
												
												//TASK_PLAY_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava", INSTANT_BLEND_IN, FAST_BLEND_OUT, -1, AF_SECONDARY, 0.0, FALSE, AIK_DISABLE_ARM_IK)	//, AF_EXTRACT_INITIAL_OFFSET)	// | AF_UPPERBODY | AF_SECONDARY)	//0.430
											ENDIF
											PLAY_ENTITY_ANIM(objBalaclava[TREVOR_BALACLAVA], "remove_balaclava_balaclava", sAnimDictPrologue6, INSTANT_BLEND_IN, FALSE, TRUE)
											PLAY_ENTITY_ANIM(objBalaclava[TREVOR_SKIMASK], "remove_balaclava_skimask", sAnimDictPrologue6, INSTANT_BLEND_IN, FALSE, TRUE)
										ENDIF
									ENDIF
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedTrevor)
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
										FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBalaclava[TREVOR_BALACLAVA])
										FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBalaclava[TREVOR_SKIMASK])
									ENDIF
									
									SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
									CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
									
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
										SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
										SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
									ENDIF
									
									SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TRV_PRO_MASK_REMOVED, TRUE)
									
									REPLAY_RECORD_BACK_FOR_TIME(2.5, 3.0, REPLAY_IMPORTANCE_HIGHEST)
									
									iMaskRemoveTimeout = GET_GAME_TIMER() + 5000	//Added for bug 1640970
									
									ADVANCE_CUTSCENE()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				//Disable Player Movement
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				
				IF IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava")
				OR IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right")
				OR IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA")
				OR GET_GAME_TIMER() > iMaskRemoveTimeout	//Added for bug 1640970
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava")
						AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava") > 0.480)
						OR (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right")
						AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right") > 0.480)
						OR (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA")
						AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA") > 0.480)
							CREATE_CONVERSATION_ADV(PRO_Unmask, "PRO_Unmask")
						ENDIF
					ENDIF
					
					IF (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava")
					AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava") > 0.488)
					OR (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right")
					AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right") > 0.488)
					OR (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA")
					AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA") > 0.488)
						SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
						SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 2
				//Disable Player Movement
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				
				IF NOT IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava")
				AND NOT IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right")
				AND NOT IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA")
				OR (IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava")
				AND GET_ENTITY_ANIM_CURRENT_TIME(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava") > 0.920) //0.936)
				OR IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right")
				OR IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6FirstPerson, "REMOVE_BALACLAVA")
				OR GET_GAME_TIMER() > iMaskRemoveTimeout	//Added for bug 1640970
					CLEAR_PED_SECONDARY_TASK(playerPedTrevor)
					
					IF DOES_ENTITY_EXIST(objBalaclava[TREVOR_BALACLAVA])
						DETACH_ENTITY(objBalaclava[TREVOR_BALACLAVA])
						FREEZE_ENTITY_POSITION(objBalaclava[TREVOR_BALACLAVA], FALSE)
						SET_ENTITY_ALPHA(objBalaclava[TREVOR_BALACLAVA], 0, TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objBalaclava[TREVOR_SKIMASK])
						DETACH_ENTITY(objBalaclava[TREVOR_SKIMASK])
						FREEZE_ENTITY_POSITION(objBalaclava[TREVOR_SKIMASK], FALSE)
						SET_ENTITY_ALPHA(objBalaclava[TREVOR_SKIMASK], 0, TRUE)
					ENDIF
					
					bRadar = TRUE
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SAFE_REMOVE_BLIP(blipCover)
					
					SETTIMERB(0)
					
					PRINT_ADV(PRO_COP, "PRO_COP")
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
					REMOVE_COVER_POINT(covPoint[1])
					
					ADVANCE_CUTSCENE()
				ENDIF
				
//				IF DOES_CAM_EXIST(camAnim)
//					IF IS_CAM_RENDERING(camAnim)
//						IF NOT IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava")
//						AND NOT IS_ENTITY_PLAYING_ANIM(playerPedTrevor, sAnimDictPrologue6, "remove_balaclava_right")
//							RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
//							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//						ENDIF
//					ENDIF
//				ENDIF
			BREAK
		ENDSWITCH
		
		INT i
		
		INT iCopsDead
		bFirstWaveCopsDead = TRUE
		iCopsDead = 0
		INT iClosestCop
		BOOL bCopInCarPark = FALSE
		
		REPEAT 9 i
			IF NOT IS_PED_INJURED(pedCop[i])
				bFirstWaveCopsDead = FALSE
				
				IF IS_ENTITY_AT_COORD(pedCop[i], <<5323.853516, -5194.200195, 93.518600>>, <<41.0, 36.0, 13.0>>)
					bCopInCarPark = TRUE
				ENDIF
			ELSE
				iCopsDead++
			ENDIF
		ENDREPEAT
		
		IF iCarPerFrame < COUNT_OF(vehCop) - 1
			iCarPerFrame++
		ELSE
			iCarPerFrame = 0
		ENDIF
		
		i = iCarPerFrame	//REPEAT COUNT_OF(vehCop) i
			IF NOT IS_ENTITY_DEAD(vehCop[i])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[i])
					IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehCop[i]))
					AND IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehCop[i]))
						STOP_PLAYBACK_RECORDED_VEHICLE(vehCop[i])
					ENDIF
				ENDIF
			ENDIF
		//ENDREPEAT
		
		IF iCopPerFrame < COUNT_OF(pedCop) - 1
			iCopPerFrame++
		ELSE
			iCopPerFrame = 0
		ENDIF
		
		i = iCopPerFrame	//REPEAT COUNT_OF(pedCop) i
			IF NOT IS_PED_INJURED(pedCop[i])
				IF (GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) = PERFORMING_TASK
				OR (GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				AND GET_SEQUENCE_PROGRESS(notPlayerPedID) = iSequenceBuddy
				AND NOT IS_ENTITY_AT_COORD(notPlayerPedID, vSequenceBuddy, <<1.0, 1.0, 2.0>>)))
				OR (GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) = PERFORMING_TASK
				OR (GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				AND GET_SEQUENCE_PROGRESS(pedBrad) = iSequenceBrad
				AND NOT IS_ENTITY_AT_COORD(pedBrad, vSequenceBrad, <<1.0, 1.0, 2.0>>)))
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(pedCop[i])) < 50.0
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), GET_ENTITY_COORDS(pedCop[i])) < 50.0
						iClosestCop = i	#IF IS_DEBUG_BUILD	PRINTLN("iClosestCop = ", iClosestCop)	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		//ENDREPEAT
		
		REPEAT COUNT_OF(pedCop) i
			IF NOT IS_PED_INJURED(pedCop[i])
//				IF IS_PED_IN_ANY_VEHICLE(pedCop[i], TRUE)
//				AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(pedCop[i])
//					SET_ENTITY_INVINCIBLE(pedCop[i], TRUE)
//				ELSE
//					SET_ENTITY_INVINCIBLE(pedCop[i], FALSE)
//				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(pedCop[i], TRUE)
					SET_PED_RESET_FLAG(pedCop[i], PRF_ConsiderAsPlayerCoverThreatWithoutLOS, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(CopCarWave1)
		AND TIMERA() > 1000
			SET_FAKE_WANTED_LEVEL(5)
			
			// RJM - Only one cop car drives up initially
			FREEZE_ENTITY_POSITION(vehCop[1], FALSE)
			START_PLAYBACK_RECORDED_VEHICLE(vehCop[1], 004, sCarrec)
			SET_VEHICLE_SIREN(vehCop[1], TRUE)
			
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCop[1])
			
			// These cars stay back
			FREEZE_ENTITY_POSITION(vehCop[0], FALSE)
			START_PLAYBACK_RECORDED_VEHICLE(vehCop[0], 003, sCarrec)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehCop[0])
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCop[0])
			
			FREEZE_ENTITY_POSITION(vehCop[2], FALSE)
			START_PLAYBACK_RECORDED_VEHICLE(vehCop[2], 005, sCarrec)
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehCop[2])
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCop[2])
			
			SET_LABEL_AS_TRIGGERED(CopCarWave1, TRUE)
		ENDIF
		
		// RJM - These are the 2 cop cars that drive in on the left and right
		IF NOT HAS_LABEL_BEEN_TRIGGERED(CopCarWave2)
			IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5334.1455, -5195.7168, 80.3420>>, <<5324.5811, -5173.1362, 87.0081>>, 10.0)
			OR GET_GAME_TIMER() > iTimerCopCars
				// Bring in Cars 2 and 3
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehCop[0])
				SET_VEHICLE_SIREN(vehCop[0], TRUE)
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehCop[2])
				SET_VEHICLE_SIREN(vehCop[2], TRUE)
				
				//Have the guard shack guy run up
				IF NOT IS_PED_INJURED(pedCop[6])
//					OPEN_SEQUENCE_TASK(seqMain)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//						TASK_PAUSE(NULL, 2500)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<5339.8594, -5174.4546, 81.8192>>), PEDMOVE_SPRINT)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//						TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, (<<5339.8594, -5174.4546, 81.8192>>), 10000, TRUE, 0, FALSE, FALSE)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//					CLOSE_SEQUENCE_TASK(seqMain)
//					TASK_PERFORM_SEQUENCE(pedCop[6], seqMain)
//					CLEAR_SEQUENCE_TASK(seqMain)
					
					//SET_COMBAT_FLOAT(pedCop[6], CCF_STRAFE_WHEN_MOVING_CHANCE, 0.0)
					
					SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[6], <<5366.3364, -5184.4507, 81.5742>>, 2.0, TRUE)
					
					//SET_PED_COMBAT_ATTRIBUTES(pedCop[6], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
					
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[6], 100.0)
				ENDIF
				
				//Have one cop run up to the parked car on the left
				IF NOT IS_PED_INJURED(pedCop[8])
//					OPEN_SEQUENCE_TASK(seqMain)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//						TASK_PAUSE(NULL, 3500)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<5356.7480, -5178.4180, 81.7719>>), PEDMOVE_SPRINT)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//					CLOSE_SEQUENCE_TASK(seqMain)
//					TASK_PERFORM_SEQUENCE(pedCop[8], seqMain)
//					CLEAR_SEQUENCE_TASK(seqMain)
					
					//SET_COMBAT_FLOAT(pedCop[8], CCF_STRAFE_WHEN_MOVING_CHANCE, 0.0)
					
					//SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[8], <<5356.7480, -5178.4180, 81.7719>>, 10.0, TRUE)
					
					//SET_PED_COMBAT_ATTRIBUTES(pedCop[8], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
					
					SET_PED_COMBAT_MOVEMENT(pedCop[8], CM_WILLADVANCE)
					
					SET_PED_COMBAT_ATTRIBUTES(pedCop[8], CA_CAN_CHARGE, TRUE)
					
					SET_PED_CONFIG_FLAG(pedCop[8], PCF_ShouldChargeNow, TRUE)
					
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[8], 100.0)
				ENDIF
				
				SET_LABEL_AS_TRIGGERED(CopCarWave2, TRUE)
			ENDIF
		ELSE
			///Cop will exit his defensive area if the player gets too close or 5 other cops die
			IF NOT IS_PED_INJURED(pedCop[6])
				IF NOT HAS_LABEL_BEEN_TRIGGERED(AdvanceCop6)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(pedCop[6])) < 10.0
					OR GET_ENTITY_HEALTH(pedCop[6]) < GET_PED_MAX_HEALTH(pedCop[6])
					OR iCopsDead >= 3
						REMOVE_PED_DEFENSIVE_AREA(pedCop[6])
						
						SET_PED_COMBAT_MOVEMENT(pedCop[6], CM_WILLADVANCE)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[6], CA_CAN_CHARGE, TRUE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[6], 100.0)
						
						SET_LABEL_AS_TRIGGERED(AdvanceCop6, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//Cop will exit his defensive area if the player gets too close
			IF NOT IS_ENTITY_DEAD(pedCop[0])
				IF NOT HAS_LABEL_BEEN_TRIGGERED(AdvanceCop0)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(pedCop[0])) < 8.0
						REMOVE_PED_DEFENSIVE_AREA(pedCop[0])
						
						SET_LABEL_AS_TRIGGERED(AdvanceCop0, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			// Once Wave 2 has been triggered, try to bring in Wave 3
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopCarWave3)
				IF (iCopsDead > 3
				AND ((IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[0]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrec) / 100) * 95)
				OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0]))
				AND ((IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[2]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(005, sCarrec) / 100) * 95)
				OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])))
				OR IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5354.344727,-5188.610840,87.812073>>, <<5367.501465,-5188.862793,79.542175>>, 70.0)
//					// One cop runs up to the cop car next to the parked car
//					IF DOES_ENTITY_EXIST(pedCop[9]) AND NOT IS_ENTITY_DEAD(pedCop[9])
//						OPEN_SEQUENCE_TASK(seqMain)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//							TASK_PAUSE(NULL, 740)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<5353.5913, -5183.7612, 81.7617>>), PEDMOVE_SPRINT)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//						CLOSE_SEQUENCE_TASK(seqMain)
//						TASK_PERFORM_SEQUENCE(pedCop[9], seqMain)
//						CLEAR_SEQUENCE_TASK(seqMain)
//					ENDIF
//					
//					// One cop runs to the cop SUV on the right
//					IF DOES_ENTITY_EXIST(pedCop[10]) AND NOT IS_ENTITY_DEAD(pedCop[10])
//						OPEN_SEQUENCE_TASK(seqMain)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//							TASK_PAUSE(NULL, 1243)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<5354.7700, -5195.4731, 81.7732>>), PEDMOVE_SPRINT)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//						CLOSE_SEQUENCE_TASK(seqMain)
//						TASK_PERFORM_SEQUENCE(pedCop[10], seqMain)
//						CLEAR_SEQUENCE_TASK(seqMain)
//					ENDIF
					
					SET_LABEL_AS_TRIGGERED(CopCarWave3, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		// Warning for the player if he abandons the allies in the parking lot
		IF NOT bFirstWaveCopsDead
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PlayLotWarning)
				IF NOT IS_ENTITY_AT_COORD(playerPedID, <<5336.451660, -5185.199707, 92.828751>>, <<21.0, 20.0, 13.0>>)				
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					PRINT_ADV(PRO_TEAM, "PRO_TEAM", DEFAULT_GOD_TEXT_TIME, FALSE)
					
					SET_LABEL_AS_TRIGGERED(PlayLotWarning, TRUE)
				ENDIF
			ENDIF
			
			IF (NOT IS_ENTITY_AT_COORD(playerPedID, <<5323.853516, -5194.200195, 93.518600>>, <<41.0, 36.0, 13.0>>)
			AND bCopInCarPark = TRUE)
			OR IS_ENTITY_AT_COORD(playerPedID, <<5301.347168, -5180.395020, 84.018921>>, <<4.0, 3.0, 1.5>>)
				eMissionFail = failAbandonedCrew
				missionFailed()
			ENDIF
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED(FirstWaveDead)
				//Score
				LOAD_AUDIO(PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE)
				
				PLAY_AUDIO(PROLOGUE_TEST_COP_GUNFIGHT_PROGRESS)
				
				SET_LABEL_AS_TRIGGERED(FirstWaveDead, TRUE)
			ENDIF
			
			IF IS_ENTITY_AT_COORD(playerPedID, <<5301.347168, -5180.395020, 84.018921>>, <<4.0, 3.0, 1.5>>)
				eMissionFail = failAbandonedCrew
				missionFailed()
			ENDIF
		ENDIF
		
		IF iSequenceBuddy <> -1
			IF GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				iSequenceBuddy = -1
			ENDIF
		ENDIF
		
		IF iSequenceBrad <> -1
			IF GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				iSequenceBrad = -1
			ENDIF
		ENDIF
		
		//Buddy Behaviour
		UPDATE_ALLIES_TASKING()
		
		IF (GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) = PERFORMING_TASK
		OR (GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
		AND GET_SEQUENCE_PROGRESS(notPlayerPedID) = iSequenceBuddy
		AND NOT IS_ENTITY_AT_COORD(notPlayerPedID, vSequenceBuddy, <<1.0, 1.0, 2.0>>)))
			IF NOT IS_ENTITY_DEAD(pedCop[iClosestCop])
				SET_PED_SHOOTS_AT_COORD(notPlayerPedID, GET_ENTITY_COORDS(pedCop[iClosestCop]))
				
				IF GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	//Fix for bumping buddy without navmesh...
					IF IS_ENTITY_AT_COORD(playerPedID, GET_ENTITY_COORDS(notPlayerPedID) - <<0.0, 0.0, 0.25>>, <<1.0, 1.0, 2.0>>)
						CLEAR_PED_TASKS(notPlayerPedID)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(notPlayerPedID, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(notPlayerPedID, 500.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) = PERFORMING_TASK
		OR (GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
		AND GET_SEQUENCE_PROGRESS(pedBrad) = iSequenceBrad
		AND NOT IS_ENTITY_AT_COORD(pedBrad, vSequenceBrad, <<1.0, 1.0, 2.0>>)))
			IF NOT IS_ENTITY_DEAD(pedCop[iClosestCop])
				SET_PED_SHOOTS_AT_COORD(pedBrad, GET_ENTITY_COORDS(pedCop[iClosestCop]))
				
				IF GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	//Fix for bumping buddy without navmesh...
					IF IS_ENTITY_AT_COORD(playerPedID, GET_ENTITY_COORDS(pedBrad) - <<0.0, 0.0, 0.25>>, <<1.0, 1.0, 2.0>>)
						CLEAR_PED_TASKS(pedBrad)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedBrad, 500.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Car Park
		SWITCH iShootOutStage1
			CASE 0
				IF HAS_LABEL_BEEN_TRIGGERED(CopCarWave2)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop0)
						IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
						AND GET_TIME_POSITION_IN_RECORDING(vehCop[0]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrec) / 100) * 90)
						OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
						AND NOT IS_ENTITY_AT_COORD(vehCop[0], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
							SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[0], TRUE)
							
							SET_LABEL_AS_TRIGGERED(SirensVehCop0, TRUE)
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[0]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrec) / 100) * 90)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
					AND NOT IS_ENTITY_AT_COORD(vehCop[0], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT SAFE_IS_PED_DEAD(pedCop[0])
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CopsArrive0)
//								OPEN_SEQUENCE_TASK(seqMain)
//									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<5346.2217, -5178.8379, 81.7782>>, playerPedID, PEDMOVE_SPRINT, TRUE)
//									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//								CLOSE_SEQUENCE_TASK(seqMain)
//								TASK_PERFORM_SEQUENCE(pedCop[0], seqMain)
//								CLEAR_SEQUENCE_TASK(seqMain)
								
								SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[0], <<5344.2725, -5180.3062, 81.7773>>, 2.0, TRUE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[0], 100.0)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[0], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedCop[0], CA_LEAVE_VEHICLES, TRUE)
								
								SET_LABEL_AS_TRIGGERED(CopsArrive0, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[0]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[0])
					AND NOT IS_ENTITY_AT_COORD(vehCop[0], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT SAFE_IS_PED_DEAD(pedCop[1])
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CopsArrive1)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[1], FALSE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[1], 1000.0)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[1], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedCop[1], CA_LEAVE_VEHICLES, TRUE)
								
								SET_LABEL_AS_TRIGGERED(CopsArrive1, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(CopCarWave1)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop1)
						IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[1])
						AND GET_TIME_POSITION_IN_RECORDING(vehCop[1]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(004, sCarrec) / 100) * 90)
						OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[1])
						AND NOT IS_ENTITY_AT_COORD(vehCop[1], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(004, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
							SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[1], TRUE)
							SET_VEHICLE_SIREN(vehCop[1], FALSE)
							
							SET_LABEL_AS_TRIGGERED(SirensVehCop1, TRUE)
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[1])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[1]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(004, sCarrec) / 100) * 90)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[1])
					AND NOT IS_ENTITY_AT_COORD(vehCop[1], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(004, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT SAFE_IS_PED_DEAD(pedCop[2])
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CopsArrive2)
//								OPEN_SEQUENCE_TASK(seqMain)
//									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<5338.0200, -5195.3315, 82.2726>>, playerPedID, PEDMOVE_SPRINT, TRUE)
//									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//								CLOSE_SEQUENCE_TASK(seqMain)
//								TASK_PERFORM_SEQUENCE(pedCop[2], seqMain)
//								CLEAR_SEQUENCE_TASK(seqMain)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[2], 100.0)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_LEAVE_VEHICLES, TRUE)
								
								SET_LABEL_AS_TRIGGERED(CopsArrive2, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[1])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[1]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(004, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[1])
					AND NOT IS_ENTITY_AT_COORD(vehCop[1], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(004, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT SAFE_IS_PED_DEAD(pedCop[3])
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CopsArrive3)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[3], FALSE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[3], 1000.0)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[3], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedCop[3], CA_LEAVE_VEHICLES, TRUE)
								
								SET_LABEL_AS_TRIGGERED(CopsArrive3, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(CopCarWave2)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop2)
						IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
						AND GET_TIME_POSITION_IN_RECORDING(vehCop[2]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(005, sCarrec) / 100) * 90)
						OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
						AND NOT IS_ENTITY_AT_COORD(vehCop[2], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(005, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
							SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[2], TRUE)
							SET_VEHICLE_SIREN(vehCop[2], FALSE)
							
							SET_LABEL_AS_TRIGGERED(SirensVehCop2, TRUE)
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[2]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(005, sCarrec) / 100) * 90)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
					AND NOT IS_ENTITY_AT_COORD(vehCop[2], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(005, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT SAFE_IS_PED_DEAD(pedCop[4])
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CopsArrive4)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[4], FALSE)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[4], 1000.0)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[4], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedCop[4], CA_LEAVE_VEHICLES, TRUE)
								
								SET_LABEL_AS_TRIGGERED(CopsArrive4, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[2]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(005, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[2])
					AND NOT IS_ENTITY_AT_COORD(vehCop[2], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(005, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT SAFE_IS_PED_DEAD(pedCop[5])
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CopsArrive5)
//								OPEN_SEQUENCE_TASK(seqMain)
//									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<5353.8188, -5197.4165, 81.7737>>, playerPedID, PEDMOVE_SPRINT, TRUE)
//									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//									TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//								CLOSE_SEQUENCE_TASK(seqMain)
//								TASK_PERFORM_SEQUENCE(pedCop[5], seqMain)
//								CLEAR_SEQUENCE_TASK(seqMain)
								
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[5], 100.0)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[5], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(pedCop[5], CA_LEAVE_VEHICLES, TRUE)
								
								SET_LABEL_AS_TRIGGERED(CopsArrive5, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (SAFE_IS_PED_DEAD(pedCop[0])	OR NOT IS_PED_IN_VEHICLE(pedCop[0], vehCop[0]))
				AND (SAFE_IS_PED_DEAD(pedCop[1]) OR NOT IS_PED_IN_VEHICLE(pedCop[1], vehCop[0]))
				AND (SAFE_IS_PED_DEAD(pedCop[2]) OR NOT IS_PED_IN_VEHICLE(pedCop[2], vehCop[1]))
				AND (SAFE_IS_PED_DEAD(pedCop[3]) OR NOT IS_PED_IN_VEHICLE(pedCop[3], vehCop[1]))
				AND (SAFE_IS_PED_DEAD(pedCop[4]) OR NOT IS_PED_IN_VEHICLE(pedCop[4], vehCop[2]))
				AND (SAFE_IS_PED_DEAD(pedCop[5]) OR NOT IS_PED_IN_VEHICLE(pedCop[5], vehCop[2]))
					iShootOutStage1++
				ENDIF
			BREAK
			CASE 1
				IF bFirstWaveCopsDead = TRUE
					SAFE_REMOVE_BLIP(blipCover)
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						CREATE_CONVERSATION_ADV(PRO_RunToCar, "PRO_RunToCar", DEFAULT, FALSE)
						
						SET_LABEL_AS_TRIGGERED(PRO_Advance_1, TRUE)
						
						iShootOutStage1++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP_ADV(PROHLP_SPRINT, "PROHLP_SPRINT_KM")
						ELSE
							IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
								PRINT_HELP_ADV(PROHLP_SPRINT, "PROHLP_SPRINT_FP")
							ELSE
								PRINT_HELP_ADV(PROHLP_SPRINT, "PROHLP_SPRINT")
							ENDIF
						ENDIF
						
						iShootOutStage1++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) <> CAM_VIEW_MODE_FIRST_PERSON
						PRINT_HELP_ADV(PROHLP_RUNANDGUN, "PROHLP_RUNANDGUN")
						iShootOutStage1++
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		//Road
		SWITCH iShootOutStage2
			CASE 0
				IF bFirstWaveCopsDead = TRUE
				AND NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5338.454102, -5212.937988, 81.762024>>, <<5338.228027, -5161.654785, 86.762024>>, 35.0)
					//Allow the initial shootout vehicles to be destroyed...
					IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[0])
						IF GET_VEHICLE_ENGINE_HEALTH(vehCop[0]) < 500.0
							SET_VEHICLE_ENGINE_HEALTH(vehCop[0], 500.0)
						ENDIF
						
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[0]) < 500.0
							SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[0], 500.0)
						ENDIF
						
						SET_ENTITY_PROOFS(vehCop[0], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[1])
						IF GET_VEHICLE_ENGINE_HEALTH(vehCop[1]) < 500.0
							SET_VEHICLE_ENGINE_HEALTH(vehCop[1], 500.0)
						ENDIF
						
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[1]) < 500.0
							SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[1], 500.0)
						ENDIF
						
						SET_ENTITY_PROOFS(vehCop[1], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[2])
						IF GET_VEHICLE_ENGINE_HEALTH(vehCop[2]) < 500.0
							SET_VEHICLE_ENGINE_HEALTH(vehCop[2], 500.0)
						ENDIF
						
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[2]) < 500.0
							SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[2], 500.0)
						ENDIF
						
						SET_ENTITY_PROOFS(vehCop[2], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					IF NOT SAFE_IS_VEHICLE_DEAD(vehVan[0])
						IF GET_VEHICLE_ENGINE_HEALTH(vehVan[0]) < 500.0
							SET_VEHICLE_ENGINE_HEALTH(vehVan[0], 500.0)
						ENDIF
						
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehVan[0]) < 500.0
							SET_VEHICLE_PETROL_TANK_HEALTH(vehVan[0], 500.0)
						ENDIF
						
						SET_ENTITY_PROOFS(vehVan[0], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					IF NOT SAFE_IS_VEHICLE_DEAD(vehVan[1])
						IF GET_VEHICLE_ENGINE_HEALTH(vehVan[1]) < 500.0
							SET_VEHICLE_ENGINE_HEALTH(vehVan[1], 500.0)
						ENDIF
						
						IF GET_VEHICLE_PETROL_TANK_HEALTH(vehVan[1]) < 500.0
							SET_VEHICLE_PETROL_TANK_HEALTH(vehVan[1], 500.0)
						ENDIF
						
						SET_ENTITY_PROOFS(vehVan[1], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					SETTIMERB(0)
					
					iShootOutStage2++
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					CREATE_CONVERSATION_ADV(PRO_Combat2, "PRO_Combat2")
				ENDIF
				
				IF IS_PED_INJURED(pedCop[9])
				AND IS_PED_INJURED(pedCop[10])
					iShootOutStage2++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF iShootOutStage3 > 0
			IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop3)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[3])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[3]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(006, sCarrec) / 100) * 95)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[3])
				AND NOT IS_ENTITY_AT_COORD(vehCop[3], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(006, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[3], TRUE)
					
					SET_LABEL_AS_TRIGGERED(SirensVehCop3, TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop9)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[3])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[3]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(006, sCarrec) / 100) * 95)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[3])
				AND NOT IS_ENTITY_AT_COORD(vehCop[3], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(006, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[9])
//						OPEN_SEQUENCE_TASK(seqMain)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<5381.1787, -5183.8745, 80.5331>>, playerPedID, PEDMOVE_SPRINT, TRUE)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//						CLOSE_SEQUENCE_TASK(seqMain)
//						TASK_PERFORM_SEQUENCE(pedCop[9], seqMain)
//						CLEAR_SEQUENCE_TASK(seqMain)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[9], 100.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[9], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[9], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(pedCop9, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop10)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[3])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[3]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(006, sCarrec) / 100) * 99)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[3])
				AND NOT IS_ENTITY_AT_COORD(vehCop[3], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(006, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[10])
//						OPEN_SEQUENCE_TASK(seqMain)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<5381.2695, -5188.1006, 80.5138>>, playerPedID, PEDMOVE_SPRINT, TRUE)
//							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.0)
//						CLOSE_SEQUENCE_TASK(seqMain)
//						TASK_PERFORM_SEQUENCE(pedCop[10], seqMain)
//						CLEAR_SEQUENCE_TASK(seqMain)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[10], 100.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[10], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[10], CA_LEAVE_VEHICLES, TRUE)
						
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCop[10], TRUE)
						
						SET_PED_MAX_HEALTH(pedCop[10], 215)
						SET_ENTITY_HEALTH(pedCop[10], 215)
						
						SET_LABEL_AS_TRIGGERED(pedCop10, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iShootOutStage3 > 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				CREATE_CONVERSATION_ADV(PRO_Combat3, "PRO_Combat3")
			ENDIF
		ENDIF
		
		SWITCH iShootOutStage3
			CASE 0
				IF bFirstWaveCopsDead = TRUE
				OR IS_ENTITY_IN_ANGLED_AREA(playerPedTrevor, <<5364.964844, -5187.167969, 89.641083>>, <<5477.841797, -5189.211914, 75.912354>>, 100.0)
					FREEZE_ENTITY_POSITION(vehCop[3], FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehCop[3], 006, sCarrec)
					SET_VEHICLE_SIREN(vehCop[3], TRUE)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[3], 1500)
					
					iShootOutStage3++
				ENDIF
			BREAK
			CASE 1
				IF DOES_ENTITY_EXIST(vehCop[3]) AND DOES_ENTITY_EXIST(playerPedID)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[3])) <= 17.5
					OR (IS_PED_INJURED(pedCop[9])
					AND IS_PED_INJURED(pedCop[10]))
						SAFE_REMOVE_BLIP(blipCover)
						
						FREEZE_ENTITY_POSITION(vehCop[4], FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(vehCop[4], 008, sCarrec)
						SET_VEHICLE_SIREN(vehCop[4], TRUE)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[4], 3000)
						FREEZE_ENTITY_POSITION(vehCop[5], FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(vehCop[5], 009, sCarrec)
						SET_VEHICLE_SIREN(vehCop[5], TRUE)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[4], 2000)
						
						iShootOutStage3++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop4)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[4]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(008, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
					AND NOT IS_ENTITY_AT_COORD(vehCop[4], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(008, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[4], TRUE)
						SET_VEHICLE_SIREN(vehCop[4], FALSE)
						
						SET_LABEL_AS_TRIGGERED(SirensVehCop4, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop11)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[4]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(008, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
					AND NOT IS_ENTITY_AT_COORD(vehCop[4], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(008, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT IS_PED_INJURED(pedCop[11])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[11], FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[11], 1000.0)
							
							SET_PED_COMBAT_ATTRIBUTES(pedCop[11], CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[11], CA_LEAVE_VEHICLES, TRUE)
							
							SET_LABEL_AS_TRIGGERED(pedCop11, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop12)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[4]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(008, sCarrec) / 100) * 99)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
					AND NOT IS_ENTITY_AT_COORD(vehCop[4], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(008, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT IS_PED_INJURED(pedCop[12])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[12], FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[12], 1000.0)
							
							SET_PED_COMBAT_ATTRIBUTES(pedCop[12], CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[12], CA_LEAVE_VEHICLES, TRUE)
							
							SET_LABEL_AS_TRIGGERED(pedCop12, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop5)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[5]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(009, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					AND NOT IS_ENTITY_AT_COORD(vehCop[5], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(009, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[5], TRUE)
						
						SET_LABEL_AS_TRIGGERED(SirensVehCop5, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop13)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[5]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(009, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					AND NOT IS_ENTITY_AT_COORD(vehCop[5], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(009, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT IS_PED_INJURED(pedCop[13])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[13], FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[13], 1000.0)
							
							SET_PED_COMBAT_ATTRIBUTES(pedCop[13], CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[13], CA_LEAVE_VEHICLES, TRUE)
							
							SET_LABEL_AS_TRIGGERED(pedCop13, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop14)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[5]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(009, sCarrec) / 100) * 99)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					AND NOT IS_ENTITY_AT_COORD(vehCop[5], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(009, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT IS_PED_INJURED(pedCop[14])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[14], FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[14], 1000.0)
							
							SET_PED_COMBAT_ATTRIBUTES(pedCop[14], CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[14], CA_LEAVE_VEHICLES, TRUE)
							
							SET_LABEL_AS_TRIGGERED(pedCop14, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[4])
				AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[5])
					iShootOutStage3++
				ENDIF
			BREAK
			CASE 3
				IF IS_PED_INJURED(pedCop[9])
				AND IS_PED_INJURED(pedCop[10])
					SETTIMERA(0)
					
					iShootOutStage3++
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_ENTITY_DEAD(vehCop[4])
					REQUEST_SCRIPT_AUDIO_BANK("Prologue_Explosions_Cop_Car")
				ENDIF
				
//				IF (IS_PED_INJURED(pedCop[11])
//				AND IS_PED_INJURED(pedCop[12]))
//				OR TIMERA() > 5000
//					IF NOT IS_ENTITY_DEAD(vehCop[4])
//					AND (GET_ENTITY_HEALTH(vehCop[4]) < 1000
//					OR GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[4]) < 1000.0
//					OR GET_VEHICLE_ENGINE_HEALTH(vehCop[4]) < 1000.0)
//						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[4])) > 7.5
//						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCop[4])) > 7.5
//						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), GET_ENTITY_COORDS(vehCop[4])) > 7.5
//							EXPLODE_VEHICLE(vehCop[4], TRUE, TRUE)
//							
//							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[4])) < 10.0
//								SHAKE_GAMEPLAY_CAM("MEDIUM_EXPLOSION_SHAKE", 0.25)
//							ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[4])) >= 10.0
//								SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.25)
//							ENDIF
//							
//							IF sIDCopCarExplosion != -1
//								sIDCopCarExplosion = GET_SOUND_ID()
//								PLAY_SOUND_FROM_COORD(sIDCopCarExplosion, "COP_CAR_EXPLODES", GET_ENTITY_COORDS(vehCop[4], FALSE), "Prologue_Sounds")
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				//Sound
				IF sIDCopCarExplosion != -1
					IF HAS_SOUND_FINISHED(sIDCopCarExplosion)
						STOP_SOUND(sIDCopCarExplosion)
						RELEASE_SOUND_ID(sIDCopCarExplosion)
						sIDCopCarExplosion = -1
						
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Explosions_Cop_Car")
					ENDIF
				ENDIF
				
				IF (IS_PED_INJURED(pedCop[11])
				AND IS_PED_INJURED(pedCop[12])
				AND IS_PED_INJURED(pedCop[13])
				AND IS_PED_INJURED(pedCop[14]))
				OR IS_ENTITY_AT_COORD(playerPedID, <<5473.072266, -5128.806152, 80.067757>>, <<56.0, 43.0, 5.0>>)
					IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[3])
						SET_ENTITY_PROOFS(vehCop[3], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[4])
						SET_ENTITY_PROOFS(vehCop[4], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[5])
						SET_ENTITY_PROOFS(vehCop[5], FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
					
					//Score
					PLAY_AUDIO(PROLOGUE_TEST_HEAD_TO_GETAWAY_VEHICLE)
					
					SETTIMERA(0)
					
					FREEZE_ENTITY_POSITION(vehCop[6], FALSE)
					SET_VEHICLE_SIREN(vehCop[6], TRUE)
					START_PLAYBACK_RECORDED_VEHICLE(vehCop[6], 011, sCarrec)
					
//					FREEZE_ENTITY_POSITION(vehCop[6], FALSE)
//					SET_VEHICLE_SIREN(vehCop[6], TRUE)
//					START_PLAYBACK_RECORDED_VEHICLE(vehCop[6], 700, sCarrec)
					
					iShootOutStage3++
				ENDIF
			BREAK
			CASE 5
				IF (NOT IS_MESSAGE_BEING_DISPLAYED()
				AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0))
				OR TIMERA() > 10000	//Failsafe for dialogue hang
					PRINT_ADV(PRO_CAR1, "PRO_CAR1", DEFAULT_GOD_TEXT_TIME, FALSE)
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV(PRO_ToCar, "PRO_ToCar", DEFAULT, DEFAULT, DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
					
					SETTIMERA(0)
					
					iShootOutStage3++
				ENDIF
			BREAK
			CASE 6
				IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop6)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[6])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[6]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(011, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[6])
					AND NOT IS_ENTITY_AT_COORD(vehCop[6], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(011, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[6], TRUE)
						SET_VEHICLE_SIREN(vehCop[6], FALSE)
						
						SET_LABEL_AS_TRIGGERED(SirensVehCop6, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop15)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[6])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[6]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(011, sCarrec) / 100) * 95)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[6])
					AND NOT IS_ENTITY_AT_COORD(vehCop[6], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(011, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT IS_PED_INJURED(pedCop[15])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[15], FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[15], 1000.0)
							
							SET_PED_COMBAT_ATTRIBUTES(pedCop[15], CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[15], CA_LEAVE_VEHICLES, TRUE)
							
							SET_LABEL_AS_TRIGGERED(pedCop15, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(pedCop16)
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[6])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[6]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(011, sCarrec) / 100) * 99)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[6])
					AND NOT IS_ENTITY_AT_COORD(vehCop[6], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(011, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT IS_PED_INJURED(pedCop[16])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[16], FALSE)
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[16], 1000.0)
							
							SET_PED_COMBAT_ATTRIBUTES(pedCop[16], CA_USE_VEHICLE, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[16], CA_LEAVE_VEHICLES, TRUE)
							
							SET_LABEL_AS_TRIGGERED(pedCop16, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_INJURED(pedCop[15])
				AND IS_PED_INJURED(pedCop[16])
					iShootOutStage3++
					
					SETTIMERA(0)
				ENDIF
			BREAK
			CASE 7
				IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[6])
					IF GET_VEHICLE_ENGINE_HEALTH(vehCop[6]) < 500.0
						SET_VEHICLE_ENGINE_HEALTH(vehCop[6], 500.0)
					ENDIF
					
					IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[6]) < 500.0
						SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[6], 500.0)
					ENDIF
					
					SET_ENTITY_PROOFS(vehCop[6], FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					CREATE_CONVERSATION_ADV(PRO_RunToCar, "PRO_RunToCar", DEFAULT, FALSE)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			IF HAS_LABEL_BEEN_TRIGGERED(PRO_ToCar)
				CREATE_CONVERSATION_ADV(PRO_RespCar, "PRO_RespCar")
			ENDIF
		ENDIF
		
		IF iShootOutStage3 > 0
		OR iCutsceneStage = 4
			IF NOT sCamDetails.bRun
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_SWITCH6)
						IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5474.493164, -5165.288574, 72.709763>>, <<5364.621582, -5163.030273, 101.909111>>, 100.0)
							PRINT_HELP_ADV(PROHLP_SWITCH6, "PROHLP_SWITCH6")
						ENDIF
					ENDIF
				ENDIF
				
				IF UPDATE_SELECTOR_HUD(sSelectorPeds)     // Returns TRUE when the player has made a selection
					IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
						INFORM_MISSION_STATS_OF_INCREMENT(PRO_SWITCHES)
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_SWITCH6")
							SAFE_CLEAR_HELP()
						ENDIF
						
						sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
						sCamDetails.bRun = TRUE
						
//						IF NOT IS_PED_IN_COVER(notPlayerPedID)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(notPlayerPedID, 1000.0)
//						ENDIF
//						
//						IF NOT IS_PED_IN_COVER(playerPedID)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(playerPedID, 1000.0)
//						ENDIF
						
						IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
						OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ATTACK)
							SET_LABEL_AS_TRIGGERED(SkipAimIntro, TRUE)
						ENDIF
						
						SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
					ENDIF
				ENDIF
			ELSE
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)	//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 0.0, 0.0, SELECTOR_CAM_STRAIGHT_INTERP)	// Returns FALSE when the camera spline is complete
					IF sCamDetails.bOKToSwitchPed
						IF NOT sCamDetails.bPedSwitched
							IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
								UPDATE_PED_REFERENCES()
								
								IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5352.876953, -5184.861328, 80.784447>>, <<5322.009766, -5184.812012, 87.518822>>, 23.0)
									CLEAR_ROOM_FOR_ENTITY(playerPedID)
								ENDIF
								
								UPDATE_BUDDY_AMMO()
								
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
								ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
								
								//Update the relationship groups
								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
								ENDIF
								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
								ENDIF
								
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(playerPedMichael, pedBrad, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
								SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(playerPedTrevor, pedBrad, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
								
								SAFE_REMOVE_BLIP(blipMichael)
								SAFE_REMOVE_BLIP(blipTrevor)
								
								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
									
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
								ENDIF
								
								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
									
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
								ENDIF
								
								//Task the non-player to run to the cached player destination
								SET_PED_FIRING_PATTERN(notPlayerPedID, FIRING_PATTERN_SHORT_BURSTS)
								
								IF HAS_LABEL_BEEN_TRIGGERED(SkipAimIntro)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(notPlayerPedID, 1000.0)
								ELSE
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(notPlayerPedID, 1000.0, COMBAT_PED_DISABLE_AIM_INTRO)
								ENDIF
								
								SET_LABEL_AS_TRIGGERED(SkipAimIntro, FALSE)
								
								sCamDetails.bPedSwitched = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_GAMEPLAY_CAM_RENDERING()
			AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			AND GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK
				CLEAR_PED_TASKS(playerPedID)
			ENDIF
		ENDIF
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			IF GET_GAME_TIMER() > iDialogueTimer
				IF iDialogueLineCount[iDialogueStage] = -1 
					 iDialogueLineCount[iDialogueStage] = 6
				ELIF iDialogueLineCount[iDialogueStage] > 0
					iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000, 5000)
					
					iDialogueLineCount[iDialogueStage]--
					
					IF iDialogueStage = 0
						CREATE_CONVERSATION_ADV(PRO_CombatM, "PRO_CombatM", CONV_PRIORITY_MEDIUM, FALSE)
						iDialogueStage++
					ELIF iDialogueStage = 1
						CREATE_CONVERSATION_ADV(PRO_CombatT, "PRO_CombatT", CONV_PRIORITY_MEDIUM, FALSE)
						iDialogueStage++
					ELIF iDialogueStage = 2
						CREATE_CONVERSATION_ADV(PRO_CombatB, "PRO_CombatB", CONV_PRIORITY_MEDIUM, FALSE)
						iDialogueStage = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		INT iAmmoInClip
		WEAPON_TYPE wtCurrentWeapon
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_RELOAD)
			GET_CURRENT_PED_WEAPON(playerPedID, wtCurrentWeapon)
			GET_AMMO_IN_CLIP(playerPedID, wtCurrentWeapon, iAmmoInClip)
			
			IF GET_MAX_AMMO_IN_CLIP(playerPedID, wtCurrentWeapon) > 1
				IF iAmmoInClip <= (GET_MAX_AMMO_IN_CLIP(playerPedID, wtCurrentWeapon) / 5) * 4
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINT_HELP_ADV(PROHLP_RELOAD, "PROHLP_RELOAD")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_RELOAD")
			AND GET_GAME_TIMER() > iHelpTimer
				GET_CURRENT_PED_WEAPON(playerPedID, wtCurrentWeapon)
				GET_AMMO_IN_CLIP(playerPedID, wtCurrentWeapon, iAmmoInClip)
				
				IF iAmmoInClip > (GET_MAX_AMMO_IN_CLIP(playerPedID, wtCurrentWeapon) / 5) * 4
					SAFE_CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
		
		IF iShootOutStage3 > 5
			IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[3])
				IF GET_VEHICLE_ENGINE_HEALTH(vehCop[3]) < -100.0
					SET_VEHICLE_ENGINE_HEALTH(vehCop[3], -100.0)
				ENDIF
				
				IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[3]) < -100.0
					SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[3], -100.0)
				ENDIF
				
				SET_ENTITY_PROOFS(vehCop[3], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
			
			IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[4])
				IF GET_VEHICLE_ENGINE_HEALTH(vehCop[4]) < -100.0
					SET_VEHICLE_ENGINE_HEALTH(vehCop[4], -100.0)
				ENDIF
				
				IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[4]) < -100.0
					SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[4], -100.0)
				ENDIF
				
				SET_ENTITY_PROOFS(vehCop[4], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
			
			IF NOT SAFE_IS_VEHICLE_DEAD(vehCop[5])
				IF GET_VEHICLE_ENGINE_HEALTH(vehCop[5]) < -100.0
					SET_VEHICLE_ENGINE_HEALTH(vehCop[5], -100.0)
				ENDIF
				
				IF GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[5]) < -100.0
					SET_VEHICLE_PETROL_TANK_HEALTH(vehCop[5], -100.0)
				ENDIF
				
				SET_ENTITY_PROOFS(vehCop[5], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
		ENDIF
		
		IF (IS_ENTITY_AT_COORD(notPlayerPedID, <<5421.871094, -5115.145020, 80.353561>>, <<30.0, 30.0, 3.50>>)
		OR IS_ENTITY_AT_COORD(pedBrad, <<5421.871094, -5115.145020, 80.353561>>, <<30.0, 30.0, 3.50>>))
		OR IS_ENTITY_AT_COORD(playerPedID, <<5421.871094, -5115.145020, 80.353561>>, <<30.0, 30.0, 3.50>>)
			IF NOT HAS_LABEL_BEEN_TRIGGERED(Pullout)
				IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<5425.953613, -5116.230957, 76.609619>>, <<5434.420898, -5113.641602, 79.724915>>, 5.0)
				AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
					FREEZE_ENTITY_POSITION(vehCar, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehCar, 026, sCarrec)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCar, 500)
				ENDIF
				
				SET_LABEL_AS_TRIGGERED(Pullout, TRUE)
			ENDIF
		ENDIF
		
		//Fix for bug 1955672
		IF IS_ENTITY_AT_COORD(notPlayerPedID, <<5341.267090, -5218.448242, 83.837387>>, <<1.5, 7.5, 2.5>>)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(playerPedID)) > 50.0
				SET_PED_POSITION(notPlayerPedID, <<5343.7412, -5209.2568, 81.7604>>, 317.6852)
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		SET_GAME_PAUSED(FALSE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(EMPEROR3)
		
		RELEASE_PED_PRELOAD_VARIATION_DATA(playerPedTrevor)
		
		//Sound
		IF sIDCopCarExplosion != -1
			STOP_SOUND(sIDCopCarExplosion)
			RELEASE_SOUND_ID(sIDCopCarExplosion)
			sIDCopCarExplosion = -1
		ENDIF
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Explosions_Cop_Car")
		
		//Alarm
		IF IS_ALARM_PLAYING("PROLOGUE_VAULT_ALARMS")
			STOP_ALARM("PROLOGUE_VAULT_ALARMS", TRUE)
		ENDIF
		
		//Interior
		UNPIN_INTERIOR(intDepot)
		
		//Wanted
		SET_FAKE_WANTED_LEVEL(0)
		
		//Cover
		INT i
		
		REPEAT COUNT_OF(covPoint) i
			REMOVE_COVER_POINT(covPoint[i])
		ENDREPEAT
		
		//SAFE_REMOVE_BLIP(blipCar)
		SAFE_REMOVE_BLIP(blipCover)
		SAFE_REMOVE_BLIP(blipDestination)
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		SAFE_DELETE_OBJECT(objHiddenCollision)
		IF DOES_ENTITY_EXIST(objCupboardDoor)
			SET_OBJECT_AS_NO_LONGER_NEEDED(objCupboardDoor)	//SAFE_DELETE_OBJECT(objCupboardDoor)
		ENDIF
		SAFE_DELETE_OBJECT(objDebris)
		
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)
		
		REPEAT COUNT_OF(pedCop) i
			IF DOES_ENTITY_EXIST(pedCop[i])
				IF NOT IS_PED_INJURED(pedCop[i])
					DISABLE_PED_PAIN_AUDIO(pedCop[i], TRUE)
					STOP_PED_SPEAKING(pedCop[i], TRUE)
					SET_ENTITY_HEALTH(pedCop[i], 0)
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(pedCop[i])
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehCop) i
			IF NOT IS_ENTITY_DEAD(vehCop[i])
				SET_ENTITY_PROOFS(vehCop[i], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
		ENDREPEAT
		
		//Group
		IF IS_PED_IN_GROUP(notPlayerPedID)
			REMOVE_PED_FROM_GROUP(notPlayerPedID)
		ENDIF
		IF IS_PED_IN_GROUP(pedBrad)
			REMOVE_PED_FROM_GROUP(pedBrad)
		ENDIF
		
//		//Unload Texture Dictionary
//		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SplashScreens")
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC cutsceneGetAway()
	IF iCutsceneStage > 0
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF

	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Radar
		bRadar = TRUE
		
		//Blips
		SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
		
		//Car
		FREEZE_ENTITY_POSITION(vehCar, FALSE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(notPlayerPedID, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, TRUE)
		
		REMOVE_PED_DEFENSIVE_AREA(notPlayerPedID)
		REMOVE_PED_DEFENSIVE_AREA(notPlayerPedID, TRUE)
		REMOVE_PED_DEFENSIVE_AREA(pedBrad)
		REMOVE_PED_DEFENSIVE_AREA(pedBrad, TRUE)
		
		SET_PED_COMBAT_ATTRIBUTES(playerPedID, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(notPlayerPedID, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_LEAVE_VEHICLES, FALSE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("pro_mcs_5", CS_SECTION_1 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6)
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("pro_mcs_5", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6)
		ENDIF
		
		//Getaway path - path that Brad & Michael follow at the end on their way to the getaway car
		vPathToGetawayCar[0] = <<5425.2314, -5160.5508, 77.2905>>
		vPathToGetawayCar[1] = <<5431.3740, -5144.4941, 77.2632>>
		vPathToGetawayCar[2] = <<5428.2598, -5129.7495, 77.0241>>
		
		IF DOES_CAM_EXIST(camMain)
			DESTROY_CAM(camMain)	//Fix for bug 2280671
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			CLEAR_PED_TASKS_IMMEDIATELY(playerPedID)
			SET_PED_POSITION(playerPedID, <<5423.2051, -5118.9268, 76.9971>>, 38.6371)
			
			CLEAR_PED_TASKS_IMMEDIATELY(notPlayerPedID)
			SET_PED_POSITION(notPlayerPedID, <<5421.7095, -5116.1396, 77.0704>>, 192.0948)
			
			CLEAR_PED_TASKS_IMMEDIATELY(pedBrad)
			SET_PED_POSITION(pedBrad, <<5422.5332, -5115.9390, 77.0794>>, 192.0846)
			
			SET_LABEL_AS_TRIGGERED(ReplaySkipCops, TRUE)
			
			//Police Cars
			//	First wave of cars in the parking lot
			IF bReplaySkip = FALSE
				SET_VEHICLE_POSITION(vehCop[0], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(003, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(003, sCarrec), sCarrec), 90.0)
				SET_VEHICLE_POSITION(vehCop[1], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(004, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(004, sCarrec), sCarrec), 90.0)
				SET_VEHICLE_POSITION(vehCop[2], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(005, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(005, sCarrec), sCarrec), 90.0)
				SET_VEHICLE_POSITION(vehCop[3], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(008, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(008, sCarrec), sCarrec), 90.0)
				SET_VEHICLE_POSITION(vehCop[4], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(009, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(009, sCarrec), sCarrec), 90.0)
				SET_VEHICLE_POSITION(vehCop[5], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(700, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(011, sCarrec), sCarrec), 90.0)
				
				SET_VEHICLE_POSITION(vehCar, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(026, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(026, sCarrec), sCarrec), 105.0)
				SET_LABEL_AS_TRIGGERED(Pullout, TRUE)
				
				ACTIVATE_PHYSICS(vehCar)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
			ELSE
				WHILE NOT HAS_CUTSCENE_LOADED()
					//Request Cutscene Variations - pro_mcs_5
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAIR, 5, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL, 0, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2, 0, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAIR, 7, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_BERD, 1, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Brad", pedBrad)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_HAIR, 0, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_FEET, 0, 0)	
						SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_SPECIAL2, 0, 0)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Getaway_driver", pedGetaway)
						//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Cop_Driving_Car", pedCutscene[CopCarDriver])
						//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_COP_Shooting_car", pedCutscene[CopCarShotgun])
					ENDIF
					
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - pro_mcs_5
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAIR, 5, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAIR, 7, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_BERD, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Brad", pedBrad)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_HAIR, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_FEET, 0, 0)	
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Brad", PED_COMP_SPECIAL2, 0, 0)	
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Getaway_driver", pedGetaway)
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_Cop_Driving_Car", pedCutscene[CopCarDriver])
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("PRO_COP_Shooting_car", pedCutscene[CopCarShotgun])
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
//				IF NOT sCamDetails.bRun
//					IF UPDATE_SELECTOR_HUD(sSelectorPeds)     // Returns TRUE when the player has made a selection
//						IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
//							INFORM_MISSION_STATS_OF_INCREMENT(PRO_SWITCHES)
//							
//							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_SWITCH6")
//								SAFE_CLEAR_HELP()
//							ENDIF
//							
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun = TRUE
//							
//							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
//						ENDIF
//					ENDIF
//				ELSE
//					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails)	//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 0.0, 0.0, SELECTOR_CAM_STRAIGHT_INTERP)	// Returns FALSE when the camera spline is complete
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
//									ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
//									
//									//Update the relationship groups
//									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
//									ENDIF
//									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
//									ENDIF
//									
//									SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(playerPedMichael, pedBrad, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
//									SET_PED_DEFENSIVE_AREA_ATTACHED_TO_PED(playerPedTrevor, pedBrad, <<5.0, 0.0, 5.0>>, <<-5.0, 0.0, -5.0>>, 5.0, TRUE)
//									
//									SAFE_REMOVE_BLIP(blipMichael)
//									SAFE_REMOVE_BLIP(blipTrevor)
//									
//									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SAFE_ADD_BLIP_PED(blipMichael, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//										
//										SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
//									ENDIF
//									
//									IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//										
//										SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
//									ENDIF
//									
//									IF notPlayerPedID = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
//										IF NOT IS_PED_IN_VEHICLE(notPlayerPedID, vehCar)
//										AND NOT IS_PED_GETTING_INTO_A_VEHICLE(notPlayerPedID)
//											IF GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
//												CPRINTLN(DEBUG_MISSION, "RJM - PLAYER SWITCH - Tasking Michael to follow path and get in vehicle")
//												CLEAR_PED_TASKS(notPlayerPedID)
//												TASK_ENTER_VEHICLE(notPlayerPedID, vehCar, -1, VS_FRONT_RIGHT, PEDMOVE_RUN, ECF_USE_RIGHT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
//											ENDIF
//										ENDIF
//									ELIF notPlayerPedID = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
//										IF NOT IS_ENTITY_AT_COORD(notPlayerPedID, <<5423.0547, -5119.1548, 76.8883>>, <<2.0, 2.0, 3.0>>)
//											IF GET_SCRIPT_TASK_STATUS(notPlayerPedID, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
//												CPRINTLN(DEBUG_MISSION, "RJM - PLAYER SWITCH - Tasking Trevor to follow path near vehicle")
//												CLEAR_PED_TASKS(notPlayerPedID)
//												TASK_ENTER_VEHICLE(notPlayerPedID, vehCar, -1, VS_BACK_LEFT, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
//											ENDIF
//										ENDIF
//									ENDIF
//									
//									CLEAR_PED_TASKS(playerPedID)
//									
//									sCamDetails.bPedSwitched = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				// RJM - I wrapped this in a boolean check, because the AI was getting tasked every tick, which 
				// is not ideal.  I saw it was being done to catch tasking the the non-player character on player switch, but
				// I just moved that tasking to the player-switch handler below.
				IF notPlayerPedID = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
					IF NOT IS_PED_IN_VEHICLE(notPlayerPedID, vehCar)
					AND NOT IS_PED_GETTING_INTO_A_VEHICLE(notPlayerPedID)
						IF NOT HAS_LABEL_BEEN_TRIGGERED(TaskMichaelToVehicle)
							CLEAR_PED_TASKS(notPlayerPedID)
							#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "RJM - Tasking Michael to follow path and get in vehicle")	#ENDIF
							// New Path to the car, instead of the direct path, which often leads across the field,
							// we now have them run down the road
							OPEN_SEQUENCE_TASK(seqMain)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[0], GET_ENTITY_COORDS(vehCar))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[0], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
								ENDIF
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], GET_ENTITY_COORDS(vehCar))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[1], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
								ENDIF
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[2], GET_ENTITY_COORDS(vehCar))
								AND NOT HAS_LABEL_BEEN_TRIGGERED(RetaskBuddyEnterVehicle)
									//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[2], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vPathToGetawayCar[2], <<5447.468262, -5130.345215, 78.351570>>, PEDMOVE_RUN, TRUE, 0.5, 4.0, FALSE, ENAV_NO_STOPPING)
								ENDIF
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(<<5423.0547, -5119.1548, 77.0883>>, GET_ENTITY_COORDS(vehCar))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<5423.0547, -5119.1548, 77.0883>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT)
								ENDIF
								TASK_ENTER_VEHICLE(NULL, vehCar, -1, VS_FRONT_RIGHT, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(notPlayerPedID, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							SET_LABEL_AS_TRIGGERED(TaskMichaelToVehicle, TRUE)
						ENDIF
					ELSE
						SAFE_REMOVE_BLIP(blipMichael)
					ENDIF
				ELIF notPlayerPedID = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
					IF NOT IS_ENTITY_AT_COORD(notPlayerPedID, <<5423.0547, -5119.1548, 76.8883>>, <<2.0, 2.0, 3.0>>)
						IF NOT HAS_LABEL_BEEN_TRIGGERED(TaskTrevorToVehicle)
							CLEAR_PED_TASKS(notPlayerPedID)
							#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "RJM - Tasking Trevor to follow path near vehicle")	#ENDIF
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_AIM_GUN_AT_COORD(NULL, vPathToGetawayCar[1], 1500)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[0], GET_ENTITY_COORDS(vehCar))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[0], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
								ENDIF
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], GET_ENTITY_COORDS(vehCar))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[1], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
								ENDIF
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[2], GET_ENTITY_COORDS(vehCar))
								AND NOT HAS_LABEL_BEEN_TRIGGERED(RetaskBuddyEnterVehicle)
									//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[2], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vPathToGetawayCar[2], <<5448.062988, -5123.283691, 78.314911>>, PEDMOVE_RUN, TRUE, 0.5, 4.0, FALSE, ENAV_NO_STOPPING)
								ENDIF
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(<<5423.0547, -5119.1548, 77.0883>>, GET_ENTITY_COORDS(vehCar))
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<5423.0547, -5119.1548, 77.0883>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT)
								ENDIF
								TASK_ENTER_VEHICLE(NULL, vehCar, -1, VS_BACK_LEFT, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(notPlayerPedID, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							SET_LABEL_AS_TRIGGERED(TaskTrevorToVehicle, TRUE)
						ENDIF
					ELSE
						SAFE_REMOVE_BLIP(blipTrevor)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(pedBrad, vehCar)
				AND NOT IS_PED_GETTING_INTO_A_VEHICLE(pedBrad)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(TaskBradToVehicle)
						CLEAR_PED_TASKS(pedBrad)
						#IF IS_DEBUG_BUILD	CPRINTLN(DEBUG_MISSION, "RJM - Tasking Brad to follow path and get in vehicle")	#ENDIF
						OPEN_SEQUENCE_TASK(seqMain)
							// New Path to the car, instead of the direct path, which often leads across the field,
							// we now have them run down the road
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[0], GET_ENTITY_COORDS(vehCar))
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[0], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
							ENDIF
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(<<5435.0527, -5140.2559, 77.1114>>, GET_ENTITY_COORDS(vehCar))
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<5435.0527, -5140.2559, 77.1114>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
							ENDIF
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), GET_ENTITY_COORDS(vehCar)) > GET_DISTANCE_BETWEEN_COORDS(<<5427.9502, -5122.7476, 77.0983>>, GET_ENTITY_COORDS(vehCar))
							AND NOT HAS_LABEL_BEEN_TRIGGERED(RetaskBuddyEnterVehicle)
								//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPathToGetawayCar[2], PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP * 2)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<5427.9502, -5122.7476, 77.0983>>, <<5448.062988, -5123.283691, 78.314911>>, PEDMOVE_RUN, TRUE, 0.5, 4.0, FALSE, ENAV_NO_STOPPING)
							ENDIF
							TASK_ENTER_VEHICLE(NULL, vehCar, -1, VS_BACK_RIGHT, PEDMOVE_RUN, ECF_USE_RIGHT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedBrad, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
						SET_LABEL_AS_TRIGGERED(TaskBradToVehicle, TRUE)
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipBuddy)
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(TaskMichaelToVehicle)
				OR HAS_LABEL_BEEN_TRIGGERED(TaskTrevorToVehicle)
				OR HAS_LABEL_BEEN_TRIGGERED(TaskBradToVehicle)
					IF HAS_LABEL_BEEN_TRIGGERED(FinalCar1Driver)
						IF (IS_PED_SHOOTING(notPlayerPedID) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 7)
						OR (IS_PED_SHOOTING(pedBrad) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 7)
							IF NOT IS_PED_INJURED(pedCop[17])
								APPLY_DAMAGE_TO_PED(pedCop[17], GET_ENTITY_HEALTH(pedCop[17]) + 100, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED(FinalCar1Passenger)
						IF (IS_PED_SHOOTING(notPlayerPedID) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 5)
						OR (IS_PED_SHOOTING(pedBrad) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 5)
							IF NOT IS_PED_INJURED(pedCop[18])
								APPLY_DAMAGE_TO_PED(pedCop[18], GET_ENTITY_HEALTH(pedCop[18]) + 100, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED(FinalCar2Driver)
						IF (IS_PED_SHOOTING(notPlayerPedID) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 4)
						OR (IS_PED_SHOOTING(pedBrad) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 4)
							IF NOT IS_PED_INJURED(pedCop[19])
								APPLY_DAMAGE_TO_PED(pedCop[19], GET_ENTITY_HEALTH(pedCop[19]) + 100, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED(FinalCar2Passenger)
						IF (IS_PED_SHOOTING(notPlayerPedID) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(notPlayerPedID), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 3)
						OR (IS_PED_SHOOTING(pedBrad) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedBrad), vPathToGetawayCar[2]) < (GET_DISTANCE_BETWEEN_COORDS(vPathToGetawayCar[1], vPathToGetawayCar[2]) / 10) * 3)
							IF NOT IS_PED_INJURED(pedCop[20])
								APPLY_DAMAGE_TO_PED(pedCop[20], GET_ENTITY_HEALTH(pedCop[20]) + 100, TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED(RetaskBuddyEnterVehicle)
						IF ((DOES_ENTITY_EXIST(pedCop[17])
						AND IS_PED_INJURED(pedCop[17]))
						AND (DOES_ENTITY_EXIST(pedCop[18])
						AND IS_PED_INJURED(pedCop[18]))
						AND (DOES_ENTITY_EXIST(pedCop[19])
						AND IS_PED_INJURED(pedCop[19])))
//						AND (DOES_ENTITY_EXIST(pedCop[20])
//						AND NOT IS_PED_INJURED(pedCop[20])))
							SET_LABEL_AS_TRIGGERED(TaskMichaelToVehicle, FALSE)
							SET_LABEL_AS_TRIGGERED(TaskTrevorToVehicle, FALSE)
							SET_LABEL_AS_TRIGGERED(TaskBradToVehicle, FALSE)
							
							SET_LABEL_AS_TRIGGERED(RetaskBuddyEnterVehicle, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(Pullout)
					IF (IS_ENTITY_AT_COORD(notPlayerPedID, <<5421.871094, -5115.145020, 80.353561>>, <<30.0, 30.0, 3.50>>)
					OR IS_ENTITY_AT_COORD(pedBrad, <<5421.871094, -5115.145020, 80.353561>>, <<30.0, 30.0, 3.50>>))
					OR IS_ENTITY_AT_COORD(playerPedID, <<5421.871094, -5115.145020, 80.353561>>, <<30.0, 30.0, 3.50>>)
						IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<5425.953613, -5116.230957, 76.609619>>, <<5434.420898, -5113.641602, 79.724915>>, 5.0)
						AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
							START_PLAYBACK_RECORDED_VEHICLE(vehCar, 026, sCarrec)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCar, 500)
						ENDIF
						
						SET_LABEL_AS_TRIGGERED(Pullout, TRUE)
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_GetIn_1)
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PLAY_PED_AMBIENT_SPEECH(pedBrad, "GET_OUT_OF_HERE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							
							SET_LABEL_AS_TRIGGERED(PRO_GetIn_1, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TaskFinalCar)
					IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<5430.223633, -5157.383789, 86.300346>>, <<5437.865234, -5089.691406, 76.055397>>, 150.0)
					OR IS_ENTITY_IN_ANGLED_AREA(notPlayerPedID, <<5430.223633, -5157.383789, 86.300346>>, <<5437.865234, -5089.691406, 76.055397>>, 150.0)
					OR IS_ENTITY_IN_ANGLED_AREA(pedBrad, <<5430.223633, -5157.383789, 86.300346>>, <<5437.865234, -5089.691406, 76.055397>>, 150.0)
						SPAWN_VEHICLE(vehCop[7], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(701, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(701, 0.0, sCarrec)))
						SPAWN_VEHICLE(vehCop[8], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(702, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(702, 0.0, sCarrec)))
						
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[7], FALSE)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[8], FALSE)
						
						createCop(17)
						createCop(18)
						createCop(19)
						//createCop(20)
						
						IF DOES_ENTITY_EXIST(pedCop[17])
							SET_PED_INTO_VEHICLE(pedCop[17], vehCop[7], VS_DRIVER)
						ENDIF
						IF DOES_ENTITY_EXIST(pedCop[18])
							SET_PED_INTO_VEHICLE(pedCop[18], vehCop[7], VS_FRONT_RIGHT)
						ENDIF
						IF DOES_ENTITY_EXIST(pedCop[19])
							SET_PED_INTO_VEHICLE(pedCop[19], vehCop[8], VS_DRIVER)
						ENDIF
						IF DOES_ENTITY_EXIST(pedCop[20])
							SET_PED_INTO_VEHICLE(pedCop[20], vehCop[8], VS_FRONT_RIGHT)
						ENDIF
						
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(701, sCarrec)
							START_PLAYBACK_RECORDED_VEHICLE(vehCop[7], 701, sCarrec)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[7], 3000)
							SET_VEHICLE_SIREN(vehCop[7], TRUE)
						ENDIF
						
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(702, sCarrec)
							START_PLAYBACK_RECORDED_VEHICLE(vehCop[8], 702, sCarrec)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[8], 3000)
							SET_VEHICLE_SIREN(vehCop[8], TRUE)
						ENDIF
						
						IF HAS_LABEL_BEEN_TRIGGERED(ReplaySkipCops)
							SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehCop[7])
							SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehCop[8])
							
							IF NOT IS_PED_INJURED(pedCop[17])
								APPLY_DAMAGE_TO_PED(pedCop[17], GET_ENTITY_HEALTH(pedCop[17]) + 100, TRUE)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedCop[18])
								APPLY_DAMAGE_TO_PED(pedCop[18], GET_ENTITY_HEALTH(pedCop[18]) + 100, TRUE)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedCop[19])
								APPLY_DAMAGE_TO_PED(pedCop[19], GET_ENTITY_HEALTH(pedCop[19]) + 100, TRUE)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedCop[20])
								APPLY_DAMAGE_TO_PED(pedCop[20], GET_ENTITY_HEALTH(pedCop[20]) + 100, TRUE)
							ENDIF
						ENDIF
						
						SET_LABEL_AS_TRIGGERED(TaskFinalCar, TRUE)
					ENDIF
				ELSE
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[7]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(701, sCarrec) / 100) * 90)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
					AND NOT IS_ENTITY_AT_COORD(vehCop[7], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(701, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop7)
							IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
							AND GET_TIME_POSITION_IN_RECORDING(vehCop[7]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(701, sCarrec) / 100) * 95)
							OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
							AND NOT IS_ENTITY_AT_COORD(vehCop[7], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(701, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
								SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[7], TRUE)
								
								SET_LABEL_AS_TRIGGERED(SirensVehCop7, TRUE)
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED(FinalCar1Driver)
							IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
							AND GET_TIME_POSITION_IN_RECORDING(vehCop[7]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(701, sCarrec) / 100) * 90)
							OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
							AND NOT IS_ENTITY_AT_COORD(vehCop[7], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(701, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
								IF NOT IS_PED_INJURED(pedCop[17])
									SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[7], TRUE)
									
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[17], 100.0)
									
									SET_PED_COMBAT_ATTRIBUTES(pedCop[17], CA_USE_VEHICLE, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(pedCop[17], CA_LEAVE_VEHICLES, TRUE)
									
									SET_LABEL_AS_TRIGGERED(FinalCar1Driver, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED(FinalCar1Passenger)
							IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
							AND GET_TIME_POSITION_IN_RECORDING(vehCop[7]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(701, sCarrec) / 100) * 95)
							OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[7])
							AND NOT IS_ENTITY_AT_COORD(vehCop[7], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(701, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
								IF NOT IS_PED_INJURED(pedCop[18])
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[18], FALSE)
									
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[18], 1000.0)
									
									SET_PED_COMBAT_ATTRIBUTES(pedCop[18], CA_USE_VEHICLE, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(pedCop[18], CA_LEAVE_VEHICLES, TRUE)
									
									SET_LABEL_AS_TRIGGERED(FinalCar1Passenger, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
					AND GET_TIME_POSITION_IN_RECORDING(vehCop[8]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(702, sCarrec) / 100) * 90)
					OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
					AND NOT IS_ENTITY_AT_COORD(vehCop[8], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(702, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
						IF NOT HAS_LABEL_BEEN_TRIGGERED(SirensVehCop8)
							IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
							AND GET_TIME_POSITION_IN_RECORDING(vehCop[8]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(702, sCarrec) / 100) * 95)
							OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
							AND NOT IS_ENTITY_AT_COORD(vehCop[8], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(702, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
								SET_VEHICLE_HAS_MUTED_SIRENS(vehCop[8], TRUE)
								SET_VEHICLE_SIREN(vehCop[8], FALSE)
								
								SET_LABEL_AS_TRIGGERED(SirensVehCop8, TRUE)
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED(FinalCar2Driver)
							IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
							AND GET_TIME_POSITION_IN_RECORDING(vehCop[8]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(702, sCarrec) / 100) * 90)
							OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
							AND NOT IS_ENTITY_AT_COORD(vehCop[8], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(702, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
								IF NOT IS_PED_INJURED(pedCop[19])
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[19], FALSE)
									
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[19], 1000.0)
									
									SET_PED_COMBAT_ATTRIBUTES(pedCop[19], CA_USE_VEHICLE, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(pedCop[19], CA_LEAVE_VEHICLES, TRUE)
									
									SET_LABEL_AS_TRIGGERED(FinalCar2Driver, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED(FinalCar2Passenger)
							IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
							AND GET_TIME_POSITION_IN_RECORDING(vehCop[8]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(702, sCarrec) / 100) * 95)
							OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[8])
							AND NOT IS_ENTITY_AT_COORD(vehCop[8], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(702, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
								IF NOT IS_PED_INJURED(pedCop[20])
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[20], 100.0)
									
									SET_PED_COMBAT_ATTRIBUTES(pedCop[20], CA_USE_VEHICLE, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(pedCop[20], CA_LEAVE_VEHICLES, TRUE)
																		
									SET_LABEL_AS_TRIGGERED(FinalCar2Passenger, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				INT i
				
				IF iCarPerFrame < COUNT_OF(vehCop) - 1
					iCarPerFrame++
				ELSE
					iCarPerFrame = 0
				ENDIF
				
				i = iCarPerFrame	//REPEAT COUNT_OF(vehCop) i
					IF NOT IS_ENTITY_DEAD(vehCop[i])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[i])
							IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehCop[i]))
							AND IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehCop[i]))
								STOP_PLAYBACK_RECORDED_VEHICLE(vehCop[i])
							ENDIF
						ENDIF
					ENDIF
				//ENDREPEAT
				
//				IF iCopPerFrame < COUNT_OF(pedCop) - 1
//					iCopPerFrame++
//				ELSE
//					iCopPerFrame = 0
//				ENDIF
//				
//				i = iCopPerFrame	//REPEAT COUNT_OF(pedCop) i
//					IF NOT IS_PED_INJURED(pedCop[i])
//						IF IS_PED_IN_ANY_VEHICLE(pedCop[i], TRUE)
//						AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(pedCop[i])
//							SET_ENTITY_INVINCIBLE(pedCop[i], TRUE)
//						ELSE
//							SET_ENTITY_INVINCIBLE(pedCop[i], FALSE)
//						ENDIF
//					ENDIF
//				//ENDREPEAT
				
				REPEAT COUNT_OF(pedCop) i
					IF NOT IS_PED_INJURED(pedCop[i])
//						IF IS_PED_IN_ANY_VEHICLE(pedCop[i], TRUE)
//						AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(pedCop[i])
//							SET_ENTITY_INVINCIBLE(pedCop[i], TRUE)
//						ELSE
//							SET_ENTITY_INVINCIBLE(pedCop[i], FALSE)
//						ENDIF
						
						IF IS_PED_IN_ANY_VEHICLE(pedCop[i], TRUE)
							SET_PED_RESET_FLAG(pedCop[i], PRF_ConsiderAsPlayerCoverThreatWithoutLOS, TRUE)
						ENDIF
					ENDIF
				ENDREPEAT
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				SET_PED_RESET_FLAG(notPlayerPedID, PRF_UseTighterEnterVehicleSettings, TRUE)
				SET_PED_RESET_FLAG(pedBrad, PRF_UseTighterEnterVehicleSettings, TRUE)
				
				IF ((NOT IS_PED_INJURED(pedCop[17])
				OR NOT IS_PED_INJURED(pedCop[18])
				OR NOT IS_PED_INJURED(pedCop[19]))
				AND IS_ENTITY_AT_COORD(playerPedID, GET_ENTITY_COORDS(vehCar), <<5.0, 5.0, 3.0>>)
				AND NOT IS_ENTITY_AT_COORD(notPlayerPedID, GET_ENTITY_COORDS(vehCar), <<5.0, 5.0, 3.0>>)
				AND NOT IS_ENTITY_AT_COORD(pedBrad, GET_ENTITY_COORDS(vehCar), <<5.0, 5.0, 3.0>>))
					PRINT_ADV(PRO_CARCREW, "PRO_CARCREW")
				ENDIF
				
				IF IS_ENTITY_AT_COORD(playerPedID, GET_ENTITY_COORDS(vehCar), <<30.0, 30.0, 3.0>>)
					SET_FORCE_HD_VEHICLE(vehCar, TRUE)
				ENDIF
				
				IF (((DOES_ENTITY_EXIST(pedCop[17]) AND IS_PED_INJURED(pedCop[17])
				AND DOES_ENTITY_EXIST(pedCop[18]) AND IS_PED_INJURED(pedCop[18])
				AND DOES_ENTITY_EXIST(pedCop[19]) AND IS_PED_INJURED(pedCop[19]))
				OR ((IS_PED_IN_VEHICLE(notPlayerPedID, vehCar) OR IS_PED_GETTING_INTO_A_VEHICLE(notPlayerPedID))
				AND (IS_PED_IN_VEHICLE(pedBrad, vehCar) OR IS_PED_GETTING_INTO_A_VEHICLE(pedBrad))))
				AND IS_ENTITY_AT_COORD(playerPedID, GET_ENTITY_COORDS(vehCar), <<5.0, 5.0, 3.0>>)
				AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID()) AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID()))
				OR bReplaySkip = TRUE
					SET_FORCE_HD_VEHICLE(vehCar, TRUE)
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_CARCREW")
						CLEAR_PRINTS()
					ENDIF
					
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_CAR1")
						PRINT_HELP_FOREVER("PROHLP_CAR1")
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_GetIn_3)
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PLAY_PED_AMBIENT_SPEECH(pedBrad, "GET_IN_VEHICLE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							
							SET_LABEL_AS_TRIGGERED(PRO_GetIn_3, TRUE)
						ENDIF
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
						IF GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							CLEAR_PED_TASKS(playerPedID)
							IF playerPedID = playerPedMichael
								TASK_ENTER_VEHICLE(playerPedID, vehCar, -1, VS_FRONT_RIGHT, PEDMOVE_RUN, ECF_USE_RIGHT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
							ELIF playerPedID = playerPedTrevor
								TASK_ENTER_VEHICLE(playerPedID, vehCar, -1, VS_BACK_LEFT, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
							ENDIF
						ENDIF
					ENDIF
					
//					IF (IS_PED_IN_VEHICLE(playerPedMichael, vehCar)
//					OR (IS_PED_GETTING_INTO_A_VEHICLE(playerPedMichael)
//					/*AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehCar, SC_DOOR_FRONT_RIGHT) > 0.5*/))
//					AND (IS_PED_IN_VEHICLE(playerPedTrevor, vehCar)
//					OR (IS_PED_GETTING_INTO_A_VEHICLE(playerPedTrevor)
//					/*AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehCar, SC_DOOR_REAR_LEFT) > 0.5*/))
//					AND (IS_PED_IN_VEHICLE(pedBrad, vehCar)
//					OR (IS_PED_GETTING_INTO_A_VEHICLE(pedBrad)
//					/*AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehCar, SC_DOOR_REAR_RIGHT) > 0.5*/))
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
					OR bReplaySkip = TRUE
						IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
								TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, FALSE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
								UPDATE_PED_REFERENCES()
								
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
								
								SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ENDIF
							
							SET_ENTITY_INVINCIBLE(pedGetaway, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("SET_ENTITY_INVINCIBLE(pedGetaway, TRUE) = ", GET_ENTITY_HEALTH(pedGetaway))	#ENDIF
							SET_ENTITY_INVINCIBLE(playerPedMichael, TRUE)
							SET_ENTITY_INVINCIBLE(playerPedTrevor, TRUE)
							SET_ENTITY_INVINCIBLE(pedBrad, TRUE)
							
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
								IF GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehCar) = GET_VEHICLE_RECORDING_ID(026, sCarrec)
									SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
								ENDIF
							ENDIF
							
							SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
							
							//Audio Scene
							IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_POLICE_SHOOTOUT")
								STOP_AUDIO_SCENE("PROLOGUE_POLICE_SHOOTOUT")
							ENDIF
							
							CLEAR_TEXT()
							
							SET_VEHICLE_ENGINE_HEALTH(vehCar, 1000.0)
							SET_VEHICLE_PETROL_TANK_HEALTH(vehCar, 1000.0)
							
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
							ENDIF
							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							REGISTER_ENTITY_FOR_CUTSCENE(pedBrad, "Brad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(pedGetaway, "PRO_Getaway_driver", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "getaway_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							REGISTER_ENTITY_FOR_CUTSCENE(vehCutscene, "chase_cop_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, POLICEOLD2)
							REGISTER_ENTITY_FOR_CUTSCENE(pedCutscene[CopCarDriver], "PRO_Cop_Driving_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, S_M_M_SNOWCOP_01)
							REGISTER_ENTITY_FOR_CUTSCENE(pedCutscene[CopCarShotgun], "PRO_COP_Shooting_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, S_M_M_SNOWCOP_01)
							
//							objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(playerPedMichael, wtCarbineRifle)
//							
//							REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_MICHAEL], "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							
							objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(playerPedTrevor, wtCarbineRifle)
							
							REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							
							//Score
							LOAD_AUDIO(PROLOGUE_TEST_POLICE_CAR_CHASE_OS)
							
							PLAY_AUDIO(PROLOGUE_TEST_GETAWAY_CUTSCENE)
							
							START_CUTSCENE()
														
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGH)
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
							WAIT_WITH_DEATH_CHECKS(0)
							
							IF NOT IS_REPEAT_PLAY_ACTIVE()
								SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
							ENDIF
							
							//Fix car door/window
							SET_VEHICLE_DOOR_CONTROL(vehCar, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 0.0)
							FIX_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
							FIX_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_LEFT)
							
							//Brad Balaclava + Mask
							SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_HAIR, 0, 0)
							SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_FEET, 0, 0)
							
							//Balaclava
							SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_HAIR, 5, 0)
							SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL, 0, 0)
							
							//Balaclava
							SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
							SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
							SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
							CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
							
							IF bReplaySkip = TRUE
								IF IS_SCREEN_FADED_OUT()
									DO_SCREEN_FADE_IN(1000)
								ENDIF
								
								bReplaySkip = FALSE
							ENDIF
							
							ADVANCE_CUTSCENE()
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROHLP_CAR1")
					AND NOT IS_HELP_MESSAGE_FADING_OUT()
						SAFE_CLEAR_HELP()
					ENDIF
				ENDIF
			BREAK
			CASE 1
				SET_PED_RESET_FLAG(playerPedMichael, PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(playerPedTrevor, PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				SET_PED_RESET_FLAG(pedBrad, PRF_PreventGoingIntoShuntInVehicleState, TRUE)
				
				SET_FORCE_HD_VEHICLE(vehCar, TRUE)
				
				IF NOT DOES_ENTITY_EXIST(vehCutscene)
					ENTITY_INDEX entityCutscene
					entityCutscene = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("chase_cop_car")
					
					IF DOES_ENTITY_EXIST(entityCutscene)
						vehCutscene = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityCutscene)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehCutscene, "vehCutscene")	#ENDIF
						
						SET_VEHICLE_SIREN(vehCutscene, TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCutscene, TRUE)
						
						SET_VEHICLE_LIGHTS(vehCutscene, FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedCutscene[CopCarDriver])
					ENTITY_INDEX entityCutscene
					entityCutscene = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("PRO_Cop_Driving_Car")
					
					IF DOES_ENTITY_EXIST(entityCutscene)
						pedCutscene[CopCarDriver] = GET_PED_INDEX_FROM_ENTITY_INDEX(entityCutscene)	#IF IS_DEBUG_BUILD	SET_PED_NAME_DEBUG(pedCutscene[CopCarDriver], "pedCutscene[CopCarDriver]")	#ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedCutscene[CopCarShotgun])
					ENTITY_INDEX entityCutscene
					entityCutscene = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("PRO_COP_Shooting_car")
					
					IF DOES_ENTITY_EXIST(entityCutscene)
						pedCutscene[CopCarShotgun] = GET_PED_INDEX_FROM_ENTITY_INDEX(entityCutscene)	#IF IS_DEBUG_BUILD	SET_PED_NAME_DEBUG(pedCutscene[CopCarShotgun], "pedCutscene[CopCarShotgun]")	#ENDIF
					ENDIF
				ENDIF
				
				//Duffle Bag
				IF NOT HAS_LABEL_BEEN_TRIGGERED(CutsceneBagRemove)
					IF GET_CUTSCENE_TIME() > ROUND(11.000000 * 1000.0)
						SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 0, 0)
						
						SET_LABEL_AS_TRIGGERED(CutsceneBagRemove, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(WindowSmash1)
					IF GET_CUTSCENE_TIME() > ROUND(27.533335 * 1000.0)
						SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
						
						SET_LABEL_AS_TRIGGERED(WindowSmash1, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(WindowSmash2)
					IF GET_CUTSCENE_TIME() > ROUND(33.233334 * 1000.0)
						SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_LEFT)
						
						//APPLY_PED_BLOOD_SPECIFIC(pedGetaway, ENUM_TO_INT(PDZ_HEAD), 0.583, 0.669, 81.680, 1.0, 0, 0.0, "ShotgunLarge")
						APPLY_PED_BLOOD_SPECIFIC(playerPedMichael, ENUM_TO_INT(PDZ_HEAD), 0.600, 0.550, 0.000, 1.000, -1, 0.000, "Scripted_Ped_Splash_Back")
						
						SET_LABEL_AS_TRIGGERED(WindowSmash2, TRUE)
					ENDIF
				ENDIF
				
//				IF NOT bVideoRecording
//					IF GET_CUTSCENE_TIME() > ROUND(41.2400002 * 1000.0)
//					AND GET_CUTSCENE_TIME() < ROUND(44.500002 * 1000.0)
//						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
//						
//						bVideoRecording = TRUE
//					ENDIF
//				ENDIF
//				
//				IF bVideoRecording
//					IF GET_CUTSCENE_TIME() > ROUND(44.569000 * 1000.0)
//						REPLAY_STOP_EVENT()
//						
//						bVideoRecording = FALSE
//					ENDIF
//				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(WindowSmash3)
					IF GET_CUTSCENE_TIME() > ROUND(41.600002 * 1000.0)
						SMASH_VEHICLE_WINDOW(vehCar, SC_WINDSCREEN_FRONT)
						
						IF NOT IS_ENTITY_DEAD(vehCutscene)
							SMASH_VEHICLE_WINDOW(vehCutscene, SC_WINDSCREEN_REAR)
						ENDIF
						
						SET_LABEL_AS_TRIGGERED(WindowSmash3, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(WindowSmash4)
					IF GET_CUTSCENE_TIME() > ROUND(41.600002 * 1000.0)
						SMASH_VEHICLE_WINDOW(vehCar, SC_WINDSCREEN_REAR)
						
						SET_LABEL_AS_TRIGGERED(WindowSmash4, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_POLICE_CAR_CHASE_OS)
					IF GET_CUTSCENE_TIME() > ROUND(16.300000 * 1000.0)
						PLAY_AUDIO(PROLOGUE_TEST_POLICE_CAR_CHASE_OS)
						
						SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_POLICE_CAR_CHASE_OS, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_POLICE_CAR_CHASE)
					IF GET_CUTSCENE_TIME() > ROUND(23.000000 * 1000.0)
						PLAY_AUDIO(PROLOGUE_TEST_POLICE_CAR_CHASE)
						
						SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_POLICE_CAR_CHASE, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_POLICE_CAR_CRASH)
					IF GET_CUTSCENE_TIME() > ROUND(41.600002 * 1000.0)
						PLAY_AUDIO(PROLOGUE_TEST_POLICE_CAR_CRASH)
						
						SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_POLICE_CAR_CRASH, TRUE)
					ENDIF
				ENDIF
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					IF NOT HAS_LABEL_BEEN_TRIGGERED(FIRST_PERSON_SWITCH_CAM_ANIM)
						IF GET_CUTSCENE_TIME() > ROUND(44.600002 * 1000.0)
						AND HAS_CUTSCENE_CUT_THIS_FRAME()
							SET_LABEL_AS_TRIGGERED(FIRST_PERSON_SWITCH_CAM_ANIM, TRUE)
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED(FIRST_PERSON_SWITCH_CAM_ANIM)
						//Camera
						IF NOT DOES_CAM_EXIST(camAnim)
							camAnim = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
							
							sceneAnimatedFirstPersonCam = CREATE_SYNCHRONIZED_SCENE(VECTOR_ZERO, VECTOR_ZERO)	//<<0.0, 0.0, -0.25>>, VECTOR_ZERO)
							
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneAnimatedFirstPersonCam, vehCar, -1)
							
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneAnimatedFirstPersonCam, FALSE)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneAnimatedFirstPersonCam, FALSE)
							
							#IF IS_DEBUG_BUILD
							STRING sAnimName
							
							IF NOT bDebugSwitchCamAnimSide
								sAnimName = "leadout_action_rear_cam"
							ELSE
								sAnimName = "leadout_action_side_cam"
							ENDIF
							
							PRINTLN("sAnimName = ", sAnimName)
							#ENDIF
							
							PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneAnimatedFirstPersonCam, #IF IS_DEBUG_BUILD	sAnimName	#ENDIF	#IF NOT IS_DEBUG_BUILD	"leadout_action_rear_cam"	#ENDIF, sAnimDictPrologue_LeadOut)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED(FIRST_PERSON_SWITCH_CAM_FLASH)
							IF GET_CUTSCENE_TIME() > GET_CUTSCENE_TOTAL_DURATION() - 300
								ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
								
								PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								
								SET_LABEL_AS_TRIGGERED(FIRST_PERSON_SWITCH_CAM_FLASH, TRUE)
							ENDIF
						ENDIF
						
						BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
					ENDIF
				ENDIF
				
//				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
//					NEW_LOAD_SCENE_START_SPHERE(<<4029.51978, -5050.21680, 107.90446>>, 50.0)
//				ELSE
//					PRINTLN("IS_NEW_LOAD_SCENE_LOADED() = ", IS_NEW_LOAD_SCENE_LOADED())
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_Getaway_driver")
					//SET_PED_INTO_VEHICLE(pedGetaway, vehCar, VS_DRIVER)
					
					//SET_ENTITY_INVINCIBLE(pedGetaway, TRUE)	PRINTLN("SET_ENTITY_INVINCIBLE(pedGetaway, TRUE) = ", GET_ENTITY_HEALTH(pedGetaway))
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					CLEAR_PED_TASKS_IMMEDIATELY(playerPedMichael)
					
					SET_PED_INTO_VEHICLE(playerPedMichael, vehCar, VS_FRONT_RIGHT)
					
					GIVE_WEAPON_TO_PED(playerPedMichael, wtPistol, 350)
					
					SET_PED_INTO_VEHICLE(playerPedMichael, vehCar, VS_DRIVER)
					
					TASK_PLAY_ANIM(playerPedMichael, sAnimDictPrologue_LeadOut, "leadout_action_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
					
					GIVE_WEAPON_TO_PED(playerPedMichael, wtPistol, 350)
					
					//SET_ENTITY_INVINCIBLE(playerPedMichael, TRUE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
//					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
//						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
//						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
//						
//						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
//					ENDIF
					
					SET_PED_INTO_VEHICLE(playerPedTrevor, vehCar, VS_BACK_LEFT)
					
					TASK_PLAY_ANIM(playerPedTrevor, sAnimDictPrologue_LeadOut, "leadout_action_trevor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
					
					GIVE_WEAPON_TO_PED(playerPedTrevor, wtPistol, 350)
					GIVE_WEAPON_TO_PED(playerPedTrevor, WEAPONTYPE_MICROSMG, 350)
					
					//SET_ENTITY_INVINCIBLE(playerPedTrevor, TRUE)
				ENDIF
				
				
				// enveff snow brad 1950940 - Rob B
				ENTITY_INDEX entitySnowBrad
				entitySnowBrad = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Brad", CS_BRAD)
				IF NOT IS_ENTITY_DEAD(entitySnowBrad)
					SET_ENABLE_PED_ENVEFF_SCALE(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), TRUE)
					SET_PED_ENVEFF_SCALE(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), 0.6)
					//PRINTSTRING("set brad") printnl()
				ENDIF					
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Brad")
					SET_PED_INTO_VEHICLE(pedBrad, vehCar, VS_BACK_RIGHT)
					
					TASK_PLAY_ANIM(pedBrad, sAnimDictPrologue_LeadOut, "leadout_action_brad", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
					PLAY_FACIAL_ANIM(pedBrad, "leadout_action_brad_facial", sAnimDictPrologue_LeadOut)
					
					GIVE_WEAPON_TO_PED(pedBrad, wtPistol, INFINITE_AMMO)
					
					//SET_ENTITY_INVINCIBLE(pedBrad, TRUE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("getaway_car")
				
					REPLAY_STOP_EVENT()
				
					//SET_VEHICLE_POSITION(vehCar, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(031, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(031, 0.0, sCarrec)))
					
					SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
					
					SET_VEHICLE_DOORS_SHUT(vehCar)
					
					SET_VEHICLE_FORWARD_SPEED(vehCar, 30.0)
					
					//SET_ENTITY_INVINCIBLE(vehCar, TRUE)
					//SET_ENTITY_PROOFS(vehCar, TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDIF
				
				//Weapon
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_weapon")
//					GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_MICHAEL], playerPedMichael)
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
					GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_TREVOR], playerPedTrevor)
				ENDIF
				
				//Cops
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_Cop_Driving_Car")
					IF NOT IS_PED_INJURED(pedCutscene[CopCarDriver])
						IF DOES_ENTITY_EXIST(vehCutscene)
						AND IS_VEHICLE_DRIVEABLE(vehCutscene)
							SET_PED_INTO_VEHICLE(pedCutscene[CopCarDriver], vehCutscene, VS_DRIVER)
						ENDIF
						
						SET_PED_SUFFERS_CRITICAL_HITS(pedCutscene[CopCarDriver], FALSE)
						
						SET_PED_MAX_HEALTH(pedCutscene[CopCarDriver], 800)
						SET_ENTITY_HEALTH(pedCutscene[CopCarDriver], GET_PED_MAX_HEALTH(pedCutscene[CopCarDriver]))
						
						GIVE_WEAPON_TO_PED(pedCutscene[CopCarDriver], wtPistol, 350)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedCutscene[CopCarDriver], relGroupEnemy)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("PRO_COP_Shooting_car")
					IF NOT IS_PED_INJURED(pedCutscene[CopCarShotgun])
						IF DOES_ENTITY_EXIST(vehCutscene)
						AND IS_VEHICLE_DRIVEABLE(vehCutscene)
							SET_PED_INTO_VEHICLE(pedCutscene[CopCarShotgun], vehCutscene, VS_FRONT_RIGHT)
						ENDIF
						
						SET_PED_SUFFERS_CRITICAL_HITS(pedCutscene[CopCarShotgun], FALSE)
						
						SET_PED_MAX_HEALTH(pedCutscene[CopCarShotgun], 800)
						SET_ENTITY_HEALTH(pedCutscene[CopCarShotgun], GET_PED_MAX_HEALTH(pedCutscene[CopCarShotgun]))
						
						GIVE_WEAPON_TO_PED(pedCutscene[CopCarShotgun], wtPistol, 350)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedCutscene[CopCarShotgun], relGroupEnemy)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("chase_cop_car")
					IF DOES_ENTITY_EXIST(vehCutscene)
					AND IS_VEHICLE_DRIVEABLE(vehCutscene)
						SET_VEHICLE_ENGINE_ON(vehCutscene, TRUE, TRUE)
						
						SET_ENTITY_INVINCIBLE(vehCutscene, TRUE)
						SET_ENTITY_PROOFS(vehCutscene, TRUE, TRUE, TRUE, TRUE, TRUE)
						
						REMOVE_VEHICLE_WINDOW(vehCutscene, SC_WINDOW_FRONT_RIGHT)
					ENDIF
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					bCutsceneSkipped = TRUE
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					IF bCutsceneSkipped
						IF IS_MUSIC_ONESHOT_PLAYING()
							CANCEL_MUSIC_EVENT("PROLOGUE_TEST_POLICE_CAR_CHASE_OS")
						ENDIF
						
						LOAD_SCENE_ADV(<<4541.3379, -5082.6982, 109.5979>>, 100.0, TRUE)
						
						SET_VEHICLE_POSITION(vehCar, <<4541.3379, -5082.6982, 109.5979>>, 118.0337)
						
						SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
						
						SET_VEHICLE_DOORS_SHUT(vehCar)
						
						SET_VEHICLE_FORWARD_SPEED(vehCar, 20.0)
						
						DO_SCREEN_FADE_IN(1000)	#IF IS_DEBUG_BUILD	PRINTLN("DO_SCREEN_FADE_IN")	#ENDIF
					ENDIF
					
					bCutsceneSkipped = FALSE
					
					//TASK_COMBAT_HATED_TARGETS_AROUND_PED(notPlayerPedID, 100.0)
					//TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedBrad, 100.0)
					
					//IF DOES_ENTITY_EXIST(pedCutscene[CopCarShotgun])
					//AND NOT IS_PED_INJURED(pedCutscene[CopCarShotgun])
					//	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCutscene[CopCarShotgun], 100.0)
					//ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					//CREATE_CONVERSATION_ADV(PRO_CHASE, "PRO_CHASE")
					
					//PRINT_HELP_ADV(PROHLP_CARAIM, "PROHLP_CARAIM")
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
					
					ADVANCE_STAGE()	//ADVANCE_CUTSCENE()
				ENDIF
			BREAK
//			CASE 2
//				//SET_ENTITY_INVINCIBLE(playerPedID, TRUE)
//				//SET_ENTITY_PROOFS(playerPedID, TRUE, TRUE, TRUE, TRUE, TRUE)
//				
//				IF UPDATE_SELECTOR_HUD(sSelectorPeds)	//Returns TRUE when the player has made a selection
//					IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
//						INFORM_MISSION_STATS_OF_INCREMENT(PRO_SWITCHES)
//						
//						IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_CHASE")
//							KILL_FACE_TO_FACE_CONVERSATION()
//						ENDIF
//						
//						IF NOT IS_PED_IN_COVER(notPlayerPedID)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(notPlayerPedID, 1000.0)
//						ENDIF
//						
//						IF NOT IS_PED_IN_COVER(playerPedID)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(playerPedID, 1000.0)
//						ENDIF
//						
//						sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//						sCamDetails.bRun = TRUE
//						
//						FLOAT fRelativeHeading, fRelativePitch
//						
//						fRelativeHeading = FirstPersonRelativeHeadingSpam()
//						fRelativePitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
//						
//						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//						
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelativeHeading)
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(fRelativePitch + 2.0)	//Negate a random shift here
//						
//						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
//						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
//						
//						//Update the relationship groups
//						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
//						ENDIF
//						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//					AND GET_PED_IN_VEHICLE_SEAT(vehCar, VS_DRIVER) = playerPedMichael
//						STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
//						
//						ADVANCE_STAGE()
//					ENDIF
//				ENDIF
//				
//				IF IS_GAMEPLAY_CAM_RENDERING()
//				AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//				AND GET_SCRIPT_TASK_STATUS(playerPedID, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = PERFORMING_TASK
//					CLEAR_PED_TASKS(playerPedID)
//				ENDIF
//				
//				IF (DOES_ENTITY_EXIST(vehCutscene)
//				AND NOT IS_VEHICLE_DRIVEABLE(vehCutscene))
//				OR (DOES_ENTITY_EXIST(pedCutscene[CopCarDriver])
//				AND IS_PED_INJURED(pedCutscene[CopCarDriver]))
//					STOP_PLAYBACK_RECORDED_VEHICLE(vehCutscene)
//					
//					PLAY_AUDIO(PROLOGUE_TEST_POLICE_CAR_CRASH)
//					
//					ADVANCE_STAGE()
//				ENDIF
//				
//				IF (DOES_ENTITY_EXIST(vehCutscene)
//				AND IS_VEHICLE_DRIVEABLE(vehCutscene)
//				AND NOT IS_ENTITY_DEAD(vehCutscene))
//					IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCutscene)
//					AND GET_TIME_POSITION_IN_RECORDING(vehCutscene) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(031, sCarrec) / 100) * 99)
//						SET_ENTITY_INVINCIBLE(vehCutscene, FALSE)
//						SET_ENTITY_PROOFS(vehCutscene, FALSE, FALSE, FALSE, FALSE, FALSE)
//						
//						SET_VEHICLE_PETROL_TANK_HEALTH(vehCutscene, 0)
//						SET_VEHICLE_ENGINE_HEALTH(vehCutscene, 0)
//						SET_ENTITY_HEALTH(vehCutscene, 0)
//						
//						IF (DOES_ENTITY_EXIST(pedCutscene[CopCarDriver])
//						AND NOT IS_PED_INJURED(pedCutscene[CopCarDriver]))
//							SET_ENTITY_HEALTH(pedCutscene[CopCarDriver], 0)
//						ENDIF
//						
//						IF (DOES_ENTITY_EXIST(pedCutscene[CopCarShotgun])
//						AND NOT IS_PED_INJURED(pedCutscene[CopCarShotgun]))
//							SET_ENTITY_HEALTH(pedCutscene[CopCarShotgun], 0)
//						ENDIF
//						
//						PLAY_AUDIO(PROLOGUE_TEST_POLICE_CAR_CRASH)
//						
//						ADVANCE_STAGE()
//					ENDIF
//				ENDIF
//			BREAK
		ENDSWITCH
		
		IF iCutsceneStage > 0
			REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(4400.0 - 2500.0, -5100.0 - 1500.0, 4400.0 + 2500.0, -5100.0 + 1500.0)
		ENDIF
		
//		IF iCutsceneStage > 1
//		OR CAN_SET_EXIT_STATE_FOR_CAMERA()
//			IF NOT HAS_LABEL_BEEN_TRIGGERED(DriverDied)
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
//				AND GET_TIME_POSITION_IN_RECORDING(vehCar) > 8000.0
//					TASK_LEAVE_VEHICLE(pedGetaway, vehCar, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT)
//					
//					SET_LABEL_AS_TRIGGERED(DriverDied, TRUE)
//				ENDIF
//			ELIF IS_VEHICLE_SEAT_FREE(vehCar, VS_DRIVER)
//				IF GET_SCRIPT_TASK_STATUS(playerPedMichael, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, TRUE)
//					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(playerPedMichael, vehCar)
//				ENDIF
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(vehCutscene)
//				VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehCar, GET_ENTITY_COORDS(vehCutscene, FALSE))
//				//PRINTLN("vOffset = <<", vOffset.X, ", ", vOffset.Y, ", ", vOffset.Z, ">>")
//				
//				IF DOES_CAM_EXIST(camCinematic)
//					VECTOR vCamCinematic = GET_CAM_COORD(camCinematic)
//					VECTOR vTrack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<6.3020, 4.8240 - (7.5 + CLAMP(vOffset.Y, -7.0, 0.0)), 3.8557>>)
//					
//					vCamCinematic.X = vCamCinematic.X + ((vTrack.X - vCamCinematic.X) / 1.5)
//					vCamCinematic.Y = vCamCinematic.Y + ((vTrack.Y - vCamCinematic.Y) / 1.5)
//					vCamCinematic.Z = vCamCinematic.Z + ((vTrack.Z - vCamCinematic.Z) / 1.5)
//					
//					SET_CAM_COORD(camCinematic, vCamCinematic)
//					
//					POINT_CAM_AT_ENTITY(camCinematic, vehCutscene, <<0.0, 3.0 - (2.0 + CLAMP(vOffset.Y, -2.0, 0.0)), 0.5>>)
//				ENDIF
//				
//				IF IS_CONTROL_RELEASED(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
//				AND NOT IS_AIM_CAM_ACTIVE()
//					IF (DOES_ENTITY_EXIST(vehCutscene)
//					AND IS_VEHICLE_DRIVEABLE(vehCutscene))
//						IF NOT IS_GAMEPLAY_CAM_RENDERING()
//						OR IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(vehCar), GET_ENTITY_COORDS(vehCutscene)) - GET_ENTITY_HEADING(playerPedID))
//						ENDIF
//					ENDIF
//					
//					IF NOT IS_CUTSCENE_PLAYING()
//					OR CAN_SET_EXIT_STATE_FOR_CAMERA()
//						IF (DOES_ENTITY_EXIST(vehCutscene)
//						AND IS_VEHICLE_DRIVEABLE(vehCutscene))
//							IF NOT DOES_CAM_EXIST(camCinematic)
//								camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
//								SET_CAM_FOV(camCinematic, 30.5)		
//								SHAKE_CAM(camCinematic, "HAND_SHAKE", 1.0)
//								SET_CAM_COORD(camCinematic, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<6.3020, 4.8240 - (7.5 + CLAMP(vOffset.Y, -7.0, 0.0)), 3.8557>>))
//								SET_CAM_ACTIVE(camCinematic, TRUE)
//								RENDER_SCRIPT_CAMS(TRUE, FALSE)
//							ELSE
//								IF NOT IS_CAM_RENDERING(camCinematic)
//									RENDER_SCRIPT_CAMS(TRUE, TRUE, 1000, FALSE)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ELIF DOES_CAM_EXIST(camCinematic)
//					IF IS_CAM_RENDERING(camCinematic)
//					AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//						RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000, FALSE)
//					ENDIF
//				ENDIF
//				
//				SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
//			ENDIF
//			
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
//		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_POLICE_SHOOTOUT")
			STOP_AUDIO_SCENE("PROLOGUE_POLICE_SHOOTOUT")
		ENDIF
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0, TRUE)
		ENDWHILE
		
		IF DOES_CAM_EXIST(camAnim)
			DESTROY_CAM(camAnim)
		ENDIF
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) <> CAM_VIEW_MODE_FIRST_PERSON
			RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
		ENDIF
				
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
		CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
		
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TRV_PRO_MASK_REMOVED, TRUE)
		
		SET_ENTITY_INVINCIBLE(playerPedID, FALSE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
		ELSE
			SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		ENDIF
		
		SET_ENTITY_INVINCIBLE(pedBrad, FALSE)
		
		SAFE_REMOVE_BLIP(blipMichael)
		SAFE_REMOVE_BLIP(blipTrevor)
		SAFE_REMOVE_BLIP(blipBuddy)
		SAFE_REMOVE_BLIP(blipCar)
		SAFE_REMOVE_BLIP(blipDestination)
		
		SAFE_DELETE_PED(pedGetaway)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
		
		SAFE_DELETE_PED(pedCop[0])
		SAFE_DELETE_PED(pedCop[1])
		
		SAFE_DELETE_PED(pedDeadGuard)
		
		SAFE_DELETE_VEHICLE(vehCop[8])
		
		INT i
		
		REPEAT COUNT_OF(objWeapon) i
			SAFE_DELETE_OBJECT(objWeapon[i])
		ENDREPEAT
		
		IF DOES_ENTITY_EXIST(vehCutscene)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCutscene)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCutscene[CopCarDriver])
			SET_PED_AS_NO_LONGER_NEEDED(pedCutscene[CopCarDriver])
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCutscene[CopCarShotgun])
			SET_PED_AS_NO_LONGER_NEEDED(pedCutscene[CopCarShotgun])
		ENDIF
		
		REPEAT COUNT_OF(pedCop) i
			IF DOES_ENTITY_EXIST(pedCop[i])
				IF NOT IS_PED_INJURED(pedCop[i])
					DISABLE_PED_PAIN_AUDIO(pedCop[i], TRUE)
					STOP_PED_SPEAKING(pedCop[i], TRUE)
					SET_ENTITY_HEALTH(pedCop[i], 0)
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(pedCop[i])
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehCop) i
			IF DOES_ENTITY_EXIST(vehCop[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCop[i])
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehVan) i
			IF DOES_ENTITY_EXIST(vehVan[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehVan[i])
			ENDIF
		ENDREPEAT
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
		ENDIF
		
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCar, TRUE)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC GetAway()
	IF INIT_STAGE()
		//Replay 
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayGetAway), "stageGetAway")
		
		//Stats
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(PRO_GETAWAY_TIME)
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
//			FLOAT fRelativeHeading, fRelativePitch
//			
//			fRelativeHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			fRelativePitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			UPDATE_PED_REFERENCES()
			
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelativeHeading)
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(fRelativePitch + 2.0)	//Negate a random shift here
			
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
		ENDIF
		
		//Brad Balaclava + Mask
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_FEET, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_HAIR, 5, 0)
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
		CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
		
		//Duffle Bag
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 0, 0)
		
		//Michael Face
		SET_FACIAL_IDLE_ANIM_OVERRIDE(playerPedMichael, "mood_angry_1")
		
		//Brad Face
		SET_FACIAL_IDLE_ANIM_OVERRIDE(pedBrad, "Mood_Angry_1")
		
		//Dialogue
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedID, "MICHAEL")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
		
		//Radar
		bRadar = TRUE
		
		//Wanted
		SET_FAKE_WANTED_LEVEL(0)
		
		//Player Car
		//SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
		
		//Radio
		SET_VEHICLE_RADIO_ENABLED(vehCar, FALSE)
		SET_USER_RADIO_CONTROL_ENABLED(FALSE)
		
		//Roadblock Cars
		SPAWN_VEHICLE(vehCop[11], POLICEOLD2, <<3497.6370, -4871.3411, 110.6404>>, 262.8277)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		SPAWN_VEHICLE(vehCop[12], POLICEOLD2, <<3497.4355, -4864.6685, 110.7156>>, 226.5504)
//		SPAWN_VEHICLE(vehCop[13], POLICEOLD1, <<3493.8447, -4867.4614, 110.7541>>, 281.1805)
//		SPAWN_VEHICLE(vehCop[14], POLICEOLD1, <<3497.9121, -4876.0188, 110.9165>>, 196.6586)
//		SPAWN_VEHICLE(vehCop[15], POLICEOLD2, <<3497.8660, -4859.6694, 110.8799>>, 324.4299)
		SET_MODEL_AS_NO_LONGER_NEEDED(POLICEOLD1)
		
		INT i
		
		REPEAT COUNT_OF(vehCop) i
			IF NOT IS_ENTITY_DEAD(vehCop[i])
				SET_VEHICLE_DOOR_OPEN(vehCop[i], SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_OPEN(vehCop[i], SC_DOOR_FRONT_RIGHT)
				SET_VEHICLE_SIREN(vehCop[i], TRUE)
				SET_SIREN_WITH_NO_DRIVER(vehCop[i], TRUE)
				//SET_ENTITY_PROOFS(vehCop[i], FALSE, TRUE, TRUE, FALSE, FALSE)
				SET_VEHICLE_STRONG(vehCop[i], TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[i], FALSE)
			ENDIF
		ENDREPEAT
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		//Barriers
		objBarrier[0] = CREATE_OBJECT(modBarrier, <<3498.559082, -4874.109863, 110.971497>>)
		SET_ENTITY_ROTATION(objBarrier[0], <<0.0, -13.999998, -95.189995>>)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		objBarrier[1] = CREATE_OBJECT(modBarrier, <<3498.899902, -4861.600098, 110.900002>>)
		SET_ENTITY_ROTATION(objBarrier[1], <<0.0, -12.999997, 69.419998>>)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		objBarrier[2] = CREATE_OBJECT(modBarrier, <<3500.000732, -4869.240234, 110.770584>>)
		SET_ENTITY_ROTATION(objBarrier[2], <<0.0, 0.0, 68.181961>>)
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		objBarrier[3] = CREATE_OBJECT(modBarrier, <<3500.223145, -4866.932129, 110.770844>>)
		SET_ENTITY_ROTATION(objBarrier[3], <<0.0, 0.0, 100.267601>>)
		SET_MODEL_AS_NO_LONGER_NEEDED(modBarrier)
		
		//Cops
		REPEAT 6 i //6 cops for roadblock
			createCop(i + 18)
			
			SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i + 18], vCopStart[i + 18], 10.0)
			
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[i + 18], 5000)
			
			IF i = 0 OR i = 3 OR i = 4
				GIVE_WEAPON_TO_PED(pedCop[i + 18], wtSMG, INFINITE_AMMO, TRUE)
			ENDIF
			
			IF i = 2 OR i = 5
				GIVE_WEAPON_TO_PED(pedCop[i + 18], wtShotgun, INFINITE_AMMO, TRUE)
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDREPEAT
		
		CLEAR_TEXT()
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Score
		SET_FRONTEND_RADIO_ACTIVE(FALSE)
		
		PLAY_AUDIO(PROLOGUE_TEST_CAR_CHASE)
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_TO_PICKUP")
			START_AUDIO_SCENE("PROLOGUE_DRIVE_TO_PICKUP")
		ENDIF
		
		//Tasks
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedTrevor, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, TRUE)
		
		SET_PED_COMBAT_ATTRIBUTES(playerPedID, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(notPlayerPedID, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_LEAVE_VEHICLES, FALSE)
		
		SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_RESTRICT_IN_VEHICLE_AIMING_TO_CURRENT_SIDE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_RESTRICT_IN_VEHICLE_AIMING_TO_CURRENT_SIDE, TRUE)
		
		SET_PED_CONFIG_FLAG(playerPedMichael, PCF_GetOutUndriveableVehicle, FALSE)
		SET_PED_CONFIG_FLAG(playerPedTrevor, PCF_GetOutUndriveableVehicle, FALSE)
		SET_PED_CONFIG_FLAG(pedBrad, PCF_GetOutUndriveableVehicle, FALSE)
		
		//Roads
		SET_ROADS_IN_ANGLED_AREA(<<3477.819580, -4862.902832, 109.788643>>, <<3504.164063, -4869.103027, 120.770584>>, 16.0, FALSE, FALSE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		iBulletTimers[0] = 0
		iBulletTimers[1] = 0
		
		IF SKIPPED_STAGE()
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
				IF GET_CURRENT_PLAYBACK_FOR_VEHICLE(vehCar) = GET_VEHICLE_RECORDING_ID(026, sCarrec)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
				ENDIF
			ENDIF
			
			SET_LABEL_AS_TRIGGERED(Pullout, TRUE)
			
			CLEAR_PED_TASKS(playerPedMichael)
			CLEAR_PED_TASKS(playerPedTrevor)
			CLEAR_PED_TASKS(pedBrad)
			
			EMPTY_VEHICLE_OF_PEDS(vehCar)
			
			SET_PED_INTO_VEHICLE(playerPedMichael, vehCar, VS_DRIVER)
			SET_PED_INTO_VEHICLE(playerPedTrevor, vehCar, VS_BACK_LEFT)
			SET_PED_INTO_VEHICLE(pedBrad, vehCar, VS_BACK_RIGHT)
			
			SET_VEHICLE_POSITION(vehCar, <<4339.5576, -5080.9526, 110.0518>>, 80.3059)
			
			SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_LEFT)
			SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
			
			GIVE_WEAPON_TO_PED(playerPedMichael, wtPistol, 350)
			
			GIVE_WEAPON_TO_PED(playerPedTrevor, wtPistol, 350)
			GIVE_WEAPON_TO_PED(playerPedTrevor, WEAPONTYPE_MICROSMG, 350)
			
			GIVE_WEAPON_TO_PED(pedBrad, wtPistol, INFINITE_AMMO)
			
			WAIT_WITH_DEATH_CHECKS(500)
			
			SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
			
			SET_VEHICLE_FORWARD_SPEED(vehCar, 30.0)
			
			STOP_CAM_SHAKING(camMain, TRUE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
			SET_CLOCK_TIME(6, 0, 0)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
		ENDIF
	ELSE
		REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(4400.0 - 2500.0, -5100.0 - 1500.0, 4400.0 + 2500.0, -5100.0 + 1500.0)
		
		//Print Objective
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Where)
				CREATE_CONVERSATION_ADV(PRO_Where, "PRO_Where")
			ELSE
				PRINT_ADV(PRO_GETAWAY, "PRO_GETAWAY", DEFAULT_GOD_TEXT_TIME - 4000)
			ENDIF
		ENDIF
		
		//Request Cutscene Variations - PRO_MCS_6
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Brad", pedBrad)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF NOT HAS_LABEL_BEEN_TRIGGERED(NODES_LOADED)
			IF ARE_NODES_LOADED_FOR_AREA(4400.0 - 2500.0, -5100.0 - 1500.0, 4400.0 + 2500.0, -5100.0 + 1500.0)
				#IF IS_DEBUG_BUILD	PRINTLN("NODES_LOADED_FOR_AREA(4400.0 - 2500.0, -5100.0 - 1500.0, 4400.0 + 2500.0, -5100.0 + 1500.0)")	#ENDIF
				
				SET_LABEL_AS_TRIGGERED(NODES_LOADED, TRUE)
			ENDIF
		ENDIF
		#ENDIF
		
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PRO_GETAWAY")
		AND HAS_LABEL_BEEN_TRIGGERED(PRO_Where)
		AND NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Blockade)
			IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF (NOT DOES_ENTITY_EXIST(vehCop[9])
					AND NOT DOES_ENTITY_EXIST(vehCop[10]))
					OR HAS_LABEL_BEEN_TRIGGERED(PRO_Drive_2)
					OR (DOES_ENTITY_EXIST(pedCop[2])
					AND IS_PED_INJURED(pedCop[2])
					AND DOES_ENTITY_EXIST(pedCop[4])
					AND IS_PED_INJURED(pedCop[4]))
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive1_1)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive1_1, "PRO_Drive1", "PRO_Drive1_1")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive1_2)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive1_2, "PRO_Drive1", "PRO_Drive1_2")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive1_3)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive1_3, "PRO_Drive1", "PRO_Drive1_3")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive1_4)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive1_4, "PRO_Drive1", "PRO_Drive1_4")
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive1_5)
							PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive1_5, "PRO_Drive1", "PRO_Drive1_5")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(playerPedID, vehCar)
		AND NOT (IS_PED_GETTING_INTO_A_VEHICLE(playerPedID)
		AND GET_VEHICLE_PED_IS_USING(playerPedID) = vehCar)
			iDialogueStage = 2	//PRO_LeftCar
			
			IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
				IF GET_GAME_TIMER() > iDialogueTimer
					IF iDialogueLineCount[iDialogueStage] = -1 
						 iDialogueLineCount[iDialogueStage] = 5
					ELIF iDialogueLineCount[iDialogueStage] > 0
						IF iDialogueLineCount[iDialogueStage] >= 4
							IF NOT HAS_LABEL_BEEN_TRIGGERED(CMN_GENGETIN)
								PRINT_ADV(CMN_GENGETIN, "CMN_GENGETIN")
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED(CMN_GENGETBCK)
								PRINT_ADV(CMN_GENGETBCK, "CMN_GENGETBCK")
							ENDIF
							
							CREATE_CONVERSATION_ADV(PRO_LeftCar, "PRO_LeftCar", CONV_PRIORITY_MEDIUM, FALSE, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							CREATE_CONVERSATION_ADV(PRO_LeftCar, "PRO_LeftCar", CONV_PRIORITY_MEDIUM, FALSE)
						ENDIF
						
						iDialogueLineCount[iDialogueStage]--
					ENDIF
					
					iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() > iFailTimer
				eMissionFail = failAbandonedCar
				
				missionFailed()
			ENDIF
		ELSE
			SAFE_CLEAR_THIS_PRINT("CMN_GENGETIN")
			SAFE_CLEAR_THIS_PRINT("CMN_GENGETBCK")
			
			iFailTimer = GET_GAME_TIMER() + 10000
		ENDIF
		
//		IF playerPedID != playerPedMichael
//			PRINT_HELP_ADV(PROHLP_CARSWITCH, "PROHLP_CARSWITCH")
//		ENDIF
//		
//		IF NOT sCamDetails.bRun
//			IF UPDATE_SELECTOR_HUD(sSelectorPeds) //Returns TRUE when the player has made a selection
//				IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
//					INFORM_MISSION_STATS_OF_INCREMENT(PRO_SWITCHES)
//					
//					CLEAR_PED_TASKS(playerPedID)
//					
//					sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//					sCamDetails.bRun  = TRUE
//				ENDIF
//			ENDIF
//		ELSE
//			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_SHORT_RANGE(sCamDetails)	//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED_RACE(sCamDetails) //Returns FALSE when the camera spline is complete
//				IF sCamDetails.bOKToSwitchPed
//					IF NOT sCamDetails.bPedSwitched
//						IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//							CLEAR_PED_TASKS(playerPedID)
//							
//							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
//							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
//							
//							//Update the relationship groups
//							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
//							ENDIF
//							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroupBuddy)
//							ENDIF
//							
//							sCamDetails.bPedSwitched = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		//Print
		IF playerPedID = playerPedMichael
		OR DOES_ENTITY_EXIST(vehTraffic[0])
			IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
				IF IS_RADAR_PREFERENCE_SWITCHED_ON()
					IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
						IF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_CAR2)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
							PRINT_HELP_ADV(PROHLP_CAR2, "PROHLP_CAR2")
							SETTIMERB(0)
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_CAR4)
							IF TIMERB() > DEFAULT_HELP_TEXT_TIME
							OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							AND TIMERB() > DEFAULT_HELP_TEXT_TIME / 10)
								PRINT_HELP_ADV(PROHLP_CAR4, "PROHLP_CAR4")
								SETTIMERB(0)
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_CAR3)
							IF TIMERB() > DEFAULT_HELP_TEXT_TIME
							OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
							AND TIMERB() > DEFAULT_HELP_TEXT_TIME / 10)
								PRINT_HELP_ADV(PROHLP_CAR3, "PROHLP_CAR3")
								SETTIMERB(0)
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_DEST1)
							IF TIMERB() > DEFAULT_HELP_TEXT_TIME
							OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							AND TIMERB() > DEFAULT_HELP_TEXT_TIME / 10)
								PRINT_HELP_ADV(PROHLP_DEST1, "PROHLP_DEST1")
								
								IF DOES_BLIP_EXIST(blipDestination)
									SET_BLIP_FLASH_DURATION(blipDestination, 5000)
								ENDIF
								
								SETTIMERB(0)
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PROHLP_DEST2)
							IF TIMERB() > DEFAULT_HELP_TEXT_TIME
								SET_GPS_FLASHES(TRUE)
								PRINT_HELP_ADV(PROHLP_DEST2, "PROHLP_DEST2")
								SETTIMERB(0)
							ENDIF
						ELSE
							IF TIMERB() > DEFAULT_HELP_TEXT_TIME
								SET_GPS_FLASHES(FALSE)
								
								SETTIMERB(0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SAFE_CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF playerPedID = playerPedTrevor
			IF ARE_NODES_LOADED_FOR_AREA(4400.0 - 2500.0, -5100.0 - 1500.0, 4400.0 + 2500.0, -5100.0 + 1500.0)
				IF GET_SCRIPT_TASK_STATUS(playerPedMichael, SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, TRUE)
					TASK_VEHICLE_MISSION_COORS_TARGET(playerPedMichael, vehCar, <<3546.0022, -4670.4585, 113.2054>>, MISSION_GOTO, 40.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, -1, -1, TRUE)
				ENDIF
			ELSE
				IF GET_SCRIPT_TASK_STATUS(playerPedMichael, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, TRUE)
					TASK_VEHICLE_DRIVE_WANDER(playerPedMichael, vehCar, 15.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_AT_COORD(playerPedID, <<4092.9468, -5062.0420, 107.4313>>, <<300.0, 300.0, 20.0>>)
		AND NOT DOES_ENTITY_EXIST(vehTraffic[0])
			IF HAS_MODEL_LOADED_CHECK(MODEL_EMPEROR3, EMPEROR3)
			AND HAS_MODEL_LOADED_CHECK(MODEL_A_M_M_HILLBILLY_01, A_M_M_HILLBILLY_01)
				IF CAN_CREATE_RANDOM_DRIVER()
					SPAWN_VEHICLE(vehTraffic[0], EMPEROR3, <<4092.9468, -5062.0420, 107.4313>>, 265.1335)
					pedTraffic[0] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[0])
					TASK_VEHICLE_DRIVE_WANDER(pedTraffic[0], vehTraffic[0], 15.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
					SET_PED_COMBAT_ATTRIBUTES(pedTraffic[0], CA_ALWAYS_FLEE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(pedTraffic[0], FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTraffic[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(EMPEROR3)
					SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_HILLBILLY_01)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_AT_COORD(playerPedID, <<3675.6301, -4915.4111, 110.7071>>, <<300.0, 300.0, 20.0>>)
		AND NOT DOES_ENTITY_EXIST(vehTraffic[1])
			IF HAS_MODEL_LOADED_CHECK(MODEL_TRACTOR3, TRACTOR3)
			AND HAS_MODEL_LOADED_CHECK(MODEL_A_M_M_HILLBILLY_02, A_M_M_HILLBILLY_02)
				IF CAN_CREATE_RANDOM_DRIVER()
					SPAWN_VEHICLE(vehTraffic[1], TRACTOR3, <<3676.4517, -4928.2305, 110.6588>>, 185.1477)
					pedTraffic[1] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[1])
					//TASK_VEHICLE_DRIVE_WANDER(pedTraffic[1], vehTraffic[1], 8.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
					TASK_VEHICLE_DRIVE_TO_COORD(pedTraffic[1], vehTraffic[1], <<5415.8516, -5128.4233, 77.1630>>, 8.0, DRIVINGSTYLE_NORMAL, TRACTOR3, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5.0, 5)
					SET_PED_COMBAT_ATTRIBUTES(pedTraffic[1], CA_ALWAYS_FLEE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(pedTraffic[1], FA_DISABLE_HESITATE_IN_VEHICLE, TRUE)
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTraffic[1], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(TRACTOR3)
					SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_HILLBILLY_02)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_AT_COORD(playerPedID, <<3670.4436, -4940.1943, 110.6677>>, <<400.0, 400.0, 20.0>>)
		AND NOT DOES_ENTITY_EXIST(vehCop[9])
		AND NOT DOES_ENTITY_EXIST(vehCop[10])
			SPAWN_VEHICLE(vehCop[9], POLICEOLD2, <<3670.4436, -4940.1943, 110.6677>>, 243.7315)
			SPAWN_VEHICLE(vehCop[10], POLICEOLD2, <<3773.0398, -4986.7998, 110.339 >>, 247.8150)
			
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[9], FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[10], FALSE)
			
			SET_VEHICLE_SIREN(vehCop[9], TRUE)
			SET_SIREN_WITH_NO_DRIVER(vehCop[9], TRUE)
			SET_VEHICLE_SIREN(vehCop[10], TRUE)
			SET_SIREN_WITH_NO_DRIVER(vehCop[10], TRUE)
			
			createCop(2)
			createCop(3)
			createCop(4)
			createCop(5)
			
			SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_LEAVE_VEHICLES, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[3], CA_LEAVE_VEHICLES, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[3], CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[4], CA_LEAVE_VEHICLES, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[4], CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[5], CA_LEAVE_VEHICLES, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedCop[5], CA_USE_VEHICLE, TRUE)
			
			SET_PED_ACCURACY(pedCop[2], 40)
			SET_PED_ACCURACY(pedCop[3], 40)
			SET_PED_ACCURACY(pedCop[4], 40)
			SET_PED_ACCURACY(pedCop[5], 40)
			
			SET_PED_INTO_VEHICLE(pedCop[2], vehCop[9])
			SET_PED_INTO_VEHICLE(pedCop[3], vehCop[9], VS_FRONT_RIGHT)
			SET_PED_INTO_VEHICLE(pedCop[4], vehCop[10])
			SET_PED_INTO_VEHICLE(pedCop[5], vehCop[10], VS_FRONT_RIGHT)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[2], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[3], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[4], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[5], TRUE)
			
			SET_VEHICLE_USE_ALTERNATE_HANDLING(vehCop[9], TRUE)
			SET_VEHICLE_USE_ALTERNATE_HANDLING(vehCop[10], TRUE)
			
			//TASK_VEHICLE_DRIVE_TO_COORD(pedCop[2], vehCop[9], <<5415.8516, -5128.4233, 77.1630>>, 40.0, DRIVINGSTYLE_NORMAL, POLICEOLD2, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5.0, 5)
			//TASK_VEHICLE_DRIVE_TO_COORD(pedCop[4], vehCop[10], <<5415.8516, -5128.4233, 77.1630>>, 40.0, DRIVINGSTYLE_NORMAL, POLICEOLD2, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5.0, 5)
			TASK_VEHICLE_MISSION_COORS_TARGET(pedCop[4], vehCop[10], <<5415.8516, -5128.4233, 77.1630>>, MISSION_GOTO_RACING, 40.0, DRIVINGMODE_STOPFORCARS_STRICT, 0.1, 0.1, TRUE)
			TASK_VEHICLE_ESCORT(pedCop[2], vehCop[9], vehCop[10], VEHICLE_ESCORT_REAR, 40.0, DRIVINGMODE_STOPFORCARS_STRICT)
		ENDIF
		
		IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
			IF DOES_ENTITY_EXIST(vehCop[10])
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[10])) < 150.0
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV(PRO_Drive, "PRO_Drive")	//PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive_1, "PRO_Drive", "PRO_Drive_1")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED(PRO_Drive_1, TRUE)
					
					//Score
					PLAY_AUDIO(PROLOGUE_TEST_POLICE_DRIVE_BY)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<3501.779541, -4879.914063, 108.446884>>, <<3500.778320, -4848.768555, 117.260483>>, 20.0)
				SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
			ELSE
				SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(pedBrad, <<3501.779541, -4879.914063, 108.446884>>, <<3500.778320, -4848.768555, 117.260483>>, 20.0)
			SET_ENTITY_INVINCIBLE(pedBrad, FALSE)
		ELSE
			SET_ENTITY_INVINCIBLE(pedBrad, TRUE)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedTrevor, <<3498.418457,-4868.590332,109.773926>>, <<3471.526367,-4867.029297,126.711273>>, 20.0)
		AND (NOT IS_PED_INJURED(pedCop[18])
		AND IS_PED_SHOOTING(pedCop[18]))
		AND (NOT IS_PED_INJURED(pedCop[19])
		AND IS_PED_SHOOTING(pedCop[19]))
		AND (NOT IS_PED_INJURED(pedCop[20])
		AND IS_PED_SHOOTING(pedCop[20]))
		AND (NOT IS_PED_INJURED(pedCop[21])
		AND IS_PED_SHOOTING(pedCop[21]))
		AND (NOT IS_PED_INJURED(pedCop[22])
		AND IS_PED_SHOOTING(pedCop[22]))
			SET_ENTITY_HEALTH(playerPedTrevor, 0)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehCop[9])
		AND DOES_ENTITY_EXIST(vehCop[10])
			IF HAS_LABEL_BEEN_TRIGGERED(PRO_Drive_1)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_ON_SCREEN(vehCop[9])
				AND NOT IS_ENTITY_ON_SCREEN(vehCop[10])
					IF NOT IS_PED_INJURED(pedCop[2])
					AND NOT IS_PED_INJURED(pedCop[4])
						IF GET_SCRIPT_TASK_STATUS(pedCop[2], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
							IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								//PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive_2, "PRO_Drive", "PRO_Drive_2")
								SET_LABEL_AS_TRIGGERED(PRO_Drive_2, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_TOUCHING_ENTITY(vehCar, vehCop[9])
			OR IS_ENTITY_TOUCHING_ENTITY(vehCar, vehCop[10])
			OR (IS_PED_SHOOTING(playerPedID)
			AND ((IS_ENTITY_ON_SCREEN(vehCop[9])
			OR IS_ENTITY_ON_SCREEN(vehCop[10]))
			OR (IS_BULLET_IN_AREA(GET_ENTITY_COORDS(vehCop[9]), 20.0)
			OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(vehCop[10]), 20.0))))
			OR ((IS_ENTITY_IN_ANGLED_AREA(vehCar, GET_ENTITY_COORDS(vehCop[9]), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCop[9], <<0.0, 6.0, 3.0>>), 3.0)
			OR IS_ENTITY_IN_ANGLED_AREA(vehCar, GET_ENTITY_COORDS(vehCop[9]), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCop[9], <<0.0, 6.0, 3.0>>), 3.0))
			AND GET_ENTITY_SPEED(vehCar) < 5.0)
				IF NOT IS_PED_INJURED(pedCop[4])
					IF GET_SCRIPT_TASK_STATUS(pedCop[4], SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
						CLEAR_PED_TASKS(pedCop[4])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[4], FALSE)
						//SAFE_ADD_BLIP_PED(blipCop[4], pedCop[4], TRUE)
						
						TASK_COMBAT_PED(pedCop[4], playerPedID)
						
						IF NOT IS_PED_INJURED(pedCop[5])
							CLEAR_PED_TASKS(pedCop[5])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[5], FALSE)
							//SAFE_ADD_BLIP_PED(blipCop[5], pedCop[5], TRUE)
							
							TASK_COMBAT_PED(pedCop[5], playerPedID)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedCop[2])
							CLEAR_PED_TASKS(pedCop[2])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[2], FALSE)
							//SAFE_ADD_BLIP_PED(blipCop[2], pedCop[2], TRUE)
							
							TASK_COMBAT_PED(pedCop[2], playerPedID)
							
							IF NOT IS_PED_INJURED(pedCop[3])
								CLEAR_PED_TASKS(pedCop[3])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[3], FALSE)
								//SAFE_ADD_BLIP_PED(blipCop[3], pedCop[3], TRUE)
								
								TASK_COMBAT_PED(pedCop[3], playerPedID)
							ENDIF
						ENDIF
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedTrevor, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, FALSE)
						
						//Police Cars pull up
						SET_FAKE_WANTED_LEVEL(5)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Blockade)
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_LABEL_BEEN_TRIGGERED(PRO_Drive1_5)
						IF (NOT DOES_ENTITY_EXIST(vehCop[9])
						AND NOT DOES_ENTITY_EXIST(vehCop[10]))
						OR HAS_LABEL_BEEN_TRIGGERED(PRO_Drive_2)
						OR (DOES_ENTITY_EXIST(pedCop[2])
						AND IS_PED_INJURED(pedCop[2])
						AND DOES_ENTITY_EXIST(pedCop[4])
						AND IS_PED_INJURED(pedCop[4]))
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), <<3499.77417, -4868.12256, 110.77395>>) > 400.0
								IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_1)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_1, "PRO_Drive2", "PRO_Drive2_1")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_2)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_2, "PRO_Drive2", "PRO_Drive2_2")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_3)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_3, "PRO_Drive2", "PRO_Drive2_3")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_4)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_4, "PRO_Drive2", "PRO_Drive2_4")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_5)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_5, "PRO_Drive2", "PRO_Drive2_5")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_6)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_6, "PRO_Drive2", "PRO_Drive2_6")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_7)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_7, "PRO_Drive2", "PRO_Drive2_7")
								ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Drive2_8)
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Drive2_8, "PRO_Drive2", "PRO_Drive2_8")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
			IF ((IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3474.520264, -4860.874512, 109.789116>>, <<3731.3, -4966.4, 139.623703>>, 200.0) AND GET_ENTITY_SPEED(vehCar) > 15.0)
			OR IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3474.520264, -4860.874512, 109.789116>>, <<3671.1, -4937.4, 139.623703>>, 200.0))
			AND IS_SPHERE_VISIBLE(<<3498.0, -4867.9, 113.0>>, 5.0)
				IF NOT HAS_LABEL_BEEN_TRIGGERED(RoadblockAudio)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedTrevor, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, FALSE)
					
					//Score
					LOAD_AUDIO(PROLOGUE_TEST_TRAIN_CRASH)
					
					PLAY_AUDIO(PROLOGUE_TEST_ROADBLOCK_WARNING)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_TO_PICKUP")
						STOP_AUDIO_SCENE("PROLOGUE_DRIVE_TO_PICKUP")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_ESCAPE")
						START_AUDIO_SCENE("PROLOGUE_DRIVE_ESCAPE")
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0)
					
					SET_LABEL_AS_TRIGGERED(RoadblockAudio, TRUE)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Blockade)
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV(PRO_Blockade, "PRO_Blockade")
					ELIF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Cutscene
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), <<3539.7244, -4718.3975, 111.8996>>) < (DEFAULT_CUTSCENE_LOAD_DIST * 5)
			REQUEST_CUTSCENE("PRO_MCS_6")
		ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), <<3539.7244, -4718.3975, 111.8996>>) > (DEFAULT_CUTSCENE_LOAD_DIST * 4) + DEFAULT_CUTSCENE_UNLOAD_DIST
			REMOVE_CUTSCENE()
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3417.754639, -4849.398682, 100.707458>>, <<3654.789307, -4920.359863, 120.700195>>, 200.0)
			IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_cutFinale)
//				SAFE_REMOVE_BLIP(blipDestination)
//				SAFE_ADD_BLIP_LOCATION(blipDestination, <<3546.0022, -4670.4585, 113.2054>>, TRUE)
				
				SET_LABEL_AS_TRIGGERED(TRIGGER_cutFinale, TRUE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3522.664307, -4850.235107, 105.671204>>, <<3530.755859, -4748.138672, 121.426514>>, 20.0)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3501.273926, -4884.231445, 109.669617>>, <<3509.990479, -4840.174072, 116.080200>>, 70.0)
			//Police Cars pull up
			SET_FAKE_WANTED_LEVEL(5)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3501.273926, -4884.231445, 109.669617>>, <<3509.990479, -4840.174072, 116.080200>>, 70.0)
		AND GET_ENTITY_SPEED(vehCar) > 10.0
			PED_INDEX pedShooting
			
			IF NOT SAFE_IS_PED_DEAD(pedCop[18])
			AND NOT IS_PED_RAGDOLL(pedCop[18])
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop[18]), GET_ENTITY_COORDS(vehCar)) > 15.0
				pedShooting = pedCop[18]
			ELIF NOT SAFE_IS_PED_DEAD(pedCop[19])
			AND NOT IS_PED_RAGDOLL(pedCop[19])
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop[19]), GET_ENTITY_COORDS(vehCar)) > 15.0
				pedShooting = pedCop[19]
			ELIF NOT SAFE_IS_PED_DEAD(pedCop[20])
			AND NOT IS_PED_RAGDOLL(pedCop[20])
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop[20]), GET_ENTITY_COORDS(vehCar)) > 15.0
				pedShooting = pedCop[20]
			ELIF NOT SAFE_IS_PED_DEAD(pedCop[21])
			AND NOT IS_PED_RAGDOLL(pedCop[21])
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop[21]), GET_ENTITY_COORDS(vehCar)) > 15.0
				pedShooting = pedCop[21]
			ELIF NOT SAFE_IS_PED_DEAD(pedCop[22])
			AND NOT IS_PED_RAGDOLL(pedCop[22])
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedCop[22]), GET_ENTITY_COORDS(vehCar)) > 15.0
				pedShooting = pedCop[22]
			ENDIF
			
			IF NOT IS_PED_INJURED(pedShooting)
			AND NOT IS_PED_RAGDOLL(pedShooting)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedShooting), GET_ENTITY_COORDS(vehCar)) > 15.0
			AND IS_PED_SHOOTING(pedShooting)
				DO_EXCITING_BULLET_ON_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<0.5, 0.0, 0.0>>), pedShooting, iBulletTimers[0], <<0.0, 0.6, 0.0>>)
				DO_EXCITING_NEAR_BULLET_MISS_ON_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<0.5, 0.0, 0.0>>), pedShooting, iBulletTimers[1], <<0.0, 0.6, 0.0>>)
				
				IF IS_VEHICLE_WINDOW_INTACT(vehCar, SC_WINDOW_REAR_LEFT)
					SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
			IF IS_THIS_PRINT_BEING_DISPLAYED("PRO_CAR2")
				SAFE_CLEAR_THIS_PRINT("PRO_CAR2")
			ENDIF
			
			SAFE_REMOVE_BLIP(blipCar)
			
			IF IS_PED_IN_VEHICLE(playerPedTrevor, vehCar)
			AND IS_PED_IN_VEHICLE(pedBrad, vehCar)
				SAFE_REMOVE_BLIP(blipTrevor)
				SAFE_REMOVE_BLIP(blipBuddy)
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_cutFinale)
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<3546.0022, -4670.4585, 113.2054>>, TRUE)	//<<3343.2200, -4845.2009, 110.8667>>, TRUE)
				ELSE
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<3546.0022, -4670.4585, 113.2054>>, TRUE)
				ENDIF
			ELSE
				SAFE_REMOVE_BLIP(blipDestination)
				
				IF NOT IS_PED_IN_VEHICLE(playerPedTrevor, vehCar)
				AND NOT IS_PED_IN_VEHICLE(pedBrad, vehCar)
					PRINT_ADV(PRO_TEAM, "PRO_TEAM")
				ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(playerPedTrevor, vehCar)
				AND NOT IS_PED_GETTING_INTO_A_VEHICLE(playerPedTrevor)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_TEAM)
						PRINT_ADV(PRO_BUDDY, "PRO_BUDDY")
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						SAFE_ADD_BLIP_PED(blipTrevor, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
					ENDIF
					
					IF GET_SCRIPT_TASK_STATUS(playerPedTrevor, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
						TASK_ENTER_VEHICLE(playerPedTrevor, vehCar, -1, VS_BACK_LEFT)
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipTrevor)
				ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(pedBrad, vehCar)
				AND NOT IS_PED_GETTING_INTO_A_VEHICLE(pedBrad)
					IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_TEAM)
						PRINT_ADV(PRO_BUDDY, "PRO_BUDDY")
					ENDIF
					
					SAFE_ADD_BLIP_PED(blipBuddy, pedBrad, FALSE)
					
					IF GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
						TASK_ENTER_VEHICLE(pedBrad, vehCar, -1, VS_BACK_RIGHT)
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipBuddy)
				ENDIF
			ENDIF
		ELSE
			SAFE_REMOVE_BLIP(blipDestination)
			SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
			PRINT_ADV(PRO_CAR2, "PRO_CAR2")
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<3629.041016, -4750.028809, 109.965340>>, <<3435.589355, -4728.467285, 130.489777>>, 175.0)
			IF IS_PED_SITTING_IN_VEHICLE(playerPedMichael, vehCar)
			AND IS_PED_SITTING_IN_VEHICLE(playerPedTrevor, vehCar)
			AND IS_PED_SITTING_IN_VEHICLE(pedBrad, vehCar)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF SAFE_START_CUTSCENE(0.0, FALSE)
						ADVANCE_STAGE()
					ENDIF
				ELSE
					ADVANCE_STAGE()
				ENDIF
			ENDIF
		ENDIF
		
		INT i
		
		BOOL bOffRoad = FALSE
		
		REPEAT COUNT_OF(vRoutePoint1) i
			IF IS_ENTITY_IN_ANGLED_AREA(playerPedID, vRoutePoint1[i], vRoutePoint2[i], fRouteWidth[i])
				bOffRoad = TRUE
			ENDIF
		ENDREPEAT
		
		IF bOffRoad = FALSE
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PRO_STAY_ON_ROAD) 
			
			IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_OffRoute")
				iDialogueStage = 0	//PRO_OffRoute
				
				IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
					IF GET_GAME_TIMER() > iDialogueTimer
						IF iDialogueLineCount[iDialogueStage] = -1 
							 iDialogueLineCount[iDialogueStage] = 6
						ELIF iDialogueLineCount[iDialogueStage] > 0
							CREATE_CONVERSATION_ADV(PRO_OffRoute, "PRO_OffRoute", CONV_PRIORITY_MEDIUM, FALSE)
							
							iDialogueLineCount[iDialogueStage]--
						ENDIF
						
						iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		VECTOR vClosest = vRoute[0]
		
		REPEAT COUNT_OF(vRoute) i
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), vRoute[i]) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), vClosest)
				vClosest = vRoute[i]
				
				IF i > 2
					vRoute[i - 3] = VECTOR_ZERO	//Delete two nodes back so you can't drive the wrong way
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), vClosest) > 30.0
		AND bOffRoad = FALSE
			IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("PRO_WrongWay")
				iDialogueStage = 1	//PRO_WrongWay
				
				IF IS_PED_IN_VEHICLE(playerPedID, vehCar)
					IF GET_GAME_TIMER() > iDialogueTimer
						IF iDialogueLineCount[iDialogueStage] = -1 
							 iDialogueLineCount[iDialogueStage] = 5
						ELIF iDialogueLineCount[iDialogueStage] > 0
							CREATE_CONVERSATION_ADV(PRO_WrongWay, "PRO_WrongWay", CONV_PRIORITY_MEDIUM, FALSE)
							
							iDialogueLineCount[iDialogueStage]--
						ENDIF
						
						iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7500, 10000)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		MATERIAL_NAMES CollisionSurface = GET_LAST_MATERIAL_HIT_BY_ENTITY(vehCar)
		
		#IF IS_DEBUG_BUILD
		IF CollisionSurface = GENERAL_SNOW_LOOSE	//snow_loose
			PRINTLN("GENERAL_SNOW_LOOSE")
		ENDIF
		
//		IF CollisionSurface = GENERAL_SNOW_TARMAC	//snow_tarmac
//			PRINTLN("GENERAL_SNOW_TARMAC")
//		ENDIF
		
		IF CollisionSurface = GENERAL_SNOW_DEEP		//snow_deep
			PRINTLN("GENERAL_SNOW_DEEP")
		ENDIF
		
		IF CollisionSurface = GENERAL_SNOW_COMPACT	//snow_compact
			PRINTLN("GENERAL_SNOW_COMPACT")
		ENDIF
		#ENDIF
		
		IF GET_ENTITY_SPEED(vehCar) < 3.0
			IF CollisionSurface = GENERAL_SNOW_LOOSE	//snow_loose
//			OR CollisionSurface = GENERAL_SNOW_TARMAC	//snow_tarmac
			OR CollisionSurface = GENERAL_SNOW_DEEP		//snow_deep
			OR CollisionSurface = GENERAL_SNOW_COMPACT	//snow_compact
				IF GET_GAME_TIMER() > iStuckTimer
					eMissionFail = failStuck
					
					missionFailed()
				ENDIF
			ENDIF
		ELSE
			iStuckTimer = GET_GAME_TIMER() + 10000
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), vClosest) > 60.0
			eMissionFail = failOffRoute
			
			missionFailed()
		ENDIF
		
		IF ARE_VECTORS_EQUAL(vTrainHornLocation, VECTOR_ZERO)
			IF IS_ENTITY_AT_COORD(vehCar, <<3875.881348, -5020.976074, 113.531639>>, <<10.0, 25.0, 8.0>>)
				iTrainHornTimer = GET_GAME_TIMER() + 2000
				
				vTrainHornLocation = <<3885.47705, -5082.06104, 110.29445>>
			ENDIF
		ELIF ARE_VECTORS_EQUAL(vTrainHornLocation, <<3885.47705, -5082.06104, 110.29445>>)
			IF IS_ENTITY_AT_COORD(vehCar, <<3669.30444, -4935.35303, 110.68387>>, <<10.0, 25.0, 8.0>>)
				iTrainHornTimer = GET_GAME_TIMER() + 2000
				
				vTrainHornLocation = <<3685.60645, -4884.99023, 110.76416>>
			ENDIF
		ELIF ARE_VECTORS_EQUAL(vTrainHornLocation, <<3685.60645, -4884.99023, 110.76416>>)
			IF IS_ENTITY_AT_COORD(vehCar, <<3523.41113, -4856.05127, 110.64999>>, <<10.0, 25.0, 8.0>>)
				iTrainHornTimer = GET_GAME_TIMER() + 2000
				
				vTrainHornLocation = <<3549.05420, -4828.07568, 112.12215>>
			ENDIF
		ENDIF
		
		//Sound
		IF REQUEST_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")	//PRINTLN("Prologue_Train_Sounds - Loaded")
			IF sIDTrainBell = -1
				sIDTrainBell = GET_SOUND_ID()	PRINTLN("PLAY_SOUND_FROM_COORD | sIDTrainBell | Train_Bell")
				PLAY_SOUND_FROM_COORD(sIDTrainBell, "Train_Bell", <<3875.881348, -5020.976074, 113.531639>>, "Prologue_Sounds")
			ENDIF
			
			IF GET_GAME_TIMER() > iTrainHornTimer
				IF sIDTrainHorn != -1
					STOP_SOUND(sIDTrainHorn)	PRINTLN("STOP_SOUND | sIDTrainHorn")
					RELEASE_SOUND_ID(sIDTrainHorn)
					sIDTrainHorn = -1
				ENDIF
			ELIF NOT ARE_VECTORS_EQUAL(vTrainHornLocation, VECTOR_ZERO)
				IF sIDTrainHorn = -1
					sIDTrainHorn = GET_SOUND_ID()	PRINTLN("PLAY_SOUND_FROM_COORD | sIDTrainHorn | Train_Horn")
					PLAY_SOUND_FROM_COORD(sIDTrainHorn, "Train_Horn", vTrainHornLocation, "Prologue_Sounds")
				ENDIF
			ENDIF
		ENDIF
		
		INT iGameTime = GET_GAME_TIMER()
		
		IF iGameTime % 8000 < 3000
			SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_DO_DRIVEBYS, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_DO_DRIVEBYS, FALSE)
		ELIF iGameTime % 8000 >= 4000 AND iGameTime % 8000 < 7000
			SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_DO_DRIVEBYS, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_DO_DRIVEBYS, TRUE)
		ELIF (iGameTime % 8000 >= 3000 AND iGameTime % 8000 < 4000)
		OR iGameTime % 8000 >= 7000
			SET_PED_COMBAT_ATTRIBUTES(pedBrad, CA_DO_DRIVEBYS, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(playerPedTrevor, CA_DO_DRIVEBYS, FALSE)
		ENDIF
		
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_TO_PICKUP")
			STOP_AUDIO_SCENE("PROLOGUE_DRIVE_TO_PICKUP")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_DRIVE_ESCAPE")
			STOP_AUDIO_SCENE("PROLOGUE_DRIVE_ESCAPE")
		ENDIF
		
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(playerPedMichael)
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(pedBrad)
		
		SET_ENTITY_INVINCIBLE(playerPedMichael, FALSE)
		SET_ENTITY_INVINCIBLE(playerPedTrevor, FALSE)
		SET_ENTITY_INVINCIBLE(pedBrad, FALSE)
		
		SET_GPS_FLASHES(FALSE)
		
		SAFE_REMOVE_BLIP(blipCop[2])
//		SAFE_REMOVE_BLIP(blipCop[3])
		
		SAFE_REMOVE_BLIP(blipCar)
		SAFE_REMOVE_BLIP(blipDestination)
		
		SAFE_DELETE_PED(pedTraffic[0])
		SAFE_DELETE_PED(pedTraffic[1])
		
		SAFE_DELETE_VEHICLE(vehTraffic[0])
		SAFE_DELETE_VEHICLE(vehTraffic[1])
		
		SET_MODEL_AS_NO_LONGER_NEEDED(TRACTOR3)
		SET_MODEL_AS_NO_LONGER_NEEDED(EMPEROR3)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_HILLBILLY_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_HILLBILLY_02)
		
		INT i
		
		REPEAT COUNT_OF(pedCop) i
			IF DOES_ENTITY_EXIST(pedCop[i])
				IF NOT IS_PED_INJURED(pedCop[i])
					DISABLE_PED_PAIN_AUDIO(pedCop[i], TRUE)
					STOP_PED_SPEAKING(pedCop[i], TRUE)
					SET_ENTITY_HEALTH(pedCop[i], 0)
				ENDIF
				
				SET_PED_AS_NO_LONGER_NEEDED(pedCop[i])
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehCop) i
			IF DOES_ENTITY_EXIST(vehCop[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCop[i])
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(objBarrier) i
			IF DOES_ENTITY_EXIST(objBarrier[i])
				SET_OBJECT_AS_NO_LONGER_NEEDED(objBarrier[i])
			ENDIF
		ENDREPEAT
		
		SAFE_DELETE_OBJECT(objGarage)
		
		//Wanted
		SET_FAKE_WANTED_LEVEL(0)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC cutsceneFinale()
	IF INIT_STAGE()
		//Replay 
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayFinale), "stageFinale")
		
		//Stats
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, PRO_GETAWAY_TIME)
		
		//Player Control
		//SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Brad Balaclava + Mask
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_FEET, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_HAIR, 5, 0)
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
		CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
		
		//Cover
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<3550.8804, -4691.8628, 113.40>>)
			covPoint[0] = ADD_COVER_POINT(<<3550.82, -4691.85, 113.40>>, 0.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_180)
		ENDIF
		
		//Component Variations
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 0, 0)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		REQUEST_CUTSCENE("PRO_MCS_6", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			IF NOT IS_PED_INJURED(pedGetaway)
				SET_PED_POSITION(pedGetaway, vCar, 0.0, FALSE)
				EXPLODE_PED_HEAD(pedGetaway)
			ENDIF
			
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
			
			IF NOT IS_PED_IN_VEHICLE(playerPedMichael, vehCar)
				SET_PED_INTO_VEHICLE(playerPedMichael, vehCar)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(playerPedTrevor, vehCar)
				SET_PED_INTO_VEHICLE(playerPedTrevor, vehCar, VS_BACK_LEFT)
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(pedBrad, vehCar)
				SET_PED_INTO_VEHICLE(pedBrad, vehCar, VS_BACK_RIGHT)
			ENDIF
			
			SET_PED_POSITION(playerPedID, <<3539.7244, -4718.3975, 111.8996>>, 346.1237)
			
			SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_LEFT)
			SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(1000)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - PRO_MCS_6
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", playerPedMichael)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Brad", pedBrad)
		ENDIF
		
		IF GET_GAME_TIMER() > iTrainHornTimer
			IF sIDTrainHorn != -1
				STOP_SOUND(sIDTrainHorn)	PRINTLN("STOP_SOUND | sIDTrainHorn")
				RELEASE_SOUND_ID(sIDTrainHorn)
				sIDTrainHorn = -1
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_TRAIN_CRASH)
			IF GET_CUTSCENE_TIME() > ROUND(3.500000 * 1000.0)
				PLAY_AUDIO(PROLOGUE_TEST_TRAIN_CRASH)
				
				LOAD_AUDIO(PROLOGUE_TEST_BRAD_DOWN)
				
				SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_TRAIN_CRASH, TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_PROLOGUE_TEST_BRAD_DOWN)
			IF GET_CUTSCENE_TIME() > ROUND(26.200001 * 1000.0)
				PLAY_AUDIO(PROLOGUE_TEST_BRAD_DOWN)
				
				SET_LABEL_AS_TRIGGERED(TRIGGER_PROLOGUE_TEST_BRAD_DOWN, TRUE)
			ENDIF
		ENDIF
		
		//Weapon Assets
		REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE, ENUM_TO_INT(WRF_REQUEST_COVER_ANIMS))
		REQUEST_WEAPON_ASSET(WEAPONTYPE_SNIPERRIFLE)
		
		SWITCH iCutsceneStage
			CASE 0
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SNIPERRIFLE)
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
						UPDATE_PED_REFERENCES()
						
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
						
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					ENDIF
					
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
					
					CLEAR_TEXT()
					
					SET_VEHICLE_ENGINE_HEALTH(vehCar, 1000.0)
					SET_VEHICLE_PETROL_TANK_HEALTH(vehCar, 1000.0)
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(playerPedID, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
					ENDIF
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_TREVOR), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(playerPedID, "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(pedBrad, "Brad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_BRAD, CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "getaway_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_MICHAEL] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(playerPedMichael, wtCarbineRifle)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_MICHAEL], "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(playerPedTrevor, wtCarbineRifle)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_BRAD] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedBrad, wtShotgun)
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_BRAD], "Brads_Shotgun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					objWeapon[WEAPON_DAVE] = CREATE_WEAPON_OBJECT(WEAPONTYPE_SNIPERRIFLE, INFINITE_AMMO, GET_ENTITY_COORDS(playerPedID) - <<0.0, 0.0, 10.0>>, TRUE)
					
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon[WEAPON_DAVE], WEAPONCOMPONENT_SNIPERRIFLE_CLIP_01)
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon[WEAPON_DAVE], WEAPONCOMPONENT_AT_SCOPE_LARGE)					
					
					REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_DAVE], "Daves_Rifle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					IF NOT DOES_ENTITY_EXIST(objBag[TREVOR_BAG_DROPPED])
						objBag[TREVOR_BAG_DROPPED] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<3547.3811, -4695.8462, 112.9602>>)
						SET_ENTITY_COORDS(objBag[TREVOR_BAG_DROPPED], <<3547.3811, -4695.8462, 112.9602>>)
						SET_ENTITY_ROTATION(objBag[TREVOR_BAG_DROPPED], <<0.0000, 0.0000, -57.9600>>)
						FREEZE_ENTITY_POSITION(objBag[TREVOR_BAG_DROPPED], TRUE)
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(objBag[TREVOR_BAG_DROPPED], "Trevors_Heist_Bag", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					//Fix car door/window
					SET_VEHICLE_DOOR_CONTROL(vehCar, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 0.0)
					//FIX_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE()
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
					ENDIF
					
					//Radar
					bRadar = FALSE
					
					//Clear Area
					CLEAR_AREA(<<3549.7505, -4660.5261, 113.5495>>, 1000.0, TRUE)
					
					REMOVE_IPL("DES_ProTree_start_lod")
					
					SET_PAD_CAN_SHAKE_DURING_CUTSCENE(TRUE)
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(1000)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TrainCrash)
					IF GET_CUTSCENE_TIME() > ROUND(2.702420 * 1000.0)
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 256)
						
						SET_LABEL_AS_TRIGGERED(TrainCrash, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TreeCrash)
					IF GET_CUTSCENE_TIME() > ROUND(3.946786 * 1000.0)
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 256)
						
						//Michael Injuries
						APPLY_PED_BLOOD_SPECIFIC(playerPedMichael, ENUM_TO_INT(PDZ_HEAD), 0.383, 0.500, 320.966, 1.0, 1, 0.0, "stab")
						APPLY_PED_BLOOD_SPECIFIC(playerPedMichael, ENUM_TO_INT(PDZ_HEAD), 0.401, 0.461, 322.404, 1.0, 3, 0.0, "stab")
						APPLY_PED_BLOOD_SPECIFIC(playerPedMichael, ENUM_TO_INT(PDZ_HEAD), 0.455, 0.772, 305.206, 1.0, 3, 0.0, "stab") 
						APPLY_PED_BLOOD_SPECIFIC(playerPedMichael, ENUM_TO_INT(PDZ_HEAD), 0.515, 0.467, 316.088, 1.0, 4, 0.0, "BulletSmall") 
						
						//Trevor Injuries
//						APPLY_PED_BLOOD_SPECIFIC(playerPedTrevor, ENUM_TO_INT(PDZ_HEAD), 0.503, 0.383, 322.372, 1.0, 3, 0.0, "BulletSmall")
//						APPLY_PED_BLOOD_SPECIFIC(playerPedTrevor, ENUM_TO_INT(PDZ_HEAD), 0.533, 0.371, 321.534, 1.0, 3, 0.0, "ShotgunSmall")
						APPLY_PED_BLOOD_SPECIFIC(playerPedTrevor, ENUM_TO_INT(PDZ_HEAD), 0.533, 0.629, 306.584, 1.0, 3, 0.0, "ShotgunSmall") 
						APPLY_PED_BLOOD_SPECIFIC(playerPedTrevor, ENUM_TO_INT(PDZ_HEAD), 0.479, 0.467, 318.127, 1.0, 6, 0.0, "ShotgunSmall") 
						
						SET_LABEL_AS_TRIGGERED(TreeCrash, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(RadiatorSteam)
					IF GET_CUTSCENE_TIME() > ROUND(5.333333 * 1000.0)
						//Particles
						IF HAS_PTFX_ASSET_LOADED()
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRadiator)
								ptfxRadiator = START_PARTICLE_FX_LOOPED_AT_COORD("scr_pro_car_steam", <<3530.8, -4717.9, 113.1>>, <<-3.3, 0.0, -54.3>>)
								
								SET_LABEL_AS_TRIGGERED(RadiatorSteam, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
//				IF bVideoRecording
//					IF GET_CUTSCENE_TIME() > ROUND(5.633000 * 1000.0)
//					AND GET_CUTSCENE_TIME() < ROUND(6.633000 * 1000.0)
//						REPLAY_STOP_EVENT()
//						
//						bVideoRecording = FALSE
//					ENDIF
//				ENDIF
//				
//				IF NOT bVideoRecording
//					IF GET_CUTSCENE_TIME() > ROUND(33.838002 * 1000.0)
//					AND GET_CUTSCENE_TIME() < ROUND(34.838002 * 1000.0)
//						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGH)
//						
//						bVideoRecording = TRUE
//					ENDIF
//				ENDIF
				
//				IF bVideoRecording
//					IF GET_CUTSCENE_TIME() > ROUND(47.659000 * 1000.0)
//						REPLAY_STOP_EVENT()
//						
//						bVideoRecording = FALSE
//					ENDIF
//				ENDIF
				
				// enveff snow brad 1950940 - Rob B
				ENTITY_INDEX entitySnowBrad
				entitySnowBrad = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Brad", CS_BRAD)
				IF NOT IS_ENTITY_DEAD(entitySnowBrad)
					IF GET_CUTSCENE_TIME() < 45701
						SET_ENABLE_PED_ENVEFF_SCALE(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), TRUE)
						SET_PED_ENVEFF_SCALE(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), 0.6)				
					ELSE
						SET_ENABLE_PED_ENVEFF_SCALE(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), TRUE)
						SET_PED_ENVEFF_SCALE(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), 0.8)
						SET_PED_ENVEFF_CPV_ADD(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySnowBrad), 0.15)				
					ENDIF
				ENDIF					
				
				//Brad Injuries
				IF NOT HAS_LABEL_BEEN_TRIGGERED(BradHeadWound)
					IF GET_CUTSCENE_TIME() > ROUND(16.133335 * 1000.0)
						ENTITY_INDEX entityBrad
						entityBrad = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Brad", CS_BRAD)
						
						IF NOT IS_ENTITY_DEAD(entityBrad)
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityBrad), ENUM_TO_INT(PDZ_HEAD), 0.479, 0.467, 318.127, 1.0, 5, 0.0, "BulletSmall")
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityBrad), ENUM_TO_INT(PDZ_HEAD), 0.521, 0.467, 315.710, 1.0, 6, 0.0, "ShotgunSmall")
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityBrad), ENUM_TO_INT(PDZ_HEAD), 0.575, 0.617, 304.560, 1.0, 4, 0.0, "ShotgunLarge") 
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityBrad), ENUM_TO_INT(PDZ_HEAD), 0.305, 0.760, 312.434, 0.0, 1, 0.0, "ShotgunSmallMonolithic") 
							
							SET_LABEL_AS_TRIGGERED(BradHeadWound, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				//Brad Shot
				IF NOT HAS_LABEL_BEEN_TRIGGERED(BradShot)
					IF GET_CUTSCENE_TIME() > ROUND(35.866669 * 1000.0)	//Brad_Bang_Bang
						ENTITY_INDEX entityBrad
						entityBrad = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Brad", CS_BRAD)
						
						IF NOT IS_ENTITY_DEAD(entityBrad)
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityBrad), ENUM_TO_INT(PDZ_TORSO), 0.898, 0.739, 277.828, 1.0, 1, 0.0, "BulletLarge")
							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entityBrad), ENUM_TO_INT(PDZ_TORSO), 0.369, 0.756, 309.838, 1.0, 4, 0.0, "BulletSmall")
							
							SET_LABEL_AS_TRIGGERED(BradShot, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(SnowCover)
					IF GET_CUTSCENE_TIME() > ROUND(6.567667 * 1000.0)
					AND HAS_CUTSCENE_CUT_THIS_FRAME()
						//IPL - Snow Tracks
						REMOVE_IPL("prologue04_cover")
						
						SET_LABEL_AS_TRIGGERED(SnowCover, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TrevorCashLost)
					IF GET_CUTSCENE_TIME() > ROUND(35.866669 * 1000.0)
						iCurrentTake -= 84000
						
						g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake	#IF IS_DEBUG_BUILD	PRINTLN("g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
						
						SET_LABEL_AS_TRIGGERED(TrevorCashLost, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelCashLost)
					IF GET_CUTSCENE_TIME() > ROUND(45.166668 * 1000.0)
						iCurrentTake = 0
						
						g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake	#IF IS_DEBUG_BUILD	PRINTLN("g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
						
						SET_LABEL_AS_TRIGGERED(MichaelCashLost, TRUE)
					ENDIF
				ENDIF
				
				//Brad
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Brad")
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedBrad, FALSE)
					
					TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue6, "lying_dead_brad", <<3550.345, -4691.505, 114.368>>, VECTOR_ZERO, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_IGNORE_GRAVITY | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
					
					IF NOT DOES_ENTITY_EXIST(objBag[TREVOR_BAG_STATIC])
						objBag[TREVOR_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<3545.9810, -4692.6260, 112.8602>>)
						SET_ENTITY_COORDS(objBag[TREVOR_BAG_STATIC], <<3545.9810, -4692.6260, 112.8602>>)
						SET_ENTITY_ROTATION(objBag[TREVOR_BAG_STATIC], <<-103.3200, 0.0000, -115.2000>>)
						FREEZE_ENTITY_POSITION(objBag[TREVOR_BAG_STATIC], TRUE)
					ENDIF
					
					REMOVE_ALL_PED_WEAPONS(pedBrad)
					
					STOP_PED_SPEAKING(pedBrad, TRUE)
				ENDIF
				
				//Player Anim
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					IF NOT HAS_LABEL_BEEN_TRIGGERED(MichaelDeadAnim)
						IF NOT IS_ENTITY_PLAYING_ANIM(playerPedMichael, sAnimDictPrologue6, "lying_dead_player0")
							sceneMichaelDead = CREATE_SYNCHRONIZED_SCENE(<<3550.345, -4691.505, 114.368>>, VECTOR_ZERO)
							TASK_SYNCHRONIZED_SCENE(playerPedMichael, sceneMichaelDead, sAnimDictPrologue6, "lying_dead_player0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_FIRE | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_IMPACT_OBJECT | RBF_MELEE | RBF_RUBBER_BULLET | RBF_FALLING | RBF_WATER_JET | RBF_DROWNING | RBF_ALLOW_BLOCK_DEAD_PED)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneMichaelDead, TRUE)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedMichael)
							
							SET_LABEL_AS_TRIGGERED(MichaelDeadAnim, TRUE)
						ENDIF
					ENDIF
					
					STOP_PED_SPEAKING(playerPedMichael, TRUE)
					
					IF NOT DOES_ENTITY_EXIST(objBag[MICHAEL_BAG_STATIC])
						objBag[MICHAEL_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<3547.3811, -4695.8462, 112.9602>>)
						SET_ENTITY_COORDS(objBag[MICHAEL_BAG_STATIC], <<3547.3811, -4695.8462, 112.9602>>)
						SET_ENTITY_ROTATION(objBag[MICHAEL_BAG_STATIC], <<0.0000, 0.0000, -57.9600>>)
						FREEZE_ENTITY_POSITION(objBag[MICHAEL_BAG_STATIC], TRUE)
					ENDIF
					
					IF NOT HAS_PED_GOT_WEAPON(playerPedMichael, wtCarbineRifle)
						GIVE_WEAPON_TO_PED(playerPedMichael, wtCarbineRifle, 500, TRUE)
					ENDIF
					
					SET_CURRENT_PED_WEAPON(playerPedMichael, wtCarbineRifle, TRUE)
				ENDIF
				
				//Player Anim
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					SET_PED_POSITION(playerPedTrevor, <<3550.82, -4691.85, 113.40>>, 90.0, FALSE)
					
					SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), FALSE)
					
//					SET_PED_LEG_IK_MODE(playerPedTrevor, LEG_IK_FULL)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(playerPedTrevor, <<3550.82, -4691.85, 113.40>>, -1, FALSE, 0.0, TRUE, TRUE, covPoint[0])
//					FORCE_INSTANT_LEG_IK_SETUP(playerPedTrevor)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedTrevor)
					
					IF NOT HAS_PED_GOT_WEAPON(playerPedTrevor, wtCarbineRifle)
						GIVE_WEAPON_TO_PED(playerPedTrevor, wtCarbineRifle, 500, TRUE)
					ENDIF
					
					SET_CURRENT_PED_WEAPON(playerPedTrevor, wtCarbineRifle, TRUE)
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(-37.5)
						SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(-5.0)
						
						SET_LABEL_AS_TRIGGERED(FirstPersonRelativeHeadingSpam, TRUE)
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(29.6 - GET_ENTITY_HEADING(playerPedTrevor))
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED(FirstPersonRelativeHeadingSpam)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-37.5)	PRINTLN("First Person Cam Active!")
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-5.0)
				ENDIF
				
				//Bag
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_Heist_Bag")
					FREEZE_ENTITY_POSITION(objBag[TREVOR_BAG_DROPPED], TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("FREEZE_ENTITY_POSITION(objBag[TREVOR_BAG_DROPPED], TRUE)")	#ENDIF
				ENDIF
				
				//Weapon
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_weapon")
					FREEZE_ENTITY_POSITION(objWeapon[WEAPON_MICHAEL], TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("FREEZE_ENTITY_POSITION(objWeapon[WEAPON_MICHAEL], TRUE)")	#ENDIF
					//GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_MICHAEL], playerPedMichael)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
					GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_TREVOR], playerPedTrevor)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Brads_Shotgun")
					SET_ENTITY_ROTATION(objWeapon[WEAPON_BRAD], <<90.0, 0.0, 0.0>>)
					FREEZE_ENTITY_POSITION(objWeapon[WEAPON_BRAD], TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("FREEZE_ENTITY_POSITION(objWeapon[WEAPON_BRAD], TRUE)")	#ENDIF
					//GIVE_WEAPON_OBJECT_TO_PED(objWeapon[WEAPON_TREVOR], playerPedTrevor)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Daves_Rifle")
					SAFE_DELETE_OBJECT(objWeapon[WEAPON_DAVE])
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(CopCarsPlayback)
					IF HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD2, POLICEOLD2)
						IF IS_CUTSCENE_PLAYING()
							IF GET_CUTSCENE_TIME() > ROUND(55.766670 * 1000.0)
								IF NOT DOES_ENTITY_EXIST(vehCop[16])
									SPAWN_VEHICLE(vehCop[16], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(027, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(027, 0.0, sCarrec)))
									SET_VEHICLE_SIREN(vehCop[16], TRUE)
									SET_SIREN_WITH_NO_DRIVER(vehCop[16], TRUE)
									createCop(0)
									createCop(1)
									SET_PED_INTO_VEHICLE(pedCop[0], vehCop[16])
									SET_PED_INTO_VEHICLE(pedCop[1], vehCop[16], VS_FRONT_RIGHT)
									START_PLAYBACK_RECORDED_VEHICLE(vehCop[16], 027, sCarrec)
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[16], 500)
								ENDIF
								
								IF NOT DOES_ENTITY_EXIST(vehCop[17])
									SPAWN_VEHICLE(vehCop[17], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(028, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(028, 0.0, sCarrec)))
									SET_VEHICLE_SIREN(vehCop[17], TRUE)
									SET_SIREN_WITH_NO_DRIVER(vehCop[17], TRUE)
									createCop(2)
									createCop(3)
									SET_PED_INTO_VEHICLE(pedCop[2], vehCop[17])
									SET_PED_INTO_VEHICLE(pedCop[3], vehCop[17], VS_FRONT_RIGHT)
									START_PLAYBACK_RECORDED_VEHICLE(vehCop[17], 028, sCarrec)
									SET_PLAYBACK_SPEED(vehCop[17], 0.75)
								ENDIF
								
								IF NOT DOES_ENTITY_EXIST(vehCop[18])
									SPAWN_VEHICLE(vehCop[18], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(029, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(029, 0.0, sCarrec)))
									SET_VEHICLE_SIREN(vehCop[18], TRUE)
									SET_SIREN_WITH_NO_DRIVER(vehCop[18], TRUE)
									createCop(4)
									createCop(5)
									SET_PED_INTO_VEHICLE(pedCop[4], vehCop[18])
									SET_PED_INTO_VEHICLE(pedCop[5], vehCop[18], VS_FRONT_RIGHT)
									START_PLAYBACK_RECORDED_VEHICLE(vehCop[18], 029, sCarrec)
								ENDIF
								
								//Wanted
								SET_FAKE_WANTED_LEVEL(5)
								
								SET_LABEL_AS_TRIGGERED(CopCarsPlayback, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					bCutsceneSkipped = TRUE
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					IF bCutsceneSkipped
						WHILE NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
						DO_SCREEN_FADE_IN(1000)	#IF IS_DEBUG_BUILD	PRINTLN("DO_SCREEN_FADE_IN")	#ENDIF
					ENDIF
					REPLAY_STOP_EVENT()
					bCutsceneSkipped = FALSE
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					ADVANCE_STAGE()
				ENDIF
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		SET_PAD_CAN_SHAKE_DURING_CUTSCENE(FALSE)
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		//Particles
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxRadiator)
			STOP_PARTICLE_FX_LOOPED(ptfxRadiator)
		ENDIF
		
		IF sIDTrainBell != -1
			STOP_SOUND(sIDTrainBell)
			RELEASE_SOUND_ID(sIDTrainBell)
			sIDTrainBell = -1
		ENDIF
		
		IF sIDTrainHorn != -1
			STOP_SOUND(sIDTrainHorn)
			RELEASE_SOUND_ID(sIDTrainHorn)
			sIDTrainHorn = -1
		ENDIF
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("Prologue_Train_Sounds")
		
		SAFE_DELETE_OBJECT(objWeapon[WEAPON_DAVE])
		SAFE_DELETE_OBJECT(objWeapon[WEAPON_TREVOR])
		
		SAFE_REMOVE_BLIP(blipMichael)
		SAFE_REMOVE_BLIP(blipBuddy)
		SAFE_REMOVE_BLIP(blipCar)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC Finale()
	IF INIT_STAGE()
		//Replay 
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayFinale), "stageFinale", TRUE)
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Take
		iCurrentTake = 0
		
		g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake	#IF IS_DEBUG_BUILD	PRINTLN("g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = ", g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE])	#ENDIF
		
		//Wanted
		SET_FAKE_WANTED_LEVEL(5)
		
		//Brad Balaclava + Mask
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_FEET, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_HAIR, 5, 0)
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL, 0, 0)
		
		//Balaclava
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
		CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
		
		//Radar
		bRadar = TRUE
		
		//Up the difficulty of the stage by increasing the cops accuracy each time they respawn
		iDeadCopPlus = 0
		
		//IPL - Snow Tracks
		REMOVE_IPL("prologue04_cover")
		
		//Navmesh
		iNavMeshBlockingFarm = ADD_NAVMESH_BLOCKING_OBJECT(<<3551.478, -4690.381, 112.995>>, <<15.0, 15.0, 6.0>>, 0.017)	//Stops enemies getting to close to the player
		iNavMeshBlockingCopPath1 = ADD_NAVMESH_BLOCKING_OBJECT(<<3518.556885, -4670.073730, 112.225830>>, <<18.0, 3.0, 6.0>>, (-1.39 / 360) * 6.28)	//Stops cops getting run over by the cop car
		iNavMeshBlockingCopPath2 = ADD_NAVMESH_BLOCKING_OBJECT(<<3528.091309, -4673.379395, 112.289085>>, <<2.0, 8.0, 6.0>>, 0.0)
		
		//Component Variations
		SET_PED_COMPONENT_VARIATION(playerPedMichael, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedBrad, PED_COMP_SPECIAL2, 0, 0)
		
		//Friendly Fire Relationship Group
		SET_PED_RELATIONSHIP_GROUP_HASH(pedBrad, relGroupFriendlyFire)
		
		SAFE_REMOVE_BLIP(blipMichael)
		SAFE_REMOVE_BLIP(blipBuddy)
		SAFE_REMOVE_BLIP(blipCar)
		
		SET_LABEL_AS_TRIGGERED(PRO_Hostage, FALSE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GRAB_WOMAN")
			START_AUDIO_SCENE("PROLOGUE_GRAB_WOMAN")
		ENDIF
		
		SPAWN_VEHICLE(vehTrain[0], FREIGHT,  <<3461.9600, -4732.6328, 113.0537 - 1.60>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[0])
		FREEZE_ENTITY_POSITION(vehTrain[0], TRUE)
		SPAWN_VEHICLE(vehTrain[1], FREIGHTCONT1, <<3478.5854, -4734.4746, 112.1684 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[1])
		FREEZE_ENTITY_POSITION(vehTrain[1], TRUE)
		SPAWN_VEHICLE(vehTrain[2], FREIGHTCONT2, <<3494.1167, -4736.1992, 112.2295 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[2])
		FREEZE_ENTITY_POSITION(vehTrain[2], TRUE)
		SPAWN_VEHICLE(vehTrain[3], FREIGHTCONT2, <<3509.6353, -4737.9224, 112.2905 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[3])
		FREEZE_ENTITY_POSITION(vehTrain[3], TRUE)
		SPAWN_VEHICLE(vehTrain[4], FREIGHTCONT1, <<3525.1606, -4739.6465, 112.3516 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[4])
		FREEZE_ENTITY_POSITION(vehTrain[4], TRUE)
		SPAWN_VEHICLE(vehTrain[5], FREIGHTCONT2, <<3540.6980, -4741.3716, 112.4128 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[5])
		FREEZE_ENTITY_POSITION(vehTrain[5], TRUE)
		SPAWN_VEHICLE(vehTrain[6], FREIGHTCONT1, <<3556.2366, -4743.0972, 112.4739 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[6])
		FREEZE_ENTITY_POSITION(vehTrain[6], TRUE)
		SPAWN_VEHICLE(vehTrain[7], FREIGHTCONT2, <<3571.7502, -4744.8198, 112.5349 - 0.50>>, 83.6637)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTrain[7])
		FREEZE_ENTITY_POSITION(vehTrain[7], TRUE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Clear Area
			CLEAR_AREA(<<3549.7505, -4660.5261, 113.5495>>, 1000.0, TRUE)
			
			IF NOT IS_PED_INJURED(pedGetaway)
				SET_PED_POSITION(pedGetaway, vCar, 0.0, FALSE)
				EXPLODE_PED_HEAD(pedGetaway)
			ENDIF
			
			REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
				UPDATE_PED_REFERENCES()
				
				SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
			
			SET_VEHICLE_POSITION(vehCar, <<3532.2458, -4716.9336, 111.8443>>, 125.7299)
			
			SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_FRONT_LEFT)
			SMASH_VEHICLE_WINDOW(vehCar, SC_WINDOW_REAR_LEFT)
			
			//Brad
			SET_PED_POSITION(pedBrad, <<3550.345, -4691.488, 113.090>>, 351.6286, FALSE)
			
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedBrad, FALSE)
			
			TASK_PLAY_ANIM_ADVANCED(pedBrad, sAnimDictPrologue6, "lying_dead_brad", <<3550.345, -4691.505, 114.368>>, VECTOR_ZERO, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_IGNORE_GRAVITY | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBrad)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBrad, TRUE)
			
			IF NOT DOES_ENTITY_EXIST(objBag[TREVOR_BAG_STATIC])
				objBag[TREVOR_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<3545.9810, -4692.6260, 112.8602>>)
				SET_ENTITY_COORDS(objBag[TREVOR_BAG_STATIC], <<3545.9810, -4692.6260, 112.8602>>)
				SET_ENTITY_ROTATION(objBag[TREVOR_BAG_STATIC], <<-103.3200, 0.0000, -115.2000>>)
				FREEZE_ENTITY_POSITION(objBag[TREVOR_BAG_STATIC], TRUE)
			ENDIF
			
			STOP_PED_SPEAKING(pedBrad, TRUE)
			
			//Player Anim
			SET_PED_POSITION(playerPedMichael, <<3543.8826, -4696.1963, 112.6563>>, 351.6286, FALSE)
			
			sceneMichaelDead = CREATE_SYNCHRONIZED_SCENE(<<3550.345, -4691.505, 114.368>>, VECTOR_ZERO)
			TASK_SYNCHRONIZED_SCENE(playerPedMichael, sceneMichaelDead, sAnimDictPrologue6, "lying_dead_player0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_FIRE | RBF_ELECTROCUTION | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_IMPACT_OBJECT | RBF_MELEE | RBF_RUBBER_BULLET | RBF_FALLING | RBF_WATER_JET | RBF_DROWNING | RBF_ALLOW_BLOCK_DEAD_PED)
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneMichaelDead, TRUE)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedMichael)
			
			STOP_PED_SPEAKING(playerPedMichael, TRUE)
			
			IF NOT HAS_PED_GOT_WEAPON(playerPedMichael, wtCarbineRifle)
				GIVE_WEAPON_TO_PED(playerPedMichael, wtCarbineRifle, 500, TRUE)
			ENDIF
			
			SET_CURRENT_PED_WEAPON(playerPedMichael, wtCarbineRifle, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedMichael, TRUE)
			
			IF NOT DOES_ENTITY_EXIST(objBag[MICHAEL_BAG_STATIC])
				objBag[MICHAEL_BAG_STATIC] = CREATE_OBJECT(PROP_CS_HEIST_BAG_02, <<3547.3811, -4695.8462, 112.9602>>)
				SET_ENTITY_COORDS(objBag[MICHAEL_BAG_STATIC], <<3547.3811, -4695.8462, 112.9602>>)
				SET_ENTITY_ROTATION(objBag[MICHAEL_BAG_STATIC], <<0.0000, 0.0000, -57.9600>>)
				FREEZE_ENTITY_POSITION(objBag[MICHAEL_BAG_STATIC], TRUE)
			ENDIF
			
			//Cover
			IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<3550.82, -4691.85, 113.40>>)
				covPoint[0] = ADD_COVER_POINT(<<3550.82, -4691.85, 113.40>>, 0.0, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_180)
			ENDIF
			
			//Player Anim
			SET_PED_POSITION(playerPedTrevor, <<3550.82, -4691.85, 113.40>>, 0.0, FALSE)
			
			SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_HAIR, 7, 0)
			SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_BERD, 1, 0)
			SET_PED_COMPONENT_VARIATION(playerPedTrevor, PED_COMP_SPECIAL, 0, 0)
			CLEAR_PED_PROP(playerPedTrevor, ANCHOR_EYES)
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TRV_PRO_MASK_REMOVED, TRUE)
			
			TASK_PUT_PED_DIRECTLY_INTO_COVER(playerPedTrevor, <<3550.82, -4691.85, 113.40>>, -1, FALSE, 0.0, TRUE, TRUE, covPoint[0])
			FORCE_PED_AI_AND_ANIMATION_UPDATE(playerPedTrevor, TRUE)
			
			IF NOT HAS_PED_GOT_WEAPON(playerPedTrevor, wtCarbineRifle)
				GIVE_WEAPON_TO_PED(playerPedTrevor, wtCarbineRifle, 500, TRUE)
			ENDIF
			
			SET_CURRENT_PED_WEAPON(playerPedTrevor, wtCarbineRifle, TRUE)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(playerPedTrevor, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(29.603371 - GET_ENTITY_HEADING(playerPedTrevor))
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Score
			PLAY_AUDIO(PROLOGUE_TEST_FINALE_RT)	//PROLOGUE_TEST_GRAB_WOMAN
		ENDIF
	ELSE
		//Request Cutscene Variations - PRO_MCS_7_Concat
		IF iCutsceneStage < 3
			//Cutscene
			REQUEST_CUTSCENE("PRO_MCS_7_Concat")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", playerPedTrevor)
			ENDIF
		ENDIF
		
		INT i
		
		SWITCH iCutsceneStage
			CASE 0
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					UPDATE_PED_REFERENCES()
				ENDIF
				
				//Friendly Fire Relationship Group
				IF DOES_ENTITY_EXIST(playerPedMichael)
					SET_PED_RELATIONSHIP_GROUP_HASH(playerPedMichael, relGroupFriendlyFire)
				ENDIF
				
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, playerPedMichael, "MICHAEL")
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, playerPedTrevor, "TREVOR")
				
				IF NOT HAS_PED_GOT_WEAPON(playerPedID, wtCarbineRifle)
					GIVE_WEAPON_TO_PED(playerPedID, wtCarbineRifle, 500, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("GIVE_WEAPON_TO_PED")	#ENDIF
				ENDIF
				
				IF GET_AMMO_IN_PED_WEAPON(playerPedID, wtCarbineRifle) < (GET_WEAPON_CLIP_SIZE(wtCarbineRifle) * 2)
					SET_PED_AMMO(playerPedID, wtCarbineRifle, (GET_WEAPON_CLIP_SIZE(wtCarbineRifle) * 2))
				ENDIF
				
				SET_CURRENT_PED_WEAPON(playerPedID, wtCarbineRifle, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("SET_CURRENT_PED_WEAPON")	#ENDIF
				
				//Radar
				bRadar = TRUE
				
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCar, FALSE)
				
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				
				ACTIVATE_PHYSICS(vehCar)
				
				//Score
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				
				//Score
				LOAD_AUDIO(PROLOGUE_TEST_FINAL_CUTSCENE)
				
				PLAY_AUDIO(PROLOGUE_TEST_GRAB_WOMAN)
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 1
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				
				bReplaySkip = FALSE
				
				//Farm Cop Cars
				IF NOT HAS_LABEL_BEEN_TRIGGERED(CopCarsPlayback)
					IF NOT DOES_ENTITY_EXIST(vehCop[16])
						SPAWN_VEHICLE(vehCop[16], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(027, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(027, sCarrec), sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(027, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(027, sCarrec), sCarrec)))
						SET_VEHICLE_SIREN(vehCop[16], TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCop[16], TRUE)
						createCop(0)
						createCop(1)
						SET_PED_ACCURACY(pedCop[0], 5)
						SET_PED_ACCURACY(pedCop[1], 5)
						SET_PED_INTO_VEHICLE(pedCop[0], vehCop[16])
						SET_PED_INTO_VEHICLE(pedCop[1], vehCop[16], VS_FRONT_RIGHT)
						START_PLAYBACK_RECORDED_VEHICLE(vehCop[16], 027, sCarrec)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCop[16], 500)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(vehCop[17])
						SPAWN_VEHICLE(vehCop[17], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(028, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(028, sCarrec), sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(028, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(028, sCarrec), sCarrec)))
						SET_VEHICLE_SIREN(vehCop[17], TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCop[17], TRUE)
						createCop(2)
						createCop(3)
						SET_PED_ACCURACY(pedCop[2], 5)
						SET_PED_ACCURACY(pedCop[3], 5)
						SET_PED_INTO_VEHICLE(pedCop[2], vehCop[17])
						SET_PED_INTO_VEHICLE(pedCop[3], vehCop[17], VS_FRONT_RIGHT)
						START_PLAYBACK_RECORDED_VEHICLE(vehCop[17], 028, sCarrec)
						SET_PLAYBACK_SPEED(vehCop[17], 0.75)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(vehCop[18])
						SPAWN_VEHICLE(vehCop[18], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(029, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(029, sCarrec), sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(029, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(029, sCarrec), sCarrec)))
						SET_VEHICLE_SIREN(vehCop[18], TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCop[18], TRUE)
						createCop(4)
						createCop(5)
						SET_PED_ACCURACY(pedCop[4], 5)
						SET_PED_ACCURACY(pedCop[5], 5)
						SET_PED_INTO_VEHICLE(pedCop[4], vehCop[18])
						SET_PED_INTO_VEHICLE(pedCop[5], vehCop[18], VS_FRONT_RIGHT)
						START_PLAYBACK_RECORDED_VEHICLE(vehCop[18], 029, sCarrec)
					ENDIF
					
					SET_LABEL_AS_TRIGGERED(CopCarsPlayback, TRUE)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(POLICEOLD2)
				
				REPEAT 3 i
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[i + 16], FALSE)
				ENDREPEAT
				
				//Cops
				REPEAT 6 i
					IF i = 0 OR i = 3
						GIVE_WEAPON_TO_PED(pedCop[i], wtSMG, INFINITE_AMMO, TRUE)
					ENDIF
					
					IF i = 2 OR i = 5
						GIVE_WEAPON_TO_PED(pedCop[i], wtShotgun, INFINITE_AMMO, TRUE)
					ENDIF
				ENDREPEAT
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 2
				SET_PED_MAX_MOVE_BLEND_RATIO(playerPedID, 2.5)
				
				IF GET_PROFILE_SETTING(PROFILE_ACTION_AUTO_AIM) = 0
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP_ADV(PROHLP_AIM3, "PROHLP_AIM3")
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(playerPedID, <<3567.723633, -4664.949707, 111.653862>>, <<3557.713623, -4704.931152, 118.520966>>, 35.0)
					eMissionFail = failRanAway
					
					missionFailed()
				ENDIF
				
				//Farm Cop Car Explodes
				IF TIMERA() > 24000
					IF NOT IS_ENTITY_DEAD(vehCop[16])
					AND (GET_ENTITY_HEALTH(vehCop[16]) < 1000
					OR GET_VEHICLE_PETROL_TANK_HEALTH(vehCop[16]) < 1000.0
					OR GET_VEHICLE_ENGINE_HEALTH(vehCop[16]) < 1000.0)
						EXPLODE_VEHICLE(vehCop[16], TRUE, TRUE)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[16])) < 10.0
							SHAKE_GAMEPLAY_CAM("MEDIUM_EXPLOSION_SHAKE", 0.25)
						ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPedID), GET_ENTITY_COORDS(vehCop[16])) >= 10.0
							SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.25)
						ENDIF
					ENDIF
				ENDIF
				
				//Farm Cop Cars Backup
				IF TIMERA() > 18000
					IF NOT HAS_LABEL_BEEN_TRIGGERED(CopCarsBackupPlayback)
						IF NOT DOES_ENTITY_EXIST(vehCop[19])
							SPAWN_VEHICLE(vehCop[19], POLICEOLD2, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(032, 0.0, sCarrec), GET_HEADING_FROM_VECTOR(GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(032, 0.0, sCarrec)))
							SET_VEHICLE_SIREN(vehCop[19], TRUE)
							SET_SIREN_WITH_NO_DRIVER(vehCop[19], TRUE)
							createCop(9)
							createCop(10)
							SET_PED_ACCURACY(pedCop[9], 5)
							SET_PED_ACCURACY(pedCop[10], 5)
							SET_PED_INTO_VEHICLE(pedCop[9], vehCop[19])
							SET_PED_INTO_VEHICLE(pedCop[10], vehCop[19], VS_FRONT_RIGHT)
							START_PLAYBACK_RECORDED_VEHICLE(vehCop[19], 032, sCarrec)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCop[19], FALSE)
							GIVE_WEAPON_TO_PED(pedCop[10], wtShotgun, INFINITE_AMMO, TRUE)
						ENDIF
						
						REPEAT COUNT_OF(pedCop) i
							IF NOT IS_PED_INJURED(pedCop[i])
								REMOVE_PED_DEFENSIVE_AREA(pedCop[i])
								SET_PED_COMBAT_ATTRIBUTES(pedCop[i], CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
								SET_PED_COMBAT_MOVEMENT(pedCop[i], CM_WILLADVANCE)
								
								SET_PED_COMBAT_ATTRIBUTES(pedCop[i], CA_CAN_CHARGE, TRUE)
							ENDIF
						ENDREPEAT
						
						SET_LABEL_AS_TRIGGERED(CopCarsBackupPlayback, TRUE)
					ENDIF
				ENDIF
				
				IF REQUEST_SCRIPT_AUDIO_BANK("SIREN_DISTANT")
					IF TIMERA() > 23000
						IF NOT HAS_LABEL_BEEN_TRIGGERED(Cops_Arrive_2)
							sIDDistantSirensFarm = GET_SOUND_ID()
							PLAY_SOUND_FROM_COORD(sIDDistantSirensFarm, "Cops_Arrive_2", <<3532.4004, -4676.8501, 113.2428>>, "Prologue_Sounds")
							
							SET_LABEL_AS_TRIGGERED(Cops_Arrive_2, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF (TIMERA() > 33000
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
				OR NOT IS_ENTITY_AT_COORD(playerPedTrevor, <<3550.010742, -4691.855469, 114.759544>>, <<2.5, 3.0, 2.0>>)
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						SAFE_REMOVE_BLIP(blipDestination)
						
						CLEAR_TEXT()
						
						//Audio Scene
						IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GRAB_WOMAN")
							STOP_AUDIO_SCENE("PROLOGUE_GRAB_WOMAN")
						ENDIF
						
						//Score
						PLAY_AUDIO(PROLOGUE_TEST_FINAL_CUTSCENE_MA)
						
						//Wanted
						SET_FAKE_WANTED_LEVEL(0)
						
						//Radar
						bRadar = FALSE
						
						SPAWN_VEHICLE(vehCop[0], POLICEOLD1, <<3493.1201, -4681.6040, 113.4866>>, 260.0987)
						SET_VEHICLE_SIREN(vehCop[0], TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCop[0], TRUE)
						SPAWN_VEHICLE(vehCop[1], POLICEOLD2, <<3492.8240, -4678.9399, 113.5952>>, 260.1201)
						SET_VEHICLE_SIREN(vehCop[1], TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCop[1], TRUE)
						SPAWN_VEHICLE(vehCop[2], POLICEOLD2, <<3491.9338, -4676.0190, 113.8171>>, 260.2887)
						SET_VEHICLE_SIREN(vehCop[2], TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehCop[2], TRUE)
						
						REGISTER_ENTITY_FOR_CUTSCENE(vehCop[0], "1st_car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(vehCop[1], "2nd_car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(vehCop[2], "3rd_car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						
						objWeapon[WEAPON_TREVOR] = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(playerPedID, wtCarbineRifle)
						
						REGISTER_ENTITY_FOR_CUTSCENE(objWeapon[WEAPON_TREVOR], "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						START_CUTSCENE()
						
						WAIT_WITH_DEATH_CHECKS(0)
						
						IF NOT IS_REPEAT_PLAY_ACTIVE()
							SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
						ENDIF
						
						//Cleanup for cutscene
						SAFE_DELETE_PED(playerPedMichael)
						
						SAFE_DELETE_VEHICLE(vehCar)
						
						REPEAT COUNT_OF(pedCop) i
							SAFE_DELETE_PED(pedCop[i])
						ENDREPEAT
						
						SAFE_DELETE_VEHICLE(vehCop[19])
						
						REPEAT COUNT_OF(vehTrain) i
							SAFE_DELETE_VEHICLE(vehTrain[i])
						ENDREPEAT
						
						SAFE_DELETE_OBJECT(objWeapon[WEAPON_MICHAEL])
						SAFE_DELETE_OBJECT(objWeapon[WEAPON_BRAD])
						SAFE_DELETE_OBJECT(objBag[MICHAEL_BAG_STATIC])
						
						UNPIN_INTERIOR(intDepot)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(RANCHERXL2)
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_SNOWCOP_01)
						SET_MODEL_AS_NO_LONGER_NEEDED(POLICEOLD1)
						SET_MODEL_AS_NO_LONGER_NEEDED(POLICEOLD2)
						SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
						SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
						SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT2)
						SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_HEIST_BAG_02)
						SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_HEIST_BAG_S_1)
						SET_MODEL_AS_NO_LONGER_NEEDED(P_CSH_STRAP_01_S)
						REMOVE_VEHICLE_RECORDING(003, sCarrec)
						REMOVE_VEHICLE_RECORDING(004, sCarrec)
						REMOVE_VEHICLE_RECORDING(005, sCarrec)
						REMOVE_VEHICLE_RECORDING(006, sCarrec)
						REMOVE_VEHICLE_RECORDING(008, sCarrec)
						REMOVE_VEHICLE_RECORDING(009, sCarrec)
						REMOVE_VEHICLE_RECORDING(011, sCarrec)
						REMOVE_VEHICLE_RECORDING(026, sCarrec)
						REMOVE_VEHICLE_RECORDING(027, sCarrec)
						REMOVE_VEHICLE_RECORDING(028, sCarrec)
						REMOVE_VEHICLE_RECORDING(029, sCarrec)
						REMOVE_VEHICLE_RECORDING(032, sCarrec)
						REMOVE_VEHICLE_RECORDING(033, sCarrec)
						REMOVE_VEHICLE_RECORDING(034, sCarrec)
						REMOVE_VEHICLE_RECORDING(035, sCarrec)
						REMOVE_VEHICLE_RECORDING(700, sCarrec)
						REMOVE_VEHICLE_RECORDING(701, sCarrec)
						REMOVE_VEHICLE_RECORDING(702, sCarrec)
						REMOVE_ANIM_DICT(sAnimDictDeepSnow)
						REMOVE_ANIM_DICT(sAnimDictMapObjects)
						REMOVE_ANIM_DICT(sAnimDictPrologue6)
						
						CLEANUP_ALL_AI_PED_BLIPS(blipStructCop)
						
						//Timecycle
						CLEAR_TIMECYCLE_MODIFIER()	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_TIMECYCLE_MODIFIER - then SET_TRANSITION_TIMECYCLE_MODIFIER")	#ENDIF
						
						IF GET_TIMECYCLE_MODIFIER_INDEX() != -1	#IF IS_DEBUG_BUILD	PRINTLN("IF GET_TIMECYCLE_MODIFIER_INDEX() != -1 - then SET_TRANSITION_TIMECYCLE_MODIFIER")	#ENDIF
							SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0)	#IF IS_DEBUG_BUILD	PRINTLN("SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0) - then SET_TRANSITION_TIMECYCLE_MODIFIER")	#ENDIF
						ENDIF
						
						SET_TRANSITION_TIMECYCLE_MODIFIER("prologue_ending_fog", 4.0)
						
						
						REPLAY_START_EVENT()
						
						iCreditsStage = 0
						
						ADVANCE_CUTSCENE()
					ENDIF
				ENDIF
			BREAK
			CASE 3
				//Rain Particles
//				IF NOT HAS_LABEL_BEEN_TRIGGERED(RainParticleOverride)
//					IF GET_CUTSCENE_TIME() > ROUND((45.43233) * 1000.0)
//					AND GET_CUTSCENE_TIME() < ROUND((45.43233 + 1.0) * 1000.0)
//					//AND HAS_CUTSCENE_CUT_THIS_FRAME()
//						SET_WEATHER_PTFX_USE_OVERRIDE_SETTINGS(TRUE)	PRINTLN("SET_WEATHER_PTFX_USE_OVERRIDE_SETTINGS(TRUE)")
//						SET_WEATHER_PTFX_OVERRIDE_CURR_LEVEL(0.001)
//						SET_WEATHER_PTFX_OVERRIDE_BOX_SIZE(<<5.0, 5.0, 5.0>>)
//						SET_WEATHER_PTFX_OVERRIDE_BOX_OFFSET(VECTOR_ZERO)
//						SET_LABEL_AS_TRIGGERED(RainParticleOverride, TRUE)
//					ENDIF
//				ELIF HAS_LABEL_BEEN_TRIGGERED(RainParticleOverride)
////					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
////					
////					DRAW_RECT(0.9, 0.1, 0.1, 0.1, 255, 0, 0, 255)
//					
//					IF GET_CUTSCENE_TIME() > ROUND((51.26567) * 1000.0)
//					//AND HAS_CUTSCENE_CUT_THIS_FRAME()
//						SET_WEATHER_PTFX_USE_OVERRIDE_SETTINGS(FALSE)	PRINTLN("SET_WEATHER_PTFX_USE_OVERRIDE_SETTINGS(FALSE)")
//						SET_LABEL_AS_TRIGGERED(RainParticleOverride, FALSE)
//					ENDIF
//				ENDIF
				
				//Scaleform
				IF NOT bRequestedCreditsScaleform
					sfCredits = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
					bRequestedCreditsScaleform = TRUE
				ENDIF
				
				//Credits
				IF HAS_SCALEFORM_MOVIE_LOADED(sfCredits)
					SWITCH iCreditsStage
						CASE 0	// - ROCKSTAR GAMES presents
							IF GET_CUTSCENE_TIME() > ROUND(59.250000 * 1000.0)
								//ADD_TEXT_TO_SINGLE_LINE("presents", "Rockstar GAMES", "$font2", "HUD_COLOUR_WHITE")
								SCALEFORM_SETUP_SINGLE_LINE("presents", 0.5, 0.5, 70, 125, "left")
								SCALEFORM_DISPLAY_SINGLE_LINE("presents", "ROCKSTAR GAMES", "$font2", "HUD_COLOUR_WHITE")
								SCALEFORM_DISPLAY_SINGLE_LINE("presents", "presents", "$font5", "HUD_COLOUR_FREEMODE")
								
								SCALEFORM_SHOW_SINGLE_LINE("presents", 0.166)
								
								iCreditsStage++
							ENDIF
						BREAK
						CASE 1
							IF GET_CUTSCENE_TIME() > ROUND(62.500000 * 1000.0)
								SCALEFORM_HIDE_SINGLE_LINE("presents", 0.166)
								
								iCreditsStage++
							ENDIF
						BREAK
						CASE 2	// - a ROCKSTAR NORTH production
							SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
							
							IF GET_CUTSCENE_TIME() > ROUND(70.000000 * 1000.0)
								//ADD_TEXT_TO_SINGLE_LINE("production", "Rockstar GAMES", "$font2", "HUD_COLOUR_WHITE")
								SCALEFORM_SETUP_SINGLE_LINE("production", 0.5, 0.5, 0, 190, "right")	//530 "left"
								SCALEFORM_DISPLAY_SINGLE_LINE("production", "a", "$font5", "HUD_COLOUR_MICHAEL")
								SCALEFORM_DISPLAY_SINGLE_LINE("production", "ROCKSTAR NORTH", "$font2", "HUD_COLOUR_WHITE")
								SCALEFORM_DISPLAY_SINGLE_LINE("production", "production", "$font5", "HUD_COLOUR_MICHAEL")
								
								SCALEFORM_SHOW_SINGLE_LINE("production", 0.166)
								
								iCreditsStage++
							ENDIF
						BREAK
						CASE 3
							IF GET_CUTSCENE_TIME() > ROUND(73.566673 * 1000.0)
								SCALEFORM_HIDE_SINGLE_LINE("production", 0.166)
								
								iCreditsStage++
							ENDIF
						BREAK
					ENDSWITCH
					
					IF IS_SCREEN_FADING_OUT()
					OR IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_IN()
						SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
					ENDIF
					
					HIDE_LOADING_ON_FADE_THIS_FRAME()
					
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					
					SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_CENTRE)
					
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfCredits, 255, 255, 255, 255)
					
					bCreditsScaleformActive = TRUE
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(EndCutsceneCleanup)
					IF GET_CUTSCENE_TIME() > ROUND(5.000000 * 1000.0)
						SAFE_DELETE_VEHICLE(vehCop[16])
						SAFE_DELETE_VEHICLE(vehCop[17])
						SAFE_DELETE_VEHICLE(vehCop[18])
						
						SAFE_DELETE_PED(pedBrad)
						
						SAFE_DELETE_OBJECT(objBag[TREVOR_BAG_STATIC])
						SAFE_DELETE_OBJECT(objBag[BRAD_BAG_STATIC])
						
						SET_LABEL_AS_TRIGGERED(EndCutsceneCleanup, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TriggerBlizzard)
					IF GET_CUTSCENE_TIME() > ROUND(9.600000 * 1000.0)	//Frame of start is 23.304815
					AND HAS_CUTSCENE_CUT_THIS_FRAME()
						REMOVE_IPL("prologue01z")
						
						SET_WEATHER_TYPE_NOW_PERSIST("BLIZZARD")
						
						SET_LABEL_AS_TRIGGERED(TriggerBlizzard, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TrevorRunsOffA)
					IF GET_CUTSCENE_TIME() > ROUND(23.600000 * 1000.0)	//Frame of start is 23.304815
						//Score
						PLAY_AUDIO(PROLOGUE_TEST_FINAL_CUTSCENE)
						
						SET_LABEL_AS_TRIGGERED(TrevorRunsOffA, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TrevorRunsOffB)
					IF GET_CUTSCENE_TIME() > ROUND(38.053001 * 1000.0)
					AND HAS_CUTSCENE_CUT_THIS_FRAME()
						SET_LABEL_AS_TRIGGERED(TrevorRunsOffB, TRUE)
						
						//Timecycle
						CLEAR_TIMECYCLE_MODIFIER()	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_TIMECYCLE_MODIFIER - Cutscene Faded Out / Cut To Funeral")	#ENDIF
						
						IF GET_TIMECYCLE_MODIFIER_INDEX() != -1	#IF IS_DEBUG_BUILD	PRINTLN("IF GET_TIMECYCLE_MODIFIER_INDEX() != -1 - Cutscene Faded Out / Cut To Funeral")	#ENDIF
							SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0)	#IF IS_DEBUG_BUILD	PRINTLN("SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0) - Cutscene Faded Out / Cut To Funeral")	#ENDIF
						ENDIF
						
						SET_WEATHER_TYPE_NOW_PERSIST("NEUTRAL")	//SET_WEATHER_TYPE_NOW_PERSIST("SNOW")
						
						LOAD_CLOUD_HAT("RAIN")
						
						REPEAT COUNT_OF(vehCop) i
							SAFE_DELETE_VEHICLE(vehCop[i])
						ENDREPEAT
					ENDIF
				ENDIF
				
				IF GET_CUTSCENE_TIME() > ROUND(38.053001 * 1000.0)
					IF NOT IS_ENTITY_AT_COORD(playerPedID, <<3254.4, -4574.3, 116.3>>, <<2.0, 2.0, 2.0>>)
						SET_PED_POSITION(playerPedID, <<3254.4, -4574.3, 116.3>>, 0.0, FALSE)
						
						FREEZE_ENTITY_POSITION(playerPedID, TRUE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
					IF bRequestedCreditsScaleform
						SCALEFORM_REMOVE_ALL_SINGLE_LINES()
						
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfCredits)
						bRequestedCreditsScaleform = FALSE
						bCreditsScaleformActive = FALSE
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					//Timecycle
					CLEAR_TIMECYCLE_MODIFIER()	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_TIMECYCLE_MODIFIER - Cutscene Ended")	#ENDIF
					
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1	#IF IS_DEBUG_BUILD	PRINTLN("IF GET_TIMECYCLE_MODIFIER_INDEX() != -1 - Cutscene Ended")	#ENDIF
						SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0)	#IF IS_DEBUG_BUILD	PRINTLN("SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0) - Cutscene Ended")	#ENDIF
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					FREEZE_ENTITY_POSITION(playerPedID, FALSE)
					
					SAFE_DELETE_OBJECT(objWeapon[WEAPON_TREVOR])
					SAFE_DELETE_PED(pedBrad)
					SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					
					//Cop Cars
					REPEAT COUNT_OF(vehCop) i
						SAFE_DELETE_VEHICLE(vehCop[i])
					ENDREPEAT
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
					
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		IF HAS_SCALEFORM_MOVIE_LOADED(sfCredits)
			IF bDebugDisplayCreditsText         
			    STRING strAlign =  GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsAlign)
			    STRING strText1 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetText1)
			    STRING strText2 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetText2)
			    STRING strText3 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetText3)
				STRING strFont1 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetFont1)
			    STRING strFont2 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetFont2)
			    STRING strFont3 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetFont3)
			    STRING strColour1 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsColour1)
				STRING strColour2 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsColour2)
				STRING strColour3 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsColour3)
				
			    SCALEFORM_SETUP_SINGLE_LINE("debug", 0.5, 0.5, fDebugCreditsBlockX, fDebugCreditsBlockY, strAlign)
				
			    IF NOT IS_STRING_NULL_OR_EMPTY(strText1)
			    AND NOT ARE_STRINGS_EQUAL(strText1, "New text widget")
			   		SCALEFORM_DISPLAY_SINGLE_LINE("debug", strText1, strFont1, strColour1)
			    ENDIF
			    
			    IF NOT IS_STRING_NULL_OR_EMPTY(strText2)
			    AND NOT ARE_STRINGS_EQUAL(strText2, "New text widget")
			    	SCALEFORM_DISPLAY_SINGLE_LINE("debug", strText2, strFont2, strColour2)
			    ENDIF
			    
			    IF NOT IS_STRING_NULL_OR_EMPTY(strText3)
			    AND NOT ARE_STRINGS_EQUAL(strText3, "New text widget")
			    	SCALEFORM_DISPLAY_SINGLE_LINE("debug", strText3, strFont3, strColour3)
			    ENDIF
			    
			    SCALEFORM_SHOW_SINGLE_LINE("debug", 0.1666)
				
			    bDebugDisplayCreditsText = FALSE
			ENDIF
			
			IF bDebugClearCreditsText
			    SCALEFORM_HIDE_SINGLE_LINE("debug", 0.1666)
			    
			    bDebugClearCreditsText = FALSE
			ENDIF
		ENDIF
		#ENDIF
		
		//Exit Cops from Cars
		IF NOT IS_ENTITY_DEAD(vehCop[16])
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm0)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[16])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[16]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(027, sCarrec) / 100) * 90)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[16])
				AND NOT IS_ENTITY_AT_COORD(vehCop[16], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(027, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[0])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[0], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[0], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[0], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm0, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm1)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[16])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[16]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(027, sCarrec) / 100) * 95)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[16])
				AND NOT IS_ENTITY_AT_COORD(vehCop[16], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(027, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[1])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[1], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[1], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[1], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm1, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehCop[17])
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm2)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[17])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[17]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(028, sCarrec) / 100) * 90)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[17])
				AND NOT IS_ENTITY_AT_COORD(vehCop[17], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(028, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[2])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[2], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[2], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm2, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm3)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[17])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[17]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(028, sCarrec) / 100) * 95)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[17])
				AND NOT IS_ENTITY_AT_COORD(vehCop[17], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(028, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[3])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[3], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[3], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[3], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm3, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehCop[18])
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm4)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[18])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[18]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(029, sCarrec) / 100) * 90)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[18])
				AND NOT IS_ENTITY_AT_COORD(vehCop[18], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(029, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[4])	
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[4], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[4], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[4], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm4, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm5)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[18])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[18]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(029, sCarrec) / 100) * 95)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[18])
				AND NOT IS_ENTITY_AT_COORD(vehCop[18], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(029, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[5])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[5], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[5], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[5], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm5, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehCop[19])
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm6)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[19])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[19]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(032, sCarrec) / 100) * 90)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[19])
				AND NOT IS_ENTITY_AT_COORD(vehCop[19], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(032, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[9])	
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[9], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[9], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[9], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm6, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(CopFarm7)
				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[19])
				AND GET_TIME_POSITION_IN_RECORDING(vehCop[19]) > (GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(032, sCarrec) / 100) * 95)
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCop[19])
				AND NOT IS_ENTITY_AT_COORD(vehCop[19], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(032, 0.0, sCarrec), <<5.0, 5.0, 5.0>>))
					IF NOT IS_PED_INJURED(pedCop[10])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[10], 1000.0)
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[10], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedCop[10], CA_LEAVE_VEHICLES, TRUE)
						
						SET_LABEL_AS_TRIGGERED(CopFarm7, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Cops
		IF iCutsceneStage > 1 AND iCutsceneStage < 3
			IF iCopPerFrame < 7
				iCopPerFrame++
			ELSE
				iCopPerFrame = 0
			ENDIF
			
			i = iCopPerFrame	//REPEAT 8 i //8 respawn cops
				IF DOES_ENTITY_EXIST(pedCop[i])
				AND IS_PED_INJURED(pedCop[i])
				OR NOT DOES_ENTITY_EXIST(pedCop[i])
					IF iRespawnTimers[i] = 0
						iRespawnTimers[i] = GET_GAME_TIMER() + 2000		#IF IS_DEBUG_BUILD	PRINTLN("iRespawnTimers[", i, "] = ", GET_GAME_TIMER() + 2000)	#ENDIF
						
						SET_PED_AS_NO_LONGER_NEEDED(pedCop[i])
					ELIF GET_GAME_TIMER() > iRespawnTimers[i]
						createCop(i)
						
						IF i = 0
							SET_PED_POSITION(pedCop[i], <<3504.7551, -4676.2471, 113.1829>>, 271.0395)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3532.4004, -4676.8501, 113.2428>>, 10.0)
						ELIF i = 1
							SET_PED_POSITION(pedCop[i], <<3481.3945, -4661.1577, 115.0043>>, 253.3929)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3533.6455, -4674.1851, 113.2056>>, 10.0)
						ELIF i = 2
							SET_PED_POSITION(pedCop[i], <<3500.8, -4651.9, 115.4>>, 229.4754)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3530.3618, -4667.2012, 113.2080>>, 10.0)
						ELIF i = 3
							SET_PED_POSITION(pedCop[i], <<3547.1851, -4643.0996, 113.0261>>, 172.3108)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3544.0862, -4660.0444, 113.3841>>, 10.0)
						ELIF i = 4
							SET_PED_POSITION(pedCop[i], <<3548.8083, -4634.8311, 113.6210>>, 180.2456)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3546.5930, -4660.5620, 113.1514>>, 10.0)
						ELIF i = 5
							SET_PED_POSITION(pedCop[i], <<3512.2898, -4674.8481, 113.1897>>, 251.4869)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3536.6877, -4683.6567, 113.4076>>, 2.0)
						ELIF i = 6
							SET_PED_POSITION(pedCop[i], <<3507.3110, -4670.9932, 113.2030>>, 250.4445)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3537.1406, -4688.0933, 113.5309>>, 2.0)
						ELIF i = 7
							SET_PED_POSITION(pedCop[i], <<3559.7903, -4634.5532, 113.5299>>, 168.8337)
							
							SET_PED_SPHERE_DEFENSIVE_AREA(pedCop[i], <<3551.6704, -4660.0039, 113.1956>>, 10.0)
						ENDIF
						
						SET_PED_COMBAT_ATTRIBUTES(pedCop[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
						
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedCop[i], 500.0)
						
						IF i = 0 OR i = 3
							GIVE_WEAPON_TO_PED(pedCop[i], wtSMG, INFINITE_AMMO, TRUE)
						ENDIF
						
						IF i = 1
							GIVE_WEAPON_TO_PED(pedCop[i], wtShotgun, INFINITE_AMMO, TRUE)
						ENDIF
						
						SET_PED_ACCURACY(pedCop[i], CLAMP_INT(1 + iDeadCopPlus, 0, 100))
						
						iDeadCopPlus++
						
						iRespawnTimers[i] = 0
					ENDIF
				ENDIF
//			ENDREPEAT
		ENDIF
		
		//God Text & Dialogue
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_CUTSCENE_PLAYING()
		AND iCutsceneStage < 3
			IF GET_GAME_TIMER() > iDialogueTimer
				IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_COPFARM)
					PRINT_ADV(PRO_COPFARM, "PRO_COPFARM")
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_1, "PRO_Rage", "PRO_Rage_1", CONV_PRIORITY_MEDIUM, TRUE, DO_NOT_DISPLAY_SUBTITLES)
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_2)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_2, "PRO_Rage", "PRO_Rage_2")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_3)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_3, "PRO_Rage", "PRO_Rage_3")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_4)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_4, "PRO_Rage", "PRO_Rage_4")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_5)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_5, "PRO_Rage", "PRO_Rage_5")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_6)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_6, "PRO_Rage", "PRO_Rage_6")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_7)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_7, "PRO_Rage", "PRO_Rage_7")
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_Rage_8)
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(PRO_Rage_8, "PRO_Rage", "PRO_Rage_8")
				ENDIF
				
				iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 8000)
			ENDIF
		ENDIF
		
		//Disable Player Weapon Change
		SET_PED_RESET_FLAG(playerPedID, PRF_DisablePlayerAutoVaulting, TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("PROLOGUE_GRAB_WOMAN")
			STOP_AUDIO_SCENE("PROLOGUE_GRAB_WOMAN")
		ENDIF
		
		REPLAY_STOP_EVENT()
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		IF bRequestedCreditsScaleform
			SCALEFORM_REMOVE_ALL_SINGLE_LINES()
		
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfCredits)
			bRequestedCreditsScaleform = FALSE
			bCreditsScaleformActive = FALSE
		ENDIF
		
		//Timecycle
		CLEAR_TIMECYCLE_MODIFIER()	#IF IS_DEBUG_BUILD	PRINTLN("CLEAR_TIMECYCLE_MODIFIER - Finale Stage End")	#ENDIF
		
		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1	#IF IS_DEBUG_BUILD	PRINTLN("IF GET_TIMECYCLE_MODIFIER_INDEX() != -1 - Finale Stage End")	#ENDIF
			SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0)	#IF IS_DEBUG_BUILD	PRINTLN("SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.0) - Finale Stage End")	#ENDIF
		ENDIF
		
		INT i
		
		REPEAT COUNT_OF(objWeapon) i
			SAFE_DELETE_OBJECT(objWeapon[i])
		ENDREPEAT
		
		//Cover
		REPEAT COUNT_OF(covPoint) i
			REMOVE_COVER_POINT(covPoint[i])
		ENDREPEAT
		
		SET_ENTITY_INVINCIBLE(playerPedID, FALSE)
		
		//DO_PLAYER_MAP_WARP_WITH_LOAD(<<-1884.0472, -576.6046, 10.8126>>, NO_HEADING, FALSE)
		
		IF g_savedGlobals.sFlow.isGameflowActive	//Check the mission is passing flow
		OR IS_REPEAT_PLAY_ACTIVE()				//and not on a repeat play
			FREEZE_ENTITY_POSITION(playerPedID, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("FREEZE_ENTITY_POSITION(playerPedID, TRUE) - Prologue is ending in flow so freezing player...")	#ENDIF
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

//----------------------------------¦ DEBUG ¦------------------------------------

#IF IS_DEBUG_BUILD

PROC debugRoutine()
	IF bAutoSkipping = TRUE
		IF ENUM_TO_INT(eMissionObjective) >= ENUM_TO_INT(eMissionObjectiveAutoJSkip)
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
			
			eMissionObjectiveAutoJSkip = initMission
			
			bAutoSkipping = FALSE
			bSkipped = TRUE
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		CLEAR_TEXT()
		
		PLAY_AUDIO(PROLOGUE_TEST_MISSION_CLEANUP)
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			#IF IS_DEBUG_BUILD	PRINTLN("STOPPING CUTSCENE...")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			#IF IS_DEBUG_BUILD	PRINTLN("REMOVING CUTSCENE...")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		missionPassed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			#IF IS_DEBUG_BUILD	PRINTLN("STOPPING CUTSCENE...")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			#IF IS_DEBUG_BUILD	PRINTLN("REMOVING CUTSCENE...")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		missionFailed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	OR ENUM_TO_INT(eMissionObjective) < ENUM_TO_INT(eMissionObjectiveAutoJSkip)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bSkipped = TRUE
		ENDIF
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			#IF IS_DEBUG_BUILD	PRINTLN("STOPPING CUTSCENE...")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			#IF IS_DEBUG_BUILD	PRINTLN("REMOVING CUTSCENE...")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		ADVANCE_STAGE()
		
		#IF IS_DEBUG_BUILD	PRINTLN("Stage: ", ENUM_TO_INT(eMissionObjective))	#ENDIF
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
	OR LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iStageSkipMenu, ENUM_TO_INT(eMissionObjective))
		IF bAutoSkipping = FALSE
			bAutoSkipping = TRUE
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				eMissionObjectiveAutoJSkip = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) - 1)
			ELSE
				eMissionObjectiveAutoJSkip = INT_TO_ENUM(MissionObjective, iStageSkipMenu)
			ENDIF
			
			eMissionObjective = initMission
			
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
			//Cutscene
			WHILE NOT HAS_CUTSCENE_FINISHED()
				STOP_CUTSCENE(TRUE)
				
				#IF IS_DEBUG_BUILD	PRINTLN("STOPPING CUTSCENE...")	#ENDIF
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			REMOVE_CUTSCENE()
			
			WHILE HAS_CUTSCENE_LOADED()
				#IF IS_DEBUG_BUILD	PRINTLN("REMOVING CUTSCENE...")	#ENDIF
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			MISSION_CLEANUP(TRUE)
			
			CLEAR_AREA(GET_ENTITY_COORDS(playerPedID), 10000.0, TRUE)
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
	ENDIF
ENDPROC 

#ENDIF

//------------------------------¦ MISSION SCRIPT ¦-------------------------------

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		UPDATE_PED_REFERENCES()
		
		SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
		
		INT i
		
		REPEAT COUNT_OF(pedCop) i
			IF NOT IS_PED_INJURED(pedCop[i])
				IF NOT IS_PED_IN_COMBAT(pedCop[i])
					IF NOT IS_PED_INJURED(notPlayerPedID)
						TASK_COMBAT_PED(pedCop[i], notPlayerPedID)
					ELIF DOES_ENTITY_EXIST(pedBrad)
					AND NOT IS_PED_INJURED(pedBrad)
						TASK_COMBAT_PED(pedCop[i], pedBrad)
					ELSE
						TASK_AIM_GUN_AT_COORD(pedCop[i], GET_ENTITY_COORDS(playerPedID, FALSE), -1)
					ENDIF
				ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCop[i], TRUE)
				SET_PED_KEEP_TASK(pedCop[i], TRUE)
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(notPlayerPedID)
			SET_PED_KEEP_TASK(notPlayerPedID, TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(pedBrad)
			SET_PED_KEEP_TASK(pedBrad, TRUE)
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF eMissionObjective < stageGetAway
				MISSION_FLOW_SET_FAIL_REASON("PRO_MDIED")
			ENDIF
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF eMissionObjective < stageGetAway
				MISSION_FLOW_SET_FAIL_REASON("PRO_TDIED")
			ENDIF
		ENDIF
		
		//Score
		PLAY_AUDIO(PROLOGUE_TEST_FAIL)
		
		SET_CLEANUP_TO_RUN_ON_RESPAWN(RCI_PROLOGUE)
		
		Mission_Flow_Mission_Force_Cleanup()
		
		MISSION_CLEANUP(FALSE, TRUE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	NETWORK_SET_CAN_RECEIVE_PRESENCE_INVITES(FALSE)
	
//-------------------------------¦ MISSION LOOP ¦--------------------------------
	
	#IF IS_DEBUG_BUILD
		MissionNames()
	#ENDIF
	
	WHILE (TRUE)
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Prologue")
		
		//Update Player Ped References
		UPDATE_PED_REFERENCES()
		
		//Fail Checks
		DEATH_CHECKS()
		
		//Blips
		UPDATE_BLIP_FLASH_TIMERS()
		
		//Text
		//UPDATE_FLOATING_HELP_ABOVE_PLAYER()
		
		//Random Peds
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		//Traffic
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		IF eMissionObjective > initMission
			//Flashing Light
			IF eMissionObjective <= stageLearnPhone
				IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_V_ILEV_CD_LAMPAL)
					CREATE_MODEL_HIDE(<<5310.8721, -5196.3398, 84.6373>>, 1.0, V_ILEV_CD_LAMPAL, TRUE)
					
					SET_LABEL_AS_TRIGGERED(TRIGGER_V_ILEV_CD_LAMPAL, TRUE)
				ENDIF
			ELSE
				IF HAS_LABEL_BEEN_TRIGGERED(TRIGGER_V_ILEV_CD_LAMPAL)
					REMOVE_MODEL_HIDE(<<5310.8721, -5196.3398, 84.6373>>, 1.0, V_ILEV_CD_LAMPAL, FALSE)
					
					SET_LABEL_AS_TRIGGERED(TRIGGER_V_ILEV_CD_LAMPAL, FALSE)
				ENDIF
			ENDIF
			
			//Sprinklers
			IF eMissionObjective <= stageLearnPhone
				REMOVE_MODEL_SWAP(<<5293.14, -5162.17, 85.92>>, 1.0, V_ILEV_CD_SPRKLR, V_ILEV_CD_SPRKLR_ON)
				REMOVE_MODEL_SWAP(<<5295.89, -5192.17, 85.92>>, 1.0, V_ILEV_CD_SPRKLR, V_ILEV_CD_SPRKLR_ON)
			ELSE
				CREATE_MODEL_SWAP(<<5293.14, -5162.17, 85.92>>, 1.0, V_ILEV_CD_SPRKLR, V_ILEV_CD_SPRKLR_ON, TRUE)
				CREATE_MODEL_SWAP(<<5295.89, -5192.17, 85.92>>, 1.0, V_ILEV_CD_SPRKLR, V_ILEV_CD_SPRKLR_ON, TRUE)
			ENDIF
		ENDIF
		
		//Doors
		IF eMissionObjective > initMission
		AND eMissionObjective < cutGetAway
			IF IS_ENTITY_AT_COORD(playerPedID, <<5307.029785, -5180.698730, 84.018654>>, <<15.0, 5.0, 1.5>>)
				SMOOTH_CLOSE_DOOR(CCTV_ROOM_DOOR, v_ilev_cd_door3, <<5305.46, -5177.75, 83.67>>, TRUE, 1.0, 1.0, 0.0)
			ENDIF
		ENDIF
		
		IF eMissionObjective > initMission
		AND eMissionObjective > stageLearnBlips
			IF IS_ENTITY_AT_COORD(playerPedID, <<5310.546387, -5206.499023, 84.518631>>, <<7.0, 12.0, 2.0>>)
				SMOOTH_CLOSE_DOOR(CUPBOARD_DOOR, V_ILev_CD_Door2, <<5316.65, -5205.74, 83.67>>, TRUE, 1.0, 1.0, 0.0)
			ENDIF
		ENDIF
		
		IF eMissionObjective > initMission
		AND eMissionObjective <= stageLearnBlips
			IF NOT HAS_LABEL_BEEN_TRIGGERED(TRIGGER_V_ILEV_CD_Door2)
				UNLOCK_DOOR(CUPBOARD_DOOR, V_ILev_CD_Door2)
			ENDIF
		ENDIF
		
		IF eMissionObjective >= stageLearnAiming
		AND eMissionObjective <= stageLearnPhone
			IF HAS_LABEL_BEEN_TRIGGERED(DoorIsClosing)
				SMOOTH_CLOSE_DOOR(LEFT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5307.52, -5204.54, 83.67>>, TRUE)
				SMOOTH_CLOSE_DOOR(RIGHT_RECEPTION_DOOR, V_ILEV_CD_DOOR, <<5310.12, -5204.54, 83.67>>, TRUE)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_AT_COORD(playerPedID, <<5307.029785, -5180.698730, 84.018654>>, <<15.0, 5.0, 1.5>>)
		OR IS_ENTITY_AT_COORD(playerPedID, <<5328.0083, -5188.691406, 83.784843>>, <<15.0, 10.0, 2.5>>)
			IF bGarageVisible = TRUE
				IF NOT DOES_ENTITY_EXIST(objGarage)
					IF bGrabShutter = FALSE
						IF HAS_MODEL_LOADED_CHECK(MODEL_PROP_GAR_DOOR_A_01, PROP_GAR_DOOR_A_01)
							CREATE_MODEL_HIDE(<<5320.60, -5188.56, 82.52>>, 1.0, PROP_GAR_DOOR_A_01, FALSE)
							
							objGarage = CREATE_OBJECT_NO_OFFSET(PROP_GAR_DOOR_A_01, vGaragePosition)
							SET_ENTITY_COLLISION(objGarage, FALSE)
							SET_ENTITY_ROTATION(objGarage, vGarageRotate)
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_GAR_DOOR_A_01)
							
							IF objGarage != NULL
								bGrabShutter = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(objHiddenCollision)
					IF bGrabShutter = TRUE
						IF HAS_MODEL_LOADED_CHECK(MODEL_P_GDOOR1COLOBJECT_S, P_GDOOR1COLOBJECT_S)
							objHiddenCollision = CREATE_OBJECT(P_GDOOR1COLOBJECT_S, <<5320.589844, -5188.490234, 82.52>>)
							FREEZE_ENTITY_POSITION(objHiddenCollision, TRUE)
							SET_ENTITY_COORDS(objHiddenCollision, <<5320.589844, -5188.490234,82.52>>)
							SET_ENTITY_ROTATION(objHiddenCollision, <<0.0, 0.0, 90.0>>)
							SET_ENTITY_VISIBLE(objHiddenCollision, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(P_GDOOR1COLOBJECT_S)
						ENDIF
					ENDIF
				ELIF eMissionObjective >= stageShootOut
					IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictMapObjects)
						SET_ENTITY_COORDS(objHiddenCollision, <<5320.589844, -5188.490234, 82.52 + 2.60>>)
						
						IF DOES_ENTITY_HAVE_DRAWABLE(objGarage)
							IF NOT IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
								PLAY_ENTITY_ANIM(objGarage, "GDoor_Open", sAnimDictMapObjects, INSTANT_BLEND_IN, FALSE, TRUE)
							ELSE
								SET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open", 1.0)
								SET_ENTITY_ANIM_SPEED(objGarage, sAnimDictMapObjects, "GDoor_Open", 0.0)
								
								bGarageVisible = FALSE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					SET_ENTITY_COORDS(objHiddenCollision, <<5320.589844, -5188.490234, 82.52>>)
					
					IF DOES_ENTITY_HAVE_DRAWABLE(objGarage)
						IF IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
							SET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open", 0.0)
							SET_ENTITY_ANIM_SPEED(objGarage, sAnimDictMapObjects, "GDoor_Open", 0.0)
						ENDIF
					ENDIF
					
					bGarageVisible = FALSE
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED(ShutterOpen)
				IF eMissionObjective >= stageDuckUnderShutter OR eMissionObjective <= stageShootOut
					IF DOES_ENTITY_EXIST(objGarage)
						IF DOES_ENTITY_HAVE_DRAWABLE(objGarage)
							IF IS_ENTITY_PLAYING_ANIM(objGarage, sAnimDictMapObjects, "GDoor_Open")
							AND GET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open") <= 1.0
								IF DOES_ENTITY_EXIST(objHiddenCollision)
									SET_ENTITY_COORDS(objHiddenCollision, <<5320.589844, -5188.490234, GET_INTERP_POINT_FLOAT(82.52, 82.52 + 2.60, 0.0, 0.888, GET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open"))>>)
								ENDIF
								
								IF GET_ENTITY_ANIM_CURRENT_TIME(objGarage, sAnimDictMapObjects, "GDoor_Open") = 1.0
									SET_LABEL_AS_TRIGGERED(ShutterOpen, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Rayfire
		IF eMissionObjective >= stageShootOut AND eMissionOBjective <= stageGetAway
			IF NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfSecDoorExplosion)
				IF IS_ENTITY_AT_COORD(playerPedID, <<5333.213867, -5187.637207, 84.804016>>, <<20.0, 10.0, 3.0>>)
					rfSecDoorExplosion = GET_RAYFIRE_MAP_OBJECT(<<5318.2, -5185.1, 83.7>>, 10.0, "des_prologue_door")
					#IF IS_DEBUG_BUILD	PRINTLN("Getting Rayfire Map Object - des_prologue_door")	#ENDIF
					SET_LABEL_AS_TRIGGERED(RayfireLoaded, FALSE)
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED(RayfireLoaded)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion) != RFMO_STATE_END
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfSecDoorExplosion, RFMO_STATE_ENDING)
					#IF IS_DEBUG_BUILD	PRINTLN("Setting Rayfire Map Object - des_prologue_door")	#ENDIF
					SET_LABEL_AS_TRIGGERED(RayfireLoaded, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		//Ragdoll
		IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF GET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) <= 101
			AND eMissionObjective = cutGuard
				SET_PED_CAN_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
			ELSE
				IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], SCRIPT_TASK_ANY) != PERFORMING_TASK
				AND NOT IS_PED_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_CAN_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ANY) != PERFORMING_TASK
			AND NOT IS_PED_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_CAN_RAGDOLL(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(pedBrad)
			IF GET_SCRIPT_TASK_STATUS(pedBrad, SCRIPT_TASK_ANY) != PERFORMING_TASK
			AND NOT IS_PED_RAGDOLL(pedBrad)
				SET_PED_CAN_RAGDOLL(pedBrad, FALSE)
			ENDIF
		ENDIF
		
		//Idle Anims
		IF DOES_ENTITY_EXIST(playerPedMichael)
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(playerPedMichael, FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(playerPedTrevor)
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(playerPedTrevor, FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedBrad)
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedBrad, FALSE)
		ENDIF
		
		IF eMissionObjective >= stageLearnWalking
		AND eMissionObjective <= stageDuckUnderShutter
		AND eMissionObjective != cutGuard
			IF IS_SCREEN_FADED_IN()
			AND NOT IS_CUTSCENE_PLAYING()
			AND NOT IS_PHONE_ONSCREEN()
				IF HAS_PED_GOT_WEAPON(playerPedID, WEAPONTYPE_CARBINERIFLE)
					WEAPON_TYPE wtRifle = WEAPONTYPE_CARBINERIFLE
					INT iClipAmmo = 0
					
					GET_AMMO_IN_CLIP(playerPedID, wtRifle, iClipAmmo)
					
					IF GET_AMMO_IN_PED_WEAPON(playerPedID, wtRifle) <= 0
					AND iClipAmmo <= 0
						eMissionFail = failOutOfAmmo
						
						missionFailed()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Debug Routine
		#IF IS_DEBUG_BUILD
			debugRoutine()
		#ENDIF
		
		//Time Fix
		SWITCH eMissionObjective 
			CASE initMission
			CASE cutIntro
			CASE stageLearnWalking
			CASE cutTieUp
			CASE stageLearnAiming
			CASE stageLearnPhone
			CASE stageLearnBlips
			CASE stageLeaveVault
			CASE cutGuard
			//CASE stageDisableCameras
			CASE stageBlastDoors
			CASE stageDuckUnderShutter
			CASE stageShootOut
				//SNOW @5:00
				//twilight as you exit the building security lights lighting up the yard
				//(img0000+0005)
				//load Snowy 01 cloud hat
				SMOOTH_SET_CLOCK_TIME(5, 0, 0)
			BREAK
			CASE cutGetAway
				//on road SNOW @6:00
				//glimer of hope going to get out of this situation/horrible weather (sun poking through)
				//(img0011)
				//keep Snowy 01 cloud hat loaded
				IF iCutsceneStage < 1	//Cutscene transition
					SMOOTH_SET_CLOCK_TIME(5, 0, 0)
				ELSE
					SMOOTH_SET_CLOCK_TIME(6, 0, 0)
				ENDIF
			BREAK
			CASE stageGetAway
				//on road SNOW @6:00
				//glimer of hope going to get out of this situation/horrible weather (sun poking through)
				//(img0011)
				//keep Snowy 01 cloud hat loaded
				SMOOTH_SET_CLOCK_TIME(6, 0, 0)
			BREAK
			CASE cutFinale
			CASE stageFinale
				IF iCutsceneStage < 3
					//get to farm SNOW @7:00
					//feds close in and so does the weather lighter BUT greyer and possibly windier.
					//(img0010)
					//keep Snowy 01 cloud hat loaded
					SMOOTH_SET_CLOCK_TIME(7, 0, 0)
				ELSE
					//funeral service cut scene SNOW @ 16:00
					//dull and grey weather with a bit of sleet.
					//(img0048)
					//load Stormy 01 cloud hat
					IF HAS_THIS_CUTSCENE_LOADED("pro_mcs_7_concat")
					AND IS_CUTSCENE_PLAYING()
					AND HAS_LABEL_BEEN_TRIGGERED(TrevorRunsOffB)
						SET_CLOCK_TIME(12, 0, 0)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		//Snow Marks + Wet
		IF eMissionObjective > initMission
			IF eMissionObjective < stageShootOut
				fSnowMarks = 0.0
			ELIF eMissionObjective >= stageShootOut OR eMissionObjective <= cutGetAway
				IF fSnowMarks < 1.0
					fSnowMarks = fSnowMarks +@ 0.1
				ENDIF
			ELIF eMissionObjective = stageGetAway
				IF fSnowMarks > 0.4
					fSnowMarks = fSnowMarks -@ 0.1
				ENDIF
			ELIF eMissionObjective >= cutFinale OR eMissionObjective <= stageFinale
				IF fSnowMarks < 1.0
					fSnowMarks = fSnowMarks +@ 0.1
				ENDIF
			ENDIF
			
			IF fSnowMarks > 1.0
				fSnowMarks = 1.0
			ENDIF
			
			IF fSnowMarks < 0.0
				fSnowMarks = 0.0
			ENDIF
			
			IF NOT IS_PED_INJURED(playerPedMichael)
				IF DOES_ENTITY_HAVE_DRAWABLE(playerPedMichael)
				AND DOES_ENTITY_HAVE_PHYSICS(playerPedMichael)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedMichael)
					SET_PED_WETNESS_ENABLED_THIS_FRAME(playerPedMichael)
					
					SET_ENABLE_PED_ENVEFF_SCALE(playerPedMichael, TRUE)
					
					IF GET_PED_ENVEFF_SCALE(playerPedMichael) <> fSnowMarks
						SET_PED_ENVEFF_SCALE(playerPedMichael, fSnowMarks)	//PRINTLN("SET_PED_ENVEFF_SCALE(playerPedMichael, ", fSnowMarks, ")")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(playerPedTrevor)
				IF DOES_ENTITY_HAVE_DRAWABLE(playerPedTrevor)
				AND DOES_ENTITY_HAVE_PHYSICS(playerPedTrevor)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(playerPedTrevor)
					SET_PED_WETNESS_ENABLED_THIS_FRAME(playerPedTrevor)
					
					SET_ENABLE_PED_ENVEFF_SCALE(playerPedTrevor, TRUE)
					
					IF GET_PED_ENVEFF_SCALE(playerPedTrevor) <> fSnowMarks
						SET_PED_ENVEFF_SCALE(playerPedTrevor, fSnowMarks)	//PRINTLN("SET_PED_ENVEFF_SCALE(playerPedTrevor, ", fSnowMarks, ")")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedBrad)
				IF DOES_ENTITY_HAVE_DRAWABLE(pedBrad)
				AND DOES_ENTITY_HAVE_PHYSICS(pedBrad)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedBrad)
					SET_PED_WETNESS_ENABLED_THIS_FRAME(pedBrad)
					
					SET_ENABLE_PED_ENVEFF_SCALE(pedBrad, TRUE)
					
					IF eMissionObjective >= cutGetAway
						IF eMissionObjective <> cutFinale
							SET_PED_ENVEFF_SCALE(pedBrad, 0.6)
							//PRINTSTRING("set brad 2") printnl()
						ELSE
							SET_PED_ENVEFF_SCALE(pedBrad, 0.8)
							SET_PED_ENVEFF_CPV_ADD(pedBrad, 0.15)
						ENDIF
					ELSE
						IF GET_PED_ENVEFF_SCALE(pedBrad) <> fSnowMarks
							SET_PED_ENVEFF_SCALE(pedBrad, fSnowMarks)	//PRINTLN("SET_PED_ENVEFF_SCALE(pedBrad, ", fSnowMarks, ")")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Disable Ambient Audio (Aiming gun at buddy) - each frame
		IF NOT IS_PED_INJURED(playerPedMichael)
			SET_PED_RESET_FLAG(playerPedMichael, PRF_DisableFriendlyGunReactAudio, TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(playerPedTrevor)
			SET_PED_RESET_FLAG(playerPedTrevor, PRF_DisableFriendlyGunReactAudio, TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(pedBrad)
			SET_PED_RESET_FLAG(pedBrad, PRF_DisableFriendlyGunReactAudio, TRUE)
		ENDIF
		
		//Give buddy ammo if they drop too low
		IF eMissionObjective >= stageShootOut AND eMissionObjective <= stageGetAway
		AND IS_SCREEN_FADED_IN()
		AND NOT IS_CUTSCENE_PLAYING()
			IF NOT IS_PED_INJURED(notPlayerPedID)
				UPDATE_BUDDY_AMMO()
			ENDIF
		ENDIF
		
		//Kinematic Flag Tracking
		IF eKinematicChar != GET_CURRENT_PLAYER_PED_ENUM()
			IF DOES_ENTITY_EXIST(playerPedID)
			AND DOES_ENTITY_EXIST(notPlayerPedID)
			AND DOES_ENTITY_EXIST(pedBrad)
				SET_PED_CONFIG_FLAG(playerPedID, PCF_UseKinematicModeWhenStationary, FALSE)
				SET_PED_CONFIG_FLAG(notPlayerPedID, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CONFIG_FLAG(pedBrad, PCF_UseKinematicModeWhenStationary, TRUE)
				
				eKinematicChar = GET_CURRENT_PLAYER_PED_ENUM()
			ENDIF
		ENDIF
		
		//Hide HUD/Radar
		IF bRadar = FALSE
			IF eMissionObjective <> cutIntro
				DISPLAY_RADAR(FALSE)
			ENDIF
			
			DISPLAY_HUD(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)
		ELSE
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
		ENDIF
		
//		IF NOT IS_GAMEPLAY_CAM_RENDERING()
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
//		ENDIF
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)	//#IF IS_DEBUG_BUILD	PRINTLN("HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)")	#ENDIF
		
		IF eMissionObjective <= stageDuckUnderShutter
			IF NOT HAS_LABEL_BEEN_TRIGGERED(PRO_COP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			ENDIF
		ENDIF
		
		IF IS_INTERPOLATING_TO_SCRIPT_CAMS()
		OR IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		OR DOES_CAM_EXIST(camMain)
		AND IS_CAM_INTERPOLATING(camMain)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENDIF
		
//		IF fAnimProgressTime = 0.0
//			IF IS_ENTITY_PLAYING_ANIM(playerPedID, sAnimDictPrologue2, "main_player_zero")
//				fAnimTotalTime = GET_ENTITY_ANIM_TOTAL_TIME(playerPedID, sAnimDictPrologue2, "main_player_zero")
//				fAnimProgressTime = GET_GAME_TIMER() + fAnimProgressTime
//			ENDIF
//		ENDIF
		
		//Birds
		SET_GLOBAL_MIN_BIRD_FLIGHT_HEIGHT(100.0)
		
		//Stats
		IF GET_VEHICLE_PED_IS_USING(playerPedID) != NULL
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_USING(playerPedID))
		ENDIF
		
		//Kills a Ped
		INT iEventCount = 0
		
		STRUCT_ENTITY_ID sei = CONSTRUCT_ENTITY_ID()
		EVENT_NAMES eventType
		
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventCount
			eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventCount)
			
			SWITCH (eventType)
				CASE EVENT_ENTITY_DESTROYED
					//Figure out what type of entity it is
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventCount, sei, SIZE_OF(STRUCT_ENTITY_ID))
					
					IF DOES_ENTITY_EXIST(sei.EntityId)//sei.EntityId
						IF IS_ENTITY_A_PED(sei.EntityId)
							IF IS_ENTITY_A_MISSION_ENTITY(sei.EntityId)
								INFORM_MISSION_STATS_OF_INCREMENT(PRO_KILLS)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
		
//		WEAPON_TYPE wtCurrentUpdate = WEAPONTYPE_UNARMED
//		GET_CURRENT_PED_WEAPON(playerPedID, wtCurrentUpdate)
//		
//		IF wtCurrent != wtCurrentUpdate
//		OR iAmmo = -1
//			wtCurrent = wtCurrentUpdate
//			iAmmo = GET_AMMO_IN_PED_WEAPON(playerPedID, wtCurrent)
//		ENDIF
//		
//		IF GET_AMMO_IN_PED_WEAPON(playerPedID, wtCurrent) < iAmmo
//			INT iDifference = iAmmo - GET_AMMO_IN_PED_WEAPON(playerPedID, wtCurrent)
//			
//			REPEAT iDifference i
//				INFORM_MISSION_STATS_OF_INCREMENT(MIC2_BULLETS_FIRED)
//			ENDREPEAT
//			
//			iAmmo = GET_AMMO_IN_PED_WEAPON(playerPedID, wtCurrent)
//		ENDIF
		
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(playerPedID)
		
		LOAD_UNLOAD_ASSETS()
		
		AUDIO_CONTROLLER()
		
		//First Person Camera Used Check
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMN_FPSHELP")
					SAFE_CLEAR_HELP()
				ENDIF
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW, TRUE)
			ENDIF
		ENDIF
		
		//Instance Priority
		IF eMissionObjective >= stageShootOut AND eMissionObjective <= cutGetAway
		AND NOT IS_CUTSCENE_PLAYING()
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
		ELSE
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		ENDIF
		
		//Show HUD
		IF eMissionObjective >= stageLearnBlips AND eMissionObjective <= stageDuckUnderShutter
		AND iCurrentTake > 0
			IF IS_VALID_INTERIOR(intDepot)
				IF NOT (HAS_THIS_CUTSCENE_LOADED("pro_mcs_7_concat")
				AND IS_CUTSCENE_PLAYING())
					DISPLAY_TAKE()
				ENDIF
			ENDIF
		ENDIF
		
		//Audio Stream (Fix for bug 2284329
		IF bAudioStream
			IF NOT IS_STREAM_PLAYING()
				SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
				
				bAudioStream = FALSE
			ENDIF
		ENDIF
		
		//DEBUG LIGHTING RIGS
//		INT i
//		
//		VECTOR vLightingRigMin, vLightingRigMax
//		
//		vLightingRigMin = VECTOR_ZERO
//		vLightingRigMax = VECTOR_ZERO
//		
//		REPEAT COUNT_OF(objLightingRig) i
//			IF DOES_ENTITY_EXIST(objLightingRig[i])
//				VECTOR vLightingRigCoords = GET_ENTITY_COORDS(objLightingRig[i])
//				PRINTLN("objLightingRig[", i, "] exists at: <<", vLightingRigCoords.X, ", ", vLightingRigCoords.Y, ", ", vLightingRigCoords.Z, ">>")
//				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(objLightingRig[i]), vLightingRigMin, vLightingRigMax)
//				IF bDebugLightingRig
//					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//					DRAW_DEBUG_SPHERE(vLightingRigCoords, 0.25, 255, 0, 0)
//					DRAW_DEBUG_BOX(vLightingRigMin, vLightingRigMax, 255, 0, 0)
//				ENDIF
//			ENDIF
//		ENDREPEAT
		
//		IF bVideoRecording
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//			
//			DRAW_RECT(0.9, 0.1, 0.1, 0.1, 255, 0, 0, 255)
//		ENDIF
		
		//Objective switch
		SWITCH eMissionObjective 
			CASE initMission
				IF HAS_MODEL_LOADED_CHECK(MODEL_GET_PLAYER_PED_MODEL_CHAR_MICHAEL, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				AND HAS_MODEL_LOADED_CHECK(MODEL_GET_PLAYER_PED_MODEL_CHAR_TREVOR, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				AND HAS_MODEL_LOADED_CHECK(MODEL_IG_BRAD, IG_BRAD)
					initialiseMission()
				ENDIF
			BREAK
			CASE cutIntro
				IF HAS_MODEL_LOADED_CHECK(MODEL_A_M_M_PROLHOST_01, A_M_M_PROLHOST_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_A_F_M_PROLHOST_01, A_F_M_PROLHOST_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_IG_PROLSEC_02, IG_PROLSEC_02)
				AND HAS_MODEL_LOADED_CHECK(MODEL_PROP_1ST_PROLOGUE_SCENE, PROP_1ST_PROLOGUE_SCENE)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue1)
					cutsceneIntro()
				ENDIF
			BREAK
			CASE stageLearnWalking
				LearnWalking()
			BREAK
			CASE cutTieUp
				IF HAS_MODEL_LOADED_CHECK(MODEL_PROP_1ST_HOSTAGE_SCENE, PROP_1ST_HOSTAGE_SCENE)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue1)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_MCS1)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_MCS1_GuardFacial)
					cutsceneTieUp()
				ENDIF
			BREAK
			CASE stageLearnAiming
				IF HAS_MODEL_LOADED_CHECK(MODEL_PROP_1ST_HOSTAGE_SCENE, PROP_1ST_HOSTAGE_SCENE)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_MCS1)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_TrevorReturn)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_Impatient)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue2_MCS1_GuardFacial)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_IdleAtCupboard)
					LearnAiming()
				ENDIF
			BREAK
			CASE stageLearnPhone
				IF HAS_MODEL_LOADED_CHECK(MODEL_modBomb, modBomb)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBombGreen, modBombGreen)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBomb3, modBomb3)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBomb2, modBomb2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBomb1, modBomb1)
				AND HAS_MODEL_LOADED_CHECK(MODEL_PROP_VAULT_DOOR_SCENE, PROP_VAULT_DOOR_SCENE)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_Cam)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_Impatient)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_IdleAtCupboard)
				AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRoute1)
					LearnPhone()
				ENDIF
			BREAK
			CASE stageLearnBlips
				IF HAS_MODEL_LOADED_CHECK(MODEL_V_ILev_CD_Dust, V_ILev_CD_Dust)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_IdleInVault)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_LeaveVault)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_Cough)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue3_Impatient)
				AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRoute2)
					LearnBlips()
				ENDIF
			BREAK
			CASE stageLeaveVault
				IF HAS_MODEL_LOADED_CHECK(MODEL_CSB_PROLSEC, CSB_PROLSEC)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_LeaveVault)
					LeaveVault()
				ENDIF
			BREAK
			CASE cutGuard
				IF HAS_MODEL_LOADED_CHECK(MODEL_CSB_PROLSEC, CSB_PROLSEC)
				AND HAS_MODEL_LOADED_CHECK(MODEL_PROP_CS_HEIST_BAG_02, PROP_CS_HEIST_BAG_02)
				AND HAS_MODEL_LOADED_CHECK(MODEL_P_LD_HEIST_BAG_S_1, P_LD_HEIST_BAG_S_1)
				AND HAS_MODEL_LOADED_CHECK(MODEL_P_CSH_STRAP_01_S, P_CSH_STRAP_01_S)
				AND HAS_MODEL_LOADED_CHECK(MODEL_PROP_MICHAEL_BALACLAVA, PROP_MICHAEL_BALACLAVA)
				AND HAS_MODEL_LOADED_CHECK(MODEL_PROP_2ND_HOSTAGE_SCENE, PROP_2ND_HOSTAGE_SCENE)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_Base)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_Shot)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_PlayerPause)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue4_Fail)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Start)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Main)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_End)
					cutsceneGuard()
				ENDIF
			BREAK
			CASE stageBlastDoors
				IF HAS_MODEL_LOADED_CHECK(MODEL_modBomb, modBomb)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBombGreen, modBombGreen)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBomb3, modBomb3)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBomb2, modBomb2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_modBomb1, modBomb1)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Start)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Main)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_End)
					BlastDoors()
				ENDIF
			BREAK
			CASE stageDuckUnderShutter
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_Duck)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_CleanSmoke)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue5_End)
					DuckUnderShutter()
				ENDIF
			BREAK
			CASE stageShootOut
				IF HAS_MODEL_LOADED_CHECK(MODEL_S_M_M_SNOWCOP_01, S_M_M_SNOWCOP_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_U_M_Y_PROLDRIVER_01, U_M_Y_PROLDRIVER_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_U_M_M_PROLSEC_01, U_M_M_PROLSEC_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD1, POLICEOLD1)
				AND HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD2, POLICEOLD2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_STOCKADE3, STOCKADE3)
				AND HAS_MODEL_LOADED_CHECK(MODEL_RANCHERXL2, RANCHERXL2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_EMPEROR3, EMPEROR3)
				AND HAS_MODEL_LOADED_CHECK(MODEL_P_TREVOR_PROLOGE_BALLY_S, P_TREVOR_PROLOGE_BALLY_S)
				AND HAS_MODEL_LOADED_CHECK(MODEL_P_TREV_SKI_MASK_S, P_TREV_SKI_MASK_S)
				AND HAS_RECORDING_LOADED_CHECK(003, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(004, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(005, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(006, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(008, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(009, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(011, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(026, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(700, sCarrec)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue6)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue6FirstPerson)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDeadGuard)
				AND HAS_VEHICLE_ASSET_LOADED(POLICEOLD1)
				AND HAS_VEHICLE_ASSET_LOADED(POLICEOLD2)
					ShootOut()
				ENDIF
			BREAK
			CASE cutGetAway
				IF HAS_RECORDING_LOADED_CHECK(026, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(701, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(702, sCarrec)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_LeadOut)
					cutsceneGetAway()
				ENDIF
			BREAK
			CASE stageGetAway
				IF HAS_MODEL_LOADED_CHECK(MODEL_S_M_M_SNOWCOP_01, S_M_M_SNOWCOP_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD1, POLICEOLD1)
				AND HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD2, POLICEOLD2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_RANCHERXL2, RANCHERXL2)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue_LeadOut)
					GetAway()
				ENDIF
			BREAK
			CASE cutFinale
				IF HAS_MODEL_LOADED_CHECK(MODEL_PROP_CS_HEIST_BAG_02, PROP_CS_HEIST_BAG_02)
				AND HAS_MODEL_LOADED_CHECK(MODEL_P_LD_HEIST_BAG_S_1, P_LD_HEIST_BAG_S_1)
				AND HAS_RECORDING_LOADED_CHECK(027, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(028, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(029, sCarrec)
					cutsceneFinale()
				ENDIF
			BREAK
			CASE stageFinale
				IF HAS_MODEL_LOADED_CHECK(MODEL_S_M_M_SNOWCOP_01, S_M_M_SNOWCOP_01)
				AND HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD1, POLICEOLD1)
				AND HAS_MODEL_LOADED_CHECK(MODEL_POLICEOLD2, POLICEOLD2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_RANCHERXL2, RANCHERXL2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_FREIGHT, FREIGHT)
				AND HAS_MODEL_LOADED_CHECK(MODEL_FREIGHTCONT1, FREIGHTCONT1)
				AND HAS_MODEL_LOADED_CHECK(MODEL_FREIGHTCONT2, FREIGHTCONT2)
				AND HAS_MODEL_LOADED_CHECK(MODEL_PROP_CS_HEIST_BAG_02, PROP_CS_HEIST_BAG_02)
				AND HAS_RECORDING_LOADED_CHECK(027, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(028, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(029, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(032, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(033, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(034, sCarrec)
				AND HAS_RECORDING_LOADED_CHECK(035, sCarrec)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictPrologue6)
					Finale()
				ENDIF
			BREAK
			CASE passMission
				missionPassed()
			BREAK
			CASE failMission
				missionFailed()
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
	
//Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
