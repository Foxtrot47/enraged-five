// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Armenian1.sc
//		AUTHOR			:	Matthew Booton
//		DESCRIPTION		:	Franklin and a buddy collect two cars and race them across town.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_cutscene.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_recording.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"

USING "cheat_controller_public.sch"
USING "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "flow_processing_game.sch"
USING "flow_public_core.sch"
USING "script_blips.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "chase_hint_cam.sch"
USING "locates_public.sch"
USING "selector_public.sch"
USING "replay_public.sch"
USING "comms_control_public.sch"
USING "cam_recording_public.sch"
USING "help_at_location.sch"
USING "vehicle_gen_public.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "shop_public.sch"
USING "code_control_data_gta5.sch"
USING "tv_control_public.sch"
USING "achievement_public.sch"

// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					117
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					15	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				38	

// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		15  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		12					

// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		10

USING "traffic.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

ENUM MISSION_STAGE
	STAGE_OPENING_CUTSCENE = 0,
	STAGE_CHOOSE_CAR,
	STAGE_START_CHASE_CUTSCENE,
	STAGE_CHASE,
	STAGE_CHASE_MID_POINT,
	STAGE_COPS_ARRIVE_CUTSCENE,
	STAGE_GO_TO_GARAGE,
	STAGE_SHOWROOM_INTRO_CUTSCENE,
	STAGE_MEET_SIMEON,
	STAGE_SHOWROOM_CUTSCENE,
	STAGE_GO_TO_HOUSE,
	STAGE_HOUSE_CUTSCENE,
	STAGE_RECORD_TRAFFIC,
	STAGE_RECORD_SET_PIECES,
	STAGE_REDO_TRIGGER_RECORDING,
	STAGE_RECORD_CAMERA,
	STAGE_PLAYBACK_CAMERA,
	STAGE_DEBUG_RECORD_TRAILER,
	STAGE_DEBUG_RESPAWN_CAM_TEST,
	STAGE_DEBUG_COMPARE_RECORDINGS
ENDENUM

ENUM MISSION_REQUIREMENT
	REQ_NINEF,
	REQ_RAPIDGT,
	REQ_UBER_CHASE_LOADED,
	REQ_FRANKLINS_CAR,
	REQ_ROOFS_DOWN,
	REQ_PLAYER_INSIDE_CAR,
	REQ_LAMAR_INSIDE_CAR,
	REQ_CARS_CHOSEN,
	REQ_MAIN_RECORDING_LOADED,
	REQ_ARRIVAL_CAR_RECORDINGS_CHOSEN,
	REQ_RAGE_BAR_NEARLY_FULL,
	REQ_LAMARS_CAR_IN_GARAGE,
	REQ_BANK_SHUTTERS_REMOVED,
	REQ_SHOWROOM_CARS
ENDENUM

ENUM FAILED_REASON
	FAILED_GENERIC = 0,
	FAILED_KILLED_BUDDY,
	FAILED_DESTROYED_CAR,
	FAILED_DESTROYED_A_CAR,
	FAILED_DESTROYED_BOTH_CARS,
	FAILED_LOST_BUDDY,
	FAILED_DESTROYED_FRANKLINS_CAR,
	FAILED_CAR_STUCK,
	FAILED_FRANKLINS_CAR_STUCK,
	FAILED_ABANDONED_LAMAR,
	FAILED_ALERTED_COPS,
	FAILED_DAMAGED_CARS_BEFORE_RACE,
	FAILED_DAMAGED_CARS_AFTER_DROPOFF,
	FAILED_ABANDONED_CAR,
	FAILED_TRASH_SHOWROOM,
	FAILED_DROVE_IN_SHOWROOM,
	FAILED_BROKEN_INTO_CAR,
	FAILED_DISTURBED_SHOWROOM,
	FAILED_DISTURBED_DEAL,
	FAILED_KILLED_SIMEON,
	FAILED_KILLED_JIMMY, 
	FAILED_LAMARS_CAR_DAMAGED,
	FAILED_LED_COPS_TO_SHOWROOM
ENDENUM

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP,
	SECTION_STAGE_JUMPING_TO_STAGE
ENDENUM

ENUM CREDITS_STAGE
	CREDITS_STAGE_0_SHRINK = 0,
	CREDITS_STAGE_1_LEAVE_SHRINK,
	CREDITS_STAGE_2_OVERHEAD,
	CREDITS_STAGE_3_SPORTS_CAR,
	CREDITS_STAGE_4_SPORTS_CAR_PARK,
	CREDITS_STAGE_5_YOGA,
	CREDITS_STAGE_6_FERRIS_FLAG,
	CREDITS_STAGE_7_FERRIS_OVERHEAD,
	CREDITS_STAGE_8_COUPLE_PIER,
	CREDITS_STAGE_9_MAD_CYCLIST,
	CREDITS_STAGE_10_GYM,
	CREDITS_STAGE_11_SUNBATHERS,
	CREDITS_STAGE_12_SUNBATHER_DOG,
	CREDITS_STAGE_13_SUNBATHER_DOG_2,
	CREDITS_STAGE_14_HOUSES,
	CREDITS_STAGE_15_MICHAEL_JOGGERS,
	CREDITS_STAGE_16_MICHAEL_JOGGERS_2,
	CREDITS_STAGE_17_BUM,
	CREDITS_STAGE_18_MICHAEL_BUM,
	CREDITS_STAGE_19_MICHAEL_SIT,
	CREDITS_STAGE_20_FRANKLIN_ARRIVES,
	CREDITS_STAGE_21_LAMAR_ASKS_MICHAEL,
	CREDITS_STAGE_22_END,
	CREDITS_STAGE_NUM_STAGES
ENDENUM

ENUM TRIGGERED_TEXT_LABEL
	ARM1_EXTRAS2 = 0,
	AR1_CHASE,
	AR1_WAIT,
	AR1_CHOOSE,
	AR1_CARHELP,
	ARM1_INTP6_1,
	ARM1_GOTD,
	ARM1_GOTD2,
	ARM1_PICK,
	ARM1_RACE,
	ARM1_RACE_PRELOAD,
	AR1_CONVHELP,
	AR1_CAMHELP,
	AR1_BRAKE,
	ARM1_CYCL,
	ARM1_ALIEN,
	ARM1_TOW,
	ARM1_TOW2,
	ARM1_RING,
	AR1_RAGEBAR,
	AR1_RAGEHOW,
	AR1_RAGESTAT,
	AR1_RAGEDEACT,
	AR1_HEADHELP,
	AR1_JUMPHELP,
	AR1_JUMPHELP2,
	ARM1_RINGINDIC,
	ARM1_PARK2,
	ARM1_BANK,
	ARM1_HILL,
	ARM1_WRONG,
	ARM1_WARN1,
	ARM1_WARN2,
	ARM1_FRONT1,
	ARM1_FRONT2,
	ARM1_FRONT3,
	ARM1_STREET,
	ARM1_RINGTOW,
	ARM1_BABY,
	AR1_PARK,
	CMN_GENGETBCK,
	CMN_FPSHELP,
	ARM1_DISS_FINISHED,
	ARM1_DISS_PRELOADED,
	ARM1_COPS_3,
	AR1_COPHELP1,
	AR1_COPHELP2,
	AR1_COPHELP2B,
	AR1_COPHELP3,
	AR1_DUCKHELP,
	AR1_COPAMB,
	AR1_SHOWROOM,
	ARM1_MCS1LI,
	ARM1_LEAVEG,
	AR1_VIEWSTATS,
	AR1_VIEWSTATS2,
	ARM1_DRIV,
	ARM1_DRIVEND,
	ARM1_ATHOME,
	AR1_GARHELP1,
	AR1_UNIQUE,
	AR1_APPHELP,
	ARM1_MCS1LO,
	AR1_BARBERS,
	ARM1_HOUSE,
	ARM1_CHAT1,
	ARM1_CHAT2,
	ARM1_CHAT3,
	ARM1_CHAT4,
	ARM1_CHAT5,
	ARM1_CHAT6,
	ARM1_CHAT7,
	ARM1_INDIC,
	ARM1_STDIO,
	AR1_CHOICE,
	ARM1_LOSE,
	ARM1_RADIO1,
	ARM1_FNKLEV1,
	AR1_CAMHELP2,
	AR1_VEHCAMH
ENDENUM

STRUCT MISSION_PED
	PED_INDEX ped
	BLIP_INDEX blip
	INT iEvent
	INT iTimer
	INT iSyncedScene
ENDSTRUCT

STRUCT BIRD_DATA
	PED_INDEX ped 
	INT iEvent
	INT iTimer
	VECTOR vDir
	FLOAT fSpeed
ENDSTRUCT

STRUCT CHASE_VEHICLE
	VEHICLE_INDEX veh
	BLIP_INDEX blip
	
	VECTOR vStartPos
	FLOAT fStartHeading
	INT iStartCarrec
ENDSTRUCT

STRUCT RAGE_BAR_DATA
	FLOAT fCurrentRage
	BOOL bHasRaged
ENDSTRUCT

BOOL bCloseFinish						= FALSE
BOOL bFranklinsCarTrashed				= FALSE
BOOL bLamarsCarTrashed					= FALSE
BOOL bBuddyFinished						= FALSE
BOOL bTruckHornActivated				= FALSE
BOOL bBusHornActivated					= FALSE
BOOL bVanHornActivated					= FALSE
BOOL bChangedTruckColour				= FALSE
BOOL bTrafficHeliRotorsOn				= FALSE
//BOOL bTrailerSkidsActive				= FALSE
BOOL bCutsceneSkipped					= FALSE
BOOL bGrabbedCreditsSportsCar			= FALSE
BOOL bShowroomCarsBlocked 				= FALSE
BOOL bLamarJustTeasedFranklin			= FALSE
BOOL bLamarJustInstructedFranklin		= FALSE
BOOL bMissionFailed						= FALSE
//BOOL bAlreadyPlayedCarTrashedDialogue	= FALSE
BOOL bAllowSpecialAbilityDuringChase	= TRUE
BOOL bPlayFirstPartOfCopsCutscene		= TRUE
BOOL bCustomGPSActive					= FALSE
BOOL bCarCollisionIsDisabled			= FALSE
BOOL bPlayedCyclistBellSound			= FALSE
BOOL bPlayerControlTurnedOffByMission	= FALSE
BOOL bTriggeredShowroomCutFromLeft		= FALSE
BOOL bBypassedShowroomCutLocates		= FALSE
BOOL bRequestedFranklinsCarAsset		= FALSE
BOOL bLostWantedLevelForFirstTime		= FALSE
BOOL bAliensStatHasBeenSet				= FALSE
BOOL bAlleyAssistedRouteActive			= FALSE
BOOL bUseNewRubberBanding				= FALSE
BOOl bCarOwnerActive					= FALSE
BOOL bPlayerHasBeenInCar				= FALSE
BOOL bPlayLamarRearViewCam				= FALSE
BOOL bHideCredits						= FALSE
BOOL bCurrentVehicleIsOutsideShowroom	= FALSE
BOOL bDialToneTriggered					= FALSE
BOOL bForcedObjectsAreActive			= FALSE
BOOL bLamarGivenWalkToCarTask			= FALSE
BOOL bMovieBarrierSmashed				= FALSE
BOOL bSafeToPlayCamAnim					= FALSE
BOOL bForceAnimatedCamsOn				= TRUE
BOOL bCyclistsSetToFlee					= FALSE
BOOL bLamarAlreadyGotIntoCar			= FALSE
BOOL bClearedHelp						= FALSE
BOOL bUsedACheckpoint					= FALSE
BOOL bSafeToDoSeamlessCarTasks			= FALSE
BOOL bPlayedPoliceReport				= FALSE
BOOL bRequestedCreditsScaleform			= FALSE
BOOL bOverriddenVanColour				= FALSE
BOOL bReplayEventStarted				= FALSE
BOOL bSkippedSpecialAbilityTutorial		= FALSE
BOOL bWarpedCarEarlyForShowroomScene	= FALSE
BOOL bForcedFirstPersonView				= FALSE
BOOL bHasFirstPersonFlashTriggered		= FALSE
BOOL bGrabbedMapObjectsForCredits		= FALSE
BOOL bUnlockedMichaelsMansion			= FALSE
BOOL bHasTextLabelTriggered[80]

FLOAT fCurrentPlaybackTime				= 0.0
FLOAT fCurrentPlaybackSpeed				= 0.0
FLOAT fNextActionCamFov					= 0.0 
FLOAT fFranklinsCarStartHeading			= 123.7440 //71.4776
FLOAT fFranklinByCarHeading				= -77.7//294.6570
FLOAT fLamarByCarHeading				= -87.9//263.4475
FLOAT fLamarLeadOutHeading				= 322.2873
FLOAT fFranklinLeadOutHeading			= 316.7220
FLOAT fLamarByStartCarHeading			= 251.8757
FLOAT fFranklinsCarFinalHeading			= 0.4923//275.1384
FLOAT fSpeedWhenHitFinalLocate			= 0.0
FLOAT fJimmyStartHeading				= 17.3
FLOAT fSimeonStartHeading				= -160.9
FLOAT fPlayersCarShowroomHeading		= 153.3213
FLOAT fLamarsCarShowroomHeading			= 159.5713


CONST_INT MAX_CAR_HEALTH				2000
CONST_INT CARREC_BUDDY					5
CONST_INT CARREC_BUDDY_ALTERNATE		5 //Currently the same as CARREC_BUDDY, but may need to be shifted to match the alternate vehicle
CONST_INT CARREC_BUDDY_ESCAPE			8
CONST_INT CARREC_BUDDY_ESCAPE_2			9
CONST_INT CARREC_COP_ESCAPE				300
CONST_INT CARREC_COP_ESCAPE_START		301
CONST_INT CARREC_SLOMO_RAPIDGT			320
CONST_INT CARREC_SLOMO_NINEF			321
CONST_INT CARREC_SHOWROOM_NINEF_1		330
CONST_INT CARREC_SHOWROOM_NINEF_2		331
CONST_INT CARREC_SHOWROOM_RAPIDGT_1		332
CONST_INT CARREC_SHOWROOM_RAPIDGT_2		333
CONST_INT CARREC_CREDITS_BOAT_1			601
CONST_INT CARREC_CREDITS_BOAT_2			602
CONST_INT SETPIECE_PACKER_INDEX			15
CONST_INT NINEF_INDEX					0
CONST_INT RAPIDGT_INDEX					1
CONST_INT CARREC_TRAILER				991
CONST_INT CARREC_TRAILER_2				993
CONST_INT CHECKPOINT_MID_CHASE			1
CONST_INT CHECKPOINT_LOSE_COPS			2
CONST_INT CHECKPOINT_GO_HOME			3
CONST_INT DECAL_SAND_TREAD_ID			3011
CONST_INT CREDITS_FEMALE_1				0
CONST_INT CREDITS_FEMALE_2				1
CONST_INT CREDITS_MALE_1				2
CONST_INT CREDITS_FEMALE_3				3
CONST_INT CREDITS_MALE_2				4
CONST_INT CREDITS_MALE_3				5
CONST_INT CREDITS_MALE_4				6
CONST_INT CREDITS_JOGGER_1				7
INT iCurrentEvent						= 0
INT iPlayersCar							= 0
INT iBuddiesCar							= 0
INT iCarrecBuddyEscape 					= 0
INT iCarrecBuddyArrive 					= 0
INT iCarrecPlayerArrive 				= 0
INT iCarrecMain							= 0
//INT iBuddyCallTimer					= 0
INT iCineCamPhoneTimer					= 0
INT iCurrentCinematicCam				= 0
INT iCrashTimer							= 0
INT iCloseTimer							= 0
INT iTimeSinceLastHorn					= 0
INT iTimeSinceLastNearMissDialogue		= 0
INT iLamarRevSound						= 0
INT iAmbientDialogueTimer				= 0
INT iCamRecs[10]
INT iDispatchSpawnBlockBank1			= -1
INT iDispatchSpawnBlockBank2			= -1
//INT iClosestVehicleCheckTimer			= 0
INT iHurryDialogueTimer					= 0
INT iNumTimesPlayedHurryDialogue 		= 0
INT iStartFailTimer						= 0
INT iStartFailUnarmedCounter			= 0
INT iRaceChatTimer						= 0
INT iInFrontChatTimer					= 0
INT iTimeWhenFranklinGotInFront			= 0
//INT iNumTimesMissedFinalCall			= 0
INT iPlayerSyncScene					= 0
INT iCameraSyncScene					= 0
//INT iCarSyncScene						= 0
INT iPreloadRecordingsIndex				= 0
INT iBusLightsTimer						= 0
INT iRandomCarCreationEvent				= 0
//INT iClockTime[3]
INT iPlayersHealthBeforeRace			= 0
INT iLamarsHealthBeforeRace				= 0
INT iLoseCopsTimer						= 0
INT iCopsAITimer						= 0
INT iChasePedBlockingArea				= -1
INT iChasePedBlockingArea2				= -1
INT iGatePedBlockingArea				= -1
INT iShowroomPedBlockingArea			= -1
INT iShowroomPedBlockingArea2			= -1
INT iRageHelpTimer						= 0
INT iHintCamHelpTimer					= 0
INT iPrevUnarmedHits					= 0
INT iNumTimesPlayedLostDialogue			= 0
INT iNumTimesPlayedFailWarnDialogue		= 0
INT iLostDialogueTimer					= 0
INT iWarnDialogueTimer					= 0
INT iWarnDialogueTimer2					= 0
INT iInFrontHornTimer					= 0
INT iStatsHelpTextTimer					= 0
INT iAudioSceneEventDialPhone			= 0
INT iAudioSceneEventLoseCops			= 0
INT iAudioSceneEventMovieStudio			= 0
INT iAudioSceneEventBumps				= 0
INT iAudioSceneEventCarPark				= 0
INT iLamarWalkToCarTimer				= 0
INT iCreditsTextEvent					= 0
INT iSyncSceneJimmySimeon				= 0
INT iSyncSceneCar						= 0
INT iCreditsEvent						= 0
INT iMeleeDialogueTimer					= 0
INT iJimmySimeonDialogueTimer			= 0
INT iNumJimmySimeonConversationsPlayed	= 0
INT iNumTimesPlayedSimeonDialogue		= 0
INT iCarHelpTimer						= 0
INT iLamarFireTimer						= 0
INT iChaseCamTimer						= 0
INT iLamarDamageDialogueTimer			= 0
INT iNavmeshBlockCutscene 				= -1
INT iDuckHelpTimer						= 0
INT iSyncSceneFerrisWheel[10]

VECTOR vOutsideGarage				= <<-31.1700, -1090.7544, 25.4344>>
VECTOR vNextToBuddy					= <<0.0, 0.0, 0.0>>
VECTOR vFranklinsHouse				= <<-25.4559, -1426.9977, 29.6560>>
VECTOR vRoomCorner1 				= <<-56.8445, -1107.2435, 24.4344>>
VECTOR vRoomCorner2 				= <<-33.6656, -1092.6023, 30.4344>>
VECTOR vFranklinsCarStartPos		= <<-16.1017, -1079.5923, 25.6721>> //<<-20.2035, -1084.2188, 25.6348>>
VECTOR vFranklinByCarPos			= <<-40.54, -1096.27, 25.43>>//<<-25.0341, -1084.6193, 25.5747>>
VECTOR vLamarByCarPos				= <<-42.0, -1095.5, 25.43>> //<<-25.1225, -1082.4664, 25.6048>> 
VECTOR vLamarLeadOutPos				= <<-1899.2156, -593.5246, 10.8973>>
VECTOR vFranklinLeadOutPos			= <<-1899.7908, -594.6526, 10.8853>>
VECTOR vLamarByStartCarPos			= <<-1883.5654, -575.7982, 10.7861>>
VECTOR vFranklinsCarFinalPos		= <<-24.9598, -1437.6470, 29.6552>> //<<-13.8292, -1458.0117, 29.4598>>
VECTOR vJimmyStartPos				= <<-42.95, -1097.30, 25.41>>
VECTOR vSimeonStartPos				= <<-43.98, -1095.47, 25.41>>
VECTOR vPlayersCarInShowroom		= <<-29.9308, -1089.8215, 25.4221>>
VECTOR vLamarsCarInShowroom			= <<-34.2931, -1089.3936, 25.4222>> //<<-33.5231, -1089.3937, 25.4222>>
VECTOR vFerrisWheelCentre 			= <<-1663.970, -1126.700, 30.700>>
//VECTOR vPrevCreditsCamPos			= <<0.0, 0.0, 0.0>>

STRING strCarrec					= "mattarmenian"
STRING strStartCutscene				= "armenian_1_int"
STRING strGarageCutscene			= "armenian_1_mcs_1"
STRING strHouseCutscene				= "arm_1_mcs_2_concat"
STRING strChaseStartAnims			= "missarmenian1walktocar"
STRING strShowroomCamAnims			= "missarmenian1@dealership"
STRING strChaseEndAnims				= "missarmenian1banter"
STRING strAlienAnims				= "missarmenian1movieextras"
STRING strLamarAnnoyedAnims			= "missarmenian1ig_13"
STRING strLamarLeadOutAnims			= "missarmenian1leadinout"
STRING strShowroomLeadInOutAnims	= "missarmenian1leadinoutarmenian_1_mcs_1_leadinout"
STRING strWaypointStartAlley		= "arm1_01"
STRING strWaypointAroundCar			= "arm1_05"
STRING strLamarCarAnimsTaunt		= "missarmenian1driving_taunts@lamar_1"
STRING strLamarCarAnimsCrash		= "missarmenian1driving_taunts@lamar_2"
STRING strFranklinCarAnimsTaunt		= "missarmenian1driving_taunts@franklin"
STRING strLamarShowroomAnims		= "missarmenian1leadinoutarm_1_ig_14_leadinout"
STRING strFerrisWheelAnims			= "MISSFINALE_C2IG_5"
//STRING strWaypointCreditsBoat		= "arm1_10"


VEHICLE_INDEX vehTriggerCar
VEHICLE_INDEX vehCutsceneCop
VEHICLE_INDEX vehTrailer
VEHICLE_INDEX vehFranklinsCar
VEHICLE_INDEX vehCameraTest
VEHICLE_INDEX vehTransporter
VEHICLE_INDEX vehTanker
VEHICLE_INDEX vehShowroomCars[4]
VEHICLE_INDEX vehCarsOutsideShowroom[1] //Used to be more cars, can probably remove this array at some point.
VEHICLE_INDEX vehCreditsCars[20]
VEHICLE_INDEX vehFinalCutsceneCar
VEHICLE_INDEX vehCreditsBoat
VEHICLE_INDEX vehCreditsSailboat
VEHICLE_INDEX vehCreditsSportsCar

PED_INDEX pedCutsceneCops[2]
PED_INDEX pedCurrentAngryDriver
PED_INDEX pedCarOwner
PED_INDEX pedCreditsBoat
PED_INDEX pedCreditsFerrisWheel[10]
PED_INDEX pedCreditsCars[20]

PTFX_ID ptfxCreditsBoat[2]

BLIP_INDEX blipCurrentDestination
BLIP_INDEX blipLamarsCar
BLIP_INDEX blipFakeGPS

CAMERA_INDEX camCutscene
CAMERA_INDEX camChase

OBJECT_INDEX objCigarette
OBJECT_INDEX objShowroomGlass[8]
OBJECT_INDEX objShirt
OBJECT_INDEX objHealthPack
OBJECT_INDEX objWardrobeDoors[2]
OBJECT_INDEX objVaultShutter
OBJECT_INDEX objLamarsPhone
OBJECT_INDEX objFerrisWheel
OBJECT_INDEX objFerrisCars[16]
OBJECT_INDEX objBankGates[2]
OBJECT_INDEX objBankGateCollision[2]

SCALEFORM_INDEX sfCredits

//PICKUP_INDEX pickupHouse

PTFX_ID iWheelSkidPtfx[2]

INTERIOR_INSTANCE_INDEX interiorShowroom
INTERIOR_INSTANCE_INDEX interiorFranklinsHouse
INTERIOR_INSTANCE_INDEX interiorShrink
INTERIOR_INSTANCE_INDEX interiorBank
INTERIOR_INSTANCE_INDEX interiorCarPark
INTERIOR_INSTANCE_INDEX interiorCarPark2

SCENARIO_BLOCKING_INDEX sbiMovieStudio
SCENARIO_BLOCKING_INDEX sbiPastMovieStudio
SCENARIO_BLOCKING_INDEX sbiShowroom
SCENARIO_BLOCKING_INDEX sbiShowroomBrowse
SCENARIO_BLOCKING_INDEX sbiSecurityGuards
SCENARIO_BLOCKING_INDEX sbiCutscene


//GROUP_INDEX groupPlayer

MODEL_NAMES modelMainCar1 			= NINEF2
MODEL_NAMES modelMainCar2			= RAPIDGT2
MODEL_NAMES modelCamCar				= manana //emperor //Model is one of the common models used in the chase to save memory
MODEL_NAMES modelBankGate			= P_Sec_Gate_01_S
MODEL_NAMES modelBankGateCollision	= P_SEC_GATE_01_S_COL
MODEL_NAMES modelBankSecurity		= S_M_M_Armoured_01
MODEL_NAMES modelMovieGuards		= S_M_M_SECURITY_01
MODEL_NAMES modelCarOwner 			= A_M_Y_BEACH_02 //U_M_Y_BABYD
MODEL_NAMES modelShirt				= P_CS_SHIRT_01_S
MODEL_NAMES modelWardrobeLeft		= V_ILEV_FA_WARDDOORL
MODEL_NAMES modelWardrobeRight		= V_ILEV_FA_WARDDOORR
MODEL_NAMES modelHealthPack			= prop_ld_health_pack
MODEL_NAMES modelCreditsBoat		= JETMAX
MODEL_NAMES modelCreditsSailboat	= MARQUIS
MODEL_NAMES modelCreditsBoatPed		= A_M_Y_BEACH_01
MODEL_NAMES modelLamarsPhone		= PROP_PHONE_ING
MODEL_NAMES modelFerrisWheel 		= PROP_LD_FERRIS_WHEEL
MODEL_NAMES modelFerrisCar 			= PROP_FERRIS_CAR_01
MODEL_NAMES modelFerrisWheelPed1	= A_F_Y_BEACH_01
MODEL_NAMES modelFerrisWheelPed2	= A_M_Y_BEACH_01
MODEL_NAMES modelJoggerFemale		= A_F_Y_FITNESS_02
MODEL_NAMES modelCreditsMale		= A_M_Y_BEACHVESP_01
MODEL_NAMES modelCreditsFemale		= A_F_Y_HIPSTER_02
//MODEL_NAMES modelFranklinDoor 	= V_ILEV_FA_ROOMDOOR

MISSION_PED sLamar
MISSION_PED sSimeon
MISSION_PED sJimmy
MISSION_PED sAliens[4]
MISSION_PED sAliensGroup2[2]
MISSION_PED sSmoker
//MISSION_PED sSetSmash[2]
BIRD_DATA sBirds[6]
MISSION_PED sSecurityGuard
MISSION_PED sMovieGuards[2]
CHASE_VEHICLE sMainCars[2]
RAGE_BAR_DATA sRageData
LOCATES_HEADER_DATA sLocatesData
SELECTOR_PED_STRUCT sDummySelectorPeds
//DOOR_LOCK_DATA sStudioGates[2]

CREDITS_STAGE eCreditsStage				= CREDITS_STAGE_0_SHRINK
MISSION_STAGE eMissionStage 			= STAGE_OPENING_CUTSCENE
SECTION_STAGE eSectionStage 			= SECTION_STAGE_SETUP
CAM_VIEW_MODE ePrevCarViewMode			= CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
STRING strFailText

structPedsForConversation sConversationPeds
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
	
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetDebug

	BOOL bDebugRecordTraffic			= FALSE
	BOOL bDebugRecordSetpieces			= FALSE
	BOOL bDebugRedoTrigger				= FALSE
	BOOL bDebugShowChaseStats			= FALSE
	BOOL bDebugEnhanceDriving			= FALSE
	BOOL bDebugAttachFilmSet			= FALSE
	BOOL bDebugAutoPlayTrigger			= FALSE
	BOOL bDebugRecordCamera				= FALSE
	BOOL bDebugPlaybackCamera			= FALSE
	BOOL bDebugFreeModeCamera			= FALSE
	BOOL bDebugPlayEdit					= FALSE
	BOOL bDebugRecordEdit				= FALSE
	BOOL bDebugPlayMixdown				= FALSE
	BOOL bDebugRecordTrailer			= FALSE
	BOOL bDebugDontUseStopWait			= FALSE
	BOOL bDebugDisplayMissionDebug 		= FALSE
	BOOL bDebugEnableHornAsserts		= FALSE
	BOOL bDebugTestRespawnCutscene		= FALSE
	BOOL bDebugUseNewAbilityButton		= FALSE
	BOOL bDebugSticksReleased			= FALSE
	BOOL bDebugCompareTriggerRecordings	= FALSE
	BOOL bDebugDisplayCreditsText		= FALSE
	BOOL bDebugTestCreditsSingleLine	= FALSE
	BOOL bDebugTestCreditsLogo			= FALSE
	BOOL bDebugTestCreditsLogoHide		= FALSE
	BOOL bDebugClearCreditsText			= FALSE
	BOOL bDebugDisplayCreditsInfo		= FALSE
	BOOL bDebugUnloadCloudHats			= FALSE
	BOOL bDebugLoadWispyCloud			= FALSE
	BOOL bDebugLoadHorizonCloud			= FALSE
	BOOL bDebugLoadContrailsCloud		= FALSE
	BOOL bDebugLoadPuffsCloud			= FALSE
	BOOL bDebugLoadStormyCloud			= FALSE
	BOOL bDebugLoadAltoCloud			= FALSE
	BOOL bDebugFerrisWheelTest			= FALSE
	BOOL bDebugFerrisWheelReattach		= FALSE
	
	INT iDebugCurrentCam				= 0
	INT iDebugCloudHat					= 0
	INT iDebugWheelAnim					= 0
	INT iDebugCreditsIndex				= 0
	
	TEXT_WIDGET_ID textWidgetRole1
	TEXT_WIDGET_ID textWidgetNames1
	TEXT_WIDGET_ID textWidgetRole2
	TEXT_WIDGET_ID textWidgetNames2
	TEXT_WIDGET_ID textWidgetRole3
	TEXT_WIDGET_ID textWidgetNames3
	TEXT_WIDGET_ID textWidgetCreditsAlign
	TEXT_WIDGET_ID textWidgetCreditsColour
	
	FLOAT fDebugCreditsBlockX = 0.0
	FLOAT fDebugCreditsBlockY = 70.0
	FLOAT fDebugRole1XOffset = 0.0
	FLOAT fDebugRole2XOffset = 0.0
	FLOAT fDebugRole3XOffset = 0.0
	FLOAT fDebugNames1XOffset = 0.0
	FLOAT fDebugNames2XOffset = 0.0
	FLOAT fDebugNames3XOffset = 0.0
	FLOAT fDebugTimeSinceLeftStickPressed 	= 0.0
	FLOAT fDebugOldTriggerTime 				= 0.0
	FLOAT fDebugNewTriggerTime 				= 0.0
	
	VECTOR vDebugFilmSetPos				= <<-0.040, 7.080, 1.280>>//<<-0.040, 1.230, 1.280>>
	VECTOR vDebugFilmSetRot				= <<-8.400, 0.000, -90.400>>//<<8.400, 0.000, 90.400>> 
	VECTOR vDebugVectorA				= <<0.0, 0.0, 0.0>>
	VECTOR vDebugVectorB				= <<0.0, 0.0, 0.0>>
	VECTOR vDebugVectorResult			= <<0.0, 0.0, 0.0>>
	VECTOR vDebugFerrisWheelOffset		= <<0.0, 0.0, 0.0>>
	VECTOR vDebugFerrisWheelRot			= <<0.0, 0.0, 0.0>>
	
	VEHICLE_INDEX vehDebug[10]
	VEHICLE_INDEX vehDebugFov
	VEHICLE_INDEX vehDebugCamCar
	
	CONST_INT MAX_SKIP_MENU_LENGTH 			12
	INT iDebugJumpStage						= 0
	INT iDebugNumPrintsThisFrame			= 0
	MissionStageMenuTextStruct sSkipMenu[MAX_SKIP_MENU_LENGTH]  
	
	CAM_RECORDING_DATA sCamData
	
	PROC CREATE_WIDGETS()
		widgetDebug = START_WIDGET_GROUP("Armenian1")	
			ADD_WIDGET_BOOL("Enable credits debug", bDebugDisplayCreditsInfo)
			ADD_WIDGET_BOOL("Compare trigger recordings", bDebugCompareTriggerRecordings)
			ADD_WIDGET_BOOL("New rubber banding", bUseNewRubberBanding)
			ADD_WIDGET_BOOL("Do respawn cutscene test", bDebugTestRespawnCutscene)
			ADD_WIDGET_BOOL("Enable horn asserts", bDebugEnableHornAsserts)
			ADD_WIDGET_BOOL("Show debug", bDebugDisplayMissionDebug)
			ADD_WIDGET_BOOL("Show chase info", bDebugShowChaseStats)
			ADD_WIDGET_BOOL("Don't wait after car stop", bDebugDontUseStopWait)
			ADD_WIDGET_BOOL("Allow special ability during chase", bAllowSpecialAbilityDuringChase)
			ADD_WIDGET_BOOL("Use alternate special ability controls", bDebugUseNewAbilityButton)
			ADD_WIDGET_BOOL("Do Lamar wait cutscene variation", bPlayLamarRearViewCam)
			ADD_WIDGET_BOOL("Force animated cams on", bForceAnimatedCamsOn)
			
			START_WIDGET_GROUP("Ferris wheel test")
				ADD_WIDGET_BOOL("Enable test", bDebugFerrisWheelTest)
				ADD_WIDGET_BOOL("Reattach", bDebugFerrisWheelReattach)
				ADD_WIDGET_INT_SLIDER("Anim", iDebugWheelAnim, 0, 4, 1)
				ADD_WIDGET_VECTOR_SLIDER("Attach", vDebugFerrisWheelOffset, -10.0, 10.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", vDebugFerrisWheelRot, 0, 360.0, 1.0)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Cops cutscene variations")
				ADD_WIDGET_BOOL("Franklin's car trashed", bFranklinsCarTrashed)
				ADD_WIDGET_BOOL("Lamar's car trashed", bLamarsCarTrashed)
				ADD_WIDGET_BOOL("Franklin finished first", bCloseFinish)
			STOP_WIDGET_GROUP()			
			
			START_WIDGET_GROUP("Credits")
				ADD_WIDGET_BOOL("Hide credits", bHideCredits)
				ADD_WIDGET_BOOL("Display logo", bDebugTestCreditsLogo)
				ADD_WIDGET_BOOL("Hide logo", bDebugTestCreditsLogoHide)
				ADD_WIDGET_BOOL("Display test text", bDebugDisplayCreditsText)
				ADD_WIDGET_BOOL("Clear test text", bDebugClearCreditsText)
				ADD_WIDGET_BOOL("Test single line", bDebugTestCreditsSingleLine)
				ADD_WIDGET_FLOAT_SLIDER("Credits block X", fDebugCreditsBlockX, -1000.0, 1000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Credits block Y", fDebugCreditsBlockY, -1000.0, 1000.0, 1.0)
				textWidgetCreditsAlign = ADD_TEXT_WIDGET("Align")
				textWidgetCreditsColour = ADD_TEXT_WIDGET("Colour")
				
				ADD_WIDGET_STRING("")
				
				textWidgetRole1 = ADD_TEXT_WIDGET("Role 1")
				ADD_WIDGET_FLOAT_SLIDER("Role 1 X offset", fDebugRole1XOffset, -1000.0, 1000.0, 1.0)
				textWidgetNames1 = ADD_TEXT_WIDGET("Names 1")
				ADD_WIDGET_FLOAT_SLIDER("Names 1 X offset", fDebugNames1XOffset, -1000.0, 1000.0, 1.0)
				textWidgetRole2 = ADD_TEXT_WIDGET("Role 2")
				ADD_WIDGET_FLOAT_SLIDER("Role 2 X offset", fDebugRole2XOffset, -1000.0, 1000.0, 1.0)
				textWidgetNames2 = ADD_TEXT_WIDGET("Names 2")
				ADD_WIDGET_FLOAT_SLIDER("Names 2 X offset", fDebugNames2XOffset, -1000.0, 1000.0, 1.0)
				textWidgetRole3 = ADD_TEXT_WIDGET("Role 3")
				ADD_WIDGET_FLOAT_SLIDER("Role 3 X offset", fDebugRole3XOffset, -1000.0, 1000.0, 1.0)
				textWidgetNames3 = ADD_TEXT_WIDGET("Names 3")
				ADD_WIDGET_FLOAT_SLIDER("Names 3 X offset", fDebugNames3XOffset, -1000.0, 1000.0, 1.0)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Clouds")
				ADD_WIDGET_INT_SLIDER("Cloud Hat", iDebugCloudHat, 0, 10, 1)
				ADD_WIDGET_BOOL("Load wispy", bDebugLoadWispyCloud)
				ADD_WIDGET_BOOL("Load stormy", bDebugLoadStormyCloud)
				ADD_WIDGET_BOOL("Load contrails", bDebugLoadContrailsCloud)
				ADD_WIDGET_BOOL("Load puffs", bDebugLoadPuffsCloud)
				ADD_WIDGET_BOOL("Load altostratus", bDebugLoadAltoCloud)
				ADD_WIDGET_BOOL("Load horizon", bDebugLoadHorizonCloud)
				ADD_WIDGET_BOOL("Unload all hats", bDebugUnloadCloudHats)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Chase recording")
				ADD_WIDGET_BOOL("Start traffic recording", bDebugRecordTraffic)
				ADD_WIDGET_BOOL("Play trigger while traffic recording", bDebugAutoPlayTrigger)
				ADD_WIDGET_BOOL("Start setpiece recording", bDebugRecordSetpieces)
				ADD_WIDGET_BOOL("Redo trigger recording", bDebugRedoTrigger)
				ADD_WIDGET_BOOL("Record trailer set-piece", bDebugRecordTrailer)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Compare trigger recordings")
				ADD_WIDGET_FLOAT_SLIDER("Old trigger time", fDebugOldTriggerTime, 0.0, 300000.0, 100.0)
				ADD_WIDGET_FLOAT_SLIDER("New trigger time", fDebugNewTriggerTime, 0.0, 300000.0, 100.0)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("billboard test")
				ADD_WIDGET_BOOL("Enable attach editing", bDebugAttachFilmSet)
				ADD_WIDGET_VECTOR_SLIDER("Pos", vDebugFilmSetPos, -10.0, 10.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rot", vDebugFilmSetRot, -180.0, 180.0, 0.1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Vector calculator")
				ADD_WIDGET_VECTOR_SLIDER("A", vDebugVectorA, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("B", vDebugVectorB, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Result", vDebugVectorResult, -10000.0, 10000.0, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Camera recording")
				ADD_WIDGET_BOOL("Start camera recording", bDebugRecordCamera)
				ADD_WIDGET_BOOL("Start camera playback", bDebugPlaybackCamera)
				ADD_WIDGET_BOOL("Free-mode camera", bDebugFreeModeCamera)
				ADD_WIDGET_BOOL("Play edited version", bDebugPlayEdit)
				ADD_WIDGET_BOOL("Record edited version as single recording", bDebugRecordEdit)
				ADD_WIDGET_BOOL("Play mixdown", bDebugPlayMixdown)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		SET_UBER_PARENT_WIDGET_GROUP(widgetDebug) 
		SET_LOCATES_HEADER_WIDGET_GROUP(widgetDebug)
		SET_CAM_RECORDING_WIDGET_GROUP(sCamData, widgetDebug)
	ENDPROC
	
	PROC DESTROY_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
			DELETE_WIDGET_GROUP(widgetDebug)
		ENDIF
	ENDPROC
	
	PROC DRAW_STRING_TO_DEBUG_DISPLAY(STRING strDebug)			
		IF bDebugDisplayMissionDebug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.05 + (0.05 * iDebugNumPrintsThisFrame), 0.0>>, 0, 0, 255, 255)
			iDebugNumPrintsThisFrame++
		ENDIF
	ENDPROC
	
	PROC DRAW_INT_TO_DEBUG_DISPLAY(INT iDebug, STRING strLabel)
		IF bDebugDisplayMissionDebug
			TEXT_LABEL strConcat = strLabel
			strConcat += iDebug
		
			DRAW_STRING_TO_DEBUG_DISPLAY(strConcat)
		ENDIF
	ENDPROC
	
	PROC DRAW_FLOAT_TO_DEBUG_DISPLAY(FLOAT fDebug, STRING strLabel)
		IF bDebugDisplayMissionDebug
			TEXT_LABEL strConcat = strLabel
			strConcat += GET_STRING_FROM_FLOAT(fDebug)
		
			DRAW_STRING_TO_DEBUG_DISPLAY(strConcat)
		ENDIF
	ENDPROC
#ENDIF

PROC LOAD_CHASE_UBER_DATA()
	TrafficCarPos[0] = <<-1869.4471, -533.4042, 11.2620>>
	TrafficCarQuatX[0] = -0.0008
	TrafficCarQuatY[0] = -0.0089
	TrafficCarQuatZ[0] = 0.4259
	TrafficCarQuatW[0] = 0.9047
	TrafficCarRecording[0] = 47
	TrafficCarStartime[0] = 2000.0000
	TrafficCarModel[0] = intruder

	TrafficCarPos[1] = <<-2017.4979, -425.4558, 11.0772>>
	TrafficCarQuatX[1] = -0.0102
	TrafficCarQuatY[1] = -0.0039
	TrafficCarQuatZ[1] = 0.4351
	TrafficCarQuatW[1] = 0.9003
	TrafficCarRecording[1] = 250
	TrafficCarStartime[1] = 2050.0000
	TrafficCarModel[1] = intruder

	TrafficCarPos[2] = <<-2140.0793, -363.5818, 12.7760>>
	TrafficCarQuatX[2] = 0.0018
	TrafficCarQuatY[2] = -0.0046
	TrafficCarQuatZ[2] = 0.8084
	TrafficCarQuatW[2] = -0.5886
	TrafficCarRecording[2] = 251
	TrafficCarStartime[2] = 2100.0000
	TrafficCarModel[2] = intruder

	TrafficCarPos[3] = <<-1994.4319, -437.5106, 11.7305>>
	TrafficCarQuatX[3] = -0.0021
	TrafficCarQuatY[3] = -0.0005
	TrafficCarQuatZ[3] = 0.4319
	TrafficCarQuatW[3] = 0.9019
	TrafficCarRecording[3] = 284
	TrafficCarStartime[3] = 2500.0000
	TrafficCarModel[3] = BUS

	TrafficCarPos[4] = <<-1924.5322, -488.0420, 11.3844>>
	TrafficCarQuatX[4] = -0.0082
	TrafficCarQuatY[4] = -0.0039
	TrafficCarQuatZ[4] = 0.4332
	TrafficCarQuatW[4] = 0.9012
	TrafficCarRecording[4] = 67
	TrafficCarStartime[4] = 3000.0000
	TrafficCarModel[4] = manana

	TrafficCarPos[5] = <<-1941.4397, -481.0277, 11.4080>>
	TrafficCarQuatX[5] = -0.0022
	TrafficCarQuatY[5] = -0.0095
	TrafficCarQuatZ[5] = 0.4264
	TrafficCarQuatW[5] = 0.9045
	TrafficCarRecording[5] = 68
	TrafficCarStartime[5] = 3050.0000
	TrafficCarModel[5] = intruder

	TrafficCarPos[6] = <<-2057.1311, -404.9521, 10.7321>>
	TrafficCarQuatX[6] = 0.0017
	TrafficCarQuatY[6] = -0.0034
	TrafficCarQuatZ[6] = 0.9104
	TrafficCarQuatW[6] = -0.4136
	TrafficCarRecording[6] = 30
	TrafficCarStartime[6] = 5000.0000
	TrafficCarModel[6] = manana

	TrafficCarPos[7] = <<-2036.5444, -436.0986, 11.0454>>
	TrafficCarQuatX[7] = 0.0002
	TrafficCarQuatY[7] = 0.0039
	TrafficCarQuatZ[7] = 0.9073
	TrafficCarQuatW[7] = -0.4204
	TrafficCarRecording[7] = 31
	TrafficCarStartime[7] = 5040.0000
	TrafficCarModel[7] = manana

	TrafficCarPos[8] = <<-1984.7413, -437.6598, 11.2996>>
	TrafficCarQuatX[8] = -0.0053
	TrafficCarQuatY[8] = -0.0097
	TrafficCarQuatZ[8] = 0.4337
	TrafficCarQuatW[8] = 0.9010
	TrafficCarRecording[8] = 32
	TrafficCarStartime[8] = 5080.0000
	TrafficCarModel[8] = intruder

	TrafficCarPos[9] = <<-1927.4957, -498.7926, 11.3251>>
	TrafficCarQuatX[9] = 0.0041
	TrafficCarQuatY[9] = 0.0017
	TrafficCarQuatZ[9] = 0.4102
	TrafficCarQuatW[9] = 0.9120
	TrafficCarRecording[9] = 35
	TrafficCarStartime[9] = 5120.0000
	TrafficCarModel[9] = intruder

	TrafficCarPos[10] = <<-2114.0156, -377.4624, 12.3287>>
	TrafficCarQuatX[10] = -0.0107
	TrafficCarQuatY[10] = -0.0139
	TrafficCarQuatZ[10] = 0.8500
	TrafficCarQuatW[10] = -0.5266
	TrafficCarRecording[10] = 36
	TrafficCarStartime[10] = 5160.0000
	TrafficCarModel[10] = intruder

	TrafficCarPos[11] = <<-2155.5698, -357.4149, 12.5796>>
	TrafficCarQuatX[11] = -0.0008
	TrafficCarQuatY[11] = -0.0001
	TrafficCarQuatZ[11] = 0.8151
	TrafficCarQuatW[11] = -0.5793
	TrafficCarRecording[11] = 33
	TrafficCarStartime[11] = 5200.0000
	TrafficCarModel[11] = manana

	TrafficCarPos[12] = <<-1955.9728, -417.3983, 17.2001>>
	TrafficCarQuatX[12] = -0.0326
	TrafficCarQuatY[12] = -0.0247
	TrafficCarQuatZ[12] = 0.4751
	TrafficCarQuatW[12] = 0.8790
	TrafficCarRecording[12] = 39
	TrafficCarStartime[12] = 9500.0000
	TrafficCarModel[12] = intruder

	TrafficCarPos[13] = <<-1760.4513, -547.8943, 35.8235>>
	TrafficCarQuatX[13] = -0.0664
	TrafficCarQuatY[13] = -0.0238
	TrafficCarQuatZ[13] = 0.4498
	TrafficCarQuatW[13] = 0.8904
	TrafficCarRecording[13] = 44
	TrafficCarStartime[13] = 14100.0000
	TrafficCarModel[13] = manana

	TrafficCarPos[14] = <<-1883.8774, -465.7772, 23.5569>>
	TrafficCarQuatX[14] = -0.0264
	TrafficCarQuatY[14] = -0.0213
	TrafficCarQuatZ[14] = 0.4694
	TrafficCarQuatW[14] = 0.8823
	TrafficCarRecording[14] = 43
	TrafficCarStartime[14] = 16733.0000
	TrafficCarModel[14] = intruder

	TrafficCarPos[15] = <<-1844.1317, -499.3117, 27.1005>>
	TrafficCarQuatX[15] = -0.0111
	TrafficCarQuatY[15] = 0.0344
	TrafficCarQuatZ[15] = 0.8823
	TrafficCarQuatW[15] = -0.4693
	TrafficCarRecording[15] = 46
	TrafficCarStartime[15] = 16781.0000
	TrafficCarModel[15] = intruder

	TrafficCarPos[16] = <<-1669.5397, -574.1034, 33.3805>>
	TrafficCarQuatX[16] = 0.0115
	TrafficCarQuatY[16] = -0.0228
	TrafficCarQuatZ[16] = 0.8892
	TrafficCarQuatW[16] = -0.4567
	TrafficCarRecording[16] = 252
	TrafficCarStartime[16] = 20000.0000
	TrafficCarModel[16] = intruder

	TrafficCarPos[17] = <<-1553.6389, -643.6885, 28.7234>>
	TrafficCarQuatX[17] = 0.0099
	TrafficCarQuatY[17] = -0.0012
	TrafficCarQuatZ[17] = 0.6289
	TrafficCarQuatW[17] = 0.7774
	TrafficCarRecording[17] = 253
	TrafficCarStartime[17] = 20050.0000
	TrafficCarModel[17] = intruder

	TrafficCarPos[18] = <<-1612.9589, -618.0091, 31.3797>>
	TrafficCarQuatX[18] = 0.0086
	TrafficCarQuatY[18] = -0.0260
	TrafficCarQuatZ[18] = 0.9021
	TrafficCarQuatW[18] = -0.4307
	TrafficCarRecording[18] = 254
	TrafficCarStartime[18] = 20100.0000
	TrafficCarModel[18] = manana

	TrafficCarPos[19] = <<-1598.3412, -514.3162, 34.7884>>
	TrafficCarQuatX[19] = -0.0217
	TrafficCarQuatY[19] = -0.0010
	TrafficCarQuatZ[19] = 0.9766
	TrafficCarQuatW[19] = 0.2140
	TrafficCarRecording[19] = 255
	TrafficCarStartime[19] = 20150.0000
	TrafficCarModel[19] = manana

	/*TrafficCarPos[20] = <<-1707.4438, -545.4189, 36.8241>>
	TrafficCarQuatX[20] = 0.0027
	TrafficCarQuatY[20] = -0.0139
	TrafficCarQuatZ[20] = 0.9113
	TrafficCarQuatW[20] = -0.4115
	TrafficCarRecording[20] = 48
	TrafficCarStartime[20] = 21153.0000
	TrafficCarModel[20] = manana*/

	TrafficCarPos[21] = <<-1664.3258, -561.4086, 33.5174>>
	TrafficCarQuatX[21] = 0.0283
	TrafficCarQuatY[21] = 0.0288
	TrafficCarQuatZ[21] = 0.2157
	TrafficCarQuatW[21] = 0.9756
	TrafficCarRecording[21] = 50
	TrafficCarStartime[21] = 21869.0000
	TrafficCarModel[21] = intruder

	TrafficCarPos[22] = <<-1617.1469, -600.6409, 32.0157>>
	TrafficCarQuatX[22] = 0.0260
	TrafficCarQuatY[22] = 0.0126
	TrafficCarQuatZ[22] = 0.4342
	TrafficCarQuatW[22] = 0.9003
	TrafficCarRecording[22] = 72
	TrafficCarStartime[22] = 22500.0000
	TrafficCarModel[22] = manana

	TrafficCarPos[23] = <<-1650.5247, -560.7370, 33.0326>>
	TrafficCarQuatX[23] = 0.0204
	TrafficCarQuatY[23] = -0.0131
	TrafficCarQuatZ[23] = 0.8283
	TrafficCarQuatW[23] = 0.5597
	TrafficCarRecording[23] = 51
	TrafficCarStartime[23] = 22584.0000
	TrafficCarModel[23] = manana

	TrafficCarPos[24] = <<-1626.8370, -568.3124, 33.0075>>
	TrafficCarQuatX[24] = 0.0218
	TrafficCarQuatY[24] = 0.0175
	TrafficCarQuatZ[24] = -0.4180
	TrafficCarQuatW[24] = 0.9080
	TrafficCarRecording[24] = 34
	TrafficCarStartime[24] = 25000.0000
	TrafficCarModel[24] = manana

	TrafficCarPos[25] = <<-1553.5674, -492.1296, 35.1499>>
	TrafficCarQuatX[25] = -0.0086
	TrafficCarQuatY[25] = -0.0063
	TrafficCarQuatZ[25] = 0.8700
	TrafficCarQuatW[25] = 0.4929
	TrafficCarRecording[25] = 49
	TrafficCarStartime[25] = 25050.0000
	TrafficCarModel[25] = manana

	TrafficCarPos[26] = <<-1566.8817, -512.4595, 35.1885>>
	TrafficCarQuatX[26] = 0.0010
	TrafficCarQuatY[26] = -0.0090
	TrafficCarQuatZ[26] = -0.4840
	TrafficCarQuatW[26] = 0.8750
	TrafficCarRecording[26] = 57
	TrafficCarStartime[26] = 27000.0000
	TrafficCarModel[26] = pcj

	TrafficCarPos[27] = <<-1405.6613, -398.7107, 36.1960>>
	TrafficCarQuatX[27] = -0.0010
	TrafficCarQuatY[27] = -0.0172
	TrafficCarQuatZ[27] = 0.8676
	TrafficCarQuatW[27] = 0.4969
	TrafficCarRecording[27] = 79
	TrafficCarStartime[27] = 28900.0000
	TrafficCarModel[27] = intruder

	/*TrafficCarPos[28] = <<-1540.7920, -502.6374, 35.3152>>
	TrafficCarQuatX[28] = 0.0080
	TrafficCarQuatY[28] = 0.0105
	TrafficCarQuatZ[28] = -0.5651
	TrafficCarQuatW[28] = 0.8249
	TrafficCarRecording[28] = 59
	TrafficCarStartime[28] = 29003.0000
	TrafficCarModel[28] = pcj*/

	TrafficCarPos[29] = <<-1385.9791, -406.3781, 36.1497>>
	TrafficCarQuatX[29] = 0.0030
	TrafficCarQuatY[29] = 0.0057
	TrafficCarQuatZ[29] = -0.4769
	TrafficCarQuatW[29] = 0.8789
	TrafficCarRecording[29] = 256
	TrafficCarStartime[29] = 30000.0000
	TrafficCarModel[29] = manana

	TrafficCarPos[30] = <<-1511.8464, -486.1111, 35.2985>>
	TrafficCarQuatX[30] = 0.0015
	TrafficCarQuatY[30] = 0.0010
	TrafficCarQuatZ[30] = -0.4916
	TrafficCarQuatW[30] = 0.8708
	TrafficCarRecording[30] = 61
	TrafficCarStartime[30] = 30912.0000
	TrafficCarModel[30] = intruder

	TrafficCarPos[31] = <<-1503.2793, -453.5208, 35.1137>>
	TrafficCarQuatX[31] = 0.0005
	TrafficCarQuatY[31] = 0.0009
	TrafficCarQuatZ[31] = 0.8742
	TrafficCarQuatW[31] = 0.4855
	TrafficCarRecording[31] = 62
	TrafficCarStartime[31] = 30962.0000
	TrafficCarModel[31] = intruder

	TrafficCarPos[32] = <<-1441.9612, -436.8392, 35.3180>>
	TrafficCarQuatX[32] = 0.0051
	TrafficCarQuatY[32] = -0.0027
	TrafficCarQuatZ[32] = -0.4849
	TrafficCarQuatW[32] = 0.8746
	TrafficCarRecording[32] = 60
	TrafficCarStartime[32] = 31000.0000
	TrafficCarModel[32] = manana

	TrafficCarPos[33] = <<-1429.4720, -475.7137, 33.7061>>
	TrafficCarQuatX[33] = 0.0134
	TrafficCarQuatY[33] = 0.0239
	TrafficCarQuatZ[33] = 0.2968
	TrafficCarQuatW[33] = 0.9545
	TrafficCarRecording[33] = 56
	TrafficCarStartime[33] = 31050.0000
	TrafficCarModel[33] = intruder

	//2047053 - There's a chance this car collides with another one on creation.
	/*TrafficCarPos[34] = <<-1288.1838, -331.0012, 36.3186>>
	TrafficCarQuatX[34] = -0.0073
	TrafficCarQuatY[34] = -0.0118
	TrafficCarQuatZ[34] = 0.8727
	TrafficCarQuatW[34] = 0.4880
	TrafficCarRecording[34] = 122
	TrafficCarStartime[34] = 32000.0000
	TrafficCarModel[34] = intruder*/

	TrafficCarPos[35] = <<-1280.4327, -384.1887, 36.0295>>
	TrafficCarQuatX[35] = 0.0126
	TrafficCarQuatY[35] = 0.0032
	TrafficCarQuatZ[35] = 0.2431
	TrafficCarQuatW[35] = 0.9699
	TrafficCarRecording[35] = 257
	TrafficCarStartime[35] = 33000.0000
	TrafficCarModel[35] = manana

	TrafficCarPos[36] = <<-1343.5938, -355.1015, 36.2497>>
	TrafficCarQuatX[36] = -0.0052
	TrafficCarQuatY[36] = -0.0032
	TrafficCarQuatZ[36] = 0.8108
	TrafficCarQuatW[36] = 0.5852
	TrafficCarRecording[36] = 69
	TrafficCarStartime[36] = 33994.0000
	TrafficCarModel[36] = manana

	TrafficCarPos[37] = <<-1216.5325, -307.8270, 37.3348>>
	TrafficCarQuatX[37] = -0.0036
	TrafficCarQuatY[37] = 0.0027
	TrafficCarQuatZ[37] = -0.5369
	TrafficCarQuatW[37] = 0.8436
	TrafficCarRecording[37] = 65
	TrafficCarStartime[37] = 35000.0000
	TrafficCarModel[37] = manana

	TrafficCarPos[38] = <<-1105.8495, -222.9649, 37.3392>>
	TrafficCarQuatX[38] = -0.0060
	TrafficCarQuatY[38] = 0.0021
	TrafficCarQuatZ[38] = 0.8546
	TrafficCarQuatW[38] = 0.5192
	TrafficCarRecording[38] = 258
	TrafficCarStartime[38] = 36000.0000
	TrafficCarModel[38] = manana

	TrafficCarPos[39] = <<-1080.2947, -214.4883, 37.4765>>
	TrafficCarQuatX[39] = -0.0017
	TrafficCarQuatY[39] = -0.0037
	TrafficCarQuatZ[39] = 0.8656
	TrafficCarQuatW[39] = 0.5006
	TrafficCarRecording[39] = 259
	TrafficCarStartime[39] = 36100.0000
	TrafficCarModel[39] = intruder

	TrafficCarPos[40] = <<-1193.5811, -297.1006, 37.7095>>
	TrafficCarQuatX[40] = -0.0010
	TrafficCarQuatY[40] = 0.0003
	TrafficCarQuatZ[40] = -0.5278
	TrafficCarQuatW[40] = 0.8494
	TrafficCarRecording[40] = 260
	TrafficCarStartime[40] = 36200.0000
	TrafficCarModel[40] = Burrito3

	TrafficCarPos[41] = <<-1020.4266, -177.1401, 37.4026>>
	TrafficCarQuatX[41] = -0.0125
	TrafficCarQuatY[41] = 0.0031
	TrafficCarQuatZ[41] = 0.8613
	TrafficCarQuatW[41] = 0.5079
	TrafficCarRecording[41] = 261
	TrafficCarStartime[41] = 36300.0000
	TrafficCarModel[41] = intruder

	TrafficCarPos[42] = <<-1267.4415, -336.0135, 36.4325>>
	TrafficCarQuatX[42] = -0.0032
	TrafficCarQuatY[42] = -0.0065
	TrafficCarQuatZ[42] = -0.5100
	TrafficCarQuatW[42] = 0.8602
	TrafficCarRecording[42] = 70
	TrafficCarStartime[42] = 37000.0000
	TrafficCarModel[42] = intruder

	/*TrafficCarPos[43] = <<-1325.1547, -349.2796, 36.0825>>
	TrafficCarQuatX[43] = 0.0059
	TrafficCarQuatY[43] = 0.0067
	TrafficCarQuatZ[43] = 0.7667
	TrafficCarQuatW[43] = 0.6419
	TrafficCarRecording[43] = 71
	TrafficCarStartime[43] = 37056.0000
	TrafficCarModel[43] = intruder*/

	TrafficCarPos[44] = <<-1272.1207, -343.9194, 36.2108>>
	TrafficCarQuatX[44] = 0.0064
	TrafficCarQuatY[44] = -0.0040
	TrafficCarQuatZ[44] = -0.5243
	TrafficCarQuatW[44] = 0.8515
	TrafficCarRecording[44] = 75
	TrafficCarStartime[44] = 37140.0000
	TrafficCarModel[44] = intruder

	TrafficCarPos[45] = <<-1031.8419, -289.7789, 37.6673>>
	TrafficCarQuatX[45] = -0.0070
	TrafficCarQuatY[45] = -0.0007
	TrafficCarQuatZ[45] = 0.2092
	TrafficCarQuatW[45] = 0.9778
	TrafficCarRecording[45] = 85
	TrafficCarStartime[45] = 37200.0000
	TrafficCarModel[45] = Burrito3

	/*TrafficCarPos[46] = <<-1233.9241, -302.6840, 37.1774>>
	TrafficCarQuatX[46] = -0.0099
	TrafficCarQuatY[46] = -0.0010
	TrafficCarQuatZ[46] = 0.9191
	TrafficCarQuatW[46] = 0.3939
	TrafficCarRecording[46] = 76
	TrafficCarStartime[46] = 37256.0000
	TrafficCarModel[46] = manana*/

	/*TrafficCarPos[47] = <<-1218.5978, -293.2798, 36.9743>>
	TrafficCarQuatX[47] = -0.0009
	TrafficCarQuatY[47] = -0.0013
	TrafficCarQuatZ[47] = 0.8495
	TrafficCarQuatW[47] = 0.5276
	TrafficCarRecording[47] = 78
	TrafficCarStartime[47] = 37422.0000
	TrafficCarModel[47] = intruder*/

	TrafficCarPos[48] = <<-1284.5294, -330.2454, 36.3752>>
	TrafficCarQuatX[48] = -0.0032
	TrafficCarQuatY[48] = -0.0043
	TrafficCarQuatZ[48] = 0.8520
	TrafficCarQuatW[48] = 0.5235
	TrafficCarRecording[48] = 201
	TrafficCarStartime[48] = 37950.0000
	TrafficCarModel[48] = intruder

	TrafficCarPos[49] = <<-961.5755, -200.8471, 37.1363>>
	TrafficCarQuatX[49] = -0.0379
	TrafficCarQuatY[49] = 0.0200
	TrafficCarQuatZ[49] = 0.8755
	TrafficCarQuatW[49] = 0.4814
	TrafficCarRecording[49] = 262
	TrafficCarStartime[49] = 39000.0000
	TrafficCarModel[49] = intruder

	TrafficCarPos[50] = <<-1030.5999, -250.9640, 37.2496>>
	TrafficCarQuatX[50] = 0.0131
	TrafficCarQuatY[50] = 0.0248
	TrafficCarQuatZ[50] = -0.5153
	TrafficCarQuatW[50] = 0.8565
	TrafficCarRecording[50] = 263
	TrafficCarStartime[50] = 39050.0000
	TrafficCarModel[50] = manana

	TrafficCarPos[51] = <<-1212.0898, -293.2291, 37.3428>>
	TrafficCarQuatX[51] = 0.0037
	TrafficCarQuatY[51] = 0.0035
	TrafficCarQuatZ[51] = 0.8554
	TrafficCarQuatW[51] = 0.5179
	TrafficCarRecording[51] = 202
	TrafficCarStartime[51] = 39600.0000
	TrafficCarModel[51] = manana

	TrafficCarPos[52] = <<-1045.8507, -276.7056, 37.2715>>
	TrafficCarQuatX[52] = -0.0005
	TrafficCarQuatY[52] = 0.0000
	TrafficCarQuatZ[52] = 0.9753
	TrafficCarQuatW[52] = -0.2209
	TrafficCarRecording[52] = 73
	TrafficCarStartime[52] = 40000.0000
	TrafficCarModel[52] = manana

	TrafficCarPos[53] = <<-1018.1578, -236.3702, 37.3773>>
	TrafficCarQuatX[53] = 0.0028
	TrafficCarQuatY[53] = -0.0089
	TrafficCarQuatZ[53] = 0.8524
	TrafficCarQuatW[53] = 0.5227
	TrafficCarRecording[53] = 94
	TrafficCarStartime[53] = 40200.0000
	TrafficCarModel[53] = intruder

	TrafficCarPos[54] = <<-992.8951, -381.1576, 37.3940>>
	TrafficCarQuatX[54] = 0.0005
	TrafficCarQuatY[54] = -0.0092
	TrafficCarQuatZ[54] = 0.9712
	TrafficCarQuatW[54] = -0.2382
	TrafficCarRecording[54] = 170
	TrafficCarStartime[54] = 42500.0000
	TrafficCarModel[54] = manana

	TrafficCarPos[55] = <<-943.2020, -420.0762, 37.3229>>
	TrafficCarQuatX[55] = -0.0044
	TrafficCarQuatY[55] = 0.0127
	TrafficCarQuatZ[55] = 0.8549
	TrafficCarQuatW[55] = 0.5186
	TrafficCarRecording[55] = 264
	TrafficCarStartime[55] = 43000.0000
	TrafficCarModel[55] = intruder

	TrafficCarPos[56] = <<-973.2485, -426.7131, 37.4479>>
	TrafficCarQuatX[56] = -0.0159
	TrafficCarQuatY[56] = -0.0064
	TrafficCarQuatZ[56] = 0.9638
	TrafficCarQuatW[56] = -0.2662
	TrafficCarRecording[56] = 265
	TrafficCarStartime[56] = 43200.0000
	TrafficCarModel[56] = intruder

	TrafficCarPos[57] = <<-1003.6150, -333.3123, 36.9974>>
	TrafficCarQuatX[57] = 0.0000
	TrafficCarQuatY[57] = 0.0000
	TrafficCarQuatZ[57] = 0.2417
	TrafficCarQuatW[57] = 0.9703
	TrafficCarRecording[57] = 86
	TrafficCarStartime[57] = 43753.0000
	TrafficCarModel[57] = intruder

	TrafficCarPos[58] = <<-910.7939, -480.5219, 36.1598>>
	TrafficCarQuatX[58] = -0.0004
	TrafficCarQuatY[58] = -0.0009
	TrafficCarQuatZ[58] = 0.8607
	TrafficCarQuatW[58] = 0.5090
	TrafficCarRecording[58] = 266
	TrafficCarStartime[58] = 44000.0000
	TrafficCarModel[58] = manana

	TrafficCarPos[59] = <<-903.4105, -534.6552, 34.3315>>
	TrafficCarQuatX[59] = 0.0250
	TrafficCarQuatY[59] = 0.0193
	TrafficCarQuatZ[59] = 0.2328
	TrafficCarQuatW[59] = 0.9720
	TrafficCarRecording[59] = 267
	TrafficCarStartime[59] = 44200.0000
	TrafficCarModel[59] = manana

	TrafficCarPos[60] = <<-1001.2458, -349.7690, 37.5263>>
	TrafficCarQuatX[60] = -0.0195
	TrafficCarQuatY[60] = -0.0038
	TrafficCarQuatZ[60] = 0.2914
	TrafficCarQuatW[60] = 0.9564
	TrafficCarRecording[60] = 89
	TrafficCarStartime[60] = 45271.0000
	TrafficCarModel[60] = intruder

	TrafficCarPos[61] = <<-944.2744, -457.4497, 37.0746>>
	TrafficCarQuatX[61] = 0.0051
	TrafficCarQuatY[61] = -0.0062
	TrafficCarQuatZ[61] = 0.2376
	TrafficCarQuatW[61] = 0.9713
	TrafficCarRecording[61] = 90
	TrafficCarStartime[61] = 46500.0000
	TrafficCarModel[61] = intruder

	TrafficCarPos[62] = <<-1066.3922, -457.1241, 36.0125>>
	TrafficCarQuatX[62] = -0.0210
	TrafficCarQuatY[62] = -0.0058
	TrafficCarQuatZ[62] = 0.8835
	TrafficCarQuatW[62] = -0.4680
	TrafficCarRecording[62] = 108
	TrafficCarStartime[62] = 47600.0000
	TrafficCarModel[62] = manana
	SET_TRAFFIC_CAR_SWITCH_TO_AI_EARLY(62, FALSE)

	TrafficCarPos[63] = <<-1372.2000, -703.8073, 23.8866>>
	TrafficCarQuatX[63] = 0.0254
	TrafficCarQuatY[63] = 0.0194
	TrafficCarQuatZ[63] = -0.4350
	TrafficCarQuatW[63] = 0.8999
	TrafficCarRecording[63] = 95
	TrafficCarStartime[63] = 52500.0000
	TrafficCarModel[63] = manana

	TrafficCarPos[64] = <<-1359.6655, -694.3884, 24.9078>>
	TrafficCarQuatX[64] = 0.0261
	TrafficCarQuatY[64] = 0.0166
	TrafficCarQuatZ[64] = -0.4655
	TrafficCarQuatW[64] = 0.8845
	TrafficCarRecording[64] = 13
	TrafficCarStartime[64] = 54500.0000
	TrafficCarModel[64] = pcj

	TrafficCarPos[65] = <<-1251.4202, -580.7559, 27.7332>>
	TrafficCarQuatX[65] = 0.0033
	TrafficCarQuatY[65] = -0.0427
	TrafficCarQuatZ[65] = 0.9369
	TrafficCarQuatW[65] = -0.3470
	TrafficCarRecording[65] = 93
	TrafficCarStartime[65] = 55000.0000
	TrafficCarModel[65] = intruder

	TrafficCarPos[66] = <<-1186.8662, -629.9144, 23.4611>>
	TrafficCarQuatX[66] = 0.0371
	TrafficCarQuatY[66] = 0.0124
	TrafficCarQuatZ[66] = 0.3335
	TrafficCarQuatW[66] = 0.9420
	TrafficCarRecording[66] = 268
	TrafficCarStartime[66] = 55050.0000
	TrafficCarModel[66] = manana

	TrafficCarPos[67] = <<-1104.0922, -750.3582, 18.9453>>
	TrafficCarQuatX[67] = -0.0083
	TrafficCarQuatY[67] = -0.0215
	TrafficCarQuatZ[67] = 0.9362
	TrafficCarQuatW[67] = -0.3506
	TrafficCarRecording[67] = 115
	TrafficCarStartime[67] = 66000.0000
	TrafficCarModel[67] = manana

	TrafficCarPos[68] = <<-1100.6582, -748.1008, 19.0332>>
	TrafficCarQuatX[68] = 0.0156
	TrafficCarQuatY[68] = -0.0216
	TrafficCarQuatZ[68] = 0.9336
	TrafficCarQuatW[68] = -0.3572
	TrafficCarRecording[68] = 116
	TrafficCarStartime[68] = 66050.0000
	TrafficCarModel[68] = pcj

	TrafficCarPos[69] = <<-1109.1354, -719.9675, 19.9357>>
	TrafficCarQuatX[69] = 0.0113
	TrafficCarQuatY[69] = -0.0008
	TrafficCarQuatZ[69] = 0.3447
	TrafficCarQuatW[69] = 0.9386
	TrafficCarRecording[69] = 154
	TrafficCarStartime[69] = 66100.0000
	TrafficCarModel[69] = intruder

	TrafficCarPos[70] = <<-1161.2339, -685.1978, 21.6411>>
	TrafficCarQuatX[70] = -0.0033
	TrafficCarQuatY[70] = -0.0261
	TrafficCarQuatZ[70] = 0.9396
	TrafficCarQuatW[70] = -0.3413
	TrafficCarRecording[70] = 156
	TrafficCarStartime[70] = 66500.0000
	TrafficCarModel[70] = intruder

	TrafficCarPos[71] = <<-1094.9194, -761.5510, 18.9358>>
	TrafficCarQuatX[71] = -0.0024
	TrafficCarQuatY[71] = -0.0034
	TrafficCarQuatZ[71] = 0.9406
	TrafficCarQuatW[71] = -0.3395
	TrafficCarRecording[71] = 272
	TrafficCarStartime[71] = 69000.0000
	TrafficCarModel[71] = intruder

	TrafficCarPos[72] = <<-1085.7266, -737.9604, 18.5164>>
	TrafficCarQuatX[72] = 0.0186
	TrafficCarQuatY[72] = 0.0051
	TrafficCarQuatZ[72] = 0.2729
	TrafficCarQuatW[72] = 0.9619
	TrafficCarRecording[72] = 101
	TrafficCarStartime[72] = 69902.0000
	TrafficCarModel[72] = intruder

	TrafficCarPos[73] = <<-1191.9774, -862.7239, 13.4924>>
	TrafficCarQuatX[73] = 0.0167
	TrafficCarQuatY[73] = -0.0078
	TrafficCarQuatZ[73] = -0.4888
	TrafficCarQuatW[73] = 0.8722
	TrafficCarRecording[73] = 109
	TrafficCarStartime[73] = 70000.0000
	TrafficCarModel[73] = pcj

	TrafficCarPos[74] = <<-1017.1104, -801.2603, 16.2402>>
	TrafficCarQuatX[74] = 0.0237
	TrafficCarQuatY[74] = -0.0005
	TrafficCarQuatZ[74] = 0.5066
	TrafficCarQuatW[74] = 0.8619
	TrafficCarRecording[74] = 269
	TrafficCarStartime[74] = 70050.0000
	TrafficCarModel[74] = manana

	TrafficCarPos[75] = <<-1042.9668, -723.3073, 19.0664>>
	TrafficCarQuatX[75] = -0.0227
	TrafficCarQuatY[75] = -0.0131
	TrafficCarQuatZ[75] = 0.9117
	TrafficCarQuatW[75] = 0.4101
	TrafficCarRecording[75] = 270
	TrafficCarStartime[75] = 70100.0000
	TrafficCarModel[75] = manana

	TrafficCarPos[76] = <<-986.2640, -811.2874, 15.3855>>
	TrafficCarQuatX[76] = 0.0037
	TrafficCarQuatY[76] = 0.0064
	TrafficCarQuatZ[76] = 0.4970
	TrafficCarQuatW[76] = 0.8677
	TrafficCarRecording[76] = 271
	TrafficCarStartime[76] = 70150.0000
	TrafficCarModel[76] = manana

	/*TrafficCarPos[77] = <<-1068.7451, -764.3805, 18.8507>>
	TrafficCarQuatX[77] = 0.0089
	TrafficCarQuatY[77] = -0.0077
	TrafficCarQuatZ[77] = 0.2601
	TrafficCarQuatW[77] = 0.9655
	TrafficCarRecording[77] = 102
	TrafficCarStartime[77] = 70200.0000
	TrafficCarModel[77] = intruder*/

	TrafficCarPos[78] = <<-1099.2067, -723.0576, 19.5426>>
	TrafficCarQuatX[78] = 0.0172
	TrafficCarQuatY[78] = 0.0138
	TrafficCarQuatZ[78] = 0.2758
	TrafficCarQuatW[78] = 0.9610
	TrafficCarRecording[78] = 96
	TrafficCarStartime[78] = 70242.0000
	TrafficCarModel[78] = intruder

	TrafficCarPos[79] = <<-1128.7385, -796.8690, 16.4235>>
	TrafficCarQuatX[79] = -0.0331
	TrafficCarQuatY[79] = -0.0425
	TrafficCarQuatZ[79] = 0.9062
	TrafficCarQuatW[79] = 0.4193
	TrafficCarRecording[79] = 104
	TrafficCarStartime[79] = 74000.0000
	TrafficCarModel[79] = manana

	TrafficCarPos[80] = <<-1155.6365, -834.2766, 13.8095>>
	TrafficCarQuatX[80] = 0.0081
	TrafficCarQuatY[80] = -0.0058
	TrafficCarQuatZ[80] = -0.4207
	TrafficCarQuatW[80] = 0.9071
	TrafficCarRecording[80] = 105
	TrafficCarStartime[80] = 74050.0000
	TrafficCarModel[80] = manana

	TrafficCarPos[81] = <<-1175.9929, -835.8189, 13.8260>>
	TrafficCarQuatX[81] = -0.0051
	TrafficCarQuatY[81] = 0.0066
	TrafficCarQuatZ[81] = 0.9051
	TrafficCarQuatW[81] = 0.4251
	TrafficCarRecording[81] = 273
	TrafficCarStartime[81] = 75000.0000
	TrafficCarModel[81] = intruder

	TrafficCarPos[82] = <<-1110.6803, -919.0346, 2.2867>>
	TrafficCarQuatX[82] = -0.0002
	TrafficCarQuatY[82] = -0.0064
	TrafficCarQuatZ[82] = 0.8645
	TrafficCarQuatW[82] = 0.5025
	TrafficCarRecording[82] = 203
	TrafficCarStartime[82] = 82357.0000
	TrafficCarModel[82] = manana

	TrafficCarPos[83] = <<-1014.4996, -1099.9009, 1.3071>>
	TrafficCarQuatX[83] = -0.0089
	TrafficCarQuatY[83] = -0.0024
	TrafficCarQuatZ[83] = 0.2603
	TrafficCarQuatW[83] = 0.9655
	TrafficCarRecording[83] = 113
	TrafficCarStartime[83] = 84818.0000
	TrafficCarModel[83] = intruder

	TrafficCarPos[84] = <<-989.3825, -1154.4706, 2.0856>>
	TrafficCarQuatX[84] = -0.0107
	TrafficCarQuatY[84] = 0.0386
	TrafficCarQuatZ[84] = 0.9645
	TrafficCarQuatW[84] = -0.2611
	TrafficCarRecording[84] = 119
	TrafficCarStartime[84] = 86732.0000
	TrafficCarModel[84] = intruder

	TrafficCarPos[85] = <<-925.1222, -1200.2721, 4.4888>>
	TrafficCarQuatX[85] = 0.0002
	TrafficCarQuatY[85] = 0.0000
	TrafficCarQuatZ[85] = 0.8763
	TrafficCarQuatW[85] = 0.4818
	TrafficCarRecording[85] = 134
	TrafficCarStartime[85] = 92700.0000
	TrafficCarModel[85] = manana

	TrafficCarPos[86] = <<-983.4543, -1252.4944, 5.2503>>
	TrafficCarQuatX[86] = -0.0040
	TrafficCarQuatY[86] = 0.0161
	TrafficCarQuatZ[86] = -0.5021
	TrafficCarQuatW[86] = 0.8646
	TrafficCarRecording[86] = 274
	TrafficCarStartime[86] = 93000.0000
	TrafficCarModel[86] = intruder

	TrafficCarPos[87] = <<-849.5126, -1152.5116, 6.0559>>
	TrafficCarQuatX[87] = -0.0229
	TrafficCarQuatY[87] = -0.0190
	TrafficCarQuatZ[87] = 0.7997
	TrafficCarQuatW[87] = 0.5997
	TrafficCarRecording[87] = 130
	TrafficCarStartime[87] = 95518.0000
	TrafficCarModel[87] = manana

	TrafficCarPos[88] = <<-848.8896, -1205.2084, 5.6769>>
	TrafficCarQuatX[88] = -0.0236
	TrafficCarQuatY[88] = -0.0291
	TrafficCarQuatZ[88] = 0.2576
	TrafficCarQuatW[88] = 0.9655
	TrafficCarRecording[88] = 126
	TrafficCarStartime[88] = 97000.0000
	TrafficCarModel[88] = intruder

	TrafficCarPos[89] = <<-873.4331, -1135.9908, 6.2755>>
	TrafficCarQuatX[89] = 0.0124
	TrafficCarQuatY[89] = -0.0485
	TrafficCarQuatZ[89] = 0.9657
	TrafficCarQuatW[89] = -0.2547
	TrafficCarRecording[89] = 125
	TrafficCarStartime[89] = 97050.0000
	TrafficCarModel[89] = manana

	/*TrafficCarPos[90] = <<-801.6751, -1063.4163, 11.5952>>
	TrafficCarQuatX[90] = 0.0146
	TrafficCarQuatY[90] = -0.0269
	TrafficCarQuatZ[90] = 0.9700
	TrafficCarQuatW[90] = -0.2412
	TrafficCarRecording[90] = 18
	TrafficCarStartime[90] = 99500.0000
	TrafficCarModel[90] = intruder*/

	TrafficCarPos[91] = <<-667.5715, -873.9283, 41.4232>>
	TrafficCarQuatX[91] = 0.0242
	TrafficCarQuatY[91] = -0.0155
	TrafficCarQuatZ[91] = -0.6235
	TrafficCarQuatW[91] = 0.7813
	TrafficCarRecording[91] = 25
	TrafficCarStartime[91] = 104000.0000
	TrafficCarModel[91] = maverick

	TrafficCarPos[92] = <<-668.2998, -1048.5413, 16.3267>>
	TrafficCarQuatX[92] = 0.0058
	TrafficCarQuatY[92] = -0.0334
	TrafficCarQuatZ[92] = 0.8869
	TrafficCarQuatW[92] = 0.4606
	TrafficCarRecording[92] = 26
	TrafficCarStartime[92] = 104500.0000
	TrafficCarModel[92] = intruder

	TrafficCarPos[93] = <<-646.0926, -913.8471, 23.5267>>
	TrafficCarQuatX[93] = -0.0178
	TrafficCarQuatY[93] = -0.0373
	TrafficCarQuatZ[93] = 0.9990
	TrafficCarQuatW[93] = 0.0153
	TrafficCarRecording[93] = 136
	TrafficCarStartime[93] = 106500.0000
	TrafficCarModel[93] = intruder

	TrafficCarPos[94] = <<-636.0123, -980.1482, 20.8531>>
	TrafficCarQuatX[94] = 0.0095
	TrafficCarQuatY[94] = -0.0064
	TrafficCarQuatZ[94] = -0.0199
	TrafficCarQuatW[94] = 0.9997
	TrafficCarRecording[94] = 276
	TrafficCarStartime[94] = 107000.0000
	TrafficCarModel[94] = manana

	TrafficCarPos[95] = <<-679.2947, -959.8151, 20.1611>>
	TrafficCarQuatX[95] = 0.0390
	TrafficCarQuatY[95] = 0.0031
	TrafficCarQuatZ[95] = -0.7025
	TrafficCarQuatW[95] = 0.7106
	TrafficCarRecording[95] = 277
	TrafficCarStartime[95] = 108000.0000
	TrafficCarModel[95] = manana

	TrafficCarPos[96] = <<-645.9667, -872.5993, 24.0909>>
	TrafficCarQuatX[96] = -0.0308
	TrafficCarQuatY[96] = 0.0030
	TrafficCarQuatZ[96] = 0.9995
	TrafficCarQuatW[96] = -0.0065
	TrafficCarRecording[96] = 278
	TrafficCarStartime[96] = 108050.0000
	TrafficCarModel[96] = manana

	TrafficCarPos[97] = <<-532.2497, -1015.1015, 22.4589>>
	TrafficCarQuatX[97] = 0.0006
	TrafficCarQuatY[97] = 0.0229
	TrafficCarQuatZ[97] = 0.0582
	TrafficCarQuatW[97] = 0.9980
	TrafficCarRecording[97] = 279
	TrafficCarStartime[97] = 112000.0000
	TrafficCarModel[97] = manana

	TrafficCarPos[98] = <<-492.1226, -871.4891, 29.2373>>
	TrafficCarQuatX[98] = 0.0305
	TrafficCarQuatY[98] = -0.0002
	TrafficCarQuatZ[98] = -0.0364
	TrafficCarQuatW[98] = 0.9989
	TrafficCarRecording[98] = 152
	TrafficCarStartime[98] = 114001.0000
	TrafficCarModel[98] = manana

	TrafficCarPos[99] = <<-477.2830, -845.0166, 29.9852>>
	TrafficCarQuatX[99] = -0.0032
	TrafficCarQuatY[99] = -0.0105
	TrafficCarQuatZ[99] = 0.7135
	TrafficCarQuatW[99] = -0.7006
	TrafficCarRecording[99] = 148
	TrafficCarStartime[99] = 116000.0000
	TrafficCarModel[99] = intruder

	TrafficCarPos[100] = <<-517.6269, -758.5123, 31.4468>>
	TrafficCarQuatX[100] = -0.0165
	TrafficCarQuatY[100] = -0.0260
	TrafficCarQuatZ[100] = 0.9731
	TrafficCarQuatW[100] = -0.2283
	TrafficCarRecording[100] = 280
	TrafficCarStartime[100] = 117000.0000
	TrafficCarModel[100] = manana

	TrafficCarPos[101] = <<-496.5247, -816.1506, 30.0963>>
	TrafficCarQuatX[101] = -0.0035
	TrafficCarQuatY[101] = -0.0011
	TrafficCarQuatZ[101] = -0.0339
	TrafficCarQuatW[101] = 0.9994
	TrafficCarRecording[101] = 281
	TrafficCarStartime[101] = 117050.0000
	TrafficCarModel[101] = intruder

	TrafficCarPos[102] = <<-502.5592, -804.7895, 30.2477>>
	TrafficCarQuatX[102] = 0.0037
	TrafficCarQuatY[102] = -0.0047
	TrafficCarQuatZ[102] = 0.9996
	TrafficCarQuatW[102] = -0.0291
	TrafficCarRecording[102] = 157
	TrafficCarStartime[102] = 118845.0000
	TrafficCarModel[102] = intruder

	TrafficCarPos[103] = <<-367.6438, -834.8538, 31.2414>>
	TrafficCarQuatX[103] = -0.0128
	TrafficCarQuatY[103] = 0.0035
	TrafficCarQuatZ[103] = 0.6848
	TrafficCarQuatW[103] = 0.7286
	TrafficCarRecording[103] = 158
	TrafficCarStartime[103] = 119000.0000
	TrafficCarModel[103] = pcj

	TrafficCarPos[104] = <<-286.6659, -846.1884, 31.3065>>
	TrafficCarQuatX[104] = -0.0127
	TrafficCarQuatY[104] = -0.0018
	TrafficCarQuatZ[104] = 0.6461
	TrafficCarQuatW[104] = 0.7631
	TrafficCarRecording[104] = 282
	TrafficCarStartime[104] = 120000.0000
	TrafficCarModel[104] = intruder

	TrafficCarPos[105] = <<-233.8871, -869.6703, 29.9880>>
	TrafficCarQuatX[105] = 0.0115
	TrafficCarQuatY[105] = 0.0067
	TrafficCarQuatZ[105] = 0.5974
	TrafficCarQuatW[105] = 0.8018
	TrafficCarRecording[105] = 283
	TrafficCarStartime[105] = 120050.0000
	TrafficCarModel[105] = intruder

	TrafficCarPos[106] = <<-507.5561, -826.8052, 29.6210>>
	TrafficCarQuatX[106] = 0.0000
	TrafficCarQuatY[106] = -0.0021
	TrafficCarQuatZ[106] = 1.0000
	TrafficCarQuatW[106] = 0.0026
	TrafficCarRecording[106] = 155
	TrafficCarStartime[106] = 120449.0000
	TrafficCarModel[106] = intruder

	TrafficCarPos[107] = <<-387.6420, -844.1628, 31.0928>>
	TrafficCarQuatX[107] = -0.0048
	TrafficCarQuatY[107] = 0.0048
	TrafficCarQuatZ[107] = 0.7350
	TrafficCarQuatW[107] = -0.6781
	TrafficCarRecording[107] = 205
	TrafficCarStartime[107] = 123701.0000
	TrafficCarModel[107] = manana

	TrafficCarPos[108] = <<-332.0064, -860.2901, 31.1178>>
	TrafficCarQuatX[108] = -0.0038
	TrafficCarQuatY[108] = 0.0044
	TrafficCarQuatZ[108] = 0.7627
	TrafficCarQuatW[108] = -0.6467
	TrafficCarRecording[108] = 206
	TrafficCarStartime[108] = 124493.0000
	TrafficCarModel[108] = manana

	TrafficCarPos[109] = <<-316.7719, -857.1534, 31.1568>>
	TrafficCarQuatX[109] = -0.0030
	TrafficCarQuatY[109] = 0.0034
	TrafficCarQuatZ[109] = 0.7628
	TrafficCarQuatW[109] = -0.6466
	TrafficCarRecording[109] = 207
	TrafficCarStartime[109] = 124955.0000
	TrafficCarModel[109] = manana

	TrafficCarPos[110] = <<-369.8260, -661.4047, 31.1689>>
	TrafficCarQuatX[110] = 0.0061
	TrafficCarQuatY[110] = -0.0061
	TrafficCarQuatZ[110] = -0.7071
	TrafficCarQuatW[110] = 0.7071
	TrafficCarRecording[110] = 166
	TrafficCarStartime[110] = 130000.0000
	TrafficCarModel[110] = intruder

	TrafficCarPos[111] = <<-248.7058, -603.2553, 33.5757>>
	TrafficCarQuatX[111] = -0.0274
	TrafficCarQuatY[111] = -0.0092
	TrafficCarQuatZ[111] = 0.9839
	TrafficCarQuatW[111] = 0.1762
	TrafficCarRecording[111] = 285
	TrafficCarStartime[111] = 130050.0000
	TrafficCarModel[111] = intruder

	TrafficCarPos[112] = <<-247.1978, -612.5249, 33.4505>>
	TrafficCarQuatX[112] = -0.0124
	TrafficCarQuatY[112] = -0.0091
	TrafficCarQuatZ[112] = 0.9849
	TrafficCarQuatW[112] = 0.1726
	TrafficCarRecording[112] = 286
	TrafficCarStartime[112] = 130100.0000
	TrafficCarModel[112] = manana

	TrafficCarPos[113] = <<-243.4324, -620.5720, 33.7070>>
	TrafficCarQuatX[113] = -0.0019
	TrafficCarQuatY[113] = -0.0092
	TrafficCarQuatZ[113] = 0.9875
	TrafficCarQuatW[113] = 0.1574
	TrafficCarRecording[113] = 287
	TrafficCarStartime[113] = 130150.0000
	TrafficCarModel[113] = Burrito3

	TrafficCarPos[114] = <<-277.3765, -654.3015, 32.7939>>
	TrafficCarQuatX[114] = -0.0041
	TrafficCarQuatY[114] = 0.0062
	TrafficCarQuatZ[114] = 0.6547
	TrafficCarQuatW[114] = 0.7558
	TrafficCarRecording[114] = 208
	TrafficCarStartime[114] = 133831.0000
	TrafficCarModel[114] = intruder

	TrafficCarPos[115] = <<-226.6089, -599.4540, 33.7347>>
	TrafficCarQuatX[115] = 0.0071
	TrafficCarQuatY[115] = -0.0012
	TrafficCarQuatZ[115] = -0.1741
	TrafficCarQuatW[115] = 0.9847
	TrafficCarRecording[115] = 164
	TrafficCarStartime[115] = 137506.0000
	TrafficCarModel[115] = intruder




	//Set-piece cars
	//Used to be traffic 0
	SetPieceCarPos[0] = <<-1829.1702, -579.5376, 10.9467>>
	SetPieceCarQuatX[0] = -0.0059
	SetPieceCarQuatY[0] = -0.0026
	SetPieceCarQuatZ[0] = 0.4222
	SetPieceCarQuatW[0] = 0.9065
	SetPieceCarRecording[0] = 38
	SetPieceCarStartime[0] = 0.0000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = manana
	
	SetPieceCarPos[1] = <<-1940.0116, -473.7903, 11.9182>>
	SetPieceCarQuatX[1] = -0.0040
	SetPieceCarQuatY[1] = 0.0114
	SetPieceCarQuatZ[1] = 0.4266
	SetPieceCarQuatW[1] = 0.9043
	SetPieceCarRecording[1] = 516
	SetPieceCarStartime[1] = 1500.0000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = flatbed
	
	SetPieceCarPos[2] = <<-1893.9094, -519.8182, 11.2157>>
	SetPieceCarQuatX[2] = 0.0010
	SetPieceCarQuatY[2] = 0.0005
	SetPieceCarQuatZ[2] = 0.4314
	SetPieceCarQuatW[2] = 0.9022
	SetPieceCarRecording[2] = 52
	SetPieceCarStartime[2] = 2000.0000
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = youga
	
	SetPieceCarPos[3] = <<-1875.3740, -542.2328, 11.2263>>
	SetPieceCarQuatX[3] = -0.0070
	SetPieceCarQuatY[3] = -0.0033
	SetPieceCarQuatZ[3] = 0.4363
	SetPieceCarQuatW[3] = 0.8998
	SetPieceCarRecording[3] = 45
	SetPieceCarStartime[3] = 3000.0000
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = manana
	
	SetPieceCarPos[4] = <<-1732.9817, -524.2459, 37.3373>>
	SetPieceCarQuatX[4] = 0.0203
	SetPieceCarQuatY[4] = -0.0250
	SetPieceCarQuatZ[4] = 0.9005
	SetPieceCarQuatW[4] = -0.4336
	SetPieceCarRecording[4] = 501
	SetPieceCarStartime[4] = 19000.0000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = intruder
	
	SetPieceCarPos[5] = <<-1568.3964, -522.3314, 34.8755>>
	SetPieceCarQuatX[5] = -0.0010
	SetPieceCarQuatY[5] = 0.0003
	SetPieceCarQuatZ[5] = -0.4567
	SetPieceCarQuatW[5] = 0.8896
	SetPieceCarRecording[5] = 54
	SetPieceCarStartime[5] = 27947.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = intruder
	
	SetPieceCarPos[6] = <<-1405.9943, -412.4767, 36.0144>>
	SetPieceCarQuatX[6] = -0.0127
	SetPieceCarQuatY[6] = 0.0028
	SetPieceCarQuatZ[6] = -0.4831
	SetPieceCarQuatW[6] = 0.8754
	SetPieceCarRecording[6] = 509
	SetPieceCarStartime[6] = 28000.0000
	SetPieceCarRecordingSpeed[6] = 1.0000
	SetPieceCarModel[6] = manana
	
	SetPieceCarPos[7] = <<-1518.7917, -386.8024, 41.1265>>
	SetPieceCarQuatX[7] = 0.0254
	SetPieceCarQuatY[7] = -0.0525
	SetPieceCarQuatZ[7] = 0.9088
	SetPieceCarQuatW[7] = -0.4131
	SetPieceCarRecording[7] = 23
	SetPieceCarStartime[7] = 29000.0000
	SetPieceCarRecordingSpeed[7] = 1.0000
	SetPieceCarModel[7] = intruder

	SetPieceCarPos[8] = <<-1350.0201, -427.0193, 34.9505>>
	SetPieceCarQuatX[8] = 0.0377
	SetPieceCarQuatY[8] = 0.0064
	SetPieceCarQuatZ[8] = 0.3218
	SetPieceCarQuatW[8] = 0.9460
	SetPieceCarRecording[8] = 510
	SetPieceCarStartime[8] = 31000.0000
	SetPieceCarRecordingSpeed[8] = 1.0000
	SetPieceCarModel[8] = intruder

	SetPieceCarPos[9] = <<-1007.4637, -361.0907, 37.5214>>
	SetPieceCarQuatX[9] = -0.0193
	SetPieceCarQuatY[9] = 0.0110
	SetPieceCarQuatZ[9] = 0.9759
	SetPieceCarQuatW[9] = -0.2173
	SetPieceCarRecording[9] = 503
	SetPieceCarStartime[9] = 43500.0000
	SetPieceCarRecordingSpeed[9] = 1.0000
	SetPieceCarModel[9] = cruiser

	SetPieceCarPos[10] = <<-1007.2927, -357.3345, 37.5357>>
	SetPieceCarQuatX[10] = -0.0075
	SetPieceCarQuatY[10] = 0.0251
	SetPieceCarQuatZ[10] = 0.9720
	SetPieceCarQuatW[10] = -0.2335
	SetPieceCarRecording[10] = 502
	SetPieceCarStartime[10] = 44000.0000
	SetPieceCarRecordingSpeed[10] = 1.0000
	SetPieceCarModel[10] = cruiser

	SetPieceCarPos[11] = <<-1010.9365, -358.4532, 37.5252>>
	SetPieceCarQuatX[11] = -0.0052
	SetPieceCarQuatY[11] = 0.0138
	SetPieceCarQuatZ[11] = 0.9724
	SetPieceCarQuatW[11] = -0.2330
	SetPieceCarRecording[11] = 504
	SetPieceCarStartime[11] = 44100.0000
	SetPieceCarRecordingSpeed[11] = 1.0000
	SetPieceCarModel[11] = cruiser

	SetPieceCarPos[12] = <<-1013.8927, -392.8538, 36.0627>>
	SetPieceCarQuatX[12] = 0.0589
	SetPieceCarQuatY[12] = -0.0367
	SetPieceCarQuatZ[12] = -0.5154
	SetPieceCarQuatW[12] = 0.8542
	SetPieceCarRecording[12] = 84
	SetPieceCarStartime[12] = 46000.0000
	SetPieceCarRecordingSpeed[12] = 1.0000
	SetPieceCarModel[12] = manana

	/*SetPieceCarPos[13] = <<-1273.2351, -545.6791, 30.4319>>
	SetPieceCarQuatX[13] = 0.0109
	SetPieceCarQuatY[13] = -0.0390
	SetPieceCarQuatZ[13] = 0.9335
	SetPieceCarQuatW[13] = -0.3563
	SetPieceCarRecording[13] = 508
	SetPieceCarStartime[13] = 54500.0000
	SetPieceCarRecordingSpeed[13] = 1.0000
	SetPieceCarModel[13] = pcj*/

	SetPieceCarPos[14] = <<-1294.7389, -621.9509, 26.9939>>
	SetPieceCarQuatX[14] = 0.0257
	SetPieceCarQuatY[14] = 0.0005
	SetPieceCarQuatZ[14] = 0.2810
	SetPieceCarQuatW[14] = 0.9594
	SetPieceCarRecording[14] = 511
	SetPieceCarStartime[14] = 55000.0000
	SetPieceCarRecordingSpeed[14] = 1.0000
	SetPieceCarModel[14] = benson

	SetPieceCarPos[15] = <<-1138.3113, -811.2895, 15.7751>>
	SetPieceCarQuatX[15] = 0.0298
	SetPieceCarQuatY[15] = -0.0129
	SetPieceCarQuatZ[15] = -0.4436
	SetPieceCarQuatW[15] = 0.8956
	SetPieceCarRecording[15] = 992
	SetPieceCarStartime[15] = 67000.0 //68000.0000
	SetPieceCarRecordingSpeed[15] = 1.0000
	SetPieceCarModel[15] = Packer

	SetPieceCarPos[16] = <<-1163.0713, -741.8860, 19.4365>>
	SetPieceCarQuatX[16] = -0.0020
	SetPieceCarQuatY[16] = -0.0283
	SetPieceCarQuatZ[16] = 0.9308
	SetPieceCarQuatW[16] = -0.3644
	SetPieceCarRecording[16] = 515
	SetPieceCarStartime[16] = 69000.0000
	SetPieceCarRecordingSpeed[16] = 1.0000
	SetPieceCarModel[16] = Burrito3

	SetPieceCarPos[17] = <<-1201.0022, -821.8204, 14.7233>>
	SetPieceCarQuatX[17] = -0.0160
	SetPieceCarQuatY[17] = -0.0359
	SetPieceCarQuatZ[17] = 0.8871
	SetPieceCarQuatW[17] = -0.4599
	SetPieceCarRecording[17] = 512
	SetPieceCarStartime[17] = 71500.0000
	SetPieceCarRecordingSpeed[17] = 1.0000
	SetPieceCarModel[17] = youga

	SetPieceCarPos[18] = <<-1259.1127, -907.3938, 10.8468>>
	SetPieceCarQuatX[18] = 0.0139
	SetPieceCarQuatY[18] = -0.0091
	SetPieceCarQuatZ[18] = -0.4673
	SetPieceCarQuatW[18] = 0.8840
	SetPieceCarRecording[18] = 513
	SetPieceCarStartime[18] = 71600.0000
	SetPieceCarRecordingSpeed[18] = 1.0000
	SetPieceCarModel[18] = manana

	SetPieceCarPos[19] = <<-960.4544, -1235.3582, 5.1090>>
	SetPieceCarQuatX[19] = -0.0003
	SetPieceCarQuatY[19] = 0.0005
	SetPieceCarQuatZ[19] = -0.5006
	SetPieceCarQuatW[19] = 0.8657
	SetPieceCarRecording[19] = 11
	SetPieceCarStartime[19] = 83000.0000
	SetPieceCarRecordingSpeed[19] = 1.0000
	SetPieceCarModel[19] = benson

	SetPieceCarPos[20] = <<-1030.3206, -1135.5731, 1.6671>>
	SetPieceCarQuatX[20] = 0.0094
	SetPieceCarQuatY[20] = -0.0037
	SetPieceCarQuatZ[20] = -0.4742
	SetPieceCarQuatW[20] = 0.8804
	SetPieceCarRecording[20] = 507
	SetPieceCarStartime[20] = 88000.0000
	SetPieceCarRecordingSpeed[20] = 1.0000
	SetPieceCarModel[20] = intruder

	SetPieceCarPos[21] = <<-776.4766, -1115.1191, 10.0461>>
	SetPieceCarQuatX[21] = 0.0003
	SetPieceCarQuatY[21] = 0.0000
	SetPieceCarQuatZ[21] = 0.8673
	SetPieceCarQuatW[21] = 0.4978
	SetPieceCarRecording[21] = 20
	SetPieceCarStartime[21] = 96000.0000
	SetPieceCarRecordingSpeed[21] = 1.0000
	SetPieceCarModel[21] = manana

	SetPieceCarPos[22] = <<-729.9686, -1088.1537, 11.4620>>
	SetPieceCarQuatX[22] = -0.0111
	SetPieceCarQuatY[22] = -0.0437
	SetPieceCarQuatZ[22] = 0.8583
	SetPieceCarQuatW[22] = 0.5112
	SetPieceCarRecording[22] = 506
	SetPieceCarStartime[22] = 98000.0000
	SetPieceCarRecordingSpeed[22] = 1.0000
	SetPieceCarModel[22] = intruder

	SetPieceCarPos[23] = <<-792.2863, -1091.0381, 10.7970>>
	SetPieceCarQuatX[23] = -0.0186
	SetPieceCarQuatY[23] = -0.0192
	SetPieceCarQuatZ[23] = 0.9698
	SetPieceCarQuatW[23] = -0.2422
	SetPieceCarRecording[23] = 12
	SetPieceCarStartime[23] = 100700.0000
	SetPieceCarRecordingSpeed[23] = 1.0000
	SetPieceCarModel[23] = benson

	SetPieceCarPos[24] = <<-733.6007, -1164.0098, 9.9703>>
	SetPieceCarQuatX[24] = -0.0104
	SetPieceCarQuatY[24] = -0.0027
	SetPieceCarQuatZ[24] = 0.2836
	SetPieceCarQuatW[24] = 0.9589
	SetPieceCarRecording[24] = 17
	SetPieceCarStartime[24] = 101000.0000
	SetPieceCarRecordingSpeed[24] = 1.0000
	SetPieceCarModel[24] = intruder

	SetPieceCarPos[25] = <<-799.6644, -1066.3711, 11.3241>>
	SetPieceCarQuatX[25] = 0.0092
	SetPieceCarQuatY[25] = -0.0294
	SetPieceCarQuatZ[25] = 0.9687
	SetPieceCarQuatW[25] = -0.2464
	SetPieceCarRecording[25] = 132
	SetPieceCarStartime[25] = 101500.0000
	SetPieceCarRecordingSpeed[25] = 1.0000
	SetPieceCarModel[25] = manana

	SetPieceCarPos[26] = <<-641.2161, -945.9627, 21.1608>>
	SetPieceCarQuatX[26] = -0.0126
	SetPieceCarQuatY[26] = -0.0188
	SetPieceCarQuatZ[26] = 0.9977
	SetPieceCarQuatW[26] = 0.0640
	SetPieceCarRecording[26] = 24
	SetPieceCarStartime[26] = 103000.0000
	SetPieceCarRecordingSpeed[26] = 1.0000
	SetPieceCarModel[26] = manana

	/*SetPieceCarPos[27] = <<-684.8077, -1054.4844, 14.9669>>
	SetPieceCarQuatX[27] = -0.0101
	SetPieceCarQuatY[27] = -0.0407
	SetPieceCarQuatZ[27] = 0.8647
	SetPieceCarQuatW[27] = 0.5005
	SetPieceCarRecording[27] = 27
	SetPieceCarStartime[27] = 104500.0000
	SetPieceCarRecordingSpeed[27] = 1.0000
	SetPieceCarModel[27] = manana*/
	
	SetPieceCarPos[28] = <<-622.8137, -964.5755, 20.9500>>
	SetPieceCarQuatX[28] = -0.0052
	SetPieceCarQuatY[28] = -0.0167
	SetPieceCarQuatZ[28] = -0.4032
	SetPieceCarQuatW[28] = 0.9150
	SetPieceCarRecording[28] = 149
	SetPieceCarStartime[28] = 109446.0000
	SetPieceCarRecordingSpeed[28] = 1.0000
	SetPieceCarModel[28] = intruder

	SetPieceCarPos[29] = <<-527.2289, -948.6147, 22.9859>>
	SetPieceCarQuatX[29] = 0.0037
	SetPieceCarQuatY[29] = 0.0266
	SetPieceCarQuatZ[29] = -0.1871
	SetPieceCarQuatW[29] = 0.9820
	SetPieceCarRecording[29] = 514
	SetPieceCarStartime[29] = 113000.0000
	SetPieceCarRecordingSpeed[29] = 1.0000
	SetPieceCarModel[29] = manana

	SetPieceCarPos[30] = <<-507.5158, -866.4927, 29.2588>>
	SetPieceCarQuatX[30] = -0.0191
	SetPieceCarQuatY[30] = -0.0472
	SetPieceCarQuatZ[30] = 0.9921
	SetPieceCarQuatW[30] = 0.1146
	SetPieceCarRecording[30] = 22
	SetPieceCarStartime[30] = 114000.0000
	SetPieceCarRecordingSpeed[30] = 1.0000
	SetPieceCarModel[30] = intruder

	SetPieceCarPos[31] = <<-510.2627, -885.1514, 27.9052>>
	SetPieceCarQuatX[31] = -0.0331
	SetPieceCarQuatY[31] = -0.0438
	SetPieceCarQuatZ[31] = 0.9805
	SetPieceCarQuatW[31] = 0.1885
	SetPieceCarRecording[31] = 165
	SetPieceCarStartime[31] = 115000.0000
	SetPieceCarRecordingSpeed[31] = 1.0000
	SetPieceCarModel[31] = intruder

	SetPieceCarPos[32] = <<-387.9428, -838.6669, 31.4645>>
	SetPieceCarQuatX[32] = -0.0002
	SetPieceCarQuatY[32] = 0.0000
	SetPieceCarQuatZ[32] = 0.6826
	SetPieceCarQuatW[32] = 0.7308
	SetPieceCarRecording[32] = 159
	SetPieceCarStartime[32] = 117750.0000
	SetPieceCarRecordingSpeed[32] = 0.900 //TODO 487010: slow the bus down a touch.
	SetPieceCarModel[32] = bus

	SetPieceCarPos[33] = <<-358.8373, -747.3198, 33.7962>>
	SetPieceCarQuatX[33] = 0.0007
	SetPieceCarQuatY[33] = -0.0008
	SetPieceCarQuatZ[33] = 0.7083
	SetPieceCarQuatW[33] = 0.7059
	SetPieceCarRecording[33] = 21
	SetPieceCarStartime[33] = 128000.0000
	SetPieceCarRecordingSpeed[33] = 1.0000
	SetPieceCarModel[33] = burrito3

	SetPieceCarPos[34] = <<-142.6160, -753.6062, 33.9993>>
	SetPieceCarQuatX[34] = 0.0337
	SetPieceCarQuatY[34] = -0.0062
	SetPieceCarQuatZ[34] = -0.1730
	SetPieceCarQuatW[34] = 0.9843
	SetPieceCarRecording[34] = 15
	SetPieceCarStartime[34] = 134600.0000
	SetPieceCarRecordingSpeed[34] = 1.0000
	SetPieceCarModel[34] = benson	

	SetPieceCarPos[35] = <<-100.8266, -622.9727, 35.7906>>
	SetPieceCarQuatX[35] = -0.0093
	SetPieceCarQuatY[35] = -0.0050
	SetPieceCarQuatZ[35] = 0.9830
	SetPieceCarQuatW[35] = 0.1832
	SetPieceCarRecording[35] = 168
	SetPieceCarStartime[35] = 140400.0000
	SetPieceCarRecordingSpeed[35] = 1.0000
	SetPieceCarModel[35] = intruder

	


		
	//Parked cars
	ParkedCarPos[0] = <<-963.3202, -453.4057, 36.7332>>
	ParkedCarQuatX[0] = 0.0330
	ParkedCarQuatY[0] = -0.0051
	ParkedCarQuatZ[0] = 0.9744
	ParkedCarQuatW[0] = -0.2222
	ParkedCarModel[0] = BURRITO3
	
	ParkedCarPos[1] = <<-1116.9606, -779.3914, 17.4599>>
	ParkedCarQuatX[1] = -0.0053
	ParkedCarQuatY[1] = -0.0372
	ParkedCarQuatZ[1] = 0.5192
	ParkedCarQuatW[1] = 0.8538
	ParkedCarModel[1] = intruder //intruder 
	
	ParkedCarPos[2] = <<-1143.3539, -748.5402, 19.1161>>
	ParkedCarQuatX[2] = 0.0336
	ParkedCarQuatY[2] = 0.0100
	ParkedCarQuatZ[2] = -0.5869
	ParkedCarQuatW[2] = 0.8089
	ParkedCarModel[2] = manana //emperor
	
	ParkedCarPos[3] = <<-1129.9711, -755.3607, 18.9380>>
	ParkedCarQuatX[3] = 0.0135
	ParkedCarQuatY[3] = -0.0293
	ParkedCarQuatZ[3] = 0.8093
	ParkedCarQuatW[3] = 0.5865
	ParkedCarModel[3] = intruder //intruder 
	
	ParkedCarPos[4] = <<-1329.2493, -396.6274, 36.1102>>
	ParkedCarQuatX[4] = -0.0000
	ParkedCarQuatY[4] = -0.0024
	ParkedCarQuatZ[4] = 0.9673
	ParkedCarQuatW[4] = -0.2538
	ParkedCarModel[4] = manana //emperor
	
	ParkedCarPos[5] = <<-1341.7618, -403.7986, 36.0021>>
	ParkedCarQuatX[5] = -0.0050
	ParkedCarQuatY[5] = -0.0058
	ParkedCarQuatZ[5] = 0.9646
	ParkedCarQuatW[5] = -0.2634
	ParkedCarModel[5] = intruder //intruder 
	
	ParkedCarPos[6] = <<-1322.9462, -383.0046, 36.1916>>
	ParkedCarQuatX[6] = 0.0043
	ParkedCarQuatY[6] = 0.0031
	ParkedCarQuatZ[6] = -0.4992
	ParkedCarQuatW[6] = 0.8665
	ParkedCarModel[6] = manana //emperor
	
	ParkedCarPos[7] = <<-1838.4237, -510.6184, 27.6644>>
	ParkedCarQuatX[7] = -0.0368
	ParkedCarQuatY[7] = -0.0152
	ParkedCarQuatZ[7] = 0.8217
	ParkedCarQuatW[7] = 0.5685
	ParkedCarModel[7] = intruder //intruder 
	
	ParkedCarPos[8] = <<-1857.2256, -498.4425, 26.0763>>
	ParkedCarQuatX[8] = -0.0340
	ParkedCarQuatY[8] = -0.0146
	ParkedCarQuatZ[8] = 0.8269
	ParkedCarQuatW[8] = 0.5612
	ParkedCarModel[8] = BURRITO3
	
	ParkedCarPos[9] = <<-1146.8906, -850.3187, 14.2053>>
	ParkedCarQuatX[9] = 0.0276
	ParkedCarQuatY[9] = 0.0353
	ParkedCarQuatZ[9] = -0.4192
	ParkedCarQuatW[9] = 0.9068
	ParkedCarModel[9] = PACKER
	
	ParkedCarPos[10] = <<-358.9616, -754.4509, 33.5192>>
	ParkedCarQuatX[10] = 0.0094
	ParkedCarQuatY[10] = 0.0004
	ParkedCarQuatZ[10] = 0.7147
	ParkedCarQuatW[10] = -0.6994
	ParkedCarModel[10] = intruder //intruder 
	
	ParkedCarPos[11] = <<-358.6446, -768.9286, 33.5192>>
	ParkedCarQuatX[11] = 0.0007
	ParkedCarQuatY[11] = -0.0093
	ParkedCarQuatZ[11] = 0.7056
	ParkedCarQuatW[11] = 0.7085
	ParkedCarModel[11] = intruder //intruder 
	
	ParkedCarPos[12] = <<-342.1623, -767.6207, 33.5187>>
	ParkedCarQuatX[12] = 0.0006
	ParkedCarQuatY[12] = -0.0094
	ParkedCarQuatZ[12] = 0.7064
	ParkedCarQuatW[12] = 0.7078
	ParkedCarModel[12] = intruder //intruder 
	
	ParkedCarPos[13] = <<-342.2834, -753.7252, 33.5180>>
	ParkedCarQuatX[13] = 0.0093
	ParkedCarQuatY[13] = 0.0006
	ParkedCarQuatZ[13] = 0.7134
	ParkedCarQuatW[13] = -0.7007
	ParkedCarModel[13] = intruder //intruder
	
	ParkedCarPos[14] = <<-6.5124, -667.6953, 31.3380>>
	ParkedCarQuatX[14] = -0.0001
	ParkedCarQuatY[14] = 0.0003
	ParkedCarQuatZ[14] = 0.9997
	ParkedCarQuatW[14] = -0.0264
	ParkedCarModel[14] = stockade
	
	
	//Custom flags
	SET_BIT(TrafficCarFlags[34], TRAFFIC_FLAGS_FREEZE_CAR_FOR_COLLISION)
ENDPROC

PROC LOAD_UBER_DATA_2()
	
ENDPROC

PROC PRELOAD_ALL_CHASE_RECORDINGS(FLOAT fPlaybackTime)
	INT i = 0
	INT iSetpieceSize = COUNT_OF(SetPieceCarID)
	INT iTrafficSize = COUNT_OF(TrafficCarID)

	//Only do 10 cars per frame max to save execution time.
	WHILE iPreloadRecordingsIndex < iTrafficSize AND i < 10
		//Do traffic cars
		IF TrafficCarRecording[i] > 0
			IF TrafficCarStartime[i] > fPlaybackTime
				REQUEST_VEHICLE_RECORDING(TrafficCarRecording[i], strCarrec)
			ENDIF
		ENDIF
	
		//Do set-pieces in the same loop
		IF iPreloadRecordingsIndex < iSetpieceSize
			IF SetPieceCarRecording[iPreloadRecordingsIndex] > 0
				IF SetPieceCarStartime[iPreloadRecordingsIndex] > fPlaybackTime 
					REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[iPreloadRecordingsIndex], strCarrec)
				ELIF fPlaybackTime > SetPieceCarStartime[iPreloadRecordingsIndex] + 30000
					//Some setpieces aren't being removed, force them to be cleaned up.
					IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[iPreloadRecordingsIndex])
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[iPreloadRecordingsIndex])
							SET_VEHICLE_AS_NO_LONGER_NEEDED(SetPieceCarID[iPreloadRecordingsIndex])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		iPreloadRecordingsIndex++
		i++
	ENDWHILE
	
	IF iPreloadRecordingsIndex >= iTrafficSize
		iPreloadRecordingsIndex = 0
	ENDIF
ENDPROC

PROC REMOVE_ALL_CHASE_RECORDINGS()
	INT i = 0
	
	REPEAT COUNT_OF(SetPieceCarID) i
		IF SetPieceCarRecording[i] > 0
			REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[i], strCarrec)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(TrafficCarID) i
		IF TrafficCarRecording[i] > 0
			REMOVE_VEHICLE_RECORDING(TrafficCarRecording[i], strCarrec)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CONVERGE_VALUE(FLOAT &val, FLOAT fDesiredVal, FLOAT fAmountToConverge, BOOL bAdjustForFramerate = FALSE)
	IF val != fDesiredVal
		FLOAT fConvergeAmountThisFrame = fAmountToConverge
		IF bAdjustForFramerate
			fConvergeAmountThisFrame = 0.0 +@ (fAmountToConverge * 30.0)
		ENDIF
	
		IF val - fDesiredVal > fConvergeAmountThisFrame
			val -= fConvergeAmountThisFrame
		ELIF val - fDesiredVal < -fConvergeAmountThisFrame
			val += fConvergeAmountThisFrame
		ELSE
			val = fDesiredVal
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(bHasTextLabelTriggered)
	INT i = 0
	
	REPEAT iArraySize i
		bHasTextLabelTriggered[i] = FALSE
	ENDREPEAT
ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &veh, FLOAT fTime)
	IF NOT IS_ENTITY_DEAD(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, fTime - GET_TIME_POSITION_IN_RECORDING(veh))
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_ALL_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING)
ENDFUNC

FUNC INT GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED)
ENDFUNC


FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

PROC DO_FADE_OUT_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC DO_FADE_IN_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(obj)
		IF IS_ENTITY_ATTACHED(obj)
			DETACH_ENTITY(obj)
		ENDIF

		IF bForceDelete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_VEHICLE(VEHICLE_INDEX &veh, BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(veh)
		IF NOT IS_ENTITY_DEAD(veh)
			STOP_SYNCHRONIZED_ENTITY_ANIM(veh, NORMAL_BLEND_OUT, TRUE)
		
			IF IS_ENTITY_ATTACHED(veh)
				DETACH_ENTITY(veh)
			ENDIF
		ENDIF

		IF bForceDelete
			DELETE_VEHICLE(veh)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_PED(PED_INDEX &ped, BOOL bForceDelete = FALSE)
	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			STOP_SYNCHRONIZED_ENTITY_ANIM(ped, NORMAL_BLEND_OUT, TRUE)
		
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
			AND NOT IS_PED_GETTING_INTO_A_VEHICLE(ped)
				IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_PED(ped)
				OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(ped)
					DETACH_ENTITY(ped)
				ENDIF
				
				FREEZE_ENTITY_POSITION(ped, FALSE)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(ped)
				SET_ENTITY_COLLISION(ped, TRUE)
			ENDIF
			
			IF IS_PED_GROUP_MEMBER(ped, PLAYER_GROUP_ID())
				REMOVE_PED_FROM_GROUP(ped)
			ENDIF
			
			IF NOT bForceDelete
				SET_PED_KEEP_TASK(ped, TRUE)
			ENDIF
		ENDIF

		IF bForceDelete
			DELETE_PED(ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(ped)
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_ALL_BLIPS()	
	IF DOES_BLIP_EXIST(blipCurrentDestination)
		REMOVE_BLIP(blipCurrentDestination)
	ENDIF
	
	IF DOES_BLIP_EXIST(sLamar.blip)
		REMOVE_BLIP(sLamar.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(sJimmy.blip)
		REMOVE_BLIP(sJimmy.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(sSimeon.blip)
		REMOVE_BLIP(sSimeon.blip)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipLamarsCar)
		REMOVE_BLIP(blipLamarsCar)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipFakeGPS)
		REMOVE_BLIP(blipFakeGPS)
	ENDIF
	
	REMOVE_BLIP(sMainCars[0].blip)
	REMOVE_BLIP(sMainCars[1].blip)
ENDPROC

PROC REMOVE_ALL_OBJECTS(BOOL bForceDelete = FALSE)
	REMOVE_OBJECT(objCigarette, bForceDelete)
	REMOVE_OBJECT(objShirt, bForceDelete)
	REMOVE_OBJECT(objWardrobeDoors[0], bForceDelete)
	REMOVE_OBJECT(objWardrobeDoors[1], bForceDelete)
	REMOVE_OBJECT(objVaultShutter, bForceDelete)
	REMOVE_OBJECT(objFerrisWheel, bForceDelete)
	REMOVE_OBJECT(objHealthPack, TRUE)
	
	INT i = 0 
	REPEAT COUNT_OF(objShowroomGlass) i
		REMOVE_OBJECT(objShowroomGlass[i], FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(objFerrisCars) i
		REMOVE_OBJECT(objFerrisCars[i], FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(objBankGates) i
		REMOVE_OBJECT(objBankGates[i], FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(objBankGateCollision) i
		REMOVE_OBJECT(objBankGateCollision[i], FALSE)
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_PEDS(BOOL bForceDelete = FALSE)
	REMOVE_PED(sLamar.ped, bForceDelete)
	REMOVE_PED(sSimeon.ped, bForceDelete)
	REMOVE_PED(sJimmy.ped, bForceDelete)
	REMOVE_PED(sSecurityGuard.ped, bForceDelete)
	REMOVE_PED(pedCutsceneCops[0], bForceDelete)
	REMOVE_PED(pedCutsceneCops[1], bForceDelete)
	REMOVE_PED(sSmoker.ped, bForceDelete)
	REMOVE_PED(pedCarOwner, bForceDelete)
	REMOVE_PED(pedCreditsBoat, bForceDelete)
	
	INT i = 0
	REPEAT COUNT_OF(sAliens) i
		REMOVE_PED(sAliens[i].ped, bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(sAliensGroup2) i
		REMOVE_PED(sAliensGroup2[i].ped, bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(sBirds) i
		REMOVE_PED(sBirds[i].ped, bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(sMovieGuards) i
		REMOVE_PED(sMovieGuards[i].ped, bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(pedCreditsFerrisWheel) i
		REMOVE_PED(pedCreditsFerrisWheel[i], bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(pedCreditsCars) i
		REMOVE_PED(pedCreditsCars[i], bForceDelete)
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_VEHICLES(BOOL bForceDelete = FALSE)
	REMOVE_VEHICLE(sMainCars[0].veh, bForceDelete)
	REMOVE_VEHICLE(sMainCars[1].veh, bForceDelete)
	REMOVE_VEHICLE(vehTriggerCar, TRUE)
	REMOVE_VEHICLE(vehCutsceneCop, bForceDelete)
	REMOVE_VEHICLE(vehTrailer, bForceDelete)
	REMOVE_VEHICLE(vehFranklinsCar, bForceDelete)
	REMOVE_VEHICLE(vehCameraTest, TRUE)
	REMOVE_VEHICLE(vehTransporter, bForceDelete)
	REMOVE_VEHICLE(vehTanker, bForceDelete)
	REMOVE_VEHICLE(vehFinalCutsceneCar, bForceDelete)
	REMOVE_VEHICLE(vehCreditsSailboat, bForceDelete)
	REMOVE_VEHICLE(vehCreditsBoat, bForceDelete)
	REMOVE_VEHICLE(vehCreditsSportsCar, TRUE)
	
	INT i = 0	
	REPEAT COUNT_OF(vehShowroomCars) i
		REMOVE_VEHICLE(vehShowroomCars[i], bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(vehCarsOutsideShowroom) i
		REMOVE_VEHICLE(vehCarsOutsideShowroom[i], bForceDelete)
	ENDREPEAT
	
	REPEAT COUNT_OF(vehCreditsCars) i
		REMOVE_VEHICLE(vehCreditsCars[i], bForceDelete)
	ENDREPEAT
ENDPROC

PROC CLEAR_ALL_RAGE_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_RAGEBAR")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_RAGEHOW")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_RAGESTAT")
		CLEAR_HELP()
	ENDIF
	
	// Clear PC specific help
	IF IS_PC_VERSION()
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_RAGEBAR_KM")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	FLASH_ABILITY_BAR(0)
ENDPROC

FUNC BOOL SETUP_REQ_VAULT_SHUTTER(VECTOR vPos, VECTOR vRot, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(objVaultShutter)
		REQUEST_MODEL(Prop_Vault_Shutter)
		
		IF NOT bJustRequestAssets
			IF interiorBank = NULL
				interiorBank = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-7.9, -662.2, 34.7>>, "dt1_03_carpark") 
			ELSE
				PIN_INTERIOR_IN_MEMORY(interiorBank)
			ENDIF
		ENDIF
			
		IF HAS_MODEL_LOADED(Prop_Vault_Shutter)
			IF NOT bJustRequestAssets
				IF interiorBank != NULL
					IF IS_INTERIOR_READY(interiorBank)
						objVaultShutter = CREATE_OBJECT_NO_OFFSET(Prop_Vault_Shutter, vPos)
						SET_ENTITY_ROTATION(objVaultShutter, vRot)
						FREEZE_ENTITY_POSITION(objVaultShutter, TRUE)
						FORCE_ROOM_FOR_ENTITY(objVaultShutter, interiorBank, HASH("GtaMloRoom003"))
						
						SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Vault_Shutter)
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///Used during the cops cutscene to keep the side car park in memory.
FUNC BOOL SET_SIDE_CAR_PARK_INTERIOR_ACTIVE(BOOL bIsActive)
	IF bIsActive
		IF interiorCarPark2 = NULL
			interiorCarPark2 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<89.7, -694.9, 32.7>>, "dt1_05_carpark") 
		ELSE
			IF IS_INTERIOR_READY(interiorCarPark2)
				RETURN TRUE
			ELSE
				PIN_INTERIOR_IN_MEMORY(interiorCarPark2)
			ENDIF
		ENDIF
	ELSE
		IF interiorCarPark2 != NULL
			UNPIN_INTERIOR(interiorCarPark2)
			interiorCarPark2 = NULL
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_RAGE()
	CONST_FLOAT RAGE_ACCEL	1.0
	CONST_FLOAT RAGE_DECEL	3.5
	CONST_FLOAT MAX_SPEED 	55.0
    
	IF NOT sRageData.bHasRaged				
		IF eMissionStage = STAGE_CHASE
			//Fill up the bar as the player progresses through the chase.
			IF bAllowSpecialAbilityDuringChase
				//Don't force this any more, just use the standard behaviour.
				/*IF NOT IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
					sRageData.fCurrentRage = (fCurrentPlaybackTime / 41500.0)
					IF sRageData.fCurrentRage > 1.0
						sRageData.fCurrentRage = 1.0
					ENDIF
					
					SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
					//SPECIAL_ABILITY_RESET(PLAYER_ID())
					SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), sRageData.fCurrentRage, TRUE)
				ENDIF*/
			ELSE
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecMain, strCarrec)
					FLOAT fTotalDuration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec)
					
					sRageData.fCurrentRage = (fCurrentPlaybackTime / fTotalDuration) * 99.0
					
					SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
					SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), sRageData.fCurrentRage / 100.0, TRUE)
				ENDIF
			ENDIF
		ELIF eMissionStage = STAGE_GO_TO_GARAGE
			//If the special ability is allowed during the chase don't do anything in the lose cops section.
			IF NOT bAllowSpecialAbilityDuringChase
				//Fill up the last bit of the bar.
				IF NOT sRageData.bHasRaged
					sRageData.fCurrentRage = sRageData.fCurrentRage +@ RAGE_ACCEL
					
					IF sRageData.fCurrentRage > 100.0
						sRageData.fCurrentRage = 100.0
					ENDIF
				
					SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
					SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), sRageData.fCurrentRage / 100.0, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


///Creates a new load scene sphere and waits until it has finished loading or it times out.
PROC NEW_LOAD_SCENE_SPHERE_WITH_WAIT(VECTOR vPos, FLOAT fRadius, NEWLOADSCENE_FLAGS controlFlags = 0, INT iMaxWaitTime = 10000)
	INT iTimeOut = GET_GAME_TIMER()

	WHILE GET_GAME_TIMER() - iTimeOut < iMaxWaitTime
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_START_SPHERE(vPos, fRadius, controlFlags)
		ELIF IS_NEW_LOAD_SCENE_LOADED()
			iTimeOut = 0
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	NEW_LOAD_SCENE_STOP()	
ENDPROC

///Creates a new load scene frustrum and waits until it has finished loading or it times out.
PROC NEW_LOAD_SCENE_FRUSTRUM_WITH_WAIT(VECTOR vPos, VECTOR vDir, FLOAT fDist, NEWLOADSCENE_FLAGS controlFlags = 0, INT iMaxWaitTime = 10000)
	INT iTimeOut = GET_GAME_TIMER()

	WHILE GET_GAME_TIMER() - iTimeOut < iMaxWaitTime
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_START(vPos, vDir, fDist, controlFlags)
		ELIF IS_NEW_LOAD_SCENE_LOADED()
			iTimeOut = 0
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	NEW_LOAD_SCENE_STOP()	
ENDPROC

///Creates an ambient car at a specific point.
PROC CREATE_RANDOM_CAR_AT_POS(MODEL_NAMES model, MODEL_NAMES pedModel, VECTOR vPos, FLOAT fHeading, INT iArrayIndex, FLOAT fSpeed = 15.0, BOOL bCreatePedInside = FALSE)
	IF HAS_MODEL_LOADED(pedModel)
	AND HAS_MODEL_LOADED(model)
		vehCreditsCars[iArrayIndex] = CREATE_VEHICLE(model, vPos, fHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehCreditsCars[iArrayIndex])
		
		IF bCreatePedInside
			pedCreditsCars[iArrayIndex] = CREATE_PED_INSIDE_VEHICLE(vehCreditsCars[iArrayIndex], PEDTYPE_MISSION, pedModel)
			TASK_STAND_STILL(pedCreditsCars[iArrayIndex], -1)
			//TASK_VEHICLE_MISSION(pedCreditsCars[iArrayIndex], vehCreditsCars[iArrayIndex], NULL, MISSION_CRUISE, fSpeed, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
			//SET_PED_KEEP_TASK(pedCreditsCars[iArrayIndex], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCreditsCars[iArrayIndex], TRUE)
		ENDIF
		
		IF fSpeed > 0.0
			//SET_VEHICLE_FORWARD_SPEED(vehCreditsCars[iArrayIndex], fSpeed)
		ENDIF
		
		CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] CREATE_RANDOM_CAR_AT_POS: Created car ", iArrayIndex)
	ENDIF
ENDPROC

PROC REQUEST_TRAFFIC_IN_ADVANCE(FLOAT fCutoffTime, FLOAT fStartTime = 0.0)
	IF fCurrentPlaybackTime < fCutoffTime
		INT i = 0
		REPEAT COUNT_OF(TrafficCarID) i
			IF TrafficCarRecording[i] != 0
				IF TrafficCarStartime[i] < fCutoffTime
				AND TrafficCarStartime[i] > fStartTime - 10000.0
					REQUEST_MODEL(TrafficCarModel[i])
					REQUEST_VEHICLE_RECORDING(TrafficCarRecording[i], strCarrec)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(SetPieceCarID) i
			IF SetPieceCarRecording[i] != 0
				IF SetPieceCarStartime[i] < fCutoffTime
				AND SetPieceCarStartime[i] > fStartTime - 10000.0
					IF NOT DOES_ENTITY_EXIST(SetPieceCarID[i])
						REQUEST_MODEL(SetPieceCarModel[i])
					ENDIF
					REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[i], strCarrec)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


PROC RUBBER_BAND_TEST(FLOAT &fCurrentSpeed, VEHICLE_INDEX vehPlayer, VEHICLE_INDEX vehTrigger)
	IF IS_VEHICLE_DRIVEABLE(vehTrigger)
	AND IS_VEHICLE_DRIVEABLE(vehPlayer)
		FLOAT fDesiredPlaybackSpeed = 1.0
		FLOAT fAccel = 0.01
		
		//Default values for any adjustments to the player's speed and the playback accel.
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			fAccel = 0.025
		ENDIF
		
		//Special cases: at these points we want the buddy to be further from or nearer to the player than normal
		IF fCurrentPlaybackTime < 7500.0 
			//At start: keep them further apart to avoid U-turn issues.
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 8000.0 AND fCurrentPlaybackTime < 12000.0 
			//Move forward at first turn
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 30.0, 55.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 26000.0 AND fCurrentPlaybackTime < 30000.0 
			//At start of dual carriageway: speed up as it's quite easy to overtake
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 20.0, 55.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 40000.0 AND fCurrentPlaybackTime < 45500.0 
			//Two turns before movie lot
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 30.0, 55.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 46000.0 AND fCurrentPlaybackTime < 51000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 45.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 60000.0 AND fCurrentPlaybackTime < 64000.0 
			//Post-movie lot
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 50.0, 60.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 77500.0 AND fCurrentPlaybackTime < 81500.0 
			//Just before start of bumpy road
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 25.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 83500.0 AND fCurrentPlaybackTime < 87500.0 
			//Just before splash screen (Lamar gets a little close)
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 30.0, 55.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 87500.0 AND fCurrentPlaybackTime < 98500.0 
			//After bumps: speed him up, slow down accel a bit as the speed up is obvious.
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 40.0, 60.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
			fAccel *= 0.7
		ELIF fCurrentPlaybackTime > 108000.0 AND fCurrentPlaybackTime < 113500.0 
			//Before police station turn
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 35.0, 55.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 119000.0 AND fCurrentPlaybackTime < 121500.0 
			//Before bus turn
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 35.0, 55.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 122000.0 AND fCurrentPlaybackTime < 125000.0 
			//Before car park turn: speed him up a bit
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 20.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 126000.0 AND fCurrentPlaybackTime < 131500.0 
			//During car park: stop him getting too far away.
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 15.0, 30.0, 60.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 131500.0 AND fCurrentPlaybackTime < 137700.0 
			//Just after car park: speed up a little
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 15.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 140900.0 AND fCurrentPlaybackTime < 144600.0
			//Final section: speed up Lamar, player can possibly overtake at this bit, but try to keep him close.
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 15.0, 30.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELIF fCurrentPlaybackTime > 145600.0 AND fCurrentPlaybackTime < 149400.0 
			//Slow down just before the cutscene trigger (TODO 274875)
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 8.0, 20.0, 80.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ELSE 
			//Default
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, vehPlayer, vehTrigger, 4.5, 10.0, 50.0, 100.0, 30.0, 1.0, 0.7, 0.5, 2.0, TRUE, 15.0, 25.0)
		ENDIF
			
		//Special cases where desired speed is forced to a specific value.
		IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			fDesiredPlaybackSpeed = 1.0
		ENDIF
		
		IF fCurrentPlaybackTime < 19000.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2013.521484,-410.929047,10.103262>>, <<-2005.116699,-399.659576,15.681062>>, 14.000000)
				IF fDesiredPlaybackSpeed < 1.0
					fDesiredPlaybackSpeed = 1.0
				ENDIF
			ENDIF
		ENDIF
		
		//Fix for 504332: if the player tries to take a shortcut down the alley make sure Lamar isn't slowed down.
		IF fCurrentPlaybackTime > 65000.0 AND fCurrentPlaybackTime < 78000.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1327.701172,-648.209534,35.882713>>, <<-1196.130981,-817.147583,13.715324>>, 33.750000)
				IF fCurrentPlaybackTime < 75000.0
					fDesiredPlaybackSpeed = 1.5
				ELSE
					fDesiredPlaybackSpeed = 1.2
				ENDIF
			ENDIF	
		ENDIF
		
		//Don't do any slowdown after going past the cutscene trigger.
		IF fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 4000.0
			IF fDesiredPlaybackSpeed < 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ELIF fCurrentPlaybackTime > 137600.0
			//Cap the max speed during the final tunnels: this gives the player the chance to overtake.
			IF fDesiredPlaybackSpeed > 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ENDIF		
		
		UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentSpeed, fDesiredPlaybackSpeed, fAccel)
	ENDIF
ENDPROC


/// PURPOSE:
///    Calculates a new speed for the trigger vehicle based on it's distance from the player
/// PARAMS:
///    fCurrentSpeed - The current playback speed
///    fDist - The distance (note it will expect this a square of the actual distance)
PROC RUBBER_BAND_CHASE(FLOAT &fCurrentSpeed, VEHICLE_INDEX vehPlayer, VEHICLE_INDEX vehTrigger)
	IF IS_VEHICLE_DRIVEABLE(vehTrigger)
	AND IS_VEHICLE_DRIVEABLE(vehPlayer)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(vehPlayer)
		VECTOR vTriggerPos = GET_ENTITY_COORDS(vehTrigger)
	
		//Rubber banding
		FLOAT fDist = VDIST(vPlayerPos, vTriggerPos)
		FLOAT fTouchingDist = 4.5 
		FLOAT fIdealDist = 10.0 
		FLOAT fMaxDist = 40.0 
		FLOAT fSuperMaxDist = 100.0 
		FLOAT fMaxDistInFront = 30.0
		FLOAT fDesiredPlaybackSpeed = 1.0
		FLOAT fAccel = 0.0
		FLOAT fPlayersSpeedModifier = 0.0
		FLOAT fSpeedDiff = GET_ENTITY_SPEED(vehTrigger) - GET_ENTITY_SPEED(vehPlayer)
		
		//Default values for any adjustments to the player's speed and the playback accel.
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			fAccel = 0.025
			fPlayersSpeedModifier = -35.0
		ELSE
			fAccel = 0.01
			fPlayersSpeedModifier = -15.0
		ENDIF
		
		//Special cases: at these points we want the buddy to be further from or nearer to the player than normal
		
		//At start: keep them further apart to avoid U-turn issues.
		IF fCurrentPlaybackTime < 7500.0
			fIdealDist = 10.0
			fMaxDist = 50.0
		ENDIF
		
		//Move forward at first turn
		IF fCurrentPlaybackTime > 8000.0 AND fCurrentPlaybackTime < 12000.0
			fIdealDist = 30.0
			fMaxDist = 55.0
		ENDIF
		
		//At start of dual carriageway: speed up as it's quite easy to overtake
		IF fCurrentPlaybackTime > 26000.0 AND fCurrentPlaybackTime < 30000.0
			fIdealDist = 20.0
			fMaxDist = 55.0
		ENDIF
		
		//Two turns before movie lot
		IF fCurrentPlaybackTime > 40000.0 AND fCurrentPlaybackTime < 45500.0
			fIdealDist = 30.0
			fMaxDist = 55.0
		ENDIF
		
		IF fCurrentPlaybackTime > 46000.0 AND fCurrentPlaybackTime < 51000.0
			fIdealDist = 45.0
			fMaxDist = 50.0
		ENDIF
		
		//Post-movie lot
		IF fCurrentPlaybackTime > 60000.0 AND fCurrentPlaybackTime < 64000.0
			fIdealDist = 50.0
			fMaxDist = 60.0
		ENDIF
		
		//Just before start of bumpy road
		IF fCurrentPlaybackTime > 77500.0 AND fCurrentPlaybackTime < 81500.0
			fIdealDist = 25.0
			fMaxDist = 50.0
		ENDIF
		
		//Just before splash screen (Lamar gets a little close)
		IF fCurrentPlaybackTime > 83500.0 AND fCurrentPlaybackTime < 87500.0
			fIdealDist = 30.0
			fMaxDist = 55.0
		ENDIF
		
		//After bumps: speed him up, slow down accel a bit as the speed up is obvious.
		IF fCurrentPlaybackTime > 87500.0 AND fCurrentPlaybackTime < 98500.0
			fAccel *= 0.7
			fIdealDist = 40.0
			fMaxDist = 60.0
		ENDIF
		
		//Before police station turn
		IF fCurrentPlaybackTime > 108000.0 AND fCurrentPlaybackTime < 113500.0
			fIdealDist = 35.0
			fMaxDist = 55.0
		ENDIF
		
		//Before bus turn
		IF fCurrentPlaybackTime > 119000.0 AND fCurrentPlaybackTime < 121500.0
			fIdealDist = 35.0
			fMaxDist = 55.0
		ENDIF
		
		//Before car park turn: speed him up a bit
		IF fCurrentPlaybackTime > 122000.0 AND fCurrentPlaybackTime < 125000.0
			fIdealDist = 20.0
			fMaxDist = 50.0
		ENDIF
		
		//During car park: stop him getting too far away.
		IF fCurrentPlaybackTime > 126000.0 AND fCurrentPlaybackTime < 131500.0
			fIdealDist = 15.0
			fMaxDist = 30.0
			fSuperMaxDist = 60.0
		ENDIF
		
		//Just after car park: speed up a little
		IF fCurrentPlaybackTime > 131500.0 AND fCurrentPlaybackTime < 137700.0 
			fIdealDist = 15.0
			fMaxDist = 50.0
		ENDIF
		
		//Final section: speed up Lamar, player can possibly overtake at this bit, but try to keep him close.
		IF fCurrentPlaybackTime > 140900.0 AND fCurrentPlaybackTime < 144600.0
			fIdealDist = 15.0
			fMaxDist = 30.0
		ENDIF
		
		//Slow down just before the cutscene trigger (TODO 274875)
		IF fCurrentPlaybackTime > 145600.0 AND fCurrentPlaybackTime < 149400.0
			fIdealDist = 8.0
			fMaxDist = 20.0
			fSuperMaxDist = 80.0
		ENDIF
		
		FLOAT fDistRatio = 0.0
		VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrigger, vPlayerPos)
		IF vOffset.y < 1.0 
			//Player is behind
			IF fDist > fMaxDist 
				//Player is a long way behind, slow the trigger car down by a lot (if not on screen).
				BOOL bInsanelyFarBehind = FALSE
				IF fDist > fSuperMaxDist
					IF fDist > 200.0
						bInsanelyFarBehind = TRUE
					ENDIF
					fDist = fSuperMaxDist
				ENDIF
				
				fDistRatio = ((fDist - fMaxDist) / (fSuperMaxDist - fMaxDist))
				
				IF IS_ENTITY_ON_SCREEN(vehTrigger) AND NOT bInsanelyFarBehind
					IF fSpeedDiff > 10.0
						fDesiredPlaybackSpeed = 0.7 - (0.2 * fDistRatio)
					ELSE
						fDesiredPlaybackSpeed = 0.7 - (0.1 * fDistRatio)
					ENDIF
				ELSE
					fDesiredPlaybackSpeed = 0.7 - (0.5 * fDistRatio) //0.5 - (0.3 * fDistRatio)
				ENDIF
				
				fPlayersSpeedModifier *= 0.5
			ELIF fDist > fIdealDist
				//Player is somewhat behind, slow the trigger car down relative to how far behind the player is.
				fDistRatio = ((fDist - fIdealDist) / (fMaxDist - fIdealDist))
				fDesiredPlaybackSpeed = 1.0 - (0.3 * fDistRatio)
			ELSE
				//Player is getting too close, speed up the trigger car relative to how close the player is.
				IF fDist < fTouchingDist
					fDist = fTouchingDist
				ENDIF
				
				fDistRatio = (fIdealDist - fDist) / (fIdealDist - fTouchingDist)
				
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					fDesiredPlaybackSpeed = 1.0 + (fDistRatio * 2.0)
				ELSE
					fDesiredPlaybackSpeed = 1.0 + (fDistRatio)
				ENDIF
			ENDIF
		ELSE 
			//Player is in front, speed up trigger car based on the player's own speed.
			IF fDist > fMaxDistInFront
				fDist = fMaxDistInFront
			ENDIF
			fDistRatio = fDist / fMaxDistInFront
			
			//If the player is already going slower then don't speed up the buddy as much.
			IF fSpeedDiff > 0.0
				fDistRatio = fDistRatio * 0.5
			ENDIF
			
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				fDesiredPlaybackSpeed = 2.0 + fDistRatio
			ELSE
				IF GET_ENTITY_SPEED(vehPlayer) < 5.0 //If the player has slowed down slow the buddy down so it's not as harsh if they plough through.
				AND fSpeedDiff > 0.0
					fDesiredPlaybackSpeed = 0.6 + fDistRatio
				ELSE
					fDesiredPlaybackSpeed = 1.0 + fDistRatio
				ENDIF
			ENDIF
		ENDIF
		
		//Special cases where desired speed is forced to a specific value.
		IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			fDesiredPlaybackSpeed = 1.0
		ENDIF
		
		IF fCurrentPlaybackTime < 19000.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2013.521484,-410.929047,10.103262>>, <<-2005.116699,-399.659576,15.681062>>, 14.000000)
				IF fDesiredPlaybackSpeed < 1.0
					fDesiredPlaybackSpeed = 1.0
				ENDIF
			ENDIF
		ENDIF
		
		//Fix for 504332: if the player tries to take a shortcut down the alley make sure Lamar isn't slowed down.
		IF fCurrentPlaybackTime > 65000.0 AND fCurrentPlaybackTime < 78000.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1327.701172,-648.209534,35.882713>>, <<-1196.130981,-817.147583,13.715324>>, 33.750000)
				IF fCurrentPlaybackTime < 75000.0
					fDesiredPlaybackSpeed = 1.5
				ELSE
					fDesiredPlaybackSpeed = 1.2
				ENDIF
			ENDIF	
		ENDIF
		
		//For the jump section don't allow the car speed to go too low.
		IF fCurrentPlaybackTime > 86500.0 AND fCurrentPlaybackTime < 96500.0
			IF fDesiredPlaybackSpeed < 0.7
				fDesiredPlaybackSpeed = 0.7
			ENDIF
		ENDIF
		
		//Don't do any slowdown after going past the cutscene trigger.
		IF fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 4000.0
			IF fDesiredPlaybackSpeed < 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ELIF fCurrentPlaybackTime > 137400.0
			//Cap the max speed during the final tunnels: this gives the player the chance to overtake.
			IF fDesiredPlaybackSpeed > 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ENDIF
		
		//If the trigger speed is trying to catch up from really slow then increase the acceleration.
		IF fCurrentSpeed < 1.0
		AND fDesiredPlaybackSpeed > fCurrentSpeed
			fAccel *= 2.0
		ENDIF
		
		//Modify the player's speed further based on the desired speed.
		IF fDesiredPlaybackSpeed > 1.0
			fPlayersSpeedModifier = fPlayersSpeedModifier - ((fDesiredPlaybackSpeed - 1.0) * 10.0)
		ENDIF
		
		
		//Make the speed changes.
		MODIFY_VEHICLE_TOP_SPEED(sMainCars[iPlayersCar].veh, fPlayersSpeedModifier)
		fCurrentSpeed = fCurrentSpeed +@ (((fDesiredPlaybackSpeed - fCurrentSpeed) * fAccel) * 30.0)
		
		#IF IS_DEBUG_BUILD
			IF bDebugShowChaseStats	
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDist = "Distance: "
				strDist += GET_STRING_FROM_FLOAT(fDist)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", strDist)
			
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDesiredSpeed = "Desired playback speed: "
				strDesiredSpeed += GET_STRING_FROM_FLOAT(fDesiredPlaybackSpeed)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.15, "STRING", strDesiredSpeed)
			
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strSpeed = "Playback speed: "
				strSpeed += GET_STRING_FROM_FLOAT(fCurrentSpeed)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.2, "STRING", strSpeed)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strTime = "Time: "
				strTime += GET_STRING_FROM_FLOAT(fCurrentPlaybackTime)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.25, "STRING", strTime)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDiff = "Speed Diff: "
				strDiff += GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(vehTrigger) - GET_ENTITY_SPEED(vehPlayer))
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.3, "STRING", strDiff)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strState = "Trigger car state: "
				IF vOffset.y < 0.0 
					strState += "in front and "
				ELSE
					strState += "behind and "
				ENDIF
				
				IF IS_ENTITY_ON_SCREEN(vehTrigger)
					strState += "on screen."
				ELSE
					strState += "off screen."
				ENDIF
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.35, "STRING", strState)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strAccel = "Accel: "
				strAccel += GET_STRING_FROM_FLOAT(fAccel)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.4, "STRING", strAccel)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strModifier = "Player speed % modifier: "
				strModifier += GET_STRING_FROM_FLOAT(fPlayersSpeedModifier)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.45, "STRING", strModifier)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDuration = "Total duration: "
				strDuration += GET_STRING_FROM_FLOAT(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec))
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.5, "STRING", strDuration)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    1220706 - Same as RUBBER_BAND_CHASE but keeps the trigger car closer to the player.
PROC RUBBER_BAND_CHASE_CLOSE_VERSION(FLOAT &fCurrentSpeed, VEHICLE_INDEX vehPlayer, VEHICLE_INDEX vehTrigger)
	IF IS_VEHICLE_DRIVEABLE(vehTrigger)
	AND IS_VEHICLE_DRIVEABLE(vehPlayer)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(vehPlayer)
		VECTOR vTriggerPos = GET_ENTITY_COORDS(vehTrigger)
	
		//Rubber banding
		FLOAT fDist = VDIST(vPlayerPos, vTriggerPos)
		FLOAT fTouchingDist = 4.5 
		FLOAT fIdealDist = 7.0 
		FLOAT fMaxDist = 40.0 
		FLOAT fSuperMaxDist = 100.0 
		FLOAT fMaxDistInFront = 30.0
		FLOAT fDesiredPlaybackSpeed = 1.0
		FLOAT fAccel = 0.0
		FLOAT fPlayersSpeedModifier = 0.0
		FLOAT fSpeedDiff = GET_ENTITY_SPEED(vehTrigger) - GET_ENTITY_SPEED(vehPlayer)
		
		//Default values for any adjustments to the player's speed and the playback accel.
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			fAccel = 0.025
			fPlayersSpeedModifier = -35.0
		ELSE
			fAccel = 0.01
			fPlayersSpeedModifier = -15.0
		ENDIF
		
		//Special cases: at these points we want the buddy to be further from or nearer to the player than normal
		
		//At start: keep them further apart to avoid U-turn issues.
		IF fCurrentPlaybackTime < 7500.0
			fIdealDist = 10.0
			fMaxDist = 50.0
		ENDIF
		
		//Move forward at first turn
		IF fCurrentPlaybackTime > 8000.0 AND fCurrentPlaybackTime < 12000.0
			fIdealDist = 30.0
			fMaxDist = 55.0
		ENDIF
		
		//At start of dual carriageway: speed up as it's quite easy to overtake
		IF fCurrentPlaybackTime > 26000.0 AND fCurrentPlaybackTime < 30000.0
			fIdealDist = 13.0
			fMaxDist = 45.0
		ENDIF
		
		//Two turns before movie lot
		IF fCurrentPlaybackTime > 40000.0 AND fCurrentPlaybackTime < 45500.0
			fIdealDist = 15.0
			fMaxDist = 45.0
		ENDIF
		
		IF fCurrentPlaybackTime > 46000.0 AND fCurrentPlaybackTime < 51000.0
			fAccel *= 0.7
			fIdealDist = 22.0
			fMaxDist = 50.0
		ENDIF
		
		//Post-movie lot
		IF fCurrentPlaybackTime > 60000.0 AND fCurrentPlaybackTime < 64000.0
			fAccel *= 0.7
			fIdealDist = 22.0
			fMaxDist = 50.0
		ENDIF
		
		//In the alley: keep a bit of distance.
		IF fCurrentPlaybackTime > 65000.0 AND fCurrentPlaybackTime < 70000.0
			fAccel *= 0.7
			fIdealDist = 15.0
			fMaxDist = 50.0
		ENDIF
		
		//Just before start of bumpy road
		IF fCurrentPlaybackTime > 77500.0 AND fCurrentPlaybackTime < 81500.0
			fIdealDist = 18.0
			fMaxDist = 45.0
		ENDIF
		
		//Just before splash screen (Lamar gets a little close)
		IF fCurrentPlaybackTime > 83500.0 AND fCurrentPlaybackTime < 87500.0
			fAccel *= 0.7
			fIdealDist = 20.0
			fMaxDist = 45.0
		ENDIF
		
		//After bumps: speed him up, slow down accel a bit as the speed up is obvious.
		IF fCurrentPlaybackTime > 87500.0 AND fCurrentPlaybackTime < 98500.0
			fAccel *= 0.7
			fIdealDist = 25.0
			fMaxDist = 50.0
		ENDIF
		
		//Before police station turn
		IF fCurrentPlaybackTime > 108000.0 AND fCurrentPlaybackTime < 113500.0
			fAccel *= 0.7
			fIdealDist = 22.0
			fMaxDist = 50.0
		ENDIF
		
		//Before bus turn
		IF fCurrentPlaybackTime > 119000.0 AND fCurrentPlaybackTime < 121500.0
			fIdealDist = 25.0
			fMaxDist = 50.0
		ENDIF
		
		//Before car park turn: speed him up a bit
		IF fCurrentPlaybackTime > 122000.0 AND fCurrentPlaybackTime < 125000.0
			fIdealDist = 15.0
			fMaxDist = 45.0
		ENDIF
		
		//During car park: stop him getting too far away.
		IF fCurrentPlaybackTime > 126000.0 AND fCurrentPlaybackTime < 131500.0
			fIdealDist = 11.0
			fMaxDist = 30.0
			fSuperMaxDist = 60.0
		ENDIF
		
		//Just after car park: speed up a little
		IF fCurrentPlaybackTime > 131500.0 AND fCurrentPlaybackTime < 137700.0 
			fAccel *= 0.7
			fIdealDist = 12.0
			fMaxDist = 45.0
		ENDIF
		
		//Final section: speed up Lamar, player can possibly overtake at this bit, but try to keep him close.
		IF fCurrentPlaybackTime > 140900.0 AND fCurrentPlaybackTime < 144600.0
			fIdealDist = 15.0
			fMaxDist = 30.0
		ENDIF
		
		//Slow down just before the cutscene trigger (TODO 274875)
		IF fCurrentPlaybackTime > 145600.0 AND fCurrentPlaybackTime < 149400.0
			fIdealDist = 8.0
			fMaxDist = 20.0
			fSuperMaxDist = 80.0
		ENDIF
		
		FLOAT fDistRatio = 0.0
		VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrigger, vPlayerPos)
		IF vOffset.y < 1.0 
			//Player is behind
			IF fDist > fMaxDist 
				//Player is a long way behind, slow the trigger car down by a lot (if not on screen).
				BOOL bInsanelyFarBehind = FALSE
				IF fDist > fSuperMaxDist
					IF fDist > 200.0
						bInsanelyFarBehind = TRUE
					ENDIF
					fDist = fSuperMaxDist
				ENDIF
				
				fDistRatio = ((fDist - fMaxDist) / (fSuperMaxDist - fMaxDist))
				
				IF IS_ENTITY_ON_SCREEN(vehTrigger) AND NOT bInsanelyFarBehind
					IF fSpeedDiff > 10.0
						fDesiredPlaybackSpeed = 0.7 - (0.2 * fDistRatio)
					ELSE
						fDesiredPlaybackSpeed = 0.7 - (0.1 * fDistRatio)
					ENDIF
				ELSE
					fDesiredPlaybackSpeed = 0.7 - (0.5 * fDistRatio) //0.5 - (0.3 * fDistRatio)
				ENDIF
				
				fPlayersSpeedModifier *= 0.5
			ELIF fDist > fIdealDist
				//Player is somewhat behind, slow the trigger car down relative to how far behind the player is.
				fDistRatio = ((fDist - fIdealDist) / (fMaxDist - fIdealDist))
				fDesiredPlaybackSpeed = 1.0 - (0.3 * fDistRatio)
			ELSE
				//Player is getting too close, speed up the trigger car relative to how close the player is.
				IF fDist < fTouchingDist
					fDist = fTouchingDist
				ENDIF
				
				fDistRatio = (fIdealDist - fDist) / (fIdealDist - fTouchingDist)
				
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					fDesiredPlaybackSpeed = 1.0 + (fDistRatio * 2.0)
				ELSE
					fDesiredPlaybackSpeed = 1.0 + (fDistRatio)
				ENDIF
			ENDIF
		ELSE 
			//Player is in front, speed up trigger car based on the player's own speed.
			IF fDist > fMaxDistInFront
				fDist = fMaxDistInFront
			ENDIF
			fDistRatio = fDist / fMaxDistInFront
			
			//If the player is already going slower then don't speed up the buddy as much.
			IF fSpeedDiff > 0.0
				fDistRatio = fDistRatio * 0.5
			ENDIF
			
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				fDesiredPlaybackSpeed = 2.0 + fDistRatio
			ELSE
				IF GET_ENTITY_SPEED(vehPlayer) < 5.0 //If the player has slowed down slow the buddy down so it's not as harsh if they plough through.
				AND fSpeedDiff > 0.0
					fDesiredPlaybackSpeed = 0.6 + fDistRatio
				ELSE
					fDesiredPlaybackSpeed = 1.0 + fDistRatio
				ENDIF
			ENDIF
		ENDIF
		
		//Special cases where desired speed is forced to a specific value.
		IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
			fDesiredPlaybackSpeed = 1.0
		ENDIF
		
		IF fCurrentPlaybackTime < 19000.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2013.521484,-410.929047,10.103262>>, <<-2005.116699,-399.659576,15.681062>>, 14.000000)
				IF fDesiredPlaybackSpeed < 1.0
					fDesiredPlaybackSpeed = 1.0
				ENDIF
			ENDIF
		ENDIF
		
		//Fix for 504332: if the player tries to take a shortcut down the alley make sure Lamar isn't slowed down.
		IF fCurrentPlaybackTime > 65000.0 AND fCurrentPlaybackTime < 78000.0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1327.701172,-648.209534,35.882713>>, <<-1196.130981,-817.147583,13.715324>>, 33.750000)
				IF fCurrentPlaybackTime < 75000.0
					fDesiredPlaybackSpeed = 1.5
				ELSE
					fDesiredPlaybackSpeed = 1.2
				ENDIF
			ENDIF	
		ENDIF
		
		//For the jump section don't allow the car speed to go too low.
		IF fCurrentPlaybackTime > 86500.0 AND fCurrentPlaybackTime < 96500.0
			IF fDesiredPlaybackSpeed < 0.7
				fDesiredPlaybackSpeed = 0.7
			ENDIF
		ENDIF
		
		//Don't do any slowdown after going past the cutscene trigger.
		IF fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 4000.0
			IF fDesiredPlaybackSpeed < 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ELIF fCurrentPlaybackTime > 137400.0
			//Cap the max speed during the final tunnels: this gives the player the chance to overtake.
			IF fDesiredPlaybackSpeed > 1.0
				fDesiredPlaybackSpeed = 1.0
			ENDIF
		ENDIF
		
		//If the trigger speed is trying to catch up from really slow then increase the acceleration.
		IF fCurrentSpeed < 1.0
		AND fDesiredPlaybackSpeed > fCurrentSpeed
			fAccel *= 2.0
		ENDIF
		
		//Modify the player's speed further based on the desired speed.
		IF fDesiredPlaybackSpeed > 1.0
			fPlayersSpeedModifier = fPlayersSpeedModifier - ((fDesiredPlaybackSpeed - 1.0) * 10.0)
		ENDIF
		
		
		//Make the speed changes.
		MODIFY_VEHICLE_TOP_SPEED(sMainCars[iPlayersCar].veh, fPlayersSpeedModifier)
		fCurrentSpeed = fCurrentSpeed +@ (((fDesiredPlaybackSpeed - fCurrentSpeed) * fAccel) * 30.0)
		
		#IF IS_DEBUG_BUILD
			IF bDebugShowChaseStats	
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDist = "Distance: "
				strDist += GET_STRING_FROM_FLOAT(fDist)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", strDist)
			
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDesiredSpeed = "Desired playback speed: "
				strDesiredSpeed += GET_STRING_FROM_FLOAT(fDesiredPlaybackSpeed)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.15, "STRING", strDesiredSpeed)
			
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strSpeed = "Playback speed: "
				strSpeed += GET_STRING_FROM_FLOAT(fCurrentSpeed)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.2, "STRING", strSpeed)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strTime = "Time: "
				strTime += GET_STRING_FROM_FLOAT(fCurrentPlaybackTime)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.25, "STRING", strTime)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDiff = "Speed Diff: "
				strDiff += GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(vehTrigger) - GET_ENTITY_SPEED(vehPlayer))
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.3, "STRING", strDiff)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strState = "Trigger car state: "
				IF vOffset.y < 0.0 
					strState += "in front and "
				ELSE
					strState += "behind and "
				ENDIF
				
				IF IS_ENTITY_ON_SCREEN(vehTrigger)
					strState += "on screen."
				ELSE
					strState += "off screen."
				ENDIF
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.35, "STRING", strState)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strAccel = "Accel: "
				strAccel += GET_STRING_FROM_FLOAT(fAccel)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.4, "STRING", strAccel)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strModifier = "Player speed % modifier: "
				strModifier += GET_STRING_FROM_FLOAT(fPlayersSpeedModifier)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.45, "STRING", strModifier)
				
				SET_TEXT_SCALE(0.5, 0.5)
				SET_TEXT_DROP_SHADOW()
				TEXT_LABEL_63 strDuration = "Total duration: "
				strDuration += GET_STRING_FROM_FLOAT(GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec))
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.5, "STRING", strDuration)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

//Lamar's vehicle lights are forced on at difficult turns
PROC MANAGE_LAMARS_CAR_LIGHTS(VEHICLE_INDEX &veh, FLOAT fPlaybackTime)
	IF IS_VEHICLE_DRIVEABLE(veh)
		#IF IS_DEBUG_BUILD
			BOOL bBrakesOn = FALSE
			BOOL bLeftIndicatorOn = FALSE
			BOOL bRightIndicatorOn = FALSE
		#ENDIF
	
		//Brake lights
		IF (fPlaybackTime > 10000.0 AND fPlaybackTime < 12500.0)
		OR (fPlaybackTime > 23000.0 AND fPlaybackTime < 25000.0)
		OR (fPlaybackTime > 42500.0 AND fPlaybackTime < 45000.0)
		OR (fPlaybackTime > 48000.0 AND fPlaybackTime < 50500.0)
		OR (fPlaybackTime > 60500.0 AND fPlaybackTime < 63500.0)
		OR (fPlaybackTime > 78750.0 AND fPlaybackTime < 80500.0)
		OR (fPlaybackTime > 95750.0 AND fPlaybackTime < 98000.0)
		OR (fPlaybackTime > 110750.0 AND fPlaybackTime < 113000.0)
		OR (fPlaybackTime > 119000.0 AND fPlaybackTime < 121000.0)
		OR (fPlaybackTime > 123750.0 AND fPlaybackTime < 126000.0)
			SET_VEHICLE_BRAKE_LIGHTS(veh, TRUE)
			
			#IF IS_DEBUG_BUILD
				bBrakesOn = TRUE
			#ENDIF
		ENDIF
		
		//Right indicator
		IF (fPlaybackTime > 10000.0 AND fPlaybackTime < 12500.0)
		OR (fPlaybackTime > 42500.0 AND fPlaybackTime < 45000.0)
		OR (fPlaybackTime > 48000.0 AND fPlaybackTime < 51000.0)
		OR (fPlaybackTime > 71250.0 AND fPlaybackTime < 73200.0)
		OR (fPlaybackTime > 74250.0 AND fPlaybackTime < 76000.0)
		OR (fPlaybackTime > 110750.0 AND fPlaybackTime < 113500.0)
		OR (fPlaybackTime > 119000.0 AND fPlaybackTime < 121250.0)
			SET_VEHICLE_INDICATOR_LIGHTS(veh, FALSE, TRUE)
			
			#IF IS_DEBUG_BUILD
				bRightIndicatorOn = TRUE
			#ENDIF
		ELSE
			SET_VEHICLE_INDICATOR_LIGHTS(veh, FALSE, FALSE)
		ENDIF
		
		//Left indicator
		IF (fPlaybackTime > 23000.0 AND fPlaybackTime < 25000.0)
		OR (fPlaybackTime > 61000.0 AND fPlaybackTime < 64000.0)
		OR (fPlaybackTime > 78500.0 AND fPlaybackTime < 81000.0)
		OR (fPlaybackTime > 95750.0 AND fPlaybackTime < 98500.0)
		OR (fPlaybackTime > 115000.0 AND fPlaybackTime < 115750.0)
		OR (fPlaybackTime > 123500.0 AND fPlaybackTime < 126500.0)
			SET_VEHICLE_INDICATOR_LIGHTS(veh, TRUE, TRUE)
			
			#IF IS_DEBUG_BUILD
				bLeftIndicatorOn = TRUE
			#ENDIF
		ELSE
			SET_VEHICLE_INDICATOR_LIGHTS(veh, TRUE, FALSE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bBrakesOn
				DRAW_STRING_TO_DEBUG_DISPLAY("BRAKES ON")
			ENDIF
			
			IF bLeftIndicatorOn
				DRAW_STRING_TO_DEBUG_DISPLAY("LEFT INDICATOR ON")
			ENDIF
			
			IF bRightIndicatorOn
				DRAW_STRING_TO_DEBUG_DISPLAY("RIGHT INDICATOR ON")
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC CHANGE_ACTION_CAM_ANGLE(VEHICLE_INDEX vehCamCar, INT iNewCamRec, FLOAT fPlaybackTime)
	IF IS_VEHICLE_DRIVEABLE(vehCamCar)
		STOP_PLAYBACK_RECORDED_VEHICLE(vehCamCar)
		START_PLAYBACK_RECORDED_VEHICLE(vehCamCar, iNewCamRec, strCarrec)
		SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCamCar, fPlaybackTime)
		FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCamCar)
	ENDIF
ENDPROC
	
PROC UPDATE_CHASE_CAM(VEHICLE_INDEX &vehCamCar, CAMERA_INDEX &cam, FLOAT fPlaybackTime)
	//The cam FOV has to be updated the frame after setting the new recording
	IF fNextActionCamFov != 0.0
		SET_CAM_FOV(cam, fNextActionCamFov)
		fNextActionCamFov = 0.0
	ENDIF

	SWITCH iCurrentCinematicCam
		CASE 0
			IF fPlaybackTime > 0.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 1
			IF fPlaybackTime > 8400.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 2
			IF fPlaybackTime > 11500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[2], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 3
			IF fPlaybackTime > 15000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 4
			IF fPlaybackTime > 19000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 5
			IF fPlaybackTime > 24000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[3], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 6
			IF fPlaybackTime > 29000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 7
			IF fPlaybackTime > 32000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 38.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 8
			IF fPlaybackTime > 38000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 9
			IF fPlaybackTime > 41000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[2], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 10
			IF fPlaybackTime > 45500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 32.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 11
			IF fPlaybackTime > 48000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 12
			IF fPlaybackTime > 50300.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[2], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 13
			IF fPlaybackTime > 56000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 14
			IF fPlaybackTime > 62000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[2], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 15
			IF fPlaybackTime > 64500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[0], fPlaybackTime)
				fNextActionCamFov = 31.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 16
			IF fPlaybackTime > 70000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 35.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 17
			IF fPlaybackTime > 72500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 18
			IF fPlaybackTime > 78500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 34.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 19
			IF fPlaybackTime > 86100.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 20
			IF fPlaybackTime > 90000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[3], fPlaybackTime)
				fNextActionCamFov = 12.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 21
			IF fPlaybackTime > 95000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 22
			IF fPlaybackTime > 98700.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[1], fPlaybackTime)
				fNextActionCamFov = 30.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 23
			IF fPlaybackTime > 101000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 27.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 24
			IF fPlaybackTime > 104500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[3], fPlaybackTime)
				fNextActionCamFov = 22.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 25
			IF fPlaybackTime > 108500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 26
			IF fPlaybackTime > 114000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 27
			IF fPlaybackTime > 119000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[3], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 28
			IF fPlaybackTime > 122000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 29
			IF fPlaybackTime > 125000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 30
			IF fPlaybackTime > 132500.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 31
			IF fPlaybackTime > 143000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[5], fPlaybackTime)
				fNextActionCamFov = 45.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
		
		CASE 32
			IF fPlaybackTime > 147000.0
				CHANGE_ACTION_CAM_ANGLE(vehCamCar, iCamRecs[4], fPlaybackTime)
				fNextActionCamFov = 25.0
				iCurrentCinematicCam++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
	

PROC DO_CHASE_CAM(VEHICLE_INDEX veh1, VEHICLE_INDEX veh2)
	IF IS_PHONE_ONSCREEN()
		iCineCamPhoneTimer = GET_GAME_TIMER()
	ENDIF

	//Check to see if the hint cam is interping (NOTE: taken from chase_hint_cam.sch, as it needs to work in conjunction with the cinematic)
	IF localChaseHintCamStruct.bHintInterpingIn AND IS_GAMEPLAY_HINT_ACTIVE()
		IF GET_GAME_TIMER() > localChaseHintCamStruct.iInterpInTimer + iCHASE_HINT_INTERP_IN_TIME
			localChaseHintCamStruct.bHintInterpingIn = FALSE			
		ENDIF
	ENDIF

	IF NOT DOES_ENTITY_EXIST(vehCameraTest)
		REQUEST_MODEL(modelCamCar)
		REQUEST_CAM_RECORDING(iCamRecs[0], strCarrec)
		REQUEST_CAM_RECORDING(iCamRecs[1], strCarrec)
		REQUEST_CAM_RECORDING(iCamRecs[2], strCarrec)
		REQUEST_CAM_RECORDING(iCamRecs[3], strCarrec)
		REQUEST_CAM_RECORDING(iCamRecs[4], strCarrec)
		REQUEST_CAM_RECORDING(iCamRecs[5], strCarrec)
		REQUEST_CAM_RECORDING(iCamRecs[6], strCarrec)
		
		IF HAS_MODEL_LOADED(modelCamCar)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[0], strCarrec)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[1], strCarrec)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[2], strCarrec)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[3], strCarrec)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[4], strCarrec)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[5], strCarrec)
		AND HAS_CAM_RECORDING_LOADED(iCamRecs[6], strCarrec)
		AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS() //Don't create the cam until the interp to gameplay from the start cutscene is complete.
			vehCameraTest = CREATE_VEHICLE(modelCamCar, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0.0, 0.0, 5.0>>)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelCamCar)
			
			camChase = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)	
			//SHAKE_CAM(camChase, "SKY_DIVING_SHAKE", 0.1)
			SHAKE_CAM(camChase, "HAND_SHAKE", 1.0)
			SET_CAM_FOV(camChase, 25.0)
			
			//START_CAM_PLAYBACK(camChase, vehCameraTest, strCarrec, iCamRecs[0])
			
			//NEW CAM: attach to a single recording playing in fron of the trigger.
			//ATTACH_CAM_TO_ENTITY(camChase, veh1, <<-1.7329, 6.4243, 0.1649>>)
			ATTACH_CAM_TO_ENTITY(camChase, vehCameraTest, <<0.0, 0.0, 0.8>>, FALSE)

			SET_ENTITY_COLLISION(vehCameraTest, FALSE)
			SET_ENTITY_INVINCIBLE(vehCameraTest, TRUE)
			SET_ENTITY_VISIBLE(vehCameraTest, FALSE)
			START_PLAYBACK_RECORDED_VEHICLE(vehCameraTest, iCamRecs[6], strCarrec)
			SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCameraTest, fCurrentPlaybackTime * 1.4286)
			SET_PLAYBACK_SPEED(vehCameraTest, fCurrentPlaybackSpeed * 1.4286)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCameraTest)
			
			fNextActionCamFov = 0.0
			iCurrentCinematicCam = 0
		ENDIF
	ELSE
		IF IS_VEHICLE_DRIVEABLE(vehCameraTest)
			//NEW CAM: set playback speed.
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCameraTest)
				SET_PLAYBACK_SPEED(vehCameraTest, fCurrentPlaybackSpeed * 1.4286) //Recorded at 0.7
			ENDIF
		
			//UPDATE_CHASE_CAM(vehCameraTest, camChase, fCurrentPlaybackTime)
			//UPDATE_CAM_PLAYBACK(camChase, vehCameraTest, fCurrentPlaybackSpeed)
		ENDIF			
		
		//NEW CAM: point at a coord that's between the trigger and the player, so both appear on screen.
		VECTOR vPlayerPos = GET_ENTITY_COORDS(veh2)
		VECTOR vTriggerPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh1, <<0.0, 2.0, 0.0>>)
		VECTOR vPointPos = vTriggerPos + ((vPlayerPos - vTriggerPos) * 0.5)
		
		STOP_CAM_POINTING(camChase)
		POINT_CAM_AT_COORD(camChase, vPointPos)
	ENDIF

	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND fCurrentPlaybackTime > 6500.0 //Don't run during the transition from the chase start cutscene, as it can screw up the interp to game cam.
	AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		#IF IS_DEBUG_BUILD	SCRIPT_ASSERT("PRIVATE_IsChaseHintCamButtonPressedInVehicle")	#ENDIF
		IF IS_VEHICLE_DRIVEABLE(veh1)
		AND IS_VEHICLE_DRIVEABLE(veh2)
		AND PRIVATE_IsChaseHintCamButtonPressedInVehicle()
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), veh2)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_CAMHELP")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_HINTHELP")
					CLEAR_HELP()
				ENDIF
			
				VECTOR v_driver_pos = GET_ENTITY_COORDS(veh2)
				VECTOR vTriggerPos = GET_ENTITY_COORDS(veh1)
	
				FLOAT fDist = VDIST2(v_driver_pos, vTriggerPos)
				IF fDist < 2500.0 //50m
					IF NOT IS_GAMEPLAY_HINT_ACTIVE() AND NOT localChaseHintCamStruct.bHintInterpingBack
						IF NOT IS_PHONE_ONSCREEN()
							IF GET_GAME_TIMER() - iCineCamPhoneTimer > 1000
								IF IS_VEHICLE_DRIVEABLE(vehCameraTest)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCameraTest)
										IF DOES_CAM_EXIST(camChase)
											IF NOT IS_CAM_RENDERING(camChase)
												//INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_START()
												SET_CAM_ACTIVE(camChase, TRUE)
												RENDER_SCRIPT_CAMS(TRUE, FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//If the trigger car is too far away, use a chase hint cam instead of the cinematic cam.
					IF DOES_CAM_EXIST(camChase)
						IF IS_CAM_RENDERING(camChase)
							//INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
						ENDIF
					ENDIF
				
					IF NOT IS_PHONE_ONSCREEN()
						IF GET_GAME_TIMER() - iCineCamPhoneTimer > 1000
							IF NOT IS_GAMEPLAY_HINT_ACTIVE()
								StartVehicleChaseHintCam(veh1)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Clean up cinematic cam
			IF DOES_CAM_EXIST(camChase)
				IF IS_CAM_RENDERING(camChase)
					//INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
			ENDIF
			
			//Clean up chase cam (NOTE: taken from chase_hint_cam.sch, as it needs to be integrated with the action cam)
			IF IS_GAMEPLAY_HINT_ACTIVE()
				IF NOT localChaseHintCamStruct.bHintInterpingBack AND NOT localChaseHintCamStruct.bHintInterpingIn
					STOP_GAMEPLAY_HINT()
					SET_GAMEPLAY_ENTITY_HINT(veh1, <<0,0,0>>, TRUE, 0, 0, 500)		
					
					localChaseHintCamStruct.bHintInterpingBack = TRUE
				ENDIF
			ELSE
				IF localChaseHintCamStruct.bHintInterpingBack
					SET_CINEMATIC_BUTTON_ACTIVE(TRUE)

					localChaseHintCamStruct.bHintInterpingBack = FALSE
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles additional set-pieces that occur during the main chase (e.g. peds walking)
PROC MANAGE_CHASE_SET_PIECES()
	BOOL bCreatedAnEntityThisFrame = FALSE
	BOOL bRemovedAnEntityThisFrame = FALSE
	//SEQUENCE_INDEX seq
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vLamarPos
	IF NOT IS_PED_INJURED(sLamar.ped)
		vLamarPos = GET_ENTITY_COORDS(sLamar.ped)
	ENDIF

	//NOTE: instead of cycling through all setpiece cars, keep constants of the particular setpieces we want to modify. This saves time
	//but needs maintenance if the setpiece array is changed for some reason.
	CONST_INT PACKER_SETPIECE_1 	15
	CONST_INT VAN_REVERSE_SETPIECE	33
	CONST_INT TRAILER_SETPIECE		28
	CONST_INT CYCLIST_SETPIECE_1	9	
	CONST_INT CYCLIST_SETPIECE_2	10	
	CONST_INT CYCLIST_SETPIECE_3	11	
	CONST_INT BUS_SETPIECE			32
	CONST_INT LIGHT_TRUCK_SETPIECE	14
	CONST_INT HELI_TRAFFIC_ID 		91
	CONST_INT STOCKADE_PARKED_ID	14
	
	//have the cyclists flee if the player crashes into any of them.
	IF NOT bCyclistsSetToFlee
		BOOL bSpookedACyclist = FALSE
		PED_INDEX pedCyclists[3]
	
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_1])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(SetPieceCarID[CYCLIST_SETPIECE_1], PLAYER_PED_ID())
				bSpookedACyclist = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_2])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(SetPieceCarID[CYCLIST_SETPIECE_2], PLAYER_PED_ID())
				bSpookedACyclist = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_3])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(SetPieceCarID[CYCLIST_SETPIECE_3], PLAYER_PED_ID())
				bSpookedACyclist = TRUE
			ENDIF
		ENDIF
		
		If bSpookedACyclist
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_1])
				pedCyclists[0] = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[CYCLIST_SETPIECE_1])
				
				IF NOT IS_PED_INJURED(pedCyclists[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[CYCLIST_SETPIECE_1])
						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[CYCLIST_SETPIECE_1])
					ENDIF
				
					TASK_VEHICLE_MISSION_PED_TARGET(pedCyclists[0], SetPieceCarID[CYCLIST_SETPIECE_1], PLAYER_PED_ID(), MISSION_FLEE, 70.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 300.0, 300.0, TRUE)
					SET_PED_KEEP_TASK(pedCyclists[0], TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_2])
				pedCyclists[1] = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[CYCLIST_SETPIECE_2])
				
				IF NOT IS_PED_INJURED(pedCyclists[1])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[CYCLIST_SETPIECE_2])
						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[CYCLIST_SETPIECE_2])
					ENDIF
				
					TASK_VEHICLE_MISSION_PED_TARGET(pedCyclists[1], SetPieceCarID[CYCLIST_SETPIECE_2], PLAYER_PED_ID(), MISSION_FLEE, 70.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 300.0, 300.0, TRUE)
					SET_PED_KEEP_TASK(pedCyclists[1], TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_3])
				pedCyclists[2] = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[CYCLIST_SETPIECE_3])
				
				IF NOT IS_PED_INJURED(pedCyclists[2])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[CYCLIST_SETPIECE_3])
						STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[CYCLIST_SETPIECE_3])
					ENDIF
				
					TASK_VEHICLE_MISSION_PED_TARGET(pedCyclists[2], SetPieceCarID[CYCLIST_SETPIECE_3], PLAYER_PED_ID(), MISSION_FLEE, 70.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 300.0, 300.0, TRUE)
					SET_PED_KEEP_TASK(pedCyclists[2], TRUE)
				ENDIF
			ENDIF
			
			bCyclistsSetToFlee = TRUE
		ENDIF
	ENDIF
	
	//Cyclist rings bell when player drives past.
	IF SetPieceCarModel[CYCLIST_SETPIECE_1] = CRUISER
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[CYCLIST_SETPIECE_1])
			IF NOT bPlayedCyclistBellSound
				IF VDIST2(GET_ENTITY_COORDS(SetPieceCarID[CYCLIST_SETPIECE_1]), vPlayerPos) < 100.0
					PLAY_SOUND_FROM_ENTITY(-1, "Bike_Bell", SetPieceCarID[CYCLIST_SETPIECE_1], "ARM_1_SOUNDSET")
					bPlayedCyclistBellSound = TRUE
				ENDIF
			ENDIF
		ELSE
			bPlayedCyclistBellSound = FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Armenian 1 - Cyclist setpiece has changed ID, please update script to match.")
		#ENDIF
	ENDIF
	
	//First packer set-piece: just drives by
	IF SetPieceCarModel[PACKER_SETPIECE_1] = PACKER
		IF SetPieceCarProgress[PACKER_SETPIECE_1] < 99
		AND fCurrentPlaybackTime < SetPieceCarStartime[PACKER_SETPIECE_1] + 10000.0 
			IF fCurrentPlaybackTime > SetPieceCarStartime[PACKER_SETPIECE_1] - 10000.0
			AND NOT DOES_ENTITY_EXIST(vehTanker)
				REQUEST_MODEL(TANKER)
			ENDIF
			
			REQUEST_VEHICLE_RECORDING(CARREC_TRAILER_2, strCarrec)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[PACKER_SETPIECE_1])
			IF NOT DOES_ENTITY_EXIST(vehTanker)
				IF fCurrentPlaybackTime < SetPieceCarStartime[PACKER_SETPIECE_1] + 10000.0
					REQUEST_MODEL(TANKER)
				
					IF HAS_MODEL_LOADED(TANKER)
					AND NOT bCreatedAnEntityThisFrame
						vehTanker = CREATE_VEHICLE(TANKER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SetPieceCarID[PACKER_SETPIECE_1], <<0.0, -10.0, 0.0>>), 
													GET_ENTITY_HEADING(SetPieceCarID[PACKER_SETPIECE_1]))
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehTanker, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(SetPieceCarID[PACKER_SETPIECE_1], TRUE)
						ATTACH_VEHICLE_TO_TRAILER(SetPieceCarID[PACKER_SETPIECE_1], vehTanker)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(TANKER)
						bCreatedAnEntityThisFrame = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[PACKER_SETPIECE_1])
					//Control trailer playback.
					IF NOT IS_ENTITY_DEAD(vehTanker)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTanker)
							REQUEST_VEHICLE_RECORDING(CARREC_TRAILER_2, strCarrec)
						
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_TRAILER_2, strCarrec)
								START_PLAYBACK_RECORDED_VEHICLE(vehTanker, CARREC_TRAILER_2, strCarrec)
								SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTanker, GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[PACKER_SETPIECE_1]))
							ENDIF
						ELSE
							SET_PLAYBACK_SPEED(vehTanker, fCurrentPlaybackSpeed)
						ENDIF
					ENDIF
					
					//Truck horn: activates when the player gets close
					IF NOT bTruckHornActivated
					AND (VDIST2(vPlayerPos, GET_ENTITY_COORDS(SetPieceCarID[PACKER_SETPIECE_1])) < 625.0 //25m
					OR VDIST2(vLamarPos, GET_ENTITY_COORDS(SetPieceCarID[PACKER_SETPIECE_1])) < 400.0) //20m
						START_VEHICLE_HORN(SetPieceCarID[PACKER_SETPIECE_1], 3000, HASH("HELDDOWN"))
						bTruckHornActivated = TRUE
					ENDIF
				ELSE
					//Clean up as soon as possible to save memory
					IF fCurrentPlaybackTime > SetPieceCarStartime[PACKER_SETPIECE_1] + 10000.0
					AND (NOT IS_ENTITY_ON_SCREEN(SetPieceCarID[PACKER_SETPIECE_1]) OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
						REMOVE_VEHICLE(SetPieceCarID[PACKER_SETPIECE_1])
						REMOVE_VEHICLE(vehTanker)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(SetPieceCarID[PACKER_SETPIECE_1])
			IF DOES_ENTITY_EXIST(vehTanker)
				REMOVE_VEHICLE(vehTanker)
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Armenian 1 - First packer setpiece has changed ID, please update script to match.")
		#ENDIF
	ENDIF

	//Bus horn
	IF SetPieceCarModel[BUS_SETPIECE] = BUS
		IF NOT IS_ENTITY_DEAD(SetPieceCarID[BUS_SETPIECE])
			IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(SetPieceCarID[BUS_SETPIECE])) < 900.0 //30m
				IF NOT bBusHornActivated
					START_VEHICLE_HORN(SetPieceCarID[BUS_SETPIECE], 3000, HASH("HELDDOWN"))
					
					IF iBusLightsTimer = 0
						SET_VEHICLE_LIGHTS(SetPieceCarID[BUS_SETPIECE], FORCE_VEHICLE_LIGHTS_ON)
						iBusLightsTimer = GET_GAME_TIMER()
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 2.0, REPLAY_IMPORTANCE_NORMAL) //Record footage of the player passing the bus.
					
					bBusHornActivated = TRUE
				ENDIF
			ENDIF
			
			//Turn the lights off after 2 seconds
			IF bBusHornActivated AND iBusLightsTimer != 0
				IF GET_GAME_TIMER() - iBusLightsTimer > 2000
					SET_VEHICLE_LIGHTS(SetPieceCarID[BUS_SETPIECE], FORCE_VEHICLE_LIGHTS_OFF)
					iBusLightsTimer = 0
				ENDIF
			ENDIF
		ELSE
			bBusHornActivated = FALSE
			iBusLightsTimer = 0
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Armenian 1 - bus setpiece has changed ID, please update script to match.")
		#ENDIF
	ENDIF
	
	
	IF SetPieceCarModel[VAN_REVERSE_SETPIECE] = BURRITO3
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[VAN_REVERSE_SETPIECE])
			//NG 1651537 - Override the colour.
			IF NOT bOverriddenVanColour
				SET_VEHICLE_COLOURS(SetPieceCarID[VAN_REVERSE_SETPIECE], 111, 111)
				SET_VEHICLE_EXTRA_COLOURS(SetPieceCarID[VAN_REVERSE_SETPIECE], 111, 111)
				bOverriddenVanColour = TRUE
			ENDIF
		
			//Van horn
			IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(SetPieceCarID[VAN_REVERSE_SETPIECE])) < 100.0
				IF NOT bVanHornActivated
					START_VEHICLE_HORN(SetPieceCarID[VAN_REVERSE_SETPIECE], 2000, HASH("HELDDOWN"))
					
					bVanHornActivated = TRUE
				ENDIF
			ENDIF
		ELSE
			bVanHornActivated = FALSE
			bOverriddenVanColour = FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Armenian 1 - van setpiece has changed ID, please update script to match.")
		#ENDIF
	ENDIF
	
	//Request: change one of the trucks colours to make it stand out
	IF SetPieceCarModel[LIGHT_TRUCK_SETPIECE] = BENSON
		IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[LIGHT_TRUCK_SETPIECE])
			IF NOT bChangedTruckColour
				SET_VEHICLE_COLOURS(SetPieceCarID[LIGHT_TRUCK_SETPIECE], 4, 4)
				SET_VEHICLE_EXTRA_COLOURS(SetPieceCarID[LIGHT_TRUCK_SETPIECE], 4, 4)
				bChangedTruckColour = TRUE
			ENDIF
		ELSE
			bChangedTruckColour = FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Armenian 1 - Light truck setpiece has changed ID, please update script to match.")
		#ENDIF
	ENDIF

	
	//Helicopter: make sure the rotor blades are turning, also force colour (TODO 486984)
	IF TrafficCarModel[HELI_TRAFFIC_ID] = MAVERICK
		IF IS_VEHICLE_DRIVEABLE(TrafficCarID[HELI_TRAFFIC_ID])
			IF NOT bTrafficHeliRotorsOn
				SET_VEHICLE_ENGINE_ON(TrafficCarID[HELI_TRAFFIC_ID], TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(TrafficCarID[HELI_TRAFFIC_ID])
				SET_VEHICLE_COLOURS(TrafficCarID[HELI_TRAFFIC_ID], 128, 0)
				SET_VEHICLE_EXTRA_COLOURS(TrafficCarID[HELI_TRAFFIC_ID], 128, 0)
				bTrafficHeliRotorsOn = TRUE
			ENDIF
		ELSE
			bTrafficHeliRotorsOn = FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Armenian 1 - Helicopter traffic has changed ID, please update script to match.")
		#ENDIF
	ENDIF
	
	//Studio set-piece: Aliens
	//For component variations, 0 =  alien, 1 = normal.
	INT i = 0
	BOOL bCheckedTrigger = FALSE
	BOOL bTriggerAliens = FALSE
	BOOL bPlayAliensDialogue = FALSE
	FLOAT fDistFromPlayer = 0.0
	VECTOR vInitialOffset
	VECTOR vInitialRotation
	TEXT_LABEL_63 strLoopAnim
	TEXT_LABEL_63 strRunAnim
	
	MODEL_NAMES modelFilmCrew = S_M_M_MOVALIEN_01
		
	IF fCurrentPlaybackTime < 43000.0
		REPEAT COUNT_OF(sAliens) i
			sAliens[i].iEvent = 0
		ENDREPEAT
		
		REPEAT COUNT_OF(sAliensGroup2) i
			sAliensGroup2[i].iEvent = 0
		ENDREPEAT
	ELSE
		REPEAT COUNT_OF(sAliens) i	
			IF i = 0
				strRunAnim = "largegroup_flee_r_f_a"
			ELIF i = 1
				strRunAnim = "largegroup_flee_l_f_b"
			ELIF i = 2
				strRunAnim = "largegroup_flee_l_m_c"
			ELIF i = 3
				strRunAnim = "largegroup_flee_r_m_d"
			ENDIF
		
			IF sAliens[i].iEvent = 0
				IF fCurrentPlaybackTime < 55000.0
					REQUEST_MODEL(modelFilmCrew)
					REQUEST_ANIM_DICT(strAlienAnims)
		
					IF HAS_MODEL_LOADED(modelFilmCrew)
					AND HAS_ANIM_DICT_LOADED(strAlienAnims)
					AND NOT bCreatedAnEntityThisFrame //Only create one entity per frame (to fix huge peaks in CPU usage)
						IF i = 0
							strLoopAnim = "largegroup_loop_f_a"
						ELIF i = 1
							strLoopAnim = "largegroup_loop_f_b"
						ELIF i = 2
							strLoopAnim = "largegroup_loop_m_c"
						ELIF i = 3
							strLoopAnim = "largegroup_loop_m_d"
						ENDIF
						
						vInitialOffset = GET_ANIM_INITIAL_OFFSET_POSITION(strAlienAnims, strLoopAnim, <<-1107.37, -504.73, 34.36>>, <<0.000, 0.000, 32.200>>)
						vInitialRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(strAlienAnims, strLoopAnim, <<-1107.37, -504.73, 34.36>>, <<0.000, 0.000, 32.200>>)
							
						sAliens[i].ped = CREATE_PED(PEDTYPE_MISSION, modelFilmCrew, vInitialOffset, vInitialRotation.z)
						SET_ENTITY_COORDS_NO_OFFSET(sAliens[i].ped, vInitialOffset)
						TASK_PLAY_ANIM(sAliens[i].ped, strAlienAnims, strLoopAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FREEZE_ENTITY_POSITION(sAliens[i].ped, TRUE)
						SET_ENTITY_COLLISION(sAliens[i].ped, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sAliens[i].ped, TRUE)
						
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sAliens[i].ped, TRUE) // rob - 2117273 - ensure player doesn't fail this stat unfairly
						
						//Add some of them for dialogue
						IF i = 2
							ADD_PED_FOR_DIALOGUE(sConversationPeds, 2, sAliens[i].ped, "EXTRA1")
						ELIF i = 3
							SET_MODEL_AS_NO_LONGER_NEEDED(modelFilmCrew)
							ADD_PED_FOR_DIALOGUE(sConversationPeds, 3, sAliens[i].ped, "EXTRA2")
						ENDIF
						
						//Randomise the outfits so some have alien heads/hands and some don't.
						SET_PED_COMPONENT_VARIATION(sAliens[i].ped, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(sAliens[i].ped, PED_COMP_TORSO, 0, 0)
						
						IF i % 2 = 0
							SET_PED_COMPONENT_VARIATION(sAliens[i].ped, PED_COMP_HEAD, 1, 0)
							SET_PED_COMPONENT_VARIATION(sAliens[i].ped, PED_COMP_HAND, 1, 0)
						ELSE
							SET_PED_COMPONENT_VARIATION(sAliens[i].ped, PED_COMP_HEAD, 0, 0)
							SET_PED_COMPONENT_VARIATION(sAliens[i].ped, PED_COMP_HAND, 0, 0)
						ENDIF
						
						sAliens[i].iEvent = 1
						sAliens[i].iTimer = 0
						
						bCreatedAnEntityThisFrame = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(sAliens[i].ped)
					fDistFromPlayer = VDIST2(vPlayerPos, GET_ENTITY_COORDS(sAliens[i].ped))
					
					IF fDistFromPlayer < 1600.0
						bPlayAliensDialogue = TRUE
					ENDIF
					
					IF sAliens[i].iEvent = 1
						FLOAT fTimeForPlayerToReachAliens, fTimeForLamarToReachAliens
					
						IF NOT bCheckedTrigger						
							IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
							AND IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
								fTimeForPlayerToReachAliens = VDIST(vPlayerPos, GET_ENTITY_COORDS(sAliens[i].ped)) / GET_ENTITY_SPEED(sMainCars[iPlayersCar].veh)
								fTimeForLamarToReachAliens = VDIST(vLamarPos, GET_ENTITY_COORDS(sAliens[i].ped)) / GET_ENTITY_SPEED(sMainCars[iBuddiesCar].veh)
						
								IF fTimeForPlayerToReachAliens < 2.0
									bTriggerAliens = TRUE
								ELIF fTimeForLamarToReachAliens < 3.0
									IF fCurrentPlaybackSpeed > 0.7
										bTriggerAliens = TRUE
									ENDIF
								ENDIF
								
								bCheckedTrigger = TRUE
							ENDIF
						ENDIF
						
						IF bTriggerAliens
							FREEZE_ENTITY_POSITION(sAliens[i].ped, FALSE)
							SET_ENTITY_COLLISION(sAliens[i].ped, TRUE)
							
							IF i = 0
								//Record footage of the player driving into the aliens.
//								IF fTimeForPlayerToReachAliens < 3.0
//									REPLAY_RECORD_BACK_FOR_TIME(2.5, 2.5, REPLAY_IMPORTANCE_HIGHEST) 
//								ENDIF
								
								TASK_PLAY_ANIM(sAliens[i].ped, strAlienAnims, strRunAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.05)
							ELIF i = 1
								TASK_PLAY_ANIM(sAliens[i].ped, strAlienAnims, strRunAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.05)
							ELIF i = 2
								TASK_PLAY_ANIM(sAliens[i].ped, strAlienAnims, strRunAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.05)
							ELIF i = 3
								TASK_PLAY_ANIM(sAliens[i].ped, strAlienAnims, strRunAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.05)
							ENDIF

							sAliens[i].iEvent++
						ENDIF
					ELIF sAliens[i].iEvent = 2
						IF IS_PED_RAGDOLL(sAliens[i].ped)
							sAliens[i].iEvent = 100
						ELSE
							IF NOT IS_ENTITY_PLAYING_ANIM(sAliens[i].ped, strAlienAnims, strRunAnim)
							OR GET_ENTITY_SPEED(sAliens[i].ped) < 0.1
								IF VDIST2(GET_ENTITY_COORDS(sAliens[i].ped), vPlayerPos) < 100.0
									sAliens[i].iEvent = 100
								ENDIF
							ENDIF
						ENDIF
					ELIF sAliens[i].iEvent = 100
						IF GET_SCRIPT_TASK_STATUS(sAliens[i].ped, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
							TASK_SMART_FLEE_PED(sAliens[i].ped, PLAYER_PED_ID(), 200.0, -1)
							SET_PED_KEEP_TASK(sAliens[i].ped, TRUE)
						ENDIF
					ENDIF
				ELSE
					IF sConversationPeds.PedInfo[2].Index = sAliens[i].ped
						REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 2)
					ELIF sConversationPeds.PedInfo[3].Index = sAliens[i].ped
						REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 3)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(sAliens[i].ped)
					IF (fCurrentPlaybackTime > 64000.0 OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
					AND NOT bRemovedAnEntityThisFrame
						IF NOT IS_PED_INJURED(sAliens[i].ped)
							TASK_SMART_FLEE_PED(sAliens[i].ped, PLAYER_PED_ID(), 200.0, -1)
							SET_PED_KEEP_TASK(sAliens[i].ped, TRUE)
						ENDIF
					
						REMOVE_PED(sAliens[i].ped, FALSE)
						bRemovedAnEntityThisFrame = TRUE
					ENDIF
				ELIF i = 0
					IF eMissionStage = STAGE_CHASE_MID_POINT
						REMOVE_ANIM_DICT(strAlienAnims)					
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF bPlayAliensDialogue
		AND NOT bHasTextLabelTriggered[ARM1_EXTRAS2]
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_PED_INJURED(sConversationPeds.PedInfo[2].Index)
				AND NOT IS_PED_INJURED(sConversationPeds.PedInfo[3].Index)
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_EXTRAS2", "ARM1_EXTRAS2_1", CONV_PRIORITY_MEDIUM)
						
						REPLAY_RECORD_BACK_FOR_TIME(2.5, 4.5, REPLAY_IMPORTANCE_HIGH) 
						
						bHasTextLabelTriggered[ARM1_EXTRAS2] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		bCheckedTrigger = FALSE
		bTriggerAliens = FALSE
		
		//Another pair of aliens further down.
		REPEAT COUNT_OF(sAliensGroup2) i	
			IF sAliensGroup2[i].iEvent = 0
				IF fCurrentPlaybackTime < 55000.0
					REQUEST_MODEL(modelFilmCrew)
					REQUEST_ANIM_DICT(strAlienAnims)
		
					IF HAS_MODEL_LOADED(modelFilmCrew)
					AND HAS_ANIM_DICT_LOADED(strAlienAnims)
					AND NOT bCreatedAnEntityThisFrame //Only create one entity per frame (to fix huge peaks in CPU usage)
						IF i = 0
							strLoopAnim = "smallgroup_loop_f_a"
						ELIF i = 1
							strLoopAnim = "smallgroup_loop_m_b"
						ENDIF
						
						vInitialOffset = GET_ANIM_INITIAL_OFFSET_POSITION(strAlienAnims, strLoopAnim, <<-1139.247, -524.062, 32.021>>, <<0.000, 0.000, 20.000>>)
						vInitialRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(strAlienAnims, strLoopAnim, <<-1139.247, -524.062, 32.021>>, <<0.000, 0.000, 20.000>>)
							
						sAliensGroup2[i].ped = CREATE_PED(PEDTYPE_MISSION, modelFilmCrew, vInitialOffset, vInitialRotation.z)
						SET_ENTITY_COORDS_NO_OFFSET(sAliensGroup2[i].ped, vInitialOffset)
						TASK_PLAY_ANIM(sAliensGroup2[i].ped, strAlienAnims, strLoopAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
						FREEZE_ENTITY_POSITION(sAliensGroup2[i].ped, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sAliensGroup2[i].ped, TRUE)
						
						//Add some of them for dialogue
						IF i = 1
							SET_MODEL_AS_NO_LONGER_NEEDED(modelFilmCrew)
						ENDIF
						
						//Randomise the outfits so some have alien heads/hands and some don't.
						SET_PED_COMPONENT_VARIATION(sAliensGroup2[i].ped, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(sAliensGroup2[i].ped, PED_COMP_TORSO, 0, 0)
						
						IF i % 2 = 0
							SET_PED_COMPONENT_VARIATION(sAliensGroup2[i].ped, PED_COMP_HEAD, 1, 0)
							SET_PED_COMPONENT_VARIATION(sAliensGroup2[i].ped, PED_COMP_HAND, 1, 0)
						ELSE
							SET_PED_COMPONENT_VARIATION(sAliensGroup2[i].ped, PED_COMP_HEAD, 0, 0)
							SET_PED_COMPONENT_VARIATION(sAliensGroup2[i].ped, PED_COMP_HAND, 0, 0)
						ENDIF
						
						sAliensGroup2[i].iEvent = 1
						sAliensGroup2[i].iTimer = 0
						
						bCreatedAnEntityThisFrame = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(sAliensGroup2[i].ped)
					IF i = 0
						strRunAnim = "smallgroup_flee_f_a"
					ELIF i = 1
						strRunAnim = "smallgroup_flee_m_b"
					ENDIF
					
					IF sAliensGroup2[i].iEvent = 1
						IF NOT bCheckedTrigger
						AND IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
							FLOAT fTimeToReachAliens = VDIST(vPlayerPos, GET_ENTITY_COORDS(sAliensGroup2[i].ped)) / GET_ENTITY_SPEED(sMainCars[iPlayersCar].veh)
						
							bTriggerAliens = fTimeToReachAliens < 1.8
							bCheckedTrigger = TRUE
						ENDIF
						
						IF bTriggerAliens
							FREEZE_ENTITY_POSITION(sAliensGroup2[i].ped, FALSE)
							TASK_PLAY_ANIM(sAliensGroup2[i].ped, strAlienAnims, strRunAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.05)
							
							sAliensGroup2[i].iEvent++
						ENDIF
					ELIF sAliensGroup2[i].iEvent = 2
						IF IS_PED_RAGDOLL(sAliensGroup2[i].ped)
							sAliensGroup2[i].iEvent = 100
						ELSE
							IF NOT IS_ENTITY_PLAYING_ANIM(sAliensGroup2[i].ped, strAlienAnims, strRunAnim)
								IF VDIST2(GET_ENTITY_COORDS(sAliensGroup2[i].ped), vPlayerPos) < 100.0
									sAliens[i].iEvent = 100
								ENDIF
							ELSE
								SET_ENTITY_ANIM_SPEED(sAliensGroup2[i].ped, strAlienAnims, strRunAnim, 0.95)
							ENDIF
						ENDIF
					ELIF sAliens[i].iEvent = 100
						IF GET_SCRIPT_TASK_STATUS(sAliensGroup2[i].ped, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
							TASK_SMART_FLEE_PED(sAliensGroup2[i].ped, PLAYER_PED_ID(), 200.0, -1)
							SET_PED_KEEP_TASK(sAliensGroup2[i].ped, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(sAliensGroup2[i].ped)
				AND (fCurrentPlaybackTime > 64000.0 OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
				AND NOT bRemovedAnEntityThisFrame
					IF NOT IS_PED_INJURED(sAliensGroup2[i].ped)
						TASK_SMART_FLEE_PED(sAliensGroup2[i].ped, PLAYER_PED_ID(), 200.0, -1)
						SET_PED_KEEP_TASK(sAliensGroup2[i].ped, TRUE)
					ENDIF
				
					REMOVE_PED(sAliensGroup2[i].ped, FALSE)
					bRemovedAnEntityThisFrame = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		
	ENDIF	
	
	//Track stat for hitting any of the alien peds.
	INT iUnarmedHits1, iUnarmedHits2, iUnarmedHits3, iUnarmedHitsTotal
	STAT_GET_INT(SP0_KILLS, iUnarmedHits1)
	STAT_GET_INT(SP1_KILLS, iUnarmedHits2)
	STAT_GET_INT(SP2_KILLS, iUnarmedHits3)
	iUnarmedHitsTotal = iUnarmedHits1 + iUnarmedHits2 + iUnarmedHits3
	
	IF NOT bAliensStatHasBeenSet
	AND sAliens[0].iEvent > 0
		IF iUnarmedHitsTotal > iPrevUnarmedHits
	
			PED_INDEX pedNearby[4]
			GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedNearby)
			
			REPEAT COUNT_OF(pedNearby) i
				IF DOES_ENTITY_EXIST(pedNearby[i])
					IF GET_ENTITY_MODEL(pedNearby[i]) = modelFilmCrew
						IF IS_PED_INJURED(pedNearby[i])
							IF VDIST2(GET_ENTITY_COORDS(pedNearby[i], FALSE), GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, FALSE)) < 100.0
								INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ARM1_HIT_ALIENS)
								bAliensStatHasBeenSet = TRUE
							ENDIF
						ELSE
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedNearby[i], PLAYER_PED_ID())
								INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ARM1_HIT_ALIENS)
								bAliensStatHasBeenSet = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		// Rob - 2117273 - also check damage
		INT iEventCount 
		STRUCT_ENTITY_ID sei 
		EVENT_NAMES eventType			
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventCount
			eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventCount)
			IF eventType = EVENT_ENTITY_DAMAGED
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventCount, sei, SIZE_OF(STRUCT_ENTITY_ID))
				IF DOES_ENTITY_EXIST(sei.EntityId)
					IF GET_ENTITY_MODEL(sei.EntityId) = modelFilmCrew
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ARM1_HIT_ALIENS)
						bAliensStatHasBeenSet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT	
	ENDIF
	
	iPrevUnarmedHits = iUnarmedHitsTotal
	
	//Security guards at movie lot
	IF fCurrentPlaybackTime < 20000.0
		sMovieGuards[0].iEvent = 0 //Chase has been restarted: reset the security guards.
		sMovieGuards[1].iEvent = 0 //Chase has been restarted: reset the security guards.
	ELIF fCurrentPlaybackTime > 43000.0
		VECTOR vStartPos
		FLOAT fStartHeading
		BOOL bTimeToTrigger
	
		REPEAT COUNT_OF(sMovieGuards) i
			SWITCH sMovieGuards[i].iEvent
				CASE 0 //Create the peds. 0 = the guard at the front entrance in the chase, 1 = the guard at the rear entrance.
					IF i = 0
						vStartPos = <<-1048.1337, -476.2388, 35.8050>>
						fStartHeading = 316.5925
					ELSE
						vStartPos = <<-1210.1215, -570.0335, 26.3435>>
						fStartHeading = 295.8782
					ENDIF
				
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStartPos) < 40000.0
						REQUEST_MODEL(modelMovieGuards)
						
						IF HAS_MODEL_LOADED(modelMovieGuards)
						AND NOT bCreatedAnEntityThisFrame
							sMovieGuards[i].ped = CREATE_PED(PEDTYPE_MISSION, modelMovieGuards, vStartPos, fStartHeading)
							TASK_LOOK_AT_ENTITY(sMovieGuards[i].ped, PLAYER_PED_ID(), -1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMovieGuards[i].ped, TRUE)
							SET_MODEL_AS_NO_LONGER_NEEDED(modelMovieGuards)
							sMovieGuards[i].iEvent++
							bCreatedAnEntityThisFrame = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE 1 //Put the guards into combat once the player or Lamar go into the studio.
					bTimeToTrigger = FALSE
					
					IF NOT IS_PED_INJURED(sMovieGuards[i].ped)
						IF i = 0
							IF NOT IS_PED_INJURED(sLamar.ped)
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sMovieGuards[i].ped)) < 900.0
								OR (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sLamar.ped)) < 900.0
								AND VDIST2(GET_ENTITY_COORDS(sMovieGuards[i].ped), GET_ENTITY_COORDS(sLamar.ped)) < 100.0)
									bTimeToTrigger = TRUE
								ENDIF
							ENDIF
						ELSE
							bTimeToTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sMovieGuards[i].ped)) < 6400.0
						ENDIF
						
						IF bTimeToTrigger
							PLAY_PED_AMBIENT_SPEECH(sMovieGuards[i].ped, "FIGHT", SPEECH_PARAMS_FORCE_NORMAL)
							
							sMovieGuards[i].iEvent++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2 //Check the combat task.
					IF NOT IS_PED_INJURED(sMovieGuards[i].ped)
						IF NOT IS_PED_IN_COMBAT(sMovieGuards[i].ped)
						AND GET_SCRIPT_TASK_STATUS(sMovieGuards[i].ped, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(sMovieGuards[i].ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
							TASK_COMBAT_PED(sMovieGuards[i].ped, PLAYER_PED_ID())
							SET_PED_KEEP_TASK(sMovieGuards[i].ped, TRUE)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			//Clean up if the ped dies or the player drives away.
			IF DOES_ENTITY_EXIST(sMovieGuards[i].ped)
				IF IS_PED_INJURED(sMovieGuards[i].ped)
					REMOVE_PED(sMovieGuards[i].ped)
				ELIF fCurrentPlaybackTime > 66000.0
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sMovieGuards[i].ped)) > 10000.0
						REMOVE_PED(sMovieGuards[i].ped)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//Smoker: delivery guy who stands in the alley smoking
	/*IF fCurrentPlaybackTime < 57000.0
		sSmoker.iEvent = 0
	ELSE
		MODEL_NAMES modelSmoker = S_M_M_UPS_01
		MODEL_NAMES modelCigarette = PROP_CS_CIGGY_01
		STRING strSmokerAnims = "missarmenian1"
	
		IF sSmoker.iEvent = 0
			IF fCurrentPlaybackTime < 66000.0
				REQUEST_MODEL(modelSmoker)
				REQUEST_MODEL(modelCigarette)
				REQUEST_ANIM_DICT(strSmokerAnims)
				
				IF HAS_MODEL_LOADED(modelSmoker)
				AND HAS_ANIM_DICT_LOADED(strSmokerAnims)
				AND NOT bCreatedAnEntityThisFrame
					sSmoker.ped = CREATE_PED(PEDTYPE_MISSION, modelSmoker, <<-1249.5641, -677.1760, 24.9935>>, 7.3495)
					TASK_PLAY_ANIM(sSmoker.ped, strSmokerAnims, "smoking_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					SET_MODEL_AS_NO_LONGER_NEEDED(modelSmoker)
					
					objCigarette = CREATE_OBJECT(modelCigarette, <<-1249.5641, -677.1760, 25.9935>>)
					ATTACH_ENTITY_TO_ENTITY(objCigarette, sSmoker.ped, GET_PED_BONE_INDEX(sSmoker.ped, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					SET_MODEL_AS_NO_LONGER_NEEDED(modelCigarette)
					
					sSmoker.iEvent = 1
					bCreatedAnEntityThisFrame = TRUE
				ENDIF
			ENDIF
		ELIF DOES_ENTITY_EXIST(sSmoker.ped)
			IF NOT bRemovedAnEntityThisFrame
			AND (fCurrentPlaybackTime > 80000.0 OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()) OR IS_PED_INJURED(sSmoker.ped))
				REMOVE_OBJECT(objCigarette)
				REMOVE_PED(sSmoker.ped)
				bRemovedAnEntityThisFrame = TRUE
			ENDIF
		ELSE
			REMOVE_ANIM_DICT(strSmokerAnims)
		ENDIF
	ENDIF*/
	
	
	//Birds: fly away as the cars drive past
	IF fCurrentPlaybackTime < 60000.0
		REPEAT COUNT_OF(sBirds) i
			sBirds[i].iEvent = 0
		ENDREPEAT
	ELSE
		MODEL_NAMES modelBird = A_C_SEAGULL
		CONST_FLOAT MAX_BIRD_SPEED		14.0
		STRING strBirdAnims = "creatures@gull@move"
		VECTOR vBirdPos
	
		REPEAT COUNT_OF(sBirds) i
			IF sBirds[i].iEvent = 0
				IF fCurrentPlaybackTime < 72000.0
					REQUEST_MODEL(modelBird)
					REQUEST_ANIM_DICT(strBirdAnims)
					
					IF HAS_MODEL_LOADED(modelBird)
					AND HAS_ANIM_DICT_LOADED(strBirdAnims)
					AND NOT bCreatedAnEntityThisFrame //Only create one entity per frame (to fix huge peaks in CPU usage)
						IF i = 0
							sBirds[i].ped = CREATE_PED(PEDTYPE_MISSION, modelBird, <<-1161.1345, -730.0198, 19.3756>> + <<0.0, 0.0, -0.15>>, 278.8218)
						ELIF i = 1
							sBirds[i].ped = CREATE_PED(PEDTYPE_MISSION, modelBird, <<-1160.7617, -728.6110, 19.4617>> + <<0.0, 0.0, -0.15>>, 322.6699)
						ELIF i = 2
							sBirds[i].ped = CREATE_PED(PEDTYPE_MISSION, modelBird, <<-1162.7354, -727.3837, 19.5456>> + <<0.0, 0.0, -0.15>>, 32.0168)
						ELIF i = 3
							sBirds[i].ped = CREATE_PED(PEDTYPE_MISSION, modelBird, <<-1160.6444, -730.7355, 19.3279>> + <<0.0, 0.0, -0.15>>, 152.0615)
						ELIF i = 4
							sBirds[i].ped = CREATE_PED(PEDTYPE_MISSION, modelBird, <<-1155.0334, -730.3810, 19.4121>> + <<0.0, 0.0, -0.15>>, 352.3865)
						ELIF i = 5
							sBirds[i].ped = CREATE_PED(PEDTYPE_MISSION, modelBird, <<-1152.8080, -723.8991, 19.7718>> + <<0.0, 0.0, -0.15>>, 25.9012)
							
							SET_MODEL_AS_NO_LONGER_NEEDED(modelBird)
						ENDIF
						
						TASK_PLAY_ANIM(sBirds[i].ped, strBirdAnims, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBirds[i].ped, TRUE)
						SET_PED_CAN_RAGDOLL(sBirds[i].ped, FALSE)
						SET_ENTITY_INVINCIBLE(sBirds[i].ped, TRUE)
						FREEZE_ENTITY_POSITION(sBirds[i].ped, TRUE)
						SET_ENTITY_COLLISION(sBirds[i].ped, FALSE)
						
						sBirds[i].iEvent = 1
						sBirds[i].fSpeed = 0.0
						bCreatedAnEntityThisFrame = TRUE
					ENDIF
				ENDIF
			ELIF DOES_ENTITY_EXIST(sBirds[i].ped)
				IF NOT IS_PED_INJURED(sBirds[i].ped)
					SWITCH sBirds[i].iEvent
						CASE 1
							IF REQUEST_SCRIPT_AUDIO_BANK("ARM_1_BIRDS")
								vBirdPos = GET_ENTITY_COORDS(sBirds[i].ped)
							
								IF VDIST2(vPlayerPos, vBirdPos) < 900.0
								OR (VDIST2(vLamarPos, vBirdPos) < 900.0 AND fCurrentPlaybackSpeed > 0.7)
									IF i = 0
										PLAY_SOUND_FROM_ENTITY(-1, "Birds", sBirds[i].ped, "ARM_1_SOUNDSET")
										
										//Record footage of the player driving into the birds.
										IF VDIST2(vPlayerPos, vBirdPos) < 2500.0
											REPLAY_RECORD_BACK_FOR_TIME(2.0, 2.0, REPLAY_IMPORTANCE_HIGH) 
										ENDIF
									ENDIF
									
									sBirds[i].iTimer = GET_GAME_TIMER()
									sBirds[i].iEvent++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF GET_GAME_TIMER() - sBirds[i].iTimer > 500
								FREEZE_ENTITY_POSITION(sBirds[i].ped, FALSE)
								SET_ENTITY_COLLISION(sBirds[i].ped, TRUE)
								sBirds[i].vDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(sBirds[i].ped) + <<45.0, 0.0, 0.0>>)
								sBirds[i].fSpeed = sBirds[i].fSpeed + ((MAX_BIRD_SPEED - sBirds[i].fSpeed) * 0.5)
								SET_ENTITY_VELOCITY(sBirds[i].ped, sBirds[i].vDir * sBirds[i].fSpeed)
								TASK_PLAY_ANIM(sBirds[i].ped, strBirdAnims, "flapping", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
								
								sBirds[i].iEvent++
							ENDIF
						BREAK
						
						CASE 3
							sBirds[i].fSpeed = sBirds[i].fSpeed + ((MAX_BIRD_SPEED - sBirds[i].fSpeed) * 0.5)
							SET_ENTITY_VELOCITY(sBirds[i].ped, sBirds[i].vDir * sBirds[i].fSpeed)
							
							IF IS_ENTITY_PLAYING_ANIM(sBirds[i].ped, strBirdAnims, "flapping")
								SET_ENTITY_ANIM_SPEED(sBirds[i].ped, strBirdAnims, "flapping", 4.0)
								sBirds[i].iEvent++
							ENDIF
						BREAK
						
						CASE 4
							sBirds[i].fSpeed = sBirds[i].fSpeed + ((MAX_BIRD_SPEED - sBirds[i].fSpeed) * 0.5)
							SET_ENTITY_VELOCITY(sBirds[i].ped, sBirds[i].vDir * sBirds[i].fSpeed)
						BREAK
					ENDSWITCH
				ENDIF
				
				IF (fCurrentPlaybackTime > 82000.0 OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
				AND NOT bRemovedAnEntityThisFrame
					REMOVE_PED(sBirds[i].ped)
					bRemovedAnEntityThisFrame = TRUE
				ENDIF
			ELSE
				IF i = 0
					REMOVE_ANIM_DICT(strBirdAnims)
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("ARM_1_BIRDS")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	//Security guard standing outside bank
	IF fCurrentPlaybackTime < 20000.0
		sSecurityGuard.iEvent = 0
	ELIF fCurrentPlaybackTime > 120000.0
		SWITCH sSecurityGuard.iEvent
			CASE 0
				REQUEST_MODEL(modelBankSecurity)
				
				IF HAS_MODEL_LOADED(modelBankSecurity)
				AND NOT bCreatedAnEntityThisFrame
					sSecurityGuard.ped = CREATE_PED(PEDTYPE_MISSION, modelBankSecurity, <<-73.6282, -676.8480, 32.9306>>, 69.2552)
					FREEZE_ENTITY_POSITION(sSecurityGuard.ped, TRUE)
					TASK_STAND_STILL(sSecurityGuard.ped, -1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSecurityGuard.ped, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(modelBankSecurity)
					sSecurityGuard.iEvent++
					bCreatedAnEntityThisFrame = TRUE
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_PED_INJURED(sSecurityGuard.ped)
					IF IS_ENTITY_STATIC(sSecurityGuard.ped)
						IF VDIST2(GET_ENTITY_COORDS(sSecurityGuard.ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 2500.0
							FREEZE_ENTITY_POSITION(sSecurityGuard.ped, FALSE)
						ENDIF
					ELSE
						//If the player somehow gets to the guard before Lamar, just have him run off.
						IF (fCurrentPlaybackTime < 144600.0 AND VDIST2(GET_ENTITY_COORDS(sSecurityGuard.ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 25.0)
						OR fCurrentPlaybackTime > 145600.0		
							sSecurityGuard.iEvent++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF NOT IS_PED_INJURED(sSecurityGuard.ped)
					IF NOT IS_PED_IN_COMBAT(sSecurityGuard.ped)
					AND GET_SCRIPT_TASK_STATUS(sSecurityGuard.ped, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
						PLAY_PED_AMBIENT_SPEECH(sSecurityGuard.ped, "FIGHT")
					
						TASK_COMBAT_PED(sSecurityGuard.ped, PLAYER_PED_ID())
					ENDIF
					
					/*IF VDIST2(GET_ENTITY_COORDS(sSecurityGuard.ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 400.0
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						OR (GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1.0 AND bBuddyFinished)
							IF NOT HAS_PED_GOT_WEAPON(sSecurityGuard.ped, WEAPONTYPE_PISTOL)
								GIVE_WEAPON_TO_PED(sSecurityGuard.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO)
								SET_PED_ACCURACY(sSecurityGuard.ped, 100)
							ENDIF
						ENDIF
					ENDIF*/
				
					//If the player starts trying to mess around just have him run off.
					/*IF IS_PED_IN_COMBAT(sSecurityGuard.ped)
						IF VDIST2(GET_ENTITY_COORDS(sSecurityGuard.ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 49.0
							OPEN_SEQUENCE_TASK(seq)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-218.90, -623.75, 33.11>>, PEDMOVE_SPRINT, -1, 1.0)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200.0, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(sSecurityGuard.ped, seq)
							CLEAR_SEQUENCE_TASK(seq)

							sSecurityGuard.iEvent++
						ENDIF
					ENDIF*/
				ENDIF
			BREAK
		ENDSWITCH
		
		//Clean up if he dies at any point.
		IF sSecurityGuard.iEvent > 0
			IF IS_PED_INJURED(sSecurityGuard.ped)
				REMOVE_PED(sSecurityGuard.ped)
			ENDIF
		ENDIF
	ENDIF
	
	//The stockade in the bank is falling through the interior, so freeze it.
	IF ParkedCarModel[STOCKADE_PARKED_ID] = STOCKADE
		IF NOT IS_ENTITY_DEAD(ParkedCarID[STOCKADE_PARKED_ID])
			IF NOT IS_ENTITY_STATIC(ParkedCarID[STOCKADE_PARKED_ID])
				FREEZE_ENTITY_POSITION(ParkedCarID[STOCKADE_PARKED_ID], TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(ParkedCarID[STOCKADE_PARKED_ID], FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	//Create a shutter over the bank vault entrance.
	IF NOT DOES_ENTITY_EXIST(objVaultShutter)
		IF VDIST2(vPlayerPos, <<-7.1741, -658.6362, 33.8238>>) < 10000.0
			SETUP_REQ_VAULT_SHUTTER(<<-7.1741, -658.6362, 33.8238>>, <<0.0000, 0.0000, 4.5837>>)
		ENDIF
	ENDIF
ENDPROC

FUNC VEHICLE_INDEX GET_CLOSEST_VEHICLE_TO_PLAYER_EXCLUDING_CHASE_CARS()
	CONST_INT NUM_CARS_TO_STORE		5

	VEHICLE_INDEX vehNearby[NUM_CARS_TO_STORE]
	
	GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehNearby)
	
	INT i = 0
	REPEAT NUM_CARS_TO_STORE i
		IF vehNearby[i] != sMainCars[0].veh AND vehNearby[i] != sMainCars[1].veh
		AND NOT IS_ENTITY_DEAD(vehNearby[i])
			RETURN vehNearby[i]
		ENDIF
	ENDREPEAT
	
	RETURN NULL
ENDFUNC


PROC DO_CHASE_HORNS(VEHICLE_INDEX vehClosest)
	//General horns for all vehicles: get the nearest vehicle and play horn
	IF GET_GAME_TIMER() - iTimeSinceLastHorn > 2000
		IF vehClosest != sMainCars[iBuddiesCar].veh
			IF IS_VEHICLE_DRIVEABLE(vehClosest)
				IF VDIST2(GET_ENTITY_COORDS(vehClosest), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 9.0
					IF (GET_ENTITY_SPEED(sMainCars[iPlayersCar].veh) - GET_ENTITY_SPEED(vehClosest)) > 3.0
					OR ABSF(GET_ENTITY_HEADING(sMainCars[iPlayersCar].veh) - GET_ENTITY_HEADING(vehClosest)) > 60.0 //Cars aren't travelling in same direction.
						PED_INDEX pedClosest = GET_PED_IN_VEHICLE_SEAT(vehClosest)
						
						IF NOT IS_PED_INJURED(pedClosest)
							START_VEHICLE_HORN(vehClosest, 2000)
							
							#IF IS_DEBUG_BUILD
								IF bDebugEnableHornAsserts
									SCRIPT_ASSERT("Armenian 1 - Triggered horn")
								ENDIF
							
								PRINTSTRING("Armenian 1 - Vehicle horn triggered at ")
								PRINTVECTOR(GET_ENTITY_COORDS(vehClosest))
								PRINTNL()
							#ENDIF
										
							iTimeSinceLastHorn = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Detects if the closest car has just missed Franklin. If so it plays a random line of dialogue.
PROC DO_NEAR_MISS_FRANKLIN_DIALOGUE(VEHICLE_INDEX vehClosest)
	IF GET_GAME_TIMER() - iTimeSinceLastNearMissDialogue > 5000
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF vehClosest != sMainCars[iBuddiesCar].veh
				IF IS_VEHICLE_DRIVEABLE(vehClosest)
				AND IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sMainCars[iPlayersCar].veh, vehClosest)
						IF GET_ENTITY_SPEED(sMainCars[iPlayersCar].veh) > 15.0
						AND GET_ENTITY_SPEED(vehClosest) > 1.0
							IF ABSF(GET_ENTITY_HEADING(sMainCars[iPlayersCar].veh) - GET_ENTITY_HEADING(vehClosest)) > 60.0 //Cars aren't travelling in same direction.
								VECTOR vOffsetFromPlayer = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sMainCars[iPlayersCar].veh, GET_ENTITY_COORDS(vehClosest))
								
								IF vOffsetFromPlayer.y < -1.5 //Car has passed the player.
								AND vOffsetFromPlayer.y > -5.0
									IF ABSF(vOffsetFromPlayer.x) < 4.0 //Car is near to the side of the player.
										IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_NEAR", CONV_PRIORITY_MEDIUM) 
											iTimeSinceLastNearMissDialogue = GET_GAME_TIMER()
											
											//Play a gesture.
											IF fCurrentPlaybackTime < 144600.0
											AND GET_CONVERTIBLE_ROOF_STATE(sMainCars[iPlayersCar].veh) = CRS_LOWERED
											AND IS_ENTITY_UPRIGHT(sMainCars[iPlayersCar].veh)
												IF HAS_ANIM_DICT_LOADED(strFranklinCarAnimsTaunt)
												AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), strFranklinCarAnimsTaunt, "learnhowtouseastick")
													TASK_PLAY_ANIM(PLAYER_PED_ID(), strFranklinCarAnimsTaunt, "learnhowtouseastick", 
															   	   NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BLOCK_SCENARIOS_FOR_CHASE(BOOL bBlock)
	IF bBlock
		IF sbiMovieStudio = NULL
			sbiMovieStudio = ADD_SCENARIO_BLOCKING_AREA(<<-1196.229370,-575.141541,0.879524>>, <<-1035.999268,-474.751373,68.879524>>)
		ENDIF
		
		SET_PED_PATHS_IN_AREA(<<-1714.314575,-567.082825,30.834576>>, <<-1680.669922,-557.942688,39.834576>>, FALSE)
		SET_PED_PATHS_IN_AREA(<<-373.6247, -835.7380, 30.4071>>, <<-327.3391, -822.5929, 38.0959>>, FALSE)
		SET_PED_PATHS_IN_AREA(<<-353.9807, -683.0400, 30.5587>>, <<-316.4468, -669.6537, 37.2847>>, FALSE)
		SET_PED_PATHS_IN_AREA(<<-1287.1616, -651.9630, 25.4564>>, <<-1256.6211, -625.6708, 29.6292>>, FALSE)
		
		SET_PED_NON_CREATION_AREA(<<-373.6247, -835.7380, 30.4071>>, <<-327.3391, -822.5929, 38.0959>>)
		
		//IF sbiPastMovieStudio = NULL
		//	sbiPastMovieStudio = ADD_SCENARIO_BLOCKING_AREA(<<-1337.2729, -687.7573, 0.0>>, <<-1237.6948, -582.6704, 47.9080>>)
		//ENDIF
	ELSE
		IF sbiMovieStudio != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiMovieStudio)
			sbiMovieStudio = NULL
		ENDIF
		
		IF sbiPastMovieStudio != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiPastMovieStudio)
			sbiPastMovieStudio = NULL
		ENDIF
		
		SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1714.314575,-567.082825,30.834576>>, <<-1680.669922,-557.942688,39.834576>>)
		SET_PED_PATHS_BACK_TO_ORIGINAL(<<-373.6247, -835.7380, 30.4071>>, <<-327.3391, -822.5929, 38.0959>>)
		SET_PED_PATHS_BACK_TO_ORIGINAL(<<-353.9807, -683.0400, 30.5587>>, <<-316.4468, -669.6537, 37.2847>>)
		SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1287.1616, -651.9630, 25.4564>>, <<-1256.6211, -625.6708, 29.6292>>)
		
		CLEAR_PED_NON_CREATION_AREA()
	ENDIF
ENDPROC

PROC BLOCK_SCENARIOS_FOR_CUTSCENE(BOOL bBlock)
	IF bBlock
		IF sbiCutscene = NULL
			sbiCutscene = ADD_SCENARIO_BLOCKING_AREA(<<-1862.3032, -607.0848, 12.0363>>, <<-1812.5437, -526.8602, 36.9263>>)
			SET_PED_NON_CREATION_AREA(<<-1862.3032, -607.0848, 13.0363>>, <<-1812.5437, -526.8602, 70.9263>>)
			SET_PED_PATHS_IN_AREA(<<-1862.3032, -607.0848, 13.0363>>, <<-1812.5437, -526.8602, 70.9263>>, FALSE)
		ENDIF
		
		IF iNavmeshBlockCutscene = -1
			iNavmeshBlockCutscene = ADD_NAVMESH_BLOCKING_OBJECT(<<-1839.0, -579.4, 15.0>>, <<10.0, 70.0, 10.0>>, DEG_TO_RAD(139.8))
		ENDIF
	ELSE
		IF sbiCutscene != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiCutscene)
			CLEAR_PED_NON_CREATION_AREA()
			SET_PED_PATHS_BACK_TO_ORIGINAL(<<-1862.3032, -607.0848, 13.0363>>, <<-1812.5437, -526.8602, 70.9263>>)
			sbiCutscene = NULL
		ENDIF
		
		IF iNavmeshBlockCutscene != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavmeshBlockCutscene)
			iNavmeshBlockCutscene = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_SHOWROOM_CARS_EXIST()
	IF DOES_ENTITY_EXIST(vehShowroomCars[0])
		IF DOES_ENTITY_EXIST(vehShowroomCars[1])
			IF DOES_ENTITY_EXIST(vehShowroomCars[2])
				IF DOES_ENTITY_EXIST(vehShowroomCars[3])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CUTSCENE_PED_COMPONENT_VARIATIONS(STRING strCutsceneName)
	INT iNameHash = GET_HASH_KEY(strCutsceneName)

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF iNameHash = HASH("armenian_1_int")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_HAIR, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_TORSO, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_LEG, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_HAND, 2, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Lamar", PED_COMP_SPECIAL, 0, 0)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_TORSO, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_LEG, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_HAND, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_FEET, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL2, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_SPECIAL, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", PED_COMP_JBIB, 0, 0)
			
			PRINTLN("Armenian1.sc - Succesfully set components for armenian_1_int.")
		ELIF iNameHash = HASH("armenian_1_mcs_1")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lamar", sLamar.ped)
			ENDIF
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Siemon", PED_COMP_TORSO, 1, 0)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_TORSO, 4, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 1, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_HAIR, 2, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_LEG, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_FEET, 0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_SPECIAL, 3, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_DECL, 2, 0)
		ELIF iNameHash = HASH("armenian_1_mcs_2")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lamar", sLamar.ped)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BLOCK_VEHICLE_GENS_AT_START_POINT(BOOL bBlock)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1886.4, -584.1, 5.4>>, <<-1872.8, -575.0, 17.5>>, NOT bBlock)
ENDPROC

PROC BLOCK_VEHICLE_GENS_IN_SHOWROOM(BOOL bBlock)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-44.9779, -1093.9878, 19.4526>>, <<-30.0332, -1074.4330, 39.4526>>, NOT bBlock)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-20.8010, -1084.0568, 20.8603>>, <<-3.4886, -1050.9978, 31.8603>>, NOT bBlock)
	
	IF bBlock
		CLEAR_AREA_OF_VEHICLES(<<-37.0, -1087.0, 26.0>>, 5.0)
	ENDIF
ENDPROC

PROC BLOCK_SCENARIOS_AT_SHOWROOM()
	IF sbiShowroom = NULL
		sbiShowroom = ADD_SCENARIO_BLOCKING_AREA(<<-49.411049,-1078.832275,20.473274>>, <<-44.080521,-1071.935669,39.473274>>)
	ENDIF
	
	IF sbiShowroomBrowse = NULL
		sbiShowroomBrowse = ADD_SCENARIO_BLOCKING_AREA(<<-54.5514, -1111.4725, 22.2924>>, <<-39.9018, -1104.0132, 39.7759>>)
	ENDIF
ENDPROC

PROC BLOCK_COPS_SPAWNING_IN_BANK(BOOL bBlock)
	IF bBlock
		IF iDispatchSpawnBlockBank1 = -1
			iDispatchSpawnBlockBank1 = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<25.406651,-664.524109,30.378651>>, <<10.393962,-690.480042,35.739861>>, 19.250000)
			
			CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Created iDispatchSpawnBlockBank1.")
		ENDIF
		
		IF iDispatchSpawnBlockBank2 = -1
			iDispatchSpawnBlockBank2 = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<14.522158,-684.694336,30.088087>>, <<-85.904114,-678.751953,38.473141>>, 52.500000)
			
			CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Created iDispatchSpawnBlockBank2.")
		ENDIF
	ELSE
		IF iDispatchSpawnBlockBank1 != -1
			REMOVE_DISPATCH_SPAWN_BLOCKING_AREA(iDispatchSpawnBlockBank1)
			iDispatchSpawnBlockBank1 = -1
			
			CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Removed iDispatchSpawnBlockBank1.")
		ENDIF
		
		IF iDispatchSpawnBlockBank2 != -1
			REMOVE_DISPATCH_SPAWN_BLOCKING_AREA(iDispatchSpawnBlockBank2)
			iDispatchSpawnBlockBank2 = -1
			
			CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Removed iDispatchSpawnBlockBank2.")
		ENDIF
	ENDIF
ENDPROC	

PROC DISABLE_CONTROLS_INSIDE_SHOWROOM()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
ENDPROC

//Creates/removes forced objects for the chase.
PROC SET_FORCED_OBJECTS_FOR_CHASE(BOOL bSetActive)
	IF bSetActive
		IF NOT bForcedObjectsAreActive
			CREATE_FORCED_OBJECT(<<-1052.49, -476.15, 36.6>>, 5.0, PROP_SEC_BARRIER_LD_01A, TRUE)
			CREATE_FORCED_OBJECT(<<-1207.25, -578.08, 26.17>>, 5.0, PROP_SEC_BARIER_03B, TRUE)
			CREATE_FORCED_OBJECT(<<-1208.3219, -579.1413, 26.27>>, 5.0, PROP_SEC_BARIER_BASE_01, TRUE)
			bForcedObjectsAreActive = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("Armenian1.sc - Created forced objects for chase.")
			#ENDIF
		ENDIF
	ELSE
		IF bForcedObjectsAreActive
			REMOVE_FORCED_OBJECT(<<-1052.49, -476.15, 36.6>>, 5.0, PROP_SEC_BARRIER_LD_01A)
			REMOVE_FORCED_OBJECT(<<-1207.25, -578.08, 26.17>>, 5.0, PROP_SEC_BARIER_03B)
			REMOVE_FORCED_OBJECT(<<-1208.3219, -579.1413, 26.27>>, 5.0, PROP_SEC_BARIER_BASE_01)
			bForcedObjectsAreActive = FALSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("Armenian1.sc - Removed forced objects for chase.")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MISSION_SETUP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEAR_PRINTS()
	CLEAR_HELP()
	CLEAR_ALL_FLOATING_HELP()
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	
	REQUEST_ADDITIONAL_TEXT("ARM1", MISSION_TEXT_SLOT)

	//groupPlayer = GET_PLAYER_GROUP(PLAYER_ID())

	ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, PLAYER_PED_ID(), "FRANKLIN")

	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)

	//The car nearest the building
	sMainCars[0].vStartPos = <<-1878.5793, -579.5922, 10.7770>>//<< -1878.3322, -579.8704, 10.7790 >>
	sMainCars[0].fStartHeading = 320.3026
	sMainCars[0].iStartCarrec = 1
	
	sMainCars[1].vStartPos = <<-1881.1982, -577.0080, 10.7690>>//<< -1880.2660, -577.8439, 10.7702 >>
	sMainCars[1].fStartHeading = 319.7534
	sMainCars[1].iStartCarrec = 2
	
	//Cam recordings
	iCamRecs[0] = 901
	iCamRecs[1] = 902
	iCamRecs[2] = 903
	iCamRecs[3] = 904
	iCamRecs[4] = 905
	iCamRecs[5] = 906
	iCamRecs[6] = 907
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(NINEF2, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT2, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(NINEF, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN), TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
	
	sbiSecurityGuards = ADD_SCENARIO_BLOCKING_AREA(<<50.2705, -636.6755, 5.3062>>, <<74.2078, -610.5507, 42.6258>>)
	
	SET_ROADS_IN_AREA(<<-112.6669, -1539.6423, 20.2851>>, <<74.2658, -1438.4238, 42.8005>>, FALSE)
	BLOCK_VEHICLE_GENS_IN_SHOWROOM(TRUE)

	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)

	
	SET_DOOR_STATE(DOORNAME_STUDIO_NORTH_GATE_IN, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_STUDIO_NORTH_GATE_OUT, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_STUDIO_SOUTH_GATE_IN, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_STUDIO_SOUTH_GATE_OUT, DOORSTATE_LOCKED)
	
	
	//INFORM_MISSION_STATS_OF_MISSION_START_ARMENIAN_ONE()
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
	SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, FALSE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_BANK, BUILDINGSTATE_DESTROYED)
	SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE)

	SET_WEATHER_TYPE_OVERTIME_PERSIST("clear", 20.0)

	iLamarRevSound = GET_SOUND_ID()
	g_eArm1PrestreamDenise = ARM1_PD_3_release

	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
		
		sSkipMenu[0].sTxtLabel = "ARMENIAN_1_INT - OPENING_CUTSCENE"    
		sSkipMenu[1].sTxtLabel = "CHOOSE_CAR"                
		sSkipMenu[2].sTxtLabel = "START_CHASE_CUTSCENE"
		sSkipMenu[3].sTxtLabel = "CHASE" 
		sSkipMenu[4].sTxtLabel = "CHASE_MID_POINT" 
		sSkipMenu[5].sTxtLabel = "COPS_ARRIVE_CUTSCENE"
		sSkipMenu[6].sTxtLabel = "LOSE_COPS"
		sSkipMenu[7].sTxtLabel = "SHOWROOM_INTRO"
		sSkipMenu[8].sTxtLabel = "MEET_SIMEON"
		sSkipMenu[9].sTxtLabel = "ARMENIAN_1_MCS_1 - SHOWROOM_CUTSCENE"
		sSkipMenu[10].sTxtLabel = "GO_TO_HOUSE"   
		sSkipMenu[11].sTxtLabel = "ARMENIAN_1_MCS_2_concat - HOUSE_CUTSCENE"   
		//sSkipMenu[12].sTxtLabel = "(debug only) GO_INSIDE_HOUSE" 
		//sSkipMenu[13].sTxtLabel = "SAVE_TUTORIAL"  
	#ENDIF
ENDPROC

PROC MISSION_CLEANUP()
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)

	CASCADE_SHADOWS_SET_SCREEN_SIZE_CHECK_ENABLED(TRUE)
	CLEAR_HELP()
	FLASH_WANTED_DISPLAY(FALSE)
	CLEAR_ALL_FLOATING_HELP()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	SET_MAX_WANTED_LEVEL(5)
	STOP_SCRIPT_GLOBAL_SHAKING()
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	SET_AGGRESSIVE_HORNS(FALSE)
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	SET_RADIO_AUTO_UNFREEZE(TRUE)
	SET_IGNORE_NO_GPS_FLAG(FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
	RESET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS(DT_POLICE_AUTOMOBILE)
	DISABLE_CELLPHONE(FALSE)
	HIDE_ACTIVE_PHONE(FALSE)
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	NEW_LOAD_SCENE_STOP()
	STOP_GAMEPLAY_HINT()
	SET_SRL_LONG_JUMP_MODE(FALSE)
	SET_NUMBER_OF_PARKED_VEHICLES(-1)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				DETACH_ENTITY(PLAYER_PED_ID())
			ENDIF
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
		SET_PED_CAPSULE(PLAYER_PED_ID(), 0.0)
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
		
		IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF NOT HAS_CUTSCENE_FINISHED()
		IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
		ENDIF
		
		STOP_CUTSCENE()
	ENDIF
	
	REMOVE_CUTSCENE()
 	
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	//If we started a replay recording then make sure it's cancelled if we're performing cleanup.
	IF bReplayEventStarted
		REPLAY_STOP_EVENT()
		bReplayEventStarted = FALSE
	ENDIF
	
	//Bank gates must be closed after this mission.
	SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS, BUILDINGSTATE_NORMAL)
	BLOCK_SCENARIOS_FOR_CHASE(FALSE)
	BLOCK_SCENARIOS_FOR_CUTSCENE(FALSE)
	BLOCK_COPS_SPAWNING_IN_BANK(FALSE)
	
	INT i = 0
	REPEAT COUNT_OF(iWheelSkidPtfx) i
		IF iWheelSkidPtfx[i] != NULL
			STOP_PARTICLE_FX_LOOPED(iWheelSkidPtfx[i])
			iWheelSkidPtfx[i] = NULL
		ENDIF
	ENDREPEAT
	
	IF ptfxCreditsBoat[0] != NULL
		STOP_PARTICLE_FX_LOOPED(ptfxCreditsBoat[0])
		ptfxCreditsBoat[0] = NULL
	ENDIF
	
	IF ptfxCreditsBoat[1] != NULL
		STOP_PARTICLE_FX_LOOPED(ptfxCreditsBoat[1])
		ptfxCreditsBoat[1] = NULL
	ENDIF
	
	REMOVE_PTFX_ASSET()
	SET_AGGRESSIVE_HORNS(FALSE)
	STOP_AUDIO_SCENES()
	STOP_STREAM()
	RELEASE_SCRIPT_AUDIO_BANK()
	
	IF sbiShowroom != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sbiShowroom)
		sbiShowroom = NULL
	ENDIF
	
	IF sbiShowroomBrowse != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sbiShowroomBrowse)
		sbiShowroomBrowse = NULL
	ENDIF
	
	REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 5)
	//REMOVE_PED_FOR_DIALOGUE(sConversationPeds, 6)
	
	IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
		MODIFY_VEHICLE_TOP_SPEED(sMainCars[iPlayersCar].veh, 0.0)
	ENDIF	
	
	DESTROY_ALL_CAMS()
	DISPLAY_RADAR(TRUE) 
	DISPLAY_HUD(TRUE)
	
	IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	IF bRequestedCreditsScaleform
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfCredits)
		bRequestedCreditsScaleform = FALSE
	ENDIF
	
	REMOVE_ALL_BLIPS()
	REMOVE_ALL_OBJECTS()
	REMOVE_ALL_PEDS()
	REMOVE_ALL_VEHICLES()

	//If we had to force the cam view at any point then make sure it's reset.
	IF bForcedFirstPersonView
		SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE, ePrevCarViewMode)
		bForcedFirstPersonView = FALSE
	ENDIF

	IF bShowroomCarsBlocked
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vRoomCorner1, vRoomCorner2, TRUE)
		bShowroomCarsBlocked = FALSE
	ENDIF
	
	IF bCustomGPSActive
		SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		CLEAR_GPS_MULTI_ROUTE()
		bCustomGPSActive = FALSE
	ENDIF

	IF interiorShowroom != NULL
		UNPIN_INTERIOR(interiorShowroom)
		interiorShowroom = NULL
	ENDIF
	
	IF interiorFranklinsHouse != NULL
		UNPIN_INTERIOR(interiorFranklinsHouse)
		interiorFranklinsHouse = NULL
	ENDIF
	
	IF interiorShrink != NULL
		UNPIN_INTERIOR(interiorShrink)
		interiorShrink = NULL
	ENDIF
	
	IF interiorBank != NULL
		UNPIN_INTERIOR(interiorBank)
		interiorBank = NULL
	ENDIF
	
	IF interiorCarPark != NULL
		UNPIN_INTERIOR(interiorCarPark)
		interiorCarPark = NULL
	ENDIF

	SET_FORCED_OBJECTS_FOR_CHASE(FALSE)
	SET_SIDE_CAR_PARK_INTERIOR_ACTIVE(FALSE)
	BLOCK_VEHICLE_GENS_AT_START_POINT(FALSE)
	
	REMOVE_MODEL_HIDE(<<-18.3539, -1438.7838, 31.3050>>, 3.0, V_ILEV_FRNKWARDDR1, TRUE)
	REMOVE_MODEL_HIDE(<<-18.3594, -1438.1329, 31.3050>>, 3.0, V_ILEV_FRNKWARDDR2, TRUE)
	REQUEST_IPL("ferris_finale_Anim")
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	IF iChasePedBlockingArea != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iChasePedBlockingArea)
		iChasePedBlockingArea = -1
	ENDIF
	
	IF iChasePedBlockingArea2 != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iChasePedBlockingArea2)
		iChasePedBlockingArea2 = -1
	ENDIF
	
	IF iGatePedBlockingArea != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iGatePedBlockingArea)
		iGatePedBlockingArea = -1
	ENDIF

	IF iShowroomPedBlockingArea != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iShowroomPedBlockingArea)
		iShowroomPedBlockingArea = -1
	ENDIF
	
	IF iShowroomPedBlockingArea2 != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iShowroomPedBlockingArea2)
		iShowroomPedBlockingArea2 = -1
	ENDIF
	
	SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_B, DOORSTATE_UNLOCKED)
	SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_UNLOCKED)
	
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_01_BH, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_02_SC, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_03_V, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_04_SS, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_05_MP, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_06_HW, FALSE)
	SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_07_PB, FALSE)

	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)

	IF eSectionStage != SECTION_STAGE_JUMPING_TO_STAGE
		//Clean up anything that was created in the initial setup (this stuff must not be cleaned up if p-skipping)
		#IF IS_DEBUG_BUILD
			DESTROY_WIDGETS()
		#ENDIF

		KILL_FACE_TO_FACE_CONVERSATION()
		SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)

		SET_VEHICLE_MODEL_IS_SUPPRESSED(NINEF2, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT2, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(NINEF, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN), FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, FALSE)
		
		IF sbiSecurityGuards != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbiSecurityGuards)
		ENDIF
		
		SET_DOOR_STATE(DOORNAME_STUDIO_NORTH_GATE_IN, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_STUDIO_NORTH_GATE_OUT, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_STUDIO_SOUTH_GATE_IN, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_STUDIO_SOUTH_GATE_OUT, DOORSTATE_UNLOCKED)
		
		SET_ROADS_BACK_TO_ORIGINAL(<<-112.6669, -1539.6423, 20.2851>>, <<74.2658, -1438.4238, 42.8005>>)
		BLOCK_VEHICLE_GENS_IN_SHOWROOM(FALSE)
		CLEAR_PED_NON_CREATION_AREA()
		REGISTER_SCRIPT_WITH_AUDIO(FALSE)
		
		SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_PSYCHEOFFICE, TRUE)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_BANK, BUILDINGSTATE_NORMAL, TRUE)
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_DT1_03_CARPARK, TRUE)
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
		ENDIF
		
		//2255037 - If we allowed Franklin to unlock Michael's mansion gate then reset.
		UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
		
		RELEASE_SOUND_ID(iLamarRevSound)

		CLEAR_WEATHER_TYPE_PERSIST()

		TERMINATE_THIS_THREAD()
	ELSE
		CLEAR_PRINTS()
		CLEAR_TRIGGERED_LABELS()
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		SET_SAVEHOUSE_RESPAWN_AVAILABLE(SAVEHOUSE_FRANKLIN_SC, FALSE)
		
		CLEANUP_UBER_PLAYBACK()
		
		g_eArm1PrestreamDenise = ARM1_PD_3_release
		
		TRIGGER_MUSIC_EVENT("ARM1_RADIO_ON")
		
		REMOVE_WAYPOINT_RECORDING(strWaypointStartAlley)
		REMOVE_WAYPOINT_RECORDING(strWaypointAroundCar)
		
		REMOVE_VEHICLE_RECORDING(sMainCars[0].iStartCarrec, strCarrec)
		REMOVE_VEHICLE_RECORDING(sMainCars[1].iStartCarrec, strCarrec)
		
		REMOVE_ANIM_DICT(strChaseStartAnims)
		REMOVE_ANIM_DICT(strChaseEndAnims)
		REMOVE_ANIM_DICT("map_objects")
		REMOVE_ANIM_DICT(strAlienAnims)
		REMOVE_ANIM_DICT(strLamarLeadOutAnims)
		REMOVE_ANIM_DICT(strShowroomLeadInOutAnims)
		REMOVE_ANIM_DICT(strLamarCarAnimsCrash)
		REMOVE_ANIM_DICT(strLamarCarAnimsTaunt)
		REMOVE_ANIM_DICT(strFranklinCarAnimsTaunt)
		REMOVE_ANIM_DICT(strShowroomCamAnims)
		REMOVE_ANIM_DICT(strLamarShowroomAnims)
		REMOVE_ANIM_DICT(strLamarAnnoyedAnims)
		REMOVE_ANIM_DICT(strFerrisWheelAnims)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCarOwner)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar2)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCamCar)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBankGate)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBankGateCollision)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelBankSecurity)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelMovieGuards)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCarOwner)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelShirt)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelShirt)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelWardrobeLeft)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelWardrobeRight)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelHealthPack)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsBoat)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsSailboat)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsBoatPed)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelLamarsPhone)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheel)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisCar)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheelPed1)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheelPed2)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelJoggerFemale)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsFemale)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsMale)
		SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_JIMMY)
		SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_SIMEON)
		
		CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000.0, TRUE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		CASCADE_SHADOWS_INIT_SESSION()
	ENDIF
ENDPROC

PROC MISSION_PASSED(BOOL bIsForcePass = FALSE)
	//If we force passed the mission then do some cutscene checks.
	IF bIsForcePass
		IF NOT HAS_CUTSCENE_FINISHED()			
			STOP_CUTSCENE()
		ENDIF
		
		REMOVE_CUTSCENE()
	 	
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE
		
		DO_FADE_IN_WITH_WAIT()
	ENDIF

	SET_SAVEHOUSE_RESPAWN_AVAILABLE(SAVEHOUSE_FRANKLIN_SC, TRUE)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_FRANKLIN_SAVEHOUSE_CAR, TRUE)

	SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	Execute_Activate_Shop_Barbers()

	Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_ACTIVE, TRUE)
	Mission_Flow_Mission_Passed()
	MISSION_CLEANUP()
ENDPROC

/// PURPOSE:
///    Begins the mission fail process. Failing the mission will result in either a fail cutscene, phone call, or an immediate termination of the script
///    depending on why and when the mission failed.
/// PARAMS:
///    reason - The reason why the mission failed.
///    bKillMissionImmediately - Forces the script to terminate immediately regardless of fail reason, if FALSE the script will attempt to start a fail cutscene/phone call.
PROC MISSION_FAILED(FAILED_REASON reason)
	IF NOT bMissionFailed
		CLEAR_PRINTS()
		CLEAR_HELP()
		REMOVE_ALL_BLIPS()
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		KILL_ANY_CONVERSATION()
		SET_SAVEHOUSE_RESPAWN_AVAILABLE(SAVEHOUSE_FRANKLIN_SC, FALSE)

		IF bCustomGPSActive
			SET_GPS_MULTI_ROUTE_RENDER(FALSE)
			CLEAR_GPS_MULTI_ROUTE()
			bCustomGPSActive = FALSE
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[0].veh)
			FREEZE_ENTITY_POSITION(sMainCars[0].veh, FALSE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[1].veh)
			FREEZE_ENTITY_POSITION(sMainCars[1].veh, FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSimeon.ped)
			TASK_SMART_FLEE_COORD(sSimeon.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
			SET_PED_KEEP_TASK(sSimeon.ped, TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(sJimmy.ped)
			TASK_SMART_FLEE_COORD(sJimmy.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 200.0, -1)
			SET_PED_KEEP_TASK(sJimmy.ped, TRUE)
		ENDIF

		SWITCH reason
			CASE FAILED_GENERIC
			BREAK
			
			CASE FAILED_DESTROYED_A_CAR
				strFailText = "AR1_FAILCAR2"
			BREAK
			
			CASE FAILED_DESTROYED_CAR
				strFailText = "AR1_FAILCAR"
			BREAK
			
			CASE FAILED_DESTROYED_BOTH_CARS
				strFailText = "AR1_FAILCARS"
			BREAK
			
			CASE FAILED_KILLED_BUDDY
				strFailText = "AR1_FAILBUDDY"
			BREAK
			
			CASE FAILED_LOST_BUDDY
				strFailText = "AR1_FAILLOST"
			BREAK
			
			CASE FAILED_ABANDONED_LAMAR
				strFailText = "AR1_FAILLEFT"
			BREAK
			
			CASE FAILED_DESTROYED_FRANKLINS_CAR
				strFailText = "CMN_GENDEST"
			BREAK
			
			CASE FAILED_CAR_STUCK
				strFailText = "AR1_STUCK"
			BREAK
			
			CASE FAILED_FRANKLINS_CAR_STUCK
				strFailText = "AR1_FRANSTUCK"
			BREAK
			
			CASE FAILED_ALERTED_COPS
				strFailText = "AR1_FAILCOPS"
			BREAK
			
			CASE FAILED_DAMAGED_CARS_BEFORE_RACE
				strFailText = "AR1_FAILDAM"
			BREAK
			
			CASE FAILED_DAMAGED_CARS_AFTER_DROPOFF
				strFailText = "AR1_FAILDAMAF"
			BREAK
			
			CASE FAILED_ABANDONED_CAR
				strFailText = "AR1_FAILCARLEF"
			BREAK
			
			CASE FAILED_TRASH_SHOWROOM
				strFailText = "AR1_FAILSHOW1"
			BREAK
			
			CASE FAILED_DROVE_IN_SHOWROOM
				strFailText = "AR1_FAILSHOW2"
			BREAK
			
			CASE FAILED_BROKEN_INTO_CAR
				strFailText = "AR1_FAILSHOW3"
			BREAK
			
			CASE FAILED_DISTURBED_SHOWROOM
				strFailText = "AR1_FAILSHOW4"
			BREAK
			
			CASE FAILED_KILLED_SIMEON
				strFailText = "AR1_FAILSIMDEAD"
			BREAK
			
			CASE FAILED_KILLED_JIMMY
				strFailText = "AR1_FAILJIMDEAD"
			BREAK
			
			CASE FAILED_LAMARS_CAR_DAMAGED
				strFailText = "AR1_FAILDAM2"
			BREAK
			
			CASE FAILED_LED_COPS_TO_SHOWROOM
				strFailText = "AR1_FCOPSDEAL"
			BREAK
			
			CASE FAILED_DISTURBED_DEAL
				strFailText = "AR1_FAILSHOW5"
			BREAK
		ENDSWITCH
		
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailText)
		
		WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
			WAIT(0)
		ENDWHILE
		
		//Deal with group members and same vehicle cleanup.
		IF NOT IS_ENTITY_DEAD(sLamar.ped)
			IF IS_PED_GROUP_MEMBER(sLamar.ped, PLAYER_GROUP_ID())
			OR ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), sLamar.ped)
				REMOVE_PED(sLamar.ped, TRUE)
			ENDIF
		ENDIF
		
		bMissionFailed = TRUE
		
		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
		MISSION_CLEANUP() // must only take 1 frame and terminate the thread
	ENDIF
ENDPROC


PROC JUMP_TO_STAGE(MISSION_STAGE stage, BOOL bIsDebugJump = FALSE)
	DO_FADE_OUT_WITH_WAIT()
	iCurrentEvent = 0
	eMissionStage = stage 
	eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
	MISSION_CLEANUP()
	
	//Update mission checkpoint in case they skipped the stages where it gets set.
	IF bIsDebugJump
		IF eMissionStage >= STAGE_GO_TO_HOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_GO_HOME, "GO_TO_HOUSE", TRUE)
		ELIF eMissionStage >= STAGE_GO_TO_GARAGE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_LOSE_COPS, "LOSE_COPS")
		ELIF eMissionStage >= STAGE_CHASE_MID_POINT
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_MID_CHASE, "CHASE_MID_POINT")
		ELSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "CHOOSE_CAR")
		ENDIF
	ENDIF
ENDPROC


PROC ROTATE_FERRIS_WHEEL(BOOL bForceRotate = FALSE)
	CONST_FLOAT FERRIS_CAR_RADIUS					15.3
	CONST_FLOAT ANGLE_BETWEEN_FERRIS_CARS			22.5

	VECTOR vCurrentRot = GET_ENTITY_ROTATION(objFerrisWheel)
	
	vCurrentRot.x = vCurrentRot.x -@ 5.0
	
	SET_ENTITY_ROTATION(objFerrisWheel, <<vCurrentRot.x, vCurrentRot.y, vCurrentRot.z>>)
	
	INT i = 0
	REPEAT COUNT_OF(objFerrisCars) i
		FLOAT fAttachAngle = i * ANGLE_BETWEEN_FERRIS_CARS
		VECTOR vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objFerrisWheel, <<0.0, FERRIS_CAR_RADIUS * COS(fAttachAngle), FERRIS_CAR_RADIUS * SIN(fAttachAngle)>>)
		
		IF bForceRotate
			SET_ENTITY_COORDS_NO_OFFSET(objFerrisCars[i], vPos)
		ELSE
			SLIDE_OBJECT(objFerrisCars[i], vPos, <<1.0, 1.0, 1.0>>, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_CLOSEST_FERRIS_CAR_TO_COORDS(VECTOR vPos)
	INT iClosest = -1
	INT i = 0
	FLOAT fClosestDist = 0.0
	
	REPEAT COUNT_OF(objFerrisCars) i
		FLOAT fDist = VDIST2(vPos, GET_ENTITY_COORDS(objFerrisCars[i]))
		
		IF fDist < fClosestDist
		OR iClosest = -1
			fClosestDist = fDist
			iClosest = i
		ENDIF
	ENDREPEAT
	
	RETURN iClosest
ENDFUNC

FUNC BOOL SETUP_REQ_BANK_GATES(BOOL bJustRequestAssets = FALSE, BOOL bOnlyCreateCollision = FALSE)
	IF NOT DOES_ENTITY_EXIST(objBankGateCollision[0])
		REQUEST_MODEL(modelBankGate)
		REQUEST_MODEL(modelBankGateCollision)
		
		IF HAS_MODEL_LOADED(modelBankGate)
		AND HAS_MODEL_LOADED(modelBankGateCollision)
			IF NOT bJustRequestAssets
				IF NOT bOnlyCreateCollision
					objBankGates[0] = CREATE_OBJECT(modelBankGate, <<-72.785, -682.290, 32.77>>)
					objBankGates[1] = CREATE_OBJECT(modelBankGate, <<25.0, -664.5, 30.76>>)
					
					SET_ENTITY_HEADING(objBankGates[0], 69.743)
					SET_ENTITY_HEADING(objBankGates[1], 339.996)
					
					FREEZE_ENTITY_POSITION(objBankGates[0], TRUE)
					FREEZE_ENTITY_POSITION(objBankGates[1], TRUE)
					
					SET_ENTITY_LOD_DIST(objBankGates[0], 250)
					SET_ENTITY_LOD_DIST(objBankGates[1], 250)
				ENDIF
				
				objBankGateCollision[0] = CREATE_OBJECT(modelBankGateCollision, <<-72.785, -682.290, 32.87>>)
				objBankGateCollision[1] = CREATE_OBJECT(modelBankGateCollision, <<25.0, -664.5, 30.86>>)
				
				SET_ENTITY_HEADING(objBankGateCollision[0], -110.257)
				SET_ENTITY_HEADING(objBankGateCollision[1], 339.996)
				
				FREEZE_ENTITY_POSITION(objBankGateCollision[0], TRUE)
				FREEZE_ENTITY_POSITION(objBankGateCollision[1], TRUE)
				
				SET_ENTITY_LOD_DIST(objBankGateCollision[0], 250)
				SET_ENTITY_LOD_DIST(objBankGateCollision[1], 250)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(modelBankGate)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelBankGateCollision)
			ENDIF
		
			RETURN TRUE
		ENDIF	
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SETUP_REQ_FERRIS_WHEEL(BOOL bJustRequestAssets = FALSE)
	CONST_FLOAT FERRIS_CAR_RADIUS					15.3
	CONST_FLOAT ANGLE_BETWEEN_FERRIS_CARS			22.5

	IF NOT DOES_ENTITY_EXIST(objFerrisWheel)
		REQUEST_MODEL(modelFerrisWheel)
		REQUEST_MODEL(modelFerrisCar)
		
		IF HAS_MODEL_LOADED(modelFerrisWheel)
		AND HAS_MODEL_LOADED(modelFerrisCar)
			IF bJustRequestAssets
				RETURN TRUE
			ELSE
				objFerrisWheel = CREATE_OBJECT_NO_OFFSET(modelFerrisWheel, vFerrisWheelCentre)
				SET_ENTITY_LOD_DIST(objFerrisWheel, 500)
				FREEZE_ENTITY_POSITION(objFerrisWheel, TRUE)
				
				INT i = 0
				REPEAT COUNT_OF(objFerrisCars) i
					FLOAT fAttachAngle = i * ANGLE_BETWEEN_FERRIS_CARS
					VECTOR vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objFerrisWheel, <<0.0, FERRIS_CAR_RADIUS * COS(fAttachAngle), FERRIS_CAR_RADIUS * SIN(fAttachAngle)>>)
					
					objFerrisCars[i] = CREATE_OBJECT_NO_OFFSET(modelFerrisCar, vPos)
					SET_ENTITY_LOD_DIST(objFerrisCars[i], 500)
					FREEZE_ENTITY_POSITION(objFerrisCars[i], TRUE)
				ENDREPEAT
				
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheel)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisCar)
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_FERRIS_WHEEL_PEDS(BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(pedCreditsFerrisWheel[0])
		REQUEST_MODEL(modelFerrisWheelPed1)
		REQUEST_MODEL(modelFerrisWheelPed2)
		REQUEST_ANIM_DICT(strFerrisWheelAnims)
		
		IF HAS_MODEL_LOADED(modelFerrisWheelPed1)
		AND HAS_MODEL_LOADED(modelFerrisWheelPed2)
		AND HAS_ANIM_DICT_LOADED(strFerrisWheelAnims)
			IF bJustRequestAssets
				RETURN TRUE
			ELSE
				IF DOES_ENTITY_EXIST(objFerrisWheel)
					pedCreditsFerrisWheel[0] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed1, vFerrisWheelCentre + <<0.0, 0.0, 1.0>>)
					pedCreditsFerrisWheel[1] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed2, vFerrisWheelCentre + <<0.0, 0.0, 2.0>>)
					pedCreditsFerrisWheel[2] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed1, vFerrisWheelCentre + <<0.0, 0.0, 3.0>>)
					pedCreditsFerrisWheel[3] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed1, vFerrisWheelCentre + <<0.0, 0.0, 4.0>>)
					pedCreditsFerrisWheel[4] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed2, vFerrisWheelCentre + <<0.0, 0.0, 5.0>>)
					pedCreditsFerrisWheel[5] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed1, vFerrisWheelCentre + <<0.0, 0.0, 6.0>>)
					pedCreditsFerrisWheel[6] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed1, vFerrisWheelCentre + <<0.0, 0.0, 7.0>>)
					pedCreditsFerrisWheel[7] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed2, vFerrisWheelCentre + <<0.0, 0.0, 8.0>>)
					pedCreditsFerrisWheel[8] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed1, vFerrisWheelCentre + <<0.0, 0.0, 9.0>>)
					pedCreditsFerrisWheel[9] = CREATE_PED(PEDTYPE_MISSION, modelFerrisWheelPed2, vFerrisWheelCentre + <<0.0, 0.0, 10.0>>)
					
					INT iFerrisCar
					TEXT_LABEL_31 strAnim
					VECTOR vSceneRot
					
					INT i = 0
					REPEAT COUNT_OF(pedCreditsFerrisWheel) i
						IF i = 0
							strAnim = "Stand_Idle_1_PEDA"
							iFerrisCar = 10
							vSceneRot = <<0.0, 0.0, 180.0>>
						ELIF i = 1
							strAnim = "Stand_Idle_1_PEDB"
							iFerrisCar = 10
							vSceneRot = <<0.0, 0.0, 180.0>>
						ELIF i = 2
							strAnim = "Stand_Idle_2_PEDA"
							iFerrisCar = 9
							vSceneRot = <<0.0, 0.0, 300.0>>
						ELIF i = 3
							strAnim = "Stand_Idle_2_PEDB"
							iFerrisCar = 9
							vSceneRot = <<0.0, 0.0, 180.0>>
						ELIF i = 4
							strAnim = "Stand_Idle_1_PEDA"
							iFerrisCar = 7
							vSceneRot = <<0.0, 0.0, 240.0>>
						ELIF i = 5
							strAnim = "Stand_Idle_2_PEDB"
							iFerrisCar = 5
							vSceneRot = <<0.0, 0.0, 0.0>>
						ELIF i = 6
							strAnim = "Stand_Idle_1_PEDB"
							iFerrisCar = 4
							vSceneRot = <<0.0, 0.0, 0.0>>
						ELIF i = 7
							strAnim = "Stand_Idle_2_PEDB"
							iFerrisCar = 0
							vSceneRot = <<0.0, 0.0, 0.0>>
						ELIF i = 8
							strAnim = "Stand_Idle_2_PEDA"
							iFerrisCar = 15
							vSceneRot = <<0.0, 0.0, 60.0>>
						ELIF i = 9
							strAnim = "Stand_Idle_1_PEDA"
							iFerrisCar = 3
							vSceneRot = <<0.0, 0.0, 60.0>>
						ENDIF
						
						SET_ENTITY_INVINCIBLE(pedCreditsFerrisWheel[i], TRUE)
						
						iSyncSceneFerrisWheel[i] = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, vSceneRot)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSyncSceneFerrisWheel[i], objFerrisCars[iFerrisCar], -1)
						TASK_SYNCHRONIZED_SCENE(pedCreditsFerrisWheel[i], iSyncSceneFerrisWheel[i], strFerrisWheelAnims, strAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, RBF_FALLING | RBF_IMPACT_OBJECT)
						SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneFerrisWheel[i], TRUE)
					ENDREPEAT
						
					SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheelPed1)
					SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheelPed2)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_REQ_SHRINK_OFFICE_LOADED()
	IF interiorShrink = NULL
		interiorShrink = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-1906.7858, -573.7576, 19.0773>>, "v_psycheoffice")
	ELSE
		IF NOT IS_INTERIOR_READY(interiorShrink)
			#IF IS_DEBUG_BUILD
				PRINTLN("Armenian1.sc - Waiting for shrink interior to load.")
			#ENDIF
		
			PIN_INTERIOR_IN_MEMORY(interiorShrink)
		ELSE		
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_JIMMY(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(sJimmy.ped)
		REQUEST_NPC_PED_MODEL(CHAR_JIMMY)
			
		IF HAS_NPC_PED_MODEL_LOADED(CHAR_JIMMY)
			IF NOT bJustRequestAssets
				IF CREATE_NPC_PED_ON_FOOT(sJimmy.ped, CHAR_JIMMY, vPos, fHeading)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_TORSO, 4, 0)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_BERD, 1, 0)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_HAIR, 2, 0)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_LEG, 0, 1)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_FEET, 0, 1)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_SPECIAL, 3, 0)
					SET_PED_COMPONENT_VARIATION(sJimmy.ped, PED_COMP_DECL, 2, 0)
				
					SET_PED_CAN_BE_TARGETTED(sJimmy.ped, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sJimmy.ped, RELGROUPHASH_PLAYER)
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_SIMEON(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(sSimeon.ped)
		REQUEST_NPC_PED_MODEL(CHAR_SIMEON)
			
		IF HAS_NPC_PED_MODEL_LOADED(CHAR_SIMEON)
			IF NOT bJustRequestAssets
				IF CREATE_NPC_PED_ON_FOOT(sSimeon.ped, CHAR_SIMEON, vPos, fHeading)
					SET_PED_COMPONENT_VARIATION(sSimeon.ped, PED_COMP_TORSO, 1, 0)
				
					SET_PED_CAN_BE_TARGETTED(sSimeon.ped, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sSimeon.ped, RELGROUPHASH_PLAYER)
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_LAMAR(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bInitialiseEvenIfAlreadyExists = FALSE)
	IF NOT DOES_ENTITY_EXIST(sLamar.ped)
	OR bInitialiseEvenIfAlreadyExists
		BOOL bLamarExists
		
		IF NOT DOES_ENTITY_EXIST(sLamar.ped)
			IF CREATE_NPC_PED_ON_FOOT(sLamar.ped, CHAR_LAMAR, vPos, fHeading)
				bLamarExists = TRUE
			ENDIF
		ELSE
			bLamarExists = TRUE
		ENDIF
		
		IF bLamarExists
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_HAIR, 0, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_HAND, 2, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(sLamar.ped, PED_COMP_SPECIAL, 0, 0)
		
			SET_PED_CAN_BE_TARGETTED(sLamar.ped, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(sLamar.ped, RELGROUPHASH_PLAYER)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 5, sLamar.ped, "LAMAR")
			
			RETURN TRUE
		ENDIF
	ELSE
		IF sConversationPeds.PedInfo[5].Index != sLamar.ped
			ADD_PED_FOR_DIALOGUE(sConversationPeds, 5, sLamar.ped, "LAMAR")
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_NINEF(VECTOR vPos, FLOAT fHeading = 0.0)
	IF NOT DOES_ENTITY_EXIST(sMainCars[0].veh)
		REQUEST_MODEL(modelMainCar1)
		
		IF HAS_MODEL_LOADED(modelMainCar1)
			sMainCars[0].veh = CREATE_VEHICLE(modelMainCar1, vPos, fHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[0].veh)
			SET_VEHICLE_DIRT_LEVEL(sMainCars[0].veh, 0.0)
			SET_VEHICLE_COLOURS(sMainCars[0].veh, 111, 111)
			SET_VEHICLE_EXTRA_COLOURS(sMainCars[0].veh, 111, 0)
			SET_ENTITY_HEALTH(sMainCars[0].veh, MAX_CAR_HEALTH)
			SET_VEHICLE_STRONG(sMainCars[0].veh, TRUE)
			SET_VEHICLE_HAS_STRONG_AXLES(sMainCars[0].veh, TRUE)
			SET_VEHICLE_TYRES_CAN_BURST(sMainCars[0].veh, FALSE)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(sMainCars[0].veh, FALSE)
			SET_VEHICLE_NUMBER_PLATE_TEXT(sMainCars[0].veh, "5912FL34")
			SET_VEHICLE_CAN_LEAK_OIL(sMainCars[0].veh, FALSE)
			SET_VEHICLE_CAN_LEAK_PETROL(sMainCars[0].veh, FALSE)
			SET_CAN_AUTO_VAULT_ON_ENTITY(sMainCars[0].veh, FALSE)
			SET_CAN_CLIMB_ON_ENTITY(sMainCars[0].veh, FALSE)
			SET_VEHICLE_ACTIVE_DURING_PLAYBACK(sMainCars[0].veh, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_RAPIDGT(VECTOR vPos, FLOAT fHeading = 0.0)
	IF NOT DOES_ENTITY_EXIST(sMainCars[1].veh)
		REQUEST_MODEL(modelMainCar2)
		
		IF HAS_MODEL_LOADED(modelMainCar2)
			sMainCars[1].veh = CREATE_VEHICLE(modelMainCar2, vPos, fHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[1].veh)
			SET_VEHICLE_DIRT_LEVEL(sMainCars[1].veh, 0.0)
			SET_VEHICLE_COLOURS(sMainCars[1].veh, 28, 28)
			SET_VEHICLE_EXTRA_COLOURS(sMainCars[1].veh, 23, 0)
			SET_VEHICLE_STRONG(sMainCars[1].veh, TRUE)
			SET_VEHICLE_HAS_STRONG_AXLES(sMainCars[1].veh, TRUE)
			SET_VEHICLE_TYRES_CAN_BURST(sMainCars[1].veh, FALSE)
			SET_ENTITY_HEALTH(sMainCars[1].veh, MAX_CAR_HEALTH)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(sMainCars[1].veh, FALSE)
			SET_VEHICLE_NUMBER_PLATE_TEXT(sMainCars[1].veh, "2603AM56")
			SET_VEHICLE_CAN_LEAK_OIL(sMainCars[1].veh, FALSE)
			SET_VEHICLE_CAN_LEAK_PETROL(sMainCars[1].veh, FALSE)
			SET_CAN_AUTO_VAULT_ON_ENTITY(sMainCars[1].veh, FALSE)
			SET_CAN_CLIMB_ON_ENTITY(sMainCars[1].veh, FALSE)
			SET_VEHICLE_ACTIVE_DURING_PLAYBACK(sMainCars[1].veh, TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar2)
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_FRANKLINS_CAR(VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE)
	IF NOT IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
		REQUEST_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
		
		IF HAS_PLAYER_VEH_MODEL_LOADED(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
			IF bJustRequestAssets
				RETURN TRUE
			ELIF CREATE_PLAYER_VEHICLE(vehFranklinsCar, CHAR_FRANKLIN, vPos, fHeading, TRUE, VEHICLE_TYPE_CAR)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehFranklinsCar, SC_DOOR_FRONT_LEFT, FALSE)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehFranklinsCar, SC_DOOR_FRONT_RIGHT, FALSE)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehFranklinsCar, FALSE)
				SET_VEHICLE_STRONG(vehFranklinsCar, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehFranklinsCar, TRUE)
				SET_PLAYER_VEHICLE_ALARM_AUDIO_ACTIVE(vehFranklinsCar, TRUE)
				
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_REQ_CAR_CHOICES()
	//If the choice hasn't been made yet, use the saved global to do it (the global defaults to the NINEF)
	IF iPlayersCar = iBuddiesCar
		iPlayersCar = g_iArmenian1VehicleChoice
		
		IF iPlayersCar = NINEF_INDEX
			iBuddiesCar = RAPIDGT_INDEX
		ELSE
			iBuddiesCar = NINEF_INDEX
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_REQ_ARRIVAL_RECORDING_CHOICES()
	//Initialise recording indices depending on which cars were picked, these are used for streaming cutscenes during and after the chase.
	IF iBuddiesCar = RAPIDGT_INDEX
		iCarrecBuddyEscape = CARREC_BUDDY_ESCAPE_2
		iCarrecBuddyArrive = 313
		iCarrecPlayerArrive = 312
	ELSE
		iCarrecBuddyEscape = CARREC_BUDDY_ESCAPE
		iCarrecBuddyArrive = 311
		iCarrecPlayerArrive = 310
	ENDIF
ENDPROC

PROC SETUP_REQ_PUT_PLAYER_INTO_CHOSEN_CAR()
	IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_REQ_LAMAR_INTO_CHOSEN_CAR()
	IF NOT IS_PED_INJURED(sLamar.ped)
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
			IF NOT IS_PED_IN_VEHICLE(sLamar.ped, sMainCars[iBuddiesCar].veh)
				SET_PED_INTO_VEHICLE(sLamar.ped, sMainCars[iBuddiesCar].veh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SETUP_REQ_LOAD_MAIN_CHASE_RECORDING()
	IF iBuddiesCar = RAPIDGT_INDEX
		iCarrecMain = CARREC_BUDDY_ALTERNATE //Same recording, shifted for new car
	ELSE
		iCarrecMain = CARREC_BUDDY
	ENDIF
	
	REQUEST_VEHICLE_RECORDING(iCarrecMain, strCarrec)
	
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecMain, strCarrec)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
	IF IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
	AND IS_VEHICLE_DRIVEABLE(sMainCars[1].veh)
		LOWER_CONVERTIBLE_ROOF(sMainCars[0].veh, TRUE)
		LOWER_CONVERTIBLE_ROOF(sMainCars[1].veh, TRUE)
	ENDIF
ENDPROC

PROC SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
	IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
		SET_ENTITY_HEADING(sMainCars[iBuddiesCar].veh, fLamarsCarShowroomHeading)
		SET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh, vLamarsCarInShowroom)
		FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, TRUE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[iBuddiesCar].veh, FALSE)
	ENDIF
ENDPROC

FUNC BOOL SETUP_SHOWROOM_CARS(BOOL bJustRequestAssets = FALSE)
	IF NOT bShowroomCarsBlocked
		VECTOR vRoomCentre 
		vRoomCentre = vRoomCorner1 + <<vRoomCorner2.x - vRoomCorner1.x, vRoomCorner2.y - vRoomCorner2.y, vRoomCorner2.z - vRoomCorner2.z>>
	
		CLEAR_AREA_OF_VEHICLES(vRoomCentre, 30.0)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vRoomCorner1, vRoomCorner2, FALSE)
		bShowroomCarsBlocked = TRUE
	ENDIF
	
	REQUEST_MODEL(BJXL)
	REQUEST_MODEL(NINEF)
	
	IF HAS_MODEL_LOADED(BJXL)
	AND HAS_MODEL_LOADED(NINEF)
		IF NOT bJustRequestAssets
			IF NOT DOES_ENTITY_EXIST(vehShowroomCars[0])
				vehShowroomCars[0] = CREATE_VEHICLE(NINEF, <<-49.9, -1094.7, 26.1366>>, 96.8607) //Right of Jimmy's car
				SET_VEHICLE_COLOURS(vehShowroomCars[0], 31, 0)
				SET_VEHICLE_EXTRA_COLOURS(vehShowroomCars[0], 42, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehShowroomCars[0], TRUE)
				//SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[0], <<-49.9, -1094.7, 26.1>>)
				
				vehShowroomCars[1] = CREATE_VEHICLE(BJXL, <<-46.5, -1097.5, 26.35>>, 108.8107) //Jimmy's car
				SET_VEHICLE_COLOURS(vehShowroomCars[1], 126, 126)
				SET_VEHICLE_EXTRA_COLOURS(vehShowroomCars[1], 126, 0)
				SET_VEHICLE_NUMBER_PLATE_TEXT(vehShowroomCars[1], "57EIG117")
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehShowroomCars[1], TRUE)
				//SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[1], <<-46.5, -1097.5, 26.4>>)
				
				vehShowroomCars[2] = CREATE_VEHICLE(NINEF, <<-41.7, -1099.5, 26.0304>>, 137.6117) //Left of Jimmy's car
				SET_VEHICLE_COLOURS(vehShowroomCars[2], 1, 1)
				SET_VEHICLE_EXTRA_COLOURS(vehShowroomCars[2], 15, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehShowroomCars[2], TRUE)
				//SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[2], <<-41.7, -1099.5, 26.1>>)
					
				vehShowroomCars[3] = CREATE_VEHICLE(BJXL, <<-36.8, -1101.2, 26.0912>>, 151.7322)
				SET_VEHICLE_COLOURS(vehShowroomCars[3], 1, 111)
				SET_VEHICLE_EXTRA_COLOURS(vehShowroomCars[3], 4, 0)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehShowroomCars[3], TRUE)
				//SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[3], <<-36.8, -1101.2, 26.1>>)
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
			SET_MODEL_AS_NO_LONGER_NEEDED(NINEF)
			
			INT i = 0
			REPEAT COUNT_OF(vehShowroomCars) i
				IF IS_VEHICLE_DRIVEABLE(vehShowroomCars[i])
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehShowroomCars[i], FALSE)
					//FREEZE_ENTITY_POSITION(vehShowroomCars[i], TRUE)
				ENDIF
			ENDREPEAT
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_REQ_ABILITY_BAR_NEARLY_FULL()
	sRageData.fCurrentRage = 99.0
	SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), sRageData.fCurrentRage / 100.0, TRUE)
ENDPROC

PROC SETUP_REQ_REMOVE_BANK_SHUTTERS()
	REMOVE_IPL("DT1_03_Shutter")
	SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS, BUILDINGSTATE_DESTROYED)
ENDPROC

FUNC BOOL SETUP_CARS_OUTSIDE_SHOWROOM(BOOL bJustRequestAssets = FALSE)
	IF NOT DOES_ENTITY_EXIST(vehCarsOutsideShowroom[0])
		REQUEST_MODEL(BLISTA)
		
		IF HAS_MODEL_LOADED(BLISTA)
			IF NOT bJustRequestAssets
				CLEAR_AREA(<<-11.6041, -1080.8674, 25.6721>>, 20.0, TRUE)
					
				vehCarsOutsideShowroom[0] = CREATE_VEHICLE(BLISTA, <<-8.6907, -1082.0977, 25.6721>>, 128.8319)

				SET_MODEL_AS_NO_LONGER_NEEDED(BLISTA)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_SHOWROOM_FAIL()
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-51.4, -1097.6, 26.4>>) < 10000.0
		//Fail if the player drives a car into the showroom.
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF NOT IS_ENTITY_DEAD(veh)
				IF IS_ENTITY_IN_ANGLED_AREA(veh, <<-31.383488,-1108.248535,25.425732>>, <<-34.108696,-1115.328735,28.922327>>, 5.500000)
				OR IS_ENTITY_IN_ANGLED_AREA(veh, <<-33.779842,-1107.204712,25.445511>>, <<-57.514992,-1098.495728,29.823339>>, 6.000000)
				OR IS_ENTITY_IN_ANGLED_AREA(veh, <<-59.338482,-1090.887573,25.003147>>, <<-26.987392,-1103.201172,28.922327>>, 10.250000)
					IF NOT bCurrentVehicleIsOutsideShowroom
						MISSION_FAILED(FAILED_BROKEN_INTO_CAR)
					ELSE
						MISSION_FAILED(FAILED_DROVE_IN_SHOWROOM)
					ENDIF
					
					bCurrentVehicleIsOutsideShowroom = FALSE
				ELSE
					bCurrentVehicleIsOutsideShowroom = TRUE
				ENDIF
			ELSE
				bCurrentVehicleIsOutsideShowroom = FALSE
			ENDIF
		ENDIF
		
		//Fail if the player fires bullets inside the showroom.
		IF HAS_BULLET_IMPACTED_IN_AREA(<<-39.627991,-1099.240112,25.252590>>, 26.5)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, <<-39.627991,-1099.240112,25.252590>>, 26.5)
			MISSION_FAILED(FAILED_DISTURBED_SHOWROOM)
		ENDIF
		
		/*INT i = 0
		
		REPEAT COUNT_OF(objShowroomGlass) i
			IF NOT DOES_ENTITY_EXIST(objShowroomGlass[i])
				//PRINTLN("Window ", i, " doesn't exist")
			
				IF i = 0
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-59.8589, -1088.6685, 25.6516>>, 50.0, Prop_Showroom_Glass_1, TRUE)
				ELIF i = 1
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-50.2792, -1104.6271, 25.4362>>, 50.0, Prop_Showroom_Glass_2, TRUE)
				ELIF i = 2
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-45.5477, -1106.2428, 25.4362>>, 50.0, Prop_Showroom_Glass_2, TRUE)
				ELIF i = 3
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-36.3472, -1111.9200, 25.4362>>, 50.0, Prop_Showroom_Glass_2, TRUE)
				ELIF i = 4
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-47.1, -1105.7, 26.4>>, 50.0, Prop_Showroom_Glass_3, TRUE)
				ELIF i = 5
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-47.1, -1105.7, 26.4>>, 50.0, Prop_Showroom_Glass_4, TRUE)
				ELIF i = 6
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-47.1, -1105.7, 26.4>>, 50.0, Prop_Showroom_Glass_5, TRUE)
				ELIF i = 7
					objShowroomGlass[i] = GET_CLOSEST_OBJECT_OF_TYPE(<<-47.1, -1105.7, 26.4>>, 50.0, Prop_Showroom_Glass_6, TRUE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(objShowroomGlass[i])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(objShowroomGlass[i], PLAYER_PED_ID())
					MISSION_FAILED(FAILED_TRASH_SHOWROOM)
				ENDIF
			ENDIF
		ENDREPEAT*/
		
		//Fail if the player fires bullets into the showroom.
		IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-31.383488,-1108.248535,25.425732>>, <<-34.108696,-1115.328735,28.922327>>, 5.500000)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-33.779842,-1107.204712,25.445511>>, <<-57.514992,-1098.495728,29.823339>>, 6.000000)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-59.338482,-1090.887573,25.003147>>, <<-26.987392,-1103.201172,28.922327>>, 10.250000)
			MISSION_FAILED(FAILED_TRASH_SHOWROOM)
		ENDIF
	ENDIF
ENDPROC

PROC SCALEFORM_SETUP_SINGLE_LINE(STRING strName, FLOAT fFadeInDuration, FLOAT fFadeOutDuration, FLOAT fXAlign, FLOAT fYAlign, STRING strAlign)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SETUP_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXAlign)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fYAlign)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAlign)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_ADD_TEXT_TO_SINGLE_LINE(STRING strName, STRING strSingleLine, STRING strFont, STRING strHudColour, BOOL bUseLiteralString = TRUE)	//, STRING strLanguage = "en", FLOAT fYOffset = 0.0)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "ADD_TEXT_TO_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strSingleLine)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFont)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strHudColour)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_SINGLE_LINE(STRING strName, FLOAT fStepDuration)	//, STRING strAnimInStyle, FLOAT fAnimInValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SHOW_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_SINGLE_LINE(STRING strName, FLOAT fStepDuration)	//, STRING strAnimOutStyle, FLOAT fAnimOutValue)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "HIDE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_REMOVE_ALL_SINGLE_LINES()
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "REMOVE_ALL")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SETUP_CREDIT_BLOCK(STRING strName, FLOAT fX, FLOAT fY, STRING strAlign, FLOAT fFadeInDuration, FLOAT fFadeOutDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SETUP_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strAlign)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK(STRING strName, STRING strRole, FLOAT fXOffset, STRING strColour, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "ADD_ROLE_TO_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strRole)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXOffset)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strColour)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK(STRING strName, STRING strNames, FLOAT fXOffset, STRING strDelimiter, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "ADD_NAMES_TO_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strNames)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fXOffset)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strDelimiter)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_CREDIT_BLOCK(STRING strName, FLOAT fStepDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SHOW_CREDIT_BLOCK")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_CREDIT_BLOCK(STRING strName, FLOAT fStepDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "HIDE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStepDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_REMOVE_CREDIT_BLOCK(STRING strName)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "REMOVE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_REMOVE_ALL_CREDIT_BLOCKS()
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "REMOVE_ALL")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_LOGO(STRING strName, FLOAT fFadeInDuration, FLOAT fFadeOutDuration, FLOAT fLogoFadeInDuration, FLOAT fLogoFadeOutDuration, 
						 FLOAT fLogoFadeInDelay, FLOAT fLogoFadeOutDelay, FLOAT fLogoScaleDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "SHOW_LOGO")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeOutDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeInDelay)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeOutDelay)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoScaleDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_LOGO(STRING strName)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfCredits, "HIDE_LOGO")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC UPDATE_TEXT_FOR_CREDITS_SEQUENCE()
	INT iCutsceneTime = 0
	
	IF IS_CUTSCENE_PLAYING()
		iCutsceneTime = GET_CUTSCENE_TIME()
	ENDIF
	
	IF NOT bRequestedCreditsScaleform
		sfCredits = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
		bRequestedCreditsScaleform = TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
		IF bDebugDisplayCreditsText		
			STRING strAlign =  GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsAlign)
			STRING strRole1 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetRole1)
			STRING strRole2 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetRole2)
			STRING strRole3 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetRole3)
			STRING strNames1 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetNames1)
			STRING strNames2 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetNames2)
			STRING strNames3 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetNames3)
			STRING strColour = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsColour)
			TEXT_LABEL strName = iDebugCreditsIndex
		
			SCALEFORM_SETUP_CREDIT_BLOCK(strName, fDebugCreditsBlockX, fDebugCreditsBlockY, strAlign, 0.5, 0.5)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strRole1)
			AND NOT ARE_STRINGS_EQUAL(strRole1, "New text widget")
				SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK(strName, strRole1, fDebugRole1XOffset, strColour)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strNames1)
			AND NOT ARE_STRINGS_EQUAL(strNames1, "New text widget")
				SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK(strName, strNames1, fDebugNames1XOffset, ",")
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strRole2)
			AND NOT ARE_STRINGS_EQUAL(strRole2, "New text widget")
				SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK(strName, strRole2, fDebugRole2XOffset, strColour)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strNames2)
			AND NOT ARE_STRINGS_EQUAL(strNames2, "New text widget")
				SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK(strName, strNames2, fDebugNames2XOffset, ",")
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strRole3)
			AND NOT ARE_STRINGS_EQUAL(strRole3, "New text widget")
				SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK(strName, strRole3, fDebugRole3XOffset, strColour)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(strNames3)
			AND NOT ARE_STRINGS_EQUAL(strNames3, "New text widget")
				SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK(strName, strNames3, fDebugNames3XOffset, ",")
			ENDIF
			
			SCALEFORM_SHOW_CREDIT_BLOCK(strName, 0.1666)
		
			iDebugCreditsIndex++
			bDebugDisplayCreditsText = FALSE
		ENDIF
		
		IF bDebugTestCreditsSingleLine
			STRING strAlign =  GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsAlign)
			STRING strRole1 = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetRole1)
			STRING strColour = GET_CONTENTS_OF_TEXT_WIDGET(textWidgetCreditsColour)
			TEXT_LABEL strName = iDebugCreditsIndex
			
			SCALEFORM_SETUP_SINGLE_LINE(strName, 0.5, 0.5, fDebugCreditsBlockX, fDebugCreditsBlockY, strAlign)
			SCALEFORM_ADD_TEXT_TO_SINGLE_LINE(strName, strRole1, "$font5", strColour)
			SCALEFORM_SHOW_SINGLE_LINE(strName, 0.0)
			
			iDebugCreditsIndex++
			bDebugTestCreditsSingleLine = FALSE
		ENDIF
		
		IF bDebugTestCreditsLogo
			SCALEFORM_SHOW_LOGO("TITLE", 4.0, 1.0, 1.5, 1.0, 4.5, 1.5, 15.0)
			
			bDebugTestCreditsLogo = FALSE
		ENDIF
		
		IF bDebugTestCreditsLogoHide
			SCALEFORM_HIDE_LOGO("TITLE")
		
			bDebugTestCreditsLogoHide = FALSE
		ENDIF
		
		IF bDebugClearCreditsText
			SCALEFORM_HIDE_CREDIT_BLOCK("debug", 0.1666)
			
			bDebugClearCreditsText = FALSE
		ENDIF
	#ENDIF
	
	//COMMON OFFSET VALUES:
	//Indent names to the right of job title: give them a +60 x offset.
	//Left align with space = 150 x offset
	//Align just to right of centre = 580 x offset

	IF HAS_SCALEFORM_MOVIE_LOADED(sfCredits)
	AND NOT bHideCredits
		SWITCH iCreditsTextEvent
			CASE 0
				IF iCutsceneTime >= 5533
					SCALEFORM_SETUP_CREDIT_BLOCK("art", 105.0, 115.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("art", "Art Director", 0.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("art", "AARON GARBUT", 50.0, ",")
					SCALEFORM_SHOW_CREDIT_BLOCK("art", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 1
				IF iCutsceneTime >= 9266
					SCALEFORM_HIDE_CREDIT_BLOCK("art", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 2
				IF iCutsceneTime >= 10700
					SCALEFORM_SETUP_CREDIT_BLOCK("techdir", 120.0, 450.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("techdir", "Technical Director", 0.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("techdir", "ADAM FOWLER", 110.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("techdir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 3
				IF iCutsceneTime >= 14400
					SCALEFORM_HIDE_CREDIT_BLOCK("techdir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 4
				IF iCutsceneTime >= 17900
					SCALEFORM_SETUP_CREDIT_BLOCK("gamedes", 85.0, 225.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("gamedes", "Game Design", 0.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("gamedes", "LESLIE BENZIES,IMRAN SARWAR", 50.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("gamedes", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 5
				IF iCutsceneTime >= 21633
					SCALEFORM_HIDE_CREDIT_BLOCK("gamedes", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 6
				IF iCutsceneTime >= 26133
					IF ARE_WIDESCREEN_BORDERS_ACTIVE()
						SCALEFORM_SETUP_CREDIT_BLOCK("written", 0.0, 50.0, "left", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("written", 0.0, 40.0, "left", 0.333, 0.333)
					ENDIF
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("written", "Written By", 80.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("written", "DAN HOUSER,RUPERT HUMPHRIES,MICHAEL UNSWORTH", 145.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("written", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 7
				IF iCutsceneTime >= 30500 //30666
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("written")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 8
				IF iCutsceneTime >= 36533
					SCALEFORM_SETUP_CREDIT_BLOCK("assocart", 85.0, 430.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("assocart", "Associate Art Directors", 0.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("assocart", "ADAM COCHRANE,MICHAEL KANE", 75.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("assocart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 9
				IF iCutsceneTime >= 40233
					SCALEFORM_HIDE_CREDIT_BLOCK("assocart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 10
				IF iCutsceneTime >= 42700
					SCALEFORM_SETUP_CREDIT_BLOCK("assisart", 0.0, 440.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("assisart", "Assistant Art Director", 105.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("assisart", "IAN McQUE", 265.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("assisart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 11
				IF iCutsceneTime >= 46436
					SCALEFORM_HIDE_CREDIT_BLOCK("assisart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 12
				IF iCutsceneTime >= 49633
					SCALEFORM_SETUP_CREDIT_BLOCK("globart", 90.0, 390.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("globart", "Global Lead Technical Artist", 0.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("globart", "RICK STIRLING", 125.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("globart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 13
				IF iCutsceneTime >= 53000 //53233
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("globart")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 14
				IF iCutsceneTime >= 58133
					SCALEFORM_SETUP_CREDIT_BLOCK("leadcut", 0.0, 395.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("leadcut", "Lead Cutscene Animation", 100.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("leadcut", "DERMOT BAILIE,FELIPE BUSQUETS", 248.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("leadcut", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 15
				IF iCutsceneTime >= 61800
					SCALEFORM_HIDE_CREDIT_BLOCK("leadcut", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 16
				IF iCutsceneTime >= 65266
					SCALEFORM_SETUP_CREDIT_BLOCK("ingame", 95.0, 320.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("ingame", "Lead Ingame Animation", 0.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("ingame", "JIM JAGGER,MARK TENNANT", 115.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("ingame", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 17
				IF iCutsceneTime >= 69366
					SCALEFORM_HIDE_CREDIT_BLOCK("ingame", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 18
				IF iCutsceneTime >= 80133
					SCALEFORM_SETUP_CREDIT_BLOCK("animdir", 0.0, 410.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("animdir", "Animation Director", 190.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("animdir", "ROB NELSON", 295.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("animdir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 19
				IF iCutsceneTime >= 83833
					SCALEFORM_HIDE_CREDIT_BLOCK("animdir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 20
				IF iCutsceneTime >= 90166
					SCALEFORM_SETUP_CREDIT_BLOCK("cutdir", 0.0, 335.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("cutdir", "Cutscene Director", 115.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("cutdir", "ROD EDGE", 195.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("cutdir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 21
				IF iCutsceneTime >= 93933
					SCALEFORM_HIDE_CREDIT_BLOCK("cutdir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 22
				IF iCutsceneTime >= 95600
					SCALEFORM_SETUP_CREDIT_BLOCK("leadenv", 90.0, 410.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("leadenv", "Principal Lead Environment Artist", 0.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("leadenv", "WAYLAND STANDING", 110.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("leadenv", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 23
				IF iCutsceneTime >= 99200
					SCALEFORM_HIDE_CREDIT_BLOCK("leadenv", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 24
				IF iCutsceneTime >= 106766
					SCALEFORM_SETUP_CREDIT_BLOCK("envart", 85.0, 315.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("envart", "Lead Environment Artists", 0.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("envart", "MICHAEL PIRSO,ABHISHEK AGRAWAL,JODY PILESKI", 60.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("envart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 25
				IF iCutsceneTime >= 110566
					SCALEFORM_HIDE_CREDIT_BLOCK("envart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 26
				IF iCutsceneTime >= 114866
					SCALEFORM_SETUP_CREDIT_BLOCK("vehart", 0.0, 180.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("vehart", "Lead Vehicle Artist", 110.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("vehart", "JOLYON ORME", 170.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("vehart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 27
				IF iCutsceneTime >= 117300 //117566
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("vehart")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 28
				IF iCutsceneTime >= 124866
					SCALEFORM_SETUP_CREDIT_BLOCK("princart", 0.0, 370.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("princart", "Principal Artists", 105.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("princart", "DAVE COOPER,IAIN McNAUGHTON", 195.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("princart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 29
				IF iCutsceneTime >= 127400
					SCALEFORM_HIDE_CREDIT_BLOCK("princart", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 30
				IF iCutsceneTime >= 133733
					SCALEFORM_SETUP_CREDIT_BLOCK("princlight", 100.0, 75.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("princlight", "Principal Lighting Artist", 0.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("princlight", "OWEN SHEPHERD", 90.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("princlight", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 31
				IF iCutsceneTime >= 136366
					SCALEFORM_HIDE_CREDIT_BLOCK("princlight", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 32
				IF iCutsceneTime >= 139166
					IF GET_IS_HIDEF()
					AND NOT ARE_WIDESCREEN_BORDERS_ACTIVE()
						SCALEFORM_SETUP_CREDIT_BLOCK("outsource", 175.0, 15.0, "right", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("outsource", 175.0, 50.0, "right", 0.333, 0.333)
					ENDIF
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("outsource", "Outsource Manager", 0.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("outsource", "SCOTT WILSON", 75.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("outsource", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 33
				IF iCutsceneTime >= 141500 //141833
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("outsource")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 34
				IF iCutsceneTime >= 144500
					SCALEFORM_SETUP_CREDIT_BLOCK("2D", 0.0, 85.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("2D", "2D/UI Director", 110.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("2D", "STUART PETRI", 135.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("2D", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 35
				IF iCutsceneTime >= 147500 //147866
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("2D")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 36
				IF iCutsceneTime >= 152633
					SCALEFORM_SETUP_CREDIT_BLOCK("Music", 110.0, 405.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("Music", "Music Director", 0.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("Music", "CRAIG CONNER", 30.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("Music", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 37
				IF iCutsceneTime >= 156100
					SCALEFORM_HIDE_CREDIT_BLOCK("Music", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 38
				IF iCutsceneTime >= 156700
					SCALEFORM_SETUP_CREDIT_BLOCK("Musicpro", 250.0, 380.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("Musicpro", "Music Producer", 0.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("Musicpro", "IVAN PAVLOVICH", 45.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("Musicpro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 39
				IF iCutsceneTime >= 159600
					SCALEFORM_HIDE_CREDIT_BLOCK("Musicpro", 0.0)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 40
				IF iCutsceneTime >= 161900
					SCALEFORM_SETUP_CREDIT_BLOCK("audiodir", 0.0, 405.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("audiodir", "Audio Director", 180.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("audiodir", "MATTHEW SMITH", 215.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("audiodir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 41
				IF iCutsceneTime >= 165566
					SCALEFORM_HIDE_CREDIT_BLOCK("audiodir", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 42
				IF iCutsceneTime >= 166366
					IF GET_IS_HIDEF()
					AND NOT ARE_WIDESCREEN_BORDERS_ACTIVE()
						SCALEFORM_SETUP_CREDIT_BLOCK("leadaud", 40.0, 25.0, "right", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("leadaud", 40.0, 48.0, "right", 0.333, 0.333)
					ENDIF
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("leadaud", "Lead Audio Programmer", 0.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("leadaud", "ALASTAIR MACGREGOR", 55.0, ",")
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("leadaud", "Lead Sound Designers", 30.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("leadaud", "JEFFREY WHITCHER,STEVE DONOHOE", 85.0, ",")
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("leadaud", "Dialogue Supervisor", 60.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("leadaud", "WILL MORTON", 115.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("leadaud", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 43
				IF iCutsceneTime >= 168833
					SCALEFORM_HIDE_CREDIT_BLOCK("leadaud", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 44
				IF iCutsceneTime >= 170133
					SCALEFORM_SETUP_CREDIT_BLOCK("score", 0.0, 325.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("score", "Original Score", 80.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("score", "TANGERINE DREAM,WOODY JACKSON,ALCHEMIST & OH NO", 165.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("score", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 45
				IF iCutsceneTime >= 173033
					SCALEFORM_HIDE_CREDIT_BLOCK("score", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 46
				IF iCutsceneTime >= 175066
					SCALEFORM_SETUP_CREDIT_BLOCK("assotech", 80.0, 365.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("assotech", "Associate Technical Directors", 0.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("assotech", "PHIL HOOKER,KLAAS SCHILSTRA", 125.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("assotech", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 47
				IF iCutsceneTime >= 178700
					SCALEFORM_HIDE_CREDIT_BLOCK("assotech", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 48
				IF iCutsceneTime >= 180500
					IF GET_IS_HIDEF()
						SCALEFORM_SETUP_CREDIT_BLOCK("assitech", 0.0, 325.0, "left", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("assitech", 0.0, 305.0, "left", 0.333, 0.333)
					ENDIF
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("assitech", "Assistant Technical Directors", 190.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("assitech", "HUGUES ST-PIERRE,TOM SHEPHERD,BRETT LAMING,KEVIN HOARE", 245.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("assitech", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 49
				IF iCutsceneTime >= 183800
					SCALEFORM_HIDE_CREDIT_BLOCK("assitech", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 50
				IF iCutsceneTime >= 185066
					IF GET_IS_HIDEF()
						SCALEFORM_SETUP_CREDIT_BLOCK("proglead", 0.0, 280.0, "left", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("proglead", 0.0, 260.0, "left", 0.333, 0.333)
					ENDIF
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("proglead", "Programming Leads", 125.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("proglead", "ALEX HADJADJ,BEN LYONS,CHRIS SWINHOE,COLIN ENTWISTLE,DAVID HYND", 215.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("proglead", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 51
				IF iCutsceneTime >= 188100
					SCALEFORM_HIDE_CREDIT_BLOCK("proglead", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 52
				IF iCutsceneTime >= 189066
					IF GET_IS_HIDEF()
						SCALEFORM_SETUP_CREDIT_BLOCK("proglead2", 0.0, 280.0, "left", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("proglead2", 0.0, 260.0, "left", 0.333, 0.333)
					ENDIF

					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("proglead2", "Programming Leads", 125.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("proglead2", "DANIEL YELLAND,JOHN WHYTE,JONATHON ASHCROFT,MICHAEL KREHAN", 215.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("proglead2", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 53
				IF iCutsceneTime >= 192066
					SCALEFORM_HIDE_CREDIT_BLOCK("proglead2", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 54
				IF iCutsceneTime >= 193266
					IF GET_IS_HIDEF()
						SCALEFORM_SETUP_CREDIT_BLOCK("toollead", 125.0, 395.0, "right", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("toollead", 125.0, 355.0, "right", 0.333, 0.333)
					ENDIF

					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("toollead", "Lead Tools Programmers", 0.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("toollead", "DAVID MUIR,LUKE OPENSHAW", 75.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("toollead", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 55
				IF iCutsceneTime >= 195833
					SCALEFORM_HIDE_CREDIT_BLOCK("toollead", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 56
				IF iCutsceneTime >= 196733
					SCALEFORM_SETUP_CREDIT_BLOCK("scripter", 0.0, 355.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("scripter", "Lead Scripters", 120.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("scripter", "BENJAMIN ROLLINSON,KENNETH ROSS,MATTHEW BOOTON", 185.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("scripter", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 57
				IF iCutsceneTime >= 200033
					SCALEFORM_HIDE_CREDIT_BLOCK("scripter", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 58
				IF iCutsceneTime >= 200666
					SCALEFORM_SETUP_CREDIT_BLOCK("scripter2", 0.0, 355.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("scripter2", "Lead Scripters", 120.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("scripter2", "ROBERT BRAY,ROSS WALLACE", 185.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("scripter2", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 59
				IF iCutsceneTime >= 203966
					SCALEFORM_HIDE_CREDIT_BLOCK("scripter2", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 60
				IF iCutsceneTime >= 207166
					SCALEFORM_SETUP_CREDIT_BLOCK("studio", 90.0, 345.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("studio", "Studio Director", 0.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("studio", "ANDREW SEMPLE", 35.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("studio", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 61
				IF iCutsceneTime >= 210500 //210800
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("studio")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 62
				IF iCutsceneTime >= 212500
					SCALEFORM_SETUP_CREDIT_BLOCK("assispro", 0.0, 420.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("assispro", "Assistant Producer", 90.0, "HUD_COLOUR_FREEMODE")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("assispro", "WILLIAM MILLS", 125.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("assispro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 63
				IF iCutsceneTime >= 216036
					SCALEFORM_HIDE_CREDIT_BLOCK("assispro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 64
				IF iCutsceneTime >= 219400
					SCALEFORM_SETUP_CREDIT_BLOCK("copro", 105.0, 320.0, "right", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("copro", "Co-Producer", 0.0, "HUD_COLOUR_MICHAEL")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("copro", "IMRAN SARWAR", 35.0, ",")
					
					SCALEFORM_SHOW_CREDIT_BLOCK("copro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 65
				IF iCutsceneTime >= 222866
					SCALEFORM_HIDE_CREDIT_BLOCK("copro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 66
				IF iCutsceneTime > 225566
					SCALEFORM_SETUP_CREDIT_BLOCK("pro", 0.0, 125.0, "left", 0.333, 0.333)
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("pro", "Producer", 120.0, "HUD_COLOUR_FRANKLIN")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("pro", "LESLIE BENZIES", 165.0, ",")

					SCALEFORM_SHOW_CREDIT_BLOCK("pro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 67
				IF iCutsceneTime > 228500 //228900 is the exact time
				AND HAS_CUTSCENE_CUT_THIS_FRAME()
					SCALEFORM_REMOVE_CREDIT_BLOCK("pro")
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 68
				IF iCutsceneTime > 230666
					IF ARE_WIDESCREEN_BORDERS_ACTIVE()
						SCALEFORM_SETUP_CREDIT_BLOCK("execpro", 215.0, 50.0, "right", 0.333, 0.333)
					ELSE
						SCALEFORM_SETUP_CREDIT_BLOCK("execpro", 215.0, 45.0, "right", 0.333, 0.333)
					ENDIF
					SCALEFORM_ADD_ROLE_TO_CREDIT_BLOCK("execpro", "Executive Producer", 0.0, "HUD_COLOUR_TREVOR")
					SCALEFORM_ADD_NAMES_TO_CREDIT_BLOCK("execpro", "SAM HOUSER", 95.0, ",")

					SCALEFORM_SHOW_CREDIT_BLOCK("execpro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
			
			CASE 69
				IF iCutsceneTime > 235333
					SCALEFORM_HIDE_CREDIT_BLOCK("execpro", 0.1666)
					
					iCreditsTextEvent++
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfCredits, 255, 255, 255, 255)
	ENDIF
ENDPROC

/*PROC DO_NEW_LOAD_SCENE_FOR_CREDITS_SHOT(CREDITS_STAGE eStage, FLOAT fFarClip)
	VECTOR vNextCamPos, vNextCamRot, vNextCamDir
	
	GET_CREDITS_SHOT_CAM_POS_AND_ROT(eStage, vNextCamPos, vNextCamRot)
	vNextCamDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vNextCamRot)
	
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_START(vNextCamPos - vNextCamDir * 10.0, vNextCamDir, fFarClip, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
	ENDIF
ENDPROC*/

PROC UPDATE_CREDITS_CARS_MOVEMENT(BOOL bCarMustHaveDriver = FALSE)
	INT i = 0
	FLOAT fSpeed = 15.0
	REPEAT COUNT_OF(vehCreditsCars) i
		IF NOT IS_ENTITY_DEAD(vehCreditsCars[i])
		AND (NOT IS_VEHICLE_SEAT_FREE(vehCreditsCars[i], VS_DRIVER) OR NOT bCarMustHaveDriver)
			IF i = 1 OR i = 4 OR i = 7 OR i = 9 OR i = 10 OR i = 14 OR i = 18
				fSpeed = 17.5
			ELIF i = 6 OR i = 8 OR i = 11 OR i = 12 OR i = 13 OR i = 15 OR i = 16 OR i = 19
				fSpeed = 20.0
			ENDIF
			
			SET_VEHICLE_FORWARD_SPEED(vehCreditsCars[i], fSpeed)
		ENDIF
	ENDREPEAT
ENDPROC

PROC REQUEST_CREDITS_PED_ANIMS()
	REQUEST_ANIM_DICT("move_f@jogger")
	REQUEST_ANIM_DICT("amb@world_human_stand_mobile@female@text@base")
	REQUEST_ANIM_DICT("amb@world_human_tourist_mobile@female@base")
	REQUEST_ANIM_DICT("amb@world_human_hang_out_street@male_b@base")
	REQUEST_ANIM_DICT("amb@world_human_hang_out_street@male_a@base")
	REQUEST_ANIM_DICT("amb@world_human_smoking@female@base")
	REQUEST_ANIM_DICT("amb@world_human_hang_out_street@female_arms_crossed@base")
ENDPROC

FUNC BOOL HAVE_CREDITS_PED_ANIMS_LOADED()
	IF HAS_ANIM_DICT_LOADED("move_f@jogger")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_stand_mobile@female@text@base")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_tourist_mobile@female@base")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@male_b@base")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@male_a@base")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_smoking@female@base")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_hang_out_street@female_arms_crossed@base")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_CREDITS_PED_ANIMS()
	REMOVE_ANIM_DICT("move_f@jogger")
	REMOVE_ANIM_DICT("amb@world_human_stand_mobile@female@text@base")
	REMOVE_ANIM_DICT("amb@world_human_tourist_mobile@female@base")
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_b@base")
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@male_a@base")
	REMOVE_ANIM_DICT("amb@world_human_smoking@female@base")
	REMOVE_ANIM_DICT("amb@world_human_hang_out_street@female_arms_crossed@base")
ENDPROC

///Warps a ped and plays the given anims on them instantly. Intended use is for the credits, where a pool of peds are being warped around for each shot to fill out the background.
PROC WARP_PED_FOR_CREDITS_SHOT(PED_INDEX ped, VECTOR vPos, FLOAT fHeading, STRING strAnimDict, STRING strAnimName, BOOL bChangeAppearance = TRUE)
	IF NOT IS_PED_INJURED(ped)
		IF bChangeAppearance
			SET_PED_RANDOM_COMPONENT_VARIATION(ped)
		ENDIF
		FREEZE_ENTITY_POSITION(ped, FALSE)
		SET_ENTITY_VISIBLE(ped, TRUE)
		SET_ENTITY_COORDS(ped, vPos)
		SET_ENTITY_HEADING(ped, fHeading)
		TASK_PLAY_ANIM(ped, strAnimDict, strAnimName, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(ped)
	ENDIF
ENDPROC

///Warps a ped underground, used during the credits to hide peds when they're not needed for a particular shot.
PROC WARP_PED_UNDERGROUND(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		VECTOR vPos = GET_ENTITY_COORDS(ped)
		SET_ENTITY_COORDS(ped, <<vPos.x, vPos.y, -10.0>>)
		FREEZE_ENTITY_POSITION(ped, TRUE)
		SET_ENTITY_VISIBLE(ped, FALSE)
	ENDIF
ENDPROC

PROC UPDATE_CREDITS_EVENTS()
	INT iCutsceneTime = 0
	INT i = 0
	BOOL bCameraJustCut = FALSE
	//VECTOR vCamPos = GET_FINAL_RENDERED_CAM_COORD()
	
	IF IS_CUTSCENE_PLAYING()
		iCutsceneTime = GET_CUTSCENE_TIME()
	ENDIF
	
	IF HAS_CUTSCENE_CUT_THIS_FRAME()
		bCameraJustCut = TRUE
	ENDIF
	
	//Force the stage if we're before the credits sequence.
	IF iCutsceneTime < 107000
		eCreditsStage = CREDITS_STAGE_0_SHRINK
	ENDIF
	
	SWITCH eCreditsStage
		CASE CREDITS_STAGE_0_SHRINK
			IF iCutsceneTime > 107000
				IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_MONTAGE_MUTES")
					START_AUDIO_SCENE("ARM_1_MONTAGE_MUTES")
				ENDIF

				SET_VEHICLE_POPULATION_BUDGET(3)
				SET_PED_POPULATION_BUDGET(1)

				iRandomCarCreationEvent = 0
				eCreditsStage = CREDITS_STAGE_1_LEAVE_SHRINK
			ENDIF
			
			IF iCutsceneTime > 102000
				REQUEST_MODEL(CAVALCADE)
				REQUEST_MODEL(ALPHA)
				REQUEST_MODEL(modelCreditsMale)
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_1_LEAVE_SHRINK	
			REQUEST_MODEL(CAVALCADE)
			REQUEST_MODEL(ALPHA)
			REQUEST_MODEL(modelCreditsMale)
		
			IF iCutsceneTime > 110000
				//NG Request: Have lots more cars. Don't stick any drivers in most of them to help with streaming/execution time.
				IF iRandomCarCreationEvent < COUNT_OF(vehCreditsCars)
					IF iRandomCarCreationEvent = 0
						CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1975.175293,-466.151398,8.161919>>, <<-1753.042358,-647.606628,15.556465>>, 40.250000)
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1964.729688,-495.260382,11.494138>>, -129.5, iRandomCarCreationEvent, 15.0, TRUE)
					ELIF iRandomCarCreationEvent = 1
						CREATE_RANDOM_CAR_AT_POS(ALPHA, modelCreditsMale, <<-1938.364746,-509.801208,10.946412>>, -129.5, iRandomCarCreationEvent, 17.5, TRUE)
					ELIF iRandomCarCreationEvent = 2
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1862.437012,-538.817749,11.623542>>, 50.7, iRandomCarCreationEvent, 15.0, TRUE)
					ELIF iRandomCarCreationEvent = 3
						CREATE_RANDOM_CAR_AT_POS(ALPHA, modelCreditsMale, <<-1814.225171,-577.825586,11.201730>>, 50.7, iRandomCarCreationEvent, 15.0)
					ELIF iRandomCarCreationEvent = 4
						CREATE_RANDOM_CAR_AT_POS(ALPHA, modelCreditsMale, <<-1889.468872,-523.005249,11.196733>>, 50.7, iRandomCarCreationEvent, 17.5)
					ELIF iRandomCarCreationEvent = 5
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1896.674072,-509.762024,11.152614>>, 50.7, iRandomCarCreationEvent, 15.0)
					ELIF iRandomCarCreationEvent = 6
						CREATE_RANDOM_CAR_AT_POS(ALPHA, modelCreditsMale, <<-1872.837036,-544.377747,11.216076>>, 50.7, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 7
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1902.065308,-539.558533,11.041574>>, -129.5, iRandomCarCreationEvent, 17.5)
					ELIF iRandomCarCreationEvent = 8
						CREATE_RANDOM_CAR_AT_POS(ALPHA, modelCreditsMale, <<-1939.535156,-500.975464,11.265397>>, -129.5, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 9
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1883.366943,-554.134827,10.846096>>, -129.5, iRandomCarCreationEvent, 17.5)
					ELIF iRandomCarCreationEvent = 10
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1953.545044,-496.880798,12.011486>>, -129.5, iRandomCarCreationEvent, 17.5)
					ELIF iRandomCarCreationEvent = 11
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1971.761841,-474.743469,11.883854>>, -129.5, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 12
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1915.927734,-520.575684,11.618545>>, -129.5, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 13
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1895.090332,-537.958374,12.235410>>, -129.5, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 14
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1788.681519,-605.775452,10.994529>>, 50.7, iRandomCarCreationEvent, 17.5)
					ELIF iRandomCarCreationEvent = 15
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1796.478638,-606.588684,10.792778>>, 50.7, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 16
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1830.822388,-578.447571,11.585022>>, 50.7, iRandomCarCreationEvent, 20.0)
					ELIF iRandomCarCreationEvent = 17
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1829.729980,-565.703979,11.653636>>, 50.7, iRandomCarCreationEvent, 15.0)
					ELIF iRandomCarCreationEvent = 18
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1848.125977,-557.549011,11.957444>>, 50.7, iRandomCarCreationEvent, 17.5)
					ELIF iRandomCarCreationEvent = 19
						CREATE_RANDOM_CAR_AT_POS(CAVALCADE, modelCreditsMale, <<-1908.515503,-515.021973,12.003843>>, 50.7, iRandomCarCreationEvent, 20.0)
					ENDIF
					
					iRandomCarCreationEvent++
				ENDIF
			ENDIF
		
			IF bCameraJustCut				
				SET_MODEL_AS_NO_LONGER_NEEDED(CAVALCADE)
				SET_MODEL_AS_NO_LONGER_NEEDED(ALPHA)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsMale)
				
				UPDATE_CREDITS_CARS_MOVEMENT()
				
				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] UPDATE_CREDITS_EVENTS: Managed to create ", iRandomCarCreationEvent, " cars, cutscene time = ", iCutsceneTime)
				
				eCreditsStage = CREDITS_STAGE_2_OVERHEAD
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_2_OVERHEAD
			UPDATE_CREDITS_CARS_MOVEMENT()
		
			IF bCameraJustCut
				eCreditsStage = CREDITS_STAGE_3_SPORTS_CAR
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_3_SPORTS_CAR
			//1755215 - Stop vehicles spawning on camera during this shot.
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
			UPDATE_CREDITS_CARS_MOVEMENT()
		
			IF bCameraJustCut
				//Warp some of the cars created in the overhead shot so we get to see them in this shot. Delete the rest to save memory.
				IF NOT IS_ENTITY_DEAD(vehCreditsCars[0])
					SET_ENTITY_COORDS(vehCreditsCars[0], <<-1742.428711,-681.913696,10.033356>>)
					SET_ENTITY_HEADING(vehCreditsCars[0], -132.316299)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehCreditsCars[0])
					SET_VEHICLE_FORWARD_SPEED(vehCreditsCars[0], 15.0)
					
					IF NOT IS_PED_INJURED(pedCreditsCars[0])
						TASK_VEHICLE_MISSION(pedCreditsCars[0], vehCreditsCars[0], NULL, MISSION_CRUISE, 15.0, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
						SET_PED_KEEP_TASK(pedCreditsCars[0], TRUE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehCreditsCars[1])
					SET_ENTITY_COORDS(vehCreditsCars[1], <<-1744.738281,-688.366455,9.544968>>)
					SET_ENTITY_HEADING(vehCreditsCars[1], -130.699753)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehCreditsCars[1])
					SET_VEHICLE_FORWARD_SPEED(vehCreditsCars[1], 15.0)
					
					IF NOT IS_PED_INJURED(pedCreditsCars[1])
						TASK_VEHICLE_MISSION(pedCreditsCars[1], vehCreditsCars[1], NULL, MISSION_CRUISE, 15.0, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
						SET_PED_KEEP_TASK(pedCreditsCars[1], TRUE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehCreditsCars[2])
					SET_ENTITY_COORDS(vehCreditsCars[2], <<-1759.989868,-666.778870,9.905796>>)
					SET_ENTITY_HEADING(vehCreditsCars[2], -133.360596)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehCreditsCars[2])
					SET_VEHICLE_FORWARD_SPEED(vehCreditsCars[2], 15.0)
					
					IF NOT IS_PED_INJURED(pedCreditsCars[2])
						TASK_VEHICLE_MISSION(pedCreditsCars[2], vehCreditsCars[2], NULL, MISSION_CRUISE, 15.0, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
						SET_PED_KEEP_TASK(pedCreditsCars[2], TRUE)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehCreditsCars[3])
					SET_ENTITY_COORDS(vehCreditsCars[3], <<-1734.6860, -734.4443, 9.4153>>)
					SET_ENTITY_HEADING(vehCreditsCars[3], 139.5574)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehCreditsCars[3])
					SET_VEHICLE_FORWARD_SPEED(vehCreditsCars[3], 0.0)
					
					REMOVE_PED(pedCreditsCars[3], TRUE)
				ENDIF
				
				REPEAT COUNT_OF(pedCreditsCars) i
					IF i > 3
						REMOVE_PED(pedCreditsCars[i], TRUE)
					ENDIF
				ENDREPEAT
				
				REPEAT COUNT_OF(vehCreditsCars) i
					IF i > 3
						REMOVE_VEHICLE(vehCreditsCars[i], TRUE)
					ENDIF
				ENDREPEAT
			
				eCreditsStage = CREDITS_STAGE_4_SPORTS_CAR_PARK
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_4_SPORTS_CAR_PARK
			SETUP_REQ_FERRIS_WHEEL(TRUE)
			UPDATE_CREDITS_CARS_MOVEMENT(TRUE)
		
			IF bCameraJustCut
				SET_VEHICLE_POPULATION_BUDGET(0)
				
				//Make sure any cars created by the script are deleted by this point.
				REPEAT COUNT_OF(pedCreditsCars) i
					REMOVE_PED(pedCreditsCars[i], TRUE)
				ENDREPEAT
				
				REPEAT COUNT_OF(vehCreditsCars) i
					REMOVE_VEHICLE(vehCreditsCars[i], TRUE)
				ENDREPEAT
			
				eCreditsStage = CREDITS_STAGE_5_YOGA
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_5_YOGA
			REQUEST_MODEL(modelCreditsBoat)
			REQUEST_MODEL(modelCreditsBoatPed)
			REQUEST_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_1, strCarrec)
			REQUEST_PTFX_ASSET()
			SETUP_REQ_FERRIS_WHEEL(TRUE)
		
			IF bCameraJustCut
				IF iCutsceneTime < 129500 //Don't set if the events have gone out of sync with the cutscene timings (e.g. due to debug or cutscene changes).
					SETUP_REQ_FERRIS_WHEEL()
					REMOVE_IPL("ferris_finale_Anim")
				ENDIF
							
				eCreditsStage = CREDITS_STAGE_6_FERRIS_FLAG
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_6_FERRIS_FLAG
			IF DOES_ENTITY_EXIST(objFerrisWheel)
				ROTATE_FERRIS_WHEEL()
			ENDIF
			
			REQUEST_MODEL(modelCreditsBoat)
			REQUEST_MODEL(modelCreditsBoatPed)
			REQUEST_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_1, strCarrec)
			REQUEST_PTFX_ASSET()
		
			IF bCameraJustCut
				IF iCutsceneTime < 132000 //Don't set if the events have gone out of sync with the cutscene timings (e.g. due to debug or cutscene changes).					
					//Create the boat and start playback synched with water
					IF HAS_MODEL_LOADED(modelCreditsBoat)
					AND HAS_MODEL_LOADED(modelCreditsBoatPed)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_CREDITS_BOAT_1, strCarrec)
						vehCreditsBoat = CREATE_VEHICLE(modelCreditsBoat, <<-1657.1, -1174.0, 0.5>>, 91.6)
						pedCreditsBoat = CREATE_PED_INSIDE_VEHICLE(vehCreditsBoat, PEDTYPE_MISSION, modelCreditsBoatPed)
						
						SET_VEHICLE_LOD_MULTIPLIER(vehCreditsBoat, 5.0)
						SET_ENTITY_LOAD_COLLISION_FLAG(vehCreditsBoat, TRUE)
						
						SET_VEHICLE_COLOURS(vehCreditsBoat, 0, 111)
						SET_VEHICLE_EXTRA_COLOURS(vehCreditsBoat, 3, 0)
						SET_VEHICLE_ENGINE_ON(vehCreditsBoat, TRUE, TRUE)
						
						START_PLAYBACK_RECORDED_VEHICLE(vehCreditsBoat, CARREC_CREDITS_BOAT_1, strCarrec, FALSE)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCreditsBoat, 2450.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCreditsBoat, FALSE)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vehCreditsBoat, TRUE)
						
						IF HAS_PTFX_ASSET_LOADED()
							ptfxCreditsBoat[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_water_boat_prop", vehCreditsBoat, <<0.0, -4.6, -1.0>>, <<0.0, 0.0, 0.0>>)
							ptfxCreditsBoat[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_water_boat_Jetmax_bow", vehCreditsBoat, <<0.0, 4.2, -0.5>>, <<0.0, 0.0, 180.0>>)
							SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCreditsBoat[0], "forward", 1.0)
							SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxCreditsBoat[1], "speed", 1.0)
						ENDIF
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCreditsBoat, TRUE)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsBoat)
					SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsBoatPed)
				ENDIF
				
				//Request: force time on the pier shots.
				SET_CLOCK_TIME(9, 30, 0)
				
				iCreditsEvent = 0
				eCreditsStage = CREDITS_STAGE_7_FERRIS_OVERHEAD
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_7_FERRIS_OVERHEAD
			IF DOES_ENTITY_EXIST(objFerrisWheel)
				ROTATE_FERRIS_WHEEL()
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehCreditsBoat)
			AND NOT IS_PED_INJURED(pedCreditsBoat)
				IF iCreditsEvent = 0
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCreditsBoat)
						IF GET_TIME_POSITION_IN_RECORDING(vehCreditsBoat) > 2850.0
							iCreditsEvent++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			REQUEST_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_2, strCarrec)			
		
			IF bCameraJustCut
				REPEAT COUNT_OF(pedCreditsFerrisWheel) i
					REMOVE_PED(pedCreditsFerrisWheel[i], TRUE)
				ENDREPEAT
				
				REPEAT COUNT_OF(objFerrisCars) i
					REMOVE_OBJECT(objFerrisCars[i], TRUE)
				ENDREPEAT
				
				REMOVE_OBJECT(objFerrisWheel, TRUE)
				
				REMOVE_ANIM_DICT(strFerrisWheelAnims)
				REQUEST_IPL("ferris_finale_Anim")

				IF NOT IS_ENTITY_DEAD(vehCreditsBoat)
					IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehCreditsBoat)
					OR IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCreditsBoat)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehCreditsBoat)
					ENDIF
					
					REMOVE_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_1, strCarrec)
					
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_CREDITS_BOAT_2, strCarrec)
						START_PLAYBACK_RECORDED_VEHICLE(vehCreditsBoat, CARREC_CREDITS_BOAT_2, strCarrec, FALSE)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCreditsBoat, 6250.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCreditsBoat, FALSE)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vehCreditsBoat, TRUE)
					ENDIF
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsSailboat) //Too late to create the sailboat if it wasn't created already.

				//Request: gradually set the clock time back to default.
				SET_CLOCK_TIME(9, 0, 0)

				iCreditsEvent = 0
				eCreditsStage = CREDITS_STAGE_8_COUPLE_PIER
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_8_COUPLE_PIER
			IF iCreditsEvent = 0
				IF NOT IS_ENTITY_DEAD(vehCreditsBoat)
				AND NOT IS_PED_INJURED(pedCreditsBoat)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCreditsBoat)
						IF GET_TIME_POSITION_IN_RECORDING(vehCreditsBoat) > 6350.0
							STOP_PLAYBACK_RECORDED_VEHICLE(vehCreditsBoat)
							TASK_VEHICLE_DRIVE_TO_COORD(pedCreditsBoat, vehCreditsBoat, <<-1852.052124,-1318.397461,0.496157>>, 40.0, DRIVINGSTYLE_STRAIGHTLINE, 
													    modelCreditsBoat, DF_DontSteerAroundPlayerPed | DF_ForceStraightLine, 1.0, 100.0)
							iCreditsEvent++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF bCameraJustCut
				REMOVE_PED(pedCreditsBoat, TRUE)
				REMOVE_VEHICLE(vehCreditsBoat, TRUE)
				REMOVE_VEHICLE(vehCreditsSailboat, TRUE)
				
				IF ptfxCreditsBoat[0] != NULL
					STOP_PARTICLE_FX_LOOPED(ptfxCreditsBoat[0])
					ptfxCreditsBoat[0] = NULL
				ENDIF
				
				IF ptfxCreditsBoat[1] != NULL
					STOP_PARTICLE_FX_LOOPED(ptfxCreditsBoat[1])
					ptfxCreditsBoat[1] = NULL
				ENDIF
				
				REMOVE_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_1, strCarrec)
				REMOVE_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_2, strCarrec)
				REMOVE_PTFX_ASSET()
			
				//Request: reset the clock change from the previous shot.
				SET_CLOCK_TIME(8, 0, 0)
				
				eCreditsStage = CREDITS_STAGE_9_MAD_CYCLIST
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_9_MAD_CYCLIST
			IF bCameraJustCut
				bGrabbedMapObjectsForCredits = FALSE
				eCreditsStage = CREDITS_STAGE_10_GYM
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_10_GYM
			//The next shot is pretty bare: take some props from around the area and place them in the shot.
			OBJECT_INDEX objBoard1, objBoard2, objBoard3, objBox, objBag, objBag2, objParasol, objLilo, objBall
		
			//The parasol object pops if grabbed on the same frame as the camera cut, so try to grab it earlier.
			IF NOT bGrabbedMapObjectsForCredits
				IF bCameraJustCut
				OR iCutsceneTime > 141400
					objParasol = GET_CLOSEST_OBJECT_OF_TYPE(<<-1801.2965, -883.5159, 3.5508>>, 3.0, prop_beach_parasol_03, FALSE)
				
					IF DOES_ENTITY_EXIST(objParasol)
					AND DOES_ENTITY_HAVE_DRAWABLE(objParasol)
					AND IS_ENTITY_VISIBLE(objParasol)
						//The parasol is currently popping so we might need to leave it where it is.
						//SET_ENTITY_COORDS_NO_OFFSET(objParasol, <<-1805.7079, -869.5021, 4.6517>>)
						//SET_ENTITY_ROTATION(objParasol, <<-39.9600, -39.2255, -59.3607>>)
						//FREEZE_ENTITY_POSITION(objParasol, TRUE)
					ENDIF
					
					bGrabbedMapObjectsForCredits = TRUE
				ENDIF
			ENDIF
		
			IF bCameraJustCut	
				//First batch of objects: surf board gear.
				objBoard1 = GET_CLOSEST_OBJECT_OF_TYPE(<<-1816.1573, -905.8969, 2.0968>>, 3.0, prop_surf_board_04, FALSE)
				objBoard2 = GET_CLOSEST_OBJECT_OF_TYPE(<<-1816.1573, -905.8969, 2.0968>>, 3.0, prop_surf_board_ldn_02, FALSE)
				objBoard3 = GET_CLOSEST_OBJECT_OF_TYPE(<<-1816.1573, -905.8969, 2.0968>>, 3.0, prop_surf_board_ldn_04, FALSE)
				objBox = GET_CLOSEST_OBJECT_OF_TYPE(<<-1816.1573, -905.8969, 2.0968>>, 3.0, prop_coolbox_01, FALSE)
				objBag = GET_CLOSEST_OBJECT_OF_TYPE(<<-1816.1573, -905.8969, 2.0968>>, 3.0, prop_beach_bag_01b, FALSE)
				objLilo = GET_CLOSEST_OBJECT_OF_TYPE(<<-1790.4347, -881.3105, 5.3852>>, 3.0, prop_beach_lilo_01, FALSE)
				objBag2 = GET_CLOSEST_OBJECT_OF_TYPE(<<-1801.2965, -883.5159, 3.5508>>, 3.0, prop_beach_bag_01b, FALSE)
				objBall = GET_CLOSEST_OBJECT_OF_TYPE(<<-1801.2965, -883.5159, 3.5508>>, 3.0, prop_beachball_01, FALSE)
				
				
				//First batch (surfing gear): it's only safe to move these if they've all streamed in.
				IF (DOES_ENTITY_EXIST(objBoard1) AND DOES_ENTITY_HAVE_DRAWABLE(objBoard1) AND IS_ENTITY_VISIBLE(objBoard1))
				AND (DOES_ENTITY_EXIST(objBoard2) AND DOES_ENTITY_HAVE_DRAWABLE(objBoard2) AND IS_ENTITY_VISIBLE(objBoard2))
				AND (DOES_ENTITY_EXIST(objBoard3) AND DOES_ENTITY_HAVE_DRAWABLE(objBoard3) AND IS_ENTITY_VISIBLE(objBoard3))
				AND (DOES_ENTITY_EXIST(objBox) AND DOES_ENTITY_HAVE_DRAWABLE(objBox) AND IS_ENTITY_VISIBLE(objBox))
				AND (DOES_ENTITY_EXIST(objBag) AND DOES_ENTITY_HAVE_DRAWABLE(objBag) AND IS_ENTITY_VISIBLE(objBag))
					SET_ENTITY_COORDS_NO_OFFSET(objBoard1, <<-1808.0835, -855.4938, 6.3014>>)
					SET_ENTITY_ROTATION(objBoard1, <<0.0000, -2.5000, 140.0400>>)
					FREEZE_ENTITY_POSITION(objBoard1, TRUE)
				
					SET_ENTITY_COORDS_NO_OFFSET(objBoard2, <<-1807.4043, -854.8961, 5.7159>>)
					SET_ENTITY_ROTATION(objBoard2, <<-79.9200, 77.4000, -62.6400>>)
					FREEZE_ENTITY_POSITION(objBoard2, TRUE)
				
					SET_ENTITY_COORDS_NO_OFFSET(objBoard3, <<-1806.4600, -856.6586, 5.8052>>)
					SET_ENTITY_ROTATION(objBoard3, <<79.9200, 87.4800, 92.5200>>)
					FREEZE_ENTITY_POSITION(objBoard3, TRUE)
				
					SET_ENTITY_COORDS_NO_OFFSET(objBox, <<-1807.9530, -855.9752, 5.5244>>)
					SET_ENTITY_ROTATION(objBox, <<0.0000, -0.0000, 102.6000>>)
					FREEZE_ENTITY_POSITION(objBox, TRUE)	
				
					SET_ENTITY_COORDS_NO_OFFSET(objBag, <<-1808.0911, -856.6115, 5.5709>>)
					SET_ENTITY_ROTATION(objBag, <<0.9658, 8.5644, -142.5600>>)
					FREEZE_ENTITY_POSITION(objBag, TRUE)
				ENDIF
				
				//Second batch of objects: sunbathing gear. NOTE: this includes the parasol but that has to be grabbed earlier to prevent pops.
				//The parasol seems to always be grabbed so it should be safe to have any combination of objects here.
				IF DOES_ENTITY_EXIST(objLilo)
				AND DOES_ENTITY_HAVE_DRAWABLE(objLilo)
				AND IS_ENTITY_VISIBLE(objLilo)
					SET_ENTITY_COORDS_NO_OFFSET(objLilo, <<-1805.2048, -868.6416, 4.8800>>)
					SET_ENTITY_ROTATION(objLilo, <<10.0800, 2.5200, -44.8016>>)
					FREEZE_ENTITY_POSITION(objLilo, TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(objBag2)
				AND DOES_ENTITY_HAVE_DRAWABLE(objBag2)
				AND IS_ENTITY_VISIBLE(objBag2)
					SET_ENTITY_COORDS_NO_OFFSET(objBag2, <<-1806.2352, -867.9008, 4.8612>>)
					SET_ENTITY_ROTATION(objBag2, <<-2.5200, 15.1200, -115.3976>>)
					FREEZE_ENTITY_POSITION(objBag2, TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(objBall)
				AND DOES_ENTITY_HAVE_DRAWABLE(objBall)
				AND IS_ENTITY_VISIBLE(objBall)
					SET_ENTITY_COORDS_NO_OFFSET(objBall, <<-1806.3030, -868.5082, 4.9113>>)
					SET_ENTITY_ROTATION(objBall, <<0.0000, 0.0000, -44.9999>>)
					FREEZE_ENTITY_POSITION(objBall, TRUE)
				ENDIF
			
				eCreditsStage = CREDITS_STAGE_11_SUNBATHERS
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_11_SUNBATHERS
			IF bCameraJustCut
				eCreditsStage = CREDITS_STAGE_12_SUNBATHER_DOG
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_12_SUNBATHER_DOG
			REQUEST_MODEL(modelCreditsFemale)
			REQUEST_MODEL(modelCreditsMale)
			REQUEST_MODEL(modelJoggerFemale)
			REQUEST_CREDITS_PED_ANIMS()
		
			IF iCutsceneTime < 147500
			AND HAVE_CREDITS_PED_ANIMS_LOADED()
				IF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_FEMALE_1]) //WORLD_HUMAN_SMOKING
					IF HAS_MODEL_LOADED(modelCreditsFemale)
						pedCreditsCars[CREDITS_FEMALE_1] = CREATE_PED(PEDTYPE_MISSION, modelCreditsFemale, <<-1773.271851,-707.433777,9.775657>>, 143.8)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_FEMALE_1], "amb@world_human_smoking@female@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_FEMALE_2]) //WORLD_HUMAN_STAND_MOBILE
					IF HAS_MODEL_LOADED(modelCreditsFemale)
						pedCreditsCars[CREDITS_FEMALE_2] = CREATE_PED(PEDTYPE_MISSION, modelCreditsFemale, <<-1780.794800,-707.496765,9.633821>>, 127.3)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_FEMALE_2], "amb@world_human_stand_mobile@female@text@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_MALE_1]) //WORLD_HUMAN_DRINKING
					IF HAS_MODEL_LOADED(modelCreditsMale)
						pedCreditsCars[CREDITS_MALE_1] = CREATE_PED(PEDTYPE_MISSION, modelCreditsMale, <<-1809.9529, -683.1652, 9.4015>>, 203.0419)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_MALE_1], "amb@world_human_hang_out_street@male_b@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_FEMALE_3]) //WORLD_HUMAN_TOURIST_MOBILE
					IF HAS_MODEL_LOADED(modelCreditsFemale)
						pedCreditsCars[CREDITS_FEMALE_3] = CREATE_PED(PEDTYPE_MISSION, modelCreditsFemale, <<-1799.859741,-690.161316,9.609697>>, 145.5)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_FEMALE_3], "amb@world_human_tourist_mobile@female@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_MALE_2]) //WORLD_HUMAN_HANG_OUT_STREET
					IF HAS_MODEL_LOADED(modelCreditsMale)
						pedCreditsCars[CREDITS_MALE_2] = CREATE_PED(PEDTYPE_MISSION, modelCreditsMale, <<-1827.309204,-660.842224,9.916418>>, -157.6)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_MALE_2], "amb@world_human_hang_out_street@male_b@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_MALE_3]) //WORLD_HUMAN_HANG_OUT_STREET
					IF HAS_MODEL_LOADED(modelCreditsMale)
						pedCreditsCars[CREDITS_MALE_3] = CREATE_PED(PEDTYPE_MISSION, modelCreditsMale, <<-1826.733276,-661.967346,9.725348>>, 20.3)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_MALE_3], "amb@world_human_hang_out_street@male_a@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_MALE_4]) //WORLD_HUMAN_STAND_MOBILE
					IF HAS_MODEL_LOADED(modelCreditsMale)
						pedCreditsCars[CREDITS_MALE_4] = CREATE_PED(PEDTYPE_MISSION, modelCreditsMale, <<-1809.3574, -684.6357, 9.4120>>, 21.1289)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_MALE_4], "amb@world_human_hang_out_street@male_a@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
					ENDIF
				ELIF NOT DOES_ENTITY_EXIST(pedCreditsCars[CREDITS_JOGGER_1]) //WORLD_HUMAN_JOG
					IF HAS_MODEL_LOADED(modelJoggerFemale)
						pedCreditsCars[CREDITS_JOGGER_1] = CREATE_PED(PEDTYPE_MISSION, modelJoggerFemale, <<-1817.0607, -682.0644, 9.4120>>, 230.3514)
						TASK_PLAY_ANIM(pedCreditsCars[CREDITS_JOGGER_1], "move_f@jogger", "run", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCreditsCars[CREDITS_JOGGER_1])
					ENDIF
				ENDIF
			ENDIF
		
			IF bCameraJustCut
			AND iCutsceneTime > 146000
				SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsFemale)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsMale)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelJoggerFemale)
			
				eCreditsStage = CREDITS_STAGE_14_HOUSES
			ENDIF
		BREAK
		
		//This stage is now gone.
		/*CASE CREDITS_STAGE_13_SUNBATHER_DOG_2
			IF bCameraJustCut
				eCreditsStage = CREDITS_STAGE_14_HOUSES
			ENDIF
		BREAK*/
		
		CASE CREDITS_STAGE_14_HOUSES		
			IF bCameraJustCut
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_1], <<-1868.0334, -625.9962, 10.2611>>, 336.5973, "amb@world_human_smoking@female@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_2], <<-1881.9839, -618.1794, 10.4857>>, 150.3923, "amb@world_human_stand_mobile@female@text@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_3], <<-1893.5, -602.4, 10.8224>>, 151.6628, "amb@world_human_hang_out_street@female_arms_crossed@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_1], <<-1867.4463, -624.8217, 10.3410>>, 124.0036, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_2], <<-1868.4, -625.2, 10.2360>>, -116.2, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_3], <<-1830.2104, -664.5304, 9.3778>>, 299.2157, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_4], <<-1829.2007, -663.9257, 9.3505>>, 119.4014, "amb@world_human_hang_out_street@male_a@base", "base")
			
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_JOGGER_1])
			
				eCreditsStage = CREDITS_STAGE_15_MICHAEL_JOGGERS
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_15_MICHAEL_JOGGERS
			IF bCameraJustCut
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_1], <<-1983.5428, -529.3809, 10.6952>>, 230.9089, "move_f@jogger", "run")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_2], <<-1919.4448, -588.8778, 10.6731>>, 170.8960, "amb@world_human_stand_mobile@female@text@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_1], <<-1918.5061, -581.2005, 10.8527>>, 202.4282, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_2], <<-1917.5505, -582.8298, 10.7776>>, 35.8903, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_3], <<-1932.9749, -569.0629, 10.8524>>, 135.6026, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_JOGGER_1], <<-1906.1226, -602.3423, 10.6828>>, 50.9085, "move_f@jogger", "run")
			
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_FEMALE_3])
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_MALE_4])
			
				eCreditsStage = CREDITS_STAGE_16_MICHAEL_JOGGERS_2
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_16_MICHAEL_JOGGERS_2
			IF bCameraJustCut
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_1], <<-1891.1615, -712.9316, 6.5149>>, 191.8786, "amb@world_human_tourist_mobile@female@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_1], <<-1878.5149, -696.8825, 8.3940>>, 32.4760, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_2], <<-1879.1495, -695.0729, 8.4385>>, 205.4313, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_3], <<-1893.1689, -631.4039, 10.1937>>, 155.5788, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_2], <<-1893.8379, -632.8634, 10.0878>>, 325.8723, "amb@world_human_hang_out_street@female_arms_crossed@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_JOGGER_1], <<-1883.5461, -725.7193, 6.3307>>, 225.0816, "move_f@jogger", "run")
			
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_MALE_4])
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_FEMALE_3])
			
				SET_CLOCK_TIME(8, 0, 0)
				SET_WEATHER_TYPE_NOW_PERSIST("clear")

				SET_PED_POPULATION_BUDGET(3)
			
				eCreditsStage = CREDITS_STAGE_17_BUM
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_17_BUM
			IF bCameraJustCut
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_1], <<-1912.0662, -602.6298, 10.5848>>, 294.1278, "amb@world_human_smoking@female@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_2], <<-1898.8030, -603.1610, 10.6174>>, 207.0302, "amb@world_human_hang_out_street@female_arms_crossed@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_1], <<-1897.5013, -604.7260, 10.6416>>, 66.1810, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_2], <<-1899.6445, -603.7774, 10.6646>>, 256.1585, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_JOGGER_1], <<-1906.1023, -604.9014, 10.6828>>, 229.4289, "move_f@jogger", "run")
			
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_FEMALE_3])
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_MALE_3])
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_MALE_4])
			
				CLEAR_AREA_OF_PEDS(<<-1849.9, -592.3, 18.3>>, 35.0)
			
				eCreditsStage = CREDITS_STAGE_18_MICHAEL_BUM
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_18_MICHAEL_BUM
			IF bCameraJustCut
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_1], <<-1881.9484, -607.5357, 14.4512>>, 129.3375, "amb@world_human_smoking@female@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_2], <<-1904.5576, -593.6984, 10.8258>>, 37.8397, "amb@world_human_hang_out_street@female_arms_crossed@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_1], <<-1889.8230, -605.4258, 10.8539>>, 170.2484, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_2], <<-1889.4088, -606.6351, 10.8001>>, 30.0624, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_3], <<-1905.2600, -592.3629, 10.8546>>, 177.2372, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_4], <<-1905.2896, -593.6451, 10.7538>>, 343.4001, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_JOGGER_1], <<-1964.4635, -554.6491, 10.6827>>, 230.4146, "move_f@jogger", "run")
			
				WARP_PED_UNDERGROUND(pedCreditsCars[CREDITS_FEMALE_3])
			
				CLEAR_AREA_OF_PEDS(<<-1849.9, -592.3, 18.3>>, 35.0)
			
				eCreditsStage = CREDITS_STAGE_19_MICHAEL_SIT
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_19_MICHAEL_SIT
			IF bCameraJustCut
				IF IS_AUDIO_SCENE_ACTIVE("ARM_1_MONTAGE_MUTES")
					STOP_AUDIO_SCENE("ARM_1_MONTAGE_MUTES")
				ENDIF
				
				CLEAR_AREA_OF_PEDS(<<-1849.9, -592.3, 18.3>>, 35.0)
			
				eCreditsStage = CREDITS_STAGE_20_FRANKLIN_ARRIVES
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_20_FRANKLIN_ARRIVES
			IF bCameraJustCut
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_2], <<-1868.5097, -641.6480, 10.0853>>, 182.5072, "amb@world_human_hang_out_street@female_arms_crossed@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_FEMALE_3], <<-1857.7383, -634.5464, 10.0832>>, 150.5682, "amb@world_human_stand_mobile@female@text@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_1], <<-1867.7147, -642.4875, 10.1098>>, 78.5870, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_2], <<-1868.9943, -642.2708, 10.1240>>, 275.0369, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_3], <<-1869.9099, -632.1488, 10.1310>>, 224.8968, "amb@world_human_hang_out_street@male_b@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_MALE_4], <<-1869.0247, -633.5297, 10.1163>>, 36.4653, "amb@world_human_hang_out_street@male_a@base", "base")
				WARP_PED_FOR_CREDITS_SHOT(pedCreditsCars[CREDITS_JOGGER_1], <<-1880.8080, -626.5896, 10.3331>>, 229.4668, "move_f@jogger", "run")
			
				CLEAR_AREA_OF_PEDS(<<-1849.9, -592.3, 18.3>>, 35.0)
			
				eCreditsStage = CREDITS_STAGE_21_LAMAR_ASKS_MICHAEL
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_21_LAMAR_ASKS_MICHAEL
			IF bCameraJustCut
				eCreditsStage = CREDITS_STAGE_22_END
			ENDIF
		BREAK
		
		CASE CREDITS_STAGE_22_END			
			
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF bDebugDisplayCreditsInfo
			PRINTLN("Armenian1.sc - credits stage = ", ENUM_TO_INT(eCreditsStage), " Time = ", iCutsceneTime)
			
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			REPEAT COUNT_OF(pedCreditsCars) i
				IF NOT IS_PED_INJURED(pedCreditsCars[i])
					TEXT_LABEL strText = i
				
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedCreditsCars[i], strText, 1.0)
				ENDIF
			ENDREPEAT
		ENDIF
	#ENDIF

ENDPROC

/// PURPOSE:
///    Placeholder stage for mocap
PROC OPENING_CUTSCENE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ENDIF
		
#IF FEATURE_GEN9_STANDALONE
		//url:bugstar:7481277 - Ensure late autosave requests don't run during this cutscene.
		CPRINTLN(DEBUG_FLOW, "Clearing autosave requests at the start of the Armenain1 intro cutscene.")
		CLEAR_AUTOSAVE_REQUESTS()
#ENDIF
		
		DO_FADE_OUT_WITH_WAIT()
		
		SETTIMERA(0)
		
		//Reduce budgets during the cutscene (no vehicles required, peds will likely be scripted or done in the cutscene).
		SET_VEHICLE_POPULATION_BUDGET(0)
		SET_PED_POPULATION_BUDGET(0)
		bRequestedCreditsScaleform = FALSE
		
		//Guarantee that the player is Franklin.
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
				WAIT(0)
			ENDWHILE
		ENDIF
		
		//Warp the player over to the cutscene start area.
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1907.3496, -577.2352, 19.1223>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 136.9707)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_WEATHER_TYPE_NOW_PERSIST("clear")
		
		//Begin streaming all of the required assets.
		REQUEST_CUTSCENE(strStartCutscene)
		SET_SRL_READAHEAD_TIMES(9, 9, 9, 9)
		
		WHILE TIMERA() < 10000
			IF SETUP_REQ_SHRINK_OFFICE_LOADED()
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START_SPHERE(<<-1907.4, -577.8, 18.8>>, 10.0)
				ELSE
					IF IS_NEW_LOAD_SCENE_LOADED()
						#IF IS_DEBUG_BUILD
							PRINTLN("Armenian1.sc - Finished loading shrink scene after ", TIMERA(), "ms")
						#ENDIF
					
						SETTIMERA(100000)
					ENDIF
				ENDIF
			ENDIF
			
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strStartCutscene)
		
			WAIT(0)
		ENDWHILE
		
		//There's a chance on next gen builds that a single frame exists where the components wouldn't be set.
		SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strStartCutscene)
		
		//The cutscene and Lamar have to be guaranteed to be loaded before progressing.
		WHILE NOT HAS_CUTSCENE_LOADED()
			SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strStartCutscene)
			
			WAIT(0)
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
			PRINTLN("Armenian1.sc - Finished loading cutscene assets after ", TIMERA(), "ms")
		#ENDIF
		
		WHILE NOT IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TITLE_SEQUENCE_DISPLAYED)
		AND NOT IS_REPEAT_PLAY_ACTIVE()
		AND NOT IS_REPLAY_IN_PROGRESS()
			WAIT(0)
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
			PRINTLN("Armenian1.sc - Finished waiting for Title screen.")
		#ENDIF
	
		//All assets are ready, start the cutscene.
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		
		// Undo the changes made to the player's state for the transition from prologue mission
		IF NOT IS_REPLAY_IN_PROGRESS()
		AND NOT IS_REPEAT_PLAY_ACTIVE()
			CPRINTLN(DEBUG_MISSION, "Armenian1 cleaning up the Prologue transition stuff.")
			REPLAY_RESET_PLAYER_STATE()
		ENDIF
		
		REGISTER_ENTITY_FOR_CUTSCENE(sLamar.ped, "Lamar", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_LAMAR))
		//REGISTER_ENTITY_FOR_CUTSCENE(vehCreditsSportsCar, "carbonizzare", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, CARBONIZZARE)
		
		NEW_LOAD_SCENE_STOP()
		CLEAR_AREA(<<0.0, 0.0, 0.0>>, 10000.0, TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
		BLOCK_SCENARIOS_FOR_CUTSCENE(TRUE)

		START_CUTSCENE()
		SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON)
		
		WAIT(50)
		
		//1769677 - Michael's outfit isn't getting set correctly, just override it manually on the cutscene ped when it starts.
		PED_INDEX pedNearest
		GET_CLOSEST_PED(<<-1907.4, -577.5, 19.2>>, 5.0, FALSE, TRUE, pedNearest, TRUE, TRUE)
		
		IF NOT IS_PED_INJURED(pedNearest)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedNearest, PED_COMP_JBIB, 0, 0)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("armenian1.sc - Overwriting Michael variations.")
			#ENDIF
		ENDIF
		
		SHUTDOWN_LOADING_SCREEN()
		
		CASCADE_SHADOWS_SET_SCREEN_SIZE_CHECK_ENABLED(FALSE)
		
		DO_FADE_IN_WITH_WAIT()
		
		eCreditsStage = CREDITS_STAGE_0_SHRINK
		iCreditsTextEvent = 0
		bCutsceneSkipped = FALSE
		bGrabbedCreditsSportsCar = FALSE
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_RUNNING
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			bCutsceneSkipped = TRUE
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
	
		UPDATE_TEXT_FOR_CREDITS_SEQUENCE()
		UPDATE_CREDITS_EVENTS()
	
		IF NOT IS_CUTSCENE_ACTIVE()
			eSectionStage = SECTION_STAGE_CLEANUP
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehCreditsSportsCar)
			IF NOT bGrabbedCreditsSportsCar
				IF GET_CUTSCENE_TIME() > 109000
					IF DOES_CUTSCENE_ENTITY_EXIST("carbonizzare")
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("carbonizzare"))
							vehCreditsSportsCar =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("carbonizzare"))
							
							IF NOT IS_ENTITY_DEAD(vehCreditsSportsCar)
								LOWER_CONVERTIBLE_ROOF(vehCreditsSportsCar, TRUE)
								SET_VEHICLE_COLOURS(vehCreditsSportsCar, 28, 0)
								SET_VEHICLE_EXTRA_COLOURS(vehCreditsSportsCar, 0, 156)
								bGrabbedCreditsSportsCar = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
        ELSE
			//Remove the car once it's not needed
			IF GET_CUTSCENE_TIME() > 125000
				REMOVE_VEHICLE(vehCreditsSportsCar, FALSE)
			ENDIF
		ENDIF

		
		SWITCH iCurrentEvent
			CASE 0 //Remove the interior once Michael leaves.
				IF GET_CUTSCENE_TIME() > 105000
					IF interiorShrink != NULL
						UNPIN_INTERIOR(interiorShrink)
						interiorShrink = NULL
					ENDIF
					
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 1 //Turn on vehicles once reaching the end.
				IF GET_CUTSCENE_TIME() > 210000
					SET_VEHICLE_POPULATION_BUDGET(3)
					
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(-41.0 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
		
		//Fetch Lamar from the cutscene and set up his default behaviour.
		IF NOT DOES_ENTITY_EXIST(sLamar.ped)
			IF GET_CUTSCENE_TIME() > 207200
				ENTITY_INDEX entityLamar = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lamar")
				IF DOES_ENTITY_EXIST(entityLamar)
					sLamar.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entityLamar)
					
					IF NOT IS_PED_INJURED(sLamar.ped)
						SETUP_LAMAR(<<0.0, 0.0, 0.0>>, 0.0, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CUTSCENE_TIME() > 210000
			SETUP_NINEF(sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
			SETUP_RAPIDGT(sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
			REQUEST_ANIM_DICT(strLamarLeadOutAnims)
		
			IF NOT IS_PED_INJURED(sLamar.ped)
			AND HAS_ANIM_DICT_LOADED(strLamarLeadOutAnims)
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar")
					IF NOT bCutsceneSkipped
					AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam") 
						TASK_PLAY_ANIM_ADVANCED(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam", <<-1900.330, -595.638, 12.233>>, <<0.000, 0.000, 48.0>>,
												INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, 
												AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.526) //0.462
						TASK_PLAY_ANIM(sLamar.ped, strLamarLeadOutAnims, "ARM1_INT_leadout_gatewalk_lam_facial", 2.0, SLOW_BLEND_OUT, -1, AF_SECONDARY, 0.526)
						SET_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_PLAYER_IMPACT | RBF_MELEE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sLamar.ped)
						
						SET_VEHICLE_POPULATION_BUDGET(3)
						
						//Open the hidden gate as the collision can affect Lamar's anim.
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1901.1627, -594.9855, 11.6781>>, 2.0, PROP_BURTO_GATE_01)
							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_BURTO_GATE_01, <<-1901.1627, -594.9855, 11.6781>>, TRUE, 1.0)
						ENDIF
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, TRUE)
					sLamar.iEvent = 0
				ENDIF
			ENDIF
		ENDIF

		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			IF NOT bCutsceneSkipped
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, -39.7, FALSE)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1901.1627, -594.9855, 11.6781>>, 2.0, PROP_BURTO_GATE_01)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_BURTO_GATE_01, <<-1901.1627, -594.9855, 11.6781>>, TRUE, 0.0)
			ENDIF
			
			//Record the sneak and just after
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
			
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP	
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		IF interiorShrink != NULL
			UNPIN_INTERIOR(interiorShrink)
			interiorShrink = NULL
		ENDIF
		
		REMOVE_PED(pedCreditsBoat, TRUE)
		
		INT i = 0
		REPEAT COUNT_OF(pedCreditsFerrisWheel) i
			REMOVE_PED(pedCreditsFerrisWheel[i], TRUE)
		ENDREPEAT
		
		REPEAT COUNT_OF(objFerrisCars) i
			REMOVE_OBJECT(objFerrisCars[i], TRUE)
		ENDREPEAT
		
		REPEAT COUNT_OF(pedCreditsCars) i
			//Set the cutscne peds to wander around after the cutscene: only do this if we don't skip and the ped is in a safe position to wander.
			IF IS_SCREEN_FADED_OUT()
			OR (NOT IS_PED_INJURED(pedCreditsCars[i]) AND IS_PED_IN_ANY_VEHICLE(pedCreditsCars[i]))
			OR i = CREDITS_FEMALE_1
			OR (NOT IS_PED_INJURED(pedCreditsCars[i]) AND NOT IS_ENTITY_VISIBLE_TO_SCRIPT(pedCreditsCars[i]))
				REMOVE_PED(pedCreditsCars[i], TRUE)
			ELSE
				IF NOT IS_PED_INJURED(pedCreditsCars[i])
					TASK_WANDER_STANDARD(pedCreditsCars[i], DEFAULT)
					SET_PED_KEEP_TASK(pedCreditsCars[i], TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCreditsCars[i], FALSE)
					REMOVE_PED(pedCreditsCars[i], FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(vehCreditsCars) i
			REMOVE_VEHICLE(vehCreditsCars[i], TRUE)
		ENDREPEAT
		
		REMOVE_OBJECT(objFerrisWheel, TRUE)
		
		REMOVE_VEHICLE(vehCreditsBoat, TRUE)
		REMOVE_VEHICLE(vehCreditsSailboat, TRUE)
		
		IF bGrabbedCreditsSportsCar
			REMOVE_VEHICLE(vehCreditsSportsCar, FALSE)
		ENDIF
		
		IF ptfxCreditsBoat[0] != NULL
			STOP_PARTICLE_FX_LOOPED(ptfxCreditsBoat[0])
			ptfxCreditsBoat[0] = NULL
		ENDIF
		
		IF ptfxCreditsBoat[1] != NULL
			STOP_PARTICLE_FX_LOOPED(ptfxCreditsBoat[1])
			ptfxCreditsBoat[1] = NULL
		ENDIF
		
		CASCADE_SHADOWS_SET_SCREEN_SIZE_CHECK_ENABLED(TRUE)
		
		REMOVE_CREDITS_PED_ANIMS()
		
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsBoat)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsBoatPed)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisCar)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheel)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheelPed1)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelFerrisWheelPed2)
		SET_MODEL_AS_NO_LONGER_NEEDED(CAVALCADE)
		SET_MODEL_AS_NO_LONGER_NEEDED(ALPHA)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelJoggerFemale)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsFemale)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCreditsMale)
		
		REMOVE_VEHICLE_RECORDING(CARREC_CREDITS_BOAT_1, strCarrec)
		
		REMOVE_ANIM_DICT(strFerrisWheelAnims)
		REMOVE_PTFX_ASSET()
		REQUEST_IPL("ferris_finale_Anim")
		
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//If we skipped make sure the time and weather are set correctly.
		IF IS_SCREEN_FADED_OUT()
			NEW_LOAD_SCENE_STOP()
			SET_CLOCK_TIME(8, 0, 0)
			SET_WEATHER_TYPE_NOW_PERSIST("clear")
		ENDIF
		
		/*IF NOT IS_PED_INJURED(sLamar.ped)
		AND HAS_ANIM_DICT_LOADED(strLamarLeadOutAnims)
		AND IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam") 
			FLOAT fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam")

			TASK_PLAY_ANIM_ADVANCED(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam", <<-1900.330, -595.638, 12.233>>, <<0.000, 0.000, 48.0>>,
									INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, 
									AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, fAnimTime) 
			TASK_PLAY_ANIM(sLamar.ped, strLamarLeadOutAnims, "ARM1_INT_leadout_gatewalk_lam_facial", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY, fAnimTime)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sLamar.ped)
			SET_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_PLAYER_IMPACT | RBF_MELEE)
		ENDIF*/
		
		IF bRequestedCreditsScaleform
			SCALEFORM_REMOVE_ALL_CREDIT_BLOCKS()
		
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfCredits)
			bRequestedCreditsScaleform = FALSE
		ENDIF
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		BLOCK_SCENARIOS_FOR_CUTSCENE(FALSE)
	
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_CHOOSE_CAR
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP
		DO_FADE_OUT_WITH_WAIT()
		
		
		SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
	
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
		ENDIF
		
		WHILE IS_CUTSCENE_ACTIVE()
			IF NOT DOES_ENTITY_EXIST(sLamar.ped)
				ENTITY_INDEX entityLamar = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lamar")
				IF DOES_ENTITY_EXIST(entityLamar)
					sLamar.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(entityLamar)
					
					IF NOT IS_PED_INJURED(sLamar.ped)
						SETUP_LAMAR(<<0.0, 0.0, 0.0>>, 0.0, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehCreditsSportsCar)
				ENTITY_INDEX entityCar = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("carbonizzare")
			
				IF DOES_ENTITY_EXIST(entityCar)
					vehCreditsSportsCar =  GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityCar)
					bGrabbedCreditsSportsCar = TRUE
				ENDIF
			ENDIF
		
			WAIT(0)
		ENDWHILE
		
		eCreditsStage = CREDITS_STAGE_22_END
		bCutsceneSkipped = TRUE
		eSectionStage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC

PROC CHOOSE_CAR()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			IF bUsedACheckpoint
				START_REPLAY_SETUP(vFranklinLeadOutPos, fFranklinLeadOutHeading, FALSE)
				
				iCurrentEvent = 99
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinLeadOutPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinLeadOutHeading)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				//1914149 - The old-style load scene is causing loading issues.				
				NEW_LOAD_SCENE_FRUSTRUM_WITH_WAIT(<<-1902.5677, -598.1853, 12.7282>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-3.0000, -0.0000, -43.2776>>), 130.0)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				iCurrentEvent = 99
			ENDIF
		ELSE		
			IF SETUP_LAMAR(vLamarLeadOutPos, fLamarLeadOutHeading)
			AND SETUP_NINEF(sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
			AND SETUP_RAPIDGT(sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
				CLEAR_AREA(sMainCars[0].vStartPos, 100.0, TRUE)
				SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<<-1889.1, -586.0, 12.5>>, 3.0, 3)
				
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		iGatePedBlockingArea = ADD_NAVMESH_BLOCKING_OBJECT(<<-1900.7888, -595.2253, 11.0273>>, <<2.0, 2.0, 3.0>>, 139.6549)
	
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_CHOOSE_CAR")
			START_AUDIO_SCENE("ARM_1_CHOOSE_CAR")
		ENDIF
	
		//If we skipped here (either Z-skip or skipping the cutscene) do some extra prep.
		IF IS_SCREEN_FADED_OUT()
			//Don't do a load scene if we skipped here from checkpoint/debug.
			IF iCurrentEvent != 99
				//1914149 - The old-style load scene shouldn't be called when skipping the cutscene, do a smaller new load scene.
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinLeadOutPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinLeadOutHeading)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				NEW_LOAD_SCENE_FRUSTRUM_WITH_WAIT(<<-1902.5677, -598.1853, 12.7282>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-3.0000, -0.0000, -43.2776>>), 130.0)
			ENDIF
			
			WAIT(0)

			WHILE NOT SETUP_NINEF(sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
			OR NOT SETUP_RAPIDGT(sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
			OR NOT SETUP_LAMAR(<<-1883.5654, -575.7982, 10.7861>>, 251.8757)
				WAIT(0)
			ENDWHILE			
						
			IF IS_VEHICLE_DRIVEABLE(sMainCars[NINEF_INDEX].veh)
				SET_ENTITY_COORDS(sMainCars[NINEF_INDEX].veh, sMainCars[NINEF_INDEX].vStartPos)
				SET_ENTITY_HEADING(sMainCars[NINEF_INDEX].veh, sMainCars[NINEF_INDEX].fStartHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[NINEF_INDEX].veh)
			ENDIF
			
			REQUEST_ANIM_DICT(strLamarLeadOutAnims)
			
			WHILE NOT HAS_ANIM_DICT_LOADED(strLamarLeadOutAnims)
				WAIT(0)
			ENDWHILE
			
			IF NOT IS_PED_INJURED(sLamar.ped)
			AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam") 
				SET_ENTITY_COORDS(sLamar.ped, vLamarLeadOutPos)
				SET_ENTITY_HEADING(sLamar.ped, fLamarLeadOutHeading)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(sLamar.ped, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, TRUE)
				
				TASK_PLAY_ANIM_ADVANCED(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam", <<-1900.330, -595.638, 12.233>>, <<0.000, 0.000, 48.0>>,
										INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, 
										AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
				TASK_PLAY_ANIM(sLamar.ped, strLamarLeadOutAnims, "ARM1_INT_leadout_gatewalk_lam_facial", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY, 0.462)
				SET_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_PLAYER_IMPACT | RBF_MELEE)

				sLamar.iEvent = 0
			ENDIF
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1901.1627, -594.9855, 11.6781>>, 2.0, PROP_BURTO_GATE_01)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_BURTO_GATE_01, <<-1901.1627, -594.9855, 11.6781>>, TRUE, 0.0)
			ENDIF
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinLeadOutPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinLeadOutHeading)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			WAIT(0)
		ENDIF
		
		//2071893 - The old non-creation area around the alley was blocking the cat scenario respawning on retries.
		//SET_PED_NON_CREATION_AREA(<<-1914.638916,-598.726135,0.271984>>, <<-1861.191284,-570.867065,21.271984>>)
		SET_PED_NON_CREATION_AREA(<<-1894.9843, -583.5402, 0.0000>>, <<-1865.4764, -563.2835, 18.1544>>)
		
		ADD_PED_FOR_DIALOGUE(sConversationPeds, 0, PLAYER_PED_ID(), "FRANKLIN")
		BLOCK_VEHICLE_GENS_AT_START_POINT(TRUE)
		
		DO_FADE_IN_WITH_WAIT()
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[NINEF_INDEX].veh)
			FREEZE_ENTITY_POSITION(sMainCars[NINEF_INDEX].veh, TRUE)
		ENDIF
		
		//The Rapidgt is being set in a slightly different position to the regular position for this sequence (1407775)
		IF IS_VEHICLE_DRIVEABLE(sMainCars[RAPIDGT_INDEX].veh)
			SET_ENTITY_COORDS(sMainCars[RAPIDGT_INDEX].veh, <<-1880.7510, -577.1409, 10.7690>>)
			SET_ENTITY_HEADING(sMainCars[RAPIDGT_INDEX].veh, sMainCars[RAPIDGT_INDEX].fStartHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[RAPIDGT_INDEX].veh)
			FREEZE_ENTITY_POSITION(sMainCars[RAPIDGT_INDEX].veh, TRUE)
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE) //For B*468045
		SET_INITIAL_PLAYER_STATION("RADIO_03_HIPHOP_NEW")
		FREEZE_RADIO_STATION("RADIO_03_HIPHOP_NEW")
		SET_RADIO_AUTO_UNFREEZE(FALSE)
		SET_RADIO_TRACK("RADIO_03_HIPHOP_NEW", "ARM1_RADIO_STARTS")
		DISABLE_TAXI_HAILING(TRUE)
		
		//1768520 - Reset the in-car cam from the previous view.
		//NOTE: This isn't going to work well with first-person cam, as we'll get the reverse issue (cam will pop from first-person to third-person before the cutscene).
		//SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
		
		iHurryDialogueTimer = GET_GAME_TIMER()
		iNumTimesPlayedHurryDialogue = 0
		iCarHelpTimer = 0
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_DisableExplosionReactions, TRUE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_UseKinematicModeWhenStationary, TRUE)
		ENDIF
		
		iStartFailUnarmedCounter = 0
		iLamarWalkToCarTimer = 0
		bLamarAlreadyGotIntoCar = FALSE
		bLamarGivenWalkToCarTask = FALSE
		bAlleyAssistedRouteActive = FALSE
		bPlayerControlTurnedOffByMission = FALSE
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_RUNNING
		REQUEST_WAYPOINT_RECORDING(strWaypointStartAlley)
		REQUEST_WAYPOINT_RECORDING(strWaypointAroundCar)
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		//DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		
		IF NOT bHasTextLabelTriggered[AR1_CHOOSE]
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1900.440796,-595.769775,10.646699>>, <<-1884.650391,-576.980774,13.571643>>, 7.50000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1886.852417,-594.809937,10.663315>>, <<-1895.973022,-587.258118,13.386953>>, 18.750000)
				//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			ENDIF
		ENDIF
		
		//2001702 - Disable running if in first-person view.
		IF GET_CAM_ACTIVE_VIEW_MODE_CONTEXT() = CAM_VIEW_MODE_CONTEXT_ON_FOOT
		AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1877.904419,-574.581177,10.633959>>, <<-1897.585449,-598.019836,14.406877>>, 12.000000)
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			ENDIF
		ENDIF
		
		//There are issues if trying to enter cars behind the cactus plant, so disable it.
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1873.636841,-578.743713,10.627910>>, <<-1877.221802,-582.763306,20.099403>>, 2.500000)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		ENDIF
		
		IF NOT bAlleyAssistedRouteActive
		AND GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointStartAlley)
			USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE(strWaypointStartAlley, TRUE)
			bAlleyAssistedRouteActive = TRUE
		ENDIF
		
		//2002800 - Display help for first-person mode.
		IF NOT bHasTextLabelTriggered[CMN_FPSHELP]
			IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("CMN_FPSHELP", DEFAULT_HELP_TEXT_TIME)
					bHasTextLabelTriggered[CMN_FPSHELP] = TRUE
				ENDIF
			ELSE
				bHasTextLabelTriggered[CMN_FPSHELP] = TRUE
			ENDIF
		ELSE
			//Clear once they use first-person.
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW)
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMN_FPSHELP")
				CLEAR_HELP()
			ENDIF
		ENDIF
	
		IF IS_VEHICLE_DRIVEABLE(sMainCars[0].veh) 
		AND IS_VEHICLE_DRIVEABLE(sMainCars[1].veh)
			//Handle objectives.
			SWITCH iCurrentEvent
				CASE 0 //Follow Lamar.
					IF NOT bHasTextLabelTriggered[AR1_CHASE]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							//PRINT_NOW("AR1_CHASE", DEFAULT_GOD_TEXT_TIME, 0)
							bHasTextLabelTriggered[AR1_CHASE] = TRUE
						ENDIF
					ENDIF
					
					IF NOT DOES_BLIP_EXIST(sLamar.blip)
						REMOVE_ALL_BLIPS()
						sLamar.blip = CREATE_BLIP_FOR_ENTITY(sLamar.ped)
					ENDIF
					
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sLamar.ped)) > 900.0
						IF NOT bHasTextLabelTriggered[AR1_WAIT]
							PRINT_NOW("AR1_WAIT", DEFAULT_GOD_TEXT_TIME, 0)
							bHasTextLabelTriggered[AR1_WAIT] = TRUE
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1885.959473,-578.539978,10.848052>>, <<-1886.864990,-579.678467,13.601677>>, 4.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1882.453613,-580.295898,16.118715>>, <<-1878.337524,-575.303833,5.633518>>, 11.500000)
						iCurrentEvent++
					ENDIF
				BREAK
				
				CASE 1 //Choose a car.
					IF NOT bHasTextLabelTriggered[AR1_CHOOSE]
						IF bHasTextLabelTriggered[ARM1_PICK]
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								PRINT_NOW("AR1_CHOOSE", DEFAULT_GOD_TEXT_TIME, 0)
								iCarHelpTimer = GET_GAME_TIMER()
								bHasTextLabelTriggered[AR1_CHOOSE] = TRUE
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[AR1_CARHELP]
						IF GET_GAME_TIMER() - iCarHelpTimer > 5000
							PRINT_HELP("AR1_CARHELP")
							bHasTextLabelTriggered[AR1_CARHELP] = TRUE
						ENDIF
					ENDIF
					
					//Handle the player leaving Lamar behind.
					IF NOT IS_PED_INJURED(sLamar.ped)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sLamar.ped)) > 1600.0
							IF NOT DOES_BLIP_EXIST(sLamar.blip)
								REMOVE_ALL_BLIPS()
								sLamar.blip = CREATE_BLIP_FOR_ENTITY(sLamar.ped)
							ENDIF
							
							IF NOT bHasTextLabelTriggered[AR1_WAIT]
								PRINT_NOW("AR1_WAIT", DEFAULT_GOD_TEXT_TIME, 0)
								bHasTextLabelTriggered[AR1_WAIT] = TRUE
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(sMainCars[0].blip)
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("AR1_CHOOSE")
								AND bHasTextLabelTriggered[AR1_CHOOSE]
									CLEAR_PRINTS()
								ENDIF
								REMOVE_ALL_BLIPS()
							
								sMainCars[0].blip = CREATE_BLIP_FOR_VEHICLE(sMainCars[0].veh)
							ENDIF
							
							IF NOT DOES_BLIP_EXIST(sMainCars[1].blip)
								IF NOT IS_PED_IN_VEHICLE(sLamar.ped, sMainCars[1].veh)
									sMainCars[1].blip = CREATE_BLIP_FOR_VEHICLE(sMainCars[1].veh)
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(sLamar.ped, sMainCars[1].veh)
									REMOVE_BLIP(sMainCars[1].blip)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			//Once the player gets into one of the cars progress the mission.
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			
			IF vehTemp = sMainCars[NINEF_INDEX].veh OR vehTemp = sMainCars[RAPIDGT_INDEX].veh
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF IS_ENTITY_STATIC(vehTemp)
						FREEZE_ENTITY_POSITION(vehTemp, FALSE)
					ENDIF
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_CARHELP")
					CLEAR_HELP()
				ENDIF
			
				IF vehTemp = sMainCars[NINEF_INDEX].veh
					iPlayersCar = NINEF_INDEX
					iBuddiesCar = RAPIDGT_INDEX
				
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehTemp, SC_DOOR_FRONT_LEFT) > 0.1
					OR GET_VEHICLE_DOOR_ANGLE_RATIO(vehTemp, SC_DOOR_FRONT_RIGHT) > 0.1
					OR IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehTemp)
						LOAD_STREAM("ARM_1_IG_1_LAMAR_DRIVES_OFF")
					
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							eSectionStage = SECTION_STAGE_CLEANUP
						ENDIF
					ENDIF
				ELIF vehTemp = sMainCars[RAPIDGT_INDEX].veh
					iPlayersCar = RAPIDGT_INDEX
					iBuddiesCar = NINEF_INDEX
					
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehTemp, SC_DOOR_FRONT_LEFT) > 0.1
					OR GET_VEHICLE_DOOR_ANGLE_RATIO(vehTemp, SC_DOOR_FRONT_RIGHT) > 0.1
					OR IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehTemp)
						LOAD_STREAM("ARM_1_IG_2_LAMAR_DRIVES_OFF_ALT")
					
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							eSectionStage = SECTION_STAGE_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//If player control was turned off yet somehow they're not getting the car, turn player control back on
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			AND bPlayerControlTurnedOffByMission
				IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK AND NOT (vehTemp = sMainCars[0].veh OR vehTemp = sMainCars[1].veh))
				OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1877.3, -582.2, 11.4>>) < 1.7				
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1877.3, -582.2, 11.4>>) < 1.7
					AND vehTemp != NULL
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
				
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					bPlayerControlTurnedOffByMission = FALSE
				ENDIF
			ENDIF
			
			IF (vehTemp = sMainCars[0].veh OR vehTemp = sMainCars[1].veh)		
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1877.5, -582.0, 11.3>>) > 2.0
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE)
				bPlayerControlTurnedOffByMission = TRUE
			ENDIF

			//Handle Lamar behaviour: he waits for the player to choose a car, then enters the other car.
			IF NOT IS_PED_INJURED(sLamar.ped)
				REQUEST_ANIM_DICT(strLamarLeadOutAnims)
				REQUEST_ANIM_DICT(strLamarAnnoyedAnims)
				
				FLOAT fDistBetweenPlayerAndLamar = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sLamar.ped))
			
				IF NOT bLamarGivenWalkToCarTask
					IF vehTemp = sMainCars[0].veh OR vehTemp = sMainCars[1].veh
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehTemp)) < 4.0
					AND NOT bLamarAlreadyGotIntoCar
						IF iLamarWalkToCarTimer = 0
							iLamarWalkToCarTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - iLamarWalkToCarTimer > 200
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF vehTemp = sMainCars[RAPIDGT_INDEX].veh
									IF GET_IS_WAYPOINT_RECORDING_LOADED(strWaypointAroundCar)
										INT iNumNodes
										WAYPOINT_RECORDING_GET_NUM_POINTS(strWaypointAroundCar, iNumNodes)
									
										SEQUENCE_INDEX seq
										OPEN_SEQUENCE_TASK(seq)
											TASK_FOLLOW_WAYPOINT_RECORDING(NULL, strWaypointAroundCar, 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT, iNumNodes - 2)
											TASK_ENTER_VEHICLE(NULL, sMainCars[NINEF_INDEX].veh, -1, VS_DRIVER, PEDMOVE_WALK)
											//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1884.2018, -577.8268, 10.8243>>, PEDMOVE_WALK, -1, DEFAULT, ENAV_NO_STOPPING | ENAV_SUPPRESS_EXACT_STOP)
											//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1881.6919, -580.1697, 10.8284>>, PEDMOVE_WALK, -1)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
									ENDIF
								ELSE
									FREEZE_ENTITY_POSITION(sMainCars[RAPIDGT_INDEX].veh, FALSE)
									
									IF NOT IS_PED_IN_ANY_VEHICLE(sLamar.ped)
										TASK_ENTER_VEHICLE(sLamar.ped, sMainCars[RAPIDGT_INDEX].veh, -1, VS_DRIVER, PEDMOVE_WALK)
									ENDIF
								ENDIF
								
								bLamarGivenWalkToCarTask = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//1261710 - If the player goes over to the Ninef then Lamar gets into the RapidGT straight away.
				IF NOT bLamarAlreadyGotIntoCar
					IF vehTemp = NULL
					AND sLamar.iEvent > 0
					AND NOT bLamarGivenWalkToCarTask
					AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1878.788574,-577.908142,10.753211>>, <<-1879.882324,-579.340576,12.554530>>, 1.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1876.007446,-577.841553,10.665591>>, <<-1879.743042,-582.368835,12.604184>>, 2.000000)
							FREEZE_ENTITY_POSITION(sMainCars[RAPIDGT_INDEX].veh, FALSE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[RAPIDGT_INDEX].veh, FALSE)
							TASK_ENTER_VEHICLE(sLamar.ped, sMainCars[RAPIDGT_INDEX].veh, -1, VS_DRIVER, PEDMOVE_WALK)
							
							LOAD_STREAM("ARM_1_IG_1_LAMAR_DRIVES_OFF")
							
							bLamarAlreadyGotIntoCar = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT bHasTextLabelTriggered[AR1_CHOICE]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_CHOICE", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[AR1_CHOICE] = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				SWITCH sLamar.iEvent
					CASE 0 //Lamar walks to the start position.											
						//Have Lamar look at the cars once he gets near.
						IF fDistBetweenPlayerAndLamar < 400.0
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							//IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_GOTD", CONV_PRIORITY_MEDIUM) 
							IF NOT bHasTextLabelTriggered[ARM1_INTP6_1]
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_INTP6", "ARM1_INTP6_1", CONV_PRIORITY_MEDIUM) 
									bHasTextLabelTriggered[ARM1_INTP6_1] = TRUE
								ENDIF
							ELIF NOT bHasTextLabelTriggered[ARM1_GOTD]
								IF IS_ENTITY_IN_ANGLED_AREA(sLamar.ped, <<-1885.658569,-577.950867,10.846542>>, <<-1887.837280,-580.549438,14.104136>>, 4.750000)
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_INTP6", "ARM1_INTP6_3", CONV_PRIORITY_MEDIUM) 
										bHasTextLabelTriggered[ARM1_GOTD] = TRUE
									ENDIF
								ENDIF
							ELIF NOT bHasTextLabelTriggered[ARM1_GOTD2]
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_INTP6", "ARM1_INTP6_4", CONV_PRIORITY_MEDIUM) 
									bHasTextLabelTriggered[ARM1_GOTD2] = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF (IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam") 
						AND GET_ENTITY_ANIM_CURRENT_TIME(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam") >= 0.99)
						OR NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_gatewalk_lam") 
							sLamar.iEvent++
						ENDIF
					BREAK
					
					CASE 1 //Lamar waits for the player to get in a car.
						//Play the "which one do you want?" line when the player arrives.
						IF iCurrentEvent > 0
						AND NOT bLamarAlreadyGotIntoCar
						AND NOT bHasTextLabelTriggered[ARM1_PICK]
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND NOT (vehTemp = sMainCars[0].veh OR vehTemp = sMainCars[1].veh)
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_INTP6", "ARM1_INTP6_5", CONV_PRIORITY_MEDIUM)
								SEQUENCE_INDEX seq
								OPEN_SEQUENCE_TASK(seq)
									TASK_PLAY_ANIM(NULL, strLamarLeadOutAnims, "arm1_int_leadout_action_lam", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, strLamarLeadOutAnims, "arm1_int_leadout_loop_2_lam", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
									TASK_PLAY_ANIM(NULL, strLamarLeadOutAnims, "arm1_int_leadout_loop_lam", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_LOOPING)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
								CLEAR_SEQUENCE_TASK(seq)
							
								bHasTextLabelTriggered[ARM1_PICK] = TRUE
								iNumTimesPlayedHurryDialogue = 0
								iHurryDialogueTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
						
						IF vehTemp != sMainCars[0].veh AND vehTemp != sMainCars[1].veh						
							//Play some dialogue from Lamar if the player hangs around
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
							AND bHasTextLabelTriggered[ARM1_PICK]
							AND iNumTimesPlayedHurryDialogue < 4
							AND NOT bLamarAlreadyGotIntoCar
							AND GET_GAME_TIMER() - iHurryDialogueTimer > 15000
							AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sLamar.ped)) < 400.0
								IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_HURRY", CONV_PRIORITY_MEDIUM)
									TASK_LOOK_AT_ENTITY(sLamar.ped, PLAYER_PED_ID(), 3000, SLF_SLOW_TURN_RATE | SLF_EXTEND_PITCH_LIMIT | SLF_EXTEND_YAW_LIMIT)
								
									//Play an appropriate anim if the player is in the right place.
									IF HAS_ANIM_DICT_LOADED(strLamarAnnoyedAnims)
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1883.812378,-577.266602,10.823965>>, <<-1899.143433,-595.466125,13.873234>>, 5.000000)
										INT iRandom
										iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
										
										SEQUENCE_INDEX seq
										OPEN_SEQUENCE_TASK(seq)
											IF iRandom = 0
												TASK_PLAY_ANIM(NULL, strLamarAnnoyedAnims, "lamar_idle_01", WALK_BLEND_IN, WALK_BLEND_IN, -1, AF_USE_KINEMATIC_PHYSICS)
											ELIF iRandom = 1
												TASK_PLAY_ANIM(NULL, strLamarAnnoyedAnims, "lamar_idle_02", WALK_BLEND_IN, WALK_BLEND_IN, -1, AF_USE_KINEMATIC_PHYSICS)
											ELSE
												TASK_PLAY_ANIM(NULL, strLamarAnnoyedAnims, "lamar_idle_03", WALK_BLEND_IN, WALK_BLEND_IN, -1, AF_USE_KINEMATIC_PHYSICS)
											ENDIF
											
											TASK_PLAY_ANIM(NULL, strLamarLeadOutAnims, "arm1_int_leadout_loop_lam", WALK_BLEND_IN, WALK_BLEND_IN, -1, AF_USE_KINEMATIC_PHYSICS | AF_LOOPING)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(sLamar.ped, seq)
										CLEAR_SEQUENCE_TASK(seq)
									ENDIF
								
									iNumTimesPlayedHurryDialogue++
									iHurryDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0, 3000)
								ENDIF
							ENDIF
							
							//Make sure Lamar stays at his start position.
							IF NOT bLamarGivenWalkToCarTask
							AND NOT bLamarAlreadyGotIntoCar
								IF VDIST2(GET_ENTITY_COORDS(sLamar.ped), vLamarByStartCarPos) > 3.0
									IF GET_SCRIPT_TASK_STATUS(sLamar.ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
									AND NOT IS_POSITION_OCCUPIED(vLamarByStartCarPos, 0.5, FALSE, TRUE, FALSE, FALSE, FALSE, sMainCars[RAPIDGT_INDEX].veh)
										TASK_FOLLOW_NAV_MESH_TO_COORD(sLamar.ped, vLamarByStartCarPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 0.5, ENAV_DEFAULT, fLamarByStartCarHeading)
									ENDIF
								ELSE
									IF HAS_ANIM_DICT_LOADED(strLamarLeadOutAnims)
									AND HAS_ANIM_DICT_LOADED(strLamarAnnoyedAnims)
										IF NOT IS_PED_WALKING(sLamar.ped)
										AND NOT IS_PED_RAGDOLL(sLamar.ped)
										AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_loop_lam")
										AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_action_lam")
										AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_loop_2_lam")
										AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarAnnoyedAnims, "lamar_idle_01")
										AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarAnnoyedAnims, "lamar_idle_02")
										AND NOT IS_ENTITY_PLAYING_ANIM(sLamar.ped, strLamarAnnoyedAnims, "lamar_idle_03")
											TASK_PLAY_ANIM(sLamar.ped, strLamarLeadOutAnims, "arm1_int_leadout_loop_lam", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_LOOPING)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
				
				//Fail for abandoning Lamar if the player drives too far away
				IF VDIST2(GET_ENTITY_COORDS(sLamar.ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 4900.0
					MISSION_FAILED(FAILED_ABANDONED_LAMAR)
				ENDIF
			ENDIF

			//Request next section assets in advance.
			REQUEST_VEHICLE_RECORDING(sMainCars[0].iStartCarrec, strCarrec)
			REQUEST_VEHICLE_RECORDING(sMainCars[1].iStartCarrec, strCarrec)
			REQUEST_ANIM_DICT(strChaseStartAnims)
			REQUEST_MODEL(modelCarOwner)
			
			//If the player damages the cars before the race starts then fail.
			IF iStartFailTimer = 0
				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[NINEF_INDEX].veh, WEAPONTYPE_UNARMED)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[RAPIDGT_INDEX].veh, WEAPONTYPE_UNARMED)
					iStartFailUnarmedCounter++
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sMainCars[NINEF_INDEX].veh)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sMainCars[RAPIDGT_INDEX].veh)
				ENDIF
			
				IF (HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[NINEF_INDEX].veh, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON) 
				AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[NINEF_INDEX].veh, WEAPONTYPE_UNARMED))
				OR (HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[RAPIDGT_INDEX].veh, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON) 
				AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[RAPIDGT_INDEX].veh, WEAPONTYPE_UNARMED))
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sMainCars[NINEF_INDEX].veh)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sMainCars[RAPIDGT_INDEX].veh)
				OR iStartFailUnarmedCounter > 3
					iStartFailTimer = GET_GAME_TIMER()
				ENDIF
			ELIF GET_GAME_TIMER() - iStartFailTimer > 750
				MISSION_FAILED(FAILED_DAMAGED_CARS_BEFORE_RACE)
			ENDIF
			
			//Fail the mission if the player gets a wanted level.
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				MISSION_FAILED(FAILED_ALERTED_COPS)
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP
		
		//Record the player getting into his chosen car
		REPLAY_RECORD_BACK_FOR_TIME(3.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE)
	
		g_iArmenian1VehicleChoice = iPlayersCar
		
		IF iPlayersCar = NINEF_INDEX
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ARM1_CAR_CHOSEN) 
		ENDIF
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_RunFromFiresAndExplosions, TRUE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_DisableExplosionReactions, FALSE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_UseKinematicModeWhenStationary, FALSE)
			CLEAR_RAGDOLL_BLOCKING_FLAGS(sLamar.ped, RBF_PLAYER_IMPACT | RBF_MELEE)
			
			TASK_CLEAR_LOOK_AT(sLamar.ped)
		ENDIF
		
		REMOVE_ANIM_DICT(strLamarLeadOutAnims)
		REMOVE_WAYPOINT_RECORDING(strWaypointStartAlley)
		REMOVE_WAYPOINT_RECORDING(strWaypointAroundCar)
		
		IF iGatePedBlockingArea != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iGatePedBlockingArea)
			iGatePedBlockingArea = -1
		ENDIF
		
		CLEAR_TRIGGERED_LABELS()
		DISABLE_TAXI_HAILING(FALSE)
	
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_START_CHASE_CUTSCENE
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP
		IF IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[0].veh)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[0].veh)
			ENDIF
		ENDIF
		
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC


PROC CHASE_START_CUTSCENE_ANIMATED_CAMS()
	CONST_FLOAT UBER_SKIP_TIME	2000.0
	
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FranklinAndLamar")
	
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			IF bUsedACheckpoint
				START_REPLAY_SETUP(vFranklinLeadOutPos, fFranklinLeadOutHeading, FALSE)
				
				iCurrentEvent = 99
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinLeadOutPos)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				LOAD_SCENE(vFranklinLeadOutPos)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				iCurrentEvent = 99
			ENDIF
		ELSE	
			IF SETUP_LAMAR(vLamarLeadOutPos)
			AND SETUP_NINEF(sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
			AND SETUP_RAPIDGT(sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
				SETUP_REQ_CAR_CHOICES()
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
				SETUP_REQ_PUT_PLAYER_INTO_CHOSEN_CAR()
				BLOCK_VEHICLE_GENS_AT_START_POINT(TRUE)
				
				SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
				bLamarGivenWalkToCarTask = FALSE
				bLamarAlreadyGotIntoCar = FALSE
				
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SETUP
		REQUEST_VEHICLE_RECORDING(sMainCars[0].iStartCarrec, strCarrec)
		REQUEST_VEHICLE_RECORDING(sMainCars[1].iStartCarrec, strCarrec)
		REQUEST_ANIM_DICT(strChaseStartAnims)
		REQUEST_MODEL(modelCarOwner)
	
		IF SETUP_REQ_LOAD_MAIN_CHASE_RECORDING()
		AND ((iPlayersCar = NINEF_INDEX AND LOAD_STREAM("ARM_1_IG_1_LAMAR_DRIVES_OFF")) OR (iPlayersCar = RAPIDGT_INDEX AND LOAD_STREAM("ARM_1_IG_2_LAMAR_DRIVES_OFF_ALT")))
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(sMainCars[0].iStartCarrec, strCarrec)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(sMainCars[1].iStartCarrec, strCarrec)
		AND HAS_ANIM_DICT_LOADED(strChaseStartAnims)
		AND HAS_MODEL_LOADED(modelCarOwner)
			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_ALL_BLIPS()
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			SPECIAL_ABILITY_RESET(PLAYER_ID())
			
			//Initialise playback now, so we can pre-stream assets
			INITIALISE_UBER_PLAYBACK(strCarrec, iCarrecMain, FALSE)
			LOAD_CHASE_UBER_DATA()
			fCurrentPlaybackTime = 0.0
			SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
			SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
			switch_SetPieceCar_to_ai_on_collision = TRUE
			allow_veh_to_stop_on_any_veh_impact = TRUE
			fUberPlaybackDensitySwitchOffRange = 200.0
			iDontSwitchThisSetpieceRecordingToAI = SetPieceCarRecording[SETPIECE_PACKER_INDEX]

			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			//Create the owner and hide him for now, he turns up later.
			pedCarOwner = CREATE_PED(PEDTYPE_MISSION, modelCarOwner, <<-1886.8330, -579.0045, 10.8163>>, 315.0557)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_LEG, 0, 1)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_SPECIAL, 1, 0)

			SET_ENTITY_VISIBLE(pedCarOwner, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelCarOwner)
			
			TEXT_LABEL_63 strCamAnim = ""
			bSafeToPlayCamAnim = FALSE
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
				SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(sMainCars[iPlayersCar].veh, TRUE)
			ENDIF
			
			WAIT(0)

			//Set up the synchronised scenes
			IF iBuddiesCar = NINEF_INDEX				
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
				AND NOT IS_PED_INJURED(sLamar.ped)
					SET_PED_PATH_CAN_USE_CLIMBOVERS(sLamar.ped, TRUE) //Reset from previous stage.
					
					iSyncSceneCar = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
					FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, FALSE)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iBuddiesCar].veh, iSyncSceneCar, "carrace_walktocar_ninef2_car_ninef2", strChaseStartAnims, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)

					sLamar.iSyncedScene = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
					CLEAR_PED_TASKS_IMMEDIATELY(sLamar.ped)
					TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseStartAnims, "carrace_walktocar_ninef2_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT) 
					
					FLOAT fStartDistFromCar = VDIST2(<<-1883.3, -576.7, 11.2>>, GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh))
					FLOAT fDistFromCar = VDIST2(GET_ENTITY_COORDS(sLamar.ped), GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh))
					
					IF fStartDistFromCar - fDistFromCar < 4.0
						SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.02)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.02)
					
						/*IF NOT IS_ENTITY_ON_SCREEN(sLamar.ped)
							//If Lamar is off-screen skip his anim forward a bit to avoid the pop in B*1778864.
							SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.06)
							SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.06)
						ELSE
							SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.02)
							SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.02)
						ENDIF*/
					ELIF fStartDistFromCar - fDistFromCar < 9.0
						SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.07)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.07)
					ELIF fStartDistFromCar - fDistFromCar < 16.0
						SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.09)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.09)
					ELIF fStartDistFromCar - fDistFromCar < 25.0
						SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.11)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.11)
					ELSE
						SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.13)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.13)
					ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sLamar.ped)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sMainCars[iBuddiesCar].veh)
					
					strCamAnim = "carrace_walktocar_ninef2_cam"
				ENDIF
			ELIF iBuddiesCar = RAPIDGT_INDEX			
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
				AND NOT IS_PED_INJURED(sLamar.ped)
					iSyncSceneCar = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
					FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, FALSE)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iBuddiesCar].veh, iSyncSceneCar, "carrace_walktocar_rapidgt_car_rapidgt2", strChaseStartAnims, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				
					BOOL bLamarWasAlreadyInCar = IS_PED_IN_VEHICLE(sLamar.ped, sMainCars[RAPIDGT_INDEX].veh)
					BOOL bSkippedPlayerEntry = FALSE
				
					CLEAR_PED_TASKS_IMMEDIATELY(sLamar.ped)
					sLamar.iSyncedScene = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
				
					TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseStartAnims, "carrace_walktocar_rapidgt_lamar", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)					
					
					IF bLamarAlreadyGotIntoCar
					OR bLamarWasAlreadyInCar
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(sMainCars[RAPIDGT_INDEX].veh, SC_DOOR_FRONT_LEFT) > 0.3
							SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.14)
							SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.14)
						ELSE
							IF bLamarWasAlreadyInCar
								SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.25)
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.25)
							ELSE
								SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.14)
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.14)
							ENDIF
						ENDIF
						
						bSkippedPlayerEntry = TRUE
					ELSE
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(sMainCars[RAPIDGT_INDEX].veh, SC_DOOR_FRONT_LEFT) > 0.0
							SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.14)
							SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.14)
							
							bSkippedPlayerEntry = TRUE
						ELSE
							IF bLamarGivenWalkToCarTask
								SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.08)
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.08)
							ELSE
								SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.03)
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, 0.03)
							ENDIF
						ENDIF
					ENDIF
					
					IF bSkippedPlayerEntry
						//Shut the doors on the player's car immediately as there isn't enough time to close them normally.
						IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
						
							SET_VEHICLE_DOOR_SHUT(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_LEFT, TRUE)
							SET_VEHICLE_DOOR_SHUT(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_RIGHT, TRUE)
						ENDIF
					ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sLamar.ped)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sMainCars[iBuddiesCar].veh)
					
					strCamAnim = "carrace_walktocar_rapidgt_cam"
				ENDIF
			ENDIF

			camCutscene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
			IF iPlayersCar = NINEF_INDEX
				iCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
			ELSE
				iCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
			ENDIF
			PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, strCamAnim, strChaseStartAnims)
			SET_SYNCHRONIZED_SCENE_PHASE(iCameraSyncScene, GET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene))

			//Initialising uber playback switches off all the roads, we need them on for the first part of the cutscene.
			SWITCH_ALL_RANDOM_CARS_ON()
			CLEAR_AREA_OF_OBJECTS(<<-1878.6208, -577.6438, 10.7868>>, 100.0)
			CLEAR_AREA_OF_PEDS(<<-1878.6208, -577.6438, 10.7868>>, 100.0)
			CLEAR_AREA_OF_PROJECTILES(<<-1878.6208, -577.6438, 10.7868>>, 100.0)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1886.993652,-585.902039,6.540985>>, <<-1852.676758,-542.452393,20.403040>>, 22.250000)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1865.273438,-558.103577,10.637986>>, <<-1879.192505,-575.103149,15.430193>>, 16.750000)
			SET_VEHICLE_GENERATOR_AREA_OF_INTEREST(<<-1910.6, -531.3, 11.2>>, 30.0)
			
			//Have to stop playback for vehicle anims to work correctly
			IF IS_VEHICLE_DRIVEABLE(sMainCars[NINEF_INDEX].veh)
				SET_ENTITY_HEADING(sMainCars[NINEF_INDEX].veh, sMainCars[NINEF_INDEX].fStartHeading)
				SET_ENTITY_COORDS(sMainCars[NINEF_INDEX].veh, sMainCars[NINEF_INDEX].vStartPos)
				
				IF iPlayersCar = NINEF_INDEX
					SET_VEHICLE_ENGINE_ON(sMainCars[NINEF_INDEX].veh, FALSE, FALSE)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[RAPIDGT_INDEX].veh)
				SET_ENTITY_HEADING(sMainCars[RAPIDGT_INDEX].veh, sMainCars[RAPIDGT_INDEX].fStartHeading)
				SET_ENTITY_COORDS(sMainCars[RAPIDGT_INDEX].veh, sMainCars[RAPIDGT_INDEX].vStartPos)
				
				IF iPlayersCar = RAPIDGT_INDEX
					SET_VEHICLE_ENGINE_ON(sMainCars[RAPIDGT_INDEX].veh, FALSE, FALSE)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
				SET_VEHICLE_RADIO_ENABLED(sMainCars[iBuddiesCar].veh, FALSE)
			ENDIF
						
			IF IS_AUDIO_SCENE_ACTIVE("ARM_1_CHOOSE_CAR")
				STOP_AUDIO_SCENE("ARM_1_CHOOSE_CAR")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_GET_IN_CAR_SCENE")
				START_AUDIO_SCENE("ARM_1_GET_IN_CAR_SCENE")
			ENDIF
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sLamar.ped, -1, SLF_WHILE_NOT_IN_FOV)
			ENDIF
			
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.35)
			CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
			
			REMOVE_ANIM_DICT(strLamarAnnoyedAnims)
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)	
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			SETTIMERB(0)
			
			DO_FADE_IN_WITH_WAIT()

			IF iBuddiesCar = NINEF_INDEX
				CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_CHOICE2", CONV_PRIORITY_MEDIUM)
			ELSE
				IF NOT bLamarAlreadyGotIntoCar
					CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_CHOICE", CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
			
			//Record the cutscene where they take off in the cars.
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			bHasFirstPersonFlashTriggered = FALSE
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_RUNNING		
		REQUEST_TRAFFIC_IN_ADVANCE(5000.0)
		REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
		REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_MED_01")
	
		FLOAT fScenePhase = 0.0
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iCameraSyncScene)
			fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iCameraSyncScene)
		ENDIF
	
		#IF IS_DEBUG_BUILD
			PRINTLN(fScenePhase)
		#ENDIF
	
		//Sometimes the player's door doesn't shut, so shut it here.
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			IF (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.19 AND fScenePhase < 0.39)
			OR (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.09 AND fScenePhase < 0.3)
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_LEFT) < 0.01
					SET_VEHICLE_DOOR_SHUT(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_LEFT, TRUE)
				ELSE
					SET_VEHICLE_DOOR_CONTROL(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, GET_VEHICLE_DOOR_ANGLE_RATIO(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_LEFT) * 0.9)
				ENDIF
				
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_RIGHT) < 0.01
					SET_VEHICLE_DOOR_SHUT(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_RIGHT, TRUE)
				ELSE
					SET_VEHICLE_DOOR_CONTROL(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, GET_VEHICLE_DOOR_ANGLE_RATIO(sMainCars[iPlayersCar].veh, SC_DOOR_FRONT_RIGHT) * 0.9)
				ENDIF
			ENDIF
		ENDIF
	
		SWITCH iCurrentEvent
			CASE 0 //Start Franklin's synced scene off-camera.
				IF iPlayersCar = NINEF_INDEX
					LOAD_STREAM("ARM_1_IG_1_LAMAR_DRIVES_OFF")
				ELSE
					LOAD_STREAM("ARM_1_IG_2_LAMAR_DRIVES_OFF_ALT")
				ENDIF
			
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.23)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.25)
					iPlayerSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
					
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					IF iBuddiesCar = NINEF_INDEX
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseStartAnims, "carrace_walktocar_ninef2_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iPlayersCar].veh, iSyncSceneCar, "carrace_walktocar_ninef2_car_rapidgt2", strChaseStartAnims, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ELIF iBuddiesCar = RAPIDGT_INDEX
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseStartAnims, "carrace_walktocar_rapidgt_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iPlayersCar].veh, iSyncSceneCar, "carrace_walktocar_rapidgt_car_ninef2", strChaseStartAnims, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
						ROLL_DOWN_WINDOWS(sMainCars[iPlayersCar].veh)
					ENDIF
					
					PLAY_STREAM_FRONTEND()
					
					SET_SYNCHRONIZED_SCENE_PHASE(iPlayerSyncScene, fScenePhase)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 1 //Start Franklin's roof.
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.25)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.24)
					LOWER_CONVERTIBLE_ROOF(sMainCars[iPlayersCar].veh)
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 2 //Lower Lamar's roof: note this needs to be done at a good spot as it stops the synced scene.
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.272)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.24)
					IF NOT bHasTextLabelTriggered[ARM1_RACE_PRELOAD]
						IF (iBuddiesCar = NINEF_INDEX AND PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RACE2", CONV_PRIORITY_MEDIUM))
						OR (iBuddiesCar = RAPIDGT_INDEX AND PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RACE", CONV_PRIORITY_MEDIUM))
							bHasTextLabelTriggered[ARM1_RACE_PRELOAD] = TRUE
						ELSE
							KILL_FACE_TO_FACE_CONVERSATION() //In case the first part of the conversation streamed late, we want to get rid of it in case it delays the second part.
						ENDIF
					ENDIF
				ENDIF
			
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.32)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.3)
					IF iBuddiesCar = NINEF_INDEX
						LOWER_CONVERTIBLE_ROOF(sMainCars[iBuddiesCar].veh)
					ENDIF
						
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 3 //Start the main dialogue.	
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.33)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.34)
					IF (iBuddiesCar = NINEF_INDEX AND CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RACE2", CONV_PRIORITY_MEDIUM))
					OR (iBuddiesCar = RAPIDGT_INDEX AND CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConversationPeds, "ARM1AUD", "ARM1_RACE", "ARM1_RACE_3", CONV_PRIORITY_MEDIUM))
						bHasTextLabelTriggered[ARM1_RACE] = TRUE
							
						SETTIMERB(0)
						iCurrentEvent++
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION() //In case the first part of the conversation streamed late, we want to get rid of it as soon as possible.
					ENDIF
				ENDIF
			BREAK
			
			CASE 4 //Clear the area.
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.39)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.39)					
					CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1865.273438,-558.103577,10.637986>>, <<-1879.192505,-575.103149,15.430193>>, 16.750000)
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					
					IF iBuddiesCar = RAPIDGT_INDEX
						LOWER_CONVERTIBLE_ROOF(sMainCars[iBuddiesCar].veh)
					ENDIF
				
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 5 //Restart Lamar's car synced scene: this seems to keep the roof going too.
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF NOT bHasTextLabelTriggered[AR1_CONVHELP]
					IF TIMERB() > 1500
						PRINT_HELP("AR1_CONVHELP", 9000)
						bHasTextLabelTriggered[AR1_CONVHELP] = TRUE
					ENDIF
				ENDIF
			
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.65)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.65)		
					IF iBuddiesCar = NINEF_INDEX
						iSyncSceneCar = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.850>>, <<1.000, -0.000, 139.040>>)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iBuddiesCar].veh, iSyncSceneCar, "carrace_walktocar_ninef2_car_ninef2", strChaseStartAnims, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, fScenePhase)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sMainCars[iBuddiesCar].veh)
					ELSE
						iSyncSceneCar = CREATE_SYNCHRONIZED_SCENE(<<-1883.150, -579.600, 10.800>>, <<1.000, -0.000, 139.040>>)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iBuddiesCar].veh, iSyncSceneCar, "carrace_walktocar_rapidgt_car_rapidgt2", strChaseStartAnims, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneCar, fScenePhase)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sMainCars[iBuddiesCar].veh)
					ENDIF
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 6 //Prepare Franklin's car for driving off.
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				IF NOT bHasTextLabelTriggered[AR1_CONVHELP]
					PRINT_HELP("AR1_CONVHELP", 9000)
					bHasTextLabelTriggered[AR1_CONVHELP] = TRUE
				ENDIF
			
				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.87)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.95)
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
						SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, TRUE, TRUE)
					ENDIF
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
						
			CASE 7 //Cut back to gameplay.	
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] CHASE_START_CUTSCENE_ANIMATED_CAMS: fScenePhase = ", fScenePhase, " TIMERB = ", TIMERB())
				
				IF NOT bHasFirstPersonFlashTriggered
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					OR GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_BONNET
						IF (iBuddiesCar = NINEF_INDEX AND TIMERB() > 2560)
						OR (iBuddiesCar = RAPIDGT_INDEX AND TIMERB() > 350)
							CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Triggering flash: fScenePhase = ", fScenePhase, " TIMERB = ", TIMERB())
							
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bHasFirstPersonFlashTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF

				IF (iBuddiesCar = NINEF_INDEX AND fScenePhase > 0.99)
				OR (iBuddiesCar = RAPIDGT_INDEX AND fScenePhase > 0.99)
					//If Lamar's roof hasn't lowered yet then do it now: needs a frame to wait.
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
						LOWER_CONVERTIBLE_ROOF(sMainCars[iBuddiesCar].veh, TRUE)
						
						IF NOT IS_PED_INJURED(sLamar.ped)
							STOP_SYNCHRONIZED_ENTITY_ANIM(sLamar.ped, NORMAL_BLEND_OUT, TRUE)
							CLEAR_PED_TASKS_IMMEDIATELY(sLamar.ped)
							SET_PED_INTO_VEHICLE(sLamar.ped, sMainCars[iBuddiesCar].veh, VS_DRIVER)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sLamar.ped)
						ENDIF
						
						WAIT(0)
					ENDIF
				
					//Start the buddies recording (trigger car for the uber chase)
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
						//Guarantee that the roof is down for the buddy at this point.
						STOP_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iBuddiesCar].veh, INSTANT_BLEND_OUT, TRUE)
						
						LOWER_CONVERTIBLE_ROOF(sMainCars[iBuddiesCar].veh, TRUE)
						SET_VEHICLE_FIXED(sMainCars[iBuddiesCar].veh)
					
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh)
							REMOVE_VEHICLE_RECORDING(sMainCars[iBuddiesCar].iStartCarrec, strCarrec)
						ENDIF
						
						START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, iCarrecMain, strCarrec)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, UBER_SKIP_TIME)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iBuddiesCar].veh)
					ENDIF
					
					//Stop player's custom anims
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh)
						ENDIF

						SET_VEHICLE_DOORS_SHUT(sMainCars[iPlayersCar].veh, TRUE)
												
						IF iPlayersCar = RAPIDGT_INDEX
							SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 321.0557)
							SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<-1880.0450, -577.4576, 10.7658>>)
						ELSE
							SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 320.1515)
							SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<-1878.4385, -579.7686, 10.7883>>)
						ENDIF
						
						STOP_SYNCHRONIZED_ENTITY_ANIM(sMainCars[iPlayersCar].veh, INSTANT_BLEND_OUT, TRUE)
						STOP_SYNCHRONIZED_ENTITY_ANIM(PLAYER_PED_ID(), INSTANT_BLEND_OUT, TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh, VS_DRIVER)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
					
					//Car owner runs out (currently disabled as we're trying out having him run later).
					IF NOT IS_PED_INJURED(pedCarOwner)
						SET_ENTITY_VISIBLE(pedCarOwner, TRUE)
						TASK_LOOK_AT_ENTITY(pedCarOwner, PLAYER_PED_ID(), -1)
						//TASK_COMBAT_PED(pedCarOwner, PLAYER_PED_ID())
					ENDIF
					
					SWITCH_ALL_RANDOM_CARS_OFF()
					CLEAR_AREA_OF_VEHICLES(<<-1883.704956,-578.825195,11.330235>>, 500.0)
					CREATE_ALL_WAITING_UBER_CARS()
					DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
					UPDATE_UBER_PLAYBACK(sMainCars[iBuddiesCar].veh, 1.0)					
					
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
//					//Stop recording cutscene.
//					REPLAY_STOP_EVENT()
					
					eSectionStage = SECTION_STAGE_CLEANUP
				ENDIF
			BREAK
		ENDSWITCH		
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP
		KILL_FACE_TO_FACE_CONVERSATION()
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			TASK_CLEAR_LOOK_AT(sLamar.ped)   
		ENDIF
	
		REMOVE_ANIM_DICT(strChaseStartAnims)
		
		CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
		CASCADE_SHADOWS_INIT_SESSION()
	
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_CHASE
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_CHASE, TRUE)
	ENDIF
ENDPROC

BOOL bHackStopRecordingFlag = FALSE
INT iTimeToStopRecording

PROC CHASE_BUDDY()
	FLOAT fUberJumpTime = 1250.0

	IF eMissionStage = STAGE_CHASE_MID_POINT
		fUberJumpTime = 83500.0
	ENDIF

	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		VECTOR vPlayerWarpPos = sMainCars[0].vStartPos
		FLOAT fPlayerWarpHeading = fFranklinLeadOutHeading
		
		IF eMissionStage = STAGE_CHASE_MID_POINT
			vPlayerWarpPos = <<-1155.1976, -873.2174, 10.6185>>
			fPlayerWarpHeading = -150.0
		ENDIF
	
		IF bUsedACheckpoint
			START_REPLAY_SETUP(vPlayerWarpPos, fPlayerWarpHeading, FALSE)
			
			iCurrentEvent = 99
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerWarpPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerWarpHeading)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			WAIT(0)
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			
			iCurrentEvent = 99
		ENDIF
		
		SETUP_REQ_CAR_CHOICES()
		REQUEST_MODEL(modelCarOwner)
		
		WHILE NOT SETUP_REQ_LOAD_MAIN_CHASE_RECORDING()
		OR NOT DOES_ENTITY_EXIST(sLamar.ped)
		OR NOT DOES_ENTITY_EXIST(sMainCars[0].veh)
		OR NOT DOES_ENTITY_EXIST(sMainCars[1].veh)
		OR NOT HAS_MODEL_LOADED(modelCarOwner)
			SETUP_LAMAR(vLamarLeadOutPos)
			SETUP_NINEF(sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
			SETUP_RAPIDGT(sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
			REQUEST_MODEL(modelCarOwner)
			REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
			REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_MED_01")
		
			WAIT(0)
		ENDWHILE
		
		END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
		SETUP_REQ_PUT_PLAYER_INTO_CHOSEN_CAR()
		SETUP_REQ_LAMAR_INTO_CHOSEN_CAR()
		SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
		BLOCK_VEHICLE_GENS_AT_START_POINT(TRUE)
		
		SPECIAL_ABILITY_RESET(PLAYER_ID())
		
		IF eMissionStage = STAGE_CHASE
			pedCarOwner = CREATE_PED(PEDTYPE_MISSION, modelCarOwner, <<-1886.8330, -579.0045, 10.8163>>, 315.0557)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_LEG, 0, 1)
			SET_PED_COMPONENT_VARIATION(pedCarOwner, PED_COMP_SPECIAL, 1, 0)
			TASK_LOOK_AT_ENTITY(pedCarOwner, PLAYER_PED_ID(), -1)
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(modelCarOwner)
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			IF eMissionStage = STAGE_CHASE_MID_POINT
				SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<-1153.1976, -875.2174, 10.6185>>)
				SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, -150.0)
			ENDIF
			
			SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, TRUE, TRUE)
		ENDIF
		
		SET_INITIAL_PLAYER_STATION("RADIO_03_HIPHOP_NEW")
		FREEZE_RADIO_STATION("RADIO_03_HIPHOP_NEW")
		SET_RADIO_AUTO_UNFREEZE(FALSE)
		SET_RADIO_TRACK("RADIO_03_HIPHOP_NEW", "ARM1_RADIO_STARTS")
			
		//Start buddy recording
		INITIALISE_UBER_PLAYBACK(strCarrec, iCarrecMain)
		LOAD_CHASE_UBER_DATA()
		SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
		SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
		switch_SetPieceCar_to_ai_on_collision = TRUE
		allow_veh_to_stop_on_any_veh_impact = TRUE
		fUberPlaybackDensitySwitchOffRange = 200.0
		iDontSwitchThisSetpieceRecordingToAI = SetPieceCarRecording[SETPIECE_PACKER_INDEX]
		
		REQUEST_TRAFFIC_IN_ADVANCE(fUberJumpTime + 4000.0, fUberJumpTime - 5000.0)
		
		WAIT(500)
							
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
		AND IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, iCarrecMain, strCarrec)
			SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, fUberJumpTime)
			SET_UBER_PLAYBACK_TO_TIME_NOW(sMainCars[iBuddiesCar].veh, fUberJumpTime)
			
			IF eMissionStage = STAGE_CHASE
				//START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, sMainCars[iPlayersCar].iStartCarrec, strCarrec)
				//SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 2000.0)
				SPECIAL_ABILITY_DEPLETE_METER(PLAYER_ID(), TRUE)
				ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
			ELSE
				SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 210.9189)
				SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<-1153.1976, -875.2174, 10.6185>>)
				SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[iPlayersCar].veh)
				ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
			ENDIF
						
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			WAIT(100)
			
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP	
		SETUP_REQ_ARRIVAL_RECORDING_CHOICES()
		
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_VEHICLE_POPULATION_BUDGET(1)
		SET_PED_POPULATION_BUDGET(2)
		bTruckHornActivated = FALSE
		bBusHornActivated = FALSE
		bVanHornActivated = FALSE
		bBuddyFinished = FALSE
		bLamarJustTeasedFranklin = FALSE
		bLamarJustInstructedFranklin = FALSE
		bAliensStatHasBeenSet = FALSE
		bMovieBarrierSmashed = FALSE
		bCyclistsSetToFlee = FALSE
		bOverriddenVanColour = FALSE
		fCurrentPlaybackSpeed = 1.0
		iTimeSinceLastHorn = 0
		sRageData.bHasRaged = FALSE
		sRageData.fCurrentRage = 0.0
		CLEAR_TRIGGERED_LABELS()
		
		DISABLE_VEHICLE_GEN_ON_MISSION(TRUE) //Stops vehicle gen controller from requesting assets during the chase.
		SET_FORCED_OBJECTS_FOR_CHASE(TRUE)
		BLOCK_SCENARIOS_FOR_CHASE(TRUE)
		DISABLE_TAXI_HAILING(TRUE)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM1_CAR_DAMAGED, FALSE)
		
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		UNFREEZE_RADIO_STATION("RADIO_03_HIPHOP_NEW")
		SET_RADIO_AUTO_UNFREEZE(TRUE)
		
		//SET_AGGRESSIVE_HORNS(TRUE)
		
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(sMainCars[iPlayersCar].veh)
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(sMainCars[iPlayersCar].veh)
		
		SETTIMERA(0)
		SETTIMERB(0)
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(sMainCars[iPlayersCar].veh, TRUE)
			iPlayersHealthBeforeRace = GET_ENTITY_HEALTH(sMainCars[iPlayersCar].veh)
		ENDIF	
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
			ACTIVATE_PHYSICS(sMainCars[iBuddiesCar].veh)
			SET_VEHICLE_ACTIVE_DURING_PLAYBACK(sMainCars[iBuddiesCar].veh, TRUE)
			SET_VEHICLE_ACT_AS_IF_HIGH_SPEED_FOR_FRAG_SMASHING(sMainCars[iBuddiesCar].veh, TRUE)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(sMainCars[iBuddiesCar].veh, "ARM_1_LAMARS_CAR")
			iLamarsHealthBeforeRace = GET_ENTITY_HEALTH(sMainCars[iBuddiesCar].veh)
		ENDIF
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sLamar.ped, TRUE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_GetOutBurningVehicle, FALSE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(sLamar.ped, PCF_DisableExplosionReactions, TRUE)
			SET_PED_LOD_MULTIPLIER(sLamar.ped, 2.0) //Prevents LOD issues at the end of the chase if the player stands by Lamar without triggering the cutscene.
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_1_GET_IN_CAR_SCENE")
			STOP_AUDIO_SCENE("ARM_1_GET_IN_CAR_SCENE")
		ENDIF
		
		//STOP_STREAM() //This stops the custom stream for the chase start cutscene. Shouldn't need to be stopped by script?
		
		IF eMissionStage = STAGE_CHASE
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_START")
				START_AUDIO_SCENE("ARM_1_DRIVE_START")
			ENDIF
		
			ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		ELSE
			//If we're jumping to half-way through the chase make sure anything that takes place beforehand doesn't trigger.
			ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)

			bHasTextLabelTriggered[AR1_CHASE] = TRUE
			bHasTextLabelTriggered[AR1_CAMHELP] = TRUE
			bHasTextLabelTriggered[AR1_BRAKE] = TRUE
			bHasTextLabelTriggered[ARM1_CYCL] = TRUE
			bHasTextLabelTriggered[ARM1_ALIEN] = TRUE
			bHasTextLabelTriggered[ARM1_TOW] = TRUE
			
			IF bAllowSpecialAbilityDuringChase
				bHasTextLabelTriggered[AR1_RAGEBAR] = TRUE
				bHasTextLabelTriggered[AR1_RAGEHOW] = TRUE
				bHasTextLabelTriggered[AR1_RAGESTAT] = TRUE
				sRageData.bHasRaged = TRUE
				bSkippedSpecialAbilityTutorial = TRUE
			ENDIF
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_MID_CHASE, "CHASE_MID_POINT")
		ENDIF
		
		iChasePedBlockingArea = ADD_NAVMESH_BLOCKING_OBJECT(<<-334.181854,-673.711304,33.338654>>, <<28.250000,3.000000,3.000000>>, 175.8)
		iChasePedBlockingArea2 = ADD_NAVMESH_BLOCKING_OBJECT(<<-1138.2, -723.6, 20.0>>, <<10.0000,10.000000,3.000000>>, 91.3)
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
		
		IF IS_SCREEN_FADED_OUT()
			SETTIMERB(0)
		
			//Spend a small bit of time getting the chase going before fading in.
			WHILE TIMERB() < 500
				REQUEST_TRAFFIC_IN_ADVANCE(fUberJumpTime + 5000.0, fUberJumpTime - 5000.0)
				CREATE_ALL_WAITING_UBER_CARS()
				DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
				UPDATE_UBER_PLAYBACK(sMainCars[iBuddiesCar].veh, 1.0)
				
				WAIT(0)
			ENDWHILE
		
			IF eMissionStage = STAGE_CHASE_MID_POINT
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					SET_VEHICLE_FORWARD_SPEED(sMainCars[iPlayersCar].veh, 20.0)
				ENDIF
			ENDIF
		
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			
			WHILE NOT IS_SCREEN_FADED_IN()
				REQUEST_TRAFFIC_IN_ADVANCE(fUberJumpTime + 5000.0, fUberJumpTime - 5000.0)
				DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
				UPDATE_UBER_PLAYBACK(sMainCars[iBuddiesCar].veh, 1.0)
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				WAIT(0)
			ENDWHILE
		ENDIF		
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(sMainCars[iPlayersCar].veh, TRUE)
		ENDIF
		
		IF eMissionStage = STAGE_CHASE
			SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
		ENDIF
				
		//REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
		
		iTimeToStopRecording = GET_GAME_TIMER()
		
		SETUP_REQ_REMOVE_BANK_SHUTTERS()
		
		iAudioSceneEventDialPhone = 0
		iAudioSceneEventMovieStudio = 0
		iAudioSceneEventBumps = 0
		iAudioSceneEventCarPark = 0
		iRageHelpTimer = 0
		bCarOwnerActive = FALSE
		iHintCamHelpTimer = 0
		bCarCollisionIsDisabled = FALSE
		bDialToneTriggered = FALSE
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF

	IF eSectionStage = SECTION_STAGE_RUNNING	
		DISABLE_CELLPHONE_THIS_FRAME_ONLY() //Don't allow phone during chase, due to being on a fake phone call with Lamar while driving.
	
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
		//Have the car owner attack the player if he hangs around, and remove the ped once the player drives off.
		IF NOT IS_PED_INJURED(pedCarOwner)
			IF NOT bCarOwnerActive
				IF fCurrentPlaybackTime > 15000.0
				OR (IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh) AND GET_ENTITY_SPEED(sMainCars[iPlayersCar].veh) > 1.0)
				OR IS_SPHERE_VISIBLE(<<-1885.0, -577.6, 12.1>>, 1.0)
					bCarOwnerActive = TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_IN_COMBAT(pedCarOwner)
				AND GET_SCRIPT_TASK_STATUS(pedCarOwner, SCRIPT_TASK_COMBAT) != PERFORMING_TASK
					TASK_COMBAT_PED(pedCarOwner, PLAYER_PED_ID())				
				ENDIF
			ENDIF
		
			IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(pedCarOwner)) > 2500.0
				IF NOT IS_ENTITY_ON_SCREEN(pedCarOwner)
					REMOVE_PED(pedCarOwner, FALSE)
				ENDIF
			ENDIF
		ENDIF

		IF bHackStopRecordingFlag = FALSE
		AND GET_GAME_TIMER() - iTimeToStopRecording  > 10000
			//Stop recording cutscene.
			REPLAY_STOP_EVENT()
			bHackStopRecordingFlag = TRUE
		ENDIF

		REQUEST_ANIM_DICT(strLamarCarAnimsCrash)
		REQUEST_ANIM_DICT(strLamarCarAnimsTaunt)
		REQUEST_ANIM_DICT(strFranklinCarAnimsTaunt)
		REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
		REQUEST_SCRIPT_AUDIO_BANK("CAR_CRASHES_MED_01")
	
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
		AND IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)			
			//Deal with carrying over the chase start recording from the cutscene.
			IF iCurrentEvent = 0
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
					FLOAT fTransitionPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sMainCars[iPlayersCar].veh)

					IF GET_VEHICLE_DOOR_LOCK_STATUS(sMainCars[iPlayersCar].veh) != VEHICLELOCK_LOCKED_PLAYER_INSIDE
						SET_VEHICLE_DOORS_LOCKED(sMainCars[iPlayersCar].veh, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
					ENDIF
				
					IF (iPlayersCar = RAPIDGT_INDEX AND fTransitionPlaybackTime > 3900.0)
					OR (iPlayersCar = NINEF_INDEX AND fTransitionPlaybackTime > 3600.0)
						STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh)
						REMOVE_VEHICLE_RECORDING(sMainCars[iPlayersCar].iStartCarrec, strCarrec)
					ENDIF
				ELSE
					IF GET_VEHICLE_DOOR_LOCK_STATUS(sMainCars[iPlayersCar].veh) != VEHICLELOCK_UNLOCKED
						SET_VEHICLE_DOORS_LOCKED(sMainCars[iPlayersCar].veh, VEHICLELOCK_UNLOCKED)
					ENDIF
				ENDIF
			ENDIF
		
			FLOAT fDistBetweenCars = VDIST2(vPlayerPos, GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh))
			
			//Get the closest vehicle to the player periodically: this is used to check dialogue and horn triggers elsewhere.
			VEHICLE_INDEX vehClosest = GET_CLOSEST_VEHICLE(vPlayerPos, 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()) //GET_CLOSEST_VEHICLE_TO_PLAYER_EXCLUDING_CHASE_CARS()
			
			//Update the uber chase.
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
				fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sMainCars[iBuddiesCar].veh)
				
				//PRELOAD_ALL_CHASE_RECORDINGS(fCurrentPlaybackTime)

				IF bUseNewRubberBanding
					RUBBER_BAND_TEST(fCurrentPlaybackSpeed, sMainCars[iPlayersCar].veh, sMainCars[iBuddiesCar].veh)
				ELSE
					//RUBBER_BAND_CHASE(fCurrentPlaybackSpeed, sMainCars[iPlayersCar].veh, sMainCars[iBuddiesCar].veh)
					RUBBER_BAND_CHASE_CLOSE_VERSION(fCurrentPlaybackSpeed, sMainCars[iPlayersCar].veh, sMainCars[iBuddiesCar].veh)
				ENDIF
				
				fUberMinTimeBeforePlaybackStartToCreate = 7000.0
				
				DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
				UPDATE_UBER_PLAYBACK(sMainCars[iBuddiesCar].veh, fCurrentPlaybackSpeed)
				SET_PLAYBACK_SPEED(sMainCars[iBuddiesCar].veh, fCurrentPlaybackSpeed)
				MANAGE_CHASE_SET_PIECES()
				MANAGE_LAMARS_CAR_LIGHTS(sMainCars[iBuddiesCar].veh, fCurrentPlaybackTime)
				
				//TEMP: There's a new traffic light that the car passes through (v209), turn off collision around there.
				//50700.0 - 50900.0
				IF NOT bCarCollisionIsDisabled
					//IF VDIST2(GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh), <<-980.11, -435.25, 36.99>>) < 9.0
					IF fCurrentPlaybackTime > 50500.0 AND fCurrentPlaybackTime < 51100.0
						SET_ENTITY_COLLISION(sMainCars[iBuddiesCar].veh, FALSE)
						bCarCollisionIsDisabled = TRUE
					ENDIF
				ELSE
					//IF VDIST2(GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh), <<-980.11, -435.25, 36.99>>) > 9.0
					IF fCurrentPlaybackTime > 51100.0
						SET_ENTITY_COLLISION(sMainCars[iBuddiesCar].veh, TRUE)
						bCarCollisionIsDisabled = FALSE
					ENDIF
				ENDIF
				
				//Once the buddy finishes clean up playback.
				IF NOT bBuddyFinished
					IF fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 4000.0
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh)
						ENDIF
						
						CLEANUP_UBER_PLAYBACK()
						
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sMainCars[iBuddiesCar].veh)
					
						bCloseFinish = FALSE
						bBuddyFinished = TRUE
					ENDIF
				ENDIF
				
				IF NOT bMovieBarrierSmashed
					IF iBuddiesCar = NINEF_INDEX
					AND fCurrentPlaybackTime >= 54635.0
						OBJECT_INDEX objBarrier = GET_CLOSEST_OBJECT_OF_TYPE(<<-1052.49, -476.15, 36.66>>, 5.0, PROP_SEC_BARRIER_LD_01A, FALSE)
						
						IF DOES_ENTITY_EXIST(objBarrier)
							SET_ENTITY_INVINCIBLE(sMainCars[iBuddiesCar].veh, TRUE)
							//ADD_EXPLOSION(<<-1052.9, -475.1, 36.9>>, EXP_TAG_GRENADE, 0.5, FALSE, TRUE, 0.0)
							//APPLY_FORCE_TO_ENTITY(objBarrier, APPLY_TYPE_EXTERNAL_IMPULSE, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.0, 0.0, 121.7>>) * 5.0, <<0.0, 0.0, 0.0>>, 0, FALSE, TRUE, TRUE)
							BREAK_OBJECT_FRAGMENT_CHILD(objBarrier, 1, FALSE)
							BREAK_OBJECT_FRAGMENT_CHILD(objBarrier, 2, FALSE)
						ENDIF
						
						bMovieBarrierSmashed = TRUE
					ENDIF
				ELSE
					IF fCurrentPlaybackTime > 56000.0
					AND fCurrentPlaybackTime < 56500.0
						SET_ENTITY_INVINCIBLE(sMainCars[iBuddiesCar].veh, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			//Add a checkpoint when the player reaches half-way
			IF eMissionStage = STAGE_CHASE
				IF fCurrentPlaybackTime > 83000.0
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
					AND IS_ENTITY_ON_SCREEN(sMainCars[iBuddiesCar].veh)
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_MID_CHASE, "CHASE_MID_POINT")
						eMissionStage = STAGE_CHASE_MID_POINT
					ENDIF
				ENDIF
			ENDIF
			
			//Fix for B*530782 - Clear the secondary taunt anims if the player tries to leave the car.
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)	
					CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			//Set the fail distance to different values depending on different parts of the chase. This distance is used for fail conditions + blip control.
			FLOAT fCurrentFailDistance
			
			IF fCurrentPlaybackTime < 28500.0
				//Fix for 279573 - Make the fail check less harsh at the start where Lamar doubles back on himself.
				fCurrentFailDistance = 160000.0
			ELSE
				fCurrentFailDistance = 60000.0
			ENDIF
			
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)				
				DO_CHASE_HORNS(vehClosest)
				DO_NEAR_MISS_FRANKLIN_DIALOGUE(vehClosest)
				
				IF IS_THIS_PRINT_BEING_DISPLAYED("CMN_GENGETBCK")
					CLEAR_PRINTS()
				ENDIF
				
				//Keep track of when Franklin gets in front.
				IF NOT bPlayerIsAheadOfChase
					iTimeWhenFranklinGotInFront = GET_GAME_TIMER()
				ENDIF
				
				//Handle use of special ability during chase.
				IF bAllowSpecialAbilityDuringChase					
					//Do help text.
					IF NOT bHasTextLabelTriggered[AR1_RAGEBAR] //Explain the rage bar as soon as the first god text has finished printing.
						IF bHasTextLabelTriggered[AR1_CHASE]
						AND (bHasTextLabelTriggered[AR1_CAMHELP] OR NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP())
						AND NOT IS_THIS_PRINT_BEING_DISPLAYED("AR1_CHASE")
						AND fCurrentPlaybackTime > 17200.0
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF iRageHelpTimer = 0
									iRageHelpTimer = GET_GAME_TIMER()
								ELIF GET_GAME_TIMER() - iRageHelpTimer > 2500
									
									// Steve R - Different help for PC keyboard and mouse controls as we use only one input instead of two to activate special ability.
									// This command only returns TRUE if it's a PC build and you're using keyboard and mouse.
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										PRINT_HELP("AR1_RAGEBAR_KM", DEFAULT_HELP_TEXT_TIME + 7000)
									ELSE
										PRINT_HELP("AR1_RAGEBAR", DEFAULT_HELP_TEXT_TIME + 7000)
									ENDIF
									bHasTextLabelTriggered[AR1_RAGEBAR] = TRUE
									iRageHelpTimer = 0
									ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
									FLASH_ABILITY_BAR(DEFAULT_HELP_TEXT_TIME)
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT bHasTextLabelTriggered[AR1_RAGEHOW] //Explain how to fill up the rage bar.
						IF iRageHelpTimer = 0
							IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
								iRageHelpTimer = GET_GAME_TIMER()
							ENDIF
						ELIF GET_GAME_TIMER() - iRageHelpTimer > 2000
							PRINT_HELP("AR1_RAGEHOW", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_RAGEHOW] = TRUE
							iRageHelpTimer = GET_GAME_TIMER()
							FLASH_ABILITY_BAR(0)
						ENDIF
					ELIF NOT bHasTextLabelTriggered[AR1_RAGESTAT] //Explain how the stat works.
						IF GET_GAME_TIMER() - iRageHelpTimer > 10000
						AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("AR1_RAGESTAT", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_RAGESTAT] = TRUE
							iRageHelpTimer = GET_GAME_TIMER()
							FLASH_ABILITY_BAR(0)
						ENDIF
					ELIF NOT bHasTextLabelTriggered[AR1_RAGEDEACT] //Explain how to deactivate.
						IF NOT IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
							iRageHelpTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - iRageHelpTimer > 500
						AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							
							// Steve R - Different help for PC keyboard and mouse controls as we use only one input instead of two to activate special ability.
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								PRINT_HELP("AR1_RAGEDEACT_KM", DEFAULT_HELP_TEXT_TIME + 3000)
							ELSE
								PRINT_HELP("AR1_RAGEDEACT", DEFAULT_HELP_TEXT_TIME + 3000)
							ENDIF
							
							bHasTextLabelTriggered[AR1_RAGEDEACT] = TRUE
							iRageHelpTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
					
					IF NOT sRageData.bHasRaged
						IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
							CLEAR_ALL_RAGE_HELP()
						
							sRageData.bHasRaged = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Once the first god text has printed we're ready to print all other help and dialogue.
				IF bHasTextLabelTriggered[AR1_CHASE]
					//Hint cam help (formerly action cam help, that cam has been removed).
					IF NOT bHasTextLabelTriggered[AR1_CAMHELP]		
						IF iHintCamHelpTimer = 0
							iHintCamHelpTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - iHintCamHelpTimer > 5000
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							AND SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
							AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								//IF fDistBetweenCars < 2500.0 //50m 
									//PRINT_HELP("AR1_CAMHELP")
									bHasTextLabelTriggered[AR1_CAMHELP] = TRUE
									iHintCamHelpTimer = GET_GAME_TIMER()
									//bFloatingHelpPrintedThisFrame = TRUE
								//ENDIF
							ENDIF
						ENDIF
					ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_CAMHELP")
						IF GET_GAME_TIMER() - iHintCamHelpTimer > 6000
							CLEAR_HELP(FALSE)
							iHintCamHelpTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
					
					//Help text
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						//BOOL bFloatingHelpPrintedThisFrame = FALSE

						//Handbrake turn help
						IF fCurrentPlaybackTime > 60000.0
						AND NOT bHasTextLabelTriggered[AR1_BRAKE]
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1210.675781,-578.628662,25.423279>>, <<-1272.774536,-627.656921,37.727161>>, 25.000000)
						AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
							
							PRINT_HELP("AR1_BRAKE", DEFAULT_HELP_TEXT_TIME + 3000)
							
							bHasTextLabelTriggered[AR1_BRAKE] = TRUE
							//bFloatingHelpPrintedThisFrame = TRUE
						ENDIF
						
						//Headlight help: triggers when player reaches tunnels near end.
						IF fCurrentPlaybackTime > 100000.0
							INT iLightsOn, iFullBeam
							GET_VEHICLE_LIGHTS_STATE(sMainCars[iPlayersCar].veh, iLightsOn, iFullBeam)
						
							IF NOT bHasTextLabelTriggered[AR1_HEADHELP]
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-349.818268,-716.851257,37.136703>>, <<-351.937622,-819.926147,29.768631>>, 16.500000)
								//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-230.399643,-625.088013,32.474281>>, <<-119.145233,-665.343506,38.499069>>, 15.250000)
								//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-84.001968,-670.068848,34.660595>>, <<-89.270409,-684.364075,38.400967>>, 15.250000)
								//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<31.517536,-666.219299,30.780993>>, <<19.212603,-661.313110,35.018368>>, 15.250000)
									IF iLightsOn = 0
										PRINT_HELP("AR1_HEADHELP", DEFAULT_HELP_TEXT_TIME + 3000)
										bHasTextLabelTriggered[AR1_HEADHELP] = TRUE
										//bFloatingHelpPrintedThisFrame = TRUE
									ELSE
										bHasTextLabelTriggered[AR1_HEADHELP] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Vehicle air help: triggers on the bumpy road.
						IF fCurrentPlaybackTime > 70000.0
							IF NOT bHasTextLabelTriggered[AR1_JUMPHELP]
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1134.825024,-900.860559,30.001677>>, <<-1002.170532,-1127.489868,-2.783524>>, 27.500000)
									PRINT_HELP("AR1_JUMPHELP", DEFAULT_HELP_TEXT_TIME + 3000)
									bHasTextLabelTriggered[AR1_JUMPHELP] = TRUE
									//bFloatingHelpPrintedThisFrame = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						//1796889 - Vehicle cam view help: Triggers after the bumpy road stretch.
						IF fCurrentPlaybackTime > 100500.0 AND fCurrentPlaybackTime < 106500.0
							IF NOT bHasTextLabelTriggered[AR1_VEHCAMH]
								PRINT_HELP("AR1_VEHCAMH", DEFAULT_HELP_TEXT_TIME + 3000)
								bHasTextLabelTriggered[AR1_VEHCAMH] = TRUE
							ENDIF
						ENDIF
						
						//Vehicle air stat help: triggers when approaching the final jump by the bank.
						IF fCurrentPlaybackTime > 125000.0
							IF NOT bHasTextLabelTriggered[AR1_JUMPHELP2]
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-150.192795,-653.960266,38.701393>>, <<-69.535858,-683.460449,31.171165>>, 16.500000)
									PRINT_HELP("AR1_JUMPHELP2", DEFAULT_HELP_TEXT_TIME + 3000)
									bHasTextLabelTriggered[AR1_JUMPHELP2] = TRUE
									//bFloatingHelpPrintedThisFrame = TRUE
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
					
					//1796889 - Vehicle cam view help: clear once the player presses the change view button.
					IF bHasTextLabelTriggered[AR1_VEHCAMH]
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_VEHCAMH")
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF

					//B*547550 - Disable the phone while the headlights help is displayed.
					//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_HEADHELP")
					//	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					//ENDIF
					
					//Have Lamar beep his horn if he ploughs through Franklin while he's in front.
					IF GET_GAME_TIMER() - iInFrontHornTimer > 10000
						IF GET_GAME_TIMER() - iTimeWhenFranklinGotInFront > 0
							IF IS_ENTITY_IN_ANGLED_AREA(sMainCars[iPlayersCar].veh,
														GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sMainCars[iBuddiesCar].veh, <<0.0, 1.0, -2.0>>),
														GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sMainCars[iBuddiesCar].veh, <<0.0, 8.0, 2.0>>), 4.0)
								START_VEHICLE_HORN(sMainCars[iBuddiesCar].veh, 1500)
								iInFrontHornTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
					
					//Dialogue: triggers when the player gets close to Lamar (or crashes into him)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							//Dialogue if Franklin crashes into Lamar
							IF iCrashTimer = 0 OR GET_GAME_TIMER() - iCrashTimer > 20000	
							AND fCurrentPlaybackTime < 144600.0
								IF IS_ENTITY_TOUCHING_ENTITY(sMainCars[0].veh, sMainCars[1].veh)
									IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_CRASH", CONV_PRIORITY_MEDIUM)
										TEXT_LABEL_23 strSpecificLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
										TEXT_LABEL_23 strSpecificAnim
										
										IF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_CRASH_01")
											strSpecificAnim = "thatscominoutourchecks"
										ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_CRASH_02")
											strSpecificAnim = "stoprubbinup"
										ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_CRASH_03")
											strSpecificAnim = "imabouttotake"
										ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_CRASH_04")
											strSpecificAnim = "imgonnatell"
										ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_CRASH_05")
											strSpecificAnim = "ayyofrank"
										ELSE
											strSpecificAnim = "stoprubbinup"
										ENDIF
										
										#IF IS_DEBUG_BUILD
											PRINTLN("Armenian1.sc - playing crash anim: ", strSpecificAnim)
										#ENDIF
										
										IF HAS_ANIM_DICT_LOADED(strLamarCarAnimsCrash)
											TASK_PLAY_ANIM(sLamar.ped, strLamarCarAnimsCrash, strSpecificAnim, NORMAL_BLEND_IN, 
														   NORMAL_BLEND_OUT, -1, AF_SECONDARY)
										ENDIF
										
										iCrashTimer = GET_GAME_TIMER()
										
										//Cancel any responses if this dialogue cuts in.
										bLamarJustTeasedFranklin = FALSE
										bLamarJustInstructedFranklin = FALSE
									ENDIF
								ENDIF
							ENDIF
							
							//Dialogue for Lamar teasing Franklin
							IF GET_GAME_TIMER() - iCloseTimer > 16000
							AND NOT bLamarJustInstructedFranklin
								IF NOT bLamarJustTeasedFranklin
									//Don't play at the start, end or during the film studio, as there's a lot of other dialogue that needs to play.
									IF fCurrentPlaybackTime > 20000.0 
									AND fCurrentPlaybackTime < 144600.0
									AND (fCurrentPlaybackTime < 55000.0 OR fCurrentPlaybackTime > 60500.0) 
										IF fDistBetweenCars < 400.0 //20m
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_TEASE", CONV_PRIORITY_MEDIUM)
												TEXT_LABEL_23 strSpecificLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
												TEXT_LABEL_23 strSpecificAnim
												
												IF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_01")
													strSpecificAnim = "hahahakeepup"
												ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_02")
													strSpecificAnim = "manthisismeanttobe"
												ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_03")
													strSpecificAnim = "rememberthis"
												ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_04")
													strSpecificAnim = "cmonfrank"
												ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_05")
													strSpecificAnim = "youaintfuckin"
												ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_06")
													strSpecificAnim = "skoolinyoass"
												ELIF ARE_STRINGS_EQUAL(strSpecificLine, "ARM1_TEASE_07")
													strSpecificAnim = "keeppoping"
												ELSE
													strSpecificAnim = "cmonmynigga"
												ENDIF
												
												#IF IS_DEBUG_BUILD
													PRINTLN("Armenian1.sc - playing taunt anim: ", strSpecificAnim)
												#ENDIF
												
												IF HAS_ANIM_DICT_LOADED(strLamarCarAnimsTaunt)
													TASK_PLAY_ANIM(sLamar.ped, strLamarCarAnimsTaunt, strSpecificAnim, NORMAL_BLEND_IN, 
																   NORMAL_BLEND_OUT, -1, AF_SECONDARY)
												ENDIF
												
												bLamarJustTeasedFranklin = TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									//Play a response from Franklin
									IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RESP", CONV_PRIORITY_MEDIUM)
										iCloseTimer = GET_GAME_TIMER()
										bLamarJustTeasedFranklin = FALSE
									ENDIF
								ENDIF
							ENDIF
							
							//On some turns Lamar indicates where he's going to go
							IF NOT bLamarJustInstructedFranklin
								IF NOT bLamarJustTeasedFranklin
									//Play the first line in all cases: if this line comes first it needs to be preceded by a ringtone sound.
									IF NOT bHasTextLabelTriggered[ARM1_RINGINDIC]
										IF fCurrentPlaybackTime > 8000.0 AND fCurrentPlaybackTime < 15000.0
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RING", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[ARM1_RINGINDIC] = TRUE
												bDialToneTriggered = TRUE
											ENDIF
										ENDIF
									ELSE
										IF NOT bHasTextLabelTriggered[ARM1_INDIC]
											//If the ringtone occurred then play the first line.
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_INDIC", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[ARM1_INDIC] = TRUE
												bLamarJustInstructedFranklin = TRUE
											ENDIF
										ENDIF
									ENDIF
										
									IF fDistBetweenCars < 2500.0 //50m	
										IF fCurrentPlaybackTime > 42000.0 AND fCurrentPlaybackTime < 43000.0
											bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RIGHT", CONV_PRIORITY_MEDIUM)
										ENDIF
										
										IF fCurrentPlaybackTime > 48000.0 AND fCurrentPlaybackTime < 49000.0
											bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RIGHT", CONV_PRIORITY_MEDIUM)
										ENDIF
										
										IF fCurrentPlaybackTime > 49000.0 AND fCurrentPlaybackTime < 52000.0
											IF NOT bHasTextLabelTriggered[ARM1_STDIO]
												IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
													IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_STDIO", CONV_PRIORITY_MEDIUM)
														bLamarJustInstructedFranklin = FALSE
														bLamarJustTeasedFranklin = FALSE
														bHasTextLabelTriggered[ARM1_STDIO] = TRUE
													ENDIF
												ELSE
													IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_STDIO2", CONV_PRIORITY_MEDIUM)
														bLamarJustInstructedFranklin = FALSE
														bLamarJustTeasedFranklin = FALSE
														bHasTextLabelTriggered[ARM1_STDIO] = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
										
										IF fCurrentPlaybackTime > 61000.0 AND fCurrentPlaybackTime < 62000.0
											IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
												bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_ALLEY", CONV_PRIORITY_MEDIUM)
											ELSE
												bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_ALLEY2", CONV_PRIORITY_MEDIUM)
											ENDIF
										ENDIF
										
										IF fCurrentPlaybackTime > 78000.0 AND fCurrentPlaybackTime < 79000.0
											bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_LEFT", CONV_PRIORITY_MEDIUM)
										ENDIF
										
										IF fCurrentPlaybackTime > 95500.0 AND fCurrentPlaybackTime < 96500.0
											bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_LEFT", CONV_PRIORITY_MEDIUM)
										ENDIF
										
										IF fCurrentPlaybackTime > 119300.0 AND fCurrentPlaybackTime < 120300.0
											bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RIGHT", CONV_PRIORITY_MEDIUM)
										ENDIF
										
										IF fCurrentPlaybackTime > 123500.0 AND fCurrentPlaybackTime < 124500.0
											IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
												bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_PARK", CONV_PRIORITY_MEDIUM)
											ELSE
												bLamarJustInstructedFranklin = CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_PARK3", CONV_PRIORITY_MEDIUM)
											ENDIF
										ENDIF
										
										/*IF fCurrentPlaybackTime > 124500.0 AND fCurrentPlaybackTime < 127500.0
											IF NOT bHasTextLabelTriggered[ARM1_PARK2]
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_PARK2", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_PARK2] = TRUE
												ENDIF
											ENDIF
										ENDIF*/
									ENDIF
								ENDIF
							ELSE
								//Play a response from Franklin
								IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_OKAY", CONV_PRIORITY_MEDIUM)
									bLamarJustInstructedFranklin = FALSE
									bLamarJustTeasedFranklin = FALSE
								ENDIF
							ENDIF
							
							//Don't play any of the dialogue below if we're waiting for a response from Franklin to being instructed/insulted.
							IF NOT bLamarJustTeasedFranklin AND NOT bLamarJustInstructedFranklin
								//Bank dialogue
								IF fCurrentPlaybackTime > 141500.0 AND fCurrentPlaybackTime < 142500.0
									IF NOT bHasTextLabelTriggered[ARM1_BANK]
										IF fDistBetweenCars < 2500.0
											IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_BANK", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_BANK] = TRUE
													bLamarJustInstructedFranklin = FALSE
													bLamarJustTeasedFranklin = FALSE
												ENDIF
											ELSE
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_BANK2", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_BANK] = TRUE
													bLamarJustInstructedFranklin = FALSE
													bLamarJustTeasedFranklin = FALSE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//NOTE: this dialogue was never recorded so remove.
								//Dialogue when Franklin passes the cyclists.
								/*IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[21])
									IF NOT bHasTextLabelTriggered[ARM1_CYCL]
										IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[21])
										AND VDIST2(vPlayerPos, GET_ENTITY_COORDS(SetPieceCarID[21])) < 225.0
										AND fDistBetweenCars < 1600.0
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_CYCL", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[ARM1_CYCL] = TRUE
												bLamarJustInstructedFranklin = FALSE
												bLamarJustTeasedFranklin = FALSE
												
												IF HAS_ANIM_DICT_LOADED(strFranklinCarAnimsTaunt)
												AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), strFranklinCarAnimsTaunt, "slowassshit")
													IF GET_CONVERTIBLE_ROOF_STATE(sMainCars[iPlayersCar].veh) = CRS_LOWERED
													AND IS_ENTITY_UPRIGHT(sMainCars[iPlayersCar].veh)
														TASK_PLAY_ANIM(PLAYER_PED_ID(), strFranklinCarAnimsTaunt, "slowassshit", 
																   	   NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF*/
								
								//Dialogue when Franklin passes the aliens.
								IF fCurrentPlaybackTime > 50000.0
									IF NOT bHasTextLabelTriggered[ARM1_ALIEN]
										IF bHasTextLabelTriggered[ARM1_EXTRAS2]
											IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1055.956543,-480.538177,42.981922>>, <<-1169.247681,-545.516846,27.537090>>, 24.000000)
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_ALIEN", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_ALIEN] = TRUE
													bLamarJustInstructedFranklin = FALSE
													bLamarJustTeasedFranklin = FALSE
												ENDIF
											ELSE
												//Player already left the area, so don't play the line.
												bHasTextLabelTriggered[ARM1_ALIEN] = TRUE
												bLamarJustInstructedFranklin = FALSE
												bLamarJustTeasedFranklin = FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Dialogue when Lamar goes over the first jump.
								IF fCurrentPlaybackTime > 87150.0 AND fCurrentPlaybackTime < 88150.0
									IF NOT bHasTextLabelTriggered[ARM1_HILL]
										IF fDistBetweenCars < 2500.0
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_HILL", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[ARM1_HILL] = TRUE
												bLamarJustInstructedFranklin = FALSE
												bLamarJustTeasedFranklin = FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF

								//Dialogue if Franklin falls too far behind: one as an insult and one if Franklin is getting close to failing.
								IF fCurrentPlaybackTime > 20000.0 
								AND fDistBetweenCars > 6400.0
									IF NOT bBuddyFinished
										IF NOT bHasTextLabelTriggered[ARM1_WRONG]
											IF GET_GAME_TIMER() - iLostDialogueTimer > 10000
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_WRONG", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_WRONG] = TRUE
													bLamarJustInstructedFranklin = FALSE
													bLamarJustTeasedFranklin = FALSE
													iNumTimesPlayedLostDialogue++
													iLostDialogueTimer = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									//Do a warning after 100m.
									IF NOT bHasTextLabelTriggered[ARM1_WARN1]
										IF GET_GAME_TIMER() - iWarnDialogueTimer > 10000
										AND GET_GAME_TIMER() - iLostDialogueTimer > 4000
											IF fDistBetweenCars > 10000.0
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_WARN", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_WARN1] = TRUE
													bLamarJustInstructedFranklin = FALSE
													bLamarJustTeasedFranklin = FALSE
													iNumTimesPlayedFailWarnDialogue++
													iWarnDialogueTimer = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									//Do another warning if getting close to failing.
									IF NOT bHasTextLabelTriggered[ARM1_WARN2]
										IF GET_GAME_TIMER() - iWarnDialogueTimer2 > 10000
										AND GET_GAME_TIMER() - iWarnDialogueTimer > 4000
											IF fDistBetweenCars > 22500.0
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_WARN", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_WARN2] = TRUE
													bLamarJustInstructedFranklin = FALSE
													bLamarJustTeasedFranklin = FALSE
													iNumTimesPlayedFailWarnDialogue++
													iWarnDialogueTimer2 = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									//Once the player gets close enough again flag the dialogue so it can be triggered later.
									IF iNumTimesPlayedLostDialogue < 4
										bHasTextLabelTriggered[ARM1_WRONG] = TRUE
									ENDIF
									
									IF iNumTimesPlayedFailWarnDialogue < 5
										bHasTextLabelTriggered[ARM1_WARN1] = TRUE
										bHasTextLabelTriggered[ARM1_WARN2] = TRUE
									ENDIF
								ENDIF
																
								//Dialogue if Franklin gets ahead
								IF fCurrentPlaybackTime > 25000.0 AND fCurrentPlaybackTime < 144600.0
								AND GET_GAME_TIMER() - iInFrontChatTimer > 15000
								AND GET_GAME_TIMER() - iTimeWhenFranklinGotInFront > 0
									//If Franklin just snuck in front, play some shouts.
									IF GET_GAME_TIMER() - iTimeWhenFranklinGotInFront < 1000
									AND fDistBetweenCars < 400.0
										IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_FRONT4", CONV_PRIORITY_MEDIUM)
											bLamarJustInstructedFranklin = FALSE
											bLamarJustTeasedFranklin = FALSE
											iInFrontChatTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
									
									//If Franklin is a long distance in front then play a confused lamar line.
									IF GET_GAME_TIMER() - iTimeWhenFranklinGotInFront > 2000
									AND fDistBetweenCars < 3600.0
									AND fDistBetweenCars > 400.0
										IF NOT bHasTextLabelTriggered[ARM1_FRONT1]
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_FRONT1", CONV_PRIORITY_MEDIUM)
												bLamarJustInstructedFranklin = FALSE
												bLamarJustTeasedFranklin = FALSE
												bHasTextLabelTriggered[ARM1_FRONT1] = TRUE
												iInFrontChatTimer = GET_GAME_TIMER()
											ENDIF
										ELIF NOT bHasTextLabelTriggered[ARM1_FRONT2]
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_FRONT2", CONV_PRIORITY_MEDIUM)
												bLamarJustInstructedFranklin = FALSE
												bLamarJustTeasedFranklin = FALSE
												bHasTextLabelTriggered[ARM1_FRONT2] = TRUE
												iInFrontChatTimer = GET_GAME_TIMER()
											ENDIF
										ELIF NOT bHasTextLabelTriggered[ARM1_FRONT3]
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_FRONT3", CONV_PRIORITY_MEDIUM)
												bLamarJustInstructedFranklin = FALSE
												bLamarJustTeasedFranklin = FALSE
												bHasTextLabelTriggered[ARM1_FRONT3] = TRUE
												iInFrontChatTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Random chat for any quiet sections.
								IF GET_GAME_TIMER() - iRaceChatTimer > 10000
									IF fDistBetweenCars < 2500.0
										//Street race line
										/*IF fCurrentPlaybackTime > 69000.0 AND fCurrentPlaybackTime < 76000.0											
											IF NOT bHasTextLabelTriggered[ARM1_STREET]
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_STREET", CONV_PRIORITY_MEDIUM)
													bHasTextLabelTriggered[ARM1_STREET] = TRUE
													iRaceChatTimer = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF*/
									
										IF fCurrentPlaybackTime > 17000.0 AND fCurrentPlaybackTime < 25000.0
										OR fCurrentPlaybackTime > 32000.0 AND fCurrentPlaybackTime < 39500.0
										OR fCurrentPlaybackTime > 102000.0 AND fCurrentPlaybackTime < 108000.0
											INT iRandomBlock = GET_RANDOM_INT_IN_RANGE(0, 6)
											STRING strLabel
											TRIGGERED_TEXT_LABEL eLabel
											
											IF iRandomBlock = 0
												strLabel = "ARM1_CHAT1"
												eLabel = ARM1_CHAT1
											ELIF iRandomBlock = 1
												strLabel = "ARM1_CHAT2"
												eLabel = ARM1_CHAT2
											ELIF iRandomBlock = 2
												strLabel = "ARM1_CHAT3"
												eLabel = ARM1_CHAT3
											ELIF iRandomBlock = 3
												strLabel = "ARM1_CHAT4"
												eLabel = ARM1_CHAT4
											ELIF iRandomBlock = 4
												strLabel = "ARM1_CHAT6"
												eLabel = ARM1_CHAT6
											ELSE
												strLabel = "ARM1_CHAT7"
												eLabel = ARM1_CHAT7
											ENDIF										
																						
											//Need to guarantee that the tow truck line is played, also if it's the first line to be played it needs to be preceded by a ringtone.
											IF NOT bHasTextLabelTriggered[ARM1_TOW]
											AND NOT bHasTextLabelTriggered[ARM1_TOW2]
												IF NOT bHasTextLabelTriggered[ARM1_RINGTOW]
													IF NOT bHasTextLabelTriggered[ARM1_RINGINDIC]
														strLabel = "ARM1_RING"
														eLabel = ARM1_RING
													ELSE
														//The ring tone was already played in an earlier line, so just progress straight to the towing line.
														bHasTextLabelTriggered[ARM1_RINGTOW] = TRUE
													ENDIF
												ENDIF
												
												IF bHasTextLabelTriggered[ARM1_RINGTOW]
													IF GET_FAILS_COUNT_WITHOUT_PROGRESS_FOR_THIS_MISSION_SCRIPT() = 0 
														strLabel = "ARM1_TOW2"
														eLabel = ARM1_TOW2
													ELSE
														strLabel = "ARM1_TOW2"
														eLabel = ARM1_TOW2
													ENDIF
												ENDIF
											ENDIF
											
											IF NOT bHasTextLabelTriggered[eLabel]
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", strLabel, CONV_PRIORITY_MEDIUM)
													IF ARE_STRINGS_EQUAL(strLabel, "ARM1_RING")
														bHasTextLabelTriggered[ARM1_RINGTOW] = TRUE
														bDialToneTriggered = TRUE
													ELSE
														bLamarJustInstructedFranklin = FALSE
														bLamarJustTeasedFranklin = FALSE
														bHasTextLabelTriggered[eLabel] = TRUE
														iRaceChatTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Peds say random dialogue if you drive near them: the ped's car must be moving, close by, and not going in the same direction as the player.
					//Also trigger the dialogue if the player crashes into them.
					IF fCurrentPlaybackTime > 10000.0
						//There are a few set-pieces that need to have dialogue played every time
						CONST_INT SETPIECE_ID_1		0
						CONST_INT SETPIECE_ID_2		1
						CONST_INT SETPIECE_ID_3		6
						CONST_INT TRAFFIC_ID_1		114
						BOOL bForceDialogue = FALSE
						
						IF vehClosest = SetPieceCarID[SETPIECE_ID_1]
						OR vehClosest = SetPieceCarID[SETPIECE_ID_2]
						OR vehClosest = SetPieceCarID[SETPIECE_ID_3]
						OR vehClosest = TrafficCarID[TRAFFIC_ID_1]
							bForceDialogue = TRUE
						ENDIF
					
						IF GET_GAME_TIMER() - iAmbientDialogueTimer > 3000
						OR (bForceDialogue AND GET_GAME_TIMER() - iAmbientDialogueTimer > 1000)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ENTITY_DEAD(vehClosest) AND vehClosest != sMainCars[iBuddiesCar].veh
									BOOL bCrashed = FALSE
									BOOL bNearMiss = FALSE
								
									IF bForceDialogue
										bNearMiss = TRUE
									ELIF IS_ENTITY_TOUCHING_ENTITY(vehClosest, sMainCars[iPlayersCar].veh)
										bCrashed = TRUE
									ELIF ABSF(GET_ENTITY_SPEED(vehClosest)) > 2.0
										IF ABSF(GET_ENTITY_HEADING(vehClosest) - GET_ENTITY_HEADING(sMainCars[iPlayersCar].veh)) > 60.0 
											bNearMiss = TRUE
										ENDIF
									ENDIF
								
									IF bCrashed OR bNearMiss
										pedCurrentAngryDriver = GET_PED_IN_VEHICLE_SEAT(vehClosest)
										
										IF NOT IS_PED_INJURED(pedCurrentAngryDriver)											
											IF bCrashed
												PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NORMAL)
											ELSE
												PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_NORMAL)
											ENDIF
											
											iAmbientDialogueTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Chase end checks
				IF NOT bBuddyFinished
					//First god text
					IF NOT DOES_BLIP_EXIST(sMainCars[iBuddiesCar].blip)
						REMOVE_ALL_BLIPS()
					
						sMainCars[iBuddiesCar].blip = CREATE_BLIP_FOR_ENTITY(sMainCars[iBuddiesCar].veh, FALSE)
					ENDIF
					
					UPDATE_CHASE_BLIP(sMainCars[iBuddiesCar].blip, sMainCars[iBuddiesCar].veh, SQRT(fCurrentFailDistance), 0.8)
					
					//Play the car owner line as soon as the player activates him.
					IF NOT bHasTextLabelTriggered[ARM1_BABY]
						IF NOT IS_PED_INJURED(pedCarOwner)
							IF bCarOwnerActive
								ADD_PED_FOR_DIALOGUE(sConversationPeds, 7, pedCarOwner, "BABYDICK")
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedCarOwner, "ARM1_CUAA", "BABYDICK", SPEECH_PARAMS_FORCE_NORMAL)
								bHasTextLabelTriggered[ARM1_BABY] = TRUE
							ENDIF
						ELSE
							bHasTextLabelTriggered[ARM1_BABY] = TRUE
						ENDIF
					ENDIF
					
					IF NOT bHasTextLabelTriggered[AR1_CHASE]
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							PRINT_NOW("AR1_CHASE", 6000, 0)
							bHasTextLabelTriggered[AR1_CHASE] = TRUE
						ENDIF
					ENDIF

					//Total chase time = 157500.0 (new one is 155700.0)
					IF (fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 6000.0 AND fDistBetweenCars < 400.0)
					OR (fCurrentPlaybackTime > 135000.0 AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<13.991759,-675.822205,31.484550>>, <<20.466270,-688.156616,34.484550>>, 4.000000))
						bCloseFinish = TRUE
						bPlayFirstPartOfCopsCutscene = TRUE
						eSectionStage = SECTION_STAGE_CLEANUP
					ENDIF
				ELSE
					vNextToBuddy = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sMainCars[iBuddiesCar].veh, <<-6.0, 0.0, 0.0>>)
				
					//Tell player to park next to their buddy
					IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
						REMOVE_ALL_BLIPS()
						blipCurrentDestination = CREATE_BLIP_FOR_COORD(vNextToBuddy, FALSE)
					ENDIF
					
					//Use a second fake blip to get the GPS to go the right way (we don't want it to go around the back of the bank).
					IF NOT DOES_BLIP_EXIST(blipFakeGPS)
						blipFakeGPS = CREATE_BLIP_FOR_COORD(<<-88.1825, -675.9736, 34.2665>>, TRUE)
						SET_BLIP_ALPHA(blipFakeGPS, 0)
					ENDIF
					
					IF NOT bHasTextLabelTriggered[AR1_PARK]
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_NOW("AR1_PARK", DEFAULT_GOD_TEXT_TIME, 0)
						bHasTextLabelTriggered[AR1_PARK] = TRUE
					ENDIF
					
					//If the player parks next to Lamar trigger a version of the next cutscene where the first part is skipped.
					IF VDIST2(GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh), vNextToBuddy) < 25.0
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						bCloseFinish = FALSE
						bPlayFirstPartOfCopsCutscene = FALSE
						eSectionStage = SECTION_STAGE_CLEANUP
					ENDIF
					
					//If the player passes through the correct route we can still trigger the normal version of the cutscene, but with Lamar already parked.
					IF IS_ENTITY_IN_ANGLED_AREA(sMainCars[iPlayersCar].veh, <<25.320662,-664.571045,30.627426>>, <<13.766054,-690.208740,36.027962>>, 12.750000)
						bCloseFinish = FALSE
						bPlayFirstPartOfCopsCutscene = TRUE
						eSectionStage = SECTION_STAGE_CLEANUP
					ENDIF
				ENDIF
				
				//Request cutscene assets in advance: recordings take a longer time than models.
				IF fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 55000.0
					REQUEST_VEHICLE_RECORDING(iCarrecBuddyEscape, strCarrec)
					REQUEST_VEHICLE_RECORDING(iCarrecBuddyArrive, strCarrec)
					REQUEST_VEHICLE_RECORDING(iCarrecPlayerArrive, strCarrec)
					REQUEST_VEHICLE_RECORDING(CARREC_COP_ESCAPE, strCarrec)
					
					IF fCurrentPlaybackTime > GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecMain, strCarrec) - 20000.0
						REQUEST_MODEL(POLICE3)
						REQUEST_MODEL(S_M_Y_COP_01)
						SETUP_REQ_BANK_GATES(TRUE)
						REQUEST_ANIM_DICT("map_objects")
						REQUEST_ANIM_DICT(strChaseEndAnims)
					ENDIF
				ENDIF
			ELSE //Player not in main car.
				//Blip the vehicle and tell the player to get back in.
				IF NOT DOES_BLIP_EXIST(sMainCars[iPlayersCar].blip)
					REMOVE_ALL_BLIPS()
					
					sMainCars[iPlayersCar].blip = CREATE_BLIP_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
				ENDIF
				
				IF IS_THIS_PRINT_BEING_DISPLAYED("AR1_PARK")
					CLEAR_PRINTS()
				ENDIF
				
				IF NOT bHasTextLabelTriggered[CMN_GENGETBCK]
					PRINT_NOW("CMN_GENGETBCK", DEFAULT_GOD_TEXT_TIME, 0)
					bHasTextLabelTriggered[CMN_GENGETBCK] = TRUE
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_CAMHELP")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_HEADHELP")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_HINTHELP")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_BRAKE")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_VEHCAMH")
					CLEAR_HELP()
				ENDIF
								
				CLEAR_ALL_RAGE_HELP()
				
				bLamarJustInstructedFranklin = FALSE
				bLamarJustTeasedFranklin = FALSE
				
				//Fail if the player gets too far away from the car
				IF VDIST2(vPlayerPos, GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh)) > 40000.0
					MISSION_FAILED(FAILED_ABANDONED_CAR)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				//Fail if the player loses Lamar during the chase
				IF fDistBetweenCars > fCurrentFailDistance
					MISSION_FAILED(FAILED_LOST_BUDDY)
				ENDIF
				
				//1483960 - Kill Lamar if the player sets him on fire.
				IF IS_ENTITY_ON_FIRE(sLamar.ped)
					IF GET_GAME_TIMER() - iLamarFireTimer > 1000
						SET_ENTITY_HEALTH(sLamar.ped, 0)
					ENDIF
				ELSE
					iLamarFireTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF			
		ENDIF
		
		
		//Fail if the player damages Lamar's car further at this stage.
		IF NOT IS_ENTITY_DEAD(sMainCars[iBuddiesCar].veh)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
				IF TIMERA() > 10000
					//Freeze buddy's car to stop player knocking it
					FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, TRUE)
				
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sMainCars[iBuddiesCar].veh, PLAYER_PED_ID())
						INT iNumPartsDamaged = GET_VEHICLE_NUM_OF_BROKEN_OFF_PARTS(sMainCars[iBuddiesCar].veh) + GET_VEHICLE_NUM_OF_BROKEN_LOOSEN_PARTS(sMainCars[iBuddiesCar].veh)
					
						IF GET_VEHICLE_PETROL_TANK_HEALTH(sMainCars[iBuddiesCar].veh) < 200.0
						OR GET_VEHICLE_ENGINE_HEALTH(sMainCars[iBuddiesCar].veh) < 200.0
						OR iNumPartsDamaged > 2
							MISSION_FAILED(FAILED_LAMARS_CAR_DAMAGED)
						ENDIF
						
						IF GET_GAME_TIMER() - iLamarDamageDialogueTimer > 0
							PLAY_PED_AMBIENT_SPEECH(sLamar.ped, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE)
							TASK_LOOK_AT_ENTITY(sLamar.ped, PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV)
							iLamarDamageDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sMainCars[iBuddiesCar].veh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ON_FIRE(sMainCars[iBuddiesCar].veh)
				MISSION_FAILED(FAILED_LAMARS_CAR_DAMAGED)
			ENDIF
		ENDIF
		
		IF eMissionStage = STAGE_CHASE
			CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sMainCars[iBuddiesCar].veh, "AR1_CAMHELP")
		ELSE
			CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sMainCars[iBuddiesCar].veh, "AR1_CAMHELP", DEFAULT, DEFAULT, FALSE)
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
		AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			IF GET_GAME_TIMER() - iChaseCamTimer > 5000
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bHasTextLabelTriggered[AR1_CAMHELP2]
					PRINT_HELP("AR1_CAMHELP2")
					bHasTextLabelTriggered[AR1_CAMHELP2] = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_CAMHELP2")
				CLEAR_HELP()
			ENDIF
		
			iChaseCamTimer = GET_GAME_TIMER()
		ENDIF
		
		//Audio Scene: Lamar calls Franklin (don't do this if we skipped to the middle of the chase).
		IF eMissionStage = STAGE_CHASE
			SWITCH iAudioSceneEventDialPhone
				CASE 0 //Start when the dial tone is activated.
					IF bDialToneTriggered
						IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_START")
							STOP_AUDIO_SCENE("ARM_1_DRIVE_START")
						ENDIF
					
						IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_PHONE_LAMAR")
							START_AUDIO_SCENE("ARM_1_DRIVE_PHONE_LAMAR")
						ENDIF
						
						iAudioSceneEventDialPhone++
					ENDIF
				BREAK
				
				CASE 1 //Stop after the relevant block of dialogue finished.
					BOOL bProgressAudioScene
					TEXT_LABEL_23 strCurrentLabel
					strCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
					IF bHasTextLabelTriggered[ARM1_RINGINDIC]
						//Dial tone occurred on the "right up here" line.
						IF bHasTextLabelTriggered[ARM1_INDIC]
							IF NOT ARE_STRINGS_EQUAL("ARM1_INDIC", strCurrentLabel)
								bProgressAudioScene = TRUE
							ENDIF
						ENDIF
					ELSE
						//Dial tone occurred on the "tow truck" line.
						IF bHasTextLabelTriggered[ARM1_TOW]
						OR bHasTextLabelTriggered[ARM1_TOW2]
							IF NOT ARE_STRINGS_EQUAL("ARM1_TOW", strCurrentLabel)
							AND NOT ARE_STRINGS_EQUAL("ARM1_TOW2", strCurrentLabel)
								bProgressAudioScene = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bProgressAudioScene
						IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_PHONE_LAMAR")
							STOP_AUDIO_SCENE("ARM_1_DRIVE_PHONE_LAMAR")
						ENDIF
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_FOLLOW_LAMAR")
							START_AUDIO_SCENE("ARM_1_DRIVE_FOLLOW_LAMAR")
						ENDIF
						
						iAudioSceneEventDialPhone++
					ENDIF
				BREAK
			ENDSWITCH
			
			//Audio scene: Movie studio
			SWITCH iAudioSceneEventMovieStudio
				CASE 0 //Enter studio
					IF fCurrentPlaybackTime > 51500.0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1048.170410,-473.170898,43.599838>>, <<-1062.491577,-486.313873,30.664543>>, 26.250000)
							IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_FOLLOW_LAMAR")
								STOP_AUDIO_SCENE("ARM_1_DRIVE_FOLLOW_LAMAR")
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_THROUGH_STUDIO")
								START_AUDIO_SCENE("ARM_1_DRIVE_THROUGH_STUDIO")
							ENDIF
							
							iAudioSceneEventMovieStudio++
						ENDIF
					ENDIF
				BREAK
				
				CASE 1 //Leave studio (either exit).
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1214.699463,-582.267456,34.075935>>, <<-1209.030273,-578.965210,21.306465>>, 26.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1048.202881,-472.566864,43.385071>>, <<-1046.013672,-466.920593,30.802151>>, 45.750000)
						IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_THROUGH_STUDIO")
							STOP_AUDIO_SCENE("ARM_1_DRIVE_THROUGH_STUDIO")
						ENDIF
						
						iAudioSceneEventMovieStudio++
					ENDIF
				BREAK
				
				CASE 2 //Trigger next audio event after leaving.
					IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_TO_CANALS")
						START_AUDIO_SCENE("ARM_1_DRIVE_TO_CANALS")
					ENDIF
					
					iAudioSceneEventMovieStudio++
				BREAK
			ENDSWITCH
		ENDIF
		
		
		
		//Audio scene: bumpy road.
		SWITCH iAudioSceneEventBumps
			CASE 0 //Arrive at the bumpy road, stop any other audio scenes first.
				IF fCurrentPlaybackTime > 80000.0
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1169.195557,-849.998596,21.094818>>, <<-961.985901,-1197.100952,-1.209268>>, 136.500000)
						IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_TO_CANALS")
							STOP_AUDIO_SCENE("ARM_1_DRIVE_TO_CANALS")
						ENDIF
						
						iAudioSceneEventBumps++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 //Start the bumps audio scene.
				IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_OVER_BRIDGES")
					START_AUDIO_SCENE("ARM_1_DRIVE_OVER_BRIDGES")
				ENDIF
				
				iAudioSceneEventBumps++
			BREAK
		ENDSWITCH
		
		//Audio scene: car park.
		SWITCH iAudioSceneEventCarPark
			CASE 0 //Arrive at the car park, stop any other audio scenes first.
				IF fCurrentPlaybackTime > 120000.0
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-313.167847,-834.345215,40.871441>>, <<-288.544739,-647.531250,27.050526>>, 115.500000)
						IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_OVER_BRIDGES")
							STOP_AUDIO_SCENE("ARM_1_DRIVE_OVER_BRIDGES")
						ENDIF
						
						iAudioSceneEventCarPark++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_THROUGH_GARAGE")
					START_AUDIO_SCENE("ARM_1_DRIVE_THROUGH_GARAGE")
				ENDIF
				
				iAudioSceneEventCarPark++
			BREAK
		ENDSWITCH
		
		//Audio scene: hint cam.
		IF IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_LAMAR_FOCUS_CAM")
				START_AUDIO_SCENE("ARM_1_LAMAR_FOCUS_CAM")
			ENDIF	
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("ARM_1_LAMAR_FOCUS_CAM")
				STOP_AUDIO_SCENE("ARM_1_LAMAR_FOCUS_CAM")
			ENDIF
		ENDIF
		
		//Disable the rage mode during the chase by blocking one of the buttons.
		IF NOT bAllowSpecialAbilityDuringChase		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
		ENDIF
		
		//Pin the car park interior to help streaming.
		IF VDIST2(vPlayerPos, <<-202.4, -638.9, 33.7>>) < 22500.0
			IF interiorCarPark = NULL
				interiorCarPark = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-202.4, -638.9, 33.7>>, "dt1_02_carpark") 
			ELIF NOT IS_INTERIOR_READY(interiorCarPark)
				PIN_INTERIOR_IN_MEMORY(interiorCarPark)
			ENDIF
		ELIF VDIST2(vPlayerPos, <<-202.4, -638.9, 33.7>>) > 40000.0
			IF interiorCarPark != NULL
				UNPIN_INTERIOR(interiorCarPark)
				interiorCarPark = NULL
			ENDIF			
		ENDIF
		
		//Pre-stream the area where the chase ends.
		IF fCurrentPlaybackTime > 100000.0
			IF VDIST2(vPlayerPos, <<34.0, -638.5, 31.6>>) < 22500.0
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START(<<46.6, -624.3, 32.1>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-1.1, 0.0, 137.4>>), 100.0)
				ENDIF
			ELIF VDIST2(vPlayerPos, <<34.0, -638.5, 31.6>>) > 40000.0
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()			
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP
		CLEAR_PRINTS()
		CLEAR_HELP()
		REMOVE_ALL_BLIPS()
		DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
		BLOCK_SCENARIOS_FOR_CHASE(FALSE)
		DISABLE_TAXI_HAILING(FALSE)
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		
		//This cleanup should no longer be needed.
		//#IF IS_DEBUG_BUILD	SCRIPT_ASSERT("PRIVATE_StopChaseHintCam")	#ENDIF
		//PRIVATE_StopChaseHintCam()
		//SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		IF interiorCarPark != NULL
			UNPIN_INTERIOR(interiorCarPark)
			interiorCarPark = NULL
		ENDIF

		UPDATE_RAGE() //In case of j-skip, this should make sure the latest playback values are used
		
		bFranklinsCarTrashed = FALSE
		bLamarsCarTrashed = FALSE
		
		//Check the final state of the cars
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
		AND IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_VEHICLE_ACT_AS_IF_HIGH_SPEED_FOR_FRAG_SMASHING(sMainCars[iBuddiesCar].veh, FALSE)
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(sMainCars[iPlayersCar].veh, FALSE)
			//PRINTLN(iPlayersHealthBeforeRace - GET_ENTITY_HEALTH(sMainCars[iPlayersCar].veh), " ", iLamarsHealthBeforeRace - GET_ENTITY_HEALTH(sMainCars[iBuddiesCar].veh))
		
			IF bCarCollisionIsDisabled
				SET_ENTITY_COLLISION(sMainCars[iBuddiesCar].veh, TRUE)
				bCarCollisionIsDisabled = FALSE
			ENDIF
			
			IF iPlayersHealthBeforeRace - GET_ENTITY_HEALTH(sMainCars[iPlayersCar].veh) > 250
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM1_CAR_DAMAGED, TRUE)
			
				bFranklinsCarTrashed = TRUE
			ENDIF
			
			IF iLamarsHealthBeforeRace - GET_ENTITY_HEALTH(sMainCars[iBuddiesCar].veh) > 250
				bLamarsCarTrashed = TRUE
			ENDIF
			
			//Reset any changes to the player's top speed.
			MODIFY_VEHICLE_TOP_SPEED(sMainCars[iPlayersCar].veh, 0.0)
		ENDIF
		
		STOP_CAM_PLAYBACK(vehCameraTest)
		REMOVE_VEHICLE(vehCameraTest, TRUE)
		
		IF iChasePedBlockingArea != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iChasePedBlockingArea)
			iChasePedBlockingArea = -1
		ENDIF
		
		IF iChasePedBlockingArea2 != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iChasePedBlockingArea2)
			iChasePedBlockingArea2 = -1
		ENDIF

		REMOVE_ANIM_DICT(strAlienAnims)
		REMOVE_ANIM_DICT(strLamarCarAnimsCrash)
		REMOVE_ANIM_DICT(strLamarCarAnimsTaunt)
		REMOVE_ANIM_DICT(strFranklinCarAnimsTaunt)
		
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
		SET_AGGRESSIVE_HORNS(FALSE)
		SETTIMERB(0)
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		SET_PED_POPULATION_BUDGET(3)
		
		SET_FORCED_OBJECTS_FOR_CHASE(FALSE)
		
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_COPS_ARRIVE_CUTSCENE
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP		
		JUMP_TO_STAGE(STAGE_COPS_ARRIVE_CUTSCENE, TRUE)
	ENDIF
ENDPROC

PROC COPS_ARRIVE_CUTSCENE()
	VECTOR vPlayerStartPos = <<39.0428, -638.7694, 30.6267>>
	FLOAT fPlayerStartHeading = 335.0152
	
	IF iBuddiesCar = NINEF_INDEX
		vPlayerStartPos = <<39.3644, -638.0751, 30.6267>>
		fPlayerStartHeading = 335.0097
	ENDIF

	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			IF bUsedACheckpoint
				START_REPLAY_SETUP(vPlayerStartPos, fPlayerStartHeading, FALSE)
				
				iCurrentEvent = 99
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerStartPos)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				LOAD_SCENE(vPlayerStartPos)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				iCurrentEvent = 99
			ENDIF
		ELSE
			SETUP_REQ_CAR_CHOICES()
		
			IF SETUP_LAMAR(vPlayerStartPos + <<0.0, 1.0, 3.0>>)
			AND SETUP_NINEF(vPlayerStartPos)
			AND SETUP_RAPIDGT(vPlayerStartPos + <<0.0, 3.0, 0.0>>)		
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
				
				SETUP_REQ_PUT_PLAYER_INTO_CHOSEN_CAR()
				SETUP_REQ_LAMAR_INTO_CHOSEN_CAR()
				SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
				SETUP_REQ_ARRIVAL_RECORDING_CHOICES()
				SETUP_REQ_ABILITY_BAR_NEARLY_FULL()
				SETUP_REQ_REMOVE_BANK_SHUTTERS()
				
				SPECIAL_ABILITY_RESET(PLAYER_ID())
				
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(sMainCars[iBuddiesCar].veh, "ARM_1_LAMARS_CAR")
				ENDIF
				
				bPlayFirstPartOfCopsCutscene = TRUE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		REQUEST_VEHICLE_RECORDING(iCarrecBuddyEscape, strCarrec)
		REQUEST_VEHICLE_RECORDING(iCarrecBuddyArrive, strCarrec)
		REQUEST_VEHICLE_RECORDING(iCarrecPlayerArrive, strCarrec)
		REQUEST_VEHICLE_RECORDING(CARREC_COP_ESCAPE, strCarrec)
		REQUEST_MODEL(POLICE3)
		REQUEST_MODEL(S_M_Y_COP_01)
		REQUEST_ANIM_DICT("map_objects")
		REQUEST_ANIM_DICT(strChaseEndAnims)
		
		IF SETUP_REQ_BANK_GATES(TRUE)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecBuddyEscape, strCarrec)		
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecBuddyArrive, strCarrec)	
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecPlayerArrive, strCarrec)	
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_COP_ESCAPE, strCarrec)
		AND HAS_MODEL_LOADED(POLICE3)
		AND HAS_MODEL_LOADED(S_M_Y_COP_01)
		AND HAS_ANIM_DICT_LOADED("map_objects")
		AND HAS_ANIM_DICT_LOADED(strChaseEndAnims)
		AND (bPlayFirstPartOfCopsCutscene OR (NOT bPlayFirstPartOfCopsCutscene AND TIMERB() > 1000)) //Timer is set in previous section if you park next to Lamar.
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)		
				FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, FALSE)
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
					STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh)
				ENDIF
				
				IF iBuddiesCar = RAPIDGT_INDEX
					REMOVE_VEHICLE_RECORDING(CARREC_BUDDY_ALTERNATE, strCarrec)
				ELSE
					REMOVE_VEHICLE_RECORDING(CARREC_BUDDY, strCarrec)
				ENDIF
				
				SET_VEHICLE_INDICATOR_LIGHTS(sMainCars[iBuddiesCar].veh, FALSE, FALSE)
				SET_VEHICLE_INDICATOR_LIGHTS(sMainCars[iBuddiesCar].veh, TRUE, FALSE)
			ENDIF

			CLEANUP_UBER_PLAYBACK()
			REMOVE_ALL_CHASE_RECORDINGS()
			REMOVE_VEHICLE(vehTrailer)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
			
			REMOVE_OBJECT(objVaultShutter)
			REMOVE_PED(sSecurityGuard.ped, TRUE)
			
			IF interiorBank != NULL
				UNPIN_INTERIOR(interiorBank)
				interiorBank = NULL
			ENDIF
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				CLEAR_PED_SECONDARY_TASK(sLamar.ped)
				CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
			
				SET_PED_CAN_PLAY_AMBIENT_ANIMS(sLamar.ped, FALSE) 
				SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(sLamar.ped, FALSE) 
			
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sLamar.ped, 20000, SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				TASK_LOOK_AT_ENTITY(sLamar.ped, PLAYER_PED_ID(), 20000, SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			
			SETUP_REQ_BANK_GATES()
			
			PLAY_ENTITY_ANIM(objBankGates[1], "p_sec_gate_01_s_close", "map_objects", 4.0, FALSE, TRUE)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objBankGates[1])
			SET_ENTITY_VISIBLE(objBankGates[1], FALSE)
			SET_ENTITY_COLLISION(objBankGateCollision[0], FALSE)
			SET_ENTITY_COLLISION(objBankGateCollision[1], FALSE)

			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			SET_CREATE_RANDOM_COPS(FALSE)
			CLEAR_AREA(<<44.494202, -634.840820, 34.558254>>, 200.0, TRUE)		

			//Create police car in advance for later
			vehCutsceneCop = CREATE_VEHICLE(POLICE3, <<7.5420, -578.0831, 36.6983>>, 336.2658)
			SET_VEHICLE_ENGINE_ON(vehCutsceneCop, TRUE, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
			
			pedCutsceneCops[0] = CREATE_PED_INSIDE_VEHICLE(vehCutsceneCop, PEDTYPE_COP, S_M_Y_COP_01, VS_DRIVER)
			GIVE_WEAPON_TO_PED(pedCutsceneCops[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
			SET_PED_COMBAT_ATTRIBUTES(pedCutsceneCops[0], CA_DO_DRIVEBYS, FALSE)
			pedCutsceneCops[1] = CREATE_PED_INSIDE_VEHICLE(vehCutsceneCop, PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
			GIVE_WEAPON_TO_PED(pedCutsceneCops[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
			SET_PED_COMBAT_ATTRIBUTES(pedCutsceneCops[1], CA_DO_DRIVEBYS, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)

			SET_FRONTEND_RADIO_ACTIVE(FALSE)

			//If the doors are open close them for the cutscene.
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)		
				SET_VEHICLE_DOORS_SHUT(sMainCars[iPlayersCar].veh)
				
				//Roll down the windows in case the player put the roof back up.
				ROLL_DOWN_WINDOWS(sMainCars[iPlayersCar].veh)
				LOWER_CONVERTIBLE_ROOF(sMainCars[iPlayersCar].veh, TRUE)
				
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
				ENDIF
			ENDIF
			
			STOP_AUDIO_SCENES()
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_CRASHES_01")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("CAR_CRASHES_MED_01")

			//If the player already parked next to Lamar then don't play the first shot.
			IF bPlayFirstPartOfCopsCutscene
				FLOAT fClosestTime = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh), iCarrecPlayerArrive, strCarrec, 20)
				FLOAT fClosestBuddyTime = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh), iCarrecBuddyArrive, strCarrec, 20)

				IF fClosestTime < 1000.0
					fClosestTime = 1000.0
				ENDIF
				
				IF fClosestBuddyTime < 1000.0
					fClosestBuddyTime = 1000.0
				ENDIF
				
				IF ABSF(fClosestBuddyTime - fClosestTime) < 750.0 
					IF fClosestBuddyTime - fClosestTime > 0.0
						fClosestBuddyTime += 500.0
					ELSE
						fClosestTime += 500.0
					ENDIF
				ENDIF

				#IF IS_DEBUG_BUILD
					PRINTLN("Franklin start time: ", fClosestTime, " Lamar start time: ", fClosestBuddyTime)
				#ENDIF
				
				//Play variations depending on if it was a close finish or not
				IF bCloseFinish
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)		
						START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, iCarrecBuddyArrive, strCarrec)
						
						IF IS_SCREEN_FADED_OUT()
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, 2500.0)
						ELSE
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, fClosestBuddyTime)
						ENDIF
						
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iBuddiesCar].veh)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(sMainCars[iBuddiesCar].veh, TRUE)
						PLAY_SOUND_FROM_ENTITY(-1, "Lamar_Throttle_Blip", sMainCars[iBuddiesCar].veh)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)				
						START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, iCarrecPlayerArrive, strCarrec)
						
						IF IS_SCREEN_FADED_OUT()
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 1500.0)
						ELSE
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, fClosestTime)
						ENDIF
						
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iPlayersCar].veh)
						PLAY_SOUND_FROM_ENTITY(-1, "Franklin_Throttle_Blip", sMainCars[iPlayersCar].veh)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)		
						START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, iCarrecBuddyArrive, strCarrec)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecBuddyArrive, strCarrec) - 200.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iBuddiesCar].veh)
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(sMainCars[iBuddiesCar].veh, TRUE)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)				
						START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, iCarrecPlayerArrive, strCarrec)
						
						IF IS_SCREEN_FADED_OUT()
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 1500.0)
						ELSE
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, fClosestTime)
						ENDIF
						
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iPlayersCar].veh)
						PLAY_SOUND_FROM_ENTITY(-1, "Franklin_Throttle_Blip", sMainCars[iPlayersCar].veh)
					ENDIF
					
					//Have Lamar look in his rear-view mirror for a few seconds.
					IF NOT IS_PED_INJURED(sLamar.ped)
						TASK_LOOK_AT_COORD(sLamar.ped, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sMainCars[iBuddiesCar].veh, <<-2.5729, 0.4432, 0.4889>>), 20000, 
																								  SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, 
																							      SLF_LOOKAT_VERY_HIGH) 
																							
					ENDIF
				ENDIF
				
				camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				
				IF bPlayLamarRearViewCam
					SET_CAM_PARAMS(camCutscene, <<43.507690,-636.952881,31.885925>>,<<-5.839731,-0.072286,141.028076>>,28.425701, 0)
					SET_CAM_PARAMS(camCutscene, <<43.448174,-636.904785,31.886021>>,<<-5.839731,-0.072286,141.028076>>,28.425701, 6500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				ELSE
					SET_CAM_PARAMS(camCutscene, <<44.494202, -634.840820, 34.558254>>, <<-9.333186, 0.007438, 141.694916>>, 34.999981, 0)
					SET_CAM_PARAMS(camCutscene, <<45.339893,-634.409973,31.496740>>,<<-1.446978,-0.011956,131.289749>>,34.999981, 4500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
				ENDIF
				
				SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.05)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
				SET_ENTITY_CAN_BE_DAMAGED(sMainCars[iPlayersCar].veh, FALSE)
				SET_ENTITY_INVINCIBLE(sMainCars[iPlayersCar].veh, TRUE)
				SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(sMainCars[iPlayersCar].veh, TRUE)
				
				//If the player is closer to the end point than Lamar then consider that a victory in the race.
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)	
					IF VDIST2(GET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh), <<40.409893,-635.809973,31.096740>>) > VDIST2(GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh), <<40.409893,-635.809973,31.096740>>)
					//Only if a segment wasn't shitskipped
					//AND not g_bShitskipAccepted
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ARM1_WINNER)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("armenian1.sc - Player won the race.")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF

			SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
			START_AUDIO_SCENE("ARM_1_COPS_ARRIVE")

			DO_FADE_IN_WITH_WAIT()
			
			SETTIMERB(0)
			SETTIMERA(0)
			sLamar.iEvent = 0
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			iCameraSyncScene = -1
			bSafeToPlayCamAnim = FALSE
			bHasFirstPersonFlashTriggered = FALSE
			//bAlreadyPlayedCarTrashedDialogue = FALSE
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_RUNNING
		SET_SIDE_CAR_PARK_INTERIOR_ACTIVE(TRUE)
	
		FLOAT f_anim_phase
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iCameraSyncScene)
			f_anim_phase = GET_SYNCHRONIZED_SCENE_PHASE(iCameraSyncScene)
			
			#IF IS_DEBUG_BUILD
				PRINTLN(f_anim_phase)
			#ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[iBuddiesCar].veh)
			SET_FORCE_HD_VEHICLE(sMainCars[iBuddiesCar].veh, TRUE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[iPlayersCar].veh)
			SET_FORCE_HD_VEHICLE(sMainCars[iPlayersCar].veh, TRUE)
		ENDIF

		IF NOT IS_ENTITY_VISIBLE(objBankGates[1])
			IF TIMERB() > 10
				SET_ENTITY_VISIBLE(objBankGates[1], TRUE)
			ENDIF
		ENDIF
		
		SWITCH iCurrentEvent		
			CASE 0	
				//Preload the first part of the conversation: different variations based on the race outcome.
				IF NOT bHasTextLabelTriggered[ARM1_DISS_PRELOADED]
					IF bFranklinsCarTrashed
						IF bLamarsCarTrashed
							IF PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_DISS4", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[ARM1_DISS_PRELOADED] = TRUE
							ENDIF
						ELSE
							IF PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_DISS3", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[ARM1_DISS_PRELOADED] = TRUE
							ENDIF
						ENDIF
					ELSE
						IF bCloseFinish
							IF PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_DISS1", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[ARM1_DISS_PRELOADED] = TRUE
							ENDIF
						ELSE
							IF PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_DISS2", CONV_PRIORITY_MEDIUM)
								bHasTextLabelTriggered[ARM1_DISS_PRELOADED] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				//TEMP: There's an issue with the cars sliding a bit after the recording ends, so just freeze the recordings for now.
				IF bPlayFirstPartOfCopsCutscene
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
							IF GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecBuddyArrive, strCarrec) - GET_TIME_POSITION_IN_RECORDING(sMainCars[iBuddiesCar].veh) < 200.0
								SET_PLAYBACK_SPEED(sMainCars[iBuddiesCar].veh, 0.0)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
							IF GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iCarrecPlayerArrive, strCarrec) - GET_TIME_POSITION_IN_RECORDING(sMainCars[iPlayersCar].veh) < 200.0
								SET_PLAYBACK_SPEED(sMainCars[iPlayersCar].veh, 0.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				REQUEST_MODEL(POLICE3)
				REQUEST_MODEL(S_M_Y_COP_01)
				REQUEST_ANIM_DICT(strChaseEndAnims)
			
				IF (TIMERB() > 4500 OR NOT bPlayFirstPartOfCopsCutscene)
				AND HAS_MODEL_LOADED(POLICE3)
				AND HAS_MODEL_LOADED(S_M_Y_COP_01)
				AND HAS_ANIM_DICT_LOADED(strChaseEndAnims)
				AND bHasTextLabelTriggered[ARM1_DISS_PRELOADED]
					//Freeze the vehicles into their start positions
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
					AND IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					AND NOT IS_PED_INJURED(sLamar.ped)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh)
						ENDIF
						
						REMOVE_VEHICLE_RECORDING(iCarrecBuddyArrive, strCarrec)
						FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh, iCarrecBuddyEscape, strCarrec, FALSE)
						//SET_VEHICLE_ACTIVE_DURING_PLAYBACK(sMainCars[iBuddiesCar].veh, TRUE)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iBuddiesCar].veh, FALSE)
						SET_PLAYBACK_SPEED(sMainCars[iBuddiesCar].veh, 0.0)
						
						PLAY_SOUND_FROM_ENTITY(iLamarRevSound, "Idling_Throttle_Blip_Loop", sMainCars[iBuddiesCar].veh, "ARM_1_SOUNDSET")

						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
							STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh)
						ENDIF
						
						REMOVE_VEHICLE_RECORDING(iCarrecPlayerArrive, strCarrec)
						SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, vPlayerStartPos)
						SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, fPlayerStartHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[iPlayersCar].veh)
						FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, TRUE)
						
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(sLamar.ped)
						TASK_CLEAR_LOOK_AT(sLamar.ped)
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						
						sLamar.iSyncedScene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						iPlayerSyncScene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
						IF iPlayersCar = NINEF_INDEX
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iPlayerSyncScene, sMainCars[NINEF_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[NINEF_INDEX].veh, "seat_dside_f"))
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sLamar.iSyncedScene, sMainCars[RAPIDGT_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[RAPIDGT_INDEX].veh, "seat_dside_f"))
						ELSE
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iPlayerSyncScene, sMainCars[RAPIDGT_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[RAPIDGT_INDEX].veh, "seat_dside_f"))
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sLamar.iSyncedScene, sMainCars[NINEF_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[NINEF_INDEX].veh, "seat_dside_f"))
						ENDIF
						
						//Play different mocaps depending on the result of the race.
						IF bFranklinsCarTrashed
							IF bLamarsCarTrashed
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseEndAnims, "CarRace_Banter_bothtrashed_FRANKLIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseEndAnims, "CarRace_Banter_bothtrashed_LAMAR", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								bSafeToPlayCamAnim = TRUE
							ELSE
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseEndAnims, "CarRace_Banter_thatbucket_FRANKLIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseEndAnims, "CarRace_Banter_thatbucket_LAMAR", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								bSafeToPlayCamAnim = TRUE
							ENDIF
							
							//bAlreadyPlayedCarTrashedDialogue = TRUE
						ELSE
							IF bCloseFinish
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseEndAnims, "CarRace_Banter_FlowThrough_FRANKLIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseEndAnims, "CarRace_Banter_FlowThrough_LAMAR", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								bSafeToPlayCamAnim = TRUE
							ELSE
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseEndAnims, "CarRace_Banter_MoveSome_FRANKLIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseEndAnims, "CarRace_Banter_MoveSome_LAMAR", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								bSafeToPlayCamAnim = TRUE
							ENDIF
						ENDIF
					ENDIF

					DESTROY_ALL_CAMS()
					NEW_LOAD_SCENE_STOP() //In case the area was getting pre-streamed during the chase.
					
					//bSafeToPlayCamAnim = FALSE //TEMP
					//Start streaming in the shutter gates.
					
					IF bSafeToPlayCamAnim
						camCutscene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
						iCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<40.715, -638.31, 30.675>>, <<0.0, 0.0, 66.0>>)
						
						IF bFranklinsCarTrashed
							IF bLamarsCarTrashed
								PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "CarRace_Banter_bothtrashed_cam", strChaseEndAnims)
							ELSE
								PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "CarRace_Banter_thatbucket_cam", strChaseEndAnims)
							ENDIF
						ELSE
							IF bCloseFinish
								PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "carrace_banter_flowthrough_cam", strChaseEndAnims)
							ELSE
								PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "carrace_banter_movesome_cam", strChaseEndAnims)
							ENDIF
						ENDIF
					ELSE
						camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					
						IF iBuddiesCar = RAPIDGT_INDEX
							SET_CAM_PARAMS(camCutscene, <<36.597473,-637.170654,31.809887>>,<<-3.324733,-0.009855,-122.254189>>,21.896662, 0)
							SET_CAM_PARAMS(camCutscene, <<36.595215,-637.169373,31.764223>>,<<-3.324733,-0.009855,-122.254189>>,21.896662, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
						ELSE
							SET_CAM_PARAMS(camCutscene, <<36.913578,-636.766541,31.839243>>,<<-3.589668,-0.002839,-124.835060>>,21.708742, 0)
							SET_CAM_PARAMS(camCutscene, <<36.911545,-636.766296,31.800074>>,<<-3.589668,-0.002839,-124.835060>>,21.708742, 15000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
						ENDIF
					ENDIF
					
					IF NOT bPlayFirstPartOfCopsCutscene
						SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.05)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DISPLAY_RADAR(FALSE)
						DISPLAY_HUD(FALSE)
						SET_WIDESCREEN_BORDERS(TRUE, 0)
					ENDIF
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
		
			CASE 1
				//Play dialogue variations based on how the race went.
				IF NOT bHasTextLabelTriggered[ARM1_DISS_FINISHED]
					BEGIN_PRELOADED_CONVERSATION()
					bHasTextLabelTriggered[ARM1_DISS_FINISHED] = TRUE
				ELSE
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
					OR TIMERB() > 20000 //In case something goes wrong with the conversation preloading.
						IF IS_VEHICLE_DRIVEABLE(vehCutsceneCop)
							IF NOT IS_PED_INJURED(pedCutsceneCops[0])
							AND NOT IS_PED_SITTING_IN_VEHICLE(pedCutsceneCops[0], vehCutsceneCop)
								SET_PED_INTO_VEHICLE(pedCutsceneCops[0], vehCutsceneCop)
							ENDIF
							
							SET_VEHICLE_ENGINE_ON(vehCutsceneCop, TRUE, TRUE)
							SET_VEHICLE_SIREN(vehCutsceneCop, TRUE)	
							SET_VEHICLE_LIGHTS(vehCutsceneCop, FORCE_VEHICLE_LIGHTS_ON)
							SET_SIREN_WITH_NO_DRIVER(vehCutsceneCop, TRUE)	
							
							//SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.1)
							//SET_CAM_PARAMS(camCutscene, <<5.463886,-571.515808,40.060337>>,<<-9.003131,-0.014244,-156.997849>>,20.389755, 0)
							//SET_CAM_PARAMS(camCutscene, <<5.362803,-571.279663,38.419109>>,<<-8.901178,-0.039444,-156.941574>>,20.389755, 7500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
							
							//ADD_PED_FOR_DIALOGUE(sConversationPeds, 7, NULL, "CopDispatch")
							//ADD_PED_FOR_DIALOGUE(sConversationPeds, 8, pedCutsceneCops[0], "COPDRIVER")
							
							SETTIMERB(0)
							iCurrentEvent++
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE 2
				REQUEST_VEHICLE_RECORDING(CARREC_COP_ESCAPE, strCarrec)
			
				IF (TIMERB() > 1000 OR f_anim_phase > 0.99)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_COP_ESCAPE, strCarrec)
					IF IS_VEHICLE_DRIVEABLE(vehCutsceneCop)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCutsceneCop)	
							STOP_PLAYBACK_RECORDED_VEHICLE(vehCutsceneCop)
						ENDIF
					
						REMOVE_VEHICLE_RECORDING(CARREC_COP_ESCAPE_START, strCarrec)
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehCutsceneCop, CARREC_COP_ESCAPE, strCarrec, ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT))
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCutsceneCop, 500.0)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCutsceneCop)
						SET_PLAYBACK_SPEED(vehCutsceneCop, 0.1)
					ENDIF
					
					IF NOT IS_PED_INJURED(sLamar.ped)
						sLamar.iSyncedScene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						iPlayerSyncScene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
						IF iPlayersCar = NINEF_INDEX
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iPlayerSyncScene, sMainCars[NINEF_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[NINEF_INDEX].veh, "seat_dside_f"))
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sLamar.iSyncedScene, sMainCars[RAPIDGT_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[RAPIDGT_INDEX].veh, "seat_dside_f"))
						ELSE
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iPlayerSyncScene, sMainCars[RAPIDGT_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[RAPIDGT_INDEX].veh, "seat_dside_f"))
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sLamar.iSyncedScene, sMainCars[NINEF_INDEX].veh, GET_ENTITY_BONE_INDEX_BY_NAME(sMainCars[NINEF_INDEX].veh, "seat_dside_f"))
						ENDIF
					
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iPlayerSyncScene, strChaseEndAnims, "CarRace_Banter_cops_FRANKLIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_PHASE(iPlayerSyncScene, 0.1)
						TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strChaseEndAnims, "CarRace_Banter_cops_LAMAR", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
					
					IF bSafeToPlayCamAnim
						DESTROY_ALL_CAMS()
						camCutscene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
						iCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<40.715, -638.31, 30.675>>, <<0.0, 0.0, 66.0>>)
						PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "CarRace_Banter_cops_cam", strChaseEndAnims)
					ELSE
						SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.05)
						SET_CAM_PARAMS(camCutscene, <<46.842010,-636.445923,31.563919>>, <<-0.180864,0.105875,110.352585>>, 30.892385, 0)
						SET_CAM_PARAMS(camCutscene, <<46.881973,-636.563721,31.602829>>, <<-0.098123,0.105875,105.779968>>, 30.892385, 16000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					ENDIF
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 3			
				IF NOT bHasTextLabelTriggered[ARM1_COPS_3]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
						CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_COPS", CONV_PRIORITY_MEDIUM)
					ENDIF
				
					TEXT_LABEL strCurrentLabel
					strCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				
					IF ARE_STRINGS_EQUAL(strCurrentLabel, "ARM1_COPS_3")
						SETTIMERB(0)
						bHasTextLabelTriggered[ARM1_COPS_3] = TRUE
					ENDIF
				ENDIF
				
				IF TIMERB() > 3000
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
						SET_PLAYBACK_SPEED(sMainCars[iBuddiesCar].veh, 1.0)
						
						IF NOT HAS_SOUND_FINISHED(iLamarRevSound)
							STOP_SOUND(iLamarRevSound)
						ENDIF						
						
						SETTIMERB(0)
						iCurrentEvent++
					ENDIF
				ENDIF
			BREAK
			
			CASE 4		
				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] COPS_ARRIVE_CUTSCENE: f_anim_phase = ", f_anim_phase, " TIMERB = ", TIMERB())
				
				IF NOT bHasFirstPersonFlashTriggered
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					OR GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_BONNET
						IF TIMERB() > 2200
							CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Triggering flash: f_anim_phase = ", f_anim_phase, " TIMERB = ", TIMERB())
							
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bHasFirstPersonFlashTriggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF TIMERB() > 2500
				OR f_anim_phase > 0.99
					eSectionStage = SECTION_STAGE_CLEANUP
				ENDIF
			BREAK
		ENDSWITCH	
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP
		
		REPLAY_STOP_EVENT()
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0)
		
		CLEAR_HELP()
		
		SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
		SET_SIDE_CAR_PARK_INTERIOR_ACTIVE(FALSE)
	
		SET_ENTITY_COLLISION(objBankGateCollision[0], TRUE)
		SET_ENTITY_COLLISION(objBankGateCollision[1], TRUE)
	
		//Stop any anims on the player, stick him back in the car
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			STOP_SYNCHRONIZED_ENTITY_ANIM(PLAYER_PED_ID(), NORMAL_BLEND_OUT, TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh, VS_DRIVER)
			SET_VEHICLE_BRAKE_LIGHTS(sMainCars[iPlayersCar].veh, FALSE) //Fudge fix for lights turning on when player is warped in
		ENDIF
	
		IF NOT IS_PED_INJURED(sLamar.ped)		
			STOP_SYNCHRONIZED_ENTITY_ANIM(sLamar.ped, NORMAL_BLEND_OUT, TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(sLamar.ped)
			TASK_CLEAR_LOOK_AT(sLamar.ped)
			SET_ENTITY_COORDS(sLamar.ped, <<-32.9120, -1086.3040, 29.2035>>)
			FREEZE_ENTITY_POSITION(sLamar.ped, TRUE)
			SET_ENTITY_COLLISION(sLamar.ped, FALSE)
			SET_ENTITY_VISIBLE(sLamar.ped, FALSE)
		ENDIF
	
		//Cleanup recordings if they haven't been cleaned up already
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh)
			ENDIF
			
			REMOVE_VEHICLE_RECORDING(iCarrecPlayerArrive, strCarrec)
			
			FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, FALSE)
			
			//Fix for TODO 271924: moved player further back at the end of the cutscene so exit is clearer.
			IF iPlayersCar = NINEF_INDEX
				SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 337.9254)
				SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<35.9457, -646.1738, 30.6258>>)
			ELSE
				SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 335.0150)
				SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<35.2234, -646.9493, 30.6292>>)
			ENDIF
			
			//SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[iPlayersCar].veh)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iBuddiesCar].veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iBuddiesCar].veh)
			ENDIF
			
			REMOVE_VEHICLE_RECORDING(iCarrecBuddyArrive, strCarrec)
			REMOVE_VEHICLE_RECORDING(iCarrecBuddyEscape, strCarrec)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sMainCars[iBuddiesCar].veh)
			
			SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
		ENDIF
		
		//If we skipped the cutscene then do a load scene here.
		IF IS_SCREEN_FADED_OUT()
			NEW_LOAD_SCENE_SPHERE_WITH_WAIT(<<35.9457, -646.1738, 30.6258>>, 200.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION, 5000)
		ENDIF
		
		//Setup the cutscene cop to transition into gameplay
		IF IS_VEHICLE_DRIVEABLE(vehCutsceneCop)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCutsceneCop)
				SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCutsceneCop, 10500.0)
				SET_PLAYBACK_SPEED(vehCutsceneCop, 1.0)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehCutsceneCop)
			ENDIF

			IF NOT IS_PED_INJURED(pedCutsceneCops[0]) 
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCutsceneCops[0], FALSE)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedCutsceneCops[1])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCutsceneCops[1], FALSE)
			ENDIF
		ENDIF
		
		CLEAR_AREA_OF_VEHICLES(<<81.9, -600.2, 32.5>>, 30.0)
		
		IF DOES_CAM_EXIST(camCutscene)
		AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON 
		AND GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() != VEHICLE_ZOOM_LEVEL_BONNET
			DESTROY_CAM(camCutscene)
			
			IF iPlayersCar = NINEF_INDEX
				camCutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<34.023193,-647.578369,31.919437>>,<<-1.429148,0.000000,-47.809055>>,50.027916, TRUE)
			ELSE
				camCutscene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<33.332428,-648.706055,31.861574>>,<<-0.377383,0.000000,-47.843166>>,50.027916, TRUE)
			ENDIF
			
			CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Creating camera for scripted transition to gameplay.")
			
			WAIT(0)
		ENDIF

		REMOVE_ANIM_DICT(strChaseEndAnims)
		
		SET_FRONTEND_RADIO_ACTIVE(TRUE)
		
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		STOP_AUDIO_SCENE("ARM_1_COPS_ARRIVE")
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(-50.5 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
		OR GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_BONNET
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ELSE
			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
		ENDIF
	
		NEW_LOAD_SCENE_STOP()
	
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_GO_TO_GARAGE
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			ENDIF
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_ENTITY_DEAD(sMainCars[iBuddiesCar].veh)
					SET_FORCE_HD_VEHICLE(sMainCars[iBuddiesCar].veh, TRUE)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(sMainCars[iPlayersCar].veh)
					SET_FORCE_HD_VEHICLE(sMainCars[iPlayersCar].veh, TRUE)
				ENDIF
			
				WAIT(0)
			ENDWHILE
		ENDIF
		
		CLEAR_PRINTS()
	
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
		IF IS_VEHICLE_DRIVEABLE(vehCutsceneCop)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCutsceneCop)	
				STOP_PLAYBACK_RECORDED_VEHICLE(vehCutsceneCop)
			ENDIF
		
			SET_VEHICLE_SIREN(vehCutsceneCop, TRUE)	
			SET_VEHICLE_LIGHTS(vehCutsceneCop, FORCE_VEHICLE_LIGHTS_ON)
			SET_SIREN_WITH_NO_DRIVER(vehCutsceneCop, TRUE)
			REMOVE_VEHICLE_RECORDING(CARREC_COP_ESCAPE_START, strCarrec)
			START_PLAYBACK_RECORDED_VEHICLE(vehCutsceneCop, CARREC_COP_ESCAPE, strCarrec)
		ENDIF
		
		IF NOT HAS_SOUND_FINISHED(iLamarRevSound)
			STOP_SOUND(iLamarRevSound)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objBankGates[1])
			IF IS_ENTITY_PLAYING_ANIM(objBankGates[1], "map_objects", "p_sec_gate_01_s_close")
				SET_ENTITY_ANIM_CURRENT_TIME(objBankGates[1], "map_objects", "p_sec_gate_01_s_close", 0.99)
			ENDIF	
		ENDIF
	
		eSectionStage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC

PROC GO_TO_GARAGE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF bUsedACheckpoint
			START_REPLAY_SETUP(<<35.2234, -646.9493, 30.6292>>, 335.0142, FALSE)
			
			iCurrentEvent = 99
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<35.2234, -646.9493, 30.6292>>)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			
			LOAD_SCENE(<<35.2234, -646.9493, 30.6292>>)
			WAIT(0)
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			
			iCurrentEvent = 99
		ENDIF
		
		SETUP_REQ_CAR_CHOICES()
		SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS, BUILDINGSTATE_NORMAL)
		
		//Need to create the cutscene cops if jumping straight to this stage
		REQUEST_MODEL(POLICE3)
		REQUEST_MODEL(S_M_Y_COP_01)
		REQUEST_VEHICLE_RECORDING(CARREC_COP_ESCAPE, strCarrec)
		
		WHILE NOT DOES_ENTITY_EXIST(sLamar.ped)
		OR NOT DOES_ENTITY_EXIST(sMainCars[0].veh)
		OR NOT DOES_ENTITY_EXIST(sMainCars[1].veh)
		OR NOT HAS_MODEL_LOADED(POLICE3)
		OR NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_COP_ESCAPE, strCarrec)
			SETUP_LAMAR(<<-25.3560, -1086.3054, 25.5721>>, 52.8017)
			SETUP_NINEF(<<34.0424, -638.7692, 30.6252>>)
			SETUP_RAPIDGT(<<44.0424, -638.7692, 30.6252>>)
			REQUEST_MODEL(POLICE3)
			REQUEST_MODEL(S_M_Y_COP_01)
			REQUEST_VEHICLE_RECORDING(CARREC_COP_ESCAPE, strCarrec)
			
			WAIT(0)
		ENDWHILE
		
		END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
		SETUP_REQ_PUT_PLAYER_INTO_CHOSEN_CAR()
		SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
		SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
		SETUP_REQ_ABILITY_BAR_NEARLY_FULL()
		
		SPECIAL_ABILITY_RESET(PLAYER_ID())
		CLEAR_PED_WETNESS(PLAYER_PED_ID())
		
		SET_CREATE_RANDOM_COPS(FALSE)
		CLEAR_AREA_OF_COPS(<<44.494202, -634.840820, 34.558254>>, 400.0)

		vehCutsceneCop = CREATE_VEHICLE(POLICE3, <<87.5619, -588.9714, 30.5990>>, 160.1988)
		SET_VEHICLE_ENGINE_ON(vehCutsceneCop, TRUE, TRUE)
		SET_VEHICLE_SIREN(vehCutsceneCop, TRUE)	
		START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehCutsceneCop, CARREC_COP_ESCAPE, strCarrec, ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT))
		SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCutsceneCop, 10500.0)
		SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
		
		pedCutsceneCops[0] = CREATE_PED_INSIDE_VEHICLE(vehCutsceneCop, PEDTYPE_COP, S_M_Y_COP_01, VS_DRIVER)
		GIVE_WEAPON_TO_PED(pedCutsceneCops[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
		SET_PED_COMBAT_ATTRIBUTES(pedCutsceneCops[0], CA_DO_DRIVEBYS, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCutsceneCops[0], FALSE)
		pedCutsceneCops[1] = CREATE_PED_INSIDE_VEHICLE(vehCutsceneCop, PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
		GIVE_WEAPON_TO_PED(pedCutsceneCops[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
		SET_PED_COMBAT_ATTRIBUTES(pedCutsceneCops[1], CA_DO_DRIVEBYS, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCutsceneCops[1], FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)

		IF NOT IS_PED_INJURED(sLamar.ped)
			SET_ENTITY_COORDS(sLamar.ped, <<-32.9120, -1086.3040, 29.2035>>)
			FREEZE_ENTITY_POSITION(sLamar.ped, TRUE)
			SET_ENTITY_COLLISION(sLamar.ped, FALSE)
			SET_ENTITY_VISIBLE(sLamar.ped, FALSE)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, TRUE, TRUE)
		
			IF iPlayersCar = NINEF_INDEX
				SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 337.9254)
				SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<35.9457, -646.1738, 30.6258>>)
			ELSE
				SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 335.0150)
				SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<35.2234, -646.9493, 30.6292>>)
			ENDIF
		ENDIF
		
		SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS, BUILDINGSTATE_NORMAL)

		SET_GAMEPLAY_CAM_RELATIVE_HEADING(-50.5 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		
		WAIT(500) //This hides some vehicle sliding issues when restarting this section.
		
		eSectionStage = SECTION_STAGE_SETUP
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP	
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_MAX_WANTED_LEVEL(2)
		UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE) //May have been disabled earlier in the mission.
		SET_CREATE_RANDOM_COPS(FALSE)
		CLEAR_AREA_OF_COPS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.0)
		DISABLE_TAXI_HAILING(TRUE)
		//SET_IGNORE_NO_GPS_FLAG(TRUE) //Enables the GPS at the back of the showroom.
		
		bBypassedShowroomCutLocates = FALSE
		bTriggeredShowroomCutFromLeft = FALSE
		bLostWantedLevelForFirstTime = FALSE
		
		//SET_DISPATCH_IDEAL_SPAWN_DISTANCE(1.0)
		//SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0.15)
		//SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS(DT_POLICE_AUTOMOBILE, 0.01)
		
		//If the player shit-skipped the first part of the chase then we'll need to display the rage help here.
		IF bSkippedSpecialAbilityTutorial
			sRageData.bHasRaged = FALSE
		ENDIF
		
		//if the player hasn't used their special ability yet then we'll print the help text again in this section.
		IF NOT sRageData.bHasRaged
			bHasTextLabelTriggered[AR1_RAGEBAR] = FALSE
		ENDIF
		
		//951590 - There's a scenario at the back of the showroom that's possibly causing issues.
		BLOCK_SCENARIOS_AT_SHOWROOM()
		BLOCK_VEHICLE_GENS_IN_SHOWROOM(TRUE)
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
		BLOCK_COPS_SPAWNING_IN_BANK(TRUE)
		
		//Lock Franklin's house.
		SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_B, DOORSTATE_LOCKED)
		SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_LOCKED)
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_LOSE_COPS")
			START_AUDIO_SCENE("ARM_1_LOSE_COPS")
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0)
		ENDIF
		
		DO_FADE_IN_WITH_WAIT()
		
		//Should now be safe to enable damage on the player's car again.
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_ENTITY_CAN_BE_DAMAGED(sMainCars[iPlayersCar].veh, TRUE)
			SET_ENTITY_INVINCIBLE(sMainCars[iPlayersCar].veh, FALSE)
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(sMainCars[iPlayersCar].veh, FALSE)
		ENDIF
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(ARM1_LOSE_WANTED_LVL)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 15.0)
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_LOSE_COPS, "LOSE_COPS")
		SETTIMERA(0)
		bPlayedPoliceReport = FALSE
		iCopsAITimer = 0
		iLoseCopsTimer = GET_GAME_TIMER()
		iAudioSceneEventLoseCops = 0
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_RUNNING
		//1958200 - The cops are able to drive into the bank, so make sure the collision remains there.
		SETUP_REQ_BANK_GATES(FALSE, TRUE)
	
		IF GET_GAME_TIMER() - iLoseCopsTimer < 7000
			UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
			REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
		ENDIF
	
		//Remove the cop car after it finishes the recording.
		IF IS_VEHICLE_DRIVEABLE(vehCutsceneCop)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCutsceneCop)		
				IF iCopsAITimer = 0
					IF NOT IS_PED_INJURED(pedCutsceneCops[0])
						TASK_ARREST_PED(pedCutsceneCops[0], PLAYER_PED_ID())
					ENDIF
					
					IF NOT IS_PED_INJURED(pedCutsceneCops[1])
						TASK_ARREST_PED(pedCutsceneCops[1], PLAYER_PED_ID())
					ENDIF
					
					iCopsAITimer = GET_GAME_TIMER()
				ELIF GET_GAME_TIMER() - iCopsAITimer > 2000
					REMOVE_PED(pedCutsceneCops[0], FALSE)
					REMOVE_PED(pedCutsceneCops[1], FALSE)
					REMOVE_VEHICLE(vehCutsceneCop, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH iCurrentEvent
			CASE 0
				IF GET_GAME_TIMER() - iLoseCopsTimer > 120000
				OR NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SET_CREATE_RANDOM_COPS(TRUE)
					
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
	
		#IF IS_DEBUG_BUILD 
			DONT_DO_J_SKIP(sLocatesData) 
		#ENDIF
		
		IF NOT bHasTextLabelTriggered[ARM1_LOSE]
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_ARMENIAN_1_02")

				bHasTextLabelTriggered[ARM1_LOSE] = TRUE
			ENDIF
		ENDIF
		
		IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vOutsideGarage, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, 
											TRUE, sMainCars[iPlayersCar].veh, "AR1_GOGARAGE", "", "CMN_GENGETBCK", TRUE)
			eSectionStage = SECTION_STAGE_CLEANUP
		ENDIF
		
		IF NOT bPlayedPoliceReport
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_ARMENIAN_1_01")
				
				bPlayedPoliceReport = TRUE
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)			
			//Request next assets in advance
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOutsideGarage) < 10000.0
				REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_NINEF_1, strCarrec)
				REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_NINEF_2, strCarrec)
				REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_RAPIDGT_1, strCarrec)
				REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_RAPIDGT_2, strCarrec)
				
				PED_VEH_DATA_STRUCT sVehData
        		GET_PLAYER_VEH_DATA(CHAR_FRANKLIN, sVehData)
				REQUEST_MODEL(sVehData.model)
			ENDIF
			
			BOOL bEnteredLeftLocate = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-58.523495,-1072.446289,25.512421>>, <<-47.367687,-1076.728027,28.776825>>, 14.750000)
			BOOL bEnteredRightLocate = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-11.135968,-1091.418945,25.422077>>, <<-24.348146,-1126.346069,29.784176>>, 13.000000)
			
			IF bEnteredLeftLocate OR bEnteredRightLocate
				bTriggeredShowroomCutFromLeft = bEnteredLeftLocate
				eSectionStage = SECTION_STAGE_CLEANUP
			ENDIF
			
			//Locates inside garage: in case the player bypasses the normal locates.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-26.301994,-1092.126343,25.433983>>, <<-34.196312,-1089.121704,28.434271>>, 3.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-32.192822,-1086.231201,25.434008>>, <<-35.282406,-1094.512451,28.434353>>, 1.500000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-26.855288,-1088.089111,25.433941>>, <<-28.949333,-1094.194946,28.434189>>, 1.500000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-21.093651,-1085.448242,25.606892>>, <<-32.265572,-1081.161133,28.401917>>, 7.750000)
				bBypassedShowroomCutLocates = TRUE
				eSectionStage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		//Hide Lamar during this bit
		IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			REMOVE_PED(sLamar.ped, TRUE)
		ENDIF
		/*IF NOT IS_PED_INJURED(sLamar.ped)
			IF IS_ENTITY_VISIBLE(sLamar.ped)
				SET_ENTITY_COORDS(sLamar.ped, <<-40.7011, -1089.3977, 25.4344>>)
				SET_ENTITY_VISIBLE(sLamar.ped, FALSE)
				SET_ENTITY_INVINCIBLE(sLamar.ped, TRUE)
				SET_ENTITY_COLLISION(sLamar.ped, FALSE)
				FREEZE_ENTITY_POSITION(sLamar.ped, TRUE)
			ENDIF
		ENDIF*/

		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			//Cops help
			IF (IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)) //Only do help if player is in the car
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF TIMERA() > 3000
						IF NOT bHasTextLabelTriggered[AR1_COPHELP1]
							PRINT_HELP("AR1_COPHELP1", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_COPHELP1] = TRUE
							FLASH_WANTED_DISPLAY(TRUE)
						ELIF NOT bHasTextLabelTriggered[AR1_COPHELP2]
							FLASH_WANTED_DISPLAY(FALSE)
							PRINT_HELP("AR1_COPHELP2", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_COPHELP2] = TRUE
						ELIF NOT bHasTextLabelTriggered[AR1_COPHELP2B]
							#IF IS_DEBUG_BUILD
								PRINTLN("Armenian1.sc - COPSHELP2B printed.") //Getting issues with this help text not printing (1033426).
							#ENDIF
						
							PRINT_HELP("AR1_COPHELP2B", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_COPHELP2B] = TRUE
						ELIF NOT bHasTextLabelTriggered[AR1_COPHELP3]
							PRINT_HELP("AR1_COPHELP3", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_COPHELP3] = TRUE
						ELIF NOT bHasTextLabelTriggered[AR1_DUCKHELP]
							PRINT_HELP("AR1_DUCKHELP", DEFAULT_HELP_TEXT_TIME + 3000)
							bHasTextLabelTriggered[AR1_DUCKHELP] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Play some ambient speech while losing the cops.
				IF NOT bHasTextLabelTriggered[AR1_COPAMB]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_DO_ALL_CHECKS)
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_CURSE_MED", SPEECH_PARAMS_FORCE_FRONTEND)
						bHasTextLabelTriggered[AR1_COPAMB] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Fail if the player drives to the back of the showroom.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-21.093651,-1085.448242,25.606892>>, <<-32.265572,-1081.161133,28.401917>>, 7.750000)
				MISSION_FAILED(FAILED_LED_COPS_TO_SHOWROOM)
			ENDIF
		ELSE		
			/*IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_COPHELP1")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_COPHELP2")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_COPHELP2B")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_COPHELP3")
				CLEAR_HELP()
			ENDIF*/
			
			//Display the duck help if it wasn't displayed when losing the wanted level.
			IF NOT bHasTextLabelTriggered[AR1_DUCKHELP]
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("AR1_DUCKHELP", DEFAULT_HELP_TEXT_TIME + 3000)
				bHasTextLabelTriggered[AR1_DUCKHELP] = TRUE
			ENDIF
			
			IF NOT bLostWantedLevelForFirstTime
				FLASH_WANTED_DISPLAY(FALSE)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				bLostWantedLevelForFirstTime = TRUE
			ENDIF
		ENDIF
		
		//Clear the duck help if the player presses the vehicle duck button.
		IF bHasTextLabelTriggered[AR1_DUCKHELP]
		AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_DUCKHELP")
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_DUCK)
				IF GET_GAME_TIMER() - iDuckHelpTimer > 500
					CLEAR_HELP()
				ENDIF
			ELSE
				iDuckHelpTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		//Rage help: triggers if the player didn't activate ability during the chase (after the cops help).
		IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
			IF NOT sRageData.bHasRaged
				IF bHasTextLabelTriggered[AR1_DUCKHELP]
				OR NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF NOT bHasTextLabelTriggered[AR1_RAGEBAR] //Help text on how to start the special ability.
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							
							// Display keyboard and mouse help if PC version and m&kb 
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								PRINT_HELP("AR1_RAGEBAR_KM", DEFAULT_HELP_TEXT_TIME + 3000)
							ELSE
								PRINT_HELP("AR1_RAGEBAR", DEFAULT_HELP_TEXT_TIME + 3000)
							ENDIF
							
							bHasTextLabelTriggered[AR1_RAGEBAR] = TRUE
							FLASH_ABILITY_BAR(DEFAULT_HELP_TEXT_TIME)
						ENDIF	
					ENDIF
				ENDIF
				
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					CLEAR_ALL_RAGE_HELP()
					
					sRageData.bHasRaged = TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_RAGEBAR")
				CLEAR_ALL_RAGE_HELP()
			ENDIF
			
			// Clear PC specific help
			IF IS_PC_VERSION()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_RAGEBAR_KM")
					CLEAR_ALL_RAGE_HELP()
				ENDIF
			ENDIF
			
		ENDIF
		
		//Create the showroom cars in script
		IF interiorShowroom = NULL
			interiorShowroom = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-38.62, -1099.01, 27.31>>, "v_carshowroom")
		ENDIF
		
		BOOL b_far_enough_to_delete = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-38.62, -1099.01, 27.31>>) > 62500.0
		BOOL b_close_enough_to_create = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-38.62, -1099.01, 27.31>>) < 40000.0
		
		IF b_close_enough_to_create
			//Create Franklin's car in advance.
			SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)
			REQUEST_ANIM_DICT(strShowroomCamAnims)
		
			IF IS_INTERIOR_READY(interiorShowroom)
				IF NOT DO_SHOWROOM_CARS_EXIST()
					SETUP_SHOWROOM_CARS()
				ENDIF
			ELSE
				PIN_INTERIOR_IN_MEMORY(interiorShowroom)
			ENDIF
		ELIF b_far_enough_to_delete
			SETUP_SHOWROOM_CARS(TRUE)
			
			REMOVE_VEHICLE(vehShowroomCars[0], TRUE)
			REMOVE_VEHICLE(vehShowroomCars[1], TRUE)
			REMOVE_VEHICLE(vehShowroomCars[2], TRUE)
			REMOVE_VEHICLE(vehShowroomCars[3], TRUE)
			
			IF DOES_ENTITY_EXIST(vehFranklinsCar)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehFranklinsCar)) > 40000.0
					REMOVE_VEHICLE(vehFranklinsCar, TRUE)
				ENDIF
			ENDIF
			
			REMOVE_ANIM_DICT(strShowroomCamAnims)
			
			IF IS_INTERIOR_READY(interiorShowroom)
				UNPIN_INTERIOR(interiorShowroom)
			ENDIF
		ENDIF
		
		//Fail if the player abandons the car
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh)) > 40000.0
				MISSION_FAILED(FAILED_ABANDONED_CAR)
			ENDIF
		ENDIF
		
		//Do a streaming volume when getting close to the showroom.
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-26.27152, -1083.75146, 25.43581>>) < 40000.0
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_START_SPHERE(<<-46.174908,-1105.760620,26.165388>>, 35.0)
				//NEW_LOAD_SCENE_START(<<-77.4, -1128.3, 27.1>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<0.3, 0.0, -51.5>>), 90.0)
			ENDIF
		ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-26.27152, -1083.75146, 25.43581>>) > 62500.0
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
		ENDIF
		
		SWITCH iAudioSceneEventLoseCops
			CASE 0
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF IS_AUDIO_SCENE_ACTIVE("ARM_1_LOSE_COPS")
						STOP_AUDIO_SCENE("ARM_1_LOSE_COPS")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_TO_DEALERSHIP")
						START_AUDIO_SCENE("ARM_1_DRIVE_TO_DEALERSHIP")
					ENDIF
					
					iAudioSceneEventLoseCops++
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)

		//Replace the bank gates with the map version.
		INT i = 0
		REPEAT COUNT_OF(objBankGates) i
			REMOVE_OBJECT(objBankGates[i], FALSE)
		ENDREPEAT
		
		REPEAT COUNT_OF(objBankGateCollision) i
			REMOVE_OBJECT(objBankGateCollision[i], FALSE)
		ENDREPEAT
		
		REMOVE_ANIM_DICT("map_objects")
		DISABLE_TAXI_HAILING(FALSE)
		
		SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS, BUILDINGSTATE_NORMAL)
		
		FLASH_WANTED_DISPLAY(FALSE)
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
		KILL_FACE_TO_FACE_CONVERSATION()
	
		SETTIMERA(0) //This is to delay the cutscene start
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_SHOWROOM_INTRO_CUTSCENE
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP
		JUMP_TO_STAGE(STAGE_SHOWROOM_INTRO_CUTSCENE, TRUE)
	ENDIF
ENDPROC

PROC SHOWROOM_INTRO_CUTSCENE()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	OVERRIDE_LODSCALE_THIS_FRAME(1.0)

	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			IF bUsedACheckpoint
				START_REPLAY_SETUP(vFranklinsCarStartPos, 335.0142, FALSE)
				
				iCurrentEvent = 99
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinsCarStartPos)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				LOAD_SCENE(vFranklinsCarStartPos)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				iCurrentEvent = 99
			ENDIF
		ELSE
			IF interiorShowroom = NULL
				interiorShowroom = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-38.62, -1099.01, 27.31>>, "v_carshowroom")
			ENDIF
		
			PIN_INTERIOR_IN_MEMORY(interiorShowroom)
		
			SETUP_REQ_CAR_CHOICES()
		
			IF SETUP_LAMAR(<<-37.5420, -1084.8704, 25.4344>>, 246.4778)
			AND SETUP_NINEF(<<34.0424, -638.7692, 30.6252>>)
			AND SETUP_RAPIDGT(<<44.0424, -638.7692, 30.6252>>)
			AND SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)
			AND IS_INTERIOR_READY(interiorShowroom)
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
				
				SETUP_REQ_PUT_PLAYER_INTO_CHOSEN_CAR()
				SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
				SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
				
				BLOCK_SCENARIOS_AT_SHOWROOM()
				BLOCK_VEHICLE_GENS_IN_SHOWROOM(TRUE)
				
				bSafeToDoSeamlessCarTasks = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	
		REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_NINEF_1, strCarrec)
		REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_NINEF_2, strCarrec)
		REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_RAPIDGT_1, strCarrec)
		REQUEST_VEHICLE_RECORDING(CARREC_SHOWROOM_RAPIDGT_2, strCarrec)
		REQUEST_ANIM_DICT(strShowroomCamAnims)
	
		IF SETUP_SHOWROOM_CARS(TRUE)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_SHOWROOM_NINEF_1, strCarrec)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_SHOWROOM_NINEF_2, strCarrec)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_SHOWROOM_RAPIDGT_1, strCarrec)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_SHOWROOM_RAPIDGT_2, strCarrec)
		AND (HAS_ANIM_DICT_LOADED(strShowroomCamAnims) OR NOT bForceAnimatedCamsOn)
			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_ALL_BLIPS()
			CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			STOP_REPLAY_RECORDING()	//B* 2546603
			
			IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
				SET_ENTITY_COORDS(vehFranklinsCar, vFranklinsCarStartPos)
				SET_ENTITY_HEADING(vehFranklinsCar, fFranklinsCarStartHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehFranklinsCar)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
				SET_VEHICLE_ENGINE_ON(sMainCars[iBuddiesCar].veh, FALSE, FALSE)
				SET_VEHICLE_DOORS_LOCKED(sMainCars[iBuddiesCar].veh, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			ENDIF
			
			//Remake the showroom cars, in case they were moved before the cutscene triggered.
			REMOVE_VEHICLE(vehShowroomCars[0], TRUE)
			REMOVE_VEHICLE(vehShowroomCars[1], TRUE)
			REMOVE_VEHICLE(vehShowroomCars[2], TRUE)
			REMOVE_VEHICLE(vehShowroomCars[3], TRUE)
			SET_NUMBER_OF_PARKED_VEHICLES(0)
			CLEAR_AREA(<<-42.8436, -1111.7252, 25.4355>>, 100.0, TRUE) //removes anything in the showroom
			SETUP_SHOWROOM_CARS()
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-66.2173, -1121.9542, 14.8642>>, <<-41.2857, -1111.1638, 34.8642>>, FALSE)

			IF IS_AUDIO_SCENE_ACTIVE("ARM_1_DRIVE_TO_DEALERSHIP")
				STOP_AUDIO_SCENE("ARM_1_DRIVE_TO_DEALERSHIP")
			ENDIF
			
			INT i = 0
			REPEAT COUNT_OF(vehShowroomCars) i
				IF i = 0
					SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[0], <<-49.9, -1094.7, 26.0416>>)
				ELIF i = 1
					SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[1], <<-46.5, -1097.5, 26.35>>)
				ELIF i = 2
					SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[2], <<-41.7, -1099.5, 26.0304>>)
				ELIF i = 3
					SET_ENTITY_COORDS_NO_OFFSET(vehShowroomCars[3], <<-36.8, -1101.2, 26.3321>>)
				ENDIF
			
				FREEZE_ENTITY_POSITION(vehShowroomCars[i], TRUE)
			ENDREPEAT
			
			//Bring Lamar back from hiding
			IF NOT IS_PED_INJURED(sLamar.ped)
				SET_ENTITY_VISIBLE(sLamar.ped, TRUE)
				SET_ENTITY_INVINCIBLE(sLamar.ped, FALSE)
				SET_ENTITY_COLLISION(sLamar.ped, TRUE)
				FREEZE_ENTITY_POSITION(sLamar.ped, FALSE)
			ENDIF
			
			CLEAR_AREA(GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh), 10.0, TRUE) //Removes any cars blocking Franklin's car
			CLEAR_AREA_OF_COPS(GET_ENTITY_COORDS(sMainCars[iPlayersCar].veh), 200.0)
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
				SET_VEHICLE_DOORS_SHUT(sMainCars[iPlayersCar].veh)
				
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
				ENDIF
				
				IF GET_ENTITY_SPEED(sMainCars[iPlayersCar].veh) < 30.0
					VECTOR vCarRot = GET_ENTITY_ROTATION(sMainCars[iPlayersCar].veh)
					
					IF ABSF(vCarRot.y) < 30.0
					AND ABSF(vCarRot.x) < 45.0
						bSafeToDoSeamlessCarTasks = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bSafeToDoSeamlessCarTasks
				IF NOT bBypassedShowroomCutLocates
					SET_ROADS_IN_AREA(<<-89.0913, -1133.1434, 0.8860>>, <<13.5596, -1053.2734, 53.8860>>, TRUE)
				
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
						TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh, <<-27.3, -1082.0, 26.2>>, 15.0, DRIVINGSTYLE_NORMAL, 
													GET_ENTITY_MODEL(sMainCars[iPlayersCar].veh), DRIVINGMODE_PLOUGHTHROUGH | DF_PreferNavmeshRoute, 2.0, 5.0)
					ENDIF
				ENDIF
			ELSE
				//Start recording of player's car (use different recordings depending on car and direction of entry).
				//NOTE: only play the recording if the player didn't find a way to bypass the locates.
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					IF NOT bBypassedShowroomCutLocates
						IF iPlayersCar = NINEF_INDEX
							IF bTriggeredShowroomCutFromLeft
								START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, CARREC_SHOWROOM_NINEF_1, strCarrec)
								SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 4535.0)
							ELSE
								START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, CARREC_SHOWROOM_NINEF_2, strCarrec)
								SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 2200.0)
							ENDIF
						ELSE
							IF bTriggeredShowroomCutFromLeft
								START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, CARREC_SHOWROOM_RAPIDGT_1, strCarrec)
								SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 4435.0)
							ELSE
								START_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, CARREC_SHOWROOM_RAPIDGT_2, strCarrec)
								SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh, 3000.0)
							ENDIF
						ENDIF
						
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sMainCars[iPlayersCar].veh)
					ENDIF
				ENDIF
			ENDIF
			
			DESTROY_ALL_CAMS()
			
			IF bForceAnimatedCamsOn
				camCutscene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				
				IF bTriggeredShowroomCutFromLeft
					iCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-58.096, -1099.135, 25.565>>, <<0.000, 0.000, -20.00>>)
					PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "car_dealership_int_ltr_cam", strShowroomCamAnims)
					SET_SYNCHRONIZED_SCENE_PHASE(iCameraSyncScene, 0.0)
				ELSE
					iCameraSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-58.096, -1101.335, 25.565>>, <<0.000, 0.000, -20.000>>)
					PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iCameraSyncScene, "car_dealership_int_rtl_cam", strShowroomCamAnims)
					SET_SYNCHRONIZED_SCENE_PHASE(iCameraSyncScene, 0.0)
				ENDIF
			ELSE
				camCutscene = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
				SET_CAM_SPLINE_SMOOTHING_STYLE(camCutscene, CAM_SPLINE_SLOW_OUT_SMOOTH)
				IF bBypassedShowroomCutLocates
					ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-77.549858,-1116.585327,27.788509>>,<<0.939515,-0.028141,-58.806252>>,35.070702, TRUE), 0)
				ELSE
					IF bTriggeredShowroomCutFromLeft
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-77.712585,-1116.481323,27.742264>>,<<0.890156,-0.013359,-38.183346>>,35.070702, TRUE), 0)
					ELSE
						ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-77.880249,-1116.143921,27.742174>>,<<0.890156,-0.013359,-94.544067>>,35.070702, TRUE), 0)
					ENDIF
				ENDIF
				ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-77.582253,-1116.546021,27.783735>>,<<0.890799,-0.014147,-60.930740>>,35.070702, TRUE), 
												 5000, CAM_SPLINE_NODE_SMOOTH_ROT)
				ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-77.099556,-1117.414307,27.783968>>,<<0.890799,-0.014147,-60.930740>>,35.070702, TRUE), 
												 6700, CAM_SPLINE_NODE_SMOOTH_ROT)
				//ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-37.8949, -1109.4103, 26.8269>>, <<-1.8127, 0.0000, -60.1200>>, 28.4800, TRUE), 7800, CAM_SPLINE_NODE_SMOOTH_ROT)
				//ADD_CAM_SPLINE_NODE_USING_CAMERA(camCutscene, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-42.785152,-1098.559692,27.029728>>, <<-9.731533,-0.000122,103.760231>>, 36.192162, TRUE), 4400, CAM_SPLINE_NODE_SMOOTH_ROT)
			ENDIF
			
			SET_CAM_ACTIVE(camCutscene, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			//SET_FOCUS_POS_AND_VEL(<<-77.4, -1128.3, 27.1>>, <<0.0, 0.0, 0.0>>)

			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			
			TRIGGER_MUSIC_EVENT("ARM1_RADIO_OFF")
			
			IF bBypassedShowroomCutLocates
				SETTIMERB(1500)
			ELSE
				SETTIMERB(0)
			ENDIF
			
			//Record just arriving at the dealership, the whole scripted scene and a little after that.
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
			
			DO_FADE_IN_WITH_WAIT()
			
			bWarpedCarEarlyForShowroomScene = FALSE
			bCutsceneSkipped = FALSE
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		SET_CONTROL_SHAKE(PLAYER_CONTROL, 0, 0)
		SETUP_LAMAR(<<-37.5420, -1084.8704, 25.4344>>, 246.4778)
	
		SWITCH iCurrentEvent
			CASE 0
				IF TIMERB() > 4500
				AND NOT bWarpedCarEarlyForShowroomScene
					SET_ENTITY_COLLISION(sMainCars[iPlayersCar].veh, FALSE)
					
					//1927392 - Stop the player from falling through the map.
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
						FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, TRUE)
					ENDIF
				ENDIF
			
				REQUEST_ANIM_DICT(strShowroomLeadInOutAnims)
				REQUEST_ANIM_DICT(strLamarShowroomAnims)
			
				VECTOR vJimmyPos, vSimeonPos
				
				IF bTriggeredShowroomCutFromLeft
					vJimmyPos = vJimmyStartPos
					vSimeonPos = vSimeonStartPos
				ELSE
					vJimmyPos = <<-51.6, -1091.9, 25.9>>
					vSimeonPos = <<-51.0, -1090.1, 25.7>>
				ENDIF
			
				IF SETUP_JIMMY(vJimmyPos, fJimmyStartHeading)
				AND SETUP_SIMEON(vSimeonPos, fSimeonStartHeading)
					ADD_PED_FOR_DIALOGUE(sConversationPeds, 6, sJimmy.ped, "JIMMY")
					ADD_PED_FOR_DIALOGUE(sConversationPeds, 4, sSimeon.ped, "SIMEON")
				
					PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_MCS1LI", CONV_PRIORITY_MEDIUM)
				
					IF HAS_ANIM_DICT_LOADED(strShowroomLeadInOutAnims)
					AND TIMERB() > 8500
						eSectionStage = SECTION_STAGE_CLEANUP
					ENDIF
				ENDIF
				
				//Help text explaining showroom as a location to get missions.
				IF NOT bHasTextLabelTriggered[AR1_SHOWROOM]
					IF TIMERB() > 2500
						PRINT_HELP("AR1_SHOWROOM", DEFAULT_HELP_TEXT_TIME + 3000)
						bHasTextLabelTriggered[AR1_SHOWROOM] = TRUE
					ENDIF
				ENDIF
				
				//2006896 - If the player is using different view modes for walking and driving then we need to get them out of the car early to prevent a pop.
				IF NOT bForcedFirstPersonView
					IF TIMERB() > 8300
						CAM_VIEW_MODE eOnFootViewMode, eInCarViewMode
						eOnFootViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
						eInCarViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE)
					
						IF (eOnFootViewMode = CAM_VIEW_MODE_FIRST_PERSON AND eInCarViewMode != CAM_VIEW_MODE_FIRST_PERSON AND GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() != VEHICLE_ZOOM_LEVEL_BONNET)
						OR (eOnFootViewMode != CAM_VIEW_MODE_FIRST_PERSON AND (eInCarViewMode = CAM_VIEW_MODE_FIRST_PERSON OR GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = VEHICLE_ZOOM_LEVEL_BONNET))
							ePrevCarViewMode = eInCarViewMode
							SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE, eOnFootViewMode)
							bForcedFirstPersonView = TRUE
						
							/*IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh)
							ENDIF
						
							SET_ENTITY_COLLISION(sMainCars[iPlayersCar].veh, TRUE)
							FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, FALSE)
							SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, fPlayersCarShowroomHeading)
							SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, vPlayersCarInShowroom)
							SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, FALSE, FALSE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[iPlayersCar].veh, FALSE)
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh, VS_DRIVER)
							ENDIF
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
							SET_ENTITY_VISIBLE(sMainCars[iPlayersCar].veh, FALSE)
							
							CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] On-foot and in-vehicle view modes don't match, warping car early.")
							
							bWarpedCarEarlyForShowroomScene = TRUE*/
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					IF GET_IS_VEHICLE_ENGINE_RUNNING(sMainCars[iPlayersCar].veh)
						IF TIMERB() > 8000
							SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, FALSE, FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				//1927394 - Force the HD models early so when the cutscene ends they should be ready.
				IF TIMERB() > 5000
					IF NOT IS_ENTITY_DEAD(sMainCars[0].veh)
						SET_FORCE_HD_VEHICLE(sMainCars[0].veh, TRUE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(sMainCars[1].veh)
						SET_FORCE_HD_VEHICLE(sMainCars[1].veh, TRUE)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			bCutsceneSkipped = TRUE
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF
		
		SETUP_CARS_OUTSIDE_SHOWROOM()
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP	
		REMOVE_VEHICLE_RECORDING(CARREC_SHOWROOM_NINEF_1, strCarrec)
		REMOVE_VEHICLE_RECORDING(CARREC_SHOWROOM_NINEF_2, strCarrec)
		REMOVE_VEHICLE_RECORDING(CARREC_SHOWROOM_RAPIDGT_1, strCarrec)
		REMOVE_VEHICLE_RECORDING(CARREC_SHOWROOM_RAPIDGT_2, strCarrec)
	
		SET_NUMBER_OF_PARKED_VEHICLES(-1)
	
		SET_ROADS_BACK_TO_ORIGINAL(<<-89.0913, -1133.1434, 0.8860>>, <<13.5596, -1053.2734, 53.8860>>)
	
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_MEET_SIMEON
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			ENDIF
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
			SET_ENTITY_COORDS(vehFranklinsCar, vFranklinsCarStartPos)
			SET_ENTITY_HEADING(vehFranklinsCar, fFranklinsCarStartHeading)
		ENDIF
		
		bWarpedCarEarlyForShowroomScene = FALSE
		
		eSectionStage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC

PROC MEET_SIMEON()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			//Need the interior in order to position the cars inside the showroom correctly.
			IF interiorShowroom = NULL
				interiorShowroom = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-38.62, -1099.01, 27.31>>, "v_carshowroom")
			ENDIF
		
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
			PIN_INTERIOR_IN_MEMORY(interiorShowroom)		
		
			IF IS_INTERIOR_READY(interiorShowroom)
				IF bUsedACheckpoint
					START_REPLAY_SETUP(vFranklinByCarPos, 335.0142, FALSE)
					
					iCurrentEvent = 99
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					
					LOAD_SCENE(vFranklinByCarPos)
					
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					
					iCurrentEvent = 99
				ENDIF
			ENDIF
		ELSE
			SETUP_REQ_CAR_CHOICES()
		
			IF SETUP_LAMAR(vLamarByCarPos, fLamarByCarHeading)
			AND SETUP_NINEF(<<34.0424, -638.7692, 30.6252>>)
			AND SETUP_RAPIDGT(<<44.0424, -638.7692, 30.6252>>)
			AND SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)	
			AND SETUP_CARS_OUTSIDE_SHOWROOM()
			AND SETUP_SHOWROOM_CARS()
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
				
				SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
				SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
				
				BLOCK_SCENARIOS_AT_SHOWROOM()
				
				bRequestedFranklinsCarAsset = TRUE
				bWarpedCarEarlyForShowroomScene = FALSE
				eSectionStage = SECTION_STAGE_SETUP	
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP		
		REQUEST_ANIM_DICT(strShowroomLeadInOutAnims)
		REQUEST_ANIM_DICT(strLamarShowroomAnims)
		REQUEST_MODEL(modelLamarsPhone)
	
		IF SETUP_JIMMY(vJimmyStartPos, fJimmyStartHeading)
		AND SETUP_SIMEON(vSimeonStartPos, fSimeonStartHeading)
		AND SETUP_LAMAR(<<-37.5420, -1084.8704, 25.4344>>, 246.4778)
		AND HAS_ANIM_DICT_LOADED(strShowroomLeadInOutAnims)
		AND HAS_ANIM_DICT_LOADED(strLamarShowroomAnims)
		AND HAS_MODEL_LOADED(modelLamarsPhone)
		AND (NOT IS_PED_INJURED(sLamar.ped) AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sLamar.ped))
			NEW_LOAD_SCENE_STOP()
		
			//If skipped: make sure interior is loaded in and cars outside showroom have been created.
			IF IS_SCREEN_FADED_OUT()	
				ADD_PED_FOR_DIALOGUE(sConversationPeds, 6, sJimmy.ped, "JIMMY")
				ADD_PED_FOR_DIALOGUE(sConversationPeds, 4, sSimeon.ped, "SIMEON")
				PRELOAD_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_MCS1LI", CONV_PRIORITY_MEDIUM)
			
				WHILE NOT SETUP_CARS_OUTSIDE_SHOWROOM()
					WAIT(0)
				ENDWHILE
				
				IF iCurrentEvent != 99
					NEW_LOAD_SCENE_SPHERE_WITH_WAIT(vPlayersCarInShowroom, 300.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				ENDIF
			ENDIF
		
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
				IF NOT bWarpedCarEarlyForShowroomScene
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[iPlayersCar].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(sMainCars[iPlayersCar].veh)
					ENDIF
				
					SET_ENTITY_COLLISION(sMainCars[iPlayersCar].veh, TRUE)
					FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, FALSE)
					SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, fPlayersCarShowroomHeading)
					SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, vPlayersCarInShowroom)
					SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, FALSE, FALSE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[iPlayersCar].veh, FALSE)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[iPlayersCar].veh, VS_DRIVER)
					ENDIF
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					
					//If we skipped the showroom intro then we might get bug 2006896 while fading in, so make sure there's some delay to hide the pop.
					IF IS_SCREEN_FADED_OUT()
					AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
						WAIT(100)
					ELSE
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 100)
					ENDIF
				ELSE
					//In certain view modes the car gets warped early, just make sure they're no longer hidden when we cut back.
					SET_ENTITY_VISIBLE(sMainCars[iPlayersCar].veh, TRUE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(sMainCars[iBuddiesCar].veh)
				FREEZE_ENTITY_POSITION(sMainCars[iBuddiesCar].veh, FALSE)
				SET_VEHICLE_ENGINE_ON(sMainCars[iBuddiesCar].veh, FALSE, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[iBuddiesCar].veh, FALSE)
				
				SET_ENTITY_HEADING(sMainCars[iBuddiesCar].veh, fLamarsCarShowroomHeading)
				SET_ENTITY_COORDS(sMainCars[iBuddiesCar].veh, vLamarsCarInShowroom)
			ENDIF
			
			BOOL bPlayedIdleDuringFade = FALSE
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				CLEAR_PED_TASKS_IMMEDIATELY(sLamar.ped)
				sLamar.iSyncedScene = CREATE_SYNCHRONIZED_SCENE(<<-30.995, -1094.700, 25.423>>, <<-0.000, 0.000, -18.720>>)

				IF IS_SCREEN_FADED_OUT()
					TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strLamarShowroomAnims, "leadin_loop", INSTANT_BLEND_IN, SLOW_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_MELEE)
					SET_SYNCHRONIZED_SCENE_RATE(sLamar.iSyncedScene, 1.0)
					SET_SYNCHRONIZED_SCENE_LOOPED(sLamar.iSyncedScene, TRUE)
					bPlayedIdleDuringFade = TRUE
				ELSE
					TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strLamarShowroomAnims, "leadin_action", INSTANT_BLEND_IN, SLOW_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_MELEE)
					SET_SYNCHRONIZED_SCENE_RATE(sLamar.iSyncedScene, 0.98)
					SET_SYNCHRONIZED_SCENE_LOOPED(sLamar.iSyncedScene, FALSE)
				ENDIF
				
				TASK_LOOK_AT_ENTITY(sLamar.ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				
				objLamarsPhone = CREATE_OBJECT(modelLamarsPhone, <<-30.990, -1094.082, 25.423>>)
				ATTACH_ENTITY_TO_ENTITY(objLamarsPhone, sLamar.ped, GET_PED_BONE_INDEX(sLamar.ped, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelLamarsPhone)
			ENDIF
			
			CLEAR_HELP()
		
			DESTROY_ALL_CAMS()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-66.2173, -1121.9542, 14.8642>>, <<-41.2857, -1111.1638, 34.8642>>, TRUE)
		
			REMOVE_ANIM_DICT(strShowroomCamAnims)
		
			BLOCK_VEHICLE_GENS_IN_SHOWROOM(TRUE)
			CLEAR_TRIGGERED_LABELS()
			
			TRIGGER_MUSIC_EVENT("ARM1_RADIO_ON")
			
			IF NOT IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				WHILE NOT IS_SCREEN_FADED_IN()
					IF NOT IS_ENTITY_DEAD(sMainCars[0].veh)
						SET_FORCE_HD_VEHICLE(sMainCars[0].veh, TRUE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(sMainCars[1].veh)
						SET_FORCE_HD_VEHICLE(sMainCars[1].veh, TRUE)
					ENDIF
				
					WAIT(0)
				ENDWHILE
			ENDIF
			
			IF bPlayedIdleDuringFade
			AND NOT IS_PED_INJURED(sLamar.ped)
				sLamar.iSyncedScene = CREATE_SYNCHRONIZED_SCENE(<<-30.995, -1094.700, 25.423>>, <<-0.000, 0.000, -18.720>>)
			
				TASK_SYNCHRONIZED_SCENE(sLamar.ped, sLamar.iSyncedScene, strLamarShowroomAnims, "leadin_action", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 
										SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_BULLET_IMPACT | RBF_MELEE)
				SET_SYNCHRONIZED_SCENE_PHASE(sLamar.iSyncedScene, 0.0)
				SET_SYNCHRONIZED_SCENE_RATE(sLamar.iSyncedScene, 0.97)
				SET_SYNCHRONIZED_SCENE_LOOPED(sLamar.iSyncedScene, FALSE)
			ENDIF
			
			//Do this after the fade so it syncs with the dialogue
			IF NOT IS_PED_INJURED(sJimmy.ped)
			AND NOT IS_PED_INJURED(sSimeon.ped)
				iSyncSceneJimmySimeon = CREATE_SYNCHRONIZED_SCENE(<<-39.546, -1092.790, 25.422>>, <<0.0, 0.0, 0.0>>)
				
				TASK_SYNCHRONIZED_SCENE(sSimeon.ped, iSyncSceneJimmySimeon, strShowroomLeadInOutAnims, "_leadin_simeon", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				TASK_SYNCHRONIZED_SCENE(sJimmy.ped, iSyncSceneJimmySimeon, strShowroomLeadInOutAnims, "_leadin_jimmy", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneJimmySimeon, 0.35)
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			DISABLE_CONTROLS_INSIDE_SHOWROOM()
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
			
			SETTIMERA(0)
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		DISABLE_CONTROLS_INSIDE_SHOWROOM()
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
	
		#IF IS_DEBUG_BUILD
			DONT_DO_J_SKIP(sLocatesData)
		#ENDIF
		
		IF NOT bHasTextLabelTriggered[ARM1_MCS1LI]
		AND NOT IS_PED_INJURED(sSimeon.ped)
		AND NOT IS_PED_INJURED(sJimmy.ped)
			BEGIN_PRELOADED_CONVERSATION()
			bHasTextLabelTriggered[ARM1_MCS1LI] = TRUE
			
			//IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConversationPeds, "ARM1AUD", "ARM1_MCS1LI", "ARM1_MCS1LI_3", CONV_PRIORITY_MEDIUM)
			//	bHasTextLabelTriggered[ARM1_MCS1LI] = TRUE
			//ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[0].veh)
			SET_FORCE_HD_VEHICLE(sMainCars[0].veh, TRUE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[1].veh)
			SET_FORCE_HD_VEHICLE(sMainCars[1].veh, TRUE)
		ENDIF
		
		//2006896 - If we forced the cam view earlier then make sure it's reset once the player gets out the car. Also make sure they can't change the view themselves.
		IF bForcedFirstPersonView
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE, ePrevCarViewMode)
				bForcedFirstPersonView = FALSE
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			ENDIF
		ENDIF
		
		SWITCH iCurrentEvent
			CASE 0
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-34.079700,-1094.802368,25.172342>>, <<-35.014748,-1097.344482,27.672344>>, 9.500000)
					SET_GAMEPLAY_COORD_HINT(GET_ENTITY_COORDS(vehShowroomCars[1], FALSE), -1, 2500)
                    SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.6)
                    SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.015)                    
                    SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.05)
                    SET_GAMEPLAY_HINT_FOV(30.0)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-39.6, -1097.0, 25.6>>, PEDMOVE_WALK, -1, 0.25, ENAV_NO_STOPPING | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
					
					IF NOT IS_PED_INJURED(sSimeon.ped)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sSimeon.ped, -1, SLF_WHILE_NOT_IN_FOV)
					ENDIF
					
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_PED_INJURED(sSimeon.ped)
		AND NOT IS_PED_INJURED(sJimmy.ped)
		AND NOT IS_PED_INJURED(sLamar.ped)
			IF NOT DOES_BLIP_EXIST(sSimeon.blip)
				sSimeon.blip = CREATE_BLIP_FOR_ENTITY(sSimeon.ped)
			ENDIF
			
			SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(), 0.87)
		
			//No longer needed: the focus push deals with player control.
			//IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sJimmy.ped)) < 25.0
			//	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
			//ENDIF
		
			IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneJimmySimeon) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneJimmySimeon) >= 0.9
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(objLamarsPhone)
				IF HAS_ANIM_EVENT_FIRED(sLamar.ped, HASH("Phone_Disappear"))
					REMOVE_OBJECT(objLamarsPhone, TRUE)
				ENDIF
			ENDIF
		
			//Progress if any of the following conditions are met:
			// - The player hits the locate and the current conversation line is killed.
			// - Simeon and Jimmy's scene plays out fully.
			// - The player tries to leave the area.
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-38.590809,-1095.906860,25.422304>>, <<-51.048512,-1100.539063,27.422304>>, 10.500000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-26.404922,-1086.267212,25.573242>>, <<-31.385181,-1084.527832,27.573242>>, 2.250000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-37.614616,-1108.703613,25.455927>>, <<-38.443035,-1110.918457,27.436029>>, 2.250000)
			OR IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneJimmySimeon) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneJimmySimeon) >= 0.99
				eSectionStage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF
		
		//Request the garage cutscene in advance.
		REQUEST_CUTSCENE(strGarageCutscene)
		SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strGarageCutscene)
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		REMOVE_ALL_BLIPS()
		KILL_FACE_TO_FACE_CONVERSATION()
		
		REMOVE_OBJECT(objLamarsPhone, TRUE)
		
		SETTIMERA(0)
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_SHOWROOM_CUTSCENE
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP	
		eSectionStage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC

PROC SHOWROOM_CUTSCENE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			//Need the interior in order to position the cars inside the showroom correctly.
			IF interiorShowroom = NULL
				interiorShowroom = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-38.62, -1099.01, 27.31>>, "v_carshowroom")
			ENDIF
		
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vJimmyStartPos)
			PIN_INTERIOR_IN_MEMORY(interiorShowroom)		
		
			IF IS_INTERIOR_READY(interiorShowroom)
				IF bUsedACheckpoint
					START_REPLAY_SETUP(vJimmyStartPos, 335.0142, FALSE)
					
					iCurrentEvent = 99
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vJimmyStartPos)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					
					LOAD_SCENE(vJimmyStartPos)
					
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					
					iCurrentEvent = 99
				ENDIF
			ENDIF
		ELSE
			REQUEST_ANIM_DICT(strShowroomLeadInOutAnims)
	
			SETUP_REQ_CAR_CHOICES()
	
			IF SETUP_LAMAR(vLamarByCarPos, fLamarByCarHeading)
			AND SETUP_NINEF(<<34.0424, -638.7692, 30.6252>>)
			AND SETUP_RAPIDGT(<<44.0424, -638.7692, 30.6252>>)
			AND SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)	
			AND SETUP_JIMMY(vJimmyStartPos, fJimmyStartHeading)
			AND SETUP_SIMEON(vSimeonStartPos, fSimeonStartHeading)
			AND SETUP_CARS_OUTSIDE_SHOWROOM()
			AND SETUP_SHOWROOM_CARS()
			AND HAS_ANIM_DICT_LOADED(strShowroomLeadInOutAnims)
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
				SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
				SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
			
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinByCarHeading)
				
				BLOCK_SCENARIOS_AT_SHOWROOM()
				
				bRequestedFranklinsCarAsset = TRUE
				eSectionStage = SECTION_STAGE_SETUP	
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP	
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
		ENDIF
		
		REQUEST_CUTSCENE(strGarageCutscene)

		SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strGarageCutscene)

		IF NOT IS_ENTITY_DEAD(sMainCars[0].veh)
			SET_FORCE_HD_VEHICLE(sMainCars[0].veh, TRUE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[1].veh)
			SET_FORCE_HD_VEHICLE(sMainCars[1].veh, TRUE)
		ENDIF

		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
		AND (NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF) OR TIMERA() > 5000) //If dialogue starts late this may look odd.
			IF NOT IS_PED_INJURED(sLamar.ped)
				TASK_CLEAR_LOOK_AT(sLamar.ped)
				REGISTER_ENTITY_FOR_CUTSCENE(sLamar.ped, "Lamar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF NOT IS_PED_INJURED(sJimmy.ped)
				REGISTER_ENTITY_FOR_CUTSCENE(sJimmy.ped, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSimeon.ped)
				REGISTER_ENTITY_FOR_CUTSCENE(sSimeon.ped, "Siemon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehShowroomCars[0])
				//REGISTER_ENTITY_FOR_CUTSCENE(vehShowroomCars[0], "Dealership_Car_2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(vehShowroomCars[0], "ninef^1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehShowroomCars[1])
				REGISTER_ENTITY_FOR_CUTSCENE(vehShowroomCars[1], "Jimmys_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehShowroomCars[2])
				REGISTER_ENTITY_FOR_CUTSCENE(vehShowroomCars[2], "Franklin_stealcar", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehShowroomCars[3])
				//REGISTER_ENTITY_FOR_CUTSCENE(vehShowroomCars[3], "Dealership_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(vehShowroomCars[3], "bjxl^1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			START_CUTSCENE()
						
			//Lamar sometimes walks the wrong way on his exit state, so add a navmesh block.
			//iShowroomPedBlockingArea = ADD_NAVMESH_BLOCKING_OBJECT(<<-59.5004, -1094.0771, 25.4223>>, <<4.0, 4.0, 3.0>>, 72.3459)
			iShowroomPedBlockingArea = ADD_NAVMESH_BLOCKING_OBJECT(<<-59.0, -1095.8, 25.4223>>, <<10.0, 4.0, 3.0>>, DEG_TO_RAD(-67.2459))
			iShowroomPedBlockingArea2 = ADD_NAVMESH_BLOCKING_OBJECT(<<-37.4137, -1108.5670, 25.4223>>, <<4.0, 4.0, 3.0>>, DEG_TO_RAD(72.3459))
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			SETTIMERB(0)
			SETTIMERA(0)
			iCurrentEvent = 0
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		SWITCH iCurrentEvent
			CASE 0 //Reposition the stolen cars to their final spots.
				IF IS_CUTSCENE_PLAYING()
					CLEAR_PRINTS()
					CLEAR_HELP()
				
					IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)			
						SET_ENTITY_COLLISION(sMainCars[iPlayersCar].veh, TRUE)
						SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, vPlayersCarInShowroom)
						SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, fPlayersCarShowroomHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[iPlayersCar].veh)
						FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, TRUE)
						SET_VEHICLE_DOORS_LOCKED(sMainCars[iPlayersCar].veh, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, FALSE, FALSE)		
					ENDIF
					
					STOP_GAMEPLAY_HINT(TRUE)
					
					REMOVE_ANIM_DICT(strLamarShowroomAnims)
					REMOVE_ANIM_DICT(strShowroomLeadInOutAnims)
					
					DO_FADE_IN_WITH_WAIT()
					
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
	
		//IF IS_CUTSCENE_PLAYING()
		//	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		//	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		//ENDIF
		
		SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)
		
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			bCutsceneSkipped = TRUE
		ENDIF	
	
		IF NOT IS_CUTSCENE_ACTIVE()
			eSectionStage = SECTION_STAGE_CLEANUP
		ELSE
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin", GET_ENTITY_MODEL(PLAYER_PED_ID()))
				//SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
				//SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinByCarHeading)
				
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, fFranklinByCarHeading, FALSE)
				//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
			ENDIF
				
			IF NOT IS_PED_INJURED(sLamar.ped)	
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lamar", GET_ENTITY_MODEL(sLamar.ped))						
					SET_ENTITY_COORDS(sLamar.ped, vLamarByCarPos)
					SET_ENTITY_HEADING(sLamar.ped, fLamarByCarHeading)
					
					IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
						TASK_ENTER_VEHICLE(sLamar.ped, vehFranklinsCar, 40000, VS_FRONT_RIGHT, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(sLamar.ped, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		REQUEST_ANIM_DICT(strShowroomLeadInOutAnims)
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		REPLAY_STOP_EVENT()
		REMOVE_ALL_BLIPS()
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		SETTIMERA(0)
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_GO_TO_HOUSE
		
		AWARD_ACHIEVEMENT_FOR_MISSION(ACH00) // Welcome To Los Santos
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP	
		DO_FADE_OUT_WITH_WAIT()
	
		REPLAY_STOP_EVENT()
	
		SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
		
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
		ENDIF
		
		REMOVE_CUTSCENE()
		
		WHILE IS_CUTSCENE_ACTIVE()
		OR NOT SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)
			WAIT(0)
		ENDWHILE
		
		IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
			SET_ENTITY_COORDS(vehFranklinsCar, vFranklinsCarStartPos)
			SET_ENTITY_HEADING(vehFranklinsCar, fFranklinsCarStartHeading)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_ENTITY_COLLISION(sMainCars[iPlayersCar].veh, TRUE)
			SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, fPlayersCarShowroomHeading)
			SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, vPlayersCarInShowroom)
			SET_VEHICLE_ON_GROUND_PROPERLY(sMainCars[iPlayersCar].veh)
			FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, TRUE)
			SET_VEHICLE_DOORS_LOCKED(sMainCars[iPlayersCar].veh, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_VEHICLE_ENGINE_ON(sMainCars[iPlayersCar].veh, FALSE, FALSE)		
		ENDIF
		
		IF NOT IS_PED_INJURED(sLamar.ped)
			SET_ENTITY_COORDS(sLamar.ped, vLamarByCarPos)
			SET_ENTITY_HEADING(sLamar.ped, fLamarByCarHeading)
		ENDIF
		
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinByCarHeading)
		
		eSectionStage = SECTION_STAGE_CLEANUP
	ENDIF
ENDPROC



PROC GO_TO_HOUSE()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		//Need the interior in order to position the cars inside the showroom correctly.
		IF interiorShowroom = NULL
			interiorShowroom = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-38.62, -1099.01, 27.31>>, "v_carshowroom")
		ENDIF
		
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
		PIN_INTERIOR_IN_MEMORY(interiorShowroom)		
		
		IF bUsedACheckpoint
			START_REPLAY_SETUP(vFranklinByCarPos, fFranklinByCarHeading, FALSE)
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinByCarHeading)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			
			LOAD_SCENE(vFranklinByCarPos)
			WAIT(0)
		ENDIF
		
		SETUP_REQ_CAR_CHOICES()
		
		WHILE NOT IS_INTERIOR_READY(interiorShowroom)
		OR NOT SETUP_CARS_OUTSIDE_SHOWROOM()
		OR NOT SETUP_SHOWROOM_CARS()
		OR NOT DOES_ENTITY_EXIST(sLamar.ped)
		OR NOT DOES_ENTITY_EXIST(sMainCars[0].veh)
		OR NOT DOES_ENTITY_EXIST(sMainCars[1].veh)
		OR NOT DOES_ENTITY_EXIST(vehFranklinsCar)
		OR NOT DOES_ENTITY_EXIST(sJimmy.ped)
		OR NOT DOES_ENTITY_EXIST(sSimeon.ped)
		OR NOT HAS_VEHICLE_ASSET_LOADED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR))
		OR NOT HAS_ANIM_DICT_LOADED(strShowroomLeadInOutAnims)
			REQUEST_VEHICLE_ASSET(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR))
			REQUEST_ANIM_DICT(strShowroomLeadInOutAnims)
			PIN_INTERIOR_IN_MEMORY(interiorShowroom)
		
			SETUP_LAMAR(vLamarByCarPos, fLamarByCarHeading)
			SETUP_NINEF(<<34.0424, -638.7692, 30.6252>>)
			SETUP_RAPIDGT(<<44.0424, -638.7692, 30.6252>>)
			SETUP_FRANKLINS_CAR(vFranklinsCarStartPos, fFranklinsCarStartHeading)	
			SETUP_JIMMY(vJimmyStartPos, fJimmyStartHeading)
			SETUP_SIMEON(vSimeonStartPos, fSimeonStartHeading)
		
			WAIT(0)
		ENDWHILE
		
		END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		
		SETUP_REQ_LAMARS_CHOSEN_CAR_IN_GARAGE()
		SETUP_REQ_FORCE_ROOFS_DOWN_FOR_BOTH_CARS()
		
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vFranklinByCarPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fFranklinByCarHeading)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
			SET_ENTITY_COORDS(sMainCars[iPlayersCar].veh, <<-30.5774, -1090.5625, 25.4222>>)
			SET_ENTITY_HEADING(sMainCars[iPlayersCar].veh, 159.6013)
			FREEZE_ENTITY_POSITION(sMainCars[iPlayersCar].veh, TRUE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[iPlayersCar].veh, FALSE)
		ENDIF
		
		iShowroomPedBlockingArea = ADD_NAVMESH_BLOCKING_OBJECT(<<-59.0, -1095.8, 25.4223>>, <<10.0, 4.0, 3.0>>, DEG_TO_RAD(-67.2459))
		iShowroomPedBlockingArea2 = ADD_NAVMESH_BLOCKING_OBJECT(<<-37.4137, -1108.5670, 25.4223>>, <<4.0, 4.0, 3.0>>, 72.3459)
		
		BLOCK_SCENARIOS_AT_SHOWROOM()
		
		iCurrentEvent = 99
		bRequestedFranklinsCarAsset = TRUE
		eSectionStage = SECTION_STAGE_SETUP	
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP	
		//Interior is no longer needed
		IF interiorShowroom != NULL
			IF IS_INTERIOR_READY(interiorShowroom)
				UNPIN_INTERIOR(interiorShowroom)
				interiorShowroom = NULL
			ENDIF
		ENDIF
	
		IF IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
			SET_VEHICLE_ENGINE_ON(sMainCars[0].veh, FALSE, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[0].veh, FALSE)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(sMainCars[1].veh)
			SET_VEHICLE_ENGINE_ON(sMainCars[1].veh, FALSE, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sMainCars[1].veh, FALSE)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
			SET_VEHICLE_FIXED(vehFranklinsCar)
		ENDIF	
	
		//If skipped: make sure interior is loaded in and cars outside showroom have been created.
		IF IS_SCREEN_FADED_OUT()
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-38.0770, -1096.4669, 25.4223>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 296.6170)
			
			IF NOT IS_PED_INJURED(sLamar.ped)
				SET_ENTITY_COORDS(sLamar.ped, vLamarByCarPos)
				SET_ENTITY_HEADING(sLamar.ped, fLamarByCarHeading)
			ENDIF
			
			WHILE NOT SETUP_CARS_OUTSIDE_SHOWROOM()
				WAIT(0)
			ENDWHILE
			
			IF iCurrentEvent != 99
				NEW_LOAD_SCENE_SPHERE_WITH_WAIT(vFranklinByCarPos, 100.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION, 5000)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
			AND NOT IS_PED_INJURED(sLamar.ped)
				TASK_ENTER_VEHICLE(sLamar.ped, vehFranklinsCar, 40000, VS_FRONT_RIGHT, PEDMOVE_WALK)
			ENDIF
			
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, 296.6170, FALSE)
			
			WAIT(500)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
		
		BLOCK_VEHICLE_GENS_IN_SHOWROOM(TRUE)
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_1_GO_HOME")
			START_AUDIO_SCENE("ARM_1_GO_HOME")
		ENDIF
		
		IF NOT IS_PED_INJURED(sJimmy.ped)
		AND NOT IS_PED_INJURED(sSimeon.ped)
			IF HAS_ANIM_DICT_LOADED(strShowroomLeadInOutAnims)
				iSyncSceneJimmySimeon = CREATE_SYNCHRONIZED_SCENE(<<-39.546, -1092.790, 25.422>>, <<0.0, 0.0, 0.0>>)
				
				TASK_SYNCHRONIZED_SCENE(sSimeon.ped, iSyncSceneJimmySimeon, strShowroomLeadInOutAnims, "_leadout_simeon", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				TASK_SYNCHRONIZED_SCENE(sJimmy.ped, iSyncSceneJimmySimeon, strShowroomLeadInOutAnims, "_leadout_jimmy", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneJimmySimeon, 0.0)
			ENDIF
			
			TASK_LOOK_AT_ENTITY(sSimeon.ped, sJimmy.ped, -1, SLF_WHILE_NOT_IN_FOV)
		ENDIF
		
		DO_FADE_IN_WITH_WAIT()
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_MAX_WANTED_LEVEL(5)
		
		//Unfreeze the cars at this point.
		IF NOT IS_ENTITY_DEAD(sMainCars[0].veh)
			FREEZE_ENTITY_POSITION(sMainCars[0].veh, FALSE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sMainCars[0].veh, TRUE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sMainCars[0].veh)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(sMainCars[1].veh)
			FREEZE_ENTITY_POSITION(sMainCars[1].veh, FALSE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sMainCars[1].veh, TRUE)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sMainCars[1].veh)
		ENDIF
		
		//Set the cars outside the showroom as no longer needed.
		REMOVE_VEHICLE(vehCarsOutsideShowroom[0])
		
		//SET_SAVEHOUSE_RESPAWN_AVAILABLE(SAVEHOUSE_FRANKLIN_SC, TRUE)
		CLEAR_TRIGGERED_LABELS()
		SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_B, DOORSTATE_LOCKED)
		SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_LOCKED)
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CHECKPOINT_GO_HOME, "GO_TO_HOUSE", TRUE)
		
		SETTIMERA(0)
		sSimeon.iEvent = 0
		iStatsHelpTextTimer = 0
		iMeleeDialogueTimer = 0
		iCurrentEvent = 0
		bPlayerHasBeenInCar = FALSE
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_RUNNING
		#IF IS_DEBUG_BUILD
			DONT_DO_J_SKIP(sLocatesData)
		#ENDIF
		
		SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_G, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
		
		//2186588 - Disable replay camera movement just after the cutscene, as some peds pop in and out of view.
		IF TIMERA() < 3000
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		ENDIF
		
		//1000197 - Have Lamar walk to the car.
		IF NOT IS_PED_INJURED(sLamar.ped)
			IF NOT bPlayerHasBeenInCar
				SET_BIT(sLocatesData.iLocatesBitSet, BS_BUDDIES_WILL_WALK_TO_SPECIFIC_VEHICLE)
				SET_PED_MAX_MOVE_BLEND_RATIO(sLamar.ped, PEDMOVE_WALK)
				
				IF NOT IS_ENTITY_DEAD(vehFranklinsCar)
				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehFranklinsCar)
					CLEAR_BIT(sLocatesData.iLocatesBitSet, BS_BUDDIES_WILL_WALK_TO_SPECIFIC_VEHICLE)
					
					IF NOT IS_PED_IN_VEHICLE(sLamar.ped, vehFranklinsCar)
						TASK_ENTER_VEHICLE(sLamar.ped, vehFranklinsCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
					ENDIF
					
					bPlayerHasBeenInCar = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH iCurrentEvent
			CASE 0 //Play Jimmy convo first, then Lamar's line.
				IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
					IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
						blipCurrentDestination = CREATE_BLIP_FOR_ENTITY(vehFranklinsCar)
					ENDIF
				ENDIF
			
				IF NOT bHasTextLabelTriggered[ARM1_MCS1LO]
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						ADD_PED_FOR_DIALOGUE(sConversationPeds, 6, sJimmy.ped, "JIMMY")
						ADD_PED_FOR_DIALOGUE(sConversationPeds, 4, sSimeon.ped, "SIMEON")
						
						IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_GETINJ", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[ARM1_MCS1LO] = TRUE
						ENDIF
					ENDIF
				ELSE
					/*IF NOT bAlreadyPlayedCarTrashedDialogue
					AND NOT IS_ENTITY_DEAD(sMainCars[iPlayersCar].veh)
					AND iPlayersHealthBeforeRace - GET_ENTITY_HEALTH(sMainCars[iPlayersCar].veh) > 250
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ARM1_CAR_DAMAGED, TRUE)
					
						IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_BEATUP", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[ARM1_LEAVEG] = TRUE
						
							iCurrentEvent++
						ENDIF
					ELSE
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_LEAVEG", "ARM1_LEAVEG_1", CONV_PRIORITY_MEDIUM)
							bHasTextLabelTriggered[ARM1_LEAVEG] = TRUE
							
							iCurrentEvent++
						ENDIF
					ENDIF*/
					
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_LEAVEG", "ARM1_LEAVEG_1", CONV_PRIORITY_MEDIUM)
						bHasTextLabelTriggered[ARM1_LEAVEG] = TRUE
						
						iCurrentEvent++
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF DOES_BLIP_EXIST(blipCurrentDestination)
					REMOVE_BLIP(blipCurrentDestination)
				ENDIF
			
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData, vFranklinsHouse, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, TRUE, sLamar.ped, vehFranklinsCar, 
														   	   "AR1_HOME", "AR1_WAIT", "CMN_GENGETINY", "CMN_GENGETBCKY", FALSE, TRUE)
					//Fake locate size: just using for the blips, god text, etc.
					iCurrentEvent++
				ELSE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						//Help text on how to view stats.
						IF NOT bHasTextLabelTriggered[AR1_VIEWSTATS]
							IF iStatsHelpTextTimer = 0
								iStatsHelpTextTimer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - iStatsHelpTextTimer > 7000
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									//B*1032162 - Clear the print before the standard god text time.
									IF IS_THIS_PRINT_BEING_DISPLAYED("AR1_HOME")
										CLEAR_PRINTS()
									ENDIF
								
									PRINT_HELP("AR1_VIEWSTATS", DEFAULT_HELP_TEXT_TIME + 5000)
									bHasTextLabelTriggered[AR1_VIEWSTATS] = TRUE
									iStatsHelpTextTimer = 0
								ENDIF
							ENDIF
						ELIF NOT bHasTextLabelTriggered[AR1_VIEWSTATS2]
							IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
								iStatsHelpTextTimer = GET_GAME_TIMER()
							ELIF GET_GAME_TIMER() - iStatsHelpTextTimer > 250
							AND IS_SELECTOR_ONSCREEN()
								PRINT_HELP("AR1_VIEWSTATS2", DEFAULT_HELP_TEXT_TIME + 5000)
								bHasTextLabelTriggered[AR1_VIEWSTATS2] = TRUE
							ENDIF
						ENDIF
					
						IF bRequestedFranklinsCarAsset
							REMOVE_VEHICLE_ASSET(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR))
							bRequestedFranklinsCarAsset = FALSE
						ENDIF
						
						//B*1045971 - Change the locate size based on how fast the player is going.
						FLOAT fLocateWidth, fMaxSpeed, fCurrentSpeed
						fMaxSpeed = 12.0
						fCurrentSpeed = GET_ENTITY_SPEED(vehFranklinsCar)
						
						IF fCurrentSpeed < 3.0
							fCurrentSpeed = 3.0
						ELIF fCurrentSpeed > fMaxSpeed
							fCurrentSpeed = fMaxSpeed
						ENDIF
						
						fLocateWidth = 2.0 + (((fCurrentSpeed - 3.0) / (fMaxSpeed - 3.0)) * 8.5)
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-27.677097,-1427.279419,29.669218>>, <<-23.049107,-1427.299805,32.159901>>, fLocateWidth)
						AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)		
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							SET_WANTED_LEVEL_MULTIPLIER(0.0)
							
							fSpeedWhenHitFinalLocate = fCurrentSpeed
							iCurrentEvent++
						ELSE
							//Dialogue
							IF TIMERA() > 1000
							AND ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(), sLamar.ped)
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								ENDIF
					
								IF NOT bHasTextLabelTriggered[ARM1_DRIV]
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_DRIV2A", CONV_PRIORITY_MEDIUM)
											bHasTextLabelTriggered[ARM1_DRIV] = TRUE
										ENDIF
									ENDIF
								ELIF NOT bHasTextLabelTriggered[ARM1_ATHOME]
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-16.964743,-1462.946655,29.157858>>, <<-17.416340,-1431.480103,36.682789>>, 22.250000)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_ATHOME", CONV_PRIORITY_MEDIUM)
												bHasTextLabelTriggered[ARM1_ATHOME] = TRUE
											ENDIF
										ELSE
											KILL_FACE_TO_FACE_CONVERSATION()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bHasTextLabelTriggered[ARM1_DRIV]
						OR bHasTextLabelTriggered[ARM1_ATHOME]
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				VEHICLE_INDEX vehFinalCar
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehFinalCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
					
				BOOl bHasStopped
					
				IF IS_VEHICLE_DRIVEABLE(vehFinalCar)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
					
					FLOAT fStopDistance
					
					IF fSpeedWhenHitFinalLocate > 3.0 AND fSpeedWhenHitFinalLocate < 12.0
						fStopDistance = 2.0
					ELSE
						fStopDistance = 3.5
					ENDIF
				
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehFinalCar, fStopDistance, 1)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
						bHasStopped = TRUE
					ENDIF
				ELSE
					bHasStopped = TRUE
				ENDIF
			
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			
				IF NOT bHasTextLabelTriggered[ARM1_ATHOME]
					IF IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
						KILL_FACE_TO_FACE_CONVERSATION()
					ELIF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_ATHOME", CONV_PRIORITY_MEDIUM)
						//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE) //just in case the conversation during the drive was paused (if it's paused the cutscene will never load).
						bHasTextLabelTriggered[ARM1_ATHOME] = TRUE
					ENDIF
				ELSE
					IF bHasStopped
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE) 
						eSectionStage = SECTION_STAGE_CLEANUP
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		//Request the house cutscene in advance (the PC version has streaming issues so make it smaller).
		IF IS_PC_VERSION()
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFranklinsHouse) < (20.0 * 20.0)
				REQUEST_CUTSCENE(strHouseCutscene)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strHouseCutscene)
			ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFranklinsHouse) > (30.0 * 30.0)
				IF IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
				ENDIF
			ENDIF
		ELSE
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFranklinsHouse) < (DEFAULT_CUTSCENE_LOAD_DIST * DEFAULT_CUTSCENE_LOAD_DIST)
				REQUEST_CUTSCENE(strHouseCutscene)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strHouseCutscene)
			ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFranklinsHouse) > (DEFAULT_CUTSCENE_UNLOAD_DIST * DEFAULT_CUTSCENE_UNLOAD_DIST)
				IF IS_CUTSCENE_ACTIVE()
					REMOVE_CUTSCENE()
				ENDIF
			ENDIF
		ENDIF
		
		//Garage help
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			IF NOT bHasTextLabelTriggered[AR1_GARHELP1]
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-24.229092,-1451.200806,29.760489>>, <<-23.506229,-1430.866455,32.157558>>, 7.000000)
					PRINT_HELP("AR1_GARHELP1", DEFAULT_HELP_TEXT_TIME + 5000)
					bHasTextLabelTriggered[AR1_GARHELP1] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Unique vehicle help and mod apps.
		IF NOT bHasTextLabelTriggered[AR1_UNIQUE]
			IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehFranklinsCar)) < 225.0
				AND IS_ENTITY_ON_SCREEN(vehFranklinsCar)
					PRINT_HELP("AR1_UNIQUE", DEFAULT_HELP_TEXT_TIME + 5000)
					bHasTextLabelTriggered[AR1_UNIQUE] = TRUE
				ENDIF
			ENDIF
		ELIF NOT bHasTextLabelTriggered[AR1_APPHELP]
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				#IF NOT FEATURE_GEN9_STANDALONE PRINT_HELP("AR1_APPHELP", DEFAULT_HELP_TEXT_TIME + 5000) #ENDIF
				bHasTextLabelTriggered[AR1_APPHELP] = TRUE
			ENDIF
		ENDIF

		IF NOT IS_PED_INJURED(sSimeon.ped)
		AND NOT IS_PED_INJURED(sJimmy.ped)
			VEHICLE_INDEX vehClosest[1]
		
			SWITCH sSimeon.iEvent
				CASE 0
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneJimmySimeon) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneJimmySimeon) > 0.0)
					OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneJimmySimeon)
						GET_PED_NEARBY_VEHICLES(sSimeon.ped, vehClosest)
						
						IF IS_VEHICLE_DRIVEABLE(vehClosest[0])
							TASK_ENTER_VEHICLE(sJimmy.ped, vehClosest[0], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK)
							TASK_ENTER_VEHICLE(sSimeon.ped, vehClosest[0], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sJimmy.ped, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSimeon.ped, TRUE)
							
							iNumJimmySimeonConversationsPlayed = 0
							iNumTimesPlayedSimeonDialogue = 0
							sSimeon.iEvent++
						ENDIF
					ENDIF
				BREAK
				
				CASE 1
					//Play ambient lines if the car is damaged by the player.
					IF IS_PED_SITTING_IN_ANY_VEHICLE(sJimmy.ped)
						vehClosest[0] = GET_VEHICLE_PED_IS_IN(sJimmy.ped)
						
						IF NOT IS_ENTITY_DEAD(vehClosest[0])
							FLOAT fDistFromPlayer
							fDistFromPlayer = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehClosest[0]))
						
							//Handle random dialogue if the player sticks around by the car.
							IF fDistFromPlayer < 225.0
								IF GET_GAME_TIMER() - iJimmySimeonDialogueTimer > 0
									INTERIOR_INSTANCE_INDEX interiorPlayer
									interiorPlayer = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
									
									IF interiorPlayer != NULL //Don't play if the player is outside.
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF NOT bHasTextLabelTriggered[ARM1_RADIO1]
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_RADIO1", CONV_PRIORITY_MEDIUM)
													SET_VEHICLE_ENGINE_ON(vehClosest[0], TRUE, TRUE)
													SET_VEHICLE_RADIO_LOUD(vehClosest[0], TRUE)
													SET_VEH_RADIO_STATION(vehClosest[0], "RADIO_01_CLASS_ROCK")
													
													bHasTextLabelTriggered[ARM1_RADIO1] = TRUE
													iJimmySimeonDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 13000)
												ENDIF
											ELIF iNumJimmySimeonConversationsPlayed < 5
												TEXT_LABEL strLabel
												
												IF iNumJimmySimeonConversationsPlayed = 0
													strLabel = "ARM1_GETINJ2"
												ELIF iNumJimmySimeonConversationsPlayed = 1
													strLabel = "ARM1_GETINJ3"
												ELIF iNumJimmySimeonConversationsPlayed = 2
													strLabel = "ARM1_GETINJ4"
												ELIF iNumJimmySimeonConversationsPlayed = 3
													strLabel = "ARM1_GETINJ5"
												ELIF iNumJimmySimeonConversationsPlayed = 4
													strLabel = "ARM1_GETINJ6"
												ENDIF
												
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", strLabel, CONV_PRIORITY_MEDIUM)
													iNumJimmySimeonConversationsPlayed++
													iJimmySimeonDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 13000)
												ENDIF
											ELIF NOT bHasTextLabelTriggered[ARM1_FNKLEV1]
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_FNKLEV1", CONV_PRIORITY_MEDIUM)
													TASK_LOOK_AT_ENTITY(sSimeon.ped, PLAYER_PED_ID(), 2000, SLF_WHILE_NOT_IN_FOV)
													bHasTextLabelTriggered[ARM1_FNKLEV1] = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
							//Kill The first Jimmy/Simeon conversation once the player leaves.
							TEXT_LABEL strLabel
							strLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							
							IF ARE_STRINGS_EQUAL(strLabel, "ARM1_GETINJ")
								IF fDistFromPlayer > 400.0
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
						
							//Handle the car getting damaged by Franklin.
							IF GET_GAME_TIMER() - iMeleeDialogueTimer > 0
								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehClosest[0], PLAYER_PED_ID())
									MISSION_FAILED(FAILED_DISTURBED_DEAL)
								
									//Do we still need any of this?
									INT iRandom, iRandom2
									iRandom = GET_RANDOM_INT_IN_RANGE(0, 2)
									iRandom2 = GET_RANDOM_INT_IN_RANGE(0, 2)
									
									IF iRandom = 0
										IF iRandom2 = 0
											PLAY_PED_AMBIENT_SPEECH(sJimmy.ped, "GENERIC_INSULT_MED", SPEECH_PARAMS_FORCE)
										ELSE
											PLAY_PED_AMBIENT_SPEECH(sJimmy.ped, "GENERIC_CURSE_MED", SPEECH_PARAMS_FORCE)
										ENDIF
										
										iMeleeDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
									ELSE
										IF iNumTimesPlayedSimeonDialogue < 3
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sConversationPeds, "ARM1AUD", "ARM1_FNKLEV2", CONV_PRIORITY_MEDIUM)
													iMeleeDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
													iNumTimesPlayedSimeonDialogue++
												ENDIF
											ENDIF
										ELSE	
											IF iRandom2 = 0
												PLAY_PED_AMBIENT_SPEECH(sJimmy.ped, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE)
											ELSE
												PLAY_PED_AMBIENT_SPEECH(sJimmy.ped, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE)
											ENDIF
											
											iMeleeDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehClosest[0])
							
							INT i 
							REPEAT COUNT_OF(vehShowroomCars) i
								IF vehShowroomCars[i] != vehClosest[0]
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehShowroomCars[i], PLAYER_PED_ID())
										MISSION_FAILED(FAILED_DISTURBED_DEAL)
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF NOT IS_VEHICLE_DRIVEABLE(vehClosest[0])
								MISSION_FAILED(FAILED_TRASH_SHOWROOM)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF

		FLOAT fDistFromShowroom = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-20.2493, -1082.7458, 25.6579>>)

		//Unpin the interior once the player moves away
		IF interiorShowroom != NULL
			IF fDistFromShowroom > 1600.0
				IF IS_INTERIOR_READY(interiorShowroom)
					UNPIN_INTERIOR(interiorShowroom)
					interiorShowroom = NULL
				ENDIF
			ENDIF
		ENDIF
		
		//Remove Simeon, Jimmy and the showroom cars.
		IF fDistFromShowroom > 10000.0
			IF DOES_ENTITY_EXIST(sJimmy.ped)
				REMOVE_PED(sJimmy.ped, TRUE)
				REMOVE_PED(sSimeon.ped, TRUE)
				REMOVE_ANIM_DICT(strShowroomLeadInOutAnims)
				
				//B* 2023813:Set a null entity to be tracked for damage - prevents reporting fake damage from deleting the vehicle
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
				
				REMOVE_VEHICLE(sMainCars[0].veh)
				REMOVE_VEHICLE(sMainCars[1].veh)
				
				//If the scripted showroom cars are still there, set them as no longer needed.
				IF bShowroomCarsBlocked
					REMOVE_VEHICLE(vehShowroomCars[0])
					REMOVE_VEHICLE(vehShowroomCars[1])
					REMOVE_VEHICLE(vehShowroomCars[2])
					REMOVE_VEHICLE(vehShowroomCars[3])
				
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vRoomCorner1, vRoomCorner2, TRUE)
				
					bShowroomCarsBlocked = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		//Remove the stolen cars.
		IF DOES_ENTITY_EXIST(sMainCars[0].veh)
			IF fDistFromShowroom > 22500.0
				REMOVE_VEHICLE(sMainCars[0].veh)
				REMOVE_VEHICLE(sMainCars[1].veh)
			ENDIF
		ENDIF
		

		//Fail for abandoning Lamar/car if the player goes too far away.
		IF NOT IS_PED_INJURED(sLamar.ped)
		AND IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
			IF VDIST2(GET_ENTITY_COORDS(sLamar.ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 90000.0
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFranklinsCar)
				MISSION_FAILED(FAILED_ABANDONED_LAMAR)
			ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(vehFranklinsCar), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 90000.0
				MISSION_FAILED(FAILED_ABANDONED_CAR)
			ENDIF
		ENDIF
		
		//Fail for damaging the cars further
		IF IS_VEHICLE_DRIVEABLE(sMainCars[NINEF_INDEX].veh)
		AND IS_VEHICLE_DRIVEABLE(sMainCars[RAPIDGT_INDEX].veh)
			IF (HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[NINEF_INDEX].veh, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON) 
			AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[NINEF_INDEX].veh, WEAPONTYPE_UNARMED))
			OR (HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[RAPIDGT_INDEX].veh, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON) 
			AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sMainCars[RAPIDGT_INDEX].veh, WEAPONTYPE_UNARMED))
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sMainCars[NINEF_INDEX].veh)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sMainCars[RAPIDGT_INDEX].veh)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sMainCars[NINEF_INDEX].veh, PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sMainCars[RAPIDGT_INDEX].veh, PLAYER_PED_ID())
					MISSION_FAILED(FAILED_DAMAGED_CARS_AFTER_DROPOFF)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_CLEANUP
		REMOVE_ALL_BLIPS()
		//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		//KILL_FACE_TO_FACE_CONVERSATION()
		
		REMOVE_VEHICLE(sMainCars[iPlayersCar].veh, TRUE)
		REMOVE_VEHICLE(sMainCars[iBuddiesCar].veh, TRUE)
		REMOVE_PED(sJimmy.ped, TRUE)
		REMOVE_PED(sSimeon.ped, TRUE)
		
		REMOVE_ANIM_DICT(strShowroomLeadInOutAnims)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		
		IF iShowroomPedBlockingArea != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iShowroomPedBlockingArea)
			iShowroomPedBlockingArea = -1
		ENDIF
		
		IF iShowroomPedBlockingArea2 != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(iShowroomPedBlockingArea2)
			iShowroomPedBlockingArea2 = -1
		ENDIF
		
		INT i = 0 
		REPEAT COUNT_OF(objShowroomGlass) i
			REMOVE_OBJECT(objShowroomGlass[i], FALSE)
		ENDREPEAT
		
		SETTIMERA(0)
		iCurrentEvent = 0
		eSectionStage = SECTION_STAGE_SETUP
		eMissionStage = STAGE_HOUSE_CUTSCENE
	ENDIF
	
	IF eSectionStage = SECTION_STAGE_SKIP	
		IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
			IF NOT IS_PED_INJURED(sLamar.ped)
				IF NOT IS_PED_IN_VEHICLE(sLamar.ped, vehFranklinsCar)
					SET_PED_INTO_VEHICLE(sLamar.ped, vehFranklinsCar, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFranklinsCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehFranklinsCar, VS_DRIVER)
			ENDIF
			
			SET_ENTITY_COORDS(vehFranklinsCar, vFranklinsHouse)
		ENDIF
	
		LOAD_SCENE(vFranklinsHouse)
	
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

PROC HOUSE_CUTSCENE_COMBINED_VERSION()
	IF eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
		IF iCurrentEvent != 99
			IF bUsedACheckpoint
				START_REPLAY_SETUP(<<-13.9541, -1446.6563, 30.5515>>, 335.0142, FALSE)
				
				iCurrentEvent = 99
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-13.9541, -1446.6563, 30.5515>>)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				
				LOAD_SCENE(<<-13.9541, -1446.6563, 30.5515>>)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				
				iCurrentEvent = 99
			ENDIF
		ELSE
			IF SETUP_LAMAR(<<-17.8292, -1458.0117, 29.4598>>)
			AND SETUP_FRANKLINS_CAR(vFranklinsCarFinalPos, fFranklinsCarFinalHeading)	
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			
				IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehFranklinsCar)
					
					IF NOT IS_PED_INJURED(sLamar.ped)
						SET_PED_INTO_VEHICLE(sLamar.ped, vehFranklinsCar, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
				
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_SETUP
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON | SPC_ALLOW_PLAYER_DAMAGE)
		ENDIF

		REQUEST_CUTSCENE(strHouseCutscene)
		REQUEST_MODEL(modelShirt)
		REQUEST_MODEL(modelWardrobeLeft)
		REQUEST_MODEL(modelWardrobeRight)
		REQUEST_MODEL(modelHealthPack)
		
		SET_CUTSCENE_PED_COMPONENT_VARIATIONS(strHouseCutscene)
		
		IF HAS_MODEL_LOADED(modelShirt)
		AND HAS_MODEL_LOADED(modelWardrobeLeft)
		AND HAS_MODEL_LOADED(modelWardrobeRight)
		AND HAS_MODEL_LOADED(modelHealthPack)
		AND TIMERA() > 1000
		AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR IS_FACE_TO_FACE_CONVERSATION_PAUSED())
			BOOL bCutsceneLoaded = FALSE
		
			IF IS_PC_VERSION()
				bCutsceneLoaded = HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			ELSE
				bCutsceneLoaded = HAS_CUTSCENE_LOADED()
			ENDIF
		
			IF bCutsceneLoaded
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				
				IF IS_AUDIO_SCENE_ACTIVE("ARM_1_GO_HOME")
					STOP_AUDIO_SCENE("ARM_1_GO_HOME")
				ENDIF
				
				objShirt = CREATE_OBJECT(modelShirt, <<-11.700, -1439.255, 30.099>>)
				
				objHealthPack = CREATE_OBJECT_NO_OFFSET(modelHealthPack, <<-17.9200, -1436.34, 30.2033>>)
				SET_ENTITY_HEADING(objHealthPack, -158.75)
				FREEZE_ENTITY_POSITION(objHealthPack, TRUE)

				objWardrobeDoors[0] = CREATE_OBJECT(modelWardrobeLeft, <<-12.700, -1439.255, 20.099>>)
				objWardrobeDoors[1] = CREATE_OBJECT(modelWardrobeRight, <<-10.700, -1439.255, 20.099>>)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(modelShirt)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelHealthPack)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelWardrobeLeft)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelWardrobeRight)
				
				REGISTER_ENTITY_FOR_CUTSCENE(objShirt, "Franklins_shirt", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objWardrobeDoors[0], "Closet_Door_L", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objWardrobeDoors[1], "Closet_Door_R", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				CREATE_MODEL_HIDE(<<-18.3539, -1438.7838, 31.3050>>, 3.0, V_ILEV_FRNKWARDDR1, FALSE)
				CREATE_MODEL_HIDE(<<-18.3594, -1438.1329, 31.3050>>, 3.0, V_ILEV_FRNKWARDDR2, FALSE)
				
				SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_B, DOORSTATE_UNLOCKED)
				SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_UNLOCKED)
				//SET_SAVEHOUSE_RESPAWN_AVAILABLE(SAVEHOUSE_FRANKLIN_SC, TRUE)
				
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILEV_FA_ROOMDOOR, <<-15.989, -1436.03, 31.199>>, TRUE, 0.0, 0.0, -1.0)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
				START_CUTSCENE(CUTSCENE_NO_WIDESCREEN_BORDERS)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

				
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					g_eArm1PrestreamDenise = ARM1_PD_1_mission_requested_prestream
				ENDIF
				
				iCurrentEvent = 0
				bCutsceneSkipped = FALSE
				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_RUNNING
		IF WAS_CUTSCENE_SKIPPED()
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
			bCutsceneSkipped = TRUE
			eSectionStage = SECTION_STAGE_SKIP
		ENDIF

		IF interiorFranklinsHouse = NULL
			interiorFranklinsHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-14.6461, -1437.3323, 31.1160>>, "v_franklins")
		ELIF NOT IS_INTERIOR_READY(interiorFranklinsHouse)
			PIN_INTERIOR_IN_MEMORY(interiorFranklinsHouse)
		ENDIF
		
		VECTOR vCamRot = GET_FINAL_RENDERED_CAM_ROT()

		//Load assets for tutorial scene in advance
		IF NOT DOES_ENTITY_EXIST(objShirt)
			REQUEST_MODEL(modelShirt)
			
			IF HAS_MODEL_LOADED(modelShirt)
				objShirt = CREATE_OBJECT_NO_OFFSET(modelShirt, <<-19.126, -1438.7911, 31.6833>>)
				SET_ENTITY_ROTATION(objShirt, <<-3.744568,-0.022376,14.537273>>)
				FREEZE_ENTITY_POSITION(objShirt, TRUE)
				SET_ENTITY_VISIBLE(objShirt, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(modelShirt)
			ENDIF
		ENDIF

		SWITCH iCurrentEvent
			CASE 0 //Store car and delete any no longer needed assets.
				IF IS_CUTSCENE_PLAYING()
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					CLEAR_PRINTS()
					CLEAR_HELP()
					
					IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
						SET_ENTITY_COORDS(vehFranklinsCar, <<-25.1690, -1427.9659, 29.6567>>)
						SET_ENTITY_HEADING(vehFranklinsCar, 0.9548)
						SET_VEHICLE_ENGINE_ON(vehFranklinsCar, FALSE, FALSE)
						SET_VEHICLE_FIXED(vehFranklinsCar)
					ENDIF
					
					REMOVE_PED(sLamar.ped, TRUE)
					
					CLEAR_AREA(<<-17.8292, -1458.0117, 29.4598>>, 100.0, TRUE)

					DO_FADE_IN_WITH_WAIT() //Do this here instead of the setup stage, otherwise you see a frame where the cutscene hasn't started playing yet.

					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 1
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			
				IF GET_CUTSCENE_TIME() > 21500
					PRINT_HELP("AR1_BARBERS", 7000)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 2 //Start of tutorial.
				IF GET_CUTSCENE_TIME() > 26700
					IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_FRANKLIN_LIVING)
						START_AMBIENT_TV_PLAYBACK(TV_LOC_FRANKLIN_LIVING)
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("armenian1.sc - TV failed to load in time: TV_LOC_FRANKLIN_LIVING")
						#ENDIF
					ENDIF
					
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_01_BH, TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_02_SC, TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_03_V, TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_04_SS, TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_05_MP, TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_06_HW, TRUE)
					SET_STATIC_BLIP_HIDDEN_IN_MISSION(STATIC_BLIP_SHOP_HAIRDO_07_PB, TRUE)
					
					/*IF DOES_PICKUP_EXIST(pickupHouse)
						REMOVE_PICKUP(pickupHouse)
					ENDIF
					
					CLEAR_AREA(<<-17.920, -1436.34, 30.2033>>, 2.0, TRUE)	
				
					INT iPlacementFlags
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
			
					pickupHouse = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, <<-17.920, -1436.34, 30.2033>>, <<0.0000, 0.0000, -158.75>>, iPlacementFlags)*/
					
					SETTIMERA(0)
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 3 //Display savehouse help.
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(vCamRot.z - GET_ENTITY_HEADING(PLAYER_PED_ID()))
				
				IF TIMERA() > 3000 	
					PRINT_HELP("AR1_SAVE1")
					DISPLAY_RADAR(TRUE)
					SET_CAN_DISPLAY_MINIMAP_DURING_CUTSCENE_THIS_UPDATE()
					
					IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
						blipCurrentDestination = CREATE_BLIP_FOR_COORD(<<-14.3803, -1438.5143, 30.1015>>)
						SET_BLIP_SPRITE(blipCurrentDestination, RADAR_TRACE_SAFEHOUSE)
						SET_BLIP_FLASHES(blipCurrentDestination, TRUE)
					ENDIF
					
					SETTIMERB(0)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK

			CASE 4 //Clear savehouse help.
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(vCamRot.z - GET_ENTITY_HEADING(PLAYER_PED_ID()))
				SET_CAN_DISPLAY_MINIMAP_DURING_CUTSCENE_THIS_UPDATE()
			
				IF TIMERB() > 5000
					//Force the front door shut.
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-14.8691, -1441.1586, 31.1932>>, 3.0, v_ilev_fa_frontdoor)
						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_fa_frontdoor, <<-14.8691, -1441.1586, 31.1932>>, TRUE, 0.0)
					ENDIF
				
					DISPLAY_RADAR(FALSE)
					SETTIMERB(0)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 5 //Display TV help
				IF NOT bClearedHelp
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_SAVE1")
						IF TIMERA() > 8750
							CLEAR_HELP(FALSE)
							bClearedHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF TIMERA() > 9500
					PRINT_HELP("AR1_TVHELP")
					
					SETTIMERB(0)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 6 //Clear TV help.
				IF TIMERB() > 5000				
					//CLEAR_HELP(FALSE)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 7 //Display medipack help.
				IF NOT bClearedHelp
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_TVHELP")
						IF TIMERA() > 15000
							CLEAR_HELP(FALSE)
							bClearedHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF TIMERA() > 16000
					PRINT_HELP("AR1_MEDIHELP")
					
					SETTIMERB(0)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 8 //Clear medipack help.
				IF TIMERB() > 5000
					//CLEAR_HELP(FALSE)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 9 //Display bed help.
				IF NOT bClearedHelp
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_MEDIHELP")
						IF TIMERA() > 22500
							CLEAR_HELP(FALSE)
							bClearedHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF TIMERA() > 23500			
					//Unlock the bathroom door.
					SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILEV_FA_ROOMDOOR, <<-15.989, -1436.03, 31.199>>, FALSE, 0.0, 0.0, 0.0)
					
					PRINT_HELP("AR1_SAVE2")
					SETTIMERB(0)
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 10 //Display phone help.
				IF TIMERB() > 5000
					PRINT_HELP("AR1_SAVE2B")
					SETTIMERB(0)
					
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 11 //Clear phone help.
				IF TIMERB() > 5000
					//CLEAR_HELP(FALSE)
					SETTIMERB(0)
					
					bClearedHelp = FALSE
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 12 //Display wardrobe help.
				IF NOT bClearedHelp
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AR1_SAVE2B")
						IF TIMERA() > 35000
							CLEAR_HELP(FALSE)
							bClearedHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF TIMERA() > 36000
					PRINT_HELP("AR1_SAVE3")
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 13 //Clear wardrobe help.
				IF TIMERB() > 7000
					CLEAR_HELP(FALSE)
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
	
		IF DOES_ENTITY_EXIST(objShirt)
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_shirt")
				FREEZE_ENTITY_POSITION(objShirt, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objWardrobeDoors[0])
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Closet_Door_L")
				FREEZE_ENTITY_POSITION(objWardrobeDoors[0], TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objWardrobeDoors[1])
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Closet_Door_R")
				FREEZE_ENTITY_POSITION(objWardrobeDoors[1], TRUE)
			ENDIF
		ENDIF
	
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			IF DOES_ENTITY_EXIST(objShirt)
				SET_ENTITY_VISIBLE(objShirt, TRUE)
			ENDIF
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		ENDIF
	
		IF NOT IS_CUTSCENE_ACTIVE()
			eSectionStage = SECTION_STAGE_CLEANUP
		ENDIF
	ENDIF

	IF eSectionStage = SECTION_STAGE_CLEANUP
		REMOVE_PED(sLamar.ped, TRUE)
		CLEAR_HELP()
	
		REPLAY_STOP_EVENT()
	
		IF bCutsceneSkipped	
		AND IS_SCREEN_FADED_OUT()
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			
			NEW_LOAD_SCENE_SPHERE_WITH_WAIT(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
		ENDIF
	
		Execute_Activate_Shop_Barbers()
	
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(V_ILEV_FA_ROOMDOOR, <<-15.989, -1436.03, 31.199>>, FALSE, 0.0, 0.0, 0.0)
		
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-14.8691, -1441.1586, 31.1932>>, 3.0, v_ilev_fa_frontdoor)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_fa_frontdoor, <<-14.8691, -1441.1586, 31.1932>>, TRUE, 0.0)
		ENDIF
	
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		SET_FRONTEND_RADIO_ACTIVE(TRUE)
	
		DO_FADE_IN_WITH_WAIT()
	
		MISSION_PASSED()
	ENDIF
		
	IF eSectionStage = SECTION_STAGE_SKIP
		DO_FADE_OUT_WITH_WAIT()
		
		SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
	
		STOP_CUTSCENE()
		
		bCutsceneSkipped = TRUE
		eSectionStage = SECTION_STAGE_RUNNING
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD			
	/// PURPOSE:
	///    Records the trigger car (which will be invisible) and all of the traffic
	PROC RECORD_CHASE_GHOST_AND_TRAFFIC()
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	
		IF eSectionStage = SECTION_STAGE_SETUP
			REQUEST_MODEL(modelMainCar1)
		
			IF HAS_MODEL_LOADED(modelMainCar1)
				sMainCars[0].veh = CREATE_VEHICLE(modelMainCar1, sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMainCars[0].veh)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[0].veh)
				ENDIF
				
				INIT_UBER_RECORDING(strCarrec)

				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			UPDATE_UBER_RECORDING()
			
			IF bDebugAutoPlayTrigger
				REQUEST_MODEL(intruder) //intruder)
				REQUEST_MODEL(manana) //emperor)
			
				IF IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
					IF IS_RECORDING_GOING_ON_FOR_VEHICLE(sMainCars[0].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sMainCars[0].veh)
							REQUEST_VEHICLE_RECORDING(CARREC_BUDDY, strCarrec)
							
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_BUDDY, strCarrec)
								START_PLAYBACK_RECORDED_VEHICLE(sMainCars[0].veh, CARREC_BUDDY, strCarrec)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Plays back the trigger car and traffic, records setpieces.
	PROC RECORD_CHASE_SET_PIECES()	
		INT iCarrec = CARREC_BUDDY
	
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
		IF eSectionStage = SECTION_STAGE_SETUP
			REQUEST_MODEL(modelMainCar1)
		
			IF HAS_MODEL_LOADED(modelMainCar1)
				REMOVE_ALL_BLIPS()
				
			
				vehTriggerCar = CREATE_VEHICLE(modelMainCar1, sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
				SET_ENTITY_COLLISION(vehTriggerCar, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
				
				//SET_ENTITY_COORDS(PLAYER_PED_ID(), sMainCars[0].vStartPos + <<-2.0, -2.0, 0.0>>)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTriggerCar)
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
				
				iCurrentEvent = 0
				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)
			SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(0.0)
		
			IF IS_VEHICLE_DRIVEABLE(vehTriggerCar)
				IF iCurrentEvent = 0
					INITIALISE_UBER_PLAYBACK(strCarrec, iCarrec)
					LOAD_CHASE_UBER_DATA()
					SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
					SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
					fUberPlaybackDensitySwitchOffRange = 200.0
					//LOAD_UBER_DATA_2()

					iCurrentEvent++
				ELIF iCurrentEvent = 1
					REQUEST_VEHICLE_RECORDING(iCarrec, strCarrec)
				
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrec, strCarrec)					
						IF SET_CAR_AT_PLAYBACK_POSITION(vehTriggerCar, iCarrec, 0.0, TRUE, FALSE)
							iCurrentEvent++
						ENDIF
					ENDIF		
				ELIF iCurrentEvent = 2
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTriggerCar)	
						FLOAT fTime = GET_TIME_POSITION_IN_RECORDING(vehTriggerCar)
					
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
						CREATE_ALL_WAITING_UBER_CARS()
						PRELOAD_ALL_CHASE_RECORDINGS(fTime)
						DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
						UPDATE_UBER_PLAYBACK(vehTriggerCar, 1.0)
						
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						
						VECTOR vPos1 = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iCarrec, fTime + 500.0, strCarrec)
						VECTOR vPos2 = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iCarrec, fTime + 1000.0, strCarrec)
						VECTOR vPos3 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTriggerCar, <<-1.7329, 6.4243, 0.1649>>)
						VECTOR vPos4 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTriggerCar, <<-1.7329, 12.4243, 0.1649>>)
						
						DRAW_DEBUG_SPHERE(vPos1, 0.5, 0, 255, 0, 128)
						DRAW_DEBUG_SPHERE(vPos2, 0.5, 0, 255, 0, 128)
						DRAW_DEBUG_SPHERE(vPos3, 1.0, 0, 0, 255, 128)
						DRAW_DEBUG_SPHERE(vPos4, 1.0, 0, 0, 255, 128)
					ELSE
						iCurrentEvent = 1
						
						CLEANUP_UBER_PLAYBACK(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Plays back the current trigger recording as a ghost, to allow a close re-recording.
	///     NOTE: doesn't include traffic or set-piece cars
	PROC REDO_TRIGGER_RECORDING()
		VECTOR vStartPos = <<-1880.2660, -577.8439, 10.7702>>
		FLOAT fStartHeading = 319.7534
	
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
		IF eSectionStage = SECTION_STAGE_SETUP
			REQUEST_MODEL(modelMainCar1)
			REQUEST_MODEL(modelMainCar2)
		
			IF HAS_MODEL_LOADED(modelMainCar1)
			AND HAS_MODEL_LOADED(modelMainCar2)
				REMOVE_ALL_BLIPS()
			
				vehTriggerCar = CREATE_VEHICLE(modelMainCar1, sMainCars[0].vStartPos, sMainCars[0].fStartHeading) 
				SET_ENTITY_COLLISION(vehTriggerCar, FALSE)
				SET_ENTITY_VISIBLE(vehTriggerCar, FALSE)
				FREEZE_ENTITY_POSITION(vehTriggerCar, TRUE)
				
				sMainCars[0].veh = CREATE_VEHICLE(modelMainCar1, vStartPos, fStartHeading) //New start location
				SET_ENTITY_COLLISION(sMainCars[0].veh, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar2)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sMainCars[0].veh)
				
				iCurrentEvent = 0
				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			IF IS_VEHICLE_DRIVEABLE(vehTriggerCar)
			AND IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
				IF iCurrentEvent = 0
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					iCurrentEvent++
				ELIF iCurrentEvent = 1
					REQUEST_VEHICLE_RECORDING(CARREC_BUDDY, strCarrec)
				
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(CARREC_BUDDY, strCarrec)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
							START_RECORDING_VEHICLE(sMainCars[0].veh, 999, strCarrec, TRUE)
							
							FREEZE_ENTITY_POSITION(vehTriggerCar, FALSE)
							START_PLAYBACK_RECORDED_VEHICLE(vehTriggerCar, CARREC_BUDDY, strCarrec)
							iCurrentEvent++
							
							PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "RECORDING STARTED", DEFAULT_GOD_TEXT_TIME, 0)
						ENDIF
					ENDIF		
				ELIF iCurrentEvent = 2
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTriggerCar)	
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(vehTriggerCar, RDM_WHOLELINE)
					ENDIF
					
					IF bDebugEnhanceDriving
						APPLY_FORCE_TO_ENTITY(sMainCars[0].veh, APPLY_TYPE_FORCE, <<0.0, 0.0, -10.0>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
						STOP_RECORDING_VEHICLE(sMainCars[0].veh)
						SET_ENTITY_COORDS(sMainCars[0].veh, vStartPos)
						SET_ENTITY_HEADING(sMainCars[0].veh, fStartHeading)
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTriggerCar)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTriggerCar)
						ENDIF
						
						SET_ENTITY_COORDS(vehTriggerCar, sMainCars[0].vStartPos + <<0.0, 0.0, 0.01>>)
						FREEZE_ENTITY_POSITION(vehTriggerCar, TRUE)
						
						iCurrentEvent = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
			bDebugEnhanceDriving = NOT bDebugEnhanceDriving
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehTriggerCar)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(vehTriggerCar), 0.5, 255, 0, 0, 20)
		ENDIF
	ENDPROC
	
	PROC RECORD_CAMERA()
		INT iCarrec = CARREC_BUDDY
		INT iCamCarrec = CARREC_BUDDY
		INT iRecordCarrec = 999
		FLOAT fStartTime = 200.0
		VECTOR vOffset = <<0.0, 0.0, 0.1>>
	
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
		IF eSectionStage = SECTION_STAGE_SETUP
			REQUEST_MODEL(modelMainCar1)
		
			IF HAS_MODEL_LOADED(modelMainCar1)
				REMOVE_ALL_BLIPS()
			
				vehTriggerCar = CREATE_VEHICLE(modelMainCar1, sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
				vehDebugCamCar = CREATE_VEHICLE(modelMainCar1, sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
				vehDebug[0] = CREATE_VEHICLE(modelMainCar1, sMainCars[1].vStartPos + <<5.0, 0.0, 0.0>>, sMainCars[1].fStartHeading)
				vehDebugFov = CREATE_VEHICLE(modelMainCar1, sMainCars[1].vStartPos + <<10.0, 0.0, 0.0>>, sMainCars[1].fStartHeading)
				SET_ENTITY_COLLISION(vehTriggerCar, FALSE)

				SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTriggerCar)
				
				iCurrentEvent = 0
				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING		
			IF IS_VEHICLE_DRIVEABLE(vehTriggerCar)
				IF iCurrentEvent = 0
					INITIALISE_UBER_PLAYBACK(strCarrec, iCarrec)
					LOAD_CHASE_UBER_DATA()
					SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
					SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
					fUberPlaybackDensitySwitchOffRange = 200.0

					iCurrentEvent++
				ELIF iCurrentEvent = 1
					REQUEST_VEHICLE_RECORDING(iCarrec, strCarrec)
					REQUEST_VEHICLE_RECORDING(iCamCarrec, strCarrec)
				
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrec, strCarrec)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCamCarrec, strCarrec)
						SET_ENTITY_VISIBLE(vehDebugFov, FALSE)
						SET_CAR_AT_PLAYBACK_POSITION(vehTriggerCar, iCarrec, 0.0, TRUE, FALSE)
						
						IF bDebugFreeModeCamera
							START_CAM_RECORDING(sCamData, vehDebug[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0.0, 0.0, 0.0>>, 25.0, strCarrec, iRecordCarrec, TRUE)
						ELSE
							IF IS_VEHICLE_DRIVEABLE(vehDebugCamCar)
								START_PLAYBACK_RECORDED_VEHICLE(vehDebugCamCar, iCamCarrec, strCarrec)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehDebugCamCar, fStartTime)
								SET_ENTITY_COLLISION(vehDebugCamCar, FALSE)
							ENDIF
						
							START_CAM_RECORDING_RELATIVE_TO_ENTITY(sCamData, vehDebugCamCar, vehDebug[0], vOffset, <<0.0, 0.0, 0.0>>, 30.0, strCarrec, iRecordCarrec, 
															   		TRUE, TRUE, FALSE, TRUE)
						ENDIF

						iCurrentEvent++
					ENDIF		
				ELIF iCurrentEvent = 2
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTriggerCar)	
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
						CREATE_ALL_WAITING_UBER_CARS()
						FLOAT fPlaybackTime = GET_TIME_POSITION_IN_RECORDING(vehTriggerCar)
						
						IF bDebugFreeModeCamera
							//Currently using the free camera as an offset from the recording in a future/past position
							VECTOR vRecOffset = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iCarrec, fPlaybackTime - 1000.0, strCarrec)
							VECTOR vTriggerPos = GET_ENTITY_COORDS(vehTriggerCar)
							VECTOR vTriggerOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTriggerCar, <<1.0, 0.0, 5.0>>)
							vRecOffset += (vTriggerOffset - vTriggerPos)
							sCamData.v_current_pos = vRecOffset
							sCamData.v_desired_pos = vRecOffset
						ENDIF
						
						PRELOAD_ALL_CHASE_RECORDINGS(fPlaybackTime)
						DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
						UPDATE_UBER_PLAYBACK(vehTriggerCar, 1.0)
						UPDATE_CAM_RECORDING(sCamData)
						
						IF IS_VEHICLE_DRIVEABLE(vehDebugCamCar)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDebugCamCar)
								SET_PLAYBACK_SPEED(vehDebugCamCar, 1.66666) //Set-piece was recorded at 0.6 for the uber
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
						STOP_CAM_RECORDING(sCamData)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
	
	
	PROC PLAYBACK_CAMERA()
		INT iCarrec = CARREC_BUDDY
		INT i = 0
		INT iCamRecMixdown
		
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		//INT i_fov_rec = 999
		//Try recording on bikes to squeeze into spots
		IF eSectionStage = SECTION_STAGE_SETUP
			REQUEST_MODEL(modelMainCar1)
		
			IF HAS_MODEL_LOADED(modelMainCar1)
				REMOVE_ALL_BLIPS()
			
				vehTriggerCar = CREATE_VEHICLE(modelMainCar1, sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
				
				IF NOT bDebugPlayMixdown
					REPEAT COUNT_OF(iCamRecs) i
						IF iCamRecs[i] != 0
							vehDebug[i] = CREATE_VEHICLE(modelMainCar1, sMainCars[1].vStartPos + <<3.0 * i, 0.0, 0.0>>, sMainCars[1].fStartHeading)
						ENDIF
					ENDREPEAT
					
					IF bDebugRecordEdit
						vehDebugCamCar = CREATE_VEHICLE(modelMainCar1, sMainCars[1].vStartPos + <<3.0, 0.0, 5.0>>, sMainCars[1].fStartHeading)
					ENDIF
				ELSE
					vehDebugCamCar = CREATE_VEHICLE(modelMainCar1, sMainCars[1].vStartPos + <<3.0, 0.0, 0.0>>, sMainCars[1].fStartHeading)
				ENDIF
				
				//SET_ENTITY_COLLISION(vehTriggerCar, FALSE)

				SET_MODEL_AS_NO_LONGER_NEEDED(modelMainCar1)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehTriggerCar)
				
				iCurrentEvent = 0
				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			//SET_USE_HI_DOF()
		
			IF IS_VEHICLE_DRIVEABLE(vehTriggerCar)
				IF iCurrentEvent = 0
					INITIALISE_UBER_PLAYBACK(strCarrec, iCarrec)
					SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
					SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
					fUberPlaybackDensitySwitchOffRange = 200.0
					
					IF bDebugPlayMixdown
						LOAD_CHASE_UBER_DATA()
					ENDIF

					iCurrentEvent++
				ELIF iCurrentEvent = 1
					REQUEST_VEHICLE_RECORDING(iCarrec, strCarrec)
					REQUEST_CAM_RECORDING(iCamRecs[0], strCarrec)
					REQUEST_CAM_RECORDING(iCamRecs[1], strCarrec)
					REQUEST_CAM_RECORDING(iCamRecs[2], strCarrec)
					REQUEST_CAM_RECORDING(iCamRecs[3], strCarrec)
					REQUEST_CAM_RECORDING(iCamRecs[4], strCarrec)
					REQUEST_CAM_RECORDING(iCamRecs[5], strCarrec)
				
					BOOL bRecsLoaded = FALSE
					
					IF HAS_CAM_RECORDING_LOADED(iCamRecs[0], strCarrec)
					AND HAS_CAM_RECORDING_LOADED(iCamRecs[1], strCarrec)
					AND HAS_CAM_RECORDING_LOADED(iCamRecs[2], strCarrec)
					AND HAS_CAM_RECORDING_LOADED(iCamRecs[3], strCarrec)
					AND HAS_CAM_RECORDING_LOADED(iCamRecs[4], strCarrec)
					AND HAS_CAM_RECORDING_LOADED(iCamRecs[5], strCarrec)
						bRecsLoaded = TRUE
					ENDIF
				
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrec, strCarrec)
					AND bRecsLoaded
						SET_CAR_AT_PLAYBACK_POSITION(vehTriggerCar, iCarrec, 0.0, TRUE, FALSE)
							
						camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
						SHAKE_CAM(camCutscene, "SKY_DIVING_SHAKE", 0.15)
						SET_CAM_MOTION_BLUR_STRENGTH(camCutscene, 0.01)
						SET_CAM_FAR_DOF(camCutscene, 100.0)
						SET_CAM_NEAR_DOF(camCutscene, 2.0)
						SET_CAM_FOV(camCutscene, 25.0)
						
						IF NOT bDebugPlayMixdown
							REPEAT COUNT_OF(iCamRecs) i
								IF iCamRecs[i] != 0
									START_CAM_PLAYBACK(camCutscene, vehDebug[i], strCarrec, iCamRecs[i])
								ENDIF
							ENDREPEAT
							
							IF bDebugRecordEdit
								START_CAM_RECORDING(sCamData, vehDebugCamCar, GET_ENTITY_COORDS(vehDebug[0]), GET_ENTITY_ROTATION(vehDebug[0]), 
													25.0, strCarrec, iCamRecMixdown, TRUE)
							ENDIF
						ELSE
							START_CAM_PLAYBACK(camCutscene, vehDebugCamCar, strCarrec, iCamRecs[0])
							iDebugCurrentCam = 0
							iCurrentCinematicCam = 0
							fNextActionCamFov = 0.0
						ENDIF
							
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						DISPLAY_RADAR(FALSE)
						iCurrentEvent++
					ENDIF		
				ELIF iCurrentEvent = 2
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTriggerCar)
						FLOAT fPlaybackTime = GET_TIME_POSITION_IN_RECORDING(vehTriggerCar)
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
						CREATE_ALL_WAITING_UBER_CARS()
						
						PRELOAD_ALL_CHASE_RECORDINGS(GET_TIME_POSITION_IN_RECORDING(vehTriggerCar))
						DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
						UPDATE_UBER_PLAYBACK(vehTriggerCar, 1.0)
						
						IF bDebugPlayEdit OR bDebugRecordEdit
							//CAM 0 (901, car is just offset) good spots:
							//5000.0 - 15000.0
							//16000.0 - 26000.0 (basic, but looks okay over jump)
							//55000.0 - 61000.0 (basic, but guaranteed to miss props)
							//65000.0 - 72000.0
							//117000.0 - 122000.0
							//
							//CAM 3 (904, same as cam 2 but more stable)
							//18000.0 - 22500.0 (fov out)
							//29000.0 - 41000.0
							//47500.0 - 53000.0
							//62000.0 - 65000.0
							//82000.0 - 88000.0
							//99000.0 - 109000.0
							//123000.0 - 126500.0
							//130000.0 - 132500.0
							//137000.0 - 141000.0
							//145000.0 - 151000.0 (fov out)
							//
							//CAM 4 (905, car in front)
							//2500.0 - 9000.0
							//15000.0 - 25000.0 (fov in)
							//41000.0 - 46000.0 (fov in)
							//50000.0 - 52000.0*
							//56000.0 - 62000.0
							//72000.0 - 81000.0 (fov in)
							//97500.0 - 120000.0
							//126000.0 - 132000.0
							//136000.0 - 151000.0 (includes pillars bit)
							//
							//CAM 5 (helicopter slightly ahead)
							//6000.0 - 12000.0
							//16000.0 - 22000.0
							//47000.0 - 55000.0
							//60000.0 - 63000.0 (backup)
							//66000.0 - 72000.0 (backup)
							//
							//CAM 6 (chopper low)
							//32000.0 - 35500.0
							//89000.0 - 98000.0* (nice jumps, fov in)
							//104500.0 - 107500.0 (nice intersection jump)
							//Some good backup after this on windy bits
							//
							//
							//CAM 7 (908, left side)
							//5400.0 - 8400.0 
							//15000.0 - 24500.0 (especially near end, does a nice pan)
							//28000.0 - 32000.0 - 40000.0 - 44000.0
							//46000.0 - 48000.0 (fov out)
							//53000.0 - 62500.0 (nice film studio pan)
							//74000.0 - 78500.0
							//84500.0 - 89000.0 (good jump)
							//94000.0 - 97000.0 (jump)
							//99000.0 - 100000.0 - 108000.0
							//108000.0 - 113500.0
							//114000.0 - 119000.0 (nice shot of weaving)
							//122000.0 - 125000.0 (nice bus shot)
							//135000.0 - 139000.0 (nice pillars through turn)
							//140000.0 - 145500.0 (pillars and jump, pillars a bit off, may be able to cut).
							//152000.0 - end (best end shot so far)
							//
							//CAM 8 (909, right side)
							//6000.0 - 11500.0 (nice weaving)
							//15000.0 - 29000.0 (nice angle, also best version of jump near end)
							//32000.0 - 38000.0 (fov out a bit)
							//38000.0 - 43000.0
							//48000.0 - 50000.0
							//69000.0 - 72500.0 (fov out)
							//80000.0 - 84000.0 (may need fov out)
							//89000.0 - 93000.0 (may need fov out)
							//95000.0 - 98000.0 
							//101500.0 - 114000.0 (nice intersection jump)
							//116500.0 - 120000.0
							//129000.0 - 132000.0
							//143500.0 - 146500.0 (might be good jump if fov out)
							IF fPlaybackTime < 8400.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 11500.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 15000.0
								iDebugCurrentCam = 2
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 19000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 24000.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 29000.0
								iDebugCurrentCam = 3 
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 32000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 38000.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 38.0)
							ELIF fPlaybackTime < 41000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 45500.0
								iDebugCurrentCam = 2
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 48000.0
								iDebugCurrentCam = 4 
								SET_CAM_FOV(camCutscene, 32.0)
							ELIF fPlaybackTime < 50300.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 56000.0
								iDebugCurrentCam = 2
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 62000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 64500.0
								iDebugCurrentCam = 2
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 70000.0
								iDebugCurrentCam = 0
								SET_CAM_FOV(camCutscene, 31.0)
							ELIF fPlaybackTime < 72500.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 35.0)
							ELIF fPlaybackTime < 78500.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 86100.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 34.0)
							ELIF fPlaybackTime < 90000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 95000.0
								iDebugCurrentCam = 3
								SET_CAM_FOV(camCutscene, 12.0)
							ELIF fPlaybackTime < 98700.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 101000.0
								iDebugCurrentCam = 1
								SET_CAM_FOV(camCutscene, 30.0)
							ELIF fPlaybackTime < 104500.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 27.0)
							ELIF fPlaybackTime < 108500.0
								iDebugCurrentCam = 3
								SET_CAM_FOV(camCutscene, 22.0)
							ELIF fPlaybackTime < 114000.0
								iDebugCurrentCam = 8
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 119000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 122000.0
								iDebugCurrentCam = 3
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 125000.0
								iDebugCurrentCam = 4 //Can't use 7 to enter the car park, so needs to end earlier
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 132500.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 143000.0
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ELIF fPlaybackTime < 147000.0
								iDebugCurrentCam = 5
								SET_CAM_FOV(camCutscene, 45.0)
							ELSE
								iDebugCurrentCam = 4
								SET_CAM_FOV(camCutscene, 25.0)
							ENDIF
						ELIF bDebugPlayMixdown
							UPDATE_CHASE_CAM(vehDebugCamCar, camCutscene, fPlaybackTime)
						ELSE
							IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
								iDebugCurrentCam++
								
								IF iCamRecs[iDebugCurrentCam] = 0
									iDebugCurrentCam = 2
								ENDIF
							ENDIF
						ENDIF
						
						IF bDebugPlayMixdown
							UPDATE_CAM_PLAYBACK(camCutscene, vehDebugCamCar, 1.0)
						ELSE
							UPDATE_CAM_PLAYBACK(camCutscene, vehDebug[iDebugCurrentCam], 1.0)
						ENDIF
						//DISPLAY_CAM_PLAYBACK(vehDebug)
						
						IF bDebugRecordEdit
							sCamData.v_current_pos = GET_ENTITY_COORDS(vehDebug[iDebugCurrentCam])
							sCamData.v_desired_pos = GET_ENTITY_COORDS(vehDebug[iDebugCurrentCam])
							sCamData.v_current_rot = GET_ENTITY_ROTATION(vehDebug[iDebugCurrentCam])
							UPDATE_CAM_RECORDING(sCamData, FALSE)
							
							IF IS_BUTTON_JUST_PRESSED(PAD1, DPADDOWN)
								STOP_CAM_RECORDING(sCamData)
							ENDIF
						ENDIF
						
						SET_TEXT_SCALE(0.5, 0.5)
						TEXT_LABEL_63 strTime = "Time: "
						strTime += GET_STRING_FROM_FLOAT(fPlaybackTime)
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", strTime)
					
						SET_TEXT_SCALE(0.5, 0.5)
						TEXT_LABEL_23 str_cam = "Cam: "
						str_cam += iDebugCurrentCam
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.15, "STRING", str_cam)
					ELSE
						iCurrentEvent = 1
						
						
						
						CLEANUP_UBER_PLAYBACK(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

	PROC RECORD_TRUCK_AND_TRAILER()
		VECTOR vTruckPos = <<-1138.3113, -811.2895, 15.7751>>//<<-194.5744, -883.5518, 28.3464>>
		FLOAT fTruckHeading = 307.1786 //72.8531
		MODEL_NAMES modelTruck = PACKER
		MODEL_NAMES modelTrailer = TANKER //TR2
		CONST_INT CARREC_TRUCK			992//990
		CONST_INT CARREC_TRUCK_REAR		993//991
		
		/*SetPieceCarPos[6] = <<-1138.3113, -811.2895, 15.7751>>
		SetPieceCarQuatX[6] = 0.0298
		SetPieceCarQuatY[6] = -0.0129
		SetPieceCarQuatZ[6] = -0.4436
		SetPieceCarQuatW[6] = 0.8956
		SetPieceCarRecording[6] = 14
		SetPieceCarStartime[6] = 68000.0000
		SetPieceCarRecordingSpeed[6] = 1.0000
		SetPieceCarModel[6] = packer*/
		
		FLOAT fQuatX, fQuatY, fQuatZ, fQuatW
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

		SWITCH iCurrentEvent
			CASE 0
				CLEAR_AREA(vTruckPos, 200.0, TRUE)
			
				IF NOT IS_VEHICLE_DRIVEABLE(vehDebug[0])
					REQUEST_MODEL(modelTruck)
					REQUEST_MODEL(modelTrailer)
					
					IF HAS_MODEL_LOADED(modelTruck)
					AND HAS_MODEL_LOADED(modelTrailer)
						vehDebug[0] = CREATE_VEHICLE(modelTruck, vTruckPos, fTruckHeading)
						vehTransporter = CREATE_VEHICLE(modelTrailer, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehDebug[0], <<0.0, -10.0, 0.0>>), GET_ENTITY_HEADING(vehDebug[0]))
						
						SET_MODEL_AS_NO_LONGER_NEEDED(modelTruck)
						SET_MODEL_AS_NO_LONGER_NEEDED(modelTrailer)
					ENDIF
				ELSE
					SET_ENTITY_COORDS(vehDebug[0], vTruckPos)
					SET_ENTITY_HEADING(vehDebug[0], fTruckHeading)
					
					IF NOT IS_ENTITY_DEAD(vehTransporter)
						SET_ENTITY_COORDS(vehTransporter, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehDebug[0], <<0.0, -10.0, 0.0>>))
						SET_ENTITY_HEADING(vehTransporter, GET_ENTITY_HEADING(vehDebug[0]))
						
						ATTACH_VEHICLE_TO_TRAILER(vehDebug[0], vehTransporter)
						DISABLE_TRAILER_BREAKING_FROM_VEHICLE(vehTransporter, TRUE)
					ENDIF
					
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehDebug[0])
					
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 1
				IF IS_VEHICLE_DRIVEABLE(vehDebug[0])
				AND NOT IS_ENTITY_DEAD(vehTransporter)
					IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
						START_RECORDING_VEHICLE(vehDebug[0], CARREC_TRUCK, strCarrec, TRUE)
						START_RECORDING_VEHICLE(vehTransporter, CARREC_TRUCK_REAR, strCarrec, TRUE)
						PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "RECORDING STARTED", DEFAULT_GOD_TEXT_TIME, 0)
						
						GET_ENTITY_QUATERNION(vehDebug[0], fQuatX, fQuatY, fQuatZ, fQuatW)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_FLOAT_TO_DEBUG_FILE(fQuatX)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_FLOAT_TO_DEBUG_FILE(fQuatY)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_FLOAT_TO_DEBUG_FILE(fQuatZ)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_FLOAT_TO_DEBUG_FILE(fQuatW)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						iCurrentEvent++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF IS_VEHICLE_DRIVEABLE(vehDebug[0])
				AND NOT IS_ENTITY_DEAD(vehTransporter)
					IF IS_BUTTON_PRESSED(PAD1, LEFTSHOULDER1)
						IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehDebug[0])
							APPLY_FORCE_TO_ENTITY(vehTransporter, APPLY_TYPE_FORCE, <<-10.0, 0.0, 0.0>>, <<0.0, 0.0, 5.0>>, 0, FALSE, TRUE, TRUE)
						ELSE
							APPLY_FORCE_TO_ENTITY(vehTransporter, APPLY_TYPE_FORCE, <<-10.0, 0.0, 1.0>>, <<0.0, -20.0, 0.0>>, 0, TRUE, TRUE, TRUE)
						ENDIF
					ENDIF
				
					IF IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
						DETACH_VEHICLE_FROM_TRAILER(vehDebug[0])
					ENDIF
				
					IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
						STOP_RECORDING_VEHICLE(vehDebug[0])
						STOP_RECORDING_VEHICLE(vehTransporter)
						
						iCurrentEvent = 0
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDPROC
	
	PROC TEST_RESPAWN_CUTSCENE()
		SWITCH iCurrentEvent
			CASE 0
				//DO_FADE_OUT_WITH_WAIT()
			
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<342.6215, -1397.4391, 31.507>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 50.9027)
				
				//LOAD_SCENE(<<342.6215, -1397.4391, 31.507>>)
				
				camCutscene = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<270.321259,-1387.484253,36.111080>>,<<14.247233,0.000000,-102.191833>>,53.342339, TRUE)
				SET_CAM_PARAMS(camCutscene, <<270.513092,-1387.525757,35.338066>>,<<14.247233,0.000000,-102.191833>>,53.342339, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				SETTIMERB(0)
				
				//DO_FADE_IN_WITH_WAIT()
				
				iCurrentEvent++
			BREAK
			
			CASE 1
				IF TIMERB() > 3000
					SET_CAM_PARAMS(camCutscene,<<336.940125,-1395.949097,35.062832>>,<<6.609568,-0.000528,-113.654175>>,50.000000)
					SET_CAM_PARAMS(camCutscene,<<338.378937,-1396.828003,32.938160>>,<<-0.780622,-0.000000,-9.172981>>,50.000000, 4000)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<338.7186, -1394.0708, 31.5032>>, PEDMOVE_WALK, -1)
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 2
				IF TIMERB() > 2500
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 2000, FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SETTIMERB(0)
					iCurrentEvent++
				ENDIF
			BREAK
			
			CASE 3
				IF TIMERB() > 1000
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					iCurrentEvent++
				ENDIF
			BREAK
		ENDSWITCH
	ENDPROC
	
	//Used after re-recording a chase route: compares the positions of the recordings at different times so set-pieces can be easily adjusted to match
	//the new timings.
	PROC COMPARE_TRIGGER_RECORDINGS(MODEL_NAMES modelCar, INT iCarrecOld, INT iCarrecNew, STRING strCarrecName)	
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
		IF eSectionStage = SECTION_STAGE_SETUP
			REQUEST_MODEL(modelCar)
		
			IF HAS_MODEL_LOADED(modelCar)
				REMOVE_ALL_BLIPS()
				
			
				vehDebug[0] = CREATE_VEHICLE(modelCar, sMainCars[0].vStartPos, sMainCars[0].fStartHeading)
				SET_ENTITY_COLLISION(vehDebug[0], FALSE)
				
				vehDebug[1] = CREATE_VEHICLE(modelCar, sMainCars[1].vStartPos, sMainCars[1].fStartHeading)
				SET_ENTITY_COLLISION(vehDebug[1], FALSE)
				
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehDebug[0])
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(modelCar)
				
				iCurrentEvent = 0
				eSectionStage = SECTION_STAGE_RUNNING
			ENDIF
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)
			SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(0.0)
		
			IF IS_VEHICLE_DRIVEABLE(vehDebug[0])
			AND IS_VEHICLE_DRIVEABLE(vehDebug[1])
				IF iCurrentEvent = 0
					REQUEST_VEHICLE_RECORDING(iCarrecOld, strCarrecName)
					REQUEST_VEHICLE_RECORDING(iCarrecNew, strCarrecName)
					
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecOld, strCarrecName)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iCarrecNew, strCarrecName)
						START_PLAYBACK_RECORDED_VEHICLE(vehDebug[0], iCarrecOld, strCarrecName)
						START_PLAYBACK_RECORDED_VEHICLE(vehDebug[1], iCarrecNew, strCarrecName)
						
						iCurrentEvent++
					ENDIF
				ELIF iCurrentEvent = 1
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDebug[0])
					AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDebug[1])
						SET_PLAYBACK_SPEED(vehDebug[0], 0.0)
						SET_PLAYBACK_SPEED(vehDebug[1], 0.0)
						
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehDebug[0], fDebugOldTriggerTime)
						SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehDebug[1], fDebugNewTriggerTime)
					ELSE
						iCurrentEvent--
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

	PROC UPDATE_WIDGETS()
		IF bDebugRecordTraffic
			IF eMissionStage = STAGE_CHOOSE_CAR
				eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
				MISSION_CLEANUP()
			
				eMissionStage = STAGE_RECORD_TRAFFIC
				bDebugRecordTraffic = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF			
		ENDIF
		
		IF bDebugRecordSetpieces
			IF eMissionStage = STAGE_CHOOSE_CAR
				eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
				MISSION_CLEANUP()
			
				eMissionStage = STAGE_RECORD_SET_PIECES
				bDebugRecordSetpieces = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF	
		ENDIF
		
		IF bDebugRedoTrigger
			IF eMissionStage = STAGE_CHOOSE_CAR
				eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
				MISSION_CLEANUP()
			
				eMissionStage = STAGE_REDO_TRIGGER_RECORDING
				bDebugRedoTrigger = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
		
		IF bDebugPlaybackCamera
			IF eMissionStage = STAGE_CHOOSE_CAR
				eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
				MISSION_CLEANUP()
			
				eMissionStage = STAGE_PLAYBACK_CAMERA
				bDebugPlaybackCamera = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
		
		IF bDebugRecordCamera
			IF eMissionStage = STAGE_CHOOSE_CAR
				eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
				MISSION_CLEANUP()
			
				eMissionStage = STAGE_RECORD_CAMERA
				bDebugRecordCamera = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
		
		IF bDebugRecordTrailer
			IF eMissionStage = STAGE_CHOOSE_CAR
				eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
				MISSION_CLEANUP()
			
				eMissionStage = STAGE_DEBUG_RECORD_TRAILER
				bDebugRecordTrailer = FALSE
				eSectionStage = SECTION_STAGE_SETUP
			ENDIF
		ENDIF
		
		IF bDebugTestRespawnCutscene
			eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
			MISSION_CLEANUP()
			
			eMissionStage = STAGE_DEBUG_RESPAWN_CAM_TEST
			bDebugTestRespawnCutscene = FALSE
			eSectionStage = SECTION_STAGE_RUNNING
			iCurrentEvent = 0
		ENDIF
		
		vDebugVectorResult = vDebugVectorA + vDebugVectorB
		
		IF bDebugUseNewAbilityButton
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL_ABILITY_FRANKLIN)
			
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
			OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
				fDebugTimeSinceLeftStickPressed += GET_FRAME_TIME()
			ELSE
				fDebugTimeSinceLeftStickPressed = 0.0
			ENDIF
			
			IF fDebugTimeSinceLeftStickPressed < 1.0
				//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
			ENDIF
			
			IF bDebugSticksReleased
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LS)
				AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RS)
					IF IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
						IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
							SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
						ELSE
							SPECIAL_ABILITY_ACTIVATE(PLAYER_ID())
						ENDIF
					ENDIF
					
					bDebugSticksReleased = FALSE
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_LS)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RS)
					bDebugSticksReleased = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bDebugCompareTriggerRecordings
			eSectionStage = SECTION_STAGE_JUMPING_TO_STAGE
			MISSION_CLEANUP()
			
			eMissionStage = STAGE_DEBUG_COMPARE_RECORDINGS
			bDebugCompareTriggerRecordings = FALSE
			eSectionStage = SECTION_STAGE_SETUP
		ENDIF
		
		IF bDebugLoadHorizonCloud
			LOAD_CLOUD_HAT("horizon")
			PRINT_NOW("horizon", DEFAULT_GOD_TEXT_TIME, 0)
			bDebugLoadHorizonCloud = FALSE
		ELIF bDebugLoadWispyCloud
			LOAD_CLOUD_HAT("wispy")
			PRINT_NOW("wispy", DEFAULT_GOD_TEXT_TIME, 0)
			bDebugLoadWispyCloud = FALSE
		ELIF bDebugLoadContrailsCloud
			LOAD_CLOUD_HAT("contrails")
			PRINT_NOW("contrails", DEFAULT_GOD_TEXT_TIME, 0)
			bDebugLoadContrailsCloud = FALSE
		ELIF bDebugLoadPuffsCloud
			LOAD_CLOUD_HAT("puffs")
			PRINT_NOW("puffs", DEFAULT_GOD_TEXT_TIME, 0)
			bDebugLoadPuffsCloud = FALSE
		ELIF bDebugLoadAltoCloud
			LOAD_CLOUD_HAT("altostratus")
			PRINT_NOW("altostratus", DEFAULT_GOD_TEXT_TIME, 0)
			bDebugLoadAltoCloud = FALSE
		ELIF bDebugLoadStormyCloud
			LOAD_CLOUD_HAT("stormy 01")
			PRINT_NOW("stormy 01", DEFAULT_GOD_TEXT_TIME, 0)
			bDebugLoadStormyCloud = FALSE
		ENDIF
		
		IF bDebugUnloadCloudHats
			UNLOAD_ALL_CLOUD_HATS()
			bDebugUnloadCloudHats = FALSE
		ENDIF
		
		IF bDebugFerrisWheelTest
			//Scene rotations: (180.0, 250.0) (300.0, 210.0)
			IF NOT DOES_ENTITY_EXIST(pedCreditsFerrisWheel[0])
				REQUEST_MODEL(A_F_Y_BEACH_01)
				REQUEST_ANIM_DICT(strFerrisWheelAnims)
				
				IF HAS_MODEL_LOADED(A_F_Y_BEACH_01)
				AND SETUP_REQ_FERRIS_WHEEL()
				AND HAS_ANIM_DICT_LOADED(strFerrisWheelAnims)
					REMOVE_IPL("ferris_finale_Anim")

					pedCreditsFerrisWheel[0] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEACH_01, <<-1664.9, -1148.1, 26.3>>)
					SET_ENTITY_INVINCIBLE(pedCreditsFerrisWheel[0], TRUE)
					
					iSyncSceneFerrisWheel[0] = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSyncSceneFerrisWheel[0], objFerrisCars[0], 0)
					TASK_SYNCHRONIZED_SCENE(pedCreditsFerrisWheel[0], iSyncSceneFerrisWheel[0], strFerrisWheelAnims, "Stand_Idle_1_PEDA", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneFerrisWheel[0], TRUE)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedCreditsFerrisWheel[0])
					IF bDebugFerrisWheelReattach
						INT iClosestCar = GET_CLOSEST_FERRIS_CAR_TO_COORDS(GET_FINAL_RENDERED_CAM_COORD())
					
						CLEAR_PED_TASKS_IMMEDIATELY(pedCreditsFerrisWheel[0])
						iSyncSceneFerrisWheel[0] = CREATE_SYNCHRONIZED_SCENE(vDebugFerrisWheelOffset, vDebugFerrisWheelRot)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSyncSceneFerrisWheel[0], objFerrisCars[iClosestCar], 0)
						
						IF iDebugWheelAnim = 0
							TASK_SYNCHRONIZED_SCENE(pedCreditsFerrisWheel[0], iSyncSceneFerrisWheel[0], strFerrisWheelAnims, "Stand_Idle_1_PEDA", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
													SYNCED_SCENE_DONT_INTERRUPT, RBF_FALLING | RBF_IMPACT_OBJECT)
						ELIF iDebugWheelAnim = 1
							TASK_SYNCHRONIZED_SCENE(pedCreditsFerrisWheel[0], iSyncSceneFerrisWheel[0], strFerrisWheelAnims, "Stand_Idle_1_PEDB", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
													SYNCED_SCENE_DONT_INTERRUPT, RBF_FALLING | RBF_IMPACT_OBJECT)
						ELIF iDebugWheelAnim = 2
							TASK_SYNCHRONIZED_SCENE(pedCreditsFerrisWheel[0], iSyncSceneFerrisWheel[0], strFerrisWheelAnims, "Stand_Idle_2_PEDA", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
													SYNCED_SCENE_DONT_INTERRUPT, RBF_FALLING | RBF_IMPACT_OBJECT)
						ELIF iDebugWheelAnim = 3
							TASK_SYNCHRONIZED_SCENE(pedCreditsFerrisWheel[0], iSyncSceneFerrisWheel[0], strFerrisWheelAnims, "Stand_Idle_2_PEDB", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
													SYNCED_SCENE_DONT_INTERRUPT, RBF_FALLING | RBF_IMPACT_OBJECT)
						ENDIF
						
						SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneFerrisWheel[0], TRUE)
						
						bDebugFerrisWheelReattach = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

	PROC DO_DEBUG()
		UPDATE_WIDGETS()
		
		//Reset any skipping from the previous frame
		IF eSectionStage = SECTION_STAGE_SKIP
			eSectionStage = SECTION_STAGE_RUNNING
		ENDIF
		
		IF eSectionStage = SECTION_STAGE_RUNNING
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				PRINTLN("armenian1.sc - Player used an S-pass.")
			
				MISSION_PASSED(TRUE)
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_FAILED(FAILED_GENERIC)
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				PRINTLN("Armenian1.sc - Used J-skip")
			
				eSectionStage = SECTION_STAGE_SKIP
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				INT iCurrentStage = ENUM_TO_INT(eMissionStage)
				
				IF iCurrentStage > 0	
					PRINTLN("Armenian1.sc - Used P-skip: ", iCurrentStage)
					
					MISSION_STAGE eStage = INT_TO_ENUM(MISSION_STAGE, iCurrentStage - 1)
					JUMP_TO_STAGE(eStage, TRUE)
				ENDIF
			ENDIF
			
			IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDebugJumpStage)
				PRINTLN("Armenian1.sc - Used Z-skip: ", iDebugJumpStage)
			
				MISSION_STAGE eStage = INT_TO_ENUM(MISSION_STAGE, iDebugJumpStage)
				JUMP_TO_STAGE(eStage, TRUE)
			ENDIF
			
			iDebugNumPrintsThisFrame = 0
		ENDIF
	ENDPROC
#ENDIF

SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_FADE_IN_AFTER_DEATH_ARREST(FALSE)
		Mission_Flow_Mission_Force_Cleanup()
		eSectionStage = SECTION_STAGE_RUNNING //If the mission is in the middle of a skip cleanup (e.g. waiting for a cutscene to finish) Force cleanup could break.
		g_eArm1PrestreamDenise = ARM1_PD_3_release
		MISSION_CLEANUP()
	ENDIF

	MISSION_SETUP()

	//TODO 547497: Set the clock and date for the start of the game.
	SET_CLOCK_TIME(8, 0, 0)
	SET_CLOCK_DATE(6, JUNE, 2009)

	IF Is_Replay_In_Progress()
		INT iStage = Get_Replay_Mid_Mission_Stage()
		
		IF g_bShitskipAccepted
			iStage++
		ENDIF
		
		IF iStage = 0
			CLEAR_SCENARIO_SPAWN_HISTORY() //2071893 - This ensures the cat scenario is there on a retry.
			JUMP_TO_STAGE(STAGE_CHOOSE_CAR)
		ELIF iStage = CHECKPOINT_MID_CHASE
			JUMP_TO_STAGE(STAGE_CHASE_MID_POINT)
		ELIF iStage = CHECKPOINT_LOSE_COPS
			IF g_bShitskipAccepted
				JUMP_TO_STAGE(STAGE_COPS_ARRIVE_CUTSCENE)
			ELSE
				JUMP_TO_STAGE(STAGE_GO_TO_GARAGE)
			ENDIF
		ELIF iStage = CHECKPOINT_GO_HOME
			IF g_bShitskipAccepted
				JUMP_TO_STAGE(STAGE_SHOWROOM_CUTSCENE)
			ELSE
				JUMP_TO_STAGE(STAGE_GO_TO_HOUSE)
			ENDIF
		ELIF iStage > CHECKPOINT_GO_HOME
			IF g_bShitskipAccepted
				JUMP_TO_STAGE(STAGE_HOUSE_CUTSCENE)
			ELSE
				JUMP_TO_STAGE(STAGE_HOUSE_CUTSCENE)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("armenian1.sc - Player used a replay to stage: ", iStage)
		#ENDIF
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		bUsedACheckpoint = TRUE
	ELSE
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "OPENING_CUTSCENE") 
	ENDIF

	WHILE (TRUE)
		WAIT(0)

		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FranklinAndLamar")

		//Reset the checkpoint flag once the mission is running again. This flag is used to determine if we jumped using a checkpoint or via Z-skip,
		//as some bits of setup are only done when debug skipping.
		IF bUsedACheckpoint
			IF eSectionStage = SECTION_STAGE_RUNNING
				bUsedACheckpoint = FALSE
			ENDIF
		ENDIF

		//Check if the player has used first-person mode yet.
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON 
			OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON 
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW, TRUE)
			ENDIF
		ENDIF

		IF eMissionStage < STAGE_HOUSE_CUTSCENE
			IF DOES_ENTITY_EXIST(sMainCars[0].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
					IF NOT IS_VEHICLE_DRIVEABLE(sMainCars[1].veh)
						MISSION_FAILED(FAILED_DESTROYED_BOTH_CARS)
					ELSE
						MISSION_FAILED(FAILED_DESTROYED_A_CAR)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sMainCars[1].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(sMainCars[1].veh)
					IF NOT IS_VEHICLE_DRIVEABLE(sMainCars[0].veh)
						MISSION_FAILED(FAILED_DESTROYED_BOTH_CARS)
					ELSE
						MISSION_FAILED(FAILED_DESTROYED_A_CAR)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(sMainCars[iPlayersCar].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					MISSION_FAILED(FAILED_DESTROYED_CAR)
				ENDIF
			ENDIF
		ENDIF
		
		//Stuck check for player's car
		IF eMissionStage > STAGE_START_CHASE_CUTSCENE
			IF DOES_ENTITY_EXIST(sMainCars[iPlayersCar].veh)
				IF IS_VEHICLE_DRIVEABLE(sMainCars[iPlayersCar].veh)
					IF IS_VEHICLE_PERMANENTLY_STUCK(sMainCars[iPlayersCar].veh)
						MISSION_FAILED(FAILED_CAR_STUCK)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Fail for destroying Franklin's car.
		IF eMissionStage < STAGE_HOUSE_CUTSCENE
			IF DOES_ENTITY_EXIST(vehFranklinsCar)
				IF IS_VEHICLE_DRIVEABLE(vehFranklinsCar)
					IF eMissionStage > STAGE_SHOWROOM_CUTSCENE
						IF IS_VEHICLE_PERMANENTLY_STUCK(vehFranklinsCar)
							MISSION_FAILED(FAILED_FRANKLINS_CAR_STUCK)
						ENDIF
					ENDIF
				ELSE
					MISSION_FAILED(FAILED_DESTROYED_FRANKLINS_CAR)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sLamar.ped)
			IF IS_PED_INJURED(sLamar.ped)
				MISSION_FAILED(FAILED_KILLED_BUDDY)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSimeon.ped)
			IF IS_PED_INJURED(sSimeon.ped)
				MISSION_FAILED(FAILED_KILLED_SIMEON)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sJimmy.ped)
			IF IS_PED_INJURED(sJimmy.ped)
				MISSION_FAILED(FAILED_KILLED_JIMMY)
			ENDIF
		ENDIF
		
		IF eMissionStage > STAGE_COPS_ARRIVE_CUTSCENE
			CHECK_SHOWROOM_FAIL()
		ENDIF
		
		//When the player reaches the garage unlock the stats screen: use a fake selector ped call.
		IF eMissionStage >= STAGE_GO_TO_HOUSE
			UPDATE_SELECTOR_HUD(sDummySelectorPeds)
		ENDIF
		
		//2255037 - If we've reached the lose cops stage then allow Franklin to unlock Michael's safehouse door.
		IF NOT bUnlockedMichaelsMansion
			IF eMissionStage >= STAGE_GO_TO_GARAGE
				CDEBUG1LN(DEBUG_MISSION, "[", GET_THIS_SCRIPT_NAME(), ".sc] Registering Franklin for AUTODOOR_MICHAEL_MANSION_GATE")
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
				bUnlockedMichaelsMansion = TRUE
			ENDIF
		ENDIF
		
		SWITCH eMissionStage
			CASE STAGE_OPENING_CUTSCENE
				OPENING_CUTSCENE()
			BREAK
			
			CASE STAGE_CHOOSE_CAR
				CHOOSE_CAR()
			BREAK
			
			CASE STAGE_START_CHASE_CUTSCENE
				//CHASE_START_CUTSCENE_2()
				CHASE_START_CUTSCENE_ANIMATED_CAMS()
			BREAK
			
			CASE STAGE_CHASE
			CASE STAGE_CHASE_MID_POINT
				CHASE_BUDDY()
			BREAK
			
			CASE STAGE_COPS_ARRIVE_CUTSCENE
				COPS_ARRIVE_CUTSCENE()
			BREAK
			
			CASE STAGE_GO_TO_GARAGE
				GO_TO_GARAGE()
			BREAK
			
			CASE STAGE_SHOWROOM_INTRO_CUTSCENE
				SHOWROOM_INTRO_CUTSCENE()
			BREAK
			
			CASE STAGE_MEET_SIMEON
				MEET_SIMEON()
			BREAK
			
			CASE STAGE_SHOWROOM_CUTSCENE
				SHOWROOM_CUTSCENE()
			BREAK
			
			CASE STAGE_GO_TO_HOUSE
				GO_TO_HOUSE()
			BREAK
			
			CASE STAGE_HOUSE_CUTSCENE
				HOUSE_CUTSCENE_COMBINED_VERSION()
			BREAK

			CASE STAGE_RECORD_TRAFFIC
				#IF IS_DEBUG_BUILD
					RECORD_CHASE_GHOST_AND_TRAFFIC()
				#ENDIF
			BREAK
			
			CASE STAGE_RECORD_SET_PIECES
				#IF IS_DEBUG_BUILD
					RECORD_CHASE_SET_PIECES()
				#ENDIF
			BREAK
			
			CASE STAGE_REDO_TRIGGER_RECORDING
				#IF IS_DEBUG_BUILD
					REDO_TRIGGER_RECORDING()
				#ENDIF
			BREAK
			
			CASE STAGE_RECORD_CAMERA
				#IF IS_DEBUG_BUILD
					RECORD_CAMERA()
				#ENDIF
			BREAK
			
			CASE STAGE_PLAYBACK_CAMERA
				#IF IS_DEBUG_BUILD
					PLAYBACK_CAMERA()
				#ENDIF
			BREAK
			
			CASE STAGE_DEBUG_RECORD_TRAILER
				#IF IS_DEBUG_BUILD
					RECORD_TRUCK_AND_TRAILER()
				#ENDIF
			BREAK
			
			CASE STAGE_DEBUG_RESPAWN_CAM_TEST
				#IF IS_DEBUG_BUILD
					TEST_RESPAWN_CUTSCENE()
				#ENDIF
			BREAK
			
			CASE STAGE_DEBUG_COMPARE_RECORDINGS
				#IF IS_DEBUG_BUILD
					COMPARE_TRIGGER_RECORDINGS(POLICE, 54, 108, "bb_chase")
				#ENDIF
			BREAK
		ENDSWITCH

		UPDATE_RAGE()
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG()
		#ENDIF	
	ENDWHILE
ENDSCRIPT		

