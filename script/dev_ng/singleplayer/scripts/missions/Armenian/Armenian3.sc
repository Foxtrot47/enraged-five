//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║				Author: Alan Litobarski				Date: 06/07/2010		 	║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║																				║
//║ 			Meet Michael - (Armenian3.sc)									║
//║ 																			║
//║ 			Franklin tries to take Michael's sons car. Michael stops 		║
//║ 			him and tells him to drive to near car showroom gives 			║
//║ 			him a slap gives his son a slap and finds out who the  			║
//║ 			car dealer is and then ram raids the car back into the  		║
//║ 			dealership and beats up the owner.								║
//║																				║
//╚═════════════════════════════════════════════════════════════════════════════╝

//═════════════════════════════════╡ HEADERS ╞═══════════════════════════════════

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_streaming.sch"
USING "commands_cutscene.sch"
USING "commands_vehicle.sch"
USING "commands_camera.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_object.sch"
USING "commands_task.sch"
USING "commands_misc.sch"
USING "commands_entity.sch"
USING "commands_event.sch"

USING "streamed_scripts.sch"
USING "cellphone_public.sch"
USING "flow_public_core_override.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "locates_public.sch"
USING "building_control_public.sch"
USING "replay_public.sch"
USING "chase_hint_cam.sch"
USING "help_at_location.sch"
USING "cutscene_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "script_heist.sch"
USING "shop_public.sch"
USING "vehicle_gen_public.sch"
USING "clearmissionarea.sch"
USING "tv_control_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//════════════════════════════════╡ VARIABLES ╞══════════════════════════════════

//Integers
INT iCutsceneStage

INT iDialogueStage = 0
INT iDialogueLineCount[5]
INT iDialogueTimer

INT iTimer

INT iFailTimer
INT iFailUprightTimer

INT iHelpTimer = -1

INT iInterruptTimer
INT iStoppedTimer = -1

INT iClimbTimer
INT iAlertTimer

INT iCarHealth = 1000
FLOAT iCarGasTank = 1000.0
FLOAT iCarEngineHealth = 1000.0

INT iAutowalkTimeout

//Vehicle Zoom Level
BOOL bVehicleZoomInOut
VEHICLE_ZOOM_LEVEL vehicleZoomLevel
BOOL bZoomLevel = FALSE
CAM_VIEW_MODE camViewModeOnFoot

//INT iGardenTimer
//INT iGardenPoint
//VECTOR vGardenPoints[7]
//FLOAT fGardenPoints[7]
//FLOAT fBlowStrenth

FLOAT fDistLastFrame
FLOAT fDistThisFrame

//Declare a TIMEOFDAY struct, a camera index and a stage tracker for the cutscene.
structTimelapse sTimelapse

//Bools
BOOL bVideoRecording

BOOL bInitStage
BOOL bCleanupStage
BOOL bRadar

BOOL bPinnedMansion
BOOL bPinnedGarage

BOOL bPassed	//Passed the mission

#IF IS_DEBUG_BUILD
BOOL bDebugAudio
#ENDIF

BOOL bTimeOfDayRunning
BOOL bTimeOfDayComplete	//Check TOD has finished

//Used for J-skipping and Mid Mission Replay
#IF IS_DEBUG_BUILD
	BOOL bAutoSkipping
#ENDIF

BOOL bSkipped
BOOL bReplaySkip
BOOL bShitSkip

BOOL bCutsceneSkipped	//Tracks if WAS_CUTSCENE_SKIPPED() returns TRUE

BOOL bJimmyTVPLayingGame
BOOL bProjectorTVDisabled

BOOL bPreloaded //For Preloaded Cutscene

BOOL bPlayerExitCar
BOOL bPlayerHasEnteredMansion

BOOL bCameraViewToggle	//Used to wait a frame for camera view toggle

//BOOL bCarDoorSlam
//INT iCarDoorSlam

//Vectors and Floats
VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>

VECTOR vIntroCutscenePos = <<-43.6345, -1110.6049, 25.9582>>
CONST_FLOAT fIntroCutsceneRot 178.9958
VECTOR vIntroCutsceneRot = <<0.0, 0.0, fIntroCutsceneRot>>

VECTOR vPlayerStart = <<-63.0336, -1107.3502, 25.3198>>
CONST_FLOAT fPlayerStart 113.3946

VECTOR vMichaelStart = <<-818.4653, 180.4985, 71.5139>>
CONST_FLOAT fMichaelStart 26.8488

VECTOR vSonStart = <<-806.9441, 171.6912, 75.3206>>
CONST_FLOAT fSonStart 110.1351

VECTOR vDaughterStart = <<-803.2621, 176.0598, 75.7406>>
CONST_FLOAT fDaughterStart 50.0651

VECTOR vWifeStart = <<-797.7103, 181.8636, 72.7925>>
CONST_FLOAT fWifeStart 0.3001

VECTOR vCoachStart = <<-798.1318, 181.0491, 72.7808>>
CONST_FLOAT fCoachStart -3.4667

//VECTOR vDriveTo = <<-17.4662, -1079.5088, 25.6721>>	//<<-27.5891, -1082.9415, 25.6017>>
CONST_FLOAT fDriveTo 125.9719	//69.2213

VECTOR vCarStart = <<-811.1514, 187.6124, 71.4744>>
CONST_FLOAT fCarStart 111.6656

//FLOAT fCutsceneHeading

VECTOR vMansion = <<-846.1608, 158.1691, 65.7346>>

//VECTOR vGarage = <<-815.4069, 185.8932, 71.4748>>

//VECTOR vClimbPoint = <<-801.3589, 167.5939, 70.5588>>

VECTOR vDealership = <<-60.3707, -1098.9924, 25.4262>> //<<-42.1335, -1111.1772, 25.4353>>

//VECTOR vGaragePosition = <<-815.3447, 185.9302, 73.0253>>
//VECTOR vGarageRotate = <<0.0, 0.0, -69.06>>

VECTOR vBlipDaughterRoute	//Coordinates to follow for blip

//#IF IS_DEBUG_BUILD //Debug Temp
//	//Position + Rotation
//	VECTOR vPos, vRot
//
//	//Input for Gamepad thumbsticks (LTS = Left Thumbstick, RTS = Right Thumbstick)
//	INT iLTSx
//	INT iLTSy
//	INT iRTSx
//	INT iRTSy
//#ENDIF

//Strings
TEXT_LABEL_23 txtConversationPoint

//Animation Dictionaries
STRING sAnimDictArm3 = "missarmenian3"
STRING sAnimDictArm3Argue = "missarmenian3mcs2"
STRING sAnimDictArm3LeadInOut = "missarmenian3leadinoutArmenian_3_int"
STRING sAnimDictArm3Climb = "missarmenian3mcs_1a"
STRING sAnimDictAngry = "missarmenian3@simeon_tauntsidle_b"
//STRING sAnimDictSlam = "missarmenian3doorslam_"
STRING sAnimDictGardenerMoveClipSet = "MOVE_M@LEAF_BLOWER"
STRING sAnimDictGarden = "missarmenian3_gardener"
STRING sAnimDictDoor = "missarmenian3_tryopendoor"
STRING sAnimDictLeadInMCS8 = "missarmenian3leadinoutarm3_mcs_8"

//Scenes
INT sceneClimbIn
VECTOR sceneClimbInPos = <<-802.411, 166.269, 70.557>>
VECTOR sceneClimbInRot = <<0.0, 0.0, 20.640>>

INT sceneTryDoor1, sceneTryDoor2, sceneTryDoor3, sceneTryDoor4

//═════════════════════════════════╡ INDEXES ╞═══════════════════════════════════

//Ped
PED_INDEX pedSon
PED_INDEX pedDaughter
PED_INDEX pedWife
PED_INDEX pedCoach
PED_INDEX pedOwner
PED_INDEX pedGardener
PED_INDEX pedIntro

//Vehicle
VEHICLE_INDEX vehDriveTo
VEHICLE_INDEX vehClimb
VEHICLE_INDEX vehCar
VEHICLE_INDEX vehIntro
VEHICLE_INDEX vehGarage[5]
VEHICLE_INDEX vehGarageOutside[2]
VEHICLE_INDEX vehOutside[2]

//Blip
BLIP_INDEX blipGardener
BLIP_INDEX blipSon
BLIP_INDEX blipDaughter
BLIP_INDEX blipWife
BLIP_INDEX blipCoach
BLIP_INDEX blipCar
BLIP_INDEX blipDestination
BLIP_INDEX blipOwner
BLIP_INDEX blipFranklin
BLIP_INDEX blipMichael

//Camera
CAMERA_INDEX camMain
CAMERA_INDEX camCinematic

//Sequence
SEQUENCE_INDEX seqMain

//Interior
INTERIOR_INSTANCE_INDEX intMansion
INTERIOR_INSTANCE_INDEX intGarage

//Object
OBJECT_INDEX objWindow
OBJECT_INDEX objTennisA
OBJECT_INDEX objTennisB
OBJECT_INDEX objBag
OBJECT_INDEX objGamepad
OBJECT_INDEX objHeadset
OBJECT_INDEX objGun
OBJECT_INDEX objGlass
OBJECT_INDEX objPhone
OBJECT_INDEX objLeafblower
OBJECT_INDEX objSeats

// Rayfire
RAYFIRE_INDEX iTVObject

//Shapetest
SHAPETEST_INDEX shapeTestIndex

//Doors
INT iBathroomDoor = ENUM_TO_INT(DOORHASH_M_MANSION_BATHROOM)
INT iJimmyDoor = ENUM_TO_INT(DOORHASH_M_MANSION_SON)
INT iTraceyDoor = ENUM_TO_INT(DOORHASH_M_MANSION_DAUGHTER)
INT iMichaelDoor = ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM)
INT iGarageDoor = HASH("GARAGE_DOOR")
INT iDealershipDoor1 = ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE)
INT iDealershipDoor2 = HASH("DEALERSHIP_DOOR2")
INT iDealershipDoor3 = ENUM_TO_INT(DOORHASH_DEALERSHIP_FRONT_L)
INT iDealershipDoor4 = ENUM_TO_INT(DOORHASH_DEALERSHIP_FRONT_R)

INT iSideGate = ENUM_TO_INT(DOORHASH_M_MANSION_GA_SM)

DOOR_DATA_STRUCT sHouseDoorData[7]

//Navmesh
INT iNavBlock = -1

//Scene
INT sceneWifeCoachFlirt
INT sceneLeadOutMCS1
INT sceneLeadInMCS8

//Sound
INT sIDLeaf = -1

//Particle
PTFX_ID ptfxLeaf

//Cover
COVERPOINT_INDEX covPoint[3]

//Model Names
MODEL_NAMES modGamepad = PROP_CONTROLLER_01
MODEL_NAMES modHeadset = PROP_HEADSET_01
MODEL_NAMES modTennis = PROP_TENNIS_RACK_01
MODEL_NAMES modBag = P_TENNIS_BAG_01_S

//Weapons
WEAPON_TYPE wtPistol = WEAPONTYPE_PISTOL

//Relationship Groups
REL_GROUP_HASH relGroupBuddy
REL_GROUP_HASH relGroupEnemy

//Locates Struct
LOCATES_HEADER_DATA sLocatesData

//Hint Cam Struct
CHASE_HINT_CAM_STRUCT localChaseHintCamStruct

//Rayfire
RAYFIRE_INDEX rfShowroomCrash1

//══════════════════════════════════╡ ENUMS ╞════════════════════════════════════

ENUM MissionObjective
	initMission,
	cutIntro,
	stageGoToMansion,
//	stageBreakIn,
	stageClimbUp,
	cutArgue,
	stageSneakThrough,
	stageStealCar,
	stageDriveTo,
	cutArrive,
	stageRammingSpeed,
	cutWindowSmash,
	stageBeatDown,
//	stageExitDealership,
//	stageSwitchTutorial,
	passMission,
	failMission
ENDENUM

MissionObjective eMissionObjective = InitMission

#IF IS_DEBUG_BUILD
	MissionObjective eMissionObjectiveAutoSkip = InitMission
#ENDIF

ENUM MissionFail
	failDefault,
	failPlayerDied,
	failMichaelDied,
	failFranklinDied,
	failSonDied,
	failWalkedInOnSon,
	failDaughterDied,
	failWalkedInOnDaughter,
	failWifeDied,
	failCoachDied,
	failGardenerDied,
	failSimeonDied,
	failSimeonDisrupted,
	failWalkedInOnWifeCoach,
	failWalkedInOnWifeCoachTrySneak,
	failWalkedInOnWifeCoachLivingRoom,
	failSpotted,
	failNoise,
	failLeftRoute,
	failFledOwner,
	failCarDestroyed,
	failExplosion,
	failWanted
ENDENUM

MissionFail eMissionFail = failDefault

ENUM PrestreamCutscene
	roomNone,
	roomSon,
	roomDaughter,
	roomWifeCoach
ENDENUM

PrestreamCutscene ePrestreamCutscene = roomNone

BOOL bPrestreamCutsceneLockedIn[COUNT_OF(PrestreamCutscene)]

//Audio
ENUM AUDIO_TRACK
	NO_AUDIO,
	ARM3_START,
	ARM3_RESTART_1,
	ARM3_WINDOW,
	ARM3_RESTART_2,
	ARM3_RESTART_3,
	ARM3_GARAGE_STOP,
	ARM3_CAR,
	ARM3_RESTART_4,
	ARM3_CALL,
	ARM3_MIC,
	ARM3_RESTART_5,
	ARM3_CS,
	ARM3_RESTART_6,
	ARM3_SPEED,
	ARM3_RESTART_7,
	ARM3_HIT,
	ARM3_HIT_STOP,
	ARM3_RESTART_8,
	ARM3_FAIL
ENDENUM

AUDIO_TRACK ePrepAudioTrack = NO_AUDIO
AUDIO_TRACK ePlayAudioTrack = NO_AUDIO

#IF IS_DEBUG_BUILD

//Stage Selector
INT iStageSkipMenu

CONST_INT MAX_SKIP_MENU_LENGTH COUNT_OF(MissionObjective) - 2 //Number of stages in mission minus Pass/Fail

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]

PROC MissionNames()
	SkipMenuStruct[0].sTxtLabel = "initMission"
	SkipMenuStruct[1].sTxtLabel = "cutIntro - (Armenian_3_int)"
	SkipMenuStruct[2].sTxtLabel = "stageGoToMansion"
//	SkipMenuStruct[?].sTxtLabel = "stageBreakIn"
	SkipMenuStruct[3].sTxtLabel = "stageClimbUp"
	SkipMenuStruct[4].sTxtLabel = "cutArgue"
	SkipMenuStruct[5].sTxtLabel = "stageSneakThrough"
	SkipMenuStruct[6].sTxtLabel = "stageStealCar"
	SkipMenuStruct[7].sTxtLabel = "stageDriveTo - (Armenian_3_mcs_6 | Armenian_3_mcs_7)"
	SkipMenuStruct[8].sTxtLabel = "cutArrive - (Armenian_3_mcs_6)"
	SkipMenuStruct[9].sTxtLabel = "stageRammingSpeed"
	SkipMenuStruct[10].sTxtLabel = "cutWindowSmash - (Armenian_3_mcs_8)"
	SkipMenuStruct[11].sTxtLabel = "stageBeatDown - (Armenian_3_MCS_9_concat)"
//	SkipMenuStruct[12].sTxtLabel = "stageExitDealership"
//	SkipMenuStruct[13].sTxtLabel = "stageSwitchTutorial"
ENDPROC

#ENDIF

//Text
STRING sConversationBlock = "ARM3AUD"

INT iTriggeredTextHashes[120]
INT iNumTextHashesStored = 0

PROC REMOVE_LABEL_ARRAY_SPACES()
	INT iArrayLength = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT (iArrayLength - 1) i
		IF iTriggeredTextHashes[i] = 0
			IF iTriggeredTextHashes[i+1] != 0
				iTriggeredTextHashes[i] = iTriggeredTextHashes[i+1]
				iTriggeredTextHashes[i+1] = 0
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_LABEL_INDEX(INT iLabelHash)
	INT i = 0
	
	REPEAT iNumTextHashesStored i
		IF iTriggeredTextHashes[i] = iLabelHash
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC BOOL HAS_LABEL_BEEN_TRIGGERED(STRING strLabel)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF GET_LABEL_INDEX(iHash) != -1
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC SET_LABEL_AS_TRIGGERED(STRING strLabel, BOOL bTrigger)
	INT iHash = GET_HASH_KEY(strLabel)
	
	IF bTrigger
		IF NOT HAS_LABEL_BEEN_TRIGGERED(strLabel)
			INT iArraySize = COUNT_OF(iTriggeredTextHashes)
		
			IF iNumTextHashesStored < iArraySize
				iTriggeredTextHashes[iNumTextHashesStored] = iHash
				iNumTextHashesStored++
			ELSE
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_LABEL_AS_TRIGGERED: Label array is full.")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = GET_LABEL_INDEX(iHash)
		IF iIndex != -1
			iTriggeredTextHashes[iIndex] = 0
			REMOVE_LABEL_ARRAY_SPACES()
			iNumTextHashesStored--
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TRIGGERED_LABELS()
	INT iArraySize = COUNT_OF(iTriggeredTextHashes)
	INT i = 0
	
	REPEAT iArraySize i
		iTriggeredTextHashes[i] = 0
	ENDREPEAT
	
	iNumTextHashesStored = 0
ENDPROC

//Models
INT iLoadedModelHashes[20]

FUNC BOOL HAS_MODEL_BEEN_LOADED(MODEL_NAMES mnModel)
	INT iHash = ENUM_TO_INT(mnModel)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedModelHashes) i
		IF iLoadedModelHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_MODEL_AS_LOADED(MODEL_NAMES mnModel, BOOL bIsLoaded)
	INT iHash = ENUM_TO_INT(mnModel)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedModelHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedModelHashes[i] = 0
				iLoadedModelHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedModelHashes[i] = iHash
				iLoadedModelHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_MODELS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedModelHashes) i
		iLoadedModelHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_MODEL_LOADED_CHECK(MODEL_NAMES mnModel)
	IF NOT HAS_MODEL_BEEN_LOADED(mnModel)
		REQUEST_MODEL(mnModel)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING MODEL #", ENUM_TO_INT(mnModel))	#ENDIF
		
		IF HAS_MODEL_LOADED(mnModel)
			SET_MODEL_AS_LOADED(mnModel, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("MODEL #", ENUM_TO_INT(mnModel), " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Recordings
INT iLoadedRecordingHashes[30]

FUNC BOOL HAS_RECORDING_BEEN_LOADED(INT iFileNumber, STRING sRecordingName)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		IF iLoadedRecordingHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_RECORDING_AS_LOADED(INT iFileNumber, STRING sRecordingName, BOOL bIsLoaded)
	TEXT_LABEL txtHash
	txtHash = iFileNumber
	txtHash += sRecordingName
	
	INT iHash = GET_HASH_KEY(txtHash)
	
	INT i = 0
	
	BOOL bQuitLoop = FALSE
	
	WHILE i < COUNT_OF(iLoadedRecordingHashes) AND NOT bQuitLoop
		IF bIsLoaded
			IF iLoadedRecordingHashes[i] = 0
				iLoadedRecordingHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iLoadedRecordingHashes[i] = iHash
				iLoadedRecordingHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_RECORDINGS()
	INT i = 0
	
	REPEAT COUNT_OF(iLoadedRecordingHashes) i
		iLoadedRecordingHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_RECORDING_LOADED_CHECK(INT iFileNumber, STRING sRecordingName)
	IF NOT HAS_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
		REQUEST_VEHICLE_RECORDING(iFileNumber, sRecordingName)
		
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iFileNumber, sRecordingName)
			SET_RECORDING_AS_LOADED(iFileNumber, sRecordingName, TRUE)
			PRINTSTRING("RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName) PRINTSTRING(" LOADED")
			PRINTNL()
		ELSE
			PRINTSTRING("LOADING RECORDING ") PRINTINT(iFileNumber) PRINTSTRING(" ") PRINTSTRING(sRecordingName)
			PRINTNL()
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Animation Dictionaries
INT iAnimDictHashes[20]

FUNC BOOL HAS_ANIM_DICT_BEEN_LOADED(STRING strAnimDict)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		IF iAnimDictHashes[i] = iHash
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_ANIM_DICT_AS_LOADED(STRING strAnimDict, BOOL bIsLoaded)
	INT iHash = GET_HASH_KEY(strAnimDict)
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	BOOL bQuitLoop = FALSE
	
	WHILE i < iNumHashes AND NOT bQuitLoop
		IF bIsLoaded
			IF iAnimDictHashes[i] = 0
				iAnimDictHashes[i] = iHash
				bQuitLoop = TRUE
			ENDIF
		ELSE
			IF iAnimDictHashes[i] = iHash
				iAnimDictHashes[i] = 0
				bQuitLoop = TRUE
			ENDIF
		ENDIF
		
		i++
	ENDWHILE
ENDPROC

PROC CLEAR_LOADED_ANIM_DICTS()
	INT iNumHashes = COUNT_OF(iAnimDictHashes)
	INT i = 0
	
	REPEAT iNumHashes i
		iAnimDictHashes[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_ANIM_DICT_LOADED_CHECK(STRING strAnimDict)
	IF NOT HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REQUEST_ANIM_DICT(strAnimDict)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING ANIM DICT ", strAnimDict)	#ENDIF
		
		IF HAS_ANIM_DICT_LOADED(strAnimDict)
			SET_ANIM_DICT_AS_LOADED(strAnimDict, TRUE)	#IF IS_DEBUG_BUILD	PRINTLN("ANIM DICT ", strAnimDict, " LOADED")	#ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_ANIM_DICT(STRING strAnimDict)
	IF HAS_ANIM_DICT_BEEN_LOADED(strAnimDict)
		REMOVE_ANIM_DICT(strAnimDict)
		SET_ANIM_DICT_AS_LOADED(strAnimDict, FALSE)
	ENDIF
ENDPROC

//════════════════════════════════╡ STRUCTURES ╞═════════════════════════════════

//Hotswap
SELECTOR_PED_STRUCT sSelectorPeds
//SELECTOR_CAM_STRUCT sCamDetails

//Dialogue
structPedsForConversation sPedsForConversation

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widGroup
#ENDIF

//════════════════════════════════╡ FUNCTIONS ╞══════════════════════════════════

BOOL bPlayerControl = TRUE

PROC SAFE_SET_PLAYER_CONTROL(PLAYER_INDEX iPlayerIndex, BOOL bSetControlOn, SET_PLAYER_CONTROL_FLAGS iFlags = 0)
	SET_PLAYER_CONTROL(iPlayerIndex, bSetControlOn, iFlags)
	bPlayerControl = bSetControlOn
ENDPROC

FUNC BOOL SAFE_IS_PLAYER_CONTROL_ON()
	IF bPlayerControl
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADVANCE_CUTSCENE()
	SETTIMERA(0)
	
	iCutsceneStage++
ENDPROC

PROC ADVANCE_STAGE()
	bCleanupStage = TRUE
ENDPROC

FUNC BOOL SKIPPED_STAGE()
	IF bSkipped = TRUE
		bSkipped = FALSE
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL INIT_STAGE()
	IF bInitStage = FALSE
		SETTIMERA(0)
		
		bInitStage = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SWAP_INTS(INT &iA, INT &iB)
	iA = iA + iB
	iB = iA - iB
	iA = iA - iB
ENDPROC

FUNC INT WRAP_INT(INT iValue, INT iMinValue, INT iMaxValue)
	IF iMaxValue < iMinValue
		SWAP_INTS(iMaxValue, iMinValue)
	ENDIF
    
	IF iValue < 0
		iValue = iValue + (10 + (ABSI(iValue) / 10) * 10)
	ENDIF
	
	RETURN (iValue % ABSI(iMaxValue - iMinValue)) + iMinValue
ENDFUNC

FUNC BOOL CLEANUP_STAGE()
	IF bCleanupStage = TRUE
		SETTIMERA(0)
		
		bInitStage = FALSE
		bCleanupStage = FALSE
		iCutsceneStage = 0
		
		iDialogueStage = 0
		INT i
		REPEAT COUNT_OF(iDialogueLineCount) i
			iDialogueLineCount[i] = -1
		ENDREPEAT
		iDialogueTimer = 0
		
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SAFE_CLEAR_HELP(BOOL bClearNow = TRUE)
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(bClearNow)
	ENDIF
ENDPROC

PROC CLEAR_TEXT()
	CLEAR_PRINTS()
	SAFE_CLEAR_HELP(TRUE)
	CLEAR_ALL_FLOATING_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
ENDPROC

FUNC BOOL PRINT_ADV(STRING sPrint, INT iDuration = DEFAULT_GOD_TEXT_TIME, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
		PRINT_NOW(sPrint, iDuration, 1)
		SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PRINT_HELP_ADV(STRING sPrint, BOOL bOnce = TRUE, INT iOverrideTime = -1)
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR (IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND GET_GAME_TIMER() > iHelpTimer)
		IF NOT HAS_LABEL_BEEN_TRIGGERED(sPrint)
			PRINT_HELP(sPrint, iOverrideTime)
			SET_LABEL_AS_TRIGGERED(sPrint, bOnce)
			PRINTLN("PRINTHELP ", sPrint)
			iHelpTimer = GET_GAME_TIMER() + 2000
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets visual field angles and ranges for specified ped.
/// PARAMS:
///    PedIndex - Ped index to change.
///    fSeeingRange - How far the vision extends.
///    fPeripheralSeeingRange - How far the peripheral vision extends.
///    fCenterAngle - The angle that defines field of view.
///    fMinAngle - The minimum angle that defines peripheral field of view.
///    fMaxAngle - The maximum angle that defines peripheral field of view.
PROC SET_PED_VISUAL_FIELD_PROPERTIES(PED_INDEX PedIndex, FLOAT fSeeingRange = 60.0, FLOAT fPeripheralSeeingRange = 5.0,
									 FLOAT fCenterAngle = 120.0, FLOAT fMinAngle = -90.0, FLOAT fMaxAngle = 90.0)
	IF NOT IS_PED_INJURED(PedIndex)
		SET_PED_SEEING_RANGE(PedIndex, fSeeingRange)
		SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE(PedIndex, fPeripheralSeeingRange)
		SET_PED_VISUAL_FIELD_CENTER_ANGLE(PedIndex, fCenterAngle / 2.0)
		SET_PED_VISUAL_FIELD_MIN_ANGLE(PedIndex, fMinAngle)
		SET_PED_VISUAL_FIELD_MAX_ANGLE(PedIndex, fMaxAngle)
	ENDIF
ENDPROC

FUNC PED_INDEX PLAYER_PED(enumCharacterList CHAR_TYPE)
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TYPE
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TYPE)]
	ENDIF
ENDFUNC

FUNC PED_INDEX NOT_PLAYER_PED_ID()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_TREVOR)]
	ELSE //GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		RETURN sSelectorPeds.pedID[GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(CHAR_MICHAEL)]
	ENDIF
ENDFUNC

PROC SET_PED_POSITION(PED_INDEX pedIndex, VECTOR vCoords, FLOAT fHeading, BOOL bKeepVehicle = TRUE)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF NOT IS_PED_INJURED(pedIndex)
			IF bKeepVehicle = TRUE
				SET_PED_COORDS_KEEP_VEHICLE(pedIndex, vCoords)
			ELSE
				SET_ENTITY_COORDS(pedIndex, vCoords)
			ENDIF
			
			CLEAR_PED_WETNESS(pedIndex)
			
			SET_ENTITY_HEADING(pedIndex, fHeading)
		ENDIF
	ENDIF
ENDPROC

PROC SET_VEHICLE_POSITION(VEHICLE_INDEX vehicleIndex, VECTOR vCoords, FLOAT fHeading)
	SET_ENTITY_COORDS(vehicleIndex, vCoords)
	SET_ENTITY_HEADING(vehicleIndex, fHeading)
ENDPROC

PROC SPAWN_PLAYER(PED_INDEX &pedIndex, enumCharacterList eChar, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = FALSE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedIndex, eChar, vStart, fStart)
			WAIT(0)
		ENDWHILE
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_PED(PED_INDEX &pedIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		pedIndex = CREATE_PED(PEDTYPE_MISSION, eModel, vStart, fStart)
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_VEHICLE(VEHICLE_INDEX &vehIndex, MODEL_NAMES eModel, VECTOR vStart, FLOAT fStart = 0.0, INT iColour = -1, FLOAT fDirt = 0.0)
	IF NOT DOES_ENTITY_EXIST(vehIndex)
		vehIndex = CREATE_VEHICLE(eModel, vStart, fStart)
		IF iColour >= 0
			SET_VEHICLE_COLOURS(vehIndex, iColour, iColour)
		ENDIF
		SET_VEHICLE_DIRT_LEVEL(vehIndex, fDirt)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex, TRUE)
	ENDIF
ENDPROC

FUNC BOOL SAFE_DEATH_CHECK_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_ENTITY_DEAD(pedIndex)
		OR IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_VEHICLE(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
		OR NOT IS_VEHICLE_DRIVEABLE(vehIndex, TRUE)
		OR IS_VEHICLE_PERMANENTLY_STUCK(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SAFE_SET_ENTITY_VISIBLE(ENTITY_INDEX EntityIndex, BOOL VisibleFlag)
	IF DOES_ENTITY_EXIST(EntityIndex)
		IF NOT IS_ENTITY_DEAD(EntityIndex)
			IF VisibleFlag
				IF NOT IS_ENTITY_VISIBLE(EntityIndex)
					SET_ENTITY_VISIBLE(EntityIndex, TRUE)
				ENDIF
			ELSE
				IF IS_ENTITY_VISIBLE(EntityIndex)
					SET_ENTITY_VISIBLE(EntityIndex, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_LOCATION(BLIP_INDEX &blipIndex, VECTOR vCoords, BOOL bRoute = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		blipIndex = CREATE_BLIP_FOR_COORD(vCoords, bRoute)
		SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGHEST)
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(pedIndex)
			IF NOT IS_PED_INJURED(pedIndex)
				blipIndex = CREATE_BLIP_FOR_PED(pedIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_VEHICLE(BLIP_INDEX &blipIndex, VEHICLE_INDEX &vehIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(vehIndex)
			IF NOT IS_ENTITY_DEAD(vehIndex)
				blipIndex = CREATE_BLIP_FOR_VEHICLE(vehIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
		REMOVE_BLIP(blipIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		DELETE_PED(pedIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_VEHICLE(VEHICLE_INDEX &vehicleIndex)
	IF DOES_ENTITY_EXIST(vehicleIndex)
		DELETE_VEHICLE(vehicleIndex)
	ENDIF
ENDPROC

PROC SAFE_DELETE_OBJECT(OBJECT_INDEX &objectIndex)
	IF DOES_ENTITY_EXIST(objectIndex)
		DELETE_OBJECT(objectIndex)
	ENDIF
ENDPROC

PROC SAFE_FREEZE_ENTITY_POSITION(ENTITY_INDEX EntityIndex, BOOL FrozenByScriptFlag)
	IF NOT IS_ENTITY_ATTACHED(EntityIndex)
		IF (IS_ENTITY_A_PED(EntityIndex)
		AND NOT IS_PED_IN_ANY_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(EntityIndex)))
		OR NOT IS_ENTITY_A_PED(EntityIndex)
			FREEZE_ENTITY_POSITION(EntityIndex, FrozenByScriptFlag)
		ENDIF
	ENDIF
ENDPROC

//Borrowed from locates_private.sch!
PROC EMPTY_VEHICLE_OF_PEDS(VEHICLE_INDEX inVehicle)
    PED_INDEX pedTemp
    INT i
    INT iMaxNumberOfPassengers
	
    //Clear tasks of any passengers or drivers
    IF DOES_ENTITY_EXIST(inVehicle)
        IF IS_VEHICLE_DRIVEABLE(inVehicle)
            //Driver
            pedTemp = GET_PED_IN_VEHICLE_SEAT(inVehicle)  
            IF DOES_ENTITY_EXIST(pedTemp)
                IF NOT IS_PED_INJURED(pedTemp)
                    CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
                ENDIF
            ENDIF
            
            iMaxNumberOfPassengers = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(inVehicle) 
			
            //Passengers
            REPEAT iMaxNumberOfPassengers i
                IF NOT IS_VEHICLE_SEAT_FREE(inVehicle, INT_TO_ENUM(VEHICLE_SEAT, i))
                    pedTemp = GET_PED_IN_VEHICLE_SEAT(inVehicle, INT_TO_ENUM(VEHICLE_SEAT, i)) 
                    IF DOES_ENTITY_EXIST(pedTemp)
                        IF NOT IS_PED_INJURED(pedTemp)
                            CLEAR_PED_TASKS_IMMEDIATELY(pedTemp)
                        ENDIF
                    ENDIF
                ENDIF
            ENDREPEAT
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL SETUP_JIMMYS_CAR(VEHICLE_INDEX vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
	AND NOT IS_ENTITY_DEAD(vehIndex)
		SET_VEHICLE_AS_RESTRICTED(vehIndex, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehIndex, "57EIG117")
		SET_VEHICLE_CAN_SAVE_IN_GARAGE(vehIndex, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehIndex, TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex, TRUE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehIndex, SC_DOOR_FRONT_LEFT, FALSE)
		SET_VEHICLE_TYRES_CAN_BURST(vehIndex, FALSE)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRUCT_ENTITY_ID CONSTRUCT_ENTITY_ID()
	STRUCT_ENTITY_ID structEntityID
	RETURN structEntityID
ENDFUNC

FLOAT SMALL_FLOAT = 0.000001

//FLOAT fHoodSpringConstant = 700.0
//FLOAT fHoodDampingRatio = 1.0

//VECTOR vHoodVelocity
//VECTOR vHoodResult

FUNC FLOAT DAMPED_SPRING_CAM(FLOAT &fVelocity, FLOAT &fResult, FLOAT fTarget, FLOAT fSpringConstant, FLOAT fDampingRatio)
	//If spring constant is too small, snap to the fTarget.
	IF (fSpringConstant < SMALL_FLOAT)
		fVelocity = 0.0
		fResult = fTarget
	ELSE
		IF (TIMESTEP() >= SMALL_FLOAT)
			//NOTE: The spring equations have been normalised for unit mass.
			FLOAT fIntialDisplacement = fResult - fTarget
			FLOAT fHalfDampingCoeff = fDampingRatio * SQRT(fSpringConstant)
			FLOAT fOmegaSqr = fSpringConstant - (fHalfDampingCoeff * fHalfDampingCoeff)
			FLOAT fSpringDisplacementToApply = 0.0
			
			IF(fOmegaSqr >= SMALL_FLOAT)
				//Under-damped.
				FLOAT fOmega = SQRT(fOmegaSqr)
				FLOAT fC = (fVelocity + (fHalfDampingCoeff * fIntialDisplacement)) / fOmega
				
				fSpringDisplacementToApply = POW(2.718281828, -fHalfDampingCoeff * TIMESTEP()) * ((fIntialDisplacement * COS(fOmega * TIMESTEP())) +
																					 (fC * SIN(fOmega * TIMESTEP())))
				fVelocity = POW(2.718281828, -fHalfDampingCoeff * TIMESTEP()) * ((-fHalfDampingCoeff *
																	((fIntialDisplacement * COS(fOmega * TIMESTEP())) + (fC * SIN(fOmega * TIMESTEP())))) +
																	(fOmega * ((fC * COS(fOmega * TIMESTEP())) - (fIntialDisplacement * SIN(fOmega * TIMESTEP())))))
			ELSE
				//Critically- or over-damped.
				//NOTE: We must negate (and clamp) the square fOmega term and use an alternate equation to avoid computing the square root of a
				//negative number.
				FLOAT fNegatedfOmegaSqr = MAX_VALUE(-fOmegaSqr, 0.0)
				FLOAT fOmega = SQRT(fNegatedfOmegaSqr)
				fSpringDisplacementToApply = fIntialDisplacement * POW(2.718281828, (-fHalfDampingCoeff + fOmega) * TIMESTEP())
				fVelocity = (-fHalfDampingCoeff + fOmega) * fSpringDisplacementToApply
			ENDIF
			
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//			NOTE: The equation for critical damping should actually be as below, but the solution for an over-damped spring works adequately for
//			our purposes.
//
//			const float fAlpha             = fVelocity + (fHalfDampingCoeff * fIntialDisplacement);
//			fSpringDisplacementToApply     = ((fAlpha * TIMESTEP()) + fIntialDisplacement) * EXP(-fHalfDampingCoeff * TIMESTEP());
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			fResult = fTarget + fSpringDisplacementToApply
		ENDIF
	ENDIF
	
	RETURN fResult
ENDFUNC

FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x, V2.y-V1.y)
ENDFUNC

INT iTakeFrameCount

BOOL bOverHoodCinematicCam

PROC OVER_HOOD_CINEMATIC_CAM()
	IF iTakeFrameCount != GET_FRAME_COUNT()
		iTakeFrameCount = GET_FRAME_COUNT()
		
		IF NOT DOES_CAM_EXIST(camCinematic)
			camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
			SET_CAM_FOV(camCinematic, 28.6103)	//30.0)		
			SHAKE_CAM(camCinematic, "HAND_SHAKE", 1.0)
			SET_CAM_ACTIVE(camCinematic, TRUE)
//			vHoodVelocity = VECTOR_ZERO
//			vHoodResult = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<-0.0481, 2.0908, 0.5994>>)
			
			//Vehicle Zoom Level
			IF NOT bZoomLevel
				vehicleZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
				
				camViewModeOnFoot = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
				
				PRINTLN("Store Zoom Level GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = ", ENUM_TO_INT(GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()))
				PRINTLN("Store Zoom Level GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = ", ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)))
				
				bZoomLevel = TRUE
			ENDIF
		ENDIF
		
		IF bOverHoodCinematicCam
		AND IS_ENTITY_UPRIGHT(vehCar)
			IF DOES_CAM_EXIST(camCinematic)
				IF NOT IS_CAM_RENDERING(camCinematic)
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					IF IS_PHONE_ONSCREEN()
						HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					ENDIF
					
					//Special Ability
					SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
					ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					OVERRIDE_LODSCALE_THIS_FRAME(1.0)
					
					CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(0.5)
					CASCADE_SHADOWS_SET_SHADOW_SAMPLE_TYPE("CSM_ST_BOX4x4")
					CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)
					
					SET_TIMECYCLE_MODIFIER("Hint_cam")
				ELSE
					OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				ENDIF
			ENDIF
		ELSE
			bOverHoodCinematicCam = FALSE
			
			IF DOES_CAM_EXIST(camCinematic)
				IF IS_CAM_RENDERING(camCinematic)
				OR eMissionObjective = cutArrive
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(1.0)
					CASCADE_SHADOWS_INIT_SESSION()
					CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
					
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
						CLEAR_TIMECYCLE_MODIFIER()
					ENDIF
					
					IF eMissionObjective != stageDriveTo
						ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_CAM_EXIST(camCinematic)
		AND eMissionObjective != cutArrive
//			VECTOR vCamCinematic = GET_CAM_COORD(camCinematic)
//			VECTOR vTrack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<-0.0481, 2.0908, 0.5994>>)	//<<-0.9, 2.2, 0.479107>>)
//			
//			vCamCinematic.X = vTrack.X -@ (((vTrack.X - vCamCinematic.X) / 6) * 15)	//vCamCinematic.X = vCamCinematic.X +@ (((vTrack.X - vCamCinematic.X) / 1.2) * 15)
//			vCamCinematic.Y = vTrack.Y -@ (((vTrack.Y - vCamCinematic.Y) / 6) * 15)	//vCamCinematic.Y = vCamCinematic.Y +@ (((vTrack.Y - vCamCinematic.Y) / 1.2) * 15)
//			vCamCinematic.Z = vTrack.Z -@ (((vTrack.Z - vCamCinematic.Z) / 6) * 15)	//vCamCinematic.Z = vCamCinematic.Z +@ (((vTrack.Z - vCamCinematic.Z) / 1.2) * 15)
//			
//			ATTACH_CAM_TO_ENTITY(camCinematic, vehCar, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehCar, vCamCinematic))	//SET_CAM_COORD(camCinematic, vCamCinematic)
//			
//			POINT_CAM_AT_ENTITY(camCinematic, vehCar, <<-0.2945, -0.8930, 0.4086>>)	//<<0.0, 0.0, 0.5>>)
//			
//			VECTOR vTrack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<-0.0481, 2.0908, 0.5994>>)
//			
//			DAMPED_SPRING_CAM(vHoodVelocity.X, vHoodResult.X, vTrack.X, fHoodSpringConstant, fHoodDampingRatio)
//			DAMPED_SPRING_CAM(vHoodVelocity.Y, vHoodResult.Y, vTrack.Y, fHoodSpringConstant, fHoodDampingRatio)
//			DAMPED_SPRING_CAM(vHoodVelocity.Z, vHoodResult.Z, vTrack.Z, fHoodSpringConstant, fHoodDampingRatio)
			
			ATTACH_CAM_TO_ENTITY(camCinematic, vehCar, <<-0.0481, 2.0908, 0.5994>>)
			
			POINT_CAM_AT_ENTITY(camCinematic, vehCar, <<-0.2945, -0.8930, 0.4086>>)
			
//			#IF IS_DEBUG_BUILD
//			VECTOR vTempOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehCar, GET_CAM_COORD(camCinematic))
//			PRINTLN("Speed: ", GET_ENTITY_SPEED(vehCar), " camCinematic offset: ", vTempOffset)
//			#ENDIF
		ENDIF
		
		IF GET_INTERIOR_FROM_ENTITY(vehCar) != NULL
			SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(GET_ROOM_KEY_FROM_ENTITY(vehCar))
		ENDIF
		
		//Stats
		IF DOES_CAM_EXIST(camCinematic)
		AND IS_CAM_ACTIVE(camCinematic)
			INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_START()
		ELSE
			INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		
		BOOL bInputNextCameraPressed
		
		bInputNextCameraPressed = FALSE
		
		IF bCameraViewToggle
			IF NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			AND NOT IS_CUSTOM_MENU_ON_SCREEN() AND NOT IS_GAMEPLAY_HINT_ACTIVE()
				bInputNextCameraPressed = TRUE
				
				bCameraViewToggle = FALSE
			ENDIF
			
			IF IS_CUSTOM_MENU_ON_SCREEN() OR IS_GAMEPLAY_HINT_ACTIVE()
				bCameraViewToggle = FALSE
			ENDIF
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			bCameraViewToggle = TRUE
		ENDIF
		
		IF bInputNextCameraPressed
			PRINTLN("DISABLED_CONTROL_JUST_PRESSED(INPUT_NEXT_CAMERA)")
			PRINTLN("vehicleZoomLevel=", vehicleZoomLevel)
			PRINTLN("bVehicleZoomInOut=", bVehicleZoomInOut)
			PRINTLN("------------------------------")
			IF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_BONNET
				SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
			ELIF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_NEAR
				SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
			ELIF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_MEDIUM
				IF bVehicleZoomInOut
					SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_FAR)
				ELSE
					SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_NEAR)
				ENDIF
				
				bVehicleZoomInOut = !bVehicleZoomInOut
			ELIF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_FAR
				SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
			ENDIF
			vehicleZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
			PRINTLN("vehicleZoomLevel=", vehicleZoomLevel)
			PRINTLN("bVehicleZoomInOut=", bVehicleZoomInOut)
			PRINTLN("------------------------------")
		ENDIF
		
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	ENDIF
ENDPROC

ENUM DOORS_LIST
	BATHROOM_DOOR,		//V_ILEV_MM_DOORW, <<-804.95, 171.86, 76.89>>
	JIMMY_DOOR,			//V_ILEV_MM_DOORSON, <<-806.77, 174.02, 76.89>>
	TRACEY_DOOR,		//V_ILEV_MM_DOORDAUGHTER, <<-802.70, 176.18, 76.89>>
	MICHAEL_DOOR,		//V_ILEV_MM_DOORW, <<-809.28, 177.86, 76.89>>
	GARAGE_DOOR			//V_ILEV_MM_DOOR, <<-806.28, 186.02, 72.62>>
ENDENUM

FUNC BOOL SMOOTH_CLOSE_DOOR(DOORS_LIST eDoorsList, MODEL_NAMES modelName, VECTOR vCoords, BOOL bLock, FLOAT fSpeed = 0.2, FLOAT fTolerance = 0.01, FLOAT fOpenRatioIn = 0.0, BOOL bRemoveSpring = FALSE)
	FLOAT fOpenRatio
	INT iFrontDoor
	
	SWITCH eDoorsList
		CASE BATHROOM_DOOR
			iFrontDoor = iBathroomDoor
		BREAK
		CASE JIMMY_DOOR
			iFrontDoor = iJimmyDoor
		BREAK
		CASE TRACEY_DOOR
			iFrontDoor = iTraceyDoor
		BREAK
		CASE MICHAEL_DOOR
			iFrontDoor = iMichaelDoor
		BREAK
		CASE GARAGE_DOOR
			iFrontDoor = iGarageDoor
		BREAK
	ENDSWITCH
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
		DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor)
		
		FLOAT fDoorOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(iFrontDoor)
		
		//Release...
		fDoorOpenRatio = fDoorOpenRatio
		eDoorState = eDoorState
		modelName = modelName
		
		DOOR_SYSTEM_SET_SPRING_REMOVED(iFrontDoor, bRemoveSpring)
		
		IF fOpenRatio <= fOpenRatioIn + -fTolerance
		OR fOpenRatio >= fOpenRatioIn + fTolerance
			IF fOpenRatio > fOpenRatioIn
				fOpenRatio -= fSpeed
			ELIF fOpenRatio < fOpenRatioIn
				fOpenRatio += fSpeed
			ENDIF
			
			DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, fOpenRatio, FALSE)
			
			CLEAR_AREA_OF_OBJECTS(vCoords, 2.0)
			
			IF bLock
				DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_LOCKED, FALSE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
			ENDIF
			
			RETURN FALSE
		ELSE
			fOpenRatio = fOpenRatioIn
			
			DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, fOpenRatio, FALSE)
			
			IF bLock
				DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_LOCKED, FALSE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC UNLOCK_DOOR(DOORS_LIST eDoorsList, MODEL_NAMES modelName, BOOL bRemoveSpring = FALSE)
	INT iFrontDoor
	
	SWITCH eDoorsList
		CASE BATHROOM_DOOR
			iFrontDoor = iBathroomDoor
		BREAK
		CASE JIMMY_DOOR
			iFrontDoor = iJimmyDoor
		BREAK
		CASE TRACEY_DOOR
			iFrontDoor = iTraceyDoor
		BREAK
		CASE MICHAEL_DOOR
			iFrontDoor = iMichaelDoor
		BREAK
		CASE GARAGE_DOOR
			iFrontDoor = iGarageDoor
		BREAK
	ENDSWITCH
	
	//Release...
	modelName = modelName
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
		DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor)
		
		IF eDoorState = DOORSTATE_LOCKED
			DOOR_SYSTEM_SET_SPRING_REMOVED(iFrontDoor, bRemoveSpring, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ELSE
			DOOR_SYSTEM_SET_SPRING_REMOVED(iFrontDoor, bRemoveSpring, FALSE, TRUE)
		ENDIF
	ENDIF
ENDPROC

//ARM3_START		= /2
//ARM3_RESTART_1	=
//ARM3_WINDOW		= 
//ARM3_RESTART_2	= 
//ARM3_RESTART_3	= 
//ARM3_GARAGE_STOP	= 
//ARM3_CAR			= 
//ARM3_RESTART_4	= 
//ARM3_CALL			= 
//ARM3_MIC			= /2
//ARM3_RESTART_5	= 
//ARM3_CS			= 
//ARM3_RESTART_6	= 
//ARM3_SPEED		= 
//ARM3_RESTART_7	= 
//ARM3_HIT			= /2
//ARM3_HIT_STOP		= 
//ARM3_RESTART_8	= 
//ARM3_FAIL			= 

STRING sAudioEvent
INT iAudioPrepareTimer	//This is an override in case an audio event is prepared but never used

PROC AUDIO_CONTROLLER()	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("<<<<<<<< AUDIO_CONTROLLER >>>>>>>> (sAudioEvent = ", sAudioEvent, ")")	ENDIF	#ENDIF
	IF ePlayAudioTrack = NO_AUDIO
	AND (NOT IS_MUSIC_ONESHOT_PLAYING()
	OR GET_GAME_TIMER() > iAudioPrepareTimer)
		SWITCH ePrepAudioTrack
			CASE ARM3_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - ARM3_START")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "ARM3_START")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "ARM3_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("ARM3_START")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - ARM3_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ARM3_MIC	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - ARM3_MIC")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "ARM3_MIC")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "ARM3_MIC"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("ARM3_MIC")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - ARM3_MIC")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ARM3_HIT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepare - ARM3_HIT")	ENDIF	#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "ARM3_HIT")
					IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
				 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
					ENDIF
					
					sAudioEvent = "ARM3_HIT"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("ARM3_HIT")
						ePrepAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Prepared audio - ARM3_HIT")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH ePlayAudioTrack
		CASE ARM3_START	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_START")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "ARM3_START")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "ARM3_START"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("ARM3_START")
					IF TRIGGER_MUSIC_EVENT("ARM3_START")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_START")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_1	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_1")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_1")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_1")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_WINDOW	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_WINDOW")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_WINDOW")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_WINDOW")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_2	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_2")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_2")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_2")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_3	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_3")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_3")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_3")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_GARAGE_STOP	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_GARAGE_STOP")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_GARAGE_STOP")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_GARAGE_STOP")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_CAR	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_CAR")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_CAR")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_CAR")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_4	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_4")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_4")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_4")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_CALL	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_CALL")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_CALL")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_CALL")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_MIC	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_MIC")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "ARM3_MIC")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "ARM3_MIC"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("ARM3_MIC")
					IF TRIGGER_MUSIC_EVENT("ARM3_MIC")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_MIC")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_5	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_5")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_5")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_5")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_CS	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_CS")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_CS")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_CS")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_6	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_6")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_6")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_6")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_SPEED	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_SPEED")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_SPEED")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_SPEED")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_7	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_7")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_7")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_7")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_HIT	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_HIT")	ENDIF	#ENDIF
			IF IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			OR NOT ARE_STRINGS_EQUAL(sAudioEvent, "ARM3_HIT")
				IF NOT IS_STRING_NULL_OR_EMPTY(sAudioEvent)
			 		#IF IS_DEBUG_BUILD	IF 	#ENDIF	CANCEL_MUSIC_EVENT(sAudioEvent)	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Cancelled audio - sAudioEvent = ", sAudioEvent)	ENDIF	ENDIF	#ENDIF
				ENDIF
				
				sAudioEvent = "ARM3_HIT"	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Preparing audio - sAudioEvent = ", sAudioEvent)	ENDIF	#ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("ARM3_HIT")
					IF TRIGGER_MUSIC_EVENT("ARM3_HIT")
						iAudioPrepareTimer = GET_GAME_TIMER() + 5000	//Assumes 5 seconds is long enough for the one shot to play
						
						ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_HIT")	ENDIF	#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE ARM3_HIT_STOP	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_HIT_STOP")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_HIT_STOP")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_HIT_STOP")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_RESTART_8	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_RESTART_8")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_RESTART_8")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_RESTART_8")	ENDIF	#ENDIF
			ENDIF
		BREAK
		CASE ARM3_FAIL	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Play - ARM3_FAIL")	ENDIF	#ENDIF
			IF TRIGGER_MUSIC_EVENT("ARM3_FAIL")
				ePlayAudioTrack = NO_AUDIO	#IF IS_DEBUG_BUILD	IF bDebugAudio	PRINTLN("Playing audio - ARM3_FAIL")	ENDIF	#ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC LOAD_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePrepAudioTrack = eSetAudioTrack	
	
	AUDIO_CONTROLLER()
ENDPROC

PROC PLAY_AUDIO(AUDIO_TRACK eSetAudioTrack)
	ePlayAudioTrack = eSetAudioTrack
	
	AUDIO_CONTROLLER()
ENDPROC

PROC CLEANUP_TENNIS_PROPS()
	IF DOES_ENTITY_EXIST(objTennisA)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
			STOP_SYNCHRONIZED_ENTITY_ANIM(objTennisA, 0.0, TRUE)
		ENDIF
		DETACH_ENTITY(objTennisA, FALSE)
		FREEZE_ENTITY_POSITION(objTennisA, FALSE)
		ACTIVATE_PHYSICS(objTennisA)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objTennisB)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
			STOP_SYNCHRONIZED_ENTITY_ANIM(objTennisB, 0.0, TRUE)
		ENDIF
		DETACH_ENTITY(objTennisB, FALSE)
		FREEZE_ENTITY_POSITION(objTennisB, FALSE)
		ACTIVATE_PHYSICS(objTennisB)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objBag)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
			STOP_SYNCHRONIZED_ENTITY_ANIM(objBag, 0.0, TRUE)
		ENDIF
		DETACH_ENTITY(objBag, FALSE)
		FREEZE_ENTITY_POSITION(objBag, FALSE)
		ACTIVATE_PHYSICS(objBag)
	ENDIF
ENDPROC

PROC CLEANUP_GAME_PROPS()
	IF DOES_ENTITY_EXIST(objGamepad)
		DETACH_ENTITY(objGamepad, FALSE)
		FREEZE_ENTITY_POSITION(objGamepad, FALSE)
		ACTIVATE_PHYSICS(objGamepad)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objHeadset)
		DETACH_ENTITY(objHeadset, FALSE)
		FREEZE_ENTITY_POSITION(objHeadset, FALSE)
		ACTIVATE_PHYSICS(objHeadset)
	ENDIF
ENDPROC

//Cleanup
PROC MISSION_CLEANUP(BOOL bRestart = FALSE)
	//Fade Out
//	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
//		IF NOT IS_SCREEN_FADING_OUT()
//		AND NOT IS_SCREEN_FADED_OUT()
//			DO_SCREEN_FADE_OUT(500)
//		ENDIF
//		
//		WHILE NOT IS_SCREEN_FADED_OUT()
//			WAIT(0)
//		ENDWHILE
//	ENDIF
	
	IF bVideoRecording
		REPLAY_STOP_EVENT()
		
		bVideoRecording = FALSE
	ENDIF
	
	bCameraViewToggle = FALSE
	
	//GPS
	//SET_IGNORE_NO_GPS_FLAG(FALSE)
	
	//Stats
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
	
	//Player
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		
		//Dialogue
		STOP_PED_SPEAKING(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_FRANKLIN))
	AND NOT IS_PED_INJURED(PLAYER_PED(CHAR_FRANKLIN))
		RESET_PED_IN_VEHICLE_CONTEXT(PLAYER_PED(CHAR_FRANKLIN))
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
	AND NOT IS_PED_INJURED(PLAYER_PED(CHAR_MICHAEL))
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED(CHAR_MICHAEL), TRUE)
	ENDIF
	
	SET_PLAYER_SNEAKING_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)
	SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)
	
	SET_PLAYER_CAN_DO_DRIVE_BY(PLAYER_ID(), TRUE)
	DISABLE_PLAYER_FIRING(PLAYER_ID(), FALSE)
	
	//Unlock Car Doors
	IF DOES_ENTITY_EXIST(vehCar)
		IF NOT IS_ENTITY_DEAD(vehCar)
			SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_UNLOCKED)
		ENDIF
	ENDIF
	
	//Doors
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iBathroomDoor)
		REMOVE_DOOR_FROM_SYSTEM(iBathroomDoor)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iJimmyDoor)
		REMOVE_DOOR_FROM_SYSTEM(iJimmyDoor)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iTraceyDoor)
		REMOVE_DOOR_FROM_SYSTEM(iTraceyDoor)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMichaelDoor)
		REMOVE_DOOR_FROM_SYSTEM(iMichaelDoor)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
		REMOVE_DOOR_FROM_SYSTEM(iGarageDoor)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor1)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoor1)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor2)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoor2)
	ENDIF
	
	//Audio - Door Portals
	REMOVE_PORTAL_SETTINGS_OVERRIDE("V_MICHAEL_PS_BATHROOM_WITH_WINDOW")
	
	//Unlock Mansion Door
	REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
	
	DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, DOORSTATE_UNLOCKED, TRUE, TRUE)
	
	//Dealership Shutter
//	IF intGarage != NULL
//		IF IS_INTERIOR_ENTITY_SET_ACTIVE(intGarage, "shutter_closed")
//			DEACTIVATE_INTERIOR_ENTITY_SET(intGarage, "shutter_closed")
//	    ENDIF
//		
//		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(intGarage, "shutter_open")
//			ACTIVATE_INTERIOR_ENTITY_SET(intGarage, "shutter_open")
//		ENDIF
//	ENDIF
	
	//LOD
	IF bPassed = FALSE
		SET_BUILDING_STATE(BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD, BUILDINGSTATE_NORMAL, FALSE)
	ENDIF
	
	//Portals
	IF bPassed = FALSE
		IF HAS_LABEL_BEEN_TRIGGERED("WindowSmashed")
			REMOVE_PORTAL_SETTINGS_OVERRIDE("V_CARSHOWROOM_PS_WINDOW_UNBROKEN")
			
			SET_LABEL_AS_TRIGGERED("WindowSmashed", FALSE)
		ENDIF
	ENDIF
	
	//Unpin
	bPinnedMansion = FALSE
	
	//New Load Scene
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	//Mod Shop
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
	//Ints/Bools
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
	
	iCarHealth = 1000
	iCarGasTank = 1000.0
	iCarEngineHealth = 1000.0
	
	iStoppedTimer = -1
	
	//Radar
	bRadar = TRUE
	
	//Camera
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(1.0)
	CASCADE_SHADOWS_INIT_SESSION()
	CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DESTROY_ALL_CAMS()
	
	STOP_GAMEPLAY_HINT(TRUE)
	
	//Particles
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeaf)
		STOP_PARTICLE_FX_LOOPED(ptfxLeaf)
	ENDIF
	
	//Anims
	IF bPassed = FALSE
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	INT i
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	ENDIF
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	ENDIF
	IF DOES_ENTITY_EXIST(pedOwner)
		SET_PED_AS_NO_LONGER_NEEDED(pedOwner)
	ENDIF
	IF DOES_ENTITY_EXIST(pedSon)
		SET_PED_AS_NO_LONGER_NEEDED(pedSon)
	ENDIF
	IF DOES_ENTITY_EXIST(pedDaughter)
		SET_PED_AS_NO_LONGER_NEEDED(pedDaughter)
	ENDIF
	IF DOES_ENTITY_EXIST(pedWife)
		SET_PED_AS_NO_LONGER_NEEDED(pedWife)
	ENDIF
	IF DOES_ENTITY_EXIST(pedCoach)
		SET_PED_AS_NO_LONGER_NEEDED(pedCoach)
	ENDIF
	IF DOES_ENTITY_EXIST(pedIntro)
		SET_PED_AS_NO_LONGER_NEEDED(pedIntro)
	ENDIF
	IF DOES_ENTITY_EXIST(pedGardener)
		SET_PED_AS_NO_LONGER_NEEDED(pedGardener)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objLeafblower)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GARDENER_01)
		REMOVE_ANIM_DICT(sAnimDictGarden)		
		IF HAS_CLIP_SET_LOADED(sAnimDictGardenerMoveClipSet)
			REMOVE_CLIP_SET(sAnimDictGardenerMoveClipSet)
		ENDIF
	ENDIF
	
	//Vehicle
	IF DOES_ENTITY_EXIST(vehDriveTo)
	AND NOT IS_ENTITY_DEAD(vehDriveTo)
		IF IS_ENTITY_A_MISSION_ENTITY(vehDriveTo) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehDriveTo)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDriveTo)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehIntro)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehIntro)
	ENDIF
	IF DOES_ENTITY_EXIST(vehClimb)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehClimb)
	ENDIF
	IF NOT bPassed
		IF DOES_ENTITY_EXIST(vehCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
		ENDIF
	ENDIF
	REPEAT COUNT_OF(vehGarage) i
		IF DOES_ENTITY_EXIST(vehGarage[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGarage[i])
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(vehGarageOutside) i
		IF DOES_ENTITY_EXIST(vehGarageOutside[i])
		AND NOT IS_ENTITY_DEAD(vehGarageOutside[i])
			IF IS_ENTITY_A_MISSION_ENTITY(vehGarageOutside[i]) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehGarageOutside[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGarageOutside[i])
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(vehOutside) i
		IF DOES_ENTITY_EXIST(vehOutside[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehOutside[i])
		ENDIF
	ENDREPEAT
	
	//Objects
	IF DOES_ENTITY_EXIST(objWindow)
		IF NOT IS_ENTITY_ON_SCREEN(objWindow)
		OR IS_ENTITY_OCCLUDED(objWindow)
			SAFE_DELETE_OBJECT(objWindow)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(objWindow)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objWindow)
	ENDIF
	IF DOES_ENTITY_EXIST(objTennisA)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objTennisA)
	ENDIF
	IF DOES_ENTITY_EXIST(objTennisB)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objTennisB)
	ENDIF
	IF DOES_ENTITY_EXIST(objBag)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objBag)
	ENDIF
	IF DOES_ENTITY_EXIST(objGamepad)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objGamepad)
	ENDIF
	IF DOES_ENTITY_EXIST(objHeadset)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objHeadset)
	ENDIF
	IF DOES_ENTITY_EXIST(objGun)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objGun)
	ENDIF
	IF DOES_ENTITY_EXIST(objGlass)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objGlass)
	ENDIF
	IF DOES_ENTITY_EXIST(objPhone)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objPhone)
	ENDIF
	IF DOES_ENTITY_EXIST(objSeats)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objSeats)
	ENDIF
	
	REMOVE_MODEL_HIDE(<<-802.73, 167.50, 77.58>>, 1.0, V_ILEV_MM_WINDOWWC)
	
	IF bPassed
		IF DOES_ENTITY_EXIST(vehCar)
			IF GET_ENTITY_MODEL(vehCar) = BJXL
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				FREEZE_ENTITY_POSITION(vehCar, FALSE)
				ACTIVATE_PHYSICS(vehCar)
				SET_VEHICLE_TYRES_CAN_BURST(vehCar, TRUE)
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BJXL_CRASH_POST_ARM3, TRUE)
				SET_VEHICLE_GEN_VEHICLE(VEHGEN_BJXL_CRASH_POST_ARM3, vehCar)
			ENDIF
		ENDIF
		
		REPEAT COUNT_OF(vehGarage) i
			IF DOES_ENTITY_EXIST(vehGarage[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGarage[i])
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(vehGarageOutside) i
			IF DOES_ENTITY_EXIST(vehGarageOutside[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGarageOutside[i])
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(vehOutside) i
			IF DOES_ENTITY_EXIST(vehOutside[i])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehOutside[i])
			ENDIF
		ENDREPEAT
	ENDIF
	
	//Stealth
	IF HAS_LABEL_BEEN_TRIGGERED("ACT_stealth_kill_a_gardener")
		ACTION_MANAGER_ENABLE_ACTION(HASH("ACT_stealth_kill_a_gardener"), FALSE)
	ENDIF
	
	//Action Mode
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	
	IF bZoomLevel
		SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(vehicleZoomLevel)
		
		SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, camViewModeOnFoot)
		
		PRINTLN("vehicleZoomLevel = ", ENUM_TO_INT(vehicleZoomLevel))
		PRINTLN("camViewModeOnFoot = ", ENUM_TO_INT(camViewModeOnFoot))
		
		bZoomLevel = FALSE
	ENDIF
	
	//Cover
	REPEAT COUNT_OF(covPoint) i
		REMOVE_COVER_POINT(covPoint[i])
	ENDREPEAT
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	//Navmesh Blocking
	IF iNavBlock <> -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlock)
		iNavBlock = -1
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BJXL, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PATRIOT, FALSE)
//	SET_VEHICLE_MODEL_IS_SUPPRESSED(VOLTIC, FALSE)
//	SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, FALSE)
	
	//Wanted
	SET_MAX_WANTED_LEVEL(6)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	//Roads
	SET_ROADS_BACK_TO_ORIGINAL(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>)
	SET_ROADS_BACK_TO_ORIGINAL(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>)
	SET_ROADS_BACK_TO_ORIGINAL(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>)
	SET_ROADS_BACK_TO_ORIGINAL(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>)
	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-57.644516, -1097.634033, 35.422352>>, <<-145.066711, -1156.563843, 23.026037>>, 20.0)
	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-801.065796, 187.031311, 71.605469>>, <<-797.865540, 178.343643, 74.834709>>, 9.0)
	
	//Car Gens
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-46.890217, -1105.479004, 29.436941>> - <<20.0, 20.0, 5.0>>, <<-46.890217, -1105.479004, 29.436941>> + <<20.0, 20.0, 5.0>>, TRUE)
	CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	
	//Blip
	SAFE_REMOVE_BLIP(blipWife)
	SAFE_REMOVE_BLIP(blipCoach)
	SAFE_REMOVE_BLIP(blipSon)
	SAFE_REMOVE_BLIP(blipDaughter)
	SAFE_REMOVE_BLIP(blipGardener)
	SAFE_REMOVE_BLIP(blipMichael)
	SAFE_REMOVE_BLIP(blipCar)
	SAFE_REMOVE_BLIP(blipDestination)
	SAFE_REMOVE_BLIP(blipOwner)
	SAFE_REMOVE_BLIP(blipFranklin)
	
	CLEAR_GPS_MULTI_ROUTE()
	
	//Sound
	IF sIDLeaf != -1
		STOP_SOUND(sIDLeaf)
		RELEASE_SOUND_ID(sIDLeaf)
		sIDLeaf = -1
	ENDIF
	
	//Audio Scene
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
		STOP_AUDIO_SCENE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_PHONE_SIMEON")
		STOP_AUDIO_SCENE("ARM_3_DRIVE_PHONE_SIMEON")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_CLIMB_INTO_GARDEN")
		STOP_AUDIO_SCENE("ARM_3_CLIMB_INTO_GARDEN")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TAKE_OUT_GARDENER")
		STOP_AUDIO_SCENE("ARM_3_TAKE_OUT_GARDENER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TARGET_GARDENER")
		STOP_AUDIO_SCENE("ARM_3_TARGET_GARDENER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_ENTER_HOUSE")
		STOP_AUDIO_SCENE("ARM_3_ENTER_HOUSE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_INSIDE_HOUSE")
		STOP_AUDIO_SCENE("ARM_3_INSIDE_HOUSE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_ENTER_GARAGE")
		STOP_AUDIO_SCENE("ARM_3_ENTER_GARAGE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_STEAL_CAR")
		STOP_AUDIO_SCENE("ARM_3_STEAL_CAR")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_EXIT_THROUGH_GATE")
		STOP_AUDIO_SCENE("ARM_3_EXIT_THROUGH_GATE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_DEALERSHIP")
		STOP_AUDIO_SCENE("ARM_3_DRIVE_TO_DEALERSHIP")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_RAM_DEALERSHIP")
		STOP_AUDIO_SCENE("ARM_3_RAM_DEALERSHIP")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_WINDOW_FOCUS_CAM")
		STOP_AUDIO_SCENE("ARM_3_WINDOW_FOCUS_CAM")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("ARM_3_BEAT_DOWN")
		STOP_AUDIO_SCENE("ARM_3_BEAT_DOWN")
	ENDIF
	
	//Audio
	PLAY_AUDIO(ARM3_FAIL)
	
	STOP_STREAM()
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	//Radio
	SET_USER_RADIO_CONTROL_ENABLED(TRUE)
	
	//Dialogue
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 1)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 3)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 6)
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 7)
	
	//Interface
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	ALLOW_SONAR_BLIPS(TRUE)
	
	//Taxi
	DISABLE_TAXI_HAILING(FALSE)
	
	//Cutscene State
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	//Michael Unavailable
	SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, FALSE)
	Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_M, FALSE)
	
	//Special Ability
	ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
	
	//Relationship Groups
	REMOVE_RELATIONSHIP_GROUP(relGroupBuddy)
	REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	
	//Reset Labels
	CLEAR_TRIGGERED_LABELS()
	CLEAR_LOADED_MODELS()
	CLEAR_LOADED_RECORDINGS()
	CLEAR_LOADED_ANIM_DICTS()
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	CLEAR_TEXT()
	
	SETTIMERA(0)
	
	iCutsceneStage = 0
	
	iDialogueStage = 0
	REPEAT COUNT_OF(iDialogueLineCount) i
		iDialogueLineCount[i] = -1
	ENDREPEAT
	iDialogueTimer = 0
	
	iHelpTimer = -1
	
	bInitStage = FALSE
	
	bReplaySkip = FALSE
	
	bShitSkip = FALSE
	
	bCutsceneSkipped = FALSE
	
	bPassed = FALSE	//Reset the bool
	
	IF bRestart = FALSE
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//Mission Passed
PROC missionPassed()
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
	
	CLEAR_TEXT()
	
	IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL)) AND NOT IS_ENTITY_DEAD(PLAYER_PED(CHAR_MICHAEL))
		GIVE_WEAPON_TO_PED(PLAYER_PED(CHAR_MICHAEL), wtPistol, 120, FALSE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_MICHAEL), WEAPONTYPE_UNARMED, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehGarage[0]) AND NOT IS_ENTITY_DEAD(vehGarage[0])
		SET_VEHICLE_DOORS_LOCKED(vehGarage[0], VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[0], TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(vehGarage[1]) AND NOT IS_ENTITY_DEAD(vehGarage[1])
		SET_VEHICLE_DOORS_LOCKED(vehGarage[1], VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[1], TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(vehGarage[2]) AND NOT IS_ENTITY_DEAD(vehGarage[2])
		SET_VEHICLE_DOORS_LOCKED(vehGarage[2], VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[2], TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(vehGarage[3]) AND NOT IS_ENTITY_DEAD(vehGarage[3])
		SET_VEHICLE_DOORS_LOCKED(vehGarage[3], VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[3], TRUE)
	ENDIF
	
	Mission_Flow_Mission_Passed()
	
	bPassed = TRUE
	
	MISSION_CLEANUP()
ENDPROC

/// PURPOSE:
///    if mission failed as Michael, warp Franklin to outside showroom
PROC HANDLE_FAIL_AS_MICHAEL()
	IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
		CPRINTLN(DEBUG_REPLAY, "Arm3: Failed as Michael, do warp")
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-27.8311, -1084.0056, 25.5727>>, 339.9998)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-26.5693, -1082.2946, 25.5820>>, 70.3239)
	ENDIF
ENDPROC


//Mission Failed
PROC missionFailed()
	CLEAR_PRINTS()	//CLEAR_TEXT()
	SAFE_CLEAR_HELP(TRUE)
	
	//Audio
	PLAY_AUDIO(ARM3_FAIL)
	
	SWITCH eMissionFail
		CASE failDefault
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FAIL")
		BREAK
		CASE failPlayerDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FAIL")
		BREAK
		CASE failMichaelDied
			IF eMissionObjective < stageDriveTo
				MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
			ELSE
				MISSION_FLOW_SET_FAIL_REASON("CMN_MDIED")
			ENDIF
		BREAK
		CASE failFranklinDied
			MISSION_FLOW_SET_FAIL_REASON("CMN_FDIED")
		BREAK
		CASE failSonDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
		BREAK
		CASE failWalkedInOnSon
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FNOISE")
		BREAK
		CASE failDaughterDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
		BREAK
		CASE failWalkedInOnDaughter
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FNOISE")
		BREAK
		CASE failWifeDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
		BREAK
		CASE failCoachDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
		BREAK
		CASE failGardenerDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
		BREAK
		CASE failWalkedInOnWifeCoach
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FNOISE")
		BREAK
		CASE failWalkedInOnWifeCoachTrySneak
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FNOISE2")
		BREAK
		CASE failWalkedInOnWifeCoachLivingRoom
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FNOISE3")
		BREAK
		CASE failSpotted
			MISSION_FLOW_SET_FAIL_REASON("ARM3_BLOWN")
		BREAK
		CASE failNoise
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FNOISE")
		BREAK
		CASE failLeftRoute
			MISSION_FLOW_SET_FAIL_REASON("ARM3_IGNORE")
		BREAK
		CASE failFledOwner
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FLED")
		BREAK
		CASE failCarDestroyed
			MISSION_FLOW_SET_FAIL_REASON("CMN_GENDEST")
		BREAK
		CASE failExplosion
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FEXP")
		BREAK
		CASE failWanted
			IF eMissionObjective < stageDriveTo	//Before Michael Appears
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-74.615479, -1102.602295, 24.006918>>, <<-28.391909, -1119.143555, 41.558163>>, 50.0)	//You bring cops to Simeon's deal
					MISSION_FLOW_SET_FAIL_REASON("ARM3_FDISRUPT")
				ELSE
					MISSION_FLOW_SET_FAIL_REASON("ARM3_FCOPSALERT")
				ENDIF
			ELIF eMissionObjective < stageBeatDown	//Michael has gun to Franklin
				MISSION_FLOW_SET_FAIL_REASON("ARM3_FCOPSALERT")
			ELSE	//Michael is fighting Simeon
				MISSION_FLOW_SET_FAIL_REASON("ARM3_FCOPSALERT")
			ENDIF
		BREAK
		CASE failSimeonDied
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FSIMEON")
		BREAK
		CASE failSimeonDisrupted
			MISSION_FLOW_SET_FAIL_REASON("ARM3_FDISRUPT")
		BREAK
	ENDSWITCH
	
	IF NOT IS_CUTSCENE_PLAYING()
		CLEANUP_TENNIS_PROPS()
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedGardener)
	AND IS_PED_INJURED(pedGardener)
		IF DOES_ENTITY_EXIST(objLeafblower)
		AND IS_ENTITY_ATTACHED(objLeafblower)
			DETACH_ENTITY(objLeafblower, TRUE, FALSE)
		ENDIF
	ENDIF
	
	Mission_Flow_Mission_Failed()
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	//Cutscene
	WHILE NOT HAS_CUTSCENE_FINISHED()
		STOP_CUTSCENE(TRUE)
		
		PRINTLN("STOPPING CUTSCENE...")
		
		WAIT(0)
	ENDWHILE
	
	REMOVE_CUTSCENE()
	
	WHILE HAS_CUTSCENE_LOADED()
		PRINTLN("REMOVING CUTSCENE...")
		
		WAIT(0)
	ENDWHILE

	HANDLE_FAIL_AS_MICHAEL()
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-23.302967, -1107.541504, 24.672077>>, <<-61.430489, -1093.553101, 35.486015>>, 40.0)
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-63.1009, -1092.9005, 25.5257>>, 71.4989)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-70.5736, -1088.5961, 25.5754>>, 340.4931)
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.414978, 135.218811, 57.205853>>, <<-807.604614, 199.880859, 94.741600>>, 90.0)
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-853.5109, 179.5495, 68.8114>>, 173.1056)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-855.8997, 172.3890, 66.9269>>, 355.1980)
	ENDIF
	
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
		IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1) != RFMO_STATE_END
			SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1, RFMO_STATE_ENDING)
		ENDIF
	ENDIF
	
	//SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_NORMAL) // Open the rear shutters - The shutters are now closed after Armenian 2 - SteveR LDS
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread 
ENDPROC

PROC DEATH_CHECKS()
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		eMissionFail = failPlayerDied
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		eMissionFail = failMichaelDied
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	AND IS_SCREEN_FADED_IN()
		eMissionFail = failFranklinDied
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedSon)
		eMissionFail = failSonDied
		
		IF DOES_ENTITY_EXIST(pedDaughter)
			IF NOT IS_PED_INJURED(pedDaughter)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
						TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_fail", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
					ELSE
						TASK_REACT_AND_FLEE_PED(pedDaughter, PLAYER_PED_ID())
					ENDIF
				ENDIF
				SET_PED_KEEP_TASK(pedDaughter, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedWife)
			IF NOT IS_PED_INJURED(pedWife)
				CLEAR_PED_TASKS(pedWife)
				TASK_REACT_AND_FLEE_PED(pedWife, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedWife, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCoach)
			IF NOT IS_PED_INJURED(pedCoach)
				CLEAR_PED_TASKS(pedCoach)
				TASK_REACT_AND_FLEE_PED(pedCoach, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedCoach, TRUE)
			ENDIF
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedDaughter)
		eMissionFail = failDaughterDied
		
		IF DOES_ENTITY_EXIST(pedSon)
			IF NOT IS_PED_INJURED(pedSon)
				CLEANUP_GAME_PROPS()
				
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					//TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_fail", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.250)
					INT sceneJimmyFail = CREATE_SYNCHRONIZED_SCENE(<<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>)
					TASK_SYNCHRONIZED_SCENE(pedSon, sceneJimmyFail, sAnimDictArm3, "jimmy_playingvideogame_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objGamepad, sceneJimmyFail, "jimmy_playingvideogame_fail_controller", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objHeadset, sceneJimmyFail, "jimmy_playingvideogame_fail_headset", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneJimmyFail, 0.250)
				ENDIF
				
				SET_PED_KEEP_TASK(pedSon, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedWife)
			IF NOT IS_PED_INJURED(pedWife)
				CLEAR_PED_TASKS(pedWife)
				TASK_REACT_AND_FLEE_PED(pedWife, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedWife, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCoach)
			IF NOT IS_PED_INJURED(pedCoach)
				CLEAR_PED_TASKS(pedCoach)
				TASK_REACT_AND_FLEE_PED(pedCoach, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedCoach, TRUE)
			ENDIF
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedWife)
		eMissionFail = failWifeDied
		
		IF DOES_ENTITY_EXIST(pedSon)
			IF NOT IS_PED_INJURED(pedSon)
				CLEANUP_GAME_PROPS()
				
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					//TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_fail", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.250)
					INT sceneJimmyFail = CREATE_SYNCHRONIZED_SCENE(<<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>)
					TASK_SYNCHRONIZED_SCENE(pedSon, sceneJimmyFail, sAnimDictArm3, "jimmy_playingvideogame_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objGamepad, sceneJimmyFail, "jimmy_playingvideogame_fail_controller", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objHeadset, sceneJimmyFail, "jimmy_playingvideogame_fail_headset", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneJimmyFail, 0.250)
				ENDIF
				
				SET_PED_KEEP_TASK(pedSon, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedDaughter)
			IF NOT IS_PED_INJURED(pedDaughter)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
						TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_fail", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
					ELSE
						TASK_REACT_AND_FLEE_PED(pedDaughter, PLAYER_PED_ID())
					ENDIF
				ENDIF
				SET_PED_KEEP_TASK(pedDaughter, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedCoach)
			IF NOT IS_PED_INJURED(pedCoach)
				CLEAR_PED_TASKS(pedCoach)
				TASK_REACT_AND_FLEE_PED(pedCoach, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(pedCoach, TRUE)
			ENDIF
		ENDIF
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedCoach)
		eMissionFail = failCoachDied
		
		IF DOES_ENTITY_EXIST(pedSon)
			IF NOT IS_PED_INJURED(pedSon)
				CLEANUP_GAME_PROPS()
				
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					//TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_fail", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.250)
					INT sceneJimmyFail = CREATE_SYNCHRONIZED_SCENE(<<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>)
					TASK_SYNCHRONIZED_SCENE(pedSon, sceneJimmyFail, sAnimDictArm3, "jimmy_playingvideogame_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objGamepad, sceneJimmyFail, "jimmy_playingvideogame_fail_controller", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objHeadset, sceneJimmyFail, "jimmy_playingvideogame_fail_headset", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneJimmyFail, 0.250)
				ENDIF
				
				SET_PED_KEEP_TASK(pedSon, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedDaughter)
			IF NOT IS_PED_INJURED(pedDaughter)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
						TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_fail", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
					ELSE
						TASK_REACT_AND_FLEE_PED(pedDaughter, PLAYER_PED_ID())
					ENDIF
				ENDIF
				SET_PED_KEEP_TASK(pedDaughter, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedWife)
			IF NOT IS_PED_INJURED(pedWife)
				CLEAR_PED_TASKS(pedWife)
				SET_PED_KEEP_TASK(pedWife, TRUE)
			ENDIF
		ENDIF
		
		missionFailed()
	ENDIF
	
//	IF SAFE_DEATH_CHECK_PED(pedGardener)
//		eMissionFail = failGardenerDied
//		
//		missionFailed()
//	ENDIF
	
	IF SAFE_DEATH_CHECK_PED(pedOwner)
		eMissionFail = failSimeonDied
		
		missionFailed()
	ENDIF
	
	IF SAFE_DEATH_CHECK_VEHICLE(vehCar)
	OR (DOES_ENTITY_EXIST(vehCar)
	AND NOT IS_ENTITY_DEAD(vehCar)
	AND ((IS_VEHICLE_DOOR_DAMAGED(vehCar, SC_DOOR_FRONT_LEFT)
	OR IS_VEHICLE_DOOR_DAMAGED(vehCar, SC_DOOR_FRONT_RIGHT)
	OR IS_VEHICLE_DOOR_DAMAGED(vehCar, SC_DOOR_REAR_LEFT)
	OR IS_VEHICLE_DOOR_DAMAGED(vehCar, SC_DOOR_REAR_RIGHT))))
	//OR IS_ENTITY_IN_WATER(vehCar)))
		IF NOT IS_CUTSCENE_PLAYING()
			IF eMissionObjective < cutWindowSmash
				eMissionFail = failCarDestroyed
				
				missionFailed()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-26.944561, -1103.164185, 25.434294>>, <<-59.328949, -1091.362061, 28.628263>>, 18.0)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-41.243816, -1087.122314, 25.434362>>, <<-26.133615, -1092.687378, 28.433880>>, 11.75)
		IF eMissionObjective < stageDriveTo
			eMissionFail = failExplosion
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		IF eMissionObjective >= stageClimbUp
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-741.353210, 175.701065, 44.151825>>, <<-871.869812, 171.457581, 116.676941>>, 100.0)
		OR (eMissionObjective = stageGoToMansion
		AND DOES_ENTITY_EXIST(pedOwner) AND DOES_ENTITY_EXIST(pedIntro)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-74.615479, -1102.602295, 24.006918>>, <<-28.391909, -1119.143555, 41.558163>>, 50.0))
			eMissionFail = failWanted
			
			missionFailed()
		ENDIF
	ENDIF
ENDPROC

PROC WAIT_WITH_DEATH_CHECKS(INT iWait = 0)
	INT iGameTime = GET_GAME_TIMER() + iWait	
	
	WHILE GET_GAME_TIMER() <= iGameTime
		WAIT(0)
		
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Complications")
		
		DEATH_CHECKS()
	ENDWHILE
ENDPROC

PROC LOAD_SCENE_ADV(VECTOR vCoords, FLOAT fRadius = 20.0)
	NEW_LOAD_SCENE_START_SPHERE(vCoords, fRadius)
	
	INT iTimeOut = GET_GAME_TIMER() + 20000
	
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_LOADED()
	AND GET_GAME_TIMER() < iTimeOut
		#IF IS_DEBUG_BUILD
		PRINTLN("NEW_LOAD_SCENE_START_SPHERE(", vCoords.X, ", ", vCoords.Y, ", ", vCoords.Z, ", ", fRadius, ")...")
		#ENDIF
		
		PRINTLN("GET_GAME_TIMER() = ", GET_GAME_TIMER(), " | iTimeOut = ", iTimeOut)
		
		WAIT_WITH_DEATH_CHECKS(0)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	IF GET_GAME_TIMER() > iTimeOut
		SCRIPT_ASSERT("NEW_LOAD_SCENE_START_SPHERE timed out, see log for details.")
	ENDIF
	#ENDIF
	
	NEW_LOAD_SCENE_STOP()
ENDPROC

PROC CREATE_CONVERSATION_ADV(STRING sLabel, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE, enumSubtitlesState eSubtitles = DISPLAY_SUBTITLES)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabel)
		WHILE NOT CREATE_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, eConvPriority, eSubtitles)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
	ENDIF
ENDPROC

PROC CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelSub)
		WHILE NOT CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(" CONVERSATION")
			PRINTNL()
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabel, bOnce)
	ENDIF
ENDPROC

PROC PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV(STRING sLabel, STRING sLabelSub, enumConversationPriority eConvPriority = CONV_PRIORITY_MEDIUM, BOOL bOnce = TRUE)
	IF NOT HAS_LABEL_BEEN_TRIGGERED(sLabelSub)
		WHILE NOT PLAY_SINGLE_LINE_FROM_CONVERSATION(sPedsForConversation, sConversationBlock, sLabel, sLabelSub, eConvPriority)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			PRINTSTRING("TRYING TO PLAY ") PRINTSTRING(sLabel) PRINTSTRING(", ") PRINTSTRING(sLabelSub) PRINTSTRING(" CONVERSATION")
			PRINTNL()

			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_LABEL_AS_TRIGGERED(sLabelSub, bOnce)
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_START_CUTSCENE(FLOAT fRadius = 0.0, BOOL bUseBlinders = TRUE)
	IF CAN_PLAYER_START_CUTSCENE() = TRUE
		IF fRadius <> 0.0
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), fRadius, TRUE, TRUE)
		ENDIF
		
		CLEAR_TEXT()
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,bUseBlinders)
	ENDIF
	
	RETURN CAN_PLAYER_START_CUTSCENE()
ENDFUNC

FUNC BOOL IS_CURRENT_PED_WEAPON_NOISY(PED_INDEX pedIndex)
	WEAPON_TYPE	wpType
	WEAPON_GROUP wpGroup
	
	GET_CURRENT_PED_WEAPON(pedIndex, wpType)
	wpGroup = GET_WEAPONTYPE_GROUP(wpType)
	
	IF wpGroup = WEAPONGROUP_PISTOL
	OR wpGroup = WEAPONGROUP_SMG
	OR wpGroup = WEAPONGROUP_RIFLE
	OR wpGroup = WEAPONGROUP_MG
	OR wpGroup = WEAPONGROUP_SHOTGUN
	OR wpGroup = WEAPONGROUP_SNIPER
	OR wpGroup = WEAPONGROUP_HEAVY
	OR wpGroup = WEAPONGROUP_RUBBERGUN
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CAUGHT_SNEAKING()
	IF DOES_ENTITY_EXIST(pedSon)
	OR DOES_ENTITY_EXIST(pedDaughter)
	OR DOES_ENTITY_EXIST(pedWife)
	OR DOES_ENTITY_EXIST(pedCoach)
		//Player is spotted by son
		IF NOT IS_CUTSCENE_PLAYING()
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.127502, 167.657471, 75.740723>>, <<-807.444336, 178.842728, 78.990883>>, 3.25)	//Son
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.724182, 167.141739, 75.740883>>, <<-809.218567, 173.508331, 79.240883>>, 5.0)
				IF ePrestreamCutscene <> roomSon
					IF HAS_CUTSCENE_LOADED()
					AND NOT HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_3")
						REMOVE_CUTSCENE()
					ELSE
						REQUEST_CUTSCENE("Armenian_3_mcs_3")
						
						//Request Cutscene Variations - Armenian_3_mcs_3
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jimmy", pedSon)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Jimmy", PED_COMP_BERD, 1, 0)
						ENDIF
						
						IF HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_3")
							ePrestreamCutscene = roomSon
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ePrestreamCutscene = roomSon
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.724182, 167.141739, 75.740883>>, <<-809.218567, 173.508331, 79.240883>>, 5.0)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.769714, 174.020981, 75.740738>>, <<-806.312378, 172.810135, 78.240738>>, 0.75))
			OR bPrestreamCutsceneLockedIn[roomSon]
				bPrestreamCutsceneLockedIn[roomSon] = TRUE
				
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("Armenian_3_mcs_3")
					CLEAR_TEXT()
					
					REGISTER_ENTITY_FOR_CUTSCENE(pedSon, "Jimmy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					START_CUTSCENE()
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
					
					CLEAR_AREA(<<-808.4632, 171.2454, 75.7504>>, 50.0, TRUE)
					
					SAFE_DELETE_OBJECT(objGamepad)
					SAFE_DELETE_OBJECT(objHeadset)
					
					WHILE NOT HAS_CUTSCENE_FINISHED()
						IF GET_CUTSCENE_TIME() >= 7000 - (FAIL_EFFECT_TIME + FAIL_OUT_EFFECT_TIME) - (100)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
							
							eMissionFail = failWalkedInOnSon
							
							missionFailed()
						ENDIF
						
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					eMissionFail = failWalkedInOnSon
					
					missionFailed()
				ENDIF
			ENDIF
		ENDIF
		
		//Player is spotted by daughter
		IF NOT IS_CUTSCENE_PLAYING()
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.1136, 176.5611, 73.9906>>, <<-805.7435, 185.8805, 79.0034>>, 5.0) //Daughter
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.644775, 169.060883, 75.740883>>, <<-802.533997, 176.084854, 79.240883>>, 4.0)
				IF ePrestreamCutscene <> roomDaughter
					IF HAS_CUTSCENE_LOADED()
					AND NOT HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_4")
						REMOVE_CUTSCENE()
					ELSE
						REQUEST_CUTSCENE("Armenian_3_mcs_4")
						
						//Request Cutscene Variations - Armenian_3_mcs_4
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Tracy", pedDaughter)
						ENDIF
						
						IF HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_4")
							ePrestreamCutscene = roomDaughter
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ePrestreamCutscene = roomDaughter
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.644775, 169.060883, 75.740883>>, <<-802.533997, 176.084854, 79.240883>>, 4.0)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.704712, 176.176849, 75.740738>>, <<-803.911987, 175.712753, 78.240738>>, 0.75))
			OR bPrestreamCutsceneLockedIn[roomDaughter]
				bPrestreamCutsceneLockedIn[roomDaughter] = TRUE
								
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("Armenian_3_mcs_4")
					CLEAR_TEXT()
					
					REGISTER_ENTITY_FOR_CUTSCENE(pedDaughter, "Tracy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					START_CUTSCENE()
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
					
					CLEAR_AREA(<<-801.2631, 173.2075, 75.7504>>, 50.0, TRUE)
					
					SAFE_DELETE_OBJECT(objPhone)
					
					WHILE NOT HAS_CUTSCENE_FINISHED()
						IF GET_CUTSCENE_TIME() >= 11000 - (FAIL_EFFECT_TIME + FAIL_OUT_EFFECT_TIME) - (100)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
							
							eMissionFail = failWalkedInOnDaughter
							
							missionFailed()
						ENDIF
						
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					eMissionFail = failWalkedInOnDaughter
					
					missionFailed()
				ENDIF
			ENDIF
		ENDIF
		
		//Player is spotted by wife/tennis coach
		IF NOT IS_CUTSCENE_PLAYING()
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-794.927551, 184.106979, 70.834709>>, <<-815.375916, 176.086212, 74.153091>>, 25.0)
				IF ePrestreamCutscene <> roomWifeCoach
					IF HAS_CUTSCENE_LOADED()
					AND NOT HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_5")
						REMOVE_CUTSCENE()
					ELSE
						REQUEST_CUTSCENE("Armenian_3_mcs_5")
						
						//Request Cutscene Variations - Armenian_3_mcs_5
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Amanda", pedWife)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("TennisCoach", pedCoach)
						ENDIF
						
						IF HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_5")
							ePrestreamCutscene = roomWifeCoach
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF ePrestreamCutscene = roomWifeCoach
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-795.923584,177.496185,70.834709>>, <<-799.760986,187.485031,74.605469>>, 5.5)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.222656, 185.655441, 71.605469>>, <<-804.654663, 184.058044, 75.357430>>, 2.75)
			OR HAS_LABEL_BEEN_TRIGGERED("CaughtWifeCoach")
			OR bPrestreamCutsceneLockedIn[roomWifeCoach]
				bPrestreamCutsceneLockedIn[roomWifeCoach] = TRUE
				
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("Armenian_3_mcs_5")
					CLEAR_TEXT()
					
					eMissionFail = failWalkedInOnWifeCoach
					
					REGISTER_ENTITY_FOR_CUTSCENE(pedWife, "Amanda", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedCoach, "TennisCoach", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					START_CUTSCENE()
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					IF DOES_ENTITY_EXIST(objTennisA)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
							STOP_SYNCHRONIZED_ENTITY_ANIM(objTennisA, 0.0, TRUE)
						ENDIF
						DETACH_ENTITY(objTennisA, FALSE)
						FREEZE_ENTITY_POSITION(objTennisA, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(objTennisA, <<-796.5159, 183.8585, 72.0977>>)
						SET_ENTITY_ROTATION(objTennisA, <<-17.1906, -143.6059, -99.3626>>)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objTennisB)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
							STOP_SYNCHRONIZED_ENTITY_ANIM(objTennisB, 0.0, TRUE)
						ENDIF
						DETACH_ENTITY(objTennisB, FALSE)
						FREEZE_ENTITY_POSITION(objTennisB, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(objTennisB, <<-796.4891, 183.8672, 72.1020>>)
						SET_ENTITY_ROTATION(objTennisB, <<-17.1865, -146.0179, -99.3660>>)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objBag)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
							STOP_SYNCHRONIZED_ENTITY_ANIM(objBag, 0.0, TRUE)
						ENDIF
						DETACH_ENTITY(objBag, FALSE)
						FREEZE_ENTITY_POSITION(objBag, TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(objBag, <<-796.1265, 184.2115, 71.8298>>)
						SET_ENTITY_ROTATION(objBag, <<-10.7331, 12.3450, -171.9909>>)
					ENDIF
					
					SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
					
					CLEAR_AREA(<<-801.7021, 183.1848, 71.6055>>, 50.0, TRUE)
					
					WHILE NOT HAS_CUTSCENE_FINISHED()
						IF GET_CUTSCENE_TIME() >= 10000 + 2500 - (FAIL_EFFECT_TIME + FAIL_OUT_EFFECT_TIME) - (100)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
							
							missionFailed()
						ENDIF
						
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					missionFailed()
				ENDIF
			ENDIF
		ENDIF
		
		//Player shoots weapon
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-815.675537, 173.802322, 70.683334>>, <<-793.942444, 182.043564, 81.835121>>, 30.0)
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
			AND NOT IS_PED_PLANTING_BOMB(PLAYER_PED_ID())
			AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
			AND IS_CURRENT_PED_WEAPON_NOISY(PLAYER_PED_ID())
			OR (GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intMansion
			AND IS_CALLING_ANY_CONTACT())
				WAIT_WITH_DEATH_CHECKS(200)
				
				CLEAR_TEXT()
				
				VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR vSonCoords = GET_ENTITY_COORDS(pedSon)
				VECTOR vDaughterCoords = GET_ENTITY_COORDS(pedDaughter)
				VECTOR vWifeCoords = GET_ENTITY_COORDS(pedWife)
				
				IF NOT IS_PED_INJURED(pedSon)
					CLEANUP_GAME_PROPS()
					
					IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
						//TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_fail", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.250)
						INT sceneJimmyFail = CREATE_SYNCHRONIZED_SCENE(<<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>)
						TASK_SYNCHRONIZED_SCENE(pedSon, sceneJimmyFail, sAnimDictArm3, "jimmy_playingvideogame_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objGamepad, sceneJimmyFail, "jimmy_playingvideogame_fail_controller", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objHeadset, sceneJimmyFail, "jimmy_playingvideogame_fail_headset", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_PHASE(sceneJimmyFail, 0.250)
					ENDIF
					
					SET_PED_KEEP_TASK(pedSon, TRUE)
					
					IF vPlayerCoords.Z > vSonCoords.Z - 2.0 AND vPlayerCoords.Z < vSonCoords.Z + 2.0 
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vSonCoords) < 10.0
						AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vSonCoords) < GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vDaughterCoords)
						AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vSonCoords) < GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vWifeCoords)
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedSon, "JIMMY")
							
							CREATE_CONVERSATION_ADV("ARM3_JIMF")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedDaughter)
					IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
						IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
							TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_fail", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						ELSE
							TASK_REACT_AND_FLEE_PED(pedDaughter, PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					SET_PED_KEEP_TASK(pedDaughter, TRUE)
					
					IF vPlayerCoords.Z > vDaughterCoords.Z - 2.0 AND vPlayerCoords.Z < vDaughterCoords.Z + 2.0 
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vDaughterCoords) < 10.0
						AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vDaughterCoords) < GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vSonCoords)
						AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vDaughterCoords) < GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vWifeCoords)
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedDaughter, "TRACEY")
							
							CREATE_CONVERSATION_ADV("ARM3_TRCF")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedWife)
					CLEAR_PED_TASKS(pedWife)
					TASK_REACT_AND_FLEE_PED(pedWife, PLAYER_PED_ID())
					SET_PED_KEEP_TASK(pedWife, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedCoach)
					CLEAR_PED_TASKS(pedCoach)
					TASK_REACT_AND_FLEE_PED(pedCoach, PLAYER_PED_ID())
					SET_PED_KEEP_TASK(pedCoach, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedWife)
				AND NOT IS_PED_INJURED(pedCoach)
					IF vPlayerCoords.Z > vWifeCoords.Z - 2.0 AND vPlayerCoords.Z < vWifeCoords.Z + 2.0 
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vWifeCoords) < 10.0
						AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vWifeCoords) < GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vSonCoords)
						AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vWifeCoords) < GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vDaughterCoords)
							CREATE_CONVERSATION_ADV("ARM3_WIFF")
						ENDIF
					ENDIF
				ENDIF
				
				CLEANUP_TENNIS_PROPS()
				
				WAIT_WITH_DEATH_CHECKS(2000)
				
				eMissionFail = failSpotted
				
				missionFailed()
			ENDIF
		ENDIF
		
		//Player makes too much noise
		IF eMissionObjective > stageSneakThrough
		OR (eMissionObjective = stageSneakThrough
		AND iCutsceneStage > 0)
			IF NOT IS_CUTSCENE_PLAYING()
			AND IS_SCREEN_FADED_IN()
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-815.675537, 173.802322, 70.683334>>, <<-793.942444, 182.043564, 81.835121>>, 30.0)
				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-802.759766, 168.739212, 77.240700>>, <<0.5, 0.5, 1.5>>)
					IF DOES_ENTITY_EXIST(pedSon)
						IF ((CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedSon) OR IS_PED_RAGDOLL(PLAYER_PED_ID()) OR IS_PED_JUMPING(PLAYER_PED_ID()))
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.628662, 170.721909, 75.273582>>, <<-804.664063, 176.914963, 78.248291>>, 1.5))
						OR IS_BULLET_IN_ANGLED_AREA(<<-808.930420, 173.655380, 74.240143>>, <<-806.430176, 166.974716, 79.240738>>, 4.25)
						OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedSon) - <<2.0, 2.0, 1.5>>, GET_ENTITY_COORDS(pedSon) + <<2.0, 2.0, 1.5>>, TRUE)
							CLEANUP_GAME_PROPS()
							
							IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
								//TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_fail", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.250)
								INT sceneJimmyFail = CREATE_SYNCHRONIZED_SCENE(<<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>)
								TASK_SYNCHRONIZED_SCENE(pedSon, sceneJimmyFail, sAnimDictArm3, "jimmy_playingvideogame_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(objGamepad, sceneJimmyFail, "jimmy_playingvideogame_fail_controller", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(objHeadset, sceneJimmyFail, "jimmy_playingvideogame_fail_headset", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_PHASE(sceneJimmyFail, 0.250)
							ENDIF
							
							SET_PED_KEEP_TASK(pedSon, TRUE)
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedSon, "JIMMY")
							
							CREATE_CONVERSATION_ADV("ARM3_JIMF")
							
							WAIT_WITH_DEATH_CHECKS(2800)	PRINTLN("ARM3_JIMF - 2800")
							
							eMissionFail = failNoise
							
							missionFailed()
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedDaughter)
					AND NOT IS_PED_INJURED(pedDaughter)
						IF ((CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedDaughter) OR IS_PED_RAGDOLL(PLAYER_PED_ID()) OR IS_PED_JUMPING(PLAYER_PED_ID()))
						AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.033020, 172.513489, 75.740738>>, <<-806.918396, 185.424911, 78.504135>>, 2.0)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.714233,176.140182,75.740738>>, <<-806.804749,179.297256,79.240799>>, 3.0)))
						OR IS_BULLET_IN_ANGLED_AREA(<<-802.397827, 176.155319, 74.240738>>, <<-799.630249, 168.990707, 79.240738>>, 4.0)
						OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedDaughter) - <<2.0, 2.0, 1.5>>, GET_ENTITY_COORDS(pedDaughter) + <<2.0, 2.0, 1.5>>, TRUE)
							IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
								IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
									TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_fail", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
								ELSE
									TASK_REACT_AND_FLEE_PED(pedDaughter, PLAYER_PED_ID())
								ENDIF
							ENDIF
							
							SET_PED_KEEP_TASK(pedDaughter, TRUE)
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedDaughter, "TRACEY")
							
							CREATE_CONVERSATION_ADV("ARM3_TRCF")
							
							WAIT_WITH_DEATH_CHECKS(2000)
							
							eMissionFail = failNoise
							
							missionFailed()
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedWife)
					AND NOT IS_PED_INJURED(pedWife)
					AND DOES_ENTITY_EXIST(pedCoach)
					AND NOT IS_PED_INJURED(pedCoach)
						IF (((((CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedWife) OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedCoach))
						OR IS_PED_RAGDOLL(PLAYER_PED_ID()) OR IS_PED_JUMPING(PLAYER_PED_ID()))
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-794.927551, 184.106979, 70.834709>>, <<-815.375916, 176.086212, 74.153091>>, 25.0)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.373352, 189.048904, 71.477020>>, <<-815.239929, 185.997620, 74.978661>>, 6.5))
						OR ((NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) AND GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) >= 1.0)
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.069885, 179.881195, 71.153091>>, <<-794.953186, 184.097305, 73.834709>>, 10.0)))
						AND (IS_ENTITY_IN_ANGLED_AREA(pedWife, <<-796.043823, 184.979950, 71.605469>>, <<-801.097717, 183.009598, 74.605469>>, 2.0)
						AND IS_ENTITY_IN_ANGLED_AREA(pedCoach, <<-796.043823, 184.979950, 71.605469>>, <<-801.097717, 183.009598, 74.605469>>, 2.0)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.577026, 179.869705, 71.153091>>, <<-807.613342, 183.736298, 75.804718>>, 4.25)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.888123, 183.775513, 71.347801>>, <<-805.190063, 184.442764, 73.847801>>, 3.5)))
						OR IS_BULLET_IN_ANGLED_AREA(<<-803.297241, 186.043411, 70.105469>>, <<-801.107117, 180.297043, 75.334709>>, 4.0)
						OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedWife) - <<2.0, 2.0, 1.5>>, GET_ENTITY_COORDS(pedWife) + <<2.0, 2.0, 1.5>>, TRUE)
						OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedCoach) - <<2.0, 2.0, 1.5>>, GET_ENTITY_COORDS(pedCoach) + <<2.0, 2.0, 1.5>>, TRUE)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("CaughtWifeCoach")
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-810.335632, 179.167435, 71.153091>>, <<-796.086731, 184.913010, 74.355469>>, 2.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.065796, 187.031311, 71.605469>>, <<-797.865540, 178.343643, 74.834709>>, 9.0)
									SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									
									SET_LABEL_AS_TRIGGERED("CaughtWifeCoach", TRUE)
								ELSE
									CLEAR_PED_TASKS(pedWife)
									TASK_REACT_AND_FLEE_PED(pedWife, PLAYER_PED_ID())
									SET_PED_KEEP_TASK(pedWife, TRUE)
									
									CLEAR_PED_TASKS(pedCoach)
									TASK_REACT_AND_FLEE_PED(pedCoach, PLAYER_PED_ID())
									SET_PED_KEEP_TASK(pedCoach, TRUE)
									
									CLEANUP_TENNIS_PROPS()
									
									CREATE_CONVERSATION_ADV("ARM3_WIFF")
									
									WAIT_WITH_DEATH_CHECKS(2000)
									
									eMissionFail = failNoise
									
									missionFailed()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Player shoots weapon
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-757.253113, 167.176361, 57.202972>>, <<-854.173096, 169.951111, 82.609200>>, 70.0)
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
		AND NOT IS_PED_PLANTING_BOMB(PLAYER_PED_ID())
		AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
		AND IS_CURRENT_PED_WEAPON_NOISY(PLAYER_PED_ID())
			eMissionFail = failSpotted
			
			missionFailed()
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF eMissionObjective < stageSneakThrough
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-846.891785, 183.005066, 55.978630>>, <<-767.868469, 178.762039, 97.749275>>, 28.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-843.800232, 155.699295, 50.815834>>, <<-766.042786, 152.706650, 91.474686>>, 27.0))
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-760.663513, 190.646255, 68.497238>>, <<-775.100037, 201.882339, 84.696632>>, 10.0)
				eMissionFail = failNoise
				
				missionFailed()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Mid Mission Replay
ENUM MissionReplay
	replayStart,
	replayClimb,
	replayMansion,
	replayGarage,
	replayDealer,
	replayFight,
	replayPass
ENDENUM

PROC MISSION_REPLAY(MissionReplay eMissionReplay = replayStart)
	//-	Replay Specific Position
	VECTOR vStartReplay
	FLOAT fStartReplay
	
	SWITCH eMissionReplay
		CASE replayStart
			vStartReplay = vPlayerStart
			fStartReplay = fPlayerStart
		BREAK
		CASE replayClimb
			vStartReplay = <<-851.8660, 178.7462, 68.6477>>
			fStartReplay = 262.7337
		BREAK
		CASE replayMansion
			vStartReplay = <<-802.6609, 168.6319, 75.7407>>
			fStartReplay = 31.0258
		BREAK
		CASE replayGarage
			vStartReplay = vCarStart
			fStartReplay = fCarStart
		BREAK
		CASE replayDealer
			vStartReplay = <<-72.6144, -1106.4908, 25.0423>>
			fStartReplay = 299.8838
		BREAK
		CASE replayFight
			vStartReplay = <<-58.1243, -1095.0555, 25.4345>>
			fStartReplay = 314.0250
		BREAK
		CASE replayPass
			vStartReplay = <<-63.8593, -1092.5178, 25.5585>>
			fStartReplay = 71.5067
		BREAK
	ENDSWITCH
	
	START_REPLAY_SETUP(vStartReplay, fStartReplay, FALSE)
	
	END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
	
	SWITCH eMissionReplay
		CASE replayStart
			SET_PED_POSITION(PLAYER_PED_ID(), vPlayerStart, fPlayerStart)
			
			CREATE_VEHICLE_FOR_REPLAY(vehDriveTo, <<-58.3923, -1114.5612, 25.4358>>, 74.8206, FALSE, FALSE, FALSE, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(vehDriveTo)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
			ENDIF
			
//			WHILE intGarage = NULL
//			OR NOT IS_INTERIOR_READY(intGarage)
//				IF intGarage = NULL
//					intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
//				ELSE
//					IF bPinnedGarage = FALSE
//						PIN_INTERIOR_IN_MEMORY(intGarage)
//						
//						IF NOT IS_INTERIOR_READY(intGarage)
//							PRINTLN("PINNING INTERIOR...")
//						ELSE
//							bPinnedGarage = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				WAIT(0)
//			ENDWHILE
			
//			WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
//				WAIT(0)
//			ENDWHILE
			
			//Load Scene
			//LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			
			eMissionObjective = stageGoToMansion
		BREAK
		CASE replayClimb
			SET_PED_POSITION(PLAYER_PED_ID(), <<-851.8660, 178.7462, 68.6477>>, 262.7337)
			
			CREATE_VEHICLE_FOR_REPLAY(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847, FALSE, FALSE, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(vehDriveTo)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehDriveTo)
			AND NOT IS_ENTITY_DEAD(vehDriveTo)
				SET_VEHICLE_POSITION(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847)
				SET_VEHICLE_FIXED(vehDriveTo)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehDriveTo)
				SET_VEHICLE_DOORS_SHUT(vehDriveTo, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
			ENDIF
			
//			REQUEST_MODEL(BJXL)
//			REQUEST_MODEL(PROP_HD_SEATS_01)
//			
//			WHILE NOT HAS_MODEL_LOADED(BJXL)
//			OR NOT HAS_MODEL_LOADED(PROP_HD_SEATS_01)
//				WAIT(0)
//			ENDWHILE
//			
//			SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
//			SETUP_JIMMYS_CAR(vehCar)
//			SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
//			SET_ENTITY_COORDS_NO_OFFSET(vehCar, vCarStart)
//			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
//			
//			objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
//			SET_ENTITY_COLLISION(objSeats, FALSE)
//			ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
//			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
			eMissionObjective = stageClimbUp
		BREAK
		CASE replayMansion
			CREATE_VEHICLE_FOR_REPLAY(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847, FALSE, FALSE, FALSE, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(vehDriveTo)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehDriveTo)
			AND NOT IS_ENTITY_DEAD(vehDriveTo)
				SET_VEHICLE_POSITION(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847)
				SET_VEHICLE_FIXED(vehDriveTo)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehDriveTo)
				SET_VEHICLE_DOORS_SHUT(vehDriveTo, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
			ENDIF
			
			REQUEST_MODEL(BJXL)
			//REQUEST_MODEL(PROP_HD_SEATS_01)
					
			WHILE NOT HAS_MODEL_LOADED(BJXL)
			//OR NOT HAS_MODEL_LOADED(PROP_HD_SEATS_01)
				WAIT(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
			SETUP_JIMMYS_CAR(vehCar)
			SET_VEHICLE_EXTRA(vehCar, 5, FALSE)
			SET_ENTITY_COORDS_NO_OFFSET(vehCar, vCarStart)
			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
			
//			objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
//			SET_ENTITY_COLLISION(objSeats, FALSE)
//			ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
//			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
			
			REQUEST_MODEL(BISON3)
			
			WHILE NOT HAS_MODEL_LOADED(BISON3)
				WAIT(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehClimb, BISON3, <<-800.1796, 164.9729, 70.5296>>, 111.0221, 132)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehClimb, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehClimb, VEHICLELOCK_LOCKED)
			SET_VEHICLE_ALARM(vehClimb, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(BISON3)
			
			// Reset the TV script brain.
			REACTIVATE_NAMED_OBJECT_BRAINS_WAITING_TILL_OUT_OF_RANGE("ob_tv")
			
			// Reset TV object - It's a rayfire object, so needs special treatment.
			iTVObject = GET_RAYFIRE_MAP_OBJECT( <<-809.9620, 170.9190, 75.7407>>, 3, "des_tvsmash" )
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(iTVObject)
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(iTVObject, RFMO_STATE_RESET) 
			ENDIF
			
			eMissionObjective = stageSneakThrough
		BREAK
		CASE replayGarage
			//Pin Interior
			intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
			
			PIN_INTERIOR_IN_MEMORY(intMansion)
			
			CREATE_VEHICLE_FOR_REPLAY(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847, FALSE, FALSE, FALSE, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(vehDriveTo)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehDriveTo)
			AND NOT IS_ENTITY_DEAD(vehDriveTo)
				SET_VEHICLE_POSITION(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847)
				SET_VEHICLE_FIXED(vehDriveTo)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehDriveTo)
				SET_VEHICLE_DOORS_SHUT(vehDriveTo, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
			ENDIF
			
			REQUEST_MODEL(BJXL)
			//REQUEST_MODEL(PROP_HD_SEATS_01)
			
			WHILE NOT HAS_MODEL_LOADED(BJXL)
			//OR NOT HAS_MODEL_LOADED(PROP_HD_SEATS_01)
				WAIT(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
			SETUP_JIMMYS_CAR(vehCar)
			SET_VEHICLE_EXTRA(vehCar, 5, FALSE)
			SET_ENTITY_COORDS_NO_OFFSET(vehCar, vCarStart)
			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
			
//			objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
//			SET_ENTITY_COLLISION(objSeats, FALSE)
//			ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
//			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
			
			//Mod Shop
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			eMissionObjective = stageStealCar
		BREAK
		CASE replayDealer
			REQUEST_MODEL(BJXL)
			//REQUEST_MODEL(PROP_HD_SEATS_01)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			WHILE NOT HAS_MODEL_LOADED(BJXL)
			//OR NOT HAS_MODEL_LOADED(PROP_HD_SEATS_01)
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				WAIT(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
			SETUP_JIMMYS_CAR(vehCar)
			SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
			
			//objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
			//SET_ENTITY_COLLISION(objSeats, FALSE)
			//ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
			//SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
			
			SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStart, fMichaelStart)
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_UNARMED, TRUE)
			
			IF NOT DOES_ENTITY_EXIST(objGun)
				objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL), vMichaelStart)
			ENDIF
			ATTACH_ENTITY_TO_ENTITY(objGun, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			
			SET_LABEL_AS_TRIGGERED("ARM3_GUN", TRUE)
			SET_LABEL_AS_TRIGGERED("ARM3_GUNA", TRUE)
			SET_LABEL_AS_TRIGGERED("ARM3_GUNB", TRUE)
			
			//Mod Shop
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			IF g_bShitskipAccepted = TRUE
				eMissionObjective = cutArrive
			ELSE
				eMissionObjective = stageRammingSpeed
			ENDIF
		BREAK
		CASE replayFight
			intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
			
			PIN_INTERIOR_IN_MEMORY(intGarage)
			
			WHILE NOT IS_INTERIOR_READY(intGarage)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedGarage = TRUE
			
			REQUEST_MODEL(BJXL)
			//REQUEST_MODEL(PROP_HD_SEATS_01)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_SIMEON))
			
			WHILE NOT HAS_MODEL_LOADED(BJXL)
			//OR NOT HAS_MODEL_LOADED(PROP_HD_SEATS_01)
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			OR NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_SIMEON))
				WAIT(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
			SETUP_JIMMYS_CAR(vehCar)
			SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
			
			//objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
			//SET_ENTITY_COLLISION(objSeats, FALSE)
			//ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
			//SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
			
			SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStart, fMichaelStart)
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_UNARMED, TRUE)
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			
			WHILE NOT CREATE_NPC_PED_ON_FOOT(pedOwner, CHAR_SIMEON, <<-56.4360, -1098.8176, 25.4345>>, 31.1490)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PED_SUFFERS_CRITICAL_HITS(pedOwner, FALSE)
			SET_ENTITY_HEALTH(pedOwner, 200 + 200)
			SET_PED_MAX_HEALTH(pedOwner, 200 + 200)
			TASK_LOOK_AT_ENTITY(pedOwner, vehCar, 3000)
			SET_ENTITY_INVINCIBLE(pedOwner, TRUE)
			SAFE_SET_ENTITY_VISIBLE(pedOwner, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_SIMEON))
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
			
			SET_LABEL_AS_TRIGGERED("ARM3_GUN", TRUE)
			SET_LABEL_AS_TRIGGERED("ARM3_GUNA", TRUE)
			SET_LABEL_AS_TRIGGERED("ARM3_GUNB", TRUE)
			
			//Mod Shop
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			eMissionObjective = stageBeatDown
		BREAK
		CASE replayPass
			intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
			
			PIN_INTERIOR_IN_MEMORY(intGarage)
			
			WHILE NOT IS_INTERIOR_READY(intGarage)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedGarage = TRUE
			
			REQUEST_MODEL(BJXL)
			//REQUEST_MODEL(PROP_HD_SEATS_01)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_SIMEON))
			
			WHILE NOT HAS_MODEL_LOADED(BJXL)
			//OR NOT HAS_MODEL_LOADED(PROP_HD_SEATS_01)
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			OR NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_SIMEON))
				WAIT(0)
			ENDWHILE
			
			SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
			SETUP_JIMMYS_CAR(vehCar)
			SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
			
			//objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
			//SET_ENTITY_COLLISION(objSeats, FALSE)
			//ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
			//SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
			
			SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStart, fMichaelStart)
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			
			SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_UNARMED, TRUE)
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			
			WHILE NOT CREATE_NPC_PED_ON_FOOT(pedOwner, CHAR_SIMEON, <<-56.4360, -1098.8176, 25.4345>>, 31.1490)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_PED_SUFFERS_CRITICAL_HITS(pedOwner, FALSE)
			SET_ENTITY_HEALTH(pedOwner, 200 + 200)
			SET_PED_MAX_HEALTH(pedOwner, 200 + 200)
			TASK_LOOK_AT_ENTITY(pedOwner, vehCar, 3000)
			SET_ENTITY_INVINCIBLE(pedOwner, TRUE)
			SAFE_SET_ENTITY_VISIBLE(pedOwner, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_SIMEON))
			
			ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
			
			SET_LABEL_AS_TRIGGERED("ARM3_GUN", TRUE)
			SET_LABEL_AS_TRIGGERED("ARM3_GUNA", TRUE)
			SET_LABEL_AS_TRIGGERED("ARM3_GUNB", TRUE)
			
			//Mod Shop
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			bShitSkip = TRUE
			
			eMissionObjective = stageBeatDown
		BREAK
	ENDSWITCH
	
	bSkipped = TRUE
	bReplaySkip = TRUE
	
	#IF IS_DEBUG_BUILD
	PRINTLN("eMissionObjective = ", eMissionObjective)
	#ENDIF
ENDPROC

PROC LOAD_UNLOAD_ASSETS()
	//Stages Reference
		//stageGoToMansion,
		//stageClimbUp,
		//cutArgue,
		//stageSneakThrough,
		//stageStealCar,
		//stageDriveTo,
		//cutArrive,
		//stageRammingSpeed,
		//cutWindowSmash,
		//stageBeatDown,
		//stageExitDealership,
		//passMission,
		//failMission
	
	//Mission Peds:
		//
	
	//Mission Cars:
		//
	
	//Mission Props:
		//
	
	//Request Models
		//GET_NPC_PED_MODEL(CHAR_JIMMY) - Son
		IF (eMissionObjective = stageClimbUp) //AND HAS_LABEL_BEEN_TRIGGERED("ARM3_CLIMB"))
		OR eMissionObjective >= cutArgue AND eMissionObjective <= stageSneakThrough
			HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_JIMMY))
		ENDIF
		
		//GET_NPC_PED_MODEL(CHAR_TRACEY) - Daughter
		IF (eMissionObjective = stageClimbUp) //AND HAS_LABEL_BEEN_TRIGGERED("ARM3_CLIMB"))
		OR eMissionObjective >= cutArgue AND eMissionObjective <= stageSneakThrough
			HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_TRACEY))
		ENDIF
		
		//GET_NPC_PED_MODEL(CHAR_AMANDA) - Wife
		IF eMissionObjective >= cutArgue AND eMissionObjective <= stageSneakThrough
			HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_AMANDA))
		ENDIF
		
		//GET_NPC_PED_MODEL(CHAR_TENNIS_COACH) - Coach
		IF eMissionObjective >= cutArgue AND eMissionObjective <= stageSneakThrough
			HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_TENNIS_COACH))
		ENDIF
		
		//BJXL - Car
		IF (eMissionObjective = stageGoToMansion AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMansion) < 200.0)
		OR (eMissionObjective >= stageClimbUp AND eMissionObjective <= stageBeatDown)
			IF NOT DOES_ENTITY_EXIST(vehCar)
				HAS_MODEL_LOADED_CHECK(BJXL)
				//HAS_MODEL_LOADED_CHECK(PROP_HD_SEATS_01)
			ENDIF
		ENDIF
		
		//GET_PLAYER_PED_MODEL(CHAR_MICHAEL)
		IF eMissionObjective = stageSneakThrough OR eMissionObjective = stageStealCar
			HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		ENDIF
		
		//PROP_SHOWROOM_GLASS_1B - Dealership Window
		IF eMissionObjective >= cutArrive
		AND eMissionObjective <= stageRammingSpeed
			HAS_MODEL_LOADED_CHECK(PROP_SHOWROOM_GLASS_1B)
		ENDIF
		
		//GET_NPC_PED_MODEL(CHAR_SIMEON) - Simeon
		IF eMissionObjective >= stageRammingSpeed
		AND eMissionObjective <= cutWindowSmash
			HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_SIMEON))
		ENDIF
		
		//GET_NPC_PED_MODEL(CHAR_SIMEON) - Simeon
		IF eMissionObjective >= stageSneakThrough
		AND eMissionObjective <= cutWindowSmash
			HAS_MODEL_LOADED_CHECK(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL))
		ENDIF
		
	//Vehicle Recordings
		//001
		//004
		
//		IF eMissionObjective = stageDriveTo
//			IF NOT HAS_RECORDING_LOADED_CHECK(001, sCarrec)
//				REQUEST_VEHICLE_RECORDING(001, sCarrec)
//			ENDIF
//			
//			IF NOT HAS_RECORDING_LOADED_CHECK(004, sCarrec)
//				REQUEST_VEHICLE_RECORDING(004, sCarrec)
//			ENDIF
//		ENDIF
		
	//Waypoint Recordings
	
	//Animation Dictionaries
		IF (eMissionObjective = stageGoToMansion AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMansion) < 200.0)
		OR (eMissionObjective >= stageClimbUp AND eMissionObjective <= stageRammingSpeed)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictArm3)
		ENDIF
		
		IF (eMissionObjective >= stageClimbUp AND eMissionObjective <= stageSneakThrough)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3Argue)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictArm3Argue)
		ENDIF
		
		IF (eMissionObjective >= stageClimbUp AND eMissionObjective <= cutArgue)
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3Climb)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictArm3Climb)
		ENDIF
		
		IF NOT (eMissionObjective = stageClimbUp)
			UNLOAD_ANIM_DICT(sAnimDictDoor)
		ENDIF
		
		IF eMissionObjective >= cutWindowSmash AND eMissionObjective <= stageBeatDown
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictAngry)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictAngry)
		ENDIF
		
		IF eMissionObjective >= stageRammingSpeed AND eMissionObjective <= cutWindowSmash
			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictLeadInMCS8)
		ELSE
			UNLOAD_ANIM_DICT(sAnimDictLeadInMCS8)
		ENDIF
		
//		IF eMissionObjective >= cutWindowSmash AND eMissionObjective <= stageBeatDown
//			HAS_ANIM_DICT_LOADED_CHECK(sAnimDictSlam)
//		ELSE
//			UNLOAD_ANIM_DICT(sAnimDictSlam)
//		ENDIF
		
	//Audio
		IF eMissionObjective = stageSneakThrough OR eMissionObjective = stageStealCar OR eMissionObjective = stageDriveTo
			REQUEST_SCRIPT_AUDIO_BANK("ARM_3_01")
		ENDIF
		
		IF eMissionObjective = stageRammingSpeed OR eMissionObjective = cutWindowSmash
			REQUEST_SCRIPT_AUDIO_BANK("ARM_3_02_CAR_CRASH")
		ENDIF
		
	//Switch Tutorial
//		IF eMissionObjective = stageBeatDown
//		OR eMissionObjective = stageExitDealership
//		OR eMissionObjective = stageSwitchTutorial
//			REQUEST_ANIM_DICT("amb@smoking@standing@male@enter")
//			REQUEST_ANIM_DICT("amb@smoking@standing@male@idle_a")
//			REQUEST_ANIM_DICT("weapon@w_j_rpg_aa_h_hi")
//			REQUEST_ANIM_DICT("mini@golf")
//			REQUEST_MODEL(PROP_CS_CIGGY_01)
//			REQUEST_MODEL(PROP_BSKBALL_01)
//		ENDIF
ENDPROC

FUNC FLOAT GET_HEADING_FROM_VECTOR(VECTOR vVector)
	RETURN vVector.Z
ENDFUNC

//═════════════════════════════╡ SETUP PROCEDURES ╞══════════════════════════════

//═══════════════════════════╡ OBJECTIVE PROCEDURES ╞════════════════════════════

PROC initialiseMission()
	IF INIT_STAGE()
		//INFORM_MISSION_STATS_OF_MISSION_START_ARMENIAN_THREE()
		
		//GPS
		//SET_IGNORE_NO_GPS_FLAG(TRUE)
		
		//Car Gens
		ADD_SCENARIO_BLOCKING_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_ARMENIAN_3) - <<25.0, 25.0, 15.0>>, GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_1) + <<25.0, 25.0, 15.0>>)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-46.890217, -1105.479004, 29.436941>> - <<20.0, 20.0, 5.0>>, <<-46.890217, -1105.479004, 29.436941>> + <<20.0, 20.0, 5.0>>, FALSE)
		
		IF NOT IS_CUTSCENE_PLAYING()
			CLEAR_AREA(<<-46.890217, -1105.479004, 29.436941>>, 20.0, TRUE)
		ENDIF
		
		CLEAR_AREA(<<-47.07522, -1114.47644, 25.43581>>, 7.5, TRUE)
		CLEAR_AREA(<<-40.14164, -1113.71350, 25.43738>>, 5.0, TRUE)
		CLEAR_AREA(<<-57.75611, -1112.76880, 25.43581>>, 10.0, TRUE)
		
		#IF IS_DEBUG_BUILD
		//Auto Skipping
		IF bAutoSkipping = TRUE
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		ENDIF
		#ENDIF
		
		bJimmyTVPLayingGame = FALSE
		bProjectorTVDisabled = FALSE
		
		//Prepare Mission
		CLEAR_AREA(vMansion, 500.0, TRUE)
		
		ADD_SCENARIO_BLOCKING_AREA(<<-804.983521, 166.219025, 70.562447>> - <<50.0, 35.0, 15.0>>, <<-804.983521, 166.219025, 70.562447>> + <<50.0, 35.0, 15.0>>)
		
		//Cover Blocking (Michael's house)
		ADD_COVER_BLOCKING_AREA(<<-807.365112, 173.095642, 77.240707>> - <<1.0, 1.0, 1.5>>, <<-807.365112, 173.095642, 77.240707>> + <<1.0, 1.0, 1.5>>, TRUE, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<-801.839478, 174.241882, 77.240738>> - <<1.5, 2.0, 1.5>>, <<-801.839478, 174.241882, 77.240738>> + <<1.5, 2.0, 1.5>>, TRUE, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<-797.673828, 181.633575, 73.345284>> - <<3.5, 4.0, 2.0>>, <<-797.673828, 181.633575, 73.345284>> + <<3.5, 4.0, 2.0>>, TRUE, TRUE, TRUE, TRUE)
		ADD_COVER_BLOCKING_AREA(<<-799.448975, 184.006149, 73.355469>> - <<4.5, 4.5, 2.0>>, <<-799.448975, 184.006149, 73.355469>> + <<4.5, 4.5, 2.0>>, TRUE, TRUE, TRUE, TRUE)
		
		//Request Additional Text
		REQUEST_ADDITIONAL_TEXT("ARM3", MISSION_TEXT_SLOT)
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			WAIT(0)	PRINTLN("LOADING TEXT")
		ENDWHILE
		
		//Doors
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iBathroomDoor)
			ADD_DOOR_TO_SYSTEM(iBathroomDoor, V_ILEV_MM_DOORW, <<-804.95, 171.86, 76.89>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iJimmyDoor)
			ADD_DOOR_TO_SYSTEM(iJimmyDoor, V_ILEV_MM_DOORSON, <<-806.77, 174.02, 76.89>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iTraceyDoor)
			ADD_DOOR_TO_SYSTEM(iTraceyDoor, V_ILEV_MM_DOORDAUGHTER, <<-802.70, 176.18, 76.89>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iMichaelDoor)
			ADD_DOOR_TO_SYSTEM(iMichaelDoor, V_ILEV_MM_DOORW, <<-809.28, 177.86, 76.89>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
			ADD_DOOR_TO_SYSTEM(iGarageDoor, V_ILEV_MM_DOOR, <<-806.28, 186.02, 72.62>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor1)
			ADD_DOOR_TO_SYSTEM(iDealershipDoor1, V_ILEV_FIB_DOOR1, <<-31.72, -1101.85, 26.57>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor2)
			ADD_DOOR_TO_SYSTEM(iDealershipDoor2, V_ILEV_FIB_DOOR1, <<-33.81, -1107.58, 26.57>>)
		ENDIF
		
		//Audio
		REGISTER_SCRIPT_WITH_AUDIO()
		
		//Widgets
		#IF IS_DEBUG_BUILD
			IF NOT DOES_WIDGET_GROUP_EXIST(widGroup)
				widGroup = START_WIDGET_GROUP("Armenian 3")
					START_WIDGET_GROUP("Debug Print")
						ADD_WIDGET_BOOL("Audio", bDebugAudio)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Debug Fail")
						ADD_WIDGET_INT_SLIDER("Fail Timer Override", iFailTimer, 0, 2147483647, 1000)
					STOP_WIDGET_GROUP()
//					START_WIDGET_GROUP("Debug Hood Cam")
//						ADD_WIDGET_FLOAT_SLIDER("fSpringConstant", fHoodSpringConstant, 0.0, 10000.0, 1.0)
//						ADD_WIDGET_FLOAT_SLIDER("fDampingRatio", fHoodDampingRatio, 0.0, 100.0, 0.1)
//					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
				
				SET_LOCATES_HEADER_WIDGET_GROUP(widGroup)
			ENDIF
		#ENDIF
		
		//Relationships
		ADD_RELATIONSHIP_GROUP("BUDDIES", relGroupBuddy)
		ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
		
		//Buddy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupBuddy, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupBuddy)
		
		//Enemy
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBuddy, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, relGroupBuddy)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemy)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, RELGROUPHASH_PLAYER)
		
		//Assisted Movement Routes
		ASSISTED_MOVEMENT_REQUEST_ROUTE("Mansion_1")
		
		//Vehicle Gens
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-46.890217, -1105.479004, 29.436941>> - <<20.0, 20.0, 5.0>>, <<-46.890217, -1105.479004, 29.436941>> + <<20.0, 20.0, 5.0>>, FALSE)
		
		//Suppress
		SET_VEHICLE_MODEL_IS_SUPPRESSED(BJXL, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(PATRIOT, TRUE)
//		SET_VEHICLE_MODEL_IS_SUPPRESSED(VOLTIC, TRUE)
//		SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
		
		//Car Gens
		//SET_CAR_GENERATORS_CAN_UPDATE_DURING_CUTSCENE(TRUE)
		
		//Set Player
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
			WAIT(0)	PRINTLN("SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)")
		ENDWHILE
		
		WHILE GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
			WAIT(0)	PRINTLN("GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN")
		ENDWHILE
		
		//Lock Mansion Door
		UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
		
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, DOORSTATE_LOCKED, TRUE, TRUE)
		
		//Dialogue
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, PLAYER_PED_ID(), "FRANKLIN")
		//ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, NULL, "SIMEON")
		
		IF intGarage = NULL
			intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
		ENDIF
		
		REQUEST_MODEL(TAILGATER)
		REQUEST_MODEL(BJXL)
		
		WHILE NOT HAS_MODEL_LOADED(TAILGATER)
		OR NOT HAS_MODEL_LOADED(BJXL)
			REQUEST_MODEL(TAILGATER)
			REQUEST_MODEL(BJXL)
			
			WAIT(0)	PRINTLN("HAS_MODEL_LOADED(TAILGATER) OR HAS_MODEL_LOADED(BJXL)")
		ENDWHILE
		
		IF NOT DOES_ENTITY_EXIST(vehGarage[0])
			SPAWN_VEHICLE(vehGarage[0], BJXL, <<-36.6411, -1102.1914, 25.4223>>, 154.2468)
			RETAIN_ENTITY_IN_INTERIOR(vehGarage[0], intGarage)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(vehGarage[1])
			SPAWN_VEHICLE(vehGarage[1], TAILGATER, <<-41.7113, -1100.0415, 25.4223>>, 138.7067)
			RETAIN_ENTITY_IN_INTERIOR(vehGarage[1], intGarage)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(vehGarage[2])
			SPAWN_VEHICLE(vehGarage[2], BJXL, <<-46.3951, -1097.7783, 25.4223>>, 108.3411)
			RETAIN_ENTITY_IN_INTERIOR(vehGarage[2], intGarage)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(vehGarage[3])
			SPAWN_VEHICLE(vehGarage[3], TAILGATER, <<-50.0989, -1094.5341, 25.4223>>, 88.9621)
			RETAIN_ENTITY_IN_INTERIOR(vehGarage[3], intGarage)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehGarage[0])
		AND NOT IS_ENTITY_DEAD(vehGarage[0])
			SET_VEHICLE_COLOURS(vehGarage[0], 39, 39)
			SET_VEHICLE_EXTRA_COLOURS(vehGarage[0], 29, 134)
			SET_VEHICLE_EXTRA(vehGarage[0], 10, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[0], 11, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[0], 12, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehGarage[0], VEHICLELOCK_CANNOT_ENTER)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[0], FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehGarage[1])
		AND NOT IS_ENTITY_DEAD(vehGarage[1])
			SET_VEHICLE_COLOURS(vehGarage[1], 68, 68)
			SET_VEHICLE_EXTRA_COLOURS(vehGarage[1], 68, 134)
			SET_VEHICLE_EXTRA(vehGarage[1], 1, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[1], 2, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[1], 3, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehGarage[1], VEHICLELOCK_CANNOT_ENTER)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[1], FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehGarage[2])
		AND NOT IS_ENTITY_DEAD(vehGarage[2])
			SET_VEHICLE_COLOURS(vehGarage[2], 6, 3)
			SET_VEHICLE_EXTRA_COLOURS(vehGarage[2], 10, 134)
			SET_VEHICLE_EXTRA(vehGarage[2], 10, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[2], 11, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[2], 12, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehGarage[2], VEHICLELOCK_CANNOT_ENTER)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[2], FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehGarage[3])
		AND NOT IS_ENTITY_DEAD(vehGarage[3])
			SET_VEHICLE_COLOURS(vehGarage[3], 42, 42)
			SET_VEHICLE_EXTRA_COLOURS(vehGarage[3], 42, 134)
			SET_VEHICLE_EXTRA(vehGarage[3], 1, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[3], 2, TRUE)
			SET_VEHICLE_EXTRA(vehGarage[3], 3, TRUE)
			SET_VEHICLE_DOORS_LOCKED(vehGarage[3], VEHICLELOCK_CANNOT_ENTER)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[3], FALSE)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehGarageOutside[0])
			SPAWN_VEHICLE(vehGarageOutside[0], TAILGATER, <<-57.1048, -1105.8546, 25.4364>> + (<<4.32, -1.55, 0.00>> * 0.0), 190.0401)
			SET_VEHICLE_EXTRA(vehGarageOutside[0], 1, TRUE)
			SET_VEHICLE_EXTRA(vehGarageOutside[0], 2, TRUE)
			SET_VEHICLE_EXTRA(vehGarageOutside[0], 3, TRUE)
			
			IF NOT IS_REPLAY_IN_PROGRESS()
			#IF IS_DEBUG_BUILD	AND bAutoSkipping = FALSE	#ENDIF
				SAFE_SET_ENTITY_VISIBLE(vehGarageOutside[0], FALSE)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehGarageOutside[1])
			SPAWN_VEHICLE(vehGarageOutside[1], TAILGATER, <<-57.1048, -1105.8546, 25.4364>> + (<<4.32, -1.55, 0.00>> * 1.0), 190.0401)
			SET_VEHICLE_EXTRA(vehGarageOutside[1], 1, TRUE)
			SET_VEHICLE_EXTRA(vehGarageOutside[1], 2, TRUE)
			SET_VEHICLE_EXTRA(vehGarageOutside[1], 3, TRUE)
			
			IF NOT IS_REPLAY_IN_PROGRESS()
			#IF IS_DEBUG_BUILD	AND bAutoSkipping = FALSE	#ENDIF
				SAFE_SET_ENTITY_VISIBLE(vehGarageOutside[1], FALSE)
			ENDIF
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
		
		IF SKIPPED_STAGE()
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		ADVANCE_STAGE()
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)	PRINTLN("eMissionObjective = ", eMissionObjective)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Your mission is being replayed
		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted = TRUE
	            MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE() + 1))
            ELSE
            	MISSION_REPLAY(INT_TO_ENUM(MissionReplay, GET_REPLAY_MID_MISSION_STAGE()))
            ENDIF
		ELSE //Reset Globals
			g_iArmenian3HelpText = 0
		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
			
		//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayStart), "initMission")
	ENDIF
ENDPROC

PROC cutsceneIntro()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Radar
		bRadar = FALSE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = TRUE
		#ENDIF
		
		//Drive to Vehicle
//		IF vehDriveTo = NULL
////			WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
////				WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)")	#ENDIF
////			ENDWHILE
//		ELSE
//			IF DOES_ENTITY_EXIST(vehDriveTo)
//				IF NOT IS_ENTITY_DEAD(vehDriveTo)
//					VECTOR vTemp1, vTemp2
//					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehDriveTo), vTemp1, vTemp2)
//					#IF IS_DEBUG_BUILD
//					PRINTLN("vMin = <<", vTemp1.X, ", ", vTemp1.Y, ", ", vTemp1.Z, ">>")
//					PRINTLN("vMax = <<", vTemp2.X, ", ", vTemp2.Y, ", ", vTemp2.Z, ">>")
//					PRINTLN("Total Size (vMax - vMin) = <<", vTemp2.X - vTemp1.X, ", ", vTemp2.Y - vTemp1.Y, ", ", vTemp2.Z - vTemp1.Z, ">>")
//					#ENDIF
//					IF vTemp2.X - vTemp1.X < 1.0
//						SET_VEHICLE_POSITION(vehDriveTo, vDriveTo, fDriveTo)	#IF IS_DEBUG_BUILD	PRINTLN("vTemp2.X - vTemp1.X < 1.0")	#ENDIF
//					ELIF vTemp2.X - vTemp1.X < 2.5
//						SET_VEHICLE_POSITION(vehDriveTo, <<-17.5816, -1080.1884, 25.6721>>, 126.9052)	#IF IS_DEBUG_BUILD	PRINTLN("vTemp2.X - vTemp1.X < 2.5")	#ENDIF	//<<-27.5891, -1082.9415, 25.6017>>, 69.2213
//					ELIF vTemp2.X - vTemp1.X < 4.0
//						SET_VEHICLE_POSITION(vehDriveTo, <<-17.5528, -1080.9896, 25.6721>>, 126.7335)	#IF IS_DEBUG_BUILD	PRINTLN("vTemp2.X - vTemp1.X < 4.0")	#ENDIF	//<<-25.6398, -1082.7731, 25.5949>>, 70.0363
////					ELSE
////						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDriveTo)
////						
////						vehDriveTo = NULL
////						
////						CLEAR_AREA(vDriveTo, 20.0, TRUE, TRUE)
////						
////						WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
////							WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)")	#ENDIF
////						ENDWHILE
////						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//If this runs then we must be on a TOD so it's ok to hang around and load the model
		IF NOT DOES_ENTITY_EXIST(vehIntro)
			REQUEST_MODEL(PREMIER)
			
			WHILE NOT HAS_MODEL_LOADED(PREMIER)
				WAIT(0)	PRINTLN("HAS_MODEL_LOADED(PREMIER)")
			ENDWHILE
			
			SPAWN_VEHICLE(vehIntro, PREMIER, vIntroCutscenePos, fIntroCutsceneRot)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehIntro, "vehIntro")	#ENDIF
			
			SET_VEHICLE_COLOURS(vehIntro, 43, 43)
			SET_VEHICLE_EXTRA(vehIntro, 10, TRUE)
			SET_VEHICLE_EXTRA(vehIntro, 11, TRUE)
			SET_VEHICLE_EXTRA(vehIntro, 12, TRUE)
			SET_VEHICLE_EXTRA_COLOURS(vehIntro, 43, 134)
			SET_VEHICLE_DOORS_LOCKED(vehIntro, VEHICLELOCK_CANNOT_ENTER)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehIntro, FALSE)
			
			PLAY_ENTITY_ANIM(vehIntro, "_leadout_action_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
		ENDIF
		
		//Cutscene
		IF bReplaySkip = FALSE
		AND bPreloaded = FALSE
			REQUEST_CUTSCENE("Armenian_3_int")
		ENDIF
		
		IF SKIPPED_STAGE()
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		SWITCH iCutsceneStage
			CASE 0
				IF bReplaySkip = FALSE
				AND bPreloaded = FALSE
					//Request Cutscene Variations - Armenian_3_int
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
						//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("CS_SiemonYetarian", pedOwner)
						//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("A_M_M_BevHills_02", pedIntro)
					ENDIF
				ENDIF
				
				IF bReplaySkip = TRUE
				OR bPreloaded = TRUE
				OR HAS_THIS_CUTSCENE_LOADED("Armenian_3_int")
					IF bReplaySkip = FALSE
					AND bPreloaded = FALSE
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						IF DOES_ENTITY_EXIST(pedOwner)
						AND NOT IS_PED_INJURED(pedOwner)
							REGISTER_ENTITY_FOR_CUTSCENE(pedOwner, "Siemon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_SIMEON))
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Siemon", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_SIMEON))
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedIntro)
						AND NOT IS_PED_INJURED(pedIntro)
							REGISTER_ENTITY_FOR_CUTSCENE(pedIntro, "customer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, A_M_M_BevHills_02)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "customer", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_M_BevHills_02)
						ENDIF
						
//						IF DOES_ENTITY_EXIST(vehGarage[0])
//						AND NOT IS_ENTITY_DEAD(vehGarage[0])
//							REGISTER_ENTITY_FOR_CUTSCENE(vehGarage[0], "Showroom_farleft", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, BJXL)
//						ELSE
//							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Showroom_farleft", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BJXL)
//						ENDIF
//						
//						IF DOES_ENTITY_EXIST(vehGarage[1])
//						AND NOT IS_ENTITY_DEAD(vehGarage[1])
//							REGISTER_ENTITY_FOR_CUTSCENE(vehGarage[1], "Customer_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, TAILGATER)
//						ELSE
//							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Customer_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, TAILGATER)
//						ENDIF
//						
//						IF DOES_ENTITY_EXIST(vehGarage[2])
//						AND NOT IS_ENTITY_DEAD(vehGarage[2])
//							REGISTER_ENTITY_FOR_CUTSCENE(vehGarage[2], "Showroom_3rdleft", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, BJXL)
//						ELSE
//							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Showroom_3rdleft", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BJXL)
//						ENDIF
						
						IF DOES_ENTITY_EXIST(vehIntro)
						AND NOT IS_ENTITY_DEAD(vehIntro)
							STOP_SYNCHRONIZED_ENTITY_ANIM(vehIntro, INSTANT_BLEND_OUT, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(vehIntro, "Showroom_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PREMIER)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Showroom_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
						ENDIF
						
						START_CUTSCENE()	#IF IS_DEBUG_BUILD	PRINTLN("START_CUTSCENE() - Armenian_3_int")	#ENDIF
						
						WAIT_WITH_DEATH_CHECKS(0)
						
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-36.443756, -1114.312988, 24.939146>>, <<-58.570412, -1111.051758, 37.435764>>, 20.0, <<-58.3923, -1114.5612, 25.4358>>, 74.8206, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						
						CLEAR_AREA(<<-48.88689, -1111.56702, 25.43577>>, 12.0, TRUE)
						
						SET_ROADS_IN_ANGLED_AREA(<<-801.065796, 187.031311, 71.605469>>, <<-797.865540, 178.343643, 74.834709>>, 9.0, FALSE, FALSE)
						
						STOP_GAMEPLAY_HINT(TRUE)
						
						CLEAR_TEXT()
						
						//When you want to cut the camera and go back into gameplay or the intro mocap scene call
						IF bTimeOfDayRunning
							SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
							
							bTimeOfDayComplete = TRUE
						ENDIF
						
						SAFE_SET_ENTITY_VISIBLE(vehGarageOutside[0], TRUE)
						SAFE_SET_ENTITY_VISIBLE(vehGarageOutside[1], TRUE)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						//Fade In
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ENDIF
					
					//Drive to Vehicle
//					IF vehDriveTo = NULL
////						WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
////							WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)")	#ENDIF
////						ENDWHILE
//					ELSE
//						IF DOES_ENTITY_EXIST(vehDriveTo)
//							IF NOT IS_ENTITY_DEAD(vehDriveTo)
//								VECTOR vTemp1, vTemp2
//								GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehDriveTo), vTemp1, vTemp2)
//								#IF IS_DEBUG_BUILD
//								PRINTLN("vMin = <<", vTemp1.X, ", ", vTemp1.Y, ", ", vTemp1.Z, ">>")
//								PRINTLN("vMax = <<", vTemp2.X, ", ", vTemp2.Y, ", ", vTemp2.Z, ">>")
//								PRINTLN("Total Size (vMax - vMin) = <<", vTemp2.X - vTemp1.X, ", ", vTemp2.Y - vTemp1.Y, ", ", vTemp2.Z - vTemp1.Z, ">>")
//								#ENDIF
//								IF vTemp2.X - vTemp1.X < 1.0
//									SET_VEHICLE_POSITION(vehDriveTo, vDriveTo, fDriveTo)	#IF IS_DEBUG_BUILD	PRINTLN("vTemp2.X - vTemp1.X < 1.0")	#ENDIF
//								ELIF vTemp2.X - vTemp1.X < 2.5
//									SET_VEHICLE_POSITION(vehDriveTo, <<-17.5816, -1080.1884, 25.6721>>, 126.9052)	#IF IS_DEBUG_BUILD	PRINTLN("vTemp2.X - vTemp1.X < 2.5")	#ENDIF	//<<-27.5891, -1082.9415, 25.6017>>, 69.2213
//								ELIF vTemp2.X - vTemp1.X < 4.0
//									SET_VEHICLE_POSITION(vehDriveTo, <<-17.5528, -1080.9896, 25.6721>>, 126.7335)	#IF IS_DEBUG_BUILD	PRINTLN("vTemp2.X - vTemp1.X < 4.0")	#ENDIF	//<<-25.6398, -1082.7731, 25.5949>>, 70.0363
////								ELSE
////									SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDriveTo)
////									
////									vehDriveTo = NULL
////									
////									CLEAR_AREA(vDriveTo, 20.0, TRUE, TRUE)
////									
////									WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
////										WAIT(0)	#IF IS_DEBUG_BUILD	PRINTLN("CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)")	#ENDIF
////									ENDWHILE
////									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
					
					IF DOES_ENTITY_EXIST(vehDriveTo)
					AND NOT IS_ENTITY_DEAD(vehDriveTo)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehDriveTo)
						
						ACTIVATE_PHYSICS(vehDriveTo)
					ENDIF
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT DOES_ENTITY_EXIST(pedOwner)
					ENTITY_INDEX entityOwner
					entityOwner = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Siemon")
					
					IF DOES_ENTITY_EXIST(entityOwner)
						pedOwner = GET_PED_INDEX_FROM_ENTITY_INDEX(entityOwner)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupBuddy)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedOwner, TRUE)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedIntro)
					ENTITY_INDEX entityIntro
					entityIntro = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("customer")
					
					IF DOES_ENTITY_EXIST(entityIntro)
						pedIntro = GET_PED_INDEX_FROM_ENTITY_INDEX(entityIntro)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedIntro, relGroupBuddy)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIntro, TRUE)
					ENDIF
				ENDIF
				
//				IF NOT DOES_ENTITY_EXIST(vehGarage[0])
//					IF NOT DOES_ENTITY_EXIST(vehGarage[0])
//						ENTITY_INDEX entityIntro
//						entityIntro = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_farleft")
//						
//						IF DOES_ENTITY_EXIST(entityIntro)
//							vehGarage[0] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityIntro)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehGarage[0], "vehGarage[0]")	#ENDIF
//							
//							SET_VEHICLE_COLOURS(vehGarage[0], 39, 39)
//							SET_VEHICLE_EXTRA_COLOURS(vehGarage[0], 29, 134)
//							SET_VEHICLE_DOORS_LOCKED(vehGarage[0], VEHICLELOCK_CANNOT_ENTER)
//							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[0], FALSE)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF NOT DOES_ENTITY_EXIST(vehGarage[1])
//					IF NOT DOES_ENTITY_EXIST(vehGarage[1])
//						ENTITY_INDEX entityIntro
//						entityIntro = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Customer_Car")
//						
//						IF DOES_ENTITY_EXIST(entityIntro)
//							vehGarage[1] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityIntro)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehGarage[1], "vehGarage[1]")	#ENDIF
//							
//							SET_VEHICLE_COLOURS(vehGarage[1], 68, 68)
//							SET_VEHICLE_EXTRA_COLOURS(vehGarage[1], 68, 134)
//							SET_VEHICLE_DOORS_LOCKED(vehGarage[1], VEHICLELOCK_CANNOT_ENTER)
//							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[1], FALSE)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF NOT DOES_ENTITY_EXIST(vehGarage[2])
//					IF NOT DOES_ENTITY_EXIST(vehGarage[2])
//						ENTITY_INDEX entityIntro
//						entityIntro = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_3rdleft")
//						
//						IF DOES_ENTITY_EXIST(entityIntro)
//							vehGarage[2] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityIntro)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehGarage[2], "vehGarage[2]")	#ENDIF
//							
//							SET_VEHICLE_COLOURS(vehGarage[2], 6, 3)
//							SET_VEHICLE_EXTRA_COLOURS(vehGarage[2], 10, 134)
//							SET_VEHICLE_DOORS_LOCKED(vehGarage[2], VEHICLELOCK_CANNOT_ENTER)
//							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[2], FALSE)
//						ENDIF
//					ENDIF
//				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehIntro)
					IF NOT DOES_ENTITY_EXIST(vehIntro)
						ENTITY_INDEX entityIntro
						entityIntro = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Showroom_Car")
						
						IF DOES_ENTITY_EXIST(entityIntro)
							vehIntro = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityIntro)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehIntro, "vehIntro")	#ENDIF
							
							SET_VEHICLE_COLOURS(vehIntro, 43, 43)
							SET_VEHICLE_EXTRA_COLOURS(vehIntro, 43, 134)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Showroom_Car")
					IF DOES_ENTITY_EXIST(vehIntro)
					AND NOT IS_ENTITY_DEAD(vehIntro)
						SET_VEHICLE_POSITION(vehIntro, vIntroCutscenePos, fIntroCutsceneRot)
						SET_VEHICLE_DOORS_LOCKED(vehIntro, VEHICLELOCK_CANNOT_ENTER)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehIntro, FALSE)
						
						PLAY_ENTITY_ANIM(vehIntro, "_leadout_action_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Siemon")
					IF DOES_ENTITY_EXIST(pedOwner)
					AND NOT IS_ENTITY_DEAD(pedOwner)
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
						
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3LeadInOut, "_leadout_action_simeon", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3LeadInOut, "_leadout_loop_simeon", vIntroCutscenePos, vIntroCutsceneRot, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedOwner, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
//						TASK_PLAY_ANIM_ADVANCED(pedOwner, sAnimDictArm3LeadInOut, "_leadout_loop_simeon", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedOwner)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("customer")
					IF DOES_ENTITY_EXIST(pedIntro)
					AND NOT IS_ENTITY_DEAD(pedIntro)
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, pedIntro, "MrKenneth")
						
						IF DOES_ENTITY_EXIST(vehIntro)
						AND NOT IS_ENTITY_DEAD(vehIntro)
							VECTOR scenePosition
							VECTOR sceneRotation
							
							scenePosition = VECTOR_ZERO
							sceneRotation = VECTOR_ZERO
							
							sceneLeadOutMCS1 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
							
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneLeadOutMCS1, vehIntro, GET_ENTITY_BONE_INDEX_BY_NAME(vehIntro, "seat_dside_f"))
							
							TASK_SYNCHRONIZED_SCENE(pedIntro, sceneLeadOutMCS1, sAnimDictArm3LeadInOut, "_leadout_action_customer", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneLeadOutMCS1, TRUE)
						ENDIF
						
//						TASK_PLAY_ANIM_ADVANCED(pedIntro, sAnimDictArm3LeadInOut, "_leadout_action_customer", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
//						IF DOES_ENTITY_EXIST(vehIntro)
//						AND NOT IS_ENTITY_DEAD(vehIntro)
//							SET_ENTITY_NO_COLLISION_ENTITY(pedIntro, vehIntro, TRUE)
//						ENDIF
						
//						IF DOES_ENTITY_EXIST(vehIntro)
//						AND NOT IS_ENTITY_DEAD(vehIntro)
//							SET_PED_INTO_VEHICLE(pedIntro, vehIntro)
//							TASK_PLAY_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_loop_customer", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY | AF_LOOPING)
//						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIntro)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
					
					FORCE_PED_MOTION_STATE(PLAYER_PED(CHAR_FRANKLIN), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF HAS_CUTSCENE_FINISHED()
					PRINTLN("HAS_CUTSCENE_FINISHED() = TRUE")
				ENDIF
				#ENDIF
				
				IF bReplaySkip = TRUE
				OR HAS_CUTSCENE_FINISHED()
					IF NOT DOES_ENTITY_EXIST(vehIntro)
						REQUEST_MODEL(PREMIER)
						
						WHILE NOT HAS_MODEL_LOADED(PREMIER)
							WAIT(0)
						ENDWHILE
						
						SPAWN_VEHICLE(vehIntro, PREMIER, vIntroCutscenePos, fIntroCutsceneRot)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehIntro, "vehIntro")	#ENDIF
						
						SET_VEHICLE_COLOURS(vehIntro, 43, 43)
						SET_VEHICLE_EXTRA(vehIntro, 10, TRUE)
						SET_VEHICLE_EXTRA(vehIntro, 11, TRUE)
						SET_VEHICLE_EXTRA(vehIntro, 12, TRUE)
						SET_VEHICLE_EXTRA_COLOURS(vehIntro, 43, 134)
						SET_VEHICLE_DOORS_LOCKED(vehIntro, VEHICLELOCK_CANNOT_ENTER)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehIntro, FALSE)
						
						PLAY_ENTITY_ANIM(vehIntro, "_leadout_action_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(pedOwner)
						REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_SIMEON))
						
						WHILE NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_SIMEON))
							WAIT(0)
						ENDWHILE
						
						WHILE NOT CREATE_NPC_PED_ON_FOOT(pedOwner, CHAR_SIMEON, <<-42.4751, -1110.9316, 25.4343>>, 312.8127)
							WAIT_WITH_DEATH_CHECKS(0)
						ENDWHILE
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupBuddy)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedOwner, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_SIMEON))
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
						
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3LeadInOut, "_leadout_action_simeon", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
							TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3LeadInOut, "_leadout_loop_simeon", vIntroCutscenePos, vIntroCutsceneRot, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedOwner, seqMain)
						CLEAR_SEQUENCE_TASK(seqMain)
						
//						TASK_PLAY_ANIM_ADVANCED(pedOwner, sAnimDictArm3LeadInOut, "_leadout_loop_simeon", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedOwner)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(pedIntro)
						REQUEST_MODEL(A_M_M_BevHills_02)
						
						WHILE NOT HAS_MODEL_LOADED(A_M_M_BevHills_02)
							WAIT(0)
						ENDWHILE
						
						SPAWN_PED(pedIntro, A_M_M_BevHills_02, <<-39.8746, -1100.5786, 25.4343>>, 139.6499)
						SET_PED_COMPONENT_VARIATION(pedIntro, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedIntro, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedIntro, PED_COMP_LEG, 0, 0)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedIntro, relGroupBuddy)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIntro, TRUE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BevHills_02)
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, pedIntro, "MrKenneth")
						
						IF DOES_ENTITY_EXIST(vehIntro)
						AND NOT IS_ENTITY_DEAD(vehIntro)
							VECTOR scenePosition
							VECTOR sceneRotation
							
							scenePosition = VECTOR_ZERO
							sceneRotation = VECTOR_ZERO
							
							sceneLeadOutMCS1 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
							
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneLeadOutMCS1, vehIntro, GET_ENTITY_BONE_INDEX_BY_NAME(vehIntro, "seat_dside_f"))
							
							TASK_SYNCHRONIZED_SCENE(pedIntro, sceneLeadOutMCS1, sAnimDictArm3LeadInOut, "_leadout_action_customer", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneLeadOutMCS1, TRUE)
						ENDIF
						
//						TASK_PLAY_ANIM_ADVANCED(pedIntro, sAnimDictArm3LeadInOut, "_leadout_action_customer", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
//						IF DOES_ENTITY_EXIST(vehIntro)
//						AND NOT IS_ENTITY_DEAD(vehIntro)
//							SET_ENTITY_NO_COLLISION_ENTITY(pedIntro, vehIntro, TRUE)
//						ENDIF
						
//						IF DOES_ENTITY_EXIST(vehIntro)
//						AND NOT IS_ENTITY_DEAD(vehIntro)
//							SET_PED_INTO_VEHICLE(pedIntro, vehIntro)
//							TASK_PLAY_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_loop_customer", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY | AF_LOOPING)
//						ENDIF
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIntro)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(PREMIER)
					SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
					SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	ENDIF
	
	IF CLEANUP_STAGE()
	
		REPLAY_STOP_EVENT()
	
		//Cleanup (Blips, peds, variables etc.)
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-801.065796, 187.031311, 71.605469>>, <<-797.865540, 178.343643, 74.834709>>, 9.0)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)		#IF IS_DEBUG_BUILD	PRINTLN("eMissionObjective = ", ENUM_TO_INT(eMissionObjective))	#ENDIF
	ENDIF
ENDPROC

PROC GoToMansion()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Radar
		bRadar = TRUE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
//		PRINT_ADV("ARM3_GO")
		
		//Blips
//		SAFE_ADD_BLIP_LOCATION(blipDestination, <<-853.2845, 178.8083, 68.6371>>, TRUE)
		
		//Drive to Vehicle
//		IF vehDriveTo = NULL
////			WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
////				WAIT(0)
////			ENDWHILE
//		ELSE
//			IF DOES_ENTITY_EXIST(vehDriveTo)
//				IF NOT IS_ENTITY_DEAD(vehDriveTo)
//					VECTOR vTemp1, vTemp2
//					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehDriveTo), vTemp1, vTemp2)
//					#IF IS_DEBUG_BUILD
//					PRINTLN("vMin = <<", vTemp1.X, ", ", vTemp1.Y, ", ", vTemp1.Z, ">>")
//					PRINTLN("vMax = <<", vTemp2.X, ", ", vTemp2.Y, ", ", vTemp2.Z, ">>")
//					PRINTLN("Total Size (vMax - vMin) = <<", vTemp2.X - vTemp1.X, ", ", vTemp2.Y - vTemp1.Y, ", ", vTemp2.Z - vTemp1.Z, ">>")
//					#ENDIF
//					IF vTemp2.X - vTemp1.X < 1.0
//						SET_VEHICLE_POSITION(vehDriveTo, vDriveTo, fDriveTo)
//					ELIF vTemp2.X - vTemp1.X < 2.5
//						SET_VEHICLE_POSITION(vehDriveTo, <<-17.5816, -1080.1884, 25.6721>>, 126.9052)	//<<-27.5891, -1082.9415, 25.6017>>, 69.2213
//					ELIF vTemp2.X - vTemp1.X < 4.0
//						SET_VEHICLE_POSITION(vehDriveTo, <<-17.5528, -1080.9896, 25.6721>>, 126.7335)	//<<-25.6398, -1082.7731, 25.5949>>, 70.0363
////					ELSE
////						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDriveTo)
////						
////						vehDriveTo = NULL
////						
////						CLEAR_AREA(vDriveTo, 20.0, TRUE)
////						
////						WHILE NOT CREATE_PLAYER_VEHICLE(vehDriveTo, CHAR_FRANKLIN, vDriveTo, fDriveTo, TRUE, VEHICLE_TYPE_CAR)
////							WAIT(0)
////						ENDWHILE
////						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Radar
		bRadar = TRUE
		
		IF SKIPPED_STAGE()
			//Set Positions
			SET_PED_POSITION(PLAYER_PED_ID(), <<-50.0827, -1114.9438, 25.4358>>, 88.4128)
			
			IF NOT DOES_ENTITY_EXIST(vehIntro)
				REQUEST_MODEL(PREMIER)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedOwner)
				REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_SIMEON))
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedIntro)
				REQUEST_MODEL(A_M_M_BevHills_02)
			ENDIF
			
			WHILE (NOT DOES_ENTITY_EXIST(vehIntro) AND NOT HAS_MODEL_LOADED(PREMIER))
			OR (NOT DOES_ENTITY_EXIST(pedOwner) AND NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_SIMEON)))
			OR (NOT DOES_ENTITY_EXIST(pedIntro) AND NOT HAS_MODEL_LOADED(A_M_M_BevHills_02))
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehIntro)
				REQUEST_MODEL(PREMIER)
				
				WHILE NOT HAS_MODEL_LOADED(PREMIER)
					WAIT(0)
				ENDWHILE
				
				SPAWN_VEHICLE(vehIntro, PREMIER, vIntroCutscenePos, fIntroCutsceneRot)	#IF IS_DEBUG_BUILD	SET_VEHICLE_NAME_DEBUG(vehIntro, "vehIntro")	#ENDIF
				
				SET_VEHICLE_COLOURS(vehIntro, 43, 43)
				SET_VEHICLE_EXTRA(vehIntro, 10, TRUE)
				SET_VEHICLE_EXTRA(vehIntro, 11, TRUE)
				SET_VEHICLE_EXTRA(vehIntro, 12, TRUE)
				SET_VEHICLE_EXTRA_COLOURS(vehIntro, 43, 134)
				SET_VEHICLE_DOORS_LOCKED(vehIntro, VEHICLELOCK_CANNOT_ENTER)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehIntro, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(PREMIER)
				
				PLAY_ENTITY_ANIM(vehIntro, "_leadout_action_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_IN, FALSE, TRUE, FALSE, 0.0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedOwner)
				WHILE NOT CREATE_NPC_PED_ON_FOOT(pedOwner, CHAR_SIMEON, <<-42.4751, -1110.9316, 25.4343>>, 312.8127)
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupBuddy)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedOwner, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_SIMEON))
				
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
				
				OPEN_SEQUENCE_TASK(seqMain)
					TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3LeadInOut, "_leadout_action_simeon", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3LeadInOut, "_leadout_loop_simeon", vIntroCutscenePos, vIntroCutsceneRot, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING)
				CLOSE_SEQUENCE_TASK(seqMain)
				TASK_PERFORM_SEQUENCE(pedOwner, seqMain)
				CLEAR_SEQUENCE_TASK(seqMain)
				
//				TASK_PLAY_ANIM_ADVANCED(pedOwner, sAnimDictArm3LeadInOut, "_leadout_loop_simeon", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedOwner)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_SIMEON))
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(pedIntro)
				SPAWN_PED(pedIntro, A_M_M_BevHills_02, <<-39.8746, -1100.5786, 25.4343>>, 139.6499)
				SET_PED_COMPONENT_VARIATION(pedIntro, PED_COMP_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedIntro, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedIntro, PED_COMP_LEG, 0, 0)
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedIntro, relGroupBuddy)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIntro, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BevHills_02)
				
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, pedIntro, "MrKenneth")
				
				IF DOES_ENTITY_EXIST(vehIntro)
				AND NOT IS_ENTITY_DEAD(vehIntro)
					VECTOR scenePosition
					VECTOR sceneRotation
					
					scenePosition = VECTOR_ZERO
					sceneRotation = VECTOR_ZERO
					
					sceneLeadOutMCS1 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
					
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneLeadOutMCS1, vehIntro, GET_ENTITY_BONE_INDEX_BY_NAME(vehIntro, "seat_dside_f"))
					
					TASK_SYNCHRONIZED_SCENE(pedIntro, sceneLeadOutMCS1, sAnimDictArm3LeadInOut, "_leadout_action_customer", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT )
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneLeadOutMCS1, TRUE)
				ENDIF
				
//				TASK_PLAY_ANIM_ADVANCED(pedIntro, sAnimDictArm3LeadInOut, "_leadout_action_customer", vIntroCutscenePos, vIntroCutsceneRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
//				IF DOES_ENTITY_EXIST(vehIntro)
//				AND NOT IS_ENTITY_DEAD(vehIntro)
//					SET_ENTITY_NO_COLLISION_ENTITY(pedIntro, vehIntro, TRUE)
//				ENDIF

//				IF DOES_ENTITY_EXIST(vehIntro)
//				AND NOT IS_ENTITY_DEAD(vehIntro)
//					SET_PED_INTO_VEHICLE(pedIntro, vehIntro)
//					TASK_PLAY_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_loop_customer", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY | AF_LOOPING)
//				ENDIF
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIntro)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BevHills_02)
			ENDIF
			
			//On a mission replay
			SET_CLOCK_TIME(10, 0, 0)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
			
			WAIT_WITH_DEATH_CHECKS(10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-54.782204, -1111.912720, 34.447197>>, <<40.0, 40.0, 10.0>>)
//				PRINT_HELP_ADV("ARM3HLP_CARVIEW")
//			ENDIF
//		ENDIF
		
		IF DOES_ENTITY_EXIST(pedOwner)
		AND DOES_ENTITY_EXIST(pedIntro)
		AND DOES_ENTITY_EXIST(vehIntro)
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedOwner, FALSE), <<200.0, 200.0, 10.0>>)
				IF NOT IS_ENTITY_DEAD(vehIntro)
					PRINTLN("[MF] - Removing animation from intro ped vehicle....")
					SET_VEHICLE_DOORS_LOCKED(vehIntro, VEHICLELOCK_UNLOCKED)
					STOP_ENTITY_ANIM(vehIntro,  "_leadout_loop_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_OUT)
					SET_VEHICLE_DOORS_SHUT(vehIntro, TRUE)
				ENDIF
				
				FREEZE_ENTITY_POSITION(vehIntro, FALSE)
				SAFE_DELETE_PED(pedOwner)	//SET_PED_AS_NO_LONGER_NEEDED(pedOwner)
				SAFE_DELETE_PED(pedIntro)	//SET_PED_AS_NO_LONGER_NEEDED(pedIntro)				
				SAFE_DELETE_VEHICLE(vehIntro)	//SET_VEHICLE_AS_NO_LONGER_NEEDED(vehIntro)
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedOwner, FALSE), <<15.0, 15.0, 5.0>>)
					AND NOT IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(pedOwner, FALSE) - <<7.5, 7.5, 5.0>>, GET_ENTITY_COORDS(pedOwner, FALSE) + <<7.5, 7.5, 5.0>>, TRUE)
						enumSubtitlesState eSubtitles
						
						IF NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
							eSubtitles = DISPLAY_SUBTITLES
						ELSE
							eSubtitles = DO_NOT_DISPLAY_SUBTITLES
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_INT_LO")
							CREATE_CONVERSATION_ADV("ARM3_INT_LO", CONV_PRIORITY_MEDIUM, TRUE, eSubtitles)
						ELSE
							IF NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
								IF GET_GAME_TIMER() > iDialogueTimer
									IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC1_1")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC1", "ARM3_LOC1_1")
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC1_2")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC1", "ARM3_LOC1_2")
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC1_3")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC1", "ARM3_LOC1_3")
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_HANG_01")
										CREATE_CONVERSATION_ADV("ARM3_HANG", CONV_PRIORITY_MEDIUM, FALSE)
										
										TASK_LOOK_AT_ENTITY(pedOwner, PLAYER_PED(CHAR_FRANKLIN), 2000)
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
										
										SET_LABEL_AS_TRIGGERED("ARM3_HANG_01", TRUE)
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC2_1")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC2", "ARM3_LOC2_1")
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC2_2")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC2", "ARM3_LOC2_2")
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC2_3")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC2", "ARM3_LOC2_3")
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_HANG_02")
										CREATE_CONVERSATION_ADV("ARM3_HANG", CONV_PRIORITY_MEDIUM, FALSE)
										
										TASK_LOOK_AT_ENTITY(pedOwner, PLAYER_PED(CHAR_FRANKLIN), 2000)
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
										
										SET_LABEL_AS_TRIGGERED("ARM3_HANG_02", TRUE)
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC3_1")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC3", "ARM3_LOC3_1")
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC3_2")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC3", "ARM3_LOC3_2")
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_HANG_03")
										CREATE_CONVERSATION_ADV("ARM3_HANG", CONV_PRIORITY_MEDIUM, FALSE)
										
										TASK_LOOK_AT_ENTITY(pedOwner, PLAYER_PED(CHAR_FRANKLIN), 2000)
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
										
										SET_LABEL_AS_TRIGGERED("ARM3_HANG_03", TRUE)
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC4_1")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC4", "ARM3_LOC4_1")
									ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOC4_2")
										PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_LOC4", "ARM3_LOC4_2")
										
										iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
									ELSE
										IF iDialogueLineCount[iDialogueStage] = -1 
											 iDialogueLineCount[iDialogueStage] = 4
										ELIF iDialogueLineCount[iDialogueStage] > 0
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 8000)
											
											iDialogueLineCount[iDialogueStage]--
											
											IF iDialogueStage = 0
												CREATE_CONVERSATION_ADV("ARM3_LOCS", CONV_PRIORITY_MEDIUM, FALSE)
												iDialogueStage++
											ELIF iDialogueStage = 1
												CREATE_CONVERSATION_ADV("ARM3_LOCM", CONV_PRIORITY_MEDIUM, FALSE)
												iDialogueStage = 0
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehIntro)
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehIntro, SC_DOOR_FRONT_LEFT) < 0.6 OR GET_VEHICLE_DOOR_ANGLE_RATIO(vehIntro, SC_DOOR_FRONT_LEFT) > 0.7
						SET_VEHICLE_DOOR_CONTROL(vehIntro, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.64)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("AchievedDoorAngle")
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehIntro, SC_DOOR_FRONT_LEFT) > 0.6 AND GET_VEHICLE_DOOR_ANGLE_RATIO(vehIntro, SC_DOOR_FRONT_LEFT) < 0.7
						SET_LABEL_AS_TRIGGERED("AchievedDoorAngle", TRUE)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedIntro)
//					IF IS_ENTITY_PLAYING_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_action_customer")
//					AND GET_ENTITY_ANIM_CURRENT_TIME(pedIntro, sAnimDictArm3LeadInOut, "_leadout_action_customer") >= 1.0
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeadOutMCS1)
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneLeadOutMCS1) >= 1.0
						CLEAR_PED_TASKS(pedIntro)
						IF DOES_ENTITY_EXIST(vehIntro)
						AND NOT IS_ENTITY_DEAD(vehIntro)
							SET_PED_INTO_VEHICLE(pedIntro, vehIntro)
						ENDIF
						TASK_PLAY_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_loop_customer", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIntro)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehIntro)
					IF IS_ENTITY_PLAYING_ANIM(vehIntro, sAnimDictArm3LeadInOut, "_leadout_action_simeon_car")
						IF NOT IS_PED_INJURED(pedOwner)
							IF IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictArm3LeadInOut, "_leadout_action_simeon")
								SET_ENTITY_ANIM_CURRENT_TIME(vehIntro, sAnimDictArm3LeadInOut, "_leadout_action_simeon_car", GET_ENTITY_ANIM_CURRENT_TIME(pedOwner, sAnimDictArm3LeadInOut, "_leadout_action_simeon"))
							ENDIF
						ENDIF
						
						IF GET_ENTITY_ANIM_CURRENT_TIME(vehIntro, sAnimDictArm3LeadInOut, "_leadout_action_simeon_car") >= 1.0
							PLAY_ENTITY_ANIM(vehIntro, "_leadout_loop_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_IN, TRUE, FALSE, FALSE, 0.0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
						ENDIF
					ENDIF
				ENDIF
				
				VEHICLE_INDEX vehLast = GET_PLAYERS_LAST_VEHICLE()
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR (DOES_ENTITY_EXIST(vehLast)
				AND NOT IS_ENTITY_DEAD(vehLast)
				AND IS_ENTITY_AT_COORD(vehLast, vIntroCutscenePos, <<10.0, 10.0, 10.0>>)
				AND GET_ENTITY_SPEED(vehLast) > 1.0)
				OR IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(vehIntro, FALSE) - <<3.0, 3.0, 3.0>>, GET_ENTITY_COORDS(vehIntro, FALSE) - <<3.0, 3.0, 3.0>>)
					FREEZE_ENTITY_POSITION(vehIntro, FALSE)
					
					SET_LABEL_AS_TRIGGERED("AchievedDoorAngle", FALSE)
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("AchievedDoorAngle")
						FREEZE_ENTITY_POSITION(vehIntro, TRUE)
					ENDIF
				ENDIF
				
				IF IS_BULLET_IN_ANGLED_AREA(<<-49.792606, -1109.161133, 24.935764>>, <<-31.390413, -1115.629639, 32.422321>>, 16.0)
				OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-49.792606, -1109.161133, 24.935764>>, <<-31.390413, -1115.629639, 32.422321>>, 16.0)
				OR GET_NUMBER_OF_FIRES_IN_RANGE(GET_ENTITY_COORDS(vehIntro, FALSE), 10.0) > 0
				OR ((NOT IS_ENTITY_DEAD(vehIntro)
				AND HAS_COLLISION_LOADED_AROUND_ENTITY(vehIntro))
				AND (NOT IS_ENTITY_AT_COORD(vehIntro, vIntroCutscenePos, <<0.25, 0.25, 1.0>>)
				OR (IS_PED_ON_VEHICLE(PLAYER_PED_ID())
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIntro, <<0.0, 2.15, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIntro, <<0.0, -2.07, 5.0>>), 2.0))))
				OR (NOT IS_ENTITY_DEAD(vehIntro)
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehIntro, PLAYER_PED_ID()))
				OR (NOT IS_PED_INJURED(pedOwner)
				AND (NOT IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictArm3LeadInOut, "_leadout_action_simeon")
				AND NOT IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictArm3LeadInOut, "_leadout_loop_simeon"))
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedOwner)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedOwner))
				OR (NOT IS_PED_INJURED(pedIntro)
				AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeadOutMCS1)	//NOT IS_ENTITY_PLAYING_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_action_customer")
				AND NOT (IS_ENTITY_PLAYING_ANIM(pedIntro, sAnimDictArm3LeadInOut, "_leadout_loop_customer")
				OR IS_PED_IN_VEHICLE(pedIntro, vehIntro)))
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIntro)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIntro))
				OR IS_PED_RAGDOLL(pedOwner)
				OR (IS_PED_INJURED(pedOwner) OR IS_PED_INJURED(pedIntro) OR IS_ENTITY_DEAD(vehIntro))
				OR (NOT IS_ENTITY_DEAD(vehIntro)
				AND (HAS_LABEL_BEEN_TRIGGERED("AchievedDoorAngle")
				AND (GET_VEHICLE_DOOR_ANGLE_RATIO(vehIntro, SC_DOOR_FRONT_LEFT) < 0.6 OR GET_VEHICLE_DOOR_ANGLE_RATIO(vehIntro, SC_DOOR_FRONT_LEFT) > 0.7)))
				
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeadOutMCS1)
					OR GET_SYNCHRONIZED_SCENE_PHASE(sceneLeadOutMCS1) < 0.568
				
						IF NOT IS_PED_INJURED(pedOwner)
							CLEAR_PED_TASKS(pedOwner)
							TASK_TURN_PED_TO_FACE_ENTITY(pedOwner, PLAYER_PED_ID())
							SET_PED_KEEP_TASK(pedOwner, TRUE)
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
							
							CREATE_CONVERSATION_ADV("ARM3_DISRUPT")
						ENDIF
						
						IF NOT IS_PED_INJURED(pedIntro)
						
							CLEAR_PED_TASKS(pedIntro)
							CLEAR_PED_SECONDARY_TASK(pedIntro)
	//						IF DOES_ENTITY_EXIST(vehIntro)
	//						AND NOT IS_ENTITY_DEAD(vehIntro)
	//							SET_PED_INTO_VEHICLE(pedIntro, vehIntro)
	//						ENDIF
							
							OPEN_SEQUENCE_TASK(seqMain)
								IF IS_PED_IN_ANY_VEHICLE(pedIntro)
									TASK_LEAVE_ANY_VEHICLE(NULL)
								ENDIF
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedIntro, seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							SET_PED_KEEP_TASK(pedIntro, TRUE)
						ENDIF
						
						IF NOT IS_ENTITY_DEAD(vehIntro)
							IF IS_ENTITY_PLAYING_ANIM(vehIntro, sAnimDictArm3LeadInOut, "_leadout_action_simeon_car")
								STOP_ENTITY_ANIM(vehIntro, "_leadout_action_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_OUT)
							ENDIF
							
							IF IS_ENTITY_PLAYING_ANIM(vehIntro, sAnimDictArm3LeadInOut, "_leadout_loop_simeon_car")
								STOP_ENTITY_ANIM(vehIntro, "_leadout_loop_simeon_car", sAnimDictArm3LeadInOut, INSTANT_BLEND_OUT)		
							ENDIF
						ENDIF
					
					ENDIF
					
					eMissionFail = failSimeonDisrupted
					
					missionFailed()

				ENDIF
			ENDIF
		ENDIF
		
//		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
//			SAFE_REMOVE_BLIP(blipDestination)
//			
//			PRINT_ADV("LOSE_WANTED")
//		ELSE
//			IF NOT DOES_BLIP_EXIST(blipDestination)
//				IF NOT DOES_BLIP_EXIST(blipDestination)
//					SAFE_ADD_BLIP_LOCATION(blipDestination, <<-851.6385, 178.6945, 68.6414>>, TRUE)
//				ENDIF
//				
//				SET_LABEL_AS_TRIGGERED("LOSE_WANTED", FALSE)
//			ENDIF
			
//			IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
//				IF ((IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-848.303101, 172.890610, 78.180351 + 20.0>>, <<-766.600525, 169.531998, 59.253189>>, 50.0, FALSE, TRUE)
//				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-848.897766, 184.735779, 74.680779>>, <<-850.007751, 172.118652, 66.604118>>, 15.0))
//				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-760.663513, 190.646255, 68.497238>>, <<-775.100037, 201.882339, 84.696632>>, 10.0))
//				OR NOT SAFE_IS_PLAYER_CONTROL_ON()
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF SAFE_IS_PLAYER_CONTROL_ON()
//							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//							
//							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//						ENDIF
//						
//						IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
//								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_ARV")
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					//Audio Scene
					IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
						START_AUDIO_SCENE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
					ENDIF
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMansion) < 1000.0
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMansion) > 100.0
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, NULL, "SIMEON")
					
					PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_SIMEON, "ARM3AUD", "ARM3_ARV", CONV_PRIORITY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
					
					//Audio Scene
					IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
						STOP_AUDIO_SCENE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_PHONE_SIMEON")
						START_AUDIO_SCENE("ARM_3_DRIVE_PHONE_SIMEON")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("ARM3_ARV", TRUE)
				ENDIF
			ELSE
				//Has the phone been hung up before it could be answered?
				IF CHECK_CELLPHONE_LAST_CALL_REJECTED() 
				OR WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("RejectedCall")
						SETTIMERB(0)
						SET_LABEL_AS_TRIGGERED("RejectedCall", TRUE)
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("RejectedCall")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_CALLTXT")
					AND TIMERB() > 2000 
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_SIMEON, "ARM3_CALLTXT", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
						SET_LABEL_AS_TRIGGERED("ARM3_CALLTXT", TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//Spawn Climb Car
			IF NOT DOES_ENTITY_EXIST(vehClimb)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMansion) < 150.0
					IF HAS_MODEL_LOADED_CHECK(BISON3)
						SPAWN_VEHICLE(vehClimb, BISON3, <<-800.1796, 164.9729, 70.5296>>, 111.0221, 132)
						SET_VEHICLE_DOORS_LOCKED(vehClimb, VEHICLELOCK_LOCKED)
						SET_VEHICLE_ALARM(vehClimb, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			BOOL bPlayerHasArrivedMansion
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-809.689331, 167.738159, 80.499176>>, <<55.0, 35.0, 25.0>>)
				bPlayerHasArrivedMansion = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-847.812927, 182.998001, 66.362663>>, <<-811.278198, 180.250854, 81.592949>>, 30.0)
										   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-843.973389, 159.883316, 65.309196>>, <<-809.686829, 159.163422, 76.786880>>, 22.0)
										   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.617554,161.953323,59.905128>>, <<-765.113220,168.867889,104.219299>>, 40.0)
										   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-772.186829,136.951172,65.974167>>, <<-772.276367,151.575790,101.474510>>, 20.5)
										   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-823.994324,190.366592,71.401894>>, <<-773.834778,189.717102,107.483505>>, 10.0)
										   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-776.787048,185.335403,71.335213>>, <<-770.144592,190.454834,107.858650>>, 10.0)
										   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-850.643372,145.596069,58.643929>>, <<-783.199402,146.322983,97.667755>>, 10.0)
			ENDIF
			
			#IF IS_DEBUG_BUILD	DONT_DO_J_SKIP(sLocatesData)	#ENDIF
			IF IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, <<-853.2845, 178.8083, 68.6371>>, <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_HEIGHT>>, TRUE, "ARM3_GO", TRUE, TRUE)
			OR bPlayerHasArrivedMansion
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-741.353210, 175.701065, 44.151825>>, <<-871.869812, 171.457581, 116.676941>>, 100.0)	//<<-864.316040, 170.810455, 56.566391>>, <<-757.057434, 167.819778, 77.386490 + 20.0>>, 55.0, FALSE, TRUE)
//			OR NOT SAFE_IS_PLAYER_CONTROL_ON()
//				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				AND IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID()))
					ADVANCE_STAGE()
//				ENDIF
			ENDIF
//		ENDIF
		
		IF intGarage <> NULL
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intGarage
				IF vehGarage[0] = NULL
				OR NOT DOES_ENTITY_EXIST(vehGarage[0])
					vehGarage[0] = GET_CLOSEST_VEHICLE(<<-36.5853, -1101.4738, 26.3444>>, 5.0, BJXL, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				ELIF DOES_ENTITY_EXIST(vehGarage[0])
				AND NOT IS_ENTITY_DEAD(vehGarage[0])
					SET_VEHICLE_DOORS_LOCKED(vehGarage[0], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[0], FALSE)
				ENDIF
				
				IF vehGarage[1] = NULL
				OR NOT DOES_ENTITY_EXIST(vehGarage[1])
					vehGarage[1] = GET_CLOSEST_VEHICLE(<<-41.4259, -1099.6481, 26.0534>>, 5.0, TAILGATER, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				ELIF DOES_ENTITY_EXIST(vehGarage[1])
				AND NOT IS_ENTITY_DEAD(vehGarage[1])
					SET_VEHICLE_DOORS_LOCKED(vehGarage[1], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[1], FALSE)
				ENDIF
				
				IF vehGarage[2] = NULL
				OR NOT DOES_ENTITY_EXIST(vehGarage[2])
					vehGarage[2] = GET_CLOSEST_VEHICLE(<<-46.2594, -1097.8386, 26.3444>>, 5.0, BJXL, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				ELIF DOES_ENTITY_EXIST(vehGarage[2])
				AND NOT IS_ENTITY_DEAD(vehGarage[2])
					SET_VEHICLE_DOORS_LOCKED(vehGarage[2], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[2], FALSE)
				ENDIF
				
				IF vehGarage[3] = NULL
				OR NOT DOES_ENTITY_EXIST(vehGarage[3])
					vehGarage[3] = GET_CLOSEST_VEHICLE(<<-50.0800, -1094.4625, 26.0671>>, 5.0, TAILGATER, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				ELIF DOES_ENTITY_EXIST(vehGarage[3])
				AND NOT IS_ENTITY_DEAD(vehGarage[3])
					SET_VEHICLE_DOORS_LOCKED(vehGarage[3], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[3], FALSE)
				ENDIF
				
				IF vehGarage[4] = NULL
				OR NOT DOES_ENTITY_EXIST(vehGarage[4])
					vehGarage[4] = GET_CLOSEST_VEHICLE(<<-37.4128, -1088.5618, 26.0671>>, 5.0, TAILGATER, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				ELIF DOES_ENTITY_EXIST(vehGarage[4])
				AND NOT IS_ENTITY_DEAD(vehGarage[4])
					SET_VEHICLE_DOORS_LOCKED(vehGarage[4], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehGarage[4], FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData, TRUE)
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
			STOP_AUDIO_SCENE("ARM_3_DRIVE_TO_MICHAELS_HOUSE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_PHONE_SIMEON")
			STOP_AUDIO_SCENE("ARM_3_DRIVE_PHONE_SIMEON")
		ENDIF
		
		SAFE_DELETE_PED(pedIntro)
		SAFE_DELETE_PED(pedOwner)
		
//		#IF IS_DEBUG_BUILD
//		IF bAutoSkipping = FALSE
//		#ENDIF
//		
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
//			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//		ENDIF
//		
//		#IF IS_DEBUG_BUILD
//		ENDIF
//		#ENDIF
		
		CLEAR_PRINTS()
		
		SAFE_REMOVE_BLIP(blipCar)
		
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 8)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC ClimbUp()
	IF INIT_STAGE()
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		//Special Ability
		SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayClimb), "stageClimbUp", FALSE, FALSE, PLAYER_PED_ID())
		
		//Fetch vehPlayer
		IF NOT DOES_ENTITY_EXIST(vehDriveTo)
			IF GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()) != NULL
				vehDriveTo = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())	#IF IS_DEBUG_BUILD	PRINTLN("vehDriveTo = ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehDriveTo))	#ENDIF
				SET_VEHICLE_DOORS_SHUT(vehDriveTo, TRUE)
				SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
			ELIF GET_PLAYERS_LAST_VEHICLE() != NULL
				vehDriveTo = GET_PLAYERS_LAST_VEHICLE()
				IF (IS_ENTITY_DEAD(vehDriveTo)
				OR NOT IS_VEHICLE_DRIVEABLE(vehDriveTo))
				OR (NOT IS_ENTITY_DEAD(vehDriveTo)
				AND IS_VEHICLE_DRIVEABLE(vehDriveTo)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehDriveTo)) > 75.0)
					#IF IS_DEBUG_BUILD	PRINTLN("vehDriveTo = ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehDriveTo), " but is too far away (", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehDriveTo)), ") or destroyed (", IS_ENTITY_DEAD(vehDriveTo), ").")	#ENDIF
					#IF IS_DEBUG_BUILD	
					VECTOR vTemp = GET_ENTITY_COORDS(vehDriveTo)
					PRINTLN("vehDriveTo = <<", vTemp.X, ", ", vTemp.Y, ", ", vTemp.Z, ">>")	
					#ENDIF
					
					vehDriveTo = NULL
				ELSE
					#IF IS_DEBUG_BUILD	PRINTLN("vehDriveTo = ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehDriveTo))	#ENDIF
					//SET_VEHICLE_DOORS_SHUT(vehDriveTo, TRUE)
					SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehDriveTo)
		AND NOT IS_ENTITY_DEAD(vehDriveTo)
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehDriveTo)
		ENDIF
		
		bPlayerHasEnteredMansion = FALSE
		
		//Radar
		bRadar = TRUE
		
		DISPLAY_RADAR(TRUE)
		DISPLAY_HUD(TRUE)
		
		//Spawn Climb Car
		IF NOT DOES_ENTITY_EXIST(vehClimb)
			SPAWN_VEHICLE(vehClimb, BISON3, <<-800.1796, 164.9729, 70.5296>>, 111.0221, 132)
			SET_VEHICLE_DOORS_LOCKED(vehClimb, VEHICLELOCK_LOCKED)
			SET_VEHICLE_ALARM(vehClimb, TRUE)
		ENDIF
		
		//Cleanup Climb Car
		SET_MODEL_AS_NO_LONGER_NEEDED(BISON3)
		
		//Spawn Car
		SPAWN_VEHICLE(vehCar, BJXL, vCarStart, fCarStart, 126)
		SETUP_JIMMYS_CAR(vehCar)
		SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
		SET_ENTITY_COORDS_NO_OFFSET(vehCar, vCarStart)
		
		//objSeats = CREATE_OBJECT(PROP_HD_SEATS_01, vCarStart)
		//SET_ENTITY_COLLISION(objSeats, FALSE)
		//ATTACH_ENTITY_TO_ENTITY(objSeats, vehCar, 0, VECTOR_ZERO, VECTOR_ZERO)
		
		//Cleanup Car
		SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
		//SET_MODEL_AS_NO_LONGER_NEEDED(PROP_HD_SEATS_01)
		
		//Retain Car
//		INTERIOR_INSTANCE_INDEX GarageInterior = GET_INTERIOR_AT_COORDS(vCarStart)
//		
//		IF GarageInterior <> NULL
//			RETAIN_ENTITY_IN_INTERIOR(vehCar, GarageInterior)
//		ELSE
//			SCRIPT_ASSERT("Armenian3 - failed to find garage interior")
//		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_CLIMB_INTO_GARDEN")
			START_AUDIO_SCENE("ARM_3_CLIMB_INTO_GARDEN")
		ENDIF
		
		//Audio
		WHILE NOT REQUEST_SCRIPT_AUDIO_BANK("ARM_3_03")
			#IF IS_DEBUG_BUILD	PRINTLN("LOADING AUDIO BANK = ARM_3_03")	#ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		//Blips
		SAFE_REMOVE_BLIP(blipDestination)
		SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)	//SAFE_ADD_BLIP_LOCATION(blipDestination, vClimbPoint + <<0.0, 0.0, 5.0 + 5.0>>, FALSE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Set Positions
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_POSITION(PLAYER_PED_ID(), <<-851.8660, 178.7462, 68.6477>>, 262.7337)
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<-851.8660, 178.7462, 68.6477>>, 50.0)
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			WAIT_WITH_DEATH_CHECKS(1500)
			
			//Audio
			PLAY_AUDIO(ARM3_RESTART_1)
			
			//Set Franklin Unarmed
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			CLEAR_AREA(<<-851.8660, 178.7462, 68.6477>>, 10.0, TRUE)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		//Objective Text
		IF IS_SCREEN_FADED_IN()
			IF PRINT_ADV("ARM3_FIND")
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 9.0, REPLAY_IMPORTANCE_HIGH)
			ENDIF
		ENDIF
		
		SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_FORCE_LOCKED_THIS_FRAME) //Big metal garage door
		
		//Audio
		IF NOT HAS_LABEL_BEEN_TRIGGERED("AudioTrackStart")
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND ((IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-852.523376, 171.691620, 56.972511>>, <<-816.684448, 169.059662, 90.509804>>, 50.75)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-762.316162, 171.466705, 58.854210>>, <<-821.178406, 164.802994, 90.092186>>, 45.0))
			OR bPlayerHasEnteredMansion)
				PLAY_AUDIO(ARM3_START)
				
				SET_LABEL_AS_TRIGGERED("AudioTrackStart", TRUE)
			ENDIF
		ENDIF
		
		//Ragdoll Door Try Anim Blocking
		IF IS_PED_RAGDOLL(PLAYER_PED_ID())
			SET_LABEL_AS_TRIGGERED("RagdollBlock", TRUE)
		ELIF HAS_LABEL_BEEN_TRIGGERED("RagdollBlock")
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.040466, 180.161285, 70.227814>>, <<-816.377197, 175.832733, 75.227814>>, 5.0)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-797.383240, 175.260773, 71.834908>>, <<-792.335388, 177.306122, 74.834908>>, 4.0)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-791.347900, 179.441803, 71.834908>>, <<-793.324829, 184.544281, 74.834908>>, 4.0)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-816.472473, 182.131744, 70.827103>>, <<-818.861328, 188.388443, 75.319893>>, 5.0)
				SET_LABEL_AS_TRIGGERED("RagdollBlock", FALSE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TryFrontDoor1")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-817.4048, 178.4506, 71.2278 - 10.0>>, <<100.0, 100.0, 100.0>>)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDoor)
				AND NOT HAS_LABEL_BEEN_TRIGGERED("RagdollBlock")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.040466, 180.161285, 71.227814>>, <<-816.377197, 175.832733, 75.227814>>, 2.0)
					OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.040466, 180.161285, 70.227814>>, <<-816.377197, 175.832733, 75.227814>>, 5.0)
					AND (GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) > 1.0 AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
					AND (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 217.7497 OR GET_ENTITY_HEADING(PLAYER_PED_ID()) < 9.0051))
					AND NOT IS_PHONE_ONSCREEN()
						IF IS_PHONE_ONSCREEN()
							HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED, TRUE)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>)))
						ELSE
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "lockeddoor_tryopen", <<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictDoor, "lockeddoor_tryopen", <<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>)))
						ENDIF
						
						iAutowalkTimeout = GET_GAME_TIMER() + 10000
						
						SET_LABEL_AS_TRIGGERED("TryFrontDoor1", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOCK_1")
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor1)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					IF NOT IS_PED_RUNNING(PLAYER_PED_ID()) OR IS_PED_SPRINTING(PLAYER_PED_ID())
						IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>), <<0.2, 0.2, 3.0>>) AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "lockeddoor_tryopen", <<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>), <<0.2, 0.2, 3.0>>)
							sceneTryDoor1 = CREATE_SYNCHRONIZED_SCENE(<<-816.445, 178.149, 71.270>>, <<0.0, 0.0, -70.0>>)
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor1, sAnimDictDoor, "LockedDoor_TryOpen_Stealth", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ELSE
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor1, sAnimDictDoor, "lockeddoor_tryopen", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ENDIF
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneTryDoor1, FALSE)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneTryDoor1, FALSE)
							
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.5)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor1)
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
//				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//				ENDIF
			ENDIF
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor1)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneTryDoor1) >= 0.9)
			OR GET_GAME_TIMER() > iAutowalkTimeout
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				SET_LABEL_AS_TRIGGERED("ARM3_LOCK_1", TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TryFrontDoor2")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-795.440, 177.572, 71.825 - 10.0>>, <<100.0, 100.0, 100.0>>)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDoor)
				AND NOT HAS_LABEL_BEEN_TRIGGERED("RagdollBlock")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-796.575806, 177.106079, 71.835190>>, <<-794.429504, 177.950287, 75.335190>>, 2.0)
					OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-797.383240, 175.260773, 71.834908>>, <<-792.335388, 177.306122, 74.834908>>, 4.0)
					AND (GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) > 1.0 AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
					AND (GET_ENTITY_HEADING(PLAYER_PED_ID()) < 107.1689 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > 32.9229))
					AND NOT IS_PHONE_ONSCREEN()
						IF IS_PHONE_ONSCREEN()
							HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED, TRUE)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>)))
						ELSE
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "lockeddoor_tryopen", <<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictDoor, "lockeddoor_tryopen", <<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>)))
						ENDIF
						
						iAutowalkTimeout = GET_GAME_TIMER() + 10000
						
						SET_LABEL_AS_TRIGGERED("TryFrontDoor2", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOCK_2")
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor2)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					IF NOT IS_PED_RUNNING(PLAYER_PED_ID()) OR IS_PED_SPRINTING(PLAYER_PED_ID())
						IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>), <<0.2, 0.2, 3.0>>) AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "lockeddoor_tryopen", <<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>), <<0.2, 0.2, 3.0>>)
							sceneTryDoor2 = CREATE_SYNCHRONIZED_SCENE(<<-795.440, 177.572, 71.825>>, <<0.0, 0.0, 18.802>>)
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor2, sAnimDictDoor, "LockedDoor_TryOpen_Stealth", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ELSE
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor2, sAnimDictDoor, "lockeddoor_tryopen", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ENDIF
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneTryDoor2, FALSE)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneTryDoor2, FALSE)
							
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.5)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor2)
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
//				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//				ENDIF
			ENDIF
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor2)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneTryDoor2) >= 0.9)
			OR GET_GAME_TIMER() > iAutowalkTimeout
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				SET_LABEL_AS_TRIGGERED("ARM3_LOCK_2", TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TryFrontDoor3")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-793.703, 181.647, 71.825 - 10.0>>, <<100.0, 100.0, 100.0>>)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDoor)
				AND NOT HAS_LABEL_BEEN_TRIGGERED("RagdollBlock")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-793.298889, 180.489944, 71.835190>>, <<-794.130127, 182.651276, 75.335190>>, 2.0)
					OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-791.347900, 179.441803, 71.834908>>, <<-793.324829, 184.544281, 74.834908>>, 4.0)
					AND (GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) > 1.0 AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
					AND (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 34.2561 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 176.6008))
					AND NOT IS_PHONE_ONSCREEN()
						IF IS_PHONE_ONSCREEN()
							HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED, TRUE)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>)))
						ELSE
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "lockeddoor_tryopen", <<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictDoor, "lockeddoor_tryopen", <<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>)))
						ENDIF
						
						iAutowalkTimeout = GET_GAME_TIMER() + 10000
						
						SET_LABEL_AS_TRIGGERED("TryFrontDoor3", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOCK_3")
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor3)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					IF NOT IS_PED_RUNNING(PLAYER_PED_ID()) OR IS_PED_SPRINTING(PLAYER_PED_ID())
						IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "LockedDoor_TryOpen_Stealth", <<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>), <<0.2, 0.2, 3.0>>) AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictDoor, "lockeddoor_tryopen", <<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>), <<0.2, 0.2, 3.0>>)
							sceneTryDoor3 = CREATE_SYNCHRONIZED_SCENE(<<-793.703, 181.647, 71.825>>, <<0.0, 0.0, 100.0>>)
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor3, sAnimDictDoor, "LockedDoor_TryOpen_Stealth", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ELSE
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor3, sAnimDictDoor, "lockeddoor_tryopen", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ENDIF
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneTryDoor3, FALSE)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneTryDoor3, FALSE)
							
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.4)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor3)
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
//				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//				ENDIF
			ENDIF
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor3)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneTryDoor3) >= 0.9)
			OR GET_GAME_TIMER() > iAutowalkTimeout
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				SET_LABEL_AS_TRIGGERED("ARM3_LOCK_3", TRUE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TryFrontDoor4")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-793.703, 181.647, 71.825 - 10.0>>, <<100.0, 100.0, 100.0>>)
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictDoor)
				AND NOT HAS_LABEL_BEEN_TRIGGERED("RagdollBlock")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-816.382141, 188.409729, 71.478989>>, <<-815.038452, 182.721573, 74.925827>>, 2.5)
					OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-816.472473, 182.131744, 70.827103>>, <<-818.861328, 188.388443, 75.319893>>, 5.0)
					AND (GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) > 1.0 AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
					AND (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 217.7497 OR GET_ENTITY_HEADING(PLAYER_PED_ID()) < 9.0051))
					AND NOT IS_PHONE_ONSCREEN()
						IF IS_PHONE_ONSCREEN()
							HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED(CHAR_FRANKLIN), WEAPONTYPE_UNARMED, TRUE)
						
						IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3, "open_garage_fail_stealth", <<-815.940, 185.655, 72.4578>>, <<0.0, 0.0, -69.0>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictArm3, "open_garage_fail_stealth", <<-815.940, 185.655, 72.4578>>, <<0.0, 0.0, -69.0>>)))
						ELSE
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3, "open_garage_fail", <<-815.840, 185.695, 72.465>>, <<0.0, 0.0, -69.0>>), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictArm3, "open_garage_fail",  <<-815.840, 185.695, 72.465>>, <<0.0, 0.0, -69.0>>)))
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.5)
						
						iAutowalkTimeout = GET_GAME_TIMER() + 10000
						
						SET_LABEL_AS_TRIGGERED("TryFrontDoor4", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_LOCK_4")
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor4)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					IF NOT IS_PED_RUNNING(PLAYER_PED_ID()) OR IS_PED_SPRINTING(PLAYER_PED_ID())
						IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3, "open_garage_fail_stealth", <<-815.940, 185.655, 72.4578>>, <<0.0, 0.0, -69.0>>), <<0.2, 0.2, 3.0>>) AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3, "open_garage_fail", <<-815.840, 185.695, 72.465>>, <<0.0, 0.0, -69.0>>), <<0.2, 0.2, 3.0>>)
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								sceneTryDoor4 = CREATE_SYNCHRONIZED_SCENE(<<-815.940, 185.655, 72.4578>>, <<0.0, 0.0, -69.0>>)
							ELSE
								sceneTryDoor4 = CREATE_SYNCHRONIZED_SCENE(<<-815.840, 185.695, 72.465>>, <<0.0, 0.0, -69.0>>)
							ENDIF
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor4, sAnimDictArm3, "open_garage_fail_stealth", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ELSE
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneTryDoor4, sAnimDictArm3, "open_garage_fail", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
							ENDIF
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneTryDoor4, FALSE)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneTryDoor4, FALSE)
							
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.5)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor4)
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
//				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//					DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//				ENDIF
			ENDIF
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneTryDoor4)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneTryDoor4) >= 0.9)
			OR GET_GAME_TIMER() > iAutowalkTimeout
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				SET_LABEL_AS_TRIGGERED("ARM3_LOCK_4", TRUE)
			ENDIF
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Interrupt"))
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		
		//Stealth help text
		IF bPlayerHasEnteredMansion
			IF DOES_BLIP_EXIST(blipGardener)
				IF DOES_ENTITY_EXIST(pedGardener)
				AND NOT IS_PED_INJURED(pedGardener)
					IF IS_ENTITY_ON_SCREEN(pedGardener)
					AND NOT IS_ENTITY_OCCLUDED(pedGardener)
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("ARM3_FIND")
							PRINT_ADV("ARM3_GARDEN")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_SNEAK") //Sneaking help
				PRINT_HELP_ADV("ARM3HLP_SNEAK")
				g_iArmenian3HelpText = 1
				
				//SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), TRUE)
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_SNEAK2") //Player blip colouring help
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_ADV("ARM3HLP_SNEAK2")
					g_iArmenian3HelpText = 2
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("AH_H_TAKEDOWN") //Stealth takedown help
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedGardener, FALSE), <<4.0, 4.0, 2.0>>)
				AND IS_SCREEN_FADED_IN()
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-829.605652, 194.842453, 62.430786>>, <<-832.317444, 151.827423, 87.163620>>, 30.0)
						PRINT_HELP_FOREVER("AH_H_TAKEDOWN")
						
						iHelpTimer = GET_GAME_TIMER() + DEFAULT_HELP_TEXT_TIME
						
						SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_STEALTH_TAKEDOWN)
						//Trigger global to indicate the help has been displayed
						
						SET_LABEL_AS_TRIGGERED("AH_H_TAKEDOWN", TRUE)
					ENDIF
				ENDIF
//			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_STEALTH1") //Player blip colouring help
//				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//				OR WAS_PED_KILLED_BY_STEALTH(pedGardener)
//				OR WAS_PED_KILLED_BY_TAKEDOWN(pedGardener)
//				OR WAS_PED_KNOCKED_OUT(pedGardener)
//					PRINT_HELP_ADV("ARM3HLP_STEALTH1")
//					g_iArmenian3HelpText = 3
//				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_STAT") //Stealth stat help
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedGardener, FALSE), <<8.0, 8.0, 4.0>>)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AH_H_TAKEDOWN")
						IF GET_GAME_TIMER() > iHelpTimer
							SAFE_CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				OR WAS_PED_KILLED_BY_STEALTH(pedGardener)
				OR WAS_PED_KILLED_BY_TAKEDOWN(pedGardener)
				OR WAS_PED_KNOCKED_OUT(pedGardener)
				OR IS_PED_INJURED(pedGardener)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AH_H_TAKEDOWN")
						SAFE_CLEAR_HELP()
					ELSE
						PRINT_HELP_ADV("ARM3HLP_STAT")
						
						REPLAY_RECORD_BACK_FOR_TIME(2.5, 4.0, REPLAY_IMPORTANCE_HIGH)
						
						g_iArmenian3HelpText = 3
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARM3HLP_SNEAK")
			IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
				SAFE_CLEAR_HELP()
			ENDIF
		ENDIF
		
		bPlayerHasEnteredMansion = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-847.812927, 182.998001, 66.362663>>, <<-811.278198, 180.250854, 81.592949>>, 30.0)
								   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-843.973389, 159.883316, 65.309196>>, <<-809.686829, 159.163422, 76.786880>>, 22.0)
								   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.617554,161.953323,59.905128>>, <<-765.113220,168.867889,104.219299>>, 40.0)
								   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-772.186829,136.951172,65.974167>>, <<-772.276367,151.575790,101.474510>>, 20.5)
								   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-823.994324,190.366592,71.401894>>, <<-773.834778,189.717102,107.483505>>, 10.0)
								   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-776.787048,185.335403,71.335213>>, <<-770.144592,190.454834,107.858650>>, 10.0)
								   OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-850.643372,145.596069,58.643929>>, <<-783.199402,146.322983,97.667755>>, 10.0)
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("GardenerTakedown")
			IF bPlayerHasEnteredMansion
				//Audio Scene
				IF IS_AUDIO_SCENE_ACTIVE("ARM_3_CLIMB_INTO_GARDEN")
					STOP_AUDIO_SCENE("ARM_3_CLIMB_INTO_GARDEN")
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_TAKE_OUT_GARDENER")
					START_AUDIO_SCENE("ARM_3_TAKE_OUT_GARDENER")
				ENDIF
			ELSE
				//Audio Scene
				IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TAKE_OUT_GARDENER")
					STOP_AUDIO_SCENE("ARM_3_TAKE_OUT_GARDENER")
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_CLIMB_INTO_GARDEN")
					START_AUDIO_SCENE("ARM_3_CLIMB_INTO_GARDEN")
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedGardener)
			AND NOT IS_PED_INJURED(pedGardener)
			AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedGardener)
				//Audio Scene
				IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_TARGET_GARDENER")
					START_AUDIO_SCENE("ARM_3_TARGET_GARDENER")
				ENDIF
			ELSE
				//Audio Scene
				IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TARGET_GARDENER")
					STOP_AUDIO_SCENE("ARM_3_TARGET_GARDENER")
				ENDIF
			ENDIF
		ELSE
			//Audio Scene
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_CLIMB_INTO_GARDEN")
				STOP_AUDIO_SCENE("ARM_3_CLIMB_INTO_GARDEN")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TAKE_OUT_GARDENER")
				STOP_AUDIO_SCENE("ARM_3_TAKE_OUT_GARDENER")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TARGET_GARDENER")
				STOP_AUDIO_SCENE("ARM_3_TARGET_GARDENER")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_ENTER_HOUSE")
				START_AUDIO_SCENE("ARM_3_ENTER_HOUSE")
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.966431, 162.509369, 69.059227>>, <<-785.860901, 168.498703, 80.872543>>, 17.0)
			PRINT_HELP_ADV("ARM3HLP_CLIMB", TRUE, DEFAULT_HELP_TEXT_TIME + 2000)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.685486, 164.185196, 70.537521>>, <<-792.507080, 167.969635, 77.397369>>, 7.0)
		OR (IS_PED_ON_VEHICLE(PLAYER_PED_ID())
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehClimb, <<0.0, 2.15, -2.0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehClimb, <<0.0, -2.07, 5.0>>), 2.0))
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<-802.1, 167.1, 76.5>>, 8000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
			
			INCREASE_PLAYER_JUMP_SUPPRESSION_RANGE(PLAYER_ID())
		ENDIF
		
		IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_EAVDROP_1")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.534729, 160.426956, 73.342880>>, <<-804.091125, 172.133453, 79.740311>>, 10.0)
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, NULL, "TRACEY")
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, NULL, "JIMMY")
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_EAVDROP", "ARM3_EAVDROP_1")
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_EAVDROP_2")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.534729, 160.426956, 73.342880>>, <<-804.091125, 172.133453, 79.740311>>, 10.0)
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, NULL, "TRACEY")
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, NULL, "JIMMY")
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_EAVDROP", "ARM3_EAVDROP_2")
				ENDIF
			ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_EAVDROP_3")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.534729, 160.426956, 73.342880>>, <<-804.091125, 172.133453, 79.740311>>, 10.0)
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, NULL, "TRACEY")
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, NULL, "JIMMY")
					
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_EAVDROP", "ARM3_EAVDROP_3")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARM3HLP_CLIMB")
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-804.265625, 163.107071, 76.963020>>, <<5.0, 5.0, 2.0>>)
				SAFE_CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.263367, 164.741470, 74.864861>>, <<-802.518921, 168.107956, 79.740738>>, 4.0)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		ENDIF
		
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF HAS_LABEL_BEEN_TRIGGERED("ClimbInTrigger")
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = DORMANT_TASK
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SET_LABEL_AS_TRIGGERED("ClimbInTrigger", FALSE)
				ENDIF
			ENDIF
			
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.670105, 166.004623, 74.841263>>, <<-802.518921, 168.107956, 79.740738>>, 2.0)
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_CELLPHONE_DISABLED()
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()))
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.039734, 169.529953, 75.740639>>, <<-802.293152, 167.590179, 79.057426>>, 1.5)
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot)))
				
				SET_LABEL_AS_TRIGGERED("ClimbInTrigger", TRUE)
			ENDIF
		ELSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot), <<0.5, 0.5, 3.0>>)
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID()) AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID())
				ADVANCE_STAGE()
			ELIF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.670105, 166.004623, 74.841263>>, <<-802.518921, 168.107956, 79.740738>>, 2.0)
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
		
		SET_PLAYER_SNEAKING_NOISE_MULTIPLIER(PLAYER_ID(), 0.1)
		SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_CLIMB_INTO_GARDEN")
			STOP_AUDIO_SCENE("ARM_3_CLIMB_INTO_GARDEN")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TAKE_OUT_GARDENER")
			STOP_AUDIO_SCENE("ARM_3_TAKE_OUT_GARDENER")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_TARGET_GARDENER")
			STOP_AUDIO_SCENE("ARM_3_TARGET_GARDENER")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_ENTER_HOUSE")
			STOP_AUDIO_SCENE("ARM_3_ENTER_HOUSE")
		ENDIF
		
		//Particles
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeaf)
			STOP_PARTICLE_FX_LOOPED(ptfxLeaf)
		ENDIF
		
		//Sound - Leafblower
		IF sIDLeaf != -1
			STOP_SOUND(sIDLeaf)
			RELEASE_SOUND_ID(sIDLeaf)
			sIDLeaf = -1
		ENDIF
		
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(pedGardener)
			SAFE_DELETE_PED(pedGardener)
			SAFE_DELETE_OBJECT(objLeafblower)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GARDENER_01)
			REMOVE_ANIM_DICT(sAnimDictGarden)
			IF HAS_CLIP_SET_LOADED(sAnimDictGardenerMoveClipSet)
				REMOVE_CLIP_SET(sAnimDictGardenerMoveClipSet)
			ENDIF
		ENDIF
		
		DISABLE_CELLPHONE(FALSE)
		
		//Audio
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("ARM_3_03")
		
		SAFE_REMOVE_BLIP(blipDestination)
		
//		CLEAR_PRINTS()
		SAFE_CLEAR_HELP()		
//		KILL_FACE_TO_FACE_CONVERSATION()
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC cutsceneArgue()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayMansion), "stageSneakThrough")
		
		IF DOES_ENTITY_EXIST(vehDriveTo)
		AND NOT IS_ENTITY_DEAD(vehDriveTo)
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehDriveTo)
		ENDIF
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
		
		//Radar
		bRadar = TRUE
		
		//Son
		WHILE NOT CREATE_NPC_PED_ON_FOOT(pedSon, CHAR_JIMMY, vSonStart, fSonStart)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		SET_ENTITY_HEALTH(pedSon, 101)
		SET_PED_COMPONENT_VARIATION(pedSon, PED_COMP_TORSO, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedSon, PED_COMP_DECL, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedSon, PED_COMP_FEET, 3, 0)
		SET_PED_CONFIG_FLAG(pedSon, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedSon, PCF_DisableExplosionReactions, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSon, TRUE)
		TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_base", <<-804.750, 175.550, 75.750>>, <<0.0, 0.0, 111.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION | AF_OVERRIDE_PHYSICS)
		FREEZE_ENTITY_POSITION(pedSon, TRUE)
		SET_PED_POSITION(pedSon, vSonStart, fSonStart)
		
		//Cleanup Son
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_JIMMY))
		
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		
		//Daughter
		WHILE NOT CREATE_NPC_PED_ON_FOOT(pedDaughter, CHAR_TRACEY, vDaughterStart, fDaughterStart)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		SET_ENTITY_HEALTH(pedDaughter, 101)
		SET_PED_COMPONENT_VARIATION(pedDaughter, PED_COMP_TORSO, 3, 0)
		SET_PED_COMPONENT_VARIATION(pedDaughter, PED_COMP_LEG, 3, 0)
		SET_PED_COMPONENT_VARIATION(pedDaughter, PED_COMP_FEET, 2, 0)
		SET_PED_CONFIG_FLAG(pedDaughter, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedDaughter, PCF_DisableExplosionReactions, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDaughter, TRUE)
		
		//Cleanup Daughter
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_TRACEY))
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio
		PLAY_AUDIO(ARM3_WINDOW)
		
		//Son
		SAFE_SET_ENTITY_VISIBLE(pedSon, FALSE)
		
		//Daughter
		SAFE_SET_ENTITY_VISIBLE(pedDaughter, FALSE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			SET_PED_POSITION(PLAYER_PED_ID(), <<-802.1424, 166.1851, 75.4634>>, 19.1130)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot)))
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Audio
			PLAY_AUDIO(ARM3_RESTART_2)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_EAVDROP_3")
			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedDaughter, "TRACEY")
				ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedSon, "JIMMY")
				
				PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_EAVDROP", "ARM3_EAVDROP_3")
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
//				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//				
//				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//				
//				CLEAR_PED_TASKS(PLAYER_PED_ID())
//				
//				//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY | ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot)))
//				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, GET_HEADING_FROM_VECTOR(GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot)))
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 1
				IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDictArm3Climb, "climb_in_window_v2", sceneClimbInPos, sceneClimbInRot), <<0.2, 0.2, 3.0>>)
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDaughter))
				OR TIMERA() > 5000
					sceneClimbIn = CREATE_SYNCHRONIZED_SCENE(sceneClimbInPos, sceneClimbInRot)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneClimbIn, sAnimDictArm3Climb, "climb_in_window_v2", WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, WALK_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneClimbIn, FALSE)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					REPLAY_RECORD_BACK_FOR_TIME(8.0, 10.0)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneClimbIn)	//IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDictArm3Climb, "climb_in_window_v2")
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneClimbIn)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneClimbIn) >= 0.95)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
//		REMOVE_CUTSCENE()
//		
//		WHILE HAS_CUTSCENE_LOADED()
//			WAIT_WITH_DEATH_CHECKS(0)
//		ENDWHILE
		
		VEHICLE_INDEX vehLast = GET_PLAYERS_LAST_VEHICLE()
		
		IF vehLast != vehDriveTo
			IF DOES_ENTITY_EXIST(vehLast)
			AND NOT IS_ENTITY_DEAD(vehLast)
				vehDriveTo = vehLast
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehDriveTo)
		AND NOT IS_ENTITY_DEAD(vehDriveTo)
			SET_VEHICLE_POSITION(vehDriveTo, <<-855.5760, 172.7193, 67.1646>>, 352.4847)
			SET_VEHICLE_FIXED(vehDriveTo)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehDriveTo)
			
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-855.5760, 172.7193, 67.1646>>, 352.4847, FALSE, CHAR_FRANKLIN)
		ENDIF
		
//		KILL_ANY_CONVERSATION()
		
		//Son
		SAFE_SET_ENTITY_VISIBLE(pedSon, TRUE)
		
		TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_loop_d", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE | AF_LOOPING)
		
		//Daughter
		SAFE_SET_ENTITY_VISIBLE(pedDaughter, TRUE)
		
		SET_PED_POSITION(pedDaughter, <<-800.5298, 170.2849, 75.7406>>, 180.0982)
		
		OPEN_SEQUENCE_TASK(seqMain)
			TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3Argue, "tracey_argument", <<-806.166, 170.525, 76.460>>, <<0.0, 0.0, 110.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED, 0.900)	//TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_enter", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED, 0.05)
			TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_a", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
			TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_b", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
			TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_c", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
			TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_d", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE | AF_LOOPING, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
			//TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_e", <<-801.470, 171.426, 76.520>>, <<-0.063, 0.027, -67.146>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_LOOPING)
		CLOSE_SEQUENCE_TASK(seqMain)
		TASK_PERFORM_SEQUENCE(pedDaughter, seqMain)
		CLEAR_SEQUENCE_TASK(seqMain)
		
		IF NOT DOES_ENTITY_EXIST(objPhone)
			objPhone = CREATE_OBJECT(prop_phone_ing_02, GET_ENTITY_COORDS(pedDaughter))
			ATTACH_ENTITY_TO_ENTITY(objPhone, pedDaughter, GET_PED_BONE_INDEX(pedDaughter, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO, TRUE, TRUE)
		ENDIF
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDaughter)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles Jimmy's TV playing RIGHTEOUS SLAUGHTER(tm)
PROC HANDLE_JIMMY_TV()
	IF bJimmyTVPLayingGame = FALSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.094299, 167.774826, 75.240730>>, <<-808.853943, 184.658707, 79.762192>>, 11.75)
			IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)
				START_AMBIENT_TV_PLAYBACK(TV_LOC_JIMMY_BEDROOM, TVCHANNELTYPE_CHANNEL_1, TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER_CUTSCENE)
				SET_TV_VOLUME(0.0)
				bJimmyTVPLayingGame = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles disabling the projector TV
PROC HANDLE_PROJECTOR_TV()
	IF bProjectorTVDisabled = FALSE
		IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)
			DISABLE_TV_CONTROLS(TV_LOC_MICHAEL_PROJECTOR, TRUE)
			bProjectorTVDisabled = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC SneakThrough()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayMansion), "stageSneakThrough")
		
		IF DOES_ENTITY_EXIST(vehDriveTo)
		AND NOT IS_ENTITY_DEAD(vehDriveTo)
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehDriveTo)
		ENDIF
		
		//Set player
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Radar
		bRadar = TRUE
		
		//Cutscenes
		ePrestreamCutscene = roomNone
		
		bPrestreamCutsceneLockedIn[roomSon] = FALSE
		bPrestreamCutsceneLockedIn[roomDaughter] = FALSE
		bPrestreamCutsceneLockedIn[roomWifeCoach] = FALSE
		
		//Blanket in car
		SET_VEHICLE_EXTRA(vehCar, 5, FALSE)
		
		//Son
		IF NOT DOES_ENTITY_EXIST(pedSon)
			WHILE NOT CREATE_NPC_PED_ON_FOOT(pedSon, CHAR_JIMMY, vSonStart, fSonStart)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_ENTITY_HEALTH(pedSon, 101)
			SET_PED_COMPONENT_VARIATION(pedSon, PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedSon, PED_COMP_DECL, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedSon, PED_COMP_FEET, 3, 0)
			SET_PED_CONFIG_FLAG(pedSon, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(pedSon, PCF_DisableExplosionReactions, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSon, TRUE)
			TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_base", <<-806.657, 170.139, 76.470>>, <<0.0, 0.0, 111.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION | AF_OVERRIDE_PHYSICS)
			FREEZE_ENTITY_POSITION(pedSon, TRUE)
			SET_PED_POSITION(pedSon, vSonStart, fSonStart)
			
			//Cleanup Son
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_JIMMY))
			
			TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_loop_d", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE | AF_LOOPING)
			
			WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		ENDIF
		
		//Daughter
		IF NOT DOES_ENTITY_EXIST(pedDaughter)
			WHILE NOT CREATE_NPC_PED_ON_FOOT(pedDaughter, CHAR_TRACEY, vDaughterStart, fDaughterStart)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			SET_ENTITY_HEALTH(pedDaughter, 101)
			SET_PED_COMPONENT_VARIATION(pedDaughter, PED_COMP_TORSO, 3, 0)
			SET_PED_COMPONENT_VARIATION(pedDaughter, PED_COMP_LEG, 3, 0)
			SET_PED_COMPONENT_VARIATION(pedDaughter, PED_COMP_FEET, 2, 0)
			SET_PED_CONFIG_FLAG(pedDaughter, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(pedDaughter, PCF_DisableExplosionReactions, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDaughter, TRUE)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDaughter)
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			IF intMansion = NULL
				intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
			ENDIF
			
			RETAIN_ENTITY_IN_INTERIOR(pedDaughter, intMansion)
			
			//Cleanup Daughter
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_TRACEY))
			
			SET_PED_POSITION(pedDaughter, <<-800.5298, 170.2849, 75.7406>>, 180.0982)
			
			OPEN_SEQUENCE_TASK(seqMain)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3Argue, "tracey_argument", <<-806.166, 170.525, 76.460>>, <<0.0, 0.0, 110.0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED, 0.900)	//TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_enter", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_a", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_b", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_c", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
				TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_d", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_NOT_INTERRUPTABLE | AF_LOOPING, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
				//TASK_PLAY_ANIM_ADVANCED(NULL, sAnimDictArm3, "tracey_idle_e", <<-801.470, 171.426, 76.520>>, <<-0.063, 0.027, -67.146>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_LOOPING)
			CLOSE_SEQUENCE_TASK(seqMain)
			TASK_PERFORM_SEQUENCE(pedDaughter, seqMain)
			CLEAR_SEQUENCE_TASK(seqMain)
			
			IF NOT DOES_ENTITY_EXIST(objPhone)
				objPhone = CREATE_OBJECT(prop_phone_ing_02, GET_ENTITY_COORDS(pedDaughter))
				ATTACH_ENTITY_TO_ENTITY(objPhone, pedDaughter, GET_PED_BONE_INDEX(pedDaughter, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO, TRUE, TRUE)
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		ENDIF
		
		//Wife
		WHILE NOT CREATE_NPC_PED_ON_FOOT(pedWife, CHAR_AMANDA, vWifeStart, fWifeStart)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		SET_PED_COMPONENT_VARIATION(pedWife, PED_COMP_HAIR, 0, 1)
		SET_PED_COMPONENT_VARIATION(pedWife, PED_COMP_TORSO, 0, 1)
		SET_PED_COMPONENT_VARIATION(pedWife, PED_COMP_LEG, 0, 1)
		SET_ENTITY_HEALTH(pedWife, 101)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWife, TRUE)
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 4, pedWife, "AMANDA")
		
		IF intMansion = NULL
			intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
		ENDIF
		
		RETAIN_ENTITY_IN_INTERIOR(pedWife, intMansion)
		
		//Cleanup Wife
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_AMANDA))
		
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		
		//Coach
		WHILE NOT CREATE_NPC_PED_ON_FOOT(pedCoach, CHAR_TENNIS_COACH, vCoachStart, fCoachStart)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		SET_ENTITY_HEALTH(pedCoach, 101)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCoach, TRUE)
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 7, pedCoach, "TENNISCOACH")
		
		IF intMansion = NULL
			intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
		ENDIF
		
		RETAIN_ENTITY_IN_INTERIOR(pedCoach, intMansion)
		
		//Cleanup Coach
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_TENNIS_COACH))
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-805.8604, 172.6347, 75.7407>>)
			covPoint[0] = ADD_COVER_POINT(<<-808.0162, 178.5150, 71.1531>>, -70.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180, TRUE)
		ENDIF
		
		IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-805.7504, 172.4508, 75.7407>>)
			covPoint[1] = ADD_COVER_POINT(<<-805.7504, 172.4508, 75.7407>>, 111.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180, TRUE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_INSIDE_HOUSE")
			START_AUDIO_SCENE("ARM_3_INSIDE_HOUSE")
		ENDIF
		
		//Audio
		PLAY_AUDIO(ARM3_WINDOW)
		
		#IF IS_DEBUG_BUILD
		IF bSkipped = FALSE
		#ENDIF
		
		SET_PORTAL_SETTINGS_OVERRIDE("V_MICHAEL_PS_BATHROOM_WITH_WINDOW", "V_MICHAEL_PS_BATHROOM_WITHOUT_WINDOW")
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Blip
		SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)	//SAFE_ADD_BLIP_LOCATION(blipDestination, vCarStart)
		
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		
		//Object
		objGamepad = CREATE_OBJECT(modGamepad, <<-801.49, 179.15, 72.32>>)
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		objHeadset = CREATE_OBJECT(modHeadset, <<-801.49, 179.15, 72.32>>)
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		objTennisA = CREATE_OBJECT(modTennis, <<-801.49, 179.35, 72.32>>)
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		objTennisB = CREATE_OBJECT(modTennis, <<-801.49, 179.85, 72.32>>)
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		objBag = CREATE_OBJECT(modBag, <<-801.49, 179.55, 72.32>>)
		WAIT_WITH_DEATH_CHECKS(0)	//Delay entity creation
		
		ATTACH_ENTITY_TO_ENTITY(objGamepad, pedSon, GET_PED_BONE_INDEX(pedSon, BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
		ATTACH_ENTITY_TO_ENTITY(objHeadset, pedSon, GET_PED_BONE_INDEX(pedSon, BONETAG_HEAD), <<0.01, 0.0, 0.0>>, <<-180.0, 90.0, 0.0>>)
		ATTACH_ENTITY_TO_ENTITY(objTennisA, pedWife, GET_PED_BONE_INDEX(pedWife, BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
		ATTACH_ENTITY_TO_ENTITY(objTennisB, pedCoach, GET_PED_BONE_INDEX(pedCoach, BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
		ATTACH_ENTITY_TO_ENTITY(objBag, pedCoach, GET_PED_BONE_INDEX(pedCoach, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Freeze
			SET_PED_POSITION(PLAYER_PED_ID(), <<-802.6609, 168.6319, 75.7407>>, 31.0258)
			
			SET_VEHICLE_POSITION(vehCar, vCarStart, fCarStart)
			
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			FREEZE_ENTITY_POSITION(pedDaughter, TRUE)
			FREEZE_ENTITY_POSITION(pedSon, TRUE)
			FREEZE_ENTITY_POSITION(pedWife, TRUE)
			FREEZE_ENTITY_POSITION(pedCoach, TRUE)
			FREEZE_ENTITY_POSITION(vehCar, TRUE)
			
			//Pin Interior
			intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
			
			PIN_INTERIOR_IN_MEMORY(intMansion)
			
			WHILE NOT IS_INTERIOR_READY(intMansion)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedMansion = TRUE
			
			//Unfreeze
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(pedDaughter, FALSE)
			FREEZE_ENTITY_POSITION(pedSon, FALSE)
			FREEZE_ENTITY_POSITION(pedWife, FALSE)
			FREEZE_ENTITY_POSITION(pedCoach, FALSE)
			FREEZE_ENTITY_POSITION(vehCar, FALSE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<-802.1, 167.9, 77.7>>, 20.0)
			ENDIF
			
			//Franklin
			SET_PED_POSITION(PLAYER_PED_ID(), <<-802.6609, 168.6319, 75.7407>>, 31.0258)
			
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
			
			SET_VEHICLE_POSITION(vehCar, vCarStart, fCarStart)
			ACTIVATE_PHYSICS(vehCar)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
						
			//Audio
			PLAY_AUDIO(ARM3_RESTART_3)
			
			//Cam
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			ENDIF
			
			SET_CAM_PARAMS(camMain, <<-802.733154, 167.991272, 77.422760>>, <<-11.807504, 0.0, 17.053619>>, 50.0)
			
			SET_CAM_ACTIVE(camMain, TRUE)
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(FALSE, 0.0, CAM_SPLINE_SLOW_IN_SMOOTH)
			
			WAIT_WITH_DEATH_CHECKS(10)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_RUN)
		
		IF DOES_ENTITY_EXIST(objPhone)
			IF DOES_ENTITY_EXIST(pedDaughter) AND NOT IS_PED_INJURED(pedDaughter)
				IF IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
					IF IS_ENTITY_VISIBLE(objPhone)
						SET_ENTITY_VISIBLE(objPhone, FALSE)
					ENDIF
				ELSE
					IF NOT IS_ENTITY_VISIBLE(objPhone)
						SET_ENTITY_VISIBLE(objPhone, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				PRINT_HELP_ADV("ARM3HLP_AVOID")
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					UNPIN_INTERIOR(intMansion)
					
					//Dialogue
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedSon, "JIMMY")
					
					CREATE_CONVERSATION_ADV("ARM3_JIM1.5")
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_ADV("ARM3_GETCAR")
				ENDIF
				
//				IF HAS_LABEL_BEEN_TRIGGERED("ARM3_WIFEA")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
						IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneWifeCoachFlirt)
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWifeCoachFlirt) = 1.0
							AND DOES_ENTITY_HAVE_DRAWABLE(objTennisA)
								sceneWifeCoachFlirt = CREATE_SYNCHRONIZED_SCENE(<<-796.971, 185.839, 72.717>>, <<0.0, 0.0, 51.0>>)
								
								TASK_SYNCHRONIZED_SCENE(pedWife, sceneWifeCoachFlirt, sAnimDictArm3, "tennis_coach_loop_wife", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
								TASK_SYNCHRONIZED_SCENE(pedCoach, sceneWifeCoachFlirt, sAnimDictArm3, "tennis_coach_loop_coach", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneWifeCoachFlirt, TRUE)
								
								SET_SYNCHRONIZED_SCENE_PHASE(sceneWifeCoachFlirt, 0.0)
								
								DETACH_ENTITY(objTennisA, FALSE)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(objTennisA, sceneWifeCoachFlirt, "tennis_coach_loop_racketa", sAnimDictArm3, INSTANT_BLEND_IN)
								
//								DETACH_ENTITY(objTennisA, FALSE)
//								FREEZE_ENTITY_POSITION(objTennisA, TRUE)
								
								DETACH_ENTITY(objTennisB, FALSE)
								FREEZE_ENTITY_POSITION(objTennisB, TRUE)
								
								DETACH_ENTITY(objBag, FALSE)
								FREEZE_ENTITY_POSITION(objBag, TRUE)
								
								SETTIMERA(0)
							ENDIF
//						ELSE
//							IF TIMERA() > 15000
//								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-810.335632, 179.167435, 71.153091>>, <<-796.086731, 184.913010, 74.355469>>, 2.0)
//									IF NOT HAS_LABEL_BEEN_TRIGGERED("CaughtWifeCoach")
//										SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//										
//										SET_LABEL_AS_TRIGGERED("CaughtWifeCoach", TRUE)
//									ENDIF
//								ENDIF
//							ENDIF
						ENDIF
					ENDIF
//				ENDIF
				
				enumSubtitlesState eDisplaySubtitles
				eDisplaySubtitles = DISPLAY_SUBTITLES
				
				IF IS_MESSAGE_BEING_DISPLAYED()
					eDisplaySubtitles = DO_NOT_DISPLAY_SUBTITLES
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF iDialogueLineCount[iDialogueStage] = -1
						 iDialogueLineCount[iDialogueStage] = 9
					ELIF iDialogueLineCount[iDialogueStage] > 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.127502, 167.657471, 75.740723>>, <<-807.444336, 178.842728, 78.990883>>, 3.25)
							IF TIMERB() > iTimer
								IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_JIM")
									CREATE_CONVERSATION_ADV("ARM3_JIM", CONV_PRIORITY_HIGH, FALSE, eDisplaySubtitles)
									
									iTimer = GET_RANDOM_INT_IN_RANGE(5500, 10000)
									SETTIMERB(0)
									
									iDialogueLineCount[iDialogueStage]--
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.127502, 167.657471, 75.740723>>, <<-807.444336, 178.842728, 78.990883>>, 3.25)
							IF TIMERB() > iTimer
								PLAY_PED_AMBIENT_SPEECH(pedSon, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE)
								
								iTimer = GET_RANDOM_INT_IN_RANGE(5500, 10000)
								SETTIMERB(0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.1136, 176.5611, 73.9906>>, <<-804.4435, 182.1805, 79.0034>>, 5.0)
						IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_TRCYA")
						OR NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_TRCYB")
						OR NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_TRCYC")
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedDaughter, "TRACEY")
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_TRCYA")
								CREATE_CONVERSATION_ADV("ARM3_TRCYA", CONV_PRIORITY_HIGH)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.0, REPLAY_IMPORTANCE_HIGH)
								
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_TRCYB")
								CREATE_CONVERSATION_ADV("ARM3_TRCYB", CONV_PRIORITY_HIGH)
							ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_TRCYC")
								CREATE_CONVERSATION_ADV("ARM3_TRCYC", CONV_PRIORITY_HIGH)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("ARM3_TRCYA")
				AND HAS_LABEL_BEEN_TRIGGERED("ARM3_TRCYB")
				AND HAS_LABEL_BEEN_TRIGGERED("ARM3_TRCYC")
				AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_TRCYA")
				AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_TRCYB")
				AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_TRCYC")
					IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3, "tracey_idle_a")
						TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_idle_a", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_LOOPING)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-794.927551, 184.106979, 70.834709>>, <<-815.375916, 176.086212, 74.153091>>, 25.0)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
					AND DOES_ENTITY_HAVE_DRAWABLE(objTennisA)
					AND DOES_ENTITY_HAVE_DRAWABLE(objTennisB)
					AND DOES_ENTITY_HAVE_DRAWABLE(objBag)
						sceneWifeCoachFlirt = CREATE_SYNCHRONIZED_SCENE(<<-796.971, 185.839, 72.717>>, <<0.0, 0.0, 51.0>>)
						
						TASK_SYNCHRONIZED_SCENE(pedWife, sceneWifeCoachFlirt, sAnimDictArm3, "tennis_coach_intro_wife", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
						TASK_SYNCHRONIZED_SCENE(pedCoach, sceneWifeCoachFlirt, sAnimDictArm3, "tennis_coach_intro_coach", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_HEAD_IK)
						
						DETACH_ENTITY(objTennisA, FALSE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objTennisA, sceneWifeCoachFlirt, "tennis_coach_intro_racketa", sAnimDictArm3, INSTANT_BLEND_IN)
						
						DETACH_ENTITY(objTennisB, FALSE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objTennisB, sceneWifeCoachFlirt, "tennis_coach_intro_racketb", sAnimDictArm3, INSTANT_BLEND_IN)
						
						DETACH_ENTITY(objBag, FALSE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objBag, sceneWifeCoachFlirt, "tennis_coach_intro_bag", sAnimDictArm3, INSTANT_BLEND_IN)
					ENDIF
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWifeCoachFlirt)
						IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_WIFE_1")
								IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneWifeCoachFlirt)
								AND GET_SYNCHRONIZED_SCENE_PHASE(sceneWifeCoachFlirt) >= 0.125
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_WIFE", "ARM3_WIFE_1")
									
									REPLAY_RECORD_BACK_FOR_TIME(1.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
									
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_WIFE_2")
								IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneWifeCoachFlirt)
								AND GET_SYNCHRONIZED_SCENE_PHASE(sceneWifeCoachFlirt) >= 0.335
									PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_WIFE", "ARM3_WIFE_2")
								ENDIF
							ENDIF
							
							IF IS_SYNCHRONIZED_SCENE_LOOPED(sceneWifeCoachFlirt)
							OR GET_SYNCHRONIZED_SCENE_PHASE(sceneWifeCoachFlirt) >= 0.39
								IF NOT HAS_LABEL_BEEN_TRIGGERED("GarageDoorSmoothClosed")
									IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_WIFEA")
									OR NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_WIFEB")
									OR NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_WIFEC")
										IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_WIFEA")
											CREATE_CONVERSATION_ADV("ARM3_WIFEA", CONV_PRIORITY_HIGH)
											
											REPLAY_RECORD_BACK_FOR_TIME(4.5, 6.0, REPLAY_IMPORTANCE_HIGH)
											
										ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_WIFEB")
											CREATE_CONVERSATION_ADV("ARM3_WIFEB", CONV_PRIORITY_HIGH)
										ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_WIFEC")
											CREATE_CONVERSATION_ADV("ARM3_WIFEC", CONV_PRIORITY_HIGH)
//										ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//											IF GET_GAME_TIMER() > iDialogueTimer
//												CREATE_CONVERSATION_ADV("ARM3_WIFER", CONV_PRIORITY_HIGH, FALSE)
//												
//												iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5500, 10000)
//											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.127502, 167.657471, 75.740723>>, <<-807.444336, 178.842728, 78.990883>>, 3.25)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.1136, 176.5611, 73.9906>>, <<-805.7435, 185.8805, 79.0034>>, 5.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-794.927551, 184.106979, 70.834709>>, <<-815.375916, 176.086212, 74.153091>>, 25.0)
				OR HAS_LABEL_BEEN_TRIGGERED("GarageDoorSmoothClosed")
					IF HAS_LABEL_BEEN_TRIGGERED("ARM3_JIM")
						SET_LABEL_AS_TRIGGERED("ARM3_JIM", FALSE)
					ENDIF
					
					IF NOT IS_PHONE_ONSCREEN()
					AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_ANY_CONVERSATION()
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SneakReminder")
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-809.956543, 175.541382, 75.740738>>, <<-801.040466, 178.926025, 79.240738>>, 3.75)
							OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-805.785767,179.748779,75.740738>>, <<-801.420898, 168.294800, 79.240738>>, 11.0)
								SET_LABEL_AS_TRIGGERED("ARM3HLP_SNEAK", FALSE)				
								PRINT_HELP_ADV("ARM3HLP_SNEAK")
								
								SET_LABEL_AS_TRIGGERED("SneakReminder", TRUE)
							ENDIF
						ELSE
							SET_LABEL_AS_TRIGGERED("ARM3HLP_SNEAK", TRUE)				
							SET_LABEL_AS_TRIGGERED("SneakReminder", TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARM3HLP_SNEAK")
					IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
						SAFE_CLEAR_HELP()
					ENDIF
				ENDIF
				
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_SNEAK") //Sneaking help
						IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_SNEAK2") //Stealth mode info
							IF g_iArmenian3HelpText < 2
								PRINT_HELP_ADV("ARM3HLP_SNEAK2")
								g_iArmenian3HelpText = 2
							ELSE
								SET_LABEL_AS_TRIGGERED("ARM3HLP_SNEAK2", TRUE)
							ENDIF
//						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_STEALTH1") //Player blip colouring help
//							IF g_iArmenian3HelpText < 3
//								PRINT_HELP_ADV("ARM3HLP_STEALTH1")
//								g_iArmenian3HelpText = 3
//							ELSE
//								SET_LABEL_AS_TRIGGERED("ARM3HLP_STEALTH1", TRUE)
//							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_STAT") //Stealth stat help
							IF g_iArmenian3HelpText < 3
								PRINT_HELP_ADV("ARM3HLP_STAT")
								g_iArmenian3HelpText = 3
							ELSE
								SET_LABEL_AS_TRIGGERED("ARM3HLP_STAT", TRUE)
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_SOUNDBLIPS") //Sound Blips
						AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-805.785767,179.748779,75.740738>>, <<-801.420898, 168.294800, 79.240738>>, 11.0)
							PRINT_HELP_ADV("ARM3HLP_SOUNDBLIPS")
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			IF GET_ENTITY_ANIM_CURRENT_TIME(pedDaughter, sAnimDictArm3Argue, "tracey_argument") >= 0.950
			AND GET_ENTITY_ANIM_CURRENT_TIME(pedDaughter, sAnimDictArm3Argue, "tracey_argument") < 1.0
				SET_ENTITY_ANIM_CURRENT_TIME(pedDaughter, sAnimDictArm3Argue, "tracey_argument", 1.0)
			ENDIF
		ENDIF
		
//		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.361633, 188.988678, 71.474586>>, <<-815.295715, 185.975479, 74.975128>>, 6.0, FALSE, TRUE)
//			//Blips
//			SAFE_REMOVE_BLIP(blipDestination)
//			
//			SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
//			
//			//Text
//			PRINT_ADV("ARM3_GO")	//PRINT_ADV("ARM3_CAR1")
//		ENDIF
		
		HANDLE_JIMMY_TV()
		HANDLE_PROJECTOR_TV()
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.402283, 189.004593, 71.478920>>, <<-815.288208, 185.978165, 74.954453>>, 6.5)
			//Audio Scene
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_INSIDE_HOUSE")
				STOP_AUDIO_SCENE("ARM_3_INSIDE_HOUSE")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_ENTER_GARAGE")
				START_AUDIO_SCENE("ARM_3_ENTER_GARAGE")
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("EnterGarageAudio")
				PLAY_AUDIO(ARM3_GARAGE_STOP)
				
				SET_LABEL_AS_TRIGGERED("EnterGarageAudio", TRUE)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
			ADVANCE_STAGE()
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-805.785767,179.748779,75.740738>>, <<-801.420898, 168.294800, 79.240738>>, 11.0)
			IF NOT DOES_BLIP_EXIST(blipSon)
			AND DOES_ENTITY_EXIST(pedSon)
				SAFE_ADD_BLIP_PED(blipSon, pedSon)
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipDaughter)
			AND DOES_ENTITY_EXIST(pedDaughter)
				blipDaughter = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(pedDaughter))
				SET_BLIP_COLOUR(blipDaughter, BLIP_COLOUR_RED)
				SET_BLIP_SCALE(blipDaughter, 0.75)
				SET_BLIP_PRIORITY(blipDaughter, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipDaughter, "ARM3_BLIPPED")
				
				vBlipDaughterRoute = GET_ENTITY_COORDS(pedDaughter)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipDaughter)
				IF IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
					vBlipDaughterRoute = GET_ENTITY_COORDS(pedDaughter)
				ELSE
					VECTOR vPedDaughterRoute = GET_ENTITY_COORDS(pedDaughter)
					
					vBlipDaughterRoute.X = vBlipDaughterRoute.X +@ ((vPedDaughterRoute.X - vBlipDaughterRoute.X) / 1.0) * CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()), 1.0, 2.0)
					vBlipDaughterRoute.Y = vBlipDaughterRoute.Y +@ ((vPedDaughterRoute.Y - vBlipDaughterRoute.Y) / 1.0) * CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()), 1.0, 2.0)
					vBlipDaughterRoute.Z = vBlipDaughterRoute.Z +@ ((vPedDaughterRoute.Z - vBlipDaughterRoute.Z) / 1.0) * CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()), 1.0, 2.0)
				ENDIF
				
				VECTOR vPedDaughter
				VECTOR vBlipDaughter
				
				vPedDaughter = vBlipDaughterRoute
				vBlipDaughter = GET_BLIP_COORDS(blipDaughter)
				
				vBlipDaughter.X = vBlipDaughter.X +@ ((vPedDaughter.X - vBlipDaughter.X) / 1.0) * CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()), 1.0, 2.0)
				vBlipDaughter.Y = vBlipDaughter.Y +@ ((vPedDaughter.Y - vBlipDaughter.Y) / 1.0) * CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()), 1.0, 2.0)
				vBlipDaughter.Z = vBlipDaughter.Z +@ ((vPedDaughter.Z - vBlipDaughter.Z) / 1.0) * CLAMP(GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()), 1.0, 2.0)
				
				SET_BLIP_COORDS(blipDaughter, vBlipDaughter)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipCoach)
				SAFE_REMOVE_BLIP(blipCoach)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipWife)
				SAFE_REMOVE_BLIP(blipWife)
			ENDIF
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("GarageDoorSmoothClosed")
				IF NOT DOES_BLIP_EXIST(blipCoach)
				AND DOES_ENTITY_EXIST(pedCoach)
					SAFE_ADD_BLIP_PED(blipCoach, pedCoach)
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(blipWife)
				AND DOES_ENTITY_EXIST(pedWife)
					SAFE_ADD_BLIP_PED(blipWife, pedWife)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipCoach)
					SAFE_REMOVE_BLIP(blipCoach)
				ENDIF
				
				IF DOES_BLIP_EXIST(blipWife)
					SAFE_REMOVE_BLIP(blipWife)
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipSon)
				SAFE_REMOVE_BLIP(blipSon)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipDaughter)
				SAFE_REMOVE_BLIP(blipDaughter)
			ENDIF
		ENDIF
		
		//Flashlight Fails
		IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
		OR (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSon)
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedDaughter)
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedWife)
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedCoach))
			IF IS_FLASH_LIGHT_ON(PLAYER_PED_ID())
				IF GET_ENTITY_HEADING(PLAYER_PED_ID()) > 32.1361 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 179.8933
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.474304, 169.566299, 75.740326>>, <<-806.659363, 177.858795, 78.240738>>, 2.5)
					CLEANUP_GAME_PROPS()
					
					IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
						//TASK_PLAY_ANIM_ADVANCED(pedSon, sAnimDictArm3, "jimmy_playingvideogame_fail", <<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.250)
						INT sceneJimmyFail = CREATE_SYNCHRONIZED_SCENE(<<-806.520, 169.751, 75.693>>, <<0.0, 0.0, -50.0>>)
						TASK_SYNCHRONIZED_SCENE(pedSon, sceneJimmyFail, sAnimDictArm3, "jimmy_playingvideogame_fail", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objGamepad, sceneJimmyFail, "jimmy_playingvideogame_fail_controller", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objHeadset, sceneJimmyFail, "jimmy_playingvideogame_fail_headset", sAnimDictArm3, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_PHASE(sceneJimmyFail, 0.250)
					ENDIF
					
					SET_PED_KEEP_TASK(pedSon, TRUE)
					
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 6, pedSon, "JIMMY")
					
					CREATE_CONVERSATION_ADV("ARM3_JIMF")
					
					WAIT_WITH_DEATH_CHECKS(2000)
					
					eMissionFail = failNoise
					
					missionFailed()
				ENDIF
				
				IF GET_ENTITY_HEADING(PLAYER_PED_ID()) > 162.1006 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 240.0467
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.476868,175.870285,73.990738>>, <<-807.128113,185.345871,78.254265>>, 3.25)
					IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
						IF NOT IS_ENTITY_PLAYING_ANIM(pedDaughter, sAnimDictArm3Argue, "tracey_argument")
							TASK_PLAY_ANIM_ADVANCED(pedDaughter, sAnimDictArm3, "tracey_fail", <<-800.835, 170.158, 75.79>>, <<0.0, 0.0, -64.0>>, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_EXIT_AFTER_INTERRUPTED | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK)
						ELSE
							TASK_REACT_AND_FLEE_PED(pedDaughter, PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					SET_PED_KEEP_TASK(pedDaughter, TRUE)
					
					ADD_PED_FOR_DIALOGUE(sPedsForConversation, 5, pedDaughter, "TRACEY")
					
					CREATE_CONVERSATION_ADV("ARM3_TRCF")
					
					WAIT_WITH_DEATH_CHECKS(2000)
					
					eMissionFail = failNoise
					
					missionFailed()
				ENDIF
				
				IF (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 262.5618 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 315.2036
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-815.792542, 177.144836, 69.403091>>, <<-796.055664, 184.881302, 73.855469>>, 3.25))
				OR (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 291.0086 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 330.7509
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.407410, 186.814575, 71.605469>>, <<-797.243835, 177.114365, 74.834709>>, 8.0))
				OR (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 313.1061 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 337.2681
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-796.149414, 185.406311, 71.105469>>, <<-806.553406, 170.221832, 74.834709>>, 6.0))
					CLEAR_PED_TASKS(pedWife)
					TASK_REACT_AND_FLEE_PED(pedWife, PLAYER_PED_ID())
					SET_PED_KEEP_TASK(pedWife, TRUE)
					
					CLEAR_PED_TASKS(pedCoach)
					TASK_REACT_AND_FLEE_PED(pedCoach, PLAYER_PED_ID())
					SET_PED_KEEP_TASK(pedCoach, TRUE)
					
					CLEANUP_TENNIS_PROPS()
					
					CREATE_CONVERSATION_ADV("ARM3_WIFF")
					
					WAIT_WITH_DEATH_CHECKS(2000)
					
					eMissionFail = failNoise
					
					missionFailed()
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_JUMPING(PLAYER_PED_ID())
		OR IS_PED_RAGDOLL(PLAYER_PED_ID())
			SET_PLAYER_SNEAKING_NOISE_MULTIPLIER(PLAYER_ID(), 2.0)
		ELSE
			SET_PLAYER_SNEAKING_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-805.785767,179.748779,75.740738>>, <<-801.420898, 168.294800, 79.240738>>, 11.0)
		AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
		AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 0.75)
		ELSE
			SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)
		ENDIF
		
		FORCE_SONAR_BLIPS_THIS_FRAME()
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_INSIDE_HOUSE")
			STOP_AUDIO_SCENE("ARM_3_INSIDE_HOUSE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_ENTER_GARAGE")
			STOP_AUDIO_SCENE("ARM_3_ENTER_GARAGE")
		ENDIF
		
		IF bJimmyTVPLayingGame
			STOP_TV_PLAYBACK(TRUE, TV_LOC_JIMMY_BEDROOM )
			bJimmyTVPLayingGame = FALSE
		ENDIF
		
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		KILL_ANY_CONVERSATION()
		
		//Blip
		SAFE_REMOVE_BLIP(blipGardener)
		SAFE_REMOVE_BLIP(blipSon)
		SAFE_REMOVE_BLIP(blipDaughter)
		SAFE_REMOVE_BLIP(blipCoach)
		SAFE_REMOVE_BLIP(blipWife)
		SAFE_REMOVE_BLIP(blipMichael)
		SAFE_REMOVE_BLIP(blipDestination)
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		
		//Cleanup Peds
		SET_PED_KEEP_TASK(pedSon, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedSon)
		
		SET_PED_KEEP_TASK(pedDaughter, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedDaughter)
		
		SET_PED_KEEP_TASK(pedWife, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedWife)
		
		SET_PED_KEEP_TASK(pedCoach, TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedCoach)
		
		//Vehicle
		SAFE_DELETE_VEHICLE(vehClimb)
		
		//Dialogue
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 5)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 6)
		REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 7)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC StealCar()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayGarage), "stageStealCar")
		
		IF DOES_ENTITY_EXIST(vehDriveTo)
		AND NOT IS_ENTITY_DEAD(vehDriveTo)
			OVERRIDE_REPLAY_CHECKPOINT_VEHICLE(vehDriveTo)
		ENDIF
		
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
		
		//Mod Shop
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
		
		//Interior
		INTERIOR_INSTANCE_INDEX interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-49.9775, -1097.2866, 25.4223>>, "v_carshowroom")
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_beforeMission")
			DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_beforeMission")
		ENDIF
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_afterMissionA")
			DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_afterMissionA")
		ENDIF
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_afterMissionB")
			DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_afterMissionB")
		ENDIF
		
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_inMission")
			ACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_inMission")	
		ENDIF
		
		SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED) // Close the rear shutters
		
		REFRESH_INTERIOR(interiorID)
		
		bPinnedGarage = FALSE	//REFRESH_INTERIOR unpins the interior
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Radar
		bRadar = TRUE
		
		//Camera
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		//Door
		UNLOCK_DOOR(GARAGE_DOOR, V_ILEV_MM_DOOR)
		
		//Car
		IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
			SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
		ENDIF
		
		bPlayerExitCar = FALSE
		
		//Special Ability
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
		
		//Unlock Mansion Door
		REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, PLAYER_PED_ID())
		
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].doorID, DOORSTATE_UNLOCKED, TRUE, TRUE)
		
		//Dialogue
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 3, PLAYER_PED_ID(), "FRANKLIN")
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, NULL, "SIMEON")
		
		STOP_PED_SPEAKING(PLAYER_PED(CHAR_FRANKLIN), TRUE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStart, fMichaelStart)
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE)
			
			SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_UNARMED, TRUE)
			
			SAFE_SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar, VS_BACK_RIGHT)
		ENDIF
		
		SET_ENTITY_PROOFS(PLAYER_PED(CHAR_MICHAEL), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
		
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_DisableExplosionReactions, TRUE)
		
		STOP_PED_SPEAKING(PLAYER_PED(CHAR_MICHAEL), TRUE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), TRUE)
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_STEAL_CAR")
			START_AUDIO_SCENE("ARM_3_STEAL_CAR")
		ENDIF
		
		//Audio
		PLAY_AUDIO(ARM3_CAR)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Freeze
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			
			//Pin Interior
			intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
			
			PIN_INTERIOR_IN_MEMORY(intMansion)
			
			WHILE NOT IS_INTERIOR_READY(intMansion)
				PRINTLN("PINNING INTERIOR...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			bPinnedMansion = TRUE
			
			SET_LABEL_AS_TRIGGERED("GarageDoorLockedSkip", TRUE)
			
			//Unfreeze
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			
			//Set Positions
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0)
			ENDIF
			
			SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
			ACTIVATE_PHYSICS(vehCar)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			WAIT_WITH_DEATH_CHECKS(750)
			
			//Audio
			PLAY_AUDIO(ARM3_RESTART_4)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
			IF TIMERA() > 1000
				SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME) //Big metal garage door
			ENDIF
			
			IF NOT DOES_BLIP_EXIST(blipDestination)
				CLEAR_PRINTS()
				PRINT_ADV("ARM3_DEST")
				
				SAFE_REMOVE_BLIP(blipCar)
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-622.7947, -362.5217, 33.8123>>, <<-762.1870, -119.8490, 56.8795>>, 250.0, FALSE, TRUE)
//					SAFE_ADD_BLIP_LOCATION(blipDestination, <<-675.3372, -299.9155, 34.4508>>, TRUE)
//				ELSE
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<-148.7715, -1149.9026, 23.2067>>, TRUE)
					
					START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
					ADD_POINT_TO_GPS_MULTI_ROUTE(<<-273.014679, -1140.789917, 22.784359>>)
					ADD_POINT_TO_GPS_MULTI_ROUTE(<<-148.7715, -1149.9026, 23.2067>>)
					
					SET_GPS_MULTI_ROUTE_RENDER(TRUE)
//				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("ARM3_DEST")
				IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_INCAR")
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-811.17505, 187.60080, 73.30764>>) > 100.0
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
							IF IS_PLAYER_PED_IN_PERSONAL_VEHICLE(PLAYER_PED_ID())
								PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_SIMEON, "ARM3AUD", "ARM3_INCARA", CONV_PRIORITY_HIGH)
							ELSE
								PLAYER_CALL_CHAR_CELLPHONE(sPedsForConversation, CHAR_SIMEON, "ARM3AUD", "ARM3_INCARB", CONV_PRIORITY_HIGH)
							ENDIF
							
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 8.0)
							
							//Audio
//							PLAY_AUDIO(ARM3_CALL)
							
							SET_LABEL_AS_TRIGGERED("ARM3_INCAR", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("ARM3_INCAR")
				IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_INCARA")
				OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_INCARB")
					IF IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
						SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
						ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
					ENDIF
				ELSE
					IF NOT IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
						ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//Damages Car (Crashes)
//			IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
//			AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_SCRATCH")
				IF GET_ENTITY_HEALTH(vehCar) < iCarHealth
				OR GET_VEHICLE_PETROL_TANK_HEALTH(vehCar) < iCarGasTank
				OR GET_VEHICLE_ENGINE_HEALTH(vehCar) < iCarEngineHealth
					//CREATE_CONVERSATION_ADV("ARM3_SCRATCH", CONV_PRIORITY_HIGH)
					INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_FRANKLIN), "ARM3_BRAA", "FRANKLIN")	PRINTLN("INTERRUPT_CONVERSATION - Damages Car (Crashes)")
					
					iCarHealth = GET_ENTITY_HEALTH(vehCar)
					iCarGasTank = GET_VEHICLE_PETROL_TANK_HEALTH(vehCar)
					iCarEngineHealth = GET_VEHICLE_ENGINE_HEALTH(vehCar)
				ENDIF
//			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipCar)
				CLEAR_PRINTS()
				PRINT_ADV("ARM3_CAR2")
				SAFE_REMOVE_BLIP(blipDestination)
				SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-845.286560, 162.103363, 64.669838>>, <<-845.331665, 155.965317, 70.589340>>, 4.0)
			//Audio Scene
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_STEAL_CAR")
				STOP_AUDIO_SCENE("ARM_3_STEAL_CAR")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_EXIT_THROUGH_GATE")
				START_AUDIO_SCENE("ARM_3_EXIT_THROUGH_GATE")
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
				bPlayerExitCar = TRUE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
			IF IS_ENTITY_UPRIGHT(vehCar)
				iFailUprightTimer = GET_GAME_TIMER() + 10000
				
				IF IS_VEHICLE_ON_ALL_WHEELS(vehCar)
					IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-60.72, -1100.55, 25.40>>) < 1200.0
					OR bPlayerExitCar = TRUE)
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
					AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_INCARA")
					AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_INCARB")
						IF SAFE_START_CUTSCENE(DEFAULT,FALSE)	//B* 2164324: Don't activate blinders while driving
							SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
							
							//SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
							//Spawn Michael
							IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStart, fMichaelStart)
								SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE)
								
								SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_UNARMED, TRUE)
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(objGun)
								objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL), vMichaelStart)
							ENDIF
							ATTACH_ENTITY_TO_ENTITY(objGun, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
							
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
							
							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
							
							ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
							
							SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar, VS_BACK_RIGHT)
							
							//TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_PED_ID(), -1)
							
							WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								//Disable Player Movement
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
								
								WAIT_WITH_DEATH_CHECKS(0)
							ENDWHILE
							
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_intro_franklin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY, 0.31)
								//TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_loop_franklin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_SECONDARY)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_intro_michael", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1)
								TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_loop_michael", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqMain)
							CLEAR_SEQUENCE_TASK(seqMain)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							
							GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtPistol, 120, TRUE)
							SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtPistol, TRUE)
							
							WAIT_WITH_DEATH_CHECKS(0)
							
							SET_GAMEPLAY_ENTITY_HINT(vehCar, <<-0.1, 1.0, 1.1>>, TRUE, 2000, 6000, 2000, HINTTYPE_ARM3_VEHICLE)
							
							IF NOT bZoomLevel
								vehicleZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
								
								camViewModeOnFoot = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
								
								PRINTLN("Store Zoom Level GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = ", ENUM_TO_INT(GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()))
								PRINTLN("Store Zoom Level GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = ", ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)))
								
								bZoomLevel = TRUE
							ENDIF
							
							SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_NEAR)
							
							SAFE_SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
							
							REPLAY_RECORD_BACK_FOR_TIME(0.0, 15.0)
							
							ADVANCE_STAGE()
						ENDIF
					ENDIF
				ENDIF
			ELIF GET_ENTITY_SPEED(vehCar) < 1.0
			AND GET_GAME_TIMER() > iFailUprightTimer
				eMissionFail = failCarDestroyed
				
				missionFailed()
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
			SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
		ENDIF
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_STEAL_CAR")
			STOP_AUDIO_SCENE("ARM_3_STEAL_CAR")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_EXIT_THROUGH_GATE")
			STOP_AUDIO_SCENE("ARM_3_EXIT_THROUGH_GATE")
		ENDIF
		
		//Special Ability
		SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
		ENABLE_SPECIAL_ABILITY(PLAYER_ID(), FALSE)
		
		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			//Spawn Michael
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SPAWN_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelStart, fMichaelStart)
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_DENIM, FALSE)
			
			SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_UNARMED, TRUE)
			
			IF NOT DOES_ENTITY_EXIST(objGun)
				objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL), vMichaelStart)
			ENDIF
			ATTACH_ENTITY_TO_ENTITY(objGun, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], relGroupBuddy)
			
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar, VS_BACK_RIGHT)
			
			//TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_PED_ID(), -1)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				OVER_HOOD_CINEMATIC_CAM()
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			#IF IS_DEBUG_BUILD
			IF bAutoSkipping = FALSE
			#ENDIF
			SET_GAMEPLAY_ENTITY_HINT(vehCar, <<-0.1, 1.0, 1.1>>, TRUE, 2000, 6000, 2000, HINTTYPE_ARM3_VEHICLE)
			
			IF NOT bZoomLevel
				vehicleZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
				
				camViewModeOnFoot = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
				
				PRINTLN("Store Zoom Level GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = ", ENUM_TO_INT(GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()))
				PRINTLN("Store Zoom Level GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = ", ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)))
				
				bZoomLevel = TRUE
			ENDIF
			
			SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_NEAR)
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
			
			OPEN_SEQUENCE_TASK(seqMain)
				TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_intro_franklin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
//				TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_loop_franklin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_SECONDARY)
			CLOSE_SEQUENCE_TASK(seqMain)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
			CLEAR_SEQUENCE_TASK(seqMain)
			
			OPEN_SEQUENCE_TASK(seqMain)
				TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_intro_michael")
				TASK_PLAY_ANIM(NULL, sAnimDictArm3, "michaelappears_loop_michael", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
			CLOSE_SEQUENCE_TASK(seqMain)
			TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqMain)
			CLEAR_SEQUENCE_TASK(seqMain)
			
			GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtPistol, 120, TRUE)
			SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], wtPistol, TRUE)
//			SET_PED_CAN_SWITCH_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		
		SAFE_SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
		
		//Cleanup Michael
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
		
		SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME) //Big metal garage door
				
		SAFE_REMOVE_BLIP(blipCar)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

BOOL bHintCamFrameDelay

PROC DriveTo()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Door
		SET_LABEL_AS_TRIGGERED("GarageDoorLockedSkip", TRUE)
		
		//Radar
		bRadar = TRUE
		
		HANG_UP_AND_PUT_AWAY_PHONE()
		
		DISABLE_CELLPHONE(TRUE)
		
		//Dialogue
		STOP_PED_SPEAKING(PLAYER_PED(CHAR_FRANKLIN), TRUE)
		STOP_PED_SPEAKING(PLAYER_PED(CHAR_MICHAEL), TRUE)
		
		//Camera
		IF bPlayerExitCar = FALSE
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		ENDIF
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
		ENDIF
		
		//Anim Clip Set
		REQUEST_CLIP_SET("clipset@missarmenian3@franklin_driving")
		
		WHILE NOT HAS_CLIP_SET_LOADED("clipset@missarmenian3@franklin_driving")
			REQUEST_CLIP_SET("clipset@missarmenian3@franklin_driving")
			
			OVER_HOOD_CINEMATIC_CAM()
			
			WAIT_WITH_DEATH_CHECKS(0)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING CLIP SET: clipset@missarmenian3@franklin_driving...")	#ENDIF
		ENDWHILE
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_DEALERSHIP")
			START_AUDIO_SCENE("ARM_3_DRIVE_TO_DEALERSHIP")
		ENDIF
		
		iFailTimer = GET_GAME_TIMER() + 180000
		
		//Cutscene
		REQUEST_CUTSCENE("Armenian_3_mcs_6")
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		
		SET_ROADS_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		SET_ROADS_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		SET_ROADS_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		SET_ROADS_IN_AREA(<<-149.3830, -1150.3278, 23.1441>> - <<5.0, 5.0, 5.0>>, <<-149.3830, -1150.3278, 23.1441>> + <<5.0, 5.0, 5.0>>, FALSE)
		
		CLEAR_AREA(<<-149.3830, -1150.3278, 23.1441>>, 10.0, TRUE)
		CLEAR_AREA(<<-149.3830, -1150.3278, 23.1441>>, 10.0, TRUE)
		CLEAR_AREA(<<-149.3830, -1150.3278, 23.1441>>, 10.0, TRUE)
		CLEAR_AREA(<<-149.3830, -1150.3278, 23.1441>>, 10.0, TRUE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		shapeTestIndex = NULL
		
		IF SKIPPED_STAGE()
			//Set Positions
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
			
			IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
				SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
				
				SET_VEHICLE_POSITION(vehCar, <<-676.2552, -278.2624, 35.1000>>, 209.8584)
			ENDIF
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			WAIT_WITH_DEATH_CHECKS(0)
			
			SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
			
			SET_VEHICLE_FORWARD_SPEED(vehCar, 20.0)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Objective
			PRINT_ADV("ARM3_DEST")
			
			SAFE_ADD_BLIP_LOCATION(blipDestination, <<-148.7715, -1149.9026, 23.2067>>, TRUE)
			
			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
			ADD_POINT_TO_GPS_MULTI_ROUTE(<<-273.014679, -1140.789917, 22.784359>>)
			ADD_POINT_TO_GPS_MULTI_ROUTE(<<-148.7715, -1149.9026, 23.2067>>)
			
			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
			
			//Audio
//			PLAY_AUDIO(ARM3_RESTART_5)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - Armenian_3_mcs_6
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
		ENDIF
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMansion, <<40.0, 40.0, 40.0>>)
			SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME) //Big metal garage door
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("PistolCock")
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2112958
				
				IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") > 0.4
					PLAY_SOUND_FRONTEND(-1, "ARM_3_PISTOL_COCK")
					
					SET_LABEL_AS_TRIGGERED("PistolCock", TRUE)
				ENDIF
			ENDIF
			
			IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") > 0.45
				IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_GUN")
					SET_PED_IN_VEHICLE_CONTEXT(PLAYER_PED_ID(), GET_HASH_KEY("MISS_ARMENIAN3_FRANKLIN_TENSE"))
					
					SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
					
					//PLAY_SOUND_FRONTEND(-1, "ARM_3_PISTOL_COCK")
					
					//Audio
					PLAY_AUDIO(ARM3_MIC)
				ENDIF
				
				CREATE_CONVERSATION_ADV("ARM3_GUN")
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("ARM3_GUN")
		AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_GUNA")
		AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_GUNB")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF ABSF(GET_ENTITY_SPEED(vehCar)) >= 1.0
					CREATE_CONVERSATION_ADV("ARM3_GUNA")
				ELSE
					CREATE_CONVERSATION_ADV("ARM3_GUNB")
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael")
		AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") > 0.57)
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop2_michael")
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop3_michael")
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop4_michael")
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop5_michael")
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop6_michael")
			IF (HAS_LABEL_BEEN_TRIGGERED("ARM3_GUNA")
			OR HAS_LABEL_BEEN_TRIGGERED("ARM3_GUNB"))
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF (NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<-289.8, -1141.7, 67.078789>>, 90.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<-101.7, -1383.3, 73.342819>>, 90.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<81.4, -1129.5, 73.334045>>, 90.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<-22.4, -914.9, 72.966431>>, 90.0))
				AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_STOP")
					CREATE_CONVERSATION_ADV("ARM3_DRIV")
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_INCAR")
				IF IS_FOLLOW_VEHICLE_CAM_ACTIVE()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) 
						VEHICLE_ZOOM_LEVEL tempZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
				        
						IF ((tempZoomLevel = VEHICLE_ZOOM_LEVEL_NEAR)
						OR (tempZoomLevel = VEHICLE_ZOOM_LEVEL_MEDIUM)
						OR (tempZoomLevel = VEHICLE_ZOOM_LEVEL_FAR))
						AND NOT IS_GAMEPLAY_CAM_LOOKING_BEHIND()
							SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
							
							PRINT_HELP_ADV("ARM3HLP_INCAR")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
//		IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael")
//		AND ((bPlayerExitCar = TRUE
//		AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") >= 0.31
//		OR GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") >= 0.4)	//0.31
//		AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") <= 0.55)	//0.47)
//			IF ABSF(GET_ENTITY_SPEED(vehCar)) >= 5.0
//				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK
//					TASK_VEHICLE_DRIVE_WANDER(PLAYER_PED_ID(), vehCar, 20.0, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
//				ENDIF
//			ENDIF
//		ELSE
//			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
//				CLEAR_PED_TASKS(PLAYER_PED_ID())
//			ENDIF
//		ENDIF
		
		BOOL bHintActive
		
		IF NOT bHintCamFrameDelay
		AND SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARM3HLP_INCAR")
				SAFE_CLEAR_HELP()	
			ENDIF
			
			bHintActive = TRUE
		ENDIF
		
		IF IS_CUSTOM_MENU_ON_SCREEN()
			bHintCamFrameDelay = TRUE
		ELSE
			bHintCamFrameDelay = FALSE
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("ARM3_GUN")
		OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael")
			IF NOT IS_PHONE_ONSCREEN()
			AND bHintActive
//			OR (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael")
//			AND ((bPlayerExitCar = TRUE
//			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") >= 0.31
//			OR GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") >= 0.4)	//0.31
//			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") <= 0.55))	//0.47))
				bOverHoodCinematicCam = TRUE
				
//				IF SHOULD_CONTROL_CHASE_HINT_CAM(localChaseHintCamStruct)
					OVER_HOOD_CINEMATIC_CAM()
//				ENDIF
			ELSE
				bOverHoodCinematicCam = FALSE
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("ARM3_GUN")
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar)
			SAFE_REMOVE_BLIP(blipFranklin)
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_GAMEPLAY_CAM_RENDERING()
					IF NOT DOES_BLIP_EXIST(blipDestination)
						CLEAR_PRINTS()
						PRINT_ADV("ARM3_TAKE", DEFAULT_GOD_TEXT_TIME, FALSE)
						SAFE_REMOVE_BLIP(blipCar)
						
						SAFE_ADD_BLIP_LOCATION(blipDestination, <<-148.7715, -1149.9026, 23.2067>>, TRUE)
						
						START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE, TRUE)
						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-273.014679, -1140.789917, 22.784359>>)
						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-148.7715, -1149.9026, 23.2067>>)
						
						SET_GPS_MULTI_ROUTE_RENDER(TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipCar)
					CLEAR_PRINTS()
					PRINT_ADV("ARM3_CAR2")
					SAFE_REMOVE_BLIP(blipDestination)
					SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
		AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("Destination1")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination2")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination3")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination4")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<-289.8, -1141.7, 67.078789>>, 90.0)
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV("ARM3_STOP")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("Destination1", TRUE)
				ENDIF
			ELIF HAS_LABEL_BEEN_TRIGGERED("Destination1")
				IF HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND (BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar))
				//OR GET_ENTITY_SPEED(vehCar) < 1.0)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
//					WAIT_WITH_DEATH_CHECKS(750)
//					
//					WHILE IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
//					OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
//					
//					ADVANCE_STAGE()

					// bug 2006108
					IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
					AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
						ADVANCE_STAGE()
					ENDIF
					
				ENDIF
				//<<-148.7715, -1149.9026, 23.2067>>, 271.8071
				IF NOT HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND IS_ENTITY_AT_COORD(vehCar, <<-148.7715, -1149.9026, 23.2067>>, <<3.0, 3.0, LOCATE_SIZE_HEIGHT>>, TRUE)
				AND IS_ENTITY_UPRIGHT(vehCar)
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					
					IF NOT DOES_BLIP_EXIST(blipDestination)
						CLEAR_GPS_MULTI_ROUTE()
						
						SAFE_REMOVE_BLIP(blipDestination)
						SAFE_ADD_BLIP_LOCATION(blipDestination, <<-148.7715, -1149.9026, 23.2067>>, TRUE)
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("HaltVehicle", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("Destination1")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination2")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination3")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination4")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<-101.7, -1383.3, 73.342819>>, 90.0)
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					
					CLEAR_GPS_MULTI_ROUTE()
					
					SAFE_REMOVE_BLIP(blipDestination)
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<-91.7239, -1180.3030, 25.3327>>, TRUE)
					
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV("ARM3_STOP")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("Destination2", TRUE)
				ENDIF
			ELIF HAS_LABEL_BEEN_TRIGGERED("Destination2")
				IF HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND (BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar))
				//OR GET_ENTITY_SPEED(vehCar) < 1.0)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
//					WAIT_WITH_DEATH_CHECKS(750)
//					
//					WHILE IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
//					OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
//					
//					ADVANCE_STAGE()
	
					// bug 2006108
					IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
					AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
						ADVANCE_STAGE()
					ENDIF
					
				ENDIF
				//<<-91.7239, -1180.3030, 25.3327>>, 3.5979
				IF NOT HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND IS_ENTITY_AT_COORD(vehCar, <<-91.7239, -1180.3030, 25.3327>>, <<3.0, 3.0, LOCATE_SIZE_HEIGHT>>, TRUE)
				AND IS_ENTITY_UPRIGHT(vehCar)
					SET_LABEL_AS_TRIGGERED("HaltVehicle", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("Destination1")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination2")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination3")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination4")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<81.4, -1129.5, 73.334045>>, 90.0)
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					
					CLEAR_GPS_MULTI_ROUTE()
					
					SAFE_REMOVE_BLIP(blipDestination)
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<-64.8446, -1130.0518, 24.7219>>, TRUE)
					
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV("ARM3_STOP")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("Destination3", TRUE)
				ENDIF
			ELIF HAS_LABEL_BEEN_TRIGGERED("Destination3")
				IF HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND (BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar))
				//OR GET_ENTITY_SPEED(vehCar) < 1.0)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
//					WAIT_WITH_DEATH_CHECKS(750)
//					
//					WHILE IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
//					OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
//					
//					ADVANCE_STAGE()
					
					// bug 2006108
					IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
					AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
						ADVANCE_STAGE()
					ENDIF

				ENDIF
				//<<-64.8446, -1130.0518, 24.7219>>, 92.7692
				IF NOT HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND IS_ENTITY_AT_COORD(vehCar, <<-64.8446, -1130.0518, 24.7219>>, <<3.0, 3.0, LOCATE_SIZE_HEIGHT>>, TRUE)
				AND IS_ENTITY_UPRIGHT(vehCar)
					SET_LABEL_AS_TRIGGERED("HaltVehicle", TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("Destination1")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination2")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination3")
			AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination4")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.467453, -1140.700806, 19.820740>>, <<-22.4, -914.9, 72.966431>>, 90.0)
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					
					CLEAR_GPS_MULTI_ROUTE()
					
					SAFE_REMOVE_BLIP(blipDestination)
					SAFE_ADD_BLIP_LOCATION(blipDestination, <<-97.2467, -1087.5890, 25.2988>>, TRUE)
					
					IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION_ADV("ARM3_STOP")
					ENDIF
					
					SET_LABEL_AS_TRIGGERED("Destination4", TRUE)
				ENDIF
			ELIF HAS_LABEL_BEEN_TRIGGERED("Destination4")
				IF HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND (BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar))
				//OR GET_ENTITY_SPEED(vehCar) < 1.0)
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
//					WAIT_WITH_DEATH_CHECKS(750)
//					
//					WHILE IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
//					OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
//					
//					ADVANCE_STAGE()

					// bug 2006108
					IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_STOP")
					AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
						ADVANCE_STAGE()
					ENDIF
					
				ENDIF
				//<<-97.2467, -1087.5890, 25.2988>>, 161.1787
				IF NOT HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
				AND IS_ENTITY_AT_COORD(vehCar, <<-97.2467, -1087.5890, 25.2988>>, <<3.0, 3.0, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
				AND IS_ENTITY_UPRIGHT(vehCar)
					SET_LABEL_AS_TRIGGERED("HaltVehicle", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objGlass)
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-59.80, -1098.78, 24.92>>, <<100.0, 100.0, 50.0>>)
				objGlass = CREATE_OBJECT_NO_OFFSET(PROP_SHOWROOM_GLASS_1B, <<-59.8700, -1098.8400, 27.20>>)	//objGlass = GET_CLOSEST_OBJECT_OF_TYPE(<<-59.80, -1098.78, 24.92>>, 1.0, PROP_SHOWROOM_GLASS_1B)
				SET_ENTITY_ROTATION(objGlass, <<0.0, 0.0, 121.5>>)
				FREEZE_ENTITY_POSITION(objGlass, TRUE)
			ENDIF
		ENDIF
		
		//Disable Player Movement
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)	//Until driveby control mapping is fixed
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		
		//Fail Timer
		IF GET_GAME_TIMER() > iFailTimer
			IF HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_7")
				IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED(CHAR_MICHAEL), vehCar, VS_BACK_RIGHT)
					WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar, 10.0, 10)
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					INTERIOR_INSTANCE_INDEX intCurrent = GET_INTERIOR_FROM_ENTITY(vehCar)
					
					ENTITY_INDEX hitEntity
					VECTOR vTempMin
					VECTOR vTempMax
					VECTOR vHitPos
					VECTOR vHitNorm	
					INT iReturnValue = -1
					
					//Now run a shapetest to check if the vehicle will collide with any game objects
					GET_MODEL_DIMENSIONS(BJXL, vTempMin, vTempMax)
					PRINTLN("MODEL_DIMENSIONS(BJXL) = ", vTempMin, ", ", vTempMax)
					
					IF shapeTestIndex = NULL
						PRINTLN("shapeTestIndex = NULL")
						shapeTestIndex = START_SHAPE_TEST_BOX(GET_ENTITY_COORDS(vehCar), vTempMax - vTempMin, GET_ENTITY_ROTATION(vehCar), DEFAULT, SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_FOLIAGE | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_GLASS, vehCar)
					ENDIF
					
					IF shapeTestIndex != NULL
						PRINTLN("shapeTestIndex != NULL")
						SHAPETEST_STATUS shapeTestResult
						
						shapeTestResult = GET_SHAPE_TEST_RESULT(shapeTestIndex, iReturnValue, vHitPos, vHitNorm, hitEntity)
						
						#IF IS_DEBUG_BUILD
						IF shapeTestResult = SHAPETEST_STATUS_RESULTS_NOTREADY
							PRINTLN("shapeTestResult = SHAPETEST_STATUS_RESULTS_NOTREADY")
						ENDIF
						
						IF shapeTestResult = SHAPETEST_STATUS_NONEXISTENT
							PRINTLN("shapeTestResult = SHAPETEST_STATUS_NONEXISTENT")
						ENDIF
						#ENDIF
						
						IF shapeTestResult = SHAPETEST_STATUS_RESULTS_READY
							PRINTLN("shapeTestResult = SHAPETEST_STATUS_RESULTS_READY")
							shapeTestIndex = NULL
						ENDIF
						
						PRINTLN("iReturnValue = ", iReturnValue)
						
						IF shapeTestResult != SHAPETEST_STATUS_RESULTS_NOTREADY
							IF iReturnValue < 1
							AND IS_ENTITY_UPRIGHT(vehCar, 90)
							AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-52.025787, -1095.310791, 32.230316>>, <<50.0, 50.0, 8.0>>)
							AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-148.7715, -1149.9026, 23.2067>>, <<20.0, 20.0, 8.0>>)
							AND NOT (intCurrent != NULL AND GET_INTERIOR_GROUP_ID(intCurrent) = 1)
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Franklins_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
								//SET_CUTSCENE_TRIGGER_AREA(VECTOR_ZERO, 0.0, GET_ENTITY_HEADING(vehCar), 0.0)
								
								SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
								
								//START_CUTSCENE_AT_COORDS(GET_ENTITY_COORDS(vehCar))
								
								START_CUTSCENE()
								
								IF NOT IS_ENTITY_UPRIGHT(vehCar, 45)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
								ENDIF
								
								SET_CUTSCENE_ORIGIN_AND_ORIENTATION(GET_ENTITY_COORDS(vehCar), GET_ENTITY_ROTATION(vehCar), 0)
								
								WAIT_WITH_DEATH_CHECKS(0)
								
								SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
								
								SAFE_DELETE_OBJECT(objGun)
								
								WHILE NOT HAS_CUTSCENE_FINISHED()
									IF GET_CUTSCENE_TIME() >= 18000 - (FAIL_EFFECT_TIME + FAIL_OUT_EFFECT_TIME) - (100)
										SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
										
										eMissionFail = failLeftRoute
										
										missionFailed()
									ENDIF
									
									WAIT_WITH_DEATH_CHECKS(0)
								ENDWHILE
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								
								eMissionFail = failLeftRoute
								
								missionFailed()
							ELSE
								eMissionFail = failLeftRoute
								
								missionFailed()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
				ELSE
					REQUEST_CUTSCENE("Armenian_3_mcs_7")
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehCar)
		AND NOT IS_ENTITY_DEAD(vehCar)
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			AND NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_SITTING_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar)
					eMissionFail = failCarDestroyed
					
					missionFailed()
				ENDIF
			ENDIF
		ENDIF
		
		//Bonnet Camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		//bOverHoodCinematicCam = FALSE
		
		IF NOT HAS_THIS_CUTSCENE_LOADED("Armenian_3_mcs_6")
			REMOVE_CUTSCENE()
			
			WHILE HAS_CUTSCENE_LOADED()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
		ENDIF
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_DRIVE_TO_DEALERSHIP")
			STOP_AUDIO_SCENE("ARM_3_DRIVE_TO_DEALERSHIP")
		ENDIF
		
		//Phone
		DISABLE_CELLPHONE(FALSE)
		
		IF DOES_CAM_EXIST(camCinematic)
			IF IS_CAM_RENDERING(camCinematic)
				//RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENABLE_SPECIAL_ABILITY(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
//		CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(1.0)
//		CASCADE_SHADOWS_INIT_SESSION()
//		CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
//		
//		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
//			CLEAR_TIMECYCLE_MODIFIER()
//		ENDIF
//		
//		SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
		
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		
		SET_VEHICLE_EXTRA(vehCar, 5, TRUE)
		
		SAFE_REMOVE_BLIP(blipCar)
		SAFE_REMOVE_BLIP(blipFranklin)
		
		CLEAR_GPS_MULTI_ROUTE()
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC CutsceneArrive()
	IF INIT_STAGE()
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cutscene
		REQUEST_CUTSCENE("Armenian_3_mcs_6")
		
//		CREATE_CONVERSATION_ADV("ARM3_ARIV")
		
		//Audio
		PLAY_AUDIO(ARM3_CS)
		
		//Bonnet Camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		IF SKIPPED_STAGE()
			//Set Positions
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
			
			IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
				SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
				
				SET_VEHICLE_POSITION(vehCar, <<-148.7715, -1149.9026, 23.2067>>, 271.8071)
			ENDIF
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Audio
			PLAY_AUDIO(ARM3_RESTART_6)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - Armenian_3_mcs_6
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
		ENDIF
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			CLEAR_PRINTS()
			
			ADVANCE_STAGE()
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(vehCar), 200.0)
				ENDIF
				
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("Armenian_3_mcs_6")
					IF DOES_CAM_EXIST(camCinematic)
						DETACH_CAM(camCinematic)
					ENDIF
					
					VECTOR vCutsceneCoords
					FLOAT fCutsceneHeading
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DEFAULT, CEO_PRESERVE_HAIR_SCALE | CEO_INSTANT_HAIR_SCALE_SETUP)
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DEFAULT, CEO_PRESERVE_HAIR_SCALE | CEO_INSTANT_HAIR_SCALE_SETUP)
					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Jimmys_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					//<<-148.7715, -1149.9026, 23.2067>>, 271.8071
					//IF IS_ENTITY_AT_COORD(vehCar, <<-148.7715, -1149.9026, 23.2067>>, <<3.0, 3.0, 3.0>>)
						vCutsceneCoords = <<-148.7715, -1149.9026, 23.2067>>
						fCutsceneHeading = 271.8071
					//ENDIF
					
					//<<-91.7239, -1180.3030, 25.3327>>, 3.5979
					IF IS_ENTITY_AT_COORD(vehCar, <<-91.7239, -1180.3030, 25.3327>>, <<3.0, 3.0, 3.0>>)
						vCutsceneCoords = <<-91.7239, -1180.3030, 25.3327>>
						fCutsceneHeading = 3.5979
					ENDIF
					
					//<<-64.8446, -1130.0518, 24.7219>>, 92.7692
					IF IS_ENTITY_AT_COORD(vehCar, <<-64.8446, -1130.0518, 24.7219>>, <<3.0, 3.0, 3.0>>)
						vCutsceneCoords = <<-64.8446, -1130.0518, 24.7219>>
						fCutsceneHeading = 92.7692
					ENDIF
					
					//<<-97.2467, -1087.5890, 25.2988>>, 161.1787
					IF IS_ENTITY_AT_COORD(vehCar, <<-97.2467, -1087.5890, 25.2988>>, <<3.0, 3.0, 3.0>>)
						vCutsceneCoords = <<-97.2467, -1087.5890, 25.2988>>
						fCutsceneHeading = 161.1787
					ENDIF
					
					PRINTLN("vCutsceneCoords = <<", vCutsceneCoords.X, ", ", vCutsceneCoords.Y, ", ", vCutsceneCoords.Z, ">>")
					PRINTLN("fCutsceneHeading = ", fCutsceneHeading)
					
					SET_CUTSCENE_TRIGGER_AREA(VECTOR_ZERO, 0.0, fCutsceneHeading - 270.0, 0.0)
					
					START_CUTSCENE_AT_COORDS(vCutsceneCoords + <<0.0, 0.0, 1.0>>, CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					// --- Bug 1871015
					bOverHoodCinematicCam = FALSE
					
					OVER_HOOD_CINEMATIC_CAM()
					
					SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
					
					//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					// ---
					
					CLEAR_AREA(GET_ENTITY_COORDS(vehCar), 25.0, TRUE)
					
					SAFE_SET_ENTITY_VISIBLE(objGun, FALSE)
					
					CLEAR_TEXT()
					
					SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehCar, TRUE)
					
					//Radar
					bRadar = FALSE
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 1
				//HD Vehicle
				SET_FORCE_HD_VEHICLE(vehCar, TRUE)
				
				//Hide Traffic
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				//Rain Particle Effects
				IF IS_CUTSCENE_PLAYING()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_1")
						SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehCar, TRUE)
						
						SET_LABEL_AS_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_1", TRUE)
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_2")
						IF GET_CUTSCENE_TIME() > ROUND(4.799000 * 1000.0)
						AND HAS_CUTSCENE_CUT_THIS_FRAME()
							SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehCar, FALSE)
							
							SET_LABEL_AS_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_2", TRUE)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_3")
						IF GET_CUTSCENE_TIME() > ROUND(14.99900 * 1000.0)
						AND HAS_CUTSCENE_CUT_THIS_FRAME()
							SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehCar, TRUE)
							
							SET_LABEL_AS_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_3", TRUE)
						ENDIF
					ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_4")
						IF GET_CUTSCENE_TIME() > ROUND(17.165668 * 1000.0)
						AND HAS_CUTSCENE_CUT_THIS_FRAME()
							SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehCar, FALSE)
							
							SET_LABEL_AS_TRIGGERED("ARMENIAN_3_MCS_6_SHOT_4", TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar, VS_BACK_RIGHT)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(objGun)
						objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL), vMichaelStart)
					ENDIF
					ATTACH_ENTITY_TO_ENTITY(objGun, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
					
//					WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						WAIT_WITH_DEATH_CHECKS(0)
//					ENDWHILE
					
					IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
						TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmys_Car")
//					//<<-148.7715, -1149.9026, 23.2067>>, 271.8071
//					IF IS_ENTITY_AT_COORD(vehCar, <<-148.7715, -1149.9026, 23.2067>>, <<3.0, 3.0, 3.0>>)
//						//SET_VEHICLE_POSITION(vehCar, <<-148.7509, -1149.9504, 23.2053>>, 271.7903)
//						SET_ENTITY_COORDS_NO_OFFSET(vehCar, <<-148.7664, -1149.9998, 24.1124>>)
//						SET_ENTITY_ROTATION(vehCar, <<3.3850, 2.9552, -88.1208>>)
//					ENDIF
//					
//					//<<-91.7239, -1180.3030, 25.3327>>, 3.5979
//					IF IS_ENTITY_AT_COORD(vehCar, <<-91.7239, -1180.3030, 25.3327>>, <<3.0, 3.0, 3.0>>)
//						SET_VEHICLE_POSITION(vehCar, <<-91.7239, -1180.3030, 25.3327>>, 3.5979)
//					ENDIF
//					
//					//<<-64.8446, -1130.0518, 24.7219>>, 92.7692
//					IF IS_ENTITY_AT_COORD(vehCar, <<-64.8446, -1130.0518, 24.7219>>, <<3.0, 3.0, 3.0>>)
//						SET_VEHICLE_POSITION(vehCar, <<-64.8446, -1130.0518, 24.7219>>, 92.7692)
//					ENDIF
//					
//					//<<-97.2467, -1087.5890, 25.2988>>, 161.1787
//					IF IS_ENTITY_AT_COORD(vehCar, <<-97.2467, -1087.5890, 25.2988>>, <<3.0, 3.0, 3.0>>)
//						SET_VEHICLE_POSITION(vehCar, <<-97.2467, -1087.5890, 25.2988>>, 161.1787)
//					ENDIF
					
					SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
					
					ACTIVATE_PHYSICS(vehCar)
					
					SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
					
//					SET_LABEL_AS_TRIGGERED("WheelCompression[vehCar](Armenian_3_mcs_6)", TRUE)
				ENDIF
				
				SET_VEHICLE_BRAKE_LIGHTS(vehCar, FALSE)
				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehCar](Armenian_3_mcs_6)")
//					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehCar)
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-56.13, -1097.60, 25.16>>) - GET_ENTITY_HEADING(PLAYER_PED_ID()))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
					
					//Clear a path
					VECTOR vPlayerCoords
					
					vPlayerCoords = GET_ENTITY_COORDS(vehCar)
					
					CLEAR_ANGLED_AREA_OF_VEHICLES(<<vPlayerCoords.X, vPlayerCoords.Y, 30.0>>, <<-55.8188, -1096.4149, 20.0>>, 30.0)
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					IF WAS_CUTSCENE_SKIPPED()
						//<<-148.7715, -1149.9026, 23.2067>>, 271.8071
						IF IS_ENTITY_AT_COORD(vehCar, <<-148.7715, -1149.9026, 23.2067>>, <<3.0, 3.0, 3.0>>)
							//SET_VEHICLE_POSITION(vehCar, <<-148.7509, -1149.9504, 23.2053>>, 271.7903)
							SET_ENTITY_COORDS_NO_OFFSET(vehCar, <<-148.7664, -1149.9998, 24.1124>>)
							SET_ENTITY_ROTATION(vehCar, <<3.3850, 2.9552, -88.1208>>)
						ENDIF
						
						//<<-91.7239, -1180.3030, 25.3327>>, 3.5979
						IF IS_ENTITY_AT_COORD(vehCar, <<-91.7239, -1180.3030, 25.3327>>, <<3.0, 3.0, 3.0>>)
							SET_VEHICLE_POSITION(vehCar, <<-91.7239, -1180.3030, 25.3327>>, 3.5979)
						ENDIF
						
						//<<-64.8446, -1130.0518, 24.7219>>, 92.7692
						IF IS_ENTITY_AT_COORD(vehCar, <<-64.8446, -1130.0518, 24.7219>>, <<3.0, 3.0, 3.0>>)
							SET_VEHICLE_POSITION(vehCar, <<-64.8446, -1130.0518, 24.7219>>, 92.7692)
						ENDIF
						
						//<<-97.2467, -1087.5890, 25.2988>>, 161.1787
						IF IS_ENTITY_AT_COORD(vehCar, <<-97.2467, -1087.5890, 25.2988>>, <<3.0, 3.0, 3.0>>)
							SET_VEHICLE_POSITION(vehCar, <<-97.2467, -1087.5890, 25.2988>>, 161.1787)
						ENDIF
					ENDIF
					
					SET_ROADS_IN_ANGLED_AREA(<<-57.644516, -1097.634033, 35.422352>>, <<-145.066711, -1156.563843, 23.026037>>, 20.0, FALSE, FALSE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		//Bonnet Camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
	ENDIF
	
	IF CLEANUP_STAGE()
		SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehCar, FALSE)
		
		REMOVE_CUTSCENE()
		
		REPLAY_STOP_EVENT()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar)
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar, VS_BACK_RIGHT)
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
		ENDIF
		
		//SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
		
		CLEAR_TEXT()
		
		SAFE_SET_ENTITY_VISIBLE(objGun, TRUE)
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
			SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
		ENDIF
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC RammingSpeed()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayDealer), "stageRammingSpeed")
		
		bCameraViewToggle = FALSE
		
		//Phone
		DISABLE_CELLPHONE(TRUE)
		
		//Reset
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		//Interior
		INTERIOR_INSTANCE_INDEX interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-49.9775, -1097.2866, 25.4223>>, "v_carshowroom")
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_beforeMission")
			DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_beforeMission")
		ENDIF
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_afterMissionA")
			DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_afterMissionA")
		ENDIF
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_afterMissionB")
			DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_afterMissionB")
		ENDIF
		
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, "csr_inMission")
			ACTIVATE_INTERIOR_ENTITY_SET(interiorID, "csr_inMission")	
		ENDIF
		
		SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED) // Close the rear shutters
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Dialogue
		STOP_PED_SPEAKING(PLAYER_PED(CHAR_FRANKLIN), TRUE)
		STOP_PED_SPEAKING(PLAYER_PED(CHAR_MICHAEL), TRUE)
		
		//Driving Anims
		//Anim Clip Set
		REQUEST_CLIP_SET("clipset@missarmenian3@franklin_driving")
		
		WHILE NOT HAS_CLIP_SET_LOADED("clipset@missarmenian3@franklin_driving")
			REQUEST_CLIP_SET("clipset@missarmenian3@franklin_driving")
			
			OVER_HOOD_CINEMATIC_CAM()
			
			WAIT_WITH_DEATH_CHECKS(0)	#IF IS_DEBUG_BUILD	PRINTLN("LOADING CLIP SET: clipset@missarmenian3@franklin_driving...")	#ENDIF
		ENDWHILE
		
		SET_PED_IN_VEHICLE_CONTEXT(PLAYER_PED_ID(), GET_HASH_KEY("MISS_ARMENIAN3_FRANKLIN_TENSE"))
		
		bRadar = TRUE
		
		SET_ENTITY_PROOFS(PLAYER_PED(CHAR_MICHAEL), TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
		
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED(CHAR_MICHAEL), PCF_DisableExplosionReactions, TRUE)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED(CHAR_MICHAEL), TRUE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		//Cam
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		//Car
		FORCE_USE_AUDIO_GAME_OBJECT(vehCar, "BJXL_ARMENIAN_3")
		
		SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
		
		//Gun Prop
		IF NOT DOES_ENTITY_EXIST(objGun)
			objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL), vMichaelStart)
		ENDIF
		ATTACH_ENTITY_TO_ENTITY(objGun, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
		
		WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
			TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		
		CLEAR_TEXT()
		SET_LABEL_AS_TRIGGERED("ARM3_GUN", TRUE)
		SET_LABEL_AS_TRIGGERED("ARM3_GUNA", TRUE)
		SET_LABEL_AS_TRIGGERED("ARM3_GUNB", TRUE)
		
		SAFE_REMOVE_BLIP(blipDestination)
		SAFE_ADD_BLIP_LOCATION(blipDestination, vDealership, FALSE)
		
		iFailTimer = GET_GAME_TIMER() + 120000
		
		//Cutscene
		REQUEST_CUTSCENE("Armenian_3_mcs_8")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_RAM_DEALERSHIP")
			START_AUDIO_SCENE("ARM_3_RAM_DEALERSHIP")
		ENDIF
		
		//Audio
		LOAD_AUDIO(ARM3_HIT)
		
		PLAY_AUDIO(ARM3_SPEED)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		SET_VEHICLE_BRAKE_LIGHTS(vehCar, FALSE)
		
		//Vehicle Zoom Level
		IF NOT bZoomLevel
			vehicleZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
			
			camViewModeOnFoot = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
			
			PRINTLN("Store Zoom Level GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = ", ENUM_TO_INT(GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()))
			PRINTLN("Store Zoom Level GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = ", ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)))
			
			bZoomLevel = TRUE
		ENDIF
		
		//Bonnet Camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		
		//Camera
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		IF SKIPPED_STAGE()
			//Audio
			PLAY_AUDIO(ARM3_RESTART_7)
			
			//Radio
			SET_VEHICLE_RADIO_ENABLED(vehCar, FALSE)
			SET_USER_RADIO_CONTROL_ENABLED(FALSE)
			
			//Set Positions
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar, VS_BACK_RIGHT)
			
			IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
				TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
			ENDIF
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
			
			IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
				SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
				
				SET_VEHICLE_POSITION(vehCar, <<-72.6144, -1106.4908, 25.0423>>, 299.8838)
			ENDIF
			
			//Load Scene
			CLEAR_AREA(<<-59.80, -1098.78, 24.92>>, 100.0, TRUE)
			
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			REFRESH_INTERIOR(interiorID)
			
			bPinnedGarage = FALSE	//REFRESH_INTERIOR unpins the interior
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-56.13, -1097.60, 25.16>>) - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			
			//Rayfire
			rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
			
			WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
				rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
				
				PRINTLN("Getting Rayfire Map Object - DES_Showroom")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			IF NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1) != RFMO_STATE_START
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1, RFMO_STATE_STARTING)
				ENDIF
			ENDIF
			
			//Position player again (1st time is so rayfire loads)
			SET_ENTITY_COORDS(vehCar, <<-148.7664, -1149.9998, 23.1124>>)
			SET_ENTITY_ROTATION(vehCar, <<3.3850, 2.9552, -88.1208>>)
			
			//Clear a path
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(vehCar)
			
			CLEAR_AREA(vPlayerCoords, 15.0, TRUE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<vPlayerCoords.X, vPlayerCoords.Y, 30.0>>, <<-55.8188, -1096.4149, 20.0>>, 20.0)
			
			SET_ROADS_IN_ANGLED_AREA(<<-57.644516, -1097.634033, 35.422352>>, <<-145.066711, -1156.563843, 23.026037>>, 20.0, FALSE, FALSE)
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-56.13, -1097.60, 25.16>>) - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10.0)
			
			//Fade In
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ELSE
		IF TIMERA() < 100
			SET_VEHICLE_BRAKE_LIGHTS(vehCar, FALSE)
		ENDIF
		
		PRINT_ADV("ARM3_RAM1", 4000)
		
		//Request Cutscene Variations - Armenian_3_mcs_8
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Siemon", pedOwner)
		ENDIF
		
//		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
//			NEW_LOAD_SCENE_START_SPHERE(<<-56.313320, -1096.538574, 27.307253>>, 25.0)
//		ENDIF
		
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1) != RFMO_STATE_PRIMED
				IF NOT HAS_LABEL_BEEN_TRIGGERED("rfShowroomCrash1")
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1, RFMO_STATE_PRIMING)
					SET_LABEL_AS_TRIGGERED("rfShowroomCrash1", TRUE)
				ENDIF
			ELSE
				SET_LABEL_AS_TRIGGERED("rfShowroomCrash1", FALSE)
			ENDIF
		ELSE
			rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
			
			PRINTLN("Getting Rayfire Map Object - DES_Showroom")
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objGlass)
			objGlass = CREATE_OBJECT_NO_OFFSET(PROP_SHOWROOM_GLASS_1B, <<-59.8700, -1098.8400, 27.20>>)	//objGlass = GET_CLOSEST_OBJECT_OF_TYPE(<<-59.80, -1098.78, 24.92>>, 1.0, PROP_SHOWROOM_GLASS_1B)
			SET_ENTITY_ROTATION(objGlass, <<0.0, 0.0, 121.5>>)
			FREEZE_ENTITY_POSITION(objGlass, TRUE)
		ENDIF
		
		IF intGarage <> NULL
			SET_INTERIOR_IN_USE(intGarage)
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-60.15, -1098.69, 25.44>>) < 100.0
		AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_GAME_TIMER() > iDialogueTimer
				IF iDialogueStage = 0
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_RAM", "ARM3_RAM_1")
					iDialogueStage++
				ELIF iDialogueStage = 1
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_RAM", "ARM3_RAM_2")
					iDialogueStage++
				ELIF iDialogueStage = 2
					PLAY_SINGLE_LINE_FROM_CONVERSATION_ADV("ARM3_RAM", "ARM3_RAM_3")
					iDialogueStage++
				ENDIF
				
				iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(20000, 25000)
			ENDIF
		ENDIF
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
			IF NOT DOES_BLIP_EXIST(blipDestination)
				CLEAR_PRINTS()
				PRINT_ADV("ARM3_RAM1", DEFAULT_GOD_TEXT_TIME, FALSE)
				SAFE_REMOVE_BLIP(blipCar)
				SAFE_ADD_BLIP_LOCATION(blipDestination, vDealership, FALSE)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipCar)
				CLEAR_PRINTS()
				PRINT_ADV("ARM3_CAR2")
				SAFE_REMOVE_BLIP(blipDestination)
				SAFE_ADD_BLIP_VEHICLE(blipCar, vehCar, FALSE)
			ENDIF
		ENDIF
		
//		VECTOR vVehicleRotation = GET_ENTITY_ROTATION_VELOCITY(vehCar)
//		VECTOR vVehicleForwardVector = GET_ENTITY_FORWARD_VECTOR(vehCar)
//		VECTOR vVehicleVelocity = GET_ENTITY_VELOCITY(vehCar)
//		vVehicleVelocity = NORMALISE_VECTOR(vVehicleVelocity)
		
		fDistLastFrame = fDistThisFrame
		fDistThisFrame = GET_DISTANCE_BETWEEN_COORDS(<<-56.71602, -1097.16968, 25.42230>>, GET_ENTITY_COORDS(vehCar))
		
		IF GET_ENTITY_SPEED(vehCar) > 5.0
			SET_ENTITY_COLLISION(objGlass, FALSE)
		ELSE
			SET_ENTITY_COLLISION(objGlass, TRUE)
		ENDIF
		
		IF ((IS_POINT_IN_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<-1.0, 2.2, -0.25>>), <<-59.764771, -1098.798828, 25.422319>>, <<-53.614487, -1095.078979, 28.922344>>, 5.25)
		OR IS_POINT_IN_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<1.0, 2.2, -0.25>>), <<-59.764771, -1098.798828, 25.422319>>, <<-53.614487, -1095.078979, 28.922344>>, 5.25)
		OR IS_POINT_IN_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<-1.0, -2.3, -0.25>>), <<-59.764771, -1098.798828, 25.422319>>, <<-53.614487, -1095.078979, 28.922344>>, 5.25)
		OR IS_POINT_IN_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<1.0, -2.3, -0.25>>), <<-59.764771, -1098.798828, 25.422319>>, <<-53.614487, -1095.078979, 28.922344>>, 5.25))
		AND GET_ENTITY_SPEED(vehCar) > 5.0
		AND fDistThisFrame < fDistLastFrame)
		OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-32.008007, -1102.252197, 25.434353>>, <<-59.300301, -1092.674316, 29.434353>>, 15.0)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-61.387348, -1103.479004, 24.859724>>, <<-58.618919, -1095.799438, 29.434353>>, 10.0))
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
				IF SAFE_START_CUTSCENE(10.0)
					//LOD
					SET_BUILDING_STATE(BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD, BUILDINGSTATE_DESTROYED, FALSE)
					
					//Audio
					PLAY_AUDIO(ARM3_HIT)
					
					//Rayfire
					IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
						IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1) = RFMO_STATE_PRIMED
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1, RFMO_STATE_START_ANIM)	#IF IS_DEBUG_BUILD	PRINTLN("Start anim on rayfire object rfShowroomCrash1")	#ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD	ELSE
						PRINTLN("Did not find rayfire object rfShowroomCrash1")	#ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(objGlass)
						SET_ENTITY_COORDS(objGlass, GET_ENTITY_COORDS(objGlass) - <<0.0, 0.0, 10.0>>)
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("WindowSmashed")
						SET_PORTAL_SETTINGS_OVERRIDE("V_CARSHOWROOM_PS_WINDOW_UNBROKEN", "V_CARSHOWROOM_PS_WINDOW_BROKEN")
						
						SET_LABEL_AS_TRIGGERED("WindowSmashed", TRUE)
					ENDIF
					
					//Sound
					PLAY_SOUND_FRONTEND(-1, "ARM_3_CAR_GLASS_CRASH")
					
					//Vibrate
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
					
					INT iTimeOut = GET_GAME_TIMER() + 500
					
					WHILE NOT IS_ENTITY_IN_ANGLED_AREA(vehCar, <<-54.038837, -1098.502319, 25.422327>>, <<-57.813641, -1091.971191, 28.422327>>, 4.0)
					AND iTimeOut > GET_GAME_TIMER()
					AND GET_ENTITY_SPEED(vehCar) > 1.0
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
						
						PRINTLN("Timeout: ", GET_GAME_TIMER(), "<", iTimeOut)
						
						IF iTimeOut < GET_GAME_TIMER()
							PRINTLN("Timed out Rayfire...")
						ENDIF
						
						//Bonnet Camera
						DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
						
						WAIT_WITH_DEATH_CHECKS(0)
					ENDWHILE
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					ADVANCE_STAGE()
				ENDIF
			ENDIF
		ELSE
			//Cinematic Cam
			CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, <<-55.2, -1095.8, 27.3>>, "ARM3HLP_WINDOW", HINTTYPE_VEHICLE_HIGH_ZOOM)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			
			BOOL bInputNextCameraPressed
			
			bInputNextCameraPressed = FALSE
			
			IF bCameraViewToggle
				IF NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				AND NOT IS_CUSTOM_MENU_ON_SCREEN() AND NOT IS_GAMEPLAY_HINT_ACTIVE()
					bInputNextCameraPressed = TRUE
					
					bCameraViewToggle = FALSE
				ENDIF
				
				IF IS_CUSTOM_MENU_ON_SCREEN() OR IS_GAMEPLAY_HINT_ACTIVE()
					bCameraViewToggle = FALSE
				ENDIF
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				bCameraViewToggle = TRUE
			ENDIF
			
			IF bInputNextCameraPressed
				PRINTLN("DISABLED_CONTROL_JUST_PRESSED(INPUT_NEXT_CAMERA)")
				PRINTLN("vehicleZoomLevel=", vehicleZoomLevel)
				PRINTLN("bVehicleZoomInOut=", bVehicleZoomInOut)
				PRINTLN("------------------------------")
				IF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_BONNET
					SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
				ELIF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_NEAR
					SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
				ELIF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_MEDIUM
					IF bVehicleZoomInOut
						SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_FAR)
					ELSE
						SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_NEAR)
					ENDIF
					
					bVehicleZoomInOut = !bVehicleZoomInOut
				ELIF vehicleZoomLevel = VEHICLE_ZOOM_LEVEL_FAR
					SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(VEHICLE_ZOOM_LEVEL_MEDIUM)
				ENDIF
				vehicleZoomLevel = GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()
				PRINTLN("vehicleZoomLevel=", vehicleZoomLevel)
				PRINTLN("bVehicleZoomInOut=", bVehicleZoomInOut)
				PRINTLN("------------------------------")
			ENDIF
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
			//Audio Scene
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_RAM_DEALERSHIP")
				STOP_AUDIO_SCENE("ARM_3_RAM_DEALERSHIP")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_WINDOW_FOCUS_CAM")
				START_AUDIO_SCENE("ARM_3_WINDOW_FOCUS_CAM")
			ENDIF
		ELSE
			//Audio Scene
			IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_RAM_DEALERSHIP")
				START_AUDIO_SCENE("ARM_3_RAM_DEALERSHIP")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("ARM_3_WINDOW_FOCUS_CAM")
				STOP_AUDIO_SCENE("ARM_3_WINDOW_FOCUS_CAM")
			ENDIF
		ENDIF
		
		//Disable Player Movement
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
		
		//Bonnet Camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		
		//Fail Timer
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-52.025787, -1095.310791, 32.230316>>, <<28.0, 28.0, 8.0>>)
			IF GET_GAME_TIMER() > iFailTimer
			OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-60.15, -1098.69, 25.44>>) > 200.0
				eMissionFail = failLeftRoute
				
				missionFailed()
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehCar)
		AND NOT IS_ENTITY_DEAD(vehCar)
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			AND NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_SITTING_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehCar)
					eMissionFail = failCarDestroyed
					
					missionFailed()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_RAM_DEALERSHIP")
			STOP_AUDIO_SCENE("ARM_3_RAM_DEALERSHIP")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_WINDOW_FOCUS_CAM")
			STOP_AUDIO_SCENE("ARM_3_WINDOW_FOCUS_CAM")
		ENDIF
		
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		
		IF DOES_CAM_EXIST(camCinematic)
			SET_CAM_ACTIVE(camCinematic, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		
		//Camera
		IF NOT DOES_CAM_EXIST(camMain)
			camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		ENDIF
		
		IF DOES_CAM_EXIST(camMain)
			SET_CAM_ACTIVE(camMain, TRUE)
		ENDIF
		
		SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
		
		CLEAR_TEXT()
		
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
		ENDIF
		
		//Anim Clip Set
		REMOVE_CLIP_SET("clipset@missarmenian3@franklin_driving")
		
		SAFE_REMOVE_BLIP(blipCar)
		SAFE_REMOVE_BLIP(blipDestination)
		
		SAFE_DELETE_OBJECT(objGlass)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

PROC CutsceneWindowSmash()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayFight), "stageBeatDown", TRUE)
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_PREVENT_EVERYBODY_BACKOFF)
		
		bRadar = FALSE
		
		//Attached Gun
		IF NOT DOES_ENTITY_EXIST(objGun)
			objGun = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL), vMichaelStart)
		ENDIF
		ATTACH_ENTITY_TO_ENTITY(objGun, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
		
		//Ped Owner
		SAFE_DELETE_PED(pedOwner)
		WHILE NOT CREATE_NPC_PED_ON_FOOT(pedOwner, CHAR_SIMEON, <<-56.4360, -1098.8176, 25.4345>>, 31.1490)
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		SET_PED_SUFFERS_CRITICAL_HITS(pedOwner, FALSE)
		SET_ENTITY_HEALTH(pedOwner, 200 + 200)
		SET_PED_MAX_HEALTH(pedOwner, 200 + 200)
		TASK_LOOK_AT_ENTITY(pedOwner, vehCar, 3000)
		SET_ENTITY_INVINCIBLE(pedOwner, TRUE)
		SAFE_SET_ENTITY_VISIBLE(pedOwner, FALSE)
		
		//Cleanup Owner
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_SIMEON))
		
		ADD_PED_FOR_DIALOGUE(sPedsForConversation, 8, pedOwner, "SIMEON")
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("WindowSmashed")
			SET_PORTAL_SETTINGS_OVERRIDE("V_CARSHOWROOM_PS_WINDOW_UNBROKEN", "V_CARSHOWROOM_PS_WINDOW_BROKEN")
			
			SET_LABEL_AS_TRIGGERED("WindowSmashed", TRUE)
		ENDIF
		
		//Car
		SET_VEHICLE_DAMAGE(vehCar, <<-0.84, 2.21, 0.22>>, 100.0, 400.0, TRUE)
		SET_VEHICLE_DAMAGE(vehCar, <<0.67, 2.12, -0.06>>, 100.0, 400.0, TRUE)
		SET_VEHICLE_DAMAGE(vehCar, <<0.05, 1.97, 0.2>>, 100.0, 400.0, TRUE)
		
		SET_ENTITY_INVINCIBLE(vehCar, TRUE)
		
		//Cutscene
		REQUEST_CUTSCENE("Armenian_3_mcs_8", CUTSCENE_REQUESTED_FROM_Z_SKIP)
		
		REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		//Bonnet Camera
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		
		IF SKIPPED_STAGE()
			//Set Positions
			SET_PED_POSITION(PLAYER_PED_ID(), <<-56.13, -1097.60, 25.16>>, 0.0)
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-56.13, -1097.60, 25.16>>) - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			
			//Rayfire
			rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
			
			WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
				rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
				
				PRINTLN("Getting Rayfire Map Object - DES_Showroom")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1) != RFMO_STATE_END
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1, RFMO_STATE_ENDING)
				ENDIF
			ENDIF
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			//Audio
			PLAY_AUDIO(ARM3_RESTART_8)
			
			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
		ENDIF
	ELSE
		//Request Cutscene Variations - Armenian_3_mcs_8
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED(CHAR_FRANKLIN))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Siemon", pedOwner)
		ENDIF
		
//		BOOL bShapeTest
//		SHAPETEST_INDEX shapeTest
//		SHAPETEST_STATUS shapeTestStatus
//		INT bShapeTestHitSomething
//		VECTOR vShapeTestPos
//		VECTOR vShapeTestNormal
//		ENTITY_INDEX shapeTestEntityIndex
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Complications")
		
		SWITCH iCutsceneStage
			CASE 0
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
//				BOOL bReposition
//				
//				shapeTest = START_SHAPE_TEST_CAPSULE(GET_ENTITY_COORDS(vehCar), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, <<0.0, 2.5, 0.0>>), 0.5, DEFAULT, vehCar)
//				PRINTLN("SHAPETEST[STARTING]")
//				
//				WHILE NOT bShapeTest
//					shapeTestStatus = GET_SHAPE_TEST_RESULT(shapeTest, bShapeTestHitSomething, vShapeTestPos, vShapeTestNormal, shapeTestEntityIndex)
//					PRINTLN("SHAPETEST[RUNNING]")
//					IF shapeTestStatus != SHAPETEST_STATUS_RESULTS_NOTREADY
//						IF shapeTestStatus = SHAPETEST_STATUS_RESULTS_READY
//							PRINTLN("SHAPETEST[SHAPETEST_STATUS_RESULTS_READY]")
//						ELIF shapeTestStatus = SHAPETEST_STATUS_NONEXISTENT
//							PRINTLN("SHAPETEST[SHAPETEST_STATUS_NONEXISTENT]")
//						ENDIF
//						PRINTLN("SHAPETEST[HitSomething=", bShapeTestHitSomething, "]")
//						PRINTLN("SHAPETEST[shapeTestEntityIndex=", DOES_ENTITY_EXIST(shapeTestEntityIndex), "]")
//						IF bShapeTestHitSomething = 1
//						OR DOES_ENTITY_EXIST(shapeTestEntityIndex)
//							PRINTLN("SHAPETEST[TRUE]")
//							bReposition = TRUE
//						ENDIF
//						
//						bShapeTest = !bShapeTest
//					ENDIF
//					
//					WAIT_WITH_DEATH_CHECKS(0)
//				ENDWHILE
//				
//				IF bReposition
					SET_ENTITY_COORDS_NO_OFFSET(vehCar, <<-57.3905, -1097.3470, 26.5800>> + <<2.08, 1.23, 0.0>>)
					SET_ENTITY_ROTATION(vehCar, <<-8.8082, 0.1836, -58.8950>>)
//				ENDIF
				
				//Michael Available
				SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
				Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
				ENDIF
				
				SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
				
				VECTOR scenePosition
				VECTOR sceneRotation
				CAMERA_INDEX sceneCamera
				
				scenePosition = VECTOR_ZERO	//<<-15.361, -1451.384, 31.547>>
				sceneRotation = VECTOR_ZERO	//<<0.0, 0.0, 131.278>>
				sceneCamera = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				sceneLeadInMCS8 = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
				
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneLeadInMCS8, vehCar, 0)
				
				//TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneLeadInMCS8, sAnimDictLeadInMCS8, "_leadin_mic", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				CLEAR_PED_TASKS(PLAYER_PED(CHAR_MICHAEL))
				SET_PED_INTO_VEHICLE(PLAYER_PED(CHAR_MICHAEL), vehCar, VS_BACK_RIGHT)
				TASK_PLAY_ANIM(PLAYER_PED(CHAR_MICHAEL), sAnimDictLeadInMCS8, "_leadin_mic", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				PLAY_FACIAL_ANIM(PLAYER_PED(CHAR_MICHAEL), "_leadin_Mic_facial", sAnimDictLeadInMCS8)
				
				//TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], sceneLeadInMCS8, sAnimDictLeadInMCS8, "_leadin_fra", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				CLEAR_PED_TASKS(PLAYER_PED(CHAR_FRANKLIN))
				CLEAR_PED_TASKS(PLAYER_PED(CHAR_FRANKLIN))
				TASK_PLAY_ANIM(PLAYER_PED(CHAR_FRANKLIN), sAnimDictLeadInMCS8, "_leadin_fra", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				PLAY_FACIAL_ANIM(PLAYER_PED(CHAR_FRANKLIN), "_leadin_Fra_facial", sAnimDictLeadInMCS8)
				
				PLAY_SYNCHRONIZED_CAM_ANIM(sceneCamera, sceneLeadInMCS8, "_leadin_cam", sAnimDictLeadInMCS8)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				CREATE_CONVERSATION_ADV("ARM3_SMASH1")
				
				//Fade In
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				ADVANCE_CUTSCENE()
			BREAK
			CASE 1
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar, 1.0, 1, 0.0)
				
				SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				IF GET_ENTITY_UPRIGHT_VALUE(vehCar) < 0.1
					APPLY_FORCE_TO_ENTITY(vehCar, APPLY_TYPE_ANGULAR_IMPULSE, -GET_ENTITY_ROTATION_VELOCITY(vehCar), VECTOR_ZERO, 0, TRUE, FALSE, FALSE)
				ENDIF
				
				IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeadInMCS8)
				OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeadInMCS8)
				AND (GET_SYNCHRONIZED_SCENE_PHASE(sceneLeadInMCS8) >= 0.7)))
				AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0, REPLAY_IMPORTANCE_HIGH)
					
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					//REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(pedOwner, "Siemon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Jimmys_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					
					SET_VEHICLE_DOOR_CONTROL(vehCar, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					START_CUTSCENE(CUTSCENE_PLAYER_TARGETABLE)
					
					WAIT_WITH_DEATH_CHECKS(0)
					
					SET_VEHICLE_ENGINE_ON(vehCar, FALSE, TRUE)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SAFE_SET_ENTITY_VISIBLE(pedOwner, TRUE)
					
					CLEAR_AREA(<<-36.6411, -1102.1914, 26.3443>>, 30.0, TRUE)
					
					IF DOES_ENTITY_EXIST(vehGarage[0]) AND NOT IS_ENTITY_DEAD(vehGarage[0])
						SET_VEHICLE_POSITION(vehGarage[0], <<-36.6411, -1102.1914, 26.3443>>, 154.2468)
					ENDIF
					IF DOES_ENTITY_EXIST(vehGarage[1]) AND NOT IS_ENTITY_DEAD(vehGarage[1])
						SET_VEHICLE_POSITION(vehGarage[1], <<-41.7113, -1100.0415, 26.0671>>, 138.7067)
					ENDIF
					IF DOES_ENTITY_EXIST(vehGarage[2]) AND NOT IS_ENTITY_DEAD(vehGarage[2])
						SET_VEHICLE_POSITION(vehGarage[2], <<-46.3951, -1097.7783, 26.3222>>, 108.3411)
					ENDIF
					IF DOES_ENTITY_EXIST(vehGarage[3]) AND NOT IS_ENTITY_DEAD(vehGarage[3])
						SET_VEHICLE_POSITION(vehGarage[3], <<-50.0989, -1094.5341, 26.0671>>, 88.9621)
					ENDIF
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					SAFE_DELETE_OBJECT(objGun)
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 2
				//Hide Peds
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
//				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
//					
//					IF NOT WAS_CUTSCENE_SKIPPED()
//						IF NOT DOES_CAM_EXIST(camMain)
//							camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//						ENDIF
//						
//						SET_CAM_PARAMS(camMain, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
//						
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					ENDIF
//				ENDIF
				
				IF DOES_ENTITY_EXIST(vehGarage[3])
				AND NOT IS_ENTITY_DEAD(vehGarage[3])
					SET_FORCE_HD_VEHICLE(vehGarage[3], TRUE)
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SwitchFX[Armenian_3_mcs_8]")
					IF GET_CUTSCENE_TIME() > ROUND(12.866668 * 1000.0)
					AND HAS_CUTSCENE_CUT_THIS_FRAME()
						ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						
						SET_LABEL_AS_TRIGGERED("SwitchFX[Armenian_3_mcs_8]", TRUE)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Siemon", IG_SIEMONYETARIAN)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupEnemy)
					SET_PED_AS_ENEMY(pedOwner, TRUE)
//					SET_PED_RESET_FLAG(pedOwner, PRF_InstantBlendToAim, TRUE)
//					TASK_COMBAT_PED(pedOwner, PLAYER_PED_ID())
					TASK_PUT_PED_DIRECTLY_INTO_MELEE(pedOwner, PLAYER_PED(CHAR_MICHAEL), 0.5, -1.0, 0.0)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedOwner)
					SET_ENTITY_INVINCIBLE(pedOwner, FALSE)
					PRINTLN("TASK_PUT_PED_DIRECTLY_INTO_MELEE")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmys_Car")
					SET_VEHICLE_DOORS_SHUT(vehCar)
					//Commented out for bug 1757182  FREEZE_ENTITY_POSITION(vehCar, TRUE)
					
//					SET_LABEL_AS_TRIGGERED("WheelCompression[vehCar](Armenian_3_mcs_8)", TRUE)
				ENDIF
				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("WheelCompression[vehCar](Armenian_3_mcs_8)")
//					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehCar)
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					CLEAR_AREA_OF_COPS(<<-57.6017, -1095.0913, 25.4343>>, 500.0)
					
					ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
					SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
					SET_CREATE_RANDOM_COPS(FALSE)
					
					//SET_PED_POSITION(PLAYER_PED_ID(), <<-57.6017, -1095.0913, 25.4343>>, 325.8112)
					
//					TASK_COMBAT_PED(PLAYER_PED_ID(), pedOwner)
					
					TASK_PUT_PED_DIRECTLY_INTO_MELEE(PLAYER_PED(CHAR_MICHAEL), pedOwner, 0.0, -1.0, 0.0)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
					
					IF bZoomLevel
						SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(vehicleZoomLevel)
						
						SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, camViewModeOnFoot)
						
						PRINTLN("vehicleZoomLevel = ", ENUM_TO_INT(vehicleZoomLevel))
						PRINTLN("camViewModeOnFoot = ", ENUM_TO_INT(camViewModeOnFoot))
						
						bZoomLevel = FALSE
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
//					IF NOT WAS_CUTSCENE_SKIPPED()
//						RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
//					ELSE
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					ENDIF
//					
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
					
					IF bZoomLevel
						SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(vehicleZoomLevel)
						
						SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, camViewModeOnFoot)
						
						PRINTLN("vehicleZoomLevel = ", ENUM_TO_INT(vehicleZoomLevel))
						PRINTLN("camViewModeOnFoot = ", ENUM_TO_INT(camViewModeOnFoot))
						
						bZoomLevel = FALSE
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					
					PRINT_ADV("ARM3_BEAT")
					PRINT_HELP_ADV("ARM3HLP_LOCKON")
					
					ADVANCE_CUTSCENE()
				ENDIF
			BREAK
			CASE 3
				IF IS_GAMEPLAY_CAM_RENDERING()
				AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		//Bonnet Camera
		IF iCutsceneStage < 2
			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		ENDIF
		
		SET_PED_RESET_FLAG(pedOwner, PRF_SuspendInitiatedMeleeActions, TRUE)
		SET_PED_RESET_FLAG(pedOwner, PRF_ForcePedToStrafe, TRUE)
		SET_PED_COMBAT_MOVEMENT(pedOwner, CM_STATIONARY)
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	ENDIF
	
	IF CLEANUP_STAGE()
	
		REPLAY_STOP_EVENT()
	
		//Cleanup (Blips, peds, variables etc.)
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
			SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehCar)
		ENDIF
		
		SET_ENTITY_INVINCIBLE(vehCar, FALSE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = TRUE
			SET_VEHICLE_POSITION(vehCar, <<-55.7623, -1096.5033, 25.4766>>, 309.4087)
		ENDIF
		#ENDIF
		
		SAFE_REMOVE_BLIP(blipOwner)
		
		SAFE_DELETE_OBJECT(objGun)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

BOOL bUnreachable = FALSE

PROC BeatDown()
	IF INIT_STAGE()
		//Checkpoint
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(ENUM_TO_INT(replayFight), "stageBeatDown", TRUE)
		
		//Michael Available
		SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
		Set_Mission_Flow_Flag_State(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
		
		//Taxi
		DISABLE_TAXI_HAILING(TRUE)
		
		//Player Control
		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		IF bZoomLevel
			SET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL(vehicleZoomLevel)
			
			SET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT, camViewModeOnFoot)
			
			PRINTLN("vehicleZoomLevel = ", ENUM_TO_INT(vehicleZoomLevel))
			PRINTLN("camViewModeOnFoot = ", ENUM_TO_INT(camViewModeOnFoot))
			
			bZoomLevel = FALSE
		ENDIF
		
		//Radar
		bRadar = TRUE
		
		ALLOW_SONAR_BLIPS(FALSE)
		
		//Phone
		DISABLE_CELLPHONE(TRUE)
		
		IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
			SET_VEHICLE_DOORS_LOCKED(vehCar, VEHICLELOCK_UNLOCKED)
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
		ENDIF
		
		WAIT_WITH_DEATH_CHECKS(0)
		
		SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())	PRINTLN("SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())")
		
		//Action Mode
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
		
		RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
		
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE) //REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
		
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
		
		SAFE_DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		
		SET_PED_COMBAT_MOVEMENT(pedOwner, CM_WILLADVANCE)
		
		SAFE_SET_ENTITY_VISIBLE(pedOwner, TRUE)
		
		#IF IS_DEBUG_BUILD
		IF bAutoSkipping = FALSE
		#ENDIF
		
		REMOVE_ALL_PED_WEAPONS(pedOwner)
		
		IF NOT IS_ENTITY_DEAD(pedOwner)
			SET_ENTITY_INVINCIBLE(pedOwner, FALSE)
		ENDIF
		
		SAFE_ADD_BLIP_PED(blipOwner, pedOwner)
		
		//Cutscene
		REQUEST_CUTSCENE("Armenian_3_MCS_9_concat")
		
		//Audio Scene
		IF NOT IS_AUDIO_SCENE_ACTIVE("ARM_3_BEAT_DOWN")
			START_AUDIO_SCENE("ARM_3_BEAT_DOWN")
		ENDIF
		
		//Car Door Slam
//		bCarDoorSlam = FALSE
//		iCarDoorSlam = 0
		
		IF iNavBlock = -1
			iNavBlock = ADD_NAVMESH_BLOCKING_OBJECT(<<-53.737026, -1096.983765, 26.494654>>, <<1.25, 0.5, 1.0>>, 30.0)
		ENDIF
		
		RECORD_BROKEN_GLASS(<<-57.15079, -1097.26355, 25.42232>>, 3.0)
		RECORD_BROKEN_GLASS(<<-51.97326, -1093.50977, 25.42232>>, 3.5)
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("WindowSmashed")
			SET_PORTAL_SETTINGS_OVERRIDE("V_CARSHOWROOM_PS_WINDOW_UNBROKEN", "V_CARSHOWROOM_PS_WINDOW_BROKEN")
			
			SET_LABEL_AS_TRIGGERED("WindowSmashed", TRUE)
		ENDIF
		
		//Wanted
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		
		CLEAR_AREA_OF_COPS(<<-57.6017, -1095.0913, 25.4343>>, 500.0)
		
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
		SET_CREATE_RANDOM_COPS(FALSE)
		
		SET_PED_RESET_FLAG(pedOwner, PRF_SuspendInitiatedMeleeActions, TRUE)
		SET_PED_RESET_FLAG(pedOwner, PRF_ForcePedToStrafe, TRUE)
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
		
		bUnreachable = FALSE
		
		IF SKIPPED_STAGE()
			//Freeze
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(pedOwner)
				FREEZE_ENTITY_POSITION(pedOwner, TRUE)
			ENDIF
			
			//Pin Interior
			IF bPinnedGarage = FALSE
				intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
				
				PIN_INTERIOR_IN_MEMORY(intGarage)
				
				WHILE NOT IS_INTERIOR_READY(intGarage)
					PRINTLN("PINNING INTERIOR...")
					
					WAIT_WITH_DEATH_CHECKS(0)
				ENDWHILE
				
				bPinnedGarage = TRUE
			ENDIF
			
			//Load Scene
			IF NOT bReplaySkip
				LOAD_SCENE_ADV(<<-58.1243, -1095.0555, 25.4345>>)
			ENDIF
			
			//Unfreeze
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(pedOwner)
				FREEZE_ENTITY_POSITION(pedOwner, FALSE)
			ENDIF
			
			//Set Positions
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SET_PED_POSITION(PLAYER_PED_ID(), <<-58.1243, -1095.0555, 25.4345>>, 314.0250)
			
			SET_VEHICLE_POSITION(vehCar, <<-55.8188, -1096.4149, 25.4344>>, 305.0423)
			FREEZE_ENTITY_POSITION(vehCar, TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-56.13, -1097.60, 25.16>>) - GET_ENTITY_HEADING(PLAYER_PED_ID()))
			
			//Rayfire
			rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
			
			WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
				rfShowroomCrash1 = GET_RAYFIRE_MAP_OBJECT(<<-56.13, -1097.60, 25.16>>, 10.0, "DES_Showroom")
				
				PRINTLN("Getting Rayfire Map Object - DES_Showroom")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfShowroomCrash1)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1) != RFMO_STATE_END
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfShowroomCrash1, RFMO_STATE_ENDING)
				ENDIF
			ENDIF
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED(CHAR_MICHAEL))
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			WAIT_WITH_DEATH_CHECKS(500)
			
			//Owner
			SET_PED_POSITION(pedOwner, <<-55.6653, -1093.4874, 25.4343>>, 125.8820)
			RETAIN_ENTITY_IN_INTERIOR(pedOwner, intGarage)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedOwner, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupEnemy)
			SET_PED_AS_ENEMY(pedOwner, TRUE)
			TASK_PUT_PED_DIRECTLY_INTO_MELEE(pedOwner, PLAYER_PED(CHAR_MICHAEL), 0.0, -1.0, 0.0)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedOwner)
			SET_ENTITY_INVINCIBLE(pedOwner, FALSE)
			SET_ENTITY_HEALTH(pedOwner, 250 + 200)
			SET_PED_MAX_HEALTH(pedOwner, 250 + 200)
			
			//Player
			SET_PED_POSITION(PLAYER_PED_ID(), <<-58.1243, -1095.0555, 25.4345>>, 314.0250)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			TASK_PUT_PED_DIRECTLY_INTO_MELEE(PLAYER_PED(CHAR_MICHAEL), pedOwner, 0.0, -1.0, 0.0)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED(CHAR_MICHAEL))
			
			//Camera Behind Player
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0)
			
			//Audio
			PLAY_AUDIO(ARM3_RESTART_8)
			
			WAIT_WITH_DEATH_CHECKS(500)
			
			//Fade In
			IF NOT bShitSkip
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINT_ADV("ARM3_BEAT")
		
		//Request Cutscene Variations - Armenian_3_MCS_9_concat
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED(CHAR_MICHAEL))
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Siemon", pedOwner)
		ENDIF
		
		SWITCH iCutsceneStage
			CASE 0
				IF NOT IS_CUTSCENE_PLAYING()
				OR bShitSkip = TRUE
					//Stats
					IF IS_PED_PERFORMING_A_COUNTER_ATTACK(PLAYER_PED_ID())	//IS_PED_PERFORMING_A_BLOCK(PLAYER_PED_ID())
						IF NOT HAS_LABEL_BEEN_TRIGGERED("Dodged")
							INFORM_MISSION_STATS_OF_INCREMENT(ARM3_COUNTERS)
							
							SET_LABEL_AS_TRIGGERED("Dodged", TRUE)
						ENDIF
					ELIF HAS_LABEL_BEEN_TRIGGERED("Dodged")
						SET_LABEL_AS_TRIGGERED("Dodged", FALSE)
					ENDIF
					
					IF NOT bUnreachable
						IF GET_PED_RESET_FLAG(pedOwner, PRF_IsMeleeTargetUnreachable)
							PRINTLN("GET_PED_RESET_FLAG(pedOwner, PRF_IsMeleeTargetUnreachable) = ", GET_PED_RESET_FLAG(pedOwner, PRF_IsMeleeTargetUnreachable))
							
							bUnreachable = TRUE
						ENDIF
					ELSE
						VECTOR vPlayer, vOwner
						
						vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
						vOwner = GET_ENTITY_COORDS(pedOwner)
						
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayer, vOwner) < 3.5
						AND vPlayer.Z > vOwner.Z - 0.15 AND vPlayer.Z < vOwner.Z + 0.15
						AND NOT IS_PED_ON_VEHICLE(PLAYER_PED_ID())
							bUnreachable = FALSE
						ENDIF
					ENDIF
					
					IF ((IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-32.008007, -1102.252197, 25.434353>>, <<-59.300301, -1092.674316, 29.434353>>, 15.0)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-34.026970, -1115.378784, 25.422327>>, <<-29.336802, -1102.294678, 28.922327>>, 5.5)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-40.934101, -1086.291992, 25.422327>>, <<-25.776924, -1091.713013, 28.921900>>, 10.5)
					OR (intGarage != NULL AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intGarage))
					AND NOT bUnreachable)
					OR bShitSkip = TRUE
						PRINTLN("POINT -")
						
						SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupEnemy)
						
						SAFE_REMOVE_BLIP(blipDestination)
						SAFE_ADD_BLIP_PED(blipOwner, pedOwner)
						
						IF IS_THIS_PRINT_BEING_DISPLAYED("ARM3_BEATBACK")
							CLEAR_PRINTS()
						ENDIF
						
						IF GET_PED_RELATIONSHIP_GROUP_HASH(pedOwner) = relGroupEnemy
							IF GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE) <> PERFORMING_TASK
								CLEAR_PED_TASKS(pedOwner)
								CLEAR_PED_SECONDARY_TASK(pedOwner)
								TASK_COMBAT_PED(pedOwner, PLAYER_PED(CHAR_MICHAEL))
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_LOCKON")
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP_ADV("ARM3HLP_LOCKON")
							ENDIF
						ELIF (NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT") AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT_KM"))
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARM3HLP_LOCKON")
							AND GET_GAME_TIMER() > iHelpTimer
							AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedOwner)
								SAFE_CLEAR_HELP()	PRINTLN("SAFE_CLEAR_HELP()")
							ENDIF
							
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								
								IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
									PRINT_HELP_ADV("ARM3HLP_FIGHT_KM")
								ELSE
									PRINT_HELP_ADV("ARM3HLP_FIGHT")
								ENDIF
								
							ENDIF
						ELIF (NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT2") AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT2_KM"))
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
									PRINT_HELP_ADV("ARM3HLP_FIGHT2_KM")
								ELSE
									PRINT_HELP_ADV("ARM3HLP_FIGHT2")
								ENDIF
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FSTAT")
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP_ADV("ARM3HLP_FSTAT")
							ENDIF
						ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FSTAT2")
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP_ADV("ARM3HLP_FSTAT2")
							ENDIF
						ENDIF
						
						IF TIMERA() > 2000
						AND IS_SCREEN_FADED_IN()
							IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_SLAM")
									CREATE_CONVERSATION_ADV("ARM3_SLAM")
								ELSE
									IF GET_GAME_TIMER() > iDialogueTimer
										IF iDialogueLineCount[0] = -1 
											iDialogueLineCount[0] = 10
										ELIF iDialogueLineCount[1] = -1 
											iDialogueLineCount[1] = 5
										ENDIF
										
										IF iDialogueLineCount[iDialogueStage] > 0
											IF iDialogueStage = 0
												CREATE_CONVERSATION_ADV("ARM3_FIGHTSI", CONV_PRIORITY_MEDIUM, FALSE)
											ELIF iDialogueStage = 1
												CREATE_CONVERSATION_ADV("ARM3_SLAM2", CONV_PRIORITY_MEDIUM, FALSE)
											ENDIF
											
											iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 8000)
											
											iDialogueLineCount[iDialogueStage]--
										ENDIF
										
										IF iDialogueStage = 0
											iDialogueStage = 1
										ELIF iDialogueStage = 1
											iDialogueStage = 0
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF HAS_CUTSCENE_LOADED()	//If the cutscene hasn't loaded you can't beat up Simeon
							SET_ENTITY_INVINCIBLE(pedOwner, FALSE)
							
							SET_PED_RESET_FLAG(pedOwner, PRF_PreventAllMeleeTakedowns, TRUE)
							//SET_PED_RESET_FLAG(pedOwner, PRF_ForcePedToStrafe, TRUE)
							
							IF IS_PED_INJURED(pedOwner)
							OR GET_ENTITY_HEALTH(pedOwner) < 170
							OR bShitSkip = TRUE
								bShitSkip = FALSE
								
								//Audio
								PLAY_AUDIO(ARM3_HIT_STOP)
								
								//Audio Scene
								IF IS_AUDIO_SCENE_ACTIVE("ARM_3_BEAT_DOWN")
									STOP_AUDIO_SCENE("ARM_3_BEAT_DOWN")
								ENDIF
								
								REGISTER_ENTITY_FOR_CUTSCENE(pedOwner, "Siemon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_SIMEON), CEO_PRESERVE_FACE_BLOOD_DAMAGE | CEO_PRESERVE_BODY_BLOOD_DAMAGE)
								REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Jimmys_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
								
								START_CUTSCENE(CUTSCENE_DELAY_ENABLING_PLAYER_CONTROL_FOR_UP_TO_DATE_GAMEPLAY_CAMERA)
								
								WAIT_WITH_DEATH_CHECKS(0)
								
								CLEAR_PRINTS()
								//SAFE_CLEAR_HELP()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								
								SET_VEHICLE_DOOR_CONTROL(vehCar, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
								
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								CLEAR_PED_TASKS(pedOwner)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupBuddy)
								
								CLEAR_AREA(<<-58.1243, -1095.0555, 25.4345>>, 100.0, TRUE)
								
								SET_ENTITY_COLLISION(vehCar, FALSE)
								
								
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
								//Radar
								bRadar = FALSE
								
								//Action Mode
								SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
								
								IF IS_SCREEN_FADED_OUT()
									DO_SCREEN_FADE_IN(500)
								ENDIF
								
								ADVANCE_CUTSCENE()
							ENDIF
						ELSE
							SET_ENTITY_INVINCIBLE(pedOwner, TRUE)
						ENDIF
						
						SETTIMERB(0)
					ELSE
						PRINTLN("Player is avoiding Simeon...")
						
						SET_ENTITY_INVINCIBLE(pedOwner, TRUE)
						
						IF NOT (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-32.008007, -1102.252197, 25.434353>>, <<-59.300301, -1092.674316, 29.434353>>, 15.0)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-34.026970, -1115.378784, 25.422327>>, <<-29.336802, -1102.294678, 28.922327>>, 5.5)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-40.934101, -1086.291992, 25.422327>>, <<-25.776924, -1091.713013, 28.921900>>, 10.5)
						OR (intGarage != NULL AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intGarage))
							SAFE_REMOVE_BLIP(blipOwner)
							SAFE_ADD_BLIP_LOCATION(blipDestination, <<-58.1243, -1095.0555, 25.4345>>)
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_BEATBACK")
								CLEAR_TEXT()
							ENDIF
							
							PRINT_ADV("ARM3_BEATBACK")
						ENDIF
						
//						VECTOR vTemp1, vTemp2
//						FLOAT fHeadingOwnerToPlayer
//						vTemp1 = GET_ENTITY_COORDS(pedOwner)
//						vTemp2 = GET_ENTITY_COORDS(PLAYER_PED_ID())
//						fHeadingOwnerToPlayer = GET_ANGLE_BETWEEN_2D_VECTORS(vTemp1.X, vTemp1.Y, vTemp2.X, vTemp2.Y)
						
						IF GET_PED_RELATIONSHIP_GROUP_HASH(pedOwner) = relGroupEnemy
							PRINTLN("POINT 0")
							CLEAR_PED_TASKS(pedOwner)
							CLEAR_PED_SECONDARY_TASK(pedOwner)
							
							SET_PED_RELATIONSHIP_GROUP_HASH(pedOwner, relGroupBuddy)
						ENDIF
						
						IF NOT IS_ENTITY_IN_ANGLED_AREA(pedOwner, <<-58.611813, -1093.174194, 25.422327>>, <<-33.443916, -1102.318359, 29.422327>>, 12.5)
							IF GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								PRINTLN("POINT 1")
								OPEN_SEQUENCE_TASK(seqMain)
									IF IS_ENTITY_IN_ANGLED_AREA(pedOwner, <<-62.596004, -1096.740356, 25.352293>>, <<-59.575451, -1102.166016, 29.412632>>, 5.0)
										TASK_GO_STRAIGHT_TO_COORD(NULL, <<-59.0830, -1097.8198, 25.4225>>, PEDMOVE_RUN)
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-54.8776, -1092.9445, 25.4344>>, PEDMOVE_RUN)
									TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)))	//TASK_GO_STRAIGHT_TO_COORD(NULL, <<-54.8776, -1092.9445, 25.4344>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, fHeadingOwnerToPlayer)
									IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictAngry)
										TASK_PLAY_ANIM(NULL, sAnimDictAngry, "cmon", WALK_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY)
									ENDIF
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedOwner, seqMain)
								CLEAR_SEQUENCE_TASK(seqMain)
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								IF IS_PED_FACING_PED(pedOwner, PLAYER_PED_ID(), 20.0)
									IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictAngry)
										PRINTLN("POINT 2")
										IF (NOT IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "cmon")
										OR (IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "cmon") AND GET_ENTITY_ANIM_CURRENT_TIME(pedOwner, sAnimDictAngry, "cmon") >= 0.8))
										AND (NOT IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "areyounotman")
										OR (IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "areyounotman") AND GET_ENTITY_ANIM_CURRENT_TIME(pedOwner, sAnimDictAngry, "areyounotman") >= 0.8))
										AND (NOT IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "lookathim")
										OR (IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "lookathim") AND GET_ENTITY_ANIM_CURRENT_TIME(pedOwner, sAnimDictAngry, "lookathim") >= 0.8))
											INT iRandom
											
											iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
											
											IF iRandom = 0
												TASK_PLAY_ANIM(pedOwner, sAnimDictAngry, "cmon", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
												TASK_LOOK_AT_ENTITY(pedOwner, PLAYER_PED(CHAR_MICHAEL), -1, SLF_USE_TORSO | SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
											ELIF iRandom = 1
												TASK_PLAY_ANIM(pedOwner, sAnimDictAngry, "areyounotman", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
												TASK_LOOK_AT_ENTITY(pedOwner, PLAYER_PED(CHAR_MICHAEL), -1, SLF_USE_TORSO | SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
											ELIF iRandom = 2
												TASK_PLAY_ANIM(pedOwner, sAnimDictAngry, "lookathim", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
												TASK_LOOK_AT_ENTITY(pedOwner, PLAYER_PED(CHAR_MICHAEL), -1, SLF_USE_TORSO | SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_SCREEN_FADED_IN()
										IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
										AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF GET_GAME_TIMER() > iDialogueTimer
												IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedOwner)) < 25.0
													iDialogueStage = 1
													
													IF iDialogueLineCount[iDialogueStage] = -1 
														 iDialogueLineCount[iDialogueStage] = 5
													ENDIF
													
													IF iDialogueLineCount[iDialogueStage] > 0
														CREATE_CONVERSATION_ADV("ARM3_SIMS", CONV_PRIORITY_MEDIUM, FALSE)
														
														iDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 15000)
														
														iDialogueLineCount[iDialogueStage]--
													ENDIF
												ELSE
													IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_SIMS")
														KILL_FACE_TO_FACE_CONVERSATION()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									//IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedOwner, PLAYER_PED_ID())	//CAN_PED_SEE_HATED_PED(pedOwner, PLAYER_PED_ID())
									IF IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "cmon")
									OR IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "areyounotman")
									OR IS_ENTITY_PLAYING_ANIM(pedOwner, sAnimDictAngry, "lookathim")
										CLEAR_PED_TASKS(pedOwner)
									ENDIF
									PRINTLN("POINT 3")
									IF GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_TURN_PED_TO_FACE_COORD) != WAITING_TO_START_TASK
									AND GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_TURN_PED_TO_FACE_COORD) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedOwner, SCRIPT_TASK_TURN_PED_TO_FACE_COORD) != DORMANT_TASK
										TASK_TURN_PED_TO_FACE_COORD(pedOwner, GET_ENTITY_COORDS(PLAYER_PED(CHAR_MICHAEL)), 1000)
									ENDIF
									//ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("WantedLevel")
							IF TIMERB() > 20000
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
								
								SET_LABEL_AS_TRIGGERED("WantedLevel", TRUE)
							ENDIF
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedOwner)) > 100.0
							eMissionFail = failFledOwner
							
							missionFailed()
						ENDIF
					ENDIF
				ENDIF
				
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				
				SET_PLAYER_MAY_NOT_ENTER_ANY_VEHICLE(PLAYER_ID())
			BREAK
			CASE 1
				IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_LOCKON")
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP_ADV("ARM3HLP_LOCKON")
					ENDIF
				ELIF (NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT") AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT_KM"))
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP_ADV("ARM3HLP_FIGHT_KM")
						ELSE
							PRINT_HELP_ADV("ARM3HLP_FIGHT")
						ENDIF
					ENDIF
				ELIF (NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT2")  AND NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FIGHT2_KM"))
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
							PRINT_HELP_ADV("ARM3HLP_FIGHT2_KM")
						ELSE
							PRINT_HELP_ADV("ARM3HLP_FIGHT2")
						ENDIF
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FSTAT")
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP_ADV("ARM3HLP_FSTAT")
					ENDIF
				ELIF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_FSTAT2")
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP_ADV("ARM3HLP_FSTAT2")
					ENDIF
				ENDIF
				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("SimeonHeadWound1")
//					IF GET_CUTSCENE_TIME() > ROUND(9.233334 * 1000.0)
//						ENTITY_INDEX entitySimeon
//						entitySimeon = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Siemon", CS_SIEMONYETARIAN)
//						
//						IF DOES_ENTITY_EXIST(entitySimeon)
//						AND NOT IS_ENTITY_DEAD(entitySimeon)
//							APPLY_PED_DAMAGE_DECAL(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySimeon), PDZ_TORSO, 0.426, 0.755,  218.157, 1.0, 1.0, 3, TRUE, "bruise")
//							
//							SET_LABEL_AS_TRIGGERED("SimeonHeadWound1", TRUE)
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("SimeonHeadWound2")
//					IF GET_CUTSCENE_TIME() > ROUND(16.133335 * 1000.0)
//						ENTITY_INDEX entitySimeon
//						entitySimeon = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Siemon", CS_SIEMONYETARIAN)
//						
//						IF DOES_ENTITY_EXIST(entitySimeon)
//						AND NOT IS_ENTITY_DEAD(entitySimeon)
//							APPLY_PED_DAMAGE_DECAL(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySimeon), PDZ_TORSO, 0.448, 0.814, 0.0, 1.0, 1.0, 5, TRUE, "bruise")
//							APPLY_PED_DAMAGE_DECAL(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySimeon), PDZ_TORSO, 0.576, 0.715, 0.0, 1.0, 1.0, 3, TRUE, "bruise")
//							APPLY_PED_DAMAGE_DECAL(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySimeon), PDZ_TORSO, 0.576, 0.680, 0.0, 1.0, 1.0, 3, TRUE, "bruise")
//							APPLY_PED_BLOOD_SPECIFIC(GET_PED_INDEX_FROM_ENTITY_INDEX(entitySimeon), ENUM_TO_INT(PDZ_TORSO), 0.535, 0.547, 222.215, 1.0, 3, 0.0, "NonFatalHeadshot")
//							
//							SET_LABEL_AS_TRIGGERED("SimeonHeadWound2", TRUE)
//						ENDIF
//					ENDIF
//				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//					
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
//					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
					
//					FORCE_PED_MOTION_STATE(PLAYER_PED(CHAR_MICHAEL), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					
//					SET_PED_POSITION(PLAYER_PED_ID(), <<-62.6231, -1092.8240, 25.5139>>, 71.5533)
//					
//					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-110.292969 - GET_ENTITY_HEADING(PLAYER_PED_ID()))
//					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.353200)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmys_Car")
					SET_VEHICLE_DOORS_SHUT(vehCar)
					SET_ENTITY_COLLISION(vehCar, FALSE)
					SET_ENTITY_DYNAMIC(vehCar, TRUE)
					FREEZE_ENTITY_POSITION(vehCar, TRUE)
				ENDIF
				
				IF WAS_CUTSCENE_SKIPPED()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					
					bCutsceneSkipped = TRUE
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					IF bCutsceneSkipped
						LOAD_SCENE_ADV(<<-56.8300, -1094.9470, 25.4223>>, 100.0)
						
						DO_SCREEN_FADE_IN(1000)	#IF IS_DEBUG_BUILD	PRINTLN("DO_SCREEN_FADE_IN")	#ENDIF
					ENDIF
					
					bCutsceneSkipped = FALSE
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					ADVANCE_STAGE()
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			SET_LABEL_AS_TRIGGERED("FirstPunch", TRUE)
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FirstPunch")
		AND TIMERA() < 3000
			SET_PED_RESET_FLAG(pedOwner, PRF_SuspendInitiatedMeleeActions, TRUE)
			SET_PED_RESET_FLAG(pedOwner, PRF_ForcePedToStrafe, TRUE)
		ENDIF
	ENDIF
	
	IF CLEANUP_STAGE()
		//Cleanup (Blips, peds, variables etc.)
		REMOVE_CUTSCENE()
		
		REPLAY_STOP_EVENT()
		
		WHILE HAS_CUTSCENE_LOADED()
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		//Audio Scene
		IF IS_AUDIO_SCENE_ACTIVE("ARM_3_BEAT_DOWN")
			STOP_AUDIO_SCENE("ARM_3_BEAT_DOWN")
		ENDIF
		
		SAFE_DELETE_PED(pedOwner)
		
		SAFE_REMOVE_BLIP(blipDestination)
		SAFE_REMOVE_BLIP(blipOwner)
		
		//Phone
		DISABLE_CELLPHONE(FALSE)
		
//		SET_GAMEPLAY_CAM_RELATIVE_HEADING(165)
//		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
		
		//Unload Texture Dictionary
//		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SplashScreens")
		
		//Wanted
		SET_MAX_WANTED_LEVEL(6)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		
		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
	ENDIF
ENDPROC

//PROC ExitDealership()
//	IF INIT_STAGE()
//		//Player Control
//		SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//		
//		//Radar
//		bRadar = FALSE
//		
//		#IF IS_DEBUG_BUILD
//		IF bAutoSkipping = FALSE
//		#ENDIF
//		
//		//Print
//		CLEAR_PRINTS()
//		SAFE_CLEAR_HELP()
//		
////		PRINT_ADV("ARM3_EXIT")
//		
//		//Blip
//		SAFE_ADD_BLIP_LOCATION(blipDestination, <<-58.9105, -1094.3636, 25.4345>>)
//		
//		//Camera
//		IF NOT DOES_CAM_EXIST(camMain)
//			camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//		ENDIF
//		
//		#IF IS_DEBUG_BUILD
//		ENDIF
//		#ENDIF
//		
//		//Texture Dictionary
////		REQUEST_STREAMED_TEXTURE_DICT("SplashScreens")
//		
//		IF SKIPPED_STAGE()
//			//Freeze
//			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
//			
//			//Pin Interior
//			intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
//			
//			PIN_INTERIOR_IN_MEMORY(intGarage)
//			
//			WHILE NOT IS_INTERIOR_READY(intGarage)
//				PRINTLN("PINNING INTERIOR...")
//				
//				WAIT_WITH_DEATH_CHECKS(0)
//			ENDWHILE
//			
//			bPinnedGarage = TRUE
//			
//			//Unfreeze
//			SAFE_FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
//			
//			//Set Positions
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//			SET_PED_POSITION(PLAYER_PED_ID(), <<-58.6544, -1094.2231, 25.4345>>, 67.7985)
//			
//			SET_VEHICLE_POSITION(vehCar, <<-55.7623, -1096.5033, 25.4766>>, 309.4087)
//			
//			TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<-64.2202, -1092.5787, 25.5570>>, PEDMOVE_WALK)
//			
//			//Load Scene
//			LOAD_SCENE_ADV(GET_ENTITY_COORDS(PLAYER_PED_ID()))
//			
//			//Camera
//			SET_CAM_PARAMS(camMain, 
//							<<-64.228828, -1090.187012, 25.727079>>,
//							<<11.967910, 0.000001, -135.002151>>,
//							30.904352)
//			
//			SET_CAM_PARAMS(camMain, 
//							<<-64.306511, -1090.265625, 25.727079>>,
//							<<11.967911, 0.000001, -134.579224>>,
//							30.904352,
//							1500)
//			
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//			CLEAR_ROOM_FOR_GAME_VIEWPORT()
//			
//			//Camera Behind Player
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
//			
//			//Fade In
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//			ENDIF
//		ENDIF
//	ELSE
//		SWITCH iCutsceneStage
//			CASE 0
////				IF HAS_STREAMED_TEXTURE_DICT_LOADED("SplashScreens")
//					ADVANCE_CUTSCENE()
////				ENDIF
//			BREAK
//			CASE 1
//				//Wall 1
//				//	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-57.246544, -1086.109375, 24.628405>>, <<-61.189556, -1096.419922, 28.965427>>, 1.5)
//				//	-3.67, 1.23, 0.0
//				//Wall 2
//				//	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-61.326393, -1096.447510, 24.622618>>, <<-58.424828, -1101.298218, 28.931204>>, 1.5)
//				//	-3.49, -2.14, 0.0
//				//Wall 3
//				//	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-58.395573, -1101.279419, 24.431620>>, <<-34.990391, -1109.947510, 28.935814>>, 1.5)
//				//	-1.39, -3.86, 0.0
//				//Wall 4
//				//	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-34.855526, -1109.858765, 24.436039>>, <<-36.557720, -1114.361084, 28.935463>>, 1.5)
//				//	-3.85, 1.39, 0.0
//				
////				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-57.246544, -1086.109375, 24.628405>>, <<-61.189556, -1096.419922, 28.965427>>, 1.5)
////				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-61.326393, -1096.447510, 24.622618>>, <<-58.424828, -1101.298218, 28.931204>>, 1.5)
////				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-58.395573, -1101.279419, 24.431620>>, <<-34.990391, -1109.947510, 28.935814>>, 1.5)
////				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-34.855526, -1109.858765, 24.436039>>, <<-36.557720, -1114.361084, 28.935463>>, 1.5)
////					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
////						IF SAFE_START_CUTSCENE(10.0)
////							bRadar = FALSE
////							
////							CLEAR_PRINTS()
////							SAFE_CLEAR_HELP()
////							
////							SAFE_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
////							
////							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
////							
////							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-57.246544, -1086.109375, 24.628405>>, <<-61.189556, -1096.419922, 28.965427>>, 1.5)
////								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-3.67, 1.23, 0.0>>, PEDMOVE_WALK)
////								SET_CAM_COORD(camMain, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-3.67 * 2, 1.23 * 2, 1.0>>)
////							ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-61.326393, -1096.447510, 24.622618>>, <<-58.424828, -1101.298218, 28.931204>>, 1.5)
////								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-3.49, -2.14, 0.0>>, PEDMOVE_WALK)
////								SET_CAM_COORD(camMain, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-3.49 * 2, -2.14 * 2, 1.0>>)
////							ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-58.395573, -1101.279419, 24.431620>>, <<-34.990391, -1109.947510, 28.935814>>, 1.5)
////								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-1.39, -3.86, 0.0>>, PEDMOVE_WALK)
////								SET_CAM_COORD(camMain, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-1.39 * 2, -3.86 * 2, 1.0>>)
////							ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-34.855526, -1109.858765, 24.436039>>, <<-36.557720, -1114.361084, 28.935463>>, 1.5)
////								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-3.85, 1.39, 0.0>>, PEDMOVE_WALK)
////								SET_CAM_COORD(camMain, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<-3.85 * 2, 1.39 * 2, 1.0>>)
////							ENDIF
////							
////							SET_CAM_FOV(camMain, 20.0)
////							
////							POINT_CAM_AT_ENTITY(camMain, PLAYER_PED_ID(), <<0.0, 0.0, 0.5>>)
////							
////							RENDER_SCRIPT_CAMS(TRUE, FALSE)
////							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
////							CLEAR_ROOM_FOR_GAME_VIEWPORT()
////							
////							ADVANCE_CUTSCENE()
////						ENDIF
////					ENDIF					
////				ENDIF
//				
//				ADVANCE_CUTSCENE()
//			BREAK
//			CASE 2
//				IF TIMERA() > 1500
//					SET_CAM_PARAMS(camMain, 
//									<<-64.106102,-1092.782837,26.726273>>,
//									<<7.561244,0.000000,-106.236137>>,
//									24.746525)
//					
//					ADVANCE_CUTSCENE()
//				ENDIF
//			BREAK
//			CASE 3
//				IF TIMERA() > 1000
////					SET_TIMECYCLE_MODIFIER("CAMERA_BW")
//					
//					ADVANCE_CUTSCENE()
//				ENDIF
//			BREAK
//			CASE 4
////				SET_GAME_PAUSED(TRUE)
////				
////				INT xScreen, yScreen
////				VECTOR vTexture
////				
////				GET_SCREEN_RESOLUTION(xScreen, yScreen)
////				vTexture = GET_TEXTURE_RESOLUTION("SplashScreens", "GTAV_PlayerIntroNames_MichaelT")				
////				
////				DRAW_SPRITE("SplashScreens", "GTAV_PlayerIntroNames_MichaelD", 0.25, 0.3, vTexture.X / xScreen, vTexture.Y / yScreen, 0.0, 255, 255, 255, 255) //"MichaelTSplash", "GTAV_PlayerIntroNames_MichaelTownley"
////				
////				DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 0.1, 0, 0, 0, 255)
////				DRAW_RECT_FROM_CORNER(0.0, 0.9, 1.0, 0.1, 0, 0, 0, 255)
////				
////				IF TIMERA() > 2250
////					SET_GAME_PAUSED(FALSE)
////					
////					CLEAR_TIMECYCLE_MODIFIER()
//					
//					ADVANCE_CUTSCENE()
////				ENDIF
//			BREAK
//			CASE 5
//				IF TIMERA() > 300
//					ADVANCE_STAGE()
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ENDIF
//	
//	IF CLEANUP_STAGE()
//		//Cleanup (Blips, peds, variables etc.)
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		
//		//Camera Behind Player
//		SET_GAMEPLAY_CAM_RELATIVE_HEADING(165)
//		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
//		
//		SAFE_REMOVE_BLIP(blipDestination)
//		
//		//If skipped
//		SET_GAME_PAUSED(FALSE)
//		
//		CLEAR_TIMECYCLE_MODIFIER()
//		
//		//Unload Texture Dictionary
////		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("SplashScreens")
//		
//		eMissionObjective = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) + 1)
//	ENDIF
//ENDPROC

//══════════════════════════════════╡ DEBUG ╞════════════════════════════════════

#IF IS_DEBUG_BUILD

PROC debugRoutine()
	IF bAutoSkipping = TRUE
		IF ENUM_TO_INT(eMissionObjective) >= ENUM_TO_INT(eMissionObjectiveAutoSkip)
//			IF IS_SCREEN_FADED_OUT()
//				DO_SCREEN_FADE_IN(500)
//				
//				WHILE IS_SCREEN_FADING_IN()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
			
			eMissionObjectiveAutoSkip = initMission
			
			bAutoSkipping = FALSE
			bSkipped = TRUE
		ENDIF
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		missionPassed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		missionFailed()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	OR ENUM_TO_INT(eMissionObjective) < ENUM_TO_INT(eMissionObjectiveAutoSkip)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bSkipped = TRUE
		ENDIF
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
		ENDIF
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
		CLEAR_TEXT()
		
		//Cutscene
		WHILE NOT HAS_CUTSCENE_FINISHED()
			STOP_CUTSCENE(TRUE)
			
			PRINTLN("STOPPING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		REMOVE_CUTSCENE()
		
		WHILE HAS_CUTSCENE_LOADED()
			PRINTLN("REMOVING CUTSCENE...")
			
			WAIT_WITH_DEATH_CHECKS(0)
		ENDWHILE
		
		ADVANCE_STAGE()
		
		TEXT_LABEL txtLabel
		
		txtLabel = ENUM_TO_INT(eMissionObjective)
		
		PRINTSTRING(txtLabel)
		PRINTNL()
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
	OR LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iStageSkipMenu, ENUM_TO_INT(eMissionObjective))
		IF bAutoSkipping = FALSE
			bAutoSkipping = TRUE
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				eMissionObjectiveAutoSkip = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjective) - 1)
				
				//P-Skip Speciale
				IF eMissionObjectiveAutoSkip = cutArgue
					eMissionObjectiveAutoSkip = INT_TO_ENUM(MissionObjective, ENUM_TO_INT(eMissionObjectiveAutoSkip) - 1)
				ENDIF
			ELSE
				eMissionObjectiveAutoSkip = INT_TO_ENUM(MissionObjective, iStageSkipMenu)
			ENDIF
			
			eMissionObjective = initMission
			
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
			//Cutscene
			WHILE NOT HAS_CUTSCENE_FINISHED()
				STOP_CUTSCENE(TRUE)
				
				PRINTLN("STOPPING CUTSCENE...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			REMOVE_CUTSCENE()
			
			WHILE HAS_CUTSCENE_LOADED()
				PRINTLN("REMOVING CUTSCENE...")
				
				WAIT_WITH_DEATH_CHECKS(0)
			ENDWHILE
			
			MISSION_CLEANUP(TRUE)
			
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10000.0, TRUE)
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
	ENDIF
ENDPROC 

#ENDIF

//══════════════════════════════╡ MISSION SCRIPT ╞═══════════════════════════════

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		HANDLE_FAIL_AS_MICHAEL()
		MISSION_CLEANUP(FALSE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
//═══════════════════════════════╡ MISSION LOOP ╞════════════════════════════════
	
	#IF IS_DEBUG_BUILD
		MissionNames()
	#ENDIF
	
	//Fetch vehPlayer from armenian3_launcher
	IF NOT IS_REPLAY_IN_PROGRESS()
		IF NOT DOES_ENTITY_EXIST(vehDriveTo)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					vehDriveTo = GET_MISSION_START_VEHICLE_INDEX()
					
					IF DOES_ENTITY_EXIST(vehDriveTo)
					AND NOT IS_ENTITY_DEAD(vehDriveTo)
						#IF IS_DEBUG_BUILD	PRINTLN("vehDriveTo = ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehDriveTo))	#ENDIF
						SET_VEHICLE_DOORS_SHUT(vehDriveTo, TRUE)
						SET_ENTITY_AS_MISSION_ENTITY(vehDriveTo, TRUE, TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehDriveTo, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT g_sTriggerSceneAssets.flag
		IF NOT IS_REPLAY_IN_PROGRESS()
			bTimeOfDayRunning = FALSE
			
			WHILE NOT DO_TIMELAPSE(SP_MISSION_ARMENIAN_3, sTimelapse, FALSE, TRUE, FALSE, TRUE)
				bTimeOfDayRunning = TRUE
				
				WAIT(0)
			ENDWHILE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])	//Simeon
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedOwner = g_sTriggerSceneAssets.ped[0]
			SET_ENTITY_AS_MISSION_ENTITY(pedOwner, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])	//Customer
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			//Transfer the global index to a local index and grab ownership of the entity.
			pedIntro = g_sTriggerSceneAssets.ped[1]
			SET_ENTITY_AS_MISSION_ENTITY(pedIntro, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])	//Customer Car
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehIntro = g_sTriggerSceneAssets.veh[0]
			SET_ENTITY_AS_MISSION_ENTITY(vehIntro, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])	//Outside Car 1
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[1])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehGarageOutside[0] = g_sTriggerSceneAssets.veh[1]
			SET_ENTITY_AS_MISSION_ENTITY(vehGarageOutside[0], TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])	//Outside Car 2
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[2])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehGarageOutside[1] = g_sTriggerSceneAssets.veh[2]
			SET_ENTITY_AS_MISSION_ENTITY(vehGarageOutside[1], TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[3])	//Garage Car 1
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[3])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehGarage[0] = g_sTriggerSceneAssets.veh[3]
			SET_ENTITY_AS_MISSION_ENTITY(vehGarage[0], TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[4])	//Garage Car 2
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[4])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehGarage[1] = g_sTriggerSceneAssets.veh[4]
			SET_ENTITY_AS_MISSION_ENTITY(vehGarage[1], TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[5])	//Garage Car 3
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[5])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehGarage[2] = g_sTriggerSceneAssets.veh[5]
			SET_ENTITY_AS_MISSION_ENTITY(vehGarage[2], TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[6])	//Garage Car 4
		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[6])
			//Transfer the global index to a local index and grab ownership of the entity.
			vehGarage[3] = g_sTriggerSceneAssets.veh[6]
			SET_ENTITY_AS_MISSION_ENTITY(vehGarage[3], TRUE, TRUE)
		ENDIF
	ENDIF
	
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_MISSION_ARMENIAN_3)
	
	IF NOT IS_REPLAY_IN_PROGRESS()
		IF HAS_CUTSCENE_LOADED()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF DOES_ENTITY_EXIST(pedOwner)
			AND NOT IS_PED_INJURED(pedOwner)
				REGISTER_ENTITY_FOR_CUTSCENE(pedOwner, "Siemon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_SIMEON))
			ELSE
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Siemon", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_SIMEON))
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedIntro)
			AND NOT IS_PED_INJURED(pedIntro)
				REGISTER_ENTITY_FOR_CUTSCENE(pedIntro, "customer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, A_M_M_BevHills_02)
			ELSE
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "customer", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_M_BevHills_02)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehIntro)
			AND NOT IS_ENTITY_DEAD(vehIntro)
//				STOP_SYNCHRONIZED_ENTITY_ANIM(vehIntro, INSTANT_BLEND_OUT, TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(vehIntro, "Showroom_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PREMIER)
			ELSE
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Showroom_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PREMIER)
			ENDIF
			
			START_CUTSCENE()
			
			WAIT(0)
			
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-36.443756, -1114.312988, 24.939146>>, <<-58.570412, -1111.051758, 37.435764>>, 20.0, <<-58.3923, -1114.5612, 25.4358>>, 74.8206, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
			
			//When you want to cut the camera and go back into gameplay or the intro mocap scene call
			IF bTimeOfDayRunning
				SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
				
				bTimeOfDayComplete = TRUE
			ENDIF
			
			CLEAR_AREA(<<-47.07522, -1114.47644, 25.43581>>, 7.5, TRUE)
			CLEAR_AREA(<<-40.14164, -1113.71350, 25.43738>>, 5.0, TRUE)
			CLEAR_AREA(<<-57.75611, -1112.76880, 25.43581>>, 10.0, TRUE)
			
			SET_ROADS_IN_ANGLED_AREA(<<-801.065796, 187.031311, 71.605469>>, <<-797.865540, 178.343643, 74.834709>>, 9.0, FALSE, FALSE)
			
			STOP_GAMEPLAY_HINT(TRUE)
			
			CLEAR_TEXT()
			
			bPreloaded = TRUE
		ENDIF
	ENDIF
	
	WHILE (TRUE)
		//Video Recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_Complications")
		
		//Fail ChecksNew Search
		DEATH_CHECKS()
		
		CAUGHT_SNEAKING()
		
		IF bTimeOfDayRunning
			IF NOT IS_REPLAY_IN_PROGRESS()
				IF bTimeOfDayComplete = FALSE
					IF IS_CUTSCENE_PLAYING()
						//When you want to cut the camera and go back into gameplay or the intro mocap scene call
						SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE)
						
						bTimeOfDayComplete = TRUE
					ENDIF
				ENDIF
			ELSE
				bTimeOfDayComplete = TRUE
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("ARM3_GUN")
			IF eMissionObjective >= stageDriveTo AND eMissionObjective <= cutWindowSmash
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)	//INPUT_FRONTEND_LT
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
			ENDIF
		ENDIF
		
		//Interior Pinning
		//Mansion
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-856.007996, 170.520981, 56.539825>>, <<-764.767883, 168.507111, 86.732788>>, 45.0)
			IF intMansion = NULL
				intMansion = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1879, 179.9663, 71.1639>>, "V_Michael")
			ELSE
				IF bPinnedMansion = FALSE
					PIN_INTERIOR_IN_MEMORY(intMansion)
					
					IF NOT IS_INTERIOR_READY(intMansion)
						PRINTLN("PINNING INTERIOR...")
					ELSE
						bPinnedMansion = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Window
			CREATE_MODEL_HIDE(<<-802.73, 167.50, 77.58>>, 1.0, V_ILEV_MM_WINDOWWC, FALSE)
			
			IF NOT DOES_ENTITY_EXIST(objWindow)
				IF HAS_MODEL_LOADED_CHECK(V_ILEV_MM_WINDOWWC)
					CLEAR_AREA(<<-801.900024, 167.699997, 76.300002>>, 10.0, TRUE)
					
					objWindow = CREATE_OBJECT(V_ILEV_MM_WINDOWWC, <<-801.900024, 167.699997, 76.300002>>)
					FREEZE_ENTITY_POSITION(objWindow, TRUE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_MM_WINDOWWC)
				ENDIF
			ENDIF
			
			//Mansion Doors
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iBathroomDoor)
				ADD_DOOR_TO_SYSTEM(iBathroomDoor, V_ILEV_MM_DOORW, <<-804.95, 171.86, 76.89>>)
			ENDIF
			
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iJimmyDoor)
				ADD_DOOR_TO_SYSTEM(iJimmyDoor, V_ILEV_MM_DOORSON, <<-806.77, 174.02, 76.89>>)
			ENDIF
			
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iTraceyDoor)
				ADD_DOOR_TO_SYSTEM(iTraceyDoor, V_ILEV_MM_DOORDAUGHTER, <<-802.70, 176.18, 76.89>>)
			ENDIF
			
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iMichaelDoor)
				ADD_DOOR_TO_SYSTEM(iMichaelDoor, V_ILEV_MM_DOORW, <<-809.28, 177.86, 76.89>>)
			ENDIF
			
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
				ADD_DOOR_TO_SYSTEM(iGarageDoor, V_ILEV_MM_DOOR, <<-806.28, 186.02, 72.62>>)
			ENDIF
			
			IF eMissionObjective <= stageDriveTo
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(iBathroomDoor)
					IF DOOR_SYSTEM_GET_OPEN_RATIO(iBathroomDoor) <> -1.0
					OR DOOR_SYSTEM_GET_DOOR_STATE(iBathroomDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
						DOOR_SYSTEM_SET_OPEN_RATIO(iBathroomDoor, -1.0, FALSE, FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iBathroomDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
					ENDIF
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(iJimmyDoor)
					IF DOOR_SYSTEM_GET_OPEN_RATIO(iJimmyDoor) <> -1.0
					OR DOOR_SYSTEM_GET_DOOR_STATE(iJimmyDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
						DOOR_SYSTEM_SET_OPEN_RATIO(iJimmyDoor, -1.0, FALSE, FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iJimmyDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
					ENDIF
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(iTraceyDoor)
					IF DOOR_SYSTEM_GET_OPEN_RATIO(iTraceyDoor) <> 1.0
					OR DOOR_SYSTEM_GET_DOOR_STATE(iTraceyDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
						DOOR_SYSTEM_SET_OPEN_RATIO(iTraceyDoor, 1.0, FALSE, FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iTraceyDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("GarageDoorLockedSkip")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DoorLocked")
						IF IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
							IF DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) < -0.4 - 0.05
							OR DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) > -0.4 + 0.05
							OR DOOR_SYSTEM_GET_DOOR_STATE(iGarageDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
								FLOAT fOpenRation = DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor)
								
								IF fOpenRation < -0.4
									fOpenRation = fOpenRation +@ 0.1
								ELIF fOpenRation > -0.4
									fOpenRation = fOpenRation +@ -0.1
								ENDIF
								
								DOOR_SYSTEM_SET_OPEN_RATIO(iGarageDoor, fOpenRation, FALSE, FALSE)
								DOOR_SYSTEM_SET_DOOR_STATE(iGarageDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
							ENDIF
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.888123, 183.775513, 71.347801>>, <<-805.190063, 184.442764, 73.847801>>, 3.5)
							SET_LABEL_AS_TRIGGERED("DoorLocked", TRUE)
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.402283, 189.004593, 71.478920>>, <<-815.288208, 185.978165, 74.954453>>, 6.5)
							IF SMOOTH_CLOSE_DOOR(GARAGE_DOOR, V_ILEV_MM_DOOR, <<-806.28, 186.02, 72.62>>, TRUE)
								SET_LABEL_AS_TRIGGERED("GarageDoorSmoothClosed", TRUE)
							ELSE
								SET_LABEL_AS_TRIGGERED("GarageDoorSmoothClosed", FALSE)
							ENDIF
						ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.958984, 186.046143, 70.974792>>, <<-804.708191, 180.184219, 74.347801>>, 7.0)
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
								IF DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) < -0.4 - 0.05
								OR DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) > -0.4 + 0.05
								OR DOOR_SYSTEM_GET_DOOR_STATE(iGarageDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
									FLOAT fOpenRation = DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor)
									
									IF fOpenRation < -0.4
										fOpenRation = fOpenRation +@ 0.1
									ELIF fOpenRation > -0.4
										fOpenRation = fOpenRation +@ -0.1
									ENDIF
									
									DOOR_SYSTEM_SET_OPEN_RATIO(iGarageDoor, fOpenRation, FALSE, FALSE)
									DOOR_SYSTEM_SET_DOOR_STATE(iGarageDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
								ENDIF
							ENDIF
						ELSE
							IF IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
								IF DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) < -0.4 - 0.05
								OR DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) > -0.4 + 0.05
								OR DOOR_SYSTEM_GET_DOOR_STATE(iGarageDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
									FLOAT fOpenRation = DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor)
									
									IF fOpenRation < -0.4
										fOpenRation = fOpenRation +@ 0.1
									ELIF fOpenRation > -0.4
										fOpenRation = fOpenRation +@ -0.1
									ENDIF
									
									DOOR_SYSTEM_SET_OPEN_RATIO(iGarageDoor, fOpenRation, FALSE, FALSE)
									DOOR_SYSTEM_SET_DOOR_STATE(iGarageDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(iGarageDoor)
						IF DOOR_SYSTEM_GET_OPEN_RATIO(iGarageDoor) <> 0.0
						OR DOOR_SYSTEM_GET_DOOR_STATE(iGarageDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
							DOOR_SYSTEM_SET_OPEN_RATIO(iGarageDoor, 0.0, FALSE, FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(iGarageDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMichaelDoor)
					IF DOOR_SYSTEM_GET_OPEN_RATIO(iMichaelDoor) <> 0.0
					OR DOOR_SYSTEM_GET_DOOR_STATE(iMichaelDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
						DOOR_SYSTEM_SET_OPEN_RATIO(iMichaelDoor, 0.0, FALSE, FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iMichaelDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
					ENDIF
				ENDIF
				
//				SMOOTH_CLOSE_DOOR(V_ILEV_MM_DOORW, <<-807.3, 175.0, 71.2>>, TRUE, 1.0, 1.0)
			ENDIF
		ELIF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-864.316040, 170.810455, 56.566391>>, <<-757.057434, 167.819778, 77.386490>>, 55.0)
			IF intMansion <> NULL
				IF bPinnedMansion = TRUE
					UNPIN_INTERIOR(intMansion)
					
					bPinnedMansion = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		//Dealership Door
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor1)
			ADD_DOOR_TO_SYSTEM(iDealershipDoor1, V_ILEV_FIB_DOOR1, <<-31.72, -1101.85, 26.57>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor2)
			ADD_DOOR_TO_SYSTEM(iDealershipDoor2, V_ILEV_FIB_DOOR1, <<-33.81, -1107.58, 26.57>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor3)
			ADD_DOOR_TO_SYSTEM(iDealershipDoor3, V_ILEV_CSR_DOOR_L, <<-59.89, -1092.95, 26.88>>)
		ENDIF
		
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor4)
			ADD_DOOR_TO_SYSTEM(iDealershipDoor4, V_ILEV_CSR_DOOR_R, <<-60.55, -1094.75, 26.89>>)
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor1)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(iDealershipDoor1) <> 0.0
			OR DOOR_SYSTEM_GET_DOOR_STATE(iDealershipDoor1) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(iDealershipDoor1, 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDealershipDoor1, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor2)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(iDealershipDoor2) <> 0.0
			OR DOOR_SYSTEM_GET_DOOR_STATE(iDealershipDoor2) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(iDealershipDoor2, 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDealershipDoor2, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor3)
			IF DOOR_SYSTEM_GET_DOOR_STATE(iDealershipDoor3) != DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_DOOR_STATE(iDealershipDoor3, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoor4)
			IF DOOR_SYSTEM_GET_DOOR_STATE(iDealershipDoor4) != DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_DOOR_STATE(iDealershipDoor4, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		//Michael's House Side Gate
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iSideGate)
			ADD_DOOR_TO_SYSTEM(iSideGate, PROP_BH1_48_GATE_1, <<-8489343, 179.3079, 70.0247>>)
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iSideGate)
			IF DOOR_SYSTEM_GET_OPEN_RATIO(iSideGate) <> 0.0
			OR DOOR_SYSTEM_GET_DOOR_STATE(iSideGate) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
				DOOR_SYSTEM_SET_OPEN_RATIO(iSideGate, 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iSideGate, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		ENDIF
		
		//Michael's House Doors
		IF sHouseDoorData[0].doorHash <> ENUM_TO_INT(DOORHASH_M_MANSION_F_L)
			sHouseDoorData[0] = GET_DOOR_DATA(DOORNAME_M_MANSION_F_L)
			sHouseDoorData[1] = GET_DOOR_DATA(DOORNAME_M_MANSION_F_R)
			sHouseDoorData[2] = GET_DOOR_DATA(DOORNAME_M_MANSION_R_L1)
			sHouseDoorData[3] = GET_DOOR_DATA(DOORNAME_M_MANSION_R_R1)
			sHouseDoorData[4] = GET_DOOR_DATA(DOORNAME_M_MANSION_R_L2)
			sHouseDoorData[5] = GET_DOOR_DATA(DOORNAME_M_MANSION_R_R2)
			sHouseDoorData[6] = GET_DOOR_DATA(DOORNAME_M_MANSION_BW)
		ENDIF
		
		INT iHouseDoor
		
		REPEAT COUNT_OF(sHouseDoorData) iHouseDoor
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(sHouseDoorData[iHouseDoor].doorHash)
				IF DOOR_SYSTEM_GET_OPEN_RATIO(sHouseDoorData[iHouseDoor].doorHash) <> 0.0
				OR DOOR_SYSTEM_GET_DOOR_STATE(sHouseDoorData[iHouseDoor].doorHash) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
					DOOR_SYSTEM_SET_OPEN_RATIO(sHouseDoorData[iHouseDoor].doorHash, 0.0, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(sHouseDoorData[iHouseDoor].doorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Garage
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-160.906296, -1036.915771, 41.273239>>, <<95.394714, -1132.606201, 18.234772>>, 280.0)
		OR (eMissionObjective >= cutArrive
		AND eMissionObjective <= stageRammingSpeed)
			IF intGarage = NULL
				intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(vDealership, "v_carshowroom")
			ELSE
				IF bPinnedGarage = FALSE
					PIN_INTERIOR_IN_MEMORY(intGarage)
					
					IF NOT IS_INTERIOR_READY(intGarage)
						PRINTLN("PINNING INTERIOR...")
					ELSE
						bPinnedGarage = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-255.981781, -1028.398804, 42.458290>>, <<135.906250, -1172.973267, 21.366043>>, 280.0)
			IF intGarage <> NULL
				IF bPinnedGarage = TRUE
					UNPIN_INTERIOR(intGarage)
					
					bPinnedGarage = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF intGarage != NULL
			IF eMissionObjective > stageDriveTo
				IF NOT HAS_LABEL_BEEN_TRIGGERED("ShowroomShutter")
					SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED, TRUE) 	//Close the shutters
					
					SET_LABEL_AS_TRIGGERED("ShowroomShutter", TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = intMansion
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-802.754333, 167.437836, 75.595863>>, <<-804.581543, 172.115005, 79.240799>>, 3.0)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			ENDIF
		ENDIF
		
		IF (IS_BULLET_IN_BOX(<<-850.43, 139.94, 61.96>>, <<-770.38, 191.25, 74.20>>)
		AND NOT IS_PED_PLANTING_BOMB(PLAYER_PED_ID())
		AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
		AND IS_CURRENT_PED_WEAPON_NOISY(PLAYER_PED_ID()))
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-848.457703, 168.873795, 58.593575>>, <<-768.442993, 168.834671, 93.190750>>, 55.0)
		OR (DOES_ENTITY_EXIST(vehClimb)
		AND NOT IS_ENTITY_DEAD(vehClimb)
		AND IS_VEHICLE_ALARM_ACTIVATED(vehClimb))
			eMissionFail = failNoise
			
			missionFailed()
		ENDIF
		
		//Idle Anims
		IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_MICHAEL))
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(PLAYER_PED(CHAR_MICHAEL), FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(PLAYER_PED(CHAR_FRANKLIN))
			SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(PLAYER_PED(CHAR_FRANKLIN), FALSE)
		ENDIF
		
		//Debug Routine
		#IF IS_DEBUG_BUILD
			debugRoutine()
		#ENDIF
		
		//Hide HUD/Radar
		IF bRadar = FALSE
		OR bOverHoodCinematicCam = TRUE
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
		ELSE
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
		ENDIF
		
//		IF IS_INTERPOLATING_TO_SCRIPT_CAMS()
//		OR IS_INTERPOLATING_FROM_SCRIPT_CAMS()
//		OR (DOES_CAM_EXIST(camMain)
//		AND IS_CAM_INTERPOLATING(camMain))
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
//			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
//		ENDIF
		
		IF eMissionObjective = stageGoToMansion OR eMissionObjective = stageClimbUp
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-843.0493, 172.7284, 68.7735>>, <<150.0, 150.0, 150.0>>)
				IF HAS_MODEL_LOADED_CHECK(S_M_M_GARDENER_01)
					IF NOT DOES_ENTITY_EXIST(pedGardener)
						SPAWN_PED(pedGardener, S_M_M_GARDENER_01, <<-835.3764, 184.7189, 70.8434>>, 331.4128)
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GARDENER_01)
						SET_ENTITY_HEALTH(pedGardener, 101)
						SET_PED_MAX_HEALTH(pedGardener, 101)
						SET_PED_COMPONENT_VARIATION(pedGardener, PED_COMP_HEAD, 0, 1)
						SET_PED_COMPONENT_VARIATION(pedGardener, PED_COMP_TORSO, 1, 0)
						SET_PED_COMPONENT_VARIATION(pedGardener, PED_COMP_LEG, 1, 1)
						SET_PED_COMPONENT_VARIATION(pedGardener, PED_COMP_SPECIAL, 1, 0)
						SET_PED_COMPONENT_VARIATION(pedGardener, PED_COMP_DECL, 2, 0)
						SET_PED_PROP_INDEX(pedGardener, ANCHOR_HEAD, 0, 1)
						
						BLOCK_PED_FROM_GENERATING_DEAD_BODY_EVENTS_WHEN_DEAD(pedGardener, TRUE)
						
						SET_PED_HEARING_RANGE(pedGardener, 10.0)
						SET_PED_VISUAL_FIELD_PROPERTIES(pedGardener, 40, 10, 30, -80, 80)
						
						SET_PED_CONFIG_FLAG(pedGardener, PCF_DontInfluenceWantedLevel, TRUE)
						
						//SET_PED_COMBAT_ATTRIBUTES(pedGardener, CA_ALWAYS_FLEE, TRUE)
						
						ADD_PED_FOR_DIALOGUE(sPedsForConversation, 2, pedGardener, "GARDENER")
						
						objLeafblower = CREATE_OBJECT(PROP_LEAF_BLOWER_01, <<-837.1777, 181.6656, 70.1302>>)
						ATTACH_ENTITY_TO_ENTITY(objLeafblower, pedGardener, GET_PED_BONE_INDEX(pedGardener, BONETAG_PH_R_HAND), VECTOR_ZERO, VECTOR_ZERO)
						
//						vGardenPoints[0] = <<-837.5160, 183.0201, 70.4801>>
//						fGardenPoints[0] = 313.7573
//						vGardenPoints[1] = <<-838.4312, 183.6012, 70.5555>>
//						fGardenPoints[1] = 339.2294
//						vGardenPoints[2] = <<-836.5823, 183.6595, 70.6604>>
//						fGardenPoints[2] = 340.9408
//						vGardenPoints[3] = <<-835.6536, 183.2167, 70.5513>>
//						fGardenPoints[3] = 298.2337
//						vGardenPoints[4] = <<-836.5137, 182.1445, 70.2603>>
//						fGardenPoints[4] = 285.9332
//						vGardenPoints[5] = <<-837.2863, 182.6584, 70.3903>>
//						fGardenPoints[5] = 328.3498
//						vGardenPoints[6] = <<-838.3234, 182.7076, 70.3408>>
//						fGardenPoints[6] = 320.1913
					ELSE
						IF IS_ENTITY_ATTACHED(objLeafblower)
							//Sound
							IF sIDLeaf = -1
								IF REQUEST_AMBIENT_AUDIO_BANK("GARDEN_LEAF_BLOWER")
									sIDLeaf = GET_SOUND_ID()
									PLAY_SOUND_FROM_ENTITY(sIDLeaf, "GARDENING_LEAFBLOWER_ANIM_TRIGGERED", objLeafblower)
								ENDIF
							ENDIF
							
							//Particles
							IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeaf)
								REQUEST_PTFX_ASSET()
								
								IF HAS_PTFX_ASSET_LOADED()
									ptfxLeaf = START_PARTICLE_FX_LOOPED_ON_ENTITY("ent_anim_leaf_blower", objLeafblower, <<1.0, 0.0, -0.25>>, <<0.0, -45.0, 0.0>>)
								ENDIF
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedGardener)
						AND IS_SCREEN_FADED_IN()
							IF IS_PED_INJURED(pedGardener)
								IF IS_THIS_PRINT_BEING_DISPLAYED("ARM3_GARDEN")
									CLEAR_PRINTS()
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(pedGardener)
								IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictGarden)
									IF NOT IS_ENTITY_PLAYING_ANIM(pedGardener, sAnimDictGarden, "Blower_Idle_a")
									AND GET_SCRIPT_TASK_STATUS(pedGardener, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
										TASK_PLAY_ANIM(pedGardener, sAnimDictGarden, "Blower_Idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
									ENDIF
								ENDIF
								
								IF NOT HAS_CLIP_SET_LOADED(sAnimDictGardenerMoveClipSet)
									REQUEST_CLIP_SET(sAnimDictGardenerMoveClipSet)
								ENDIF
								
								IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictGarden)
								AND HAS_CLIP_SET_LOADED(sAnimDictGardenerMoveClipSet)
									IF HAS_LABEL_BEEN_TRIGGERED("ARM3HLP_SNEAK")
										IF NOT HAS_LABEL_BEEN_TRIGGERED("GardenerAdvance")
											IF GET_GAME_TIMER() > iClimbTimer
												//TASK_PLAY_ANIM(pedGardener, sAnimDictGarden, "Blower_Idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY | AF_LOOPING)
												SET_PED_MOVEMENT_CLIPSET(pedGardener, sAnimDictGardenerMoveClipSet)
												SET_PED_WEAPON_MOVEMENT_CLIPSET(pedGardener, sAnimDictGardenerMoveClipSet)
												
												OPEN_SEQUENCE_TASK(seqMain)
													TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-827.8197, 191.4635, 72.2064>>, 0.5 /*PEDMOVE_WALK*/, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 299.3068)
													TASK_PLAY_ANIM(NULL, sAnimDictGarden, "Blower_Idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
												CLOSE_SEQUENCE_TASK(seqMain)
												TASK_PERFORM_SEQUENCE(pedGardener, seqMain)
												CLEAR_SEQUENCE_TASK(seqMain)
												
												SET_LABEL_AS_TRIGGERED("GardenerAdvance", TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
//								IF NOT IS_PED_IN_COMBAT(pedGardener)
//									SET_PED_MOVE_RATE_OVERRIDE(pedGardener, 0.5)
//								ENDIF
							ENDIF
							
							IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictGarden)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("ACT_stealth_kill_a_gardener")
									ACTION_MANAGER_ENABLE_ACTION(HASH("ACT_stealth_kill_a_gardener"), TRUE)
									
									SET_LABEL_AS_TRIGGERED("ACT_stealth_kill_a_gardener", TRUE)
								ENDIF
							ENDIF
							
							IF IS_PLAYER_CLIMBING(PLAYER_ID())
								iClimbTimer = GET_GAME_TIMER() + 3000
							ENDIF
							
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedGardener, FALSE), <<8.0, 8.0, 2.0>>)
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedGardener, 3000)
							ENDIF
							
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-847.812927, 182.998001, 66.362663>>, <<-811.278198, 180.250854, 81.592949>>, 30.0)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-843.973389, 159.883316, 65.309196>>, <<-809.686829, 159.163422, 76.786880>>, 22.0)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-796.666016, 190.556183, 71.834915>>, <<-819.054932, 191.735168, 84.225395>>, 7.0)
								SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
								
								IF IS_PED_PLANTING_BOMB(PLAYER_PED_ID())
								OR IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
								OR NOT IS_CURRENT_PED_WEAPON_NOISY(PLAYER_PED_ID())
									SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_FIREARM_DISCHARGE)
								ENDIF
								
								IF IS_PED_INJURED(pedGardener)
								OR HAS_ANIM_EVENT_FIRED(pedGardener, HASH("DROP"))
									SAFE_REMOVE_BLIP(blipGardener)
									
									DETACH_ENTITY(objLeafblower, TRUE, FALSE)
									
									IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeaf)
										STOP_PARTICLE_FX_LOOPED(ptfxLeaf)
									ENDIF
									
									IF sIDLeaf != -1
										STOP_SOUND(sIDLeaf)
										RELEASE_SOUND_ID(sIDLeaf)
										sIDLeaf = -1
									ENDIF
									
									INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ARM3_GARDEN_KO)	#IF IS_DEBUG_BUILD PRINTLN("GardenerTakedown = TRUE")	#ENDIF
									
									SET_LABEL_AS_TRIGGERED("GardenerTakedown", TRUE)
								ENDIF
								
								IF NOT DOES_BLIP_EXIST(blipGardener)
								AND NOT HAS_LABEL_BEEN_TRIGGERED("GardenerTakedown")
									SAFE_ADD_BLIP_PED(blipGardener, pedGardener)
								ENDIF
								
								IF GET_PED_RELATIONSHIP_GROUP_HASH(pedGardener) != relGroupEnemy
									SET_PED_RELATIONSHIP_GROUP_HASH(pedGardener, relGroupEnemy)
								ENDIF
								
								IF NOT IS_PED_INJURED(pedGardener)
									IF NOT HAS_LABEL_BEEN_TRIGGERED("GardenerAlert")
										IF NOT HAS_LABEL_BEEN_TRIGGERED("GardenerTakedown")
										OR IS_ENTITY_ATTACHED(objLeafblower)
											IF IS_PED_IN_COMBAT(pedGardener)
											OR IS_PED_RAGDOLL(pedGardener)
											OR (CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedGardener)
											AND GET_GAME_TIMER() > iClimbTimer)
											OR GET_PED_ALERTNESS(pedGardener) <> AS_NOT_ALERT
											OR IS_PED_FLEEING(pedGardener)
											OR (NOT IS_ENTITY_PLAYING_ANIM(pedGardener, sAnimDictGarden, "Blower_Idle_a")
											AND GET_SCRIPT_TASK_STATUS(pedGardener, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK)
											//OR IS_PED_INJURED(pedGardener)
												SET_LABEL_AS_TRIGGERED("ARM3_GARDEN", FALSE)
												
												IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() % 2 = 0
													CREATE_CONVERSATION_ADV("ARM3_GARDEN")
												ELSE
													CREATE_CONVERSATION_ADV("ARM3_GARDEN2")
												ENDIF
												
												SET_LABEL_AS_TRIGGERED("ARM3_GARDEN", TRUE)
												
												CLEAR_PED_TASKS(pedGardener)
												
												TASK_LOOK_AT_ENTITY(pedGardener, PLAYER_PED_ID(), 1000, SLF_USE_TORSO)
												
												OPEN_SEQUENCE_TASK(seqMain)
													TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
													TASK_REACT_AND_FLEE_PED(NULL, PLAYER_PED_ID())
												CLOSE_SEQUENCE_TASK(seqMain)
												TASK_PERFORM_SEQUENCE(pedGardener, seqMain)
												CLEAR_SEQUENCE_TASK(seqMain)
												
												WHILE DOES_ENTITY_EXIST(pedGardener)
												AND NOT IS_PED_INJURED(pedGardener)
												AND NOT IS_PED_IN_COMBAT(pedGardener)
												AND NOT IS_PED_RAGDOLL(pedGardener)
												AND GET_SCRIPT_TASK_STATUS(pedGardener, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
													WAIT_WITH_DEATH_CHECKS(0)
												ENDWHILE
												
												IF DOES_ENTITY_EXIST(pedGardener)
												AND NOT IS_PED_INJURED(pedGardener)
												AND IS_ENTITY_PLAYING_ANIM(pedGardener, sAnimDictGarden, "Blower_Idle_a")
													STOP_ANIM_TASK(pedGardener, sAnimDictGarden, "Blower_Idle_a")
												ENDIF
												
												DETACH_ENTITY(objLeafblower, TRUE, FALSE)
												
												IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxLeaf)
													STOP_PARTICLE_FX_LOOPED(ptfxLeaf)
												ENDIF
												
												IF sIDLeaf != -1
													STOP_SOUND(sIDLeaf)
													RELEASE_SOUND_ID(sIDLeaf)
													sIDLeaf = -1
												ENDIF
												
												iAlertTimer = GET_GAME_TIMER() + 2000
												
												IF DOES_ENTITY_EXIST(pedGardener)
												AND IS_PED_INJURED(pedGardener)
													REPLAY_RECORD_BACK_FOR_TIME(5.5, 2.0, REPLAY_IMPORTANCE_HIGH)
												ENDIF
												
												SET_LABEL_AS_TRIGGERED("GardenerAlert", TRUE)
											ENDIF
										ENDIF
									ELSE
										IF GET_GAME_TIMER() > iAlertTimer
											WAIT_WITH_DEATH_CHECKS(500)
											
											eMissionFail = failNoise
											missionFailed()
										ENDIF
									ENDIF
								ENDIF
							ELSE
								//SAFE_REMOVE_BLIP(blipGardener)
								
								IF NOT IS_PED_INJURED(pedGardener)
									IF GET_PED_RELATIONSHIP_GROUP_HASH(pedGardener) = relGroupEnemy
										IF IS_PED_IN_COMBAT(pedGardener)
											CLEAR_PED_TASKS(pedGardener)
										ENDIF
										
										SET_PED_RELATIONSHIP_GROUP_HASH(pedGardener, relGroupBuddy)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Looping Anims
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael") >= 1.0)
			OR (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop2_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop2_michael") >= 1.0)
			OR (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop3_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop3_michael") >= 1.0)
			OR (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop4_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop4_michael") >= 1.0)
			OR (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop5_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop5_michael") >= 1.0)
			OR (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop6_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop6_michael") >= 1.0)
				INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 6)
				
				IF iRandom = 0
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				ELIF iRandom = 1
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop2_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				ELIF iRandom = 2
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop3_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				ELIF iRandom = 3
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop4_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				ELIF iRandom = 4
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop5_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				ELIF iRandom = 5
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop6_michael", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				ENDIF
			ENDIF
		ENDIF
		
		//Interrupt Dialogue
		IF SAFE_IS_PLAYER_CONTROL_ON()
		AND eMissionObjective = stageDriveTo
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF (IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael")
				AND GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_intro_michael") > 0.75)
				OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
				OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop2_michael")
				OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop3_michael")
				OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop4_michael")
				OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop5_michael")
				OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop6_michael")
					IF (HAS_LABEL_BEEN_TRIGGERED("ARM3_DRIV"))
					AND GET_GAME_TIMER() > iInterruptTimer
						IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_STOP")
							IF NOT IS_STRING_NULL_OR_EMPTY(txtConversationPoint)
								IF (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF IS_ENTITY_UPRIGHT(vehCar)
									AND IS_VEHICLE_ON_ALL_WHEELS(vehCar)
										//CREATE_CONVERSATION_FROM_SPECIFIC_LINE_ADV("ARM3_DRIV", txtConversationPoint)
										
										txtConversationPoint = NULL_STRING()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_PHONE")
						AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_CRASH")
						AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_FLIP")
						AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_HIT")
						AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_ROUTE")
						AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_GOGO")
						AND NOT IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_GETOUT")
							//Damages Car (Crashes)
							IF GET_ENTITY_HEALTH(vehCar) < iCarHealth
							OR GET_VEHICLE_PETROL_TANK_HEALTH(vehCar) < iCarGasTank
							OR GET_VEHICLE_ENGINE_HEALTH(vehCar) < iCarEngineHealth
								IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
								OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(txtConversationPoint)
									txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								ENDIF
								
								//CREATE_CONVERSATION_ADV("ARM3_CRASH", CONV_PRIORITY_HIGH)
								INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_MICHAEL), "ARM3_BGAA", "MICHAEL")	PRINTLN("INTERRUPT_CONVERSATION - Damages Car (Crashes)")
								
								iCarHealth = GET_ENTITY_HEALTH(vehCar)
								iCarGasTank = GET_VEHICLE_PETROL_TANK_HEALTH(vehCar)
								iCarEngineHealth = GET_VEHICLE_ENGINE_HEALTH(vehCar)
								
								iInterruptTimer = GET_GAME_TIMER() + 12000
							ENDIF
							
							//Flips Car
							IF NOT IS_ENTITY_UPRIGHT(vehCar)
								IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
								OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(txtConversationPoint)
									txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								ENDIF
								
								//CREATE_CONVERSATION_ADV("ARM3_FLIP", CONV_PRIORITY_HIGH)
								INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_MICHAEL), "ARM3_BPAA", "MICHAEL")	PRINTLN("INTERRUPT_CONVERSATION - Flips Car")
								
								iInterruptTimer = GET_GAME_TIMER() + 12000
							ENDIF
							
							//Hits a Ped
							INT iEventCount = 0
							
							STRUCT_ENTITY_ID sei = CONSTRUCT_ENTITY_ID()
							EVENT_NAMES eventType
							
							REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventCount
								eventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventCount)
								
								SWITCH (eventType)
									CASE EVENT_ENTITY_DESTROYED
									CASE EVENT_ENTITY_DAMAGED
										//Figure out what type of entity it is
										GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventCount, sei, SIZE_OF(STRUCT_ENTITY_ID))
										
										IF DOES_ENTITY_EXIST(sei.EntityId)//sei.EntityId
											IF IS_ENTITY_A_PED(sei.EntityId)
												IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
												OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(txtConversationPoint)
													txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
												ENDIF
												
												//CREATE_CONVERSATION_ADV("ARM3_HIT", CONV_PRIORITY_HIGH)
												INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_MICHAEL), "ARM3_BHAA", "MICHAEL")	PRINTLN("INTERRUPT_CONVERSATION - Hits a Ped")
												
												IF eventType = EVENT_ENTITY_DESTROYED
													INFORM_MISSION_STATS_OF_INCREMENT(ARM3_INNOCENTS_KILLED)
												ENDIF
												
												iInterruptTimer = GET_GAME_TIMER() + 12000
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							ENDREPEAT
							
							//Off Route
							//ARM3_ROUTE
							
							//Doesn't Drive
							IF NOT HAS_LABEL_BEEN_TRIGGERED("ARM3_STOP")
								IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop_michael")
								OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop2_michael")
								OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop3_michael")
								OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop4_michael")
								OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop5_michael")
								OR IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], sAnimDictArm3, "michaelappears_loop6_michael")
									IF NOT HAS_LABEL_BEEN_TRIGGERED("Destination1")
									AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination2")
									AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination3")
									AND NOT HAS_LABEL_BEEN_TRIGGERED("Destination4")
										IF GET_ENTITY_SPEED(vehCar) < 1.0
											IF iStoppedTimer = -1
												iStoppedTimer = GET_GAME_TIMER() + 3000
											ENDIF
										ELSE
											iStoppedTimer = -1
										ENDIF
										
										IF iStoppedTimer != -1
										AND GET_GAME_TIMER() > iStoppedTimer
										AND NOT HAS_LABEL_BEEN_TRIGGERED("HaltVehicle")
											IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
											OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(txtConversationPoint)
												txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
											ENDIF
											
											//CREATE_CONVERSATION_ADV("ARM3_GOGO", CONV_PRIORITY_HIGH)
											INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_MICHAEL), "ARM3_BJAA", "MICHAEL")	PRINTLN("INTERRUPT_CONVERSATION - Doesn't Drive")
											
											iInterruptTimer = GET_GAME_TIMER() + 12000
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							//Tries to Get Out
							IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
								IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
								OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(txtConversationPoint)
									txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								ENDIF
								
								//CREATE_CONVERSATION_ADV("ARM3_GETOUT", CONV_PRIORITY_HIGH)
								INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_MICHAEL), "ARM3_BKAA", "MICHAEL")	PRINTLN("INTERRUPT_CONVERSATION - Tries to Get Out")
								
								iInterruptTimer = GET_GAME_TIMER() + 12000
							ENDIF
							
							//Uses Phone
							IF IS_PHONE_ONSCREEN(TRUE)
								IF IS_THIS_CONVERSATION_ONGOING_OR_QUEUED("ARM3_DRIV")
								OR IS_THIS_CONVERSATION_ONGOING_OR_QUEUED(txtConversationPoint)
									txtConversationPoint = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								ENDIF
								
								//CREATE_CONVERSATION_ADV("ARM3_PHONE", CONV_PRIORITY_HIGH)
								INTERRUPT_CONVERSATION(PLAYER_PED(CHAR_MICHAEL), "ARM3_BLAA", "MICHAEL")	PRINTLN("INTERRUPT_CONVERSATION - Uses Phone")
								
								HANG_UP_AND_PUT_AWAY_PHONE()
								
								DISABLE_CELLPHONE(TRUE)
								
								iInterruptTimer = GET_GAME_TIMER() + 12000
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), ARM3_DAMAGE)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), ARM3_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), ARM3_MAX_SPEED)
		ELSE
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, ARM3_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, ARM3_MAX_SPEED)
		ENDIF
		
		//Stop peds attacking player
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_DRIVE_AGAINST_TRAFFIC)
		SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_RUN_REDLIGHT)
		SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_RECKLESS_DRIVING)
		SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_SPEEDING)
		
		LOAD_UNLOAD_ASSETS()
		
		AUDIO_CONTROLLER()
		
//		PRINTLN("GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() = ", ENUM_TO_INT(GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL()))
//		PRINTLN("GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = ", ENUM_TO_INT(GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)))
		
		//Objective switch
		SWITCH eMissionObjective 
			CASE initMission
				initialiseMission()
			BREAK
			CASE cutIntro
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3LeadInOut)
					cutsceneIntro()
				ENDIF
			BREAK
			CASE stageGoToMansion
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3LeadInOut)
					GoToMansion()
				ENDIF
			BREAK
			CASE stageClimbUp
				IF HAS_MODEL_LOADED_CHECK(BJXL)
				//AND HAS_MODEL_LOADED_CHECK(PROP_HD_SEATS_01)
				AND HAS_MODEL_LOADED_CHECK(BISON3)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					ClimbUp()
				ENDIF
			BREAK
			CASE cutArgue
				IF HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_JIMMY))
				AND HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_TRACEY))
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3Climb)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3Argue)
					cutsceneArgue()
				ENDIF
			BREAK
			CASE stageSneakThrough
				IF HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_JIMMY))
				AND HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_TRACEY))
				AND HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_AMANDA))
				AND HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_TENNIS_COACH))
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3Argue)
					SneakThrough()
				ENDIF
			BREAK
			CASE stageStealCar
				IF HAS_MODEL_LOADED_CHECK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					StealCar()
				ENDIF
			BREAK
			CASE stageDriveTo
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
					DriveTo()
				ENDIF
				
				OVER_HOOD_CINEMATIC_CAM()
			BREAK
			CASE cutArrive
				CutsceneArrive()
			BREAK
			CASE stageRammingSpeed
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictArm3)
				AND HAS_MODEL_LOADED_CHECK(PROP_SHOWROOM_GLASS_1B)
					RammingSpeed()
				ENDIF
			BREAK
			CASE cutWindowSmash
				IF HAS_MODEL_LOADED_CHECK(GET_NPC_PED_MODEL(CHAR_SIMEON))
				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictLeadInMCS8)
//				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictSlam)
					CutsceneWindowSmash()
				ENDIF
			BREAK
			CASE stageBeatDown
				IF HAS_ANIM_DICT_LOADED_CHECK(sAnimDictAngry)
//				AND HAS_ANIM_DICT_LOADED_CHECK(sAnimDictSlam)
					BeatDown()
				ENDIF
			BREAK
//			CASE stageExitDealership
//				ExitDealership()
//			BREAK
//			CASE stageSwitchTutorial
//				SwitchTutorial()
//			BREAK
			CASE passMission
				missionPassed()
			BREAK
			CASE failMission
				missionFailed()
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
	
//Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
